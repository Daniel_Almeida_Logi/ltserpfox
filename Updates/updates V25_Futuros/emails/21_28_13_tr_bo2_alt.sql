
-- ================================================
-- Template generated from Template Explorer using:
-- Create Trigger (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- See additional Create Trigger templates for more
-- examples of different Trigger statements.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Jose Simoes>
-- Create date: <18/11/2021>
-- Description:	<trigger to insert in bo2_alt>
-- =============================================
if OBJECT_ID('[dbo].[tr_bo2_alt]') IS NOT NULL
	DROP trigger [dbo].tr_bo2_alt;
GO

CREATE TRIGGER [dbo].[tr_bo2_alt] ON [dbo].[bo2]
   AFTER INSERT, UPDATE, DELETE
AS 
BEGIN
	SET NOCOUNT ON;

	Declare @Type varchar(50)='';
	DECLARE @bostamp varchar(25)
	DECLARE @status varchar(50)
	DECLARE @autoriza_emails bit 
	DECLARE @site VARCHAR(20)
	DECLARE @email VARCHAR(100)


	IF EXISTS (SELECT * FROM inserted) and  EXISTS (SELECT * FROM deleted)
	BEGIN
		set  @Type = 'UPDATE'
		INSERT into [bo2_alt] (bo2stamp,autotipo,pdtipo,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,armazem,xpdviatura,xpddata,xpdhora,morada,codpost,telefone,contacto,email,etotalciva,etotiva,u_class,u_doccont,u_fostamp,ebo71_IVA,ebo81_IVA,ebo91_IVA
							   ,ATDocCode,exportado,ebo71_bins,ebo72_bins,ebo72_iva,ebo81_bins,ebo82_bins,ebo82_iva,ebo91_bins,ebo92_bins,ebo92_iva,IVATX1,IVATX2,IVATX3,IVATX4,IVATX5,IVATX6,IVATX7,IVATX8,IVATX9,nrReceita,codext
							   ,morada_ent,localidade_ent,codpost_ent,pais_ent,status,modo_envio,pagamento,u_nratend,pinAcesso,pinOpcao,nrviasimp,momento_pagamento,ebo101_bins,ebo111_bins,ebo121_bins,ebo131_bins,ebo101_iva,ebo111_iva,ebo121_iva,ebo131_iva
							   ,ivatx10,ivatx11,ivatx12,ivatx13,integrado,dataIntegrado,iddistribuidor,ticketId,alteracao,alteracaoData)
		select 	bo2stamp,autotipo,pdtipo,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,armazem,xpdviatura,xpddata,xpdhora,morada,codpost,telefone,contacto,email,etotalciva,etotiva,u_class,u_doccont,u_fostamp,ebo71_IVA,ebo81_IVA,ebo91_IVA
							   ,ATDocCode,exportado,ebo71_bins,ebo72_bins,ebo72_iva,ebo81_bins,ebo82_bins,ebo82_iva,ebo91_bins,ebo92_bins,ebo92_iva,IVATX1,IVATX2,IVATX3,IVATX4,IVATX5,IVATX6,IVATX7,IVATX8,IVATX9,nrReceita,codext
							   ,morada_ent,localidade_ent,codpost_ent,pais_ent,status,modo_envio,pagamento,u_nratend,pinAcesso,pinOpcao,nrviasimp,momento_pagamento,ebo101_bins,ebo111_bins,ebo121_bins,ebo131_bins,ebo101_iva,ebo111_iva,ebo121_iva,ebo131_iva
							   ,ivatx10,ivatx11,ivatx12,ivatx13,integrado,dataIntegrado,iddistribuidor,ticketId,@type,GETDATE()
		from inserted			


	END
	ELSE IF EXISTS(SELECT * FROM inserted)
	BEGIN
		set @Type = 'INSERT'
		INSERT into [bo2_alt] (bo2stamp,autotipo,pdtipo,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,armazem,xpdviatura,xpddata,xpdhora,morada,codpost,telefone,contacto,email,etotalciva,etotiva,u_class,u_doccont,u_fostamp,ebo71_IVA,ebo81_IVA,ebo91_IVA
							   ,ATDocCode,exportado,ebo71_bins,ebo72_bins,ebo72_iva,ebo81_bins,ebo82_bins,ebo82_iva,ebo91_bins,ebo92_bins,ebo92_iva,IVATX1,IVATX2,IVATX3,IVATX4,IVATX5,IVATX6,IVATX7,IVATX8,IVATX9,nrReceita,codext
							   ,morada_ent,localidade_ent,codpost_ent,pais_ent,status,modo_envio,pagamento,u_nratend,pinAcesso,pinOpcao,nrviasimp,momento_pagamento,ebo101_bins,ebo111_bins,ebo121_bins,ebo131_bins,ebo101_iva,ebo111_iva,ebo121_iva,ebo131_iva
							   ,ivatx10,ivatx11,ivatx12,ivatx13,integrado,dataIntegrado,iddistribuidor,ticketId,alteracao,alteracaoData)
		select 	bo2stamp,autotipo,pdtipo,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,armazem,xpdviatura,xpddata,xpdhora,morada,codpost,telefone,contacto,email,etotalciva,etotiva,u_class,u_doccont,u_fostamp,ebo71_IVA,ebo81_IVA,ebo91_IVA
							   ,ATDocCode,exportado,ebo71_bins,ebo72_bins,ebo72_iva,ebo81_bins,ebo82_bins,ebo82_iva,ebo91_bins,ebo92_bins,ebo92_iva,IVATX1,IVATX2,IVATX3,IVATX4,IVATX5,IVATX6,IVATX7,IVATX8,IVATX9,nrReceita,codext
							   ,morada_ent,localidade_ent,codpost_ent,pais_ent,status,modo_envio,pagamento,u_nratend,pinAcesso,pinOpcao,nrviasimp,momento_pagamento,ebo101_bins,ebo111_bins,ebo121_bins,ebo131_bins,ebo101_iva,ebo111_iva,ebo121_iva,ebo131_iva
							   ,ivatx10,ivatx11,ivatx12,ivatx13,integrado,dataIntegrado,iddistribuidor,ticketId,@type,GETDATE()
		from inserted	

	END
	ElSE IF EXISTS(SELECT * FROM deleted)
	BEGIN
		set @Type = 'DELETE'
		INSERT into [bo2_alt] (bo2stamp,autotipo,pdtipo,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,armazem,xpdviatura,xpddata,xpdhora,morada,codpost,telefone,contacto,email,etotalciva,etotiva,u_class,u_doccont,u_fostamp,ebo71_IVA,ebo81_IVA,ebo91_IVA
							   ,ATDocCode,exportado,ebo71_bins,ebo72_bins,ebo72_iva,ebo81_bins,ebo82_bins,ebo82_iva,ebo91_bins,ebo92_bins,ebo92_iva,IVATX1,IVATX2,IVATX3,IVATX4,IVATX5,IVATX6,IVATX7,IVATX8,IVATX9,nrReceita,codext
							   ,morada_ent,localidade_ent,codpost_ent,pais_ent,status,modo_envio,pagamento,u_nratend,pinAcesso,pinOpcao,nrviasimp,momento_pagamento,ebo101_bins,ebo111_bins,ebo121_bins,ebo131_bins,ebo101_iva,ebo111_iva,ebo121_iva,ebo131_iva
							   ,ivatx10,ivatx11,ivatx12,ivatx13,integrado,dataIntegrado,iddistribuidor,ticketId,alteracao,alteracaoData)
		select 	bo2stamp,autotipo,pdtipo,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,armazem,xpdviatura,xpddata,xpdhora,morada,codpost,telefone,contacto,email,etotalciva,etotiva,u_class,u_doccont,u_fostamp,ebo71_IVA,ebo81_IVA,ebo91_IVA
							   ,ATDocCode,exportado,ebo71_bins,ebo72_bins,ebo72_iva,ebo81_bins,ebo82_bins,ebo82_iva,ebo91_bins,ebo92_bins,ebo92_iva,IVATX1,IVATX2,IVATX3,IVATX4,IVATX5,IVATX6,IVATX7,IVATX8,IVATX9,nrReceita,codext
							   ,morada_ent,localidade_ent,codpost_ent,pais_ent,status,modo_envio,pagamento,u_nratend,pinAcesso,pinOpcao,nrviasimp,momento_pagamento,ebo101_bins,ebo111_bins,ebo121_bins,ebo131_bins,ebo101_iva,ebo111_iva,ebo121_iva,ebo131_iva
							   ,ivatx10,ivatx11,ivatx12,ivatx13,integrado,dataIntegrado,iddistribuidor,ticketId,@type,GETDATE()
		from deleted

	END


	SELECT 
		 @bostamp= inserted.bo2stamp,
		 @status=inserted.status,
		 @autoriza_emails=b_utentes.autoriza_emails,
		 @site= bo.site,
		 @email= case when ISNULL(inserted.email,'') = '' then b_utentes.email else inserted.email end 
	from inserted
			 INNER JOIN bo			ON bo.bostamp =   inserted.bo2stamp
			 INNER JOIN b_utentes ON bo.no = b_utentes.NO  and bo.estab= b_utentes.estab
	where ndos=41 and inserted.status !=''


	if( ISNULL(@bostamp,'')!='' AND (SELECT TOP 1 bool FROM B_Parameters_site  WHERE stamp='ADM0000000146' and site=@site)=1 AND NOT EXISTS(select * from docsToSendSchedule where tableName='BO' and tableStamp=@bostamp and status=@status) AND @autoriza_emails=1 )
	BEGIN
		DECLARE @tokenDocTosend varchar(36)
		SET @tokenDocTosend = LEFT(NEWID(),36)

		INSERT INTO docsToSendSchedule (stamp,tokenDocToSend,tableName,tableStamp,status,typeUser,typeUserDesc,ousrdata,sent)
			VALUES (LEFT(NEWID(),36), @tokenDocTosend,'BO',@bostamp,@status,1,'Cliente' ,GETDATE(),0)


		INSERT INTO docsToSend(TOKEN,id,site,nameDoc,usp,typeDocSave,typeSend,frequency,typeExec,dateNextCall,state,toSend,visibility,delimiter,zip)
		values(@tokenDocTosend,(select max(id) + 100 from docsToSend),@site, 'Envio de email de encomenda','up_get_EmailOrdersToSend','HTML','EMAIL','UNICO','ORDERS',GETDATE(),1,@email,1,'',0)

		IF( @status IN (select items from dbo.up_splitToTable((SELECT textValue FROM B_Parameters_site WHERE STAMP='ADM0000000147' AND site=@site),';')))
		BEGIN 
			DECLARE @tokenDocTosendP varchar(36)
			SET @tokenDocTosendP = LEFT(NEWID(),36)

			Declare @emailEmpresa varchar(100)

			select @emailEmpresa = email_bcc from empresa where site=@site
		
			INSERT INTO docsToSendSchedule (stamp,tokenDocToSend,tableName,tableStamp,status,typeUser,typeUserDesc,ousrdata,sent)
			VALUES (LEFT(NEWID(),36), @tokenDocTosendP,'BO',@bostamp,@status,2,'Farmacia' ,GETDATE(),0)

			INSERT INTO docsToSend(TOKEN,id,site,nameDoc,usp,typeDocSave,typeSend,frequency,typeExec,dateNextCall,state,toSend,visibility,delimiter,zip)
			values(@tokenDocTosendP,(select max(id) + 20 from docsToSend),@site, 'Envio de email de encomenda-Farmacia','up_get_EmailOrdersToSend','HTML','EMAIL','UNICO','ORDERS',GETDATE(),1,@emailEmpresa,1,'',0)

		END

	END 

END