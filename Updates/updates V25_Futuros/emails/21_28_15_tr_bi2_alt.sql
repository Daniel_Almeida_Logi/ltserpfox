
-- ================================================
-- Template generated from Template Explorer using:
-- Create Trigger (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- See additional Create Trigger templates for more
-- examples of different Trigger statements.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Jose Simoes>
-- Create date: <18/11/2021>
-- Description:	<trigger to insert in bi2_alt>
-- =============================================
if OBJECT_ID('[dbo].[tr_bi2_alt]') IS NOT NULL
	DROP trigger [dbo].tr_bi2_alt;
GO

CREATE TRIGGER [dbo].tr_bi2_alt ON [dbo].[bi2]
   AFTER INSERT, UPDATE, DELETE
AS 
BEGIN
	SET NOCOUNT ON;

	Declare @Type varchar(50)='';
	IF EXISTS (SELECT * FROM inserted) and  EXISTS (SELECT * FROM deleted)
	BEGIN
		set  @Type = 'UPDATE'
		INSERT into [bi2_alt] (bi2stamp,fnstamp,bostamp,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,morada,local,codpost,fistamp,exportado,valeNr,idCampanha,qttembal,valcartao,nrreceita,alteracao,alteracaoData)
		select 	bi2stamp,fnstamp,bostamp,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,morada,local,codpost,fistamp,exportado,valeNr,idCampanha,qttembal,valcartao,nrreceita,@type,GETDATE()
		from inserted			


	END
	ELSE IF EXISTS(SELECT * FROM inserted)
	BEGIN
		set @Type = 'INSERT'
		INSERT into [bi2_alt] (bi2stamp,fnstamp,bostamp,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,morada,local,codpost,fistamp,exportado,valeNr,idCampanha,qttembal,valcartao,nrreceita,alteracao,alteracaoData)
		select 	bi2stamp,fnstamp,bostamp,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,morada,local,codpost,fistamp,exportado,valeNr,idCampanha,qttembal,valcartao,nrreceita,@type,GETDATE()
		from inserted	
	END
	ElSE IF EXISTS(SELECT * FROM deleted)
	BEGIN
		set @Type = 'DELETE'
		INSERT into [bi2_alt] (bi2stamp,fnstamp,bostamp,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,morada,local,codpost,fistamp,exportado,valeNr,idCampanha,qttembal,valcartao,nrreceita,alteracao,alteracaoData)
		select 	bi2stamp,fnstamp,bostamp,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,morada,local,codpost,fistamp,exportado,valeNr,idCampanha,qttembal,valcartao,nrreceita,@type,GETDATE()
		from deleted

	END


END
GO
