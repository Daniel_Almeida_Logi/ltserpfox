
/*
	exec up_dem_dadosReceita 'FRr1E40DE00-CDAD-4D73-81C'
	select * from dispensa_eletronica where chave_pedido = 'ADMDC19E09F-62F5-446C-B9E'

				exec up_dem_dadosReceita 'ADM8FA4F9FA-0090-438E-93A'
*/
if OBJECT_ID('[dbo].[up_dem_dadosReceita]') IS NOT NULL
	drop procedure dbo.up_dem_dadosReceita
go

create procedure dbo.up_dem_dadosReceita
	@chave_pedido  varchar (100)

	/* WITH ENCRYPTION */
	AS
	
		select 
			'sel'	= convert(bit,0)
			,id
			,resultado_consulta_cod
			,resultado_consulta_descr
			,Dispensa_Eletronica.token
			,medicamento_cod = ISNULL(Dispensa_Eletronica_D.medicamento_cod,'')
			,medicamento_cnpem = ISNULL(Dispensa_Eletronica_D.medicamento_cnpem,'')
			,medicamento_descr =  ISNULL(LEFT(LTRIM(RTRIM(Dispensa_Eletronica_D.medicamento_descr)),250),'')
			,medicamento_descrcomp = iif(LEN(rtrim(ltrim(medicamento_descr)))>250,medicamento_descr,'')
			--,medicamento_descr = ISNULL(Dispensa_Eletronica_D.medicamento_descr,'')
			,receita_nr
			,excecao = ISNULL(Dispensa_Eletronica_D.excecao,'')
			,diploma_cod = ISNULL(diploma_cod,'')
			,diploma_descr = ISNULL(diploma_descr,'')
			,diploma_cod2 = ISNULL(diploma_cod2,'')
			,diploma_descr2 = ISNULL(diploma_descr2,'')
			,u_design = ISNULL(dplms.u_design,'')
			,dplmsstamp = ISNULL(dplms.dplmsstamp,'')
			,posologia  = convert(varchar(254),left(REPLACE(ISNULL(Dispensa_Eletronica_D.posologia,''),'<br>',char(13)+char(10)),254))
			,receita_tipo = ISNULL(Dispensa_Eletronica.receita_tipo,'')
			,efr_cod = ISNULL(Dispensa_Eletronica.efr_cod,'')
			,tipo_linha = ISNULL(Dispensa_Eletronica_D.tipo_linha,'')
			,Dispensa_Eletronica.recm
			,Dispensa_Eletronica.pais_migrante
			,receita_data = convert(datetime,Dispensa_Eletronica.receita_data)
			--,validade_linha1 = isnull(case when Dispensa_Eletronica_D.receita_renovavel = 'S' then convert(datetime,DateAdd(month,6,receita_data)) else convert(datetime,DateAdd(month,1,receita_data)) END,'')
			,validade_linha = isnull(case when Dispensa_Eletronica_D.data_caducidade = '1900-01-01' then convert(datetime,DateAdd(month,6,receita_data)) else Dispensa_Eletronica_D.data_caducidade END,'')
			,medico_codigo = convert(varchar,prescritor_nr_ordem)
			,'prescritor_nome' = ISNULL(prescritor_nome,'')
			,'prescritor_contacto'=ISNULL(prescritor_contacto,'')
			,'utente_nome' = ISNULL(utente_descr,'')
			,'utente_contacto'=ISNULL(utente_contacto,'')
			,'especialidade' =   ISNULL((SELECT top 1 especialidade FROM medico_especialidade(nolock) WHERE id= Dispensa_Eletronica.prescritor_especialidade),'')
			,'local_presc' = ISNULL(local_presc_descr,'')
			,'nbenef' = (case when efr_cod='935601' then beneficiario_nr else '' end)
			,'nrcartao' = (case when efr_cod<>'935601' then beneficiario_nr else '' end)
			,'AT'	= convert(bit,0)
			,'tipoCamara' = rtrim(ltrim(isnull(Dispensa_Eletronica_D.tipoCamara,'')))
			,'tipoPsci'   = rtrim(ltrim(isnull(Dispensa_Eletronica_D.codigoPSCI,'')))
			,'descrCamara' = convert(varchar(254),left(rtrim(ltrim(isnull(Dispensa_Eletronica_D.descrCamara,''))),400))
			,'descrPsci' = convert(varchar(254),left(rtrim(ltrim(isnull(Dispensa_Eletronica_D.descrPsci,''))),400))
			,ISNULL(Dispensa_Eletronica.anulacaoData, '') AS anulacaoData
			,ISNULL(Dispensa_Eletronica.anulacaoMotivo, '') AS anulacaoMotivo
			,Dispensa_Eletronica_D.areaCodigo
			,Dispensa_Eletronica_D.areaDescr
			,Dispensa_Eletronica_D.taxaModeradora
			,Dispensa_Eletronica_D.tokenEfetivado
			,'qtdMaxDispensavel'=(case when Dispensa_Eletronica_D.qtdMaxDispensavel < 0
									then 'N/A' else convert(varchar,Dispensa_Eletronica_D.qtdMaxDispensavel) end)
			,'qtdMaxPorDispensar' = (case when Dispensa_Eletronica_D.qtdMaxPorDispensar < 0
									then 'N/A' else convert(varchar,Dispensa_Eletronica_D.qtdMaxPorDispensar) end) 
			,Dispensa_Eletronica_D.tokenPosologia
			,(CASE
				WHEN CONVERT(varchar(MAX), Dispensa_Eletronica_D.tokenEfetivado) <> '' 
					THEN CAST(1 as bit)
				ELSE
					CAST(0 as bit)
			 END) as isEfetivado
			 ,isnull(bonusTipo,'')  as bonusTipo
			 ,isnull(bonusId,'')    as bonusId
			 ,isnull(bonusDescr,'') as bonusDescr
			 ,isnull(Dispensa_Eletronica_D.CHNM,'') as CHNM
			 ,efr_descr = ISNULL(Dispensa_Eletronica.efr_descr,'')
			 ,id_processo = ISNULL(Dispensa_Eletronica.idProcesso,'')
			 ,id_episodio = ISNULL(Dispensa_Eletronica.numeroEpisodio,'')
		from 
			Dispensa_Eletronica (nolock) 
			left join Dispensa_Eletronica_D (nolock) on Dispensa_Eletronica.token = Dispensa_Eletronica_D.token
			left join dplms (nolock) on diploma_cod = diploma_id
		where 
			Dispensa_Eletronica.chave_pedido = @chave_pedido
			AND Dispensa_Eletronica.chave_pedido != ''
				

GO
Grant Execute On dbo.up_dem_dadosReceita to Public
Grant Control On dbo.up_dem_dadosReceita to Public
Go 



