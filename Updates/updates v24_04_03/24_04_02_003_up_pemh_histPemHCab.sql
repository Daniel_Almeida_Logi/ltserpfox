/*
	         exec up_PEMH_histPEMHCab 'ADM678E2476-3A70-4468-B5B'
	exec up_pemh_histPemhCab  'D66AF523-AD92-4434-BD68-0A75931D3517'
	exec up_pemh_histPemhCab  ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_pemh_histpemhCab]') IS NOT NULL
	drop procedure dbo.up_pemh_histpemhCab
go

create procedure dbo.up_pemh_histpemhCab
	@groupToken VARCHAR(50),
	@nrUtente Varchar(15) = ''

AS
BEGIN

	SELECT
		receita_nr
		,CONVERT(VARCHAR, receita_data, 112) AS receita_data
		,utente_descr as design
		,isnull(utente_contacto,'') as contacto
		,ISNULL(beneficiario_nr, '') as nrBeneficiario
		,ISNULL(numeroEpisodio, '') as  numeroEpisodio
		,ISNULL(idProcesso, '') as  idProcesso
		,token
	FROM
		Dispensa_Eletronica(nolock)
	WHERE
		groupToken = @groupToken
		and resultado_consulta_cod = '10400100001'
		and beneficiario_nr=  case when @nrUtente <> '' 
				then 
					@nrUtente
				else
					beneficiario_nr
			end
	ORDER BY
		Dispensa_eletronica.receita_data DESC

END

GO
Grant Execute on dbo.up_pemh_histpemhCab to Public
Grant Control on dbo.up_pemh_histpemhCab to Public
GO