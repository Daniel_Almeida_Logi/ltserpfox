/*Criar cursor ucrsVale2 para regularizar adiantamento*/
/*exec up_reservas_regularizarAdiantamento 'ADM997A7742-0820-4974-AAA'*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_reservas_regularizarAdiantamento]') IS NOT NULL
	drop procedure dbo.up_reservas_regularizarAdiantamento
go

create procedure dbo.up_reservas_regularizarAdiantamento

@fistamp char(25)
/* with encryption */

AS 
BEGIN
	select top 1
		ft.ftstamp
		,fi.fistamp
		,convert(varchar,fdata,102) as fdata
		,ft.ousrhora
		,case when ft.cobrado=1 then ft.nmdoc + ' (S)' else ft.nmdoc end as nmdoc
		,ft.fno
		,ft.nome
		,ft.no
		,ft.estab
		,ft.ncont
		,fi.ref
		,fi.design
		,fi.desconto
		,fi.desc2
		,fi.u_descval
		,fi.u_comp
		,qtt = fi.qtt
		,fi.qtt as qtt2
		,fi.etiliquido
		,fi.etiliquido as etiliquido2 
		,totlin = fi.etiliquido
		,fi.etiliquido as totalCdesc
		,fi.epv
		,ft.ndoc
		,ft.u_lote
		,ft.u_nslote
		,ft.u_lote2
		,ft.u_nslote2
		,convert(bit,0) as sel
		,CONVERT(bit,0) as copied
		,ft.u_ltstamp
		,ft.u_ltstamp2
		,ft2.u_codigo
		,ft2.u_codigo2
		,fi.u_diploma
		,0 as dev
		,CONVERT(bit,0) as fact
		,CONVERT(bit,0) as factPago
		,fi.fmarcada
		,eaquisicao	= fi.qtt
		,oeaquisicao = fi.qtt
		,fi.u_psicont
		,fi.u_bencont
		,ft2.u_receita
		,ft2.u_nbenef
		,ft2.u_nbenef2
		,ft2.u_nopresc
		,ft2.u_entpresc
		,u_recdata		= convert(datetime,'19000101')
		,u_medico		= ''
		,u_nmutdisp		= ''
		,u_moutdisp		= ''
		,u_cputdisp		= ''
		,u_nmutavi		= ''
		,u_moutavi		= ''
		,u_cputavi		= ''
		,u_ndutavi		= ''
		,u_ddutavi		= convert(datetime,'19000101')
		,u_idutavi		= 0
		,fi.u_txcomp
		,fi.u_epvp
		,fi.u_ettent1
		,fi.u_ettent2
		,docinvert	= CONVERT(bit,0)
		,u_backoff = isnull(re.u_backoff,0)
		,lancacc = convert(bit,0)
		,ft2.u_design
		,ft2.u_abrev
		,ft2.u_abrev2
		,fi.u_epref
		,ft.tipodoc
		,fi.tabiva
		,fi.iva
		,fi.opcao
		,ft2.token
		,ft2.codAcesso
		,ft2.codDirOpcao
		,fi.id_Dispensa_Eletronica_D as id_validacaoDem
		,fi.amostra
		,ft2.c2codpost
		,fi.lote
		,fi2.dataValidade
	from
		ft (nolock)
		inner join fi (nolock) on ft.ftstamp=fi.ftstamp
		LEFT JOIN fi2 (nolock) on fi2.fistamp = fi.fistamp
		inner join ft2 (nolock) on ft.ftstamp=ft2.ft2stamp
		left join re (nolock) on re.restamp=(select top 1 restamp from rl where rl.ccstamp=ft.ftstamp)
	where
		fi.fistamp = @fistamp
END

GO
Grant Execute on dbo.up_reservas_regularizarAdiantamento to Public
GO
Grant Control on dbo.up_reservas_regularizarAdiantamento to Public
GO