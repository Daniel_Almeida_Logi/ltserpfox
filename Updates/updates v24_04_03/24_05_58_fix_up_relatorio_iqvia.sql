
/****** Object:  StoredProcedure [dbo].[up_relatorio_iqvia]    Script Date: 18/11/2024 16:07:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[up_relatorio_iqvia]
    @site           VARCHAR(36),
    @dataini        DATETIME = '19000101',
    @datafim        DATETIME = '19000101',
    @questionario   VARCHAR(254)
AS
SET NOCOUNT ON
   
    DECLARE @sortedColumns NVARCHAR(MAX) = ''
    DECLARE @columns NVARCHAR(MAX) = ''
    DECLARE @sql NVARCHAR(MAX) = ''
    DECLARE @columnNames NVARCHAR(MAX) = ''

    -- Criar uma tabela temporária para armazenar as perguntas
    CREATE TABLE #TempPerguntas (
        pergunta VARCHAR(255),
        ordem   INT
    )

    -- Inserir perguntas distintas na tabela temporária
    INSERT INTO #TempPerguntas (pergunta, ordem)
    SELECT DISTINCT 
        pergunta, 
        ordem
    FROM 
        quest_respostas(NOLOCK)
    INNER JOIN 
        quest_pergunta(NOLOCK) ON quest_respostas.quest_perguntaStamp = quest_pergunta.quest_perguntastamp
    INNER JOIN 
        Quest(NOLOCK) ON Quest.QuestStamp =  quest_respostas.questStamp
    WHERE 
        (Quest.Descr IN (SELECT items FROM dbo.up_splitToTable(@questionario, ',')) OR @questionario = '')



	CREATE TABLE #TempColumns (
		ColumnPart NVARCHAR(MAX)
	)

	-- Insert parts of the column list
	INSERT INTO #TempColumns (ColumnPart)
	SELECT QUOTENAME(pergunta)
	FROM #TempPerguntas
	ORDER BY ordem

	SELECT @sortedColumns = STUFF((
    SELECT ', ' + ColumnPart
    FROM #TempColumns
    FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, '')


    -- Ordenar as perguntas na tabela temporária
    --SELECT 
    --    @sortedColumns = COALESCE(@sortedColumns + ', ', '') + QUOTENAME(pergunta)
    --FROM 
    --    #TempPerguntas
    --ORDER BY 
    --    ordem
	Print @sortedColumns
    -- Construir a lista de colunas ordenadas
    SET @columns = @sortedColumns
	--SET @columns =STUFF(@columns, 1, 1, '')
	Print @columns
    -- Construir a lista de nomes das colunas
    SELECT 
        @columnNames = COALESCE(@columnNames + ', ', '') + '''' + pergunta + ''''
    FROM 
        #TempPerguntas
    ORDER BY 
        ordem

	 -- Remover a primeira vírgula, se houver
    SET @columnNames = STUFF(@columnNames, 1, 1, '')

    -- Consulta dinâmica
    SET @sql = '
	SELECT 
        ''Data'','+ @columnNames +', ''Ref'', ''Codfarm'', ''Nomabrv''
    UNION ALL
    SELECT DISTINCT
         ousrdata,' + @columns + ', ref, codfarm, nomabrv
    FROM (
        SELECT 
            CONVERT(VARCHAR(10), quest_respostas.ousrdata, 103)  +'' ''+
        CONVERT(VARCHAR(8), quest_respostas.ousrdata, 108) AS ousrdata,
            quest_pergunta.pergunta, 
            ISNULL(quest_respostas.resposta,'''') AS resposta,
            quest_respostas.ref,
            empresa.codfarm,
            empresa.nomabrv,
			quest_respostas.quest_respostas_grpStamp as grpstamp
        FROM
            quest_respostas(NOLOCK)
            LEFT JOIN fi(NOLOCK) ON fi.fistamp = quest_respostas.fistamp
            LEFT JOIN ft(NOLOCK) ON ft.ftstamp = fi.ftstamp
            INNER JOIN empresa(NOLOCK) ON empresa.site = quest_respostas.site
            left JOIN quest_pergunta(NOLOCK) ON quest_pergunta.quest_perguntastamp = quest_respostas.quest_perguntaStamp
            INNER JOIN Quest(NOLOCK) ON Quest.QuestStamp = quest_respostas.questStamp
        WHERE
            quest_respostas.site = '''+ @site +'''
            AND  CONVERT(VARCHAR, quest_respostas.ousrdata,23)    BETWEEN '''+CONVERT(VARCHAR, @dataIni,23)+ '''  AND
                                     '''+CONVERT(VARCHAR, @dataFim,23)+''' 
            AND (Quest.Descr IN ('''+ @questionario +''' ))
    ) AS Source
    PIVOT (
        MAX(resposta)
        FOR pergunta IN (' + @columns + ')
    ) AS PivotTable

   '
  -- set @sql = 'Select * from #tempcolumns'
    -- Executar consulta
    PRINT @sql
    EXEC (@sql)
