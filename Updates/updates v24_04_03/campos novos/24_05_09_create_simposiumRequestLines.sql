
	   -- drop table simposiumRequestLines
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'simposiumRequestLines'))
BEGIN
	

CREATE TABLE [dbo].[simposiumRequestLines](
	[stamp]				[varchar](36) NOT NULL  ,
	[token]				[varchar](36) NOT NULL  ,
	[ref]				[varchar](100),
	[ousrinis]			[varchar](30),
	[ousrdata]			[datetime] NOT NULL,
	[usrinis]			[varchar](30),
	[usrdata]			[datetime] NOT NULL,
	PRIMARY KEY (stamp)
) ON [PRIMARY]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [IX_simposiumRequestLines_stamp] ON [dbo].[simposiumRequestLines]
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

SET ANSI_PADDING ON

ALTER TABLE [dbo].[simposiumRequestLines] ADD  CONSTRAINT [DF_simposiumRequestLines_token]  DEFAULT ('') FOR [token]

ALTER TABLE [dbo].[simposiumRequestLines] ADD  CONSTRAINT [DF_simposiumRequestLines_ref]  DEFAULT ('') FOR [ref]

ALTER TABLE [dbo].[simposiumRequestLines] ADD  CONSTRAINT [DF_simposiumRequestLines_ousrinis]  DEFAULT ('') FOR [ousrinis]

ALTER TABLE [dbo].[simposiumRequestLines] ADD  CONSTRAINT [DF_simposiumRequestLines_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

ALTER TABLE [dbo].[simposiumRequestLines] ADD  CONSTRAINT [DF_simposiumRequestLines_usrinis]  DEFAULT ('') FOR [usrinis]

ALTER TABLE [dbo].[simposiumRequestLines] ADD  CONSTRAINT [DF_simposiumRequestLines_usrdata]  DEFAULT (getdate()) FOR [usrdata]

END


