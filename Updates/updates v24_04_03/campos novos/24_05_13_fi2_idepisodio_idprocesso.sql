DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'fi2'
	,@columnName		= 'idProcesso'
	,@columnType		= 'varchar (200)'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go


DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'fi2'
	,@columnName		= 'idEpisodio'
	,@columnType		= 'varchar (200)'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go


IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[fi2]') AND name = N'IX_fi2_processo_episodio')
CREATE NONCLUSTERED INDEX [IX_fi2_processo_episodio] ON [dbo].[fi2]
(
	idProcesso ASC,
	idEpisodio ASC

)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO




DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'Dispensa_Eletronica_D'
	,@columnName		= 'dataPrimeiraDispensa'
	,@columnType		= 'varchar (200)'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go





DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'Dispensa_Eletronica'
	,@columnName		= 'localExt'
	,@columnType		= 'varchar (200)'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go



DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'Dispensa_Eletronica'
	,@columnName		= 'contactoExt'
	,@columnType		= 'varchar (200)'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go

