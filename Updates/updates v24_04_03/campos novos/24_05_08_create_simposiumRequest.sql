
	   -- drop table simposiumRequest
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'simposiumRequest'))
BEGIN
	

CREATE TABLE [dbo].[simposiumRequest](
	[token]				[varchar](36) NOT NULL  ,
	[typeRequest]		NUMERIC(5,0),
	[typeRequestDesc]	[varchar](254),
	[test]				bit,
	[workplace]			[varchar](100),
	[service]			[varchar](254) NOT NULL,
	[memberId]			[varchar](100),
	[site]				[varchar](30),
	[ousrinis]			[varchar](30),
	[ousrdata]			[datetime] NOT NULL,
	[usrinis]			[varchar](30),
	[usrdata]			[datetime] NOT NULL,
	[nrAtend]			[varchar](20),
	PRIMARY KEY (token)
) ON [PRIMARY]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [IX_simposiumRequest_token] ON [dbo].[simposiumRequest]
(
	[token] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

SET ANSI_PADDING ON


ALTER TABLE [dbo].[simposiumRequest] ADD  CONSTRAINT [DF_simposiumRequest_typeRequest]  DEFAULT ('0') FOR [typeRequest]

ALTER TABLE [dbo].[simposiumRequest] ADD  CONSTRAINT [DF_simposiumRequest_typeRequestDesc]  DEFAULT ('') FOR [typeRequestDesc]

ALTER TABLE [dbo].[simposiumRequest] ADD  CONSTRAINT [DF_simposiumRequest_test]  DEFAULT ('1') FOR [test]

ALTER TABLE [dbo].[simposiumRequest] ADD  CONSTRAINT [DF_simposiumRequest_workplace]  DEFAULT ('') FOR [workplace]

ALTER TABLE [dbo].[simposiumRequest] ADD  CONSTRAINT [DF_simposiumRequest_service]  DEFAULT ('') FOR [service]

ALTER TABLE [dbo].[simposiumRequest] ADD  CONSTRAINT [DF_simposiumRequest_memberId]  DEFAULT ('') FOR [memberId]

ALTER TABLE [dbo].[simposiumRequest] ADD  CONSTRAINT [DF_simposiumRequest_site]  DEFAULT ('') FOR [site]

ALTER TABLE [dbo].[simposiumRequest] ADD  CONSTRAINT [DF_simposiumRequest_ousrinis]  DEFAULT ('') FOR [ousrinis]

ALTER TABLE [dbo].[simposiumRequest] ADD  CONSTRAINT [DF_simposiumRequest_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

ALTER TABLE [dbo].[simposiumRequest] ADD  CONSTRAINT [DF_simposiumRequest_usrinis]  DEFAULT ('') FOR [usrinis]

ALTER TABLE [dbo].[simposiumRequest] ADD  CONSTRAINT [DF_simposiumRequest_usrdata]  DEFAULT (getdate()) FOR [usrdata]

ALTER TABLE [dbo].[simposiumRequest] ADD  CONSTRAINT [DF_simposiumRequest_nrAtend]  DEFAULT ('') FOR [nrAtend]



end


DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'simposiumRequest'
	,@columnName		= 'memberId'
	,@columnType		= 'varchar(100)'
	,@columnDefault		= '('''')'
	
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
			and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end
	
	if @columnDefault is null
	begin
		exec('ALTER TABLE dbo.' + @tableName + ' alter column ' + @columnName + ' ' + @columnType)
	end else begin
		exec('ALTER TABLE dbo.' + @tableName + ' alter column ' + @columnName + ' ' + @columnType + ' not null ')
		exec('ALTER TABLE dbo.' + @tableName + ' add constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault + ' for ' + @columnName)
	end
end

go

