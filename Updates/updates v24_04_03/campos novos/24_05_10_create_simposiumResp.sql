
	   -- drop table simposiumResp
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'simposiumResp'))
BEGIN
	

CREATE TABLE [dbo].[simposiumResp](
	[token]					[varchar](36) NOT NULL  ,
	[status]				[int],
	[message]				[varchar](254),
	[additionalProp1]		[varchar](254) NOT NULL,
	[additionalProp2]		[varchar](254) NOT NULL,
	[additionalProp3]		[varchar](254) NOT NULL,
	[tokenConflicts]		[varchar](36),
	[stampDrugInfo]			[varchar](36),
	[tokenInteraction]		[varchar](36),
	[tokenFoodInteraction]  [varchar](36),
	[tokenPhysioAlert]		[varchar](36),
	[ousrinis]				[varchar](30),
	[ousrdata]				[datetime] NOT NULL,
	[usrinis]				[varchar](30),
	[usrdata]				[datetime] NOT NULL,
	PRIMARY KEY (token)
) ON [PRIMARY]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [IX_simposiumResp_token] ON [dbo].[simposiumResp]
(
	[token] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

SET ANSI_PADDING ON


ALTER TABLE [dbo].[simposiumResp] ADD  CONSTRAINT [DF_simposiumResp_status]  DEFAULT ('0') FOR [status]

ALTER TABLE [dbo].[simposiumResp] ADD  CONSTRAINT [DF_simposiumResp_message]  DEFAULT ('') FOR [message]

ALTER TABLE [dbo].[simposiumResp] ADD  CONSTRAINT [DF_simposiumResp_additionalProp1]  DEFAULT ('') FOR [additionalProp1]

ALTER TABLE [dbo].[simposiumResp] ADD  CONSTRAINT [DF_simposiumResp_additionalProp2]  DEFAULT ('') FOR [additionalProp2]

ALTER TABLE [dbo].[simposiumResp] ADD  CONSTRAINT [DF_simposiumResp_additionalProp3]  DEFAULT ('') FOR [additionalProp3]

ALTER TABLE [dbo].[simposiumResp] ADD  CONSTRAINT [DF_simposiumResp_tokenConflicts]  DEFAULT ('') FOR [tokenConflicts]

ALTER TABLE [dbo].[simposiumResp] ADD  CONSTRAINT [DF_simposiumResp_stampDrugInfo]  DEFAULT ('') FOR [stampDrugInfo]

ALTER TABLE [dbo].[simposiumResp] ADD  CONSTRAINT [DF_simposiumResp_tokenInteraction]  DEFAULT ('') FOR [tokenInteraction]

ALTER TABLE [dbo].[simposiumResp] ADD  CONSTRAINT [DF_simposiumResp_tokenFoodInteraction]  DEFAULT ('') FOR [tokenFoodInteraction]

ALTER TABLE [dbo].[simposiumResp] ADD  CONSTRAINT [DF_simposiumResp_tokenPhysioAlert]  DEFAULT ('') FOR [tokenPhysioAlert]

ALTER TABLE [dbo].[simposiumResp] ADD  CONSTRAINT [DF_simposiumResp_ousrinis]  DEFAULT ('') FOR [ousrinis]

ALTER TABLE [dbo].[simposiumResp] ADD  CONSTRAINT [DF_simposiumResp_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

ALTER TABLE [dbo].[simposiumResp] ADD  CONSTRAINT [DF_simposiumResp_usrinis]  DEFAULT ('') FOR [usrinis]

ALTER TABLE [dbo].[simposiumResp] ADD  CONSTRAINT [DF_simposiumResp_usrdata]  DEFAULT (getdate()) FOR [usrdata]

END





	   -- drop table simposiumRespConflits
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'simposiumRespConflits'))
BEGIN
	

CREATE TABLE [dbo].[simposiumRespConflits](

	[stamp]					[varchar](36) NOT NULL  ,
	[token]					[varchar](36) NOT NULL  ,
	[id]					[varchar](254),
	[name]					[varchar](254),
	[moleculeName]			[varchar](254),
	[drugClass]				[varchar](200),
	[ousrinis]				[varchar](30),
	[ousrdata]				[datetime] NOT NULL,
	[usrinis]				[varchar](30),
	[usrdata]				[datetime] NOT NULL,
	PRIMARY KEY (stamp)
) ON [PRIMARY]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [IX_simposiumRespConflits_stamp] ON [dbo].[simposiumRespConflits]
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

SET ANSI_PADDING ON


ALTER TABLE [dbo].[simposiumRespConflits] ADD  CONSTRAINT [DF_simposiumRespConflits_token]  DEFAULT ('') FOR [token]

ALTER TABLE [dbo].[simposiumRespConflits] ADD  CONSTRAINT [DF_simposiumRespConflits_id]  DEFAULT ('') FOR [id]

ALTER TABLE [dbo].[simposiumRespConflits] ADD  CONSTRAINT [DF_simposiumRespConflits_name]  DEFAULT ('') FOR [name]

ALTER TABLE [dbo].[simposiumRespConflits] ADD  CONSTRAINT [DF_simposiumRespConflits_moleculeName]  DEFAULT ('') FOR [moleculeName]

ALTER TABLE [dbo].[simposiumRespConflits] ADD  CONSTRAINT [DF_simposiumRespConflits_drugClass]  DEFAULT ('') FOR [drugClass]

ALTER TABLE [dbo].[simposiumRespConflits] ADD  CONSTRAINT [DF_simposiumRespConflits_ousrinis]  DEFAULT ('') FOR [ousrinis]

ALTER TABLE [dbo].[simposiumRespConflits] ADD  CONSTRAINT [DF_simposiumRespConflits_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

ALTER TABLE [dbo].[simposiumRespConflits] ADD  CONSTRAINT [DF_simposiumRespConflits_usrinis]  DEFAULT ('') FOR [usrinis]

ALTER TABLE [dbo].[simposiumRespConflits] ADD  CONSTRAINT [DF_simposiumRespConflits_usrdata]  DEFAULT (getdate()) FOR [usrdata]

END





	   -- drop table simposiumRespInteractions
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'simposiumRespInteractions'))
BEGIN
	

CREATE TABLE [dbo].[simposiumRespInteractions](

	[stamp]					[varchar](36) NOT NULL  ,
	[token]					[varchar](36) NOT NULL  ,
	[tokenItems]			[varchar](36),
	[tokenBibliography]		[varchar](36),
	[severityDescr]			[varchar](max),
	[severityAdvice]		[varchar](max),
	[advice]				[varchar](max),
	[infoDescr]				[varchar](max),
	[infoAdvice]			[varchar](max),
	[level]					[int],
	[ousrinis]				[varchar](30),
	[ousrdata]				[datetime] NOT NULL,
	[usrinis]				[varchar](30),
	[usrdata]				[datetime] NOT NULL,
	PRIMARY KEY (stamp)
) ON [PRIMARY]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [IX_simposiumRespInteractions_stamp] ON [dbo].[simposiumRespInteractions]
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

SET ANSI_PADDING ON


ALTER TABLE [dbo].[simposiumRespInteractions] ADD  CONSTRAINT [DF_simposiumRespInteractions_token]  DEFAULT ('') FOR [token]

ALTER TABLE [dbo].[simposiumRespInteractions] ADD  CONSTRAINT [DF_simposiumRespInteractions_tokenItems]  DEFAULT ('') FOR [tokenItems]

ALTER TABLE [dbo].[simposiumRespInteractions] ADD  CONSTRAINT [DF_simposiumRespInteractions_tokenBibliography]  DEFAULT ('') FOR [tokenBibliography]

ALTER TABLE [dbo].[simposiumRespInteractions] ADD  CONSTRAINT [DF_simposiumRespInteractions_severityDescr]  DEFAULT ('') FOR [severityDescr]

ALTER TABLE [dbo].[simposiumRespInteractions] ADD  CONSTRAINT [DF_simposiumRespInteractions_severityAdvice]  DEFAULT ('') FOR [severityAdvice]

ALTER TABLE [dbo].[simposiumRespInteractions] ADD  CONSTRAINT [DF_simposiumRespInteractions_advice]  DEFAULT ('') FOR [advice]

ALTER TABLE [dbo].[simposiumRespInteractions] ADD  CONSTRAINT [DF_simposiumRespInteractions_infoDescr]  DEFAULT ('') FOR [infoDescr]

ALTER TABLE [dbo].[simposiumRespInteractions] ADD  CONSTRAINT [DF_simposiumRespInteractions_infoAdvice]  DEFAULT ('') FOR [infoAdvice]

ALTER TABLE [dbo].[simposiumRespInteractions] ADD  CONSTRAINT [DF_simposiumRespInteractions_level]  DEFAULT ('') FOR [level]

ALTER TABLE [dbo].[simposiumRespInteractions] ADD  CONSTRAINT [DF_simposiumRespInteractions_ousrinis]  DEFAULT ('') FOR [ousrinis]

ALTER TABLE [dbo].[simposiumRespInteractions] ADD  CONSTRAINT [DF_simposiumRespInteractions_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

ALTER TABLE [dbo].[simposiumRespInteractions] ADD  CONSTRAINT [DF_simposiumRespInteractions_usrinis]  DEFAULT ('') FOR [usrinis]

ALTER TABLE [dbo].[simposiumRespInteractions] ADD  CONSTRAINT [DF_simposiumRespInteractions_usrdata]  DEFAULT (getdate()) FOR [usrdata]

END



	   -- drop table simposiumRespInteractionsItems
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'simposiumRespInteractionsItems'))
BEGIN
	

CREATE TABLE [dbo].[simposiumRespInteractionsItems](

	[stamp]					[varchar](36) NOT NULL  ,
	[token]					[varchar](36) NOT NULL  ,
	[id]					[int],
	[name]					[varchar](254),
	[moleculeName]			[varchar](254),
	[drugClass]				[varchar](254),
	[ousrinis]				[varchar](30),
	[ousrdata]				[datetime] NOT NULL,
	[usrinis]				[varchar](30),
	[usrdata]				[datetime] NOT NULL,
	PRIMARY KEY (stamp)
) ON [PRIMARY]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [IX_simposiumRespInteractionsItems_stamp] ON [dbo].[simposiumRespInteractionsItems]
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

SET ANSI_PADDING ON


ALTER TABLE [dbo].[simposiumRespInteractionsItems] ADD  CONSTRAINT [DF_simposiumRespInteractionsItems_token]  DEFAULT ('') FOR [token]

ALTER TABLE [dbo].[simposiumRespInteractionsItems] ADD  CONSTRAINT [DF_simposiumRespInteractionsItems_id]  DEFAULT ('0') FOR [id]

ALTER TABLE [dbo].[simposiumRespInteractionsItems] ADD  CONSTRAINT [DF_simposiumRespInteractionsItems_name]  DEFAULT ('') FOR [name]

ALTER TABLE [dbo].[simposiumRespInteractionsItems] ADD  CONSTRAINT [DF_simposiumRespInteractionsItems_moleculeName]  DEFAULT ('') FOR [moleculeName]

ALTER TABLE [dbo].[simposiumRespInteractionsItems] ADD  CONSTRAINT [DF_simposiumRespInteractionsItems_drugClass]  DEFAULT ('') FOR [drugClass]

ALTER TABLE [dbo].[simposiumRespInteractionsItems] ADD  CONSTRAINT [DF_simposiumRespInteractionsItems_ousrinis]  DEFAULT ('') FOR [ousrinis]

ALTER TABLE [dbo].[simposiumRespInteractionsItems] ADD  CONSTRAINT [DF_simposiumRespInteractionsItems_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

ALTER TABLE [dbo].[simposiumRespInteractionsItems] ADD  CONSTRAINT [DF_simposiumRespInteractionsItems_usrinis]  DEFAULT ('') FOR [usrinis]

ALTER TABLE [dbo].[simposiumRespInteractionsItems] ADD  CONSTRAINT [DF_simposiumRespInteractionsItems_usrdata]  DEFAULT (getdate()) FOR [usrdata]

END




	   -- drop table simposiumRespBibliography
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'simposiumRespBibliography'))
BEGIN
	

CREATE TABLE [dbo].[simposiumRespBibliography](

	[stamp]					[varchar](36) NOT NULL  ,
	[token]					[varchar](36) NOT NULL  ,
	[title]					[varchar](254),
	[author]				[varchar](254),
	[accessDate]			datetime,
	[type]					[varchar](254),
	[language]				[varchar](254),
	[url]					[varchar](254),
	[edition]				[varchar](254),
	[publisher]				[varchar](254),
	[volume]				[varchar](254),
	[book]					[varchar](254),
	[journal]				[varchar](254),
	[number]				[varchar](254),
	[institution]			[varchar](254),
	[ousrinis]				[varchar](30),
	[ousrdata]				[datetime] NOT NULL,
	[usrinis]				[varchar](30),
	[usrdata]				[datetime] NOT NULL,
	PRIMARY KEY (stamp)
) ON [PRIMARY]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [IX_simposiumRespBibliography_stamp] ON [dbo].[simposiumRespBibliography]
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

SET ANSI_PADDING ON


ALTER TABLE [dbo].[simposiumRespBibliography] ADD  CONSTRAINT [DF_simposiumRespBibliography_token]  DEFAULT ('') FOR [token]

ALTER TABLE [dbo].[simposiumRespBibliography] ADD  CONSTRAINT [DF_simposiumRespBibliography_title]  DEFAULT ('') FOR [title]

ALTER TABLE [dbo].[simposiumRespBibliography] ADD  CONSTRAINT [DF_simposiumRespBibliography_author]  DEFAULT ('') FOR [author]

ALTER TABLE [dbo].[simposiumRespBibliography] ADD  CONSTRAINT [DF_simposiumRespBibliography_accessDate]  DEFAULT ('') FOR [accessDate]

ALTER TABLE [dbo].[simposiumRespBibliography] ADD  CONSTRAINT [DF_simposiumRespBibliography_type]  DEFAULT ('') FOR [type]

ALTER TABLE [dbo].[simposiumRespBibliography] ADD  CONSTRAINT [DF_simposiumRespBibliography_language]  DEFAULT ('') FOR [language]

ALTER TABLE [dbo].[simposiumRespBibliography] ADD  CONSTRAINT [DF_simposiumRespBibliography_url]  DEFAULT ('') FOR [url]

ALTER TABLE [dbo].[simposiumRespBibliography] ADD  CONSTRAINT [DF_simposiumRespBibliography_edition]  DEFAULT ('') FOR [edition]

ALTER TABLE [dbo].[simposiumRespBibliography] ADD  CONSTRAINT [DF_simposiumRespBibliography_publisher]  DEFAULT ('') FOR [publisher]

ALTER TABLE [dbo].[simposiumRespBibliography] ADD  CONSTRAINT [DF_simposiumRespBibliography_volume]  DEFAULT ('') FOR [volume]

ALTER TABLE [dbo].[simposiumRespBibliography] ADD  CONSTRAINT [DF_simposiumRespBibliography_book]  DEFAULT ('') FOR [book]

ALTER TABLE [dbo].[simposiumRespBibliography] ADD  CONSTRAINT [DF_simposiumRespBibliography_journal]  DEFAULT ('') FOR [journal]

ALTER TABLE [dbo].[simposiumRespBibliography] ADD  CONSTRAINT [DF_simposiumRespBibliography_number]  DEFAULT ('') FOR [number]

ALTER TABLE [dbo].[simposiumRespBibliography] ADD  CONSTRAINT [DF_simposiumRespBibliography_institution]  DEFAULT ('') FOR [institution]

ALTER TABLE [dbo].[simposiumRespBibliography] ADD  CONSTRAINT [DF_simposiumRespBibliography_ousrinis]  DEFAULT ('') FOR [ousrinis]

ALTER TABLE [dbo].[simposiumRespBibliography] ADD  CONSTRAINT [DF_simposiumRespBibliography_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

ALTER TABLE [dbo].[simposiumRespBibliography] ADD  CONSTRAINT [DF_simposiumRespBibliography_usrinis]  DEFAULT ('') FOR [usrinis]

ALTER TABLE [dbo].[simposiumRespBibliography] ADD  CONSTRAINT [DF_simposiumRespBibliography_usrdata]  DEFAULT (getdate()) FOR [usrdata]

END




	   -- drop table simposiumRespFoodInteraction
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'simposiumRespFoodInteraction'))
BEGIN
	

CREATE TABLE [dbo].[simposiumRespFoodInteraction](

	[stamp]					[varchar](36) NOT NULL  ,
	[token]					[varchar](36) NOT NULL  ,
	[id]					[int],
	[descr]					[varchar](max),
	[explanation]			[varchar](max),
	[severity]				[varchar](254),
	[foodType]				[varchar](254),
	[level]					[int],
	[ousrinis]				[varchar](30),
	[ousrdata]				[datetime] NOT NULL,
	[usrinis]				[varchar](30),
	[usrdata]				[datetime] NOT NULL,
	PRIMARY KEY (stamp)
) ON [PRIMARY]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [IX_simposiumRespFoodInteraction_stamp] ON [dbo].[simposiumRespFoodInteraction]
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

SET ANSI_PADDING ON


ALTER TABLE [dbo].[simposiumRespFoodInteraction] ADD  CONSTRAINT [DF_simposiumRespFoodInteraction_token]  DEFAULT ('') FOR [token]

ALTER TABLE [dbo].[simposiumRespFoodInteraction] ADD  CONSTRAINT [DF_simposiumRespFoodInteraction_id]  DEFAULT ('0') FOR [id]

ALTER TABLE [dbo].[simposiumRespFoodInteraction] ADD  CONSTRAINT [DF_simposiumRespFoodInteraction_descr]  DEFAULT ('') FOR [descr]

ALTER TABLE [dbo].[simposiumRespFoodInteraction] ADD  CONSTRAINT [DF_simposiumRespFoodInteraction_explanation]  DEFAULT ('') FOR [explanation]

ALTER TABLE [dbo].[simposiumRespFoodInteraction] ADD  CONSTRAINT [DF_simposiumRespFoodInteraction_severity]  DEFAULT ('') FOR [severity]

ALTER TABLE [dbo].[simposiumRespFoodInteraction] ADD  CONSTRAINT [DF_simposiumRespFoodInteraction_foodType]  DEFAULT ('') FOR [foodType]

ALTER TABLE [dbo].[simposiumRespFoodInteraction] ADD  CONSTRAINT [DF_simposiumRespFoodInteraction_level]  DEFAULT ('0') FOR [level]

ALTER TABLE [dbo].[simposiumRespFoodInteraction] ADD  CONSTRAINT [DF_simposiumRespFoodInteraction_ousrinis]  DEFAULT ('') FOR [ousrinis]

ALTER TABLE [dbo].[simposiumRespFoodInteraction] ADD  CONSTRAINT [DF_simposiumRespFoodInteraction_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

ALTER TABLE [dbo].[simposiumRespFoodInteraction] ADD  CONSTRAINT [DF_simposiumRespFoodInteraction_usrinis]  DEFAULT ('') FOR [usrinis]

ALTER TABLE [dbo].[simposiumRespFoodInteraction] ADD  CONSTRAINT [DF_simposiumRespFoodInteraction_usrdata]  DEFAULT (getdate()) FOR [usrdata]

END


	   -- drop table simposiumRespPhysioAlerts
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'simposiumRespPhysioAlerts'))
BEGIN
	

CREATE TABLE [dbo].[simposiumRespPhysioAlerts](

	[stamp]					[varchar](36) NOT NULL  ,
	[token]					[varchar](36) NOT NULL  ,
	[id]					[int],
	[typeId]				[int],
	[type]					[varchar](254),
	[level]					[int],
	[route]					[varchar](254),
	[descr]					[varchar](max),
	[risk]					[varchar](max),
	[advice]				[varchar](max),
	[ousrinis]				[varchar](30),
	[ousrdata]				[datetime] NOT NULL,
	[usrinis]				[varchar](30),
	[usrdata]				[datetime] NOT NULL,
	PRIMARY KEY (stamp)
) ON [PRIMARY]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [IX_simposiumRespPhysioAlerts_stamp] ON [dbo].[simposiumRespPhysioAlerts]
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

SET ANSI_PADDING ON


ALTER TABLE [dbo].[simposiumRespPhysioAlerts] ADD  CONSTRAINT [DF_simposiumRespPhysioAlerts_token]  DEFAULT ('') FOR [token]

ALTER TABLE [dbo].[simposiumRespPhysioAlerts] ADD  CONSTRAINT [DF_simposiumRespPhysioAlerts_id]  DEFAULT ('0') FOR [id]

ALTER TABLE [dbo].[simposiumRespPhysioAlerts] ADD  CONSTRAINT [DF_simposiumRespPhysioAlerts_typeId]  DEFAULT ('0') FOR [typeId]

ALTER TABLE [dbo].[simposiumRespPhysioAlerts] ADD  CONSTRAINT [DF_simposiumRespPhysioAlerts_type]  DEFAULT ('') FOR [type]

ALTER TABLE [dbo].[simposiumRespPhysioAlerts] ADD  CONSTRAINT [DF_simposiumRespPhysioAlerts_level]  DEFAULT ('0') FOR [level]

ALTER TABLE [dbo].[simposiumRespPhysioAlerts] ADD  CONSTRAINT [DF_simposiumRespPhysioAlerts_route]  DEFAULT ('') FOR [route]

ALTER TABLE [dbo].[simposiumRespPhysioAlerts] ADD  CONSTRAINT [DF_simposiumRespPhysioAlerts_descr]  DEFAULT ('') FOR [descr]

ALTER TABLE [dbo].[simposiumRespPhysioAlerts] ADD  CONSTRAINT [DF_simposiumRespPhysioAlerts_advice]  DEFAULT ('') FOR [advice]

ALTER TABLE [dbo].[simposiumRespPhysioAlerts] ADD  CONSTRAINT [DF_simposiumRespPhysioAlerts_risk]  DEFAULT ('') FOR [risk]

ALTER TABLE [dbo].[simposiumRespPhysioAlerts] ADD  CONSTRAINT [DF_simposiumRespPhysioAlerts_ousrinis]  DEFAULT ('') FOR [ousrinis]

ALTER TABLE [dbo].[simposiumRespPhysioAlerts] ADD  CONSTRAINT [DF_simposiumRespPhysioAlerts_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

ALTER TABLE [dbo].[simposiumRespPhysioAlerts] ADD  CONSTRAINT [DF_simposiumRespPhysioAlerts_usrinis]  DEFAULT ('') FOR [usrinis]

ALTER TABLE [dbo].[simposiumRespPhysioAlerts] ADD  CONSTRAINT [DF_simposiumRespPhysioAlerts_usrdata]  DEFAULT (getdate()) FOR [usrdata]

END

	   -- drop table simposiumRespDrugInfo
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'simposiumRespDrugInfo'))
BEGIN
	

CREATE TABLE [dbo].[simposiumRespDrugInfo](

	[stamp]							[varchar](36) NOT NULL  ,
	[token]							[varchar](36) NOT NULL  ,
	[message]						[varchar](254),
	[status]						[int],
	[itemId]						[int],
	[rank]							[int],
	[packageImageId]				[int],
	[dciId]							[int],
	[descr]							[varchar](254),
	[dciName]						[varchar](254),
	[name]							[varchar](254),
	[dosage]						[varchar](254),
	[canTrissel]					bit,
	[isTestSolution]				bit,
	[isSolution]					bit,
	[isSubstance]					bit,
	[isDrug]						bit,
	[isGeneric]						bit,
	[hasTherapeuticMargin]			bit,
	[needsAdditionalMonitoring]		bit,
	[type]							[varchar](254),
	[packages]						[varchar](254),
	[hasAdditionalInfo]				bit,
	[hasClientMonography]			bit,
	[hasDCIMonography]				bit,
	[monographyId]					[int],
	[monography]					[varchar](254),
	[tokenpackageIds]				[VARCHAR](36),
	[tokenAtc]						[VARCHAR](36),
	[tokenCtf]						[VARCHAR](36),
	[tokenRoutes]					[VARCHAR](36),
	[tokenClassification]			[VARCHAR](36),
	[tokenExemptionClass]			[VARCHAR](36),
	[tokenAdditionalInfo]			[VARCHAR](36),
	[tokenGroups]					[VARCHAR](36),
	[dosageInfoId]					[int],
	[dosageInfoRoute]				[varchar](254),
	[dosageInfoGuideLine]			[varchar](254),
	[dosageInfoElder]				[varchar](254),
	[dosageInfoAdult]				[varchar](254),
	[dosageInfoChild]				[varchar](254),
	[dosageInfoLiverFailure]		[varchar](254),
	[dosageInfoKidneyFailure]		[varchar](254),
	[dosageInfoFractionalDose]		[varchar](254),
	[dosageInfoBibliography]		[varchar](254),
	[formDescr]						[varchar](max),
	[formShortName]					[varchar](254),
	[formShortNameCHNM]				[varchar](254),
	[narcoticClassDescr]			[varchar](max),
	[narcoticClassShortName]		[varchar](254),
	[labId]							numeric(10,0),
	[labShortName]					[varchar](254),
	[anestheticClassId]				numeric(10,0),
	[anestheticClassDescr]			[varchar](max),
	[AgentId]						varchar(254),
	[AgentName]						[varchar](254),
	[AgentEmail]					[varchar](254),
	[AgentPhone]					[varchar](254),
	[AgentFax]						[varchar](254),
	[AgentSupervisionEmail]			[varchar](254),
	[AgentSupervisionPhone]			[varchar](254),
	[AgentSupervisionFax]			[varchar](254),
	[AgentWebSite]					[varchar](254),
	[ousrinis]						[varchar](30),
	[ousrdata]						[datetime] NOT NULL,
	[usrinis]						[varchar](30),
	[usrdata]						[datetime] NOT NULL,
	PRIMARY KEY (stamp)
) ON [PRIMARY]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [IX_simposiumRespDrugInfo_stamp] ON [dbo].[simposiumRespDrugInfo]
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

SET ANSI_PADDING ON



    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_token] DEFAULT ('') FOR [token];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_message] DEFAULT ('') FOR [message];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_status] DEFAULT (0) FOR [status];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_descr] DEFAULT ('') FOR [descr];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_dciName] DEFAULT ('') FOR [dciName];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_name] DEFAULT ('') FOR [name];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_dosage] DEFAULT ('') FOR [dosage];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_type] DEFAULT ('') FOR [type];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_packages] DEFAULT ('') FOR [packages];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_monography] DEFAULT ('') FOR [monography];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_tokenpackageIds] DEFAULT ('') FOR [tokenpackageIds];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_tokenAtc] DEFAULT ('') FOR [tokenAtc];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_tokenCtf] DEFAULT ('') FOR [tokenCtf];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_tokenRoutes] DEFAULT ('') FOR [tokenRoutes];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_tokenClassification] DEFAULT ('') FOR [tokenClassification];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_tokenExemptionClass] DEFAULT ('') FOR [tokenExemptionClass];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_tokenAdditionalInfo] DEFAULT ('') FOR [tokenAdditionalInfo];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_tokenGroups] DEFAULT ('') FOR [tokenGroups];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_dosageInfoId] DEFAULT (0) FOR [dosageInfoId];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_dosageInfoRoute] DEFAULT ('') FOR [dosageInfoRoute];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_dosageInfoGuideLine] DEFAULT ('') FOR [dosageInfoGuideLine];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_dosageInfoElder] DEFAULT ('') FOR [dosageInfoElder];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_dosageInfoAdult] DEFAULT ('') FOR [dosageInfoAdult];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_dosageInfoChild] DEFAULT ('') FOR [dosageInfoChild];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_dosageInfoLiverFailure] DEFAULT ('') FOR [dosageInfoLiverFailure];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_dosageInfoKidneyFailure] DEFAULT ('') FOR [dosageInfoKidneyFailure];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_dosageInfoFractionalDose] DEFAULT ('') FOR [dosageInfoFractionalDose];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_dosageInfoBibliography] DEFAULT ('') FOR [dosageInfoBibliography];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_formDescr] DEFAULT ('') FOR [formDescr];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_formShortName] DEFAULT ('') FOR [formShortName];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_formShortNameCHNM] DEFAULT ('') FOR [formShortNameCHNM];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_narcoticClassDescr] DEFAULT ('') FOR [narcoticClassDescr];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_narcoticClassShortName] DEFAULT ('') FOR [narcoticClassShortName];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_labId] DEFAULT (0) FOR [labId];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_labShortName] DEFAULT ('') FOR [labShortName];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_anestheticClassId] DEFAULT (0) FOR [anestheticClassId];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_anestheticClassDescr] DEFAULT ('') FOR [anestheticClassDescr];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_AgentId] DEFAULT ('') FOR [AgentId];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_AgentName] DEFAULT ('') FOR [AgentName];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_AgentEmail] DEFAULT ('') FOR [AgentEmail];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_AgentPhone] DEFAULT ('') FOR [AgentPhone];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_AgentFax] DEFAULT ('') FOR [AgentFax];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_AgentSupervisionEmail] DEFAULT ('') FOR [AgentSupervisionEmail];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_AgentSupervisionPhone] DEFAULT ('') FOR [AgentSupervisionPhone];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_AgentSupervisionFax] DEFAULT ('') FOR [AgentSupervisionFax];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_AgentWebSite] DEFAULT ('') FOR [AgentWebSite];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_ousrinis] DEFAULT ('') FOR [ousrinis];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_ousrdata] DEFAULT (getdate()) FOR [ousrdata];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_usrinis] DEFAULT ('') FOR [usrinis];

    ALTER TABLE [dbo].[simposiumRespDrugInfo] ADD CONSTRAINT [DF_simposiumRespDrugInfo_usrdata] DEFAULT (getdate()) FOR [usrdata];



END


	   -- drop table simposiumRespPackageIds
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'simposiumRespPackageIds'))
BEGIN
	

CREATE TABLE [dbo].[simposiumRespPackageIds](

	[stamp]					[varchar](36) NOT NULL  ,
	[token]					[varchar](36) NOT NULL  ,
	[id]					numeric(10,0),
	[ousrinis]				[varchar](30),
	[ousrdata]				[datetime] NOT NULL,
	[usrinis]				[varchar](30),
	[usrdata]				[datetime] NOT NULL,
	PRIMARY KEY (stamp)
) ON [PRIMARY]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [IX_simposiumRespPackageIds_stamp] ON [dbo].[simposiumRespPackageIds]
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

SET ANSI_PADDING ON


ALTER TABLE [dbo].[simposiumRespPackageIds] ADD  CONSTRAINT [DF_simposiumRespPackageIds_token]  DEFAULT ('') FOR [token]

ALTER TABLE [dbo].[simposiumRespPackageIds] ADD  CONSTRAINT [DF_simposiumRespPackageIds_id]  DEFAULT ('0') FOR [id]

ALTER TABLE [dbo].[simposiumRespPackageIds] ADD  CONSTRAINT [DF_simposiumRespPackageIds_ousrinis]  DEFAULT ('') FOR [ousrinis]

ALTER TABLE [dbo].[simposiumRespPackageIds] ADD  CONSTRAINT [DF_simposiumRespPackageIds_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

ALTER TABLE [dbo].[simposiumRespPackageIds] ADD  CONSTRAINT [DF_simposiumRespPackageIds_usrinis]  DEFAULT ('') FOR [usrinis]

ALTER TABLE [dbo].[simposiumRespPackageIds] ADD  CONSTRAINT [DF_simposiumRespPackageIds_usrdata]  DEFAULT (getdate()) FOR [usrdata]

END

	   -- drop table simposiumRespAtc
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'simposiumRespAtc'))
BEGIN
	

CREATE TABLE [dbo].[simposiumRespAtc](

	[stamp]					[varchar](36) NOT NULL  ,
	[token]					[varchar](36) NOT NULL  ,
	[code]					[varchar](254),
	[descr]					[varchar](max),
	[ousrinis]				[varchar](30),
	[ousrdata]				[datetime] NOT NULL,
	[usrinis]				[varchar](30),
	[usrdata]				[datetime] NOT NULL,
	PRIMARY KEY (stamp)
) ON [PRIMARY]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [IX_simposiumRespAtc_stamp] ON [dbo].[simposiumRespAtc]
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

SET ANSI_PADDING ON


ALTER TABLE [dbo].[simposiumRespAtc] ADD  CONSTRAINT [DF_simposiumRespAtc_token]  DEFAULT ('') FOR [token]

ALTER TABLE [dbo].[simposiumRespAtc] ADD  CONSTRAINT [DF_simposiumRespAtc_code]  DEFAULT ('') FOR [code]

ALTER TABLE [dbo].[simposiumRespAtc] ADD  CONSTRAINT [DF_simposiumRespAtc_descr]  DEFAULT ('') FOR [descr]

ALTER TABLE [dbo].[simposiumRespAtc] ADD  CONSTRAINT [DF_simposiumRespAtc_ousrinis]  DEFAULT ('') FOR [ousrinis]

ALTER TABLE [dbo].[simposiumRespAtc] ADD  CONSTRAINT [DF_simposiumRespAtc_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

ALTER TABLE [dbo].[simposiumRespAtc] ADD  CONSTRAINT [DF_simposiumRespAtc_usrinis]  DEFAULT ('') FOR [usrinis]

ALTER TABLE [dbo].[simposiumRespAtc] ADD  CONSTRAINT [DF_simposiumRespAtc_usrdata]  DEFAULT (getdate()) FOR [usrdata]

END

	   -- drop table simposiumRespCtf
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'simposiumRespCtf'))
BEGIN
	

CREATE TABLE [dbo].[simposiumRespCtf](

	[stamp]					[varchar](36) NOT NULL  ,
	[token]					[varchar](36) NOT NULL  ,
	[code]					[varchar](254),
	[descr]					[varchar](max),
	[ousrinis]				[varchar](30),
	[ousrdata]				[datetime] NOT NULL,
	[usrinis]				[varchar](30),
	[usrdata]				[datetime] NOT NULL,
	PRIMARY KEY (stamp)
) ON [PRIMARY]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [IX_simposiumRespCtf_stamp] ON [dbo].[simposiumRespCtf]
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

SET ANSI_PADDING ON


ALTER TABLE [dbo].[simposiumRespCtf] ADD  CONSTRAINT [DF_simposiumRespCtf_token]  DEFAULT ('') FOR [token]

ALTER TABLE [dbo].[simposiumRespCtf] ADD  CONSTRAINT [DF_simposiumRespCtf_code]  DEFAULT ('') FOR [code]

ALTER TABLE [dbo].[simposiumRespCtf] ADD  CONSTRAINT [DF_simposiumRespCtf_descr]  DEFAULT ('') FOR [descr]

ALTER TABLE [dbo].[simposiumRespCtf] ADD  CONSTRAINT [DF_simposiumRespCtf_ousrinis]  DEFAULT ('') FOR [ousrinis]

ALTER TABLE [dbo].[simposiumRespCtf] ADD  CONSTRAINT [DF_simposiumRespCtf_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

ALTER TABLE [dbo].[simposiumRespCtf] ADD  CONSTRAINT [DF_simposiumRespCtf_usrinis]  DEFAULT ('') FOR [usrinis]

ALTER TABLE [dbo].[simposiumRespCtf] ADD  CONSTRAINT [DF_simposiumRespCtf_usrdata]  DEFAULT (getdate()) FOR [usrdata]

END


	   -- drop table simposiumRespRoutes
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'simposiumRespRoutes'))
BEGIN
	

CREATE TABLE [dbo].[simposiumRespRoutes](

	[stamp]					[varchar](36) NOT NULL  ,
	[token]					[varchar](36) NOT NULL  ,
	[shortName]				[varchar](254),
	[descr]					[varchar](max),
	[ousrinis]				[varchar](30),
	[ousrdata]				[datetime] NOT NULL,
	[usrinis]				[varchar](30),
	[usrdata]				[datetime] NOT NULL,
	PRIMARY KEY (stamp)
) ON [PRIMARY]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [IX_simposiumRespRoutes_stamp] ON [dbo].[simposiumRespRoutes]
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

SET ANSI_PADDING ON


ALTER TABLE [dbo].[simposiumRespRoutes] ADD  CONSTRAINT [DF_simposiumRespRoutes_token]  DEFAULT ('') FOR [token]

ALTER TABLE [dbo].[simposiumRespRoutes] ADD  CONSTRAINT [DF_simposiumRespRoutes_shortName]  DEFAULT ('') FOR [shortName]

ALTER TABLE [dbo].[simposiumRespRoutes] ADD  CONSTRAINT [DF_simposiumRespRoutes_descr]  DEFAULT ('') FOR [descr]

ALTER TABLE [dbo].[simposiumRespRoutes] ADD  CONSTRAINT [DF_simposiumRespRoutes_ousrinis]  DEFAULT ('') FOR [ousrinis]

ALTER TABLE [dbo].[simposiumRespRoutes] ADD  CONSTRAINT [DF_simposiumRespRoutes_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

ALTER TABLE [dbo].[simposiumRespRoutes] ADD  CONSTRAINT [DF_simposiumRespRoutes_usrinis]  DEFAULT ('') FOR [usrinis]

ALTER TABLE [dbo].[simposiumRespRoutes] ADD  CONSTRAINT [DF_simposiumRespRoutes_usrdata]  DEFAULT (getdate()) FOR [usrdata]

END

	   -- drop table simposiumRespClassification
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'simposiumRespClassification'))
BEGIN
	

CREATE TABLE [dbo].[simposiumRespClassification](

	[stamp]					[varchar](36) NOT NULL  ,
	[token]					[varchar](36) NOT NULL  ,
	[code]					[varchar](254),
	[descr]					[varchar](max),
	[numberOfItems]			NUMERIC(10,0),
	[ousrinis]				[varchar](30),
	[ousrdata]				[datetime] NOT NULL,
	[usrinis]				[varchar](30),
	[usrdata]				[datetime] NOT NULL,
	PRIMARY KEY (stamp)
) ON [PRIMARY]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [IX_simposiumRespClassification_stamp] ON [dbo].[simposiumRespClassification]
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

SET ANSI_PADDING ON


ALTER TABLE [dbo].[simposiumRespClassification] ADD  CONSTRAINT [DF_simposiumRespClassification_token]  DEFAULT ('') FOR [token]

ALTER TABLE [dbo].[simposiumRespClassification] ADD  CONSTRAINT [DF_simposiumRespClassification_code]  DEFAULT ('') FOR [code]

ALTER TABLE [dbo].[simposiumRespClassification] ADD  CONSTRAINT [DF_simposiumRespClassification_descr]  DEFAULT ('') FOR [descr]

ALTER TABLE [dbo].[simposiumRespClassification] ADD  CONSTRAINT [DF_simposiumRespClassification_numberOfItems]  DEFAULT ('0') FOR [numberOfItems]

ALTER TABLE [dbo].[simposiumRespClassification] ADD  CONSTRAINT [DF_simposiumRespClassification_ousrinis]  DEFAULT ('') FOR [ousrinis]

ALTER TABLE [dbo].[simposiumRespClassification] ADD  CONSTRAINT [DF_simposiumRespClassification_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

ALTER TABLE [dbo].[simposiumRespClassification] ADD  CONSTRAINT [DF_simposiumRespClassification_usrinis]  DEFAULT ('') FOR [usrinis]

ALTER TABLE [dbo].[simposiumRespClassification] ADD  CONSTRAINT [DF_simposiumRespClassification_usrdata]  DEFAULT (getdate()) FOR [usrdata]

END


	   -- drop table simposiumRespAdditionalInfo
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'simposiumRespAdditionalInfo'))
BEGIN
	

CREATE TABLE [dbo].[simposiumRespAdditionalInfo](

	[stamp]					[varchar](36) NOT NULL  ,
	[token]					[varchar](36) NOT NULL  ,
	[image]					[varchar](max),
	[descr]					[varchar](max),
	[url]					[varchar](254),
	[ousrinis]				[varchar](30),
	[ousrdata]				[datetime] NOT NULL,
	[usrinis]				[varchar](30),
	[usrdata]				[datetime] NOT NULL,
	PRIMARY KEY (stamp)
) ON [PRIMARY]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [IX_simposiumRespAdditionalInfo_stamp] ON [dbo].[simposiumRespAdditionalInfo]
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

SET ANSI_PADDING ON


ALTER TABLE [dbo].[simposiumRespAdditionalInfo] ADD  CONSTRAINT [DF_simposiumRespAdditionalInfo_token]  DEFAULT ('') FOR [token]

ALTER TABLE [dbo].[simposiumRespAdditionalInfo] ADD  CONSTRAINT [DF_simposiumRespAdditionalInfo_image]  DEFAULT ('') FOR [image]

ALTER TABLE [dbo].[simposiumRespAdditionalInfo] ADD  CONSTRAINT [DF_simposiumRespAdditionalInfo_descr]  DEFAULT ('') FOR [descr]

ALTER TABLE [dbo].[simposiumRespAdditionalInfo] ADD  CONSTRAINT [DF_simposiumRespAdditionalInfo_url]  DEFAULT ('') FOR [numberOfItems]

ALTER TABLE [dbo].[simposiumRespAdditionalInfo] ADD  CONSTRAINT [DF_simposiumRespAdditionalInfo_ousrinis]  DEFAULT ('') FOR [ousrinis]

ALTER TABLE [dbo].[simposiumRespAdditionalInfo] ADD  CONSTRAINT [DF_simposiumRespAdditionalInfo_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

ALTER TABLE [dbo].[simposiumRespAdditionalInfo] ADD  CONSTRAINT [DF_simposiumRespAdditionalInfo_usrinis]  DEFAULT ('') FOR [usrinis]

ALTER TABLE [dbo].[simposiumRespAdditionalInfo] ADD  CONSTRAINT [DF_simposiumRespAdditionalInfo_usrdata]  DEFAULT (getdate()) FOR [usrdata]

END


	   -- drop table simposiumRespGroups
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'simposiumRespGroups'))
BEGIN
	

CREATE TABLE [dbo].[simposiumRespGroups](

	[stamp]					[varchar](36) NOT NULL  ,
	[token]					[varchar](36) NOT NULL  ,
	[id]					NUMERIC(10,0),
	[descr]					[varchar](max),
	[ousrinis]				[varchar](30),
	[ousrdata]				[datetime] NOT NULL,
	[usrinis]				[varchar](30),
	[usrdata]				[datetime] NOT NULL,
	PRIMARY KEY (stamp)
) ON [PRIMARY]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [IX_simposiumRespGroups_stamp] ON [dbo].[simposiumRespGroups]
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

SET ANSI_PADDING ON


ALTER TABLE [dbo].[simposiumRespGroups] ADD  CONSTRAINT [DF_simposiumRespGroups_token]  DEFAULT ('') FOR [token]

ALTER TABLE [dbo].[simposiumRespGroups] ADD  CONSTRAINT [DF_simposiumRespGroups_id]  DEFAULT ('0') FOR [id]

ALTER TABLE [dbo].[simposiumRespGroups] ADD  CONSTRAINT [DF_simposiumRespGroups_descr]  DEFAULT ('') FOR [descr]

ALTER TABLE [dbo].[simposiumRespGroups] ADD  CONSTRAINT [DF_simposiumRespGroups_ousrinis]  DEFAULT ('') FOR [ousrinis]

ALTER TABLE [dbo].[simposiumRespGroups] ADD  CONSTRAINT [DF_simposiumRespGroups_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

ALTER TABLE [dbo].[simposiumRespGroups] ADD  CONSTRAINT [DF_simposiumRespGroups_usrinis]  DEFAULT ('') FOR [usrinis]

ALTER TABLE [dbo].[simposiumRespGroups] ADD  CONSTRAINT [DF_simposiumRespGroups_usrdata]  DEFAULT (getdate()) FOR [usrdata]

END


	   -- drop table simposiumRespExemptionClass
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'simposiumRespExemptionClass'))
BEGIN
	

CREATE TABLE [dbo].[simposiumRespExemptionClass](

	[stamp]					[varchar](36) NOT NULL  ,
	[token]					[varchar](36) NOT NULL  ,
	[descr]					[varchar](max),
	[ousrinis]				[varchar](30),
	[ousrdata]				[datetime] NOT NULL,
	[usrinis]				[varchar](30),
	[usrdata]				[datetime] NOT NULL,
	PRIMARY KEY (stamp)
) ON [PRIMARY]

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [IX_simposiumRespExemptionClass_stamp] ON [dbo].[simposiumRespExemptionClass]
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

SET ANSI_PADDING ON


ALTER TABLE [dbo].[simposiumRespExemptionClass] ADD  CONSTRAINT [DF_simposiumRespExemptionClass_token]  DEFAULT ('') FOR [token]

ALTER TABLE [dbo].[simposiumRespExemptionClass] ADD  CONSTRAINT [DF_simposiumRespExemptionClass_descr]  DEFAULT ('') FOR [descr]

ALTER TABLE [dbo].[simposiumRespExemptionClass] ADD  CONSTRAINT [DF_simposiumRespExemptionClass_ousrinis]  DEFAULT ('') FOR [ousrinis]

ALTER TABLE [dbo].[simposiumRespExemptionClass] ADD  CONSTRAINT [DF_simposiumRespExemptionClass_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

ALTER TABLE [dbo].[simposiumRespExemptionClass] ADD  CONSTRAINT [DF_simposiumRespExemptionClass_usrinis]  DEFAULT ('') FOR [usrinis]

ALTER TABLE [dbo].[simposiumRespExemptionClass] ADD  CONSTRAINT [DF_simposiumRespExemptionClass_usrdata]  DEFAULT (getdate()) FOR [usrdata]

END
