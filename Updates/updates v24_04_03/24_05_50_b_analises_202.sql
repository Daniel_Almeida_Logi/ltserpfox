IF( NOT EXISTS (SELECT * FROM B_analises (NOLOCK) WHERE ordem=202))
	BEGIN
		INSERT INTO [dbo].[B_analises]
			([ordem],[grupo],[subgrupo],[descricao]
			,[report],[stamp],[url],[comentario]
			,[ecra],[stamp_analiseav],[estado]
			,[tabela],[perfil],[filtros],[formatoExp]
			,[comandoFox],[objectivo],[marcada])
		VALUES
			(202,'Stocks','Stocks','Relatório Medicamentos Veterinários'
			,'relatorio_medicamentos_vet','','','Obs.:'
			,'','',1
			,'','Relatório Medicamentos Veterinários ','',''
			,'','',0)
	END


IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=202 and label='Data Início'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 202,[nome],[label],[tipo],[comando],1,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 201 and label='Data Início'  
END

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=202 and label='Data Fim'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 202,[nome],[label],[tipo],[comando],2,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 201 and label='Data Fim'
END

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=202 and label='Loja'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 202,[nome],[label],[tipo],[comando],3,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],0,[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 201 and label='Loja'
END

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=202 and label='Referência'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 202,[nome],[label],[tipo],[comando],4,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],0,[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 197 and label='Referência'
END

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=202 and label='Lote'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 202,[nome],[label],[tipo],[comando],5,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],0,[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 168 and label='Lote'
END
	
IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=202 and label='Tipo Mov'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 202,[nome],[label],[tipo],[comando],6,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],0,[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 68 and label='Tipo Mov'
END
