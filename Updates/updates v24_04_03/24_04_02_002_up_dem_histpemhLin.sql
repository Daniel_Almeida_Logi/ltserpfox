/*
	
	exec up_pemh_histpemhLin  '75c6968e-6c3e-4c15-9efb-94a4e15c3f86'
	exec up_pemh_histpemhLin  ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_pemh_histpemhLin]') IS NOT NULL
	drop procedure dbo.up_pemh_histpemhLin
go

create procedure dbo.up_pemh_histpemhLin
	@Token VARCHAR(40)

AS
BEGIN

	SELECT
		ISNULL(medicamento_cod, '') as medicamento_cod
		,ISNULL(medicamento_cnpem, '') AS medicamento_cnpem
		,CONVERT(VARCHAR(254),isnull(medicamento_descr,'')) AS medicamento_descr
		,CONVERT(VARCHAR,data_caducidade, 112) AS data_caducidade
		,isnull(CHNM,'') as CHNM
		,ISNULL(LEFT(Replace(rtrim(ltrim(posologia)),'<br>',' / '), 254),'') as posologia
		,ISNULL(rtrim(ltrim(posologia)),'') as posologiaComp
	FROM 
		Dispensa_Eletronica_D(nolock)
	WHERE
		Token = @Token
		


END

GO
Grant Execute on dbo.up_pemh_histpemhLin to Public
Grant Control on dbo.up_pemh_histpemhLin to Public
GO