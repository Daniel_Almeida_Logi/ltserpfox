--exec [up_relatorio_SitCli] 'loja 3', '19000101','20250101', 'Situacoes Clinicas'



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[up_relatorio_SitCli]') IS NOT NULL
    DROP PROCEDURE dbo.[up_relatorio_SitCli] 
GO

CREATE PROCEDURE [dbo].[up_relatorio_SitCli]
    @site           VARCHAR(36),
    @dataini        DATETIME = '19000101',
    @datafim        DATETIME = '19000101',
    @questionario   VARCHAR(254)
AS
SET NOCOUNT ON
   
    DECLARE @sortedColumns NVARCHAR(MAX) = ''
    DECLARE @columns NVARCHAR(MAX) = ''
    DECLARE @sql NVARCHAR(MAX) = ''
    DECLARE @columnNames NVARCHAR(MAX) = ''

    -- Criar uma tabela temporária para armazenar as perguntas
    CREATE TABLE #TempPerguntas (
        pergunta VARCHAR(255),
        ordem   INT
    )

    -- Inserir perguntas distintas na tabela temporária
    INSERT INTO #TempPerguntas (pergunta, ordem)
    SELECT DISTINCT 
        pergunta, 
        ordem
    FROM 
        quest_respostas(NOLOCK)
    INNER JOIN 
        quest_pergunta(NOLOCK) ON quest_respostas.quest_perguntaStamp = quest_pergunta.quest_perguntastamp
    INNER JOIN 
        Quest(NOLOCK) ON Quest.QuestStamp = quest_respostas.questStamp
    WHERE 
        (Quest.Descr IN (SELECT items FROM dbo.up_splitToTable(@questionario, ',')) OR @questionario = '')

    CREATE TABLE #TempColumns (
        ColumnPart NVARCHAR(MAX)
    )

    -- Inserir partes da lista de colunas
    INSERT INTO #TempColumns (ColumnPart)
    SELECT QUOTENAME(pergunta)
    FROM #TempPerguntas
    ORDER BY ordem

    -- Concatenar colunas ordenadas
    SELECT @sortedColumns = STUFF((
        SELECT ', ' + ColumnPart
        FROM #TempColumns
        FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, '')

    -- Concatenar nomes das colunas
    SELECT @columnNames = STUFF((
        SELECT ', ''' + pergunta + ''''
        FROM #TempPerguntas
        FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, '')

    -- Verificar se há registros no intervalo especificado
    DECLARE @RecordCount INT
    SELECT @RecordCount = COUNT(*)
    FROM quest_respostas(NOLOCK)
    INNER JOIN Quest(NOLOCK) ON Quest.QuestStamp = quest_respostas.questStamp
    WHERE
        quest_respostas.site = @site
        AND CONVERT(VARCHAR, quest_respostas.ousrdata, 23) BETWEEN CONVERT(VARCHAR, @dataini, 23) AND CONVERT(VARCHAR, @datafim, 23)
        AND (Quest.Descr IN (SELECT items FROM dbo.up_splitToTable(@questionario, ',')) OR @questionario = '')

    -- Construir o cabeçalho da consulta
    SET @sql = '
    SELECT ''CodFarm'',''Data'',''CNP'',''Ficha'', ''Sexo'', ''Faixa_etaria'' ' + @columnNames

    -- Se não houver registros, retorne apenas o cabeçalho
    IF @RecordCount = 0
    BEGIN
        PRINT @sql
        EXEC(@sql)
        RETURN
    END

    -- Consulta dinâmica com dados
    SET @sql = @sql + '
    UNION ALL
    SELECT DISTINCT
         codfarm, CONVERT(VARCHAR(10), quest_respostas.ousrdata, 103)  +'' ''+ CONVERT(VARCHAR(8), quest_respostas.ousrdata, 108) AS ousrdata,
         quest_respostas.ref, IIF(no = 200, ''Não'',''Sim'') AS Ficha, sexo, faixaEtaria ,' + @sortedColumns + '
    FROM (
        SELECT 
            quest_respostas.ousrdata,
            quest_pergunta.pergunta, 
            ISNULL(quest_respostas.resposta, '''') AS resposta,
            quest_respostas.ref,
            empresa.codfarm,
            quest_respostas.no,
            quest_respostas.sexo,
            quest_respostas.faixaetaria
        FROM
            quest_respostas(NOLOCK)
        LEFT JOIN fi(NOLOCK) ON fi.fistamp = quest_respostas.fistamp
        LEFT JOIN ft(NOLOCK) ON ft.ftstamp = fi.ftstamp
        INNER JOIN empresa(NOLOCK) ON empresa.site = quest_respostas.site
        LEFT JOIN quest_pergunta(NOLOCK) ON quest_pergunta.quest_perguntastamp = quest_respostas.quest_perguntaStamp
        INNER JOIN Quest(NOLOCK) ON Quest.QuestStamp = quest_respostas.questStamp
        WHERE
            quest_respostas.site = ''' + @site + '''
            AND CONVERT(VARCHAR, quest_respostas.ousrdata, 23) BETWEEN ''' + CONVERT(VARCHAR, @dataini, 23) + ''' AND ''' + CONVERT(VARCHAR, @datafim, 23) + '''
AND (Quest.Descr IN (SELECT items FROM dbo.up_splitToTable(''' + @questionario + ''', '','')) OR ''' + @questionario + ''' = '''')
    ) AS Source
    PIVOT (
        MAX(resposta)
        FOR pergunta IN (' + @sortedColumns + ')
    ) AS PivotTable'

    PRINT @sql
    EXEC(@sql)

GO
GRANT EXECUTE ON dbo.[up_relatorio_SitCli] TO Public
GRANT CONTROL ON dbo.[up_relatorio_SitCli] TO Public
GO
