

INSERT INTO [dbo].[wklcols]
           ([wklcolsstamp]
           ,[wkfield]
           ,[wktitle]
           ,[wkstamp]
           ,[wkdocsstamp]
           ,[ousrinis]
           ,[ousrdata]
           ,[ousrhora]
           ,[usrinis]
           ,[usrdata]
           ,[usrhora]
           ,[wkreadonly]
           ,[u_largura]
           ,[u_mascara]
           ,[u_ordem])
SELECT  LEFT (NEWID(),25)
           ,'fi.lote'
           ,'Lote'
           ,wk.wkstamp
           ,''
           ,'ADM'
           ,GETDATE()
           ,GETDATE()
           ,'ADM'
           ,GETDATE()
           ,GETDATE()
           ,0
           ,70
           ,''
           ,(select isnull(MAX(u_ordem) + 1,99) from wklcols(nolock) x where x.wkstamp = wk.wkstamp)  
FROM td (nolock)
INNER JOIN wk (nolock) ON wk.wkdoc = td.ndoc
WHERE not EXISTS (
    SELECT 1
    FROM wklcols (nolock)
    WHERE wklcols.wkstamp = wk.wkstamp 
      AND wklcols.wkfield = 'fi.lote'
)
and td.lancasl = 1


update wklcols 
set wkreadonly = 0
where  wkfield = 'fi.lote'