SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

-- Se a procedure já existir, remove
IF OBJECT_ID('[dbo].[up_save_simposiumRespInteractionsItems]') IS NOT NULL
    DROP PROCEDURE dbo.[up_save_simposiumRespInteractionsItems]
GO

-- Cria a procedure
CREATE PROCEDURE dbo.[up_save_simposiumRespInteractionsItems]
    @stamp         AS VARCHAR(36),
    @token         AS VARCHAR(36),
    @id            AS INT,
    @name          AS VARCHAR(254),
    @moleculeName  AS VARCHAR(254),
    @drugClass     AS VARCHAR(254),
    @ousrinis      AS VARCHAR(30),
    @usrinis       AS VARCHAR(30)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON;

-- Verifica se o registro já existe na tabela pelo stamp
IF EXISTS (SELECT 1 FROM dbo.simposiumRespInteractionsItems WHERE stamp = @stamp)
BEGIN
    -- Faz o UPDATE se o registro já existir
    UPDATE dbo.simposiumRespInteractionsItems
    SET 
        token = @token,
        id = @id,
        name = @name,
        moleculeName = @moleculeName,
        drugClass = @drugClass,
        usrdata = GETDATE(),
        usrinis = @usrinis
    WHERE stamp = @stamp;
END
ELSE
BEGIN
    -- Faz o INSERT se o registro não existir
    INSERT INTO dbo.simposiumRespInteractionsItems (
        stamp, token, id, name, moleculeName, drugClass, ousrinis, ousrdata, usrinis, usrdata
    )
    VALUES (
        @stamp, @token, @id, @name, @moleculeName, @drugClass, @ousrinis, GETDATE(), @usrinis, GETDATE()
    );
END;

GO
Grant Execute On dbo.[up_save_simposiumRespInteractionsItems] to Public
Grant Control On dbo.[up_save_simposiumRespInteractionsItems] to Public
GO