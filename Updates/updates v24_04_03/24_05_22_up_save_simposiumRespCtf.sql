/****** 
	Object:  up_save_simposiumRespCtf
	exec [dbo].[up_save_simposiumRespCtf]  
***/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_save_simposiumRespCtf]') IS NOT NULL
    DROP PROCEDURE dbo.[up_save_simposiumRespCtf]
GO

CREATE PROCEDURE dbo.[up_save_simposiumRespCtf]
    @stamp         VARCHAR(36),
    @token         VARCHAR(36),
    @code          VARCHAR(254),
    @descr         VARCHAR(max),
    @ousrinis      VARCHAR(30),
    @usrinis       VARCHAR(30)
AS
BEGIN
    SET NOCOUNT ON;

    IF EXISTS (SELECT 1 FROM dbo.[simposiumRespCtf] WHERE [stamp] = @stamp)
    BEGIN
        -- Atualiza o registro existente
        UPDATE dbo.[simposiumRespCtf]
        SET 
            [token] = @token,
            [code] = @code,
            [descr] = @descr,
            [usrinis] = @usrinis,
            [usrdata] = GETDATE()
        WHERE 
            [stamp] = @stamp;
    END
    ELSE
    BEGIN
        -- Insere um novo registro
        INSERT INTO dbo.[simposiumRespCtf] (
            [stamp], [token], [code], [descr], [ousrinis], [ousrdata], [usrinis], [usrdata]
        )
        VALUES (
            @stamp, @token, @code, @descr, @ousrinis, GETDATE(), @usrinis, GETDATE()
        );
    END
END

GO
Grant Execute On dbo.[up_save_simposiumRespCtf] to Public
Grant Control On dbo.[up_save_simposiumRespCtf] to Public
GO