/*
	
	exec up_pemh_histpemhLin  '567bcf8e-c911-4f8b-94fd-99eb44b0ab82'
	exec up_pemh_histpemhLin  ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_pemh_histpemhLin]') IS NOT NULL
	drop procedure dbo.up_pemh_histpemhLin
go

create procedure dbo.up_pemh_histpemhLin
	@Token VARCHAR(40)

AS
BEGIN

	SELECT
		ISNULL(medicamento_cod, '')												as medicamento_cod
		,CONVERT(VARCHAR(254),isnull(medicamento_descr,''))						AS medicamento_descr
		,CONVERT(VARCHAR,data_caducidade)										AS data_caducidade
		,CONVERT(VARCHAR,isnull(dataPrimeiraDispensa,''), 103)					AS dataPrimeiraDispensa
		,ISNULL(duracaoProlongada, '')											as periodicidade
		,ISNULL(qtdMaxDispensavel, '')											as qtdMaxDispensavel
		,ISNULL(LEFT(Replace(rtrim(ltrim(posologia)),'<br>',' / '), 254),'')	as posologia
		,ISNULL(rtrim(ltrim(qtt)) + ' ' + RTRIM(ltrim(qttUnidade)),'')			as qtt_presc
		,isnull(CHNM,'')														as CHNM
		,ISNULL(medicamento_cnpem, '')											AS medicamento_cnpem
		,ISNULL(rtrim(ltrim(posologia)),'')										as posologiaComp
	FROM 
		Dispensa_Eletronica_D(nolock)
	WHERE
		Token = @Token
		


END

GO
Grant Execute on dbo.up_pemh_histpemhLin to Public
Grant Control on dbo.up_pemh_histpemhLin to Public
GO