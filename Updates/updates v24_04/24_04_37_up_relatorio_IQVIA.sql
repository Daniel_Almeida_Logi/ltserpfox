
IF OBJECT_ID('[dbo].[up_relatorio_iqvia]') IS NOT NULL
    DROP PROCEDURE dbo.[up_relatorio_iqvia]
GO

CREATE PROCEDURE [dbo].[up_relatorio_iqvia]
    @site           VARCHAR(36),
    @dataini        DATETIME = '19000101',
    @datafim        DATETIME = '19000101',
    @questionario   VARCHAR(254)
AS
SET NOCOUNT ON

    -- Variáveis locais
    DECLARE @sortedColumns NVARCHAR(MAX) = ''
    DECLARE @columns NVARCHAR(MAX) = ''
    DECLARE @sql NVARCHAR(MAX) = ''
    DECLARE @columnNames NVARCHAR(MAX) = ''
    DECLARE @RecordCount INT = 0

    -- Passo 1: Criar uma tabela temporária para armazenar as perguntas
    CREATE TABLE #TempPerguntas (
        pergunta VARCHAR(255),
        ordem   INT
    )

    -- Inserir perguntas distintas na tabela temporária
    INSERT INTO #TempPerguntas (pergunta, ordem)
    SELECT DISTINCT 
        pergunta, 
        ordem
    FROM 
        quest_respostas(NOLOCK)
    INNER JOIN 
        quest_pergunta(NOLOCK) ON quest_respostas.quest_perguntaStamp = quest_pergunta.quest_perguntastamp
    INNER JOIN 
        Quest(NOLOCK) ON Quest.QuestStamp = quest_respostas.questStamp
    WHERE 
        (Quest.Descr IN (SELECT items FROM dbo.up_splitToTable(@questionario, ',')) OR @questionario = '')

    -- Verificar se a tabela temporária foi preenchida
    IF EXISTS (SELECT 1 FROM #TempPerguntas)
    BEGIN
        PRINT 'Perguntas carregadas com sucesso'
    END
    ELSE
    BEGIN
        PRINT 'Nenhuma pergunta encontrada para o questionário especificado'
    END

    -- Criar tabela temporária para armazenar as colunas
    CREATE TABLE #TempColumns (
        ColumnPart NVARCHAR(MAX)
    )

    -- Inserir partes da lista de colunas
    INSERT INTO #TempColumns (ColumnPart)
    SELECT QUOTENAME(pergunta)
    FROM #TempPerguntas
    ORDER BY ordem

    -- Concatenar colunas ordenadas
    SELECT @sortedColumns = STUFF((
        SELECT ', ' + ColumnPart
        FROM #TempColumns
        FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, '')

    -- Concatenar nomes das colunas
    SELECT @columnNames = STUFF((
        SELECT ', ''' + pergunta + ''''
        FROM #TempPerguntas
        FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, '')

    -- Verificar se há registros no intervalo especificado
    SELECT @RecordCount = COUNT(*)
    FROM quest_respostas(NOLOCK)
    INNER JOIN Quest(NOLOCK) ON Quest.QuestStamp = quest_respostas.questStamp
    WHERE
        quest_respostas.site = @site
        AND CONVERT(VARCHAR, quest_respostas.ousrdata, 23) BETWEEN CONVERT(VARCHAR, @dataini, 23) AND CONVERT(VARCHAR, @datafim, 23)
        AND (Quest.Descr IN (SELECT items FROM dbo.up_splitToTable(@questionario, ',')) OR @questionario = '')

    -- Debug: imprimir contagem de registros
    PRINT 'Número de registros encontrados: ' + CAST(@RecordCount AS NVARCHAR(10))

    -- Construir o cabeçalho da consulta
    SET @sql = '
    SELECT ''Data'',' + @columnNames + ', ''Ref'', ''Codfarm'', ''Nomabrv'' '

    -- Se não houver registros, retornar apenas o cabeçalho
    IF @RecordCount = 0
    BEGIN
        PRINT 'Nenhum registro encontrado, retornando apenas o cabeçalho.'
        PRINT @sql
        EXEC(@sql)
        RETURN
    END

    -- Consulta dinâmica com dados
    SET @sql = @sql + '
    UNION ALL
    SELECT DISTINCT
         ousrdata,' + @sortedColumns + ', ref, codfarm, nomabrv
    FROM (
        SELECT 
            CONVERT(VARCHAR(10), quest_respostas.ousrdata, 103)  +'' ''+ CONVERT(VARCHAR(8), quest_respostas.ousrdata, 108) AS ousrdata,
            quest_pergunta.pergunta, 
            ISNULL(quest_respostas.resposta, '''') AS resposta,
            quest_respostas.ref,
            empresa.codfarm,
            empresa.nomabrv
        FROM
            quest_respostas(NOLOCK)
        LEFT JOIN fi(NOLOCK) ON fi.fistamp = quest_respostas.fistamp
        LEFT JOIN ft(NOLOCK) ON ft.ftstamp = fi.ftstamp
        INNER JOIN empresa(NOLOCK) ON empresa.site = quest_respostas.site
        LEFT JOIN quest_pergunta(NOLOCK) ON quest_pergunta.quest_perguntastamp = quest_respostas.quest_perguntaStamp
        INNER JOIN Quest(NOLOCK) ON Quest.QuestStamp = quest_respostas.questStamp
        WHERE
            quest_respostas.site = ''' + @site + '''
            AND CONVERT(VARCHAR, quest_respostas.ousrdata, 23) BETWEEN ''' + CONVERT(VARCHAR, @dataini, 23) + ''' AND ''' + CONVERT(VARCHAR, @datafim, 23) + '''
            AND (Quest.Descr IN (SELECT items FROM dbo.up_splitToTable(''' + @questionario + ''', '','')) OR ''' + @questionario + ''' = '''')
    ) AS Source
    PIVOT (
        MAX(resposta)
        FOR pergunta IN (' + @sortedColumns + ')
    ) AS PivotTable'

    -- Depurar e verificar o SQL final
    PRINT 'Consulta SQL Final:'
    PRINT @sql

    -- Executar a consulta final
    EXEC(@sql)
GO

GRANT EXECUTE ON dbo.[up_relatorio_iqvia] TO Public
GRANT CONTROL ON dbo.[up_relatorio_iqvia] TO Public
GO
