/* Atualiza validades e preços da ST e Lotes
	
	exec up_Documentos_AtualizaST 'ADMF08DD8D7-D65F-48FC-9F0', 1, 0,1
	exec up_Documentos_AtualizaST 'ADM39AAD23B-9EF5-4594-B4D', 1, 0, 1

*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_AtualizaST]') IS NOT NULL
	drop procedure dbo.up_Documentos_AtualizaST
go

create procedure dbo.up_Documentos_AtualizaST

@stamp varchar(30)
,@usr numeric(5)
,@usaLote bit = 0
,@site_nr tinyint

/* WITH ENCRYPTION */
AS

	/* Só atualiza se não existirem documentos posterior para a referencia */
	If OBJECT_ID('tempdb.dbo.#dadosUtlDoc') IS NOT NULL
		drop table #dadosUtlDoc;
	If OBJECT_ID('tempdb.dbo.#dadosUtlDocLote') IS NOT NULL
		drop table #dadosUtlDocLote;
	If OBJECT_ID('tempdb.dbo.#dadosComparacao') IS NOT NULL
		drop table #dadosComparacao;
	If OBJECT_ID('tempdb.dbo.#dadosFn') IS NOT NULL
		drop table #dadosFn;

	DECLARE @calcAttr varchar(100) = ''



	select 
		@calcAttr=rtrim(ltrim(textValue))    
	from 
		B_Parameters (nolock) 
	where stamp = 'ADM0000000278'
	
		
	/* Guardar último documento de entrada da referência por Lote */
	select 
		ref
		,lote
		,data = Max(data)
	into
		#dadosUtlDocLote
	from 
		fn (nolock) 
	where 
		fn.docnome in ('V/Factura', 'N/Guia Entrada', 'V/Guia Transp.', 'V/Factura Med.') 
		and fn.ref in (select ref from fn(nolock) where fn.fostamp = @stamp)
		and fn.qtt > 0 /* O ultimo documentos tem que ter quantidade, devido aos esgotados*/
		and armazem = @site_nr
	Group by
		ref, lote

	-- Guardar último documento de entrada da referência
	select 
		ref
		,data = Max(data)
	into
		#dadosUtlDoc
	from 
		#dadosUtlDocLote
	Group by
		ref
	
	

	-- Lotes
	IF @usaLote = 1
	BEGIN
		/* Validade Lotes */
		update
			st_lotes
		set
		--	st_lotes.validade = case when year(fn.u_validade) != 1900 then fn.u_validade else st.validade end
			st_lotes.validade = case
								when year(fn.u_validade) != 1900 and st.validade = '19000101'
									then fn.u_validade
								when year(fn.u_validade) != 1900 and fn.u_stockact > 0 and fn.u_validade < st.validade
									then fn.u_validade
								when year(fn.u_validade) != 1900 and fn.u_stockact <= 0
									then fn.u_validade
								else
									st.validade
								end
					
		from
			st_lotes
			inner join fn (nolock) on st_lotes.ref = fn.ref and st_lotes.lote = fn.lote
			inner join fo (nolock) on fo.fostamp = fn.fostamp
			inner join st (nolock) on fn.ref = st.ref or fn.oref = st.ref
		where
			fn.fostamp = @stamp
			and fn.ref != ''
			and fn.lote != ''
			and exists (select fnstamp from #dadosUtlDocLote where fn.ref = #dadosUtlDocLote.ref and fn.lote = #dadosUtlDocLote.lote and fo.data = #dadosUtlDocLote.data)

		/* Preços Lotes  */		
		insert into st_lotes_precos (
			stampLote
			,pct
			,epv1
		)
		Select distinct
			stampLote = st_lotes.stamp
			,pct = case when fn.epv>0 then fn.epv else 0 end
			,epv1 = case when fn.u_pvp != 0 then fn.u_pvp else 0 end
		from
			st_lotes
			inner join fn (nolock) on st_lotes.ref = fn.ref and st_lotes.lote = fn.lote and fn.armazem = st_lotes.armazem
			inner join fo (nolock) on fo.fostamp = fn.fostamp
			inner join st (nolock) on fn.ref = st.ref or fn.oref = st.ref
			inner join cm1 (nolock) on cm1.cm = fo.doccode
		where
			fn.fostamp = @stamp
			and fn.ref != ''
			and fn.lote != ''
			and cm1.folansl=1
			and fn.qtt>0
			and (fn.epv>0 or fn.u_pvp>0)
			and exists (select fnstamp from #dadosUtlDocLote where fn.ref = #dadosUtlDocLote.ref and fn.lote = #dadosUtlDocLote.lote and fo.data = #dadosUtlDocLote.data)
			and not exists (select slp.stamplote from st_lotes_precos slp where slp.epv1 = fn.u_pvp and slp.pct = fn.epv)

	END
		
	/*Retira Iva incluido ao preços de custo caso necessário*/
	select 
		fn.fnstamp
		,fn.ref
		,fn.oref
		,fn.u_pvp
		,epv = case when fn.ivaincl = 1 then round(fn.epv/(fn.iva/100+1),2) else fn.epv end 
		,u_upc = case when fn.ivaincl = 1 then round(fn.u_upc-(fn.u_upc*fn.descFin_pcl/100)/(fn.iva/100+1),2) else fn.u_upc-(fn.u_upc*fn.descFin_pcl/100) end 
		,fn.qtt
		,fn.docnome
		,fn.u_validade
		,fn.fostamp
		,fn.u_stockact
		,fn.val_desp_lin
	into
		#dadosFn
	From
		fn (nolock)
	where
		fn.fostamp = @stamp
		and fn.qtt>0


	-- insert na tabela de histórico de preços
	insert into B_historicopvp(stamp, ref, opvp, dpvp, oMc, dMc, oDc, dDc, oMb, dMb, oBb, dBb, data, local, usr, site, terminal, ostamp)
	Select 
		stamp = LEFT(newid(),25)
		,fn.ref
		,opvp = st.epv1
		,dpvp = fn.u_pvp
		,oMc = st.marg1
		,dMc = case
				when fn.u_pvp > 0 AND ROUND(fn.epv,2) > 0 then ROUND( ((fn.u_pvp / (taxasiva.taxa/100+1) / fn.epv) -1) * 100 ,2)
				when ROUND(fn.epv,2) > 0 then ROUND(((st.epv1 / (taxasiva.taxa/100+1) / round(fn.epv,2)) -1) * 100 ,2)
				when fn.u_pvp > 0 AND ROUND(st.epcusto,2) > 0 then ROUND( ((fn.u_pvp / (taxasiva.taxa/100+1) / round(st.epcusto,2)) -1) * 100 ,2)
				ELSE st.marg1 
				end
		,oDc = st.marg2
		,dDc = case 
				when fn.epv > 0 and ROUND(fn.u_upc,2) > 0 then  ROUND(((round(fn.epv,2) / round(fn.u_upc,2)) -1) * 100 ,2)
				when fn.epv > 0 and ROUND(st.epcult,2) > 0 then ROUND(((round(fn.epv,2) / round(st.epcult,2)) -1) * 100 ,2)
				ELSE st.marg2
				end
		,oMb = st.marg3
		,dMb = case 
				when fn.u_pvp = 0 and st.epv1 > 0 and ROUND(st.epcpond,2) > 0 then ROUND(((st.epv1 / (taxasiva.taxa/100+1) / round(st.epcpond,2)) - 1) * 100 ,2)
				when fn.u_pvp > 0 and ROUND(st.epcpond,2) > 0 then ROUND(((fn.u_pvp / (taxasiva.taxa/100+1) / round(st.epcpond,2)) - 1) * 100,2) 
				when st.epv1 > 0 and st.epcpond = 0 and cm1.folansl = 1 and fn.qtt > 0 AND ROUND(fn.u_upc,2) > 0 then ROUND(((st.epv1 / (taxasiva.taxa/100+1) / round(fn.u_upc,2)) - 1) * 100,2)
				when st.epv1 > 0 and st.epcpond = 0 and ROUND(st.epcult,2) > 0 and(cm1.folansl != 1 or fn.qtt<=0 or fn.u_upc<=0) then ROUND(((st.epv1 / (taxasiva.taxa/100+1) / round(st.epcult,2)) - 1) * 100,2)
				when fn.u_pvp = 0 and st.epv1 = 0 then 0
				else st.marg3
				end
		,oBb = st.marg4
		,dBb = case 				
					when fn.u_pvp = 0 and st.epv1 > 0 and @calcAttr='pcp' and st.epcpond > 0  then ROUND((st.epv1 / (taxasiva.taxa/100+1)) - round(st.epcpond,2),2)
					when fn.u_pvp = 0 and st.epv1 > 0 and @calcAttr='pcl' and st.epcult > 0  then ROUND((st.epv1 / (taxasiva.taxa/100+1)) - round(st.epcult,2),2)
					when fn.u_pvp = 0 and st.epv1 > 0 and @calcAttr='pct' and st.epcusto > 0  then ROUND((st.epv1 / (taxasiva.taxa/100+1)) - round(st.epcusto,2),2)
					when fn.u_pvp = 0 and st.epv1 > 0 and st.epcpond <= 0  and  st.epcult >= 0 then ROUND((st.epv1 / (taxasiva.taxa/100+1)) - round(st.epcult,2),2)
					when fn.u_pvp = 0 and st.epv1 = 0 then 0
					when fn.u_pvp > 0 and st.epcpond>0 and @calcAttr='pcp' then ROUND((fn.u_pvp / (taxasiva.taxa/100+1)) - round(st.epcpond,2),2)
					when fn.u_pvp > 0 and st.epcusto>0  and @calcAttr='pct' then ROUND((fn.u_pvp / (taxasiva.taxa/100+1)) - round(st.epcusto,2),2) 
					when fn.u_pvp > 0 and st.epcult>0 and @calcAttr='pcl' then ROUND((fn.u_pvp / (taxasiva.taxa/100+1)) - round(st.epcult,2),2) 
					when st.epv1 > 0 and st.epcpond=0 and cm1.folansl = 1 and fn.qtt>0 AND fn.u_upc>0 then ROUND((st.epv1 / (taxasiva.taxa/100+1)) - round(fn.u_upc,2),2)
					when st.epv1 > 0 and st.epcpond=0 and (cm1.folansl != 1 or fn.qtt<=0 or fn.u_upc<=0) then ROUND((st.epv1 / (taxasiva.taxa/100+1)) - round(st.epcult,2),2)
					else st.marg4
					end
		,data = GETDATE()
		,local = fn.docnome
		,usr = @usr
		,site = fo.site
		,terminal = fo.pno
		,ostamp = fn.fnstamp
	from 
		#dadosFn Fn
		inner join fo (nolock) on fo.fostamp = fn.fostamp
		inner join st (nolock) on fn.ref = st.ref or fn.oref = st.ref
		inner join taxasiva (nolock) on taxasiva.codigo = st.tabiva
		inner join cm1 (nolock) on cm1.cm = fo.doccode
	where 
		fn.fostamp = @stamp
		and fn.u_pvp != st.epv1
		and exists (select * from #dadosUtlDoc where fn.ref = #dadosUtlDoc.ref and fo.data = #dadosUtlDoc.data)
		and st.site_nr = @site_nr

	--  


	Select 
		st.ref
		,st.design
		,validadeAnterior = st.validade
		,st.stock	
	--	,validadeNova = case
	--						when year(fn.u_validade) != 1900 --and st.validade = '19000101'
	--							then fn.u_validade
	--						else
	--								st.validade
	--						end
		,validadeNova =  case
							when year(fn.u_validade) != 1900 and st.validade = '19000101'
								then fn.u_validade
							when year(fn.u_validade) != 1900 and fn.u_stockact > 0 and fn.u_validade < st.validade
								then fn.u_validade
							when year(fn.u_validade) != 1900 and fn.u_stockact <= 0
								then fn.u_validade
							else
								st.validade
							end
		,PVPAnterior = st.epv1
		,PVPNovo = case when ROUND(fn.u_pvp,2) != 0 then ROUND(fn.u_pvp,2) else st.epv1 end
		,epcult = case when (cm1.folansl = 1 or cm1.cm = 55) and fn.qtt>0 AND fn.u_upc>0 then fn.u_upc else st.epcult end
		,epcusto = case when (cm1.folansl = 1 or cm1.cm = 55) and fn.qtt>0 AND fn.epv>0 then fn.epv else st.epcusto end
	into
		#dadosComparacao
	from 
		#dadosFn Fn
		inner join fo (nolock) on fo.fostamp = fn.fostamp 
		inner join st (nolock) on fn.ref = st.ref or fn.oref = st.ref
		inner join taxasiva (nolock) on taxasiva.codigo = st.tabiva
		inner join cm1 (nolock) on cm1.cm = fo.doccode
	where 
		fn.fostamp = @stamp
		and fn.ref != ''
		and exists (select * from #dadosUtlDoc where fn.ref = #dadosUtlDoc.ref and fo.data = #dadosUtlDoc.data)
		and st.site_nr = @site_nr

	--



	Update
		st
	Set 

		--validade = case
		--				when year(fn.u_validade) != 1900 --and st.validade = '19000101'
		--					then fn.u_validade
		--				else
		--					st.validade
		--				end 
		validade = case
						when year(fn.u_validade) != 1900 and st.validade < GETDATE()- YEAR(10) and st.stock <>0
							then fn.u_validade
						when year(fn.u_validade) != 1900 and fn.u_stockact > 0 and fn.u_validade < st.validade
							then fn.u_validade
						when year(fn.u_validade) != 1900 and fn.u_stockact <= 0
							then fn.u_validade
						else
							st.validade
						end
		--then fn.u_validade else st.validade end
		
		,epv1    = case when ROUND(fn.u_pvp,2) > 0 then ROUND(fn.u_pvp,2) else st.epv1 end
		,epcult  = case when cm1.folansl = 1 and cm2.mudapcult=1 and fn.qtt > 0 AND fn.u_upc > 0 then fn.u_upc + round(fn.val_desp_lin/fn.qtt,2) else st.epcult end
		,epcusto = st.epcusto--case when cm1.folansl = 1 and cm2.mudaepcusto=1 and fn.qtt > 0 AND fn.epv > 0 then fn.epv + round(fn.val_desp_lin/fn.qtt,2) else st.epcusto end
		-- Margem Comercial
		,marg1	= case
					when fn.u_pvp > 0 AND fn.epv>0 then ROUND( ((fn.u_pvp / (taxasiva.taxa/100+1) / round(fn.epv,2)) -1) * 100 ,2)
					when fn.epv   > 0 then ROUND( ((st.epv1 / (taxasiva.taxa/100+1) / round(fn.epv,2)) -1) * 100 ,2)
					when fn.u_pvp > 0 AND st.epcusto > 0 then ROUND( ((fn.u_pvp / (taxasiva.taxa/100+1) / round(st.epcusto,2)) -1) * 100 ,2)
					ELSE st.marg1 
					end
		-- Desconto Comercial
		,marg2	= case 
					when fn.epv > 0 and fn.u_upc > 0 then ROUND(((round(fn.epv,2) / round(fn.u_upc,2)) -1) * 100 ,2)
					when fn.epv > 0 and st.epcult > 0 then ROUND(((round(fn.epv,2) / round(st.epcult,2)) -1) * 100 ,2)
					ELSE st.marg2
					end
		-- Margem Bruta s/PC!!!
		,marg3	= case 
					when fn.u_pvp = 0 and st.epv1 > 0 and st.epcpond>0 then ROUND(((st.epv1 / (taxasiva.taxa/100+1) / st.epcpond) - 1) * 100 ,2)
					when fn.u_pvp > 0 and st.epcpond > 0 then ROUND(((fn.u_pvp / (taxasiva.taxa/100+1) / st.epcpond) - 1) * 100,2) 
					when st.epv1  > 0 and st.epcpond = 0 and cm1.folansl = 1 and fn.qtt>0 AND fn.u_upc>0 then ROUND(((st.epv1 / (taxasiva.taxa/100+1) / fn.u_upc) - 1) * 100,2)
					when st.epv1  > 0 and st.epcpond = 0 and st.epcult > 0 and (cm1.folansl != 1 or fn.qtt<=0 or fn.u_upc<=0) then ROUND(((st.epv1 / (taxasiva.taxa/100+1) / st.epcult) - 1) * 100,2)
					when fn.u_pvp = 0 and st.epv1 = 0 then 0
					else st.marg3
					end
		-- Benificio Bruto
		,marg4	= case 
				
					when fn.u_pvp = 0 and st.epv1 > 0 and @calcAttr='pcp' and st.epcpond > 0  then ROUND((st.epv1 / (taxasiva.taxa/100+1)) - round(st.epcpond,2),2)
					when fn.u_pvp = 0 and st.epv1 > 0 and @calcAttr='pcl' and st.epcult > 0  then ROUND((st.epv1 / (taxasiva.taxa/100+1)) - round(st.epcult,2),2)
					when fn.u_pvp = 0 and st.epv1 > 0 and @calcAttr='pct' and st.epcusto > 0  then ROUND((st.epv1 / (taxasiva.taxa/100+1)) - round(st.epcusto,2),2)
					when fn.u_pvp = 0 and st.epv1 > 0 and st.epcpond <= 0  and  st.epcult >= 0 then ROUND((st.epv1 / (taxasiva.taxa/100+1)) - round(st.epcult,2),2)
					when fn.u_pvp = 0 and st.epv1 = 0 then 0
					when fn.u_pvp > 0 and st.epcpond>0 and @calcAttr='pcp' then ROUND((fn.u_pvp / (taxasiva.taxa/100+1)) - round(st.epcpond,2),2)
					when fn.u_pvp > 0 and st.epcusto>0  and @calcAttr='pct' then ROUND((fn.u_pvp / (taxasiva.taxa/100+1)) - round(st.epcusto,2),2) 
					when fn.u_pvp > 0 and st.epcult>0 and @calcAttr='pcl' then ROUND((fn.u_pvp / (taxasiva.taxa/100+1)) - round(st.epcult,2),2) 
					when st.epv1 > 0 and st.epcpond=0 and cm1.folansl = 1 and fn.qtt>0 AND fn.u_upc>0 then ROUND((st.epv1 / (taxasiva.taxa/100+1)) - round(fn.u_upc,2),2)
					when st.epv1 > 0 and st.epcpond=0 and (cm1.folansl != 1 or fn.qtt<=0 or fn.u_upc<=0) then ROUND((st.epv1 / (taxasiva.taxa/100+1)) - round(st.epcult,2),2)
					else st.marg4
					end
	from
		#dadosFn Fn
		inner join fo (nolock) on fo.fostamp = fn.fostamp
		inner join st (nolock) on fn.ref = st.ref or fn.oref = st.ref
		inner join taxasiva (nolock) on taxasiva.codigo = st.tabiva
		inner join cm1 (nolock) on cm1.cm = fo.doccode
		inner join cm2 (nolock) on cm1.cm=cm2.cm
	where
		fn.fostamp = @stamp
		and fn.ref != ''
		and exists (select * from #dadosUtlDoc where fn.ref = #dadosUtlDoc.ref and fo.data = #dadosUtlDoc.data)
		and st.site_nr = @site_nr
		
	select * from #dadosComparacao

	If OBJECT_ID('tempdb.dbo.#dadosUtlDoc') IS NOT NULL
		drop table #dadosUtlDoc;
	If OBJECT_ID('tempdb.dbo.#dadosUtlDocLote') IS NOT NULL
		drop table #dadosUtlDocLote;
	If OBJECT_ID('tempdb.dbo.#dadosComparacao') IS NOT NULL
		drop table #dadosComparacao;
	If OBJECT_ID('tempdb.dbo.#dadosFn') IS NOT NULL
		drop table #dadosFn;
GO
Grant Execute On dbo.up_Documentos_AtualizaST to Public
Grant Control On dbo.up_Documentos_AtualizaST to Public
Go