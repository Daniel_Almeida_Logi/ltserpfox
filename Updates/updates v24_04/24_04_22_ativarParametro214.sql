DECLARE @site VARCHAR(20)
declare emp_cursor cursor for
select site as siteemp from empresa
open emp_cursor
fetch next from emp_cursor into @site
while @@FETCH_STATUS = 0
begin
	IF (SELECT tipoempresa FROM empresa WHERE site=@site) = 'FARMACIA'
	BEGIN
		update B_Parameters_site set bool=1 where stamp='ADM0000000214' AND site=@site
	END
	ELSE
	BEGIN
		update B_Parameters_site set bool=0 where stamp='ADM0000000214' AND site=@site
	END
	fetch next from emp_cursor into @site
end
close emp_cursor
deallocate emp_cursor



