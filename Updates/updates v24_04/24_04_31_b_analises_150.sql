

delete b_analises_config where ordem=150

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=150 and label='Operador'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 150,[nome],[label],[tipo],[comando],3,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],0,[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 170 and label='Operador'
END

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=150 and label='Data Início'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 150,[nome],[label],[tipo],[comando],1,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 1 and label='Data Início'
END

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=150 and label='Data Fim'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 150,[nome],[label],[tipo],[comando],2,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 1 and label='Data Fim'
END

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=150 and label='Utente'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 150,[nome],[label],[tipo],[comando],4,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],0,[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 6 and label='Utente'
END
