
DECLARE @site VARCHAR(20)
declare emp_cursor cursor for
select site as siteemp from empresa
open emp_cursor
fetch next from emp_cursor into @site
while @@FETCH_STATUS = 0
begin
	
	if(exists(select * from docsToSendParameters WHERE idDocs=103 AND SITE=@site))
	BEGIN
		if( NOT exists(select * from docsToSendParameters WHERE idDocs=103 AND SITE=@site AND name='adiantamento'))
		BEGIN
			INSERT INTO docsToSendParameters (token,idDocs,site,name,type,value,sequence)
			VALUES
				   (LEFT(NEWID(),36),103,'Loja 1','adiantamento','vc','',9)
		END
	END

	fetch next from emp_cursor into @site
end
close emp_cursor
deallocate emp_cursor

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=6 and label='Adiantamento'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],
					[comando],
					[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
Values( 6,'adiantamento','Adiantamento','vc',
		'select distinct sel = convert(bit,0), texto = ''C/Adiantamento'' UNION ALL select distinct sel = convert(bit,0), texto = ''S/Adiantamento''',
		7,'',0,0
		,'texto','',0,1,'','',0,1,1)
END