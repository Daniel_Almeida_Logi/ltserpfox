IF not EXISTS (SELECT 1 FROM B_Parameters WHERE stamp='ADM0000000377')  AND DB_NAME() <> 'F01078A'
	BEGIN
		insert into B_Parameters (stamp, name, Type, textValue,
				 numValue, bool, ODate, LDate, visivel, MostraTextValue, MostraNumValue, MostraBool,
				  ListaTextValue, Unidades, obs)
		values ('ADM0000000377', 'Tempo Consulta DEM', 'DEM', '',
				 2000, 0, getdate(), getdate(), 0, 0, 1, 0,
				  '', '', 'Tempo em Milisegundos')
	END


	--NA ANTERO CHAVES ENTRA O TIMER DA DEM COM 1 SEG

	IF not EXISTS (SELECT 1 FROM B_Parameters WHERE stamp='ADM0000000377')  AND DB_NAME() = 'F01078A'
	BEGIN
		insert into B_Parameters (stamp, name, Type, textValue,
				 numValue, bool, ODate, LDate, visivel, MostraTextValue, MostraNumValue, MostraBool,
				  ListaTextValue, Unidades, obs)
		values ('ADM0000000377', 'Tempo Consulta DEM', 'DEM', '',
				 1000, 0, getdate(), getdate(), 0, 0, 1, 0,
				  '', '', 'Tempo em Milisegundos')
	END