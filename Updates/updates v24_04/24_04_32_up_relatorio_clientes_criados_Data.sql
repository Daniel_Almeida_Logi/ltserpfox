/* Calculo Inventário Detalhado 

	select ousrdata from b_utentes
	exec up_relatorio_clientes_criados_Data '20191213','20240901','',''
*/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('[dbo].[up_relatorio_clientes_criados_Data]') IS NOT NULL
	drop procedure dbo.up_relatorio_clientes_criados_Data
GO

CREATE PROCEDURE [dbo].[up_relatorio_clientes_criados_Data]
	@dataIni	AS DATETIME,
	@dataFim	AS DATETIME,
	@op			AS VARCHAR(254),
	@utente		AS VARCHAR(254)

/* with encryption */

AS 		

	/* Result set */
	SELECT
		b_utentes.no, b_utentes.nome, b_utentes.ousrdata, b_utentes.ousrhora, b_utentes.ousrinis
	FROM
		b_utentes
	INNER JOIN 
		b_us(nolock) on b_us.iniciais = b_utentes.ousrinis
	WHERE
		CONVERT(DATE,b_utentes.ousrdata ) BETWEEN @dataIni AND @dataFim 
		AND (CONVERT(varchar(20),b_us.userno) in (select items from dbo.up_splitToTable(@op,','))or @op= '' or @op= '0')
		AND no_ext=''
		AND '[' + convert(varchar(9),b_utentes.no) + '] ' + '[' + convert(varchar(9),b_utentes.estab) + '] ' +
			 b_utentes.nome = case when @utente = '' then  '[' + convert(varchar(9),b_utentes.no) + '] ' + 
			 '[' + convert(varchar(9),b_utentes.estab) + '] ' + b_utentes.nome else @utente end 
	ORDER BY
		CAST(CAST(b_utentes.ousrdata AS DATE) AS DATETIME) + CAST(b_utentes.ousrhora AS DATETIME) 

GO
Grant Execute on dbo.up_relatorio_conferencia_inventarioData to Public
Grant control on dbo.up_relatorio_conferencia_inventarioData to Public
Go