/* SP que retorna Sit Clinicas Ligeiras

	exec up_utentes_SitClinicas '61489', '19000101', '20500101'

*/
if OBJECT_ID('[dbo].[up_utentes_SitClinicas ]') IS NOT NULL
	drop procedure dbo.up_utentes_SitClinicas 
go

create procedure dbo.up_utentes_SitClinicas 
	@ClNo varchar(36),
	@DataIni varchar(20) = '19000101',
	@DataFim varchar(20) = getdate

/* WITH ENCRYPTION */
AS
	Select
			CONVERT(VARCHAR(10), quest_respostas.ousrdata, 103) AS Data
			,quest_respostas.ousrhora AS Hora
			,quest_respostas_grpStamp

			,convert(varchar(254),rtrim(ltrim((select qr.resposta from quest_respostas(nolock) qr where qr.quest_perguntaStamp = 'C25686DF-22A6-414D-88CA-8' and qr.quest_respostas_grpStamp = quest_respostas.quest_respostas_grpStamp)))
				+
					'. '
				+
				rtrim(ltrim(isnull((select qr.resposta from quest_respostas(nolock) qr where qr.quest_perguntaStamp = '5739A9A9-6E69-4193-857C-A' and qr.quest_respostas_grpStamp = quest_respostas.quest_respostas_grpStamp),''))))
					as Obs
			,(select  iniciais from b_us(nolock) where userno = quest_respostas.operador) as ut
			, (select qr.resposta	from quest_respostas(nolock) qr where qr.quest_respostas_grpStamp = quest_respostas.quest_respostas_grpStamp and  qr.quest_perguntastamp = 'C25686DF-22A6-414D-88CA-8') as sitcli
			,origem			= (select qr.resposta	from quest_respostas(nolock) qr where qr.quest_respostas_grpStamp = quest_respostas.quest_respostas_grpStamp and  qr.quest_perguntastamp = '2B442423-878C-4424-8113-9')
			,intervencao	= (select qr.resposta	from quest_respostas(nolock) qr where qr.quest_respostas_grpStamp = quest_respostas.quest_respostas_grpStamp and  qr.quest_perguntastamp = 'FC9689AB-927A-473D-8510-A')
			,prodSolicitado = isnull((select qr.resposta	from quest_respostas(nolock) qr where qr.quest_respostas_grpStamp = quest_respostas.quest_respostas_grpStamp and  qr.quest_perguntastamp = '6112C549-6F77-4A6D-A5BA-E'),'')
			,observacoes	= convert(varchar(254),isnull((select qr.resposta from quest_respostas(nolock) qr where qr.quest_respostas_grpStamp = quest_respostas.quest_respostas_grpStamp and  qr.quest_perguntastamp = '5739A9A9-6E69-4193-857C-A'),''))
			,quest_respostas.ref 
			,design = ISNULL((select st.design from st (nolock) where ref = quest_respostas.ref and site_nr = 1),'')
			,nmdoc = isnull(ft.nmdoc,'')
			,ndoc = isnull(str(ft.fno),'')
			,quest_respostas.sexo
			,quest_respostas.faixaEtaria
	from quest_respostas(nolock)
	left join fi(nolock) on fi.fistamp = quest_respostas.fistamp
	left join ft(nolock) on fi.ftstamp = ft.ftstamp
	left join st (nolock) on st.ref = quest_respostas.ref  and st.site_nr = 1
	where quest_respostas.QuestStamp = '230AE1CA-D832-49E4-8D73-E '
		and quest_respostas.no = @ClNo
		and quest_respostas.ousrdata between @DataIni and @DataFim
	group by quest_respostas_grpStamp, quest_respostas.operador, quest_respostas.ousrdata, quest_respostas.fistamp,quest_respostas.ref ,quest_respostas.sexo,quest_respostas.faixaEtaria, quest_respostas.ousrhora, st.design,ft.nmdoc,ft.fno 
	
	--	exec up_utentes_SitClinicas '234', '19000101', '20500101'


GO
Grant Execute On dbo.up_utentes_SitClinicas  to Public
Grant Control On dbo.up_utentes_SitClinicas  to Public
Go 