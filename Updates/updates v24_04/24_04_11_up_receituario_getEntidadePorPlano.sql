/* Retorna informacao da entidade comparticipadora

	 exec up_receituario_getEntidadePorPlano 'GL'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_getEntidadePorPlano]') IS NOT NULL
	drop procedure dbo.up_receituario_getEntidadePorPlano
go


create procedure dbo.up_receituario_getEntidadePorPlano

@plano varchar(10)


/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

		
	select 
		permiteCalcCompart = isnull(cptorg.permiteCalcCompart,1),
		percadic
	from 
		cptorg(nolock)
	inner join 
		cptpla(nolock) on cptpla.cptorgstamp = cptorg.cptorgstamp
	where 
		cptpla.codigo=@plano



GO
Grant Execute on dbo.up_receituario_getEntidadePorPlano to Public
Grant Execute on dbo.up_receituario_getEntidadePorPlano to Public
go