/* SP que retorna Questionario

	exec up_questionario_getQuest '91078C81-4CBD-449B-B823-28DF54053A44'

*/
if OBJECT_ID('[dbo].[up_questionario_getQuest]') IS NOT NULL
	drop procedure dbo.up_questionario_getQuest
go

create procedure dbo.up_questionario_getQuest
	@QuestStamp varchar(36)

/* WITH ENCRYPTION */
AS
	--	exec up_questionario_getQuest '91078C81-4CBD-449B-B823-28DF54053A44'
	Select
		CAST(0 AS bit) AS SEL 
		,Quest.QuestStamp 
		,quest_pergunta.quest_perguntastamp
		,quest_pergunta.ordem
		,convert(varchar(200), quest_pergunta.pergunta) as pergunta
		,quest_pergunta_resp.Quest_pergunta_respStamp
		,ltrim(rtrim(quest_pergunta_resp .resposta)) as resposta
		,quest_regras.Prox_Ordem
		,quest_pergunta_resp_tp.ID
		,quest_pergunta.permiteMulti
	from quest
	inner join quest_pergunta(nolock)			on quest_pergunta.queststamp = Quest.QuestStamp
	inner join quest_pergunta_resp(nolock)		on quest_pergunta_resp.Quest_perguntaStamp = quest_pergunta.quest_perguntastamp 
	inner join quest_pergunta_resp_tp(nolock)	on quest_pergunta_resp.quest_pergunta_resp_tpStamp = quest_pergunta_resp_tp.quest_pergunta_resp_tpStamp 
	inner join quest_regras (nolock )			on quest_regras.quest_pergunta_respStamp = quest_pergunta_resp.Quest_pergunta_respStamp
	where quest.QuestStamp = @QuestStamp
	group by quest_pergunta.pergunta, quest_pergunta_resp .resposta ,quest_regras.Prox_Ordem, quest_pergunta_resp_tp.ID
			, quest_pergunta.ordem, quest_pergunta_resp.ordem, Quest.QuestStamp,quest_pergunta.quest_perguntastamp
			,quest_pergunta_resp.Quest_pergunta_respStamp, quest_pergunta.permiteMulti
	order by quest_pergunta.ordem asc, quest_pergunta_resp.ordem asc


GO
Grant Execute On dbo.up_questionario_getQuest to Public
Grant Control On dbo.up_questionario_getQuest to Public
Go 