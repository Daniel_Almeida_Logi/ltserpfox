delete b_analises_config WHERE ordem=170 and label='Estado'
IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=170 and label='Ação'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],
					[comando],
					[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
Values( 170,'acao','Ação','vc',
		'select distinct sel = convert(bit,0), design from nmvoAcaoTraducao(NOLOCK)',
		5,'',0,0
		,'design','',0,1,'','',1,1,1)
END

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=170 and label='Operador'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],
					[comando],
					[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
Values( 170,'op','Operador','vc',
		'Select sel = convert(bit,0),convert(varchar(3),isnull(cm,0)) as cm,b_us.nome from b_us (nolock) inner join cm3 (nolock) on cm3.cm = b_us.userno UNION ALL SELECT sel = convert(bit,0),convert(varchar(3),0) AS cm,''todos'' as nome order by b_us.nome',
		6,'',0,0
		,'cm, nome','',0,1,'','',1,1,1)
END

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=170 and label='Estado'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],
					[comando],
					[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
Values( 170,'status','Estado','vc',
		'select distinct sel = convert(bit,0), texto = ''C\Erro'' UNION ALL select distinct sel = convert(bit,0), texto = ''S\Erro''',
		7,'',0,0
		,'texto','',0,1,'','',0,1,1)
END

update  B_analises
set comentario='Listagem da verificação, inativação e reativação de códigos bidimensionais entre um determinado período ou para referências específicas',
objectivo='Listagem da verificação, inativação e reativação de códigos bidimensionais entre um determinado período ou para referências específicas'
 where ordem=170

 delete b_analises_config WHERE ordem=170 and label='Cód. Produto'

 IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=170 and label='Nº Série'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],
					[comando],
					[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
Values( 170,'productCode','Nº Série','vc',
		'select distinct  sel = convert(bit,0),texto = packSerialNumber from fi_trans_info(nolock)',
		8,'',0,0
		,'texto','',0,1,'','',0,1,1)
END

