/* Movimentos de Stocks para o serviço CONSIGO da PLURAL+Udifar

	 exec up_stocks_MovimentosTotais_CONSIGO 2, '20240101', '20240105'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


IF OBJECT_ID('[dbo].[up_stocks_MovimentosTotais_CONSIGO]') IS NOT NULL
	drop procedure dbo.up_stocks_MovimentosTotais_CONSIGO
go

CREATE procedure dbo.up_stocks_MovimentosTotais_CONSIGO
@armazem numeric(5,0),
@dataIni datetime,
@dataFim datetime


/* WITH ENCRYPTION */
AS



	SELECT 
		COD_FARMACIA		= (select infarmed from empresa 
										where no = (select empresa_no from empresa_arm 
														where armazem = @armazem
														)
									)
		,COD_MONITORIZAÇÂO	= 'PLMV-FARM' 	-- "PLMV-FARM" se for uma farmácia, e se for um posto "PLMV-POSTO001"
											-- O 001 corresponde ao mapeamento que fazem eles e altera de posto para posto e é indicado por eles.
		,INI_PERIODO		= CONVERT(varchar,@dataIni,112)
		,FIM_PERIODO		= CONVERT(varchar,@dataFim,112)
		,DT_HR_MOV			= CONCAT(
									CONVERT(varchar,ousrdata,112) --formato YYYYMMDD
									,REPLACE(CONVERT(varchar(8), ousrhora, 108), ':', '') --formato HHMMSS
								)
		,CODIGO				= ref
		,DESIGNACAO			= design
		,QT_TOT				= case	when sl.cm < 50 
							  			then round(sl.qtt,0) 
									when sl.cm > 50
							  			then round(sl.qtt,0) * -1
									else round(0,0)
							  	end
		,VPVP				= evu * 100
		,TIPO_MOVIMENTO		= CASE 	WHEN cmdesc = 'V/Factura'				THEN 'VFT'
									WHEN cmdesc = 'V/Vd.Dinheiro'			THEN 'VFR'
									WHEN cmdesc = 'Entrada p/ Devolução'	THEN 'EDV'
									WHEN cmdesc = 'V/Guia Transp.'			THEN 'VGT'
									WHEN cmdesc = 'Entrada prod.'			THEN 'ENT'
									WHEN cmdesc = 'V/Nt. Débito'			THEN 'VND'
									WHEN cmdesc = 'N/G.Entrada'				THEN 'NGE'
									WHEN cmdesc = 'V/Nt.Crédito'			THEN 'VNC'
									WHEN cmdesc = 'V/Nt.Crédito E'			THEN 'VNC'
									WHEN cmdesc = 'Acerto de Stock'			THEN 'AST'
									WHEN cmdesc = 'Conversao Unidades'		THEN 'CON'
									WHEN cmdesc = 'Entrada p/Inventário'	THEN 'EIV'
									WHEN cmdesc = 'Entrada p/trf'			THEN 'ETR'
									WHEN cmdesc = 'Stock Inicial'			THEN 'STI'
									WHEN cmdesc = 'Saida diversa'			THEN 'SAI'
									WHEN cmdesc = 'Cons. interno'			THEN 'INT'
									WHEN cmdesc = 'Saida p/Devolução'		THEN 'SDV'
									WHEN cmdesc = 'N/Factura'				THEN 'NFT'
									WHEN cmdesc = 'N/V.Dinheiro'			THEN 'NVD'
									WHEN cmdesc = 'N/Nt.Crédito'			THEN 'NNC'
									WHEN cmdesc = 'N/Nt.Débito'				THEN 'NND'
									WHEN cmdesc = 'N/Guia de Transp.'		THEN 'NGT'
									WHEN cmdesc = 'Stock Quebra'			THEN 'QST'
									WHEN cmdesc = 'Saida p/Inventário'		THEN 'SIV'
									WHEN cmdesc = 'Saida prod.'				THEN 'SAI'
									WHEN cmdesc = 'Saida p/trf'				THEN 'STR'
									ELSE ''
								END
		from sl (nolock)
		where	sl.datalc between @dataIni and @dataFim
			and sl.armazem = @armazem
		order by datalc asc

	GO
	
GO
Grant Execute on dbo.up_stocks_MovimentosTotais_CONSIGO to Public
Grant Control on dbo.up_stocks_MovimentosTotais_CONSIGO to Public
go
  