
/*

	Devolve plano PEMH

	up_dem_planoPemH 'd6c9caab-5139-4d95-987b-3ceaefaa00f3'

*/
if OBJECT_ID('[dbo].[up_dem_planoPemH]') IS NOT NULL
	drop procedure dbo.up_dem_planoPemH
go

create procedure dbo.up_dem_planoPemH
	@token  varchar (40)

	AS

	DECLARE 
		@local_presc_cod VARCHAR(100) = '',
		@plano VARCHAR(3) = ''
	
	SELECT 
		@local_presc_cod = rtrim(ltrim(ISNULL(local_presc_cod,'')))
	FROM
		Dispensa_Eletronica(NOLOCK)
	WHERE
		token = @token


    SET @plano = CASE 
                    WHEN @local_presc_cod = '3147102' THEN 'H04'
                    WHEN @local_presc_cod = '' THEN ''
                    ELSE ''  -- ou outro valor padrão, se necessário
                 END;

    
    SELECT @plano AS plano;



GO
Grant Execute On dbo.up_dem_planoPemH to Public
Grant Control On dbo.up_dem_planoPemH to Public
Go 