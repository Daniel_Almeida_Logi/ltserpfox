/*  lista de documentos a enviar 
EXEC usp_servico_ListaDocumentoEnviar
EXEC usp_servico_ListaDocumentoEnviar ''
EXEC usp_servico_ListaDocumentoEnviar 'ORDERS'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[usp_servico_ListaDocumentoEnviar]') IS NOT NULL
	drop procedure dbo.usp_servico_ListaDocumentoEnviar
go

CREATE PROCEDURE [dbo].[usp_servico_ListaDocumentoEnviar]

	@typeExec VARCHAR(60)=''
AS
SET NOCOUNT ON


	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRodape'))
		DROP TABLE #dadosRodape

	create table #dadosRodape(
		rodape		varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AI,
		site		varchar(200),
		inicial		VARCHAR(30)
	)


	DECLARE @site VARCHAR(20)
	DECLARE @codInfarmed VARCHAR(7)
	DECLARE emp_cursor cursor for
	select site as siteemp from empresa

	open emp_cursor
	fetch next from emp_cursor into @site

	while @@FETCH_STATUS = 0
	begin
		INSERT #dadosRodape (rodape,site,inicial)
		EXEC usp_get_rodapeEmail  'ADM', @site 
		
		fetch next from emp_cursor into @site
	end
	close emp_cursor
	deallocate emp_cursor


	SELECT 
		 token
		,id			
		,site
		,(CASE nameDocN
			WHEN 0 THEN 	((SELECT top 1 nomabrv FROM empresa (NOLOCK) WHERE site=docsToSend.site) + '_' + nameDoc + '_' + convert(varchar(10),(SELECT CONVERT(date,dateNextCall)))   )
			WHEN 1 THEN 	(select convert(varchar(20),(select convert(varchar(10),codfarm) from empresa where empresa.site = docsToSend.site) + (SELECT Replace(CONVERT(VARCHAR(20), DATEADD(DAY,-1,dateNextCall), 103),'/','')))) --- OCP
			WHEN 2 THEN     (select convert(varchar(20),(select convert(varchar(10),infarmed) from empresa where empresa.site = docsToSend.site) + '_VENDAS_' + replace(convert(VARCHAR,getdate(),103),'/','')))		-- Ferreira interno
			WHEN 3 THEN     '0708407' + (SELECT Replace(CONVERT(VARCHAR(20), DATEADD(DAY,-1,dateNextCall), 112),'/','')) +'001'																							-- Ferreira interno
			WHEN 4 THEN     '0725407' + (SELECT Replace(CONVERT(VARCHAR(20), DATEADD(DAY,-1,dateNextCall), 112),'/','')) +'001'																							-- Ferreira interno
			WHEN 5 THEN     '0724407' + (SELECT Replace(CONVERT(VARCHAR(20), DATEADD(DAY,-1,dateNextCall), 112),'/','')) +'001'																							-- Ferreira interno
			WHEN 6 THEN		 (SELECT top 1 nomabrv FROM empresa (NOLOCK) WHERE site=docsToSend.site) 																													-- nome empresa
			WHEN 7 THEN		(select convert(varchar(20),(select RIGHT('000000' + convert(varchar(10),codfarm), 7) from empresa where empresa.site = docsToSend.site))+'_' + (SELECT Replace(CONVERT(VARCHAR(20), DATEADD(DAY,-1,dateNextCall), 112),'/','')) )					-- infarmed mais data dos dados (dia anterior) --  + (SELECT Replace(CONVERT(VARCHAR(20), DATEADD(DAY,-1,dateNextCall), 112),'/',''))	
		 	WHEN 8 THEN		nameDoc  	+ '_' + '' + CONVERT(VARCHAR, dateNextCall, 112) + '_' + LEFT(REPLACE(CONVERT(VARCHAR, dateNextCall, 108), ':', ''),4)
			WHEN 9 THEN		nameDoc  	+ '_' +  (select convert(varchar(20),(select convert(varchar(10),infarmed) from empresa where empresa.site = docsToSend.site))) +'_' + (select top 1  CONVERT(varchar(10), CONVERT(datetime, value, 126), 112) value  from docsToSendParameters  where docsToSendParameters.site = docsToSend.site and name ='dataIni') +'_' + (select top 1 CONVERT(varchar(10), CONVERT(datetime, value, 126), 112) value  from docsToSendParameters  where docsToSendParameters.site = docsToSend.site and name ='dataFim')
		 ELSE 
				nameDoc
		 END) 																				 as nameDoc	
		,usp			
		,typeDocSave
		,typeSend
		,frequency
		,typeExec
		,dateNextCall
		,state
		,toSend
		,visibility
		,delimiter
		,zip
		,[subject]
		,(case when body!='' then body + CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10) else '' end ) + LEFT(ISNULL((select rodape  from #dadosRodape where #dadosRodape.site COLLATE SQL_Latin1_General_CP1_CI_AI = docsToSend.site COLLATE SQL_Latin1_General_CP1_CI_AI),''),2500) as body
		,toSendSec
		,tosendSecBcc
		,nameDocN
	FROM docsToSend (nolock)
	WHERE    (   (@typeExec = '' AND typeExec != 'ORDERS') OR
				 (@typeExec != '' AND typeExec = @typeExec))
		AND
		dateNextCall <= GETDATE() and state = 1
	order by site asc 


GO
GRANT EXECUTE ON dbo.usp_servico_ListaDocumentoEnviar to Public
GRANT CONTROL ON dbo.usp_servico_ListaDocumentoEnviar to Public
GO