IF( NOT EXISTS (SELECT * FROM B_analises (NOLOCK) WHERE ordem=201))
	BEGIN
		INSERT INTO [dbo].[B_analises]
			([ordem],[grupo],[subgrupo],[descricao]
			,[report],[stamp],[url],[comentario]
			,[ecra],[stamp_analiseav],[estado]
			,[tabela],[perfil],[filtros],[formatoExp]
			,[comandoFox],[objectivo],[marcada])
		VALUES
			(201,'Gestão','Farmácia','Relatório Situações Clínicas Ligeiras'
			,'','','','Obs.:'
			,'','',1
			,'','Relatório Situações Clínicas Ligeiras','',''
			,'do uf_analises_sitCli_exportion','',0)
	END


IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=201 and label='Data Início'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 201,[nome],[label],[tipo],[comando],1,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 1 and label='Data Início'
END

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=201 and label='Data Fim'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 201,[nome],[label],[tipo],[comando],2,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 1 and label='Data Fim'
END

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=201 and label='Loja'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 201,[nome],[label],[tipo],[comando],3,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],0,[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 1 and label='Loja'
END
