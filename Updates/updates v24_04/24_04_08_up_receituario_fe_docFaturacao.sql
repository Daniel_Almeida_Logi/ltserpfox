/* 
use F11378A
select * from empresa
--Farmácia: Farmácia São Martinho do Porto
--código: 10537
--Notas de crédito:
--Nr: 11 e 12
--Série do documento: 77


	exec [dbo].[up_receituario_fe_docFaturacao] 'MF0B466740-4014-4E3C-A03', 0, 99
	exec up_receituario_fe_docFaturacao 'ho8CF26AE8-0457-45AA-92F', '0', '99'

	delete faturacao_eletronica_med where aceite=0
	delete faturacao_eletronica_med_d where id_faturacao_eletronica_med not in (select id from faturacao_eletronica_med)

	select * from faturacao_eletronica_med
	select * from faturacao_eletronica_med_d where id_faturacao_eletronica_med = 'SPCasdsaC9-C74321453213'

	exec up_receituario_fe_docFaturacao 'ADM7C9F78E7-C7FE-4500-8B0', '0', '99'

	exec up_receituario_fe_docFaturacao 'ho397A54A6-F049-4C08-BBC', '0', '99'

*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_fe_docFaturacao]') IS NOT NULL
	drop procedure [up_receituario_fe_docFaturacao]
go

CREATE procedure [dbo].[up_receituario_fe_docFaturacao]
	@stamp varchar(25)
	,@tipoLoteIni int -- Não está a ser usado
	,@tipoLoteFim int -- Não está a ser usado

/* with encryption */

AS
SET NOCOUNT ON

	If OBJECT_ID('tempdb.dbo.#receitasFe') IS NOT NULL
		drop table #receitasFe;

		
	If OBJECT_ID('tempdb.dbo.#resultTemp') IS NOT NULL
		drop table #resultTemp;

	If OBJECT_ID('tempdb.dbo.#resultFinalTemp') IS NOT NULL
		drop table #resultFinalTemp;
	
	



	-- site p filtrar em selects mais abaixo
	declare @site as varchar(20)
	set @site = (select site from ft (nolock) where ftstamp = @stamp)


	-- Get codigo AT
	declare @codigoAt varchar(10)
	set @codigoAt = (select ltrim(rtrim(textValue)) from b_parameters (nolock) where stamp='ADM0000000078')

	
	-- Get tipo doc
	declare @tipodoc numeric(5)
	set @tipodoc = (select tipodoc from ft(nolock) where ftstamp=@stamp)


	-- Get stamp do documento de origem caso exista (caso notas de crédito/débito)
	declare @oftstamp varchar(25)
	set @oftstamp = case 
						when @tipodoc != 1 then (select top 1 ftstamp from fi(nolock) where fistamp in (select ofistamp from fi(nolock) fix where fix.ftstamp=@stamp))
						else null
						end


	-- save year, month and cptorgabrev
	declare @ano int, @mes int, @abrev varchar(15), @lote varchar(15) = ''
	select top 1
		@ano = ano, @mes = mes, @abrev = cptorgabrev, @lote = tlote
	from
		ctltrct (nolock)
	where
		u_fistamp in (select bistamp from fi (nolock) where ftstamp = case when @tipodoc=1 then @stamp else @oftstamp end)




	-- save original invoice data (if needed)
	declare @nrFatura numeric(10), @dataFatura datetime, @ndocFatura numeric(3), @anoFatura numeric(5)
	select
		@nrFatura = fno,
		@dataFatura = fdata,
		@anoFatura = ftano,
		@ndocFatura = ndoc
	from
		ft (nolock)
	where
		ftstamp = @oftstamp
	


	-- save cptorg data
	DECLARE @idEFR varchar(5), @cpt_org_abrev varchar(15), @id_cpt_org_anf varchar(3), @id_cpt_org varchar(9)
 	
	-- FT
	if(isnull(@oftstamp,'')='')
	begin
		select
			@idEFR = replace(cptorgstamp,'ADM','')
			,@cpt_org_abrev  = abrev
			,@id_cpt_org_anf = id_anf
			,@id_cpt_org     = isnull(id,'')
		from
			cptorg (nolock)
		left join b_utentes_site(nolock) on b_utentes_site.no = cptorg.u_no and b_utentes_site.site =   (select site from ft (nolock) where ftstamp=@stamp)
		where
			cptorg.abrev = (select id_efr_externa from ft2 (nolock) where ft2stamp=@stamp)	
	end else begin --NCs & NDs
		select
			@idEFR = replace(cptorgstamp,'ADM','')
			,@cpt_org_abrev  = abrev
			,@id_cpt_org_anf = id_anf
			,@id_cpt_org     = isnull(id,'')
		from
			cptorg (nolock)
		left join b_utentes_site(nolock) on b_utentes_site.no = cptorg.u_no and b_utentes_site.site =   (select site from ft (nolock) where ftstamp=@stamp)
		where
			cptorg.abrev = (select id_efr_externa from ft2 (nolock) where ft2stamp=@oftstamp)	
	end



	declare  @externalCompart  bit = 0;
	
	if(isnull(@cpt_org_abrev,'')!='')
	begin
		select 
			top 1 @externalCompart = isnull(cptpla.e_compart,0) 
		from 
			cptpla(nolock)
		where 
			cptpla.cptorgabrev = isnull(@cpt_org_abrev,'')
			and inactivo = 0
	

		set @externalCompart = isnull(@externalCompart,0)
	end
		
	/* Guardar receitas	*/
	select
		ctltrctstamp, lote, nreceita, ano, mes, cptorgabrev, u_fistamp, tlote, site, loteId = isnull(t.loteId,''), plano=c.cptplacode
	into
		#receitasFe
	from
		ctltrct c (nolock)
	left join tlote t(nolock) on t.codigo  = c.tlote
	where
		c.ano=@ano and c.mes=@mes
		and c.cptorgabrev=@abrev
		and c.u_fistamp!=''
		--and c.tlote=right(left(fi.design,7),2)
		and site = @site


	-- cria indice no ctltrctstamp (optimizacao)
	create index idx_temp_ctltrctstamp on #receitasFe(ctltrctstamp)	


	select
		ft.ndoc
		,ndoc_datamatrix = 'E'
		,ft.fno
		,nrFatura = isnull(@nrFatura,ft.fno)
					--case
					--	when ft.tipodoc = 1 then ft.fno
					--	else (select fno from ft ftx (nolock) where ftx.ftstamp = @oftstamp)
					--	end
		,ndocFatura = isnull(@ndocFatura,ft.ndoc)
		,anoFatura  = isnull(@anoFatura,ft.ftano)
		,ft.tipodoc
		,ft.fdata
		,dataFatura	= isnull(@dataFatura,'19000101')
						--case
						--when ft.tipodoc = 1 then '19000101'
						--else (select fdata from ft ftx (nolock) where ftx.ftstamp=@oftstamp)
						--end
		,obsdoc = (select obsdoc from ft2 (nolock) where ft2stamp=@stamp)
		,ft.ncont
		,etotal = abs(ft.etotal)
		,ivaTotal = abs(round(round(ft.eivav1,2) + round(ft.eivav2,2) + round(ft.eivav3,2) + round(ft.eivav4,2) + round(ft.eivav5,2) + round(ft.eivav6,2) + round(ft.eivav7,2) + round(ft.eivav8,2) + round(ft.eivav9,2),2))
		,eivainTotal = abs(round(round(ft.eivain1,2) + round(ft.eivain2,2) + round(ft.eivain3,2) + round(ft.eivain4,2) + round(ft.eivain5,2) + round(ft.eivain6,2) + round(ft.eivain7,2) + round(ft.eivain8,2) + round(ft.eivain9,2),2))
		,eivav1 = abs(round(ft.eivav1,2)), eivav2 = abs(round(ft.eivav2,2)), eivav3 = abs(round(ft.eivav3,2)), eivav4 = abs(round(ft.eivav4,2)), eivav5 = abs(round(ft.eivav5,2))
		,eivav6 = abs(round(ft.eivav6,2)), eivav7 = abs(round(ft.eivav7,2)), eivav8 = abs(round(ft.eivav8,2)), eivav9 = abs(round(ft.eivav9,2))
		,ivatx1 = abs(round(ft.ivatx1,2)), ivatx2 = abs(round(ft.ivatx2,2)), ivatx3 = abs(round(ft.ivatx3,2)), ivatx4 = abs(round(ft.ivatx4,2))
		,ivatx5 = abs(round(ft.ivatx5,2)), ivatx6 = abs(round(ft.ivatx6,2)), ivatx7 = abs(round(ft.ivatx7,2)), ivatx8 = abs(round(ft.ivatx8,2)), ivatx9 = abs(round(ft.ivatx9,2))
		,eivain1 = abs(round(ft.eivain1,2)), eivain2 = abs(round(ft.eivain2,2)), eivain3 = abs(round(ft.eivain3,2)), eivain4 = abs(round(ft.eivain4,2))
		,eivain5 = abs(round(ft.eivain5,2)), eivain6 = abs(round(ft.eivain6,2)), eivain7 = abs(round(ft.eivain7,2)), eivain8 = abs(round(ft.eivain8,2)), eivain9 = abs(round(ft.eivain9,2))

		,fi.design
		,fi.qtt
		--,iva = case -- Condição para evitar duplicação de linhas quando a farmácia comparticipa um produto com 0% de iva
		--		when ft.eivain1>0 then (select taxa from taxasiva (nolock) where codigo=1)
		--		when ft.eivain2>0 then (select taxa from taxasiva (nolock) where codigo=2)
		--		when ft.eivain3>0 then (select taxa from taxasiva (nolock) where codigo=3)
		--		when ft.eivain4>0 then (select taxa from taxasiva (nolock) where codigo=4)
		--		when ft.eivain5>0 then (select taxa from taxasiva (nolock) where codigo=5)
		--		when ft.eivain6>0 then (select taxa from taxasiva (nolock) where codigo=6)
		--		when ft.eivain7>0 then (select taxa from taxasiva (nolock) where codigo=7)
		--		when ft.eivain8>0 then (select taxa from taxasiva (nolock) where codigo=8)
		--		when ft.eivain9>0 then (select taxa from taxasiva (nolock) where codigo=9)
		--		end
		,etiliquido = abs(sum(fi.etiliquido))
		,eivain = abs(sum(round(fi.etiliquido / (fi.iva/100+1),2)))
		,eiva = abs(sum(round(fi.etiliquido - (fi.etiliquido / (fi.iva/100+1)),2)))
		,pvp = abs(sum(altura))
		,utente = abs(sum(largura))
		,pvp4_fee = abs(sum(fi.pvp4_fee))
		,lotes = case
					when ft.tipodoc!=1 then 1
					else (select count(distinct case when  plano='RS' then 99 else c.lote end) from #receitasFe c (nolock) where c.loteId=right(left(fi.design,7),2))
							--where c.ano=@ano and c.mes=@mes and c.cptorgabrev=@abrev and c.u_fistamp!='' and c.tlote=right(left(fi.design,7),2) and site = @site)
					end
		,receitas = (select count(c.nreceita) from #receitasFe c (nolock) where c.loteId=right(left(fi.design,7),2))
						--where c.ano=@ano and c.mes=@mes and c.cptorgabrev=@abrev and c.u_fistamp!='' and c.tlote=right(left(fi.design,7),2) and site = @site)
		,totalMedicamentos = isnull(
								(select sum(fix.qtt)
								from #receitasFe c (nolock) 
								inner join ft ftx (nolock) on ftx.u_ltstamp = c.ctltrctstamp or ftx.u_ltstamp2 = c.ctltrctstamp 
								inner join fi fix (nolock) on fix.ftstamp = ftx.ftstamp
								where c.loteId=right(left(fi.design,7),2) and (fix.u_ettent1>0 or fix.u_ettent2>0)
								)
							
								
								--where c.ano=@ano and c.mes=@mes and c.cptorgabrev=@abrev and c.u_fistamp!='' and c.tlote=right(left(fi.design,7),2) and c.site = @site)
								,0)
		,idEFR = @idEFR
		,id_cpt_org = @cpt_org_abrev
		,id_cpt_org_anf = @id_cpt_org_anf
		,externalCompart = @externalCompart
		,ft.nome
		,ft.morada
		,ft.codpost
		,ft.local
		,codigoAT = @codigoAt
		,hash = SUBSTRING(b_cert.hash,1,1) + SUBSTRING(b_cert.hash,11,1) + SUBSTRING(b_cert.hash,21,1) + SUBSTRING(b_cert.hash,31,1)
		,anoFact = @ano
		,mesFact = @mes
		,ft.nmdoc
		,lote = @lote
		,docAno = ft.ftano
		,id_org = isnull(@id_cpt_org,'')	
	into
		#resultTemp	
	from
		ft (nolock)
		inner join ft2 (nolock) on ft2.ft2stamp = ft.ftstamp
		inner join fi (nolock) on fi.ftstamp = ft.ftstamp
		left join fprod (nolock) on fprod.cnp = fi.ref
		inner join b_cert (nolock) on b_cert.stamp = ft.ftstamp
	where
		ft.ftstamp = @stamp
	group by -- Condição para evitar duplicação de linhas quando a farmácia comparticipa um produto com 0% de iva
		ft2.ltstamp,ft.ndoc, ft.fno, ft.tipodoc, ft.fdata, ft.ncont, ft.etotal,  ft.nome, ft.morada, ft.codpost, ft.local
		,ft.eivav1, ft.eivav2, ft.eivav3, ft.eivav4, ft.eivav5, ft.eivav6, ft.eivav7, ft.eivav8, ft.eivav9
		,ft.eivain1, ft.eivain2, ft.eivain3, ft.eivain4, ft.eivain5, ft.eivain6, ft.eivain7, ft.eivain8, ft.eivain9
		,ft.eivav1, ft.eivav2, ft.eivav3, ft.eivav4, ft.eivav5, ft.eivav6, ft.eivav7, ft.eivav8, ft.eivav9
		,ft.ivatx1, ft.ivatx2, ft.ivatx3, ft.ivatx4, ft.ivatx5, ft.ivatx6, ft.ivatx7, ft.ivatx8, ft.ivatx9,ft.nmdoc
		,fi.design, fi.qtt
		,b_cert.hash,ft.ftano
	order by
		fi.design


	if(rtrim(ltrim(isnull(@cpt_org_abrev,'')))='SNS')
	begin
		/*agrupa lotes repetidos com taxas de iva diferentes - pedido pelo SPMS*/
		select 
			ROW_NUMBER() OVER(PARTITION BY right(left(res.design,7),2)  ORDER BY right(left(res.design,7),2)  ASC) AS numeroLinha
			,*  
		into 
			#resultFinalTemp
		from 
			#resultTemp res


		update 
			#resultFinalTemp
		set
			pvp =   isnull((select sum(isnull(res.pvp,0)) from #resultFinalTemp res where right(left(res.design,7),2) =  right(left(#resultFinalTemp.design,7),2) ),0)
			,utente =   isnull((select sum(isnull(res.utente,0)) from #resultFinalTemp res where  right(left(res.design,7),2) =  right(left(#resultFinalTemp.design,7),2) ),0)
			,pvp4_fee =  isnull((select sum(isnull(res.pvp4_fee,0)) from #resultFinalTemp res where  right(left(res.design,7),2) =  right(left(#resultFinalTemp.design,7),2) ),0)
			,etiliquido =  isnull((select sum(isnull(res.etiliquido,0)) from #resultFinalTemp res where  right(left(res.design,7),2) =  right(left(#resultFinalTemp.design,7),2) ),0)
		
		select  
			* 
		from 
			#resultFinalTemp res
		where 
			res.numeroLinha = 1

	end else begin
		select 
			*
		from 
			#resultTemp	
	end



	If OBJECT_ID('tempdb.dbo.#resultFinalTemp') IS NOT NULL
		drop table #resultFinalTemp;
	
			
	If OBJECT_ID('tempdb.dbo.#resultTemp') IS NOT NULL
		drop table #resultTemp;


	
	If OBJECT_ID('tempdb.dbo.#receitasFe') IS NOT NULL
		drop table #receitasFe;

GO
Grant Execute On [up_receituario_fe_docFaturacao] to Public
Grant Control On [up_receituario_fe_docFaturacao] to Public
GO