/*
	marcar linhas enviadas
	
	exec up_encomendas_orders_detalheFalta_marcar_enviadas '','20240728','20240728',1,'Sucesso','5440987','Loja 1'
	exec up_encomendas_orders_detalheFalta_marcar_enviadas '','20240724','20240724',0, 'Erro','5440987','Loja 1'

	
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_encomendas_orders_detalheFalta_marcar_enviadas]') IS NOT NULL
	drop procedure dbo.up_encomendas_orders_detalheFalta_marcar_enviadas
go

create procedure dbo.up_encomendas_orders_detalheFalta_marcar_enviadas
	@stamp			VARCHAR (50),
	@dataini		DATETIME,
	@datafim		DATETIME,
	@resultCode     int,
	@resultDescr    varchar(MAX),
	@ref			VARCHAR (18),
	@site           VARCHAR (20)


/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	DECLARE @sql VARCHAR(4000) = ''

	SET @sql = @sql + N'
		update bi
			set
				comunicada_ws = ''' + convert(varchar,@resultCode) + ''',
				comunicada_ws_descr  = ''' + ltrim(rtrim(isnull(@resultDescr,''))) + '''
		FROM
			bi (NOLOCK)
			INNER JOIN ts ON ts.ndos = bi.ndos
			inner join bo(nolock) on bo.bostamp = bi.bostamp
		WHERE
			bi.ref = ''' + ltrim(rtrim(@ref)) + '''	
			AND ts.codigoDoc in (''50'',''51'') --registo de faltas, reservas de cliente
			AND bo.site = '''+@site+'''
			'
	
	IF @stamp != ''
		SET @sql = @sql + N' AND bi.bostamp = ''' + @stamp + ''' '

	IF @dataini != '' AND @datafim != ''
		SET @sql = @sql + N' AND bi.ousrdata BETWEEN '''+convert(varchar,@dataini)+''' AND
							 '''+convert(varchar,@datafim)+''''

	
	
	PRINT @sql
	EXEC(@sql)

GO
Grant Execute on dbo.up_encomendas_orders_detalheFalta_marcar_enviadas to Public
Grant Control on dbo.up_encomendas_orders_detalheFalta_marcar_enviadas to Public
GO


