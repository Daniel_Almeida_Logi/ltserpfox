


/****** Object:  Table [dbo].[quest_pergunta_resp]    Script Date: 26/08/2024 14:22:42 ******/
DROP TABLE [dbo].[quest_pergunta_resp]
GO

/****** Object:  Table [dbo].[quest_pergunta_resp]    Script Date: 26/08/2024 14:22:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[quest_pergunta_resp](
	[Quest_pergunta_respStamp] [varchar](36) NOT NULL,
	[Quest_perguntaStamp] [varchar](36) NOT NULL,
	[resposta] [varchar](100) NOT NULL,
	[ordem] [int] NOT NULL,
	[quest_pergunta_resp_tpStamp] [varchar](36) NOT NULL,
	[ousrdata] [datetime] NOT NULL,
	[ousrinis] [varchar](30) NOT NULL,
	[usrdata] [datetime] NOT NULL,
	[usrinis] [varchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Quest_pergunta_respStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [C_quest_pergunta_resp_unique] UNIQUE NONCLUSTERED 
(
	[Quest_pergunta_respStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[quest_pergunta_resp] ADD  DEFAULT ('') FOR [Quest_pergunta_respStamp]
GO

ALTER TABLE [dbo].[quest_pergunta_resp] ADD  DEFAULT ('') FOR [Quest_perguntaStamp]
GO

ALTER TABLE [dbo].[quest_pergunta_resp] ADD  DEFAULT ('') FOR [resposta]
GO

ALTER TABLE [dbo].[quest_pergunta_resp] ADD  DEFAULT ((0)) FOR [ordem]
GO

ALTER TABLE [dbo].[quest_pergunta_resp] ADD  DEFAULT ('') FOR [quest_pergunta_resp_tpStamp]
GO

ALTER TABLE [dbo].[quest_pergunta_resp] ADD  DEFAULT (getdate()) FOR [ousrdata]
GO

ALTER TABLE [dbo].[quest_pergunta_resp] ADD  DEFAULT ('') FOR [ousrinis]
GO

ALTER TABLE [dbo].[quest_pergunta_resp] ADD  DEFAULT (getdate()) FOR [usrdata]
GO

ALTER TABLE [dbo].[quest_pergunta_resp] ADD  DEFAULT ('') FOR [usrinis]
GO


