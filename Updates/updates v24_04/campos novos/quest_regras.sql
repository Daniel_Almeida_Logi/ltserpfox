
/****** Object:  Table [dbo].[quest_regras]    Script Date: 26/08/2024 14:23:05 ******/
DROP TABLE [dbo].[quest_regras]
GO

/****** Object:  Table [dbo].[quest_regras]    Script Date: 26/08/2024 14:23:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[quest_regras](
	[quest_regrasStamp] [varchar](36) NOT NULL,
	[questStamp] [varchar](36) NOT NULL,
	[quest_perguntaStamp] [varchar](36) NOT NULL,
	[quest_pergunta_respStamp] [varchar](36) NOT NULL,
	[quest_pergunta_respStamp_dependente] [varchar](36) NOT NULL,
	[Prox_Ordem] [int] NOT NULL,
	[ousrdata] [datetime] NOT NULL,
	[ousrinis] [varchar](30) NOT NULL,
	[usrdata] [datetime] NOT NULL,
	[usrinis] [varchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[quest_regrasStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [C_quest_resgras_unique] UNIQUE NONCLUSTERED 
(
	[questStamp] ASC,
	[quest_regrasStamp] ASC,
	[quest_perguntaStamp] ASC,
	[quest_pergunta_respStamp] ASC,
	[Prox_Ordem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[quest_regras] ADD  DEFAULT ('') FOR [quest_regrasStamp]
GO

ALTER TABLE [dbo].[quest_regras] ADD  DEFAULT ('') FOR [questStamp]
GO

ALTER TABLE [dbo].[quest_regras] ADD  DEFAULT ('') FOR [quest_perguntaStamp]
GO

ALTER TABLE [dbo].[quest_regras] ADD  DEFAULT ('') FOR [quest_pergunta_respStamp]
GO

ALTER TABLE [dbo].[quest_regras] ADD  DEFAULT ('') FOR [quest_pergunta_respStamp_dependente]
GO

ALTER TABLE [dbo].[quest_regras] ADD  DEFAULT ((0)) FOR [Prox_Ordem]
GO

ALTER TABLE [dbo].[quest_regras] ADD  DEFAULT (getdate()) FOR [ousrdata]
GO

ALTER TABLE [dbo].[quest_regras] ADD  DEFAULT ('') FOR [ousrinis]
GO

ALTER TABLE [dbo].[quest_regras] ADD  DEFAULT (getdate()) FOR [usrdata]
GO

ALTER TABLE [dbo].[quest_regras] ADD  DEFAULT ('') FOR [usrinis]
GO


