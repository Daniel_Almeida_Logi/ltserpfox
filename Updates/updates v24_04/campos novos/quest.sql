

/****** Object:  Table [dbo].[Quest]    Script Date: 26/08/2024 14:21:26 ******/
DROP TABLE [dbo].[Quest]
GO

/****** Object:  Table [dbo].[Quest]    Script Date: 26/08/2024 14:21:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Quest](
	[QuestStamp] [varchar](36) NOT NULL,
	[ID] [int] NOT NULL,
	[Descr] [varchar](100) NOT NULL,
	[ousrdata] [datetime] NOT NULL,
	[ousrinis] [varchar](30) NOT NULL,
	[usrdata] [datetime] NOT NULL,
	[usrinis] [varchar](30) NOT NULL,
	[Inativo] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[QuestStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [C_quest_unique] UNIQUE NONCLUSTERED 
(
	[QuestStamp] ASC,
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Quest] ADD  DEFAULT ('') FOR [QuestStamp]
GO

ALTER TABLE [dbo].[Quest] ADD  DEFAULT ((0)) FOR [ID]
GO

ALTER TABLE [dbo].[Quest] ADD  DEFAULT ('') FOR [Descr]
GO

ALTER TABLE [dbo].[Quest] ADD  DEFAULT (getdate()) FOR [ousrdata]
GO

ALTER TABLE [dbo].[Quest] ADD  DEFAULT ('') FOR [ousrinis]
GO

ALTER TABLE [dbo].[Quest] ADD  DEFAULT (getdate()) FOR [usrdata]
GO

ALTER TABLE [dbo].[Quest] ADD  DEFAULT ('') FOR [usrinis]
GO

ALTER TABLE [dbo].[Quest] ADD  DEFAULT ((0)) FOR [Inativo]
GO


