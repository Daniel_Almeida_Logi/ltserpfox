


/****** Object:  Table [dbo].[quest_pergunta]    Script Date: 26/08/2024 14:22:07 ******/
DROP TABLE [dbo].[quest_pergunta]
GO

/****** Object:  Table [dbo].[quest_pergunta]    Script Date: 26/08/2024 14:22:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[quest_pergunta](
	[quest_perguntastamp] [varchar](36) NOT NULL,
	[queststamp] [varchar](36) NOT NULL,
	[pergunta] [varchar](150) NOT NULL,
	[ordem] [int] NOT NULL,
	[ousrdata] [datetime] NOT NULL,
	[ousrinis] [varchar](30) NOT NULL,
	[usrdata] [datetime] NOT NULL,
	[usrinis] [varchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[quest_perguntastamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [C_quest_pergunta_unique] UNIQUE NONCLUSTERED 
(
	[quest_perguntastamp] ASC,
	[ordem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[quest_pergunta] ADD  DEFAULT ('') FOR [quest_perguntastamp]
GO

ALTER TABLE [dbo].[quest_pergunta] ADD  DEFAULT ('') FOR [queststamp]
GO

ALTER TABLE [dbo].[quest_pergunta] ADD  DEFAULT ('') FOR [pergunta]
GO

ALTER TABLE [dbo].[quest_pergunta] ADD  DEFAULT ((0)) FOR [ordem]
GO

ALTER TABLE [dbo].[quest_pergunta] ADD  DEFAULT (getdate()) FOR [ousrdata]
GO

ALTER TABLE [dbo].[quest_pergunta] ADD  DEFAULT ('') FOR [ousrinis]
GO

ALTER TABLE [dbo].[quest_pergunta] ADD  DEFAULT (getdate()) FOR [usrdata]
GO

ALTER TABLE [dbo].[quest_pergunta] ADD  DEFAULT ('') FOR [usrinis]
GO


