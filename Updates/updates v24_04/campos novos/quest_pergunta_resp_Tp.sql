
/****** Object:  Table [dbo].[quest_pergunta_resp_tp]    Script Date: 26/08/2024 14:22:53 ******/
DROP TABLE [dbo].[quest_pergunta_resp_tp]
GO

/****** Object:  Table [dbo].[quest_pergunta_resp_tp]    Script Date: 26/08/2024 14:22:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[quest_pergunta_resp_tp](
	[quest_pergunta_resp_tpStamp] [varchar](36) NOT NULL,
	[ID] [int] NOT NULL,
	[Descr] [varchar](100) NOT NULL,
	[ousrdata] [datetime] NOT NULL,
	[ousrinis] [varchar](30) NOT NULL,
	[usrdata] [datetime] NOT NULL,
	[usrinis] [varchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[quest_pergunta_resp_tpStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [C_quest_pergunta_resp_tp_unique] UNIQUE NONCLUSTERED 
(
	[quest_pergunta_resp_tpStamp] ASC,
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[quest_pergunta_resp_tp] ADD  DEFAULT ('') FOR [quest_pergunta_resp_tpStamp]
GO

ALTER TABLE [dbo].[quest_pergunta_resp_tp] ADD  DEFAULT ((0)) FOR [ID]
GO

ALTER TABLE [dbo].[quest_pergunta_resp_tp] ADD  DEFAULT ('') FOR [Descr]
GO

ALTER TABLE [dbo].[quest_pergunta_resp_tp] ADD  DEFAULT (getdate()) FOR [ousrdata]
GO

ALTER TABLE [dbo].[quest_pergunta_resp_tp] ADD  DEFAULT ('') FOR [ousrinis]
GO

ALTER TABLE [dbo].[quest_pergunta_resp_tp] ADD  DEFAULT (getdate()) FOR [usrdata]
GO

ALTER TABLE [dbo].[quest_pergunta_resp_tp] ADD  DEFAULT ('') FOR [usrinis]
GO


