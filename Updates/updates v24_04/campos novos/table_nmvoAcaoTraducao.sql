IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'nmvoAcaoTraducao'))
BEGIN
	drop table nmvoAcaoTraducao
END

CREATE TABLE [dbo].[nmvoAcaoTraducao](
	[stamp] [varchar](36) NOT NULL,
	[codePais] [varchar](24) NOT NULL,
	[design] [varchar](254) NOT NULL,
	[codigoAcao] [varchar](254) NOT NULL,	
	[ousrinis] [varchar](30) NOT NULL,
	[ousrdata] [datetime] NOT NULL,
	[ousrhora] [varchar](8) NOT NULL,
	[usrinis] [varchar](30) NOT NULL,
	[usrdata] [datetime] NOT NULL,
	[usrhora] [varchar](8) NOT NULL,

 CONSTRAINT [PK_nmvoAcaoTraducao_stamp] PRIMARY KEY NONCLUSTERED 
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO


IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[nmvoAcaoTraducao]') AND name = N'IX_nmvoAcaoTraducao_codigoAcao')
CREATE NONCLUSTERED INDEX [IX_nmvoAcaoTraducao_codigoAcao] ON [dbo].[nmvoAcaoTraducao]
(
	[codigoAcao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE [dbo].[nmvoAcaoTraducao] ADD  DEFAULT ('') FOR [stamp]
ALTER TABLE [dbo].[nmvoAcaoTraducao] ADD  DEFAULT ('') FOR [codePais]
ALTER TABLE [dbo].[nmvoAcaoTraducao] ADD  DEFAULT ('') FOR design
ALTER TABLE [dbo].[nmvoAcaoTraducao] ADD  DEFAULT ('') FOR codigoAcao
ALTER TABLE [dbo].[nmvoAcaoTraducao] ADD  DEFAULT ('') FOR ousrinis
ALTER TABLE [dbo].[nmvoAcaoTraducao] ADD  DEFAULT (getdate()) FOR ousrdata
ALTER TABLE [dbo].[nmvoAcaoTraducao] ADD  DEFAULT ('') FOR ousrhora
ALTER TABLE [dbo].[nmvoAcaoTraducao] ADD  DEFAULT ('') FOR usrinis
ALTER TABLE [dbo].[nmvoAcaoTraducao] ADD  DEFAULT (getdate()) FOR usrdata
ALTER TABLE [dbo].[nmvoAcaoTraducao] ADD  DEFAULT (getdate()) FOR usrhora







INSERT INTO [dbo].[nmvoAcaoTraducao]
			([stamp],[codePais]
			,[design]
			,[codigoAcao]
			,[ousrinis]
			,[ousrdata]
			,[ousrhora]
			,[usrinis]
			,[usrdata]
			,[usrhora])
     VALUES
           (LEFT(newid(),36)
           ,'Portugal'
           ,'Dispensa'
           ,'DISPENSE'
           ,'VT'
           ,GETDATE()
           ,FORMAT(GETDATE(), 'HH:mm:ss')
           ,'VT'
           ,GETDATE()
           ,FORMAT(GETDATE(), 'HH:mm:ss'))

INSERT INTO [dbo].[nmvoAcaoTraducao]
			([stamp],[codePais]
			,[design]
			,[codigoAcao]
			,[ousrinis]
			,[ousrdata]
			,[ousrhora]
			,[usrinis]
			,[usrdata]
			,[usrhora])
     VALUES
           (LEFT(newid(),36)
           ,'Portugal'
           ,'Dispensa manual'
           ,'DISPENSE_MANUAL'
           ,'VT'
           ,GETDATE()
           ,FORMAT(GETDATE(), 'HH:mm:ss')
           ,'VT'
           ,GETDATE()
           ,FORMAT(GETDATE(), 'HH:mm:ss'))

INSERT INTO [dbo].[nmvoAcaoTraducao]
	([stamp],[codePais]
	,[design]
	,[codigoAcao]
	,[ousrinis]
	,[ousrdata]
	,[ousrhora]
	,[usrinis]
	,[usrdata]
	,[usrhora])
VALUES
    (LEFT(newid(),36)
    ,'Portugal'
    ,'Reativação'
    ,'UNDODISPENSE'
    ,'VT'
    ,GETDATE()
    ,FORMAT(GETDATE(), 'HH:mm:ss')
    ,'VT'
    ,GETDATE()
    ,FORMAT(GETDATE(), 'HH:mm:ss'))

		INSERT INTO [dbo].[nmvoAcaoTraducao]
	([stamp],[codePais]
	,[design]
	,[codigoAcao]
	,[ousrinis]
	,[ousrdata]
	,[ousrhora]
	,[usrinis]
	,[usrdata]
	,[usrhora])
VALUES
    (LEFT(newid(),36)
    ,'Portugal'
    ,'Verificação'
    ,'VERIFY'
    ,'VT'
    ,GETDATE()
    ,FORMAT(GETDATE(), 'HH:mm:ss')
    ,'VT'
    ,GETDATE()
    ,FORMAT(GETDATE(), 'HH:mm:ss'))

		INSERT INTO [dbo].[nmvoAcaoTraducao]
	([stamp],[codePais]
	,[design]
	,[codigoAcao]
	,[ousrinis]
	,[ousrdata]
	,[ousrhora]
	,[usrinis]
	,[usrdata]
	,[usrhora])
VALUES
    (LEFT(newid(),36)
    ,'Portugal'
    ,'Verificação manual'
    ,'VERIFY_MANUAL'
    ,'VT'
    ,GETDATE()
    ,FORMAT(GETDATE(), 'HH:mm:ss')
    ,'VT'
    ,GETDATE()
    ,FORMAT(GETDATE(), 'HH:mm:ss'))

INSERT INTO [dbo].[nmvoAcaoTraducao]
	([stamp],[codePais]
	,[design]
	,[codigoAcao]
	,[ousrinis]
	,[ousrdata]
	,[ousrhora]
	,[usrinis]
	,[usrdata]
	,[usrhora])
VALUES
    (LEFT(newid(),36)
    ,'Portugal'
    ,'Destruição'
    ,'DESTROY'
    ,'VT'
    ,GETDATE()
    ,FORMAT(GETDATE(), 'HH:mm:ss')
    ,'VT'
    ,GETDATE()
    ,FORMAT(GETDATE(), 'HH:mm:ss'))

 INSERT INTO [dbo].[nmvoAcaoTraducao]
			([stamp],[codePais]
			,[design]
			,[codigoAcao]
			,[ousrinis]
			,[ousrdata]
			,[ousrhora]
			,[usrinis]
			,[usrdata]
			,[usrhora])
     VALUES
           (LEFT(newid(),36)
           ,'Portugal'
           ,'Amostragem'
           ,'SAMPLE'
           ,'VT'
           ,GETDATE()
           ,FORMAT(GETDATE(), 'HH:mm:ss')
           ,'VT'
           ,GETDATE()
           ,FORMAT(GETDATE(), 'HH:mm:ss'))

INSERT INTO [dbo].[nmvoAcaoTraducao]
([stamp],[codePais]
,[design]
,[codigoAcao]
,[ousrinis]
,[ousrdata]
,[ousrhora]
,[usrinis]
,[usrdata]
,[usrhora])
 VALUES
    (LEFT(newid(),36)
    ,'Portugal'
    ,'Anulação de amostra'
    ,'UNDOSAMPLE'
    ,'VT'
    ,GETDATE()
    ,FORMAT(GETDATE(), 'HH:mm:ss')
    ,'VT'
    ,GETDATE()
    ,FORMAT(GETDATE(), 'HH:mm:ss'))


