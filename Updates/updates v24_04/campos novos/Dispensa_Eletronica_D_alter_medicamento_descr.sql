

DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'Dispensa_Eletronica_D'
	,@columnName		= 'medicamento_descr'
	,@columnType		= 'varchar(1000)'
	,@columnDefault		= NULL
	
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
			and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end
	
	if @columnDefault is null
	begin
		exec('ALTER TABLE dbo.' + @tableName + ' alter column ' + @columnName + ' ' + @columnType)
	end else begin
		exec('ALTER TABLE dbo.' + @tableName + ' alter column ' + @columnName + ' ' + @columnType + ' not null ')
		exec('ALTER TABLE dbo.' + @tableName + ' add constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault + ' for ' + @columnName)
	end
end

go
