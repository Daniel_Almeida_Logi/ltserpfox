/****** Object:  Table [dbo].[quest_respostas]    Script Date: 18/09/2024 16:27:29 ******/
DROP TABLE [dbo].[quest_respostas]
GO

/****** Object:  Table [dbo].[quest_respostas]    Script Date: 18/09/2024 16:27:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[quest_respostas](
	[quest_respostasStamp] [varchar](36) NOT NULL,
	[questStamp] [varchar](36) NOT NULL,
	[quest_respostas_grpStamp] [varchar](36) NOT NULL,
	[quest_perguntaStamp] [varchar](36) NOT NULL,
	[fistamp] [varchar](36) NOT NULL,
	[resposta] [varchar](254) NOT NULL,
	[ref] [varchar](50) NOT NULL,
	[no] [int] NOT NULL,
	[sexo] [varchar](1) NOT NULL,
	[faixaEtaria] [varchar](20) NOT NULL,
	[operador] [varchar](10) NOT NULL,
	[site] [varchar](60) NOT NULL,
	[ousrdata] [datetime] NOT NULL,
	[ousrhora] [varchar](8) NOT NULL,
	[ousrinis] [varchar](30) NOT NULL,
	[usrdata] [datetime] NOT NULL,
	[usrinis] [varchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[quest_respostasStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [C_quest_respostas_unique] UNIQUE NONCLUSTERED 
(
	[quest_respostasStamp] ASC,
	[questStamp] ASC,
	[quest_respostas_grpStamp] ASC,
	[quest_perguntaStamp] ASC,


	[resposta] ASC,
	[fistamp] ASC,
	[ref] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[quest_respostas] ADD  DEFAULT ('') FOR [quest_respostasStamp]
GO

ALTER TABLE [dbo].[quest_respostas] ADD  DEFAULT ('') FOR [questStamp]
GO

ALTER TABLE [dbo].[quest_respostas] ADD  DEFAULT ('') FOR [quest_respostas_grpStamp]
GO

ALTER TABLE [dbo].[quest_respostas] ADD  DEFAULT ('') FOR [quest_perguntaStamp]
GO

ALTER TABLE [dbo].[quest_respostas] ADD  DEFAULT ('') FOR [fistamp]
GO

ALTER TABLE [dbo].[quest_respostas] ADD  DEFAULT ('') FOR [resposta]
GO

ALTER TABLE [dbo].[quest_respostas] ADD  DEFAULT ('') FOR [ref]
GO

ALTER TABLE [dbo].[quest_respostas] ADD  DEFAULT ((0)) FOR [no]
GO

ALTER TABLE [dbo].[quest_respostas] ADD  DEFAULT ('') FOR [sexo]
GO

ALTER TABLE [dbo].[quest_respostas] ADD  DEFAULT ('') FOR [faixaEtaria]
GO

ALTER TABLE [dbo].[quest_respostas] ADD  DEFAULT ('') FOR [operador]
GO

ALTER TABLE [dbo].[quest_respostas] ADD  DEFAULT ('') FOR [site]
GO

ALTER TABLE [dbo].[quest_respostas] ADD  DEFAULT (getdate()) FOR [ousrdata]
GO

ALTER TABLE [dbo].[quest_respostas] ADD  DEFAULT ('') FOR [ousrhora]
GO

ALTER TABLE [dbo].[quest_respostas] ADD  DEFAULT ('') FOR [ousrinis]
GO

ALTER TABLE [dbo].[quest_respostas] ADD  DEFAULT (getdate()) FOR [usrdata]
GO

ALTER TABLE [dbo].[quest_respostas] ADD  DEFAULT ('') FOR [usrinis]
GO




Drop table [Quest]

Drop table [quest_pergunta]

Drop table [quest_pergunta_resp]

Drop table quest_pergunta_resp_tp

drop table quest_regras

/****** Object:  Table [dbo].[Quest]    Script Date: 17/09/2024 11:39:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quest](
	[QuestStamp] [varchar](36) NOT NULL,
	[ID] [int] NOT NULL,
	[Descr] [varchar](100) NOT NULL,
	[DadosCli] [bit] NOT NULL,
	[ousrdata] [datetime] NOT NULL,
	[ousrinis] [varchar](30) NOT NULL,
	[usrdata] [datetime] NOT NULL,
	[usrinis] [varchar](30) NOT NULL,
	[Inativo] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[QuestStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[quest_pergunta]    Script Date: 17/09/2024 11:39:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[quest_pergunta](
	[quest_perguntastamp] [varchar](36) NOT NULL,
	[queststamp] [varchar](36) NOT NULL,
	[pergunta] [varchar](150) NOT NULL,
	[ordem] [int] NOT NULL,
	[ousrdata] [datetime] NOT NULL,
	[ousrinis] [varchar](30) NOT NULL,
	[usrdata] [datetime] NOT NULL,
	[usrinis] [varchar](30) NOT NULL,
	[permiteMulti] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[quest_perguntastamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[quest_pergunta_resp]    Script Date: 17/09/2024 11:39:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[quest_pergunta_resp](
	[Quest_pergunta_respStamp] [varchar](36) NOT NULL,
	[Quest_perguntaStamp] [varchar](36) NOT NULL,
	[resposta] [varchar](100) NOT NULL,
	[ordem] [int] NOT NULL,
	[quest_pergunta_resp_tpStamp] [varchar](36) NOT NULL,
	[ousrdata] [datetime] NOT NULL,
	[ousrinis] [varchar](30) NOT NULL,
	[usrdata] [datetime] NOT NULL,
	[usrinis] [varchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Quest_pergunta_respStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[quest_pergunta_resp_tp]    Script Date: 17/09/2024 11:39:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[quest_pergunta_resp_tp](
	[quest_pergunta_resp_tpStamp] [varchar](36) NOT NULL,
	[ID] [int] NOT NULL,
	[Descr] [varchar](100) NOT NULL,
	[ousrdata] [datetime] NOT NULL,
	[ousrinis] [varchar](30) NOT NULL,
	[usrdata] [datetime] NOT NULL,
	[usrinis] [varchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[quest_pergunta_resp_tpStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[quest_regras]    Script Date: 17/09/2024 11:39:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[quest_regras](
	[quest_regrasStamp] [varchar](36) NOT NULL,
	[questStamp] [varchar](36) NOT NULL,
	[quest_perguntaStamp] [varchar](36) NOT NULL,
	[quest_pergunta_respStamp] [varchar](36) NOT NULL,
	[quest_pergunta_respStamp_dependente] [varchar](36) NOT NULL,
	[Prox_Ordem] [int] NOT NULL,
	[ousrdata] [datetime] NOT NULL,
	[ousrinis] [varchar](30) NOT NULL,
	[usrdata] [datetime] NOT NULL,
	[usrinis] [varchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[quest_regrasStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Quest] ([QuestStamp], [ID], [Descr], [DadosCli], [ousrdata], [ousrinis], [usrdata], [usrinis], [Inativo]) VALUES (N'230AE1CA-D832-49E4-8D73-E', 2, N'Situacoes Clinicas', 1, CAST(N'2024-06-11T17:01:08.620' AS DateTime), N'', CAST(N'2024-06-11T17:01:08.620' AS DateTime), N'', 0)
GO
INSERT [dbo].[Quest] ([QuestStamp], [ID], [Descr], [DadosCli], [ousrdata], [ousrinis], [usrdata], [usrinis], [Inativo]) VALUES (N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', 1, N'IQVIA-ASMA/DPOC', 0, CAST(N'2024-04-11T11:52:19.970' AS DateTime), N'', CAST(N'2024-04-11T11:52:19.970' AS DateTime), N'', 0)
GO
INSERT [dbo].[quest_pergunta] ([quest_perguntastamp], [queststamp], [pergunta], [ordem], [ousrdata], [ousrinis], [usrdata], [usrinis], [permiteMulti]) VALUES (N'0EED4FE7-215D-449A-99F7-6A07B42F900E', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'Nos últimos 12 meses, quantas vezes foi à urgência devido à DPOC?', 5, CAST(N'2024-04-11T11:56:15.187' AS DateTime), N'', CAST(N'2024-04-11T11:56:15.187' AS DateTime), N'', 0)
GO
INSERT [dbo].[quest_pergunta] ([quest_perguntastamp], [queststamp], [pergunta], [ordem], [ousrdata], [ousrinis], [usrdata], [usrinis], [permiteMulti]) VALUES (N'27E1D2A6-17E2-4237-9D80-1C90AF0B183B', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'Nos últimos 12 meses, quantos inaladores comprou?', 2, CAST(N'2024-04-11T11:55:32.317' AS DateTime), N'', CAST(N'2024-04-11T11:55:32.317' AS DateTime), N'', 0)
GO
INSERT [dbo].[quest_pergunta] ([quest_perguntastamp], [queststamp], [pergunta], [ordem], [ousrdata], [ousrinis], [usrdata], [usrinis], [permiteMulti]) VALUES (N'2B442423-878C-4424-8113-9', N'230AE1CA-D832-49E4-8D73-E', N'Origem Aconselhamento:', 2, CAST(N'2024-06-25T17:22:18.757' AS DateTime), N'ADM', CAST(N'2024-06-25T17:22:18.757' AS DateTime), N'ADM', 0)
GO
INSERT [dbo].[quest_pergunta] ([quest_perguntastamp], [queststamp], [pergunta], [ordem], [ousrdata], [ousrinis], [usrdata], [usrinis], [permiteMulti]) VALUES (N'3013004B-1BF0-40F5-BA5E-AA4BDEC67A19', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'A prescrição deste medicamento foi feita por um médico de que especialidade?', 1, CAST(N'2024-04-11T11:54:36.690' AS DateTime), N'', CAST(N'2024-04-11T11:54:36.690' AS DateTime), N'', 0)
GO
INSERT [dbo].[quest_pergunta] ([quest_perguntastamp], [queststamp], [pergunta], [ordem], [ousrdata], [ousrinis], [usrdata], [usrinis], [permiteMulti]) VALUES (N'5739A9A9-6E69-4193-857C-A', N'230AE1CA-D832-49E4-8D73-E', N'Observações:', 5, CAST(N'2024-06-25T17:22:57.977' AS DateTime), N'ADM', CAST(N'2024-06-25T17:22:57.977' AS DateTime), N'ADM', 0)
GO
INSERT [dbo].[quest_pergunta] ([quest_perguntastamp], [queststamp], [pergunta], [ordem], [ousrdata], [ousrinis], [usrdata], [usrinis], [permiteMulti]) VALUES (N'586CC18B-68C0-47FB-B808-AEE07D1540AA', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'Qual o seu género?', 6, CAST(N'2024-04-11T11:56:22.453' AS DateTime), N'', CAST(N'2024-04-11T11:56:22.453' AS DateTime), N'', 0)
GO
INSERT [dbo].[quest_pergunta] ([quest_perguntastamp], [queststamp], [pergunta], [ordem], [ousrdata], [ousrinis], [usrdata], [usrinis], [permiteMulti]) VALUES (N'6112C549-6F77-4A6D-A5BA-E', N'230AE1CA-D832-49E4-8D73-E', N'No caso de solicitação do medicamento e/ou produto de saúde foi dispensado o solicitado pelo utente?', 4, CAST(N'2024-08-07T11:33:10.433' AS DateTime), N'', CAST(N'2024-08-07T11:33:10.433' AS DateTime), N'', 0)
GO
INSERT [dbo].[quest_pergunta] ([quest_perguntastamp], [queststamp], [pergunta], [ordem], [ousrdata], [ousrinis], [usrdata], [usrinis], [permiteMulti]) VALUES (N'ACFEF80E-B440-414D-B28A-DFEBFEB68234', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'Qual a sua idade?', 7, CAST(N'2024-04-11T11:57:39.927' AS DateTime), N'', CAST(N'2024-04-11T11:57:39.927' AS DateTime), N'', 0)
GO
INSERT [dbo].[quest_pergunta] ([quest_perguntastamp], [queststamp], [pergunta], [ordem], [ousrdata], [ousrinis], [usrdata], [usrinis], [permiteMulti]) VALUES (N'B185CF07-F404-4E5D-9E1F-053FD96E593F', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'Nos últimos 12 meses, quantas vezes foi à urgência devido à Asma?', 4, CAST(N'2024-04-11T11:55:44.287' AS DateTime), N'', CAST(N'2024-04-11T11:55:44.287' AS DateTime), N'', 0)
GO
INSERT [dbo].[quest_pergunta] ([quest_perguntastamp], [queststamp], [pergunta], [ordem], [ousrdata], [ousrinis], [usrdata], [usrinis], [permiteMulti]) VALUES (N'BECA822D-9B37-44DA-81F5-471CFDB2E4FF', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'Para que indicação foi prescrito este medicamento?', 3, CAST(N'2024-04-11T11:55:23.393' AS DateTime), N'', CAST(N'2024-04-11T11:55:23.393' AS DateTime), N'', 0)
GO
INSERT [dbo].[quest_pergunta] ([quest_perguntastamp], [queststamp], [pergunta], [ordem], [ousrdata], [ousrinis], [usrdata], [usrinis], [permiteMulti]) VALUES (N'C25686DF-22A6-414D-88CA-8', N'230AE1CA-D832-49E4-8D73-E', N'Situação Clínica Ligeira', 1, CAST(N'2024-06-25T17:21:51.600' AS DateTime), N'ADM', CAST(N'2024-06-25T17:21:51.600' AS DateTime), N'ADM', 0)
GO
INSERT [dbo].[quest_pergunta] ([quest_perguntastamp], [queststamp], [pergunta], [ordem], [ousrdata], [ousrinis], [usrdata], [usrinis], [permiteMulti]) VALUES (N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'Qual o distrito onde reside?', 8, CAST(N'2024-04-11T11:57:57.773' AS DateTime), N'', CAST(N'2024-04-11T11:57:57.773' AS DateTime), N'', 0)
GO
INSERT [dbo].[quest_pergunta] ([quest_perguntastamp], [queststamp], [pergunta], [ordem], [ousrdata], [ousrinis], [usrdata], [usrinis], [permiteMulti]) VALUES (N'FC9689AB-927A-473D-8510-A', N'230AE1CA-D832-49E4-8D73-E', N'Intervenção Farmacêutica:', 3, CAST(N'2024-06-25T17:22:35.443' AS DateTime), N'ADM', CAST(N'2024-06-25T17:22:35.443' AS DateTime), N'ADM', 1)
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'000DAA5A-139A-423B-BACD-1EA183F942AC', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'Faro', 5, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:20:01.267' AS DateTime), N'', CAST(N'2024-04-15T10:20:01.267' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'03404C5D-3F65-4B19-9960-0', N'C25686DF-22A6-414D-88CA-8', N'Herpes labial', 26, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:59:51.073' AS DateTime), N'ADM', CAST(N'2024-06-26T09:59:51.073' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'058D2551-62BE-425E-B5BE-A51688EB75A5', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'Setúbal', 6, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:20:07.657' AS DateTime), N'', CAST(N'2024-04-15T10:20:07.657' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'05D73C65-46AD-4DEB-86F7-B67D3D6675DE', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'Évora', 16, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:21:23.783' AS DateTime), N'', CAST(N'2024-04-15T10:21:23.783' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'0605A59F-E1B2-40EB-93A3-26E43651B8E9', N'B185CF07-F404-4E5D-9E1F-053FD96E593F', N'', 1, N'CA3C9FF9-EEDB-4BF9-A10C-AA7C10F2FE46', CAST(N'2024-04-15T10:10:44.857' AS DateTime), N'', CAST(N'2024-04-15T10:10:44.857' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'075DFE5B-4923-4678-AE57-D', N'FC9689AB-927A-473D-8510-A', N'Encaminhamento a consulta médica', 3, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-08-07T15:25:09.820' AS DateTime), N'', CAST(N'2024-08-07T15:25:09.820' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'08FD0227-8FEB-4A12-95C1-2ECD6F2A7064', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'R.A dos Açores', 12, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:20:48.490' AS DateTime), N'', CAST(N'2024-04-15T10:20:48.490' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'0A29B937-CACF-449C-B6A9-BF08C3419CB9', N'3013004B-1BF0-40F5-BA5E-AA4BDEC67A19', N'Médico de medicina interna', 2, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:06:03.070' AS DateTime), N'', CAST(N'2024-04-15T10:06:03.070' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'0E47146B-D43C-45FD-803B-23A9704F7AA7', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'Bragança', 19, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:21:58.300' AS DateTime), N'', CAST(N'2024-04-15T10:21:58.300' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'10521D90-38EF-47E5-886F-C', N'5739A9A9-6E69-4193-857C-A', N'', 1, N'BB1BE89D-D985-4C9B-B34C-84EDD1F6F0B2', CAST(N'2024-08-07T15:26:40.540' AS DateTime), N'', CAST(N'2024-08-07T15:26:40.540' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'12AAD427-9EDD-4065-A775-03977934DF29', N'0EED4FE7-215D-449A-99F7-6A07B42F900E', N'', 1, N'CA3C9FF9-EEDB-4BF9-A10C-AA7C10F2FE46', CAST(N'2024-04-15T10:11:35.170' AS DateTime), N'', CAST(N'2024-04-15T10:11:35.170' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'185ED5AD-3E6D-43C6-BA93-F', N'D79DE1C1-178C-4B72-83E1-4', N' 0-18 anos', 1, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:46:40.173' AS DateTime), N'ADM', CAST(N'2024-06-26T09:46:40.173' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'1A793AA9-B410-429A-A02F-1', N'C25686DF-22A6-414D-88CA-8', N'Otalgia', 28, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T10:15:32.250' AS DateTime), N'ADM', CAST(N'2024-06-26T10:15:32.250' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'1C2E19FB-D454-4849-A714-F', N'2B442423-878C-4424-8113-9', N'Solicitação de um medicamento e/ou produto de saúde específico pelo utente', 2, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-08-07T11:39:39.353' AS DateTime), N'', CAST(N'2024-08-07T11:39:39.353' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'1F3D2F5F-7CDE-4BDF-9C5B-19E2C77C7D40', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'Viana Do Castelo', 13, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:20:54.490' AS DateTime), N'', CAST(N'2024-04-15T10:20:54.490' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'1F3E0AA2-07E4-4691-A657-4', N'ECC6181B-71D8-4C93-806C-5', N'Masculino', 1, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:45:45.513' AS DateTime), N'ADM', CAST(N'2024-06-26T09:45:45.513' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'1F6892A2-F5DC-4B8A-A3E3-ECE305EF352E', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'R.A da Madeira', 11, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:20:41.927' AS DateTime), N'', CAST(N'2024-04-15T10:20:41.927' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'1F7C094C-5A6D-4719-B306-7', N'C25686DF-22A6-414D-88CA-8', N'Onicomicose', 14, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:52:35.757' AS DateTime), N'ADM', CAST(N'2024-06-26T09:52:35.757' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'22792AD1-17DE-4125-993C-77A232AD14B2', N'ACFEF80E-B440-414D-B28A-DFEBFEB68234', N'', 1, N'CA3C9FF9-EEDB-4BF9-A10C-AA7C10F2FE46', CAST(N'2024-04-15T10:18:41.477' AS DateTime), N'', CAST(N'2024-04-15T10:18:41.477' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'310AE0C4-7192-42C4-94AF-6', N'C25686DF-22A6-414D-88CA-8', N'Estomatite aftosa', 24, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:59:32.060' AS DateTime), N'ADM', CAST(N'2024-06-26T09:59:32.060' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'347C1739-FF6F-4EBF-9E44-D', N'C25686DF-22A6-414D-88CA-8', N'Cólicas/Flatulência', 9, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:51:22.897' AS DateTime), N'ADM', CAST(N'2024-06-26T09:51:22.897' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'35F14EC4-8AF9-4F1A-BC00-D8D69BC3313D', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'Portalegre', 20, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:22:25.457' AS DateTime), N'', CAST(N'2024-04-15T10:22:25.457' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'395369D8-23E8-491D-8F3B-6', N'ECC6181B-71D8-4C93-806C-5', N'Feminino', 2, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:45:58.733' AS DateTime), N'ADM', CAST(N'2024-06-26T09:45:58.733' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'4424600E-785E-43A5-9A93-5256C6927B39', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'Viseu', 10, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:20:34.833' AS DateTime), N'', CAST(N'2024-04-15T10:20:34.833' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'48456F78-FD12-4209-A5D2-7', N'6112C549-6F77-4A6D-A5BA-E', N'Sim', 1, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-08-07T15:25:52.570' AS DateTime), N'', CAST(N'2024-08-07T15:25:52.570' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'4A1EB1A0-6F37-4076-BA12-2', N'C25686DF-22A6-414D-88CA-8', N'Dor de cabeça (cefaleia/enxaqueca)', 7, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:50:46.927' AS DateTime), N'ADM', CAST(N'2024-06-26T09:50:46.927' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'4F4C51E2-843A-4DF4-831B-F', N'C25686DF-22A6-414D-88CA-8', N'Queimadura solar', 23, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:59:09.903' AS DateTime), N'ADM', CAST(N'2024-06-26T09:59:09.903' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'554F36B8-8448-4F27-998C-CA1D2C9F2412', N'586CC18B-68C0-47FB-B808-AEE07D1540AA', N'Masculino', 2, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:18:21.330' AS DateTime), N'', CAST(N'2024-04-15T10:18:21.330' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'55F74F40-3620-424E-AAFA-239EFC368C05', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'Aveiro', 4, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:19:52.640' AS DateTime), N'', CAST(N'2024-04-15T10:19:52.640' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'5AB52EDC-C16F-485F-97FF-3', N'FC9689AB-927A-473D-8510-A', N'Aconselhamento de medidas farmacológicas', 1, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-08-07T15:24:33.930' AS DateTime), N'', CAST(N'2024-08-07T15:24:33.930' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'5B2D9241-9D16-4A1C-9078-5', N'C25686DF-22A6-414D-88CA-8', N'Obstipação', 8, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:51:12.020' AS DateTime), N'ADM', CAST(N'2024-06-26T09:51:12.020' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'5E08C77E-2861-4684-AB78-B', N'C25686DF-22A6-414D-88CA-8', N'Pé de atleta', 31, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T10:15:59.237' AS DateTime), N'ADM', CAST(N'2024-06-26T10:15:59.237' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'5EFE1742-3551-47D2-9DFD-7', N'C25686DF-22A6-414D-88CA-8', N'Diarreia aguda', 17, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:56:17.010' AS DateTime), N'ADM', CAST(N'2024-06-26T09:56:17.010' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'62C37A0D-BEAB-45D0-BF77-E', N'C25686DF-22A6-414D-88CA-8', N'Conjuntivite alérgica', 19, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:56:49.120' AS DateTime), N'ADM', CAST(N'2024-06-26T09:56:49.120' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'68E7C270-05E8-40D9-B1C4-2', N'C25686DF-22A6-414D-88CA-8', N'Verruga', 32, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T10:16:10.767' AS DateTime), N'ADM', CAST(N'2024-06-26T10:16:10.767' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'6A5FB75B-58C8-4BE4-B2FF-D', N'2B442423-878C-4424-8113-9', N'Apresentação de um quadro de sintomas/queixas pelo utente', 1, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-08-07T11:39:14.083' AS DateTime), N'', CAST(N'2024-08-07T11:39:14.083' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'6C1F29EC-A6E6-4320-8F54-3', N'C25686DF-22A6-414D-88CA-8', N'Tosse', 1, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:49:31.660' AS DateTime), N'ADM', CAST(N'2024-06-26T09:49:31.660' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'7A6D33C1-BBE0-4E00-ACAC-43E254181EE6', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'Castelo Branco', 15, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:21:17.500' AS DateTime), N'', CAST(N'2024-04-15T10:21:17.500' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'7A9DA92A-E636-4C92-8AE0-C', N'C25686DF-22A6-414D-88CA-8', N'Febre', 3, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:49:48.880' AS DateTime), N'ADM', CAST(N'2024-06-26T09:49:48.880' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'8111386D-AAB4-4111-88B5-03F24DBDA8F0', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'Santarém', 9, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:20:28.803' AS DateTime), N'', CAST(N'2024-04-15T10:20:28.803' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'82A3B6F7-A7FE-41E9-8903-0', N'C25686DF-22A6-414D-88CA-8', N'Candidíase Oral', 25, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:59:41.997' AS DateTime), N'ADM', CAST(N'2024-06-26T09:59:41.997' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'85BB4B3B-8D0A-48BC-9146-5', N'C25686DF-22A6-414D-88CA-8', N'Vómitos/enjoo do movimento', 11, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:51:52.240' AS DateTime), N'ADM', CAST(N'2024-06-26T09:51:52.240' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'86EAD830-1813-4443-B13C-77B3527F4126', N'27E1D2A6-17E2-4237-9D80-1C90AF0B183B', N'', 1, N'CA3C9FF9-EEDB-4BF9-A10C-AA7C10F2FE46', CAST(N'2024-04-15T10:08:26.180' AS DateTime), N'', CAST(N'2024-04-15T10:08:26.180' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'88799D5C-AE86-4BF1-B290-D', N'FC9689AB-927A-473D-8510-A', N'Aconselhamento de medidas não farmacológicas', 2, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-08-07T15:24:51.227' AS DateTime), N'', CAST(N'2024-08-07T15:24:51.227' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'8920723E-4BD1-4135-9DBF-5', N'C25686DF-22A6-414D-88CA-8', N'Rinite alérgica', 18, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:56:29.963' AS DateTime), N'ADM', CAST(N'2024-06-26T09:56:29.963' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'892CDB44-1510-44C4-B93C-9', N'C25686DF-22A6-414D-88CA-8', N'Outro', 34, N'BB1BE89D-D985-4C9B-B34C-84EDD1F6F0B2', CAST(N'2024-06-26T11:14:46.890' AS DateTime), N'ADM', CAST(N'2024-06-26T11:14:46.890' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'8A8948C3-B325-4D17-9787-BFE7EAF3496C', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'Braga', 3, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:19:45.810' AS DateTime), N'', CAST(N'2024-04-15T10:19:45.810' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'8B0070E4-334F-4C09-91D8-1', N'C25686DF-22A6-414D-88CA-8', N'Insónia', 15, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:52:45.427' AS DateTime), N'ADM', CAST(N'2024-06-26T09:52:45.427' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'8E945BCA-7AF1-481B-BF72-8', N'C25686DF-22A6-414D-88CA-8', N'Pés e pernas cansadas', 21, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:57:25.260' AS DateTime), N'ADM', CAST(N'2024-06-26T09:57:25.260' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'8FE60002-1D20-4614-9726-3', N'13A1DFC4-871C-408F-8F7A-5', N'', 1, N'BB1BE89D-D985-4C9B-B34C-84EDD1F6F0B2', CAST(N'2024-06-26T09:44:54.480' AS DateTime), N'ADM', CAST(N'2024-06-26T09:44:54.480' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'A74EBF84-F8D6-4CCC-9ADE-2', N'C25686DF-22A6-414D-88CA-8', N'Infeção do Trato Urinário (ITU) não complicada', 6, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:50:30.533' AS DateTime), N'ADM', CAST(N'2024-06-26T09:50:30.533' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'A8BC390F-1F65-498D-95CA-D', N'C25686DF-22A6-414D-88CA-8', N'Dermatites', 27, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T10:15:24.593' AS DateTime), N'ADM', CAST(N'2024-06-26T10:15:24.593' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'AC3DEB06-538E-4D79-9499-9', N'C25686DF-22A6-414D-88CA-8', N'Urticária', 33, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T10:16:19.300' AS DateTime), N'ADM', CAST(N'2024-06-26T10:16:19.300' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'B8F9FBBB-028D-4600-B31C-5D4A1D64BDBF', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'Beja', 18, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:21:37.783' AS DateTime), N'', CAST(N'2024-04-15T10:21:37.783' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'C7F6BA7A-93DF-496B-B4BB-914CE45C632B', N'3013004B-1BF0-40F5-BA5E-AA4BDEC67A19', N'Outro. Qual?', 5, N'BB1BE89D-D985-4C9B-B34C-84EDD1F6F0B2', CAST(N'2024-04-15T10:07:50.337' AS DateTime), N'', CAST(N'2024-04-15T10:07:50.337' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'C87CDE61-9A70-488C-A06E-5', N'D79DE1C1-178C-4B72-83E1-4', N'Mais de 65 anos', 3, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:48:59.020' AS DateTime), N'ADM', CAST(N'2024-06-26T09:48:59.020' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'CD484A27-94D8-489D-80EE-3', N'C25686DF-22A6-414D-88CA-8', N'Infeção Aguda da Orofaringe (IAO)', 4, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:50:03.550' AS DateTime), N'ADM', CAST(N'2024-06-26T09:50:03.550' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'CE4042FF-6ABD-41EF-9BBD-F909CEC1FE2E', N'3013004B-1BF0-40F5-BA5E-AA4BDEC67A19', N'Médico de família/ Médico de medicina geral e familiar/ Clínico geral', 1, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:04:33.790' AS DateTime), N'', CAST(N'2024-04-15T10:04:33.790' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'CE7D50D5-FBD8-445B-8FD5-AE9707D50A96', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'Coimbra', 8, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:20:22.567' AS DateTime), N'', CAST(N'2024-04-15T10:20:22.567' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'CEB7B571-F641-4233-BAC8-C0D4FD7154D4', N'BECA822D-9B37-44DA-81F5-471CFDB2E4FF', N'DPOC – doença pulmonar obstrutiva crónica', 2, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:10:00.667' AS DateTime), N'', CAST(N'2024-04-15T10:10:00.667' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'D009C6D3-45B9-4811-99AE-D266FB697AF1', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'Guarda', 17, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:21:30.970' AS DateTime), N'', CAST(N'2024-04-15T10:21:30.970' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'D042D23A-6552-47F3-B75D-2', N'C25686DF-22A6-414D-88CA-8', N'Constipação', 2, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:49:39.690' AS DateTime), N'ADM', CAST(N'2024-06-26T09:49:39.690' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'D0D4D382-CDD1-4381-AB83-AA142FC1F2C9', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'Lisboa', 1, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:19:08.633' AS DateTime), N'', CAST(N'2024-04-15T10:19:08.633' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'D31D48CB-FD0D-4D77-9C36-FB069B24B830', N'3013004B-1BF0-40F5-BA5E-AA4BDEC67A19', N'Pneumologista', 3, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:06:45.260' AS DateTime), N'', CAST(N'2024-04-15T10:06:45.260' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'D798060B-05CE-4EE7-965D-C', N'2B442423-878C-4424-8113-9', N'Outro', 3, N'BB1BE89D-D985-4C9B-B34C-84EDD1F6F0B2', CAST(N'2024-08-07T11:40:04.670' AS DateTime), N'', CAST(N'2024-08-07T11:40:04.670' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'DA0D3FBA-5256-4A53-AB2A-9', N'C25686DF-22A6-414D-88CA-8', N'Acne', 30, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T10:15:50.313' AS DateTime), N'ADM', CAST(N'2024-06-26T10:15:50.313' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'DC7A2A3F-4489-4ADB-B12A-A', N'C25686DF-22A6-414D-88CA-8', N'Picadas de inseto', 22, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:58:24.807' AS DateTime), N'ADM', CAST(N'2024-06-26T09:58:24.807' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'E1F40F58-8DFD-4B82-93F1-4', N'C25686DF-22A6-414D-88CA-8', N'Azia dispepsia', 16, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:53:08.070' AS DateTime), N'ADM', CAST(N'2024-06-26T09:53:08.070' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'E65D9048-835B-4B27-990A-F', N'6112C549-6F77-4A6D-A5BA-E', N'Não', 2, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-08-07T15:26:08.447' AS DateTime), N'', CAST(N'2024-08-07T15:26:08.447' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'E9D58BC1-F590-45E3-BCF8-8', N'D79DE1C1-178C-4B72-83E1-4', N'19-65 anos', 2, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:48:37.520' AS DateTime), N'ADM', CAST(N'2024-06-26T09:48:37.520' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'EE9A5425-08AB-443B-92D0-0', N'C25686DF-22A6-414D-88CA-8', N'Candidíase vaginal', 10, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:51:35.757' AS DateTime), N'ADM', CAST(N'2024-06-26T09:51:35.757' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'EED0D950-4189-472D-A4D8-F9CD3F8E0B37', N'586CC18B-68C0-47FB-B808-AEE07D1540AA', N'Feminino', 1, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:14:46.860' AS DateTime), N'', CAST(N'2024-04-15T10:14:46.860' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'F11B5E3A-0217-4FE1-8F2B-6627FF26B57C', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'Vila Real', 14, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:21:02.273' AS DateTime), N'', CAST(N'2024-04-15T10:21:02.273' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'F37D85A8-5E16-4B5E-BD8C-E', N'C25686DF-22A6-414D-88CA-8', N'Dismenorreia', 12, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:52:09.397' AS DateTime), N'ADM', CAST(N'2024-06-26T09:52:09.397' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'F3E13E4C-8E3A-4129-B9EF-9584287E8434', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'Leiria', 7, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:20:16.007' AS DateTime), N'', CAST(N'2024-04-15T10:20:16.007' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'F51042B0-03D2-4816-9CD1-ED8EB3FD9C2F', N'BECA822D-9B37-44DA-81F5-471CFDB2E4FF', N'Asma', 1, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:09:34.290' AS DateTime), N'', CAST(N'2024-04-15T10:09:34.290' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'F5FB1C31-9B95-4A1F-B103-C', N'C25686DF-22A6-414D-88CA-8', N'Dor musculoesquelética', 5, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:50:17.473' AS DateTime), N'ADM', CAST(N'2024-06-26T09:50:17.473' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'F6DCB34C-1F54-4228-84B6-540FAF38D0D9', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'Distrito do Porto', 2, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:19:29.777' AS DateTime), N'', CAST(N'2024-04-15T10:19:29.777' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'F7FC07F0-8726-4461-99AF-F', N'C25686DF-22A6-414D-88CA-8', N'Odontalgia', 29, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T10:15:40.690' AS DateTime), N'ADM', CAST(N'2024-06-26T10:15:40.690' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'F914F13D-70F4-4BC5-BE35-C', N'C25686DF-22A6-414D-88CA-8', N'Contraceção Oral de emergência', 13, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:52:20.647' AS DateTime), N'ADM', CAST(N'2024-06-26T09:52:20.647' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'FC69A5D0-E7D5-4790-A118-A', N'C25686DF-22A6-414D-88CA-8', N'Hemorroidas', 20, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-06-26T09:56:57.650' AS DateTime), N'ADM', CAST(N'2024-06-26T09:56:57.650' AS DateTime), N'ADM')
GO
INSERT [dbo].[quest_pergunta_resp] ([Quest_pergunta_respStamp], [Quest_perguntaStamp], [resposta], [ordem], [quest_pergunta_resp_tpStamp], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'FFEB6E42-F00D-4BD8-86F4-567B8D98C356', N'3013004B-1BF0-40F5-BA5E-AA4BDEC67A19', N'Imunoalergologista', 4, N'F9F51224-542C-4963-8486-5AAECC88F377', CAST(N'2024-04-15T10:07:12.760' AS DateTime), N'', CAST(N'2024-04-15T10:07:12.760' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp_tp] ([quest_pergunta_resp_tpStamp], [ID], [Descr], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'A7F49A7A-DCF0-4D76-B1F8-C', 5, N'MultiSel', CAST(N'2024-09-02T09:07:02.500' AS DateTime), N'', CAST(N'2024-09-02T09:07:02.500' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp_tp] ([quest_pergunta_resp_tpStamp], [ID], [Descr], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'BB1BE89D-D985-4C9B-B34C-84EDD1F6F0B2', 2, N'string', CAST(N'2024-04-03T11:31:54.353' AS DateTime), N'', CAST(N'2024-04-03T11:31:54.353' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp_tp] ([quest_pergunta_resp_tpStamp], [ID], [Descr], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'CA3C9FF9-EEDB-4BF9-A10C-AA7C10F2FE46', 3, N'numeric', CAST(N'2024-04-03T11:32:24.170' AS DateTime), N'', CAST(N'2024-04-03T11:32:24.170' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp_tp] ([quest_pergunta_resp_tpStamp], [ID], [Descr], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'E2299E21-DC64-4A8A-8A29-ED41433EFD0B', 4, N'date', CAST(N'2024-04-03T11:33:21.983' AS DateTime), N'', CAST(N'2024-04-03T11:33:21.983' AS DateTime), N'')
GO
INSERT [dbo].[quest_pergunta_resp_tp] ([quest_pergunta_resp_tpStamp], [ID], [Descr], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'F9F51224-542C-4963-8486-5AAECC88F377', 1, N'Multi', CAST(N'2024-04-03T11:31:18.480' AS DateTime), N'', CAST(N'2024-04-03T11:31:18.480' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'022601A6-BD14-49A1-BB04-F57FCB4F2616', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'8111386D-AAB4-4111-88B5-03F24DBDA8F0', N'', 0, CAST(N'2024-04-15T10:42:37.463' AS DateTime), N'', CAST(N'2024-04-15T10:42:37.463' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'03224229-4DB3-4E1D-991B-23F72DDD589F', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'D009C6D3-45B9-4811-99AE-D266FB697AF1', N'', 0, CAST(N'2024-04-15T10:42:37.947' AS DateTime), N'', CAST(N'2024-04-15T10:42:37.947' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'06C2319F-56FC-466B-941B-0FDED7FBB1B7', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'D0D4D382-CDD1-4381-AB83-AA142FC1F2C9', N'', 0, CAST(N'2024-04-15T10:42:38.070' AS DateTime), N'', CAST(N'2024-04-15T10:42:38.070' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'08E64335-BA14-47F4-9BCE-3', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'F37D85A8-5E16-4B5E-BD8C-E', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'0D565E6C-0F4B-4E4F-8A40-D', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'F5FB1C31-9B95-4A1F-B103-C', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'0EEE3996-591C-4B5F-8F45-6', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'8E945BCA-7AF1-481B-BF72-8', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'0F43D890-177B-42EB-88F9-0', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'310AE0C4-7192-42C4-94AF-6', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'0FA16AC1-6723-46E0-A976-2107400F4E70', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'3013004B-1BF0-40F5-BA5E-AA4BDEC67A19', N'0A29B937-CACF-449C-B6A9-BF08C3419CB9', N'', 2, CAST(N'2024-04-15T10:42:33.777' AS DateTime), N'', CAST(N'2024-04-15T10:42:33.777' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'11489D98-5ABC-4580-8B1B-7', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'F7FC07F0-8726-4461-99AF-F', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'1180A450-14C8-482D-B544-1228ACFDB708', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'3013004B-1BF0-40F5-BA5E-AA4BDEC67A19', N'FFEB6E42-F00D-4BD8-86F4-567B8D98C356', N'', 2, CAST(N'2024-04-15T10:42:34.480' AS DateTime), N'', CAST(N'2024-04-15T10:42:34.480' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'15411C37-A3E4-4886-85E3-2', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'82A3B6F7-A7FE-41E9-8903-0', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'158AF9BE-DE72-43DD-90A3-0877EE326B08', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'4424600E-785E-43A5-9A93-5256C6927B39', N'', 0, CAST(N'2024-04-15T10:42:37.087' AS DateTime), N'', CAST(N'2024-04-15T10:42:37.087' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'185B5328-EFF3-4E46-A598-6', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'CD484A27-94D8-489D-80EE-3', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'1B676505-319A-4799-BFB8-5', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'A74EBF84-F8D6-4CCC-9ADE-2', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'1FCB3561-30D2-4D7E-ADD0-DB2F30243AFC', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'8A8948C3-B325-4D17-9787-BFE7EAF3496C', N'', 0, CAST(N'2024-04-15T10:42:37.587' AS DateTime), N'', CAST(N'2024-04-15T10:42:37.587' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'23EE1B2C-E5D3-46B6-886E-C5423A532904', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'F6DCB34C-1F54-4228-84B6-540FAF38D0D9', N'', 0, CAST(N'2024-04-15T10:42:38.447' AS DateTime), N'', CAST(N'2024-04-15T10:42:38.447' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'25F0BDA3-FE16-4906-9D90-9', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'F914F13D-70F4-4BC5-BE35-C', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'26CDD90C-8242-41F9-95E5-2C8DB30520F9', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'0E47146B-D43C-45FD-803B-23A9704F7AA7', N'', 0, CAST(N'2024-04-15T10:42:36.527' AS DateTime), N'', CAST(N'2024-04-15T10:42:36.527' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'28C8CDDD-A224-442D-BCE6-E', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'E1F40F58-8DFD-4B82-93F1-4', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'367D40F3-33E6-4CCB-A848-44E0B2DEB54B', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'3013004B-1BF0-40F5-BA5E-AA4BDEC67A19', N'CE4042FF-6ABD-41EF-9BBD-F909CEC1FE2E', N'', 2, CAST(N'2024-04-15T10:42:34.070' AS DateTime), N'', CAST(N'2024-04-15T10:42:34.070' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'3BB8E627-DB8F-412C-9C59-6', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'D042D23A-6552-47F3-B75D-2', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'4360D20C-6594-4860-B726-9', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'68E7C270-05E8-40D9-B1C4-2', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'469E60FC-2631-47EF-918D-7F304915FE3A', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'ACFEF80E-B440-414D-B28A-DFEBFEB68234', N'22792AD1-17DE-4125-993C-77A232AD14B2', N'', 8, CAST(N'2024-04-15T10:42:35.777' AS DateTime), N'', CAST(N'2024-04-15T10:42:35.777' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'4BB1B721-B3EA-446C-9F2E-348699E44282', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'3013004B-1BF0-40F5-BA5E-AA4BDEC67A19', N'D31D48CB-FD0D-4D77-9C36-FB069B24B830', N'', 2, CAST(N'2024-04-15T10:42:34.243' AS DateTime), N'', CAST(N'2024-04-15T10:42:34.243' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'4D6E6C36-E395-4FDE-8BFB-3', N'230AE1CA-D832-49E4-8D73-E', N'6112C549-6F77-4A6D-A5BA-E', N'E65D9048-835B-4B27-990A-F', N'', 5, CAST(N'2024-08-19T15:13:16.087' AS DateTime), N'', CAST(N'2024-08-19T15:13:16.087' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'5103282D-EEF3-4CE8-8D88-D', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'1A793AA9-B410-429A-A02F-1', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'52F8C682-E09A-40AC-81DC-2', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'8920723E-4BD1-4135-9DBF-5', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'531BC0E4-45F1-42AA-9BC1-6', N'230AE1CA-D832-49E4-8D73-E', N'2B442423-878C-4424-8113-9', N'D798060B-05CE-4EE7-965D-C', N'', 3, CAST(N'2024-08-19T12:26:59.683' AS DateTime), N'', CAST(N'2024-08-19T12:26:59.683' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'55C20E55-4525-4AF1-9C96-D', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'347C1739-FF6F-4EBF-9E44-D', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'5DDB6385-1E3D-440A-919A-9BFE92D770FC', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'0EED4FE7-215D-449A-99F7-6A07B42F900E', N'12AAD427-9EDD-4065-A775-03977934DF29', N'', 5, CAST(N'2024-04-15T10:42:35.337' AS DateTime), N'', CAST(N'2024-04-15T10:42:35.337' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'5F30BA5A-A51D-455C-A927-A', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'AC3DEB06-538E-4D79-9499-9', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'6AE4E89D-3D9B-477E-A457-2', N'230AE1CA-D832-49E4-8D73-E', N'2B442423-878C-4424-8113-9', N'1C2E19FB-D454-4849-A714-F', N'', 3, CAST(N'2024-08-19T12:26:59.683' AS DateTime), N'', CAST(N'2024-08-19T12:26:59.683' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'72C1F3C3-A523-488E-AB86-9', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'6C1F29EC-A6E6-4320-8F54-3', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'74157DAF-9A17-4F04-B0FF-E7B5ECEF9C2F', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'000DAA5A-139A-423B-BACD-1EA183F942AC', N'', 0, CAST(N'2024-04-15T10:42:35.917' AS DateTime), N'', CAST(N'2024-04-15T10:42:35.917' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'753C2ECD-61DC-4196-8D83-23BA2C99025A', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'B8F9FBBB-028D-4600-B31C-5D4A1D64BDBF', N'', 0, CAST(N'2024-04-15T10:42:37.713' AS DateTime), N'', CAST(N'2024-04-15T10:42:37.713' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'7847A0AC-34AA-42A6-BC0C-D', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'85BB4B3B-8D0A-48BC-9146-5', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'784E56A1-D2D9-4360-8D83-9', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'03404C5D-3F65-4B19-9960-0', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'7DC3D7C9-FE44-40F6-AB05-CD521302722F', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'BECA822D-9B37-44DA-81F5-471CFDB2E4FF', N'CEB7B571-F641-4233-BAC8-C0D4FD7154D4', N'', 5, CAST(N'2024-04-15T10:42:34.870' AS DateTime), N'', CAST(N'2024-04-15T10:42:34.870' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'878DDFBA-74A4-4A6A-8519-60E85F3F9C32', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'F3E13E4C-8E3A-4129-B9EF-9584287E8434', N'', 0, CAST(N'2024-04-15T10:42:38.307' AS DateTime), N'', CAST(N'2024-04-15T10:42:38.307' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'8804E5B3-9288-46F7-A327-67F065E6E429', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'1F3D2F5F-7CDE-4BDF-9C5B-19E2C77C7D40', N'', 0, CAST(N'2024-04-15T10:42:36.680' AS DateTime), N'', CAST(N'2024-04-15T10:42:36.680' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'8830A8A4-E47D-4BF5-BA9F-290B7C03041E', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'058D2551-62BE-425E-B5BE-A51688EB75A5', N'', 0, CAST(N'2024-04-15T10:42:36.087' AS DateTime), N'', CAST(N'2024-04-15T10:42:36.087' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'8A2F22B0-B391-40F4-88ED-2', N'230AE1CA-D832-49E4-8D73-E', N'6112C549-6F77-4A6D-A5BA-E', N'48456F78-FD12-4209-A5D2-7', N'', 5, CAST(N'2024-08-19T15:13:03.260' AS DateTime), N'', CAST(N'2024-08-19T15:13:03.260' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'8B8DD75E-9298-47A3-918D-8', N'230AE1CA-D832-49E4-8D73-E', N'FC9689AB-927A-473D-8510-A', N'5AB52EDC-C16F-485F-97FF-3', N'', 4, CAST(N'2024-08-19T12:34:32.010' AS DateTime), N'', CAST(N'2024-08-19T12:34:32.010' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'9167C4FC-568C-4EE7-AF4D-6', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'892CDB44-1510-44C4-B93C-9', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'95722070-E9AB-4BCA-B4F4-FAE84E5BFB22', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'B185CF07-F404-4E5D-9E1F-053FD96E593F', N'0605A59F-E1B2-40EB-93A3-26E43651B8E9', N'', 5, CAST(N'2024-04-15T10:42:35.180' AS DateTime), N'', CAST(N'2024-04-15T10:42:35.180' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'9778E4F0-A40F-4EBA-B392-9', N'230AE1CA-D832-49E4-8D73-E', N'5739A9A9-6E69-4193-857C-A', N'10521D90-38EF-47E5-886F-C', N'', 0, CAST(N'2024-08-19T15:14:10.353' AS DateTime), N'', CAST(N'2024-08-19T15:14:10.353' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'9C4D04E9-8EB1-405E-8DA8-6', N'230AE1CA-D832-49E4-8D73-E', N'FC9689AB-927A-473D-8510-A', N'075DFE5B-4923-4678-AE57-D', N'', 5, CAST(N'2024-08-19T12:35:42.897' AS DateTime), N'', CAST(N'2024-08-19T12:35:42.897' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'9CA25B46-9996-4E15-A07E-C', N'230AE1CA-D832-49E4-8D73-E', N'2B442423-878C-4424-8113-9', N'6A5FB75B-58C8-4BE4-B2FF-D', N'', 3, CAST(N'2024-08-19T12:26:59.683' AS DateTime), N'', CAST(N'2024-08-19T12:26:59.683' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'A37220A0-B108-4B82-A946-E180D2BB9DAE', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'7A6D33C1-BBE0-4E00-ACAC-43E254181EE6', N'', 0, CAST(N'2024-04-15T10:42:37.337' AS DateTime), N'', CAST(N'2024-04-15T10:42:37.337' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'A500513B-6397-4809-8F09-08F161DE7251', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'35F14EC4-8AF9-4F1A-BC00-D8D69BC3313D', N'', 0, CAST(N'2024-04-15T10:42:36.980' AS DateTime), N'', CAST(N'2024-04-15T10:42:36.980' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'AB1E31FD-303A-4B2C-BD6C-5', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'7A9DA92A-E636-4C92-8AE0-C', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'ACF1A701-4D98-4116-B177-20493F5745F3', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'05D73C65-46AD-4DEB-86F7-B67D3D6675DE', N'', 0, CAST(N'2024-04-15T10:42:36.260' AS DateTime), N'', CAST(N'2024-04-15T10:42:36.260' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'B346A797-A2C9-4B71-B2AE-7', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'FC69A5D0-E7D5-4790-A118-A', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'B7CFF9B0-1EF9-46D0-A2DB-2', N'230AE1CA-D832-49E4-8D73-E', N'FC9689AB-927A-473D-8510-A', N'88799D5C-AE86-4BF1-B290-D', N'', 5, CAST(N'2024-08-19T12:35:19.990' AS DateTime), N'', CAST(N'2024-08-19T12:35:19.990' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'B8444E20-A918-4FE5-A989-A', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'1F7C094C-5A6D-4719-B306-7', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'BA3577D5-21BF-4E7F-922E-D', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'A8BC390F-1F65-498D-95CA-D', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'BE304BCD-1DED-4A3E-82D0-1316ED25A10A', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'F11B5E3A-0217-4FE1-8F2B-6627FF26B57C', N'', 0, CAST(N'2024-04-15T10:42:38.197' AS DateTime), N'', CAST(N'2024-04-15T10:42:38.197' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'C1A08B03-CE80-40C8-8E9D-6C83C335B7CF', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'586CC18B-68C0-47FB-B808-AEE07D1540AA', N'554F36B8-8448-4F27-998C-CA1D2C9F2412', N'', 5, CAST(N'2024-04-15T10:42:35.493' AS DateTime), N'', CAST(N'2024-04-15T10:42:35.493' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'C9774A65-8700-4DF2-913F-F', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'8B0070E4-334F-4C09-91D8-1', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'CA2E87E2-757C-49B1-8758-A', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'5E08C77E-2861-4684-AB78-B', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'CBDBC59A-4B1F-4B8B-ACB8-8', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'EE9A5425-08AB-443B-92D0-0', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'CC81009D-F011-4D01-B4A1-FBCEE33991A3', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'55F74F40-3620-424E-AAFA-239EFC368C05', N'', 0, CAST(N'2024-04-15T10:42:37.213' AS DateTime), N'', CAST(N'2024-04-15T10:42:37.213' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'D139CACE-FB58-477D-BFAB-2', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'62C37A0D-BEAB-45D0-BF77-E', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'D1CF8824-A695-4B97-846F-DCCA494484F8', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'BECA822D-9B37-44DA-81F5-471CFDB2E4FF', N'F51042B0-03D2-4816-9CD1-ED8EB3FD9C2F', N'', 4, CAST(N'2024-04-15T10:42:35.027' AS DateTime), N'', CAST(N'2024-04-15T10:42:35.027' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'D1DB1776-A226-4FC9-957C-EA67EF82EA85', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'3013004B-1BF0-40F5-BA5E-AA4BDEC67A19', N'C7F6BA7A-93DF-496B-B4BB-914CE45C632B', N'', 2, CAST(N'2024-04-15T10:42:33.947' AS DateTime), N'', CAST(N'2024-04-15T10:42:33.947' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'D5DED702-08EF-4B56-BB36-4', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'4F4C51E2-843A-4DF4-831B-F', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'D5FA396D-8932-4BA6-87F2-A', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'4A1EB1A0-6F37-4076-BA12-2', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'D6898465-DE38-4B03-A21F-33E399E1C32B', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'08FD0227-8FEB-4A12-95C1-2ECD6F2A7064', N'', 0, CAST(N'2024-04-15T10:42:36.383' AS DateTime), N'', CAST(N'2024-04-15T10:42:36.383' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'DC4FD429-36AB-4893-8CC8-9', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'5B2D9241-9D16-4A1C-9078-5', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'DD993F86-0B48-4821-B4AF-F18A1456832D', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'586CC18B-68C0-47FB-B808-AEE07D1540AA', N'EED0D950-4189-472D-A4D8-F9CD3F8E0B37', N'', 5, CAST(N'2024-04-15T10:42:35.633' AS DateTime), N'', CAST(N'2024-04-15T10:42:35.633' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'E3A1146B-1222-4B43-9D2D-EB2E062DC6E7', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'CE7D50D5-FBD8-445B-8FD5-AE9707D50A96', N'', 0, CAST(N'2024-04-15T10:42:37.837' AS DateTime), N'', CAST(N'2024-04-15T10:42:37.837' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'E914AC4F-036E-4755-BDB4-920565F64DF2', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'27E1D2A6-17E2-4237-9D80-1C90AF0B183B', N'86EAD830-1813-4443-B13C-77B3527F4126', N'', 3, CAST(N'2024-04-15T10:42:34.713' AS DateTime), N'', CAST(N'2024-04-15T10:42:34.713' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'EC3DE084-49FD-43A4-9067-F', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'DC7A2A3F-4489-4ADB-B12A-A', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'EFA6EBCC-A922-4D24-A8D6-9668AC405E3B', N'9ECFF4DF-3F01-49D1-A88F-B80EFECC474F', N'D63613A4-A269-46B3-B326-78D8D9D254C8', N'1F6892A2-F5DC-4B8A-A3E3-ECE305EF352E', N'', 0, CAST(N'2024-04-15T10:42:36.837' AS DateTime), N'', CAST(N'2024-04-15T10:42:36.837' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'F87E075E-C2E7-4EF9-B094-7', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'5EFE1742-3551-47D2-9DFD-7', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
INSERT [dbo].[quest_regras] ([quest_regrasStamp], [questStamp], [quest_perguntaStamp], [quest_pergunta_respStamp], [quest_pergunta_respStamp_dependente], [Prox_Ordem], [ousrdata], [ousrinis], [usrdata], [usrinis]) VALUES (N'F96B0EFF-8F35-4F43-9FD8-A', N'230AE1CA-D832-49E4-8D73-E', N'C25686DF-22A6-414D-88CA-8', N'DA0D3FBA-5256-4A53-AB2A-9', N'', 2, CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'', CAST(N'2024-08-19T12:23:18.183' AS DateTime), N'')
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [C_quest_unique]    Script Date: 17/09/2024 11:39:48 ******/
ALTER TABLE [dbo].[Quest] ADD  CONSTRAINT [C_quest_unique] UNIQUE NONCLUSTERED 
(
	[QuestStamp] ASC,
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [C_quest_pergunta_unique]    Script Date: 17/09/2024 11:39:48 ******/
ALTER TABLE [dbo].[quest_pergunta] ADD  CONSTRAINT [C_quest_pergunta_unique] UNIQUE NONCLUSTERED 
(
	[quest_perguntastamp] ASC,
	[ordem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [C_quest_pergunta_resp_unique]    Script Date: 17/09/2024 11:39:48 ******/
ALTER TABLE [dbo].[quest_pergunta_resp] ADD  CONSTRAINT [C_quest_pergunta_resp_unique] UNIQUE NONCLUSTERED 
(
	[Quest_pergunta_respStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [C_quest_pergunta_resp_tp_unique]    Script Date: 17/09/2024 11:39:48 ******/
ALTER TABLE [dbo].[quest_pergunta_resp_tp] ADD  CONSTRAINT [C_quest_pergunta_resp_tp_unique] UNIQUE NONCLUSTERED 
(
	[quest_pergunta_resp_tpStamp] ASC,
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [C_quest_resgras_unique]    Script Date: 17/09/2024 11:39:48 ******/
ALTER TABLE [dbo].[quest_regras] ADD  CONSTRAINT [C_quest_resgras_unique] UNIQUE NONCLUSTERED 
(
	[questStamp] ASC,
	[quest_regrasStamp] ASC,
	[quest_perguntaStamp] ASC,
	[quest_pergunta_respStamp] ASC,
	[Prox_Ordem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Quest] ADD  DEFAULT ('') FOR [QuestStamp]
GO
ALTER TABLE [dbo].[Quest] ADD  DEFAULT ((0)) FOR [ID]
GO
ALTER TABLE [dbo].[Quest] ADD  DEFAULT ('') FOR [Descr]
GO
ALTER TABLE [dbo].[Quest] ADD  DEFAULT ((0)) FOR [DadosCli]
GO
ALTER TABLE [dbo].[Quest] ADD  DEFAULT (getdate()) FOR [ousrdata]
GO
ALTER TABLE [dbo].[Quest] ADD  DEFAULT ('') FOR [ousrinis]
GO
ALTER TABLE [dbo].[Quest] ADD  DEFAULT (getdate()) FOR [usrdata]
GO
ALTER TABLE [dbo].[Quest] ADD  DEFAULT ('') FOR [usrinis]
GO
ALTER TABLE [dbo].[Quest] ADD  DEFAULT ((0)) FOR [Inativo]
GO
ALTER TABLE [dbo].[quest_pergunta] ADD  DEFAULT ('') FOR [quest_perguntastamp]
GO
ALTER TABLE [dbo].[quest_pergunta] ADD  DEFAULT ('') FOR [queststamp]
GO
ALTER TABLE [dbo].[quest_pergunta] ADD  DEFAULT ('') FOR [pergunta]
GO
ALTER TABLE [dbo].[quest_pergunta] ADD  DEFAULT ((0)) FOR [ordem]
GO
ALTER TABLE [dbo].[quest_pergunta] ADD  DEFAULT (getdate()) FOR [ousrdata]
GO
ALTER TABLE [dbo].[quest_pergunta] ADD  DEFAULT ('') FOR [ousrinis]
GO
ALTER TABLE [dbo].[quest_pergunta] ADD  DEFAULT (getdate()) FOR [usrdata]
GO
ALTER TABLE [dbo].[quest_pergunta] ADD  DEFAULT ('') FOR [usrinis]
GO
ALTER TABLE [dbo].[quest_pergunta] ADD  DEFAULT ((0)) FOR [permiteMulti]
GO
ALTER TABLE [dbo].[quest_pergunta_resp] ADD  DEFAULT ('') FOR [Quest_pergunta_respStamp]
GO
ALTER TABLE [dbo].[quest_pergunta_resp] ADD  DEFAULT ('') FOR [Quest_perguntaStamp]
GO
ALTER TABLE [dbo].[quest_pergunta_resp] ADD  DEFAULT ('') FOR [resposta]
GO
ALTER TABLE [dbo].[quest_pergunta_resp] ADD  DEFAULT ((0)) FOR [ordem]
GO
ALTER TABLE [dbo].[quest_pergunta_resp] ADD  DEFAULT ('') FOR [quest_pergunta_resp_tpStamp]
GO
ALTER TABLE [dbo].[quest_pergunta_resp] ADD  DEFAULT (getdate()) FOR [ousrdata]
GO
ALTER TABLE [dbo].[quest_pergunta_resp] ADD  DEFAULT ('') FOR [ousrinis]
GO
ALTER TABLE [dbo].[quest_pergunta_resp] ADD  DEFAULT (getdate()) FOR [usrdata]
GO
ALTER TABLE [dbo].[quest_pergunta_resp] ADD  DEFAULT ('') FOR [usrinis]
GO
ALTER TABLE [dbo].[quest_pergunta_resp_tp] ADD  DEFAULT ('') FOR [quest_pergunta_resp_tpStamp]
GO
ALTER TABLE [dbo].[quest_pergunta_resp_tp] ADD  DEFAULT ((0)) FOR [ID]
GO
ALTER TABLE [dbo].[quest_pergunta_resp_tp] ADD  DEFAULT ('') FOR [Descr]
GO
ALTER TABLE [dbo].[quest_pergunta_resp_tp] ADD  DEFAULT (getdate()) FOR [ousrdata]
GO
ALTER TABLE [dbo].[quest_pergunta_resp_tp] ADD  DEFAULT ('') FOR [ousrinis]
GO
ALTER TABLE [dbo].[quest_pergunta_resp_tp] ADD  DEFAULT (getdate()) FOR [usrdata]
GO
ALTER TABLE [dbo].[quest_pergunta_resp_tp] ADD  DEFAULT ('') FOR [usrinis]
GO
ALTER TABLE [dbo].[quest_regras] ADD  DEFAULT ('') FOR [quest_regrasStamp]
GO
ALTER TABLE [dbo].[quest_regras] ADD  DEFAULT ('') FOR [questStamp]
GO
ALTER TABLE [dbo].[quest_regras] ADD  DEFAULT ('') FOR [quest_perguntaStamp]
GO
ALTER TABLE [dbo].[quest_regras] ADD  DEFAULT ('') FOR [quest_pergunta_respStamp]
GO
ALTER TABLE [dbo].[quest_regras] ADD  DEFAULT ('') FOR [quest_pergunta_respStamp_dependente]
GO
ALTER TABLE [dbo].[quest_regras] ADD  DEFAULT ((0)) FOR [Prox_Ordem]
GO
ALTER TABLE [dbo].[quest_regras] ADD  DEFAULT (getdate()) FOR [ousrdata]
GO
ALTER TABLE [dbo].[quest_regras] ADD  DEFAULT ('') FOR [ousrinis]
GO
ALTER TABLE [dbo].[quest_regras] ADD  DEFAULT (getdate()) FOR [usrdata]
GO
ALTER TABLE [dbo].[quest_regras] ADD  DEFAULT ('') FOR [usrinis]
GO
