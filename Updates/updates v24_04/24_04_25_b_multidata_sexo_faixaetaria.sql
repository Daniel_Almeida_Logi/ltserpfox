






GO
IF not EXISTS (SELECT 1 FROM b_multidata WHERE campo = 'Masculino' and tipo = 'a_sexo')  
	BEGIN
		INSERT INTO [dbo].[b_multidata]
				   ([multidatastamp]
				   ,[campo]
				   ,[tipo]
				   ,[ousrinis]
				   ,[ousrdata]
				   ,[ousrhora]
				   ,[usrinis]
				   ,[usrdata]
				   ,[usrhora]
				   ,[code_motive]
				   ,[pais]
				   ,[code]
				   ,[ordem])
			 VALUES
				   (LEFT(NEWID(),25)
				   ,'Masculino'
				   ,'a_sexo'
				   ,'ADM'
				   ,GETDATE()
				   ,LEFT(CONVERT(VARCHAR(8), GETDATE(), 108), 5)
				   ,'ADM'
				   ,GETDATE()
				   ,LEFT(CONVERT(VARCHAR(8), GETDATE(), 108), 5)
				   ,''
				   ,''
				   ,'M'
				   ,99999)
	END
GO
GO
IF not EXISTS (SELECT 1 FROM b_multidata WHERE campo = 'Feminino' and tipo = 'a_sexo')  
	BEGIN
		INSERT INTO [dbo].[b_multidata]
				   ([multidatastamp]
				   ,[campo]
				   ,[tipo]
				   ,[ousrinis]
				   ,[ousrdata]
				   ,[ousrhora]
				   ,[usrinis]
				   ,[usrdata]
				   ,[usrhora]
				   ,[code_motive]
				   ,[pais]
				   ,[code]
				   ,[ordem])
			 VALUES
				   (LEFT(NEWID(),25)
				   ,'Feminino'
				   ,'a_sexo'
				   ,'ADM'
				   ,GETDATE()
				   ,LEFT(CONVERT(VARCHAR(8), GETDATE(), 108), 5)
				   ,'ADM'
				   ,GETDATE()
				   ,LEFT(CONVERT(VARCHAR(8), GETDATE(), 108), 5)
				   ,''
				   ,''
				   ,'F'
				   ,99999)
	END
GO


GO
IF not EXISTS (SELECT 1 FROM b_multidata WHERE campo = '0-18' and tipo = 'a_faixaetaria')  
	BEGIN
		INSERT INTO [dbo].[b_multidata]
				   ([multidatastamp]
				   ,[campo]
				   ,[tipo]
				   ,[ousrinis]
				   ,[ousrdata]
				   ,[ousrhora]
				   ,[usrinis]
				   ,[usrdata]
				   ,[usrhora]
				   ,[code_motive]
				   ,[pais]
				   ,[code]
				   ,[ordem])
			 VALUES
				   (LEFT(NEWID(),25)
				   ,'0-18'
				   ,'a_faixaetaria'
				   ,'ADM'
				   ,GETDATE()
				   ,LEFT(CONVERT(VARCHAR(8), GETDATE(), 108), 5)
				   ,'ADM'
				   ,GETDATE()
				   ,LEFT(CONVERT(VARCHAR(8), GETDATE(), 108), 5)
				   ,''
				   ,''
				   ,''
				   ,99999)
	END
GO




GO
IF not EXISTS (SELECT 1 FROM b_multidata WHERE campo = '19-65' and tipo = 'a_faixaetaria')  
	BEGIN
		INSERT INTO [dbo].[b_multidata]
				   ([multidatastamp]
				   ,[campo]
				   ,[tipo]
				   ,[ousrinis]
				   ,[ousrdata]
				   ,[ousrhora]
				   ,[usrinis]
				   ,[usrdata]
				   ,[usrhora]
				   ,[code_motive]
				   ,[pais]
				   ,[code]
				   ,[ordem])
			 VALUES
				   (LEFT(NEWID(),25)
				   ,'19-65'
				   ,'a_faixaetaria'
				   ,'ADM'
				   ,GETDATE()
				   ,LEFT(CONVERT(VARCHAR(8), GETDATE(), 108), 5)
				   ,'ADM'
				   ,GETDATE()
				   ,LEFT(CONVERT(VARCHAR(8), GETDATE(), 108), 5)
				   ,''
				   ,''
				   ,''
				   ,99999)
	END
GO





GO
IF not EXISTS (SELECT 1 FROM b_multidata WHERE campo = 'Mais de 65 anos' and tipo = 'a_faixaetaria')  
	BEGIN
		INSERT INTO [dbo].[b_multidata]
				   ([multidatastamp]
				   ,[campo]
				   ,[tipo]
				   ,[ousrinis]
				   ,[ousrdata]
				   ,[ousrhora]
				   ,[usrinis]
				   ,[usrdata]
				   ,[usrhora]
				   ,[code_motive]
				   ,[pais]
				   ,[code]
				   ,[ordem])
			 VALUES
				   (LEFT(NEWID(),25)
				   ,'Mais de 65 anos'
				   ,'a_faixaetaria'
				   ,'ADM'
				   ,GETDATE()
				   ,LEFT(CONVERT(VARCHAR(8), GETDATE(), 108), 5)
				   ,'ADM'
				   ,GETDATE()
				   ,LEFT(CONVERT(VARCHAR(8), GETDATE(), 108), 5)
				   ,''
				   ,''
				   ,''
				   ,99999)
	END
GO