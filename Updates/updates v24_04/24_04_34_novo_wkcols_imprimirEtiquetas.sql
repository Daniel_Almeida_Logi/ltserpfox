
IF( NOT EXISTS (SELECT * FROM wklcols (NOLOCK) WHERE wkstamp = (select top 1 wkstamp  from wk where wkdocdescr like 'Imprimir etiquetas') and wkfield = 'bi.iva' ))
	BEGIN

INSERT INTO [dbo].[wklcols]
           ([wklcolsstamp]
           ,[wkfield]
           ,[wktitle]
           ,[wkstamp]
           ,[wkdocsstamp]
           ,[ousrinis]
           ,[ousrdata]
           ,[ousrhora]
           ,[usrinis]
           ,[usrdata]
           ,[usrhora]
           ,[wkreadonly]
           ,[u_largura]
           ,[u_mascara]
           ,[u_ordem])
    SELECT top 1 LEFT(newid(),25)
      ,'bi.iva'
      ,'IVA'
      ,[wkstamp]
      ,[wkdocsstamp]
      ,[ousrinis]
      ,[ousrdata]
      ,[ousrhora]
      ,[usrinis]
      ,CONVERT(varchar,ousrdata, 112)
      ,CONVERT(VARCHAR(8), GETDATE(), 108)
      ,1
      ,70
      ,99999.99
      ,6
  FROM [dbo].[wklcols]
  where wkstamp = (select top 1 wkstamp  from wk where wkdocdescr like 'Imprimir etiquetas')


end 