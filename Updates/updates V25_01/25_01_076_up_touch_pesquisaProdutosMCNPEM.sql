/*  Pesquisa de Produtos por mesmo CNPEM (ALTERADO ANTERIORMENTE ERA GH) - Usada na Aplicação da Exceção C
	Só mostra os Produtos que existem na BD do Cliente - 

	 exec up_touch_pesquisaProdutosMCNPEM '5440987', '50036432', 1, 1

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_pesquisaProdutosMCNPEM]') IS NOT NULL
	drop procedure dbo.up_touch_pesquisaProdutosMCNPEM
go

create procedure dbo.up_touch_pesquisaProdutosMCNPEM
	@ref varchar(18),
	@cnpem varchar (8),
	@armazem numeric(5,0),
	@site_nr tinyint

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	/* 5 Mais Baratos*/
	declare @grphmgcode varchar(6)
	declare @dcipt_id varchar(10)

	select
		@cnpem			= fprod.cnpem
		,@grphmgcode	= fprod.grphmgcode
		,@dcipt_id		= fprod.dcipt_id
	from
		fprod (nolock)
	where
		cnp = @ref


	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosMaisBaratos'))
	BEGIN
		DROP TABLE #dadosMaisBaratos
	END


	select
		 ref		= fprod.cnp
		 ,epreco	= case when pvpcalc=0 then pvporig else pvpcalc end
	into 
		#dadosMaisBaratos
	from
		fprod (nolock)
		left join	st (nolock)	on st.ref = fprod.cnp and st.site_nr = @site_nr
	where
		ranking=1
		and grphmgcode = @grphmgcode
		and grphmgcode != 'GH0000' and grphmgcode != ''
		and fprod.pvporig <= fprod.pvpmaxre


		/* Result Set */
		/* Informação Produto Seleccionado */
		Select
			 ordem = 1
			,'sel'			= convert(bit,0)
			,'ref'			= fprod.cnp
			,'design'		= ltrim(rtrim(isnull(st.design,fprod.design)))
			,'stock'		= isnull(st.stock,0)
			,'epv1'			= isnull(st.epv1,'0')
			,'validade'		= isnull(convert(varchar,st.validade,102),'1900.01.01')
			,'faminome'		= isnull(st.faminome,convert(varchar,fprod.u_familia))
			,'comp'			= isnull( (case when (left(cptgrp.descrição,3)!='Não' and cptgrp.u_diploma1='' and cptgrp.u_diploma2='' and cptgrp.u_diploma3='') AND cptgrp.grupo!='ST'
											then 1
											else 0
									 end)
								  ,0)
			,'generico'		= IsNull(fprod.generico,0)
			,'GRPHMG'		= case when fprod.grphmgcode='GH0000' then fprod.grphmgcode
								else fprod.grphmgcode + ' - ' + fprod.grphmgdescr
							 end
			,'psico'		= isnull(fprod.psico,0)
			,'benzo'		= isnull(fprod.benzo,0)
			,'ststamp'		= isnull(st.ststamp,'')
			,'ptoenc'		= isnull(st.ptoenc,0)
			,'stmax'		= isnull(st.stmax,0)
			,'marg1'		= isnull(st.marg1,0)
			,'local'		= isnull(st.local,'')
			,'u_local'		= isnull(st.u_local,'')
			,'u_local2'		= isnull(st.u_local2,'')
			,'qttCli'		= isnull(st.qttcli, 0)
			,oref			= @ref /* mapeamento com cursor */
			,fprod.cnpem
			,bb = isnull(st.marg4,0)
			,labOrdem = 0
			,textosel = ''	
			,MCNPEM  = convert(bit,1)
			,t5 = case when (select COUNT(ref) from #dadosMaisBaratos where #dadosMaisBaratos.ref = fprod.cnp) > 0 then convert(bit,1) else convert(bit,0) end
			,'refInt' = ISNULL(st.ref, '')
			,'descVal'		= isnull(st.mfornec2,0)
			,'descPerc'		= isnull(st.mfornec,0)
			,'dosuni'		=  convert(varchar(254),dosuni)
			,'fformasabrev' = convert(varchar(254),fformasabrev)
			, num_unidades
			, fprod.grupo
			, valorUtenteComSns = isnull(st.epv1,'0')
			, fprod.pref
		from
			fprod (nolock)
			left join st (nolock) on st.ref=fprod.cnp and st.site_nr = @site_nr
			inner join cptgrp (nolock) on cptgrp.grupo=fprod.grupo
			left join sa (nolock) on sa.ref=st.ref AND sa.armazem=@armazem	
		where 
			st.ref = @ref

			UNION ALL

		/* Informação Restantes Produtos */
		Select
			 ordem = 2
			,'sel'			= convert(bit,0)
			,'ref'			= fprod.cnp
			,'design'		= ltrim(rtrim(isnull(st.design,fprod.design)))
			,'stock'		= isnull(st.stock,0)
			,'epv1'			= isnull(st.epv1,'0')
			,'validade'		= isnull(convert(varchar,st.validade,102),'1900.01.01')
			,'faminome'		= isnull(st.faminome,convert(varchar,fprod.u_familia))
			,'comp'			= isnull( (case when (left(cptgrp.descrição,3)!='Não' and cptgrp.u_diploma1='' and cptgrp.u_diploma2='' and cptgrp.u_diploma3='') AND cptgrp.grupo!='ST'
											then 1
											else 0
									 end)
								  ,0)
			,'generico'		= IsNull(fprod.generico,0)
			,'GRPHMG'		= case when fprod.grphmgcode='GH0000' then fprod.grphmgcode
								else fprod.grphmgcode + ' - ' + fprod.grphmgdescr
							 end
			,'psico'		= isnull(fprod.psico,0)
			,'benzo'		= isnull(fprod.benzo,0)
			,'ststamp'		= isnull(st.ststamp,'')
			,'ptoenc'		= isnull(st.ptoenc,0)
			,'stmax'		= isnull(st.stmax,0)
			,'marg1'		= isnull(st.marg1,0)
			,'local'		= isnull(st.local,'')
			,'u_local'		= isnull(st.u_local,'')
			,'u_local2'		= isnull(st.u_local2,'')
			,'qttCli'		= isnull(st.qttcli, 0)
			,oref			= @ref /* mapeamento com cursor */
			,fprod.cnpem
			,bb = isnull(st.marg4,0)
			,labOrdem = 0
			,textosel = ''	
			,MCNPEM  = convert(bit,1)
			,t5 = case when (select COUNT(ref) from #dadosMaisBaratos where #dadosMaisBaratos.ref = fprod.cnp) > 0 then convert(bit,1) else convert(bit,0) end
			,'refInt' = ISNULL(st.ref, '')
			,'descVal'		= isnull(st.mfornec2,0)
			,'descPerc'		= isnull(st.mfornec,0)
			,'dosuni'		=  convert(varchar(254),dosuni)
			,'fformasabrev' = convert(varchar(254),fformasabrev)
			, num_unidades
			, fprod.grupo
		    , valorUtenteComSns = isnull(st.epv1,'0')
			, fprod.pref
		from
			fprod (nolock)
			left join st (nolock) on st.ref=fprod.cnp and st.site_nr = @site_nr
			inner join cptgrp (nolock) on cptgrp.grupo=fprod.grupo
			left join sa (nolock) on sa.ref=st.ref AND sa.armazem=@armazem	
		where 
			st.ref != @ref
			AND st.epv1 < (select epv1 from st where ref = @ref and st.site_nr = @site_nr)
			--AND st.epv1 <= (select epv1 from st where ref = @ref and st.site_nr = @site_nr)
			AND fprod.cnpem = @CNPEM
			AND fprod.grupo != 'ST'
			AND (
				left(cptgrp.descricao,3)!='Não' 
				or cptgrp.u_diploma1 != '' 
				or cptgrp.u_diploma2 != ''
				or cptgrp.u_diploma3 != ''
			)
			and ISNULL(st.inactivo, 0) = 0
			
		--order by 
		--	st.stock desc
	
OPTION (OPTIMIZE FOR (@ref unknown, @cnpem unknown,@armazem unknown))	

GO
Grant Execute On dbo.up_touch_pesquisaProdutosMCNPEM to Public
Grant Control On dbo.up_touch_pesquisaProdutosMCNPEM to Public
Go

