/*
			exec up_receituario_CalcCompartBonus '1', '99', '01', '5815881'
	

*/

if OBJECT_ID('[dbo].[up_receituario_CalcCompartBonus ]') IS NOT NULL
	drop procedure dbo.up_receituario_CalcCompartBonus 
go





create procedure dbo.up_receituario_CalcCompartBonus 
	@tipo				varchar(100)
	,@idExt			    varchar(100)
	,@idPlan            varchar(10)
	,@ref               varchar(18)

/* with encryption */
as
	declare @grp varchar(5) = 'ST'
	select top 1 @grp=isnull(fprod.grupo,'ST') from fprod(nolock) where cnp = @ref
	
	set @grp = ISNULL(@grp,'ST')


	select 
		top 1
		grppreco,
		compart 
	from
		cptvalbonus(nolock)
	where
		tipo = isnull(@tipo,0) and
		idExt = @idExt and
		id_cpt_grp = @grp and
		id_cpt_pla = @idPlan 

	order by 
		data_alt
		desc



go
grant execute on dbo.up_receituario_CalcCompartBonus  to public		
grant control on dbo.up_receituario_CalcCompartBonus  to public
go