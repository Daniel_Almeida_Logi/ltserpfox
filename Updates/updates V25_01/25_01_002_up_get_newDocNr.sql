/* 
	Devolve o próximo número de documento, dentro da série e tabela indicadas
	EXEC up_get_newDocNr 'BO', 2, 'Loja 1', 1, ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_newDocNr]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_newDocNr
GO

CREATE PROCEDURE dbo.up_get_newDocNr 	
	@tabela VARCHAR(10)
	,@serie NUMERIC(5)
	,@site VARCHAR(20)
	,@gravaNum BIT = 0
	,@CorrigeReceita BIT = 0
	,@stamp varchar(36) = ''

AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tmpNewDocNr'))
		DROP TABLE #tmpNewDocNr

	DECLARE 
	@nrField VARCHAR(30)
	,@serieField VARCHAR(30)
	,@anoField VARCHAR(30)
	,@difhour int = 0
	,@reset bit = 0
	,@useSite bit = 0

	SET @difhour = (select isnull(numValue,0) from B_Parameters_site(nolock) where stamp='ADM0000000008' and site=@site)

	SET @nrField = CASE 
				WHEN @tabela = 'FT' THEN 'fno'
				WHEN @tabela = 'BO' THEN 'obrano'
				WHEN @tabela = 'RE' THEN 'rno'
				END

	SET @serieField =  CASE 
					WHEN @tabela = 'FT' THEN 'ndoc'
					WHEN @tabela = 'BO' THEN 'ndos'
					WHEN @tabela = 'RE' THEN 'ndoc'
					END

	SET @anoField =  CASE 
					WHEN @tabela = 'FT' THEN 'ftano'
					WHEN @tabela = 'BO' THEN 'boano'
					WHEN @tabela = 'RE' THEN 'reano'
					END


	SET @useSite = CASE 
					WHEN @tabela = 'FT' THEN (CASE WHEN (select site FROM td(nolock) where ndoc = @serie) <> '' THEN 1 ELSE 0 END)
					WHEN @tabela = 'BO' THEN (CASE WHEN (select site FROM ts(nolock) where ndos = @serie) <> '' THEN 1 ELSE 0 END)
					WHEN @tabela = 'RE' THEN  (select Bool from B_Parameters(nolock) where stamp = 'ADM0000000308')
				   END

	SET @reset = 	 CASE 
					WHEN @tabela = 'FT' THEN (SELECT resetNumeracaoAnual FROM td(nolock) where ndoc = @serie and site = @site)
					WHEN @tabela = 'BO' THEN (SELECT resetNumeracaoAnual FROM ts(nolock) where ndos = @serie and 1 = (CASE WHEN site <> '' THEN (CASE WHEN site = @site THEN 1 ELSE 0 END) ELSE 1 END))
					WHEN @tabela = 'RE' THEN (CASE 
												WHEN EXISTS (SELECT 1 FROM tsre(nolock) WHERE ndoc = @serie and (site = @site or @useSite = 0)) 
													THEN (SELECT resetNumeracaoAnual FROM tsre(nolock) WHERE ndoc = @serie and site = @site)
												ELSE
													(SELECT ISNULL(resetNumeracaoAnual, 0) FROM cm1(nolock) where cm = @serie )
											  END)
					END



	DECLARE @filtro VARCHAR(MAX) = CASE WHEN @reset = 1
									THEN ' and ' +@anoField  + IIF(@CorrigeReceita = 0, ' = YEAR(dateadd(HOUR, ' + LTRIM(@difhour) + ', getdate()))', '=' + str((select YEAR(ousrdata) from ft(nolock) where ftstamp = @stamp )))
								ELSE
									''
								END

	DECLARE @sql VARCHAR(MAX) = 'SELECT TOP 1 ' + @nrField + ' FROM ' + @tabela + ' (nolock) WHERE ' + (CASE WHEN @useSIte = 1 THEN 'site = ''' + @site + ''' and ' ELSE '' END) + @serieField + ' = ' + LTRIM(@serie) + ' ' +  @filtro

	SET @sql = @sql + ' order by ousrdata desc, ' + @nrField + ' desc, ousrhora desc'

	SET @sql = 'SELECT ISNULL((' + @sql + '), 0) + 1 AS newDocNr INTO #tmpNewDocNr'

	IF @gravaNum = 1
	BEGIN

		SET @sql = @sql + N'

IF (SELECT newDocNr FROM #tmpNewDocNr) <> 0
BEGIN
	INSERT INTO b_docNrLog
	(stamp, tabela, serie, newDocNr, site, ousrdata)
	SELECT
	LEFT(NEWID(), 15) + RIGHT(NEWID(), 10), ''' + @tabela + ''', ' + LTRIM(@serie) + ', newDocNr, ''' + @site + ''', getDate()
	FROM #tmpNewDocNr
END'

	END


SET @sql = @sql + N'
SELECT * FROM #tmpNewDocNr'

	print @sql
	EXECUTE (@sql)

GO
GRANT EXECUTE on dbo.up_get_newDocNr TO PUBLIC
GRANT Control on dbo.up_get_newDocNr TO PUBLIC
GO
