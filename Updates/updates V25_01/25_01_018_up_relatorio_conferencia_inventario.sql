/* Relatório Inventário com diferentes tipos de valorização e outros filtros
select * from empresa
	exec up_relatorio_conferencia_inventario '20210625', '2', 0, '', '', '', '', '', '', 'Loja 2', '', 'PCL' , 0, 0, 0,  -999
	exec up_relatorio_conferencia_inventario '20250101', '1', 0, '', '', '', '', '', '', 'Loja 1', '', 'PCL' , 0, 0, 0,  -999,1,1

	alterada a 20200907, JG
	a variavel @design deve abarcar uma string por forma a pesquisar todos os produtos que iniciem por ela
	e terminem com qq dado, ou seja:
	DESIGN like @design + '%'

	alterado a 20200923, JG
	alteração dos joins do departamento, seccao, categoria e segmento para as novas tabelas do HMR

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_conferencia_inventario]') IS NOT NULL
	drop procedure dbo.up_relatorio_conferencia_inventario
go

create procedure dbo.up_relatorio_conferencia_inventario
	@data		datetime,
	@armazem	int,
	@incluiInactivos bit,
	@ref		varchar(254),
	@design		varchar(254),
	@familia	varchar(max),
	@lab		varchar(254),
	@marca		varchar(200),
	@grphmgcode	varchar(6),
	@site		varchar(55),
	@atributosFam varchar(max),
	@tipovalorizacao varchar(3),
	@inluiPCL bit,
	@inluiPCT bit,
	@inluiPCP bit,
	--@incluiPV bit=0,
	@stockData numeric(9,0),
	@psicotropicos bit,
	@benzodiazepinas bit

/* with encryption */
AS 
BEGIN
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempFamilia'))
		DROP TABLE #TempFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempSL'))
		DROP TABLE #TempSL
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempFN'))
		DROP TABLE #TempFN
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempLOJAS'))
		DROP TABLE #TempLOJAS
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempInventario'))
		DROP TABLE #TempInventario
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#EmpresaArm'))
		DROP TABLE #EmpresaArm
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempPsicoBenzo'))
		DROP TABLE #TempPsicoBenzo

	SET @atributosFam = (CASE WHEN (@atributosFam IS NULL OR @atributosFam = '') THEN ';;;' ELSE @atributosFam END)

	DECLARE @departamento VARCHAR(254) = ''
	SET @departamento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 1),'') 
	DECLARE @sessao       VARCHAR(254) = ''
	set @sessao = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 2) ,'')
	DECLARE @categoria    VARCHAR(254) = ''
	SET @categoria = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 3),'') 
	DECLARE @segmento     VARCHAR(254) = ''
	SET @segmento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 4),'') 

	 -- tabela temp para filtros psico e benzo
	CREATE TABLE #TempPsicoBenzo (
		ref VARCHAR(254),
		psico BIT,
		benzo BIT
	)

	INSERT INTO #TempPsicoBenzo (ref, psico, benzo)
	SELECT
		fprod.ref,
		fprod.psico,
		fprod.benzo
	FROM
		fprod (NOLOCK)
	WHERE
		(@psicotropicos = 0 AND @benzodiazepinas = 0) 
		OR (@psicotropicos = 1 AND fprod.psico = 1)  
		OR (@benzodiazepinas = 1 AND fprod.benzo = 1) 


	/* Calc Loja e Armazens */
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)
	select 
		armazem
	into
		#EmpresaArm
	From
		empresa
		inner join empresa_arm on empresa.no = empresa_arm.empresa_no
	where
		empresa.site = case when @site = '' then empresa.site else @site end

		
	
	/* Calc Familia */
	Select 
		distinct ref 
	INTO
		#TempFamilia
	From
		stfami (nolock)
	Where
		ref in (Select items from dbo.up_splitToTable(@familia,','))
		Or @familia = ''
	/* Calc Movimentos */
	SELECT
		slstamp ,cm ,cmdesc ,sl.ref ,qtt ,sl.epcpond ,evu ,datalc ,sl.ousrhora ,ORIGEM ,armazem
	INTO
		#TempSl
	FROM
		sl (nolock)
		inner join st (nolock) ON st.site_nr=@site_nr and  sl.ref  COLLATE DATABASE_DEFAULT=st.ref COLLATE DATABASE_DEFAULT 
	Where
		datalc <= @data and ST.stns=0
		and sl.armazem = @armazem
	/* Calc Compras */
	SELECT	
		fnstamp
		,epv
		,ref
		,fo.data
		,fo.docnome
		,FOLANSL
		,fn.ousrhora
	INTO
		#TempFN
	FROM
		fn (nolock)
		inner join fo (nolock) on fo.fostamp = fn.fostamp
		Inner join cm1 (nolock) on cm1.cmdesc = fn.docnome
	Where	
		fn.docnome IN ('N/Guia Entrada','V/Factura','V/Guia Transp.')
		and cm1.FOLANSL = 1
		and	fo.data <= @data
		and fo.site = case when @site = '' then fo.site else @site end

	/* Calc Inventario */
	SELECT
		ARMAZEM
		,SA.REF
		,st.DESIGN
		,STOCK_DATA = 
			ISNULL((
				SELECT
					SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END )
				FROM	
					#TempSl sl (nolock) 
				WHERE	
					sl.ref = st.ref and St.stns=0
					and sl.datalc <= @data 
					and sl.armazem in (select armazem from #EmpresaArm)
				),0)
		,PCL_DATA = --CASE WHEN @inluiPCL=1 THEN (
			ROUND(ISNULL((
				SELECT	
					TOP 1 EVU 
				FROM	
					#TempSl SL (nolock) 
				WHERE	
					(ORIGEM = 'FO' OR ORIGEM = 'IF')
					AND SL.DATALC <= @data
					AND SL.REF = ST.REF
					AND EVU !=0
					AND sl.armazem in (select armazem from #EmpresaArm)
				ORDER BY 
					sl.datalc + sl.ousrhora DESC
			),st.EPCULT),2) --)
			--ELSE
			--	0
			--END
		,PCP_DATA	= --CASE WHEN @inluiPCP=1 THEN (
			ROUND(ISNULL((
				SELECT
					TOP 1 epcpond 
				FROM
					#TempSl
				WHERE
					ref=st.ref 
					AND datalc <= @data 
					AND #TEMPSL.ref = ST.REF
					AND #TEMPSL.armazem in (select armazem from #EmpresaArm)
				ORDER BY 
					datalc + ousrhora DESC
			), st.EPCPOND),2) --)
			--ELSE
			--	0
			--END
		,PCT_DATA		=	--CASE WHEN @inluiPCT=1 THEN (
			ISNULL((
				SELECT
					TOP 1 epv
				FROM
					#TempFN cteFn
				WHERE
					cteFn.REF = ST.REF
					AND cteFn.epv > 0
					AND cteFn.data <= @data
				ORDER BY
					cteFn.data + cteFn.ousrhora DESC
			),st.EPCUSTO)--)
			--ELSE
			--	0
			--END
-- Último preço de venda - 09-10-2017 JC
			,PVP_DATA	= --cast(case when @incluiPV=1 then (
			ISNULL((
				SELECT
					TOP 1 u_epvp
				FROM
					fi 
				WHERE
					ref=st.ref 
					AND rdata <= @data 
					--AND #TEMPSL.ref = ST.REF
					AND fi.armazem in (select armazem from #EmpresaArm)
					and fi.nmdoc in ('Venda a Dinheiro','Factura')
				ORDER BY
					rdata + ousrhora DESC
			),st.EPV1) --)
			--else
			--	0
			--end as numeric(15,2))
		,st.stns
		,st.familia
		,PRECOCUSTOPONDERADO_ACTUAL = st.EPCPOND
		,ULTIMOPRECOCUSTO_ACTUAL = st.EPCULT
		,PRECOCUSTODETABELA_ACTUAL = st.EPCUSTO
		,PRECOVENDA1 = st.epv1
		,IVA = (SELECT TAXA FROM taxasiva (nolock) WHERE codigo = TABIVA)
		,ST.UINTR as ULTIMAENTRADA
		,ST.USAID as ULTIMASAIDA
		,ST.u_lab
		,st.faminome
		,marca = st.usr1
		,grphmgcode = isnull(grphmgcode,'')
		,grphmgdescr = isnull(grphmgdescr,'')
		,local1 = st.local
		,local2 = st.u_local
		,local3 = st.u_local2	
		, ISNULL(A.descr,'') as departamento
		, ISNULL(B.descr,'') as seccao
		, ISNULL(C.descr,'') as categoria
		, ISNULL(D.descr,'') as segmento
		
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 1) valDepartamento
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 2) valSeccao
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 3) valCategoria
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 4) valSegmento
	INTO
		#TempInventario
	FROM
		SA (nolock)
		INNER JOIN ST (nolock) ON SA.REF = ST.REF
		left join fprod (nolock) on fprod.cnp = st.ref	
		left join grande_mercado_hmr (nolock) A on A.id = st.u_depstamp
		left join mercado_hmr (nolock) B on B.id = st.u_secstamp
		left join categoria_hmr (nolock) C on C.id = st.u_catstamp 
		left join segmento_hmr (nolock) D on D.id = st.u_segstamp	
		left join #TempPsicoBenzo PB on  PB.ref COLLATE DATABASE_DEFAULT=st.ref COLLATE DATABASE_DEFAULT
	WHERE
		st.stns = 0
		AND st.site_nr = @site_nr
		and st.ref like @ref + '%'
		and st.DESIGN like @design + '%'
		and isnull(st.inactivo,0) = case when @incluiInactivos = 1 then isnull(st.inactivo,0) else 0 end
		and (
			@psicotropicos = 0 AND @benzodiazepinas = 0  -- sem filtros
			OR (@psicotropicos = 1 AND PB.psico = 1)    -- psico
			OR (@benzodiazepinas = 1 AND PB.benzo = 1)  -- benzo
		)

	/* Result Set */
	Select
		total_valorizacao_data=
			Case 
				when @tipovalorizacao = 'PCL' then STOCK_DATA * PCL_DATA
				when @tipovalorizacao = 'PCT' then STOCK_DATA * PCT_DATA
				when @tipovalorizacao = 'PCP' then STOCK_DATA * PCP_DATA
				when @tipovalorizacao = 'PVP' then STOCK_DATA * PVP_DATA
			End
		,*
	From
		#TempInventario cteInventario
	where 
		cteInventario.armazem in (select armazem from #EmpresaArm)
		and cteInventario.stns = 0
		and	cteInventario.ref LIKE @ref + '%' 
	
		and cteInventario.design like @design + '%'

		and armazem = @armazem
		and (@familia = '' or @familia = '0'  or cteInventario.familia in (Select ref from #TempFamilia))
		and cteInventario.u_lab = case when @lab = '' then cteInventario.u_lab else @lab end 
		and marca = case when @marca = '' then marca else @marca end
		and grphmgcode = case when @grphmgcode = '' then grphmgcode else @grphmgcode end 
		AND departamento = (CASE WHEN @departamento = '' THEN departamento ELSE @departamento END)
		AND seccao = (CASE WHEN @sessao = '' THEN seccao ELSE  @sessao END)
		AND categoria = (CASE WHEN @categoria = '' THEN categoria ELSE  @categoria END)
		AND segmento = (CASE WHEN @segmento = '' THEN segmento ELSE  @segmento END) 
		and STOCK_DATA >= @stockData
	Order by
		design
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempPsicoBenzo'))
		DROP TABLE #TempPsicoBenzo
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempFamilia'))
		DROP TABLE #TempFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempSL'))
		DROP TABLE #TempSL
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempFN'))
		DROP TABLE #TempFN
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempInventario'))
		DROP TABLE #TempInventario
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#EmpresaArm'))
		DROP TABLE #EmpresaArm
END

GO
Grant Execute on dbo.up_relatorio_conferencia_inventario to Public
Grant control on dbo.up_relatorio_conferencia_inventario to Public
Go