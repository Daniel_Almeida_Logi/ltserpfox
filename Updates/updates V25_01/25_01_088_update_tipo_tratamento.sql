
update b_tipo_tratamento 
set tipo = 'Consulta Farmac�utica'
where tipo = 'Consulta M�dica'

if not Exists (Select * from b_tipo_tratamento where tipo = 'Dispensa em Proximidade') 
begin
 INSERT INTO [dbo].[b_tipo_tratamento]
           ([stamp]
           ,[tipo])
     VALUES
           (LEFT(NEWID(),25)
           ,'Dispensa em Proximidade')
end 