/* Relatório Quantidade Vendida Mensal - Relatório + Utilizado pelas Farmácias 
	

	exec up_relatorio_vendas_base_detalhe '20170727','20170728', '', 0
	exec up_relatorio_vendas_base_detalhe '20170727','20170728', 'Loja 2', 0
	exec up_relatorio_vendas_base_detalhe '20240101','20240821', 'Loja 1,Loja 2', 0 

	exec up_relatorio_vendas_qtVendidaMensal '20240801','20240821', '', '', '', '', 'Haleon', 'Loja 1,Loja 2' , 0, 1, 0, 0, '', -99999, 0, 0, 0, 0, '', '', '', '', '', '', -1, 1,'>=','' , 0,'','','QUANTIDADE',0,0
	exec up_relatorio_vendas_qtVendidaMensal '20210101', '20230712', '', '8705160', '', '', '', '' , 0, 0, 0, 0, '', 0, 0, 0, 0, 0, '', '', '', '', '', '', -1, 1,'>=','' , 0,'','','QUANTIDADE',0,1,0

	exec up_relatorio_vendas_qtVendidaMensal '20241201', '20250130', '', '', '', '', '', 'Loja 1' , 0, 0, 0, 0, '', 0, 0, 0, 0, 0,'', '', '', '', '', '', -1, 1,'>','' , 0,'','','QUANTIDADE',0,0,'',1,1
	exec up_relatorio_vendas_qtVendidaMensal '20241201', '20250130', '', '', '', '', '', 'Loja 1' , 0, 0, 0, 0, '', 0, 0, 0, 0, 0,'', '', '', '', '', '', -1, 1,'>','' , 0,'','','BB',0,0,'',1,0

*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_vendas_qtVendidaMensal]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_qtVendidaMensal
go

create procedure dbo.up_relatorio_vendas_qtVendidaMensal
	@dataIni							as datetime
	,@dataFim							as datetime
	,@op								as varchar(55)
	,@ref								as varchar(254)
	,@design							as varchar(254)
	,@familia							as varchar(max)
	,@lab								as varchar(254)
	,@site								as varchar(254)
	,@pvp								as bit
	,@stock								as bit
	,@pcl								as bit
	,@maxenc							as bit
	,@atributosFam						as varchar(max)
	,@qttvendida						as numeric(9,0)
	,@mostraProdutosNIncluidoVendasTag	as bit
	,@incluiHistorico					as bit = 0
	,@pvu								as bit = 0
	,@incluiServicos					as bit = 0
	,@fornecedor						as varchar(254)
	,@dci								as varchar(max)
	,@tipoProduto						as varchar(254)
	,@usr1								as varchar(254)
	,@generico							as varchar(18) = ''
	,@no								as varchar(9) = ''
	,@estab								as numeric(5,0) = -1
	,@ativos							as bit
	,@mostraProdutosNIncluidoVendasOp	as varchar(2)=''
	,@mostraProdutosNIncluidoVendas		as varchar(10)=''
	,@incliva							as bit
	,@cnpem								as varchar(max)					
	,@grphmgcode						as varchar(max)	
	,@opcaoSum							as varchar(50)
	,@t4t5								as BIT = 0
	,@totaisPorSites					as BIT =0
	,@armazem						    as varchar(60)=''
	,@IncMbpv							as bit = 0
	,@InclBb							as bit = 0

/* with encryption */
AS
BEGIN	
	
	set language portuguese
	If OBJECT_ID('tempdb.dbo.#dadosVendasBase') IS NOT NULL
		DROP TABLE #dadosVendasBase
	If OBJECT_ID('tempdb.dbo.#dadosST') IS NOT NULL
		DROP TABLE #dadosST
	If OBJECT_ID('tempdb.dbo.#dadosFamilia') IS NOT NULL
		DROP TABLE #dadosFamilia
	If OBJECT_ID('tempdb.dbo.#qtVendidas') IS NOT NULL
		DROP TABLE #qtVendidas
	If OBJECT_ID('tempdb.dbo.#dadosGerais') IS NOT NULL
		DROP TABLE #dadosGerais
	If OBJECT_ID('tempdb.dbo.#dadosResumidos') IS NOT NULL
		DROP TABLE #dadosResumidos
	If OBJECT_ID('tempdb.dbo.#EmpresaArm') IS NOT NULL
		DROP TABLE #EmpresaArm
	If OBJECT_ID('tempdb.dbo.#dadosMarcas') IS NOT NULL
		DROP TABLE #dadosMarcas
	If OBJECT_ID('tempdb.dbo.#dadosAnoMes') IS NOT NULL
		drop table #dadosAnoMes;
	If OBJECT_ID('tempdb.dbo.#dadosOperadores') IS NOT NULL
			drop table #dadosOperadores;
	If OBJECT_ID('tempdb.dbo.#dadosSTFiltrado') IS NOT NULL
		DROP TABLE #dadosSTFiltrado
	If OBJECT_ID('tempdb.dbo.#dadosAnoMesRef') IS NOT NULL
		drop table #dadosAnoMesRef;
	If OBJECT_ID('tempdb.dbo.#filtroRef') IS NOT NULL
		drop table #filtroRef;
	
	set @atributosFam = (case when (@atributosFam IS null OR @atributosFam = '') THEN ';;;' else @atributosFam END)

	DECLARE @departamento VARCHAR(254) = ''
	SET @departamento = ISNULL((select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 1),'') 
	DECLARE @sessao       VARCHAR(254) = ''
	set @sessao = ISNULL((select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 2) ,'')
	DECLARE @categoria    VARCHAR(254) = ''
	SET @categoria = ISNULL((select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 3),'') 
	DECLARE @segmento     VARCHAR(254) = ''
	SET @segmento = ISNULL((select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 4),'') 

	DECLARE @totaisSeparados BIT
	DECLARE @validaRef		 BIT = 0

	SET @totaisSeparados=0

	IF(@totaisPorSites =1 or @pvp = 1 OR @pcl=1 OR @maxenc=1)
	BEGIN 
		SET @totaisSeparados = 1
	END 

	IF(@site='TODAS')
		SET @site=''
	
	IF @site = ''
		SET @site = isnull((select  convert(varchar(254),(select 
						site + ', '
					FROM 
						empresa (nolock)							
					FOR XML PATH(''))) as no),0)
	


	IF @generico = 'GENERICO'
		SET @generico = 1
	ELSE IF @generico = 'NÃO GENERICO'
		SET @generico = 0

	SELECT 
		armazem
	INTO
		#EmpresaArm
	From
		empresa (nolock)
		inner join empresa_arm (NOLOCK) ON empresa.no = empresa_arm.empresa_no
	WHERE
		empresa.site in (SELECT items FROM dbo.up_splitToTable(@site, ',')) 
		AND ( empresa_arm.armazem in (SELECT items FROM dbo.up_splitToTable(@armazem,','))
			or @armazem = '' or @armazem = '0' )

--select * from #EmpresaArm

	    Declare @dataactual		AS DATETIME
		Declare @dateftFirst	AS DATETIME
		DECLARE @fdata			AS DATETIME 

		IF(@dataIni< DATEADD(year, -2,GETDATE()))
			BEGIN
				select TOP 1 @dateftFirst= CONVERT(date, case when fdata <=DATEADD(year, -2,GETDATE()) 
															  then  DATEADD(year, -2,GETDATE()) 
															  else fdata end) from ft (nolock)
				set  @dataactual = DATEADD(month, DATEDIFF(month, 0, @dateftFirst), 0) 
			END
		ELSE 
			BEGIN 
				set  @dataactual =@dataIni
			END 
			
		SELECT @dateftFirst as data, YEAR(@dateftFirst) as ano,MONTH(@dateftFirst) as mes INTO  #dadosAnoMes where 1=0
			
		WHILE @dataactual <= @dataFim
			BEGIN
				INSERT INTO #dadosAnoMes 
				SELECT @DATAACTUAL as data, year(@DATAACTUAL) as ano, month(@DATAACTUAL) as mes
				
				SET @DATAACTUAL = DATEADD(MM,1,@DATAACTUAL)
			END



	SELECT 
		valueText,
		site
	INTO
		#filtroRef
	FROM
		tempFilters(nolock)
	WHERE
		token = @ref and
		site in (Select items from dbo.up_splitToTable(@site, ',')) 

	/* Dados Base Vendas  */ 
	create table #dadosVendasBase (
		nrAtend varchar(20) 
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)
		
	)


    insert #dadosVendasBase
    exec up_relatorio_vendas_base_Detalhe @dataIni, @dataFim, @site, @incluiHistorico
	
	delete from 
			#dadosVendasBase 
		where  
			#dadosVendasBase.no between 1 and 198  
			and #dadosVendasBase.loja_nr in (select no from empresa (nolock) WHERE tipoempresa in ('FARMACIA', 'PARAFARMACIA') )

	delete from 
		#dadosVendasBase 
	where 
	 #dadosVendasBase.ftstamp COLLATE DATABASE_DEFAULT not in 
														(select 
															ftstamp COLLATE DATABASE_DEFAULT 
														 from  FI 
															 INNER JOIN empresa_arm		ON empresa_arm.armazem=FI.armazem 
																WHERE ( empresa_arm.armazem in 
																(select items from dbo.up_splitToTable(@armazem,','))
																or @armazem = '' or @armazem = '0' ) 
																AND FI.rdata BETWEEN @dataIni AND @dataFim)
		
--select * from #dadosVendasBase  where ref= '6438739'


	/* Calc Dados Produto */	
	Select distinct
		st.ref
		,st.design
		,stock = isnull((select SUM(stock) from sa (nolock) where sa.ref = st.ref 
						and armazem in (select armazem from #EmpresaArm where armazem in (select armazem from empresa_arm where empresa_no = st.site_nr))),st.stock)	
		,st.u_lab
		,st.usr1
		,st.familia
		,epv1 
		,epv1Siva = case when st.ivaincl = 1 then ISNULL(st.epv1,1) / (isnull(taxasiva.taxa,1)/100+1) else ISNULL(st.epv1,1) end
		,st.inactivo
		,epcult
		,st.stmax
		,st.ptoenc
		,st.fornecedor	
		, ISNULL(A.descr,'') as departamento
		, ISNULL(B.descr,'') as seccao
		, ISNULL(C.descr,'') as categoria
		, ISNULL(D.descr,'') as segmento
		,tipoProduto =  isnull(b_famFamilias.design,'')
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 1) valDepartamento
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 2) valSeccao
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 3) valCategoria
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 4) valSegmento
		,st.ivaincl
		,st.stns
		,fprod.dci
		,st.site_nr
		,empresa.site
		,fprod.dcipt_id
		,fprod.cnpem
		,fprod.grphmgcode
		, ISNULL(CASE WHEN fprod.pvporig <= fprod.preco_acordo and fprod.preco_acordo > 0 THEN 'T4'
						WHEN ranking=1 and grphmgcode != 'GH0000' and grphmgcode != '' and fprod.pvporig <= fprod.pvpmaxre 
						THEN 'T5'  else '' END,'')			AS T4T5 
	    , ISNULL(CASE WHEN fprod.pvporig <= fprod.preco_acordo and fprod.preco_acordo > 0 THEN 1
						WHEN ranking=1 and grphmgcode != 'GH0000' and grphmgcode != '' and fprod.pvporig <= fprod.pvpmaxre 
						THEN 1   END,0)						AS T4T5Bit 		
		, st.marg4 as bb
	into 
		#dadosST
	From
		st (nolock)
		inner join empresa (nolock) on empresa.no = st.site_nr
		inner join empresa_arm (nolock) on empresa.no = empresa_arm.empresa_no
		left join fprod (nolock) on st.ref = fprod.cnp
		left join b_famFamilias (nolock) on st.u_famstamp = b_famFamilias.famstamp	
		left join grande_mercado_hmr (nolock) A on A.id = st.u_depstamp
		left join mercado_hmr (nolock) B on B.id = st.u_secstamp
		left join categoria_hmr (nolock) C on C.id = st.u_catstamp 
		left join segmento_hmr (nolock) D on D.id = st.u_segstamp				
		left join taxasiva (nolock) on st.tabiva = taxasiva.codigo 
	where
		empresa.site in (Select items from dbo.up_splitToTable(@site, ',')) 
		AND  (
			st.usr1 in (select items from dbo.up_splitToTable(@usr1,','))
			or @usr1 = '0'
			or @usr1 = ''
		)
		and isnull(fprod.generico,0) = case when @generico = '' then isnull(fprod.generico,0) else @generico end
		and st.inactivo between 0 and (case when @ativos=1 then 0 else 1 end)
		and (isnull(fprod.grphmgcode,'') in (Select items from dbo.up_splitToTable(@grphmgcode,',')) or @grphmgcode = '')

--select * from #dadosST

	/* Calc Familias */
	Select
		distinct ref
	into
		#dadosFamilia
	From
		stfami (nolock)
	Where
		ref in (Select items from dbo.up_splitToTable(@familia,','))
		Or @familia = ''


	/* Calc operadores */
	Select
		iniciais,
		cm = userno,
		username
	into
		#dadosOperadores
	from
		b_us (nolock)
	Where
		userno in (select items from dbo.up_splitToTable(@op,','))
		or @op= '0'
		or @op= ''

	--select * from #dadosST 
	--INNER JOIN #dadosVendasBase ON #dadosST.ref = #dadosVendasBase.ref
	--WHERE seccao ='Bebé e mamã'
  

	/* Calc Result Set */
	SELECT 
		'ANO'			= year(fdata)
		,'MES'			= STR(MONTH(fdata)) + ' - ' + LEFT(UPPER(datename(month,fdata)),3)
		,'REF'			= cteSt.ref
		,'DESIGN'		= cteSt.design
		,'QTD'			= SUM(ft.qtt)
		,'STOCKACTUAL'	= cteSt.stock
		,'STOCKMAX'		= cteSt.stmax
		,'PTOENC'		= cteSt.ptoenc
		,'FAMILIA'		= ft.familia
		,'LAB'			= u_lab
		,vdpvpcl		= SUM(etiliquido)
		,vdpvp			= SUM(etiliquido + ettent1 + ettent2)
		,vdpvcl			= SUM(etiliquidoSiva)
		,vdpv			= SUM(etiliquidoSiva + ettent1Siva + ettent2Siva)
		,'INACTIVO'		= cteSt.inactivo
		,pclficha		= cteSt.epcult
		,vdpcl			= SUM(ft.ecusto)
		,servico		= cteSt.stns
		,pvFicha		= cteSt.epv1Siva
		,pvpFicha		= cteSt.epv1
		,dci			= cteSt.dci
		,usr1			= cteSt.usr1
		,opcaoS         =  CASE @opcaoSum WHEN 'VALOR' THEN (CASE WHEN @incliva=1 
																  THEN etiliquido + ettent1 + ettent2 
																  ELSE etiliquidoSiva + ettent1Siva + ettent2Siva END)
											  WHEN 'BB'    THEN cteSt.bb --(etiliquidoSIva + ettent1SIva + ettent2SIva) - epcpond
											  WHEN 'QUANTIDADE'	  THEN SUM(ft.qtt) END 
		,case when @totaisSeparados=1  then  cTeSt.site else cTeSt.site  end site 
		,'STOCKACTUALAux'= cteSt.stock
		,ROW_NUMBER() OVER(PARTITION BY  cteSt.ref , cteSt.site ORDER BY cteSt.ref DESC, cteSt.site DESC) AS rowline 
		, '1' as rowsubline
		,MONTH(fdata) AS monthint
		, LEFT(UPPER(datename(month,fdata)),3) as MESNome
	    , MBPV = Isnull((Sum((etiliquidoSIva + ettent1SIva + ettent2SIva) - epcpond) / NULLIF(SUM(etiliquidoSiva + ettent1Siva + ettent2Siva),0))*100,0)
		, bb = cteSt.bb*SUM(ft.qtt)  
	into
		#qtVendidas
	FROM 
		#dadosVendasBase ft
		inner join #dadosST cteSt on ft.ref = cteSt.ref and ft.loja_nr = cteSt.site_nr
		inner join empresa (nolock) on empresa.site = cteSt.site
		left join fprod (nolock) on ft.ref = fprod.cnp
	WHERE
		ft.no = case when @no = '' then ft.no else @no end		
		and ( ft.no = 0 or ft.no > 198 or empresa.tipoempresa = 'CLINICA' )		 		
		and ft.estab = case when @estab>-1 then  @estab else ft.estab  end
		and	(ft.tipodoc!=4 or ft.u_tipodoc=4) AND ft.u_tipodoc != 1 AND ft.u_tipodoc != 5
		and (ft.ref in (Select valueText from #filtroRef ) or @ref = '')
		and cteSt.DESIGN like '%' + @design + '%'
		and ft.familia in (Select ref from #dadosFamilia)
		and ft.ousrinis in (select iniciais from #dadosOperadores)
		and (isnull(cteSt.u_lab,'') in (Select items from dbo.up_splitToTable(@lab,';')) or @lab = '')
		and isnull(ctest.fornecedor,'') = case when @fornecedor = '' then isnull(ctest.fornecedor,'') else @fornecedor end
		and (isnull(cteSt.dcipt_id,'') in (Select items from dbo.up_splitToTable(@dci,';')) or @dci = '')
		and (isnull(cteSt.cnpem,'') in (Select items from dbo.up_splitToTable(@cnpem,';')) or @cnpem = '')
		and (isnull(cteSt.grphmgcode,'') in (Select items from dbo.up_splitToTable(@grphmgcode,';')) or @grphmgcode = '') 
		and isnull(cteSt.tipoProduto, '') = case when @tipoProduto = '' then isnull(cteSt.tipoProduto, '') else @tipoProduto end
		AND cteSt.T4T5Bit = (CASE WHEN @t4t5 = 0 THEN cteSt.T4T5Bit ELSE  @t4t5 END)
		
		
		and departamento = (CASE WHEN @departamento = '' THEN departamento ELSE @departamento END)
		and seccao = (CASE WHEN @sessao = '' THEN seccao ELSE  @sessao END)
		and categoria = (CASE WHEN @categoria = '' THEN categoria ELSE  @categoria END)
		and segmento = (CASE WHEN @segmento = '' THEN segmento ELSE  @segmento END) 
		 
		and (isnull(fprod.grphmgcode,'') in (Select items from dbo.up_splitToTable(@grphmgcode,',')) or @grphmgcode = '') 


	GROUP BY 
		year(fdata)
		,datename(MONTH,fdata)
		,MONTH(fdata)
		,cteSt.ref
		,cteSt.design
		,cteSt.stock
		,ft.familia
		,cteSt.u_lab
		,cteSt.epv1
		,cteSt.inactivo
		,cteSt.epcult
		,cteSt.stmax
		,cteSt.ptoenc
		,cteSt.epv1Siva
		,cteSt.stns
		,ctest.dci
		,cteSt.usr1
		,cTeSt.site
		,etiliquidoSiva
		,ettent1siva
		,ettent2siva
		,epcpond
		,ndoc
		,etiliquido , ettent1 , ettent2
		,cteSt.bb

--select * from #dadosST
--select * from #qtVendidas

	/*Elimina Qtt Vendida Inferir a x */
	if @qttvendida > 0
	delete from #qtVendidas where ref in (select ref from #qtVendidas group by ref having sum(qtd) < @qttvendida ) 
		
	/* Elimina Servicos*/
	if @incluiServicos = 0
	delete from #qtVendidas where servico = 1

	/* Inclui todos os produtos, mesmo os que não estão incluidos nas vendas, está separado porque esta opção é muito mais lenta */
	IF @mostraProdutosNIncluidoVendasTag = 1 
		BEGIN 
				
			select top 0
				*
			into
				#dadosSTFiltrado
			From #dadosST

			if  @mostraProdutosNIncluidoVendasOp = '<'
				begin 
					insert into #dadosSTFiltrado
					select * from #dadosST
					where
						 #dadosST.stock < @mostraProdutosNIncluidoVendas
				End
			else if @mostraProdutosNIncluidoVendasOp = '='
				begin 
					insert into #dadosSTFiltrado
					select * from #dadosST
					where
						 #dadosST.stock = @mostraProdutosNIncluidoVendas
				End
			else if @mostraProdutosNIncluidoVendasOp = '>'
				begin 
					insert into #dadosSTFiltrado
					select * from #dadosST
					where
						 #dadosST.stock > @mostraProdutosNIncluidoVendas
				End
			else if @mostraProdutosNIncluidoVendasOp = '>='
				begin 
					insert into #dadosSTFiltrado
					select * from #dadosST
					where
						 #dadosST.stock >= @mostraProdutosNIncluidoVendas
				End
			else if @mostraProdutosNIncluidoVendasOp = '<='
				begin 
					insert into #dadosSTFiltrado
					select * from #dadosST
					where
						 #dadosST.stock <= @mostraProdutosNIncluidoVendas
				End
			else 
				begin 
					insert into #dadosSTFiltrado
					select * from #dadosST
				End

		--select * from #dadosSTFiltrado  where ref='5440987'

			insert into #qtVendidas
			SELECT	
				'ANO'			= dam.ano
				,'MES'			= STR(MONTH(data)) + ' - ' + LEFT(UPPER(datename(month,data)),3)
				,'REF'			= st.ref
				,'DESIGN'		= st.design
				,'QTD'			= 0
				,'STOCKACTUAL'	= st.stock
				,'STOCKMAX'		= st.stmax
				,'PTOENC'		= st.ptoenc
				,'FAMILIA'		= st.familia
				,'LAB'			= st.u_lab
				,vdpvpcl		= 0
				,vdpvp			= 0
				,vdpvcl			= 0
				,vdpv			= 0
				,'INACTIVO'		= st.inactivo
				,pclficha		= st.epcult
				,vdpcl			= 0
				,servico		= st.stns
				,pvFicha		= st.epv1Siva
				,pvpFicha		= st.epv1
				,dci			= st.dci
				,usr1			= st.usr1
				,opcaoS         = 0
				, st.site
				,'STOCKACTUALAux'	= st.stock
				,ROW_NUMBER() OVER(PARTITION BY  st.ref , st.site ORDER BY st.ref DESC, st.site DESC)  AS rowline 
				, '2' AS rowsubline
				,MONTH(data) AS monthint
				, LEFT(UPPER(datename(month,data)),3) as MESNome
				,mbpv			= 0
				,bb				= 0
			from 
				#dadosAnoMes dam
				,#dadosSTFiltrado st 
				left join fprod (nolock) on st.ref = fprod.cnp
			Where
				 (st.ref in (Select valueText from #filtroRef ) or @ref = '')
				and st.DESIGN like + '%' + @design + '%'
				and st.familia in (Select ref from #dadosFamilia)
				and (isnull(st.u_lab,'') in (Select items from dbo.up_splitToTable(@lab,';')) or @lab = '')
				and isnull(st.fornecedor,'') = case when @fornecedor = '' then isnull(st.fornecedor,'') else @fornecedor end
				and (isnull(st.dcipt_id,'') in (Select items from dbo.up_splitToTable(@dci,';')) or @dci = '')
				and (isnull(st.cnpem,'') in (Select items from dbo.up_splitToTable(@cnpem,';')) or @cnpem = '')
				and (isnull(st.grphmgcode,'') in (Select items from dbo.up_splitToTable(@grphmgcode,';')) or @grphmgcode = '') 
				and isnull(st.tipoProduto, '') = case when @tipoProduto = '' then isnull(st.tipoProduto, '') else @tipoProduto end
				and departamento = (CASE WHEN @departamento = '' THEN departamento ELSE @departamento END)
				and seccao = (CASE WHEN @sessao = '' THEN seccao ELSE  @sessao END)
				and categoria = (CASE WHEN @categoria = '' THEN categoria ELSE  @categoria END)
				and segmento = (CASE WHEN @segmento = '' THEN segmento ELSE  @segmento END) 
		END

	--select * from #qtVendidas  where ref= '6438739'

	IF @totaisSeparados = 1
		/* create #dadosSTFiltrado */
		BEGIN 
			select top 0
				dam.*,
				#qtVendidas.ref,
				#qtVendidas.site as site
			into
				 #dadosAnoMesRef
			from 
				#dadosAnoMes dam,
				#qtVendidas
			Group by ref, site, data,dam.ano, dam.mes
			-- Where #qtVendidas.ano != dam.ano or  #qtVendidas.monthint != dam.mes

			insert into #dadosAnoMesRef
			SELECT
				dam.*,
				#qtVendidas.ref,
				#qtVendidas.site as site
			from 
				#dadosAnoMes dam
				, #qtVendidas 
			Group by ref, site, data,dam.ano, dam.mes
		
		
			delete 
				#dadosAnoMesRef  
			FROM 
				#dadosAnoMesRef 
				INNER JOIN #qtVendidas  ON (#dadosAnoMesRef.ref=#qtVendidas.REF and 
											#dadosAnoMesRef.ano = #qtVendidas.ano and
											#dadosAnoMesRef.mes = #qtVendidas.monthint and
											#dadosAnoMesRef.site= #qtVendidas.site)
		
		--select * from #dadosAnoMesRef
		
		insert into #qtVendidas	
		SELECT	
			'ANO'			= damRef.ano
			,'MES'			= STR(MONTH(data)) + ' - ' + LEFT(UPPER(datename(month,data)),3)
			,'REF'			= st.ref
			,'DESIGN'		= st.design
			,'QTD'			= 0
			,'STOCKACTUAL'	= st.stock
			,'STOCKMAX'		= st.stmax
			,'PTOENC'		= st.ptoenc
			,'FAMILIA'		= st.familia
			,'LAB'			= st.u_lab
			,vdpvpcl		= 0
			,vdpvp			= 0
			,vdpvcl			= 0
			,vdpv			= 0
			,'INACTIVO'		= st.inactivo
			,pclficha		= st.epcult
			,vdpcl			= 0
			,servico		= st.stns
			,pvFicha		= st.epv1Siva
			,pvpFicha		= st.epv1
			,dci			= st.dci
			,usr1			= st.usr1
			,opcaoS         = 0
			,site			= st.site
			,'STOCKACTUALAux'	= st.stock
			,ROW_NUMBER() OVER(PARTITION BY  st.ref , st.site ORDER BY st.ref DESC, st.site DESC)  AS rowline 
			, '2' AS rowsubline
			,MONTH(data) AS monthint
			, LEFT(UPPER(datename(month,data)),3) as MESNome
			,mbpv			= 0
			,bb				= 0
		from 
			#dadosAnoMesRef damRef
			INNER JOIN #dadosST st  ON ST.REF = damRef.REF and ST.site=damRef.site
			left join fprod (nolock) on st.ref = fprod.cnp
		Where
			(st.ref in (Select valueText from #filtroRef ) or @ref = '')
			and st.DESIGN like '%' + @design + '%'
			and st.familia in (Select ref from #dadosFamilia)
			and (isnull(st.u_lab,'') in (Select items from dbo.up_splitToTable(@lab,';')) or @lab = '')
			and isnull(st.fornecedor,'') = case when @fornecedor = '' then isnull(st.fornecedor,'') else @fornecedor end
			and isnull(st.dci,'') = case when @dci = '' then isnull(st.dci,'') else @dci end
			and isnull(st.tipoProduto, '') = case when @tipoProduto = '' then isnull(st.tipoProduto, '') else @tipoProduto end
			and 
				(
					(departamento	= valDepartamento or valDepartamento is null)
					or (seccao		= valSeccao or valSeccao is null)
					or (categoria	= valCategoria or valCategoria is null)
					or (segmento	= valSegmento or valSegmento is null)
				)   
		
			update #qtVendidas set STOCKACTUALAux=0, STOCKACTUAL=0 where  rowline !=1 or 
									(rowsubline=2 and rowline=1 and 
									isnull((select count(*) from #qtVendidas qt1 where
										 qt1.ref=#qtVendidas.ref and qt1.site=#qtVendidas.site and rowsubline=1 and rowsubline=1 ),0)>=1 )
			

			Select
				 * , @totaisSeparados as totaisSeparados 
			from 
				#qtVendidas 
			order by 
				DESIGN,ref,site OPTION (RECOMPILE)
		END 
	ELSE
		BEGIN 
			update #qtVendidas set STOCKACTUALAux=0, STOCKACTUAL=0 where  rowline !=1 or
									 (rowsubline=2 and rowline=1 and
									  isnull((select count(*) from #qtVendidas qt1 where
										 qt1.ref=#qtVendidas.ref and qt1.site=#qtVendidas.site and rowsubline=1 and rowsubline=1 ),0)>=1 )

			declare @sql		varchar(max) = ''
			declare @sqlSelect	varchar(max) = ''
			declare @sqlFrom	varchar(max) = ''
			declare @sqlGroupBy varchar(max) = ''
			declare @sqlOrderBy varchar(max) = ''
			
			set @sqlGroupBy = @sqlGroupBy	+ N' group by ANO,MES,REF,DESIGN,QTD,STOCKACTUAL, STOCKACTUALAux, site,
												 MESNome,opcaoS,FAMILIA,LAB,INACTIVO,dci,usr1,servico'
			set @sqlFrom	= @sqlFrom		+ N' from #qtVendidas'
			set @sqlOrderBy = @sqlOrderBy	+ N' order by DESIGN,ref,site OPTION (RECOMPILE)'
			
			set @sqlSelect= N'		
					SELECT ANO,MES,REF,DESIGN,QTD,STOCKACTUAL, STOCKACTUALAux,#qtVendidas.site as site,
					'+ convert(varchar(254),@totaisSeparados) +' as totaisSeparados ,MESNome,SUM(opcaoS) AS opcaoS
						,FAMILIA,LAB,INACTIVO,dci,usr1,servico'
						IF @pvp = 1 and @incliva = 1
						BEGIN
							set @sqlSelect = @sqlSelect	  + N',pvpFicha,vdpvp,vdpvcl,vdpv '
							set @sqlGroupBy = @sqlGroupBy + N'pvpFicha,vdpvp,vdpvcl,vdpv '
						END	
						IF @pcl = 1
						BEGIN
							set @sqlSelect  = @sqlSelect  + N',vdpcl, pclficha, vdpvpcl'
							set @sqlGroupBy = @sqlGroupBy + N',vdpcl, pclficha, vdpvpcl'
						END
						IF @maxenc = 1
						BEGIN
							set @sqlSelect  = @sqlSelect  + N',PTOENC'
							set @sqlGroupBy = @sqlGroupBy + N',PTOENC'
						END
						IF @IncMbpv = 1
						BEGIN
							set @sqlSelect  = @sqlSelect  + N',mbpv'
							set @sqlGroupBy = @sqlGroupBy + N',mbpv'
						END
						IF @InclBb = 1
						BEGIN
							set @sqlSelect  = @sqlSelect  + N',bb'
							set @sqlGroupBy = @sqlGroupBy + N',bb'
						END

			set @sql =	@sqlSelect + @sqlFrom + @sqlGroupBy + @sqlOrderBy
			--print @sql
			exec(@sql)

		END 

	If OBJECT_ID('tempdb.dbo.#dadosVendasBase') IS NOT NULL
		DROP TABLE #dadosVendasBase
	If OBJECT_ID('tempdb.dbo.#dadosST') IS NOT NULL
		DROP TABLE #dadosST
	If OBJECT_ID('tempdb.dbo.#dadosFamilia') IS NOT NULL
		DROP TABLE #dadosFamilia
	If OBJECT_ID('tempdb.dbo.#qtVendidas') IS NOT NULL
		DROP TABLE #qtVendidas
	If OBJECT_ID('tempdb.dbo.#dadosGerais') IS NOT NULL
		DROP TABLE #dadosGerais
	If OBJECT_ID('tempdb.dbo.#dadosResumidos') IS NOT NULL
		DROP TABLE #dadosResumidos
	If OBJECT_ID('tempdb.dbo.#EmpresaArm') IS NOT NULL
		DROP TABLE #EmpresaArm
	If OBJECT_ID('tempdb.dbo.#dadosMarcas') IS NOT NULL
		DROP TABLE #dadosMarcas
	If OBJECT_ID('tempdb.dbo.#dadosAnoMes') IS NOT NULL
			drop table #dadosAnoMes;
	If OBJECT_ID('tempdb.dbo.#dadosOperadores') IS NOT NULL
			drop table #dadosOperadores;
	If OBJECT_ID('tempdb.dbo.#dadosSTFiltrado') IS NOT NULL
		DROP TABLE #dadosSTFiltrado
	If OBJECT_ID('tempdb.dbo.#dadosAnoMesRef') IS NOT NULL
		drop table #dadosAnoMesRef;
	If OBJECT_ID('tempdb.dbo.#filtroRef') IS NOT NULL
		drop table #filtroRef;
END

GO
Grant Execute on dbo.up_relatorio_vendas_qtVendidaMensal to Public
Grant control on dbo.up_relatorio_vendas_qtVendidaMensal to Public
GO