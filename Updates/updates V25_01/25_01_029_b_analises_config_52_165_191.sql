/*
select * From b_analises where report like '%mensal%' 

Relatório Evolução Mensal Vendas -- relatorio_vendas_qtEvolucaoMensalVendas -- 52
Relatório Evolução Mensal Vendas Simplificado - relatorio_vendas_qtEvolucaoMensalVendas_simplificado -- 191
Relatório Quantidade Vendida Mensal Detalhe Lojas -- relatorio_vendas_qtVendidaMensalLojas -- 165

select * from b_analises_config where ordem = '52' order by fordem 
select * from b_analises_config where ordem = '191' order by fordem 
select * from b_analises_config where ordem = '165' order by fordem 
*/

-- Novo parâmetro 'incl. BB' no Relatório Evolução Mensal Vendas
IF NOT EXISTS (
    SELECT 1 
    FROM b_analises_config 
    WHERE ordem = 52 AND nome = 'InclBb'
)
BEGIN
    INSERT INTO b_analises_config (ordem, nome, label, tipo, fordem, visivel, obrigatorio, firstempty)
    VALUES (
        52,
        'InclBb',
        'Incl. BB',
        'l',
        '35',
        1,
        0,
		0
    );
END
ELSE
BEGIN
    DELETE FROM b_analises_config 
     WHERE ordem = 52 AND nome = 'InclBb'

    INSERT INTO b_analises_config (ordem, nome, label, tipo, fordem, visivel, obrigatorio, firstempty)
    VALUES (
        52,
        'InclBb',
        'Incl. BB',
        'l',
        '35',
        1,
        0,
		0
    );
END;


-- Novo parâmetro InclBb no relatorio Evolução Mensal Vendas Simplificado
IF NOT EXISTS (
    SELECT 1 
    FROM b_analises_config 
    WHERE ordem = 191 AND nome = 'InclBb'
)
BEGIN
	INSERT INTO b_analises_config (ordem, nome, label, tipo, fordem, visivel, obrigatorio, firstempty)
    VALUES (
        191,
        'InclBb',
        'Incl. BB',
        'l',
        '35',
        1,
        0,
		0
    );
END
ELSE
BEGIN
    DELETE FROM b_analises_config 
    WHERE ordem = 191 AND nome = 'InclBb';

    INSERT INTO b_analises_config (ordem, nome, label, tipo, fordem, visivel, obrigatorio, firstempty)
    VALUES (
        191,
        'InclBb',
        'Incl. BB',
        'l',
        '35',
        1,
        0,
		0
    );
END;

-- Novo parâmetro InclBb no relatorio Quantidade Vendida Mensal Detalhe Lojas
IF NOT EXISTS (
    SELECT 1 
    FROM b_analises_config 
    WHERE ordem = 165 AND nome = 'InclBb'
)
BEGIN
	INSERT INTO b_analises_config (ordem, nome, label, tipo, fordem, visivel, obrigatorio, firstempty)
    VALUES (
        165,
        'InclBb',
        'Incl. BB',
        'l',
        '34',
        1,
        0,
		0
    );
END
ELSE
BEGIN
    DELETE FROM b_analises_config 
    WHERE ordem = 165 AND nome = 'InclBb';

    INSERT INTO b_analises_config (ordem, nome, label, tipo, fordem, visivel, obrigatorio, firstempty)
    VALUES (
        165,
        'InclBb',
        'Incl. BB',
        'l',
        '34',
        1,
        0,
		0
    );
END;



-- edita parâmetro estab no relatorio Evolução Mensal Vendas detalhe
IF NOT EXISTS (
    SELECT 1 
    FROM b_analises_config 
    WHERE ordem = 165 AND nome = 'estab'
)
BEGIN
	INSERT INTO b_analises_config (ordem, nome, label, tipo, fordem, visivel, obrigatorio, firstempty, vdefault, vdefaultsql, vdefaultfox, mascara, formato, multiselecao, comandoesql)
    VALUES (
        165,
        'estab',
        'Dep.',
        't',
        '6',
        1,
        0,
		1,
		0,
		0,
		0,
		'9999999999999',
		'9999999999999',
		0,
		0
    );
END
ELSE
BEGIN
    DELETE FROM b_analises_config 
    WHERE ordem = 165 AND nome = 'estab';

    INSERT INTO b_analises_config (ordem, nome, label, tipo, fordem, visivel, obrigatorio, firstempty, vdefault, vdefaultsql, vdefaultfox, mascara, formato, multiselecao, comandoesql)
    VALUES (
        165,
        'estab',
        'Dep.',
        't',
        '6',
        1,
        0,
		1,
		0,
		0,
		0,
		'9999999999999',
		'9999999999999',
		0,
		0
    );
END;
