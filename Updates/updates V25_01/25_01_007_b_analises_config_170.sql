/*--select alertIdReturn From fi_trans_info (nolock)

select * From b_analises (nolock) where report = 'relatorio_Informacao_NMVO'

select * From b_analises_config (nolock) where ordem = '170' and nome = 'lote'

*/

-- Novo parâmetro lote
IF NOT EXISTS (
    SELECT 1 
    FROM b_analises_config 
    WHERE ordem = 170 AND nome = 'lote'
)
BEGIN
    INSERT INTO b_analises_config (ordem, nome, label, tipo, comando, fordem, visivel, obrigatorio, firstempty)
    VALUES (
        170,
        'lote',
        'Lote',
        't',
        '',
        '9',
        1,
        0,
		1
    );
END
ELSE
BEGIN
    DELETE FROM b_analises_config 
    WHERE ordem = 170 AND nome = 'lote';

    INSERT INTO b_analises_config (ordem, nome, label, tipo, comando, fordem, visivel, obrigatorio, firstempty)
    VALUES (
        170,
        'lote',
        'Lote',
        't',
        '',
        '9',
        1,
        0,
		1
    );
END;

-- Update status
IF EXISTS (
    SELECT 1 
    FROM b_analises_config 
    WHERE nome = 'status' AND ordem = 170
)
BEGIN
    UPDATE b_analises_config
    SET comando = 'select distinct sel = convert(bit,0), texto = ''C\Erro'' UNION ALL 
    select distinct sel = convert(bit,0), texto = ''S\Erro'' UNION ALL 
    select distinct sel = convert(bit,0), texto = ''C\Alerta'''
    WHERE nome = 'status' AND ordem = 170;
END;