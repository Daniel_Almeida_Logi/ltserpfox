

if exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'paymentCode_ext' AND TABLE_SCHEMA = 'dbo')
BEGIN
	drop table [paymentCode_ext]
END



/****** Object:  Table [dbo].[paymentCode_ext]    Script Date: 13/02/2025 17:31:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[paymentCode_ext](
	[stamp] [varchar](36) NOT NULL,
	[code] [varchar](50) NOT NULL,
	[codeDesc] [varchar](50) NOT NULL,
	[type] [int] NOT NULL,
	[typeDesc] [varchar](254) NOT NULL,
	[receiverid] [varchar](100) NOT NULL,
	[receivername] [varchar](254) NOT NULL,
	[finalState]	bit not null ,
	[ousrinis] [varchar](30) NOT NULL,
	[ousrdata] [datetime] NOT NULL,
	[usrinis] [varchar](30) NOT NULL,
	[usrdata] [datetime] NOT NULL,
 CONSTRAINT [PK_paymentCode_ext] PRIMARY KEY CLUSTERED 
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[paymentCode_ext] ADD  CONSTRAINT [DF_paymentCode_ext_code]  DEFAULT ('') FOR [code]
GO

ALTER TABLE [dbo].[paymentCode_ext] ADD  CONSTRAINT [DF_paymentCode_ext_codeDesc]  DEFAULT ('') FOR [codeDesc]
GO

ALTER TABLE [dbo].[paymentCode_ext] ADD  CONSTRAINT [DF_paymentCode_ext_type]  DEFAULT ('0') FOR [type]
GO

ALTER TABLE [dbo].[paymentCode_ext] ADD  CONSTRAINT [DF_paymentCode_ext_typeDesc]  DEFAULT ('') FOR [typeDesc]
GO

ALTER TABLE [dbo].[paymentCode_ext] ADD  CONSTRAINT [DF_paymentCode_ext_receiverid]  DEFAULT ('') FOR [receiverid]
GO

ALTER TABLE [dbo].[paymentCode_ext] ADD  CONSTRAINT [DF_paymentCode_ext_receivername]  DEFAULT ('') FOR [receivername]
GO

ALTER TABLE [dbo].[paymentCode_ext] ADD  CONSTRAINT [DF_paymentCode_ext_finalState]  DEFAULT ('0') FOR [finalState]
GO

ALTER TABLE [dbo].[paymentCode_ext] ADD  CONSTRAINT [DF_paymentCode_ext_ousrinis]  DEFAULT ('') FOR [ousrinis]
GO

ALTER TABLE [dbo].[paymentCode_ext] ADD  CONSTRAINT [DF_paymentCode_ext_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]
GO

ALTER TABLE [dbo].[paymentCode_ext] ADD  CONSTRAINT [DF_paymentCode_ext_usrinis]  DEFAULT ('') FOR [usrinis]
GO

ALTER TABLE [dbo].[paymentCode_ext] ADD  CONSTRAINT [DF_paymentCode_ext_usrdata]  DEFAULT (getdate()) FOR [usrdata]
GO



INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'0428FF9A-1F5F-466A-9519-79580DDC083F', N'er2', N'Recusado', -1, N'N�o Pago', N'1', N'HIPAY', 1, N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'14524066-F5A8-4018-A92D-EED9B693297A', N'c6', N'Em Autoriza��o', 0, N'Aguarda Pagamento', N'1', N'HIPAY', 0, N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'181EBAF9-A3EA-4BDF-BAC6-B92AD72A28D2', N'er1', N'Recusado', -1, N'N�o Pago', N'1', N'HIPAY', 1, N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'1EB0B2AB-21EA-4744-B832-0146CFCF07F4', N'ap1', N'Reembolsado', 2, N'Reembolsado', N'1', N'HIPAY', 1, N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'4B53B3EC-01C2-4871-9B29-A92C3F2749A7', N'c9', N'Opera��o anulada por timeout', -1, N'N�o Pago', N'1', N'HIPAY', 1, N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'4EAD43A0-4562-4067-9475-44412D433096', N'c3', N'Em Autoriza��o', 0, N'Aguarda Pagamento', N'1', N'HIPAY', 0, N'ADM', CAST(N'2023-02-28T15:20:47.737' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.737' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'585A52A7-3E7E-410C-B6DC-33AC6AF18055', N'c5', N'Recusado', -1, N'N�o Pago', N'1', N'HIPAY', 1, N'ADM', CAST(N'2023-02-28T15:20:47.737' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.737' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'6608D8EE-3196-4068-85D5-46BB30A71B49', N'vp3', N'Recusado', -1, N'N�o Pago', N'1', N'HIPAY', 1, N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'8851432F-1DE7-4999-9BA4-261727EE03FA', N'c8', N'Opera��o anulada', -1, N'N�o Pago', N'1', N'HIPAY', 1, N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'911757B3-EF2D-4F80-A9F8-A29B835FF406', N'vp2', N'Recusado', -1, N'N�o Pago', N'1', N'HIPAY', 1, N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'B69D6D63-11B3-462E-9D41-643891567396', N'vp1', N'Em curso', 0, N'Aguarda Pagamento', N'1', N'HIPAY', 0, N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'CC92B4A1-6F86-47A8-8518-ABBD806D4146', N'c999', N'Erro desconhecido', 0, N'Aguarda Pagamento', N'1', N'HIPAY', 0, N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'CD5A3301-B775-4FAE-BD51-9801582044EA', N'c1', N'Conclu�do', 1, N'Pago', N'1', N'HIPAY', 1, N'ADM', CAST(N'2023-02-28T15:20:47.720' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.720' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'CFF649F4-5D0C-4D58-85B7-409134148C46', N'c2', N'Recusado', -1, N'N�o Pago', N'1', N'HIPAY', 1, N'ADM', CAST(N'2023-02-28T15:20:47.737' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.737' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'ED673945-4EE3-45CB-9DD3-6B3E68DCBB5D', N'c4', N'Recusado', -1, N'N�o Pago', N'1', N'HIPAY', 1, N'ADM', CAST(N'2023-02-28T15:20:47.737' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.737' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'FE67D6F6-078F-4FD1-8F4A-89EB913703E2', N'c7', N'Opera��o N�o encontrada', -1, N'N�o Pago', N'1', N'HIPAY', 1, N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime))
GO




																																																																			


INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'911757B3-EF2D-4F80-A9F8-A29B835FF410', N'0', N'Pendente', -1, N'N�o Pago', N'3', N'EuPago', 0, N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'B69D6D63-11B3-462E-9D41-643891567311', N'0', N'Processamento', 0, N'Aguarda Pagamento', N'3', N'EuPago', 0, N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'CC92B4A1-6F86-47A8-8518-ABBD806D4112', N'0', N'Transferida', 0, N'Aguarda Pagamento', N'3', N'EuPago', 0, N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'CD5A3301-B775-4FAE-BD51-980158204413', N'0', N'Paga', 1, N'Pago', N'3', N'EuPago', 1, N'ADM', CAST(N'2023-02-28T15:20:47.720' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.720' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'CFF649F4-5D0C-4D58-85B7-409134148C14', N'0', N'Reembolsada', 2, N'Reembolsado', N'3', N'EuPago', 1, N'ADM', CAST(N'2023-02-28T15:20:47.737' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.737' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'ED673945-4EE3-45CB-9DD3-6B3E68DCBB15', N'0', N'Cancelada', 3, N'N�o Pago', N'3', N'EuPago', 1, N'ADM', CAST(N'2023-02-28T15:20:47.737' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.737' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'FE67D6F6-078F-4FD1-8F4A-89EB91370316', N'0', N'Expirada', 3, N'N�o Pago', N'3', N'EuPago', 1, N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime))
GO

INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'FE67D6F6-078F-4FD1-8F4A-89EB91370317', N'0', N'Erro', 0, N'Aguarda Pagamento', N'3', N'EuPago', 0, N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'FE67D6F6-078F-4FD1-8F4A-89EB91370318', N'0', N'Devolvida', 3, N'N�o Pago', N'3', N'EuPago', 1, N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'FE67D6F6-078F-4FD1-8F4A-89EB91370319', N'0', N'Cativa', 0, N'Aguarda Pagamento', N'3', N'EuPago', 0, N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime))
GO