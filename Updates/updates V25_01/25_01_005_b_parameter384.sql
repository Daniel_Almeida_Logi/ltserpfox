
IF not EXISTS (SELECT 1 FROM B_Parameters WHERE stamp='ADM0000000384')
	BEGIN
		insert into B_Parameters (stamp, name, Type, textValue,
				 numValue, bool, ODate, LDate, visivel, MostraTextValue, MostraNumValue, MostraBool,
				  ListaTextValue, Unidades, obs)
		values ('ADM0000000384', 'Permite Edição clientes <200', 'admin', '',
				 0, 0, getdate(), getdate(), 0, 0, 0, 1,
				  '', '', ' Permite Edição clientes com no menor que 200')
	END