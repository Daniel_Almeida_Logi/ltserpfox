DECLARE @site NVARCHAR(MAX);
Declare @idoc numeric(3,0) = '94'
DECLARE @name  NVARCHAR(MAX)  = 'InclBb';
DECLARE @type  NVARCHAR(MAX)  = 'l';
DECLARE @value  NVARCHAR(MAX)  = 'False';
DECLARE @sequence  NVARCHAR(MAX)  = 41;


-- Declaração do cursor
DECLARE siteCursor CURSOR FOR
SELECT site FROM empresa;

-- Abre o cursor e começa a iteração
OPEN siteCursor;

FETCH NEXT FROM siteCursor INTO @site;

WHILE @@FETCH_STATUS = 0
BEGIN
		-- Verifica se já existe o registro
	IF( EXISTS(SELECT * FROM docsToSend WHERE id=@idoc AND SITE=@site))
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM docsToSendParameters WHERE idDocs = @idoc AND SITE = @site and name =@name)
		BEGIN
			-- Insere os valores
			INSERT INTO docsToSendParameters (token, idDocs, site, name, type, value, sequence)
			VALUES
				(LEFT(NEWID(), 36), @idoc, @site, @name, @type, @value, @sequence);
		END;
	END;
    -- Próximo site
    FETCH NEXT FROM siteCursor INTO @site;
END;

-- Fecha e desaloca o cursor
CLOSE siteCursor;
DEALLOCATE siteCursor;
