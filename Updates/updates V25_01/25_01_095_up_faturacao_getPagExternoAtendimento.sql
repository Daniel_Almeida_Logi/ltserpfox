/* 
	
	exec up_faturacao_getPagExternoAtendimento '1-163131008701',1

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_faturacao_getPagExternoAtendimento]') IS NOT NULL
	drop procedure dbo.up_faturacao_getPagExternoAtendimento
go

create procedure dbo.up_faturacao_getPagExternoAtendimento
	@nrAtend VARCHAR(30),
	@porPagar BIT

AS

	SELECT 
		pce.*,
		(CASE 
			WHEN pce.statePaymentIDDesc <> ''
				THEN LTRIM(RTRIM(pce.statePaymentIDDesc))
			WHEN pce.statePaymentID = 0
				THEN 'Por Consultar'
			WHEN pce.statePaymentID = 1
				THEN 'Pago'
			WHEN pce.statePaymentID = -1
				THEN 'Cancelado'
			ELSE 
				''
		END) as estado,
		req.amount as valor,
		req.email,
		req.phone,
		CAST(pce.statusDesc as varchar(254)) as statusDescrip,
		req.receiverid,
		req.receivername,
		ISNULL((SELECT TOP 1 ref FROM b_modoPag(nolock) WHERE tipoPagExt = req.typePayment AND receiverID = req.receiverID), '') as modoPag,
		req.description,
		req.timeLimitDays
	FROM
		b_pagCentral_ext(nolock) as pce
		join paymentMethodResponse(nolock) as resp ON resp.operationId = pce.operationId
		join paymentMethodRequest(nolock) as req ON req.token = resp.token and req.typeId = 1
	WHERE
		nrAtend = @nrAtend
		AND pce.statePaymentID = (CASE WHEN @porPagar = 0 THEN 1 ELSE pce.statePaymentID END)
	ORDER BY 
		ousrdata DESC

GO
Grant Execute on dbo.up_faturacao_getDocsPagExt to Public
Grant control on dbo.up_faturacao_getDocsPagExt to Public
GO