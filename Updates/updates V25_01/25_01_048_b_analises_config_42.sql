/*

select * from b_analises_config where ordem = 42 and nome = 'site'

*/

IF NOT EXISTS (
    SELECT 1 
    FROM b_analises_config 
  where ordem = 42 and nome = 'site'
)
BEGIN
	INSERT INTO [dbo].[b_analises_config]
			   ([ordem]
			   ,[nome]
			   ,[label]
			   ,[tipo]
			   ,[comando]
			   ,[fordem]
			   ,[vdefault]
			   ,[vdefaultsql]
			   ,[vdefaultfox]
			   ,[colunas]
			   ,[obrigatorio]
			   ,[firstempty]
			   ,[multiselecao]
			   ,[comandoesql]
			   ,[visivel])
		 VALUES
			   (42
			   ,'site'
			   ,'Loja'
			   ,'vc'
			   ,'uCrsTempLojas'
			   ,7
			   ,'mysite'
			   ,0
			   ,1
			   ,'Local, Designacao'
			   ,1
			   ,1
			   ,0
			   ,0
			   ,1)
			END
ELSE
BEGIN
    DELETE FROM b_analises_config 
    where ordem = 42 and nome = 'site'

    INSERT INTO [dbo].[b_analises_config]
			   ([ordem]
			   ,[nome]
			   ,[label]
			   ,[tipo]
			   ,[comando]
			   ,[fordem]
			   ,[vdefault]
			   ,[vdefaultsql]
			   ,[vdefaultfox]
			   ,[colunas]
			   ,[obrigatorio]
			   ,[firstempty]
			   ,[multiselecao]
			   ,[comandoesql]
			   ,[visivel])
		 VALUES
			   (42
			   ,'site'
			   ,'Loja'
			   ,'vc'
			   ,'uCrsTempLojas'
			   ,7
			   ,'mysite'
			   ,0
			   ,1
			   ,'Local, Designacao'
			   ,1
			   ,1
			   ,0
			   ,0
			   ,1)
		END