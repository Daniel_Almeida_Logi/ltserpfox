
delete from b_multidata where tipo = 'motiseimp' and pais = 'Portugal'
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'Artigo 16.º, n.º 6 do CIVA', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M01', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'Artigo 6.º do Decreto-Lei n.º 198/90, de 19 de junho', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M02', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'Isento artigo 13.º do CIVA', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M04', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'Isento artigo 14.º do CIVA', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M05', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'Isento artigo 15.º do CIVA', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M06', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'Isento artigo 9.º do CIVA', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M07', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'IVA - não confere direito a dedução', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M09', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'IVA – regime de isenção', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M10', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'Regime particular do tabaco', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M11', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'Regime da margem de lucro – Agências de viagens', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M12', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'Regime da margem de lucro – Bens em segunda mão', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M13', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'Regime da margem de lucro – Objetos de arte', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M14', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'Regime da margem de lucro – Objetos de coleção ou antiguidades', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M15', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'Isento artigo 14.º do RITI', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M16', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'Outras isenções', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M19', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'IVA - regime forfetário', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M20', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'IVA – não confere direito à dedução (ou expressão similar)', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M21', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'Mercadorias à consignação', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M25', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'Isenção de IVA com direito à dedução no cabaz alimentar', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M26', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'IVA - autoliquidação', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M30', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'IVA - autoliquidação', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M31', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'IVA - autoliquidação', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M32', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'IVA - autoliquidação', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M33', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'IVA - autoliquidação', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M34', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'IVA - autoliquidação', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M40', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'IVA - autoliquidação', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M41', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'IVA - autoliquidação', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M42', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'IVA - autoliquidação', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M43', 'Portugal', '', '9999999');
INSERT INTO [dbo].[b_multidata]
           ([multidatastamp], [campo], [tipo], [ousrinis], [ousrdata], [ousrhora], 
            [usrinis], [usrdata], [usrhora], [code_motive], [pais], [code], [ordem])
     VALUES
           (left(newid(), 25), 'Não sujeito ou não tributado', 'motiseimp', 'ADM', GETDATE(), GETDATE(), 
            'ADM', GETDATE(), GETDATE(), 'M99', 'Portugal', '', '9999999');