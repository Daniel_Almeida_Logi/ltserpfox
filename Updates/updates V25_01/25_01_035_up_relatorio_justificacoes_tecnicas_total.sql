/*
	relatório justificações técnicas  total

	exec up_relatorio_justificacoes_tecnicas_total '20220101','20250625','Loja 1',''
	exec up_relatorio_justificacoes_tecnicas_total '20250114','20250114','Loja 1',''
	
	(Select items from dbo.up_splitToTable(@codjustificacao, ','))
*/	

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('[dbo].[up_relatorio_justificacoes_tecnicas_total]') IS NOT NULL
    DROP PROCEDURE dbo.up_relatorio_justificacoes_tecnicas_total
GO

CREATE PROCEDURE [dbo].up_relatorio_justificacoes_tecnicas_total
    @dataIni AS datetime = '19000101',
    @dataFim AS datetime = '19000101',
    @site VARCHAR(60),
    @op INT = ''
AS
SET NOCOUNT ON

DECLARE @sql NVARCHAR(MAX)
DECLARE @params NVARCHAR(MAX)

SET @sql = '
    
select code
	, descr
	, total = count(code)
from panel_exceptions (nolock)
inner join fi2 (nolock) on fi2.justificacao_tecnica_cod = panel_exceptions.code
		LEFT JOIN fi (NOLOCK) ON fi2.fistamp = fi.fistamp
        LEFT JOIN ft (NOLOCK) ON ft.ftstamp = fi.ftstamp

where type = ''JUSTIFICAODEM'' 
		AND fivendedor = (
            CASE WHEN @op = '''' 
                 THEN fivendedor 
                 ELSE @op END
        )
        AND (u_ltstamp != '''' OR u_ltstamp2 != '''')
        AND CONVERT(DATE, fi.ousrdata) >= @dataIni
        AND CONVERT(DATE, fi.ousrdata) <= @dataFim
        AND ft.u_lote != 0
        AND justificacao_tecnica_cod != ''''
        AND justificacao_tecnica_descr != ''''
        AND ft.site = @site
group by code, descr

union all 

select code= ''TOTAL c/ JUSTIFICAÇÃO''
	, descr = ''''
	, total = count(*)
FROM
        fi (NOLOCK)
        LEFT JOIN fi2 (NOLOCK) ON fi.fistamp = fi2.fistamp
        LEFT JOIN ft (NOLOCK) ON ft.ftstamp = fi.ftstamp

where  justificacao_tecnica_cod != ''''
		AND fivendedor = (
            CASE WHEN @op = '''' 
                 THEN fivendedor 
                 ELSE @op END
        )
        AND (u_ltstamp != '''' OR u_ltstamp2 != '''')
        AND CONVERT(DATE, fi.ousrdata) >= @dataIni
        AND CONVERT(DATE, fi.ousrdata) <= @dataFim
        AND ft.u_lote != 0
        AND ft.site = @site

		union all 

select code= ''TOTAL s/ JUSTIFICAÇÃO''
	, descr = ''''
	, total = count(*)
FROM
        fi (NOLOCK)
        LEFT JOIN fi2 (NOLOCK) ON fi.fistamp = fi2.fistamp
        LEFT JOIN ft (NOLOCK) ON ft.ftstamp = fi.ftstamp

where  justificacao_tecnica_cod = ''''
		AND fivendedor = (
            CASE WHEN @op = '''' 
                 THEN fivendedor 
                 ELSE @op END
        )
        AND (u_ltstamp != '''' OR u_ltstamp2 != '''')
        AND CONVERT(DATE, fi.ousrdata) >= @dataIni
        AND CONVERT(DATE, fi.ousrdata) <= @dataFim
        AND ft.u_lote != 0
        AND ft.site = @site
'

SET @params = N'@dataIni DATETIME, @dataFim DATETIME, @site VARCHAR(60), @op INT'
print @sql
EXEC sp_executesql @sql, @params, @dataIni, @dataFim, @site, @op
GO

GRANT EXECUTE ON up_relatorio_justificacoes_tecnicas_total TO PUBLIC
GRANT CONTROL ON up_relatorio_justificacoes_tecnicas_total TO PUBLIC
GO
