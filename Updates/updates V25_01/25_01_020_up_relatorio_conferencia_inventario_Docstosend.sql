DECLARE @site NVARCHAR(MAX);
-- Declaração do cursor
DECLARE siteCursor CURSOR FOR
SELECT site FROM empresa;

-- Abre o cursor e começa a iteração
OPEN siteCursor;

FETCH NEXT FROM siteCursor INTO @site;

WHILE @@FETCH_STATUS = 0
BEGIN
		-- Verifica se já existe o registro
	IF( EXISTS(SELECT * FROM docsToSend WHERE id=24 AND SITE=@site))
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM docsToSendParameters WHERE idDocs = 24 AND SITE = @site and name ='psicotropicos')
		BEGIN
			-- Insere os valores
			INSERT INTO docsToSendParameters (token, idDocs, site, name, type, value, sequence)
			VALUES
				(LEFT(NEWID(), 36), 24, @site, 'psicotropicos', 'l', 'false', 18);
		END;


		IF NOT EXISTS (SELECT 1 FROM docsToSendParameters WHERE idDocs = 24 AND SITE = @site and name ='benzodiazepinas')
		BEGIN
			-- Insere os valores
			INSERT INTO docsToSendParameters (token, idDocs, site, name, type, value, sequence)
			VALUES
					 (LEFT(NEWID(), 36), 24, @site, 'benzodiazepinas', 'l', 'false', 19);
		END;
	END;
    -- Próximo site
    FETCH NEXT FROM siteCursor INTO @site;
END;

-- Fecha e desaloca o cursor
CLOSE siteCursor;
DEALLOCATE siteCursor;
