/* get  PaymentMethods Request 
	

	exec up_get_paymentMethodsRequest ''

*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_paymentMethodsRequest]') IS NOT NULL
	drop procedure dbo.up_get_paymentMethodsRequest
go

create procedure [dbo].up_get_paymentMethodsRequest
	@token				[varchar](36) 
AS
BEGIN	


  
	select  token,username,[password],typeID,typeIDdesc,typePayment,typePaymentDesc,receiverid,receivername,entityType,amount
				,[description],paymentMethodRequest.email,phone,clientIdNumber,externalReference,[name],categoryId,callBackURL,origin,additionalInfo,
				[address],postCode,city,nic,idUserBackoffice,timeLimitDays,sendEmail,operationId,startDate,endDate,typeGetInfo,
				paymentMethodRequest.[site],test , id_lt, nomecomp , ncont,officekey,channel,fractionalPayment,currency,countryCode,sendSMS,errorUrl,successUrl,cancelUrl,
				reason,trxId,iban,bic,urlPost
	FROM paymentMethodRequest (nolock)
	INNER JOIN empresa (nolock) ON paymentMethodRequest.site = empresa.site
	WHERE token=@token
END

GO
Grant Execute on dbo.up_get_paymentMethodsRequest to Public
Grant control on dbo.up_get_paymentMethodsRequest to Public
GO