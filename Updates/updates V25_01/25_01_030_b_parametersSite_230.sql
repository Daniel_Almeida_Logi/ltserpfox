DECLARE @site VARCHAR(20)
declare emp_cursor cursor for
select site as siteemp from empresa
open emp_cursor
fetch next from emp_cursor into @site
while @@FETCH_STATUS = 0
begin
	IF not EXISTS (SELECT 1 FROM B_Parameters_site(nolock) WHERE stamp='ADM0000000230' and site=@site)
	BEGIN
		insert into B_Parameters_site (stamp, name, Type, textValue,
				 numValue, bool, ODate, LDate, visivel, MostraTextValue, MostraNumValue, MostraBool,
 ListaTextValue, Unidades, Site, obs)
		values ('ADM0000000230', 'Utiliza Comparticipação SPMS em DEM', 'DEM', '',
				 0, 0, getdate(), getdate(), 0, 0, 0, 1,
 'Bool = 0 - Não usa; Bool = 1 - usa', '', @site, 'Utiliza Comparticipação SPMS em receitas eletronicas')
	END
	fetch next from emp_cursor into @site
end
close emp_cursor
deallocate emp_cursor