-- Pesquisa de Produtos que poderão ser substituidos pelo lab de patrocinio
-- exec up_touch_pesquisaProdutosMLab '5330154', '50010220', 1, 'TOLIFE', 1
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_pesquisaProdutosMLab]') IS NOT NULL
	drop procedure dbo.up_touch_pesquisaProdutosMLab
go

create procedure dbo.up_touch_pesquisaProdutosMLab
	@ref varchar(18),
	@cnpem varchar (8),
	@armazem numeric(5,0),
	@lab varchar(100),
	@site_nr tinyint

/* WITH ENCRYPTION */
AS

	SET NOCOUNT ON

	Select
		 'sel'			= convert(bit,0)
		,'ref'			= fprod.cnp
		,'design'		= ltrim(rtrim(isnull(st.design,fprod.design)))
		,'stock'		= isnull(sa.stock,0)
		,'epv1'			= isnull(st.epv1,'0')
		,'validade'		= isnull(convert(varchar,st.validade,102),'1900.01.01')
		,'faminome'		= isnull(st.faminome,convert(varchar,fprod.u_familia))
		,'comp'			= isnull( (case when (left(cptgrp.descrição,3)!='Não' and cptgrp.u_diploma1='' and cptgrp.u_diploma2='' and cptgrp.u_diploma3='') AND cptgrp.grupo!='ST'
										then 1
										else 0
								 end)
							  ,0)
		,'generico'		= IsNull(fprod.generico,0)
		,'GRPHMG'		= case when fprod.grphmgcode='GH0000' then fprod.grphmgcode
							else fprod.grphmgcode + ' - ' + fprod.grphmgdescr
						 end
		,'psico'		= isnull(fprod.psico,0)
		,'benzo'		= isnull(fprod.benzo,0)
		,'ststamp'		= isnull(st.ststamp,'')
		,'ptoenc'		= isnull(st.ptoenc,0)
		,'stmax'		= isnull(st.stmax,0)
		,'marg1'		= isnull(st.marg1,0)
		,'local'		= isnull(st.local,'')
		,'u_local'		= isnull(st.u_local,'')
		,'u_local2'		= isnull(st.u_local2,'')
		,'qttCli'		= isnull(st.qttcli, 0)
		,oref			= @ref /* mapeamento com cursor */
		,'refInt'		= ISNULL(st.ref, '')
		,'descVal'		= isnull(st.mfornec2,0)
		,'descPerc'		= isnull(st.mfornec,0)
		,'dosuni'		=  convert(varchar(254),dosuni)
		,'fformasabrev' = convert(varchar(254),fformasabrev)
		, num_unidades
		, fprod.grupo
		, valorUtenteComSns = isnull(st.epv1,'0')
		, fprod.pref

	from
		fprod (nolock)
		left join st  (nolock) on st.ref=fprod.cnp and st.site_nr = @site_nr
		inner join cptgrp (nolock) on cptgrp.grupo=fprod.grupo
		left join sa (nolock) on sa.ref=st.ref AND sa.armazem=@armazem	
	where 
		fprod.cnp != @ref
		and fprod.cnpem = @cnpem
		and (fprod.titaimdescr = @lab or st.u_lab = @lab)
		and ISNULL(st.inactivo, 0) = 0
	order by 
		st.stock desc, st.design
		
	OPTION (OPTIMIZE FOR (@ref unknown, @cnpem unknown,@armazem unknown,@lab unknown))	

GO
Grant Execute On dbo.up_touch_pesquisaProdutosMLab to Public
Grant Control On dbo.up_touch_pesquisaProdutosMLab to Public
Go