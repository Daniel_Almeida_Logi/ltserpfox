/*
select * From b_analises where report = 'relatorio_conferencia_inventarioData' 
select * from b_analises_config where ordem = '71' order by fordem 
*/


-- Novo parâmetro Psicotropicos no relatorio inventario simplificado
IF NOT EXISTS (
    SELECT 1 
    FROM b_analises_config 
    WHERE ordem = 71 AND nome = 'psicotropicos'
)
BEGIN
    INSERT INTO b_analises_config (ordem, nome, label, tipo, fordem, visivel, obrigatorio, firstempty)
    VALUES (
        71,
        'psicotropicos',
        'Psicotrópicos',
        'l',
        '10',
        1,
        0,
		0
    );
END
ELSE
BEGIN
    DELETE FROM b_analises_config 
    WHERE ordem = 71 AND nome = 'psicotropicos';

    INSERT INTO b_analises_config (ordem, nome, label, tipo, fordem, visivel, obrigatorio, firstempty)
    VALUES (
        71,
        'psicotropicos',
        'Psicotrópicos',
        'l',
        '10',
        1,
        0,
		0
    );
END;


-- Novo parâmetro Benzodiazepinas no relatorio inventario simplificado
IF NOT EXISTS (
    SELECT 1 
    FROM b_analises_config 
    WHERE ordem = 71 AND nome = 'benzodiazepinas'
)
BEGIN
    INSERT INTO b_analises_config (ordem, nome, label, tipo, fordem, visivel, obrigatorio, firstempty)
    VALUES (
        71,
        'benzodiazepinas',
        'Benzodiazepinas',
        'l',
        '11',
        1,
        0,
		0
    );
END
ELSE
BEGIN
    DELETE FROM b_analises_config 
    WHERE ordem = 71 AND nome = 'benzodiazepinas';

    INSERT INTO b_analises_config (ordem, nome, label, tipo, fordem, visivel, obrigatorio, firstempty)
    VALUES (
        71,
        'benzodiazepinas',
        'Benzodiazepinas',
        'l',
        '11',
        1,
        0,
		0
    );
END;
