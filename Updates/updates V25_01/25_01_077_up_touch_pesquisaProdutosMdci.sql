-- Pesquisa de Produtos por mesmo DCI nas Vendas
-- exec up_touch_pesquisaProdutosMdci '8168617', '', 1,1
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_pesquisaProdutosMdci]') IS NOT NULL
	drop procedure dbo.up_touch_pesquisaProdutosMdci
go

create procedure dbo.up_touch_pesquisaProdutosMdci
	@ref varchar(18),
	@dci varchar (120),
	@armazem numeric(5,0),
	@site_nr tinyint

/* WITH ENCRYPTION */
AS

	SET NOCOUNT ON

	declare @patroLab varchar(200)
	if (select top 1 bool from B_Parameters where stamp='ADM0000000204') = 1
		begin
			set @patroLab = (select top 1 textValue from B_Parameters where stamp='ADM0000000204')
		end
	else
		begin
			set @patroLab = char(39)+'labquenaoexiste'+CHAR(39)
		end

	/* 5 Mais Baratos*/
	declare @cnpem varchar(8)
	declare @grphmgcode varchar(6)
	declare @dcipt_id varchar(10)

	select
		@cnpem			= fprod.cnpem
		,@grphmgcode	= fprod.grphmgcode
		,@dcipt_id		= fprod.dcipt_id
	from
		fprod (nolock)
	where
		cnp = @ref


	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosMaisBaratos'))
	BEGIN
		DROP TABLE #dadosMaisBaratos
	END

	select
		 ref		= fprod.cnp
		 ,epreco	= case when pvpcalc=0 then pvporig else pvpcalc end
	into 
		#dadosMaisBaratos
	from
		fprod (nolock)
		left join	st (nolock)	on st.ref = fprod.cnp and st.site_nr = @site_nr
	where
		ranking=1
		and grphmgcode = @grphmgcode
		and grphmgcode != 'GH0000' and grphmgcode != ''
		and fprod.pvporig <= fprod.pvpmaxre

	/* Informação do produto selecionado */
	Select
		ordem = 1 
		,'sel'			= convert(bit,0)
		,'ref'			= fprod.cnp
		,'design'		= ltrim(rtrim(isnull(st.design,fprod.design)))
		,'stock'		= isnull(sa.stock,0)
		,'epv1'			= isnull(st.epv1,'0')
		,'validade'		= isnull(convert(varchar,st.validade,102),'1900.01.01')
		,'faminome'		= isnull(st.faminome,convert(varchar,fprod.u_familia))
		,'comp'			= isnull( (case when (left(cptgrp.descrição,3)!='Não' and cptgrp.u_diploma1='' and cptgrp.u_diploma2='' and cptgrp.u_diploma3='') AND cptgrp.grupo!='ST'
										then 1
										else 0
								 end)
							  ,0)
		,'generico'		= IsNull(fprod.generico,0)
		,'GRPHMG'		= case when fprod.grphmgcode='GH0000' then fprod.grphmgcode
							else fprod.grphmgcode + ' - ' + fprod.grphmgdescr
						 end
		,'psico'		= isnull(fprod.psico,0)
		,'benzo'		= isnull(fprod.benzo,0)
		,'ststamp'		= isnull(st.ststamp,'')
		,'ptoenc'		= isnull(st.ptoenc,0)
		,'stmax'		= isnull(st.stmax,0)
		,'marg1'		= isnull(st.marg1,0)
		,'local'		= isnull(st.local,'')
		,'u_local'		= isnull(st.u_local,'')
		,'u_local2'		= isnull(st.u_local2,'')
		,'qttCli'		= isnull(st.qttcli, 0)
		,oref			= @ref /* mapeamento com cursor */
		,fprod.cnpem
		,bb = isnull(st.marg4,0)
		,t5 = case when (select COUNT(ref) from #dadosMaisBaratos where #dadosMaisBaratos.ref = fprod.cnp) > 0 then convert(bit,1) else convert(bit,0) end
		,MCNPEM  = convert(bit,1)
		,labOrdem = 0
		,textosel = ''		
		,'refInt' = ISNULL(st.ref, '')	
		,'descVal'		= isnull(st.mfornec2,0)
		,'descPerc'		= isnull(st.mfornec,0)
		,'dosuni'		= convert(varchar(254),dosuni)
		,'fformasabrev' = convert(varchar(254),fformasabrev)
		,num_unidades
		,fprod.grupo
		,valorUtenteComSns = isnull(st.epv1,'0')
		,fprod.pref

	from
		fprod (nolock)
		left join st  (nolock) on st.ref=fprod.cnp and st.site_nr = @site_nr
		inner join cptgrp (nolock) on cptgrp.grupo=fprod.grupo
		left join sa (nolock) on sa.ref=st.ref AND sa.armazem=@armazem	
	where 
		st.ref = @ref
	/* Fim de Informação do produto selecionado */

	Union all	

	/* Informação dos  restantes produtos */
	Select
		ordem = 2 
		,'sel'			= convert(bit,0)
		,'ref'			= fprod.cnp
		,'design'		= ltrim(rtrim(isnull(st.design,fprod.design)))
		,'stock'		= isnull(sa.stock,0)
		,'epv1'			= isnull(st.epv1,'0')
		,'validade'		= isnull(convert(varchar,st.validade,102),'1900.01.01')
		,'faminome'		= isnull(st.faminome,convert(varchar,fprod.u_familia))
		,'comp'			= isnull( (case when (left(cptgrp.descrição,3)!='Não' and cptgrp.u_diploma1='' and cptgrp.u_diploma2='' and cptgrp.u_diploma3='') AND cptgrp.grupo!='ST'
										then 1
										else 0
								 end)
							  ,0)
		,'generico'		= IsNull(fprod.generico,0)
		,'GRPHMG'		= case when fprod.grphmgcode='GH0000' then fprod.grphmgcode
							else fprod.grphmgcode + ' - ' + fprod.grphmgdescr
						 end
		,'psico'		= isnull(fprod.psico,0)
		,'benzo'		= isnull(fprod.benzo,0)
		,'ststamp'		= isnull(st.ststamp,'')
		,'ptoenc'		= isnull(st.ptoenc,0)
		,'stmax'		= isnull(st.stmax,0)
		,'marg1'		= isnull(st.marg1,0)
		,'local'		= isnull(st.local,'')
		,'u_local'		= isnull(st.u_local,'')
		,'u_local2'		= isnull(st.u_local2,'')
		,'qttCli'		= isnull(st.qttcli, 0)
		,oref			= @ref /* mapeamento com cursor */
		,fprod.cnpem
		,bb = isnull(st.marg4,0)
		,t5 = case when (select COUNT(ref) from #dadosMaisBaratos where #dadosMaisBaratos.ref = fprod.cnp) > 0 then convert(bit,1) else convert(bit,0) end
		,MCNPEM  = case when fprod.cnpem = @cnpem then convert(bit,1) else convert(bit,0) end
		,labOrdem = case when st.u_lab = @patroLab then 1 else 0 end 
		,textosel = ''	
		,'refInt' = ISNULL(st.ref, '')	
		,'descVal'		= isnull(st.mfornec2,0)
		,'descPerc'		= isnull(st.mfornec,0)
		,'dosuni'		=  convert(varchar(254),dosuni)
		,'fformasabrev' = convert(varchar(254),fformasabrev)
		,num_unidades
		,fprod.grupo
		,valorUtenteComSns = isnull(st.epv1,'0')
		,fprod.pref
	from
		fprod (nolock)
		left join st  (nolock) on st.ref=fprod.cnp and st.site_nr = @site_nr
		inner join cptgrp (nolock) on cptgrp.grupo=fprod.grupo
		left join sa (nolock) on sa.ref=st.ref AND sa.armazem=@armazem	
	where 
		st.ref != @ref
		AND fprod.dcipt_id = @dcipt_id
		and fprod.dcipt_id != ''
		and ISNULL(st.inactivo, 0) = 0

	/* Fim  Informação dos  restantes produtos */

	order by 
		ordem, labOrdem desc , isnull(marg4,0), stock desc, design


	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosMaisBaratos'))
	BEGIN
		DROP TABLE #dadosMaisBaratos
	END


GO
Grant Execute On dbo.up_touch_pesquisaProdutosMdci to Public
Grant Control On dbo.up_touch_pesquisaProdutosMdci to Public
Go