
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[b_pagCentral_ext]') AND name = N'IX_b_pagCentral_ext_nrAtend')
CREATE NONCLUSTERED INDEX [IX_b_pagCentral_ext_nrAtend] ON [dbo].b_pagCentral_ext
(
	nrAtend ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[b_pagCentral_ext]') AND name = N'IX_b_pagCentral_ext_operationId')
CREATE NONCLUSTERED INDEX [IX_b_pagCentral_ext_operationId] ON [dbo].b_pagCentral_ext
(
	operationId ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO




/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].paymentMethodResponse') AND name = N'IX_paymentMethodResponse_operationId')
CREATE NONCLUSTERED INDEX [IX_paymentMethodResponse_operationId] ON [dbo].paymentMethodResponse
(
	operationId ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

