/*

	Create Table Using Columns

*/
DECLARE @tableName NVARCHAR(50) = 'fi_trans_info_faltas'


DECLARE @col1 NVARCHAR(100) = 'fi_trans_info_faltasStamp varchar(25) NOT NULL PRIMARY KEY'
DECLARE @col2 NVARCHAR(100) = 'fistamp VARCHAR(36) NOT NULL CONSTRAINT DF_'+@tableName+'_fistamp DEFAULT ('''')'
DECLARE @col3 NVARCHAR(100) = 'lote VARCHAR(100) NOT NULL CONSTRAINT DF_'+@tableName+'_lote DEFAULT ('''')'
DECLARE @col4 NVARCHAR(100) = 'dataValidade DATETIME NOT NULL CONSTRAINT DF_'+@tableName+'_dataValidade DEFAULT GETDATE()'
DECLARE @col5 NVARCHAR(100) = 'packsn varchar(100) NOT NULL CONSTRAINT DF_'+@tableName+'_packsn DEFAULT ('''')'
DECLARE @col6 NVARCHAR(100) = 'productCode  VARCHAR(100) NOT NULL CONSTRAINT DF_'+@tableName+'_productCode DEFAULT ('''')'
DECLARE @col7 NVARCHAR(100) = 'operadorno  NUMERIC(5,0) NOT NULL CONSTRAINT DF_'+@tableName+'_operadorno DEFAULT ('''')'
DECLARE @col8 NVARCHAR(100) = 'operador  VARCHAR(100) NOT NULL CONSTRAINT DF_'+@tableName+'_operador DEFAULT ('''')'
DECLARE @col9 NVARCHAR(100) = 'terminal  VARCHAR(100) NOT NULL CONSTRAINT DF_'+@tableName+'_terminal DEFAULT ('''')'
DECLARE @col10 NVARCHAR(100) = 'terminalno  NUMERIC(5,0) NOT NULL CONSTRAINT DF_'+@tableName+'_terminalno DEFAULT ('''')'
DECLARE @col11 NVARCHAR(100) = 'atendimento  VARCHAR(100) NOT NULL CONSTRAINT DF_'+@tableName+'_atendimento DEFAULT ('''')'
DECLARE @col12 NVARCHAR(100) = 'site  VARCHAR(100) NOT NULL CONSTRAINT DF_'+@tableName+'_site DEFAULT ('''')'
DECLARE @col13 NVARCHAR(100) = 'ousrdata DATETIME NOT NULL CONSTRAINT DF_'+@tableName+'_ousrdata DEFAULT GETDATE()'

-- Concatenate columns into a single definition
DECLARE @columns NVARCHAR(MAX) = @col1 + ',' + @col2 + ',' + @col3 + ',' + @col4 + ',' + @col5 + ',' + @col6 + ',' + @col7+ ',' + @col8+ ',' + @col9+ ',' + @col10+ ',' + @col11+ ',' + @col12+ ',' + @col13







IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = @tableName)
BEGIN
    EXEC('
        CREATE TABLE dbo.' + @tableName + ' (
            ' + @columns + '
        )
    ')
    PRINT 'Table ' + @tableName + ' created successfully.'
END
ELSE
BEGIN
    PRINT 'Table ' + @tableName + ' already exists.'
END
GO