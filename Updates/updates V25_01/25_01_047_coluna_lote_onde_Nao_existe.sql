
--delete from [wklcols] where  [wkfield] = 'fn.lote'
--and [wktitle]='Lote'


--delete from [wklcols] where  [wkfield] = 'bi.lote'
--and [wktitle]='Lote'
  
INSERT INTO [dbo].[wklcols]
           ([wklcolsstamp]
           ,[wkfield]
           ,[wktitle]
           ,[wkstamp]
           ,[wkdocsstamp]
           ,[ousrinis]
           ,[ousrdata]
           ,[ousrhora]
           ,[usrinis]
           ,[usrdata]
           ,[usrhora]
           ,[wkreadonly]
           ,[u_largura]
           ,[u_mascara]
           ,[u_ordem])
SELECT  LEFT (NEWID(),25)
           ,'fn.lote'
           ,'Lote'
           ,wk.wkstamp
           ,''
           ,'ADM'
           ,GETDATE()
           ,GETDATE()
           ,'ADM'
           ,GETDATE()
           ,GETDATE()
           ,0
           ,70
           ,''
           ,(select isnull(MAX(u_ordem) + 1,99) from wklcols(nolock) x where x.wkstamp = wk.wkstamp)  
FROM ts (nolock)
INNER JOIN wk (nolock) ON wk.wkdoc = ts.ndos
WHERE not EXISTS (
    SELECT 1
    FROM wklcols (nolock)
    WHERE wklcols.wkstamp = wk.wkstamp 
      AND wklcols.wkfield = 'fi.lote'
)
and ts.cmstocks <>0





INSERT INTO [dbo].[wklcols]
           ([wklcolsstamp]
           ,[wkfield]
           ,[wktitle]
           ,[wkstamp]
           ,[wkdocsstamp]
           ,[ousrinis]
           ,[ousrdata]
           ,[ousrhora]
           ,[usrinis]
           ,[usrdata]
           ,[usrhora]
           ,[wkreadonly]
           ,[u_largura]
           ,[u_mascara]
           ,[u_ordem])
SELECT  LEFT (NEWID(),25)
           ,'bi.lote'
           ,'Lote'
           ,wk.wkstamp
           ,''
           ,'ADM'
           ,GETDATE()
           ,GETDATE()
           ,'ADM'
           ,GETDATE()
           ,GETDATE()
           ,0
           ,70
           ,''
           ,(select isnull(MAX(u_ordem) + 1,99) from wklcols(nolock) x where x.wkstamp = wk.wkstamp)  
FROM cm1 (nolock)
INNER JOIN wk (nolock) ON wk.wkdoc = cm1.ndos
WHERE not EXISTS (
    SELECT 1
    FROM wklcols (nolock)
    WHERE wklcols.wkstamp = wk.wkstamp 
      AND wklcols.wkfield = 'fi.lote'
)
and cm1.fosl <>0



update wklcols 
set wkreadonly = 0
where wklcols.wkfield = 'fi.lote' or wklcols.wkfield ='bi.lote'