/*
	cria documento de bonus de fornecedor a partir da resposta do serviço de bonus Orders
	
	exec up_esb_orders__DocumentoBonunsFornecedor 
	
	 up_esb_orders__DocumentoBonunsFornecedor 'ADM8B0EF6CC-1826-43F8-857','2c8ef33d-638a-4881-b82b-0deb54438a',1,416
	  exec up_esb_orders__DocumentoBonunsFornecedor 'ADM2EB54BF4-327C-441E-B30','133e26e7-a361-4bb9-af8a-9481094dfd',35,1,873

	exec up_esb_orders__DocumentoBonunsFornecedor '7B72D2FB-ECBA-439D-BB15-B970C84891D9','15524041-d1c8-424f-b603-7bff066a9c',35,1,3039,0


*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_esb_orders__DocumentoBonunsFornecedor]') IS NOT NULL
	drop procedure dbo.up_esb_orders__DocumentoBonunsFornecedor
go

CREATE PROCEDURE dbo.up_esb_orders__DocumentoBonunsFornecedor
	@token VARCHAR(36),
	@stampCampaigns VARCHAR(36),
	@ndos NUMERIC(3,0),
	@site_nr int,
	@noforne numeric(10,0),
	@noforneEstab numeric(3,0)=0
AS

SET NOCOUNT ON

	DECLARE @ano INT 
	DECLARE @site VARCHAR(20)
	DECLARE @nomeForne VARCHAR(80)
	DECLARE @date DATETIME
	DECLARE @no_ext VARCHAR(50)
	
	SELECT @no_ext= no_ext  FROM fl WHERE no=@noForne AND estab = @noforneEstab

	SET @ano =(SELECT YEAR(GETDATE()))
	SET @site= (SELECT site FROM EMPRESA WHERE no=@site_nr)


		DECLARE @CampaignCodeName VARCHAR(254)
		DECLARE @CampaignCode VARCHAR(50)
		DECLARE @dateBegin  datetime
		DECLARE @dateend  datetime

	UPDATE bonus set ativo=0 where endDate  < convert(date ,GETDATE()); 

	SELECT @CampaignCode=campaignCode,@CampaignCodeName=campaignName, @dateBegin = beginDate , @dateend= endDate  FROM ext_esb_orders_campaigns (NOLOCK)  WHERE TOKEN= @token AND  stamp=@stampCampaigns -- codigo que foi recebido agora 

	if(@no_ext='9')
	begin
		UPDATE bonus set ativo=0 where ousrdata  <= convert(date ,GETDATE()) and site=@site and campaignCode = @CampaignCode

	end



	if(not exists(select * from bonus where bonus.campaignCode =@CampaignCode and bonus.campaignName=@CampaignCodeName and bonus.no_fl = @noforne and bonus.estab_fl =@noforneEstab and site=@site and beginDate =@dateBegin and endDate=@dateend ))
	BEGIN

		DECLARE @stamp VARCHAR(36) = left(NEWID(),36)

		insert into bonus (stamp,responseDate,campaignName,campaignCode,beginDate,endDate,addendum,message,
						   no_fl,estab_fl,ativo,site,ousrdata,ousrinis,usrdata,usrinis)
		select @stamp,responseDate,campaignName,campaignCode,beginDate,endDate,addendum,message
			,@noforne,@noforneEstab,1,@site,GETDATE(),'ADM',GETDATE(),'ADM'
			from ext_esb_orders_campaigns (nolock)
			WHERE TOKEN= @token AND  stamp=@stampCampaigns

	
		insert into	bonus_d (stamp,bonus_stamp,ref,position,qttmin,qttmax,multipleAcquisition,packagingBonus,productOffered,descrOffer,discount,beginDate
						,endDate,PVU,ousrdata,ousrinis,usrdata,usrinis)

		select left(NEWID(),36),@stamp,ext_esb_orders_campaigns_d.ref,position,qttmin,qttmax,multipleAcquisition,packagingBonus,productOffered,descrOffer,discount,beginDate
						,endDate,PVU,GETDATE(),'ADM',GETDATE(),'ADM'
		from ext_esb_orders_campaigns_d (nolock) 
		WHERE TOKEN=@token AND  campaignsStamp=@stampCampaigns

	END 
	
GO
Grant Execute on dbo.up_esb_orders__DocumentoBonunsFornecedor to Public
Grant Control on dbo.up_esb_orders__DocumentoBonunsFornecedor to Public
GO
