use ltstag 
-- exec up_email_emailTextoAutomatico 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_email_emailTextoAutomatico]') IS NOT NULL
	drop procedure dbo.up_email_emailTextoAutomatico
go

create procedure dbo.up_email_emailTextoAutomatico

/* WITH ENCRYPTION */ 

AS 
BEGIN
	Select	[desc]		= 'Documento',
			[cod]		= '�doc�',
			[cursor]	= 'ft',
			[campo]		= 'nmdoc'

	union all

	Select	[desc]		= 'N�mero',
			[cod]		= '�DocNo�',
			[cursor]	= 'ft',
			[campo]		= 'fno'

	union all

	Select	[desc]		= 'Cliente',
			[cod]		= '�nomeCli�',
			[cursor]	= 'ft',
			[campo]		= 'nome'
END

GO
Grant Execute on dbo.up_email_emailTextoAutomatico to Public
GO
Grant Control on dbo.up_email_emailTextoAutomatico to Public
GO



