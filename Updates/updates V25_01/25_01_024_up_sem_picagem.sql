/*

Linhas dispensadas sem picagem de qrcode

select * from fi where ftstamp = 'fBAE3720E-BEB3-4CF9-8AD'

select * from fi_trans_info_faltas

exec up_sem_picagem '22134887667001'
*/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('[dbo].[up_sem_picagem ]') IS NOT NULL
	drop procedure dbo.up_sem_picagem 
GO

CREATE PROCEDURE [dbo].up_sem_picagem 
	@nrAtendimento	varchar(60),
	@site			varchar(20)

/* with encryption */
AS
SET NOCOUNT ON

	DECLARE @ftstamp VARCHAR(50);
	DECLARE @ref VARCHAR(50);
	DECLARE @dispositivoSeguranca BIT;

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempFi'))
		DROP TABLE #tempFi

	-- Obter os registros de 'fi' relacionados ao número de atendimento
	SELECT 
		fi.ref, 
		fi.fistamp, 
		fi.lote, 
		isnull(fi2.dataValidade,'') as dataValidade, 
		fi2.productCodeEmbal, 
		fi.fivendedor, 
		fi.fivendnm, 
		ft.site,
		ft.pno,
		ft.pnome
	INTO #tempFi
	FROM fi(nolock)
	LEFT JOIN fi2 (nolock) ON fi.fistamp	= fi2.fistamp
	INNER JOIN fprod (nolock) ON fprod.cnp	= fi.ref
	INNER JOIN ft (nolock) ON ft.ftstamp	= fi.ftstamp
	inner join td (nolock) on ft.ndoc		= td.ndoc
	WHERE 
		ft.u_nratend = @nrAtendimento -- Filtrar pelo número de atendimento
		AND fprod.dispositivo_seguranca = 1 -- Somente registros com dispositivo de segurança ativo
		and ft.site = @site
		AND (td.mvoAlteraEstado =  1 or td.mvoVerificaEstado =  1)
		AND td.mvoOpcional = 0 

	-- Verificar se a tabela temporária possui registros
	IF EXISTS(SELECT 1 FROM #tempFi)
	BEGIN
		-- Processar cada registro na tabela temporária
		INSERT INTO fi_trans_info_faltas(fi_trans_info_faltasStamp, fistamp, lote, dataValidade, packsn, productCode, operadorno, operador, site, terminal, terminalno, atendimento, ousrdata)
		SELECT 
			LEFT(NEWID(), 25), 
			fitemp.fistamp, 
			fitemp.lote, 
			fitemp.dataValidade, 
			'', 
			'', 
			fitemp.fivendedor, 
			fitemp.fivendnm, 
			fitemp.site,
			pnome,
			pno,
			@nrAtendimento,
			GETDATE()
		FROM #tempFi (nolock) fitemp
		LEFT JOIN fi_trans_info (nolock) fiInfo ON fitemp.fistamp = fiInfo.recStamp
		WHERE 
			fiInfo.recStamp IS NULL -- Não foi picado
			AND NOT EXISTS (
				SELECT 1 
				FROM fi_trans_info_faltas (nolock) faltas
				WHERE faltas.fistamp = fitemp.fistamp
					and fitemp.site = @site 
			);

		
	END

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempFi'))
		DROP TABLE #tempFi
GO
Grant Execute On up_sem_picagem  to Public
Grant Control On up_sem_picagem  to Public
GO