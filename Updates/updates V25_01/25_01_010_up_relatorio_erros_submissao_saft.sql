/*
	exec up_relatorio_erros_submissao_saft '20201118','20251118','Loja 1',''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_relatorio_erros_submissao_saft]') IS NOT NULL
    DROP procedure dbo.up_relatorio_erros_submissao_saft
GO

CREATE procedure dbo.up_relatorio_erros_submissao_saft
	
	@dataIni	DATETIME,
	@dataFim	DATETIME,
	@site		VARCHAR(254),
	@valido		VARCHAR(20)

	
/* WITH ENCRYPTION */
AS
	If OBJECT_ID('tempdb.dbo.#idLtLojas') IS NOT NULL
    	drop table #idLtLojas;

	DECLARE @sql		VARCHAR(4000) = ''
--o valor da loja é guardado pelo xml dai não se poder usar o site mas sim o id_lt
	SELECT DISTINCT
		site
	INTO 
		#idLtLojas
	FROM 
		empresa(NOLOCK)
	WHERE
		site IN (SELECT items FROM dbo.up_splitToTable(@site,','))

	SET 
		@sql = @sql + N'

		SELECT
    ano,
    mes,
    design,
    code,
    loja,
    type,
    ousrdata,
    ousrinis 
FROM
    saftResposta(NOLOCK)
    INNER JOIN #idLtLojas 
        ON LTRIM(RTRIM(#idLtLojas.site)) = LTRIM(RTRIM(saftResposta.loja))
WHERE
    CONVERT(varchar, ousrdata, 23) BETWEEN ''' + CONVERT(varchar, @dataIni, 23) + ''' 
        AND ''' + CONVERT(varchar, @dataFim, 23) + ''' '

		IF @valido = 'Validar'
			SET @sql = @sql + N' AND type = ''validar'''
		IF @valido = 'Enviar'
			SET @sql = @sql + N' AND type = ''enviar'''

		PRINT @sql
		EXEC(@sql)

	If OBJECT_ID('tempdb.dbo.#idLtLojas') IS NOT NULL
    	drop table #idLtLojas;
GO
GRANT EXECUTE on dbo.up_relatorio_erros_submissao_saft TO PUBLIC
GRANT Control on dbo.up_relatorio_erros_submissao_saft TO PUBLIC
GO