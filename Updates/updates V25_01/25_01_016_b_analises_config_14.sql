/*
select * From b_analises where report = 'relatorio_conferencia_inventario'

select * from b_analises_config where ordem = '14' order by fordem
*/

-- Novo parâmetro Psicotropicos no relatorio inventario
IF NOT EXISTS (
    SELECT 1 
    FROM b_analises_config 
    WHERE ordem = 14 AND nome = 'psicotropicos'
)
BEGIN
    INSERT INTO b_analises_config (ordem, nome, label, tipo, fordem, visivel, obrigatorio, firstempty)
    VALUES (
        14,
        'psicotropicos',
        'Psicotrópicos',
        'l',
        '21',
        1,
        0,
		0
    );
END
ELSE
BEGIN
    DELETE FROM b_analises_config 
    WHERE ordem = 14 AND nome = 'psicotropicos';

    INSERT INTO b_analises_config (ordem, nome, label, tipo, fordem, visivel, obrigatorio, firstempty)
    VALUES (
        14,
        'psicotropicos',
        'Psicotrópicos',
        'l',
        '21',
        1,
        0,
		0
    );
END;


-- Novo parâmetro Benzodiazepinas no relatorio inventario
IF NOT EXISTS (
    SELECT 1 
    FROM b_analises_config 
    WHERE ordem = 14 AND nome = 'benzodiazepinas'
)
BEGIN
    INSERT INTO b_analises_config (ordem, nome, label, tipo, fordem, visivel, obrigatorio, firstempty)
    VALUES (
        14,
        'benzodiazepinas',
        'Benzodiazepinas',
        'l',
        '22',
        1,
        0,
		0
    );
END
ELSE
BEGIN
    DELETE FROM b_analises_config 
    WHERE ordem = 14 AND nome = 'benzodiazepinas';

    INSERT INTO b_analises_config (ordem, nome, label, tipo, fordem, visivel, obrigatorio, firstempty)
    VALUES (
        14,
        'benzodiazepinas',
        'Benzodiazepinas',
        'l',
        '22',
        1,
        0,
		0
    );
END;