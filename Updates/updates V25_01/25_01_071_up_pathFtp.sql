SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_stocks_InventarioStockArmazem]') IS NOT NULL
    DROP PROCEDURE up_pathFtp
GO

Create procedure up_pathFtp
@stamp varchar(50),
@ano  varchar(10) = 0,
@fno  varchar (5) = 0,
@ndoc varchar (5) = 0
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON
Begin 
	Declare @where varchar (50)
	Set @stamp = case when @stamp <>''
				then
					@stamp
				else
					(select ftstamp from ft (nolock) where ndoc = @ndoc and fno = @fno and ftano = @ano)
				end
	Select rtrim(ltrim(b_utentes.ncont)) + '/'  
			+ ltrim(rtrim(str(year(ft.fdata)))) + '/' 
			+ RIGHT('0' + RTRIM(MONTH(ft.fdata)), 2) as path
			, rtrim(ltrim(ft.nmdoc)) + '_' + ltrim(rtrim(str(ft.fno)))+ '_'+ format(ft.fdata, 'yyyyMMdd') as nome
	from ft(nolock) 
	inner join  b_utentes (nolock) on b_utentes.no = ft.no
	where ft.ftstamp =@stamp
End
GRANT EXECUTE ON up_stocks_InventarioStockArmazem TO Public;
-- Note: CONTROL permission was omitted for security reasons. Use it only if necessary.
GO

