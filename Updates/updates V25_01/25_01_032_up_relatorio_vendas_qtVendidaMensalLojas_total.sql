/* Relatório Quantidade Vendida Mensal Totais - Relatório + Utilizado pelas Farmácias (totais)


exec up_relatorio_vendas_qtVendidaMensalLojas_total  @site=N'Loja 1,Loja 2',@dataIni='2024-01-13 00:00:00',@dataFim='2025-01-13 00:00:00',@op=N'',@ref=N'',@design=N'',@familia=N'',@lab=N'',@Dev=0,@pvp=0,@stock=0,@pcl=0,@maxenc=0,@atributosFam=N'',@qttvendida=-9999,@mostraProdutosNIncluidoVendasTag=0,@incluiHistorico=0,@pvu=0,@incluiServicos=0,@fornecedor=N'',@dci=N'',@tipoProduto=N'',@usr1=N'',@generico=N'',@no=N'',@estab=N'0',@ativos=0,@mostraProdutosNIncluidoVendasOp=N'>',@mostraProdutosNIncluidoVendas=N'0',@grphmgcode=N''
	
	exec up_relatorio_vendas_qtVendidaMensalLojas_total
    '20241001',                -- @dataIni
    '20241231',                -- @dataFim
    '',                        -- @op
    '',                        -- @ref
    '',                        -- @design
    '',                        -- @familia
    '',                        -- @lab
    1,                         -- @Dev
    'loja 1,Loja 2, Loja 3',  -- @site
    1,                         -- @pvp
    1,                         -- @stock
    1,                         -- @pcl
    1,                         -- @maxenc
    '',                        -- @atributosFam
    '0',                        -- @qttvendida
    1,                         -- @mostraProdutosNIncluidoVendasTag
    1,                         -- @incluiHistorico
    1,                         -- @pvu
    1,                         -- @incluiServicos
    '',                        -- @fornecedor
    '',                        -- @dci
    '',                        -- @tipoProduto
    '',                        -- @usr1
    '',                        -- @generico
    '',                        -- @no
    0,                        -- @estab
    1,                         -- @ativos
    '',                        -- @mostraProdutosNIncluidoVendasOp
    '',                        -- @mostraProdutosNIncluidoVendas
    ''                         -- @grphmgcode

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_vendas_qtVendidaMensalLojas_total]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_qtVendidaMensalLojas_total
go

create procedure [dbo].[up_relatorio_vendas_qtVendidaMensalLojas_total]
	 @dataIni							as datetime
	,@dataFim							as datetime
	,@op								varchar(55)
	,@ref								as varchar(1024)
	,@design							as varchar(254)
	,@familia							as varchar(max)
	,@lab								as varchar(254)
	,@Dev								as bit
	,@site								as varchar(254)
	,@pvp								as bit
	,@stock								as bit
	,@pcl								as bit
	,@maxenc							as bit
	,@atributosFam						as varchar(max)
	,@qttvendida						as numeric(9,0)
	,@mostraProdutosNIncluidoVendasTag	as bit
	,@incluiHistorico					bit = 0
	,@pvu								as bit = 0
	,@incluiServicos					as bit
	,@fornecedor						as varchar(254)
	,@dci								as varchar(max)
	,@tipoProduto						as varchar(254)
	,@usr1								as varchar(254)
	,@generico							varchar(18) = ''
	,@no								as varchar(9) = ''
	,@estab								as numeric(5,0) = -1
	,@ativos							as bit
	,@mostraProdutosNIncluidoVendasOp	as varchar(2)=''
	,@mostraProdutosNIncluidoVendas		as varchar(10)=''
	,@grphmgcode						as varchar(max)

	

/* with encryption */
AS
BEGIN	
	
	set language portuguese
	If OBJECT_ID('tempdb.dbo.#dadosVendasBase') IS NOT NULL
		DROP TABLE #dadosVendasBase
	If OBJECT_ID('tempdb.dbo.#dadosST') IS NOT NULL
		DROP TABLE #dadosST
	If OBJECT_ID('tempdb.dbo.#dadosFamilia') IS NOT NULL
		DROP TABLE #dadosFamilia
	If OBJECT_ID('tempdb.dbo.#qtVendidas') IS NOT NULL
		DROP TABLE #qtVendidas
	If OBJECT_ID('tempdb.dbo.#dadosGerais') IS NOT NULL
		DROP TABLE #dadosGerais
	If OBJECT_ID('tempdb.dbo.#dadosResumidos') IS NOT NULL
		DROP TABLE #dadosResumidos
	If OBJECT_ID('tempdb.dbo.#EmpresaArm') IS NOT NULL
		DROP TABLE #EmpresaArm
	If OBJECT_ID('tempdb.dbo.#dadosMarcas') IS NOT NULL
		DROP TABLE #dadosMarcas
	If OBJECT_ID('tempdb.dbo.#dadosAnoMes') IS NOT NULL
		drop table #dadosAnoMes;
	If OBJECT_ID('tempdb.dbo.#dadosOperadores') IS NOT NULL
			drop table #dadosOperadores;
	If OBJECT_ID('tempdb.dbo.#dadosSTFiltrado') IS NOT NULL
		DROP TABLE #dadosSTFiltrado
	If OBJECT_ID('tempdb.dbo.#dadosAnoMesRef') IS NOT NULL
		drop table #dadosAnoMesRef;

	SET @atributosFam = (CASE WHEN (@atributosFam IS NULL OR @atributosFam = '') THEN ';;;' ELSE @atributosFam END)

	DECLARE @departamento VARCHAR(254) = ''
	SET @departamento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 1),'') 
	DECLARE @sessao       VARCHAR(254) = ''
	set @sessao = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 2) ,'')
	DECLARE @categoria    VARCHAR(254) = ''
	SET @categoria = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 3),'') 
	DECLARE @segmento     VARCHAR(254) = ''
	SET @segmento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 4),'') 

	/* Calc Loja e Armazens */

	--declare @site_nr as tinyint
	--set @site_nr = ISNULL((select no from empresa where site = @site),0)


	if(@site='TODAS')
		set @site=''
	if @site = ''
		set @site = isnull((select  convert(varchar(254),(select 
						site + ', '
					from 
						empresa (nolock)							
					FOR XML PATH(''))) as no),0)


		/* Converter Valor Generico*/
	IF @generico = 'GENERICO'
		set @generico = 1
	ELSE IF @generico = 'NÃO GENERICO'
		set @generico = 0

	Declare @dataactual as datetime
	set  @dataactual = DATEADD(month, DATEDIFF(month, 0, @dataIni), 0) 
		
	SELECT '19000101' as data, 1900 as ano,12 as mes INTO  #dadosAnoMes where 1=0
		
	WHILE @dataactual <= @dataFim
		BEGIN
			INSERT INTO #dadosAnoMes 
			SELECT @DATAACTUAL as data, year(@DATAACTUAL) as ano, month(@DATAACTUAL) as mes
			
			SET @DATAACTUAL = DATEADD(MM,1,@DATAACTUAL)
		END

	select 
		armazem
	into
		#EmpresaArm
	From
		empresa (nolock)
		inner join empresa_arm (nolock) on empresa.no = empresa_arm.empresa_no
	where
		empresa.site in (Select items from dbo.up_splitToTable(@site, ',')) 

	/* Dados Base Vendas  */
	create table #dadosVendasBase (
		nrAtend varchar(20) 
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)

	)

   
    insert #dadosVendasBase
    exec up_relatorio_vendas_base_Detalhe @dataIni, @dataFim, @site, @incluiHistorico

	/* Elimina Vendas tendo em conta tipo de cliente */
	
	delete from 
		#dadosVendasBase 
	where  
		#dadosVendasBase.no between 1 and 198  
		and #dadosVendasBase.loja_nr in (select no from empresa (nolock) WHERE tipoempresa in ('FARMACIA', 'PARAFARMACIA') )

	--select * from #dadosVendasBase
	DECLARE @firstSite varchar(50)
	DECLARE @firstSiteint int

	/* Calc Dados Produto */	
	Select distinct
		st.ref
		, st.design as design 
		
		,stock = isnull((select SUM(stock) from sa (nolock) where sa.ref = st.ref 
						and armazem in (select armazem from #EmpresaArm where armazem in (select armazem from empresa_arm where empresa_no = st.site_nr))),st.stock)	
		
		,st.u_lab
		,st.usr1
		,st.familia
		,epv1 
		,epv1Siva = case when st.ivaincl = 1 then ISNULL(st.epv1,1) / (isnull(taxasiva.taxa,1)/100+1) else ISNULL(st.epv1,1) end
		,st.inactivo
		,epcult
		--,st.stmax
		,st.ptoenc
		,st.fornecedor	
		, ISNULL(A.descr,'') as departamento
		, ISNULL(B.descr,'') as seccao
		, ISNULL(C.descr,'') as categoria
		, ISNULL(D.descr,'') as segmento
		,tipoProduto =  isnull(b_famFamilias.design,'')
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 1) valDepartamento
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 2) valSeccao
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 3) valCategoria
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 4) valSegmento
		,st.ivaincl
		,st.stns
		,fprod.dcipt_id
		,st.site_nr
		,empresa.site
		,st.marg4 as bb
	into 
		#dadosST
	From
		st (nolock)
		inner join empresa (nolock) on empresa.no = st.site_nr
		left join fprod (nolock) on st.ref = fprod.cnp
		left join b_famFamilias (nolock) on st.u_famstamp = b_famFamilias.famstamp	
		left join grande_mercado_hmr (nolock) A on A.id = st.u_depstamp
		left join mercado_hmr (nolock) B on B.id = st.u_secstamp
		left join categoria_hmr (nolock) C on C.id = st.u_catstamp 
		left join segmento_hmr (nolock) D on D.id = st.u_segstamp				
		left join taxasiva (nolock) on st.tabiva = taxasiva.codigo 
	where
		empresa.site in (Select items from dbo.up_splitToTable(@site, ',')) 
		and (
			st.usr1 in (select items from dbo.up_splitToTable(@usr1,','))
			or @usr1 = '0'
			or @usr1 = ''
		)
		and isnull(fprod.generico,0) = case when @generico = '' then isnull(fprod.generico,0) else @generico end
		and st.inactivo between 0 and (case when @ativos=1 then 0 else 1 end)
		and (isnull(fprod.grphmgcode,'') in (Select items from dbo.up_splitToTable(@grphmgcode,',')) or @grphmgcode = '') 

--select * from #dadosST

	/* Calc Familias */
	Select
		distinct ref
	into
		#dadosFamilia
	From
		stfami (nolock)
	Where
		ref in (Select items from dbo.up_splitToTable(@familia,','))
		Or @familia = ''

	/* Calc operadores */
	Select
		iniciais,
		cm = userno,
		username
	into
		#dadosOperadores
	from
		b_us (nolock)
	Where
		userno in (select items from dbo.up_splitToTable(@op,','))
		or @op= '0'
		or @op= ''

	/* Calc Result Set */
	SELECT
		'ANO'			= year(fdata)
		,'MES'			= STR(MONTH(fdata)) + ' - ' + LEFT(UPPER(datename(month,fdata)),3)
		,'REF'			= cteSt.ref
		,'DESIGN'		= cteSt.design
		,'QTD'			= SUM(ft.qtt)
		/*
		,'STOCKACTUAL'	= cteSt.stock
		,'STOCKMAX'		= cteSt.stmax
		*/
		,'PTOENC'		= cteSt.ptoenc
		,'FAMILIA'		= ft.familia
		,'LAB'			= u_lab
		,vdpvpcl		= SUM(etiliquido)
		,vdpvp			= SUM(etiliquido + ettent1 + ettent2)
		,vdpvcl			= SUM(etiliquidoSiva)
		,vdpv			= SUM(etiliquidoSiva + ettent1Siva + ettent2Siva)
		,'INACTIVO'		= cteSt.inactivo
		,pclficha		= cteSt.epcult
		,vdpcl			= SUM(ft.ecusto)
		,servico		= cteSt.stns
		,pvFicha		= cteSt.epv1Siva
		,pvpFicha		= cteSt.epv1
		,dci			= cteSt.dcipt_id
		,usr1			= cteSt.usr1
	    ,site			= cteSt.site
		,ROW_NUMBER() OVER(PARTITION BY  cteSt.ref , cteSt.site ORDER BY cteSt.ref DESC, cteSt.site DESC)  AS rowline 
		, '1' as rowsubline
		,MONTH(fdata) AS monthint
		,bb				= cteSt.bb*SUM(ft.qtt)
	into
		#qtVendidas
	FROM
		#dadosVendasBase ft
		inner join #dadosST cteSt on ft.ref = cteSt.ref and ft.loja_nr = cteSt.site_nr
		inner join empresa (nolock) on empresa.site = cteSt.site
		left join fprod (nolock) on ft.ref = fprod.cnp
	WHERE
		ft.no = case when @no = '' then ft.no else @no end		
		and ( ft.no = 0 or ft.no > 198 or empresa.tipoempresa = 'CLINICA' )		 		
		and ft.estab = case when @estab>-1 then  @estab else ft.estab  end
		and	(ft.tipodoc!=4 or ft.u_tipodoc=4) AND ft.u_tipodoc != 1 AND ft.u_tipodoc != 5 
		and (ft.ref in (Select items from dbo.up_splitToTable(@ref,',')) or @ref = '')
		and cteSt.DESIGN like '%' + @design + '%'
		and ft.familia in (Select ref from #dadosFamilia)
		and ft.ousrinis in (select iniciais from #dadosOperadores)
		and (isnull(cteSt.u_lab,'') in (Select items from dbo.up_splitToTable(@lab,';')) or @lab = '')
		and isnull(ctest.fornecedor,'') = case when @fornecedor = '' then isnull(ctest.fornecedor,'') else @fornecedor end
		and (isnull(cteSt.dcipt_id,'') in (Select items from dbo.up_splitToTable(@dci,';')) or @dci = '')
		and isnull(cteSt.tipoProduto, '') = case when @tipoProduto = '' then isnull(cteSt.tipoProduto, '') else @tipoProduto end
		AND departamento = (CASE WHEN @departamento = '' THEN departamento ELSE @departamento END)
		AND seccao = (CASE WHEN @sessao = '' THEN seccao ELSE  @sessao END)
		AND categoria = (CASE WHEN @categoria = '' THEN categoria ELSE  @categoria END)
		AND segmento = (CASE WHEN @segmento = '' THEN segmento ELSE  @segmento END) 
			and (isnull(fprod.grphmgcode,'') in (Select items from dbo.up_splitToTable(@grphmgcode,',')) or @grphmgcode = '') 
	GROUP BY 
		year(fdata)
		,datename(MONTH,fdata)
		,MONTH(fdata)
		,cteSt.ref
		,cteSt.design
		--,cteSt.stock
		,ft.familia
		,cteSt.u_lab
		,cteSt.epv1
		,cteSt.inactivo
		,cteSt.epcult
		,cteSt.ptoenc
		,cteSt.epv1Siva
		,cteSt.stns
		,ctest.dcipt_id
		,cteSt.usr1
		,cTeSt.site
		,cteSt.bb

--select *  from #qtVendidas 

	/*Elimina Qtt Vendida Inferir a x */
	if @qttvendida > 0
		delete from #qtVendidas where ref in (select ref from #qtVendidas group by ref having sum(qtd) < @qttvendida ) 
		
	/* Elimina Servicos*/
	if @incluiServicos = 0
		delete from #qtVendidas where servico = 1

	/* Devoluções a Fornecedor */
	if @Dev = 1
		begin
			insert into #qtVendidas
			SELECT
				'ANO'			= year(bo.dataobra)
				,'MES'			= STR(MONTH(bo.dataobra)) + ' - ' + LEFT(UPPER(datename(month,bo.dataobra)),3)
				,'REF'			= cteSt.ref
				,'DESIGN'		= bi.design
				,'QTD'			= SUM(bi.qtt)
				/*
				,'STOCKACTUAL'	= cteSt.stock
				,'STOCKMAX'		= cteSt.stmax
				*/
				,'PTOENC'		= cteSt.ptoenc
				,'FAMILIA'		= ISNULL(bi.familia,99)
				,'LAB'			= u_lab
				,vdpvpcl		= 0
				,vdpvp			= 0
				,vdpvcl			= 0
				,vdpv			= 0
				,'INACTIVO'		= cteSt.inactivo
				,pclficha		= cteSt.epcult
				,vdpcl			= 0
				,servico		= cteSt.stns
				,pvFicha		= cteSt.epv1Siva
				,pvpFicha		= cteSt.epv1
				,dci			= cteSt.dcipt_id	
				,usr1			= cteSt.usr1
				, site			= cteSt.site
				--,'STOCKACTUALAux'	= cteSt.stock
				,ROW_NUMBER() OVER(PARTITION BY  cteSt.ref , cteSt.site ORDER BY cteSt.ref DESC, cteSt.site DESC)  AS rowline 
			    , '1' AS rowsubline
				,MONTH(bo.dataobra) AS monthint
				, bb			= cteSt.bb*SUM(bi.qtt)
			FROM
				bi (nolock)
				inner join bo (nolock) on bo.bostamp=bi.bostamp
				inner join #dadosSt cteSt on bi.ref=cteSt.ref and bo.site = cteSt.site
				left join fprod (nolock) on bi.ref = fprod.cnp			
			WHERE
				bo.nmdos = 'Devol. a Fornecedor'
				and bo.no = case when @no = '' then bo.no else @no end
				and bo.estab = case when @estab >-1 then bo.estab else @estab end
				and (bo.dataobra between @dataIni and @dataFim) 
				and (bi.ref in (Select items from dbo.up_splitToTable(@ref,',')) or @ref = '')
				and BI.DESIGN like '%' + @design + '%'
				and bi.familia in (Select ref from #dadosFamilia)
				and (isnull(cteSt.u_lab,'') in (Select items from dbo.up_splitToTable(@lab,';')) or @lab = '')			
				and bo.site  in (Select items from dbo.up_splitToTable(@site, ',')) 
				and (isnull(fprod.grphmgcode,'') in (Select items from dbo.up_splitToTable(@grphmgcode,',')) or @grphmgcode = '') 
				and (isnull(cteSt.dcipt_id,'') in (Select items from dbo.up_splitToTable(@dci,';')) or @dci = '')
			GROUP BY
				year(bo.dataobra)
				,datename(month,bo.dataobra)
				,MONTH(bo.dataobra)
				,cteSt.ref
				,bi.design
				,bi.familia
				,cteSt.u_lab
				,cteSt.epv1
				,cteSt.inactivo
				,cteSt.epcult
				/*
				,cteSt.stock
				,cteSt.stmax 
				*/
				,cteSt.ptoenc
				,cteSt.epv1Siva
				,cteSt.stns
				,cteSt.dcipt_id
				,cteSt.usr1
				,cteSt.site
				,cteSt.bb
		end

	/* Inclui todos os produtos, mesmo os que não estão incluidos nas vendas, está separado porque esta opção é muito mais lenta */
	IF @mostraProdutosNIncluidoVendasTag = 1 
		BEGIN 
			select top 0
				*
			into
				#dadosSTFiltrado
			From #dadosST

			if  @mostraProdutosNIncluidoVendasOp = '<'
				begin 
					insert into #dadosSTFiltrado
					select * from #dadosST
					where
						 #dadosST.stock < @mostraProdutosNIncluidoVendas
				End
			else if @mostraProdutosNIncluidoVendasOp = '='
				begin 
					insert into #dadosSTFiltrado
					select * from #dadosST
					where
						 #dadosST.stock = @mostraProdutosNIncluidoVendas
				End
			else if @mostraProdutosNIncluidoVendasOp = '>'
				begin 
					insert into #dadosSTFiltrado
					select * from #dadosST
					where
						 #dadosST.stock > @mostraProdutosNIncluidoVendas
				End
			else if @mostraProdutosNIncluidoVendasOp = '>='
				begin 
					insert into #dadosSTFiltrado
					select * from #dadosST
					where
						 #dadosST.stock >= @mostraProdutosNIncluidoVendas
				End
			else if @mostraProdutosNIncluidoVendasOp = '<='
				begin 
					insert into #dadosSTFiltrado
					select * from #dadosST
					where
						 #dadosST.stock <= @mostraProdutosNIncluidoVendas
				End
			else 
				begin 
					insert into #dadosSTFiltrado
					select * from #dadosST
				End
		
			insert into #qtVendidas
			SELECT	
				'ANO'			= dam.ano
				,'MES'			= STR(MONTH(data)) + ' - ' + LEFT(UPPER(datename(month,data)),3)
				,'REF'			= st.ref
				,'DESIGN'		= st.design
				,'QTD'			= 0
				,'PTOENC'		= st.ptoenc
				,'FAMILIA'		= st.familia
				,'LAB'			= st.u_lab
				,vdpvpcl		= 0
				,vdpvp			= 0
				,vdpvcl			= 0
				,vdpv			= 0
				,'INACTIVO'		= st.inactivo
				,pclficha		= st.epcult
				,vdpcl			= 0
				,servico		= st.stns
				,pvFicha		= st.epv1Siva
				,pvpFicha		= st.epv1
				,dci			= st.dcipt_id
				,usr1			= st.usr1
				,site			= st.site
				/*
				,'STOCKACTUAL'	= st.stock
				,'STOCKMAX'		= st.stmax
				,'STOCKACTUALAux'	= st.stock
				*/
				,ROW_NUMBER() OVER(PARTITION BY  st.ref , st.site ORDER BY st.ref DESC, st.site DESC)  AS rowline 
				, '2' AS rowsubline
				,MONTH(data) AS monthint
				,bb				= 0
			from 
				#dadosAnoMes dam
				,#dadosSTFiltrado st
				left join fprod (nolock) on st.ref = fprod.cnp
			Where
				(st.ref in (Select items from dbo.up_splitToTable(@ref,',')) or @ref = '')
				and st.DESIGN like '%' + @design + '%'
				and st.familia in (Select ref from #dadosFamilia)
				and (isnull(st.u_lab,'') in (Select items from dbo.up_splitToTable(@lab,';')) or @lab = '')
				and isnull(st.fornecedor,'') = case when @fornecedor = '' then isnull(st.fornecedor,'') else @fornecedor end
				and (isnull(st.dcipt_id,'') in (Select items from dbo.up_splitToTable(@dci,';')) or @dci = '')
				and isnull(st.tipoProduto, '') = case when @tipoProduto = '' then isnull(st.tipoProduto, '') else @tipoProduto end
				AND departamento = (CASE WHEN @departamento = '' THEN departamento ELSE @departamento END)
				AND seccao = (CASE WHEN @sessao = '' THEN seccao ELSE  @sessao END)
				AND categoria = (CASE WHEN @categoria = '' THEN categoria ELSE  @categoria END)
				AND segmento = (CASE WHEN @segmento = '' THEN segmento ELSE  @segmento END)  
					and (isnull(fprod.grphmgcode,'') in (Select items from dbo.up_splitToTable(@grphmgcode,',')) or @grphmgcode = '') 	
		END

	select top 0
			dam.*,
			#qtVendidas.ref,
			#qtVendidas.site as site
	into
		 #dadosAnoMesRef
	from 
		#dadosAnoMes dam,
		#qtVendidas
	
	insert into #dadosAnoMesRef
	SELECT
		dam.*,
		#qtVendidas.ref,
		#qtVendidas.site as site
	from 
		#dadosAnoMes dam
		, #qtVendidas 
	
	delete 
		#dadosAnoMesRef  
	FROM
		 #dadosAnoMesRef 
		 INNER JOIN #qtVendidas  ON (#dadosAnoMesRef.ref=#qtVendidas.REF and
									 #dadosAnoMesRef.ano = #qtVendidas.ano and
									  #dadosAnoMesRef.mes = #qtVendidas.monthint and
									   #dadosAnoMesRef.site= #qtVendidas.site)
	
	insert into #qtVendidas	
	SELECT	
		'ANO'			= damRef.ano
		,'MES'			= STR(MONTH(data)) + ' - ' + LEFT(UPPER(datename(month,data)),3)
		,'REF'			= st.ref
		,'DESIGN'		= st.design
		,'QTD'			= 0
		/*
		,'STOCKACTUAL'	= st.stock
		,'STOCKMAX'		= st.stmax
		,'STOCKACTUALAux'	= st.stock
		*/
		,'PTOENC'		= st.ptoenc
		,'FAMILIA'		= st.familia
		,'LAB'			= st.u_lab
		,vdpvpcl		= 0
		,vdpvp			= 0
		,vdpvcl			= 0
		,vdpv			= 0
		,'INACTIVO'		= st.inactivo
		,pclficha		= st.epcult
		,vdpcl			= 0
		,servico		= st.stns
		,pvFicha		= st.epv1Siva
		,pvpFicha		= st.epv1
		,dci			= st.dcipt_id
		,usr1			= st.usr1
		,site			= st.site		
		,ROW_NUMBER() OVER(PARTITION BY  st.ref , st.site ORDER BY st.ref DESC, st.site DESC)  AS rowline 
		, '2' AS rowsubline
		,MONTH(data) AS monthint
		,bb				= 0
	from 
		#dadosAnoMesRef damRef
		INNER JOIN #dadosST st  ON ST.REF = damRef.REF and ST.site=damRef.site
		left join fprod (nolock) on st.ref = fprod.cnp
	Where
		(st.ref in (Select distinct ref from #qtVendidas ) or @ref = '')
		and st.DESIGN like '%' + @design + '%'
		and st.familia in (Select ref from #dadosFamilia)
		and (isnull(st.u_lab,'') in (Select items from dbo.up_splitToTable(@lab,';')) or @lab = '')
		and isnull(st.fornecedor,'') = case when @fornecedor = '' then isnull(st.fornecedor,'') else @fornecedor end
		and (isnull(st.dcipt_id,'') in (Select items from dbo.up_splitToTable(@dci,';')) or @dci = '')
		and isnull(st.tipoProduto, '') = case when @tipoProduto = '' then isnull(st.tipoProduto, '') else @tipoProduto end
		AND departamento = (CASE WHEN @departamento = '' THEN departamento ELSE @departamento END)
		AND seccao = (CASE WHEN @sessao = '' THEN seccao ELSE  @sessao END)
		AND categoria = (CASE WHEN @categoria = '' THEN categoria ELSE  @categoria END)
		AND segmento = (CASE WHEN @segmento = '' THEN segmento ELSE  @segmento END) 
			and (isnull(fprod.grphmgcode,'') in (Select items from dbo.up_splitToTable(@grphmgcode,',')) or @grphmgcode = '') 

	--update #qtVendidas set STOCKACTUALAux=0 where  rowline !=1 or (rowsubline=2 and rowline=1 and isnull((select count(*) from #qtVendidas qt1 where qt1.ref=#qtVendidas.ref and qt1.site=#qtVendidas.site and rowsubline=1 and rowsubline=1 ),0)>=1 )
		  
	SELECT
		ANO as ANO_t,			
		MES as MES_t,			
		(select sum(QTD) from #qtVendidas qttTemp where qttTemp.MES = #qtVendidas.MES and qttTemp.site = #qtVendidas.site) as QTD_t,			
		(select sum(vdpvpcl) from #qtVendidas qttTemp where qttTemp.MES = #qtVendidas.MES and qttTemp.site = #qtVendidas.site) as vdpvpcl_t,
		(select sum(vdpvp) from #qtVendidas qttTemp where qttTemp.MES = #qtVendidas.MES and qttTemp.site = #qtVendidas.site) as vdpvp_t,
		(select sum(vdpvcl) from #qtVendidas qttTemp where qttTemp.MES = #qtVendidas.MES and qttTemp.site = #qtVendidas.site) as vdpvcl_t,
		(select sum(vdpv) from #qtVendidas qttTemp where qttTemp.MES = #qtVendidas.MES and qttTemp.site = #qtVendidas.site) as vdpv_t,			
		(select sum(pclficha) from #qtVendidas qttTemp where qttTemp.MES = #qtVendidas.MES and qttTemp.site = #qtVendidas.site) as pclficha_t,
		(select sum(vdpcl) from #qtVendidas qttTemp where qttTemp.MES = #qtVendidas.MES and qttTemp.site = #qtVendidas.site) as vdpcl_t,		
		site as site_t,
		(select sum(bb) from #qtVendidas qttTemp where qttTemp.MES = #qtVendidas.MES and qttTemp.site = #qtVendidas.site) as bb_t	
		/*
		(select sum(STOCKACTUAL) from #qtVendidas qttTemp where qttTemp.MES = #qtVendidas.MES and qttTemp.site = #qtVendidas.site) as STOCKACTUAL_t,			
		(select sum(STOCKACTUALAux) from #qtVendidas qttTemp where qttTemp.MES = #qtVendidas.MES and qttTemp.site = #qtVendidas.site) as STOCKACTUALAux_t
		--rowline as rowline_t, 
		--rowsubline as rowsubline_t,
		--monthint as monthint_t
		*/
	iNTO   
		#tEMPtOTAIS
	FROM #qtVendidas
	ORDER BY ano, mes ;

	SELECT * FROM #tEMPtOTAIS
	GROUP BY 
		ANO_t,			
		MES_t,			
		QTD_t,				
		vdpvpcl_t,		
		vdpvp_t,			
		vdpvcl_t,			
		vdpv_t,			
		pclficha_t,		
		vdpcl_t,			
		site_t,
		bb_t
				
		union all
		 
		select 
			ano
			,mes
			,sum(QTD)
			,sum(vdpvpcl)
			,sum(vdpvp)
			,sum(vdpvcl)
			,sum(vdpv)
			,sum(pclficha)
			,sum(vdpcl)
			,'Total' as site_t 
			,sum(bb)
		FROM #qtVendidas
		group by ano, mes, qtd, vdpvpcl,vdpvp,vdpvcl,vdpv,pclficha,vdpcl,bb
		

	If OBJECT_ID('tempdb.dbo.#dadosVendasBase') IS NOT NULL
		DROP TABLE #dadosVendasBase
	If OBJECT_ID('tempdb.dbo.#dadosST') IS NOT NULL
		DROP TABLE #dadosST
	If OBJECT_ID('tempdb.dbo.#dadosFamilia') IS NOT NULL
		DROP TABLE #dadosFamilia
	If OBJECT_ID('tempdb.dbo.#qtVendidas') IS NOT NULL
		DROP TABLE #qtVendidas
	If OBJECT_ID('tempdb.dbo.#dadosGerais') IS NOT NULL
		DROP TABLE #dadosGerais
	If OBJECT_ID('tempdb.dbo.#dadosResumidos') IS NOT NULL
		DROP TABLE #dadosResumidos
	If OBJECT_ID('tempdb.dbo.#EmpresaArm') IS NOT NULL
		DROP TABLE #EmpresaArm
	If OBJECT_ID('tempdb.dbo.#dadosMarcas') IS NOT NULL
		DROP TABLE #dadosMarcas
	If OBJECT_ID('tempdb.dbo.#dadosAnoMes') IS NOT NULL
		DROP TABLE #dadosAnoMes;
	If OBJECT_ID('tempdb.dbo.#dadosOperadores') IS NOT NULL
		DROP TABLE #dadosOperadores;
	If OBJECT_ID('tempdb.dbo.#dadosSTFiltrado') IS NOT NULL
		DROP TABLE #dadosSTFiltrado

	If OBJECT_ID('tempdb.dbo.#dadosAnoMesRef') IS NOT NULL
		drop table #dadosAnoMesRef;

END

GO
Grant Execute on dbo.up_relatorio_vendas_qtVendidaMensalLojas_total to Public
Grant control on dbo.up_relatorio_vendas_qtVendidaMensalLojas_total to Public
GO