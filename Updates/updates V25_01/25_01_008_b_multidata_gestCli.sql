






GO
IF not EXISTS (SELECT 1 FROM b_multidata WHERE campo = 'Francisco Seixas' and tipo = 'a_gestCli')  
	BEGIN
		INSERT INTO [dbo].[b_multidata]
				   ([multidatastamp]
				   ,[campo]
				   ,[tipo]
				   ,[ousrinis]
				   ,[ousrdata]
				   ,[ousrhora]
				   ,[usrinis]
				   ,[usrdata]
				   ,[usrhora]
				   ,[code_motive]
				   ,[pais]
				   ,[code]
				   ,[ordem])
			 VALUES
				   (LEFT(NEWID(),25)
				   ,'Francisco Seixas'
				   ,'a_gestCli'
				   ,'ADM'
				   ,GETDATE()
				   ,LEFT(CONVERT(VARCHAR(8), GETDATE(), 108), 5)
				   ,'ADM'
				   ,GETDATE()
				   ,LEFT(CONVERT(VARCHAR(8), GETDATE(), 108), 5)
				   ,''
				   ,''
				   ,'FS'
				   ,9999999)
	END
GO


GO
IF not EXISTS (SELECT 1 FROM b_multidata WHERE campo = 'Inês Jacinto' and tipo = 'a_gestCli')  
	BEGIN
		INSERT INTO [dbo].[b_multidata]
				   ([multidatastamp]
				   ,[campo]
				   ,[tipo]
				   ,[ousrinis]
				   ,[ousrdata]
				   ,[ousrhora]
				   ,[usrinis]
				   ,[usrdata]
				   ,[usrhora]
				   ,[code_motive]
				   ,[pais]
				   ,[code]
				   ,[ordem])
			 VALUES
				   (LEFT(NEWID(),25)
				   ,'Inês Jacinto'
				   ,'a_gestCli'
				   ,'ADM'
				   ,GETDATE()
				   ,LEFT(CONVERT(VARCHAR(8), GETDATE(), 108), 5)
				   ,'ADM'
				   ,GETDATE()
				   ,LEFT(CONVERT(VARCHAR(8), GETDATE(), 108), 5)
				   ,''
				   ,''
				   ,'IJ'
				   ,9999999)
	END
GO




GO
IF not EXISTS (SELECT 1 FROM b_multidata WHERE campo = 'João Barbosa' and tipo = 'a_gestCli')  
	BEGIN
		INSERT INTO [dbo].[b_multidata]
				   ([multidatastamp]
				   ,[campo]
				   ,[tipo]
				   ,[ousrinis]
				   ,[ousrdata]
				   ,[ousrhora]
				   ,[usrinis]
				   ,[usrdata]
				   ,[usrhora]
				   ,[code_motive]
				   ,[pais]
				   ,[code]
				   ,[ordem])
			 VALUES
				   (LEFT(NEWID(),25)
				   ,'João Barbosa'
				   ,'a_gestCli'
				   ,'ADM'
				   ,GETDATE()
				   ,LEFT(CONVERT(VARCHAR(8), GETDATE(), 108), 5)
				   ,'ADM'
				   ,GETDATE()
				   ,LEFT(CONVERT(VARCHAR(8), GETDATE(), 108), 5)
				   ,''
				   ,''
				   ,'JB'
				   ,9999999)
	END
GO




GO
IF not EXISTS (SELECT 1 FROM b_multidata WHERE campo = 'Vânia Fernandes' and tipo = 'a_gestCli')  
	BEGIN
		INSERT INTO [dbo].[b_multidata]
				   ([multidatastamp]
				   ,[campo]
				   ,[tipo]
				   ,[ousrinis]
				   ,[ousrdata]
				   ,[ousrhora]
				   ,[usrinis]
				   ,[usrdata]
				   ,[usrhora]
				   ,[code_motive]
				   ,[pais]
				   ,[code]
				   ,[ordem])
			 VALUES
				   (LEFT(NEWID(),25)
				   ,'Vânia Fernandes'
				   ,'a_gestCli'
				   ,'ADM'
				   ,GETDATE()
				   ,LEFT(CONVERT(VARCHAR(8), GETDATE(), 108), 5)
				   ,'ADM'
				   ,GETDATE()
				   ,LEFT(CONVERT(VARCHAR(8), GETDATE(), 108), 5)
				   ,''
				   ,''
				   ,'VF'
				   ,9999999)
	END
GO

