/* 

 
select * from b_analises (NOLOCK) WHERE ordem=204 

select * from b_analises_config (NOLOCK) WHERE ordem=204 order by fordem

*/


IF( NOT EXISTS (SELECT * FROM B_analises (NOLOCK) WHERE ordem=204))
	BEGIN
		INSERT INTO [dbo].[B_analises]
			([ordem],[grupo],[subgrupo],[descricao]
			,[report],[stamp],[url],[comentario]
			,[ecra],[stamp_analiseav],[estado]
			,[tabela],[perfil],[filtros],[formatoExp]
			,[comandoFox],[objectivo],[marcada])
		VALUES
			(204,'Gestão','Farmácia','Relatório Dispensa em Proximidade'
			,'relatorio_dispensa_proximidade','','','Obs.:'
			,'','',1
			,'','Relatório Dispensa em Proximidade','',''
			,'','',0)
	END


IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=204 and label='Data Início'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 204,[nome],[label],[tipo],[comando],1,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 202 and label='Data Início'
END

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=204 and label='Data Fim'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 204,[nome],[label],[tipo],[comando],2,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 202 and label='Data Fim'
END

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=204 and label='Cliente'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 204,[nome],[label],[tipo],[comando],3,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],0,[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 191 and label='Cliente'
END

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=204 and label='Referência'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 204,[nome],[label],[tipo],[comando],4,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],1,[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 202 and label='Referência'
END

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=204 and label='Num. Receita'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 204,[nome],[label],[tipo],[comando],5,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],'','',0,[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 47 and label='Num. Receita'
END

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=204 and label='Loja'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 204,[nome],[label],[tipo],[comando],6,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],0,[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 202 and label='Loja'
END

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=204 and label='Só medicamentos'))
BEGIN
INSERT INTO [dbo].[b_analises_config]
           ([ordem]
           ,[nome]
           ,[label]
           ,[tipo]
           ,[comando]
           ,[fordem]
           ,[vdefault]
           ,[vdefaultsql]
           ,[vdefaultfox]
           ,[colunas]
           ,[colunasWidth]
           ,[obrigatorio]
           ,[firstempty]
           ,[mascara]
           ,[formato]
           ,[multiselecao]
           ,[comandoesql]
           ,[visivel])
     VALUES
           (204
           ,'prod' 
           ,'Só medicamentos'
           ,'l'
           ,''
           ,7
           ,''
           ,0
           ,0
           ,''
           ,''
           ,0
           ,0
           ,''
           ,''
           ,0
           ,0
           ,1)
END


IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=204 and label='Só serviços'))
BEGIN
INSERT INTO [dbo].[b_analises_config]
           ([ordem]
           ,[nome]
           ,[label]
           ,[tipo]
           ,[comando]
           ,[fordem]
           ,[vdefault]
           ,[vdefaultsql]
           ,[vdefaultfox]
           ,[colunas]
           ,[colunasWidth]
           ,[obrigatorio]
           ,[firstempty]
           ,[mascara]
           ,[formato]
           ,[multiselecao]
           ,[comandoesql]
           ,[visivel])
     VALUES
           (204
           ,'serv'
           ,'Só serviços'
           ,'l'
           ,''
           ,8
           ,''
           ,0
           ,0
           ,''
           ,''
           ,0
           ,0
           ,''
           ,''
           ,0
           ,0
           ,1)
END
