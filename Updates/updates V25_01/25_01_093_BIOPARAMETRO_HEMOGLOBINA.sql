IF NOT EXISTS 
		(SELECT nome 
		 FROM bioparametros_geral
		 WHERE nome = 'Hemoglobina Glicada (HbA1c)'
		)
BEGIN
INSERT INTO [dbo].[bioparametros_geral]

           ([id]
	      ,[nome]
           ,[variavel]
           ,[unidade]
           ,[vref]
           ,[usaValMin]
           ,[textVal]
           ,[textValMin]
           ,[usaDecimal])

		   Values
           ((SELECT (SELECT MAX(convert(int, id)) FROM bioparametros_geral)+1)
           ,'Hemoglobina Glicada (HbA1c)'
           ,'%'
           ,'%'
           ,''
           ,0
           ,''<
           ,''
           ,1)

END
 
GO
 
IF NOT EXISTS
		(SELECT nome 
		 FROM bioparametros
		 WHERE nome = 'Hemoglobina Glicada (HbA1c)'
		)
BEGIN
	INSERT INTO [dbo].[bioparametros]

           ([id]
           ,[nome]
           ,[variavel]
           ,[unidade]
           ,[inactivo]
           ,[tipo]
           ,[obs]
           ,[formula]
           ,[vref]
           ,[usaValMin]
           ,[textVal]
           ,[textValMin]
           ,[usaDecimal])

		SELECT [id]
			,[nome]
			,[variavel]
			,[unidade]
			,(SELECT 0)
			,(SELECT '')
			,(SELECT '')
			,(SELECT '')
			,[vref]
			,[usaValMin]s
			,[textVal]
			,[textValMin]
			,[usaDecimal]
		FROM [dbo].[bioparametros_geral]
			WHERE nome = 'Hemoglobina Glicada (HbA1c)'

END

GO