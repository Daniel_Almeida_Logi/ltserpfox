/*	Relatório de medicamentos veterinários

	 exec up_relatorios_medicamentos_vet '20100815', '20301115', 'saídas', '','','Loja 1'
	 exec up_relatorios_medicamentos_vet '20250207', '20250207', 'saídas', '','','Loja 1'
	 exec up_relatorios_medicamentos_vet '20100815', '20301115', 'entradas', '','','Loja 1'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_relatorios_medicamentos_vet]') IS NOT NULL
    DROP PROCEDURE dbo.up_relatorios_medicamentos_vet
GO

CREATE PROCEDURE dbo.up_relatorios_medicamentos_vet
    @dataIni datetime,
    @dataFim datetime,
    @movimento varchar(18),
    @ref varchar(18),
    @lote varchar(30),
    @SITE varchar(18)
AS

IF OBJECT_ID('tempdb.dbo.#TempMovimentos') IS NOT NULL
    DROP TABLE #TempMovimentos

DECLARE @sitenr int = 0 
SELECT @sitenr = no FROM empresa (nolock)
WHERE site = @SITE

-- Criação da tabela temporária com todas as colunas necessárias
SELECT
    datahora_order = sl.ousrdata + sl.ousrhora,
    datahora = CONVERT(varchar, sl.datalc, 111),
    st.ref, 
    st.design, 
    sl.lote,
    validade = CASE 
                   WHEN sl.fistamp != '' THEN CONVERT(varchar, fi2.dataValidade, 111)
                   WHEN sl.bistamp != '' THEN '1900/01/01'
                   WHEN sl.fnstamp != '' THEN '1900/01/01'
                   ELSE '1900/01/01'
               END,

    ENTRADAS = CASE 
                    WHEN sl.cm < 50 THEN 
                        CASE WHEN sl.qtt < 0 THEN 0 ELSE CAST(sl.qtt AS int) END
                    ELSE 
                        CASE WHEN sl.qtt < 0 AND CAST(sl.cm AS int) > 50 THEN CAST(sl.qtt * -1 AS int) ELSE 0 END
               END,

    SAIDAS = CASE 
                    WHEN sl.cm > 50 AND sl.qtt > 0 THEN CAST(sl.qtt AS int)
                    ELSE 
                        CASE WHEN sl.cm < 50 AND sl.qtt < 0 THEN CAST(sl.qtt AS int) ELSE 0 END
                END,

    sl.nome, 
    sl.adoc,
    documento = CASE 
                   WHEN sl.fistamp != '' THEN fi.nmdoc
                   WHEN sl.bistamp != '' THEN bi.nmdos
                   WHEN sl.fnstamp != '' THEN fn.docnome
                   ELSE ''
               END,

    receita = CASE 
                   WHEN sl.fistamp != '' THEN ft2.u_receita
                   WHEN sl.bistamp != '' THEN ''
                   WHEN sl.fnstamp != '' THEN ''
                   ELSE ''
               END

INTO #TempMovimentos
FROM sl (nolock)
INNER JOIN st ON st.ref = sl.ref AND sl.armazem IN (SELECT armazem FROM empresa_arm (nolock) WHERE empresa_no = @sitenr)
LEFT JOIN fi (nolock) ON fi.fistamp = sl.fistamp AND sl.fistamp != ''
LEFT JOIN fi2 (nolock) ON fi2.fistamp = sl.fistamp AND sl.fistamp != ''
LEFT JOIN ft2 (nolock) ON ft2.ft2stamp = fi.ftstamp AND sl.fistamp != ''
LEFT JOIN bi (nolock) ON bi.bistamp = sl.bistamp AND sl.bistamp != ''
LEFT JOIN fn (nolock) ON fn.fnstamp = sl.fnstamp AND sl.fnstamp != ''

WHERE st.familia IN (97, 98) AND st.faminome LIKE '%Veterinária%' -- medicamentos veterinários
    AND sl.datalc BETWEEN @dataIni AND @dataFim 
    AND (@ref = '' OR sl.ref = @ref) 
    AND (@lote = '' OR sl.lote = @lote)


	DECLARE @sql NVARCHAR(MAX);

SET @sql = N'
    SELECT *
    FROM #TempMovimentos'
	+
		case when @movimento = 'todos'
			then
				' '
			else
				case when @movimento = 'saídas'
					then
						' Where saídas > 0 '
					else
						' Where entradas > 0 '
					end
			end
	+
    'GROUP BY 
         datahora_order,
         datahora,
         ref,
         design,
         lote,
         validade,
         ENTRADAS,
         SAIDAS,
         nome,
         adoc,
         documento,
         receita
'

EXEC sp_executesql @sql

IF OBJECT_ID('tempdb.dbo.#TempMovimentos') IS NOT NULL
    DROP TABLE #TempMovimentos

GO

GRANT EXECUTE ON dbo.up_relatorios_medicamentos_vet TO Public
GRANT CONTROL ON dbo.up_relatorios_medicamentos_vet TO Public

