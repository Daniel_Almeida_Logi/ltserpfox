--	exec up_stocks_InventarioStockArmazem '3809787           ', 1, '','20250114 11:36:00' 


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



IF OBJECT_ID('[dbo].[up_stocks_InventarioStockArmazem]') IS NOT NULL
    DROP PROCEDURE up_stocks_InventarioStockArmazem
GO

CREATE PROCEDURE up_stocks_InventarioStockArmazem
@ref VARCHAR(18),
@armazem NUMERIC(5,0),
@lote VARCHAR(30),
@data DATETIME
AS
BEGIN
    DECLARE @sql NVARCHAR(MAX);
    DECLARE @parameters NVARCHAR(MAX);

    -- Base SQL query
    SET @sql = 'SELECT ISNULL(SUM(CASE WHEN cm < 50 THEN qtt ELSE -qtt END), 0) AS stock ' +
               'FROM sl (NOLOCK) ' +
               'WHERE ref = @ref AND armazem = @armazem AND datalc + ousrhora <= @data';

    -- Add condition for lote if applicable
    IF EXISTS (SELECT 1 FROM b_parameters(nolock) WHERE stamp = 'ADM0000000211' AND bool = 1) AND @lote <> ''
    BEGIN
        SET @sql = @sql + ' AND lote = @lote';
    END

    -- Define parameters for sp_executesql
    SET @parameters = N'@ref VARCHAR(18), @armazem NUMERIC(5,0), @lote VARCHAR(30), @data DATETIME';

	print @sql
    -- Execute dynamic SQL
    EXEC sp_executesql @sql, @parameters, @ref = @ref, @armazem = @armazem, @lote = @lote, @data = @data;
END
GO

GRANT EXECUTE ON up_stocks_InventarioStockArmazem TO Public;
-- Note: CONTROL permission was omitted for security reasons. Use it only if necessary.
GO
