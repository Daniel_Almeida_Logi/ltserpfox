/* SP Para validar a existência de Campos duplicados na BD ao inserir novos registos 

	exec up_utentes_camposduplicados 'ADM9B76ED39BE3343BF97478A', 15, 0, '', '', '' ,'', '', '214540700', '', 'Avenida da Liberdade, 4450-718 Leça da Palmeira', '19000101'

	exec up_utentes_camposduplicados 'ADMCA04FCF6-555A-4049-B29', 10442, 0, '444556666', '', ''
										 ,'', '', '', '', 'rua ao lado de cima', '19000101', ''
										 , '11038409'

 b_utentes

*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_camposduplicados]') IS NOT NULL
    drop procedure up_utentes_camposduplicados
go

create PROCEDURE up_utentes_camposduplicados

@utstamp			varchar(25)
,@no				numeric(10)
,@estab				numeric(3)
,@ncont				varchar(20)
,@nbenef			nvarchar(100)
,@nbenef2			nvarchar(100)
,@email				varchar(45)
,@tlmvl				varchar(13)
,@telefone			varchar(13)
,@id			    varchar(10)
,@morada			varchar(55)
,@dataNascimento	datetime
,@no_ext			varchar(20) = ''
,@bino				varchar(20) = ''
,@tipo				varchar(20) = ''


/* WITH ENCRYPTION */
AS
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosUtentesncont'))
		DROP TABLE #dadosUtentesncont
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosUtentesnbi'))
		DROP TABLE #dadosUtentesnbi
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosUtentestlmvl'))
		DROP TABLE #dadosUtentestlmvl
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosUtentestelefone'))
		DROP TABLE #dadosUtentestelefone
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosUtentesnbenef'))
		DROP TABLE #dadosUtentesnbenef
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosUtentesnbenef2'))
		DROP TABLE #dadosUtentesnbenef2
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosUtentesemail'))
		DROP TABLE #dadosUtentesemail
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosUtentesid'))
		DROP TABLE #dadosUtentesid
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosUtentesmorada'))
		DROP TABLE #dadosUtentesmorada
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosUtentesNo_Ext'))
		DROP TABLE #dadosUtentesNo_Ext	

		
		
	declare @invalido as bit, @texto as varchar(100), @aviso as bit, @permite_ncont_repetido bit, @ncont_obrigatorio bit, @datanascimento_obrigatoria bit, @telefone_obrigatorio bit, @morada_obrigatoria bit, @permite_nbi_repetido bit, @nbi_obrigatorio bit

	set @invalido = 0
	set @texto	  = ''
	set @aviso    = 0 
	set @permite_ncont_repetido = (select bool from B_Parameters (nolock) where stamp = 'ADM0000000239')
	set @ncont_obrigatorio = (select bool from B_Parameters (nolock) where stamp = 'ADM0000000046')
	set @telefone_obrigatorio = (select bool from B_Parameters (nolock) where stamp = 'ADM0000000022')
	set @datanascimento_obrigatoria = (select bool from B_Parameters (nolock) where stamp = 'ADM0000000023')
	set @morada_obrigatoria = (select bool from B_Parameters (nolock) where stamp = 'ADM0000000112')
	set @permite_nbi_repetido = (select bool from B_Parameters (nolock) where stamp = 'ADM0000000307')
	set @nbi_obrigatorio = (select bool from B_Parameters (nolock) where stamp = 'ADM0000000311')
	set @tipo = isnull(@tipo,'')
	print @permite_nbi_repetido

	/* Reps N Cont*/
	Select 
		count(ncont)		as ContNcont
	into
		#dadosUtentesncont
	from 
		b_utentes (nolock)
	where
		ncont		 = case when @ncont    = '' then ncont else @ncont END	
		AND utstamp  != @utstamp
		AND no		 != @no
		and removido = 0
		and inactivo = 0


	/* Reps N bi*/
	Select 
		count(bino)		as ContNbi
	into
		#dadosUtentesnbi
	from 
		b_utentes (nolock)
	where
		bino		 = case when @bino    = '' then bino else @bino END	
		AND utstamp  != @utstamp
		AND no		 != @no
		and removido = 0
		and inactivo = 0

	/* Reps N tlvml*/
	Select 
		count(tlmvl)		as ContTelemovel
	into
		#dadosUtentestlmvl
	from 
		b_utentes (nolock)
	where
		tlmvl	 = case when @tlmvl    = '' then tlmvl else @tlmvl END	
		AND utstamp  != @utstamp
		and removido = 0
		and inactivo = 0

	/* Reps N telefone*/
	Select 
		count(telefone)		as ContTelefone
	into
		#dadosUtentestelefone
	from 
		b_utentes (nolock)
	where
		telefone = case when @telefone = '' then telefone else @telefone END
		AND utstamp  != @utstamp
		and removido = 0
	
	/* Reps N Benef*/
	Select 
		count(nbenef)		as ContNBenef
	into
		#dadosUtentesnbenef
	from 
		b_utentes (nolock)
	where
		nbenef	 = case when @nbenef   = '' then nbenef else @nbenef END	
		AND utstamp  != @utstamp
		and removido = 0

	/* Reps N Benef2*/
	Select 
		count(nbenef2)		as ContNBenef2
	into
		#dadosUtentesnbenef2
	from 
		b_utentes (nolock)
	where
		nbenef2	 = case when @nbenef   = '' then nbenef else @nbenef END
		AND utstamp  != @utstamp
		and removido = 0	

	/* Reps N email*/
	Select 
		Count(email)		as ContEmail
	into
		#dadosUtentesemail
	from 
		b_utentes (nolock)
	where
		email	 = case when @email    = '' then email else @email END	
		AND utstamp  != @utstamp	
		and removido = 0

	/* Reps N id*/
	Select 
		Count(id)			as ContID
	into
		#dadosUtentesid
	from 
		b_utentes (nolock)
	where
		ID		 = case when @id	   = '' then id else @id END
		AND utstamp  != @utstamp	
		and removido = 0

	/* Reps Morada*/
	Select 
		Count(morada)		as ContMorada
	into
		#dadosUtentesmorada
	from 
		b_utentes (nolock)
	where
		morada	 = case when @morada   = '' then morada else @morada END
		AND utstamp  != @utstamp	
		and removido = 0

	/* Reps no_ext*/
	Select 
		Count(no_ext)	as Contno_ext
	into
		#dadosUtentesNo_Ext
	from 
		b_utentes (nolock)
	where
		no_ext	 = case when @no_ext   = '' then no_ext else @no_ext END
		AND utstamp  != @utstamp
		and removido = 0	

	/*Ncont Obrigatório - Validar a existência de Repetidos */
	IF  @ncont_obrigatorio = convert(bit,1) and @ncont = ''
	Begin
		set @texto = 'O Nº de Contribuinte é obrigatório na criação da ficha do Cliente. Por favor verifique.'
	end 

	IF  @nbi_obrigatorio = convert(bit,1) and @bino = ''
	Begin
		set @texto = 'O Nº de Identificação é obrigatório na criação da ficha do Cliente. Por favor verifique.'
	end 

	
	/* Telefone Obrigatório*/ 
	IF  @telefone_obrigatorio = convert(bit,1) and @telefone = '' and @tlmvl = ''
	Begin
		set @texto = 'O Nº de Telefone ou Telemovel é obrigatório na criação da ficha do Cliente. Por favor verifique.'
	end 

	/* Data Nascimento Obrigatória*/ 
	IF  @datanascimento_obrigatoria = convert(bit,1) and @dataNascimento = ' ' and @tipo!='Empresa' and  @tipo!='Empresas' and left(@ncont,1) != '5' and left(@ncont,1) != '6'
	Begin
		set @texto = 'A data de nascimento é obrigatória na criação da ficha do Cliente. Por favor verifique.'
	end 

	/* Morada Obrigatória*/ 
	IF  @morada_obrigatoria = convert(bit,1) and @morada = ''
	Begin
		set @texto = 'A morada é obrigatória na criação da ficha do Cliente. Por favor verifique.'
		set @aviso = 0
	end 

	/*Validar Nº de Contribuinte Repetido */
	IF @texto = '' and @permite_ncont_repetido =  convert(bit,1) and  @ncont != '' and exists (Select ContNcont from #dadosUtentesncont where ContNcont > 0 )
	Begin
		set @texto = 'Atenção: Já existe um Cliente com esse Nº de Contribuinte. O registo será gravado.'
		set @aviso = 1
	end 

	IF  @nbi_obrigatorio = convert(bit,1) and @bino = ''
	Begin
		set @texto = 'O Nº de Identificação é obrigatório na criação da ficha do Cliente. Por favor verifique.'
	end 

	/*Validar Nº de BI Repetido */
	IF @texto = '' and @permite_nbi_repetido =  convert(bit,0) and  @bino != '' and exists (Select ContNbi from #dadosUtentesnbi where ContNbi > 0 )
	Begin
		set @texto = 'Atenção: Já existe um Cliente com esse Nº de Identificação. Por favor verifique.'
		set @aviso = 0
	end 
	
	IF @texto = '' and @ncont != '' and exists (Select ContNcont from #dadosUtentesncont where ContNcont > 0 )
	Begin
		set @texto = 'Já existe um Cliente com esse Nº de Contribuinte. Por favor verifique.'
	end 	
	
	/* Telemovel Repetido */
	IF @texto = '' and @tlmvl != '' and exists (Select ContTelemovel from #dadosUtentestlmvl where ContTelemovel > 0)
	Begin
		set @texto = 'Atenção: Já existe um cliente com esse Nº de Telemóvel. O registo será gravado.'
		set @aviso = 1
	end 
	
	/* Telefone Repetido */
	IF @texto = '' and @telefone != '' and exists (Select ContTelefone from #dadosUtentestelefone where ContTelefone > 0)
	Begin
		set @texto = 'Atenção: Já existe um cliente com esse Nº de Telefone. O registo será gravado.'
		set @aviso = 1
	end 
	
	/* Nº Utente Repetido */
	IF @texto = '' and @nbenef != '' and exists (Select ContNBenef  from #dadosUtentesnbenef where ContNBenef > 0)
	Begin
		set @texto = 'Atenção: Já existe um cliente com esse Nº de Utente. O registo será gravado.'
		set @aviso = 1
	end 
	
	/* Nº Beneficiário Repetido */
	IF @texto = '' and @nbenef2 != '' and exists (Select ContNBenef2 from #dadosUtentesnbenef2 where ContNBenef2 > 0) 
	Begin
		set @texto = 'Atenção: Já existe um cliente com esse Nº de Beneficiário. O registo será gravado.'
		set @aviso = 1
	end 

	/* EMail Repetido */
	IF @texto = '' and @email != '' and exists (Select ContEmail from #dadosUtentesemail where ContEmail > 0)
	Begin
		set @texto = 'Atenção: Já existe um cliente com esse Email. O registo será gravado.'
		set @aviso = 1
	end 

	/* ID Repetido */
	IF @texto = '' and @id != '' and exists (Select ContID from #dadosUtentesid where ContID > 0)
	Begin
		set @texto = 'Atenção: Já existe um cliente com esse ID. O registo será gravado.'
		set @aviso = 1
	end 

	/* Morada Repetida */
	IF @texto = '' and @morada != '' and exists (select ContMorada from #dadosUtentesmorada where ContMorada > 0)
	begin
		set @texto = 'Atenção: Já existe um cliente com essa Morada. O registo será gravado.'
		set @aviso = 1 
	end

	/* Morada no_ext repetido */
	IF @texto = '' and @no_ext != '' and exists (select ContNo_ext from #dadosUtentesNo_Ext where ContNo_ext > 0)
	begin
		set @texto = 'Atenção: Já existe um cliente com esse Nº Externo. O registo será gravado.'
		set @aviso = 1 
	end


	select @texto as texto, @aviso as aviso


	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosUtentesncont'))
		DROP TABLE #dadosUtentesncont
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosUtentestlmvl'))
		DROP TABLE #dadosUtentestlmvl
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosUtentestelefone'))
		DROP TABLE #dadosUtentestelefone
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosUtentesnbenef'))
		DROP TABLE #dadosUtentesnbenef
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosUtentesnbenef2'))
		DROP TABLE #dadosUtentesnbenef2
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosUtentesemail'))
		DROP TABLE #dadosUtentesemail
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosUtentesid'))
		DROP TABLE #dadosUtentesid
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosUtentesmorada'))
		DROP TABLE #dadosUtentesmorada
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosUtentesNo_Ext'))
		DROP TABLE #dadosUtentesNo_Ext	

	
GO
Grant Execute On up_utentes_camposduplicados to Public
Grant Control On up_utentes_camposduplicados to Public
go
