-- Procurar Valor Texto nos Parâmetros
-- exec up_parameters_ProcuraTexto_site 'adm0000000114', 'Loja 1'



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



if OBJECT_ID('[dbo].[up_parameters_ProcuraTexto_site]') IS NOT NULL
	drop procedure dbo.up_parameters_ProcuraTexto_site
go

create procedure dbo.up_parameters_ProcuraTexto_site

@stamp varchar(30),
@site varchar(18)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select rtrim(ltrim(textValue)) as textValue from b_parameters_site (nolock) where stamp=@stamp and site=@site


GO
Grant Execute on dbo.up_parameters_ProcuraTexto_site to Public
Grant Control on dbo.up_parameters_ProcuraTexto_site to Public
GO