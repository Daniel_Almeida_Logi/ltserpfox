/* Lista de Entidades por data

	 exec up_receituario_listEntidadesPorData '2025','01', 'Loja 99'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_listEntidadesPorData]') IS NOT NULL
	drop procedure dbo.up_receituario_listEntidadesPorData
go

Create procedure dbo.up_receituario_listEntidadesPorData

@Ano		NUMERIC(4),
@Mes		NUMERIC(2),
@Site		VARCHAR(18) = ''


/* with encryption */
AS
SET NOCOUNT ON

select 'TODAS' as cptorgabrev

Union ALL

select 
	distinct cptorgabrev
from 
	ctltrct (nolock)
where
	ano=@ano and mes=@mes
	and site = case when @Site !='' then @site else site end

Go
Grant Execute on dbo.up_receituario_listEntidadesPorData to Public
Grant Control on dbo.up_receituario_listEntidadesPorData to Public
Go