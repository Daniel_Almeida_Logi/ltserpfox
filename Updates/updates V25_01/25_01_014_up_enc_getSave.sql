/* Encomenda Automática Guardada
	 exec up_enc_getSave 'ADM11032375953.315261825', 1, 1
	 select * from B_encAutoSave

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_enc_getSave]') IS NOT NULL
	drop procedure dbo.up_enc_getSave
go

create procedure dbo.up_enc_getSave

@stamp varchar(30)
,@sitenr numeric(5,0) = 1
,@manual INT = 0

/* with encryption */
AS

SET NOCOUNT ON

	DECLARE @sql VARCHAR(MAX) = ''

	SET @sql = N'

	Select
		pesquisa = CONVERT(bit,0)
		,BBfornec  = convert(numeric(9,2),0)
		,mbpcfornec = convert(numeric(9,2),0)
		,qtprevista = convert(numeric(9,2),0)
		,prioritario = CONVERT(bit,0)
		,stockGrupo		= (select sum(stt.stock) from st stt (nolock) where stt.ref = B_encAutoSave.ref)
		,B_encAutoSave.stamp
		,B_encAutoSave.data
		,B_encAutoSave.hora
		,B_encAutoSave.udata
		,B_encAutoSave.uhora
		,B_encAutoSave.tipoenc
		,B_encAutoSave.oUsr
		,B_encAutoSave.usr
		,B_encAutoSave.ststamp
		,B_encAutoSave.enc
		,B_encAutoSave.u_fonte
		,B_encAutoSave.ref
		,B_encAutoSave.refb
		,B_encAutoSave.qttb
		,B_encAutoSave.codigo
		,B_encAutoSave.design
		,B_encAutoSave.stock
		,B_encAutoSave.stmin
		,B_encAutoSave.stmax
		,B_encAutoSave.ptoenc
		,B_encAutoSave.eoq
		,B_encAutoSave.qtbonus
		,B_encAutoSave.qttadic
		,B_encAutoSave.qttfor
		,B_encAutoSave.qttacin
		,B_encAutoSave.qttcli
		,B_encAutoSave.fornecedor
		,B_encAutoSave.fornec
		,B_encAutoSave.fornestab
		,B_encAutoSave.sel
		,B_encAutoSave.epcusto
		,B_encAutoSave.epcpond
		,B_encAutoSave.epcult
		,B_encAutoSave.tabiva
		,B_encAutoSave.iva
		,B_encAutoSave.cpoc
		,B_encAutoSave.familia
		,B_encAutoSave.faminome
		,B_encAutoSave.u_lab
		,B_encAutoSave.conversao
		,CAST(B_encAutoSave.bonusFornec as varchar(20)) as bonusFornec
		,B_encAutoSave.esgotado
		,B_encAutoSave.ordem
		,B_encAutoSave.generico
		,B_encAutoSave.stockGH
		,B_encAutoSave.PCLFORNEC
		,B_encAutoSave.alertaStockGH
		,B_encAutoSave.alertaPCL
		,B_encAutoSave.alertaBonus
		,B_encAutoSave.stmax as ostmax
		,B_encAutoSave.ptoenc as optoenc
		,vv		= convert(bit,0)
		,qttres	= (select isnull(sum(bi.qtt - bi.qtt2),0) from bi (nolock) where bi.ndos=5 and fechada=0 and armazem=''' + LTRIM(@sitenr) + ''' and bi.ref=B_encAutoSave.ref)
		,m3um = (select isnull(round(sum(qtt)/3,0),0) from sl (nolock) where ref=B_encAutoSave.ref and datalc between getdate()-90 and getdate() and origem=''FT'' and armazem= ''' + LTRIM(@sitenr) + ''')
		,fornecabrev	= isnull(case when fl.nome2='''' then fl.nome else fl.nome2 end,'''')
		,B_encAutoSave.eoq as sug
		,pvp	= (select top 1 stt.epv1 from st stt (nolock) where stt.ref = B_encAutoSave.ref)
		,obs	 = (SELECT TOP 1 dbo.alltrimIsNull(stt.obs)    from st stt(nolock) where stt.ref = B_encAutoSave.ref)
		,u_nota1 = (SELECT TOP 1 dbo.alltrimIsNull(stt.u_nota1)from st stt(nolock) where stt.ref =  B_encAutoSave.ref) 
		,descontoForn = B_encAutoSave.desconto
		,descontoFornOri = B_encAutoSave.desconto
		'
		 + (CASE WHEN @manual = 1 THEN ',pcustoImp as pcusto' ELSE '' END) +
		'
	from
		B_encAutoSave (nolock)
		left join fl (nolock)	on B_encAutoSave.fornec=fl.no and B_encAutoSave.fornestab=fl.estab
	where
		stamp= ''' + @stamp + '''
	order by
		design
	'

	PRINT @sql
	EXEC (@sql)

GO
Grant Execute on dbo.up_enc_getSave to Public
Grant Control on dbo.up_enc_getSave to Public
GO