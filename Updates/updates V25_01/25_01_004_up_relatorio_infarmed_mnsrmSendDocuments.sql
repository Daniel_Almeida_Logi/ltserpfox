/* Relatório Comunicação MNSRM Infarmed

	exec up_relatorio_infarmed_mnsrmSendDocuments '20240101','20241231','Loja 1'
	
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_infarmed_mnsrmSendDocuments]') IS NOT NULL
	drop procedure dbo.up_relatorio_infarmed_mnsrmSendDocuments
go

create procedure dbo.up_relatorio_infarmed_mnsrmSendDocuments
@dataIni as datetime,
@dataFim as datetime,
@site as varchar(60)
/* with encryption */
AS
SET NOCOUNT ON


	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tablemnsrm'))
	DROP TABLE #tablemnsrm


	create table #tablemnsrm (
		ano numeric(5,0)
		,mes numeric(3,0)
		,Ref varchar(200)
		,qtt numeric(10,0)
		,PVP varchar(200)
	)


	insert #tablemnsrm (ano,mes ,ref,qtt,PVP)
	exec up_relatorio_infarmed_mnsrm @dataIni,@dataFim,@site

	select ano,mes ,'"'+ref + '"' as ref,qtt,'"'+PVP+ '"' as PVP from #tablemnsrm
GO
Grant Execute on dbo.up_relatorio_infarmed_mnsrmSendDocuments to Public
GO
Grant Control on dbo.up_relatorio_infarmed_mnsrmSendDocuments to Public
Go