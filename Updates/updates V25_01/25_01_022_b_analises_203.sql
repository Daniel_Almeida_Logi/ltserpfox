IF( NOT EXISTS (SELECT * FROM B_analises (NOLOCK) WHERE ordem=203))
	BEGIN
		INSERT INTO [dbo].[B_analises]
			([ordem],[grupo],[subgrupo],[descricao]
			,[report],[stamp],[url],[comentario]
			,[ecra],[stamp_analiseav],[estado]
			,[tabela],[perfil],[filtros],[formatoExp]
			,[comandoFox],[objectivo],[marcada])
		VALUES
			(203,'Gestão','Farmácia','Relatório Justificações Técnicas'
			,'relatorio_justificacoes_tecnicas','','','Obs.:'
			,'','',1
			,'','Relatório Justificações Técnicas','',''
			,'','',0)
	END


IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=203 and label='Data Início'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 203,[nome],[label],[tipo],[comando],1,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 202 and label='Data Início'
END

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=203 and label='Data Fim'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 203,[nome],[label],[tipo],[comando],2,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 202 and label='Data Fim'
END

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=203 and label='Loja'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 203,[nome],[label],[tipo],[comando],3,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],0,[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 202 and label='Loja'
END

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=203 and label='Operador'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 203,[nome],[label],[tipo],[comando],4,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],0,[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 200 and label='Operador'
END

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=203 and label='Código Justificação')) 
BEGIN
INSERT INTO [dbo].[b_analises_config]
           ([ordem]
           ,[nome]
           ,[label]
           ,[tipo]
           ,[comando]
           ,[fordem]
           ,[vdefault]
           ,[vdefaultsql]
           ,[vdefaultfox]
           ,[colunas]
           ,[colunasWidth]
           ,[obrigatorio]
           ,[firstempty]
           ,[mascara]
           ,[formato]
           ,[multiselecao]
           ,[comandoesql]
           ,[visivel])
     VALUES
           (203
           ,'codJustificacao' 
           ,'Código Justificação'
           ,'vc'
           ,'select sel =  CONVERT(BIT,0),convert(varchar(10), code) as codJustif , descr as justificacao from panel_exceptions(nolock) where type=''JUSTIFICAODEM'' order by code asc '
           ,5
           ,''
           ,0
           ,0
           ,'codJustif, justificacao'
           ,''
           ,0
           ,0
           ,''
           ,''
           ,1
           ,1
           ,1)
END