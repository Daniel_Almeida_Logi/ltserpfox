delete from  cptvalBonus where tipo = 2
insert into cptvalBonus
select id_cpt_grp, id_cpt_pla, 2, 'Lei 46/2020, de 20/08 - EAC',95,50,'RMP',getdate(),0,0 from cptvalBonus where tipo = 1

delete from  cptvalBonus where tipo = 3
insert into cptvalBonus
select id_cpt_grp, id_cpt_pla, 3, 'Lei 46/2020, de 20/08 -EAC ',98,50,'RMP',getdate(),0,0 from cptvalBonus where tipo = 1

update cptvalBonus set compart = 0  where tipo = 2
update cptvalBonus set compart = 0  where tipo = 3

update cptval set u_p11 = 90, usrdata=getdate()  where u_p11=69  

update cptval set  u_g24 = 'ref', u_p24 = 90, usrdata=getdate()  where u_p11=90  
and u_g11 = 'ref'


delete from dplms where dplmsstamp='96'
if not exists (select dplmsstamp from dplms where dplmsstamp='96')     begin      insert into dplms (       dplmsstamp, diploma, inactivo, ousrdata, usrdata, u_coluna, u_design, diploma_id)      Values(       '96','despacho n.º 10910/2009, de 2024',0,'20241223'       ,'20241223',24,'10910/2024-Procriacao',96)     end else begin      update dplms set       dplmsstamp='96',diploma='despacho n.º 10910/2024, de 2024',inactivo=0,ousrdata='20241223'       ,usrdata='20241223',u_coluna=24,u_design='10910/2009-Procriacao',diploma_id=96      where dplmsstamp = '96'     end

update cptval set pct='16.63' where grupo = 'PC' and cptplacode='P01'
update cptval set pct='16.63' where grupo = 'PC' and cptplacode='P02'


DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'excecoes'
	,@columnName		= 'bonusId'
	,@columnType		= 'varchar (100)'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go


DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'excecoes'
	,@columnName		= 'recmdId'
	,@columnType		= 'varchar (100)'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go
    
	delete from [excecoes] where bonusId!=''
	delete from [excecoes] where [recmdId]!=''
	
	INSERT INTO [dbo].[excecoes]
           ([stamp]
           ,[cnp]
           ,[cnpem]
           ,[nome]
           ,[tipo]
           ,[descricao]
           ,[nivel]
           ,[ousrdata]
		   ,[bonusId]
		   ,[recmdId]
		   )    
		Values
		( NEWID (), '','','',1,'Execeção Bonus BAS',5,GETDATE(),'1',''),
		( NEWID (), '','','',1,'Execeção Bonus EAC',5,GETDATE(),'2',''),
		( NEWID (), '','','',1,'Execeção Bonus EAC',5,GETDATE(),'3',''),
		( NEWID (), '','','',1,'Execeção Bonus BAS',5,GETDATE(),'99',''),
		( NEWID (), '','','',1,'Execeção Bonus EAC',5,GETDATE(),'95',''),
		( NEWID (), '','','',1,'Execeção Bonus EAC',5,GETDATE(),'98',''),
		( NEWID (), '','','',1,'Execeção Estatuto Combatente',5,GETDATE(),'','5003'),
		( NEWID (), '','','',1,'Execeção Estatuto Combatente',5,GETDATE(),'','5004')



		
delete from   excecoes where diploma_cod=95
insert into excecoes
values('01520892-4555-4145-BAF2-D3FBC7F27521','','','',1,'Execeção EAC - Aposentados',5,getdate(),95,'','')

delete from   excecoes where diploma_cod=98
insert into excecoes
values('01520892-4555-4145-BAF2-D3FBC7F27522','','','',1,'Execeção EAC - Não Aposentados',5,getdate(),98,'','')

