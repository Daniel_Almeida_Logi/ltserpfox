/*
	
	Daniel Almeida
	Criado: 2023-03-16

	Usa o resultado da DEM para comparticipar receitas


	use ltdev30
	EXEC up_calc_compart_dem '101100000772269010011', 6.8600, '09010B92C502A3DBE06390E2CA0A1004', 1, 9.030000, 0   
	    
	        
*/





IF OBJECT_ID('[dbo].[up_calc_compart_dem]') IS NOT NULL
    DROP procedure dbo.up_calc_compart_dem 
GO

CREATE procedure dbo.up_calc_compart_dem
	@id varchar(100),
	@compLts NUMERIC(16,2),
	@token varchar(100),
	@ecra int =  0,
	@pvp  NUMERIC(16,2) =  0,
	@pref NUMERIC(16,2) =  0
	
AS
	declare @perFinal NUMERIC(16,2) = 0
	declare @compFinal NUMERIC(16,2) = -1
	declare @pvpFinal  NUMERIC(16,2) = -1

	-- se front office ou vazio
	-- mantem a comparticipacao
	-- 0 - não sei
	-- 1 - atendimento
	-- 2 - backoffice
	--if(@ecra!=2)
	--begin
	--	select percentagem = @percInitial
	--	return
	--end



	select 	
		@perFinal = case when retorno_comp_sns > 0 then  round(dd.retorno_comp_sns/dd.retorno_pvp,4) * 100 else  0  end,
		@compFinal = dd.retorno_comp_sns,
		@pvpFinal  = isnull(dd.retorno_pvp,0)
	from Dispensa_Eletronica_dd(nolock)  dd
	inner join Dispensa_Eletronica(nolock) on Dispensa_Eletronica.token = dd.token
	where 
		dd.id = @id 
		and dd.token = @token 
		and Dispensa_Eletronica.token = @token
		and dd.id != ''
		and dd.token != ''
		and isnull(dd.retorno_info_prestacao,'') != ''
	order by Dispensa_Eletronica.data_cre desc



	set @perFinal = isnull(@perFinal,0)
	set @compFinal = isnull(@compFinal,@compLts)

	if(@compFinal<0)
		set @compFinal = @compLts


	
	if(@compFinal>@pvpFinal and @compFinal>0 and @pvpFinal>0)
		set @compFinal = @pvpFinal

	

	-- Recalcula a compart do logitools com base no estado
 

	if(@perFinal>0)
	begin
	
		update
			Dispensa_Eletronica_dd  
		set comp_sns= @compFinal,
			comp_sns_tx=@perFinal 
		where 
			Dispensa_Eletronica_dd.id = @id 
			and Dispensa_Eletronica_dd.token = @token 
			and Dispensa_Eletronica_dd.id != ''
			and Dispensa_Eletronica_dd.token != ''
			and isnull(retorno_info_prestacao,'') != ''
					
	end


		

	select percentagem = @perFinal, valorFinal = @compFinal

GO
GRANT EXECUTE on dbo.up_calc_compart_dem  TO PUBLIC
GRANT Control on dbo.up_calc_compart_dem  TO PUBLIC
GO



