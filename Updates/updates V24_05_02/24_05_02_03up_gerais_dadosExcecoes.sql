/* SP que carrega Dados das excepcoes 

	exec up_gerais_dadosExcecoes 1,'', '','14'
	** adicionar excepcoes

	if(select count(*) from excecoes(nolock)) =0 
	begin
	
	INSERT INTO [dbo].[excecoes]
           ([stamp]
           ,[cnp]
           ,[cnpem]
           ,[nome]
           ,[tipo]
           ,[descricao]
           ,[nivel]
           ,[ousrdata]
		   ,[bonusId]
		   ,[recmdId]
		   )    
		Values
		( NEWID (), '2454684','','',2,'Execeção vacina gripe',5,GETDATE(),'',''),
		( NEWID ( ), '8650309','','',2,'Execeção vacina gripe',5,GETDATE(),'',''),
		( NEWID ( ), '8567404','','',2,'Execeção vacina gripe',5,GETDATE(),'',''),
		( NEWID ( ), '','50139215','',2,'Execeção vacina gripe',5,GETDATE(),'','')

		
	end


	select * from excecoes
			
*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_dadosExcecoes]') IS NOT NULL
	drop procedure dbo.up_gerais_dadosExcecoes
go

CREATE PROCEDURE dbo.up_gerais_dadosExcecoes

@tipo			INT, 
@cnp			VARCHAR(100),
@cnpem			VARCHAR(100),
@diploma_cod	VARCHAR(100),
@idBonus	    VARCHAR(100) = '',
@recmId	        VARCHAR(254) = ''


/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

Declare @noExcecoes as numeric(2,0) = 0   



if(@diploma_cod !="")
begin	
	select
		@noExcecoes = count(*) 
	from
		excecoes (nolock)
	 where
		tipo = @tipo and diploma_cod = @diploma_cod 
	
	 
	if @noExcecoes > 0
	begin 
		select @noExcecoes as "no"
		return
	end 
end

/* pesquisa por bonusId */
if(@idBonus !="")
begin	
	select
		@noExcecoes = count(*) 
	from
		excecoes (nolock)
	 where
		tipo = @tipo and bonusId = @idBonus 


	
	if @noExcecoes > 0
	begin 
		select @noExcecoes as "no"
		return
	end 
end

/* pesquisa por recm beneficiario */
if(@recmId !="")
begin	
	
	select
		@noExcecoes = count(*) 
	from
		excecoes (nolock)
	 where
		tipo = @tipo AND recmdId IN (SELECT Value FROM dbo.SplitString(@recmId, ','));

	
	if @noExcecoes > 0
	begin 
		select @noExcecoes as "no"
		return
	end 
end





/* pesquisa por cnpem */
if(@cnpem !="")
begin	
	select
		@noExcecoes = count(*) 
	from
		excecoes (nolock)
	 where
		tipo = @tipo and cnpem = @cnpem 


	
	if @noExcecoes > 0
	begin 
		select @noExcecoes as "no"
		return
	end 
end

/* pesquisa por cnp */
if(@cnp!="")
begin	
	select
		@noExcecoes = count(*) 
	from
		excecoes (nolock)
	 where
		tipo =  @tipo  and cnp = @cnp 


	
	if @noExcecoes > 0
	begin 
		select @noExcecoes as "no"
		return
	end 

end


/* RETURN NO EXCEÇÕES A 0*/
Select @noExcecoes as "no" 

GO
Grant Execute on dbo.up_gerais_dadosExcecoes to Public
Grant Control on dbo.up_gerais_dadosExcecoes to Public
GO
