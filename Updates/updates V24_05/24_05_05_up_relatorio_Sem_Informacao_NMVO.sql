/* Relatório com registo de inativação e ativação (nas devoluções dos códigos)

	exec up_relatorio_Sem_Informacao_NMVO '20240830','20280930','Loja 1',''
	exec up_relatorio_Sem_Informacao_NMVO '19000101','20280908','Loja 1','56'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_Sem_Informacao_NMVO]') IS NOT NULL
	DROP PROCEDURE dbo.up_relatorio_Sem_Informacao_NMVO
GO

CREATE PROCEDURE dbo.up_relatorio_Sem_Informacao_NMVO
@dataIni		AS DATETIME,
@dataFim		AS DATETIME,
@site			AS VARCHAR(55),
@op				AS VARCHAR(254)
/* with encryption */
AS 
SET NOCOUNT ON
BEGIN 
	;WITH 
		cteFix AS (
		SELECT 
			ofistamp
			,fistamp
			,qtt
			,fi.fno
		FROM 
			fi(NOLOCK)
	)
	SELECT DISTINCT
		CAST(CAST(ft.ousrdata AS DATE) AS DATETIME) + CAST(ft.ousrhora AS DATETIME) AS ousrdata									
		,fi.ousrinis
		,fi.nmdoc
		,fi.fno
		,fi.ref
		,fi.design							
		,fi.qtt											
	FROM 
		ft(NOLOCK)
	INNER JOIN
		fi(NOLOCK) ON fi.ftstamp = ft.ftstamp
	INNER JOIN
		fprod(NOLOCK) ON fprod.ref = fi.ref
	FULL OUTER JOIN
		fi_trans_info(NOLOCK) ON  fi_trans_info.recStamp = fi.fistamp
	INNER JOIN 
		td (NOLOCK)  ON td.ndoc=ft.ndoc
	LEFT JOIN 
		cteFix fix (NOLOCK) ON fi.fistamp = fix.ofistamp 
	WHERE
		CONVERT(DATE,ft.ousrdata) BETWEEN @dataIni AND @dataFim 
		AND ft.site = @site
		AND (ft.Ousrinis in (select items from dbo.up_splitToTable(@op,','))or @op= '' or @op= '0')
		AND fprod.dispositivo_seguranca = 1
		AND fi.nmdoc != ''
		AND (fi.qtt-isnull(fix.qtt,0)) != 0
		AND td.cmsl != 0
		AND  (fi.fistamp IS NULL OR fi_trans_info.recStamp IS NULL)	
	ORDER BY
		CAST(CAST(ft.ousrdata AS DATE) AS DATETIME) + CAST(ft.ousrhora AS DATETIME)  ASC

END
GO
Grant Execute on dbo.up_relatorio_Sem_Informacao_NMVO to Public
Grant control on dbo.up_relatorio_Sem_Informacao_NMVO to Public
GO