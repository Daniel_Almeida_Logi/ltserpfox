/****** 
	Object:  get simposium Response
	exec [dbo].[up_save_simposiumResp]  
***/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

-- Se a procedure já existir, remove
IF OBJECT_ID('[dbo].[up_save_simposiumResp]') IS NOT NULL
    DROP PROCEDURE dbo.[up_save_simposiumResp]
GO

-- Cria a procedure
CREATE PROCEDURE dbo.[up_save_simposiumResp]
    @token                AS VARCHAR(36),
    @status               AS INT,
    @message              AS VARCHAR(254),
    @additionalProp1      AS VARCHAR(254),
    @additionalProp2      AS VARCHAR(254),
    @additionalProp3      AS VARCHAR(254),
    @tokenConflicts       AS VARCHAR(36),
    @stampDrugInfo        AS VARCHAR(36),
    @tokenInteraction     AS VARCHAR(36),
    @tokenFoodInteraction AS VARCHAR(36),
    @tokenPhysioAlert     AS VARCHAR(36),
    @ousrinis             AS VARCHAR(30),
    @usrinis              AS VARCHAR(30)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON;

-- Verifica se o registro já existe na tabela pelo token
IF EXISTS (SELECT 1 FROM dbo.simposiumResp WHERE token = @token)
BEGIN
    -- Faz o UPDATE se o token já existir
    UPDATE dbo.simposiumResp
    SET 
        status = @status,
        message = @message,
        additionalProp1 = @additionalProp1,
        additionalProp2 = @additionalProp2,
        additionalProp3 = @additionalProp3,
        tokenConflicts = @tokenConflicts,
        stampDrugInfo = @stampDrugInfo,
        tokenInteraction = @tokenInteraction,
        tokenFoodInteraction = @tokenFoodInteraction,
        tokenPhysioAlert = @tokenPhysioAlert,
        usrdata = GETDATE(),
        usrinis = @usrinis
    WHERE token = @token;
END
ELSE
BEGIN
    -- Faz o INSERT se o token não existir
    INSERT INTO dbo.simposiumResp (
        token, status, message, additionalProp1, additionalProp2, additionalProp3,
        tokenConflicts, stampDrugInfo, tokenInteraction, tokenFoodInteraction,
        tokenPhysioAlert, ousrinis, ousrdata, usrinis, usrdata
    )
    VALUES (
        @token, @status, @message, @additionalProp1, @additionalProp2, @additionalProp3,
        @tokenConflicts, @stampDrugInfo, @tokenInteraction, @tokenFoodInteraction,
        @tokenPhysioAlert, @ousrinis, GETDATE(), @usrinis, GETDATE()
    );
END;
GO

-- Concede permissões de execução
GO
Grant Execute On dbo.[up_save_simposiumResp] to Public
Grant Control On dbo.[up_save_simposiumResp] to Public
GO

