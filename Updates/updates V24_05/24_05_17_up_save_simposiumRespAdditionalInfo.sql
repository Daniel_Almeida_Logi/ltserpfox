/****** 
	Object:  up_save_simposiumRespAdditionalInfo
	exec [dbo].[up_save_simposiumRespAdditionalInfo]  
***/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_save_simposiumRespAdditionalInfo]') IS NOT NULL
    DROP PROCEDURE dbo.[up_save_simposiumRespAdditionalInfo]
GO

CREATE PROCEDURE dbo.[up_save_simposiumRespAdditionalInfo]
    @stamp           VARCHAR(36),
    @token           VARCHAR(36),
    @image           VARCHAR(MAX),
    @descr           VARCHAR(MAX),
    @url             VARCHAR(254),
    @ousrinis        VARCHAR(30),
    @usrinis         VARCHAR(30)
AS
BEGIN
    SET NOCOUNT ON;

    IF EXISTS (SELECT 1 FROM dbo.[simposiumRespAdditionalInfo] WHERE [stamp] = @stamp)
    BEGIN
        -- Atualiza o registro existente
        UPDATE dbo.[simposiumRespAdditionalInfo]
        SET 
            [token] = @token,
            [image] = @image,
            [descr] = @descr,
            [url] = @url,
            [usrinis] = @usrinis,
            [usrdata] = GETDATE()
        WHERE 
            [stamp] = @stamp;
    END
    ELSE
    BEGIN
        -- Insere um novo registro
        INSERT INTO dbo.[simposiumRespAdditionalInfo] (
            [stamp], [token], [image], [descr], [url], [ousrinis], [ousrdata], [usrinis], [usrdata]
        )
        VALUES (
            @stamp, @token, @image, @descr, @url, @ousrinis, GETDATE(), @usrinis, GETDATE()
        );
    END
END

GO
Grant Execute On dbo.[up_save_simposiumRespAdditionalInfo] to Public
Grant Control On dbo.[up_save_simposiumRespAdditionalInfo] to Public
GO
