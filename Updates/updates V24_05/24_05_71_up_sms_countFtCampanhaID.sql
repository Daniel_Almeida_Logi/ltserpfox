/*
	Get última campanha ID facturada
	
	exec up_sms_countFtCampanhaID 1
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_countFtCampanhaID]') IS NOT NULL
	drop procedure dbo.up_sms_countFtCampanhaID
go

create procedure dbo.up_sms_countFtCampanhaID
	@campanhaID varchar(254)
	
/* WITH ENCRYPTION */ 
AS

	select
		COUNT(stamp) [count]
		,[status]
	from
		b_sms_fact
	where
		campaignId = @campanhaID
	group by
		[status]

GO
Grant Execute on dbo.up_sms_countFtCampanhaID to Public
Grant Control on dbo.up_sms_countFtCampanhaID to Public
GO