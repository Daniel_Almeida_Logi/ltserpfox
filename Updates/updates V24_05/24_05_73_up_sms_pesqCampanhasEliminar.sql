-- Pesquisa listas contactos para eliminar
-- exec up_sms_pesqCampanhasEliminar
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_pesqCampanhasEliminar]') IS NOT NULL
    drop procedure dbo.up_sms_pesqCampanhasEliminar
go

create procedure dbo.up_sms_pesqCampanhasEliminar

/* WITH ENCRYPTION */
AS

select * 
from b_sms (nolock) 
where dataenvio < GETDATE() 
	and apagado = 0 
	and emitido = 1

GO
Grant Execute on dbo.up_sms_pesqCampanhasEliminar to Public
Grant Control on dbo.up_sms_pesqCampanhasEliminar to Public
go