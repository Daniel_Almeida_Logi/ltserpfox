
/****** Object:  Trigger [dbo].[Tr_Fi_Insert]    Script Date: 02/11/2023 12:47:51 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER TRIGGER [dbo].[Tr_Fi_Insert] 
ON [dbo].[fi] FOR INSERT  
AS 
IF (@@ROWCOUNT = 0)
	RETURN;
SET NOCOUNT ON
-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Fi_Insert'

set @Msg = '<' + convert(varchar,getdate(),14) + '>'
/*
	Insert SL
*/

--update ft set numLinhas = (select COUNT(*) FROM fi(nolock) where fi.ftstamp = ft.ftstamp) FROM  ft(nolock) inner join inserted as i ON i.ftstamp = ft.ftstamp

IF EXISTS (SELECT i.fistamp 
			FROM inserted i
				INNER JOIN ft (nolock) on ft.ftstamp=i.ftstamp 
				INNER JOIN td (nolock) on i.ndoc=td.ndoc 
			WHERE td.lancasl=1 and Ltrim(rtrim(i.ref)) != ''
		)
BEGIN
	INSERT INTO SL 
		(
		slstamp
		,fistamp
		,stns
		,usrinis
		,usrhora
		,usrdata
		,ousrinis
		,ousrhora
		,ousrdata
		,origem
		,fref
		,ccusto
		,ncusto
		,nome
		,segmento
		,codigo
		,moeda
		,datalc
		,design
		,ref
		,lote
		,armazem
		,qtt
		--,tt
		--,vu
		,ett
		,evu
		,num1
		,frcl
		,adoc
		,cm
		,cmdesc
		,composto
		,cor
		,tam
		,usr1
		,usr2
		,usr3
		,usr4
		,usr5
		,vumoeda
		,ttmoeda
		,pcpond
		,cpoc
		,valiva
		,evaliva
		,epcpond 
		)
	SELECT 
		inserted.fistamp
		,inserted.fistamp
		,inserted.stns
		,inserted.usrinis
		,inserted.usrhora
		,inserted.usrdata
		,inserted.ousrinis
		,inserted.ousrhora
		,inserted.ousrdata
		,'FT'
		,CASE WHEN td.lifref=1 THEN inserted.fifref ELSE ft.fref END,CASE WHEN td.liccusto=1 THEN inserted.ficcusto ELSE ft.ccusto END
		,CASE WHEN td.lincusto=1 THEN inserted.fincusto ELSE ft.ncusto END
		,ft.nome
		,ft.segmento
		,inserted.codigo
		,ft.moeda
		,ft.fdata
		,inserted.design
		,inserted.ref
		,inserted.lote
		,inserted.armazem
		,CASE WHEN ft.tipodoc=3 THEN inserted.qtt*-1 ELSE inserted.qtt END
		,inserted.esltt
		,inserted.eslvu
		,inserted.num1
		,ft.no
		,convert(char (10), ft.fno)
		,td.cmsl
		,td.cmsln
		,inserted.composto
		,inserted.cor
		,inserted.tam
		,inserted.usr1
		,inserted.usr2
		,inserted.usr3
		,inserted.usr4
		,inserted.usr5
		,inserted.slvumoeda
		,inserted.slttmoeda
		,inserted.pcp
		,inserted.cpoc
		,CASE WHEN inserted.iectin<>0 THEN inserted.pv-inserted.vbase ELSE CASE WHEN inserted.ivaincl=1 THEN inserted.tiliquido-inserted.tiliquido/(1+inserted.iva/100) ELSE inserted.tiliquido*inserted.iva/100 END END
		,CASE WHEN inserted.eiectin<>0 THEN inserted.epv-inserted.evbase ELSE CASE WHEN inserted.ivaincl=1 THEN inserted.etiliquido-inserted.etiliquido/(1+inserted.iva/100) ELSE inserted.etiliquido*inserted.iva/100 END END
		,inserted.epcp 
	FROM
		inserted
		INNER JOIN ft (nolock) on ft.ftstamp=inserted.ftstamp
		INNER JOIN td (nolock) on inserted.ndoc=td.ndoc
	WHERE
		td.lancasl = 1
		AND Ltrim(rtrim(inserted.ref)) != ''
	if @@ROWCOUNT > 0
		SET @Msg += '<Insert SL (' + convert(varchar,getdate(),8) + ')>'




END
/*
	update BI (origem)
*/
IF EXISTS (SELECT * FROM inserted WHERE inserted.bistamp != '')
BEGIN
	set Context_Info 0x55555 -- evitar que o trigger de update corra validações desnecessárias
	UPDATE
		BI
	SET
		bi.fdata = ft.fdata
		,bi.fno = ft.fno
		,bi.adoc = convert(char(10),ft.fno)
		,bi.nmdoc = ft.nmdoc
		,bi.oftstamp = ft.ftstamp
		,bi.ndoc = ft.ndoc
		,bi.qtt2 = bi.qtt2 + (SELECT SUM(i.qtt*(CASE WHEN i.tipodoc=3 THEN -1 ELSE 1 END)) FROM inserted i WHERE i.bistamp = bi.bistamp)
		,bi.pbruto = bi.pbruto + (SELECT SUM(i.qtt*(CASE WHEN i.tipodoc=3 THEN -1 ELSE 1 END)) FROM inserted i WHERE i.bistamp = bi.bistamp)
		,bi.usrdata = getdate()
		,bi.usrhora = convert(varchar,getdate(),8)
	FROM
		inserted
		INNER JOIN ft (nolock) ON inserted.ftstamp=ft.ftstamp 
	WHERE 
		inserted.bistamp != ''
		and inserted.bistamp = bi.bistamp 
	if @@ROWCOUNT > 0
		SET @Msg += '<Update BI (' + convert(varchar,getdate(),8) + ')>'
	If OBJECT_ID('tempdb.dbo.#bostamp') IS NOT NULL
			drop table #bostamp;
	select distinct bostamp into #bostamp from bi (nolock) where bistamp in (select bistamp from inserted WHERE inserted.bistamp != '')
	IF EXISTS (SELECT * FROM #bostamp WHERE #bostamp.bostamp != '')
	BEGIN
		update bo set usrdata = getdate(),	usrhora = convert(varchar,getdate(),8) where bostamp in (select bostamp from #bostamp)
	end 
	set Context_Info 0x0
	/* 
		Marcações:
		Marca registos nas marcações a dizer que já foi pago
	*/
	UPDATE 
		marcacoesServ 
	SET 
		marcacoesServ.ufistamp = inserted.fistamp
		,fu = case when td.u_tipodoc = 1 then fu else 1 end
		,fe = case when td.u_tipodoc = 1 then 1 else fe end
	FROM
		marcacoesServ
		INNER JOIN inserted ON inserted.bistamp = marcacoesServ.servmrstamp 
		INNER JOIN td (nolock) on inserted.ndoc = td.ndoc
	WHERE 
		inserted.bistamp != ''
	if @@ROWCOUNT > 0
		SET @Msg += '<Update BI (' + convert(varchar,getdate(),8) + ')>'
END 
-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg);
