/* SP Pesquisa de artigos [CUIDADO COM ALTERAÇÕES, ANALISAR SEMPRE A PERFORMANCE DA QUERY]

	exec up_stocks_pesquisa 5000,'','','','','', 0,'maior',0, -1, 0, 1, 0, 0,'', 1, 0, 1, 0, -1, '', 90, '', '', 0,''
	exec up_stocks_pesquisa 30,'','','','','',0,'maior',-999999,-1,0,1,0,0,'',1,0,1,0,-1,'',0,'','','','','venda',''

	exec up_stocks_pesquisa_comecado 30,'','','','','',0,'maior',-999999,-1,0,1,0,0,'',1,0,0,0,-1,'',0,'MSRM','',0,'' 

	exec up_stocks_pesquisa_comecado 30,'aspirin','','','','',0,'maior',-999999,-1,0,1,0,0,'',1,0,1,0,-1,'',0,'','',0,''
	exec up_stocks_pesquisa_comecado 30,'','','','','',0,'maior',-999999,-1,0,1,0,0,'',1,0,0,0,-1,'',0,'','',0,'','Sem Localização'

	exec up_stocks_pesquisa_comecado 90,'','Alimentação Especial','','','',0,'maior',-999999,-1,0,1,0,0,'',1,0,1,0,-1,'',0,'','',0,'',''

	exec up_stocks_pesquisa_comecado 90,'5440987','','','','',0,'maior',-999999,-1,0,1,0,0,'',1,0,1,0,-1,'',0,'','',0,'',''
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_stocks_pesquisa_comecado]') IS NOT NULL
    DROP procedure dbo.up_stocks_pesquisa_comecado
GO

CREATE procedure dbo.up_stocks_pesquisa_comecado
	@top				INT
	,@artigo			VARCHAR(60)
	,@familia			VARCHAR(60)
	,@lab				VARCHAR(120)
	,@marca				VARCHAR(200)
	,@dci				VARCHAR(120)
	,@servicos			int = 0
	,@operadorLogico	VARCHAR(5)
	,@stock				NUMERIC(30,3)
	,@generico			int 
	,@inactivo			bit
	,@armazem			numeric(10)
	,@servMarc			bit
	,@atendimento		bit = 0
	,@recurso			VARCHAR(120) = ''
	,@site_nr			tinyint
	,@parafarmacia		bit 
	,@comFicha			bit = 0
	,@t5				bit = 0
	,@txIva				numeric(15,2) = -1
	,@fornecedor		varchar(80) = ''
	,@diassmov			int = 0
	,@class2			varchar(max) = ''
	,@tipoProduto		varchar(254) 
	,@t4				bit = 0
	,@fornStamp			varchar(25) = ''
	,@localizacao		varchar(254) = ''
	
/* WITH ENCRYPTION */
AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosArmazens'))
		DROP TABLE #dadosArmazens	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt2'))
		DROP TABLE #dadosSt2	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#localizacoes'))
		DROP TABLE #localizacoes

	create table #localizacoes (local varchar(254),local2 varchar(254),local3 varchar(254))

	IF(@localizacao = '')
		begin
			insert #localizacoes (local)
			select descr from locais(nolock) union select ''  as descr
		end 
	else IF @localizacao = 'Sem Localização'
		begin 
			insert into #localizacoes (local)
			values ('')
		end
	else
		begin
			insert #localizacoes (local)
			select items from dbo.up_splitToTable(@localizacao,',')
		end
	
	/* variaveis auxiliares */		
	declare @familiaNo as varchar(18)
	set @familiaNo = isnull((Select top 1 stfami.ref from stfami (nolock) where stfami.nome = @familia),'')
				
	Declare @refExcluir varchar(200)
	if @atendimento = 1 and (select top 1 bool from B_Parameters where stamp='ADM0000000219') = 1
		set @refExcluir = (select top 1 rtrim(ltrim(textValue)) from B_Parameters where stamp = 'ADM0000000219')
	else
		set @refExcluir = char(39)+'vazio'+CHAR(39)
	-- tipo produto
	declare @tipoProdutoID as varchar(4)
	set @tipoProdutoID = (select top 1 famstamp from b_famFamilias (nolock) where upper(rtrim(ltrim(design))) = upper(@tipoProduto))

	declare @stkcaixa as bit
	set @stkcaixa = (select bool from B_Parameters_site(nolock) where stamp='ADM0000000067' and site=(select site from empresa where no=@site_nr))

	declare @stcativoretira as bit
	set @stcativoretira = (select bool from B_Parameters_site(nolock) where stamp='ADM0000000098' and site=(select site from empresa where no=@site_nr))

	Declare @departamento varchar(254)
	DECLARE @departamentoID varchar(15)
	set @departamento =  (select items from dbo.up_SplitToTableCId(@class2,';') where id = 1)
	SET	 @departamentoID = ISNULL((select top 1 id from grande_mercado_hmr (nolock) where upper(rtrim(ltrim(descr))) = upper(ltrim(rtrim(@departamento)))),'')
	
	Declare @seccao varchar(254)
	DECLARE @seccaoID varchar(15)
	set @seccao =  (select items from dbo.up_SplitToTableCId(@class2,';') where id = 2)
	SET @seccaoID = ISNULL((select top 1 id from mercado_hmr (nolock) where upper(rtrim(ltrim(descr))) = upper(ltrim(rtrim(@seccao)))),'')
	
	Declare @categoria varchar(254)
	DECLARE @categoriaID varchar(15)
	set @categoria =   (select items from dbo.up_SplitToTableCId(@class2,';') where id = 3)
	SET @categoriaID = ISNULL((select top 1 id from categoria_hmr (nolock) where upper(rtrim(ltrim(descr))) = upper(ltrim(rtrim(@categoria)))),'')
	
	Declare @segmento varchar(254)
	DECLARE @segmentoID varchar(15)
	set @segmento =   (select items from dbo.up_SplitToTableCId(@class2,';') where id = 4)
	SET @segmentoID = ISNULL((select top 1 id from segmento_hmr (nolock) where upper(rtrim(ltrim(descr))) = upper(ltrim(rtrim(@segmento)))),'')	

	DECLARE @var_ordenacao VARCHAR(254)
	SET @var_ordenacao = (SELECT textValue FROM B_Parameters_site(NOLOCK) WHERE stamp='ADM0000000108' 
							AND site = (SELECT site FROM empresa(NOLOCK) WHERE no = @site_nr))

	DECLARE @ePemH bit = 0
	IF LEFT(@artigo, 5) = 'PEMH-'
	begin
		set @ePemH = 1
		SET @artigo = SUBSTRING(@artigo, 6, LEN(@artigo) - 5)
	end

	/* Armazens disponiveis por empresa */
	select 
		empresa_no
	into
		#dadosArmazens
	from
		empresa_arm (nolock)
	inner join 
		empresa on empresa_arm.empresa_no = empresa.no 
	where
		empresa.ncont = (select ncont from empresa where no = @site_nr)
		
	/* Temp Table para permitir usar If statement */
	create table #dadosSt2 (
		sel				bit
		,ref			varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
		,design			varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,temFicha		bit
		,stock			numeric(13,3)
		,cativado		numeric(13,3)
		,epv1			numeric(19,6)
		,validade		varchar(10)
		,usaid			datetime
		,faminome		varchar(100)
		,familia		varchar(18)
		,comp			bit
		,generico		bit
		,psico			bit 
		,benzo			bit
		,ststamp		char(25)
		,ptoenc			numeric(10,3)
		,stmax			numeric(13,3)
		,marg1			numeric(16,3)
		,local			varchar(20)
		,u_local		varchar(60)
		,u_local2		varchar(60)
		,cnpem			varchar(8)
		,marg3			numeric(16,3)
		,marg4			numeric(16,3)
		,t5				bit
		,t4				bit
		,t45			varchar(20)
		,textosel		varchar(254)
		,u_lab			varchar(150)
		,usr1			varchar(200)
		,stns			bit 
		,inactivo		bit
		,dci			varchar(254)
		,site			varchar(18)
		,qttcli			numeric(13,3)
		,mfornec		numeric(6,2)
		,mfornec2		numeric(6,2)
		,u_duracao		numeric(8,0)
		,mrsimultaneo   int
		,especialidade	varchar(254)
		,obs			varchar(254)
		,marcada		bit
		,iva			numeric(5,2)
		,fornecedor		varchar(80)
		,hst			bit
		,u_familia		varchar(18)
		,pctf			varchar(18)
		,dispositivo_seguranca bit
		,qttminvd		numeric(10,2)
		,qttembal  		numeric(10,2)
		,epvqttminvd	numeric(20,2)
		,epvqttembal  	numeric(20,2)
		,tipo		    varchar(80)
		,histDias       varchar(10)
		,histDiasOrdem  numeric(10)
		,u_nota1		TEXT
		,hist			NUMERIC(10)
		,BB				numeric(16,3)
		,dataIniPromo	varchar(20)
		,dataFimPromo	varchar(20)
		)
	
	/* Result Set Para Pesquisas Feitas Apenas a Produtos com Ficha */
	IF @comFicha = 1
	BEGIN
		/* Dados do Produto */
		insert into #dadosSt2
		Select 
			sel	= convert(bit,0)
			,ref = isnull(st.ref, fprod.cnp)
			,design = ltrim(rtrim(isnull(st.design,fprod.design)))
			,temFicha = convert(bit,1)
			,stock = case when @stcativoretira=0 then
						case when @stkcaixa=0 then (case when st.stns = 0 then isnull(st.stock,0) else 0 end) else (case when st.stns = 0 and st.qttembal>0 then isnull(st.stock,0)/st.qttembal else 0 end) end 
					else
						case when @stkcaixa=0 then (case when st.stns = 0 then isnull(st.stock-st.cativado,0) else 0 end) else (case when st.stns = 0 and st.qttembal>0 then isnull(st.stock-st.cativado,0)/st.qttembal else 0 end) end 
					end 
			,cativado = case when @stkcaixa=0 then (case when st.stns = 0 then isnull(st.cativado,0) else 0 end) else (case when st.stns = 0 and st.qttembal>0 then isnull(st.cativado,0)/st.qttembal else 0 end) end 
			,epv1 = isnull(isnull(st.epv1,fprod.pvporig),0)
			,validade = isnull(convert(varchar,validade,102), '1900.01.01')
			,usaid = isnull(st.usaid, '1900.01.01')
			,faminome = isnull(st.faminome,'')
			,familia = isnull(st.familia, fprod.u_familia)
			,comp = isnull( (case 
								when ((left(cptgrp.descricao,3)!='Não' and cptgrp.u_diploma1='' and cptgrp.u_diploma2='' and cptgrp.u_diploma3='') AND cptgrp.grupo!='ST') or comp_sns>0
							then 1
							else 0
					 end)
				  ,0)
			,generico		= IsNull(fprod.generico,0)
			,'psico'		= isnull(fprod.psico,0)
			,'benzo'		= isnull(fprod.benzo,0)
			,'ststamp'		= isnull(st.ststamp,'')
			,'ptoenc'		= isnull(st.ptoenc,0)
			,'stmax'		= isnull(st.stmax,0)
			,'marg1'		= isnull(st.marg1,0)
			,'local'		= isnull(st.local,'')
			,'u_local'		= isnull(st.u_local,'')
			,'u_local2'		= isnull(st.u_local2,'')
			,cnpem = convert(varchar(8),ISNULL(fprod.cnpem,''))
			,marg3 = ROUND(isnull(st.marg3,0),2)
			,marg4 = ROUND(isnull(st.marg4,0),2)
			,t5 = case when ranking=1 and grphmgcode != 'GH0000' and grphmgcode != '' and fprod.pvporig <= fprod.pvpmaxre then convert(bit,1) else convert(bit,0) end
			,t4 = case when fprod.pvporig <= fprod.preco_acordo and fprod.preco_acordo > 0 then convert(bit,1) else convert(bit,0) end
			,t45 = case when fprod.pvporig <= fprod.preco_acordo and fprod.preco_acordo > 0 then 'T4'
						when ranking=1 and grphmgcode != 'GH0000' and grphmgcode != '' and fprod.pvporig <= fprod.pvpmaxre then 'T5' 
						else ''
				   end
			,textosel = ''
			,u_lab = ISNULL(st.u_lab,fprod.titaimdescr)
			,usr1 = left(ISNULL(st.usr1,fprod.u_marca),200)
			,ISNULL(stns,0)
			,st.inactivo
			,dci = ISNULL(dci,'')
			,site
			,qttcli
			,mfornec
			,mfornec2
			,u_duracao
			,mrsimultaneo
			,especialidade = isnull((select top 1 nome from b_cli_stRecursos(nolock) where b_cli_stRecursos.ref = st.ref and  tipo = 'Especialidade'),'')
			,obs = isnull(st.obs,'')
			,st.marcada
			,iva = isnull(taxasiva.taxa, 0)
			,st.fornecedor
			,hst = convert(bit,0)
			,u_familia = isnull(fprod.u_familia,'')
			,pctf = ''
			,dispositivo_seguranca = isnull(fprod.dispositivo_seguranca,0)
			,qttminvd
			,qttembal 
			,epvqttminvd = round(qttminvd * st.epv1,2)
			,epvqttembal = round(qttembal * st.epv1,2)
			,tipo =  isnull(b_famFamilias.design,'')
			,histDias = ''
			,histDiasOrdem = 9999999999
			,u_nota1 = ISNULL(st.u_nota1,'')
			,hist = 0
			,BB = st.marg4
			,convert(varchar(20), convert(date, st.dataIniPromo,108)) as dataIniPromo
			,convert(varchar(20), convert(date, st.dataFimPromo,108)) as dataFimPromo	
		From 
			st (nolock)
			left join fprod (nolock) on st.ref = fprod.cnp or (st.codCNP = fprod.cnp  and LTRIM(RTRIM(fprod.cnp))!='') and st.codCNP<>'0'
			left join cptgrp (nolock) on cptgrp.grupo = fprod.grupo
			left join empresa (nolock) on st.site_nr = empresa.no
			left join taxasiva (nolock) on st.tabiva = taxasiva.codigo
			left join b_famFamilias (nolock) on st.u_famstamp = b_famFamilias.famstamp
			left join stfami (nolock) on stfami.ref = st.familia
		Where 
			st.site_nr = @site_nr
			AND (
				st.ref like @artigo
				OR exists (Select bc.ref from bc (nolock) where bc.ref = st.ref and bc.site_nr = @site_nr and bc.codigo = @artigo)  
				OR st.design LIKE @artigo + '%'
				OR fprod.cnpem = @artigo
				OR fprod.cnpem_old = @artigo
				OR fprod.CHNM = @artigo		
				AND st.inactivo = @inactivo
				)
			AND st.u_famstamp = case when @tipoProduto = '' then st.u_famstamp else @tipoProdutoID END

			AND st.u_depstamp = case when @departamentoID = 0 then st.u_depstamp else @departamentoID END 
			AND st.u_secstamp = case when @seccaoID = 0 then st.u_secstamp else @seccaoID END
			AND st.u_catstamp = case when @categoriaID = 0 then st.u_catstamp else @categoriaID END
			AND st.u_segstamp = case when @segmentoID = 0 then st.u_segstamp else @segmentoID END  

			and st.fornec = (case when @fornStamp = '' then st.fornec else (select no from fl(nolock) where flstamp = @fornStamp) end)   		
			and st.fornestab = (case when @fornStamp = '' then st.fornestab else (select estab from fl(nolock) where flstamp = @fornStamp) end)
			and ( (st.local  COLLATE DATABASE_DEFAULT  in  (select local from #localizacoes(nolock)))
					or  (st.u_local COLLATE DATABASE_DEFAULT   in  (select local from #localizacoes(nolock)))
					or  (st.u_local2  COLLATE DATABASE_DEFAULT  in  (select local from #localizacoes(nolock)))
						)
			AND st.familia = CASE WHEN @familiaNo = '' THEN st.familia else  @familiaNo END
			AND st.u_lab = CASE WHEN @lab = '' THEN st.u_lab ELSE @lab END
			AND st.usr1 = CASE WHEN @marca = '' THEN st.usr1 ELSE @marca  END
	END
	ELSE /* Result Set Para Pesquisas Feitas a todos os produtos*/
	BEGIN
		insert into #dadosSt2
		Select 
			sel	= convert(bit,0)
			,ref = isnull(st.ref, fprod.cnp)
			,design = ltrim(rtrim(isnull(st.design,fprod.design)))
			,temFicha = case when st.ref is null or  st.site_nr!=@site_nr then convert(bit,0) else convert(bit,1) end
			,stock = case when @stcativoretira=0 then
						case when @stkcaixa=0 then (case when st.stns = 0 then isnull(st.stock,0) else 0 end) else (case when st.stns = 0 and st.qttembal>0 then isnull(st.stock,0)/st.qttembal else 0 end) end 
					else
						case when @stkcaixa=0 then (case when st.stns = 0 then isnull(st.stock-st.cativado,0) else 0 end) else (case when st.stns = 0 and st.qttembal>0 then isnull(st.stock-st.cativado,0)/st.qttembal else 0 end) end 
					end 
			,cativado = case when @stkcaixa=0 then (case when st.stns = 0 then isnull(st.cativado,0) else 0 end) else (case when st.stns = 0 and st.qttembal>0 then isnull(st.cativado,0)/st.qttembal else 0 end) end 
			,epv1 = isnull(isnull(st.epv1,fprod.pvporig),0)
			,validade = isnull(convert(varchar,validade,102), '1900.01.01')
			,usaid = isnull(st.usaid, '1900.01.01')
			,faminome = isnull(st.faminome,'')
			,familia = isnull(st.familia, fprod.u_familia)
			,comp = isnull( (case 
								when ((left(cptgrp.descricao,3)!='Não' and cptgrp.u_diploma1='' and cptgrp.u_diploma2='' and cptgrp.u_diploma3='') AND cptgrp.grupo!='ST')   or comp_sns>0
							then 1
							else 0
					 end)
				  ,0)
			,generico		= IsNull(fprod.generico,0)
			,'psico'		= isnull(fprod.psico,0)
			,'benzo'		= isnull(fprod.benzo,0)
			,'ststamp'		= isnull(st.ststamp,'')
			,'ptoenc'		= isnull(st.ptoenc,0)
			,'stmax'		= isnull(st.stmax,0)
			,'marg1'		= isnull(st.marg1,0)
			,'local'		= isnull(st.local,'')
			,'u_local'		= isnull(st.u_local,'')
			,'u_local2'		= isnull(st.u_local2,'')
			,cnpem = convert(varchar(8),ISNULL(fprod.cnpem,''))
			,marg3 = ROUND(isnull(st.marg3,0),2)
			,marg4 = ROUND(isnull(st.marg4,0),2)
			,t5 = case when ranking=1 and grphmgcode != 'GH0000' and grphmgcode != '' and fprod.pvporig <= fprod.pvpmaxre then convert(bit,1) else convert(bit,0) end
			,t4 = case when fprod.pvporig <= fprod.preco_acordo and fprod.preco_acordo > 0 then convert(bit,1) else convert(bit,0) end
			,t45 = case when fprod.pvporig <= fprod.preco_acordo and fprod.preco_acordo > 0 then 'T4'
						when ranking=1 and grphmgcode != 'GH0000' and grphmgcode != '' and fprod.pvporig <= fprod.pvpmaxre then 'T5' 
						else ''
				   end
			,textosel = ''
			,u_lab = ISNULL(st.u_lab,fprod.titaimdescr)
			,usr1 = left(ISNULL(st.usr1,fprod.u_marca),200)
			,ISNULL(stns,0)
			,st.inactivo
			,dci = ISNULL(dci,'')
			,site
			,qttcli
			,mfornec
			,mfornec2
			,u_duracao
			,mrsimultaneo
			,especialidade = isnull((select top 1 nome from b_cli_stRecursos(nolock) where b_cli_stRecursos.ref = st.ref and  tipo = 'Especialidade'),'')
			,obs = isnull(st.obs,'')
			,st.marcada
			,iva = isnull(taxasiva.taxa, 0)
			,st.fornecedor
			,hst = convert(bit,0)
			,u_familia = isnull(fprod.u_familia,'')
			,pctf = ''
			,dispositivo_seguranca = isnull(fprod.dispositivo_seguranca,0)
			,qttminvd
			,qttembal 
			,epvqttminvd = round(qttminvd * st.epv1,2)
			,epvqttembal = round(qttembal * st.epv1,2)
			,tipo =  isnull(b_famFamilias.design,'')
			,histDias = ''
			,histDiasOrdem = 9999999999
			,u_nota1 = ISNULL(st.u_nota1,'')
			,hist = 0
			,BB = st.marg4
			,convert(varchar(20), convert(date, st.dataIniPromo,108)) as dataIniPromo
			,convert(varchar(20), convert(date, st.dataFimPromo,108)) as dataFimPromo
		From 
			st (nolock)
			full join fprod (nolock) on st.ref = fprod.cnp or (st.codCNP = fprod.cnp  and LTRIM(RTRIM(fprod.cnp))!='')  and st.codCNP<>'0'
			left join cptgrp (nolock) on cptgrp.grupo = fprod.grupo
			left join empresa (nolock) on st.site_nr = empresa.no
			left join taxasiva (nolock) on st.tabiva = taxasiva.codigo
			left join b_famFamilias (nolock) on st.u_famstamp = b_famFamilias.famstamp
			left join stfami (nolock) on stfami.ref = st.familia
		Where			
				(
					(st.ref like @artigo
					OR st.design LIKE @artigo + '%'				
					OR exists (Select bc.ref from bc (nolock) where bc.ref = st.ref and bc.site_nr = @site_nr and bc.codigo = @artigo)  --st.codigo = @artigo
					)
					/*para poder usar os filtros na pesquisa sem ficha, caso existam na ficha produto (st) */
					AND st.u_famstamp = case when @tipoProduto = '' then st.u_famstamp else @tipoProdutoID END
					
					AND st.u_depstamp = case when @departamentoID = 0 then st.u_depstamp else @departamentoID END 
					AND st.u_secstamp = case when @seccaoID = 0 then st.u_secstamp else @seccaoID END
					AND st.u_catstamp = case when @categoriaID = 0 then st.u_catstamp else @categoriaID END
					AND st.u_segstamp = case when @segmentoID = 0 then st.u_segstamp else @segmentoID END  
				
					AND st.site_nr = @site_nr
					AND st.inactivo = @inactivo
					and ((st.local  COLLATE DATABASE_DEFAULT  in   (select local from #localizacoes(nolock)))
						or  (st.u_local  COLLATE DATABASE_DEFAULT in   (select local from #localizacoes(nolock)))
						or  (st.u_local2  COLLATE DATABASE_DEFAULT  in  (select local from #localizacoes(nolock))))	
						AND st.familia = CASE WHEN @familiaNo = '' THEN st.familia else  @familiaNo END
						AND st.u_lab = CASE WHEN @lab = '' THEN st.u_lab ELSE @lab END
						AND st.usr1 = CASE WHEN @marca = '' THEN st.usr1 ELSE @marca  END	
				)				
				OR 
				(
					(fprod.cnp = @artigo
					OR fprod.cnpem = @artigo
					OR fprod.cnpem_old = @artigo
					OR fprod.CHNM = @artigo				
					OR fprod.design LIKE @artigo + '%'
					) 
					AND fprod.id_grande_mercado_hmr = case when @departamentoID = 0 then fprod.id_grande_mercado_hmr 
																else @departamentoID END 
					AND fprod.id_mercado_hmr = case when @seccaoID = 0 then fprod.id_mercado_hmr else @seccaoID END
					AND fprod.id_categoria_hmr = case when @categoriaID = 0 then fprod.id_categoria_hmr else @categoriaID END
					AND fprod.id_segmento_hmr = case when @segmentoID = 0 then fprod.id_segmento_hmr else @segmentoID END
					AND fprod.cnp not in (select ref from st (nolock) where st.site_nr = @site_nr) 
	
				)	
	END

	if @localizacao='Sem Localização'
	begin
		delete  from #dadosSt2 where local !='' or u_local !='' or u_local2 !=''	
	end

	if @dci != ''
		delete from #dadosSt2 where isnull(dci,'''') not like @dci +'%'
	if @servicos = 1
		delete from #dadosSt2 where stns != 1
	if @servicos = 2
		delete from #dadosSt2 where stns = 1
	if @generico = 1
		delete from #dadosSt2 where generico != 1
	if @generico = 0
		delete from #dadosSt2 where generico != 0
	if @stock != -999999
	begin
		if @operadorLogico = 'maior'
			delete from #dadosSt2 where stock <= @stock and #dadosSt2.stns != 1
		if @operadorLogico = 'menor'
			delete from #dadosSt2 where stock >= @stock and #dadosSt2.stns != 1
		if @operadorLogico = 'igual'
			delete from #dadosSt2 where stock != @stock and #dadosSt2.stns != 1
	end
	if @atendimento = 1 and @refExcluir != char(39)+'vazio'+CHAR(39)
		delete from #dadosSt2 where ref in (select items from dbo.up_SplitToTable(@refExcluir,','))
	if @parafarmacia = 1
		delete from #dadosSt2 where familia = "1" or u_familia = "1" or u_familia = "58" or familia="58"
	if @t5 = 1
		delete from #dadosSt2 where t5 = 0
	if @t4 = 1
		delete from #dadosSt2 where t4 = 0		
	if @recurso != ''
		delete from #dadosSt2 where ref not in (select ref from b_cli_stRecursos where nome = @recurso)
	if @txIva != -1
		delete from #dadosSt2 where iva != @txIva
	if @fornecedor != '' AND @fornStamp = ''
	begin 
		if @fornecedor = 'S/ Fornecedor'		
			delete from #dadosSt2 where fornecedor != ''
		else 
			delete from #dadosSt2 where fornecedor != @fornecedor
	end
	if @diassmov != 0
	begin
		delete from #dadosSt2 where DATEDIFF(day,#dadosSt2.usaid, getdate()) < @diassmov 
	end


	/* result set final */
	/* Caso seja multi Loja */
	IF (select count(*) empresa_no from #dadosArmazens) > 1
		BEGIN
			DECLARE @sql VARCHAR(4000) = ''
			SET @sql = @sql +N'
			Select 
				top (' + convert(varchar(10),@top) + ') st2.*
				,diassmov = DATEDIFF(day,st2.usaid, getdate())
				,stockempresa = case when ' + convert(varchar(10),@stkcaixa) +'=0 then isnull(x.stockempresa,0)  else ( case when st2.qttembal>0 then isnull(x.stockempresa,0) /st2.qttembal else 0 end) end 
				,StockGrupo  = case when ' + convert(varchar(10),@stkcaixa) +'=0 then isnull(y.StockGrupo,0)  else ( case when st2.qttembal>0 then isnull(y.StockGrupo,0) /st2.qttembal else 0 end) end 
			From
				#dadosSt2 st2
				left outer join (select 
									ref
									,StockEmpresa = sum(stock) 
								from 
									st (nolock)
								where 
									site_nr in (select * from #dadosArmazens)
									and stns = 0
								group by 
									ref) x on st2.ref = x.ref
				left outer join (select 
									ref
									,StockGrupo = sum(stock) 
								from 
									st (nolock)
								where 
									stns = 0
								group by 
									ref) y on st2.ref = y.ref
			order by
				st2.marcada desc'
				if @var_ordenacao != ''
				set @sql = @sql+ N', ' + @var_ordenacao +' ' 

				print @sql
				exec(@sql)
		END
	ELSE /* Caso seja apenas 1 Loja */
		BEGIN
			DECLARE @sql1 VARCHAR(4000) = ''
			SET @sql1 = @sql1 +N'
			Select 
				top (' + convert(varchar(10),@top) + ') st2.*
				,diassmov = DATEDIFF(day,st2.usaid, getdate())
				,stockEmpresa = case when ' + convert(varchar(10),@stkcaixa) +'=0 then st2.stock else ( case when st2.qttembal>0 then st2.stock/st2.qttembal else 0 end) end
				,StockGrupo   = case when ' + convert(varchar(10),@stkcaixa) +'=0 then st2.stock else ( case when st2.qttembal>0 then st2.stock/st2.qttembal else 0 end) end

			From
				#dadosSt2 st2
			order by
				st2.marcada desc'
				IF @var_ordenacao != ''
				set @sql1 = @sql1+ N', ' + @var_ordenacao +' ' 

				print @sql1
				exec(@sql1)
		END

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosArmazens'))
		DROP TABLE #dadosArmazens	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt2'))
		DROP TABLE #dadosSt2	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#localizacoes'))
		DROP TABLE #localizacoes
GO
GRANT EXECUTE on dbo.up_stocks_pesquisa_comecado TO PUBLIC
GRANT Control on dbo.up_stocks_pesquisa_comecado TO PUBLIC
GO