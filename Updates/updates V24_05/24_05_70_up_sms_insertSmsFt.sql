/*
	select * from b_sms_fact
	
exec up_sms_insertSmsFt '467374359', 'Reserva', 557467998, 'LTS_Testes', 'WS_INTERNAL_557430254 - WS_INTERNAL_557466737 - WS_INTERNAL_557471014 - WS_INTERNAL_557467998 - WS_INTERNAL_557471016 - WS_INTERNAL_557425464 - ', 1, 'Tem uma reserva com o numero 220 relativa à data Mar 11 2022 12:00AM', '2022-03-22T12:27:32.0000000Z', 2

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_insertSmsFt]') IS NOT NULL
	drop procedure dbo.up_sms_insertSmsFt
go

create procedure dbo.up_sms_insertSmsFt
	@campanhaID varchar(250)
	,@name varchar(250)
	,@listID int
	,@username varchar(50)
	,@listTlm varchar(max)
	,@Total int
	,@mensagem      varchar(459)
	,@strDataEnvio  varchar(100)
	,@status int
	
/* WITH ENCRYPTION */ 
AS


	declare @dataenvio date   =  convert(date,@strDataEnvio)
	declare @horaenvio varchar(8) = convert(varchar(8),convert(time,@strDataEnvio))



	insert into b_sms_fact
		(stamp, campaignId, name, listId, username, listTlm, Total, mensagem, dataenvio, horaenvio, [status])
	values
		(left(newid(),25), @campanhaID, @name, @listID, @username, @listTlm
		,isnull(@Total,0), @mensagem, @dataenvio, @horaenvio, @status)                   

GO
Grant Execute on dbo.up_sms_insertSmsFt to Public
Grant Control on dbo.up_sms_insertSmsFt to Public
GO

