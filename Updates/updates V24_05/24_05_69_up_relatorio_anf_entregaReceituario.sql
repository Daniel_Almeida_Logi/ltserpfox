-- Análise: Mapa Comprovativo da Entrega/Envio do Receituario do Mes 
-- exec up_relatorio_anf_entregaReceituario 2024,10,0,'Loja 1'

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_anf_entregaReceituario]') IS NOT NULL
	drop procedure dbo.up_relatorio_anf_entregaReceituario
go

create procedure dbo.up_relatorio_anf_entregaReceituario
@Ano numeric(4,0),
@Mes numeric(2,0),
@excSNS bit,
@site varchar(55)

/* with encryption */
AS
SET NOCOUNT ON

DECLARE @tipoEntidade VARCHAR(10)

SET @tipoEntidade = (SELECT assfarm from empresa(nolock) where site= @site)

IF @tipoEntidade = 'AFP'
BEGIN
	IF @excSNS = 1 
	begin 
		;with
			cte1 as (
				select 
					ofistamp
				from
					fi (nolock)
					inner join ft (nolock) on ft.ftstamp=fi.ftstamp
					inner join td (nolock) on td.ndoc=ft.ndoc
				where 
					td.u_tipodoc in (2, 7, 6)
					and ft.no < 199 and ft.anulado = 0
					and ft.site = (case when @site = '' Then ft.site else @site end)
			)
		select
			ano, mes, cast(no as numeric(10,0)) as no, Organismo, design, nmdoc, cast(fno as numeric(10,0)) as fno, sum(comp) as comp
		from (
			select
				year(ft.fdata) as ano, month(ft.fdata) as Mes, cast(ft.no as numeric(10,0)) as no, ft.nome as Organismo, ft.nmdoc, cast(ft.fno as numeric(10,0)) as fno, sum(etiliquido) as comp,
				case when fi.design like ('%Protocolo%') then fi.design else '' end as design
	
			from
				fi (nolock)
				inner join ft (nolock)	on ft.ftstamp=fi.ftstamp
				inner join td (nolock)	on td.ndoc=ft.ndoc
				inner join b_utentes (nolock) on ft.no=b_utentes.no
				left join cptorg(nolock) on cptorg.u_no = b_utentes.no
			where
				year(ft.fdata)= @ano and month(ft.fdata)= @mes 
				and ft.anulado=0 and b_utentes.estab=0
				and ((td.u_tipodoc = 1 And @tipoEntidade = 'AFP'))
				and ft.site = (case when @site = '' Then ft.site else @site end)
				--and fistamp not in (select ofistamp from cte1)
				and ft.no > 1 
				and ft.no < 199 
				and ft.no not in (94,142) -- excluir manuais
				and ISNULL(cptorg.mcdt,0)=0
				and cptorg.abrev != 'SICAD'
			group by
				year(ft.fdata), month(ft.fdata), ft.fno, ft.nmdoc, ft.no, ft.nome, design, ft.site
		) x
		where
			comp!=0
		group by
			ano, mes, no, Organismo, design, nmdoc, fno
	end
	else
	begin
		;with
			cte1 as (
				select 
					ofistamp
				from
					fi (nolock)
					inner join ft (nolock) on ft.ftstamp=fi.ftstamp
					inner join td (nolock) on td.ndoc=ft.ndoc
				where 
					td.u_tipodoc in (2, 7, 6)
					and ft.no < 199 and ft.anulado = 0
					and ft.site = (case when @site = '' Then ft.site else @site end)
			)
		select
			ano, mes, cast(no as numeric(10,0)) as no, Organismo, design, nmdoc, cast(fno as numeric(10,0)) as fno, sum(comp) as comp
		from (
			select
				year(ft.fdata) as ano, month(ft.fdata) as Mes, cast(ft.no as numeric(10,0)) as no, ft.nome as Organismo, ft.nmdoc, cast(ft.fno as numeric(10,0)) as fno,etiliquido as comp,
				case when fi.design like ('%Protocolo%') then fi.design else '' end as design
			
			from
				fi (nolock)
				inner join ft (nolock)	on ft.ftstamp=fi.ftstamp
				inner join ft2 (nolock) on ft.ftstamp = ft2.ft2stamp
				inner join td (nolock)	on td.ndoc=ft.ndoc
				inner join b_utentes (nolock) on ft.no=b_utentes.no
				left join cptorg(nolock) on cptorg.u_no = b_utentes.no
			where
				@Ano = case when td.nmdoc like 'Factura SNS%' then year(ft.cdata) else year(ft.fdata) end
				and @mes = case when td.nmdoc like 'Factura SNS%' then month(ft.cdata) else month(ft.fdata) end
				and ft.anulado=0 
				and b_utentes.estab=0
				and ((td.u_tipodoc = 1 And @tipoEntidade = 'AFP'))
				and ft.site = (case when @site = '' Then ft.site else @site end)
				--and fistamp not in (select ofistamp from cte1)
				and ft.no < 199 
				and ft.no not in (94) -- excluir manuais
			group by
				year(ft.fdata), month(ft.fdata), ft.fno, ft.nmdoc, ft.no, ft.nome, design, ft.site, fi.etiliquido
		) x
		where
			comp!=0
		group by
			ano, mes, no, Organismo, design, nmdoc, fno
	end
END

IF @tipoEntidade = 'ANF'
BEGIN
	IF @excSNS = 1 
	begin 
		;with
			cte1 as (
				select 
					ofistamp
				from
					fi (nolock)
					inner join ft (nolock) on ft.ftstamp=fi.ftstamp
					inner join td (nolock) on td.ndoc=ft.ndoc
				where 
					td.u_tipodoc in (2, 7, 6)
					and ft.no < 199 and ft.anulado = 0
					and ft.site = (case when @site = '' Then ft.site else @site end)
			)
		select
			ano, mes, cast(no as numeric(10,0)) as no, Organismo, design, nmdoc, cast(fno as numeric(10,0)) as fno, sum(comp) as comp
		from (
			select
				year(ft.fdata) as ano, month(ft.fdata) as Mes, cast(ft.no as numeric(10,0)) as no, ft.nome as Organismo, ft.nmdoc, cast(ft.fno as numeric(10,0)) as fno, sum(etiliquido) as comp,
				case when fi.design like ('%Protocolo%') then fi.design else '' end as design
	
			from
				fi (nolock)
				inner join ft (nolock)	on ft.ftstamp=fi.ftstamp
				inner join td (nolock)	on td.ndoc=ft.ndoc
				inner join b_utentes (nolock) on ft.no=b_utentes.no
				left join cptorg(nolock) on cptorg.u_no = b_utentes.no
			where
				year(ft.fdata)= @ano and month(ft.fdata)= @mes 
				and ft.anulado=0 and b_utentes.estab=0
				and (td.u_tipodoc in (1,6,7) And @tipoEntidade = 'ANF')
				and ft.site = (case when @site = '' Then ft.site else @site end)
				--and fistamp not in (select ofistamp from cte1)
				and ft.no > 1 
				and ft.no < 199 
				and ft.no not in (94,142) -- excluir manuais
				and ISNULL(cptorg.mcdt,0)=0
				and cptorg.id_anf in('36','04','15','FM','77','F0','JC','X1','30','53','03','11','12','W1','S1','77')
			group by
				year(ft.fdata), month(ft.fdata), ft.fno, ft.nmdoc, ft.no, ft.nome, design, ft.site
		) x
		where
			comp!=0
		group by
			ano, mes, no, Organismo, design, nmdoc, fno
	end
	else
	begin
		;with
			cte1 as (
				select 
					ofistamp
				from
					fi (nolock)
					inner join ft (nolock) on ft.ftstamp=fi.ftstamp
					inner join td (nolock) on td.ndoc=ft.ndoc
				where 
					td.u_tipodoc in (2, 7, 6)
					and ft.no < 199 and ft.anulado = 0
					and ft.site = (case when @site = '' Then ft.site else @site end)
			)
		select
			ano, mes, cast(no as numeric(10,0)) as no, Organismo, design, nmdoc, cast(fno as numeric(10,0)) as fno, sum(comp) as comp
		from (
			select
				year(ft.fdata) as ano, month(ft.fdata) as Mes, cast(ft.no as numeric(10,0)) as no, ft.nome as Organismo, ft.nmdoc, cast(ft.fno as numeric(10,0)) as fno, sum(etiliquido) as comp,
				case when fi.design like ('%Protocolo%') then fi.design else '' end as design
			
			from
				fi (nolock)
				inner join ft (nolock)	on ft.ftstamp=fi.ftstamp
				inner join ft2 (nolock) on ft.ftstamp = ft2.ft2stamp
				inner join td (nolock)	on td.ndoc=ft.ndoc
				inner join b_utentes (nolock) on ft.no=b_utentes.no
				left join cptorg(nolock) on cptorg.u_no = b_utentes.no
			where
				@Ano = case when td.nmdoc like 'Factura SNS%' then year(ft.cdata) else year(ft.fdata) end
				and @mes = case when td.nmdoc like 'Factura SNS%' then month(ft.cdata) else month(ft.fdata) end
				and ft.anulado=0 
				and b_utentes.estab=0
				and ((td.u_tipodoc in (1,6,7) And @tipoEntidade = 'ANF'))
				and ft.site = (case when @site = '' Then ft.site else @site end)
				--and fistamp not in (select ofistamp from cte1)
				and ft.no < 199 
				and ft.no not in (94) -- excluir manuais
				and cptorg.id_anf in('36','04','15','FM','77','F0','JC','X1','30','53','03','11','12','W1','S1','77')
			group by
				year(ft.fdata), month(ft.fdata), ft.fno, ft.nmdoc, ft.no, ft.nome, design, ft.site
		) x
		where
			comp!=0
		group by
			ano, mes, no, Organismo, design, nmdoc, fno
	end
END

GO
Grant Execute on dbo.up_relatorio_anf_entregaReceituario to Public
Grant Control on dbo.up_relatorio_anf_entregaReceituario to Public
Go


