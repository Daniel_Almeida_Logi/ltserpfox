
/****** 
	Object:  get simposium Request
	exec [dbo].[up_documentos_infoCompartHeader]   'ADM3280B948-3FDD-45E7-8F4'
***/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_simposiumRequest]') IS NOT NULL
    drop procedure dbo.[up_get_simposiumRequest]
go
create procedure dbo.[up_get_simposiumRequest]
@token as varchar(36)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

    SELECT
		typeRequest,
		typeRequestDesc,
		workplace,
		service,
		memberId,
		test,
		site
    FROM
		simposiumRequest (NOLOCK) 
    WHERE
		simposiumRequest.token = @token     
GO
Grant Execute On dbo.[up_get_simposiumRequest] to Public
Grant Control On dbo.[up_get_simposiumRequest] to Public
GO
