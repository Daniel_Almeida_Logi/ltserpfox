-- exec up_res_NotificacoesTextoAutomatico 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_res_NotificacoesTextoAutomatico]') IS NOT NULL
	drop procedure dbo.up_res_NotificacoesTextoAutomatico
go

create procedure dbo.up_res_NotificacoesTextoAutomatico

/* WITH ENCRYPTION */

AS 
BEGIN
	Select	[desc] = 'Número da Reserva',
			[cod] = '«obrano»'
	Union ALL
	Select	[desc] = 'Data da Reserva',
			[cod] = '«dataobra»'
	Union ALL
	Select	[desc] = 'Designação do Produto',
			[cod] = '«design»'
	Union ALL
	Select	[desc] = 'Referência do Produto',
			[cod] = '«ref»'
	Union ALL
	Select	[desc] = 'Nova Linha',
			[cod] = '\r\n'
END

GO
Grant Execute on dbo.up_res_NotificacoesTextoAutomatico to Public
GO
Grant Control on dbo.up_res_NotificacoesTextoAutomatico to Public
GO