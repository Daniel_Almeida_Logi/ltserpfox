IF( NOT EXISTS (SELECT * FROM B_analises (NOLOCK) WHERE ordem=200))
	BEGIN
		INSERT INTO [dbo].[B_analises]
			([ordem],[grupo],[subgrupo],[descricao]
			,[report],[stamp],[url],[comentario]
			,[ecra],[stamp_analiseav],[estado]
			,[tabela],[perfil],[filtros],[formatoExp]
			,[comandoFox],[objectivo],[marcada])
		VALUES
			(200,'Gestão','Farmácia','Rel Embalagens com Disp de Segurança sem Registo MVO '
			,'relatorio_Sem_Informacao_NMVO','','','Obs.:'
			,'','',1
			,'','Rel Embalagens com Disp de Segurança sem Registo MVO ','',''
			,'','',0)
	END


IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=200 and label='Data Início'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 200,[nome],[label],[tipo],[comando],1,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 1 and label='Data Início'
END

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=200 and label='Data Fim'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 200,[nome],[label],[tipo],[comando],2,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 1 and label='Data Fim'
END

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=200 and label='Loja'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 200,[nome],[label],[tipo],[comando],3,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],0,[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 1 and label='Loja'
END

IF (NOT EXISTS (SELECT * FROM b_analises_config WHERE ordem=200 and label='Operador'))
BEGIN
INSERT INTO [dbo].[b_analises_config]([ordem],[nome],[label],[tipo],[comando],[fordem] ,[vdefault],[vdefaultsql],[vdefaultfox]
				,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],[multiselecao],[comandoesql],[visivel])
SELECT 200,[nome],[label],[tipo],[comando],4,[vdefault],[vdefaultsql],[vdefaultfox]
		,[colunas],[colunasWidth],[obrigatorio],[firstempty],[mascara],[formato],0,[comandoesql],[visivel]
  FROM [dbo].[b_analises_config]
  where ordem = 170 and label='Operador'
END