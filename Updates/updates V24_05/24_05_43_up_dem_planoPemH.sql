
/*

	Devolve plano PEMH

	up_dem_planoPemH 'd6c9caab-5139-4d95-987b-3ceaefaa00f3'


	349 - FARM. HOSP. AMATO LUSITANO ULS - Castelo Branco
	360 - FARM. HOSP. EGAS MONIZ -ULS Lisboa Ocidental
	343 - FARM. HOSP. GERAL SANTO ANTÓNIO - ULS Santo António
	359 - FARM. HOSP. H.G.ORTA -ULS Almada Seixal
	331 - FARM. HOSP. HOSPITAL CURRY CABRAL - ULS São José
	328 - FARM. HOSP. IPO LISBOA - IPO Lisboa
	318 - FARM. HOSP. SÃO JOÃO - ULS São João
	309 - FARM. HOSP. BRAGA - ULS Braga

	--Como os codigos não são devolvidos temos de pesquisar por nome


*/
if OBJECT_ID('[dbo].[up_dem_planoPemH]') IS NOT NULL
	drop procedure dbo.up_dem_planoPemH
go

create procedure dbo.up_dem_planoPemH
	@token  varchar (40)

	AS

	DECLARE 
		@local_presc_cod VARCHAR(100) = '',
		@local_presc_descr  VARCHAR(150) = '',
		@plano VARCHAR(3) = ''
	
	SELECT 
		@local_presc_cod = rtrim(ltrim(ISNULL(local_presc_cod,''))),
		@local_presc_descr = rtrim(ltrim(ISNULL(local_presc_descr,'')))
	FROM
		Dispensa_Eletronica(NOLOCK)
	WHERE
		token = @token

	
    SET @plano = CASE 
                    WHEN @local_presc_descr like '%LUSITANO%' THEN 'H12'
					WHEN @local_presc_descr like '%MONIZ%' THEN 'H16'
					WHEN @local_presc_descr like '5ANTÓNIO%' THEN 'H20'
					WHEN @local_presc_descr like '%Seixal%' THEN 'H08'
					WHEN @local_presc_descr like '%CURRY%' THEN 'H22'
					WHEN @local_presc_descr like '%IPO LISBOA%' THEN 'H41'
					WHEN @local_presc_descr like '%SÃO JOÃO%' THEN 'H21'
					WHEN @local_presc_descr like '%BRAGA%' THEN 'H11'
                    WHEN @local_presc_descr = '' THEN 'SU'
                    ELSE 'SU'  -- ou outro valor padrão, se necessário
                 END;

    
    SELECT @plano AS plano;



GO
Grant Execute On dbo.up_dem_planoPemH to Public
Grant Control On dbo.up_dem_planoPemH to Public
Go 


