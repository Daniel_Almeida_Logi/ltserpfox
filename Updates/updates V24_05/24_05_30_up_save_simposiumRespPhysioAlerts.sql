SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

-- Se a procedure já existir, remove
IF OBJECT_ID('[dbo].[up_save_simposiumRespPhysioAlerts]') IS NOT NULL
    DROP PROCEDURE dbo.[up_save_simposiumRespPhysioAlerts]
GO

-- Cria a procedure
CREATE PROCEDURE dbo.[up_save_simposiumRespPhysioAlerts]
    @stamp        AS VARCHAR(36),
    @token        AS VARCHAR(36),
    @id           AS INT,
    @typeId       AS INT,
    @type         AS VARCHAR(254),
    @level        AS INT,
    @route        AS VARCHAR(254),
    @descr        AS VARCHAR(MAX),
    @risk         AS VARCHAR(MAX),
    @advice       AS VARCHAR(MAX),
    @ousrinis     AS VARCHAR(30),
    @usrinis      AS VARCHAR(30)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON;

-- Verifica se o registro já existe na tabela pelo stamp
IF EXISTS (SELECT 1 FROM dbo.simposiumRespPhysioAlerts WHERE stamp = @stamp)
BEGIN
    -- Faz o UPDATE se o registro já existir (sem atualizar o campo ousrinis)
    UPDATE dbo.simposiumRespPhysioAlerts
    SET 
        token = @token,
        id = @id,
        typeId = @typeId,
        type = @type,
        level = @level,
        route = @route,
        descr = @descr,
        risk = @risk,
        advice = @advice,
        usrdata = GETDATE(),
        usrinis = @usrinis
    WHERE stamp = @stamp;
END
ELSE
BEGIN
    -- Faz o INSERT se o registro não existir
    INSERT INTO dbo.simposiumRespPhysioAlerts (
        stamp, token, id, typeId, type, level, route, descr, risk, advice, ousrinis, ousrdata, usrinis, usrdata
    )
    VALUES (
        @stamp, @token, @id, @typeId, @type, @level, @route, @descr, @risk, @advice, @ousrinis, GETDATE(), @usrinis, GETDATE()
    );
END;

GO
Grant Execute On dbo.[up_save_simposiumRespPhysioAlerts] to Public
Grant Control On dbo.[up_save_simposiumRespPhysioAlerts] to Public
GO
