/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'fi_compart'
	,@columnName		= 'refDescription'
	,@columnType		= 'varchar (254)'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go


/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'fi_compart'
	,@columnName		= 'unitValue'
	,@columnType		= 'numeric (18,4)'
	,@columnDefault		= '(''0'')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go


/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'ft_compart'
	,@columnName		= 'inclIva'
	,@columnType		= 'bit'
	,@columnDefault		= '(''0'')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go

/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'ft_compart'
	,@columnName		= 'nif'
	,@columnType		= 'varchar (25)'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go

/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'ft_compart'
	,@columnName		= 'commercialDiscount'
	,@columnType		= 'numeric (18,4)'
	,@columnDefault		= '(''0'')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go


/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'ft_compart'
	,@columnName		= 'incidenceBasis'
	,@columnType		= 'numeric (18,4)'
	,@columnDefault		= '(''0'')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go


/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'ft_compart'
	,@columnName		= 'totalPercIva'
	,@columnType		= 'numeric (18,4)'
	,@columnDefault		= '(''0'')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go


/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'ft_compart'
	,@columnName		= 'financialDiscount'
	,@columnType		= 'numeric (18,4)'
	,@columnDefault		= '(''0'')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go


/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'ft_compart'
	,@columnName		= 'totalValueDocument'
	,@columnType		= 'numeric (18,4)'
	,@columnDefault		= '(''0'')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go


/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'ft_compart'
	,@columnName		= 'idmotive'
	,@columnType		= 'numeric (10,0)'
	,@columnDefault		= '(''0'')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go

/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'ft_compart'
	,@columnName		= 'beneficiaryName'
	,@columnType		= 'varchar (254)'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go





/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'fi_compart'
	,@columnName		= 'percCmp'
	,@columnType		= 'numeric (18,4)'
	,@columnDefault		= '(''0'')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go
