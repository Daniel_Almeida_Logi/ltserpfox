/****** 
	Object:  up_save_simposiumRespClassification
	exec [dbo].[up_save_simposiumRespClassification]  
***/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_save_simposiumRespClassification]') IS NOT NULL
    DROP PROCEDURE dbo.[up_save_simposiumRespClassification]
GO

CREATE PROCEDURE dbo.[up_save_simposiumRespClassification]
    @stamp           VARCHAR(36),
    @token           VARCHAR(36),
    @code            VARCHAR(254),
    @descr           VARCHAR(max),
    @numberOfItems   NUMERIC(10,0),
    @ousrinis        VARCHAR(30),
    @usrinis         VARCHAR(30)
AS
BEGIN
    SET NOCOUNT ON;

    IF EXISTS (SELECT 1 FROM dbo.[simposiumRespClassification] WHERE [stamp] = @stamp)
    BEGIN
        -- Atualiza o registro existente
        UPDATE dbo.[simposiumRespClassification]
        SET 
            [token] = @token,
            [code] = @code,
            [descr] = @descr,
            [numberOfItems] = @numberOfItems,
            [usrinis] = @usrinis,
            [usrdata] = GETDATE()
        WHERE 
            [stamp] = @stamp;
    END
    ELSE
    BEGIN
        -- Insere um novo registro
        INSERT INTO dbo.[simposiumRespClassification] (
            [stamp], [token], [code], [descr], [numberOfItems], [ousrinis], [ousrdata], [usrinis], [usrdata]
        )
        VALUES (
            @stamp, @token, @code, @descr, @numberOfItems, @ousrinis, GETDATE(), @usrinis, GETDATE()
        );
    END
END

GO
Grant Execute On dbo.[up_save_simposiumRespClassification] to Public
Grant Control On dbo.[up_save_simposiumRespClassification] to Public
GO
