/*
	Mapa de Registo de Saídas de Psicotrópicos (Gestão de Psico / Benzo)
	
	 exec up_psico_saidasPsico '20241126', '20241206', 'Loja 1'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_psico_saidasPsico]') IS NOT NULL
    drop procedure dbo.up_psico_saidasPsico
go

create procedure dbo.up_psico_saidasPsico

@dataIni DATETIME,
@dataFim DATETIME,
@site	 VARCHAR(20)

/* with encryption */
AS

SET NOCOUNT ON
	

SELECT
	pesquisa				= CONVERT(BIT,0)
	,fi.ref
	,fi.design
	,fi.u_psicont
	,qtt					= CONVERT(INT,fi.qtt - ISNULL(xx.qtt,0))
	,ft.nmdoc
	,ft.fno
	,fdata					= CONVERT(VARCHAR,ft.fdata,102)
	,ft.nome
	,fi.fistamp
	,ft.ftstamp
	,quebras				= CONVERT(INT,0)
	,stock		= (
		SELECT 
			SUM(stock)
		FROM 
			sa(NOLOCK) 
			INNER JOIN empresa_arm(NOLOCK)	on empresa_arm.armazem = sa.armazem
			INNER JOIN empresa(NOLOCK)		on empresa_arm.empresa_no = empresa.no
		WHERE 
			sa.ref = fi.ref
			AND empresa.site = @site
	)
	,dp.u_medico
	,dp.u_nmutdisp
	,dp.u_moutdisp
	,dp.u_nmutavi
	,dp.u_moutavi
	,dp.u_idutavi
	,dp.u_dirtec
	,ft2.u_nbenef
	,u_receita				= ISNULL((SELECT TOP 1 yy.u_receita), ft2.u_receita)
	,dataini				= CONVERT(CHAR(10),@DataIni,103)
	,datafim				= CONVERT(CHAR(10),@DataFim,103)
	,obs					= CASE WHEN ft.anulado=1 THEN 'Documento Anulado' ELSE '' END
	,obs2					= CASE WHEN zz.qtt=fi.qtt THEN 'Venda Anulada por Devolução' ELSE '' END
	,dp.u_ndutavi
FROM
	fi (NOLOCK)
	INNER JOIN st (NOLOCK)					ON fi.ref=st.ref
	INNER JOIN fprod (NOLOCK)				ON fi.ref = fprod.cnp And fprod.psico=1
	INNER JOIN ft (NOLOCK)					ON ft.ftstamp = fi.ftstamp
	INNER JOIN ft2 (NOLOCK)					ON ft.ftstamp = ft2.ft2stamp
	INNER JOIN empresa (NOLOCK)				ON empresa.no = st.site_nr
	INNER JOIN B_dadosPsico dp	(NOLOCK)	ON dp.ftstamp=ft.ftstamp

	LEFT JOIN (
		SELECT 
			u_psicont,
			qtt = SUM(qtt) 
		FROM
			fi xfi				(NOLOCK)
			INNER JOIN ft xft	(NOLOCK) ON xft.ftstamp=xfi.ftstamp
		WHERE
			xfi.tipodoc=3
			AND xfi.fistamp NOT IN (
				SELECT ofistamp FROM fi (NOLOCK) WHERE u_psicont=fi.u_psicont
			)
			AND xft.site = CASE WHEN @site = '' THEN xft.site ELSE @site END
		GROUP BY
			xfi.u_psicont
	) AS xx ON fi.u_psicont = xx.u_psicont

	LEFT JOIN (
		SELECT
			u_psicont,
			ft2x.u_receita
		FROM
			ft2 ft2x			(NOLOCK)
			inner join ft xft	(NOLOCK) on xft.ftstamp = ft2x.ft2stamp
			inner join fi xfi	(NOLOCK) on ft2x.ft2stamp = xfi.ftstamp
		WHERE
			xfi.tipodoc IN (1,4)
			AND (SELECT COUNT(*) FROM fi yfi (NOLOCK) WHERE yfi.ofistamp=xfi.fistamp) = 0
			AND xft.site = CASE WHEN @site = '' THEN xft.site ELSE @site END
	) AS yy ON fi.u_psicont = yy.u_psicont


	LEFT JOIN (
		SELECT
			u_psicont,
			qtt = SUM(qtt)
		FROM
			fi xfi				(NOLOCK)
			inner join ft xft	(NOLOCK) ON xft.ftstamp=xfi.ftstamp
		WHERE
			xfi.tipodoc=3
			AND (SELECT COUNT(*) FROM fi yfi (NOLOCK) WHERE yfi.ofistamp=xfi.fistamp) = 0
			AND xft.site = CASE WHEN @site = '' THEN xft.site ELSE @site END
		GROUP BY
			xfi.u_psicont
	) AS zz ON fi.u_psicont=zz.u_psicont


WHERE
	ft.site = CASE WHEN @site = '' THEN ft.site ELSE @site END
	AND ft.fdata Between @DataIni AND @DataFim
	AND ft.ndoc!=76
	AND fi.u_psicont>0
	AND (
		fi.ofistamp=''
		OR (SELECT u_psicont FROM fi wfi (NOLOCK) WHERE wfi.fistamp=fi.ofistamp) = 0
	)
	AND ft.site = empresa.site

UNION ALL

SELECT
	pesquisa				= CONVERT(BIT,0)
	,ref = ISNULL(sl.ref,bi.ref)
	,bi.design
	,bi.u_psicont
	,qtt					= CONVERT(INT,
								ISNULL(sl.qtt, CASE WHEN cm>50 AND (ts.codigoDoc=32 OR ts.codigoDoc=34 OR ts.codigoDoc=35)
								  THEN bi.qtt ELSE 0 END)
							)
	,nmdoc					= case when sl.cm  = 55
								then
									'Devol. a Fornecedor'
								else
									ISNULL(sl.cmdesc,bo.nmdos)
								end 
	,fno					= bo.obrano
	,fdata					= CONVERT(VARCHAR,bo.dataobra,102)
	,bo.nome
	,bi.bistamp
	,bo.bostamp
	,quebras				= CONVERT(INT,ISNULL(sl.qtt,CASE WHEN cm>50 and ts.codigoDoc=33 THEN bi.qtt ELSE 0 END))
	,stock		= (
		SELECT 
			SUM(stock)
		FROM 
			sa(NOLOCK) 
			INNER JOIN empresa_arm(NOLOCK) ON empresa_arm.armazem = sa.armazem
			INNER JOIN empresa(NOLOCK)     ON empresa_arm.empresa_no = empresa.no
		WHERE 
			sa.ref = sl.ref
			AND empresa.site = @site
	)
	,u_medico				= ''
	,u_nmutdisp				= '' 
	,u_moutdisp				= '' 
	,u_nmutavi				= '' 
	,u_moutavi				= ''
	,u_idutavi				= 0
	,u_nbenef				= ''
	,u_receita				= ''
	,u_dirtec				= ''
	,dataini				= CONVERT(CHAR(10),@DataIni,103) 
	,datafim				= CONVERT(CHAR(10),@DataFim,103)
	,bo.obs
	,obs2					= ''
	,u_ndutavi = '19000101'
FROM
	sl (NOLOCK)
	INNER JOIN fprod (NOLOCK) on sl.ref = fprod.cnp AND fprod.psico=1
	INNER JOIN bi (NOLOCK) on sl.bistamp = bi.bistamp
	INNER JOIN bo (NOLOCK) on bo.bostamp = bi.bostamp
	inner join empresa_arm (nolock) on empresa_arm.armazem = bi.armazem 
	INNER JOIN st (NOLOCK) on st.ref = bi.ref AND st.site_nr = empresa_arm.empresa_no
	--INNER JOIN st (NOLOCK) on st.ref = bi.ref AND st.site_nr = bi.armazem
	INNER JOIN empresa (NOLOCK) on empresa.no = st.site_nr
	LEFT JOIN ts (NOLOCK) on ts.ndos = bi.ndos
WHERE 
	(bo.dataobra Between @DataIni And @DataFim) 
	AND bi.u_psicont > 0
	AND sl.cmdesc NOT LIKE '%Entrada p/trf%'
	AND sl.armazem IN (select armazem from empresa_arm(NOLOCK) INNER JOIN empresa(NOLOCK) 
							ON empresa.no = empresa_arm.empresa_no WHERE site= @site)
ORDER BY 
	fi.u_psicont

GO
Grant Execute On dbo.up_psico_saidasPsico to Public
Grant Control On dbo.up_psico_saidasPsico to Public
go