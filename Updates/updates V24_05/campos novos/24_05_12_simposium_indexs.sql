/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[simposiumResp]') AND name = N'IX_simposiumResp_tokenInteraction')
CREATE NONCLUSTERED INDEX [IX_simposiumResp_tokenInteraction] ON [dbo].simposiumResp
(
	[tokenInteraction] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[simposiumResp]') AND name = N'IX_simposiumResp_tokenFoodInteraction')
CREATE NONCLUSTERED INDEX [IX_simposiumResp_tokenFoodInteraction] ON [dbo].simposiumResp
(
	[tokenFoodInteraction] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[simposiumResp]') AND name = N'IX_simposiumResp_tokenPhysioAlert')
CREATE NONCLUSTERED INDEX [IX_simposiumResp_tokenPhysioAlert] ON [dbo].simposiumResp
(
	[tokenPhysioAlert] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[simposiumResp]') AND name = N'IX_simposiumResp_tokenConflict')
CREATE NONCLUSTERED INDEX [IX_simposiumResp_tokenConflict] ON [dbo].simposiumResp
(
	[tokenConflicts] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[simposiumResp]') AND name = N'IX_simposiumResp_stampDrugInfo')
CREATE NONCLUSTERED INDEX [IX_simposiumResp_stampDrugInfo] ON [dbo].simposiumResp
(
	[stampDrugInfo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[simposiumRespInteractions]') AND name = N'IX_simposiumRespInteractions_token')
CREATE NONCLUSTERED INDEX [IX_simposiumRespInteractions_token] ON [dbo].simposiumRespInteractions
(
	[token] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO





IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[simposiumRespInteractions]') AND name = N'IX_simposiumRespInteractions_token')
CREATE NONCLUSTERED INDEX [IX_simposiumRespInteractions_token] ON [dbo].simposiumRespInteractions
(
	[token] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[simposiumRespInteractions]') AND name = N'IX_simposiumRespInteractions_tokenItems')
CREATE NONCLUSTERED INDEX [IX_simposiumRespInteractions_tokenItems] ON [dbo].simposiumRespInteractions
(
	[tokenItems] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[simposiumRespInteractionsItems]') AND name = N'IX_simposiumRespInteractionsItems_token')
CREATE NONCLUSTERED INDEX [IX_simposiumRespInteractionsItems_token] ON [dbo].simposiumRespInteractionsItems
(
	[token] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[simposiumRespInteractionsItems]') AND name = N'IX_simposiumRespInteractionsItems_id')
CREATE NONCLUSTERED INDEX [IX_simposiumRespInteractionsItems_id] ON [dbo].simposiumRespInteractionsItems
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

