DECLARE @col1 NVARCHAR(100) = 'painelCentralAtalhosStamp varchar(25) NOT NULL PRIMARY KEY'
DECLARE @col2 NVARCHAR(100) = 'nome VARCHAR(100) NOT NULL CONSTRAINT DF_b_painelCentralAtalhos_nome DEFAULT ('''')'
DECLARE @col3 NVARCHAR(100) = 'comando VARCHAR(100) NOT NULL CONSTRAINT DF_b_painelCentralAtalhos_comando DEFAULT ('''')'
DECLARE @col4 NVARCHAR(100) = 'iconNome VARCHAR(100) NOT NULL CONSTRAINT DF_b_painelCentralAtalhos_iconNome DEFAULT ('''')'
DECLARE @col5 NVARCHAR(100) = 'posicao numeric(1,0) NOT NULL CONSTRAINT DF_b_painelCentralAtalhos_posicao DEFAULT (0)'
DECLARE @col6 NVARCHAR(100) = 'usstamp VARCHAR(25) NOT NULL CONSTRAINT DF_b_painelCentralAtalhos_usstamp DEFAULT ('''')'
DECLARE @col7 NVARCHAR(100) = 'ousrdata DATETIME NOT NULL CONSTRAINT DF_b_painelCentralAtalhos_ousrdata DEFAULT GETDATE()'

-- Concatenate columns into a single definition
DECLARE @columns NVARCHAR(MAX) = @col1 + ',' + @col2 + ',' + @col3 + ',' + @col4 + ',' + @col5 + ',' + @col6 + ',' + @col7



/*

	Create Table Using Columns

*/
DECLARE @tableName NVARCHAR(50) = 'b_painelCentralAtalhos'



IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = @tableName)
BEGIN
    EXEC('
        CREATE TABLE dbo.' + @tableName + ' (
            ' + @columns + '
        )
    ')
    PRINT 'Table ' + @tableName + ' created successfully.'
END
ELSE
BEGIN
    PRINT 'Table ' + @tableName + ' already exists.'
END
GO