
IF NOT EXISTS (
    SELECT 1 
    FROM sys.triggers 
    WHERE name = 'Tr_St_Insert'
)
BEGIN
    EXEC('
    CREATE TRIGGER Tr_St_Insert
       ON ST  
       FOR INSERT
    AS
    BEGIN
        -- Declaração de variáveis
        DECLARE @site_nr TINYINT;
        DECLARE @ref VARCHAR(50);

        -- Obtém o site_nr da tabela inserted
        SET @site_nr = (SELECT TOP 1 site_nr FROM inserted);

        IF @site_nr IS NULL
        BEGIN
            SET @site_nr = ISNULL((SELECT TOP 1 site_nr FROM deleted), 1);
        END;

        -- Evita conjuntos de resultados adicionais
        SET NOCOUNT ON;

        -- Cria um cursor para iterar sobre os registros de "inserted"
        PRINT ''Processando registros inseridos'';
        DECLARE curInserted CURSOR LOCAL FOR
        SELECT ref FROM inserted;

        OPEN curInserted;
        FETCH NEXT FROM curInserted INTO @ref;

        WHILE @@FETCH_STATUS = 0
        BEGIN
            -- Verifica se a referência já foi processada
            IF NOT EXISTS (SELECT 1 FROM TriggerExecutionLog WHERE Ref = @ref)
            BEGIN
                PRINT ''Processando referência: '' + @ref;

                -- Registra a execução na tabela de controle
                INSERT INTO TriggerExecutionLog (Ref) VALUES (@ref);

                -- Executa a lógica principal
                EXEC up_stocks_CorrigeSA_ref @site_nr, @ref;
            END;

            FETCH NEXT FROM curInserted INTO @ref;
        END;

        CLOSE curInserted;
        DEALLOCATE curInserted;

        -- Limpa a tabela de execução (opcional, dependendo do cenário)
        DELETE FROM TriggerExecutionLog;

        PRINT ''Gatilho concluído.'';
    END;
    ');
END;
