
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'cptorg'
	,@columnName		= 'permiteCalcCompart'
	,@columnType		= 'bit'
	,@columnDefault		= '(''1'')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go



DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'cptpla'
	,@columnName		= 'permiteMultiplasRefsCompart'
	,@columnType		= 'bit'
	,@columnDefault		= '(''1'')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go



DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'fi_compart'
	,@columnName		= 'qttminvd'
	,@columnType		= 'numeric(16,2)'
	,@columnDefault		= '(0)'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go


DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'fi_compart'
	,@columnName		= 'qttembal'
	,@columnType		= 'numeric(16,2)'
	,@columnDefault		= '(0)'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go



DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'cptorg'
	,@columnName		= 'entPorLoja'
	,@columnType		= 'bit'
	,@columnDefault		= '(''0'')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go


DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'cptorg'
	,@columnName		= 'entPorLoja'
	,@columnType		= 'bit'
	,@columnDefault		= '(''0'')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go






DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'cptorg'
	,@columnName		= 'prestacao'
	,@columnType		= 'bit'
	,@columnDefault		= '(''0'')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go





DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'cptorg'
	,@columnName		= 'u_estab'
	,@columnType		= 'int'
	,@columnDefault		= '(''0'')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go




DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'cptorg'
	,@columnName		= 'entPorLojaAposData'
	,@columnType		= 'date'
	,@columnDefault		= '(''3000-01-01'')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go




DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'cptorg'
	,@columnName		= 'pemh'
	,@columnType		= 'bit'
	,@columnDefault		= '(0)'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go





DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'cptorg'
	,@columnName		= 'permiteCalcCompart'
	,@columnType		= 'bit'
	,@columnDefault		= '(''1'')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go



DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'cptpla'
	,@columnName		= 'permiteMultiplasRefsCompart'
	,@columnType		= 'bit'
	,@columnDefault		= '(''1'')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go





DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'fi_compart'
	,@columnName		= 'qttminvd'
	,@columnType		= 'numeric(16,2)'
	,@columnDefault		= '(0)'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go


DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'fi_compart'
	,@columnName		= 'qttembal'
	,@columnType		= 'numeric(16,2)'
	,@columnDefault		= '(0)'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go


DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'cptorg'
	,@columnName		= 'motivoAnulacaoDefeitoId'
	,@columnType		= 'int'
	,@columnDefault		= '(0)'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go



DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'cptorg'
	,@columnName		= 'motivoAnulacaoDescricao'
	,@columnType		= 'varchar(254)'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go







