/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'paymentMethodRequest'
	,@columnName		= 'officeKey'
	,@columnType		= 'varchar (254)'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go


/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'paymentMethodRequest'
	,@columnName		= 'channel'
	,@columnType		= 'varchar (254)'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go


/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'paymentMethodRequest'
	,@columnName		= 'fractionalPayment'
	,@columnType		= 'bit'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go

/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'paymentMethodRequest'
	,@columnName		= 'currency'
	,@columnType		= 'varchar (254)'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go

/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'paymentMethodRequest'
	,@columnName		= 'countryCode'
	,@columnType		= 'varchar (254)'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go


/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'paymentMethodRequest'
	,@columnName		= 'sendSMS'
	,@columnType		= 'bit'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go


/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'paymentMethodResponse'
	,@columnName		= 'transactionID'
	,@columnType		= 'varchar(254)'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go



/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'paymentMethodRequest'
	,@columnName		= 'errorUrl'
	,@columnType		= 'varchar(254)'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go



/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'paymentMethodRequest'
	,@columnName		= 'successUrl'
	,@columnType		= 'varchar(254)'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go



/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'paymentMethodRequest'
	,@columnName		= 'cancelUrl'
	,@columnType		= 'varchar(254)'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go


/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'paymentMethodRequest'
	,@columnName		= 'reason'
	,@columnType		= 'varchar(254)'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go


/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'paymentMethodRequest'
	,@columnName		= 'trxId'
	,@columnType		= 'varchar(254)'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go


/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'paymentMethodRequest'
	,@columnName		= 'iban'
	,@columnType		= 'varchar(254)'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go


/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'paymentMethodRequest'
	,@columnName		= 'bic'
	,@columnType		= 'varchar(254)'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go


/*

	Alter table - Add column

*/
DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'paymentMethodResponse'
	,@columnName		= 'trxId'
	,@columnType		= 'varchar(254)'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end

go