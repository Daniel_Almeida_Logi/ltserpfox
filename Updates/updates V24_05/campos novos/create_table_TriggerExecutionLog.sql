IF NOT EXISTS (
    SELECT 1
    FROM sys.objects
    WHERE object_id = OBJECT_ID(N'dbo.TriggerExecutionLog')
     
)
BEGIN
    CREATE TABLE dbo.TriggerExecutionLog (
        Ref VARCHAR(50) PRIMARY KEY,
        ExecutionTime DATETIME DEFAULT GETDATE()
    );
END;