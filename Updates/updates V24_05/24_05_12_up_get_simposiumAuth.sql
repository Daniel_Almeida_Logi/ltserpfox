/****** 
	Object:  up_get_simposiumAuth
	exec [dbo].[up_get_simposiumAuth]   'Loja 1'
***/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_simposiumAuth]') IS NOT NULL
    drop procedure dbo.[up_get_simposiumAuth]
go
create procedure dbo.[up_get_simposiumAuth]
@site as varchar(36)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	declare @username VARCHAR(100)
	declare @pw VARCHAR(100)
	declare @acessToken VARCHAR(MAX)

	set @username = (select textValue from B_Parameters_site where stamp='ADM0000000193' and site=@site)
	set @pw = (select textValue from B_Parameters_site where stamp='ADM0000000194' and site=@site)
	SET @acessToken =( SELECT top 1 acessToken  FROM servicesAuthentication (NOLOCK) where endDateToken>= DATEADD(HH,2,GETDATE())  and site=@site order by endDateToken desc)


	SELECT ISNULL(@acessToken,'') as acessToken ,  @username as username , @pw as pw 

	

GO
Grant Execute On dbo.[up_get_simposiumAuth] to Public
Grant Control On dbo.[up_get_simposiumAuth] to Public
GO



