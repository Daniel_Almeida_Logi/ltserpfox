
--myColorMenu ADM0000000202	
--myColorBarraUser = ADM0000000333
--myColorPagina = ADM0000000235
update B_Parameters
set textValue = '247,247,247'
 where stamp = 'ADM0000000235'
------------------------myColorBotao-----------------
IF not EXISTS (SELECT 1 FROM B_Parameters WHERE stamp='ADM0000000378')
	BEGIN
		insert into B_Parameters (stamp, name, Type, textValue,
				 numValue, bool, ODate, LDate, visivel, MostraTextValue, MostraNumValue, MostraBool,
				  ListaTextValue, Unidades, obs)
		values ('ADM0000000378', 'Cor Botão', 'Cores', '0,167,231',
				 0, 0, getdate(), getdate(), 0, 1, 0, 0,
				  '', '', 'Cor da Barra Principal do Software')
	END


------------------------myColorBarra-----------------		
IF not EXISTS (SELECT 1 FROM B_Parameters WHERE stamp='ADM0000000379')
	BEGIN
		insert into B_Parameters (stamp, name, Type, textValue,
				 numValue, bool, ODate, LDate, visivel, MostraTextValue, MostraNumValue, MostraBool,
				  ListaTextValue, Unidades, obs)
		values ('ADM0000000379', 'Cor Pagina Principal', 'Cores', '0,20,34',
				 0, 0, getdate(), getdate(), 0, 1, 0, 0,
				  '', '', 'Cor da Barra Principal do Software')
	END



------------------------myColorSucesso---------------
IF not EXISTS (SELECT 1 FROM B_Parameters WHERE stamp='ADM0000000380')
	BEGIN
		insert into B_Parameters (stamp, name, Type, textValue,
				 numValue, bool, ODate, LDate, visivel, MostraTextValue, MostraNumValue, MostraBool,
				  ListaTextValue, Unidades, obs)
		values ('ADM0000000380', 'Cor Sucesso', 'Cores', '102,206,119',
				 0, 0, getdate(), getdate(), 0, 1, 0, 0,
				  '', '', 'Cor de Sucesso do Software')
	END
------------------------myColorAviso-----------------
IF not EXISTS (SELECT 1 FROM B_Parameters WHERE stamp='ADM0000000381')
	BEGIN
		insert into B_Parameters (stamp, name, Type, textValue,
				 numValue, bool, ODate, LDate, visivel, MostraTextValue, MostraNumValue, MostraBool,
				  ListaTextValue, Unidades, obs)
		values ('ADM0000000381', 'Cor Aviso', 'Cores', '255,191,107',
				 0, 0, getdate(), getdate(), 0, 1, 0, 0,
				  '', '', 'Cor de Aviso do Software')
	END
------------------------myColorErro------------------
IF not EXISTS (SELECT 1 FROM B_Parameters WHERE stamp='ADM0000000382')
	BEGIN
		insert into B_Parameters (stamp, name, Type, textValue,
				 numValue, bool, ODate, LDate, visivel, MostraTextValue, MostraNumValue, MostraBool,
				  ListaTextValue, Unidades, obs)
		values ('ADM0000000382', 'Cor Erro', 'Cores', '255,94,91',
				 0, 0, getdate(), getdate(), 0, 1, 0, 0,
				  '', '', 'Cor da Barra Principal do Software')
	END


------------------------myColorPaginaAtual-----------------		
IF not EXISTS (SELECT 1 FROM B_Parameters WHERE stamp='ADM0000000383')
	BEGIN
		insert into B_Parameters (stamp, name, Type, textValue,
				 numValue, bool, ODate, LDate, visivel, MostraTextValue, MostraNumValue, MostraBool,
				  ListaTextValue, Unidades, obs)
		values ('ADM0000000383', 'Cor Pagina Principal', 'Cores', '0,77,133',
				 0, 0, getdate(), getdate(), 0, 1, 0, 0,
				  '', '', 'Cor da Barra Principal do Software')
	END

