
/****** 
	Object:  StoredProcedure [dbo].[up_documentos_infoCompartHeader] 
	exec [dbo].[up_documentos_infoCompartHeader]   'ADM3280B948-3FDD-45E7-8F4'
***/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_documentos_infoCompartHeader]') IS NOT NULL
    drop procedure dbo.[up_documentos_infoCompartHeader]
go
create procedure dbo.[up_documentos_infoCompartHeader]


@token as varchar(50)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

    SELECT
		 ft_compart.efrid,
		 ft_compart.siteno,
		 ft_compart.ftstamp,
		 ft_compart.cardNumber,
		 ft_compart.[type],
		 ft_compart.contributionCorrelationId,
		 ft_compart.currency,
		 ft_compart.codacesso,
		 ft_compart.codExtInsurance,
		 ft_compart.abrevInsurance,
		 ft_compart.ousrdata,
		 ft_compart.ousrUser,
		 ft_compart.invoiceNumber,
		 ft_compart.docData,
		 ft_compart.programId,
		 ft_compart.inclIva,
		 ft_compart.commercialDiscount,
		 ft_compart.incidenceBasis,
		 ft_compart.totalPercIva,
		 ft_compart.financialDiscount,
	     ft_compart.totalValueDocument,
		 ft_compart.idmotive,
		 ft_compart.nif,
		 ft_compart.beneficiaryName
    FROM
		ft_compart (NOLOCK) 
    WHERE
		ft_compart.token = @token     
GO
Grant Execute On dbo.[up_documentos_infoCompartHeader] to Public
Grant Control On dbo.[up_documentos_infoCompartHeader] to Public
GO
