 
 DECLARE @site VARCHAR(20)
declare emp_cursor cursor for
select site as siteemp from empresa

open emp_cursor
fetch next from emp_cursor into @site

while @@FETCH_STATUS = 0
begin
	IF not EXISTS (SELECT 1 FROM B_Parameters_site WHERE stamp='ADM0000000225' and site=@site)
	BEGIN
		insert into B_Parameters_site (stamp, name, Type, textValue,
				 numValue, bool, ODate, LDate, visivel, MostraTextValue, MostraNumValue, MostraBool,
				  ListaTextValue, Unidades, Site, obs)
		values ('ADM0000000225', 'Valida interações de medicamentos via Api Simpósio', 'SIMPOSIUM', '',
				 0, 1, getdate(), getdate(), 0, 0, 0, 1,
				  'Bool = 0 - Não valida; Bool = 1 - Valida', '', @site, 'Valida interações de medicamentos via Api Simposium no backoffice e frontOffice')
	END
	fetch next from emp_cursor into @site
end
close emp_cursor
deallocate emp_cursor


GO

 
 DECLARE @site VARCHAR(20)
declare emp_cursor cursor for
select site as siteemp from empresa

open emp_cursor
fetch next from emp_cursor into @site

while @@FETCH_STATUS = 0
begin
	IF not EXISTS (SELECT 1 FROM B_Parameters_site WHERE stamp='ADM0000000226' and site=@site)
	BEGIN
		insert into B_Parameters_site (stamp, name, Type, textValue,
				 numValue, bool, ODate, LDate, visivel, MostraTextValue, MostraNumValue, MostraBool,
				  ListaTextValue, Unidades, Site, obs)
		values ('ADM0000000226', 'O número de meses de histórico a ser considerado para a validação de interações de medicamentos via API do Simpósio', 'SIMPOSIUM', '',
				 1, 0, getdate(), getdate(), 0, 0, 0, 1,
				  'num = 1', ' Valor em meses', @site, 'Valida interações de medicamentos via Api Simposium no backoffice e frontOffice')
	END
	fetch next from emp_cursor into @site
end
close emp_cursor
deallocate emp_cursor
