/* 1 Tabelas e campos que n�o existem na BD do cliente*/
select 
	* 
from 
	dicionarioDados 
	left join INFORMATION_SCHEMA.COLUMNS on TABLE_NAME = tabela and COLUMN_NAME = campo
where
	TABLE_NAME is null



/* 2 Tabelas existem na BD e nao existem no dicionario de Dados*/
/* Ac��o Eliminar Tabela */
select 
	distinct TABLE_NAME 
from 
	INFORMATION_SCHEMA.COLUMNS
where
	TABLE_NAME not in (select tabela from dicionarioDados)



/* 3 Diferen�as Tipos */
select 
	tabela = TABLE_NAME
	,campo = COLUMN_NAME
	,tipoDicDados = tipo 
	,tipoBdCliente = CASE 
			WHEN DATA_TYPE = 'varchar' or DATA_TYPE = 'char' then DATA_TYPE + '(' + convert(varchar,CHARACTER_MAXIMUM_LENGTH) + ')'
			WHEN DATA_TYPE = 'numeric' then DATA_TYPE + '(' + convert(varchar,NUMERIC_PRECISION) + ',' + convert(varchar,NUMERIC_SCALE) + ')' 
			ELSE DATA_TYPE 
		END
	,defaultDicDados =  valordefaullt
	,defaultBdCliente = isnull(COLUMN_DEFAULT,'')
from 
	INFORMATION_SCHEMA.COLUMNS
	inner join dicionarioDados on TABLE_NAME = tabela and COLUMN_NAME = campo
where 
	LEFT(TABLE_NAME,4) != 'temp'
	and 
	tipo !=
		CASE 
			WHEN DATA_TYPE = 'varchar' or DATA_TYPE = 'char' then DATA_TYPE + '(' + convert(varchar,CHARACTER_MAXIMUM_LENGTH) + ')'
			WHEN DATA_TYPE = 'numeric' then DATA_TYPE + '(' + convert(varchar,NUMERIC_PRECISION) + ',' + convert(varchar,NUMERIC_SCALE) + ')'
			ELSE DATA_TYPE 
		END
	or valordefaullt != isnull(COLUMN_DEFAULT,'')
