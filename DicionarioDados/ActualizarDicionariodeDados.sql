/* Actualizar tabela de dados */


/* 1 - Apagar as tabelas e campos que j� n�o existem */
select *
--delete dicionarioDados
from 
	dicionarioDados 
	left join INFORMATION_SCHEMA.COLUMNS on TABLE_NAME = tabela and COLUMN_NAME = campo
where
	TABLE_NAME is null
	
	


/* 2 - criar registos novos no dicionario */
insert into dicionarioDados
select 
	tabela = TABLE_NAME
	,campo = COLUMN_NAME
	,tipo =  
		CASE 
			WHEN DATA_TYPE = 'varchar' or DATA_TYPE = 'char' then DATA_TYPE + '(' + convert(varchar,CHARACTER_MAXIMUM_LENGTH) + ')'
			WHEN DATA_TYPE = 'numeric' then DATA_TYPE + '(' + convert(varchar,NUMERIC_PRECISION) + ',' + convert(varchar,NUMERIC_SCALE) + ')'
			ELSE DATA_TYPE
		END
	,valordefault = isnull(COLUMN_DEFAULT,'')
	,descricao = ''
	,acao = ''
from 
	INFORMATION_SCHEMA.COLUMNS
	left join dicionarioDados on TABLE_NAME = tabela and COLUMN_NAME = campo
where 
	campo is null
	and LEFT(TABLE_NAME,4) != 'temp'
Order BY tabela
	


/* 3 - Diferen�as de tipos */

update dicionarioDados
set 
	valordefaullt = isnull(COLUMN_DEFAULT,'')
	,tipo = CASE 
			WHEN DATA_TYPE = 'varchar' or DATA_TYPE = 'char' then DATA_TYPE + '(' + convert(varchar,CHARACTER_MAXIMUM_LENGTH) + ')'
			WHEN DATA_TYPE = 'numeric' then DATA_TYPE + '(' + convert(varchar,NUMERIC_PRECISION) + ',' + convert(varchar,NUMERIC_SCALE) + ')'
			ELSE DATA_TYPE
		END
	

--select 
--	tabela = TABLE_NAME
--	,campo = COLUMN_NAME
--	,tipo =  
--		CASE 
--			WHEN DATA_TYPE = 'varchar' or DATA_TYPE = 'char' then DATA_TYPE + '(' + convert(varchar,CHARACTER_MAXIMUM_LENGTH) + ')' 
--			WHEN DATA_TYPE = 'numeric' then DATA_TYPE + '(' + convert(varchar,NUMERIC_PRECISION) + ',' + convert(varchar,NUMERIC_SCALE) + ')'
--			ELSE DATA_TYPE 
--		END
	
--	,descricao = ''
--	,acao = ''
--	,tipo 
--	,CASE 
--			WHEN DATA_TYPE = 'varchar' or DATA_TYPE = 'char' then DATA_TYPE + '(' + convert(varchar,CHARACTER_MAXIMUM_LENGTH) + ')'
--			WHEN DATA_TYPE = 'numeric' then DATA_TYPE + '(' + convert(varchar,NUMERIC_PRECISION) + ',' + convert(varchar,NUMERIC_SCALE) + ')' 
--			ELSE DATA_TYPE
--		END
--	,valordefaullt
from 
	INFORMATION_SCHEMA.COLUMNS
	inner join dicionarioDados on TABLE_NAME = tabela and COLUMN_NAME = campo
where 
	LEFT(TABLE_NAME,4) != 'temp'
	and 
	tipo !=
		CASE 
			WHEN DATA_TYPE = 'varchar' or DATA_TYPE = 'char' then DATA_TYPE + '(' + convert(varchar,CHARACTER_MAXIMUM_LENGTH) + ')'
			WHEN DATA_TYPE = 'numeric' then DATA_TYPE + '(' + convert(varchar,NUMERIC_PRECISION) + ',' + convert(varchar,NUMERIC_SCALE) + ')'
			ELSE DATA_TYPE 
		END
	or valordefaullt != isnull(COLUMN_DEFAULT,'')


/* Actualizar Informa��o SPS */

--delete from dicionarioDadosSps
--insert into dicionarioDadosSps

SELECT ROUTINE_NAME, ROUTINE_DEFINITION = ISNULL(ROUTINE_DEFINITION,'')
FROM INFORMATION_SCHEMA.ROUTINES 