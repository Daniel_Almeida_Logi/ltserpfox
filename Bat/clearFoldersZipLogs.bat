@echo off
set /p "folder=Folder: "

@echo off
set /p "day=DAY: "

@echo off
forfiles /p "C:\Logitools\Versoes\%folder%\ldata\Cache\QRCODE" /m *.* /D -%day% /C "cmd /c del @path"

@echo off
forfiles /p "C:\Logitools\Versoes\%folder%\ldata\Cache\PDF" /m *.* /D -%day% /C "cmd /c del @path"

@echo off
forfiles /p "C:\Logitools\Versoes\%folder%\ldata\Cache\XML" /m *.* /D -%day% /C "cmd /c del @path"

@echo off
forfiles /p "C:\Logitools\Versoes\%folder%\ldata\Cache\XLS" /m *.* /D -%day% /C "cmd /c del @path"

@echo off
forfiles /p "C:\Logitools\Versoes\%folder%\ldata\Cache" /m *.* /D -%day%  /C "cmd /c del @path"


@echo off
SET F="C:\Logitools\Versoes\%folder%\ldata\Cache\OLA"

@echo off

set "root_dir=C:\Logitools\Versoes\%folder%\ldata\Cache"
set "exclude_list=QRCODE PDF XML XLS"
pushd "%root_dir%"
for /f "skip=10 tokens=* delims=" %%# in ('dir /b /O-D/a:d^| findstr /v "%exclude_list%"') do (
    rd /s /q "%%~f#"
)


@echo off
setlocal EnableExtensions DisableDelayedExpansion

rem // Define constants here:
set "_ROOT=C:\Logitools\Versoes\%folder%\DEM\Logs"
set "_PATTERN=*.*"
set "_LIST=%TEMP%\%~n0.tmp"
set "_ARCHIVER=%ProgramFiles%\7-Zip\7z.exe"

rem // Get current date in locale-independent format:
for /F "tokens=2 delims==" %%D in ('wmic OS get LocalDateTime /VALUE') do set "TDATE=%%D"
set "TDATE=%TDATE:~,8%"

rem // Create a list file containing all files to move to the archive:
> "%_LIST%" (
    for /F "delims=" %%F in ('
        forfiles /S /P "%_ROOT%" /M "%_PATTERN%" /D -%day% /C "cmd /C echo @path"
    ') do echo(%%~F
) && (
    rem // Archive all listed files at once and delete the processed files finally:
    "%_ARCHIVER%" a -sdel "%_ROOT%\_%TDATE%.zip" @"%_LIST%"
    rem // Delete the list file:
    del "%_LIST%"
)

endlocal

@echo off
setlocal EnableExtensions DisableDelayedExpansion

rem // Define constants here:
set "_ROOT=C:\Logitools\Versoes\%folder%\ltsFaturacaoEletronica\Logs"
set "_PATTERN=*.*"
set "_LIST=%TEMP%\%~n0.tmp"
set "_ARCHIVER=%ProgramFiles%\7-Zip\7z.exe"

rem // Get current date in locale-independent format:
for /F "tokens=2 delims==" %%D in ('wmic OS get LocalDateTime /VALUE') do set "TDATE=%%D"
set "TDATE=%TDATE:~,8%"

rem // Create a list file containing all files to move to the archive:
> "%_LIST%" (
    for /F "delims=" %%F in ('
        forfiles /S /P "%_ROOT%" /M "%_PATTERN%" /D -%day% /C "cmd /C echo @path"
    ') do echo(%%~F
) && (
    rem // Archive all listed files at once and delete the processed files finally:
    "%_ARCHIVER%" a -sdel "%_ROOT%\_%TDATE%.zip" @"%_LIST%"
    rem // Delete the list file:
    del "%_LIST%"
)

endlocal
exit /B