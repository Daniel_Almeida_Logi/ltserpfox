USE [LTSMSB]
GO



declare @newId varchar(36) = CONVERT(varchar(36), NEWID()) 
declare @id_lt varchar(20) = 'Ftestes'
declare @obs varchar(20) = 'Migração de testes - Farmacia Pipa - Sifarma Stocks e movimentos'


INSERT INTO [dbo].[migrationCtrl]
           ([stamp]
           ,[user]
           ,[token]
           ,[url]
           ,[comType]
           ,[frequencyMinutes]
           ,[clientId]
           ,[clientDB]
           ,[serverDB]
           ,[site]
           ,[migrationType]
           ,[lastChangeDate]
           ,[lastRunDate]
           ,[obs]
           ,[multiSite]
           ,[folderPath]
           ,[password]
           ,[deleted]
           ,[fileType]
           ,[sqlProcedures]
           ,[port]
           ,[zipFile]
           ,[portDB]
           ,[filenames]
           ,[sqlCreationTable]
           ,[migrationTables]
           ,[fileNameToImport])
		   SELECT top 1 		   
				@newId
			  ,[user]
			  ,[token]
			  ,[url]
			  ,[comType]
			  ,[frequencyMinutes]
			  ,@id_lt
			  ,@id_lt
			  ,[serverDB]
			  ,[site]
			  ,[migrationType]
			  ,getdate()
			  ,[lastRunDate]
			  ,@obs
			  ,[multiSite]
			  ,[folderPath]
			  ,[password]
			  ,[deleted]
			  ,[fileType]
			  ,[sqlProcedures]
			  ,[port]
			  ,[zipFile]
			  ,[portDB]
			  ,[filenames]
			  ,[sqlCreationTable]
			  ,[migrationTables]
			  ,@id_lt + '.zip'
		  FROM [dbo].[migrationCtrl]
		  where stamp='9F4B7FF0-4AAA-45B5-9E5F-C6CE5EA8DF96'
GO


