/* SP inserir anexo
	

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_paymentMethod_createPaymentInfo]') IS NOT NULL
	DROP PROCEDURE dbo.up_paymentMethod_createPaymentInfo
GO

CREATE PROCEDURE dbo.up_paymentMethod_createPaymentInfo

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON
	If OBJECT_ID('tempdb.dbo.#payment') IS NOT NULL
		DROP TABLE #payment


	-- expirados
	update [b_pagCentral_ext] set statePaymentID=-3 , statePaymentIDDesc='Expirado' where   statePaymentID =0  and limitDate != '1900-01-01 00:00:00.000' and  limitDate<= getdate()

	-- Passou a data MBWAY
	update [b_pagCentral_ext] set statePaymentID=-3 , statePaymentIDDesc='Expirado' where   statePaymentID =0  and limitDate = '1900-01-01 00:00:00.000' and   DATEDIFF (day, ousrdata, GETDATE()) >60

	SELECT 
		LEFT(newid(),36) AS token,
		operationId        AS operationId,
		typePayment	       AS typePayment,
		typePaymentDesc    AS typePaymentDesc,
		statePaymentID     AS statePaymentID, 
		statePaymentIDDesc AS statePaymentIDDesc,	 
		receiverid		   AS receiverid,
		receivername       AS receivername,
		site               AS site ,
		(select textValue from B_Parameters_site where stamp='ADM0000000187' and B_Parameters_site.site=[b_pagCentral_ext].site)              AS  username,-- buscar no parametro 
		(select textValue from B_Parameters_site where stamp='ADM0000000188' and B_Parameters_site.site=[b_pagCentral_ext].site)           AS password,-- buscar no parametro 
		(select textValue from B_Parameters_site where stamp='ADM0000000189' and B_Parameters_site.site=[b_pagCentral_ext].site)           AS entityType,-- buscar no parametro 
		(select bool from B_Parameters  where stamp='ADM0000000227') as test -- buscar no parametro 
	INTO #payment
	FROM [b_pagCentral_ext]
	WHERE statePaymentID =0  and (limitDate>=getdate()+1 or limitDate ='1900-01-01 00:00:00.000')

	insert into paymentMethodRequest(token,username,[password],typeID,typeIDdesc,typePayment,typePaymentDesc,receiverid,receivername,entityType,amount
				,[description],email,phone,clientIdNumber,externalReference,[name],categoryId,callBackURL,origin,additionalInfo,
				[address],postCode,city,nic,idUserBackoffice,timeLimitDays,sendEmail,operationId,startDate,endDate,typeGetInfo,[site],test)
		select token,username,password,2,'INFOPAYMENT',typePayment,typePaymentDesc,receiverid,receivername,entityType,0,
					'','','','','','',0,'','','',
					'','','','',0,0,0,operationId,'','','',site,1 
		from #payment

	SELECT
		token
	from #payment


	If OBJECT_ID('tempdb.dbo.#payment') IS NOT NULL
		DROP TABLE #payment

GO
GRANT EXECUTE on dbo.up_paymentMethod_createPaymentInfo TO PUBLIC
GRANT Control on dbo.up_paymentMethod_createPaymentInfo TO PUBLIC
GO

 