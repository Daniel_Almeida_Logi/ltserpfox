/* Save  PaymentMethods Response 
	

	exec up_update_PaymentMethods_b_pagCentral_ext

*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_update_PaymentMethods_b_pagCentral_ext]') IS NOT NULL
	drop procedure dbo.up_update_PaymentMethods_b_pagCentral_ext
go

create procedure [dbo].[up_update_PaymentMethods_b_pagCentral_ext]
	@operationId		[varchar](100),
	@statusCode			[varchar](254),
	@statusDescription	[varchar](max),
	@paid				bit,
	@trxId				[varchar](254)



/* with encryption */
AS
BEGIN	

	DECLARE @statePaymentID int =0 
	DECLARE @statePaymentIDDesc varchar(254) ='Aguarda Pagamento' 

	IF(@paid=1 and @statusCode !='2')
	BEGIN
		set @statePaymentID =1 
		set @statePaymentIDDesc ='Pago' 
		set @statusCode=1
		set @statusDescription='Conclu�do'
	END  
	ELSE if (@statusDescription!='')
	BEGIN
		select   top 1 @statePaymentID=isnull(type,0), @statePaymentIDDesc=isnull(typeDesc,'Aguarda Pagamento')  from [paymentCode_ext] where codeDesc=@statusDescription 
	END 

	UPDATE [b_pagCentral_ext] SET 
			[status] = @statusCode,
			[statusDesc] = @statusDescription,
			statePaymentID = @statePaymentID , 
			statePaymentIDDesc = @statePaymentIDDesc,
			[usrinis] = 'ADM',
			[usrdata] = GETDATE(),
			[trxId]= @trxId
	WHERE operationId =@operationId and ((limitDate != '1900-01-01 00:00:00.000' and  limitDate>=getdate()) or (limitDate = '1900-01-01 00:00:00.000' and  DATEDIFF (day, ousrdata, GETDATE()) <60 ) )
 
 	-- expirados
	update [b_pagCentral_ext] set statePaymentID=-3 , statePaymentIDDesc='Expirado' where operationId =@operationId and   statePaymentID =0  and limitDate != '1900-01-01 00:00:00.000' and  limitDate<= getdate()

	-- Passou a data MBWAY
	update [b_pagCentral_ext] set statePaymentID=-3 , statePaymentIDDesc='Expirado' where  operationId =@operationId and  statePaymentID =0  and limitDate = '1900-01-01 00:00:00.000' and   DATEDIFF (day, ousrdata, GETDATE()) >60

END

GO
Grant Execute on dbo.up_update_PaymentMethods_b_pagCentral_ext to Public
Grant control on dbo.up_update_PaymentMethods_b_pagCentral_ext to Public
GO