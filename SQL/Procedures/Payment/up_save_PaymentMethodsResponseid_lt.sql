/* Save  PaymentMethods Response 
	

	exec up_save_PaymentMethodsResponse

*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_PaymentMethodsResponseid_lt]') IS NOT NULL
	drop procedure dbo.up_save_PaymentMethodsResponseid_lt
go

create procedure [dbo].[up_save_PaymentMethodsResponseid_lt]
	@token				[varchar](36),
	@statusId			int,
	@statusDesc			[varchar](254),
	@errorId			int, 
	@errorDesc			[varchar](254), 
	@operationId		[varchar](100),
	@status				[varchar](100),
	@statusCode			[varchar](254),
	@statusDescription	[varchar](max),
	@statusDescriptionDetail  [varchar](max),
	@entityType			[varchar](50),
	@amount				NUMERIC(20,2),
	@categoryId			NUMERIC(10,0),
	@description		[varchar](max),
	@phone				[varchar](50),
	@requestDate		[datetime],
	@statusDate			[datetime],
	@validRequest		bit ,
	@paid				bit,
	@lastPaymentDate	[varchar](50),
	@totalPayments		numeric(5,0),
	@transactionID		[varchar](254),
	@trxId				[varchar](254)


/* with encryption */
AS
BEGIN	


	 IF (not exists(select 1 from paymentMethodResponse where token=@token))
	 BEGIN
		Insert into paymentMethodResponse(token,statusId,statusDesc,errorId,errorDesc,operationId,status,statusCode,statusDescription,
							statusDescriptionDetail,entityType,amount,categoryId,description,phone,requestDate,statusDate,validRequest,paid,
							lastPaymentDate,totalPayments,ousrinis,ousrdata,usrinis,usrdata,transactionID,trxId)
		values(@token,@statusId,@statusDesc,@errorId,@errorDesc,@operationId,@status,@statusCode,@statusDescription,
							@statusDescriptionDetail,@entityType,@amount,@categoryId,@description,@phone,@requestDate,@statusDate,@validRequest,@paid,
							@lastPaymentDate,@totalPayments,'ADM',GETDATE(),'ADM',GETDATE(),@transactionID,@trxId)
	END


END

GO
Grant Execute on dbo.up_save_PaymentMethodsResponseid_lt to Public
Grant control on dbo.up_save_PaymentMethodsResponseid_lt to Public
GO