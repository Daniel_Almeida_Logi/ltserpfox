/* 
	Alphega COMPRAS
	Devolve informacao de linhas das Compras e devoluções a fornecedor
		
	exec up_alphegaComprasLinhas '20210422','Loja 1'




*/

SET ANSI_NULLS OFF 
GO
SET QUOTED_IDENTIFIER OFF
GO



if OBJECT_ID('[dbo].[up_alphegaComprasLinhas]') IS NOT NULL
	drop procedure dbo.up_alphegaComprasLinhas
go

create procedure dbo.up_alphegaComprasLinhas


@data as datetime,
@site  as varchar(254)

/* WITH ENCRYPTION */ 

AS
BEGIN



	DECLARE @site_nr INT
	SELECT  @site_nr=no from empresa(nolock) where site = @site

	declare @seconds int = 5
	

	Select
		codProd 
		,qt
		,bonus
		,posStock      
		,Pvf     
		,comprasId
	From(
			SELECT 
			 codProd ='codProd' 
			,qt      ='qt'
			,bonus	 ='bonus'
			,posStock='posStock'      
			,Pvf     ='Pvf'     
			, comprasId ='comprasId'
			,numLinCpr   = -2
			, data = '19000101'
		UNION ALL

		Select
			codProd     = fn.ref
			,qt          = convert(varchar,convert(int,fn.qtt) - convert(int,u_bonus))
			,bonus		 = convert(varchar,convert(int,u_bonus))
			,posStock			= convert(varchar,convert(int,
									(ISNULL((
										SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
										FROM	sl (nolock)
										WHERE	ref = fn.REF AND    convert(datetime,convert(varchar(10),sl.ousrdata,120)+' ' + sl.ousrhora)   <=  Dateadd(ss,@seconds,convert(datetime,convert(varchar(10),fo.ousrdata,120)+' ' + fo.ousrhora)) 
										and  armazem in  (select armazem from empresa_arm(nolock) where empresa_no = @site_nr) 
									),0) ))
							)
			,Pvf         = convert(varchar,convert(int,fn.etiliquido/qtt*100))
			,comprasId    =  fo.fostamp
			,numLinCpr   = lordem
			,data = fn.ousrdata + fn.ousrhora
		From
			empresa	lojas (nolock)
			inner join fo (nolock)  on fo.site = Lojas.site
			inner join fn (nolock)  on fn.fostamp = fo.fostamp
			inner join st (nolock)  on st.ref = fn.ref and st.site_nr = @site_nr 
			inner join cm1 (nolock) on cm1.cm = fo.doccode
		
		Where
			Lojas.site	    = @site
			and fo.ousrdata	 = @data
			and cm1.FOLANSL = 1
			and fo.site     = @site
			and (ltrim(rtrim(isnull(fn.ref,'')))!='' or  ltrim(rtrim(isnull(fn.oref,'')))!='')
			and st.inactivo = 0
			and fn.qtt>0
	


		UNION ALL

		Select
			 codProd     = bi.ref
			,qt          = convert(varchar,case  when bo.nmdos like '%Devol%' then  abs(convert(int,bi.qtt))*-1 else convert(int,bi.qtt) end - case when bo.nmdos like '%Devol%' then  abs(convert(int,bi.u_bonus))*-1 else convert(int,bi.u_bonus) end)
			,bonus       =convert(varchar,case when bo.nmdos like '%Devol%' then  abs(convert(int,bi.u_bonus))*-1 else convert(int,bi.u_bonus) end)
			,posStock			= convert(varchar,convert(int,
									(ISNULL((
										SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
										FROM	sl (nolock)
										WHERE	ref = bi.REF AND    convert(datetime,convert(varchar(10),sl.ousrdata,120)+' ' + sl.ousrhora)   <=   Dateadd(ss,@seconds,convert(datetime,convert(varchar(10),bo.ousrdata,120)+' ' + bo.ousrhora)) 
										and armazem in  (select armazem from empresa_arm(nolock) where empresa_no = @site_nr) 
									),0) ))
							) 
			,Pvf         = convert(varchar,convert(int,bi.ettdeb/qtt*100))
			,comprasId   =  bo.bostamp
			,numLinCpr   = lordem
			,data = bi.ousrdata + bi.ousrhora
		From
			empresa lojas (nolock)
			inner join bo (nolock) on bo.site = Lojas.site
			inner join bi (nolock) on bi.bostamp = bo.bostamp
			inner join st (nolock) on st.ref = bi.ref and st.site_nr = @site_nr
			inner join ts (nolock) on ts.ndos = bo.ndos
	
		Where
			Lojas.site	= @site
			and bo.ousrdata = @data 
			and ts.STOCKS =	1
			and bo.ndos!=35
			and ts.bdempresas = 'FL'
			and bo.site  = @site
			and ltrim(rtrim(isnull(bi.ref,'')))!=''
			and st.inactivo = 0
			and bi.qtt>0
		
	 ) a
		order by a.data asc,
			a.numLinCpr asc
	

END

Go
Grant Execute on dbo.up_alphegaComprasLinhas to Public
Grant control on dbo.up_alphegaComprasLinhas to Public
Go


