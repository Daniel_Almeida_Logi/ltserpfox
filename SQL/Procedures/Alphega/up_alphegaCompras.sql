/* 
	Alphega COMPRAS
	Devolve informação dos cabecalhos das Compras e devoluções a fornecedor
		
	exec up_alphegaCompras '20210422', 'Loja 1'


	exec up_up_alphegaComprasLinhas '20210422', 'Loja 1'

	select  docdata, data , ousrdata, ousrhora, *  from fo(nolock) where fostamp='NM19DE77C1-26E8-444B-8A7'


	select rdata, ousrdata, ousrHora from fi(nolock) where ref='5745765'  and rdata='2021-04-23 00:00:00.000'
	
*/

SET ANSI_NULLS OFF 
GO
SET QUOTED_IDENTIFIER OFF
GO



if OBJECT_ID('[dbo].[up_alphegaCompras]') IS NOT NULL
	drop procedure dbo.up_alphegaCompras
go


create procedure dbo.up_alphegaCompras

@data as datetime,
@site  as varchar(254)

/* WITH ENCRYPTION */ 

AS
BEGIN
	if Object_id('tempdb.dbo.#movimentosProd') is not null
		drop table #movimentosProd

	Select
		*
		into #movimentosProd
		From (

			Select
			 comprasId			="comprasId"         
			,numCprCC			="numCprCC"           
			,codFarm			="codFarm"	
			,dtCompra			="dtCompra"      		
			,TipoFornecedor		="TipoFornecedor"       
			,tipoCompra			="tipoCompra"      
			,ord	 =1
			union all

		Select
			 comprasId          =  fo.fostamp
			,numCprCC           =  left(convert(varchar,fo.adoc)  + convert(varchar,DATEDIFF(SECOND,{d '1970-01-01'}, fo.ousrdata)),15) 
			,codFarm			=  convert(varchar,lojas.infarmed)	
			,"dtCompra"         =   convert(varchar(10),fo.ousrdata,120)+' ' + fo.ousrhora
			,"TipoFornecedor"   =  ""
			,"tipoCompra"       =  fo.docnome
			,ord				=2
		From
			empresa	lojas (nolock)
			inner join fo (nolock) on fo.site = Lojas.site
			inner join cm1 (nolock) on cm1.cm = fo.doccode

		Where
			Lojas.site	    = @site
			and fo.ousrdata	 = @data
			and cm1.FOLANSL = 1
			and fo.site     = @site
		

		UNION ALL


		Select
			comprasId           = bo.bostamp
			,numCprCC           =  left(convert(varchar,bo.obrano)  + convert(varchar,DATEDIFF(SECOND,{d '1970-01-01'}, bo.ousrdata)),15) 
			,codFarm			=  convert(varchar,lojas.infarmed	)
			,"dtCompra"         =  convert(varchar(10),bo.ousrdata,120)+' ' + bo.ousrhora
			,"TipoFornecedor"   =  ""
			,"tipoCompra"       =  bo.nmdos
			,ord	 =2
		From
			empresa lojas (nolock)
			inner join bo (nolock) on bo.site = Lojas.site
			inner join ts (nolock) on ts.ndos = bo.ndos

		Where
			Lojas.site	= @site
			and bo.ndos!=35
			and bo.ousrdata = @data 
			and ts.STOCKS =	1
			and ts.bdempresas = 'FL'
			and bo.site  = @site
	
		) a


	select 
		comprasId	
		,numCprCC	
		,codFarm
		,dtCompra		
		,TipoFornecedor	
		,tipoCompra	
	from 
		#movimentosProd
	order by ord, dtCompra asc
	
	if Object_id('tempdb.dbo.#movimentosProd') is not null
		drop table #movimentosProd
END

Go
Grant Execute on dbo.up_alphegaCompras to Public
Grant control on dbo.up_alphegaCompras to Public
Go