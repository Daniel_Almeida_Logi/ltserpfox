/*
	Alphega DE DISPENSAS
	Devolve informação do detalhe de dispensa
	exec up_alphegaDetalheDispensasLinhas '20200526','Loja 1'


		exec up_alphegaDetalheDispensasLinhas '20210427','Loja 1'



	select *from ft(nolock) order by ousrdata desc

	
*/






SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_alphegaDetalheDispensasLinhas]') IS NOT NULL
	drop procedure dbo.up_alphegaDetalheDispensasLinhas
go



create procedure dbo.up_alphegaDetalheDispensasLinhas
	@data as datetime
	,@site  as varchar(15)


	
/* WITH ENCRYPTION */

AS
BEGIN
	
	
	DECLARE @site_nr INT
	SELECT  @site_nr=no from empresa(nolock) where site = @site

	declare @seconds int = 5

	Select
		 LinDispensasId 
		 ,dispensasId	
		 ,numLinDisp    
		 ,codProd		
		 ,nomeProd		
		 ,pvp		    
		 ,qt            
		 ,stockMin		
		 ,stockMax		
		 ,posStock		
	From (
		Select
			 LinDispensasId   =	 'LinDispensasId' 
			 ,dispensasId	  =	 'dispensasId'	
			 ,numLinDisp      =	 'numLinDisp'   
			 ,codProd		  =	 'codProd'		
			 ,nomeProd		  =	 'nomeProd'		
			 ,pvp		      =	 'pvp'		    
			 ,qt              =	 'qt'            
			 ,stockMin		  =	 'stockMin'		
			 ,stockMax		  =	 'stockMax'		
			 ,posStock		  =	 'posStock'		
			 ,data			  =	'19000101'		
			 ,ord			  =1


		UNION ALL 

		Select
			 LinDispensasId     = ISNULL(fi.fistamp,'')
			 ,dispensasId	    = ISNULL(ft.ftstamp,'')
		     ,numLinDisp        = ISNULL(convert(VARCHAR,lordem),'')
			 ,codProd		    = ISNULL(RTRIM(LTRIM((case when fi.ref='' then fi.oref else fi.ref end))),'')										
			 ,nomeProd		    = ISNULL(RTRIM(LTRIM(fi.design)) ,'')
			 ,pvp		        = ISNULL(convert(VARCHAR,abs(convert(int,fi.u_epvp*100))),'')						    
			 ,qt                = ISNULL(convert(VARCHAR,convert(int,case when ft.tipodoc=3 then fi.qtt*(-1) else fi.qtt end)),'')
			 ,stockMin			= ISNULL(convert(VARCHAR,convert(int,st.ptoenc)),'')
			 ,stockMax			= ISNULL(convert(VARCHAR,convert(int,st.stmax)),'')
			 ,posStock			= ISNULL(convert(VARCHAR,convert(int,
											(ISNULL((
												SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
												FROM	sl (nolock)
												WHERE	ref = fi.REF AND    convert(datetime,convert(varchar(10),sl.ousrdata,120)+' ' +  sl.ousrhora)   <=  Dateadd(ss,@seconds,convert(datetime,convert(varchar(10),ft.ousrdata,120)+' ' + ft.ousrhora)) 
												and armazem in  (select armazem from empresa_arm(nolock) where empresa_no = @site_nr) 
											),0) ))
									),'')
			
			 ,data				= convert(datetime,convert(varchar(10),ft.ousrdata,120)+' ' + ft.ousrhora)
			 ,ord				=2
		From fi (nolock)
			inner join ft (nolock)	on fi.ftstamp = ft.ftstamp
			left join  ft2 (nolock)	on ft.ftstamp = ft2.ft2stamp
			left  join st (nolock) on st.ref = fi.ref and st.site_nr = @site_nr
			inner join td (nolock)	on td.ndoc = ft.ndoc
		Where
					ft.no >= 200
					and ft.ousrdata = @data
					and ft.site = @site
					AND (FT.tipodoc != 4 or u_tipodoc = 4) AND FT.tipodoc != 3
					AND U_TIPODOC not in (1, 5)
					and td.nmdoc not like '%import%'
				and (fi.ref != '' or fi.oref!='')
	) a
	order by a.ord ASC, 
			 a.data ASC, 
			 a.numLinDisp asc
	


END

GO
Grant Execute on dbo.up_alphegaDetalheDispensasLinhas to Public
Grant control on dbo.up_alphegaDetalheDispensasLinhas  to Public
Go




