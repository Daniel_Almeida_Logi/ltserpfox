/*20200526_034940
	Alphega DE DISPENSAS ANULADAS
	Devolve informação  detalhe das dispensa anuladas
	exec up_alphegaDetalheDispensasAnuladas '20200526', 'Loja 1'
	exec up_alphegaDetalheDispensasAnuladas '20210426', 'Loja 3'




*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_alphegaDetalheDispensasAnuladas]') IS NOT NULL
	drop procedure dbo.up_alphegaDetalheDispensasAnuladas
go

create procedure dbo.up_alphegaDetalheDispensasAnuladas
	@data as datetime
	,@site  as varchar(15)


/* WITH ENCRYPTION */

AS
BEGIN

		
	DECLARE @site_nr INT
	SELECT  @site_nr=no from empresa(nolock) where site = @site

	If OBJECT_ID('tempdb.dbo.#dadosVendas') IS NOT NULL
   		DROP TABLE #dadosVendas



		SELECT 

		  numAnulCC     
		 ,numDispCC     		
		 ,dtAnul
		 ,dispensasId   
		 , lindispensasId
    		into #dadosVendas
		FROM ( 

			Select
					  numAnulCC         = 'numAnulCC '
					 ,numDispCC         = 'numDispCC' 
					 ,dtAnul			= 'dtAnul'
					 ,dispensasId		= 'dispensasId'
					 ,lindispensasId	= 'lindispensasId'
					 ,data				= '19000101'		
					 ,ord				=1
			UNION ALL

			Select
					  numAnulCC         = Fi.ftstamp
					 ,numDispCC         = left(convert(varchar,YEAR(ft.ousrdata))  + convert(varchar,ft.ndoc) +  convert(varchar,ft.fno),15) 
					 ,dtAnul			= ISNULL(convert(varchar(10),ft.fdata,120)+' ' + ft.ousrhora,'')
					 ,dispensasId      = ISNULL((select ftstamp from fi fii where fii.fistamp=  fi.ofistamp),'')
					 ,lindispensaID    =  fi.ofistamp
					 ,data				=convert(datetime,convert(varchar(10),ft.ousrdata,120)+' ' + ft.ousrhora)
					 ,ord				=2
				From fi (nolock)
					inner join ft (nolock)	on fi.ftstamp = ft.ftstamp
					left join  ft2 (nolock)	on ft.ftstamp = ft2.ft2stamp
					left  join st (nolock) on st.ref = fi.ref and st.site_nr = @site_nr
					inner join td (nolock)	on td.ndoc = ft.ndoc
	
				Where
							ft.no >= 200
							and ft.ousrdata = @data
							and ft.site = @site
							AND FT.tipodoc = 3
							AND U_TIPODOC not in (1, 5)
							and td.nmdoc not like '%import%'
							and fistamp not in (select ofistamp from fi f2i(nolock) where f2i.rdata>=fi.rdata)
						and (fi.ref != '' or fi.oref!='')

		) A
		order by A.ord asc,
				 A.data asc


		select
		  numAnulCC     
		 ,numDispCC     		
		 ,dtAnul
		 ,dispensasId   
    	 ,lindispensasId
		from
			#dadosVendas



	
	If OBJECT_ID('tempdb.dbo.#dadosVendas') IS NOT NULL
   		DROP TABLE #dadosVendas

END

GO
Grant Execute on dbo.up_alphegaDetalheDispensasAnuladas to Public
Grant control on dbo.up_alphegaDetalheDispensasAnuladas  to Public
Go


