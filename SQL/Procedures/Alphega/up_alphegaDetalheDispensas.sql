/*
	Alphega DE DISPENSAS
	Devolve informação de cabecalho detalhe de dispensa
	exec up_alphegaDetalheDispensas '20210426', 'Loja 1'
	exec up_alphegaDetalheDispensas '20210426', 'Loja 3'




*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_alphegaDetalheDispensas]') IS NOT NULL
	drop procedure dbo.up_alphegaDetalheDispensas
go

create procedure dbo.up_alphegaDetalheDispensas
	@data as datetime
	,@site  as varchar(15)


/* WITH ENCRYPTION */

AS
BEGIN

	


	DECLARE @operator		    VARCHAR(8) = 'ONL'

	If OBJECT_ID('tempdb.dbo.#dadosVendas') IS NOT NULL
   		DROP TABLE #dadosVendas



		SELECT 

		  dipensasID    
		 ,numDispCC     
		 ,codFarm		
		 ,dtDispensa    
		, cartaofp		
    
    		into #dadosVendas
		FROM ( 

			Select
					  dipensasID        = 	'dipensasID'
					 ,numDispCC         = 	'numDispCC' 
					 ,codFarm			= 	'codFarm'	
					 ,dtDispensa        = 	'dtDispensa'
					, cartaofp			= 	'cartaofp'	
					,  data				=	'19000101'		
					,  ord				=1
			UNION ALL

			Select
					  dipensasID        = ft.ftstamp
					 ,numDispCC         = left(convert(varchar,YEAR(ft.ousrdata))  + convert(varchar,ft.ndoc) +  convert(varchar,ft.fno),15) 
					 ,codFarm			= convert(varchar,lojas.infarmed)
					 ,dtDispensa        = convert(varchar(10),ft.ousrdata,120)+' ' + ft.ousrhora
					, cartaofp			= 'null'
					, data =convert(datetime,convert(varchar(10),ft.ousrdata,120)+' ' + ft.ousrhora)
					,ord				=2
				From
					empresa	lojas (nolock)
					inner join ft (nolock)	on ft.site = Lojas.site
					inner join ft2 (nolock)	on ft2.ft2stamp = ft.ftstamp
					inner join td (nolock)	on td.ndoc = ft.ndoc
				Where
		
					ft.no >= 200
					and ft.ousrdata = @data
					and ft.site = @site
					AND (FT.tipodoc != 4 or u_tipodoc = 4) AND FT.tipodoc != 3
					AND U_TIPODOC not in (1, 5)
					and td.nmdoc not like '%import%'

		) A
		order by A.ord asc,
				 A.data asc


		select
			 dipensasID    
			,numDispCC     
			,codFarm		
			,dtDispensa    
			, cartaofp		
		from
			#dadosVendas



	
	If OBJECT_ID('tempdb.dbo.#dadosVendas') IS NOT NULL
   		DROP TABLE #dadosVendas

END

GO
Grant Execute on dbo.up_alphegaDetalheDispensas to Public
Grant control on dbo.up_alphegaDetalheDispensas  to Public
Go


