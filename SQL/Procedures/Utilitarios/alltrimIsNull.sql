
/****** 

alltrim plus is null

Select dbo.alltrimIsNull(' dsadsadsadsa dsada')  

******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[alltrimIsNull]') IS NOT NULL
    DROP FUNCTION [dbo].alltrimIsNull
GO


CREATE FUNCTION [dbo].alltrimIsNull 
(	
	@value varchar(254)
)
RETURNS varchar(254)
AS
BEGIN
	
	set @value =  ltrim(rtrim(isnull(@value,'')))
	return @value

END
GO


