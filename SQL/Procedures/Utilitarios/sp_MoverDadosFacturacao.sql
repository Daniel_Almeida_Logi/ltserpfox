/*

Faz a c�pia de dados (entre datas e para determinada loja) da DB corrente para a Hist e depois elimina na DB corrente.
Ac��es: 
1) copia de dados para Hist
2) disable trigger 
3) delete dados corrente
4) enable trigger

Ficheiro *.sql gravado em '\lts-erp\SQL\Procedures\Utilitarios'

Jorge Gomes
22 Fevereiro 2020

-------------------------------------------------------------------------------------------------------

Stored Procedures: 

sp_MoverDadosEncomendas:
ENCOMENDAS

select * from bo where dataobra between '2020-01-01' and '2020-01-30' and site = 'Loja 1'
select * from bo2 where bo2stamp in (select bostamp from bo where dataobra between '2020-01-01' and '2020-01-30'  and site = 'Loja 1')
select * from bi where bostamp in (select bostamp from bo where dataobra between '2020-01-01' and '2020-01-30'  and site = 'Loja 1')
select * from bi2 where bostamp in (select bostamp from bo where dataobra between '2020-01-01' and '2020-01-30'  and site = 'Loja 1')

Vari�veis de input: @DBName VARCHAR(50), @Loja VARCHAR(50), @DataInicio VARCHAR(10), @DataFim VARCHAR(10)
Tabelas: (bo,bo2,bi,bi2) 
EXEC dbo.sp_MoverDadosFacturacao 'ltdev30','Loja 1','2020-01-01','2020-01-30'

----------------------------------------------x----------------------------------------------

sp_MoverDadosCertificacaoFact:
CERTIFICA��O DOS DOCS FACTURA�AO

select * from b_cert where date between '2020-01-01' and '2020-01-30'  and site = 'Loja 1'

Vari�veis de input: @DBName VARCHAR(50), @Loja VARCHAR(50), @DataInicio VARCHAR(10), @DataFim VARCHAR(10)
Tabelas: (b_cert) 
EXEC dbo.sp_MoverDadosCertificacaoFact 'ltdev30','Loja 1','2020-01-01','2020-01-30'

----------------------------------------------x----------------------------------------------

sp_MoverDadosDocumentos:
DOCUMENTOS

select * from fo where docdata between '2020-01-01' and '2020-01-30' and site = 'Loja 1'
select * from fo2 where fo2stamp in (select fostamp from fo where docdata between '2020-01-01' and '2020-01-30'  and site = 'Loja 1')
select * from fn where fostamp in (select fostamp from fo where docdata between '2020-01-01' and '2020-01-30'  and site = 'Loja 1')

Vari�veis de input: @DBName VARCHAR(50), @Loja VARCHAR(50), @DataInicio VARCHAR(10), @DataFim VARCHAR(10)
Tabelas: (fo,fo2,fn) 
EXEC dbo.sp_MoverDadosDocumentos 'ltdev30','Loja 1','2020-01-01','2020-01-30'

----------------------------------------------x----------------------------------------------

sp_MoverDadosFacturacao:
FACTURA��O

select * from ft2 where fdata between '2020-01-01' and '2020-01-08'  and site = 'Loja 1'
select * from ft2 where ft2stamp in (select ftstamp from ft where fdata between '2020-01-01' and '2020-01-08'  and site = 'Loja 1') 
select * from fi where ftstamp in (select ftstamp from ft where fdata between '2020-01-01' and '2020-01-08'  and site = 'Loja 1') 
select * from fi2 where ftstamp in (select ftstamp from ft where fdata between '2020-01-01' and '2020-01-08' and site = 'Loja 1') 

Vari�veis de input: @DBName VARCHAR(50), @Loja VARCHAR(50), @DataInicio VARCHAR(10), @DataFim VARCHAR(10)
Tabelas: (ft,ft2,fi,fi2) 
EXEC dbo.sp_MoverDadosFacturacao 'ltdev30','Loja 1','2020-01-01','2020-01-30'


----------------------------------------------x----------------------------------------------

sp_MoverDadosMovimentos:
MOVIMENTOS

select * from sl where datalc between '2020-01-01' and '2020-01-31' and armazem in (select no from empresa where site='Loja 1')

Vari�veis de input: @DBName VARCHAR(50), @Loja VARCHAR(50), @DataInicio VARCHAR(10), @DataFim VARCHAR(10)
Tabelas: (sl) 
EXEC dbo.sp_MoverDadosMovimentos 'ltdev30','Loja 1','2020-01-01','2020-01-31'


Antes de mover os dados entre a DB corrente e a Historico � preciso actualizar o stock, 
executando a sp up_relatorio_conferencia_inventarioData 
e colocando suas linhas na tabela dbo.stil.
Simultaneamente, � preciso inserir na tabela dbo.stic uma linha de resumo das anteriores.


----------------------------------------------x----------------------------------------------

sp_MoverDadosPagamentoFact:
PAGAMENTOS DOS DOCS FACTURA�AO

select * from b_pagcentral where oData between '2020-01-01' and '2020-01-30'  and site = 'Loja 1'

Vari�veis de input: @DBName VARCHAR(50), @Loja VARCHAR(50), @DataInicio VARCHAR(10), @DataFim VARCHAR(10)
Tabelas: (b_pagcentral) 
EXEC dbo.sp_MoverDadosPagamentoFact 'ltdev30','Loja 1','2020-01-01','2020-01-30'

----------------------------------------------x----------------------------------------------

					MUITO IMPORTANTE AP�S CRIAR A PROCEDURE
----marcar procedure como system object

USE Master
GO
EXEC sp_MS_MarkSystemObject sp_MoverDadosXXXXXXXXXXX
GO

*/



USE master
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[sp_MoverDadosFacturacao]') IS NOT NULL
    drop procedure sp_MoverDadosFacturacao
GO


CREATE PROCEDURE dbo.sp_MoverDadosFacturacao 
	@DBName VARCHAR(50), @Loja VARCHAR(50), @DataInicio VARCHAR(10), @DataFim VARCHAR(10)
AS 

--DECLARE @DBName varchar(50) -- database name
DECLARE @DBNameHist varchar(50) -- database name
DECLARE @sqlCommand VARCHAR(8000) 
DECLARE @cursorDisableT VARCHAR(8000) 
DECLARE @cursorEnableT VARCHAR(8000)
DECLARE @ctabela VARCHAR(50)  
DECLARE @cscript VARCHAR(300)
DECLARE @Tabelinha VARCHAR(15)

--DECLARE @Loja VARCHAR(50)  
--DECLARE @DataInicio VARCHAR(10)
--DECLARE @DataFim VARCHAR(10)

--SET @DBName = 'ltdev30'	
SET @DBNameHist = @DBName + 'HIST'	

--SET @Loja = 'Loja 1'
--SET @DataInicio = '2020-01-30'
--SET @DataFim = '2020-02-10'
SET @Tabelinha = 'ft,ft2,fi,fi2'

/* 
--------------------------------------------------------------------------
						C�DIGO DE C�PIA DE DADOS ENTRE TABELAS
-------------------------------------------------------------------------- 
*/

PRINT('  ')
PRINT(' MOVER DADOS PARA HISTORICO REFERENTE � FACTURA��O (ft,ft2,fi,fi2) ')
PRINT('  ')

DECLARE @TablePathHistft varchar(50)
DECLARE @TablePathft varchar(50)
DECLARE @TablePathHistft2 varchar(50)
DECLARE @TablePathft2 varchar(50)
DECLARE @TablePathHistfi varchar(50)
DECLARE @TablePathfi varchar(50)
DECLARE @TablePathHistfi2 varchar(50)
DECLARE @TablePathfi2 varchar(50)

DECLARE @CamposTableft varchar(2000)

SET @TablePathHistft = @DBNameHist + '.dbo.ft'
SET @TablePathft = @DBName + '.dbo.ft'
SET @TablePathHistft2 = @DBNameHist + '.dbo.ft2'
SET @TablePathft2 = @DBName + '.dbo.ft2'
SET @TablePathHistfi = @DBNameHist + '.dbo.fi'
SET @TablePathfi = @DBName + '.dbo.fi'
SET @TablePathHistfi2 = @DBNameHist + '.dbo.fi2'
SET @TablePathfi2 = @DBName + '.dbo.fi2'

SET @CamposTableft = 'ftstamp,pais,nmdoc,fno,no,nome,morada,local,codpost,ncont,bino,bidata,bilocal,telefone,zona,vendedor,vendnm,fdata,
ftano,pdata,carga,descar,saida,ivatx1,ivatx2,ivatx3,fin,final,ndoc,moeda,fref,
ccusto,ncusto,facturada,fnoft,nmdocft,estab,cdata,ivatx4,segmento,totqtt,qtt1,
qtt2,tipo,cobrado,cobranca,tipodoc,chora,ivatx5,ivatx6,ivatx7,ivatx8,
ivatx9,cambiofixo,memissao,cobrador,rota,multi,cheque,clbanco,clcheque,chtotal,echtotal,
custo,eivain1,eivain2,eivain3,eivav1,eivav2,eivav3,ettiliq,edescc,ettiva,etotal,
eivain4,eivav4,efinv,ecusto,eivain5,eivav5,edebreg,eivain6, eivav6,	eivain7,eivav7,	eivain8,
eivav8,eivain9,	eivav9,	total,totalmoeda,ivain1,ivain2,	ivain3,	ivain4,ivain5,ivain6,ivain7,
ivain8,	ivain9,	ivav1,ivav2,ivav3,ivav4,ivav5,ivav6,ivav7,ivav8,ivav9,ttiliq,ttiva,descc,debreg,
debregm,intid,nome2,tpstamp,tpdesc,erdtotal,rdtotal,rdtotalm,cambio,site,pnome,	pno,
cxstamp,cxusername,ssstamp,	ssusername,	anulado,virs,evirs,	valorm2,ousrinis,ousrdata,
ousrhora,usrinis,usrdata,usrhora,u_nratend,u_lote2,	u_ltstamp2,	u_nslote2,u_nslote,	u_slote2,u_lote,u_tlote,
u_tlote2,u_ltstamp,	u_slote,u_hclstamp,	exportado,datatransporte,localcarga,tabIva,	id_tesouraria_conta,
pontosVd,campanhas'
--retirado coluna ftid por ser identity

--ft2, fi, fi2 nao tem identity
--select * from ft2 where ft2stamp in (select ftstamp from ft where fdata between '2020-01-01' and '2020-01-08'  and site = 'Loja 1') 
--select * from fi where ftstamp in (select ftstamp from ft where fdata between '2020-01-01' and '2020-01-08'  and site = 'Loja 1') 
--select * from fi2 where ftstamp in (select ftstamp from ft where fdata between '2020-01-01' and '2020-01-08' and site = 'Loja 1') 

SET @sqlCommand = ' BEGIN TRY
						BEGIN TRANSACTION TT; 
							--ft
							INSERT INTO ' + @TablePathHistft + ' select '+ @CamposTableft +' from ' + @TablePathft + ' 
								where fdata BETWEEN ''' + @DataInicio + ''' and ''' + @DataFim + ''' and site = ''' + @Loja + '''; 
							--ft2	
							INSERT INTO ' + @TablePathHistft2 + ' select * from ' + @TablePathft2 + ' 
								where ft2stamp in (select ftstamp from ft where fdata BETWEEN ''' + @DataInicio + ''' and ''' + @DataFim + ''' and site = ''' + @Loja + '''); 
							--fi	
							INSERT INTO ' + @TablePathHistfi + ' select * from ' + @TablePathfi + ' 
								where ftstamp in (select ftstamp from ft where fdata BETWEEN ''' + @DataInicio + ''' and ''' + @DataFim + ''' and site = ''' + @Loja + '''); 
							--fi2	
							INSERT INTO ' + @TablePathHistfi2 + ' select * from ' + @TablePathfi2 + ' 
								where ftstamp in (select ftstamp from ft where fdata BETWEEN ''' + @DataInicio + ''' and ''' + @DataFim + ''' and site = ''' + @Loja + '''); 
																			
						COMMIT TRANSACTION TT;
						PRINT(''' + '-------------- TRANSA��O COM SUCESSO --------------' + ''')
					END TRY
					BEGIN CATCH
						SELECT   
								ERROR_NUMBER() AS ErrorNumber  
								,ERROR_MESSAGE() AS ErrorMessage;  
						IF @@TRANCOUNT > 0
						BEGIN
							ROLLBACK TRANSACTION TT;							
							PRINT(''' + '-------------- TRANSA��O SEM SUCESSO --------------' + ''')							
						END
					END CATCH' 

--print(@sqlCommand)
EXEC (@sqlCommand)

/* 
--------------------------------------------------------------------------
	C�DIGO PARA PROCURAR TRIGGERS ACTIVOS E GUARDAR NUMA TABELA TEMPOR�RIA 
-------------------------------------------------------------------------- 
*/
--procurar triggers activos nas taberlas especificadas e guardar numa temp
select a.name AS Tabela, a.object_id, b.name 
, 'ALTER TABLE dbo.' + object_name(b.parent_id) + ' DISABLE TRIGGER '+ b.Name AS DisableScript
, 'ALTER TABLE dbo.' + object_name(b.parent_id) + ' ENABLE TRIGGER '+ b.Name AS EnableScript
INTO #Tables_Triggers
FROM sys.tables a
INNER Join sys.triggers b
ON a.object_id = b.parent_id
where a.name in ('ft','ft2','fi','fi2','bo','bo2','bi', 'bi2', 'sl', 'b_cert','fo','fo2','fn', 'b_pagcentral') 
--and  b.is_disabled = 0
and b.name not in ('Tr_Fi_Insert_docslogitools','DelFT') -- triggers inactivos actualmente, retirar do script de desactiva��o/activa��o

--select * from #Tables_Triggers

/* 
--------------------------------------------------------------------------
						CURSOR TO DISABLE TRIGGERS 
						desactivar triggers apenas antes de eliminar os dados das tabelas,
						ou seja, ap�s replicar os dados para a hist�rico; e apenas das tabelas que ir�o ser copiadas
						activar logo de seguida
-------------------------------------------------------------------------- 
*/
--concatena na mesma linha as palavras do mesmo tipo colocando ; entre elas 
-- a funcao STUFF permite colocar espa�o vazio, para eliminar o ; inicial 
	
	SET @cursorDisableT ='DECLARE col_cursor CURSOR FOR 
				select Tabela, 
				STUFF((select ''; '' + us.DisableScript from #Tables_Triggers us
					where us.Tabela = ss.Tabela 							
					FOR XML PATH('''')),1,1,'' '') Script
				from #Tables_Triggers ss
				group by ss.Tabela  
   				having Tabela in (''' + replace(@Tabelinha,',',''',''') + ''') '

	EXEC (@cursorDisableT) 		
	OPEN col_cursor    
	FETCH NEXT FROM col_cursor INTO @ctabela, @cscript 
	
	WHILE @@FETCH_STATUS = 0    
	BEGIN   
			--DB Historico
			SET @sqlCommand = (REPLACE(@cscript,'dbo.', @DBNameHist + '.dbo.'))
			--print(@sqlCommand)			
			EXEC (@sqlCommand)

			--DB corrente
			SET @sqlCommand = (REPLACE(@cscript,'dbo.', @DBName + '.dbo.'))
			--print(@sqlCommand)			
			EXEC (@sqlCommand)

			FETCH NEXT FROM col_cursor INTO @ctabela, @cscript 		  		   
	END  
	CLOSE col_cursor    
	DEALLOCATE col_cursor
	PRINT('----------------  TRIGGERS DISABLED  ----------------')

/* 
--------------------------------------------------------------------------
						DELETE DADOS
-------------------------------------------------------------------------- 
*/

SET @sqlCommand = ' BEGIN TRY
						BEGIN TRANSACTION TT; 
												
							DELETE FROM ' + @TablePathft2 + ' where ft2stamp in (select ftstamp from ft where fdata BETWEEN ''' + @DataInicio + ''' and ''' + @DataFim + ''' and site = ''' + @Loja + '''); 
							DELETE FROM ' + @TablePathfi + ' where ftstamp in (select ftstamp from ft where fdata BETWEEN ''' + @DataInicio + ''' and ''' + @DataFim + ''' and site = ''' + @Loja + '''); 
							DELETE FROM ' + @TablePathfi2 + ' where ftstamp in (select ftstamp from ft where fdata BETWEEN ''' + @DataInicio + ''' and ''' + @DataFim + ''' and site = ''' + @Loja + '''); 
							
							--Nota: a ft deve ser a ultima a ser eliminada pois as outras precisam dela...
							DELETE FROM ' + @TablePathft + ' where fdata BETWEEN ''' + @DataInicio + ''' and ''' + @DataFim + ''' and site = ''' + @Loja + '''; 

							ALTER INDEX ALL ON ' + @TablePathft2 + ' REBUILD ;	
							ALTER INDEX ALL ON ' + @TablePathfi + ' REBUILD ;	
							ALTER INDEX ALL ON ' + @TablePathfi2 + ' REBUILD ;
							ALTER INDEX ALL ON ' + @TablePathft + ' REBUILD ;
									
						COMMIT TRANSACTION TT;
						PRINT(''' + '-------------- TRANSA��O COM SUCESSO --------------' + ''')
					END TRY
					BEGIN CATCH
						SELECT   
								ERROR_NUMBER() AS ErrorNumber  
								,ERROR_MESSAGE() AS ErrorMessage;  
						IF @@TRANCOUNT > 0
						BEGIN
							ROLLBACK TRANSACTION TT;							
							PRINT(''' + '-------------- TRANSA��O SEM SUCESSO --------------' + ''')							
						END
					END CATCH' 

--print(@sqlCommand)
EXEC (@sqlCommand)

/* 
--------------------------------------------------------------------------
						CURSOR TO ENABLE TRIGGERS 
-------------------------------------------------------------------------- 
*/
--concatena na mesma linha as palavras do mesmo tipo colocando ; entre elas 
-- a funcao STUFF permite colocar espa�o vazio, para eliminar o ; inicial 

--select * from #Tables_TriggersDisable	
	SET @cursorEnableT ='DECLARE col_cursor CURSOR FOR 
				select Tabela, 
				STUFF((select ''; '' + us.EnableScript from #Tables_Triggers us
					where us.Tabela = ss.Tabela 							
					FOR XML PATH('''')),1,1,'' '') Script
				from #Tables_Triggers ss
				group by ss.Tabela  
   				having Tabela in (''' + replace(@Tabelinha,',',''',''') + ''') '


	EXEC (@cursorEnableT) 
	OPEN col_cursor    
	FETCH NEXT FROM col_cursor INTO @ctabela, @cscript 
	
	WHILE @@FETCH_STATUS = 0    
	BEGIN   
			--DB Historico n�o precisa activar os triggers
			--SET @sqlCommand = (REPLACE(@cscript,'dbo.', @DBNameHist + '.dbo.'))			
			--print(@sqlCommand)
			--EXEC (@sqlCommand)

			--DB corrente
			SET @sqlCommand = (REPLACE(@cscript,'dbo.', @DBName + '.dbo.'))
			--print(@sqlCommand)			
			EXEC (@sqlCommand)

			FETCH NEXT FROM col_cursor INTO @ctabela, @cscript 		  		   
	END  
	CLOSE col_cursor    
	DEALLOCATE col_cursor
	PRINT('----------------  TRIGGERS ENABLED ----------------')


drop table #Tables_Triggers


GO

Grant Execute On sp_MoverDadosFacturacao to Public
Grant Control On sp_MoverDadosFacturacao to Public
GO

EXEC sp_MS_MarkSystemObject sp_MoverDadosFacturacao
GO


