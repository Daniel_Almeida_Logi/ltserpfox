/*
Stored Procedure sp_DBBackRest
Vari�veis de input: DBname e Path_bak --> ex:  dbo.sp_DBBackRest 'ltdev30_v1', 'C:\sql_bak\'

Esta sp serve para criar backup de uma DB para uma localiza��o no disco e posteriormente
criar nova DB com o mesmo nome mas termina��o "HIST".
Especial aten��o aos nomes l�gicos e f�sicos das DBs

Ficheiro *.sql gravado em '\lts-erp\SQL\Procedures\Utilitarios'


Jorge Gomes
19 Fevereiro 2020



-----------------------------------------------------------------------------
					USAGE - NA NEW QUERY DA DB A USAR

meu PC
EXEC dbo.sp_DBBackRest 'ltdev30_v1', 'C:\sql_bak\'

servidor 10.100.200.41
EXEC dbo.sp_DBBackRest 'ltdev30', 'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\Backup\'



*/



/*
-----------------------------------------------------------------------------
					MUITO IMPORTANTE AP�S CRIAR A PROCEDURE

----marcar procedure como system object
...So, if you need to utilize a stored procedure that has been placed in Master for ease of use and you have to reference system tables 
within the database you are currently in, you�ll have to make sure to mark it as a system stored procedure.

Ap�s criar a procedure no master esta fica em stored procedures dbo.sp_DBBackRest
preciso agora eleger a mesma como objecto de sistema para poder utilizar os dados de contexto de qualquer DB, desde que seja executada
nessa DB (abrir new query na DB contexto a usar).

Ap�s marcar a procedure como objecto de sistema ela passa para master\stored procedures\system stored procedures\

USE Master
GO
EXEC sp_MS_MarkSystemObject sp_DBBackRest
GO

*/



USE master
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[sp_DBBackRest]') IS NOT NULL
    drop procedure sp_DBBackRest
GO


CREATE PROCEDURE dbo.sp_DBBackRest @DBName VARCHAR(50), @path VARCHAR(256)
AS 

--DECLARE @DBName varchar(50) -- database name
--DECLARE @path VARCHAR(256) -- path for backup files  
DECLARE @UseDBName varchar(50) -- USE sintaxe 
DECLARE @LogicalDataName VARCHAR(200) 
DECLARE @LogicalLogName VARCHAR(200) 
DECLARE @PhysicalDataName VARCHAR(200) 
DECLARE @PhysicalLogName VARCHAR(200)
DECLARE @fileNameOrigem VARCHAR(256)
DECLARE @fileNameDestino VARCHAR(256)

-- valores a alterar se necess�rio 
--SET @DBName = 'ltdev30'				
--SET @path = 'C:\sql_bak\' 

select @UseDBName = 'USE ' + @DBName --USE @DBName option
EXEC(@UseDBName)


SET @LogicalDataName = (select name from sys.database_files where type_desc='ROWS') 
SET @LogicalLogName = (select name from sys.database_files where type_desc='LOG') 
SET @PhysicalDataName = (select physical_name from sys.database_files where type_desc='ROWS') 
SET @PhysicalLogName = (select physical_name from sys.database_files where type_desc='LOG') 
/*
SET @LogicalDataName = 'select name from ' + @DBName + '.sys.database_files where type_desc=''ROWS'''
SET @LogicalLogName = 'select name from ' + @DBName + '.sys.database_files where type_desc=''LOG'''
SET @PhysicalDataName = 'select physical_name from ' + @DBName + '.sys.database_files where type_desc=''ROWS'''
SET @PhysicalLogName = 'select physical_name from ' + @DBName + '.sys.database_files where type_desc=''LOG'''
...
*/

--alterar os nomes fisicos dos ficheiros para colocar HIST na sua nomenclatura
SET @PhysicalDataName = LEFT(@PhysicalDataName, LEN(@PhysicalDataName)-4) + 'Hist.MDF'
SET @PhysicalLogName = LEFT(@PhysicalLogName, LEN(@PhysicalLogName)-4) + 'Hist.LDF'

SET @fileNameOrigem = @path + @DBName + '.BAK'
SET @fileNameDestino = @DBName + 'HIST'

IF not EXISTS (select name from sys.databases where name = @fileNameDestino)
BEGIN
	--backup WITH INIT all backup sets should be overwritten	 
	PRINT('----------------------- Backup Database ---------------------------')
	BACKUP DATABASE @DBName TO DISK = @fileNameOrigem  WITH INIT 
	PRINT(' ')
	PRINT('BACKUP ' + @fileNameOrigem + ' com Sucesso')
	PRINT(' ')

	--restore para outra localiza��o/nome apenas se n�o existir ainda	 
	PRINT('----------------------- Restore Database --------------------------')
	
		/*
		codigo que permite substituir as variaveis acima do disco logico e fisico...
		...deixou de ser necess�rio fazendo a procedure como sistema (sp_) colocando na master DB e garantindo-lhe 
		direitos de system object (com isto, a procedure fica gravada no master mas pode ser executada em qualquer DB com uma simples chamada,
		o interior da chamada �s sys.tables assumem a DB em quest�o em vez da master...

		ver artigo: h_ttps://sqlrus.com/2011/10/add-a-procedure-to-master-database-yes-please/
		)


		DECLARE @fileListTable TABLE (
			[LogicalName]           NVARCHAR(128),
			[PhysicalName]          NVARCHAR(260),
			[Type]                  CHAR(1),
			[FileGroupName]         NVARCHAR(128),
			[Size]                  NUMERIC(20,0),
			[MaxSize]               NUMERIC(20,0),
			[FileID]                BIGINT,
			[CreateLSN]             NUMERIC(25,0),
			[DropLSN]               NUMERIC(25,0),
			[UniqueID]              UNIQUEIDENTIFIER,
			[ReadOnlyLSN]           NUMERIC(25,0),
			[ReadWriteLSN]          NUMERIC(25,0),
			[BackupSizeInBytes]     BIGINT,
			[SourceBlockSize]       INT,
			[FileGroupID]           INT,
			[LogGroupGUID]          UNIQUEIDENTIFIER,
			[DifferentialBaseLSN]   NUMERIC(25,0),
			[DifferentialBaseGUID]  UNIQUEIDENTIFIER,
			[IsReadOnly]            BIT,
			[IsPresent]             BIT,
			[TDEThumbprint]         VARBINARY(32), -- remove this column if using SQL 2005
			[SnapshotUrl]           NVARCHAR(100) -- remove this column if using SQL 2005
		)
		INSERT INTO @fileListTable EXEC('RESTORE FILELISTONLY FROM DISK = ''' + @fileNameOrigem + '''')
		--SELECT [LogicalName], [PhysicalName], [Type] FROM @fileListTable

		SET @LogicalDataName = (select LogicalName from @fileListTable where Type='D') 
		SET @LogicalLogName = (select LogicalName from @fileListTable where Type='L') 
		SET @PhysicalDataName = (select PhysicalName from @fileListTable where Type='D') 
		SET @PhysicalLogName = (select PhysicalName from @fileListTable where Type='L') 

		--alterar os nomes fisicos dos ficheiros para colocar HIST na sua nomenclatura
		SET @PhysicalDataName = LEFT(@PhysicalDataName, LEN(@PhysicalDataName)-4) + 'Hist.MDF'
		SET @PhysicalLogName = LEFT(@PhysicalLogName, LEN(@PhysicalLogName)-4) + 'Hist.LDF'
		*/

	
	RESTORE DATABASE @fileNameDestino  
		FROM DISK = @fileNameOrigem
		WITH 
			MOVE @LogicalDataName TO @PhysicalDataName,
			MOVE @LogicalLogName TO @PhysicalLogName 
	PRINT(' ')
	PRINT('RESTORE ' + @fileNameDestino + ' com Sucesso')
	PRINT(' ')	
END 
ELSE
BEGIN
	PRINT('---------------------------------------------------------------------------------')
	PRINT('DB '+ @fileNameDestino + ' j� existe, n�o pode ser realizado o procedimento de backup/restore')
	PRINT('---------------------------------------------------------------------------------')
END

GO

Grant Execute On sp_DBBackRest to Public
Grant Control On sp_DBBackRest to Public
GO


