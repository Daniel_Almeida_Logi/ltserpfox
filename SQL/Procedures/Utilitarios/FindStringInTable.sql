
/*
sp para pesquisa em qq tabela de determinada string

baseado em Cursor

Cursor 1: divide a string de pesquisa e coloca cada termo num cursor
Cursor 2: para cada tabela especificada para a pesquisa, s�o procuradas as colunas e colocadas num cursor

Processo:
Para cada elemento de pesquisa do cursor 1 � procurado em todas as colunas do cursor 2
Para cada pesquisa o resultado � gravado numa tabela tempor�ria, sendo o primeiro passo um select into 
e os seguintes um insert


N�o usa a fun��o STRING_SPLIT do sql 2016 mas sim  a fun��o splitstring criada para o efeito (table valueed functions).

12/02/2020
JG


Usage:
EXEC dbo.up_FindStringInTable 'xxx yyy www ...', 'schema_name', 'table_name'

EXEC dbo.up_FindStringInTable '176533582', 'dbo', 'B_cli_dados'

*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_FindStringInTable]') IS NOT NULL
    drop procedure up_FindStringInTable
go



CREATE PROCEDURE dbo.up_FindStringInTable @stringToFind VARCHAR(200), @schema sysname, @table sysname 
AS 

DECLARE @sqlCommand VARCHAR(8000) 
DECLARE @stmt VARCHAR(50)
DECLARE @where VARCHAR(8000) 
DECLARE @columnName sysname 
DECLARE @wordlist sysname
DECLARE @cursor VARCHAR(8000) 
DECLARE @cursor2 VARCHAR(8000) 
DECLARE @contador int

/*
DECLARE @stringToFind VARCHAR(200) = 'gondomar'
DECLARE @schema sysname = 'dbo'
DECLARE @table sysname = 'b_utentes'
*/

BEGIN TRY
	SET @stmt = ' SELECT * INTO ##TempTbl ' 

   --repartir a string de entrada em v�rias words e colocar no cursor @wordlist
   --SET @cursor2 = 'DECLARE col_cursor2 CURSOR FOR SELECT value FROM STRING_SPLIT(''' + @stringToFind + ''' ,'' '')'
   -- criei fun��o para fazer o split de uma string baseado em determinado simbolo mas evitar estar a alterar
   -- o modo de compatibilidade da DB para 130 (est� a 100)

   SET @cursor2 = 'DECLARE col_cursor2 CURSOR FOR SELECT Name FROM dbo.splitstring(''' + @stringToFind + ''' ,'' '')'
   EXEC (@cursor2)
   OPEN col_cursor2    
   FETCH NEXT FROM col_cursor2 INTO @wordlist     

   WHILE @@FETCH_STATUS = 0    
   BEGIN         
		SET @sqlCommand =  @stmt + ' FROM [' + @schema + '].[' + @table + '] WHERE' 
		SET @where = '' 

		--case when DATA_TYPE='datetime' then CONVERT(varchar(25), COLUMN_NAME, 126) else COLUMN_NAME end as COLUMN_NAME

		--obter nomes das colunas da tabela e colocar no cursor @columnName
		SET @cursor = 'DECLARE col_cursor CURSOR FOR SELECT COLUMN_NAME 
		FROM ' + DB_NAME() + '.INFORMATION_SCHEMA.COLUMNS 
		WHERE TABLE_SCHEMA = ''' + @schema + ''' 
		AND TABLE_NAME = ''' + @table + ''' 
		AND DATA_TYPE IN (''char'',''nchar'',''ntext'',''nvarchar'',''text'',''varchar'', ''numeric'')'  
		EXEC (@cursor) 
		OPEN col_cursor    
		FETCH NEXT FROM col_cursor INTO @columnName

		WHILE @@FETCH_STATUS = 0    
		BEGIN    
			IF @where <> '' 
			BEGIN	   
			   SET @where = @where + ' OR' 
			   SET @stmt = ' INSERT INTO ##TempTbl SELECT * ' 
			END

			SET @where = @where + ' [' + @columnName + '] LIKE ''' + '%'+ @wordlist + '%'+'''' 
			FETCH NEXT FROM col_cursor INTO @columnName  		  		   
		END  
		CLOSE col_cursor    
		DEALLOCATE col_cursor

		SET @sqlCommand = @sqlCommand + @where 
		PRINT @sqlCommand 
		 
		 --novo para guardar resultados
		 --insert into #TempTbl
		EXEC (@sqlCommand)			 		 
	    FETCH NEXT FROM col_cursor2 INTO @wordlist  
   END 
   CLOSE col_cursor2    
   DEALLOCATE col_cursor2


   SELECT * FROM  ##TempTbl
   /*

   seria especifico para uma tabela com coluna unica chamada utstamp, como a b_utentes
   

   SET @contador = (SELECT count(*) FROM 
   (SELECT (ROW_NUMBER() over (partition by utstamp order by utstamp)) as k from ##TempTbl) kk 
   where kk.k=1)   
   --PRINT(@contador)

   IF @contador >= 15 
		BEGIN	   
			--divide o set de dados em buckets por forma a fazer pagina��o de amostragem de dados
		   SELECT *, NTILE( CEILING (@contador / 10) +1) over (order by nome) AS bins FROM 
		   (SELECT *, (ROW_NUMBER() over (partition by utstamp order by utstamp)) as n_linhas from ##TempTbl) kk 
		   where kk.n_linhas = 1		   
		END
	ELSE
		BEGIN	   
			--divide o set de dados em buckets por forma a fazer pagina��o de amostragem de dados
		   SELECT *, NTILE(1) over (order by nome) AS bins FROM 
		   (SELECT *, (ROW_NUMBER() over (partition by utstamp order by utstamp)) as n_linhas from ##TempTbl) kk 
		   where kk.n_linhas = 1		   
		END

	*/

	drop table ##TempTbl
END TRY

BEGIN CATCH 
   PRINT 'There was an error. Check to make sure object exists.'
   PRINT error_message()
    
   IF CURSOR_STATUS('variable', 'col_cursor') <> -3 
   BEGIN 
       CLOSE col_cursor    
       DEALLOCATE col_cursor  
   END 
   IF CURSOR_STATUS('variable', 'col_cursor2') <> -3 
   BEGIN 
       CLOSE col_cursor2    
       DEALLOCATE col_cursor2 
   END 
END CATCH 


GO
Grant Execute On up_FindStringInTable to Public
Grant Control On up_FindStringInTable to Public
go
     