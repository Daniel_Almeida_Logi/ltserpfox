/*
Stored Procedure sp_gerarTriggers_table

exec sp_gerarTriggers_table '20000101','20210401', 'fi'

*/




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[sp_gerarTriggers_table]') IS NOT NULL
    drop procedure sp_gerarTriggers_table
GO


CREATE PROCEDURE dbo.sp_gerarTriggers_table 

@dateInit   datetime,
@dateEnd    datetime,
@table      varchar(100)

AS 


Declare @DateTable table ([date]       DATE PRIMARY KEY);

-- use the catalog views to generate as many rows as we need

	INSERT @DateTable ([date])
	SELECT d
	FROM (
		SELECT d = DATEADD(DAY, rn - 1, @dateInit)
		FROM (
			SELECT TOP (DATEDIFF(DAY, @dateInit, @dateEnd)) rn = ROW_NUMBER() OVER (
					ORDER BY s1.[object_id]
					)
			FROM sys.all_objects AS s1
			CROSS JOIN sys.all_objects AS s2
			-- on my system this would support > 5 million days
			ORDER BY s1.[object_id]
			) AS x
		) AS y;

	SELECT 
	    'data' = 	 ' update ' + ltrim(rtrim(@table)) + ' set qtt=qtt where  convert(varchar, ousrdata, 112)   =  '''+ convert(varchar, date, 112) + ''''   + CHAR(10) + ' print '''+ convert(varchar, date, 112) + ''' GO'
	FROM @DateTable
	where convert(varchar, date, 112)  = (select top 1 convert(varchar, ousrdata, 112)  from fi(nolock) where  convert(varchar, date, 112)=convert(varchar, ousrdata, 112) )
	ORDER BY [date]



Grant Execute On sp_gerarTriggers_table to Public
Grant Control On sp_gerarTriggers_table to Public
GO

EXEC sp_MS_MarkSystemObject sp_gerarTriggers_table
GO
