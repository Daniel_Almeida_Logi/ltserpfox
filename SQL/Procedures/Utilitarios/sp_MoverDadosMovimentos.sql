/*

Faz a c�pia de dados (entre datas e para determinada loja) da DB corrente para a Hist e depois elimina na DB corrente.
Ac��es: 
1) copia de dados para Hist
2) disable trigger 
3) delete dados corrente
4) enable trigger

Ficheiro *.sql gravado em '\lts-erp\SQL\Procedures\Utilitarios'

Jorge Gomes
22 Fevereiro 2020

-------------------------------------------------------------------------------------------------------

Stored Procedures: 

sp_MoverDadosEncomendas:
ENCOMENDAS

select * from bo where dataobra between '2020-01-01' and '2020-01-30' and site = 'Loja 1'
select * from bo2 where bo2stamp in (select bostamp from bo where dataobra between '2020-01-01' and '2020-01-30'  and site = 'Loja 1')
select * from bi where bostamp in (select bostamp from bo where dataobra between '2020-01-01' and '2020-01-30'  and site = 'Loja 1')
select * from bi2 where bostamp in (select bostamp from bo where dataobra between '2020-01-01' and '2020-01-30'  and site = 'Loja 1')

Vari�veis de input: @DBName VARCHAR(50), @Loja VARCHAR(50), @DataInicio VARCHAR(10), @DataFim VARCHAR(10)
Tabelas: (bo,bo2,bi,bi2) 
EXEC dbo.sp_MoverDadosEncomendas 'ltdev30','Loja 1','2020-01-01','2020-01-30'

----------------------------------------------x----------------------------------------------

sp_MoverDadosCertificacaoFact:
CERTIFICA��O DOS DOCS FACTURA�AO

select * from b_cert where date between '2020-01-01' and '2020-01-30'  and site = 'Loja 1'

Vari�veis de input: @DBName VARCHAR(50), @Loja VARCHAR(50), @DataInicio VARCHAR(10), @DataFim VARCHAR(10)
Tabelas: (b_cert) 
EXEC dbo.sp_MoverDadosCertificacaoFact 'ltdev30','Loja 1','2020-01-01','2020-01-30'

----------------------------------------------x----------------------------------------------

sp_MoverDadosDocumentos:
DOCUMENTOS

select * from fo where docdata between '2020-01-01' and '2020-01-30' and site = 'Loja 1'
select * from fo2 where fo2stamp in (select fostamp from fo where docdata between '2020-01-01' and '2020-01-30'  and site = 'Loja 1')
select * from fn where fostamp in (select fostamp from fo where docdata between '2020-01-01' and '2020-01-30'  and site = 'Loja 1')

Vari�veis de input: @DBName VARCHAR(50), @Loja VARCHAR(50), @DataInicio VARCHAR(10), @DataFim VARCHAR(10)
Tabelas: (fo,fo2,fn) 
EXEC dbo.sp_MoverDadosDocumentos 'ltdev30','Loja 1','2020-01-01','2020-01-30'

----------------------------------------------x----------------------------------------------

sp_MoverDadosFacturacao:
FACTURA��O

select * from ft2 where fdata between '2020-01-01' and '2020-01-08'  and site = 'Loja 1'
select * from ft2 where ft2stamp in (select ftstamp from ft where fdata between '2020-01-01' and '2020-01-08'  and site = 'Loja 1') 
select * from fi where ftstamp in (select ftstamp from ft where fdata between '2020-01-01' and '2020-01-08'  and site = 'Loja 1') 
select * from fi2 where ftstamp in (select ftstamp from ft where fdata between '2020-01-01' and '2020-01-08' and site = 'Loja 1') 

Vari�veis de input: @DBName VARCHAR(50), @Loja VARCHAR(50), @DataInicio VARCHAR(10), @DataFim VARCHAR(10)
Tabelas: (ft,ft2,fi,fi2) 
EXEC dbo.sp_MoverDadosFacturacao 'ltdev30','Loja 1','2020-01-01','2020-01-30'


----------------------------------------------x----------------------------------------------

sp_MoverDadosMovimentos:
MOVIMENTOS

select * from sl where datalc between '2020-01-01' and '2020-01-31' and armazem in (select no from empresa where site='Loja 1')

Vari�veis de input: @DBName VARCHAR(50), @Loja VARCHAR(50), @DataInicio VARCHAR(10), @DataFim VARCHAR(10)
Tabelas: (sl) 
EXEC dbo.sp_MoverDadosMovimentos 'ltdev30','Loja 1','2020-01-01','2020-01-31'


Antes de mover os dados entre a DB corrente e a Historico � preciso actualizar o stock, 
executando a sp up_relatorio_conferencia_inventarioData 
e colocando suas linhas na tabela dbo.stil.
Simultaneamente, � preciso inserir na tabela dbo.stic uma linha de resumo das anteriores.

----------------------------------------------x----------------------------------------------

sp_MoverDadosPagamentoFact:
PAGAMENTOS DOS DOCS FACTURA�AO

select * from b_pagcentral where oData between '2020-01-01' and '2020-01-30'  and site = 'Loja 1'

Vari�veis de input: @DBName VARCHAR(50), @Loja VARCHAR(50), @DataInicio VARCHAR(10), @DataFim VARCHAR(10)
Tabelas: (b_pagcentral) 
EXEC dbo.sp_MoverDadosPagamentoFact 'ltdev30','Loja 1','2020-01-01','2020-01-30'

----------------------------------------------x----------------------------------------------

					MUITO IMPORTANTE AP�S CRIAR A PROCEDURE
----marcar procedure como system object

USE Master
GO
EXEC sp_MS_MarkSystemObject sp_MoverDadosMovimentos
GO

*/


USE master
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[sp_MoverDadosMovimentos]') IS NOT NULL
    drop procedure sp_MoverDadosMovimentos
GO

CREATE PROCEDURE dbo.sp_MoverDadosMovimentos 
	@DBName VARCHAR(50), @Loja VARCHAR(50), @DataInicio VARCHAR(10), @DataFim VARCHAR(10)
AS 

--DECLARE @DBName varchar(50) -- database name
DECLARE @DBNameHist varchar(50) -- database name
DECLARE @sqlCommand VARCHAR(8000) 
DECLARE @cursorDisableT VARCHAR(8000) 
DECLARE @cursorEnableT VARCHAR(8000)
DECLARE @ctabela VARCHAR(50)  
DECLARE @cscript VARCHAR(300)
DECLARE @Tabelinha VARCHAR(15)

--DECLARE @Loja VARCHAR(50)  
--DECLARE @DataInicio VARCHAR(10)
--DECLARE @DataFim VARCHAR(10)

--SET @DBName = 'ltdev30'	
SET @DBNameHist = @DBName + 'HIST'	

--SET @Loja = 'Loja 1'
--SET @DataInicio = '2020-01-30'
--SET @DataFim = '2020-02-10'
SET @Tabelinha = 'sl'

DECLARE @TablePathstic varchar(50)
DECLARE @TablePathstil varchar(50)
SET @TablePathstic = @DBName + '.dbo.stic'
SET @TablePathstil = @DBName + '.dbo.stil'
DECLARE @stampid CHAR(25)  
SET @stampid = (select left(newid(),21))

DECLARE @TablePathHistsl varchar(50)
DECLARE @TablePathsl varchar(50)
--DECLARE @CamposTablesl varchar(2000)
SET @TablePathHistsl = @DBNameHist + '.dbo.sl'
SET @TablePathsl = @DBName + '.dbo.sl'


--select * from @TablePathsl where datalc between '2019-11-01' and '2019-11-30' and armazem in (select no from empresa where site='Loja 1')

SET @sqlCommand = 'select slstamp from ' + @TablePathsl + ' where datalc BETWEEN ''' + @DataInicio + ''' 
	and ''' + @DataFim + ''' and armazem in (select no from empresa where site=''' + @Loja + ''')'


SET @sqlCommand = ' 
				IF EXISTS (' + @sqlCommand + ') 
				BEGIN	
					BEGIN TRY
						BEGIN TRANSACTION; 		

							/* 							
								C�DIGO DE inser��o de dados de stock
								O resultado da sp up_relatorio_conferencia_inventarioData
								tem de ser inserido na tabela stil.
								Criar linha unica com o stamp na stic.						
							*/
							DECLARE @inventarioTable TABLE (
								armazem      NUMERIC(5,0),
								ref          VARCHAR(18),
								design       VARCHAR(100),
								STOCKADATA   NUMERIC(14,3),
								PclAData     NUMERIC(14,3),
								Total        NUMERIC(14,3),
								local1       VARCHAR(20),
								local2       VARCHAR(20),
								local3       VARCHAR(20),
								lordem  INT IDENTITY(1,1)
							)
							--execu��o da sp para uma tabela tempor�ria
							insert into @inventarioTable 
							exec up_relatorio_conferencia_inventarioData ''' + @DataFim + ''','''','''', ''' + @Loja + ''', '''','''','''',-9999;
							
							insert into ' + @TablePathstic + ' (sticstamp, data, descricao, lanca, stamp, ccusto,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,hora,exportado)  
							select ''' + @stampid + ''', ''' + @DataFim + ''', ''Invent�rio Migracao de Dados ''+ CONVERT(VARCHAR(11), GETDATE(), 113), 1, '''', ''''
								, ''ADM'', GETDATE(), CONVERT(VARCHAR(8), GETDATE(), 108)
									, ''ADM'', GETDATE(), CONVERT(VARCHAR(8), GETDATE(), 108), ''23:30:00'', 0 ;

							--insere todas as linhas da tabela temporaria anterior
							insert into ' + @TablePathstil + ' (stilstamp,ref,design,data,stock,sticstamp,armazem,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,zona,pcpond,epcpond,lordem,u_qtt,exportado,lote)
							select left(newid(),21), ref, design, ''' + @DataFim + ''', STOCKADATA, ''' + @stampid + ''', armazem, ''ADM'', GETDATE(), CONVERT(VARCHAR(8), GETDATE(), 108)
									, ''ADM'', GETDATE(), CONVERT(VARCHAR(8), GETDATE(), 108), '''', 0, 0, lordem, 0, 0, '''' from @inventarioTable	;	
							

							/* 
									COPIAR DADOS PARA HISTORICO REFERENTE A MOVIMENTOS (sl),
									o DELETE somente ap�s desactivar TRIGGERS
							*/

							INSERT INTO ' + @TablePathHistsl + ' select * from ' + @TablePathsl + ' 
								where datalc BETWEEN ''' + @DataInicio + ''' and ''' + @DataFim + ''' and armazem in (select no from empresa where site=''' + @Loja + '''); 	
						
						COMMIT TRANSACTION;						
					END TRY
					BEGIN CATCH
						SELECT   
								ERROR_NUMBER() AS ErrorNumber  
								,ERROR_MESSAGE() AS ErrorMessage;  
						IF @@TRANCOUNT > 0
						BEGIN
							ROLLBACK TRANSACTION;		
						END
					END CATCH
				END' 

--print(@sqlCommand)
EXEC (@sqlCommand)



/* 
--------------------------------------------------------------------------
	C�DIGO PARA PROCURAR TRIGGERS ACTIVOS E GUARDAR NUMA TABELA TEMPOR�RIA 
-------------------------------------------------------------------------- 
*/
--procurar triggers activos nas taberlas especificadas e guardar numa temp
select a.name AS Tabela, a.object_id, b.name 
, 'ALTER TABLE dbo.' + object_name(b.parent_id) + ' DISABLE TRIGGER '+ b.Name AS DisableScript
, 'ALTER TABLE dbo.' + object_name(b.parent_id) + ' ENABLE TRIGGER '+ b.Name AS EnableScript
INTO #Tables_Triggers
FROM sys.tables a
INNER Join sys.triggers b
ON a.object_id = b.parent_id
where a.name in ('ft','ft2','fi','fi2','bo','bo2','bi', 'bi2', 'sl', 'b_cert','fo','fo2','fn', 'b_pagcentral') 
--and  b.is_disabled = 0
and b.name not in ('Tr_Fi_Insert_docslogitools','DelFT') -- triggers inactivos actualmente, retirar do script de desactiva��o/activa��o

--select * from #Tables_Triggers


/* 
--------------------------------------------------------------------------
						CURSOR TO DISABLE TRIGGERS 
						desactivar triggers apenas antes de eliminar os dados das tabelas,
						ou seja, ap�s replicar os dados para a hist�rico; e apenas das tabelas que ir�o ser copiadas
						activar logo de seguida
-------------------------------------------------------------------------- 
*/
--concatena na mesma linha as palavras do mesmo tipo colocando ; entre elas 
-- a funcao STUFF permite colocar espa�o vazio, para eliminar o ; inicial 
	
	SET @cursorDisableT ='DECLARE col_cursor CURSOR FOR 
				select Tabela, 
				STUFF((select ''; '' + us.DisableScript from #Tables_Triggers us
					where us.Tabela = ss.Tabela 							
					FOR XML PATH('''')),1,1,'' '') Script
				from #Tables_Triggers ss
				group by ss.Tabela  
   				having Tabela in (''' + replace(@Tabelinha,',',''',''') + ''') '

	EXEC (@cursorDisableT) 		
	OPEN col_cursor    
	FETCH NEXT FROM col_cursor INTO @ctabela, @cscript 
	
	WHILE @@FETCH_STATUS = 0    
	BEGIN   
			--DB Historico
			SET @sqlCommand = (REPLACE(@cscript,'dbo.', @DBNameHist + '.dbo.'))
			--print(@sqlCommand)			
			EXEC (@sqlCommand)

			--DB corrente
			SET @sqlCommand = (REPLACE(@cscript,'dbo.', @DBName + '.dbo.'))
			--print(@sqlCommand)			
			EXEC (@sqlCommand)

			FETCH NEXT FROM col_cursor INTO @ctabela, @cscript 		  		   
	END  
	CLOSE col_cursor    
	DEALLOCATE col_cursor
	PRINT('----------------  TRIGGERS DISABLED  ----------------')

/* 
--------------------------------------------------------------------------
						DELETE DADOS 
-------------------------------------------------------------------------- 
*/

SET @sqlCommand = ' BEGIN TRY
						BEGIN TRANSACTION TT; 
														
							DELETE FROM ' + @TablePathsl + ' where datalc BETWEEN ''' + @DataInicio + ''' and ''' + @DataFim + '''
							and armazem in (select no from empresa where site=''' + @Loja + '''); 

							ALTER INDEX ALL ON ' + @TablePathsl + ' REBUILD ;							
						
						COMMIT TRANSACTION TT;
						PRINT(''' + '-------------- TRANSA��O COM SUCESSO --------------' + ''')
					END TRY
					BEGIN CATCH
						SELECT   
								ERROR_NUMBER() AS ErrorNumber  
								,ERROR_MESSAGE() AS ErrorMessage;  
						IF @@TRANCOUNT > 0
						BEGIN
							ROLLBACK TRANSACTION TT;							
							PRINT(''' + '-------------- TRANSA��O SEM SUCESSO --------------' + ''')							
						END
					END CATCH' 

--print(@sqlCommand)
EXEC (@sqlCommand)

/* 
--------------------------------------------------------------------------
						CURSOR TO ENABLE TRIGGERS 
-------------------------------------------------------------------------- 
*/
--concatena na mesma linha as palavras do mesmo tipo colocando ; entre elas 
-- a funcao STUFF permite colocar espa�o vazio, para eliminar o ; inicial 

--select * from #Tables_TriggersDisable	
	SET @cursorEnableT ='DECLARE col_cursor CURSOR FOR 
				select Tabela, 
				STUFF((select ''; '' + us.EnableScript from #Tables_Triggers us
					where us.Tabela = ss.Tabela 							
					FOR XML PATH('''')),1,1,'' '') Script
				from #Tables_Triggers ss
				group by ss.Tabela  
   				having Tabela in (''' + replace(@Tabelinha,',',''',''') + ''') '


	EXEC (@cursorEnableT) 
	OPEN col_cursor    
	FETCH NEXT FROM col_cursor INTO @ctabela, @cscript 
	
	WHILE @@FETCH_STATUS = 0    
	BEGIN   
			--DB Historico n�o precisa activar os triggers
			--SET @sqlCommand = (REPLACE(@cscript,'dbo.', @DBNameHist + '.dbo.'))			
			--print(@sqlCommand)
			--EXEC (@sqlCommand)

			--DB corrente
			SET @sqlCommand = (REPLACE(@cscript,'dbo.', @DBName + '.dbo.'))
			--print(@sqlCommand)			
			EXEC (@sqlCommand)

			FETCH NEXT FROM col_cursor INTO @ctabela, @cscript 		  		   
	END  
	CLOSE col_cursor    
	DEALLOCATE col_cursor
	PRINT('----------------  TRIGGERS ENABLED ----------------')


drop table #Tables_Triggers


GO

Grant Execute On sp_MoverDadosMovimentos to Public
Grant Control On sp_MoverDadosMovimentos to Public
GO

EXEC sp_MS_MarkSystemObject sp_MoverDadosMovimentos
GO


