/*
Stored Procedure sp_EliminarDadosHist
Vari�veis de input: DBname --> ex:  dbo.sp_EliminarDadosHist 'ltdev30'

Eliminar� da ltdev30HIST as seguintes tabelas:

('ft','ft2','fi','fi2','bo','bo2','bi', 'bi2', 'sl', 'b_cert','fo','fo2','fn', 'b_pagcentral') 

Ficheiro *.sql gravado em '\lts-erp\SQL\Procedures\Utilitarios'


Jorge Gomes
22 Fevereiro 2020
-----------------------------------------------------------------------------
					USAGE - NA NEW QUERY DA DB A USAR

EXEC dbo.sp_EliminarDadosHist 'ltdev30'


--verifica��o: nao pode ter registos
USE ltdev30HIST
select top 2 * from ft
select top 2 * from ft2
select top 2 * from fi
select top 2 * from fi2
select top 2 * from bo
select top 2 * from bo2
select top 2 * from bi
select top 2 * from bi2
select top 2 * from sl
select top 2 * from b_cert
select top 2 * from fo
select top 2 * from fo2
select top 2 * from fn
select top 2 * from b_pagcentral
*/



/*
-----------------------------------------------------------------------------
					MUITO IMPORTANTE AP�S CRIAR A PROCEDURE
----marcar procedure como system object

USE Master
GO
EXEC sp_MS_MarkSystemObject sp_EliminarDadosHist
GO

*/



USE master
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[sp_EliminarDadosHist]') IS NOT NULL
    drop procedure sp_EliminarDadosHist
GO


CREATE PROCEDURE dbo.sp_EliminarDadosHist @DBName VARCHAR(50)
AS 

/* 
--------------------------------------------------------------------------
					TRUNCATE TABLES NA DB HISTORICO
-------------------------------------------------------------------------- 
*/

--DECLARE @DBName varchar(50) -- database name
DECLARE @DBNameHist varchar(50) -- database name
DECLARE @Stmt_ varchar(15)
DECLARE @Stmt varchar(100)

SET @Stmt_ = 'TRUNCATE TABLE '
--SET @DBName = 'ltdev30'	
SET @DBNameHist = @DBName + 'HIST'	

SET @Stmt = @Stmt_ + @DBNameHist + '.dbo.ft'
EXEC(@Stmt)
SET @Stmt = @Stmt_ + @DBNameHist + '.dbo.ft2'
EXEC(@Stmt)
SET @Stmt = @Stmt_ + @DBNameHist + '.dbo.fi'
EXEC(@Stmt)
SET @Stmt = @Stmt_ + @DBNameHist + '.dbo.fi2'
EXEC(@Stmt)
SET @Stmt = @Stmt_ + @DBNameHist + '.dbo.bo'
EXEC(@Stmt)
SET @Stmt = @Stmt_ + @DBNameHist + '.dbo.bo2'
EXEC(@Stmt)
SET @Stmt = @Stmt_ + @DBNameHist + '.dbo.bi'
EXEC(@Stmt)
SET @Stmt = @Stmt_ + @DBNameHist + '.dbo.bi2'
EXEC(@Stmt)
SET @Stmt = @Stmt_ + @DBNameHist + '.dbo.sl'
EXEC(@Stmt)
SET @Stmt = @Stmt_ + @DBNameHist + '.dbo.b_cert'
EXEC(@Stmt)
SET @Stmt = @Stmt_ + @DBNameHist + '.dbo.fo'
EXEC(@Stmt)
SET @Stmt = @Stmt_ + @DBNameHist + '.dbo.fo2'
EXEC(@Stmt)
SET @Stmt = @Stmt_ + @DBNameHist + '.dbo.fn'
EXEC(@Stmt)
SET @Stmt = @Stmt_ + @DBNameHist + '.dbo.b_pagcentral'
EXEC(@Stmt)

GO

Grant Execute On sp_EliminarDadosHist to Public
Grant Control On sp_EliminarDadosHist to Public
GO

EXEC sp_MS_MarkSystemObject sp_EliminarDadosHist
GO
