SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_logs_getDocsFarm]') IS NOT NULL
	drop procedure dbo.up_logs_getDocsFarm
go



create procedure dbo.up_logs_getDocsFarm

AS

	select 
		ISNULL(a.data_envio, '19000101') as dtenvio
		,a.loja
		,a.tipo
		,a.transid doc
		,a.data
		,a.doc_X3 as docx3
		,a.Rf_Ng as ref
		,0 qtt
		,ISNULL(a.erro, '') AS erro
	from (                 
		select
			t.tiposaft tipo
			,t.tiposaft+'/'+convert(varchar(4),f.ndoc)+'/'+convert(varchar(6),f.fno) transid
			,convert(varchar(8),f.fdata,112) data
			,(select u.no_ext from b_utentes u where u.no = f.no) cod_X3
			,e.siteext loja
			,e.site descricao
			,f.ftstamp
			,t.nmdocext tipo_doc
			,t.nmdocext+'-'+e.siteext+'-'+substring(convert(varchar(4),f.ftano),3,2)+'-'+convert(varchar(6),format(f.fno, '000000')) doc_X3
			,'' erro
			,'' Rf_Ng
			,'19000101' data_envio
		from ft f (NOLOCK)
			join empresa e (NOLOCK) ON f.site = e.site
			join td t (NOLOCK) on f.ndoc = t.ndoc and t.site = e.site
			join Rep_Control(nolock) as r on r.Identifier = f.ftstamp
		where
		t.tiposaft <> 'PP'
		and t.nmdoc not like '%Proforma%'
		and r.Processed = 0
		and r.operation = 'I'
		and r.table_name = 'FT'

	) a
	order by  a.loja asc, a.data  asc

GO
Grant Execute On dbo.up_logs_getDocsFarm to Public
Grant Control On dbo.up_logs_getDocsFarm to Public
GO
