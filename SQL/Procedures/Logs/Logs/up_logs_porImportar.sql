/*

	Retorna o LOG de Documentos por integrar MECO

	exec up_logs_porImportar 'ERRO', '', '', '19000101', '20220714'

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_logs_porImportar]') IS NOT NULL
	drop procedure dbo.up_logs_porImportar
go

create procedure dbo.up_logs_porImportar
	@type AS VARCHAR(50) = '',
	@name AS VARCHAR(100) = '',
	@site AS VARCHAR(60) = '',
	@dateI AS DATE = '19000101',
	@dateF AS DATE = '19000101',
	@descr AS VARCHAR(20) = ''


AS

SET NOCOUNT ON

	DECLARE @sql VARCHAR(MAX) = N'

	SELECT 
		x.type,
		x.name,
		x.description,
		(CASE WHEN x.date <> ''19000101'' THEN x.date ELSE x.ftDate END) AS date,
		x.registerStamp,
		''FT'' AS tableName,
		x.site,
		x.stamp
	FROM 
		(
			SELECT 
				ISNULL(extLog.type, ''PORIMPORTAR'') AS type,
				ISNULL(extLog.name, '''') AS name,
				ISNULL(cast(extLog.description as varchar(200)), '''') AS description,
				ISNULL(extLog.date, ''19000101'') AS date,
				ISNULL(ft.fdata, ''19000101'') AS ftDate,
				ft.ftstamp AS registerStamp,
				ft.site,
				ft.fdata,
				ft.fno,
				ISNULL( extLog.stamp, '''') as stamp

			FROM
				ft(nolock)
				inner join table_sync_status (nolock) on ft.ftstamp=table_sync_status.regstamp
				left join LogsExternal.dbo.ExternalCommunicationLogs (nolock) as extLog ON ft.ftstamp=extLog.registerStamp
			WHERE 
				table_sync_status.sync = 0
				and ft.nmdoc not like (''%Proforma%'')'

				IF @type <> ''
				BEGIN

					SET @sql = @sql + N'
					AND ISNULL(extLog.type, ''PORIMPORTAR'') = ''' + @type + ''''

				END

				IF @name <> ''
				BEGIN

					SET @sql = @sql + N'
					AND ISNULL(extLog.name, '''') = ''' + @name + ''''

				END

				IF @site <> ''
				BEGIN

					SET @sql = @sql + N'
					AND ft.site = ''' + @site + ''''

				END
				
				IF @descr <> ''
				BEGIN

					SET @sql = @sql + N'
					AND description like ''%' + @descr + '%'''

				END


SET @sql = @sql + N'
		) AS X
WHERE 
	1=1'
/*
	IF @dateI <> '19000101'
	BEGIN

		SET @sql = @sql + N'
		AND x.date >= ''' + CONVERT(VARCHAR(20), @dateI, 112) + ''''

	END

	IF @dateF <> '19000101'
	BEGIN

		SET @sql = @sql + N'
		AND x.date <= ''' + CONVERT(VARCHAR(20), @dateF, 112) + ''''

	END
*/
SET @sql =  @sql + N'
ORDER BY 
	x.fdata, x.fno'

	PRINT @sql
	EXECUTE (@sql)

GO
Grant Execute On dbo.up_logs_porImportar to Public
Grant Control On dbo.up_logs_porImportar to Public
GO