/*

	Retorna o LOG de Documentos integrados MECO

	exec up_logs_importados 'ERRO', '', '', '19000101', '20220714', ''

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_logs_importados]') IS NOT NULL
	drop procedure dbo.up_logs_importados
go

create procedure dbo.up_logs_importados
	@type AS VARCHAR(50) = '',
	@name AS VARCHAR(100) = '',
	@site AS VARCHAR(60) = '',
	@dateI AS DATE = '19000101',
	@dateF AS DATE = '19000101',
	@tableName AS VARCHAR(50) = ''


AS

SET NOCOUNT ON

	DECLARE @sql VARCHAR(MAX) = N'

	select 
		type, 
		name, 
		cast(description as varchar(200)) as description, 
		date, 
		registerStamp,
		tableName, 
		site,
		stamp
	from 
		LogsExternal.dbo.ExternalCommunicationLogs
	where 
		convert(varchar(8),date,112) between ''' + CONVERT(VARCHAR,@dateI, 112) + ''' and ''' + CONVERT(VARCHAR,@dateF, 112) + '''
	'

	IF @type <> ''
	BEGIN

		SET @sql = @sql + N'
		AND type = ''' + @type + ''''

	END

	IF @name <> ''
	BEGIN

		SET @sql = @sql + N'
		AND name = ''' + @name + ''''

	END

	IF @site <> ''
	BEGIN

		SET @sql = @sql + N'
		AND site = ''' + @site + ''''

	END

	IF @tableName <> ''
	BEGIN

		SET @sql = @sql + N'
		AND tableName = ''' + @tableName + ''''

	END

	SET @sql =  @sql + N'
	ORDER BY 
		date desc'

	PRINT @sql
	EXECUTE (@sql)

GO
Grant Execute On dbo.up_logs_importados to Public
Grant Control On dbo.up_logs_importados to Public
GO