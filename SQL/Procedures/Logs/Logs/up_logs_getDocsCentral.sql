/*

	EXEC up_logs_getDocsCentral 'ATLANTICO', '19000101', '20230130', 'PORINTEGRAR', '[172.20.90.17]'

*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_logs_getDocsCentral]') IS NOT NULL
	drop procedure dbo.up_logs_getDocsCentral

go

create procedure up_logs_getDocsCentral
	 @site				as varchar(40)
	, @dataini			as varchar(8)
	, @datafim			as varchar(8)
	, @tipo				as varchar(40)
	, @ip				as varchar(50)

AS

	DECLARE @RunStoredProcSQL VARCHAR(MAX)

	SET @RunStoredProcSQL = N'

		select
			ISNULL(sync_date , ''19000101'') as dtenvio
			,e.siteext as loja
			,t.tiposaft AS tipo
			,t.tiposaft+''/''+convert(varchar(4),ft.ndoc)+''/''+convert(varchar(6),ft.fno) doc
			,convert(varchar(8),ft.fdata,112) as data
			,t.nmdocext+''-''+e.siteext+''-''+substring(convert(varchar(4),ft.ftano),3,2)+''-''+convert(varchar(6),format(ft.fno, ''000000''))  as docx3
			,'''' ref
			,0 as qtt
			,convert(varchar(254),ISNULL((select top 1 description from  [172.20.90.17].LogsExternal.dbo.ExternalCommunicationLogs with (nolock) where registerStamp=ft.ftstamp order by date desc),'''')) AS erro
		from 
			' + @ip + '.[Mecofarma].dbo.ft with (nolock) 
			join ' + @ip + '.[Mecofarma].dbo.table_sync_status with (nolock) on ft.ftstamp=table_sync_status.regstamp
			join ' + @ip + '.[Mecofarma].dbo.empresa e with (NOLOCK) ON ft.site = e.site
			join ' + @ip + '.[Mecofarma].dbo.td t with (NOLOCK) on ft.ndoc = t.ndoc and t.site = e.site
		where
		t.tiposaft <> ''PP''
		and t.nmdoc NOT LIKE ''%Proforma%''
		and table_sync_status.sync=0
		and ft.site= ''' + @site + '''
		and ft.fdata between ''' + @dataini + ''' and ''' + @datafim + '''

	'
	print @RunStoredProcSQL
	EXEC (@RunStoredProcSQL)

GO
Grant Execute On dbo.up_logs_getDocsCentral to Public
Grant Control On dbo.up_logs_getDocsCentral to Public
Go

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO