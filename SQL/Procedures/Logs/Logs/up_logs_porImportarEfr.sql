SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('[dbo].[up_logs_porImportarEfr]') IS NOT NULL
    DROP PROCEDURE dbo.up_logs_porImportarEfr
GO

CREATE PROCEDURE up_logs_porImportarEfr
    @site         AS VARCHAR(40),
    @dataini      AS VARCHAR(8),
    @datafim      AS VARCHAR(8),
    @integrado    AS INT = -1
AS
BEGIN
    DECLARE @RunStoredProcSQL VARCHAR(MAX)

    SET @RunStoredProcSQL = N'

    WITH LastLog AS (
        SELECT 
            ec.registerStamp,
            ec.description,  -- Coluna relevante da tabela LogsExternal
            ROW_NUMBER() OVER (PARTITION BY ec.registerStamp ORDER BY ec.date DESC) AS RowNum
        FROM LogsExternal.dbo.ExternalCommunicationSapLogs (NOLOCK) ec
    )

    SELECT
        sel = CONVERT(BIT, 0),
        ISNULL(sync_date, ''19000101'') AS dtenvio,
        e.siteext AS loja,
        t.tiposaft AS tipo,
        (SELECT ndoc FROM uf_nDoc_Mecofarma(ft.ftstamp)) AS doc,
        CONVERT(VARCHAR(10), ft.fdata, 120) AS data,
        ISNULL(LastLog.description, '''') AS description,
        CASE WHEN ISNULL(table_sync_status_sap.sync, 0) = 1 THEN ''Sim'' ELSE ''N�o'' END AS integrado,
        ft.ftstamp
    FROM 
        ft (NOLOCK) 
        INNER JOIN table_sync_status_sap (NOLOCK) ON ft.ftstamp = table_sync_status_sap.regstamp
        INNER JOIN empresa e (NOLOCK) ON ft.site = e.site
        INNER JOIN td t (NOLOCK) ON ft.ndoc = t.ndoc AND t.site = e.site
        LEFT JOIN LastLog ON LastLog.registerStamp collate DATABASE_DEFAULT  = ft.ftstamp  collate DATABASE_DEFAULT  AND LastLog.RowNum = 1
    WHERE
        t.tiposaft <> ''PP''
        AND t.nmdoc NOT LIKE ''%Proforma%''
        AND ft.etotal != 0
        AND ft.site = ''' + @site + '''
        AND ft.fdata BETWEEN ''' + @dataini + ''' AND ''' + @datafim + ''''
        
    IF(@integrado = 0)
        SET @RunStoredProcSQL = @RunStoredProcSQL + N'
        AND table_sync_status_sap.sync = 0 '

    SET @RunStoredProcSQL = @RunStoredProcSQL + N'
    ORDER BY ft.ousrdata + ft.ousrhora ASC'

    PRINT @RunStoredProcSQL
    EXEC (@RunStoredProcSQL)
END
GO

GRANT EXECUTE ON dbo.up_logs_porImportarEfr TO Public
GRANT CONTROL ON dbo.up_logs_porImportarEfr TO Public
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
