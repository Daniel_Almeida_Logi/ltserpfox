--select * from B_Parameters_site where site='Loja 1' order by stamp
--select distinct type from B_Parameters_site
--select distinct type from B_Parameters
-- select * from B_Parameters_site order by stamp desc

DECLARE @site VARCHAR(20)
declare emp_cursor cursor for
select site as siteemp from empresa

open emp_cursor
fetch next from emp_cursor into @site

while @@FETCH_STATUS = 0
begin
	IF not EXISTS (SELECT 1 FROM B_Parameters_site WHERE stamp='ADM0000000094' and site=@site)
	BEGIN
		insert into B_Parameters_site (stamp, name, Type, textValue, numValue, bool, ODate, LDate, visivel, MostraTextValue, MostraNumValue, MostraBool, ListaTextValue, Unidades, Site, obs)
		values ('ADM0000000094', 'Email para enviar Documentos', 'Admin', '', 0, 0, getdate(), getdate(), 1, 0, 0, 1, 'Email para enviar Documentos', '', @site, '')
	END
	fetch next from emp_cursor into @site
end
close emp_cursor
deallocate emp_cursor


