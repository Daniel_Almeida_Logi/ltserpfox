-- get ConfigEmail

-- exec usp_get_configEmailFarmacia 'Loja 1'


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[usp_get_configEmailFarmacia]') IS NOT NULL
	drop procedure dbo.usp_get_configEmailFarmacia
go

create procedure dbo.usp_get_configEmailFarmacia

@site		varchar(20)

AS
SET NOCOUNT ON
	SELECT 
	 TOP 1
		email_smtp		AS smtp , 
		email_username  AS username, 
		email_password  AS pw, 
		email_port      AS port, 
		email_ssl       AS ssl, 
		email_tls		AS tls
	FROM empresa
	WHERE site = (CASE WHEN  @site ='' THEN site ELSE @site END) 
		AND email_username !=''

	
GO
Grant Execute On dbo.usp_get_configEmailFarmacia to Public
Grant Control On dbo.usp_get_configEmailFarmacia to Public
Go