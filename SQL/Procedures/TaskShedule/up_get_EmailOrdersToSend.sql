/*  informa��o relativamente a encomenda 
	EXEC up_get_EmailOrdersToSend '9DC881F5-D563-42A0-AC5D-21CF67CD3F41'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_EmailOrdersToSend]') IS NOT NULL
	drop procedure dbo.up_get_EmailOrdersToSend
go

CREATE PROCEDURE [dbo].[up_get_EmailOrdersToSend]
	@token					varchar(36)		
/* with encryption */
AS
SET NOCOUNT ON
BEGIN	

	DECLARE @bostamp varchar(36)
	DECLARE @typeUser int
	SELECT @bostamp =tableStamp , @typeUser=typeUser FROM docsToSendSchedule WHERE tokenDocToSend=@token

	SELECT 
		bo.bostamp																					AS bostamp, 
		obrano																						AS numdoc,
		ndos																						AS seriesNumber,
		bo.site																						AS site,
		boano																						AS year, 
		''																							AS paymentURL,
		''																							AS imageSitePath,
		'Resumo da Encomenda n� ' + Convert(varchar(10),Obrano)										AS subject,
		b_utentes.email																				AS email,
		nomePais																					AS country,
		case bo.moeda 
			when "EURO"  then "�"
			when "USD"   then "$"
			else ""		end																			AS currency,
		@typeUser																					AS typeUser,
		b_utentes.local																				AS city, 
		b_utentes.morada																			AS address,		
		b_utentes.codpost																			AS zipCode,
		nmdos                                                                                       AS nameDoc,
		empresa.nomecomp																			AS CompanyName,
		addressCompany																			    AS CompanyAddress,
		empresa.telefone + ", " + empresa.emp_email_username										AS CompanyContact,
		websiteUrl																					AS websiteUrl,
		websiteImg																					AS websiteImg,
		case when b_utentes.tlmvl !='' then  b_utentes.tlmvl else b_utentes.telefone end 			AS phone 
	from  bo (nolock)
	INNER JOIN b_utentes (nolock) ON bo.no	=b_utentes.no and bo.estab = b_utentes.estab
	INNER JOIN b_cli_tabela_pais (nolock) ON b_utentes.codigop= b_cli_tabela_pais.code
	INNER JOIN empresa (nolock) ON BO.SITE= EMPRESA.SITE
	WHERE bostamp=@bostamp

END 
GO
GRANT EXECUTE ON dbo.up_get_EmailOrdersToSend to Public
GRANT CONTROL ON dbo.up_get_EmailOrdersToSend to Public
GO
