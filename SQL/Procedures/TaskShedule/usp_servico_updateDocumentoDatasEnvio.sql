/*  update de datas dos documentos 
	EXEC usp_servico_updateDocumentoDatasEnvio '1','Loja 1'
	 exec usp_servico_updateDocumentoDatasEnvio 11,'Loja 1'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[usp_servico_updateDocumentoDatasEnvio]') IS NOT NULL
	drop procedure dbo.usp_servico_updateDocumentoDatasEnvio
go

CREATE PROCEDURE [dbo].[usp_servico_updateDocumentoDatasEnvio]
	@id						int,
	@site					varchar(60)				
AS
SET NOCOUNT ON

 DECLARE @frequency VARCHAR(60)
 DECLARE @dateLastMonth DATE

 select @frequency = frequency from docsToSend 	WHERE id=@id and site = @site 


	
	update docsToSend
	SET 
	dateNextCall = CASE WHEN frequency= 'HORA'		 then DATEADD(HOUR, 1, dateNextCall) 
						WHEN frequency= '4HORA'		 then DATEADD(HOUR, 4, dateNextCall) 
						WHEN frequency= 'DIARIO'     then DATEADD(DAY, 1, dateNextCall) 
				        WHEN frequency= 'MENSAL'	 then DATEADD(MONTH, 1, dateNextCall) 
						WHEN frequency= 'TRIMESTRAL' then DATEADD(MONTH, 3, dateNextCall) 
						WHEN frequency= 'ANUAL'		 then DATEADD(YEAR, 1, dateNextCall) 
						WHEN frequency= 'SEMANAL'	 then DATEADD(DAY, 7, dateNextCall) 
						WHEN frequency= '4SEMANAL'	 then DATEADD(DAY, 28, dateNextCall) 
						ELSE '' 
					END
	WHERE 
		id=@id and site = @site

	SET @dateLastMonth = (select DATEADD(MONTH, -1,CONVERT(date,  (select dateNextCall from docsToSend 	WHERE id=@id and site = @site) )))		

	update docsToSendParameters
	SET 
		value =	CASE	WHEN @frequency= 'HORA'			THEN CONVERT(varchar(10),(select DATEADD(HOUR, 1,CONVERT(datetime, value))))
						--WHEN @frequency= '4HORA'		Não tem aqui porque é preciso fazer o update da dataini e datafim separadamente pois há situações que dataini != datafim ex: entre 08-08-2024 23:00:00 e 09-08-2024 03:00:00. Update no final da SP
						WHEN @frequency= 'DIARIO'		THEN CONVERT(varchar(10),(select DATEADD(DAY, 1,CONVERT(date, value))))
				        WHEN @frequency= 'MENSAL'		THEN (CASE WHEN MONTH(CONVERT(date, value))!=2 
																THEN (CASE WHEN (DAY(CONVERT(date, value)) <27)  
																			THEN (select  CONVERT(varchar(10),DATEADD(MONTH, 1,CONVERT(date, value)))) 
																			ELSE  CONVERT(varchar(10),(select eomonth(CONVERT(date, value),1))) END)
																ELSE (CASE WHEN (DAY(CONVERT(date, value)) <27)  
																			THEN (select  CONVERT(varchar(10),DATEADD(MONTH, 1,CONVERT(date, value)))) 
																			ELSE  CONVERT(varchar(10),(select eomonth(CONVERT(date, value),1))) END)
															END)   
				        WHEN @frequency= 'TRIMESTRAL'	THEN (CASE	WHEN MONTH(CONVERT(date, value))!=2 
																	THEN (CASE	WHEN (DAY(CONVERT(date, value)) <27)  
																				THEN (select  CONVERT(varchar(10),DATEADD(MONTH, 3,CONVERT(date, value)))) 
																				ELSE  CONVERT(varchar(10),(select eomonth(CONVERT(date, value),3))) END)
																	ELSE (CASE	WHEN (DAY(CONVERT(date, value)) <27)  
																				THEN (select  CONVERT(varchar(10),DATEADD(MONTH, 3,CONVERT(date, value)))) 
																				ELSE  CONVERT(varchar(10),(select eomonth(CONVERT(date, value),3))) END)
															END)   						

	
						WHEN @frequency= 'ANUAL'		THEN CONVERT(varchar(10),(select DATEADD(YEAR, 1,CONVERT(date, value)))) 
						WHEN @frequency= 'SEMANAL'		THEN CONVERT(varchar(10),(select DATEADD(DAY, 7,CONVERT(date, value))))
						WHEN @frequency= '4SEMANAL'		THEN DATEADD(DAY, 28, CONVERT(date, value)) 
					END
	WHERE 
		idDocs=@id and site = @site and type in ('d','d1')


	UPDATE docsToSendParameters
		SET 
			value = CASE    
						WHEN @frequency = 'HORA' THEN CONVERT(varchar, DATEADD(HOUR, 1, CONVERT(datetime, VALUE)), 120)
						WHEN @frequency = 'DIARIO' THEN CONVERT(varchar, DATEADD(DAY, 1, CONVERT(datetime, VALUE)), 120)
                
						-- Para casos mensais
						WHEN @frequency = 'MENSAL' THEN 
							CASE 
								-- Se o dia for maior ou igual a 28, ajusta para o último dia do próximo mês
								WHEN DAY(CONVERT(datetime, value)) >= 28 
									THEN CONVERT(varchar, EOMONTH(DATEADD(MONTH, 1, CONVERT(datetime, VALUE))), 120)
								-- Caso contrário, apenas adiciona um mês normalmente
								ELSE CONVERT(varchar, DATEADD(MONTH, 1, CONVERT(datetime, VALUE)), 120)
							END
                
						-- Para casos trimestrais
						WHEN @frequency = 'TRIMESTRAL' THEN 
							CASE 
								-- Se o dia for maior ou igual a 28, ajusta para o último dia do próximo trimestre
								WHEN DAY(CONVERT(datetime, value)) >= 28 
									THEN CONVERT(varchar, EOMONTH(DATEADD(MONTH, 3, CONVERT(datetime, VALUE))), 120)
								-- Caso contrário, adiciona três meses
								ELSE CONVERT(varchar, DATEADD(MONTH, 3, CONVERT(datetime, VALUE)), 120)
							END
                
						WHEN @frequency = 'ANUAL' THEN CONVERT(varchar, DATEADD(YEAR, 1, CONVERT(datetime, VALUE)), 120)
						WHEN @frequency = 'SEMANAL' THEN CONVERT(varchar, DATEADD(DAY, 7, CONVERT(datetime, VALUE)), 120)
						WHEN @frequency = '4SEMANAL' THEN CONVERT(varchar, DATEADD(DAY, 28, CONVERT(datetime, VALUE)), 120)
					END
		WHERE 
			idDocs=@id and site = @site and type in ('dt')
		

	update docsToSendParameters --> para as stiuacoes como o saft diario 
	SET 
			value =	(SELECT DATEADD(DAY,1,EOMONTH((select dateNextCall from docsToSend where id= @id),-1)))
					
	WHERE 
		idDocs=@id and site = @site and type='di'


	update docsToSendParameters
	SET 
		value =	(SELECT CONVERT(varchar(10),YEAR(@dateLastMonth)))
					
	WHERE 
		idDocs=@id and site = @site and type='vcA'

	update docsToSendParameters
	SET 
		value =	(SELECT CONVERT(varchar(10),MONTH(@dateLastMonth)))
					
	WHERE 
		idDocs=@id and site = @site and type='vcM'

----> para as casos de SPs com horaini e horafim com incrementos de 4 em 4h para mudar as horas e a data com base no datenextcall corretamente. 
	--criado o type h4 para isto. 	
	
	update docsToSendParameters SET value =	CONVERT(varchar,(select DATEADD(HOUR, -4,CONVERT(datetime, (select dateNextCall from docsToSend where id= @id)))),108)
		WHERE idDocs=@id and site = @site and type='h4' and name = 'horaIni'

	update docsToSendParameters SET	value =	CONVERT(varchar,CONVERT(datetime, (select dateNextCall from docsToSend where id= @id)),108)
		WHERE idDocs=@id and site = @site and type='h4' and name = 'horaFim'

	update docsToSendParameters SET value =	CONVERT(varchar,(select DATEADD(HOUR, -4,CONVERT(datetime, (select dateNextCall from docsToSend where id= @id)))),23)
		WHERE idDocs=@id and site = @site and type='h4' and name = 'dataIni'

	update docsToSendParameters SET	value =	CONVERT(varchar,CONVERT(datetime, (select dateNextCall from docsToSend where id= @id)),23)
		WHERE idDocs=@id and site = @site and type='h4' and name = 'dataFim'


GO
GRANT EXECUTE ON dbo.usp_servico_updateDocumentoDatasEnvio to Public
GRANT CONTROL ON dbo.usp_servico_updateDocumentoDatasEnvio to Public
GO

