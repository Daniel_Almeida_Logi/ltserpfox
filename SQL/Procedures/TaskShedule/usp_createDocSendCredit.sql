/* Criar o envio de clientes em credito 

	exec usp_createDocSendCredit  '20230818','Loja 1'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].usp_createDocSendCredit') IS NOT NULL
	drop procedure dbo.usp_createDocSendCredit
go

CREATE PROCEDURE [dbo].[usp_createDocSendCredit]
	@dataFim			AS DATETIME,
	@site				AS VARCHAR(60)
AS
SET NOCOUNT ON
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosClientes'))
		DROP TABLE #dadosClientes

	create table #dadosClientes(
		no		NUMERIC(10,0), 
		estab	NUMERIC(3,0), 
		nome	VARCHAR(80), 
		d15		NUMERIC(10,2), 
		d30		NUMERIC(10,2), 
		d60		NUMERIC(10,2), 
		d90		NUMERIC(10,2), 
		d180	NUMERIC(10,2), 
		d270	NUMERIC(10,2), 
		d360	NUMERIC(10,2), 
		d540	NUMERIC(10,2), 
		d720	NUMERIC(10,2), 
		m720	NUMERIC(10,2), 
		saldo	NUMERIC(15,2), 
		plafond NUMERIC(15,2),
		util    NUMERIC(15,2),  
		alimite	NUMERIC(5,0),
		valorVencido NUMERIC(15,2) 
	)


	INSERT INTO #dadosClientes
	exec up_controlo_credito_antiguidade 0,@dataFim,'',''

	Declare @sendno		VARCHAR(20)
	Declare @sendestab	VARCHAR(20)
	Declare @sendnome	VARCHAR(80)
	Declare @sendEmail	VARCHAR(254)
	Declare @maxID	NUMERIC(10,0)

	Declare @body varchar(max)
	Declare @emails varchar(200)


	select top 1 @body=email_body_ccNR , @emails=email from empresa (nolock) where site=@site

	DECLARE emp_cursor CURSOR FOR
	select CONVERT(VARCHAR(20) ,#dadosClientes.no),CONVERT(VARCHAR(20) ,#dadosClientes.estab),#dadosClientes.nome,email= ( case when @emails !='' then  b_utentes.email +';' + @emails else b_utentes.email end)
		FROM #dadosClientes
		inner join b_utentes on #dadosClientes.no = b_utentes.no and #dadosClientes.estab = b_utentes.estab 
		WHERE b_utentes.email !='' and autoriza_emails=1 and autorizado=1 and #dadosClientes.valorVencido>0 and b_utentes.autoriza_envio_ccNR=1
	OPEN emp_cursor
	FETCH NEXT FROM emp_cursor INTO @sendno , @sendestab,@sendnome,@sendEmail
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		
	    SET @maxID	= ISNULL((SELECT   MAX(ISNULL(ID,0)) + 1000 FROM docsToSend),1000)
	
		IF(NOT EXISTS(SELECT * FROM docsToSend WHERE id=@maxID AND SITE=@site))
		BEGIN
			INSERT INTO docsToSend(TOKEN,id,site,nameDoc,usp,typeDocSave,typeSend,frequency,typeExec,dateNextCall,state,toSend,visibility,delimiter,zip,subject,body)
			values(LEFT(NEWID(),36),@maxID,@site, 'Relat�rio Extrato de Conta Corrente N�o Regularizado de Cliente','relatorio_nReg_Cliente','PDF','EMAIL','UNICO','RELATORIO',DATEADD(hh, 1, GETDATE()),1,@sendEmail,1,'',0,'Relat�rio Extrato de Conta Corrente N�o Regularizado',@body)
		END
	
		--  ano,client,estab,mesIni,mesFim,op,familia,atributosFam,lab,marca,site,&incliva
		if(not exists(select * from docsToSendParameters WHERE idDocs=@maxID AND SITE=@site))
		BEGIN
			INSERT INTO docsToSendParameters (token,idDocs,site,name,type,value,sequence)
			VALUES
				  (LEFT(NEWID(),36),@maxID,'Loja 1','No','vc',@sendno,1),
				  (LEFT(NEWID(),36),@maxID,'Loja 1','estab','t',@sendestab,2),
				  (LEFT(NEWID(),36),@maxID,'Loja 1','verTodos','vc','3',3),
				  (LEFT(NEWID(),36),@maxID,'Loja 1','loja','vc','',4),
				  (LEFT(NEWID(),36),@maxID,'Loja 1','site','vc',@site,5),
				  (LEFT(NEWID(),36),@maxID,'Loja 1','modo','l','True',6)
		END
		FETCH NEXT FROM emp_cursor INTO @sendno , @sendestab,@sendnome,@sendEmail
	END
	CLOSE emp_cursor
	DEALLOCATE emp_cursor




	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosClientes'))
		DROP TABLE #dadosClientes

GO
GRANT EXECUTE ON dbo.usp_createDocSendCredit to Public
GRANT CONTROL ON dbo.usp_createDocSendCredit to Public
GO