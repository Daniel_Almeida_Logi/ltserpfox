/*  lista de documentos a enviar 

	exec dbo.usp_servico_ChamaDocumentoEnviar 21,'Loja 1','up_relatorio_exportInfoPrex'
	exec dbo.usp_servico_ChamaDocumentoEnviar 70,'Loja 1','up_getDocsFailedToSendX3'
	select * from docsToSend order by dateNextCall asc
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[usp_servico_ChamaDocumentoEnviar]') IS NOT NULL
	drop procedure dbo.usp_servico_ChamaDocumentoEnviar
go

CREATE PROCEDURE [dbo].[usp_servico_ChamaDocumentoEnviar]
	@idDoc				NUMERIC(36),
	@site				VARCHAR(60),
	@usp				VARCHAR(500)
AS
SET NOCOUNT ON

declare @sql NVARCHAR(max)
select @sql = N' ' +@usp + ' ' 

	SELECT 
		@sql += COALESCE(	
			(CASE WHEN TYPE in( 'l','s','vcA','vcM') THEN VALUE
			 ELSE '''' +VALUE +'''' END + ', '),VALUE)
	FROM docsToSendParameters
	WHERE idDocs = @idDoc and site=@site
	order by docsToSendParameters.sequence asc 

	if((select count(*)  	FROM docsToSendParameters WHERE idDocs = @idDoc and site=@site)>0)
	BEGIN
		SET @sql = (SELECT LEFT(@sql, LEN(@sql) - 1))
	END 
	PRINT @sql
	EXEC sp_executesql  @sql

GO
GRANT EXECUTE ON dbo.usp_servico_ChamaDocumentoEnviar to Public
GRANT CONTROL ON dbo.usp_servico_ChamaDocumentoEnviar to Public
GO