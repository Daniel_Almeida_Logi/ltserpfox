/*  update de datas dos documentos unicos
	EXEC usp_servico_updateDocumentosUnicos '',''

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[usp_servico_updateDocumentosUnicos]') IS NOT NULL
	drop procedure dbo.usp_servico_updateDocumentosUnicos
go

CREATE PROCEDURE [dbo].[usp_servico_updateDocumentosUnicos]
	@token				VARCHAR(36),
	@emailHtml			VARCHAR(MAX),
	@usp				VARCHAR(254)
AS
SET NOCOUNT ON
	Declare @email varchar(254)
	declare @id numeric(18,0)

	select @email = toSend , @id=id from docsToSend where token=@token

	IF((select frequency from docsToSend 	WHERE token=@token)='UNICO')
	BEGIN

		IF(EXISTS(SELECT * FROM docsToSendSchedule WHERE tokenDocToSend=@token))
		BEGIN
			UPDATE docsToSendSchedule SET sent=1, emailHtml=@emailHtml, email=@email, dateSent=GETDATE() WHERE tokenDocToSend=@token
		END
		ELSE
		BEGIN
			INSERT INTO docsToSendSchedule (stamp,tokenDocToSend,tableName,tableStamp,status,typeUser,typeUserDesc,ousrdata,sent,email,emailHtml,dateSent)
			VALUES (LEFT(NEWID(),36), @token,'','','',3,@usp ,GETDATE(),1,@email,@emailHtml,GETDATE())
		END 


		DELETE docsToSend WHERE token=@token
		
		delete docsToSendParameters where idDocs=@id

	END 
GO
GRANT EXECUTE ON dbo.usp_servico_updateDocumentosUnicos to Public
GRANT CONTROL ON dbo.usp_servico_updateDocumentosUnicos to Public
GO