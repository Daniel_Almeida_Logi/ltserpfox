/*  lista de documentos a enviar 
	EXEC usp_servico_credenciaisEnvio 'Loja 1'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[usp_servico_credenciaisEnvio]') IS NOT NULL
	drop procedure dbo.usp_servico_credenciaisEnvio
go

CREATE PROCEDURE [dbo].[usp_servico_credenciaisEnvio]
	@site				VARCHAR(60)
AS
SET NOCOUNT ON
	
	SELECT 
		  (SELECT textValue FROM B_Parameters_site WHERE STAMP='ADM0000000093' AND SITE=@site) AS token,
		  (SELECT textValue FROM B_Parameters_site WHERE STAMP='ADM0000000094' AND SITE=@site) AS emails,
		  ''																				   AS ftppath

GO
GRANT EXECUTE ON dbo.usp_servico_credenciaisEnvio to Public
GRANT CONTROL ON dbo.usp_servico_credenciaisEnvio to Public
GO