/*   

	exec up_getDocsToSendExecOnly '2022-06-01'

*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_getDocsToSendExecOnly]') IS NOT NULL
	DROP PROCEDURE dbo.up_getDocsToSendExecOnly
GO

CREATE PROCEDURE dbo.up_getDocsToSendExecOnly
@DateR   date
/* WITH ENCRYPTION */
AS
	Declare @str VARCHAR(MAX)

	
	if OBJECT_ID('tempdb.dbo.#tableCompare') is not null
		drop table #tableCompare

	set @str = '<u>Relatorio diario comp. Mecofarma </u>'


	SELECT 
		'<br><br><u>'  +a.site + ': </u><br>' + STRING_AGG (isnull(a.message, ' '), '<br> ') as Descricao 

	into #tableCompare
	FROM
		docsToSendExecOnly A
	WHERE dateExec = @DateR and tosend=1
	Group by site,dateExec 
	ORDER BY site asc 


	SELECT 
		@str as Descricao 
	UNION ALL
	select Descricao from #tableCompare

	if OBJECT_ID('tempdb.dbo.#tableCompare') is not null
		drop table #tableCompare

GO
Grant Execute On dbo.up_getDocsToSendExecOnly to Public
Grant Control On dbo.up_getDocsToSendExecOnly to Public
Go

