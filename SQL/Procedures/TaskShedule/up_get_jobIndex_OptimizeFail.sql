/*  lista de jobs que falharam desde de 
	EXEC up_get_jobIndex_OptimizeFail '2024-02-28'

	EXEC up_get_jobIndex_OptimizeFail '2024-04-20'

	EXEC up_get_jobIndex_OptimizeFail '2024-04-20'

	EXEC up_get_jobIndex_OptimizeFail '2023-05-29'
*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_jobIndex_OptimizeFail]') IS NOT NULL
	drop procedure dbo.up_get_jobIndex_OptimizeFail
go

CREATE PROCEDURE [dbo].[up_get_jobIndex_OptimizeFail]
	@date				 DATETIME	
AS
SET NOCOUNT ON

Declare @central bit


select @central=bool from B_Parameters where stamp='ADM0000000353'


	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'#Index_OptimizeFail'))
		DROP TABLE #Index_OptimizeFail


	SELECT *

	into #Index_OptimizeFail

  FROM [master].[dbo].[LTS_CommandLog] (nolock)

  WHERE StartTime<= DATEADD(day,1,@date)  and StartTime>= DATEADD(day,-7,@date)
  order by StartTime desc

  IF ((SELECT ISNULL(count(*),0) from #Index_OptimizeFail where  ErrorNumber !=0) !=0)
  BEGIN
		
		SELECT  (case when @central=1 then   'MECOCENTRAL - Falhou o job Index_Optimize . O n�mero de linhas que falharam foram as seguintes: ' ELSE 'Falhou o job Index_Optimize . O n�mero de linhas que falharam foram as seguintes: ' END ) + convert(varchar(10),count(*)) 
		FROM #Index_OptimizeFail
  END 
  ELSE IF ((select ISNULL(count(*),0) from #Index_OptimizeFail) =0)
  BEGIN
  	SELECT case when @central=1 then 'MECOCENTRAL - JOB n�o correu nos ultimos 7 dias, verificar o que se passa!!!! ' ELSE 'JOB Index_Optimize correu com sucesso '  END
  END 
  ELSE
  BEGIN
  	SELECT  case when @central=1 then 'MECOCENTRAL - JOB Index_Optimize correu com sucesso ' ELSE 'JOB Index_Optimize correu com sucesso '  END
  END 

  
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'#Index_OptimizeFail'))
		DROP TABLE #Index_OptimizeFail

  GO
GRANT EXECUTE ON dbo.up_get_jobIndex_OptimizeFail to Public
GRANT CONTROL ON dbo.up_get_jobIndex_OptimizeFail to Public
GO