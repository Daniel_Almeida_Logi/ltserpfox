
/*  lista de parametros do relatorio a enviar 
	EXEC usp_servico_EnvioDocumentosParametros '1','Loja 1'
	 exec usp_servico_EnvioDocumentosParametros 63,'Loja 1'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[usp_servico_EnvioDocumentosParametros]') IS NOT NULL
	drop procedure dbo.usp_servico_EnvioDocumentosParametros
go

CREATE PROCEDURE [dbo].[usp_servico_EnvioDocumentosParametros]
	@id						int,
	@site					varchar(60)				
AS
SET NOCOUNT ON
	
	SELECT 
		name	AS nome,
		case type 
			 WHEN  'd'   THEN CONVERT(varchar(10),(select CONVERT(date, value)))
			 WHEN  'd1'  THEN CONVERT(varchar(8),cast(value as date),112)
			 WHEN  'NIF' THEN ncont
			 WHEN  'Dbname' THEN DB_NAME()
			 WHEN  'ServerName' then 	convert(varchar,ServerProperty('machinename')) 
								+ case when convert(varchar,(SELECT local_tcp_port FROM   sys.dm_exec_connections WHERE  session_id = @@SPID)) is null then '' else ':' end
								+ isnull(convert(varchar,(SELECT local_tcp_port FROM   sys.dm_exec_connections WHERE  session_id = @@SPID)),'')  
			WHEN   'InstanceName' THEN case when (select ServerProperty('InstanceName')) is null then '' else '\' end + convert(varchar,isnull((select ServerProperty('InstanceName')),''))
			 ELSE  value END	AS valor
	FROM docsToSendParameters
	INNER JOIN empresa ON docsToSendParameters.site = empresa.SITE
	WHERE 
		idDocs=@id and docsToSendParameters.site = @site
	order by docsToSendParameters.site asc, docsToSendParameters.sequence asc 

GO
GRANT EXECUTE ON dbo.usp_servico_EnvioDocumentosParametros to Public
GRANT CONTROL ON dbo.usp_servico_EnvioDocumentosParametros to Public
GO