/* ---SP's para o programa java MCDT--- */ 

/* exec up_MCDT_getCabecalho 'jsD62019E6-82DD-46A1-9E3'*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_MCDT_getCabecalho]') IS NOT NULL
	drop procedure up_MCDT_getCabecalho
go

create PROCEDURE up_MCDT_getCabecalho
@stamp varchar(25)

/* WITH ENCRYPTION */ 
AS 
BEGIN 
	
	SELECT 
		b_cli_presc.prescno
		,b_cli_presc.vias
		,b_cli_presc.data
		,b_cli_presc.mcdt_isento
		,b_utentes.cesd
		,b_utentes.atestado
		,b_utentes.cesdcart
		,b_utentes.cesdcp
		,b_utentes.atnumero
		,b_utentes.atcp
		,b_utentes.attipo
		,b_utentes.cesdval
		,b_utentes.atval
		,codigop = case when b_cli_presc.nrutente='' then b_utentes.codigop else 'PT' end 
		,b_cli_presc.entidadeCod
		,b_cli_presc.nrutente
		,b_cli_presc.validade
		,b_cli_efr.tipo
		,b_cli_presc.mcdt_domicilio
		,b_cli_presc.mcdt_justificacaoD
		,b_cli_presc.mcdt_urgente
		,b_cli_presc.mcdt_justificacaoU
		,b_cli_presc.mcdt_infoClinica
		,b_cli_presc.mcdt_isento
		,b_cli_presc.mcdt_outrasinfo
		,convert(decimal,case when b_cli_presc.drordem='' then '0' else b_cli_presc.drordem end) drordem
		,localPrescEnvioWs = convert(varchar(1),empresa.zonaacss) + empresa.localcod
		,b_us.drclprofi
		,B_cli_presc.nrbenef
	FROM 
		b_cli_presc (nolock)
		inner join b_utentes (nolock) on b_utentes.utstamp = b_cli_presc.utstamp
		left join b_cli_efr (nolock) on b_cli_efr.cod = b_cli_presc.entidadeCod
		inner join empresa (nolock) on empresa.site = b_cli_presc.site
		inner join b_us (nolock) on b_cli_presc.drstamp = b_us.usstamp
	WHERE 
		prescstamp = @stamp
		
END

GO
Grant Execute On up_MCDT_getCabecalho to Public
Grant Control On up_MCDT_getCabecalho to Public
GO
--------------------------------------------------------------


/* exec up_MCDT_getUtente 'jsD62019E6-82DD-46A1-9E3'*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_MCDT_getUtente]') IS NOT NULL
	drop procedure up_MCDT_getUtente
go

create PROCEDURE up_MCDT_getUtente
@stamp varchar(25)

/* WITH ENCRYPTION */ 
AS 
BEGIN 
	
	SELECT 
		bino
		,nascimento
		,duplicado
		,idade = convert(decimal,DATEDIFF(year, nascimento, GETDATE()) - (CASE WHEN DATEADD(year, DATEDIFF(year, nascimento, GETDATE()), nascimento) > GETDATE() THEN 1 ELSE 0 END))
		,nome
		,nomesproprios
		,convert(decimal, case when nbenef='' then '0' else nbenef end) nutente
		,obito
		,codigop
		,codigopNat
		,sexo
		,apelidos
	FROM 
		b_utentes (nolock)
	WHERE
		utstamp = (select utstamp from b_cli_presc where b_cli_presc.prescstamp = @stamp)
		
END

GO
Grant Execute On up_MCDT_getUtente to Public
Grant Control On up_MCDT_getUtente to Public
GO
--------------------------------------------------------------

/* exec up_MCDT_getAnulado 'jsD62019E6-82DD-46A1-9E3'*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_MCDT_getAnulado]') IS NOT NULL
	drop procedure up_MCDT_getAnulado
go

create PROCEDURE up_MCDT_getAnulado
@stamp varchar(25)

/* WITH ENCRYPTION */ 
AS 
BEGIN 
	
	SELECT
		anulado
		,anuladata
		,anulamotivoCod
		,anulamotivoDesc
	FROM
		b_cli_presc (nolock)
	WHERE
		prescstamp = @stamp
		
END

GO
Grant Execute On up_MCDT_getAnulado to Public
Grant Control On up_MCDT_getAnulado to Public
GO
--------------------------------------------------------------


/* exec up_MCDT_getItemsReq 'jsD62019E6-82DD-46A1-9E3'*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_MCDT_getItemsReq]') IS NOT NULL
	drop procedure up_MCDT_getItemsReq
go

create PROCEDURE up_MCDT_getItemsReq
@stamp varchar(25)

/* WITH ENCRYPTION */ 
AS 
BEGIN 
	
	SELECT
		mcdt_area
		,mcdt_infoClinica
		,mcdt_codLateral
		,mcdt_codconv
		,mcdt_sinonimo
		,convert(decimal,mcdt_qtt) mcdt_qtt
		,mcdt_outro
		,mcdt_codProduto
	FROM
		b_cli_prescl (nolock)
	WHERE
		prescstamp = @stamp
		and left(b_cli_prescl.design,1) != '.'
		
END

GO
Grant Execute On up_MCDT_getItemsReq to Public
Grant Control On up_MCDT_getItemsReq to Public
GO
--------------------------------------------------------------