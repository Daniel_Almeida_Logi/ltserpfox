	SET ANSI_NULLS ON
	GO
	SET QUOTED_IDENTIFIER ON
	GO

	if OBJECT_ID('[dbo].[up_update_farmacia2]') IS NOT NULL
		drop procedure dbo.up_update_farmacia2
	go


	create PROCEDURE [dbo].[up_update_farmacia2]
	/* WITH ENCRYPTION */
	AS
	BEGIN

		/*
			SP para actualiza��o de Dicinoario de Farmacia
			Autor:					Ivo Pereira
			Data Mod:				28/08/2013
			Vers�o:					2.5.6
			
				******************** APENAS PARA VERSAO 13.7.x!!! Novos campos!	*********************
				******************** VERSAO ESTAVEL, NAO INCLUI REDU��O 40% ESPA�O ******************
				******************** VERSAO COM NOVOS CAMPOS PEM_AMB E CNPEM PARA v 13.7.xx *********
			
			Done:
			> Fix update ST quando a TAXAIVA era 0%; divide by zero%
			> Fix update cCPTORG no WHERE estava o NCONT para compara��o mas faltava no UPDATE o campo; loop em ciclo.
			> novos campos CPTPLA cartao e maxembcartao
			> Mais HASH JOINs em outras tabelas grandes para paralelismo
			> Aplicado TOP 1 � ficha de empresa campo U_LOCALCOD (bds com duas fichas dava erro subquery)
			> Corrigido o UPSERT da tabela LOCAISPRESC de forma a que nao apaguem o registo ou o MERGE falhe tendo em conta o LOCALCOD
			> Activado o campo PUBLICO em falta como criterio de UPDATE e INSERT nas clinicas, tabelas B_PE_LOCAIS
			> Activado INSERT e UPDATE completo de informa��o Classificao Atributos em medicamentos / Insert completo parafarmacia
			> Reactivado update parafarmacia c/ novas REFs enviadas pela ELODIE
			> Desactivado UPDATE de Parafarmacia porque estava a bloquear a farmacia MAIO; temporario!
			> Corrigido e melhorado INSERT da PARAFARMACIA para novos prods enviados �do acordo BIOWELL
			> Tabelas em FULL FEED imparcial: TODAS excepto FPROD, CPTORG(b_utentes), FPRECO
			> Fim da tabela CPTORG_ORIG
			> Removido no UPDATE da FPORD a compara��o de POSOLOGIA que e um campo TEXT e pode estar a gastar muita RAM
			> Aplicada incrementalidade; se a tabela de origem for entregue na TempDB, correr update
			> Melhoria: se o campo RANK nao existir ou for NULL, aplicar ISNULL
			> Melhoria: 
		*/


		BEGIN TRY

			SET NOCOUNT ON;
			SET XACT_ABORT ON;
			SET ANSI_NULLS ON;
			SET QUOTED_IDENTIFIER ON;

			DECLARE
				@tran						varchar(32)
				, @SQL						varchar(MAX)
				, @Error					varchar(MAX)
				, @Return					INT
				, @paramulti				varchar(256) 
			
			DECLARE @changed_pvps table (stamp varchar(25))
			
			BEGIN TRANSACTION @tran

				if exists (select name from tempdb.sys.tables where name='d_labs')
					begin 
						TRUNCATE TABLE B_labs
						INSERT INTO b_labs (labID, sourceID, nome)
						SELECT xx.labID, xx.sourceID, xx.nome 
						FROM tempdb.dbo.d_labs AS xx
					end
	
				if exists (select name from tempdb.sys.tables where name='d_atc')
					begin 		
						TRUNCATE TABLE B_atc
						INSERT INTO b_atc (atcstamp,atccode,descricao,atcmini,Ac_farmac,Indica,Posomono,Contra,Interaccoes,Efectind,Condu,Sobredose,Subdop,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,marcada)
						SELECT  xx.atcstamp,xx.atccode,xx.descricao,xx.atcmini,xx.Ac_farmac,xx.Indica,xx.Posomono,xx.Contra,xx.Interaccoes,xx.Efectind,xx.Condu,xx.Sobredose,xx.Subdop,xx.ousrinis,xx.ousrdata,xx.ousrhora,xx.usrinis,xx.usrdata,xx.usrhora,xx.marcada
						FROM tempdb.dbo.d_atc AS xx 
					end
					
				if exists (select name from tempdb.sys.tables where name='d_atcfp')
					begin	
						TRUNCATE TABLE B_atcfp
						INSERT INTO b_atcfp (atcfpstamp, atccode, cnp, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, marcada)
						SELECT xx.atcfpstamp, xx.atccode, xx.cnp, xx.ousrinis, xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada
						FROM tempdb.dbo.d_atcfp AS xx
					end
	
				if exists (select name from tempdb.sys.tables where name='d_cft')
					begin	
						
						TRUNCATE TABLE B_cft
					/*	delete from b_cft */
						INSERT INTO  b_cft 
							(cftstamp, cftcode, descricao, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, marcada, Mecanismo_accao, Espectro_accao, Indicacoes, 
							Via_modo_administracao, Posologia, DMaxDMin, DDD, Insuficiencia_renal, Insuficiencia_hepatica, Reconstituicao_diluicao, Estabilidade, Incompatibilidade, 
							Dialisavel, Contra_indicacoes, Advertencias_precaucoes, Interaccoes_outros_medicamentos, Interaccoes_testes_laboratoriais, Interaccoes_alimentos, 
							Classificacao_gravidez_amamentacao, Efeitos_secundarios, Toxicologia, Conducao, SubsDop, SobreDos, moleculaID)
						SELECT
							xx.cftstamp, xx.cftcode, xx.descricao, xx.ousrinis, xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada, xx.Mecanismo_accao, 
							xx.Espectro_accao, xx.Indicacoes, xx.Via_modo_administracao, xx.Posologia, xx.DMaxDMin, xx.DDD, xx.Insuficiencia_renal, xx.Insuficiencia_hepatica, 
							xx.Reconstituicao_diluicao, xx.Estabilidade, xx.Incompatibilidade, xx.Dialisavel, xx.Contra_indicacoes, xx.Advertencias_precaucoes, 
							xx.Interaccoes_outros_medicamentos, xx.Interaccoes_testes_laboratoriais, xx.Interaccoes_alimentos, xx.Classificacao_gravidez_amamentacao, 
							xx.Efeitos_secundarios, xx.Toxicologia, xx.Conducao, xx.SubsDop, xx.SobreDos, xx.moleculaID
						FROM         
							tempdb.dbo.d_cft AS xx 
					end

				if exists (select name from tempdb.sys.tables where name='d_cftfp')
					begin	
						TRUNCATE TABLE B_cftfp
					/* 	delete from b_cftfp */
						INSERT INTO  b_cftfp (cftfpstamp, cftcode, cnp, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, marcada)
						SELECT xx.cftfpstamp, xx.cftcode, xx.cnp, xx.ousrinis, xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada
						FROM tempdb.dbo.d_cftfp AS xx						
					end
	
				if exists (select name from tempdb.sys.tables where name='d_fformas')
					begin	
						TRUNCATE TABLE B_fformas
						INSERT INTO  b_fformas
						SELECT xx.fformasstamp, xx.abrev, xx.descricao, xx.ousrinis, xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada
						FROM tempdb.dbo.d_fformas AS xx
					end
		
				if exists (select name from tempdb.sys.tables where name='d_grphmg')
					begin	
						TRUNCATE TABLE B_grphmg
						INSERT INTO  b_grphmg (grphmgstamp,grphmgcode,descricao,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,marcada,Nome_molecula,Class_desc,Via_desc,Quantidade_com_unidade,Itens_por_emb,Nivel_embalagem_mais_cara)
						SELECT xx.grphmgstamp,xx.grphmgcode,xx.descricao,xx.ousrinis,xx.ousrdata,xx.ousrhora,xx.usrinis,xx.usrdata,xx.usrhora,xx.marcada,xx.Nome_molecula,xx.Class_desc,xx.Via_desc,xx.Quantidade_com_unidade,xx.Itens_por_emb,xx.Nivel_embalagem_mais_cara
						FROM  tempdb.dbo.d_grphmg AS xx						
					end

				if exists (select name from tempdb.sys.tables where name='d_vadminfp')
					begin
						TRUNCATE TABLE B_vadminfp
					/*	delete from b_vadminfp */
						insert into B_vadminfp
						select * From tempdb.dbo.d_vadminfp						
					end

				if exists (select name from tempdb.sys.tables where name='d_sitcomfp')
					begin
						TRUNCATE TABLE B_sitcomfp
					/*	delete from b_sitcomfp */
						INSERT INTO  b_sitcomfp (sitcomfpstamp, cnp, fprodnome, sitcomdescr, sitcomdata, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, marcada)
						SELECT xx.sitcomfpstamp, xx.cnp, xx.fprodnome, xx.sitcomdescr, xx.sitcomdata, xx.ousrinis, xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada
						FROM tempdb.dbo.d_sitcomfp AS xx						
					end

				if exists (select name from tempdb.sys.tables where name='d_CPTORG')
					begin
						INSERT INTO b_utentes (utstamp,nome,no,tipo,nome2,morada,ncont,telefone,fax,codigoP,moeda,conta,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora)
						SELECT
							'ADM' + LEFT(REPLACE(NEWID(),'-',''), 22) AS stamp, 
							nome = left(nome,55),
							u_no AS no,
							'Entidade' AS tipo, 
							abrev AS nome2,
							CASE WHEN morada IS NOT NULL THEN LEFT(morada,50) ELSE '' END,
							Left(ncont,9), 
							CASE WHEN telefone IS NOT NULL THEN telefone ELSE '' END,
							CASE WHEN fax IS NOT NULL THEN fax ELSE '' END,
							'PT' AS pais,
							'PTE ou EURO' AS moeda, 
							CONVERT(varchar(15), 2111000000 + CONVERT(numeric(12), RIGHT(u_no,5))),
							'ADM',
							DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())),
							CONVERT(CHAR(8),getdate(),8),
							'ADM',
							DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())),
							CONVERT(CHAR(8),getdate(),8)
						FROM tempdb.dbo.d_CPTORG
						WHERE (tempdb.dbo.d_cptorg.u_no NOT IN (
							SELECT no FROM b_utentes)
						)
						
						INSERT INTO cptorg (cptorgstamp,nome,abrev,dataass,morada,telefone,fax,email,maxacum,emaxacum,inactivo,proprio,ncont,u_no,u_assfarm,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,marcada)
						SELECT xx.cptorgstamp,xx.nome,xx.abrev,xx.dataass,xx.morada,xx.telefone,xx.fax,xx.email,xx.maxacum,xx.emaxacum,xx.inactivo,xx.proprio,xx.ncont,xx.u_no,xx.u_assfarm,xx.ousrinis,xx.ousrdata,xx.ousrhora,xx.usrinis,xx.usrdata,xx.usrhora,xx.marcada
						FROM tempdb.dbo.d_cptorg AS xx 
						LEFT OUTER JOIN cptorg AS yy ON xx.cptorgstamp = yy.cptorgstamp
						WHERE (yy.cptorgstamp IS NULL)
							
						UPDATE	cptorg SET 
							nome=xx.nome,abrev=xx.abrev,dataass=xx.dataass,morada=xx.morada,telefone=xx.telefone,fax=xx.fax,email=xx.email,maxacum=xx.maxacum,emaxacum=xx.emaxacum,inactivo=xx.inactivo,proprio=xx.proprio,ncont=xx.ncont,u_no=xx.u_no,u_assfarm=xx.u_assfarm,ousrinis=xx.ousrinis,ousrdata=xx.ousrdata,ousrhora=xx.ousrhora,usrinis=xx.usrinis,usrdata=xx.usrdata,usrhora=xx.usrhora,marcada=xx.marcada
						FROM tempdb.dbo.d_cptorg AS xx 
						INNER JOIN cptorg as yy ON xx.cptorgstamp = yy.cptorgstamp 
						WHERE 
							(
								xx.abrev <> yy.abrev or
								xx.nome <> yy.nome or
								xx.morada <> yy.morada or
								xx.ncont  <> yy.ncont or
								xx.u_no <> yy.u_no or 
								xx.u_assfarm <> yy.u_assfarm
							)
							AND xx.cptorgstamp NOT IN ('ADM001','ADM050')
					
					end
			
				if exists  (select name from tempdb.sys.tables where name='d_cptgrp')
					begin
						TRUNCATE TABLE cptgrp
						INSERT INTO cptgrp (cptgrpstamp,grupo,descricao,rgeralpct,resppct,subgrupo,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,marcada,u_diploma1,u_diploma2,u_diploma3)
						SELECT xx.cptgrpstamp,xx.grupo,xx.descricao,xx.rgeralpct,xx.resppct,xx.subgrupo,xx.ousrinis,xx.ousrdata,xx.ousrhora,xx.usrinis,xx.usrdata,xx.usrhora,xx.marcada,xx.u_diploma1,xx.u_diploma2,xx.u_diploma3
						FROM tempdb.dbo.d_cptgrp AS xx
					end	
				
				if exists  (select name from tempdb.sys.tables where name='d_cptpla')
					begin
						TRUNCATE TABLE cptpla
						INSERT INTO cptpla (cptplastamp,codigo,design,cptorgstamp,cptorgabrev,tlotestamp,tlotecode,tlotedescr,snsid,inactivo,datainactivo,simples,diploma,protocolo,beneficiario,nrctlt,maxembrct,maxembmed,maxembuni,compl,modrct,manipulado,compldescr,dplmsstamp,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,marcada,u_proprio,regra_pens,cartao,maxembcartao)
						SELECT
							xx.cptplastamp, xx.codigo, xx.design, xx.cptorgstamp, xx.cptorgabrev, xx.tlotestamp, xx.tlotecode, xx.tlotedescr, xx.snsid, xx.inactivo, 
							xx.datainactivo, xx.simples, xx.diploma, xx.protocolo, xx.beneficiario, xx.nrctlt, xx.maxembrct, xx.maxembmed, xx.maxembuni, xx.compl, xx.modrct, 
							xx.manipulado, xx.compldescr, xx.dplmsstamp, xx.ousrinis, xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada, xx.u_proprio,xx.regra_pens,
							xx.cartao,xx.maxembcartao
						FROM tempdb.dbo.d_cptpla AS xx 
					end	

				if exists  (select name from tempdb.sys.tables where name='d_cptval')
					begin
						TRUNCATE TABLE cptval
						insert into cptval (cptplacode,cptpladesign,cptvalstamp,datafim,dataini,grpdescr,grppreco,grupo,marcada,ousrdata,ousrhora,ousrinis,pct,u_diploma,u_g1,u_g10,u_g11,u_g12,u_g13,u_g14,u_g15,u_g16,u_g17,u_g18,u_g19,u_g2,u_g20,u_g3,u_g4,u_g5,u_g6,u_g7,u_g8,u_g9,u_p1,u_p10,u_p11,u_p12,u_p13,u_p14,u_p15,u_p16,u_p17,u_p18,u_p19,u_p2,u_p20,u_p3,u_p4,u_p5,u_p6,u_p7,u_p8,u_p9,usrdata,usrhora,usrinis)
						select  cptplacode,cptpladesign,cptvalstamp,datafim,dataini,grpdescr,grppreco,grupo,marcada,ousrdata,ousrhora,ousrinis,pct,u_diploma,u_g1,u_g10,u_g11,u_g12,u_g13,u_g14,u_g15,u_g16,u_g17,u_g18,u_g19,u_g2,u_g20,u_g3,u_g4,u_g5,u_g6,u_g7,u_g8,u_g9,u_p1,u_p10,u_p11,u_p12,u_p13,u_p14,u_p15,u_p16,u_p17,u_p18,u_p19,u_p2,u_p20,u_p3,u_p4,u_p5,u_p6,u_p7,u_p8,u_p9,usrdata,usrhora,usrinis
						from tempdb.dbo.d_cptval					
					end		
							
				if exists  (select name from tempdb.sys.tables where name='d_tlote')
					begin
						TRUNCATE TABLE tlote
						INSERT INTO tlote (tlotestamp, codigo, descricao, regime, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, marcada)
						SELECT xx.tlotestamp, xx.codigo, xx.descricao, xx.regime, xx.ousrinis, xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada
						FROM tempdb.dbo.d_tlote AS xx					
					end	

				if exists  (select name from tempdb.sys.tables where name='d_dplms')
					begin			
						TRUNCATE TABLE dplms
						INSERT INTO dplms (dplmsstamp,diploma,patologia,inactivo,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,u_coluna,u_design)
						SELECT xx.dplmsstamp,xx.diploma,xx.patologia,xx.inactivo,xx.ousrinis,xx.ousrdata,xx.ousrhora,xx.usrinis,xx.usrdata,xx.usrhora,xx.u_coluna,xx.u_design
						FROM tempdb.dbo.d_dplms AS xx
					end						

				if exists  (select name from tempdb.sys.tables where name='d_fpreco')
					begin
						/* No 1� update nao usar ROBAR, full feed. Nao tem tempo a perder, precisam dos dados para vender. */
						if (select COUNT(cnp) from fpreco (nolock)) = 0
							begin
								INSERT INTO fpreco (fprecostamp,cnp,fprodnome,tipo,grupo,data,preco,epreco,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,marcada,u_precouni)
								SELECT xx.fprecostamp, xx.cnp, xx.fprodnome, xx.tipo, xx.grupo, xx.data, xx.preco, xx.epreco, xx.ousrinis,xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada, xx.preco_unit
								FROM tempdb.dbo.d_fpreco AS xx						
							end
						else
							begin
								/* Actualiza��o normal, ROBAR. Nao posso arriscar TABLE LOCK e parar a venda nas farmcias.*/
								DELETE FROM fpreco
								FROM tempdb.dbo.d_fpreco AS xx 
								RIGHT OUTER hash JOIN fpreco AS yy ON xx.cnp  = yy.cnp AND xx.grupo = yy.grupo
								WHERE (xx.cnp IS NULL)
								
								UPDATE	fpreco SET fprodnome=xx.fprodnome,tipo=xx.tipo,data=xx.data,preco=xx.preco,epreco=xx.epreco,ousrinis=xx.ousrinis,ousrdata=xx.ousrdata,ousrhora=xx.ousrhora,usrinis=xx.usrinis,usrdata=xx.usrdata,usrhora=xx.usrhora,marcada=xx.marcada,u_precouni=xx.preco_unit
								FROM tempdb.dbo.d_fpreco AS xx 
								INNER hash JOIN fpreco AS yy ON xx.cnp = yy.cnp AND xx.grupo = yy.grupo
								WHERE
									(xx.fprodnome <> yy.fprodnome) OR
									(xx.data <> yy.data) OR
									(xx.preco <> yy.preco) OR
									(xx.epreco <> yy.epreco) OR
									(xx.preco_unit <> yy.u_precouni)
													
								INSERT INTO fpreco (fprecostamp,cnp,fprodnome,tipo,grupo,data,preco,epreco,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,marcada,u_precouni)
								SELECT xx.fprecostamp, xx.cnp, xx.fprodnome, xx.tipo, xx.grupo, xx.data, xx.preco, xx.epreco, xx.ousrinis,xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada, xx.preco_unit
								FROM tempdb.dbo.d_fpreco AS xx 
								LEFT OUTER hash JOIN fpreco AS yy ON xx.cnp = yy.cnp AND xx.grupo = yy.grupo
								WHERE (yy.cnp IS NULL)		
							end
					
						/* Apenas executar actualiza��o de PRre�os registada SE a tabela de pre�os for entregue via DIcionario! */
						if (select count(table_name) from INFORMATION_SCHEMA.TABLES where TABLE_NAME='b_parameters')=1
							begin
						
								if (select isnull(bool,0) from B_Parameters where stamp='ADM0000000141') = 1
									begin
										
										/* Gravar pre�os originais */
										insert into b_historicopvp (stamp,ref,opvp,oMc,oDc,oMb,oBb,data,local,usr,site,terminal) 
										output inserted.stamp into @changed_pvps (stamp) 
										select																														
											stamp =											LEFT(newid(),25),
											ref =											a.ref,
											opvp =											a.epv1,
											oMc	=											
												case																																								
													when a.epv1>0 AND round(a.epcusto,2)>0	then ROUND( ((a.epv1 / (b.taxa/100+1) / round(a.epcusto,2)) -1) * 100 ,2)
													ELSE 0 
												end,
											oDc	= 
												case 
													when round(epcusto,2)>0 and round(epcpond,2)>0 then ROUND( ((round(a.epcusto,2) / round(a.epcpond,2)) -1) * 100 ,2)
													ELSE 0 
												end,
											oMb = 
												case 
													when a.epv1>0 and round(a.epcpond,2)>0 then ROUND( ((a.epv1 / (case when b.taxa=0 then 1 else b.taxa/100+1 end) / round(a.epcpond,2)) -1) * 100 ,2)
													ELSE 0 
												end,
											oBb	= 
												case 
													when a.epv1>0 then ROUND( (a.epv1 / (b.taxa/100+1)) - round(a.epcpond,2) ,2)
													ELSE 0 
												end,
											data =											GETDATE(),
											local =											'Actualiza��o de Dicion�rio',
											usr =											0,
											site =											'',
											terminal =										0
										from st as a 
										inner join taxasiva as b on a.tabiva=b.codigo 
										inner hash join tempdb.dbo.d_fpreco as c on a.ref=c.cnp and c.grupo='pvp'
										where
											a.epv1<>c.epreco								
										
										/* Actualizar pre�os na ST */
										update st set
											epv1=b.epreco, 
											pv1=b.preco
										FROM         st as a
										inner hash join tempdb.dbo.d_fpreco as b on a.ref=b.cnp and b.grupo='pvp'
										where 
											a.epv1 <> b.epreco
										
										/* Actualizar registo de PVP como so novos PVPs */
										UPDATE b_historicopvp SET
											dpvp =	b.epv1,						
											dMc	=											
												case 
													when epv1>0 AND epcusto>0	then ROUND( ((b.epv1 / (c.taxa/100+1) / round(b.epcusto,2)) -1) * 100 ,2)
													ELSE 0 
												end,
											dDc	= 
												case 
													when b.epcusto>0 and b.epcpond>0 then ROUND( ((round(b.epcusto,2) / round(b.epcpond,2)) -1) * 100 ,2)
													ELSE 0 
												end,
											dMb = 
												case 
													when b.epv1>0 and b.epcpond>0 then ROUND( ((b.epv1 / (c.taxa/100+1) / round(b.epcpond,2)) -1) * 100 ,2)
													ELSE 0 
												end,
											dBb	= 
												case 
													when b.epv1>0 then ROUND( (b.epv1 / (c.taxa/100+1)) - round(b.epcpond,2) ,2)
													ELSE 0 
												end
										from b_historicopvp as a
										inner hash join st as b on a.ref=b.ref
										inner join taxasiva as c on b.tabiva=c.codigo 
										inner join @changed_pvps as d on a.stamp=d.stamp									
									end
							end
					
					end
					
				if exists  (select name from tempdb.sys.tables where name='d_fprod_final')
					begin
						/* Melhorar primeira actualiza��o: se nao houver registos, correr codigo mais simples. Inserir Farmacia apenas. */
						if (select COUNT(cnp) from fprod (nolock)) = 0 
							begin
								INSERT INTO	fprod
									(fprodstamp, medid, fformasstamp, cptgrpstamp, cnp, barcode, generico, dcidescr, fformasdescr, fformasabrev, grupo, cptgrpdescr, tdbtdescr, grphmgcode, 
									grphmgdescr, dispdescr, estupfdescr, titaimdescr, estaimdescr, dataaim, dataestmed, dataaltmed, dataaltestmed, dataaltcpt, dataaltpreco, tiptratdescr, tipembdescr, 
									ref, design, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, marcada, psico, protocolo, benzo, manipulado, u_tabiva, u_marca, u_familia, u_conserv, 
									u_posprog, nome, dosuni, descricao, titaim, ranking, 
									pvpmaxre,pvporig,pvpcalc,pvpmax,
									u_escoacompart,num_unidades, dci
								/* Novos campos Classificacao Atributos*/
									,secstamp,catstamp,famstamp,depstamp,sfstamp
								/* Novos campos PEM_AMB e CNPEM */
									,cnpem,u_nomerc
									)
								SELECT
									xx.fprodstamp, xx.medID, xx.fformasstamp, xx.cptgrpstamp, xx.cnp, xx.barcode, xx.generico, xx.dcidescr, xx.fformasdescr, xx.fformasabrev, xx.grupo,xx.cptgrpdescr,
									xx.tdbtdescr, xx.grphmgcode, xx.grphmgdescr, xx.dispdescr, xx.estupfdescr, xx.titaimdescr, xx.estaimdescr, xx.dataaim, xx.dataestmed, xx.dataaltmed, xx.dataaltestmed,
									xx.dataaltcpt, xx.dataaltpreco, xx.tiptratdescr, xx.tipembdescr, xx.ref, xx.design, xx.ousrinis, xx.ousrdata, xx.ousrhora, xx.usrinis,xx.usrdata, xx.usrhora, xx.marcada,
									xx.psico, xx.protocolo, xx.benzo, xx.manipulado, xx.tabiva, ISNULL((xx.marca),''), xx.familiaref, xx.conservacao, xx.posprograma, xx.nome, xx.dosuni,
									LEFT(xx.descricao, 100) AS descricao, xx.titular_aimid, isnull(xx.[rank],0), 
									ISNULL(xx.pvpmaxre,0), isnull(xx.pvporig,0),isnull(xx.pvpcalc,0),isnull(xx.pvpmax,0),
									isnull(Escoa_Compart,0), isnull(xx.num_unidades,0), ISNULL(left(xx.dci, 254),'')
								/* Novos campos Classificao Atributos */
									,xx.secstamp,xx.catstamp,xx.famstamp,xx.depstamp,xx.sfstamp
								/* Novos campos PEM_AMB e CNPEM */
									,cnpem = isnull(xx.cnpem,''), u_nomerc = case when isnull(xx.pem_amb,'N') = 'S' then 1 else 0 end
								/*	from dpf3.dbo.fprod_final as xx */
								FROM tempdb.dbo.d_fprod_final AS xx	
							end
						else
							begin
								/* Logica normal de actualiza��o, por enquanto manter. Nao � problema actualizar uma FPROD ja preenchida lentamente! */
								INSERT INTO	fprod
									(fprodstamp, medid, fformasstamp, cptgrpstamp, cnp, barcode, generico, dcidescr, fformasdescr, fformasabrev, grupo, cptgrpdescr, tdbtdescr, grphmgcode, 
									grphmgdescr, dispdescr, estupfdescr, titaimdescr, estaimdescr, dataaim, dataestmed, dataaltmed, dataaltestmed, dataaltcpt, dataaltpreco, tiptratdescr, tipembdescr, 
									ref, design, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, marcada, psico, protocolo, benzo, manipulado, u_tabiva, u_marca, u_familia, u_conserv, 
									u_posprog, nome, dosuni, descricao, titaim, ranking, 
									pvpmaxre,pvporig,pvpcalc,pvpmax,
									u_escoacompart,num_unidades, dci
								/* Novos campos Classificacao Atributos*/
									,secstamp,catstamp,famstamp,depstamp,sfstamp									
								/* Novos campos PEM_AMB e CNPEM */
									,cnpem,u_nomerc
									)
								SELECT
									xx.fprodstamp, xx.medID, xx.fformasstamp, xx.cptgrpstamp, xx.cnp, xx.barcode, xx.generico, xx.dcidescr, xx.fformasdescr, xx.fformasabrev, xx.grupo,xx.cptgrpdescr,
									xx.tdbtdescr, xx.grphmgcode, xx.grphmgdescr, xx.dispdescr, xx.estupfdescr, xx.titaimdescr, xx.estaimdescr, xx.dataaim, xx.dataestmed, xx.dataaltmed, xx.dataaltestmed,
									xx.dataaltcpt, xx.dataaltpreco, xx.tiptratdescr, xx.tipembdescr, xx.ref, xx.design, xx.ousrinis, xx.ousrdata, xx.ousrhora, xx.usrinis,xx.usrdata, xx.usrhora, xx.marcada,
									xx.psico, xx.protocolo, xx.benzo, xx.manipulado, xx.tabiva, ISNULL((xx.marca),''), xx.familiaref, xx.conservacao, xx.posprograma, xx.nome, xx.dosuni,
									LEFT(xx.descricao, 100) AS descricao, xx.titular_aimid, isnull(xx.[rank],0),
									ISNULL(xx.pvpmaxre,0), isnull(xx.pvporig,0),isnull(xx.pvpcalc,0),isnull(xx.pvpmax,0),
									isnull(Escoa_Compart,0), isnull(xx.num_unidades,0), ISNULL(left(xx.dci, 254),'')
								/* Novos campos Classificacao Atributos*/
									,xx.secstamp,xx.catstamp,xx.famstamp,xx.depstamp,xx.sfstamp									
								/* Novos campos PEM_AMB e CNPEM */
									,cnpem = isnull(xx.cnpem,''), u_nomerc = case when isnull(xx.pem_amb,'N') = 'S' then 1 else 0 end
							/*	from dpf3.dbo.fprod_final as xx */
								FROM tempdb.dbo.d_fprod_final AS xx	
								LEFT OUTER hash JOIN fprod AS yy ON xx.cnp= yy.cnp
								WHERE (xx.medID<>'') AND (yy.cnp IS NULL)
								
								/* Update FPROD farmacia */		
								UPDATE  fprod SET
									fprodstamp = xx.fprodstamp, medid = xx.medID, fformasstamp = xx.fformasstamp, cptgrpstamp = xx.cptgrpstamp, nome = xx.nome, barcode = xx.barcode, 
									dosuni = xx.dosuni, descricao = LEFT(xx.descricao, 100), generico = xx.generico, dcidescr = xx.dcidescr, fformasdescr = xx.fformasdescr, 
									fformasabrev = xx.fformasabrev, grupo = xx.grupo, cptgrpdescr = xx.cptgrpdescr, tdbtdescr = xx.tdbtdescr, grphmgcode = xx.grphmgcode, 
									grphmgdescr = xx.grphmgdescr, dispdescr = xx.dispdescr, estupfdescr = xx.estupfdescr, titaimdescr = xx.titaimdescr, estaimdescr = xx.estaimdescr, 
									dataaim = xx.dataaim, dataestmed = xx.dataestmed, dataaltmed = xx.dataaltmed, dataaltestmed = xx.dataaltestmed, dataaltcpt = xx.dataaltcpt, 
									dataaltpreco = xx.dataaltpreco, tiptratdescr = xx.tiptratdescr, tipembdescr = xx.tipembdescr, tipproddescr = '', ref = xx.ref, design = xx.design, ousrinis = xx.ousrinis, 
									ousrdata = xx.ousrdata, ousrhora = xx.ousrhora, usrinis = xx.usrinis, usrdata = xx.usrdata, usrhora = xx.usrhora, marcada = xx.marcada, psico = xx.psico, 
									benzo = xx.benzo, protocolo = xx.protocolo, manipulado = xx.manipulado, u_tabiva = xx.tabiva, u_marca = ISNULL((xx.marca),''), u_familia = xx.familiaref, 
									u_conserv = xx.conservacao, u_posprog = xx.posprograma, titaim = xx.titular_aimid, 
									ranking=isnull(xx.[rank],0),
									pvpmax = ISNULL(xx.pvpmax,0),	pvpmaxre=ISNULL(xx.pvpmaxre,0),pvporig=ISNULL(xx.pvporig,0),pvpcalc=ISNULL(xx.pvpcalc,0),
									u_alterado=0,
									u_escoacompart=ISNULL(xx.escoa_compart,0),
									dci=ISNULL(left(xx.dci, 254),'')
								/* Novos campos Classificao Atributos*/
									,secstamp=xx.secstamp
									,catstamp=xx.catstamp
									,famstamp=xx.famstamp
									,depstamp=xx.depstamp
									,sfstamp=xx.sfstamp					
								/* Novos campos PEM_AMB e CNPEM */				
									,cnpem = isnull(xx.cnpem,'')
									,u_nomerc = case when isnull(xx.pem_amb,'N') = 'S' then 1 else 0 end
							/*	from dpf3.dbo.fprod_final as xx */
								FROM tempdb.dbo.d_fprod_final AS xx 
								INNER hash JOIN fprod AS yy ON xx.cnp = yy.cnp
								WHERE 
									/* Patch 28/08/2013: temos que incluir os produtos Protocolo que NAO tem medid! */
									(xx.medID <> '' or xx.protocolo = 1)
									AND (
										(yy.cptgrpstamp <> xx.cptgrpstamp) OR
										(yy.generico <> xx.generico) OR
										(yy.grphmgcode <> xx.grphmgcode) OR
										(yy.dispdescr <> xx.dispdescr) OR				/* Tipo de Dispensa: Sujeito ou Nao a Receita Medica */ 
										(yy.estupfdescr <> xx.estupfdescr) OR			/* Tabela de Psicotropicos */ 
										(yy.estaimdescr <> xx.estaimdescr) OR			/* Estado de Comercializa��o */
										(yy.design <> xx.design) OR
										(yy.psico <> xx.psico) OR
										(yy.benzo <> xx.benzo) OR
										(yy.protocolo <> xx.protocolo) OR
										(yy.u_tabiva <> xx.tabiva) OR
										(yy.u_familia <> xx.familiaref) OR
										(yy.titaim <> xx.titular_aimid) OR
										(yy.ranking <> isnull(xx.[rank],0)) OR
										(yy.pvpmaxre <> isnull(xx.pvpmaxre,0)) or
										(yy.pvpmax <> ISNULL(xx.pvpmax,0)) OR
										(yy.pvporig <> isnull(xx.pvporig,0)) OR
										(yy.pvpcalc <> isnull(xx.pvpcalc,0)) OR 
										(yy.dci <> left(xx.dci,254)) OR 
									/* Novos campos Classificao Atributos*/
										(yy.secstamp<>xx.secstamp) OR 
										(yy.catstamp<>xx.catstamp) OR 
										(yy.famstamp<>xx.famstamp) OR
										(yy.depstamp<>xx.depstamp) OR 
										(yy.sfstamp<>xx.sfstamp) OR 
									/* Novos campos PEM_AMB e CNPEM */				
										(yy.cnpem <> isnull(xx.cnpem,'')) OR 
										(yy.u_nomerc<>case when isnull(xx.pem_amb,'N') = 'S' then 1 else 0 end)
									)
							end
					end
				
				/*
					ENTREGA DE NOVA PARAFARMACIA QUE NAO MAPEIA COM FPROD:
					Trabalho da Elodie de processar REF DESIGN IVA
				*/
						
				if exists (select name from tempdb.sys.tables where name='d_fprod_final_paraf')
					begin
						
						INSERT INTO fprod
							(fprodstamp, medid, fformasstamp, cptgrpstamp, cnp, nome, barcode, dosuni, descricao, generico, dcidescr, fformasdescr, fformasabrev, grupo, cptgrpdescr, 
							tdbtdescr, grphmgcode, grphmgdescr, dispdescr, estupfdescr, titaimdescr, estaimdescr, dataaim, dataestmed, dataaltmed, dataaltestmed, dataaltcpt, dataaltpreco, 
							tiptratdescr, tipembdescr, tipproddescr, ref, design, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, marcada, psico, protocolo, benzo, manipulado, u_tabiva, 
							u_marca, u_familia, u_conserv, u_posprog, titaim
							/* Novos campos Classificacao Atributos*/
							,secstamp,catstamp,famstamp,depstamp,sfstamp
							/* Novos campos PEM_AMB e CNPEM a vazio mas 1 no PEM_AMB; parafarmacia pode sempre ser prescrita */		
							,cnpem,u_nomerc					
							)
						SELECT    
							xx.fprodstamp, xx.medID, xx.fformasstamp, xx.cptgrpstamp, xx.cnp, xx.nome, xx.barcode, xx.dosuni, xx.descricao, 
							generico = isnull(xx.generico,0),
							xx.dcidescr, xx.fformasdescr, 
							xx.fformasabrev, xx.grupo, xx.cptgrpdescr, xx.tdbtdescr, xx.grphmgcode, xx.grphmgdescr, xx.dispdescr, 
							estupfdescr = isnull(xx.estupfdescr,0),
							xx.titaimdescr, xx.estaimdescr, xx.dataaim, 
							xx.dataestmed, xx.dataaltmed, xx.dataaltestmed, xx.dataaltcpt, xx.dataaltpreco, xx.tiptratdescr, xx.tipembdescr, xx.tipproddescr, xx.ref, xx.design, 
							xx.ousrinis, xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, isnull(xx.marcada,0), 
							psico = isnull(xx.psico,0), 
							protocolo = isnull(xx.protocolo,0),
							benzo = isnull(xx.benzo,0),
							manipulado = isnull(xx.manipulado,0),
							xx.tabiva, 
							u_marca = ISNULL((left(xx.marca,20)),''), 
							xx.familiaref, 
							'' AS u_conserv, '' AS u_posprog, 
							titaim = isnull(xx.titaimid,'')
							/* Novos campos Classificacao Atributos*/
							,xx.secstamp,xx.catstamp,xx.famstamp,xx.depstamp,xx.sfstamp
							/* Novos campos PEM_AMB e CNPEM a vazio mas 1 no PEM_AMB; parafarmacia pode sempre ser prescrita */		
							,cnpem = '',u_nomerc = 1
					/*	from dpf3.dbo.fprod_final_paraf as xx */
						FROM tempdb.dbo.d_fprod_final_paraf AS xx 
						LEFT OUTER hash JOIN fprod AS yy ON xx.cnp = yy.cnp
						WHERE 
							(xx.medID = '') AND (yy.cnp IS NULL)
			
						/*					
						UPDATE	fprod SET
							fprodstamp = xx.fprodstamp, medid = xx.medID, fformasstamp = xx.fformasstamp, cptgrpstamp = xx.cptgrpstamp, nome = xx.nome, barcode = xx.barcode, 
							dosuni = xx.dosuni, descricao = xx.descricao, generico = xx.generico, dcidescr = xx.dcidescr, fformasdescr = xx.fformasdescr, fformasabrev = xx.fformasabrev, 
							grupo = xx.grupo, cptgrpdescr = xx.cptgrpdescr, tdbtdescr = xx.tdbtdescr, grphmgcode = xx.grphmgcode, grphmgdescr = xx.grphmgdescr, dispdescr = xx.dispdescr, 
							estupfdescr = xx.estupfdescr, titaimdescr = xx.titaimdescr, estaimdescr = xx.estaimdescr, dataaim = xx.dataaim, dataestmed = xx.dataestmed, 
							dataaltmed = xx.dataaltmed, dataaltestmed = xx.dataaltestmed, dataaltcpt = xx.dataaltcpt, dataaltpreco = xx.dataaltpreco, tiptratdescr = xx.tiptratdescr, 
							tipembdescr = xx.tipembdescr, tipproddescr = xx.tipproddescr, ref = xx.ref, design = xx.design, ousrinis = xx.ousrinis, ousrdata = xx.ousrdata, 
							ousrhora = xx.ousrhora, usrinis = xx.usrinis, usrdata = xx.usrdata, usrhora = xx.usrhora, marcada = xx.marcada, psico = xx.psico, benzo = xx.benzo, 
							protocolo = xx.protocolo, manipulado = xx.manipulado, u_tabiva = xx.tabiva, u_marca = CASE WHEN xx.marca IS NULL THEN '' ELSE xx.marca END, 
							u_familia = xx.familiaref, u_conserv = '', u_posprog = '', titaim = xx.titaimid
						FROM tempdb.dbo.d_fprod_final_paraf AS xx 
						INNER JOIN fprod AS yy ON xx.cnp = yy.cnp
						WHERE
							(xx.medID='') 
							AND (																															
								(yy.barcode <> xx.barcode) OR
								(yy.design <> xx.design) OR
								(yy.cptgrpstamp <> xx.cptgrpstamp) OR
								(yy.u_tabiva <> xx.tabiva) OR
								(yy.u_familia <> xx.familiaref)
							)
						*/
					end
			
				if exists (select name from tempdb.sys.tables where name='d_moleculas')
					begin
						TRUNCATE TABLE B_moleculas
						INSERT INTO  b_moleculas (molecstamp, moleculaID, substID, descricao, Denominacao, class1, class2, Formula, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, marcada)
						SELECT xx.molecstamp, xx.moleculaID, xx.substID, xx.descricao, xx.Denominacao, xx.class1, xx.class2,xx.Formula, xx.ousrinis, xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada
						FROM tempdb.dbo.d_moleculas AS xx
					end

				if exists (select name from tempdb.sys.tables where name='d_moleculasfp')
					begin
						
						TRUNCATE TABLE B_moleculasfp
						INSERT INTO  b_moleculasfp (molecfpstamp, moleculaID, cnp, qtt, tipo, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, marcada)
						SELECT xx.molecfpstamp, xx.moleculaID, xx.cnp, xx.qtt, xx.tipo, xx.ousrinis, xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada
						FROM tempdb.dbo.d_moleculasfp AS xx 
					end

				if exists (select name from tempdb.sys.tables where name='d_alrg')
					begin
						TRUNCATE TABLE B_alrg
						INSERT INTO  b_alrg (alrgstamp,alergia,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,marcada)
						SELECT 	xx.alrgstamp,xx.alergia,xx.ousrinis,xx.ousrdata,xx.ousrhora,xx.usrinis,xx.usrdata,xx.usrhora,xx.marcada
						FROM tempdb.dbo.d_alrg AS xx
					end

				if exists (select name from tempdb.sys.tables where name='d_alrgfp')
					begin
						TRUNCATE TABLE B_alrgfp
						INSERT INTO b_alrgfp (alrgfpstamp,alrgstamp,moleculaID,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,marcada)
						SELECT xx.alrgfpstamp,xx.alrgstamp,xx.moleculaID,xx.ousrinis,xx.ousrdata,xx.ousrhora,xx.usrinis,xx.usrdata,xx.usrhora,xx.marcada
						FROM tempdb.dbo.d_alrgfp AS xx 
					end

				if exists (select name from tempdb.sys.tables where name='d_cons')
					begin
						TRUNCATE TABLE B_cons
						INSERT INTO  b_cons (consstamp, conservacaoID, descricao, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, marcada)
						SELECT xx.consstamp, xx.conservacaoID, xx.descricao, xx.ousrinis, xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada
						FROM tempdb.dbo.d_cons AS xx 
					end
				
				if exists (select name from tempdb.sys.tables where name='d_ctidef')
					begin
						TRUNCATE TABLE B_ctidef
						INSERT INTO  b_ctidef (ctiefpstamp, descricao, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, marcada)
						SELECT xx.ctiefpstamp, xx.descricao, xx.ousrinis, xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada
						FROM tempdb.dbo.d_ctidef AS xx
					end
	
				if exists (select name from tempdb.sys.tables where name='d_ctiexp')
					begin
						TRUNCATE TABLE B_ctiexp
						INSERT INTO  b_ctiexp (ctiexpstamp, descricao, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, marcada)
						SELECT xx.ctiexpstamp, xx.descricao, xx.ousrinis, xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada
						FROM tempdb.dbo.d_ctiexp AS xx
					end	

				if exists (select name from tempdb.sys.tables where name='d_ctinvl')
					begin
						TRUNCATE TABLE B_ctinvl
						INSERT INTO b_ctinvl (ctinvlstamp, descricao, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, marcada)
						SELECT xx.ctinvlstamp, xx.descricao, xx.ousrinis, xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada
						FROM tempdb.dbo.d_ctinvl AS xx
					end	

				if exists (select name from tempdb.sys.tables where name='d_ctifp')
					begin	
						TRUNCATE TABLE B_ctifp
						INSERT INTO  b_ctifp (ctifpstamp, ctidefstamp, ctinvlstamp, ctiexpstamp, moleculaID,ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, marcada)
						SELECT xx.ctifpstamp, xx.ctidefstamp, xx.ctinvlstamp, xx.ctiexpstamp, xx.moleculaID,xx.ousrinis, xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada
						FROM tempdb.dbo.d_ctifp AS xx					
					end		

				if exists (select name from tempdb.sys.tables where name='d_inaalm')
					begin	
						TRUNCATE TABLE B_inaalm
						INSERT INTO  b_inaalm (inaalmstamp, descricao, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, marcada)
						SELECT xx.inaalmstamp, xx.descricao, xx.ousrinis, xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada
						FROM tempdb.dbo.d_inaalm AS xx 						
					end
		
				if exists (select name from tempdb.sys.tables where name='d_inaexp')
					begin
						TRUNCATE TABLE B_inaexp
						INSERT INTO b_inaexp (inaexpstamp, descricao, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, marcada)
						SELECT xx.inaexpstamp, xx.descricao, xx.ousrinis, xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada
						FROM tempdb.dbo.d_inaexp AS xx
					end

				if exists (select name from tempdb.sys.tables where name='d_inasen')
					begin
						TRUNCATE TABLE B_inasen
						INSERT INTO  b_inasen (inasenstamp, descricao, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, marcada)
						SELECT 	xx.inasenstamp, xx.descricao, xx.ousrinis, xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada
						FROM tempdb.dbo.d_inasen AS xx
					end

				if exists (select name from tempdb.sys.tables where name='d_inafp')
					begin
						TRUNCATE TABLE B_inafp
						INSERT INTO  b_inafp (inafpstamp, inaalmstamp, inasenstamp, inaexpstamp, moleculaID, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, marcada)
						SELECT xx.inafpstamp, xx.inaalmstamp, xx.inasenstamp, xx.inaexpstamp, xx.moleculaID, xx.ousrinis, xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada
						FROM tempdb.dbo.d_inafp AS xx
					end

				if exists (select name from tempdb.sys.tables where name='d_intmed')
					begin	
						TRUNCATE TABLE B_intmed
						INSERT INTO  b_intmed (intmedstamp, meddef, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, marcada)
						SELECT xx.intmedstamp, xx.meddef, xx.ousrinis, xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada
						FROM tempdb.dbo.d_intmed AS xx
					end

				if exists (select name from tempdb.sys.tables where name='d_intexp')
					begin	
						TRUNCATE TABLE B_intexp
						INSERT INTO  b_intexp (intexpstamp, expdef, predef, disdef, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, marcada)
						SELECT xx.intexpstamp, xx.expdef, xx.predef, xx.disdef, xx.ousrinis, xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada
						FROM tempdb.dbo.d_intexp AS xx
					end
				
				if exists (select name from tempdb.sys.tables where name='d_intfp')
					begin
						TRUNCATE TABLE B_intfp
						INSERT INTO  b_intfp (intfpstamp, intmedstamp, intexpstamp, molecula1, molecula2, class1, class2, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, marcada)
						SELECT xx.intfpstamp, xx.intmedstamp, xx.intexpstamp, xx.molecula1, xx.molecula2, xx.class1,xx.class2, xx.ousrinis, xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada
						FROM tempdb.dbo.d_intfp AS xx
					end
				
				if exists (select name from tempdb.sys.tables where name='d_intplantas')
					begin	
						TRUNCATE TABLE B_intplantas
						INSERT INTO  b_intplantas (intplantasstamp, Pk, Descricao, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, marcada)
						SELECT xx.intplantasstamp, xx.Pk, xx.Descricao, xx.ousrinis, xx.ousrdata,xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada
						FROM tempdb.dbo.d_intplantas AS xx
					end

				if exists (select name from tempdb.sys.tables where name='d_intplantasfp')
					begin	
						TRUNCATE TABLE B_intplantasfp
						INSERT INTO  b_intplantasfp (intplantasfpstamp, moleculaID, plantaID, Pk, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, marcada)
						SELECT xx.intplantasfpstamp, xx.moleculaID, xx.plantaID, xx.Pk, xx.ousrinis,xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada
						FROM tempdb.dbo.d_intplantasfp AS xx
					end

				if exists (select name from tempdb.sys.tables where name='d_posolog')
					begin		
						TRUNCATE TABLE B_posolog
						INSERT INTO  b_posolog (posologstamp, monografiaID, Nome_monografia, Pos_programa, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, marcada)
						SELECT xx.posologstamp, xx.monografiaID, xx.Nome_monografia, xx.Pos_programa, xx.ousrinis,xx.ousrdata, xx.ousrhora, xx.usrinis, xx.usrdata, xx.usrhora, xx.marcada
						FROM tempdb.dbo.d_posolog AS xx
					end

				/* INSERT das Mensagens internas re-portado da SP antiga, sem leitura do detalhe de actualiza��o*/
				if (select count(table_name) from INFORMATION_SCHEMA.TABLES where TABLE_NAME='b_parameters')=1
					begin
						select 
							@paramulti = replace((replace(convert(varchar,(select upper(iniciais) as 'l' from b_us where superact<>0 for XML path (''), type)),'</l>',',')),'<l>',''),
							@paramulti=SUBSTRING(@paramulti,1,len(@paramulti)-1)
						
						insert into mg (data,de,decomp,resumo,descricao,ehtml,para,paracomp,multi,paramulti,hora,mgstamp,ousrdata,ousrhora,ousrinis)
						select													   
							data				= DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())),
							de					= 'ADM',
							decomp				= 'Administrador do Sistema',
							resumo				= 'Actualizacao de Comparticipacoes ',
							descricao			=  'Actualizacao de Dicionario efectuada com sucesso! ',
							ehtml				= 0,
							para				= upper(iniciais),
							paracomp			= username,
							multi				= case when (select count(username) from b_us where superact<>0)>1 then 1 else 0 end,
							paramulti			= case when (@paramulti<>'' and (select count(username) from b_us where superact<>0)>1) then @paramulti else '' end,
							hora				= CONVERT(CHAR(8),getdate(),8),
							mgstamp				= 'ADM' + replace(left(newid(),22), '-',''),
							ousrdata			= DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())),
							ousrhora			= CONVERT(CHAR(8),getdate(),8),
							ousrinis			= 'ADM '
						from b_us
						where
							superact<>0 or username='sa'
					end

			/*
				UPDATE DA INFORMA��O DE MCDT - 6 TABELAS : TRUNCATE TABLE / INSERT OPCIONAL CASO AS TABELAS EXISTAM
			*/
		
				if exists (select name from tempdb.sys.tables where name='d_mcdt')
					begin 
						if exists (select name from sys.tables where name = 'b_pe_mcdt')
							begin
								TRUNCATE TABLE b_pe_mcdt
								insert into b_pe_mcdt select * from tempdb.dbo.d_mcdt
							end
					end 
				
				if exists (select name from tempdb.sys.tables where name='d_mcdt_sin')
					begin 
						if exists (select name from sys.tables where name = 'b_pe_mcdt_sin')
							begin
								TRUNCATE TABLE b_pe_mcdt_sin
								insert into b_pe_mcdt_sin select * from tempdb.dbo.d_mcdt_sin
							end
					end 
				
				if exists (select name from tempdb.sys.tables where name='d_mcdt_natureza')
					begin 
						if exists (select name from sys.tables where name = 'b_pe_mcdt_natureza')
							begin
								TRUNCATE TABLE b_pe_mcdt_natureza
								insert into b_pe_mcdt_natureza select * from tempdb.dbo.d_mcdt_natureza
							end
					end			
				
				if exists (select name from tempdb.sys.tables where name='d_mcdt_tagarea')
					begin 
						if exists (select name from sys.tables where name = 'b_pe_mcdt_tagarea')
							begin
								TRUNCATE TABLE b_pe_mcdt_tagarea
								insert into b_pe_mcdt_tagarea select * from tempdb.dbo.d_mcdt_tagarea
							end
					end				
			
				if exists (select name from tempdb.sys.tables where name='d_mcdt_tagproduto')
					begin 
						if exists (select name from sys.tables where name='b_pe_mcdt_tagproduto')
							begin
								TRUNCATE TABLE b_pe_mcdt_tagproduto
								insert into b_pe_mcdt_tagProduto select * from tempdb.dbo.d_mcdt_tagproduto
							end
					end	
				
				if exists (select name from tempdb.sys.tables where name='d_mcdt_motivoanulacao')
					begin 
						if exists (select name from sys.tables where name = 'b_pe_mcdt_motivoanulacao')
							begin
								TRUNCATE TABLE b_pe_mcdt_motivoanulacao
								insert into b_pe_mcdt_MotivoAnulacao select * from tempdb.dbo.d_mcdt_motivoanulacao
							end
					end				
			
			
			/* 
				FEED DA INFORMA��O DA PRESCRICAO ELECTRONICA 
			*/
			
				
				if exists (select name from tempdb.sys.tables where name='d_motivoanulacao')
					begin 
						if exists (select name from sys.tables where name='b_pe_motivoanulacao')
							begin
								TRUNCATE TABLE b_pe_motivoanulacao
								insert into b_pe_MotivoAnulacao select * from tempdb.dbo.d_motivoanulacao
							end
					end						
					
				if exists (select name from tempdb.sys.tables where name='d_tabelaPaises') 
					begin 
						if exists (select name from sys.tables where name='b_pe_tabela_pais')		
							begin	
								TRUNCATE TABLE b_pe_tabela_pais
								insert into b_pe_tabela_pais select * from tempdb.dbo.d_tabelaPaises
							end
					end 
					
				if exists (select name from tempdb.sys.tables where name='d_efr') 
					begin 
						if exists (select name from sys.tables where name='b_pe_efr')		
							begin	
								TRUNCATE TABLE b_pe_efr
								insert into b_pe_efr select * from tempdb.dbo.d_efr
							end	
					end					
					
				if exists (select name from tempdb.sys.tables where name='d_listadeRECM') 
					begin 
						if exists (select name from sys.tables where name='b_pe_ListadeRECM')
							begin	
								TRUNCATE TABLE b_pe_ListadeRECM
								insert into b_pe_ListadeRecm select * from tempdb.dbo.d_listadeRECM
							end	
					end									

				if exists (select name from tempdb.sys.tables where name='d_listadeBenef') 
					begin 
						if exists (select name from sys.tables where name='b_pe_ListadeBenef')		
							begin	
								TRUNCATE TABLE b_pe_ListadeBenef
								insert into b_pe_ListadeBenef select * from tempdb.dbo.d_listadeBenef
							end
					end		 				
					
				if exists (select name from tempdb.sys.tables where name='d_listadeOutrosBenef') 
					begin  
						if exists (select name from sys.tables where name='b_pe_ListadeOutrosBenef')		
							begin	
								TRUNCATE TABLE b_pe_ListadeOutrosBenef
								insert into b_pe_ListadeOutrosBenef select * from tempdb.dbo.d_listadeOutrosBenef
							end
					end	  									
					
					
				/* 
					UPSERT dos Locais de Prescricao, nao e removida informa��o no cliente 
					ha uma Regra PHC que cria o registo na tabela automaticamente e este nao pode ser ermovido!
					destes registos apenas o ARS e LOCAL nao podem ser alterados, ha updates de designacao
				*/
				
				if exists (select name from tempdb.sys.tables where name='d_locaisPresc') 
					begin  
						/* Se o campo LocalACS existir E nao for vazio ou nulo, apagar todos os registos da tabela peLocais EXCEPTO este
						 � critico para a Clinica ter este registo e ele NUNCA ser apagado senao o software bloqueia */
						if (select top 1 localcod from B_Lojas) is not null
							begin
								if (select top 1 localcod from B_Lojas) <> ''
									begin
										
										delete from b_pe_locais where COD_LOCAL not in (
											select top 1 localcod from B_Lojas (nolock)
										)
										
										insert into b_pe_locais (ARS,COD_CONC,COD_DIST,COD_FREG,COD_LOCAL,DESIGNACAO,dominio,TIPO,tipo_entidade,TIPO_LOCAL,publico) 
										select b.ARS,b.COD_CONC,b.COD_DIST,b.COD_FREG,b.COD_LOCAL,b.DESIGNACAO,b.dominio,b.TIPO,b.tipo_entidade,b.TIPO_LOCAL,b.publico
										from tempdb.dbo.d_locaisPresc as b
										where
											cod_local not in (
												select top 1 localcod from B_Lojas (nolock)
											)
									end
								/*  Se por algum motivo a tabela estiver vazia ou o codigo nao preenchido, inserir TUDO */
								else
									begin
										insert into b_pe_locais (ARS,COD_CONC,COD_DIST,COD_FREG,COD_LOCAL,DESIGNACAO,dominio,TIPO,tipo_entidade,TIPO_LOCAL,publico) 
										select b.ARS,b.COD_CONC,b.COD_DIST,b.COD_FREG,b.COD_LOCAL,b.DESIGNACAO,b.dominio,b.TIPO,b.tipo_entidade,b.TIPO_LOCAL,b.publico
										from tempdb.dbo.d_locaisPresc as b									
									end
							end
					end
		
			/* UPDATE INFORMACAO MARGENS REGRESSIVAS */ 

				if exists (select name from tempdb.sys.tables where name='d_b_mrgRegressivas')
					begin  
						if exists (select name from sys.tables where name='b_mrgRegressivas') 
							begin
								TRUNCATE TABLE b_mrgRegressivas
								insert into b_mrgRegressivas select * from tempdb.dbo.d_b_mrgRegressivas
							end
					end	  
				

			/* UPDATE INFORMA��O HISTORICO DE PRE�OS */ 
				if exists (select name from tempdb.sys.tables where name='d_a_HistPrecosInf')
					begin  
						if exists (select name from sys.tables where name='a_HistPrecosInf')
							begin 
								TRUNCATE TABLE a_HistPrecosInf
								insert into a_HistPrecosInf select * from tempdb.dbo.d_a_HistPrecosInf
							end
					end			
			

			/* FINAL DE PROCESSAMENTO */

			COMMIT TRANSACTION @tran 
			
			return 0
			
		END TRY
					
		BEGIN CATCH

			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION @tran
			
			declare
				@errortext varchar(256),
				@error_severity	int,
				@error_state	int	
			
			select 
				@errortext = 'ERRO: Procedure  ' + isnull(ERROR_PROCEDURE(),'') + ', C�digo ' + convert(varchar,ERROR_NUMBER()) + ', linha ' + convert(varchar,ERROR_LINE()) + ': ' + ERROR_MESSAGE(),
				@error_severity = ERROR_SEVERITY(),
				@error_state = ERROR_state()
		
			insert into B_eLog 
			select
				tipo			= 'E'
				,Status			= 'A'
				,mensagem		= @errortext
				,origem			= 'up_update_farmacia2'
				,data			= GETDATE()
				,usr			= 1
				,terminal		= 99
				,versao			= ''
				,sugestao		= ''
			
			return 1
		
		END CATCH

	END

go

grant execute on dbo.up_update_farmacia2 to public
grant control on dbo.up_update_farmacia2 to public
go