	/*
		FICHEIRO DE TRIGGERS DE UTILIZADOR DE SEGURAN�A
	*/

	
	/*
		cuidado: testar quando o UPDATE faz algo (ha linhas) e quando nao 
		update B_Parameters set bool=0 where stamp='adm0000000141'
		update B_Parameters set bool=0 where stamp='adm00000000141'
		select * from b_ocorrencias where tipo='Seguranca' order by date desc	
	*/
	
	
	if OBJECT_ID('[dbo].[ut_RegistaAlteraParam]') IS NOT NULL
		drop trigger dbo.ut_RegistaAlteraParam
	go

	CREATE TRIGGER [dbo].[ut_RegistaAlteraParam]
		ON  [dbo].[b_parameters]
	AFTER 
		UPDATE
	AS 
		BEGIN
			SET NOCOUNT ON;

			/* Se nao ha linhas a actualizar, nem tentar fazer o INSERT senao os valores sao NULLOS */
			if (select COUNT(stamp) from deleted) = 0
				return
			
			declare @old bit, @new bit
				
			select @old = bool from deleted
			select @new = bool from inserted
			
			insert into b_ocorrencias (stamp,linkstamp,tipo,grau,descr,ovalor,dvalor,usr,date,terminal)
			select
				stamp							= 'ADM'+left(replace(newid(),'-',''),23)
				,linkstamp						= (select stamp from inserted)
				,tipo							= 'Seguranca'
				,Grau							= 1
				,descr							= 'Par�metro [' + (select name from inserted) + '] alterado; Utilizador: [' + suser_sname() + ']'
				,oValor							= @old
				,dValor							= @new
				,usr							= 1
				,date							= getdate()
				,terminal						= 1
		END
		
	
	