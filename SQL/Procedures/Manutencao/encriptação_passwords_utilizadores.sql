declare @masterkey varchar(200)
select @masterkey= (SELECT DB_NAME()+replace(cast(getdate() as varchar(30)),' ','')+(CAST((SELECT CODFARM FROM empresa where no=1) as varchar(9)))+replace((SELECT dirtec FROM empresa where no=1),' ',''))

--Criação de chave primária para encriptação 
begin 
	IF NOT EXISTS (SELECT * FROM sys.symmetric_keys WHERE symmetric_key_id = 101)
	begin
		exec(' CREATE MASTER KEY ENCRYPTION BY PASSWORD = ''' + @masterkey + '''' )
	end
end 
GO

CREATE CERTIFICATE userpassword
   WITH SUBJECT = 'Users Password';
GO

CREATE SYMMETRIC KEY UPWD_Key_01
    WITH ALGORITHM = AES_256
    ENCRYPTION BY CERTIFICATE userpassword;
GO

-- Criação de nova coluna com chave encriptada
ALTER TABLE b_us
    ADD Encryptedpassw varbinary(128); 
GO

-- Abertura de chave para encriptação
OPEN SYMMETRIC KEY UPWD_Key_01
   DECRYPTION BY CERTIFICATE userpassword;

-- Encriptação da password
UPDATE b_us
SET Encryptedpassw = EncryptByKey(Key_GUID('UPWD_Key_01'), userpass);
close SYMMETRIC KEY UPWD_Key_01
GO

-- Remover campo antigo da password
--DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50)

--select @tableName		= 'b_us'
--	,@columnName		= 'userpass'
--select @constraintName	= (select o.name 
--							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
--							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
--							)
							
--if exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
--			and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
--begin
--	if @constraintName is not null
--	begin
--		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
--	end
	
--	exec('ALTER TABLE dbo.' + @tableName + ' drop column ' + @columnName)
--end

--go
