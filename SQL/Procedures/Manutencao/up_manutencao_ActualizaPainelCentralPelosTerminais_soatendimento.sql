-- up_manutencao_ActualizaPainelCentralPelosTerminais_soatendimento 6, 'Loja 1'
--select top 1 tipoempresa from empresa where site = 'Loja 1'

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_manutencao_ActualizaPainelCentralPelosTerminais_soatendimento]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[up_manutencao_ActualizaPainelCentralPelosTerminais_soatendimento]
GO

/****** Object:  StoredProcedure [dbo].[up_manutencao_ActualizaPainelCentralPelosTerminais_soatendimento]    Script Date: 20/11/2019 12:08:46 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

create procedure [dbo].[up_manutencao_ActualizaPainelCentralPelosTerminais_soatendimento]
	
		@tipo char(25) = 0
		,@site varchar(60) = ''


AS
	declare @no numeric(5,0)
	declare @tipoEmpresa varchar(20)
	
	/* Verifica tipo empresa*/
	set @tipoEmpresa = (select top 1 tipoempresa from empresa where site = @site)
	
	print @tipoEmpresa
	
	if @tipoEmpresa = 'FARMACIA' and @tipo = 0
	begin
		set @tipo = 1
	end
	if @tipoEmpresa = 'CLINICA' and @tipo = 0
	begin
		set @tipo = 2
	end
	if @tipoEmpresa = 'ARMAZEM' and @tipo = 0
	begin
		set @tipo = 3
	end
	if @tipoEmpresa = 'EMPRESA' and @tipo = 0
	begin
		set @tipo = 4
	end

	if @tipoEmpresa = 'PARAFARMACIA' and @tipo = 0
	begin
		set @tipo = 5
	end

	if @tipoEmpresa = 'PORTAL' 
	begin
		set @tipo = 6
	end
	
	
	/*Para não dar erro aplica configuração de empresa*/
	if @tipo = 0
	begin
		set @tipo = 4
	end
	
	print @tipo
	  	
	begin
	
		DECLARE terminais_cursor CURSOR FOR 
		SELECT distinct no FROM B_Terminal order by no

		OPEN terminais_cursor

		FETCH NEXT FROM terminais_cursor 
		INTO @no

	

		WHILE @@FETCH_STATUS = 0
		BEGIN
	   
			IF (select COUNT(terminal) from B_pcentral where terminal = @no) = 0
			BEGIN
				    			    
				insert into B_pcentral ([nome], [imagem], [imagem2], [texto], [tipo], [comando], [comandosql], [imagemSimbolo], [colIni], [colFin], [linIni], [linFin], [pagina], [terminal], [backStyle], [backColor], [borderColor], [borderWidth], [imgMenu], [txtMenu], [forcaMenu], [ocultaVoltar], [inactivo],[forecolor])
				Select [nome], [imagem], [imagem2], [texto], [tipo], [comando], [comandosql], [imagemSimbolo], [colIni], [colFin], [linIni], [linFin], [pagina], [terminal] = @no, [backStyle], [backColor], [borderColor], [borderWidth], [imgMenu], [txtMenu], [forcaMenu], [ocultaVoltar], [inactivo],[forecolor]
				From (	
					
					
					
					SELECT  N'atendimento' AS [nome],	   N'atendimento_n.png' AS [imagem], N'' AS [imagem2],              N'ATENDIMENTO' AS [texto], N'A' AS [tipo],						N'uf_GestaoCaixas_chama with .f., .t.' AS [comando], N'' AS [comandosql], N'' AS [imagemSimbolo], N'3' AS [colIni], N'3' AS [colFin], N'2' AS [linIni], N'3' AS [linFin], N'1' AS [pagina], @no AS [terminal], N'1' AS [backStyle],  N'212,241,249' AS [backColor], N'102,173,191' AS [borderColor], N'0' AS [borderWidth],			 N'' AS [imgMenu],     N'Farmácia' AS [txtMenu], N'1' AS [forcaMenu], N'0' AS [ocultaVoltar], N'0' AS [inactivo], N'16,89,107' as [foreColor] UNION ALL
					SELECT  N'gestaocaixa' AS [nome], N'pagcentralizados_n.png' AS [imagem], N'' AS [imagem2],				   N'CAIXAS' AS [texto], N'A' AS [tipo],						N'uf_GestaoCaixas_chama with .f., .f.' AS [comando], N'' AS [comandosql], N'' AS [imagemSimbolo], N'4' AS [colIni], N'4' AS [colFin], N'2' AS [linIni], N'3' AS [linFin], N'1' AS [pagina], @no AS [terminal], N'1' AS [backStyle],  N'212,241,249' AS [backColor], N'102,173,191' AS [borderColor], N'0' AS [borderWidth],			 N'' AS [imgMenu],     N'Farmácia' AS [txtMenu], N'1' AS [forcaMenu], N'0' AS [ocultaVoltar], N'0' AS [inactivo], N'16,89,107' as [foreColor] UNION ALL

					SELECT		   N'logo' AS [nome],		N'logitools_n.png'  AS [imagem], N'' AS [imagem2],						 N'' AS [texto], N'A' AS [tipo],														   N'' AS [comando], N'' AS [comandosql], N'' AS [imagemSimbolo], N'1' AS [colIni], N'1' AS [colFin], N'4' AS [linIni], N'4' AS [linFin], N'1' AS [pagina], @no AS [terminal], N'1' AS [backStyle],  N'255,255,255' AS [backColor], N'255,255,255' AS [borderColor], N'0' AS [borderWidth],			 N'' AS [imgMenu],		N'Farmácia' AS [txtMenu], N'1' AS [forcaMenu], N'0' AS [ocultaVoltar], N'0' AS [inactivo], N'' as [foreColor] UNION ALL

					SELECT      N'acessos'    AS [nome],   N'permissoes_n.png' AS [imagem], N'' AS [imagem2],				N'GESTÃO DE ACESSOS' AS [texto], N'A' AS [tipo],												  N'uf_acessoslt_chama' AS [comando],	N'' AS [comandosql], N'' AS [imagemSimbolo], N'2' AS [colIni], N'2' AS [colFin], N'2' AS [linIni], N'2' AS [linFin], N'3' AS [pagina], @no AS [terminal], N'1' AS [backStyle], N'253,234,198'  AS [backColor], N'182,112,43'  AS [borderColor], N'0' AS [borderWidth],			   N'' AS [imgMenu],		N'' AS [txtMenu], N'0' AS [forcaMenu], N'0' AS [ocultaVoltar], N'0' AS [inactivo], N'250,164,7' as [foreColor] UNION ALL
					SELECT  N'confsistema'    AS [nome],	 N'sistema_n.png'  AS [imagem], N'' AS [imagem2],		  N'CONFIGURAÇÃO DO SISTEMA' AS [texto], N'A' AS [tipo],											  N'uf_sistemaconfig_chama' AS [comando],	N'' AS [comandosql], N'' AS [imagemSimbolo], N'3' AS [colIni], N'3' AS [colFin], N'2' AS [linIni], N'2' AS [linFin], N'3' AS [pagina], @no AS [terminal], N'1' AS [backStyle], N'253,234,198'  AS [backColor], N'182,112,43'  AS [borderColor], N'0' AS [borderWidth],			   N'' AS [imgMenu],		N'' AS [txtMenu], N'0' AS [forcaMenu], N'0' AS [ocultaVoltar], N'0' AS [inactivo], N'250,164,7' as [foreColor] UNION ALL
					SELECT  N'confsistemaemp' AS [nome], N'sistema_empresa_n.png'  AS [imagem], N'' AS [imagem2], N'CONFIGURAÇÃO DO SISTEMA-EMPRESA' AS [texto], N'A' AS [tipo],										   N'uf_sistemaconfigemp_chama' AS [comando],	N'' AS [comandosql], N'' AS [imagemSimbolo], N'4' AS [colIni], N'4' AS [colFin], N'2' AS [linIni], N'2' AS [linFin], N'3' AS [pagina], @no AS [terminal], N'1' AS [backStyle], N'253,234,198'  AS [backColor], N'182,112,43'  AS [borderColor], N'0' AS [borderWidth],			   N'' AS [imgMenu],		N'' AS [txtMenu], N'0' AS [forcaMenu], N'0' AS [ocultaVoltar], N'0' AS [inactivo], N'250,164,7' as [foreColor] UNION ALL
					SELECT  N'integracoes'    AS [nome],  N'integracoes_n.png' AS [imagem], N'' AS [imagem2],					  N'INTEGRAÇÕES' AS [texto], N'A' AS [tipo],											   N'uf_gerais_integracoes' AS [comando],	N'' AS [comandosql], N'' AS [imagemSimbolo], N'5' AS [colIni], N'5' AS [colFin], N'2' AS [linIni], N'2' AS [linFin], N'3' AS [pagina], @no AS [terminal], N'1' AS [backStyle], N'253,234,198'  AS [backColor], N'182,112,43'  AS [borderColor], N'0' AS [borderWidth],			   N'' AS [imgMenu],		N'' AS [txtMenu], N'0' AS [forcaMenu], N'0' AS [ocultaVoltar], N'0' AS [inactivo], N'250,164,7' as [foreColor] UNION ALL
					SELECT  N'restartRobot'   AS [nome],   N'sistema_n.png' AS [imagem], N'' AS [imagem2],				N'REINICIAR ROBOT' AS [texto], N'A' AS [tipo],											N'uf_painelcentral_restart_robot' AS [comando],	N'' AS [comandosql], N'' AS [imagemSimbolo], N'2' AS [colIni], N'2' AS [colFin], N'3' AS [linIni], N'3' AS [linFin], N'3' AS [pagina], @no AS [terminal], N'1' AS [backStyle], N'253,234,198'  AS [backColor], N'182,112,43'  AS [borderColor], N'0' AS [borderWidth],			   N'' AS [imgMenu],		N'' AS [txtMenu], N'0' AS [forcaMenu], N'0' AS [ocultaVoltar], N'0' AS [inactivo], N'250,164,7' as [foreColor] UNION ALL
					SELECT		   N'empresa' AS [nome],	  N'empresa_w.png' AS [imagem], N'' AS [imagem2],			N'CONFIGURAÇÃO EMPRESAS' AS [texto], N'A' AS [tipo],												N'uf_pesqempresa_chama' AS [comando],	N'' AS [comandosql], N'' AS [imagemSimbolo], N'3' AS [colIni], N'3' AS [colFin], N'3' AS [linIni], N'3' AS [linFin], N'3' AS [pagina], @no AS [terminal], N'1' AS [backStyle], N'253,234,198' AS [backColor], N'238,142,56'  AS [borderColor], N'0' AS [borderWidth],			   N'' AS [imgMenu],		N'' AS [txtMenu], N'1' AS [forcaMenu], N'0' AS [ocultaVoltar], N'0' AS [inactivo], N'250,164,7' as [foreColor] UNION ALL				
					SELECT	  N'utilizadores' AS [nome],		N'users_n.png' AS [imagem], N'' AS [imagem2], 		   N'GESTÃO DE UTILIZADORES' AS [texto], N'A' AS [tipo],											'uf_pesqutilizadores_chama' AS [comando],	N'' AS [comandosql], N'' AS [imagemSimbolo], N'4' AS [colIni], N'4' AS [colFin], N'3' AS [linIni], N'3' AS [linFin], N'3' AS [pagina], @no AS [terminal], N'1' AS [backStyle], N'253,234,198' AS [backColor], N'238,142,56'  AS [borderColor], N'0' AS [borderWidth],			   N'' AS [imgMenu],		N'' AS [txtMenu], N'1' AS [forcaMenu], N'0' AS [ocultaVoltar], N'0' AS [inactivo], N'250,164,7' as [foreColor] UNION ALL
					SELECT	      N'recursos' AS [nome],	   N'recursos_n.png' AS [imagem], N'' AS [imagem2],			   N'GESTÃO DE RECURSOS' AS [texto], N'A' AS [tipo],                                           N'uf_recursos_chama with ""' AS [comando],   N'' AS [comandosql], N'' AS [imagemSimbolo], N'5' AS [colIni], N'5' AS [colFin], N'3' AS [linIni], N'3' AS [linFin], N'3' AS [pagina], @no AS [terminal], N'1' AS [backStyle], N'253,234,198'  AS [backColor], N'238,142,56' AS [borderColor], N'0' AS [borderWidth],			   N'' AS [imgMenu],		N'' AS [txtMenu], N'0' AS [forcaMenu], N'0' AS [ocultaVoltar], N'0' AS [inactivo], N'250,164,7' as [foreColor] UNION ALL

					SELECT		 N'contactos' AS [nome],	  N'suporte_w.png' AS [imagem], N'' AS [imagem2],			 N'CONTACTOS' AS [texto], N'A' AS [tipo],										   N'uf_painelcentral_alternaPagina with 5' AS [comando],	N'' AS [comandosql], N'' AS [imagemSimbolo], N'2' AS [colIni], N'2' AS [colFin], N'2' AS [linIni], N'2' AS [linFin], N'4' AS [pagina], @no AS [terminal], N'1' AS [backStyle], N'238,142,56'  AS [backColor], N'182,112,43'  AS [borderColor], N'0' AS [borderWidth],			   N'' AS [imgMenu],		N'' AS [txtMenu], N'0' AS [forcaMenu], N'0' AS [ocultaVoltar], N'0' AS [inactivo], N'' as [foreColor] UNION ALL
					SELECT		N'circulares' AS [nome],   N'doc_linhas_w.png' AS [imagem], N'' AS [imagem2],			N'CIRCULARES' AS [texto], N'A' AS [tipo],													 N'uf_painelcentral_circulares' AS [comando],	N'' AS [comandosql], N'' AS [imagemSimbolo], N'3' AS [colIni], N'3' AS [colFin], N'2' AS [linIni], N'2' AS [linFin], N'4' AS [pagina], @no AS [terminal], N'1' AS [backStyle], N'238,142,56'  AS [backColor], N'182,112,43'  AS [borderColor], N'0' AS [borderWidth],			   N'' AS [imgMenu],		N'' AS [txtMenu], N'0' AS [forcaMenu], N'0' AS [ocultaVoltar], N'0' AS [inactivo], N'' as [foreColor] UNION ALL
					SELECT		   N'manuais' AS [nome],   N'doc_linhas_w.png' AS [imagem], N'' AS [imagem2],	  N'MANUAIS TECNICOS' AS [texto], N'A' AS [tipo],		 												N'uf_painelcentral_manuais' AS [comando],	N'' AS [comandosql], N'' AS [imagemSimbolo], N'4' AS [colIni], N'4' AS [colFin], N'2' AS [linIni], N'2' AS [linFin], N'4' AS [pagina], @no AS [terminal], N'1' AS [backStyle], N'238,142,56'  AS [backColor], N'182,112,43'  AS [borderColor], N'0' AS [borderWidth],			   N'' AS [imgMenu],		N'' AS [txtMenu], N'0' AS [forcaMenu], N'0' AS [ocultaVoltar], N'0' AS [inactivo], N'' as [foreColor] UNION ALL
					SELECT			N'termos' AS [nome],   N'informacao_w.png' AS [imagem], N'' AS [imagem2], N'POLITICA PRIVACIDADE' AS [texto], N'A' AS [tipo],										   N'uf_painelcentral_alternaPagina with 6' AS [comando],	N'' AS [comandosql], N'' AS [imagemSimbolo], N'5' AS [colIni], N'5' AS [colFin], N'2' AS [linIni], N'2' AS [linFin], N'4' AS [pagina], @no AS [terminal], N'1' AS [backStyle], N'238,142,56'  AS [backColor], N'182,112,43'  AS [borderColor], N'0' AS [borderWidth],			   N'' AS [imgMenu],		N'' AS [txtMenu], N'0' AS [forcaMenu], N'0' AS [ocultaVoltar], N'0' AS [inactivo], N'' as [foreColor] UNION ALL
					SELECT		N'wdgsistema' AS [nome],	  			   N'' AS [imagem], N'' AS [imagem2],	N'SISTEMA / TERMINAL' AS [texto], N'W' AS [tipo],																			    N'' AS [comando],	N'' AS [comandosql], N'' AS [imagemSimbolo], N'3' AS [colIni], N'4' AS [colFin], N'3' AS [linIni], N'3' AS [linFin], N'4' AS [pagina], @no AS [terminal], N'0' AS [backStyle], N'240,240,240' AS [backColor], N'204,204,204' AS [borderColor], N'0' AS [borderWidth],			   N'' AS [imgMenu],		N'' AS [txtMenu], N'0' AS [forcaMenu], N'0' AS [ocultaVoltar], N'0' AS [inactivo], N'' as [foreColor] UNION ALL

					SELECT		N'wdgbrowser' AS [nome],				   N'' AS [imagem], N'' AS [imagem2],			   N'SUPORTE' AS [texto], N'W' AS [tipo],  N'uf_wdgbrowser_abreURL with "http://logitools.pt/contatos.htm", sys(1272,this)' AS [comando],	N'' AS [comandosql], N'' AS [imagemSimbolo], N'1' AS [colIni], N'6' AS [colFin], N'1' AS [linIni], N'4' AS [linFin], N'5' AS [pagina], @no AS [terminal], N'0' AS [backStyle], N'240,240,240' AS [backColor], N'204,204,204' AS [borderColor], N'0' AS [borderWidth], N'sistema_w.png' AS [imgMenu], N'Sistema' AS [txtMenu], N'0' AS [forcaMenu], N'0' AS [ocultaVoltar], N'0' AS [inactivo], N'' as [foreColor] UNION ALL

					SELECT		N'wdgbrowser' AS [nome],				   N'' AS [imagem], N'' AS [imagem2],			   N'SUPORTE' AS [texto], N'W' AS [tipo],	 N'uf_wdgbrowser_abreURL with "c:\logitools\termos\intro.html", sys(1272,this)' AS [comando],	N'' AS [comandosql], N'' AS [imagemSimbolo], N'1' AS [colIni], N'6' AS [colFin], N'1' AS [linIni], N'3' AS [linFin], N'6' AS [pagina], @no AS [terminal], N'0' AS [backStyle], N'240,240,240' AS [backColor], N'204,204,204' AS [borderColor], N'0' AS [borderWidth], N'sistema_w.png' AS [imgMenu], N'Sistema' AS [txtMenu], N'0' AS [forcaMenu], N'0' AS [ocultaVoltar], N'0' AS [inactivo], N'' as [foreColor] UNION ALL
					SELECT		  N'concordo' AS [nome],        N'visto_w.png' AS [imagem], N'' AS [imagem2],				N'ACEITO' AS [texto], N'A' AS [tipo],		 											N'uf_painelcentral_user_aceita' AS [comando],	N'' AS [comandosql], N'' AS [imagemSimbolo], N'3' AS [colIni], N'3' AS [colFin], N'4' AS [linIni], N'4' AS [linFin], N'6' AS [pagina], @no AS [terminal], N'1' AS [backStyle], N'238,142,56'  AS [backColor], N'182,112,43'  AS [borderColor], N'0' AS [borderWidth],			   N'' AS [imgMenu],		N'' AS [txtMenu], N'0' AS [forcaMenu], N'0' AS [ocultaVoltar], N'0' AS [inactivo], N'' as [foreColor] UNION ALL
					SELECT       N'nconcordo' AS [nome],         N'cruz_w.png' AS [imagem], N'' AS [imagem2],			N'NÃO ACEITO' AS [texto], N'A' AS [tipo],													N'uf_painelcentral_user_recusa' AS [comando],	N'' AS [comandosql], N'' AS [imagemSimbolo], N'4' AS [colIni], N'4' AS [colFin], N'4' AS [linIni], N'4' AS [linFin], N'6' AS [pagina], @no AS [terminal], N'1' AS [backStyle], N'238,142,56'  AS [backColor], N'182,112,43'  AS [borderColor], N'0' AS [borderWidth],			   N'' AS [imgMenu],		N'' AS [txtMenu], N'0' AS [forcaMenu], N'0' AS [ocultaVoltar], N'0' AS [inactivo], N'' as [foreColor]
					
				) a
					
				PRINT 'Terminal ' + STR(@no) + ' configurado com sucesso'
			END
			ELSE
			BEGIN
				PRINT 'Terminal ' + STR(@no) + ' já estava configurado'
			END
			
					
			FETCH NEXT FROM terminais_cursor 
			INTO @no
		END 
		CLOSE terminais_cursor;
		DEALLOCATE terminais_cursor;
	End


	

