

-- Comparar estrutura da BD com o dicionario de Dados
-- exec up_manutencao_DiferencasBDClienteDic

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_manutencao_DiferencasBDClienteDic]') IS NOT NULL
	drop procedure dbo.up_manutencao_DiferencasBDClienteDic
go

create procedure dbo.up_manutencao_DiferencasBDClienteDic

/* WITH ENCRYPTION */

AS

/* 1 Tabelas e campos que não existem na BD do cliente*/
select 
	tabela
	,campo
	,descricao = 'Existe no dicionario e Não existe na BD do Cliente' 
	,tipoDicDados = ''
	,tipoBdCliente = ''
	,defaultDicDados =  ''
	,defaultBdCliente = ''
	,acao = 'Criar tabela/campo'
from 
	dicionarioDados 
	left join INFORMATION_SCHEMA.COLUMNS on TABLE_NAME = tabela and COLUMN_NAME = campo
where
	TABLE_NAME is null

union all


/* 2 Tabelas existem na BD e nao existem no dicionario de Dados*/
/* Acção Eliminar Tabela */
select 
	distinct tabela = TABLE_NAME
	,campo = COLUMN_NAME
	,descricao = 'Tabela Existe na BD do cliente e Não existe no dicionario'
	,tipoDicDados = ''
	,tipoBdCliente = ''
	,defaultDicDados =  ''
	,defaultBdCliente = ''
	,acao = 'Eliminar tabela/campo'
from 
	INFORMATION_SCHEMA.COLUMNS
	left join dicionarioDados on TABLE_NAME = tabela and COLUMN_NAME = campo
where
	dicionarioDados.tabela is null

union all

/* 3 Diferenças Tipos */
select 
	tabela = TABLE_NAME
	,campo = COLUMN_NAME
	,descricao = 'Tipo de Campo diferente'
	,tipoDicDados = tipo 
	,tipoBdCliente = CASE 
			WHEN DATA_TYPE = 'varchar' or DATA_TYPE = 'char' then DATA_TYPE + '(' + convert(varchar,CHARACTER_MAXIMUM_LENGTH) + ')'
			WHEN DATA_TYPE = 'numeric' then DATA_TYPE + '(' + convert(varchar,NUMERIC_PRECISION) + ',' + convert(varchar,NUMERIC_SCALE) + ')' 
			ELSE DATA_TYPE 
		END
	,defaultDicDados =  valordefaullt
	,defaultBdCliente = isnull(COLUMN_DEFAULT,'')
	,acao = 'Alterar tipo ou default do campo'
from 
	INFORMATION_SCHEMA.COLUMNS
	inner join dicionarioDados on TABLE_NAME = tabela and COLUMN_NAME = campo
where 
	LEFT(TABLE_NAME,4) != 'temp'
	and 
	tipo !=
		CASE 
			WHEN DATA_TYPE = 'varchar' or DATA_TYPE = 'char' then DATA_TYPE + '(' + convert(varchar,CHARACTER_MAXIMUM_LENGTH) + ')'
			WHEN DATA_TYPE = 'numeric' then DATA_TYPE + '(' + convert(varchar,NUMERIC_PRECISION) + ',' + convert(varchar,NUMERIC_SCALE) + ')'
			ELSE DATA_TYPE 
		END
	--or REPLACE(REPLACE(valordefaullt,'(',''),')','') != REPLACE(REPLACE(isnull(COLUMN_DEFAULT,''),'(',''),')','')


GO
Grant Execute On dbo.up_manutencao_DiferencasBDClienteDic to Public
Grant Control On dbo.up_manutencao_DiferencasBDClienteDic to Public
GO
-------------------------------------------------


