
-- Aplica manuten��o gen�rica/geral � base de dados
-- exec up_manutencao_geral 

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_manutencao_geral]') IS NOT NULL
	drop procedure dbo.up_manutencao_geral
go

create procedure dbo.up_manutencao_geral

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

dbcc freesystemcache('sql plans')
dbcc freeproccache
dbcc dropcleanbuffers
exec sp_updatestats

GO
Grant Execute On dbo.up_manutencao_geral to Public
Grant Control On dbo.up_manutencao_geral to Public
GO
-------------------------------------------------


