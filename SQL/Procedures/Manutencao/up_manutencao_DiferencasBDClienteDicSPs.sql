
-- Comparar estrutura da BD com o dicionario de Dados
-- exec up_manutencao_DiferencasBDClienteDicSPs

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_manutencao_DiferencasBDClienteDicSPs]') IS NOT NULL
	drop procedure dbo.up_manutencao_DiferencasBDClienteDicSPs
go

create procedure dbo.up_manutencao_DiferencasBDClienteDicSPs

/* WITH ENCRYPTION */

AS


/* 1 SPS que n�o existem na BD do cliente*/
select 
	nome
	,descricao = 'Existe no dicionario e N�o existe na BD do Cliente' 
	,acao = 'Criar SP'
from 
	dicionarioDadosSps 
	left join INFORMATION_SCHEMA.ROUTINES on ROUTINE_NAME = nome
where
	ROUTINE_NAME is null

union all


/* 2 SPs existem na BD e nao existem no dicionario de Dados*/
/* Ac��o Eliminar SP */
select 
	ROUTINE_NAME
	,descricao = 'SP Existe na BD do cliente e N�o existe no dicionario'
	,acao = 'Eliminar SP'
from 
	INFORMATION_SCHEMA.ROUTINES
	left join dicionarioDadosSps on ROUTINE_NAME = nome
where
	nome is null

union all

/* 3 Diferen�as Tipos */
select 
	tabela = nome
	,descricao = 'C�digo SP diferente'
	,acao = 'Compliar SP novamente'
from 
	INFORMATION_SCHEMA.ROUTINES
	left join dicionarioDadosSps on ROUTINE_NAME = nome
where 
	ISNULL(ROUTINE_DEFINITION,'') != dicionarioDadosSps.codigo
	




GO
Grant Execute On dbo.up_manutencao_DiferencasBDClienteDicSPs to Public
Grant Control On dbo.up_manutencao_DiferencasBDClienteDicSPs to Public
GO
-------------------------------------------------
