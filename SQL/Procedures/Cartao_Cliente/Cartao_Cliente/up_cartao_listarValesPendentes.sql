-- Procurar Vales Pendentes
-- exec up_cartao_listarValesPendentes 2000, '', 0, 999, ''
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cartao_listarValesPendentes]') IS NOT NULL
	drop procedure dbo.up_cartao_listarValesPendentes
go

create procedure dbo.up_cartao_listarValesPendentes

@top int,
@nome varchar(55),
@no numeric(10,0),
@estab numeric(3,0),
@tipo varchar(20),
@abatidos bit = 0,
@anulado bit = 0,
@dataInicio as datetime = '19000101',
@dataFim as datetime = '30000101'



/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

if @top=0
	set @top=1000

select 
	top(@top) *
from (

	select
		'fvstamp'		=	fvstamp,
		'nr'			=	nr,
		'ref'			=	ref,
		'valor'			=	valor,
		'pontos'		=	pontos,
		'clstamp'		=	vale.clstamp,
		'anular'		=	CONVERT(bit,0),
		'nome'			=	b_utentes.nome,
		'no'			=	b_utentes.no,
		'estab'			=	b_utentes.estab,
		'tipo'			=	b_utentes.tipo,
		'u_nrcartao'	=	b_utentes.nrcartao,
		'pontosLivres'	=	isnull((B_fidel.pontosatrib-B_fidel.pontosusa),0),
		vale.abatido,
		vale.anulado,
		dataEmissao	= vale.ousrdata
	from
		B_fidelvale vale (nolock)
		inner join b_utentes (nolock) on b_utentes.utstamp=vale.clstamp
		left join B_fidel (nolock) on B_fidel.clstamp=b_utentes.utstamp
	where
		vale.abatido = Case when @abatidos = 1 then vale.abatido else 0 end
		and vale.anulado = Case when @anulado = 1 then vale.anulado else 0 end
		AND b_utentes.inactivo=0 
		AND (b_utentes.nome like @nome+'%')
		AND b_utentes.no	 = case when @no=0 then b_utentes.[no] else @no end
		AND b_utentes.estab = case when @estab=999 then b_utentes.estab else @estab end
		AND b_utentes.tipo	 = case when @tipo='' then b_utentes.tipo else @tipo end
		AND convert(date,vale.ousrdata) between @dataInicio and @dataFim
		
) x
order by nr

Go
Grant Execute on dbo.up_cartao_listarValesPendentes to Public
Grant Control on dbo.up_cartao_listarValesPendentes to Public
Go
