-- ver pontos  (STOUCHPOS)
/* 
	select * from b_fidel
	exec up_cartao_eliminarPontosNC 'ADMA03415B2-EC32-42EA-B7D', 249,0 

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO 

if OBJECT_ID('[dbo].[up_cartao_eliminarPontosNC]') IS NOT NULL
	drop procedure dbo.up_cartao_eliminarPontosNC
go

create procedure dbo.up_cartao_eliminarPontosNC
@ftstamp as varchar(25),
@no numeric,
@estab numeric

/* WITH ENCRYPTION */
AS

	declare @pontosRetirar as numeric(9,0)

	set @pontosRetirar = isnull((Select SUM(fi.pontos) from fi where ftstamp = @ftstamp),0)

	IF @pontosRetirar != 0
	BEGIN	
		update b_fidel set pontosatrib = pontosatrib + @pontosRetirar where B_fidel.clno = @no and B_fidel.clestab = @estab
	END 


GO
Grant Execute on dbo.up_cartao_eliminarPontosNC to Public
GO
Grant Control on dbo.up_cartao_eliminarPontosNC to Public
Go
