-- ver vales disponíveis
-- exec up_cartao_aplicarValesPendentes 233, 0
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cartao_aplicarValesPendentes]') IS NOT NULL
	drop procedure dbo.up_cartao_aplicarValesPendentes
go

create procedure dbo.up_cartao_aplicarValesPendentes

@no numeric,
@estab numeric

/* WITH ENCRYPTION */
AS

	SET NOCOUNT ON

	DECLARE @valMes AS DECIMAL(20,5) = (SELECT numValue FROM B_Parameters(NOLOCK) where stamp = 'ADM0000000017')


	select 
		 B_fidelvale.fvstamp
		,B_fidelvale.ref
		,B_fidelvale.valor
		,B_fidelvale.pontos
		,B_fidelvale.clstamp
		,pontosDisp	= isnull(B_fidel.pontosatrib-B_fidel.pontosusa,0)
		,sel = convert(bit,0)
		,estab = B_fidel.clestab
		,data = B_fidelVale.ousrdata
		,idade_meses = DATEDIFF(MONTH, B_fidelVale.ousrdata, getdate())
	from 
		B_fidelvale (nolock)
		left join B_fidel (nolock) on B_fidel.clstamp = B_fidelVale.clstamp and convert(varchar,B_fidel.validade,112) >= convert(varchar,GETDATE(),112)
		inner join b_utentes (nolock) on b_utentes.utstamp=B_fidelvale.clstamp 
	where 
		anulado=0 
		and abatido=0 
		and b_utentes.no=@no
		and CONVERT(varchar, B_fidelvale.ousrdata, 112)<CONVERT(varchar, getdate(), 112)
		AND CONVERT(VARCHAR,DATEADD(MONTH,@valMes, B_fidelvale.ousrdata),112) >= CONVERT(varchar, GETDATE(),112)
				
GO
Grant Execute on dbo.up_cartao_aplicarValesPendentes to Public
Grant Control on dbo.up_cartao_aplicarValesPendentes to Public
Go