-- ver pontos  (STOUCHPOS)
/* exec up_cartao_pontosFidel 254, 0 */
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO 

if OBJECT_ID('[dbo].[up_cartao_pontosFidel]') IS NOT NULL
	drop procedure dbo.up_cartao_pontosFidel
go

create procedure dbo.up_cartao_pontosFidel

@no numeric,
@estab numeric

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select 
	b_utentes.utstamp
	, nome
	, nome2
	, [no]
	, estab
	, ISNULL(fidel.inactivo,0) inactivo
	, b_utentes.tipo
	, b_utentes.nrcartao as nrcartao
	, pontoslivres = case when (select textvalue from b_parameters where stamp='ADM0000000302')='Pontos' then isnull(fidel.pontosatrib-fidel.pontosusa,0) else ISNULL(fidel.valcartao,0) end
	, pontostotal = case when (select textvalue from b_parameters where stamp='ADM0000000302')='Pontos' then isnull(fidel.pontosatrib,0) else ISNULL(fidel.valcartaohist,0) end
from 
	b_utentes (nolock)
left join
	B_fidel fidel on b_utentes.utstamp=fidel.clstamp
where
	(b_utentes.[no]=@no and b_utentes.estab=@estab) and b_utentes.inactivo=0


GO
Grant Execute on dbo.up_cartao_pontosFidel to Public
GO
Grant Control on dbo.up_cartao_pontosFidel to Public
Go