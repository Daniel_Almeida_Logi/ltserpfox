-- Procurar Regras do Cart�o de Cliente
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

If OBJECT_ID('[dbo].[up_cartao_listarRegras]') IS NOT NULL
	drop procedure dbo.up_cartao_listarRegras
Go

create procedure dbo.up_cartao_listarRegras

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select * from B_fidelregras (nolock)


GO
Grant Execute on dbo.up_cartao_listarRegras to Public
GO
Grant Control on dbo.up_cartao_listarRegras to Public
Go
