-- Procurar Clientes sem Cart�o
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cartao_listarClSemCartao]') IS NOT NULL
	drop procedure dbo.up_cartao_listarClSemCartao
go

create procedure dbo.up_cartao_listarClSemCartao

@top int,
@nome varchar(55),
@no numeric(10,0),
@estab numeric(3,0),
@tipo varchar(20)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

if @top=0
	set @top=1000

select top(@top) 
	nome
	,[no]
	,estab
	,tipo
	,utstamp
from 
	b_utentes (nolock)
where
	 b_utentes.inactivo=0 and b_utentes.nrcartao='' and b_utentes.[no]>200
	and (b_utentes.nome like @nome+'%')
	and [no] = case when @no=0 then [no] else @no end
	and estab = case when @estab=999 then estab else @estab end
	and tipo = case when @tipo='' then tipo else @tipo end
order by nome


GO
Grant Execute on dbo.up_cartao_listarClSemCartao to Public
Grant Control on dbo.up_cartao_listarClSemCartao to Public
Go
