-- Procurar Cart�es de Cliente
-- exec up_cartao_relatorio_valor '20210101', '20220131', 'Loja 1','',1

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cartao_relatorio_valor]') IS NOT NULL
	drop procedure dbo.up_cartao_relatorio_valor
go

create procedure dbo.up_cartao_relatorio_valor

@dataini as datetime,
@datafim as datetime,
@site as varchar(30),
@tipo varchar(20),
@cartao bit = 1

/* WITH ENCRYPTION */
AS
	SET NOCOUNT ON	

	SELECT
		  nrcartao			= ISNULL(b_utentes.nrcartao,'')
		, clno				= ISNULL(b_utentes.no,0)
		, clestab			= ISNULL(b_utentes.estab,0)
		, nome				= b_utentes.nome	
		, nif				= b_utentes.ncont
		, morada			= b_utentes.morada
		, id				= b_utentes.no_ext
		, telem				= b_utentes.tlmvl
		, telf				= b_utentes.telefone
		, profissao			= b_utentes.profi
		, idade				= CASE WHEN YEAR(b_utentes.nascimento)=1900 THEN 0 
									ELSE DATEDIFF(YEAR, b_utentes.nascimento, GETDATE()) END
		, sexo				= b_utentes.sexo
		, tipo				= b_utentes.tipo
		, valcartaoacum		= ISNULL(b_fidel.valcartaohist,0)
		, valcartaousado	= ISNULL(b_fidel.valcartaousado,0)
		, valvendas			= ISNULL((SELECT SUM(etotal) FROM ft 
										WHERE ft.no=b_utentes.no AND ft.estab=b_utentes.estab 
											AND fdata BETWEEN @dataini AND @datafim
											AND site = (CASE WHEN @site = 'Todas' OR @site='' THEN ft.site 
																ELSE @site END)
									),0)
		, carregamentos		= ISNULL((SELECT SUM(nvalor) FROM B_ocorrencias 
										WHERE tipo='Cart�o Cliente' AND tiporeg='ALTVALCART' 
												AND linkStamp=b_utentes.utstamp),0)
		, valorDisponivel	= ISNULL(b_fidel.valcartao,0) 

	FROM
		b_utentes (NOLOCK)
		LEFT JOIN B_fidel (NOLOCK) ON b_utentes.no=clno AND b_utentes.estab=clestab
	WHERE
		b_utentes.inactivo=0
		AND b_utentes.no > 200
		AND b_utentes.tipo	= CASE WHEN @tipo='' THEN b_utentes.tipo ELSE @tipo END
		AND b_utentes.nrcartao != CASE WHEN @cartao = 1 THEN '' ELSE 'xxxx' END
	ORDER BY 
		b_utentes.no

GO
Grant Execute on dbo.up_cartao_relatorio_valor to Public
Grant Control on dbo.up_cartao_relatorio_valor to Public
Go
