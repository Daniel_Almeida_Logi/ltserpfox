-- Procurar Cart�es de Cliente
-- exec up_cartao_listarFidel 0, '', 204, 0, '',0
-- exec up_cartao_listarFidel 1000, 'jos�', 0, 999, '',0
--exec up_cartao_listarFidel 0, 'jos�', 0, 0, '',0

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cartao_listarFidel]') IS NOT NULL
	drop procedure dbo.up_cartao_listarFidel
go

create procedure dbo.up_cartao_listarFidel

@top int,
@nome varchar(55),
@no numeric(10,0),
@estab numeric(3,0),
@tipo varchar(20),
@cartao bit = 1

/* WITH ENCRYPTION */
AS

	SET NOCOUNT ON	

	if @top=0
	set @top=10000000

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFreq'))
		DROP TABLE #dadosFreq
	
	/* guardar margem de actividade */
	declare @margemActivo numeric(2), @dataActivo datetime

	select @margemActivo = convert(int,numValue) from B_Parameters (nolock) where stamp='ADM0000000117'
	Select @dataActivo = convert(varchar,(select DATEADD(mm, -@margemActivo, getdate())),102)

	Select
		--ft.no
		--,ft.estab
		--,
		ft.ncont
		,ultVenda = MAX(fdata) 
	into	
		#dadosFreq
	From
		ft (nolock)
	where 
		ft.nome like @nome+'%' 
		and ft.no = case when @no=0 then ft.no else @no end
		and ft.estab	= case when @estab=999 then ft.estab else @estab end
		and nmdoc not like '%Impor%' and nmdoc not like '%Manual%'
	Group by 	
		--ft.no
		--,ft.estab
		--,
		ft.ncont
		

	select
		top(@top)
		 fstamp = ISNULL(fidel.fstamp,'')
		,clstamp = b_utentes.utstamp
		,b_utentes.nome

		,clno = isnull(fidel.clno,b_utentes.no)
		,clestab = isnull(fidel.clestab,b_utentes.estab)
		,nrcartao = isnull(fidel.nrcartao,b_utentes.nrcartao)
		,pontosatrib = case when (select textvalue from b_parameters where stamp='ADM0000000302')='Pontos' then isnull(fidel.pontosatrib,0) else ISNULL(fidel.valcartaohist,0) end 
		,pontosusa	= case when (select textvalue from b_parameters where stamp='ADM0000000302')='Pontos' then  (isnull(fidel.pontosatrib,0) - isnull(fidel.pontosusa,0)) else ISNULL(fidel.valcartao,0) end 
		,b_utentes.tipo
		,inactivo = isnull(fidel.inactivo,CONVERT(bit,0))
		,atendimento = isnull(fidel.atendimento,0)
		,validade = isnull(CONVERT(varchar,validade,104),'')
		,fidel.ousrdata
		,fidel.usrdata
		,ousr
		,usr
		,sel =	convert(bit,0)
		,estado	=	case when Freq.ultVenda >= @dataActivo then 'Activo' else 'Inactivo' end

	from
		b_utentes (nolock)
		--left join #dadosFreq as Freq on Freq.no = b_utentes.no and Freq.estab = b_utentes.estab
		left join #dadosFreq as Freq on Freq.ncont = b_utentes.ncont
		left join B_fidel fidel (nolock) on b_utentes.no=fidel.clno and b_utentes.estab=fidel.clestab
	where
		b_utentes.inactivo=0
		and b_utentes.no > 200
		and (b_utentes.nome like @nome+'%' or fidel.nrcartao like @nome+'%')
		and b_utentes.no = case when @no=0 then b_utentes.no else @no end
		and b_utentes.estab	= case when @estab=999 then b_utentes.estab else @estab end
		and b_utentes.tipo	= case when @tipo='' then b_utentes.tipo else @tipo end
		and b_utentes.nrcartao != case when @cartao = 1 then '' else 'xxxx' end
	order by 
		b_utentes.no

GO
Grant Execute on dbo.up_cartao_listarFidel to Public
Grant Control on dbo.up_cartao_listarFidel to Public
Go
