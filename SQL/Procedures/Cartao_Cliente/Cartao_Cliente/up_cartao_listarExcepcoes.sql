-- Procurar Excepcoes do Cart�o de Cliente
-- exec up_cartao_listarExcepcoes ''
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

If OBJECT_ID('[dbo].[up_cartao_listarExcepcoes]') IS NOT NULL
	drop procedure dbo.up_cartao_listarExcepcoes
Go

create procedure dbo.up_cartao_listarExcepcoes

@tipo varchar(50)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

SELECT *
FROM 
	b_fidelprod (nolock)
where 
	tipocl= case when @tipo='' then tipocl else @tipo end
	AND negado=1

GO
Grant Execute on dbo.up_cartao_listarExcepcoes to Public
Grant Control on dbo.up_cartao_listarExcepcoes to Public
Go
