-- Dados Impress�o Vale
-- exec up_cartao_printVale 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cartao_printVale]') IS NOT NULL
	drop procedure dbo.up_cartao_printVale
go

create procedure dbo.up_cartao_printVale

@fvstamp varchar(25)



/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select 
	*
from (

	select
		'fvstamp'		=	fvstamp,
		'nr'			=	nr,
		'ref'			=	ref,
		'valor'			=	valor,
		'pontos'		=	pontos,
		'clstamp'		=	vale.clstamp,
		'anular'		=	CONVERT(bit,0),
		'nome'			=	b_utentes.nome,
		'no'			=	b_utentes.no,
		'estab'			=	b_utentes.estab,
		'tipo'			=	b_utentes.tipo,
		'u_nrcartao'	=	b_utentes.nrcartao,
		'pontosLivres'	=	isnull((B_fidel.pontosatrib-B_fidel.pontosusa),0),
		vale.abatido,
		vale.anulado,
		dataEmissao	= vale.ousrdata
	from
		B_fidelvale vale (nolock)
		inner join b_utentes (nolock) on b_utentes.utstamp=vale.clstamp
		left join B_fidel (nolock) on B_fidel.clstamp=b_utentes.utstamp
	where
		vale.fvstamp = @fvstamp
		
) x
order by nr

Go
Grant Execute on dbo.up_cartao_printVale to Public
Grant Control on dbo.up_cartao_printVale to Public
Go
