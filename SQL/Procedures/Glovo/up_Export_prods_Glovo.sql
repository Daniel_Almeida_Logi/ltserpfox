/****** Object:  StoredProcedure [dbo].[up_Export_prods_Glovo]  

exec up_Export_prods_Glovo 'FFerrSilva_NS','Loja 1','u_local2','robot',1
update b_parameters_site set numvalue = 0 WHERE stamp='ADM0000000222'
Script Date: 23/04/2024 11:31:42 ******/


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_Export_prods_Glovo]') IS NOT NULL
	drop procedure dbo.up_Export_prods_Glovo
go

create procedure [dbo].up_Export_prods_Glovo
@store_id varchar (100),			--criado por nós, único por loja
@site varchar (100),
@filtro varchar (100),				--campo na st que se quer filtrar
@descrfiltro varchar (100),			--valor para o qual se filtrar
@headers bit						--se contém cabeçalhos


/* with encryption */
AS
SET NOCOUNT ON
/* Elimina Tabelas */


DECLARE @no varchar (3)= (select no from empresa (nolock) where site =@site)
--PRINT @no
DECLARE @sql varchar (max)
DECLARE @headersdescr varchar (max) = '''store_id'',''product_id'',''price'',''stock'''
DECLARE @margemextra varchar (5) = 
		(select CONVERT(varchar,numvalue) from B_Parameters_site WHERE stamp='ADM0000000222' and site = @site)

BEGIN
IF @headers = 1 

Select @sql = '
SELECT '+@headersdescr+'
UNION ALL
select '''+@store_id+''',ref, CONVERT(VARCHAR,CONVERT(numeric (10,2),epv1*(1+('+@margemextra+'/100)))), CONVERT(VARCHAR,CONVERT(numeric (10,2),stock)) from st (nolock)
	where inactivo = 0 and site_nr = '+@no+' and '+@filtro+' like ''%'+@descrfiltro+'%'''+ CHAR(13) + CHAR(10)

ELSE
Select @sql = '

select '''+@store_id+'''as ''store_id'',ref as ''product_id'', CONVERT(VARCHAR,CONVERT(numeric (10,2),epv1*(1+('+@margemextra+'/100)))) as ''price'', CONVERT(VARCHAR,CONVERT(numeric (10,2),stock)) as ''stock'' from st (nolock)
	where inactivo = 0 and site_nr = '+@no+' and '+@filtro+' like ''%'+@descrfiltro+'%'''
END
PRINT @sql
EXEC (@sql)

GO
Grant Execute on dbo.up_Export_prods_Glovo to Public
Grant Control on dbo.up_Export_prods_Glovo to public
GO
