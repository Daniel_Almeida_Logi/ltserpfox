
/*  lista de imagem para imprimir 
	EXEC usp_servico_infoImprimir 'ADM7C32F4D4-4DC6-484E-AC3'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[usp_servico_infoImprimir]') IS NOT NULL
	drop procedure dbo.usp_servico_infoImprimir
go

CREATE PROCEDURE [dbo].[usp_servico_infoImprimir]
	@token					varchar(25)				
AS
SET NOCOUNT ON
	
	SELECT
		token,
		docName,
		imagePath,
		printerName,
		date,
		feedcut,
		image_printPos.[order],
		ATCUD
	FROM image_printPos
	WHERE 
		token =@token 
	order by image_printPos.[order] asc 

GO
GRANT EXECUTE ON dbo.usp_servico_infoImprimir to Public
GRANT CONTROL ON dbo.usp_servico_infoImprimir to Public
GO