/****** 
	Object:  up_save_simposiumRespExemptionClass
	exec [dbo].[up_save_simposiumRespExemptionClass]  
***/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_save_simposiumRespExemptionClass]') IS NOT NULL
    DROP PROCEDURE dbo.[up_save_simposiumRespExemptionClass]
GO

CREATE PROCEDURE dbo.[up_save_simposiumRespExemptionClass]
    @stamp           VARCHAR(36),
    @token           VARCHAR(36),
    @descr           VARCHAR(max),
    @ousrinis        VARCHAR(30),
    @usrinis         VARCHAR(30)
AS
BEGIN
    SET NOCOUNT ON;

    IF EXISTS (SELECT 1 FROM dbo.[simposiumRespExemptionClass] WHERE [stamp] = @stamp)
    BEGIN
        -- Atualiza o registro existente
        UPDATE dbo.[simposiumRespExemptionClass]
        SET 
            [token] = @token,
            [descr] = @descr,
            [usrinis] = @usrinis,
            [usrdata] = GETDATE()
        WHERE 
            [stamp] = @stamp;
    END
    ELSE
    BEGIN
        -- Insere um novo registro
        INSERT INTO dbo.[simposiumRespExemptionClass] (
            [stamp], [token], [descr], [ousrinis], [ousrdata], [usrinis], [usrdata]
        )
        VALUES (
            @stamp, @token, @descr, @ousrinis, GETDATE(), @usrinis, GETDATE()
        );
    END
END

GO
Grant Execute On dbo.[up_save_simposiumRespExemptionClass] to Public
Grant Control On dbo.[up_save_simposiumRespExemptionClass] to Public
GO
