
/****** 
	Object:  get simposium Request
	exec [dbo].[up_documentos_infoCompartHeader]   'ADM3280B948-3FDD-45E7-8F4'
***/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_simposiumRequestLines]') IS NOT NULL
    drop procedure dbo.[up_get_simposiumRequestLines]
go
create procedure dbo.[up_get_simposiumRequestLines]
@token as varchar(36)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

    SELECT
		stamp,
		ref
    FROM
		simposiumRequestlines (NOLOCK) 
    WHERE
		simposiumRequestlines.token = @token     
GO
Grant Execute On dbo.[up_get_simposiumRequestLines] to Public
Grant Control On dbo.[up_get_simposiumRequestLines] to Public
GO
