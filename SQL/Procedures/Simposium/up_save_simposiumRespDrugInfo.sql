/****** 
	Object:  up_save_simposiumRespDrugInfo
	exec [dbo].[up_save_simposiumRespDrugInfo]  
***/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_save_simposiumRespDrugInfo]') IS NOT NULL
    DROP PROCEDURE dbo.[up_save_simposiumRespDrugInfo]
GO

CREATE PROCEDURE dbo.[up_save_simposiumRespDrugInfo]
    @stamp                         VARCHAR(36),
    @token                         VARCHAR(36),
    @message                       VARCHAR(254),
    @status                        INT,
    @itemId                        INT,
    @rank                          INT,
    @packageImageId                INT,
    @dciId                         INT,
    @descr                         VARCHAR(254),
    @dciName                       VARCHAR(254),
    @name                          VARCHAR(254),
    @dosage                        VARCHAR(254),
    @canTrissel                    BIT,
    @isTestSolution                BIT,
    @isSolution                    BIT,
    @isSubstance                   BIT,
    @isDrug                        BIT,
    @isGeneric                     BIT,
    @hasTherapeuticMargin          BIT,
    @needsAdditionalMonitoring     BIT,
    @type                          VARCHAR(254),
    @packages                      VARCHAR(254),
    @hasAdditionalInfo             BIT,
    @hasClientMonography           BIT,
    @hasDCIMonography              BIT,
    @monographyId                  INT,
    @monography                    VARCHAR(254),
    @tokenpackageIds               VARCHAR(36),
    @tokenAtc                      VARCHAR(36),
    @tokenCtf                      VARCHAR(36),
    @tokenRoutes                   VARCHAR(36),
    @tokenClassification           VARCHAR(36),
    @tokenExemptionClass           VARCHAR(36),
    @tokenAdditionalInfo           VARCHAR(36),
    @tokenGroups                   VARCHAR(36),
    @dosageInfoId                  INT,
    @dosageInfoRoute               VARCHAR(254),
    @dosageInfoGuideLine           VARCHAR(254),
    @dosageInfoElder               VARCHAR(254),
    @dosageInfoAdult               VARCHAR(254),
    @dosageInfoChild               VARCHAR(254),
    @dosageInfoLiverFailure        VARCHAR(254),
    @dosageInfoKidneyFailure       VARCHAR(254),
    @dosageInfoFractionalDose      VARCHAR(254),
    @dosageInfoBibliography        VARCHAR(254),
    @formDescr                     VARCHAR(max),
    @formShortName                 VARCHAR(254),
    @formShortNameCHNM             VARCHAR(254),
    @narcoticClassDescr            VARCHAR(max),
    @narcoticClassShortName        VARCHAR(254),
    @labId                         NUMERIC(10, 0),
    @labShortName                  VARCHAR(254),
    @anestheticClassId             NUMERIC(10, 0),
    @anestheticClassDescr          VARCHAR(max),
    @AgentId                       VARCHAR(254),
    @AgentName                     VARCHAR(254),
    @AgentEmail                    VARCHAR(254),
    @AgentPhone                    VARCHAR(254),
    @AgentFax                      VARCHAR(254),
    @AgentSupervisionEmail         VARCHAR(254),
    @AgentSupervisionPhone         VARCHAR(254),
    @AgentSupervisionFax           VARCHAR(254),
    @AgentWebSite                  VARCHAR(254),
	@ousrinis					   VARCHAR(30),
    @usrinis					   VARCHAR(30)
AS
BEGIN
    SET NOCOUNT ON;

    IF EXISTS (SELECT 1 FROM dbo.[simposiumRespDrugInfo] WHERE [stamp] = @stamp)
    BEGIN
        -- Update existing record
        UPDATE dbo.[simposiumRespDrugInfo]
        SET 
            [token] = @token,
            [message] = @message,
            [status] = @status,
            [itemId] = @itemId,
            [rank] = @rank,
            [packageImageId] = @packageImageId,
            [dciId] = @dciId,
            [descr] = @descr,
            [dciName] = @dciName,
            [name] = @name,
            [dosage] = @dosage,
            [canTrissel] = @canTrissel,
            [isTestSolution] = @isTestSolution,
            [isSolution] = @isSolution,
            [isSubstance] = @isSubstance,
            [isDrug] = @isDrug,
            [isGeneric] = @isGeneric,
            [hasTherapeuticMargin] = @hasTherapeuticMargin,
            [needsAdditionalMonitoring] = @needsAdditionalMonitoring,
            [type] = @type,
            [packages] = @packages,
            [hasAdditionalInfo] = @hasAdditionalInfo,
            [hasClientMonography] = @hasClientMonography,
            [hasDCIMonography] = @hasDCIMonography,
            [monographyId] = @monographyId,
            [monography] = @monography,
            [tokenpackageIds] = @tokenpackageIds,
            [tokenAtc] = @tokenAtc,
            [tokenCtf] = @tokenCtf,
            [tokenRoutes] = @tokenRoutes,
            [tokenClassification] = @tokenClassification,
            [tokenExemptionClass] = @tokenExemptionClass,
            [tokenAdditionalInfo] = @tokenAdditionalInfo,
            [tokenGroups] = @tokenGroups,
            [dosageInfoId] = @dosageInfoId,
            [dosageInfoRoute] = @dosageInfoRoute,
            [dosageInfoGuideLine] = @dosageInfoGuideLine,
            [dosageInfoElder] = @dosageInfoElder,
            [dosageInfoAdult] = @dosageInfoAdult,
            [dosageInfoChild] = @dosageInfoChild,
            [dosageInfoLiverFailure] = @dosageInfoLiverFailure,
            [dosageInfoKidneyFailure] = @dosageInfoKidneyFailure,
            [dosageInfoFractionalDose] = @dosageInfoFractionalDose,
            [dosageInfoBibliography] = @dosageInfoBibliography,
            [formDescr] = @formDescr,
            [formShortName] = @formShortName,
            [formShortNameCHNM] = @formShortNameCHNM,
            [narcoticClassDescr] = @narcoticClassDescr,
            [narcoticClassShortName] = @narcoticClassShortName,
            [labId] = @labId,
            [labShortName] = @labShortName,
            [anestheticClassId] = @anestheticClassId,
            [anestheticClassDescr] = @anestheticClassDescr,
            [AgentId] = @AgentId,
            [AgentName] = @AgentName,
            [AgentEmail] = @AgentEmail,
            [AgentPhone] = @AgentPhone,
            [AgentFax] = @AgentFax,
            [AgentSupervisionEmail] = @AgentSupervisionEmail,
            [AgentSupervisionPhone] = @AgentSupervisionPhone,
            [AgentSupervisionFax] = @AgentSupervisionFax,
            [AgentWebSite] = @AgentWebSite,
            [usrinis] = @usrinis,
            [usrdata] = GETDATE()
        WHERE 
            [stamp] = @stamp;
    END
    ELSE
    BEGIN
        -- Insert new record
        INSERT INTO dbo.[simposiumRespDrugInfo] (
            [stamp], [token], [message], [status], [itemId], [rank], [packageImageId],
            [dciId], [descr], [dciName], [name], [dosage], [canTrissel], [isTestSolution],
            [isSolution], [isSubstance], [isDrug], [isGeneric], [hasTherapeuticMargin],
            [needsAdditionalMonitoring], [type], [packages], [hasAdditionalInfo],
            [hasClientMonography], [hasDCIMonography], [monographyId], [monography],
            [tokenpackageIds], [tokenAtc], [tokenCtf], [tokenRoutes], [tokenClassification],
            [tokenExemptionClass], [tokenAdditionalInfo], [tokenGroups], [dosageInfoId],
            [dosageInfoRoute], [dosageInfoGuideLine], [dosageInfoElder], [dosageInfoAdult],
            [dosageInfoChild], [dosageInfoLiverFailure], [dosageInfoKidneyFailure],
            [dosageInfoFractionalDose], [dosageInfoBibliography], [formDescr], [formShortName],
            [formShortNameCHNM], [narcoticClassDescr], [narcoticClassShortName], [labId],
            [labShortName], [anestheticClassId], [anestheticClassDescr], [AgentId], [AgentName],
            [AgentEmail], [AgentPhone], [AgentFax], [AgentSupervisionEmail], [AgentSupervisionPhone],
            [AgentSupervisionFax], [AgentWebSite], [ousrinis], [ousrdata], [usrinis], [usrdata]
        )
        VALUES (
            @stamp, @token, @message, @status, @itemId, @rank, @packageImageId,
            @dciId, @descr, @dciName, @name, @dosage, @canTrissel, @isTestSolution,
            @isSolution, @isSubstance, @isDrug, @isGeneric, @hasTherapeuticMargin,
            @needsAdditionalMonitoring, @type, @packages, @hasAdditionalInfo,
            @hasClientMonography, @hasDCIMonography, @monographyId, @monography,
            @tokenpackageIds, @tokenAtc, @tokenCtf, @tokenRoutes, @tokenClassification,
            @tokenExemptionClass, @tokenAdditionalInfo, @tokenGroups, @dosageInfoId,
            @dosageInfoRoute, @dosageInfoGuideLine, @dosageInfoElder, @dosageInfoAdult,
            @dosageInfoChild, @dosageInfoLiverFailure, @dosageInfoKidneyFailure,
            @dosageInfoFractionalDose, @dosageInfoBibliography, @formDescr, @formShortName,
            @formShortNameCHNM, @narcoticClassDescr, @narcoticClassShortName, @labId,
            @labShortName, @anestheticClassId, @anestheticClassDescr, @AgentId, @AgentName,
            @AgentEmail, @AgentPhone, @AgentFax, @AgentSupervisionEmail, @AgentSupervisionPhone,
            @AgentSupervisionFax, @AgentWebSite, @ousrinis, GETDATE(), @usrinis, GETDATE()
        );
    END
END

GO
Grant Execute On dbo.[up_save_simposiumRespDrugInfo] to Public
Grant Control On dbo.[up_save_simposiumRespDrugInfo] to Public
GO