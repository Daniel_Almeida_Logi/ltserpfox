/****** 
	Object:  up_save_servicesAuthentication
	exec [dbo].[up_save_servicesAuthentication]  
***/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_servicesAuthentication]') IS NOT NULL
    drop procedure dbo.[up_save_servicesAuthentication]
go
create procedure dbo.[up_save_servicesAuthentication]
@acessToken		AS VARCHAR(MAX),
@refreshToken	AS VARCHAR(MAX),
@endDateToken	AS DATETIME,
@typeNr			AS INT,
@typeDesc		AS VARCHAR(254),
@site			AS VARCHAR(36)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON


	 INSERT INTO servicesAuthentication (stamp,acessToken,refreshToken,endDateToken,typeNr,typeDesc,ousrdata,site)
	 VALUES(LEFT(NEWID(),36),@acessToken,@refreshToken,@endDateToken,@typeNr,@typeDesc,GETDATE(),@site)


	

GO
Grant Execute On dbo.[up_save_servicesAuthentication] to Public
Grant Control On dbo.[up_save_servicesAuthentication] to Public
GO



