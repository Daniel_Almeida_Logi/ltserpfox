SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

-- Se a procedure j� existir, remove
IF OBJECT_ID('[dbo].[up_save_simposiumRespInteractions]') IS NOT NULL
    DROP PROCEDURE dbo.[up_save_simposiumRespInteractions]
GO

-- Cria a procedure
CREATE PROCEDURE dbo.[up_save_simposiumRespInteractions]
    @stamp             AS VARCHAR(36),
    @token             AS VARCHAR(36),
    @tokenItems        AS VARCHAR(36),
    @tokenBibliography AS VARCHAR(36),
    @severityDescr     AS VARCHAR(max),
    @severityAdvice    AS VARCHAR(max),
    @advice            AS VARCHAR(max),
    @infoDescr         AS VARCHAR(max),
    @infoAdvice        AS VARCHAR(max),
    @level             AS INT,
    @ousrinis          AS VARCHAR(30),
    @usrinis           AS VARCHAR(30)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON;

-- Verifica se o registro j� existe na tabela pelo stamp
IF EXISTS (SELECT 1 FROM dbo.simposiumRespInteractions WHERE stamp = @stamp)
BEGIN
    -- Faz o UPDATE se o registro j� existir
    UPDATE dbo.simposiumRespInteractions
    SET 
        token = @token,
        tokenItems = @tokenItems,
        tokenBibliography = @tokenBibliography,
        severityDescr = @severityDescr,
        severityAdvice = @severityAdvice,
        advice = @advice,
        infoDescr = @infoDescr,
        infoAdvice = @infoAdvice,
        level = @level,
        usrdata = GETDATE(),
        usrinis = @usrinis
    WHERE stamp = @stamp;
END
ELSE
BEGIN
    -- Faz o INSERT se o registro n�o existir
    INSERT INTO dbo.simposiumRespInteractions (
        stamp, token, tokenItems, tokenBibliography, severityDescr, severityAdvice, 
        advice, infoDescr, infoAdvice, level, ousrinis, ousrdata, usrinis, usrdata
    )
    VALUES (
        @stamp, @token, @tokenItems, @tokenBibliography, @severityDescr, @severityAdvice,
        @advice, @infoDescr, @infoAdvice, @level, @ousrinis, GETDATE(), @usrinis, GETDATE()
    );
END;

GO
Grant Execute On dbo.[up_save_simposiumRespInteractions] to Public
Grant Control On dbo.[up_save_simposiumRespInteractions] to Public
GO