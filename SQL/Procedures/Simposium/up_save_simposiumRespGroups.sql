/****** 
	Object:  up_save_simposiumRespGroups
	exec [dbo].[up_save_simposiumRespGroups]  
***/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_save_simposiumRespGroups]') IS NOT NULL
    DROP PROCEDURE dbo.[up_save_simposiumRespGroups]
GO

CREATE PROCEDURE dbo.[up_save_simposiumRespGroups]
    @stamp           VARCHAR(36),
    @token           VARCHAR(36),
    @id              NUMERIC(10,0),
    @descr           VARCHAR(MAX),
    @ousrinis        VARCHAR(30),
    @usrinis         VARCHAR(30)
AS
BEGIN
    SET NOCOUNT ON;

    IF EXISTS (SELECT 1 FROM dbo.[simposiumRespGroups] WHERE [stamp] = @stamp)
    BEGIN
        -- Atualiza o registro existente
        UPDATE dbo.[simposiumRespGroups]
        SET 
            [token] = @token,
            [id] = @id,
            [descr] = @descr,
            [usrinis] = @usrinis,
            [usrdata] = GETDATE()
        WHERE 
            [stamp] = @stamp;
    END
    ELSE
    BEGIN
        -- Insere um novo registro
        INSERT INTO dbo.[simposiumRespGroups] (
            [stamp], [token], [id], [descr], [ousrinis], [ousrdata], [usrinis], [usrdata]
        )
        VALUES (
            @stamp, @token, @id, @descr, @ousrinis, GETDATE(), @usrinis, GETDATE()
        );
    END
END

GO
Grant Execute On dbo.[up_save_simposiumRespGroups] to Public
Grant Control On dbo.[up_save_simposiumRespGroups] to Public
GO
