SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

-- Se a procedure j� existir, remove
IF OBJECT_ID('[dbo].[up_save_simposiumRespFoodInteraction]') IS NOT NULL
    DROP PROCEDURE dbo.[up_save_simposiumRespFoodInteraction]
GO

-- Cria a procedure
CREATE PROCEDURE dbo.[up_save_simposiumRespFoodInteraction]
    @stamp        AS VARCHAR(36),
    @token        AS VARCHAR(36),
    @id           AS INT,
    @descr        AS VARCHAR(MAX),
    @explanation  AS VARCHAR(MAX),
    @severity     AS VARCHAR(254),
    @foodType     AS VARCHAR(254),
    @level        AS INT,
    @ousrinis     AS VARCHAR(30),
    @usrinis      AS VARCHAR(30)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON;

-- Verifica se o registro j� existe na tabela pelo stamp
IF EXISTS (SELECT 1 FROM dbo.simposiumRespFoodInteraction WHERE stamp = @stamp)
BEGIN
    -- Faz o UPDATE se o registro j� existir (sem atualizar o campo ousrinis)
    UPDATE dbo.simposiumRespFoodInteraction
    SET 
        token = @token,
        id = @id,
        descr = @descr,
        explanation = @explanation,
        severity = @severity,
        foodType = @foodType,
        level = @level,
        usrdata = GETDATE(),
        usrinis = @usrinis
    WHERE stamp = @stamp;
END
ELSE
BEGIN
    -- Faz o INSERT se o registro n�o existir
    INSERT INTO dbo.simposiumRespFoodInteraction (
        stamp, token, id, descr, explanation, severity, foodType, level, ousrinis, ousrdata, usrinis, usrdata
    )
    VALUES (
        @stamp, @token, @id, @descr, @explanation, @severity, @foodType, @level, @ousrinis, GETDATE(), @usrinis, GETDATE()
    );
END;

GO
Grant Execute On dbo.[up_save_simposiumRespFoodInteraction] to Public
Grant Control On dbo.[up_save_simposiumRespFoodInteraction] to Public
GO

