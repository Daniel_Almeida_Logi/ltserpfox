/****** 
	Object:  up_save_simposiumRespAtc
	exec [dbo].[up_save_simposiumRespAtc]  
***/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_save_simposiumRespAtc]') IS NOT NULL
    DROP PROCEDURE dbo.[up_save_simposiumRespAtc]
GO

CREATE PROCEDURE dbo.[up_save_simposiumRespAtc]
    @stamp         VARCHAR(36),
    @token         VARCHAR(36),
    @code          VARCHAR(254),
    @descr         VARCHAR(max),
    @ousrinis      VARCHAR(30),
    @usrinis       VARCHAR(30)
AS
BEGIN
    SET NOCOUNT ON;

    IF EXISTS (SELECT 1 FROM dbo.[simposiumRespAtc] WHERE [stamp] = @stamp)
    BEGIN
        -- Atualiza o registro existente
        UPDATE dbo.[simposiumRespAtc]
        SET 
            [token] = @token,
            [code] = @code,
            [descr] = @descr,
            [usrinis] = @usrinis,
            [usrdata] = GETDATE()
        WHERE 
            [stamp] = @stamp;
    END
    ELSE
    BEGIN
        -- Insere um novo registro
        INSERT INTO dbo.[simposiumRespAtc] (
            [stamp], [token], [code], [descr], [ousrinis], [ousrdata], [usrinis], [usrdata]
        )
        VALUES (
            @stamp, @token, @code, @descr, @ousrinis, GETDATE(), @usrinis, GETDATE()
        );
    END
END

GO
Grant Execute On dbo.[up_save_simposiumRespAtc] to Public
Grant Control On dbo.[up_save_simposiumRespAtc] to Public
GO