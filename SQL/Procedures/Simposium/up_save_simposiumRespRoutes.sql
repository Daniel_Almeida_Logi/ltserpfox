/****** 
	Object:  up_save_simposiumRespRoutes
	exec [dbo].[up_save_simposiumRespRoutes]  
***/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_save_simposiumRespRoutes]') IS NOT NULL
    DROP PROCEDURE dbo.[up_save_simposiumRespRoutes]
GO

CREATE PROCEDURE dbo.[up_save_simposiumRespRoutes]
    @stamp         VARCHAR(36),
    @token         VARCHAR(36),
    @shortName     VARCHAR(254),
    @descr         VARCHAR(max),
    @ousrinis      VARCHAR(30),
    @usrinis       VARCHAR(30)
AS
BEGIN
    SET NOCOUNT ON;

    IF EXISTS (SELECT 1 FROM dbo.[simposiumRespRoutes] WHERE [stamp] = @stamp)
    BEGIN
        -- Atualiza o registro existente
        UPDATE dbo.[simposiumRespRoutes]
        SET 
            [token] = @token,
            [shortName] = @shortName,
            [descr] = @descr,
            [usrinis] = @usrinis,
            [usrdata] = GETDATE()
        WHERE 
            [stamp] = @stamp;
    END
    ELSE
    BEGIN
        -- Insere um novo registro
        INSERT INTO dbo.[simposiumRespRoutes] (
            [stamp], [token], [shortName], [descr], [ousrinis], [ousrdata], [usrinis], [usrdata]
        )
        VALUES (
            @stamp, @token, @shortName, @descr, @ousrinis, GETDATE(), @usrinis, GETDATE()
        );
    END
END

GO
Grant Execute On dbo.[up_save_simposiumRespRoutes] to Public
Grant Control On dbo.[up_save_simposiumRespRoutes] to Public
GO
