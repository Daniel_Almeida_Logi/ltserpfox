/****** 
	Object:  up_save_simposiumRespPackageIds
	exec [dbo].[up_save_simposiumRespPackageIds]  
***/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_save_simposiumRespPackageIds]') IS NOT NULL
    DROP PROCEDURE dbo.[up_save_simposiumRespPackageIds]
GO

CREATE PROCEDURE dbo.[up_save_simposiumRespPackageIds]
    @stamp         VARCHAR(36),
    @token         VARCHAR(36),
    @id            NUMERIC(10,0),
    @ousrinis      VARCHAR(30),
    @usrinis       VARCHAR(30)
AS
BEGIN
    SET NOCOUNT ON;

    IF EXISTS (SELECT 1 FROM dbo.[simposiumRespPackageIds] WHERE [stamp] = @stamp)
    BEGIN
        -- Atualiza o registro existente
        UPDATE dbo.[simposiumRespPackageIds]
        SET 
            [token] = @token,
            [id] = @id,
            [usrinis] = @usrinis,
            [usrdata] = GETDATE()
        WHERE 
            [stamp] = @stamp;
    END
    ELSE
    BEGIN
        -- Insere um novo registro
        INSERT INTO dbo.[simposiumRespPackageIds] (
            [stamp], [token], [id], [ousrinis], [ousrdata], [usrinis], [usrdata]
        )
        VALUES (
            @stamp, @token, @id, @ousrinis, GETDATE(), @usrinis, GETDATE()
        );
    END
END

GO
Grant Execute On dbo.[up_save_simposiumRespPackageIds] to Public
Grant Control On dbo.[up_save_simposiumRespPackageIds] to Public
GO