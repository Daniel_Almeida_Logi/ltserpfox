SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

-- Se a procedure j� existir, remove
IF OBJECT_ID('[dbo].[up_save_simposiumRespBibliography]') IS NOT NULL
    DROP PROCEDURE dbo.[up_save_simposiumRespBibliography]
GO

-- Cria a procedure
CREATE PROCEDURE dbo.[up_save_simposiumRespBibliography]
    @stamp        AS VARCHAR(36),
    @token        AS VARCHAR(36),
    @title        AS VARCHAR(254),
    @author       AS VARCHAR(254),
    @accessDate   AS DATETIME,
    @type         AS VARCHAR(254),
    @language     AS VARCHAR(254),
    @url          AS VARCHAR(254),
    @edition      AS VARCHAR(254),
    @publisher    AS VARCHAR(254),
    @volume       AS VARCHAR(254),
    @book         AS VARCHAR(254),
    @journal      AS VARCHAR(254),
    @number       AS VARCHAR(254),
    @institution  AS VARCHAR(254),
    @ousrinis     AS VARCHAR(30),
    @usrinis      AS VARCHAR(30)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON;

-- Verifica se o registro j� existe na tabela pelo stamp
IF EXISTS (SELECT 1 FROM dbo.simposiumRespBibliography WHERE stamp = @stamp)
BEGIN
    -- Faz o UPDATE se o registro j� existir
    UPDATE dbo.simposiumRespBibliography
    SET 
        token = @token,
        title = @title,
        author = @author,
        accessDate = @accessDate,
        type = @type,
        language = @language,
        url = @url,
        edition = @edition,
        publisher = @publisher,
        volume = @volume,
        book = @book,
        journal = @journal,
        number = @number,
        institution = @institution,
        usrdata = GETDATE(),
        usrinis = @usrinis
    WHERE stamp = @stamp;
END
ELSE
BEGIN
    -- Faz o INSERT se o registro n�o existir
    INSERT INTO dbo.simposiumRespBibliography (
        stamp, token, title, author, accessDate, type, language, url, edition, publisher, 
        volume, book, journal, number, institution, ousrinis, ousrdata, usrinis, usrdata
    )
    VALUES (
        @stamp, @token, @title, @author, @accessDate, @type, @language, @url, @edition, @publisher,
        @volume, @book, @journal, @number, @institution, @ousrinis, GETDATE(), @usrinis, GETDATE()
    );
END;

GO
Grant Execute On dbo.[up_save_simposiumRespBibliography] to Public
Grant Control On dbo.[up_save_simposiumRespBibliography] to Public
GO