/* Devolve VEndas Pim tiMedi



	exec up_get_sales_lines_pim 'ADM4FC84E6A-A991-4F79-977','Loja 1'

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_sales_lines_pim]') IS NOT NULL
	drop procedure dbo.up_get_sales_lines_pim
go

create procedure dbo.up_get_sales_lines_pim
	@stamp					VARCHAR(255) = '',
	@site					VARCHAR(20)
/* WITH ENCRYPTION */

AS

	DECLARE @sql	VARCHAR(8000) = ''

	set @sql = @sql + N'
	SELECT 
		fi.lordem																			  AS SalesLinesInfo_sellLineId,
		dbo.alltrimIsNull(fi.ref)                                                             AS SalesLinesInfo_ref, 
		dbo.alltrimIsNull(fi.design)                                                          AS SalesLinesInfo_descProduct ,
		CASE WHEN fi.tipodoc = 3 THEN convert(int,ISNULL(fi.qtt,0)*-1 )
						   ELSE convert(int,ISNULL(fi.qtt,0)) END							  AS SalesLinesInfo_qttProduct, '

		IF (select bool FROM B_Parameters_site WHERE stamp='ADM0000000219' AND site=@site) = 0
		BEGIN
			set @sql = @sql + N'	
 			-999999				                                                              AS SalesLinesInfo_pvpProduct'
		END
		ELSE
		BEGIN
			set @sql = @sql + N'	
 			fi.etiliquido				                                                      AS SalesLinesInfo_pvpProduct'
		END
		set @sql = @sql + N'			
		FROM 
			ft (NOLOCK) 
		INNER JOIN 
			ft2 (NOLOCK) ON ft.ftstamp = ft2.ft2stamp
		INNER JOIN 
			td (NOLOCK) ON td.ndoc = ft.ndoc
		INNER JOIN 
			fi (NOLOCK) ON ft.ftstamp = fi.ftstamp
		INNER JOIN
			b_utentes ut (NOLOCK) ON ft.no = ut.no AND ft.estab = ut.estab
		INNER JOIN 
			empresa emp (NOLOCK) ON ft.site = emp.site
		LEFT JOIN
			st (NOLOCK) ON fi.ref = st.ref AND emp.no = st.site_nr
		LEFT JOIN 
			b_us (NOLOCK) ON ft.vendedor = b_us.userno
		LEFT JOIN
			fprod (NOLOCK) ON fprod.ref = st.ref
		WHERE
			 fi.ftstamp = ''' + @stamp +''' 
			 AND td.tipodoc IN (1,3) 
			 AND fi.ref!=''''
			 AND fi.stns = 0
			 AND fi.qtt>0
			 AND ft.no>= 200
			 AND st.pim = 1
			 AND ut.pim = 1
			 AND ft.cobrado = 0	
		ORDER BY
			 ft.fdata		ASC
			,ft.ousrhora	ASC
			,ft.fno			ASC
			,fi.ref			ASC'
	
	print(@sql)
	exec(@sql)


GO
Grant Execute On dbo.up_get_sales_lines_pim to Public
Grant Control On dbo.up_get_sales_lines_pim to Public
Go


