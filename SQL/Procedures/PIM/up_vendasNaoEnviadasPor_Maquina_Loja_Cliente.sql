

if OBJECT_ID('[dbo].[up_vendasNaoEnviadasPor_Maquina_Loja_Cliente]') IS NOT NULL
	drop procedure dbo.up_vendasNaoEnviadasPor_Maquina_Loja_Cliente
go
 
/*
	exec up_vendasNaoEnviadasPor_Maquina_Loja_Cliente
	'FARMÁCIA', 'Loja 1', '', 'ADMB06BCF06-D0A7-465F-BAE', '', '', '', '', 'Logitools_250120231747_ADMB06BCF06-D0A7-465F-BAE'
*/

Create PROCEDURE [dbo].[up_vendasNaoEnviadasPor_Maquina_Loja_Cliente]
	@tipoClient as varchar(200)
	,@loja as varchar(200)
	,@grupoStamp as varchar(200) -- se preenchido, é um reenvio 
	,@token as varchar(200) --é o token a preencher o grupoStamp no insert dos logs de venda
	,@no as varchar(20)
	,@estab as varchar(20)
	,@ftstamp as varchar(36)
	,@atendimento as varchar(100)
	,@nomeFicheiro as varchar(100)
AS  
BEGIN 
declare 
	@condicoes as varchar(4000)
begin try 
begin tran up_vendasNaoEnviadas
	--se tiver sido exportado, vai-se buscar os dados á tabela de logs da exportação
	if len(Ltrim(RTRIM(@grupoStamp)))> 0 and @grupoStamp is not null
	begin  	 
		print '@grupo stamp not null'
		select emp.nomecomp as HOSPITAL_NAME, 
			codfarm as HOSPITAL_ID, 
			ut.no as PATIENT_ID, 
			(select first_name from dbo.uf_gerais_devolve_primeiro_e_ultimo_nome(ut.nome))  as PATIENTE_1ST_NAME, 
			(select last_name from dbo.uf_gerais_devolve_primeiro_e_ultimo_nome(ut.nome))  as PATIENT_LAST_NAME, 
			dbo.alltrimIsNull(STR(ft.ndoc)) + '-' + dbo.alltrimIsNull(STR(ft.fno)) as INVOICE_NUMBER, 
			ft.ousrdata as INVOICE_DATE, 
			fi.ref as PRODUCT_CODE,
			fi.qtt as NUMBER_OF_BOXES_INVOICE,
			'' as LINE_NUMBER,
			'' as EMPTY_FIELD_2, 
			'' as BATCH_NUMBER, 
			'' as SOCIAL_SECURITY_NUMBER, 
			'' as ADDRESS, 
			'' as EMPTY_FIELD_1, 
			'' as PATIENT_POSTAL_CODE,
			'' as PATIENT_CITY, 
			'' as ROOM_NUMBER,
			'' as PHARMACY_NAME, 
			'' as PHARMACY_ID, 
			'' as SALES_IS, 
			'' as SOFTWARE_NAME,
			'' as DOCTOR_NAME, 
			'' as DOCTOR_ID ,
			'' as SALE_DATE,
			'' as SALE_NUMBER 
		from  
			logVendasExportadas (nolock) 
		inner join
			ft (nolock) on logVendasExportadas.ftstamp = ft.ftstamp
		inner join 
			ft2 (nolock) on ft.ftstamp = ft2.ft2stamp and logVendasExportadas.ftstamp = ft2.ft2stamp
		inner join 
			fi (nolock) on ft.ftstamp = fi.ftstamp and logVendasExportadas.fistamp = fi.fistamp
		inner join 
			b_utentes ut (nolock) on ft.no = ut.no and ft.estab = ut.estab
		inner join 
			empresa emp (nolock) on ft.site = emp.site 
		where 
			grupoStamp = @grupoStamp 
	end
	else
	begin
		print '@grupo stamp  null'   	
		set @condicoes = 'ft2.exportado = 0 and fi.qtt > 0 and ut.tipo like ''' + @tipoClient + ''' and emp.site = ''' + @loja +''' ' 
		if len(dbo.alltrimIsNull(@no))> 0 set @condicoes = @condicoes + 'and ut.no = ' + convert(varchar,@no) + ' '
		if len(dbo.alltrimIsNull(@estab))> 0 set @condicoes = @condicoes + 'and ut.estab = ' + convert(varchar,@estab) + ' '
		if len(dbo.alltrimIsNull(@ftstamp))> 0 set @condicoes = @condicoes + 'and ft.ftstamp = ''' + @ftstamp + ''' '
		if len(dbo.alltrimIsNull(@atendimento))> 0 set @condicoes = @condicoes + 'and ft.u_nratend = ''' + @atendimento + ''' '
		print 'insert'
		declare @insert varchar(4000)
		print 'insert 2'
		set @insert = 'insert into logVendasExportadas (grupoStamp, ftstamp, fistamp, site, no, estab, filename, ousrdata)
						select ''' + @token + ''', ft.ftstamp, fi.fistamp,emp.site,ut.no,ut.estab, '''+@nomeFicheiro+''',
						CONVERT(varchar, GETDATE(), 120)
						from ft (nolock) inner join ft2 (nolock) on ft.ftstamp = ft2.ft2stamp 
						inner join 	fi (nolock) on ft.ftstamp = fi.ftstamp 
						inner join 	b_utentes ut (nolock) on ft.no = ut.no and ft.estab = ut.estab
						inner join 	empresa emp (nolock) on ft.site = emp.site
						WHERE ' + @condicoes
		print 'insert = ' + @insert
		print 'select'
		declare @select varchar(4000)
		print 'select 2'
		set @select = ' select emp.nomecomp as HOSPITAL_NAME,codfarm as HOSPITAL_ID,ut.no as PATIENT_ID, 		
			(select first_name from dbo.uf_gerais_devolve_primeiro_e_ultimo_nome(ut.nome)) as PATIENTE_1ST_NAME, 			
			(select last_name from dbo.uf_gerais_devolve_primeiro_e_ultimo_nome(ut.nome)) as PATIENT_LAST_NAME, 
			dbo.alltrimIsNull(STR(ft.ndoc)) +''-''+ dbo.alltrimIsNull(STR(ft.fno)) as INVOICE_NUMBER, 
			ft.ousrdata as INVOICE_DATE, fi.ref as PRODUCT_CODE,
			convert(int,fi.qtt) as NUMBER_OF_BOXES_INVOICE,
			'''' as LINE_NUMBER,
			'''' as EMPTY_FIELD, 
			'''' as BATCH_NUMBER,
			'''' as SOCIAL_SECURITY_NUMBER, 
			'''' as ADDRESS, 
			'''' as PATIENT_POSTAL_CODE, 
			'''' as PATIENT_CITY,
			'''' as ROOM_NUMBER,
			'''' as PHARMACY_NAME,
			'''' as PHARMACY_ID, 
			'''' as SALES_IS, 
			'''' as SOFTWARE_NAME,
			'''' as DOCTOR_NAME, 
			'''' as DOCTOR_ID ,
			'''' as SALE_DATE, 
			'''' as SALE_NUMBER 
		from 
			ft (nolock) 
		inner join 
			ft2 (nolock) on ft.ftstamp = ft2.ft2stamp
		inner join 
			fi (nolock) on ft.ftstamp = fi.ftstamp 
		inner join
			b_utentes ut (nolock) on ft.no = ut.no and ft.estab = ut.estab
		inner join 
			empresa emp (nolock) on ft.site = emp.site
		WHERE ' + @condicoes 
		print 'select = ' + @select
		exec(@insert + ' ' + @select)
	end
commit tran up_vendasNaoEnviadas
end try
begin catch
	rollback tran up_vendasNaoEnviadas
	print error_number()
	print error_message()
	print error_line()
	select error_number(), error_message(), error_line()
end catch
END
