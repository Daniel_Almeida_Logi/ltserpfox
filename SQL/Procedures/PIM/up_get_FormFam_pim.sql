/* Devolve VEndas Pim tiMedi



	exec up_get_FormFam_pim '20230131','20230331', 'Loja 1', 1000, 1

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_FormFam_pim]') IS NOT NULL
	drop procedure dbo.up_get_FormFam_pim
go

create procedure dbo.up_get_FormFam_pim
	@startDate					DATETIME,
	@endDate					DATETIME,
	@site						VARCHAR(20),
	@topMax						INT = 1000,
	@pageNumber					INT = 1,
	@search						VARCHAR(255) = ''
/* WITH ENCRYPTION */

AS
	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dosagem'))
		DROP TABLE #dosagem
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dosagemRow'))
		DROP TABLE #dosagemRow


	DECLARE @sql	VARCHAR(8000) = ''
	DECLARE @site_nr INT

	IF(ISNULL(@pageNumber,0)<1)
	BEGIN
		SET @pageNumber = 1
	END

	SET @search = ISNULL(@search, '')
	SET @site_nr = isnull((Select top 1 no from empresa (nolock) where empresa.site = @site),0)

	set @sql = @sql + N'
	SELECT DISTINCT 
		dbo.alltrimIsNull(fprod.fformasdescr)												  AS ProductInfo_formFarma,
		CASE WHEN ISNUMERIC(Form_Farm_ID)=1 
			THEN convert(int,dbo.alltrimIsNull(fprod.Form_Farm_ID))	 
			ELSE 0 END																		  AS ProductInfo_idDosageForm
		INTO
			#dosagem
		FROM 
			fprod (NOLOCK) 
		WHERE
			fprod.Form_Farm_ID != 0'
		
		IF @search !=''
		BEGIN
			set @sql = @sql + N'
				AND fprod.fformasdescr like ''%' + @search +'%'' '
		END	
					
		set @sql = @sql + N' ORDER BY
			ProductInfo_idDosageForm		ASC'
		
		set @sql = @sql + N'
		SELECT DISTINCT 
				*,
				rowNumber = ROW_NUMBER() OVER(PARTITION BY ProductInfo_idDosageForm ORDER BY
							 ProductInfo_idDosageForm ASC) 
			INTO
				#dosagemRow
			FROM 
				#dosagem (NOLOCK) '

		set @sql = @sql + N' 
		SELECT '+
		convert(varchar(10),@topMax) +'														  AS pageSize,
		(COUNT(*) OVER() / ' +  convert(varchar(10),@topMax) +') + 1						  AS pageTotal, '
		+ convert(varchar(10),@PageNumber) +'												  AS pageNumber,
		COUNT(*) OVER()																		  AS linesTotal,
		' + convert(varchar(10),@topMax) +'													  AS topMax,
		ProductInfo_formFarma,
		ProductInfo_idDosageForm
		FROM
			#dosagemRow
		WHERE
			rowNumber = 1
		ORDER BY
			ProductInfo_idDosageForm	ASC
			OFFSET ' + convert(varchar(10),@topMax) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
			FETCH NEXT  ' + convert(varchar(10),@topMax) + ' ROWS ONLY'

	print(@sql)
	exec(@sql)

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dosagem'))
		DROP TABLE #dosagem
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dosagemRow'))
		DROP TABLE #dosagemRow
GO
Grant Execute On dbo.up_get_FormFam_pim to Public
Grant Control On dbo.up_get_FormFam_pim to Public
Go


