/* Devolve VEndas Pim tiMedi

	exec up_get_clients_pim '20230131','20230331', 50, 1, ''

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_clients_pim]') IS NOT NULL
	drop procedure dbo.up_get_clients_pim
go

create procedure dbo.up_get_clients_pim
	@startDate					DATETIME,
	@endDate					DATETIME,
	@topMax						INT = 1000,
	@pageNumber					INT = 1,
	@search						VARCHAR(max) = ''
/* WITH ENCRYPTION */

AS

	DECLARE @sql	VARCHAR(8000) = ''


	IF(ISNULL(@pageNumber,0)<1)
	BEGIN
		SET @pageNumber = 1
	END

	SET @topMax =   isnull(@topMax,100)
	IF(@topMax<=0)
		SET @topMax = 100
	
	SET @search = ISNULL(@search, '')	

	set @sql = @sql + N'
	SELECT ' + convert(varchar(10),@topMax) +'												  AS pageSize,
		(COUNT(*) OVER() / ' +  convert(varchar(10),@topMax) +') + 1						  AS pageTotal, '
		+ convert(varchar(10),@PageNumber) +'												  AS pageNumber,
		COUNT(*) OVER()																		  AS linesTotal,
		' + convert(varchar(10),@topMax) +'													  AS topMax,
		 
		ut.no																				  AS ClientInfo_id,
		ut.estab																			  AS ClientInfo_dep,
		ISNULL((select FIRST_NAME from uf_gerais_devolve_primeiro_e_ultimo_nome (ut.nome))
		,ut.nome)																			  AS ClientInfo_name, 
		ISNULL((select LAST_NAME from uf_gerais_devolve_primeiro_e_ultimo_nome (ut.nome))
		,'''')																				  AS ClientInfo_surname
		 					
		FROM 	
			b_utentes AS ut
		WHERE
			 ut.inactivo = 0
			 AND ut.pim = 1	
			 AND ut.no > 200'
	IF @search !=''
	BEGIN
		set @sql = @sql + N'
			AND ut.nome like ''%' + @search +'%'' '
	END	
	set @sql = @sql + N'
		ORDER BY
			 ut.no		ASC,
			 ut.estab	ASC
			 --,ut.usrhora	ASC
			 --,ut.no			ASC
			OFFSET ' + convert(varchar(10),@topMax) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
			FETCH NEXT  ' + convert(varchar(10),@topMax) + ' ROWS ONLY '
	
	print(@sql)
	exec(@sql)


GO
Grant Execute On dbo.up_get_clients_pim to Public
Grant Control On dbo.up_get_clients_pim to Public
Go


