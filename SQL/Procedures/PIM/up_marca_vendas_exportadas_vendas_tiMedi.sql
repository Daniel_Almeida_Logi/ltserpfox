/****** Object:  StoredProcedure [dbo].[up_marca_vendas_exportadas_vendas_tiMedi]    Script Date: 08/02/2024 12:25:00 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marca_vendas_exportadas_vendas_tiMedi]') IS NOT NULL
	drop procedure dbo.up_marca_vendas_exportadas_vendas_tiMedi
go

create procedure [dbo].[up_marca_vendas_exportadas_vendas_tiMedi]
	@dataIni	DATETIME,
	@dataFim	DATETIME,
	@site		VARCHAR(20),
	@tipoCli	VARCHAR(256),
	@incExport  bit = 0,
	@user		VARCHAR(20)
/* WITH ENCRYPTION */

AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosTipoCliente'))
		DROP TABLE #dadosTipoCliente	

	create  table #dadosTipoCliente (
		[tipo] varchar(20)
	)

	IF(@tipoCli='')
	BEGIN

		INSERT INTO #dadosTipoCliente (tipo)
		SELECT distinct tipo FROM b_utentes
	END 
	ELSE
	BEGIN
		INSERT INTO #dadosTipoCliente (tipo)
		SELECT distinct tipo FROM b_utentes Where b_utentes.tipo in (Select items as tipo from dbo.up_splitToTable(@tipoCli,','))
	END 


		update 
			ft2
		set 
			exportado = 1,
			exportDate = getdate(),
			exportUser = @user
		from 
			ft2 (nolock) 
		inner join 
			ft (nolock) on ft.ftstamp = ft2.ft2stamp
		inner join 
			td (nolock) on td.ndoc = ft.ndoc
		inner join 
			fi (nolock) on ft.ftstamp = fi.ftstamp  
		inner join
			b_utentes ut (nolock) on ft.no = ut.no and ft.estab = ut.estab
		inner join 
			empresa emp (nolock) on ft.site = emp.site
		WHERE
			 ft.fdata>= @dataIni and  ft.fdata<=@dataFim
			 AND ft.site = case when @site = '' then ft.site else @site end
		     AND ft2.exportado = case when @incExport = 1 then ft2.exportado else 0 end 
		     AND ut.tipo  COLLATE DATABASE_DEFAULT IN (select tipo from #dadosTipoCliente )
			 AND (FT.tipodoc != 4 or u_tipodoc = 4)
			 AND td.U_TIPODOC not in (1, 5)
			 and fi.ref!=''
			 and fi.stns = 0
			 and fi.qtt>0
			 AND ft.no> 200
			


	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosTipoCliente'))
		DROP TABLE #dadosTipoCliente	

GO
Grant Execute on dbo.up_marca_vendas_exportadas_vendas_tiMedi to Public
Grant Control on dbo.up_marca_vendas_exportadas_vendas_tiMedi to Public
Go