/* Devolve VEndas Pim tiMedi



	exec up_relatorio_vendas_pim '20230131','20230331', 'Loja 1', 50, 1

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_vendas_pim]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_pim
go

create procedure dbo.up_relatorio_vendas_pim
	@startDate					DATETIME,
	@endDate					DATETIME,
	@site						VARCHAR(20),
	@topMax						INT = 1000,
	@pageNumber					INT = 1,
	@search						VARCHAR(255) = ''
/* WITH ENCRYPTION */

AS

	DECLARE @sql	VARCHAR(8000) = ''

	IF(ISNULL(@pageNumber,0)<1)
	BEGIN
		SET @pageNumber = 1
	END

	SET @search = ISNULL(@search, '')

	set @sql = @sql + N'
	SELECT ' + convert(varchar(10),@topMax) +'												  AS pageSize,
		(COUNT(*) OVER() / ' +  convert(varchar(10),@topMax) +') + 1						  AS pageTotal, '
		+ convert(varchar(10),@PageNumber) +'												  AS pageNumber,
		COUNT(*) OVER()																		  AS linesTotal,
		' + convert(varchar(10),@topMax) +'													  AS topMax,

		dbo.alltrimIsNull(st.ref)															  AS ProductInfo_ref,
		dbo.alltrimIsNull(st.design)														  AS ProductInfo_name,
		dbo.alltrimIsNull(fprod.descricao)													  AS ProductInfo_description, 		
	--	st.epv1																				  AS ProductInfo_pvp, 			
	--	st.stock																			  AS ProductInfo_stock, 
	--	dbo.alltrimIsNull(fprod.fprodstamp)													  AS ProductInfo_idForm,
		ISNULL(fprod.num_unidades,0)														  AS ProductInfo_qttMin,  
		st.usaid																			  AS ProductInfo_lastSale, 
		st.UINTR																			  AS ProductInfo_lastBuy,
		dbo.alltrimIsNull(fprod.fformasdescr)												  AS ProductInfo_formFarma,
		dbo.alltrimIsNull(fprod.Form_Farm_ID)												  AS ProductInfo_idDosageForm,

		ISNULL(ft.vendedor,0)                                                                 AS SellerInfo_number, 
		dbo.alltrimIsNull(ft.vendnm)                                                          AS SellerInfo_name,
	--	null                                                                                  AS SellerInfo_surname,
		 
		dbo.alltrimIsNull(str(ft.no)) + ''.'' + dbo.alltrimIsNull(str(ft.estab))              AS ClientInfo_id,
	--	dbo.alltrimIsNull(ft.nome)															  AS ClientInfo_name, 
	--	dbo.alltrimIsNull(ft.nome2)                                                           AS ClientInfo_surname,
		 
		dbo.alltrimIsNull(ft.ftstamp)                                                         AS SalesInfo_id,
		dbo.alltrimIsNull(str(ft.no)) + ''.'' + dbo.alltrimIsNull(str(ft.estab))              AS SalesInfo_clientId,
		ISNULL(b_us.userno,0)                                                                 AS SalesInfo_sellerNumber,
		CONVERT(VARCHAR(10),fi.ousrdata,120)                                                  AS SalesInfo_date, 
		dbo.alltrimIsNull(ft.ftstamp)                                                         AS SalesInfo_sellId, 
		dbo.alltrimIsNull(fi.fistamp)                                                         AS SalesInfo_sellLineId,
		dbo.alltrimIsNull(fi.ref)                                                             AS SalesInfo_ref, 
		dbo.alltrimIsNull(fi.design)                                                          AS SalesInfo_descProduct ,
		CASE WHEN fi.tipodoc = 3 THEN convert(int,ISNULL(fi.qtt,0)*-1 )
						   ELSE convert(int,ISNULL(fi.qtt,0)) END							  AS SalesInfo_qttProduct 
 --		fi.etiliquido				                                                          AS SalesInfo_pvpProduct
					
		FROM 
			ft (NOLOCK) 
		INNER JOIN 
			ft2 (NOLOCK) ON ft.ftstamp = ft2.ft2stamp
		INNER JOIN 
			td (NOLOCK) ON td.ndoc = ft.ndoc
		INNER JOIN 
			fi (NOLOCK) ON ft.ftstamp = fi.ftstamp
		INNER JOIN
			b_utentes ut (NOLOCK) ON ft.no = ut.no AND ft.estab = ut.estab
		INNER JOIN 
			empresa emp (NOLOCK) ON ft.site = emp.site
		LEFT JOIN
			st (NOLOCK) ON fi.ref = st.ref AND emp.no = st.site_nr
		LEFT JOIN 
			b_us (NOLOCK) ON ft.vendedor = b_us.userno
		LEFT JOIN
			fprod (NOLOCK) ON fprod.ref = st.ref
		WHERE
			 ft.fdata  BETWEEN ''' + CONVERT(VARCHAR(10),@startDate, 120) + ''' AND '''+ CONVERT(VARCHAR(10),@endDate, 120) + '''
			 AND ft.site = '''+ @site+''' 
			 AND td.tipodoc IN (1,3) 
			 AND fi.ref!=''''
			 AND fi.stns = 0
			 AND fi.qtt>0
			 AND ft.no> 200
			 AND st.pim = 1
			 AND ut.pim = 1			
		ORDER BY
			 ft.fdata		ASC
			,ft.ousrhora	ASC
			,ft.fno			ASC
			,fi.ref			ASC
			OFFSET ' + convert(varchar(10),@topMax) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
			 FETCH NEXT  ' + convert(varchar(10),@topMax) + ' ROWS ONLY'
	
	print(@sql)
	exec(@sql)


GO
Grant Execute On dbo.up_relatorio_vendas_pim to Public
Grant Control On dbo.up_relatorio_vendas_pim to Public
Go


