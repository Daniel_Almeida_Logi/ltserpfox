/* Devolve VEndas Pim tiMedi



	exec up_get_sales_pim '20231001','20231031', 'Loja 1', 1000, 1

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_sales_pim]') IS NOT NULL
	drop procedure dbo.up_get_sales_pim
go

create procedure dbo.up_get_sales_pim
	@startDate					DATETIME,
	@endDate					DATETIME,
	@site						VARCHAR(20),
	@topMax						INT = 1000,
	@pageNumber					INT = 1,
	@search						VARCHAR(255) = ''
/* WITH ENCRYPTION */

AS

	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#sales'))
		DROP TABLE #sales
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#salesRow'))
		DROP TABLE  #salesRow

	DECLARE @sql	VARCHAR(8000) = ''

	IF(ISNULL(@pageNumber,0)<1)
	BEGIN
		SET @pageNumber = 1
	END

	SET @search = ISNULL(@search, '')

	set @sql = @sql + N'
	SELECT
		ft.ftstamp																			  AS SalesInfo_stamp,
		ft.ftano																			  AS SalesInfo_year,
		ft.ndoc																				  AS SalesInfo_docTypeNumber,
		ft.fno																				  AS SalesInfo_number,
		dbo.alltrimIsNull(ft.nmdoc)															  AS SalesInfo_docTypeDesc,
		ft.no																				  AS SalesInfo_clientId,
		ft.estab																			  AS SalesInfo_clientDep,
		ISNULL(b_us.userno,0)                                                                 AS SalesInfo_sellerNumber,
		CONVERT(VARCHAR(10),ft.fdata,120)                                                     AS SalesInfo_date,
		ISNULL(ft.pnome,'''')																  AS SalesInfo_terminal
		INTO
			#sales			
		FROM 
			ft (NOLOCK) 
		INNER JOIN 
			ft2 (NOLOCK) ON ft.ftstamp = ft2.ft2stamp
		INNER JOIN 
			td (NOLOCK) ON td.ndoc = ft.ndoc
		INNER JOIN 
			fi (NOLOCK) ON ft.ftstamp = fi.ftstamp
		INNER JOIN
			b_utentes ut (NOLOCK) ON ft.no = ut.no AND ft.estab = ut.estab
		INNER JOIN 
			empresa emp (NOLOCK) ON ft.site = emp.site
		LEFT JOIN
			st (NOLOCK) ON fi.ref = st.ref AND emp.no = st.site_nr
		LEFT JOIN 
			b_us (NOLOCK) ON ft.vendedor = b_us.userno
		LEFT JOIN
			fprod (NOLOCK) ON fprod.ref = st.ref
		WHERE
			 ft.fdata  BETWEEN ''' + CONVERT(VARCHAR(10),@startDate, 120) + ''' AND '''+ CONVERT(VARCHAR(10),@endDate, 120) + '''
			 AND ft.site = '''+ @site+''' 
			 AND td.tipodoc IN (1,3) 
			 AND fi.ref!=''''
			 AND fi.stns = 0
			 AND fi.qtt>0
			 AND ft.no>= 200
			 AND st.pim = 1
			 AND ut.pim = 1	
			 AND ft.cobrado = 0	'
			 
		IF @search !=''
		BEGIN
			set @sql = @sql + N'
				AND st.ref like ''%' + @search +'%'' '
		END		

		set @sql = @sql + N' ORDER BY
			 ft.fdata		ASC
			,ft.ousrhora	ASC
			,ft.fno			ASC
			,fi.ref			ASC '
		

		set @sql = @sql + N'
		SELECT  
				*,
				rowNumber = ROW_NUMBER() OVER(PARTITION BY SalesInfo_stamp ORDER BY
							 SalesInfo_stamp ASC) 
			INTO
				#salesRow
			FROM 
				#sales (NOLOCK) '

		set @sql = @sql + N' 
		SELECT '+
		convert(varchar(10),@topMax) +'														  AS pageSize,
		(COUNT(*) OVER() / ' +  convert(varchar(10),@topMax) +') + 1						  AS pageTotal, '
		+ convert(varchar(10),@PageNumber) +'												  AS pageNumber,
		COUNT(*) OVER()																		  AS linesTotal,
		' + convert(varchar(10),@topMax) +'													  AS topMax,
		*
		FROM
			#salesRow
		WHERE
			rowNumber = 1
		ORDER BY
			SalesInfo_stamp	ASC
			OFFSET ' + convert(varchar(10),@topMax) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
			FETCH NEXT  ' + convert(varchar(10),@topMax) + ' ROWS ONLY'

	print(@sql)
	exec(@sql)

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#sales'))
		DROP TABLE #sales
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#salesRow'))
		DROP TABLE  #salesRow

GO
Grant Execute On dbo.up_get_sales_pim to Public
Grant Control On dbo.up_get_sales_pim to Public
Go


