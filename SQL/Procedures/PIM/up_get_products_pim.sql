/* Devolve VEndas Pim tiMedi



	exec up_get_products_pim '20230131','20231031', 'Loja 1', 2, 1,''

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_products_pim]') IS NOT NULL
	drop procedure dbo.up_get_products_pim
go

create procedure dbo.up_get_products_pim
	@startDate					DATETIME,
	@endDate					DATETIME,
	@site						VARCHAR(20),
	@topMax						INT = 1000,
	@pageNumber					INT = 1,
	@search						VARCHAR(255) = ''
/* WITH ENCRYPTION */

AS

	DECLARE @sql	VARCHAR(8000) = ''
	DECLARE @site_nr INT

	IF(ISNULL(@pageNumber,0)<1)
	BEGIN
		SET @pageNumber = 1
	END

	SET @search = ISNULL(@search, '')

	SET @site_nr = isnull((Select top 1 no from empresa (nolock) where empresa.site = @site),0)

	set @sql = @sql + N'
	SELECT ' + convert(varchar(10),@topMax) +'												  AS pageSize,
		(COUNT(*) OVER() / ' +  convert(varchar(10),@topMax) +') + 1						  AS pageTotal, '
		+ convert(varchar(10),@PageNumber) +'												  AS pageNumber,
		COUNT(*) OVER()																		  AS linesTotal,
		' + convert(varchar(10),@topMax) +'													  AS topMax,

		dbo.alltrimIsNull(st.ref)															  AS ProductInfo_ref,
		dbo.alltrimIsNull(st.design)														  AS ProductInfo_name,
		dbo.alltrimIsNull(fprod.descricao)													  AS ProductInfo_description, '
		
		IF (select bool FROM B_Parameters_site WHERE stamp='ADM0000000219' AND site=@site) = 0
		BEGIN
			set @sql = @sql + N'	
				-999999																		  AS ProductInfo_pvp, 			
				-999999																		  AS ProductInfo_stock, '
		END
		ELSE
		BEGIN
			set @sql = @sql + N'	
				st.epv1																		  AS ProductInfo_pvp, 			
				st.stock																	  AS ProductInfo_stock, '
		END

		set @sql = @sql + N'
		CASE WHEN ISNUMERIC(Form_Farm_ID)=1 
			THEN convert(int,dbo.alltrimIsNull(fprod.Form_Farm_ID))	 
			ELSE 0 END																		  AS ProductInfo_idDosageForm,
		dbo.alltrimIsNull(fprod.fformasdescr)												  AS ProductInfo_formFarma,
		ISNULL(fprod.num_unidades,0)														  AS ProductInfo_qttMin,  
		st.usaid																			  AS ProductInfo_lastSale, 
		st.UINTR																			  AS ProductInfo_lastBuy
					
		FROM 
			st (NOLOCK)
		LEFT JOIN
			fprod (NOLOCK) ON fprod.ref = st.ref
		WHERE
			  st.pim = 1
			  AND st.site_nr = '''+ convert(varchar(10), @site_nr) +'''  '
		IF @search !=''
		BEGIN
			set @sql = @sql + N'
				AND st.design like ''%' + @search +'%'' '
		END	
		
	set @sql = @sql + N' ORDER BY
			st.ref		ASC
			OFFSET ' + convert(varchar(10),@topMax) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
			 FETCH NEXT  ' + convert(varchar(10),@topMax) + ' ROWS ONLY'
	
	print(@sql)
	exec(@sql)

GO
Grant Execute On dbo.up_get_products_pim to Public
Grant Control On dbo.up_get_products_pim to Public
Go


