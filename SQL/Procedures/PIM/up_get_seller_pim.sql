/* Devolve VEndas Pim tiMedi



	exec up_get_seller_pim '20230131','20231031', 20, 1,''

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_seller_pim]') IS NOT NULL
	drop procedure dbo.up_get_seller_pim
go

create procedure dbo.up_get_seller_pim
	@startDate					DATETIME,
	@endDate					DATETIME,
	@topMax						INT = 1000,
	@pageNumber					INT = 1,
	@search						VARCHAR(255) = ''
/* WITH ENCRYPTION */

AS

	DECLARE @sql	VARCHAR(8000) = ''

	IF(ISNULL(@pageNumber,0)<1)
	BEGIN
		SET @pageNumber = 1
	END

	SET @search = ISNULL(@search, '')

	set @sql = @sql + N'
	SELECT ' + convert(varchar(10),@topMax) +'												  AS pageSize,
		(COUNT(*) OVER() / ' +  convert(varchar(10),@topMax) +') + 1						  AS pageTotal, '
		+ convert(varchar(10),@PageNumber) +'												  AS pageNumber,
		COUNT(*) OVER()																		  AS linesTotal,
		' + convert(varchar(10),@topMax) +'													  AS topMax,

		ISNULL(b_us.userno,0)                                                                 AS SellerInfo_number, 
		ISNULL((select FIRST_NAME from uf_gerais_devolve_primeiro_e_ultimo_nome (b_us.nome))
		,b_us.nome)																			  AS SellerInfo_name,
		ISNULL((select LAST_NAME from uf_gerais_devolve_primeiro_e_ultimo_nome (b_us.nome))
		,'''')                                                                                  AS SellerInfo_surname
					
		FROM 
			b_us
		WHERE
			b_us.inactivo = 0	'
		
		IF @search !=''
		BEGIN
			set @sql = @sql + N'
				AND b_us.nome like ''%' + @search +'%'' '
		END	
			
	set @sql = @sql + N'	ORDER BY
			b_us.userno ASC
			OFFSET ' + convert(varchar(10),@topMax) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
			 FETCH NEXT  ' + convert(varchar(10),@topMax) + ' ROWS ONLY'
	
	print(@sql)
	exec(@sql)


GO
Grant Execute On dbo.up_get_seller_pim to Public
Grant Control On dbo.up_get_seller_pim to Public
Go


