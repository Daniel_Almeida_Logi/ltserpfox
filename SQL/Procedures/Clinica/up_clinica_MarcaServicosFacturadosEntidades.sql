
-- exec up_clinica_MarcaServicosFacturadosEntidades 1,0,'19000101','20141201'
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_clinica_MarcaServicosFacturadosEntidades]') IS NOT NULL
    drop procedure up_clinica_MarcaServicosFacturadosEntidades
go

create PROCEDURE up_clinica_MarcaServicosFacturadosEntidades
@no	numeric(9,0)
,@estab	numeric(5,3)
,@dataIni	datetime
,@dataFim	datetime


/* WITH ENCRYPTION */
AS

	UPDATE 
		marcacoesServ
	set
		fe = 1
	from 
		marcacoesServ
		inner join cpt_marcacoes on marcacoesServ.servmrstamp = cpt_marcacoes.servmrstamp
		inner join marcacoes on marcacoes.mrstamp = marcacoesServ.mrstamp
		inner join cpt_val_cli on cpt_val_cli.id = cpt_marcacoes.id_cp
		inner join B_utentes on cpt_val_cli.id_entidade = B_utentes.no
	where 
		entCompart = 1
		and dataInicio between @dataIni and @dataFim
		and B_utentes.no = @no
		and B_utentes.estab = @estab

	
GO
Grant Execute On up_clinica_MarcaServicosFacturadosEntidades to Public
Grant Control On up_clinica_MarcaServicosFacturadosEntidades to Public
go


