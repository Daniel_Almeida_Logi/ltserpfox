-- exec up_clinica_exportFt_ADSE
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_clinica_exportFt_ADSE]') IS NOT NULL
    drop procedure up_clinica_exportFt_ADSE
go

create PROCEDURE up_clinica_exportFt_ADSE
@ftstamp varchar(25)
/* WITH ENCRYPTION */
AS

	/* Facturacao Entidades ADSE*/

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosHeader'))
		DROP TABLE #dadosHeader
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosDetalhe'))
		DROP TABLE #dadosDetalhe
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFooter'))
		DROP TABLE #dadosFooter
		
		
	select 
		TIPOLINHA = '01'
		,TIPOFICHEIRO = '2'
		,NIF = REPLICATE('0',9-len(RTRIM(LTRIM(FT.NCONT))))+ LTRIM(RTRIM(FT.NCONT))
		,CODTIPENT = '00030002' /*??*/
		,NUMFACT = REPLICATE('0',8-len(RTRIM(LTRIM(STR(FT.FNO)))))+ LTRIM(RTRIM(STR(FT.FNO)))
		,ANOMESFACT = LEFT(convert(varchar,FT.FDATA,112),6)
		,DATAENVIO = convert(varchar,FT.FDATA,112)
		,FILER = REPLICATE('0',6)
		,NIB = REPLICATE('0',21)
		,FILER_2 = REPLICATE('0',56)
	into
		#dadosHeader
	from 
		ft (nolock)
	where 
		ft.ftstamp = @ftstamp
		

	Select
		TIPOLINHA = '02'
		,NUMUNIBEN = REPLICATE('0',9-len(LEFT(RTRIM(LTRIM(B_entidadesUtentes.cartao)),9))) + LEFT(RTRIM(LTRIM(B_entidadesUtentes.cartao)),9)
		,FILLER = '  '
		,CODCSAUDE = REPLICATE('0',12-len(RTRIM(LTRIM(cpt_val_cli.refEntidade)))) + RTRIM(LTRIM(cpt_val_cli.refEntidade))
		,DTACSAUDE = convert(varchar,marcacoes.data,112)
		,QTD = REPLICATE('0',4-len(RTRIM(LTRIM(STR(marcacoesServ.qtt)))))+ LTRIM(RTRIM(STR(marcacoesServ.qtt)))
		,LOCAL_PREST = '' /*Numero de Local de Presta��o ??*/
		,CODPATOLOGIA = '' /* FALTA */
		,PRESCRITOR = REPLICATE('0',6-len(isnull((
			select 
				b_us.drcedula 
			from 
				b_us 
				inner join marcacoesRecurso on b_us.userno = marcacoesRecurso.no 
			where 
				marcacoesRecurso.tipo = 'Utilizadores' 
				and marcacoesRecurso.mrstamp = marcacoes.mrstamp
		),''))) +
			isnull((
				select 
					b_us.drcedula 
				from 
					b_us 
					inner join marcacoesRecurso on b_us.userno = marcacoesRecurso.no 
				where 
					marcacoesRecurso.tipo = 'Utilizadores' 
					and marcacoesRecurso.mrstamp = marcacoes.mrstamp
			),'')
		,FILLER_2 = '   '
		,VALPAGAR_EUROS = REPLICATE('0',16-LEN(RTRIM(LTRIM(STR(ABS(fi.etiliquido)*100,9,0))))) + RTRIM(LTRIM(STR(ABS(fi.etiliquido)*100,9,0)))
		,NUMDOC = REPLICATE('0',4-LEN(RTRIM(LTRIM(STR(ROW_NUMBER() OVER(ORDER BY fi.lordem DESC)))))) + RTRIM(LTRIM(STR(ROW_NUMBER() OVER(ORDER BY fi.lordem DESC))))
		,FILLER_3 = REPLICATE(' ',5)
		,NUMREQUISICAO = REPLICATE('0',9) /* FALTA */
		,LOCAL_PRESCRICAO = 'U' + REPLICATE('0',6) /* FALTA */
		,NIF_MEDICORESP = REPLICATE('0',9) /* FALTA */
		,NIF_TECNICORESP = REPLICATE('0',9) /* FALTA */
		,INFO = REPLICATE('0',3)
		,fi.etiliquido
	into
		#dadosDetalhe
	from 
		fi (nolock)
		inner join ft (nolock) on fi.ftstamp = ft.ftstamp
		inner join marcacoesServ (nolock) on marcacoesServ.servmrstamp = fi.bistamp
		inner join marcacoes (nolock) on marcacoes.mrstamp = marcacoesServ.mrstamp
		inner join b_utentes (nolock) on marcacoes.no = b_utentes.no and b_utentes.estab = marcacoes.estab
		inner join B_entidadesUtentes (nolock) on B_entidadesUtentes.no = b_utentes.no
		inner join cpt_val_cli (nolock) on fi.ref = cpt_val_cli.ref and id_entidade = ft.no
	where 
		ft.ftstamp = @ftstamp



	Select 
		TIPOLINHA = '03'
		,QTDTOTBEN = REPLICATE('0',7-LEN(RTRIM(LTRIM(STR(LEN(COUNT(*))))))) + RTRIM(LTRIM(STR(LEN(COUNT(*)))))
		,FILER = REPLICATE('0',16)
		,VALTOTAL_EUROS = REPLICATE('0',16-LEN(RTRIM(LTRIM(STR(ABS(SUM(#dadosDetalhe.etiliquido))*100,9,0))))) + RTRIM(LTRIM(STR(ABS(SUM(#dadosDetalhe.etiliquido))*100,9,0)))
		,FILER_2 = REPLICATE('0',84)
	Into
		#dadosFooter
	From
		#dadosDetalhe
		
		
		
	Select 
		texto = TIPOLINHA 
		+ TIPOFICHEIRO	
		+ NIF
		+ CODTIPENT
		+ NUMFACT
		+ ANOMESFACT
		+ DATAENVIO
		+ FILER
		+ NIB
		+ FILER_2
	From
		#dadosHeader
		
	Union ALL

	Select
		TIPOLINHA
		+ NUMUNIBEN
		+ FILLER
		+ CODCSAUDE
		+ DTACSAUDE
		+ QTD
		+ LOCAL_PREST
		+ CODPATOLOGIA
		+ PRESCRITOR
		+ FILLER_2
		+ VALPAGAR_EUROS
		+ NUMDOC
		+ FILLER_3
		+ NUMREQUISICAO
		+ LOCAL_PRESCRICAO
		+ NIF_MEDICORESP
		+ NIF_TECNICORESP
		+ INFO
	From
		#dadosDetalhe
		
	Union ALL

			
	Select 
		TIPOLINHA
		+ QTDTOTBEN
		+ FILER
		+ VALTOTAL_EUROS
		+ FILER_2
	from 
		#dadosFooter
	
GO
Grant Execute On up_clinica_exportFt_ADSE to Public
Grant Control On up_clinica_exportFt_ADSE to Public
go


