

-- exec up_clinica_validaExistenciaRefFatEntidades
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_clinica_validaExistenciaRefFatEntidades]') IS NOT NULL
    drop procedure up_clinica_validaExistenciaRefFatEntidades
go

create PROCEDURE up_clinica_validaExistenciaRefFatEntidades

/* WITH ENCRYPTION */
AS

	IF (select COUNT(ref) from st where ref = '9999999') = 0
	Begin
		INSERT INTO st ([ststamp], [ref], [design], [familia], [stock], [epv1], [forref], [fornecedor], [usr1], [validade], [usaid], [uintr], [usrqtt], [eoq], [pcult], [pvultimo], [unidade], [ptoenc], [tabiva], [local], [fornec], [fornestab], [qttfor], [qttcli], [qttrec], [udata], [pcusto], [pcpond], [qttacin], [qttacout], [stmax], [stmin], [obs], [codigo], [uni2], [conversao], [ivaincl], [nsujpp], [ecomissao], [imagem], [cpoc], [containv], [contacev], [contareo], [contacoe], [contaieo], [peso], [bloqueado], [fobloq], [mfornec], [mfornec2], [pentrega], [consumo], [baixr], [despimp], [marg1], [marg2], [marg3], [marg4], [diaspto], [diaseoq], [clinica], [pbruto], [volume], [usalote], [faminome], [qttesp], [epcusto], [epcpond], [epcult], [epvultimo], [iva1incl], [iva2incl], [iva3incl], [iva4incl], [iva5incl], [ivapcincl], [stns], [url], [iecasug], [iecaref], [iecarefnome], [iecaisref], [qlook], [qttcat], [compnovo], [inactivo], [u_impetiq], [ousrinis], [ousrdata], [ousrhora], [usrinis], [usrdata], [usrhora], [marcada], [u_nota1], [u_local], [u_local2], [u_lab], [u_tipoetiq], [u_validavd], [u_aprov], [u_fonte], [u_validarc], [sujinv], [u_duracao], [u_servmarc], [u_catstamp], [u_famstamp], [u_secstamp], [u_depstamp], [rateamento], [exportado], [u_codint])
		SELECT N'ADM004116AB-49C1-456D-81F' AS [ststamp], N'9999999' AS [ref], N'Factura��o �s Entidades' AS [design], N'99' AS [familia], N'0.000' AS [stock], N'0.000000' AS [epv1], N'' AS [forref], N'' AS [fornecedor], N'' AS [usr1], N'1900-01-01 00:00:00.000' AS [validade], N'1900-01-01 00:00:00.000' AS [usaid], N'1900-01-01 00:00:00.000' AS [uintr], N'0.000' AS [usrqtt], N'0.000' AS [eoq], N'0.00000' AS [pcult], N'0.00000' AS [pvultimo], N'' AS [unidade], N'0.000' AS [ptoenc], N'0' AS [tabiva], N'' AS [local], N'0' AS [fornec], N'0' AS [fornestab], N'0.000' AS [qttfor], N'0.000' AS [qttcli], N'0.000' AS [qttrec], N'1900-01-01 00:00:00.000' AS [udata], N'0.00000' AS [pcusto], N'0.00000' AS [pcpond], N'0.000' AS [qttacin], N'0.000' AS [qttacout], N'0.000' AS [stmax], N'0.000' AS [stmin], N'' AS [obs], N'' AS [codigo], N'' AS [uni2], N'0.0000000' AS [conversao], N'1' AS [ivaincl], N'0' AS [nsujpp], N'0' AS [ecomissao], N'' AS [imagem], N'0' AS [cpoc], N'' AS [containv], N'' AS [contacev], N'' AS [contareo], N'' AS [contacoe], N'' AS [contaieo], N'0.000' AS [peso], N'0' AS [bloqueado], N'0' AS [fobloq], N'0.00' AS [mfornec], N'0.00' AS [mfornec2], N'0' AS [pentrega], N'0.000' AS [consumo], N'0' AS [baixr], N'0.00' AS [despimp], N'0.000' AS [marg1], N'0.000' AS [marg2], N'0.000' AS [marg3], N'0.000' AS [marg4], N'0' AS [diaspto], N'0' AS [diaseoq], N'0' AS [clinica], N'0.000' AS [pbruto], N'0.000' AS [volume], N'0' AS [usalote], N'Outros' AS [faminome], N'0.000' AS [qttesp], N'0.000000' AS [epcusto], N'0.000000' AS [epcpond], N'0.000000' AS [epcult], N'0.000000' AS [epvultimo], N'0' AS [iva1incl], N'0' AS [iva2incl], N'0' AS [iva3incl], N'0' AS [iva4incl], N'0' AS [iva5incl], N'0' AS [ivapcincl], N'1' AS [stns], N'' AS [url], N'0' AS [iecasug], N'' AS [iecaref], N'' AS [iecarefnome], N'0' AS [iecaisref], N'0' AS [qlook], N'0.000' AS [qttcat], N'0' AS [compnovo], N'0' AS [inactivo], N'0' AS [u_impetiq], N'' AS [ousrinis], N'2011-03-10 18:12:36.947' AS [ousrdata], N'' AS [ousrhora], N'vss' AS [usrinis], N'2013-05-14 00:00:00.000' AS [usrdata], N'17:13:31' AS [usrhora], N'0' AS [marcada], N'' AS [u_nota1], N'' AS [u_local], N'' AS [u_local2], N'' AS [u_lab], N'0' AS [u_tipoetiq], N'0' AS [u_validavd], N'' AS [u_aprov], N'I' AS [u_fonte], N'0' AS [u_validarc], N'0' AS [sujinv], N'0' AS [u_duracao], N'0' AS [u_servmarc], N'' AS [u_catstamp], N'' AS [u_famstamp], N'' AS [u_secstamp], N'' AS [u_depstamp], N'0' AS [rateamento], N'0' AS [exportado], N'' AS [u_codint] 
	End 
	
GO
Grant Execute On up_clinica_validaExistenciaRefFatEntidades to Public
Grant Control On up_clinica_validaExistenciaRefFatEntidades to Public
go


