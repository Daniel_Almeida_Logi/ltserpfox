

-- exec up_clinica_CalculoHonorariosDetalhe '19000101','20160101','','','',''
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_clinica_CalculoHonorariosDetalhe]') IS NOT NULL
    drop procedure up_clinica_CalculoHonorariosDetalhe
go

create PROCEDURE up_clinica_CalculoHonorariosDetalhe
@dataIni	datetime
,@dataFim	datetime
,@especialista varchar(30)
,@utente	varchar(55)
,@entidade	varchar(55)
,@ref	varchar(18)


/* WITH ENCRYPTION */
AS

	select 
		sel = CONVERT(bit,0)
		,marcacoesServ.servmrstamp
		,Especialista = Convert(varchar(254),RTRIM(LTRIM(marcacoesRecurso.nome)) + ' [' + CONVERT(varchar,marcacoesRecurso.no) + ']')
		,EspecialistaNome = marcacoesRecurso.nome
		,EspecialistaNo = marcacoesRecurso.no
		,Entidade = isnull(B_utentes.nome,'')
		,EntidadeNo = isnull(B_utentes.no,0)
		,EntidadeEstab = isnull(B_utentes.estab,0)
		,Entidade = isnull(B_utentes.nome,'')
		,EntidadeNo = isnull(B_utentes.no,0)
		,EntidadeEstab = isnull(B_utentes.estab,0)
		,marcacoes.mrno
		,dataInicio = marcacoesServ.dataInicio
		,dataFim = marcacoesServ.dataFim
		,marcacoesServ.hinicio
		,marcacoesServ.hfim
		,marcacoes.nome
		,marcacoes.no
		,marcacoes.estab
		,marcacoesServ.ref
		,marcacoesServ.design
		,marcacoesServ.pvp
		,marcacoesServ.qtt
		,marcacoesServ.total
		,totalLinha = marcacoesServ.pvp * marcacoesServ.qtt
		,tipoCompart = isnull(cpt_val_cli.tipoCompart,'')
		,compart = isnull(cpt_val_cli.compart,0)
		,maximo = isnull(cpt_val_cli.maximo,0)
		,marcacoes.obs
		,valorUtente = marcacoesServ.total
		,valorEntidade = marcacoesServ.compart
		,HonorarioBi = isnull(b_us.HonorarioBi,0)
		,HonorarioTipo = isnull(b_us.HonorarioTipo,'')
		,HonorarioValor = isnull(b_us.HonorarioValor,0)
		,HonorarioDeducao = isnull(b_us.HonorarioDeducao,0)
		,valorHonorario = 0.00 /*Calculo Feito no fox*/
		,VALORADICIONAL = 0.00
		,processado = CONVERT(bit,0)
		,honEspPendente = case when EstadoHonEsp = 1 then CONVERT(bit,1) else CONVERT(bit,0) end
		,honEspNPagar = case when EstadoHonEsp = 2 then CONVERT(bit,1) else CONVERT(bit,0) end
		,honEspPagar = case when EstadoHonEsp = 3 then CONVERT(bit,1) else CONVERT(bit,0) end
		,honOpPendente = case when EstadoHonOp = 1 then CONVERT(bit,1) else CONVERT(bit,0) end
		,honOpNPagar = case when EstadoHonOp = 2 then CONVERT(bit,1) else CONVERT(bit,0) end
		,honOpPagar = case when EstadoHonOp = 3 then CONVERT(bit,1) else CONVERT(bit,0) end
	from 
		marcacoes
		inner join marcacoesServ on marcacoes.mrstamp = marcacoesServ.mrstamp
		left join marcacoesRecurso on marcacoesRecurso.mrstamp = marcacoes.mrstamp and marcacoesRecurso.tipo = 'Utilizador' and marcacoesServ.ref = marcacoesRecurso.ref
		Left join b_us on b_us.userno = marcacoesRecurso.no
		left join cpt_marcacoes on marcacoesServ.servmrstamp = cpt_marcacoes.servmrstamp
		left join cpt_val_cli on cpt_val_cli.id = cpt_marcacoes.id_cp
		left join B_utentes on cpt_val_cli.id_entidade = B_utentes.no
	Where
		marcacoes.data  >= @dataIni and marcacoes.dataFim < @dataFim +1 /* At� ao final do dia*/
		and isnull(marcacoesRecurso.no,0) != 0	
		--and (isnull(marcacoesRecurso.nome,'') =  @especialista or @especialista = '')
		--and (isnull(marcacoesRecurso.nome,'') =  @especialista or @especialista = '')
		--and (marcacoes.nome = @utente or @utente = '')
		--and (B_utentes.nome = @entidade or @entidade = '')
		--and (marcacoesServ.ref = @ref or @ref = '')
		and marcacoesServ.honorario = 0
	Order
		by mrno

	
GO
Grant Execute On up_clinica_CalculoHonorariosDetalhe to Public
Grant Control On up_clinica_CalculoHonorariosDetalhe to Public
go
