


-- exec up_clinica_DadosComparticipacao 'ADM849B66C6-7FEB-4F9D-AC9','1000955','BENEDITA'
--exec up_clinica_DadosComparticipacao '52A484DC1E774035A917','1001207','BENEDITA',''
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_clinica_DadosComparticipacao]') IS NOT NULL
    drop procedure up_clinica_DadosComparticipacao
go

create PROCEDURE up_clinica_DadosComparticipacao
@utstamp	varchar(55)
,@ref	varchar(18)
,@site varchar(60) = ''
,@nomeUtilizadores varchar(max) = ''

/* WITH ENCRYPTION */
AS
	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosUS'))
		DROP TABLE #dadosUS
	
	Select 
		userno
	into
		#dadosUS
	from 
		b_us (nolock)
	where 
		b_us.nome in (Select items from up_SplitToTable(@nomeUtilizadores,','))
	

	select 
		distinct 
		B_entidadesUtentes.enome
		,B_entidadesUtentes.eno
		,B_entidadesUtentes.eestab
		,B_entidadesUtentes.eutstamp
		,cpt_val_cli.utstamp
		,cpt_val_cli.ref
		,cpt_val_cli.tipoCompart
		,cpt_val_cli.compart
		,cpt_val_cli.maximo
		,cpt_val_cli.obs
		,cpt_val_cli.inativo
		,cpt_val_cli.refEntidade
		,cpt_val_cli.designEntidade
		,cpt_val_cli.pref
		,cpt_val_cli.site
		,cpt_val_cli.id_cpt_conv
		,cpt_val_cli.id_entidade
		,cpt_val_cli.pvp
		,valorUtente = CONVERT(numeric(9,2),0)
		,valorEntidade = CONVERT(numeric(9,2),0)
		,convencao = cpt_conv.descr
		,design = isnull((select top 1 design from st (nolock) where st.ref = cpt_val_cli.ref),'')
		,id = cpt_conv.id
	from 
		B_entidadesUtentes
		inner join cpt_conv (nolock) on cpt_conv.id_entidade = B_entidadesUtentes.eno
		inner join cpt_val_cli (nolock) on cpt_val_cli.id_cpt_conv = cpt_conv.id
		inner join cpt_conv_us (nolock) on cpt_conv.id = cpt_conv_us.id_cpt_conv
	where
		B_entidadesUtentes.utstamp = @utstamp
		and (B_entidadesUtentes.validade >= convert(varchar,GETDATE(),112) or cpt_conv.vDtCartao = 0)
		and ref = @ref
		and cpt_conv.site = @site
		and (cpt_conv_us.id_us in (select userno from #dadosUS) or @nomeUtilizadores = '')
		

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosUS'))
		DROP TABLE #dadosUS

	
GO
Grant Execute On up_clinica_DadosComparticipacao to Public
Grant Control On up_clinica_DadosComparticipacao to Public
go
