-- exec up_clinica_EntidadesFacturacao ''
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_clinica_EntidadesFacturacao]') IS NOT NULL
    drop procedure up_clinica_EntidadesFacturacao
go

create PROCEDURE up_clinica_EntidadesFacturacao
@nome		varchar(55)

/* WITH ENCRYPTION */
AS
	select 
		nome
		,no
		,estab
	from 
		B_utentes 
	where 
		entCompart = 1
		and nome = case when @nome = '' then nome else @nome end
	Order by
		nome
GO
Grant Execute On up_clinica_EntidadesFacturacao to Public
Grant Control On up_clinica_EntidadesFacturacao to Public
go


