-- exec up_clinica_exportFt_ADM ''
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_clinica_exportFt_ADM]') IS NOT NULL
    drop procedure up_clinica_exportFt_ADM
go

create PROCEDURE up_clinica_exportFt_ADM
@ftstamp varchar(25)

/* WITH ENCRYPTION */
AS
		
	select 
		Tipo_de_Registo = '1'
		,Codigo_da_Entidade = 'OE162400'
		,Codigo_da_Sucursal = ''
		,NIF = REPLICATE('0',9-len(RTRIM(LTRIM(FT.NCONT))))+ LTRIM(RTRIM(FT.NCONT))
		,Numero_do_Documento = REPLICATE('0',20-len(RTRIM(LTRIM(STR(FT.FNO)))))+ LTRIM(RTRIM(STR(FT.FNO)))
		,Data_da_Factura = LEFT(convert(varchar,FT.FDATA,112),4) + '-' + RIGHT(LEFT(convert(varchar,FT.FDATA,112),6),2)+ '-' + RIGHT(convert(varchar,FT.FDATA,112),2)
		,Total_Factura = REPLICATE('0',12-len(RTRIM(LTRIM(STR(Ft.etotal,12,2)))))+RTRIM(LTRIM(STR(Ft.etotal,12,2)))
		,Total_ADM = REPLICATE('0',12-len(RTRIM(LTRIM(STR(Ft.etotal,12,2)))))+RTRIM(LTRIM(STR(Ft.etotal,12,2)))
		,Total_Beneficiario = REPLICATE('0',12-len(RTRIM(LTRIM(STR(Ft.etotal,12,2)))))+RTRIM(LTRIM(STR(Ft.etotal,12,2)))
	into
		#dadosHeader
	from 
		ft (nolock)
	where 
		ft.ftstamp = @ftstamp
		

	Select
		Tipo_de_Registo = '2'
		,Tipo_de_Lote = '' /*So Farmacias*/
		,Numero_do_Lote = '' /*So Farmacias*/
		,Numero_da_Receita = '' /*So Farmacias*/
		,Numero_Cartao_ADM = REPLICATE('0',15-len(LEFT(RTRIM(LTRIM(B_entidadesUtentes.cartao)),9))) + LEFT(RTRIM(LTRIM(B_entidadesUtentes.cartao)),9)
		,Data_do_Acto = LEFT(convert(varchar,marcacoes.data,112),4) + '-' + RIGHT(LEFT(convert(varchar,marcacoes.data,112),6),2)+ '-' + RIGHT(convert(varchar,marcacoes.data,112),2)
		,Data_Alta = ''
		,Codigo_Comparticipacao_ADM = REPLICATE('0',20-len(RTRIM(LTRIM(cpt_val_cli.refEntidade)))) + RTRIM(LTRIM(cpt_val_cli.refEntidade))
		,Percentagem_Comparticipacao = REPLICATE('0',5-len(RTRIM(LTRIM(STR(cpt_val_cli.compart,9,2))))) + RTRIM(LTRIM(STR(cpt_val_cli.compart,9,2)))
		,Quantidade = REPLICATE('0',4-len(RTRIM(LTRIM(STR(marcacoesServ.qtt)))))+ LTRIM(RTRIM(STR(marcacoesServ.qtt)))
		,PVP = REPLICATE('0',5-len(RTRIM(LTRIM(STR(cpt_val_cli.pvp,9,2))))) + RTRIM(LTRIM(STR(cpt_val_cli.pvp,9,2)))
		,Preco_Referencia = ''
		,Preco_ADM = REPLICATE('0',5-len(RTRIM(LTRIM(STR(fi.etiliquido,9,2))))) + RTRIM(LTRIM(STR(fi.etiliquido,9,2)))
		,Preco_Beneficiario = REPLICATE('0',5-len(RTRIM(LTRIM(STR(marcacoesServ.total,9,2))))) + RTRIM(LTRIM(STR(marcacoesServ.total,9,2)))
		,NumeroComplementar_Dente = ''
	into
		#dadosDetalhe
	from 
		fi (nolock)
		inner join ft (nolock) on fi.ftstamp = ft.ftstamp
		inner join marcacoesServ (nolock) on marcacoesServ.servmrstamp = fi.bistamp
		inner join marcacoes (nolock) on marcacoes.mrstamp = marcacoesServ.mrstamp
		inner join b_utentes (nolock) on marcacoes.no = b_utentes.no and b_utentes.estab = marcacoes.estab
		inner join B_entidadesUtentes (nolock) on B_entidadesUtentes.no = b_utentes.no
		inner join cpt_val_cli (nolock) on fi.ref = cpt_val_cli.ref and id_entidade = ft.no
	where 
		ft.ftstamp = @ftstamp


		
		
		
	Select 
		texto = Tipo_de_Registo + '#'
		+ Codigo_da_Entidade + '#'
		+ Codigo_da_Sucursal + '#'
		+ NIF + '#'
		+ Numero_do_Documento + '#'
		+ Data_da_Factura + '#'
		+ Total_Factura + '#'
		+ Total_ADM + '#'
		+ Total_Beneficiario + '#'
	From
		#dadosHeader
		
	Union ALL

	Select
		texto = Tipo_de_Registo + '#'
		+ Tipo_de_Lote + '#'
		+ Numero_do_Lote + '#'
		+ Numero_da_Receita + '#'
		+ Numero_Cartao_ADM + '#'
		+ Data_do_Acto + '#'
		+ Data_Alta + '#'
		+ Codigo_Comparticipacao_ADM + '#'
		+ Percentagem_Comparticipacao + '#'
		+ Quantidade + '#'
		+ PVP + '#'
		+ Preco_Referencia + '#'
		+ Preco_ADM + '#'
		+ Preco_Beneficiario + '#'
		+ NumeroComplementar_Dente + '#'
	From
		#dadosDetalhe
	
GO
Grant Execute On up_clinica_exportFt_ADM to Public
Grant Control On up_clinica_exportFt_ADM to Public
go


