-- exec up_clinica_importaHonorarios '19000101','20160101',50,'','',''

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_clinica_importaHonorarios]') IS NOT NULL
	drop procedure up_clinica_importaHonorarios
go

create PROCEDURE up_clinica_importaHonorarios
@dataIni	datetime
,@dataFim	datetime
,@userno numeric(9,0)
,@utente	varchar(55)
,@entidade	varchar(55)
,@ref	varchar(18)


/* WITH ENCRYPTION */
AS

		 
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempDadosComConvencao'))
		DROP TABLE #tempDadosComConvencao
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempDadosSemConvencao'))
		DROP TABLE #tempDadosSemConvencao
		
		
	select 
		userno = marcacoesRecurso.no
		,marcacoesServ.servmrstamp
		,marcacoes.mrstamp
		,marcacoes.data
		,marcacoes.nome
		,marcacoes.no
		,marcacoesServ.ref
		,marcacoesServ.design
		,marcacoesServ.pvp
		,marcacoesServ.qtt
		,marcacoesServ.compart
		,marcacoesServ.total
		,entidade = isnull(entidade,'')
		,entidadeno = isnull(cpt_marcacoes.entidadeno,0)
		,marcacoesRecurso.tipo
		,convencao = isnull(cpt_conv.descr,'SEM CONVENCAO')
		,HonorarioBi = isnull(cpt_val_cli_us.HonorarioBi,'')
		,HonorarioTipo = isnull(cpt_val_cli_us.HonorarioTipo,'')
		,HonorarioValor = isnull(cpt_val_cli_us.HonorarioValor,0)
		,HonorarioDeducao = isnull(cpt_val_cli_us.HonorarioDeducao,0)
		,id_cpt_val_cli = isnull(cpt_val_cli_us.id_cpt_val_cli,'')
		,marcacoesServ.fu
		,marcacoesServ.fe
	into
		#tempDadosComConvencao
	from
		marcacoes
		inner join marcacoesServ on marcacoesServ.mrstamp = marcacoes.mrstamp
		inner join marcacoesRecurso on marcacoesRecurso.mrstamp = marcacoes.mrstamp and marcacoesServ.ref = marcacoesRecurso.ref
		left join fn_honorarios (nolock) on marcacoesServ.servmrstamp = fn_honorarios.servmrstamp
		left join cpt_marcacoes on marcacoesServ.servmrstamp = cpt_marcacoes.servmrstamp
		left join cpt_conv on cpt_conv.id = cpt_marcacoes.id_cp
		left join cpt_val_cli on cpt_val_cli.id_cpt_conv = cpt_conv.id and cpt_val_cli.ref = marcacoesServ.ref 
		left join cpt_val_cli_us on cpt_val_cli_us.id_cpt_val_cli = cpt_val_cli.id
	Where
		cpt_conv.descr is not null
		and marcacoesRecurso.no = @userno
		and fn_honorarios.honorario is null
	
	select 
		userno = marcacoesRecurso.no
		,marcacoesServ.servmrstamp
		,marcacoes.mrstamp
		,marcacoes.data
		,marcacoes.nome
		,marcacoes.no
		,marcacoesServ.ref
		,marcacoesServ.design
		,marcacoesServ.pvp
		,marcacoesServ.qtt
		,marcacoesServ.compart
		,marcacoesServ.total
		,entidade = isnull(entidade,'')
		,entidadeno = isnull(cpt_marcacoes.entidadeno,0)
		,marcacoesRecurso.tipo
		,convencao = isnull(cpt_conv.descr,'SEM CONVENCAO')
		,HonorarioBi = isnull(cpt_val_cli_us.HonorarioBi,'')
		,HonorarioTipo = isnull(cpt_val_cli_us.HonorarioTipo,'')
		,HonorarioValor = isnull(cpt_val_cli_us.HonorarioValor,0)
		,HonorarioDeducao = isnull(cpt_val_cli_us.HonorarioDeducao,0)
		,id_cpt_val_cli = isnull(cpt_val_cli_us.id_cpt_val_cli,'')
		,marcacoesServ.fu
		,marcacoesServ.fe
	into
		#tempDadosSemConvencao
	from
		marcacoes
		inner join marcacoesServ on marcacoesServ.mrstamp = marcacoes.mrstamp
		inner join marcacoesRecurso on marcacoesRecurso.mrstamp = marcacoes.mrstamp and marcacoesServ.ref = marcacoesRecurso.ref
		left join fn_honorarios (nolock) on marcacoesServ.servmrstamp = fn_honorarios.servmrstamp
		left join cpt_marcacoes on marcacoesServ.servmrstamp = cpt_marcacoes.servmrstamp
		left join cpt_conv on cpt_conv.id = cpt_marcacoes.id_cp
		left join cpt_val_cli on cpt_val_cli.id_cpt_conv = cpt_conv.id and cpt_val_cli.ref = marcacoesServ.ref 
		left join cpt_val_cli_us on marcacoesServ.ref = cpt_val_cli_us.ref  and cpt_val_cli_us.id_cpt_val_cli = '' and cpt_val_cli_us.userno = @userno
	Where
		cpt_conv.descr is null
		and marcacoesRecurso.no = @userno
		and fn_honorarios.honorario is null
		
	
	Select 
		sel = CONVERT(bit,0)
		,honorario = 0.00
		,despesa = 0.00
		,*
	From
		#tempDadosComConvencao
	
	union all
	
	Select 
		sel = CONVERT(bit,0)
		,honorario = 0.00
		,despesa = 0.00
		,*
	From
		#tempDadosSemConvencao
	Order by 
		data
	
	
	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempDadosComConvencao'))
		DROP TABLE #tempDadosComConvencao
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempDadosSemConvencao'))
		DROP TABLE #tempDadosSemConvencao
		
	
GO
Grant Execute On up_clinica_importaHonorarios to Public
Grant Control On up_clinica_importaHonorarios to Public
go
