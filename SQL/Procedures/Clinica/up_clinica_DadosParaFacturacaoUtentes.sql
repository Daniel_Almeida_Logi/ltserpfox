
-- exec up_clinica_DadosParaFacturacaoUtentes '','19000101','20140601'
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_clinica_DadosParaFacturacaoUtentes]') IS NOT NULL
    drop procedure up_clinica_DadosParaFacturacaoUtentes
go

create PROCEDURE up_clinica_DadosParaFacturacaoUtentes
@utente	varchar(55)
,@dataIni	datetime
,@dataFim	datetime


/* WITH ENCRYPTION */
AS

	select 
		sel = CONVERT(bit,0)
		,Utente = LTRIM(RTRIM(marcacoes.nome)) + ' [' + CONVERT(VARCHAR(9),marcacoes.no) + ']' + ' [' + CONVERT(VARCHAR(9),marcacoes.estab) + ']'
		,UtenteNo = marcacoes.no
		,UtenteEstab = marcacoes.estab
		,faturarUtente = sum(case when marcacoesServ.fu = 0 then marcacoesServ.total else 0 end)
		,dataIni = @dataIni
		,dataFim = @dataFim
	from
		marcacoes 
		inner join marcacoesServ on marcacoesServ.mrstamp = marcacoes.mrstamp	
	where 
		(marcacoes.nome = @utente or @utente = '')
		and dataInicio between @dataIni and @dataFim
		and servmrstamp in (Select servmrstamp from marcacoesServ where marcacoesServ.mrstamp = marcacoes.mrstamp and marcacoesServ.fu = 0)
	group by
		marcacoes.nome
		,marcacoes.no
		,marcacoes.estab
	
		
	
GO
Grant Execute On up_clinica_DadosParaFacturacaoUtentes to Public
Grant Control On up_clinica_DadosParaFacturacaoUtentes to Public
go


