

-- exec up_clinica_DadosParaFacturacaoUtentesDetalhe '','20140506','20150506','',''
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_clinica_DadosParaFacturacaoUtentesDetalhe]') IS NOT NULL
    drop procedure up_clinica_DadosParaFacturacaoUtentesDetalhe
go

create PROCEDURE up_clinica_DadosParaFacturacaoUtentesDetalhe

@utente		varchar(55)
,@dataIni	datetime
,@dataFim	datetime
,@entidade	varchar(55)
,@ref	varchar(18)


/* WITH ENCRYPTION */
AS

	select 
		
		sel = CONVERT(bit,0)
		,Entidade = isnull(cpt_marcacoes.Entidade,'')
		,EntidadeNo = isnull(cpt_marcacoes.Entidadeno,0)
		,EntidadeEstab = isnull(cpt_marcacoes.Entidadeestab,0)
		,marcacoes.mrno
		,dataInicio = marcacoesServ.dataInicio
		,dataFim = marcacoesServ.dataFim
		,marcacoesServ.hinicio
		,marcacoesServ.hfim
		,marcacoes.nome
		,marcacoes.no
		,marcacoes.estab
		,marcacoesServ.ref
		,marcacoesServ.design
		,marcacoesServ.pvp
		,marcacoesServ.qtt
		,total = marcacoesServ.pvp * marcacoesServ.qtt
		,tipoCompart = isnull(cpt_val_cli.tipoCompart,'')
		,compart = isnull(cpt_val_cli.compart,0)
		,maximo = isnull(cpt_val_cli.maximo,0)
		,marcacoes.obs
		,NBENEF = ''
		,faturarUtente = case when marcacoesServ.fu = 0 then marcacoesServ.total else 0 end
		,faturarEntidade =  case when marcacoesServ.fe = 0 then marcacoesServ.compart else 0 end
		,tabiva = isnull(st.tabiva,4)
		,iva = isnull(taxasiva.taxa,0)
		,ivaincl = isnull(st.ivaincl,0)
		,marcacoesServ.servmrstamp
	from 
		marcacoes
		inner join marcacoesServ on marcacoes.mrstamp = marcacoesServ.mrstamp
		inner join empresa on marcacoes.site = empresa.site
		left join cpt_marcacoes on marcacoesServ.servmrstamp = cpt_marcacoes.servmrstamp
		left join cpt_val_cli on cpt_val_cli.id = cpt_marcacoes.id_cp
		left join B_utentes on cpt_val_cli.id_entidade = B_utentes.no
		left join st on st.ref = marcacoesServ.ref and st.site_nr = empresa.no
		left join taxasiva on taxasiva.codigo = st.tabiva
	where 
		dataInicio between @dataIni and @dataFim
		and (marcacoes.nome = @utente or @utente = '')
		and (B_utentes.nome = @entidade or @entidade = '')
		and (marcacoesServ.ref = @ref or @ref = '')
		and marcacoesServ.fu = 0
		--(
		--	case when marcacoesServ.fu = 0 then marcacoesServ.total else 0 end != 0 
		--	or case when marcacoesServ.fe = 0 then marcacoesServ.compart else 0 end != 0)
		
GO
Grant Execute On up_clinica_DadosParaFacturacaoUtentesDetalhe to Public
Grant Control On up_clinica_DadosParaFacturacaoUtentesDetalhe to Public
go


