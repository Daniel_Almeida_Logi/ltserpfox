
-- exec up_clinica_DadosParaFacturacaoEntidades '','19000101','20140601'
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_clinica_DadosParaFacturacaoEntidades]') IS NOT NULL
    drop procedure up_clinica_DadosParaFacturacaoEntidades
go

create PROCEDURE up_clinica_DadosParaFacturacaoEntidades
@entidade	varchar(55)
,@dataIni	datetime
,@dataFim	datetime

/* WITH ENCRYPTION */
AS

	select 
		sel = CONVERT(bit,0)
		,Entidade = isnull(cpt_marcacoes.Entidade,'')
		,EntidadeNo = isnull(cpt_marcacoes.Entidadeno,0)
		,EntidadeEstab = isnull(cpt_marcacoes.Entidadeestab,0)
	from 
		cpt_marcacoes
		inner join marcacoesServ on marcacoesServ.servmrstamp = cpt_marcacoes.servmrstamp
		left join cpt_val_cli on cpt_val_cli.id = cpt_marcacoes.id_cp
		left join B_utentes on cpt_val_cli.id_entidade = B_utentes.no
	where 
		(B_utentes.nome = @entidade or @entidade = '')
		and dataInicio between @dataIni and @dataFim
	group by
		cpt_marcacoes.Entidade
		,cpt_marcacoes.Entidadeno
		,cpt_marcacoes.Entidadeestab
	--having
	--	sum(case when marcacoesServ.fe = 0 then cpt_marcacoes.valor else 0 end) != 0
	
GO
Grant Execute On up_clinica_DadosParaFacturacaoEntidades to Public
Grant Control On up_clinica_DadosParaFacturacaoEntidades to Public
go


