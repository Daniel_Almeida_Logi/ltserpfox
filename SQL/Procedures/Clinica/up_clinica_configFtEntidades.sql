

-- exec up_clinica_configFtEntidades
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_clinica_configFtEntidades]') IS NOT NULL
    drop procedure up_clinica_configFtEntidades
go

create PROCEDURE up_clinica_configFtEntidades

/* WITH ENCRYPTION */
AS

	Select '1 - Agrupada por referencia' as configuracao
	union all
	Select '2 - Detalhada por marca��o' as configuracao
	union all
	Select '3 - Detalhada por marca��o c/ utente' as configuracao
	
GO
Grant Execute On up_clinica_configFtEntidades to Public
Grant Control On up_clinica_configFtEntidades to Public
go


