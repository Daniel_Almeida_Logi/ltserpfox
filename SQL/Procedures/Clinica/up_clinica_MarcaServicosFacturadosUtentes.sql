

-- exec up_clinica_MarcaServicosFacturadosUtentes 1,0,'19000101','20140601'
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_clinica_MarcaServicosFacturadosUtentes]') IS NOT NULL
    drop procedure up_clinica_MarcaServicosFacturadosUtentes
go

create PROCEDURE up_clinica_MarcaServicosFacturadosUtentes
@no	numeric(9,0)
,@estab	numeric(5,3)
,@dataIni	datetime
,@dataFim	datetime


/* WITH ENCRYPTION */
AS

	UPDATE 
		marcacoesServ
	set
		fu = 1
	from 
		marcacoesServ
		inner join marcacoes on marcacoes.mrstamp = marcacoesServ.mrstamp
		left join cpt_marcacoes on marcacoesServ.servmrstamp = cpt_marcacoes.servmrstamp
		left join cpt_val_cli on cpt_val_cli.id = cpt_marcacoes.id_cp
		left join B_utentes on cpt_val_cli.id_entidade = B_utentes.no
	where 
		dataInicio between @dataIni and @dataFim
		and marcacoes.no = @no
		and marcacoes.estab = @estab

	
GO
Grant Execute On up_clinica_MarcaServicosFacturadosUtentes to Public
Grant Control On up_clinica_MarcaServicosFacturadosUtentes to Public
go



