

-- exec up_clinica_configHonorariosUs 'ANA UNIAO','',''
-- Select * from b_us where userno = 104

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_clinica_configHonorariosUs]') IS NOT NULL
    drop procedure up_clinica_configHonorariosUs
go

create PROCEDURE up_clinica_configHonorariosUs
@especialista as varchar(150)
,@convencao as varchar(80)
,@especialidade as varchar(100)


/* WITH ENCRYPTION */
AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosEspecialidades'))
		DROP TABLE #dadosEspecialidades
		
	/* Refrencias da especialidade passada por parametro*/
	Select 
		ref 
	into
		#dadosEspecialidades
	from 
		B_cli_stRecursos 
	where 
		tipo = 'Especialidade'
		and nome = case when @especialidade = '' then nome else @especialidade end
	
	
	/**/
	declare @userno as numeric(6,0)
	set @userno = ISNULL((Select userno from b_us (nolock) where nome  = @especialista),0)
	
	select 
		distinct 
		sel = CONVERT(bit,0)
		,especialidade = convert(varchar(254),left(isnull(substring((select distinct ', '+nome AS 'data()' from b_cli_stRecursos where b_cli_stRecursos.ref = cpt_val_cli_us.ref and b_cli_stRecursos.tipo = 'Especialidade' for xml path('')),3, 255) ,''),254)) 
		,convencao = isnull(cpt_conv.descr,'SEM CONVENCAO')
		,cpt_val_cli_us.ref
		,design = isnull((select top 1 design from st (nolock) where st.ref = cpt_val_cli_us.ref),'')
		,refEntidade = isnull(cpt_val_cli.refEntidade,'')
		,designEntidade = isnull(cpt_val_cli.designEntidade,'')
		,HonorarioBi = isnull(cpt_val_cli_us.HonorarioBi,'')
		,HonorarioTipo = isnull(cpt_val_cli_us.HonorarioTipo,'')
		,HonorarioValor = isnull(cpt_val_cli_us.HonorarioValor,0)
		,HonorarioDeducao = isnull(cpt_val_cli_us.HonorarioDeducao,0)
		,id = isnull(cpt_val_cli_us.id,0)
		,id_cpt_val_cli = ISNULL(cpt_val_cli.id,'')
		,id_cpt_conv = ISNULL(cpt_val_cli.id_cpt_conv,0)
		,userno = cpt_val_cli_us.userno
	from 
		cpt_val_cli_us (nolock)
		left join cpt_val_cli (nolock) on cpt_val_cli.ref = cpt_val_cli_us.ref and cpt_val_cli.id_cpt_conv = cpt_val_cli_us.id_cpt_conv
		left join cpt_conv (nolock) on cpt_conv.id = cpt_val_cli.id_cpt_conv
	Where
		cpt_val_cli_us.userno = @userno
		and isnull(cpt_conv.descr,'SEM CONVENCAO') = case when @convencao = '' then isnull(cpt_conv.descr,'SEM CONVENCAO') else @convencao end
		and (@especialidade = '' or cpt_val_cli_us.ref in (Select ref from #dadosEspecialidades))
	Order by
		especialidade, convencao, design
			
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosEspecialidades'))
		DROP TABLE #dadosEspecialidades
GO
Grant Execute On up_clinica_configHonorariosUs to Public
Grant Control On up_clinica_configHonorariosUs to Public
go


