

-- exec up_clinica_CalculoHonorariosAgrup '19000101','20151201','','','',''
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_clinica_CalculoHonorariosAgrup]') IS NOT NULL
    drop procedure up_clinica_CalculoHonorariosAgrup
go

create PROCEDURE up_clinica_CalculoHonorariosAgrup
@dataIni	datetime
,@dataFim	datetime
,@especialista varchar(30)
,@utente	varchar(55)
,@entidade	varchar(55)
,@ref	varchar(18)


/* WITH ENCRYPTION */
AS
select 
	sel = CONVERT(bit,0)
	,stamp = left(NEWid(),25)
	,Especialista = Convert(varchar(254),RTRIM(LTRIM(marcacoesRecurso.nome)) + ' [' + CONVERT(varchar,marcacoesRecurso.no) + ']')
	,EspecialistaNome = marcacoesRecurso.nome
	,EspecialistaNo = marcacoesRecurso.no
	,ftUtente = SUM(marcacoesServ.total)
	,ftEntidade = SUM(marcacoesServ.compart)
	,ftTotal = SUM(marcacoesServ.total) + SUM(marcacoesServ.compart)
	,b_us.HonorarioBi
	,b_us.HonorarioTipo
	,b_us.HonorarioValor
	,b_us.HonorarioDeducao
	,valorHonorario = 0.00 /*Calculo Feito no fox*/
	,especialidade = isnull((Select top 1 especialidade From B_usesp where B_usesp.userno = marcacoesRecurso.no),'')
	,cedula = isnull(b_us.drcedula,'')
	,valorProcessado = 0
	,honEspPendente = case when EstadoHonEsp = 1 then CONVERT(bit,1) else CONVERT(bit,0) end
	,honEspNPagar = case when EstadoHonEsp = 2 then CONVERT(bit,1) else CONVERT(bit,0) end
	,honEspPagar = case when EstadoHonEsp = 3 then CONVERT(bit,1) else CONVERT(bit,0) end
	,honOpPendente = case when EstadoHonOp = 1 then CONVERT(bit,1) else CONVERT(bit,0) end
	,honOpNPagar = case when EstadoHonOp = 2 then CONVERT(bit,1) else CONVERT(bit,0) end
	,honOpPagar = case when EstadoHonOp = 3 then CONVERT(bit,1) else CONVERT(bit,0) end
from 
	marcacoes
	inner join marcacoesServ on marcacoes.mrstamp = marcacoesServ.mrstamp
	inner join marcacoesRecurso on marcacoesRecurso.mrstamp = marcacoes.mrstamp and marcacoesRecurso.tipo = 'Utilizador' and marcacoesServ.ref = marcacoesRecurso.ref
	inner join b_us on b_us.userno = marcacoesRecurso.no
	left join cpt_marcacoes on marcacoesServ.servmrstamp = cpt_marcacoes.servmrstamp
	left join cpt_val_cli on cpt_val_cli.id = cpt_marcacoes.id_cp
	left join B_utentes on cpt_val_cli.id_entidade = B_utentes.no
Where
	marcacoes.data  >= @dataIni and marcacoes.dataFim < @dataFim +1 /* At� ao final do dia*/
	and (isnull(marcacoesRecurso.nome,'') =  @especialista or @especialista = '')
	and (isnull(marcacoesRecurso.nome,'') =  @especialista or @especialista = '')
	and (marcacoes.nome = @utente or @utente = '')
	and (B_utentes.nome = @entidade or @entidade = '')
	and (marcacoesServ.ref = @ref or @ref = '')
	and marcacoesServ.honorario = 0
Group by 
	marcacoesRecurso.nome
	,marcacoesRecurso.no
	,b_us.HonorarioBi
	,b_us.HonorarioTipo
	,b_us.HonorarioValor
	,b_us.HonorarioDeducao
	,b_us.drcedula
	,marcacoesServ.EstadoHonEsp
	,marcacoesServ.EstadoHonOp
Order By 
	marcacoesRecurso.nome
	,marcacoesRecurso.no
	
	
GO
Grant Execute On up_clinica_CalculoHonorariosAgrup to Public
Grant Control On up_clinica_CalculoHonorariosAgrup to Public
go

 