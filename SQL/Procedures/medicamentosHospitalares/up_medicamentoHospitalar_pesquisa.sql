-- exec	up_medicamentoHospitalar_pesquisa  '20200301','20200425','',''
-- exec	up_medicamentoHospitalar_pesquisa  '','','',''
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_medicamentoHospitalar_pesquisa]') IS NOT NULL
	drop procedure dbo.up_medicamentoHospitalar_pesquisa
go


CREATE procedure dbo.up_medicamentoHospitalar_pesquisa

@Dataini		datetime,
@Datafim		datetime,
@Cliente		varchar(60),
@Administrante	varchar(60)

/* with encryption */

AS
SET NOCOUNT ON

	select sel=CAST(0 as bit),*
	From B_medicamentosHospitalares (nolock)
	Where dataDispensa between @Dataini and @DataFim
			and nomeUtente like '%'+@Cliente+'%'
			and nomeFarmaceutico like '%'+@Administrante+'%'
	order by convert(int,rtrim(no)) desc

	
Go
Grant Execute on dbo.up_medicamentoHospitalar_pesquisa to Public
Grant Control on dbo.up_medicamentoHospitalar_pesquisa to Public
Go