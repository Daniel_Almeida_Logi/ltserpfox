/* DECLARE @RunStoredProcSQL VARCHAR(1000);
				SET @RunStoredProcSQL = 'EXEC ltdev30.[dbo].up_valcartao_cl_central 210,0';
				EXEC (@RunStoredProcSQL) AT [SQLLOGITOOLS]; */

if OBJECT_ID('[dbo].[up_valcartao_cl_central]') IS NOT NULL
	drop procedure dbo.up_valcartao_cl_central
go

SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

create procedure [dbo].[up_valcartao_cl_central]
	@no numeric (20,0),
	@estab numeric (20,0),
	@nrCartao VARCHAR(50)
	
/* WITH ENCRYPTION */
AS

	select valcartao-valcartao_cativado as valcartao from B_fidel (nolock) where clno=@no and clestab=@estab and inactivo=0 and nrcartao = @nrCartao
	--(select nrcartao from b_utentes (nolock) where b_utentes.no=@no and b_utentes.estab=@estab)

GO

Grant Execute On dbo.up_valcartao_cl_central to Public
Grant Control On dbo.up_valcartao_cl_central to Public
Go


