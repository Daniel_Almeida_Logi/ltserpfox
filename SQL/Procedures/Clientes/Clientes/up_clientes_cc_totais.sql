/* SP:	up_CC_Clientes
	
	 exec	up_clientes_cc_totais 5, 0, '19000101', '2020', 0, 0, ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_clientes_cc_totais]') IS NOT NULL
	drop procedure dbo.up_clientes_cc_totais
go

create PROCEDURE [dbo].up_clientes_cc_totais

@No			NUMERIC(10),
@estab		NUMERIC(4),
@dataIni	datetime,
@dataFim	datetime,
@modo		bit,
@verTodos	bit,
@loja		varchar(20)

/* with encryption */
AS
SET NOCOUNT ON


IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosCc'))
	DROP TABLE #dadosCc

select 
	datalc=''
	,ousrhora=''
	,dataven=''
	,serie=''
	,cmdesc=''
	,nrdoc=0
	,edeb=0
	,edebf=0
	,ecred=0
	,ecredf=0
	,obs=''
	,moeda=''
	,ultdoc=''
	,intid=0
	,cbbno=0
	,username = ''
	,ccstamp = ''
	,no = b_utentes.no
	,nome = b_utentes.nome
	,facstamp=''
	,verdoc=0
	,ousrinis=''
	--,saldoanterior = (select sum(a.edeb)- sum(a.ecred) from cc a (nolock) where b_utentes.no = a.no and a.estab = (case when @verTodos=1 then b_utentes.estab else @estab end) and a.datalc < @Dataini)
	--,edebanterior = (select sum(a.edeb) from cc a (nolock) where b_utentes.no = a.no and a.estab = (case when @verTodos=1 then b_utentes.estab else @estab end) and a.datalc < @Dataini)
	--,ecredanterior = (select sum(a.ecred) from cc a (nolock) where b_utentes.no = a.no and a.estab = (case when @verTodos=1 then b_utentes.estab else @estab end) and a.datalc < @Dataini)
	--,saldoperiodo =(select sum(a.edeb)- sum(a.ecred) from cc a (nolock) where b_utentes.no = a.no and a.estab = (case when @verTodos=1 then b_utentes.estab else @estab end) and a.datalc between @Dataini and @Datafim)
	--,site=''
--	,saldo_total = 0
	,edeb_anterior = case when cc.datalc < @Dataini then isnull(edeb,0) else 0 end
	,ecred_anterior = case when cc.datalc < @Dataini then isnull(ecred,0) else 0 end
	,edeb_total = isnull(edeb,0)
	,ecred_total = isnull(ecred,0)
--	,saldo_periodo = 0
	,site=''
	,dep=0
	,numero=0
into
	#dadosCc
From
	b_utentes (nolock)
	left join cc (nolock) on b_utentes.no = cc.no and b_utentes.estab = cc.estab
Where
	b_utentes.no = case when @no = 0 then b_utentes.no else @no end
	and b_utentes.estab = (case when @verTodos=1 then b_utentes.estab else @estab end)


union all


Select	
	datalc = convert(varchar,cc.datalc,102)
	,ousrhora = cc.ousrhora
	,dataven	= convert(varchar,cc.dataven,102)
	,serie = ISNULL(re.nmdoc,' ')
	,cc.cmdesc
	,cc.nrdoc
	,cc.edeb
	,cc.edebf
	,cc.ecred
	,cc.ecredf
	,cc.obs
	,cc.moeda
	,cc.ultdoc
	,cc.intid
	,cc.cbbno 
	,username = isnull(b_us.nome,'')
	,cc.ccstamp
	,cc.no
	,cc.nome
	,cc.faccstamp
	,verdoc = CONVERT(bit,0)
	,cc.ousrinis
	,edeb_anterior = 0
	,ecred_anterior = 0
	,edeb_total = 0
	,ecred_total = 0
	,site = isnull(ft.site,re.site)
	,dep = cc.estab
	,numero = ROW_NUMBER() over(order by cc.datalc,cc.ousrhora, cc.cm, cc.nrdoc asc)
From
	b_utentes (nolock)
	left join cc (nolock) on b_utentes.no = cc.no and b_utentes.estab = cc.estab
	left join RE (nolock) ON cc.restamp = re.restamp
	left join b_us on b_us.iniciais = cc.ousrinis
	left join ft (nolock) on cc.ftstamp = ft.ftstamp
WHERE
	cc.no=case when @no = 0 then cc.no else @no end
	And cc.estab=(case when @verTodos=1 then cc.estab else @estab end)
	and cc.datalc between @Dataini and @Datafim
	and (
		ft.site = case when @loja = '' then ft.site else @loja end
		or
		re.site = case when @loja = '' then re.site else @loja end
		or 
		(ft.site is null and re.site is null)
	)



Select 
	credito_Total = SUM(ecred_total)
	,debito_Total = SUM(edeb_total)
	--,saldo_Total = SUM(edeb_total - ecred_total)
	,saldo_Total = (select sum((edeb-edebf)-(ecred-ecredf)) from cc where no=(case when @no = 0 then cc.no else @no end) and estab=(case when @verTodos=1 then cc.estab else @estab end))
	-- periodo
	,credito_periodo = SUM(ecred)
	,debito_periodo = SUM(edeb)
	,saldo_periodo = SUM(edeb) - SUM(ecred)
	

	,credito_anterior = SUM(ecred_anterior)
	,debito_anterior = SUM(edeb_anterior)
	,saldo_anterior = SUM(edeb_anterior - ecred_anterior)

from 
	#dadosCc

	

Go
Grant Execute on dbo.up_clientes_cc_totais to Public
Grant Control on dbo.up_clientes_cc_totais to Public
Go	