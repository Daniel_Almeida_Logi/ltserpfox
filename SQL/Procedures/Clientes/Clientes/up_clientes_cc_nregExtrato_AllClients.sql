
/****** Object:  StoredProcedure [dbo].[up_clientes_cc_nregExtrato_AllClients]   

exec up_clientes_cc_nregExtrato_AllClients 1,'','Loja 1'

Script Date: 18/12/2024 12:59:02 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_clientes_cc_nregExtrato_AllClients]') IS NOT NULL
	drop procedure dbo.[up_clientes_cc_nregExtrato_AllClients]
go

CREATE procedure [dbo].[up_clientes_cc_nregExtrato_AllClients]

		@modo		bit,
		@verTodos	int,
		@site		varchar(20)


/* with encryption */
AS
SET NOCOUNT ON

;with
	cteUser as
	(
		select 
			distinct username
			,iniciais
		from 
			b_us
	), cteEstabs as (
		Select 
			estab
		From
			B_utentes
	)

Select	'SALDO' = --ISNULL(SUM(VALOR),0),
				Case when @modo=1
				then
					isnull(
							(
								Select sum((cc.edeb-cc.edebf)-(cc.ecred-cc.ecredf)) 
								From	(
									SELECT	cc.ccstamp, ROW_NUMBER() over(order by cc.datalc,cc.ousrhora, cc.cm, cc.nrdoc) as numero, edeb, edebf, ecred,ecredf, valor
									FROM	cc (nolock)
									left join ft (nolock) on cc.ftstamp=ft.ftstamp
									WHERE	cc.no=cc.no
											and cc.no > 199
											And cc.estab=cc.estab
											And (abs((cc.ecred-cc.ecredf)-(cc.edeb-cc.edebf)) > 0.010000)
											and ft.anulado=0
							) a inner join cc (nolock) on cc.ccstamp = a.ccstamp
							
					),0)
				else
					0
				end,
		*
From	(	
		select 	'DATALC'		= convert(varchar,cc.datalc,102),
				'DATAVEN'		= convert(varchar,cc.dataven,102),
				'EDEB'			= cc.edeb,
				'ECRED'			= cc.ecred,
				'EDEBF'			= cc.edebf,
				'ECREDF'		= cc.ecredf,
				'VALOR'			= (cc.edeb-cc.edebf)-(cc.ecred-cc.ecredf),
				'MOEDA'			= cc.moeda,
				'CMDESC'		= cc.cmdesc,
				'NRDOC'			= cc.nrdoc,
				'ULTDOC'		= cc.ultdoc,
				'INTID'			= cc.intid,
				'FACSTAMP'		= cc.faccstamp,
				'CBBNO'			= cc.cbbno,
				'ORIGEM'		= cc.origem,
				'OUSRHORA'		= cc.ousrhora,
				'OUSRINIS'		= cc.ousrinis,
				'IEMISSAO'		= datediff(d,cc.datalc,GETDATE()),
				'IVENCIMENTO'	= datediff(d,cc.datalc,GETDATE()),
				'USERNAME'		= isnull(cteUser.username,''),
				'NO'			= cc.no,
				'NO.ESTAB'		= CONCAT(cc.no,".",cc.estab),
				'NOME'			= cc.nome,
				'SITE'			= isnull(ft.site,re.site)
		from
			cc (nolock)
			left join ft (nolock) on cc.ftstamp=ft.ftstamp
			left join cteUser on cteUser.iniciais = cc.ousrinis
			left join re (nolock) on re.restamp = cc.restamp
		where
			(abs((cc.ecred-cc.ecredf)-(cc.edeb-cc.edebf)) > 0.010000)

			/*and ft.anulado is not null */
			and (
				ft.site = case when @site = '' then ft.site else @site end
				or
				re.site = case when @site = '' then re.site else @site end
				or 
				(ft.site is null and re.site is null)
		
			)
		
) b
order by nome



GO
GRANT EXECUTE on dbo.[up_clientes_cc_nregExtrato_AllClients] TO PUBLIC
GRANT Control on dbo.[up_clientes_cc_nregExtrato_AllClients] TO PUBLIC
GO



