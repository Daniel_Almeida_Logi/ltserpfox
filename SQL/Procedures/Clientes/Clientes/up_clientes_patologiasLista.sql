-- Lista de Patologias
-- exec up_clientes_patologiasLista
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_clientes_patologiasLista]') IS NOT NULL
    drop procedure dbo.up_clientes_patologiasLista
go

create procedure dbo.up_clientes_patologiasLista

/* WITH ENCRYPTION */
AS

select 
	patol_ID = ctiefpstamp
	,descricao
from
	b_ctidef (nolock)
order by
	descricao

GO
Grant Execute on dbo.up_clientes_patologiasLista to Public
Grant Control on dbo.up_clientes_patologiasLista to Public
go