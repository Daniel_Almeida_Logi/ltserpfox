-- Patologias do cliente
-- exec up_clientes_patologias 'ADM183793FA-0C21-4D4F-817'
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_clientes_patologias]') IS NOT NULL
    drop procedure dbo.up_clientes_patologias
go

create procedure dbo.up_clientes_patologias
	@stamp varchar(25)

/* WITH ENCRYPTION */
AS

select 
	patol_id = B_patologias.ctiefpstamp
	,descricao
	,patostamp
from
	b_patologias (nolock)
	inner join b_ctidef (nolock) on B_patologias.patol_id = B_ctidef.ctiefpstamp
where 
	ostamp = @stamp
order by
	descricao

option (optimize  for (@stamp UNKNOWN))

GO
Grant Execute on dbo.up_clientes_patologias to Public
Grant Control on dbo.up_clientes_patologias to Public
go