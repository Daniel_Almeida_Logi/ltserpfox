

if OBJECT_ID('[dbo].[up_saldocc_cl_central]') IS NOT NULL
	drop procedure dbo.up_saldocc_cl_central
go

SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

create procedure [dbo].[up_saldocc_cl_central]
	@no numeric (20,0),
	@estab numeric (20,0)
/* WITH ENCRYPTION */
AS
	;with cte1 as (
		select SUM(case when nmdoc='Factura' then etotal else etotal*(-1) end)  as div,
		(select SUM(erec) 
			from rl (nolock) 
			inner join cc (nolock) on cc.ccstamp=rl.ccstamp 
			where cc.ftstamp=ft.ftstamp) 
		as rec 
	from ft (nolock) 
	where nmdoc in ('Factura','Nota de Cr�dito') and ftstamp in (select regstamp from table_sync_status (nolock) where [table]='FT' and sync=0) and no=@no and estab=@estab
	group by ftstamp
	)
	select isnull(SUM(div-rec),0) as totalsaldo from cte1
	--select isnull(SUM(case when nmdoc='Factura' then etotal else etotal*(-1) end),0) as totalsaldo 
	--from ft (nolock) 
	--where nmdoc in ('Factura','Nota de Cr�dito') and ftstamp in (select regstamp from table_sync_status (nolock) where [table]='FT' and sync=0) and no=@no and estab=@estab
GO


