/* Pesquisa de Clientes

				exec up_clientes_pesquisa 5000, '', 0, -1, 'avenida', ''
														,'', '', ''
														,'', 0, '>', -999999, '', 0, ''
														,'', '', '', '', '', 0
														,'', '', '', 0,0, '', '', ''

 exec up_clientes_pesquisa 10, '', 0, -1, 'avenida', '', '', '', '', '', 0, '', '', '', 0, '', '', '', '', '', '19000101', 0, '', '', '', 0, 0,'', ''

exec up_clientes_pesquisa 999999, '', 0, -1, '', ''
					,'', '', ''
					,'', 0, '>', -999999, '', 0, ''
					,'', '', '20010101', '20230512', '19000101', 0
					,'', '', '', 0,0, '', '', '', '', '', 0, 0

*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_clientes_pesquisa]') IS NOT NULL
    drop procedure dbo.up_clientes_pesquisa
go

create procedure dbo.up_clientes_pesquisa

@top				INT
,@nome				VARCHAR(55)
,@no				NUMERIC(9)
,@estab				NUMERIC(5)
,@morada			VARCHAR(55)
,@ncont				VARCHAR(20)
,@ncartao			VARCHAR(30)
,@tipo				VARCHAR(20)
,@Profissao			VARCHAR(60)
,@sexo				VARCHAR(10)
,@tlmvl				BIT
,@operador			CHAR(1)
,@idade				INT
,@obs				VARCHAR(MAX)
,@mail				BIT
,@entPla			VARCHAR(50)
,@patologia			VARCHAR(25)
,@ref				VARCHAR(MAX)
,@Entre				DATETIME
,@E					DATETIME
,@dtnasc			DATETIME
,@inactivo			BIT
,@tlf				VARCHAR(60)
,@nbenef			VARCHAR(100)
,@nbenef2			VARCHAR(100)
,@touch				BIT
,@entCompart		BIT
,@id				VARCHAR(20)
,@marca				VARCHAR(200) = ''
,@bino				VARCHAR(20) = ''
,@email				VARCHAR(45) = ''
,@codPost			VARCHAR(45) = ''
,@autorizaEmail		BIT = 0
,@autorizaSms		BIT = 0
,@pim				BIT = 0
,@pemh				BIT = 0

/* WITH ENCRYPTION */
AS

/* guardar valores para idade*/
declare @maiorQue int, @menorQue int
set @maiorQue = case
					when @operador = '>' or @operador = '='
					then @idade
					else -999999
					--else case when @idade != 0 then -115 else -999999 end
				end
set @menorQue = case
					when @operador = '=' or @operador = '<'
					then @idade
					else 999999
					--else case when @idade != 0 then 115 else 999999 end
				end

If OBJECT_ID('tempdb.dbo.#filtroRef') IS NOT NULL
	drop table #filtroRef

IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#emailTelmTemp'))
	DROP TABLE #emailTelmTemp

CREATE TABLE #emailTelmTemp(
	estado varchar(100)
)

INSERT INTO  #emailTelmTemp(estado)
values(	's/mail'),('s/tlm'),('' )

	SELECT 
		valueText,
		site
	INTO
		#filtroRef
	FROM
		tempFilters(nolock)
	WHERE
		token = @ref
	
if @top=0
begin 

		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosclientes'))
			DROP TABLE #dadosclientes1

		SELECT
			 Sel = CONVERT(bit,0)
			,(case when b_utentes.removido=0 then b_utentes.nome else '*********' end) as nome
			,b_utentes.no
			,b_utentes.estab
			,(case when b_utentes.removido=0 then b_utentes.tlmvl else '*********' end) as tlmvl
			,b_utentes.autoriza_sms
			,(case when b_utentes.removido=0 then b_utentes.telefone else '*********' end) as telefone
			,(case when b_utentes.removido=0 then b_utentes.nrcartao else '*********' end) as nrcartao
			,(case when b_utentes.removido=0 then b_utentes.nbenef else '*********' end) as nbenef
			,(case when b_utentes.removido=0 then b_utentes.nbenef2 else '*********' end) as nbenef2
			,idade = case when year(nascimento) != 1900 and b_utentes.removido=0 then (CONVERT(int,CONVERT(char(8),GETDATE(),112))-CONVERT(char(8),nascimento,112))/10000 else 0 end
			,b_utentes.inactivo
			,b_utentes.nascimento
			,b_utentes.utstamp
			,b_utentes.morada
			,b_utentes.local
			,b_utentes.codpost
			,(case when b_utentes.removido=0 then b_utentes.ncont else '*********' end) as ncont
			,b_utentes.codigop
			,b_utentes.tipo
			,b_utentes.ccusto
			,b_utentes.zona
			,b_utentes.desconto
			,b_utentes.nocredit
			,b_utentes.modofact
			,b_utentes.entfact
			,b_utentes.profi
			,b_utentes.sexo
			,b_utentes.obs
			,b_utentes.email
			,b_utentes.autoriza_emails
			,b_utentes.entpla
			,b_utentes.tabiva
			,row = ROW_NUMBER() over(partition by b_utentes.utstamp order by b_utentes.utstamp)
			,clinica
			,b_utentes.entCompart
			,b_utentes.id
			,binovalidade = '19000101'
			,autorizado
			,removido
			,b_utentes.no_ext
			,b_utentes.bino
		into 
			#dadosclientes1
		from
			b_utentes (nolock)	
		where
			0=1

			select * from #dadosclientes1
end
else
begin

	if @ref = '' AND @marca = '' AND @Entre ='19000101' AND @E ='30000101'
	begin

		declare @sql varchar(max)
		declare @sqlwhere varchar(max)
		
		select @sql = N' SELECT top('+ convert(varchar,@top)  +')
						Sel = CONVERT(bit,0)
						 ,(case when b_utentes.removido=0 then b_utentes.nome else ''*********'' end) as nome
						 ,b_utentes.no
						,b_utentes.estab
						,(case when b_utentes.removido=0 then b_utentes.tlmvl else ''*********'' end) as tlmvl
						,b_utentes.autoriza_sms
						,(case when b_utentes.removido=0 then b_utentes.telefone else ''*********'' end) as telefone
						,(case when b_utentes.removido=0 then b_utentes.nrcartao else ''*********'' end) as nrcartao
						,(case when b_utentes.removido=0 then b_utentes.nbenef else ''*********'' end) as nbenef
						,(case when b_utentes.removido=0 then b_utentes.nbenef2 else ''*********'' end) as nbenef2
						,idade = case when year(nascimento) != 1900 and b_utentes.removido=0 then (CONVERT(int,CONVERT(char(8),GETDATE(),112))-CONVERT(char(8),nascimento,112))/10000 else 0 end
						,b_utentes.inactivo
						,b_utentes.nascimento
						,b_utentes.utstamp
						,b_utentes.morada
						,b_utentes.local
						,b_utentes.codpost
						,(case when b_utentes.removido=0 then b_utentes.ncont else ''*********'' end) as ncont
						,b_utentes.codigop
						,b_utentes.tipo
						,b_utentes.ccusto
						,b_utentes.zona
						,b_utentes.desconto
						,b_utentes.nocredit
						,b_utentes.modofact
						,b_utentes.entfact
						,b_utentes.profi
						,b_utentes.sexo
						,b_utentes.obs
						,b_utentes.email
						,b_utentes.autoriza_emails
						,b_utentes.entpla
						,b_utentes.tabiva
						,row = ROW_NUMBER() over(partition by b_utentes.utstamp order by b_utentes.utstamp)
						,clinica
						,b_utentes.entCompart
						,b_utentes.id
						,binovalidade = ''19000101''
						,autorizado
						,removido
						,b_utentes.no_ext
						,b_utentes.bino
						, ncartao = ''''
				from
					b_utentes (nolock) '
				if @patologia!=''
					select @sql = @sql + N'LEFT JOIN b_utentes_patologias(NOLOCK) ON b_utentes_patologias.utstamp = b_utentes.utstamp
										 LEFT JOIN B_ctidef(NOLOCK) on b_utentes_patologias.ctiefpstamp = B_ctidef.ctiefpstamp'
			select @sqlwhere =' WHERE 
					b_utentes.estab = case when ' + convert(varchar,@estab) +'>= 0 then ' + convert(varchar,@estab) + ' else b_utentes.estab end
			 '  

		if @ncont != ''
				select @sqlwhere = @sqlwhere + N'
						and b_utentes.ncont = '''+@ncont+''''
		if @no != 0
		select @sqlwhere = @sqlwhere + N'
			AND b_utentes.NO = '+convert(varchar,@no)


		if @entCompart != ''
				select @sqlwhere = @sqlwhere + N'
						and b_utentes.entCompart = '''+convert(varchar,@entCompart)+''''

		if @email != ''		
		select @sqlwhere = @sqlwhere + N'
				and b_utentes.email LIKE ''%'+@email+ '%'''

		if  @mail=1
		select @sqlwhere = @sqlwhere + N'
			and len(isnull(b_utentes.email,'''')) >= 1'

		IF @autorizaEmail=1
			select @sqlwhere = @sqlwhere + N'
				and b_utentes.autoriza_emails =  '''+convert(varchar,@autorizaEmail)+'''
				and b_utentes.autorizado = 1'

		IF @autorizaSms = 1
			select @sqlwhere = @sqlwhere + N'
				and b_utentes.autoriza_sms =  '''+convert(varchar,@autorizaSms)+'''
				and b_utentes.autorizado = 1'

		IF @pim = 1
			select @sqlwhere = @sqlwhere + N'
				and b_utentes.pim =  '''+convert(varchar,@pim)+'''
				and b_utentes.pim = 1'

		IF @pemh = 1
			select @sqlwhere = @sqlwhere + N'
				and b_utentes.pemh =  '''+convert(varchar,@pemh)+'''
				and b_utentes.pemh = 1'

		if @nome != ''		
		select @sqlwhere = @sqlwhere + N'
		AND (nome like ''%' + @nome + '%''' + 'or cast(no as varchar(10))+''.''+cast(estab as varchar(10))=''' + @nome + ''' or cast(no as varchar(10))='''+
		@nome + ''' or id='''+ @nome + ''' or nrcartao='''+ @nome + ''' or no_ext='''+ @nome + ''')'

	
		if @morada != ''		
		select @sqlwhere = @sqlwhere + N'
				and b_utentes.morada LIKE ''%'+@morada+ '%'''

		if @sexo not in('','M/F')
		select @sqlwhere = @sqlwhere + N'
			AND b_utentes.sexo = '''+@sexo+''''

		if @obs != ''
		select @sqlwhere = @sqlwhere + N'
			AND convert(varchar,b_utentes.obs) LIKE ''%'+convert(varchar,@obs)+ '%'''

		if @entpla != ''
		select @sqlwhere = @sqlwhere + N'
			AND b_utentes.entpla = '''+@entpla+''''

		if @dtnasc != '19000101'
		select @sqlwhere = @sqlwhere + N'
			AND b_utentes.nascimento = '''+convert(varchar,@dtnasc) +''''

		if @inactivo != 1
		select @sqlwhere = @sqlwhere + N'
			AND b_utentes.inactivo = '''+convert(varchar,@inactivo)+''''


		if @tlf != ''
				select @sqlwhere = @sqlwhere + N'
						and (b_utentes.tlmvl = '''+@tlf+''' or  b_utentes.telefone = '''+@tlf+''')' 

		if @tlmvl=1
				select @sqlwhere = @sqlwhere + N'
					and (len(isnull(b_utentes.tlmvl,'''')) >= 1 or len(isnull(b_utentes.telefone,'''')) >= 1)'

		if @nbenef != ''
				select @sqlwhere = @sqlwhere + N'
						and b_utentes.nbenef = '''+@nbenef+''''
			if @nbenef2 != ''
				select @sqlwhere = @sqlwhere + N'
						and b_utentes.nbenef2 = '''+@nbenef2+''''			

						---- idade 
		if (@idade != -999999 or  @idade != 999999 ) and @operador !=''
		select @sqlwhere = @sqlwhere + N'
			AND ( ((DateDiff(year, nascimento, getdate()) - (case when MONTH(nascimento)> MONTH(getdate()) then 1 else 0 end))
		 - (case when  MONTH(nascimento)= MONTH(getdate()) AND DAY(nascimento)> DAY(getdate()) then 1 else 0 end)) )'+ @operador +convert(varchar,@idade) 

		 if (@idade != -999999 and  @idade != 999999  and @idade!='') and @operador !=''
			select @sqlwhere = @sqlwhere + N' and year(nascimento) !=''1900'''

		if @touch = 1
				select @sqlwhere = @sqlwhere + N'
						and b_utentes.no >=200 '

		if @tipo != ''
		select @sqlwhere = @sqlwhere + N'
			AND b_utentes.tipo = '''+@tipo+''''

		if @profissao != ''
		select @sqlwhere = @sqlwhere + N'
			AND b_utentes.profi LIKE ''%'+@Profissao+ '%'''

		if @bino != ''
		select @sqlwhere = @sqlwhere + N'
			AND b_utentes.bino LIKE ''%'+@bino+ '%'''


		if @codPost != ''
		select @sqlwhere = @sqlwhere + N'
			AND b_utentes.codpost = '''+@codPost+''''

		if @patologia != ''
		select @sqlwhere = @sqlwhere + N'
			AND convert(varchar,B_ctidef.descricao) LIKE ''%'+@patologia+ '%'''


		select @sqlwhere = @sqlwhere + N'	order by no, estab '
			

			print @sql + @sqlwhere
			exec (@sql + @sqlwhere)

	end
	else
	begin
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosclientes2'))
			DROP TABLE #dadosclientes2

			SELECT
				 Sel = CONVERT(bit,0)
				 ,'stamp' = CONVERT(varchar(255), NEWID() )  
				,st.ref
				,(case when b_utentes.removido=0 then b_utentes.nome else '*********' end) as nome
				,b_utentes.no
				,b_utentes.estab
				,(case when b_utentes.removido=0 then b_utentes.tlmvl else '*********' end) as tlmvl
				,b_utentes.autoriza_sms
				,(case when b_utentes.removido=0 then b_utentes.telefone else '*********' end) as telefone
				,(case when b_utentes.removido=0 then b_utentes.nrcartao else '*********' end) as nrcartao
				,(case when b_utentes.removido=0 then b_utentes.nbenef else '*********' end) as nbenef
				,(case when b_utentes.removido=0 then b_utentes.nbenef2 else '*********' end) as nbenef2
				,idade = case when year(nascimento) != 1900 and b_utentes.removido=0 then (CONVERT(int,CONVERT(char(8),GETDATE(),112))-CONVERT(char(8),nascimento,112))/10000 else 0 end
				,b_utentes.inactivo
				,b_utentes.nascimento
				,b_utentes.utstamp
				,b_utentes.morada
				,b_utentes.local
				,b_utentes.codpost
				,(case when b_utentes.removido=0 then b_utentes.ncont else '*********' end) as ncont
				,b_utentes.codigop
				,b_utentes.tipo
				,b_utentes.ccusto
				,b_utentes.zona
				,b_utentes.desconto
				,b_utentes.nocredit
				,b_utentes.modofact
				,b_utentes.entfact
				,b_utentes.profi
				,b_utentes.sexo
				,b_utentes.obs
				,b_utentes.email
				,b_utentes.autoriza_emails
				,b_utentes.entpla
				,b_utentes.tabiva
				,b_utentes.clinica
				,b_utentes.entCompart
				,b_utentes.id
				,binovalidade = '19000101'
				,autorizado
				,removido
				,b_utentes.no_ext
				,b_utentes.bino	
			into 
				#dadosCLIENTEs2
			from
				b_utentes (nolock)	
				inner join ft (nolock) on ft.no=b_utentes.no and ft.estab=b_utentes.estab
				inner join fi (nolock) on ft.ftstamp = fi.ftstamp
				inner join td (nolock) on ft.ndoc = td.ndoc 
				inner join st (nolock) on st.ref=fi.ref
				LEFT JOIN b_utentes_patologias(NOLOCK) ON b_utentes_patologias.utstamp = b_utentes.utstamp
				LEFT JOIN B_ctidef(NOLOCK) on b_utentes_patologias.ctiefpstamp = B_ctidef.ctiefpstamp
			where
				(fi.ref in (Select valueText from #filtroRef) OR @ref='')
				and	st.usr1 = case when @marca = '' then st.usr1 else @marca end
				and ft.fdata between @Entre and @E
				and ft.estab = case when @estab >= 0 then @estab else ft.estab end
				and ft.no = case when @no > 0 then @no else ft.no end
				and (B_utentes.entCompart = @entCompart or @entCompart = 0)
				and td.tipodoc = 1
				and len(isnull(b_utentes.email,'')) >= case when @mail=1 then  1 else 0 end  
				and (len(isnull(b_utentes.tlmvl,'')) >= (case when @tlmvl=1 then 1 else 0 end ) or len(isnull(b_utentes.telefone,'')) >= (case when @tlmvl=1 then 1 else 0 end ))
				and ft.no > 200
				and b_utentes.email like (case when @email = '' then b_utentes.email else  '%' + RTRIM(LTRIM(@email)) + '%' end)
				AND b_utentes.autoriza_emails = CASE WHEN @autorizaEmail=1  THEN 1 ELSE  b_utentes.autoriza_emails END
				AND b_utentes.autoriza_sms = CASE WHEN @autorizaSms = 1 THEN 1 ELSE b_utentes.autoriza_sms END
				AND b_utentes.pim = CASE WHEN @pim = 1 THEN 1 ELSE b_utentes.pim END
				AND b_utentes.pemh = CASE WHEN @pemh = 1 THEN 1 ELSE b_utentes.pemh END
				AND b_utentes.autorizado =  CASE WHEN (@autorizaSms = 1 OR @autorizaEmail=1 ) THEN 1 ELSE b_utentes.autorizado END
				AND isnull(B_ctidef.descricao,'') like (case when @patologia='' then '%' else @patologia end)
			order by no,estab	
		
			CREATE CLUSTERED INDEX IDX_dadosCLIENTEs_stamp ON #dadosCLIENTEs2(stamp)
			CREATE NONCLUSTERED INDEX IDX_dadosCLIENTEs2_ref  ON #dadosCLIENTEs2(ref)

			if @marca= '' AND @ref != ''
			begin
				delete from #dadosCLIENTEs2 where ref not in (Select valueText from #filtroRef)
			end

			DELETE FROM #dadosCLIENTEs2 WHERE stamp IN (
				SELECT stamp FROM (
					SELECT 
						stamp
						,ROW_NUMBER() OVER (PARTITION BY utstamp ORDER BY no) AS [ItemNumber]
					FROM 
						#dadosCLIENTEs2
				) a WHERE ItemNumber > 1
			)

			select  top(@top) a.*, ncartao = '' 
			from 
				#dadosclientes2 as a
			where
				(nome like '%' + @nome + '%' or cast(no as varchar(10))+'.'+cast(estab as varchar(10))=@nome or cast(no as varchar(10))=@nome or id=@nome or nrcartao=@nome or no_ext=@nome)
				and morada like '%' + @morada + '%'
				and ncont = case when @ncont = '' then ncont else @ncont end
				and sexo = case when @sexo not in('','M/F') then @sexo else sexo end
				and obs like '%' + @obs + '%'
				and entpla like '%' + @entPla + '%'
				and nascimento = (case when @dtnasc = '19000101' then nascimento else @dtnasc end)
				and inactivo = CASE when @inactivo = 1 THEN inactivo else @inactivo END
				and (
					tlmvl = case when @tlf = '' then tlmvl else @tlf end
					or 
					telefone = case when @tlf = '' then telefone else @tlf end
				)
				and nbenef = case when @nbenef = '' then nbenef else @nbenef end
				and nbenef2 = case when @nbenef2 = '' then nbenef2 else @nbenef2 end   
				and (DateDiff(year, nascimento, getdate()) - (case when MONTH(nascimento)> MONTH(getdate()) then 1 else 0 end))
					- (case when  MONTH(nascimento)= MONTH(getdate()) AND DAY(nascimento)> DAY(getdate()) then 1 else 0 end)   between @maiorQue and @menorQue
				and no >= case when @touch = 1 then 200 else 0 end
				and tipo = case when @tipo='' then tipo else @tipo end
				and profi like '%' + @profissao + '%'
				and bino like '%' + @bino + '%'
				and codpost = (case when @codPost <> '' then @codPost else codpost end)
			order by no, estab

		
		end
end 

IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#emailTelmTemp'))
	DROP TABLE #emailTelmTemp

If OBJECT_ID('tempdb.dbo.#filtroRef') IS NOT NULL
	drop table #filtroRef

GO
Grant Execute on dbo.up_clientes_pesquisa to Public
Grant Control on dbo.up_clientes_pesquisa to Public
go