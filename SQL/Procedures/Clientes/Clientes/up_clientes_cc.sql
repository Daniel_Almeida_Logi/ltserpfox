
/****** Object:  StoredProcedure [dbo].[up_clientes_cc]    Script Date: 21/06/2019 10:30:27 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_clientes_cc]') IS NOT NULL
	drop procedure dbo.up_clientes_cc
go


Create PROCEDURE [dbo].[up_clientes_cc]

@no			NUMERIC(10),
@estab		NUMERIC(4),
@dataIni	datetime,
@dataFim	datetime,
@modo		bit,
@verTodos	bit,
@loja		varchar(20)

/* with encryption */
AS
SET NOCOUNT ON


IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosCc'))
	DROP TABLE #dadosCc

	select 
		datalc=''
		,ousrhora=''
		,dataven=''
		,serie=''
		,cmdesc=''
		,nrdoc=0
		,edeb=0
		,ecred=0
		,obs=''
		,moeda=''
		,ultdoc=''
		,intid=0
		,cbbno=0
		,username = ''
		,ccstamp = ''
		,no = b_utentes.no
		,nome = b_utentes.nome
		,facstamp=''
		,verdoc=0
		,ousrinis=''
		,saldoanterior = (select sum(a.edeb)- sum(a.ecred) from cc a (nolock) where b_utentes.no = a.no and a.estab = (case when @verTodos=1 then b_utentes.estab else @estab end) and a.datalc < @Dataini)
		,edebanterior = (select sum(a.edeb) from cc a (nolock) where b_utentes.no = a.no and a.estab = (case when @verTodos=1 then b_utentes.estab else @estab end) and a.datalc < @Dataini)
		,ecredanterior = (select sum(a.ecred) from cc a (nolock) where b_utentes.no = a.no and a.estab = (case when @verTodos=1 then b_utentes.estab else @estab end) and a.datalc < @Dataini)
		,saldoperiodo =(select sum(a.edeb)- sum(a.ecred) from cc a (nolock) where b_utentes.no = a.no and a.estab = (case when @verTodos=1 then b_utentes.estab else @estab end) and a.datalc between @Dataini and @Datafim)
		,site=''
		,dep=0
		,numero=0
		,suspensa=''
	into
		#dadosCc
	From
		b_utentes (nolock)
	Where
		b_utentes.no  = @no
		and b_utentes.estab = 0

	union all

	Select	
		datalc = convert(varchar,cc.datalc,102)
		,ousrhora = cc.ousrhora
		,dataven	= convert(varchar,cc.dataven,102)
		,serie = ISNULL(re.nmdoc,' ')
		,cc.cmdesc
		,cc.nrdoc
		,cc.edeb
		,cc.ecred
		,cc.obs
		,cc.moeda
		,cc.ultdoc
		,cc.intid
		,cc.cbbno 
		,username = isnull(b_us.nome,'')
		,cc.ccstamp
		,cc.no
		,cc.nome
		,cc.faccstamp
		,verdoc = CONVERT(bit,0)
		,cc.ousrinis
		,saldoanterior = 0
		,edebanterior = 0
		,ecredanterior = 0
		,saldoperiodo = 0
		,site = isnull(ft.site,re.site)
		,dep = cc.estab
		,numero = ROW_NUMBER() over(order by cc.datalc,cc.ousrhora, cc.cm, cc.nrdoc asc)
		,suspensa= (case when ft.cobrado=1 then 'Vd. Susp.)' else ft.nmdoc end)
	From
		b_utentes (nolock)
		left join cc (nolock) on b_utentes.no = cc.no and b_utentes.estab = cc.estab
		left join RE (nolock) ON cc.restamp = re.restamp
		left join b_us on b_us.iniciais = cc.ousrinis
		left join ft (nolock) on cc.ftstamp = ft.ftstamp
	WHERE
		cc.no = @no
		And cc.estab=(case when @verTodos=1 then cc.estab else @estab end)
		and cc.datalc between @Dataini and @Datafim
		and (
			ft.site = case when @loja = '' then ft.site else @loja end
			or
			re.site = case when @loja = '' then re.site else @loja end
			or 
			(ft.site is null and re.site is null)
		)


	Select 
		saldo = (select (SUM(edebanterior) + SUM(edeb)) - (SUM(ecredanterior) + SUM(case when ecred<0 then ecred*(-1) else ecred end)) from #dadosCc a where a.numero <= #dadosCc.numero)
		,* 
	from 
		#dadosCc
	order by
		numero
		
		
Go
Grant Execute on dbo.up_clientes_cc to Public
Grant Control on dbo.up_clientes_cc to Public
Go
	

