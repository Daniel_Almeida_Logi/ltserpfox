/*
	Cria ficha de cliente se n�o existir, caso contr�rio, atualiza a ficha

	exec up_clientes_CartaoCidadao_insertUpdate 
		'176533582', 1

	scripts:
		select * from b_utentes where nbenef='176533582'
		select bool from b_parameters where stamp='ADM0000000259'
		update b_parameters set bool=1 where stamp='ADM0000000259'

	Notas:
		. O cliente � criado com base na leitura do cart�o de cidad�o - tabela: dispensa_eletronica_cc
		. Cria cart�o de cliente se definidos nos parametros 'ADM0000000021'
		. Se existir mais que uma ficha com o mesmo nr Benef, nada � executado						
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_clientes_CartaoCidadao_insertUpdate]') IS NOT NULL
	drop procedure dbo.up_clientes_CartaoCidadao_insertUpdate
go

create procedure dbo.up_clientes_CartaoCidadao_insertUpdate 
	@nrSNS int
	,@usr numeric(5)

/* WITH ENCRYPTION */
AS
	SET NOCOUNT ON
	
	if (select bool from b_parameters (nolock) where stamp='ADM0000000259') = 0
		return

	-- Iniciais de Utilizador
	declare @iniciais varchar(3);
	set @iniciais = isnull((select iniciais from B_us (nolock) where userno = @usr),'ADM')


	IF (select count(nbenef) from b_utentes (nolock) where nbenef=ltrim(str(@nrSns))) = 1
	/*
		Atualiza Ficha
	*/
	begin 

		-- Update Client
		update
			B_utentes
		set
			nome			= cc.nome
			,ncont			= cc.nif
			,morada			= case when cc.morada is null then b_utentes.morada else cc.morada end
			,bino			= ltrim(str(cc.id))
			,nascimento		= convert(datetime,cc.nascimento)
			,sexo			= cc.sexo

			,usrinis		= @iniciais
			,usrdata		= convert(varchar,getdate(),112)
			,usrhora		= convert(varchar,getdate(),108)
		from
			dispensa_eletronica_cc cc (nolock)
		where
			cc.nrSns = @nrSns
			and b_utentes.nbenef = ltrim(str(@nrSns))

	/*
		Cria Ficha
	*/
	end else begin

		-- Cart�o de Cliente
		declare @criaCartaoCliente bit, @nrcartao varchar(7)
		set @criaCartaoCliente = isnull((select bool from b_parameters (nolock) where stamp='ADM0000000021'),0)
		if @criaCartaoCliente=1
		begin
			set @nrcartao = right((select ISNULL(max(nrcartao),'CL00000') as nrcartao from B_fidel (nolock)),5)
			set @nrcartao = 'CL' + replicate('0',5-LEN(convert(int,@nrcartao))) + convert(varchar,@nrcartao+1)
		end else
			set @nrcartao = ''
		

		-- Create stamp and client number
		declare @stamp char(25), @no numeric(10)
		set @stamp = left(newid(),21)
		set @no = (select isnull(max(no)+1,1) from b_utentes (nolock))

		-- Create Client 
		INSERT INTO b_utentes (
			utstamp, no, nome
			,ncont, morada, bino, nascimento, sexo, nbenef
			,ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
			,nrcartao
			,codigoP, descP, Moeda

			,conta, contaacer, contafac, contaainc, contatit, contalet, contaletdes, contaletsac

			,codigoPNat, descPNat, pais, entfact
		)
		select
			stamp = @stamp
			,no = @no
			,nome = cc.nome
			,ncont = cc.nif
			,morada = case when cc.morada is null then '' else cc.morada end
			,bino = ltrim(str(cc.id))
			,nascimento = convert(datetime,cc.nascimento)
			,sexo = cc.sexo
			,nbenef = ltrim(str(cc.nrSns))

			,ousrinis = @iniciais
			,ousrdata = convert(varchar,getdate(),112)
			,ousrhora = convert(varchar,getdate(),108)
			,usrinis = @iniciais
			,usrdata = convert(varchar,getdate(),112)
			,usrhora = convert(varchar,getdate(),108)

			,nrcartao = @nrcartao
			,codigoP = 'PT'
			,descP = 'PORTUGAL'
			,moeda = 'EURO'

			/* contas */
			,conta = isnull(LEFT((select conta = replace(replace(rtrim(textvalue),'TP','11'),'NNNNNN', right('000000' + convert(varchar,@no), (case when len(@no)<=6 then 6 else len(@no) end) )) from b_parameters (nolock) where stamp='ADM0000000242' and bool=1),15),'')
			,contaacer = isnull(LEFT((select contaacer = replace(replace(rtrim(textvalue),'TP','11'),'NNNNNN', right('000000' + convert(varchar,@no), (case when len(@no)<=6 then 6 else len(@no) end) )) from b_parameters (nolock) where stamp='ADM0000000244' and bool=1),15),'')
			,contafac = isnull(LEFT((select contafac = replace(replace(rtrim(textvalue),'TP','11'),'NNNNN', right('00000' + convert(varchar,@no), (case when len(@no)<=5 then 5 else len(@no) end) )) from b_parameters (nolock) where stamp='ADM0000000246' and bool=1),15),'')
			,contaainc = isnull(LEFT((select contaainc = replace(replace(rtrim(textvalue),'TP','11'),'NNNNNN', right('00000' + convert(varchar,@no), (case when len(@no)<=6 then 6 else len(@no) end) )) from b_parameters (nolock) where stamp='ADM0000000243' and bool=1),15),'')
			,contatit = isnull(LEFT((select contatit = replace(replace(rtrim(textvalue),'TP','11'),'NNNNN', right('00000' + convert(varchar,@no), (case when len(@no)<=5 then 5 else len(@no) end) )) from b_parameters (nolock) where stamp='ADM0000000245' and bool=1),15),'')
			,contalet = isnull(LEFT((select contalet = replace(replace(rtrim(textvalue),'TP','1'),'NNNNN', right('00000' + convert(varchar,@no), (case when len(@no)<=5 then 5 else len(@no) end) )) from b_parameters (nolock) where stamp='ADM0000000247' and bool=1),15),'')
			,contaletdes = isnull(LEFT((select contaletdes = replace(replace(rtrim(textvalue),'TP','11'),'NNNNN', right('00000' + convert(varchar,@no), (case when len(@no)<=5 then 5 else len(@no) end) )) from b_parameters (nolock) where stamp='ADM0000000248' and bool=1),15),'')
			,contaletsac = isnull(LEFT((select contaletsac = replace(replace(rtrim(textvalue),'TP','11'),'NNNNN', right('00000' + convert(varchar,@no), (case when len(@no)<=5 then 5 else len(@no) end) )) from b_parameters (nolock) where stamp='ADM0000000249' and bool=1),15),'')

			,codigoPNat = 'PT'
			,descPNat = 'PORTUGAL'
			,pais = 1
			,entfact = 1
		from
			dispensa_eletronica_cc cc (nolock)
		where
			cc.nrSNS = @nrSNS
	
		-- Cria cart�o de cliente se aplic�vel
		if @criaCartaoCliente=1
		begin
			insert into B_fidel
				(fstamp, clstamp, clno, clestab, nrcartao, validade, ousr, usr)
			values
				(left(NEWID(),23), @stamp, @no, 0, @nrcartao, '30001231', 1, 1)
		end

	end


go
Grant Execute On dbo.up_clientes_CartaoCidadao_insertUpdate to Public
Grant control On dbo.up_clientes_CartaoCidadao_insertUpdate to Public
go