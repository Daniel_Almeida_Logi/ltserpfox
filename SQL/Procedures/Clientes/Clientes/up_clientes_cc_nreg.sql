/* SP:		up_clientes_cc_nreg
	
	 exec up_clientes_cc_nreg 242, 2, 1, 3, 'Loja 1'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_clientes_cc_nreg]') IS NOT NULL
	drop procedure dbo.up_clientes_cc_nreg
go

Create procedure dbo.up_clientes_cc_nreg
@No			NUMERIC(10),
@estab		NUMERIC(4),
@modo		bit,
@verTodos	int,
@loja		varchar(20)


/* with encryption */
AS
SET NOCOUNT ON

;with
	cteUser as
	(
		select 
			distinct username
			,iniciais
		from 
			b_us
	), cteEstabs as (
		Select 
			estab
		From
			B_utentes
		Where
			b_utentes.no = @No
			And estab = (
						case 
							when @verTodos=0 then 0 /*SEDE*/
							when @verTodos =3 then @estab  /*APENAS ESTAB*/
							when @verTodos in (1,2) then b_utentes.estab  /*TODOS*/
							else -1
						end
				)
			And estab != (
						case 
							when @verTodos = 2 then 0 /*APENAS ESTAB*/
							else -1
						end
				)
	)

	
	
	

Select	'SALDO' = 
				Case when @modo=1
				then
					isnull(
							(
								Select sum((cc.edeb-cc.edebf)-(cc.ecred-cc.ecredf)) 
								From	(
									SELECT	cc.ccstamp, ROW_NUMBER() over(order by cc.datalc,cc.ousrhora, cc.cm, cc.nrdoc) as numero, edeb, ecred
									FROM	cc (nolock)
									left join ft (nolock) on cc.ftstamp=ft.ftstamp
									WHERE	cc.no=@no 
											And cc.estab=(case when @verTodos=1 then cc.estab else @estab end)
											And (abs((cc.ecred-cc.ecredf)-(cc.edeb-cc.edebf)) > 0.010000)
											and ft.anulado=0
							) a inner join cc (nolock) on cc.ccstamp = a.ccstamp
							Where a.numero <= b.numero
					),0)
				else
					0
				end,
		*
From	(	
		select 	
			ROW_NUMBER() over(order by cc.datalc,cc.ousrhora, cc.cm, cc.nrdoc asc) as numero,
			'CCSTAMP'		= cc.ccstamp,
			'DATALC'		= convert(varchar,cc.datalc,102),
			'DATAVEN'		= convert(varchar,cc.dataven,102),
			'EDEB'			= cc.edeb,
			'ECRED'			= cc.ecred,
			'EDEBF'			= cc.edebf,
			'ECREDF'		= cc.ecredf,
			'VALOR'			= (cc.edeb-cc.edebf)-(cc.ecred-cc.ecredf),
			'MOEDA'			= cc.moeda,
			'OBS'			= cc.obs,
			'CMDESC'		= cc.cmdesc,
			'NRDOC'			= cc.nrdoc,
			'ULTDOC'		= cc.ultdoc,
			'INTID'			= cc.intid,
			'FACSTAMP'		= cc.faccstamp,
			'CBBNO'			= cc.cbbno,
			'ORIGEM'		= cc.origem,
			'OUSRHORA'		= cc.ousrhora,
			'OUSRINIS'		= cc.ousrinis,
			'IEMISSAO'		= datediff(d,cc.datalc,GETDATE()),
			'IVENCIMENTO'	= datediff(d,cc.datalc,GETDATE()),
			'USERNAME'		= isnull(cteUser.username,'')
			,cc.nome
			,'SITE'			= isnull(ft.site,re.site)
		from
			cc (nolock)
			left join ft (nolock) on cc.ftstamp=ft.ftstamp
			left join cteUser on cteUser.iniciais = cc.ousrinis
			left join re (nolock) on re.restamp = cc.restamp
		where
			cc.no=@No
			And cc.estab in (Select estab from cteEstabs)
			And (abs((cc.ecred-cc.ecredf)-(cc.edeb-cc.edebf)) > 0.010000)
			/*and ft.anulado is not null */
			and (
				ft.site = case when @loja = '' then ft.site else @loja end
				or
				re.site = case when @loja = '' then re.site else @loja end
				or 
				(ft.site is null and re.site is null)
			)
			
) b
order by numero


Go
Grant Execute on dbo.up_clientes_cc_nreg to Public
Grant Control on dbo.up_clientes_cc_nreg to Public
Go