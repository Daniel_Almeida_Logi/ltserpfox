-- Verificar Histórico de Tratamento do Utente
-- exec up_clientes_historicoTratamento2 234, 0, 't12052146678.831222544','20120916', '20251016','Loja 1'
-- exec up_clientes_historicoTratamento 234, 0, '','20200205', '20250306', 'Loja 1'
-- tipo: V - Vendas (atendimento) ; I - Interno (introduzido manualmente)
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_clientes_historicoTratamento]') IS NOT NULL
	drop procedure dbo.up_clientes_historicoTratamento
go

create procedure dbo.up_clientes_historicoTratamento

@no numeric(10,0),
@estab numeric(3,0),
@stamp varchar(25),
@de datetime,
@a datetime,
@site varchar(20)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

Begin
	select * 
	from (
		select 
			'DataDoc' = convert(varchar(100), ft.fdata, 108 )
			,fi.ref
			,fi.design
			,fi.qtt
			,ft.vendnm
			,LEFT(convert(varchar(254),ft2.obsdoc),200) as obsdoc
			,'V' as tipo 
			,ft.usrhora
			,CONVERT(bit,0) pesquisa
			,ft2.ft2stamp as 'stamp'
			,dci = LEFT(convert(varchar(254),''),200)
			,posologia = LEFT(convert(varchar(254),fi.posologia),200)
			,dtini = convert(date, ft.fdata)
			,dtp = convert(date, ft.fdata)
			,palmoco = CONVERT(bit,0)
			,almoco = CONVERT(bit,0)
			,lanche = CONVERT(bit,0)
			,jantar = CONVERT(bit,0)
			,noite = CONVERT(bit,0)
			,fi.fistamp
			,alterado = CONVERT(VARCHAR(1),CONVERT(bit,0))
		from fi (nolock)
		inner join ft  (nolock) on ft.ftstamp = fi.ftstamp
		inner join ft2 (nolock) on ft2.ft2stamp = ft.ftstamp
		inner join td  (nolock) on td.ndoc = ft.ndoc
		left join B_histConsumo (nolock) on B_histConsumo.fistamp = fi.fistamp
		where 
			(
				(
					ft.no = @no 
					and ft.estab = @estab
					and ft.u_hclstamp = ''
				)
				or ft.u_hclstamp = @stamp
			)
			and ltrim(rtrim(fi.ref)) != ''
			and ft.fdata between @de and @a
			and fi.fistamp not in (select top 1 ofistamp from fi fix where fix.ofistamp=fi.fistamp)
			and td.codigoDoc in (1,2,17,19,20) /*Factura, Venda a Dinheiro, Factura Resumo, Venda Importada, Factura Importada*/
			and ft.site = @site
			and B_histConsumo.fistamp is null
			
			/*REgistos de consumo que ainda não forma registados na tabela de consumos*/
			
		union all 
		
		select 
			'DataDoc' = convert(varchar(100), data, 108)
			,ref
			,design
			,qtt
			,operador
			,LEFT(convert(varchar(254),obs),200) as obsdoc
			,'I' as tipo 
			,usrhora
			,CONVERT(bit,0) pesquisa
			,B_histConsumo.clstamp as 'stamp'
			,dci = LEFT(convert(varchar(254),dci),200)
			,posologia = LEFT(convert(varchar(254),posologia),200) 
			,dtini
			,dtp
			,palmoco
			,almoco
			,lanche
			,jantar
			,noite
			,fistamp 
			,alterado = CONVERT(VARCHAR(1),CONVERT(bit,1))
		from B_histConsumo (nolock)
		where 
			clstamp = @stamp
			and data between @de and @a
			and site = @site
	)x
	order by 
		DataDoc desc, usrhora desc
End


GO
Grant Execute On dbo.up_clientes_historicoTratamento to Public
Grant Control On dbo.up_clientes_historicoTratamento to Public
GO