
--code to validate if the procedure exists, if exists, drops procedure
if OBJECT_ID('[dbo].[up_cooprofarMovimentosProd]') IS NOT NULL
	drop procedure dbo.up_cooprofarMovimentosProd
go

/*
	EXEC up_cooprofarMovimentosProd '2022-01-03','2022-01-03', 'LOJA 1', '0051'
*/
-- =============================================
-- Author:		Jos� Moreira
-- Create date: 2022-11-07
-- Description: Returns sales from a machine/store/client that hadn't been sent yet
-- =============================================
CREATE PROCEDURE dbo.up_cooprofarMovimentosProd
@dataini  as varchar(50),
@dataFim  as varchar(50),
@site  as varchar(50),
@codigoCooprofar  as varchar(10),
@numContribuinteCooprofar varchar(50) = '''500336512'''

/* WITH ENCRYPTION */ 

AS
	declare 
		@seconds int = 1	,
		@numContribuinteConco varchar(100)  
BEGIN
	set nocount on
	
	set @numContribuinteConco = '''500349142'',''504100050'',''500046921'',''502693150'',''500364877'''
	
	-- para controlar as origens da sl
	IF  OBJECT_ID(N'dbo.LOC_ORI_ANONIM_FO', N'U') is not null
	BEGIN
	   DROP TABLE LOC_ORI_ANONIM_FO;
	END;
	IF  OBJECT_ID(N'dbo.LOC_ORI_ANONIM_FT', N'U') is not null
	BEGIN
	   DROP TABLE LOC_ORI_ANONIM_FT;
	END;
	IF  OBJECT_ID(N'dbo.LOC_ORI_ANONIM_BO', N'U') is not null
	BEGIN
	   DROP TABLE LOC_ORI_ANONIM_BO;
	END;
	--
	--para filtrar os stamps permitos na sl (para evitar linhas repetidas
	IF  OBJECT_ID(N'tempdb..#fi_artigos') is not null
	BEGIN
	   DROP TABLE #fi_artigos;
	END;
	IF  OBJECT_ID(N'tempdb..#fi_stamps') is not null
	BEGIN
	   DROP TABLE #fi_stamps;
	END;
	IF  OBJECT_ID(N'tempdb..#bi_artigos') is not null
	BEGIN
	   DROP TABLE #bi_artigos;
	END;
	IF  OBJECT_ID(N'tempdb..#bi_stamps') is not null
	BEGIN
	   DROP TABLE #bi_stamps;
	END;
	IF  OBJECT_ID(N'tempdb..#fn_artigos') is not null
	BEGIN
	   DROP TABLE #fn_artigos;
	END;
	IF  OBJECT_ID(N'tempdb..#fn_stamps') is not null
	BEGIN
	   DROP TABLE #fn_stamps;
	END; 
	---------------------------------------------------------------------------------------------------
	-------- filtra todos os fistamps permitidos no intervalo (para evitar linhas repetidas) ----------
	---------------------------------------------------------------------------------------------------
	select distinct
		fi_inner.ref
		, fi_inner.ousrdata
		, fi_inner.ousrhora
		, fi_inner.qtt 
		, fi_inner.tipodoc
	into
		#fi_artigos
	from 
		fi (nolock) fi_inner
	where
		fi_inner.ref != ''
		and fi_inner.ftstamp in (
						select 
							ft.ftstamp 
						from 
							ft (nolock) 
						where 
							ft.ousrdata >= @dataini
							and ft.ousrdata <= @dataFim 
					)
	create nonclustered index ref_idx		on #fi_artigos (ref)
	create nonclustered index ousrdata_idx	on #fi_artigos (ousrdata)
	create nonclustered index ousrhora_idx	on #fi_artigos (ousrhora)
	create nonclustered index qtt_idx		on #fi_artigos (qtt)
	create nonclustered index tipodoc_idx	on #fi_artigos (tipodoc)

	select
		(select top 1 
			fistamp 
		from 
			fi (nolock) 
		where 
			fi.ref = #fi_artigos.ref
			and fi.ousrdata = #fi_artigos.ousrdata
			and fi.ousrhora = #fi_artigos.ousrhora
			and fi.qtt = #fi_artigos.qtt
			and fi.tipodoc = #fi_artigos.tipodoc) as fistamp
	into 
		#fi_stamps
	from 
		#fi_artigos
	create nonclustered index fistamp_idx on #fi_stamps (fistamp)
	---------------------------------------------------------------------------------------------------
	-------------------------------------- fim de filtro ----------------------------------------------
	---------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------
	-------- filtra todos os bistamps permitidos no intervalo (para evitar linhas repetidas) ----------
	---------------------------------------------------------------------------------------------------
	select distinct
		bi_inner.ref
		, bi_inner.ousrdata
		, bi_inner.ousrhora
		, bi_inner.qtt
		, bi_inner.ndos
	into
		#bi_artigos
	from 
		bi (nolock) bi_inner
	where
		bi_inner.ref != ''
		and bi_inner.bostamp in (
						select 
							bo.bostamp 
						from 
							bo (nolock) 
						where 
							bo.ousrdata >= @dataini
							and bo.ousrdata <= @dataFim
					)
	create nonclustered index ref_idx		on #bi_artigos (ref)
	create nonclustered index ousrdata_idx	on #bi_artigos (ousrdata)
	create nonclustered index ousrhora_idx	on #bi_artigos (ousrhora)
	create nonclustered index qtt_idx		on #bi_artigos (qtt)
	create nonclustered index ndos_idx		on #bi_artigos (ndos)

	select
		(select top 1 
			bistamp 
		from 
			bi (nolock) 
		where 
			bi.ref = #bi_artigos.ref
			and bi.ousrdata = #bi_artigos.ousrdata
			and bi.ousrhora = #bi_artigos.ousrhora
			and bi.qtt = #bi_artigos.qtt
			and bi.ndos = #bi_artigos.ndos) as bistamp
	into 
		#bi_stamps
	from 
		#bi_artigos
	create nonclustered index fistamp_idx on #bi_stamps (bistamp)
	---------------------------------------------------------------------------------------------------
	-------------------------------------- fim de filtro ----------------------------------------------
	
	---------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------
	-------- filtra todos os fnstamps permitidos no intervalo (para evitar linhas repetidas) ----------
	---------------------------------------------------------------------------------------------------
	select distinct
		fn_inner.ref
		, fn_inner.ousrdata
		, fn_inner.ousrhora
		, fn_inner.qtt
		, fn_inner.docnome
	into
		#fn_artigos
	from 
		fn (nolock) fn_inner
	where
		fn_inner.ref != ''
		and fn_inner.fostamp in (
						select 
							fo.fostamp 
						from 
							fo (nolock) 
						where 
							fo.ousrdata >= @dataini
							and fo.ousrdata <= @dataFim
					)
	create nonclustered index ref_idx		on #fn_artigos (ref)
	create nonclustered index ousrdata_idx	on #fn_artigos (ousrdata)
	create nonclustered index ousrhora_idx	on #fn_artigos (ousrhora)
	create nonclustered index qtt_idx	on #fn_artigos (qtt)
	create nonclustered index docnome_idx	on #fn_artigos (docnome)

	select
		(select top 1 
			fnstamp 
		from 
			fn (nolock) 
		where 
			fn.ref = #fn_artigos.ref
			and fn.ousrdata = #fn_artigos.ousrdata
			and fn.ousrhora = #fn_artigos.ousrhora
			and fn.qtt = #fn_artigos.qtt
			and fn.docnome = #fn_artigos.docnome) as fnstamp
	into 
		#fn_stamps
	from 
		#fn_artigos
	create nonclustered index fnstamp_idx on #fn_stamps (fnstamp)
	---------------------------------------------------------------------------------------------------
	-------------------------------------- fim de filtro ----------------------------------------------
	---------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------
	-----------------------------Define os diverso campos origem---------------------------------------
	---------------------------------------------------------------------------------------------------
	create table LOC_ORI_ANONIM_FO(slstamp varchar(25), origem varchar(2), LOC_ORI_ANONIM varchar(MAX))
	create table LOC_ORI_ANONIM_FT(slstamp varchar(25), origem varchar(2), LOC_ORI_ANONIM varchar(MAX))
	create table LOC_ORI_ANONIM_BO(slstamp varchar(25), origem varchar(2), LOC_ORI_ANONIM varchar(MAX))
	
	Declare @sql varchar(max) = '
	Insert into
		LOC_ORI_ANONIM_FO
	select	
		slstamp
		,origem 
		,case when 
			cm > 50
		then
			(select nomabrv from empresa (nolock) where site = ''' + @site +''')
		else
			case when 
				(select distinct ncont from fo fo_inner(nolock)  inner join fn fn_inner (nolock) on fo_inner.fostamp = fn_inner.fostamp where sl.fnstamp = fn_inner.fnstamp and cm<50) ='+ @numContribuinteCooprofar +'
			then 
				''COOPROFAR''
			else  
				case when 
					(select distinct ncont from fo fo_inner(nolock)  inner join fn fn_inner (nolock) on fo_inner.fostamp = fn_inner.fostamp where sl.fnstamp = fn_inner.fnstamp and cm<50) in('+@numContribuinteConco+') 
				then 
					''OUTROS1''
				else 
					''OUTROS2''
				end
			end
		end as LOC_ORI_ANONIM 
	from 
		sl (nolock)
	where 		 
		sl.ousrdata >= '''+@dataini+'''
		and sl.ousrdata <= '''+@dataFim+'''
		and armazem in (select armazem from empresa_arm(nolock) where empresa_no  in (select no from empresa(nolock) where site in (select * from up_SplitToTable('''+@site+''','';''))))
		and origem IN (''FO'')'
	exec(@sql)

	set @sql = '
	Insert into
		LOC_ORI_ANONIM_FT
	select	
		slstamp
		,origem 
		,case when 
			cm > 50
		then
			(select nomabrv from empresa (nolock) where site = ''' + @site +''')
		else
			case when 
				(select distinct ncont from ft ft_inner(nolock)  inner join fi fi_inner (nolock) on ft_inner.ftstamp = fi_inner.fistamp where sl.fistamp = fi_inner.fistamp and cm<50) = '+@numContribuinteCooprofar+' 
			then 
				''COOPROFAR''
			else  
				case when 
					(select distinct ncont from ft ft_inner(nolock)  inner join fi fi_inner (nolock) on ft_inner.ftstamp = fi_inner.fistamp where sl.fistamp = fi_inner.fistamp and cm<50) in('+@numContribuinteConco+') 
				then 
					''OUTROS1'' 
				else 
					''OUTROS2''
				end
			end
		
		end as LOC_ORI_ANONIM 
	from 
		sl (nolock)
	where 		 
		sl.ousrdata >= '''+@dataini+'''
		and sl.ousrdata <= '''+@dataFim+'''
		and armazem in (select armazem from empresa_arm(nolock) where empresa_no  in (select no from empresa(nolock) where site in (select * from up_SplitToTable('''+@site+''','';''))))
		and origem IN (''FT'')'
	exec(@sql)

	set @sql = '
	Insert into
		LOC_ORI_ANONIM_BO
	select	
		slstamp
		,origem 
		,case when 
			cm > 50
		then
			 (select nomabrv from empresa (nolock) where site = ''' + @site +''')
		else
			case when 
				(select distinct ncont from bo bo_inner(nolock)  inner join bi bi_inner (nolock) on bo_inner.bostamp = bi_inner.bistamp where sl.fistamp = bi_inner.bistamp and cm<50) = '+@numContribuinteCooprofar+' 
			then 
				''COOPROFAR''
			else  
				case when 
					(select distinct ncont from bo bo_inner(nolock)  inner join bi bi_inner (nolock) on bo_inner.bostamp = bi_inner.bistamp where sl.fistamp = bi_inner.bistamp and cm<50) in('+@numContribuinteConco+') 
				then 
					''OUTROS1''
				else 
					''OUTROS2''
				end
			end
		end as LOC_ORI_ANONIM		 
	from 
		sl (nolock)
	where 		 
		sl.ousrdata >= '''+@dataini+'''
		and sl.ousrdata <= '''+@dataFim+'''
		and armazem in (select armazem from empresa_arm(nolock) where empresa_no  in (select no from empresa(nolock) where site in (select * from up_SplitToTable('''+@site+''','';''))))
		and origem IN (''BO'')' 
	exec(@sql) 
	 
	set nocount off 
	select  
		'NUM_MOV'			as NUM_MOV 
		,'PRODUTOS_ID'		as PRODUTOS_ID 
		,'PRD_COD'			as PRD_COD
		,'DESIGNACAO'		as DESIGNACAO
		,'DATA'				as DATA
		,'HORA_BASE'		as HORA_BASE
		,'HORA'				as HORA
		,'STK_ANTIGO'		as STK_ANTIGO
		,'STK_NOVO'			as STK_NOVO
		,'QTD'				as QTD
		,'QTD_ABS'			as QTD_ABS
		,'TIPO_MOV'			as TIPO_MOV
		,'FARMACIA'			as FARMACIA
		,'NUM_DOC'			as NUM_DOC
		,'LOC_ORI_ANONIM'	as LOC_ORI_ANONIM
		,'STOCK_MAXIMO'		as STOCK_MAXIMO
		,'STOCK_MINIMO'		as STOCK_MINIMO
		,'OPERADOR'			as OPERADOR
		,'STOCK_ATUAL'		as STOCK_ATUAL
		,'PCUS_PROD'		as P_CUS_PROD
	union all
	select 
		NUM_MOV				= rtrim(ltrim(convert(varchar, convert(varchar,DATEDIFF(SECOND,{d '1970-01-01'}, sl.ousrdata)),25)+convert(varchar, ROW_NUMBER() OVER(ORDER BY SL.SLSTAMP ASC))))  
		,PRODUTOS_ID		= rtrim(ltrim(sl.ref))
		,PRD_COD			= rtrim(ltrim(st.ref))
		,DESIGNACAO			= rtrim(ltrim(st.design))
		,DATA				= convert(varchar,sl.datalc, 23) --YYYY-MM-DD
		,HORA_BASE			= convert(varchar,datepart(hour, sl.ousrhora)*3600 
										+ DATEPART(minute, sl.ousrhora)*60 
										+ DATEPART(second, sl.ousrhora))
		,HORA				= convert(varchar,sl.ousrhora)
		,STK_ANTIGO			= convert(varchar,convert(int,
									(ISNULL((
										SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
										FROM	sl (nolock)sl1
										WHERE sl1.ref = sl.ref and convert(datetime,convert(varchar(10),sl1.ousrdata,120)+' ' + sl1.ousrhora)   <=   Dateadd(ss,-@seconds,convert(datetime,convert(varchar(10),sl.ousrdata,120)+' ' + sl.ousrhora)) 
										and armazem in  (select armazem from empresa_arm(nolock) where empresa_no  in (select  no from empresa(nolock) where site in (select * from up_SplitToTable(@site,';')))) 
									),0) )
							)) 
		,STK_NOVO			= convert(varchar,convert(int,
									(ISNULL((
										SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
										FROM	sl (nolock)sl1
										WHERE sl1.ref = sl.ref and convert(datetime,convert(varchar(10),sl1.ousrdata,120)+' ' + sl1.ousrhora)   <=   Dateadd(ss,0,convert(datetime,convert(varchar(10),sl.ousrdata,120)+' ' + sl.ousrhora)) 
										and armazem in  (select armazem from empresa_arm(nolock) where empresa_no  in (select  no from empresa(nolock) where site in (select * from up_SplitToTable(@site,';')))) 
									),0) )
							))
		,QTD				= convert(varchar,convert(int, case when sl.cm > 50 then-(select
													sum(sl_inner.qtt) 
												from 
													sl as sl_inner (nolock) 
												where 
													sl.ref = sl_inner.ref 
													and sl_inner.ousrhora = sl.ousrhora
													and sl_inner.ousrdata = sl.ousrdata
												) else (select
													sum(sl_inner.qtt) 
												from 
													sl as sl_inner (nolock) 
												where 
													sl.ref = sl_inner.ref 
													and sl_inner.ousrhora = sl.ousrhora
													and sl_inner.ousrdata = sl.ousrdata
												) end))
		,QTD_ABS			= convert(varchar,convert(int, abs((select
													sum(sl_inner.qtt) 
												from 
													sl as sl_inner (nolock) 
												where 
													sl.ref = sl_inner.ref 
													and sl_inner.ousrhora = sl.ousrhora
													and sl_inner.ousrdata = sl.ousrdata
												))))
		,TIPO_MOV			= CONVERT(varchar,cm) +'/' +cmdesc
		,FARMACIA			= @codigoCooprofar
		,NUM_DOC			= CONVERT(varchar,adoc)
		,LOC_ORI_ANONIM		= (select LOC_ORI_ANONIM	from LOC_ORI_ANONIM_FO (nolock) where LOC_ORI_ANONIM_FO.slstamp = sl.slstamp and LOC_ORI_ANONIM_FO.origem = sl.origem)
		,STOCK_MAXIMO		= convert(varchar,convert(int,st.stmax))
		,STOCK_MINIMO		= convert(varchar,convert(int,st.ptoenc))
		,OPERADOR			= (select top 1 nome from b_us (nolock) where b_us.iniciais =sl.ousrinis)
		,STOCK_ATUAL		= convert(varchar,convert(numeric(18,0),st.stock))
		,PCUS_PROD			= convert(varchar,convert(numeric(18,2),st.epcpond))
	from 
				sl		(nolock)
	inner join	st		(nolock)	on	sl.ref = st.ref and st.site_nr = sl.armazem
	where 
		sl.ousrdata >= @dataini 
		and sl.ousrdata <= @dataFim
		and origem = 'FO'
		and sl.fnstamp in (select fnstamp from #fn_stamps)
		and armazem in  (select 
							armazem 
						from 
							empresa_arm(nolock) 
						where 
							empresa_no  in (select  
												no 
											from 
												empresa(nolock) 
											where 
												site in (select 
															* 
														from 
															up_SplitToTable(@site,';'))))   
	union all
	select 
		NUM_MOV				= rtrim(ltrim(convert(varchar, convert(varchar,DATEDIFF(SECOND,{d '1970-01-01'}, sl.ousrdata)),25)+convert(varchar, ROW_NUMBER() OVER(ORDER BY SL.SLSTAMP ASC))))  
		,PRODUTOS_ID		= rtrim(ltrim(sl.ref))
		,PRD_COD			= rtrim(ltrim(st.ref))
		,DESIGNACAO			= rtrim(ltrim(st.design))
		,data				= convert(varchar,sl.datalc, 23) --YYYY-MM-DD
		,HORA_BASE			= convert(varchar,datepart(hour, sl.ousrhora)*3600 
										+ DATEPART(minute, sl.ousrhora)*60 
										+ DATEPART(second, sl.ousrhora))
		,HORA				= convert(varchar,sl.ousrhora)
		,STK_ANTIGO			= convert(varchar,convert(int,
									(ISNULL((
										SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
										FROM	sl (nolock)sl1
										WHERE sl1.ref = sl.ref and convert(datetime,convert(varchar(10),sl1.ousrdata,120)+' ' + sl1.ousrhora)   <=   Dateadd(ss,-@seconds,convert(datetime,convert(varchar(10),sl.ousrdata,120)+' ' + sl.ousrhora)) 
										and armazem in  (select armazem from empresa_arm(nolock) where empresa_no  in (select  no from empresa(nolock) where site in (select * from up_SplitToTable(@site,';')))) 
									),0) )
							)) 
		,STK_NOVO			= convert(varchar,convert(int,
									(ISNULL((
										SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
										FROM	sl (nolock)sl1
										WHERE sl1.ref = sl.ref and convert(datetime,convert(varchar(10),sl1.ousrdata,120)+' ' + sl1.ousrhora)   <=   Dateadd(ss,0,convert(datetime,convert(varchar(10),sl.ousrdata,120)+' ' + sl.ousrhora)) 
										and armazem in  (select armazem from empresa_arm(nolock) where empresa_no  in (select  no from empresa(nolock) where site in (select * from up_SplitToTable(@site,';')))) 
									),0) )
							))
		,QTD				= convert(varchar,convert(int, case when sl.cm > 50 then-(select
													sum(sl_inner.qtt) 
												from 
													sl as sl_inner (nolock) 
												where 
													sl.ref = sl_inner.ref 
													and sl_inner.ousrhora = sl.ousrhora
													and sl_inner.ousrdata = sl.ousrdata
												) else (select
													sum(sl_inner.qtt) 
												from 
													sl as sl_inner (nolock) 
												where 
													sl.ref = sl_inner.ref 
													and sl_inner.ousrhora = sl.ousrhora
													and sl_inner.ousrdata = sl.ousrdata
												) end))
		,QTD_ABS			= convert(varchar,convert(int, abs((select
													sum(sl_inner.qtt) 
												from 
													sl as sl_inner (nolock) 
												where 
													sl.ref = sl_inner.ref 
													and sl_inner.ousrhora = sl.ousrhora
													and sl_inner.ousrdata = sl.ousrdata
												))))
		,TIPO_MOV			= CONVERT(varchar,cm) +'/' +cmdesc
		,FARMACIA			= @codigoCooprofar
		,NUM_DOC			= CONVERT(varchar,adoc)
		,LOC_ORI_ANONIM		= (select LOC_ORI_ANONIM	from LOC_ORI_ANONIM_FT (nolock) where LOC_ORI_ANONIM_FT.slstamp = sl.slstamp and LOC_ORI_ANONIM_FT.origem = sl.origem)
		,STOCK_MAXIMO		= convert(varchar,convert(int,st.stmax))
		,STOCK_MINIMO		= convert(varchar,convert(int,st.ptoenc))
		,OPERADOR			= (select top 1 nome from b_us (nolock) where b_us.iniciais =sl.ousrinis)
		,STOCK_ATUAL		= convert(varchar,convert(numeric(18,0),st.stock))
		,P_CUS_PROD			= convert(varchar,convert(numeric(18,2),st.epcpond))
	from 
				sl		(nolock)
	inner join	st		(nolock)	on	sl.ref = st.ref and st.site_nr = sl.armazem
	where 
		sl.ousrdata >= @dataini 
		and sl.ousrdata <= @dataFim
		and origem = 'FT'
		and sl.fistamp in (select fistamp from #fi_stamps)
		and armazem in  (select 
							armazem 
						from 
							empresa_arm(nolock) 
						where 
							empresa_no  in (select  
												no 
											from 
												empresa(nolock) 
											where 
												site in (select 
															* 
														from 
															up_SplitToTable(@site,';'))))      
	union all
	select 
		NUM_MOV				= rtrim(ltrim(convert(varchar, convert(varchar,DATEDIFF(SECOND,{d '1970-01-01'}, sl.ousrdata)),25)+convert(varchar, ROW_NUMBER() OVER(ORDER BY SL.SLSTAMP ASC))))  
		,PRODUTOS_ID		= rtrim(ltrim(sl.ref))
		,PRD_COD			= rtrim(ltrim(st.ref))
		,DESIGNACAO			= rtrim(ltrim(st.design))
		,data				= convert(varchar,sl.datalc, 23) --YYYY-MM-DD
		,HORA_BASE			= convert(varchar,datepart(hour, sl.ousrhora)*3600 
										+ DATEPART(minute, sl.ousrhora)*60 
										+ DATEPART(second, sl.ousrhora))
		,HORA				= convert(varchar,sl.ousrhora)
		,STK_ANTIGO			= convert(varchar,convert(int,
									(ISNULL((
										SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
										FROM	sl (nolock)sl1
										WHERE sl1.ref = sl.ref and convert(datetime,convert(varchar(10),sl1.ousrdata,120)+' ' + sl1.ousrhora)   <=   Dateadd(ss,-@seconds,convert(datetime,convert(varchar(10),sl.ousrdata,120)+' ' + sl.ousrhora)) 
										and armazem in  (select armazem from empresa_arm(nolock) where empresa_no  in (select  no from empresa(nolock) where site in (select * from up_SplitToTable(@site,';')))) 
									),0) )
							)) 
		,STK_NOVO			= convert(varchar,convert(int,
									(ISNULL((
										SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
										FROM	sl (nolock)sl1
										WHERE sl1.ref = sl.ref and convert(datetime,convert(varchar(10),sl1.ousrdata,120)+' ' + sl1.ousrhora)   <=   Dateadd(ss,0,convert(datetime,convert(varchar(10),sl.ousrdata,120)+' ' + sl.ousrhora)) 
										and armazem in  (select armazem from empresa_arm(nolock) where empresa_no  in (select  no from empresa(nolock) where site in (select * from up_SplitToTable(@site,';')))) 
									),0) )
							))
		,QTD				= convert(varchar,convert(int, case when sl.cm > 50 then-(select
													sum(sl_inner.qtt) 
												from 
													sl as sl_inner (nolock) 
												where 
													sl.ref = sl_inner.ref 
													and sl_inner.ousrhora = sl.ousrhora
													and sl_inner.ousrdata = sl.ousrdata
												) else (select
													sum(sl_inner.qtt) 
												from 
													sl as sl_inner (nolock) 
												where 
													sl.ref = sl_inner.ref 
													and sl_inner.ousrhora = sl.ousrhora
													and sl_inner.ousrdata = sl.ousrdata
												) end))
		,QTD_ABS			= convert(varchar,convert(int, abs((select
													sum(sl_inner.qtt) 
												from 
													sl as sl_inner (nolock) 
												where 
													sl.ref = sl_inner.ref 
													and sl_inner.ousrhora = sl.ousrhora
													and sl_inner.ousrdata = sl.ousrdata
												))))
		,TIPO_MOV			= CONVERT(varchar,cm) +'/' +cmdesc
		,FARMACIA			= @codigoCooprofar
		,NUM_DOC			= CONVERT(varchar,adoc)
		,LOC_ORI_ANONIM		= (select LOC_ORI_ANONIM	from LOC_ORI_ANONIM_BO (nolock) where LOC_ORI_ANONIM_BO.slstamp = sl.slstamp and LOC_ORI_ANONIM_BO.origem = sl.origem) 
		,STOCK_MAXIMO		= convert(varchar,convert(int,st.stmax))
		,STOCK_MINIMO		= convert(varchar,convert(int,st.ptoenc))
		,OPERADOR			= (select top 1 nome from b_us (nolock) where b_us.iniciais =sl.ousrinis)
		,STOCK_ATUAL		= convert(varchar,convert(numeric(18,0),st.stock))
		,P_CUS_PROD			= convert(varchar,convert(numeric(18,2),st.epcpond))
	from 
				sl		(nolock)
	inner join	st		(nolock)	on	sl.ref = st.ref and st.site_nr = sl.armazem
	where 
		sl.ousrdata >= @dataini 
		and sl.ousrdata <= @dataFim
		and origem = 'BO'
		and sl.bistamp in (select bistamp from #bi_stamps)
		and armazem in  (select 
							armazem 
						from 
							empresa_arm(nolock) 
						where 
							empresa_no  in (select  
												no 
											from 
												empresa(nolock) 
											where 
												site in (select 
															* 
														from 
															up_SplitToTable(@site,';'))))   
	 


	-- para controlar as origens da sl
	IF  OBJECT_ID(N'dbo.LOC_ORI_ANONIM_FO', N'U') is not null
	BEGIN
	   DROP TABLE LOC_ORI_ANONIM_FO;
	END;
	IF  OBJECT_ID(N'dbo.LOC_ORI_ANONIM_FT', N'U') is not null
	BEGIN
	   DROP TABLE LOC_ORI_ANONIM_FT;
	END;
	IF  OBJECT_ID(N'dbo.LOC_ORI_ANONIM_BO', N'U') is not null
	BEGIN
	   DROP TABLE LOC_ORI_ANONIM_BO;
	END;
	--
	--para filtrar os stamps permitos na sl (para evitar linhas repetidas
	IF  OBJECT_ID(N'tempdb..#fi_artigos') is not null
	BEGIN
	   DROP TABLE #fi_artigos;
	END;
	IF  OBJECT_ID(N'tempdb..#fi_stamps') is not null
	BEGIN
	   DROP TABLE #fi_stamps;
	END;
	IF  OBJECT_ID(N'tempdb..#bi_artigos') is not null
	BEGIN
	   DROP TABLE #bi_artigos;
	END;
	IF  OBJECT_ID(N'tempdb..#bi_stamps') is not null
	BEGIN
	   DROP TABLE #bi_stamps;
	END;
	IF  OBJECT_ID(N'tempdb..#fn_artigos') is not null
	BEGIN
	   DROP TABLE #fn_artigos;
	END;
	IF  OBJECT_ID(N'tempdb..#fn_stamps') is not null
	BEGIN
	   DROP TABLE #fn_stamps;
	END;
END
GO
