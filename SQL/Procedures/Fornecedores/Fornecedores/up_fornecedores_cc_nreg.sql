/* 

exec	up_fornecedores_cc_nreg 208, 0

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_fornecedores_cc_nreg]') IS NOT NULL
	drop procedure dbo.up_fornecedores_cc_nreg
go

Create procedure dbo.up_fornecedores_cc_nreg

@No			NUMERIC(10),
@estab		NUMERIC(4),
@site		varchar(20)

/* with encryption */
AS
SET NOCOUNT ON

select
	'FCSTAMP'		= fc.fcstamp, 
	'FOSTAMP'		= fc.fostamp, 
	'NO'			= fc.no, 
	'NOME'			= fc.nome, 
	'DATALC'		= convert(varchar,fc.datalc,102), 
	'DATAVEN'		= convert(varchar,fc.dataven,102),
	'ECRED'			= fc.ecred, 
	'EDEB'			= fc.edeb, 
	'ECREDF'		= fc.ecredf, 
	'EDEBF'			= fc.edebf, 
	'ORIGEM'		= fc.origem, 
	'MOEDA'			= fc.moeda, 
	'CMDESC'		= fc.cmdesc, 
	'ADOC'			= fc.adoc, 
	'ULTDOC'		= fc.ultdoc,
	'INTID'			= fc.intid, 
	'SALDO'			= ISNULL((SElect	SUM((a.ecred-abs(a.ecredf)))- SUM((a.edeb-a.edebf)) 
									From	fc a 
									Where	a.no=@No and a.estab=@estab
											and (abs((a.ecred-a.ecredf)-(a.edeb-a.edebf)) > 0.010000)
											and (a.ousrdata + a.ousrhora <= fc.ousrdata + fc.ousrhora))
							,0),			
	'DOCDATA'		= convert(varchar,isnull(fo.docdata,'19000101'),102), 
	'OUSRHORA'		= fc.ousrhora, 
	'USERNAME'		= (Select top 1 username from b_us where fc.ousrinis=b_us.iniciais), 
	'OUSRINIS'		= fc.ousrinis,
	'IEMISSAO'		= datediff(d,fo.docdata,GETDATE()),
	'IVENCIMENTO'	= datediff(d,fc.dataven,GETDATE()),
	'VALOR'			= (fc.ecred-abs(fc.ecredf))-(fc.edeb-fc.edebf), 
	'VERDOC'		= CONVERT(bit,0)
from
	fc (nolock) 
	left join fo (nolock) on fc.fostamp=fo.fostamp
	left join po (nolock) on po.postamp = fc.postamp
where
	fc.no=@No and fc.estab=@estab
	and (abs((fc.ecred-fc.ecredf)-(fc.edeb-fc.edebf)) > 0.010000)
	and (
		fo.site = case when @site = '' then fo.site else @site end
		or
		po.site = case when @site = '' then po.site else @site end
		or 
			(fo.site is null and po.site is null)
		)
		and 1 = (case
					when EXISTS(
								select 
									1 
								from 
									fn(nolock) 
									left join fo(nolock) as ffo on ffo.fostamp = fn.fostamp 
								where 
									ffo.doccode = 112 
									and fn.ofnstamp in (select ffn.fnstamp from fn(nolock) as ffn where ffn.fostamp = fo.fostamp)
								)
						then 0
					else 
						1
				 end)
order by
	datalc, ousrhora

Go
Grant Execute on dbo.up_fornecedores_cc_nreg to Public
Grant Control on dbo.up_fornecedores_cc_nreg to Public
Go