/* An�lise Conta Corrente de Fornecedor

	 exec up_fornecedores_cc 1, 0, '20000101', '20210101', 'Loja 1'

	 select
		no, estab, edeb, ecred, datalc,ousrdata,ousrhora,fcstamp
	From
		fc (nolock)
	Where
		fc.no = 28
		And fc.estab = 0

		select * from fl where nome like '%alli%'


	exec up_fornecedores_cc 2, 0, '19000101', '20230101', 'Loja 1'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_fornecedores_cc]') IS NOT NULL
	drop procedure dbo.up_fornecedores_cc
go

create procedure dbo.up_fornecedores_cc

@No			NUMERIC(10),
@estab		NUMERIC(4),
@Dataini	datetime,
@Datafim	datetime,
@site		varchar(20),
@calcSaldo	bit = 1


/* WITH ENCRYPTION */
AS
SET NOCOUNT ON


IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFc'))
	DROP TABLE #dadosFc
IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSaldoAnterior'))
	DROP TABLE #dadosSaldoAnterior
IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSaldo'))
	DROP TABLE #dadosSaldo
IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFacturaResumo'))
	DROP TABLE #dadosFacturaResumo


	--documentos que foram importados para vossas facturas resumo
	--ir�o ser excluidos
	select 	 
		distinct	'fostamp' =  isnull(fn.fostamp,'')
	into 
		#dadosFacturaResumo
	from 
		fn(nolock) 
		left join fo(nolock)  on fo.fostamp = fn.fostamp 
	where 
		fo.no = @No	
		and	fo.estab = @estab 
		and fn.fnstamp in (select 
								ffn.ofnstamp 
							from 
								fn(nolock) as ffn 
							inner join 
								fo(nolock) foo
							on 
								foo.fostamp=ffn.fostamp 
							 where foo.doccode = 112  )
		
	CREATE CLUSTERED INDEX IDX_dadosFacturaResumo_fostamp ON #dadosFacturaResumo(fostamp)

	-- temp table com cc do fornecedor
	select
		no, estab, edeb, ecred, datalc,ousrdata,ousrhora,fcstamp
	into 
		#dadosSaldo
	From
		fc (nolock)
	Where
		fc.no = @No 
		And fc.estab=@estab

	CREATE CLUSTERED INDEX IDX_#dadosSaldo_no_estab ON #dadosSaldo(no,estab)

	--
	select
		no, estab, edeb, ecred, datalc,ousrdata,ousrhora,fcstamp
	into 
		#dadosFc
	From
		fc (nolock)
	Where
		fc.no = @No 
		And fc.estab=@estab
		And datalc < @Dataini


	select
		saldoanterior = 
			ISNULL(
				(SElect 
					SUM(ecred) - SUM(edeb)
				From
					fc (nolock)
				Where
					fc.no=@No 
					And fc.estab=@estab
					And fc.datalc < @Dataini --between @Dataini and @Datafim
				)
			,0)	 
	into #dadosSaldoAnterior		
		
	declare 
		@saldoanterior as numeric(14,3)
		,@EDEBANTERIOR as numeric(14,3)
		,@ECREDANTERIOR as numeric(14,3)
	
	
	set @saldoanterior = ISNULL((SElect SUM(ecred) - SUM(edeb) From #dadosFc),0)
	set @EDEBANTERIOR = ISNULL((SElect SUM(edeb) From #dadosFc),0)
	set @ECREDANTERIOR = ISNULL((SElect SUM(ecred) From #dadosFc),0)
	

	select
		saldoanterior=0
		,edebanterior=0
		,ecredanterior=0
		,fcstamp=''
		,no=0
		,nome=''
		,estab=0
		,cmdesc=''
		,adoc=''
		,docdata=''
		,datalc=''
		,dataven=''
		,ecred=0
		,edeb=0
		,saldo=0
		,moeda=''
		,ousrhora=''
		,username=''
		,obs=''
		,ousrinis=''
		,saldoperiodo=0
		,ultdoc=''
		,intid=''
		,verdoc=0
		,origem = ''
	
	union all

	select
		saldoanterior=0
		,edebanterior=0
		,ecredanterior=0
		,fcstamp=''
		,no=0
		,nome=''
		,estab=0
		,cmdesc=''
		,adoc=''
		,docdata=''
		,datalc=''
		,dataven=''
		,ecred=0
		,edeb=0
		,saldo=0
		,moeda=''
		,ousrhora=''
		,username=''
		,obs='',ousrinis=''
		,saldoperiodo=0
		,ultdoc=''
		,intid=''
		,verdoc=0
		,origem = ''
	
	union all

	select
		'SALDOANTERIOR'	=	@saldoanterior,
		'EDEBANTERIOR'	=	@EDEBANTERIOR,
		'ECREDANTERIOR'	=	@ECREDANTERIOR,
		'FCSTAMP'		=	fc.fcstamp, 
		'NO'			=	fc.no, 
		'NOME'			=	fc.nome, 
		'ESTAB'			=	fc.estab, 
		'CMDESC'		=	fc.cmdesc, 
		'ADOC'			=	case when fc.postamp = '' Then
								fc.adoc
							ELSE
								convert(varchar(20),po.rno)
							END, 
		'DOCDATA'		=	case when fc.postamp = '' Then
								convert(varchar,isnull(fo.docdata,'19000101'),102)
							Else
								convert(varchar,isnull(po.rdata,'19000101'),102)
							end, 
		'DATALC'		=	convert(varchar,fc.datalc,102),
		'DATAVEN'		=	convert(varchar,fc.dataven,102), 
		'ECRED'			=	fc.ecred, 
		'EDEB'			=	fc.edeb, 
		'SALDO'			=	case when @calcSaldo=0 then 0 else
							ISNULL(
									(Select	
										SUM(ecred) - SUM(edeb) 
									From	
										#dadosSaldo a 
									Where	
										a.no = @No and a.estab=@estab 
										and (a.datalc + a.ousrhora <= fc.datalc + fc.ousrhora)
									)
							,0) end,
		'MOEDA'			=	fc.moeda, 
		'OUSRHORA'		=	fc.ousrhora, 
		'USERNAME'		=	(Select top 1 username from b_us where fc.ousrinis=b_us.iniciais),
		'OBS'			=	fc.obs,
		'OUSRINIS'		=	fc.ousrinis,
		'SALDOPERIODO'	=	(select saldoanterior from #dadosSaldoAnterior),
		'ULTDOC'		=	fc.ultdoc,
		'INTID'			=	fc.intid,
		'VERDOC'		=	CONVERT(bit,0),
		'ORIGEM'		=	fc.origem
	from
		fc (nolock)
		left join fo (nolock) on fc.fostamp = fo.fostamp
		left join po (nolock) on po.postamp = fc.postamp
		left join #dadosFacturaResumo ffn (nolock)  on ffn.fostamp = fo.fostamp
	where
		fc.no = @No	
		and	fc.estab = @estab 
		And	fc.datalc between @Dataini and @Datafim
		and ffn.fostamp is null
		and (
			fo.site = case when @site = '' then fo.site else @site end
			or
			po.site = case when @site = '' then po.site else @site end
			or 
				(fo.site is null and po.site is null)
			)

	order by
		datalc,ousrhora



		

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFc'))
		DROP TABLE #dadosFc
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSaldoAnterior'))
		DROP TABLE #dadosSaldoAnterior
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSaldo'))
		DROP TABLE #dadosSaldo

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFacturaResumo'))
		DROP TABLE #dadosFacturaResumo
	


Go
Grant Execute on dbo.up_fornecedores_cc to Public
Grant Control on dbo.up_fornecedores_cc to Public
Go