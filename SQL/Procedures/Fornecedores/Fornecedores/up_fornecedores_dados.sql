-- exec	up_fornecedores_dados 208, 0
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_fornecedores_dados]') IS NOT NULL
	drop procedure dbo.up_fornecedores_dados
go

Create procedure dbo.up_fornecedores_dados

@No			NUMERIC(10),
@estab		NUMERIC(4)

/* with encryption */
AS
SET NOCOUNT ON

select	*
from fl (nolock) 
where fl.no=@No and fl.estab=@estab

Go
Grant Execute on dbo.up_fornecedores_dados to Public
Grant Control on dbo.up_fornecedores_dados to Public
Go