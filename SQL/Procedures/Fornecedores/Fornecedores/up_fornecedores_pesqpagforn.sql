-- exec	up_fornecedores_pesqpagforn 0,'',0,-1,'19000101','20140101'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_fornecedores_pesqpagforn]') IS NOT NULL
	drop procedure dbo.up_fornecedores_pesqpagforn
go

Create procedure dbo.up_fornecedores_pesqpagforn

@No			NUMERIC(10),
@fornecedor	varchar(55),
@fornno		numeric(10),
@fornestab	NUMERIC(3),
@datainicio	datetime,
@datafim	datetime

/* with encryption */
AS
SET NOCOUNT ON

select	
	CONVERT(bit,0) as sel
	,rno
	,convert(varchar,rdata,102) as rdata
	,nome
	,no
	,estab
	,etotal
	,postamp
from 
	po (nolock) 
where 
	rno = case when @No = 0 then rno else @No end
	and nome like case when @fornecedor = '' then nome else '%' + @fornecedor + '%' end
	and no = case when @fornno = 0 then no else @fornno end
	and estab = case when @fornestab = -1 then estab else @fornestab end
	and rdata between @datainicio and @datafim

Go
Grant Execute on dbo.up_fornecedores_pesqpagforn to Public
Grant Control on dbo.up_fornecedores_pesqpagforn to Public
Go