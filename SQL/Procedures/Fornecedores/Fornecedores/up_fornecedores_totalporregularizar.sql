/*

 exec	up_fornecedores_totalporregularizar 8,0

 */
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_fornecedores_totalporregularizar]') IS NOT NULL
	drop procedure dbo.up_fornecedores_totalporregularizar
go

Create procedure dbo.up_fornecedores_totalporregularizar

@no		numeric(10),
@estab	numeric(3)

/* with encryption */
AS
SET NOCOUNT ON

Select 
	total = sum(ecred - abs(ecredf)) - sum(edeb - edebf)
from 
	fc (nolock)
where 
	fc.no = @no --n� fornecedor
	and fc.estab = @estab --n� estab
	And abs((fc.ecred-fc.ecredf)-(fc.edeb-fc.edebf)) > 0.00

Go
Grant Execute on dbo.up_fornecedores_totalporregularizar to Public
Grant Control on dbo.up_fornecedores_totalporregularizar to Public
Go