/* Procurar Fornecedores

	exec up_fornecedores_pesquisarKeiretsu '', 0, 0, '', '', '', 0, '3761780', 1

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_fornecedores_pesquisarKeiretsu]') IS NOT NULL
	drop procedure dbo.up_fornecedores_pesquisarKeiretsu
go

create procedure dbo.up_fornecedores_pesquisarKeiretsu

@nome		varchar(55)
,@no		numeric(10, 0)
,@estab		numeric(3)
,@tipo		varchar(20)
,@ncont		varchar(20)
,@tele		varchar(60)
,@inactivo	bit
,@ref		varchar(18)
,@site_nr tinyint

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

	;with 
	
	cteExistencia5Baratos as (
		select
			existeNos5Baratos = sum(case when ranking = 1 then 1 else 0 end),
			c.grphmgcode
		from 
			st (nolock) as a
			inner hash join fpreco (nolock) as b on a.ref=b.cnp and b.grupo='pvp'
			inner hash join fprod (nolock) as c on a.ref=c.cnp and (c.grphmgcode!='GH0000' or c.grphmgcode!='')
		where
			ranking = 1
			and a.site_nr = @site_nr 
		group by
			grphmgcode
	),
	cteMelhorBB as (

		select 
			* 
		from (
				select
					id = ROW_NUMBER() over (partition by c.grphmgcode order by c.grphmgcode, a.marg4 desc)
					,a.ref,
					a.design,
					epv1,
					b.epreco,
					c.pvpmaxre,
					c.ranking,
					c.grphmgcode,
					a.marg4
				from 
					st (nolock) as a
					inner hash join fpreco (nolock) as b on a.ref=b.cnp and b.grupo='pvp'
					inner hash join fprod (nolock) as c on a.ref=c.cnp and (c.grphmgcode!='GH0000' or c.grphmgcode!='')
				where
					ranking = 1
					and a.site_nr = @site_nr 
			) as x 
		where
			x.id = 1
	),
	cteEsgotados as (
		select * 
		from 
			(
			Select	
				contagem = ROW_NUMBER() over (partition by ref order by ref)
				,id = ROW_NUMBER() over (partition by ref,fo.no,fo.estab order by fo.data desc,fo.usrhora desc)
				,fn.ref
				,fn.u_upc
				,fo.no
				,fo.estab
				,fo.data
				,fn.qtt
				,fo.usrhora
			From
				fo (nolock)
				inner join fn (nolock) on fn.fostamp = fo.fostamp and fo.doccode IN (55,101,102) and fn.ref = @ref
				Inner join cm1 (nolock) on cm1.cmdesc = fn.docnome and cm1.FOLANSL = 1
			) x
		where
			x.id = 1
			and x.qtt=0 
	),
	cteBonus as (
		Select 
			*
		From(
			Select 
				contagem = ROW_NUMBER() over (partition by ref order by ref)
				,id = ROW_NUMBER() over (partition by ref,bo.no,bo.estab order by bo.dataobra desc,bo.usrhora desc)
				,bi.ref
				,bonus = bi.lobs
				,bo.no
				,bo.estab
				,bo.dataobra
			from 
				bo (nolock)
				inner join bi (nolock) on bi.bostamp = bo.bostamp and bo.ndos = 35 and bi.ref = @ref
			Where	
				MONTH(bo.dataobra) = MONTH(GETDATE())
				and YEAR(bo.dataobra) = YEAR(GETDATE())
				
		) x
		where x.id = 1
	)
	,ctePCLForn as (
		Select 
			*
			 ,melhorPreco = dense_rank() over (partition by ref order by u_upc)
		from 
			(
			Select	
				id = ROW_NUMBER() over (partition by bi.ref,bo.no,bo.estab order by bo.dataobra desc,bo.usrhora desc)
				,bi.ref
				,u_upc = bi.binum2
				,bo.no
				,bo.estab
				,data = bo.dataobra
				,bi.qtt
				,bo.usrhora
				,bi.u_stockact
			From
				bo (nolock)
				inner join bi (nolock) on bo.bostamp = bi.bostamp	
			where
				bo.ndos = 45
				and bi.ref = @ref
			) x
		where
			x.id = 1	
	),
	/* CTE os dados da ref */
	cte_st as (
		select
			st.ststamp
			,st.ref
			,grphmgcode		= isnull(fprod.grphmgcode,'')
			,grphmgdescr	= isnull(fprod.grphmgdescr,'')
			,st.marg4
		from
			st (nolock)
			left join fprod (nolock)	on fprod.cnp=st.ref
		where
			st.ref = @ref 
			and st.site_nr = @site_nr 

	)	
	,cteDados as (

		Select 
			 sel		= CONVERT(bit,0)
			,fl.nome
			,fl.[no]
			,fl.estab
			,no2		= fl.u_no
			,local
			,telefone
			,tlmvl
			,codpost
			,ncont
			,esaldo		= case
							when fl.estab = 0
							then esaldo
							else isnull((select
											sum(edeb-edebf)
										from
											fc (nolock)
										where
											fc.no=fl.no and fc.estab=fl.estab
											and	(case when fc.moeda='PTE ou EURO' or fc.moeda=space(11)
													then abs((fc.edeb-fc.edebf)-(fc.ecred-fc.ecredf))
													else abs((fc.debm-fc.debfm)-(fc.credm-fc.credfm)) end)
													>								
												(case when fc.moeda='PTE ou EURO' or fc.moeda=space(11)
													then 0.010000
													else 0 end))
										,0)
						end
			,tpdesc
			,eplafond
			,fl.tipo
			,flstamp
			,pais
			,morada
			,ccusto
			,zona
			,bonus	= isnull(cteBonus.bonus,'') /*isnull((Select top 1 bonus from cteBonus where st.fornec = cteBonus.no and st.fornestab = cteBonus.estab and cteBonus.ref = st.ref order by cteBonus.dataobra desc),'') */
			,pcl	= isnull(ctePCLForn.u_upc,0)
			,Esgotado	=   case when isnull(ctePCLForn.u_stockact,0) > 0 then convert(varchar,ctePCLForn.u_stockact) ELSE 'SIM' END
			,alertaStockGH		= case 
							when (st.grphmgcode='GH0000' or st.grphmgcode='' or st.grphmgcode is null or st.marg4 = 0) then 0 
							when cteMelhorBB.ref is not null then 0 
							when cteExistencia5Baratos.grphmgcode is null then 0
							else 1 
						end
			,alertaPCL			= case when ctePCLForn.melhorPreco = 1 or ctePCLForn2.ref is null then 0 else 1 end 
			,alertaBonus		= case when cteBonus2.ref is not null and cteBonus.ref is null then 1 else 0 end
			,ctePCLForn.melhorPreco
			,tabiva
		From
			fl (nolock)
			left join ctePCLForn on ctePCLForn.ref = @ref  and fl.no = ctePCLForn.no and fl.estab = ctePCLForn.estab 
			left join ctePCLForn as ctePCLForn2 on ctePCLForn2.ref = @ref and ctePCLForn2.u_upc != 0
			left join cteBonus on cteBonus.ref = @ref  and fl.no = cteBonus.no and fl.estab = cteBonus.estab 
			left join cteBonus as cteBonus2 on cteBonus2.ref = @ref and cteBonus2.contagem = 1
			left join cteEsgotados on cteEsgotados.ref = @ref and fl.no = cteEsgotados.no and fl.estab = cteEsgotados.estab
			left join cte_st as st on ctePCLForn.ref = st.ref
			left join cteMelhorBB on st.ref = cteMelhorBB.ref
			left join cteExistencia5Baratos on st.grphmgcode = cteExistencia5Baratos.grphmgcode
		Where
			fl.nome like @nome + '%'
			and fl.[no]	= case when @no = 0 then fl.no else @no end
			and fl.estab = case when @estab = 999 then fl.estab else @estab end
			AND (telefone = case when @tele = '' then telefone else @tele end or tlmvl = case when @tele = '' then tlmvl else @tele end)
			AND ncont = case when @ncont='' then ncont else @ncont end
			AND inactivo = case when @inactivo = 1 then fl.inactivo else @inactivo end
		)
		
		Select 
			* 
		from 
			cteDados
		group by sel, nome, [no], estab, no2,local,telefone,tlmvl,codpost,ncont,esaldo,tpdesc,eplafond,tipo,flstamp,pais,morada,ccusto,zona,bonus,pcl,esgotado,alertaStockGH,alertaPCL,alertaBonus,melhorPreco,tabiva
		Order by
			case when pcl = 0 then 9999999 else pcl end asc, bonus desc, nome, no


GO
Grant Execute on dbo.up_fornecedores_pesquisarKeiretsu to Public
Grant Control on dbo.up_fornecedores_pesquisarKeiretsu to Public
Go