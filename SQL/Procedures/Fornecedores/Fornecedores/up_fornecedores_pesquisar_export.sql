/* Procurar Fornecedores

	 exec up_fornecedores_pesquisar_export 1

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_fornecedores_pesquisar_export]') IS NOT NULL
	drop procedure dbo.up_fornecedores_pesquisar_export
go

create procedure dbo.up_fornecedores_pesquisar_export

@inactivo	bit

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON
	select numero, nome, contribuinte, estab, telefone, telemovel, tipo, pais, morada, localidade, codigopostal, moeda from
		(select
			ordem			= 1
			,numero			= 'NO'
			,nome			= 'NOME'
			,contribuinte	= 'NCONT'
			,estab			= 'ESTAB'
			,telefone		= 'TELEFONE'
			,telemovel		= 'TLMV'
			,tipo			= 'TIPO'
			,pais			= 'PAIS'
			,morada			= 'MORADA'
			,localidade		= 'LOCAL'
			,codigopostal	= 'CODPOST'
			,moeda			= 'MOEDA'
		union all 
		select 
			ordem			= 2
			,numero			= str(fl.no,15,0)
			,nome			= fl.nome
			,contribuinte	= ncont
			,estab			= str(fl.estab,15,0)
			,telefone		= telefone
			,telemovel		= tlmvl
			,tipo			= fl.tipo
			,pais			= case when fl.pais=1 then 'NACIONAL' else (case when fl.pais=2 then 'UE' else 'OUTROS' end) end
			,morada			= morada
			,localidade		= local
			,codigopostal	= codpost
			,moeda			= moeda
		From
			fl (nolock)
		Where
			inactivo	= case when @inactivo = 1 then fl.inactivo else @inactivo end
		)x
	Order by
		ordem, nome, numero

GO
Grant Execute on dbo.up_fornecedores_pesquisar_export to Public
Grant Control on dbo.up_fornecedores_pesquisar_export to Public
Go