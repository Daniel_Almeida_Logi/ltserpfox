-- exec	up_fornecedores_pagpendentesforn 13,0
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_fornecedores_pagpendentesforn]') IS NOT NULL
	drop procedure dbo.up_fornecedores_pagpendentesforn
go

Create procedure dbo.up_fornecedores_pagpendentesforn

@no		numeric(10),
@estab	numeric(3)

/* with encryption */
AS
SET NOCOUNT ON

Select 
	convert(bit, 0) as sel
	,valorNreg = (ecred - abs(ecredf)) - (edeb - edebf)
	--,convert(varchar,datalc,102) as datadoc
	,(SELECT convert(varchar,docdata,102) FROM fo(nolock) where fo.fostamp = fc.fostamp) as datadoc
	,convert(varchar,dataven,102) as datavencimento
	,regularizar = (ecred - abs(ecredf)) - (edeb - edebf)
	,convert(bit, 0) as jaincluido
	,(select sum(erec) erec from pl (nolock) where pl.fcstamp = fc.fcstamp) as inclPag
	,*
from 
	fc (nolock)
where 
	fc.no = @no --n� fornecedor
	and fc.estab = @estab --n� estab
	And abs((fc.ecred-fc.ecredf)-(fc.edeb-fc.edebf)) > 0.00
order by 
	datalc
	,dataven

Go
Grant Execute on dbo.up_fornecedores_pagpendentesforn to Public
Grant Control on dbo.up_fornecedores_pagpendentesforn to Public
Go