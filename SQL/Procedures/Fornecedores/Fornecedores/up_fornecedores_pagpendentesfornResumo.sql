-- exec	up_fornecedores_pagpendentesfornResumo 'ADMC4DBEDF0-2B41-4B47-BF4'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_fornecedores_pagpendentesfornResumo]') IS NOT NULL
	drop procedure dbo.up_fornecedores_pagpendentesfornResumo
go

Create procedure dbo.up_fornecedores_pagpendentesfornResumo
@fostamp as varchar(25)

/* with encryption */
AS
SET NOCOUNT ON

Select 
	convert(bit, 0) as sel
	,valorNreg = (ecred - abs(ecredf)) - (edeb - edebf)
	,convert(varchar,datalc,102) as datadoc
	,convert(varchar,dataven,102) as datavencimento
	,regularizar = (ecred - abs(ecredf)) - (edeb - edebf)
	,convert(bit, 0) as jaincluido
	,(select sum(erec) erec from pl (nolock) where pl.fcstamp = fc.fcstamp) as inclPag
	,*
from 
	fc (nolock)
where 
	fc.fostamp = @fostamp
	And abs((fc.ecred-fc.ecredf)-(fc.edeb-fc.edebf)) > 0.00
order by 
	datalc
	,dataven

	

Go
Grant Execute on dbo.up_fornecedores_pagpendentesfornResumo to Public
Grant Control on dbo.up_fornecedores_pagpendentesfornResumo to Public
Go