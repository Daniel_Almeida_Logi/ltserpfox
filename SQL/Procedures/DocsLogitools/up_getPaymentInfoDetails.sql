
/****** Object:  StoredProcedure [dbo].[up_getDocsFailedToSendX3]    Script Date: 11/11/2022 14:41:57 
exec up_getPaymentInfoDetails
******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
		
if OBJECT_ID('[dbo].[up_getPaymentInfoDetails]') IS NOT NULL
	drop procedure dbo.up_getPaymentInfoDetails
go

CREATE PROCEDURE [dbo].[up_getPaymentInfoDetails]
/* WITH ENCRYPTION */
AS

	if OBJECT_ID('tempdb.dbo.#tablePaymentInfoDetails') is not null
		drop table #tablePaymentInfoDetails


	select 
			senderId , 
			site,
			operationID,
			urlPost,
			ousrdata
	INTO #tablePaymentInfoDetails
	from LTSMSB.dbo.paymentMethodInfo (nolock) 
	where finalState=1

	order by  
	paymentMethodInfo.senderId, paymentMethodInfo.site 

		--	(CASE WHEN erro1!=''  THEN 'Foi enviado para o X3 e deu o erro:  ' + erro1 + ' ftstamp: ' + ftstamp + ' ftstamp: ' + ftstamp
		--	  WHEN falta !='' THEN 'Falta integrar a tabela ' + falta 
		--
		--else '' end) AS
	SELECT 
		' <u><b>Pagamentos em Falta:</b></u> ' + CONVERT(VARCHAR(30), COUNT(*)) 	 as descricao,
		' <u>� os Pagamentos que ainda n�o est�o num estado final</u> '  as info_doc,
	    '<u>data:</u>' +  CONVERT(VARCHAR(30),GETDATE(), 121)  as dataHora
	FROM #tablePaymentInfoDetails
	UNION ALL
	SELECT top 200
				'<u>senderId: </u>'  + senderId + '<u> site:</u>' + [site]   as descricao,
		' <u>operationID:</u> ' + operationID + '     <u>urlPost:</u> ' + urlPost   as info_doc,
	    '<u>data:</u>' +  CONVERT(VARCHAR(30),ousrdata, 121)  as dataHora

	FROM #tablePaymentInfoDetails


	if OBJECT_ID('tempdb.dbo.#tablePaymentInfoDetails') is not null
		drop table #tablePaymentInfoDetails

GO
GRANT EXECUTE ON dbo.up_getPaymentInfoDetails to Public
GRANT CONTROL ON dbo.up_getPaymentInfoDetails to Public
GO