USE [mecofarma]
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_utente_loja_loja_atendimento]    Script Date: 13/03/2023 10:27:13 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE [dbo].[sp_insert_utente_loja_loja_atendimento]
@stamp		varchar(50)
,@nome		varchar(100)
,@ncont		varchar(20) 
,@morada	varchar(150)
,@tlmvl		varchar(30)
,@nbenef	varchar(30)
,@email		varchar(30)
,@bino		varchar(30)
,@sexo		varchar(5) = ''
,@dtnasc	varchar(10) = '19000101'
--,@nopref	numeric(15,0)
,@clno		numeric(15,0)
AS
BEGIN
	declare @nextclno as numeric(15,0)
	--set @nextclno = (select isnull(max(no)+1,@clno*1000000+1) as no from b_utentes where no>@clno*1000000)
	set @nextclno = (select isnull(max(no)+1,@clno*1000000+1) as no from b_utentes where no between @clno*1000000 and (@clno*1000000)+999999)
	print @nextclno
	SET NOCOUNT ON;
	SET XACT_ABORT ON
			INSERT INTO  dbo.b_utentes (utstamp, no, estab, nome ,ncont ,morada ,tlmvl ,nbenef ,email , bino, sexo, nascimento, moeda, codigop, descp, codigopnat, descpnat )
			SELECT @stamp
				, @nextclno
				,0
				,@nome
				,@ncont
				,@morada
				,@tlmvl
				,@nbenef
				,@email
				,@bino
				,@sexo
				,@dtnasc
				,(select textvalue from B_Parameters where stamp='ADM0000000260')
				,(select textvalue from B_Parameters where stamp='ADM0000000260')
				,(select top 1 textvalue from B_Parameters_site where stamp='ADM0000000050')
				,(select textvalue from B_Parameters where stamp='ADM0000000260')
				,(select top 1 textvalue from B_Parameters_site where stamp='ADM0000000050')  
		SET XACT_ABORT OFF
select no from b_utentes where utstamp=@stamp
print 'cliente criados'
END