
-- exec up_utentes_TiposCompart
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_TiposCompart]') IS NOT NULL
    drop procedure up_utentes_TiposCompart
go

create PROCEDURE up_utentes_TiposCompart


/* WITH ENCRYPTION */
AS

	Select tipocompart = 'Valor' 
	
	Union all
	
	Select 'Percent'
	
GO
Grant Execute On up_utentes_TiposCompart to Public
Grant Control On up_utentes_TiposCompart to Public
go


