
-- exec up_utentes_BioParametros
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_BioParametros]') IS NOT NULL
    drop procedure up_utentes_BioParametros
go

create PROCEDURE up_utentes_BioParametros


/* WITH ENCRYPTION */
AS

	select  * from bioparametros order by nome 

	
GO
Grant Execute On up_utentes_BioParametros to Public
Grant Control On up_utentes_BioParametros to Public
go