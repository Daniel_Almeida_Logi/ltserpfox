SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('[dbo].[up_utentes_syncRNURecm]') IS NOT NULL
    DROP PROCEDURE dbo.up_utentes_syncRNURecm;
GO

CREATE PROCEDURE dbo.up_utentes_syncRNURecm
    @token AS VARCHAR(100) -- Par�metro: token usado para identificar o utente
AS
BEGIN
    /* Common Table Expression (CTE) para gerar a lista concatenada de c�digos */
    WITH CodeListCTE AS (
        SELECT 
            STUFF((
                SELECT ',' + CAST(r.code AS VARCHAR(254))
                FROM ext_RNU_RespHealthU_RECM r (NOLOCK)
                INNER JOIN ext_RNU_RespHealthU u (NOLOCK) 
                    ON u.healthExemptionRECMToken = r.token
                WHERE u.token = @token
                FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') AS codeAsList
    )
    SELECT 
        r.code, -- C�digo de benef�cio
        r.codeDesc, -- Descri��o do benef�cio
        convert(VARCHAR(254),c.codeAsList) as codeAsList -- Lista concatenada de todos os c�digos relacionados ao token
    FROM 
        ext_RNU_RespHealthU_RECM r WITH (NOLOCK)
    INNER JOIN 
        ext_RNU_RespHealthU u WITH (NOLOCK) 
        ON u.healthExemptionRECMToken = r.token
    CROSS JOIN 
        CodeListCTE c -- Junta a lista concatenada com os resultados principais
    WHERE 
        u.token = @token -- Filtra os resultados principais pelo token
        AND u.token != '' -- Garante que o token n�o est� vazio
END
GO

-- Concede permiss�o para executar a procedure
GRANT EXECUTE ON dbo.up_utentes_syncRNURecm TO Public;
GO
