

-- exec up_utentes_registoClinico ''
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_registoClinico]') IS NOT NULL
    drop procedure up_utentes_registoClinico
go

create PROCEDURE up_utentes_registoClinico
@utstamp varchar(25),
@userini varchar(5)




/* WITH ENCRYPTION */
AS

		with dados (regstamp
		, utstamp
		, data
		, hora
		, ousrinis
		, ousrdata
		, ousrhora
		, usrinis
		, usrdata
		, usrhora
		, s
		, o
		, a
		, p
		, prescno
		,tipo
		,confidencial )
	as 
	(
	Select 
		regstamp
		, utstamp
		, data
		, hora
		, ousrinis
		, ousrdata
		, ousrhora
		, usrinis
		, usrdata
		, usrhora
		, s
		, cast (o as varchar(254)) as o
		, cast(a as varchar(254)) as a
		, cast(p as varchar(254)) as p
		, '' as prescno
		,tipo
		,confidencial
	from 
		registoClinico 
	Where
		utstamp = @utstamp
		and confidencial = 0
		and confidencial = (case when @userini = registoClinico.ousrinis
				then
					1
				else
					0
				end )
	union
	select
		cast(prescstamp as varchar(30)) as regstamp
		, cast(utstamp as varchar(30)) as utstamp
		, ousrdata as data
		, ousrhora as hora
		, ousrinis
		, ousrdata
		, ousrhora
		, usrinis
		, usrdata
		, usrhora
		, 'Prescrição nr. ' + cast(nrprescricao as varchar(30)) as s
		, cast('' as varchar(254)) as o
		, cast('' as varchar(254)) as a
		, cast('' as varchar(254)) as p
		, nrprescricao as prescno
		,'' as tipo
		,confidencial

	from 
		B_cli_presc 	
	Where
		utstamp = @utstamp
		and confidencial = 0
		and confidencial = (case when @userini = B_cli_presc.ousrinis
				then
					1
				else
					0
				end )
	)
	select * from dados order by ousrdata, ousrhora
GO
Grant Execute On up_utentes_registoClinico to Public
Grant Control On up_utentes_registoClinico to Public
go
