/*
sp para select dos dados da tabela Telefones_Utente_Fornecedor,
para utentes e fornecedores
a colocar num form no logitools

20200925, JG, v1

input: no, estab, tipo
output: nome, telefone, stamp (para futuros updates, chave primaria)


exec up_Telefones_Utente_Fornecedor_Select 10783196,0,'U'
exec up_Telefones_Utente_Fornecedor_Select 3037,0,'F'

exec up_Telefones_Utente_Fornecedor_Select null,null,null
exec up_Telefones_Utente_Fornecedor_Select '','',''

*/


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_Telefones_Utente_Fornecedor_Select]') IS NOT NULL
    drop procedure up_Telefones_Utente_Fornecedor_Select
go

CREATE PROCEDURE up_Telefones_Utente_Fornecedor_Select		
	  @no					INT
	, @estab				INT	
	, @tipo					VARCHAR(1)
	
AS

SET @no			=   rtrim(ltrim(isnull(@no,0)))
SET @estab		=   rtrim(ltrim(isnull(@estab,0)))	
SET @tipo		=   rtrim(ltrim(isnull(@tipo,'')))
	
select 	* from Telefones_Utente_Fornecedor where 
	no = isnull(@no,0) and estab=isnull(@estab,0) and tipo=isnull(@tipo,'') 

GO
GRANT EXECUTE on dbo.up_Telefones_Utente_Fornecedor_Select TO PUBLIC
GRANT Control on dbo.up_Telefones_Utente_Fornecedor_Select TO PUBLIC
GO