/* sp pesquisa prescricao eletronica

	 exec up_utentes_pesquisa 'ADMFA55AE371EB9469DAD66CC ','', '','', ''
*/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_pesquisa]') IS NOT NULL
    drop procedure up_utentes_pesquisa
go

create PROCEDURE up_utentes_pesquisa

@utstamp as varchar(25)
,@nome		varchar(55)
,@nbenef2	nvarchar(100)
,@nbenef	nvarchar(100)
,@ncont		varchar(20)


/* WITH ENCRYPTION */
AS

	Select 
		 Sel		= CONVERT(bit,0)
		,utstamp
		,nome		
		,no			
		,estab		
		,tlmvl		
		,telefone	
		,nrcartao
		,nbenef
		,nbenef2
		,idade		= case when year(nascimento) != 1900 then (CONVERT(int,CONVERT(char(8),GETDATE(),112))-CONVERT(char(8),nascimento,112))/10000 else 0 end
		,inactivo
		,nascimento
		,morada		
		,local		
		,codpost
		,ncont		
		,codigop	
		,tipo		
		,ccusto
		,zona
		,desconto
		,nocredit
		,modofact
		,entfact
		,profi
		,sexo
		,obs
		,email
		,entpla
		,clstamp = utstamp
		,descp
		,codigop
		,codigopnat
		,bino
		,idstamp
		,tratamento
		,ninscs
		,csaude
		,drfamilia
		,tlmvl
		,zona
		,tipo
		,sexo
		,email
		,codigop
		,mae
		,pai
		,pacgen
		,ligado = case when idstamp = '' then CONVERT(bit,0) else CONVERT(bit,1) end
		,cesd
		,cesdidp
		,cesdcart
		,no_ext
	from 
		b_utentes (nolock)
	where
		utstamp = case when @utstamp = '' then utstamp else @utstamp end
		and nome like @nome + '%'
		and ncont = case when @ncont = '' then ncont else @ncont end
		and nbenef2 = case when @nbenef2 = '' then nbenef2 else @nbenef2 end
		and nbenef = case when @nbenef = '' then nbenef else @nbenef end 
		and removido = 0
	
GO
Grant Execute On up_utentes_pesquisa to Public
Grant Control On up_utentes_pesquisa to Public
go
