
/*

Actualiza definições de rgpd

select tokenaprovacao,autoriza_emails,autoriza_sms,* from b_utentes(nolock) where no='1078310' 


exec up_update_utente_rgpd 810969, '1', '1'



*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_update_utente_rgpd]') IS NOT NULL
	drop procedure dbo.up_update_utente_rgpd
go



create procedure dbo.up_update_utente_rgpd

 @rgpdToken as numeric(16,0)
 ,@rgpEmail  as varchar(1)
 ,@rgpSms as varchar(1)


/* WITH ENCRYPTION */
AS


	declare @utno numeric(10,0)

	select @utno=(select 
						top 1 no 
						from 
							b_utentes(nolock) 
						where
							b_utentes.tokenaprovacao = @rgpdToken				
						order by
							no
						desc
					 )
	
	declare @utestab numeric(10,0)

	select @utestab=(select 
						top 1 estab 
						from 
							b_utentes(nolock) 
						where
							b_utentes.tokenaprovacao = @rgpdToken				
						order by
							no
						desc
					 )

	update
		b_utentes
	set				
		autoriza_emails = case when @rgpEmail='' then autoriza_emails else @rgpEmail end
		,autoriza_sms = case when @rgpSms='' then autoriza_sms else @rgpSms end
		,autorizado = 1
		
	where
		b_utentes.no = ltrim(rtrim(@utno))
		and b_utentes.estab = ltrim(rtrim(@utestab))
								
 
GO
Grant Execute On dbo.up_update_utente_rgpd to Public
Grant Control On dbo.up_update_utente_rgpd to Public
GO
