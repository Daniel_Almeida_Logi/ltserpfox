
-- exec up_utentes_Entidades 0,0
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_Entidades]') IS NOT NULL
    drop procedure up_utentes_Entidades
go

create PROCEDURE up_utentes_Entidades
@utstamp varchar(25)

/* WITH ENCRYPTION */
AS

	Select 
		B_entidadesUtentes.* 
	from 
		B_entidadesUtentes 
	Where
		B_entidadesUtentes.utstamp = @utstamp
	
GO
Grant Execute On up_utentes_Entidades to Public
Grant Control On up_utentes_Entidades to Public
go



