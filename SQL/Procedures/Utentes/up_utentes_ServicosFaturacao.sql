/*
 
	exec up_utentes_ServicosFaturacao '', 1
	select * from cl_servicos
	
*/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_ServicosFaturacao]') IS NOT NULL
    drop procedure up_utentes_ServicosFaturacao
go

create PROCEDURE up_utentes_ServicosFaturacao
@utstamp varchar(25),
@site_nr tinyint

/* WITH ENCRYPTION */
AS

	select
		st.ref
		,st.design
		,cls.fatura
		,sel = convert(bit,0)
		,cls.data
		,cls.desvio_mes_fat
		,cls.preco
		,cls.desconto
		,cls.desconto_valor
		,cls.qtt
	from
		b_utentes (nolock)
		inner join cl_servicos cls (nolock) on b_utentes.no = cls.nr_cl and b_utentes.estab = cls.dep_cl
		inner join st (nolock) on st.ref = cls.ref and st.site_nr = @site_nr
	where
		b_utentes.utstamp = @utstamp
	order by
		cls.data asc
		
		
			
GO
Grant Execute On up_utentes_ServicosFaturacao to Public
Grant Control On up_utentes_ServicosFaturacao to Public
go



