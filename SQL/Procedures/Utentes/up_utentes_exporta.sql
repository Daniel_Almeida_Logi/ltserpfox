
-- exec up_utentes_exporta 'ADM5F310C87-15D9-4BBE-B97'
-- select * from b_utentes where nome like 'Jos�%'

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_exporta]') IS NOT NULL
    drop procedure up_utentes_exporta
go

create PROCEDURE up_utentes_exporta

@utstamp as varchar(25)

/* WITH ENCRYPTION */
AS

	select 
		no as 'N�mero'
		, nome as 'Nome'
		, morada as 'Morada'
		, Local as 'Localidade'
		, codpost as 'Codigo Postal'
		, telefone as 'Telefone'
		, tlmvl as 'Telemovel'
		, fax as 'Fax'
		, email as 'Email'
		, ncont as 'Contribuinte'
		, nbenef as 'Benificiario'
		, (case when year(nascimento)=1900 then '' else CONVERT(varchar, nascimento, 102) end) as 'Data Nascimento'
	from 
		b_utentes (nolock)
	where
		utstamp = @utstamp
		
	
GO
Grant Execute On up_utentes_exporta to Public
Grant Control On up_utentes_exporta to Public
go
