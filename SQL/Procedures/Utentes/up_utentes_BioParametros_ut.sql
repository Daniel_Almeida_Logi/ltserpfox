
-- exec up_utentes_BioParametros_ut 0,0,'ADM755C3937-DA8D-47CA-9A7'
--select * from b_utentes where utstamp = 'ADM755C3937-DA8D-47CA-9A7'
-- select * from bioregistos
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_BioParametros_ut]') IS NOT NULL
    drop procedure up_utentes_BioParametros_ut
go

create PROCEDURE up_utentes_BioParametros_ut
@utno numeric(10,0),
@utestab numeric(10,0) = 0,
@utstamp varchar(30) = ''

/* WITH ENCRYPTION */
AS
	if (@utstamp = '')
	begin 
		set @utstamp = (select utstamp from b_utentes (nolock) where no=@utno and estab=@utestab)
	end  

	select  *
	,(CASE	
		WHEN bioparametros.usaValMin = 1
			THEN isnull((select top 1 (CASE WHEN usaDecimal = 0 then LTRIM(cast(valor as numeric(13,0))) else LTRIM(cast(valor as numeric(13,2))) END) + (CASE WHEN valor_min <> 0 THEN '-' + (CASE WHEN usaDecimal = 0 then LTRIM(cast(valor_min as numeric(13,0))) else LTRIM(cast(valor_min as numeric(12,2))) END) ELSE '' END) from bioregistos (nolock) where bioparametros.id=bioregistos.id_bioparametros and utstamp=@utstamp order by data desc, hora desc),'') 
		ELSE 
			isnull((select top 1 (CASE WHEN usaDecimal = 0 then LTRIM(cast(valor as numeric(13,0))) else LTRIM(cast(valor as numeric(13,2))) END) from bioregistos (nolock) where bioparametros.id=bioregistos.id_bioparametros and utstamp=@utstamp order by data desc, hora desc),'') 
	END) as ultreg
	,isnull((select top 1 CONVERT(varchar, data, 102)+' '+left(hora,5) from bioregistos (nolock) where bioparametros.id=bioregistos.id_bioparametros and utstamp=@utstamp order by data desc, hora desc),'') as ultdata
	from bioparametros (nolock) 
	order by nome 
	

	
GO
Grant Execute On up_utentes_BioParametros_ut to Public
Grant Control On up_utentes_BioParametros_ut to Public
go