/*
  exec up_utentes_probSaude_insertPatologias  198288042, 0, 'Gravidez','ADM'
 */
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('[dbo].[up_utentes_probSaude_insertPatologias ]') IS NOT NULL
    DROP PROCEDURE dbo.up_utentes_probSaude_insertPatologias 
GO

CREATE PROCEDURE dbo.up_utentes_probSaude_insertPatologias 

	@no					AS NUMERIC(20,2),			
	@estab				AS NUMERIC(20,0),
	@desginPatol		AS VARCHAR(254),
	@operador			AS VARCHAR(10)
/* WITH ENCRYPTION */
AS
	
	DECLARE @stampPatol			VARCHAR(36)
	DECLARE @stamputente		VARCHAR(36)

	SELECT
		@stampPatol = b_ctidef.ctiefpstamp
	FROM
		b_ctidef(NOLOCK)
	WHERE
		b_ctidef.descricao = @desginPatol

	SELECT
		@stamputente = b_utentes.utstamp
	FROM
		b_utentes(NOLOCK)
	WHERE
		b_utentes.no = @no
		AND b_utentes.estab = @estab
	

	IF NOT EXISTS(SELECT * FROM b_utentes_patologias(NOLOCK) WHERE b_utentes_patologias.utstamp = @stamputente
																AND b_utentes_patologias.ctiefpstamp = @stampPatol  )
	BEGIN
		INSERT INTO b_utentes_patologias(utstamp, ctiefpstamp,ousrdata, ousrinis, usrdata, usrinis )
		VALUES (@stamputente, @stampPatol, GETDATE(),@operador ,GETDATE(),@operador)
	END
	
	IF OBJECT_ID('tempdb.dbo.#stampPatol') IS NOT NULL
		DROP TABLE #stampPatol

GO
Grant Execute on dbo.up_utentes_probSaude_insertPatologias  to Public
Grant Control on dbo.up_utentes_probSaude_insertPatologias  to Public
go