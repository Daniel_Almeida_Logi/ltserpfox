/****** Object:  StoredProcedure [dbo].[sp_insert_utente_loja_dc]    Script Date: 20/10/2023 09:51:54 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE [dbo].[sp_insert_utente_loja_dc]
@stamp		varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET XACT_ABORT ON
	BEGIN TRY  
	   -- statements that may cause exceptions
		IF(1=1 )
		BEGIN
		  BEGIN TRAN
			insert into b_utentes (utstamp, no, estab, moeda)
			select @stamp, (select max(no)+1 from b_utentes (nolock) where no<1000000),0, (select textValue from B_Parameters(nolock) where stamp = 'ADM0000000260')
		  COMMIT TRAN
		END
	END TRY 
	BEGIN CATCH  
	   ROLLBACK TRAN
	END CATCH  
		SET XACT_ABORT OFF
select max(no) from b_utentes
print 'cliente criados'
END 