/*
PESQUISA CLIENTES DETALHE
J.Gomes, 18-03-2020

Pesquisa detalhe a partir de 5 campos de pesquisa:
	ncont,bino,no_ext ,no ,estab 

O campo inactivo=0 � utilizado na pesquisa.
O resultado ser� sempre uma linha, top 1

Output dos seguintes campos:
	 nome AS name, email, nascimento AS dob
	, CASE WHEN (nascimento IS NULL or nascimento = '1900-01-01 00:00:00.000') then 0 
		else DATEDIFF(hour,nascimento,GETDATE())/8766 END AS age
	, sexo AS gender, tipo AS type
	, altura AS height, peso AS weight, profi AS job, codigop As countryCode, obs
	, dbo.ListaEstab(no) AS deps
	, no, estab, no_ext AS extId, noAssociado As assocNr, id AS assocId, nrcartao AS cardNumber
	, ncont AS vatNr, nbenef AS medicalNr, nrss AS ssNr, bino AS ccNr, tlmvl AS cellphone, telefone AS phone, fax
	, local AS city, morada AS address, codpost AS zipCode, zona AS zone, codigop AS countryCode
	, nocredit AS noCredit, naoencomenda AS noOrder, desconto AS discount, moeda AS currency
	, nib AS iban, codSwift AS swift, cativa AS vatCat, perccativa AS vatPerc, autoriza_sms AS smsAuth
	, autoriza_emails AS emailAuth, codpla AS codPla, despla AS descPla, ousrdata AS creationDate
	, usrdata AS lastChangeDate

FUN�AO a criar dbo.ListaEstab(no):
	Um dos campos a mostrar, "deps", � o resultado de concatena��o dos diferentes valores da variavel "estab"
	para o mesmo valor de "no" nas diferentes linhas, separado por virgula.
	Para tal foi criada a fun��o escalar dbo.ListaEstab(no) com input "no" e retorno de uma lista.

	ex: no=1078970 tem 3 linhas

	CODE:
	select no, estab, dbo.ListaEstab(no) AS deps from b_utentes where no=1078970

	RESULT:
	no			estab	deps
	1078970		0		0,1,2
	1078970		1		0,1,2
	1078970		2		0,1,2


PROCEDURE
Criada procedure up_utentes_PesquisaClientesDetalhe com par�metros: 

Parametros: up_utentes_PesquisaClientesDetalhe 'ncont','bino','no_ext','no','estab'

exec up_utentes_PesquisaClientesDetalhe '','','','10783192','0'

*/



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_PesquisaClientesDetalhe]') IS NOT NULL
    drop procedure up_utentes_PesquisaClientesDetalhe
go

CREATE PROCEDURE up_utentes_PesquisaClientesDetalhe
	@ncont varchar(20), @bino varchar(20), @no_ext varchar(20), @no varchar(10), @estab varchar(3)

AS

DECLARE @sqlCommand VARCHAR(5000) 
/*
SET @ncont = ''
SET @bino = ''
SET @no_ext = ''
--campos numericos na tabela, n�o permitir input de nao numericos
SET @no = ''
SET @estab = ''
--n�o mostrar dados caso n�o se preencha nenhum dos campos: len(@ncont+@bino+@no_ext+@no+@estab)=0
*/

IF (@no='' OR @no NOT LIKE '%[^0-9]%') AND (@estab='' OR @estab NOT LIKE '%[^0-9]%') AND (len(@ncont+@bino+@no_ext+@no+@estab)>0 )
BEGIN
	/*
	SELECT  nome AS name, email, nascimento AS dob
	, CASE WHEN (nascimento IS NULL or nascimento = '1900-01-01 00:00:00.000') then 0 
		else DATEDIFF(hour,nascimento,GETDATE())/8766 END AS age
	, sexo AS gender, tipo AS type
	, altura AS height, peso AS weight, profi AS job, codigop As countryCode, obs
	, dbo.ListaEstab(no) AS deps
	, no, estab, no_ext AS extId, noAssociado As assocNr, id AS assocId, nrcartao AS cardNumber
	, ncont AS vatNr, nbenef AS medicalNr, nrss AS ssNr, bino AS ccNr, tlmvl AS cellphone, telefone AS phone, fax
	, local AS city, morada AS address, codpost AS zipCode, zona AS zone, codigop AS countryCode
	, nocredit AS noCredit, naoencomenda AS noOrder, desconto AS discount, moeda AS currency
	, nib AS iban, codSwift AS swift, cativa AS vatCat, perccativa AS vatPerc, eplafond as plafond
	, autoriza_sms AS smsAuth
	, autoriza_emails AS emailAuth, codpla AS codPla, despla AS descPla, ousrdata AS creationDate
	, usrdata AS lastChangeDate 
	FROM dbo.b_utentes where ( 
		ncont = COALESCE(NULLIF(@ncont,''), ncont) 
		AND bino = COALESCE(NULLIF(@bino,''), bino) 
		AND no_ext = COALESCE(NULLIF(@no_ext,''), no_ext) 
		AND (no = COALESCE(NULLIF(@no,''), no) AND estab = COALESCE(NULLIF(@estab,''), estab))
		) AND inactivo = 0
	--AND no=1078970	
	ORDER by ousrdata desc
	*/

	SET @sqlCommand = 'SELECT top 1 nome AS name, email, nascimento AS dob
	, CASE WHEN (nascimento IS NULL or nascimento = ''' + '1900-01-01 00:00:00.000' +''') then 0 
		else DATEDIFF(hour,nascimento,GETDATE())/8766 END AS age
	, sexo AS gender, tipo AS type, altura AS height, peso AS weight, profi AS job, codigop As countryCode, obs
	, dbo.ListaEstab(no) AS deps
	, no As number, estab As dep, no_ext AS extId, noAssociado As assocNr, id AS assocId, nrcartao AS cardNumber
	, ncont AS vatNr, nbenef AS medicalNr, nrss AS ssNr, bino AS ccNr, tlmvl AS cellphone, telefone AS phone, fax
	, local AS city, morada AS address, codpost AS zipCode, zona AS zone, codigop AS countryCode
	, descp AS countryName
	, nocredit AS noCredit, naoencomenda AS noOrder, desconto AS discount, moeda AS currency
	, nib AS iban, codSwift AS swift, cativa AS vatCat, perccativa AS vatPerc, eplafond as plafond
	, autoriza_sms AS smsAuth, autoriza_emails AS emailAuth, codpla AS codPla, despla AS descPla
	, ousrdata AS creationDate, usrdata AS lastChangeDate
	,rtrim(ltrim(utstamp))													AS regstamp
	FROM dbo.b_utentes 
	where ( 
			ncont = COALESCE(NULLIF(''' + @ncont + ''',''''), ncont) 
			AND bino = COALESCE(NULLIF(''' + @bino + ''',''''), bino) 
			AND no_ext = COALESCE(NULLIF(''' + @no_ext + ''',''''), no_ext) 
			AND (no = COALESCE(NULLIF(''' + @no + ''',''''), no) 
				AND estab = COALESCE(NULLIF(''' + @estab + ''',''''), estab))
			) AND inactivo = 0 
	ORDER by ousrdata desc'
	--PRINT @sqlCommand 
	EXEC (@sqlCommand)
END
/*
ELSE
BEGIN
	print('Tem de preencher pelo menos um campo de pesquisa com valores num�ricos')
END
*/


GO
Grant Execute On up_utentes_PesquisaClientesDetalhe to Public
Grant Control On up_utentes_PesquisaClientesDetalhe to Public
go



/*

-- Codigo para cria��o de fun��o escalar com input do campo "no" e retorno de uma lista
-- com base na tabela b_utentes
-- select dbo.ListaEstab(no)

------------------usando coalesce : escolhida --------------------------
declare @aa varchar (5000)
set @aa = ''

select @aa = 
    case when @aa = ''
    then cast(estab AS nvarchar)
    else @aa + coalesce(',' + cast(estab AS nvarchar), '')
    end
  from b_utentes where no=1078900
print @aa


-----------------------usando xml-------------------------

SELECT LEFT(estab, LEN(estab) - 1)
FROM (
    select cast(estab AS nvarchar) + ', '
    FROM b_utentes
	where no=1078900
    FOR XML PATH ('')
  ) c (estab)
   
  
select no, estab, dbo.ListaEstab(no) from b_utentes  where no=1078900
select no, estab from b_utentes where no=1078900

*/