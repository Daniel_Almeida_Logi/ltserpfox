
-- exec up_utentes_actNbenf
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_actNbenf]') IS NOT NULL
    drop procedure up_utentes_actNbenf
go

create PROCEDURE up_utentes_actNbenf

@utstamp as  varchar(25)

/* WITH ENCRYPTION */
AS

	;with CteBenf as (
		Select 
			top 1 NumeroBenefEntidade, stamp 
		from 
			b_cli_efrutente 
		where 
			stamp = @utstamp
	)
	
	update 
		b_utentes 
	set nbenef = LEFT(NumeroBenefEntidade,18)
	from 
		b_utentes 
		inner join CteBenf on CteBenf.stamp = b_utentes.utstamp
	where 
		utstamp = @utstamp
	
	
		 
	
GO
Grant Execute On up_utentes_actNbenf to Public
Grant Control On up_utentes_actNbenf to Public
go
