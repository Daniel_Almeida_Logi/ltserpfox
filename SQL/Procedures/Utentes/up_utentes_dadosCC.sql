/* SP que consulta informa��o lida atrav�s do leitor de cart�o de cidad�o

	exec up_utentes_dadosCC 'L-LEAL', 'Loja 1', 0

	select * from Dispensa_eletronica_CC

*/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_dadosCC]') IS NOT NULL
    drop procedure up_utentes_dadosCC
go

create PROCEDURE up_utentes_dadosCC
	@machine         as varchar(25)
	,@site			 as varchar(20)
	,@PainelPesquisa as bit

/* WITH ENCRYPTION */
AS

	/* Declare variaveis */
	declare @ncont varchar(15)
	declare @nrSNS int

	-- ncont
	SELECT TOP 1 @ncont = nif
	from 
		Dispensa_eletronica_CC (nolock)
	where 
		Dispensa_eletronica_CC.machine = @machine 
		and Dispensa_eletronica_CC.site = @site
		and lido = 0
		and Dispensa_eletronica_CC.data_alt >= DATEADD(MINUTE,-5, getdate()) 
	order by 
		data_alt desc

	-- nrSNS
	SELECT TOP 1 @nrSNS = nrSNS
	from 
		Dispensa_eletronica_CC (nolock)
	where 
		Dispensa_eletronica_CC.machine = @machine 
		and Dispensa_eletronica_CC.site = @site
		and lido = 0
		and Dispensa_eletronica_CC.data_alt >= DATEADD(MINUTE,-5, getdate()) 
	order by 
		data_alt desc

	-- informacao para cursor fox
	Select 
		top 1 
		id
		,id_emp
		,machine
		,nome
		,morada
		,nif
		,nrSNS
		,sexo
		,convert(datetime,nascimento) as nascimento
		,validade_fim
		,publicKey
		,tipodoc
		,data_alt
		,site
		,lido
	from 
		Dispensa_eletronica_CC (nolock)
	where 
		Dispensa_eletronica_CC.machine = @machine 
		and Dispensa_eletronica_CC.site = @site
		and lido = 0
		and Dispensa_eletronica_CC.data_alt >= DATEADD(MINUTE,-5, getdate())
	order by 
		data_alt desc

	-- valida se existe na BD e se a consulta � feita no painel de pesquisa de utentes
	if exists (select top 1 * from b_utentes (nolock) where ncont = @ncont or nbenef = convert(varchar(18),@nrSNS)) or @PainelPesquisa = 1
	begin
		Update Dispensa_eletronica_CC set lido = 1 where Dispensa_eletronica_CC.machine = @machine and Dispensa_eletronica_CC.site = @site
	end
	
GO
Grant Execute On up_utentes_dadosCC to Public
Grant Control On up_utentes_dadosCC to Public
go

