/*
  exec up_utentes_probSaude_Patologias 198288042, 0
 */
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('[dbo].[up_utentes_probSaude_Patologias]') IS NOT NULL
    DROP PROCEDURE dbo.up_utentes_probSaude_Patologias
GO

CREATE PROCEDURE dbo.up_utentes_probSaude_Patologias

	@no					AS NUMERIC(20,2),			
	@estab				AS NUMERIC(20,0)
/* WITH ENCRYPTION */
AS

	select 
		b_ctidef.descricao
	from
		b_utentes(NOLOCK)
		INNER JOIN b_utentes_patologias(NOLOCK) ON b_utentes_patologias.utstamp = b_utentes.utstamp
		INNER JOIN b_ctidef (NOLOCK) ON  b_utentes_patologias.ctiefpstamp = b_ctidef.ctiefpstamp
	WHERE
		b_utentes.no = @no
		AND b_utentes.estab = @estab

GO
Grant Execute on dbo.up_utentes_probSaude_Patologias to Public
Grant Control on dbo.up_utentes_probSaude_Patologias to Public
go