/*
		SP para sincronizar clientes da Associa��o Sandim para a base de dados da farm�cia para permitir vendas
		
		Notas:
		> Validar se um cliente tem moviemtnso antes de fazer um delete!
		> Crit�rio de sincroniza��o: apenas clientes do estabelecimento 0 (zero) e superioes a 201
		> Campo desconto � gerado por nos quando alimentado no INSERT; o UPDATE Foi comentado para mantermos sempre o valor virtual gerado por nos
		> Campo u_nss foi criado na nossa base de dados id�ntico ao do parceiro PHC
		> Campo u_utenteno j� tinhaamos, foi mapeado ao campo u_utente do parceiro PHC
		> Para sincronizar todos chamar a SP com par�metro "-1"
	*/


SET ansi_nulls on
SET ANSI_WARNINGS on

go

if OBJECT_ID('[dbo].[up_fanse_sync_cl_parametro]') IS NOT NULL
	drop procedure dbo.up_fanse_sync_cl_parametro
go

create procedure dbo.up_fanse_sync_cl_parametro
@clno	numeric(9)

/*with encryption*/
as
begin

	
declare @sql varchar(max)
declare @sql2 varchar(max)
set @sql = N'
	begin try

		declare
			@counter				bigint,
			@errortext				varchar(256),
			@tran					varchar(32),
			@error_severity			int,
			@error_state			int

		select @tran = LEFT(newid(),32)
		begin tran @tran
			
			/* UPDATE */
			SELECT 
				@counter=0,
				@Counter = COUNT(clx.nome collate SQL_Latin1_General_CP1_CI_AI) 
			FROM [SRVASSANDIM].assandim.dbo.cl clx WITH (NOLOCK) 
			inner join b_utentes cly (nolock) on cly.no=clx.no and cly.estab=clx.estab
			where
				(cly.usrdata<>clx.usrdata 
				or cly.usrhora<>clx.usrhora collate SQL_Latin1_General_CP1_CI_AI
				or cly.inactivo<>clx.inactivo
				or cly.nocredit<>clx.nocredit
				or cly.tipo<>clx.tipo collate SQL_Latin1_General_CP1_CI_AI
				or cly.ncont<>clx.ncont collate SQL_Latin1_General_CP1_CI_AI)
				and clx.no =(case when '+convert(varchar,@clno)+' > 0 then '+convert(varchar,@clno)+' else clx.no end)
			
			IF @Counter > 0
				BEGIN
					UPDATE b_utentes SET 
						nome=				clx.nome,
						no=					clx.no,
						morada=				clx.morada,
						local=				clx.local,
						codpost=			clx.codpost,
						conta=				(SELECT TEXTVALUE FROM B_PARAMETERS WHERE STAMP = ''ADM0000000242''),
						telefone=			LEFT(clx.telefone,13),
						fax=				LEFT(clx.fax,13),
						email=				clx.email,
						tlmvl=				left(clx.tlmvl,13),
						ncont=				LEFT(clx.ncont,9),
						zona=				clx.zona,
						desconto =			case when clx.tipo in (''Associado'',''Ass.Medic.'') then 5 else 0 end,
						tipo=				clx.tipo,
						ccusto=				clx.ccusto,
						bino=				clx.bino,
						inactivo=			clx.inactivo,
						obs=				clx.obs,
						nocredit=			clx.nocredit,
						--u_nss=			clx.u_nss,
						nbenef=				LEFT(clx.u_utente,9),
						nome2=				clx.nome2,
						--pais=				clx.pais,
						moeda=				clx.moeda,
						usrhora =			clx.usrhora,
						usrdata =			clx.usrdata, 
						codigoP =			''PT''
						
					FROM b_utentes cly WITH (NOLOCK) 
					INNER JOIN [SRVASSANDIM].assandim.dbo.cl clx WITH (NOLOCK) on cly.no=clx.no and cly.estab=clx.estab 
					where
						(cly.usrdata<>clx.usrdata
						or cly.usrhora<>clx.usrhora collate SQL_Latin1_General_CP1_CI_AI'
set @sql2 = N'
						/* campos podem ser alterados manualmente por SQL em rotinas de verificacao de saldos pelo parceiro PHC*/
						or cly.inactivo<>clx.inactivo
						or cly.nocredit<>clx.nocredit
						or cly.tipo<>clx.tipo collate SQL_Latin1_General_CP1_CI_AI
						or cly.ncont<>clx.ncont collate SQL_Latin1_General_CP1_CI_AI)
						and cly.no =(case when '+convert(varchar,@clno)+' > 0 then '+convert(varchar,@clno)+' else cly.no end)
					
					IF @@ROWCOUNT <> @Counter 
						BEGIN
							select @errortext =  ''ERRO: Tabela de Clientes existentes n�o actualizada. Actualizados '' + CONVERT(VARCHAR,@@ROWCOUNT) + '' Clientes.''
							raiserror(@errortext,18,1)
						END
				END
			
			
			/* INSERT */
			SELECT 
				@counter=0,@Counter = COUNT(nome)
			FROM [SRVASSANDIM].assandim.dbo.cl clx WITH (NOLOCK) 
			where 
				clx.clivd=0 
				and clx.estab=0 
				and clx.[no]>200
				and clx.no =(case when '+convert(varchar,@clno)+' > 0 then '+convert(varchar,@clno)+' else clx.no end)
				and clx.no not in (			
					SELECT no FROM b_utentes WITH (NOLOCK)
				)
			
			IF @Counter > 0
				BEGIN
					INSERT INTO b_utentes (utstamp,nome,no,morada,local,codpost,conta,telefone,fax,email,tlmvl,ncont,zona,desconto,tipo,ccusto,bino,inactivo,obs,nocredit,nbenef,nome2,moeda,ousrhora,ousrdata,usrhora,usrdata,codigoP)
					SELECT 
						clstamp,nome,no,morada,local
						,codpost
						,conta  = (SELECT TEXTVALUE FROM B_PARAMETERS WHERE STAMP = ''ADM0000000242'')
						,left(telefone,13)
						,LEFT(fax,13)
						,email
						,LEFT(tlmvl,13),left(ncont,9),zona,case when tipo in (''Associado'',''Ass.Medic.'') then 5 else 0 end as desconto,tipo,ccusto,bino,inactivo,obs,nocredit,LEFT(u_utente,9),nome2,moeda,ousrhora,ousrdata,usrhora,usrdata
						,codigoP = ''PT''
					FROM [SRVASSANDIM].assandim.dbo.cl clx WITH (NOLOCK)
					where 
						clx.clivd=0 
						and clx.estab=0 
						and clx.[no]>200
						and clx.no =(case when '+convert(varchar,@clno)+' > 0 then '+convert(varchar,@clno)+' else clx.no end)
						and clx.no not in (			
							SELECT no FROM b_utentes WITH (NOLOCK)
						)
					
					IF @@ROWCOUNT <> @Counter 
						BEGIN
							select @errortext =  ''ERRO: Tabela de Clientes: ERRO. Migrados '' + CONVERT(VARCHAR,@@ROWCOUNT) + ''Clientes.''
							raiserror(@errortext,18,1)
						END
				END
		
		commit tran @tran
		
	end try
	
	begin catch
		
		rollback tran @tran
		
		select 
			@errortext = isnull(ERROR_PROCEDURE(),'''') + '': '' + ERROR_MESSAGE(),
			@error_severity = ERROR_SEVERITY(),
			@error_state = ERROR_state()

		insert into b_eLog (tipo,status,origem,mensagem) 
		select 
			tipo = ''E'',
			status = ''A'',
			origem = ''Sincroniza��o Clientes Associa��o FANSE'',
			mensagem = @errortext

		raiserror(@errortext,@error_severity,@error_state)
		
	end catch'
	
	execute (@sql+@sql2)
end

GO
Grant Execute on dbo.up_fanse_sync_cl_parametro to Public
grant control on dbo.up_fanse_sync_cl_parametro to Public
go