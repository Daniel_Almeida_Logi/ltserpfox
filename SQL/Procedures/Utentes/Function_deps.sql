
/****** Object:  UserDefinedFunction [dbo].[ListaEstab]    Script Date: 18/03/2020 14:54:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[ListaEstab]') IS NOT NULL
    DROP FUNCTION [dbo].[ListaEstab]
GO

-- =============================================
-- Author:		<JG>
-- Create date: <18-03-2020>
-- Description:	<calcular lista de dependentes (estab) para cada no da tabela b_utentes>
-- =============================================
CREATE FUNCTION [dbo].[ListaEstab] 
(	
	@no int
)
RETURNS varchar(30)
AS
BEGIN
	declare @aa varchar (30)
	set @aa = ''
	select @aa = 
		case when @aa = '' then cast(estab AS nvarchar)
			 else @aa + coalesce(',' + cast(estab AS nvarchar), '')
		end
	  from b_utentes where no=@no
	
	RETURN @aa
END
GO


