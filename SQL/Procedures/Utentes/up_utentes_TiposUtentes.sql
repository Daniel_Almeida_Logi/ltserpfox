

-- exec up_utentes_TiposUtentes
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_TiposUtentes]') IS NOT NULL
    drop procedure up_utentes_TiposUtentes
go

create PROCEDURE up_utentes_TiposUtentes


/* WITH ENCRYPTION */
AS

	select distinct tipo from B_utentes where tipo != '' order by tipo

	
GO
Grant Execute On up_utentes_TiposUtentes to Public
Grant Control On up_utentes_TiposUtentes to Public
go
