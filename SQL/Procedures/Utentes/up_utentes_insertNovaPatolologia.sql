/* 
	exec up_utentes_insertNovaPatolologia 'uhb', 'VT'
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_utentes_insertNovaPatolologia]') IS NOT NULL
	drop procedure dbo.up_utentes_insertNovaPatolologia
go

create procedure dbo.up_utentes_insertNovaPatolologia
	@descricao		VARCHAR(100),
	@iniciais		VARCHAR(10),
	@marcada		BIT  = 0

AS
BEGIN


	DECLARE @lastStamp			VARCHAR(25) = ''
	DECLARE @PermiteInsert		BIT = 1

	SELECT top 1
		@lastStamp = MAX(CONVERT(INT, STUFF(ctiefpstamp, 1, PATINDEX('%[0-9]%', ctiefpstamp)-1, '')))+1 
	FROM
		B_ctidef(NOLOCK)
	GROUP BY 
		ctiefpstamp
	ORDER BY 
		CONVERT(INT, STUFF(ctiefpstamp, 1, PATINDEX('%[0-9]%', ctiefpstamp)-1, '')) DESC

	IF NOT EXISTS (SELECT descricao FROM B_ctidef WHERE 
		UPPER(LTRIM(RTRIM(descricao))) = UPPER(LTRIM(RTRIM(@descricao))))
	BEGIN
		INSERT INTO B_ctidef
			(ctiefpstamp,descricao,ousrinis,ousrdata,ousrhora,usrinis,
				usrdata,usrhora,marcada)
		VALUES
			('ADM'+@lastStamp, @descricao, @iniciais, GETDATE(),CONVERT(VARCHAR(8),getdate(),108),@iniciais,
				GETDATE(),CONVERT(VARCHAR(8),getdate(),108),  @marcada)
	END	

	SELECT 
		CASE WHEN @@rowcount = 1 
					THEN CAST(1 AS BIT) 
					ELSE CAST(0 AS BIT) END AS correu
END

GO
Grant Execute on dbo.up_utentes_insertNovaPatolologia to Public
Grant control on dbo.up_utentes_insertNovaPatolologia to Public
Go 

