-- Daniel Almeida
-- Actualiza a ficha do utente para autorizar os email caso seja empresa.
-- O NIPC Número de Identificação de Pessoa Colectiva é o termo mais correcto para nos referirmos ao NIF de uma empresa. 
-- O primeiro dígito pode ser 5 (pessoa colectiva), 6 (pessoa colectiva pública), 8 (empresário em nome individual), 9 (pessoa colectiva irregular ou número provisório).
-- exec up_utentes_update_autoriza_email '527956486','1','0'
-- select ncont, autoriza_emails,autorizado,autoriza_sms, * from b_utentes where no = 1 and estab = 0

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_update_autoriza_email]') IS NOT NULL
    drop procedure up_utentes_update_autoriza_email 
go

create PROCEDURE up_utentes_update_autoriza_email 

	@ncont           as varchar(25)
	,@no			 numeric(12,0)
	,@estab			 numeric(12,0)


/* WITH ENCRYPTION */
AS
	
	if(LEFT(@ncont,1) in ('5','6','8','9'))
	BEGIN
		update b_utentes set autoriza_emails = 1, autorizado=1, autoriza_sms=1   where no = @no and estab = @estab and inactivo = 0
	END

	

	
GO
Grant Execute On up_utentes_update_autoriza_email  to Public
Grant Control On up_utentes_update_autoriza_email  to Public
go


