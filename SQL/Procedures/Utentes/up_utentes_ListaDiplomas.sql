
-- exec up_utentes_ListaDiplomas
-- exec up_utentes_DiplomaUtente ''
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_ListaDiplomas]') IS NOT NULL
    drop procedure up_utentes_ListaDiplomas
go

create PROCEDURE up_utentes_ListaDiplomas
/* WITH ENCRYPTION */
AS
--

	select 
		distinct 
		patologia = b_Patol.descr
		,despacho = isnull(dplms.diploma,'')
		,diploma = isnull(dplms.u_design,'')
	from 
		b_Patol
		left join b_patol_dip_lnk on b_Patol.id = b_patol_dip_lnk.patol_ID
		left join dplms on dplms.Diploma_ID = b_patol_dip_lnk.diploma_ID
	order by
		patologia


	--select 
	--	dplmsstamp
	--	,diploma 
	--	,descricao = u_design
	--From 
	--	dplms (nolock) 
	--where 
	--	u_design != ''
	--order by 
	--	u_design

	--if (select bool from B_Parameters where stamp = 'ADM0000000224') = 0
	--begin 
	--	select 
	--		dplmsstamp
	--		,diploma 
	--		,descricao = u_design
	--	From 
	--		dplms (nolock) 
	--	where 
	--		u_design != ''
	--	order by 
	--		u_design
	--end
	--else
	--begin
	--	select 
	--		dplmsstamp
	--		,diploma 
	--		,descricao = u_design
	--	From 
	--		dplms (nolock) 
	--	order by 
	--		diploma
	--end
		
	
GO
Grant Execute On up_utentes_ListaDiplomas to Public
Grant Control On up_utentes_ListaDiplomas to Public
go
