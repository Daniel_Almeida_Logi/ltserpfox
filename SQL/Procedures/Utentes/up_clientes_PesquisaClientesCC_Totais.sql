/*
PESQUISA CLIENTES CONTA-CORRENTE TOTAL
J.Gomes, 24-03-2020

Pesquisa a partir de 5 campos de pesquisa:
	no ,estab, dataini, datafim, vertodos, loja 

O resultado ser� sempre uma linha com d�bitos e cr�ditos e respectivos balan�os totais.

Output dos seguintes campos:
	TotalDebit,	TotalCredit, TotalBalance, PeriodDebit,	PeriodCredit, PeriodBalance, PreviousDebit
	, PreviousCredit, PreviousBalance



PROCEDURE
Criada procedure up_clientes_PesquisaClientesCC_Totais com par�metros 

Parametros: 
up_clientes_PesquisaClientesCC_Totais no, estab, 'dataini', 'datafim', vertodos, 'loja'

EXEC dbo.up_clientes_PesquisaClientesCC_Totais 215, 0, '2020-01-24', '2020-03-24', 0, 'Loja 1' 
*/



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_clientes_PesquisaClientesCC_Totais]') IS NOT NULL
    drop procedure up_clientes_PesquisaClientesCC_Totais
go

CREATE PROCEDURE up_clientes_PesquisaClientesCC_Totais
	@no			NUMERIC(10),
	@estab		NUMERIC(4),
	@dataIni	datetime,
	@dataFim	datetime,	
	@verTodos	bit,
	@loja		varchar(20)

AS
SET NOCOUNT ON

IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosCc'))
	DROP TABLE #dadosCc

select 
	datalc=''
	,ousrhora=''
	,dataven=''
	,serie=''
	,cmdesc=''
	,nrdoc=0
	,edeb=0
	,edebf=0
	,ecred=0
	,ecredf=0
	,obs=''
	,moeda=''
	,ultdoc=''
	,intid=0
	,cbbno=0
	,username = ''
	,ccstamp = ''
	,no = b_utentes.no
	,nome = b_utentes.nome
	,facstamp=''
	,verdoc=0
	,ousrinis=''
	--,saldoanterior = (select sum(a.edeb)- sum(a.ecred) from cc a (nolock) where b_utentes.no = a.no and a.estab = (case when @verTodos=1 then b_utentes.estab else @estab end) and a.datalc < @Dataini)
	--,edebanterior = (select sum(a.edeb) from cc a (nolock) where b_utentes.no = a.no and a.estab = (case when @verTodos=1 then b_utentes.estab else @estab end) and a.datalc < @Dataini)
	--,ecredanterior = (select sum(a.ecred) from cc a (nolock) where b_utentes.no = a.no and a.estab = (case when @verTodos=1 then b_utentes.estab else @estab end) and a.datalc < @Dataini)
	--,saldoperiodo =(select sum(a.edeb)- sum(a.ecred) from cc a (nolock) where b_utentes.no = a.no and a.estab = (case when @verTodos=1 then b_utentes.estab else @estab end) and a.datalc between @Dataini and @Datafim)
	--,site=''
--	,saldo_total = 0
	,edeb_anterior = case when cc.datalc < @Dataini then isnull(edeb,0) else 0 end
	,ecred_anterior = case when cc.datalc < @Dataini then isnull(ecred,0) else 0 end
	,edeb_total = isnull(edeb,0)
	,ecred_total = isnull(ecred,0)
--	,saldo_periodo = 0
	,site=''
	,dep=0
	,numero=0
into
	#dadosCc
From
	b_utentes (nolock)
	left join cc (nolock) on b_utentes.no = cc.no and b_utentes.estab = cc.estab
Where
	b_utentes.no = case when @no = 0 then b_utentes.no else @no end
	and b_utentes.estab = (case when @verTodos=1 then b_utentes.estab else @estab end)

union all

Select	
	datalc = convert(varchar,cc.datalc,102)
	,ousrhora = cc.ousrhora
	,dataven	= convert(varchar,cc.dataven,102)
	,serie = ISNULL(re.nmdoc,' ')
	,cc.cmdesc
	,cc.nrdoc
	,cc.edeb
	,cc.edebf
	,cc.ecred
	,cc.ecredf
	,cc.obs
	,cc.moeda
	,cc.ultdoc
	,cc.intid
	,cc.cbbno 
	,username = isnull(b_us.nome,'')
	,cc.ccstamp
	,cc.no
	,cc.nome
	,cc.faccstamp
	,verdoc = CONVERT(bit,0)
	,cc.ousrinis
	,edeb_anterior = 0
	,ecred_anterior = 0
	,edeb_total = 0
	,ecred_total = 0
	,site = isnull(ft.site,re.site)
	,dep = cc.estab
	,numero = ROW_NUMBER() over(order by cc.datalc,cc.ousrhora, cc.cm, cc.nrdoc asc)
From
	b_utentes (nolock)
	left join cc (nolock) on b_utentes.no = cc.no and b_utentes.estab = cc.estab
	left join RE (nolock) ON cc.restamp = re.restamp
	left join b_us on b_us.iniciais = cc.ousrinis
	left join ft (nolock) on cc.ftstamp = ft.ftstamp
WHERE
	cc.no=case when @no = 0 then cc.no else @no end
	And cc.estab=(case when @verTodos=1 then cc.estab else @estab end)
	and cc.datalc between @Dataini and @Datafim
	and (
		ft.site = case when @loja = '' then ft.site else @loja end
		or
		re.site = case when @loja = '' then re.site else @loja end
		or 
		(ft.site is null and re.site is null)
	)



Select 	  
	  TotalDebit = SUM(edeb_total)	
	, TotalCredit = SUM(ecred_total)
	, TotalBalance = (select sum((edeb-edebf)-(ecred-ecredf)) from cc where no=(case when @no = 0 then cc.no else @no end) and estab=(case when @verTodos=1 then cc.estab else @estab end))
	-- periodo	
	, PeriodDebit = SUM(edeb)
	, PeriodCredit = SUM(ecred)
	, PeriodBalance = SUM(edeb-edebf) - SUM(ecred-ecredf)
	--anterior	
	, PreviousDebit = SUM(edeb_anterior)
	, PreviousCredit = SUM(ecred_anterior)
	, PreviousBalance = SUM(edeb_anterior - ecred_anterior)
from 
	#dadosCc






GO
Grant Execute On up_clientes_PesquisaClientesCC_Totais to Public
Grant Control On up_clientes_PesquisaClientesCC_Totais to Public
GO


