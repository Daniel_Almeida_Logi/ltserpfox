


-- exec up_utentes_numero
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_numero]') IS NOT NULL
    drop procedure up_utentes_numero
go

create PROCEDURE up_utentes_numero
/* WITH ENCRYPTION */
AS

	select 
		no = ISNULL(MAX(no),0)+1 
	from 
		b_utentes (nolock)
		
	
GO
Grant Execute On up_utentes_numero to Public
Grant Control On up_utentes_numero to Public
go
