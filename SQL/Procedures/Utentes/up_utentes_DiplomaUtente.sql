
-- exec up_utentes_DiplomaUtente '' 
-- exec up_utentes_DiplomaUtente 'ADMBD8D2A75-B29F-4D17-86E'
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_DiplomaUtente]') IS NOT NULL
    drop procedure up_utentes_DiplomaUtente
go

create PROCEDURE up_utentes_DiplomaUtente
@utstamp as varchar(25)
/* WITH ENCRYPTION */
AS


	
	Select 
		* 
	from 
		B_cli_diplomasID
	where 
		idstamp = @utstamp
	order by 
		diploma
	
	
GO
Grant Execute On up_utentes_DiplomaUtente to Public
Grant Control On up_utentes_DiplomaUtente to Public
go

