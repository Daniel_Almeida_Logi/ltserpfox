USE [mecofarma]
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_utente_loja_dc_atendimento]    Script Date: 13/03/2023 10:28:03 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
--if OBJECT_ID('[dbo].[sp_insert_utente_loja_dc_atendimento]') IS NOT NULL
--	drop procedure dbo.sp_insert_utente_loja_dc_atendimento
--go
ALTER PROCEDURE [dbo].[sp_insert_utente_loja_dc_atendimento]
@stamp		varchar(50)
,@nome		varchar(100)
,@ncont		varchar(20) 
,@morada	varchar(150)
,@tlmvl		varchar(30)
,@nbenef	varchar(30)
,@email		varchar(30)
,@bino		varchar(30)
,@sexo		varchar(5) = ''
,@dtnasc	varchar(10) = '19000101'
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON
	BEGIN TRY  
		IF(1=1 )
		BEGIN
		  BEGIN TRAN
			INSERT INTO  dbo.b_utentes (utstamp, no, estab, nome ,ncont ,morada ,tlmvl ,nbenef ,email , bino, sexo, nascimento, moeda, codigop, descp, codigopnat, descpnat )
			SELECT @stamp
				, (select max(no)+1 from b_utentes (nolock) where no<1000000)
				,0
				,@nome
				,@ncont
				,@morada
				,@tlmvl
				,@nbenef
				,@email
				,@bino
				,@sexo
				,@dtnasc
				,(select textvalue from B_Parameters where stamp='ADM0000000260')
				,(select textvalue from B_Parameters where stamp='ADM0000000260')
				,(select top 1 textvalue from B_Parameters_site where stamp='ADM0000000050')
				,(select textvalue from B_Parameters where stamp='ADM0000000260')
				,(select top 1 textvalue from B_Parameters_site where stamp='ADM0000000050')
		  COMMIT TRAN
		END
	END TRY 
	BEGIN CATCH  
	   ROLLBACK TRAN
	END CATCH  
		SET XACT_ABORT OFF
select no from b_utentes where utstamp=@stamp
print 'cliente criados'
END
