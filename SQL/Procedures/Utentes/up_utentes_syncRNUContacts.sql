/*

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_utentes_syncRNUContacts]') IS NOT NULL
	drop procedure dbo.[up_utentes_syncRNUContacts]
go

create procedure dbo.up_utentes_syncRNUContacts
	@token AS varchar(100),
	@no as numeric(10,0),
	@estab as numeric(3,0),
	@sns as VARCHAR(50)

AS

	SELECT 
		*
	FROM
	(
		select 
			name,
			mobilePhone as contacto
		from 
			ext_RNU_RespHealthU_Contacts(nolock) 
		where 
			token = (SELECT TOP 1 listContactsToken FROM ext_RNU_RespHealthU(nolock) where token = @token and sns = @sns)
			and name <> ''
		UNION ALL
		select 
			name,
			phone as contacto
		from 
			ext_RNU_RespHealthU_Contacts(nolock) 
		where 
			token = (SELECT TOP 1 listContactsToken FROM ext_RNU_RespHealthU(nolock) where token = @token and sns = @sns)
			and name <> ''
		UNION ALL
		select 
			nome as name,
			telefone as contacto
		from 
			Telefones(nolock) 
		where 
			no = @no
			and estab = @estab
	) AS X
	WHERE 
		X.contacto <> ''
	ORDER BY
		Name ASC 

GO
Grant Execute On dbo.up_utentes_syncRNUContacts to Public
Grant Control On dbo.up_utentes_syncRNUContacts to Public
Go