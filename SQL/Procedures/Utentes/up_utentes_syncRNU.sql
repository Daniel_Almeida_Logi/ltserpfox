/*
	exec up_utentes_syncRNU 'ADMB490D299-5B45-4299-9FA', ''
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_utentes_syncRNU]') IS NOT NULL
	drop procedure dbo.up_utentes_syncRNU
go

create procedure dbo.up_utentes_syncRNU
	@token AS VARCHAR(100),
	@sns AS VARCHAR(50),
	@nif as varchar(50)

AS

	set @nif = case when @sns <> '0' Or @sns <>''
				then
					  '0'
				else
					@nif 
				end


	SELECT 
		CONVERT(varchar(50),ut.sns) as cl_nbenef,
		CONVERT(varchar(20), ut.nif) as cl_ncont,
		ut.fullName as cl_nome,
		ut.birthDate as cl_nascimento,
		ut.sex as cl_sexo,
		left(ISNULL((CASE
			WHEN ISNULL(ut_Add.road, '') <> ''
				THEN CONVERT(VARCHAR(254), LTRIM(RTRIM(ISNULL(ut_Add.roadTypeDesc, ''))) + ' ' + LTRIM(RTRIM(ISNULL(ut_Add.road, ''))) + ' ' + LTRIM(RTRIM(ISNULL(ut_Add.door, ''))) + ' ' + LTRIM(RTRIM(ISNULL(ut_Add.floor, '')))) 
			ELSE
				ISNULL(ut_add.foreignAddress, '')
		END), ''),55) as cl_morada,
		ut.countryNac as cl_codigoP,
		ut.countryNacDesc as cl_descP,
		(CASE WHEN ut.countryNat <> '' THEN ut.countryNat ELSE ut.countryNac END) as cl_codigoPNat,
		(CASE WHEN ut.countryNatDesc <> '' THEN ut.countryNatDesc ELSE ut.countryNacDesc END) as cl_descPNat,
		ISNULL((CASE 
					WHEN
					ISNULL(ut_Add.postalCode, '') <> ''
						THEN LTRIM(RTRIM(ISNULL(ut_Add.postalCode, ''))) + '-' + LTRIM(RTRIM(ISNULL(ut_Add.sequencePostcard, ''))) 
					ELSE
						ISNULL(ut_Add.foreignPostalCode, '')
				END), '') as cl_codPost,
		ISNULL((CASE WHEN ISNULL(ut_Add.postalLocation, '') <> '' THEN ISNULL(ut_Add.postalLocation, '') WHEN ISNULL(ut_Add.location, '') <> '' THEN ISNULL(ut_Add.location, '') ELSE ISNULL(ut_Add.foreignLocation, '')  END), '') as cl_local,
		ISNULL((CASE WHEN ISNULL(ut_contact.phone, '') <> '' THEN ut_contact.phone ELSE (SELECT TOP 1 phone FROM ext_RNU_RespHealthU_Contacts(NOLOCK) WHERE token = ut.listContactsToken ORDER BY changeDate desc) END), '') as cl_telefone,
		ISNULL((CASE WHEN ISNULL(ut_contact.mobilePhone, '') <> '' THEN ut_contact.mobilePhone ELSE (SELECT TOP 1 mobilePhone FROM ext_RNU_RespHealthU_Contacts(NOLOCK) WHERE token = ut.listContactsToken ORDER BY changeDate desc) END), '') as cl_tlmvl,
		ISNULL((CASE WHEN ISNULL(ut_contact.email, '') <> '' THEN ut_contact.email ELSE (SELECT TOP 1 email FROM ext_RNU_RespHealthU_Contacts(NOLOCK) WHERE token = ut.listContactsToken ORDER BY changeDate desc) END), '') as cl_email,
		LTRIM(RTRIM(ut.ownNamesFather)) + ' ' + LTRIM(RTRIM(ut.surnameFather)) as cl_Pai,
		LTRIM(RTRIM(ut.ownNamesMother)) + ' ' + LTRIM(RTRIM(ut.surnameMother)) as cl_Mae,
		LEFT(LTRIM(RTRIM(ut.nDocIdent)),20) as cl_bino,
		(CASE 
			WHEN ut.typeDocIdent IN ('', 'N') THEN ''
			WHEN ut.typeDocIdent = 'P' THEN 'PASSP'
			ELSE ''
		END) as cl_codigoDocSPMS,
		(CASE WHEN ut.death = 'S' THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT)  END) as cl_obitoRNU
	FROM
		ext_RNU_RespHealthU(nolock) as ut
		LEFT join ext_RNU_RespHealthU_Address(nolock) as ut_Add on ut.addressStamp = ut_Add.stamp
		LEFT join ext_RNU_RespHealthU_Contacts(nolock) as ut_contact ON ut_contact.token = ut.listContactsToken and ut_contact.name = ''
	
	WHERE
		ut.token = @token
		AND (
        (CASE 
            WHEN @nif = 0 THEN ut.sns 
            WHEN @sns = 0 THEN ut.nif 
         END) = CASE 
                    WHEN (SELECT COUNT(*) FROM ext_RNU_RespHealthU(NOLOCK) WHERE token = @token) > 1
                        THEN CASE 
                                WHEN @nif = 0  THEN @sns 
                                WHEN @SNS = 0  THEN @nif 
                             END
                    ELSE 
                        (CASE 
                            WHEN @nif = 0  THEN ut.sns 
                            WHEN @sns = 0  THEN ut.nif 
                         END)
                 END
    );

GO
Grant Execute On dbo.up_utentes_syncRNU to Public
Grant Control On dbo.up_utentes_syncRNU to Public
Go