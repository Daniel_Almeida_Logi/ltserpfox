/*
	Retorna dados do Cart�o de Cliente


	exec up_utentes_getCartaoCLI 234, 0 ,'CL2021000130'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_utentes_getCartaoCLI]') IS NOT NULL
	DROP procedure dbo.up_utentes_getCartaoCLI
GO

CREATE procedure dbo.up_utentes_getCartaoCLI	
	@no NUMERIC(9),
	@estab NUMERIC(9),
	@nrCartao VARCHAR(50)

AS
	SELECT 
		*
	FROM
		b_fidel(nolock)
	WHERE
		clno = @no
		AND clestab = @estab
		AND nrcartao = @nrCartao
		AND inactivo = 0

GO
GRANT EXECUTE on dbo.up_utentes_getCartaoCLI TO PUBLIC
GRANT CONTROL on dbo.up_utentes_getCartaoCLI TO PUBLIC
GO