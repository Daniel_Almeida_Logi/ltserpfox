/*
sp para insert dos dados da tabela Telefones_Utente_Fornecedor,
para utentes e fornecedores
a colocar num form no logitools

20200928, JG, v1

input: no, estab, tipo, nome, telefone


exec up_Telefones_Utente_Fornecedor_Insert 10783196,0,'U','tttt','963555533',null,null
exec up_Telefones_Utente_Fornecedor_Insert 10783196,0,'U','jjjjjj','963555533','',''

exec up_Telefones_Utente_Fornecedor_Insert 3037,0,'F','sofia gomes','9122266652'

exec up_Telefones_Utente_Fornecedor_Insert 10783196, 0,'U','gggdfg vfdf','23131','/  /     :  :   AM','' 

exec up_Telefones_Utente_Fornecedor_Insert 10783196, 0,'U','','','/  /     :  :  ','' 

*/


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_Telefones_Utente_Fornecedor_Insert]') IS NOT NULL
    drop procedure up_Telefones_Utente_Fornecedor_Insert
go

CREATE PROCEDURE up_Telefones_Utente_Fornecedor_Insert		
	  @no					INT
	, @estab				INT	
	, @tipo					VARCHAR(1)
	, @nome					VARCHAR(80)
	, @telefone				VARCHAR(20)
	, @ousrdata				datetime
	, @ousrhora				varchar(8)	

AS

SET @no			=   rtrim(ltrim(isnull(@no,0)))
SET @estab		=   rtrim(ltrim(isnull(@estab,0)))	
SET @tipo		=   rtrim(ltrim(isnull(@tipo,'')))
SET @nome		=   rtrim(ltrim(isnull(@nome,'')))
SET @telefone	=   rtrim(ltrim(isnull(@telefone,'')))
--SET @ousrdata	=   rtrim(ltrim(isnull(@ousrdata,'')))
SET @ousrhora	=   rtrim(ltrim(isnull(@ousrhora,'')))


declare @tipodesc varchar(10)
set @tipodesc = 
				case 
					when @tipo='U' then 'utente'
					when @tipo='F' then 'fornecedor'
					else 'outro'
				end
	


if @ousrhora = '' or len(@ousrhora) < 8 
begin
	SET @ousrdata	=   ''
	SET @ousrhora	=   ''
end
	
IF @nome <> '' and len(@nome) > 0
BEGIN
	BEGIN TRANSACTION;	
		INSERT INTO [Telefones_Utente_Fornecedor]
			([stamp]
			,[no]
			,[estab]
			,[tipo]
			,[tipodesc]
			,nome
			,telefone
			,[ousrinis]
			,[ousrdata] --data gravacao
			,[ousrhora] 
			,[usrinis]
			,[usrdata]	--data alteracao
			,[usrhora])			
			select left(newid(),25)
			, @no
			, @estab
			, @tipo
			, @tipodesc
			, @nome
			, @telefone
			,'ADM'
			, COALESCE(NULLIF(@ousrdata,''), convert(varchar,getdate(),23))
			, COALESCE(NULLIF(@ousrhora,''), convert(varchar(8),getdate(),24))
			,'ADM'			
			, convert(varchar,getdate(),23)
			, convert(varchar(8),getdate(),24)
	COMMIT TRANSACTION;		
END

GO
GRANT EXECUTE on dbo.up_Telefones_Utente_Fornecedor_Insert TO PUBLIC
GRANT Control on dbo.up_Telefones_Utente_Fornecedor_Insert TO PUBLIC
GO