-- exec up_utentes_cartaoH 25
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_cartaoH]') IS NOT NULL
    drop procedure up_utentes_cartaoH
go

create PROCEDURE up_utentes_cartaoH
@no numeric(9,0)

/* WITH ENCRYPTION */
AS

	select 
		nome
		,no
		,validade = DATEADD(YY,-1,validade)
		,tipo = ISNULL(
			Case 
				when B_entidadesUtentes.enome = 'CART�O H SA�DE BRONZE' then 'BRONZE' 
				when B_entidadesUtentes.enome = 'CART�O H SA�DE PRATA' then 'PRATA' 
				when B_entidadesUtentes.enome = 'CART�O H SA�DE OURO' then 'OURO' 
				when B_entidadesUtentes.enome = 'CART�O H SA�DE BRONZE (PATAIAS)' then 'BRONZE' 
				when B_entidadesUtentes.enome = 'CART�O H SA�DE PRATA PATAIAS)' then 'PRATA' 
				when B_entidadesUtentes.enome = 'CART�O H SA�DE OURO (PATAIAS)' then 'OURO' 
				when B_entidadesUtentes.enome = 'CARTAO H SAUDE PRATA - CO' then 'PRATA' 
				when B_entidadesUtentes.enome = 'CARTAO H SAUDE BRONZE - CO' then 'BRONZE' 
				when B_entidadesUtentes.enome = 'CARTAO H SAUDE OURO - CO' then 'OURO' 
			end,'')
	from 
		B_entidadesUtentes 
	where 
		B_entidadesUtentes.enome in (
			'CART�O H SA�DE BRONZE'
			,'CART�O H SA�DE PRATA'
			,'CART�O H SA�DE OURO'
			,'CART�O H SA�DE BRONZE (PATAIAS)'
			,'CART�O H SA�DE PRATA PATAIAS)'
			,'CART�O H SA�DE OURO (PATAIAS)'
			,'CARTAO H SAUDE PRATA - CO'
			,'CARTAO H SAUDE BRONZE - CO'
			,'CARTAO H SAUDE OURO - CO'
		)
		and B_entidadesUtentes.no = @no
		
	
	

	
GO
Grant Execute On up_utentes_cartaoH to Public
Grant Control On up_utentes_cartaoH to Public
go
