/* SP que retorna Sit Clinicas Ligeiras

	exec up_utentes_SitClinicas '198288042'

*/
if OBJECT_ID('[dbo].[up_utentes_SitClinicas ]') IS NOT NULL
	drop procedure dbo.up_utentes_SitClinicas 
go

create procedure dbo.up_utentes_SitClinicas 
	@ClNo varchar(36)

/* WITH ENCRYPTION */
AS
	Select
			CONVERT(VARCHAR(10), ousrdata, 103) AS Data
			,LEFT(CONVERT(VARCHAR(8), ousrdata, 108), 5) AS Hora
			,quest_respostas_grpStamp
			,rtrim(ltrim((select qr.resposta from quest_respostas qr where qr.quest_perguntaStamp = 'C25686DF-22A6-414D-88CA-8' and qr.quest_respostas_grpStamp = quest_respostas.quest_respostas_grpStamp)))
				+
					'. '
				+
				rtrim(ltrim(isnull((select qr.resposta from quest_respostas qr where qr.quest_perguntaStamp = '5739A9A9-6E69-4193-857C-A' and qr.quest_respostas_grpStamp = quest_respostas.quest_respostas_grpStamp),'')))
					as Obs
			, (select  iniciais from b_us where userno = quest_respostas.operador) as ut
	from quest_respostas 
	where quest_respostas.QuestStamp = '230AE1CA-D832-49E4-8D73-E '
		and quest_respostas.no = @ClNo
	group by quest_respostas_grpStamp, ousrdata, quest_respostas.operador
	--exec up_utentes_SitClinicas '198288042'


GO
Grant Execute On dbo.up_utentes_SitClinicas  to Public
Grant Control On dbo.up_utentes_SitClinicas  to Public
Go 