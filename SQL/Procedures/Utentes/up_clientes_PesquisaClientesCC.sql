/*
PESQUISA CLIENTES CONTA CORRENTE
J.Gomes, 24-03-2020

Pesquisa a partir de 6 campos de pesquisa:
	no ,estab, dataini, datafim, vertodos, loja 

O resultado ser� sempre um conjunto de linhas de documentos com d�bitos e cr�ditos e respectivos balan�os.
A primeira linha cont�m o resultado total anterior at� � Dataini.


Output dos seguintes campos:
	 datalc AS CreationDate, ousrhora AS CreationTime, no AS Number, dep AS Dep, nrdoc AS DocNumber
	, serie As SerieDesc, cmdesc AS DocDesc
	, COALESCE(NULLIF(edeb,0), edebanterior) As Debit, COALESCE(NULLIF(ecred,0), ecredanterior)  AS Credit
	, (select (SUM(edebanterior) + SUM(edeb)) - (SUM(ecredanterior) + SUM(case when ecred<0 then ecred*(-1) else ecred end)) from #dadosCc a where a.numero <= #dadosCc.numero) AS Balance
	, moeda AS Currency, obs, site


PROCEDURE
Criada procedure up_clientes_PesquisaClientesCC com par�metros 

Parametros: 
up_clientes_PesquisaClientesCC no, estab, 'dataini', 'datafim', vertodos, 'loja'

EXEC dbo.up_clientes_PesquisaClientesCC 215, 0, '2020-01-24', '2020-03-24', 0, 'Loja 1'  

*/



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_clientes_PesquisaClientesCC]') IS NOT NULL
    drop procedure up_clientes_PesquisaClientesCC
go

CREATE PROCEDURE up_clientes_PesquisaClientesCC
	@no			NUMERIC(10),
	@estab		NUMERIC(4),
	@dataIni	datetime,
	@dataFim	datetime,	
	@verTodos	bit,
	@loja		varchar(20)

AS
SET NOCOUNT ON


	
IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosCc'))
	DROP TABLE #dadosCc

	select 
		datalc=''
		,ousrhora=''
		,dataven=''
		,serie=''
		,cmdesc='SALDO ANTERIOR'
		,nrdoc=0
		,edeb=0
		,ecred=0
		,obs=''
		,moeda=''
		,ultdoc=''
		,intid=0
		,cbbno=0
		,username = ''
		,ccstamp = ''
		,no = b_utentes.no
		,nome = b_utentes.nome
		,facstamp=''
		,verdoc=0
		,ousrinis=''
		,saldoanterior = (select sum(a.edeb)- sum(a.ecred) from cc a (nolock) where b_utentes.no = a.no and a.estab = (case when @verTodos=1 then b_utentes.estab else @estab end) and a.datalc < @Dataini)
		,edebanterior = (select sum(a.edeb) from cc a (nolock) where b_utentes.no = a.no and a.estab = (case when @verTodos=1 then b_utentes.estab else @estab end) and a.datalc < @Dataini)
		,ecredanterior = (select sum(a.ecred) from cc a (nolock) where b_utentes.no = a.no and a.estab = (case when @verTodos=1 then b_utentes.estab else @estab end) and a.datalc < @Dataini)
		,saldoperiodo =(select sum(a.edeb)- sum(a.ecred) from cc a (nolock) where b_utentes.no = a.no and a.estab = (case when @verTodos=1 then b_utentes.estab else @estab end) and a.datalc between @Dataini and @Datafim)
		,site=''
		,dep=0
		,numero=0
		,suspensa=''
	into
		#dadosCc
	From
		b_utentes (nolock)
	Where
		b_utentes.no  = @no
		and b_utentes.estab = 0

	union all

	Select	
		datalc = convert(varchar,cc.datalc,102)
		,ousrhora = cc.ousrhora
		,dataven	= convert(varchar,cc.dataven,102)
		,serie = ISNULL(re.nmdoc,' ')
		,cc.cmdesc
		,cc.nrdoc
		,cc.edeb
		,cc.ecred
		,cc.obs
		,cc.moeda
		,cc.ultdoc
		,cc.intid
		,cc.cbbno 
		,username = isnull(b_us.nome,'')
		,cc.ccstamp
		,cc.no
		,cc.nome
		,cc.faccstamp
		,verdoc = CONVERT(bit,0)
		,cc.ousrinis
		,saldoanterior = 0
		,edebanterior = 0
		,ecredanterior = 0
		,saldoperiodo = 0
		,site = isnull(ft.site,re.site)
		,dep = cc.estab
		,numero = ROW_NUMBER() over(order by cc.datalc,cc.ousrhora, cc.cm, cc.nrdoc asc)
		,suspensa= (case when ft.cobrado=1 then 'Vd. Susp.)' else ft.nmdoc end)
	From
		b_utentes (nolock)
		left join cc (nolock) on b_utentes.no = cc.no and b_utentes.estab = cc.estab
		left join RE (nolock) ON cc.restamp = re.restamp
		left join b_us on b_us.iniciais = cc.ousrinis
		left join ft (nolock) on cc.ftstamp = ft.ftstamp
	WHERE
		cc.no = @no
		And cc.estab=(case when @verTodos=1 then cc.estab else @estab end)
		and cc.datalc between @Dataini and @Datafim
		and (
			ft.site = case when @loja = '' then ft.site else @loja end
			or
			re.site = case when @loja = '' then re.site else @loja end
			or 
			(ft.site is null and re.site is null)
		)
			   
	Select 
		datalc AS CreationDate, ousrhora AS CreationTime, no AS Number, dep AS Dep, nrdoc AS DocNumber, serie As SerieDesc, cmdesc AS DocDesc
		, COALESCE(NULLIF(edeb,0), edebanterior) As Debit, COALESCE(NULLIF(ecred,0), ecredanterior)  AS Credit
		, (select (SUM(edebanterior) + SUM(edeb)) - (SUM(ecredanterior) + SUM(case when ecred<0 then ecred*(-1) else ecred end)) from #dadosCc a where a.numero <= #dadosCc.numero) AS Balance
		--, edebanterior, ecredanterior, saldoanterior
		, moeda AS Currency, obs, site
		/*
		, saldo = (select (SUM(edebanterior) + SUM(edeb)) - (SUM(ecredanterior) + SUM(case when ecred<0 then ecred*(-1) else ecred end)) from #dadosCc a where a.numero <= #dadosCc.numero)
		,*
		*/
	from 
		#dadosCc
	order by
		numero
	


GO
Grant Execute On up_clientes_PesquisaClientesCC to Public
Grant Control On up_clientes_PesquisaClientesCC to Public
GO


