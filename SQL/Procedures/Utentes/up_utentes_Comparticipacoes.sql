

-- exec up_utentes_Comparticipacoes  '',1
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_Comparticipacoes]') IS NOT NULL
    drop procedure up_utentes_Comparticipacoes
go

create PROCEDURE up_utentes_Comparticipacoes
@id varchar(10),
@site_nr tinyint


/* WITH ENCRYPTION */
AS
		
	select 
		cpt_val_cli.*
		,st.design
		,especialidade = isnull(b_cli_stRecursos.nome,'')
	from 
		cpt_val_cli
		inner join cpt_conv on cpt_val_cli.id_cpt_conv = cpt_conv.id
		inner join B_utentes on B_utentes.no = cpt_conv.id_entidade and B_utentes.entCompart = 1
		inner join st on cpt_val_cli.ref = st.ref and st.site_nr = @site_nr
		left join b_cli_stRecursos on b_cli_stRecursos.ref = cpt_val_cli.ref and b_cli_stRecursos.tipo = 'Especialidade'
	Where
		cpt_conv.id = @id
		
	
GO
Grant Execute On up_utentes_Comparticipacoes to Public
Grant Control On up_utentes_Comparticipacoes to Public
go



