



--	
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_BioRegistos]') IS NOT NULL
    drop procedure up_utentes_BioRegistos
go

create PROCEDURE up_utentes_BioRegistos
@utstamp varchar(25)

/* WITH ENCRYPTION */
AS

	select 
		BioRegistos.id, 
		utstamp, 
		id_bioparametros, 
		CONVERT(varchar, data, 102) as [data], 
		left(hora,5) as hora, 
		--cast(valor as numeric(10,0)) as valor, 
		--cast(valor_min as numeric(10,0)) as valor_min, 
		valor,
		valor_min,
		BioRegistos.obs, 
		vref 
	from 
		BioRegistos (nolock)
		inner join bioparametros (nolock) on bioparametros.id=bioregistos.id_bioparametros 
	where 
		utstamp = @utstamp 
	order by 
		DATA desc, 
		Hora desc

	
GO
Grant Execute On up_utentes_BioRegistos to Public
Grant Control On up_utentes_BioRegistos to Public
go



