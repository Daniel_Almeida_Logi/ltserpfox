
/*

select no_ext,* from b_utentes(nolock) where no='1078310' 
select no_ext,* from b_utentes(nolock) where no='1079035' 



exec up_insere_update_utente
'1079035','', 'Esse Esse',  '3333', 0,  '234098700', '9521511',
'R.'' OURO, 321', 'Agueda', '4505-102', 'email@email.pt',
'123456','109000000','1',
'19990113','M', 'OBS OBS''', 0, 'Utente', '0', 0.0; 



*/




SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_insere_update_utente]') IS NOT NULL
	drop procedure dbo.up_insere_update_utente
go



create procedure dbo.up_insere_update_utente

  @no_ext as varchar(10)
, @code_ext as varchar(20)
, @nome as varchar(150)
, @ncont as varchar(10)
, @estab as numeric(10,0)
, @telef as varchar(25)
, @tlmvl as varchar(25)
, @morada as varchar(150) 
, @localidade as varchar(150)
, @codpost as varchar(150) 
, @email as varchar(100) 
, @nrut as varchar(50)
, @niss as varchar(20)
, @nrcc as varchar(15)
, @dtnasc as date
, @sexo as varchar(1)
, @obs as varchar(MAX)
, @forceact as bit = 0
, @tipoC as varchar(255) = ""
, @inactivo as varchar(1) = ""
, @desconto as numeric(16,4)
, @rgpdToken as numeric(16,0)
, @rgpEmail  as varchar(1)
, @rgpSms as varchar(1)
, @ousrinis as varchar(255) = "IMP" 
, @descp as varchar(255) = "PORTUGAL"
, @abrevp as varchar(255) = "PT"
, @pais as numeric(5,0) = 1
, @notif as numeric(5,0) = 1





/* WITH ENCRYPTION */
AS






Declare @tipo as numeric(10,0)



/* valida numero externo
   senão existir, valida pelo nif (campo obrigatório)
*/

if(@no_ext) = ''
begin 
set @tipo  = (select 
				top 1 count(no) as no  
			  from 
				b_utentes(nolock) 
			  where 			
				b_utentes.ncont = ltrim(rtrim(@ncont))
			  order by 
				no
			  asc			
			)
end

/* 
   se existir, valida pelo numero externo
*/

else begin
	set @tipo  = (select 
				top 1 count(no) as no  
			  from 
				b_utentes(nolock) 
			  where 			
				ltrim(rtrim(@no_ext)) = [no] 
			  order by 
				no			 	
			  asc			
			)
end


/* Proteções para o funcionamento o do Logitools*/

if(len(@nrcc)>8)
begin 
	set @nrcc = SUBSTRING(@nrcc,0,8)
end


if(@nrcc!='')
begin
	set @nrcc = ISNULL(REPLICATE('0',8-LEN(@nrcc)),'') + @nrcc
end


set @morada = REPLACE(@morada,'''',' ')
set @localidade = REPLACE(@localidade,'''',' ')
set @obs = REPLACE(@obs,'''',' ')
set @email = left(rtrim(ltrim(isnull(@email,''))),45)



if @tipo=0
begin
	insert into b_utentes (
		utstamp
		, no
		, no_ext
		, nome
		, ncont
		, telefone
		, tlmvl
		, morada
		, [local] 
		, codpost
		, email
		, nbenef
		, bino
		, nascimento
		, ousrdata
		, sexo
		, tipo
		, ousrinis
		, descp
		, codigop
		, notif
		, descpNat
		, codigopNat
		, pais
		, estab
		, obs
		, nrss
		, inactivo
		, desconto
		, autoriza_emails
		, autoriza_sms

	)
	select 
		left(newid(), 21) as utstamp
		, case 
			when  @no_ext = '' then  (select max(no)+1 from b_utentes(nolock)) else  @no_ext end
		, @code_ext
		, @nome
		, @ncont
		, @telef
		, @tlmvl
		, @morada
		, @localidade
		, @codpost
		, @email
		, @nrut
		, @nrcc
		, @dtnasc
		, getdate()
		, @sexo
		, @tipoC
		, @ousrinis
		, @descp
		, @abrevp
		, @notif
		, @descp
		, @abrevp
		, @pais
		, @estab
		, @obs
		, @niss
		, isnull(@inactivo,0)
		, case 
			when  @desconto = -1 then 0 else  @desconto end
		, @rgpEmail
		, @rgpSms 

end
else
begin

	declare @utno numeric(10,0)
	declare @utestab numeric(10,0) = 0
	declare @temDocFact numeric(10,0) = 0
	declare @nif varchar(20) = ''

	if(@no_ext) = ''
	begin

		select 
			top 1 
				@utno     = no,
				@utestab  = estab,
				@nif      = isnull(ncont,'')
			from 
				b_utentes(nolock) 
			where
				b_utentes.ncont = ltrim(rtrim(@ncont))					
			order by
				no
			desc

					 
	end
	else begin
		
			select
					top 1 
						@utno     = no,
						@utestab  = estab,
						@nif      = isnull(ncont,'')
					from 
						b_utentes(nolock) 
					where
						ltrim(rtrim(@no_ext)) = [no] 				
					order by
						no
					desc

					 

	end

	 select @temDocFact = count(*) from ft(nolock) inner join td(nolock) on ft.ndoc=td.ndoc  where td.tiposaft!=''  and no=@utno and estab = @utestab

	update
		b_utentes
	set
		nome = case when 
					@temDocFact > 0 and @ncont=''
				then
					nome
				else
					case when @forceact=1 then
						@nome
					else
						case when @nome='' then nome else @nome end
					end 
				end 

		,ncont = case when 
					@temDocFact > 0 and @ncont!=''
				then
					ncont
				else
					case when @forceact=1 then
						@ncont
					else
						case when @ncont='' then ncont else @ncont end
					end 
				end 
		 
		, telefone = case when @forceact=1 then
						@telef
					else
						 case when @telef='' then telefone else @telef end
					end
		, tlmvl =	case when @forceact=1 then
						 @tlmvl
					else
						case when @tlmvl='' then tlmvl else @tlmvl end
					end 
		, morada =	case when @forceact=1 then
						@morada
					else
						case when @morada='' then morada else @morada end
					end
		, [local] =	case when @forceact=1 then
						@localidade
					else
						case when @localidade='' then [local] else @localidade end
					end
		, codpost = case when @forceact=1 then
						@codpost
					else
						case when @codpost='' then codpost else @codpost end
					end
		, email = case when @forceact=1 then
						@email
					else
						case when @email='' then email else @email end
					end
		, obs = case when @forceact=1 then
						@obs
					else
						case when @obs='' then obs else @obs end
					end

		, nbenef = case when @forceact=1 then
						@nrut
					else
						case when @nrut='' then nbenef else @nrut end
					end
		, bino = case when @forceact=1 then
						@nrcc
					else
						case when @nrcc='' then bino else @nrcc end
					end
		, nrss = case when @forceact=1 then
						@niss
					else
						case when @niss='' then nrss else @niss end
					end
		, nascimento = case when @forceact=1 then
						@dtnasc
					else
						case when year(@dtnasc)=1900 then nascimento else @dtnasc end
					end
		, usrdata = getdate()
		, sexo = case when @forceact=1 then
						@sexo
					else
						case when @sexo='' then sexo else @sexo end
					end


		, no_ext = case when @forceact=1 then
						@code_ext
					else
						case when @code_ext='' then no_ext else @code_ext end
					end	

		, tipo = case when @forceact=1 then
						@tipoC
					else
						case when @tipoC='' then tipo else @tipoC end
					end

		, inactivo = case when @forceact=1 then
						@inactivo
					else
						case when @inactivo='' then inactivo else @inactivo end
					end

		, desconto = case when @forceact=1 then
						@desconto
					else
						case when @desconto=-1.0 then desconto else @desconto end
					end	

		, autoriza_emails = case when @forceact=1 then
			@rgpEmail
		else
			case when @rgpEmail='' then autoriza_emails else @rgpEmail end
		end

		, autoriza_sms = case when @forceact=1 then
			@rgpSms
		else
			case when @rgpSms='' then autoriza_sms else @rgpSms end
		end

	where
			b_utentes.no = @utno
			and b_utentes.estab  = @utestab
								
end 
GO
Grant Execute On dbo.up_insere_update_utente to Public
Grant Control On dbo.up_insere_update_utente to Public
GO
