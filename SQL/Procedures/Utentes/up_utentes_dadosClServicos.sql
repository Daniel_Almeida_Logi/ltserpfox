/*

 exec up_utentes_dadosClServicos 1,0,1
 
*/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_dadosClServicos]') IS NOT NULL
    drop procedure up_utentes_dadosClServicos
go

create PROCEDURE up_utentes_dadosClServicos

@no as  numeric(10),
@estab numeric(3),
@site_nr tinyint

/* WITH ENCRYPTION */
AS

		
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosClServicos'))
		DROP TABLE #dadosClServicos
			
	Select 
		st.ref
		,st.design
		,st.ivaincl
		,taxasiva.taxa
		,st.tabiva
		,qtt
		,desconto
		,preco
		,pvp	   = case when ivaincl = 1 then ROUND(preco,2) else ROUND(preco * (taxa/100+1),2) END
		,pvp_sIva  = case when ivaincl = 1 then ROUND(preco / (taxa/100+1),2) ELSE ROUND(preco,2) END
		,iva	   = case when ivaincl = 1 then ROUND(preco - (preco / (taxa/100+1)),2) ELSE ROUND(preco * (taxa/100+1) - preco,2) END
		,totalBase = case when ivaincl = 1 then ROUND(((preco / (taxa/100+1)) * ((100-desconto)/100)) * qtt ,2) ELSE ROUND((preco * ((100-desconto)/100)) * qtt ,2) END
		,totalIva  = case when ivaincl = 1 then ROUND((preco - (preco / (taxa/100+1))) * ((100-desconto)/100) * QTT ,2) ELSE ROUND((preco * (taxa/100+1) - preco) * ((100-desconto)/100) * qtt,2) END
		--,total	   = case when ivaincl = 1 then ROUND(preco * qtt - (preco * qtt * (desconto / 100)),2) else ROUND((preco + (preco * (taxa/100))) * qtt - ((preco + (preco * (taxa/100))) * qtt * (desconto / 100)),2) END
		,total     = ROUND((preco - (preco * (desconto/100))) * qtt ,2) 
		,id		   = ROW_NUMBER() OVER(Partition by st.Ref ORDER BY data DESC)
	into
		#dadosClServicos
	from 
		cl_servicos  (nolock)
		inner join st (nolock) on cl_servicos.ref = st.ref
		left join taxasiva on st.tabiva = taxasiva.codigo
	Where
		nr_cl = @no
		and dep_cl = @estab	
		and st.site_nr = @site_nr
		and fatura = 1
		
	Select
		*
	From	
		#dadosClServicos
	Where
		id = 1


	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosClServicos'))
		DROP TABLE #dadosClServicos
			
GO
Grant Execute On up_utentes_dadosClServicos to Public
Grant Control On up_utentes_dadosClServicos to Public
go
