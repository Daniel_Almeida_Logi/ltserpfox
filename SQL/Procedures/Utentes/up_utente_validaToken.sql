
/*

valida se token existe


exec up_utente_validaToken 86350


*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_utente_validaToken]') IS NOT NULL
	drop procedure dbo.up_utente_validaToken
go



create procedure dbo.up_utente_validaToken

 @rgpdToken as numeric(16,0)


/* WITH ENCRYPTION */
AS

	Select top 1 count(*) as no
	from b_utentes(nolock)
	where tokenaprovacao=@rgpdToken 
								 
GO
Grant Execute On dbo.up_utente_validaToken to Public
Grant Control On dbo.up_utente_validaToken to Public
GO
