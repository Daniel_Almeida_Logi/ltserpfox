

/*  Inserir chamada 3cx 
exec usp_get_ChamadasTelefonicas '',null,null,'',-1,-1,''
			exec usp_get_ChamadasTelefonicas '','2020-03-10', '2020-03-10', '', -1, -1,''
			exec usp_get_ChamadasTelefonicas '','2020-03-10','2020-03-10','','','',''
			exec usp_get_ChamadasTelefonicas '','20200606','20200706','',-1,-1,''
			exec usp_get_ChamadasTelefonicas '0033616873975','19000101','20200706','operadores',2101,0,''
			exec usp_get_ChamadasTelefonicas '0033616873975','19000101','20200706','operadores',-1,-1,''
			exec usp_get_ChamadasTelefonicas '','19000101','20200706','fornecedores',-1,-1,''
			exec usp_get_ChamadasTelefonicas '0033616873975','19000101','20200706','',-1,0,''
			exec usp_get_ChamadasTelefonicas '','19000101','20200706','utentes',-1,-1,''
			select * from logsPhoneCalls where fromNumber like '%1504%' or toNumber like '%1504%' or toDn like '%1504%'
						exec usp_get_ChamadasTelefonicas '','20200607','20200707','operadores',1504,0,''
									exec usp_get_ChamadasTelefonicas '','20200607','20200707','fornecedores',5,0,''
			exec usp_get_ChamadasTelefonicas '','20200607','20200707','',-1,-1,''
						exec usp_get_ChamadasTelefonicas '','20200607','20200707','utentes',-1,-1,''
									exec usp_get_ChamadasTelefonicas '','20200607','20200707','utentes',-1,-1,''



			exec usp_get_ChamadasTelefonicas '','20200920','20201020','',-1,-1,''
			exec usp_get_ChamadasTelefonicas '','20200920','20201020','utentes',-1,-1,''
			exec usp_get_ChamadasTelefonicas '','20200920','20201020','fornecedores',-1,-1,''
			exec usp_get_ChamadasTelefonicas '','20200920','20201020','operadores',-1,-1,''
			
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[usp_get_ChamadasTelefonicas]') IS NOT NULL
	drop procedure dbo.usp_get_ChamadasTelefonicas
go


CREATE PROCEDURE [dbo].[usp_get_ChamadasTelefonicas]
	@numeroTel			VARCHAR(100),
	@DataInicio			DATE,
	@DataFim			DATE,
	@tipo				VARCHAR(100),
	@Number				NUMERIC(10,0),
	@estab				NUMERIC(3,0),
	@motivo				VARCHAR(50)

AS
SET NOCOUNT ON

SET @numeroTel	= ISNULL(@numeroTel,'')
SET @DataInicio = ISNULL(@DataInicio,'19000101')
SET @DataFim	= ISNULL(@DataFim,'30000101')
SET @tipo		= ISNULL(@tipo,'')
SET @Number		= ISNULL(@Number,-1)
SET @estab		= ISNULL(@estab,-1)
SET @motivo		= ISNULL(@motivo,'')

If OBJECT_ID('tempdb.dbo.#temp_Numeros') IS NOT NULL
		drop table #temp_Numeros;	

If OBJECT_ID('tempdb.dbo.#temp_User') IS NOT NULL
		drop table #temp_User;	

 SELECT 
	  nome,
	  no,
	  estab,
	  telefone1,
	  telemovel,	
	  tipo
	INTO #temp_Numeros
				FROM (

				select 
					nome             as  nome,
					no				 as	 no,
					estab			 as  estab,
					telefone         as  telefone1,
					tlmvl            as  telemovel,					
					'utentes'        as  tipo
				from b_utentes (nolock)

				UNION ALL 
				select 
					nome             AS  nome,
					no				 AS	 no,
					estab			 AS  estab,
					telefone         AS  telefone1,
					tlmvl            AS  telemovel,				
					'fornecedores'   AS  tipo
				from FL (nolock)

				UNION ALL 

				--type =1 extensao final
				SELECT 
					nome																	AS  nome,
					convert(Numeric(10,0),isnull(nullif(extensaotelefone,''),0))			AS	 no,
					convert(Numeric(3,0),0)													AS  estab,
					'Ext.'+extensaotelefone													AS  telefone1,
					'Ext.'+extensaotelefone													AS  telemovel,					
					'operadores'															AS  tipo
				FROM B_us (nolock)

				UNION ALL 

				--type =2 extensao intermedia
				SELECT 
					nome																	AS  nome,
					--convert(Numeric(10,0),extensaotelefone)			AS	 no,
					convert(Numeric(10,0),isnull(nullif(extensaotelefone,''),0))			AS	 no,					
					convert(Numeric(3,0),0)													AS  estab,
					'Ext.'+extensaotelefone													AS  telefone1,
					'Ext.'+extensaotelefone													AS  telemovel,					
					'operadores'															AS  tipo
				FROM B_ug (nolock) where extensaotelefone != 'HOL'

				UNION ALL

				SELECT 
					nome																	AS nome,					
					no																		AS	 no,					
					estab																	AS  estab,
					'Ext.' + telefone														AS  telefone1,
					'Ext.' + telefone														AS  telemovel,					
					CASE 
						When tipo = 'U' then 'utentes'	
						When tipo = 'F' then 'fornecedores' 
						END 																AS  tipo				
				FROM Telefones

		) as x

--select * from #temp_Numeros



IF @Number !=-1 or @tipo !=''
BEGIN 
	
	 SELECT 
	  nome,
	  no,
	  estab,
	  telefone1,
	  telemovel,	
	  tipo
	INTO #temp_User
				FROM (

				SELECT 
					nome																AS  nome,
					no																	AS	 no,
					estab																AS  estab,
					(CASE WHEN telefone !=''	THEN  telefone  ELSE  'VAZIO' END )		AS  telefone1,
					(CASE WHEN tlmvl   !=''		THEN  tlmvl   ELSE 'VAZIO' END)			AS telemovel,					
					'utentes'															AS  tipo
				from b_utentes (nolock)
				WHERE (@tipo ='utentes' or @tipo ='')
					AND (telefone !='' OR tlmvl!='')
					AND  b_utentes.no =( CASE WHEN @Number!=-1 Then @Number else   b_utentes.no END ) 
				    AND  b_utentes.estab =( CASE WHEN @estab =-1 and @Number!=-1 Then 0 
												WHEN @estab !=-1 and @Number!=-1 Then @estab
												ELSE  b_utentes.estab END )  

				UNION ALL 

				select 
					nome																AS  nome,
					no																	AS	 no,
					estab																AS  estab,
					( CASE WHEN telefone!=''	THEN  telefone  ELSE  'VAZIO' END )     AS  telefone1,
					( CASE WHEN tlmvl!=''		THEN  tlmvl  ELSE  'VAZIO' END )        AS  telemovel,					
					'fornecedores'														AS  tipo
				from FL (nolock)
				WHERE (@tipo ='fornecedores' or @tipo ='')
					AND (telefone !='' OR tlmvl!='')
					AND  FL.no =( CASE WHEN @Number!=-1 Then @Number else   FL.no END ) 
				    AND  FL.estab =( CASE WHEN @estab =-1 and @Number!=-1 Then 0 
												WHEN @estab !=-1 and @Number!=-1 Then @estab
												ELSE  FL.estab END )  

				UNION ALL 

				SELECT 
					nome																	AS  nome,					
					convert(Numeric(10,0),isnull(nullif(extensaotelefone,''),0))			AS	 no,
					convert(Numeric(3,0),0)													AS  estab,
					'Ext.'+extensaotelefone													AS  telefone1,
					'Ext.'+extensaotelefone													AS  telemovel,			
					'operadores'															AS  tipo
				FROM B_us (nolock)
				WHERE (@tipo ='Operadores' or @tipo ='') AND extensaotelefone !=''
					AND  B_us.extensaotelefone =( CASE WHEN @Number!=-1 Then Rtrim(Ltrim(CONVERT(VARCHAR(100),@Number))) else   B_us.extensaotelefone END ) 


				UNION ALL 

				SELECT 
					nome																	AS  nome,					
					convert(Numeric(10,0),isnull(nullif(extensaotelefone,''),0))			AS	 no,
					convert(Numeric(3,0),0)													AS  estab,
					'Ext.'+extensaotelefone													AS  telefone1,
					'Ext.'+extensaotelefone													AS  telemovel,
					'operadores'															AS  tipo
				FROM B_ug (nolock)
				WHERE (@tipo ='Operadores' or @tipo ='') AND extensaotelefone !=''
					AND  B_ug.extensaotelefone =( CASE WHEN @Number!=-1 Then Rtrim(Ltrim(CONVERT(VARCHAR(100),@Number))) else   B_ug.extensaotelefone END ) 
					AND B_ug.extensaotelefone != 'HOL'


				UNION ALL 

				select 
					''				 AS  nome,
					0				 AS	 no,
					0				 AS  estab,
					@numeroTel       AS  telefone1,
					@numeroTel       AS  telemovel,					
					'fornecedores'   AS  tipo
				WHERE @numeroTel !=''

				UNION ALL

				SELECT 
					nome										AS nome,					
					no											AS	 no,					
					estab										AS  estab,
					'Ext.' + telefone							AS  telefone1,
					'Ext.' + telefone							AS  telemovel,					
					CASE 
						When tipo = 'U' then 'utentes'	
						When tipo = 'F' then 'fornecedores' 
						END 									AS  tipo		
				FROM Telefones
				WHERE (@tipo ='utentes' or @tipo ='fornecedores' or @tipo ='') AND telefone !=''
					AND  Telefones.no =( CASE WHEN @Number!=-1 Then @Number else   Telefones.no END ) 
				    AND  Telefones.estab =( CASE WHEN @estab =-1 and @Number!=-1 Then 0 
												WHEN @estab !=-1 and @Number!=-1 Then @estab
												ELSE  Telefones.estab END )  

		) as x

--select * from #temp_User

		
	SELECT 
		historyid																																					AS codigo,
		Convert(varchar(8),duration)																																AS duracao, 
		Cast(timeAnswered as smalldatetime)																															AS inicioData,
	  --CONVERT(VARCHAR(8),timeAnswered,108)																														AS inicioHora,
		Cast(timeEnd as smalldatetime)																																AS terminouData,
	  --CONVERT(VARCHAR(8),timeEnd,108)																																AS terminouHora,  
		reasonTerminated																																			AS motivo,
		fromNumber																																					AS origem,
		ISNULL((SELECT TOP 1 nome from #temp_Numeros 
				where(#temp_Numeros.telefone1 = logsPhoneCalls.fromNumber OR #temp_Numeros.telemovel = logsPhoneCalls.fromNumber)),'')								AS nomeOrigem,
				(CASE WHEN finalNumber!='' THEN finalNumber ELSE toNumber END)																						AS destino,
		ISNULL((SELECT TOP 1 nome from #temp_Numeros 
				where (#temp_Numeros.telefone1 = (CASE WHEN finalNumber!='' THEN finalNumber ELSE  (CASE WHEN toNumber!='' then toNumber ELSE 'VAZIO' END )END)	 
					OR #temp_Numeros.telemovel = (CASE WHEN finalNumber!='' THEN finalNumber ELSE (CASE WHEN toNumber!='' then toNumber ELSE 'VAZIO' END)END))),'')  AS nomeDestino,
		Convert (varchar(254),Rtrim(Ltrim(ISNULL(chain,''))))																										 AS fluxo,
		ISNULL((SELECT top 1 ISNULL(filename,'') from anexos  where regstamp= cxStamp and tabela = 'logsPhoneCalls' ),'')											 AS gravacao
	FROM  
	logsPhoneCalls (nolock)
		WHERE   CONVERT(DATE, timeStart) between @DataInicio and @DataFim
				AND  reasonTerminated = (CASE WHEN @motivo != '' THEN @motivo ELSE reasonTerminated end )
				AND ( 
					fromNumber IN (SELECT telefone1 FROM #temp_User ) or  fromNumber IN (SELECT telemovel FROM #temp_User )  OR
					finalNumber IN (SELECT telefone1 FROM #temp_User ) or  finalNumber IN (SELECT telemovel FROM #temp_User )  OR
					toNumber IN (SELECT telefone1 FROM #temp_User ) or  toNumber IN (SELECT telemovel FROM #temp_User ))


END 
ELSE
BEGIN   
	SELECT 
		historyid																																					AS codigo,
		Convert(varchar(8),duration)																																AS duracao, 
		Cast(timeAnswered as smalldatetime)																															AS inicioData,
	    --CONVERT(VARCHAR(8),timeAnswered,108)																														AS inicioHora,
		Cast(timeEnd as smalldatetime)																																AS terminouData,
		--CONVERT(VARCHAR(8),timeEnd,108)																															AS terminouHora,  
		reasonTerminated																																			AS motivo,
		fromNumber																																					AS origem,
		ISNULL((SELECT TOP 1 nome from #temp_Numeros 
				where(#temp_Numeros.telefone1 = logsPhoneCalls.fromNumber OR #temp_Numeros.telemovel = logsPhoneCalls.fromNumber )),'')								AS nomeOrigem,
				(CASE WHEN finalNumber!='' THEN finalNumber ELSE toNumber END)																						AS destino,
		ISNULL((SELECT TOP 1 nome from #temp_Numeros 
				where (#temp_Numeros.telefone1 = (CASE WHEN finalNumber!='' THEN finalNumber ELSE  (CASE WHEN toNumber!='' then toNumber ELSE 'VAZIO' END )END)	 
					OR #temp_Numeros.telemovel = (CASE WHEN finalNumber!='' THEN finalNumber ELSE (CASE WHEN toNumber!='' then toNumber ELSE 'VAZIO' END)END))),'')  AS nomeDestino,
		Convert (varchar(254),Rtrim(Ltrim(ISNULL(chain,''))))																										 AS fluxo,
		ISNULL((SELECT top 1 ISNULL(filename,'') from anexos  where regstamp= cxStamp and tabela = 'logsPhoneCalls' ),'')											 AS gravacao
	FROM  
	logsPhoneCalls (nolock)
		WHERE   CONVERT(DATE, timeStart) between @DataInicio and @DataFim
				AND  reasonTerminated = (CASE WHEN @motivo != '' THEN @motivo ELSE reasonTerminated end )
				AND  (
						fromNumber = (CASE WHEN @numeroTel != '' THEN @numeroTel ELSE fromNumber end ) OR  
						finalNumber = (CASE WHEN @numeroTel != '' THEN @numeroTel ELSE finalNumber end ) OR 
						toNumber = (CASE WHEN @numeroTel != '' THEN @numeroTel ELSE toNumber end ))

END 

If OBJECT_ID('tempdb.dbo.#temp_Numeros') IS NOT NULL
		drop table #temp_Numeros;	

If OBJECT_ID('tempdb.dbo.#temp_User') IS NOT NULL
		drop table #temp_User;	

GO
GRANT EXECUTE ON dbo.usp_get_ChamadasTelefonicas to Public
GRANT CONTROL ON dbo.usp_get_ChamadasTelefonicas to Public
GO

