
/* retorna informacao se existir chamada para aquele audio 

servico_getInfoToSend  'Call 340' ,'00000170ECC4C79E_98','Loja 1'

servico_getInfoToSend  'Call 5977' ,'000001773DFDAA1F_204','Loja 1'

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


IF OBJECT_ID('[dbo].[servico_getInfoToSend]') IS NOT NULL
		DROP PROCEDURE [dbo].[servico_getInfoToSend] ;
GO


CREATE PROCEDURE [dbo].[servico_getInfoToSend]
	@historyid		  VARCHAR(100),
	@callid			  VARCHAR(100),
	@site		      VARCHAR(20)
AS
SET NOCOUNT ON
	DECLARE @cxStamp			VARCHAR(36)
	DECLARE @reasonTerminated	VARCHAR(200)
	DECLARE @name				VARCHAR(36)
	DECLARE @fromNumber			VARCHAR(100)
	DECLARE @date				SMALLDATETIME
	DECLARE @chain				VARCHAR(500)
	DECLARE @fromDn				VARCHAR(100)
	DECLARE @firstNumber        VARCHAR(100)
	DECLARE @lastNumber			VARCHAR(100)
	DECLARE @email				VARCHAR(200)
	DECLARE @lastExtension		VARCHAR(100)

	SET @name= ''

	If OBJECT_ID('tempdb.dbo.#temp_Numeros') IS NOT NULL
		drop table #temp_Numeros;	

	SELECT 
		@cxStamp= cxStamp ,
		@fromNumber =fromNumber,
		@reasonTerminated=reasonTerminated ,
		@date = Cast(timeAnswered as smalldatetime)	, 
		@chain = chain,
		@firstNumber = SUBSTRING(logsPhoneCalls.chain, PATINDEX('%[0-9]%', chain), PATINDEX('%[0-9][^0-9]%', chain + 't') - PATINDEX('%[0-9]%', chain) + 1),
		@lastNumber = REVERSE(SUBSTRING(REVERSE(chain), PATINDEX('%[0-9]%', REVERSE(chain)), PATINDEX('%[0-9][^0-9]%', REVERSE(chain) + 't') - PATINDEX('%[0-9]%', REVERSE(chain)) + 1)),
		@fromDn =fromDn,
		@lastExtension=	REPLACE(SUBSTRING(REVERSE(SUBSTRING(REVERSE(chain), CHARINDEX(REVERSE(';'), REVERSE(chain)), 
                 CHARINDEX(REVERSE('Ext.'), REVERSE(chain)+'t') - CHARINDEX(';', REVERSE(chain))))
				 ,0,CHARINDEX(';',REVERSE(SUBSTRING(REVERSE(chain), CHARINDEX(REVERSE(';'), REVERSE(chain)), 
                 CHARINDEX(REVERSE('Ext.'), REVERSE(chain)+'t') - CHARINDEX(';', REVERSE(chain)))))+1),';','')
	FROM logsPhoneCalls 
		WHERE historyid=@historyid and callid=@callid and site = @site 

	SET @email =''
	SELECT @email = isnull(email,'') FROM b_ug (Nolock) WHERE extensaotelefone = @lastExtension and loja = @site

	IF (@email = '')
	BEGIN
			SELECT @email = case when isnull(email,'') ='' then (SELECT textValue FROM B_Parameters_site WHERE STAMP='ADM0000000112' AND SITE=@site) else email end FROM b_us (Nolock) WHERE extensaotelefone = @lastExtension and loja = @site
	END


	IF (@cxStamp!='' AND  
			(
				(@fromDn = 'QCB' AND  @email!='' ) 
				OR 
				(@fromDn != 'QCB' AND (select COUNT(*) from b_ug (Nolock) where extensaotelefone IN (@lastNumber,@lastExtension)and @firstNumber!=@lastExtension)!=0)
			)
		)
	BEGIN 
		 SELECT 
		  nome,
		  telefone1,
		  telemovel,		 
		  tipo
		INTO #temp_Numeros
				FROM (

					select 
						nome             as  nome,
						telefone         as  telefone1,
						tlmvl            as  telemovel,						
						'utentes'        as  tipo
					from b_utentes (nolock)

					UNION ALL 

					select 
						nome             AS  nome,
						telefone         AS  telefone1,
						tlmvl            AS  telemovel,						
						'fornecedores'   AS  tipo
					from FL (nolock)

					UNION ALL 

					/*
					SELECT 
						NAME									AS  nome,
						'Ext.'+extension						AS  telefone1,
						'Ext.'+extension						AS  telemovel,						
						'operadores'							AS  tipo
					FROM extensionPhone (nolock)
					*/

					SELECT 
						nome												AS nome,	
						'Ext.' + telefone									AS  telefone1,
						'Ext.' + telefone									AS  telemovel,					
						CASE 
							When tipo = 'U' then 'utentes'	
							When tipo = 'F' then 'fornecedores' 
							END 											AS  tipo				
					FROM Telefones

					UNION ALL 

					SELECT 
						nome												AS  nome,						
						'Ext.'+extensaotelefone								AS  telefone1,
						'Ext.'+extensaotelefone								AS  telemovel,					
						'operadores'										AS  tipo
					FROM B_us (nolock)

					UNION ALL 

					SELECT 
						nome												AS  nome,						
						'Ext.'+extensaotelefone								AS  telefone1,
						'Ext.'+extensaotelefone								AS  telemovel,					
						'operadores'										AS  tipo
					FROM B_ug (nolock) where extensaotelefone != 'HOL'
					
			) as x

			
			SELECT TOP 1 @name= ISNULL(nome,'') from #temp_Numeros 
				where(#temp_Numeros.telefone1 = @fromNumber OR #temp_Numeros.telemovel = @fromNumber )

			SELECT 
				CASE WHEN @fromDn !='QCB' THEN '3CXP: ' + @fromNumber + '- ' +@name  ELSE (case when @name !='' then '3CXS: ' + @fromNumber + '- ' +@name else '3CXS: ' + @fromNumber END )END assunto ,
				'DATA: ' + convert(VARCHAR, @date,120)  + '<br /> FILA: ' +  @chain + '<br /> NUMERO: ' + @fromNumber   + '<br /> NOME: ' + @name AS corpo, 
				@email AS emails
				
	END
	ELSE 
	BEGIN
			SELECT 
				'' assunto, 
				'' corpo,
				'' emails
	
	END  

	If OBJECT_ID('tempdb.dbo.#temp_Numeros') IS NOT NULL
		drop table #temp_Numeros;	

GO
GRANT EXECUTE on dbo.servico_getInfoToSend TO PUBLIC
GRANT Control on dbo.servico_getInfoToSend TO PUBLIC
GO

