
/* retorna informacao se existir chamada para aquele audio 

exec servico_getInfoGravar  '2101' ,'8023','2020-03-12 11:05:28'

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


IF OBJECT_ID('[dbo].[servico_getInfoGravar]') IS NOT NULL
		DROP PROCEDURE [dbo].[servico_getInfoGravar] ;
GO


CREATE PROCEDURE [dbo].[servico_getInfoGravar]

	@fromNumber		  VARCHAR(100),
	@toNumber		  VARCHAR(100),
	@DateTime		  DATETIME
AS
SET NOCOUNT ON

	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#logPhones'))
		DROP TABLE #logPhones
		
	
	SELECT 
		cxStamp,
		SUBSTRING(logsPhoneCalls.chain, PATINDEX('%[0-9]%', chain), PATINDEX('%[0-9][^0-9]%', chain + 't') - PATINDEX('%[0-9]%', chain) + 1) AS firstNumber,
		REVERSE(SUBSTRING(REVERSE(chain), PATINDEX('%[0-9]%', REVERSE(chain)), PATINDEX('%[0-9][^0-9]%', REVERSE(chain) + 't') - PATINDEX('%[0-9]%', REVERSE(chain)) + 1)) AS lastNumber,
		timeAnswered,
		site
	into #logPhones
	FROM 
		logsPhoneCalls
	WHERE chain !=''  and timeAnswered BETWEEN   DATEADD(ss,-30,@DateTime)  AND  DATEADD(ss,30,@DateTime)

	SELECT 
		TOP 1
		#logPhones.cxStamp,
		#logPhones.site,
		(select textValue from B_Parameters_site(nolock) where stamp='ADM0000000010' and site = #logPhones.site) As caminho
	FROM #logPhones
		WHERE firstNumber!= lastNumber and (#logPhones.firstNumber in (@fromNumber,@toNumber) and #logPhones.lastNumber in (@fromNumber,@toNumber))
	order by timeAnswered desc


GO
GRANT EXECUTE on dbo.servico_getInfoGravar TO PUBLIC
GRANT Control on dbo.servico_getInfoGravar TO PUBLIC
GO

