
/*  Inserir chamada 3cx 
exec servico_getExtensoesTelefone 'Loja 1;Loja 2'
exec servico_getExtensoesTelefone 'Loja 1'
exec serviço_getExtensoesTelefone ''
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[servico_getExtensoesTelefone]') IS NOT NULL
	drop procedure dbo.servico_getExtensoesTelefone
go

CREATE PROCEDURE [dbo].[servico_getExtensoesTelefone]
	@site VARCHAR(300) =''
AS
SET NOCOUNT ON

	If OBJECT_ID('tempdb.dbo.#temp_Sites') IS NOT NULL
			drop table #temp_Sites;	
		
	create  table #temp_Sites (
		[siteTemp] varchar(30)
	)
	SET @site		=   rtrim(ltrim(isnull(@site,'')))


	IF(@site <> '')
	BEGIN 
	print 1
		INSERT INTO #temp_Sites (siteTemp)
		SELECT * FROM dbo.up_splitToTable(@site,';')
	END
	ELSE
	BEGIN 
		INSERT INTO #temp_Sites (siteTemp)
		SELECT site FROM Empresa

		
	END  

	/*
	SELECT DISTINCT
		extension				AS ext,
		extensionPhone.site		AS site 		
	FROM extensionPhone 
	WHERE extension !='' and extensionPhone.site  IN  (SELECT siteTemp FROM  #temp_Sites)
	ORDER BY SITE ASC -- é muito importante que esteja ordenado para ler
	*/

	SELECT DISTINCT ext, site,design,type from (
		SELECT 
			extensaotelefone				AS ext,
			loja							AS site,
			iniciais						AS design,
			1								AS type
		FROM b_us	
		union 
		SELECT 
			extensaotelefone				AS ext,
			loja							AS site,
			nome							AS design,
		    2 								AS type	
		FROM b_ug
	) k	
	WHERE ext !='' AND ext!='HOL' AND site COLLATE DATABASE_DEFAULT IN  (SELECT siteTemp FROM  #temp_Sites)
	ORDER BY SITE ASC -- é muito importante que esteja ordenado para ler

	 
	If OBJECT_ID('tempdb.dbo.#temp_Sites') IS NOT NULL
			drop table #temp_Sites;	

GO
GRANT EXECUTE ON dbo.servico_getExtensoesTelefone to Public
GRANT CONTROL ON dbo.servico_getExtensoesTelefone to Public
GO



