

/*  Inserir chamada 3cx 
serviço_insere_ChamadaTelefonicas
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[serviço_insere_ChamadaTelefonicas]') IS NOT NULL
	drop procedure dbo.serviço_insere_ChamadaTelefonicas
go

CREATE PROCEDURE [dbo].[serviço_insere_ChamadaTelefonicas]
	@historyid		  VARCHAR(100),
	@callid			  VARCHAR(100),
	@duration		  VARCHAR(100),
	@timeStart		  DATETIME,
	@timeAnswered     DATETIME,
	@timeEnd		  DATETIME,
	@reasonTerminated VARCHAR(200),
	@fromNumber		  VARCHAR(100),
	@toNumber		  VARCHAR(100),
	@fromDn			  VARCHAR(100),
	@toDn             VARCHAR(100),
	@dialNo           VARCHAR(100),
	@reasonChanged	  VARCHAR(200),
	@finalNumber      VARCHAR(100),
	@finalDn          VARCHAR(100),
	@chain			  VARCHAR(500),
	@site		      VARCHAR(20),
	@processed		  BIT =0
AS
SET NOCOUNT ON

	IF( NOT EXISTS(SELECT * FROM logsPhoneCalls where historyid=@historyid and callid=@callid and site = @site))
	BEGIN
		INSERT INTO  logsPhoneCalls (cxStamp,historyid,callid,duration,timeStart,timeAnswered,timeEnd,reasonTerminated,fromNumber,toNumber,fromDn,toDn,dialNo,reasonChanged,finalNumber,finalDn,chain,ousrdata,usrdata,site,processed)
		VALUES (LEFT(NEWID(),36),@historyid,@callid,@duration,@timeStart,@timeAnswered,@timeEnd,@reasonTerminated,@fromNumber,@toNumber,@fromDn,@toDn,@dialNo,@reasonChanged,@finalNumber,@finalDn,@chain,GETDATE(),GETDATE(),@site,@processed)
	END
	ELSE
	BEGIN
		UPDATE   logsPhoneCalls 
		SET  		 
			duration		 =@duration,		 
			timeStart		 =@timeStart,		 
			timeAnswered	 =@timeAnswered,    
			timeEnd			 =@timeEnd,		 
			reasonTerminated =@reasonTerminated,
			fromNumber		 =@fromNumber,		 
			toNumber		 =@toNumber,		 
			fromDn			 =@fromDn,			 
			toDn             =@toDn,            
			dialNo           =@dialNo,          
			reasonChanged	 =@reasonChanged,	 
			finalNumber      =@finalNumber,     
			finalDn          =@finalDn,         
			chain			 =@chain,
			usrdata			 = GETDATE(),
			site			 = @site,
			processed		 =@processed		 
		WHERE historyid=@historyid and callid=@callid
	END  
GO
GRANT EXECUTE ON dbo.serviço_insere_ChamadaTelefonicas to Public
GRANT CONTROL ON dbo.serviço_insere_ChamadaTelefonicas to Public
GO

