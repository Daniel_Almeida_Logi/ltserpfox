

/*  Inserir chamada 3cx 

exec servico_insere_ChamadaTelefonicasEmAndamento '46544d81-3fc5-4a56-afa4-27fac6263cex' ,'Call 2033', 'Loja 1'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[servico_insere_ChamadaTelefonicasEmAndamento]') IS NOT NULL
	drop procedure dbo.servico_insere_ChamadaTelefonicasEmAndamento
go

CREATE PROCEDURE [dbo].[servico_insere_ChamadaTelefonicasEmAndamento]
	@cxStamp			VARCHAR(36),
	@historyid			VARCHAR(100),
	@site				VARCHAR(20),
	@date				VARCHAR(20)
AS
SET NOCOUNT ON

	IF( NOT EXISTS(SELECT * FROM logsPhoneCalls where historyid=@historyid and cxStamp=@cxStamp and site = @site and timeStart=@date))
	BEGIN
		INSERT INTO  logsPhoneCalls (cxStamp,historyid,timeStart,ousrdata,site,processed)
		VALUES (@cxStamp,@historyid,@date,GETDATE(),@site,0)
	END 
GO
GRANT EXECUTE ON dbo.servico_insere_ChamadaTelefonicasEmAndamento to Public
GRANT CONTROL ON dbo.servico_insere_ChamadaTelefonicasEmAndamento to Public
GO
