/*
	exec up_devolveSintomasPorMedCalculadora '', 100, 1, 'dor'
*/

-- =============================================
-- Author:		José Moreira
-- Create date: 2022-12-19
-- Description: Returns the symptoms for the giving medication 
-- =============================================


--code to validate if the procedure exists, if exists, drops procedure
if OBJECT_ID('[dbo].[up_devolveSintomasPorMedCalculadora]') IS NOT NULL
	drop procedure dbo.up_devolveSintomasPorMedCalculadora
	GO

Create PROCEDURE [dbo].[up_devolveSintomasPorMedCalculadora]  
	@idMed			varchar(100) 
	,@topMax		int				=	100
	,@pageNumber	int				=	1
	,@search		varchar(max)	=  ''

AS
BEGIN

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#calculatePages'))
		DROP TABLE #calculatePages

Declare
	@sql varchar(8000)

	if(ISNULL(@idMed, '') = '')
	begin
		set @idMed = ''
	end
	if(isnull(@topMax,0)>100)
	begin
		set @topMax = 100
	end
	if(isnull(@topMax,0)<=0)
	begin
		set @topMax = 100
	end
	if(isnull(@pageNumber,0)<1)
	begin
		set @pageNumber = 1
	end

	DECLARE @PageSize int = isnull(@topMax,100)

	set @sql =  'SELECT distinct 
        temas.* 
    INTO 
		#calculatePages
	FROM 
        base (nolock)   
        INNER JOIN base_x_idade (nolock) 
            ON base.id=base_x_idade.id_base 
        INNER JOIN temas (nolock)  
            ON temas.id=base_x_idade.id_tema 
    WHERE 
		1 = 1 '
	
	IF @idMed != ''
	BEGIN
		set @sql = @sql + '	AND base.id_med =' + @idMed + ''
	END

	IF @search != ''
	BEGIN
		set @sql = @sql + '	AND temas.descricao LIKE ''%'+convert(varchar,@search)+ '%'' '
	END

	set @sql = @sql + '
	SELECT '  
		+ convert(varchar(10),@PageSize) +' as pageSize,
		(COUNT(*) OVER() / ' +  convert(varchar(10),@PageSize) +') + 1 as pageTotal, '
		+ convert(varchar(10),@PageNumber) +' as pageNumber, 
		COUNT(*) OVER() as linesTotal,
		#calculatePages.*
	FROM #calculatePages
	ORDER BY 
		#calculatePages.nome_resultado ASC 
		OFFSET ' + convert(varchar(10),@PageSize) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
		FETCH NEXT  ' + convert(varchar(10),@PageSize) + ' ROWS ONLY '	

   print @sql
   
   exec(@sql)

   
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#calculatePages'))
		DROP TABLE #calculatePages
END
