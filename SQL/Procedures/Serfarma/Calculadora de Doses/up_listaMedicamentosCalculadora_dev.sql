
--code to validate if the procedure exists, if exists, drops procedure
if OBJECT_ID('[dbo].[up_listaMedicamentosCalculadora_dev]') IS NOT NULL
	drop procedure dbo.up_listaMedicamentosCalculadora_dev
	GO
/*
	exec up_listaMedicamentosCalculadora_dev 'broncoliber'
*/
-- =============================================
-- Author:		José Moreira
-- Create date: 2022-11-07
-- Description: Returns all meds available
-- =============================================
CREATE PROCEDURE [dbo].[up_listaMedicamentosCalculadora_dev]  
	@ref varchar(20)
AS
BEGIN 
	IF OBJECT_ID(N'tempdb..#maxDate') IS NOT NULL
		DROP TABLE #maxDate
	IF OBJECT_ID(N'tempdb..#artigos_ativos') IS NOT NULL
		DROP TABLE #artigos_ativos
-------------------------------------------------------------------------------------------
	select 
		max(isnull(data,'3000-01-01')) data,
		emb_id 
	into 
		#maxDate 
	from
		comerc (nolock)
	group by 
		emb_id  
-------------------------------------------------------------------------------------------
	select 
		emb.Med_ID 
	into
		#artigos_ativos
	from 
		comerc		(nolock)
	inner join 
		#maxDate	(nolock)	on		comerc.Emb_ID = #maxDate.Emb_ID	
								and		isnull(comerc.Data,'3000-01-01') = #maxDate.data
	inner join 
		emb			(nolock)	on		comerc.Emb_ID = emb.Emb_ID
	where 
		comerc.activo = 1
		and comerc.Dispo_ID in (0, 1, 4)
	order by 
		#maxDate.data desc
-------------------------------------------------------------------------------------------
	SELECT  distinct
		base.id as id_base, 
		base.id_med, 
		med.Nome, 
		med.Dosagem, 
		dcipt.Descr as dcipt, 
		form_farm.Descr as forma_farmaceutica,
		form_farm.Form_Farm_ID,
		isnull(med.nome+ ',' ,'') + ISNULL(med.Dosagem+ ',', '')  +isnull(dcipt.descr +',', '')+ isnull(form_farm.descr, '') as combox_field
	FROM 
		base (nolock)
	INNER JOIN	med 			ON	base.id_med=med.Med_ID 
	LEFT JOIN	dcipt  			ON	med.DCIPT_ID=dcipt.DCIPT_ID 
	INNER JOIN	form_farm  		ON	med.Form_Farm_ID=form_farm.Form_Farm_ID 
	INNER JOIN	#artigos_ativos ON	#artigos_ativos.Med_ID=base.id_med
    WHERE  
		--form_farm.Form_Farm_ID IN('1','2','3','4','5','6','13','19','7','9','11','8','10') 
		--and 
		med.Nome like @ref
	ORDER BY 
		med.Nome ASC 
-------------------------------------------------------------------------------------------
	IF OBJECT_ID(N'tempdb..#maxDate') IS NOT NULL
		DROP TABLE #maxDate
	IF OBJECT_ID(N'tempdb..#artigos_ativos') IS NOT NULL
		DROP TABLE #artigos_ativos
END 