--code to validate if the procedure exists, if exists, drops procedure
if OBJECT_ID('[dbo].[up_obter_url_para_o_medicamento]') IS NOT NULL
	drop procedure dbo.up_obter_url_para_o_medicamento
	GO
/*
	exec up_obter_url_para_o_medicamento '40'
*/

-- =============================================
-- Author:		José Moreira
-- Create date: 2022-11-07
-- Description: Returns url for The FI and RCM of the med
-- =============================================
CREATE PROCEDURE [dbo].[up_obter_url_para_o_medicamento]  
	@medicamento varchar(MAX)
AS
BEGIN
	declare						 
		@urlFI varchar(MAX)		= 'https://extranet.infarmed.pt/INFOMED-fo/download-ficheiro.xhtml?med_guid=introduzir_med_guid&tipo_doc=fi'
		,@urlRCM varchar(MAX)	= 'https://extranet.infarmed.pt/INFOMED-fo/download-ficheiro.xhtml?med_guid=introduzir_med_guid&tipo_doc=rcm'
		,@medGuid varchar(MAX)

		select @medGuid=med_guid from med (nolock) where id = @medicamento
		
		select 
			REPLACE(@urlFI, 'introduzir_med_guid',  @medGuid ) urlPatientInformationLeaflet ,
			REPLACE(@urlRCM, 'introduzir_med_guid',  @medGuid ) urlSummaryOfProductCharacteristic
END

 