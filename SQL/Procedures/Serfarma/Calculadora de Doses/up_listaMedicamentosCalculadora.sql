/*
	exec up_listaMedicamentosCalculadora '',100,1, ''
*/
-- =============================================
-- Author:		José Moreira
-- Create date: 2022-11-07
-- Description: Returns all meds available
-- =============================================
--code to validate if the procedure exists, if exists, drops procedure

if OBJECT_ID('[dbo].[up_listaMedicamentosCalculadora]') IS NOT NULL
	drop procedure dbo.up_listaMedicamentosCalculadora
	GO

CREATE  PROCEDURE [dbo].[up_listaMedicamentosCalculadora]  
	@id_form_farm	VARCHAR(100)	=	'1,2,3,4,5,6,13,19,7,9,11,8,10' 
	,@topMax		INT				=	100
	,@pageNumber	INT				=	1
	,@search		VARCHAR(max)	=	''

AS
BEGIN
IF OBJECT_ID(N'tempdb..#maxDate') IS NOT NULL
		DROP TABLE #maxDate
	IF OBJECT_ID(N'tempdb..#artigos_ativos') IS NOT NULL
		DROP TABLE #artigos_ativos
-------------------------------------------------------------------------------------------
	select 
		max(isnull(data,'3000-01-01')) data,
		emb_id 
	into 
		#maxDate 
	from
		comerc (nolock)
	group by 
		emb_id  
-------------------------------------------------------------------------------------------
	select 
		emb.Med_ID 
	into
		#artigos_ativos
	from 
		comerc		(nolock)
	inner join 
		#maxDate	(nolock)	on		comerc.Emb_ID = #maxDate.Emb_ID	
								and		isnull(comerc.Data,'3000-01-01') = #maxDate.data
	inner join 
		emb			(nolock)	on		comerc.Emb_ID = emb.Emb_ID
	where 
		comerc.activo = 1
		and comerc.Dispo_ID in (0, 1, 4, 9)
	order by 
		#maxDate.data desc
Declare
	@sql varchar(8000)

	if(ISNULL(@id_form_farm, '') = '')
	begin
		set @id_form_farm = '1,2,3,4,5,6,13,19,7,9,11,8,10'
	end
	if(isnull(@topMax,0)>100)
	begin
		set @topMax = 100
	end
	if(isnull(@topMax,0)<=0)
	begin
		set @topMax = 100
	end
	if(isnull(@pageNumber,0)<1)
	begin
		set @pageNumber = 1
	end

	DECLARE @PageSize int = isnull(@topMax,100) 

	set @sql = '
	SELECT '  
		+ convert(varchar(10),@PageSize) +' as pageSize,
		(COUNT(*) OVER() / ' +  convert(varchar(10),@PageSize) +') + 1 as pageTotal, '
		+ convert(varchar(10),@PageNumber) +' as pageNumber, 
		COUNT(*) OVER() as linesTotal,
		base.id as id_base, 
		base.id_med, 
		med.Nome, 
		med.Dosagem, 
		dcipt.Descr as dcipt, 
		form_farm.Descr as forma_farmaceutica,
		isnull(med.nome+ '','' ,'''') + ISNULL(med.Dosagem+ '','', '''')  +isnull(dcipt.descr +'','', '''')+ isnull(form_farm.descr, '''') as combox_field
	FROM 
		base   
	INNER JOIN	med  			ON	base.id_med=med.Med_ID 
	LEFT JOIN	dcipt  			ON	med.DCIPT_ID=dcipt.DCIPT_ID 
	INNER JOIN	form_farm  		ON	med.Form_Farm_ID=form_farm.Form_Farm_ID 
    INNER JOIN	#artigos_ativos ON	#artigos_ativos.Med_ID=base.id_med
    WHERE  
		med.Form_Farm_ID IN('+@id_form_farm+')'
	
	IF @search != ''
	BEGIN
		Set @sql = @sql + '
			AND( med.Nome		LIKE ''%'+convert(varchar,@search)+ '%''
			OR dcipt.Descr		LIKE ''%'+convert(varchar,@search)+ '%'')
		'	
	END

	set @sql = @sql + '	 
	ORDER BY 
		med.Nome ASC
		OFFSET ' + convert(varchar(10),@PageSize) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
		FETCH NEXT  ' + convert(varchar(10),@PageSize) + ' ROWS ONLY '
	print @sql
	exec(@sql)
	IF OBJECT_ID(N'tempdb..#maxDate') IS NOT NULL
		DROP TABLE #maxDate
	IF OBJECT_ID(N'tempdb..#artigos_ativos') IS NOT NULL
		DROP TABLE #artigos_ativos
END
