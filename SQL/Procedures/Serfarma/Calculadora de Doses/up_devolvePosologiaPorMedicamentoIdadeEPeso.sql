
--code to validate if the procedure exists, if exists, drops procedure
if OBJECT_ID('[dbo].[up_devolvePosologiaPorMedicamentoIdadeEPeso]') IS NOT NULL
	drop procedure dbo.up_devolvePosologiaPorMedicamentoIdadeEPeso
	GO

/*
	exec up_devolvePosologiaPorMedicamentoIdadeEPeso '20', '5807', '113', 300, 2
*/
-- =============================================
-- Author:		José Moreira
-- Create date: 2022-12-21
-- Description: Returns posology for product/age/weight
-- =============================================
CREATE PROCEDURE [dbo].[up_devolvePosologiaPorMedicamentoIdadeEPeso]  
	@idade varchar(10),
	@id_med varchar(10),
	@id_sintoma varchar(10),
	@peso varchar(10),
	@tipo int
AS
BEGIN
	SET NOCOUNT ON

	IF OBJECT_ID(N'tempdb..#tempRes') IS NOT NULL
	BEGIN
		DROP TABLE #tempRes
	END
	IF OBJECT_ID(N'tempdb..#res') IS NOT NULL
	BEGIN
		DROP TABLE #res
	END

	declare @id_idade  varchar(10)

	select @id_idade = id from idades (nolock) where idade = @idade and tipo = @tipo

	SELECT distinct top 100
		base_x_idade.id_idade,
		base_x_idade.id_tema,
		med.Med_ID,
		base_x_idade.freq_poso_doente as posologia, 
		base_x_idade.dose_maxima_diaria_doente as dose_maxima_diaria, 
		base_x_idade.numero_toma_doente as numero_toma, 
		base.modo_administracao, 
		base.precaucoes, 
		base.reacoes_adversas, 
		base.validade, 
		base.id as id_base, 
		base.condutor_texto, 
		base.gravida_texto, 
		base.lactante_texto, 
		base.lactante_texto2, 
		base.condutor, 
		base.foto_emb, 
		med.Class_Disp_ID as class_disp_id, 
		base.id_grupo,
		/*Added by José Moreira - logitools*/
		/*product info*/
		base.id as ProductInfo_id_base,
		base.id_med as ProductInfo_id_med,
		med.Nome as ProductInfo_nome,
		med.Dosagem  as ProductInfo_dosagem,
		dcipt.DCIPT_ID  as ProductInfo_dcipt,
		form_farm.Descr   as ProductInfo_forma_farmaceutica,
		/*client info*/
		@idade as Age_age,
		@tipo as Age_type,
		@peso as ClientInfo_weight,
		/*symptom info*/
		temas.id as SymptomInfo_id,
		temas.descricao as SymptomInfo_descricao,
		temas.nome_resultado as SymptomInfo_nomeResultado,
		temas.prioridade as SymptomInfo_prioridade,
		temas.nome_resultado_mc as SymptomInfo_nomeResultadoMc 
	INTO
		#tempRes
	FROM 
		med  
	INNER JOIN		form_farm		 			ON		med.Form_Farm_ID=form_farm.Form_Farm_ID 
	INNER JOIN		emb				 			ON		emb.Med_ID=med.Med_ID 
	INNER JOIN		base			 			ON		base.id_med=med.Med_ID 
	LEFT JOIN		base_x_idade	 			ON		base_x_idade.id_base=base.id 	
	LEFT JOIN		dcipt			 			ON		med.DCIPT_ID=dcipt.DCIPT_ID 
	LEFT JOIN		temas			 			ON		temas.id=base_x_idade.id_tema 
	WHERE
		base_x_idade.id_idade=@id_idade
	AND base_x_idade.id_tema=@id_sintoma
	and med.Med_ID=@id_med
	--GROUP BY a.Med_ID 
	if exists(select top 1 * from #tempRes where posologia is not null)
	begin
		select top 1 * from #tempRes where posologia is not null

		IF OBJECT_ID(N'tempdb..#tempRes') IS NOT NULL
		BEGIN
			DROP TABLE #tempRes
		END
	end
	else
	begin
		print 'não tenho resultado para os filtros aplicados'
		create table #res(
								id varchar(25) ,
								/*client info*/
								Age_age varchar(25),
	 							Age_type varchar(25),
								ClientInfo_weight varchar(25),
								ProductInfo_id_base varchar(25),
								ProductInfo_id_med varchar(25),
								ProductInfo_nome varchar(25),
								ProductInfo_dosagem varchar(25),
								ProductInfo_dcipt varchar(25),
								ProductInfo_forma_farmaceutica varchar(25),
								SymptomInfo_id varchar(25),
								SymptomInfo_descricao varchar(25),
								SymptomInfo_nomeResultado varchar(25),
								SymptomInfo_prioridade varchar(25),
								SymptomInfo_nomeResultadoMc varchar(250),
								Msg varchar(250)
								)
		print 'tabela construida'
		Declare @id varchar(25) = (select LEFT(newid(),11)+ REPLACE(REPLACE(replace(convert(varchar, getdate(), 120), '-', ''), ' ', ''), ':', ''))
		print 'id de linha definido: ' + @id
		insert into #res (id, Msg) values (@id, 'O medicamento selecionado não tem indicação para a idade/peso que selecionou.' /*'Verifique se os dados estão corretos ou consulte o RCM do medicamento.'*/)
		print 'id de linha inserido'
		--client info
		update 
			#res 
		set 
			Age_age				= @idade,
			Age_type			= @tipo,
			ClientInfo_weight	= @peso
		where 
			#res.id = @id
		print 'cliente atualizado'
		/*product info*/
		update 
			#res 
		set 
			ProductInfo_id_base				= base.id,
			ProductInfo_id_med				= base.id_med,
			ProductInfo_nome				= med.Nome,
			ProductInfo_dosagem				= med.Dosagem,
			ProductInfo_dcipt				= dcipt.DCIPT_ID,
			ProductInfo_forma_farmaceutica	= form_farm.Descr
		FROM 
			med						as	med 
			INNER JOIN	base		as	base		ON	base.id_med=med.Med_ID 
			LEFT JOIN	dcipt		as	dcipt		ON	med.DCIPT_ID=dcipt.DCIPT_ID 
			INNER JOIN	form_farm	as	form_farm	ON	med.Form_Farm_ID=form_farm.Form_Farm_ID 
		where 
			#res.id		= @id
			and med.Med_ID	= @id_med
		print 'produto atualizado'
		/*symptom info*/
		update 
			#res 
		set
			SymptomInfo_id					= temas.id,
			SymptomInfo_descricao			= temas.descricao,
			SymptomInfo_nomeResultado		= temas.nome_resultado,
			SymptomInfo_prioridade		= temas.prioridade,
			SymptomInfo_nomeResultadoMc		= temas.nome_resultado_mc
		from
			temas	as	temas
		where 
			#res.id		= @id
			and temas.id	= @id_sintoma 
		print 'sintoma	 atualizado'
		
		select * from #res

		IF OBJECT_ID(N'tempdb..#res') IS NOT NULL
		BEGIN
			DROP TABLE #res
		END
	end
END
