-- Limpa o historico das tabelas do rob�
-- exec up_historico_limparRobo
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_historico_limparRobo]') IS NOT NULL
	drop procedure dbo.up_historico_limparRobo
go

create procedure dbo.up_historico_limparRobo

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

if exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME='B_movtec_robot')
begin

	delete B_movtec_robot where oData < GETDATE()-60
	delete B_movtec_robot_log where data < GETDATE()-30
	
end

GO
Grant Execute On dbo.up_historico_limparRobo to Public
Grant Control On dbo.up_historico_limparRobo to Public
GO
-----------------------------------


-- Limpa o historico de logs (tabela b_eLog)
---	Nota: elimina historico > 150 dias (~5meses)
-- exec up_historico_limparLog

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_historico_limparLog]') IS NOT NULL
	drop procedure dbo.up_historico_limparLog
go

create procedure dbo.up_historico_limparLog

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

if exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME='b_elog')
begin

	delete
		B_eLog
	where
		data < GETDATE()-150
		or status='F'
		
end

GO
Grant Execute On dbo.up_historico_limparLog to Public
Grant Control On dbo.up_historico_limparLog to Public
GO
-----------------------------------