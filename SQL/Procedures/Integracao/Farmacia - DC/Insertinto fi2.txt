

DISABLE TRIGGER ALL ON fi2;

go

INSERT INTO [dbo].[fi2]



SELECT 	
	   ut1.[fistamp]
      ,ut1.[ftstamp]
      ,ut1.[u_comb_emb_matrix]
      ,ut1.[valcativa]
      ,ut1.[justificacao_tecnica_cod]
      ,ut1.[justificacao_tecnica_descr]
  FROM [172.20.120.6\SQLEXPRESS].mecofarma.dbo.fi2 ut1(nolock)
	inner join fi on fi.fistamp = ut1.fistamp
	LEFT JOIN  fi2(nolock) ON ut1.fistamp=fi2.fistamp
WHERE  fi2.fistamp IS NULL and ousrdata < '20191216'
order by ousrdata desc

go



enable TRIGGER ALL ON fi2 
go