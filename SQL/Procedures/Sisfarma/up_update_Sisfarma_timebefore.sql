
/****** Object:  StoredProcedure [dbo].[up_update_Sisfarma_timebefore]  
use F07096A
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma','minute','15','b_fidel','b_fidel','Loja 1'

Script Date: 24/04/2024 10:29:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[up_update_Sisfarma_timebefore]

@ipdestino varchar (100),
@bddestino varchar (100),
@unitoftime varchar (15),
@timebefore varchar(3),
@tabelaorigem VARCHAR (100), 
@tabeladestino VARCHAR (100),
@site varchar (100)
/* with encryption */
AS
SET NOCOUNT OFF
/* Elimina Tabelas */

DECLARE @sql varchar (MAX) = ''
DECLARE @destino varchar (200) = '['+@ipdestino+'].['+@bddestino+']'
DECLARE @insertSql varchar(max) = ''
DECLARE @no varchar (3)= (select no from empresa (nolock) where site =@site)
DECLARE @columns varchar (max)
DECLARE @sql2 varchar (max)

--Find Primary keys for table

CREATE TABLE #PKey (
	COLUMNNAME varchar (50)
	)

DECLARE @sql1 varchar (max) =
				'INSERT INTO #PKey 
				SELECT COL_NAME(IC.OBJECT_ID,IC.COLUMN_ID) 
					FROM SYS.INDEXES  X 
					INNER JOIN SYS.INDEX_COLUMNS  IC 
			        ON X.OBJECT_ID = IC.OBJECT_ID
						AND X.INDEX_ID = IC.INDEX_ID
					WHERE (X.IS_PRIMARY_KEY = 1 '+
							CASE WHEN @tabelaorigem != 'st'
								 THEN ' or X.is_unique =1)'
								 ELSE ')'
								 END +
					'AND OBJECT_NAME(IC.OBJECT_ID)='''+@tabelaorigem+''''
exec (@sql1)
DECLARE @PKey VARCHAR (max) = ''
select @PKey = @PKey + ' a.' + COLUMNNAME + ' = b.' + COLUMNNAME +' AND ' from #PKey

if LEN (@PKey) > 4
BEGIN 
set @PKey = LEFT(@PKey,LEN(@PKey) -4)
END

drop table #PKey

--create condition for 'where (primary key) is null'
SELECT COL_NAME(IC.OBJECT_ID,IC.COLUMN_ID) AS COLUMNNAME into #PKeyisnull
	  FROM SYS.INDEXES  X 
INNER JOIN SYS.INDEX_COLUMNS  IC 
        ON X.OBJECT_ID = IC.OBJECT_ID
       AND X.INDEX_ID = IC.INDEX_ID
	 WHERE (X.IS_PRIMARY_KEY = 1
		or X.is_unique =1)
		AND OBJECT_NAME(IC.OBJECT_ID)=@tabelaorigem

DECLARE @PKeyisnull VARCHAR (max) = ''

select @PKeyisnull = @PKeyisnull +'b.' + COLUMNNAME + ' IS NULL AND ' from #PKeyisnull
--PRINT @PKeyisnull
if LEN (@PKeyisnull) > 4
BEGIN 
set @PKeyisnull = LEFT(@PKeyisnull,LEN(@PKeyisnull) -4) -- remover o 'AND ' do ' IS NULL AND '
END

drop table #PKeyisnull

SELECT   c.COLUMN_NAME into #columnTemp
FROM     INFORMATION_SCHEMA.COLUMNS c
WHERE	 TABLE_NAME  = ''+@tabelaorigem+''
 
SET @sql2 = CASE	WHEN 'site' in (select * from #columnTemp) 
						THEN 'site'
					WHEN 'site_nr' in (select * from #columnTemp) 
						THEN 'site_nr'
					WHEN 'armazem' in (select * from #columnTemp)  
						THEN 'armazem'
					WHEN 'site' not in (select * from #columnTemp) 
							AND 'site_nr' not in (select * from #columnTemp)
							AND 'armazem' not in (select * from #columnTemp)
						THEN ''+@PKey+''
			END

EXEC  up_retorna_insert_sql_dinamico_sisfarma @tabelaorigem,@tabeladestino,@ipdestino,@bddestino, @insertSql OUTPUT
--print @insertsql
--preparar os DELETES 

--deletes para casos em que os ftstamp e bostamp estão diferentes e o innerjoin não os apanha nos deletes abaixo
IF @tabelaorigem = 'ft' 
	BEGIN	
	DELETE FROM [POWERBI].[F07096A_sisfarma].dbo.ft WHERE ftstamp IN (
		SELECT a.ftstamp FROM [POWERBI].[F07096A_sisfarma].dbo.ft a
		INNER JOIN ft b 
			ON a.fno = b.fno AND a.ftano = b.ftano AND a.ndoc = b.ndoc
		WHERE a.ftstamp != b.ftstamp
		)
	END
IF @tabelaorigem = 'bo' --a fi2 não tem usrhora mas é preciso fazer o delete via hora, tem de ligar a fi. Nos outros casos vê se existe usrhora
	BEGIN
	DELETE FROM [POWERBI].[F07096A_sisfarma].dbo.bo WHERE bostamp IN (
		SELECT a.bostamp FROM [POWERBI].[F07096A_sisfarma].dbo.bo a
		INNER JOIN bo b 
			ON a.obrano = b.obrano AND a.boano = b.boano AND a.ndos = b.ndos
		WHERE a.bostamp != b.bostamp
		)
	END
				
--Deletes com base na PK ou outra

IF @tabelaorigem = 'fi2' --a fi2 não tem usrhora mas é preciso fazer o delete via hora, tem de ligar a fi. Nos outros casos vê se existe usrhora
	BEGIN
		set @sql = @sql +'DELETE a from '+@destino+'.dbo.'+@tabeladestino+' a inner join fi with (nolock) on a.fistamp COLLATE DATABASE_DEFAULT = fi.fistamp COLLATE DATABASE_DEFAULT '
		
		set @sql = @sql +' where CONVERT(datetime,CONVERT(date,usrdata))+ fi.usrhora
										between DATEADD('+@unitoftime+', -'+@timebefore+' , getdate()) 
											and GETDATE() '
	END

IF @tabelaorigem = 'B_fidel' --a b_fidel não tem primary key, vai ligar hard coded 
	BEGIN
		set @sql = @sql +'DELETE a from '+@destino+'.dbo.b_fidel a inner join b_fidel with (nolock) on a.fstamp COLLATE DATABASE_DEFAULT = b_fidel.fstamp COLLATE DATABASE_DEFAULT '
	END

IF @tabelaorigem = 'fo' --a fo tem adoc como primary key, mas é editavel logo parte os joins porque o user edita o adoc. vai fazer delete apenas por data
	BEGIN
		set @sql = @sql +'DELETE a from '+@destino+'.dbo.fo a '

		set @sql = @sql +' where CONVERT(datetime,CONVERT(date,a.ousrdata))+ a.ousrhora
										between DATEADD('+@unitoftime+', -('+@timebefore+'+180) , getdate()) 
											and GETDATE() '
	END

IF @tabelaorigem = 'ft2' --a ft2 tem usrhoras como '.F.' por isso as vezes parte, para isso faz-se o case abaixo 
	BEGIN
		set @sql ='DELETE b from '+@destino+'.dbo.'+@tabeladestino+' b'
	
		set @sql = @sql +' inner join '+@tabelaorigem+' a with (nolock)
									on '+@PKey+'
									where CONVERT(datetime,CONVERT(date,a.usrdata))+ (CASE WHEN a.usrhora = ''.F.''
																						THEN a.ousrhora
																						ELSE a.usrhora
																						END) 
											between DATEADD('+@unitoftime+', -'+@timebefore+' , getdate()) 
												and GETDATE() '
	END

IF @tabelaorigem not in ('fi2','b_fidel','ft2','fo')
	BEGIN
		set @sql ='DELETE b from '+@destino+'.dbo.'+@tabeladestino+' b'
	
		IF 'usrdata' in (
			SELECT   c.COLUMN_NAME 
			FROM     INFORMATION_SCHEMA.COLUMNS c
			WHERE	 TABLE_NAME  = ''+@tabelaorigem+'')
			
			set @sql = @sql +' inner join '+@tabelaorigem+' a with (nolock)
									on '+@PKey+'
									where CONVERT(datetime,CONVERT(date,a.usrdata))+ a.usrhora
											between DATEADD('+@unitoftime+', -'+@timebefore+' , getdate()) 
												and GETDATE() '
	END	
--define se usa site, site_nr ou nada
BEGIN
	IF @sql2 = 'site'
	set @sql = @sql + ' AND a.'+ @sql2 +' = '''+@site+''''
	IF @sql2 = 'site_nr'
	set @sql = @sql + ' AND a.'+ @sql2 +' = '+@no
	IF @sql2 = 'armazem'
	set @sql = @sql + ' AND a.'+ @sql2 +' in (select armazem from empresa_arm where empresa_no = '+@no+') '
END

--junção delete + insert 
set @sql = @sql + CHAR(13) + CHAR(10) + @insertSql 

Select @sql = @sql + 
	CASE 
		WHEN @tabeladestino = 'B_fidel' --inner join hard coded da b_fidel
		THEN 	'left join [POWERBI].F07096A_Sisfarma.dbo.'+@tabeladestino+' b with (nolock)  
									on a.fstamp = b.fstamp
								where b.fstamp is null '


		ELSE
				'left join [POWERBI].F07096A_Sisfarma.dbo.'+@tabeladestino+' b with (nolock)  
									on '+@PKey+'
								where '+ @PKeyisnull
	END

BEGIN
IF @tabelaorigem = 'bi'
set @sql = @sql + ' AND a.nmdos != ''Bonus Fornecedor'''
END

BEGIN
	IF @sql2 = 'site'
	set @sql = @sql + ' AND a.'+ @sql2 +' = '''+@site+''''
	IF @sql2 = 'site_nr'
	set @sql = @sql + ' AND a.'+ @sql2 +' = '+@no
	IF @sql2 = 'armazem'
	set @sql = @sql + ' AND a.'+ @sql2 +' in (select armazem from empresa_arm where empresa_no = '+@no+') '
END

PRINT @sql
EXECUTE (@sql)

GO
GRANT EXECUTE on dbo.up_update_Sisfarma_timebefore TO PUBLIC
GRANT Control on dbo.up_update_Sisfarma_timebefore TO PUBLIC
GO


