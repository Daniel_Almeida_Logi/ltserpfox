
/****** Object:  StoredProcedure [dbo].[up_Sync_Sisfarma]    Script Date: 28//12/2022 15:57:47 
SP para chamar todas as SPs de sincronização do SISFARMA
USE F07096A
DECLARE @ip_errortable	varchar (100) = 'SQLLOGITOOLS'
DECLARE @bd_errortable	varchar (100) = 'LTSMSB'
DECLARE @ipdestino	varchar (100) = 'POWERBI'
DECLARE @bddestino	varchar (100) = 'F07096A_Sisfarma'
DECLARE @site		varchar (55)  = 'Loja 1'
DECLARE @unitoftime	varchar (10)  = 'day'
DECLARE @timebefore	varchar (10)  = '35'


exec up_Sync_SISFARMA	 @ip_errortable
			,@bd_errortable
			,@ipdestino 
			,@bddestino 	
			,@site	
			,@unitoftime
			,@timebefore

		select * from [SQLLOGITOOLS].[LTSMSB].[dbo].[Errors_SISFARMA] order by id desc

******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'up_Sync_SISFARMA'))
DROP procedure [dbo].[up_Sync_SISFARMA]
GO
CREATE procedure [dbo].[up_Sync_SISFARMA]

	@ip_errortable	varchar (100)
	,@bd_errortable varchar (100)
	,@ipdestino		varchar (100)
	,@bddestino		varchar (100)
	,@site			varchar(55)
	,@unitoftime	varchar(10)
	,@timebefore	varchar(10)

/* with encryption */
AS
SET NOCOUNT OFF

BEGIN TRY  

IF @unitoftime = 'minute'--para correr de 30 em 30 minutos
BEGIN
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'B_fidel','B_fidel',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'B_labs','B_labs',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'b_utentes','b_utentes',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'bc','bc',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'st','st',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'ft','ft',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'ft2','ft2',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'fi','fi',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'fi2','fi2',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'fl','fl',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'fo','fo',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'fn','fn',@site


END

IF @unitoftime = 'day'--para correr 1x por dia
BEGIN
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'B_atc','B_atc',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'B_atcfp','B_atcfp',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'b_us','b_us',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'bo','bo',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'bi','bi',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'st','st',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'categoria_hmr','categoria_hmr',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'grande_mercado_hmr','grande_mercado_hmr',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'mercado_hmr','mercado_hmr',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'segmento_hmr','segmento_hmr',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'fprod','fprod',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'taxasiva','taxasiva',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'B_fidel','B_fidel',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'B_labs','B_labs',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'b_utentes','b_utentes',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'bc','bc',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'ft','ft',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'ft2','ft2',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'fi','fi',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'fi2','fi2',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'fl','fl',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'fo','fo',@site
exec up_update_Sisfarma_timebefore 'POWERBI','F07096A_Sisfarma',@unitoftime,@timebefore,'fn','fn',@site

END
END TRY  

BEGIN CATCH 
	DECLARE @sql varchar (max) 
	set @sql=	'INSERT INTO ['+@ip_errortable+'].['+@bd_errortable+'].[dbo].[Errors_SISFARMA]
	           ([bd_destino]
	           ,[site]
	           ,[datainicio]
	           ,[datafim]
	           ,[data_erro]
	           ,[msg_erro])
			 SELECT '+
	           ''''+@bddestino+''','''
	           +@site+''',
	           CONVERT(varchar,DATEADD('+@unitoftime+', '+@timebefore+' , getdate()),120),
	           CONVERT(varchar,GETDATE(),120),
	           GETDATE(),
	           ERROR_MESSAGE ()'
	print @sql
	exec (@sql)
END CATCH

GO
GRANT EXECUTE on dbo.up_Sync_SISFARMA TO PUBLIC
GRANT Control on dbo.up_Sync_SISFARMA TO PUBLIC
GO



