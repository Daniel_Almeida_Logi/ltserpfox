-- Create a new Column (unpersisted):
ALTER TABLE b_labs
   ADD tlmvl_seq2 varchar(30)
GO

UPDATE b_labs
SET tlmvl_seq2 = tlmvl_seq
GO

-- Delete the persisted column
ALTER TABLE b_labs
   DROP COLUMN tlmvl_seq
GO

-- Rename new column to old name
EXEC sp_rename 'b_labs.tlmvl_seq2', 'tlmvl_seq', 'COLUMN'
GO