
/****** Object:  StoredProcedure [dbo].[up_retorna_insert_sql_dinamico_sisfarma]  


exec up_retorna_insert_sql_dinamico_sisfarma 'fi','fi','POWERBI','F07096A_Sisfarma',''


Script Date: 17/04/2024 12:19:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_retorna_insert_sql_dinamico_sisfarma]') IS NOT NULL
	drop procedure dbo.[up_retorna_insert_sql_dinamico_sisfarma]
go

CREATE procedure [dbo].[up_retorna_insert_sql_dinamico_sisfarma] (
	@tabelaorigem varchar (100),
	@tabeladestino varchar(100), 
	@ipdestino varchar (100), 
	@bddestino varchar (100),
	@insertSql  varchar(max) out
) 
AS
BEGIN		
	DECLARE @sqlheaders VARCHAR (max) = ''
	DECLARE @sqllines VARCHAR (max) = ''
	DECLARE @sqlSchema VARCHAR (max) = ''
	DECLARE @destino varchar (200) = '['+@ipdestino+'].['+@bddestino+']'
	if OBJECT_ID('tempdb.dbo.#columnTemp1') is not null
		drop table #columnTemp1
	create table #columnTemp1 (
		COLUMN_NAME varchar(200)
		,DATA_TYPE varchar(200)
		,CHARACTER_MAXIMUM_LENGTH int
		,TABLE_NAME varchar(200)
	)
	set @sqlSchema = '
	insert into #columnTemp1
	SELECT   c.COLUMN_NAME, c.DATA_TYPE, c.CHARACTER_MAXIMUM_LENGTH, c.TABLE_NAME
	FROM     ' + @destino + '.INFORMATION_SCHEMA.COLUMNS c 
	WHERE COLUMNPROPERTY(object_id(TABLE_SCHEMA+''.''+TABLE_NAME), COLUMN_NAME, ''IsIdentity'') = 0 AND	
	TABLE_NAME  = ''' + @tabeladestino + ''''
	EXEC (@sqlSchema)
	SET @sqlheaders = @sqlheaders + ' insert into '+@destino+'.dbo.'+@tabeladestino+ ' ('
	SELECT   @sqlheaders = @sqlheaders + LTRIM(RTRIM(c.COLUMN_NAME)) + ','
	FROM     #columnTemp1 c 
	WHERE TABLE_NAME   = @tabeladestino
	SELECT @sqlheaders = substring(@sqlheaders, 1, (len(@sqlheaders) - 1)) + ') '
	SELECT @sqllines = @sqllines + ' select ' 
	SELECT @sqllines = @sqllines + CASE WHEN DATA_TYPE not in ('CHAR', 'VARCHAR','NVARCHAR') 
						THEN 'a.'+COLUMN_NAME
						ELSE CASE WHEN CHARACTER_MAXIMUM_LENGTH = -1 
							THEN 'LEFT(LTRIM(RTRIM(a.'+COLUMN_NAME+')),'+LTRIM(RTRIM(STR(4000)))+')' 
							ELSE 'LEFT(LTRIM(RTRIM(a.'+COLUMN_NAME+')),'+LTRIM(RTRIM(STR(CHARACTER_MAXIMUM_LENGTH)))+')'
							END
						END + ','
	FROM #columnTemp1
	WHERE TABLE_NAME   = @tabeladestino 
	SELECT @sqllines = substring(@sqllines, 1, (len(@sqllines) - 1))
	SELECT @sqllines = @sqllines + ' from '+ @tabelaorigem +' a with (nolock) '
	SET @insertSql = @sqlheaders +CHAR(13)+CHAR(10)+ @sqllines
		if OBJECT_ID('tempdb.dbo.#columnTemp') is not null
		drop table #columnTemp

--PRINT @insertsql
END
GO
Grant Execute on dbo.up_retorna_insert_sql_dinamico_sisfarma to Public
grant control on dbo.up_retorna_insert_sql_dinamico_sisfarma to public
GO
