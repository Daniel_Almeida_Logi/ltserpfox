/*	Cria cabe�alho de vendas

	exec up_sales_createHeader 'stamp14', 0, 200, 0, 3, '2014-05-16', '14:44:10', 1, 0, 10, 2, 2, 0.2								
								,0, 2, 0, 0, 0, 0, 0, 0, 0
								,0, 0.5, 0, 0 ,0, 0, 0, 0, 0
								,'Loja 1', 15, 'cxstamp', 'sstamp', 'obs', '123'
								
	Notas:
		. campo FREF utilizado para guardar refer�ncia da venda (quando tem de mapear com outro sistema)
		
	select * from ft2 where ft2stamp='stamp'
	delete ft where ftstamp='stamp'
	delete ft2 where ft2stamp='stamp'
	select * from ft where ftstamp='stamp'
	update ft set vendedor=9999 where ftstamp='stamp'
*/




SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sales_createHeader]') IS NOT NULL
	drop procedure dbo.up_sales_createHeader
go


create procedure dbo.up_sales_createHeader
	@stamp char(25)
	,@fno int
	,@no numeric(10)
	,@estab numeric(3)
	,@ndoc numeric(9)
	,@data datetime
	,@hora varchar(8)
	,@vendedor numeric(4)
	,@suspensa bit
	,@total numeric(19,6)
	,@totalQtt numeric(15,3)
	,@totalCusto numeric(19,6)
	,@totalDesconto numeric(19,6)
	,@totalBase1 numeric(19,6)
	,@totalBase2 numeric(19,6)
	,@totalBase3 numeric(19,6)
	,@totalBase4 numeric(19,6)
	,@totalBase5 numeric(19,6)
	,@totalBase6 numeric(19,6)
	,@totalBase7 numeric(19,6)
	,@totalBase8 numeric(19,6)
	,@totalBase9 numeric(19,6)
	,@totalIva1 numeric(19,6)
	,@totalIva2 numeric(19,6)
	,@totalIva3 numeric(19,6)
	,@totalIva4 numeric(19,6)
	,@totalIva5 numeric(19,6)
	,@totalIva6 numeric(19,6)
	,@totalIva7 numeric(19,6)
	,@totalIva8 numeric(19,6)
	,@totalIva9 numeric(19,6)
	,@site varchar(20)
	,@terminal numeric(3)
	,@cxstamp char(25)
	,@ssstamp char(25)
	,@obs varchar(254)
	,@saleRef varchar(20)
	,@nome varchar(55) = ''
	,@morada varchar(55) = ''
	,@telefone varchar(13) = ''
	,@ncont varchar(20) = ''


/* WITH ENCRYPTION */
AS

	/*
		Guardar n�mero de venda, devolver como output de update
		caso o n�mero de venda entre por parametro n�o calcula o nr de venda
	*/
	declare @fno2 int
	/*
		Tabelas de Iva
	*/
	declare @ivatx1 numeric(5,2), @ivatx2 numeric(5,2), @ivatx3 numeric(5,2), @ivatx4 numeric(5,2)
		,@ivatx5 numeric(5,2), @ivatx6 numeric(5,2), @ivatx7 numeric(5,2), @ivatx8 numeric(5,2), @ivatx9 numeric(5,2)
	
	select
		 @ivatx1 = case when taxasiva.codigo=1 then taxasiva.taxa else @ivatx1 end
		,@ivatx2 = case when taxasiva.codigo=2 then taxasiva.taxa else @ivatx2 end
		,@ivatx3 = case when taxasiva.codigo=3 then taxasiva.taxa else @ivatx3 end
		,@ivatx4 = case when taxasiva.codigo=4 then taxasiva.taxa else @ivatx4 end
		,@ivatx5 = case when taxasiva.codigo=5 then taxasiva.taxa else @ivatx5 end
		,@ivatx6 = case when taxasiva.codigo=6 then taxasiva.taxa else @ivatx6 end
		,@ivatx7 = case when taxasiva.codigo=7 then taxasiva.taxa else @ivatx7 end
		,@ivatx8 = case when taxasiva.codigo=8 then taxasiva.taxa else @ivatx8 end
		,@ivatx9 = case when taxasiva.codigo=9 then taxasiva.taxa else @ivatx9 end
	from
		taxasiva (nolock)

	 if(lower(rtrim(ltrim(isnull(@ncont,'')))) = 'null')
	 begin
		set @ncont = ''
	 end
	 

	
	/*
		Dados da caixa	e sess�o de utilizador
	*/
	declare @cxusername varchar(30), @ssusername varchar(30)
	
	set @cxusername = isnull((select ausername from cx (nolock) where cxstamp = @cxstamp),'')
	set @ssusername = isnull((select ausername from ss (nolock) where ssstamp = @ssstamp),'')

	/*
		N�mero de Atendimento
	*/
	declare @atendimento varchar(20)

	select @atendimento = Right(Year(getDate()),2) 
							+ right('000' + ltrim(str(datepart(dayofyear, getdate()))),3)
							+ replace(CONVERT(VARCHAR(8),GETDATE(),108),':','')
							+ left(newid(),3)

	--select @atendimento = isnull((select TOP 1
	--								isnull(case 
	--									when stamp='' or year(getdate()) != year(b_ultreg.data)
	--									then '0000000000' 
	--									else stamp 
	--									end
	--									,'0000000000')
	--								FROM
	--									b_ultreg (nolock)
	--								WHERE
	--									tabela = convert(varchar,@terminal)
	--									AND site = @site)
	--						,0)
							
	--select @atendimento = 
	--	case 
	--		when @atendimento = '99999999' 
	--		then '00000001' + convert(varchar,@terminal) 
	--		else right(replicate('0', 10) + ltrim(rtrim(
	--				convert(varchar,
	--					convert(numeric,
	--						@atendimento
	--					) + 1
	--				) + convert(varchar,@terminal)
	--			)),10)
	--		end


	/*
		Criar Cabe�alho
	*/
	/* Calcula n. de venda */
	IF @fno=0
	BEGIN
		DECLARE @tabNewDocNr TABLE (newDocNr NUMERIC(10,0))
		INSERT INTO @tabNewDocNr (newDocNr)
		EXEC up_get_newDocNr 'FT', @ndoc, @site, 1
		SET  @fno2 = (SELECT newDocNr FROM @tabNewDocNr)
	END

	/*
		Criar cabe�alho 2
	*/
	Insert Into ft2 (
		contacto, u_viatura, morada
		,codpost, local, telefone
		,email, horaentrega, ft2stamp
		,u_vddom, obsdoc, u_receita
		,u_entpresc, u_nopresc, u_codigo
		,u_codigo2, u_nbenef, u_nbenef2 
		,u_abrev, u_abrev2, c2no
		,c2estab, subproc, u_vdsusprg
		,u_design, u_design2
		,c2nome
		,c2pais, evdinheiro, epaga1
		,epaga2, etroco
		,u_acertovs, c2codpost
		,eacerto, vdollocal, vdlocal
		,modop1, modop2, ousrdata
		,ousrinis, ousrhora, usrdata
		,usrinis, usrhora
	)
	/* Validar bem estes dados se utilizar esta sp no software */
	select
		'', '', ''
		,'', '', ''
		,cl.email, '', @stamp
		,0 /* venda ao domicilio */ , @obs, '' /* n�mero da receita */
		,'', '', ''
		,'', '', ''
		,'', '', 0
		,0, '', 0
		,'', ''
		,''
		,0, 0, 0
		,0, 0
		,0, ''
		,0, 'Caixa', 'C'
		,'', '', getdate()
		,b_us.iniciais, @hora, getdate()
		,b_us.iniciais, @hora
	from
		b_utentes cl (nolock)
		,b_us (nolock)
	where 
		cl.no = @no and cl.estab = @estab
		and b_us.userno = @vendedor

	/*
		Criar cabe�alho 1
	*/
	insert into ft
		( ftstamp
		,pais, nmdoc, fno, [no], nome, morada, [local] ,codpost
		,ncont, bino, bidata, bilocal, telefone
		,zona, vendedor, vendnm, fdata, ftano ,pdata
		,carga, descar, saida
		,ivatx1, ivatx2, ivatx3
		,fin, ndoc, moeda ,fref
		
		,ccusto, ncusto, facturada, fnoft, nmdocft
		,estab, cdata, ivatx4, segmento ,totqtt
		,qtt1, qtt2, tipo ,cobrado, cobranca
		,tipodoc ,chora, ivatx5, ivatx6, ivatx7
		,ivatx8, ivatx9 ,cambiofixo, memissao, cobrador
		,rota, multi ,cheque, clbanco, clcheque

		,chtotal, echtotal ,custo, eivain1, eivain2
		,eivain3, eivav1 ,eivav2 ,eivav3 ,ettiliq
		,edescc, ettiva ,etotal, eivain4, eivav4
		,efinv, ecusto, eivain5, eivav5, edebreg
		,eivain6 ,eivav6, eivain7 ,eivav7 ,eivain8
		,eivav8, eivain9, eivav9 ,total, totalmoeda

		,ivain1, ivain2 ,ivain3, ivain4, ivain5
		,ivain6, ivain7, ivain8, ivain9, ivav1
		,ivav2 ,ivav3 ,ivav4, ivav5, ivav6
		,ivav7 ,ivav8, ivav9, ttiliq, ttiva
		,descc, debreg, debregm, intid ,nome2
		,tpstamp, tpdesc ,erdtotal, rdtotal, rdtotalm

		,cambio, [site], pnome, pno, cxstamp
		,cxusername, ssstamp, ssusername, anulado ,virs
		,evirs, valorm2 ,u_lote, u_tlote, u_tlote2
		,u_ltstamp ,u_slote, u_slote2, u_nslote, u_nslote2
		,u_ltstamp2, u_lote2, ousrinis, ousrdata, ousrhora
		,usrinis, usrdata ,usrhora, u_nratend,u_hclstamp ,datatransporte, localcarga )

	select top 1
		rtrim(ltrim(@stamp))
		,cl.pais
		,td.nmdoc
		,case when @fno=0 then @fno2 else @fno end
		,@no	
		,case when @nome!='' then @nome else cl.nome end
		,case when @morada!='' then @morada else cl.morada end
		,cl.local
		,cl.codpost
		,case when @ncont!='' then @ncont else cl.ncont end
		,cl.bino
		,'19000101'
		,''
		,cl.telefone
		,cl.zona
		,@vendedor
		,left(b_us.nome,20), @data, YEAR(@data)
		,isnull(dateadd(day,case when tp.vencimento = 1 then isnull(cl.vencimento,0) else dias end,@data),@data)
		,'', '', LEFT(@hora,5)
		,@ivatx1, @ivatx2, @ivatx3, 0 /*Melhorar, desconto financeiro*/, @ndoc, cl.moeda
		,@saleRef, cl.ccusto, '', 0, 0, ''
		,@estab, @data, @ivatx4, ''
		,@totalQtt, @totalQtt, 0, cl.tipo
		,@suspensa, '', td.tipodoc
		,left(replace(@hora,':',''),4), @ivatx5, @ivatx6, @ivatx7, @ivatx8, @ivatx9
		,0, LEFT(cl.moeda,4), '', '', 0
		,0, '', '', 0, 0
		,@totalCusto * 200.482, @totalBase1, @totalBase2, @totalBase3
		,@totalIva1, @totalIva2, @totalIva3
		,(@totalBase1 + @totalBase2 + @totalBase3 + @totalBase4 + @totalBase5 + @totalBase6 + @totalBase7 + @totalBase8 + @totalBase9)
		,@totalDesconto
		,(@totalIva1+@totalIva2+@totalIva3+@totalIva4+@totalIva5+@totalIva6+@totalIva7+@totalIva8+@totalIva9)
		,cast(@total as numeric(10,2))
		,@totalBase4, @totalIva4
		,0, @totalCusto, @totalBase5, @totalIva5, 0, @totalBase6
		,@totalIva6, @totalBase7, @totalIva7, @totalBase8, @totalIva8, @totalBase9, @totalIva9
		,@total * 200.482, 0, @totalBase1 * 200.482, @totalBase2 * 200.482
		,@totalBase3 * 200.482, @totalBase4 * 200.482, @totalBase5 * 200.482
		,@totalBase6 * 200.482, @totalBase7 * 200.482, @totalBase8 * 200.482, @totalBase9 * 200.482
		,@totalIva1 * 200.482, @totalIva2 * 200.482, @totalIva3 * 200.482
		,@totalIva4 * 200.482, @totalIva5 * 200.482, @totalIva6 * 200.482, @totalIva7 * 200.482
		,@totalIva8 * 200.482, @totalIva9 * 200.482
		,(@totalBase1 + @totalBase2 + @totalBase3 + @totalBase4 + @totalBase5 + @totalBase6 + @totalBase7 + @totalBase8 + @totalBase9) * 200.482
		,(@totalIva1+@totalIva2+@totalIva3+@totalIva4+@totalIva5+@totalIva6+@totalIva7+@totalIva8+@totalIva9) * 200.482
		,@totalDesconto * 200.482, 0, 0, 0
		,'', isnull(tp.tpstamp,''), isnull(tp.descricao,'')
		,0, 0, 0
		,case when @ndoc=1 then 1 else 0 end /* validar melhor este campo */, @site, B_Terminal.terminal, @terminal, @cxstamp
		,isnull(@cxusername,''), @ssstamp, isnull(@ssusername,''), 0 
		,0, 0, 0 /* ver melhor este valor, quando reg. a cliente */
		,0, '', '', '' /* validar melhor estes dados de receituario */
		,0, 0, 0, 0, '', 0 /* validar melhor estes dados de receituario */
		,b_us.iniciais, GETDATE(), @hora, b_us.iniciais, getdate()
		,@hora, @atendimento
		,'' /* ver melhor este campo, entidade financeira diferente do utente */
		,'19000101', ''
	from
		b_utentes cl (nolock)
		left join tp (nolock) on tp.descricao = cl.tpdesc
		,b_us (nolock)
		,td (nolock)
		,B_Terminal (nolock)
	where
		cl.no = @no and cl.estab = @estab
		and b_us.userno = @vendedor
		and td.ndoc = @ndoc
		and B_Terminal.no = @terminal

	
go
Grant Execute On dbo.up_sales_createHeader to Public
Grant control On dbo.up_sales_createHeader to Public
go 
