/*	Devolve o c�digo de uma taxa

	exec up_sales_getTaxCode 6
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sales_getTaxCode]') IS NOT NULL
	drop procedure dbo.up_sales_getTaxCode
go

create procedure dbo.up_sales_getTaxCode
	@taxa numeric(5,2)

/* WITH ENCRYPTION */
as

	select top 1
		codigo
	from
		taxasiva (nolock)
	where
		taxa = @taxa
	order by
		codigo

GO
Grant Execute On dbo.up_sales_getTaxCode to Public
Grant control On dbo.up_sales_getTaxCode to Public
GO
