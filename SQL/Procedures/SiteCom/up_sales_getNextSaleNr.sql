/*	Get next sale number
	
	exec up_sales_getNextSaleNr '2024', 905

	up_sales_getNextSaleNr '2024-05-01', 905

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sales_getNextSaleNr]') IS NOT NULL
	drop procedure dbo.up_sales_getNextSaleNr
go

create procedure dbo.up_sales_getNextSaleNr
	@data datetime
	,@ndoc numeric(6)

/* WITH ENCRYPTION */
as
	SET NOCOUNT ON
	DECLARE @tabNewDocNr TABLE (newDocNr NUMERIC(10,0))
	declare @site    varchar(18) = ''
	declare @tipoDoc int = 0
	declare @fno int = 0
	select top 1 @site = site, @tipoDoc = td.codigoDoc from td(nolock) where ndoc = @ndoc and inativo = 0
	
	--vendas criadas online
	if(@tipoDoc!=5)
	begin
	
		INSERT INTO @tabNewDocNr (newDocNr)
		EXEC up_get_newDocNr 'FT', @ndoc, @site, 1
		SET  @fno = (SELECT newDocNr FROM @tabNewDocNr)
	end else begin
		
		INSERT INTO @tabNewDocNr (newDocNr)
		EXEC up_get_newDocNrPorData 'FT', @ndoc, @site, 1
		SET  @fno = (SELECT newDocNr FROM @tabNewDocNr)

	end
	 select
		fno = @fno

GO
Grant Execute On dbo.up_sales_getNextSaleNr to Public
Grant control On dbo.up_sales_getNextSaleNr to Public
GO