/*	Cria detalhe de vendas

	exec up_sales_createDetail 'ADM2EC6451A-4571-4569-8E2', 'stampfi', '', 1, 2
							,23, 1, 1, 100, 5, 0, 0, 0

							select * from fi
							where fistamp = 'stampfi'
							delete from fi where fistamp = 'stampfi'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_sales_createDetail]') IS NOT NULL
	drop procedure dbo.up_sales_createDetail
go

create procedure dbo.up_sales_createDetail
	@ftstamp char(25)
	,@fistamp char(25)
	,@ref varchar(18)
	,@design varchar(100) 
	,@qt numeric(11,3)
	,@total numeric(19,6)
	,@iva numeric(5,2)
	,@ivaIncluido bit
	,@armazem numeric(5)
	,@ordem numeric(10)
	,@pvp numeric(19,6)
	,@descontoP numeric(6,2)
	,@descontoV numeric(9,2)

/* WITH ENCRYPTION */
AS


	declare @site_nr int
	declare @armazemDefault int = 0

	SELECT @armazemDefault = numValue  FROM B_Parameters(nolock) WHERE stamp='ADM0000000367'

	set @armazemDefault = isnull(@armazemDefault,0)

	if(isnull(@armazemDefault,0)>0)
		select @site_nr= isnull(empresa_no,@armazem) from empresa_arm(nolock) where armazem=@armazemDefault
	else
		select @site_nr= isnull(empresa_no,@armazem) from empresa_arm(nolock) where armazem=@armazem
		

	insert into fi (
		fistamp
		,nmdoc, fno, ref, design
		,qtt, tiliquido, etiliquido, unidade, unidad2, iva, ivaincl
		,tabiva, ndoc, armazem, fnoft, ndocft
		,ftanoft, ftregstamp, lobs2, litem2
		,litem, lobs3, rdata
		,cpoc, composto, lrecno, lordem
		,fmarcada, partes, altura, largura
		,oref, lote, usr1, usr2, usr3
		,usr4, usr5
		,ftstamp
		,cor, tam, stipo, fifref
		,tipodoc, familia, bistamp
		,stns, ficcusto, fincusto
		,ofistamp, pv, pvmoeda, epv, tmoeda
		,eaquisicao, custo, ecusto, slvu, eslvu
		,sltt,esltt,slvumoeda,slttmoeda
		,nccod,epromo
		,fivendedor,fivendnm
		,desconto,desc2,desc3,desc4,desc5,desc6, u_descval
		,iectin,eiectin
		,vbase,evbase
		,tliquido,num1
		,pcp,epcp
		,pvori,epvori, zona,morada,[local]
		,codpost,telefone,email
		,tkhposlstamp
		,u_generico, u_cnp
		,u_psicont, u_bencont, u_psico, u_benzo
		,u_ettent1, u_ettent2, u_epref, u_stock, u_epvp, u_ip
		,u_comp, u_diploma, ousrinis, ousrdata
		,ousrhora, usrinis, usrdata, usrhora
		,marcada, u_genalt, u_txcomp, amostra, u_refvale 
		,evaldesc, valdesc
		,u_codemb
		)
	select top 1
		ltrim(rtrim(@fistamp))
		,ft.nmdoc, ft.fno, @ref , case when @design = '' then st.design else @design end
		,@qt, @total*200.482, @total, '', '', @iva, @ivaIncluido
		,isnull(taxasiva.codigo,0), ft.ndoc, @armazem, 0, 0
		,ft.ftano, '', '', '' 
		,'', '', ft.fdata
		,st.cpoc, 0, @fistamp, @ordem
		,0, 0, 0, 0
		,'', '', '', '', ''
		,'', ''
		,ft.ftstamp
		,'', '', 1, ''
		,td.tipodoc, st.familia, ''
		,st.stns, cl.ccusto, ''
		,'', @pvp*200.482, @pvp, @pvp, 0
		,0, st.epcusto * 200.482, st.epcusto, @pvp / (@iva/100+1) * 200.482, @pvp / (@iva/100+1)
		,@pvp / (@iva/100+1) * 200.482 * @qt, @pvp / (@iva/100+1) * @qt, 0, 0
		,0, 0
		,ft.vendedor, ft.vendnm
		,@descontoP, 0, 0, 0, 0, 0, @descontoV
		,0, 0
		,0, 0
		,@pvp / (@iva/100+1) * @qt, 0
		,st.epcpond * 200.482, st.epcpond
		,@pvp * 200.482, @pvp, cl.zona, cl.morada, cl.local
		,cl.codpost, cl.telefone, cl.email
		,''
		,ISNULL(fprod.generico,0), ''
		,0, 0, ISNULL(fprod.psico,0), ISNULL(fprod.benzo,0)
		,0, 0, ISNULL(fprod.pref,0), st.stock, @pvp, 0
		,0, '', ft.ousrinis, ft.ousrdata
		,ft.ousrhora, ft.usrinis, ft.usrdata, ft.usrhora
		,0, 0, 100, 0, ''
		,0, 0
		,''
	from
		ft(nolock)
		,st (nolock)
		left join fprod (nolock) on fprod.cnp = st.ref
		left join taxasiva (nolock) on taxasiva.taxa=@iva
		,td (nolock)
		,b_utentes cl (nolock)
	where
		ft.ftstamp = @ftstamp
		and (st.ref = @ref or @ref = '')
		and td.ndoc = ft.ndoc
		and cl.no = ft.no and cl.estab = ft.estab
		and st.site_nr=@site_nr

	insert into fi2(
		fistamp
		,ftstamp
		,motiseimp
		,codmotiseimp
	)
	select top 1
		ltrim(rtrim(@fistamp))
		,ft.ftstamp
		,case when @iva=0 then RTRIM(LTRIM(st.motiseimp))    else '' end
		,case when @iva=0 then RTRIM(LTRIM(st.codmotiseimp)) else '' end
	from
		ft(nolock)
		,st (nolock)
		left join fprod (nolock) on fprod.cnp = st.ref
		left join taxasiva (nolock) on taxasiva.taxa=@iva
		,td (nolock)
		,b_utentes cl (nolock)
	where
		ft.ftstamp = @ftstamp
		and (st.ref = @ref or @ref = '')
		and td.ndoc = ft.ndoc
		and cl.no = ft.no and cl.estab = ft.estab
		and st.site_nr=@site_nr


GO
Grant Execute On dbo.up_sales_createDetail to Public
Grant control On dbo.up_sales_createDetail to Public
GO
