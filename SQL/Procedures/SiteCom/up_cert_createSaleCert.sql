/*
	Insert into tabela de certificação

	exec up_cert_createSaleCert '20130101', 3, 2
*/




SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cert_createSaleCert]') IS NOT NULL
	drop procedure dbo.up_cert_createSaleCert
go

create procedure dbo.up_cert_createSaleCert
	@stamp varchar(30)
	,@date varchar(10)
	,@time varchar(8)
	,@docType varchar(10)
	,@docNr varchar(40)
	,@total varchar(30)
	,@hash varchar(240)
	,@site varchar(20)
	,@version varchar(40)

/* WITH ENCRYPTION */
as

	insert into b_cert
		(stamp, date, systemDate, systemTime
        ,invoiceType, invoiceNo, total
        ,hash
        ,site, versaochave)
	values
		(@stamp, convert(varchar(10),@date), convert(varchar(10),@date), @time
        ,@docType, @docNr, @total
        ,@hash
        ,@site, @version)

GO
Grant Execute On dbo.up_cert_createSaleCert to Public
Grant control On dbo.up_cert_createSaleCert to Public
GO
