/*
	Devolve o c�digo de uma taxa

	exec up_prod_getField 'stns', '8168617'
*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prod_getField]') IS NOT NULL
	drop procedure dbo.up_prod_getField
go

create procedure dbo.up_prod_getField
	@field varchar(40)
	,@ref char(18)

/* WITH ENCRYPTION */
as

	declare @str varchar(max)
	
	set @str = N'
	select
		'+@field+'
	from 
		st (nolock) 
	where 
		ref = ''' + rtrim(@ref) + ''''
	
	exec(@str)

GO
Grant Execute On dbo.up_prod_getField to Public
Grant control On dbo.up_prod_getField to Public
GO
---------------------------------