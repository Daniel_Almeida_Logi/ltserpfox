/*
	Cria ficha de cliente
	Select * from empresa
	exec up_clients_create 'stamp', 0, 0, 'nome', '999999999', 'morada', 'codpost'
								,'local', 'zona', 'fax', 'telefone', 'tlmvl', 'Normal', 'email'
								,'111111111', '20131212', 'M', 'observa��es', 1, 'Loja 1', ''

	select nrcartao,* from b_utentes where utstamp='stamp'
	delete b_utentes where no=99991
	
	Notas:
		. Se chamada com o @no<=0 incrementa no n�mero de cliente (max(no)+1)
		. Cria cart�o de cliente se definidos nos parametros 'ADM0000000021'						
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_clients_create]') IS NOT NULL
	drop procedure dbo.up_clients_create
go

create procedure dbo.up_clients_create
	@stamp char(25)
	,@no numeric(10)
	,@estab numeric(3)
	,@nome varchar(55)
	,@ncont varchar(20)
	,@morada varchar(55)
	,@codpost varchar(45)
	,@local varchar(45)
	,@zona varchar(20)
	,@fax varchar(13)
	,@telefone varchar(13)
	,@tlmvl varchar(13)
	,@tipo varchar(20)
	,@email varchar(45)
	,@bino varchar(20)
	,@nascimento datetime
	,@sexo varchar(1)
	,@profi varchar(20)
	,@obs varchar(254)
	,@nbenef varchar(18)
	,@nbenef2 varchar(20)
	,@csaude varchar(254)
	,@drfamilia varchar(55)
	,@user numeric(6)
	,@loja varchar(20)
	,@ousrdata datetime
	,@usrdata datetime
	,@usrhora varchar(8) = '00:00:00'
	,@no_ext varchar(20) = ''
	,@idstamp char(25)
	,@codPla varchar(40)
	,@descrPla varchar(254)
	,@validadePla varchar(10)


/* WITH ENCRYPTION */
AS

	set nocount on

	/* Iniciais de Utilizador */
	declare @iniciais varchar(3);
	set @iniciais = isnull((select iniciais from B_us (nolock) where userno = @no),'ADM')

	/* Cart�o de Cliente */
	declare @criaCartaoCliente bit, @nrcartao varchar(7)
	set @criaCartaoCliente = isnull((select bool from b_parameters (nolock) where stamp='ADM0000000021'),0)
	if @criaCartaoCliente=1
	begin
		set @nrcartao = right((select ISNULL(max(nrcartao),'CL00000') as nrcartao from B_fidel (nolock)),5)
		set @nrcartao = 'CL' + replicate('0',5-LEN(convert(int,@nrcartao))) + convert(varchar,@nrcartao+1)
	end else
		set @nrcartao = ''

	/* Increment client number if needed */
	set @no = case when @no <= 0 then (select isnull(max(no)+1,1) from b_utentes (nolock)) else @no end
	
	/* 
	* Create Client 
	*/
	INSERT INTO b_utentes (
		utstamp, nome
		,no, estab, ncont, nome2
		,nrcartao, inactivo, naoencomenda, nocredit, cobnao
		,morada, codpost, local, zona, fax, telefone, tlmvl
		,tipo, email, url, profi, hablit, bino, nascimento, sexo, codigoP, descP
		,nbenef, nbenef2, codpla, desPLA, entPla, VALIPLA, VALIPLA2
		,pai, mae, notif, pacgen, tratamento, obs, ninscs, csaude, drfamilia
		,Vencimento, desconto, tipodesc, alimite, modofact, odatraso, tabiva, nib
		,particular, autofact, clivd, clinica, eplafond
		,descpp, radicaltipoemp, TpDesc, Classe, Ccusto, fref, Moeda
		,conta, contaacer, contafac, contaainc, contatit, contalet, contaletdes, contaletsac
		,ousrinis, ousrdata, ousrhora
		,usrinis, usrdata, usrhora
		,entfact, peso, altura
		,recmmotivo, recmdescricao, recmdatainicio, recmdatafim, recmtipo, itmotivo, itdescricao, itdatainicio, itdatafim
		,cesd, cesdcart, cesdcp, cesdidi, cesdidp, cesdpd, cesdval
		,atestado, atnumero, attipo, atcp, atpd, atval
		,codigoPNat, descPNat, pais
		,no_ext, idstamp)
	
	select
		@stamp, left(@nome,55)
		,@no, @estab, @ncont, ''
		,@nrcartao, 0, 0, 0, 0
		,@morada, @codpost, @local, @zona, @fax, @telefone, @tlmvl
		,@tipo, @email, '', @profi, '', @bino, @nascimento, @sexo, 'PT', 'PORTUGAL'
		,@nbenef, @nbenef2, @codPla, @descrPla, '', '19000101', @validadePla
		,'', '', 1, 0, '', @obs, '', @csaude, @drfamilia
		,'30', 0, '', 0, '', 0, 0, ''
		,0, 0, 0, 0, 0
		,0, 1, '', '', '', '', 'EURO'
		/* contas */
		,conta = isnull(LEFT((select conta = replace(replace(rtrim(textvalue),'TP','11'),'NNNNNN', right('000000' + convert(varchar,@no), (case when len(@no)<=6 then 6 else len(@no) end) )) from b_parameters (nolock) where stamp='ADM0000000242' and bool=1),15),'')
		,contaacer = isnull(LEFT((select contaacer = replace(replace(rtrim(textvalue),'TP','11'),'NNNNNN', right('000000' + convert(varchar,@no), (case when len(@no)<=6 then 6 else len(@no) end) )) from b_parameters (nolock) where stamp='ADM0000000244' and bool=1),15),'')
		,contafac = isnull(LEFT((select contafac = replace(replace(rtrim(textvalue),'TP','11'),'NNNNN', right('00000' + convert(varchar,@no), (case when len(@no)<=5 then 5 else len(@no) end) )) from b_parameters (nolock) where stamp='ADM0000000246' and bool=1),15),'')
		,contaainc = isnull(LEFT((select contaainc = replace(replace(rtrim(textvalue),'TP','11'),'NNNNNN', right('00000' + convert(varchar,@no), (case when len(@no)<=6 then 6 else len(@no) end) )) from b_parameters (nolock) where stamp='ADM0000000243' and bool=1),15),'')
		,contatit = isnull(LEFT((select contatit = replace(replace(rtrim(textvalue),'TP','11'),'NNNNN', right('00000' + convert(varchar,@no), (case when len(@no)<=5 then 5 else len(@no) end) )) from b_parameters (nolock) where stamp='ADM0000000245' and bool=1),15),'')
		,contalet = isnull(LEFT((select contalet = replace(replace(rtrim(textvalue),'TP','1'),'NNNNN', right('00000' + convert(varchar,@no), (case when len(@no)<=5 then 5 else len(@no) end) )) from b_parameters (nolock) where stamp='ADM0000000247' and bool=1),15),'')
		,contaletdes = isnull(LEFT((select contaletdes = replace(replace(rtrim(textvalue),'TP','11'),'NNNNN', right('00000' + convert(varchar,@no), (case when len(@no)<=5 then 5 else len(@no) end) )) from b_parameters (nolock) where stamp='ADM0000000248' and bool=1),15),'')
		,contaletsac = isnull(LEFT((select contaletsac = replace(replace(rtrim(textvalue),'TP','11'),'NNNNN', right('00000' + convert(varchar,@no), (case when len(@no)<=5 then 5 else len(@no) end) )) from b_parameters (nolock) where stamp='ADM0000000249' and bool=1),15),'')
		/* fim de contas */
		,@iniciais, case when @ousrdata = '19000101' then convert(varchar,getdate(),112) else @ousrdata end, case when @usrhora = '00:00:00' then convert(varchar,getdate(),108) else @usrhora end
		,@iniciais, case when @usrdata = '19000101' then convert(varchar,getdate(),112) else @usrdata end, case when @usrhora = '00:00:00' then convert(varchar,getdate(),108) else @usrhora end
		,1, 0, 0
		,'', '', '19000101', '19000101', '', '', '', '19000101', '19000101'
		,0, '', '', '', '', '', '19000101'
		,0, '', '', '', '', '19000101' 
		,'PT', 'PORTUGAL', 1
		,@no_ext, @idstamp
		
	
	/*
	* Cria cart�o de cliente se aplic�vel
	*/
	if @criaCartaoCliente=1
	begin
		insert into B_fidel
			(fstamp, clstamp, clno, clestab, nrcartao, validade, ousr, usr)
		values
			(left(NEWID(),23), @stamp, @no, @estab, @nrcartao, '30001231', 1, 1)
	end

go
Grant Execute On dbo.up_clients_create to Public
Grant control On dbo.up_clients_create to Public
go
------------------------------------------------

/*
	Atualiza ficha de cliente

	exec up_clients_update 200, 0, 'Cliente Final', '999999999', 'morada', 'codpost'
								,'local', 'zona', 'fax', 'telefone', 'tlmvl', 'Normal', 'email'
								,'111111111', '20131212', 'M', 'observa��es', 1, 'Loja 1', '', 0

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_clients_update]') IS NOT NULL
	drop procedure dbo.up_clients_update
go

create procedure dbo.up_clients_update
	@no numeric(10)
	,@estab numeric(3)
	,@nome varchar(55)
	,@ncont varchar(20)
	,@morada varchar(55)
	,@codpost varchar(45)
	,@local varchar(45)
	,@zona varchar(20)
	,@fax varchar(13)
	,@telefone varchar(13)
	,@tlmvl varchar(13)
	,@tipo varchar(20)
	,@email varchar(45)
	,@bino varchar(20)
	,@nascimento datetime
	,@sexo varchar(1)
	,@profi varchar(20)
	,@obs varchar(254)
	,@nbenef varchar(18)
	,@nbenef2 varchar(20)
	,@csaude varchar(254)
	,@drfamilia varchar(55)
	,@user numeric(6)
	,@loja varchar(20)
	,@usrdata datetime
	,@usrhora varchar(8) = '00:00:00'
	,@no_ext varchar(20) = ''
	,@idstamp char(25) = ''
	,@codPla varchar(40)
	,@descrPla varchar(254)
	,@validadePla varchar(10)
	,@inativo bit
	,@sync bit = 0

/* WITH ENCRYPTION */

AS

	set nocount on

	/* Iniciais de Utilizador */
	declare @iniciais varchar(3);
	set @iniciais = isnull((select iniciais from B_us (nolock) where userno = @no),'ADM')

	/*
	* Create Client
	*/
	update
		B_utentes
	set
		nome			= left(@nome,55)
		,ncont			= @ncont
		,nome2			= ''
		,inactivo		= @inativo
		,morada			= @morada
		,codpost		= @codpost
		,local			= @local
		,zona			= @zona
		,fax			= @fax
		,telefone		= @telefone
		,tlmvl			= @tlmvl
		,tipo			= @tipo
		,email			= @email
		,bino			= @bino
		,nascimento		= @nascimento
		,sexo			= @sexo
		,profi			= @profi
		,obs			= @obs
		,nbenef			= @nbenef
		,nbenef2		= @nbenef2
		,csaude			= @csaude
		,drfamilia		= @drfamilia

		,usrinis		= @iniciais
		,usrdata		= case when @usrdata = '19000101' then convert(varchar,getdate(),112) else @usrdata end
		,usrhora		= case when @usrhora='00:00:00' then convert(varchar,getdate(),108) else @usrhora end
		,idstamp		= @idstamp
		,codPla			= @codPla
		,despla			= @descrPla
		,valipla2		= @validadePla
	where
		no = case when idstamp='' then @no else no end
		and estab = case when idstamp='' then @estab else estab end
		and idstamp = case when idstamp='' then idstamp else @idstamp end
		/* Se em modo sincroniza��o apenas cria se o usrdata e usrhora for superior */
		and 1 = case when @sync=0 then 1
					else case
							when (@usrdata > b_utentes.usrdata) or (@usrdata = b_utentes.usrdata and @usrhora >= b_utentes.usrhora)
							then 1 else 0
							end
					end

go
Grant Execute On dbo.up_clients_update to Public
Grant control On dbo.up_clients_update to Public
go 
------------------------------------------------


/*
	Procurar �ltimo n�mero de cliente

	exec up_clients_getNextNr
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_clients_getNextNr]') IS NOT NULL
	drop procedure dbo.up_clients_getNextNr
go

create procedure dbo.up_clients_getNextNr

/* WITH ENCRYPTION */
AS

	set nocount on

	select
		isnull(max(no)+1,1)
	from
		b_utentes

go
Grant Execute On dbo.up_clients_getNextNr to Public
Grant control On dbo.up_clients_getNextNr to Public
go
------------------------------------------------

/*
	Procurar �ltimo n�mero de cliente
	
	exec up_clients_getNr 'ADMCE5C819194114FE3A99C4D'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_clients_getNr]') IS NOT NULL
	drop procedure dbo.up_clients_getNr
go

create procedure dbo.up_clients_getNr
	@stamp char(25)
	
/* WITH ENCRYPTION */
AS

	set nocount on

	select 
		no, estab 
	from 
		b_utentes (nolock) 
	where 
		utstamp = rtrim(@stamp)

go
Grant Execute On dbo.up_clients_getNr to Public
Grant control On dbo.up_clients_getNr to Public
go
------------------------------------------------


/*
	Procurar �ltimo n�mero de cliente
	
	exec up_clients_exists 1, 0
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_clients_exists]') IS NOT NULL
	drop procedure dbo.up_clients_exists
go

create procedure dbo.up_clients_exists
	@no numeric(10)
	,@dep numeric(3)

/* WITH ENCRYPTION */
AS
	set nocount on

	if exists (select 1 from b_utentes (nolock) where no = @no and estab = @dep)
	begin
		select convert(bit,1)
	end else begin
		select convert(bit,0)
	end

go
Grant Execute On dbo.up_clients_exists to Public
Grant control On dbo.up_clients_exists to Public
go
------------------------------------------------