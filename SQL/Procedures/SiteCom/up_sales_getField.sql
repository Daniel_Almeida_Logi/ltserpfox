/* Devolve o valor de um campo

	exec up_sales_getField 'fno', 'ADMD39E3A31-C4FF-4795-840'
		
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sales_getField]') IS NOT NULL
	drop procedure dbo.up_sales_getField
go

create procedure dbo.up_sales_getField
	@field varchar(40)
	,@stamp char(25)

/* WITH ENCRYPTION */
as

	declare @str varchar(max)
	
	set @str = N'
	select
		'+@field+'
	from 
		ft (nolock)
	where 
		ftstamp = ''' + rtrim(@stamp) + ''''
	
	exec(@str)

GO
Grant Execute On dbo.up_sales_getField to Public
Grant control On dbo.up_sales_getField to Public
GO
