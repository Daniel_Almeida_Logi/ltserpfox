/*
	Devolve o c�digo de uma taxa

	exec up_cert_getDocTypeAndLastHash '20130101', 3, 2
*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cert_getDocTypeAndLastHash]') IS NOT NULL
	drop procedure dbo.up_cert_getDocTypeAndLastHash
go

create procedure dbo.up_cert_getDocTypeAndLastHash
	@date datetime
	,@docType numeric(3)
	,@saleNr numeric(10)
	,@site varchar(20)

/* WITH ENCRYPTION */
as

	declare @docNrAnt varchar(40)
	set @docNrAnt = convert(varchar(10),@docType) + '/' + convert(varchar(10),@saleNr-1)

	select
		tiposaft = (select top 1 td.tiposaft from td (nolock) where ndoc=3)
        ,hash = ISNULL(
					(select 
						c.hash
					 from 
						B_cert c (nolock)
					 where 
						YEAR(c.date)  = YEAR(@DATE)
						and c.invoiceNo = @docNrAnt
						and c.site = @site)
					,'')

GO
Grant Execute On dbo.up_cert_getDocTypeAndLastHash to Public
Grant control On dbo.up_cert_getDocTypeAndLastHash to Public
GO
---------------------------------