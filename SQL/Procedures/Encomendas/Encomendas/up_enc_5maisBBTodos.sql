-- Ver 5 mais Baratos Ordenado por BB
-- exec up_enc_5maisBBTodos 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

If OBJECT_ID('[dbo].[up_enc_5maisBBTodos]') IS NOT NULL
	drop procedure dbo.up_enc_5maisBBTodos
Go

create procedure dbo.up_enc_5maisBBTodos

@site_nr tinyint

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	select 
	* 
	from (
		select
			a.ref,
			a.design,
			epv1,
			b.epreco,
			c.pvpmaxre,
			c.ranking,
			c.grphmgcode,
			a.marg4, 
			id = ROW_NUMBER() over (partition by c.grphmgcode order by c.grphmgcode, a.marg4 desc)
		from st (nolock) as a
		inner join fpreco (nolock) as b on a.ref=b.cnp and b.grupo='pvp'
		inner join fprod (nolock) as c on a.ref=c.cnp
		where
			ranking = 1
			and a.site_nr = @site_nr 
	) as x 
	where
		x.id = 1
	
Option (recompile)		

GO
Grant Execute On dbo.up_enc_5maisBBTodos to Public
Grant Control On dbo.up_enc_5maisBBTodos to Public
GO