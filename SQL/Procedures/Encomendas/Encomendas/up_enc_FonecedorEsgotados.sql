-- exec up_enc_FonecedorEsgotados
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

If OBJECT_ID('[dbo].[up_enc_FonecedorEsgotados]') IS NOT NULL
	drop procedure dbo.up_enc_FonecedorEsgotados
Go

create procedure dbo.up_enc_FonecedorEsgotados

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON


select * 
from 
	(
	Select	
		id = ROW_NUMBER() over (partition by ref order by fo.data desc,fo.usrhora desc)
		,fn.ref
		,fn.u_upc
		,fo.no
		,fo.estab
		,fo.data
		,fn.qtt
		,fo.usrhora
	From
		fo (nolock)
		inner join fn (nolock) on fo.fostamp = fn.fostamp
		Inner join cm1 (nolock) on cm1.cmdesc = fn.docnome
	Where
		qtt = 0
		and fo.doccode IN (55,101,102)
		and FOLANSL = 1
	) x
where
	x.id = 1
Option (recompile)		

GO
Grant Execute On dbo.up_enc_FonecedorEsgotados to Public
Grant Control On dbo.up_enc_FonecedorEsgotados to Public
GO