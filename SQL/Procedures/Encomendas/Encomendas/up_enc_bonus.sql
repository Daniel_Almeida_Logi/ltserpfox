/* Ver Bonus

	exec up_enc_bonus '', '1'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_enc_bonus]') IS NOT NULL
	drop procedure dbo.up_enc_bonus
go

create procedure dbo.up_enc_bonus
@ref char (18)
,@site_nr tinyint

/* with encryption */
AS

SET NOCOUNT ON

declare @data as datetime = getdate()

select
	bonus			= bi.lobs
	,fornecedor		= bo.nome
	,nr				= bo.no
	,validade		= convert(varchar,bo.datafinal,102)
from
	bi (nolock)
	inner join bo (nolock) on bi.bostamp=bo.bostamp
	inner join st (nolock) on bi.ref=st.ref
where
	bo.ndos=35 and bi.ref=@ref and bo.datafinal>=@data
	and st.site_nr = @site_nr 

GO
Grant Execute on dbo.up_enc_bonus to Public
Grant Control on dbo.up_enc_bonus to Public
Go
