-- Ver 5 mais Baratos (FUN��ES DA VENDA)
-- exec up_enc_5maisBB '5440987',1, 0, 0, '1'
-- exec up_enc_5maisBB '5440987',1, 8, 0, '1'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

If OBJECT_ID('[dbo].[up_enc_5maisBB]') IS NOT NULL
	drop procedure dbo.up_enc_5maisBB
Go

create procedure dbo.up_enc_5maisBB
	 @ref	varchar (18)
	,@armazem numeric(5,0)
	,@no	numeric(9,0)
	,@estab numeric(5,0)
	,@site_nr tinyint

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	IF @no != 0
	BEGIN

		;with
			cte1(grpmhgcode, epreco) as (
				select
					grphmgcode,
					epreco = case when pvpcalc=0 then pvporig else pvpcalc end
				from 
					fprod (nolock)
				where
					fprod.cnp = @ref
			),
			
			cte2 as (
				select
					 ref		= fprod.cnp
					,design		= fprod.design
					,epreco		= case when pvpcalc=0 then pvporig else pvpcalc end
					,stock		= ISNULL(sa.stock,0)
					,marg4
					,grphmgcode
					,t45 = case when fprod.pvporig <= fprod.preco_acordo and fprod.preco_acordo > 0 then "T4"
								when ranking=1 and grphmgcode != 'GH0000' and grphmgcode != '' and fprod.pvporig <= fprod.pvpmaxre then "T5" 
								else ""
							end
				from
					fprod (nolock)
					left join	st (nolock)	on st.ref = fprod.cnp
					inner join	cte1		on fprod.grphmgcode=cte1.grpmhgcode
					left join	sa (nolock)	on sa.ref = st.ref and sa.armazem = @armazem
				where
					--ranking=1
					--and 
					grphmgcode != 'GH0000' and grphmgcode != ''
					and fprod.cnp != @ref
					and st.site_nr = @site_nr
			)

		select 
			* 
		from (		
			select
				sel = convert(bit,0)
				,oref = @ref /* guarda referencia original para usar no cursor */
				,cte2.ref
				,cte2.design
				,cte2.epreco
				,stock
				,barato = dense_rank() over (order by epreco)
				,isnull(cte2.marg4,0) as bb
				,isnull(cte2.t45,0) as t45
			from
				cte2

		) x
		--where
		--	barato between 1 and 5
			
		order by
			bb desc
	
	END
	ELSE
	BEGIN
		;with
			cteExistencia5Baratos as (
				select
					existeNos5Baratos = sum(case when ranking = 1 then 1 else 0 end),
					c.grphmgcode
				from 
					st (nolock) as a
					inner hash join fpreco (nolock) as b on a.ref=b.cnp and b.grupo='pvp'
					inner hash join fprod (nolock) as c on a.ref=c.cnp and (c.grphmgcode!='GH0000' or c.grphmgcode!='')
				where
					--ranking = 1
					--and 
					a.site_nr = @site_nr 
				group by
					grphmgcode
			),
			cteMelhorBB as (

				select 
					* 
				from (
						select
							id = ROW_NUMBER() over (partition by c.grphmgcode order by c.grphmgcode, a.marg4 desc)
							,a.ref,
							a.design,
							epv1,
							b.epreco,
							c.pvpmaxre,
							c.ranking,
							c.grphmgcode,
							a.marg4
						from 
							st (nolock) as a
							inner hash join fpreco (nolock) as b on a.ref=b.cnp and b.grupo='pvp'
							inner hash join fprod (nolock) as c on a.ref=c.cnp and (c.grphmgcode!='GH0000' or c.grphmgcode!='')
						where
							--ranking = 1
							--and 
							a.site_nr = @site_nr
					) as x 
				where
					x.id = 1
			),
			cteEsgotados as (
				select * 
				from 
					(
					Select	
						contagem = ROW_NUMBER() over (partition by ref order by ref)
						,id = ROW_NUMBER() over (partition by ref,fo.no,fo.estab order by fo.data desc,fo.usrhora desc)
						,fn.ref
						,fn.u_upc
						,fo.no
						,fo.estab
						,fo.data
						,fn.qtt
						,fo.usrhora
					From
						fo (nolock)
						inner hash join fn (nolock) on fn.fostamp = fo.fostamp and fo.doccode IN (55,101,102) and fn.ref != ''
						Inner hash join cm1 (nolock) on cm1.cmdesc = fn.docnome and cm1.FOLANSL = 1
					Where 
						fo.no = @no
						and fo.estab = @estab
					) x
				where
					x.id = 1
					and x.qtt=0 
			),
			cteBonus as (
				Select 
					*
				From(
					Select 
						contagem = ROW_NUMBER() over (partition by ref order by ref)
						,id = ROW_NUMBER() over (partition by ref,bo.no,bo.estab order by bo.dataobra desc,bo.usrhora desc)
						,bi.ref
						,bonus = bi.lobs
						,bo.no
						,bo.estab
						,bo.dataobra
					from 
						bo (nolock)
						inner join bi (nolock) on bi.bostamp = bo.bostamp and bo.ndos = 35
					Where	
						MONTH(bo.dataobra) = MONTH(GETDATE())
						and YEAR(bo.dataobra) = YEAR(GETDATE())
						and bo.no = @no
						and bo.estab = @estab
						
				) x
				where x.id = 1
			)
			,ctePCLForn as (
				select 
					*
					 ,melhorPreco = dense_rank() over (partition by ref order by u_upc)
				from 
					(
					Select	
						id = ROW_NUMBER() over (partition by ref,no,estab order by fo.data desc,fo.usrhora desc)
						,fn.ref
						,fn.u_upc
						,fo.no
						,fo.estab
						,fo.data
						,fn.qtt
						,fo.usrhora
					From
						fo (nolock)
						Inner hash join cm1 (nolock) on fo.doccode=cm1.cm and cm1.FOLANSL = 1 and fo.doccode IN (55,101,102)
						inner hash join fn (nolock) on fo.fostamp = fn.fostamp and fn.u_upc != 0 and fn.ref!=''
					Where 
						fo.no = @no
						and fo.estab = @estab
					) x
				where
					x.id = 1
			),
			cte1(grpmhgcode, epreco) as (
				select
					grphmgcode,
					epreco = case when pvpcalc=0 then pvporig else pvpcalc end
				from 
					fprod (nolock)
				where
					fprod.cnp = @ref
			),
			
			cte2 as (
				select
					 ref		= fprod.cnp
					,design		= fprod.design
					,epreco		= case when pvpcalc=0 then pvporig else pvpcalc end
					,stock		= ISNULL(sa.stock,0)
					,marg4
					,grphmgcode
					,t45 = case when fprod.pvporig <= fprod.preco_acordo and fprod.preco_acordo > 0 then "T4"
								when ranking=1 and grphmgcode != 'GH0000' and grphmgcode != '' and fprod.pvporig <= fprod.pvpmaxre then "T5" 
								else ""
							end
				from
					fprod (nolock)
					left join	st (nolock)	on st.ref = fprod.cnp
					inner join	cte1		on fprod.grphmgcode=cte1.grpmhgcode
					left join	sa (nolock)	on sa.ref = st.ref and sa.armazem = @armazem
				where
					--ranking=1
					--and 
					grphmgcode != 'GH0000' and grphmgcode != ''
					and fprod.cnp != @ref
					and st.site_nr = @site_nr
			)

		select 
			* 
		from (		
			select
				sel = convert(bit,0)
				,oref = @ref /* guarda referencia original para usar no cursor */
				,cte2.ref
				,cte2.design
				,cte2.epreco
				,stock
				,barato = dense_rank() over (order by cte2.epreco)
				,isnull(cte2.marg4,0) as bb
				/*BB do mesmo grupo Homogeneo*/
				,alertaStockGH		= case 
										when (cte2.grphmgcode='GH0000' or cte2.grphmgcode='' or cte2.grphmgcode is null or cte2.marg4 = 0) then 0 
										when cteMelhorBB.ref is not null then 0 
										when cteExistencia5Baratos.grphmgcode is null then 0
										else 1 
									end
				,alertaPCL			= case when ctePCLForn.melhorPreco = 1 or ctePCLForn.melhorPreco is null then 0 else 1 end 
				,alertaBonus		= case when cteBonus2.ref is not null and cteBonus.ref is null then 1 else 0 end
				,cteMelhorBB.ref as melhorbb
				,cteExistencia5Baratos.grphmgcode
				,cte2.t45
			from
				cte2
				left join ctePCLForn on ctePCLForn.ref = @ref
				left join cteBonus on cteBonus.ref = @ref
				left join cteBonus as cteBonus2 on cteBonus2.ref = @ref and cteBonus2.contagem = 1
				left join cteEsgotados on cteEsgotados.ref = @ref 
				left join cteMelhorBB on @ref = cteMelhorBB.ref
				left join cteExistencia5Baratos on cte2.grphmgcode = cteExistencia5Baratos.grphmgcode
		) x
		--where
		--	barato between 1 and 5
			
		order by
			bb desc	
	END

GO
Grant Execute On dbo.up_enc_5maisBB to Public
Grant Control On dbo.up_enc_5maisBB to Public
GO