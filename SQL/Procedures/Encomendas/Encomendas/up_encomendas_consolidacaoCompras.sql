/*
	Consolidação de Compras
	. Criar cenários para testes

	  

                If OBJECT_ID('tempdb.dbo.#temp_consolidacaoEmpresas') IS NOT NULL

                        DROP TABLE #temp_consolidacaoEmpresas

                If OBJECT_ID('tempdb.dbo.#temp_consolidacaoMeses') IS NOT NULL

                        DROP TABLE #temp_consolidacaoMeses

                If OBJECT_ID('tempdb.dbo.#temp_consolidacaoSt') IS NOT NULL

                        DROP TABLE #temp_consolidacaoSt

                create table #temp_consolidacaoEmpresas (site_nr tinyint, site varchar(20))

                create table #temp_consolidacaoMeses (ano int, mes int)

                create table #temp_consolidacaoSt (ref varchar(18))

                        INSERT INTO #temp_consolidacaoEmpresas(site_nr,site)Values (1,'Loja 1')

                        INSERT INTO #temp_consolidacaoEmpresas(site_nr,site)Values (2,'Loja 2')

                        INSERT INTO #temp_consolidacaoEmpresas(site_nr,site)Values (3,'Loja 3')

                        INSERT INTO #temp_consolidacaoEmpresas(site_nr,site)Values (4,'Loja 4')

                        INSERT INTO #temp_consolidacaoEmpresas(site_nr,site)Values (5,'Loja 5')

                        INSERT INTO #temp_consolidacaoMeses (ano,mes)Values(2023,11)

                        INSERT INTO #temp_consolidacaoMeses (ano,mes)Values(2023,10)

                        INSERT INTO #temp_consolidacaoMeses (ano,mes)Values(2022,12)

                        INSERT INTO #temp_consolidacaoSt(ref)Values ('3854585')

                        INSERT INTO #temp_consolidacaoSt(ref)Values ('5397435')

                        INSERT INTO #temp_consolidacaoSt(ref)Values ('5631767')

                        INSERT INTO #temp_consolidacaoSt(ref)Values ('5440987')

                        INSERT INTO #temp_consolidacaoSt(ref)Values ('8168534')

                exec up_encomendas_consolidacaoCompras 'temp_consolidacaoEmpresas','temp_consolidacaoMeses','temp_consolidacaoSt'




	
		If OBJECT_ID('tempdb.dbo.#temp_consolidacaoEmpresas') IS NOT NULL
			DROP TABLE #temp_consolidacaoEmpresas
		If OBJECT_ID('tempdb.dbo.#temp_consolidacaoMeses') IS NOT NULL
			DROP TABLE #temp_consolidacaoMeses
		If OBJECT_ID('tempdb.dbo.#temp_consolidacaoSt') IS NOT NULL
			DROP TABLE #temp_consolidacaoSt
		create table #temp_consolidacaoEmpresas (site_nr tinyint, site varchar(20))
		create table #temp_consolidacaoMeses (ano int, mes int)
		create table #temp_consolidacaoSt (ref varchar(18))
			INSERT INTO #temp_consolidacaoEmpresas(site_nr,site)Values (1,'Loja 1')
			INSERT INTO #temp_consolidacaoMeses (ano,mes)Values(2023,2)
			INSERT INTO #temp_consolidacaoMeses (ano,mes)Values(2023,1)
			INSERT INTO #temp_consolidacaoMeses (ano,mes)Values(2022,3)
			INSERT INTO #temp_consolidacaoSt(ref)Values ('5440987')
		exec up_encomendas_consolidacaoCompras 'temp_consolidacaoEmpresas','temp_consolidacaoMeses','temp_consolidacaoSt'
			
*/

if OBJECT_ID('[dbo].[up_encomendas_consolidacaoCompras]') IS NOT NULL
	drop procedure dbo.up_encomendas_consolidacaoCompras
go

create procedure dbo.up_encomendas_consolidacaoCompras
	 @tabelaEmpresas varchar(40)
	 ,@tabelaMeses varchar(40)
	 ,@tabelaRefs varchar(40)

/* with encryption */
AS
SET NOCOUNT ON

	DECLARE @cols AS NVARCHAR(MAX), @cols2 AS NVARCHAR(max)='', @cols3 AS NVARCHAR(max)='', @colsHistVendas AS NVARCHAR(max)=''
	declare @sql  NVARCHAR(max) = ''
	declare @sql1 NVARCHAR(max) = ''
	declare @sql2 Nvarchar(max) = ''
	declare @colunas_t table (site_nr tinyint, site varchar(20))
	
	-- Prepara colunas
	set @cols = 'select	site_nr, site from #' + @tabelaEmpresas
	insert into @colunas_t
	exec(@cols)
	
	-- para o inner select
	select
		@cols2 = @cols2 + N'
					,vd_' + replace(site,' ','_') + ' = sum(case when ft.site=''' + site + ''' then CASE WHEN 
															td.cmcc <50 then ISNULL(fi.qtt,0) 
															else ISNULL(-fi.qtt,0) end end)
					,exc_' + replace(site,' ','_') + ' = 0
					,nec_' + replace(site,' ','_') + ' = 0'
					
	from
		@colunas_t

	select
		@colsHistVendas = @colsHistVendas + N'
					,vd_' + replace(site,' ','_') + ' = sum(case when hist_vendas.site=''' + site + ''' then ISNULL(hist_vendas.qt,0) else 0 end)
					,exc_' + replace(site,' ','_') + ' = 0
					,nec_' + replace(site,' ','_') + ' = 0'
					
	from
		@colunas_t

	-- para o outer select
	select
		@cols3 = @cols3 + N'
				,stk_' + replace(site,' ','_') + ' = sum(case when st.site_nr=' + ltrim(str(site_nr)) + ' then st.stock else 0 end)
				,ISNULL(vd_' + replace(site,' ','_') + ',0) AS vd_' + replace(site,' ','_') + '
				,exc_' + replace(site,' ','_') + ' = ISNULL(case when (sum(case when st.site_nr=' + ltrim(str(site_nr)) + ' then st.stock else 0 end)) - vd_' + replace(site,' ','_') + ' < 0 then 0 else (sum(case when st.site_nr=' + ltrim(str(site_nr)) + ' then st.stock else 0 end)) - vd_' + replace(site,' ','_') + ' end,0)
				,nec_' + replace(site,' ','_') + ' = ISNULL(case when vd_' + replace(site,' ','_') + ' - (sum(case when st.site_nr=' + ltrim(str(site_nr)) + ' then st.stock else 0 end)) < 0 then 0 else vd_' + replace(site,' ','_') + ' - (sum(case when st.site_nr=' + ltrim(str(site_nr)) + ' then st.stock else 0 end)) end,0)
				,dist_' + replace(site,' ','_') + ' = 0'
	from
		@colunas_t
	

	
	-- para a clausula where
	set @cols=''
	select
		@cols = @cols + N'
				,vd_' + replace(site,' ','_') + ',nec_' + replace(site,' ','_')
	from
		@colunas_t

	-- prepare query
	set @sql = @sql + 
	 N'
			select
				x.ref,design = (select top 1 design from st (nolock) where x.ref = st.ref) ' + @cols3 + '
				,convert(numeric(9,0),0) as stk_total,
				convert(numeric(9,0),0) as vd_total,
				convert(numeric(9,0),0) as exc_total,
				convert(numeric(9,0),0) as nec_total,
				convert(numeric(9,0),0) as dist_total 
				,convert(bit,0) as pesquisa
			from (
				select
					fi.ref' + @cols2 + '
				from
					fi (nolock)
					inner join ft (nolock) on ft.ftstamp=fi.ftstamp
					inner join td (nolock) on td.ndoc = ft.ndoc'
			
	set @sql1 = @sql1 + N'	where
					ft.site in (select site from #' + @tabelaEmpresas + ') and
					fi.ref in (select ref from #' + @tabelaRefs + ') and
					CONCAT(year(ft.fdata),month(ft.fdata)) in
						(select CONCAT(ano,mes) from #' + @tabelaMeses + ') AND 
					td.cmsl != 0
					AND ( ft.no = 0 or ft.no > 198)
					AND	(ft.tipodoc!=4 ) 
				group by
					fi.ref
				union all
				select
					hist_vendas.ref' + @colsHistVendas + '
				from 
					hist_vendas (nolock)
				where
					hist_vendas.site in (select site from #' + @tabelaEmpresas + ') and
					hist_vendas.ref in (select ref from #' + @tabelaRefs + ') and
					CONCAT(year(hist_vendas.ano),month(hist_vendas.mes)) in
						(select CONCAT(ano,mes) from #' + @tabelaMeses + ') 
				group by
					hist_vendas.ref

			)	x
				inner join st on st.ref=x.ref
			group by 
				x.ref' + @cols
				

	print @sql + @sql1
	execute(@sql + @sql1)

	/* CleanUp */
	declare @dropTables varchar(1024)
	set @dropTables = '
		If OBJECT_ID(''tempdb.dbo.#'+@tabelaEmpresas + ''') IS NOT NULL
			drop table #' + @tabelaEmpresas + ';
		If OBJECT_ID(''tempdb.dbo.#'+@tabelaMeses + ''') IS NOT NULL
			drop table #' + @tabelaMeses + ';
		If OBJECT_ID(''tempdb.dbo.#' + @tabelaRefs + ''') IS NOT NULL
			drop table #'+@tabelaRefs + ';'
	exec(@dropTables)

GO
Grant Execute on dbo.up_encomendas_consolidacaoCompras to Public
Grant Control on dbo.up_encomendas_consolidacaoCompras to Public
Go