 -- exec up_enc_dadosAlertasEncAutomaticas '',2,0,'A01205A'
 -- exec up_enc_dadosAlertasEncAutomaticas '', 2, 0, '', '', '', 0,'1198564'
SET ANSI_NULLS ON
SET ANSI_WARNINGS ON
SET QUOTED_IDENTIFIER OFF
GO

If OBJECT_ID('[dbo].[up_enc_dadosAlertasEncAutomaticas]') IS NOT NULL
	drop procedure dbo.up_enc_dadosAlertasEncAutomaticas
Go

create procedure dbo.up_enc_dadosAlertasEncAutomaticas
	 @ref	varchar (18)
	,@no	numeric(9,0)
	,@estab numeric(5,0)
	,@site_nr tinyint
	,@grupo varchar(20) = ''

/* WITH ENCRYPTION */
AS


	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosExistencia5Baratos'))
		DROP TABLE #dadosExistencia5Baratos
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosMelhorBB'))
		DROP TABLE #dadosMelhorBB
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosEsgotados'))
		DROP TABLE #dadosEsgotados
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosBonus'))
		DROP TABLE #dadosBonus
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosPCLForn'))
		DROP TABLE #dadosPCLForn
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosPCLFornGRupo'))
		DROP TABLE #dadosPCLFornGRupo		
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
		DROP TABLE #dadosSt	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosGerais'))
		DROP TABLE #dadosGerais			
		
	declare @tipoForn as varchar(20)
	set @tipoForn = (select tipo from fl where fl.no = @no and fl.estab = @estab)

	Select 
		*
	into 
		#dadosBonus
	From(
		Select 
			contagem = ROW_NUMBER() over (partition by ref order by ref)
			,id = ROW_NUMBER() over (partition by ref,bo.no,bo.estab order by bo.dataobra desc,bo.usrhora desc)
			,bi.ref
			,bonus = bi.lobs
			,bo.no
			,bo.estab
			,bo.dataobra
		from 
			bo (nolock)
			inner join bi (nolock) on bi.bostamp = bo.bostamp and bo.ndos = 35 -- and bi.ref = @ref
			inner join [dbo].[bo2] (nolock) on bo2.bo2stamp = bo.bostamp
		Where	
			MONTH(bo.dataobra) = MONTH(GETDATE())
			and YEAR(bo.dataobra) = YEAR(GETDATE())
			and bo.no = @no 
			and bo.estab = @estab
			and bo2.status=''			
	) x
	where x.id = 1


	/**/	

	select 
		*
		,melhorPreco = dense_rank() over (partition by ref order by u_upc)
	into
		#dadosPCLForn
	from 
		(
		Select	
			id = ROW_NUMBER() over (partition by ref,no,estab order by fo.data desc,fo.usrhora desc)
			,fn.ref
			,fn.u_upc
			,fo.no
			,fo.estab
			,fo.data
			,fn.qtt
			,fo.usrhora
		From
			fo (nolock)
			Inner join cm1 (nolock) on fo.doccode=cm1.cm and cm1.FOLANSL = 1 
			inner join fn (nolock) on fo.fostamp = fn.fostamp
		Where
			fo.no = @no 
			and fo.estab = @estab
			and fo.doccode IN (55,101,102)
			and  fn.u_upc != 0 
		) x
	where
		x.id = 1

	/**/
	select 
		*
		,melhorPreco = dense_rank() over (partition by ref order by u_upc)
	into
		#dadosPCLForn2
	from 
		(
		Select	
			id = ROW_NUMBER() over (partition by ref,no,estab order by fo.data desc,fo.usrhora desc)
			,fn.ref
			,fn.u_upc
			,fo.no
			,fo.estab
			,fo.data
			,fn.qtt
			,fo.usrhora
		From
			fo (nolock)
			Inner join cm1 (nolock) on fo.doccode=cm1.cm and cm1.FOLANSL = 1 
			inner join fn (nolock) on fo.fostamp = fn.fostamp
		Where
			fo.doccode IN (55,101,102)
			and  fn.u_upc != 0 
		) x
	where
		x.id = 1
	/**/

	

	/**/

	select 
		* 
	into
		#dadosEsgotados
	from 
		(
		Select	
			contagem = ROW_NUMBER() over (partition by ref order by ref)
			,id = ROW_NUMBER() over (partition by ref,fo.no,fo.estab order by fo.data desc,fo.usrhora desc)
			,fn.ref
			,fn.u_upc
			,fo.no
			,fo.estab
			,fo.data
			,fn.qtt
			,fo.usrhora
		From
			fo (nolock)
			inner join fn (nolock) on fn.fostamp = fo.fostamp and fo.doccode IN (55,101,102)
			Inner join cm1 (nolock) on cm1.cmdesc = fn.docnome and cm1.FOLANSL = 1
		) x
	where
		x.id = 1
		and x.qtt=0 

	/**/

	select 
		* 
	into 
		#dadosMelhorBB
	from (
			select
				id = ROW_NUMBER() over (partition by c.grphmgcode order by c.grphmgcode, a.marg4 desc)
				,a.ref,
				a.design,
				epv1,
				c.pvporig,
				c.pvpmaxre,
				c.ranking,
				c.grphmgcode,
				a.marg4
			from 
				st (nolock) as a
				inner hash join fprod (nolock) as c on a.ref=c.cnp and (c.grphmgcode!='GH0000' or c.grphmgcode!='')
			where
				ranking = 1
				and a.site_nr = @site_nr
		) as x 
	where
		x.id = 1

	/**/

	select
		existeNos5Baratos = sum(case when ranking = 1 then 1 else 0 end),
		c.grphmgcode
	into
		#dadosExistencia5Baratos
	from 
		st (nolock) as a
		inner hash join fpreco (nolock) as b on a.ref=b.cnp and b.grupo='pvp'
		inner hash join fprod (nolock) as c on a.ref=c.cnp and (c.grphmgcode!='GH0000' or c.grphmgcode!='')
	where
		ranking = 1
		and a.site_nr = @site_nr
	group by
		grphmgcode


	/* INDIVIDUAL */
	IF @grupo = ''
	Begin 


		Select 
			*
			,melhorPreco = dense_rank() over (partition by ref order by u_upc)
		into
			#dadosPCLFornGRupo
		from 
			(
			Select	
				id = ROW_NUMBER() over (partition by bi.ref,bo.no,bo.estab order by bo.dataobra desc,bo.usrhora desc)
				,bi.ref
				,u_upc = bi.binum2
				,bo.no
				,bo.estab
				,data = bo.dataobra
				,bi.qtt
				,bo.usrhora
				,bi.u_stockact
			From
				bo (nolock)
				inner join bi (nolock) on bo.bostamp = bi.bostamp	
			where
				bo.ndos = 45
					
			) x
		where
			x.id = 1	
			
		/**/

		select 
			st.ref
			,bonus	= isnull(cteBonus.bonus,'')
			,pcl	= case 
						when UPPER(@tipoForn) != 'GRUPO' then 
							isnull(ctePCLForn.u_upc,0)
						else 
							isnull(ctePCLFornGRupo.u_upc,0)
					end
			,esgotado = 
					case 
						when UPPER(@tipoForn) != 'GRUPO' then
							case 
								when cteEsgotados.ref is null then 
									'N�O' 
								ELSE 
									'SIM' 
							END
						Else
							case 
								when isnull(ctePCLFornGRupo.u_stockact,0) > 0 then convert(varchar,ctePCLFornGRupo.u_stockact) 
								ELSE 'SIM' 
							END
					end 
			,alertaStockGH		= 
						case 
							when (fprod.grphmgcode='GH0000' or fprod.grphmgcode='' or fprod.grphmgcode is null or st.marg4 = 0) then 
								0 
							when cteMelhorBB.ref is not null then 
								0 
							when cteExistencia5Baratos.grphmgcode is null then 
								0
							else 
								1 
						end
			,alertaPCL			= case when exists(Select * from #dadosPCLForn2 where #dadosPCLForn2.ref = st.ref and #dadosPCLForn2.melhorPreco = 1 and #dadosPCLForn2.no = @no and #dadosPCLForn2.estab = @estab) or not exists(Select * from #dadosPCLForn2 where #dadosPCLForn2.ref = st.ref) then 0 else 1 end 
			,alertaBonus		= case when cteBonus2.ref is not null and cteBonus.ref is null then 1 else 0 end
			
		from 
			st
			left join fprod on st.ref = fprod.cnp
			left join #dadosPCLForn as ctePCLForn on ctePCLForn.ref = st.ref
			left join #dadosPCLFornGRupo as ctePCLFornGRupo on ctePCLFornGRupo.ref = st.ref
			left join #dadosBonus as cteBonus on cteBonus.ref = st.ref
			left join #dadosEsgotados as cteEsgotados on cteEsgotados.ref = st.ref
			left join #dadosMelhorBB as cteMelhorBB on st.ref = cteMelhorBB.ref
			left join #dadosExistencia5Baratos as cteExistencia5Baratos on fprod.grphmgcode = cteExistencia5Baratos.grphmgcode
			left join #dadosBonus as cteBonus2 on cteBonus2.ref = st.ref and cteBonus2.contagem = 1 
			
		Where
			st.ref = case when @ref = '' then st.ref else @ref end		
			and st.site_nr = @site_nr
	End
	/*			GRUPO			*/
	ELSE 
	BEGIN 
	/**/
		
		-- guardar linkedserver e dados fonecedor grupo
		declare @sql nvarchar(1024), @sql2 nvarchar(2048),@no_server numeric(10), @estab_server numeric(3), @nome_server varchar(55)
		declare @server varchar(30)

		select top 1
			@server=ccc.odbc
			,@no_server=ccc.no
			,@estab_server=ccc.estab
			,@nome_server=fl.nome
		from condComercConfig ccc (nolock) inner join fl (nolock) on fl.no=ccc.no and fl.estab=ccc.estab
		where bdname=@grupo and sede=1

		If OBJECT_ID('tempdb.dbo.#dadosPCLFornGRupo2') IS NOT NULL
			drop table #dadosPCLFornGRupo2;

		create table #dadosPCLFornGRupo2(
			[id] [int] NOT NULL DEFAULT(0)
			,[ref] [char](18) NOT NULL DEFAULT ('')
			,[u_upc] [numeric](13,2) NOT NULL DEFAULT(0)
			,[no] [numeric](10,0) NOT NULL DEFAULT(0)
			,[estab] [numeric](3,0) NOT NULL DEFAULT(0)
			,[data] [datetime] NOT NULL DEFAULT(GETDATE())
			,[qtt] [numeric](12,3) NOT NULL DEFAULT(0)
			,[usrhora] [varchar](8) NOT NULL DEFAULT('')
			,[u_stockact] [numeric](14) NOT NULL DEFAULT(0)
			,[melhorPreco] [int] DEFAULT(0)
		)



		set @sql = N'
			select
				*
				--,melhorPreco = dense_rank() over (partition by ref order by u_upc)
				,melhorPreco = null
			from
				(
				Select
					id = ROW_NUMBER() over (partition by bi.ref, bo.no, bo.estab order by bo.dataobra desc,bo.usrhora desc)
					,bi.ref
					,u_upc = bi.binum2 -- PCLk
					,no = ' + convert(varchar,@no_server) + '
					,estabo = ' + convert(varchar,@estab_server) + '
					,data = bo.dataobra
					,bi.qtt
					,bo.usrhora
					,bi.u_stockact
				From
					' + @server + '.' + @grupo + '.dbo.bo
					inner join ' + @server + '.' + @grupo + '.dbo.bi on bo.bostamp = bi.bostamp
				where
					bo.fechada = 0
					and bo.ndos = 45
					
					and bi.ref != ''''
					and bi.binum2 != 0
				) x
			where
				x.id = 1'
	
		INSERT INTO #dadosPCLFornGRupo2
		execute sp_executesql @sql
	
		/**/

		select 
			st.ref
			,bonus	= isnull(cteBonus.bonus,'')
			,pcl	= case 
						when UPPER(@tipoForn) != 'GRUPO' then 
							isnull(ctePCLForn.u_upc,0)
						else 
							isnull(ctePCLFornGRupo.u_upc,0)
					end
			,esgotado = 
					case 
						when UPPER(@tipoForn) != 'GRUPO' then
							case 
								when cteEsgotados.ref is null then 
									'N�O' 
								ELSE 
									'SIM' 
							END
						Else
							case 
								when isnull(ctePCLFornGRupo.u_stockact,0) > 0 then convert(varchar,ctePCLFornGRupo.u_stockact) 
								ELSE 'SIM' 
							END
					end 
			,alertaStockGH		= 
						case 
							when (fprod.grphmgcode='GH0000' or fprod.grphmgcode='' or fprod.grphmgcode is null or st.marg4 = 0) then 
								0 
							when cteMelhorBB.ref is not null then 
								0 
							when cteExistencia5Baratos.grphmgcode is null then 
								0
							else 
								1 
						end
			,alertaPCL			= case when exists(Select * from #dadosPCLForn2 where #dadosPCLForn2.ref = st.ref and #dadosPCLForn2.melhorPreco = 1 and #dadosPCLForn2.no = @no and #dadosPCLForn2.estab = @estab) or not exists(Select * from #dadosPCLForn2 where #dadosPCLForn2.ref = st.ref) then 0 else 1 end 
			,alertaBonus		= case when cteBonus2.ref is not null and cteBonus.ref is null then 1 else 0 end
			
		from 
			st
			left join fprod on st.ref = fprod.cnp
			left join #dadosPCLForn as ctePCLForn on ctePCLForn.ref = st.ref
			left join #dadosPCLFornGRupo2 as ctePCLFornGRupo on ctePCLFornGRupo.ref = st.ref
			left join #dadosBonus as cteBonus on cteBonus.ref = st.ref
			left join #dadosEsgotados as cteEsgotados on cteEsgotados.ref = st.ref
			left join #dadosMelhorBB as cteMelhorBB on st.ref = cteMelhorBB.ref
			left join #dadosExistencia5Baratos as cteExistencia5Baratos on fprod.grphmgcode = cteExistencia5Baratos.grphmgcode
			left join #dadosBonus as cteBonus2 on cteBonus2.ref = st.ref and cteBonus2.contagem = 1 
			
		Where
			st.ref = case when @ref = '' then st.ref else @ref end	
			and st.site_nr = @site_nr
	END
	
	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosExistencia5Baratos'))
		DROP TABLE #dadosExistencia5Baratos
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosMelhorBB'))
		DROP TABLE #dadosMelhorBB
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosEsgotados'))
		DROP TABLE #dadosEsgotados
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosBonus'))
		DROP TABLE #dadosBonus
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosPCLForn'))
		DROP TABLE #dadosPCLForn
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosPCLFornGRupo'))
		DROP TABLE #dadosPCLFornGRupo		
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
		DROP TABLE #dadosSt	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosGerais'))
		DROP TABLE #dadosGerais			
		

GO
Grant Execute On dbo.up_enc_dadosAlertasEncAutomaticas to Public
Grant Control On dbo.up_enc_dadosAlertasEncAutomaticas to Public
GO