/*
	Insere detalhe BI
	
	exec up_encomendas_postal_insereBi '16050jkCmL', '16050tBLQI', 56, 35, '2000990', 'Leite Magnesia Phillips 83 mg/ml 200 ml Susp Or', '0 4,99 PVF(PLAT) 5,96 PVFSD 17% S/5,72'
	
	DELETE FROM BI WHERE BISTAMP = '16050jkCmL'
	bi
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_encomendas_postal_insereBi]') IS NOT NULL
	drop procedure dbo.up_encomendas_postal_insereBi
go

create procedure dbo.up_encomendas_postal_insereBi
	@bistamp varchar(25)
	,@bostamp varchar(25)
	,@obrano numeric(10,0)
	,@ndos numeric(3,0)
	,@ref varchar(18)
	,@design varchar(100)
	,@valor varchar(199)


/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	INSERT INTO bi
		(
		bistamp, bostamp,nmdos,obrano,ndos,ref,design,lobs
		,ousrinis, ousrdata, ousrhora,usrinis, usrdata, usrhora
		,dataobra,armazem,fechada
		)
	SELECT
		left(newid(),25)
		,@bostamp
		,'Bonus Fornecedor'
		,@obrano
		,@ndos
		,@ref
		,@design
		,@valor
		,'Adm', convert(varchar,getdate(),102), substring(convert(char(25),getdate(),113),13,8)
		,'Adm', convert(varchar,getdate(),102), substring(convert(char(25),getdate(),113),13,8)
		,convert(varchar,getdate(),102)
		,1
		,1

GO
Grant Execute on dbo.up_encomendas_postal_insereBi to Public
Grant Control on dbo.up_encomendas_postal_insereBi to Public
GO