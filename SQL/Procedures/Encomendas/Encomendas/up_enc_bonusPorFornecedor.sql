/* Ver Bonus por Fornecedor

	exec up_enc_bonusPorFornecedor '', 0, 0, '1'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_enc_bonusPorFornecedor]') IS NOT NULL
	drop procedure dbo.up_enc_bonusPorFornecedor
go

create procedure dbo.up_enc_bonusPorFornecedor
@ref	 char (18),
@forn	 numeric(10,0),
@estab	 numeric(3,0),
@site_nr tinyint 

/* with encryption */
AS

SET NOCOUNT ON

declare @data as datetime = getdate()

select
	bi.lobs as Bonus
from
	bi (nolock)
	inner join bo (nolock) on bi.bostamp=bo.bostamp
	inner join st (nolock) on bi.ref=st.ref
where
	bo.ndos=35 and bi.ref=@ref and bo.datafinal>=@data
	and bo.[no]=@forn and bo.estab=@estab
	and st.site_nr = @site_nr 

GO
Grant Execute on dbo.up_enc_bonusPorFornecedor to Public
Grant Control on dbo.up_enc_bonusPorFornecedor to Public
Go