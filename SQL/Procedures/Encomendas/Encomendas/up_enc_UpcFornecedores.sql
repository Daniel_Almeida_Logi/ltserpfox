-- exec up_enc_UpcFornecedores
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

If OBJECT_ID('[dbo].[up_enc_UpcFornecedores]') IS NOT NULL
	drop procedure dbo.up_enc_UpcFornecedores
Go

create procedure dbo.up_enc_UpcFornecedores

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON


	select 
		*
		 ,melhorPreco = dense_rank() over (partition by ref order by u_upc)
	from 
		(
		Select	
			id = ROW_NUMBER() over (partition by ref,no,estab order by fo.data desc,fo.usrhora desc)
			,fn.ref
			,fn.u_upc
			,fo.no
			,fo.estab
			,fo.data
			,fn.qtt
			,fo.usrhora
		From
			fo (nolock)
			Inner hash join cm1 (nolock) on fo.doccode=cm1.cm and cm1.FOLANSL = 1 and fo.doccode IN (55,101,102)
			inner hash join fn (nolock) on fo.fostamp = fn.fostamp and fn.u_upc != 0 and fn.ref!=''
		) x
	where
		x.id = 1

GO
Grant Execute On dbo.up_enc_UpcFornecedores to Public
Grant Control On dbo.up_enc_UpcFornecedores to Public
GO