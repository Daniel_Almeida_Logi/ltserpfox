-- exec up_enc_vendas '8168617'

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_enc_vendas]') IS NOT NULL
	drop procedure dbo.up_enc_vendas
go

create procedure dbo.up_enc_vendas
@ref char(18)

/* with encryption */
AS

SET NOCOUNT ON

declare @anoActual as datetime = YEAR(GETDATE())
declare @anoAnterior as datetime = YEAR(GETDATE()) - 1


SELECT
	slstamp
	,cm
	,ref
	,qtt
	,datalc
	,mes = MONTH(datalc)
	,ano = YEAR(datalc)
into
	#Enc_vendas
FROM
	sl (nolock)
where
	origem = 'FT'
	and ref = @ref
	and YEAR(datalc) between @anoAnterior and @anoActual
	and CM > 50

/* saidas do ano anterior */

select
	ano		= YEAR(GETDATE())-1
	,JAN	= (select ISNULL(SUM(qtt),0) From #Enc_vendas WHERE mes = 1  and ano = @anoAnterior)
	,FEV	= (select ISNULL(SUM(qtt),0) From #Enc_vendas WHERE mes = 2  and ano = @anoAnterior)
	,MAR	= (select ISNULL(SUM(qtt),0) From #Enc_vendas WHERE mes = 3  and ano = @anoAnterior)
	,ABR	= (select ISNULL(SUM(qtt),0) From #Enc_vendas WHERE mes = 4  and ano = @anoAnterior)
	,MAI	= (select ISNULL(SUM(qtt),0) From #Enc_vendas WHERE mes = 5  and ano = @anoAnterior)
	,JUN	= (select ISNULL(SUM(qtt),0) From #Enc_vendas WHERE mes = 6  and ano = @anoAnterior)
	,JUL	= (select ISNULL(SUM(qtt),0) From #Enc_vendas WHERE mes = 7  and ano = @anoAnterior)
	,AGO	= (select ISNULL(SUM(qtt),0) From #Enc_vendas WHERE mes = 8  and ano = @anoAnterior)
	,[SET]	= (select ISNULL(SUM(qtt),0) From #Enc_vendas WHERE mes = 9  and ano = @anoAnterior)
	,[OUT]	= (select ISNULL(SUM(qtt),0) From #Enc_vendas WHERE mes = 10 and ano = @anoAnterior)
	,NOV	= (select ISNULL(SUM(qtt),0) From #Enc_vendas WHERE mes = 11 and ano = @anoAnterior)
	,DEZ	= (select ISNULL(SUM(qtt),0) From #Enc_vendas WHERE mes = 12 and ano = @anoAnterior)

UNION ALL

/* saidas do ano currente */

select
	ano		= YEAR(GETDATE())
	,JAN	= (select ISNULL(SUM(qtt),0) From #Enc_vendas WHERE mes = 1  and ano = @anoActual)
	,FEV	= (select ISNULL(SUM(qtt),0) From #Enc_vendas WHERE mes = 2  and ano = @anoActual)
	,MAR	= (select ISNULL(SUM(qtt),0) From #Enc_vendas WHERE mes = 3  and ano = @anoActual)
	,ABR	= (select ISNULL(SUM(qtt),0) From #Enc_vendas WHERE mes = 4  and ano = @anoActual)
	,MAI	= (select ISNULL(SUM(qtt),0) From #Enc_vendas WHERE mes = 5  and ano = @anoActual)
	,JUN	= (select ISNULL(SUM(qtt),0) From #Enc_vendas WHERE mes = 6  and ano = @anoActual)
	,JUL	= (select ISNULL(SUM(qtt),0) From #Enc_vendas WHERE mes = 7  and ano = @anoActual)
	,AGO	= (select ISNULL(SUM(qtt),0) From #Enc_vendas WHERE mes = 8  and ano = @anoActual)
	,[SET]	= (select ISNULL(SUM(qtt),0) From #Enc_vendas WHERE mes = 9  and ano = @anoActual)
	,[OUT]	= (select ISNULL(SUM(qtt),0) From #Enc_vendas WHERE mes = 10 and ano = @anoActual)
	,NOV	= (select ISNULL(SUM(qtt),0) From #Enc_vendas WHERE mes = 11 and ano = @anoActual)
	,DEZ	= (select ISNULL(SUM(qtt),0) From #Enc_vendas WHERE mes = 12 and ano = @anoActual)


drop table #Enc_vendas

/*Option (optimize for (@ref unknown))*/

GO
Grant Execute on dbo.up_enc_vendas to Public
Grant Control on dbo.up_enc_vendas to Public
Go