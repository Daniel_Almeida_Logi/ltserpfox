/*
	Levantar designacao do produto
	
	exec up_encomendas_postal_getStDesign '8168617'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_encomendas_postal_getStDesign]') IS NOT NULL
	drop procedure dbo.up_encomendas_postal_getStDesign
go

create procedure dbo.up_encomendas_postal_getStDesign
	@ref      varchar(25)
	,@site_nr tinyint


/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select
	ref, design
from
    st (nolock)
where
     ref = @ref
	 and st.site_nr = @site_nr 
GO
Grant Execute on dbo.up_encomendas_postal_getStDesign to Public
Grant Control on dbo.up_encomendas_postal_getStDesign to Public
GO