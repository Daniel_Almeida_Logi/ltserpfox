/* VIEW QUE RETORNA TODAS AS REFERÊNCIAS E FORNECEDORES COM DOCUMENTO DE BONUS DOS ULTIMOS DOIS DIAS

	SELECT * FROM uv_encomendas_ultBonus(nolock)

*/

IF OBJECT_ID('[dbo].[uv_encomendas_ultBonus]') IS NOT NULL
	DROP VIEW dbo.uv_encomendas_ultBonus
GO

CREATE VIEW uv_encomendas_ultBonus AS

SELECT 
	ref,
	no
FROM (
	select 
		MAX(bo.dataobra) as dataUlDoc,
		bi.ref,
		bo.no
	from 
		bo(nolock) 
		join bi(nolock) on bi.bostamp = bo.bostamp
	where 
		bo.ndos = 35	
		AND bo.dataobra BETWEEN GETDATE() - 1 AND GETDATE()
	GROUP BY
		bi.ref,
		bo.no
	
	union all

	select 
		getdate() as dataUlDoc
		,bonus_d.ref
		,bonus.no_fl
	from bonus (nolock)
	inner join bonus_d(nolock)  on bonus_d.bonus_stamp = bonus.stamp
	where 1=1
		and ativo = 1
		and getdate() between bonus.begindate and bonus.endDate 
	
	) AS X






