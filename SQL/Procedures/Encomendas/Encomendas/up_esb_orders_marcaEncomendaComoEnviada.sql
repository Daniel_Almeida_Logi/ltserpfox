/*
	Atualizar estado de envio da encomenda

	exec up_esb_orders_marcaEncomendaComoEnviada 'ADM2978662A-51DF-4A1E-A1D', 8
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_esb_orders_marcaEncomendaComoEnviada]') IS NOT NULL
	drop procedure dbo.up_esb_orders_marcaEncomendaComoEnviada
go

create procedure dbo.up_esb_orders_marcaEncomendaComoEnviada
	@stamp varchar (25)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

update
	bo
set
	logi1 = 1
where
	bostamp = @stamp

GO
Grant Execute on dbo.up_esb_orders_marcaEncomendaComoEnviada to Public
Grant Control on dbo.up_esb_orders_marcaEncomendaComoEnviada to Public
GO