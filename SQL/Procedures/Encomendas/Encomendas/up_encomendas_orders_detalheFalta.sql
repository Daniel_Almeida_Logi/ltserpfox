/*
	Levantar detalhe da falta a comunicar
	
	
	exec up_encomendas_orders_detalheFalta '','20240728','20240728', 'Loja 1'
	teste:
	update bi set comunicada_desrc='', comunicada_sucesso = 0 where bostamp = 'FR690E900F-AA16-4EBE-ADD'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_encomendas_orders_detalheFalta]') IS NOT NULL
	drop procedure dbo.up_encomendas_orders_detalheFalta
go

create procedure dbo.up_encomendas_orders_detalheFalta
	@stamp			VARCHAR (50),
	@dataini		DATETIME,
	@datafim		DATETIME,
	@site           VARCHAR (20)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	DECLARE @sql VARCHAR(4000) = ''

	DECLARE @fornecs VARCHAR(250) = ''
	Declare @siteNr int = 0;

	select top 1 @siteNr=empresa.no from empresa(nolock) where site = @site
	select top 1 @fornecs = rtrim(ltrim(isnull(textValue,''))) from B_Parameters_site(nolock) where stamp='ADM0000000207'  and  site = @site


	SET @sql = @sql + N'
		SELECT
			ISNULL(bi.ref,'''')			  AS ref
			,COUNT(ISNULL(bi.qtt,0))      AS qtt
		
		FROM
			bi (NOLOCK)
			inner join bo(NOLOCK) on bo.bostamp = bi.bostamp
			INNER JOIN ts ON ts.ndos = bi.ndos
			INNER JOIN st ON st.ref = bi.ref
		WHERE
			bi.ref != ''''	
			and st.site_nr = '''+convert(varchar,@siteNr)+'''
			and isnumeric(bi.ref) = 1
			AND ts.codigoDoc in (''50'',''51'') --registo de faltas, reservas de cliente
			AND bi.comunicada_ws = 0
			AND bo.site = '''+@site+'''
			 '

			

	if @fornecs !=''
		SET @sql = @sql + N'  and isnull( convert(varchar(10),st.fornec),'''') in (	select isnull( convert(varchar(10),no),'''') from fl(nolock) where convert(varchar(10),no) in ( (Select items from dbo.up_splitToTable('''+@fornecs+''','';'')))) '
	
	IF @stamp != ''
		SET @sql = @sql + N' AND bi.bostamp = ''' + @stamp + ''' '

	IF @dataini != '' AND @datafim != ''
		SET @sql = @sql + N' AND bo.ousrdata BETWEEN '''+convert(varchar,@dataini)+''' AND
							 '''+convert(varchar,@datafim)+''''

	SET @sql = @sql + N'
		GROUP BY 
			bi.ref'
	
	PRINT @sql
	EXEC(@sql)

GO
Grant Execute on dbo.up_encomendas_orders_detalheFalta to Public
Grant Control on dbo.up_encomendas_orders_detalheFalta to Public
GO


