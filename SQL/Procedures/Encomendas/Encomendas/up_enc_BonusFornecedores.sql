/*

 exec up_enc_BonusFornecedores

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

If OBJECT_ID('[dbo].[up_enc_BonusFornecedores]') IS NOT NULL
	drop procedure dbo.up_enc_BonusFornecedores
Go

create procedure dbo.up_enc_BonusFornecedores

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON


Select *
From(
	Select 
		id = ROW_NUMBER() over (partition by ref order by bo.dataobra desc)
		,bi.ref
		,bonus = bi.lobs
		,bo.no
		,bo.estab
		,bo.dataobra
	from 
		bo (nolock)
		inner join bi (nolock) on bi.bostamp = bo.bostamp
	Where	
		bi.nmdos = 'Bonus Fornecedor'
		and MONTH(bo.dataobra) = MONTH(GETDATE())
		and YEAR(bo.dataobra) = YEAR(GETDATE())

) x
where x.id = 1

Option (recompile)		

GO
Grant Execute On dbo.up_enc_BonusFornecedores to Public
Grant Control On dbo.up_enc_BonusFornecedores to Public
GO