/*	Levantar dados de fornecedor
	
	exec up_encomendas_postal_dadosFornecedor 2098, 0, 2
	select * from fl_site

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_encomendas_postal_dadosFornecedor]') IS NOT NULL
	drop procedure dbo.up_encomendas_postal_dadosFornecedor
go

create procedure dbo.up_encomendas_postal_dadosFornecedor
	@no numeric(10,0)
	,@estab numeric(3,0)
	,@site_nr int

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select
	no
    ,estab
    ,nome
    ,moeda
    ,u_clpass = RTRIM(LTRIM(fl_site.clpass))
    ,u_idclfl = RTRIM(LTRIM(fl_site.idclfl))
    ,u_ipfl   = RTRIM(LTRIM(fl_site.ipfl))
    ,u_ipport = RTRIM(LTRIM(fl_site.ipport))
from
    fl (nolock)
    inner join fl_site on fl.no = fl_site.nr_fl and fl.estab = fl_site.dep_fl
where
     no = @no
     and estab = @estab
     and fl_site.site_nr = @site_nr

GO
Grant Execute on dbo.up_encomendas_postal_dadosFornecedor to Public
Grant Control on dbo.up_encomendas_postal_dadosFornecedor to Public
GO