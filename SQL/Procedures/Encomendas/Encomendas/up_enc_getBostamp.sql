/* 
	exec up_enc_getBostamp 'ADM6B72B529-DE6C-42C8-8AF'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_enc_getBostamp]') IS NOT NULL
	drop procedure dbo.up_enc_getBostamp
go

create procedure dbo.up_enc_getBostamp
@ftstamp as varchar(25)

/* WITH ENCRYPTION */
AS
	;with cte as (
					select distinct bistamp 
							,(select bostamp from bi (nolock) where bi.bistamp=fi.bistamp) as bostamp 
					from fi (nolock) 
					where ftstamp=@ftstamp
							and (select nmdos from bi (nolock) where bi.bistamp=fi.bistamp)='Encomenda de cliente')
	select distinct bostamp from cte		

GO
Grant Execute On dbo.up_enc_getBostamp to Public
Grant Control On dbo.up_enc_getBostamp to Public
Go

