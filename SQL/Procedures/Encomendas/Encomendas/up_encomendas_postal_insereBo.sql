/*	Insere Cabecalho BO
	
	exec up_encomendas_postal_insereBo 'asdd123', '54664', 35, 0, 'asdasd', 98, 'EURO', '20150828', 'Loja 1'
	
	select datafinal,* From bo where obrano = '54664'
	
	delete from bo where obrano = '54664'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_encomendas_postal_insereBo]') IS NOT NULL
	drop procedure dbo.up_encomendas_postal_insereBo
go

create procedure dbo.up_encomendas_postal_insereBo
	@bostamp varchar(25)
	,@obrano numeric(10,0)
    ,@ndos numeric(3,0)
    ,@estab numeric(3,0)
	,@nome varchar(80)
	,@no numeric(10,0)
	,@moeda varchar(11)
    ,@dataExpira varchar(8)
    ,@site varchar(60)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

INSERT INTO bo
	(
	bostamp, nmdos, obrano
	,ndos,estab,nome,no,tipo, boano, dataobra,moeda
	,ousrinis, ousrdata, ousrhora,usrinis, usrdata, usrhora
	,datafinal,site, fechada 
	)
Select
	@bostamp
	,'Bonus Fornecedor'
	,@obrano
    ,@ndos
    ,@estab
	,@nome
	,@no
	,'Activo', datepart(year,getdate()), convert(varchar,getdate(),102)
	,@moeda
    ,'Adm', convert(varchar,getdate(),102), substring(convert(char(25),getdate(),113),13,8)
    ,'Adm', convert(varchar,getdate(),102), substring(convert(char(25),getdate(),113),13,8)
    ,(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@dataExpira)+1,0)))
    ,@site
    ,1
   

GO
Grant Execute on dbo.up_encomendas_postal_insereBo to Public
Grant Control on dbo.up_encomendas_postal_insereBo to Public
GO