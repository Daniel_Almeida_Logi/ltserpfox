/* Encomenda Super Automática
          
 exec up_enc_dadosRefConfEnc '1001887' , '1'

 */
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_enc_dadosRefConfEnc]') IS NOT NULL
	drop procedure dbo.up_enc_dadosRefConfEnc
go

create procedure dbo.up_enc_dadosRefConfEnc

	@ref varchar(13)
	,@site_nr tinyint

/* with encryption */
AS

/* Acrescentado a 26-01-2012 colocação de informação sobre as margens regressivas Marcelino Teixeira */
;With cte1(versao,escalao,PVAmin,PVAmax,PVPmin,PVPmax,mrgGross,mrgGrossTipo,mrgFarm,FeeFarm,feeFarmCiva,txCom,factorPond) as (
	Select	versao,escalao,PVAmin,PVAmax,PVPmin,PVPmax,mrgGross,mrgGrossTipo,mrgFarm,FeeFarm,feeFarmCiva,txCom,factorPond
	from	b_mrgRegressivas (nolock)
)


select
	 'tabiva'		= tabiva
	,'validade'		= ltrim(right(convert(varchar(10),st.validade,104),7))
	,'taxiva'		= isnull(taxasiva.taxa,0.00)
	,'epv1'			= st.epv1
	,'epcusto'		= st.epcusto
	,'bb'			= st.marg4
	,'marg1'		= st.marg1
	,'stock'		= st.stock
	,'design'		= st.design
	,'pvpmax'		= ISNULL(fprod.pvpmax,0)
	,'pvpdic'		= isnull(fprod.pvpcalc,0)
	,'pvporig'		= ISNULL(fprod.pvporig,0)
	,'local'		= case 
						when u_local = '' then local
						else local + ',' + u_local
						end,
	isnull((Select top 1 escalao from cte1 Where pvporig between PVPmin and PVPmax order by versao desc),0) as escalao,
	isnull((Select top 1 mrgFarm from cte1 Where pvporig between PVPmin and PVPmax order by versao desc),0) as mrgFarm,
	isnull((Select top 1 feeFarm from cte1 Where pvporig between PVPmin and PVPmax order by versao desc),0) as feeFarm
	,'pvpmaxre'		= isnull(fprod.pvpmaxre,0)
from
	st (nolock)
	left join fprod (nolock) on fprod.cnp=st.ref 
	left join bc (nolock) on bc.ref=st.ref and bc.site_nr = @site_nr
	left join taxasiva (nolock) on taxasiva.codigo=st.tabiva /*AND taxasiva.codigo<=4*/
where
	st.ref=@ref
	OR bc.codigo=@ref
	OR st.codigo=@ref
	and st.site_nr = @site_nr 

GO
Grant Execute on dbo.up_enc_dadosRefConfEnc to Public
Grant Control on dbo.up_enc_dadosRefConfEnc to Public
Go