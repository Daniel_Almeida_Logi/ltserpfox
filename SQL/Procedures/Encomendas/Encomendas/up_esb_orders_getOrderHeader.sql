/*
	Levantar detalhe da encomenda a enviar
	
	exec up_esb_orders_getOrderHeader 'ADM468C3252-3DE1-4BA4-B14'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_esb_orders_getOrderHeader]') IS NOT NULL
	drop procedure dbo.up_esb_orders_getOrderHeader
go

create procedure dbo.up_esb_orders_getOrderHeader
	@stamp varchar (25)

/* WITH ENCRYPTION */
AS

	SET NOCOUNT ON

	SELECT
		 bo.boano + '_' + bo.obrano AS id -- 2020_1
		,bo.bostamp					AS bostamp
		,bo.boano					AS boano
		,bo.obrano					AS obrano
		,bo.ndos					AS ndoss
		,bo.ousrdata				AS ousrdata
		,bo.moeda					AS moeda
		,bo.etotal					AS etotal
		,u_dataentr					AS dataEntr
		,1							AS toleraData
		,0							AS tipoEntrega -- 0 diurna 
		,1							AS recebePDF
		,1							AS tipoEncomenda -- Instantânea
		,30							AS prazoPagamento -- numero de dias a seguir a factura
		,''							AS distribuidorCod -- codigo do distribuidor 
	FROM
		bo (NOLOCK)
	WHERE
		 bo.bostamp = @stamp

GO
Grant Execute on dbo.up_esb_orders_getOrderHeader to Public
Grant Control on dbo.up_esb_orders_getOrderHeader to Public
GO