/*
	Levantar detalhe da encomenda a enviar
	
	exec up_esb_orders_getOrderLines 'ADM468C3252-3DE1-4BA4-B14'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_esb_orders_getOrderLines]') IS NOT NULL
	drop procedure dbo.up_esb_orders_getOrderLines
go

create procedure dbo.up_esb_orders_getOrderLines
	@stamp varchar (25)

/* WITH ENCRYPTION */
AS

	SET NOCOUNT ON

	Select
		bi.bostamp						AS bostamp
		,bi.ref							AS ref
		,bi.design						AS design
		,CONVERT(INT,bi.edebito *100)	AS edebito -- B2B RECEBE O VALOR INT EM QUE 10,60 FICA   1060 
		, case
				when qtt > u_bonus
				then qtt - u_bonus
				else qtt
			end							AS qtt
		,bi.u_bonus						AS u_bonus
		,bi.desconto					AS desconto
		,bi.desc2						AS desc2
		,bi.desc3						AS desc3
		,bi.iva							AS iva
		,bi.ettdeb						AS ettdeb
		,0								AS oferta
		,'1'							AS confirmar -- para solicitar confirmacao da encomenda
		,''								AS CodigoCampanha
	from
		bi (nolock)
	where
		bi.ref != ''
		and bi.bostamp = @stamp

GO
Grant Execute on dbo.up_esb_orders_getOrderLines to Public
Grant Control on dbo.up_esb_orders_getOrderLines to Public
GO