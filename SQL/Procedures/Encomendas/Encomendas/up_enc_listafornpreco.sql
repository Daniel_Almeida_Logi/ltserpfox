-- Lista de Fornecedores com Pre�o
-- exec up_enc_listafornpreco '8168617'

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_enc_listafornpreco]') IS NOT NULL
	drop procedure dbo.up_enc_listafornpreco
go

create procedure dbo.up_enc_listafornpreco
@ref char(18)

/* with encryption */
AS

	SELECT
		 nome
		,evu
		,no
	into
		#enc_listafornpreco
	FROM
	(
		select
			sl.nome,
			'id' = ROW_NUMBER() over (partition by sl.nome order by sl.usrdata+sl.usrhora desc),
			sl.evu,
			fl.tipo,
			fl.no
		from
			sl (nolock)
			inner join fl (nolock) on sl.frcl=fl.no
		where 
			sl.ref=@ref
			and sl.cm<50
			and fl.tipo = 'Activo'
	) as X
	where
		x.id=1

	select
		nome, evu = convert(varchar,convert(decimal(6,2), evu))
	from
		#enc_listafornpreco

	union all

	select
		fl.nome, evu='0'
	from
		fl (nolock)
	where
		tipo='Activo' and fl.nome not in (select nome from #enc_listafornpreco)
	order by
		evu desc, nome asc

	Drop table #enc_listafornpreco

GO
Grant Execute on dbo.up_enc_listafornpreco to Public
Grant Control on dbo.up_enc_listafornpreco to Public
Go