/*
	Atualizar estado de envio da encomenda
	
	exec up_encomendas_postal_dadosOperador 1
	exec up_encomendas_postal_dadosOperador 1, "Loja 1"
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_encomendas_postal_dadosOperador]') IS NOT NULL
	drop procedure dbo.up_encomendas_postal_dadosOperador
go

create procedure dbo.up_encomendas_postal_dadosOperador
	@userno numeric(6,0)
	,@site varchar(20) = ""

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select
	iniciais, userno, username
	,u_supermsg = supermsg
from
   b_us (nolock)
where
	(userno=@userno or supermsg=1)
	and loja = case when @site="" then loja else @site end

GO
Grant Execute on dbo.up_encomendas_postal_dadosOperador to Public
Grant Control on dbo.up_encomendas_postal_dadosOperador to Public
GO


