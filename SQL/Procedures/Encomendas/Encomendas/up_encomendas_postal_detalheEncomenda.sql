/*
	Levantar detalhe da encomenda a enviar
	
	exec up_encomendas_postal_detalheEncomenda 'ADM2978662A-51DF-4A1E-A1D'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_encomendas_postal_detalheEncomenda]') IS NOT NULL
	drop procedure dbo.up_encomendas_postal_detalheEncomenda
go

create procedure dbo.up_encomendas_postal_detalheEncomenda
	@stamp varchar (25)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

Select
	bi.bostamp
	,bi.ref
	,qtt = case
			when qtt > u_bonus
			then qtt - u_bonus
			else qtt
		end
from
    bi (nolock)
where
	bi.ref != ''
	and bi.bostamp = @stamp

GO
Grant Execute on dbo.up_encomendas_postal_detalheEncomenda to Public
Grant Control on dbo.up_encomendas_postal_detalheEncomenda to Public
GO