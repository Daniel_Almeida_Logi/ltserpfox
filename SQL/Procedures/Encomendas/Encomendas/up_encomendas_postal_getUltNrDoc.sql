/*
	Levantar �ltimo n�mero de documento

	exec up_encomendas_postal_getUltNrDoc 35,2014
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_encomendas_postal_getUltNrDoc]') IS NOT NULL
	drop procedure dbo.up_encomendas_postal_getUltNrDoc
go

create procedure dbo.up_encomendas_postal_getUltNrDoc
	@ndos numeric(3,0)
	,@boano numeric(4,0)
	
/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select
	isnull(max(obrano),0) as obrano
from
	bo (nolock)
where
	ndos=@ndos
	and
	boano=@boano

GO
Grant Execute on dbo.up_encomendas_postal_getUltNrDoc to Public
Grant Control on dbo.up_encomendas_postal_getUltNrDoc to Public
GO