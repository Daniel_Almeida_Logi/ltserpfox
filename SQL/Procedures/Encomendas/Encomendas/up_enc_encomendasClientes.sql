/*
	Encomenda Cliente

	exec up_enc_encomendasClientes '', 1, '1'
*/
if OBJECT_ID('[dbo].[up_enc_encomendasClientes]') IS NOT NULL
	drop procedure dbo.up_enc_encomendasClientes
go

create procedure dbo.up_enc_encomendasClientes
	@tipoUtente varchar(max)
	,@armazem   numeric(5,0)
	,@site_nr   tinyint

/* with encryption */
AS

declare
	 @Mes datetime = MONTH(GETDATE())
	,@Ano datetime = YEAR(GETDATE())


-- Clientes
select
	no
	,estab
into
	#temp_clientes
From
	B_utentes (nolock)
Where
	B_utentes.tipo in (select items from up_SplitToTable(@tipoUtente,','))


-- Bonus
If OBJECT_ID('tempdb.dbo.#temp_bonus') IS NOT NULL
	drop table #temp_bonus;

Select
	*
into
	#temp_bonus
From(
	Select
		contagem = ROW_NUMBER() over (partition by ref order by ref)
		,id = ROW_NUMBER() over (partition by ref,bo.no,bo.estab order by bo.dataobra desc,bo.usrhora desc)
		,bi.ref
		,bonus = bi.lobs
		,bo.no
		,bo.estab
		,bo.dataobra
	from
		[dbo].[bo] (nolock)
		inner join [dbo].[bi] (nolock) on bi.bostamp = bo.bostamp and bo.ndos = 35
	Where
		bo.ndos = 35
		and bo.boano = @Ano
		and MONTH(bo.dataobra) = @Mes
		and bi.armazem = case when @armazem=0 then bi.armazem else @armazem end
) x
where
	x.id = 1

-- Esgotados
If OBJECT_ID('tempdb.dbo.#temp_Esgotados') IS NOT NULL
	drop table #temp_Esgotados;

select
	*
into
	#temp_Esgotados
from (
	Select	
		 contagem = ROW_NUMBER() over (partition by ref order by ref)
		,id = ROW_NUMBER() over (partition by ref,fo.no,fo.estab order by fo.data desc,fo.usrhora desc)
		,fn.ref
		,fn.u_upc
		,fo.no
		,fo.estab
		,fo.data
		,fn.qtt
		,fo.usrhora
	From
		fo (nolock)
		inner hash join fn (nolock) on fn.fostamp = fo.fostamp 
	where
		fo.doccode IN (55,101,102)
		and fo.docdata >= dateadd(year,-1,getdate())
		and fn.armazem = case when @armazem=0 then fn.armazem else @armazem end
		and fn.ref != ''

) x
where
	x.id = 1
	and x.qtt=0 

--  5 mais baratos
If OBJECT_ID('tempdb.dbo.#temp_Existencia5Baratos') IS NOT NULL
	drop table #temp_Existencia5Baratos;

select
	existeNos5Baratos = sum(ranking)
	,fprod.grphmgcode
into
	#temp_Existencia5Baratos
from 
	[dbo].[st] (nolock)
	inner join [dbo].[fprod] (nolock) on st.ref=fprod.cnp and (fprod.grphmgcode!='GH0000' or fprod.grphmgcode!='')
where
	ranking = 1
	and st.site_nr = @site_nr 
group by
	grphmgcode

-- Stock por Grupo Homog�neo
If OBJECT_ID('tempdb.dbo.#temp_GH') IS NOT NULL
	drop table #temp_GH;

Select
	 stock = sum(isnull(sa.stock,st.stock))
	,grphmgcode
into 
	#temp_GH
From
	[dbo].[st] (nolock)
	inner join [dbo].[fprod] (nolock) on st.ref = fprod.cnp
	left join [dbo].[sa] (nolock) on st.ref = sa.ref and sa.armazem = @armazem
where
	grphmgcode!='GH0000' and grphmgcode!=''
	and st.site_nr = @site_nr 
Group by
	grphmgcode

-- benef�cio Bruto
If OBJECT_ID('tempdb.dbo.#temp_MelhorBB') IS NOT NULL
	drop table #temp_MelhorBB;

select
	*
into 
	#temp_MelhorBB
from (
		select
			id = ROW_NUMBER() over (partition by fprod.grphmgcode order by fprod.grphmgcode, st.marg4 desc)
			,st.ref
			,st.design
			,epv1
			,epreco = fprod.pvporig
			,fprod.pvpmaxre
			,fprod.ranking
			,fprod.grphmgcode
			,st.marg4
		from
			[dbo].[st] (nolock)
			inner join [dbo].[fprod] (nolock) on st.ref=fprod.cnp and (fprod.grphmgcode!='GH0000' or fprod.grphmgcode!='')
		where
			ranking = 1
			and st.site_nr = @site_nr 
	) as x
where
	x.id = 1

-- PCL de Fornecedor
If OBJECT_ID('tempdb.dbo.#temp_PCLForn') IS NOT NULL
	drop table #temp_PCLForn;

select
	*
	,melhorPreco = dense_rank() over (partition by ref order by u_upc)
into
	#temp_PCLForn
from (
	Select	
		id = ROW_NUMBER() over (partition by ref,no,estab order by fo.data desc,fo.usrhora desc)
		,fn.ref
		,fn.u_upc
		,fo.no
		,fo.estab
		,fo.data
		,fn.qtt
		,fo.usrhora
		,fn.u_stockact
	From
		[dbo].[fo] (nolock)
		inner join [dbo].[fn] (nolock) on fo.fostamp = fn.fostamp 
	where
		fo.doccode IN (55,101,102)
		and fo.docdata >= dateadd(year,-1,getdate())
		and fn.u_upc != 0 and fn.ref != ''
		and fn.armazem = case when @armazem=0 then fn.armazem else @armazem end
) x
where
	x.id = 1

If OBJECT_ID('tempdb.dbo.#temp_ST') IS NOT NULL
	drop table #temp_ST;

select
	*
into
	#temp_ST
from (
	Select
		st.ststamp
		,st.ref
		,st.codigo
		,st.design
		,encomendar		= qttcli - qttfor
		,qtt			= 0
		,qttfor			= 0
		,stock			= st.stock
		,st.ptoenc
		,st.stmin
		,st.stmax
		,st.eoq
		,qttcli			= st.qttcli
		,st.qttacin
		,epcusto
		,epcult
		,st.epcpond
		,conversao
		,cpoc
		,familia
		,faminome
		,u_lab
		,u_fonte
		/*,u_esgotado*/
		,fornec
		,fornecedor
		,fornestab
		/*,u_esgoflno*/
		/*,u_esgoflnm*/
		,tabiva
		,iva			= ISNULL(taxasiva.taxa,0)
		,generico		= isnull(fprod.generico,0)
		,componente		= 0
		,grphmgcode
		,marg4
		,marca = usr1
		,psico
		,benzo
	from
		[dbo].[st] (nolock)
		left join [dbo].[fprod] (nolock) on fprod.cnp=st.ref
		left join [dbo].[taxasiva] (nolock) on taxasiva.codigo=st.tabiva
		--left join [dbo].[sa] (nolock) on st.ref = sa.ref and sa.armazem = @armazem
	where
		st.inactivo = 0
		and st.stns = 0
		and st.site_nr = @site_nr 

)x


select 
	pesquisa		= CONVERT(bit,0)
	,ststamp		= st.ststamp
	,ENC			= convert(bit,0)
	,u_fonte		= st.u_fonte
	,ref			= isnull(st.ref,'')
	,refb			= st.REF
	--,qttb			= case when SUM(bi.qtt) > st.stock then SUM(bi.qtt) - st.stock else SUM(bi.qtt) end
	,qttb			= SUM(bi.qtt)
	,codigo			= st.codigo
	,DESIGN			= ltrim(st.design)
	,Stock			= convert(int,st.stock)
	,STMIN			= st.stmin
	,STMAX			= st.stmax
	,PTOENC			= st.ptoenc
	--,EOQ			= case when SUM(bi.qtt) > st.stock then SUM(bi.qtt) - st.stock else SUM(bi.qtt) end
	,EOQ			= SUM(bi.qtt)
	,qtBonus		= 0
	,qttadic		= st.eoq
	,qttfor			= st.qttfor
	,qttacin		= st.qttacin
	,qttcli			= st.qttcli
	,fornecedor		= st.fornecedor
	,fornec			= st.fornec
	,fornestab		= st.fornestab
	,SEL			= CONVERT(bit,0)
	,epcusto		= st.epcusto
	,epcpond		= st.epcpond
	,epcult			= st.epcult
	,tabiva			= st.tabiva
	,iva			= taxasiva.taxa
	,cpoc			= st.cpoc
	,familia		= st.familia
	,faminome		= st.faminome
	,u_lab			= st.u_lab
	,conversao		= st.conversao
	-- colunas sobre os esgotados
	,bonusFornec	= isnull(Bonus.bonus,'')--''
	,pclFornec		= isnull(#temp_PCLForn.u_upc,0)
	-- � esgotado se o ultimo documento do fornecedor tiver a qtt = 0, no caso de n�o ter fornecedor associado n�o aparece como esgotado
	,Esgotado		= 'N�O'
	,ordem			= 0
	,generico		= st.generico
	,componente
	,stockGH		= isnull(#temp_GH.stock,0)
	,st.marg4
	/*BB do mesmo grupo Homogeneo*/
	,alertaStockGH		= case 
							when (st.grphmgcode='GH0000' or st.grphmgcode='' or st.grphmgcode is null or st.marg4 = 0) then 0 
							when #temp_MelhorBB.ref is not null then 0 
							when #temp_Existencia5Baratos.grphmgcode is null then 0
							else 1 
						end
	,alertaPCL			= case when #temp_PCLForn.melhorPreco = 1 or #temp_PCLForn.melhorPreco is null then 0 else 1 end 
	,alertaBonus		= case when Bonus2.ref is not null and Bonus.ref is null then 1 else 0 end
	,st.grphmgcode
	,#temp_MelhorBB.ref as melhorbb
	,#temp_Existencia5Baratos.grphmgcode
	,st.marca
	,BBfornec  = convert(numeric(9,2),0)
	,mbpcfornec = convert(numeric(9,2),0)
	,qtprevista = convert(numeric(9,2),0)
from 
	[dbo].[bo] (nolock) 
	inner join #temp_clientes cl on bo.no = cl.no and bo.estab = cl.estab 
	inner join [dbo].[bi] (nolock) on bo.bostamp = bi.bostamp
	inner join #temp_st as st on bi.ref = st.ref
	left join fprod (nolock) on fprod.cnp=st.ref
	left join #temp_Bonus as Bonus		on Bonus.ref = st.ref and st.fornec = Bonus.no and st.fornestab = Bonus.estab
	left join #temp_Bonus as Bonus2		on Bonus2.ref = st.ref and Bonus2.contagem = 1
	left join #temp_PCLForn				on #temp_PCLForn.ref = st.ref and st.fornec = #temp_PCLForn.no and st.fornestab = #temp_PCLForn.estab 
	left join #temp_Esgotados			on #temp_Esgotados.ref = st.ref and st.fornec = #temp_Esgotados.no and st.fornestab = #temp_Esgotados.estab 
	left join taxasiva					on taxasiva.codigo = st.tabiva
	left join #temp_GH					on #temp_GH.grphmgcode = st.grphmgcode
	left join #temp_MelhorBB			on st.ref = #temp_MelhorBB.ref
	left join #temp_Existencia5Baratos	on st.grphmgcode = #temp_Existencia5Baratos.grphmgcode
where
	bo.ndos = 41
	AND bo.fechada = 0
group by
	st.ststamp
	,st.u_fonte
	,st.ref
	,st.codigo
	,st.design
	,st.stock
	,st.stmin
	,st.stmax
	,st.ptoenc
	,st.eoq
	,st.qttfor
	,st.qttacin
	,st.qttcli
	,st.fornecedor
	,st.fornec
	,st.fornestab
	,st.epcusto
	,st.epcpond
	,st.epcult
	,st.tabiva
	,taxasiva.taxa
	,st.cpoc
	,st.familia
	,st.faminome
	,st.u_lab
	,st.conversao
	,Bonus.bonus
	,st.marg4
	,#temp_PCLForn.u_upc
	,st.generico
	,componente
	,#temp_GH.stock
	,st.grphmgcode
	,#temp_MelhorBB.ref
	,#temp_Existencia5Baratos.grphmgcode
	,#temp_PCLForn.melhorPreco
	,Bonus2.ref
	,Bonus.ref
	,st.grphmgcode
	,st.marca
	,Bonus.bonus
--Having
	--case when SUM(bi.qtt) > st.stock then SUM(bi.qtt) else SUM(bi.qtt) - st.stock end > 0	


If OBJECT_ID('tempdb.dbo.#temp_bonus') IS NOT NULL
	drop table #temp_bonus;

If OBJECT_ID('tempdb.dbo.#temp_sc') IS NOT NULL
	drop table #temp_sc;

If OBJECT_ID('tempdb.dbo.#temp_MelhorBB') IS NOT NULL
	drop table #temp_MelhorBB;

If OBJECT_ID('tempdb.dbo.#temp_GH') IS NOT NULL
	drop table #temp_GH;

If OBJECT_ID('tempdb.dbo.#temp_Existencia5Baratos') IS NOT NULL
	drop table #temp_Existencia5Baratos;

If OBJECT_ID('tempdb.dbo.#temp_Esgotados') IS NOT NULL
	drop table #temp_Esgotados;

If OBJECT_ID('tempdb.dbo.#temp_PCLForn') IS NOT NULL
	drop table #temp_PCLForn;

If OBJECT_ID('tempdb.dbo.#temp_ST') IS NOT NULL
	drop table #temp_ST;		

GO
Grant Execute on dbo.up_enc_encomendasClientes to Public
Grant Control on dbo.up_enc_encomendasClientes to Public
Go