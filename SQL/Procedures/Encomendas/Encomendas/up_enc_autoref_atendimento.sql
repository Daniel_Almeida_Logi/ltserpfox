/*
	Encomenda Automática para uma Referência sem Crítérios de Stock

	exec up_enc_autoref_atendimento 1, '5354311', '', 1, 0
	exec up_enc_autoref_atendimento 1,'5350889', 1
	exec up_enc_autoref_atendimento 1, '5440987', '', 1, 0
*/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_enc_autoref_atendimento]') IS NOT NULL
	drop procedure dbo.up_enc_autoref_atendimento
go

create procedure dbo.up_enc_autoref_atendimento
	@armazem numeric(5,0)
	,@ref varchar(254) = ''
	,@grupo varchar(20) = '' -- '' = Individual, [id armazem] = Grupo
	,@site_nr tinyint
	,@conjunta bit = 0 

/* with encryption */
AS

SET NOCOUNT ON

declare
	 @Mes datetime = MONTH(GETDATE())
	,@Ano datetime = YEAR(GETDATE())
	,@validaStockMaxZero  bit = 0

	--valida se deve excluir o stock max <= 0
	SELECT @validaStockMaxZero=bool FROM B_Parameters  WHERE stamp = 'ADM0000000275'

-- Se a encomenda for de grupo garantir que todas as fichas dos produtos existentes nas cond comerc. existem na BD
if @grupo != ''
begin
	-- guardar linkedserver e dados fonecedor grupo
	declare @sql nvarchar(1024), @sql2 nvarchar(2048)
	declare @server varchar(30), @no numeric(10), @estab numeric(3), @nome varchar(55)

	select top 1
		@server=ccc.odbc
		,@no=ccc.no
		,@estab=ccc.estab
		,@nome=fl.nome
	from condComercConfig ccc (nolock) inner join fl (nolock) on fl.no=ccc.no and fl.estab=ccc.estab
	where bdname=@grupo and sede=1

	set @sql2 = N'
		insert into st
			(ststamp, ref, design
			,tabiva, ivaincl, iva1incl, iva2incl, iva3incl, iva4incl, iva5incl
			,u_impetiq, u_tipoetiq
			,fornec, fornecedor
			,u_lab, familia, faminome
			,site_nr
			,ousrinis, ousrdata, ousrhora)
		select
			''ADM'' + left(replace(newid(),''-'',''''), 21)
			,st.ref
			,st.design
			,tabiva	= st.tabiva
			,ivaincl = 1
			,iva1incl = 1
			,iva2incl = 1
			,iva3incl = 1
			,iva4incl = 1
			,iva5incl = 1
			,u_impetiq = case when isnull(fprod.pvporig,0) = 0 then 1 else 0 end
			,u_tipoetiq = 1
			,fornec = isnull((select top 1 no from condComercConfig where sede=1),0)
			,fornecedor = isnull((select nome from fl where fl.no = (select top 1 no from condComercConfig where sede=1)),'''')
			,u_lab = isnull(st.u_lab,isnull(fprod.titaimdescr,''''))
			,familia = st.familia
			,faminome = st.faminome
			,site_nr = ' + ltrim(str(@site_nr)) + '
			,ousrinis = ''ADM''
			,ousrdata = convert(datetime,getdate())
			,ousrhora = convert(time,getdate(),102)
		from
			' + @server + '.' + @grupo + '.dbo.bi
			inner join ' + @server + '.' + @grupo + '.dbo.bo on bo.bostamp=bi.bostamp
			inner join ' + @server + '.' + @grupo + '.dbo.st on st.ref=bi.ref
			left join fprod (nolock) on fprod.cnp=bi.ref
		where
			bo.ndos=45
			and bo.fechada=0
			and bi.ref not in (select ref from st (nolock))
		Group by
			st.ref
			,st.design
			,st.tabiva
			,st.u_lab
			,fprod.pvporig
			,fprod.titaimdescr
			,st.familia
			,st.faminome	
		'
	execute sp_executesql @sql2	
end

Select 
		distinct ref 
	INTO
		#TempRefAt
	From
		st (nolock)
	Where
		ref in (Select items from dbo.up_splitToTable(@ref,','))
		and site_nr=@site_nr

-- Bonus
If OBJECT_ID('tempdb.dbo.#temp_bonus') IS NOT NULL
	drop table #temp_bonus;

Select
	*
into
	#temp_bonus
From (
	Select
		contagem = ROW_NUMBER() over (partition by ref order by ref)
		,id = ROW_NUMBER() over (partition by ref,bo.no,bo.estab order by bo.dataobra desc,bo.usrhora desc)
		,bi.ref
		,bonus = bi.lobs
		,bo.no
		,bo.estab
		,bo.dataobra
	from
		[dbo].[bo] (nolock)
		inner join [dbo].[bi] (nolock) on bi.bostamp = bo.bostamp
		inner join [dbo].[bo2] (nolock) on bo2.bo2stamp = bo.bostamp
	Where
		bo.ndos = 35
		and bo.boano = @Ano
		and MONTH(bo.dataobra) = @Mes
		and bi.armazem = case when @armazem=0 then bi.armazem else @armazem end
		and bo2.status=''
) x
where 
	x.id = 1


-- Detalhe composto/componente
If OBJECT_ID('tempdb.dbo.#temp_sc') IS NOT NULL
	drop table #temp_sc;

select
	ref, refb, qtt
into 
	#temp_sc
from
	sc

-- BB
If OBJECT_ID('tempdb.dbo.#temp_MelhorBB') IS NOT NULL
	drop table #temp_MelhorBB;

select
	*
into 
	#temp_MelhorBB
from (
		select
			id = ROW_NUMBER() over (partition by fprod.grphmgcode order by fprod.grphmgcode, st.marg4 desc)
			,st.ref
			,st.design
			,epv1
			,epreco = fprod.pvporig
			,fprod.pvpmaxre
			,fprod.ranking
			,fprod.grphmgcode
			,st.marg4
		from
			[dbo].[st] (nolock)
			inner join [dbo].[fprod] (nolock) on st.ref=fprod.cnp 
		where
			ranking = 1
			and (st.site_nr = @site_nr or @conjunta = 1) 
			and (fprod.grphmgcode!='GH0000' or fprod.grphmgcode!='')
	) as x
where
	x.id = 1

	 --select ref, * from #temp_MelhorBB where grphmgcode = 'GH0851'

-- Stock por Grupo Homogéneo
If OBJECT_ID('tempdb.dbo.#temp_GH') IS NOT NULL
	drop table #temp_GH;
	
Select
	 stock = sum(isnull(sa.stock,st.stock))
	,grphmgcode
into 
	#temp_GH
From
	[dbo].[st] (nolock)
	inner join [dbo].[fprod] (nolock) on st.ref = fprod.cnp
	left join [dbo].[sa] (nolock) on st.ref = sa.ref and sa.armazem = @armazem
where
	grphmgcode!='GH0000' and grphmgcode!=''
	and (st.site_nr = @site_nr or @conjunta = 1)
Group by
	grphmgcode


/*  todos os compostos que só têm 1 componente, encomendar composto */
If OBJECT_ID('tempdb.dbo.#temp_Existencia5Baratos') IS NOT NULL
	drop table #temp_Existencia5Baratos;

select
	existeNos5Baratos = sum(ranking)
	,fprod.grphmgcode
into
	#temp_Existencia5Baratos
from 
	[dbo].[st] (nolock)
	inner join [dbo].[fprod] (nolock) on st.ref=fprod.cnp 
where
	ranking = 1
	and (st.site_nr = @site_nr or @conjunta = 1)
	and (fprod.grphmgcode!='GH0000' or fprod.grphmgcode!='')
group by
	grphmgcode


If OBJECT_ID('tempdb.dbo.#temp_Esgotados') IS NOT NULL
	drop table #temp_Esgotados;	

select
	* 
into
	#temp_Esgotados
from (
	Select	
		contagem = ROW_NUMBER() over (partition by ref order by ref)
		,id = ROW_NUMBER() over (partition by ref,fo.no,fo.estab order by fo.data desc,fo.usrhora desc)
		,fn.ref
		,fn.u_upc
		,fo.no
		,fo.estab
		,fo.data
		,fn.qtt
		,fo.usrhora
	From
		fo (nolock)
		inner hash join fn (nolock) on fn.fostamp = fo.fostamp 
	where
		fo.doccode IN (55,101,102)
		and fo.docdata >= dateadd(year,-1,getdate())
		and fn.armazem = case when @armazem=0 then fn.armazem else @armazem end
		and fn.ref != ''
	) x
where
	x.id = 1
	and x.qtt=0 
	

/*
	Notas:
		.Os produtos a encomendar e o PCL no caso de ser uma encomenda de grupo 
		deve incluir apenas os produtos/PCLs do grupo (cond comerciais)
		.Todos os compostos que só têm 1 componente, encomendar composto
*/
-- PCL do Fornecedor
If OBJECT_ID('tempdb.dbo.#temp_PCLForn') IS NOT NULL
	drop table #temp_PCLForn;

create table #temp_PCLForn(
	[id] [int] NOT NULL DEFAULT(0)
	,[ref] [char](18) NOT NULL DEFAULT ('')
	,[u_upc] [numeric](13,2) NOT NULL DEFAULT(0)
	,[no] [numeric](10,0) NOT NULL DEFAULT(0)
	,[estab] [numeric](3,0) NOT NULL DEFAULT(0)
	,[data] [datetime] NOT NULL DEFAULT(GETDATE())
	,[qtt] [numeric](12,3) NOT NULL DEFAULT(0)
	,[usrhora] [varchar](8) NOT NULL DEFAULT('')
	,[u_stockact] [numeric](14) NOT NULL DEFAULT(0)
	,[melhorPreco] [int] DEFAULT(0)
)

-- Produtos a encomendar
If OBJECT_ID('tempdb.dbo.#temp_ST') IS NOT NULL
	drop table #temp_ST;

create table #temp_ST(
	ststamp char(25) NOT NULL
	,ref char(18) NOT NULL DEFAULT ('')
	,codigo char(40) NOT NULL DEFAULT('')
	,design varchar(100) NOT NULL DEFAULT('')
	,encomendar bit NOT NULL DEFAULT(0)
	,qtt numeric(13,3) NOT NULL DEFAULT(0)
	,stockGrupo numeric(13,3) NOT NULL DEFAULT(0)
	,qttfor numeric(13,3) NOT NULL DEFAULT(0)
	,stock numeric(13,3) NOT NULL DEFAULT(0)
	,ptoenc numeric(10,3) NOT NULL DEFAULT(0)
	,stmin numeric(13,3) NOT NULL DEFAULT(0)
	,stmax numeric(13,3) NOT NULL DEFAULT(0)
	,eoq numeric(13,3) NOT NULL DEFAULT(0)
	,qttcli numeric(13,3) NOT NULL DEFAULT(0)
	,qttacin numeric(13,3) NOT NULL DEFAULT(0)
	,epcusto numeric(19,6) NOT NULL DEFAULT(0)
	,epcult numeric(19,6) NOT NULL DEFAULT(0)
	,epcpond numeric(19,6) NOT NULL DEFAULT(0)
	,conversao numeric(15,7) NOT NULL DEFAULT(0)
	,cpoc numeric(6,0) NOT NULL DEFAULT(0)
	,familia varchar(18) NOT NULL DEFAULT('')
	,faminome varchar(60) NOT NULL DEFAULT('')
	,u_lab varchar(150) NOT NULL DEFAULT('')
	,u_fonte varchar(1) NOT NULL DEFAULT('')
	,fornec numeric(10,0) NOT NULL DEFAULT(0)
	,fornecedor varchar(80) NOT NULL DEFAULT('')
	,fornestab numeric(3,0) NOT NULL DEFAULT(0)
	,tabiva numeric(5,2) NOT NULL DEFAULT(0)
	,iva numeric(5,2) NOT NULL DEFAULT(0)
	,generico bit NOT NULL DEFAULT(0)
	,componente bit NOT NULL DEFAULT(0)
	,grphmgcode varchar(6)
	,grphmgdescr varchar(100)
	,marg4 numeric(16,3) NOT NULL DEFAULT(0)
	,psico bit
	,benzo bit
	,marca varchar(200) NOT NULL DEFAULT('')
	,prioritario bit
	,pvp numeric(19,6) NOT NULL DEFAULT(0)
	,tipoProduto varchar(254)  NOT NULL DEFAULT('')
	,obs varchar(254)  NOT NULL DEFAULT('')
	,u_nota1 TEXT
)

IF @grupo = '' -- Individual
BEGIN

	INSERT INTO #temp_PCLForn
	select
		*
		,melhorPreco = dense_rank() over (partition by ref order by u_upc)
	from
		(
			Select
				id = ROW_NUMBER() over (partition by ref,no,estab order by fo.data desc,fo.usrhora desc)
				,fn.ref
				,fn.u_upc
				,fo.no
				,fo.estab
				,fo.data
				,fn.qtt
				,fo.usrhora
				,fn.u_stockact
			From
				[dbo].[fo] (nolock)
				inner join [dbo].[fn] (nolock) on fo.fostamp = fn.fostamp 
			where
				fo.doccode IN (55,101,102)
				and fo.docdata >= dateadd(year,-1,getdate())
				and fn.u_upc != 0 and fn.ref != ''
				and fn.armazem = case when @armazem=0 then fn.armazem else @armazem end
		) x
	where
		x.id = 1


	IF @conjunta = 1
	BEGIN
		
		-- Enc Conjunta
		If OBJECT_ID('tempdb.dbo.#TempStConjunta') IS NOT NULL
			drop table #TempStConjunta;

		Select 
			ref
			,stock = SUM(st.stock)
			,qttfor = SUM(st.qttfor)
			,qttcli = SUM(st.qttcli)
		into
			#TempStConjunta
		From
			st (nolock)
		group by 
			st.ref
			
			
		-- produtos a encomendar
		INSERT INTO #temp_ST
		select
			*
		from (
			select
				st.ststamp
				,st.ref
				,st.codigo
				,st.design
				,encomendar		= case
									when isnull((select COUNT(ref) from #temp_sc sc2 (nolock) where sc2.refb=st.ref),0) > 1
										or isnull((select COUNT(refb) from #temp_sc sc2 (nolock) where sc2.refb=(select refb from #temp_sc where #temp_sc.ref=st.ref)),0) = 1
									then 0
									else 1
									end
				,qtt			= isnull(sc.qtt,1)
				,stockGrupo		= (select sum(stt.stock) from st stt (nolock) where stt.ref = st.ref)
				,qttfor			= isnull(conjunta.qttfor,0)
				,stock			= isnull(conjunta.stock,0) 
				,st.ptoenc
				,st.stmin
				,stmax
				,st.eoq
				,qttcli			= isnull(conjunta.qttcli,0)
				,st.qttacin
				,epcusto, epcult, st.epcpond, conversao
				,cpoc, familia, faminome, u_lab
				,u_fonte
				,st.fornec, st.fornecedor, st.fornestab
				,tabiva
				,iva			= ISNULL(taxasiva.taxa,0)
				,generico		= isnull(fprod.generico,0)
				,componente		= case when sc.ref is null then 0 else 1 end
				,grphmgcode
				,grphmgdescr
				,marg4
				,psico
				,benzo
				,marca = usr1
				,prioritario = st.marcada
				,pvp=st.epv1
				,tipoProduto = RTRIM(LTRIM(ISNULL(b_famFamilias.design,''))) 
				,obs = dbo.alltrimIsNull(st.obs)
				,u_nota1 = dbo.alltrimIsNull(st.u_nota1)  
			from
				[dbo].[st] (nolock)
				left join [dbo].[fprod] (nolock)	on fprod.cnp COLLATE DATABASE_DEFAULT=st.ref COLLATE DATABASE_DEFAULT
				left join #temp_sc sc				on sc.ref COLLATE DATABASE_DEFAULT=st.ref COLLATE DATABASE_DEFAULT
				left join taxasiva (nolock)			on taxasiva.codigo=st.tabiva
				Left join #TempStConjunta conjunta on conjunta.ref COLLATE DATABASE_DEFAULT= st.ref COLLATE DATABASE_DEFAULT
				inner join #TempRefAt on #TempRefAt.ref COLLATE DATABASE_DEFAULT=st.ref COLLATE DATABASE_DEFAULT
				left join b_famFamilias(nolock)		on st.u_famstamp = b_famFamilias.famstamp
			where
				st.inactivo = 0 and st.stns = 0
				--and st.ref = case when @ref = '' then st.ref else @ref end
				and st.site_nr = @site_nr
				--and st.stmax  > case when @validaStockMaxZero=1 then 0
				--else -9999999
				--end
				 
		) x
		where
			encomendar=1

	END
	ELSE
	BEGIN

		-- produtos a encomendar
		INSERT INTO #temp_ST
		select
			*
		from (
			select
				st.ststamp
				,st.ref
				,st.codigo
				,st.design
				,encomendar		= case
									when isnull((select COUNT(ref) from #temp_sc sc2 (nolock) where sc2.refb=st.ref),0) > 1
										or isnull((select COUNT(refb) from #temp_sc sc2 (nolock) where sc2.refb=(select refb from #temp_sc where #temp_sc.ref=st.ref)),0) = 1
									then 0
									else 1
									end
				,qtt			= isnull(sc.qtt,1)
				,stockGrupo		= (select sum(stt.stock) from st stt (nolock) where stt.ref = st.ref)
				,qttfor			= ISNULL(sa.resfor,st.qttfor)
				,stock			= ISNULL(sa.stock,st.stock)
				,st.ptoenc
				,st.stmin
				,stmax
				,st.eoq
				,qttcli			= ISNULL(sa.rescli, st.qttcli)
				,st.qttacin
				,epcusto, epcult, st.epcpond, conversao
				,cpoc, familia, faminome, u_lab
				,u_fonte
				,st.fornec, st.fornecedor, st.fornestab
				,tabiva
				,iva			= ISNULL(taxasiva.taxa,0)
				,generico		= isnull(fprod.generico,0)
				,componente		= case when sc.ref is null then 0 else 1 end
				,grphmgcode
				,grphmgdescr
				,marg4
				,psico
				,benzo
				,marca = usr1
				,prioritario = st.marcada
				,pvp=st.epv1
				,tipoProduto = RTRIM(LTRIM(ISNULL(b_famFamilias.design,''))) 
				,obs = dbo.alltrimIsNull(st.obs)
				,u_nota1 = dbo.alltrimIsNull(st.u_nota1) 
			from
				[dbo].[st] (nolock)
				left join [dbo].[sa] (nolock)		on st.ref COLLATE DATABASE_DEFAULT=sa.ref COLLATE DATABASE_DEFAULT and sa.armazem=@armazem
				left join [dbo].[fprod] (nolock)	on fprod.cnp COLLATE DATABASE_DEFAULT=st.ref COLLATE DATABASE_DEFAULT
				left join #temp_sc sc				on sc.ref COLLATE DATABASE_DEFAULT=st.ref COLLATE DATABASE_DEFAULT
				left join taxasiva (nolock)			on taxasiva.codigo=st.tabiva
				inner join #TempRefAt on #TempRefAt.ref COLLATE DATABASE_DEFAULT=st.ref COLLATE DATABASE_DEFAULT
				left join b_famFamilias(nolock)		on st.u_famstamp = b_famFamilias.famstamp
			where
				st.inactivo = 0 and st.stns = 0
				--and st.ref = case when @ref = '' then st.ref else @ref end
				and st.site_nr = @site_nr
				--and st.stmax  > case when @validaStockMaxZero=1 then 0
				--else -9999999
				--end
				
				 
		) x
		where
			encomendar=1
	END

END
ELSE -- Grupo
BEGIN


	set @sql = N'
		select
			*
			--,melhorPreco = dense_rank() over (partition by ref order by u_upc)
			,melhorPreco = null
		from
			(
			Select
				id = ROW_NUMBER() over (partition by bi.ref, bo.no, bo.estab order by bo.dataobra desc,bo.usrhora desc)
				,bi.ref
				,u_upc = bi.binum2 -- PCLk
				,no = ' + convert(varchar,@no) + '
				,estabo = ' + convert(varchar,@estab) + '
				,data = bo.dataobra
				,bi.qtt
				,bo.usrhora
				,bi.u_stockact
			From
				' + @server + '.' + @grupo + '.dbo.bo
				inner join ' + @server + '.' + @grupo + '.dbo.bi on bo.bostamp = bi.bostamp
			where
				bo.fechada = 0
				and bo.ndos = 45
				and bi.armazem = case when ' + convert(varchar,@armazem) + '=0 then bi.armazem else ' + convert(varchar,@armazem) + ' end
				and bi.ref != ''''
				and bi.binum2 != 0
			) x
		where
			x.id = 1'

	INSERT INTO #temp_PCLForn
	execute sp_executesql @sql
	
	-- produtos a encomendar
	set @sql2 = N'
		select
			*
		from (
			select
				stx.ststamp
				,stx.ref
				,stx.codigo
				,stx.design
				,encomendar		= case
									when isnull((select COUNT(ref) from #temp_sc sc2 (nolock) where sc2.refb=stx.ref),0) > 1
										or isnull((select COUNT(refb) from #temp_sc sc2 (nolock) where sc2.refb=(select refb from #temp_sc where #temp_sc.ref=stx.ref)),0) = 1
									then 0
									else 1
									end
				,qtt			= isnull(sc.qtt,1)
				,qttfor			= ISNULL(sa.resfor,stx.qttfor)
				,stock			= ISNULL(sa.stock,stx.stock)
				,stx.ptoenc
				,stx.stmin
				,stx.stmax
				,stx.eoq
				,qttcli			= ISNULL(sa.rescli, stx.qttcli)
				,stx.qttacin
				,stx.epcusto, stx.epcult, stx.epcpond, stx.conversao
				,stx.cpoc
				,familia = isnull(st.familia,stx.familia)
				,faminome = isnull(st.faminome,stx.faminome)
				,u_lab = isnull(st.u_lab, stx.u_lab)
				,stx.u_fonte
				,fornec			= ' + convert(varchar,@no) + '
				,fornecedor		= ''' + @nome + '''
				,fornestab		= ' + convert(varchar,@estab) + '
				,stx.tabiva
				,iva			= ISNULL(taxasiva.taxa,0)
				,generico		= isnull(fprod.generico,0)
				,componente		= case when sc.ref is null then 0 else 1 end
				,fprod.grphmgcode
				,fprod.grphmgdescr
				,marg4 = isnull(st.marg4,stx.marg4)
				,fprod.psico
				,fprod.benzo
				,marca = isnull(st.usr1,stx.usr1)
				,prioritario = st.marcada
				,pvp=st.epv1
				,tipoProduto = RTRIM(LTRIM(ISNULL(b_famFamilias.design,'''')))
				,obs = dbo.alltrimIsNull(stx.obs)
				,u_nota1 = dbo.alltrimIsNull(stx.u_nota1) 
			from
				' + @server + '.' + @grupo + '.dbo.bo
				inner join ' + @server + '.' + @grupo + '.dbo.bi on bo.bostamp = bi.bostamp
				inner join ' + @server + '.' + @grupo + '.dbo.st on st.ref = bi.ref
				inner join st stx (nolock)		on stx.ref=bi.ref
				left join sa (nolock)		on stx.ref=sa.ref and sa.armazem=' + convert(varchar,@armazem)+'
				left join fprod (nolock)	on fprod.cnp=stx.ref
				left join #temp_sc sc		on sc.ref=stx.ref
				left join taxasiva (nolock)	on taxasiva.codigo=stx.tabiva
				inner join #TempRefAt on #TempRefAt.ref=stx.ref
				left join b_famFamilias(nolock)		on stx.u_famstamp = b_famFamilias.famstamp
			where
				bo.fechada = 0
				and bo.ndos = 45
				--and bi.armazem = case when ' + convert(varchar,@armazem) + '=0 then bi.armazem else ' + convert(varchar,@armazem) + ' end
				and bi.ref != ''''
				and bi.binum2 != 0
				and stx.inactivo = 0 and stx.stns = 0
				--and stx.ref = case when ''' + @ref + ''' = '''' then stx.ref else ''' + @ref + ''' end
				and stx.site_nr = ' + ltrim(str(@site_nr)) + '
				and st.stmax  > case when @validaStockMaxZero=1 then 0
							else -9999999
							end
		) x
		where
			encomendar=1'

	INSERT INTO #temp_ST
	execute sp_executesql @sql2
END
	print '1'

-- Resultset final
select
	pesquisa		= convert(bit,0)
	,ststamp		= st.ststamp
	,ENC			= convert(bit,0)
	,u_fonte		= st.u_fonte
	,ref			= isnull(st.ref,'')
	,refb			= st.REF
	,qttb			= st.qtt
	,stockGrupo		= st.stockGrupo
	,codigo			= st.codigo
	,DESIGN			= ltrim(st.design)
	,Stock			= convert(int,st.stock)
	,STMIN			= st.stmin
	,STMAX			= st.stmax
	,OSTMAX			= st.stmax
	,PTOENC			= st.ptoenc
	,OPTOENC			= st.ptoenc
	,EOQ			= (select * from uf_calcular_qtt_encomendar_emb 
						(Case
							when (st.qttfor+st.stock<=st.ptoenc and st.ptoenc!=0 and st.stock+st.qttfor<st.stmax)
								then 
									convert(int, (st.STMAX-st.STOCK+st.EOQ-st.QTTFOR+st.QTTCLI) * st.qtt)
								else 
									convert(int, (st.EOQ) * st.qtt)
						end
						, @site_nr
						,st.ref, '0'))

	,sug			= Case
						when (st.qttfor+st.stock<=st.ptoenc and st.ptoenc!=0 and st.stock+st.qttfor<st.stmax)
						then convert(int, (st.STMAX-st.STOCK+st.EOQ-st.QTTFOR+st.QTTCLI) * st.qtt)
						else convert(int, (st.EOQ) * st.qtt)
						end
	,qtBonus		= 0
	,qttadic		= st.eoq
	,qttfor			= st.qttfor
	,qttacin		= st.qttacin
	,qttcli			= st.qttcli
	,fornecedor		= st.fornecedor
	,fornec			= st.fornec
	,fornestab		= st.fornestab
	,SEL			= CONVERT(bit,0)
	,epcusto		= st.epcusto
	,epcpond		= st.epcpond
	,epcult			= st.epcult
	,tabiva			= st.tabiva
	,iva			= st.iva
	,cpoc			= st.cpoc
	,familia		= st.familia
	,faminome		= st.faminome
	,u_lab			= st.u_lab
	,conversao		= st.conversao
	/* colunas sobre os esgotados */
--	,bonusFornec	= case when isnull(Bonus.bonus,'')='' then '' else 'S' end
	,bonusFornec	= (case  
						when EXISTS(select 1 from uv_encomendas_ultBonus(nolock) as ult where ult.ref COLLATE DATABASE_DEFAULT = st.ref COLLATE DATABASE_DEFAULT and ult.no = st.fornec)
							THEN CAST('S' AS varchar(20))
						ELSE CAST('' AS varchar(20))
					  end)
	,pclFornec		= isnull(#temp_PCLForn.u_upc,0)	
	,Esgotado		= case /* É esgotado se o ultimo documento do fornecedor tiver a qtt = 0, no caso de não ter fornecedor associado não aparece como esgotado*/
						when @grupo = '' and #temp_Esgotados.ref is not null then 'S' 
						-- Nas encomendas de grupo não notificar de esgotados
						else ''
						END
	,ordem			= 0
	,generico		= st.generico
	,componente
	,stockGH			= isnull(#temp_GH.stock,0)
	,st.marg4
	--exec up_enc_autoref_atendimento 1, '5448949', '', 1, 0
	/*BB do mesmo grupo Homogeneo*/
	,alertaStockGH		= case
							when (st.grphmgcode='GH0000' or st.grphmgcode='' or st.grphmgcode is null) then 0
							--when #temp_MelhorBB.ref is not null then 0

							when #temp_Existencia5Baratos.grphmgcode is null then 0
							else 1
						  end
	,alertaPCL			= case when #temp_PCLForn.melhorPreco = 1 or #temp_PCLForn.melhorPreco is null then 0 else 1 end
	,alertaBonus		= case when Bonus2.ref is not null and Bonus.ref is null then 1 else 0 end
	,#temp_MelhorBB.ref
	,#temp_Existencia5Baratos.grphmgcode
	,marca
	,psico
	,benzo
	,BBfornec  = convert(numeric(9,2),0)
	,mbpcfornec = convert(numeric(9,2),0)
	,qtprevista = convert(numeric(9,2),0)
	,prioritario
	,vv		= case when (select count(*) from emb_via_verde (nolock) where emb_via_verde.ref COLLATE DATABASE_DEFAULT=st.ref COLLATE DATABASE_DEFAULT and (year(emb_via_verde.data_fim)=1900 or data_fim>getdate()))>0 then convert(bit,1) else convert(bit,0) end
	--exec up_enc_autoref_atendimento 1, '2177384', '', 1, 0
	,qttres	= (select isnull(sum(bi.qtt - bi.qtt2),0) from bi (nolock) join bo(nolock) on bo.bostamp = bi.bostamp where bi.ndos=5 and bo.fechada=0 and bi.fechada=0 and armazem = @site_nr and bi.ref COLLATE DATABASE_DEFAULT=st.ref COLLATE DATABASE_DEFAULT)
	,m3um = (select isnull(round(sum(qtt)/3,0),0) from sl (nolock) where ref COLLATE DATABASE_DEFAULT=st.ref COLLATE DATABASE_DEFAULT and datalc between getdate()-90 and getdate() and origem='FT' and armazem = @site_nr)
	,lordem  = convert(numeric(10,0),0)
	,nreceita = convert(varchar(50),'')
	,fornecabrev	= isnull(case when fl.nome2='' then fl.nome else fl.nome2 end,'')
	,pvp
	,tipoProduto
	,st.obs 
	,st.u_nota1  
	,descontoForn = (CASE 
						WHEN isnull(case when fl.nome2='' then fl.nome else fl.nome2 end,'') <> '' 
					THEN ISNULL((SELECT 
									TOP 1 bi.desconto 
								FROM 
									bi(NOLOCK) 
									join bo(nolock) ON bi.bostamp COLLATE DATABASE_DEFAULT= bo.bostamp COLLATE DATABASE_DEFAULT 
									join ts(nolock) ON ts.ndos = bo.ndos 
								WHERE 
									ts.codigoDoc = 39 
									and bi.ref COLLATE DATABASE_DEFAULT = st.ref COLLATE DATABASE_DEFAULT
									AND bo.no = st.fornec 
									and bo.estab = st.fornestab 
									and bo.fechada = 0
								ORDER BY 
									bi.ousrdata desc, bi.ousrhora desc), CAST(0 AS NUMERIC(6,2)))
						ELSE CAST(0 AS NUMERIC(6,2)) 
					END)
	,descontoFornOri = (CASE 
						WHEN isnull(case when fl.nome2='' then fl.nome else fl.nome2 end,'') <> '' 
					THEN ISNULL((SELECT 
									TOP 1 bi.desconto 
								FROM 
									bi(NOLOCK) 
									join bo(nolock) ON bi.bostamp COLLATE DATABASE_DEFAULT= bo.bostamp COLLATE DATABASE_DEFAULT 
									join ts(nolock) ON ts.ndos = bo.ndos 
								WHERE 
									ts.codigoDoc = 39 
									and bi.ref COLLATE DATABASE_DEFAULT= st.ref COLLATE DATABASE_DEFAULT
									AND bo.no = st.fornec 
									and bo.estab = st.fornestab 
									and bo.fechada = 0
								ORDER BY 
									bi.ousrdata desc, bi.ousrhora desc), CAST(0 AS NUMERIC(6,2)))
						ELSE CAST(0 AS NUMERIC(6,2)) 
					END)
from
	#temp_st as st
	left join #temp_GH					on #temp_GH.grphmgcode COLLATE DATABASE_DEFAULT= st.grphmgcode COLLATE DATABASE_DEFAULT
	left join #temp_PCLForn				on #temp_PCLForn.ref COLLATE DATABASE_DEFAULT= st.ref COLLATE DATABASE_DEFAULT and st.fornec = #temp_PCLForn.no and st.fornestab = #temp_PCLForn.estab 
	left join #temp_bonus as Bonus		on Bonus.ref COLLATE DATABASE_DEFAULT= st.ref COLLATE DATABASE_DEFAULT and st.fornec = Bonus.no and st.fornestab = Bonus.estab 
	left join #temp_bonus as Bonus2		on Bonus2.ref COLLATE DATABASE_DEFAULT= st.ref COLLATE DATABASE_DEFAULT and Bonus2.contagem = 1
	left join #temp_Esgotados			on #temp_Esgotados.ref COLLATE DATABASE_DEFAULT= st.ref COLLATE DATABASE_DEFAULT and st.fornec = #temp_Esgotados.no and st.fornestab = #temp_Esgotados.estab 
	left join #temp_MelhorBB			on st.ref COLLATE DATABASE_DEFAULT= #temp_MelhorBB.ref COLLATE DATABASE_DEFAULT
	left join #temp_Existencia5Baratos	on st.grphmgcode COLLATE DATABASE_DEFAULT= #temp_Existencia5Baratos.grphmgcode COLLATE DATABASE_DEFAULT
	left join fl (nolock)				on st.fornec=fl.no and st.fornestab=fl.estab
order by
	design
--exec up_enc_autoref_atendimento 1, '8113837,5635768', '', 1, 0
If OBJECT_ID('tempdb.dbo.#temp_bonus') IS NOT NULL
	drop table #temp_bonus;

If OBJECT_ID('tempdb.dbo.#temp_sc') IS NOT NULL
	drop table #temp_sc;

If OBJECT_ID('tempdb.dbo.#temp_MelhorBB') IS NOT NULL
	drop table #temp_MelhorBB;

If OBJECT_ID('tempdb.dbo.#temp_GH') IS NOT NULL
	drop table #temp_GH;

If OBJECT_ID('tempdb.dbo.#temp_Existencia5Baratos') IS NOT NULL
	drop table #temp_Existencia5Baratos;

If OBJECT_ID('tempdb.dbo.#temp_Esgotados') IS NOT NULL
	drop table #temp_Esgotados;

If OBJECT_ID('tempdb.dbo.#temp_PCLForn') IS NOT NULL
	drop table #temp_PCLForn;

If OBJECT_ID('tempdb.dbo.#temp_ST') IS NOT NULL
	drop table #temp_ST;

GO
Grant Execute on dbo.up_enc_autoref_atendimento to Public
Grant Control on dbo.up_enc_autoref_atendimento to Public
GO