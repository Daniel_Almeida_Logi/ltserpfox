/*
	Atualizar estado de envio da encomenda

	exec up_encomendas_postal_marcaEncomendaComoEnviada 'ADM2978662A-51DF-4A1E-A1D', 8
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_encomendas_postal_marcaEncomendaComoEnviada]') IS NOT NULL
	drop procedure dbo.up_encomendas_postal_marcaEncomendaComoEnviada
go

create procedure dbo.up_encomendas_postal_marcaEncomendaComoEnviada
	@stamp varchar (25)
	,@userno numeric(6,0)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

update
	bo
set
	logi1 = 1
	,usrinis = isnull((select iniciais from b_us (nolock) where userno=@userno),'')
	,usrdata = convert(varchar,getdate(),102)
	,usrhora = convert(varchar,getdate(),108)
where
	bostamp = @stamp

GO
Grant Execute on dbo.up_encomendas_postal_marcaEncomendaComoEnviada to Public
Grant Control on dbo.up_encomendas_postal_marcaEncomendaComoEnviada to Public
GO