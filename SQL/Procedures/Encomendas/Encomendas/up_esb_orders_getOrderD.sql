/*
	Levantar detalhe da encomenda a enviar
	
	exec up_esb_orders_getOrderD 'ADM468C3252-3DE1-4BA4-B14'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_esb_orders_getOrderD]') IS NOT NULL
	drop procedure dbo.up_esb_orders_getOrderD
go

create procedure dbo.up_esb_orders_getOrderD
	@stamp varchar (25)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

Select
	bi.bostamp
	,bo.boano
	,bo.obrano
	,bo.ndos
	,bo.ousrdata
	,bo.moeda
	,bo.etotal
	,bi.ref
	,bi.design
	,bi.edebito
	,qtt = case
			when qtt > u_bonus
			then qtt - u_bonus
			else qtt
		end
	,bi.u_bonus
	,bi.desconto
	,bi.desc2
	,bi.desc3
	,bi.iva
	,bi.ettdeb
	,bi.epu
from
    bi (nolock)
	inner join bo (nolock) on bo.bostamp = bi.bostamp
where
	bi.ref != ''
	and bi.bostamp = @stamp

GO
Grant Execute on dbo.up_esb_orders_getOrderD to Public
Grant Control on dbo.up_esb_orders_getOrderD to Public
GO