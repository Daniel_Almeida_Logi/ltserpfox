
/*
	 exec up_RPM_getCabecalho 'js45CAB5FF-C588-475C-B10' 

	 select nprescsns, * from b_cli_presc where prescstamp = 'js6AED3232-96BC-423A-8D5'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_RPM_getCabecalho]') IS NOT NULL
	drop procedure up_RPM_getCabecalho
go

create PROCEDURE up_RPM_getCabecalho
@stamp varchar(25)

/* WITH ENCRYPTION */ 
AS 
BEGIN 
	
	SELECT 
		b_utentes.cesd
		,b_utentes.atestado
		,b_utentes.cesdcart
		,b_utentes.cesdcp
		,b_utentes.cesdidp
		,b_utentes.atnumero
		,b_utentes.atcp
		,b_utentes.attipo
		,b_utentes.cesdval
		,b_utentes.atval
		,b_utentes.autorizado
		,convert(decimal,case when b_cli_presc.nrutente='' then '0' else b_cli_presc.nrutente end) utdecimal
		,CONVERT(varchar,nreclocal) nreclocalString
		,b_cli_presc.prescstamp
		,b_cli_presc.prescno
		,b_cli_presc.prescno2
		,b_cli_presc.prescno3
		,b_cli_presc.nreclocal
		,b_cli_presc.data
		,b_cli_presc.no
		,b_cli_presc.estab
		,b_cli_presc.nome
		,b_cli_presc.nome2
		,b_cli_presc.morada
		,b_cli_presc.local
		,b_cli_presc.codpost
		,telefone = case when b_cli_presc.telefone = '' then b_cli_presc.telemovel else  b_cli_presc.telefone end 
		,telemovel = case when b_cli_presc.telefone = '' then b_cli_presc.telemovel else  b_cli_presc.telefone end 
		,b_us.email
		,b_cli_presc.ncont
		,b_cli_presc.nrutente
		,b_cli_presc.nrbenef
		,b_cli_presc.sexo
		,b_cli_presc.naturalidade
		,b_cli_presc.nacionalidade
		,b_cli_presc.tipo
		,b_cli_presc.dtnasc
		,b_cli_presc.prescano
		,b_cli_presc.utstamp
		,b_cli_presc.drstamp
		,b_cli_presc.drnome
		,b_cli_presc.drno
		,b_cli_presc.drespecialidade
		,b_cli_presc.drtratamento
		,b_cli_presc.drnacionalidade
		,b_cli_presc.drnascimento
		,b_cli_presc.drsexo
		,b_cli_presc.drmorada
		,b_cli_presc.drlocal
		,b_cli_presc.drcodpost
		,b_cli_presc.drtelefone
		,b_cli_presc.drtelemovel
		,b_cli_presc.dremail
		,b_cli_presc.drcedula
		,b_cli_presc.total
		,b_cli_presc.codigoComp1
		,b_cli_presc.designComp1
		,b_cli_presc.codigoComp2
		,b_cli_presc.designComp2
		,b_cli_presc.entidade
		,b_cli_presc.entidadeDescr
		,b_cli_presc.entidadeCod
		,b_cli_presc.validade
		,b_cli_presc.obs
		,b_cli_presc.ousrinis
		,b_cli_presc.ousrdata
		,b_cli_presc.ousrhora
		,b_cli_presc.usrinis
		,b_cli_presc.usrdata
		,b_cli_presc.usrhora
		,b_cli_presc.marcada
		,b_cli_presc.localPresc
		,b_cli_presc.vias
		,b_cli_presc.anulado
		,b_cli_presc.u_codigop
		,b_cli_presc.u_descp
		,b_cli_presc.RECMtipo
		,b_cli_presc.RECMcodigo
		,b_cli_presc.RECMdescricao
		,b_cli_presc.RECMdatainicio
		,b_cli_presc.RECMdatafim
		,b_cli_presc.BENEFcodigo
		,b_cli_presc.BENEFdescricao
		,b_cli_presc.BENEFdatainicio
		,b_cli_presc.BENEFdatafim
		,b_cli_presc.OUTROSBENEFcodigo
		,b_cli_presc.OUTROSBENEFdescricao
		,b_cli_presc.OUTROSBENEFdatainicio
		,b_cli_presc.OUTROSBENEFdatafim
		,b_cli_presc.drinscricao
		,b_cli_presc.drordem
		,b_cli_presc.anulamotivoCod
		,b_cli_presc.anulamotivoDesc
		,b_cli_presc.anuladata
		,b_cli_presc.renovavel
		,b_cli_presc.mcdt_inclDados
		,b_cli_presc.mcdt_data
		,b_cli_presc.mcdt_isento
		,b_cli_presc.nrprescricao
		,b_cli_presc.terminal
		,b_cli_presc.tiporeceita
		,b_cli_presc.receitamcdt
		,b_cli_presc.estadoDescr
		,b_cli_presc.estado
		,b_cli_presc.site
		,b_cli_presc.enviado
		,b_cli_presc.pin
		,b_cli_presc.pinOpcao
		,b_cli_presc.pin2
		,b_cli_presc.pinOpcao2
		,b_cli_presc.pin3
		,b_cli_presc.pinOpcao3
		,b_cli_presc.mcdt_domicilio
		,b_cli_presc.mcdt_justificacaoD
		,b_cli_presc.mcdt_urgente
		,b_cli_presc.mcdt_justificacaoU
		,b_cli_presc.mcdt_infoClinica
		,b_cli_presc.mcdt_outrasInfo
		,b_cli_presc.nprescsns
		,b_cli_presc.mcdt_episodioID
		,b_cli_presc.mcdt_notificacaoRequisicaoDesc
		,b_cli_presc.mcdt_notificacaoRequisicaoID
		,b_cli_presc.mcdt_emissaoExterna
		,dominio = isnull(b_cli_efr.dominio,'') 
		--,codigop = case when b_cli_presc.nrutente = '' then b_utentes.codigop else 'PT' end 
		--,codigop = case when isnull(b_cli_efrUtente.Pais,'') = '' then b_utentes.codigop else b_cli_efrUtente.Pais end 
		,codigop = case when dominio='EFRPT'  then 'PT'
					   else case when isnull(b_cli_efrUtente.Pais,'') = '' then b_utentes.codigop else b_cli_efrUtente.Pais end
				   end  
		,espno = isnull(b_especialidades.espno,'')
		,cedula = case 
			when drinscri != 2 then CONVERT(decimal,right(b_cli_presc.drcedula,5))
			else CONVERT(decimal,right(b_cli_presc.drcedula,4)) 
		end
		,drclprofi
		,localcod = convert(varchar(1),empresa.zonaacss) + empresa.localcod
		,localnm = empresa.localnm
		,localPrescEnvioWs = convert(varchar(1),empresa.zonaacss) + empresa.localcod
		,b_us.nacion
		,b_us.ncont AS nifPrescritor
	FROM 
		b_cli_presc (nolock)
		left join b_cli_efr (nolock) on b_cli_efr.cod = b_cli_presc.entidadeCod
		inner join b_utentes (nolock) on b_utentes.utstamp = b_cli_presc.utstamp
		left join b_especialidades (nolock) on b_cli_presc.drespecialidade = b_especialidades.especialidade
		inner join b_us (nolock) on b_cli_presc.drstamp = b_us.usstamp
		inner join empresa (nolock) on empresa.site = b_cli_presc.site
		left join b_cli_efrutente (nolock) on b_cli_efrutente.stamp = b_utentes.utstamp
	WHERE 
		prescstamp = @stamp
END

GO
Grant Execute On up_RPM_getCabecalho to Public
Grant Control On up_RPM_getCabecalho to Public
GO