/*
	 exec up_RPM_getAnulaVias 'jsC4F1CEC9-9EE9-4384-849'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_RPM_getAnulaVias]') IS NOT NULL
	drop procedure up_RPM_getAnulaVias
go

create PROCEDURE up_RPM_getAnulaVias
@stamp varchar(25)

/* WITH ENCRYPTION */ 
AS 
BEGIN 
	SELECT 
		anulado
		,vias
	FROM 
		b_cli_presc (nolock)
	WHERE 
		prescstamp = @stamp
END

GO
Grant Execute On up_RPM_getAnulaVias to Public
Grant Control On up_RPM_getAnulaVias to Public
GO
