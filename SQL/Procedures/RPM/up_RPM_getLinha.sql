/*
	 exec up_RPM_getLinha 'js46B9DFD7-61F4-4DE4-8BF' 

	 select * from b_cli_prescl where presclstamp = 'jsF86A60AC-0AA9-449B-880 '
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_RPM_getLinha]') IS NOT NULL
	drop procedure up_RPM_getLinha
go

create PROCEDURE up_RPM_getLinha
@stamp varchar(25)

/* WITH ENCRYPTION */ 

AS 
BEGIN 
	
	SELECT 
		CONVERT(integer, isnull(b_cli_prescl.cnpem,'0')) cnpem_int
		,isnull(fprod.form_farm_id,'') form_farm_id
		,isnull(fprod.descricao,'') descricao
		,isnull(fprod.dosuni,'') dosuni
		,isnull(fprod.dcipt_id,'') dcipt_id
		,isnull(b_cli_prescl.tiporeceita,'LN') tipoLinhaMed
		,b_cli_prescl.* 
	FROM 
		b_cli_prescl (nolock)
		left join fprod (nolock) on fprod.cnp=b_cli_prescl.ref
	WHERE
		b_cli_prescl.prescstamp = @stamp
		and LEFT(b_cli_prescl.design,1) != '.' 
	order by b_cli_prescl.pos
END

GO
Grant Execute On up_RPM_getLinha to Public
Grant Control On up_RPM_getLinha to Public
GO