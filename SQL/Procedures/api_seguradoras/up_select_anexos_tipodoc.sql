/* SP inserir anexo
	
	exec up_select_anexos_tipodoc 'ecra3','MAIANGA'
	

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_select_anexos_tipodoc]') IS NOT NULL
	DROP PROCEDURE dbo.up_select_anexos_tipodoc
GO

CREATE PROCEDURE [dbo].[up_select_anexos_tipodoc]
	@ecra varchar(30)
	,@site varchar(20)


/* WITH ENCRYPTION */
AS

SET NOCOUNT ON
	select 
		nmdoc  ,
		ndoc
	from 
		apiSeguradorasEcrasPermitidosPorSite (nolock)
	WHERE 
		nomeEcra = @ecra
		and site = @site

GO
GRANT EXECUTE on dbo.up_select_anexos_tipodoc TO PUBLIC
GRANT Control on dbo.up_select_anexos_tipodoc TO PUBLIC
GO