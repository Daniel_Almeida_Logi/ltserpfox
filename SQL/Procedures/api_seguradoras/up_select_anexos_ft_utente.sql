/* SP inserir anexo
  
  exec up_select_anexos_ft_utente 'ADM935F7015-00E8-4533-ABA'
  select * from anexos where regstamp='321654'
  update anexos set validade=getdate()+30 where regstamp='321654'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_select_anexos_ft_utente]') IS NOT NULL
  DROP PROCEDURE dbo.up_select_anexos_ft_utente
GO

CREATE PROCEDURE dbo.up_select_anexos_ft_utente
   @ftstampproc     varchar(50)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

Declare
  @no int,
  @estab int

  SELECT 
    @no   = no
    ,@estab = estab
  FROM   
    ft (nolock)
  WHERE 
    ftstamp = (SELECT 
            stamporigproc
          FROM   
            ft2 (nolock)
          WHERE 
            ft2stamp = @ftstampproc)
  
  SELECT utstamp,
       nome,
       no,
       estab,
       ncont,
       morada,
       telefone,
       tlmvl,
       bino,
     autorizado
  FROM   b_utentes (nolock)
  WHERE  no = @no
       AND estab = @estab

       
  
GO
GRANT EXECUTE on dbo.up_select_anexos_ft_utente TO PUBLIC
GRANT Control on dbo.up_select_anexos_ft_utente TO PUBLIC
GO