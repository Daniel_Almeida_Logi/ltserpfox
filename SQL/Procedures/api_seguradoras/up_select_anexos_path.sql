/* SP inserir anexo
	
	exec up_select_anexos_path 'Loja 1'
	select * from anexos where regstamp='321654'
	update anexos set validade=getdate()+30 where regstamp='321654'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_select_anexos_path]') IS NOT NULL
	DROP PROCEDURE dbo.up_select_anexos_path
GO

CREATE PROCEDURE dbo.up_select_anexos_path
	@site				varchar(25) = '',
	@stamp				varchar(25) = ''

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	SELECT 
		textvalue  as textvalue
	FROM	b_parameters_site
	WHERE	stamp = @stamp
		AND site = @site
	
GO
GRANT EXECUTE on dbo.up_select_anexos_path TO PUBLIC
GRANT Control on dbo.up_select_anexos_path TO PUBLIC
GO