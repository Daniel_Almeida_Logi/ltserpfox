/* SP inserir anexo
	
	exec up_select_anexos_doc_check 'Factura', 1, 'ATLANTICO'
	
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_select_anexos_doc_check]') IS NOT NULL
	DROP PROCEDURE dbo.up_select_anexos_doc_check
GO

CREATE PROCEDURE dbo.up_select_anexos_doc_check
	@nmdoc				varchar(25) = ''
	,@ndoc				numeric(15,0) = ''
	,@site					varchar(25) = ''

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	select 
		ftstamp
	from ft (nolock)
	where nmdoc=@nmdoc
		and fno=@ndoc
		and site=@site
		and ftano=YEAR(GETDATE())
GO
GRANT EXECUTE on dbo.up_select_anexos_doc_check TO PUBLIC
GRANT Control on dbo.up_select_anexos_doc_check TO PUBLIC
GO