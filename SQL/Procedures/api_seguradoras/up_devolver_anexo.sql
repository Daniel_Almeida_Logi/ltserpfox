

--code to validate if the procedure exists, if exists, drops procedure
if OBJECT_ID('[dbo].[up_devolver_anexo]') IS NOT NULL
	drop procedure dbo.up_devolver_anexo
go
/* 
	exec up_devolver_anexo 'CCV', '714CE46C-1A82-437A-BE43-E', ''
	exec up_devolver_anexo 'CCV', 'DACFD073-68D5-4B35-A2FF-B', ''
	exec up_devolver_anexo 'invoices', '', 'ADM77025F81-7110-4E00-8ED'
*/
-- =============================================
-- Author:		José Moreira
-- Create date: 2022-11-07
-- Description:  
-- =============================================
CREATE PROCEDURE up_devolver_anexo
	@type				varchar(3),
	@userStamp			varchar(36),
	@ftStamp			varchar(36)
AS
BEGIN   
	DECLARE @sql VARCHAR(MAX)
	set @sql = '
	select top 1 
		anexosstamp
		,regstamp
		,tabela
		,filename
		,descr
		,keyword
		,protected  
		,tipo
		,validade
		,readOnly
		,anexos.path 
		,anexos.ousrdata
	from
		anexos (nolock)		
	left join
		estadoProcessoAPISeguradoras (nolock) on anexos.anexosstamp = estadoProcessoAPISeguradoras.stampAnexos
	where
		' + case when @userStamp is not null and len(@userStamp)>0 
				then 'regstamp = ''' +  @userStamp + ''' and tabela = ''b_utentes'' and TIPO'
				else  'ftstamp = ''' +  @ftStamp + '''  and TIPOANEXO' end 				
				+ ' in (
					 SELECT 
						campo 
					FROM 
						b_multidata (NOLOCK) 
					inner join 
						regrasCartaoApiSeguradoras (nolock)		on	b_multidata.code_motive = regrasCartaoApiSeguradoras.grouperCod
					 where 
						regrasCartaoApiSeguradoras.code = '''+  @type +'''
						and  b_multidata.TIPO = ''anexos''
						and b_multidata.campo = regrasCartaoApiSeguradoras.description)
		and filename != ''''	  
		and anexos.path != ''''
	order by
		ousrdata desc'
	print @sql
	exec(@sql)
END
GO
GRANT EXECUTE on dbo.up_devolver_anexo TO PUBLIC
GRANT Control on dbo.up_devolver_anexo TO PUBLIC
GO
 