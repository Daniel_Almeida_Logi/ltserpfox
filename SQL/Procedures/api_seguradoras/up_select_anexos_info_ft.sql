/* SP inserir anexo
	
	exec up_select_anexos_info_ft 'ADM77025F81-7110-4E00-8ED' 

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_select_anexos_info_ft]') IS NOT NULL
	DROP PROCEDURE dbo.up_select_anexos_info_ft
GO

CREATE PROCEDURE dbo.up_select_anexos_info_ft
	 @regstamp			varchar(60) = ''


/* WITH ENCRYPTION */
AS


SET NOCOUNT ON

	select 
		ft_seguradora.ftstamp							as ftstamp,
		ft_cliente.estab								as clientDep,
		ft_cliente.no									as clientNumber,
		ft_seguradora.estab								as dep,
		ft_seguradora.no								as number,
		convert(varchar, ft_seguradora.fdata, 111)		as date,
		ft_seguradora.fno								as docNumber,
		ft_seguradora.ndoc								as docType,
		ft_seguradora.nmdoc								as descDocType,
		ft_seguradora.etotal							as etotal,
		isnull((select top 1
			 estadoProcessoAPISeguradoras.path 
		from  
			estadoProcessoAPISeguradoras (nolock) 
		where 
			estadoProcessoAPISeguradoras.ftstamp = ft_seguradora.ftstamp
			and estadoProcessoAPISeguradoras.tipoAnexo like ('Processo - Pasta')
			and estadoProcessoAPISeguradoras.valido = 1
		), '')											as path

	from 
		ft as ft_seguradora(nolock) 
	inner join
		ft2 (nolock) on ft_seguradora.ftstamp = ft2.ft2stamp
	left join
		ft as ft_cliente (nolock) on ft_cliente.ftstamp = ft2.stamporigproc
	where 
		ft_seguradora.ftstamp = @regstamp

		
	
GO
GRANT EXECUTE on dbo.up_select_anexos_info_ft TO PUBLIC
GRANT Control on dbo.up_select_anexos_info_ft TO PUBLIC
GO