
IF OBJECT_ID('[dbo].[up_select_documents_per_screen]') IS NOT NULL
    DROP procedure dbo.up_select_documents_per_screen
GO
/* 
	exec up_select_documents_per_screen 'ecra1', 'ATLANTICO', '19000101', '30000101',2,1
*/
CREATE procedure dbo.up_select_documents_per_screen 
	@screenId			varchar(60)		= ''  ,
	@site				varchar(60)		= ''  ,	 
	@initialDate		varchar(10)		= '19000101',
	@finalDate			varchar(10)		= '30000101',
	@topMax				int				= 100 ,
	@pageNumber			int				=	1
AS  
begin   

	if(isnull(@topMax,0)>100)
	begin
		set @topMax = 100
	end
	if(isnull(@topMax,0)<=0)
	begin
		set @topMax = 100
	end
	if(isnull(@pageNumber,0)<1)
	begin
		set @pageNumber = 1
	end

	DECLARE @PageSize int = isnull(@topMax,100) 
	DECLARE @SQL varchar(MAX)

	set @SQL = '
	select ' 
		+ convert(varchar(10),@PageSize) +' as pageSize,
		(COUNT(*) OVER() / ' +  convert(varchar(10),@PageSize) +') + 1 as pageTotal, '
		+ convert(varchar(10),@PageNumber) +' as pageNumber, 
		COUNT(*) OVER() as linesTotal,

		 isnull(ft_client.estab, -1)			as ClientDep,
		 isnull(ft_client.no, -1)				as clientNumber,
		 ft_seguradoras.estab					as dep,
		 ft_seguradoras.no						as number,
		 ft_seguradoras.fdata					as date,
		 ft_seguradoras.fno						as docNumber,
		 ft_seguradoras.ndoc					as docType,
		 ft_seguradoras.nmdoc					as docTypeDescription,
		 ft_seguradoras.etotal					as retailPrice,
		 ft_seguradoras.ftstamp					as ftstamp,
		 ft2.nrelegibilidade					as nrelegibilidade,		 
		(select 
			(case when count(ft_compart.token)> 0 then 1 else 2 end )
		from
			ft_compart (nolock)	 
		where 
			ft_seguradoras.ftstamp = ft_compart.ftstamp 
			and ft_compart.type = 2	
			and ft_compart.returnContributionCorrelationId = ft2.nrelegibilidade) as tipoFaturacao,
			(select 
				(case when count(ft_compart.token)> 0 then ''Elegibilidade eletrónica'' else ''Elegibilidade manual'' end )
			from
				ft_compart (nolock)	 
			where 
				ft_seguradoras.ftstamp = ft_compart.ftstamp 
				and ft_compart.type = 2	
				and ft_compart.returnContributionCorrelationId = ft2.nrelegibilidade) as descrTipoFaturacao 
	from
		FT (nolock) as ft_seguradoras  
	inner join
		ft2 (nolock) on ft_seguradoras.ftstamp = ft2.ft2stamp
	left join
		ft (nolock) as ft_client on ft2.stamporigproc = ft_client.ftstamp
	where
		ft_seguradoras.ndoc in (select 
									ndoc 
								from 
									apiSeguradorasEcrasPermitidosPorSite (nolock) 
								where 
									nomeEcra ='''+ @screenId + '''
									and site = ''' + @site + ''')	
		and 
			ft_seguradoras.fdata between ''' + @initialDate + ''' and '''+@finalDate + '''
	order by 
		ft_seguradoras.ftstamp asc
	OFFSET ' + convert(varchar(10),@PageSize) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
	FETCH NEXT  ' + convert(varchar(10),@PageSize) + ' ROWS ONLY '  
	
	print @SQL
	EXEC (@SQL)
				
end  
GO
GRANT EXECUTE on dbo.up_select_documents_per_screen TO PUBLIC
GRANT Control on dbo.up_select_documents_per_screen TO PUBLIC
GO