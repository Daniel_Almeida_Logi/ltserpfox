

--code to validate if the procedure exists, if exists, drops procedure
if OBJECT_ID('[dbo].[up_devolver_anexo_cartao]') IS NOT NULL
	drop procedure dbo.up_devolver_anexo_cartao
go
/* 
	exec up_devolver_anexo_cartao 'CCV', '714CE46C-1A82-437A-BE43-E'  
	exec up_devolver_anexo_cartao 'CCV', 'DACFD073-68D5-4B35-A2FF-B'   
*/
-- =============================================
-- Author:		José Moreira
-- Create date: 2022-11-07
-- Description:  
-- =============================================
CREATE PROCEDURE up_devolver_anexo_cartao
	@type				varchar(3),
	@userStamp			varchar(36)
AS
BEGIN    
	select top 1 
		anexosstamp
		,regstamp
		,tabela
		,filename
		,descr
		,keyword
		,protected  
		,tipo
		,validade
		,readOnly
		,path 
		,ousrdata
	from
		anexos (nolock)
	where
		regstamp =   @userStamp 
		and tabela = 'b_utentes'
		and tipo in (
					 SELECT 
						campo 
					FROM 
						b_multidata (NOLOCK) 
					inner join 
						regrasCartaoApiSeguradoras (nolock)		on	b_multidata.code_motive = regrasCartaoApiSeguradoras.grouperCod
					 where 
						regrasCartaoApiSeguradoras.code =   @type 
						and  b_multidata.TIPO = 'anexos'
						and b_multidata.campo = regrasCartaoApiSeguradoras.description)
		and filename != ''	  
		and path != ''
	order by
		ousrdata desc
END
GRANT EXECUTE on dbo.up_devolver_anexo_cartao TO PUBLIC
GRANT Control on dbo.up_devolver_anexo_cartao TO PUBLIC
GO 

 