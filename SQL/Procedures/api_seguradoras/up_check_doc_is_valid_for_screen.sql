 
IF OBJECT_ID('[dbo].[up_check_doc_is_valid_for_screen]') IS NOT NULL
    DROP procedure dbo.up_check_doc_is_valid_for_screen
GO
/* 

*/
CREATE procedure dbo.up_check_doc_is_valid_for_screen 
	 @stamp			varchar(60)		= ''  
	 ,@screen		varchar(20)		= ''
AS  
begin    
	declare @result int = 0
	select 
		@result = count(*)
	from
		apiSeguradorasEcrasPermitidosPorSite (nolock)
	where
		nomeEcra = @screen
		and ndoc = (select
						ndoc 
					from 
						ft (nolock)
					where 
						ftstamp = @stamp)

	if @result is not null and @result > 0
	begin
		select result = 1 
		return 
	end
	if @result is null or @result < 1
	begin
		select result = 0
		return 
	end
end  
GO
GRANT EXECUTE on dbo.up_check_doc_is_valid_for_screen TO PUBLIC
GRANT Control on dbo.up_check_doc_is_valid_for_screen TO PUBLIC
GO