/* SP retorna informação do cliente
	
	exec up_select_utente 107830, 0, 0, 'ATLANTICO'
	exec up_select_utente 0, 0, 'ADM000EC1BD-AF08-468C-816', 'MAIANGA'
	exec up_select_utente 219,0 , 'ADM77025F81-7110-4E00-8ED', 'MAIANGA'

	


*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_select_utente]') IS NOT NULL
	DROP PROCEDURE dbo.up_select_utente
GO
IF OBJECT_ID(N'tempdb..#minuta') IS NOT NULL
BEGIN
	DROP TABLE #minuta
END
GO

CREATE PROCEDURE dbo.up_select_utente
	@no				numeric(32)
	,@estab			numeric(32)	
	,@id			varchar(25)
	,@site			varchar(40)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	if @no > 0 and @estab > 0
	begin	  	
		print 'information was not filled'
	end
	else 
	begin
		DECLARE @stampOrigi varchar(25) = ''
		SELECT 
			@stampOrigi = stamporigproc
		FROM   
			ft2 (nolock)
		WHERE 
			ft2stamp = @id

		if @stampOrigi is not null and len(@stampOrigi) > 0
		begin
			SELECT
				@no = no
				,@estab = estab
			FROM   
				ft (nolock)
			WHERE 
				ftstamp = @stampOrigi
		end
		
		if (@no is null or @no = 0) and (@estab is null or @estab = 0)
		begin
			SELECT
				@no = no
				,@estab = estab
			FROM   
				ft (nolock)
			WHERE 
				ftstamp = @id
		end 

		
		create table #minuta(
			text varchar(MAX)
		)	

		insert into #minuta 
		exec up_select_rgpd_texto @site

		SELECT  
			b_utentes.utstamp as utstamp,
			b_utentes.nome as nome,
			b_utentes.no as no,
			b_utentes.estab as estab,
			b_utentes.ncont as ncont,
			b_utentes.morada as morada,
			b_utentes.telefone as telefone,
			b_utentes.tlmvl as tlmvl,
			b_utentes.bino as bino,
			b_utentes.autorizado as autorizado,
			convert(varchar, 
									(select top 1 
												anexos.ousrdata 
										from 
											anexos (nolock) 
										where 
											anexos.regstamp =b_utentes.utstamp 
											and anexos.keyword = 'RGPD Signature'
										ORDER BY
											anexos.ousrdata desc)
					,112) as rgpddataAssinatura, 
				(select top 1 
					anexos.path 
				from 
					anexos (nolock) 
				where 
					anexos.regstamp = b_utentes.utstamp 
					and anexos.keyword = 'RGPD Signature'
					ORDER BY
						anexos.ousrdata desc) as rgpdAssinatura, 
				(select top 1	
					anexos.path 
				from 
					anexos (nolock) 
				where 
					anexos.regstamp = b_utentes.utstamp 
					and anexos.keyword = 'RGPD PDF'
				ORDER BY
					anexos.ousrdata desc)  as rgpdPDF,
		 (select text from #minuta)  as rgpdMinuta 	
		FROM   
			b_utentes (nolock) 
		WHERE 
			no = @no
			AND estab = @estab 
	end
GO
GRANT EXECUTE on dbo.up_select_utente TO PUBLIC
GRANT Control on dbo.up_select_utente TO PUBLIC
GO

IF OBJECT_ID(N'tempdb..#minuta') IS NOT NULL
BEGIN
	DROP TABLE #minuta
END