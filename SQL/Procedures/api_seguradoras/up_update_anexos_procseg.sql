/* SP inserir anexo
	
	exec up_update_anexos_procseg 'ADM935F7015-00E8-4533-ABA' , 'teste 123', '20210913'
	select * from anexos where regstamp='321654'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_update_anexos_procseg]') IS NOT NULL
	DROP PROCEDURE dbo.up_update_anexos_procseg
GO

CREATE PROCEDURE dbo.up_update_anexos_procseg
	 @regstamp			varchar(60) = ''
	,@nomeprocsegur		varchar(250) = ''
	,@dataprocsegur		date = '19000101'


/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

if @regstamp <>'' and @nomeprocsegur<>'' 
	begin 
		BEGIN TRANSACTION;
			UPDATE ft2
			SET    nomeprocsegur = @nomeprocsegur,
			       dataprocsegur = @dataprocsegur
			WHERE  ft2stamp = @regstamp

		COMMIT TRANSACTION;	
		select getdate() AS DateMessag, 1 AS StatMessag, 1 AS ActionMessage, 'UPDATE OK' As DescMessag
			, @regstamp AS regstamp
	end 	
else 
	begin
		select getdate() AS DateMessag, 0 AS StatMessag, 1 AS ActionMessage, 'UPDATE NOK - Falta Informacao' As DescMessag
			, @regstamp AS regstamp
	end 
GO
GRANT EXECUTE on dbo.up_update_anexos_procseg TO PUBLIC
GRANT Control on dbo.up_update_anexos_procseg TO PUBLIC
GO