/* SP inserir anexo
	
	exec up_select_anexos_documentos '101,111', 'ATLANTICO', '19000101', '30000101', 2, 1
	
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_select_anexos_documentos]') IS NOT NULL
	DROP PROCEDURE dbo.up_select_anexos_documentos
GO

CREATE PROCEDURE dbo.up_select_anexos_documentos
		@tipodoc				varchar(25) = ''
	,@site					varchar(25) = ''
	,@dataIni				varchar(25) = ''
	,@dataFim				varchar(25) = ''
	,@topMax				int				= 100 
	,@pageNumber			int				=	1

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	if(isnull(@topMax,0)>100)
	begin
		set @topMax = 100
	end
	if(isnull(@topMax,0)<=0)
	begin
		set @topMax = 100
	end
	if(isnull(@pageNumber,0)<1)
	begin
		set @pageNumber = 1
	end

	DECLARE @PageSize int = isnull(@topMax,100) 

declare @sql as varchar(MAX)
SET @sql ='
	select '
		+ convert(varchar(10),@PageSize) +' as pageSize,
		(COUNT(*) OVER() / ' +  convert(varchar(10),@PageSize) +') + 1 as pageTotal, '
		+ convert(varchar(10),@PageNumber) +' as pageNumber, 
		COUNT(*) OVER() as linesTotal,
		ft.fno
		, ft.no
		, ft.estab
		, ft.fdata
		, ft.ndoc
		, ft.nmdoc
		, ft.ftstamp
		, isnull(ft_seguradora.no, -1) as clientNo
		, isnull(ft_seguradora.estab, -1) as clientDep
		, ft.etotal as retailPrice
		,ft2.nrelegibilidade		as nrelegibilidade
		,(select 
			(case when count(ft_compart.token)> 0 then 1 else 2 end )
		from
			ft_compart (nolock)	 
		where 
			ft_seguradora.ftstamp = ft_compart.ftstamp 
			and ft_compart.type = 2	
			and ft_compart.returnContributionCorrelationId = ft2.nrelegibilidade) as tipoFaturacao
		,(select 
				(case when count(ft_compart.token)> 0 then ''Elegibilidade eletrónica'' else ''Elegibilidade manual'' end )
			from
				ft_compart (nolock)	 
			where 
				ft_seguradora.ftstamp = ft_compart.ftstamp 
				and ft_compart.type = 2	
				and ft_compart.returnContributionCorrelationId = ft2.nrelegibilidade) as descrTipoFaturacao 
	from
		ft (nolock)
	inner join
		td (nolock) on ft.ndoc = td.ndoc
	inner join
		ft2 (nolock) on ft2.ft2stamp = ft.ftstamp
	left join 
		ft (nolock) ft_seguradora on ft_seguradora.ftstamp = ft2.stamporigproc
	where
		td.anexosapp	= 1 
		and ft.site	='''+ @site+'''
		and ft.ndoc	in ('+@tipodoc+')'
	if @dataIni != ''
	begin
		set @sql = @sql + ' 
			and ft.fdata >= ''' + @dataIni + ''''
	end
	if @dataFIm != ''
	begin
		set @sql = @sql + ' 
			and ft.fdata <= ''' + @dataFIm + ''''
	end
	set @sql = @sql + '
	order by 
		ft_seguradora.ftstamp asc
	OFFSET ' + convert(varchar(10),@PageSize) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
	FETCH NEXT  ' + convert(varchar(10),@PageSize) + ' ROWS ONLY '  
	
	
	print @SQL
	exec (@SQL) 
GO
GRANT EXECUTE on dbo.up_select_anexos_documentos TO PUBLIC
GRANT Control on dbo.up_select_anexos_documentos TO PUBLIC
GO