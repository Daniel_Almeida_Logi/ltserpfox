/* SP inserir anexo
	
	exec up_valida_anexos_utente_password 'sa' , '1234'
	select * from anexos where regstamp='321654'
	update anexos set validade=getdate()+30 where regstamp='321654'



*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_valida_anexos_utente_password]') IS NOT NULL
	DROP PROCEDURE dbo.up_valida_anexos_utente_password
GO

CREATE PROCEDURE dbo.up_valida_anexos_utente_password
	 @username				varchar(50)
	,@userpass				varchar(50)
	,@param					varchar(50)


/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

if not exists(
	SELECT  
		nome 
		, iniciais
		, userpass
		, password
		, (select   rtrim(ltrim(isnull(textValue,''))) from B_Parameters (nolock) where stamp = @param) as site
	FROM    
		b_us  (nolock) 
	WHERE   
		username  =  @username
		AND password  =  @userpass
)
begin
	select 
		nome
		, '' as iniciais
		, '' as password 
		, '' as site
	from 
		b_us (nolock)
	where
		username = @username
end
else
begin
	SELECT  
		nome 
		, iniciais
		, userpass
		, password
		, (select rtrim(ltrim(isnull(textValue,''))) from B_Parameters (nolock) where stamp = @param) as site
	FROM    
		b_us  (nolock) 
	WHERE   
		username  =  @username
		AND password  =  @userpass
end 
	
GO
GRANT EXECUTE on dbo.up_valida_anexos_utente_password TO PUBLIC
GRANT Control on dbo.up_valida_anexos_utente_password TO PUBLIC
GO

 