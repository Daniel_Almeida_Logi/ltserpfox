 
--code to validate if the procedure exists, if exists, drops procedure
if OBJECT_ID('[dbo].[up_ApiSeguradoras_guardar_cartao]') IS NOT NULL
	drop procedure dbo.up_ApiSeguradoras_guardar_cartao
go
/*  
	begin tran
		exec up_ApiSeguradoras_guardar_cartão 'CC', 'DACFD073-68D5-4B35-A2FF-B', 'nome ficheiro', 'descr', '2022-10-10', 'caminho para o ficheiro (com nome)'
		select * from anexos (nolock) order by ousrdata desc
	rollback
*/
-- =============================================
-- Author:		José Moreira
-- Create date: 2022-11-07
-- Description: Returns all the products that haven't been processed begining at date "@data_pesquisa"
-- =============================================
CREATE PROCEDURE up_ApiSeguradoras_guardar_cartao
	@type				varchar(3)
	,@utStamp			varchar(25)
	,@filename			varchar(254)
	,@descr				varchar(254)
	,@validade			varchar(254)
	,@path				varchar(254)
	,@ftstamp			varchar(254)
	,@userInitials		varchar(10)
AS
BEGIN TRY
BEGIN TRANSACTION  
	declare @stampAnexo varchar(36) = left(newid(), 10) + replace(replace(replace(convert(varchar, getdate(), 120), ' ', ''), ':', ''), '-', '')	
	declare @tipoCartao varchar(200)
	declare @ousrdata	varchar(23) = convert(varchar, getdate(), 120)
	declare @estado int = 1
	select 
		@descr		= description, 
		@tipoCartao = grouperCodDescription
	from 
		regrasCartaoApiSeguradoras (nolock) 
	where 
		code = @descr

	delete from
		anexos
	where
		regstamp = @utStamp 
		and keyword = @tipoCartao
		and descr = @descr

	insert into 
		anexos (
			anexosstamp
			, regstamp
			, tabela
			, filename
			, descr
			, tipo
			, validade
			, path
			, ousrdata
			, keyword
			, ousrinis
			, protected
			, usrdata
			, usrinis
		 )
	values 
		(	@stampAnexo
			, @utStamp
			, 'b_utentes'
			, @filename
			, @descr
			, @descr
			, @validade
			, @path
			, @ousrdata
			, @tipoCartao
			, @userInitials
			, 0
			, @ousrdata
			, @userInitials
		) 
	
	update
		estadoProcessoAPISeguradoras
	set
		valido = 0
	where
		ftstamp = @ftstamp
		and ousrdata != @ousrdata
		and tipoAnexo = @descr

	insert into
		estadoProcessoAPISeguradoras
			( stampAnexos
			,tipoAnexo
			,utstamp
			,ftstamp
			,path
			,ousrdata
			,valido
			,estado
			,descEstado
			,ousrinis
			)
		select
			@stampAnexo
			, @descr
			, @utStamp
			, @ftstamp
			, @path
			, @ousrdata
			, 1
			, @estado
			,(select campo from b_multidata (nolock) where code_motive = @estado and tipo = 'estadoProcesso')
			,@userInitials
		where 
			@ftstamp != ''
END TRY
begin catch
	if (@@TRANCOUNT > 0) 
	begin
		print 'a' 
		rollback transaction   
		insert into
			regErrorSQL 
				(stamp
				,callName
				,errorMessage
				,errorNumber
				,error_state
				,error_severity
				,error_procedure
				,error_line 
				,descr
				,TYPEID
				,TYPEDesc
				,site
				,ousrinis
				,ousrdata
				)
			values(  
				/*stamp*/ left(newid(), 10) + replace(replace(replace(convert(varchar, getdate(), 120), ' ', ''), ':', ''), '-', '') 
				/*callName*/,'up_ApiSeguradoras_guardar_cartão'
				/*errorMessage*/, ERROR_MESSAGE()
				/*errorNumber*/,convert(varchar, ERROR_NUMBER())
				,convert(varchar, ERROR_STATE())
				,convert(varchar, ERROR_SEVERITY())
				,ERROR_PROCEDURE()
				,convert(varchar, ERROR_LINE())
				,convert(varchar, ERROR_NUMBER())+'-'+ERROR_MESSAGE() + '-' + convert(varchar, ERROR_STATE()) + '-' + convert(varchar, ERROR_SEVERITY()) 
						+ '-' + ERROR_PROCEDURE() + '-' + convert(varchar, ERROR_LINE())
				,6
				,'Api Seguradoras'
				,''
				, 'API Seguradoras'
				, convert(varchar, getdate(), 120)
				)

		/*Inserir na tabela de erros*/
	end  		 
end catch
	print 'b'

	select top 1
		* 
	from 
		anexos (nolock)
	left join
		estadoProcessoAPISeguradoras (nolock) on	estadoProcessoAPISeguradoras.stampAnexos = anexos.anexosstamp
	where 
		anexos.regstamp = @utStamp 
		and anexos.filename = @filename
		and anexos.path = @path 
		and anexos.validade = @validade
		commit transaction
GO 

 