

--code to validate if the procedure exists, if exists, drops procedure
if OBJECT_ID('[dbo].[up_validar_se_anexo_existe]') IS NOT NULL
	drop procedure dbo.up_validar_se_anexo_existe
go
/* 
	exec up_validar_se_anexo_existe 'CC', '714CE46C-1A82-437A-BE43-E'
	exec up_validar_se_anexo_existe 'CS', 'DACFD073-68D5-4B35-A2FF-B'
*/
-- =============================================
-- Author:		José Moreira
-- Create date: 2022-11-07
-- Description: Returns all the products that haven't been processed begining at date "@data_pesquisa"
-- =============================================
CREATE PROCEDURE up_validar_se_anexo_existe
	@type			varchar(3),
	@userStamp			varchar(36)	
AS
Declare
	@stampAnexoFrente	varchar(36),
	@stampAnexoVerso	varchar(36)
BEGIN     
		 
	select top 1
		@stampAnexoFrente =  anexosstamp
	from
		anexos (nolock)
	where
		regstamp = @userStamp
		and tabela = 'b_utentes'
		and tipo in (select distinct campo from b_multidata (nolock) where tipo = 'anexos' and code_motive = @type)
		and filename != ''	  
		and path != '' 
		and upper(descr) like '%FRENTE%'
	order by 
		ousrdata desc
		
	select top 1
		@stampAnexoVerso =  anexosstamp
	from
		anexos (nolock)
	where
		regstamp = @userStamp
		and tabela = 'b_utentes'
		and tipo in (select distinct campo from b_multidata (nolock) where tipo = 'anexos' and code_motive = @type)
		and filename != ''	  
		and path != '' 
		and upper(descr) like '%VERSO%'
	order by 
		ousrdata desc 

	select      
		anexosstamp
		,regstamp
		,tabela
		,filename
		,descr
		,keyword
		,protected  
		,tipo
		,validade
		,readOnly
		,path 
	
	from
		anexos (nolock)
	where
		anexosstamp in (@stampAnexoFrente, @stampAnexoVerso)
	order by 
		ousrdata desc
END
GO 

 