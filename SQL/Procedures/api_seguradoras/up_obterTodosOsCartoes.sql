

--code to validate if the procedure exists, if exists, drops procedure
if OBJECT_ID('[dbo].[up_obterTodosOsCartoes]') IS NOT NULL
	drop procedure dbo.up_obterTodosOsCartoes
go
/* 
	exec up_obterTodosOsCartoes '8228ABF3-29B3-4499-90' 
*/
-- =============================================
-- Author:		José Moreira
-- Create date: 2022-11-07
-- Description: Returns all the products that haven't been processed begining at date "@data_pesquisa"
-- =============================================
CREATE PROCEDURE up_obterTodosOsCartoes 
	@userStamp			varchar(36)	
AS
Declare
	@stampAnexoFrente	varchar(36),
	@stampAnexoVerso	varchar(36)
BEGIN     		 
	IF OBJECT_ID(N'tempdb..#tipoanexo') IS NOT NULL
	BEGIN
		DROP TABLE #tipoanexo
	END 
	IF OBJECT_ID(N'tempdb..#allcards') IS NOT NULL
	BEGIN
		DROP TABLE #allcards
	END 

	create table #tipoanexo(nome varchar(100))
	
	insert into #tipoanexo (nome) values ('CA')
	insert into #tipoanexo (nome) values ('CC')
	insert into #tipoanexo (nome) values ('CS') 
	
	create table #allcards(
		anexosstamp	varchar(30),
		regstamp varchar(60),
		tabela varchar(30),
		filename varchar(254),
		descr varchar(254),
		keyword varchar(254),
		protected varchar(1),
		tipo varchar(30),
		validade date,
		readOnly bit,
		path varchar(254)
	)
	insert into #allcards
	select  
		anexosstamp
		,regstamp
		,tabela
		,filename
		,descr
		,keyword
		,protected  
		,tipo
		,validade
		,readOnly
		,path 
	from
		anexos (nolock)
	where
		regstamp = @userStamp
		and tabela = 'b_utentes'
		and tipo in (select distinct campo COLLATE DATABASE_DEFAULT  from b_multidata (nolock) where tipo = 'anexos' and code_motive in (select nome COLLATE DATABASE_DEFAULT from #tipoanexo))
		and filename != ''	  
		and path != '' 
		and upper(descr) like '%FRENTE%'
	order by 
		ousrdata desc

		
		
	insert into #allcards
	select 
		anexosstamp
		,regstamp
		,tabela
		,filename
		,descr
		,keyword
		,protected  
		,tipo
		,validade
		,readOnly
		,path 
	from
		anexos (nolock)
	where
		regstamp = @userStamp
		and tabela = 'b_utentes'
		and tipo in (select distinct campo COLLATE DATABASE_DEFAULT  from b_multidata (nolock) where tipo = 'anexos' and code_motive in (select nome COLLATE DATABASE_DEFAULT  from #tipoanexo))
		and filename != ''	  
		and path != '' 
		and upper(descr) like '%VERSO%'
	order by 
		ousrdata desc 


		
	select      
		anexosstamp
		,regstamp
		,tabela
		,filename
		,descr
		,keyword
		,protected  
		,tipo
		,validade
		,readOnly
		,path 
	from
		#allcards (nolock)
	order by 
		tipo
	 

	IF OBJECT_ID(N'tempdb..#tipoanexo') IS NOT NULL
	BEGIN
		DROP TABLE #tipoanexo
	END 
	IF OBJECT_ID(N'tempdb..#allcards') IS NOT NULL
	BEGIN
		DROP TABLE #allcards
	END 
END
GO 
 