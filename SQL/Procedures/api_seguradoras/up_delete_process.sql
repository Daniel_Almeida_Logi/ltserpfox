
IF OBJECT_ID('[dbo].[up_delete_process]') IS NOT NULL

    DROP procedure dbo.up_delete_process
GO
/*
EXEC up_delete_process  'ADM77025F81-7110-4E00-8ED'
*/
CREATE procedure dbo.up_delete_process 
	 @ftstamp			varchar(60)		= '' 
	 ,@userInitials		varchar(10)		= ''
	 ,@site				varchar(100)	= ''
AS  
 
begin try 
	PRINT 'A'
	declare @sql varchar(MAX)
	IF OBJECT_ID(N'tempdb..#anexosStamp') IS NOT NULL
	BEGIN
		DROP TABLE #anexosStamp
	END
	PRINT 'B'
	create table #anexosStamp(
		anexosStamp varchar(36),
		tipoAnexo varchar(36) 
	)
	
	select @sql = '
		insert into  
			#anexosStamp (anexosStamp,tipoAnexo)
		select 
			stampAnexos	
			,tipoAnexo
		from
			estadoProcessoAPISeguradoras (nolock) 
		where
			ftstamp = '''+ @ftstamp + 
		+ '''and valido = 1 
			and( utstamp = '''' or tipoanexo like (''Cartão Adquiriente%''))
		DELETE FROM 
			anexos
		WHERE
			anexosStamp in (select anexosStamp from #anexosStamp)'
		PRINT @SQL
		EXEC (@sql)
	PRINT 'C'
		update
			estadoProcessoAPISeguradoras
		set
			valido = 0
		where 
			ftstamp = @ftstamp and valido = 1 
		insert into	
			B_ocorrencias
		(
			stamp
			,linkStamp
			,tipo
			,grau
			,descr
			,date
			,site
			,usr
		)
		select
			left(newid(),11) + replace(replace(replace(convert(varchar, getdate(), 120), ' ', ''), ':', ''), '-', '')
			,anexosStamp
			,'APISeguradoras-' +tipoAnexo
			,99
			,'APAGAR PROCESSO'
			,convert(varchar, getdate(), 120)
			,@site
			,(select 
					userno
			from 
				b_us (nolock)
			where b_us.iniciais = @userInitials)
		from
			#anexosStamp (nolock)
	PRINT 'D'
		IF OBJECT_ID(N'tempdb..#anexosStamp') IS NOT NULL
		BEGIN
			DROP TABLE #anexosStamp
		END
	PRINT 'E'
		select result = 1
	PRINT 'F'
		return
end try
begin catch 
	select result = 0
end catch
GO
GRANT EXECUTE on dbo.up_delete_process TO PUBLIC
GRANT Control on dbo.up_delete_process TO PUBLIC
GO