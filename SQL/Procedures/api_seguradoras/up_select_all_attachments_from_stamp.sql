/****** Object:  StoredProcedure [dbo].[up_select_process_pdf_path]    Script Date: 22/11/2022 10:33:00 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

/*
	exec up_select_all_attachments_from_stamp 'ADM77025F81-7110-4E00-8ED'
*/

if OBJECT_ID('[dbo].[up_select_all_attachments_from_stamp]') IS NOT NULL
	drop procedure up_select_all_attachments_from_stamp
go

Create procedure [dbo].up_select_all_attachments_from_stamp
	@FTSTAMP varchar(36)
/* with encryption */
AS
begin

SET NOCOUNT ON

	select 
		path 
	from 
		estadoProcessoAPISeguradoras (nolock)
	where 
		estadoProcessoAPISeguradoras.valido = 1 
		and estadoProcessoAPISeguradoras.ftstamp = @FTSTAMP
		and estadoProcessoAPISeguradoras.tipoAnexo not like('Processo - Pasta')
 end
GO
