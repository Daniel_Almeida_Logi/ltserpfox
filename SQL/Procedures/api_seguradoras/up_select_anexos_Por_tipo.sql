/* SP inserir anexo
	
	exec up_select_anexos_Por_tipo 'ADM77025F81-7110-4E00-8ED', 'invoice ' 
	

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_select_anexos_Por_tipo]') IS NOT NULL
	DROP PROCEDURE dbo.up_select_anexos_Por_tipo
GO

CREATE PROCEDURE [dbo].[up_select_anexos_Por_tipo]
	@stamp varchar(36),
	@tipo varchar(30)
/* WITH ENCRYPTION */
AS

SET NOCOUNT ON
	select 
		anexos.anexosstamp,
		anexos.regstamp,
		anexos.tabela,
		anexos.filename,
		anexos.descr,
		anexos.keyword,
		anexos.protected,
		anexos.tipo,
		anexos.path,
		anexos.ousrdata,
		anexos.ousrinis,
		anexos.usrdata,
		anexos.usrinis,
		anexos.validade,
		anexos.path
	from
		estadoProcessoAPISeguradoras (nolock)
	inner join
		anexos (nolock)
	on	estadoProcessoAPISeguradoras.stampAnexos = anexos.anexosstamp
	where 
		anexos.regstamp = @stamp
		and anexos.tipo = @tipo

GO
GRANT EXECUTE on dbo.up_select_anexos_tipodoc TO PUBLIC
GRANT Control on dbo.up_select_anexos_tipodoc TO PUBLIC
GO