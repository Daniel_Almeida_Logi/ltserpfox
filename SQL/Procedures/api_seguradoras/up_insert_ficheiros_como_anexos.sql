 /****** Object:  StoredProcedure [dbo].[up_insert_ficheiros_como_anexos]    Script Date: 11/10/2021 17:39:17 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_insert_ficheiros_como_anexos]') IS NOT NULL
	DROP PROCEDURE dbo.up_insert_ficheiros_como_anexos
GO

CREATE PROCEDURE [dbo].[up_insert_ficheiros_como_anexos]
	 @regstamp			varchar(60)		= ''
	,@tabela			varchar(60)		= ''
	,@filename			varchar(254)	= ''
	,@descr				varchar(254)	= '' 
	,@tipo				varchar(25)		= ''
	,@keyword			varchar(254)	= ''
	,@path				varchar(254)	= ''
	,@userInitials		varchar(10)	= ''
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

declare 
	@validade			date 
	,@result            bit  
	,@stampAnexo		varchar(36) = left(newid(),11) + replace(replace(replace(convert(varchar, getdate(), 120), ':', ''), '-', ''), ' ', '')
	,@ousrdata			varchar(23) = convert(varchar, GETDATE(), 120)
	,@estado int = 1
	,@sql				varchar(8000)
	
set @result = isnull(@result,1)
set @validade = isnull(@validade, '19000101') 

if @regstamp <>'' and @tabela<>'' and @filename<>'' and @descr<>'' and @userInitials<>'' and @tipo<>''
begin 
	BEGIN TRANSACTION;
		IF OBJECT_ID(N'tempdb..#anexosStamp') IS NOT NULL
		BEGIN
			DROP TABLE #anexosStamp
		END

		create table #anexosStamp(anexosStamp varchar(36))

		select @sql = '
		insert into  
			#anexosStamp (anexosStamp)
		select 
			anexosStamp	
		from
			anexos (nolock) 
		where
			regstamp = ''' + @regstamp + '''   
			and keyword = ''' + @keyword + ''
			+ (case when @keyword != 'Processo' then ''' and filename = ''' +@filename else '' end)
		+ ''' 
		DELETE FROM 
			anexos
		WHERE
			regstamp = ''' + @regstamp + '''
			and keyword = ''' + @keyword + ''
		+ (case when @keyword != 'Processo' then ''' and filename = ''' +@filename + '''' else '''' end)

		PRINT @SQL
		EXEC (@sql)
		 
		INSERT INTO 
			anexos
			   (anexosstamp,
				regstamp,
				tabela,
				filename,
				descr,
				keyword,
				protected,
				ousrdata,
				ousrinis,
				usrdata,
				usrinis,
				tipo,
				validade,
				path)
			select @stampAnexo,
				@regstamp,
				@tabela,
				@filename,
				@descr,
				@keyword,
				0,
				Getdate(),
				@userInitials,
				Getdate(),
				@userInitials,
				@tipo,
				@validade,
				@path
		  
			
		update
			estadoProcessoAPISeguradoras
		set
			valido = 0
		where
			--ftstamp = @regstamp
			--and ousrdata != @ousrdata
			--and tipoAnexo = @descr
			--and valido = 1
			--and 
			stampAnexos COLLATE DATABASE_DEFAULT in (select anexosStamp  COLLATE DATABASE_DEFAULT from #anexosStamp)

		insert into
			estadoProcessoAPISeguradoras
				( stampAnexos
				,tipoAnexo
				,utstamp
				,path
				,ousrdata
				,ftstamp
				,valido
				,estado
				,descEstado
				,ousrinis
				)
			values
				(@stampAnexo
				, @descr
				, ''
				, @path
				, @ousrdata
				,@regstamp
				,1
				, @estado
				,(select campo COLLATE DATABASE_DEFAULT  from b_multidata (nolock) where code_motive = @estado and tipo = 'estadoProcesso')
				,@userInitials
				)

		IF OBJECT_ID(N'tempdb..#anexosStamp') IS NOT NULL
		BEGIN
			DROP TABLE #anexosStamp
		END
		
		if upper(@tipo) like '%RGPD%' and @tabela='B_UTENTES'
		begin
			update b_utentes set autorizado=1, usrdata=GETDATE(), autoriza_emails=1, autoriza_sms=1 where utstamp=@regstamp
		end 
	COMMIT TRANSACTION;	

	if(@result=1)
	begin
		select getdate() AS DateMessag, 1 AS StatMessag, 1 AS ActionMessage, 'INSERT OK' As DescMessag , @regstamp AS regstamp
	end
end 	
else 
begin
	if(@result=1)
	begin
		select getdate() AS DateMessag, 0 AS StatMessag, 1 AS ActionMessage, 'INSERT NOK - Falta Informação' As DescMessag, @regstamp AS regstamp
	end
end 
	
GO
 