/* SP inserir anexo
	
	exec up_check_process_exist 'ADM041D8795-E905-4DBD-952'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_check_process_exist]') IS NOT NULL
	DROP PROCEDURE dbo.up_check_process_exist
GO

CREATE PROCEDURE dbo.up_check_process_exist
	@ftstamp				varchar(25) = ''


/* WITH ENCRYPTION */
AS
	declare @path varchar(8000)

SET NOCOUNT ON
	select top 1 
		@path = estadoProcessoAPISeguradoras.path
	from
		estadoProcessoAPISeguradoras (nolock)
 	where 
		  estadoProcessoAPISeguradoras.ftstamp = @ftstamp
		and estadoProcessoAPISeguradoras.tipoAnexo like '%Processo - Pasta%'
		and estadoProcessoAPISeguradoras.valido = 1

	if @path is not null and len(ltrim(rtrim(@path)))>0
		select @path as path,  ndoc, fno, year(fdata) as ano from ft (nolock) where ftstamp = @ftstamp
	else
	begin
		select '' as path, ndoc, fno, year(fdata) as ano from ft (nolock) where ftstamp =  @ftstamp
	end
GO
GRANT EXECUTE on dbo.up_check_process_exist TO PUBLIC
GRANT Control on dbo.up_check_process_exist TO PUBLIC
GO