/* SP inserir anexo
	
	exec up_select_cards_path 'ATLANTICO', 'ADM77025F81-7110-4E00-8ED'
 
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_select_cards_path]') IS NOT NULL
	DROP PROCEDURE dbo.up_select_cards_path
GO

CREATE PROCEDURE dbo.up_select_cards_path
	@site				varchar(25) = ''
	,@ftstamp			varchar(25) = ''


/* WITH ENCRYPTION */
AS 
SET NOCOUNT ON

	if @ftstamp = '' 
	begin 
		SELECT 
			ltrim(rtrim(textvalue))+ '\utentes' as path
		FROM	
			b_parameters_site (nolock)
		WHERE	
			stamp = 'ADM0000000010'
			AND site = @site
	end
	if @ftstamp != '' 
	begin
		select top 1 
			 path + '\cartoes' as path
		from
			estadoProcessoAPISeguradoras (nolock) 
		where 
			estadoProcessoAPISeguradoras.ftstamp = @ftstamp 
			and estadoProcessoAPISeguradoras.tipoAnexo like '%Processo - pasta%'
		order by 
			ousrdata desc
	end


GO
GRANT EXECUTE on dbo.up_select_cards_path TO PUBLIC
GRANT Control on dbo.up_select_cards_path TO PUBLIC
GO