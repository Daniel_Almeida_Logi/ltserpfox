/****** Object:  StoredProcedure [dbo].[up_insert_anexos]    Script Date: 10/03/2023 09:55:12 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_insert_anexos]') IS NOT NULL
	DROP PROCEDURE dbo.up_insert_anexos
GO


CREATE PROCEDURE [dbo].[up_insert_anexos]
	 @regstamp			varchar(60) = ''
	,@tabela			varchar(60) = ''
	,@filename			varchar(254) = ''
	,@descr				varchar(254) = ''
	,@usrinis			varchar(25) = ''
	,@tipo				varchar(25) = ''
	,@keyword			varchar(25) = ''
	,@path				varchar(200) = ''
	,@validade			date = '19000101'
	,@result            bit = 1
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON


set @result = isnull(@result,1)
set @validade = isnull(@validade, '19000101')


if @regstamp <>'' and @tabela<>'' and @filename<>'' and @descr<>'' and @usrinis<>'' and @tipo<>''
	begin 
		BEGIN TRANSACTION;
			DELETE 
			FROM
				anexos
			where
				regstamp = @regstamp
				AND tabela = tabela
				AND keyword = @keyword
			INSERT INTO anexos
				   (anexosstamp,
					regstamp,
					tabela,
					filename,
					descr,
					keyword,
					protected,
					ousrdata,
					ousrinis,
					usrdata,
					usrinis,
					tipo,
					validade,
					path)
			select  LEFT(NEWID(),21),
					@regstamp,
					@tabela,
					@filename,
					@descr,
					@keyword,
					0,
					Getdate(), 
					@usrinis,
					Getdate(),
					@usrinis,
					@tipo,
					@validade,
					@path
			if upper(@tipo) like '%RGPD%' and @tabela='B_UTENTES'
			begin
				update b_utentes set autorizado=1, usrdata=GETDATE(), autoriza_emails=1, autoriza_sms=1 where utstamp=@regstamp
			end 
		COMMIT TRANSACTION;	

		if(@result=1)
			select getdate() AS DateMessag, 1 AS StatMessag, 1 AS ActionMessage, 'INSERT OK' As DescMessag , @regstamp AS regstamp
	end 	
else 

	begin
		if(@result=1)
			select getdate() AS DateMessag, 0 AS StatMessag, 1 AS ActionMessage, 'INSERT NOK - Falta Informação' As DescMessag, @regstamp AS regstamp
	end 
	
