/****** Object:  StoredProcedure [dbo].[up_select_process_pdf_path]    Script Date: 22/11/2022 10:33:00 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

/*
	exec up_select_process_info 'ADM77025F81-7110-4E00-8ED'
*/

if OBJECT_ID('[dbo].[up_select_process_info]') IS NOT NULL
	drop procedure up_select_process_info
go

Create procedure [dbo].up_select_process_info
	@FTSTAMP varchar(36)
/* with encryption */
AS
begin

SET NOCOUNT ON

select 
	ft_seguradora.ftstamp							as ftstamp,
	ft_cliente.estab								as clientDep,
	ft_cliente.no									as clientNumber,
	ft_seguradora.estab								as dep,
	ft_seguradora.no								as number,
	convert(varchar, ft_seguradora.fdata, 111)		as date,
	ft_seguradora.fno								as docNumber,
	ft_seguradora.ndoc								as docType,
	ft_seguradora.nmdoc								as descDocType
from 
	ft as ft_seguradora(nolock) 
inner join
	ft2 (nolock) on ft_seguradora.ftstamp = ft2.ft2stamp
left join
	ft as ft_cliente (nolock) on ft_cliente.ftstamp = ft2.stamporigproc
where 
	ft_seguradora.ftstamp = @FTSTAMP

end
GO
