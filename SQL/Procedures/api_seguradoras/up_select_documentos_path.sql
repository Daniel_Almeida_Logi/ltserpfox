/* SP inserir anexo
	
	exec up_select_documentos_path 'ATLANTICO'
	select * from anexos where regstamp='321654'
	update anexos set validade=getdate()+30 where regstamp='321654'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_select_documentos_path]') IS NOT NULL
	DROP PROCEDURE dbo.up_select_documentos_path
GO

CREATE PROCEDURE dbo.up_select_documentos_path
	@site				varchar(25) = ''


/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	SELECT 
		ltrim(rtrim(textvalue))+'\doccs' as path
	FROM	b_parameters_site
	WHERE	stamp = 'ADM0000000010'
		AND site = @site
	
GO
GRANT EXECUTE on dbo.up_select_documentos_path TO PUBLIC
GRANT Control on dbo.up_select_documentos_path TO PUBLIC
GO