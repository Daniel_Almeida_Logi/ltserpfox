/****** Object:  StoredProcedure [dbo].[up_select_process_pdf_path]    Script Date: 22/11/2022 10:33:00 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

/*
	exec up_select_process_XML_path 'ADM77025F81-7110-4E00-8ED'
*/

if OBJECT_ID('[dbo].[up_select_process_XML_path]') IS NOT NULL
	drop procedure up_select_process_XML_path
go

Create procedure [dbo].up_select_process_XML_path
	@FTSTAMP varchar(36)
/* with encryption */
AS
begin

SET NOCOUNT ON

 select top 1 path from estadoProcessoAPISeguradoras where valido = 1 and tipoAnexo = 'Processo - XML' and ftstamp = @FTSTAMP
 end
GO
 
GRANT EXECUTE on dbo.up_select_process_XML_path TO PUBLIC
GRANT Control on dbo.up_select_process_XML_path TO PUBLIC
GO
