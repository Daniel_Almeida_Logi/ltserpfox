/*  get de informacao da farmacia de integracao de um tipo

	exec up_get_InfoXML_appseg 'MR72F83782-7760-4F0D-BD2 ','aaaa', 'Outro'

	select * from ft(nolock) where ft.nmdoc   like '%segur%' and ft.ftano = 2023
select *from ft(nolock) where ftstamp = 'MIM3E7EF8BA-9C1F-41F3-847 '
select ft2.stamporigproc,*from ft2 where ft2stamp = 'MIM3E7EF8BA-9C1F-41F3-847'




*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_InfoXML_appseg]') IS NOT NULL
	drop procedure dbo.up_get_InfoXML_appseg
go

CREATE PROCEDURE [dbo].[up_get_InfoXML_appseg]
	@stamp			VARCHAR(60),
	@nmficheiro		VARCHAR(254),
	@proc           VARCHAR(50)
AS
SET NOCOUNT ON


	
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosApp'))
		DROP TABLE #dadosApp


				select 
				top 1
					processo		= isnull(@proc,''),
					nrdocumento		= isnull(left(isnull((select invoiceType from b_cert(nolock) bce where ft.ftstamp=bce.stamp),'FT') + ' ' + convert(varchar,ft.ndoc) +  convert(varchar,ft.ftano) + '/' + CONVERT(varchar,ft.fno),60),''),
					tipodoc			= isnull(ft.nmdoc,''),
					seguradora		= isnull(ft.nome,''),
					loja			= isnull(siteExt,''),
					nmficheiro		= isnull(@nmficheiro,''),
					datadocumento	= isnull(CONVERT(varchar, ft.fdata, 103),''),
					valorliquido	= isnull(cast(ft.ettiliq as numeric(15,2)),0.00),
					utenteOrig		= isnull((select nome from ft ftcl (nolock) where ftcl.ftstamp=ft2.stamporigproc),''),
					transacao		= isnull(nrelegibilidade,''),
					posto			= isnull(ft.pno,''),
					telefoneOrig	= isnull((select telefone from ft ftcl (nolock) where ftcl.ftstamp=ft2.stamporigproc),''),
					morada			= isnull(ft.morada,''),
					contribuinte	= case when isnull(ft.ncont,'') = '' then isnull((select top 1 ltrim(rtrim(textvalue)) from B_Parameters_site(nolock) where B_Parameters_site.stamp='ADM0000000075' and B_Parameters_site.site=ft.site),'') else  isnull(ft.ncont,'') end,
					nreferencial	= isnull(td.nmdocext+'-'+empresa.siteext+'-'+substring(convert(varchar(4),ft.ftano),3,2)+'-'+convert(varchar(6),format(ft.fno, '000000')),''),
					tipoSaft        = isnull(td.tiposaft,''),
					isSeguradora    = case when isnull(ft.nmdoc,'') like '%segur%' then 1 else 0 end,
					ftstamp         = ft.ftstamp,
					nome			= ft.nome,
					telefone		= ft.telefone,
					moradaOrig		= isnull((select morada from ft ftcl (nolock) where ftcl.ftstamp=ft2.stamporigproc),''),
					nbenefOrig      = isnull((select u_nbenef2 from ft ftcl (nolock) where ftcl.ftstamp=ft2.stamporigproc),''),
					nbenef          = ft2.u_nbenef2

				into #dadosApp
				from ft (nolock)
				inner join ft2 (nolock) on ft.ftstamp=ft2.ft2stamp
				inner join td (nolock) on td.ndoc=ft.ndoc and td.site=ft.site
				inner join empresa (nolock) on empresa.site=ft.site
				where ftstamp=@stamp
				order by ft.ousrdata desc

		select 
			processo = case when isSeguradora=1 then 'Seguradoras' 
							when tipoSaft = 'FT'  or  tipoSaft = 'FR'  then  'Receitas' 
							when tipoSaft = 'NC'   then  'Creditos'  
							else rtrim(ltrim(tipodoc)) 
						end
			,nrdocumento
			,tipodoc = case when isSeguradora=1 and tipoSaft = 'NC' then 'NCSEG'
							when isSeguradora=1 and tipoSaft = 'FT' then 'Ft.Seguradoras'
							when tipoSaft = 'NC' and tipodoc like '%reg%' and isSeguradora=0  then  'RC'  
							else rtrim(ltrim(tipoSaft)) 
						end
			,seguradora =  
				case when isSeguradora = 0 and #dadosApp.contribuinte like '%99999%' then 'Cliente Final' 
					 when isSeguradora = 0 and #dadosApp.contribuinte not like '%99999%' then  rtrim(ltrim(#dadosApp.nome)) 
					 else rtrim(ltrim(seguradora)) end
			,loja
			,nmficheiro
			,datadocumento
			,valorliquido
			,utente = 
					case   
						when isSeguradora = 0 then rtrim(ltrim(nome)) + case when  nbenef!='' then '-'  + nbenef else '' end
					    else rtrim(ltrim(utenteOrig)) + case when  nbenefOrig!='' then '-'  + nbenefOrig else '' end										
					end
			,transacao
			,posto
			,telefone = case when isSeguradora = 0 then rtrim(ltrim(telefone)) else rtrim(ltrim(telefoneOrig)) end
			,morada 
			,contribuinte
			,nreferencial
		from 
	
		#dadosApp
	
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosApp'))
		DROP TABLE #dadosApp
	
GO
GRANT EXECUTE ON dbo.up_get_InfoXML_appseg to Public
GRANT CONTROL ON dbo.up_get_InfoXML_appseg to Public
GO


