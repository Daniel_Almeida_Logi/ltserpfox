/* SP inserir anexo	
	exec up_select_rgpd_texto 'VIANAPARK'
	select * from anexos where regstamp='321654'
	update anexos set validade=getdate()+30 where regstamp='321654'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_select_rgpd_texto]') IS NOT NULL
	DROP PROCEDURE dbo.up_select_rgpd_texto
GO

CREATE PROCEDURE dbo.up_select_rgpd_texto
	 @site				varchar(20)


/* WITH ENCRYPTION */
AS

SET NOCOUNT ON
	select top 1 email_body as texto
	from b_sms (nolock) 
	where tipoenvio='MINUTA' 
		and codminuta='RGPD-MINUTA' 
		and site=@site 
	order by ousrdata
	
GO
GRANT EXECUTE on dbo.up_select_rgpd_texto TO PUBLIC
GRANT Control on dbo.up_select_rgpd_texto TO PUBLIC
GO
