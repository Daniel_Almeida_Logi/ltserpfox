
/****** 

exec up_get_document_info '288','110','ATLANTICO',2023

 ******/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



if OBJECT_ID('[dbo].[up_get_document_info]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_document_info
GO

CREATE PROCEDURE [dbo].[up_get_document_info]
	 @fno			varchar(60)		= ''
	,@ndoc			varchar(60)		= ''
	,@site			varchar(254)	= ''
	,@ano			varchar(254)	= '' 
	,@ftstamp		varchar(254)	= '' 
	 
/* WITH ENCRYPTION */
AS 
begin
	if(LEN(@ftstamp)= 0)
	begin
		select 
			ft_seguradora.ftstamp, 
			ft_seguradora.fdata, 
			ft_seguradora.fno, 
			ft_seguradora.ndoc,
			ft_seguradora.nmdoc, 
			ft_seguradora.no as segno,
			ft_seguradora.estab as segestab, 
			isnull(ft_client.no, -1) as clino,		
			isnull(ft_client.estab, -1) as cliestab,
			convert(numeric(18,2), round(ft_seguradora.etotal, 2)) as etotal,
			ft2.nrelegibilidade,
			(select 
				(case when count(ft_compart.token)> 0 then 1 else 2 end )
			from
				ft_compart (nolock)	 
			where 		
				 ft_compart.returnContributionCorrelationId = ft2.nrelegibilidade and ft2.nrelegibilidade!='' ) as tipoFaturacao,
			(select 
				(case when count(ft_compart.token)> 0 then 'Elegibilidade eletrónica' else 'Elegibilidade manual' end )
			from
				ft_compart (nolock)	 
			where 
				ft_compart.returnContributionCorrelationId = ft2.nrelegibilidade  and ft2.nrelegibilidade!='') as descrTipoFaturacao 
		from 
			FT (NOLOCK) as ft_seguradora  
		inner join 
			ft2 (nolock) on ft_seguradora.ftstamp = ft2.ft2stamp
		left join 
			ft ft_client (nolock) on ft_client.ftstamp = ft2.stamporigproc
		where 
			ft_seguradora.fno				= @fno 
			and ft_seguradora.ndoc			= @ndoc 
			and ft_seguradora.site			= @site 
			and ft_seguradora.ftano			= @ano 
	end
	else
	begin
		select 
			ft_seguradora.ftstamp, 
			ft_seguradora.fdata, 
			ft_seguradora.fno, 
			ft_seguradora.ndoc,
			ft_seguradora.nmdoc, 
			ft_seguradora.no as segno,
			ft_seguradora.estab as segestab, 
			isnull(ft_client.no, -1) as clino,		
			isnull(ft_client.estab, -1) as cliestab,
			convert(numeric(18,2), round(ft_seguradora.etotal, 2)) as etotal,
			ft2.nrelegibilidade,
			(select 
				(case when count(ft_compart.token)> 0 then 1 else 2 end )
			from
				ft_compart (nolock)	 
			where 
				ft_compart.returnContributionCorrelationId = ft2.nrelegibilidade  and ft2.nrelegibilidade!='') as tipoFaturacao,
			(select 
				(case when count(ft_compart.token)> 0 then 'Elegibilidade eletrónica' else 'Elegibilidade manual' end )
			from
				ft_compart (nolock)	 
			where 
				ft_compart.returnContributionCorrelationId = ft2.nrelegibilidade  and ft2.nrelegibilidade!='') as descrTipoFaturacao ,
			(select top 1
				estadoProcessoAPISeguradoras.path
			from
				estadoProcessoAPISeguradoras (nolock)
			where 
				estadoProcessoAPISeguradoras.ftstamp = ft_seguradora.ftstamp
				and estadoProcessoAPISeguradoras.tipoAnexo = 'Processo - pasta'
				and valido = 1)	as path
		from 
			FT (NOLOCK) as ft_seguradora  
		inner join 
			ft2 (nolock) on ft_seguradora.ftstamp = ft2.ft2stamp
		left join 
			ft ft_client (nolock) on ft_client.ftstamp = ft2.stamporigproc
		where 
			ft_seguradora.ftstamp = @ftstamp
	end
end 
	
GO
