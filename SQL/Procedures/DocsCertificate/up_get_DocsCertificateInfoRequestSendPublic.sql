
/* SP Pesquisa Documentos

	exec up_get_DocsCertificateInfoRequestSendPublic '46C3A331-3868-4DF4-8850-0F78ED12CC20'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_DocsCertificateInfoRequestSendPublic]') IS NOT NULL
	DROP PROCEDURE [dbo].[up_get_DocsCertificateInfoRequestSendPublic] ;
GO

CREATE PROCEDURE [dbo].[up_get_DocsCertificateInfoRequestSendPublic]
	@token varchar(36)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

DECLARE @filePathToSign VARCHAR(254)
DECLARE @nameDocToSign  VARCHAR(254)
DECLARE @TypeDoc VARCHAR(50)

	SELECT TOP 1 
		@filePathToSign = filePathToSign,
		@nameDocToSign = nameDocToSign,
		@TypeDoc =CASE WHEN tiposaft='FT' THEN    'INVOICE'
					   WHEN tiposaft='NC'  THEN 'CREDIT_NOTE'
					   else '' END   
	FROM docsCertificateRequest (nolock) req 
	INNER JOIN docsCertificateRequest_docs docs (nolock) ON  req.documentsToken=docs.documentsToken
	INNER JOIN ft								(nolock) ON  docs.ftstamp = ft.ftstamp
	INNER JOIN td								(nolock) ON	 td.ndoc = ft.ndoc
	WHERE req.TOKEN =@token



	SELECT 
		 type																										   AS type
		,typeDesc																									   AS typeDesc
		,certifyingEntity																							   AS certifyingEntity
		,certifyingEntityName																						   AS certifyingEntityName
		,test																										   AS test
		,site																										   AS site 	
		,operationID																								   AS operationID
		, accountType																								   AS accountType
		, accountTypeDesc																							   AS accountTypeDesc
		, ISNULL(@filePathToSign,'')																				   AS filePathToSign
		, ISNULL(@nameDocToSign	,'')																				   AS nameDocToSign
		, ISNULL(@TypeDoc,'')																						   AS typeDoc	
		,(select 'PT'+ ncont from empresa where site=docsCertificateRequest.site)									   AS ncont																				
	FROM docsCertificateRequest (NOLOCK)
	WHERE TOKEN =@token




GO
GRANT EXECUTE on dbo.up_get_DocsCertificateInfoRequestSendPublic TO PUBLIC
GRANT Control on dbo.up_get_DocsCertificateInfoRequestSendPublic TO PUBLIC
GO


