/* SP save Certificate Documents

exec up_save_DocsCertificateInfoResponse '23518B1A-B4EA-48F0-A9DA-944A8726B096','',401,'','','','','','','','','',-1,'Temp file with externalReference=Ext_Ref_42.e, already exists.','2.2.4','External reference already exists in the system.','','0',''
exec up_save_DocsCertificateInfoResponse '9EE46431-7B26-438E-8794-178E714AF537','EXECUTED',200,'226916','','','Some context.','774d610d-7e65-4949-88d4-d7c5c6996eb6','Ext_Ref_42.f','DOCUMENT','','SIGN',-1,'','','','cca6fb15-0f10-4daa-b4ff-6cdd1cf21b1','0','2a66f113-1114-4954-a0a6-fbcac44e64b'
                          

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_DocsCertificateInfoResponse]') IS NOT NULL
	DROP PROCEDURE [dbo].[up_save_DocsCertificateInfoResponse] ;
GO

CREATE PROCEDURE [dbo].[up_save_DocsCertificateInfoResponse]
	@token varchar(36),
	@status varchar(254),
	@statusId NUMERIC(5,0),
	@id varchar(254),
	@creationDate varchar(50),
	@folderExternalReference varchar(254),
	@folder_Desc varchar(254),
	@folder_Id varchar(254),
	@folder_externalReference varchar(254),
	@folder_Content varchar(254),
	@issuerClientServiceAlias varchar(254),
	@type varchar(254),
	@typeId numeric(5,0),
	@causeMessage varchar(254),
	@errorCode varchar(254),
	@errorCodeDescription varchar(254),
	@documentsToken varchar(36),
	@deliveryInfo_AttachDocuments bit,
	@personReceiverToken varchar(36)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	Declare @res bit 
	set @res=0

	IF( NOT EXISTS (SELECT * FROM docsCertificateResponse (NOLOCK) WHERE TOKEN =@token))
	BEGIN
		INSERT INTO docsCertificateResponse(token ,status,statusId,id,creationDate,folderExternalReference,folder_Desc,folder_Id,folder_externalReference,folder_Content
						,issuerClientServiceAlias,type,typeId,causeMessage,errorCode,errorCodeDescription,documentsToken,deliveryInfo_AttachDocuments
						,personReceiverToken,ousrdata,ousrinis,usrdata,usrinis)
		VALUES (@token ,@status,@statusId,@id,@creationDate,@folderExternalReference,@folder_Desc,@folder_Id,@folder_externalReference,@folder_Content
				,@issuerClientServiceAlias,@type,@typeId,@causeMessage,@errorCode,@errorCodeDescription,@documentsToken,@deliveryInfo_AttachDocuments
				,@personReceiverToken,getdate(),'ADM',getdate(),'ADM')


				set @res = 1 -- correu bem 
	END 
			select @res as result 

GO
GRANT EXECUTE on dbo.up_save_DocsCertificateInfoResponse TO PUBLIC
GRANT Control on dbo.up_save_DocsCertificateInfoResponse TO PUBLIC
GO


