/**
 exec up_invoicUBL_billingReference 'ADM361FBA33-E2FA-49A6-B39', 'Loja 1'
**/
--code to validate if the procedure exists, if exists, drops procedure
if OBJECT_ID('[dbo].[up_invoicUBL_billingReference]') IS NOT NULL
	drop procedure dbo.up_invoicUBL_billingReference
go
-- =============================================
-- Author:		Jos� Moreira
-- Create date: 2022-12-06
-- Description: Returns the original document reference and date
-- =============================================
CREATE PROCEDURE up_invoicUBL_billingReference  
	@ftstamp varchar(25),
	@site varchar(10) 
as
begin 
	 
	select 
		(select dbo.alltrimIsNull(CONVERT(varchar, td.ndoc) + '/' + CONVERT(varchar, ft.fno) + '/' + CONVERT(varchar,RIGHT(ft.ftano,2)))) as InvoiceDocumentReference_ID
		,(select dbo.alltrimIsNull(convert(varchar, fdata, 120)))																		  as InvoiceDocumentReference_IssueDate
	from 
				ft	(nolock) 
	inner join	td	(nolock)	on	ft.ndoc = td.ndoc
	where  
		ftstamp in(	select 
						distinct ftstamp 
					from 
						fi	(nolock) 
					where 
						fistamp in (select 
										distinct ofistamp 
									from 
										fi (nolock) 
									where 
										ftstamp = @ftstamp 
										and ofistamp != ''
									)
					)
END

