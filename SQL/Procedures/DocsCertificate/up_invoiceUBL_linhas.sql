/**
 exec up_invoiceUBL_linhas 'ADM361FBA33-E2FA-49A6-B39', 'Loja 1'
**/
--code to validate if the procedure exists, if exists, drops procedure
if OBJECT_ID('[dbo].[up_invoiceUBL_linhas]') IS NOT NULL
	drop procedure dbo.up_invoiceUBL_linhas
go
-- =============================================
-- Author:		Jos� Moreira
-- Create date: 2022-12-06
-- Description: Returns the header information for the eletronic invoic comunication
-- =============================================
CREATE PROCEDURE up_invoiceUBL_linhas  
	@ftstamp varchar(25),
	@site varchar(10)
as
begin
	declare @query varchar(8000) 
	select
		 fistamp
		 --, (select * from uf_calcular_valor_desconto(fi.fistamp, 'Fi'))	 as desconto
		 ,ROW_NUMBER() OVER(ORDER BY lordem ASC)																														as InvoiceLine_ID
		 , fi.qtt																																						as InvoiceLine_InvoicedQuantity
		 ,  case when fi.ndoc in (1,3)
				 then convert(numeric(30,2),round(((select * from uf_montante_Linha_Sem_Imposto(fistamp, 'Fi'))
										+ (select * from uf_calcular_valor_desconto(fi.fistamp, 'Fi')))
											/fi.qtt,2))
				 else convert(numeric(30,2),round((select * from uf_montante_Linha_Sem_Imposto(fistamp, 'Fi'))/fi.qtt,2))
				 end																																					as InvoiceLine_Price_PriceAmount
		 , (select dbo.alltrimIsNull(fi.ref))																															as InvoiceLine_Item_Name
		 , (select dbo.alltrimIsNull('S'))																																as InvoiceLine_Item_ClassifiedTaxCategory_ID
		 , (select dbo.alltrimIsNull(fi.iva))																															as InvoiceLine_Item_ClassifiedTaxCategory_Percent
		 , (select dbo.alltrimIsNull('VAT'))																															as InvoiceLine_Item_ClassifiedTaxCategory_TaxScheme_ID
		 , (select * from uf_montante_Linha_Sem_Imposto(fi.fistamp, 'fi'))																								as InvoiceLine_LineExtensionAmount
		 , convert(numeric(30,2), fi.qtt)																																as InvoiceLine_Price_BaseQuantity
		 
		 /*Allowance Charge*/
		 , 'false'																																						as AllowanceCharge_ChargeIndicator
		 , '95'/*code obtained on the URL: https://docs.peppol.eu/poacc/billing/3.0/codelist/UNCL5189/ */																as AllowanceCharge_AllowanceChargeReasonCode
		 , 'desconto'																																					as AllowanceCharge_AllowanceChargeReason
		 , case when fi.ndoc in (1, 3) 
				then (select percentagemPartInt from uf_calcular_Percentagem(
						(select * from uf_calcular_valor_desconto(fi.fistamp, 'Fi'))
						,(select * from uf_montante_Linha_Sem_Imposto(fi.fistamp, 'fi')) +	(select * from uf_calcular_valor_desconto(fi.fistamp, 'Fi'))))				
				else
					0
				end																																						as AllowanceCharge_MultiplierFactorNumeric
		 , case when fi.ndoc in (1, 3) 
				then convert(numeric(20,2), round((select * from uf_calcular_valor_desconto(fi.fistamp, 'Fi')),2))
				else
					0
				end																																						as AllowanceCharge_Amount
		 , case when fi.ndoc in (1, 3) 
				then (select * from uf_montante_Linha_Sem_Imposto(fi.fistamp, 'fi')) 
							+	(select * from uf_calcular_valor_desconto(fi.fistamp, 'Fi'))	
				else
					0
				end																																						as AllowanceCharge_BaseAmount 
	from
				fi			(nolock)
	inner join	empresa		(nolock)	on	empresa.site = (select site from ft (nolock) where ftstamp = @ftstamp)
	where 
		fi.ftstamp = @ftstamp
		and empresa.site = @site
		and fi.ref != '' and fi.epv != 0 
	order by lordem
END
