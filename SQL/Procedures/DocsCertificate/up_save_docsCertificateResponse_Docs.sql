/* SP save Certificate Documents

	exec up_save_docsCertificateResponse_Docs ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_docsCertificateResponse_Docs]') IS NOT NULL
	DROP PROCEDURE [dbo].[up_save_docsCertificateResponse_Docs] ;
GO

CREATE PROCEDURE [dbo].[up_save_docsCertificateResponse_Docs]
		@stamp						varchar(36),
		@documentsToken				varchar(36),
		@description				varchar(254),
		@additionalContentLength	NUMERIC(10,0),
		@cipherStatus				varchar(254),
		@contentLength				NUMERIC(10,0),
		@contentType				varchar(254),
		@externalReference			varchar(254),
		@internalId					NUMERIC(10,0),
		@storageEndDate				varchar(254),
		@name						varchar(254),
		@receptionDate				varchar(254),
		@signError					varchar(254),
		@signStatus					varchar(254),
		@signStatusId				NUMERIC(5,0),
		@signedContent				varchar(254),
		@storagePeriod				NUMERIC(10,0),
		@storageStatus				varchar(254)


/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	Declare @res bit 
	set @res=0

	IF( NOT EXISTS (SELECT * FROM docsCertificateResponse_Docs (NOLOCK) WHERE stamp =@stamp))
	BEGIN
		INSERT INTO docsCertificateResponse_Docs(stamp, documentsToken, description, additionalContentLength, cipherStatus, contentLength, 
						contentType, externalReference, internalId, storageEndDate, name, receptionDate, signError, signStatus,signStatusId, signedContent, 
						storagePeriod, storageStatus, ousrdata, ousrinis, usrdata, usrinis)
		VALUES (@stamp,@documentsToken,@description,@additionalContentLength,@cipherStatus,@contentLength
				,@contentType,@externalReference,@internalId,@storageEndDate,@name,@receptionDate,@signError,@signStatus,@signStatusId,@signedContent,
				@storagePeriod,@storageStatus,getdate(),'ADM',getdate(),'ADM')

		set @res = 1 -- correu bem 
	END 
		select @res as result 
GO
GRANT EXECUTE on dbo.up_save_docsCertificateResponse_Docs TO PUBLIC
GRANT Control on dbo.up_save_docsCertificateResponse_Docs TO PUBLIC
GO


