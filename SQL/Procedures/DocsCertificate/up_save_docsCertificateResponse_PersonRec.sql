/* SP save PersonRec

	exec up_save_docsCertificateResponse_PersonRec ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_docsCertificateResponse_PersonRec]') IS NOT NULL
	DROP PROCEDURE [dbo].[up_save_docsCertificateResponse_PersonRec] ;
GO

CREATE PROCEDURE [dbo].[up_save_docsCertificateResponse_PersonRec]
	@stamp varchar(36),
	@personReceiverToken varchar(36),
	@deliveryStatus varchar(254),
	@email varchar(254)


/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	Declare @res bit 
	set @res=0

	IF( NOT EXISTS (SELECT * FROM docsCertificateResponse_PersonRec (NOLOCK) WHERE stamp =@stamp))
	BEGIN
		INSERT INTO docsCertificateResponse_PersonRec(stamp,personReceiverToken,deliveryStatus,email,ousrdata,ousrinis,usrdata,usrinis)
		VALUES (@stamp ,@personReceiverToken,@deliveryStatus,@email,getdate(),'ADM',getdate(),'ADM')

		set @res = 1 -- correu bem 
	END 
		select @res as result 

GO
GRANT EXECUTE on dbo.up_save_docsCertificateResponse_PersonRec TO PUBLIC
GRANT Control on dbo.up_save_docsCertificateResponse_PersonRec TO PUBLIC
GO


