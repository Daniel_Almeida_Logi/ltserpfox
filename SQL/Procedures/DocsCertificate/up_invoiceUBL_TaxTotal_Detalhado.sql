/**
 exec  up_invoiceUBL_TaxTotal_Detalhado 'ADM361FBA33-E2FA-49A6-B39', 'FT'
**/
--code to validate if the procedure exists, if exists, drops procedure
if OBJECT_ID('[dbo].[up_invoiceUBL_TaxTotal_Detalhado]') IS NOT NULL
	drop procedure dbo.up_invoiceUBL_TaxTotal_Detalhado
go
-- =============================================
-- Author:		José Moreira
-- Create date: 2022-12-06
-- Description: Returns the header information for the eletronic invoic comunication
-- =============================================
CREATE PROCEDURE [dbo].[up_invoiceUBL_TaxTotal_Detalhado]  
	@stamp varchar(25),
	@tabela varchar(2)
as
begin
	if @tabela = 'FT'
	begin	
		declare @ndoc		int				=	(select ndoc from ft (nolock) where ftstamp = @stamp)
		declare @desconto	numeric(28,2)	=	0
		if @ndoc in (1,3)
		begin
			set @desconto = (select * from uf_calcular_valor_desconto(@stamp, 'FT'))
		end

		if OBJECT_ID('tempdb..#tab_precos_iva') is not null
			 drop table #tab_precos_iva

		select * into #tab_precos_iva from uf_ver_todos_os_ivas_por_stamp (@stamp, @tabela)
		 
		select 
			(select convert(numeric(28,2), SUM(round(abs((dinheiroProduto - ((@desconto/100)*dinheiroProduto))*(percentagemIva/100)),2))) from #tab_precos_iva m)		as TaxTotal_TaxAmount
			, convert(numeric(18,2), round(abs((dinheiroProduto - ((@desconto/100)*dinheiroProduto))*(percentagemIva/100)),2))											as TaxTotal_TaxSubtotal_TaxAmount
			--,(select dbo.alltrimIsNull(convert(varchar,(select * from uf_calcularMontante_BaseDeIncidencia_PorStamp (@stamp,  'Ft')))))									
			,(select dbo.alltrimIsNull('S'))																															as TaxTotal_TaxSubtotal_TaxCategory_ID
			,percentagemIva																																				as TaxTotal_TaxSubtotal_TaxCategory_Percent
			,(select dbo.alltrimIsNull('VAT'))																															as TaxTotal_TaxSubtotal_TaxCategory_TaxScheme_ID	
			,convert(numeric(18,2), round(abs(dinheiroProduto - ((@desconto/100)*dinheiroProduto)),2))																	as TaxTotal_TaxSubtotal_TaxableAmount
		from 
			#tab_precos_iva

		if OBJECT_ID('#tab_precos_iva') is not null
		 drop table #tab_precos_iva
	end
END
