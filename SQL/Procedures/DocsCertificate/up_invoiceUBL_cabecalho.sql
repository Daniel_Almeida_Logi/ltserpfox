/**
 exec up_invoiceUBL_cabecalho 'ADM361FBA33-E2FA-49A6-B39', 'Loja 1'
**/

--code to validate if the procedure exists, if exists, drops procedure
if OBJECT_ID('[dbo].[up_invoiceUBL_cabecalho]') IS NOT NULL
	drop procedure dbo.up_invoiceUBL_cabecalho
go
-- =============================================
-- Author:		José Moreira
-- Create date: 2022-12-06
-- Description: Returns the header information for the eletronic invoic comunication
-- =============================================
Create PROCEDURE [dbo].[up_invoiceUBL_cabecalho]  
	@ftstamp varchar(25),
	@site varchar(10) 
as
begin

	if OBJECT_ID('tempdb..#tab_precos_iva') is not null
		drop table #tab_precos_iva

		select * into #tab_precos_iva from uf_ver_todos_os_ivas_por_stamp (@ftstamp, 'ft')

	--print 'entrou'
	--print 'a declarar variaveis'
	declare 
		@LegalMonetaryTotal_SemImposto					numeric(30,2)
		,@LegalMonetaryTotal_SemImposto_ComDesconto		numeric(30,2)
		, @LegalMonetaryTotal_ComImposto				numeric(30,2)
		, @LegalMonetaryTotal_ComImposto_ComDesconto	numeric(30,2)
		, @moeda										varchar(10)
		, @billingStamp									varchar(25)
		, @billingId									varchar(50)
		, @billingDate									varchar(30) 
		, @desconto										numeric(28,2) 
	--print 'declarou variaveis'
	--print 'a atribuir valores a variaveis'
	set @desconto									=					(select * from uf_calcular_valor_desconto(@ftstamp, 'FT'))
	set @LegalMonetaryTotal_SemImposto				=					(select * from uf_calcularMontante_Total_Linhas_Documento_PorArtigo_Sem_Imposto(@ftstamp, 'FT'))
	set @LegalMonetaryTotal_SemImposto_ComDesconto	=					@LegalMonetaryTotal_SemImposto - (@LegalMonetaryTotal_SemImposto * (@desconto/100))
	set @LegalMonetaryTotal_ComImposto				=					(select * from uf_calcularMontante_Total_Linhas_Documento_PorArtigo_Sem_Imposto(@ftstamp, 'FT')) 
																			+ (select convert(numeric(28,2), SUM(round(abs((dinheiroProduto - ((@desconto/100)*dinheiroProduto))*(percentagemIva/100)),2))) from #tab_precos_iva m)
	set @LegalMonetaryTotal_ComImposto_ComDesconto	=					@LegalMonetaryTotal_ComImposto - (@LegalMonetaryTotal_SemImposto * (@desconto/100))
	set @moeda										=					''
	set @billingStamp								=					(select dbo.alltrimIsNull((select distinct top 1 ftstamp from fi (nolock) 
																						where fistamp in (select ofistamp from fi (nolock) 
																														where ftstamp = @ftstamp and ofistamp != ''))))	
	set @billingId									=					(select dbo.alltrimIsNull((select top 1 CONVERT(varchar, td.ndoc) + '/' + CONVERT(varchar, ft.fno) + '/' + CONVERT(varchar,RIGHT(ft.ftano,2)) 
																			from ft (nolock) inner join td (nolock) on ft.ndoc = td.ndoc
																				where ftstamp = @billingStamp)))
	set @billingDate								=					(select dbo.alltrimIsNull(convert(varchar,(select top 1 fdata from ft (nolock) where ftstamp in (@billingStamp)), 120)))	
 
	if OBJECT_ID('#tab_precos_iva') is not null
		drop table #tab_precos_iva															
	--print 'atribuiu valores a variaveis'
	--print 'a prepapar o select'

	select
		/*Cabeçalhos gerais*/
		(select dbo.alltrimIsNull('TESTEE'))																							as ProfileID
		, (select dbo.alltrimIsNull('urn:cen.eu:en16931:2017#compliant#urn:feap.gov.pt:CIUS-PT:1.0.0.'))								as CustomizationID
		, (select dbo.alltrimIsNull('TESTE'))																							as UBLVersionID
		, (select dbo.alltrimIsNull(convert(varchar, td.ndoc) +'/' + convert(varchar, ft.fno) +'/' + right(ft.ftano,2)))				as ID
		, convert(varchar,ft.fdata, 120)																								as IssueDate
		, convert(varchar,ft.pdata, 120)																								as DueDate
		, (select dbo.alltrimIsNull(td.tiposaft))																						as TypeCode
		/*OPCIONAL*/, (select dbo.alltrimIsNull(ft2.obsdoc))																			as Note
		, (select dbo.alltrimIsNull(case when ft.moeda = 'EURO' then 'EUR' else '' end))												as DocumentCurrencyCode
		/*OPCIONAL*/, (select dbo.alltrimIsNull(convert(varchar, ft.no) +'.' + convert(varchar, ft.estab)))								as BuyerReference
		
		/*AccountingSupplierParty*/ 
		, (select dbo.alltrimIsNull(empresa.nomecomp))																					as AccountingSupplier_Party_PartyName_Name
		, (select dbo.alltrimIsNull(empresa.morada))																					as AccountingSupplier_Party_PostalAddress_StreetName
		, (select dbo.alltrimIsNull(case when empresa.local = '' then empresa.concelho else empresa.local end))							as AccountingSupplier_Party_PostalAddress_CityName
		, (select dbo.alltrimIsNull(empresa.codpost))																					as AccountingSupplier_Party_PostalAddress_PostalZone
		, (select dbo.alltrimIsNull('PT'))																								as AccountingSupplier_Party_PostalAddress_Country_IdentificationCode
		, (select dbo.alltrimIsNull(empresa.nomecomp))																					as AccountingSupplier_Party_PartyLegalEntity_RegistrationName
		, (select dbo.alltrimIsNull('PT' + empresa.ncont))																				as AccountingSupplier_Party_PartyTaxScheme_CompanyID
		, (select dbo.alltrimIsNull('VAT'))																								as AccountingSupplier_Party_PartyTaxScheme_TaxScheme_ID
		, (select dbo.alltrimIsNull('IVA'))																								as AccountingSupplier_Party_PartyTaxScheme_TaxScheme_TaxTypeCode
		, (select dbo.alltrimIsNull(empresa.telefone))																					as AccountingSupplier_Party_Contact_Telephone
	
		/*AccountingCustomerParty*/
		, (select dbo.alltrimIsNull(ft.nome))																							as AccountingCustomer_Party_PartyName_Name 
		,  (select dbo.alltrimIsNull(b_utentes.morada))																					as AccountingCustomer_Party_PostalAddress_StreetName 
		,  (select dbo.alltrimIsNull(b_utentes.local))																					as AccountingCustomer_Party_PostalAddress_CityName
		, case when (select dbo.alltrimIsNull(ft.codpost)) != '' 
				then (select dbo.alltrimIsNull(ft.codpost)) 
				else (select dbo.alltrimIsNull(b_utentes.codpost)) end																	as AccountingCustomer_Party_PostalAddress_PostalZone 		
		, (select dbo.alltrimIsNull('PT')) /*talvez tenha de ser hardcoded PT*/															as AccountingCustomer_Party_PostalAddress_Country_IdentificationCode  
		, (select dbo.alltrimIsNull(ft.nome))																							as AccountingCustomer_Party_PartyLegalEntity_RegistrationName
		, (select dbo.alltrimIsNull('PT' + ft.ncont))																					as AccountingCustomer_Party_PartyTaxScheme_CompanyID 
		, (select dbo.alltrimIsNull('VAT'))																								as AccountingCustomer_Party_PartyTaxScheme_TaxScheme_ID
		, (select dbo.alltrimIsNull('IVA'))																								as AccountingCustomer_Party_PartyTaxScheme_TaxScheme_TaxTypeCode 
		, (select dbo.alltrimIsNull(b_utentes.telefone))																				as AccountingCustomer_Party_Contact_Telephone
		
		/*LegalMonetaryTotal*/
		, (select dbo.alltrimIsNull(convert(varchar, abs(@LegalMonetaryTotal_SemImposto))))												as LegalMonetaryTotal_LineExtensionAmount
		, (select dbo.alltrimIsNull(convert(varchar, abs(@LegalMonetaryTotal_SemImposto_ComDesconto))))									as LegalMonetaryTotal_TaxExclusiveAmount
		, (select dbo.alltrimIsNull(convert(numeric(28,2), abs(ft.etotal))))															as LegalMonetaryTotal_TaxInclusiveAmount
		, (select dbo.alltrimIsNull(convert(numeric(28,2), abs(round(@LegalMonetaryTotal_SemImposto *(@desconto/100),2)))))				as LegalMonetaryTotal_AllowanceTotalAmount
		, (select dbo.alltrimIsNull(convert(numeric(28,2), abs(ft.etotal))))															as LegalMonetaryTotal_PayableAmount 
		
		/*Delivery*/
		,case when (select dbo.alltrimIsNull(b_utentes.morada))!= '' 
				then (select dbo.alltrimIsNull(b_utentes.morada))
				else (select dbo.alltrimIsNull(ft.morada ))	end																	as Delivery_DeliveryLocation_Address_StreetName
		,case when (select dbo.alltrimIsNull(b_utentes.local))!= '' 
				then (select dbo.alltrimIsNull(b_utentes.local))
				else (select dbo.alltrimIsNull(ft.local))end																	as Delivery_DeliveryLocation_Address_CityName
		,case when (select dbo.alltrimIsNull(ft.codpost))!= '' 
				then (select dbo.alltrimIsNull(ft.codpost ))
				else (select dbo.alltrimIsNull(b_utentes.codpost))	end																	as Delivery_DeliveryLocation_Address_PostalZone
		,(select dbo.alltrimIsNull('PT'))																								as Delivery_DeliveryLocation_Address_Country_IdentificationCode
		/*PaymentTerms*/
		,case	when len(trim(convert(varchar,ft2.obsdoc))) > 0 then ft2.obsdoc else 'Sem observações' end								as PaymentTerms_Note
	
		/*other needed info*/
		, (select dbo.alltrimIsNull('FT'))																								as 'Table'
	from
				ft			(nolock)
	inner join	b_utentes	(nolock)	on	b_utentes.no = ft.no and b_utentes.estab = ft.estab
	inner join	td			(nolock)	on	td.ndoc = ft.ndoc
	inner join	ft2			(nolock)	on	ft2stamp = ftstamp
	inner join	empresa		(nolock)	on	ft.site = empresa.site  
	where 
		ft.ftstamp = @ftstamp
		and empresa.site = @site
	--print 'executou o select'
	--print 'vai sair'
END