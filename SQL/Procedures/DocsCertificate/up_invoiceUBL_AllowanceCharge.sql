/**
exec up_invoiceUBL_AllowanceCharge  'JpMA9725F2C-6E0D-490D-B77', 'Loja 1'
**/
--code to validate if the procedure exists, if exists, drops procedure
if OBJECT_ID('[dbo].[up_invoiceUBL_AllowanceCharge]') IS NOT NULL
	drop procedure dbo.up_invoiceUBL_AllowanceCharge
go
-- =============================================
-- Author:		José Moreira
-- Create date: 2022-12-06
-- Description: Returns the header information for the eletronic invoic comunication
-- =============================================
CREATE PROCEDURE [dbo].[up_invoiceUBL_AllowanceCharge]  
	@ftstamp varchar(25),
	@site varchar(10)
as
begin
	if OBJECT_ID('tempdb..#tabdesconto') is not null
		 drop table #tabdesconto
	if OBJECT_ID('tempdb..#tabIvaPorStamp') is not null
		 drop table #tabIvaPorStamp

	select 
		abs((select * from uf_calcularMontante_Total_Linhas_Documento_PorIva_Sem_Imposto(fi.fistamp, 'fi'))) as valor_sem_iva
		, fi.iva
	into 
		#tabIvaPorStamp
	from
		fi	(nolock)
	where
		fi.ftstamp = @ftstamp

--	select * from #tabIvaPorStamp

	select
		'false'																																			as AllowanceCharge_ChargeIndicator
		, '95'/*code obtained on the URL: https://docs.peppol.eu/poacc/billing/3.0/codelist/UNCL5189/ */												as AllowanceCharge_AllowanceChargeReasonCode
		, 'desconto'																																	as AllowanceCharge_AllowanceChargeReason
		, (select * from uf_calcular_valor_desconto(@ftstamp, 'FT'))																					as AllowanceCharge_MultiplierFactorNumeric
		, convert(numeric(18,2), round((select sum(valor_sem_iva) from #tabIvaPorStamp where iva = fi.iva)	
			*(select * from uf_calcular_valor_desconto(@ftstamp, 'FT'))	/100,2))																		as AllowanceCharge_Amount
		, (select convert(numeric(18,2), round(sum(valor_sem_iva),2)) from #tabIvaPorStamp where iva = fi.iva)											as AllowanceCharge_BaseAmount
		, 'S'																																			as AllowanceCharge_TaxCategory_ID
		, convert(numeric(18,2), round(abs(fi.iva),2))																									as AllowanceCharge_TaxCategory_Percent
		, 'VAT'																																			as AllowanceCharge_TaxCategory_TaxScheme_ID 
	into
		#tabDesconto
	from 
		fi			(nolock)
	inner join
		ft			(nolock)	on fi.ftstamp = ft.ftstamp	 
	inner join	
		empresa		(nolock)	on	empresa.site = ft.site 
	where 
		ft.ftstamp = @ftstamp
		and empresa.site = @site  
	group by
		fi.iva, ft.fin, ft.ettiliq

	select 
		* 
	from 
		#tabdesconto 
	where 
		AllowanceCharge_BaseAmount > 0 
		and AllowanceCharge_Amount > 0
		and AllowanceCharge_MultiplierFactorNumeric > 0
	 
	if OBJECT_ID('tempdb..#tabdesconto') is not null
		drop table #tabdesconto
	if OBJECT_ID('tempdb..#tabIvaPorStamp') is not null
		 drop table #tabIvaPorStamp
END
