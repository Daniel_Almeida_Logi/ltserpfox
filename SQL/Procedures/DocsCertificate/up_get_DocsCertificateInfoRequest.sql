/* SP Pesquisa Documentos

	exec up_get_DocsCertificateInfoRequest 'C608FD11-ECED-4CBD-B970-B18E90345317'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_DocsCertificateInfoRequest]') IS NOT NULL
	DROP PROCEDURE [dbo].[up_get_DocsCertificateInfoRequest] ;
GO

CREATE PROCEDURE [dbo].[up_get_DocsCertificateInfoRequest]
	@token varchar(36)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	SELECT 
		 documentsToken																								   AS documentsToken
		,externalReference																							   AS externalReference
		,type																										   AS type
		,typeDesc																									   AS typeDesc
		,certifyingEntity																							   AS certifyingEntity
		,certifyingEntityName																						   AS certifyingEntityName
		,description																								   AS description
		,returnSignedContent																					       AS returnSignedContent
		,preserInfoAlias																							   AS preserInfoAlias
		,preserInfoContent																							   AS preserInfoContent
		,preserInfoAttachDocuments																					   AS preserInfoAttachDocuments
		,personReceiverToken																						   AS personReceiverToken
		,test																										   AS test
		,site																										   AS site 	
		,operationID																								   AS operationID
		,attachDocuments																							   AS attachDocuments
		, (SELECT textValue FROM B_Parameters_site WHERE stamp='ADM0000000169' and site= docsCertificateRequest.site)  AS credential
		, accountType																								   AS accountType
		, accountTypeDesc																							   AS accountTypeDesc
		,(select 'PT'+ ncont from empresa where site=docsCertificateRequest.site)									   AS ncont
	FROM docsCertificateRequest (NOLOCK)
	WHERE TOKEN =@token




GO
GRANT EXECUTE on dbo.up_get_DocsCertificateInfoRequest TO PUBLIC
GRANT Control on dbo.up_get_DocsCertificateInfoRequest TO PUBLIC
GO


