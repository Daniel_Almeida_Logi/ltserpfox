/* SP Pesquisa Documentos

	exec up_get_DocsCertificateInfoRequestDocs 'CBAD8950-880F-4180-B8F8-45F13BDFF390'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_DocsCertificateInfoRequestDocs]') IS NOT NULL
	DROP PROCEDURE [dbo].[up_get_DocsCertificateInfoRequestDocs] ;
GO

CREATE PROCEDURE [dbo].[up_get_DocsCertificateInfoRequestDocs]
	@documentsToken varchar(36)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	SELECT 
		stamp
		,ftStamp
		,externalReference
		,name
		,nameDocToSign
		,filePathToSign
		,filePath
		,contentType
		,applyTimestamp
		,location
		,reason
		,signatureType
		,area
		,areaBackgroundColor
		,font
		,Case when imageType ='' then null else imageType end  imageType
		,Case when imageValue ='' then null else imageValue end imageValue
		,locationType
		,locationValue
		,oneOffTemplate
		,templateType
		,canonicalTransform
		,signatureLocation
		,signatureTransform

	FROM docsCertificateRequest_docs (NOLOCK)
	WHERE documentsToken =@documentsToken




GO
GRANT EXECUTE on dbo.up_get_DocsCertificateInfoRequestDocs TO PUBLIC
GRANT Control on dbo.up_get_DocsCertificateInfoRequestDocs TO PUBLIC
GO


