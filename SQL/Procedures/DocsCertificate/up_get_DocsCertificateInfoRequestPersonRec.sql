/* SP Pesquisa Documentos

	
	exec up_get_DocsCertificateInfoRequestPersonRec ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_DocsCertificateInfoRequestPersonRec]') IS NOT NULL
	DROP PROCEDURE [dbo].[up_get_DocsCertificateInfoRequestPersonRec] ;
GO

CREATE PROCEDURE [dbo].[up_get_DocsCertificateInfoRequestPersonRec]
	@personReceiverToken varchar(36)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	SELECT 
			deliveryStatus ,
			email
	FROM docsCertificateRequest_PersonRec (NOLOCK)
	WHERE personReceiverToken =@personReceiverToken




GO
GRANT EXECUTE on dbo.up_get_DocsCertificateInfoRequestPersonRec TO PUBLIC
GRANT Control on dbo.up_get_DocsCertificateInfoRequestPersonRec TO PUBLIC
GO


