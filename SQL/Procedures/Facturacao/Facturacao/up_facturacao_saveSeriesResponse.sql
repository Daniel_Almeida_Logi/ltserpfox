/* 
	Escreve a resposta do pedido de inser��o de serie de facturacao via webservice

	exec up_facturacao_saveSeriesResponse 'ADM5AFD2480-233B-4E0E-BC2',2001,'S�rie registada com sucesso. A situa��o ficou ativa e foi atribu�do o seguinte c�digo de valida��o : JFJW68K8','96','JFJW68K8','TD'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_facturacao_saveSeriesResponse]') IS NOT NULL
	drop procedure dbo.up_facturacao_saveSeriesResponse
go

create procedure dbo.up_facturacao_saveSeriesResponse

@token as varchar(36),
@codigo as numeric(5,0),
@resp as varchar(200),
@serie as varchar(35),
@codigoAtc as varchar(35),
@tabela as varchar(35)

/* WITH ENCRYPTION */
AS

	delete from resp_regTD where token = @token

	insert into resp_regTD (token, codigo, resp, serie)
	values(@token,@codigo,@resp,@serie)


	--sucesso na at
	if(@codigo=2001 and @codigoAtc!='' and @tabela!='')
	begin
		set @tabela = LOWER(LTRIM(RTRIM(@tabela)))
		if(@tabela='td')
			update td set ATCUD = @codigoAtc where ndoc=@serie
		else if(@tabela='cm1')
			update cm1 set ATCUD = @codigoAtc where cm=@serie
		else if(@tabela='tsre')
			update tsre set ATCUD = @codigoAtc where ndoc=@serie
		else if(@tabela='ts')
			update ts set ATCUD = @codigoAtc where ndos=@serie
	end


GO
Grant Execute On dbo.up_facturacao_saveSeriesResponse to Public
Grant Control On dbo.up_facturacao_saveSeriesResponse to Public
Go

