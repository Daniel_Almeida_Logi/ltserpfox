/* 
	
	exec up_faturacao_getDocsPagExt 20, '', 0, '', '20250224', '20250224', '', 0, 0, 'Loja 1', 0

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_faturacao_getDocsPagExt]') IS NOT NULL
	drop procedure dbo.up_faturacao_getDocsPagExt
go

create procedure dbo.up_faturacao_getDocsPagExt
	@top numeric(9),
	@nmdoc varchar(20),
	@fno numeric(9),
	@nrAtend varchar(30),
	@dataini varchar(8),
	@datafim varchar(8),
	@nome varchar(80),
	@no numeric(9),
	@estab numeric(9),
	@site varchar(20),
	@porPagar bit

AS

	DECLARE @sql VARCHAR(MAX)
	set @porPagar =  isnull(@porPagar,0)

	SET @sql = N'

	SELECT
		DISTINCT

		TOP ' + LTRIM(@top) + '

		pc.nrAtend,
		pc.Total,
		cl.nome,
		pc.no,
		pc.estab,
		pc.oData,
		pc.nome + ''['' + LTRIM(pc.vendedor) + '']'' as [user]
	FROM
		b_pagCentral(nolock) as pc
		left join b_utentes(nolock) as cl on cl.no = pc.no and cl.estab = pc.estab
		left join ft(nolock) on ft.u_nratend = pc.nrAtend
		inner join b_pagCentral_ext(nolock) on pc.nrAtend = b_pagCentral_ext.nrAtend and pc.site = b_pagCentral_ext.site
	WHERE
		
		pc.site = ''' + @site +  '''
		and CONVERT( VARCHAR, pc.oData, 112) BETWEEN ''' + LTRIM(@dataini) + ''' AND ''' + LTRIM(@datafim) + '''
	'
		+ (CASE WHEN @nmdoc <> '' THEN ' and ISNULL(ft.nmdoc, '''') = ''' + LTRIM(RTRIM(@nmdoc)) + ''' ' ELSE '' END)
		+ (CASE WHEN @fno <> 0 THEN ' and ISNULL(ft.fno, 0) = ' + LTRIM(RTRIM(@fno)) ELSE '' END)
		+ (CASE WHEN @nrAtend <> '' THEN ' and pc.nrAtend = ''' + LTRIM(RTRIM(@nrAtend)) + ''' ' ELSE '' END)
		+ (CASE WHEN @nome <> '' THEN ' and ISNULL(ft.nome, '''') LIKE ''%' + LTRIM(RTRIM(@nome)) + '%'' ' ELSE '' END)
		+ (CASE WHEN @no <> 0 THEN ' and ISNULL(ft.no, 0) = ' + LTRIM(RTRIM(@no)) + ' and ISNULL(ft.estab, 0) = ' + LTRIM(RTRIM(@estab)) ELSE '' END)
		+ (CASE WHEN @porPagar = 0 THEN ' AND b_pagCentral_ext.statePaymentID  in (1,2)  ' ELSE ' AND  b_pagCentral_ext.statePaymentID not  in (1,2)  ' END)	
	
	SET @sql = @sql + N' ORDER BY pc.oData asc'

	print @sql

	EXEC (@sql)


GO
Grant Execute on dbo.up_faturacao_getDocsPagExt to Public
Grant control on dbo.up_faturacao_getDocsPagExt to Public
GO