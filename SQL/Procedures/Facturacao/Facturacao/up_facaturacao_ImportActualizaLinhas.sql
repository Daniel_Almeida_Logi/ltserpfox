-- Painel de Importação de Documentos de Facturação
-- exec up_facaturacao_ImportActualizaLinhas 0,0,'ADM10121035759.606000002'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_facturacao_ImportActualizaLinhas]') IS NOT NULL
	drop procedure dbo.up_facturacao_ImportActualizaLinhas
go

create procedure dbo.up_facturacao_ImportActualizaLinhas

@quantidadeMov	int,
@quantidade		int,
@cabstamp as varchar(60)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

If @quantidadeMov = 0
BEGIN
	IF @quantidade = 0
	BEGIN
		SELECT
			FISTAMP, copiar	= 0, REF, DESIGN, EPV, QTT, ETILIQUIDO, qttmov	= fi.pbruto
		FROM
			FI
		WHERE
			ftstamp = @cabstamp
		
		UNION ALL
		
		SELECT
			BISTAMP, copiar = 0, REF, DESIGN, EDEBITO, QTT, ETTDEB, qttmov = bi.pbruto
		FROM
			BI
		WHERE
			bostamp = @cabstamp
	END
	ELSE
	BEGIN
		SELECT
			FISTAMP, copiar = 0, REF, DESIGN, EPV, QTT, ETILIQUIDO, qttmov	= fi.pbruto
		FROM
			FI
		WHERE
			ftstamp = @cabstamp AND Fi.QTT > 0
		
		UNION ALL
		
		SELECT
			BISTAMP, copiar = 0, REF, DESIGN, EDEBITO, QTT, ETTDEB, qttmov = bi.pbruto
		FROM
			BI
		WHERE
			bostamp = @cabstamp AND BI.QTT > 0
	END
END
ELSE
BEGIN
	IF @quantidade = 0 
	BEGIN
		SELECT
			FISTAMP, copiar = 0, REF, DESIGN, EPV, QTT, ETILIQUIDO, fi.pbruto as qttmov
		FROM
			FI
		WHERE
			ftstamp = @cabstamp AND Fi.qtt > fi.pbruto
			
		UNION ALL
		
		SELECT
			BISTAMP, copiar = 0, REF, DESIGN, EDEBITO, QTT, ETTDEB, qttmov = bi.pbruto
		FROM
			BI
		WHERE
			bostamp = @cabstamp AND BI.qtt > bi.pbruto
	END
	ELSE
	BEGIN
		SELECT
			FISTAMP, copiar = 0, REF, DESIGN, EPV, QTT, ETILIQUIDO, qttmov = fi.pbruto
		FROM
			FI
		WHERE
			ftstamp = @cabstamp AND Fi.QTT > 0 AND Fi.qtt > fi.pbruto 
		
		UNION ALL
		
		SELECT
			BISTAMP, copiar = 0, REF, DESIGN, EDEBITO, QTT, ETTDEB, qttmov = bi.pbruto
		FROM
			BI
		WHERE
			bostamp = @cabstamp AND BI.QTT > 0 AND BI.qtt > bi.pbruto
	END
END

GO
Grant Execute On dbo.up_facturacao_ImportActualizaLinhas to Public
Grant Control On dbo.up_facturacao_ImportActualizaLinhas to Public
GO