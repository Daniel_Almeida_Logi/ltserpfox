/* 
	
	exec up_facturacao_picData 
		
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_facturacao_picData]') IS NOT NULL
	drop procedure dbo.up_facturacao_picData
go

create procedure dbo.up_facturacao_picData
		
/* WITH ENCRYPTION */
AS

	select
		ref
		,preco
		,data = CONVERT(datetime,data)
		,data_fim = convert(datetime,case when data_fim = '19000101' then '30000101' else data_fim end)
	from
		hist_precos hp (nolock)
	where
		hp.id_preco in (1, 602)
		and ativo = 1
		

GO
Grant Execute On dbo.up_facturacao_picData to Public
Grant Control On dbo.up_facturacao_picData to Public
Go

