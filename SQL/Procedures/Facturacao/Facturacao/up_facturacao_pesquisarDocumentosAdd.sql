-- Pesquisar Documentos Facturação de Back-Office
-- exec up_facturacao_pesquisarDocumentosAdd 30, '', '', '', '', '19000101',  '30001231', '', 1, 'Administrador',''
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_facturacao_pesquisarDocumentosAdd]') IS NOT NULL
	drop procedure dbo.up_facturacao_pesquisarDocumentosAdd
go

create procedure dbo.up_facturacao_pesquisarDocumentosAdd

@top		int,
@entidade	varchar(55),
@numdoc		numeric(10),
@no			numeric(10),
@estab		numeric(3),
@dataini	datetime,
@datafim	datetime,
@doc		varchar(20),
@user		numeric(6),
@group		varchar(50),
@design		varchar(60),
@site		varchar(20)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON	

SELECT Top(@top) Tipodoc, QTTOT, ESCOLHA, operador, cabstamp, Datav, Data, a.ousrdata, a.ousrhora, Documento, Numdoc, Entidade, no, estab, Total,u_nratend
FROM
(
	SELECT  
		 Tipodoc		=	'FT'
		,QTTOT		=	ISNULL(SUM(al.QTT),0)
		,ESCOLHA	=	convert(bit,0)
		,Operador	=	ac.VENDNM
		,CabStamp	=	ac.ftstamp
		,Datav		=	ac.fdata
		,Data			=	CONVERT(varchar,ac.fdata,102)
		,ac.OUSRDATA
		,ac.OUSRHORA
		,Documento	=	ac.NMDOC
		,Numdoc		=	Rtrim(Ltrim(CONVERT(varchar,ac.FNO)))
		,Entidade	=	UPPER(ac.NOME)
		,ac.NO
		,ac.ESTAB
		,Total			=	etotal
		,ac.u_nratend
	FROM
		B_ArquivoTalaoCab ac (nolock)
   		inner join B_ArquivoTalaoLin al (nolock) on ac.ftstamp=al.ftstamp
		LEFT JOIN b_pf pf (nolock) ON pf.resumo=ac.NMDOC + ' - Visualizar'
		LEFT JOIN b_pfu pfu (nolock) on pfu.pfstamp=pf.pfstamp
		LEFT JOIN b_pfg pfg (nolock) on pfg.pfstamp=pf.pfstamp
	WHERE
		ac.nome LIKE @entidade+'%'
		AND ac.fno = CASE WHEN @numDoc = 0 THEN ac.fno ELSE @numDoc END
		AND NO = CASE WHEN @no = 0 THEN NO ELSE @no END
		AND ESTAB = CASE WHEN @estab = -1 THEN ESTAB ELSE @estab END
		AND (ac.fdata >= @dataini AND ac.fdata <= @datafim)
		AND (CONVERT(varchar,ac.NMDOC) = CASE WHEN @doc = '' THEN ac.NMDOC ELSE @doc END)
		AND isnull(pfu.userno,9999) != @user
		AND ISNULL(pfg.nome,'') != @group
		AND (design like @design+'%' OR ref like @design+'%')
		and ac.site = case when  @site = '' then ac.site else @site end
	group by
		ac.VENDNM, ac.ftstamp, ac.fdata, ac.ousrdata, ac.ousrhora, ac.NMDOC, ac.fno, ac.nome, no, estab, eTotal, u_nratend
) a
ORDER BY
	a.OUSRDATA + a.OUSRHORA DESC

GO
Grant Execute On dbo.up_facturacao_pesquisarDocumentosAdd to Public
Grant Control On dbo.up_facturacao_pesquisarDocumentosAdd to Public
Go