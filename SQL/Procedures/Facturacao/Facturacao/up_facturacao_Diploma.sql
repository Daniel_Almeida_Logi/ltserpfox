-- DIPLOMA
-- exec up_facturacao_Diploma '8168617', '59A'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_facturacao_Diploma]') IS NOT NULL
	drop procedure dbo.up_facturacao_Diploma
GO
create procedure dbo.up_facturacao_Diploma

@ref		varchar(15),
@cod		varchar(10)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

/* Todos os Diplomas excepto o de lupos e paramiloidose */
select CONVERT(bit,0) as sel,
	dplms.dplmsstamp, dplms.u_design
from fprod			(nolock)
inner join cptgrp	(nolock) on fprod.grupo=cptgrp.grupo
inner join dplms	(nolock) on cptgrp.u_diploma1=dplms.dplmsstamp or cptgrp.u_diploma2=dplms.dplmsstamp or cptgrp.u_diploma3=dplms.dplmsstamp
inner join cptpla	(nolock) on cptpla.codigo=@cod
where 
	fprod.ref = @ref
	AND cptpla.diploma=1 and cptpla.dplmsstamp='' and cptpla.inactivo=0
	AND dplms.inactivo=0

UNION ALL

/* Apenas os Diplomas lupos e paramiloidose */
select
	CONVERT(bit,0) as sel,
	dplms.dplmsstamp, dplms.u_design
from
	dplms			(nolock)
	inner join cptpla	(nolock) on cptpla.codigo=@cod and cptpla.dplmsstamp=dplms.dplmsstamp
where
	cptpla.diploma=1 and cptpla.inactivo=0 
	AND dplms.inactivo=0
order by
	u_design

GO
Grant Execute On dbo.up_facturacao_Diploma to Public
Grant Control On dbo.up_facturacao_Diploma to Public
GO