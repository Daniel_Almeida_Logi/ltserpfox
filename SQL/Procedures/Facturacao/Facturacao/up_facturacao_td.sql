-- Retorna informação do relativa ao tipo de documento
-- exec up_facturacao_td 30,'nmdoc','Loja 1'


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_facturacao_td]') IS NOT NULL
	drop procedure dbo.up_facturacao_td
go

create procedure dbo.up_facturacao_td
@tipoDoc	numeric(5,0),
@attr	    varchar(500),
@site	    varchar(20)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON	



	declare @sql varchar(max)
	select @sql = N'
		select 
			' + @attr + '
		from 
			td(nolock) 
		where 
			site =  ''' + @site + '''						 
	'

	if(@tipoDoc>0)
		set @sql = @sql +  N' and ndoc = ' + RTRIM(LTRIM(STR(@tipoDoc))) + ''

	print 	@sql		
	execute (@sql)


GO
Grant Execute On dbo.up_facturacao_td to Public
Grant Control On dbo.up_facturacao_td to Public
Go