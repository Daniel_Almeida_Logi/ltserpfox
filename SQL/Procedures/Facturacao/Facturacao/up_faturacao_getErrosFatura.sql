/*
	
	up_faturacao_getErrosFatura '','19000101', '20211110', '', '', 'SAV', '',0

*/

SET ANSI_NULLS OFF 
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_faturacao_getErrosFatura]') IS NOT NULL
	drop procedure dbo.up_faturacao_getErrosFatura
go


CREATE PROCEDURE dbo.up_faturacao_getErrosFatura
@doc varchar(20),
@datIni date,
@datFim date,
@nome varchar(254),
@id INT,
@entidade varchar(255) = '',
@tipoDoc AS VARCHAR(30) = '',
@erro as bit = 0,
@mes as varchar(2) = '',
@ano as varchar(4) = '',
@noEnt as VARCHAR(10) = '',
@mesFt as varchar(2) = '',
@anoFt as varchar(4) = ''

AS
BEGIN

	DECLARE @ul_tipo AS TABLE (tipo VARCHAR(25))

	INSERT INTO 
		@ul_tipo (tipo)
	SELECT 
		y.value
	FROM 
		dbo.SplitString(@tipoDoc, ',') x
		cross apply dbo.SplitString(x.value, ',') y

	select 
        ext_esb_doc.id_ext_pharmacy as id,
        ext_esb_doc.docNr as adoc,
        convert(varchar,ext_esb_doc.docdate, 102) as docData,
        ext_esb_doc.name_ext_pharmacy as nome,
        ext_esb_doc.stamp,
		ext_esb_doc.rejectedAfterConf,
		ext_esb_doc.name_ext_entity as entidade,
		--ISNULL(cptorg.mostraZip, CAST(0 as bit)) as mostraZip,
		cast(1 as bit) as mostraZip,
		ISNULL(cptorg.pwZip, '') as pwZip,
		(CASE 
			WHEN ext_esb_doc.tipoDoc = 1
				THEN 'Fatura' 
			WHEN ext_esb_doc.tipoDoc = 3
				THEN 'Nota de D�bito' 
			WHEN ext_esb_doc.tipoDoc = 2
				THEN 'Nota de Cr�dito' 
			ELSE
				''
		END) as tipoDoc,
		ext_esb_doc.error as erro,
		(CASE 
			WHEN EXISTS(SELECT 1 FROM ext_esb_doc(nolock) as docs WHERE docs.oDocDate = ext_esb_doc.docDate and docs.oDocNr = ext_esb_doc.docNr and docs.id_ext_pharmacy = ext_esb_doc.id_ext_pharmacy and tipoDoc <> 1) 
				THEN CAST(1 AS bit)
			ELSE CAST(0 AS bit)
		END) as hasNC,
		cptorg.u_no as numEnt,
		(CASE WHEN CONVERT(varchar(MAX), ext_esb_doc.invoicePdfPATH) <> '' THEN CAST(1 as bit) ELSE CAST(0 as bit) END) as hasPDF,
		ext_esb_doc.exportedPDF
    from 
        ext_esb_doc(nolock) 
		left join cptorg(nolock) on cptorg.ncont = ext_esb_doc.vatNr_ext_entity and cptorg.cptorgstamp = 'ADM' + ext_esb_doc.id_ext_efr
    where 
        ext_esb_doc.error = @erro
        and ext_esb_doc.test = 0
	    and ext_esb_doc.invalid = 0
        and ext_esb_doc.docNr like (CASE WHEN @doc = '' THEN ext_esb_doc.docNr ELSE '%' + @doc + '%' END)
		and 1 = (CASE
					WHEN @mes = '' OR @ano = ''
						THEN 1 
					WHEN ext_esb_doc.tipoDoc = 2 
						THEN (CASE 
								WHEN 
									ISNULL((SELECT TOP 1 LTRIM(RTRIM(docs.month)) FROM ext_esb_doc(nolock) as docs WHERE docs.DocDate = ext_esb_doc.odocDate and docs.DocNr = ext_esb_doc.odocNr and docs.id_ext_pharmacy = ext_esb_doc.id_ext_pharmacy and tipoDoc = 1), '') = @mes AND
									ISNULL((SELECT TOP 1 LTRIM(RTRIM(docs.year)) FROM ext_esb_doc(nolock) as docs WHERE docs.DocDate = ext_esb_doc.odocDate and docs.DocNr = ext_esb_doc.odocNr and docs.id_ext_pharmacy = ext_esb_doc.id_ext_pharmacy and tipoDoc = 1), '') = @ano
									THEN 1
								ELSE 
									0
							 END)
					ELSE
						(CASE WHEN LTRIM(RTRIM(ext_esb_doc.year)) = @ano AND LTRIM(RTRIM(ext_esb_doc.month)) = @mes THEN 1 ELSE 0 END)
						
				END)
		and 1 = (CASE
					WHEN @mesFt = '' OR @anoFt = ''
						THEN 1 
					ELSE
						(CASE WHEN LTRIM(RTRIM(YEAR(ext_esb_doc.docDate))) = @anoFt AND LTRIM(RTRIM(MONTH(ext_esb_doc.docDate))) = @mesFt THEN 1 ELSE 0 END)
						
				END)
        and 1 = (CASE	
					WHEN @noEnt <> ''
						THEN (CASE WHEN LTRIM(RTRIM(cptorg.u_no)) = @noEnt THEN 1 ELSE 0 END)
					ELSE
						(CASE 
							WHEN 
								@entidade <> '' 
									THEN (CASE WHEN ext_esb_doc.name_ext_entity LIKE '%' + @entidade + '%' THEN 1 else 0 END) 
							ELSE 1  
						END) 
				END) 
        and ext_esb_doc.id_ext_pharmacy = (CASE WHEN @id = '' THEN ext_esb_doc.id_ext_pharmacy ELSE @id END)
		--and ext_esb_doc.name_ext_pharmacy like (CASE WHEN @nome = '' THEN ext_esb_doc.name_ext_pharmacy ELSE '%' + @nome + '%'  END)
		and 1 = (CASE 
					WHEN @tipoDoc = ''
						THEN 1
					ELSE
						(CASE 
							WHEN LTRIM(ext_esb_doc.tipoDoc) IN (SELECT tipo from @ul_tipo)
								THEN 1
							ELSE
								0
						END)
				END)
		and exists (select 1 from b_utentes(nolock) where id = LTRIM(ext_esb_doc.id_ext_pharmacy))
	ORDER BY
		ext_esb_doc.docDate desc
		

END

Go
Grant Execute on dbo.up_faturacao_getErrosFatura to Public
Grant control on dbo.up_faturacao_getErrosFatura to Public
Go
