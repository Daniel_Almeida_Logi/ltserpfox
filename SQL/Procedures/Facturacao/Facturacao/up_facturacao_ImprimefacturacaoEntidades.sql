/*
	up_facturacao_ImprimefacturacaoEntidades 'ADM21E5EE3A-9B49-4873-889'
	select epv,etiliquido,largura,altura,* from fi where ftstamp='ADM9280F001-A100-45D0-A78'
	select FT.EIVAIN1+FT.EIVAIN2+FT.EIVAIN3+FT.EIVAIN4+FT.EIVAIN5, * from ft
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_facturacao_ImprimefacturacaoEntidades]') IS NOT NULL
	drop procedure dbo.up_facturacao_ImprimefacturacaoEntidades
go

create procedure dbo.up_facturacao_ImprimefacturacaoEntidades
@ftstamp	varchar(25)

/* WITH ENCRYPTION */
AS

	SET NOCOUNT ON	
	
	select 
		design
		,sum(largura) as largura
		,sum(altura) as altura
		,MAX(litem2) as litem2
		,sum(litem) as litem
		,sum(etiliquido) as etiliquido
		,SUM(pvp4_fee) as fee
		,SUM(lobs2) as embalagens
	from (
		SELECT	
			design
			,(largura) as largura
			,(altura) as altura
			,(CASE WHEN ISNUMERIC(litem2) = 1 THEN convert(numeric,litem2) ELSE 0 END) as litem2
			,(CASE WHEN ISNUMERIC(litem) = 1 THEN convert(numeric,litem) ELSE 0 END) as litem
			,(etiliquido) as etiliquido
			,pvp4_fee
			,(CASE WHEN ISNUMERIC(lobs2) = 1 THEN convert(numeric,lobs2) ELSE 0 END) as lobs2
		FROM
			fi (nolock)
		WHERE 
			ftstamp = @ftstamp
	) x
	GROUP BY DESIGN

GO
Grant Execute On dbo.up_facturacao_ImprimefacturacaoEntidades to Public
Grant Control On dbo.up_facturacao_ImprimefacturacaoEntidades to Public
Go
