/*

	exec up_facturacao_getDocsNtLancamento 314, 0, '20230612'

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_facturacao_getDocsNtLancamento]') IS NOT NULL
	DROP PROCEDURE dbo.up_facturacao_getDocsNtLancamento
GO

CREATE PROCEDURE dbo.up_facturacao_getDocsNtLancamento
	@no NUMERIC(9),
	@estab NUMERIC(5),
	@fdata DATETIME

AS

	select 
		fno, cast(etotal as numeric(10,2)) as etotal 
	from 
		ft 
	where 
		no=@no and estab=@estab
		and month(fdata)=month(@fdata)
		and year(fdata)=year(@fdata)
		and nmdoc='Factura'
		and not exists (select 1 from fi(nolock) inner join td(nolock) on td.ndoc = fi.ndoc where ofistamp in (select fistamp from fi(nolock) where fi.ftstamp = ft.ftstamp) and td.tipodoc = 3)
	order by 
		fdata desc

GO
Grant Execute on dbo.up_facturacao_getDocsNtLancamento to Public
Grant Control on dbo.up_facturacao_getDocsNtLancamento to Public
GO