/* sp p verificar precos � data - usada na inser��o de receita em bo

	select * from hist_precos where ref = '4607784' order by data desc
	exec up_facturacao_calcCompData '5133285', '20151201'
	exec up_facturacao_calcCompData '5133285', '20160428'
	 
	ID_PRECO = 1   => pvp
	ID_PRECO = 101 => pref
	ID_PRECO = 301 => pvpMAX
	ID_PRECO = 401 => pvpTOP5

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_facturacao_calcCompData]') IS NOT NULL
	drop procedure dbo.up_facturacao_calcCompData
GO

create procedure dbo.up_facturacao_calcCompData
	@ref		char(18)
	,@data		as datetime

/* WITH ENCRYPTION */
AS
BEGIN
	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosBase'))
		DROP TABLE #dadosBase
	
	--
	--select *
	--into #dadosBase
	--from hist_precos (nolock) 
	--where ref = @ref
	--and @data between data and (case when data_fim = '1900-01-01' then GETDATE() + 1 else data_fim end)
	--order by id_preco, data_fim desc

	--
	select *
	into #dadosBase
	from hist_precos (nolock) 
	where ref = @ref
	and (data <= @data and (case when data_fim = '1900-01-01' then GETDATE() + 1 else data_fim end) >= @data)
	order by id_preco, data_fim desc

	--
	declare @PRECO_ID_1 as numeric(10,3), @PRECO_ID_101 as numeric(10,3), @PRECO_ID_301 as numeric(10,3), @PRECO_ID_401 as numeric(10,3)

	select top 1 @PRECO_ID_1 = preco from #dadosBase where id_preco = 1
	select top 1 @PRECO_ID_101 = preco from #dadosBase where id_preco = 101
	select top 1 @PRECO_ID_301 = preco from #dadosBase where id_preco = 301
	select top 1 @PRECO_ID_401 = preco from #dadosBase where id_preco = 401

	-- result set final 
	select 
		ref
		,PRECO_ID1	 = isnull(@PRECO_ID_1,0)
		,PRECO_ID101 = isnull(@PRECO_ID_101,0)
		,PRECO_ID301 = isnull(@PRECO_ID_301,0)
		,PRECO_ID401 = isnull(@PRECO_ID_401,0)
	from #dadosBase
	group by ref
	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosBase'))
		DROP TABLE #dadosBase

END

GO
Grant Execute On dbo.up_facturacao_calcCompData to Public
Grant Control On dbo.up_facturacao_calcCompData to Public
GO