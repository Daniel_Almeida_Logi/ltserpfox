-- Tabelas de Iva
-- exec up_facturacao_tabIva 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_facturacao_tabIva]') IS NOT NULL
	drop procedure dbo.up_facturacao_tabIva
go

create procedure dbo.up_facturacao_tabIva

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON	
	Select	
		codigo,	
		taxa 
	From	
		Taxasiva (nolock)	

GO
Grant Execute On dbo.up_facturacao_tabIva to Public
Grant Control On dbo.up_facturacao_tabIva to Public
Go