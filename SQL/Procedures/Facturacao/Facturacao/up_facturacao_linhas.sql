/* 
	exec up_facturacao_linhas 1, 1, ''
	exec up_facturacao_linhas 0, 1, 'FR308D2611-551C-4692-905'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_facturacao_linhas]') IS NOT NULL
	drop procedure dbo.up_facturacao_linhas
go

create procedure dbo.up_facturacao_linhas
@info bit
,@site_nr tinyint
,@ftstamp as varchar(25)

/* WITH ENCRYPTION */
AS
	if @info = 1
	begin 
		SELECT 
			top 0
			pesquisa = convert(bit,0)
			,usalote = convert(bit,0)
			,alertalote = convert(bit,0)
			,convert(bit,0) as compSNS
			,fi.* 
			,comp_tipo = isnull(fi_comp.comp_tipo,0)
			,comp = isnull(fi_comp.comp ,0)
			,comp_diploma = isnull(fi_comp.comp_diploma,0)
			,comp_tipo_2 = isnull(fi_comp.comp_tipo_2,0)
			,comp_2 = isnull(fi_comp.comp_2,0)
			,comp_diploma_2 = isnull(fi_comp.comp_diploma_2,0)
			,cnpem = ISNULL(fprod.cnpem,'')
			,AlertaPic = convert(bit,0)
			,fi.id_Dispensa_Eletronica_D as id_validacaoDem
			,manipulado = convert(bit,0)
			,convert(varchar(2),'') as tipoR
			,convert(varchar(21),'') as id_validacaoDem
			,convert(varchar(20),'') as receita
			,convert(varchar(40),'') as token
			,convert(bit,0) as dem
			,0 as qtDem
			,0.00 as PvpTop4
			,posologia_c=convert(varchar(200),fi.posologia)
			,convert(varchar(40),'') as motisencao 
			,convert(varchar(20),'') as tpres
			,convert(bit,0) as encomenda 
			,convert(varchar(40),'') as EncReceita
			,convert(varchar(40),'') as EncPinReceita
			,convert(varchar(40),'') as EncCodOpReceita
			,convert(varchar(40),'') as EncPagam
			,convert(varchar(40),'') as EncMomPagam
			,convert(varchar(40),'') as bostamp
			,convert(varchar(40),'') as nmdos
			,convert(varchar(40),'') as codigoDemResposta
			,0                       as tipoErro
			,isnull(fi2.codmotiseimp,'') as codmotiseimp
			,isnull(fi2.motiseimp,'') as motiseimp
			,ISNULL(round(st.epcusto,2),0)		as epcusto
			,CAST(0 as bit) as forceCalcCompart
		FROM 
			ft (nolock)
			inner join FI (nolock) on fi.ftstamp = ft.ftstamp 
			left join fi_comp (nolock) on fi.fistamp = fi_comp.id_fi
			left join fprod (nolock) on fprod.cnp = fi.ref
			left join fi2 (nolock) on fi.fistamp=fi2.fistamp
			left join st (nolock) on fi.ref = st.ref and st.site_nr = @site_nr
	end
	else
	begin 
		SELECT 
			pesquisa = convert(bit,0)
			,usalote = isnull(st.usalote,convert(bit,0))
			,alertalote = convert(bit,0)
			,convert(bit,0) as compSNS
			,fi.* 
			,comp_tipo = isnull(fi_comp.comp_tipo,0)
			,comp = isnull(fi_comp.comp ,0)
			,comp_diploma = isnull(fi_comp.comp_diploma,0)
			,comp_tipo_2 = isnull(fi_comp.comp_tipo_2,0)
			,comp_2 = isnull(fi_comp.comp_2,0)
			,comp_diploma_2 = isnull(fi_comp.comp_diploma_2,0)
			,cnpem = ISNULL(fprod.cnpem,'')
			,AlertaPic = convert(bit,0)
			,manipulado = convert(bit,0)
			,convert(varchar(2),'') as tipoR
			--,convert(varchar(21),'') as id_validacaoDem
			,id_Dispensa_Eletronica_D as id_validacaoDem
			,convert(varchar(20),'') as receita
			,convert(varchar(40),'') as token
			,convert(bit,0) as dem
			,0 as qtDem
			,0.00 as PvpTop4
			,posologia_c=convert(varchar(200),fi.posologia)
			,motisencao = isnull((select isnull((select campo from b_multidata where b_multidata.multidatastamp=taxasiva.multidataStamp),'') from taxasiva where taxasiva.codigo=fi.tabiva and fi.iva=0),'')
			,convert(varchar(20),'') as tpres
			,convert(bit,0) as encomenda 
			,convert(varchar(40),'') as EncReceita
			,convert(varchar(40),'') as EncPinReceita
			,convert(varchar(40),'') as EncCodOpReceita
			,convert(varchar(40),'') as EncPagam
			,convert(varchar(40),'') as EncMomPagam
			,convert(varchar(40),'') as bostamp
			,convert(varchar(40),'') as nmdos
			,convert(varchar(40),'') as codigoDemResposta
			,0                       as tipoErro
			,isnull(fi2.codmotiseimp,'') as codmotiseimp
			,isnull(fi2.motiseimp,'') as motiseimp
			,ISNULL(round(st.epcusto,2),0)		as epcusto
			,CAST(0 as bit) as forceCalcCompart
		FROM 
			ft(nolock)
			inner join FI (nolock) on fi.ftstamp = ft.ftstamp
			left join fi_comp (nolock) on fi.fistamp = fi_comp.id_fi
			left join st (nolock) on fi.ref = st.ref and st.site_nr = @site_nr
			left join fprod (nolock) on fprod.cnp = fi.ref
			left join fi2 (nolock) on fi.fistamp=fi2.fistamp
		WHERE 
			fi.ftstamp = @ftstamp
		Order By 
			fi.Lordem 
			,fi.ref
	end	

GO
Grant Execute On dbo.up_facturacao_linhas to Public
Grant Control On dbo.up_facturacao_linhas to Public
Go

