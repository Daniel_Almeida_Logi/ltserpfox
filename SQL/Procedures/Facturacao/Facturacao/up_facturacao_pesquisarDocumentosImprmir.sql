-- IMPRIMIR a Pesquisa de Documentos Facturação de Back-Office
-- exec up_facturacao_pesquisarDocumentosImprmir 30, '', 0, 0, -1, '20130606',  '20130606', '', 1, 'Administrador','', '', '','Loja 1', ''
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_facturacao_pesquisarDocumentosImprmir]') IS NOT NULL
	drop procedure dbo.up_facturacao_pesquisarDocumentosImprmir
go

create procedure dbo.up_facturacao_pesquisarDocumentosImprmir

@topo		int,
@entidade	varchar(55),
@numdoc		numeric(10,0),
@no			numeric(10,0),
@estab		numeric(3,0),
@dataini	datetime,
@datafim	datetime,
@doc		varchar(20),
@user		numeric(6),
@group		varchar(50),
@design		varchar(60),
@atend		varchar(20),
@receita	varchar(20),
@site		varchar(20),
@ncont		varchar(20)

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

;with
	cte1(nmdoc)as
	(
		select 
			nmdoc 
		from 
			td (nolock)
		where 
			dbo.up_PerfilFacturacao(@user, @group, RTRIM(LTRIM(td.nmdoc)) + ' - ' + 'Visualizar',@site) = 0
	)
select top (@topo)
	* 
from (
	SELECT 
		Tipodoc		= 'FT',
		fdata		= ft.fdata,
		QTTOT		= ft.totqtt,
		ESCOLHA		= convert(bit,0),
		Operador	= FT.VENDNM,
		CabStamp	= FT.ftstamp,
		Datav		= ft.fdata,
		Data		= CONVERT(varchar,ft.fdata,102),
		OUSRDATA	= FT.OUSRDATA, 
		OUSRHORA	= FT.OUSRHORA,
		Documento	= FT.NMDOC,
		Numdoc		= FT.FNO,
		Entidade	= UPPER(FT.NOME),
		NO			= FT.NO, 
		Estab		= FT.ESTAB,
		Total		= ft.etotal,
		u_nratend	= FT.u_nratend,
		u_receita	= ft2.u_receita,
		id			= ROW_NUMBER() over(partition by ft.ftstamp order by fdata)
	FROM FT (nolock)
		inner join FI (nolock) on ft.ftstamp=fi.ftstamp
		inner join ft2 (nolock) on ft2.ft2stamp=ft.ftstamp
	WHERE
		ft.fdata between @dataini AND @datafim
		and ft.nmdoc in (select nmdoc from cte1)
		and ft.nome LIKE @entidade + '%'
		and (fi.design like @design + '%' OR fi.ref like @design + '%')
		and ft2.u_receita = case when @receita = '' then ft2.u_receita else @receita end
		and ft.site = case when @site = '' then ft.site else @site end
		and ft.fno = case when @numdoc = 0 then ft.fno else @numdoc end
		and u_nratend = case when @atend = '' then u_nratend else @atend end
		and ft.nmdoc = case when @doc = '' then ft.nmdoc else @doc end
		AND NO = case when @no = 0 then no else @no end
		AND ESTAB = case when @estab = -1 then estab else @estab end
		and ft.ncont = case when @ncont = '' then ft.ncont else @ncont end
)x
where x.id = 1
order by convert(varchar,ousrdata,102)+OUSRHORA desc
	
GO
Grant Execute On dbo.up_facturacao_pesquisarDocumentosImprmir to Public
Grant Control On dbo.up_facturacao_pesquisarDocumentosImprmir to Public
Go