-- Ver o ultimo documento Alterado - Facturação
-- exec up_facturacao_ultimo 1,'Administrador'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_facturacao_ultimo]') IS NOT NULL
	drop procedure dbo.up_facturacao_ultimo
go

create procedure dbo.up_facturacao_ultimo
@useno	numeric(5,0),
@grupo	varchar(20)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON	




	declare @site as varchar(60)
	set @site = isnull((select top 1 loja from b_us where userno = @useno),'')

	;with 
		cte1 (ndoc,fno,cabstamp,usrdata,usrhora,mydata,nomedoc,perfil) as 
		(
			Select
				ndoc, fno, ftstamp as cabstamp, 
				usrdata, usrhora, usrdata+usrhora as mydata,
				NMDOC as NOMEDOC, dbo.up_PerfilFacturacao(@useno, @grupo, RTRIM(LTRIM(nmdoc)) + ' - ' + 'Visualizar',@site)
			From
				FT (nolock)
			Where 
				DATEDIFF(day,fdata,GETDATE()) < 15	
		)
	select	
		ndoc, fno, cabstamp, mydata, nomedoc, perfil
	from
		cte1
	where
	 	usrdata + usrhora =  ((select MAX(usrdata+usrhora) from cte1 where perfil=0))



GO
Grant Execute On dbo.up_facturacao_ultimo to Public
Grant Control On dbo.up_facturacao_ultimo to Public
Go