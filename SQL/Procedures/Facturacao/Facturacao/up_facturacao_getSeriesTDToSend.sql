/* 
	Buscar s�ries de Fatura��o para Registar na AT
	exec up_facturacao_getSeriesTDToSend 'ADM44968FE8-C134-485D-898'
	up_facturacao_getSeriesTDToSend 'ADM3CA6ABDC-0CCC-4256-861'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_facturacao_getSeriesTDToSend]') IS NOT NULL
	drop procedure dbo.up_facturacao_getSeriesTDToSend
go

create procedure dbo.up_facturacao_getSeriesTDToSend

@token as varchar(100)
/* WITH ENCRYPTION */
AS

	SELECT 
		serie,
		tipoSerie,
		classeDoc,
		tipoDoc,
		dataInicioPrevUtiliz,
		numCertSWFatur,
		meioProcessamento,
		numInicialSeq,
		tabOri
	FROM 
		req_regTD(nolock)
	WHERE
		token = @token
		and tabOri!=''
	ORDER BY
		ousrdata asc

GO
Grant Execute On dbo.up_facturacao_getSeriesTDToSend to Public
Grant Control On dbo.up_facturacao_getSeriesTDToSend to Public
Go


