SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_getTotAtendimento]') IS NOT NULL
	drop procedure dbo.up_getTotAtendimento
go

create procedure [dbo].up_getTotAtendimento
@nrAtend VARCHAR(20)

AS

	SELECT
		SUM((CASE WHEN FT.nmdoc = 'Ft. Seguradoras' THEN (fi.epv + (fi.epv * (fi.iva/100)))*fi.qtt ELSE fi.etiliquido END)) as totAtendimento
	FROM
		fi(nolock)
		join ft(nolock) on ft.ftstamp = fi.ftstamp
	WHERE
		ft.u_nratend = @nrAtend

GO
Grant Execute On dbo.up_getTotAtendimento to Public
Grant Control On dbo.up_getTotAtendimento to Public
GO