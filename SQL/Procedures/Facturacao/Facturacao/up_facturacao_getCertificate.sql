/* 
	exec up_facturacao_getCertificate 'da22dc65-5b87-45aa-a7e6-7aa52fffa97'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_facturacao_getCertificate]') IS NOT NULL
	drop procedure dbo.up_facturacao_getCertificate
go

create procedure dbo.up_facturacao_getCertificate
	@stampCertificate VARCHAR(36)
AS
	SELECT 
		ISNULL(docsCertificateResponse.id, '') AS id, 
		ISNULL(docsCertificateResponse_Docs.signStatusId, 0) AS signStatusId,
		ISNULL(docsCertificateResponse_Docs.externalReference, '') AS externalReference,
		ISNULL(docsCertificateResponse_Docs.name, '') as name
	FROM 
		docsCertificateResponse_Docs(nolock)
		LEFT join docsCertificateResponse(nolock) on docsCertificateResponse.documentsToken = docsCertificateResponse_Docs.documentsToken
	WHERE 
		docsCertificateResponse_Docs.stamp=@stampCertificate

GO
Grant Execute On dbo.up_facturacao_getCertificate to Public
Grant Control On dbo.up_facturacao_getCertificate to Public
Go