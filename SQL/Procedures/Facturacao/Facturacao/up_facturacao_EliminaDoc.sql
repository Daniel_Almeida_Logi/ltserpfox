-- Elimina Documento de Facturação
-- exec up_facturacao_EliminaDoc 'ADM10080182913.485000003'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_facturacao_EliminaDoc]') IS NOT NULL
	drop procedure dbo.up_facturacao_EliminaDoc
go
create procedure dbo.up_facturacao_EliminaDoc

@ftstamp as char(25)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

BEGIN TRANSACTION
	Delete From Ft	Where Ftstamp	=	@ftstamp
	Delete From Ft2 Where Ft2stamp	=	@ftstamp
	Delete From Fi	Where Ftstamp	=	@ftstamp
COMMIT
GO
Grant Execute On dbo.up_facturacao_EliminaDoc to Public
Grant Control On dbo.up_facturacao_EliminaDoc to Public
GO
