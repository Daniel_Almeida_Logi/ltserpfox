/*
	exec up_faturacao_getTsre '206','Loja 1'
	update B_Parameters set bool=0 where stamp = 'ADM0000000308'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_faturacao_getTsre]') IS NOT NULL
	drop procedure dbo.up_faturacao_getTsre
go


create procedure [dbo].up_faturacao_getTsre

	@ndoc		numeric(5,0) = 0,
	@site		varchar(20)	 = ''

AS
	DECLARE @recPorLoja  bit = 0	 
	SET @recPorLoja = (select bool from B_Parameters(nolock) where stamp = 'ADM0000000308')
	SET @recPorLoja = ISNULL(@recPorLoja,0)
	
	DECLARE @sql	varchar(max) = ''
	
	set @sql = N'
		SELECT 
			u_tipodoc,
			nmdoc
		FROM
			tsre(nolock)
		WHERE
			ndoc = '+convert(varchar(4),@ndoc)+''
		IF(@recPorLoja = 1)
		BEGIN
			set @sql = @sql +' and [site] = '''+@site+''''
		END
		
		print @sql
		exec (@sql)

GO
Grant Execute On dbo.up_faturacao_getTsre to Public
Grant control On dbo.up_faturacao_getTsre to Public
GO