/*
	Painel de Importação de Documentos de Facturação
	
	exec up_facturacao_ImportActualizaLinhas  0, 0, 'ADM27C70998-76D5-407B-90D;ADM0D0B008E-075B-41C0-B78;ADM5B08215D-50D3-416D-9B4'
	exec up_facturacao_ImportActualizaLinhas  0, 0, '3', 'TARV'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_facturacao_ImportActualizaLinhas]') IS NOT NULL
	drop procedure dbo.up_facturacao_ImportActualizaLinhas
go

create procedure dbo.up_facturacao_ImportActualizaLinhas
	@quantidadeMov int
	,@quantidade int
	,@cabstamp as varchar(MAX)
	,@site varchar(20) = ''

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON


DECLARE @ul_cab AS TABLE (cab VARCHAR(25))


INSERT INTO 
	@ul_cab (cab)
SELECT 
	y.value
FROM 
	dbo.SplitString(@cabStamp, ';') x
	cross apply dbo.SplitString(x.value, ';') y

if @site = 'TARV'
begin

	SELECT
		FISTAMP = t.id, copiar = 0, t.REF, DESIGN = t.descr, EPV = 0, QTT = t.qt, ETILIQUIDO = 0, qttmov = t.qt_dispensada, tabiva=0, cabStamp = t.id_ext_tarv, STAMP = t.id, tipo = 'TARV', DOCUMENTO = RTRIM(LTRIM(t.id_prescricao))
	FROM
		ext_tarv_prescricao t (nolock)
	WHERE
		t.id_ext_tarv in (select cab from @ul_cab) AND t.qt > t.qt_dispensada

END ELSE BEGIN

	SELECT
		FISTAMP, 
		copiar = 0, 
		REF, 
		DESIGN, 
		EPV, 
		QTT, 
		ETILIQUIDO, 
		qttmov = fi.pbruto,  
		tabiva=fi.tabiva, 
		cabStamp = fi.ftstamp, 
		STAMP = fi.fistamp, 
		tipo = 'FT', 
		DOCUMENTO = RTRIM(LTRIM(fi.fno))
	FROM
		FI (nolock)
	WHERE
		ftstamp in (select cab from @ul_cab) 
		AND Fi.QTT > (CASE WHEN  @quantidade = 1 THEN 0 ELSE fi.qtt -1 END)
		AND fi.pbruto < (CASE WHEN @quantidadeMov = 1 THEN fi.qtt ELSE fi.pbruto + 1 END)
		
	UNION ALL
		
	SELECT
		BISTAMP, 
		copiar = 0, 
		REF, 
		DESIGN, 
		EDEBITO, 
		QTT, 
		ETTDEB, 
		qttmov = bi.pbruto, 
		tabiva=0, 
		cabStamp = bi.bostamp , 
		STAMP = bi.bistamp, 
		tipo = 'BO', 
		DOCUMENTO = RTRIM(LTRIM(bi.obrano))
	FROM
		BI (nolock)
	WHERE
		bostamp in (select cab from @ul_cab) 
		AND BI.QTT > (CASE WHEN  @quantidade = 1 THEN 0 ELSE bi.qtt -1 END) 
		AND bi.qtt2 < (CASE WHEN @quantidadeMov = 1 THEN bi.qtt ELSE bi.qtt2 + 1 END)

	UNION ALL
		
	SELECT
		BISTAMP, 
		copiar = 0, 
		REF, 
		DESIGN, 
		epv, 
		QTT, 
		etiliquido, 
		qttmov = fn.pbruto, 
		tabiva=0, 
		cabStamp = fn.fostamp , 
		STAMP = fn.fnstamp, 
		tipo = 'FO', 
		DOCUMENTO = RTRIM(LTRIM(fo.adoc))
	FROM
		FN (nolock)
		JOIN FO(nolock) on fo.fostamp = fn.fostamp
	WHERE
		fo.fostamp in (select cab from @ul_cab) 
		AND fn.QTT > (CASE WHEN  @quantidade = 1 THEN 0 ELSE fn.qtt -1 END)
		AND fn.pbruto < (CASE WHEN @quantidadeMov = 1 THEN fn.qtt ELSE fn.pbruto + 1 END)


END

GO
Grant Execute On dbo.up_facturacao_ImportActualizaLinhas to Public
Grant Control On dbo.up_facturacao_ImportActualizaLinhas to Public
GO