/* 
	exec up_facturacao_ft2 'ADM986782D5-1DC9-4A47-A4F'


	update b_parameters set bool = 1 where stamp = 'ADM0000000270'
	update b_parameters set bool = 0 where stamp = 'ADM0000000270'
	

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_facturacao_ft2]') IS NOT NULL
	drop procedure dbo.up_facturacao_ft2
go

create procedure dbo.up_facturacao_ft2
@ft2stamp as varchar(25)

/* WITH ENCRYPTION */
AS
	declare @consulta bit
	set @consulta = (select bool from b_parameters where stamp = 'ADM0000000270')

	IF @consulta = 0
	begin
		SELECT
			respostaDt = isnull((select top 1 DescrResposta from b_dt_resposta WHERE stampDoc = @ft2stamp order by DataEnvio),'')
			,ft2stamp, modop1, epaga1, modop2,epaga2, eacerto, etroco, evdinheiro
			,subproc, vdollocal, vdlocal, c2no ,c2nome ,c2estab, c2codpost, c2pais, horaentrega, obsdoc
			,morada, local, codpost, telefone, contacto, email, ltstamp, ltstamp2, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
			,reexgiva, motiseimp, formapag, assinatura, u_codigo2, u_abrev, u_abrev2, u_design2, u_acertovs, u_vdsusprg, u_entpresc, u_nopresc, u_codigo
			,u_receita, u_nbenef, u_design, u_viatura, u_vddom, u_nbenef2, u_docorig, exportado, epaga3, epaga4, epaga5, epaga6
			,codAcesso = convert(varchar(6),''), codDirOpcao = convert(varchar(4),'')
			,token , token_anulacao_compl, token_efectivacao_compl
			,ft2.cativa
			,ft2.valcativa
			,ft2.perccativa
			,ft2.nrelegibilidade
			,ft2.stamporigproc
			,ft2.impresso
			,ft2.u_hclstamp1
			,ft2.valcartao
			,ft2.obsCl
			,ft2.obsInt
			,ft2.xpdCodPost
			,ft2.xpdLocalidade
			,ft2.stampCertificate
			,ft2.idCertXML
			,ft2.idCreateXML
			,ft2.id_efr_externa
		FROM 
			ft2 (nolock)
		WHERE
			ft2stamp = LTRIM(RTRIM(@ft2stamp))
	end
	ELSE
	begin
		SELECT
				respostaDt = isnull((select top 1 DescrResposta from b_dt_resposta WHERE stampDoc = @ft2stamp order by DataEnvio),'')
				,ft2stamp, modop1, epaga1, modop2,epaga2, eacerto, etroco, evdinheiro
				,subproc, vdollocal, vdlocal, c2no ,c2nome ,c2estab, c2codpost, c2pais, horaentrega, obsdoc
				,morada, local, codpost, telefone, contacto, email, ltstamp, ltstamp2, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
				,reexgiva, motiseimp, formapag, assinatura, u_codigo2, u_abrev, u_abrev2, u_design2, u_acertovs, u_vdsusprg, u_entpresc, u_nopresc, u_codigo
				,u_receita, u_nbenef, u_design, u_viatura, u_vddom, u_nbenef2, u_docorig, exportado, epaga3, epaga4, epaga5, epaga6
				,codAcesso, codDirOpcao 
				,token, token_anulacao_compl, token_efectivacao_compl
				,ft2.cativa
				,ft2.valcativa
				,ft2.perccativa
				,ft2.nrelegibilidade
				,ft2.stamporigproc
				,ft2.impresso
				,ft2.u_hclstamp1
				,ft2.valcartao
				,ft2.obsCl
				,ft2.obsInt
				,ft2.xpdCodPost
				,ft2.xpdLocalidade
				,ft2.id_efr_externa
			FROM 
				ft2 (nolock)
			WHERE
				ft2stamp = LTRIM(RTRIM(@ft2stamp))
	end
			

GO
Grant Execute On dbo.up_facturacao_ft2 to Public
Grant Control On dbo.up_facturacao_ft2 to Public
Go

