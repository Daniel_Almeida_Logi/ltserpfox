/* 
	Buscar s�ries de Fatura��o para Registar na AT

	EXEC getRegTD 'Loja 1'


*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[getRegTD]') IS NOT NULL
	drop procedure dbo.getRegTD
go

create procedure dbo.getRegTD
	@site VARCHAR(18)

/* WITH ENCRYPTION */
AS

	SELECT 
		*
	FROM
		(
		SELECT 
			CAST(0 as BIT) as sel,
			ndoc,
			nmdoc,
			ATCUD,
			tiposaft,
			(SELECT TOP 1 classe FROM b_classeSAFT(nolock) WHERE tiposaft = td.tiposaft) as classe,
			1 AS ordem,
			'TD' as origem
		FROM 
			td(nolock)
		WHERE
			site = @site
			AND tiposaft <> ''

		UNION ALL

		SELECT 
			CAST(0 as BIT) as sel,
			cm as ndoc,
			cmdesc as nmdoc,
			ATCUD,
			tiposaft,
			(SELECT TOP 1 classe FROM b_classeSAFT(nolock) WHERE tiposaft = cm1.tiposaft) as classe,
			2 AS ordem,
			'CM1' as origem
		FROM	
			cm1(nolock)
		WHERE
			tiposaft <> ''


		UNION ALL

		SELECT 
			CAST(0 as BIT) as sel,
			ndoc,
			nmdoc,
			ATCUD,
			codsaft as tiposaft,
			(SELECT TOP 1 classe FROM b_classeSAFT(nolock) WHERE tiposaft = tsre.codsaft) as classe,
			3 AS ordem,
			'TSRE' as origem
		FROM	
			tsre(nolock)
		WHERE
			codsaft <> ''
			and (site = @site or site='')

		UNION ALL

		SELECT 
			CAST(0 as BIT) as sel,
			ndos as ndoc,
			nmdos as nmdoc,
			ATCUD,
			tiposaft,
			(SELECT TOP 1 classe FROM b_classeSAFT(nolock) WHERE tiposaft = ts.tipoSaft) as classe,
			4 as ordem,
			'TS' as origem
		FROM
			ts(nolock)
		WHERE
			tiposaft <> ''
			and 1 = (CASE WHEN site = '' THEN 1 ELSE (CASE WHEN site = @site THEN 1 else 0 END) END)

		) AS x
	WHERE
		ISNULL(x.classe,'') <> ''
	ORDER BY 
		x.ordem asc, x.ndoc asc

GO
Grant Execute On dbo.getRegTD to Public
Grant Control On dbo.getRegTD to Public
Go