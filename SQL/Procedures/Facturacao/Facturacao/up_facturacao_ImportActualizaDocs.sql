/*
	exec up_facturacao_ImportActualizaDocs 200, 'Venda a Dinheiro', 0, '20170101','20170301', 0, 'Loja 1','ben'
	
	exec up_facturacao_ImportActualizaDocs 0, '', 0, '20140101','20171231', 0, 'TARV'

	Notas:
		select * from ext_tarv_prescricao
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_facturacao_ImportActualizaDocs]') IS NOT NULL
    drop procedure dbo.up_facturacao_ImportActualizaDocs
go

create procedure dbo.up_facturacao_ImportActualizaDocs
	@no as numeric(10),
	@docnome as varchar(MAX),
	@filtra as int,
	@dataIni as datetime,
	@dataFim as datetime,
	@mostrafechados as int,
	@site as varchar(20),
	@prod as varchar(99) = '',
	@estab as numeric(10) = -1

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

DECLARE @ul_docs AS TABLE (doc VARCHAR(25))


INSERT INTO 
	@ul_docs (doc)
SELECT 
	y.value
FROM 
	dbo.SplitString(@docnome, ',') x
	cross apply dbo.SplitString(x.value, ',') y

SET @prod = isnull(ltrim(rtrim(@prod)),'')
SET @prod = REPLACE(@prod,' ','%')

if @site = 'TARV'
begin

	/* TARV */
	select
		distinct
		*
	from (
		SELECT

			distinct 
			 'QTTOT' = SUM(t.qt)
			,'Operador' = 'ADM'
			,'HORA' = convert(varchar,data_prescricao,108)
			,'TIPO' = 'TARV'
			,'ESTADO' = ''
			,'CABSTAMP' = id_ext_tarv
			--,'LINSTAMP' = (Select top 1 id_ext_tarv from ext_tarv_prescricao (nolock) where id_ext_tarv = t.id_ext_tarv)
			,'LINSTAMP' = id_ext_tarv
			,'COPIAR' = 0
			,'TIPODOC' = 'TARV'
			,'DOCUMENTO' = id_prescricao
			,'data' = data_prescricao
			,'VALOR' = 0
			,'u_copref' = 1
			,qt_reg = sum(t.qt_dispensada)
		FROM
			ext_tarv_prescricao t (nolock)
		WHERE
			convert(date,t.data_prescricao) between @dataIni and @dataFim
			and t.id_doente = @no
		group by
			t.data_prescricao, t.id_ext_tarv, t.id_prescricao
	) x
	where
		1 = case
				when @mostrafechados=0 then 1
				else case
						when x.qttot > x.qt_reg then 1
						else 0
						end
				end
	ORDER BY
		x.data DESC

end else begin


	Select 
		
		*
	FROM (
		/* Facturas */
		SELECT
			distinct
			 'QTTOT'			=	SUM(fi.QTT)
			,'Operador'			=	ft.ousrinis--ft.vendnm
			,'HORA'				=	ft.ousrhora
			,'TIPO'				=	'FT'
			,'ESTADO'			=	''
			,'CABSTAMP'			=	ft.FTSTAMP
			,'LINSTAMP'			=	isnull((Select top 1 fistamp from fi (nolock) where ft.ftstamp = fi.ftstamp),'')
			,'COPIAR'			=	0
			,'TIPODOC'			=	ft.nmdoc
			,'DOCUMENTO'		=	CONVERT(varchar,ft.Fno)
			,'data'				=	ft.fdata
			,'VALOR'			=	ft.ETOTAL
			,'u_copref'			=	td.u_copref
			,'tiposaft'			=	td.tiposaft
			,'ndoc'				=	CONVERT(varchar,ft.ndoc)
			,'id_efr_externa'	=	ft2.id_efr_externa
		FROM
			FT (nolock)
			inner	join fi (nolock) on fi.ftstamp = ft.ftstamp
			inner	join td (nolock) on td.ndoc = ft.ndoc
			left	join ft2(nolock) on ft2.ft2stamp = ft.ftstamp 
		WHERE
			FT.nmdoc in (select doc from @ul_docs)
			AND FT.no = case when @filtra=1 then @no else ft.no end
			AND FT.estab = case when @filtra=1 and  @estab> -1 then @estab else ft.estab end
			AND ft.fdata between @dataIni and @dataFim
			and ft.site = @site
			AND 
			(fi.ref = case when @prod='' then fi.ref else ltrim(rtrim(left(@prod,18))) end
			Or
			fi.design like case when @prod='' then fi.design else + '%'+  @prod +'%' end)
		group by
			ft.vendnm, ft.ousrhora, ft.FTSTAMP, ft.nmdoc, ft.Fno, ft.fdata,ft.ETOTAL, td.u_copref, ft.ousrinis, td.tiposaft, ft.ndoc, ft2.id_efr_externa
	    
		/* Dossiers */
		UNION ALL
	    
		SELECT
			distinct
			'QTTOT'			=	SUM(bi.QTT)
			,'OPERADOR'		=	bo.ousrinis--bo.vendnm
			,'HORA'			=	bo.ousrhora
			,'TIPO'			=	'BO'
			,'ESTADO'		=	CASE WHEN bo.FECHADA = 0 THEN 'A' ELSE 'F' END
			,'CABSTAMP'		=	bo.bostamp
			,'LINSTAMP'		=	isnull((Select top 1 bistamp from bi (nolock) where bi.bostamp = bo.bostamp),'')
			,'COPIAR'		=	0
			,'TIPODOC'		=	bo.NMDOS
			,'DOCUMENTO'	=	CONVERT(varchar,bo.OBRANO)
			,'DATA'			=	bo.dataobra
			,'VALOR'		=	bo.ETOTAL
			,'u_copref'		=	ts.u_copref
			,'tiposaft'		=	ts.tiposaft
			,'ndoc'			=	CONVERT(varchar,bo.ndos)
			,'id_efr_externa'= ''
		FROM
			BO (nolock)
			inner join bi (nolock) on bi.bostamp = bo.bostamp
			inner join ts (nolock) on ts.ndos = bi.ndos
		Where
			BO.nmdos in (select doc from @ul_docs)
			AND BO.no = case when @filtra=1 then @no else bo.no end
			AND bo.dataobra between @dataIni and @dataFim
			AND bo.fechada = Case when @mostrafechados = 0 Then bo.fechada ELSE 0 END
			and bo.site = @site
			AND 
			(bi.ref = case when @prod='' then bi.ref else ltrim(rtrim(left(@prod,18))) end
			Or
			bi.design like case when @prod='' then bi.design else + '%'+  @prod +'%' end)
		group by
			bo.vendnm, bo.ousrhora, bo.FECHADA, bo.bostamp,bo.NMDOS, bo.OBRANO, bo.dataobra, bo.ETOTAL, ts.u_copref, bo.ousrinis, ts.tiposaft, bo.ndos

		/* Compras */
		UNION ALL
	    
		SELECT
			distinct
			'QTTOT'			=	SUM(fn.QTT)
			,'OPERADOR'		=	fo.ousrinis--bo.vendnm
			,'HORA'			=	fo.ousrhora
			,'TIPO'			=	'FO'
			,'ESTADO'		=	fo.u_status
			,'CABSTAMP'		=	fo.fostamp
			,'LINSTAMP'		=	isnull((Select top 1 fnstamp from fn (nolock) where fn.fostamp = fo.fostamp),'')
			,'COPIAR'		=	0
			,'TIPODOC'		=	fo.docnome
			,'DOCUMENTO'	=	fo.adoc
			,'DATA'			=	fo.docdata
			,'VALOR'		=	fo.ETOTAL
			,'u_copref'		=	cm1.u_copref
			,'tiposaft'		=	''
			,'ndoc'			=	''
			,'id_efr_externa' = ''
		FROM
			FO (nolock)
			inner join fn (nolock) on fn.fostamp = fo.fostamp
			inner join cm1 (nolock) on cm1.cm = fo.doccode
		Where
			FO.docnome in (select doc from @ul_docs)
			AND FO.no = case when @filtra=1 then @no else fo.no end
			AND fo.docdata between @dataIni and @dataFim
			AND fo.u_status = Case when @mostrafechados = 0 Then fo.u_status ELSE 'A' END
			and fo.site = @site
			AND 
			(fn.ref = case when @prod='' then fn.ref else ltrim(rtrim(left(@prod,18))) end
			Or
			fn.design like case when @prod='' then fn.design else + '%'+  @prod +'%' end)
		group by
			fo.ousrhora, fo.u_status, fo.fostamp,fo.docnome, fo.adoc, fo.docdata, fo.ETOTAL, cm1.u_copref, fo.ousrinis

	) a
	ORDER BY 
		DATA desc, HORA DESC


end
	    
GO
Grant Execute On dbo.up_facturacao_ImportActualizaDocs to Public
Grant Control On dbo.up_facturacao_ImportActualizaDocs to Public
GO