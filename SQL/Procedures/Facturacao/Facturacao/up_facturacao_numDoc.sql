-- Numero do documento de Facturação
-- select * from td
-- exec up_facturacao_numDoc '1', 2021, 'Loja 1'
-- exec up_facturacao_numDoc 1102, 2019
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_facturacao_numDoc]') IS NOT NULL
	drop procedure dbo.up_facturacao_numDoc
go

create procedure dbo.up_facturacao_numDoc
@ndoc	numeric(6)
,@ano	numeric(4)
,@site	varchar(15) = 'Loja 1'

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON
	Select
		Isnull(Max(ft.fno),0)+1 as numdoc
	From
		Ft (nolock)
	Where
		ft.ndoc = @ndoc
		AND YEAR(Fdata) between @ano-(case when (select bool from B_Parameters_site where stamp='ADM0000000142' and site=@site)=1 then 0 else 1 end) and @ano
GO
Grant Execute On dbo.up_facturacao_numDoc to Public
Grant Control On dbo.up_facturacao_numDoc to Public
Go