-- Workflow do documento de Facturação
-- exec up_facturacao_wk 'Factura'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_facturacao_wk]') IS NOT NULL
	drop procedure dbo.up_facturacao_wk
go

create procedure dbo.up_facturacao_wk
@nmdoc	varchar(20)

/* WITH ENCRYPTION */
AS
	
			
	SELECT	
		distinct
		a.wkreadonly
		,a.wkfield
		,a.wktitle
		,a.u_ordem + 2 as Ordem
		,a.u_largura as Largura
		,a.u_mascara as mascara
	FROM 
		WKLCOLS (nolock) a 
		INNER JOIN	WK (nolock) b ON a.wkstamp = b.wkstamp
	WHERE 
		b.WKDOCDESCR = @nmdoc
		and a.u_ordem != 0
	ORDER BY 
		Ordem ASC
	
	

GO
Grant Execute On dbo.up_facturacao_wk to Public
Grant Control On dbo.up_facturacao_wk to Public
Go
