-- Condi��o De Pagamento
-- exec up_facturacao_CondPagCliente2 22,0,'20101223'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_facturacao_CondPagCliente2]') IS NOT NULL
	drop procedure dbo.up_facturacao_CondPagCliente2
go
create procedure dbo.up_facturacao_CondPagCliente2

@no as numeric(9,0),
@estab as numeric(5,0),
@data as datetime

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

SELECT
	 'TPSTAMP'				=	TP.TPSTAMP
	,'VENCIMENTO'			=	TP.VENCIMENTO
	,'CONDPAG'				=	TP.descricao
	,'DIAS'					=	CASE WHEN TP.VENCIMENTO = 1 THEN ISNULL(b_utentes.VENCIMENTO,0) ELSE DIAS END
	,'DATAVENCIMENTO'		=	DATEADD(DAY,
									CASE WHEN TP.tipo = 1 
										THEN 
										CASE WHEN ISNULL(b_utentes.VENCIMENTO,0) = 0
											THEN 30
											ELSE b_utentes.VENCIMENTO END
										ELSE DIAS END
										,@data)
	,b_utentes.no
	,b_utentes.estab
FROM
	TP (nolock)
	INNER JOIN b_utentes (nolock) ON tp.descricao = b_utentes.tpdesc
WHERE
	b_utentes.no = @no
	AND b_utentes.estab = @estab

GO
Grant Execute On dbo.up_facturacao_CondPagCliente2 to Public
Grant Control On dbo.up_facturacao_CondPagCliente2 to Public
GO