-- Condi��o De Pagamento
-- exec up_facturacao_CondPagCliente 201,1,'Clientes 45 Dias','20111223'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_facturacao_CondPagCliente]') IS NOT NULL
	drop procedure dbo.up_facturacao_CondPagCliente
go

create procedure dbo.up_facturacao_CondPagCliente

@no as numeric(9,0),
@estab as numeric(5,0),
@condpag as varchar(150),
@data as  datetime

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

SELECT
	 'TPSTAMP'				=	tp.TPSTAMP
	,'VENCIMENTO'			=	tp.VENCIMENTO
	,'DIAS'					=	CASE
										WHEN tp.tipo = 1 THEN ISNULL(b_utentes.VENCIMENTO,0)
										ELSE DIAS
								END
	,'DATAVENCIMENTO'	=	DATEADD(DAY,CASE
											WHEN tp.tipo = 1 THEN 
												CASE WHEN ISNULL(b_utentes.VENCIMENTO,0) = 0
													THEN 30
													ELSE b_utentes.VENCIMENTO END
											ELSE DIAS
										END, @data)
FROM
	TP (nolock)
	INNER JOIN b_utentes (nolock) ON tp.descricao = b_utentes.tpdesc
WHERE
	DESCRICAO			= @condpag
	AND b_utentes.no	= @no
	AND b_utentes.estab	= @estab

GO
Grant Execute On dbo.up_facturacao_CondPagCliente to Public
Grant Control On dbo.up_facturacao_CondPagCliente to Public
GO