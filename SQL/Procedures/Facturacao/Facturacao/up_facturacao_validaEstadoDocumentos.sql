-- Ver se os documento de factura��o est�o todos com linhas e sem buracos na numera��o
-- exec up_facturacao_validaEstadoDocumentos '',2023
-- exec up_facturacao_validaEstadoDocumentos 'ATLANTICO',2023
-- exec up_facturacao_validaEstadoDocumentos '',2022

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_facturacao_validaEstadoDocumentos]') IS NOT NULL
	drop procedure dbo.up_facturacao_validaEstadoDocumentos
go

create procedure dbo.up_facturacao_validaEstadoDocumentos
	@site	varchar(18) = '',
	@ano    int = 0

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON	

	if(@ano=0)
	begin
		set @ano = Year(GetDate())
	end

	declare @ndoc  int = 0
	declare @nmdoc varchar(20) = ''
	declare @siteToUse varchar(20) = ''

	

	
	If OBJECT_ID('tempdb.dbo.#finalResult') IS NOT NULL
			DROP TABLE #finalResult
	If OBJECT_ID('tempdb.dbo.#missingNumbers') IS NOT NULL
			DROP TABLE #missingNumbers

	CREATE TABLE #finalResult (
		ndoc int,
		nmdoc varchar(20),
		ano int,
		numero int,
		razao varchar(100),
		loja varchar(100),
		fdata datetime,
		fhora varchar(100),
		ftstamp  varchar(100)
	)

	
	IF CURSOR_STATUS('global','myCursor')>=-1
	BEGIN
	 close mycursor
	 deallocate  mycursor
	END
	
	
	

	declare myCursor cursor for 
	select 
		distinct td.ndoc 
	from 
		td(nolock) 
	inner join 
		ft(nolock) on td.ndoc = ft.ndoc and td.site = case when @site = '' then td.site else @site end
	where 
		ftano=@ano 
		and ft.site= case when @site = '' then ft.site else @site end 
		and td.tiposaft!=''
		and td.excluisaft = 0
	order by td.ndoc asc



 
	open myCursor
	fetch next from mycursor into @ndoc



	while @@fetch_status = 0
	begin
		
		If OBJECT_ID('tempdb.dbo.#tempFT') IS NOT NULL
				DROP TABLE #tempFT
		If OBJECT_ID('tempdb.dbo.#tempFTResult') IS NOT NULL
				DROP TABLE #tempFTResult	
		If OBJECT_ID('tempdb.dbo.#missingNumbers') IS NOT NULL
			DROP TABLE #missingNumbers


		CREATE TABLE #missingNumbers (number int)
		CREATE TABLE #tempFT (
				nmdoc varchar(50),				
				site varchar(100),
				fno int,
				ftano int,
				ftstamp  varchar(100)
		
		)
		create nonclustered index idx1 on #missingNumbers (number)
	    create nonclustered index idxTemp1 on #tempFT (fno)

		declare @min int
		declare @max int
		declare @siteFinal varchar(20)

		insert into #tempFT
		select 
			ft.nmdoc,
			ft.site,
			ft.fno,
			ft.ftano,
			ft.ftstamp
		from 
			ft(nolock)		
		where 
			ndoc=@ndoc 
			and ftano=@ano 
			and site=case when @site = '' then ft.site else @site end  
		order by fno desc




		select top 1 @nmdoc=RTRIM(LTRIM(ISNULL(ft.nmdoc,''))), @siteFinal = ft.site from #tempFT  ft


		select 
			@min = min(fno),
			@max = max(fno)
		from 
			#tempFT(nolock)		
		

		--falha na numera��o
		while @min <= @max
		begin
		   if not exists (select * from #tempFT where fno = @min)
			  insert into #missingNumbers (number) values (@min)
		   set @min = @min + 1
		end

		print 'depois'
		insert into #finalResult(ndoc, nmdoc, ano,numero,loja,razao,fdata,fhora, ftstamp)
		select @ndoc,@nmdoc,@ano,#missingNumbers.number, @siteFinal,'Numera��o furada','1900-01-01','00:00:00','' from #missingNumbers
	

		--cabe�alhos sem linhas
		insert into #finalResult(ndoc, nmdoc, ano,numero,loja,razao,fdata,fhora, ftstamp)
		select 
			ndoc, nmdoc, ft.ftano, ft.fno, ft.site, 'Cabe�alho sem Linhas', ft.fdata,ft.ousrhora, ft.ftstamp
		from ft(nolock)
		where ndoc=@ndoc and ftano=@ano  
		and ftstamp not in (select fi.ftstamp from fi(nolock) where fi.ftstamp = ft.ftstamp )
		

		If OBJECT_ID('tempdb.dbo.#tempFTResult') IS NOT NULL
				DROP TABLE #tempFTResult

		If OBJECT_ID('tempdb.dbo.#tempFT') IS NOT NULL
				DROP TABLE #tempFT

		If OBJECT_ID('tempdb.dbo.#missingNumbers') IS NOT NULL
			DROP TABLE #missingNumbers

		fetch next from mycursor into @ndoc
	end
	

	IF CURSOR_STATUS('global','myCursor')>=-1
	BEGIN
	 close mycursor
	 deallocate  mycursor
	END
	

	select     
    '<b>'+ nmdoc + ' ('+convert(varchar(10),ndoc)+') ' + '</b> : n� ' + convert(varchar(10),numero) + ' de ' + convert(varchar(10),ano) + ' da farmacia: ' + #finalResult.loja as documento ,
    '<b>Razao:</b><u> ' + razao + ' </u> ' razao ,
    '<b>ftstamp: </b> ' + (CASE WHEN ISNULL(ftstamp,'') = '' THEN 'NAO TEM ' else ftstamp end ) ftstamp, 
    '<b>Data:</b> ' + CONVERT(VARCHAR(30),fdata , 121)   + ' : </b>' 	
	as data

    from #finalResult


	If OBJECT_ID('tempdb.dbo.#missingNumbers') IS NOT NULL
			DROP TABLE #missingNumbers

GO
Grant Execute On dbo.up_facturacao_validaEstadoDocumentos to Public
Grant Control On dbo.up_facturacao_validaEstadoDocumentos to Public
Go


