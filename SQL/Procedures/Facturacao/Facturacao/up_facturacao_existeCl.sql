/* Verificar se existe cliente pelo Nome

exec up_facturacao_existeCl 'Filipa Silva Resende        ' 

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_facturacao_existeCl]') IS NOT NULL
	drop procedure dbo.up_facturacao_existeCl
go

create procedure dbo.up_facturacao_existeCl
@nome	varchar(80),
@no varchar(10) = '',
@estab varchar(10) = ''

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON	
	Select 	
		nome
		,no
		,estab
		,desconto
		,nome2
		,morada
		,local
		,codpost
		,ncont
		,telefone
		,zona
		,tipo
		,bino
		,moeda
		,codigop
		,ccusto
		,email
		,nocredit
		,modofact
		,utstamp
		,pais
		,isnull(fidel.valcartao,0) as valcartao
		,b_utentes.cativa
		,b_utentes.perccativa
		,u_dcutavi = convert(datetime,'19000101')
		,b_utentes.nbenef
		,b_utentes.sexo
		,b_utentes.tlmvl
	From
		b_utentes (nolock)
		left join B_fidel fidel (nolock) on b_utentes.utstamp=fidel.clstamp
	Where
		Rtrim(Ltrim(NOME)) = @nome
		and removido=0
		and LTRIM(b_utentes.no) = (CASE WHEN @no <> '' THEN @no ELSE LTRIM(b_utentes.no) END)
		and LTRIM(b_utentes.estab) = (CASE WHEN @estab <> '' THEN @estab ELSE LTRIM(b_utentes.estab) END)
GO
Grant Execute On dbo.up_facturacao_existeCl to Public
Grant Control On dbo.up_facturacao_existeCl to Public
Go