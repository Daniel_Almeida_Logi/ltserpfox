/*  
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_etiq_ele_atualizar_data_ult_env]') IS NOT NULL
	drop procedure dbo.up_etiq_ele_atualizar_data_ult_env
go

-- Devolve todos os artigos ativos ativos cuja data de alteração é superior á do último envio
create procedure dbo.up_etiq_ele_atualizar_data_ult_env
@loja	varchar(30)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON 
begin try
	update 
		B_Parameters_site
	set
		textValue = convert(varchar, getdate(),112)
	where 
		stamp = 'ADM0000000200'
		and site = @loja

	select 1 as result
end try
begin catch
	select 0 as result
end catch

GO
	Grant Execute on dbo.up_etiq_ele_atualizar_data_ult_env to Public
	Grant Control on dbo.up_etiq_ele_atualizar_data_ult_env to Public
GO




