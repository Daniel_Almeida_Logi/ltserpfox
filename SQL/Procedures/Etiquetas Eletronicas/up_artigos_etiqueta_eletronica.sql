/* 
	exec up_artigos_etiqueta_eletronica 'Loja 1'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_artigos_etiqueta_eletronica]') IS NOT NULL
	drop procedure dbo.up_artigos_etiqueta_eletronica
go

-- Devolve todos os artigos ativos ativos cuja data de alteração é superior á do último envio
create procedure dbo.up_artigos_etiqueta_eletronica
@loja	varchar(30)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON 
	declare @no varchar(35) = (select no from empresa where site = @loja)
	declare @data varchar(35) = (select textvalue from b_parameters_site (nolock) where stamp = 'ADM0000000200' and site = @loja)

	DECLARE @SQL varchar(8000)
	set @SQL = 'select 
		st.design
		, st.ref
		, st.tabiva
		, convert(int,taxasiva.taxa) as  taxa
		, CAST(st.epv1 AS NUMERIC(15,2)) epv1
		, st.stock
		,ISNULL( STUFF((SELECT DISTINCT '','' + Ltrim(Rtrim(bc.codigo))
				  FROM bc 
				    where ref=st.ref and bc.site_nr = '''+@no+'''
				  FOR XML PATH('''')), 1, 1, ''''),'''')  as refs

		,CAST(st.epv1_final AS NUMERIC(15,2)) epv1Final
		, CASE WHEN CAST(st.mfornec AS NUMERIC(15,2)) = 0 
				THEN '''' ELSE convert(varchar(254),CAST(st.mfornec AS NUMERIC(15,0))) end AS descP
		, CASE WHEN CAST(st.mfornec2 AS NUMERIC(15,2)) = 0 
				THEN '''' ELSE convert(varchar(254),CAST(st.mfornec2 AS NUMERIC(15,2))) end AS descV
		, CASE WHEN  getdate() between dataIniPromo and dataFimPromo  AND (st.mfornec >0 OR st.mfornec2 >0) 
			THEN ''true'' ELSE ''false'' END inPromotion
		, REPLACE(CONVERT(date,dataIniPromo),''-'',''/'') AS dataIniPromo
		, REPLACE(CONVERT(date,dataFimPromo),''-'',''/'') AS dataFimPromo
	from
		st (nolock)
	inner join
		taxasiva (nolock) on st.tabiva = taxasiva.codigo
	left join
		fprod (nolock) on fprod.ref = st.ref
	where
		st.inactivo = 0
		and st.site_nr = '''+@no+'''
		and st.epv1 > 0
		and st.stns = 0 '
	print @sql

	exec(@sql)

GO
	Grant Execute on dbo.up_artigos_etiqueta_eletronica to Public
	Grant Control on dbo.up_artigos_etiqueta_eletronica to Public
GO




