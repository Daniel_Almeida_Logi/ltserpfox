/*
	FUNCAO:	Valida��es � Base de dados Logitools
	SP:		up_validacoes_Logitools
	Data:	2014-09-03
	Valida��es Implementadas:

	Valida��es sem Restri��o de Data
		. Validar se existem documentos sem u_tipodoc
		. Artigos com nome, marca, fornecedor com plicas
		. Fornecedores com nome, morada, localidade, observa��es, codpostal com plicas
		. Clientes com nome, morada, localidade, observa��es, codpostal com plicas
		. Dicionario com marca, design com plicas 
		. Artigos com Tabela de IVA Errado ou Iva n�o Inclu�do 
		. Artigos com PVP <= 0 (exclui os produtos com descri��o '%vale%' e '%desconto%' e ref='R000001')
		. Validar stocks e reservas por armaz�m
		. Artigos com PVP != autorizados
		. Encomendas abertas com mais de 15 dias
		. PagCentral sem Venda/Atendimento
		. Caixas abertas + de 3 dias
		. Validar moeda
		. Validar Psicotr�picos/Benzodiazepinas
		. C�digos do Cart�o de cliente Errados

	Valida��es com Restri��o de Data
		. Docs de Factura��o sem linhas
		. Linhas de Factura��o sem cabe�alhos
		. IVAs a zero (0) nas linhas de Vendas
		. Totais de Documentos diferente do total das linhas
		. Vendas sem Vendedor
		. Produtos nas Vendas com C�digos Alternativos
		. Corrigir rdata na BI
		. SPs nao encriptadas	

	Limpezas implementadas:
		. limpar mensagens automaticas com mais do que 6 meses
		. limpar mensagens do rob�


	-- exec up_validacoes_Logitools 1, 0, '19000101', '20150101'
	-- delete b_validacoes

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_validacoes_Logitools]') IS NOT NULL
	drop procedure dbo.up_validacoes_Logitools
go

create procedure dbo.up_validacoes_Logitools

@regista bit,
@corrige bit,
@dataini datetime,
@datafim datetime
	
/* WITH ENCRYPTION */
AS

BEGIN
	Begin Try

		-- se as datas estiverem vazias define valores por defeito (dia anterior)
		if @dataini =''
			set @dataini = getdate()-1

		if @datafim =''
			set @datafim = getdate()-1

		-- cria tabela tempor�ria para correc��es
		declare @stamps table (stamp varchar(30))

		SET NOCOUNT ON

		print 'Valida��es entre ' + convert(varchar,@dataini) + ' e ' + convert(varchar,@dataini)

		/*
		* Inicio das valida��es de Inconsist�ncias
		*
		* Valida��es Sem Restri��o de Data
		*/

		-- Validar se existem documentos sem u_tipodoc
		print 'Validar se existem documentos sem u_tipodoc'

		insert into B_validacoes (stamp, linkstamp, tipo, grau, descr, valor)
			select 
				left(newid(),20), tdstamp, 'Documentos de Venda', 1, 'Tipo de Documento Adicional N�o Configurado',
				 '['+convert(varchar,nmdoc)+'] ['+convert(varchar,ndoc)+']'
			from 
				td (nolock)
			where
				u_tipodoc=0
		
		
		-- Artigos com nome, marca, fornecedor com plicas
		print 'Artigos com nome, marca, fornecedor com plicas'
		
		insert into B_validacoes (stamp, linkstamp, tipo, grau, descr, valor, obs)
		output inserted.linkStamp into @stamps(stamp)
		select
			left(newid(),20), ststamp, 'Stocks', 1, 'Plicas', 
			'['+rtrim(ref)+'] ['+rtrim(design)+'] ['+rtrim(usr1)+'] ['+rtrim(fornecedor)+']',
			'Usar Query de Replace'
		from
			st (nolock)
		where
			design like '%'+CHAR(39)+'%' or usr1 like '%'+CHAR(39)+'%' or fornecedor like  '%'+CHAR(39)+'%'
		
		if @corrige=1
		begin
			update
				st
			set
				design		= REPLACE(design,char(39),'�')
				,usr1		= REPLACE(usr1,char(39),'�')
				,fornecedor	= REPLACE(fornecedor,char(39),'�')
			from
				st
				inner join @stamps a on st.ststamp=a.stamp
		end

		delete @stamps
		
		
		-- Fornecedores com nome, morada, localidade, observa��es, codpostal com plicas
		print 'Fornecedores com nome, morada, localidade, observa��es, codpostal com plicas'
		
		insert into B_validacoes (stamp, linkstamp, tipo, grau, descr, valor, obs)
		output inserted.linkStamp into @stamps(stamp)
		select
			left(newid(),20), flstamp, 'Fornecedores', 1, 'Plicas', 
			'['+convert(varchar,No)+'] ['+convert(varchar,estab)+'] ['+Nome+']',
			'Usar Query de Replace'
		from
			fl (nolock)
		where
			nome like '%'+CHAR(39)+'%' or morada like '%'+CHAR(39)+'%' or local like  '%'+CHAR(39)+'%' or codpost like  '%'+CHAR(39)+'%' or obs like '%'+CHAR(39)+'%'
		
		if @corrige=1
		begin
			update 
				fl
			set 
				nome		= REPLACE(nome,char(39),'�')
				,morada		= REPLACE(morada,char(39),'�')
				,local		= REPLACE(local,char(39),'�')
				,codpost	= REPLACE(codpost,char(39),'�')
				,obs		= REPLACE(obs,char(39),'�')
			from
				fl
				inner join @stamps a on fl.flstamp=a.stamp
		end

		delete @stamps



		-- Clientes com nome, morada, localidade, observa��es, codpostal com plicas
		print 'Clientes com nome, morada, localidade, observa��es, codpostal com plicas'

		insert into B_validacoes (stamp, linkstamp, tipo, grau, descr, valor, obs)
		output inserted.linkStamp into @stamps(stamp)
			select 
				left(newid(),20), utstamp, 'Fornecedores', 1, 'Plicas', 
				'['+convert(varchar,No)+'] ['+convert(varchar,estab)+'] ['+Nome+']',
				'Usar Query de Replace'
			from
				b_utentes (nolock)
			where
				nome like '%'+CHAR(39)+'%' or morada like '%'+CHAR(39)+'%' or local like  '%'+CHAR(39)+'%' or codpost like  '%'+CHAR(39)+'%' or obs like '%'+CHAR(39)+'%'

		if @corrige=1
		begin
			update b_utentes
			set nome		= REPLACE(nome,char(39),'�')
				,morada		= REPLACE(morada,char(39),'�')
				,local		= REPLACE(local,char(39),'�')
				,codpost	= REPLACE(codpost,char(39),'�')
				,obs		= REPLACE(convert(varchar,obs),char(39),'�')
			from
				b_utentes
				inner join @stamps a on b_utentes.utstamp=a.stamp
		end

		delete @stamps
		
		

		-- Dicionario com marca, design com plicas
		print 'Dicionario com marca, design com plicas'

		insert into B_validacoes (stamp, linkstamp, tipo, grau, descr, valor, obs)
		output inserted.linkStamp into @stamps(stamp)
			select
				left(newid(),20), fprodstamp, 'Fornecedores', 1, 'Plicas', 
				'['+ref+'] ['+design+']',
				'Usar Query de Replace'
			from
				fprod (nolock)
			where
				u_marca like '%'+CHAR(39)+'%' or design like '%'+CHAR(39)+'%'

		if @corrige=1
		begin
			update
				fprod
			set
				u_marca		= REPLACE(u_marca,char(39),' ')
				,design		= REPLACE(design,char(39),' ')
			from
				fprod
				inner join @stamps a on fprod.fprodstamp=a.stamp
		end
		delete @stamps
		


		-- Artigos com Tabela de IVA Errado, PVP a zero ou Iva n�o Inclu�do
		print 'Artigos com Tabela de IVA Errado, PVP a zero ou Iva n�o Inclu�do'

		insert into B_validacoes (stamp, linkstamp, tipo, grau, descr, valor, obs)
		output inserted.linkStamp into @stamps(stamp)
			select
				left(newid(),20), ststamp, 'Stocks', 2, 'TabIva Errado ou Iva N�o Inclu�do',
				'['+ltrim(rtrim(ref))+'] ['+ltrim(rtrim(design))+'] [tabiva:'+convert(varchar,tabiva)+'] [Pvp:'+CONVERT(varchar,round(epv1,2))+']',
				'Enviar listagem � farm�cia'
			from
				st (nolock)
			where
				tabiva not in (1,2,3,4) or iva1incl=0

		if @corrige=1
		begin
			update
				st
			set
				ivaincl	= 1
			from
				st
				inner join @stamps a on st.ststamp=a.stamp	where st.ivaincl=0
		end
		delete @stamps		



		-- Artigos com PVP <= 0
		print 'Artigos com PVP <= 0'

		insert into B_validacoes (stamp, linkstamp, tipo, grau, descr, valor, obs)
			select
				left(newid(),20), ststamp, 'Stocks', 2, 'PVP < Zero',
				'['+ltrim(rtrim(ref))+'] ['+ltrim(rtrim(design))+'] [Pvp:'+CONVERT(varchar,round(epv1,2))+']',
				'Enviar listagem � farm�cia'
			from
				st (nolock)
			where
				epv1<=0 and st.qlook=0 and ref not in ('R000001')


		-- Validar stocks e reservas por armaz�m
		print 'Stocks errados, tabelas produto e/ou armaz�m'

		DECLARE @cteArmazens TABLE (armazem numeric(5));
		insert into	@cteArmazens
		select distinct armazem from sl

		DECLARE @cteReservasClientes TABLE (ref varchar(18), armazem numeric(5), qtt numeric(9));
		insert into	@cteReservasClientes
		select bi.ref, bi.armazem, qtt = bi.qtt-bi.qtt2 from ts inner join bi on bi.ndos = ts.ndos where ts.rescli = 1 and bi.fechada=0

		DECLARE @cteReservasFornecedores TABLE (ref varchar(18), armazem numeric(5), qtt numeric(9));
		insert into	@cteReservasFornecedores
		select bi.ref, bi.armazem,qtt = bi.qtt-bi.qtt2 from ts inner join bi on bi.ndos = ts.ndos where ts.resfor = 1 and bi.fechada=0

		insert into B_validacoes (stamp, linkstamp, tipo, grau, descr, valor, obs)
			select
				left(newid(),20), st.ref, 'Stocks', 2, 'Stocks Errados',
				'['+ltrim(rtrim(ref))+'] [Stock Tot:'+CONVERT(varchar,round(stock,2))+']',
				'Correr fun��o de correc��o'
			from
				st (nolock)
			where
				st.stock != isnull((select SUM(stock) from sa (nolock) where ref=st.ref),0)
				or
				st.stock != isnull((select SUM(case when cm <50 then qtt else -qtt end) from sl (nolock) where sl.ref = st.ref),0)

			union all

			select
				left(newid(),20), sa.ref, 'Stocks', 2, 'Reservas Erradass',
				'['+ltrim(rtrim(sa.ref))+'] [Reservas Cli:'+CONVERT(varchar,sa.rescli)+'] [Reservas Forn:'+CONVERT(varchar,sa.resfor)+']',
				'Correr fun��o de correc��o'
			from
				sa (nolock)
				,@cteArmazens cteArmazens
			where
				sa.rescli != isnull((select SUM(qtt) from @cteReservasClientes cteRC where ref = sa.ref and cteArmazens.armazem = cteRC.armazem),0)
				or
				sa.resfor != isnull((select SUM(qtt) from @cteReservasFornecedores cteRF where ref = sa.ref and cteArmazens.armazem = cteRF.armazem),0)


		-- Artigos com PVP != PVP autorizados
		print 'Artigos com PVP != autorizados'

		insert into B_validacoes (stamp, linkstamp, tipo, grau, descr, valor, obs)
			select
				left(newid(),20), ststamp, 'Stocks', 2, 'PVP != PVP Autorizado',
				'['+ltrim(rtrim(st.ref))+'] [Pvp:'+CONVERT(varchar,epv1)+']',
				'Enviar listagem � farm�cia'
			from
				st (nolock)
				inner join fprod (nolock) on fprod.cnp=st.ref
			where
				epv1 not in (select preco
							from hist_precos (nolock)
							where ref=st.ref
								and data > dateadd(dd,-60,CONVERT(date,getdate())) 
								and (data_fim='19000101' or data_fim > convert(date,GETDATE()))
								)
		
		
		-- Encomendas abertas com mais de 15 dias
		print 'Encomendas abertas com mais de 15 dias'
		
		insert into B_validacoes (stamp, linkstamp, tipo, grau, descr, valor, obs)
		select 
			left(newid(),20), bo.bostamp, 'Encomendas', 2, 'Encomendas Abertas > 15 dias',
			'[Ano: '+convert(varchar,bo.boano)+'] [Doc: '+CONVERT(varchar,bo.obrano)+'] [Forn: '+rtrim(bo.nome)+']',
			'Enviar listagem � farm�cia'
		from 
			bo
			inner join bi on bo.bostamp=bi.bostamp
		where
			bo.ndos=2 and bo.obrano < CONVERT(varchar,getdate()-15,112)
			and (bo.fechada=0 or bi.fechada=0)


		-- PagCentral sem Venda/Atendimento
		print 'PagCentral sem Venda/Atendimento'
		
		insert into B_validacoes (stamp, linkstamp, tipo, grau, descr, valor, obs)
		select
			left(newid(),20), pc.nratend, 'Vendas', 2, 'PagCentral sem Venda/Atendimento',
			'[Atend: '+rtrim(pc.nratend)+']',
			'Eliminar pagamento centralizado'
		from
			b_pagcentral pc (nolock)
			left join ft (nolock) on pc.nratend=ft.u_nratend
		where
			ft.ftstamp is null


		-- Caixas abertas + de 3 dias
		print 'Caixas abertas + de 3 dias'

		insert into B_validacoes (stamp, linkstamp, tipo, grau, descr, valor, obs)
		select
			left(newid(),20), cx.cxstamp, 'Caixas', 2, 'Caixas abertas + 3 dias',
			'[' + convert(varchar,cx.dabrir,102) + '] [' + rtrim(cx.pnome) + '] [' + rtrim(cx.ausername) + ']',
			'Enviar listagem � farm�cia'
		from
			cx (nolock)
		where
			fechada=0 and dabrir < DATEADD(dd,-2,getdate())
		order by 
			cx.dabrir 


		-- Validar moeda
		print 'Validar moeda'

		insert into B_validacoes (stamp, linkstamp, tipo, grau, descr, valor, obs)
		select
			left(newid(),20), b_utentes.utstamp, 'Clientes', 2, 'Moeda Incorreta',
			'[' + convert(varchar,b_utentes.no) + '] [' + convert(varchar,b_utentes.estab) + '] [Moeda: ' + rtrim(b_utentes.moeda) + ']',
			'Alterar moeda'
		from
			b_utentes (nolock)
		where
			b_utentes.moeda != 'EURO'
		union all
		select
			left(newid(),20), fl.flstamp, 'Fornecedores', 2, 'Moeda Incorreta',
			'[' + convert(varchar,fl.no) + '] [' + convert(varchar,fl.estab) + '] [Moeda: ' + rtrim(fl.moeda) + ']',
			'Alterar moeda'
		from
			fl (nolock)
		where
			fl.moeda != 'EURO'
		union all
		select
			left(newid(),20), fo.fostamp, 'Compras', 2, 'Moeda Incorreta',
			'[Ano: ' + convert(varchar,fo.foano) + '] [Doc: ' + convert(varchar,fo.adoc) + '] [Moeda: ' + rtrim(fo.moeda) + ']',
			'Alterar moeda'
		from
			fo (nolock)
		where
			fo.moeda != 'EURO'
		union all
		select
			left(newid(),20), ft.ftstamp, 'Vendas', 2, 'Moeda Incorreta',
			'[Ano: ' + convert(varchar,ft.ftano) + '] [Doc: ' + convert(varchar,ft.fno) + '] [Moeda: ' + rtrim(ft.moeda) + ']',
			'Alterar moeda'
		from
			ft (nolock)
		where
			ft.moeda != 'EURO'
		union all
		select
			left(newid(),20), ccstamp, 'Clientes C/C', 2, 'Moeda Incorreta',
			'[Doc: ' + cc.cmdesc + '] [Nr: ' + convert(varchar,cc.nrdoc) + '] [Moeda: ' + rtrim(cc.moeda) + ']',
			'Alterar moeda'
		from
			cc (nolock)
		where
			cc.moeda != 'EURO'
		union all
		select
			left(newid(),20), fcstamp, 'Fornecedores C/C', 2, 'Moeda Incorreta',
			'[Doc: ' + cmdesc + '] [Nr: ' + adoc + '] [Moeda: ' + rtrim(moeda) + ']',
			'Alterar moeda'
		from
			fc (nolock)
		where
			moeda != 'EURO'


		-- Validar Psicotr�picos/Benzodiazepinas
		print 'Validar Psicotr�picos/Benzodiazepinas'
		
		insert into B_validacoes (stamp, linkstamp, tipo, grau, descr, valor, obs)
		select
			left(newid(),20), fi.fistamp, 'Psico/Benzo', 2, 'Contadores Incorretos',
			'[Doc: ' + fi.nmdoc + '] [Ref: ' + fi.ref + '] '
			+ case 
				when fi.u_psico=1 then '[PsiCont: '+convert(varchar,fi.u_psicont)+']' 
				when fi.u_benzo=1 then '[BenCont: '+convert(varchar,fi.u_bencont)+']' 
				else '[N�o Psico/Benzo]' 
				end,
			'N�o � suposto existir contador neste tipo de documento'
		from
			fi (nolock) 
			inner join td (nolock) on td.ndoc=fi.ndoc
		where 
			(td.u_tipodoc=4 or td.tipodoc=4) 
			and (u_psicont>0 or u_bencont>0)
		
		
		-- C�digos do Cart�o de cliente Errados
		print 'C�digos do Cart�o de cliente Errados'
		
		insert into B_validacoes (stamp, linkstamp, tipo, grau, descr, valor, obs)
		select
			left(newid(),20), b_utentes.utstamp, 'Clientes', 2, 'C�digos Cart�o Cliente Errados',
			'[' + convert(varchar,b_utentes.no) + '] [' + convert(varchar,b_utentes.estab) + ']',
			'Corrigir C�digo Cart�o. Aten��o �s Depend�ncias.'
		from
			b_utentes (nolock)
		where
			nrcartao != ''
			and (left(nrcartao,2) != 'CL' or LEN(nrcartao) != 7)
		

		
		/*
		*
		* Valida��es Com Restri��o de Data
		*
		*/

		-- Docs de Factura��o sem linhas ou Linhas sem cabe�alhos (por data)
		print 'Docs de Factura��o sem linhas ou Linhas sem cabe�alhos (por data)'
		
		insert into B_validacoes (stamp, linkstamp, tipo, grau, descr, valor, odate)
			select
				left(newid(),28), ftstamp, 'Documentos de Venda', 1, 'Cabe�alhos Sem Linhas',
				'['+convert(varchar,nmdoc)+'] ['+convert(varchar,fno)+'] ['+CONVERT(varchar,ft.ftano)+']',
				ft.fdata
			from
				ft (nolock)
			where
				ftstamp not in (select fi.ftstamp from fi (nolock) where fi.ftstamp=ft.ftstamp)
				and ft.fdata between @dataini and @datafim
				
			union all
			
			select
				left(newid(),28), ftstamp, 'Documentos de Venda', 1, 'Linhas Sem Cabe�alhos',
				'['+convert(varchar,nmdoc)+'] ['+convert(varchar,fno)+']',
				fi.rdata
			from
				fi (nolock)
			where
				ftstamp not in (select ft.ftstamp from ft (nolock) where fi.ftstamp=ft.ftstamp)
				and fi.rdata between @dataini and @datafim


		-- IVAs a zero (0) nas linhas de Vendas (por data)
		print 'IVAs a zero (0) nas linhas de Vendas (por data)'
		
		insert into B_validacoes (stamp, linkstamp, tipo, grau, descr, valor, odate)
			select
				left(newid(),28), ft.ftstamp, 'Documentos de Venda', 1, 'Produtos sem IVA',
				'['+convert(varchar,ft.nmdoc)+'] ['+convert(varchar,ft.fno)+'] ['+CONVERT(varchar,ft.ftano)+']',
				ft.fdata
			from
				ft (nolock)
				inner join fi (nolock) on ft.ftstamp=fi.ftstamp
			where
				fi.tabiva=0 and (ft.fdata between @dataini and @datafim) and fi.ref!=''


		-- Totais de Documentos diferente do total das linhas (por data)
		print 'Totais de Documentos diferente do total das linhas (por data)'
		
		insert into B_validacoes (stamp, linkstamp, tipo, grau, descr, valor, odate)
			select
				left(newid(),28), ftstamp, 'Documentos de Venda', 1, 'Totais Cabe�alho <> Somat�rio Linhas',
				'['+convert(varchar,ft.nmdoc)+'] ['+convert(varchar,ft.fno)+'] ['+CONVERT(varchar,ft.ftano)+']',
				ft.fdata
			from
				ft (nolock)
			where
				round(ft.etotal,2) != round(isnull((select sum(total) from 
											(select case when fi.ivaincl=1 then fi.etiliquido else fi.etiliquido+fi.etiliquido*(fi.iva/100) end as total from fi (nolock) where fi.ftstamp=ft.ftstamp)
											as x ),0)
										,2)
				and ft.fdata between @dataini and @datafim
		
		
		
		-- Vendas sem Vendedor (por data)
		print 'Vendas sem Vendedor (por data)'
		
		insert into B_validacoes (stamp, linkstamp, tipo, grau, descr, valor, obs)
			select
				left(newid(),28), ftstamp, 'Documentos de Venda', 2, 'Documento sem Vendedor',
					'['+convert(varchar,ft.nmdoc)+'] ['+convert(varchar,ft.fno)+'] ['+CONVERT(varchar,ft.ftano)+']',
					''
			from
				ft (nolock)
			where
				vendedor=0
				and nmdoc not like '%importa%'
				and ft.fdata between @dataini and @datafim


		-- Produtos nas Vendas com C�digos Alternativos
		print 'Produtos nas Vendas com C�digos Alternativos'
		
		insert into B_validacoes (stamp, linkstamp, tipo, grau, descr, valor, odate, obs)
			select left(newid(),28), ftstamp, 'Documentos de Venda', 2, 'Produtos com C�digos Alternativos',
					'['+convert(varchar,fi.nmdoc)+'] ['+convert(varchar,fi.fno)+'] ['+CONVERT(varchar,YEAR(fi.rdata))+'] [ref:'+fi.ref+']',
					fi.rdata,
					'update fi 
					set ref = isnull((select ref from st (nolock) where st.ref = isnull((select top 1 ref from bc (nolock) where bc.ref=st.ref and bc.codigo=fi.ref), ISNULL((select ref from st (nolock) where codigo=fi.ref),fi.ref))), fi.ref)
					where fi.ref not in (select ref from st where ref=fi.ref) and fi.ref!='+char(39)+char(39)
			from
				fi (nolock)
			where
				fi.ref not in (select ref from st (nolock) where st.ref=fi.ref) and fi.ref!='' and (fi.rdata between @dataini and @datafim)
			
		
		-- Corrigir rdata na Bi
		print 'Corrigir rdata na Bi'	
		
		insert into B_validacoes (stamp, linkstamp, tipo, grau, descr, valor, odate, obs)
			output inserted.linkStamp into @stamps(stamp)
			select
				left(newid(),28), bo.bostamp, 'Dossiers internos', 2,bo.nmdos,'Data das linhas <> data cabe�alho',bo.dataobra,''
			from
				bi (nolock)
				inner join bo (nolock) on bi.bostamp=bo.bostamp
			where
				convert(varchar,bi.rdata,102)!=convert(varchar,bo.dataobra,102) and bo.dataobra between @dataini and @datafim
			
		if @corrige=1
		begin
			update bi
			set bi.rdata = bo.dataobra
			from bi 
				inner join bo on bo.bostamp = BI.bostamp 
				inner join @stamps a on a.stamp=bi.bostamp	
		end
		
		delete @stamps	
					
	
		-- SPs nao encriptadas
		print 'SPs nao encriptadas'
		insert into B_validacoes (stamp, linkstamp, tipo, grau, descr, valor, obs)
			select
				left(newid(),28), '', 'SPs', 3, 'N�o Encriptadas',
				p.name,
				'Correr novamente o c�digo fonte da procedure'
			from
				sys.sql_modules as m
				inner join sys.procedures as p on m.object_id=p.object_id
			where
				LEFT(p.name,3)='up_' and m.definition is not null	
					
		/*
		*
		* Fim das valida��es de inconsist�ncias
		*
		*/

		
		/*
		* Visualizar resultados
		*/
		print 'Visualizar resultados'
		
		select 
			linkStamp, tipo, Grau, descr, Valor, odate, date, obs
		from
			B_validacoes (nolock)
		where
			convert(varchar,date,102)=convert(varchar,GETDATE(),102)
		order by
			tipo, grau, descr
		
		/*
		* Emitir listagem de produtos sem iva ou sem pvp para enviar listagem para a farmacia
		*/
		print 'Emitir listagem de produtos sem iva ou sem pvp para enviar listagem para a farmacia'
			
		if (select COUNT(grau) from B_validacoes (nolock) where obs='Enviar listagem � farm�cia') > 0
			select 
				Tipo, Grau, Descr, Valor
			from
				B_validacoes (nolock) 
			where
				obs='Enviar listagem � farm�cia'
		
		
		-- Verifica se regista valida��es
		print 'Verifica se regista valida��es'
		
		if isnull((SELECT bool FROM B_Parameters WHERE stamp = 'ADM0000000070'),0)=0 or @regista=0
			delete
				B_validacoes
			where
				convert(varchar,date,102)=convert(varchar,getdate(),102)


		/*
		* Inicio de limpeza
		*/

		-- limpar mensagens internas com mais do que 12 meses
		print 'limpar mensagens automaticas com mais do que 12 meses'

		delete
			mg
		where
			data < DATEADD(MONTH, -12, GETDATE())


		-- limpar mensagens do rob�
		print 'limpar mensagens do rob�'

		delete
			B_movtec_robot
		where
			oData < GETDATE()
		
		delete
			B_movtec_robot_log
		where
			data < DATEADD(MONTH, -1, GETDATE())

		-- Fim de Limpeza
		
	End try

	Begin Catch
		SELECT ERROR_NUMBER()  AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity, ERROR_STATE()  AS ErrorState,
			ERROR_PROCEDURE() AS ErrorProcedure, ERROR_LINE()  AS ErrorLine, ERROR_MESSAGE() AS ErrorMessage;

		insert into B_elog (tipo, status, mensagem, origem)
		values ('E', 'A', ERROR_MESSAGE(), 'up_validacoes_Logitools')

		return
	end catch
END


GO
Grant Execute On dbo.up_validacoes_Logitools to Public
Grant control On dbo.up_validacoes_Logitools to Public
GO
---------------------------------------------------------