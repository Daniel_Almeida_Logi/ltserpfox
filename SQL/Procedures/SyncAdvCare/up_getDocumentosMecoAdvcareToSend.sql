/*
Pesquisa documentos em falta para enviar da advance care
D.Almeida, 08-11-2025

Pesquisa a partir de 2 campos de pesquisa:
	datainicio ,datafim


Parametros: 
EXEC dbo.up_getDocumentosMecoAdvcareToSend '2022-10-01', '2022-10-31'

*/



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_getDocumentosMecoAdvcareToSend]') IS NOT NULL
    drop procedure up_getDocumentosMecoAdvcareToSend
go

CREATE PROCEDURE up_getDocumentosMecoAdvcareToSend
	@dataIni	date,
	@dataFim	date	

AS
SET NOCOUNT ON


         select 
			td.tiposaft+ ' '+ltrim(rtrim(str(ft.ndoc)))+rtrim(STR(ft.ftano))+'/'+ltrim(rtrim(str(fno))) as docdescr,
			ftstamp, 
			ft.ousrinis,
			ft2.stamporigproc,
			ft2.nrelegibilidade, 
			ft.fdata
         from ft
         inner join ft2(nolock) on ft2.ft2stamp = ft.ftstamp
         inner join td (nolock) on ft.ndoc=td.ndoc
         inner join b_utentes (nolock) on ft.no=b_utentes.no and ft.estab = b_utentes.estab
         where
          td.tiposaft='FT'
          and  b_utentes.no in (select u_no from cptorg(nolock) where efrID = 72)
          and ft2.nrelegibilidade !=''
          and ft.fdata >= @dataIni and ft.fdata <=@dataFim
          and stamporigproc not in (select ftc.ftstamp from ft_compart ftc (nolock) where ftc.type=4 and ftc.invoice=1)
          and stamporigproc not in (select ftc.ftstamp from ft_compart ftc (nolock) where ftc.type=3)
		  and ft2.nrelegibilidade not in (select ftc.contributionCorrelationId from ft_compart ftc (nolock) where contributionCorrelationId=ft2.nrelegibilidade  and ftc.type=4 )
		 order by 
			ft.fdata asc


GO
Grant Execute On up_getDocumentosMecoAdvcareToSend to Public
Grant Control On up_getDocumentosMecoAdvcareToSend to Public
GO


