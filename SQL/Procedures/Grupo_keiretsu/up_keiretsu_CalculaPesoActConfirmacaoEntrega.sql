-- exec up_keiretsu_CalculaPesoActConfirmacaoEntrega '5017074',''
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_keiretsu_CalculaPesoActConfirmacaoEntrega ]') IS NOT NULL
    drop procedure up_keiretsu_CalculaPesoActConfirmacaoEntrega 
go

create PROCEDURE up_keiretsu_CalculaPesoActConfirmacaoEntrega 

@ref varchar(25)
,@tipoUtente varchar(max)

/* WITH ENCRYPTION */
AS

	
	;with cteUtentes as (
		select 
			no
			,estab
		From	
			B_utentes (nolock)
		Where
			B_utentes.tipo in (select items from up_SplitToTable(@tipoUtente,','))
	), 
	cteEncomendasCliente as (
		Select 
			ref
			,obistamp
			,bistamp
			,bo.ndos
			,bo.nome
			,bo.no
			,bo.estab
			,bo.nmdos
			,bo.bostamp
			,qtt
			,qttrec = isnull((select SUM(a.qtt2) from bi a where a.obistamp = bi.bistamp),0)
			,qttfalta = (bi.qtt-isnull((select SUM(a.qtrec) from bi a where a.obistamp = bi.bistamp),0))
			,fnstamp = convert(varchar(25),'')
			,armazem
		from 
			bi (nolock)
			inner join bo (nolock) on bi.bostamp = bo.bostamp 
			inner join cteUtentes on bo.no = cteUtentes.no and bo.estab = cteUtentes.estab 
		where 
			bo.ndos = 41
			and bi.ref = @ref
			and (bi.qtt-isnull((select SUM(a.qtrec) from bi a where a.obistamp = bi.bistamp),0)) != 0 
			and bo.fechada = 0 
	)

	select 
		cteEncomendasCliente.*
		,bdname
		,odbc
		,sede
		,peso = qtt/(select SUM(qtt) from cteEncomendasCliente)
	From 
		cteEncomendasCliente 
		left join condComercConfig on cteEncomendasCliente.no = condComercConfig.no and cteEncomendasCliente.estab = condComercConfig.estab
	Order
		by qtt/(select SUM(qtt) from cteEncomendasCliente) desc

		
go
Grant Execute On up_keiretsu_CalculaPesoActConfirmacaoEntrega  to Public
Grant Control On up_keiretsu_CalculaPesoActConfirmacaoEntrega  to Public
go