-- exec up_keiretsu_ValidaConfEncODBC 'Grupo'
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_keiretsu_ValidaConfEncODBC ]') IS NOT NULL
    drop procedure up_keiretsu_ValidaConfEncODBC 
go

create PROCEDURE up_keiretsu_ValidaConfEncODBC 
@tipoUtente varchar(max)

/* WITH ENCRYPTION */
AS

		
	; with cteDados as (
			Select 
			bo.ndos
			,bo.no
			,bo.estab
			,bo.nmdos
		from 
			bo (nolock)
		where 
			bo.ndos = 43 
			and bo.fechada = 0
	),
	cteUtentes as (
		select 
			no
			,estab
		From	
			B_utentes (nolock)
		Where
			B_utentes.tipo in (select items from up_SplitToTable(@tipoUtente,','))
	)

	Select 
		*
	From 
		cteDados 
		inner join cteUtentes on cteUtentes.no = cteDados.no  and cteUtentes.estab = cteDados.estab
		left join condComercConfig on cteDados.no = condComercConfig.no  and cteDados.estab = condComercConfig.estab
	Where
		condComercConfig.odbc is null 
		
		
GO
Grant Execute On up_keiretsu_ValidaConfEncODBC  to Public
Grant Control On up_keiretsu_ValidaConfEncODBC  to Public
go