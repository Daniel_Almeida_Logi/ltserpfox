-- exec up_keiretsu_Stock_ActualizaStockAtual '',''
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_keiretsu_Stock_ActualizaStockAtual]') IS NOT NULL
    drop procedure up_keiretsu_Stock_ActualizaStockAtual 
go

create PROCEDURE up_keiretsu_Stock_ActualizaStockAtual
@bd_Armazem  as varchar(60)
,@bd_Cliente  as varchar(60)


/* WITH ENCRYPTION */
AS

/*@bd_Armazem  as varchar(60) = 'SRVSQL01.A01205A'*/
/*,@bd_Cliente  as varchar(60) = '[172.20.5.40].F10201A'*/


	declare @sql  as varchar(max)

	set @sql = N'
	;with cteDados as (
		select 
			bistamp
			,ref
		from 
			(
			Select	
				id = ROW_NUMBER() over (partition by bi.ref,bo.no,bo.estab order by bo.dataobra desc,bo.usrhora desc)
				,bi.bistamp
				,bi.ref
				,bi.u_stockact
			From
				' + @bd_Cliente + '.dbo.bo 
				inner join ' + @bd_Cliente + '.dbo.bi on bo.bostamp = bi.bostamp				
			where
				bo.ndos = 45
				
			) x
		where
			x.id = 1
	)

	/*
	select 
		cteDados.*,  st.stock, bi.u_stockact
	*/

	update ' + @bd_Cliente + '.dbo.bi
	set u_stockact = st.stock
	from 
		cteDados		
		inner join ' + @bd_Armazem + '.dbo.st on cteDados.ref = st.ref
		inner join ' + @bd_Cliente + '.dbo.bi on bi.bistamp = cteDados.bistamp
	'
	print @bd_Cliente + ' OK'
	exec(@sql)

GO
Grant Execute On up_keiretsu_Stock_ActualizaStockAtual  to Public
Grant Control On up_keiretsu_Stock_ActualizaStockAtual  to Public
go