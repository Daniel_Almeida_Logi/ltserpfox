/* 

	 exec up_keiretsu_Stock_Distribuir 3

*/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_keiretsu_Stock_Distribuir]') IS NOT NULL
    drop procedure up_keiretsu_Stock_Distribuir 
go

create PROCEDURE up_keiretsu_Stock_Distribuir
@armazem numeric(5,0)


/* WITH ENCRYPTION */
AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#StockConfirmacao'))
	BEGIN
		DROP TABLE #StockConfirmacao
	END
	
	/* Stock em confirma��o, mas n�o facturado */
	
	select 
		ref 
		,stock = SUM(qtrec-qtt2)
	into 
		#StockConfirmacao
	from 
		bi 
	where 
		ndos = 43 
		and fechada = 0 
		and bistamp not in (select bistamp from fi)
	group by
		ref
	having 
		SUM(qtt-qtt2) != 0


	Select 
		sl.ref
		,stock = sum(case when sl.cm<50 then sl.qtt else -sl.qtt end) 
		,stockConfirmacao = isnull(stConf.stock,0)
	from 
		sl (nolock)
		left join #StockConfirmacao stConf on stConf.ref = sl.ref
	where 
		armazem = @armazem
	group by 
		sl.ref
		,isnull(stConf.stock,0)
	having 
		sum(case when cm<50 then qtt else -qtt end)  > 0



	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#StockConfirmacao'))
	BEGIN
		DROP TABLE #StockConfirmacao
	END

GO
Grant Execute On up_keiretsu_Stock_Distribuir  to Public
Grant Control On up_keiretsu_Stock_Distribuir  to Public
go
