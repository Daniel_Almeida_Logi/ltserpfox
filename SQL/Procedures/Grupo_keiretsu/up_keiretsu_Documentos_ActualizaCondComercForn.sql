/*
	 exec up_keiretsu_Documentos_ActualizaCondComercForn '5323951 '
*/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_keiretsu_Documentos_ActualizaCondComercForn]') IS NOT NULL
    drop procedure up_keiretsu_Documentos_ActualizaCondComercForn 
go

create PROCEDURE up_keiretsu_Documentos_ActualizaCondComercForn
@ref  as varchar(18)


/* WITH ENCRYPTION */
AS

	select 
		top 1 pclk = b.binum2
	from 
		bo a
		inner join bi b on a.bostamp = b.bostamp	
	where 
		a.ndos = 45 
		and a.fechada = 0
		and b.ref = @ref
	order by 
		a.dataobra desc, a.ousrhora desc
GO
Grant Execute On up_keiretsu_Documentos_ActualizaCondComercForn  to Public
Grant Control On up_keiretsu_Documentos_ActualizaCondComercForn  to Public
go