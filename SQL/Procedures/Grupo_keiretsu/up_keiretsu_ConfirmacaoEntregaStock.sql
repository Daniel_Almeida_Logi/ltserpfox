/*
	 exec up_keiretsu_ConfirmacaoEntregaStock '',1,'ToLife - Produtos Farmac�uticos, S.A.','', 1

*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_keiretsu_ConfirmacaoEntregaStock]') IS NOT NULL
    drop procedure up_keiretsu_ConfirmacaoEntregaStock 
go

create PROCEDURE up_keiretsu_ConfirmacaoEntregaStock
@tipoUtente			varchar(max),
@armazem				numeric(5,0),
@lab						varchar(max),
@nome					varchar(55),
@site_nr					tinyint

/* WITH ENCRYPTION */
AS

	/* Elimina Tabelas Temporarias */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#Utentes'))
	BEGIN
		DROP TABLE #Utentes
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#StockArmazem'))
	BEGIN
		DROP TABLE #StockArmazem
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#StockConfirmacao'))
	BEGIN
		DROP TABLE #StockConfirmacao
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#EncomendasCliente'))
	BEGIN
		DROP TABLE #EncomendasCliente
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#ConfirmacoesEntregaCliente'))
	BEGIN
		DROP TABLE #ConfirmacoesEntregaCliente
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#StLab'))
	BEGIN
		DROP TABLE #StLab
	END

	/* Laboratorios */
	Select 
		ref
	into 
		#StLab	
	From
		st
	Where
		(st.u_lab = @lab OR @lab = '')
		and site_nr = @site_nr

	/* Utentes */	
	select 
		no
		,estab
	into 
		#Utentes
	From	
		B_utentes (nolock)
	Where
		(B_utentes.tipo in (select items from up_SplitToTable(@tipoUtente,',')) OR @tipoUtente = '')
		and (nome = @nome or @nome = '')
	
	/*
		Stock em confirma��o, mas n�o facturado
	*/
	select 
		ref 
		,stock = SUM(qtrec-qtt2)
	into 
		#StockConfirmacao
	from 
		bi 
	where 
		ndos = 43 and fechada = 0 
		and bistamp not in (select bistamp from fi)
	group by
		ref
	having 
		SUM(qtt-qtrec) != 0


	/*Stock Armazem*/
	Select 
		sl.ref
		,stock = sum(case when sl.cm<50 then sl.qtt else -sl.qtt end) 
		,stockConfirmacao = isnull(stConf.stock,0)
	into 
		#StockArmazem
	from 
		sl (nolock)
		left join #StockConfirmacao stConf on stConf.ref = sl.ref
	where 
		armazem = @armazem
	group by 
		sl.ref
		,isnull(stConf.stock,0)
	having 
		sum(case when cm<50 then qtt else -qtt end)  > 0
		
	/* Confirma��es de Entrega de Encomendas a Cliente */
	Select 
		bi.ref
		,bi.qtt2
		,bi.qtt
		,bi.obistamp
		,bi.qtrec
	into 
		#ConfirmacoesEntregaCliente
	From
		bi (nolock)
		inner join bo (nolock) on bi.bostamp = bo.bostamp 
	where
		bo.ndos = 43


	/* Encomendas de Cliente */
	Select 
		bi.ref
		,bi.design
		,obistamp
		,bistamp
		,bo.ndos
		,bo.nome
		,bo.no
		,bo.estab
		,bo.nmdos
		,bo.bostamp
		,bo.obrano
		,qtt
		,qttrec = isnull((select SUM(a.qtrec) from #ConfirmacoesEntregaCliente a where a.obistamp = bi.bistamp),0)
		,qttMov = isnull((select SUM(a.qtt2) from #ConfirmacoesEntregaCliente a where a.obistamp = bi.bistamp),0)
		,qttatribuida = 0
		,stock = isnull(sa.stock,0)
		,stockConfirmacao = isnull(sa.stockConfirmacao,0)
		,armazem = @armazem
	into
		#EncomendasCliente
	from 
		bi (nolock)
		inner join bo (nolock) on bi.bostamp = bo.bostamp 
		inner join #StLab on bi.ref = #StLab.ref
		inner join #Utentes cteUtentes on bo.no = cteUtentes.no and bo.estab = cteUtentes.estab
		left join #StockArmazem sa on bi.ref = sa.ref
	where 
		bo.ndos = 41
		and (bi.qtt-isnull((select SUM(a.qtrec) from bi a where a.obistamp = bi.bistamp),0)) != 0 
		and bo.fechada = 0 
		

	select 
		pesquisa = CONVERT(bit,0)
		,sel = CONVERT(bit,0)
		,cteEncomendasCliente.*
		,bdname
		,odbc
		,sede
		,ordem = ROW_NUMBER() over(partition by ref order by qtt/(select SUM(qtt) from #EncomendasCliente) desc)
	From 
		#EncomendasCliente cteEncomendasCliente 
		left join condComercConfig on cteEncomendasCliente.no = condComercConfig.no and cteEncomendasCliente.estab = condComercConfig.estab

	/* Elimina Tabelas Temporarias */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#Utentes'))
	BEGIN
		DROP TABLE #Utentes
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#StockArmazem'))
	BEGIN
		DROP TABLE #StockArmazem
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#StockConfirmacao'))
	BEGIN
		DROP TABLE #StockConfirmacao
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#EncomendasCliente'))
	BEGIN
		DROP TABLE #EncomendasCliente
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#ConfirmacoesEntregaCliente'))
	BEGIN
		DROP TABLE #ConfirmacoesEntregaCliente
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#StLab'))
	BEGIN
		DROP TABLE #StLab
	END

GO
Grant Execute On up_keiretsu_ConfirmacaoEntregaStock  to Public
Grant Control On up_keiretsu_ConfirmacaoEntregaStock  to Public
go