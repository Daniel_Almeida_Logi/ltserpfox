-- exec up_keiretsu_distribuicaoEncomendas '20120101','20150101',''
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_keiretsu_distribuicaoEncomendas]') IS NOT NULL
    drop procedure up_keiretsu_distribuicaoEncomendas 
go

create PROCEDURE up_keiretsu_distribuicaoEncomendas
@dataini datetime,
@datafim datetime,
@cliente varchar(55)


/* WITH ENCRYPTION */
AS

	

Select 
	bo.nome,bo.no, bi.ref, bi.design, qtt, stock = ISNULL(st.stock,0)
from
	bo
	inner join bi on bo.bostamp = bi.bostamp 
	left join st on st.ref = bi.ref
where 
	bo.nmdos = 'Encomenda de Cliente'
	and bo.dataobra between @dataini and @dataFim
	and bo.nome = case when @cliente = '' then bo.nome else @cliente end
	and bo.fechada = 0
Order by
	bo.nome,
	bi.design
		
		
GO
Grant Execute On up_keiretsu_distribuicaoEncomendas  to Public
Grant Control On up_keiretsu_distribuicaoEncomendas  to Public
go
