-- select ref,epcusto from bi where bostamp = 'ADMF742B7A5-D3AC-450E-A89'
-- exec up_keiretsu_OPL 'ADMF742B7A5-D3AC-450E-A89'
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_keiretsu_OPL]') IS NOT NULL
    drop procedure up_keiretsu_OPL 
go

create PROCEDURE up_keiretsu_OPL
@bostamp varchar(25)


/* WITH ENCRYPTION */
AS

	/*╔ gerado um OPL por cada conferencia*/
	;with cteConfEncomendaFornec as (
		select 
			cnp = bi.ref
			,design
			,laboratorio = bo.nome 
			,bistamp
			,qtt
			,qtt2
			,bo.no
			,bo.estab
			,bi.u_upc
			,bi.iva
			,bi.epcusto
		from 
			bi (nolock)
			inner join bo on bo.bostamp = bi.bostamp
		where 
			bo.ndos = 42
			and ref != ''
			and bo.bostamp = @bostamp
	), cteConfEncomendaCliente as (
		select 
			* 
		from 
			bi 
		where 
			ndos = 43
			and bi.fechada = 0
			and ref != ''
	), cteCondicoesComerciais as (
		select 
			ref
			,fee = bi.num1
			,descontok = bi.binum1
			,pclk = bi.binum2
			,bo.no
			,bo.estab
			,bo.dataobra
			,bo.obrano
			
		from 
			bi (nolock)
			inner join bo (nolock) on bi.bostamp = bo.bostamp
		where 
			bo.ndos = 40
			and ref != ''	
	)
	Select 
		cnp
		,cteConfEncomendaFornec.design
		,laboratorio
		,[k] = isnull((select top 1 pclk from cteCondicoesComerciais where cteCondicoesComerciais.ref = cteConfEncomendaFornec.cnp and cteConfEncomendaFornec.no = cteCondicoesComerciais.no and cteConfEncomendaFornec.estab = cteCondicoesComerciais.estab Order by dataobra desc, obrano desc),0) - cteConfEncomendaFornec.u_upc /*Diferenša entre PCLK e PCL*/
		,pclk = isnull((select top 1 pclk from cteCondicoesComerciais where cteCondicoesComerciais.ref = cteConfEncomendaFornec.cnp and cteConfEncomendaFornec.no = cteCondicoesComerciais.no and cteConfEncomendaFornec.estab = cteCondicoesComerciais.estab Order by dataobra desc, obrano desc),0)
		,pcl = cteConfEncomendaFornec.u_upc
		,PCT = cteConfEncomendaFornec.epcusto
		,cteConfEncomendaFornec.Iva
		,ficaStock = cteConfEncomendaFornec.qtt - isnull(cteConfEncomendaCliente.qtt2,0)
		,encomendasFarmacias = isnull(cteConfEncomendaCliente.qtt2,0)
		,cteConfEncomendaCliente.nome
		,cteConfEncomendaCliente.no
		,cteConfEncomendaCliente.estab
		,encomendadoLab = cteConfEncomendaFornec.qtt
		,qtRec = cteConfEncomendaCliente.qtt2
		,encomendadoCli = cteConfEncomendaCliente.qtt
		,qtRecLab = cteConfEncomendaFornec.qtt2
	from 
		cteConfEncomendaFornec 
		left join cteConfEncomendaCliente on cteConfEncomendaFornec.bistamp = cteConfEncomendaCliente.obistamp

GO
Grant Execute On up_keiretsu_OPL  to Public
Grant Control On up_keiretsu_OPL  to Public
go
