
/****** Object:  StoredProcedure [dbo].[sp_codbarras_sync]    Script Date: 10/11/2022 02:05:11 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[sp_codbarras_sync]') IS NOT NULL
	drop procedure dbo.sp_codbarras_sync
go

CREATE  PROCEDURE [dbo].[sp_codbarras_sync]
@tipo		varchar(50)
,@bcstamp	varchar(50)
,@ref		varchar(50)
,@design	varchar(100)
,@codigo	varchar(50)
,@ststamp	varchar(50)
,@defeito	bit
,@emb		varchar(50)
,@qtt		numeric(15,0)
,@ousrinis	varchar(50)
,@ousrdata	datetime
,@ousrhora	varchar(50)
,@usrinis	varchar(50)
,@usrdata	datetime
,@usrhora	varchar(50)
,@marcada	bit
,@u_tipo	varchar(50)
,@exportado	bit
,@site_nr	numeric(15,0)
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON
	BEGIN TRY  
		if ( not exists(select * from bc where bcstamp=@bcstamp) and @tipo != 'DEL')
		begin
			insert into bc (bcstamp,ref,design,codigo,ststamp,defeito,emb,qtt,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,marcada,u_tipo,exportado,site_nr)
			values (@bcstamp,@ref,@design,@codigo,@ststamp,@defeito,@emb,@qtt,@ousrinis,@ousrdata,@ousrhora,@usrinis,@usrdata,@usrhora,@marcada,@u_tipo,@exportado,@site_nr)
		end
		else
		begin
			if (exists(select * from bc where bcstamp=@bcstamp) and @tipo != 'DEL')
			begin
			 update bc 
			 set ref=@ref
				,design=@design
				,codigo=@codigo
				,ststamp=@ststamp
				,defeito=@defeito
				,emb=@emb
				,qtt=@qtt
				,ousrinis=@ousrinis
				,ousrdata=@ousrdata
				,ousrhora=@ousrhora
				,usrinis=@usrinis
				,usrdata=@usrdata
				,usrhora=@usrhora
				,marcada=@marcada
				,u_tipo=@u_tipo
				,exportado=@exportado
				,site_nr=@site_nr 
			 where bcstamp=@bcstamp
			end
			else
			begin
				delete from bc where bcstamp=@bcstamp
			end
		end
	END TRY 
	BEGIN CATCH  
	   ROLLBACK TRAN
	END CATCH  
		SET XACT_ABORT OFF
END

GO
GRANT EXECUTE ON dbo.sp_codbarras_sync to Public
GRANT CONTROL ON dbo.sp_codbarras_sync to Public
GO