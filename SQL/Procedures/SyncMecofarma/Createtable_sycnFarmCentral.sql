SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'sincFarmaciaCentral'))
BEGIN

	CREATE TABLE [dbo].[sincFarmaciaCentral](
		[stamp]				[varchar](36) NOT NULL,
		[tabela]			[varchar](4),
		[valor]				[int],
		[site]				[varchar](60),
		[descricao]			[varchar](254),
		[data]				[date]
		
	 CONSTRAINT [PK_sincFarmaciaCentral] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[sincFarmaciaCentral] ADD  CONSTRAINT [DF_sincFarmaciaCentral_tabela]  DEFAULT ('') FOR [tabela]

	ALTER TABLE [dbo].[sincFarmaciaCentral] ADD  CONSTRAINT [DF_sincFarmaciaCentral_valor]  DEFAULT (0) FOR [valor]

	ALTER TABLE [dbo].[sincFarmaciaCentral] ADD  CONSTRAINT [DF_sincFarmaciaCentral_site]  DEFAULT ('') FOR [site]

	ALTER TABLE [dbo].[sincFarmaciaCentral] ADD  CONSTRAINT [DF_sincFarmaciaCentral_descricao]  DEFAULT ('') FOR [descricao]

	ALTER TABLE [dbo].[sincFarmaciaCentral] ADD  CONSTRAINT [DF_sincFarmaciaCentral_data]  DEFAULT (getdate()) FOR [data]
	
END
