USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_ReplicateFaturas]    Script Date: 3/11/2022 3:59:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- =============================================
CREATE PROCEDURE [dbo].[sp_ReplicateFaturas]

@server			varchar(60)
--,@site_nr		numeric(4,0)
--,@site			varchar(20)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @sql varchar(max),
	@sql1 varchar(max),
	@sql2 varchar(max),
	@sql3 varchar(max),
	@sql4 varchar(max)
set @sql = N'
	SET NOCOUNT ON
	SET XACT_ABORT ON
	IF OBJECT_ID(''tempdb..#FT'') IS NOT NULL DROP TABLE #FT

	IF OBJECT_ID(''tempdb..#RE'') IS NOT NULL DROP TABLE #RE
	
	;with lista as 
		(select top 50 id, Identifier from ['+@server+'].[mecofarma].dbo.Rep_Control with (nolock) WHERE Table_Name = ''FT'' AND Processed = 0 and Operation=''I'' and identifier<>'''' order by id, Identifier asc)
	select Identifier INTO #FT from lista 
	select * from #FT

	;with lista1 as 
		(select distinct restamp from ['+@server+'].[mecofarma].dbo.rl  with (nolock) where ccstamp in (select ccstamp from ['+@server+'].[mecofarma].dbo.cc  with (nolock) where ftstamp in (select Identifier from #FT)) and ousrinis<>''ADM'')
	select restamp INTO #RE from lista1
	select * from #RE
	
	BEGIN TRY  
	   -- statements that may cause exceptions
		IF((SELECT COUNT(*) FROM #FT as R ) > 0)
		BEGIN
		  BEGIN TRAN

			PRINT ''INSERT''
			-- inser��o na FT'
set @sql1 = N'
			INSERT INTO [mecofarma].[dbo].[FT] ([ftstamp], [pais], [nmdoc], [fno], [no], [nome], [morada], [local], [codpost], [ncont], [bino], [bidata], [bilocal], [telefone], [zona], [vendedor], [vendnm], [fdata], [ftano], [pdata], [carga], [descar], [saida], [ivatx1], [ivatx2], [ivatx3], [fin], [final], [ndoc], [moeda], [fref], [ccusto], [ncusto], [facturada], [fnoft], [nmdocft], [estab], [cdata], [ivatx4], [segmento], [totqtt], [qtt1], [qtt2], [tipo], [cobrado], [cobranca], [tipodoc], [chora], [ivatx5], [ivatx6], [ivatx7], [ivatx8], [ivatx9], [cambiofixo], [memissao], [cobrador], [rota], [multi], [cheque], [clbanco], [clcheque], [chtotal], [echtotal], [custo], [eivain1], [eivain2], [eivain3], [eivav1], [eivav2], [eivav3], [ettiliq], [edescc], [ettiva], [etotal], [eivain4], [eivav4], [efinv], [ecusto], [eivain5], [eivav5], [edebreg], [eivain6], [eivav6], [eivain7], [eivav7], [eivain8], [eivav8], [eivain9], [eivav9], [total], [totalmoeda], [ivain1], [ivain2], [ivain3], [ivain4], [ivain5], [ivain6], [ivain7], [ivain8], [ivain9], [ivav1], [ivav2], [ivav3], [ivav4], [ivav5], [ivav6], [ivav7], [ivav8], [ivav9], [ttiliq], [ttiva], [descc], [debreg], [debregm], [intid], [nome2], [tpstamp], [tpdesc], [erdtotal], [rdtotal], [rdtotalm], [cambio], [site], [pnome], [pno], [cxstamp], [cxusername], [ssstamp], [ssusername], [anulado], [virs], [evirs], [valorm2], [ousrinis], [ousrdata], [ousrhora], [usrinis], [usrdata], [usrhora], [u_nratend], [u_lote2], [u_ltstamp2], [u_nslote2], [u_nslote], [u_slote2], [u_lote], [u_tlote], [u_tlote2], [u_ltstamp], [u_slote], [u_hclstamp], [exportado], [datatransporte], [localcarga], [tabIva], [id_tesouraria_conta], [pontosVd], [campanhas],[ivatx10],[ivatx11],[ivatx12],[ivatx13],[eivain10],[eivain11],[eivain12],[eivain13],[eivav10],[eivav11],[eivav12],[eivav13]) ' 
set @sql2 = N'
			SELECT [ftstamp], [pais], [nmdoc], [fno], [no], [nome], [morada], [local], [codpost], [ncont], [bino], [bidata], [bilocal], [telefone], [zona], [vendedor], [vendnm], [fdata], [ftano], [pdata], [carga], [descar], [saida], [ivatx1], [ivatx2], [ivatx3], [fin], [final], [ndoc], [moeda], [fref], [ccusto], [ncusto], [facturada], [fnoft], [nmdocft], [estab], [cdata], [ivatx4], [segmento], [totqtt], [qtt1], [qtt2], [tipo], [cobrado], [cobranca], [tipodoc], [chora], [ivatx5], [ivatx6], [ivatx7], [ivatx8], [ivatx9], [cambiofixo], [memissao], [cobrador], [rota], [multi], [cheque], [clbanco], [clcheque], [chtotal], [echtotal], [custo], [eivain1], [eivain2], [eivain3], [eivav1], [eivav2], [eivav3], [ettiliq], [edescc], [ettiva], [etotal], [eivain4], [eivav4], [efinv], [ecusto], [eivain5], [eivav5], [edebreg], [eivain6], [eivav6], [eivain7], [eivav7], [eivain8], [eivav8], [eivain9], [eivav9], [total], [totalmoeda], [ivain1], [ivain2], [ivain3], [ivain4], [ivain5], [ivain6], [ivain7], [ivain8], [ivain9], [ivav1], [ivav2], [ivav3], [ivav4], [ivav5], [ivav6], [ivav7], [ivav8], [ivav9], [ttiliq], [ttiva], [descc], [debreg], [debregm], [intid], [nome2], [tpstamp], [tpdesc], [erdtotal], [rdtotal], [rdtotalm], [cambio], [site], [pnome], [pno], [cxstamp], [cxusername], [ssstamp], [ssusername], [anulado], [virs], [evirs], [valorm2], [ousrinis], [ousrdata], [ousrhora], [usrinis], [usrdata], [usrhora], [u_nratend], [u_lote2], [u_ltstamp2], [u_nslote2], [u_nslote], [u_slote2], [u_lote], [u_tlote], [u_tlote2], [u_ltstamp], [u_slote], [u_hclstamp], [exportado], [datatransporte], [localcarga], [tabIva], [id_tesouraria_conta], [pontosVd], [campanhas],[ivatx10],[ivatx11],[ivatx12],[ivatx13],[eivain10],[eivain11],[eivain12],[eivain13],[eivav10],[eivav11],[eivav12],[eivav13]'
set @sql3 = N'
			--INSERT INTO [mecofarma].[dbo].[FT] 
			--SELECT *
			FROM ['+@server+'].mecofarma.dbo.FT  WITH (nolock)
			WHERE ftstamp IN (SELECT Identifier from #FT AS R )
			 PRINT ''INSERT FT''
			--inser��o na FT2
			INSERT INTO [mecofarma].[dbo].[FT2]
			SELECT * FROM ['+@server+'].mecofarma.dbo.FT2  WITH (nolock)
			WHERE ft2stamp IN (SELECT Identifier from #FT AS R)
			 PRINT ''INSERT FT2''
			-- inser��o na FI
			INSERT INTO [mecofarma].[dbo].[FI] 
			SELECT * FROM ['+@server+'].mecofarma.dbo.FI  WITH (nolock)
			WHERE ftstamp IN (SELECT Identifier from #FT AS R )
			 PRINT ''INSERT FI''
			-- inser��o na FI2
			INSERT INTO [mecofarma].[dbo].[FI2] 
			SELECT * FROM ['+@server+'].mecofarma.dbo.FI2  WITH (nolock)
			WHERE ftstamp IN (SELECT Identifier from #FT AS R )
			 PRINT ''INSERT FI2''
			-- inser��o na B_CERT
			INSERT INTO [mecofarma].[dbo].[B_cert] 
			SELECT * FROM ['+@server+'].mecofarma.dbo.B_cert  WITH (nolock)
			WHERE stamp IN (SELECT Identifier from #FT AS R )
			 PRINT ''INSERT B_CERT''
			UPDATE RC 
			SET RC.Processed = 1
			FROM ['+@server+'].[mecofarma].dbo.Rep_Control as RC  WITH (nolock)
			WHERE rc.Operation = ''I'' AND RC.Identifier in (select Identifier from #FT)
			 PRINT ''UPDATE''
		  COMMIT TRAN
		END'
set @sql4 = N'

		IF((SELECT COUNT(*) FROM #RE as R ) > 0)
		BEGIN
		  BEGIN TRAN
			PRINT ''INSERT RE''
			INSERT INTO re
			SELECT * FROM ['+@server+'].[mecofarma].[dbo].RE
			WHERE restamp IN (SELECT restamp from #RE AS R )

			insert into rl
			SELECT * FROM ['+@server+'].[mecofarma].[dbo].RL
			WHERE restamp IN (SELECT restamp from #RE AS R )
		  COMMIT TRAN
		END

	END TRY 
	BEGIN CATCH  
	   ROLLBACK TRAN
	END CATCH 
	
	update ft set no=no WHERE ftstamp IN (SELECT Identifier from #FT AS R ) 
	update re set no=no WHERE restamp IN (SELECT restamp from #RE AS R )
	SET XACT_ABORT OFF
'

print @sql+@sql1+@sql2+@sql3+@sql4
execute (@sql+@sql1+@sql2+@sql3+@sql4)
GO


