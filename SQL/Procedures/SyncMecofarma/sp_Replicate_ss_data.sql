USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_Replicate_ss_data]    Script Date: 3/11/2022 3:53:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- =============================================
CREATE PROCEDURE [dbo].[sp_Replicate_ss_data]

@server	varchar(60)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @sql varchar(max),
	@sql1 varchar(max),
	@sql2 varchar(max)
set @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON
IF OBJECT_ID(''tempdb..#SS'') IS NOT NULL DROP TABLE #SS

SELECT TOP 100 *
INTO #SS
FROM ['+@server+'].[mecofarma].dbo.Rep_Control
WHERE Table_Name = ''SS'' AND Processed = 0 order by id
	SELECT * from #SS

BEGIN TRY  
   -- statements that may cause exceptions
IF((SELECT COUNT(*) FROM #SS as R where r.Operation = ''I'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''INSERT''
	INSERT INTO [mecofarma].[dbo].[SS] 
	(ssstamp,site,pnome,pno,cxstamp,cxusername,ausername,auserno,dabrir,habrir,fusername,fuserno,dfechar,hfechar,fechada,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,marcada,evdinheiro_bruto,devolucoes,devolucoes_reais,creditos,nr_vendas,nr_atend,evdinheiro,epaga2,epaga1,echtotal,totalcaixa,pendente,exportado,fdstamp,fecho_epaga3,fecho_epaga4,fecho_epaga5,fecho_epaga6,causa,valorTPA,comissaoTPA,sacoDinheiro,fundoCaixa,diaFechado)
	SELECT 
	ssstamp,site,pnome,pno,cxstamp,cxusername,ausername,auserno,dabrir,habrir,fusername,fuserno,dfechar,hfechar,fechada,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,marcada,evdinheiro_bruto,devolucoes,devolucoes_reais,creditos,nr_vendas,nr_atend,evdinheiro,epaga2,epaga1,echtotal,totalcaixa,pendente,exportado,fdstamp,fecho_epaga3,fecho_epaga4,fecho_epaga5,fecho_epaga6,causa,valorTPA,comissaoTPA,sacoDinheiro,fundoCaixa,diaFechado
	FROM ['+@server+'].mecofarma.dbo.ss 
	WHERE ssstamp IN (SELECT Identifier from #ss AS R where r.Operation = ''I'')
	
	UPDATE RC
	SET RC.Processed = 1
	FROM ['+@server+'].[mecofarma].dbo.Rep_Control as RC
	INNER JOIN #SS as T ON rc.ID = t.ID
	WHERE t.Operation = ''I'' AND t.Processed = 0 and t.Table_Name =''SS''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  '

set @sql1 =N'BEGIN TRY  

IF((SELECT COUNT(*) FROM #SS as R where r.Operation = ''U'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''UPDATE''

	UPDATE T
	SET T.[site]=U.[site],T.[pnome]=U.[pnome],T.[pno]=U.[pno],T.[cxstamp]=U.[cxstamp],T.[cxusername]=U.[cxusername],T.[ausername]=U.[ausername],T.[auserno]=U.[auserno],T.[dabrir]=U.[dabrir],T.[habrir]=U.[habrir],T.[fusername]=U.[fusername],T.[fuserno]=U.[fuserno],T.[dfechar]=U.[dfechar],T.[hfechar]=U.[hfechar],T.[fechada]=U.[fechada],T.[ousrinis]=U.[ousrinis],T.[ousrdata]=U.[ousrdata],T.[ousrhora]=U.[ousrhora],T.[usrinis]=U.[usrinis],T.[usrdata]=U.[usrdata],T.[usrhora]=U.[usrhora],T.[marcada]=U.[marcada],T.[evdinheiro_bruto]=U.[evdinheiro_bruto],T.[devolucoes]=U.[devolucoes],T.[devolucoes_reais]=U.[devolucoes_reais],T.[creditos]=U.[creditos],T.[nr_vendas]=U.[nr_vendas],T.[nr_atend]=U.[nr_atend],T.[evdinheiro]=U.[evdinheiro],T.[epaga2]=U.[epaga2],T.[epaga1]=U.[epaga1],T.[echtotal]=U.[echtotal],T.[totalcaixa]=U.[totalcaixa],T.[pendente]=U.[pendente],T.[fdstamp]=U.[fdstamp],T.[fecho_epaga3]=U.[fecho_epaga3],T.[fecho_epaga4]=U.[fecho_epaga4],T.[fecho_epaga5]=U.[fecho_epaga5],T.[fecho_epaga6]=U.[fecho_epaga6],T.[causa]=U.[causa],T.[valorTPA]=U.[valorTPA],T.[comissaoTPA]=U.[comissaoTPA],T.[sacoDinheiro]=U.[sacoDinheiro],T.[fundoCaixa]=U.[fundoCaixa],T.[diaFechado]=U.[diaFechado] 
	FROM mecofarma.dbo.ss AS T
	INNER JOIN ['+@server+'].[mecofarma].[dbo].[ss] AS U on t.ssstamp = U.ssstamp
	WHERE U.ssstamp IN (SELECT Identifier from #SS AS R where r.Operation = ''U'')
	
	UPDATE RC
	SET RC.Processed = 1
	FROM ['+@server+'].mecofarma.dbo.Rep_Control as RC
	INNER JOIN #SS as T ON rc.ID = t.ID
	WHERE t.Operation = ''U'' AND t.Processed = 0 and t.Table_Name =''SS''

  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  '

set @sql2 =N'BEGIN TRY  
IF((SELECT COUNT(*) FROM #SS as R where r.Operation = ''D'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''DELETE''
	DELETE FROM [mecofarma].[dbo].[ss]
	WHERE ssstamp IN (SELECT Identifier from #SS AS R where r.Operation = ''D'')

	UPDATE RC
	SET RC.Processed = 1
	FROM ['+@server+'].mecofarma.dbo.Rep_Control as RC
	INNER JOIN #SS as T ON rc.ID = t.ID
	WHERE t.Operation = ''D'' AND t.Processed = 0 and t.Table_Name =''SS''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  
SET XACT_ABORT OFF'

print @sql+@sql1+@sql2
execute (@sql+@sql1+@sql2)
GO


