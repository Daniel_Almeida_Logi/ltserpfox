
/****** Object:  StoredProcedure [dbo].[up_getDocsFailedToSendX3]    Script Date: 11/11/2022 14:41:57 
exec up_getDocsFailedToSendX3
******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
		
if OBJECT_ID('[dbo].[up_getDocsFailedToSendX3]') IS NOT NULL
	drop procedure dbo.up_getDocsFailedToSendX3
go

CREATE PROCEDURE [dbo].[up_getDocsFailedToSendX3]
/* WITH ENCRYPTION */
AS

	if OBJECT_ID('tempdb.dbo.#tableX3DOCS') is not null
		drop table #tableX3DOCS



	select isnull((select top 1 description from LogsExternal.dbo.ExternalCommunicationLogs (nolock) where registerStamp=ft.ftstamp order by date desc),'') as erro1,
	case when (select COUNT(fistamp) from fi (nolock) where fi.ftstamp=ft.ftstamp)=0 then 'Sem Linhas'
			else case when(select COUNT(fistamp) from fi (nolock) where fi.ftstamp=ft.ftstamp) != ft.numLinhas then 'Faltam Linhas da Fi'
			else case when (select COUNT(stamp) from B_pagCentral (nolock) where B_pagCentral.nrAtend=ft.u_nratend)=0 then 'Falta B_Pagcentral'
					else case when (select COUNT(utstamp) from b_utentes (nolock) where b_utentes.no=ft.no and b_utentes.estab=ft.estab and b_utentes.nome is not null)=0  then 'Falta Cliente'
						else case when (select TOP 1 ssstamp from B_pagCentral (nolock) where B_pagCentral.nrAtend=ft.u_nratend)='' then 'Falta ssstamp B_Pagcentral'
							else ''end
						end
					end
				end
			end as falta,
	ftstamp,fdata , ousrhora, site
	INTO #tableX3DOCS
	from ft (nolock) 
	inner join table_sync_status (nolock) on ft.ftstamp=table_sync_status.regstamp
	where table_sync_status.sync=0 
	and nmdoc not like ('%Proforma%') 

	order by  
	ft.fdata, ft.fno 

		--	(CASE WHEN erro1!=''  THEN 'Foi enviado para o X3 e deu o erro:  ' + erro1 + ' ftstamp: ' + ftstamp + ' ftstamp: ' + ftstamp
		--	  WHEN falta !='' THEN 'Falta integrar a tabela ' + falta 
		--
		--else '' end) AS
	SELECT 
		' <u><b>Documentos em Falta:</b></u> ' + CONVERT(VARCHAR(30), COUNT(*)) 	 as descricao,
		' <u>� os documentos que ainda faltam enviar,com e sem erros. </u> '  as info_doc,
	    '<u>data:</u>' +  CONVERT(VARCHAR(30),GETDATE(), 121)  as dataHora
	FROM #tableX3DOCS
	UNION ALL
	SELECT top 100
	case when (erro1!='') then '<u>Erro:</u> '  + erro1
		 when (falta!='') then '<u>Falta:</u> '  + falta
		 else '<u>Erro: </u>'  + erro1 + '| Falta:</u> ' end as descricao,
		' <u>ftstamp:</u> ' + ftstamp + '     <u>Site:</u> ' + site   as info_doc,
	    '<u>data:</u>' +  CONVERT(VARCHAR(30),fdata + ousrhora, 121)  as dataHora
	FROM #tableX3DOCS


	if OBJECT_ID('tempdb.dbo.#tableX3DOCS') is not null
		drop table #tableX3DOCS

GO
GRANT EXECUTE ON dbo.up_getDocsFailedToSendX3 to Public
GRANT CONTROL ON dbo.up_getDocsFailedToSendX3 to Public
GO