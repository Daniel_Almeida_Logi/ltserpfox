SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[sp_stfami_ins_sync]') IS NOT NULL
	drop procedure dbo.sp_stfami_ins_sync
go

CREATE PROCEDURE [dbo].[sp_stfami_ins_sync]
	 @stfamistamp varchar(25)
	,@ref		  varchar(18)
	,@nome		  varchar(60)
	,@ousrinis	  varchar(30)
	,@ousrdata	  datetime
	,@ousrhora	  varchar(8)
	,@usrinis     varchar(30)
	,@usrdata     datetime
	,@usrhora	  varchar(8)
	,@exportado   bit
AS
BEGIN

	SET NOCOUNT ON;
	SET XACT_ABORT ON
	BEGIN TRY  

		IF (NOT EXISTS (SELECT 1 FROM stfami(nolock) WHERE stfamistamp = @stfamistamp))
		BEGIN
			INSERT INTO stfami (stfamistamp, ref,nome,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,exportado)
			VALUES (@stfamistamp,@ref,@nome,@ousrinis,@ousrdata,@ousrhora,@usrinis,@usrdata,@usrhora,@exportado) 
		END 
	END TRY 
	BEGIN CATCH  
	   ROLLBACK TRAN
	END CATCH  
		SET XACT_ABORT OFF
	
END

GO
GRANT EXECUTE ON dbo.sp_stfami_ins_sync to Public
GRANT CONTROL ON dbo.sp_stfami_ins_sync to Public
GO