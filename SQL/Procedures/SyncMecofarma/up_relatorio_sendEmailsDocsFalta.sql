/*
	EXEC up_relatorio_sendEmailsDocsFalta '20230127'
*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_relatorio_sendEmailsDocsFalta]') IS NOT NULL
drop procedure dbo.up_relatorio_sendEmailsDocsFalta
go

create procedure up_relatorio_sendEmailsDocsFalta
 @data		DATE
/* WITH ENCRYPTION */
AS

	select
		'<b>Farmacias : </b>'  as loja
		union all 
	SELECT
		'<b>' + site + ' : </b><u> '+ descricao +'</u>'  as loja
	FROM 
		sincFarmaciaCentral (NOLOCK)
	WHERE
		data = CONVERT(DATE,@data)
	
GO
Grant Execute On dbo.up_relatorio_sendEmailsDocsFalta to Public
Grant Control On dbo.up_relatorio_sendEmailsDocsFalta to Public
Go
