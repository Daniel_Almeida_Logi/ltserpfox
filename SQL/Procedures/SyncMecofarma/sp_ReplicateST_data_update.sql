



/*  call sql jobs 

exec sp_ReplicateST_data_update 1 


*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if OBJECT_ID('[dbo].[sp_ReplicateST_data_update]') IS NOT NULL
	DROP PROCEDURE dbo.sp_ReplicateST_data_update
GO

CREATE PROCEDURE [dbo].[sp_ReplicateST_data_update]
		@sitenr			INT
	
AS
SET NOCOUNT ON
	UPDATE st
	SET  st.epv1 = st1.epv1
		,st.qttminvd = st1.qttminvd
		,st.qttembal = st1.qttembal
		,st.epv1Ext = st1.epv1Ext
		,st.epv1ExtMoeda = st1.epv1ExtMoeda
		,st.inactivo = st1.inactivo
		,st.design = st1.design
		,st.tabiva = st1.tabiva
		,st.u_famstamp = st1.u_famstamp
		,st.url = st1.url
		,st.usr1 = st1.usr1
		,st.u_lab = st1.u_lab
		,st.obs = st1.obs
		,st.usr2=st1.usr2 
		,st.usr2desc=st1.usr2desc 
		,st.usr3=st1.usr3 
		,st.usr3desc=st1.usr3desc
		,st.codCNP =st1.codCNP
		,st.familia =st1.familia
		,st.faminome = st1.faminome
    FROM  st(nolock)
		  INNER JOIN [172.20.90.17].mecofarma.dbo.st st1(nolock) ON st.ststamp=st1.ststamp 
	WHERE  (st.epv1 <> st1.epv1 or st.qttminvd <> st1.qttminvd or st.qttembal <> st1.qttembal or st.epv1Ext <> st1.epv1Ext or st.inactivo <> st1.inactivo or st.design <> st1.design or st.tabiva <> st1.tabiva or st.epv1ExtMoeda <> st1.epv1ExtMoeda
	or st.validade<>st1.validade or st.u_famstamp <> st1.u_famstamp or st.url <> st1.url or st.usr1 <> st1.usr1 or st.u_lab <> st1.u_lab or st.obs <> st1.obs or st.usr2<>st1.usr2 or st.usr2desc<>st1.usr2desc or st.usr3<>st1.usr3 
	or st.usr3desc<>st1.usr3desc or st.codCNP<>st1.codCNP or st.familia<>st1.familia or st.faminome<>st1.faminome) 
	and st.site_nr = @sitenr and st1.site_nr = @sitenr


	UPDATE stfami
	SET
		 stfami.ref        =stfami1.ref      
		,stfami.nome	   =stfami1.nome	 
		,stfami.ousrinis   =stfami1.ousrinis 
		,stfami.ousrdata   =stfami1.ousrdata 
		,stfami.ousrhora   =stfami1.ousrhora 
		,stfami.usrinis    =stfami1.usrinis  
		,stfami.usrdata    =stfami1.usrdata  
		,stfami.usrhora    =stfami1.usrhora  
		,stfami.exportado  =stfami1.exportado
    FROM  stfami(nolock)
		  INNER JOIN [172.20.90.17].mecofarma.dbo.stfami stfami1(nolock) ON stfami.stfamistamp=stfami1.stfamistamp 
    WHERE (stfami.ref <> stfami1.ref or stfami.nome <> stfami1.nome or stfami.ousrinis <> stfami1.ousrinis or stfami.ousrdata <> stfami1.ousrdata 
	or stfami.usrinis <> stfami1.usrinis  or stfami.usrdata <> stfami1.usrdata or stfami.usrhora <> stfami1.usrhora or stfami.exportado <> stfami1.exportado)

	SET ANSI_NULLS off
	GO
	SET QUOTED_IDENTIFIER off
GO
GRANT EXECUTE ON dbo.sp_ReplicateST_data_update to Public
GRANT CONTROL ON dbo.sp_ReplicateST_data_update to Public
GO

