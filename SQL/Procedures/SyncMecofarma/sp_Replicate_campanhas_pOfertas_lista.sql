USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_Replicate_campanhas_pOfertas_lista]    Script Date: 3/11/2022 3:49:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- exec [sp_Replicate_campanhas_pOfertas_lista] '172.20.40.6\SQLEXPRESS'

-- =============================================
CREATE PROCEDURE [dbo].[sp_Replicate_campanhas_pOfertas_lista]

@server			varchar(60)
--,@site_nr		numeric(4,0)
--,@site			varchar(20)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @sql varchar(max)

set @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON

delete from ['+@server+'].[mecofarma].dbo.campanhas_pOfertas_lista
declare @contador numeric(15,0)
set @contador= (select COUNT(ref) from campanhas_pOfertas_lista )
while @contador > 0
begin 
DECLARE @result NVARCHAR(MAX), @result1 NVARCHAR(MAX) , @result2 NVARCHAR(MAX)
	select @result1=''insert into  ['+@server+'].[mecofarma].dbo.campanhas_pOfertas_lista''
	SELECT @result = ''select top 1000 * from campanhas_pOfertas_lista (nolock) ''
	select @result2 = ''where ref not in (select distinct ref from ['+@server+'].[mecofarma].dbo.campanhas_pOfertas_lista with (nolock))''
	print @result1+char(13)+@result+'' ''+@result2
	execute (@result1+'' ''+@result+'' ''+@result2)
	set @contador= @contador -1000
	print @contador
end
	'

print @sql


execute (@sql)
GO


