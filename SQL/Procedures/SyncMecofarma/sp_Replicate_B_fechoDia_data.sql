USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_Replicate_B_fechoDia_data]    Script Date: 3/11/2022 3:47:51 PM ******/
DROP PROCEDURE [dbo].[sp_Replicate_B_fechoDia_data]
GO

/****** Object:  StoredProcedure [dbo].[sp_Replicate_B_fechoDia_data]    Script Date: 3/11/2022 3:47:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- =============================================
CREATE PROCEDURE [dbo].[sp_Replicate_B_fechoDia_data]

@server	varchar(60)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @sql varchar(max)
select @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON
IF OBJECT_ID(''tempdb..#SS'') IS NOT NULL DROP TABLE #CX

SELECT TOP 30 *
INTO #CX
FROM ['+@server+'].[mecofarma].dbo.Rep_Control
WHERE Table_Name = ''B_fechoDia'' AND Processed = 0
	--SELECT * from #BC

BEGIN TRY  
   -- statements that may cause exceptions
IF((SELECT COUNT(*) FROM #CX as R where r.Operation = ''I'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''INSERT''
	INSERT INTO [mecofarma].[dbo].[B_fechoDia] 
	SELECT * FROM ['+@server+'].mecofarma.dbo.B_fechoDia
	WHERE fdstamp IN (SELECT Identifier from #CX AS R where r.Operation = ''I'')
	
	UPDATE RC
	SET RC.Processed = 1
	FROM ['+@server+'].[mecofarma].dbo.Rep_Control as RC
	INNER JOIN #CX as T ON rc.ID = t.ID
	WHERE t.Operation = ''I'' AND t.Processed = 0 and t.Table_Name =''B_fechoDia''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  

BEGIN TRY  

IF((SELECT COUNT(*) FROM #CX as R where r.Operation = ''U'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''UPDATE''

	UPDATE T
	SET T.[fdStamp]=U.[fdStamp],T.[site]=U.[site],T.[fechado]=U.[fechado],T.[dataAbriu]=U.[dataAbriu],T.[userAbriu]=U.[userAbriu],T.[dataFecho]=U.[dataFecho],T.[userFecho]=U.[userFecho],T.[ousr]=U.[ousr],T.[usr]=U.[usr],T.[usrdata]=U.[usrdata]
	FROM mecofarma.dbo.B_fechoDia AS T
	INNER JOIN ['+@server+'].[mecofarma].[dbo].[B_fechoDia] AS U on t.fdstamp = U.fdstamp
	WHERE U.fdstamp IN (SELECT Identifier from #CX AS R where r.Operation = ''U'')
	
	UPDATE RC
	SET RC.Processed = 1
	FROM ['+@server+'].mecofarma.dbo.Rep_Control as RC
	INNER JOIN #CX as T ON rc.ID = t.ID
	WHERE t.Operation = ''U'' AND t.Processed = 0 and t.Table_Name =''B_fechoDia''

  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  


BEGIN TRY  
IF((SELECT COUNT(*) FROM #CX as R where r.Operation = ''D'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''DELETE''
	DELETE FROM [mecofarma].[dbo].[B_fechoDia]
	WHERE fdstamp IN (SELECT Identifier from #CX AS R where r.Operation = ''D'')

	UPDATE RC
	SET RC.Processed = 1
	FROM ['+@server+'].mecofarma.dbo.Rep_Control as RC
	INNER JOIN #CX as T ON rc.ID = t.ID
	WHERE t.Operation = ''D'' AND t.Processed = 0 and t.Table_Name =''B_fechoDia''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  
SET XACT_ABORT OFF

'
print @sql
execute (@sql)
GO


