USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_ReplicateDocumentos]    Script Date: 3/11/2022 3:58:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- exec [sp_ReplicateDocumentos] '172.20.40.6\SQLEXPRESS', 1, 'ATLANTICO'

-- =============================================
CREATE PROCEDURE [dbo].[sp_ReplicateDocumentos]

@server			varchar(60)
,@site_nr		numeric(4,0)
,@site			varchar(20)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @sql varchar(max),
	@sql1 varchar(max),
	@sql11 varchar(max),
	@sql2 varchar(max),
	@sql20 varchar(max),
	@sql21 varchar(max),
	@sql3 varchar(max),
	@sql31 varchar(max),
	@sql4 varchar(max),
	@sql5 varchar(max)
set @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON
	IF OBJECT_ID(''tempdb..#BO'') IS NOT NULL DROP TABLE #BO
	IF OBJECT_ID(''tempdb..#BO2'') IS NOT NULL DROP TABLE #BO2
	IF OBJECT_ID(''tempdb..#BO22'') IS NOT NULL DROP TABLE #BO22
	IF OBJECT_ID(''tempdb..#BOALT'') IS NOT NULL DROP TABLE #BOALT
	IF OBJECT_ID(''tempdb..#BONOTIF'') IS NOT NULL DROP TABLE #BONOTIF
	IF OBJECT_ID(''tempdb..#BONOTIFU'') IS NOT NULL DROP TABLE #BONOTIFU

	declare @siteenc varchar(30),
			@notif_enc bit,
			@utilnotif varchar(200),
			@grpnotif varchar(200)

	update Rep_Control_site set Processed=1 WHERE Table_Name = ''BO'' AND Processed = 0 and Site='''+@site+''' 
	and (select COUNT(bistamp) from bi (nolock) where bi.bostamp=Rep_Control_site.Identifier and (armazem='+convert(varchar,@site_nr)+' or ar2mazem='+convert(varchar,@site_nr)+'))=0

	SELECT top 10 *
	INTO #BO
	FROM dbo.Rep_Control_site (nolock)
	WHERE Table_Name = ''BO'' AND Processed = 0 and Site='''+@site+'''
	select * from #BO

	BEGIN TRY  
	IF((SELECT COUNT(*) FROM #BO as R where r.Operation = ''I'' ) > 0)
	BEGIN
	  BEGIN TRAN
	  PRINT ''INSERT''

		DELETE FROM ['+@server+'].[mecofarma].[dbo].[BO]
		WHERE bostamp IN (SELECT Identifier from #BO AS R where r.Operation = ''I'')

		INSERT INTO ['+@server+'].[mecofarma].[dbo].[BO]
		SELECT * FROM mecofarma.dbo.BO (NOLOCK) 
		WHERE bostamp IN (SELECT Identifier from #BO AS R where r.Operation = ''I'')

		SELECT bostamp INTO #BO22
		FROM mecofarma.dbo.BO (NOLOCK)
		WHERE bostamp in (SELECT Identifier from #BO AS R where r.Operation = ''I'')

		delete from ['+@server+'].[mecofarma].[dbo].[BO2] 
		where bo2stamp in (select bostamp from #BO22)
	
		INSERT INTO ['+@server+'].[mecofarma].[dbo].[BO2]
		SELECT * FROM mecofarma.dbo.BO2  (NOLOCK)
		WHERE bo2stamp IN (SELECT Identifier from #BO AS R where r.Operation = ''I'')

		delete from ['+@server+'].[mecofarma].[dbo].[BI] 
		where bostamp in (select bostamp from #BO22)

		INSERT INTO ['+@server+'].[mecofarma].[dbo].[BI]
		SELECT * FROM mecofarma.dbo.BI (NOLOCK)
		WHERE bostamp IN (SELECT Identifier from #BO AS R where r.Operation = ''I'')
		and (armazem='+convert(varchar,@site_nr)+' or ar2mazem='+convert(varchar,@site_nr)+')'
set @sql1 = N'
		--Inser��o na BI2

		delete from ['+@server+'].[mecofarma].[dbo].[BI2] 
		where bostamp in (select bostamp from #BO22)

		INSERT INTO ['+@server+'].[mecofarma].[dbo].[BI2] (bi2stamp,fnstamp,bostamp,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,morada,local,codpost,fistamp,exportado)
		SELECT BI2.bi2stamp,BI2.fnstamp,BI2.bostamp,BI2.ousrinis,BI2.ousrdata,BI2.ousrhora,BI2.usrinis,BI2.usrdata,BI2.usrhora,BI2.morada,BI2.local,BI2.codpost,BI2.fistamp,BI2.exportado FROM mecofarma.dbo.BI2 (NOLOCK)
		inner join mecofarma.dbo.BI (NOLOCK) on mecofarma.dbo.BI2.bi2stamp=mecofarma.dbo.BI.bistamp
		WHERE mecofarma.dbo.BI2.bostamp IN (SELECT Identifier from #BO AS R where r.Operation = ''I'')
		and (armazem='+convert(varchar,@site_nr)+' or ar2mazem='+convert(varchar,@site_nr)+')

		UPDATE RC
		SET RC.Processed = 1
		FROM dbo.Rep_Control_site as RC
		INNER JOIN #BO as T ON rc.ID = t.ID
		WHERE t.Operation = ''I'' AND t.Processed = 0 and t.Table_Name =''BO'' and t.Site='''+@site+''' '
set @sql11 = N'
		------
		IF((SELECT 
				COUNT(*) 
			FROM #BO as R
			inner join bo (nolock) on bo.bostamp=r.identifier 
			where r.Operation = ''I'' and bo.nmdos=''Encomenda de Cliente'' ) > 0)
		BEGIN
			SELECT distinct identifier, operation INTO #BONOTIF
			FROM #BO as R
			inner join bo (nolock) on bo.bostamp=r.identifier 
			where r.Operation = ''I'' and bo.nmdos=''Encomenda de Cliente''

			--declare @siteenc varchar(30)
			set @siteenc = '''+@site+'''
			--declare @notif_enc bit
			set @notif_enc = (select top 1 bool from ['+@server+'].[mecofarma].[dbo].B_Parameters_site with (nolock) where stamp=''ADM0000000115'' and site=@siteenc)
			IF @notif_enc = 1
			begin
				--declare @utilnotif varchar(200)
				set @utilnotif = (select textValue = ltrim(rtrim(isnull(textValue,''''))) from ['+@server+'].[mecofarma].[dbo].B_Parameters_site with (nolock) where stamp=''ADM0000000113'' and site=@siteenc)
				if @utilnotif<>''''
				begin
					insert into ['+@server+'].[mecofarma].[dbo].[notificacoes]
						(stamp
						,origem
						,usrorigem
						,destino
						,usrdestino
						,grpdestino,ecradestino,stampdestino,mensagem,recebido,tiponotif,data,site)
					select 
						left(newid(),21)
						,''Online''
						,''''
						,''''
						,b_us.iniciais
						,''''
						,''BO''
						,inserted.bostamp
						,''Inser��o de nova encomenda com o n� ''+ cast(inserted.obrano as varchar(10))  + '' Status: '' + LTRIM(RTRIM(bo2.status))
						,0
						,''Nova Encomenda''
						,getdate()
						,inserted.site
						from #BONOTIF
						inner join bo (nolock) as inserted on inserted.bostamp=#BONOTIF.Identifier 
						inner join bo2 (nolock) on inserted.bostamp=bo2.bo2stamp
						inner join b_us (nolock) on iniciais in (Select * from dbo.up_splitToTable(@utilnotif,'',''))
						where inserted.nmdos=''Encomenda de Cliente'' and inserted.ousrinis=''ONL''
								and #BONOTIF.Operation = ''I''
				end
				--declare @grpnotif varchar(200)
				set @grpnotif = (select textValue = ltrim(rtrim(isnull(textValue,''''))) from ['+@server+'].[mecofarma].[dbo].B_Parameters_site with (nolock) where stamp=''ADM0000000114'' and site=@siteenc)
				if @grpnotif<>''''
				begin
					insert into ['+@server+'].[mecofarma].[dbo].notificacoes
						(stamp
						,origem
						,usrorigem
						,destino
						,usrdestino
						,grpdestino,ecradestino,stampdestino,mensagem,recebido,tiponotif,data,site)
					select 
						left(newid(),21)
						,''Online''
						,''''
						,''''
						,b_us.iniciais
						,''''
						,''BO''
						,inserted.bostamp
						,''Inser��o de nova encomenda com o n� ''+ cast(inserted.obrano as varchar(10))  + '' Status: '' + LTRIM(RTRIM(bo2.status))
						,0
						,''Nova Encomenda''
						,getdate()
						,inserted.site
						from #BONOTIF
						inner join bo (nolock) as inserted on inserted.bostamp=#BONOTIF.Identifier
						inner join bo2 (nolock) on inserted.bostamp=bo2.bo2stamp  
						inner join b_us (nolock) on grupo in (Select * from dbo.up_splitToTable(@grpnotif,'',''))
						where inserted.nmdos=''Encomenda de Cliente'' and inserted.ousrinis=''ONL''
								and #BONOTIF.Operation = ''I''
				end
			end
		end
		------
	  COMMIT TRAN
	END
	END TRY 
	BEGIN CATCH  
	   ROLLBACK TRAN
	END CATCH  

	BEGIN TRY  

	IF((SELECT COUNT(*) FROM #BO as R where r.Operation = ''U'' ) > 0)
	BEGIN
	  BEGIN TRAN
	  PRINT ''UPDATE'''
set @sql2 = N'
		-- Update BO
		UPDATE T
		SET T.[nmdos] = U.[nmdos], T.[obrano] = U.[obrano], T.[dataobra] = U.[dataobra], T.[nome] = U.[nome], T.[totaldeb] = U.[totaldeb], T.[etotaldeb] = U.[etotaldeb], T.[tipo] = U.[tipo], T.[datafinal] = U.[datafinal], T.[sqtt14] = U.[sqtt14], T.[vendedor] = U.[vendedor], T.[vendnm] = U.[vendnm], T.[no] = U.[no], T.[boano] = U.[boano], T.[dataopen] = U.[dataopen], T.[obs] = U.[obs], T.[trab1] = U.[trab1], T.[ndos] = U.[ndos], T.[moeda] = U.[moeda], T.[estab] = U.[estab], T.[morada] = U.[morada], T.[local] = U.[local], T.[codpost] = U.[codpost], T.[ncont] = U.[ncont], T.[logi1] = U.[logi1], T.[ccusto] = U.[ccusto], T.[etotal] = U.[etotal], T.[ecusto] = U.[ecusto], T.[ebo_1tvall] = U.[ebo_1tvall], T.[ebo_2tvall] = U.[ebo_2tvall], T.[ebo11_bins] = U.[ebo11_bins], T.[ebo11_iva] = U.[ebo11_iva], T.[ebo21_bins] = U.[ebo21_bins], T.[ebo21_iva] = U.[ebo21_iva], T.[ebo31_bins] = U.[ebo31_bins], T.[ebo31_iva] = U.[ebo31_iva], T.[ebo41_bins] = U.[ebo41_bins], T.[ebo41_iva] = U.[ebo41_iva], T.[ebo51_bins] = U.[ebo51_bins], T.[ebo51_iva] = U.[ebo51_iva], T.[ebo12_bins] = U.[ebo12_bins], T.[ebo12_iva] = U.[ebo12_iva], T.[ebo22_bins] = U.[ebo22_bins], T.[ebo22_iva] = U.[ebo22_iva], T.[ebo32_bins] = U.[ebo32_bins], T.[ebo32_iva] = U.[ebo32_iva], T.[ebo42_bins] = U.[ebo42_bins], T.[ebo42_iva] = U.[ebo42_iva], T.[ebo52_bins] = U.[ebo52_bins], T.[ebo52_iva] = U.[ebo52_iva], T.[ebo_totp1] = U.[ebo_totp1], T.[ebo_totp2] = U.[ebo_totp2], T.[memissao] = U.[memissao]'
set @sql20 = N'
		, T.[nome2] = U.[nome2], T.[origem] = U.[origem], T.[site] = U.[site], T.[pnome] = U.[pnome], T.[pno] = U.[pno], T.[ocupacao] = U.[ocupacao], T.[tpdesc] = U.[tpdesc], T.[ousrinis] = U.[ousrinis], T.[ousrdata] = U.[ousrdata], T.[ousrhora] = U.[ousrhora], T.[usrinis] = U.[usrinis], T.[usrdata] = U.[usrdata], T.[usrhora] = U.[usrhora], T.[u_dataentr] = U.[u_dataentr], T.[ebo61_iva] = U.[ebo61_iva], T.[fref] = U.[fref], T.[ncusto] = U.[ncusto], T.[ultfact] = U.[ultfact], T.[exportado] = U.[exportado], T.[tabIva] = U.[tabIva], T.[ebo61_bins] = U.[ebo61_bins], T.[ebo62_bins] = U.[ebo62_bins], T.[ebo62_iva] = U.[ebo62_iva], T.[edescc] = U.[edescc] 
		FROM ['+@server+'].[mecofarma].[dbo].[BO] as T
		INNER JOIN mecofarma.dbo.BO (NOLOCK) AS U on t.bostamp = U.bostamp
		WHERE U.bostamp IN (SELECT Identifier from #BO AS R where r.Operation = ''U'')'
set @sql21 = N'
		UPDATE T
		SET T.[autotipo]=U.[autotipo],T.[pdtipo]=U.[pdtipo],T.[usrinis]=U.[usrinis],T.[usrdata]=U.[usrdata],T.[usrhora]=U.[usrhora],T.[armazem]=U.[armazem],T.[xpdviatura]=U.[xpdviatura],T.[xpddata]=U.[xpddata],T.[xpdhora]=U.[xpdhora],T.[morada]=U.[morada],T.[codpost]=U.[codpost],T.[telefone]=U.[telefone],T.[contacto]=U.[contacto],T.[email]=U.[email],T.[etotalciva]=U.[etotalciva],T.[etotiva]=U.[etotiva],T.[u_class]=U.[u_class],T.[u_doccont]=U.[u_doccont],T.[u_fostamp]=U.[u_fostamp],T.[ebo71_IVA]=U.[ebo71_IVA],T.[ebo81_IVA]=U.[ebo81_IVA],T.[ebo91_IVA]=U.[ebo91_IVA],T.[ATDocCode]=U.[ATDocCode],T.[exportado]=U.[exportado],T.[ebo71_bins]=U.[ebo71_bins],T.[ebo72_bins]=U.[ebo72_bins],T.[ebo72_iva]=U.[ebo72_iva],T.[ebo81_bins]=U.[ebo81_bins],T.[ebo82_bins]=U.[ebo82_bins],T.[ebo82_iva]=U.[ebo82_iva],T.[ebo91_bins]=U.[ebo91_bins],T.[ebo92_bins]=U.[ebo92_bins],T.[ebo92_iva]=U.[ebo92_iva],T.[IVATX1]=U.[IVATX1],T.[IVATX2]=U.[IVATX2],T.[IVATX3]=U.[IVATX3],T.[IVATX4]=U.[IVATX4],T.[IVATX5]=U.[IVATX5],T.[IVATX6]=U.[IVATX6],T.[IVATX7]=U.[IVATX7],T.[IVATX8]=U.[IVATX8],T.[IVATX9]=U.[IVATX9],T.[nrReceita]=U.[nrReceita],T.[codext]=U.[codext],T.[u_nratend]=U.[u_nratend],T.[morada_ent]=U.[morada_ent],T.[localidade_ent]=U.[localidade_ent],T.[codpost_ent]=U.[codpost_ent],T.[pais_ent]=U.[pais_ent],T.[modo_envio]=U.[modo_envio],T.[pagamento]=U.[pagamento],T.[pinAcesso]=U.[pinAcesso],T.[pinOpcao]=U.[pinOpcao],T.[nrviasimp]=U.[nrviasimp],T.[momento_pagamento]=U.[momento_pagamento]
		,T.[ebo101_bins]= U.[ebo101_bins],T.[ebo111_bins]= U.[ebo111_bins],T.[ebo121_bins]= U.[ebo121_bins],T.[ebo131_bins]= U.[ebo131_bins],T.[ebo101_iva]= U.[ebo101_iva],T.[ebo111_iva]= U.[ebo111_iva],T.[ebo121_iva]= U.[ebo121_iva],T.[ebo131_iva]= U.[ebo131_iva],T.[ivatx10]= U.[ivatx10],T.[ivatx11]= U.[ivatx11],T.[ivatx12]= U.[ivatx12],T.[ivatx13]= U.[ivatx13]
		FROM ['+@server+'].[mecofarma].[dbo].[BO2] as T
		INNER JOIN mecofarma.dbo.BO2 (NOLOCK) AS U on t.bo2stamp = U.bo2stamp
		WHERE U.bo2stamp IN (SELECT Identifier from #BO AS R where r.Operation = ''U'')

		delete from ['+@server+'].[mecofarma].[dbo].[BI] 
		where bostamp in (SELECT Identifier from #BO AS R where r.Operation = ''U'')

		insert into ['+@server+'].[mecofarma].[dbo].[BI] 
		select * FROM mecofarma.dbo.BI (NOLOCK) where mecofarma.dbo.BI.bostamp in (SELECT Identifier from #BO AS R where r.Operation = ''U'') and (armazem='+convert(varchar,@site_nr)+' or ar2mazem='+convert(varchar,@site_nr)+')

		delete from ['+@server+'].[mecofarma].[dbo].[BI2] 
		where bostamp in (SELECT Identifier from #BO AS R where r.Operation = ''U'')'
set @sql3 = N'
		INSERT INTO ['+@server+'].[mecofarma].[dbo].[BI2] (bi2stamp,fnstamp,bostamp,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,morada,local,codpost,fistamp,exportado)
		SELECT BI2.bi2stamp,BI2.fnstamp,BI2.bostamp,BI2.ousrinis,BI2.ousrdata,BI2.ousrhora,BI2.usrinis,BI2.usrdata,BI2.usrhora,BI2.morada,BI2.local,BI2.codpost,BI2.fistamp,BI2.exportado FROM mecofarma.dbo.BI2 (NOLOCK)
		inner join mecofarma.dbo.BI (NOLOCK) on mecofarma.dbo.BI2.bi2stamp=mecofarma.dbo.BI.bistamp
		WHERE mecofarma.dbo.BI2.bostamp IN (SELECT Identifier from #BO AS R where r.Operation = ''U'')
		and (armazem='+convert(varchar,@site_nr)+' or ar2mazem='+convert(varchar,@site_nr)+')

		UPDATE RC
		SET RC.Processed = 1
		FROM dbo.Rep_Control_site as RC
		INNER JOIN #BO as T ON rc.ID = t.ID
		WHERE t.Operation = ''U'' AND t.Processed = 0 and t.Table_Name =''BO'' and t.Site='''+@site+''' '
set @sql31 = N'
		------
		IF((SELECT 
				COUNT(*) 
			FROM #BO as R
			inner join bo (nolock) on bo.bostamp=r.identifier 
			where r.Operation = ''U'' and bo.nmdos=''Encomenda de Cliente'' ) > 0)
		BEGIN
			SELECT distinct identifier, operation INTO #BONOTIFU
			FROM #BO as R
			inner join bo (nolock) on bo.bostamp=r.identifier 
			where r.Operation = ''U'' and bo.nmdos=''Encomenda de Cliente''

			--declare @siteenc varchar(30)
			set @siteenc = '''+@site+'''
			--declare @notif_enc bit
			set @notif_enc = (select top 1 bool from ['+@server+'].[mecofarma].[dbo].B_Parameters_site with (nolock) where stamp=''ADM0000000115'' and site=@siteenc)
			IF @notif_enc = 1
			begin
				--declare @utilnotif varchar(200)
				set @utilnotif = (select textValue = ltrim(rtrim(isnull(textValue,''''))) from ['+@server+'].[mecofarma].[dbo].B_Parameters_site with (nolock) where stamp=''ADM0000000113'' and site=@siteenc)
				if @utilnotif<>''''
				begin
					insert into ['+@server+'].[mecofarma].[dbo].[notificacoes]
						(stamp
						,origem
						,usrorigem
						,destino
						,usrdestino
						,grpdestino,ecradestino,stampdestino,mensagem,recebido,tiponotif,data,site)
					select 
						left(newid(),21)
						,''Online''
						,''''
						,''''
						,b_us.iniciais
						,''''
						,''BO''
						,inserted.bostamp
						,''Foi alterada a encomenda com o n� ''+ cast(inserted.obrano as varchar(10))  + '' Status: '' + LTRIM(RTRIM(bo2.status))
						,0
						,''Altera��o Encomenda''
						,getdate()
						,inserted.site
						from #BONOTIFU
						inner join bo (nolock) as inserted on inserted.bostamp=#BONOTIFU.Identifier 
						inner join bo2 (nolock) on inserted.bostamp=bo2.bo2stamp
						inner join b_us (nolock) on iniciais in (Select * from dbo.up_splitToTable(@utilnotif,'',''))
						where inserted.nmdos=''Encomenda de Cliente'' and inserted.ousrinis=''ONL''
								and (select count(stamp) from ['+@server+'].[mecofarma].[dbo].[notificacoes] with (nolock) where tiponotif like ''%Encomenda%'' and recebido=0 and stampdestino=inserted.bostamp)=0
				end
				--declare @grpnotif varchar(200)
				set @grpnotif = (select textValue = ltrim(rtrim(isnull(textValue,''''))) from ['+@server+'].[mecofarma].[dbo].B_Parameters_site with (nolock) where stamp=''ADM0000000114'' and site=@siteenc)
				if @grpnotif<>''''
				begin
					insert into ['+@server+'].[mecofarma].[dbo].notificacoes
						(stamp
						,origem
						,usrorigem
						,destino
						,usrdestino
						,grpdestino,ecradestino,stampdestino,mensagem,recebido,tiponotif,data,site)
					select 
						left(newid(),21)
						,''Online''
						,''''
						,''''
						,b_us.iniciais
						,''''
						,''BO''
						,inserted.bostamp
						,''Foi alterada a encomenda com o n� ''+ cast(inserted.obrano as varchar(10))  + '' Status: '' + LTRIM(RTRIM(bo2.status))
						,0
						,''Altera��o Encomenda''
						,getdate()
						,inserted.site
						from #BONOTIFU
						inner join bo (nolock) as inserted on inserted.bostamp=#BONOTIFU.Identifier
						inner join bo2 (nolock) on inserted.bostamp=bo2.bo2stamp  
						inner join b_us (nolock) on grupo in (Select * from dbo.up_splitToTable(@grpnotif,'',''))
						where inserted.nmdos=''Encomenda de Cliente'' and inserted.ousrinis=''ONL''
								and (select count(stamp) from ['+@server+'].[mecofarma].[dbo].[notificacoes] with (nolock) where tiponotif like ''%Encomenda%'' and recebido=0 and stampdestino=inserted.bostamp)=0
				end
			end
		end
		------'
set @sql4 = N'
	  COMMIT TRAN
	END
	END TRY 
	BEGIN CATCH  
	   ROLLBACK TRAN
	END CATCH  

	BEGIN TRY  
	IF((SELECT COUNT(*) FROM #BO as R where r.Operation = ''D'' ) > 0)
	BEGIN
	  BEGIN TRAN
	  PRINT ''DELETE''
	
		UPDATE RC
		SET RC.Processed = 1
		FROM dbo.Rep_Control_site as RC
		INNER JOIN #BO as T ON rc.ID = t.ID
		WHERE t.Operation = ''D'' AND t.Processed = 0 and t.Table_Name =''BO'' and t.Site='''+@site+'''
	  COMMIT TRAN
	END
	END TRY 
	BEGIN CATCH  
	   ROLLBACK TRAN
	END CATCH  
	SET XACT_ABORT OFF

	insert into ['+@server+'].[mecofarma].[dbo].[Bi]
	select * from bi (NOLOCK) where bostamp in (select identifier from Rep_Control_site (nolock) where Table_Name=''BO'' and Site='''+@site+''' and CONVERT(varchar, Date, 112)>=''20200401'' and Processed=1)
	and bistamp not in (select bistamp from ['+@server+'].[mecofarma].[dbo].[Bi] ) and (armazem='+convert(varchar,@site_nr)+' or ar2mazem='+convert(varchar,@site_nr)+')  and CONVERT(varchar, rdata, 112)>=''20200401''
	
'
set @sql5 = N'
	insert into Rep_Control_site (Table_Name, Identifier, Operation, Processed, Site, Date)
	select ''BO'',bostamp,''I'',0,'''+@site+''', GETDATE() from bo (nolock) where nmdos=''Encomenda de Cliente'' and site='''+@site+'''
	and bostamp not in (select bostamp from ['+@server+'].[mecofarma].[dbo].[BO] boo with (nolock) where boo.nmdos=''Encomenda de Cliente'' and boo.site='''+@site+''')
	and bo.dataobra between GETDATE()-1 and getdate()
'
print @sql
print @sql1
print @sql11
print @sql2
print @sql20
print @sql21
print @sql3
print @sql31
print @sql4
print @sql5

execute (@sql+@sql1+@sql11+@sql2+@sql20+@sql21+@sql3+@sql31+@sql4+@sql5)
GO


