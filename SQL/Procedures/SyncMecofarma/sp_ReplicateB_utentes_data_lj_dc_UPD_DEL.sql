

/****** Object:  StoredProcedure [dbo].[sp_ReplicateB_utentes_data_lj_dc_UPD_DEL]    Script Date: 3/11/2022 3:56:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- =============================================
CREATE PROCEDURE [dbo].[sp_ReplicateB_utentes_data_lj_dc_UPD_DEL]

@server			varchar(60)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @sql varchar(max),
	@sql1 varchar(max),
	@sql2 varchar(max)
set @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON
IF OBJECT_ID(''tempdb..#TP'') IS NOT NULL DROP TABLE #TP

;with ctecur as (
SELECT TOP 100 *
FROM ['+@server+'].[mecofarma].[dbo].[Rep_Control]
WHERE Table_Name = ''B_utentes'' AND Processed = 0 and Operation in (''U''))
select distinct identifier INTO #TP from ctecur


insert into #TP
SELECT TOP 30 rc.identifier
FROM ['+@server+'].[mecofarma].[dbo].[Rep_Control] rc
inner join ['+@server+'].[mecofarma].[dbo].[b_utentes] but on rc.Identifier=but.utstamp
WHERE rc.Table_Name = ''B_utentes'' AND rc.Processed = 0 and rc.Operation in (''I'') and but.no>1000000
and but.no in (select no from b_utentes (nolock))

SELECT * from #TP

BEGIN TRY  

IF((SELECT COUNT(*) FROM #TP as R ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''UPDATE''

	UPDATE T'
set @sql1 = N'
	SET T.[nome] = U.[nome], T.[no] = U.[no], T.[estab] = U.[estab], T.[ncont] = U.[ncont], T.[nbenef] = U.[nbenef], T.[local] = U.[local], T.[nascimento] = U.[nascimento], T.[morada] = U.[morada], T.[telefone] = U.[telefone], T.[obs] = U.[obs], T.[codpost] = U.[codpost], T.[bino] = U.[bino], T.[fax] = U.[fax], T.[tlmvl] = U.[tlmvl], T.[zona] = U.[zona], T.[tipo] = U.[tipo], T.[sexo] = U.[sexo], T.[email] = U.[email], T.[codpla] = U.[codpla], T.[despla] = U.[despla], T.[valipla] = U.[valipla], T.[valipla2] = U.[valipla2], T.[nrcartao] = U.[nrcartao], T.[hablit] = U.[hablit], T.[profi] = U.[profi], T.[nbenef2] = U.[nbenef2], T.[entfact] = U.[entfact], T.[peso] = U.[peso], T.[altura] = U.[altura], T.[inactivo] = U.[inactivo], T.[nocredit] = U.[nocredit], T.[ousrinis] = U.[ousrinis], T.[ousrdata] = U.[ousrdata], T.[ousrhora] = U.[ousrhora], T.[usrinis] = U.[usrinis], T.[usrdata] = getdate(), T.[usrhora] = CONVERT(varchar, getdate(), 108), T.[nome2] = U.[nome2], T.[url] = U.[url], T.[cobnao] = U.[cobnao], T.[naoencomenda] = U.[naoencomenda], T.[vencimento] = U.[vencimento], T.[eplafond] = U.[eplafond], T.[nib] = U.[nib], T.[odatraso] = U.[odatraso], T.[tabiva] = U.[tabiva], T.[tipodesc] = U.[tipodesc], T.[esaldlet] = U.[esaldlet], T.[clinica] = U.[clinica], T.[modofact] = U.[modofact], T.[desconto] = U.[desconto], T.[alimite] = U.[alimite], T.[particular] = U.[particular], T.[clivd] = U.[clivd], T.[autofact] = U.[autofact], T.[esaldo] = U.[esaldo], T.[conta] = U.[conta], T.[descpp] = U.[descpp], T.[ccusto] = U.[ccusto], T.[contalet] = U.[contalet], T.[contaletdes] = U.[contaletdes], T.[contaletsac] = U.[contaletsac], T.[contaainc] = U.[contaainc], T.[contaacer] = U.[contaacer], T.[tpdesc] = U.[tpdesc], T.[classe] = U.[classe], T.[radicaltipoemp] = U.[radicaltipoemp], T.[fref] = U.[fref], T.[moeda] = U.[moeda], T.[contatit] = U.[contatit], T.[contafac] = U.[contafac], T.[ultvenda] = U.[ultvenda], T.[entpla] = U.[entpla], T.[descp] = U.[descp], T.[codigop] = U.[codigop], T.[mae] = U.[mae], T.[pai] = U.[pai], T.[notif] = U.[notif], T.[pacgen] = U.[pacgen], T.[tratamento] = U.[tratamento], T.[ninscs] = U.[ninscs], T.[csaude] = U.[csaude], T.[drfamilia] = U.[drfamilia], T.[recmtipo] = U.[recmtipo], T.[recmmotivo] = U.[recmmotivo], T.[recmdescricao] = U.[recmdescricao], T.[recmdatainicio] = U.[recmdatainicio], T.[recmdatafim] = U.[recmdatafim], T.[itmotivo] = U.[itmotivo], T.[itdescricao] = U.[itdescricao], T.[itdatainicio] = U.[itdatainicio], T.[itdatafim] = U.[itdatafim], T.[cesd] = U.[cesd], T.[cesdcart] = U.[cesdcart], T.[cesdcp] = U.[cesdcp], T.[cesdidi] = U.[cesdidi], T.[cesdidp] = U.[cesdidp], T.[cesdpd] = U.[cesdpd], T.[cesdval] = U.[cesdval], T.[atestado] = U.[atestado], T.[atnumero] = U.[atnumero], T.[attipo] = U.[attipo], T.[atcp] = U.[atcp], T.[atpd] = U.[atpd], T.[atval] = U.[atval], T.[descpNat] = U.[descpNat], T.[codigopNat] = U.[codigopNat], T.[duplicado] = U.[duplicado], T.[nomesproprios] = U.[nomesproprios], T.[obito] = U.[obito], T.[apelidos] = U.[apelidos], T.[idstamp] = U.[idstamp], T.[pais] = U.[pais], T.[exportado] = U.[exportado], T.[entCompart] = U.[entCompart], T.[no_ext] = U.[no_ext], T.[noConvencao] = U.[noConvencao], T.[designConvencao] = U.[designConvencao], T.[id] = U.[id], T.[nprescsns] = U.[nprescsns], T.[direcaoTec] = U.[direcaoTec], T.[proprietario] = U.[proprietario], T.[moradaAlt] = U.[moradaAlt], T.[noAssociado] = U.[noAssociado], T.[Alvara] = U.[Alvara], T.[software] = U.[software], T.[ars] = U.[ars], T.[codSwift] = U.[codSwift], T.[designcom] = U.[designcom], T.[nomcom] = U.[nomcom], T.[autorizado] = U.[autorizado], T.[removido] = U.[removido], T.[data_removido] = U.[data_removido], T.[tokenaprovacao] = U.[tokenaprovacao], T.[autoriza_sms] = U.[autoriza_sms], T.[autoriza_emails] = U.[autoriza_emails], T.[nrss] = U.[nrss], T.[site_compart] = U.[site_compart], T.[cativa] = U.[cativa], T.[perccativa] = U.[perccativa]'
set @sql2 = N'
	from mecofarma.dbo.b_utentes AS T 
	inner join ['+@server+'].[mecofarma].[dbo].[b_utentes] as U on t.utstamp = U.utstamp
	WHERE U.utstamp IN (SELECT Identifier from #TP AS R )
	
	UPDATE ['+@server+'].[mecofarma].[dbo].[rep_control]
	SET RC.Processed = 1
	FROM ['+@server+'].[mecofarma].[dbo].[rep_control] as RC
	--INNER JOIN #TP as T ON rc.ID = t.ID
	WHERE  rc.Processed = 0 and rc.Table_Name =''B_utentes'' AND rc.identifier IN (SELECT Identifier from #TP AS R)

  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  
SET XACT_ABORT OFF
'

print @sql+@sql1+@sql2
execute (@sql+@sql1+@sql2)


