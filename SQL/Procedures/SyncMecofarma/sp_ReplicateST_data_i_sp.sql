USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_ReplicateST_data_i_sp]    Script Date: 3/11/2022 4:01:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- =============================================
CREATE PROCEDURE [dbo].[sp_ReplicateST_data_i_sp]

@server			varchar(60),
@site_nr		numeric(4,0)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @sql varchar(max),
	@sql1 varchar(max),
	@sql2 varchar(max)
set @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON
IF OBJECT_ID(''tempdb..#ST'') IS NOT NULL DROP TABLE #ST
IF OBJECT_ID(''tempdb..#ST1'') IS NOT NULL DROP TABLE #ST1

declare @ststamp	varchar(50)
, @ref	varchar(50)
, @design	varchar(50)
, @familia	varchar(50)
, @stock	numeric(15,2)
, @epv1		numeric(15,2)
, @fornecedor	varchar(80)
, @usr1		varchar(50)
, @validade	datetime
, @unidade	varchar(50)
, @tabiva	numeric(5,0)
, @fornec	numeric(15,0)
, @obs		varchar(254)
, @conversao	numeric(15,2)
, @ivaincl	bit
, @faminome	varchar(60)
, @epcusto	numeric(15,2)
, @epcpond	numeric(15,2)
, @ousrinis	varchar(60)
, @ousrdata	datetime
, @ousrhora varchar(60)
, @inactivo	bit
, @u_lab	varchar(150)
, @u_tipoetiq numeric(15,2)
, @u_fonte	varchar(30)
, @u_famstamp	varchar(60)
, @site_nr	numeric(5,0)
, @qttminvd	numeric(9,0)
, @qttembal	numeric(9,0)
, @epv1Ext numeric(15,2)
, @epv1ExtMoeda	varchar(60)
, @codCNP	varchar(60)
, @Id		varchar(50)

SELECT TOP 100 *
INTO #ST
FROM dbo.Rep_Control (nolock)
inner join st (nolock) on Rep_Control.Identifier=st.ststamp
WHERE Table_Name = ''st'' AND Processed = 0 and st.site_nr='+convert(varchar,@site_nr)+' and Operation = ''I''
order by id
	SELECT * from #ST

delete from Rep_Control where Identifier in (select ststamp from ['+@server+'].[mecofarma].[dbo].[st] stt with (nolock) where stt.site_nr='+convert(varchar,@site_nr)+') and Table_Name=''ST'' and Processed=0 and Operation=''I''

BEGIN TRY  
   -- statements that may cause exceptions
IF((SELECT COUNT(*) FROM #ST as R where r.Operation = ''I'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''INSERT''

	select * into #ST1 from #ST where Operation = ''I''
	
	While (Select Count(*) From #ST1) > 0
	Begin
		Select Top 1 @Id = Identifier From #ST1
		print @Id
		SELECT @ststamp=ststamp, @ref=ref, @design=design, @familia=familia, @stock=stock, @epv1=epv1, @fornecedor=fornecedor, @usr1=usr1, @validade=validade, @unidade=unidade
			, @tabiva=tabiva, @fornec=fornec,@obs=obs, @conversao=conversao, @ivaincl=ivaincl, @faminome=faminome, @epcusto=epcusto, @epcpond=epcpond, @ousrinis=ousrinis
			, @ousrdata=ousrdata, @ousrhora=ousrhora, @inactivo=inactivo, @u_lab=u_lab, @u_tipoetiq=u_tipoetiq, @u_fonte=u_fonte, @u_famstamp=u_famstamp, @site_nr=site_nr
			, @qttminvd=qttminvd, @qttembal=qttembal, @epv1Ext=epv1Ext, @epv1ExtMoeda=epv1ExtMoeda, @codCNP=codCNP 
		FROM dbo.st (nolock) where ststamp=@id
		exec ['+@server+'].[mecofarma].[dbo].[sp_ST_ins_sync] @ststamp, @ref, @design, @familia, @stock, @epv1, @fornecedor, @usr1, @validade, @unidade, @tabiva, @fornec,@obs, @conversao, @ivaincl, @faminome, @epcusto, @epcpond, @ousrinis, @ousrdata, @ousrhora, @inactivo, @u_lab, @u_tipoetiq, @u_fonte, @u_famstamp, @site_nr, @qttminvd, @qttembal, @epv1Ext, @epv1ExtMoeda, @codCNP
		Delete #ST1 Where Identifier = @Id
	End
	
	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control as RC
	INNER JOIN #ST as T ON rc.ID = t.ID
	WHERE t.Operation = ''I'' AND t.Processed = 0 and t.Table_Name =''st''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  

SET XACT_ABORT OFF
'

print @sql
execute (@sql)
GO


