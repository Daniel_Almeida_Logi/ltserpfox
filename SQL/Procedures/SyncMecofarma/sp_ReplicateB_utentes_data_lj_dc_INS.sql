USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_ReplicateB_utentes_data_lj_dc_INS]    Script Date: 3/11/2022 3:55:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- =============================================
CREATE PROCEDURE [dbo].[sp_ReplicateB_utentes_data_lj_dc_INS]

@server			varchar(60)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @sql varchar(max),
	@sql1 varchar(max),
	@sql2 varchar(max)
set @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON
IF OBJECT_ID(''tempdb..#TP'') IS NOT NULL DROP TABLE #TP

SELECT TOP 15 rc.*
INTO #TP
FROM ['+@server+'].[mecofarma].[dbo].[Rep_Control] rc
inner join ['+@server+'].[mecofarma].[dbo].[b_utentes] but on rc.Identifier=but.utstamp
WHERE rc.Table_Name = ''B_utentes'' AND rc.Processed = 0 and rc.Operation in (''I'') and but.no>1000000
and but.no not in (select no from b_utentes (nolock))

	SELECT * from #TP

BEGIN TRY  
   -- statements that may cause exceptions
IF((SELECT COUNT(*) FROM #TP as R where r.Operation = ''I'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''INSERT''
	INSERT INTO b_utentes (utstamp,nome,no,estab,ncont,nbenef,local,nascimento,morada,telefone,obs,codpost,bino,fax,tlmvl,zona,tipo,sexo,email,codpla,despla,valipla,valipla2,nrcartao,hablit,profi,nbenef2,entfact
,peso,altura,inactivo,nocredit,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,nome2,url,cobnao,naoencomenda,vencimento,eplafond,nib,odatraso,tabiva,tipodesc,esaldlet,clinica,modofact,desconto,alimite,particular
,clivd,autofact,esaldo,conta,descpp,ccusto,contalet,contaletdes,contaletsac,contaainc,contaacer,tpdesc,classe,radicaltipoemp,fref,moeda,contatit,contafac,ultvenda,entpla,descp,codigop,mae,pai,notif,pacgen,tratamento
,ninscs,csaude,drfamilia,recmtipo,recmmotivo,recmdescricao,recmdatainicio,recmdatafim,itmotivo,itdescricao,itdatainicio,itdatafim,cesd,cesdcart,cesdcp,cesdidi,cesdidp,cesdpd,cesdval,atestado,atnumero,attipo
,atcp,atpd,atval,descpNat,codigopNat,duplicado,nomesproprios,obito,apelidos,idstamp,pais,exportado,entCompart,no_ext,noConvencao,designConvencao,id,nprescsns,direcaoTec,proprietario,moradaAlt,noAssociado,Alvara
,software,ars,codSwift,designcom,nomcom,autorizado,removido,data_removido,tokenaprovacao,autoriza_sms,autoriza_emails,nrss,site_compart,cativa,perccativa)
	SELECT utstamp,nome,no,estab,ncont,nbenef,local,nascimento,morada,telefone,obs,codpost,bino,fax,tlmvl,zona,tipo,sexo,email,codpla,despla,valipla,valipla2,nrcartao,hablit,profi,nbenef2,entfact
,peso,altura,inactivo,nocredit,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,nome2,url,cobnao,naoencomenda,vencimento,eplafond,nib,odatraso,tabiva,tipodesc,esaldlet,clinica,modofact,desconto,alimite,particular
,clivd,autofact,esaldo,conta,descpp,ccusto,contalet,contaletdes,contaletsac,contaainc,contaacer,tpdesc,classe,radicaltipoemp,fref,moeda,contatit,contafac,ultvenda,entpla,descp,codigop,mae,pai,notif,pacgen,tratamento
,ninscs,csaude,drfamilia,recmtipo,recmmotivo,recmdescricao,recmdatainicio,recmdatafim,itmotivo,itdescricao,itdatainicio,itdatafim,cesd,cesdcart,cesdcp,cesdidi,cesdidp,cesdpd,cesdval,atestado,atnumero,attipo
,atcp,atpd,atval,descpNat,codigopNat,duplicado,nomesproprios,obito,apelidos,idstamp,pais,exportado,entCompart,no_ext,noConvencao,designConvencao,id,nprescsns,direcaoTec,proprietario,moradaAlt,noAssociado,Alvara
,software,ars,codSwift,designcom,nomcom,autorizado,removido,data_removido,tokenaprovacao,autoriza_sms,autoriza_emails,nrss,site_compart,cativa,perccativa FROM ['+@server+'].[mecofarma].[dbo].[b_utentes]
	WHERE utstamp IN (SELECT distinct Identifier from #TP AS R where r.Operation = ''I'')
	
	UPDATE ['+@server+'].[mecofarma].[dbo].[rep_control]
	SET RC.Processed = 1
	FROM ['+@server+'].[mecofarma].[dbo].[Rep_Control] as RC
	INNER JOIN #TP as T ON rc.ID = t.ID
	WHERE t.Operation = ''I'' AND t.Processed = 0 and t.Table_Name =''B_utentes''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  
SET XACT_ABORT OFF
'

print @sql
execute (@sql)
GO


