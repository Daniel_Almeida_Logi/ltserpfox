USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_ReplicateST_data_u_d]    Script Date: 3/11/2022 4:01:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- =============================================
CREATE PROCEDURE [dbo].[sp_ReplicateST_data_u_d]

@server			varchar(60),
@site_nr		numeric(4,0)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @sql varchar(max),
	@sql1 varchar(max),
	@sql2 varchar(max)
set @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON
IF OBJECT_ID(''tempdb..#ST'') IS NOT NULL DROP TABLE #ST

SELECT TOP 500 *
INTO #ST
FROM dbo.Rep_Control (nolock)
inner join st (nolock) on Rep_Control.Identifier=st.ststamp
WHERE Table_Name = ''st'' AND Processed = 0 and st.site_nr='+convert(varchar,@site_nr)+' and (Operation = ''U'' or Operation = ''D'')
order by id
	--SELECT * from #ST

BEGIN TRY  
   -- statements that may cause exceptions
IF((SELECT COUNT(*) FROM #ST as R where r.Operation = ''I'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''INSERT''
	INSERT INTO ['+@server+'].[mecofarma].[dbo].[st]
	SELECT * FROM mecofarma.dbo.st 
	WHERE ststamp IN (SELECT Identifier from #ST AS R where r.Operation = ''I'')
	
	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control as RC
	INNER JOIN #ST as T ON rc.ID = t.ID
	WHERE t.Operation = ''I'' AND t.Processed = 0 and t.Table_Name =''st''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  

BEGIN TRY  

IF((SELECT COUNT(*) FROM #ST as R where r.Operation = ''U'' ) > 0)
BEGIN
  BEGIN DISTRIBUTED TRANSACTION;  
  PRINT ''UPDATE''

	INSERT INTO ['+@server+'].[mecofarma].[dbo].[st_replicated]
	SELECT * FROM mecofarma.dbo.st 
	WHERE ststamp IN (SELECT Identifier from #ST AS R where r.Operation = ''U'')

	PRINT ''Processing changes on replication control table''
	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control as RC
	INNER JOIN #ST as T ON rc.ID = t.ID
	WHERE t.Operation = ''U'' AND t.Processed = 0 and t.Table_Name =''st''

	PRINT ''Processing on External Source''
	EXEC ['+@server+'].[mecofarma].[dbo].sp_SYNC_ST
	   


  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  


BEGIN TRY  
IF((SELECT COUNT(*) FROM #ST as R where r.Operation = ''D'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''DELETE''
	DELETE FROM ['+@server+'].[mecofarma].[dbo].[st]
	WHERE ststamp IN (SELECT Identifier from #ST AS R where r.Operation = ''D'')

	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control as RC
	INNER JOIN #ST as T ON rc.ID = t.ID
	WHERE t.Operation = ''D'' AND t.Processed = 0 and t.Table_Name =''st''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  

SET XACT_ABORT OFF
'

print @sql
execute (@sql)
GO


