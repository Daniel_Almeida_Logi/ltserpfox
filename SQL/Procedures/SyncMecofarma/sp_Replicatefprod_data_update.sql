
/*  

exec sp_Replicatefprod_data_update 1 


*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if OBJECT_ID('[dbo].[sp_Replicatefprod_data_update]') IS NOT NULL
	DROP PROCEDURE dbo.sp_Replicatefprod_data_update
GO

CREATE PROCEDURE [dbo].[sp_Replicatefprod_data_update]
		@sitenr			INT
	
AS
SET NOCOUNT ON

	UPDATE fprod
	SET   
		fprod.fprodstamp=fprod1.fprodstamp,
		fprod.nome=fprod1.nome,	
		fprod.barcode=fprod1.barcode,
		fprod.dosuni=fprod1.dosuni,
		fprod.descricao=fprod1.descricao,
		fprod.generico=fprod1.generico,
		fprod.grphmgcode=fprod1.grphmgcode,
		fprod.grphmgdescr=fprod1.grphmgdescr,
		fprod.tiptratdescr=fprod1.tiptratdescr,
		fprod.ref=fprod1.ref,
		fprod.design=fprod1.design,
		fprod.u_nomerc=fprod1.u_nomerc,
		fprod.dci=fprod1.dci,
		fprod.dcipt_id=fprod1.dcipt_id,
		fprod.cnpem=fprod1.cnpem,
		fprod.sitcomdescr=fprod1.sitcomdescr,
		fprod.vias_admin= fprod1.vias_admin,
		fprod.med_guid =fprod1.med_guid,
		fprod.psico =fprod1.psico
	FROM  fprod(nolock)
		  INNER JOIN [172.20.90.17].mecofarma.dbo.fprod fprod1(nolock) ON fprod.cnp=fprod1.cnp 
	WHERE  (fprod.nome<>fprod1.nome or 	fprod.barcode<>fprod1.barcode  or fprod.descricao<>fprod1.descricao or fprod.generico<>fprod1.generico or fprod.grphmgcode<>fprod1.grphmgcode or 
fprod.grphmgdescr<>fprod1.grphmgdescr or fprod.tiptratdescr<>fprod1.tiptratdescr or fprod.ref<>fprod1.ref or fprod.design<>fprod1.design or fprod.u_nomerc<>fprod1.u_nomerc or fprod.dci<>fprod1.dci or 
fprod.dcipt_id<>fprod1.dcipt_id or fprod.cnpem<>fprod1.cnpem or fprod.sitcomdescr<>fprod1.sitcomdescr or fprod.vias_admin<> fprod1.vias_admin or fprod.med_guid <>fprod1.med_guid or fprod.fprodstamp<>fprod1.fprodstamp) 


GO
GRANT EXECUTE ON dbo.sp_Replicatefprod_data_update to Public
GRANT CONTROL ON dbo.sp_Replicatefprod_data_update to Public
GO

