USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_Replicate_campanhas_st]    Script Date: 3/11/2022 3:50:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- exec [sp_Replicate_campanhas_st] '172.20.40.6\SQLEXPRESS', 1

-- =============================================
CREATE PROCEDURE [dbo].[sp_Replicate_campanhas_st]

@server			varchar(60)
,@site_nr		numeric(4,0)
--,@site			varchar(20)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @sql varchar(max)

set @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON

delete from ['+@server+'].[mecofarma].dbo.campanhas_st
insert into  ['+@server+'].[mecofarma].dbo.campanhas_st
select * from campanhas_st (nolock) 
	'

print @sql


execute (@sql)
GO


