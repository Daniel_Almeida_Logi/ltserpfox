USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_ReplicateCB_data]    Script Date: 3/11/2022 3:57:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- =============================================
CREATE PROCEDURE [dbo].[sp_ReplicateCB_data]

@server			varchar(60)
--,@site_nr		numeric(4,0)
,@site			varchar(20)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON
SET XACT_ABORT ON

declare @sql varchar(max),
	@sql1 varchar(max),
	@sql2 varchar(max)
set @sql = N'
SET NOCOUNT ON;
SET XACT_ABORT ON

IF OBJECT_ID(''tempdb..#CB'') IS NOT NULL DROP TABLE #CB

	SELECT *
INTO #CB
FROM dbo.Rep_Control_site (nolock)
WHERE Table_Name = ''CB'' AND Processed = 0 and site='''+@site+'''
----	SELECT * from #CB

BEGIN TRY  
   -- statements that may cause exceptions
IF((SELECT COUNT(*) FROM #CB as R where r.Operation = ''I'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''INSERT''
	INSERT INTO ['+@server+'].[mecofarma].[dbo].[CB]
	SELECT * FROM mecofarma.dbo.CB (nolock)
	WHERE cbstamp IN (SELECT Identifier from #CB AS R where r.Operation = ''I'') and processado=0
	
	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control_site as RC
	INNER JOIN #CB as T ON rc.ID = t.ID
	WHERE t.Operation = ''I'' AND t.Processed = 0 and t.Table_Name =''CB'' and t.site='''+@site+'''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  

BEGIN TRY  

IF((SELECT COUNT(*) FROM #CB as R where r.Operation = ''U'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''UPDATE'''
set @sql1 = N'
	UPDATE T
	SET T.[pais] = U.[pais], T.[moeda] = U.[moeda], T.[data] = U.[data], T.[cambio] = U.[cambio], T.[ousrinis] = U.[ousrinis], T.[ousrdata] = U.[ousrdata], T.[ousrhora] = U.[ousrhora], T.[usrinis] = U.[usrinis], T.[usrdata] = U.[usrdata], T.[usrhora] = U.[usrhora], T.[moedaOrigem] = U.[moedaOrigem], T.[intromanual] = U.[intromanual]
	FROM ['+@server+'].[mecofarma].[dbo].[CB] as T
	INNER JOIN mecofarma.dbo.CB AS U on t.cbstamp = U.cbstamp
	WHERE U.cbstamp IN (SELECT Identifier from #CB AS R where r.Operation = ''U'')'
set @sql2 = N'
	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control_site as RC
	INNER JOIN #CB as T ON rc.ID = t.ID
	WHERE t.Operation = ''U'' AND t.Processed = 0 and t.Table_Name =''CB'' and t.site='''+@site+'''

  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  


BEGIN TRY  
IF((SELECT COUNT(*) FROM #CB as R where r.Operation = ''D'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''DELETE''
	DELETE FROM ['+@server+'].[mecofarma].[dbo].[CB]
	WHERE cbstamp IN (SELECT Identifier from #CB AS R where r.Operation = ''D'')

	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control_site as RC
	INNER JOIN #CB as T ON rc.ID = t.ID
	WHERE t.Operation = ''D'' AND t.Processed = 0 and t.Table_Name =''CB'' and t.site='''+@site+'''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  
SET XACT_ABORT OFF

'

print @sql+@sql1+@sql2
execute (@sql+@sql1+@sql2)
GO


