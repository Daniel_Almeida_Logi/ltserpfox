SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[sp_b_fidel_replicate]') IS NOT NULL
	drop procedure dbo.sp_b_fidel_replicate
go

CREATE PROCEDURE [dbo].[sp_b_fidel_replicate]
	@fstamp varchar(28),
	@clstamp varchar(28),
	@clno numeric(18,0),
	@clestab numeric(18,0),
	@nrcartao varchar(50),
	@pontosAtrib numeric(18,0),
	@pontosUsa numeric(18,0),
	@inactivo bit,
	@porvalor numeric(10,2),
	@porpontos numeric(18,0),
	@validade datetime,
	@ousrdata datetime,
	@usrdata datetime,
	@ousr int,
	@usr  int,
	@atendimento  numeric(18,0),
	@nat	numeric(9,0),
	@nvd numeric(9,0),
	@valcartao numeric(15,2),
	@valcartaousado numeric(15,2),
	@valcartaohist numeric(15,2),
	@valcartaoini numeric(15,2),
	@valcartao_cativado numeric(15,2),
	@valcativo numeric(20,2),
	@operation char(1)

AS
BEGIN

	SET NOCOUNT ON;
	SET XACT_ABORT ON
	BEGIN TRY  

	if @operation = 'D'
	BEGIN
	 DELETE b_fidel where fstamp=@fstamp 
	END

		IF (NOT EXISTS (SELECT 1 FROM b_fidel(nolock) WHERE fstamp = @fstamp))
		BEGIN
			INSERT INTO b_fidel (fstamp,clstamp,clno,clestab,nrcartao,pontosAtrib,pontosUsa,inactivo,porvalor,porpontos,validade,ousrdata,usrdata,ousr,usr,atendimento,nat,nvd,valcartao,valcartaousado,valcartaohist,valcartaoini,valcartao_cativado,valcativo)
			VALUES (@fstamp,@clstamp,@clno,@clestab,@nrcartao,@pontosAtrib,@pontosUsa,@inactivo,@porvalor,@porpontos,@validade,@ousrdata,@usrdata,@ousr,@usr,@atendimento,@nat,@nvd,@valcartao,@valcartaousado,@valcartaohist,@valcartaoini,@valcartao_cativado,@valcativo) 
		END 
		ELSE
		BEGIN
			update b_fidel set
				clstamp =@clstamp,
				clno=@clno ,
				clestab=@clestab ,
				nrcartao=@nrcartao ,
				pontosAtrib=@pontosAtrib ,
				pontosUsa=@pontosUsa ,
				inactivo=@inactivo ,
				porvalor=@porvalor,
				porpontos=@porpontos,
				validade=@validade,
				ousrdata=@ousrdata,
				usrdata=@usrdata,
				ousr=@ousr,
				usr=@usr,
				atendimento=@atendimento,
				nat=@nat,
				nvd=@nvd,
				valcartao=@valcartao ,
				valcartaousado=@valcartaousado ,
				valcartaohist=@valcartaohist ,
				valcartaoini=@valcartaoini ,
				valcartao_cativado=@valcartao_cativado ,
				valcativo=@valcativo
			WHERE fstamp=@fstamp

		END
	END TRY 
	BEGIN CATCH  
	   ROLLBACK TRAN
	END CATCH  
		SET XACT_ABORT OFF
	
END

GO
GRANT EXECUTE ON dbo.sp_b_fidel_replicate to Public
GRANT CONTROL ON dbo.sp_b_fidel_replicate to Public
GO