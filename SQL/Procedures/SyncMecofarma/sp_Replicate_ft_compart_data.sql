USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_Replicate_ft_compart_data]    Script Date: 3/11/2022 3:52:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - ft_compart Ext Server to local server 
-- =============================================
CREATE PROCEDURE [dbo].[sp_Replicate_ft_compart_data]

@server			varchar(60)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @sql varchar(max),
	@sql1 varchar(max),
	@sql2 varchar(max)
set @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON
IF OBJECT_ID(''tempdb..#ft_compart'') IS NOT NULL DROP TABLE #ft_compart

SELECT TOP 100 *
INTO #ft_compart
FROM ['+@server+'].[mecofarma].dbo.Rep_Control
WHERE Table_Name = ''ft_compart'' AND Processed = 0
order by id
	SELECT * from #ft_compart

BEGIN TRY  
   -- statements that may cause exceptions
IF((SELECT COUNT(*) FROM #ft_compart as R where r.Operation in (''I'',''U'') ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''INSERT''

	delete from ft_compart WHERE token IN (SELECT Identifier from #ft_compart AS R where r.Operation in (''I'',''U''))

	INSERT INTO [mecofarma].[dbo].[ft_compart]
	SELECT * FROM ['+@server+'].mecofarma.dbo.ft_compart 
	WHERE token IN (SELECT distinct Identifier from #ft_compart AS R where r.Operation in (''I'',''U''))
	
	UPDATE RC
	SET RC.Processed = 1
	FROM ['+@server+'].[mecofarma].dbo.Rep_Control as RC
	INNER JOIN #ft_compart as T ON rc.ID = t.ID
	WHERE t.Operation in (''I'',''U'') AND t.Processed = 0 and t.Table_Name =''ft_compart''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  

BEGIN TRY  
IF((SELECT COUNT(*) FROM #ft_compart as R where r.Operation = ''D'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''DELETE''
	DELETE FROM [mecofarma].[dbo].[ft_compart]
	WHERE token IN (SELECT Identifier from #ft_compart AS R where r.Operation = ''D'')

	UPDATE RC
	SET RC.Processed = 1
	FROM ['+@server+'].[mecofarma].dbo.Rep_Control as RC
	INNER JOIN #ft_compart as T ON rc.ID = t.ID
	WHERE t.Operation = ''D'' AND t.Processed = 0 and t.Table_Name =''ft_compart''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  
	SET XACT_ABORT OFF

'

print @sql
execute (@sql)
GO


