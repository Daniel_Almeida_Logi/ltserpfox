USE [mecofarma]
GO
/****** Object:  StoredProcedure [dbo].[sp_ReplicateST_data_i_sp]    Script Date: 03/11/2022 21:51:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- =============================================

if OBJECT_ID('[dbo].[sp_Replicatestfami_data_i_sp]') IS NOT NULL
	drop procedure dbo.sp_Replicatestfami_data_i_sp
go

create PROCEDURE [dbo].[sp_Replicatestfami_data_i_sp]

@server			varchar(60),
@site			varchar(30)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @sql varchar(max),
	@sql1 varchar(max),
	@sql2 varchar(max)
set @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON
IF OBJECT_ID(''tempdb..#STFAMI'') IS NOT NULL DROP TABLE #STFAMI
IF OBJECT_ID(''tempdb..#STFAMI1'') IS NOT NULL DROP TABLE #STFAMI1

declare  @stfamistamp varchar(25)
,@ref		  varchar(18)
,@nome		  varchar(60)
,@ousrinis	  varchar(30)
,@ousrdata	  datetime
,@ousrhora	  varchar(8)
,@usrinis     varchar(30)
,@usrdata	  datetime
,@usrhora	  varchar(8)
,@exportado   bit
,@Id varchar(36)




SELECT TOP 100 *
INTO #stfami
FROM dbo.Rep_Control_site (nolock)
inner join STFAMI (nolock) on Rep_Control_site.Identifier=STFAMI.stfamistamp
WHERE Table_Name = ''STFAMI'' AND Processed = 0 and Rep_Control_site.site='''+convert(varchar,@site)+''' and Operation = ''I''
order by id
	SELECT * from #stfami

BEGIN TRY  
   -- statements that may cause exceptions
IF((SELECT COUNT(*) FROM #stfami as R where r.Operation = ''I'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''INSERT''

	select * into #stfami1 from #stfami where Operation = ''I''
	
	While (Select Count(*) From #stfami1) > 0
	Begin
		Select Top 1 @Id = Identifier From #stfami1
		print @Id
		SELECT @stfamistamp=stfamistamp, @ref=ref, @nome=nome, @ousrinis=ousrinis, @ousrdata=ousrdata, 
			  @ousrhora=ousrhora, @usrinis=usrinis, @usrdata=usrdata, @usrhora=usrhora, @exportado=exportado
		FROM dbo.stfami (nolock) where stfamistamp=@id
		exec ['+@server+'].[mecofarma].[dbo].[sp_stfami_ins_sync] @stfamistamp, @ref, @nome, @ousrinis, @ousrdata, @ousrhora, @usrinis,@usrdata, @usrhora, @exportado
		Delete #stfami1 Where Identifier = @Id
	End
	
	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control_site as RC
	INNER JOIN #stfami as T ON rc.ID = t.ID
	WHERE t.Operation = ''I'' AND t.Processed = 0 and t.Table_Name =''STFAMI'' and RC.site='''+convert(varchar,@site)+'''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  
SET XACT_ABORT OFF
'

print @sql
execute (@sql)

GO
GRANT EXECUTE ON dbo.sp_Replicatestfami_data_i_sp to Public
GRANT CONTROL ON dbo.sp_Replicatestfami_data_i_sp to Public
GO
