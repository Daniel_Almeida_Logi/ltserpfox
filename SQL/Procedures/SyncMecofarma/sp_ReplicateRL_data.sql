USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_ReplicateRL_data]    Script Date: 3/11/2022 4:00:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Campos
-- Create date: 2019
-- Description:	Replicate Data From TB -RL Ext Server to local server 
-- =============================================
CREATE PROCEDURE [dbo].[sp_ReplicateRL_data]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SET XACT_ABORT ON
IF OBJECT_ID('tempdb..#RL') IS NOT NULL DROP TABLE #RL

SELECT *
INTO #RL
FROM dbo.Rep_Control (nolock)
WHERE Table_Name = 'RL' AND Processed = 0
	SELECT * from #RL

BEGIN TRY  
   -- statements that may cause exceptions
IF((SELECT COUNT(*) FROM #RL as R where r.Operation = 'I' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT 'INSERT'
	INSERT INTO [172.20.120.6\SQLEXPRESS].[mecofarma].[dbo].[RL]
	SELECT * FROM mecofarma.dbo.RL 
	WHERE rlstamp IN (SELECT Identifier from #RL AS R where r.Operation = 'I')
	
	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control as RC
	INNER JOIN #RL as T ON rc.ID = t.ID
	WHERE t.Operation = 'I' AND t.Processed = 0 and t.Table_Name ='RL'
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  

BEGIN TRY  

IF((SELECT COUNT(*) FROM #RL as R where r.Operation = 'U' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT 'UPDATE'

	UPDATE T
	SET T.[ndoc] = U.[ndoc], T.[rno] = U.[rno], T.[cdesc] = U.[cdesc], T.[nrdoc] = U.[nrdoc], T.[rec] = U.[rec], T.[datalc] = U.[datalc], T.[dataven] = U.[dataven], T.[restamp] = U.[restamp], T.[ccstamp] = U.[ccstamp], T.[cm] = U.[cm], T.[cambio] = U.[cambio], T.[eval] = U.[eval], T.[erec] = U.[erec], T.[process] = U.[process], T.[moeda] = U.[moeda], T.[rdata] = U.[rdata], T.[escrec] = U.[escrec], T.[eivav1] = U.[eivav1], T.[eivav2] = U.[eivav2], T.[eivav3] = U.[eivav3], T.[eivav4] = U.[eivav4], T.[eivav5] = U.[eivav5], T.[eivav6] = U.[eivav6], T.[eivav7] = U.[eivav7], T.[eivav8] = U.[eivav8], T.[eivav9] = U.[eivav9], T.[lordem] = U.[lordem], T.[earred] = U.[earred], T.[arred] = U.[arred], T.[enaval] = U.[enaval], T.[cheque] = U.[cheque], T.[desconto] = U.[desconto], T.[evori] = U.[evori], T.[eivavori1] = U.[eivavori1], T.[eivavori2] = U.[eivavori2], T.[eivavori3] = U.[eivavori3], T.[eivavori4] = U.[eivavori4], T.[eivavori5] = U.[eivavori5], T.[eivavori6] = U.[eivavori6], T.[eivavori7] = U.[eivavori7], T.[eivavori8] = U.[eivavori8], T.[eivavori9] = U.[eivavori9], T.[moedoc] = U.[moedoc], T.[ecambio] = U.[ecambio], T.[virs] = U.[virs], T.[evirs] = U.[evirs], T.[ousrinis] = U.[ousrinis], T.[ousrdata] = U.[ousrdata], T.[ousrhora] = U.[ousrhora], T.[usrinis] = U.[usrinis], T.[usrdata] = U.[usrdata], T.[usrhora] = U.[usrhora], T.[exportado] = U.[exportado]
	FROM [172.20.120.6\SQLEXPRESS].[mecofarma].[dbo].[RL] as T
	INNER JOIN mecofarma.dbo.RL AS U on t.rlstamp = U.rlstamp
	WHERE U.rlstamp IN (SELECT Identifier from #RL AS R where r.Operation = 'U')
	
	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control as RC
	INNER JOIN #RL as T ON rc.ID = t.ID
	WHERE t.Operation = 'U' AND t.Processed = 0 and t.Table_Name ='RL'

  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  


BEGIN TRY  
IF((SELECT COUNT(*) FROM #RL as R where r.Operation = 'D' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT 'DELETE'
	DELETE FROM [172.20.120.6\SQLEXPRESS].[mecofarma].[dbo].[RL]
	WHERE rlstamp IN (SELECT Identifier from #RL AS R where r.Operation = 'D')

	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control as RC
	INNER JOIN #RL as T ON rc.ID = t.ID
	WHERE t.Operation = 'D' AND t.Processed = 0 and t.Table_Name ='RL'
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  
SET XACT_ABORT OFF




END
GO


