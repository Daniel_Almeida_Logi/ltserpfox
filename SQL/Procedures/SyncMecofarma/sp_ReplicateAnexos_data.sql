USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_ReplicateAnexos_data]    Script Date: 3/11/2022 3:54:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- =============================================
CREATE PROCEDURE [dbo].[sp_ReplicateAnexos_data]

@server			varchar(60)
--,@site_nr		numeric(4,0)
,@site			varchar(20)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON
SET XACT_ABORT ON

declare @sql varchar(max),
	@sql1 varchar(max),
	@sql2 varchar(max)
set @sql = N'
SET NOCOUNT ON;
SET XACT_ABORT ON

IF OBJECT_ID(''tempdb..#AN'') IS NOT NULL DROP TABLE #AN

	SELECT *
INTO #AN
FROM dbo.Rep_Control_site (nolock)
WHERE Table_Name = ''anexos'' AND Processed = 0 and site='''+@site+''' and Operation = ''I''
----	SELECT * from #AN

BEGIN TRY  
   -- statements that may cause exceptions
IF((SELECT COUNT(*) FROM #AN as R where r.Operation = ''I'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''INSERT''
	INSERT INTO ['+@server+'].[mecofarma].[dbo].[anexos]
	SELECT * FROM mecofarma.dbo.anexos (nolock)
	WHERE anexosstamp IN (SELECT Identifier from #AN AS R where r.Operation = ''I'') 
	
	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control_site as RC
	INNER JOIN #AN as T ON rc.ID = t.ID
	WHERE t.Operation = ''I'' AND t.Processed = 0 and t.Table_Name =''anexos'' and t.site='''+@site+'''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  '

print @sql
execute (@sql)
GO


