USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_Replicate_campanhas_ut_lista]    Script Date: 3/11/2022 3:51:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- exec [sp_Replicate_campanhas_ut_lista] '172.20.40.6\SQLEXPRESS', 'ATLANTICO'

-- =============================================
CREATE PROCEDURE [dbo].[sp_Replicate_campanhas_ut_lista]

@server			varchar(60)
--,@site_nr		numeric(4,0)
,@site			varchar(20)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @sql varchar(max)

set @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON

delete from ['+@server+'].[mecofarma].dbo.campanhas_ut_lista

declare @contador2 numeric(15,0)
set @contador2= (select COUNT(no) from campanhas_ut_lista )
while @contador2 > 0
begin 
	DECLARE @resultb NVARCHAR(MAX), @resultb1 NVARCHAR(MAX) , @resultb2 NVARCHAR(MAX)
	select @resultb1=''insert into  ['+@server+'].[mecofarma].dbo.campanhas_ut_lista''
	SELECT @resultb = left(''select top 1000 * from campanhas_ut_lista (nolock) where (campanhas like ''''%''+STUFF((
		select ''%''+ cast(id as varchar(30)) + ''%'''' or campanhas like ''''''
		from campanhas where site = site
		FOR XML PATH('''')
		)
		,1,1,'''')
		,LEN (''select top 1000 * from campanhas_ut_lista (nolock) where campanhas like ''''%''+STUFF((
		select ''%''+ cast(id as varchar(30)) + ''%'''' or campanhas like ''''''
		from campanhas where site = site
		FOR XML PATH('''')
		)
		,1,1,''''))-19 )
	select @resultb2 = '') and no not in (select distinct no from ['+@server+'].[mecofarma].dbo.campanhas_ut_lista with (nolock))''
	--print @result1+char(13)+@result+'' ''+@result2
	execute (@resultb1+'' ''+@resultb+'' ''+@resultb2)
	set @contador2= @contador2 -1000
	print @contador2
end 
	'

print @sql


execute (@sql)
GO


