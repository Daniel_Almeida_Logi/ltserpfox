
GO
/****** Object:  StoredProcedure [dbo].[sp_ReplicateB_pagcentral_EmFalta]    Script Date: 29/06/2022 09:42:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jos� Sim�es
-- Create date: 2022
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- =============================================
if OBJECT_ID('[dbo].[sp_ReplicateB_pagcentral_EmFalta]') IS NOT NULL
	DROP PROCEDURE dbo.sp_ReplicateB_pagcentral_EmFalta
GO

create PROCEDURE [dbo].[sp_ReplicateB_pagcentral_EmFalta]

@server			varchar(60)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @sql varchar(max),
	@sql1 varchar(max),
	@sql2 varchar(max)
set @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON

BEGIN TRY  
   -- statements that may cause exceptions

  BEGIN TRAN
  PRINT ''INSERT''

  	insert into ['+@server+'].mecofarma.dbo.B_pagCentral (stamp,nrAtend,nrVendas,Total,ano,no,estab,vendedor,nome,terminal,terminal_nome,dinheiro,mb,visa,cheque,troco,oData,uData,fechado,abatido,fechaStamp,fechaUser
	,evdinheiro,epaga1,epaga2,echtotal,evdinheiro_semTroco,etroco,total_bruto,devolucoes,ntCredito,creditos,cxstamp,ssstamp,site,obs,epaga3,epaga4,epaga5,epaga6,exportado,pontosAt,campanhas
	,id_contab,valpagmoeda,moedapag,nrMotivoMov)

	select left(NEWID(),21),u_nratend,1,0.00,ftano,1,0,1,ousrinis,pno,pnome,0,0,0,0,0,fdata,fdata,1,0,'''',0
	,0,0,0,0,0,0,0,0,0,0,cxstamp,ssstamp,site,'''',0,0,0,0,0,0,''''
	,0,0,'''','''' from ['+@server+'].mecofarma.dbo.ft WITH (nolock) where u_nratend not in (select nratend from ['+@server+'].mecofarma.dbo.B_pagCentral WITH (nolock)) and ft.fdata>=getdate()-4

  COMMIT TRAN

END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  


	SET XACT_ABORT OFF

'

print @sql
execute (@sql)
GO
GRANT EXECUTE ON dbo.sp_ReplicateB_pagcentral_EmFalta to Public
GRANT CONTROL ON dbo.sp_ReplicateB_pagcentral_EmFalta to Public
GO