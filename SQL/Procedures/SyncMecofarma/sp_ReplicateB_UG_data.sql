USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_ReplicateB_UG_data]    Script Date: 3/11/2022 3:54:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Campos
-- Create date: 2019
-- Description:	Replicate Data From TB -B_UG Ext Server to local server 
-- =============================================
CREATE PROCEDURE [dbo].[sp_ReplicateB_UG_data]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SET XACT_ABORT ON
IF OBJECT_ID('tempdb..#B_UG') IS NOT NULL DROP TABLE #B_UG

SELECT *
INTO #B_UG
FROM [172.20.120.6\SQLEXPRESS].[mecofarma].dbo.Rep_Control
WHERE Table_Name = 'B_UG' AND Processed = 0
	--SELECT * from #B_UG

BEGIN TRY  
   -- statements that may cause exceptions
IF((SELECT COUNT(*) FROM #B_UG as R where r.Operation = 'I' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT 'INSERT'
	INSERT INTO [mecofarma].[dbo].[B_UG]
	SELECT * FROM [172.20.120.6\SQLEXPRESS].mecofarma.dbo.B_UG 
	WHERE ugstamp IN (SELECT Identifier from #B_UG AS R where r.Operation = 'I')
	
	UPDATE RC
	SET RC.Processed = 1
	FROM [172.20.120.6\SQLEXPRESS].[mecofarma].dbo.Rep_Control as RC
	INNER JOIN #B_UG as T ON rc.ID = t.ID
	WHERE t.Operation = 'I' AND t.Processed = 0 and t.Table_Name ='B_UG'
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  

BEGIN TRY  

IF((SELECT COUNT(*) FROM #B_UG as R where r.Operation = 'U' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT 'UPDATE'

	UPDATE T
	SET T.[nome] = U.[nome], T.[descricao] = U.[descricao], T.[ousrinis] = U.[ousrinis], T.[ousrdata] = U.[ousrdata], T.[ousrhora] = U.[ousrhora], T.[usrinis] = U.[usrinis], T.[usrdata] = U.[usrdata], T.[usrhora] = U.[usrhora]
	FROM mecofarma.dbo.B_UG AS T
	INNER JOIN [172.20.120.6\SQLEXPRESS].[mecofarma].[dbo].[B_UG] AS U on t.ugstamp = U.ugstamp
	WHERE U.ugstamp IN (SELECT Identifier from #B_UG AS R where r.Operation = 'U')
	
	UPDATE RC
	SET RC.Processed = 1
	FROM [172.20.120.6\SQLEXPRESS].mecofarma.dbo.Rep_Control as RC
	INNER JOIN #B_UG as T ON rc.ID = t.ID
	WHERE t.Operation = 'U' AND t.Processed = 0 and t.Table_Name ='B_UG'

  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  


BEGIN TRY  
IF((SELECT COUNT(*) FROM #B_UG as R where r.Operation = 'D' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT 'DELETE'
	DELETE FROM [mecofarma].[dbo].[B_UG]
	WHERE ugstamp IN (SELECT Identifier from #B_UG AS R where r.Operation = 'D')

	UPDATE RC
	SET RC.Processed = 1
	FROM [172.20.120.6\SQLEXPRESS].mecofarma.dbo.Rep_Control as RC
	INNER JOIN #B_UG as T ON rc.ID = t.ID
	WHERE t.Operation = 'D' AND t.Processed = 0 and t.Table_Name ='B_UG'
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  
	SET XACT_ABORT OFF


END
GO


