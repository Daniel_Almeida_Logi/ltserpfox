USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_ReplicateB_pagcentral_data]    Script Date: 3/11/2022 3:54:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- =============================================
CREATE PROCEDURE [dbo].[sp_ReplicateB_pagcentral_data]

@server			varchar(60)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @sql varchar(max),
	@sql1 varchar(max),
	@sql2 varchar(max)
set @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON
IF OBJECT_ID(''tempdb..#b_pagcentral'') IS NOT NULL DROP TABLE #b_pagcentral

SELECT TOP 100 *
INTO #b_pagcentral
FROM ['+@server+'].[mecofarma].dbo.Rep_Control with (nolock)
WHERE Table_Name = ''b_pagcentral'' AND Processed = 0
	SELECT * from #b_pagcentral

BEGIN TRY  
   -- statements that may cause exceptions
IF((SELECT COUNT(*) FROM #b_pagcentral as R where r.Operation = ''I'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''INSERT''

	delete from b_pagcentral WHERE stamp IN (SELECT Identifier from #b_pagcentral AS R where r.Operation = ''I'')

	INSERT INTO [mecofarma].[dbo].[b_pagcentral]
	SELECT * FROM ['+@server+'].mecofarma.dbo.b_pagcentral with (nolock)
	WHERE stamp IN (SELECT Identifier from #b_pagcentral AS R where r.Operation = ''I'')
	
	UPDATE RC
	SET RC.Processed = 1
	FROM ['+@server+'].[mecofarma].dbo.Rep_Control as RC
	INNER JOIN #b_pagcentral as T ON rc.ID = t.ID
	WHERE t.Operation = ''I'' AND t.Processed = 0 and t.Table_Name =''b_pagcentral''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  

BEGIN TRY  

IF((SELECT COUNT(*) FROM #b_pagcentral as R where r.Operation = ''U'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''UPDATE''

	UPDATE T
	SET T.[nrAtend] = U.[nrAtend], T.[nrVendas] = U.[nrVendas], T.[Total] = U.[Total], T.[ano] = U.[ano], T.[no] = U.[no], T.[estab] = U.[estab], T.[vendedor] = U.[vendedor], T.[nome] = U.[nome], T.[terminal] = U.[terminal], T.[terminal_nome] = U.[terminal_nome], T.[dinheiro] = U.[dinheiro], T.[mb] = U.[mb], T.[visa] = U.[visa], T.[cheque] = U.[cheque], T.[troco] = U.[troco], T.[oData] = U.[oData], T.[uData] = U.[uData], T.[fechado] = U.[fechado], T.[abatido] = U.[abatido], T.[fechaStamp] = U.[fechaStamp], T.[fechaUser] = U.[fechaUser], T.[evdinheiro] = U.[evdinheiro], T.[epaga1] = U.[epaga1], T.[epaga2] = U.[epaga2], T.[echtotal] = U.[echtotal], T.[evdinheiro_semTroco] = U.[evdinheiro_semTroco], T.[etroco] = U.[etroco], T.[total_bruto] = U.[total_bruto], T.[devolucoes] = U.[devolucoes], T.[ntCredito] = U.[ntCredito], T.[creditos] = U.[creditos], T.[cxstamp] = U.[cxstamp], T.[ssstamp] = U.[ssstamp], T.[site] = U.[site], T.[obs] = U.[obs], T.[epaga3] = U.[epaga3], T.[epaga4] = U.[epaga4], T.[epaga5] = U.[epaga5], T.[epaga6] = U.[epaga6], T.[exportado] = U.[exportado], T.[pontosAt] = U.[pontosAt], T.[campanhas] = U.[campanhas], T.[id_contab] = U.[id_contab], T.[valpagmoeda] = U.[valpagmoeda], T.[moedapag] = U.[moedapag]
	FROM mecofarma.dbo.b_pagcentral AS T
	INNER JOIN ['+@server+'].[mecofarma].[dbo].[b_pagcentral] AS U on t.stamp = U.stamp
	WHERE U.stamp IN (SELECT Identifier from #b_pagcentral AS R where r.Operation = ''U'')
	
	UPDATE RC
	SET RC.Processed = 1
	FROM ['+@server+'].mecofarma.dbo.Rep_Control as RC
	INNER JOIN #b_pagcentral as T ON rc.ID = t.ID
	WHERE t.Operation = ''U'' AND t.Processed = 0 and t.Table_Name =''b_pagcentral''

  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  


BEGIN TRY  
IF((SELECT COUNT(*) FROM #b_pagcentral as R where r.Operation = ''D'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''DELETE''
	DELETE FROM [mecofarma].[dbo].[b_pagcentral]
	WHERE stamp IN (SELECT Identifier from #b_pagcentral AS R where r.Operation = ''D'')

	UPDATE RC
	SET RC.Processed = 1
	FROM ['+@server+'].mecofarma.dbo.Rep_Control as RC
	INNER JOIN #b_pagcentral as T ON rc.ID = t.ID
	WHERE t.Operation = ''D'' AND t.Processed = 0 and t.Table_Name =''b_pagcentral''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  
	SET XACT_ABORT OFF

'

print @sql
execute (@sql)
GO


