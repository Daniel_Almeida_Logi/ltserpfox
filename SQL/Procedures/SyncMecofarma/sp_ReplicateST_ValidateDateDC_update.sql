/*   

sp_ReplicateST_ValidateDateDC_update
*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[sp_ReplicateST_ValidateDateDC_update]') IS NOT NULL
	DROP PROCEDURE dbo.sp_ReplicateST_ValidateDateDC_update
GO

CREATE PROCEDURE dbo.sp_ReplicateST_ValidateDateDC_update

/* WITH ENCRYPTION */
AS


DECLARE @sqlTable1 NVARCHAR(MAX) =''
		DECLARE @sqlTable2 NVARCHAR(MAX) =''

		DECLARE @ip varchar(100)
		DECLARE @sitenr int
		DECLARE @site varchar(50)
		DECLARE @design varchar(250)
		DECLARE emp_cursorIp CURSOR FOR
		SELECT sitenr,ipaddress FROM ABERTURAEMPRESA
		OPEN emp_cursorIp
		FETCH NEXT FROM emp_cursorIp INTO @sitenr,@ip  
		WHILE @@FETCH_STATUS = 0
		BEGIN

		PRINT 'BEFORE TRY'
		BEGIN TRY
			BEGIN TRAN
		  set @sqlTable1 =N'exec ['+@ip+'\SQLEXPRESS].[mecofarma].[dbo].[sp_ReplicateST_validateDate_update] ' + convert(varchar(5),@sitenr)

			print @sqlTable1

			exec( @sqlTable1)


			set @sqlTable2 =N'UPDATE st
			SET  
				st.validade =  stFarmacia.validade
			FROM  st(nolock)
				  INNER JOIN ['+ @ip+'\SQLEXPRESS].[mecofarma].[dbo].st stFarmacia(nolock) ON st.ststamp=stFarmacia.ststamp 
			WHERE  (st.validade !=stFarmacia.validade) and st.site_nr = ' + convert(varchar(5),@sitenr) +' and stFarmacia.site_nr ='  + convert(varchar(5),@sitenr)

			print @sqlTable2

			exec( @sqlTable2)

		END TRY
		BEGIN CATCH
			IF(@@TRANCOUNT > 0)
			begin
				 select @site =site  from empresa where no= @sitenr
				 PRINT 'Error CATCH Block';
				 set @design = 'ERRO a sincronizar as validades na st ' + @site
				EXEC LogsExternal.dbo.x3_servico_Inserirx3_log 'ERRO','JOBValidateDateST',@design, 'ST','',@site
			end

		END CATCH

 	COMMIT TRAN
	FETCH NEXT FROM emp_cursorIp INTO  @sitenr,@ip  
	END
	CLOSE emp_cursorIp
	DEALLOCATE emp_cursorIp

		
GO
Grant Execute On dbo.sp_ReplicateST_ValidateDateDC_update to Public
Grant Control On dbo.sp_ReplicateST_ValidateDateDC_update to Public
Go

