USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_ReplicateST_data]    Script Date: 3/11/2022 4:00:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Campos
-- Create date: 2019
-- Description:	Replicate data from table ST
-- =============================================
CREATE PROCEDURE [dbo].[sp_ReplicateST_data] 

@site_nr		numeric(4,0)

AS
BEGIN
	 	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SET XACT_ABORT ON
IF OBJECT_ID('tempdb..#ST') IS NOT NULL DROP TABLE #ST

SELECT TOP 100 *
INTO #ST
FROM dbo.Rep_Control (nolock)
inner join st on Rep_Control.Identifier=st.ststamp
WHERE Table_Name = 'st' AND Processed = 0 and st.site_nr=@site_nr --and Operation = 'U'
	--SELECT * from #ST

BEGIN TRY  
   -- statements that may cause exceptions
IF((SELECT COUNT(*) FROM #ST as R where r.Operation = 'I' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT 'INSERT'
	INSERT INTO [172.20.120.6\SQLEXPRESS].[mecofarma].[dbo].[st]
	SELECT * FROM mecofarma.dbo.st 
	WHERE ststamp IN (SELECT Identifier from #ST AS R where r.Operation = 'I')
	
	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control as RC
	INNER JOIN #ST as T ON rc.ID = t.ID
	WHERE t.Operation = 'I' AND t.Processed = 0 and t.Table_Name ='st'
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  

BEGIN TRY  

IF((SELECT COUNT(*) FROM #ST as R where r.Operation = 'U' ) > 0)
BEGIN
  BEGIN DISTRIBUTED TRANSACTION;  
  PRINT 'UPDATE'

	INSERT INTO [172.20.120.6\SQLEXPRESS].[mecofarma].[dbo].[st_replicated]
	SELECT * FROM mecofarma.dbo.st 
	WHERE ststamp IN (SELECT Identifier from #ST AS R where r.Operation = 'U')

	PRINT 'Processing changes on replication control table'
	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control as RC
	INNER JOIN #ST as T ON rc.ID = t.ID
	WHERE t.Operation = 'U' AND t.Processed = 0 and t.Table_Name ='st'

	PRINT 'Processing on External Source'
	EXEC [172.20.120.6\SQLEXPRESS].[mecofarma].[dbo].sp_SYNC_ST
	   


  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  


BEGIN TRY  
IF((SELECT COUNT(*) FROM #ST as R where r.Operation = 'D' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT 'DELETE'
	DELETE FROM [172.20.120.6\SQLEXPRESS].[mecofarma].[dbo].[st]
	WHERE ststamp IN (SELECT Identifier from #ST AS R where r.Operation = 'D')

	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control as RC
	INNER JOIN #ST as T ON rc.ID = t.ID
	WHERE t.Operation = 'D' AND t.Processed = 0 and t.Table_Name ='st'
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  
SET XACT_ABORT OFF


END
GO


