/*  lista de jobs que falharam desde de 
	EXEC up_get_jobFailLogsExternal '2022-11-09'
*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_jobFailLogsExternal]') IS NOT NULL
	drop procedure dbo.up_get_jobFailLogsExternal
go

CREATE PROCEDURE [dbo].[up_get_jobFailLogsExternal]
	@dateBegin				 DATETIME	
AS
SET NOCOUNT ON

SELECT [sql_server_agent_job_failure_id]

      ,sql_server_agent_job.sql_server_agent_job_name

      ,[sql_server_agent_instance_id]

      ,[job_start_time_utc]

      ,[job_failure_time_utc]

      ,[job_failure_step_number]

      ,[job_failure_step_name]

      ,[job_failure_message]

      ,[job_step_failure_message]

   into #jobFail

  FROM [LogsExternal].[dbo].[sql_server_agent_job_failure]

  inner join  [LogsExternal].[dbo].[sql_server_agent_job] on sql_server_agent_job.sql_server_agent_job_id = [sql_server_agent_job_failure].sql_server_agent_job_id

  WHERE job_start_time_utc>= @dateBegin

  order by job_failure_time_utc asc


  
	SELECT top 100
	   ' <u>job Name:</u> ' + sql_server_agent_job_name as jobname ,
	    '<u>data Inicio:</u>' +  CONVERT(VARCHAR(30),#jobFail.job_start_time_utc, 121)  as dataInicio,
		'<u>data Fim:</u>' +  CONVERT(VARCHAR(30),#jobFail.job_failure_time_utc, 121)   as dataFim,
		'<u>data ERRO:</u>' + #jobFail.[job_step_failure_message] as descricao
	FROM #jobFail

  GO
GRANT EXECUTE ON dbo.up_get_jobFailLogsExternal to Public
GRANT CONTROL ON dbo.up_get_jobFailLogsExternal to Public
GO