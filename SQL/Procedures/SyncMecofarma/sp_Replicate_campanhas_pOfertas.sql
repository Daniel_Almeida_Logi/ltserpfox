USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_Replicate_campanhas_pOfertas]    Script Date: 3/11/2022 3:49:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- exec [sp_Replicate_campanhas_pOfertas] '172.20.40.6\SQLEXPRESS', 1

-- =============================================
CREATE PROCEDURE [dbo].[sp_Replicate_campanhas_pOfertas]

@server			varchar(60)
,@site_nr		numeric(4,0)
--,@site			varchar(20)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @sql varchar(max)

set @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON

	delete from ['+@server+'].[mecofarma].dbo.campanhas_pOfertas
	insert into  ['+@server+'].[mecofarma].dbo.campanhas_pOfertas
	select * from campanhas_pOfertas (nolock) 
	'

print @sql

execute (@sql)
GO


