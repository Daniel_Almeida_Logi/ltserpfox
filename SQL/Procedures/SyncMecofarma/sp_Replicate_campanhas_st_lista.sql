USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_Replicate_campanhas_st_lista]    Script Date: 3/11/2022 3:50:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- exec [sp_Replicate_campanhas_st_lista] '172.20.40.6\SQLEXPRESS', 'ATLANTICO'

-- =============================================
CREATE PROCEDURE [dbo].[sp_Replicate_campanhas_st_lista]

@server			varchar(60)
--,@site_nr		numeric(4,0)
,@site			varchar(20)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @sql varchar(max)

set @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON

delete from ['+@server+'].[mecofarma].dbo.campanhas_st_lista
declare @contador1 numeric(15,0)
set @contador1= (select COUNT(ref) from campanhas_st_lista )
while @contador1 > 0
begin 
	DECLARE @resulta NVARCHAR(MAX), @resulta1 NVARCHAR(MAX) , @resulta2 NVARCHAR(MAX)
	select @resulta1=''insert into  ['+@server+'].[mecofarma].dbo.campanhas_st_lista''
	SELECT @resulta = left(''select top 1000 * from campanhas_st_lista (nolock) where (campanhas like ''''%''+STUFF((
		select ''%''+ cast(id as varchar(30)) + ''%'''' or campanhas like ''''''
		from campanhas where site = site
		FOR XML PATH('''')
		)
		,1,1,'''')
		,LEN (''select top 1000 * from campanhas_st_lista (nolock) where campanhas like ''''%''+STUFF((
		select ''%''+ cast(id as varchar(30)) + ''%'''' or campanhas like ''''''
		from campanhas where site = site
		FOR XML PATH('''')
		)
		,1,1,''''))-19 )
	select @resulta2 = '') and ref not in (select distinct ref from ['+@server+'].[mecofarma].dbo.campanhas_st_lista with (nolock))''
	print @resulta1+char(13)+@resulta+'' ''+@resulta2
	execute (@resulta1+'' ''+@resulta+'' ''+@resulta2)
	set @contador1= @contador1 -1000
	print @contador1
end 
	'

print @sql


execute (@sql)
GO


