USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_ReplicateB_utentes_data_Loja]    Script Date: 3/11/2022 3:56:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- =============================================
CREATE PROCEDURE [dbo].[sp_ReplicateB_utentes_data_Loja]

@server			varchar(60)
,@site		varchar(30)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
declare @sql varchar(max),
	@sql1 varchar(max),
	@sql2 varchar(max)
set @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON
IF OBJECT_ID(''tempdb..#TP'') IS NOT NULL DROP TABLE #TP

;with utstamp as 
(select top 200000 Date, Identifier from Rep_Control_site (nolock)
WHERE Table_Name = ''B_utentes'' AND Processed = 0 and site='''+@site+''' order by date)
select distinct top 500 identifier INTO #TP from utstamp 
SELECT * from #TP

BEGIN TRY  

IF((SELECT COUNT(*) FROM #TP as R ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''UPDATE''

	 insert into ['+@server+'].[mecofarma].[dbo].[b_utentes_aux] (utstamp,nome,no,estab,ncont,nbenef,local,nascimento,morada,telefone,obs,codpost,bino,fax,tlmvl,zona,tipo,sexo,email,codpla,despla,valipla,valipla2,nrcartao,hablit,profi,nbenef2,entfact,peso
		,altura,inactivo,nocredit,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,nome2,url,cobnao,naoencomenda,vencimento,eplafond,nib,odatraso,tabiva,tipodesc,esaldlet,clinica,modofact,desconto,alimite,particular
		,clivd,autofact,esaldo,conta,descpp,ccusto,contalet,contaletdes,contaletsac,contaainc,contaacer,tpdesc,classe,radicaltipoemp,fref,moeda,contatit,contafac,ultvenda,entpla,descp,codigop,mae,pai,notif,pacgen,tratamento
		,ninscs,csaude,drfamilia,recmtipo,recmmotivo,recmdescricao,recmdatainicio,recmdatafim,itmotivo,itdescricao,itdatainicio,itdatafim,cesd,cesdcart,cesdcp,cesdidi,cesdidp,cesdpd,cesdval,atestado,atnumero,attipo,atcp
		,atpd,atval,descpNat,codigopNat,duplicado,nomesproprios,obito,apelidos,idstamp,pais,exportado,entCompart,no_ext,noConvencao,designConvencao,id,nprescsns,direcaoTec,proprietario,moradaAlt,noAssociado,Alvara,software,ars
		,codSwift,designcom,nomcom,autorizado,removido,data_removido,tokenaprovacao,autoriza_sms,autoriza_emails,nrss,site_compart,cativa,perccativa,username,password_auto_gen, soleitura,autorizasite,password, codigoDocSPMS, obitoRNU)'
set @sql1 = N'
	select utstamp,nome,no,estab,ncont,nbenef,local,nascimento,morada,telefone,obs,codpost,bino,fax,tlmvl,zona,tipo,sexo,email,codpla,despla,valipla,valipla2,nrcartao,hablit,profi,nbenef2,entfact,peso
		,altura,inactivo,nocredit,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,nome2,url,cobnao,naoencomenda,vencimento,eplafond,nib,odatraso,tabiva,tipodesc,esaldlet,clinica,modofact,desconto,alimite,particular
		,clivd,autofact,esaldo,conta,descpp,ccusto,contalet,contaletdes,contaletsac,contaainc,contaacer,tpdesc,classe,radicaltipoemp,fref,moeda,contatit,contafac,ultvenda,entpla,descp,codigop,mae,pai,notif,pacgen,tratamento
		,ninscs,csaude,drfamilia,recmtipo,recmmotivo,recmdescricao,recmdatainicio,recmdatafim,itmotivo,itdescricao,itdatainicio,itdatafim,cesd,cesdcart,cesdcp,cesdidi,cesdidp,cesdpd,cesdval,atestado,atnumero,attipo,atcp
		,atpd,atval,descpNat,codigopNat,duplicado,nomesproprios,obito,apelidos,idstamp,pais,exportado,entCompart,no_ext,noConvencao,designConvencao,id,nprescsns,direcaoTec,proprietario,moradaAlt,noAssociado,Alvara,software,ars
		,codSwift,designcom,nomcom,autorizado,removido,data_removido,tokenaprovacao,autoriza_sms,autoriza_emails,nrss,site_compart,cativa,perccativa,username,password_auto_gen, soleitura,autorizasite,password, codigoDocSPMS, obitoRNU
		from b_utentes (nolock) WHERE utstamp  IN (SELECT Identifier from #TP AS R where Identifier not in (select utstamp from ['+@server+'].[mecofarma].[dbo].[b_utentes_aux] ))

	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control_site as RC
	WHERE rc.identifier in (select identifier from #TP) and  rc.site='''+@site+''' and rc.Table_Name =''B_utentes'' AND rc.Processed = 0

  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  
'

print @sql+@sql1
execute (@sql+@sql1)
GO


