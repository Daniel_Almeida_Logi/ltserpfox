SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[sp_fprod_ins_sync]') IS NOT NULL
	drop procedure dbo.sp_fprod_ins_sync
go

CREATE PROCEDURE [dbo].[sp_fprod_ins_sync]
	@fprodstamp varchar(25), 
	@cnp varchar(18), 
	@nome varchar(100),
	@design varchar(200),
	@grupo varchar(2),
	@cptgrpstamp varchar(25),
	@dcipt_id varchar(10),
	@dci varchar(254),
	@ousrinis varchar(50),
	@ousrdata datetime,
	@ousrhora varchar(60),
	@usrinis varchar(50),
	@usrdata datetime,
	@usrhora varchar(60),
	@barcode varchar(13),
	@dosuni text,
	@descricao varchar(200),
	@generico bit,
	@grphmgcode varchar(6),
	@grphmgdescr varchar(100),
	@tiptratdescr varchar(30),
	@ref	varchar(18),
	@cnpem varchar(18),
	@sitcomdescr varchar(8),
	@vias_admin varchar(80),
	@med_guid varchar(200),
	@u_nomerc bit,
	@psico bit

AS
BEGIN

	SET NOCOUNT ON;
	SET XACT_ABORT ON
	BEGIN TRY  

		IF (NOT EXISTS (SELECT 1 FROM fprod(nolock) WHERE cnp = @cnp))
		BEGIN
			INSERT INTO fprod (fprodstamp, cnp,nome,design,grupo,cptgrpstamp,dcipt_id,dci,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,barcode,dosuni ,descricao,generico,grphmgcode,grphmgdescr,tiptratdescr,ref,cnpem,sitcomdescr,vias_admin,med_guid,u_nomerc,psico)
			VALUES (@fprodstamp,@cnp,@nome,@design,@grupo,@cptgrpstamp,@dcipt_id,@dci,@ousrinis,@ousrdata,@ousrhora,@usrinis,@usrdata,@usrhora,@barcode,@dosuni ,@descricao,@generico,@grphmgcode,@grphmgdescr,@tiptratdescr,@ref,@cnpem,@sitcomdescr,@vias_admin,@med_guid,@u_nomerc,@psico) 
		END 
	END TRY 
	BEGIN CATCH  
	   ROLLBACK TRAN
	END CATCH  
		SET XACT_ABORT OFF
	
END

GO
GRANT EXECUTE ON dbo.sp_fprod_ins_sync to Public
GRANT CONTROL ON dbo.sp_fprod_ins_sync to Public
GO