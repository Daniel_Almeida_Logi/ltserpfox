USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_Replicate_cx_data]    Script Date: 3/11/2022 3:51:44 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- =============================================
CREATE PROCEDURE [dbo].[sp_Replicate_cx_data]

@server	varchar(60)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @sql varchar(max)
select @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON
IF OBJECT_ID(''tempdb..#SS'') IS NOT NULL DROP TABLE #CX

SELECT TOP 30 *
INTO #CX
FROM ['+@server+'].[mecofarma].dbo.Rep_Control
WHERE Table_Name = ''CX'' AND Processed = 0
	--SELECT * from #BC

BEGIN TRY  
   -- statements that may cause exceptions
IF((SELECT COUNT(*) FROM #CX as R where r.Operation = ''I'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''INSERT''
	INSERT INTO [mecofarma].[dbo].[cx] 
	(cxstamp,site,pnome,pno,cxno,cxano,ausername,auserno,dabrir,habrir,fusername,fuserno,dfechar,hfechar,fechada,causa,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,marcada,exportado,nr_vendas,nr_atend,fecho_dinheiro,fecho_mb,fecho_cheques,sacoDinheiro,saldoc,valorTpa,saldod,fecho_visa,fdStamp,diaFechado,comissaoTPA,fecho_epaga3,fecho_epaga4,fecho_epaga5,fecho_epaga6,fundocaixa)
	SELECT 
	cxstamp,site,pnome,pno,cxno,cxano,ausername,auserno,dabrir,habrir,fusername,fuserno,dfechar,hfechar,fechada,causa,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,marcada,exportado,nr_vendas,nr_atend,fecho_dinheiro,fecho_mb,fecho_cheques,sacoDinheiro,saldoc,valorTpa,saldod,fecho_visa,fdStamp,diaFechado,comissaoTPA,fecho_epaga3,fecho_epaga4,fecho_epaga5,fecho_epaga6,fundocaixa
	FROM ['+@server+'].mecofarma.dbo.cx
	WHERE cxstamp IN (SELECT Identifier from #CX AS R where r.Operation = ''I'')
	
	UPDATE RC
	SET RC.Processed = 1
	FROM ['+@server+'].[mecofarma].dbo.Rep_Control as RC
	INNER JOIN #CX as T ON rc.ID = t.ID
	WHERE t.Operation = ''I'' AND t.Processed = 0 and t.Table_Name =''CX''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  

BEGIN TRY  

IF((SELECT COUNT(*) FROM #CX as R where r.Operation = ''U'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''UPDATE''

	UPDATE T
	SET T.[site]=U.[site],T.[pnome]=U.[pnome],T.[pno]=U.[pno],T.[cxno]=U.[cxno],T.[cxano]=U.[cxano],T.[ausername]=U.[ausername],T.[auserno]=U.[auserno],T.[dabrir]=U.[dabrir],T.[habrir]=U.[habrir],T.[fusername]=U.[fusername],T.[fuserno]=U.[fuserno],T.[dfechar]=U.[dfechar],T.[hfechar]=U.[hfechar],T.[fechada]=U.[fechada],T.[causa]=U.[causa],T.[ousrinis]=U.[ousrinis],T.[ousrdata]=U.[ousrdata],T.[ousrhora]=U.[ousrhora],T.[usrinis]=U.[usrinis],T.[usrdata]=U.[usrdata],T.[usrhora]=U.[usrhora],T.[marcada]=U.[marcada],T.[exportado]=U.[exportado],T.[nr_vendas]=U.[nr_vendas],T.[nr_atend]=U.[nr_atend],T.[fecho_dinheiro]=U.[fecho_dinheiro],T.[fecho_mb]=U.[fecho_mb],T.[fecho_cheques]=U.[fecho_cheques],T.[sacoDinheiro]=U.[sacoDinheiro],T.[saldoc]=U.[saldoc],T.[valorTpa]=U.[valorTpa],T.[saldod]=U.[saldod],T.[fecho_visa]=U.[fecho_visa],T.[fdStamp]=U.[fdStamp],T.[diaFechado]=U.[diaFechado],T.[comissaoTPA]=U.[comissaoTPA],T.[fecho_epaga3]=U.[fecho_epaga3],T.[fecho_epaga4]=U.[fecho_epaga4],T.[fecho_epaga5]=U.[fecho_epaga5],T.[fecho_epaga6]=U.[fecho_epaga6],T.[fundocaixa]=U.[fundocaixa]
	FROM mecofarma.dbo.cx AS T
	INNER JOIN ['+@server+'].[mecofarma].[dbo].[cx] AS U on t.cxstamp = U.cxstamp
	WHERE U.cxstamp IN (SELECT Identifier from #CX AS R where r.Operation = ''U'')
	
	UPDATE RC
	SET RC.Processed = 1
	FROM ['+@server+'].mecofarma.dbo.Rep_Control as RC
	INNER JOIN #CX as T ON rc.ID = t.ID
	WHERE t.Operation = ''U'' AND t.Processed = 0 and t.Table_Name =''CX''

  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  


BEGIN TRY  
IF((SELECT COUNT(*) FROM #CX as R where r.Operation = ''D'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''DELETE''
	DELETE FROM [mecofarma].[dbo].[cx]
	WHERE cxstamp IN (SELECT Identifier from #CX AS R where r.Operation = ''D'')

	UPDATE RC
	SET RC.Processed = 1
	FROM ['+@server+'].mecofarma.dbo.Rep_Control as RC
	INNER JOIN #CX as T ON rc.ID = t.ID
	WHERE t.Operation = ''D'' AND t.Processed = 0 and t.Table_Name =''CX''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  
SET XACT_ABORT OFF

'

--print @sql
execute (@sql)
GO


