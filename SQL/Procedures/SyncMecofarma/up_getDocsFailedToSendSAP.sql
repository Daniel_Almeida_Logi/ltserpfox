
/******
exec up_getDocsFailedToSendSAP
******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
		
if OBJECT_ID('[dbo].[up_getDocsFailedToSendSAP]') IS NOT NULL
	drop procedure dbo.up_getDocsFailedToSendSAP
go

CREATE PROCEDURE [dbo].[up_getDocsFailedToSendSAP]
/* WITH ENCRYPTION */
AS

	if OBJECT_ID('tempdb.dbo.#tableSAPDOCS') is not null
		drop table #tableSAPDOCS



	select isnull((select top 1 description from LogsExternal.dbo.ExternalCommunicationsapLogs (nolock) where registerStamp=ft.ftstamp order by date desc),'') as erro1,
	ftstamp,fdata , ousrhora, site
	INTO #tableSAPDOCS
	from ft (nolock) 
	inner join table_sync_status_sap (nolock) on ft.ftstamp=table_sync_status_sap.regstamp
	where table_sync_status_sap.sync=0 
	and nmdoc not like ('%Proforma%') 

	order by  
	ft.fdata, ft.fno 

		--	(CASE WHEN erro1!=''  THEN 'Foi enviado para o X3 e deu o erro:  ' + erro1 + ' ftstamp: ' + ftstamp + ' ftstamp: ' + ftstamp
		--	  WHEN falta !='' THEN 'Falta integrar a tabela ' + falta 
		--
		--else '' end) AS
	SELECT 
		' <u><b>Documentos em Falta:</b></u> ' + CONVERT(VARCHAR(30), COUNT(*)) 	 as descricao,
		' <u>� os documentos que ainda faltam enviar,com e sem erros. </u> '  as info_doc,
	    '<u>data:</u>' +  CONVERT(VARCHAR(30),GETDATE(), 121)  as dataHora
	FROM #tableSAPDOCS
	UNION ALL
	SELECT top 100
	case when (erro1!='') then '<u>Erro:</u> '  + erro1
		 else '<u>Erro: </u>'  + erro1 + '| Falta:</u> ' end as descricao,
		' <u>ftstamp:</u> ' + ftstamp + '     <u>Site:</u> ' + site   as info_doc,
	    '<u>data:</u>' +  CONVERT(VARCHAR(30),fdata + ousrhora, 121)  as dataHora
	FROM #tableSAPDOCS


	if OBJECT_ID('tempdb.dbo.#tableSAPDOCS') is not null
		drop table #tableSAPDOCS

GO
GRANT EXECUTE ON dbo.up_getDocsFailedToSendSAP to Public
GRANT CONTROL ON dbo.up_getDocsFailedToSendSAP to Public
GO