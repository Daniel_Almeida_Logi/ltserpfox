
GO
/****** Object:  StoredProcedure [dbo].[sp_Replicate_b_fidel]    Script Date: 10/10/2022 14:46:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		José Costa
-- Create date: 2021
-- Description:	Replicate Data From b_fidel Ext Server to local server 
-- exec [sp_Replicate_b_fidel] '172.20.40.6\SQLEXPRESS', 'ATLANTICO'

-- =============================================
if OBJECT_ID('[dbo].[sp_Replicate_b_fidel]') IS NOT NULL
	drop procedure dbo.sp_Replicate_b_fidel
go


CREATE PROCEDURE [dbo].[sp_Replicate_b_fidel]

@server			varchar(60)
,@site			varchar(20)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @sql varchar(max)
	
set @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON
IF OBJECT_ID(''tempdb..#b_fidel'') IS NOT NULL DROP TABLE #b_fidel
IF OBJECT_ID(''tempdb..#b_fidel1'') IS NOT NULL DROP TABLE #b_fidel1
DECLARE 
	@fstamp varchar(28),
	@clstamp varchar(28),
	@clno numeric(18,0),
	@clestab numeric(18,0),
	@nrcartao varchar(50),
	@pontosAtrib numeric(18,0),
	@pontosUsa numeric(18,0),
	@inactivo bit,
	@porvalor numeric(10,2),
	@porpontos numeric(18,0),
	@validade datetime,
	@ousrdata datetime,
	@usrdata datetime,
	@ousr numeric(10,0),
	@usr  numeric(10,0),
	@atendimento  numeric(18,0),
	@nat	numeric(9,0),
	@nvd numeric(9,0),
	@valcartao numeric(15,2),
	@valcartaousado numeric(15,2),
	@valcartaohist numeric(15,2),
	@valcartaoini numeric(15,2),
	@valcartao_cativado numeric(15,2),
	@valcativo numeric(20,2),
	@id varchar(50),
	@operation char(1),
	@fstamp_aux varchar(28)

SELECT TOP 100 *
INTO #b_fidel
FROM dbo.Rep_Control_site  (nolock)
inner join b_fidel (nolock) on Rep_Control_site.Identifier=b_fidel.fstamp
WHERE Table_Name = ''b_fidel'' AND Processed = 0 and Rep_Control_site.site='''+ @site +''' 
order by id
BEGIN TRY  
   -- statements that may cause exceptions
IF((SELECT COUNT(*) FROM #b_fidel as R  ) > 0)
BEGIN
  BEGIN TRAN
	select * into #b_fidel1 from #b_fidel
	While (Select Count(*) From #b_fidel1) > 0
	Begin
		Select Top 1 @Id = ID, @fstamp_aux = fstamp,@operation= Operation From #b_fidel1
		print @Id
		SELECT
			@fstamp=fstamp,@clstamp=clstamp,@clno=clno,@clestab=clestab,@nrcartao=nrcartao,@pontosAtrib=pontosAtrib,@pontosUsa=pontosUsa,@inactivo=inactivo,@porvalor=porvalor
			,@porpontos=porpontos,@validade=validade,@ousrdata=ousrdata,@usrdata=usrdata,@ousr=ousr,@usr=usr,@atendimento=atendimento,@nat=nat,@nvd=nvd,@valcartao=valcartao,
			@valcartaousado=valcartaousado,@valcartaohist=valcartaohist,@valcartaoini=valcartaoini,@valcartao_cativado=valcartao_cativado,@valcativo=valcativo   
		FROM b_fidel (nolock) where fstamp=@fstamp_aux
		exec ['+@server+'].[mecofarma].[dbo].[sp_b_fidel_replicate] @fstamp,@clstamp,@clno,@clestab,@nrcartao,@pontosAtrib,@pontosUsa,@inactivo,@porvalor,@porpontos,@validade,@ousrdata,@usrdata,@ousr,@usr,@atendimento,@nat,@nvd,@valcartao,@valcartaousado,@valcartaohist,@valcartaoini,@valcartao_cativado,@valcativo,@operation
		Delete #b_fidel1 Where id = @Id
	End
	print ''estou aqui''
	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control_site as RC
	INNER JOIN #b_fidel as T ON rc.ID = t.ID
	WHERE  t.Processed = 0 and t.Table_Name =''b_fidel'' and RC.site='''+ @site +''' 
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  
SET XACT_ABORT OFF
'
print @sql

execute (@sql)
