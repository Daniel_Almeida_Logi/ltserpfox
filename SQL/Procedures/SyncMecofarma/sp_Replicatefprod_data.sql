
GO
/****** Object:  StoredProcedure [dbo].[sp_Replicatefrod_data]    Script Date: 12/07/2022 11:00:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if OBJECT_ID('[dbo].[sp_Replicatefprod_data]') IS NOT NULL
	DROP PROCEDURE dbo.sp_Replicatefprod_data
GO
create PROCEDURE [dbo].[sp_Replicatefprod_data]
	@server			varchar(60),
	@site_nr		numeric(4,0)
AS
SET NOCOUNT ON
declare @sql varchar(max),
	@sql1 varchar(max),
	@sql2 varchar(max),
	@site varchar(50)
	SELECT @site = site from empresa where no=@site_nr
set @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON
IF OBJECT_ID(''tempdb..#fprod'') IS NOT NULL DROP TABLE #fprod
IF OBJECT_ID(''tempdb..#fprod'') IS NOT NULL DROP TABLE #fprod1
DECLARE 
	@fprodstamp varchar(25), 
	@cnp varchar(18), 
	@nome varchar(100),
	@design varchar(200),
	@grupo varchar(2),
	@cptgrpstamp varchar(25),
	@dcipt_id varchar(10),
	@dci varchar(254),
	@ousrinis varchar(50),
	@ousrdata datetime,
	@ousrhora varchar(60),
	@usrinis varchar(50),
	@usrdata datetime,
	@usrhora varchar(60),
	@barcode varchar(13),
	@dosuni varchar(max),
	@descricao varchar(200),
	@generico bit,
	@grphmgcode varchar(6),
	@grphmgdescr varchar(100),
	@tiptratdescr varchar(30),
	@ref	varchar(18),
	@cnpem varchar(18),
	@sitcomdescr varchar(8),
	@vias_admin varchar(80),
	@med_guid varchar(200),
	@u_nomerc bit ,
	@id varchar(50),
	@psico bit
SELECT TOP 100 *
INTO #fprod
FROM dbo.Rep_Control_site  (nolock)
inner join fprod (nolock) on Rep_Control_site.Identifier=fprod.cnp
WHERE Table_Name = ''fprod'' AND Processed = 0 and Rep_Control_site.site='''+ @site +''' and Operation = ''I''
order by id
BEGIN TRY  
   -- statements that may cause exceptions
IF((SELECT COUNT(*) FROM #fprod as R where r.Operation = ''I'' ) > 0)
BEGIN
  BEGIN TRAN
	select * into #frod1 from #fprod where Operation = ''I''
	While (Select Count(*) From #frod1) > 0
	Begin
		Select Top 1 @Id = Identifier From #frod1
		print @Id
		SELECT
			@fprodstamp = fprodstamp, @cnp = cnp , @nome = nome,@design = design ,@grupo = grupo ,@cptgrpstamp = cptgrpstamp ,@dcipt_id = dcipt_id,
			@dci = dci,@ousrinis = ousrinis ,@ousrdata = ousrdata ,@ousrhora = ousrhora,@usrinis = usrinis,@usrdata = usrdata,@usrhora = usrhora,
				@barcode = barcode,@dosuni = dosuni,@descricao	=descricao,@generico=generico,@grphmgcode = grphmgcode,@grphmgdescr = grphmgdescr,
				@tiptratdescr = tiptratdescr,@ref= ref,@cnpem=cnpem,@sitcomdescr=sitcomdescr,@vias_admin=vias_admin,@med_guid=med_guid,@u_nomerc=u_nomerc,@psico =psico
		FROM fprod (nolock) where cnp=@id
		exec ['+@server+'].[mecofarma].[dbo].[sp_fprod_ins_sync] @fprodstamp, @cnp, @nome, @design, @grupo, @cptgrpstamp, @dcipt_id, @dci, @ousrinis, @ousrdata, @ousrhora, @usrinis,@usrdata, @usrhora,@barcode,@dosuni ,@descricao,@generico,@grphmgcode,@grphmgdescr,@tiptratdescr,@ref,@cnpem,@sitcomdescr,@vias_admin,@med_guid,@u_nomerc,@psico
		Delete #frod1 Where Identifier = @Id
	End
	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control_site as RC
	INNER JOIN #fprod as T ON rc.ID = t.ID
	WHERE t.Operation = ''I'' AND t.Processed = 0 and t.Table_Name =''fprod'' and RC.site='''+ @site +'''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  
SET XACT_ABORT OFF
'
print @sql
execute (@sql)


GO
GRANT EXECUTE ON dbo.sp_Replicatefprod_data to Public
GRANT CONTROL ON dbo.sp_Replicatefprod_data to Public
GO

