USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_ReplicateB_PF_data]    Script Date: 3/11/2022 3:54:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Campos
-- Create date: 2019
-- Description:	Replicate Data From TB -B_PF Ext Server to local server 
-- =============================================
CREATE PROCEDURE [dbo].[sp_ReplicateB_PF_data]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SET XACT_ABORT ON
IF OBJECT_ID('tempdb..#B_PF') IS NOT NULL DROP TABLE #B_PF

SELECT *
INTO #B_PF
FROM [172.20.120.6\SQLEXPRESS].[mecofarma].dbo.Rep_Control
WHERE Table_Name = 'B_PF' AND Processed = 0
	--SELECT * from #B_PF

BEGIN TRY  
   -- statements that may cause exceptions
IF((SELECT COUNT(*) FROM #B_PF as R where r.Operation = 'I' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT 'INSERT'
	INSERT INTO [mecofarma].[dbo].[B_PF]
	SELECT * FROM [172.20.120.6\SQLEXPRESS].mecofarma.dbo.B_PF 
	WHERE pfstamp IN (SELECT Identifier from #B_PF AS R where r.Operation = 'I')
	
	UPDATE RC
	SET RC.Processed = 1
	FROM [172.20.120.6\SQLEXPRESS].[mecofarma].dbo.Rep_Control as RC
	INNER JOIN #B_PF as T ON rc.ID = t.ID
	WHERE t.Operation = 'I' AND t.Processed = 0 and t.Table_Name ='B_PF'
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  

BEGIN TRY  

IF((SELECT COUNT(*) FROM #B_PF as R where r.Operation = 'U' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT 'UPDATE'

	UPDATE T
	SET T.[codigo] = U.[codigo], T.[resumo] = U.[resumo], T.[descricao] = U.[descricao], T.[ousrinis] = U.[ousrinis], T.[ousrdata] = U.[ousrdata], T.[ousrhora] = U.[ousrhora], T.[usrinis] = U.[usrinis], T.[usrdata] = U.[usrdata], T.[usrhora] = U.[usrhora], T.[grupo] = U.[grupo], T.[tipo] = U.[tipo], T.[visivel] = U.[visivel]
	FROM mecofarma.dbo.B_PF AS T
	INNER JOIN [172.20.120.6\SQLEXPRESS].[mecofarma].[dbo].[B_PF] AS U on t.pfstamp = U.pfstamp
	WHERE U.pfstamp IN (SELECT Identifier from #B_PF AS R where r.Operation = 'U')
	
	UPDATE RC
	SET RC.Processed = 1
	FROM [172.20.120.6\SQLEXPRESS].mecofarma.dbo.Rep_Control as RC
	INNER JOIN #B_PF as T ON rc.ID = t.ID
	WHERE t.Operation = 'U' AND t.Processed = 0 and t.Table_Name ='B_PF'

  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  


BEGIN TRY  
IF((SELECT COUNT(*) FROM #B_PF as R where r.Operation = 'D' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT 'DELETE'
	DELETE FROM [mecofarma].[dbo].[B_PF]
	WHERE pfstamp IN (SELECT Identifier from #B_PF AS R where r.Operation = 'D')

	UPDATE RC
	SET RC.Processed = 1
	FROM [172.20.120.6\SQLEXPRESS].mecofarma.dbo.Rep_Control as RC
	INNER JOIN #B_PF as T ON rc.ID = t.ID
	WHERE t.Operation = 'D' AND t.Processed = 0 and t.Table_Name ='B_PF'
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  
	SET XACT_ABORT OFF


END
GO


