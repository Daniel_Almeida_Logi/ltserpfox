USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_ReplicateBC_data]    Script Date: 3/11/2022 3:57:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- =============================================
CREATE PROCEDURE [dbo].[sp_ReplicateBC_data]

@server			varchar(60)
, @site_nr		numeric(4,0)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @sql varchar(max),
	@sql1 varchar(max),
	@sql2 varchar(max)
set @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON
IF OBJECT_ID(''tempdb..#BC'') IS NOT NULL DROP TABLE #BC

SELECT TOP 1000 *
INTO #BC
FROM dbo.Rep_Control (nolock)
inner join bc on bc.bcstamp = Rep_Control.Identifier and bc.site_nr='+convert(varchar,@site_nr)+'
WHERE Table_Name = ''BC'' AND Processed = 0
order by id
	--SELECT * from #BC

BEGIN TRY  
   -- statements that may cause exceptions
IF((SELECT COUNT(*) FROM #BC as R where r.Operation = ''I'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''INSERT''

	delete from ['+@server+'].[mecofarma].[dbo].[BC] 
	WHERE bcstamp IN (SELECT Identifier from #BC AS R where r.Operation = ''I'')

	INSERT INTO ['+@server+'].[mecofarma].[dbo].[BC]
	SELECT * FROM mecofarma.dbo.BC 
	WHERE bcstamp IN (SELECT Identifier from #BC AS R where r.Operation = ''I'')
	
	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control as RC
	INNER JOIN #BC as T ON rc.ID = t.ID
	WHERE t.Operation = ''I'' AND t.Processed = 0 and t.Table_Name =''BC''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  

BEGIN TRY  

IF((SELECT COUNT(*) FROM #BC as R where r.Operation = ''U'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''UPDATE''

	UPDATE T'
set @sql1 = N'
	SET T.[ref] = U.[ref], T.[design] = U.[design], T.[codigo] = U.[codigo], T.[ststamp] = U.[ststamp], T.[defeito] = U.[defeito], T.[emb] = U.[emb], T.[qtt] = U.[qtt], T.[ousrinis] = U.[ousrinis], T.[ousrdata] = U.[ousrdata], T.[ousrhora] = U.[ousrhora], T.[usrinis] = U.[usrinis], T.[usrdata] = U.[usrdata], T.[usrhora] = U.[usrhora], T.[marcada] = U.[marcada], T.[u_tipo] = U.[u_tipo], T.[exportado] = U.[exportado], T.[site_nr] = U.[site_nr]'
set @sql2 = N'
	FROM ['+@server+'].[mecofarma].[dbo].[BC] as T
	INNER JOIN mecofarma.dbo.BC AS U on t.bcstamp = U.bcstamp
	WHERE U.bcstamp IN (SELECT Identifier from #BC AS R where r.Operation = ''U'')
	
	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control as RC
	INNER JOIN #BC as T ON rc.ID = t.ID
	WHERE t.Operation = ''U'' AND t.Processed = 0 and t.Table_Name =''BC''

  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  

IF OBJECT_ID(''tempdb..#BC1'') IS NOT NULL DROP TABLE #BC1

SELECT TOP 1000 *
INTO #BC1
FROM dbo.Rep_Control (nolock)
inner join ['+@server+'].[mecofarma].[dbo].[BC] on bc.bcstamp = Rep_Control.Identifier and bc.site_nr='+convert(varchar,@site_nr)+'
WHERE Table_Name = ''BC'' AND Processed = 0 and Operation=''D''
order by id

BEGIN TRY  
IF((SELECT COUNT(*) FROM #BC1 as R where r.Operation = ''D'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''DELETE''
	DELETE FROM ['+@server+'].[mecofarma].[dbo].[BC]
	WHERE bcstamp IN (SELECT Identifier from #BC1 AS R where r.Operation = ''D'')

	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control as RC
	INNER JOIN #BC1 as T ON rc.ID = t.ID
	WHERE t.Operation = ''D'' AND t.Processed = 0 and t.Table_Name =''BC''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH 
SET XACT_ABORT OFF
'

print @sql+@sql1+@sql2
execute (@sql+@sql1+@sql2)
GO


