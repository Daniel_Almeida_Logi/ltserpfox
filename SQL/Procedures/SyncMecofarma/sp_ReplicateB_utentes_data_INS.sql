USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_ReplicateB_utentes_data_INS]    Script Date: 3/11/2022 3:55:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- =============================================
CREATE PROCEDURE [dbo].[sp_ReplicateB_utentes_data_INS]

@server			varchar(60)
,@site		varchar(30)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
declare @sql varchar(max),
	@sql1 varchar(max),
	@sql2 varchar(max)
set @sql = N'

SET NOCOUNT ON
SET XACT_ABORT ON

IF OBJECT_ID(''tempdb..#TP'') IS NOT NULL DROP TABLE #TP

SELECT TOP 80 *
INTO #TP
FROM dbo.Rep_Control_site (nolock)
WHERE Table_Name = ''B_utentes'' AND Processed = 0 and Operation in (''I'') and site='''+@site+'''
	--SELECT * from #TP

BEGIN TRY  
   -- statements that may cause exceptions
IF((SELECT COUNT(*) FROM #TP as R where r.Operation = ''I'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''INSERT''
	INSERT INTO ['+@server+'].[mecofarma].[dbo].[b_utentes] (utstamp,nome,no,estab,ncont,nbenef,local,nascimento,morada,telefone,obs,codpost,bino,fax,tlmvl,zona,tipo,sexo,email,codpla,despla,valipla,valipla2,nrcartao,hablit,profi,nbenef2,entfact
,peso,altura,inactivo,nocredit,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,nome2,url,cobnao,naoencomenda,vencimento,eplafond,nib,odatraso,tabiva,tipodesc,esaldlet,clinica,modofact,desconto,alimite,particular
,clivd,autofact,esaldo,conta,descpp,ccusto,contalet,contaletdes,contaletsac,contaainc,contaacer,tpdesc,classe,radicaltipoemp,fref,moeda,contatit,contafac,ultvenda,entpla,descp,codigop,mae,pai,notif,pacgen,tratamento
,ninscs,csaude,drfamilia,recmtipo,recmmotivo,recmdescricao,recmdatainicio,recmdatafim,itmotivo,itdescricao,itdatainicio,itdatafim,cesd,cesdcart,cesdcp,cesdidi,cesdidp,cesdpd,cesdval,atestado,atnumero,attipo
,atcp,atpd,atval,descpNat,codigopNat,duplicado,nomesproprios,obito,apelidos,idstamp,pais,exportado,entCompart,no_ext,noConvencao,designConvencao,id,nprescsns,direcaoTec,proprietario,moradaAlt,noAssociado,Alvara
,software,ars,codSwift,designcom,nomcom,autorizado,removido,data_removido,tokenaprovacao,autoriza_sms,autoriza_emails,nrss,site_compart,cativa,perccativa)
	SELECT utstamp,nome,no,estab,ncont,nbenef,local,nascimento,morada,telefone,obs,codpost,bino,fax,tlmvl,zona,tipo,sexo,email,codpla,despla,valipla,valipla2,nrcartao,hablit,profi,nbenef2,entfact
,peso,altura,inactivo,nocredit,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,nome2,url,cobnao,naoencomenda,vencimento,eplafond,nib,odatraso,tabiva,tipodesc,esaldlet,clinica,modofact,desconto,alimite,particular
,clivd,autofact,esaldo,conta,descpp,ccusto,contalet,contaletdes,contaletsac,contaainc,contaacer,tpdesc,classe,radicaltipoemp,fref,moeda,contatit,contafac,ultvenda,entpla,descp,codigop,mae,pai,notif,pacgen,tratamento
,ninscs,csaude,drfamilia,recmtipo,recmmotivo,recmdescricao,recmdatainicio,recmdatafim,itmotivo,itdescricao,itdatainicio,itdatafim,cesd,cesdcart,cesdcp,cesdidi,cesdidp,cesdpd,cesdval,atestado,atnumero,attipo
,atcp,atpd,atval,descpNat,codigopNat,duplicado,nomesproprios,obito,apelidos,idstamp,pais,exportado,entCompart,no_ext,noConvencao,designConvencao,id,nprescsns,direcaoTec,proprietario,moradaAlt,noAssociado,Alvara
,software,ars,codSwift,designcom,nomcom,autorizado,removido,data_removido,tokenaprovacao,autoriza_sms,autoriza_emails,nrss,site_compart,cativa,perccativa FROM mecofarma.dbo.b_utentes (nolock)
	WHERE utstamp IN (SELECT Identifier from #TP AS R where r.Operation = ''I'')
	
	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control_site as RC
	INNER JOIN #TP as T ON rc.ID = t.ID
	WHERE t.Operation = ''I'' AND t.Processed = 0 and t.Table_Name =''B_utentes''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  

SET XACT_ABORT OFF
'

print @sql
execute (@sql)
GO


