/*
	EXEC up_relatorio_docsFaltaFarmCentral 'Brisas',5
*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_relatorio_docsFaltaFarmCentral]') IS NOT NULL
drop procedure dbo.up_relatorio_docsFaltaFarmCentral
go

create procedure up_relatorio_docsFaltaFarmCentral
@site			varchar(60),
@dia			int

/* WITH ENCRYPTION */
AS
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#saveValor'))
		DROP TABLE #saveValor

	DECLARE @tabela		VARCHAR(4)	 = 'FT'
	DECLARE @descr		VARCHAR(254) = ''
	DECLARE @valor		int			 = 0
	DECLARE @data		DATE		 =  CONVERT(DATE,GETDATE())
	DECLARE @stampVerificacao varchar(36) = ''
	DECLARE @siteIp		VARCHAR(100)
	DECLARE @sql		VARCHAR(MAX)

	CREATE TABLE #saveValor(
		valorFalta int
	)

	SELECT 
		@siteIp = ipaddress
	FROM	
		AberturaEmpresa(NOLOCK)
	WHERE 
		AberturaEmpresa.site = @site

	SELECT 
		@stampVerificacao = stamp
	FROM
		sincFarmaciaCentral(NOLOCK)
	WHERE
		site = @site AND
		data =  CONVERT(DATE,GETDATE())
				
		BEGIN TRY

			set @sql = N'
				SELECT� 
					COALESCE(ftLocal.ftstamp,'''')
�					FROM 
						['+@siteIp+'\SQLEXPRESS].mecofarma.dbo.ft  ftLocal WITH(nolock)
�					LEFT JOIN 
						[172.20.90.17].mecofarma.dbo.ft ftDc (NOLOCK) ON ftLocal.ftstamp= ftDc.ftstamp
					WHERE ftLocal.ousrdata>='+CONVERT(varchar(100),CONVERT(date,GETDATE()-@dia))+' 
						AND ftLocal.ousrdata<=GETDATE() -1 
						AND�ftLocal.site = ''' + convert(varchar(100),@site) +'''
						AND ftDc.ftstamp IS NULL�
						AND ftLocal.nmdoc NOT LIKE (''%Proforma%'')�'	
				
				INSERT INTO #saveValor
				EXEC (@sql)
				
				SELECT 
					@valor = valorFalta
				FROM 
					#saveValor

				SET @descr = 'N� Documentos Que Ex�ste Na Farm�cia E N�o Ex�ste Na Central: '	

				IF(@stampVerificacao = '')
				BEGIN	
					insert sincFarmaciaCentral(stamp, tabela, valor, site, descricao, data)
					VALUES(LEFT(newid(),36), @tabela, @valor, @site, @descr + @valor, @data)
				END
				ELSE
				BEGIN
					UPDATE sincFarmaciaCentral set descricao =  CONCAT(@descr,@valor), valor = @valor where stamp = @stampVerificacao
				END
			END TRY	
			Begin Catch
				IF(@stampVerificacao = '')
				BEGIN
					set @descr = 'N�o Foi Possivel Obter Respostas Por Parte Da Farm�cia ' + @site
					set @data  = CONVERT(DATE,GETDATE())

					insert sincFarmaciaCentral(stamp, tabela, valor, site, descricao, data)
					VALUES(LEFT(newid(),36), @tabela, 0, @site, @descr, @data)
				END
			END Catch

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#saveValor'))
		DROP TABLE #saveValor

GO
Grant Execute On dbo.up_relatorio_docsFaltaFarmCentral to Public
Grant Control On dbo.up_relatorio_docsFaltaFarmCentral to Public
Go

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

