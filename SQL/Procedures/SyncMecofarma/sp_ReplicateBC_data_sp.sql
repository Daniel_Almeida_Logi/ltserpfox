USE [mecofarma]
GO
/****** Object:  StoredProcedure [dbo].[sp_ReplicateBC_data_sp]    Script Date: 10/24/2024 12:37:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		José Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- =============================================
ALTER PROCEDURE [dbo].[sp_ReplicateBC_data_sp]

@server			varchar(60)
, @site_nr		numeric(4,0)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	DECLARE @ip NVARCHAR(100)
    DECLARE @site NVARCHAR(100)

    -- Remove \SQLEXPRESS
    SET @ip = LEFT(@server, CHARINDEX('\', @server) - 1)

    -- Busca o site baseado no IP address
    SELECT @site = site
    FROM mecofarma.dbo.AberturaEmpresa
    WHERE ipaddress = @ip and associado=0

declare @sql varchar(max),
	@sql1 varchar(max),
	@sql2 varchar(max)
set @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON
IF OBJECT_ID(''tempdb..#BC'') IS NOT NULL DROP TABLE #BC
IF OBJECT_ID(''tempdb..#BC1'') IS NOT NULL DROP TABLE #BC1
IF OBJECT_ID(''tempdb..#BC2'') IS NOT NULL DROP TABLE #BC2
IF OBJECT_ID(''tempdb..#BC3'') IS NOT NULL DROP TABLE #BC3

declare @tipo		varchar(50)
,@bcstamp	varchar(50)
,@ref		varchar(50)
,@design	varchar(50)
,@codigo	varchar(50)
,@ststamp	varchar(50)
,@defeito	bit
,@emb		varchar(50)
,@qtt		numeric(15,0)
,@ousrinis	varchar(50)
,@ousrdata	datetime
,@ousrhora	varchar(50)
,@usrinis	varchar(50)
,@usrdata	datetime
,@usrhora	varchar(50)
,@marcada	bit
,@u_tipo	varchar(50)
,@exportado	bit
,@site_nr	numeric(15,0)
,@Id		varchar(50)

DECLARE @idError int 
DECLARE @messageError varchar(max)

SELECT TOP 1000 *
INTO #BC
FROM dbo.Rep_Control (nolock)
inner join bc on bc.bcstamp = Rep_Control.Identifier and bc.site_nr='+convert(varchar(3),@site_nr)+'
WHERE Table_Name = ''BC'' AND Processed = 0
order by id
	SELECT * from #BC

BEGIN TRY  
   -- statements that may cause exceptions
IF((SELECT COUNT(*) FROM #BC as R where r.Operation = ''I'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''INSERT''

	select * into #BC1 from #BC where Operation = ''I''
	
	While (Select Count(*) From #BC1) > 0
	Begin
		Select Top 1 @Id = Identifier From #BC1
		print @Id
		SELECT @bcstamp	= bcstamp,@ref=ref,@design=design,@codigo=codigo,@ststamp=ststamp,@defeito=defeito,@emb=emb,@qtt=qtt,@ousrinis=ousrinis,@ousrdata=ousrdata,@ousrhora=ousrhora,@usrinis=usrinis,@usrdata=usrdata,@usrhora=usrhora,@marcada=marcada,@u_tipo=u_tipo,@exportado=exportado,@site_nr=site_nr FROM dbo.bc (nolock) where bcstamp=@id
		exec ['+@server+'].[mecofarma].[dbo].[sp_codbarras_sync] ''INS'', @bcstamp,@ref,@design,@codigo,@ststamp,@defeito,@emb,@qtt,@ousrinis,@ousrdata,@ousrhora,@usrinis,@usrdata,@usrhora,@marcada,@u_tipo,@exportado,@site_nr
		Delete #BC1 Where Identifier = @Id
	End
	
	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control as RC
	INNER JOIN #BC as T ON rc.ID = t.ID
	WHERE t.Operation = ''I'' AND t.Processed = 0 and t.Table_Name =''BC''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
   set @idError = ERROR_NUMBER()
   set @messageError = ERROR_MESSAGE()
   EXEC usp_insert_LogsExternal_table_errorLogs ''Sincronização cod barras INSERT'',''MECOCENTRAL'',''' + @site + ''', @idError, @messageError
END CATCH  

BEGIN TRY  

IF((SELECT COUNT(*) FROM #BC as R where r.Operation = ''U'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''UPDATE''
'
set @sql1 = N'
	select * into #BC2 from #BC where Operation = ''U''
	While (Select Count(*) From #BC2) > 0
	Begin
		Select Top 1 @Id = Identifier From #BC2
		SELECT @bcstamp	= bcstamp,@ref=ref,@design=design,@codigo=codigo,@ststamp=ststamp,@defeito=defeito,@emb=emb,@qtt=qtt,@ousrinis=ousrinis,@ousrdata=ousrdata,@ousrhora=ousrhora,@usrinis=usrinis,@usrdata=usrdata,@usrhora=usrhora,@marcada=marcada,@u_tipo=u_tipo,@exportado=exportado,@site_nr=site_nr FROM dbo.bc (nolock) where bcstamp=@id
		exec ['+@server+'].[mecofarma].[dbo].[sp_codbarras_sync] ''UPD'', @bcstamp,@ref,@design,@codigo,@ststamp,@defeito,@emb,@qtt,@ousrinis,@ousrdata,@ousrhora,@usrinis,@usrdata,@usrhora,@marcada,@u_tipo,@exportado,@site_nr
		Delete #BC2 Where Identifier = @Id
	End
	'
set @sql2 = N'
	
	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control as RC
	INNER JOIN #BC as T ON rc.ID = t.ID
	WHERE t.Operation = ''U'' AND t.Processed = 0 and t.Table_Name =''BC''

  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
   set @idError = ERROR_NUMBER()
   set @messageError = ERROR_MESSAGE()
   EXEC usp_insert_LogsExternal_table_errorLogs ''Sincronização cod barras UPDATE'',''MECOCENTRAL'',''' + @site + ''', @idError, @messageError
END CATCH  

IF OBJECT_ID(''tempdb..#BC4'') IS NOT NULL DROP TABLE #BC4

SELECT TOP 1000 *
INTO #BC4
FROM dbo.Rep_Control (nolock)
inner join ['+@server+'].[mecofarma].[dbo].[BC] on bc.bcstamp = Rep_Control.Identifier and bc.site_nr='+convert(varchar,@site_nr)+'
WHERE Table_Name = ''BC'' AND Processed = 0 and Operation=''D''
order by id

BEGIN TRY  
IF((SELECT COUNT(*) FROM #BC4 as R where r.Operation = ''D'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''DELETE''
	select * into #BC3 from #BC4 where Operation = ''D''
	While (Select Count(*) From #BC3) > 0
	Begin
		Select Top 1 @Id = Identifier From #BC3
		SELECT @bcstamp	= bcstamp,@ref=ref,@design=design,@codigo=codigo,@ststamp=ststamp,@defeito=defeito,@emb=emb,@qtt=qtt,@ousrinis=ousrinis,@ousrdata=ousrdata,@ousrhora=ousrhora,@usrinis=usrinis,@usrdata=usrdata,@usrhora=usrhora,@marcada=marcada,@u_tipo=u_tipo,@exportado=exportado,@site_nr=site_nr FROM #BC3 (nolock) where bcstamp=@id
		exec ['+@server+'].[mecofarma].[dbo].[sp_codbarras_sync] ''DEL'', @bcstamp,@ref,@design,@codigo,@ststamp,@defeito,@emb,@qtt,@ousrinis,@ousrdata,@ousrhora,@usrinis,@usrdata,@usrhora,@marcada,@u_tipo,@exportado,@site_nr
		Delete #BC3 Where Identifier = @Id
	End

	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control as RC
	INNER JOIN #BC4 as T ON rc.ID = t.ID
	WHERE t.Operation = ''D'' AND t.Processed = 0 and t.Table_Name =''BC''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
   set @idError = ERROR_NUMBER()
   set @messageError = ERROR_MESSAGE()
   EXEC usp_insert_LogsExternal_table_errorLogs ''Sincronização cod barras UPDATE'',''MECOCENTRAL'',''' + @site + ''', @idError, @messageError
END CATCH 
SET XACT_ABORT OFF
'

print @sql+@sql1+@sql2
execute (@sql+@sql1+@sql2)
