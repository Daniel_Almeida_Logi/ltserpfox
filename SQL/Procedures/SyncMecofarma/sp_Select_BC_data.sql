USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_Select_BC_data]    Script Date: 3/11/2022 4:01:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Campos
-- Create date: 2019
-- Description:	Replicate data from table ST
-- =============================================
CREATE PROCEDURE [dbo].[sp_Select_BC_data] 

@site_nr		numeric(4,0)

AS
BEGIN
	select * from bc (nolock)  where site_nr=@site_nr
END
GO


