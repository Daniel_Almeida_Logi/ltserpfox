USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_ReplicateDocumentos_encomendas_alter_site]    Script Date: 3/11/2022 3:58:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- =============================================
create PROCEDURE [dbo].[sp_ReplicateDocumentos_encomendas_alter_site]

@server			varchar(60)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @sql varchar(max),
	@sql1 varchar(max),
	@sql2 varchar(max),
	@sql3 varchar(max),
	@sql4 varchar(max)
set @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON
	IF OBJECT_ID(''tempdb..#BO'') IS NOT NULL DROP TABLE #BO
	IF OBJECT_ID(''tempdb..#BO2'') IS NOT NULL DROP TABLE #BO2
	IF OBJECT_ID(''tempdb..#BO22'') IS NOT NULL DROP TABLE #BO22

	SELECT top 30 *
	INTO #BO
	FROM ['+@server+'].[mecofarma].[dbo].[dbo.Rep_Control_site] (nolock)
	WHERE Table_Name = ''BO'' AND Processed = 0 and operation=''U''
		SELECT * from #BO

	BEGIN TRY  

	IF((SELECT COUNT(*) FROM #BO as R where r.Operation = ''U'' ) > 0)
	BEGIN
	  BEGIN TRAN
	  PRINT ''UPDATE'''
set @sql2 = N'
		-- Update BO
		UPDATE T
		SET T.[site] = U.[site]
		FROM mecofarma.dbo.BO as T
		INNER JOIN ['+@server+'].[mecofarma].[dbo].[BO] AS U on t.bostamp = U.bostamp
		WHERE U.bostamp IN (SELECT Identifier from #BO AS R where r.Operation = ''U'')

		SELECT bostamp
		INTO #BO2
		FROM mecofarma.dbo.BO
		WHERE bostamp in (SELECT Identifier from #BO AS R where r.Operation = ''U'')
	
		delete from mecofarma.dbo.BI 
		where bostamp in (select bostamp from #BO2)

		insert into mecofarma.dbo.BI 
		select * FROM ['+@server+'].[mecofarma].[dbo].[BI] where mecofarma.dbo.BI.bostamp in (select bostamp from #BO2) 

		UPDATE RC
		SET RC.Processed = 1
		FROM ['+@server+'].[mecofarma].[dbo].[Rep_Control_site] as RC
		INNER JOIN #BO as T ON rc.ID = t.ID
		WHERE t.Operation = ''U'' AND t.Processed = 0 and t.Table_Name =''BO'' 

	  COMMIT TRAN
	END
	END TRY 
	BEGIN CATCH  
	   ROLLBACK TRAN
	END CATCH  

	SET XACT_ABORT OFF'

set @sql4 = N'

'

print @sql+@sql1+@sql2+@sql3+@sql4
execute (@sql+@sql1+@sql2+@sql3+@sql4)
GO


