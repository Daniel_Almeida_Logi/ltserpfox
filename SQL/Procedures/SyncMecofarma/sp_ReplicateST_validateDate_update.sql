
/*  call sql jobs 

exec sp_ReplicateST_validateDate_update 1 


*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if OBJECT_ID('[dbo].[sp_ReplicateST_validateDate_update]') IS NOT NULL
	DROP PROCEDURE dbo.sp_ReplicateST_validateDate_update
GO

CREATE PROCEDURE [dbo].[sp_ReplicateST_validateDate_update]
		@sitenr			INT
	
AS
SET NOCOUNT ON

  	UPDATE st
	SET  
		st.validade = (CASE WHEN CONVERT(varchar, st.validade, 112) = '19000101'
						THEN stcentral.validade
					WHEN st.stock <= 0
						THEN stcentral.validade
					WHEN st.validade > stcentral.validade
						THEN stcentral.validade
					ELSE st.validade
				END)
    FROM  st(nolock)
		  INNER JOIN [172.20.90.17].mecofarma.dbo.st stcentral(nolock) ON st.ststamp=stcentral.ststamp 
	WHERE  (st.validade !=stcentral.validade) and st.site_nr = @sitenr and stcentral.site_nr = @sitenr
GO
GRANT EXECUTE ON dbo.sp_ReplicateST_validateDate_update to Public
GRANT CONTROL ON dbo.sp_ReplicateST_validateDate_update to Public
GO

