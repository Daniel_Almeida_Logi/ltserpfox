USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_ReplicateRE_Loja_DC]    Script Date: 3/11/2022 3:59:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Campos + Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From RE -  escrit�rio para loja
-- =============================================
CREATE PROCEDURE [dbo].[sp_ReplicateRE_Loja_DC]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SET XACT_ABORT ON
IF OBJECT_ID('tempdb..#RE') IS NOT NULL DROP TABLE #RE

SELECT top 20 *
INTO #RE
FROM [172.20.120.6\SQLEXPRESS].[mecofarma].[dbo].Rep_Control
WHERE Table_Name = 'RE' AND Processed = 0
	--SELECT * from #RE

BEGIN TRY  
   -- statements that may cause exceptions
IF((SELECT COUNT(*) FROM #RE as R where r.Operation = 'I' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT 'INSERT'
	INSERT INTO re
	SELECT * FROM [172.20.120.6\SQLEXPRESS].[mecofarma].[dbo].RE
	WHERE restamp IN (SELECT Identifier from #RE AS R where r.Operation = 'I')

	insert into rl
	SELECT * FROM [172.20.120.6\SQLEXPRESS].[mecofarma].[dbo].RL
	WHERE restamp IN (SELECT Identifier from #RE AS R where r.Operation = 'I')
	
	UPDATE RC
	SET RC.Processed = 1
	FROM [172.20.120.6\SQLEXPRESS].[mecofarma].[dbo].Rep_Control as RC
	INNER JOIN #RE as T ON rc.ID = t.ID
	WHERE t.Operation = 'I' AND t.Processed = 0 and t.Table_Name ='RE'
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  

--BEGIN TRY  

--IF((SELECT COUNT(*) FROM #RE as R where r.Operation = 'U' ) > 0)
--BEGIN
--  BEGIN TRAN
--  PRINT 'UPDATE'

--	UPDATE T
--	SET T.[nmdoc] = U.[nmdoc], T.[rno] = U.[rno], T.[rdata] = U.[rdata], T.[nome] = U.[nome], T.[total] = U.[total], T.[etotal] = U.[etotal], T.[ndoc] = U.[ndoc], T.[no] = U.[no], T.[morada] = U.[morada], T.[local] = U.[local], T.[codpost] = U.[codpost], T.[ncont] = U.[ncont], T.[reano] = U.[reano], T.[olcodigo] = U.[olcodigo], T.[telocal] = U.[telocal], T.[totalmoeda] = U.[totalmoeda], T.[moeda] = U.[moeda], T.[desc1] = U.[desc1], T.[fref] = U.[fref], T.[ccusto] = U.[ccusto], T.[ncusto] = U.[ncusto], T.[contado] = U.[contado], T.[process] = U.[process], T.[cobranca] = U.[cobranca], T.[fin] = U.[fin], T.[finv] = U.[finv], T.[procdata] = U.[procdata], T.[vdata] = U.[vdata], T.[zona] = U.[zona], T.[ollocal] = U.[ollocal], T.[vendedor] = U.[vendedor], T.[vendnm] = U.[vendnm], T.[segmento] = U.[segmento], T.[efinv] = U.[efinv], T.[edifcambio] = U.[edifcambio], T.[difcambio] = U.[difcambio], T.[tipo] = U.[tipo], T.[pais] = U.[pais], T.[estab] = U.[estab], T.[earred] = U.[earred], T.[memissao] = U.[memissao], T.[arred] = U.[arred], T.[cobrador] = U.[cobrador], T.[rota] = U.[rota], T.[cheque] = U.[cheque], T.[intid] = U.[intid], T.[nome2] = U.[nome2], T.[echtotal] = U.[echtotal], T.[site] = U.[site], T.[pnome] = U.[pnome], T.[pno] = U.[pno], T.[cxstamp] = U.[cxstamp], T.[cxusername] = U.[cxusername], T.[ssstamp] = U.[ssstamp], T.[ssusername] = U.[ssusername], T.[evdinheiro] = U.[evdinheiro], T.[modop1] = U.[modop1], T.[epaga1] = U.[epaga1], T.[modop2] = U.[modop2], T.[epaga2] = U.[epaga2], T.[virs] = U.[virs], T.[moeda2] = U.[moeda2], T.[faccstamp] = U.[faccstamp], T.[ousrinis] = U.[ousrinis], T.[ousrdata] = U.[ousrdata], T.[ousrhora] = U.[ousrhora], T.[usrinis] = U.[usrinis], T.[usrdata] = U.[usrdata], T.[usrhora] = U.[usrhora], T.[cbbno] = U.[cbbno], T.[u_backoff] = U.[u_backoff], T.[u_nratend] = U.[u_nratend], T.[finvmoeda] = U.[finvmoeda], T.[evirs] = U.[evirs], T.[chdata] = U.[chdata], T.[clbanco] = U.[clbanco], T.[clcheque] = U.[clcheque], T.[totol2] = U.[totol2], T.[etotol2] = U.[etotol2], T.[ollocal2] = U.[ollocal2], T.[contado2] = U.[contado2], T.[telocal2] = U.[telocal2], T.[eacerto] = U.[eacerto], T.[exportado] = U.[exportado]
--	FROM [172.20.120.6\SQLEXPRESS].[mecofarma].[dbo].[RE] as T
--	INNER JOIN mecofarma.dbo.RE AS U on t.restamp = U.restamp
--	WHERE U.restamp IN (SELECT Identifier from #RE AS R where r.Operation = 'U')
	
--	UPDATE RC
--	SET RC.Processed = 1
--	FROM dbo.Rep_Control as RC
--	INNER JOIN #RE as T ON rc.ID = t.ID
--	WHERE t.Operation = 'U' AND t.Processed = 0 and t.Table_Name ='RE'

--  COMMIT TRAN
--END
--END TRY 
--BEGIN CATCH  
--   ROLLBACK TRAN
--END CATCH  


--BEGIN TRY  
--IF((SELECT COUNT(*) FROM #RE as R where r.Operation = 'D' ) > 0)
--BEGIN
--  BEGIN TRAN
--  PRINT 'DELETE'
--	DELETE FROM [172.20.120.6\SQLEXPRESS].[mecofarma].[dbo].[RE]
--	WHERE restamp IN (SELECT Identifier from #RE AS R where r.Operation = 'D')

--	UPDATE RC
--	SET RC.Processed = 1
--	FROM dbo.Rep_Control as RC
--	INNER JOIN #RE as T ON rc.ID = t.ID
--	WHERE t.Operation = 'D' AND t.Processed = 0 and t.Table_Name ='RE'
--  COMMIT TRAN
--END
--END TRY 
--BEGIN CATCH  
--   ROLLBACK TRAN
--END CATCH  
SET XACT_ABORT OFF




END
GO


