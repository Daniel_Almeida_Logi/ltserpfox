USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_ReplicateB_utentes_data_UPD_DEL]    Script Date: 3/11/2022 3:56:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- =============================================
CREATE PROCEDURE [dbo].[sp_ReplicateB_utentes_data_UPD_DEL]

@server			varchar(60)
,@site		varchar(30)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.


declare @sql varchar(max),
	@sql1 varchar(max),
	@sql2 varchar(max)
set @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON
IF OBJECT_ID(''tempdb..#TP'') IS NOT NULL DROP TABLE #TP

SELECT  distinct top 50 identifier
INTO #TP
FROM dbo.Rep_Control_site (nolock)
WHERE Table_Name = ''B_utentes'' AND Processed = 0 and Operation in (''U'') and site='''+@site+'''
	SELECT * from #TP

BEGIN TRY  

IF((SELECT COUNT(*) FROM #TP as R ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''UPDATE''

	UPDATE T'
set @sql1 = N'
	SET T.[nome] = U.[nome], T.[no] = U.[no], T.[estab] = U.[estab], T.[ncont] = U.[ncont], T.[nbenef] = U.[nbenef], T.[local] = U.[local], T.[nascimento] = U.[nascimento], T.[morada] = U.[morada], T.[telefone] = U.[telefone], T.[obs] = U.[obs], T.[codpost] = U.[codpost], T.[bino] = U.[bino], T.[tlmvl] = U.[tlmvl], T.[zona] = U.[zona], T.[tipo] = U.[tipo], T.[sexo] = U.[sexo], T.[email] = U.[email], T.[codpla] = U.[codpla], T.[despla] = U.[despla], T.[valipla] = U.[valipla], T.[valipla2] = U.[valipla2], T.[nrcartao] = U.[nrcartao], T.[nbenef2] = U.[nbenef2], T.[entfact] = U.[entfact], T.[inactivo] = U.[inactivo], T.[nocredit] = U.[nocredit], T.[ousrinis] = U.[ousrinis], T.[ousrdata] = U.[ousrdata], T.[ousrhora] = U.[ousrhora], T.[usrinis] = U.[usrinis], T.[usrdata] = U.[usrdata], T.[usrhora] = U.[usrhora], T.[nome2] = U.[nome2], T.[url] = U.[url], T.[cobnao] = U.[cobnao], T.[naoencomenda] = U.[naoencomenda], T.[vencimento] = U.[vencimento], T.[eplafond] = U.[eplafond], T.[nib] = U.[nib], T.[tabiva] = U.[tabiva], T.[tipodesc] = U.[tipodesc], T.[modofact] = U.[modofact], T.[desconto] = U.[desconto], T.[alimite] = U.[alimite], T.[particular] = U.[particular], T.[clivd] = U.[clivd], T.[esaldo] = U.[esaldo], T.[conta] = U.[conta], T.[descpp] = U.[descpp], T.[tpdesc] = U.[tpdesc], T.[classe] = U.[classe], T.[radicaltipoemp] = U.[radicaltipoemp], T.[fref] = U.[fref], T.[moeda] = U.[moeda], T.[entpla] = U.[entpla], T.[descp] = U.[descp], T.[codigop] = U.[codigop], T.[mae] = U.[mae], T.[pai] = U.[pai], T.[notif] = U.[notif], T.[tratamento] = U.[tratamento], T.[ninscs] = U.[ninscs], T.[itmotivo] = U.[itmotivo], T.[itdescricao] = U.[itdescricao], T.[itdatainicio] = U.[itdatainicio], T.[itdatafim] = U.[itdatafim], T.[cesd] = U.[cesd], T.[cesdcart] = U.[cesdcart], T.[cesdcp] = U.[cesdcp], T.[cesdidi] = U.[cesdidi], T.[cesdidp] = U.[cesdidp], T.[cesdpd] = U.[cesdpd], T.[cesdval] = U.[cesdval], T.[descpNat] = U.[descpNat], T.[codigopNat] = U.[codigopNat], T.[duplicado] = U.[duplicado], T.[idstamp] = U.[idstamp], T.[pais] = U.[pais], T.[exportado] = U.[exportado], T.[entCompart] = U.[entCompart], T.[no_ext] = U.[no_ext], T.[id] = U.[id], T.[nprescsns] = U.[nprescsns], T.[moradaAlt] = U.[moradaAlt], T.[designcom] = U.[designcom], T.[nomcom] = U.[nomcom], T.[nrss] = U.[nrss], T.[site_compart] = U.[site_compart], T.[cativa] = U.[cativa], T.[perccativa] = U.[perccativa]'
	
set @sql2 = N'
	FROM ['+@server+'].[mecofarma].[dbo].[b_utentes] as T
	INNER JOIN mecofarma.dbo.b_utentes AS U (nolock) on t.utstamp = U.utstamp
	WHERE U.utstamp IN (SELECT Identifier from #TP AS R )
	
	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control_site as RC
	--INNER JOIN #TP as T ON rc.identifier = t.ID
	WHERE rc.Operation = ''U'' AND rc.Processed = 0 and rc.Table_Name =''B_utentes'' and  rc.site='''+@site+'''
	and rc.identifier in (select identifier from #TP)

  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  

 
SET XACT_ABORT OFF
'

print @sql+@sql1+@sql2
execute (@sql+@sql1+@sql2)
GO


