USE [mecofarma]
GO

/****** Object:  StoredProcedure [dbo].[sp_Replicate_ft_compart_result_data]    Script Date: 3/11/2022 3:52:48 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - ft_compart Ext Server to local server 
-- =============================================
CREATE PROCEDURE [dbo].[sp_Replicate_ft_compart_result_data]

@server			varchar(60)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @sql varchar(max),
	@sql1 varchar(max),
	@sql2 varchar(max)
set @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON
IF OBJECT_ID(''tempdb..#ft_compart_result'') IS NOT NULL DROP TABLE #ft_compart_result

SELECT TOP 100 *
INTO #ft_compart_result
FROM ['+@server+'].[mecofarma].dbo.Rep_Control
WHERE Table_Name = ''ft_compart_result'' AND Processed = 0
order by id
	SELECT * from #ft_compart_result

BEGIN TRY  
   -- statements that may cause exceptions
IF((SELECT COUNT(*) FROM #ft_compart_result as R where r.Operation in (''I'',''U'') ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''INSERT''

	delete from ft_compart_result WHERE token IN (SELECT Identifier from #ft_compart_result AS R where r.Operation in (''I'',''U''))

	INSERT INTO [mecofarma].[dbo].[ft_compart_result]
	SELECT * FROM ['+@server+'].mecofarma.dbo.ft_compart_result 
	WHERE token IN (SELECT distinct Identifier from #ft_compart_result AS R where r.Operation in (''I'',''U''))
	
	UPDATE RC
	SET RC.Processed = 1
	FROM ['+@server+'].[mecofarma].dbo.Rep_Control as RC
	INNER JOIN #ft_compart_result as T ON rc.ID = t.ID
	WHERE t.Operation in (''I'',''U'') AND t.Processed = 0 and t.Table_Name =''ft_compart_result''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  

BEGIN TRY  
IF((SELECT COUNT(*) FROM #ft_compart_result as R where r.Operation = ''D'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''DELETE''
	DELETE FROM [mecofarma].[dbo].[ft_compart_result]
	WHERE token IN (SELECT Identifier from #ft_compart_result AS R where r.Operation = ''D'')

	UPDATE RC
	SET RC.Processed = 1
	FROM ['+@server+'].[mecofarma].dbo.Rep_Control as RC
	INNER JOIN #ft_compart_result as T ON rc.ID = t.ID
	WHERE t.Operation = ''D'' AND t.Processed = 0 and t.Table_Name =''ft_compart_result''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  
	SET XACT_ABORT OFF

'

print @sql
execute (@sql)
GO


