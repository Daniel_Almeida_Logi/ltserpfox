USE [mecofarma]
GO
/****** Object:  StoredProcedure [dbo].[sp_Replicate_B_US_data]    Script Date: 11/10/2022 13:02:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Campos + Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - BC  headquarters to store
-- =============================================
ALTER PROCEDURE [dbo].[sp_Replicate_B_US_data]

@server			varchar(60)
,@site		varchar(20)

AS
declare @sql varchar(max),
	@sql1 varchar(max),
	@sql2 varchar(max),
	@sql3 varchar(max)
set @sql = N'

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SET XACT_ABORT ON
IF OBJECT_ID(''tempdb..#BUS'') IS NOT NULL DROP TABLE #BUS

SELECT TOP 15 *
INTO #BUS
FROM dbo.Rep_Control_site (nolock)
WHERE Table_Name = ''B_US'' AND Processed = 0 and site='''+@site+'''
order by id
	SELECT * from #BUS

BEGIN TRY  
   -- statements that may cause exceptions
IF((SELECT COUNT(*) FROM #BUS as R where r.Operation = ''I'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''INSERT''
	delete from ['+@server+'].[mecofarma].[dbo].[b_us] where userno in (SELECT userno FROM mecofarma.dbo.B_us
																		WHERE usstamp IN (SELECT Identifier from #BUS AS R where r.Operation = ''I''))
	
	INSERT INTO ['+@server+'].[mecofarma].[dbo].[b_us] 
	SELECT * FROM mecofarma.dbo.B_us
	WHERE usstamp IN (SELECT Identifier from #BUS AS R where r.Operation = ''I'')
	
	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control_site as RC
	INNER JOIN #BUS as T ON rc.ID = t.ID
	WHERE t.Operation = ''I'' AND t.Processed = 0 and t.Table_Name =''B_US''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  

BEGIN TRY  

IF((SELECT COUNT(*) FROM #BUS as R where r.Operation = ''U'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''UPDATE'''
set @sql1 = N'
	UPDATE T
	SET T.[iniciais] = U.[iniciais],T.[username] = U.[username],T.[userpass] = U.[userpass],T.[userno] = U.[userno],T.[grupo] = U.[grupo],T.[ugstamp] = U.[ugstamp],T.[inactivo] = U.[inactivo],T.[supermsg] = U.[supermsg],T.[superact] = U.[superact],T.[d500] = U.[d500],T.[nome] = U.[nome],T.[tipo] = U.[tipo],T.[morada] = U.[morada],T.[local] = U.[local],T.[codpost] = U.[codpost],T.[tlmvl] = U.[tlmvl],T.[email] = U.[email],T.[dtnasc] = U.[dtnasc],T.[sexo] = U.[sexo],T.[nacion] = U.[nacion],T.[tratamento] = U.[tratamento],T.[obs] = U.[obs],T.[ncont] = U.[ncont],T.[drno] = U.[drno],T.[drestab] = U.[drestab],T.[drcedula] = U.[drcedula],T.[drinscri] = U.[drinscri],T.[drordem] = U.[drordem],T.[drclprofi] = U.[drclprofi],T.[ousrdata] = U.[ousrdata],T.[ousrhora] = U.[ousrhora],T.[ousrinis] = U.[ousrinis],T.[usrdata] = U.[usrdata],T.[usrhora] = U.[usrhora],T.[usrinis] = U.[usrinis],T.[lingua] = U.[lingua],T.[loja] = U.[loja],T.[departamento] = U.[departamento],T.[area] = U.[area],T.[HonorarioDeducao] = U.[HonorarioDeducao],T.[HonorarioValor] = U.[HonorarioValor],T.[HonorarioBi] = U.[HonorarioBi],T.[HonorarioTipo] = U.[HonorarioTipo],T.[retencao] = U.[retencao],T.[aceita_termos] = U.[aceita_termos],T.[dt_aceita_termos] = U.[dt_aceita_termos],T.[primacesso] = U.[primacesso],T.[password] = U.[password], T.[data_alt_pw]=U.[data_alt_pw]'
set @sql2 = N'
	FROM ['+@server+'].[mecofarma].[dbo].[b_us] as T
	INNER JOIN mecofarma.dbo.B_us AS U on t.usstamp = U.usstamp
	WHERE U.usstamp IN (SELECT Identifier from #BUS AS R where r.Operation = ''U'')
	
	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control_site as RC
	INNER JOIN #BUS as T ON rc.ID = t.ID
	WHERE t.Operation = ''U'' AND t.Processed = 0 and t.Table_Name =''B_US''

  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  


BEGIN TRY  
IF((SELECT COUNT(*) FROM #BUS as R where r.Operation = ''D'' ) > 0)
BEGIN
  BEGIN TRAN
  PRINT ''DELETE''
	DELETE FROM ['+@server+'].[mecofarma].[dbo].[b_us]
	WHERE usstamp IN (SELECT Identifier from #BUS AS R where r.Operation = ''D'')

	UPDATE RC
	SET RC.Processed = 1
	FROM dbo.Rep_Control_site as RC
	INNER JOIN #BUS as T ON rc.ID = t.ID
	WHERE t.Operation = ''D'' AND t.Processed = 0 and t.Table_Name =''B_US''
  COMMIT TRAN
END
END TRY 
BEGIN CATCH  
   ROLLBACK TRAN
END CATCH  
SET XACT_ABORT OFF

'
print @sql+@sql1+@sql2
execute (@sql+@sql1+@sql2)