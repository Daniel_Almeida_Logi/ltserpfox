/****** Object:  StoredProcedure [dbo].[up_update_Prods_PBI]    Script Date: 14/11/2022 15:35:30 
exec up_update_Prods_PBI 'POWERBI', 'F13156A','Loja 99'
 
******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_update_Prods_PBI]') IS NOT NULL    drop procedure up_update_Prods_PBI
go

CREATE procedure [dbo].[up_update_Prods_PBI]
@ipdestino varchar (100),
@bddestino varchar (100),
@site varchar (100)

/* with encryption */
AS
SET NOCOUNT OFF

/* Elimina Tabelas */


IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#temp_prods'))
DROP TABLE #temp_prods 




DECLARE @DELETE varchar (MAX) = ''
DECLARE @DELETECHAR varchar (max) = ''
DECLARE @INSERT varchar (max) = ''
DECLARE @destino varchar (200) = '['+@ipdestino+'].['+@bddestino+']'
DECLARE @insertSql varchar(max) = ''
DECLARE @tabelaorigem VARCHAR (100) = 'st'
DECLARE @tabeladestino VARCHAR (100) = 'PowerBI_prods'
DECLARE @site_nr varchar (3)= 0
	IF @site != ''
	set @site_nr = (select no from empresa (nolock)	where empresa.site = @site)


EXEC  up_retorna_insert_sql_dinamico @tabelaorigem,@tabeladestino,@ipdestino,@bddestino, @insertSql OUTPUT


set @DELETE = 'DELETE from	'+@destino+'.dbo.'+@tabeladestino

IF @site != ''
BEGIN
set @DELETE = @DELETE +' where site_nr = '+@site_nr 
END
PRINT @DELETE
EXECUTE (@DELETE)

set @INSERT = @INSERT + CHAR(13) + CHAR(10) + @insertSql 

IF @site != ''
BEGIN
set @INSERT = @INSERT +' where site_nr = '+@site_nr 
END

PRINT @INSERT
EXECUTE (@INSERT)

set @DELETECHAR = 'DELETE from	'+@destino+'.dbo.'+@tabeladestino +' where ref like ''%'' + CHAR(160) + ''%'' and inactivo = 1'

PRINT @DELETECHAR
EXECUTE (@DELETECHAR)

IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#temp_prods'))
DROP TABLE #temp_prods 

GO
GRANT EXECUTE on dbo.up_update_Prods_PBI TO PUBLIC
GRANT Control on dbo.up_update_Prods_PBI TO PUBLIC
GO




