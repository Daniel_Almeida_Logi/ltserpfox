
/****** Object:  StoredProcedure [dbo].[up_Export_Pagamentos_PBI]  

SP para exportar pagamentos de fornecedores para a BD do PBI

  Script Date: 29/06/2023 10:51:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'up_Export_Pagamentos_PBI'))
DROP procedure [dbo].[up_Export_Pagamentos_PBI]
GO
CREATE procedure [dbo].[up_Export_Pagamentos_PBI]
	@ipdestino		varchar(100)
	,@bddestino		varchar(100)

/*
select * from [POWERBI].[F13156A].dbo.PowerBI_dadosComprasBase with (nolock)
exec [up_Export_Pagamentos_PBI] 'POWERBI','F13156A'

*/
/* with encryption */
AS
SET NOCOUNT OFF

/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#pagamentos'))
		DROP TABLE #pagamentos

SELECT pl.[postamp]
      ,pl.[plstamp]
	  ,po.site
	  ,po.[nome]
      ,pl.[rno]
      ,pl.[cdesc]
      ,pl.[adoc]
      ,pl.[rec]
      ,pl.[datalc]
      ,pl.[dataven]
      ,pl.[fcstamp]
      ,pl.[cm]
      ,pl.[cambio]
      ,pl.[eval]
      ,pl.[erec]
      ,pl.[process]
      ,pl.[moeda]
      ,pl.[rdata]
      ,pl.[escrec]
      ,pl.[eivav1]
      ,pl.[eivav2]
      ,pl.[eivav3]
      ,pl.[eivav4]
      ,pl.[eivav5]
      ,pl.[eivav6]
      ,pl.[eivav7]
      ,pl.[eivav8]
      ,pl.[eivav9]
      ,pl.[lordem]
      ,pl.[earred]
      ,pl.[arred]
      ,pl.[enaval]
      ,pl.[desconto]
      ,pl.[evori]
      ,pl.[virs]
      ,pl.[evirs]
      ,pl.[ecambio]
      ,pl.[crend]
      ,pl.[moedoc]
      ,pl.[ousrinis]
      ,pl.[ousrdata]
      ,pl.[ousrhora]
      ,pl.[usrinis]
      ,pl.[usrdata]
      ,pl.[usrhora]
      ,pl.[ivatx1]
      ,pl.[ivatx2]
      ,pl.[ivatx3]
      ,pl.[ivatx4]
      ,pl.[ivatx5]
      ,pl.[ivatx6]
      ,pl.[ivatx7]
      ,pl.[ivatx8]
      ,pl.[ivatx9]
      ,pl.[fcorigem]
      ,pl.[exportado]
	  into #pagamentos
  FROM [dbo].[pl] (nolock)  join [dbo].[po] (nolock)
	on pl.postamp = po.postamp


	DECLARE @INSERT varchar(max)
	DECLARE @DELETE varchar (max)
	DECLARE @destino varchar (200) = '['+@ipdestino+'].['+@bddestino+']'
	DECLARE @tabeladestino VARCHAR (50) = 'PowerBI_Pagamentos'	

	SET @DELETE=' 
	DELETE from '+@destino+'.dbo.'+@tabeladestino+' '

	PRINT @DELETE
	EXEC (@DELETE)

	SET @INSERT=' 
	INSERT into '+@destino+'.dbo.'+@tabeladestino+' '
	
	SET @INSERT= @INSERT + 'select #pagamentos.* from #pagamentos 
							LEFT JOIN '+@destino+'.dbo.'+@tabeladestino+' a with (nolock)
								ON #pagamentos.plstamp COLLATE DATABASE_DEFAULT = a.stamplinhapagam COLLATE DATABASE_DEFAULT
							where a.stamplinhapagam COLLATE DATABASE_DEFAULT IS NULL' 	
	PRINT @INSERT
	EXEC (@INSERT)

	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#pagamentos'))
		DROP TABLE #pagamentos