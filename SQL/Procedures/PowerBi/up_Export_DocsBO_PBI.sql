/****** Object:  StoredProcedure [dbo].[up_Export_DocsBO_PBI] 

exec [up_Export_DocsBO_PBI] '20200101','20240113','POWERBI','F13156A',''

SP para exportar docs da BO a BD do PBI:
	(neste momento: 'Reserva de Cliente', 'Registo Faltas','Encomenda a Fornecedor','Acerto de Stock','Trf entre Armazéns','Devol. a Fornecedor','Acerto de Stock','Consumo Interno')

   Script Date: 16/02/2023 18:01:45 JB
   Last Change: 07/01/2025 14:55:00 JB
   ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'up_Export_DocsBO_PBI'))
DROP procedure [dbo].[up_Export_DocsBO_PBI]
GO
CREATE procedure [dbo].[up_Export_DocsBO_PBI]
	@dataIni		datetime
	,@dataFim		datetime
	,@ipdestino		varchar(100)
	,@bddestino		varchar(100)
	,@site			varchar (50)
/*
exec [up_Export_DocsBO_PBI] 'POWERBI','F13156A'
*/
/* with encryption */
AS
SET NOCOUNT OFF

/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DocsBO'))
		DROP TABLE #DocsBO
		


SELECT distinct
		bi.[bostamp]
	  ,bi.[bistamp]
	  ,bo.site						AS 'Site'
	  ,bo.[dataobra]
      ,bi.[nmdos]
	  ,bi.[obrano]
	  ,bo.[fechada]					AS 'DocFechado'
	  ,bo.[nome]
	  ,CONCAT(bo.[no],'.',bo.estab)	as 'NCliDep'
	  ,bo.[vendnm]
      ,bi.[ref]
      ,bi.[design]
      ,bi.[qtt]
      ,bi.[iva]
      ,bi.[tabiva]
      ,bi.[armazem]
	  ,bi.[u_mquebra]
      ,bi.[lobs]
      ,bi.[fechada]					as 'LinhaFechada'
      ,bi.[ar2mazem]
      ,bi.[local]
      ,bi.[morada]
      ,bi.[codpost]
      ,bi.[lote]
      ,bi.[epu]
      ,bi.[edebito]
      ,bi.[eprorc]
      ,bi.[epcusto]
      ,bi.[ettdeb]
      ,bi.[adoc]
      ,bi.[obistamp]
      ,bi.[oobistamp]
      ,bi.[familia]
      ,bi.[desconto]
      ,bi.[desc2]
      ,bi.[desc3]
      ,bi.[desc4]
      ,bi.[ccusto]
      ,bi.[num1]
      ,bi.[pbruto]
      ,bi.[ecustoind]
      ,bi.[u_bencont]
      ,bi.[u_psicont]
      ,bi.[ivaincl]
      ,bi.[fno]
      ,bi.[nmdoc]
      ,bi.[ndoc]
      ,bi.[oftstamp]
      ,bi.[fdata]
      ,bi.[qtRec]
      ,bi.[diploma]
      ,bi.[descval]
      ,bi.[exepcaoTrocaMed]
	  into #DocsBO
  FROM [dbo].[bi] (nolock) inner join [dbo].[bo] (nolock)
	on bi.bostamp = bo.bostamp
	where bo.nmdos in ('Reserva de Cliente'	
						,'Registo Faltas'
						,'Encomenda a Fornecedor'
						,'Acerto de Stock'
						,'Trf entre Armazéns'
						,'Devol. a Fornecedor'
						,'Acerto de Stock'
						,'Consumo Interno')
		and bo.site = CASE WHEN @site != '' THEN @site ELSE bo.site END

	DECLARE @INSERT varchar(max)
	DECLARE @DELETE varchar (max)
	DECLARE @destino varchar (200) = '['+@ipdestino+'].['+@bddestino+']'
	DECLARE @tabeladestino VARCHAR (50) = 'PowerBI_DocsInternos'	

	SET @DELETE = '
	DELETE from '+@destino+'.dbo.'+@tabeladestino+' WHERE StampRegisto IN (
		SELECT a.StampRegisto FROM '+@destino+'.dbo.'+@tabeladestino+' a
		INNER JOIN bo b 
			ON a.NrDoc = b.obrano AND YEAR(a.DataDoc) = b.boano AND a.NomeDoc = b.nmdos
		WHERE a.StampRegisto != b.bostamp
		) 

	DELETE a from '+@destino+'.dbo.'+@tabeladestino+' a INNER JOIN bo b 
			ON a.NrDoc = b.obrano AND YEAR(a.DataDoc) = b.boano AND a.NomeDoc = b.nmdos
					where b.USRDATA between '''+CONVERT(varchar,@dataini,112)
						+''' and '''+CONVERT(varchar,@datafim,112)+'''' 

	PRINT @DELETE
	EXEC (@DELETE)

	SET @INSERT=' 
	INSERT into '+@destino+'.dbo.'+@tabeladestino+' '
	
	SET @INSERT= @INSERT + 'select #DocsBO.* from #DocsBO 
							LEFT JOIN '+@destino+'.dbo.'+@tabeladestino+' b with (nolock)
								ON #DocsBO.bistamp COLLATE DATABASE_DEFAULT = b.StampLinhaRegisto COLLATE DATABASE_DEFAULT
							where b.StampLinhaRegisto COLLATE DATABASE_DEFAULT IS NULL ' 

	PRINT @INSERT
	EXEC (@INSERT)

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DocsBO'))
		DROP TABLE #DocsBO
