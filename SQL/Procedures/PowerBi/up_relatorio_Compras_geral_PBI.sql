

/****** Object:  StoredProcedure [dbo].[up_relatorio_Compras_geral_PBI]    Script Date: 14/11/2022 18:15:48 
Criação de dados de compras para utilizar no PBI

exec up_relatorio_Compras_geral_PBI '20200101','20250123','Loja 1','POWERBI','F01079A'

******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_relatorio_Compras_geral_PBI]') IS NOT NULL
	drop procedure dbo.up_relatorio_Compras_geral_PBI
go

CREATE procedure [dbo].[up_relatorio_Compras_geral_PBI]
	@dataIni		datetime
	,@dataFim		datetime
	,@site			varchar(55)
	,@ipdestino varchar (100)
	,@bddestino varchar (100)
/* with encryption */
AS
SET NOCOUNT OFF
	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosComprasBase'))
		DROP TABLE #dadosComprasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
		DROP TABLE #dadosFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRefs'))
		DROP TABLE #dadosRefs
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
		DROP TABLE #dadosSt	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosComprasBaseFiltro'))
		DROP TABLE #dadosComprasBaseFiltro	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosComprasBaseQuotaLab'))
		DROP TABLE #dadosComprasBaseQuotaLab	

	/* Calc Loja e Armazens */
	DECLARE @site_nr as varchar (50) = 0

	BEGIN
	IF @site != ''
	set @site_nr = (select no from empresa (nolock)	where empresa.site = @site)
					
	END


	select 
		armazem
	into
		#EmpresaArm
	From
		empresa
		inner join empresa_arm on empresa.no = empresa_arm.empresa_no
	where
		empresa.site = case when @site != '' then @site ELSE empresa.site end

	/* Calc familias */
	Select
		distinct ref, nome
	into
		#dadosFamilia
	From
		stfami (nolock)
	/* Calc Ref ST */
	Select 
		ref
	into
		#dadosRefs
	from
		st (nolock)
	Where
		site_nr = CASE WHEN @site != '' THEN @site_nr ELSE st.site_nr END
	/* Calc Informaçăo Produto */
	Select
		st.ref
		,st.design
		,usr1
		,u_lab
		,departamento = isnull(a.descr ,'')
		,seccao = isnull(b.descr,'')
		,categoria = isnull(c.descr,'')
		,famfamilia =  isnull(d.descr,'')
		,dispdescr = ISNULL(dispdescr,'')
		,generico = ISNULL(generico,CONVERT(bit,0))
		,psico = ISNULL(psico,CONVERT(bit,0))
		,benzo = ISNULL(benzo,CONVERT(bit,0))
		,protocolo = ISNULL(protocolo,CONVERT(bit,0))
		,dci = ISNULL(dci,'')
		,grphmgcode = ISNULL(grphmgcode,'')
		,valDepartamento = a.id
		,valSeccao = b.id
		,valCategoria = c.id
		,valFamfamilia = d.id
		,st.site_nr
	INTO
		#dadosSt
	From
		st (nolock)
		left join fprod (nolock) on st.ref = fprod.cnp
		left join grande_mercado_hmr (nolock) A on A.id = st.u_depstamp
		left join mercado_hmr (nolock) B on B.id = st.u_secstamp
		left join categoria_hmr (nolock) C on C.id = st.u_catstamp
		left join segmento_hmr (nolock) D on D.id = st.u_segstamp
	Where
		site_nr = CASE WHEN @site != '' THEN @site_nr ELSE st.site_nr END
	/* Dados Base Vendas  */
	create table #dadosComprasBase (
		fnstamp varchar(25)
		,docdata datetime
		,dataven datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(50)
		,doccode numeric(6,0)
		,adoc varchar(50)
		,docnome varchar(50)
		,cm numeric(10,0)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
		,u_validade datetime
        ,pcp numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,pcl numeric(19,6)
		,pct numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)
		,Obs varchar (254)
		,usrdata datetime
	)
    insert #dadosComprasBase
    exec up_relatorio_compras_base_detalhe_PBI @dataIni, @dataFim, @site

	/* Preparar Result Set */
	Select distinct
		fnstamp
		,loja
		,docdata
		,dataven
		,[no]
		,estab
		,tipo
		,doccode
		,adoc
		,docnome
		,cm
		,left(isnull(#dadosComprasBase.[ref],''),18) as ref
		,left(isnull(#dadosComprasBase.[design],''),100) as design
		,left(isnull(#dadosComprasBase.[familia],''),50) as familia
		,left(isnull(#dadosfamilia.[nome],''),50) as faminome
		,[u_epvp]
		,[u_validade]
		,[pcp]
		,[pcl]
		,[pct]
		,[iva]
		,[qtt]
		,[etiliquido]
		,[etiliquidoSiva]
		,[desconto]
		,descvalor
		,[ousrhora]
		,[ousrinis]
		,left([usr1],100) as usr1
		,left([u_lab],100) as u_lab
		,left([departamento],100) as departamento
		,left([seccao],100) as seccao
		,left([categoria],100) as categoria
		,left([famfamilia],100) as famfamilia
		,left([dispdescr],100) as dispdescr
		,[generico]
		,[psico]
		,[benzo]
		,[protocolo]
		,[dci]
		,isnull([valDepartamento],'') as valDepartamento
		,isnull([valSeccao],'') as valSeccao
		,isnull([valCategoria],'') as valCategoria
		,isnull([valFamfamilia],'') as valFamfamilia
		,Obs 
		,usrdata
	into
		#dadosComprasBaseFiltro
	From
		#dadosComprasBase 
		left Join #dadosSt on #dadosComprasBase.ref = #dadosSt.ref and #dadosComprasBase.loja_nr = #dadosSt.site_nr
		left join #dadosFamilia on #dadosComprasBase.familia = #dadosfamilia.ref

	
	DECLARE @stm varchar(5000)
	DECLARE @destino varchar (200) = '['+@ipdestino+'].['+@bddestino+']'
	DECLARE @tabeladestino VARCHAR (50) = 'PowerBI_dadosComprasBase'
	DECLARE @insertSql varchar(max) = ''
	DECLARE @tabelaorigem VARCHAR (100) = '#dadosComprasBaseFiltro'	
	EXEC  up_retorna_insert_sql_dinamico @tabelaorigem,@tabeladestino,@ipdestino,@bddestino, @insertSql OUTPUT
	SET @stm='
		DELETE b from '+@destino+'.dbo.'+@tabeladestino+' b
			where USRDATA between '''+CONVERT(varchar,@dataini,112)+''' and '''+CONVERT(varchar,@datafim,112)+''''

	if @site != ''
		set @stm = @stm + ' AND b.Loja ='''+@site+''''

	set @stm = @stm + @insertSql + 'LEFT JOIN '+@destino+'.dbo.'+@tabeladestino+' b with (nolock) 
									ON a.fnstamp COLLATE DATABASE_DEFAULT = b.fnstamp COLLATE DATABASE_DEFAULT
								where b.fnstamp COLLATE DATABASE_DEFAULT IS NULL'
	if @site != ''
		set @stm = @stm + ' AND a.Loja ='''+@site+''''

	PRINT @stm
	EXEC (@stm)
	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosComprasBase'))
		DROP TABLE #dadosComprasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
		DROP TABLE #dadosFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRefs'))
		DROP TABLE #dadosRefs
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
		DROP TABLE #dadosSt
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.##dadosComprasBaseFiltro'))
		DROP TABLE #dadosComprasBaseFiltro	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosComprasBaseQuotaLab'))
		DROP TABLE #dadosComprasBaseQuotaLab	
