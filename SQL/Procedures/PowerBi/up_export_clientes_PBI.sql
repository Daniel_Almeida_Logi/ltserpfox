
/****** Object:  StoredProcedure [dbo].[up_export_clientes_PBI]    Script Date: 14/11/2022 17:57:47 
Criação de tabela de informação de clientes para usar no PBI

exec up_export_clientes_PBI 'POWERBI','F13156A'
******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'up_export_clientes_PBI'))
DROP procedure [dbo].[up_export_clientes_PBI]
GO
CREATE procedure [dbo].[up_export_clientes_PBI]
	@ipdestino varchar (100),
	@bddestino varchar (100)

	
/* with encryption */
AS
SET NOCOUNT OFF

DECLARE @stm varchar(5000)
DECLARE @sql2 varchar (4000) = ''
DECLARE @destino varchar (200) = '['+@ipdestino+'].['+@bddestino+']'
DECLARE @insertSql varchar(max) = ''
DECLARE @tabelaorigem VARCHAR (50) = 'b_utentes'
DECLARE @tabeladestino VARCHAR (50) = 'PowerBI_clientes'

EXEC  up_retorna_insert_sql_dinamico @tabelaorigem,@tabeladestino,@ipdestino,@bddestino, @insertSql OUTPUT
    --PRINT  @insertSql

SET @stm='
DELETE a from '+@destino+'.dbo.'+@tabeladestino+' a '+CHAR(13)+CHAR(10)

SET @stm = @stm + CHAR(13) + CHAR(10) + @insertSql 

PRINT @stm+@sql2
EXEC (@stm+@sql2)




