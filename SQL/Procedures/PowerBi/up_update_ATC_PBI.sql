/****** Object:  StoredProcedure [dbo].[up_update_ATC_Prods_PBI]    Script Date: 14/11/2022 15:35:30 
exec up_update_ATC_PBI 'POWERBI', 'F13156A'
 
******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_update_ATC_PBI]') IS NOT NULL    drop procedure up_update_ATC_PBI
go

CREATE procedure [dbo].[up_update_ATC_PBI]

@ipdestino varchar (100),
@bddestino varchar (100)

/* with encryption */
AS
SET NOCOUNT OFF
 
/* Elimina Tabelas */

IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#temp_Atc'))
DROP TABLE #temp_Atc 

DECLARE @sql varchar (max) = ''
DECLARE @destino varchar (200) = '['+@ipdestino+'].['+@bddestino+']'
DECLARE @tabeladestino VARCHAR (50) = 'PowerBI_atc'


set @sql = '

SELECT	d.[cnp]
		,d.[CNPEM]
		,d.grphmgcode
		,d.grphmgdescr
		,a.atccode
		,a.descricao
        ,a.[atcmini]
        ,a.[Ac_farmac]
        ,a.[Indica]
		,ROW_NUMBER() OVER(PARTITION BY d.[cnp] ORDER BY d.[cnp] ASC)  RowN 
	into  #temp_Atc
	from B_atc a(nolock)
	inner join B_atcfp	b with (nolock)	on a.atccode = b.atccode 
	inner join st		c with (nolock) 	on b.cnp = c.ref
	inner join fprod	d with (nolock)	on c.ref = d.ref
	
DELETE from '+@destino+'.dbo.'+@tabeladestino+' ' 

set @sql = @sql + '

INSERT INTO '+@destino+'.dbo.'+@tabeladestino+'  
           ([cnp]
			,[CNPEM]
			,grphmgcode
			,grphmgdescr
			,atccode
			,descricao
			,[atcmini]
			,[Ac_farmac]
			,[Indica])

	SELECT	[cnp]
			,[CNPEM]
			,grphmgcode
			,grphmgdescr
			,atccode
			,descricao
			,[atcmini]
			,[Ac_farmac]
			,[Indica]
	from #temp_Atc
	where RowN = 1'
	


PRINT @sql 
EXECUTE (@sql )

IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#temp_Atc'))
DROP TABLE #temp_Atc 

GO
GRANT EXECUTE on dbo.up_update_ATC_PBI TO PUBLIC
GRANT Control on dbo.up_update_ATC_PBI TO PUBLIC
GO



	