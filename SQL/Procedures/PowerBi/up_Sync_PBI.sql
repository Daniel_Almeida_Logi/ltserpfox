
/****** Object:  StoredProcedure [dbo].[up_Sync_PBI]    Script Date: 28//12/2022 15:57:47 
SP para chamar todas as SPs de sincronização do PBI

--ip_errortable - SQLLOGITOOLS
--bd_errortable - LTSMSB
--ipdestino		- POWERBI
--bddestino		- F13156A (bd cliente no PBIserver)
--dataIni		- CONVERT(varchar, getdate()-180, 112) (nr de dias anteriores a sincronizar)
--dataFim		- CONVERT(varchar, getdate(), 112)
--site			- Loja X	

DECLARE @ip_errortable varchar (100) = 'SQLLOGITOOLS'
DECLARE @bd_errortable varchar (100) = 'LTSMSB'
DECLARE @ipdestino varchar (100) = 'POWERBI'
DECLARE @bddestino varchar (100) = 'F13156A'
DECLARE @dataIni varchar (100) = '20201201'
DECLARE @dataFim varchar (100) = '20250130'
DECLARE @site	varchar(55) = ''

exec up_Sync_PBI @ip_errortable
		,@bd_errortable
		,@ipdestino 
		,@bddestino 
		,@dataIni		
		,@dataFim		
		,@site	


		select * from [SQLLOGITOOLS].[LTSMSB].[dbo].[Errors_PBI] order by id desc

******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'up_Sync_PBI'))
DROP procedure [dbo].[up_Sync_PBI]
GO
CREATE procedure [dbo].[up_Sync_PBI]

	@ip_errortable varchar (100)
	,@bd_errortable varchar (100)
	,@ipdestino varchar (100)
	,@bddestino varchar (100)
	,@dataIni		varchar (100)
	,@dataFim		varchar (100)
	,@site			varchar(55)

/* with encryption */
AS
SET NOCOUNT OFF

BEGIN TRY  
	exec up_relatorio_vendas_geral_PBI @dataIni,@dataFim,@site,@ipdestino,@bddestino
	exec up_relatorio_Compras_geral_PBI @dataIni,@dataFim,@site,@ipdestino,@bddestino
	exec up_export_clientes_PBI @ipdestino,@bddestino
	exec up_export_fornecedores_PBI @ipdestino,@bddestino
	exec up_relatorio_pagamentos_geral_PBI @dataIni,@dataFim,@ipdestino,@bddestino
	exec up_update_Prods_PBI @ipdestino,@bddestino,@site
	exec up_update_ATC_PBI @ipdestino,@bddestino
	exec up_Export_DocsBO_PBI @dataIni,@dataFim,@ipdestino,@bddestino,@site
	exec up_Export_Movimentos_PBI @dataIni,@dataFim,@ipdestino,@bddestino,@site
	exec up_Export_Pagamentos_PBI @ipdestino,@bddestino
	exec up_Export_CTLTRCT @ipdestino,@bddestino,@site

	DECLARE @DBName NVARCHAR(128) = DB_NAME();
	DECLARE @SQL1 NVARCHAR(MAX);
	
	SET @SQL1 = N'EXEC (''' + 'EXEC ' + @bddestino + '.dbo.up_gerais_runDatabasesClean_PBI' + ''') AT ' + QUOTENAME(@ipdestino);
	--EXEC ('EXEC DatabaseName.dbo.ProcedureName') AT POWERBI;
	
	PRINT @sql1
	EXEC sp_executesql @SQL1;

END TRY  

BEGIN CATCH 
	DECLARE @sql varchar (max) 
	set @sql=	'INSERT INTO ['+@ip_errortable+'].['+@bd_errortable+'].[dbo].[Errors_PBI]
	           ([bd_destino]
	           ,[site]
	           ,[datainicio]
	           ,[datafim]
	           ,[data_erro]
	           ,[msg_erro])
			 SELECT '+
	           ''''+@bddestino+''','''
	           +@site+''','''
	           +CONVERT(varchar,@dataIni,120)+''','''
	           +CONVERT(varchar,@dataFim,120)+''',
	           GETDATE(),
	           ERROR_MESSAGE ()'
	print @sql
	exec (@sql)
END CATCH



GO
GRANT EXECUTE on dbo.up_Sync_PBI TO PUBLIC
GRANT Control on dbo.up_Sync_PBI TO PUBLIC
GO



