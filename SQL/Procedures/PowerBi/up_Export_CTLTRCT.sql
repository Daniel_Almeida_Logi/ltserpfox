
/****** Object:  StoredProcedure [dbo].[up_Export_CTLTRCT]   
exec up_Export_CTLTRCT 'POWERBI','F13156A',''
 Script Date: 12/01/2024 12:10:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'up_Export_CTLTRCT'))
DROP procedure [dbo].[up_Export_CTLTRCT]
GO
CREATE procedure [dbo].[up_Export_CTLTRCT]
	@ipdestino		varchar(100)
	,@bddestino		varchar(100)
	,@site			varchar (50)

/* with encryption */
AS
SET NOCOUNT OFF

/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#CTLTRCT'))
		DROP TABLE #CTLTRCT

	DECLARE @DELETE varchar(max)
	DECLARE @INSERT varchar(5000)
	DECLARE @destino varchar (200) = '['+@ipdestino+'].['+@bddestino+']'
	DECLARE @tabeladestino VARCHAR (50) = 'PowerBI_Receituário'	

SET @DELETE = 'DELETE FROM ' + @destino + '.dbo.' + @tabeladestino + ' 
               WHERE ' + 
               CASE 
                   WHEN MONTH(GETDATE()) = 1 AND DAY(GETDATE()) <= 6 
                   THEN '(ano = ' + CAST(YEAR(GETDATE()) - 1 AS VARCHAR) + ' AND mes = 12) 
                         OR (ano = ' + CAST(YEAR(GETDATE()) AS VARCHAR) + ' AND mes = 1)'
                   WHEN MONTH(GETDATE()) != 1 AND DAY(GETDATE()) <= 6 
                   THEN 'ano = ' + CAST(YEAR(GETDATE()) AS VARCHAR) + ' AND mes BETWEEN ' + 
                        CAST(MONTH(GETDATE()) - 1 AS VARCHAR) + ' AND ' + CAST(MONTH(GETDATE()) AS VARCHAR)
                   WHEN DAY(GETDATE()) > 6 
                   THEN 'ano = ' + CAST(YEAR(GETDATE()) AS VARCHAR) + ' AND mes = ' + 
                        CAST(MONTH(GETDATE()) AS VARCHAR)
               END;


IF @site != ''
BEGIN
set @DELETE = @DELETE +' and site = '''+@site+'''' 
END

PRINT @DELETE
EXEC (@DELETE)

	SET @INSERT=' 
	INSERT into '+@destino+'.dbo.'+@tabeladestino+' '
	
	SET @INSERT= @INSERT + '

	SELECT distinct a.[ctltrctstamp]
	  ,fi.[fistamp]
      ,a.[cptorgstamp]
      ,a.[cptorgabrev]
      ,a.[lote]
      ,a.[tlote]
      ,a.[slote]
      ,a.[ano]
      ,a.[mes]
      ,a.[nreceita]
	  ,[CompSNS] = CASE WHEN a.cptorgabrev = ''SNS''
							THEN CASE	WHEN u_abrev = ''SNS'' THEN u_ettent1 
										WHEN u_abrev != ''SNS'' AND u_abrev2 = ''SNS'' THEN fi.u_ettent2 
										ELSE 0 
										END 
							ELSE 0 
							END 
	  ,[CompEnt] = CASE WHEN a.cptorgabrev != ''SNS'' THEN fi.u_ettent1 ELSE 0 END
	  ,[CompSNS_Siva] = CASE WHEN a.cptorgabrev = ''SNS''
							THEN CASE	WHEN u_abrev = ''SNS'' THEN u_ettent1/(fi.iva/100+1) 
										WHEN u_abrev != ''SNS'' AND u_abrev2 = ''SNS'' THEN fi.u_ettent2/(fi.iva/100+1) 
										ELSE 0 
										END 
							ELSE 0 
							END 
	  ,[CompEnt_Siva] = CASE WHEN a.cptorgabrev != ''SNS'' THEN fi.u_ettent1/(fi.iva/100+1) ELSE 0 END
      ,a.[fechado]
      ,a.[datafechado]
      ,a.[horafechado]
	  ,ft.[fdata]
      ,a.[usrlote]
      ,a.[cptplacode]
      ,a.[ousrinis]
      ,a.[ousrdata]
      ,a.[ousrhora]
      ,a.[usrinis]
      ,a.[usrdata]
      ,a.[usrhora]
      ,a.[marcada]
      ,a.[u_fistamp]
      ,ft.site
      ,a.[no_orig]
      ,a.[lote_orig]
      ,a.[posto]
      ,a.[receita]
      ,a.[cartao]
      ,a.[via]
      ,a.[comprovativo_registo]
      ,a.[token]
      ,a.[lote_dem]
	
	FROM [dbo].[ctltrct] (nolock) a 
		inner join ft (nolock) on (ft.u_ltstamp=ctltrctstamp or ft.u_ltstamp2=ctltrctstamp)
		inner join fi (nolock) on ft.ftstamp = fi.ftstamp
		inner join ft2 (nolock) on ft2.ft2stamp = ft.ftstamp
		LEFT JOIN '+@destino+'.dbo.'+@tabeladestino+' b with (nolock)
								ON a.ctltrctstamp COLLATE DATABASE_DEFAULT = b.[StampRegistoReceita] COLLATE DATABASE_DEFAULT
							where b.[StampRegistoReceita] COLLATE DATABASE_DEFAULT IS NULL '
							
IF @site != ''
BEGIN
set @INSERT = @INSERT +' and site = '''+@site+'''' 
END

	PRINT @INSERT
	EXEC (@INSERT)

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#CTLTRCT'))
		DROP TABLE #CTLTRCT
