
/****** Object:  StoredProcedure [dbo].[up_relatorio_pagamentos_geral_PBI]    Script Date: 14/11/2022 18:16:24 *****
exec up_relatorio_pagamentos_geral_PBI '20250107','20250107','POWERBI','F13156A'
*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'up_relatorio_pagamentos_geral_PBI'))
DROP procedure [dbo].[up_relatorio_pagamentos_geral_PBI]
GO
CREATE procedure [dbo].[up_relatorio_pagamentos_geral_PBI]
	@dataIni		varchar(100)
	,@dataFim		varchar(100)
	,@ipdestino		varchar(100)
	,@bddestino		varchar(100)
	
	
/* with encryption */
AS
SET NOCOUNT OFF

declare @epaga1 varchar(50) 
declare @epaga1_ varchar(50) 
declare @evdinheiro varchar(50)
declare @evdinheiro_ varchar(50)
declare @echtotal varchar(50)
declare @echtotal_ varchar(50)
declare @epaga2 varchar(50) 
declare @epaga2_ varchar(50) 
declare @epaga3 varchar(50) 
declare @epaga3_ varchar(50) 
declare @epaga4 varchar(50) 
declare @epaga4_ varchar(50) 
declare @epaga5 varchar(50) 
declare @epaga5_ varchar(50) 
declare @epaga6 varchar(50) 
declare @epaga6_ varchar(50) 

DECLARE @textvalue varchar(15)

DECLARE @INSERT varchar(max)
DECLARE @DELETE varchar(max)
DECLARE @destino varchar (200) = '['+@ipdestino+'].['+@bddestino+']'
DECLARE @tabeladestino VARCHAR (50) = 'PowerBI_MeiosPagamento'


select @textvalue = textvalue from B_Parameters where stamp='ADM0000000260'
IF @textvalue = 'EURO'
begin
	select @epaga1 = design, @epaga1_ = right(campoFact,len(campoFact)-4) from B_modoPag(nolock) where campoFact like '%epaga1%'  and inativo=0 
	select @evdinheiro = design, @evdinheiro_ = right(campoFact,len(campoFact)-4)  from B_modoPag(nolock) where campoFact like '%evdinheir%'  and inativo=0 
	select @echtotal = design, @echtotal_ = right(campoFact,len(campoFact)-3)  from B_modoPag(nolock) where campoFact like '%echtotal%'  and inativo=0 
	select @epaga2 = design, @epaga2_ = right(campoFact,len(campoFact)-4) from B_modoPag(nolock) where campoFact like '%epaga2%' and inativo=0 
	select @epaga3 = design, @epaga3_ = right(campoFact,len(campoFact)-4) from B_modoPag(nolock) where campoFact like '%epaga3%' and inativo=0 
	select @epaga4 = design, @epaga4_ = right(campoFact,len(campoFact)-4) from B_modoPag(nolock) where campoFact like '%epaga4%' and inativo=0 
	select @epaga5 = design, @epaga5_ = right(campoFact,len(campoFact)-4) from B_modoPag(nolock) where campoFact like '%epaga5%' and inativo=0 
	select @epaga6 = design, @epaga6_ = right(campoFact,len(campoFact)-4) from B_modoPag(nolock) where campoFact like '%epaga6%' and inativo=0 
end
ELSE
begin
	select @epaga1 = design, @epaga1_ = right(campoFact,len(campoFact)-4) from B_modoPag_moeda(nolock) where campoFact like '%epaga1%' and inativo=0 
	select @evdinheiro = design, @evdinheiro_ = right(campoFact,len(campoFact)-4)  from B_modoPag_moeda(nolock) where campoFact like '%evdinheir%'  and inativo=0 
	select @echtotal = design, @echtotal_ = right(campoFact,len(campoFact)-3)  from B_modoPag_moeda(nolock) where campoFact like '%echtotal%'  and inativo=0 
	select @epaga2 = design, @epaga2_ = right(campoFact,len(campoFact)-4) from B_modoPag_moeda(nolock) where campoFact like '%epaga2%' and inativo=0 
	select @epaga3 = design, @epaga3_ = right(campoFact,len(campoFact)-4) from B_modoPag_moeda(nolock) where campoFact like '%epaga3%' and inativo=0 
	select @epaga4 = design, @epaga4_ = right(campoFact,len(campoFact)-4) from B_modoPag_moeda(nolock) where campoFact like '%epaga4%' and inativo=0 
	select @epaga5 = design, @epaga5_ = right(campoFact,len(campoFact)-4) from B_modoPag_moeda(nolock) where campoFact like '%epaga5%' and inativo=0 
	select @epaga6 = design, @epaga6_ = right(campoFact,len(campoFact)-4) from B_modoPag_moeda(nolock) where campoFact like '%epaga6%' and inativo=0 
end
	

set @DELETE = 'DELETE from	'+@destino+'.dbo.'+@tabeladestino + ' where CONVERT(varchar, uData, 112) between CONVERT(varchar, '''+@dataIni+''', 112) and CONVERT(varchar, '''+@dataFim+''', 112);' 

print @DELETE
exec (@DELETE)

set @INSERT='
INSERT into '+@destino+'.dbo.'+@tabeladestino+' '


set	@INSERT = @INSERT + 
		'select a.nratend, a.ano, a.oData, a.uData, convert(varchar,a.no) + ''.''+ convert(varchar,a.estab)  as nCliDep
, a.vendedor, a.nome, a.nrVendas, a.total, a.devolucoes, a.ntCredito, a.creditos,' + 'a.site, '  +  @epaga1_ + ' as [' + @epaga1 + 
'],' +  @evdinheiro_ + ' as [' + @evdinheiro +
'],' +  @echtotal_ + ' as [' + @echtotal +

'],' +  @epaga2_ + ' as [' + @epaga2 +
'],' +  @epaga3_ + ' as [' + @epaga3 + ']'

if @epaga4_ is not null	
	set  @INSERT = @INSERT + ',' +  @epaga4_ + ' as [' + @epaga4 +']'
if @epaga5_ is not null	
	set  @INSERT = @INSERT + ',' +  @epaga5_ + ' as [' + @epaga5 +']'
if @epaga6_ is not null	
	set  @INSERT = @INSERT + ',' +  @epaga6_ + ' as [' + @epaga6 +']'

set	@INSERT = @INSERT + ' from B_pagCentral a with (nolock) LEFT JOIN '+@destino+'.dbo.'+@tabeladestino+' b with (nolock) 
						ON a.nratend COLLATE DATABASE_DEFAULT = b.nratend COLLATE DATABASE_DEFAULT
					where b.nratend COLLATE DATABASE_DEFAULT IS NULL and
						CONVERT(varchar, a.udata, 112) between CONVERT(varchar, '''+@dataIni+''', 112) and CONVERT(varchar, '''+@dataFim+''', 112); '

print @INSERT
exec (@INSERT)
