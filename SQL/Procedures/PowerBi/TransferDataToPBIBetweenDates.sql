DECLARE @StartDate	DATE		= '20200101';
DECLARE @EndDate	DATE		= '20201231';
DECLARE @bddestino varchar (100) = 'F13156A'
DECLARE @ipdestino varchar (100) = 'POWERBI'
DECLARE @site	varchar(55) = ''

WHILE @StartDate <= @EndDate
BEGIN
    DECLARE @NextMonth DATE = DATEADD(MONTH, 1, @StartDate);
    DECLARE @StartDateFormatted NVARCHAR(8) = CONVERT(VARCHAR(8), @StartDate, 112);
    DECLARE @NextMonthFormatted NVARCHAR(8) = CONVERT(VARCHAR(8), @NextMonth, 112);
	DECLARE @go varchar(4)='GO'
    EXEC up_relatorio_vendas_geral_PBI @StartDateFormatted, @NextMonthFormatted, @site, @ipdestino, @bddestino;
    EXEC up_relatorio_Compras_geral_PBI @StartDateFormatted, @NextMonthFormatted, @site, @ipdestino, @bddestino;
    EXEC up_relatorio_pagamentos_geral_PBI @StartDateFormatted, @NextMonthFormatted, @ipdestino, @bddestino;
    EXEC [up_Export_DocsBO_PBI] @StartDateFormatted, @NextMonthFormatted, @ipdestino, @bddestino, @site;
	PRINT CONCAT('transferido com sucesso', @startdate, '-' ,@NextMonthFormatted)
	exec ( @go)
    PRINT 'GO'
	SET @StartDate = @NextMonth;
END
PRINT CONCAT('transferido com sucesso', @startdate, '-' ,@enddate)


DECLARE @ip_errortable varchar (100) = 'SQLLOGITOOLS'
DECLARE @bd_errortable varchar (100) = 'LTSMSB'

	exec up_update_Prods_PBI @ipdestino,@bddestino,@site
	exec up_update_ATC_PBI @ipdestino,@bddestino
	exec up_Export_Pagamentos_PBI @ipdestino,@bddestino
	exec up_Export_CTLTRCT @ipdestino,@bddestino,@site
	exec up_export_clientes_PBI @ipdestino,@bddestino
	exec up_export_fornecedores_PBI @ipdestino,@bddestino

