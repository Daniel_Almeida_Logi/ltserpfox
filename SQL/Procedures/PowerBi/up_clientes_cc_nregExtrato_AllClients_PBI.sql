
/****** Object:  StoredProcedure [dbo].[up_clientes_cc_nregExtrato_AllClients_PBI]  

exec up_clientes_cc_nregExtrato_AllClients_PBI 1,'Loja 1','POWERBI','F13151A'


Script Date: 18/12/2024 12:59:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_clientes_cc_nregExtrato_AllClients_PBI]') IS NOT NULL
	drop procedure dbo.[up_clientes_cc_nregExtrato_AllClients_PBI]
go

CREATE procedure [dbo].[up_clientes_cc_nregExtrato_AllClients_PBI]

		@modo		bit -- Com ou sem saldo calculado
		,@ipdestino		varchar(100)
		,@bddestino		varchar(100)



/* with encryption */
AS
SET NOCOUNT ON


/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#CC_NReg'))
		DROP TABLE #CC_NReg

;with
	cteUser as
	(
		select 
			distinct username
			,iniciais
		from 
			b_us
	), cteEstabs as (
		Select 
			estab
		From
			B_utentes
	)

Select	'SALDO' = 
				Case when @modo=1
				then
					isnull(
							(
								Select sum((cc.edeb-cc.edebf)-(cc.ecred-cc.ecredf)) 
								From	(
									SELECT	cc.ccstamp, ROW_NUMBER() over(order by cc.datalc,cc.ousrhora, cc.cm, cc.nrdoc) as numero, edeb, ecred
									FROM	cc (nolock)
									left join ft (nolock) on cc.ftstamp=ft.ftstamp
									WHERE	cc.no=cc.no
											and cc.no>199 --excluir entidades
											And cc.estab=cc.estab
											And (abs((cc.ecred-cc.ecredf)-(cc.edeb-cc.edebf)) > 0.010000)
											and ft.anulado=0
							) a inner join cc (nolock) on cc.ccstamp = a.ccstamp
							
					),0)
				else
					0
				end,
		*
INTO #CC_NReg
From	(	
		select 	'DATALC'		= convert(varchar,cc.datalc,102),
				'DATAVEN'		= convert(varchar,cc.dataven,102),
				'EDEB'			= cc.edeb,
				'ECRED'			= cc.ecred,
				'EDEBF'			= cc.edebf,
				'ECREDF'		= cc.ecredf,
				'VALOR'			= (cc.edeb-cc.edebf)-(cc.ecred-cc.ecredf),
				'MOEDA'			= cc.moeda,
				'CMDESC'		= cc.cmdesc,
				'NRDOC'			= cc.nrdoc,
				'ULTDOC'		= cc.ultdoc,
				'INTID'			= cc.intid,
				'FACSTAMP'		= cc.ftstamp,
				'RESTAMP'		= cc.restamp,
				'CCSTAMP'		= cc.ccstamp,
				'CBBNO'			= cc.cbbno,
				'ORIGEM'		= cc.origem,
				'OUSRHORA'		= cc.ousrhora,
				'OUSRINIS'		= cc.ousrinis,
				'IEMISSAO'		= datediff(d,cc.datalc,GETDATE()),
				'IVENCIMENTO'	= datediff(d,cc.datalc,GETDATE()),
				'USERNAME'		= isnull(cteUser.username,''),
				'NO'			= cc.no,
				'NO_ESTAB'		= CONCAT(cc.no,'.',cc.estab),
				'NOME'			= cc.nome,
				'SITE'			= ISNULL(isnull(ft.site,re.site),'')
		from
			cc (nolock)
			left join ft (nolock) on cc.ftstamp=ft.ftstamp
			left join cteUser on cteUser.iniciais = cc.ousrinis
			left join re (nolock) on re.restamp = cc.restamp
		where
			(abs((cc.ecred-cc.ecredf)-(cc.edeb-cc.edebf)) > 0.010000)

		
) b
order by nome

										 
	DECLARE @stm varchar(5000)
	DECLARE @destino varchar (200) = '['+@ipdestino+'].['+@bddestino+']'
	DECLARE @tabeladestino VARCHAR (50) = 'Extrato_CC_PBI'	
	DECLARE @insertSql varchar(max) = ''
	DECLARE @tabelaorigem VARCHAR (100) = '#CC_NReg'

	EXEC  up_retorna_insert_sql_dinamico @tabelaorigem,@tabeladestino,@ipdestino,@bddestino, @insertSql OUTPUT
	
	SET @stm = 'DELETE from	'+@destino+'.dbo.'+@tabeladestino 
					
	set @stm = @stm + @insertSql 

	PRINT @stm
	EXEC (@stm)

GO
GRANT EXECUTE on dbo.[up_clientes_cc_nregExtrato_AllClients_PBI] TO PUBLIC
GRANT Control on dbo.[up_clientes_cc_nregExtrato_AllClients_PBI] TO PUBLIC
GO


/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#CC_NReg'))
		DROP TABLE #CC_NReg


