
/****** Object:  StoredProcedure [dbo].[up_gerais_runDatabasesClean_PBI]    Script Date: 28/01/2025 12:54:28 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF EXISTS(SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[up_gerais_runDatabasesClean_PBI]'))
DROP procedure [dbo].[up_gerais_runDatabasesClean_PBI]
GO
CREATE PROCEDURE [dbo].[up_gerais_runDatabasesClean_PBI]

	
/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

--exec up_gerais_runDatabasesClean_PBI	
	
	DECLARE @DB_Name varchar(100) = ''
	DECLARE @DB_NameLogs varchar(100) = ''
	DECLARE @DB_NameF varchar(100) = ''
	DECLARE @DB_id smallint = -1
	DECLARE @sql nvarchar(max) = ''
	DECLARE @sql2 nvarchar(max) = ''
	DECLARE @sql1 nvarchar(max) = ''
	declare @date datetime = getdate()-30

	declare @currentBd varchar(50) = ''
	
	SELECT @currentBd = DB_NAME() 

	BEGIN 
		 print @DB_Name

		  set @DB_id = (SELECT DB_ID(@DB_Name))

		  set @DB_NameF = (SELECT name FROM sys.master_files WHERE database_id = @DB_id AND type = 0) 

		 set @DB_NameLogs = (SELECT name FROM sys.master_files WHERE database_id = @DB_id AND type = 1) 

		 set @sql = '
		 			
			use '+ @currentBd +'
			ALTER DATABASE ['+ @DB_Name +'] SET RECOVERY SIMPLE
			DBCC SHRINKFILE ('+ @DB_NameF +', 1)
			ALTER DATABASE ['+ @DB_Name +'] SET RECOVERY FULL
			use '+ @currentBd +'	

		'
		print @sql
		EXEC sp_executesql @sql 

		set @sql1  = 'use '+ @currentBd +'
			ALTER DATABASE ['+ @DB_Name +'] SET RECOVERY SIMPLE
			DBCC SHRINKFILE ('+ @DB_NameLogs +', 1)
			ALTER DATABASE ['+ @DB_Name +'] SET RECOVERY FULL
			use '+ @currentBd +'
			
			'	

		print @sql1
		EXEC sp_executesql @sql1

		set @sql2 = ''
		select @sql2 = @sql2 + CASE
		    WHEN avg_fragmentation_in_percent >= 30 THEN
		        'ALTER INDEX [' + i.name + '] ON [' + s.name + '].[' + t.name + '] REBUILD;' + CHAR(10)
		    WHEN avg_fragmentation_in_percent >= 5 THEN
		        'ALTER INDEX [' + i.name + '] ON [' + s.name + '].[' + t.name + '] REORGANIZE;' + CHAR(10) + 
		        'EXEC sp_MSforeachtable ''UPDATE STATISTICS ?'';' + CHAR(10)
		    ELSE
		        ''
		    END
		FROM sys.dm_db_index_physical_stats(DB_ID(), NULL, NULL, NULL, 'DETAILED') d
		INNER JOIN sys.indexes i ON d.object_id = i.object_id AND d.index_id = i.index_id
		INNER JOIN sys.objects t ON i.object_id = t.object_id
		INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
		WHERE i.type > 0 -- Apenas índices
		  AND t.type = 'U'; -- Apenas tabelas de usuário

		print @sql2
		EXEC sp_executesql @sql2

	
	END

	
	If OBJECT_ID('tempdb.dbo.#temp') IS NOT NULL
		DROP TABLE #temp
	
