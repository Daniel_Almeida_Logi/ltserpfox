
ALTER TABLE [dbo].[PowerBI_dadosVendasBase] DROP CONSTRAINT [DF_PowerBI_dadosVendasBase_usrdata]
GO
ALTER TABLE [dbo].[PowerBI_dadosComprasBase] DROP CONSTRAINT [DF_PowerBI_dadosComprasBase_usrdata]
GO
/****** Object:  Table [dbo].[PowerBI_prods]    Script Date: 14/11/2022 19:08:42 ******/
DROP TABLE [dbo].[PowerBI_prods]
GO
/****** Object:  Table [dbo].[PowerBI_MeiosPagamento]    Script Date: 14/11/2022 19:08:42 ******/
DROP TABLE [dbo].[PowerBI_MeiosPagamento]
GO
/****** Object:  Table [dbo].[PowerBI_fornecedores]    Script Date: 14/11/2022 19:08:42 ******/
DROP TABLE [dbo].[PowerBI_fornecedores]
GO
/****** Object:  Table [dbo].[PowerBI_dadosVendasBase]    Script Date: 14/11/2022 19:08:42 ******/
DROP TABLE [dbo].[PowerBI_dadosVendasBase]
GO
/****** Object:  Table [dbo].[PowerBI_dadosComprasBase]    Script Date: 14/11/2022 19:08:42 ******/
DROP TABLE [dbo].[PowerBI_dadosComprasBase]
GO
/****** Object:  Table [dbo].[PowerBI_clientes]    Script Date: 14/11/2022 19:08:42 ******/
DROP TABLE [dbo].[PowerBI_clientes]
GO
/****** Object:  Table [dbo].[PowerBI_atc]    Script Date: 14/11/2022 19:08:42 ******/
DROP TABLE [dbo].[PowerBI_atc]
GO
/****** Object:  Table [dbo].[PowerBI_atc]    Script Date: 14/11/2022 19:08:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PowerBI_atc](
	[cnp] [varchar](50) NOT NULL,
	[CNPEM] [varchar] (8) NOT NULL,
	[grphmgcode] [varchar] (6) NOT NULL, 
	[grphmgdescr] [varchar] (100) NOT NULL,
	[atccode] [varchar] (10) NOT NULL,
	[descricao] [varchar] (100) NOT NULL,
	[atcmini] [varchar](200) NULL,
	[Ac_farmac] [varchar](max) NULL,
	[Indica] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PowerBI_clientes]    Script Date: 14/11/2022 19:08:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PowerBI_clientes](
	[utstamp] [char](25) NOT NULL,
	[nome] [varchar](80) NOT NULL,
	[no] [numeric](10, 0) NOT NULL,
	[estab] [numeric](3, 0) NOT NULL,
	[ncont] [varchar](20) NOT NULL,
	[nbenef] [varchar](50) NOT NULL,
	[local] [varchar](45) NOT NULL,
	[nascimento] [datetime] NOT NULL,
	[morada] [varchar](55) NOT NULL,
	[telefone] [varchar](20) NOT NULL,
	[obs] [text] NOT NULL,
	[codpost] [varchar](45) NOT NULL,
	[bino] [varchar](20) NOT NULL,
	[fax] [varchar](13) NOT NULL,
	[tlmvl] [varchar](20) NOT NULL,
	[zona] [varchar](20) NOT NULL,
	[tipo] [varchar](20) NOT NULL,
	[sexo] [varchar](3) NOT NULL,
	[email] [varchar](45) NOT NULL,
	[codpla] [varchar](40) NOT NULL,
	[despla] [varchar](254) NOT NULL,
	[valipla] [datetime] NOT NULL,
	[valipla2] [datetime] NOT NULL,
	[nrcartao] [varchar](30) NOT NULL,
	[hablit] [varchar](60) NOT NULL,
	[profi] [varchar](60) NOT NULL,
	[nbenef2] [varchar](35) NOT NULL,
	[entfact] [bit] NOT NULL,
	[peso] [numeric](4, 2) NOT NULL,
	[altura] [numeric](3, 2) NOT NULL,
	[inactivo] [bit] NOT NULL,
	[nocredit] [bit] NOT NULL,
	[ousrinis] [varchar](30) NOT NULL,
	[ousrdata] [datetime] NOT NULL,
	[ousrhora] [varchar](8) NOT NULL,
	[usrinis] [varchar](30) NOT NULL,
	[usrdata] [datetime] NOT NULL,
	[usrhora] [varchar](8) NOT NULL,
	[nome2] [varchar](55) NOT NULL,
	[url] [varchar](100) NOT NULL,
	[vencimento] [numeric](3, 0) NOT NULL,
	[eplafond] [numeric](19, 6) NOT NULL,
	[nib] [varchar](28) NOT NULL,
	[desconto] [numeric](5, 2) NOT NULL,
	[particular] [bit] NOT NULL,
	[esaldo] [numeric](19, 6) NOT NULL,
	[moeda] [varchar](11) NOT NULL,
	[ultvenda] [datetime] NOT NULL,
	[entpla] [varchar](50) NOT NULL,
	[descp] [varchar](25) NOT NULL,
	[codigop] [varchar](10) NOT NULL,
	[notif] [bit] NOT NULL,
	[descpNat] [varchar](25) NOT NULL,
	[codigopNat] [varchar](10) NOT NULL,
	[autorizado] [bit] NOT NULL,
	[removido] [bit] NOT NULL,
	[data_removido] [datetime] NULL,
	[autoriza_sms] [bit] NOT NULL,
	[autoriza_emails] [bit] NOT NULL,
 CONSTRAINT [PowerBI_clientes_pk] PRIMARY KEY CLUSTERED 
(
	[no] ASC,
	[estab] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PowerBI_dadosComprasBase]    Script Date: 03/11/2023 16:58:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PowerBI_dadosComprasBase](
	[fnstamp] [varchar](25) NOT NULL,
	[loja] [varchar](50) NOT NULL,
	[docdata] [datetime] NOT NULL,
	[dataven] [datetime] NOT NULL,
	[no] [numeric](10, 0) NOT NULL,
	[estab] [numeric](5, 0) NOT NULL,
	[tipo] [varchar](100) NOT NULL,
	[doccode] [numeric](6, 0) NOT NULL,
	[adoc] [varchar](50) NOT NULL,
	[docnome] [varchar](50) NOT NULL,
	[cm] [numeric](10, 0) NOT NULL,
	[ref] [varchar](18) NOT NULL,
	[design] [varchar](200) NOT NULL,
	[familia] [varchar](50) NOT NULL,
	[faminome] [varchar](50) NOT NULL,
	[u_epvp] [numeric](15, 3) NOT NULL,
	[pcp] [numeric](19, 6) NOT NULL,
	[pcl] [numeric](19, 6) NOT NULL,
	[pct] [numeric](19, 6) NOT NULL,
	[iva] [numeric](5, 2) NOT NULL,
	[qtt] [numeric](11, 3) NOT NULL,
	[etiliquido] [numeric](19, 6) NOT NULL,
	[etiliquidoSiva] [numeric](19, 6) NOT NULL,
	[desconto] [numeric](19, 6) NOT NULL,
	[descvalor] [numeric](19, 6) NOT NULL,
	[ousrhora] [varchar](8) NOT NULL,
	[ousrinis] [varchar](3) NOT NULL,
	[usr1] [varchar](100) NOT NULL,
	[u_lab] [varchar](100) NOT NULL,
	[departamento] [varchar](100) NOT NULL,
	[seccao] [varchar](100) NOT NULL,
	[categoria] [varchar](100) NOT NULL,
	[famfamilia] [varchar](100) NOT NULL,
	[dispdescr] [varchar](100) NOT NULL,
	[generico] [bit] NULL,
	[psico] [bit] NULL,
	[benzo] [bit] NULL,
	[protocolo] [bit] NULL,
	[dci] [varchar](100) NOT NULL,
	[valDepartamento] [varchar](100) NOT NULL,
	[valSeccao] [varchar](100) NOT NULL,
	[valCategoria] [varchar](100) NOT NULL,
	[valFamfamilia] [varchar](100) NOT NULL,
	[Obs] [varchar](254) NOT NULL,
	[usrdata] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PowerBI_dadosVendasBase]    Script Date: 03/11/2023 16:55:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PowerBI_dadosVendasBase](
	[nrAtend] [varchar](50) NULL,
	[ftstamp] [varchar](25) NULL,
	[fistamp] [varchar](25) NULL,
	[loja] [varchar](20) NULL,
	[fdata] [datetime] NULL,
	[no] [numeric](10, 0) NULL,
	[estab] [numeric](5, 0) NULL,
	[ndoc] [numeric](6, 0) NULL,
	[nmdoc] [varchar](20) NOT NULL,
	[fno] [numeric](10, 0) NULL,
	[tipodoc] [numeric](2, 0) NULL,
	[u_tipodoc] [numeric](2, 0) NULL,
	[ref] [varchar](18) NULL,
	[design] [varchar](254) NULL,
	[suspenso] [bit] NULL,
	[familia] [varchar](254) NULL,
	[faminome] [varchar](60) NULL,
	[u_epvp] [numeric](15, 3) NULL,
	[epcpond] [numeric](19, 6) NULL,
	[iva] [numeric](5, 2) NULL,
	[qtt] [numeric](11, 3) NULL,
	[etiliquido] [numeric](19, 6) NULL,
	[etiliquidoSiva] [numeric](19, 6) NULL,
	[ettent1] [numeric](13, 3) NULL,
	[ettent2] [numeric](13, 3) NULL,
	[ettent1Siva] [numeric](13, 3) NULL,
	[ettent2Siva] [numeric](13, 3) NULL,
	[desconto] [numeric](6, 2) NULL,
	[descvalor] [numeric](21, 6) NULL,
	[descvale] [numeric](19, 6) NULL,
	[ousrhora] [varchar](8) NULL,
	[ousrinis] [varchar](3) NULL,
	[vendnm] [varchar](25) NULL,
	[AbrevEnt1] [varchar](15) NULL,
	[NomeEnt1] [varchar](80) NULL,
	[AbrevEnt2] [varchar](15) NULL,
	[NomeEnt2] [varchar](80) NULL,
	[u_ltstamp] [varchar](25) NULL,
	[u_ltstamp2] [varchar](25) NULL,
	[usr1] [varchar](20) NULL,
	[u_lab] [varchar](200) NULL,
	[departamento] [varchar](254) NULL,
	[seccao] [varchar](254) NULL,
	[categoria] [varchar](254) NULL,
	[famfamilia] [varchar](254) NULL,
	[dispdescr] [varchar](80) NULL,
	[generico] [bit] NULL,
	[psico] [bit] NULL,
	[benzo] [bit] NULL,
	[protocolo] [bit] NULL,
	[dci] [varchar](254) NULL,
	[valDepartamento] [varchar](8000) NULL,
	[valSeccao] [varchar](8000) NULL,
	[valCategoria] [varchar](8000) NULL,
	[valFamfamilia] [varchar](8000) NULL,
	[usrdata] [datetime] NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[PowerBI_dadosVendasBase] ADD  CONSTRAINT [DF_PowerBI_dadosVendasBase_usrdata]  DEFAULT ('') FOR [usrdata]
GO
/****** Object:  Table [dbo].[PowerBI_fornecedores]    Script Date: 14/11/2022 19:08:42 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PowerBI_fornecedores](
	[flstamp] [char](25) NOT NULL,
	[nome] [varchar](80) NOT NULL,
	[nome2] [varchar](55) NOT NULL,
	[no] [numeric](10, 0) NOT NULL,
	[estab] [numeric](3, 0) NOT NULL,
	[moeda] [varchar](11) NOT NULL,
	[fax] [varchar](60) NOT NULL,
	[telefone] [varchar](60) NOT NULL,
	[contacto] [varchar](30) NOT NULL,
	[morada] [varchar](55) NOT NULL,
	[local] [varchar](43) NOT NULL,
	[codpost] [varchar](45) NOT NULL,
	[ncont] [varchar](20) NOT NULL,
	[zona] [varchar](20) NOT NULL,
	[tipo] [varchar](20) NOT NULL,
	[desconto] [numeric](5, 2) NOT NULL,
	[vencimento] [numeric](3, 0) NOT NULL,
	[obs] [varchar](254) NOT NULL,
	[pais] [numeric](1, 0) NOT NULL,
	[nib] [varchar](28) NOT NULL,
	[descpp] [numeric](5, 2) NOT NULL,
	[tabiva] [numeric](2, 0) NOT NULL,
	[c1tele] [varchar](60) NOT NULL,
	[c1fax] [varchar](60) NOT NULL,
	[c1func] [varchar](15) NOT NULL,
	[c2tele] [varchar](60) NOT NULL,
	[c2fax] [varchar](60) NOT NULL,
	[c2func] [varchar](15) NOT NULL,
	[c2tacto] [varchar](30) NOT NULL,
	[email] [varchar](100) NOT NULL,
	[nocredit] [bit] NOT NULL,
	[nacional] [varchar](20) NOT NULL,
	[tlmvl] [varchar](45) NOT NULL,
	[esaldo] [numeric](19, 6) NOT NULL,
	[eplafond] [numeric](19, 6) NOT NULL,
	[url] [varchar](100) NOT NULL,
	[tpdesc] [varchar](55) NOT NULL,
	[inactivo] [bit] NOT NULL,
	[pncont] [varchar](2) NOT NULL,
	[ousrinis] [varchar](30) NOT NULL,
	[ousrdata] [datetime] NOT NULL,
	[ousrhora] [varchar](8) NOT NULL,
	[usrinis] [varchar](30) NOT NULL,
	[usrdata] [datetime] NOT NULL,
	[usrhora] [varchar](8) NOT NULL,
	[iban] [varchar](40) NOT NULL,
	[id] [varchar](10) NOT NULL,
	[SWIFT] [varchar](50) NOT NULL,
 CONSTRAINT [PowerBI_fornecedores_pk] PRIMARY KEY NONCLUSTERED 
(
	[no] ASC,
	[estab] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PowerBI_MeiosPagamento]    
Atenção aos meios de pagamento da farmácia. Fazer:

select * from b_modopag 

e preencher abaixo os meios pela ordem (substituir o nome do meio [XXX] pelo nome na b_modopag):

epaga1 as [CC],evdinheiro as [Dinheiro],echtotal as [Cheques],epaga2 as [Multibanco],epaga3 as [Vales],epaga4 as [TB],epaga5 as [Ifthenpay],epaga6 as [ModPag Ad.]
******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--
--CREATE TABLE [dbo].[PowerBI_MeiosPagamento](
--	[nratend] [varchar](20) NOT NULL,
--	[ano] [numeric](4, 0) NOT NULL,
--	[oData] [datetime] NOT NULL,
--	[uData] [datetime] NOT NULL,
--	[nCliDep] [varchar](61) NULL,
--	[vendedor] [numeric](6, 0) NOT NULL,
--	[nome] [varchar](60) NOT NULL,
--	[nrVendas] [numeric](5, 0) NOT NULL,
--	[total] [numeric](13, 3) NOT NULL,
--	[devolucoes] [numeric](13, 3) NOT NULL,
--	[ntCredito] [numeric](13, 3) NOT NULL,
--	[creditos] [numeric](13, 3) NOT NULL,
--	[site] [varchar](50) NOT NULL,
--	[Visa] [numeric](13, 3) NOT NULL,
--	[Dinheiro] [numeric](13, 3) NOT NULL,
--	[Cheques] [numeric](13, 3) NOT NULL,
--	[Multibanco] [numeric](13, 3) NOT NULL,
--	[Vales] [numeric](13, 3) NOT NULL,
--	[TRF Banc] [numeric](13, 3) NOT NULL,
--	[ModPag Ad.1] [numeric](13, 3) NOT NULL,
--	[ModPag Ad.2] [numeric](13, 3) NOT NULL
--) ON [PRIMARY]
--GO




/****** Object:  Table [dbo].[PowerBI_prods]    Script Date: 03/11/2023 17:03:57 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PowerBI_prods](
	[ststamp] [char](25) NOT NULL,
	[ref] [char](18) NOT NULL,
	[design] [varchar](100) NOT NULL,
	[familia] [varchar](18) NOT NULL,
	[stock] [numeric](13, 3) NOT NULL,
	[epv1] [numeric](19, 6) NOT NULL,
	[forref] [char](20) NOT NULL,
	[fornecedor] [varchar](80) NOT NULL,
	[usr1] [varchar](200) NOT NULL,
	[validade] [datetime] NOT NULL,
	[usaid] [datetime] NOT NULL,
	[uintr] [datetime] NOT NULL,
	[usrqtt] [numeric](13, 3) NOT NULL,
	[eoq] [numeric](13, 3) NOT NULL,
	[pcult] [numeric](18, 5) NOT NULL,
	[pvultimo] [numeric](18, 5) NOT NULL,
	[unidade] [varchar](4) NOT NULL,
	[ptoenc] [numeric](10, 3) NOT NULL,
	[tabiva] [numeric](1, 0) NOT NULL,
	[local] [varchar](20) NOT NULL,
	[fornec] [numeric](10, 0) NOT NULL,
	[fornestab] [numeric](3, 0) NOT NULL,
	[qttfor] [numeric](13, 3) NOT NULL,
	[qttcli] [numeric](13, 3) NOT NULL,
	[qttrec] [numeric](13, 3) NOT NULL,
	[udata] [datetime] NOT NULL,
	[pcusto] [numeric](18, 5) NOT NULL,
	[pcpond] [numeric](18, 5) NOT NULL,
	[qttacin] [numeric](13, 3) NOT NULL,
	[qttacout] [numeric](13, 3) NOT NULL,
	[stmax] [numeric](13, 3) NOT NULL,
	[stmin] [numeric](13, 3) NOT NULL,
	[obs] [varchar](254) NOT NULL,
	[codigo] [char](40) NOT NULL,
	[uni2] [varchar](4) NOT NULL,
	[conversao] [numeric](15, 7) NOT NULL,
	[ivaincl] [bit] NOT NULL,
	[nsujpp] [bit] NOT NULL,
	[ecomissao] [numeric](3, 0) NOT NULL,
	[imagem] [varchar](120) NOT NULL,
	[cpoc] [numeric](6, 0) NOT NULL,
	[containv] [varchar](15) NOT NULL,
	[contacev] [varchar](15) NOT NULL,
	[contareo] [varchar](15) NOT NULL,
	[contacoe] [varchar](15) NOT NULL,
	[peso] [numeric](14, 3) NOT NULL,
	[bloqueado] [bit] NOT NULL,
	[fobloq] [bit] NOT NULL,
	[mfornec] [numeric](6, 2) NOT NULL,
	[mfornec2] [numeric](6, 2) NOT NULL,
	[pentrega] [numeric](3, 0) NOT NULL,
	[consumo] [numeric](13, 3) NOT NULL,
	[baixr] [bit] NOT NULL,
	[despimp] [numeric](5, 2) NOT NULL,
	[marg1] [numeric](16, 3) NOT NULL,
	[marg2] [numeric](16, 3) NOT NULL,
	[marg3] [numeric](16, 3) NOT NULL,
	[marg4] [numeric](16, 3) NOT NULL,
	[diaspto] [numeric](3, 0) NOT NULL,
	[diaseoq] [numeric](3, 0) NOT NULL,
	[clinica] [bit] NOT NULL,
	[pbruto] [numeric](14, 3) NOT NULL,
	[volume] [numeric](11, 3) NOT NULL,
	[usalote] [bit] NOT NULL,
	[faminome] [varchar](60) NOT NULL,
	[qttesp] [numeric](11, 3) NOT NULL,
	[epcusto] [numeric](19, 6) NOT NULL,
	[epcpond] [numeric](19, 6) NOT NULL,
	[epcult] [numeric](19, 6) NOT NULL,
	[epvultimo] [numeric](19, 6) NOT NULL,
	[iva1incl] [bit] NOT NULL,
	[iva2incl] [bit] NOT NULL,
	[iva3incl] [bit] NOT NULL,
	[iva4incl] [bit] NOT NULL,
	[iva5incl] [bit] NOT NULL,
	[ivapcincl] [bit] NOT NULL,
	[stns] [bit] NOT NULL,
	[url] [varchar](100) NOT NULL,
	[iecasug] [bit] NOT NULL,
	[iecaref] [varchar](18) NOT NULL,
	[iecarefnome] [varchar](60) NOT NULL,
	[iecaisref] [bit] NOT NULL,
	[qlook] [bit] NOT NULL,
	[qttcat] [numeric](13, 3) NOT NULL,
	[compnovo] [bit] NOT NULL,
	[ousrinis] [varchar](30) NOT NULL,
	[ousrdata] [datetime] NOT NULL,
	[ousrhora] [varchar](8) NOT NULL,
	[usrinis] [varchar](30) NOT NULL,
	[usrdata] [datetime] NOT NULL,
	[usrhora] [varchar](8) NOT NULL,
	[marcada] [bit] NOT NULL,
	[contaieo] [varchar](15) NOT NULL,
	[inactivo] [bit] NOT NULL,
	[sujinv] [bit] NOT NULL,
	[u_nota1] [text] NOT NULL,
	[u_impetiq] [bit] NOT NULL,
	[u_local] [varchar](60) NOT NULL,
	[u_local2] [varchar](60) NOT NULL,
	[u_lab] [varchar](150) NOT NULL,
	[u_tipoetiq] [numeric](2, 0) NOT NULL,
	[u_validavd] [numeric](4, 0) NOT NULL,
	[u_aprov] [varchar](1) NOT NULL,
	[u_fonte] [varchar](1) NOT NULL,
	[u_validarc] [numeric](4, 0) NOT NULL,
	[u_servmarc] [bit] NOT NULL,
	[u_duracao] [numeric](8, 0) NOT NULL,
	[u_catstamp] [varchar](4) NOT NULL,
	[u_famstamp] [varchar](4) NOT NULL,
	[u_secstamp] [varchar](4) NOT NULL,
	[u_depstamp] [varchar](4) NOT NULL,
	[rateamento] [bit] NOT NULL,
	[exportado] [bit] NOT NULL,
	[u_codint] [varchar](13) NOT NULL,
	[mrsimultaneo] [int] NOT NULL,
	[site_nr] [varchar] (50) NOT NULL,
	[refentidade] [varchar](18) NOT NULL,
	[designentidade] [varchar](200) NOT NULL,
	[categoria] [varchar](150) NOT NULL,
	[generis] [bit] NOT NULL,
	[codidt] [numeric](10, 0) NOT NULL,
	[qttminvd] [numeric](10, 2) NOT NULL,
	[qttembal] [numeric](10, 2) NOT NULL,
	[epv1Ext] [numeric](19, 6) NOT NULL,
	[epv1ExtMoeda] [varchar](3) NOT NULL,
	[codCNP] [varchar](30) NOT NULL,
	[robotStockMax] [int] NOT NULL,
	[cativado] [numeric](15, 0) NOT NULL,
	[profundidade] [decimal](14, 2) NOT NULL,
	[altura] [decimal](14, 2) NOT NULL,
	[designOnline] [varchar](100) NOT NULL,
	[caractOnline] [varchar](max) NOT NULL,
	[apresentOnline] [varchar](max) NOT NULL,
	[novidadeOnline] [bit] NOT NULL,
	[tempoIndiponibilidade] [decimal](8, 1) NOT NULL,
	[dispOnline] [bit] NOT NULL,
	[portesGratis] [bit] NOT NULL,
	[u_segstamp] [varchar](4) NOT NULL,
	[motiseimp] [varchar](100) NOT NULL,
	[codmotiseimp] [varchar](30) NOT NULL,
	[vaccine] [bit] NOT NULL,
	[vaccineCode] [varchar](100) NOT NULL,
	[precoSobConsulta] [bit] NOT NULL,
	[usr2] [varchar](150) NOT NULL,
	[usr2desc] [varchar](150) NOT NULL,
	[usr3] [varchar](150) NOT NULL,
	[usr3desc] [varchar](150) NOT NULL,
	[usr4] [varchar](150) NOT NULL,
	[usr4desc] [varchar](150) NOT NULL,
 CONSTRAINT [pk_st] PRIMARY KEY CLUSTERED 
(
	[ref] ASC,
	[site_nr] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 95, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [ststamp]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [ref]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_design]  DEFAULT ('') FOR [design]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [familia]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [stock]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [epv1]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [forref]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_fornecedor]  DEFAULT ('') FOR [fornecedor]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_usr1]  DEFAULT ('') FOR [usr1]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT (CONVERT([datetime],'19000101')) FOR [validade]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT (CONVERT([datetime],'19000101')) FOR [usaid]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT (CONVERT([datetime],'19000101')) FOR [uintr]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [usrqtt]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [eoq]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [pcult]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [pvultimo]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [unidade]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [ptoenc]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [tabiva]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [local]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [fornec]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [fornestab]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [qttfor]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [qttcli]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [qttrec]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT (CONVERT([datetime],'19000101')) FOR [udata]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [pcusto]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [pcpond]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [qttacin]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [qttacout]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [stmax]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [stmin]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_obs]  DEFAULT ('') FOR [obs]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF__ST__CODIGO__reconstruido]  DEFAULT ('') FOR [codigo]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [uni2]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [conversao]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [ivaincl]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [nsujpp]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [ecomissao]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [imagem]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [cpoc]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [containv]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [contacev]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [contareo]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [contacoe]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [peso]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [bloqueado]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [fobloq]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [mfornec]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [mfornec2]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [pentrega]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [consumo]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [baixr]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [despimp]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [marg1]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [marg2]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [marg3]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [marg4]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [diaspto]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [diaseoq]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [clinica]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [pbruto]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [volume]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [usalote]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [faminome]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [qttesp]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [epcusto]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [epcpond]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [epcult]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [epvultimo]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [iva1incl]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [iva2incl]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [iva3incl]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [iva4incl]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [iva5incl]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [ivapcincl]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [stns]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [url]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [iecasug]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [iecaref]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [iecarefnome]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [iecaisref]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [qlook]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [qttcat]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [compnovo]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [ousrinis]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT (getdate()) FOR [ousrdata]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [ousrhora]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [usrinis]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT (getdate()) FOR [usrdata]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [usrhora]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [marcada]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [contaieo]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [inactivo]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [sujinv]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [u_nota1]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [u_impetiq]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [u_local]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [u_local2]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_u_lab]  DEFAULT ('') FOR [u_lab]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [u_tipoetiq]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [u_validavd]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [u_aprov]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [u_fonte]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [u_validarc]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [u_servmarc]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [u_duracao]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [u_catstamp]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [u_famstamp]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [u_secstamp]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [u_depstamp]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [rateamento]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ((0)) FOR [exportado]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  DEFAULT ('') FOR [u_codint]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_mrsimultaneo]  DEFAULT ((0)) FOR [mrsimultaneo]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_refentidade]  DEFAULT ('') FOR [refentidade]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_designentidade]  DEFAULT ('') FOR [designentidade]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_categoria]  DEFAULT ('') FOR [categoria]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_generis]  DEFAULT ((0)) FOR [generis]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_codidt]  DEFAULT ((0)) FOR [codidt]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_qttminvd]  DEFAULT ((0)) FOR [qttminvd]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_qttembal]  DEFAULT ((0)) FOR [qttembal]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_epv1Ext]  DEFAULT ('0') FOR [epv1Ext]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_epv1ExtMoeda]  DEFAULT ('') FOR [epv1ExtMoeda]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_codCNP]  DEFAULT ('') FOR [codCNP]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_robotStockMax]  DEFAULT ((0)) FOR [robotStockMax]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_cativado]  DEFAULT ((0)) FOR [cativado]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_profundidade]  DEFAULT ((0)) FOR [profundidade]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_altura]  DEFAULT ((0)) FOR [altura]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_designOnline]  DEFAULT ('') FOR [designOnline]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_caractOnline]  DEFAULT ('') FOR [caractOnline]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_apresentOnline]  DEFAULT ('') FOR [apresentOnline]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_novidadeOnline]  DEFAULT ((0)) FOR [novidadeOnline]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_tempoIndiponibilidade]  DEFAULT ((0)) FOR [tempoIndiponibilidade]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_dispOnline]  DEFAULT ((0)) FOR [dispOnline]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_portesGratis]  DEFAULT ((0)) FOR [portesGratis]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_u_segstamp]  DEFAULT ('') FOR [u_segstamp]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_motiseimp]  DEFAULT ('') FOR [motiseimp]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_codmotiseimp]  DEFAULT ('') FOR [codmotiseimp]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_vaccine]  DEFAULT ((0)) FOR [vaccine]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_vaccineCode]  DEFAULT ('') FOR [vaccineCode]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_precoSobConsulta]  DEFAULT ((0)) FOR [precoSobConsulta]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_usr2]  DEFAULT ('') FOR [usr2]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_usr2desc]  DEFAULT ('') FOR [usr2desc]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_usr3]  DEFAULT ('') FOR [usr3]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_usr3desc]  DEFAULT ('') FOR [usr3desc]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_usr4]  DEFAULT ('') FOR [usr4]
GO

ALTER TABLE [dbo].[PowerBI_prods] ADD  CONSTRAINT [DF_st_usr4desc]  DEFAULT ('') FOR [usr4desc]
GO

GO
ALTER TABLE [dbo].[PowerBI_dadosComprasBase] ADD  CONSTRAINT [DF_PowerBI_dadosComprasBase_usrdata]  DEFAULT ('') FOR [usrdata]
GO
ALTER TABLE [dbo].[PowerBI_dadosVendasBase] ADD  CONSTRAINT [DF_PowerBI_dadosVendasBase_usrdata]  DEFAULT ('') FOR [usrdata]
GO



/****** Object:  Table [dbo].PowerBI_DocsInternos    
drop table PowerBI_DocsInternos
Script Date: 29/06/2023 11:34:56 ******/
SET ANSI_NULLS OFF
GO


SET QUOTED_IDENTIFIER ON
GO
drop table PowerBI_DocsInternos
CREATE TABLE [dbo].PowerBI_DocsInternos(
	[StampRegisto] [char](25) NOT NULL,
	[StampLinhaRegisto] [char](25) NOT NULL,
	[Loja] [varchar](20) NOT NULL,
	[DataDoc] [datetime] NOT NULL,
	[NomeDoc] [varchar](24) NOT NULL,
	[NrDoc] [numeric](10, 0) NOT NULL,
	[DocFechado] [bit] NOT NULL,
	[Nome] [varchar](80) NOT NULL,
	[NCliDep] [varchar](20) NOT NULL,
	[Vendedor] [varchar](20) NOT NULL,
	[ref] [char](18) NOT NULL,
	[design] [varchar](100) NOT NULL,
	[qtt] [numeric](14, 4) NOT NULL,
	[iva] [numeric](5, 2) NOT NULL,
	[tabiva] [numeric](1, 0) NOT NULL,
	[armazem] [numeric](5, 0) NOT NULL,
	[Motivo] [varchar] (60) NOT NULL,
	[Plano] [varchar](200) NOT NULL,
	[LinhaFechada] [bit] NOT NULL,
	[Armazem_Dest] [numeric](5, 0) NOT NULL,
	[local] [varchar](43) NOT NULL,
	[morada] [varchar](55) NOT NULL,
	[codpost] [varchar](45) NOT NULL,
	[lote] [varchar](30) NOT NULL,
	[epu] [numeric](19, 6) NOT NULL,
	[edebito] [numeric](19, 6) NOT NULL,
	[eprorc] [numeric](19, 6) NOT NULL,
	[epcusto] [numeric](19, 6) NOT NULL,
	[ettdeb] [numeric](19, 6) NOT NULL,
	[adoc] [varchar](20) NOT NULL,
	[obistamp] [char](25) NOT NULL,
	[oobistamp] [char](25) NOT NULL,
	[familia] [varchar](18) NOT NULL,
	[desconto] [numeric](6, 2) NOT NULL,
	[desc2] [numeric](5, 2) NOT NULL,
	[desc3] [numeric](5, 2) NOT NULL,
	[desc4] [numeric](5, 2) NOT NULL,
	[ccusto] [varchar](20) NOT NULL,
	[num1] [numeric](19, 5) NOT NULL,
	[pbruto] [numeric](14, 3) NOT NULL,
	[ecustoind] [numeric](19, 6) NOT NULL,
	[u_bencont] [numeric](10, 0) NOT NULL,
	[u_psicont] [numeric](10, 0) NOT NULL,
	[ivaincl] [bit] NOT NULL,
	[NrVenda] [numeric](10, 0) NOT NULL,
	[DocVenda] [varchar](24) NOT NULL,
	[ndoc] [numeric](9, 0) NOT NULL,
	[ftstamp] [char](25) NOT NULL,
	[DataDocVenda] [datetime] NOT NULL,
	[qtRec] [numeric](14, 4) NOT NULL,
	[diploma] [varchar](40) NOT NULL,
	[descval] [numeric](9, 2) NOT NULL,
	[exepcaoTrocaMed] [varchar](50) NOT NULL,
 CONSTRAINT [PK_PowerBI_DocsInternos] PRIMARY KEY NONCLUSTERED 
(
	[StampLinhaRegisto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_StampLinhaRegisto]  DEFAULT ('') FOR [StampLinhaRegisto]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_NomeDoc]  DEFAULT ('') FOR [NomeDoc]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_NrDoc]  DEFAULT ((0)) FOR [NrDoc]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_ref]  DEFAULT ('') FOR [ref]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_design]  DEFAULT ('') FOR [design]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_qtt]  DEFAULT ((0)) FOR [qtt]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_iva]  DEFAULT ((0)) FOR [iva]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_tabiva]  DEFAULT ((0)) FOR [tabiva]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_armazem]  DEFAULT ((0)) FOR [armazem]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_Plano]  DEFAULT ('') FOR [Plano]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_LinhaFechada]  DEFAULT ((0)) FOR [LinhaFechada]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_Armazem_Dest]  DEFAULT ((0)) FOR [Armazem_Dest] 
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_local]  DEFAULT ('') FOR [local]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_morada]  DEFAULT ('') FOR [morada]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_codpost]  DEFAULT ('') FOR [codpost]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_BI_lote]  DEFAULT ('') FOR [lote]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_epu]  DEFAULT ((0)) FOR [epu]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_edebito]  DEFAULT ((0)) FOR [edebito]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_eprorc]  DEFAULT ((0)) FOR [eprorc]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_epcusto]  DEFAULT ((0)) FOR [epcusto]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_ettdeb]  DEFAULT ((0)) FOR [ettdeb]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_adoc]  DEFAULT ('') FOR [adoc]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_obistamp]  DEFAULT ('') FOR [obistamp]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_oobistamp]  DEFAULT ('') FOR [oobistamp]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_familia]  DEFAULT ('') FOR [familia]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_desconto]  DEFAULT ((0)) FOR [desconto]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_desc2]  DEFAULT ((0)) FOR [desc2]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_desc3]  DEFAULT ((0)) FOR [desc3]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_desc4]  DEFAULT ((0)) FOR [desc4]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_ccusto]  DEFAULT ('') FOR [ccusto]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_num1]  DEFAULT ((0)) FOR [num1]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_pbruto]  DEFAULT ((0)) FOR [pbruto]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_ecustoind]  DEFAULT ((0)) FOR [ecustoind]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_StampRegisto]  DEFAULT ('') FOR [StampRegisto]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_u_bencont]  DEFAULT ((0)) FOR [u_bencont]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_u_psicont]  DEFAULT ((0)) FOR [u_psicont]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_ivaincl]  DEFAULT ((0)) FOR [ivaincl]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_DocVenda]  DEFAULT ('') FOR [DocVenda]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_ndoc]  DEFAULT ((0)) FOR [ndoc]


ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_ftstamp]  DEFAULT ('') FOR [ftstamp]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_DataDocVenda]  DEFAULT (CONVERT([datetime],'19000101',(0))) FOR [DataDocVenda]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_Bi_qtRec]  DEFAULT ((0)) FOR [qtRec]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_diploma]  DEFAULT ('') FOR [diploma]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_descval]  DEFAULT ((0)) FOR [descval]
GO

ALTER TABLE [dbo].PowerBI_DocsInternos ADD  CONSTRAINT [DF_bi_exepcaoTrocaMed]  DEFAULT ('') FOR [exepcaoTrocaMed]
GO




/****** Object:  Table [dbo].PowerBI_Pagamentos    

select * from PowerBI_Pagamentos (nolock)
drop table PowerBI_Pagamentos
Script Date: 28/06/2023 17:18:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PowerBI_Pagamentos](
	[StampPagamento] [char](25) NOT NULL,
	[StampLinhaPagam] [char](25) NOT NULL,
	[Loja] [varchar](20) NOT NULL,
	[Nome] [varchar](80) NOT NULL,
	[NrPagam] [numeric](10, 0) NOT NULL,
	[cdesc] [varchar](20) NOT NULL,
	[adoc] [varchar](20) NOT NULL,
	[rec] [numeric](19, 6) NOT NULL,
	[datalc] [datetime] NOT NULL,
	[dataven] [datetime] NOT NULL,
	[fcstamp] [char](25) NOT NULL,
	[cm] [numeric](6, 0) NOT NULL,
	[cambio] [numeric](16, 6) NOT NULL,
	[eval] [numeric](19, 6) NOT NULL,
	[erec] [numeric](19, 6) NOT NULL,
	[process] [bit] NOT NULL,
	[moeda] [varchar](11) NOT NULL,
	[rdata] [datetime] NOT NULL,
	[escrec] [numeric](19, 6) NOT NULL,
	[eivav1] [numeric](19, 6) NOT NULL,
	[eivav2] [numeric](19, 6) NOT NULL,
	[eivav3] [numeric](19, 6) NOT NULL,
	[eivav4] [numeric](19, 6) NOT NULL,
	[eivav5] [numeric](19, 6) NOT NULL,
	[eivav6] [numeric](19, 6) NOT NULL,
	[eivav7] [numeric](19, 6) NOT NULL,
	[eivav8] [numeric](19, 6) NOT NULL,
	[eivav9] [numeric](19, 6) NOT NULL,
	[lordem] [numeric](10, 0) NOT NULL,
	[earred] [numeric](19, 6) NOT NULL,
	[arred] [numeric](18, 5) NOT NULL,
	[enaval] [numeric](19, 6) NOT NULL,
	[desconto] [numeric](6, 2) NOT NULL,
	[evori] [numeric](19, 6) NOT NULL,
	[virs] [numeric](18, 5) NOT NULL,
	[evirs] [numeric](19, 6) NOT NULL,
	[ecambio] [numeric](20, 10) NOT NULL,
	[crend] [varchar](3) NOT NULL,
	[moedoc] [varchar](11) NOT NULL,
	[ousrinis] [varchar](30) NOT NULL,
	[ousrdata] [datetime] NOT NULL,
	[ousrhora] [varchar](8) NOT NULL,
	[usrinis] [varchar](30) NOT NULL,
	[usrdata] [datetime] NOT NULL,
	[usrhora] [varchar](8) NOT NULL,
	[ivatx1] [numeric](5, 2) NOT NULL,
	[ivatx2] [numeric](5, 2) NOT NULL,
	[ivatx3] [numeric](5, 2) NOT NULL,
	[ivatx4] [numeric](5, 2) NOT NULL,
	[ivatx5] [numeric](5, 2) NOT NULL,
	[ivatx6] [numeric](5, 2) NOT NULL,
	[ivatx7] [numeric](5, 2) NOT NULL,
	[ivatx8] [numeric](5, 2) NOT NULL,
	[ivatx9] [numeric](5, 2) NOT NULL,
	[fcorigem] [varchar](2) NOT NULL,
	[exportado] [bit] NOT NULL,
 CONSTRAINT [pk_pl] PRIMARY KEY NONCLUSTERED 
(
	[StampLinhaPagam] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_StampLinhaPagam]  DEFAULT ('') FOR [StampLinhaPagam]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_rno]  DEFAULT ((0)) FOR [NrPagam]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_cdesc]  DEFAULT ('') FOR [cdesc]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_adoc]  DEFAULT ('') FOR [adoc]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_rec]  DEFAULT ((0)) FOR [rec]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_datalc]  DEFAULT (CONVERT([datetime],'19000101',(0))) FOR [datalc]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_dataven]  DEFAULT (CONVERT([datetime],'19000101',(0))) FOR [dataven]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_fcstamp]  DEFAULT ('') FOR [fcstamp]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_cm]  DEFAULT ((0)) FOR [cm]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_cambio]  DEFAULT ((0)) FOR [cambio]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_eval]  DEFAULT ((0)) FOR [eval]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_erec]  DEFAULT ((0)) FOR [erec]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_process]  DEFAULT ((0)) FOR [process]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_moeda]  DEFAULT ('') FOR [moeda]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_rdata]  DEFAULT (CONVERT([datetime],'19000101',(0))) FOR [rdata]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_escrec]  DEFAULT ((0)) FOR [escrec]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_eivav1]  DEFAULT ((0)) FOR [eivav1]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_eivav2]  DEFAULT ((0)) FOR [eivav2]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_eivav3]  DEFAULT ((0)) FOR [eivav3]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_eivav4]  DEFAULT ((0)) FOR [eivav4]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_eivav5]  DEFAULT ((0)) FOR [eivav5]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_eivav6]  DEFAULT ((0)) FOR [eivav6]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_eivav7]  DEFAULT ((0)) FOR [eivav7]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_eivav8]  DEFAULT ((0)) FOR [eivav8]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_eivav9]  DEFAULT ((0)) FOR [eivav9]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_lordem]  DEFAULT ((0)) FOR [lordem]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_earred]  DEFAULT ((0)) FOR [earred]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_arred]  DEFAULT ((0)) FOR [arred]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_enaval]  DEFAULT ((0)) FOR [enaval]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_desconto]  DEFAULT ((0)) FOR [desconto]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_evori]  DEFAULT ((0)) FOR [evori]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_virs]  DEFAULT ((0)) FOR [virs]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_evirs]  DEFAULT ((0)) FOR [evirs]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_ecambio]  DEFAULT ((0)) FOR [ecambio]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_crend]  DEFAULT ('') FOR [crend]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_moedoc]  DEFAULT ('') FOR [moedoc]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_StampPagamento]  DEFAULT ('') FOR [StampPagamento]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_ousrinis]  DEFAULT ('') FOR [ousrinis]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_ousrhora]  DEFAULT ('') FOR [ousrhora]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_usrinis]  DEFAULT ('') FOR [usrinis]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_usrdata]  DEFAULT (getdate()) FOR [usrdata]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_usrhora]  DEFAULT ('') FOR [usrhora]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_ivatx1]  DEFAULT ((0)) FOR [ivatx1]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_ivatx2]  DEFAULT ((0)) FOR [ivatx2]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_ivatx3]  DEFAULT ((0)) FOR [ivatx3]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_ivatx4]  DEFAULT ((0)) FOR [ivatx4]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_ivatx5]  DEFAULT ((0)) FOR [ivatx5]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_ivatx6]  DEFAULT ((0)) FOR [ivatx6]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_ivatx7]  DEFAULT ((0)) FOR [ivatx7]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_ivatx8]  DEFAULT ((0)) FOR [ivatx8]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_ivatx9]  DEFAULT ((0)) FOR [ivatx9]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_fcorigem]  DEFAULT ('') FOR [fcorigem]
GO

ALTER TABLE [dbo].PowerBI_Pagamentos ADD  CONSTRAINT [DF_pl_exportado]  DEFAULT ((0)) FOR [exportado]
GO


/*Create table dates for dates conections between different tables */
CREATE TABLE PowerBI_Dates (
  [Date] DATE,
  PRIMARY KEY ([Date])
)
DECLARE @dIncr DATE = '2015-01-01'
DECLARE @dEnd DATE = '2050-12-31'

WHILE ( @dIncr <= @dEnd )
BEGIN
  INSERT INTO PowerBI_Dates ([Date]) VALUES( @dIncr )
  SELECT @dIncr = DATEADD(DAY, 1, @dIncr )
END


/****** Object:  Table [dbo].[PowerBI_Receituário]    Script Date: 08/04/2024 17:26:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
drop table [PowerBI_Receituário]
CREATE TABLE [dbo].[PowerBI_Receituário](
	[StampRegistoReceita] [char](25) NOT NULL,
	[StampLinhaFatura] [char](25) NOT NULL,
	[cptorgstamp] [varchar](25) NOT NULL,
	[cptorgabrev] [varchar](15) NOT NULL,
	[nr_lote] [numeric](10, 0) NOT NULL,
	[tipolote] [varchar](2) NOT NULL,
	[slote] [numeric](3, 0) NOT NULL,
	[ano] [numeric](4, 0) NOT NULL,
	[mes] [numeric](2, 0) NOT NULL,
	[nr_receita_lote] [numeric](4, 0) NOT NULL,
	[CompSNS] [numeric](16, 5) NOT NULL,
	[CompEnt] [numeric](16, 5) NOT NULL,
	[CompSNS_Siva] [numeric](16, 5) NOT NULL,
	[CompEnt_Siva] [numeric](16, 5) NOT NULL,
	[fechado] [bit] NOT NULL,
	[datafechado] [datetime] NOT NULL,
	[horafechado] [varchar](8) NOT NULL,
	[datavenda] [datetime] NOT NULL,
	[usrlote] [varchar](3) NOT NULL,
	[cptplacode] [varchar](3) NOT NULL,
	[ousrinis] [varchar](30) NOT NULL,
	[ousrdata] [datetime] NOT NULL,
	[ousrhora] [varchar](8) NOT NULL,
	[usrinis] [varchar](30) NOT NULL,
	[usrdata] [datetime] NOT NULL,
	[usrhora] [varchar](8) NOT NULL,
	[marcada] [bit] NOT NULL,
	[u_fistamp] [varchar](25) NOT NULL,
	[Loja] [varchar](20) NOT NULL,
	[no_orig] [numeric](4, 0) NOT NULL,
	[lote_orig] [numeric](10, 0) NOT NULL,
	[posto] [nchar](10) NOT NULL,
	[receita] [varchar](20) NOT NULL,
	[cartao] [varchar](50) NOT NULL,
	[via] [tinyint] NOT NULL,
	[comprovativo_registo] [varchar](max) NOT NULL,
	[token] [varchar](40) NOT NULL,
	[lote_dem] [numeric](10, 0) NOT NULL,
 CONSTRAINT [PK_PowerBI_Receituário] PRIMARY KEY CLUSTERED 
(
	[ano] ASC,
	[mes] ASC,
	[tipolote] ASC,
	[slote] ASC,
	[nr_lote] ASC,
	[cptorgstamp] ASC,
	[StampLinhaFatura] ASC,
	[nr_receita_lote] ASC,
	[Loja] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_cptorgstamp]  DEFAULT ('') FOR [cptorgstamp]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_cptorgabrev]  DEFAULT ('') FOR [cptorgabrev]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_nr_lote]  DEFAULT ((0)) FOR [nr_lote]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_tipolote]  DEFAULT ('') FOR [tipolote]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_slote]  DEFAULT ((0)) FOR [slote]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_ano]  DEFAULT ((0)) FOR [ano]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_mes]  DEFAULT ((0)) FOR [mes]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_nr_receita_lote]  DEFAULT ((0)) FOR [nr_receita_lote]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_fechado]  DEFAULT ((0)) FOR [fechado]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_datafechado]  DEFAULT (CONVERT([datetime],'19000101',(0))) FOR [datafechado]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_horafechado]  DEFAULT ('') FOR [horafechado]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_usrlote]  DEFAULT ('') FOR [usrlote]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_cptplacode]  DEFAULT ('') FOR [cptplacode]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_ousrinis]  DEFAULT ('') FOR [ousrinis]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_ousrhora]  DEFAULT ('') FOR [ousrhora]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_usrinis]  DEFAULT ('') FOR [usrinis]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_usrdata]  DEFAULT (getdate()) FOR [usrdata]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_usrhora]  DEFAULT ('') FOR [usrhora]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_marcada]  DEFAULT ((0)) FOR [marcada]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_u_fistamp]  DEFAULT ('') FOR [u_fistamp]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_loja]  DEFAULT ('') FOR [Loja]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_no_orig]  DEFAULT ((0)) FOR [no_orig]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_lote_orig]  DEFAULT ((0)) FOR [lote_orig]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_posto]  DEFAULT ('') FOR [posto]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_receita]  DEFAULT ('') FOR [receita]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_cartao]  DEFAULT ('') FOR [cartao]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_via]  DEFAULT ((0)) FOR [via]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_comprovativo_registo]  DEFAULT ('') FOR [comprovativo_registo]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_token]  DEFAULT ('') FOR [token]
GO

ALTER TABLE [dbo].[PowerBI_Receituário] ADD  CONSTRAINT [DF_ctltrct_lote_dem]  DEFAULT ((0)) FOR [lote_dem]
GO


/****** Object:  Table [dbo].[Extrato_CC_PBI]    Script Date: 18/12/2024 13:26:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
if OBJECT_ID('[dbo].[Extrato_CC_PBI]') IS NOT NULL
	drop table dbo.[Extrato_CC_PBI]
GO
CREATE TABLE dbo.Extrato_CC_PBI (
    SALDO           decimal(18, 6) NULL,
    DATALC          varchar(10)    NOT NULL,
    DATAVEN         varchar(10)    NULL,
    EDEB            decimal(18, 6) NULL,
    ECRED           decimal(18, 6) NULL,
    EDEBF           decimal(18, 6) NULL,
    ECREDF          decimal(18, 6) NULL,
    VALOR           decimal(18, 6) NULL,
    MOEDA           varchar(3)     NULL,
    CMDESC          varchar(255)   NULL,
    NRDOC           varchar(50)    NULL,
    ULTDOC          varchar(50)    NULL,
    INTID           int            NULL,
    FACSTAMP        varchar(50)    NOT NULL,
    RESTAMP         varchar(50)    NOT NULL,
    CCSTAMP         varchar(50)    NOT NULL,
    CBBNO           varchar(50)    NULL,
    ORIGEM          varchar(255)   NULL,
    OUSRHORA        varchar(8)     NULL,
    OUSRINIS        varchar(10)    NULL,
    IEMISSAO        int            NULL,
    IVENCIMENTO     int            NULL,
    USERNAME        varchar(255)   NULL,
    NO              varchar(50)    NOT NULL,
    NO_ESTAB      	varchar(100)   NULL,
    NOME            varchar(255)   NULL,
    SITE            varchar(20)    NOT NULL,
    CONSTRAINT PK_Extrato_CC_PBI PRIMARY KEY (NO, SITE, FACSTAMP, RESTAMP, CCSTAMP, DATALC)
);

/****** Object:  Table [dbo].[PowerBI_Movimentos]    Script Date: 28/01/2025 10:32:22 ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [CK_PowerBI_Movimentos_cm]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_usrhora]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_usrdata]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_usrinis]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_ousrhora]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_ousrdata]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_ousrinis]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_num1]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_sticstamp]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_usr5]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_usr4]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_usr3]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_usr2]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_usr1]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_epcpond]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_stns]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_bistamp]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_fnstamp]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_fistamp]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_evaliva]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_segmento]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_lote]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_ttmoeda]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_trfa]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_cpoc]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_codigo]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_armazem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_cm]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_frcl]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_origem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_nome]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_moeda]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_vumoeda]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_ncusto]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_ccusto]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_fref]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_ett]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_evu]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_qtt]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_adoc]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_cmdesc]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_datalc]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_design]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_ref]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] DROP CONSTRAINT [DF_PowerBI_Movimentos_slstamp]
GO

/****** Object:  Table [dbo].[PowerBI_Movimentos]    Script Date: 28/01/2025 10:32:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U'))
DROP TABLE [dbo].[PowerBI_Movimentos]
GO

/****** Object:  Table [dbo].[PowerBI_Movimentos]    Script Date: 28/01/2025 10:32:22 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PowerBI_Movimentos](
	[slstamp] [char](25) NOT NULL,
	[ref] [char](18) NOT NULL,
	[design] [varchar](100) NOT NULL,
	[datalc] [datetime] NOT NULL,
	[cmdesc] [varchar](20) NOT NULL,
	[adoc] [varchar](20) NOT NULL,
	[qtt] [numeric](12, 3) NOT NULL,
	[evu] [numeric](19, 6) NOT NULL,
	[ett] [numeric](19, 6) NOT NULL,
	[fref] [varchar](20) NOT NULL,
	[ccusto] [varchar](20) NOT NULL,
	[ncusto] [varchar](20) NOT NULL,
	[vumoeda] [numeric](11, 2) NOT NULL,
	[moeda] [varchar](11) NOT NULL,
	[nome] [varchar](80) NOT NULL,
	[origem] [varchar](2) NOT NULL,
	[frcl] [numeric](10, 0) NOT NULL,
	[cm] [numeric](6, 0) NOT NULL,
	[armazem] [numeric](5, 0) NOT NULL,
	[codigo] [varchar](18) NOT NULL,
	[cpoc] [numeric](6, 0) NOT NULL,
	[trfa] [bit] NOT NULL,
	[ttmoeda] [numeric](15, 3) NOT NULL,
	[lote] [varchar](30) NOT NULL,
	[segmento] [varchar](25) NOT NULL,
	[evaliva] [numeric](19, 6) NOT NULL,
	[fistamp] [char](25) NOT NULL,
	[fnstamp] [char](25) NOT NULL,
	[bistamp] [char](25) NOT NULL,
	[stns] [bit] NOT NULL,
	[epcpond] [numeric](19, 6) NOT NULL,
	[usr1] [varchar](20) NOT NULL,
	[usr2] [varchar](20) NOT NULL,
	[usr3] [varchar](35) NOT NULL,
	[usr4] [varchar](20) NOT NULL,
	[usr5] [varchar](120) NOT NULL,
	[sticstamp] [char](25) NOT NULL,
	[num1] [numeric](19, 5) NOT NULL,
	[ousrinis] [varchar](30) NOT NULL,
	[ousrdata] [datetime] NOT NULL,
	[ousrhora] [varchar](8) NOT NULL,
	[usrinis] [varchar](30) NOT NULL,
	[usrdata] [datetime] NOT NULL,
	[usrhora] [varchar](8) NOT NULL,
 CONSTRAINT [PK_sl] PRIMARY KEY NONCLUSTERED 
(
	[slstamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_slstamp]  DEFAULT ('') FOR [slstamp]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_ref]  DEFAULT ('') FOR [ref]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_design]  DEFAULT ('') FOR [design]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_datalc]  DEFAULT (CONVERT([datetime],'19000101',0)) FOR [datalc]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_cmdesc]  DEFAULT ('') FOR [cmdesc]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_adoc]  DEFAULT ('') FOR [adoc]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_qtt]  DEFAULT ((0)) FOR [qtt]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_evu]  DEFAULT ((0)) FOR [evu]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_ett]  DEFAULT ((0)) FOR [ett]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_fref]  DEFAULT ('') FOR [fref]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_ccusto]  DEFAULT ('') FOR [ccusto]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_ncusto]  DEFAULT ('') FOR [ncusto]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_vumoeda]  DEFAULT ((0)) FOR [vumoeda]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_moeda]  DEFAULT ('') FOR [moeda]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_nome]  DEFAULT ('') FOR [nome]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_origem]  DEFAULT ('') FOR [origem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_frcl]  DEFAULT ((0)) FOR [frcl]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_cm]  DEFAULT ((0)) FOR [cm]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_armazem]  DEFAULT ((0)) FOR [armazem]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_codigo]  DEFAULT ('') FOR [codigo]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_cpoc]  DEFAULT ((0)) FOR [cpoc]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_trfa]  DEFAULT ((0)) FOR [trfa]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_ttmoeda]  DEFAULT ((0)) FOR [ttmoeda]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_lote]  DEFAULT ('') FOR [lote]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_segmento]  DEFAULT ('') FOR [segmento]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_evaliva]  DEFAULT ((0)) FOR [evaliva]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_fistamp]  DEFAULT ('') FOR [fistamp]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_fnstamp]  DEFAULT ('') FOR [fnstamp]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_bistamp]  DEFAULT ('') FOR [bistamp]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_stns]  DEFAULT ((0)) FOR [stns]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_epcpond]  DEFAULT ((0)) FOR [epcpond]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_usr1]  DEFAULT ('') FOR [usr1]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_usr2]  DEFAULT ('') FOR [usr2]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_usr3]  DEFAULT ('') FOR [usr3]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_usr4]  DEFAULT ('') FOR [usr4]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_usr5]  DEFAULT ('') FOR [usr5]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_sticstamp]  DEFAULT ('') FOR [sticstamp]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_num1]  DEFAULT ((0)) FOR [num1]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_ousrinis]  DEFAULT ('') FOR [ousrinis]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_ousrhora]  DEFAULT ('') FOR [ousrhora]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_usrinis]  DEFAULT ('') FOR [usrinis]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_usrdata]  DEFAULT (getdate()) FOR [usrdata]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] ADD  CONSTRAINT [DF_PowerBI_Movimentos_usrhora]  DEFAULT ('') FOR [usrhora]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos]  WITH CHECK ADD  CONSTRAINT [CK_PowerBI_Movimentos_cm] CHECK  ((NOT [cm]=(50)))
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PowerBI_Movimentos]') AND type in (N'U')) 
ALTER TABLE [dbo].[PowerBI_Movimentos] CHECK CONSTRAINT [CK_PowerBI_Movimentos_cm]
GO

