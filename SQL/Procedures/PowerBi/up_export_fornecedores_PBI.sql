
/****** Object:  StoredProcedure [dbo].[up_export_fornecedores_PBI]    Script Date: 14/11/2022 
Criação de tabela de informação de fornecedores para usar no PBI

exec up_export_fornecedores_PBI 'POWERBI','F13156A'

18:02:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'up_export_fornecedores_PBI'))
DROP procedure [dbo].[up_export_fornecedores_PBI]
GO
CREATE procedure [dbo].[up_export_fornecedores_PBI]
	@ipdestino varchar (100),
	@bddestino varchar (100)
	
/* with encryption */
AS
SET NOCOUNT OFF

DECLARE @stm varchar(5000)
DECLARE @destino varchar (200) = '['+@ipdestino+'].['+@bddestino+']'
DECLARE @insertSql varchar(max) = ''
DECLARE @tabelaorigem VARCHAR (50) = 'fl'
DECLARE @tabeladestino VARCHAR (50) = 'PowerBI_fornecedores'

EXEC  up_retorna_insert_sql_dinamico @tabelaorigem,@tabeladestino,@ipdestino,@bddestino, @insertSql OUTPUT
    --PRINT  @insertSql

SET @stm='
DELETE from '+@destino+'.dbo.'+@tabeladestino+' '+CHAR(13)+CHAR(10)

SET @stm = @stm + CHAR(13) + CHAR(10) + @insertSql

PRINT @stm
EXEC (@stm)

