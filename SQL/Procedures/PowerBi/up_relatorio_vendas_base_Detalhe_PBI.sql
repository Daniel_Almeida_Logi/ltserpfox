
/****** Object:  StoredProcedure [dbo].[up_relatorio_vendas_base_Detalhe_PBI]    Script Date: 14/11/2022 18:20:23 
Criação detalhe de vendas para ser utilizado no up_relatorio_vendas_geral_PBI

******/


/****** Object:  StoredProcedure [dbo].[up_relatorio_vendas_base_Detalhe_PBI]    Script Date: 11/01/2024 16:45:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'up_relatorio_vendas_base_Detalhe_PBI'))
DROP procedure [dbo].[up_relatorio_vendas_base_Detalhe_PBI]
GO
CREATE procedure [dbo].[up_relatorio_vendas_base_Detalhe_PBI]
	@dataIni			datetime
	,@dataFim			datetime
	,@site				varchar(200)
	,@incluiHistorico	bit = 0
/* with encryption */
AS
SET NOCOUNT ON
	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosNumAtend'))
		DROP TABLE #dadosNumAtend
	/* Id do site */
	DECLARE @site_nr as varchar (50) = 0

	BEGIN
	IF @site != ''
	set @site_nr = (select no from empresa (nolock)	where empresa.site = @site)			
	END

	/*  Usado no calculo de numero de atendimentos; Usado para validar existencia de Vales ao atendimento */
	Select distinct
		ft.u_nratend
	into
		#dadosNumAtend
	From
		fi (nolock)
		inner join ft (nolock) on fi.ftstamp = ft.ftstamp
	where
		fi.epromo = 1
		and fdata between @dataIni and @dataFim

	/* DOCS DE FACTURACAO */
	if @incluiHistorico = 0
	begin
		SELECT
			nrAtend			= ft.u_nratend
			,ft.ftstamp
			,fi.fistamp
			,ft.fdata
			,ft.no
			,ft.estab
			,b_utentes.Tipo
			,ft.ndoc
			,ft.fno
			,td.tipodoc
			,td.u_tipodoc
			,ref			= case when fi.ref='' then fi.oref else fi.ref end
			,fi.design
			,suspenso = CASE WHEN ft.cobrado = 1 and ft.ndoc!=75 and fi.qtt > isnull((select sum(fi1.qtt) from fi(nolock) fi1 where fi1.ofistamp=fi.fistamp and len(fi1.ofistamp)>0 ),0) THEN 1 ELSE 0 END
			,fi.familia
			,u_epvp			= u_epvp
			,epcpond		= (CASE 
								WHEN ROUND(FI.EPCP,1) = 0 THEN FI.ECUSTO * qtt --se pcp for centimos, vai buscar o pcl
								ELSE FI.EPCP * qtt 
								END)
								*
								(CASE WHEN TD.tipodoc = 3 THEN (-1) else 1 end)
			,fi.iva
			,qtt			= CASE WHEN TD.tipodoc = 3 THEN -qtt ELSE qtt END
			,etiliquido		= case when fi.ivaincl = 1 then fi.etiliquido else fi.etiliquido + (fi.etiliquido*(fi.iva/100)) end
			,etiliquidoSIva = case when fi.ivaincl = 1 then (fi.etiliquido / (fi.iva/100+1)) else fi.etiliquido end
			,ettent1		= case when ft.tipodoc = 3 then u_ettent1*-1 else u_ettent1 end
			,ettent2		= case when ft.tipodoc = 3 then u_ettent2*-1 else u_ettent2 end
			,ettent1SIva	= case when ft.tipodoc = 3 then (u_ettent1/(fi.iva/100+1))*-1 else (u_ettent1/(fi.iva/100+1)) end
			,ettent2SIva	= case when ft.tipodoc = 3 then (u_ettent2/(fi.iva/100+1))*-1 else (u_ettent2/(fi.iva/100+1)) end
			,fi.desconto
			,descvalor		= ((case 
								when fi.desconto = 100 then fi.epv * fi.qtt 
								when (fi.desconto between 0.01 and 99.99) then (fi.epv * fi.qtt) - abs(fi.etiliquido)
								else 0 
								end)
								--+
								-u_descval
								)
								*
								(CASE WHEN TD.tipodoc = 3 THEN (-1) else 1 end)
			-- Só descontos em valor e que correspondem a um vale
			,descvale		= case  
								when ft.u_nratend in (select u_nratend from #dadosNumAtend) THEN u_descval 
								ELSE 0 
								END
			,ousrhora		= Ft.ousrhora
			,ousrinis		= ft.ousrinis
			,vendnm			= ft.vendnm
			,AbrevEnt1		= CASE WHEN ft2.u_abrev =  '' and fi.ofistamp != '' 
									THEN (SELECT a.U_ABREV  from ft2 (nolock) a  
											where a.ft2stamp = (SELECT b.FTSTAMP from fi (nolock) b 
																	where b.fistamp = fi.ofistamp
																	)
											) 
									ELSE ft2.u_abrev
									END
			,NomeEnt1		= CASE WHEN ft2.u_abrev =  '' and fi.ofistamp != '' 
									THEN (SELECT a.nome from cptorg (nolock) a 
											where a.abrev = (SELECT a.U_ABREV  from ft2 (nolock) a 
																where a.ft2stamp = (SELECT b.FTSTAMP from fi (nolock) b 
																						where b.fistamp = fi.ofistamp))) 
									ELSE cptorg.nome 
									END
			,AbrevEnt2		= CASE WHEN ft2.u_abrev2 =  '' and fi.ofistamp != '' 
									THEN (SELECT a.U_ABREV2  from ft2 (nolock) a  
											where a.ft2stamp = (SELECT b.FTSTAMP from fi (nolock) b 
																	where b.fistamp = fi.ofistamp
																	)
											) 
									ELSE ft2.u_abrev2
									END
			,NomeEnt2		= CASE WHEN ft2.u_abrev2 = '' and fi.ofistamp != '' and (SELECT a.U_ABREV2  from ft2 (nolock) a where a.ft2stamp = (SELECT b.FTSTAMP from fi (nolock) b where b.fistamp = fi.ofistamp)) = ''
									THEN (SELECT a.nome from cptorg (nolock) a 
											where a.abrev = (SELECT a.U_ABREV2 from ft2 (nolock) a 
																where a.ft2stamp = (SELECT b.FTSTAMP from fi (nolock) b 
																						where b.fistamp = fi.ofistamp)))
									ELSE (SELECT a.nome from cptorg (nolock) a 
											where a.abrev = (SELECT a.U_ABREV2 from ft2 (nolock) a 
																where a.ft2stamp = (SELECT b.FTSTAMP from fi (nolock) b 
																						where b.fistamp = fi.fistamp)))
									END
			,ft.u_ltstamp
			,ft.u_ltstamp2
			,ecusto			= (fi.ecusto * qtt) * (CASE WHEN TD.tipodoc = 3 THEN (-1) else 1 end)
			,ft.site
			,site_nr = (select empresa.no from empresa where empresa.site = ft.site)
			--,ecustoEPCP =(fi.epcp * qtt) * (CASE WHEN TD.tipodoc = 3 THEN (-1) else 1 end)
			,ft.usrdata
		FROM
			FI				(nolock)
			LEFT JOIN FI2	(nolock) on FI.FISTAMP = FI2.FISTAMP
			LEFT JOIN FT	(nolock) on FI.FTSTAMP = FT.FTSTAMP
			LEFT JOIN FT2	(nolock) on FT2.FT2STAMP = FT.FTSTAMP
			LEFT  JOIN CPTORG (nolock)on CPTORG.ABREV = FT2.U_ABREV
			INNER JOIN TD	(nolock) on TD.NDOC = FT.NDOC
			INNER JOIN B_UTENTES (nolock) on ft.no = b_utentes.no and ft.estab = b_utentes.estab
			INNER JOIN empresa (nolock) on empresa.site = ft.site
		WHERE
			fi.composto = 0
			--AND B_UTENTES.inactivo = 0
			AND ft.anulado = 0
			AND (FT.tipodoc != 4 or u_tipodoc = 4)
			AND fdata between @dataIni and @dataFim
			and ft.site = CASE WHEN @site != '' THEN @site ELSE ft.site END
	end else begin
		SELECT
			nrAtend			= ft.u_nratend
			,ft.ftstamp
			,fi.fistamp
			,ft.fdata
			,ft.no
			,ft.estab
			,b_utentes.Tipo
			,ft.ndoc
			,ft.fno
			,td.tipodoc
			,td.u_tipodoc
			,ref			= case when fi.ref='' then fi.oref else fi.ref end
			,fi.design
			,suspenso = CASE WHEN ft.cobrado = 1 and ft.ndoc!=75 and fi.qtt > isnull((select sum(fi1.qtt) from fi(nolock) fi1 where fi1.ofistamp=fi.fistamp and len(fi1.ofistamp)>0 ),0) THEN 1 ELSE 0 END
			,fi.familia
			,u_epvp			= u_epvp
			,epcpond		= (CASE 
								WHEN ROUND(FI.EPCP,1) = 0 THEN FI.ECUSTO * qtt --se pcp for menos de (+/-) 10 centimos, vai buscar o pcl
								ELSE FI.EPCP * qtt 
								END)
								*
								(CASE WHEN TD.tipodoc = 3 THEN (-1) else 1 end)
			,fi.iva
			,qtt			= CASE WHEN TD.tipodoc = 3 THEN -qtt ELSE qtt END
			,etiliquido		= case when fi.ivaincl = 1 then fi.etiliquido else fi.etiliquido + (fi.etiliquido*(fi.iva/100)) end
			,etiliquidoSIva = case when fi.ivaincl = 1 then (fi.etiliquido / (fi.iva/100+1)) else fi.etiliquido end
			,ettent1		= case when ft.tipodoc = 3 then u_ettent1*-1 else u_ettent1 end
			,ettent2		= case when ft.tipodoc = 3 then u_ettent2*-1 else u_ettent2 end
			,ettent1SIva	= case when ft.tipodoc = 3 then (u_ettent1/(fi.iva/100+1))*-1 else (u_ettent1/(fi.iva/100+1)) end
			,ettent2SIva	= case when ft.tipodoc = 3 then (u_ettent2/(fi.iva/100+1))*-1 else (u_ettent2/(fi.iva/100+1)) end
			,fi.desconto
			,descvalor		= ((case 
								when fi.desconto = 100 then fi.epv * fi.qtt 
								when (fi.desconto between 0.01 and 99.99) then (fi.epv * fi.qtt) - abs(fi.etiliquido)
								else 0 
								end)
								-u_descval
								)
								*
								(CASE WHEN TD.tipodoc = 3 THEN (-1) else 1 end)
			-- Só descontos em valor e que correspondem a um vale
			,descvale		= case when ft.u_nratend in (select u_nratend from #dadosNumAtend) THEN u_descval ELSE 0 END
			,ousrhora		= Ft.ousrhora
			,ousrinis		= ft.ousrinis
			,vendnm			= ft.vendnm
			,AbrevEnt1		= CASE WHEN ft2.u_abrev =  '' and fi.ofistamp != '' 
									THEN (SELECT a.U_ABREV  from ft2 (nolock) a  
											where a.ft2stamp = (SELECT b.FTSTAMP from fi (nolock) b 
																	where b.fistamp = fi.ofistamp
																	)
											) 
									ELSE ft2.u_abrev
									END
			,NomeEnt1		= CASE WHEN ft2.u_abrev =  '' and fi.ofistamp != '' 
									THEN (SELECT a.nome from cptorg (nolock) a 
											where a.abrev = (SELECT a.U_ABREV  from ft2 (nolock) a 
																where a.ft2stamp = (SELECT b.FTSTAMP from fi (nolock) b 
																						where b.fistamp = fi.ofistamp))) 
									ELSE cptorg.nome 
									END
			,AbrevEnt2		= CASE WHEN ft2.u_abrev2 =  '' and fi.ofistamp != '' 
									THEN (SELECT a.U_ABREV2  from ft2 (nolock) a  
											where a.ft2stamp = (SELECT b.FTSTAMP from fi (nolock) b 
																	where b.fistamp = fi.ofistamp
																	)
											) 
									ELSE ft2.u_abrev2
									END
			,NomeEnt2		= CASE WHEN ft2.u_abrev2 = '' and fi.ofistamp != '' and (SELECT a.U_ABREV2  from ft2 (nolock) a where a.ft2stamp = (SELECT b.FTSTAMP from fi (nolock) b where b.fistamp = fi.ofistamp)) = ''
									THEN (SELECT a.nome from cptorg (nolock) a 
											where a.abrev = (SELECT a.U_ABREV2 from ft2 (nolock) a 
																where a.ft2stamp = (SELECT b.FTSTAMP from fi (nolock) b 
																						where b.fistamp = fi.ofistamp)))
									ELSE (SELECT a.nome from cptorg (nolock) a 
											where a.abrev = (SELECT a.U_ABREV2 from ft2 (nolock) a 
																where a.ft2stamp = (SELECT b.FTSTAMP from fi (nolock) b 
																						where b.fistamp = fi.fistamp)))
									END			,ft.u_ltstamp
			,ft.u_ltstamp2
			,ecusto = (fi.ecusto * qtt) * (CASE WHEN TD.tipodoc = 3 THEN (-1) else 1 end)
			,ft.site
			,site_nr = (select empresa.no from empresa where empresa.site = ft.site)
			--,ecustoEPCP =(fi.epcp * qtt) * (CASE WHEN TD.tipodoc = 3 THEN (-1) else 1 end)
			,ft.usrdata
		FROM
			FI				(nolock)
			INNER JOIN FT	(nolock) on FI.FTSTAMP = FT.FTSTAMP
			INNER JOIN FT2	(nolock) on FT2.FT2STAMP = FT.FTSTAMP
			LEFT  JOIN CPTORG (nolock)on CPTORG.ABREV = FT2.U_ABREV
			INNER JOIN TD	(nolock) on TD.NDOC = FT.NDOC
			INNER JOIN B_UTENTES (nolock) on ft.no = b_utentes.no and ft.estab = b_utentes.estab
			INNER JOIN empresa (nolock) on empresa.site = ft.site
		WHERE
			fi.composto = 0
			--AND B_UTENTES.inactivo = 0
			AND ft.anulado = 0
			AND (FT.tipodoc != 4 or u_tipodoc = 4)
			AND fdata between @dataIni and @dataFim
			and ft.site = CASE WHEN @site != '' THEN @site ELSE ft.site END		
		union all
		SELECT
			nrAtend			= '0000000199'
			,ftstamp		= 'x'
			,fdata			= dateadd(mm, (hv.ano - 1900) * 12 + hv.mes - 1 , case when hv.dia=0 then 28 else hv.dia end - 1)
			,no				= 0
			,estab			= 0
			,Tipo			= 'x'
			,ndoc			= 3
			,fno			= 0
			,tipodoc		= 1
			,u_tipodoc		= 8
			,ref			= hv.ref
			,design			= st.design
			,suspenso		= 0
			,familia		
			,u_epvp			= hv.pvp
			,epcpond		= CASE
								WHEN st.epcpond = 0 THEN st.epcult * hv.qt
								ELSE st.epcpond * hv.qt
								END
			,iva			= taxasiva.taxa
			,qtt			= hv.qt
			,etiliquido		= hv.pvp*hv.qt
			,etiliquidoSIva = hv.pvp / (taxasiva.taxa/100+1)
			,ettent1		= 0
			,ettent2		= 0
			,ettent1SIva	= 0
			,ettent2SIva	= 0
			,desconto		= 0
			,descvalor		= 0
			,descvale		= 0
			,ousrhora		= hv.hora
			,ousrinis		= 'ADM'
			,vendnm			= 'ADM'
			,u_ltstamp		= ''
			,u_ltstamp2		= ''
			,ecusto = 0
			,hv.site
			,site_nr = CASE WHEN @site != '' THEN @site ELSE st.site_nr END	
			--,ecustoEPCP = 0
			,usrdata		=getdate()
		FROM
			hist_vendas hv	(nolock)
			inner join empresa(nolock) on empresa.site = hv.site
			inner join st (nolock) on st.ref = hv.ref and st.site_nr = empresa.no
			inner join taxasiva (nolock) on taxasiva.codigo = st.tabiva
		WHERE		
			hv.site = CASE WHEN @site != '' THEN @site ELSE hv.site END
			and st.site_nr = CASE WHEN @site != '' THEN @site_nr ELSE st.site_nr END
			and dateadd(mm, (hv.ano - 1900) * 12 + hv.mes - 1 , case when hv.dia=0 then 28 else hv.dia end - 1)	between @dataIni and @dataFim
	end
	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosNumAtend'))
		DROP TABLE #dadosNumAtend

