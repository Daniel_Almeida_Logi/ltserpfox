/****** Object:  StoredProcedure [dbo].[up_Export_Movimentos_PBI] 

exec [up_Export_Movimentos_PBI] '20240101','20250128','POWERBI','F01179A',''

SP para exportar docs da SL para a BD do PBI:	

   Script Date: 28/01/2025 10:45:00 JB
   Last Change: 28/01/2025 10:45:00 JB
   ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'up_Export_Movimentos_PBI'))
DROP procedure [dbo].[up_Export_Movimentos_PBI]
GO
CREATE procedure [dbo].[up_Export_Movimentos_PBI]
	@dataIni		datetime
	,@dataFim		datetime
	,@ipdestino		varchar(100)
	,@bddestino		varchar(100)
	,@site			varchar (50)
/*

*/
/* with encryption */
AS
SET NOCOUNT OFF

/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#movimentos'))
		DROP TABLE #movimentos
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#EmpresaArm'))
		DROP TABLE #EmpresaArm

		
/* Calc Loja e Armazens */
	DECLARE @site_nr as varchar (50) = 0

	BEGIN
	IF @site != ''
	set @site_nr = (select no from empresa (nolock)	where empresa.site = @site)
					
	END
	   	
	select 
		armazem
	into
		#EmpresaArm
	From
		empresa (nolock)
		inner join empresa_arm on empresa.no = empresa_arm.empresa_no
	where
		empresa.site = case when @site != '' then @site ELSE empresa.site end


SELECT [slstamp]
      ,[ref]
      ,[design]
      ,[datalc]
      ,[cmdesc]
      ,[adoc]
      ,[qtt]
      ,[evu]
      ,[ett]
      ,[fref]
      ,[ccusto]
      ,[ncusto]
      ,[vumoeda]
      ,[moeda]
      ,[nome]
      ,[origem]
      ,[frcl]
      ,[cm]
      ,[armazem]
      ,[codigo]
      ,[cpoc]
      ,[trfa]
      ,[ttmoeda]
      ,[lote]
      ,[segmento]
      ,[evaliva]
      ,[fistamp]
      ,[fnstamp]
      ,[bistamp]
      ,[stns]
      ,[epcpond]
      ,[usr1]
      ,[usr2]
      ,[usr3]
      ,[usr4]
      ,[usr5]
      ,[sticstamp]
      ,[num1]
      ,[ousrinis]
      ,[ousrdata]
      ,[ousrhora]
      ,[usrinis]
      ,[usrdata]
      ,[usrhora]
	into #movimentos
  FROM [dbo].[sl] (nolock)
  	WHERE  sl.armazem in (select #EmpresaArm.armazem from #EmpresaArm)
		and sl.ousrdata between CONVERT(varchar,@dataini,112) and CONVERT(varchar,@datafim,112)

	DECLARE @INSERT varchar(max)
	DECLARE @DELETE varchar (max)
	DECLARE @destino varchar (200) = '['+@ipdestino+'].['+@bddestino+']'
	DECLARE @tabeladestino VARCHAR (50) = 'PowerBI_Movimentos'	

	SET @DELETE = 
	/*apagar slstamps que por algum motivo já não existam na origem*/
	'DELETE from '+@destino+'.dbo.'+@tabeladestino+' WHERE slstamp IN (
		SELECT b.slstamp FROM '+@destino+'.dbo.'+@tabeladestino+' b
		LEFT JOIN sl a 
			ON a.slstamp = b.slstamp 
		where a.slstamp COLLATE DATABASE_DEFAULT IS NULL)  
		
'
	/*apagar entradas com data de alteração entre as datas definidas*/
	+
	' DELETE b from '+@destino+'.dbo.'+@tabeladestino+' b INNER JOIN sl a 
			ON a.slstamp = b.slstamp 
					where a.USRDATA between '''+CONVERT(varchar,@dataini,112)
						+''' and '''+CONVERT(varchar,@datafim,112)+'''' 

	PRINT @DELETE
	EXEC (@DELETE)

	SET @INSERT=' 
	INSERT into '+@destino+'.dbo.'+@tabeladestino+' '
	
	SET @INSERT= @INSERT + 'select #movimentos.* from #movimentos 
							LEFT JOIN '+@destino+'.dbo.'+@tabeladestino+' b with (nolock)
								ON #movimentos.slstamp COLLATE DATABASE_DEFAULT = b.slstamp COLLATE DATABASE_DEFAULT
							where b.slstamp COLLATE DATABASE_DEFAULT IS NULL ' 

	EXEC (@INSERT)
	PRINT @INSERT

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#movimentos'))
		DROP TABLE #movimentos
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#EmpresaArm'))
		DROP TABLE #EmpresaArm

GO
Grant Execute on dbo.up_Export_Movimentos_PBI to Public
Grant control on dbo.up_Export_Movimentos_PBI to Public
Go