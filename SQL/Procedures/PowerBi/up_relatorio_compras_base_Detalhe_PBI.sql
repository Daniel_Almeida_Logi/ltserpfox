
/****** Object:  StoredProcedure [dbo].[up_relatorio_compras_base_Detalhe_PBI]    Script Date: 16/12/2022 13:07:20 

Criação de detalhe de compras para ser utilizado pelo up_relatorio_Compras_geral_PBI
******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_relatorio_compras_base_Detalhe_PBI]') IS NOT NULL
	drop procedure dbo.up_relatorio_compras_base_Detalhe_PBI
go

CREATE procedure [dbo].[up_relatorio_compras_base_Detalhe_PBI]
	@dataIni			datetime
	,@dataFim			datetime
	,@site				varchar(200)
/* with encryption */
AS
SET NOCOUNT ON
	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosNumAtend'))
		DROP TABLE #dadosNumAtend
	/* Id do site */
	DECLARE @site_nr as varchar (50) = 0

	BEGIN
	IF @site != ''
	set @site_nr = (select no from empresa (nolock)	where empresa.site = @site)			
	END

	/* DOCS DE FACTURACAO */
		SELECT
			fn.fntamp
			,fo.docdata
			,fo.pdata
			,fo.no
			,fo.estab
			,fl.Tipo
			,fo.doccode
			,fo.adoc
			,fo.docnome
			,cm1.cm
			,ref			= case when fn.ref='' then fn.oref else fn.ref end
			,fn.design
			,fn.familia
			,u_epvp			= st.epv1
			,fn.u_validade
			,pcp		= sl.epcpond
			,fn.iva
			,qtt			= fn.qtt
			,etiliquido		= case when fn.ivaincl = 1 then fn.etiliquido else fn.etiliquido + (fn.etiliquido*(fn.iva/100)) end
			,etiliquidoSIva = case when fn.ivaincl = 1 then (fn.etiliquido / (fn.iva/100+1)) else fn.etiliquido end
			,fn.desconto
			,descvalor		= ((case 
								when fn.desconto = 100 then fn.epv * fn.qtt 
								when (fn.desconto between 0.01 and 99.99) then (fn.epv * fn.qtt) - abs(etiliquido)
								else 0 
								end)
								)
								*
								(CASE WHEN cm1.cm < 50 THEN (-1) else 1 end)
			,ousrhora		= Fo.ousrhora
			,ousrinis		= fo.ousrinis
			,pcl			= fn.eslvu
			,pct 			= fn.eslvu
			,fo.site
			,site_nr = (select empresa.no from empresa where empresa.site = fo.site)	
			,fo.final
			,fo.usrdata
		FROM
			FN				(nolock)
			INNER JOIN FO	(nolock) on Fn.FoSTAMP = Fo.FoSTAMP
			INNER JOIN cm1	(nolock) on cm1.cm = fo.doccode
			INNER JOIN fl (nolock) on fo.no = fl.no and fo.estab = fl.estab
			INNER join empresa (nolock) on empresa.site = fo.site
			inner join sl	(nolock) on sl.fnstamp=fn.fnstamp
			inner join st	(nolock) on st.ref=fn.ref and st.site_nr=(select empresa.no from empresa inner join empresa_arm on empresa.no = empresa_arm.empresa_no where empresa_arm.armazem = fn.armazem)
		WHERE
			fn.composto = 0
			AND cm1.folansl = 1
			and fn.ref<>''
			and CONVERT(varchar, fn.usrdata, 112) between CONVERT(varchar, @dataIni, 112) and CONVERT(varchar, @dataFim, 112)
			and fo.site = CASE WHEN @site != '' THEN @site ELSE fo.site END

