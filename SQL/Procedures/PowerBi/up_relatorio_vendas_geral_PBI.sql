
/****** Object:  StoredProcedure [dbo].[up_relatorio_vendas_geral_PBI]  

exec up_relatorio_vendas_geral_PBI '20250107','20250107','Loja 1','POWERBI','F01078A'
select * from ft (nolock) where fdata = '20230711' and nmdoc like '%reg%'
  Script Date: 16/02/2023 18:01:45 ******/

/****** Object:  StoredProcedure [dbo].[up_relatorio_vendas_geral_PBI]    Script Date: 11/01/2024 16:44:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[up_relatorio_vendas_geral_PBI]
	@dataIni		datetime
	,@dataFim		datetime
	,@site			varchar(55)
	,@ipdestino		varchar(100)
	,@bddestino		varchar(100)
/* with encryption */
AS
SET NOCOUNT OFF
	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
		DROP TABLE #dadosFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRefs'))
		DROP TABLE #dadosRefs
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
		DROP TABLE #dadosSt	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosTd'))
		DROP TABLE #dadosTd	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseFiltro'))
		DROP TABLE #dadosVendasBaseFiltro	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseQuotaLab'))
		DROP TABLE #dadosVendasBaseQuotaLab	
		
	/* Calc Loja e Armazens */
	DECLARE @site_nr as varchar (50) = 0

	BEGIN
	IF @site != ''
	set @site_nr = (select no from empresa (nolock)	where empresa.site = @site)
					
	END
	   	
	select 
		armazem
	into
		#EmpresaArm
	From
		empresa
		inner join empresa_arm on empresa.no = empresa_arm.empresa_no
	where
		empresa.site = case when @site != '' then @site ELSE empresa.site end	
		
	/* Calc Documentos */
	Select
		ndoc
		,nmdoc
		,tipodoc
		,u_tipodoc
	into
		#dadosTd
	from
		td
	/* Calc familias */
	Select
		distinct ref, nome
	into
		#dadosFamilia
	From
		stfami (nolock)
	/* Calc operadores */
	Select
		iniciais, cm = userno, username
	into
		#dadosOperadores
	from
		b_us (nolock)
	/* Calc Informaçăo Produto */
	Select
		st.ref
		,st.design
		,st.site_nr
		,usr1
		,u_lab
		,departamento = isnull(a.descr ,'')
		,seccao = isnull(b.descr,'')
		,categoria = isnull(c.descr,'')
		,famfamilia =  isnull(d.descr,'')
		,dispdescr = ISNULL(dispdescr,'')
		,generico = ISNULL(generico,CONVERT(bit,0))
		,psico = ISNULL(psico,CONVERT(bit,0))
		,benzo = ISNULL(benzo,CONVERT(bit,0))
		,protocolo = ISNULL(protocolo,CONVERT(bit,0))
		,dci = ISNULL(dci,'')
		,grphmgcode = ISNULL(grphmgcode,'')
		,valDepartamento = a.id
		,valSeccao = b.id
		,valCategoria = c.id
		,valFamfamilia = d.id
	INTO
		#dadosSt
	From
		st (nolock)
		left join fprod (nolock) on st.ref = fprod.cnp
		left join grande_mercado_hmr (nolock) A on A.id = st.u_depstamp
		left join mercado_hmr (nolock) B on B.id = st.u_secstamp
		left join categoria_hmr (nolock) C on C.id = st.u_catstamp
		left join segmento_hmr (nolock) D on D.id = st.u_segstamp
	Where
		site_nr = CASE WHEN @site != '' THEN @site_nr ELSE st.site_nr END

	/* Define se vais mostrar resultados abaixo do Cliente 199 */
	declare @Entidades as bit
	set @Entidades = case when 'FARMACIA' in (select tipoempresa from empresa) OR 'PARAFARMACIA' in (select tipoempresa from empresa) then convert(bit,1) else convert(bit,0) end
	/* Dados Base Vendas  */
	create table #dadosVendasBase (
		nrAtend varchar(50)
		,ftstamp varchar(25)
		,fistamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,suspenso bit
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(25)
		,AbrevEnt1 varchar (15)
		,NomeEnt1 varchar(80)
		,AbrevEnt2 varchar (15)
		,NomeEnt2 varchar(80)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)
		,usrdata datetime
	)
    insert #dadosVendasBase
    exec up_relatorio_vendas_base_detalhe_PBI @dataIni, @dataFim, @site
	/* Elimina Vendas tendo em conta tipo de cliente*/
	If @Entidades = 1
	delete from #dadosVendasBase where #dadosVendasBase.no < 199
	/* Preparar Result Set */
	Select distinct
		[nrAtend]
		,[ftstamp]
		,[fistamp]
		,[loja]
		,[fdata]
		,[no]
		,estab
		,#dadosVendasBase.[ndoc]
		,#dadosTd.[nmdoc]
		,[fno]
		,#dadosVendasBase.[tipodoc]
		,#dadosVendasBase.[u_tipodoc]
		,#dadosVendasBase.[ref]
		,#dadosVendasBase.[design]
		,suspenso
		,#dadosVendasBase.[familia]
		,#dadosfamilia.[nome] as faminome
		,CASE WHEN #dadosVendasBase.tipodoc = 1 THEN u_epvp WHEN #dadosVendasBase.tipodoc = 3 THEN (u_epvp*-1) END as u_epvp
		,[epcpond]
		,[iva]
		,[qtt]
		,[etiliquido]
		,[etiliquidoSiva]
		,[ettent1]
		,[ettent2]
		,[ettent1Siva]
		,[ettent2Siva]
		,[desconto]
		,descvalor=CASE WHEN #dadosVendasBase.[tipodoc] != 3 THEN ABS(#dadosVendasBase.[descvalor]) ELSE ABS([descvalor])*-1 END
		,[descvale]
		,[ousrhora]
		,[ousrinis]
		,[vendnm]
		,[AbrevEnt1]
		,[NomeEnt1]
		,[AbrevEnt2]
		,[NomeEnt2]
		,[u_ltstamp]
		,[u_ltstamp2]
		,[usr1]
		,[u_lab]
		,[departamento]
		,[seccao]
		,[categoria]
		,[famfamilia]
		,[dispdescr]
		,[generico]
		,[psico]
		,[benzo]
		,[protocolo]
		,[dci]
		,[valDepartamento]
		,[valSeccao]
		,[valCategoria]
		,[valFamfamilia]
		,usrdata
	into
		#dadosVendasBaseFiltro
	From
		#dadosVendasBase 
		inner join #dadosTd on #dadosTd.ndoc = #dadosVendasBase.ndoc
		left Join #dadosSt on #dadosVendasBase.ref = #dadosSt.ref and #dadosVendasBase.loja_nr = #dadosSt.site_nr
		left join #dadosFamilia on #dadosVendasBase.familia = #dadosfamilia.ref
	WHERE
		#dadosVendasBase.tipo != 'ENTIDADE'
		AND #dadosVendasBase.u_tipodoc not in (1, 5)
		AND isnull(#dadosVendasBase.familia,'99') in (Select ref from #dadosFamilia)
											 
	DECLARE @DELETE varchar(5000)
	DECLARE @INSERT varchar(5000)
	DECLARE @destino varchar (200) = '['+@ipdestino+'].['+@bddestino+']'
	DECLARE @tabeladestino VARCHAR (50) = 'PowerBI_dadosVendasBase'	
	DECLARE @insertSql varchar(max) = ''
	DECLARE @tabelaorigem VARCHAR (100) = '#dadosVendasBaseFiltro'
	EXEC  up_retorna_insert_sql_dinamico @tabelaorigem,@tabeladestino,@ipdestino,@bddestino, @insertSql OUTPUT
	SET @DELETE = 'DELETE from	'+@destino+'.dbo.'+@tabeladestino+' 
					where USRDATA between '''+CONVERT(varchar,@dataini,112)+''' and '''+CONVERT(varchar,@datafim,112)+''''

	IF @site != ''
	SET @DELETE = @DELETE + ' AND Loja ='''+@site+''''

	EXEC (@DELETE)
	PRINT @DELETE				
	
	set @INSERT = @insertSql + 'LEFT JOIN '+@destino+'.dbo.'+@tabeladestino+' b with (nolock) 
									ON b.fistamp COLLATE DATABASE_DEFAULT = a.fistamp COLLATE DATABASE_DEFAULT
								where b.fistamp COLLATE DATABASE_DEFAULT IS NULL'
	IF @site != ''
	SET @INSERT = @INSERT + ' AND a.Loja ='''+@site+''''

	PRINT @INSERT
	EXEC (@INSERT)
		
	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
		DROP TABLE #dadosFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRefs'))
		DROP TABLE #dadosRefs
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
		DROP TABLE #dadosSt
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosTd'))
		DROP TABLE #dadosTd
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.##dadosVendasBaseFiltro'))
		DROP TABLE #dadosVendasBaseFiltro	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseQuotaLab'))
		DROP TABLE #dadosVendasBaseQuotaLab	
