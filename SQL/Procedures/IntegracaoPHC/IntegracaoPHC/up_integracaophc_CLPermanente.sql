-- exec up_integracaophc_CLPermanente
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_integracaophc_CLPermanente]') IS NOT NULL
    drop procedure up_integracaophc_CLPermanente
go

create PROCEDURE up_integracaophc_CLPermanente
/* WITH ENCRYPTION */
AS

	DECLARE @sql varchar(max)
	set @sql = N'
		insert into b_utentes (
			utstamp
			,nome
			,no
			,estab
			,ncont
			,morada
			,codpost
			,local
			,zona
			,fax
			,telefone
			,tlmvl
			,tipo
			,email
			,bino
			,nascimento
			,codigoP
			,descP
			,ousrinis
			,ousrdata
			,ousrhora
			,usrinis
			,usrdata
			,usrhora
			,idstamp
			,codpla
			,despla
		)
		Select 
			LEFT(newid(),25)
			,nome
			,no =  (Select isnull(max(no),1) from b_utentes) + ROW_NUMBER() OVER (ORDER BY (select 0))
			,estab = 0
			,ncont
			,Morada
			,CodPost
			,Local
			,Zona
			,Fax
			,Telefone
			,TLMVL
			,Tipo
			,Email
			,bino
			,nascimento
			,''PT''
			,''PORTUGAL''
			,ousrinis
			,ousrdata
			,ousrhora
			,usrinis
			,usrdata
			,usrhora
			,clstamp
			,''999998''
			,''SEM COMPARTICIPAÇÃO P/ SNS''
		From 
			'+ (select RTRIM(LTRIM(textValue)) from B_Parameters where stamp = 'ADM0000000233') + '.dbo.cl
		Where
			clstamp not in (select idstamp from b_utentes (nolock))	
			
		update b_utentes
		set idstamp = cl.clstamp
		    ,nome = cl.nome
			,Morada = cl.Morada
			,CodPost = cl.CodPost
			,Local = cl.Local
			,Zona = cl.Zona
			,Fax = cl.Fax
			,Telefone = cl.Telefone
			,TLMVL = cl.TLMVL
			,Tipo = cl.Tipo
			,Email = cl.Email
			,bino = cl.bino
			,nascimento = cl.nascimento 
		From	
			'+ (select RTRIM(LTRIM(textValue)) from B_Parameters where stamp = 'ADM0000000233') + '.dbo.cl 
			inner join b_utentes (nolock) on cl.clstamp = b_utentes.idstamp
		Where
			cl.clstamp != b_utentes.idstamp
			OR cl.nome != b_utentes.nome
			OR cl.Morada != b_utentes.Morada
			OR cl.CodPost != b_utentes.CodPost
			OR cl.Local != b_utentes.Local
			OR cl.Zona != b_utentes.Zona
			OR cl.Fax != b_utentes.Fax
			OR cl.Telefone != b_utentes.Telefone
			OR cl.TLMVL != b_utentes.TLMVL
			OR cl.Tipo != b_utentes.Tipo
			OR cl.Email != b_utentes.Email
			OR cl.bino != b_utentes.bino
			OR cl.nascimento != b_utentes.nascimento	
	'
	print @sql
	execute (@sql)
	
GO
Grant Execute On up_integracaophc_CLPermanente to Public
Grant Control On up_integracaophc_CLPermanente to Public
go