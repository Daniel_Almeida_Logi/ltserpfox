-- exec up_integracaophc_verificaComandosPhc 'D:\PHC\PHC2013\mteixeira2',1
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_integracaophc_verificaComandosPhc]') IS NOT NULL
    drop procedure up_integracaophc_verificaComandosPhc
go

create PROCEDURE up_integracaophc_verificaComandosPhc
@machine as varchar(254),
@userno as int 

/* WITH ENCRYPTION */
AS

	/*Elimina comandos com mais de 3 minutos*/
	delete from B_cli_integraPhc where data < DATEADD(MINUTE,-3,GETDATE())
	
	Select 
		top 1 * 
	from 
		b_cli_integraPhc 
	where 
		userno = @userno
		and executado = 0
	Order by 
		data
	
	
GO
Grant Execute On up_integracaophc_verificaComandosPhc to Public
Grant Control On up_integracaophc_verificaComandosPhc to Public
go