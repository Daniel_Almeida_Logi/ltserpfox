-- exec up_integracaophc_MergeGrupos
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_integracaophc_MergeGrupos]') IS NOT NULL
    drop procedure up_integracaophc_MergeGrupos
go

create PROCEDURE up_integracaophc_MergeGrupos
/* WITH ENCRYPTION */
AS
DECLARE @sql varchar(max)
set @sql = N'
	
	insert into b_ug (ugstamp,nome,descricao,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora)
	Select ugstamp,nome,descricao = '''',ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora
	from
		'+ (select RTRIM(LTRIM(textValue)) from B_Parameters where stamp = 'ADM0000000233') + '.dbo.ug
	Where 
		ug.ugstamp not in (select ugstamp from b_ug)
'
	print @sql
	execute (@sql)
GO
Grant Execute On up_integracaophc_MergeGrupos to Public
Grant Control On up_integracaophc_MergeGrupos to Public
go