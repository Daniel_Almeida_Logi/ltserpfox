-- exec up_integracaophc_pesquisaUs ''
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_integracaophc_pesquisaUs]') IS NOT NULL
    drop procedure up_integracaophc_pesquisaUs
go

create PROCEDURE up_integracaophc_pesquisaUs
@nome		varchar(55)

/* WITH ENCRYPTION */
AS


	Select 
		 Sel		= CONVERT(bit,0)
		 ,usstamp = us.usstamp 
		 ,username
		 ,iniciais
		 ,us.username
		 ,us.userno
		 ,grupo
		 ,drno
		 ,ligado =0
	from 
		b_us as us (nolock)
	where
		us.nome like @nome + '%'
	Order by 
		grupo
		,nome
	
	
GO
Grant Execute On up_integracaophc_pesquisaUs to Public
Grant Control On up_integracaophc_pesquisaUs to Public
go