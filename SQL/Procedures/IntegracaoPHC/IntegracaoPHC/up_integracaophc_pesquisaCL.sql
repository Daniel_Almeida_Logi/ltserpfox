-- exec up_integracaophc_pesquisaCL 'GON�ALO GON�ALVES  LE�O', '244101493'
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_integracaophc_pesquisaCL]') IS NOT NULL
    drop procedure up_integracaophc_pesquisaCL
go

create PROCEDURE up_integracaophc_pesquisaCL

@nome		varchar(55)
,@ncont		varchar(20)


/* WITH ENCRYPTION */
AS

	IF (select RTRIM(LTRIM(textValue)) from B_Parameters where stamp = 'ADM0000000233') = ''
	Begin 
		Select 
			 Sel	= CONVERT(bit,0)
			 ,idstamp = cl.clstamp 
			 ,nome
			 ,no
			 ,estab = 0
			 ,utstamp = isnull((select top 1 utstamp from b_utentes where b_utentes.idstamp = cl.clstamp ),'')
			 ,ligado = case when cl.clstamp in (select idstamp from b_utentes) then CONVERT(bit,1) else CONVERT(bit,0) end
			 ,cl.ncont
			 ,cl.morada
			 ,cl.codpost
			 ,cl.local
			 ,cl.zona
			 ,cl.fax
			 ,cl.telefone
			 ,cl.tlmvl
			 ,cl.tipo
			 ,cl.Email
			 ,cl.bino
			 ,cl.nascimento
			 ,nutente = ''
			 ,nbenef = ''
			 
		from 
			cl (nolock)
		where
			cl.nome like @nome + '%'
			and cl.ncont = case when @ncont = '' then cl.ncont else @ncont end
		order by
			nome
	End
	Else
	begin
		
		DECLARE @sql varchar(max)
		set @sql = N'
			
			Select 
			 Sel	= CONVERT(bit,0)
			 ,idstamp = cl.clstamp 
			 ,nome
			 ,no
			 ,estab = 0
			 ,utstamp = isnull((select top 1 utstamp from b_utentes where b_utentes.idstamp = cl.clstamp ),'''')
			 ,ligado = case when cl.clstamp in (select idstamp from b_utentes) then CONVERT(bit,1) else CONVERT(bit,0) end
			 ,cl.ncont
			 ,cl.morada
			 ,cl.codpost
			 ,cl.local
			 ,cl.zona
			 ,cl.fax
			 ,cl.telefone
			 ,cl.tlmvl
			 ,cl.tipo
			 ,cl.Email
			 ,cl.bino
			 ,cl.nascimento
			 ,nutente = ''''
			 ,nbenef = ''''
			 
		from '+ (select RTRIM(LTRIM(textValue)) from B_Parameters where stamp = 'ADM0000000233') + '.dbo.cl 	
		where
			cl.nome like ''' + @nome + '%'''
			+ ' and cl.ncont = case when ''' + @ncont + ''' = '''' then cl.ncont else ''' + @ncont + ''' end'
		+ ' order by nome'
		
		print @sql
		execute (@sql)
	end				
		
GO
Grant Execute On up_integracaophc_pesquisaCL to Public
Grant Control On up_integracaophc_pesquisaCL to Public
go