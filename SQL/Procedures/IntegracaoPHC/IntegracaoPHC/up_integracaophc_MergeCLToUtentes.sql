-- exec up_integracaophc_MergeCLToUtentes 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_integracaophc_MergeCLToUtentes]') IS NOT NULL
    drop procedure up_integracaophc_MergeCLToUtentes
go

create PROCEDURE up_integracaophc_MergeCLToUtentes

/* WITH ENCRYPTION */
AS

declare @no as int
set @no = isnull((select MAX(no) from B_utentes),0)

DECLARE @sql varchar(max)
set @sql = N'

	INSERT into b_utentes (
		utstamp
		,nome
		,no
		,estab
		,ncont
		,morada
		,codpost
		,local
		,zona
		,fax
		,telefone
		,tlmvl
		,tipo
		,email
		,profi
		,bino
		,nascimento
		,sexo
		,codigoP
		,descP
		,nbenef
		,nbenef2
		,pai
		,mae
		,pacgen
		,tratamento
		,obs
		,csaude
		,drfamilia
		,usrinis
		,usrdata
		,usrhora
		,idstamp
	) 

	Select 
		left(NEWID(),25) as utstamp
		,nome
		,' + convert(varchar(20),@no) + ' + ROW_NUMBER ( ) over (order by idno)
		,estab
		,ncont
		,morada
		,codpost
		,local
		,zona
		,fax
		,telefone
		,tlmvl
		,tipo
		,email
		,profissao = ''''
		,bino
		,dtnasc = ''19000101''
		,sexo = ''''
		,''PT'' as pais
		,''PORTUGAL'' as paisdesc
		,u_nutente = LEFT(u_nutente,9)
		,nbenef = ''''
		,pai = ''''
		,mae = ''''
		,pacgen = 0
		,tratamento = ''''
		,obs
		,csaude = ''''
		,drfamilia = ''''
		,usrinis
		,usrdata
		,usrhora
		,clstamp
	from 
		'+ (select RTRIM(LTRIM(textValue)) from B_Parameters where stamp = 'ADM0000000233') + '.dbo.cl
	Where
		clstamp not in (Select idstamp from B_utentes)
	Order by no
	
	update B_utentes
	Set 
		nome = cl.nome
		,ncont = cl.ncont
		,morada = cl.morada
		,codpost = cl.codpost
		,local = cl.local
		,zona = cl.zona
		,fax = cl.fax
		,telefone = cl.telefone
		,tlmvl = cl.tlmvl
		,bino = cl.bino
		,obs = cl.obs
		,usrinis = cl.usrinis
		,usrdata = cl.usrdata
		,usrhora = cl.usrhora
	from 
		'+ (select RTRIM(LTRIM(textValue)) from B_Parameters where stamp = 'ADM0000000233') + '.dbo.cl
		inner join B_utentes (nolock) on cl.clstamp = B_utentes.idstamp
	where
		(B_utentes.nome != cl.nome
		Or B_utentes.ncont != cl.ncont
		OR B_utentes.morada != cl.morada
		OR B_utentes.codpost != cl.codpost
		Or B_utentes.local != cl.local
		OR B_utentes.zona != cl.zona
		OR B_utentes.fax != cl.fax
		Or B_utentes.telefone != cl.telefone
		OR B_utentes.tlmvl != cl.tlmvl
		Or B_utentes.local != cl.local
		OR B_utentes.bino != cl.bino)
		and B_utentes.idstamp!= ''''

'


	--print @sql
	execute (@sql)
	
GO
Grant Execute On up_integracaophc_MergeCLToUtentes to Public
Grant Control On up_integracaophc_MergeCLToUtentes to Public
go