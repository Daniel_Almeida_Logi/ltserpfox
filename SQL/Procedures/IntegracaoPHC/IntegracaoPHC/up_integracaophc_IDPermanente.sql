-- exec up_integracaophc_IDPermanente
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_integracaophc_IDPermanente]') IS NOT NULL
    drop procedure up_integracaophc_IDPermanente
go

create PROCEDURE up_integracaophc_IDPermanente
/* WITH ENCRYPTION */
AS

	DECLARE @sql varchar(max)
	set @sql = N'
		insert into b_utentes (
				utstamp
				,nome
				,no
				,estab
				,ncont
				,morada
				,codpost
				,local
				,zona
				,fax
				,telefone
				,tlmvl
				,tipo
				,email
				,bino
				,nascimento
				,codigoP
				,descP
				,ousrinis
				,ousrdata
				,ousrhora
				,usrinis
				,usrdata
				,usrhora
				,idstamp
				,codpla
				,despla
				,nbenef
				,nbenef2
			)
			Select
				LEFT(newid(),25)
				,nome
				,no = (Select max(no) from b_utentes) + ROW_NUMBER() OVER (ORDER BY (select 0))
				,estab = 0
				,ncont
				,Morada
				,CodPost
				,Local
				,Zona
				,Fax
				,Telefone
				,TLMVL
				,Tipo
				,Email
				,bino
				,dtnasc
				,''PT''
				,''PORTUGAL''
				,ousrinis
				,ousrdata
				,ousrhora
				,usrinis
				,usrdata
				,usrhora
				,idstamp
				,''999998''
				,''SEM COMPARTICIPAÇÃO P/ SNS''
				,nutente
				,nbenef
			from 
				'+ (select RTRIM(LTRIM(textValue)) from B_Parameters where stamp = 'ADM0000000233') + '.dbo.id
			Where
				idstamp not in (select idstamp from b_utentes (nolock))
	'
	print @sql
	execute (@sql)
GO
Grant Execute On up_integracaophc_IDPermanente to Public
Grant Control On up_integracaophc_IDPermanente to Public
go