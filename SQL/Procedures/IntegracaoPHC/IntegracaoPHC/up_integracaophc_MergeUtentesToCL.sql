-- exec up_integracaophc_MergeUtentesToCL '6BE17F24-E761-47F4-892D-C'
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_integracaophc_MergeUtentesToCL]') IS NOT NULL
    drop procedure up_integracaophc_MergeUtentesToCL
go

create PROCEDURE up_integracaophc_MergeUtentesToCL
/* WITH ENCRYPTION */
@utstamp as varchar(25)

AS

declare @idstamp as varchar(25)
set @idstamp = LEFT(newid(),25)
	
DECLARE @sql varchar(max)
set @sql = N'

/*Verifica se o utente existe na tabela b_utentes*/
if exists (select * from '+ (select RTRIM(LTRIM(textValue)) from B_Parameters where stamp = 'ADM0000000233') + '.dbo.cl inner join b_utentes on cl.clstamp = b_utentes.idstamp where utstamp =  '''+ @utstamp + ''')
begin 

		;With cteUtentes as (
		Select 
			nome 
			,idno = no
			,ncont 
			,nbenef = nbenef2
			,nutente = nbenef
			,local 
			,dtnasc = nascimento
			,morada
			,telefone
			,obs
			,profissao = profi
			,codpost = codpost
			,bino
			,tratamento
			,ninscs
			,csaude
			,drfamilia
			,tlmvl
			,fax
			,zona
			,tipo
			,sexo = case when sexo=''M'' then 1 else 2 end
			,email
			,nacion = codigop
			,mae
			,pai
			,idnome = nome
			,pacgen = pacgen
			,idstamp
			, ROW_NUMBER() OVER(ORDER BY no DESC) AS incremento
			,utstamp
		from 
			b_utentes (nolock)
		Where
			utstamp = ''' + @utstamp + '''
	)

	update
		'+ (select RTRIM(LTRIM(textValue)) from B_Parameters where stamp = 'ADM0000000233') + '.dbo.cl
	set 
		nome = cteUtentes.nome
		,ncont  = cteUtentes.ncont
		,local  = cteUtentes.local
		,morada = cteUtentes.morada
		,telefone = cteUtentes.telefone
		,obs = cteUtentes.obs
		,fax = cteUtentes.fax
		,codpost = cteUtentes.codpost
		,bino = cteUtentes.bino
		,tlmvl = cteUtentes.tlmvl
		,zona= cteUtentes.zona
		,tipo = cteUtentes.tipo
		,email = cteUtentes.email
	From
		cteUtentes
		inner join '+ (select RTRIM(LTRIM(textValue)) from B_Parameters where stamp = 'ADM0000000233') + '.dbo.cl  on cl.clstamp = cteUtentes.idstamp 
	Where
		cteUtentes.utstamp = ''' + @utstamp + '''
		

end
else
begin	
	
		;With cteUtentes as (
		Select 
			''' + @idstamp + ''' as idstamp
			,nome
			,no = isnull((select MAX(no) + 1 from '+ (select RTRIM(LTRIM(textValue)) from B_Parameters where stamp = 'ADM0000000233') + '.dbo.cl),1)
			,ncont 
			,local 
			,morada
			,telefone
			,obs
			,codpost
			,bino
			,tlmvl
			,fax
			,zona
			,tipo
			,email
		from 
			b_utentes (nolock)
		Where
			utstamp = ''' + @utstamp + '''
	)
	
	insert into	'+ (select RTRIM(LTRIM(textValue)) from B_Parameters where stamp = 'ADM0000000233') + '.dbo.cl (
		clstamp
		,nome 
		,no
		,ncont 
		,local 
		,morada
		,telefone
		,obs
		,codpost
		,bino
		,tlmvl
		,fax
		,zona
		,tipo
		,email
	)
	
	Select * From cteUtentes 

	/*Actualiza Stamp de liga��o na b_utentes para o stamp que foi criado*/
	update b_utentes set idstamp = ''' + @idstamp + ''' where utstamp = ''' + @utstamp + '''
	
end

'

print @sql
execute (@sql)

GO
Grant Execute On up_integracaophc_MergeUtentesToCL to Public
Grant Control On up_integracaophc_MergeUtentesToCL to Public
go