-- exec up_integracaophc_pesquisaID '', '','', ''
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_integracaophc_pesquisaID]') IS NOT NULL
    drop procedure up_integracaophc_pesquisaID
go

create PROCEDURE up_integracaophc_pesquisaID

@nome		varchar(55)
,@nbenef2	nvarchar(100)
,@nbenef	nvarchar(100)
,@ncont		varchar(20)


/* WITH ENCRYPTION */
AS

	DECLARE @sql varchar(max)
	set @sql = N'
		Select 
		 Sel		= CONVERT(bit,0)
		 ,id.idstamp
		 ,nome = idnome 
		 ,no = idno
		 ,estab = 0
		 ,nutente
		 ,id.nbenef
		 ,id.ncont
		 ,utstamp = isnull((select top 1 utstamp from b_utentes where b_utentes.idstamp = id.idstamp),'''')
		 ,ligado = case when id.idstamp in (select idstamp from b_utentes) then CONVERT(bit,1) else CONVERT(bit,0) end
		 ,id.ncont
		 ,id.morada
		 ,id.codpost
		 ,id.local
		 ,id.zona
		 ,id.fax
		 ,id.telefone
		 ,id.tlmvl
		 ,id.tipo
		 ,id.Email
		 ,profissao
		 ,id.bino
		 ,dtnasc
		 ,id.sexo
		 ,nacion
		 ,nutente
		 ,id.nbenef
		 ,id.pai
		 ,id.mae
		 ,id.pacgen
		 ,id.tratamento
		 ,id.obs
		 ,id.csaude
		 ,id.drfamilia
		 ,codpla = id.u_codpla
		 ,despla = id.u_despla
	from ' + (select RTRIM(LTRIM(textValue)) from B_Parameters where stamp = 'ADM0000000233') +	'.dbo.id 
	where
		id.idnome like ''' + @nome + '%'''
		+ 'and id.ncont = case when '''+  @ncont + ''' = '''' then id.ncont else '''+  @ncont + ''' end
		and id.nutente = case when '''+  @nbenef + ''' = '''' then id.nutente else '''+  @nbenef + ''' end
		and id.nbenef = case when '''+  @nbenef2 + ''' = '''' then id.nbenef else '''+  @nbenef2 + ''' end 
	order by
		nome
		'
	print @sql
	execute (@sql)

	
GO
Grant Execute On up_integracaophc_pesquisaID to Public
Grant Control On up_integracaophc_pesquisaID to Public
go