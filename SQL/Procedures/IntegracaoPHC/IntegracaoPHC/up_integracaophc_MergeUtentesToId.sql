-- exec up_integracaophc_MergeUtentesToId 'AB9E0D9D-D559-4E8E-9D2F-6'
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_integracaophc_MergeUtentesToId]') IS NOT NULL
    drop procedure up_integracaophc_MergeUtentesToId
go

create PROCEDURE up_integracaophc_MergeUtentesToId
/* WITH ENCRYPTION */
@utstamp as varchar(25)

AS

/*Verifica se o utente existe na tabela b_utentes*/
DECLARE @sql varchar(max)
set @sql = N'
	if exists (select * 
	from '+ (select RTRIM(LTRIM(textValue)) from B_Parameters where stamp = 'ADM0000000233') + '.dbo.id 
		inner join b_utentes on id.idstamp = b_utentes.idstamp where utstamp = ''' + @utstamp + ''')
	begin 

			;With cteUtentes as (
			Select 
				nome 
				,idno = no
				,ncont 
				,nbenef = nbenef2
				,nutente = nbenef
				,local 
				,dtnasc = nascimento
				,morada
				,telefone
				,obs
				,profissao = profi
				,codpost = codpost
				,bino
				,tratamento
				,ninscs
				,csaude
				,drfamilia
				,tlmvl
				,fax
				,zona
				,tipo
				,sexo = case when sexo=''M'' then 1 else 2 end
				,email
				,nacion = codigop
				,mae
				,pai
				,idnome = nome
				,pacgen = pacgen
				,idstamp
				,ROW_NUMBER() OVER(ORDER BY no DESC) AS incremento
				,utstamp
			from 
				b_utentes (nolock)
			Where
				utstamp = ''' + @utstamp + '''
		)

		update
			'+ (select RTRIM(LTRIM(textValue)) from B_Parameters where stamp = 'ADM0000000233') + '.dbo.id 
		set 
			nome = cteUtentes.nome
			,ncont  = cteUtentes.ncont
			,nbenef = cteUtentes.nbenef
			,nutente = cteUtentes.nutente
			,local  = cteUtentes.local
			,dtnasc = cteUtentes.dtnasc
			,morada = cteUtentes.morada
			,telefone = cteUtentes.telefone
			,obs = cteUtentes.obs
			,profissao = cteUtentes.profissao
			,codpost = cteUtentes.codpost
			,bino = cteUtentes.bino
			,tratamento = cteUtentes.tratamento
			,ninscs = cteUtentes.ninscs
			,csaude = cteUtentes.csaude
			,drfamilia = cteUtentes.drfamilia
			,tlmvl = cteUtentes.tlmvl
			,fax = cteUtentes.fax
			,zona= cteUtentes.zona
			,tipo = cteUtentes.tipo
			,sexo = cteUtentes.sexo
			,email = cteUtentes.email
			,nacion = cteUtentes.nacion
			,mae = cteUtentes.mae
			,pai = cteUtentes.pai
			,idnome = cteUtentes.idnome
			,pacgen = cteUtentes.pacgen
		From
			cteUtentes
			inner join 
				'+ (select RTRIM(LTRIM(textValue)) from B_Parameters where stamp = 'ADM0000000233') + '.dbo.id
			on id.idstamp = cteUtentes.idstamp 
		Where
			cteUtentes.utstamp = ''' + @utstamp + '''
			

	end
	else
	begin	

		declare @idstamp as varchar(25)
		set @idstamp = LEFT(newid(),25)
		
			;With cteUtentes as (
			Select 
				idstamp = ''' + @utstamp + '''
				,nome
				,no = isnull((select MAX(idno) + 1 from '+ (select RTRIM(LTRIM(textValue)) from B_Parameters where stamp = 'ADM0000000233') + '.dbo.id),1)
				,ncont 
				,nbenef = nbenef2
				,nutente = nbenef
				,local 
				,dtnasc = nascimento
				,morada
				,telefone
				,obs
				,profissao = profi
				,codpost = codpost
				,bino
				,tratamento
				,ninscs
				,csaude
				,drfamilia
				,tlmvl
				,fax
				,zona
				,tipo
				,sexo = case when sexo=''M'' then 1 else 2 end
				,email
				,nacion = codigop
				,mae
				,pai
				,idnome = nome
				,pacgen = pacgen
			from 
				b_utentes (nolock)
			Where
				utstamp = ''' +@utstamp + '''
		)
		
		insert into	'+ (select RTRIM(LTRIM(textValue)) from B_Parameters where stamp = 'ADM0000000233') + '.dbo.id (
			idstamp
			,nome 
			,idno
			,ncont 
			,nbenef
			,nutente
			,local 
			,dtnasc
			,morada
			,telefone
			,obs
			,profissao
			,codpost
			,bino
			,tratamento
			,ninscs
			,csaude
			,drfamilia
			,tlmvl
			,fax
			,zona
			,tipo
			,sexo
			,email
			,nacion
			,mae
			,pai
			,idnome
			,pacgen
		)
		
		Select * From cteUtentes 
		
		/*Actualiza Stamp de liga��o na b_utentes para o stamp que foi criado*/
		update b_utentes set idstamp = ''' + @utstamp + ''' where utstamp = ''' + @utstamp + '''
	end
'

		print @sql
		execute (@sql)

GO
Grant Execute On up_integracaophc_MergeUtentesToId to Public
Grant Control On up_integracaophc_MergeUtentesToId to Public
go