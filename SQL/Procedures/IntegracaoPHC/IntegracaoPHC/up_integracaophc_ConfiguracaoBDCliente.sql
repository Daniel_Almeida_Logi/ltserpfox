-- exec up_integracaophc_ConfiguracaoBDCliente 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_integracaophc_ConfiguracaoBDCliente]') IS NOT NULL
    drop procedure up_integracaophc_ConfiguracaoBDCliente
go

create PROCEDURE up_integracaophc_ConfiguracaoBDCliente
/* WITH ENCRYPTION */
AS
	
	select 
			caminhoPastaLogitools = isnull((select top 1 textValue from b_parameters where stamp = 'ADM0000000185'),'')
			,IDClienteLogitools = isnull((select top 1 textValue from b_parameters where stamp = 'ADM0000000157'),'')
			,caminhoLdata = isnull((select top 1 textValue from b_parameters where stamp = 'ADM0000000015'),'')
			,ServerName = 
				convert(varchar,ServerProperty('machinename')) 
				+ case when convert(varchar,(SELECT local_tcp_port FROM   sys.dm_exec_connections WHERE  session_id = @@SPID)) is null then '' else ':' end
				+ isnull(convert(varchar,(SELECT local_tcp_port FROM   sys.dm_exec_connections WHERE  session_id = @@SPID)),'')
			,InstanceName = convert(varchar,isnull((select ServerProperty('InstanceName')),''))
			, dbName = DB_NAME()
			, autenticacao = (select textValue from B_Parameters where stamp = 'ADM0000000064')
			, pastaJava  = isnull('',(select textValue from B_Parameters where stamp = 'ADM0000000233'))
	

Grant Execute On up_integracaophc_ConfiguracaoBDCliente to Public
Grant Control On up_integracaophc_ConfiguracaoBDCliente to Public
