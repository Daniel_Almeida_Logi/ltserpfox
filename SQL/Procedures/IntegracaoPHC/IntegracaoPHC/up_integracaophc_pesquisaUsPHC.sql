-- exec up_integracaophc_pesquisaUsPHC ''
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_integracaophc_pesquisaUsPHC]') IS NOT NULL
    drop procedure up_integracaophc_pesquisaUsPHC
go

create PROCEDURE up_integracaophc_pesquisaUsPHC
@username		varchar(30)

/* WITH ENCRYPTION */
AS

	DECLARE @sql varchar(max)
	set @sql = N'
		Select 
			 Sel = CONVERT(bit,0)
			 ,usstamp = us.usstamp 
			 ,username
			 ,iniciais
			 ,us.usercode
			 ,us.username
			 ,us.userno
			 ,grupo
			 ,ugstamp
			 ,us.inactivo
			 ,tipo = isnull(dr.tipo,'''')
			 ,morada = isnull(dr.morada,'''')
			 ,local = isnull(dr.local,'''')
			 ,codpost = isnull(dr.codpost,'''')
			 ,tlmvl = isnull(dr.tlmvl,'''')
			 ,email = isnull(dr.email,'''')
			 ,dtnasc = isnull(dr.dtnasc,''19000101'')
			 ,sexo = isnull(dr.sexo,1)
			 ,nacion = isnull(dr.nacion,'''')
			 ,tratamento = isnull(dr.tratamento,'''')
			 ,obs = isnull(dr.obs,'''')
			 ,ncont = isnull('''','''')
			 ,area
			 ,profission	 
			 ,us.drno
			 ,pwpos
			 ,ligado = case when us.usstamp in (select usstamp from b_us) then CONVERT(bit,1) else CONVERT(bit,0) end
			 ,u_cedula = isnull(dr.u_cedula,'''')
			 ,u_estab = isnull(dr.u_estab,0)
			 ,u_inscri = isnull(dr.u_inscri,0)
			 ,u_ordem = isnull(dr.u_ordem,'''')
		from '
		+ (select RTRIM(LTRIM(textValue)) from B_Parameters where stamp = 'ADM0000000233') +
		'.dbo.us
		left join 
		'
		+ (select RTRIM(LTRIM(textValue)) from B_Parameters where stamp = 'ADM0000000233') +
		'.dbo.dr on dr.drstamp = us.drstamp
		where
			us.username like ''' + @username + '%'''
		+'Order by grupo, nome
		'
	print @sql
	execute (@sql)
	
	
GO
Grant Execute On up_integracaophc_pesquisaUsPHC to Public
Grant Control On up_integracaophc_pesquisaUsPHC to Public
go