-- exec up_integracaophc_MergeIDToUtentes 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_integracaophc_MergeIDToUtentes]') IS NOT NULL
    drop procedure up_integracaophc_MergeIDToUtentes
go

create PROCEDURE up_integracaophc_MergeIDToUtentes

 /*WITH ENCRYPTION */
AS

declare @no as int
set @no = isnull((select MAX(no) from B_utentes),0)
	
DECLARE @sql varchar(max)
set @sql = N'

	
	/* N�o existe */
	insert into B_utentes (
		utstamp
		,nome
		,no
		,estab
		,ncont
		,morada
		,codpost
		,local
		,zona
		,fax
		,telefone
		,tlmvl
		,profi
		,bino
		,nascimento
		,sexo
		,codigoP
		,descP
		,nbenef
		,nbenef2
		,pai
		,mae
		,pacgen
		,tratamento
		,obs
		,csaude
		,drfamilia
		,usrinis
		,usrdata
		,usrhora
		,idstamp
	) 
	Select 
		left(NEWID(),25)
		,idnome
		,idno --' + convert(varchar(20),@no) + ' + ROW_NUMBER ( ) over (order by idno)
		,ncont
		,morada
		,codpost
		,local
		,zona
		,fax
		,telefone
		,tlmvl
		,profissao
		,bino
		,dtnasc
		,sexo
		,''PT''
		,''PORTUGAL''
		,nutente
		,nbenef
		,pai
		,mae
		,pacgen
		,tratamento
		,obs
		,csaude
		,drfamilia
		,usrinis
		,usrdata
		,usrhora
		,idstamp
	from 
		'+ (select RTRIM(LTRIM(textValue)) from B_Parameters where stamp = 'ADM0000000233') + '.dbo.id
	Where
		idstamp not in (Select idstamp from B_utentes)
	Order by idno
		

	/* Existe */

	update B_utentes
	Set 
		nome = id.idnome
		,ncont = id.ncont
		,morada = id.morada
		,codpost = id.codpost
		,local = id.local
		,zona = id.zona
		,fax = id.fax
		,telefone = id.telefone
		,tlmvl = id.tlmvl
		,profi = id.profissao
		,bino = id.bino
		,nascimento = id.dtnasc
		,sexo = id.sexo
		,nbenef = id.nutente
		,nbenef2 = id.nbenef
		,pai = id.pai
		,mae = id.mae
		,pacgen = id.pacgen
		,tratamento = id.tratamento
		,obs = id.obs
		,csaude= id.csaude
		,drfamilia = id.drfamilia
		,usrinis = id.usrinis
		,usrdata = id.usrdata
		,usrhora = id.usrhora
	from 
		'+ (select RTRIM(LTRIM(textValue)) from B_Parameters where stamp = 'ADM0000000233') + '.dbo.id
		inner join B_utentes (nolock) on id.idstamp = B_utentes.idstamp
	where
		(B_utentes.nome != id.idnome
		Or B_utentes.ncont != id.ncont
		OR B_utentes.morada != id.morada
		OR B_utentes.codpost != id.codpost
		Or B_utentes.local != id.local
		OR B_utentes.zona != id.zona
		OR B_utentes.fax != id.fax
		Or B_utentes.telefone != id.telefone
		OR B_utentes.tlmvl != id.tlmvl
		Or B_utentes.local != id.local
		OR B_utentes.bino != id.bino
		OR B_utentes.nbenef != id.nutente)
		and B_utentes.idstamp!= ''''

'


print @sql
execute (@sql)


GO
Grant Execute On up_integracaophc_MergeIDToUtentes to Public
Grant Control On up_integracaophc_MergeIDToUtentes to Public
go