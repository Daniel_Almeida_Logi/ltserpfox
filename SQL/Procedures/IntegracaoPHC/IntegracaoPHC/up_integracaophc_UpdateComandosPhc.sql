-- exec up_integracaophc_UpdateComandosPhc '2131'
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_integracaophc_UpdateComandosPhc]') IS NOT NULL
    drop procedure up_integracaophc_UpdateComandosPhc
go

create PROCEDURE up_integracaophc_UpdateComandosPhc
@stamp varchar(25)
/* WITH ENCRYPTION */
AS

	update b_cli_integraPhc
	set executado = 1
	where id = @stamp

	
	
GO
Grant Execute On up_integracaophc_UpdateComandosPhc to Public
Grant Control On up_integracaophc_UpdateComandosPhc to Public
go