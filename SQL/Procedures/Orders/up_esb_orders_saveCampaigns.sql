/*
	gravar os details o cabecalho de uma campanha 
	exec up_esb_orders_saveCampaigns 
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_esb_orders_saveCampaigns]') IS NOT NULL
	DROP PROCEDURE dbo.up_esb_orders_saveCampaigns
GO

CREATE PROCEDURE dbo.up_esb_orders_saveCampaigns
	@token				VARCHAR (36),
	@stamp				VARCHAR (36),
	@responseDate		datetime,
	@campaignName		VARCHAR (100),
	@campaignCode		VARCHAR (50),
	@beginDate			datetime,
	@endDate			datetime,
	@addendum			int,
	@message			varchar(100),
	@inis				VARCHAR(30)

/* WITH ENCRYPTION */
AS

	IF(not exists(SELECT * FROM ext_esb_orders_campaigns WHERE token=@token and stamp=@stamp and campaignCode=@campaignCode ))
	BEGIN
		INSERT INTO ext_esb_orders_campaigns(stamp,token,campaignName,campaignCode,beginDate,endDate,addendum,[message],ousrdata,ousrinis,usrdata,usrinis)
		VALUES(@stamp,@token,@campaignName,@campaignCode,@beginDate,@endDate,@addendum,@message,GETDATE(),@inis,GETDATE(),@inis)
	END
	ELSE
	BEGIN
		UPDATE ext_esb_orders_campaigns
			set 
				campaignName		=@campaignName,
				beginDate			=@beginDate,
				endDate				=@endDate,
				usrdata				=GETDATE(),
				usrinis				=@inis,
				addendum			=@addendum,
				[message]			=@message
			WHERE token=@token AND 
				  stamp=@stamp AND 
				  campaignCode=@campaignCode
	END  

GO
Grant Execute on dbo.up_esb_orders_saveCampaigns to Public
Grant Control on dbo.up_esb_orders_saveCampaigns to Public
GO