/*
	grava os facturas incluidas de factura encomenda
	exec  up_esb_orders_saveConsultInvoice_ordersInc
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_esb_orders_saveConsultInvoice_ordersInc]') IS NOT NULL
	DROP PROCEDURE dbo.up_esb_orders_saveConsultInvoice_ordersInc
GO

CREATE PROCEDURE dbo.up_esb_orders_saveConsultInvoice_ordersInc
	@token					VARCHAR (36),
	@stamp					VARCHAR (36),
	@stampConsultInvoice	VARCHAR (36),
	@orderNumber			VARCHAR (20),
	@orderDate				DATETIME,
	@inis					VARCHAR(50)	

/* WITH ENCRYPTION */
AS

	IF(NOT EXISTS(SELECT * FROM ext_esb_orders_consultInvoice_ordersInc WHERE token=@token and stampConsultInvoice=@stampConsultInvoice AND orderNumber=@orderNumber))
	BEGIN
		INSERT INTO ext_esb_orders_consultInvoice_ordersInc(stamp,token,stampConsultInvoice,orderNumber,orderDate,ousrdata,ousrinis,usrdata,usrinis)
		VALUES(@stamp,@token,@stampConsultInvoice,@orderNumber,@orderDate,GETDATE(),@inis,GETDATE(),@inis)
	END
	ELSE
	BEGIN
		UPDATE ext_esb_orders_consultInvoice_ordersInc
			set 
				orderNumber			 =@orderNumber,
				orderDate			 =@orderDate,
				usrdata				 =GETDATE(),
				usrinis				 =@inis
			WHERE token=@token AND 
				  stampConsultInvoice=@stampConsultInvoice AND 
				  orderNumber=@orderNumber
	END  

GO
Grant Execute on dbo.up_esb_orders_saveConsultInvoice_ordersInc to Public
Grant Control on dbo.up_esb_orders_saveConsultInvoice_ordersInc to Public
GO