/*
	cria linhas para fazer consulta de bonus
	EXEC up_esb_orders_createBonus
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_esb_orders_createBonus]') IS NOT NULL
	drop procedure dbo.up_esb_orders_createBonus
go

create procedure dbo.up_esb_orders_createBonus
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON
	DECLARE @site VARCHAR(20)
	DECLARE @sitenr INT
	DECLARE @multiStamp VARCHAR(36)
	DECLARE @date datetime

	If OBJECT_ID('tempdb.dbo.#BOB2B') IS NOT NULL
		DROP TABLE #BOB2B

	If OBJECT_ID('tempdb.dbo.#BOB2BSend') IS NOT NULL
		DROP TABLE #BOB2BSend


	SET @multiStamp = (LEFT(NEWID(),36))
	SET @date = getdate()

	SELECT DISTINCT 		
		nr_fl,
		dep_fl,
		site_nr,
		 dataobra
	INTO #BOB2B
	FROM fl_site (nolock)
		left JOIN BO (nolock) ON BO.no = fl_site.nr_fl AND BO.estab = fl_site.dep_fl AND fl_site.site_nr=(SELECT no from empresa where site= bo.site)
	WHERE id_esb ='B2B' AND  BO.ndos=35 and dataobra = CONVERT(date, getdate())  -- 35 � os bonus fornecedor  adicionar 1 dia  -- para chamar uma vez por dia  -- 502693150 Alliance neste momento nao posso chamar 
	GROUP BY 	nr_fl, dep_fl,site_nr,dataobra


	SELECT  
		fl_site.nr_fl,
		fl_site.dep_fl,
		dateadd(DAY,-5, GETDATE()) as date,
		fl_site.site_nr,
		nome,
		no_ext,
		1  AS rowline,
		#BOB2B.nr_fl as t 
	INTO #BOB2BSend
	FROM fl (nolock)
		INNER JOIN fl_site (nolock) on fl.no= fl_site.nr_fl and fl.estab=fl_site.dep_fl 
		left join #BOB2B (nolock) on #BOB2B.nr_fl= fl_site.nr_fl and #BOB2B.dep_fl=fl_site.dep_fl  and #BOB2B.site_nr = fl_site.site_nr
	WHERE id_esb ='B2B' and #BOB2B.nr_fl is null and fl.inactivo=0


	DECLARE emp_cursorEmpresa CURSOR FOR
	SELECT SITE AS siteemp FROM empresa (nolock)
   
	OPEN emp_cursorEmpresa
	FETCH NEXT FROM emp_cursorEmpresa INTO @site
   
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT @sitenr = NO FROM empresa (nolock) where site=@site
		INSERT INTO ext_esb_orders (TOKEN,id,id_sender,type,created_on,sent_on,currency,typeOrderID,typeOrderIDDescr,flExt_no,flnome,entity,bostamp,site,nr_fl,flestab,multiStamp)
		 SELECT (LEFT(NEWID(),36)),'','','',GETDATE(),GETDATE(),'',3,'consulta campanhas',no_ext,nome, 'B2B','',@site ,#BOB2BSend.nr_fl,#BOB2BSend.dep_fl, @multiStamp from  #BOB2BSend WHERE site_nr=@sitenr  

		 FETCH NEXT FROM emp_cursorEmpresa INTO @site
	END

	CLOSE emp_cursorEmpresa
	DEALLOCATE emp_cursorEmpresa
	
	If OBJECT_ID('tempdb.dbo.#BOB2B') IS NOT NULL
		DROP TABLE #BOB2B

	If OBJECT_ID('tempdb.dbo.#BOB2BSend') IS NOT NULL
		DROP TABLE #BOB2BSend
   
	SELECT token  AS TOKEN FROM ext_esb_orders (nolock) WHERE multiStamp = @multiStamp
   
GO
Grant Execute on dbo.up_esb_orders_createBonus to Public
Grant Control on dbo.up_esb_orders_createBonus to Public
GO

