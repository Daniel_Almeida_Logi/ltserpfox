/*
	grava o header da consulta factura encomenda
	exec up_esb_orders_saveConsultInvoice 
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_esb_orders_saveConsultInvoice]') IS NOT NULL
	DROP PROCEDURE dbo.up_esb_orders_saveConsultInvoice
GO

CREATE PROCEDURE dbo.up_esb_orders_saveConsultInvoice
	@token					VARCHAR (36),
	@stamp					VARCHAR (36),
	@orderNumber			VARCHAR (20),
	@message				VARCHAR (100),
	@responseDate			DATETIME,
	@orderDate				DATETIME,
	@grossTotal				NUMERIC(17,3),
	@netTotal				NUMERIC(17,3),
	@commercialDiscount		NUMERIC(17,3),
	@financialDiscount		NUMERIC(17,3),
	@totalValueVat			NUMERIC(17,3),
	@inis					VARCHAR(30)	

/* WITH ENCRYPTION */
AS

	IF(NOT EXISTS(SELECT * FROM ext_esb_orders_consultInvoice WHERE token=@token and orderNumber=@orderNumber AND stamp=@stamp))
	BEGIN
		INSERT INTO ext_esb_orders_consultInvoice(stamp,token,orderNumber,[message],responseDate,orderDate,grossTotal,netTotal,commercialDiscount,financialDiscount,
			totalValueVat,ousrdata,ousrinis,usrdata,usrinis)
		VALUES(@stamp,@token,@orderNumber,@message,@responseDate,@orderDate,@grossTotal,@netTotal,@commercialDiscount,@financialDiscount,
			@totalValueVat,GETDATE(),@inis,GETDATE(),@inis)
	END
	ELSE
	BEGIN
		UPDATE ext_esb_orders_consultInvoice
			set 
				[message]			=@message,
				responseDate		=@responseDate,
				orderDate			=@orderDate,
				grossTotal			=@grossTotal,
				netTotal			=@netTotal,
				commercialDiscount	=@commercialDiscount,
				financialDiscount   =@financialDiscount,
				totalValueVat		=@totalValueVat,
				usrdata				=GETDATE(),
				usrinis				=@inis
			WHERE token=@token AND 
				  orderNumber=@orderNumber AND 
				  stamp=@stamp
	END  

GO
Grant Execute on dbo.up_esb_orders_saveConsultInvoice to Public
Grant Control on dbo.up_esb_orders_saveConsultInvoice to Public
GO