
DECLARE @site VARCHAR(20)
declare emp_cursor cursor for
select site as siteemp from empresa

open emp_cursor
fetch next from emp_cursor into @site

while @@FETCH_STATUS = 0
begin
	IF not EXISTS (SELECT * FROM B_Parameters_site WHERE stamp='ADM0000000117' and site=@site)
	BEGIN
	INSERT [dbo].[B_Parameters_site] ([stamp], [name], [Type], [textValue], [numValue], [bool], [ODate], [LDate], [visivel], [MostraTextValue], [MostraNumValue], [MostraBool], [ListaTextValue], [Unidades], [site], [obs]) 
	VALUES ('ADM0000000117', 'Indica se quer receber PDF nas integrações externas de encomendas', 'Encomendas', '', 0, 0, getdate(), getdate(), 1, 0, 0, 1, 'Permite escolher se quer recebe pdf externos', '',@site, 'Permite escolher se quer recebe pdf externos')

	END
	fetch next from emp_cursor into @site
end
close emp_cursor
deallocate emp_cursor


