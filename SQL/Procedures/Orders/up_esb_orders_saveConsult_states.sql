/*
	grava os estados do order da consulta 
	exec up_esb_orders_saveConsult 
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_esb_orders_saveConsult_states]') IS NOT NULL
	DROP PROCEDURE dbo.up_esb_orders_saveConsult_states
GO

CREATE PROCEDURE dbo.up_esb_orders_saveConsult_states
	@token					VARCHAR (36),
	@stamp					VARCHAR (36),
	@stampconsult			VARCHAR (36),
	@state					VARCHAR (50),
	@stateDate				VARCHAR (50),
	@inis					VARCHAR	

/* WITH ENCRYPTION */
AS

	IF(NOT EXISTS(SELECT * FROM ext_esb_orders_consult_states WHERE token=@token and stampconsult=@stampconsult AND stamp=@stamp))
	BEGIN
		INSERT INTO ext_esb_orders_consult_states(stamp,token,stampconsult,[state],stateDate,ousrdata,ousrinis,usrdata,usrinis)
		VALUES(@stamp,@token,@stampconsult,@state,@stateDate,GETDATE(),@inis,GETDATE(),@inis)
	END
	ELSE
	BEGIN
		UPDATE ext_esb_orders_consult_states
			set 
				state					= @state,
				stateDate				= @stateDate,
				usrdata					= GETDATE(),
				usrinis					= @inis
			WHERE token=@token AND 
				  stampconsult=@stampconsult AND 
				  stamp=@stamp
	END  

GO
Grant Execute on dbo.up_esb_orders_saveConsult_states to Public
Grant Control on dbo.up_esb_orders_saveConsult_states to Public
GO