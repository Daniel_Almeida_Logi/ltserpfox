/*  get a linha de bonus de uma referencia de um armazenista 

exec up_get_InformacaoConsultaBonus_linha 1,'Loja 1'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_InformacaoConsultaBonus_linha]') IS NOT NULL
	drop procedure dbo.up_get_InformacaoConsultaBonus_linha
go

CREATE PROCEDURE [dbo].[up_get_InformacaoConsultaBonus_linha]
	@token			NUMERIC(10,0),
	@campaignsStamp	VARCHAR(36),
	@ref			VARCHAR(18)
AS
SET NOCOUNT ON

	SELECT 
	*
	FROM 
		ext_esb_orders_campaigns_d
	where token =@token and campaignsStamp = @campaignsStamp and ref=@ref

	
GO
GRANT EXECUTE ON dbo.up_get_InformacaoConsultaBonus_linha to Public
GRANT CONTROL ON dbo.up_get_InformacaoConsultaBonus_linha to Public
GO