/*
	gravar os details de um destino seja fornecedor, distribuidor ou facturador

	1- distribuidor ,
	2- fornecedor ,
	3- facturador,
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_esb_orders_saveDetailDestination]') IS NOT NULL
	DROP PROCEDURE dbo.up_esb_orders_saveDetailDestination
GO

CREATE PROCEDURE dbo.up_esb_orders_saveDetailDestination
	@token				VARCHAR (36),
	@stamp				VARCHAR (36),
	@urlDestiny			VARCHAR (256),
	@destinyCode		VARCHAR (256),
	@destinyName		VARCHAR (256),
	@deliveryHours		INT,
	@detailDestType		INT,
	@detailDestTypeDesc	VARCHAR(200),
	@inis				VARCHAR(30)

/* WITH ENCRYPTION */
AS

	IF(not exists(SELECT * FROM ext_esb_orders_detailDestination WHERE token=@token and stamp=@stamp and destinyCode=@destinyCode ))
	BEGIN
		INSERT INTO ext_esb_orders_detailDestination(stamp,token,urlDestiny,destinyCode,destinyName,deliveryHours,detailDestType,detailDestTypeDesc,ousrdata,ousrinis,usrdata,usrinis)
		VALUES(@stamp,@token,@urlDestiny,@destinyCode,@destinyName,@deliveryHours,@detailDestType,@detailDestTypeDesc,GETDATE(),@inis,GETDATE(),@inis)
	END
	ELSE
	BEGIN
		UPDATE ext_esb_orders_detailDestination
			set 
				urlDestiny			=@urlDestiny,
				destinyName			=@destinyName,
				deliveryHours		=@deliveryHours,
				usrdata				=GETDATE(),
				usrinis				=@inis,
				detailDestType		=@detailDestType,
				detailDestTypeDesc  =@detailDestTypeDesc
			WHERE token=@token AND 
				  stamp=@stamp AND 
				  destinyCode=@destinyCode
	END  

GO
Grant Execute on dbo.up_esb_orders_saveDetailDestination to Public
Grant Control on dbo.up_esb_orders_saveDetailDestination to Public
GO