/*
	cria consulta das facturas do esb_orders 
	exec up_esb_orders_createOrdersInvoicesExt_doc '4F237ED6-2398-4CB8-BCB0-4DAD005DAAB0'


	exec up_esb_orders_createOrdersInvoicesExt_doc 'ADMFF9D1EB3-E27E-410C-998'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_esb_orders_createOrdersInvoicesExt_doc]') IS NOT NULL
	drop procedure dbo.up_esb_orders_createOrdersInvoicesExt_doc
go

create procedure dbo.up_esb_orders_createOrdersInvoicesExt_doc
	@multiToken			VARCHAR(36)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	DECLARE @sql1 VARCHAR(MAX) 
	DECLARE @sql2 VARCHAR(MAX)
	DECLARE @sql3 VARCHAR(MAX)

	DECLARE @cabFields VARCHAR(MAX) = ''
	DECLARE @linFields VARCHAR(MAX) = ''
	DECLARE @cabFieldsInsert VARCHAR(MAX) = ''
	DECLARE @linFieldsInsert VARCHAR(MAX) = ''
	DECLARE @cabFieldsEXP VARCHAR(MAX) = ''
	DECLARE @linFieldsEXP VARCHAR(MAX) = ''
	DECLARE @cabFieldsEXPSELECT VARCHAR(MAX) = ''
	DECLARE @linFieldsEXPSELECT VARCHAR(MAX) = ''

	DECLARE @no_ext VARCHAR(50)

	If OBJECT_ID('tempdb.dbo.#ext_esb_ordersTokens') IS NOT NULL
		DROP TABLE #ext_esb_ordersTokens

	If OBJECT_ID('tempdb.dbo.#ext_esb_orders_consultInvoice') IS NOT NULL
		DROP TABLE #ext_esb_orders_consultInvoice

	If OBJECT_ID('tempdb.dbo.#ext_esb_orders_consultInvoice_d') IS NOT NULL
		DROP TABLE #ext_esb_orders_consultInvoice_d

	SELECT 
		ext_esb_orders.TOKEN, 
		site,
		nr_fl,
		flestab , 
		ROW_NUMBER() over(partition by ext_esb_orders_consultInvoice.orderNumber order by ousrdata desc) as number,
		ext_esb_orders.date_cre as date_asked,
		ext_esb_orders.flExt_no,
		(CASE WHEN (SELECT COUNT(*) FROM fieldsImport_B2B(nolock) WHERE no_ext = ext_esb_orders.flExt_no) > 0 THEN ext_esb_orders.flExt_no ELSE '0' END) as noExtFields
	INTO #ext_esb_ordersTokens
	FROM ext_esb_orders (nolock)
		INNER JOIN ext_esb_orders_consultInvoice (nolock) ON ext_esb_orders.token = ext_esb_orders_consultInvoice.token		
	WHERE multiStamp= LTRIM(@multitoken) and ext_esb_orders_consultInvoice.orderNumber!='' and (ext_esb_orders_consultInvoice.orderNumber not in ( SELECT nr FROM ext_doc where YEAR(data)=year(ext_esb_orders.date_cre) and ext_doc.nr_fl = ext_esb_orders.nr_fl))

	delete #ext_esb_ordersTokens where number !=1

	DECLARE uc_fields CURSOR FOR
	SELECT 
		DISTINCT 
		#ext_esb_ordersTokens.flExt_no,
		STUFF(( SELECT ',' + LTRIM(RTRIM(fieldsImport_B2B.targetField))
				FROM fieldsImport_B2B(nolock)
				WHERE fieldsImport_B2B.no_ext = #ext_esb_ordersTokens.noExtFields AND fieldsImport_B2B.cab = 1
				ORDER BY targetField asc 
				FOR XML PATH('')), 1, 1, '')  as CAB,
		STUFF(( SELECT ',' + LTRIM(RTRIM(fieldsImport_B2B.targetField))
				FROM fieldsImport_B2B(nolock)
				WHERE fieldsImport_B2B.no_ext = #ext_esb_ordersTokens.noExtFields AND fieldsImport_B2B.cab = 1 AND fieldsImport_B2B.isImported = 1
				ORDER BY targetField asc 
				FOR XML PATH('')), 1, 1, '')  as CAB_INSERT,
		STUFF(( SELECT ',' + LTRIM(RTRIM(fieldsImport_B2B.[exp]))
				FROM fieldsImport_B2B(nolock)
				WHERE fieldsImport_B2B.no_ext = #ext_esb_ordersTokens.noExtFields AND fieldsImport_B2B.cab = 1
				ORDER BY targetField asc
				FOR XML PATH('')), 1, 1, '')  as CAB_EXP,
		STUFF(( SELECT ',' + LTRIM(RTRIM(fieldsImport_B2B.[exp])) + ' AS ' + LTRIM(RTRIM(fieldsImport_B2B.targetField))
				FROM fieldsImport_B2B(nolock)
				WHERE fieldsImport_B2B.no_ext = #ext_esb_ordersTokens.noExtFields AND fieldsImport_B2B.cab = 1
				ORDER BY targetField asc
				FOR XML PATH('')), 1, 1, '')  as CAB_EXP_SELECT,
		STUFF(( SELECT ',' + LTRIM(RTRIM(fieldsImport_B2B.targetField))
				FROM fieldsImport_B2B(nolock)
				WHERE fieldsImport_B2B.no_ext = #ext_esb_ordersTokens.noExtFields AND fieldsImport_B2B.cab = 0 AND fieldsImport_B2B.isImported = 1
				ORDER BY targetField asc
				FOR XML PATH('')), 1, 1, '')  as LIN_INSERT,
		STUFF(( SELECT ',' + LTRIM(RTRIM(fieldsImport_B2B.targetField))
				FROM fieldsImport_B2B(nolock)
				WHERE fieldsImport_B2B.no_ext = #ext_esb_ordersTokens.noExtFields AND fieldsImport_B2B.cab = 0
				ORDER BY targetField asc
				FOR XML PATH('')), 1, 1, '')  as LIN,
		STUFF(( SELECT ',' + LTRIM(RTRIM(fieldsImport_B2B.[exp]))
				FROM fieldsImport_B2B(nolock)
				WHERE fieldsImport_B2B.no_ext = #ext_esb_ordersTokens.noExtFields AND fieldsImport_B2B.cab = 0
				ORDER BY targetField asc
				FOR XML PATH('')), 1, 1, '')  as LIN_EXP,
		STUFF(( SELECT ',' + LTRIM(RTRIM(fieldsImport_B2B.[exp])) + ' AS ' + LTRIM(RTRIM(fieldsImport_B2B.targetField))
				FROM fieldsImport_B2B(nolock)
				WHERE fieldsImport_B2B.no_ext = #ext_esb_ordersTokens.noExtFields AND fieldsImport_B2B.cab = 0
				ORDER BY targetField asc
				FOR XML PATH('')), 1, 1, '')  as LIN_EXP_SELECT
	FROM
		#ext_esb_ordersTokens
	GROUP BY flExt_no, noExtFields

	OPEN uc_fields

	FETCH NEXT FROM uc_fields INTO @no_ext, @cabFields, @cabFieldsInsert, @cabFieldsEXP, @cabFieldsEXPSELECT, @linFields, @linFieldsInsert, @linFieldsEXP, @linFieldsEXPSELECT

	WHILE @@FETCH_STATUS = 0
	BEGIN

		If OBJECT_ID('tempdb.dbo.#ext_esb_orders_consultInvoice') IS NOT NULL
			DROP TABLE #ext_esb_orders_consultInvoice

		If OBJECT_ID('tempdb.dbo.#ext_esb_orders_consultInvoice_d') IS NOT NULL
			DROP TABLE #ext_esb_orders_consultInvoice_d
		
		SET @cabFieldsEXPSELECT = REPLACE(@cabFieldsEXPSELECT,'&lt;','<')
		SET @cabFieldsEXPSELECT = REPLACE(@cabFieldsEXPSELECT,'&gt;','>')

		SET @linFieldsEXPSELECT = REPLACE(@linFieldsEXPSELECT,'&lt;','<')
		SET @linFieldsEXPSELECT = REPLACE(@linFieldsEXPSELECT,'&gt;','>')

		SET @cabFieldsInsert	= REPLACE(@cabFieldsInsert,'&gt;','>')
		SET @cabFieldsInsert	= REPLACE(@cabFieldsInsert,'&gt;','>')	

		SET @linFieldsInsert	= REPLACE(@linFieldsInsert,'&gt;','>')
		SET @linFieldsInsert	= REPLACE(@linFieldsInsert,'&gt;','>')

		SET @sql1 = N'

		IF ((SELECT COUNT (TOKEN) FROM #ext_esb_ordersTokens) >0)
		BEGIN 

			SELECT 
				' + 
					LTRIM(RTRIM(@cabFieldsEXPSELECT)) 
				+ '
				INTO #ext_esb_orders_consultInvoice
				FROM ext_esb_orders_consultInvoice 
				INNER JOIN #ext_esb_ordersTokens ON ext_esb_orders_consultInvoice.token =#ext_esb_ordersTokens.token AND #ext_esb_ordersTokens.flExt_no = ''' + LTRIM(RTRIM(@no_ext)) + '''
		
		'

	
		SET @sql2 = N'
	
		IF((SELECT COUNT(token) FROM #ext_esb_orders_consultInvoice)>0)
		BEGIN
			SELECT  
				'+
					LTRIM(RTRIM(@linFieldsEXPSELECT))
				+'
			INTO #ext_esb_orders_consultInvoice_d
			FROM 
				ext_esb_orders_consultInvoice_d
				INNER JOIN #ext_esb_orders_consultInvoice AS INV ON INV.token = ext_esb_orders_consultInvoice_d.token
			WHERE ext_esb_orders_consultInvoice_d.ref !=''''
	

			DELETE FROM #ext_esb_orders_consultInvoice WHERE #ext_esb_orders_consultInvoice.TOKEN NOT IN ( SELECT DISTINCT #ext_esb_orders_consultInvoice.TOKEN FROM #ext_esb_orders_consultInvoice_d) 

			IF((SELECT count(*) FROM #ext_esb_orders_consultInvoice) >0)
			BEGIN
							
				INSERT INTO ext_doc_d (' + LTRIM(RTRIM(@linFieldsInsert)) + ')
				SELECT ' + LTRIM(RTRIM(@linFieldsInsert)) + ' FROM #ext_esb_orders_consultInvoice_d

				INSERT INTO ext_doc(' + LTRIM(RTRIM(@cabFieldsInsert)) + ')
				SELECT ' + LTRIM(RTRIM(@cabFieldsInsert)) + ' FROM #ext_esb_orders_consultInvoice


				UPDATE ext_esb_orders SET ext_esb_orders.orderConfirmationResponse=1 , orderMessageResponse=''Consulta de estado Factura'' , status=''Success''  WHERE TOKEN in (select token from #ext_esb_ordersTokens where #ext_esb_ordersTokens.flExt_no = ''' + LTRIM(RTRIM(@no_ext)) + ''')
			END 
			ELSE
			BEGIN
				UPDATE ext_esb_orders SET ext_esb_orders.orderConfirmationResponse=1 , orderMessageResponse=''Consulta de estado Factura'', status=''Success''  WHERE TOKEN in (select token from #ext_esb_ordersTokens where #ext_esb_ordersTokens.flExt_no = ''' + LTRIM(RTRIM(@no_ext)) + ''')
			END

		END
		'


		SET @sql3 = N'
	
			ELSE
			BEGIN
					UPDATE ext_esb_orders SET ext_esb_orders.orderConfirmationResponse=1 , orderMessageResponse=''Consulta de estado Factura'', status=''Success''  WHERE TOKEN in (select token from #ext_esb_ordersTokens where #ext_esb_ordersTokens.flExt_no = ''' + LTRIM(RTRIM(@no_ext)) + ''')
			END 

		     
		END 
		ELSE
		BEGIN
			UPDATE ext_esb_orders SET ext_esb_orders.orderConfirmationResponse=1 , orderMessageResponse=''Consulta de estado Factura'' , status=''Success'' WHERE TOKEN in (select token from #ext_esb_ordersTokens where #ext_esb_ordersTokens.flExt_no = ''' + LTRIM(RTRIM(@no_ext)) + ''')
		END 
	
		'


		PRINT @sql1 + @sql2 + @sql3
		EXEC(@sql1 + @sql2 + @sql3)

		FETCH NEXT FROM uc_fields INTO @no_ext, @cabFields, @cabFieldsInsert, @cabFieldsEXP, @cabFieldsEXPSELECT, @linFields, @linFieldsInsert, @linFieldsEXP, @linFieldsEXPSELECT
	END

	CLOSE uc_fields
	DEALLOCATE uc_fields


	SELECT  orderConfirmationResponse, orderMessageResponse , status FROM ext_esb_orders WHERE token=@multiToken 


   	If OBJECT_ID('tempdb.dbo.#ext_esb_ordersTokens') IS NOT NULL
		DROP TABLE #ext_esb_ordersTokens

	If OBJECT_ID('tempdb.dbo.#ext_esb_orders_consultInvoice') IS NOT NULL
		DROP TABLE #ext_esb_orders_consultInvoice

	If OBJECT_ID('tempdb.dbo.#ext_esb_orders_consultInvoice_d') IS NOT NULL
		DROP TABLE #ext_esb_orders_consultInvoice_d
GO
Grant Execute on dbo.up_esb_orders_createOrdersInvoicesExt_doc  to Public
Grant Control on dbo.up_esb_orders_createOrdersInvoicesExt_doc  to Public
GO

