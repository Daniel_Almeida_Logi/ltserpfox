/*
	cria linhas para fazer consulta de x dias 
	EXEC up_esb_orders_createInvoiceAutomaticDays 3

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_esb_orders_createInvoiceAutomaticDays]') IS NOT NULL
	drop procedure dbo.up_esb_orders_createInvoiceAutomaticDays
go

create procedure dbo.up_esb_orders_createInvoiceAutomaticDays
	@days			int

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	If OBJECT_ID('tempdb.dbo.#BOB2BSend') IS NOT NULL
		DROP TABLE #BOB2BSend

	If OBJECT_ID('tempdb.dbo.#B2BFL') IS NOT NULL
		DROP TABLE #B2BFL

	If OBJECT_ID('tempdb.dbo.#daysSend') IS NOT NULL
		DROP TABLE #daysSend


	 CREATE TABLE #dadosHorario(
         [days_date] [date] NOT NULL,
         [weekday] [nvarchar](50) NULL
     ) ON [PRIMARY]

	; WITH CTE (DT) AS 
	 (
		SELECT CAST(DATEADD(DAY,- @days, GETDATE())  AS DATE) DT
		UNION ALL
		SELECT DATEADD(DAY, 1, DT)
		FROM CTE 
		WHERE DATEADD(DAY, 1, DT) < CONVERT(date,GETDATE())
	 )

	 INSERT INTO [#dadosHorario]([days_date],[weekday])
	 SELECT DT,DATENAME(DW,DT)  
	 FROM CTE 
	 OPTION (maxrecursion 0)

	SELECT 
		fl.no    ,
		fl.estab  ,
		FL.nome   as fl_nome  ,
		fl.no_ext as ext_no, 
		site  as site, 
	   row_number() over (partition by fl.no_ext,site order by fl.no,fl.estab ) as number
	INTO #B2BFL
	FROM  fl (NOLOCK) 
	left JOIN fl_site ON fl_site.nr_fl = fl.no and fl_site.dep_fl=fl.estab
	INNER JOIN empresa ON empresa.NO=fl_site.site_nr
	INNER JOIN fl_perms_externalNO ON fl_perms_externalNO.no_ext= FL.no_ext
	WHERE  id_esb='B2B' and inativo=0

	delete #B2BFL where number !=1

	SELECT 
		LEFT(NEWID(),36) AS TOKEN ,
		no ,
		estab  ,
		[#dadosHorario].days_date  as date,
		fl_nome  ,
		ext_no, 
		site  as site
	INTO #BOB2BSend
	FROM  #B2BFL (NOLOCK) 
	,[#dadosHorario]

	INSERT INTO ext_esb_orders (TOKEN,id,id_sender,type,created_on,sent_on,currency,typeOrderID,typeOrderIDDescr,flExt_no,flnome,entity,bostamp,site,nr_fl,flestab,date_cre)
	SELECT TOKEN,'','','',GETDATE(),GETDATE(),'',7,'consulta estado facturas Dia',ext_no,fl_nome, 'B2B','',site ,#BOB2BSend.no,#BOB2BSend.estab,#BOB2BSend.date  from  #BOB2BSend


     SELECT token AS TOKEN   FROM #BOB2BSend (nolock) 

	If OBJECT_ID('tempdb.dbo.#BOB2BSend') IS NOT NULL
		DROP TABLE #BOB2BSend

	If OBJECT_ID('tempdb.dbo.#daysSend') IS NOT NULL
		DROP TABLE #daysSend

	If OBJECT_ID('tempdb.dbo.#B2BFL') IS NOT NULL
		DROP TABLE #B2BFL
   
   
GO
Grant Execute on dbo.up_esb_orders_createInvoiceAutomaticDays to Public
Grant Control on dbo.up_esb_orders_createInvoiceAutomaticDays to Public
GO

