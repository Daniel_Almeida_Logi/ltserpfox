/* select last token from a sender 

exec up_orders_getLastAcessToken 'B2B'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_orders_getLastAcessToken]') IS NOT NULL
    DROP procedure dbo.up_orders_getLastAcessToken
GO

CREATE procedure dbo.up_orders_getLastAcessToken
	@sender				VARCHAR(100)

AS

	SELECT TOP 1
		orderAcessStamp,
		access_token,
		token_type,
		expires_in,
		refresh_token,
		issuedDate,
		expiresDate,
		userName,
		displayName,
		roles,
		sender,
		case when expiresDate >=  DATEADD(mi, 30,   GETDATE()) THEN 1 ELSE 0 END controlAcess
	 FROM orderAcess
		WHERE sender = @sender
	order by expiresDate desc

GO
GRANT EXECUTE on dbo.up_orders_getLastAcessToken TO PUBLIC
GRANT Control on dbo.up_orders_getLastAcessToken TO PUBLIC
GO