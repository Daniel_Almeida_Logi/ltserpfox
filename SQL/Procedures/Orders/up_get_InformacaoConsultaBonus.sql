/*  get do bonus de um armazenista 

exec up_get_InformacaoConsultaBonus 0,'Loja 1'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_InformacaoConsultaBonus]') IS NOT NULL
	drop procedure dbo.up_get_InformacaoConsultaBonus
go

CREATE PROCEDURE [dbo].[up_get_InformacaoConsultaBonus]
	@nr_fl			NUMERIC(10,0),
	@site			VARCHAR(60)
AS
SET NOCOUNT ON

	--DELETE ext_esb_orders_campaigns WHERE ousrdata > (SELECT DATEADD(month, -1, GETDATE()))
	--DELETE ext_esb_orders_campaigns_d WHERE ousrdata > (SELECT DATEADD(month, -1, GETDATE()))
	DECLARE @d datetime;
	DECLARE @dateIn datetime;
	DECLARE @dateEnd datetime;
	SELECT @d = DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0);
	DECLARE @Hora int

		set @Hora = convert(int,CONVERT(VARCHAR(2),GETDATE(),108))

	DECLARE @begin time(7);
	DECLARE @end time(7);

	IF (@Hora<12)
	BEGIN
		SET @begin = CAST('00:00:00' AS time);
		SET @end = CAST('11:59:59' AS time);
	END
	ELSE 
	BEGIN 
		SET @begin = CAST('12:00:00' AS time);
		SET @end = CAST('23:59:59' AS time);
	END 

	SET @dateIn = CONVERT(datetime, CONVERT(varchar(10), CONVERT(date, @d, 101)) + ' ' + CONVERT(varchar(8), @begin));
	SET @dateEnd = CONVERT(datetime, CONVERT(varchar(10), CONVERT(date, @d, 101)) + ' ' + CONVERT(varchar(8), @end));

	SELECT TOP  1
		ext_esb_orders_campaigns.*
	FROM ext_esb_orders
		INNER JOIN ext_esb_orders_campaigns ON ext_esb_orders.token = ext_esb_orders_campaigns.token
		INNER JOIN ext_esb_msgStatus		ON ext_esb_orders.token = ext_esb_msgStatus.token
	WHERE ext_esb_orders.nr_fl = @nr_fl AND ext_esb_orders.SITE= @site and
		ext_esb_orders_campaigns.usrdata between @dateIn  and @dateEnd and ext_esb_msgStatus.id='200'
	ORDER BY ext_esb_orders_campaigns.usrdata DESC

	
GO
GRANT EXECUTE ON dbo.up_get_InformacaoConsultaBonus to Public
GRANT CONTROL ON dbo.up_get_InformacaoConsultaBonus to Public
GO