/*  get de informacao da farmacia de integracao de um tipo

	exec up_get_InformacaoEmpresaFornecedor 
exec up_get_InformacaoEmpresaFornecedoresId_esb1 'Loja 1','B2B'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_InformacaoEmpresaFornecedoresId_esb]') IS NOT NULL
	drop procedure dbo.up_get_InformacaoEmpresaFornecedoresId_esb
go

CREATE PROCEDURE [dbo].[up_get_InformacaoEmpresaFornecedoresId_esb]
	@site			VARCHAR(60),
	@id_esb			VARCHAR(15)
AS
SET NOCOUNT ON

	DECLARE @no NUMERIC (4,0) 
	DECLARE @infarmed VARCHAR(10)  
	DECLARE	@codFarm  VARCHAR(25)

	SELECT  @no=no,@infarmed=Ltrim(RTRIM(infarmed)),@codFarm=codFarm  FROM empresa (NOLOCK) WHERE site=@site


	if(len(@infarmed)<5)
	begin 
		SET @infarmed =(SELECT RIGHT('00000'+CAST(@infarmed AS VARCHAR(5)),5))

	end 


	SELECT 
		@no																														AS no,
		@infarmed																												AS infarmed,
		@codFarm																												AS codFarm,
		CASE WHEN (fl_site.id_esb ='B2B') THEN LTRIM(RTRIM(id_esb_user)) ELSE LTRIM(RTRIM(idclfl)) END							AS us,
		CASE WHEN (fl_site.id_esb ='B2B') THEN LTRIM(RTRIM(id_esb_pw)) ELSE LTRIM(RTRIM(clpass))	END							AS pw,
		fl.nome																													AS supplierName,
		no_ext																													AS supplierNo,
		(select bool from B_Parameters_site where stamp='ADM0000000117' and site=@site )										AS recebePDF,
		fl.no																													AS nr_fl,
		fl.estab																												AS estab, 
		row_number() over(partition by fl.no order by fl.no desc)																AS FirstElem	
	
	Into #flaux
	FROM 
		fl_site		
	INNER JOIN fl			ON fl.no= fl_site.nr_fl
	WHERE fl_site.id_esb=@id_esb and fl_site.site_nr=@no and usa_esb=1 and id_esb='B2B' and fl.inactivo=0 

	SELECT 
		no,
		infarmed,
		codFarm,
		us,
		pw,
		supplierName,
		supplierNo,
		recebePDF,
		nr_fl,
		estab
	FROM #flaux
	where #flaux.FirstElem=1
	
GO
GRANT EXECUTE ON dbo.up_get_InformacaoEmpresaFornecedoresId_esb to Public
GRANT CONTROL ON dbo.up_get_InformacaoEmpresaFornecedoresId_esb to Public