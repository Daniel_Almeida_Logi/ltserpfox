/* retorna se utiliza o B2B ou não 
select * from B_Parameters where stamp='ADM0000000227'

	exec up_get_usaB2B 0
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_usaB2B]') IS NOT NULL
	drop procedure dbo.up_get_usaB2B
go

create procedure dbo.up_get_usaB2B
	@test   bit 


/* WITH ENCRYPTION */
AS
	
	IF((SELECT COUNT(*) FROM fl_site where id_esb='B2B') > 0 and (select bool from B_Parameters where stamp='ADM0000000227')=@test)
	BEGIN
		SELECT 1 AS usa

	END
	ELSE
	BEGIN
		SELECT 0 AS usa
	END
		
			
GO
Grant Execute On dbo.up_get_usaB2B to Public
Grant Control On dbo.up_get_usaB2B to Public
GO