/*  get de informacao da farmacia e fornecedor correspondente ao envio 

	exec up_get_InfOrders 
exec up_get_InfOrders '79EDFDE0-D54E-4099-8015-6670AFF44960'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_InfOrders]') IS NOT NULL
	drop procedure [dbo].[up_get_InfOrders]
go

CREATE PROCEDURE [dbo].[up_get_InfOrders]
	@token		VARCHAR(36)
AS
SET NOCOUNT ON


	SELECT 
		bostamp																								AS bostamp,
		empresa.no																							AS sitenr,
		esb.site																							AS site,
		entity																								AS entity,
		typeOrderID																							AS orderId,
		case when bostamp ='' then nr_fl 
			else (		SELECT top 1 fl_site.nr_fl
						FROM ext_esb_orders 
						INNER join empresa		ON empresa.site = esb.site 
						INNER JOIN bo on ext_esb_orders.bostamp = bo.bostamp
						INNER JOIN fl_site		ON fl_site.nr_fl= bo.no and fl_site.site_nr= empresa.no
						WHERE token =  @token) end															AS nr_fl ,
		case when bostamp ='' then flestab 
			else (		SELECT top 1 fl_site.dep_fl
						FROM ext_esb_orders 
						INNER join empresa		ON empresa.site = esb.site 
						INNER JOIN bo on ext_esb_orders.bostamp = bo.bostamp
						INNER JOIN fl_site		ON fl_site.nr_fl= bo.no and fl_site.site_nr= empresa.no
						WHERE token =  @token) end															AS nr_fl_estab


	FROM 
		ext_esb_orders   esb
		INNER join empresa		ON empresa.site = esb.site 
	WHERE token =  @token
	
GO
GRANT EXECUTE ON [dbo].[up_get_InfOrders] to Public
GRANT CONTROL ON [dbo].[up_get_InfOrders] to Public
GO

