/*  get de informacao da farmacia e fornecedor correspondente ao envio 

	exec up_get_InformacaoEmpresaFornecedor 
exec up_get_InformacaoEmpresaFornecedor 'Loja 1','FRrCAF59ABB-ACF4-4528-8C4' 
exec up_get_InformacaoEmpresaFornecedor 'Loja 1','','4E5AE030-DB9B-4183-B106-3295A6E31AAC'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_InformacaoEmpresaFornecedor]') IS NOT NULL
	drop procedure dbo.up_get_InformacaoEmpresaFornecedor
go

CREATE PROCEDURE [dbo].[up_get_InformacaoEmpresaFornecedor]
	@site			VARCHAR(60),
	@boStamp		VARCHAR(36),
	@token			VARCHAR(36) =''
AS
SET NOCOUNT ON

	DECLARE @no NUMERIC (4,0) 
	DECLARE @infarmed NUMERIC(10,0)  
	DECLARE	@codFarm  VARCHAR(25)

	SELECT  @no=no,@infarmed=infarmed,@codFarm=codFarm  FROM empresa (NOLOCK) WHERE site=@site

	IF(@boStamp!='')
	BEGIN 
		SELECT 
			@no																								AS no,
			(SELECT RIGHT('00000'+CAST(@infarmed AS VARCHAR(5)),5))											AS infarmed,
			@codFarm																						AS codFarm,
			CASE WHEN (fl_site.id_esb ='B2B') THEN LTRIM(RTRIM(id_esb_user)) ELSE LTRIM(RTRIM(idclfl)) END	AS us,
			CASE WHEN (fl_site.id_esb ='B2B') THEN LTRIM(RTRIM(id_esb_pw)) ELSE LTRIM(RTRIM(clpass))END		AS pw,
			fl.nome																							AS supplierName,
			no_ext																							AS supplierNo,
			(select bool from B_Parameters_site where stamp='ADM0000000117' and site=@site )				AS recebePDF,
			nr_fl																							AS nr_fl,
			fl.estab																						AS estab  
		FROM 
			bo
		INNER JOIN fl_site		ON fl_site.nr_fl= bo.no and fl_site.site_nr= @no
		INNER JOIN fl			ON fl.no= fl_site.nr_fl
		WHERE bostamp =  @boStamp
	END
	ELSE
	BEGIN
		SELECT 
			@no																								AS no,
			(SELECT RIGHT('00000'+CAST(@infarmed AS VARCHAR(5)),5))											AS infarmed,
			@codFarm																						AS codFarm,
			CASE WHEN (fl_site.id_esb ='B2B') THEN LTRIM(RTRIM(id_esb_user)) ELSE LTRIM(RTRIM(idclfl)) END	AS us,
			CASE WHEN (fl_site.id_esb ='B2B') THEN LTRIM(RTRIM(id_esb_pw)) ELSE LTRIM(RTRIM(clpass))	END	AS pw,
			fl.nome																							AS supplierName,
			no_ext																							AS supplierNo,
			(select bool from B_Parameters_site where stamp='ADM0000000117' and site=@site )				AS recebePDF,
			fl.no																							AS nr_fl,
			fl.estab																						AS estab   		 
		FROM 
			ext_esb_orders
		INNER JOIN fl_site		ON fl_site.nr_fl= ext_esb_orders.nr_fl and fl_site.site_nr= @no
		INNER JOIN fl			ON fl.no= fl_site.nr_fl
		WHERE ext_esb_orders.token =  @token
		
	
	END 
	
GO
GRANT EXECUTE ON dbo.up_get_InformacaoEmpresaFornecedor to Public
GRANT CONTROL ON dbo.up_get_InformacaoEmpresaFornecedor to Public
GO