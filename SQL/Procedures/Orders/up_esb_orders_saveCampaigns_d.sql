/*
	gravar os details de uma linha de uma campanha 
	exec up_esb_orders_saveCampaigns_d 
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_esb_orders_saveCampaigns_d]') IS NOT NULL
	DROP PROCEDURE dbo.up_esb_orders_saveCampaigns_d
GO

CREATE PROCEDURE dbo.up_esb_orders_saveCampaigns_d
	@token					VARCHAR (36),
	@stamp					VARCHAR (36),
	@campaignsStamp			VARCHAR (36),
	@ref					VARCHAR (18),
	@position				INT,
	@qttmin					INT,
	@qttmax					INT,
	@multipleAcquisition	INT,
	@packagingBonus			INT,
	@productOffered			VARCHAR(13),
	@descrOffer				VARCHAR(255),
	@discount				NUMERIC(13,3),
	@beginDate				DATETIME,
	@endDate				DATETIME,
	@pvu					NUMERIC(13,3),
	@inis					VARCHAR	(30)

/* WITH ENCRYPTION */
AS

	IF(NOT EXISTS(SELECT * FROM ext_esb_orders_campaigns_d WHERE token=@token and campaignsStamp=@campaignsStamp and ref=@ref ))
	BEGIN
		INSERT INTO ext_esb_orders_campaigns_d(stamp,token,campaignsStamp,ref,position,qttmin,qttmax,multipleAcquisition,packagingBonus,
			productOffered,descrOffer,discount,beginDate,endDate,pvu,ousrdata,ousrinis,usrdata,usrinis)
		VALUES(@stamp,@token,@campaignsStamp,@ref,@position,@qttmin,@qttmax,@multipleAcquisition,@packagingBonus,
			@productOffered,@descrOffer,@discount,@beginDate,@endDate,@pvu,GETDATE(),@inis,GETDATE(),@inis)
	END
	ELSE
	BEGIN
		UPDATE ext_esb_orders_campaigns_d
			set 
				ref						= @ref,
				position				= @position,
				qttmin					= @qttmin,
				qttmax					= @qttmax,
				multipleAcquisition		= @multipleAcquisition,
				packagingBonus			= @packagingBonus,
				productOffered			= @productOffered,
				descrOffer				= @descrOffer,
				discount				= @discount,
				beginDate				= @beginDate,
				endDate					= @endDate,
				PVU						= @pvu,
				usrdata					= GETDATE(),
				usrinis					= @inis
			WHERE token=@token AND 
				  campaignsStamp=@campaignsStamp AND 
				  ref=@ref
	END  

GO
Grant Execute on dbo.up_esb_orders_saveCampaigns_d to Public
Grant Control on dbo.up_esb_orders_saveCampaigns_d to Public
GO