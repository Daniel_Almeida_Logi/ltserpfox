/*
	cria linhas para fazer consulta de todas as facturas de um dia 
	EXEC up_esb_orders_createOrdersInvoicesDates 'ADM0BF91001-82D7-4539-9B4'
	exec up_esb_orders_createOrdersInvoicesDates 'ADM846C90BD-D704-4C1A-90C'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_esb_orders_createOrdersInvoicesDates]') IS NOT NULL
	drop procedure dbo.up_esb_orders_createOrdersInvoicesDates
go

create procedure dbo.up_esb_orders_createOrdersInvoicesDates
	@token			VARCHAR(36)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	If OBJECT_ID('tempdb.dbo.#BOB2BSend') IS NOT NULL
		DROP TABLE #BOB2BSend

	DECLARE @nr_fl		NUMERIC(10,0)
	DECLARE @site		VARCHAR(30)
	DECLARE @dep_fl		NUMERIC(3,0) 
	DECLARE @fl_nome	VARCHAR(36)
	DECLARE	@date		DATETIME
	declare @flExt_no	VARCHAR(50)



	SELECT @nr_fl=nr_fl,@dep_fl=flestab , @fl_nome=flNome , @date= date_cre ,@flExt_no=flExt_no,@site=site  FROM ext_esb_orders WHERE token=@token


	SELECT 
		@nr_fl     as nr_fl,
		@dep_fl    as dep_fl,
		obrano     as id,
		CONVERT(DATETIME, CONVERT(CHAR(8), dataobra, 112) + ' ' + CONVERT(CHAR(8), bo.ousrhora, 108)) as dataobra,
		@fl_nome   as fl_nome  ,
		@flExt_no as ext_no ,
		bostamp    as bostamp
	INTO #BOB2BSend
	FROM BO (NOLOCK) 
	WHERE ndos=2 AND  dataobra = @date AND NO = @nr_fl and estab=@dep_fl and logi1=1 and site= @site

	INSERT INTO ext_esb_orders (TOKEN,id,id_sender,type,created_on,sent_on,currency,typeOrderID,typeOrderIDDescr,flExt_no,flnome,entity,site,nr_fl,flestab,date_cre,bostamp ,multiStamp)
    SELECT (LEFT(NEWID(),36)),id,'','',GETDATE(),GETDATE(),'',7,'consulta estado facturas Dia',#BOB2BSend.ext_no,#BOB2BSend.fl_nome, 'B2B',@site ,#BOB2BSend.nr_fl,#BOB2BSend.dep_fl,#BOB2BSend.dataobra,#BOB2BSend.bostamp,@token from  #BOB2BSend

	
	If OBJECT_ID('tempdb.dbo.#BOB2BSend') IS NOT NULL
		DROP TABLE #BOB2BSend
   
	SELECT token  AS TOKEN FROM ext_esb_orders (nolock) WHERE multiStamp = @token
   
GO
Grant Execute on dbo.up_esb_orders_createOrdersInvoicesDates to Public
Grant Control on dbo.up_esb_orders_createOrdersInvoicesDates to Public
GO

