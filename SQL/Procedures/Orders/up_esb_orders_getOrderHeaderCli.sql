/*
	Levantar detalhe da encomenda a enviar
	ext_esb_orders
	exec up_esb_orders_getOrderHeaderCli 'VQ46B565B2-6B23-4FE1-95F'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_esb_orders_getOrderHeaderCli]') IS NOT NULL
	drop procedure dbo.up_esb_orders_getOrderHeaderCli
go

create procedure dbo.up_esb_orders_getOrderHeaderCli
	@token varchar (36)

/* WITH ENCRYPTION */
AS

	SET NOCOUNT ON

	SELECT
		 ext_esb_orders.id																					AS id 
		,bo.bostamp																							AS bostamp
		,bo.boano																							AS boano
		,bo.obrano																							AS obrano
		,bo.ndos																							AS ndoss
		,ext_esb_orders.date_cre																			AS ousrdata
		,ext_esb_orders.currency																			AS moeda
		,bo.etotal																							AS etotal
		,deliveryDesiredDate																				AS dataEntr
		,tolerateDays																						AS toleraData
		,deliveryType																						AS tipoEntrega -- 0 diurna
		,CASE WHEN ((select count(*) from ext_esb_orders_d where token=@token) >1 )
		THEN   4
		ELSE	1 END 																						AS tipoEncomenda -- Instantânea
		,paymentTermDays																					AS prazoPagamento -- numero de dias a seguir a factura
		,distributorCode																					AS distribuidorCod -- codigo do distribuidor 
	FROM
		ext_esb_orders (NOLOCK)
		left join bo  (NOLOCK) ON bo.bostamp = ext_esb_orders.bostamp
		left join fl  (NOLOCK) ON bo.no = fl.no and bo.estab = fl.estab
	WHERE
		 ext_esb_orders.token = @token

GO
Grant Execute on dbo.up_esb_orders_getOrderHeaderCli to Public
Grant Control on dbo.up_esb_orders_getOrderHeaderCli to Public
GO