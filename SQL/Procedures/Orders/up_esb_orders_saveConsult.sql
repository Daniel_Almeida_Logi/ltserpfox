/*
	grava o cabecalho da consulta 
	exec up_esb_orders_saveConsult 
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_esb_orders_saveConsult]') IS NOT NULL
	DROP PROCEDURE dbo.up_esb_orders_saveConsult
GO

CREATE PROCEDURE dbo.up_esb_orders_saveConsult
	@token					VARCHAR (36),
	@stamp					VARCHAR (36),
	@orderNumber			VARCHAR (20),
	@message				VARCHAR (100),
	@confirmationId			INT,
	@confirmationDesc		VARCHAR (100),
	@inis					VARCHAR	(30)

/* WITH ENCRYPTION */
AS

	IF(NOT EXISTS(SELECT * FROM ext_esb_orders_consult WHERE token=@token and orderNumber=@orderNumber AND stamp=@stamp))
	BEGIN
		INSERT INTO ext_esb_orders_consult(stamp,token,orderNumber,[message],confirmationId,confirmationDesc,ousrdata,ousrinis,usrdata,usrinis)
		VALUES(@stamp,@token,@orderNumber,@message,@confirmationId,@confirmationDesc,GETDATE(),@inis,GETDATE(),@inis)
	END
	ELSE
	BEGIN
		UPDATE ext_esb_orders_consult
			set 
				message					= @message,
				confirmationId			= @confirmationId,
				confirmationDesc		= @confirmationDesc,
				usrdata					= GETDATE(),
				usrinis					= @inis
			WHERE token=@token AND 
				  orderNumber=orderNumber AND 
				  stamp=@stamp
	END  

GO
Grant Execute on dbo.up_esb_orders_saveConsult to Public
Grant Control on dbo.up_esb_orders_saveConsult to Public
GO