/*
	grava as linha da consulta da factura
	exec up_esb_orders_saveConsultInvoice_d 
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_esb_orders_saveConsultInvoice_d]') IS NOT NULL
	DROP PROCEDURE dbo.up_esb_orders_saveConsultInvoice_d
GO

CREATE PROCEDURE dbo.up_esb_orders_saveConsultInvoice_d
	@token					VARCHAR (36),
	@stamp					VARCHAR (36),
	@stampConsultInvoice	VARCHAR (36),
	@ref					VARCHAR (18),
	@descr					VARCHAR (100),
	@orderNumber			VARCHAR (20),
	@orderDate				DATETIME,
	@validateDate			VARCHAR(30),
	@bonus					INT,
	@discount				NUMERIC(20,3),
	@qttOrder				INT,
	@qttSent				INT,
	@productLot				VARCHAR (50),
	@unitaryValue			NUMERIC(20,3),
	@netValue				NUMERIC(20,3),
	@pvp					NUMERIC(20,3),
	@vat					NUMERIC(6,3),
	@inis					VARCHAR	(30)

/* WITH ENCRYPTION */
AS
	
	Set @unitaryValue = case 
							when (@qttSent !=0 and isnull(@unitaryValue,0) < isnull(@netValue,0) / @qttSent ) 
								then
									@netValue
								else
									@unitaryValue
							end 




	INSERT INTO ext_esb_orders_consultInvoice_d(stamp,token,stampConsultInvoice,ref,descr,orderNumber,orderDate,validateDate,bonus,discount,qttOrder,
		qttSent,productLot,unitaryValue,netValue,pvp,vat,ousrdata,ousrinis,usrdata,usrinis)
	VALUES(@stamp,@token,@stampConsultInvoice,@ref,@descr,@orderNumber,@orderDate,@validateDate,@bonus,@discount,@qttOrder,
		@qttSent,@productLot,@unitaryValue,@netValue,@pvp,@vat,GETDATE(),@inis,GETDATE(),@inis)

GO
Grant Execute on dbo.up_esb_orders_saveConsultInvoice_d to Public
Grant Control on dbo.up_esb_orders_saveConsultInvoice_d to Public
GO