/*
	grava os ivas incidentes daquela factura encomenda
	exec  up_esb_orders_saveConsultInvoice_incVats
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_esb_orders_saveConsultInvoice_incVats]') IS NOT NULL
	DROP PROCEDURE dbo.up_esb_orders_saveConsultInvoice_incVats
GO

CREATE PROCEDURE dbo.up_esb_orders_saveConsultInvoice_incVats
	@token					VARCHAR (36),
	@stamp					VARCHAR (36),
	@stampConsultInvoice	VARCHAR (36),
	@vat					VARCHAR (20),
	@incidenceValue			NUMERIC (20,3),
	@vatValue				NUMERIC (20,3),
	@inis					VARCHAR	(30)

/* WITH ENCRYPTION */
AS

	IF(NOT EXISTS(SELECT * FROM ext_esb_orders_consultInvoice_incVats WHERE token=@token and stampConsultInvoice=@stampConsultInvoice AND vat=@vat))
	BEGIN
		INSERT INTO ext_esb_orders_consultInvoice_incVats(stamp,token,stampConsultInvoice,vat,incidenceValue,vatValue,ousrdata,ousrinis,usrdata,usrinis)
		VALUES(@stamp,@token,@stampConsultInvoice,@vat,@incidenceValue,@vatValue,GETDATE(),@inis,GETDATE(),@inis)
	END
	ELSE
	BEGIN
		UPDATE ext_esb_orders_consultInvoice_incVats
			set 
				vat					 =@vat,
				incidenceValue		 =@incidenceValue,
				vatValue			 =@vatValue,
				usrdata				 =GETDATE(),
				usrinis				 =@inis
			WHERE token=@token AND 
				  stampConsultInvoice=@stampConsultInvoice AND 
				  vat=@vat
	END  

GO
Grant Execute on dbo.up_esb_orders_saveConsultInvoice_incVats to Public
Grant Control on dbo.up_esb_orders_saveConsultInvoice_incVats to Public
GO