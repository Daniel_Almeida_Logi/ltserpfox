/*
	Levantar detalhe da encomenda a enviar
	ext_esb_orders_d
	exec up_esb_orders_getOrderLinesCli 'AN149AD67D-D226-48F5-819'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_esb_orders_getOrderLinesCli]') IS NOT NULL
	drop procedure dbo.up_esb_orders_getOrderLinesCli
go

create procedure dbo.up_esb_orders_getOrderLinesCli
	@token varchar (36)

/* WITH ENCRYPTION */
AS

	SET NOCOUNT ON
	
	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadoRef'))
		DROP TABLE #dadoRef


	DECLARE @bostamp	VARCHAR(36)
	DECLARE @nr_fl		NUMERIC(9,0)
	DECLARE @flestab	INT
	DECLARE @no_ext		VARCHAR(50)

	SELECT @bostamp =bostamp,@nr_fl=nr_fl,@flestab=flestab FROM ext_esb_orders WHERE token = @token

	IF (@bostamp!='')
	BEGIN
		SELECT 
			@no_ext =no_ext
		FROM 
			BO
		INNER JOIN FL on fl.no = bo.no and fl.estab = bo.estab
		WHERE bostamp=@bostamp
	
	END
	ELSE
	BEGIN
		SELECT 
			@no_ext =no_ext
		FROM 
			fl
		WHERE fl.no=@nr_fl and fl.estab=@flestab
	
	END 

	SELECT
		 ext_esb_orders_d.ref											AS ref
		,ext_esb_orders_d.descr											AS design
		,isnull(pvp,0)													AS edebito     -- B2B RECEBE O VALOR INT EM QUE 10,60 FICA  
		, case
				when ext_esb_orders_d.qt > ext_esb_orders_d.qt_bonus
				then ext_esb_orders_d.qt - ext_esb_orders_d.qt_bonus
				else ext_esb_orders_d.qt
			end															AS qtt
		,ext_esb_orders_d.qt_bonus										AS u_bonus
		,ext_esb_orders_d.desc1											AS desconto
		,ext_esb_orders_d.desc2											AS desc2
		,ext_esb_orders_d.desc3											AS desc3
		,ext_esb_orders_d.tax											AS iva
		,total															AS ettdeb
		,ext_esb_orders_d.qt_offers										AS oferta
		,ext_esb_orders_d.confirmation									AS confirmar -- para solicitar confirmacao da encomenda
		,ext_esb_orders_d.campaign										AS CodigoCampanha
		,pct															AS pct
		,ROW_NUMBER() OVER(PARTITION BY ext_esb_orders_d.ref ORDER BY ext_esb_orders_d.ref DESC)  AS rowline  
	INTO #dadoRef
	FROM
		ext_esb_orders_d (nolock)
	WHERE
		ext_esb_orders_d.ref != ''
		and ext_esb_orders_d.token = @token

	IF(@no_ext='105') -- colocado pois a botelho estoura do lado deles se enviarmos duas linhas com a mesma referencia 
	BEGIN
		SELECT ref,design,edebito,(select sum(qtt) from #dadoRef x where x.ref=#dadoRef.ref  ) as qtt,u_bonus,desconto,desc2,desc3,iva,ettdeb,oferta,confirmar,CodigoCampanha,pct 
		FROM #dadoRef WHERE rowline =1 
	END 
	ELSE
	BEGIN
		SELECT ref,design,edebito,qtt,u_bonus,desconto,desc2,desc3,iva,ettdeb,oferta,confirmar,CodigoCampanha,pct FROM #dadoRef
	END


	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadoRef'))
		DROP TABLE #dadoRef

GO
Grant Execute on dbo.up_esb_orders_getOrderLinesCli to Public
Grant Control on dbo.up_esb_orders_getOrderLinesCli to Public
GO