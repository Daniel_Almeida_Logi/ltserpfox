/*
	gravar os details de cada referencia consultada 
	exec up_esb_orders_saveConsultRef 
	exec up_esb_orders_saveConsultRef 'ADM0FA6D7DC-521C-411D-962','0','','',0,'ND','Produto Desconhecido',0.0,'0',0.0,'','','',0,'ADM',527,0,''
	exec up_esb_orders_saveConsultRef 'ADMF3F9D567-48C5-417D-92F','5440987','BEN-U-RON 1000 MG COMP. X18','',-1,'Est� dispon�vel na quantidade solicitada','',0.0,I,2.79,'','2021-01-08T00:00:00Z','',0,'ADM',''
 exec up_esb_orders_saveConsultRef 'ADM39ED0644-7AFC-4113-B51','','','',0,'ND','',0.0,'0',0.0,'','','O Pedido n�o pode ser concluido',0,'ADM',''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_esb_orders_saveConsultRef]') IS NOT NULL
	DROP PROCEDURE dbo.up_esb_orders_saveConsultRef
GO

CREATE PROCEDURE dbo.up_esb_orders_saveConsultRef
	@token				VARCHAR (36),
	@ref				VARCHAR (20),
	@name				VARCHAR (25),
	@presentationForm	VARCHAR (25),
	@existence			INT,
	@existenceDesc		VARCHAR(100),
	@reason				VARCHAR(100),
	@costPrice			NUMERIC(11,3),
	@vat				VARCHAR(100),
	@pvp				NUMERIC(11,3),
	@validateDate		DATETIME,
	@deliveryDate		DATETIME,
	@message			VARCHAR(100),
	@confirmation		bit,
	@inis				VARCHAR(30),
	@nr_fl				int ,
	@flestab			int ,
	@cond				VARCHAR(200)=''

/* WITH ENCRYPTION */
AS
	DECLARE @no_ext VARCHAR(50)
	DECLARE @pct NUMERIC(11,3)
	DECLARE @pcl NUMERIC(11,3)

	select  @no_ext = no_ext from fl  where no=@nr_fl and estab = @flestab


	SET @pct=0
	SET @pcl=@costPrice



	IF( exists(SELECT * FROM ext_esb_orders_d WHERE token=@token  and ref=@ref AND nr_fl =0))
	BEGIN 
		UPDATE ext_esb_orders_d
				set 
					descr				=@name,				
					presentationForm	=@presentationForm,
					existence			=@existence,
					existenceDesc		=@existenceDesc,
					reason				=@reason,	
					pct				    =@pct,			
					taxString			=@vat,				
					pvp					=@pvp,				
					validateDate		=@validateDate,		
					deliveryDate		=@deliveryDate,		
					obs					=@message,			
					confirmation		=@confirmation,	
					usrdata 			=GETDATE(),
					usrinis				=@inis,
					nr_fl				=@nr_fl,
					flestab				=@flestab,
					conditions			=@cond,
					pcl					=@pcl								
				WHERE token=@token AND 
					  ref=@ref
		
	END
	ELSE
	BEGIN
		IF(not exists(SELECT * FROM ext_esb_orders_d WHERE token=@token  and ref=@ref AND nr_fl =@nr_fl and flestab=@flestab))
		BEGIN
			INSERT INTO ext_esb_orders_d(token, ref ,descr,presentationForm,existence,existenceDesc,reason,pct,taxString,pvp,validateDate,deliveryDate,obs,confirmation,
				ousrdata,ousrinis,usrdata,usrinis,qt,nr_fl,flestab, id_order,row_id,total,conditions,pcl)
			VALUES(@token,@ref,@name,@presentationForm,@existence,@existenceDesc,@reason,@pct,@vat,@pvp,@validateDate,@deliveryDate,@message,@confirmation,
			   GETDATE(),@inis,GETDATE(),@inis,ISNULL((SELECT top 1 qt FROM ext_esb_orders_d WHERE token=@token  and ref=@ref),0),@nr_fl,@flestab,'',1,0,@cond,@pcl)
		END
		ELSE
		BEGIN
			UPDATE ext_esb_orders_d
				set 
					descr				=@name,				
					presentationForm	=@presentationForm,
					existence			=@existence,
					existenceDesc		=@existenceDesc,
					reason				=@reason,	
					pct				    =@pct,			
					taxString			=@vat,				
					pvp					=@pvp,				
					validateDate		=@validateDate,		
					deliveryDate		=@deliveryDate,		
					obs					=@message,			
					confirmation		=@confirmation,	
					usrdata 			=GETDATE(),
					usrinis				=@inis,
					conditions			=@cond,
					pcl					=@pcl					
				WHERE token=@token AND 
					  ref=@ref AND nr_fl =@nr_fl and flestab=@flestab
		END  
	END

GO
Grant Execute on dbo.up_esb_orders_saveConsultRef to Public
Grant Control on dbo.up_esb_orders_saveConsultRef to Public
GO