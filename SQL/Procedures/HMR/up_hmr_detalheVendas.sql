/*
	IMS DETALHE DE VENDAS
	Devolve informação do detalhe de vendas
	exec up_hrmDetalheVendas '20170101', '20230101', 'Loja 1', 0
	exec up_hrmDetalheVendas '20170101', '20170131', 'F_Uniao', 0

	Nota:
		O nome (abrev) do laboratório está hardcoded

		select id_lt,infarmed,site,nomecomp,* from empresa

		exec up_hmr_detalheVendas '20230501', '20230531', 'Loja 1', 0, '',1

		SELECT abs(pvp),* from hist_vendas hv where ano=2017 and site='F_Uniao'
		SELECT CONVERT(DATE,CAST(ano AS VARCHAR(4)) + '-' + CAST(mes AS VARCHAR(2)) + '-' + CAST((case when dia=0 then 1 else dia end) AS VARCHAR(2))), * from hist_vendas where ano=2017
*/




SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_hmr_detalheVendas]') IS NOT NULL
	DROP PROCEDURE dbo.up_hmr_detalheVendas
GO

CREATE PROCEDURE dbo.up_hmr_detalheVendas
	@dataIni			AS DATETIME
	,@dataFim			AS DATETIME
	,@site				AS VARCHAR(20)
	,@incluiHistorico	AS BIT = 0
	,@codHmr			AS VARCHAR(7) = ''
	,@comCab			AS BIT = 0

/* WITH ENCRYPTION */

AS
BEGIN
	
	DECLARE @countryCode		VARCHAR(2)   = 'PT'
	DECLARE @geographicLevel	VARCHAR(3)   = 'POS'
	DECLARE @geographicValue	VARCHAR(100) = 'Portugal'
	DECLARE @currencyCode		VARCHAR(3)	 = 'EUR'
	DECLARE @laboratoryName		VARCHAR(100) = 'Sanofi CHC'
	--DECLARE @periodo			NUMERIC(10,0) = CONVERT(NUMERIC(10,0),concat(YEAR(@dataIni),'',RIGHT('00' + MONTH(@dataIni), 2)))
	DECLARE @periodo			VARCHAR(6) = RTRIM(YEAR(@dataIni)) + RIGHT('00' + LTRIM(MONTH(@dataIni)),2)
	DECLARE @global_channel		VARCHAR(15)  = 'PHARMACY'	

	IF Object_id('tempdb.dbo.#temp_hmrDetalheVendas') is not null
		DROP TABLE #temp_hmrDetalheVendas
	
	-- get farmacy code	
	DECLARE @codigo VARCHAR(6)
	DECLARE @isPost BIT =0

	SET @codHmr = ISNULL(@codHmr,'')

	IF(@codHmr='') BEGIN
		SET @codigo = (SELECT infarmed FROM empresa(NOLOCK) WHERE site=@site)
		SET @isPost =  (SELECT TOP 1 posto FROM empresa(NOLOCK) WHERE siteposto=@site)
	END
	ELSE
		SET @codigo = RTRIM(LTRIM(@codHmr))
	
	
	IF(@isPost=1)
	BEGIN
		SELECT @site = @site + ',' + RTRIM(LTRIM(site)) FROM empresa(NOLOCK) WHERE siteposto=@site
	END



	SET @codigo = REPLICATE('0',6-LEN(@codigo)) + @codigo
	IF (@incluiHistorico=0)
	BEGIN

		

		-- get sales detail
		SELECT
			ofistamp
		INTO
			#temp_hmrDetalheVendas
		FROM
			fi (NOLOCK)
			inner join ft (NOLOCK) ON ft.ftstamp = fi.ftstamp
			inner join td (NOLOCK) ON td.ndoc = ft.ndoc
			inner join fprod (NOLOCK) ON fprod.cnp = fi.ref
		WHERE
			ft.tipodoc != 3
			and ft.no > 200
			AND (FT.tipodoc != 4 or u_tipodoc = 4)
			AND td.U_TIPODOC not in (1, 5)
			AND ft.anulado = 0
			AND fi.ofistamp != ''
			and fprod.hmr = 1
			and ft.fdata >= @dataFim


	SELECT 

	COUNTRY_CODE			
	,GEOGRAPHIC_LEVEL		
	,GEOGRAPHIC_VALUE		
	,CUSTOMER_ID			
	,CUSTOMER_NAME			
	,GLOBAL_CHANNEL			
	,SUB_CHANNEL			
	,[PERIOD]				
	,PRODUCT_ID				
	,PRODUCT_NAME			
	,SEGMENT				
	,SUB_SEGMENT			
	,UNITS_QTY				
	,UNITS_EQUIVALENT_QTY	
	,NET_AMT				
	,UNIT_SELL_PRICE		
	,CURRENCY_CODE			
	,LABORATORY_NAME		
	,CATEGORY_CUST_1		
	,CATEGORY_CUST_2		
	,CATEGORY_CUST_3		
	,CATEGORY_CUST_4		
	,WTD_SELLING_DIST		
	,NUM_SELLING_DIST		
	,STORE_COUNT 			
	,NUM_DIST				
	,USABLE					
	,FREE_TEXT_1			
	,FREE_TEXT_2			
	,FREE_TEXT_3			
	,FREE_TEXT_4			
	,FREE_TEXT_5			
	,FREE_NUM_1				
	,FREE_NUM_2				
	,FREE_NUM_3				
	,FREE_DATE_1			
	,FREE_DATE_2			
	,FREE_DATE_3			

	FROM (

		SELECT 
				cab 							= 1
				,[order]						= 1
				,COUNTRY_CODE					= 'COUNTRY_CODE'
				,GEOGRAPHIC_LEVEL				= 'GEOGRAPHIC_LEVEL'
				,GEOGRAPHIC_VALUE				= 'GEOGRAPHIC_VALUE'
				,CUSTOMER_ID					= 'CUSTOMER_ID'
				,CUSTOMER_NAME					= 'CUSTOMER_NAME'
				,GLOBAL_CHANNEL					= 'GLOBAL_CHANNEL'
				,SUB_CHANNEL					= 'SUB_CHANNEL'
				,[PERIOD]						= 'PERIOD'
				,PRODUCT_ID						= 'PRODUCT_ID'
				,PRODUCT_NAME					= 'PRODUCT_NAME'
				,SEGMENT						= 'SEGMENT'
				,SUB_SEGMENT					= 'SUB_SEGMENT'
				,UNITS_QTY						= 'UNITS_QTY'
				,UNITS_EQUIVALENT_QTY			= 'UNITS_EQUIVALENT_QTY'
				,NET_AMT						= 'NET_AMT'
				,UNIT_SELL_PRICE				= 'UNIT_SELL_PRICE'
				,CURRENCY_CODE					= 'CURRENCY_CODE'
				,LABORATORY_NAME				= 'LABORATORY_NAME'
				,CATEGORY_CUST_1				= 'CATEGORY_CUST_1'
				,CATEGORY_CUST_2				= 'CATEGORY_CUST_2'
				,CATEGORY_CUST_3				= 'CATEGORY_CUST_3'
				,CATEGORY_CUST_4				= 'CATEGORY_CUST_4'
				,WTD_SELLING_DIST				= 'WTD_SELLING_DIST'
				,NUM_SELLING_DIST				= 'NUM_SELLING_DIST'
				,STORE_COUNT 					= 'STORE_COUNT'
				,NUM_DIST						= 'NUM_DIST'
				,USABLE							= 'USABLE'
				,FREE_TEXT_1					= 'FREE_TEXT_1'
				,FREE_TEXT_2					= 'FREE_TEXT_2'
				,FREE_TEXT_3					= 'FREE_TEXT_3'
				,FREE_TEXT_4					= 'FREE_TEXT_4'
				,FREE_TEXT_5					= 'FREE_TEXT_5'
				,FREE_NUM_1						= 'FREE_NUM_1'
				,FREE_NUM_2						= 'FREE_NUM_2'
				,FREE_NUM_3						= 'FREE_NUM_3'
				,FREE_DATE_1					= 'FREE_DATE_1'
				,FREE_DATE_2					= 'FREE_DATE_2'
				,FREE_DATE_3					= 'FREE_DATE_3'

		UNION ALL

		Select
			cab = 0
			,[order] = 2
			,COUNTRY_CODE	
			,GEOGRAPHIC_LEVEL
			,GEOGRAPHIC_VALUE
			,CUSTOMER_ID	
			,CUSTOMER_NAME	
			,GLOBAL_CHANNEL	
			,SUB_CHANNEL	
			,[PERIOD]		
			,PRODUCT_ID		
			,PRODUCT_NAME	
			,SEGMENT		
			,SUB_SEGMENT	
			,LTRIM(SUM(UNITS_QTY)) AS UNITS_QTY
			,UNITS_EQUIVALENT_QTY
			,LTRIM(SUM(NET_AMT)) as NET_AMT
			,LTRIM(UNIT_SELL_PRICE) AS UNIT_SELL_PRICE
			,CURRENCY_CODE	
			,LABORATORY_NAME
			,CATEGORY_CUST_1
			,CATEGORY_CUST_2
			,CATEGORY_CUST_3
			,CATEGORY_CUST_4
			,WTD_SELLING_DIST
			,NUM_SELLING_DIST
			,STORE_COUNT 	
			,NUM_DIST		
			,USABLE			
			,FREE_TEXT_1	
			,FREE_TEXT_2	
			,FREE_TEXT_3	
			,FREE_TEXT_4	
			,FREE_TEXT_5	
			,FREE_NUM_1		
			,FREE_NUM_2		
			,FREE_NUM_3		
			,FREE_DATE_1	
			,FREE_DATE_2	
			,FREE_DATE_3	
		From (

			Select

				COUNTRY_CODE					= @countryCode
				,GEOGRAPHIC_LEVEL				= @geographicLevel
				,GEOGRAPHIC_VALUE				= @geographicValue
				,CUSTOMER_ID					= @codigo
				,CUSTOMER_NAME					= dbo.uf_RemoveSpecialChars(empresa.nomabrv)
				,GLOBAL_CHANNEL					= @global_channel
				,SUB_CHANNEL					= 'Independent'
				,[PERIOD]						= @periodo
				,PRODUCT_ID						= RTRIM(fi.ref)
				,PRODUCT_NAME					= dbo.uf_RemoveSpecialChars(LTRIM(RTRIM(fi.design)))
				,SEGMENT						= ''
				,SUB_SEGMENT					= ''
				,UNITS_QTY						= CONVERT(INT,SUM(fi.qtt))
				,UNITS_EQUIVALENT_QTY			= ''
				,NET_AMT						= CONVERT(NUMERIC(14,2),ROUND(SUM(fi.u_epvp * fi.qtt),2))
				,UNIT_SELL_PRICE				= CONVERT(NUMERIC(14,2),ROUND(fi.epvori,2))
				,CURRENCY_CODE					= @currencyCode
				,LABORATORY_NAME				= @laboratoryName
				,CATEGORY_CUST_1				= ''
				,CATEGORY_CUST_2				= ''
				,CATEGORY_CUST_3				= ''
				,CATEGORY_CUST_4				= ''
				,WTD_SELLING_DIST				= ''
				,NUM_SELLING_DIST				= ''
				,STORE_COUNT 					= ''
				,NUM_DIST						= ''
				,USABLE							= '1'
				,FREE_TEXT_1					= ''
				,FREE_TEXT_2					= ''
				,FREE_TEXT_3					= ''
				,FREE_TEXT_4					= ''
				,FREE_TEXT_5					= ''
				,FREE_NUM_1						= ''
				,FREE_NUM_2						= ''
				,FREE_NUM_3						= ''
				,FREE_DATE_1					= ''
				,FREE_DATE_2					= ''
				,FREE_DATE_3					= ''			
			From
				ft (NOLOCK)
				INNER JOIN ft2 (NOLOCK) ON ft2.ft2stamp = ft.ftstamp
				INNER JOIN fi (NOLOCK) ON fi.ftstamp = ft.ftstamp
				INNER JOIN td (NOLOCK) ON td.ndoc = ft.ndoc
				INNER JOIN fprod (NOLOCK) ON fprod.cnp = fi.ref
				INNER JOIN empresa  (NOLOCK) ON empresa.site = ft.site
			Where
				ft.site  in (select * from up_SplitToTable(@site,','))
				and td.tipodoc != 3
				AND (FT.tipodoc != 4 or u_tipodoc = 4)
				AND U_TIPODOC not in (1, 5)
				and ft.no >= 200
				AND ft.anulado = 0
				and fi.ofistamp = ''
				and fprod.hmr = 1
				and fdata between @dataIni and @dataFim
			GROUP BY fi.ref, empresa.nomabrv, fi.design, fi.epvori

			UNION ALL

			Select
				COUNTRY_CODE					= @countryCode
				,GEOGRAPHIC_LEVEL				= @geographicLevel
				,GEOGRAPHIC_VALUE				= @geographicValue
				,CUSTOMER_ID					= @codigo
				,CUSTOMER_NAME					= dbo.uf_RemoveSpecialChars(empresa.nomabrv)
				,GLOBAL_CHANNEL					= @global_channel
				,SUB_CHANNEL					= 'Independent'
				,[PERIOD]						= @periodo
				,PRODUCT_ID						= RTRIM(fi.ref)
				,PRODUCT_NAME					= dbo.uf_RemoveSpecialChars(LTRIM(RTRIM(fi.design)))
				,SEGMENT						= ''
				,SUB_SEGMENT					= ''
				,UNITS_QTY						= CONVERT(INT,SUM(fi.qtt))
				,UNITS_EQUIVALENT_QTY			= ''
				,NET_AMT						= CONVERT(NUMERIC(14,2),ROUND(SUM(fi.u_epvp * fi.qtt),2))
				,UNIT_SELL_PRICE				= CONVERT(NUMERIC(14,2),ROUND(fi.epvori,2))
				,CURRENCY_CODE					= @currencyCode
				,LABORATORY_NAME				= @laboratoryName
				,CATEGORY_CUST_1				= ''
				,CATEGORY_CUST_2				= ''
				,CATEGORY_CUST_3				= ''
				,CATEGORY_CUST_4				= ''
				,WTD_SELLING_DIST				= ''
				,NUM_SELLING_DIST				= ''
				,STORE_COUNT 					= ''
				,NUM_DIST						= ''
				,USABLE							= '1'
				,FREE_TEXT_1					= ''
				,FREE_TEXT_2					= ''
				,FREE_TEXT_3					= ''
				,FREE_TEXT_4					= ''
				,FREE_TEXT_5					= ''
				,FREE_NUM_1						= ''
				,FREE_NUM_2						= ''
				,FREE_NUM_3						= ''
				,FREE_DATE_1					= ''
				,FREE_DATE_2					= ''
				,FREE_DATE_3					= ''		
			FROM
				ft (NOLOCK)
				INNER JOIN ft2 (NOLOCK)	ON ft2.ft2stamp = ft.ftstamp
				INNER JOIN fi (NOLOCK)	ON fi.ftstamp = ft.ftstamp
				INNER JOIN td (NOLOCK)	ON td.ndoc = ft.ndoc
				INNER JOIN fprod (NOLOCK) ON fprod.cnp = fi.ref
				INNER JOIN empresa  (NOLOCK) ON empresa.site = ft.site
			WHERE
				ft.site  in (SELECT * FROM up_SplitToTable(@site,','))
				and ft.tipodoc = 3
				-- a seguinte condição não é necessária dado à anterior
				-- AND (FT.tipodoc != 4 or u_tipodoc = 4) AND U_TIPODOC not in (1, 5)
				and ft.no >= 200
				and fdata between @dataIni and @dataFim
				AND ft.anulado = 0
				and fistamp not in (SELECT ofistamp FROM #temp_hmrDetalheVendas)
				and fprod.hmr = 1
			GROUP BY
				fi.ref, empresa.nomabrv, fi.design, fi.epvori
	
		) a

		GROUP BY
			a.COUNTRY_CODE, a.GEOGRAPHIC_LEVEL, a.GEOGRAPHIC_VALUE, a.CUSTOMER_ID, A.CUSTOMER_NAME, a.GLOBAL_CHANNEL, a.SUB_CHANNEL, a.[PERIOD], a.PRODUCT_ID, a.PRODUCT_NAME, a.SEGMENT
			,a.SUB_SEGMENT, a.UNITS_EQUIVALENT_QTY, a.UNIT_SELL_PRICE, a.CURRENCY_CODE, a.LABORATORY_NAME, a.CATEGORY_CUST_1, a.CATEGORY_CUST_2, a.CATEGORY_CUST_3, a.CATEGORY_CUST_4
			,a.WTD_SELLING_DIST, a.NUM_SELLING_DIST, a.STORE_COUNT, a.NUM_DIST, a.USABLE, a.FREE_TEXT_1, a.FREE_TEXT_2, a.FREE_TEXT_3, a.FREE_TEXT_4, a.FREE_TEXT_5
			,a.FREE_NUM_1, a.FREE_NUM_2, a.FREE_NUM_3, a.FREE_DATE_1, a.FREE_DATE_2, a.FREE_DATE_3

	) as b
	WHERE
		1 = (CASE WHEN @comCab = 1 THEN 1 ELSE (CASE WHEN b.cab = 1 THEN 0 ELSE 1 END) END)
	ORDER BY 
		b.[order] asc, b.PRODUCT_ID
			

	END ELSE BEGIN -- desenvolvido para a INOUT

		Select
			COUNTRY_CODE	
			,GEOGRAPHIC_LEVEL
			,GEOGRAPHIC_VALUE
			,CUSTOMER_ID	
			,CUSTOMER_NAME	
			,GLOBAL_CHANNEL	
			,SUB_CHANNEL	
			,[PERIOD]		
			,PRODUCT_ID		
			,PRODUCT_NAME	
			,SEGMENT		
			,SUB_SEGMENT	
			,UNITS_QTY		
			,UNITS_EQUIVALENT_QTY
			,NET_AMT		
			,UNIT_SELL_PRICE
			,CURRENCY_CODE	
			,LABORATORY_NAME
			,CATEGORY_CUST_1
			,CATEGORY_CUST_2
			,CATEGORY_CUST_3
			,CATEGORY_CUST_4
			,WTD_SELLING_DIST
			,NUM_SELLING_DIST
			,STORE_COUNT 	
			,NUM_DIST		
			,USABLE			
			,FREE_TEXT_1	
			,FREE_TEXT_2	
			,FREE_TEXT_3	
			,FREE_TEXT_4	
			,FREE_TEXT_5	
			,FREE_NUM_1		
			,FREE_NUM_2		
			,FREE_NUM_3		
			,FREE_DATE_1	
			,FREE_DATE_2	
			,FREE_DATE_3	
		From (

			SELECT 
				cab								= 1
				,[ORDER]						= 1
				,COUNTRY_CODE					= 'COUNTRY_CODE'
				,GEOGRAPHIC_LEVEL				= 'GEOGRAPHIC_LEVEL'
				,GEOGRAPHIC_VALUE				= 'GEOGRAPHIC_VALUE'
				,CUSTOMER_ID					= 'COD_FARMACIA'
				,CUSTOMER_NAME					= 'CUSTOMER_NAME'
				,GLOBAL_CHANNEL					= 'GLOBAL_CHANNEL'
				,SUB_CHANNEL					= 'SUB_CHANNEL'
				,[PERIOD]						= 'PERIOD'
				,PRODUCT_ID						= 'PRODUCT_ID'
				,PRODUCT_NAME					= 'PRODUCT_NAME'
				,SEGMENT						= 'SEGMENT'
				,SUB_SEGMENT					= 'SUB_SEGMENT'
				,UNITS_QTY						= 'UNITS_QTY'
				,UNITS_EQUIVALENT_QTY			= 'UNITS_EQUIVALENT_QTY'
				,NET_AMT						= 'NET_AMT'
				,UNIT_SELL_PRICE				= 'UNIT_SELL_PRICE'
				,CURRENCY_CODE					= 'CURRENCY_CODE'
				,LABORATORY_NAME				= 'LABORATORY_NAME'
				,CATEGORY_CUST_1				= 'CATEGORY_CUST_1'
				,CATEGORY_CUST_2				= 'CATEGORY_CUST_2'
				,CATEGORY_CUST_3				= 'CATEGORY_CUST_3'
				,CATEGORY_CUST_4				= 'CATEGORY_CUST_4'
				,WTD_SELLING_DIST				= 'WTD_SELLING_DIST'
				,NUM_SELLING_DIST				= 'NUM_SELLING_DIST'
				,STORE_COUNT 					= 'STORE_COUNT'
				,NUM_DIST						= 'NUM_DIST'
				,USABLE							= 'USABLE'
				,FREE_TEXT_1					= 'FREE_TEXT_1'
				,FREE_TEXT_2					= 'FREE_TEXT_2'
				,FREE_TEXT_3					= 'FREE_TEXT_3'
				,FREE_TEXT_4					= 'FREE_TEXT_4'
				,FREE_TEXT_5					= 'FREE_TEXT_5'
				,FREE_NUM_1						= 'FREE_NUM_1'
				,FREE_NUM_2						= 'FREE_NUM_2'
				,FREE_NUM_3						= 'FREE_NUM_3'
				,FREE_DATE_1					= 'FREE_DATE_1'
				,FREE_DATE_2					= 'FREE_DATE_2'
				,FREE_DATE_3					= 'FREE_DATE_3'

			UNION ALL

			SELECT
				cab								= 0
				,[ORDER]						= 2
				,COUNTRY_CODE					= @countryCode
				,GEOGRAPHIC_LEVEL				= @geographicLevel
				,GEOGRAPHIC_VALUE				= @geographicValue
				,CUSTOMER_ID					= @codigo
				,CUSTOMER_NAME					= dbo.uf_RemoveSpecialChars(empresa.nomabrv)
				,GLOBAL_CHANNEL					= @global_channel
				,SUB_CHANNEL					= 'Independent'
				,[PERIOD]						= @periodo
				,PRODUCT_ID						= RTRIM(hv.ref)
				,PRODUCT_NAME					= dbo.uf_RemoveSpecialChars(LTRIM(fprod.design))
				,SEGMENT						= ''
				,SUB_SEGMENT					= ''
				,UNITS_QTY						= LTRIM(CONVERT(INT,SUM(hv.qt)))
				,UNITS_EQUIVALENT_QTY			= ''
				,NET_AMT						= LTRIM(CONVERT(NUMERIC(14,2),ROUND(SUM(hv.pvp * hv.qt),2)))
				,UNIT_SELL_PRICE				= (CASE WHEN hv.pvp <> 0 THEN LTRIM(CONVERT(NUMERIC(14,2),ROUND(hv.pvp,2))) ELSE '' END)
				,CURRENCY_CODE					= @currencyCode
				,LABORATORY_NAME				= @laboratoryName
				,CATEGORY_CUST_1				= ''
				,CATEGORY_CUST_2				= ''
				,CATEGORY_CUST_3				= ''
				,CATEGORY_CUST_4				= ''
				,WTD_SELLING_DIST				= ''
				,NUM_SELLING_DIST				= ''
				,STORE_COUNT 					= ''
				,NUM_DIST						= ''
				,USABLE							= '1'
				,FREE_TEXT_1					= ''
				,FREE_TEXT_2					= ''
				,FREE_TEXT_3					= ''
				,FREE_TEXT_4					= ''
				,FREE_TEXT_5					= ''
				,FREE_NUM_1						= ''
				,FREE_NUM_2						= ''
				,FREE_NUM_3						= ''
				,FREE_DATE_1					= ''
				,FREE_DATE_2					= ''
				,FREE_DATE_3					= ''
			FROM
				hist_vendas hv (NOLOCK)
				INNER JOIN fprod (NOLOCK) ON fprod.cnp = hv.ref	
				INNER JOIN empresa  (NOLOCK) ON empresa.site = hv.site			
			WHERE
				hv.site  in (SELECT * FROM up_SplitToTable(@site,','))
				and fprod.hmr = 1
				and hv.dia != 0
				and hv.hora != ''
				and CONVERT(DATE,CAST(hv.ano AS VARCHAR(4)) + '-' + CAST(hv.mes AS VARCHAR(2)) + '-' + CAST(hv.dia AS VARCHAR(2))) 
					between @dataIni and @dataFim
			GROUP BY 
				hv.ref, fprod.design, empresa.nomabrv, hv.pvp
					
		) a
		WHERE
			1 = (CASE WHEN @comCab = 1 THEN 1 ELSE (CASE WHEN a.cab = 1 THEN 0 ELSE 1 END) END)
		ORDER BY
			a.[ORDER] ASC,a.PRODUCT_ID
	END


	if Object_id('tempdb.dbo.#temp_hmrDetalheVendas') is not null
		drop table #temp_hmrDetalheVendas
END

GO
Grant Execute on dbo.up_hmr_detalheVendas to Public
Grant control on dbo.up_hmr_detalheVendas to Public
Go