/*
	HMR - Envio de informação
	exec up_HMR_Extract '20181101','20181210','Loja 1'

	select * from ft where fno=1 and fdata='20160102'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
  
if OBJECT_ID('[dbo].[up_HMR_Extract]') IS NOT NULL
	drop procedure dbo.up_HMR_Extract
go

create procedure dbo.up_HMR_Extract
	@dataini as datetime,
	@datafim as datetime,
	@Loja as varchar(10)
	--@codigo as varchar(254)
	
/* WITH ENCRYPTION */ 

AS
BEGIN

If OBJECT_ID('tempdb.dbo.#t_movOfGoods') IS NOT NULL
		drop table #t_movOfGoods;

	Select
		numdispcc			=	ft.no
		, tipodospcc		=	(case when ft.etotal>0 then 'FT' else 'NC' end )
		, sigladispcc		=	case 
								when ft.nmdoc='Venda a Dinheiro' and ft.cobrado=1 then 'TSR'
								when ft.nmdoc='Venda a Dinheiro' and ft.cobrado=0 then 'FR'
								when ft.nmdoc='Reg. a Cliente' then 'NC'
								when ft.nmdoc='Nota de Crédito' then 'NC'
								when ft.nmdoc='Factura' and ft.cobrado=1 then 'TS'
								when ft.nmdoc='Factura' and ft.cobrado=0 then 'FT'
								else ''
							end 
		, codfarm			=	isnull(empresa.codigo,'')
		, dtdispensa		=	ft.fdata
		, numreceita		=	''
		, validado			=	''
		, centrosaude		=	''
		, especialidade		=	''
		, numlindisp		=	fi.lordem
		, codProdRec		=	isnull(isnull(dispensa_eletronica_d.medicamento_cnpem, dispensa_eletronica_d.medicamento_cod),'')
		, codProd			=	fi.ref
		, EanProd			=	''
		, nomeProd			=	fi.design
		, pvp				=	fi.u_epvp
		, pagoUtente		=	fi.etiliquido
		, descontoUtente	=	fi.desconto
		, qt				=	fi.qtt
		, precoRef			=	st.epv1
		, posStock			=	0
		, codOrg			=	ft2.u_codigo
		, valorCompart		=	fi.etiliquido-(fi.u_epvp*fi.qtt)
		, portaria			=	dispensa_eletronica_d.diploma_cod


		--exec up_HMR_Extract '20181002','20181002','Loja 1'
	into
		#t_movOfGoods
	from 
		ft (nolock)
		inner join ft2 (nolock) on ft.ftstamp = ft2.ft2stamp
		inner join fi (nolock) on ft.ftstamp = fi.ftstamp
		inner join td (nolock) on td.ndoc = ft.ndoc
		left join B_cert (nolock) on ft.ftstamp = b_cert.stamp
		inner join empresa (nolock) on empresa.site = ft.site
		inner join st (nolock) on st.ref=fi.ref and st.site_nr=fi.armazem
		left join dispensa_eletronica (nolock) on dispensa_eletronica.token=ft2.token and dispensa_eletronica.resultado_efetivacao_cod='100003040001'
		left join dispensa_eletronica_dd (nolock) on dispensa_eletronica_dd.token=dispensa_eletronica.token
		left join dispensa_eletronica_d (nolock) on dispensa_eletronica_d.token=dispensa_eletronica_dd.token and dispensa_eletronica_d.id=dispensa_eletronica_dd.id
	where
		len(td.tiposaft) >= 2
		and td.tiposaft not in ('GR','GT','GA','GC','GD') -- MovementsOfGoods
		and td.tiposaft not in ('PF') -- working documents
		and ft.fdata between @dataini and @datafim
		and ft.site = case when @Loja = 'TODAS' then ft.site else @Loja end
		and (fi.ref != '' or fi.oref != '')
		
	order by
		fdata

	select convert(text,replace(convert(varchar(max),(
		select
			(select 
				 numdispcc = numdispcc
			from 
				#t_movOfGoods
			for xml path ('Payment'), type)
			
		 for xml path ('Payments'), type

	)),'<AuditFile>','<?xml version = "1.0" encoding="Windows-1252" standalone="yes"?>'+ CHAR(13)+CHAR(10) +'<AuditFile xmlns="urn:OECD:StandardAuditFile-Tax:PT_1.04_01" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">')) as saft

	--exec up_HMR_Extract '20181002','20181002','Loja 1'

	If OBJECT_ID('tempdb.dbo.#t_movOfGoods') IS NOT NULL
		drop table #t_movOfGoods;

END

GO
Grant Execute on dbo.up_HMR_Extract to Public
Grant control on dbo.up_HMR_Extract to Public
Go

--exec up_HMR_Extract '20181101','20181210','Loja 1'