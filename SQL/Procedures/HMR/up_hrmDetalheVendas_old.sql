/*
	IMS DETALHE DE VENDAS
	Devolve informação do detalhe de vendas
	exec up_hmr_detalheVendas '20170101', '20170131', 'F_Uniao', 1
	exec up_hmr_detalheVendas '20170101', '20170131', 'F_Uniao', 0

	Nota:
		O nome (abrev) do laboratório está hardcoded

		select id_lt,infarmed,site,nomecomp,* from empresa

		exec up_hmr_detalheVendas '20210101', '20210131', 'Loja 2', 0, ''

		SELECT abs(pvp),* from hist_vendas hv where ano=2017 and site='F_Uniao'
		SELECT CONVERT(DATE,CAST(ano AS VARCHAR(4)) + '-' + CAST(mes AS VARCHAR(2)) + '-' + CAST((case when dia=0 then 1 else dia end) AS VARCHAR(2))), * from hist_vendas where ano=2017
*/




SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_hmr_detalheVendas]') IS NOT NULL
	drop procedure dbo.up_hmr_detalheVendas
go

create procedure dbo.up_hmr_detalheVendas
	@dataIni as datetime
	,@dataFim as datetime
	,@site varchar(20)
	,@incluiHistorico bit = 0
	,@codHmr  varchar(7) = ''

/* WITH ENCRYPTION */

AS
BEGIN


	if Object_id('tempdb.dbo.#temp_hmrDetalheVendas') is not null
		drop table #temp_hmrDetalheVendas
	
	-- get farmacy code	
	declare @codigo varchar(6)
	declare @isPost bit =0

	set @codHmr = isnull(@codHmr,'')

	if(@codHmr='') begin
		set @codigo = (select infarmed from empresa(nolock) where site=@site)
		set @isPost =  (select top 1 posto from empresa(nolock) where siteposto=@site)
	end
	else
		set @codigo = rtrim(ltrim(@codHmr))
	
	
	if(@isPost=1)
	begin
		SELECT @site = @site + "," + rtrim(ltrim(site)) from empresa(nolock) where siteposto=@site
	end



	set @codigo = replicate('0',6-len(@codigo)) + @codigo
	if (@incluiHistorico=0)
	begin
		-- get sales detail
		select
			ofistamp
		into
			#temp_hmrDetalheVendas
		from
			fi (nolock)
			inner join ft (nolock) on ft.ftstamp = fi.ftstamp
			inner join td (nolock) on td.ndoc = ft.ndoc
			inner join fprod (nolock) on fprod.cnp = fi.ref
		Where
			ft.tipodoc != 3
			and ft.no > 200
			AND (FT.tipodoc != 4 or u_tipodoc = 4)
			AND td.U_TIPODOC not in (1, 5)
			AND ft.anulado = 0
			AND fi.ofistamp != ''
			and fprod.hmr = 1
			and ft.fdata >= @dataFim
		
		Select
			*
		From (
			Select
				COD_FARMACIA = @codigo
				,COD_CAMPANHA = ''
				,INI_PERIODO = convert(varchar,@dataIni,112)
				,FIM_PERIODO = convert(varchar,@dataFim,112)
				,DT_HR_MOV = convert(varchar,ft.fdata,112) + replace(ft.ousrhora,':','')
				,COD_EMP = ''
				,[CODIGO Produto] = rtrim(fi.ref)
				,[DESIGNACAO Produto] = fi.design
				,QT_TOT = convert(int,fi.qtt)
				,QT_CAMP = ''
				,VPVP = convert(int,fi.u_epvp * fi.qtt * 100)
			From
				ft (nolock)
				inner join ft2 (nolock) on ft2.ft2stamp = ft.ftstamp
				inner join fi (nolock) on fi.ftstamp = ft.ftstamp
				inner join td (nolock) on td.ndoc = ft.ndoc
				inner join fprod (nolock) on fprod.cnp = fi.ref
			Where
				ft.site  in (select * from up_SplitToTable(@site,','))
				and td.tipodoc != 3
				AND (FT.tipodoc != 4 or u_tipodoc = 4)
				AND U_TIPODOC not in (1, 5)
				and ft.no >= 200
				AND ft.anulado = 0
				and fi.ofistamp = ''
				and fprod.hmr = 1
				and fdata between @dataIni and @dataFim
		

			UNION ALL

			Select
				COD_FARMACIA = @codigo
				,COD_CAMPANHA = ''
				,INI_PERIODO = convert(varchar,@dataIni,112)
				,FIM_PERIODO = convert(varchar,@dataFim,112)
				,DT_HR_MOV = convert(varchar,ft.fdata,112) + replace(ft.ousrhora,':','')
				,COD_EMP = ''
				,[CODIGO Produto] = rtrim(fi.ref)
				,[DESIGNACAO Produto] = fi.design
				,QT_TOT = case when ft.tipodoc = 3  then abs(convert(int,fi.qtt)) * -1 else convert(int,fi.qtt) end
				,QT_CAMP = ''
				,VPVP =  case when ft.tipodoc = 3 then abs(convert(int,fi.u_epvp * fi.qtt * 100))*-1 else convert(int,fi.u_epvp * fi.qtt * 100) end
			From
				ft (nolock)
				inner join ft2 (nolock)	on ft2.ft2stamp = ft.ftstamp
				inner join fi (nolock)	on fi.ftstamp = ft.ftstamp
				inner join td (nolock)	on td.ndoc = ft.ndoc
				inner join fprod (nolock) on fprod.cnp = fi.ref
			Where
				ft.site  in (select * from up_SplitToTable(@site,','))
				and ft.tipodoc = 3
				-- a seguinte condição não é necessária dado à anterior
				-- AND (FT.tipodoc != 4 or u_tipodoc = 4) AND U_TIPODOC not in (1, 5)
				and ft.no >= 200
				and fdata between @dataIni and @dataFim
				AND ft.anulado = 0
				and fistamp not in (select ofistamp from #temp_hmrDetalheVendas)
				and fprod.hmr = 1
	
		) a
		order by
			a.DT_HR_MOV, a.[CODIGO Produto]

	end else begin -- desenvolvido para a INOUT

		Select
			*
		From (
			Select
				COD_FARMACIA = @codigo
				,COD_CAMPANHA = ''
				,INI_PERIODO = convert(varchar,@dataIni,112)
				,FIM_PERIODO = convert(varchar,@dataFim,112)
				,DT_HR_MOV = convert(varchar,ano) + replicate('0',2-len(mes)) + convert(varchar,mes) + replicate('0',2-len(dia)) + convert(varchar,dia) + replace(hora,':','')
				,COD_EMP = ''
				,[CODIGO Produto] = rtrim(hv.ref)
				,[DESIGNACAO Produto] = fprod.design
				,QT_TOT = convert(int,hv.qt)
				,QT_CAMP = ''
				,VPVP = convert(int,abs(hv.pvp) * hv.qt * 100)
			From
				hist_vendas hv (nolock)
				inner join fprod (nolock) on fprod.cnp = hv.ref				
			Where
				hv.site  in (select * from up_SplitToTable(@site,','))
				and fprod.hmr = 1
				and hv.dia != 0
				and hv.hora != ''
				and CONVERT(DATE,CAST(hv.ano AS VARCHAR(4)) + '-' + CAST(hv.mes AS VARCHAR(2)) + '-' + CAST(hv.dia AS VARCHAR(2))) 
					between @dataIni and @dataFim
					
		) a
		order by
			a.DT_HR_MOV, a.[CODIGO Produto]
	end


	if Object_id('tempdb.dbo.#temp_hmrDetalheVendas') is not null
		drop table #temp_hmrDetalheVendas
END

GO
Grant Execute on dbo.up_hmr_detalheVendas to Public
Grant control on dbo.up_hmr_detalheVendas to Public
Go