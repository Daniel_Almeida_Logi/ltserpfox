/* 
	HMR MOVIMENTOS
	Devolve informação de detalhe  movimentos internos de productos
		
	exec up_hmrMovimentosInternosLinhas 'FRr0AD2011A-9D9E-4A90-8E3', 'Loja 1'
	exec up_hmrMovimentosInternosLinhas 'FRr5A8B4ED3-944E-469E-9A3', 'Loja 1'
*/

SET ANSI_NULLS OFF 
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_hmrMovimentosInternosLinhas]') IS NOT NULL
	drop procedure dbo.up_hmrMovimentosInternosLinhas
go


create procedure dbo.up_hmrMovimentosInternosLinhas

@stamp  as varchar(25),
@site  as varchar(254)


/* WITH ENCRYPTION */ 

AS
BEGIN
	if Object_id('tempdb.dbo.#movimentosProd') is not null
		drop table #movimentosProd

	-- get farmacy code	



	
	declare @numDispCC varchar(15)
	declare @totalDispensas int = 0

	declare @qtMaxLote int = 99999

	declare @seconds int = 5



	Select
		*
		into #movimentosProd
		From (

			-- documentos internos
			Select
				 linNum             = lordem
				,prodCod            = rtrim(ltrim(isnull(dbo.uf_replaceRef(bi.ref),'')))  
				,prodNome           = ltrim(rtrim(bi.nome))	
				,prodQtd            = convert(int,abs(bi.qtt))
				,prodStk            = convert(int,
												(ISNULL((
													SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
													FROM	sl (nolock) sl
								 					WHERE	sl.ref = bi.REF AND    convert(datetime,convert(varchar(10),sl.ousrdata,120)+' ' + bo.ousrhora)   <=  Dateadd(ss,@seconds,convert(datetime,convert(varchar(10),bo.ousrdata,120)+' ' + bo.ousrhora)) 
													and armazem in  (select armazem from empresa_arm(nolock) where empresa_no  in (select  no from empresa(nolock) where site in (select * from up_SplitToTable(@site,';'))))
												),0) )
										)
				,prodPVP            =  abs(convert(int,bi.ettdeb*100))
				,movData            =  convert(datetime,convert(varchar(10),bo.ousrdata,120)+' ' + bo.ousrhora)
				,tabela             = 'BI'
				,movMotivo          =  ''		
			From bi(nolock)
			inner join bo(nolock) on bo.bostamp = bi.bostamp
			where 
				bo.bostamp = @stamp
				and ltrim(rtrim(isnull(bi.ref,'')))!=''
				and bo.site in (select * from up_SplitToTable(@site,';')) 

			Union all

			-- documentos de fornecedor
			Select
				 linNum             = lordem
				,prodCod            = rtrim(ltrim(isnull(dbo.uf_replaceRef(fn.ref),''))) 
				,prodNome           = ltrim(rtrim(fn.design))	
				,prodQtd            = convert(int,abs(fn.qtt))
				,prodStk            = convert(int,
												(ISNULL((
													SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
													FROM	sl (nolock) sl
								 					WHERE	sl.ref = fn.REF AND    convert(datetime,convert(varchar(10),sl.ousrdata,120)+' ' + sl.ousrhora)   <=  Dateadd(ss,@seconds,convert(datetime,convert(varchar(10),fo.ousrdata,120)+' ' + fo.ousrhora))
													and armazem in  (select armazem from empresa_arm(nolock) where empresa_no  in (select  no from empresa(nolock) where site in (select * from up_SplitToTable(@site,';')))) 	
												),0) )
										)
				,prodPVP            =  convert(int,fn.etiliquido*100)
				,movData            =  convert(datetime,convert(varchar(10),fo.ousrdata,120)+' ' + fo.ousrhora)
				,tabela             = 'FN' 
				,movMotivo          =  ''    		
			From fn(nolock)
			inner join fo(nolock) on fn.fostamp = fo.fostamp
			where 
				fn.fostamp = @stamp
				and (ltrim(rtrim(isnull(fn.ref,'')))!='' or  ltrim(rtrim(isnull(fn.oref,'')))!='')
				and fo.site in  (select * from up_SplitToTable(@site,';')) 
			
			Union all
			-- inventario
			Select
				 linNum             = lordem
				,prodCod            = rtrim(ltrim(isnull(dbo.uf_replaceRef(stil.ref),''))) 
				,prodNome           = ltrim(rtrim(stil.design))	
				,prodQtd            = convert(int,abs(sl.qtt))
				,prodStk            =  convert(int,abs(stock)) 
				,prodPVP            =  convert(int,sl.ett*100)
				,movData            =  convert(datetime,convert(varchar(10),sl.ousrdata,120)+' ' + sl.ousrhora)
				,tabela              = 'STIL' 
				,movMotivo          =  ''    		
			From stil(nolock)
			inner join stic(nolock) on stil.sticstamp = stic.sticstamp
			inner join sl(nolock)   on sl.sticstamp = stic.sticstamp and sl.ref = stil.ref
			where 
				stic.sticstamp = @stamp
				and ltrim(rtrim(isnull(stil.ref,'')))!='' 
				and stil.armazem in  (select armazem from empresa_arm(nolock) where empresa_no  in (select  no from empresa(nolock) where site in (select * from up_SplitToTable(@site,';')))) 	
				and stic.lanca = 1
				and sl.armazem = stil.armazem

		) a



		order by
			a.linNum asc





	select @totalDispensas = count(*) from  #movimentosProd

	select 
		*
	from 
		#movimentosProd
	order by movData asc


	
	
	
	if Object_id('tempdb.dbo.#movimentosProd') is not null
		drop table #movimentosProd
END

Go
Grant Execute on dbo.up_hmrMovimentosInternosLinhas to Public
Grant control on dbo.up_hmrMovimentosInternosLinhas to Public
Go