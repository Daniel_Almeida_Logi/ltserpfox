/* 
	HMR COMPRAS
	Devolve informacao de linhas das Compras e devoluções a fornecedor
		
	exec up_hmrComprasLinhasCertificacao '20200501 00:00','20210430 23:59','Loja 1','9020305'

	
	exec up_hmrComprasLinhasCertificacao '20200501 00:00','20210430 23:59','Loja 1','9999966'

*/

SET ANSI_NULLS OFF 
GO
SET QUOTED_IDENTIFIER OFF
GO




if OBJECT_ID('[dbo].[up_hmrComprasLinhasCertificacao]') IS NOT NULL
	drop procedure dbo.up_hmrComprasLinhasCertificacao
go

create procedure dbo.up_hmrComprasLinhasCertificacao

@dateInit  as datetime,
@dateEnd   as datetime,
@site      as varchar(18),
@PharmacyId as  varchar(18)

/* WITH ENCRYPTION */ 

AS
BEGIN





	DECLARE @site_nr INT
	SELECT  @site_nr=no from empresa(nolock) where site = @site


	declare @seconds int = 5
	


	Select
		Day           =  LEFT(CONVERT(VARCHAR(15),data, 105), 10)
		,PharmacyID   =  @PharmacyId
		,ProductID    =  codProd
		,ProductName  =  nomeProd
		,SupplierCode =  ncont	
		,SupplierName =  nome
		,[Total Quantity Purchased] = qt
		,[total Quantity Bonus] = qtBonus
		,[UnitPrice] = Pvf
		,[StockLevelAfterPurchases] = posStock
	From(
		Select
			numLinCpr   = lordem
			,codProd     = rtrim(ltrim(isnull(dbo.uf_replaceRef(fn.ref),'')))
			,numCprCC    = left(convert(varchar,fo.adoc)  + convert(varchar,DATEDIFF(SECOND,{d '1970-01-01'}, fo.ousrdata)),15)  
			,nomeProd    = ltrim(rtrim(isnull(fn.design,'')))
			,eanProd     = ''
			,qt          = convert(int,fn.qtt) - convert(int,u_bonus)
			,Pvf         = convert(int,fn.etiliquido/qtt*100)
			,stockMin    = convert(int,st.ptoenc)
			,stockMax    = convert(int,st.stmax)
			,posStock			= convert(int,
									(ISNULL((
										SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
										FROM	sl (nolock)
										WHERE	ref = fn.REF AND    convert(datetime,convert(varchar(10),sl.ousrdata,112))   <=  Dateadd(ss,@seconds,convert(datetime,convert(varchar(10),fo.ousrdata,112))) 
										and armazem in  (select armazem from empresa_arm(nolock) where empresa_no = @site_nr) 
									),0) )
							)
			,motivoDev = ''
			,qtBonus = convert(int,u_bonus)
			,qtEnc = convert(int,isnull((select top 1  qtt from bi(nolock) where bistamp = fn.bistamp order by ousrdata desc),0))
			,tabela              = 'fn'
			,data =  fo.ousrdata
			,ncont = isnull(fl.ncont,'')
			,nome = isnull(fl.nome,'')
			,fl.no
			,stamp = fo.fostamp

		From
			empresa	lojas (nolock)
			inner join fo (nolock)  on fo.site = Lojas.site
			inner join fn (nolock)  on fn.fostamp = fo.fostamp
			inner join st (nolock)  on st.ref = fn.ref and st.site_nr = @site_nr 
			inner join cm1 (nolock) on cm1.cm = fo.doccode
			left join fl(nolock) on fl.no = fo.no and fo.estab = fl.estab 



		Where

			(ltrim(rtrim(isnull(fn.ref,'')))!='' or  ltrim(rtrim(isnull(fn.oref,'')))!='')
			and st.inactivo = 0
			and fn.qtt>0
			and  fo.ousrdata >= @dateInit and  fo.ousrdata <= @dateEnd
			and cm1.FOLANSL = 1


		UNION ALL

		Select
			numLinCpr   = lordem
			,codProd     = rtrim(ltrim(isnull(dbo.uf_replaceRef(bi.ref),'')))
			,numCprCC    =  left(convert(varchar,bo.obrano)  + convert(varchar,DATEDIFF(SECOND,{d '1970-01-01'}, bo.ousrdata)),15)  
			,nomeProd    = ltrim(rtrim(isnull(bi.design,'')))
			,eanProd     = ''
			,qt          = case  when bo.nmdos like '%Devol%' then  abs(convert(int,bi.qtt))*-1 else convert(int,bi.qtt) end - case when bo.nmdos like '%Devol%' then  abs(convert(int,bi.u_bonus))*-1 else convert(int,bi.u_bonus) end
			,Pvf         = convert(int,bi.ettdeb/qtt*100)
			,stockMin    = convert(int,st.ptoenc)
			,stockMax    = convert(int,st.stmax)
			,posStock			= convert(int,
									(ISNULL((
										SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
										FROM	sl (nolock)
										WHERE	ref = bi.REF AND    convert(datetime,convert(varchar(10),sl.ousrdata,112))   <=  Dateadd(ss,@seconds,convert(datetime,convert(varchar(10),bo.ousrdata,112))) 
										and armazem in  (select armazem from empresa_arm(nolock) where empresa_no = @site_nr) 
									),0) )
							)
			,motivoDev = case when  dbo.alltrimIsNull(bi.lobs) like '%motivo%' and bo.nmdos like '%Devol%' then  dbo.alltrimIsNull(bi.lobs) else '' end
			,qtBonus =   case when bo.nmdos like '%Devol%' then  abs(convert(int,bi.u_bonus))*-1 else convert(int,bi.u_bonus) end
			,qtEnc = 0
			,tabela  = 'bi'
			,dataObra =  bo.ousrdata
			,ncont = isnull(fl.ncont,'')
			,nome = isnull(fl.nome,'')
			,fl.no
			,stamp = bo.bostamp
		From
			empresa lojas (nolock)
			inner join bo (nolock) on bo.site = Lojas.site
			inner join bi (nolock) on bi.bostamp = bo.bostamp
			inner join st (nolock) on st.ref = bi.ref and st.site_nr = @site_nr
			inner join ts (nolock) on ts.ndos = bo.ndos
			left  join fl(nolock)  on fl.no = bo.no and fl.estab = bo.estab 
		Where

			ltrim(rtrim(isnull(bi.ref,'')))!=''
			and st.inactivo = 0
			and bi.qtt>0
			and  bo.ousrdata >= @dateInit and  bo.ousrdata <= @dateEnd
			and ts.STOCKS =	1
			and ts.bdempresas = 'FL'
	 ) a
		order by
			a.data asc
	

END

Go
Grant Execute on dbo.up_hmrComprasLinhasCertificacao to Public
Grant control on dbo.up_hmrComprasLinhasCertificacao to Public
Go


