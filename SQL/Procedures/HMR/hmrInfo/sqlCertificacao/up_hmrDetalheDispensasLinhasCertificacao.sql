/*
	HRM DE DISPENSAS
	Devolve informação do detalhe de dispensa
	exec up_hmrDetalheDispensasLinhasCertificacao '20200401 00:00','20210515 23:59','Loja 1','3494348'

	
*/




SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_hmrDetalheDispensasLinhasCertificacao]') IS NOT NULL
	drop procedure dbo.up_hmrDetalheDispensasLinhasCertificacao
go


create procedure dbo.up_hmrDetalheDispensasLinhasCertificacao
	@dateInit  as datetime,
	@dateEnd   as datetime,
	@site      as varchar(18),
	@PharmacyId as  varchar(18)




	
/* WITH ENCRYPTION */

AS
BEGIN
	

	declare @susp bit = 0



	DECLARE @site_nr INT
	SELECT  @site_nr=no from empresa(nolock) where site = @site



	Select
		Day =  dtDispensa
		,PharmacyID = @PharmacyId
		,ProductID  = rtrim(ltrim(isnull(dbo.uf_replaceRef(codProd),'')))
		,ProductName =  nomeProd
		,HealthCareCentreID = centroSaude
		,DoctorSpecialityID = especialidade
		,TotalReimbursement = case when tipodoc=3 then abs(valorCompart + valorCompart2) *-1 else valorCompart + valorCompart2 end
		,[Total Quantity Sold] = qt
		,[Total Value] = case when tipodoc=3 then abs(pagoUtente)*-1 else pagoUtente end
		,[Total Discount Value]    =  case when tipodoc=3 then 0 else descontoUtente end
		,[Unit Price] = case when tipodoc=3 then   abs(pvp)*-1  else pvp end
		,[Reference Price]  = precoRef

		
								
	From (
		Select
			 codProd		    = RTRIM(LTRIM((case when fi.ref='' then fi.oref else fi.ref end)))												
			 ,dtDispensa        = LEFT(CONVERT(VARCHAR(15),ft.ousrdata, 105), 10)
			 ,nomeProd		    = RTRIM(LTRIM(fi.design)) 
			 ,pvp		        = abs(convert(int,fi.u_epvp*100))
			 ,pagoUtente		= abs(convert(int,fi.etiliquido*100))
			 ,descontoUtente    = case when abs(convert(int,convert(int,fi.u_epvp*100)*fi.qtt) -   convert(int,fi.etiliquido*100) -  convert(int,isnull(fi.u_ettent2,0)*100) - convert(int,isnull(fi.u_ettent1,0)*100)) <=0 then 0
                                            else abs(convert(int,convert(int,fi.u_epvp*100)*fi.qtt) -   convert(int,fi.etiliquido*100) -  convert(int,isnull(fi.u_ettent2,0)*100) - convert(int,isnull(fi.u_ettent1,0)*100)) end
			 ,qt                = convert(int,case when ft.tipodoc=3 then fi.qtt*(-1) else fi.qtt end)
			 ,precoRef          =case when isnull((select top 1 pref from Dispensa_Eletronica_DD(nolock) where Dispensa_Eletronica_DD.id = fi.id_Dispensa_Eletronica_D and fi.id_Dispensa_Eletronica_D!=''),0) = 0
								 then 
									isnull(convert(int,fi.u_epref*100),0) 
								 else
									convert(int,isnull((select top 1 pref from Dispensa_Eletronica_DD(nolock) where Dispensa_Eletronica_DD.id = fi.id_Dispensa_Eletronica_D and fi.id_Dispensa_Eletronica_D!=''),0)*100)	
								 end
 			,valorCompart  = convert(int,fi.u_ettent1*100)
			,u_diploma	=  case when  ltrim(rtrim(isnull(u_design,'')))! ='' or ltrim(rtrim(isnull(u_diploma,'')))!='' then left(isnull(ltrim(rtrim(u_design)),''),25) + ";"+ left(isnull(ltrim(rtrim(u_diploma)),''),24) else '' end
 			,valorCompart2  = convert(int,fi.u_ettent2*100)
			,u_diploma2	= ''
			,ft2.u_codigo
			,ft2.u_codigo2
			,tabela              = 'fi'
			,origem              =  case 
									when fi.tipodoc !=3 
									then '' 
									else  
												isnull((select 
											top 1 left(convert(varchar,YEAR(fii.ousrdata)) + convert(varchar,fii.ndoc) + convert(varchar,fii.fno),15) 
											from 
												fi(nolock) fii 		
											where
												fii.fistamp = fi.ofistamp
												),'')
									end		
										
			,fi.fistamp
			,fi.ofistamp
			,numer = left(convert(varchar,YEAR(fi.ousrdata)) +  convert(varchar,fi.ndoc) + convert(varchar,fi.fno),15) 
			,ft.tipodoc
			,centroSaude          = isnull((select top 1 rtrim(ltrim(isnull(d.local_presc_cod,''))) from Dispensa_Eletronica(nolock) d where  d.receita_nr = ft2.u_receita and  d.receita_nr !='' ),'')
			,especialidade        = left(isnull((select top 1 rtrim(ltrim(isnull(d.prescritor_especialidade,''))) from Dispensa_Eletronica(nolock) d where  d.receita_nr = ft2.u_receita and  d.receita_nr !=''),''),15)
			,medico               = isnull((select top 1 d.prescritor_nr_ordem from Dispensa_Eletronica(nolock) d where  d.receita_nr = ft2.u_receita and  d.receita_nr !=''),0)
			,dataOrder = ft.ousrdata
			,ft.ftstamp

		From fi (nolock)
			inner join ft (nolock)	on fi.ftstamp = ft.ftstamp
			left join  ft2 (nolock)	on ft.ftstamp = ft2.ft2stamp
			left  join fprod (nolock) on fprod.ref = fi.ref
			inner join empresa(nolock) on empresa.site = ft.site
			inner join td (nolock)	on td.ndoc = ft.ndoc
		Where
				(fi.ref != '' or fi.oref!='')
				and ft.site = @site
				and ft.ousrdata >= @dateInit and  ft.ousrdata <= @dateEnd
				and ft.no >= 200
			
			AND (FT.tipodoc != 4 or u_tipodoc = 4)
			AND U_TIPODOC not in (1, 5)
			and td.nmdoc not like '%import%'
		
				
	) a
	order by
			a.dataOrder asc
	





END

GO
Grant Execute on dbo.up_hmrDetalheDispensasLinhasCertificacao to Public
Grant control on dbo.up_hmrDetalheDispensasLinhasCertificacao  to Public
Go




