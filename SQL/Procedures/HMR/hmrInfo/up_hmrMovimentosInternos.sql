/* 
	HMR MOVIMENTOS
	Devolve informação de cabecalho movimentos internos de productos
		
	exec up_hmrMovimentosInternos '20210329', 'Loja 1','000000'


	exec up_hmrMovimentosInternos '20210129','Loja 2;Loja 3','9626163'

	exec up_hmrMovimentosInternos '20210129','Loja 2','9626163'


	exec up_hmrMovimentosInternos '20210129','Loja 3','9626163'
*/

SET ANSI_NULLS OFF 
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_hmrMovimentosInternos]') IS NOT NULL
	drop procedure dbo.up_hmrMovimentosInternos
go

create procedure dbo.up_hmrMovimentosInternos

@data as datetime,
@site  as varchar(254),
@codigoHmr  as varchar(15) = ''

/* WITH ENCRYPTION */ 

AS
BEGIN
	if Object_id('tempdb.dbo.#movimentosProd') is not null
		drop table #movimentosProd

	-- get farmacy code	
	declare @codigo varchar(6)
	declare @numDispCC varchar(15)
	declare @totalDispensas int = 0
	declare @qtMaxLote int = 99999

	set @codigo = (select top 1 infarmed from empresa(nolock) where site=(select top 1 * from up_SplitToTable(@site,';')))


	set @codigo = replicate('0',6-len(@codigo)) + @codigo

	set @numDispCC = @codigo + convert(varchar, getdate(), 12)



	Select
		*
		into #movimentosProd
		From (
		Select
			 movNum             =  left(ltrim(rtrim(sl.adoc))  + convert(varchar,DATEDIFF(SECOND,{d '1970-01-01'}, sl.ousrdata)),25) 
			,codFarm			=  @codigoHmr
			,movTipo            =  ltrim(rtrim(cmdesc)) 
			,movData            =  convert(datetime,convert(varchar(10),sl.ousrdata,120)+' ' + sl.ousrhora)
			,linNum             = ROW_NUMBER() OVER(PARTITION BY  sl.cmdesc  ORDER BY sl.ousrdata DESC, sl.ousrhora DESC)  
			,prodCod            = rtrim(ltrim(isnull(dbo.uf_replaceRef(sl.ref),''))) 
			,prodNome           = ltrim(rtrim(sl.design))	
			,prodQtd            = convert(int,sl.qtt)
			,prodStk            = convert(int,
											(ISNULL((
												SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
												FROM	sl (nolock) sl1
												WHERE	sl1.ref = sl.REF AND    convert(datetime,convert(varchar(10),sl1.ousrdata,120)+' ' + sl1.ousrhora)   <=  convert(datetime,convert(varchar(10),sl.ousrdata,120)+' ' + sl.ousrhora) 
												and armazem in  (select armazem from empresa_arm(nolock) where empresa_no  in (select  no from empresa(nolock) where site in (select * from up_SplitToTable(@site,';')))) 	
											),0) )
									)
			,prodPVP            =  abs(convert(int,sl.ett*100))      
			,sl.fnstamp
			,sl.bistamp	
			,sl.slstamp
			,sticstamp          = isnull(sl.sticstamp,'')
			,bostamp            = isnull(bi.bostamp,'')
			,fostamp            = isnull(fn.fostamp,'')				
		From sl(nolock)
		left join bi (nolock)   on bi.bistamp     = sl.bistamp   and bi.armazem in (select  no from empresa(nolock) where site in (select * from up_SplitToTable(@site,';'))) and sl.bistamp!=''
		left join fn (nolock)   on fn.fnstamp     = sl.fnstamp   and fn.armazem in (select  no from empresa(nolock) where site in (select * from up_SplitToTable(@site,';'))) and fn.fnstamp!=''
		left join stil (nolock) on stil.stilstamp = sl.sticstamp and sl.armazem in (select  no from empresa(nolock) where site in (select * from up_SplitToTable(@site,';'))) and sl.sticstamp!=''
		where 
			sl.ousrdata = @data
			and sl.armazem in (select  no from empresa(nolock) where site in (select * from up_SplitToTable(@site,';')))
			and (cmdesc like '%Acerto%' or cmdesc like '%Inventário%' or cmdesc like '%interno%' or cmdesc like '%consumo%'
			or cmdesc like '%trf%' 
			)


		) a



	select @totalDispensas = count(*) from  #movimentosProd

	select 
		distinct
		numRegCpr = @totalDispensas
		,*
	from 
		#movimentosProd
	order by movData asc


	
	
	
	if Object_id('tempdb.dbo.#movimentosProd') is not null
		drop table #movimentosProd
END

Go
Grant Execute on dbo.up_hmrMovimentosInternos to Public
Grant control on dbo.up_hmrMovimentosInternos to Public
Go