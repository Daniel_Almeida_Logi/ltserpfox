/*
	HRM DE DISPENSAS
	Devolve informação de cabecalho detalhe de dispensa
	exec up_hmrDetalheDispensas '20210426', 'Loja 1','9999966'
	exec up_hmrDetalheDispensas '20210426', 'Loja 3','9999966'

	2021319269

	postos:
	exec up_hmrDetalheDispensas '20220315','Loja 2;Loja 3','9626163'
	exec up_hmrDetalheDispensas '20220315','Loja 1','9626163'
	exec up_hmrDetalheDispensas '20220315','Loja 3','9626163'


	select *from ft(nolock) where site='Loja 3' order by fdata desc
*/ 



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_hmrDetalheDispensas]') IS NOT NULL
	drop procedure dbo.up_hmrDetalheDispensas
go

create procedure dbo.up_hmrDetalheDispensas
	@data as datetime
	,@site  as varchar(50)
	,@codigoHmr  as varchar(15) = ''


/* WITH ENCRYPTION */

AS
BEGIN

	
	-- get farmacy code	
	declare @codigo varchar(6)
	declare @numDispCC varchar(15)
	declare @totalDispensas int = 0
	declare @qtMaxLote int = 999999




	set @codigo = (select top 1 infarmed from empresa(nolock) where site=(select top 1 * from up_SplitToTable(@site,';')))




	set @codigo = replicate('0',6-len(@codigo)) + @codigo

	set @numDispCC = @codigoHmr +convert(varchar, @data, 12)

	DECLARE @operator		    VARCHAR(8) = 'ONL'

	If OBJECT_ID('tempdb.dbo.#dadosVendas') IS NOT NULL
   		DROP TABLE #dadosVendas


	Select
			 @totalDispensas = count(ftstamp)

		From
			empresa	lojas (nolock)
			inner join ft (nolock)	on ft.site = Lojas.site
			inner join ft2 (nolock)	on ft2.ft2stamp = ft.ftstamp
			inner join td (nolock)	on td.ndoc = ft.ndoc
		Where
	
			ft.no >= 200
			and ft.ousrdata = @data
			and ft.site in  (select * from up_SplitToTable(@site,';')) 
			AND (FT.tipodoc != 4 or u_tipodoc = 4)
			AND U_TIPODOC not in (1, 5)
			and td.nmdoc not like '%import%'


	Select
			 numRegDisp         = @totalDispensas
			 ,qtLotes           = 1
			 ,qtMaxLote         = @qtMaxLote
			 ,numEnvDispCC      = @numDispCC

		     ,numDispCC        = left(convert(varchar,YEAR(ft.ousrdata))  + convert(varchar,ft.ndoc) +  convert(varchar,ft.fno),15) 
			 ,tipoDispCC        = case when ft.tipodoc = 3 then 'NC' else 'FT' end
			 ,siglaDispCC       = case when ft.nmdoc='Reg. a Cliente' then  ft.nmdoc + ' (NCR)' 
								       when ft.nmdoc like '%factura%'	 and ft.cobrado = 1 then  ft.nmdoc + ' (TS)' 	
								       when ft.nmdoc like '%venda%'	 and ft.cobrado = 1 then  ft.nmdoc + ' (TSR)'
								       else ft.nmdoc +  ' ('+ ltrim(rtrim(isnull(td.tiposaft,'')))  + ')'	
								  end
			 ,codFarm			= @codigoHmr
			 ,dtDispensa        = convert(datetime,convert(varchar(10),ft.ousrdata,120)+' ' + ft.ousrhora)
			 ,numReceita		= RTRIM(LTRIM(isnull(ft2.u_receita,'')))
			 ,validado			= isnull((select 
											top 1 'S'
											from 
												ext_esb_associatecard(nolock)  ac
											where
												ac.atendStamp = ft.u_nratend 
												and ac.id = ft2.u_nbenef2
												
											order by 
												ac.ousrdata
											desc),'N') 
			,centroSaude          = isnull((select top 1 rtrim(ltrim(isnull(d.local_presc_cod,''))) from Dispensa_Eletronica(nolock) d where  d.receita_nr = ft2.u_receita and d.receita_nr!=''),'')
			,especialidade        = left(isnull((select top 1 rtrim(ltrim(isnull(d.prescritor_especialidade,''))) from Dispensa_Eletronica(nolock) d where  d.receita_nr = ft2.u_receita and d.receita_nr!=''),''),15)
			,medico               = isnull((select top 1 d.prescritor_nr_ordem from Dispensa_Eletronica(nolock) d where  d.receita_nr = ft2.u_receita and d.receita_nr!=''),0)
			,ftstamp
			,ft.nmdoc
			,online               =  isnull((select 
											top 1 1
											from 
												fi(nolock)  
											inner join 
												bi(nolock) on bi.bistamp=fi.bistamp			
											where
												fi.ftstamp = ft.ftstamp 
												and bi.ousrinis = @operator
									),0)

		 	,origem              =   isnull((
											select	top 1 left(convert(varchar,YEAR(fiiOrg.ousrdata)) + convert(varchar,fiiOrg.ndoc) +  convert(varchar,fiiOrg.fno),15) 
											from 
												fi(nolock) fii	
												inner join fi(nolock) fiiOrg on fii.ofistamp = 	fiiOrg.fistamp  and fii.ofistamp !=''	
											where
												fii.ftstamp = ft.ftstamp
												),'')


			,cobrado
			,ft.tipodoc												 

		into
			#dadosVendas
		From
			empresa	lojas (nolock)
			inner join ft (nolock)	on ft.site = Lojas.site
			inner join ft2 (nolock)	on ft2.ft2stamp = ft.ftstamp
			inner join td (nolock)	on td.ndoc = ft.ndoc
		Where
	
			ft.no >= 200
			and ft.ousrdata = @data
			and ft.site  in (select * from up_SplitToTable(@site,';')) 
			AND (FT.tipodoc != 4 or u_tipodoc = 4)
			AND U_TIPODOC not in (1, 5)
			and td.nmdoc not like '%import%'
	

		order by convert(datetime,convert(varchar(10),ft.ousrdata,120)+' ' + ft.ousrhora)



		select
			  numRegDisp         
			 ,qtLotes           
			 ,qtMaxLote         
			 ,numEnvDispCC      
		     ,numDispCC       
			 ,tipoDispCC 
			 ,siglaDispCC  = case when isnull(online,0) = 0 then siglaDispCC  else 'OL' end    
			 ,codFarm			
			 ,dtDispensa        
			 ,numReceita		
			 ,validado			
			 ,centroSaude         
			 ,especialidade        
			 ,medico               
			 ,ftstamp
			 ,nmdoc
			 ,origem = case when tipodoc = 3 then isnull(origem,'') else '' end
			 ,cobrado
		from
			#dadosVendas
		order by  dtDispensa asc
	
	If OBJECT_ID('tempdb.dbo.#dadosVendas') IS NOT NULL
   		DROP TABLE #dadosVendas

END

GO
Grant Execute on dbo.up_hmrDetalheDispensas to Public
Grant control on dbo.up_hmrDetalheDispensas  to Public
Go


