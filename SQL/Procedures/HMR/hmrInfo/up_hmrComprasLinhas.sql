/* 
	HMR COMPRAS
	Devolve informacao de linhas das Compras e devoluções a fornecedor
		
	exec up_hmrComprasLinhas 'naCB31454A-82FA-43C6-AFC','Loja 2'


	exec up_hmrComprasLinhas naCB31454A-82FA-43C6-AFC','Loja 1'



	exec up_hmrComprasLinhas 'naCB31454A-82FA-43C6-AFC ','Loja 2;Loja 3'




*/

SET ANSI_NULLS OFF 
GO
SET QUOTED_IDENTIFIER OFF
GO



if OBJECT_ID('[dbo].[up_hmrComprasLinhas]') IS NOT NULL
	drop procedure dbo.up_hmrComprasLinhas
go

create procedure dbo.up_hmrComprasLinhas


@stamp as varchar(25),
@site as varchar(254)

/* WITH ENCRYPTION */ 

AS
BEGIN





	declare @seconds int = 5
	


	Select
		distinct
		*
	From(
		Select			 
			numLinCpr   = lordem
			,codProd     = rtrim(ltrim(isnull(dbo.uf_replaceRef(fn.ref),''))) 
			,nomeProd    = ltrim(rtrim(isnull(fn.design,'')))
			,eanProd     = ''
			,qt          = convert(int,fn.qtt) - convert(int,u_bonus)
			,Pvf         = convert(int,fn.etiliquido/qtt*100)
			,stockMin    = convert(int,st.ptoenc)
			,stockMax    = convert(int,st.stmax)
			,posStock			= convert(int,
									(ISNULL((
										SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
										FROM	sl (nolock)
										WHERE	ref = fn.REF AND    convert(datetime,convert(varchar(10),sl.ousrdata,120)+' ' + sl.ousrhora)   <=  Dateadd(ss,@seconds,convert(datetime,convert(varchar(10),fo.ousrdata,120)+' ' + fo.ousrhora)) 
										and armazem in  (select armazem from empresa_arm(nolock) where empresa_no  in (select  no from empresa(nolock) where site in (select * from up_SplitToTable(@site,';')))) 
									),0) )
							)
			,motivoDev = ''
			,qtBonus = convert(int,u_bonus)
			,qtEnc = convert(int,isnull((select top 1  qtt from bi(nolock) where bistamp = fn.bistamp order by ousrdata desc),0))
			,tabela              = 'fn'
	

		From
			empresa	lojas (nolock)
			inner join fo (nolock)  on fo.site = Lojas.site
			inner join fn (nolock)  on fn.fostamp = fo.fostamp
			inner join st (nolock)  on st.ref = fn.ref and st.site_nr in (select  no from empresa(nolock) where site in (select * from up_SplitToTable(@site,';'))) 
			inner join cm1 (nolock) on cm1.cm = fo.doccode
		
		Where
			fo.fostamp= @stamp
			and (ltrim(rtrim(isnull(fn.ref,'')))!='' or  ltrim(rtrim(isnull(fn.oref,'')))!='')
			and st.inactivo = 0
			and fn.qtt>0
			and fo.site in (select * from up_SplitToTable(@site,';'))
	


		UNION ALL

		Select
	
			numLinCpr   = lordem
			,codProd     = rtrim(ltrim(isnull(dbo.uf_replaceRef(bi.ref),''))) 
			,nomeProd    = ltrim(rtrim(isnull(bi.design,'')))
			,eanProd     = ''
			,qt          = case  when bo.nmdos like '%Devol%' then  abs(convert(int,bi.qtt))*-1 else convert(int,bi.qtt) end - case when bo.nmdos like '%Devol%' then  abs(convert(int,bi.u_bonus))*-1 else convert(int,bi.u_bonus) end
			,Pvf         = convert(int,bi.ettdeb/qtt*100)
			,stockMin    = convert(int,st.ptoenc)
			,stockMax    = convert(int,st.stmax)
			,posStock			= convert(int,
									(ISNULL((
										SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
										FROM	sl (nolock)
										WHERE	ref = bi.REF AND    convert(datetime,convert(varchar(10),sl.ousrdata,120)+' ' + sl.ousrhora)   <=   Dateadd(ss,@seconds,convert(datetime,convert(varchar(10),bo.ousrdata,120)+' ' + bo.ousrhora)) 
										and armazem in  (select armazem from empresa_arm(nolock) where empresa_no  in (select  no from empresa(nolock) where site in (select * from up_SplitToTable(@site,';')))) 
									),0) )
							)
			,motivoDev = case when  dbo.alltrimIsNull(bi.lobs) like '%motivo%' and bo.nmdos like '%Devol%' then  dbo.alltrimIsNull(bi.lobs) else '' end
			,qtBonus =   case when bo.nmdos like '%Devol%' then  abs(convert(int,bi.u_bonus))*-1 else convert(int,bi.u_bonus) end
			,qtEnc = 0
			,tabela              = 'bi'
	
		From
			empresa lojas (nolock)
			inner join bo (nolock) on bo.site = Lojas.site
			inner join bi (nolock) on bi.bostamp = bo.bostamp
			inner join st (nolock) on st.ref = bi.ref and st.site_nr in (select  no from empresa(nolock) where site in (select * from up_SplitToTable(@site,';'))) 
			inner join ts (nolock) on ts.ndos = bo.ndos
	
		Where
			bo.bostamp= @stamp
			and ltrim(rtrim(isnull(bi.ref,'')))!=''
			and st.inactivo = 0
			and bi.qtt>0
			and bo.site in (select * from up_SplitToTable(@site,';')) 

		
	 ) a
		order by
			a.numLinCpr asc
	

END

Go
Grant Execute on dbo.up_hmrComprasLinhas to Public
Grant control on dbo.up_hmrComprasLinhas to Public
Go


