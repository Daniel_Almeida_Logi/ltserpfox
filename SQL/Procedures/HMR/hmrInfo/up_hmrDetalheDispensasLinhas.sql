/*
	HRM DE DISPENSAS
	Devolve informação do detalhe de dispensa
	exec up_hmrDetalheDispensasLinhas 'ADMFCA471F3-7EBA-43EB-BC1','Loja 1'


	exec up_hmrDetalheDispensasLinhas 'UBFC05237C-6E83-4035-8DE ','Loja 1'



	select *from ft(nolock) order by ousrdata desc

	
*/






SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_hmrDetalheDispensasLinhas]') IS NOT NULL
	drop procedure dbo.up_hmrDetalheDispensasLinhas
go



create procedure dbo.up_hmrDetalheDispensasLinhas
	@ftstamp  as varchar(25),
	@site as varchar(254)


	
/* WITH ENCRYPTION */

AS
BEGIN
	
	if Object_id('tempdb.dbo.#temp_hmrDetalheDispensasLinhas') is not null
		drop table #temp_hmrDetalheDispensasLinhas

	declare @susp bit = 0

	declare @seconds int = 5

	select top 1 @susp=isnull(cobrado,0) from ft(nolock) where ftstamp=@ftstamp and site in (select * from up_SplitToTable(@site,';'))


	
	Select
		 numLinDisp
		,codProd = rtrim(ltrim(isnull(dbo.uf_replaceRef(codProd),'')))
		,codProdRec
		,nomeProdRec 
		,eanProd 
		,productCodeScheme 
		,batchId
		,packSerialNumber
		,dtDispensa
		,nomeProd
		,pvp
		,pagoUtente
		,descontoUtente = case when tipodoc=3 then 0 else descontoUtente end
		,qt
		,precoRef
		,posStock
		,codOrg          = codOrg 
		,valorCompart    = valorCompart 
		,u_diploma       = left(u_diploma,50)   
		,codOrg2         = codOrg2 
		,valorCompart2   = valorCompart2 
		,u_diploma2      = left(u_diploma2,50) 
		,u_codigo        = u_codigo   
		,u_codigo2       = u_codigo2  
		,tabela
		,origem
		,fistamp
		,ofistamp
		,numer
		,tipodoc									

	From (
		Select
		     numLinDisp        = lordem
			 ,codProd		   = RTRIM(LTRIM((case when fi.ref='' then fi.oref else fi.ref end)))
			 ,codProdRec       =  case  when ft.nmdoc = 'Reg. a Cliente' then
														isnull((select top 1 
															case 
																when medicamento_cod!='' then medicamento_cod 
																else medicamento_cnpem 
																end
														from Dispensa_Eletronica_D ded (nolock)
														left join fi fi_aux(nolock) on fi_aux.id_dispensa_eletronica_d=ded.id
														where fi_aux.fistamp = fi.ofistamp
														),'')
													else
														isnull((select top 1 
															case 
																when medicamento_cod!='' then medicamento_cod 
																else medicamento_cnpem 
																end
														from Dispensa_Eletronica_D ded (nolock)
														where ded.id = fi.id_dispensa_eletronica_d
														),'') 
											end	
			 ,nomeProdRec       =  case  when ft.nmdoc = 'Reg. a Cliente' then
														isnull((select top 1 
															rtrim(ltrim(isnull(medicamento_descr,'')))
														from Dispensa_Eletronica_D ded (nolock)
														left join fi fi_aux(nolock) on fi_aux.id_dispensa_eletronica_d=ded.id
														where fi_aux.fistamp = fi.ofistamp
														),'')
													else
														isnull((select top 1 
															rtrim(ltrim(isnull(medicamento_descr,'')))
														from Dispensa_Eletronica_D ded (nolock)
														where ded.id = fi.id_dispensa_eletronica_d
														),'') 
													end													
			 ,eanProd           = isnull((select top 1 ltrim(rtrim(isnull(productCode,''))) from fi_trans_info(nolock) where fi_trans_info.recStamp = fi.fistamp),'')
			 ,productCodeScheme = isnull((select top 1 ltrim(rtrim(isnull(productCodeScheme,''))) from fi_trans_info(nolock) where fi_trans_info.recStamp = fi.fistamp),'')
			 ,batchId           = isnull((select top 1 ltrim(rtrim(isnull(batchId,''))) from fi_trans_info(nolock) where fi_trans_info.recStamp = fi.fistamp),'')
			 ,packSerialNumber  = isnull((select top 1 ltrim(rtrim(isnull(packSerialNumber,''))) from fi_trans_info(nolock) where fi_trans_info.recStamp = fi.fistamp),'')
			 ,dtDispensa        = convert(datetime,convert(varchar(10),ft.ousrdata,120)+' ' + ft.ousrhora)
			 ,nomeProd		    = RTRIM(LTRIM(fi.design)) 
			 ,pvp		        = abs(convert(int,fi.u_epvp*100))
			 ,pagoUtente		= abs(convert(int,fi.etiliquido*100))
			 ,descontoUtente    = case when abs(convert(int,convert(int,fi.u_epvp*100)*fi.qtt) -   convert(int,fi.etiliquido*100) -  convert(int,isnull(fi.u_ettent2,0)*100) - convert(int,isnull(fi.u_ettent1,0)*100)) <=0 then 0
                                            else abs(convert(int,convert(int,fi.u_epvp*100)*fi.qtt) -   convert(int,fi.etiliquido*100) -  convert(int,isnull(fi.u_ettent2,0)*100) - convert(int,isnull(fi.u_ettent1,0)*100)) end
			-- ,descontoUtente	= abs(convert(int,fi.u_epvp*100*qtt)) *   (fi.desconto/100) + abs(convert(int,fi.u_descval*100))									    
			 ,qt                = convert(int,case when ft.tipodoc=3 then fi.qtt*(-1) else fi.qtt end)
			 ,precoRef          =case when isnull((select top 1 pref from Dispensa_Eletronica_DD(nolock) where Dispensa_Eletronica_DD.id = fi.id_Dispensa_Eletronica_D and fi.id_Dispensa_Eletronica_D!=''),0) = 0
								 then 
									isnull(convert(int,fi.u_epref*100),0) 
								 else
									convert(int,isnull((select top 1 pref from Dispensa_Eletronica_DD(nolock) where Dispensa_Eletronica_DD.id = fi.id_Dispensa_Eletronica_D and fi.id_Dispensa_Eletronica_D!=''),0)*100)	
								 end
			 ,posStock			= convert(int,
											(ISNULL((
												SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
												FROM	sl (nolock)
												WHERE	ref = fi.REF AND    convert(datetime,convert(varchar(10),sl.ousrdata,120)+' ' +  sl.ousrhora)   <=  Dateadd(ss,@seconds,convert(datetime,convert(varchar(10),ft.ousrdata,120)+' ' + ft.ousrhora)) 
												and armazem in  (select armazem from empresa_arm(nolock) where empresa_no  in (select  no from empresa(nolock) where site in (select * from up_SplitToTable(@site,';')))) 
											),0) )
									)
			
			,codOrg	=  case when fi.u_ettent1 > 0 then 
										case when isnumeric(ft2.u_codigo) = 0 then isnull(ft2.u_codigo,'') else right("0" + isnull(ft2.u_codigo,''),3)  end 
						  else  '' end
 			,valorCompart  = convert(int,fi.u_ettent1*100)
			,u_diploma	=  case when  ltrim(rtrim(isnull(u_design,'')))! ='' or ltrim(rtrim(isnull(u_diploma,'')))!='' then left(isnull(ltrim(rtrim(u_design)),''),25) + ";"+ left(isnull(ltrim(rtrim(u_diploma)),''),24) else '' end
			,codOrg2	= case when fi.u_ettent2 > 0 then 
										case when isnumeric(ft2.u_codigo2) = 0 then isnull(ft2.u_codigo2,'') else right("0" + isnull(ft2.u_codigo2,''),3)  end 
						  else  '' end
 			,valorCompart2  = convert(int,fi.u_ettent2*100)
			,u_diploma2	=   left(isnull(ltrim(rtrim(u_design2)),''),50)
			,ft2.u_codigo
			,ft2.u_codigo2
			,tabela              = 'fi'
			,origem              =  case 
									when fi.tipodoc !=3 
									then '' 
									else  
												isnull((select 
											top 1 left(convert(varchar,YEAR(fii.ousrdata)) + convert(varchar,fii.ndoc) + convert(varchar,fii.fno),15) 
											from 
												fi(nolock) fii 		
											where
												fii.fistamp = fi.ofistamp
												),'')
									end		
										
			,fi.fistamp
			,fi.ofistamp
			,numer = left(convert(varchar,YEAR(fi.ousrdata)) +  convert(varchar,fi.ndoc) + convert(varchar,fi.fno),15) 
			,ft.tipodoc

		From fi (nolock)
			inner join ft (nolock)	on fi.ftstamp = ft.ftstamp
			left join  ft2 (nolock)	on ft.ftstamp = ft2.ft2stamp
			left  join fprod (nolock) on fprod.ref = fi.ref
		Where
				fi.ftstamp = @ftstamp
				and (fi.ref != '' or fi.oref!='')
				and ft.site in (select * from up_SplitToTable(@site,';')) 
	) a
	order by
			a.numLinDisp asc
	

	if Object_id('tempdb.dbo.#temp_hmrDetalheDispensasLinhas') is not null
		drop table #temp_hmrDetalheDispensasLinhas




END

GO
Grant Execute on dbo.up_hmrDetalheDispensasLinhas to Public
Grant control on dbo.up_hmrDetalheDispensasLinhas  to Public
Go




