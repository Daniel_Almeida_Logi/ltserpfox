
/* 
	HMR Catalogo de productos
	Devolve informação de todos os productos
		
	exec [up_hmrCatalogoProductos]  '20210305', 'Loja 2;Loja 3','00000'

	exec [up_hmrCatalogoProductos]  '20210305', 'Loja 2','00000'

	exec [up_hmrCatalogoProductos]  '20210305', 'Loja 3','00000'



*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_hmrCatalogoProductos]') IS NOT NULL
	drop procedure dbo.[up_hmrCatalogoProductos]
go


create procedure dbo.[up_hmrCatalogoProductos]


@data as datetime,
@site  as varchar(254),
@codigoHmr  as varchar(15) = ''

/* WITH ENCRYPTION */ 

AS
BEGIN

	
	If OBJECT_ID('tempdb.dbo.#tempTableStocks') IS NOT NULL
   		DROP TABLE #tempTableStocks


	-- get farmacy code	
	declare @codigo varchar(6)
	declare @numDispCC varchar(15)
	declare @numEnvCatalogo varchar(15)
	declare @totalDispensas int = 0
	declare @qtMaxLote int = 99999

	set @codigo = (select top 1 infarmed from empresa(nolock) where site=(select top 1 * from up_SplitToTable(@site,';')))


	set @codigo = replicate('0',6-len(@codigo)) + @codigo

	set @numDispCC = @codigo + convert(varchar, getdate(), 12)

	select top 1 @numEnvCatalogo= rtrim(ltrim(left(newid(),15)))

	Select
		numEnvCatalogo       =  @numEnvCatalogo
		,dataCatalogo        =  @data
		,codFarm			 =  @codigoHmr
		,prodCod             =  rtrim(ltrim(isnull(dbo.uf_replaceRef(st.ref),'')))
		,prodNome            =  rtrim(ltrim(isnull(st.design,'')))
		,prodStk             =  (select sum(stock)  from st(nolock) s where s.ref=st.ref and s.site_nr in   (select  no from empresa(nolock) where site in (select * from up_SplitToTable(@site,';'))))
		,prodPVP             =  convert(int,st.epv1*100)
		,rowNumber           = ROW_NUMBER() OVER(PARTITION BY ref ORDER BY site_nr ASC) 

	into 
		#tempTableStocks				
	From st(nolock)
	where 
		st.site_nr  in (select  no from empresa(nolock) where site in (select * from up_SplitToTable(@site,';')))
		and inactivo = 0	
	group by st.design, st.ref, st.epv1, st.site_nr



	select 
		*
	from
		#tempTableStocks
	where
		rowNumber = 1
	order by 
		prodCod
	asc


		
	If OBJECT_ID('tempdb.dbo.#tempTableStocks') IS NOT NULL
   		DROP TABLE #tempTableStocks
END



