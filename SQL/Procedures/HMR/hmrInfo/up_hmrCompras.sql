/* 
	HMR COMPRAS
	Devolve informação dos cabecalhos das Compras e devoluções a fornecedor
		
	exec up_hmrCompras '20210422', 'Loja 1','000000'


	exec up_hmrComprasLinhas 'NM19DE77C1-26E8-444B-8A7','Loja 1'

	select  docdata, data , ousrdata, ousrhora, *  from fo(nolock) where fostamp='NM19DE77C1-26E8-444B-8A7'


	select rdata, ousrdata, ousrHora from fi(nolock) where ref='5745765'  and rdata='2021-04-23 00:00:00.000'

	exec up_hmrCompras '20220315','Loja 2;Loja 3','9626163'
	exec up_hmrCompras '20220315','Loja 2','9626163'
	exec up_hmrCompras '20220315','Loja 3','9626163'



	
*/

SET ANSI_NULLS OFF 
GO
SET QUOTED_IDENTIFIER OFF
GO



if OBJECT_ID('[dbo].[up_hmrCompras]') IS NOT NULL
	drop procedure dbo.up_hmrCompras
go


create procedure dbo.up_hmrCompras

@data as datetime,
@site  as varchar(254),
@codigoHmr  as varchar(15) = ''

/* WITH ENCRYPTION */ 

AS
BEGIN
	if Object_id('tempdb.dbo.#movimentosProd') is not null
		drop table #movimentosProd

	-- get farmacy code	
	declare @codigo varchar(6)
	declare @numDispCC varchar(15)
	declare @totalDispensas int = 0
	declare @qtMaxLote int = 99999

	set @codigo = (select top 1 infarmed from empresa(nolock) where site=(select top 1 * from up_SplitToTable(@site,';')))

	set @codigo = replicate('0',6-len(@codigo)) + @codigo

	set @numDispCC = @codigoHmr +convert(varchar, @data, 12)




	
	Select
		*
		into #movimentosProd
		From (
		Select
			 qtMaxLote          =  @qtMaxLote
			,qtLotes            =  1
			,numCprCC           =  left(convert(varchar,fo.adoc)  + convert(varchar,DATEDIFF(SECOND,{d '1970-01-01'}, fo.ousrdata)),15) 
			,codFarm			=  @codigoHmr
			,tipoCPRCC          =  case when fo.docnome like  '%V/Factura%'       then  'REC'   
								        when fo.docnome like  '%V/Nt. Crédito%'   then  'RDF' 
										when fo.docnome like  '%V/Nt. Débito%'    then  'RDF' 
										when fo.docnome like  '%V/Vd. Dinhei.%'   then  'RDF' 
										when fo.docnome like  '%V/Guia%'          then  'REC' 
										else 'RDF'
								   end				
			,codCpr             =  left(convert(varchar,fo.adoc),5)
			,Desforn	        =  RTRIM(LTRIM(fo.nome))  
			,stamp              =  fo.fostamp
			,"tipo"             =  fo.docnome
			,"dtCompra"        =  convert(datetime,convert(varchar(10),fo.ousrdata,120)+' ' + fo.ousrhora)
		From
			empresa	lojas (nolock)
			inner join fo (nolock) on fo.site = Lojas.site
			inner join cm1 (nolock) on cm1.cm = fo.doccode

		Where
			Lojas.site	     in (select * from up_SplitToTable(@site,';')) 
			and fo.ousrdata	 = @data
			and cm1.FOLANSL = 1
			and fo.site      in (select * from up_SplitToTable(@site,';')) 
		

		UNION ALL


		Select
			qtMaxLote           =  @qtMaxLote
			,qtLotes            =  1
			,numCprCC           =  left(convert(varchar,bo.obrano)  + convert(varchar,DATEDIFF(SECOND,{d '1970-01-01'}, bo.ousrdata)),15) 
			,codFarm			=  @codigoHmr
			,tipoCPRCC          =  case when bo.nmdos like  '%Devol. a Fornecedor%'  then 'GD'  else '' end 															
			,codCpr             =  left(convert(varchar,bo.obrano),5)
			,Desforn	        =  RTRIM(LTRIM(bo.nome)) 
			,stamp              =  bo.bostamp
			,"tipo"             =  bo.nmdos
			,"dtCompra"         =  convert(datetime,convert(varchar(10),bo.ousrdata,120)+' ' + bo.ousrhora)
		From
			empresa lojas (nolock)
			inner join bo (nolock) on bo.site = Lojas.site
			inner join ts (nolock) on ts.ndos = bo.ndos

		Where
			Lojas.site	in (select * from up_SplitToTable(@site,';')) 
			and bo.ousrdata = @data 
			and ts.STOCKS =	1
			and ts.bdempresas = 'FL'
			and bo.site in (select * from up_SplitToTable(@site,';')) 
	
		) a


	select @totalDispensas = count(*) from  #movimentosProd

	select 
		distinct
		numRegCpr = convert(int,@totalDispensas)
		,*
	from 
		#movimentosProd
	order by dtCompra asc
	
	if Object_id('tempdb.dbo.#movimentosProd') is not null
		drop table #movimentosProd
END

Go
Grant Execute on dbo.up_hmrCompras to Public
Grant control on dbo.up_hmrCompras to Public
Go