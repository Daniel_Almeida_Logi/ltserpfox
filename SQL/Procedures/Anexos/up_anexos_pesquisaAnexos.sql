/*


Daniel Almeida
Criado: 2023-03-16

 SP que carrega Dados de anexos

exec up_anexos_pesquisaAnexos 'ADM77025F81-7110-4E00-8ED', 'FT', 'ADM'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_anexos_pesquisaAnexos]') IS NOT NULL
	drop procedure dbo.up_anexos_pesquisaAnexos
go

create procedure dbo.up_anexos_pesquisaAnexos

@stamp varchar(100),
@tabela varchar(100),
@op  varchar(25) 

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON
	
	set @stamp  = dbo.alltrimIsNull(@stamp) 
	set @tabela = dbo.alltrimIsNull(@tabela) 
	set @op     = dbo.alltrimIsNull(@op)  

	select 
		CAST(0 AS bit) as sel
		,filename AS nomeficheiro
		,descr as descricao
		,descr as descr
		,CONVERT(varchar, ousrdata, 102) as dateins
		,CONVERT(varchar, validade, 102) as validadedoc
		,anexosstamp 
		,protected
		,ousrinis
		,tabela
		,regstamp
		,path	
	from 
		anexos 
	where 
		regstamp=@stamp
		AND tabela=@tabela
		and ousrinis = (case when protected=1 then @op else ousrinis end)
		and UPPER(ltrim(rtrim(tipo)))!='DIC' 
	 
	


GO
Grant Execute on dbo.up_anexos_pesquisaAnexos to Public
Grant Control on dbo.up_anexos_pesquisaAnexos to Public
GO