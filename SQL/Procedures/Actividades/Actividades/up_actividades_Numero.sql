-- exec up_actividades_Numero 

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_actividades_Numero]') IS NOT NULL
	drop procedure up_actividades_Numero
go

create PROCEDURE up_actividades_Numero

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

	Select	
		numero = ISNULL(MAX(no),0) +1 
	from	
		b_actividades (nolock)

		
GO
Grant Execute On up_actividades_Numero to Public
grant control on up_actividades_Numero to Public
go
