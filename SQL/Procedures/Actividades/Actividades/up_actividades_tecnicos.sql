/* exec up_actividades_tecnicos '' */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_actividades_tecnicos]') IS NOT NULL
    drop procedure up_actividades_tecnicos
go

create PROCEDURE up_actividades_tecnicos
@actstamp varchar(25)


/* WITH ENCRYPTION */
AS

	select 
		 b_us.nome
		 ,b_us.userno
		 ,b_us.usstamp
		 ,b_actividadesUs.duracao
		 ,b_actividadesUs.actusstamp
	from 
		b_Actividades
		inner join b_actividadesUs on b_Actividades.actstamp = b_actividadesUs.actstamp
		inner join b_us on b_us.usstamp = b_actividadesUs.usstamp
	where
		b_Actividades.actstamp = @actstamp
	Order
		by nome, no
	
GO
Grant Execute On up_actividades_tecnicos to Public
Grant Control On up_actividades_tecnicos to Public
go