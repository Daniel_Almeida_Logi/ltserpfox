/* exec up_actividades_recursos '' */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_actividades_recursos]') IS NOT NULL
    drop procedure up_actividades_recursos
go

create PROCEDURE up_actividades_recursos
@actstamp varchar(25)


/* WITH ENCRYPTION */
AS

	select 
		 b_cli_recursos.nome
		 ,b_cli_recursos.no
		 ,b_actividadesRecurso.duracao
		 ,b_cli_recursos.recursosstamp
	from 
		b_Actividades
		inner join b_actividadesRecurso on b_Actividades.actstamp = b_actividadesRecurso.actstamp
		inner join b_cli_recursos on b_cli_recursos.recursosstamp = b_actividadesRecurso.recursosstamp
	where
		b_Actividades.actstamp = @actstamp
	Order
		by nome, no, duracao
	
GO
Grant Execute On up_actividades_recursos to Public
Grant Control On up_actividades_recursos to Public
go