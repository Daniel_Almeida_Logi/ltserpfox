/* exec up_actividades_servicos '' */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_actividades_servicos]') IS NOT NULL
    drop procedure up_actividades_servicos
go

create PROCEDURE up_actividades_servicos
@actstamp varchar(25)


/* WITH ENCRYPTION */
AS

	select 
		 st.ref
		 ,st.design
		 ,b_actividadesSt.qtt
		 ,b_actividadesSt.duracao
	from 
		b_Actividades
		inner join b_actividadesSt on b_Actividades.actstamp = b_actividadesSt.actstamp
		inner join st on st.ref = b_actividadesSt.ref
	where
		b_Actividades.actstamp = @actstamp
	Order
		by nome, no
	
GO
Grant Execute On up_actividades_servicos to Public
Grant Control On up_actividades_servicos to Public
go