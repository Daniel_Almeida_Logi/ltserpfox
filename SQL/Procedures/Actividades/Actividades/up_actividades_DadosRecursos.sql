/* exec up_actividades_DadosRecursos 'nome' */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_actividades_DadosRecursos]') IS NOT NULL
    drop procedure up_actividades_DadosRecursos
go

create PROCEDURE up_actividades_DadosRecursos
@nome varchar(254)

/* WITH ENCRYPTION */
AS

	select 
		 b_cli_recursos.nome
		 ,b_cli_recursos.no
		 ,duracao = 0
		 ,b_cli_recursos.recursosstamp
	from 
		b_cli_recursos (nolock)
	Where
		nome = @nome
		
GO
Grant Execute On up_actividades_DadosRecursos to Public
Grant Control On up_actividades_DadosRecursos to Public
go