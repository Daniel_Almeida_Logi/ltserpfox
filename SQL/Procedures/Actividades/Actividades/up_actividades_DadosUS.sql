/* exec up_actividades_DadosUS 'nome' */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_actividades_DadosUS]') IS NOT NULL
    drop procedure up_actividades_DadosUS
go

create PROCEDURE up_actividades_DadosUS
@nome varchar(254)

/* WITH ENCRYPTION */
AS

	select 
		 nome
		 ,userno
		 ,duracao = 0
		 ,usstamp
	from 
		b_us (nolock)
	Where
		nome = @nome
		
GO
Grant Execute On up_actividades_DadosUS to Public
Grant Control On up_actividades_DadosUS to Public
go