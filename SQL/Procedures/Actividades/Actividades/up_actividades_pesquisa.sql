-- Lista actividades pesquisa
-- exec up_actividades_pesquisa ''

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_actividades_pesquisa]') IS NOT NULL
	drop procedure up_actividades_pesquisa
go

create PROCEDURE up_actividades_pesquisa
@stamp as varchar(25)

/* WITH ENCRYPTION */

AS 
BEGIN

	Select 
		*
	from 
		B_actividades (nolock) 
	where
		actstamp = @stamp

END

GO
Grant Execute On up_actividades_pesquisa to Public
GO
Grant Control On up_actividades_pesquisa to Public
GO