/* exec up_actividades_DadosSt 'nome' */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_actividades_DadosSt]') IS NOT NULL
    drop procedure up_actividades_DadosSt
go

create PROCEDURE up_actividades_DadosSt
@ref varchar(18)
,@site_nr tinyint

/* WITH ENCRYPTION */
AS

	select 
		 ref
		 ,design
		 ,duracao = u_duracao
		 ,epv1
	from 
		st (nolock)
	Where
		st.ref = @ref
		and st.site_nr = @site_nr
GO
Grant Execute On up_actividades_DadosSt to Public
Grant Control On up_actividades_DadosSt to Public
go