/* exec up_actividades_ListaTecnicos '' */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_actividades_ListaTecnicos]') IS NOT NULL
    drop procedure up_actividades_ListaTecnicos
go

create PROCEDURE up_actividades_ListaTecnicos
@actstamp varchar(25)

/* WITH ENCRYPTION */
AS

	select 
		 b_us.nome
		/* ,userno
		 ,duracao = 0
		 ,b_us.usstamp*/
	from 
		b_us (nolock)
	Order by 
		nome, userno
	
GO
Grant Execute On up_actividades_ListaTecnicos to Public
Grant Control On up_actividades_ListaTecnicos to Public
go