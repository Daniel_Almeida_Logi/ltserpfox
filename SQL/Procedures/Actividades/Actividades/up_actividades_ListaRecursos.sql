/* exec up_actividades_ListaRecursos '' */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_actividades_ListaRecursos]') IS NOT NULL
    drop procedure up_actividades_ListaRecursos
go

create PROCEDURE up_actividades_ListaRecursos
@actstamp varchar(25)

/* WITH ENCRYPTION */
AS

	select 
		 b_cli_recursos.nome
		/* ,b_cli_recursos.no
		 ,duracao = 0
		 ,b_cli_recursos.recursosstamp*/
	from 
		b_cli_recursos (nolock)
	Where
		recursosstamp not in (
			Select 
				recursosstamp
			From
				b_actividadesRecurso
			Where
				actstamp = @actstamp
		)	
	Order by 
		nome
	
GO
Grant Execute On up_actividades_ListaRecursos to Public
Grant Control On up_actividades_ListaRecursos to Public
go