/* exec up_actividades_ListaServicos */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_actividades_ListaServicos]') IS NOT NULL
    drop procedure up_actividades_ListaServicos
go

create PROCEDURE up_actividades_ListaServicos
@site_nr tinyint

/* WITH ENCRYPTION */
AS

	select 
		 st.ref
		 ,st.design
		 ,duracao = u_duracao
	from 
		st (nolock)
	where
		st.site_nr = @site_nr 
	Order by 
		ref, design
	
GO
Grant Execute On up_actividades_ListaServicos to Public
Grant Control On up_actividades_ListaServicos to Public
go