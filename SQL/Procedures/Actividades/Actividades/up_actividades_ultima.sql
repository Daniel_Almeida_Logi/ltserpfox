-- Ultima actividade
-- exec up_actividades_ultima

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_actividades_ultima]') IS NOT NULL
	drop procedure up_actividades_ultima
go

create PROCEDURE up_actividades_ultima


/* WITH ENCRYPTION */

AS 
BEGIN

	Select 
		 top 1 *
	from 
		B_actividades (nolock) 
	Order by 
		usrdata desc
		,usrhora desc
		

END

GO
Grant Execute On up_actividades_ultima to Public
GO
Grant Control On up_actividades_ultima to Public
GO