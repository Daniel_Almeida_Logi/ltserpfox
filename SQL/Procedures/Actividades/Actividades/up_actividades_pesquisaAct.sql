/* exec up_actividades_pesquisaAct 't',0 */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_actividades_pesquisaAct]') IS NOT NULL
    drop procedure up_actividades_pesquisaAct
go

create PROCEDURE up_actividades_pesquisaAct
@nome varchar(254),
@no as numeric(9,2)

/* WITH ENCRYPTION */
AS

	select 
		sel = CONVERT(bit,0)
		,*
	from 
		b_Actividades (nolock)
	Where
		nome like @nome + '%'
		and no = case when @no = 0 then no else @no end
	
GO
Grant Execute On up_actividades_pesquisaAct to Public
Grant Control On up_actividades_pesquisaAct to Public
go