/* 
	exec   up_save_PostOfficesResponse '','','',''
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_PostOfficesResponse]') IS NOT NULL
	drop procedure dbo.up_save_PostOfficesResponse
go

create procedure [dbo].up_save_PostOfficesResponse
	@token		VARCHAR(36),
	@id_cl		VARCHAR(36),
	@response	text,
	@send		bit 
AS

	IF  EXISTS (select * from postOffice where token=@token and id_cl=@id_cl) 
	BEGIN 
		UPDATE postOffice 
		SET
			response = @response ,
			send=@send
		FROM	
			postOffice (nolock)
		where token=@token and id_cl=@id_cl
	END

GO
Grant Execute On [dbo].[up_save_PostOfficesResponse] to Public
Grant control On [dbo].[up_save_PostOfficesResponse]to Public
GO	