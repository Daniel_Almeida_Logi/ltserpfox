/* inserir informação de base de dados 
	

	exec up_insert_connectionDB 
exec up_insert_connectionDB 'DSADSADS','SQLLOGITOOLS',1433,'LTDEV30','TESTE','ADM'

*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_insert_connectionDB]') IS NOT NULL
	drop procedure dbo.up_insert_connectionDB
go

create procedure [dbo].[up_insert_connectionDB]
	 @token								AS VARCHAR(36),
	 @server							AS VARCHAR(36),
	 @port								AS int,	
	 @nameDB							AS VARCHAR(36),
	 @appName							AS VARCHAR(100),
	 @inis								AS VARCHAR(100),
	 @Dbname							AS VARCHAR(100)

/* with encryption */
AS
SET NOCOUNT ON
BEGIN

	DELETE connectionDBS WHERE usrdata <= DATEADD(DAY, -5, GETDATE()) 

	INSERT INTO connectionDBS(stamp,token,[server],[port],[name],[appName],[ousrdata],[ousrinis],[usrdata],[usrinis],[dbname])
	values(LEFT(NEWID(),36),@token,@server,@port,@nameDB,@appName,GETDATE(),@inis,GETDATE(),@inis,@Dbname)

END 
Grant Execute on dbo.up_insert_connectionDB to Public
Grant control on dbo.up_insert_connectionDB to Public
GO