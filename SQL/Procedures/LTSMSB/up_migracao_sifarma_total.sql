
/* 
	exec up_migracao_sifarma_total
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_migracao_sifarma_total]') IS NOT NULL
	drop procedure dbo.up_migracao_sifarma_total
go

Create procedure dbo.up_migracao_sifarma_total

/* with encryption */
AS
SET NOCOUNT ON
BEGIN

	/**********************************************************************
	TRIGERS DISABLE
	**********************************************************************/
	   EXEC('ALTER TABLE bi DISABLE TRIGGER tr_bi_delete'); 
		EXEC('ALTER TABLE bi DISABLE TRIGGER tr_bi_insert'); 
		EXEC('ALTER TABLE bi DISABLE TRIGGER tr_bi_update'); 

		EXEC('ALTER TABLE fi DISABLE TRIGGER tr_fi_delete'); 
		EXEC('ALTER TABLE fi DISABLE TRIGGER tr_fi_insert'); 
		EXEC('ALTER TABLE fi DISABLE TRIGGER tr_fi_update'); 

		EXEC('ALTER TABLE sl DISABLE TRIGGER tr_sl_delete'); 
		EXEC('ALTER TABLE sl DISABLE TRIGGER tr_sl_insert'); 
		EXEC('ALTER TABLE sl DISABLE TRIGGER tr_sl_update'); 
	/**********************************************************************
	TRIGERS DISABLE
	**********************************************************************/

	/**********************************************************************
	Definir vari�veis Gerais
**********************************************************************/

	/* Data e Hora */
	declare @data nvarchar(8), @hora as nvarchar(8), @horaInv as nvarchar(8)
	set @data			= '20220824'
	set @hora			= '23:58:59'
	set @horaInv		= '23:59:59'

	/* Identifica��o do Documento que Guarda o Historico de Entradas */
	declare @ndoc_e numeric(3), @nmdoc_e nvarchar(20)
	set @ndoc_e			= 102
	set @nmdoc_e		= (select cmdesc from cm1 where cm=@ndoc_e)

	/* Identifica��o do Documento que Guarda o Historico de Saidas */
	declare @ndoc_s numeric(3), @nmdoc_s nvarchar(20), @tipodoc_s numeric(1)
	set @ndoc_s			= 80--80,213,313
	set @nmdoc_s		= (select nmdoc from td where ndoc=@ndoc_s)
	set @tipodoc_s		= (select tipodoc from td where ndoc=@ndoc_s)

	/* Identifica��o do Documento que Guarda Vendas a Credito */
	declare @ndoc_vc numeric(3), @nmdoc_vc nvarchar(20), @tipodoc_vc numeric(1)
	set @ndoc_vc		= 88--88,216,316
	set @nmdoc_vc		= (select nmdoc from td where ndoc=@ndoc_vc)
	set @tipodoc_vc		= (select tipodoc from td where ndoc=@ndoc_vc)

	/* Identifica��o do Documento que Guarda Vendas Suspensas a Dinheiro */
	declare @ndoc_vd numeric(3), @nmdoc_vd nvarchar(20), @tipodoc_vd numeric(1)
	set @ndoc_vd		= 87--87,215,315	
	set @nmdoc_vd		= (select nmdoc from td where ndoc=@ndoc_vd)
	set @tipodoc_vd		= (select tipodoc from td where ndoc=@ndoc_vd)

	/* Identifica��o do Documento que Guarda Devolu��es Pendentes */
	declare @ndoc_dp numeric(3), @nmdoc_dp nvarchar(20)--, @tipodoc_dp numeric(1)
	set @ndoc_dp		= 17
	set @nmdoc_dp		= (select nmdos from ts where ndos=@ndoc_dp)

		/* Identifica��o da Loja */
	declare @loja nvarchar(20), @site_nr int, @armazem numeric(1), @terminalNome varchar(20), @terminal numeric(5)
	set @loja		= 'Loja 1' --Loja 1, Loja 2, Loja 3
	set @site_nr	= 1
	set @armazem	= 1--1,2,3
	set @terminal	= 99
	set @terminalNome = 'Terminal 99'

		/*  */
	declare @cropVd numeric(5), @incrementaCli int, @incrementaForn int, @incrementaDevForn int, @invNome varchar(60)
	set @cropVd = 10
	set @incrementaCli = 200
	set @incrementaForn = 0
	set @incrementaDevForn = 0
	set @invNome = 'Invent�rio Migra��o SIF2000 - ' + @loja

/**********************************************************************
	Fim - Definir vari�veis Gerais
**********************************************************************/


	/*********************************************************************/
	-- 1 - PROCESSO DE VALIDACAO DE DADOS 
	/*********************************************************************/
	/*********************************************************************/
	-- Remover Productos repetidos
	/*********************************************************************/
BEGIN TRY 
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempProd'))
			DROP TABLE #tempProd

		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempProdAnulado'))
			DROP TABLE #tempProdAnulado


				select *
					into #tempProd
				from sif2k_prod 
	
				where
					cpr in (select cpr from sif2k_prod group by cpr having count(cpr)>1)
					and stm=0 

				-- compara com anulados ou apagados
				select *
					into #tempProdAnulado
				from sif2k_prod 
	
				where
					cpr in (select cpr from sif2k_prod group by cpr having count(cpr)>1)
					and stm=0 and (ANULADO = 'S' or APAGADO = 'S')
	
				delete
				 sif2k_prod
				from (
						select sp.ANULADO,sp.apagado,sp.cpr from sif2k_prod sp
						inner join #tempProdAnulado on #tempProdAnulado.cpr = sp.cpr
						where #tempProdAnulado.ANULADO = sp.anulado and #tempProdAnulado.APAGADO=sp.apagado
				) x
				where sif2k_prod.ANULADO = x.anulado and sif2k_prod.APAGADO=x.apagado and sif2k_prod.cpr=x.cpr
			

		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempProd'))
			DROP TABLE #tempProd

		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempProdAnulado'))
			DROP TABLE #tempProdAnulado;

	/* remove productos repetidos */
			WITH cte AS (
				SELECT 
					[CPR],
					ROW_NUMBER() OVER (
						PARTITION BY 
					[CPR]
					  ,[NOM]
					  ,[FAP]
					  ,[SAC]
					  ,[STM]
					  ,[SMI]
					  ,[PVP]
					  ,[PCU]
					  ,[IVA]
					  ,[GPR]
					  ,[CLA]
					  ,[DTVAL]
					  ,[PRATELEIRA]
					  ,[GAM]
					  ,[GAMA]
					  ,[APAGADO]
					  ,[ANULADO]
			
						ORDER BY 
							[CPR]
						  ,[NOM]
						  ,[FAP]
						  ,[SAC]
						  ,[STM]
						  ,[SMI]
						  ,[PVP]
						  ,[PCU]
						  ,[IVA]
						  ,[GPR]
						  ,[CLA]
						  ,[DTVAL]
						  ,[PRATELEIRA]
						  ,[GAM]
						  ,[GAMA]
						  ,[APAGADO]
						  ,[ANULADO]
						
							) row_num
				 FROM 
					sif2k_prod
				 where
				 cpr in (select cpr from sif2k_prod group by cpr having count(cpr)>1)
			)
			delete  from  cte
			WHERE row_num > 1;
			
			/* fim productos repetidos */
			
			/* remove productos repetidos com stocks diferente*/
			
			WITH cte AS (
				SELECT 
					*,
					ROW_NUMBER() OVER (
						PARTITION BY 
						[CPR]
						ORDER BY 	
						  [SAC]
						 DESC
							) row_num
				 FROM 
					sif2k_prod
				 where
				 cpr in (select cpr from sif2k_prod group by cpr having count(cpr)>1)
			)
			delete from cte
			where row_num>1

		-- FIM REMOVER PRODUCTOS REPETIDOS
	
	/*************************************************************************
		CORRIGIR NOME DE UTILIZADOR
	*************************************************************************/
		update sif2k_vdsusp
		set UTILIZADOR = 'Sif 2000'
		--select * from sif2k_vdsusp
		where NUM_VENDA in (
				select num_venda--, count(distinct isnull(utilizador,'x'))
				from (
				select vd.NUM_VENDA, vd.UTILIZADOR
				from sif2k_vdsusp vd
				--where UTILIZADOR is not null and UTILIZADOR!=''
				--order by vd.NUM_VENDA, vd.UTILIZADOR
				)x
				group by NUM_VENDA
				having  count(distinct isnull(utilizador,'x'))>1
			)
		
		
		update sif2k_vendas
		set UTILIZADOR = 'Sif 2000'
		--select * from sif2k_vdsusp
		where NUM_VENDA in (
				select num_venda--, count(distinct isnull(utilizador,'x'))
				from (
				select vd.NUM_VENDA, vd.UTILIZADOR
				from sif2k_vendas vd
				--where UTILIZADOR is not null and UTILIZADOR!=''
				--order by vd.NUM_VENDA, vd.UTILIZADOR
				)x
				group by NUM_VENDA
				having  count(distinct isnull(utilizador,'x'))>1
			)

		update sif2k_vdnormal
		set UTILIZADOR = 'Sif 2000'
		-- select * from sif2k_vdnormal
		where NUM_VENDA in (
				select num_venda--, count(distinct utilizador)
				from (
				select vd.NUM_VENDA, vd.UTILIZADOR
				from sif2k_vdnormal vd
				--where UTILIZADOR is not null and UTILIZADOR!=''
				--order by vd.NUM_VENDA, vd.UTILIZADOR
				)x
				group by NUM_VENDA
				having  count(distinct isnull(utilizador,'x'))>1
			)

	/* APAGA REFS REPETIDAS NO CODIGOS ALTERNATIVOS */	
	delete from sif2k_prod_alternativo
	where cpr = cod

   /* APAGA CODIGO ALTERANATIVOS DE 7 DIGITOS:  */
	
	delete from sif2k_prod_alternativo 
	where len(cod) = 7 or cod is null or cod like '%+%'

	/*PLICAS*/

	/* PLICAS NOS NOMES E OBSERVA��ES DE CLIENTES -> FAZER REPLACE */
	update sif2k_cli
	set
		nome = replace(nome,char(39),' ')
		,morada = replace(morada,char(39),' ')
		,obervacao = replace(obervacao,char(39),' ')
	where
		nome like '%'+char(39)+'%'
		or morada like '%'+char(39)+'%'
		or obervacao like '%'+char(39)+'%'

	/* PLICAS NOS NOMES DE FORNECEDORES -> FAZER REPLACE */
	update sif2k_forn
	set
		nome = replace(nome,char(39),' ')
		,morada = replace(morada,char(39),' ')
		,local = replace(local,char(39),' ')
	where
		nome like '%'+char(39)+'%'
		or morada like '%'+char(39)+'%'
		or local like '%'+char(39)+'%'

	/* PLICAS NOS NOMES DE PRODUTOS -> FAZER REPLACE */
	update
		sif2k_prod
	set
		nom = replace(nom,char(39),' ')
		,fap = replace(fap,char(39),' ')
		,prateleira = replace(prateleira,char(39),' ')
	where
		nom like '%'+char(39)+'%'
		or fap like '%'+char(39)+'%'
		or prateleira like '%'+char(39)+'%'
	
	/* PLICAS NOS NOMES DE PRODUTOS (HISTORICO CONSUMO)-> FAZER REPLACE */
	update
		sif2k_histConsumo
	set
		DESIGNACAO = replace(DESIGNACAO,char(39),' ')
	where
		DESIGNACAO like '%'+char(39)+'%'

	update
		infoprex
	set
		fpd = replace(fpd,char(39),' ')

	delete from infoprex where  cpr is null	

	update sif2k_cli set num_cliente = convert(varchar,convert(int, num_cliente))
	
	/* PRODUTOS COM VALIDADES INVALIDAS QUE VEM DO SIFARMA! */
	update
		sif2k_prod
	set
		dtval = '09-2009'
	where
		cpr = 9570838

		/*
		VIRGULAS POR PONTOS NOS VALORES NUMERICOS
		select * from sif2k_prod order by pvp
		select convert(numeric(13,3),pvp), * from sif2k_prod
	*/
	update sif2k_cli
	set limite_credito	= REPLACE(limite_credito,',','.')
	where limite_credito like '%'+char(44)+'%'
	
	/*
		CORRE��O DE BIS COM ERRO != 8 DIGITOS
	*/	
	update sif2k_cli set bi='' 
	where bi is not null and len(LTRIM(LTRIM(bi)))<7 or  len(LTRIM(LTRIM(bi)))>8
	
	update sif2k_cli set bi='0' + bi
	where bi is not null and len(LTRIM(LTRIM(bi)))=7 

	--CORRIGIR VALORES DE MOEDA
	update sif2k_prod set pvp = '0' where pvp is null
	update sif2k_prod set pcu = '0' where pcu  is null
	update sif2k_prod set pvp = replace(pvp,',','.'), pcu = replace(pcu,',','.')
	update sif2k_vdsusp set preco=replace(preco,',','.'),  total=replace(total,',','.'),  pago=replace(pago,',','.'), sum_total=replace(sum_total,',','.'),  sum_pago=replace(sum_pago,',','.'), creditado=replace(creditado,',','.'), iva=replace(iva,',','.')
	update sif2k_vendas set preco=replace(preco,',','.'),  total=replace(total,',','.'),  pago=replace(pago,',','.'), sum_total=replace(sum_total,',','.'),  sum_pago=replace(sum_pago,',','.'), creditado=replace(creditado,',','.'), iva=replace(iva,',','.')
	update sif2k_vdnormal set preco=replace(preco,',','.'),  total=replace(total,',','.'),  creditado=replace(creditado,',','.'), debito=replace(debito,',','.'),  credito=replace(credito,',','.'), iva=replace(iva,',','.'), remanescente=replace(remanescente,',','.')
	update sif2k_vdnormal set iva=0 where iva is null
	update sif2k_vdnormal set qt=0 where qt is null
	update sif2k_vdnormal set total=0 where total is null
	update sif2k_devolucoes set preco=replace(preco,',','.')
	update sif2k_devolucoes set preco='0' where preco is null

	--APAGA DEVOLUCOES E VENDAS REGULARIZADA
	delete from sif2k_vendas where tipo='N'
	delete from sif2k_vendas where regular='Regularizada'
END TRY 
BEGIN CATCH
	PRINT("ERRO PROCESSO DE VALIDACAO DE DADOS ")
	SELECT
    ERROR_NUMBER() AS ErrorNumber,
    ERROR_STATE() AS ErrorState,
    ERROR_SEVERITY() AS ErrorSeverity,
    ERROR_PROCEDURE() AS ErrorProcedure,
    ERROR_LINE() AS ErrorLine,
    ERROR_MESSAGE() AS ErrorMessage;
	RETURN 
END CATCH
	/***************************************************************
	-- FIM PROCESSO DE VALIDACAO DE DADOS 
	***************************************************************/

	/**********************************************************************
	Migra��o de Clientes
	select * from sif2k_cli where num_cliente!=''
	select * from b_utentes order by no
**********************************************************************/

BEGIN TRY 
		
		delete b_utentes where no > 200

		insert into b_utentes
			(
			utstamp, no, nome, telefone, tlmvl, morada, sexo, tipo
			,ncont, moeda, radicaltipoemp, vencimento, pais
			,inactivo, ousrinis, ousrdata
			,email, bino, eplafond, nascimento, entfact
			,obs,autorizado,autoriza_sms,autoriza_emails, codpost, [local],nbenef
			)
		
		select
			utstamp			= 'ADM' + left(replace(newid(),'-',''), 21)
			,no				= convert(int,sc.num_cliente) + @incrementaCli
			,nome			= isnull(left(sc.nome,55),'')
			,telefone		= ISNULL(left(sc.tel_residencia,13),'')
			,tlmvl			= isnull(left(sc.telemovel,13),'')
			,morada			= isnull(left(sc.morada,55),'')
			,sexo			= isnull(sc.sexo, 'M')
			,tipo			= 'Normal'
			/*,codpost		= sc.codpost
			,local			= sc.local
			,distrito		= left(cont.DISTRICT,20)*/ /*so existe este campo na versao 2013*/
			,ncont			= isnull(sc.nif,'')
			,moeda			= 'EURO'
			,radicaltipoemp	= 1
			,vencimento		= 30
			,pais			= 1
			/*,obs			= sc.obs*/
			,inactivo		=  case when  sit != 'A' then 1 else 0 end
			,ousrinis		= 'ADM'
			,ousrdata		= convert(varchar,getdate(), 112)
			,email			= isnull(left(sc.email,45),'')
			,bino			= isnull(left(sc.bi,20),'')
			,plafond		= ISNULL(limite_credito, 0)
			,nascimento		=  isnull(sc.dt_nasc,'19000101') 
			,entfact		= 1
			,isnull(obervacao,'')
			,autorizado		= case when (consent_3 is null or consent_3 = 'N') then 0 else 1 end
			,autoriza_sms	= case when (consent_3 is null or consent_3 = 'N') then 0 else 1 end
			,autoriza_emails= case when (consent_3 is null or consent_3 = 'N') then 0 else 1 end
			,codpost = case when codPost1 is not null and  codPost2 is not null then isnull(codPost1,'') + '-' + isnull(codPost2,'')   else '' end
			,localidade = isnull(localidade,'')
			,nbenef   = case when num_cartao_ute is not null then isnull(num_cartao_ute,'') else '' end
		from
			sif2k_cli sc
		where
			ltrim(rtrim(num_cliente)) !=''
			and num_cliente is not null
			and num_cliente != '0'
			and apagado != ''
			--	and email<>''
		order by
			convert(int, sc.num_cliente)
		
		/* 
			INSERT dos clientes SEM QUALQUER NUMERO
			DEPENDE DO INSERT ANTERIOR PARA LER O MAX(NO) PORQUE SOMA UM ROW_ID() AO ULTIMO REGISTO DA CL
			ESTES CLIS NAO PODEM SER MAPEADOS COM VENDAS; APENAS SAO IMPORTADOS POR CONVENIENCIA ou registo de consumos
		*/

			insert into b_utentes (
			utstamp,no,nome,email,telefone,tlmvl,bino,morada
			--,eplafond
			,nascimento
			,sexo,tipo,ncont,moeda,radicaltipoemp,vencimento,pais,entfact
			,ousrinis,ousrdata,ousrhora,autorizado,autoriza_sms,inactivo
			,codpost, [local],nbenef
		)
		select
			c.utstamp
			,no = (select max(no) from b_utentes) + c.no
			,nome = replace(c.nome,'%','')
			,c.email
			,c.telefone
			,c.tlmvl
			,c.bino
			,c.morada
			--,c.eplafond
			,c.nascimento
			,c.sexo
			,c.tipo
			,c.ncont
			,c.moeda
			,c.radicaltipoemp
			,c.vencimento
			,c.pais
			,c.entfact
			,c.ousrinis
			,c.ousrdata
			,c.ousrhora
			,c.autorizado
			,c.autoriza_sms
			,c.inactivo
			,c.codpost
			,c.codpost
			,c.nbenef
		from (
			select
				utstamp				= 'ADM' + left(replace(newid(),'-',''), 21)
				,no					= row_number() over (order by sif2k_cli.nome)
				,nome				= isnull(left(sif2k_cli.nome,55),'')
				,email				= isnull(left(sif2k_cli.email,45),'')
				,telefone			= ISNULL(left(tel_residencia,13),'')
				,tlmvl				= isnull(left(telemovel,13),'')
				,bino				= isnull(bi,'')
				,morada				= isnull(left(sif2k_cli.morada,55),'')
				--,eplafond			= ISNULL(case when limite_credito='' then '0' else limite_credito end,0)
				/*,nascimento		= isnull(convert(date,replace(dt_nasc,'.','-'),103),'1900-01-01')*/
				,nascimento			= isnull(dt_nasc,'19000101') 
				,sexo				= isnull(sexo,'M')
				,tipo				= 'Normal'
				,ncont				= isnull(nif,'')
				,moeda				= 'EURO'
				,radicaltipoemp		= 1
				,vencimento			= 30
				,pais				= 1
				,entfact			= 1
				,ousrinis			= 'ADM'
				,ousrdata			= isnull(dt_criacao,'19000101')
				,ousrhora			= '00:00:00'
				,autorizado		= case when (consent_3 is null or consent_3 = 'N') then 0 else 1 end
				,autoriza_sms	= case when (consent_3 is null or consent_3 = 'N') then 0 else 1 end
				,autoriza_emails= case when (consent_3 is null or consent_3 = 'N') then 0 else 1 end
				,inactivo		=  case when  sit != 'A' then 1 else 0 end
				,codpost = case when codPost1 is not null and  codPost2 is not null then isnull(codPost1,'') + '-' + isnull(codPost2,'')   else '' end
				,localidade = isnull(localidade,'')
				,nbenef   = case when num_cartao_ute is not null then isnull(num_cartao_ute,'') else '' end
			
			from
				sif2k_cli
			where
				num_cliente is null or num_cliente='' or num_cliente='0' 
		) c
		order by
			(select max(no) from b_utentes) + c.no

		/* actualiza os perfil comercial depois dos utentes criados*/
		update	
			b_utentes
		set
			tipo = isnull(left(ltrim(rtrim(nomePerfil)),20),tipo)
		from b_utentes(nolock)
		inner join sif2k_cli_perfil scp(nolock) on scp.num_cliente + @incrementaCli = b_utentes.no 
		where num_cliente is not null or num_cliente!=''


		/* Inserir um stamp qualquer para o botao ultimo cliente alterado funcionar */
		delete from b_ultreg where tabela='b_utentes'
		insert into b_ultreg 
			(tabela,stamp,data)
		select
			top 1 
			tabela	= 'b_utentes'
			,stamp	= utstamp
			,data	= getdate()
		from b_utentes
END TRY
BEGIN CATCH
	PRINT("ERRO PROCESSO DE Migra��o de Clientes ")
	SELECT
    ERROR_NUMBER() AS ErrorNumber,
    ERROR_STATE() AS ErrorState,
    ERROR_SEVERITY() AS ErrorSeverity,
    ERROR_PROCEDURE() AS ErrorProcedure,
    ERROR_LINE() AS ErrorLine,
    ERROR_MESSAGE() AS ErrorMessage;
	RETURN 
END CATCH
/**********************************************************************
	Fim - Migra��o de Clientes
**********************************************************************/

BEGIN TRY

/**********************************************************************
	Migra��o de Fornecedores
	select * from sif2k_forn order by no
**********************************************************************/
	delete from fl

		insert into fl (
			flstamp, no, nome, morada, local, ncont, telefone
			,u_ipport, moeda, pais, radicaltipoemp, pncont
			,ousrinis, ousrdata, ousrhora, inactivo
		)
		SELECT
			flstamp			= 'ADM' + left(replace(newid(),'-',''), 21)
			,no				= CONVERT(int,sf.no) + @incrementaForn
			,nome			= left(isnull(sf.nome,''),79)
			,morada			= left(isnull(sf.morada,''),54)
			,local			= left(isnull(sf.local,''),43)
			,ncont			= left(isnull(sf.ncont,''),20)
			,telefone		= isnull(sf.telefone,0)
			/*u_postver		= '1'*/
			,u_ipport		= '1707'
			,moeda			= 'EURO'
			,pais			= 1
			,radicaltipoemp	= 1
			,pncont			= 'PT'
			,ousrinis		= 'ADM'
			,ousrdata		= convert(date,getdate())
			,ousrhora		= convert(time,getdate())
			,inactivo	    = case when estado = 'INACTIVO'  then 1 else 0 end
			/*,tipo			= ''*/
		from
			sif2k_forn sf
		where
			sf.no is not null and apagado != 'S' and sf.no!='0' and ISNUMERIC(no) = 1
		order by
			convert(int,no)

		/* Inserir um stamp qualquer para o botao ultimo fornecedor alterado funcionar */
		delete from b_ultreg where tabela='fl'
		insert into b_ultreg
			(tabela,stamp,data)
		select
			top 1 
			tabela	= 'fl'
			,stamp	= flstamp
			,data	= getdate()
		from fl

		/* Caso n�o exista, Criar fornecedore Logitools	*/
		if (select nome from fl where nome='Logitools, Lda') is null
		begin
			insert into fl
				(
				flstamp, no, nome, morada, local, ncont, telefone, tlmvl, c1tele, email, codpost, url
				,moeda, pais, radicaltipoemp, pncont, ousrinis
				)
			SELECT
				flstamp			= 'ADM' + left(replace(newid(),'-',''), 21)
				,no				= isnull((select max(no) from fl),0) + 1
				,nome			= 'Logitools, Lda'
				,morada			= 'Edif�cio Scala - Rua do Vilar, 235, 6� D'
				,local			= 'Campo Alegre'
				,ncont			= '508935490'
				,telefone		= '222 451 554'
				,tlmvl			= '937 970 048'
				,c1tele			= '222 451 554'
				,email			= 'info@logitools.pt'
				,codpost		= '4050-626 Porto'
				,url			= 'www.logitools.pt'
				,moeda			= 'EURO'
				,pais			= 1
				,radicaltipoemp	= 1
				,pncont			= 'PT'
				,ousrinis		= 'ADM'
		end

END TRY
BEGIN CATCH
	PRINT("ERRO PROCESSO DE Migra��o de Fornecedores ")
	SELECT
    ERROR_NUMBER() AS ErrorNumber,
    ERROR_STATE() AS ErrorState,
    ERROR_SEVERITY() AS ErrorSeverity,
    ERROR_PROCEDURE() AS ErrorProcedure,
    ERROR_LINE() AS ErrorLine,
    ERROR_MESSAGE() AS ErrorMessage;
	RETURN 
END CATCH
/**********************************************************************
	Fim - Migra��o de Fornecedores
**********************************************************************/

BEGIN TRY

/**********************************************************************
	Migra��o de Artigos e Servi�os
	select * from sif2k_prod
	select * from st
**********************************************************************/
	
	delete from st where ousrdata >=GETDATE()-1 and site_nr= @site_nr
	delete from stic where descricao=@invNome
	delete from stil where armazem = @armazem
	delete from bc where site_nr = @site_nr

	insert into st (
		ststamp, ref, codigo, design, epcusto, epcpond, epcult, epv1, stns, tabiva
		,ivaincl, iva1incl, iva2incl, iva3incl, iva4incl, iva5incl
		,ptoenc, stmax, u_impetiq, u_tipoetiq, fornec, fornecedor
		,ousrinis, ousrdata, ousrhora, u_fonte, u_lab, familia, faminome
		,local, u_local2, validade
		,site_nr
	)
	select
		--valID,
		ststamp
		,ref
		,codigo = ref
		,design = ISNULL(design,'')
		,epcusto
		,epcult = epcusto
		,epcpond
		,epv1
		,stns
		,tabiva
		,ivaincl
		,iva1incl
		,iva2incl
		,iva3incl
		,iva4incl
		,iva5incl
		,ptoenc
		,stmax
		,u_impetiq
		,u_tipoetiq
		,fornec
		,fornecedor
		,ousrinis
		,ousrdata
		,ousrhora
		,u_fonte
		,u_lab
		,familia
		,faminome
		,local
		,u_local2
		,validade = case when validade < '20000101' then '1900-01-01' else validade end
		,site_nr = @site_nr
	from (
		select
			ststamp			= 'ADM' + convert(varchar,sif2k_prod.CPR) + '_' + ltrim(str(@site_nr))
			,ref			= convert(varchar,sif2k_prod.CPR)
							+ case
								when len(sif2k_prod.CPR)=6
								then dbo.uf_CheckDigit(sif2k_prod.CPR)
								else ''
							end
			,design			= left(ltrim(rtrim(replace(sif2k_prod.fap,char(39),''))),99)
				
			,epcusto		= case 
								-- 1� cenario: Produtos sem EPCUSTO, iva diferente de zero, pre�o diferente de zero
								when isnull(convert(numeric(13,3),pcu),0) = 0 and isnull(abs(convert(numeric(13,3),pvp)), 0) != 0 and taxasiva.codigo in (1, 2, 3)
									then convert(numeric(13,3), round(convert(numeric(13,3),convert(numeric(13,3),pvp)) / (taxasiva.taxa/100+1),2))
									
								---- 2� cenario: Produtos sem EPCUSTO, iva IGUAL a zero, pre�o diferente de zero. Avalia-se TAXA igual a 0% porque ha varias linhas na TAXASIVA para 0%
								when isnull(convert(numeric(13,3),pcu),0)=0 and isnull(abs(convert(numeric(13,3),pvp)),0)!=0 and taxasiva.taxa=0
									then convert(numeric(13,3), round(convert(numeric(13,3),pvp),2))
									
								-- 3� cenario: PCU original da farmacia
								else
									abs(isnull(convert(numeric(13,3),pcu),0))
							end
			,epcpond		= case 
								-- 1� cenario: Produtos sem pcp, iva diferente de zero, pre�o diferente de zero
								when isnull(convert(numeric(13,3),pcp),0) = 0 and isnull(abs(convert(numeric(13,3),pvp)), 0) != 0 and taxasiva.codigo in (1, 2, 3)
									then convert(numeric(13,3), round(convert(numeric(13,3),convert(numeric(13,3),pvp)) / (taxasiva.taxa/100+1),2))
									
								---- 2� cenario: Produtos sem pcp, iva IGUAL a zero, pre�o diferente de zero. Avalia-se TAXA igual a 0% porque ha varias linhas na TAXASIVA para 0%
								when isnull(convert(numeric(13,3),pcp),0)=0 and isnull(abs(convert(numeric(13,3),pvp)),0)!=0 and taxasiva.taxa=0
									then convert(numeric(13,3), round(convert(numeric(13,3),pvp),2))
									
								-- 3� cenario: pcp original da farmacia
								else
									abs(isnull(convert(numeric(13,3),pcp),0))
							end
			,epv1			= isnull(abs(convert(numeric(13,3),pvp)),0)
			,stns			= 0--case when isnull(sif2k_prod.PVP,0)<0 then 1 else 0 end
			
			,tabiva			= case when sif2k_prod.IVA='0' then 4 else isnull(taxasiva.codigo,2) end
			,ivaincl		= 1
			,iva1incl		= 1
			,iva2incl		= 1
			,iva3incl		= 1
			,iva4incl		= 1
			,iva5incl		= 1
			,ptoenc			= case when isnull(convert(int,sif2k_prod.STM),0)=1 and isnull(convert(int,sif2k_prod.SMI),0)=0 then 1 else isnull(convert(int,sif2k_prod.SMI),0) end
			,stmax			= isnull(convert(int,sif2k_prod.STM),0)
			,u_impetiq		= case when isnull(fprod.pvporig,0) = 0 then 0 else 1 end
			,u_tipoetiq		= 1
			,fornec			= isnull(sif2k_forn.no,0) + @incrementaForn
			,fornecedor		= isnull(sif2k_forn.nome,'')
			,ousrinis		= 'ADM'
			,ousrdata		= convert(datetime,getdate())
			,ousrhora		= convert(time,getdate(),102)
			,u_fonte		= case when fprod.cnp is not null then 'D' else 'I' end
			,u_lab			= isnull(b_labs.abrev,'')
			,familia		= isnull(stfami.ref,'99')
			,faminome		= isnull(stfami.nome,'Outros')
			,local			= left(isnull(prateleira,''),20)
			,u_local2		= isnull(gama,'')
			,validade		= DATEADD(DAY, -(DAY(DATEADD(MONTH, 1, CONVERT(date,replace(isnull(dtval,'01-1900'),'-','-01-'))))),DATEADD(MONTH, 1, CONVERT(date,replace(isnull(dtval,'01-1900'),'-','-01-'))))
			,valID			= row_number() over (partition by cpr 
												order by DATEADD(DAY, -(DAY(DATEADD(MONTH, 1, CONVERT(date,replace(isnull(dtval,'01-1900'),'-','-01-'))))),DATEADD(MONTH, 1, CONVERT(date,replace(isnull(dtval,'01-1900'),'-','-01-')))) desc, sac desc, stm desc, smi desc
												)
		from
			sif2k_prod
			left join taxasiva on convert(numeric(5,2),sif2k_prod.IVA) = taxasiva.taxa
			left join fprod on fprod.cnp = sif2k_prod.CPR
			left join b_labs on fprod.titaim = b_labs.id
			left join stfami on fprod.u_familia = stfami.ref
			left join sif2k_forn on sif2k_prod.forn_farmacia_id = sif2k_forn.idt
		--where
		--	isnull(sif2k_prod.encomendavel,'N') = 'S'
	) x
	where
		valID = 1
		
	order by
		ref


	/* Inserir c�digos alternativos */
	insert into bc
		(bcstamp, ref, codigo, design, ststamp, site_nr)
	select
		bcstamp		= left(replace(newid(),'-',''), 21)
		,ref		= cpr -- Codigo principal
		,codigo		= cod -- Codigo alternativo
		,design		= st.design
		,ststamp	= st.ststamp
		,site_nr	= @site_nr
	from (
		select
			cpr = convert(varchar,pa.cpr) + case when len(pa.cpr)=6 then dbo.uf_CheckDigit(pa.cpr) else '' end
			,pa.cod
		from
			sif2k_prod_alternativo pa
		where
			ltrim(rtrim(pa.cpr)) != ltrim(rtrim(pa.cod))
	) x
	inner join st (nolock) on st.ref=x.cpr and st.site_nr = @site_nr

	/* Inserir um stamp qualquer para o botao ultimo registo funcionar */
	delete from b_ultreg where tabela='st'
	insert into b_ultreg
		(tabela, stamp, data)
	select
		top 1
		tabela	= 'st'
		,stamp	= ststamp
		,data	= getdate()
	from
		st
	
	/* Alguns acertos */
	update
		st
	set
		u_fonte = 'D'
	where
		site_nr = @site_nr
		and ref in (select cnp from fprod)
		
			
	update st  set
		u_impetiq = case when isnull(fprod.pvporig,0) > 0 then 1 else 0 end
	from st
	left join fprod(nolock) on st.ref = fprod.ref


	/*marcas, labs e situa��es comerciais*/

	update 
		st 
	set 
		usr1=case when fprod.u_marca!='''' then fprod.u_marca else usr1 end, 
		u_lab=case when fprod.titaimdescr!='''' then fprod.titaimdescr else st.u_lab end
		,u_depstamp = case when fprod.id_grande_mercado_hmr is not null then  convert(varchar(4),fprod.id_grande_mercado_hmr) else u_depstamp end
		,u_secstamp = case when fprod.id_mercado_hmr is not null then convert(varchar(4),fprod.id_mercado_hmr) else u_secstamp end
		,u_catstamp = case when fprod.id_categoria_hmr is not null then convert(varchar(4),fprod.id_categoria_hmr) else u_catstamp end
		,u_segstamp = case when fprod.id_segmento_hmr is not null then convert(varchar(4),fprod.id_segmento_hmr)	else  u_segstamp end
	from 
		st (nolock) 
	inner join fprod (nolock) on fprod.cnp=st.ref 

		
	update
		st
	set 
			marg1 = ((st.epv1 / 
						(case
							when (select taxa from taxasiva where codigo=st.tabiva) != 0
							then (select taxa / 100 + 1 from taxasiva where codigo=st.tabiva)
							else 1
						end )
						/ st.epcusto) - 1) * 100
			,marg4 = ROUND((st.epv1 / (taxasiva.taxa/100+1)) - st.epcusto,2)
	from
		st (nolock)
		inner join taxasiva (nolock) on taxasiva.codigo=st.tabiva
	where
		site_nr = @site_nr
		and st.epv1 > 0 
		and st.epcusto > 0 
		and st.epv1 > st.epcusto
	
			
	/* Ver se ha pre�os acima de 9 mil euros */
	-- select ref,design,epv1,pv1 from st (nolock) where epv1>9000
	
	/* Correc��o de pre�os errados do sifarma (exageradamente altos) */
	update
		st
	set 
		epv1 = case 
				when epv1 = 9999.99 then 99.99
				when epv1 = 10094.990 then 94.99
				when epv1 = 10249.360 then 49.36
				when epv1 = 10164.990 then 64.99
				when epv1 = 10088.99 then 88.99
				else epv1
				end
	where 
		site_nr = @site_nr
		and epv1 > 9000

	/*
		Cria��o de Invent�rio F�sico

		Notas: Deve usado o mesmo criterio de valida��o das Datas e CNPs duplicados; ordenar pela data de interesse (mais antiga ou mais recente; default=mais antiga)
	*/

	insert into stic
		(sticstamp, data, hora, descricao, lanca, ousrinis, ousrdata, ousrhora)
	select
		sticstamp		= 'ADM' + left(replace(newid(),'-',''), 21)
		,data			= @data
		,hora			= left(@horaInv,5)
		,descricao		= @invNome
		,lanca			= 0
		,ousrinis		= 'ADM'
		,ousrdata		= @data
		,ousrhora		= @horaInv

	Insert into stil
		(stilstamp, ref, design, data, stock, sticstamp, armazem, ousrinis, ousrdata, ousrhora)
	select
		stilstamp, ref, design, data, stock, sticstamp, armazem, ousrinis, ousrdata, ousrhora
	from (
		Select
			stilstamp		= left(replace(newid(),'-',''), 21)
			,ref			= st.ref
			,design			= left(st.design,60)
			,data			= @data
			,stock			= CONVERT(int,sif2k_prod.SAC)
			,sticstamp		= (select top 1 sticstamp from stic where descricao = @invNome)
			,armazem		= @armazem
			,ousrinis		= 'ADM'
			,ousrdata		= @data
			,ousrhora		= @horaInv
			,valID			= row_number() over (partition by cpr 
												order by DATEADD(DAY, -(DAY(DATEADD(MONTH, 1, CONVERT(date,replace(isnull(dtval,'01-1900'),'-','-01-'))))),DATEADD(MONTH, 1, CONVERT(date,replace(isnull(dtval,'01-1900'),'-','-01-')))) desc, sac desc
												)
		From
			st (nolock)
			inner join sif2k_prod (nolock) on st.ref=sif2k_prod.CPR
		where
			stns = 0
	) x
	where
		valID=1
	order by
		ref
	
END TRY
BEGIN CATCH
	PRINT("ERRO PROCESSO DE Migra��o de Fornecedores ")
	SELECT
    ERROR_NUMBER() AS ErrorNumber,
    ERROR_STATE() AS ErrorState,
    ERROR_SEVERITY() AS ErrorSeverity,
    ERROR_PROCEDURE() AS ErrorProcedure,
    ERROR_LINE() AS ErrorLine,
    ERROR_MESSAGE() AS ErrorMessage;
	RETURN 
END CATCH
/**********************************************************************
	Fim - Migra��o de Artigos e Servi�os
**********************************************************************/




	/**********************************************************************
	TRIGERS ENABLE
	**********************************************************************/
	   EXEC('ALTER TABLE bi ENABLE TRIGGER tr_bi_delete'); 
		EXEC('ALTER TABLE bi ENABLE TRIGGER tr_bi_insert'); 
		EXEC('ALTER TABLE bi ENABLE TRIGGER tr_bi_update'); 

		EXEC('ALTER TABLE fi ENABLE TRIGGER tr_fi_delete'); 
		EXEC('ALTER TABLE fi ENABLE TRIGGER tr_fi_insert'); 
		EXEC('ALTER TABLE fi ENABLE TRIGGER tr_fi_update'); 

		EXEC('ALTER TABLE sl ENABLE TRIGGER tr_sl_delete'); 
		EXEC('ALTER TABLE sl ENABLE TRIGGER tr_sl_insert'); 
		EXEC('ALTER TABLE sl ENABLE TRIGGER tr_sl_update'); 
	/**********************************************************************
	TRIGERS ENABLE
	**********************************************************************/
END

Grant Execute on dbo.up_migracao_sifarma_total to Public
Grant Control on dbo.up_migracao_sifarma_total to Public
Go