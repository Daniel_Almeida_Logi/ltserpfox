SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'Distributors'))
BEGIN

	CREATE TABLE [dbo].[Distributors](
		[stamp]				[varchar](36) NOT NULL,
		[id]				[varchar](100)NOT NULL,
		[supplierName]		[varchar](100)NOT NULL,
		[supplierNo]		[varchar](60)NOT NULL,
		[ousrinis]			[VARCHAR](30) NOT NULL,
		[ousrdata]			DATETIME	  NOT NULL, 	   
		[usrinis]			[VARCHAR](30)     NOT NULL, 
		[usrdata]			DATETIME	NOT NULL,	

		
	 CONSTRAINT [PK_Distributors] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[Distributors] ADD  CONSTRAINT [DF_Distributors_id]  DEFAULT ('') FOR [id]
					 
	ALTER TABLE [dbo].[Distributors] ADD  CONSTRAINT [DF_Distributors_supplierName]  DEFAULT ('') FOR [supplierName]
					
	ALTER TABLE [dbo].[Distributors] ADD  CONSTRAINT [DF_Distributors_supplierNo]  DEFAULT ('') FOR [supplierNo]
					 
	ALTER TABLE [dbo].[Distributors] ADD  CONSTRAINT [DF_Distributors_ousrinis]  DEFAULT ('') FOR [ousrinis]
					 
	ALTER TABLE [dbo].[Distributors] ADD  CONSTRAINT [DF_Distributors_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]
	
	ALTER TABLE [dbo].[Distributors] ADD  CONSTRAINT [DF_Distributors_usrinis]  DEFAULT ('') FOR [usrinis]
					 
	ALTER TABLE [dbo].[Distributors] ADD  CONSTRAINT [DF_Distributors_usrdata]  DEFAULT (getdate()) FOR [usrdata]
END
