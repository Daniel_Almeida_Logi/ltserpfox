/* 
	exec   up_get_PostOfficesRequest '',''
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_PostOfficesRequest]') IS NOT NULL
	drop procedure dbo.up_get_PostOfficesRequest
go

create procedure [dbo].up_get_PostOfficesRequest
	@token VARCHAR(36),
	@id_cl VARCHAR(36)
AS

	SELECT
		request  as request,
		response as response,
		id_cl,
		token
	FROM	
		postOffice (nolock)
	where token=@token and id_cl=@id_cl

GO
Grant Execute On [dbo].[up_get_PostOfficesRequest] to Public
Grant control On [dbo].[up_get_PostOfficesRequest]to Public
GO	