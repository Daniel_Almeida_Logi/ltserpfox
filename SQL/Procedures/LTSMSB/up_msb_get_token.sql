/* 
	Devolve último token válido sms Pro
    EXEC up_msb_get_token
	select * from smsTokenInfo
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_msb_get_token]') IS NOT NULL
	drop procedure dbo.up_msb_get_token
go

create procedure [dbo].up_msb_get_token

AS
BEGIN
	declare @date1970 varchar(20)= '1970-01-01'
	declare @margemSegundos int = 1000 

	 SELECT TOP 1 
		access_token
	FROM smsTokenInfo (NOLOCK)
	WHERE
		DATEADD(SECOND, CAST(issued_at / 1000 AS BIGINT) + expires_in - @margemSegundos, @date1970) > GETDATE()
	ORDER BY issued_at DESC

END

Go
Grant Execute on dbo.up_msb_get_token to Public
Grant control on dbo.up_msb_get_token to Public
Go