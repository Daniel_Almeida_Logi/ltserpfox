
/* 
	Devolve informação de todos os clientes
	exec [up_getServicesToRun]   '2021-12-08T11:22:06.309'
*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_getServicesToRun]') IS NOT NULL
	drop procedure dbo.up_getServicesToRun
go

create procedure [dbo].up_getServicesToRun


@date  as datetime,
@key   as varchar(255) = ''


/* WITH ENCRYPTION */ 

AS
BEGIN
	



	select 
	   [stamp]
	  ,[user]
      ,[token]
      ,[url]
      ,[comType]
      ,[frequencyMinutes]
      ,[clientId]
      ,[clientDB]
      ,[serverDB]
      ,[site]
      ,[migrationType]
      ,[lastChangeDate]
      ,[lastRunDate]
      ,[obs]
      ,[multiSite]
      ,[folderPath]
      ,[password] =  ISNULL(CONVERT(varchar(20),DECRYPTBYPASSPHRASE(@key, password)),'')
      ,[sqlProcedures]
	  ,[fileType]
	  ,[port]
	  ,[nextRunDate] = convert(datetime,convert(date,DATEADD(mi, frequencyMinutes, lastRunDate))) + hourToRun
	  ,[zipFile]
	  ,[portDB]
	  ,[port]
	  ,[filenames]
	  ,[sqlCreationTable]
	  ,[migrationTables]
	  ,[fileNameToImport]
	  ,[shouldDelete]
	  ,[dadosValidos] 
	from 
		migrationCtrl(nolock)
	where 
		deleted = 0 and
		@date >=   convert(datetime,convert(date,DATEADD(mi, frequencyMinutes, lastRunDate)))  + hourToRun

	order by 
		 convert(datetime,convert(date,DATEADD(mi, frequencyMinutes, lastRunDate)))  + hourToRun
END



Go
Grant Execute on dbo.up_getServicesToRun to Public
Grant control on dbo.up_getServicesToRun to Public
Go




