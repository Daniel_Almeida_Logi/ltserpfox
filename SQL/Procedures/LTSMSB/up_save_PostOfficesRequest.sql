/* 
	exec   up_save_PostOfficesRequest '','','',''
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_PostOfficesRequest]') IS NOT NULL
	drop procedure dbo.up_save_PostOfficesRequest
go

create procedure [dbo].up_save_PostOfficesRequest
	@token		VARCHAR(36),
	@id_cl		VARCHAR(36),
	@request	text
AS

	Declare @bit bit 
	set @bit=0
	IF  NOT EXISTS (select * from postOffice where token=@token and id_cl=@id_cl) 
	BEGIN 
		INSERT INTO  postOffice (token,id_cl,request,ousrdata,ousrinis,usrdata,usrinis )
		VALUES (@token,@id_cl,@request,GETDATE(),'ADM',GETDATE(),'ADM')  
		set @bit =1;	
	END

	select @bit as resp

GO
Grant Execute On [dbo].[up_save_PostOfficesRequest] to Public
Grant control On [dbo].[up_save_PostOfficesRequest] to Public
GO	