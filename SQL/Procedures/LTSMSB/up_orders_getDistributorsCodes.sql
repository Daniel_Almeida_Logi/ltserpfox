

/*
	EXEC up_orders_getDistributorsCodes 'B2B'
	exec up_orders_getDistributorsCodes 'B2B'
*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_orders_getDistributorsCodes]') IS NOT NULL
drop procedure dbo.up_orders_getDistributorsCodes
go

create procedure up_orders_getDistributorsCodes
 @id		varchar(30)
/* WITH ENCRYPTION */
AS

	SELECT 
		id, 
		supplierName,
		supplierNo
	FROM Distributors
	WHERE id=@id

	
GO
Grant Execute On dbo.up_orders_getDistributorsCodes to Public
Grant Control On dbo.up_orders_getDistributorsCodes to Public
Go

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

