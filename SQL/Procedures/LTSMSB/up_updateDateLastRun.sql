
/* 
	Actualiza a data da ultima vez que correu
	exec [up_updateDateLastRun]   ''

*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_updateDateLastRun]') IS NOT NULL
	drop procedure dbo.up_updateDateLastRun
go

create procedure [dbo].up_updateDateLastRun


@stamp  as varchar(36)



/* WITH ENCRYPTION */ 
AS
BEGIN
	
	update 
		migrationCtrl 
	set 
		lastRunDate = getdate() 
	where
		stamp = rtrim(ltrim(@stamp))

END



Go
Grant Execute on dbo.up_updateDateLastRun to Public
Grant control on dbo.up_updateDateLastRun to Public
Go









