SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON


IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'postOffice'))
BEGIN

	CREATE TABLE [dbo].[postOffice](
		[token]						[varchar](36) NOT NULL,
		[id_cl]						[varchar](50) NOT NULL,
		[request]					[varchar](max),
		[response]					[varchar](max),
		[send]						[bit],
		[ousrdata]					[datetime],
		[ousrinis]					[varchar](30),
		[usrdata]					[datetime],
		[usrinis]					[varchar](30)
		
	 CONSTRAINT [PK_pfReq] PRIMARY KEY CLUSTERED 
	(
		[token] ASC,
		[id_cl]	 asc
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[postOffice] ADD  CONSTRAINT [DF_postOffice_token]  DEFAULT ('') FOR [token]

	ALTER TABLE [dbo].[postOffice] ADD  CONSTRAINT [DF_postOffice_id_cl]  DEFAULT ('') FOR [id_cl]

	ALTER TABLE [dbo].[postOffice] ADD  CONSTRAINT [DF_postOffice_request]  DEFAULT ('')  FOR [request]
	
	ALTER TABLE [dbo].[postOffice] ADD  CONSTRAINT [DF_postOffice_response]  DEFAULT ('') FOR [response]

	ALTER TABLE [dbo].[postOffice] ADD  CONSTRAINT [DF_postOffice_send]  DEFAULT (0) FOR [send]

	ALTER TABLE [dbo].[postOffice] ADD  CONSTRAINT [DF_postOffice_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[postOffice] ADD  CONSTRAINT [DF_postOffice_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[postOffice] ADD  CONSTRAINT [DF_postOffice_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[postOffice] ADD  CONSTRAINT [DF_postOffice_usrinis]  DEFAULT ('') FOR [usrinis]
END									

