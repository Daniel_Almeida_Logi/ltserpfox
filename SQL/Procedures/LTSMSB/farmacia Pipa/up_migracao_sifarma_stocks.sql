
/* 
	Migra��o das tabelas de stocks e movimentos do sifarma

	exec up_migracao_sifarma_stocks 'Loja 5'


	
	
*/






SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_migracao_sifarma_stocks]') IS NOT NULL
	drop procedure dbo.up_migracao_sifarma_stocks
go

create procedure [dbo].up_migracao_sifarma_stocks

		 @site	VARCHAR(36)

/* WITH ENCRYPTION */ 

AS
BEGIN

		--disable trigger
	 --   EXEC('ALTER TABLE bi DISABLE TRIGGER tr_bi_delete'); 
		--EXEC('ALTER TABLE bi DISABLE TRIGGER tr_bi_insert'); 
		--EXEC('ALTER TABLE bi DISABLE TRIGGER tr_bi_update'); 


		--EXEC('ALTER TABLE fi DISABLE TRIGGER tr_fi_delete'); 
		--EXEC('ALTER TABLE fi DISABLE TRIGGER tr_fi_insert'); 
		--EXEC('ALTER TABLE fi DISABLE TRIGGER tr_fi_update'); 


		
		--EXEC('ALTER TABLE sl DISABLE TRIGGER tr_sl_delete'); 
		--EXEC('ALTER TABLE sl DISABLE TRIGGER tr_sl_insert'); 
		--EXEC('ALTER TABLE sl DISABLE TRIGGER tr_sl_update'); 

			/* Identifica��o da Loja */
		declare @loja nvarchar(20), @site_nr int, @armazem numeric(1), @terminalNome varchar(20), @terminal numeric(5)
		set @loja		= @site
		set @site_nr	=  0
		set @armazem	= 0
		set @terminal	= 99
		set @terminalNome = 'Terminal 99'




		 
		/* Identifica��o do Documento de devolucao a fornecedor */
		declare @ndoc_df numeric(3), @nmdoc_df nvarchar(30), @monthLeft int
		set @ndoc_df   = 17  
		set @nmdoc_df  = (select nmdos from ts where ndos=@ndoc_df)

	
		/* Identifica��o do Documento de entrada de compra */
		declare @ndoc_vf numeric(3), @nmdoc_vf nvarchar(30) 
		set @ndoc_vf   = 55   -- V/Factura
		set @nmdoc_vf  = (select cmdesc from cm1 where cm=@ndoc_vf)


		set @monthLeft = -3





		/* Identifica��o do Documento de entrada de compra */
		declare @ndoc_s numeric(3), @nmdoc_s nvarchar(20), @tipodoc_s numeric(1)
		set @nmdoc_s		='Venda Manual' 
		set @ndoc_s			= (select ndoc from td where nmdoc=@nmdoc_s and site=@site)
		set @tipodoc_s		= (select tipodoc from td where ndoc=@ndoc_s)

		
		/* Obs */
		declare @obsFornecEntrada     varchar(40) = 'VFactura'+ "_" + @loja + "_" + convert(varchar,GETDATE())
		declare @obsFornecSaida       varchar(40) = 'Devolu��es'+ "_" + @loja + "_" + convert(varchar,GETDATE())
		declare @obsCli				  varchar(40) = 'Movimentos de clientes'
		declare @finalCli			  varchar(100) = 'Historico Movimentos Sa�das sif2000' + "_" + @loja + "_" + convert(varchar,GETDATE())


		/* Data e Hora */
		declare @data nvarchar(8), @hora as nvarchar(8)
		set @data			= GETDATE()
		set @hora			= CONVERT(VARCHAR(8), GETDATE(), 108) 


		select @site_nr = no from empresa(nolock) where site = @site
		select @armazem = armazem from empresa_arm(nolock) where empresa_no = @site_nr

		declare @inc int = 10000 * @site_nr




		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempProd'))
		DROP TABLE #tempProd

		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempProdAnulado'))
		DROP TABLE #tempProdAnulado

	
		
		-- guarda productos repetidos
			select *
				into #tempProd
			from sif2k_prod 
	
			where
				cpr in (select cpr from sif2k_prod group by cpr having count(cpr)>1)
				and stm=0 

			
			-- compara com anulados ou apagados
			select *
				into #tempProdAnulado
			from sif2k_prod 
	
			where
				cpr in (select cpr from sif2k_prod group by cpr having count(cpr)>1)
				and stm=0 and (ANULADO = 'S' or APAGADO = 'S')
			


			delete
			 sif2k_prod
			from (
					select sp.ANULADO,sp.apagado,sp.cpr from sif2k_prod sp
					inner join #tempProdAnulado on #tempProdAnulado.cpr = sp.cpr
					where #tempProdAnulado.ANULADO = sp.anulado and #tempProdAnulado.APAGADO=sp.apagado
			) x
			where sif2k_prod.ANULADO = x.anulado and sif2k_prod.APAGADO=x.apagado and sif2k_prod.cpr=x.cpr

			
		


		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempProd'))
		DROP TABLE #tempProd

		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempProdAnulado'))
		DROP TABLE #tempProdAnulado
		


		
		
	/* remove productos repetidos */
	
			;WITH cte AS (
				SELECT 
					[CPR],
					ROW_NUMBER() OVER (
						PARTITION BY 
					[CPR]
					  ,[NOM]
					  ,[FAP]
					  ,[SAC]
					  ,[STM]
					  ,[SMI]
					  ,[PVP]
					  ,[PCU]
					  ,[IVA]
					  ,[GPR]
					  ,[CLA]
					  ,[DTVAL]
					  ,[PRATELEIRA]
					  ,[GAM]
					  ,[GAMA]
					  ,[APAGADO]
					  ,[ANULADO]
			
						ORDER BY 
							[CPR]
						  ,[NOM]
						  ,[FAP]
						  ,[SAC]
						  ,[STM]
						  ,[SMI]
						  ,[PVP]
						  ,[PCU]
						  ,[IVA]
						  ,[GPR]
						  ,[CLA]
						  ,[DTVAL]
						  ,[PRATELEIRA]
						  ,[GAM]
						  ,[GAMA]
						  ,[APAGADO]
						  ,[ANULADO]
						
							) row_num
				 FROM 
					sif2k_prod
				 where
				 cpr in (select cpr from sif2k_prod group by cpr having count(cpr)>1)
			)
			delete  from  cte
			WHERE row_num > 1;
			
			/* fim productos repetidos */
			
			/* remove productos repetidos com stocks diferente*/
			
			;WITH cte AS (
				SELECT 
					*,
					ROW_NUMBER() OVER (
						PARTITION BY 
						[CPR]
					
			
						ORDER BY 
						  [STM]	
						  ,[SAC]
						 DESC

						
							) row_num
				 FROM 
					sif2k_prod
				 where
				 cpr in (select cpr from sif2k_prod group by cpr having count(cpr)>1)
			)
			delete from cte
			where row_num>1

		update
			sif2k_prod
		set
			nom = replace(nom,char(39),' ')
			,fap = replace(fap,char(39),' ')
			,prateleira = replace(prateleira,char(39),' ')
		where
			nom like '%'+char(39)+'%'
			or fap like '%'+char(39)+'%'
			or prateleira like '%'+char(39)+'%'

		update
			sif2k_prod
		set
			dtval = '09-2009'
		where
			cpr = 9570838


		update sif2k_prod set pvp = '0' where pvp is null
		update sif2k_prod set pcu = '0' where pcu  is null
		update sif2k_prod set pvp = replace(pvp,',','.'), pcu = replace(pcu,',','.')

		--fim validacoes stocks

		--validacoes movimentos
		-- fim movimentos

		--importar productos
		update sif2k_prod set  dtval = REPLACE(DTVAL, 'jan-', '01-20') where dtval like '%jan%'
		update sif2k_prod set  dtval = REPLACE(DTVAL, 'fev-', '02-20') where dtval like '%fev%'
		update sif2k_prod set  dtval = REPLACE(DTVAL, 'mar-', '03-20') where dtval like '%mar%'
		update sif2k_prod set  dtval = REPLACE(DTVAL, 'abr-', '04-20') where dtval like '%abr%'
		update sif2k_prod set  dtval = REPLACE(DTVAL, 'mai-', '05-20') where dtval like '%mai%'
		update sif2k_prod set  dtval = REPLACE(DTVAL, 'jun-', '06-20') where dtval like '%jun%'
		update sif2k_prod set  dtval = REPLACE(DTVAL, 'jul-', '07-20') where dtval like '%jul%'
		update sif2k_prod set  dtval = REPLACE(DTVAL, 'ago-', '08-20') where dtval like '%ago%'
		update sif2k_prod set  dtval = REPLACE(DTVAL, 'set-', '09-20') where dtval like '%set%'
		update sif2k_prod set  dtval = REPLACE(DTVAL, 'out-', '10-20') where dtval like '%out%'
		update sif2k_prod set  dtval = REPLACE(DTVAL, 'nov-', '11-20') where dtval like '%nov%'
		update sif2k_prod set  dtval = REPLACE(DTVAL, 'dez-',  '12-20') where dtval like '%dez%'


		delete from st where site_nr = @site_nr


		insert into st (
			ststamp, ref, codigo, design, epcusto, epcult,epcpond, epv1, stns, tabiva
			,ivaincl, iva1incl, iva2incl, iva3incl, iva4incl, iva5incl
			,ptoenc, stmax, u_impetiq, u_tipoetiq, fornec, fornecedor
			,ousrinis, ousrdata, ousrhora, u_fonte, u_lab, familia, faminome
			,local, u_local2, validade
			,site_nr,stock
		)
		select
			--valID,
			ststamp
			,ref
			,codigo = ref
			,design = ISNULL(design,'')
			,epcusto
			,epcult = epcusto
			,epcpond 
			,epv1
			,stns
			,tabiva
			,ivaincl
			,iva1incl
			,iva2incl
			,iva3incl
			,iva4incl
			,iva5incl
			,ptoenc
			,stmax
			,u_impetiq
			,u_tipoetiq
			,fornec
			,fornecedor
			,ousrinis
			,ousrdata
			,ousrhora
			,u_fonte
			,u_lab
			,familia
			,faminome
			,local
			,u_local2
			,validade = case when validade < '20000101' then '1900-01-01' else validade end
			,site_nr = @site_nr
			,stock
		from (
			select
				ststamp			= 'ADM' + convert(varchar,sif2k_prod.CPR) + '_' + ltrim(str(@site_nr))
				,ref			= convert(varchar,sif2k_prod.CPR)
								+ case
									when len(sif2k_prod.CPR)=6
									then dbo.uf_CheckDigit(sif2k_prod.CPR)
									else ''
								end
				,design			= left(ltrim(rtrim(replace(sif2k_prod.fap,char(39),''))),99)
				
				,epcusto		= case 
									-- 1� cenario: Produtos sem EPCUSTO, iva diferente de zero, pre�o diferente de zero
									when isnull(convert(numeric(13,3),pcu),0) = 0 and isnull(abs(convert(numeric(13,3),pvp)), 0) != 0 and taxasiva.codigo in (1, 2, 3)
										then convert(numeric(13,3), round(convert(numeric(13,3),convert(numeric(13,3),pvp)) / (taxasiva.taxa/100+1),2))
									
									---- 2� cenario: Produtos sem EPCUSTO, iva IGUAL a zero, pre�o diferente de zero. Avalia-se TAXA igual a 0% porque ha varias linhas na TAXASIVA para 0%
									when isnull(convert(numeric(13,3),pcu),0)=0 and isnull(abs(convert(numeric(13,3),pvp)),0)!=0 and taxasiva.taxa=0
										then convert(numeric(13,3), round(convert(numeric(13,3),pvp),2))
									
									-- 3� cenario: PCU original da farmacia
									else
										abs(isnull(convert(numeric(13,3),pcu),0))
								end
				,epcpond        = abs(isnull(convert(numeric(13,3),pcp),0))
				,epv1			= isnull(abs(convert(numeric(13,3),pvp)),0)
				,stns			= 0--case when isnull(sif2k_prod.PVP,0)<0 then 1 else 0 end 
				,tabiva			= case when sif2k_prod.IVA='0' then 4 else isnull(taxasiva.codigo,2) end
				,ivaincl		= 1
				,iva1incl		= 1
				,iva2incl		= 1
				,iva3incl		= 1
				,iva4incl		= 1
				,iva5incl		= 1
				,ptoenc			= case when isnull(convert(int,sif2k_prod.STM),0)=1 and isnull(convert(int,sif2k_prod.SMI),0)=0 then 1 else isnull(convert(int,sif2k_prod.SMI),0) end
				,stmax			= isnull(convert(int,sif2k_prod.STM),0)
				,u_impetiq		= case when isnull(fprod.pvporig,0) = 0 then 0 else 1 end
				,u_tipoetiq		= 1
				,fornec			= 0
				,fornecedor		= ''
				,ousrinis		= 'ADM'
				,ousrdata		= convert(datetime,getdate())
				,ousrhora		= convert(time,getdate(),102)
				,u_fonte		= case when fprod.cnp is not null then 'D' else 'I' end
				,u_lab			= isnull(b_labs.abrev,'')
				,familia		= isnull(stfami.ref,'99')
				,faminome		= isnull(stfami.nome,'Outros')
				,local			= left(isnull(prateleira,''),20)
				,u_local2		= isnull(gama,'')
				,validade		= DATEADD(DAY, -(DAY(DATEADD(MONTH, 1, CONVERT(date,replace(isnull(dtval,'01-1900'),'-','-01-'))))),DATEADD(MONTH, 1, CONVERT(date,replace(isnull(dtval,'01-1900'),'-','-01-'))))
				,valID			= row_number() over (partition by cpr 
													order by DATEADD(DAY, -(DAY(DATEADD(MONTH, 1, CONVERT(date,replace(isnull(dtval,'01-1900'),'-','-01-'))))),DATEADD(MONTH, 1, CONVERT(date,replace(isnull(dtval,'01-1900'),'-','-01-')))) desc, sac desc, stm desc, smi desc
													)
				,stock = CONVERT(int,sif2k_prod.SAC)
			from
				sif2k_prod
				left join taxasiva on convert(numeric(5,2),sif2k_prod.IVA) = taxasiva.taxa
				left join fprod on fprod.cnp = sif2k_prod.CPR
				left join b_labs on fprod.titaim = b_labs.id
				left join stfami on fprod.u_familia = stfami.ref

		) x
		where
			valID = 1
		order by
			ref
		

	
			/* Alguns acertos */
			update
				st
			set
				u_fonte = 'D'
			where
				site_nr = @site_nr
				and ref in (select cnp from fprod)
		
			
			update st  set
				u_impetiq = case when isnull(fprod.pvporig,0) > 0 then 1 else 0 end
			from st
			left join fprod(nolock) on st.ref = fprod.ref
			where st.site_nr = @site_nr


		/*marcas, labs e situa��es comerciais*/

		update 
			st 
		set 
			usr1=case when fprod.u_marca!='''' then fprod.u_marca else usr1 end, 
			u_lab=case when fprod.titaimdescr!='''' then fprod.titaimdescr else st.u_lab end
			,u_depstamp = case when fprod.id_grande_mercado_hmr is not null then  convert(varchar(4),fprod.id_grande_mercado_hmr) else u_depstamp end
			,u_secstamp = case when fprod.id_mercado_hmr is not null then convert(varchar(4),fprod.id_mercado_hmr) else u_secstamp end
			,u_catstamp = case when fprod.id_categoria_hmr is not null then convert(varchar(4),fprod.id_categoria_hmr) else u_catstamp end
			,u_segstamp = case when fprod.id_segmento_hmr is not null then convert(varchar(4),fprod.id_segmento_hmr)	else  u_segstamp end
		from 
			st (nolock) 
		inner join fprod (nolock) on fprod.cnp=st.ref 
		where st.site_nr = @site_nr

		
		update
			st
		set 
				marg1 = ((st.epv1 / 
							(case
								when (select taxa from taxasiva where codigo=st.tabiva) != 0
								then (select taxa / 100 + 1 from taxasiva where codigo=st.tabiva)
								else 1
							end )
							/ st.epcusto) - 1) * 100
				,marg4 = ROUND((st.epv1 / (taxasiva.taxa/100+1)) - st.epcusto,2)
		from
			st (nolock)
			inner join taxasiva (nolock) on taxasiva.codigo=st.tabiva
		where
			site_nr = @site_nr
			and st.epv1 > 0 
			and st.epcusto > 0 
			and st.epv1 > st.epcusto
	

	

		
				
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempProd'))
		DROP TABLE #tempProd

		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempProdAnulado'))
		DROP TABLE #tempProdAnulado


	/* Caso n�o exista, Criar fornecedore Logitools	*/
		if (select nome from fl where nome='Logitools, Lda') is null
		begin
			insert into fl
				(
				flstamp, no, nome, morada, local, ncont, telefone, tlmvl, c1tele, email, codpost, url
				,moeda, pais, radicaltipoemp, pncont, ousrinis
				)
			SELECT
				flstamp			= 'ADM' + left(replace(newid(),'-',''), 21)
				,no				= isnull((select max(no) from fl),0) + 1
				,nome			= 'Logitools, Lda'
				,morada			= 'Edif�cio Scala - Rua do Vilar, 235, 6� D'
				,local			= 'Campo Alegre'
				,ncont			= '508935490'
				,telefone		= '222 451 554'
				,tlmvl			= '937 970 048'
				,c1tele			= '222 451 554'
				,email			= 'info@logitools.pt'
				,codpost		= '4050-626 Porto'
				,url			= 'www.logitools.pt'
				,moeda			= 'EURO'
				,pais			= 1
				,radicaltipoemp	= 1
				,pncont			= 'PT'
				,ousrinis		= 'ADM'
		end
		
		declare @idFornecedor int
		select @idFornecedor=no from fl(nolock) where nome='Logitools, Lda'
	
		print ''
		print '-------------------------------'
		print 'Fornecedores Migrados'
		print '-------------------------------'
		print ''
	


	delete from bo where dataobra >= DATEADD(month, @monthLeft, GETDATE()) and site = @loja and ndos in (@ndoc_df)
	delete bo2 where bo2stamp not in (select bostamp from bo)
	delete bi where bostamp not in (select bostamp from bo)
	delete bi2 where bostamp not in (select bostamp from bo)



	delete from ft where fdata >= DATEADD(month, @monthLeft, GETDATE()) and site = @loja and ndoc=@ndoc_s
	delete ft2 where ft2stamp not in (select ftstamp from ft)
	delete fi where ftstamp not in (select ftstamp from ft)
	delete fi2 where ftstamp not in (select ftstamp from ft)

	delete from fo where docdata >= DATEADD(month, @monthLeft, GETDATE()) and site = @loja and doccode=@ndoc_vf
	delete fo2 where fo2stamp not in (select fostamp from fo)
	delete fn where fostamp not in (select fostamp from fo)




	--tabela de compras a fornecedor

	declare @MPComprasForn table 
	(
	fnstamp nvarchar(25), fostamp nvarchar(25), data datetime
		,obrano nvarchar(25), no numeric(10)
		,ref nvarchar(18), codigo nvarchar(18), design nvarchar(100), qtt numeric(7), stns bit
		,iva numeric(13,3), tabiva numeric(1), epv numeric(13,3), lobs varchar(40)
		,ousrinis nvarchar(8), ousrdata datetime, ousrhora nvarchar(8), usrinis nvarchar(8), usrdata datetime, usrhora nvarchar(8)
		,obs varchar(254)
	)



	--tabela de devolucoes a fornecedor

	declare @MPDevForn table 
	(
		bistamp nvarchar(25), bostamp nvarchar(25), data datetime
		,obrano numeric(9), no numeric(10)
		,ref nvarchar(18), codigo nvarchar(18), design nvarchar(100), qtt numeric(7), stns bit
		,iva numeric(13,3), tabiva numeric(1), epv numeric(13,3), lobs varchar(40)
		,ousrinis nvarchar(8), ousrdata datetime, ousrhora nvarchar(8), usrinis nvarchar(8), usrdata datetime, usrhora nvarchar(8)
		,obs varchar(254)
	)


	

	--tabela de entradas e saidas de clientes
	declare @MPVendas table 
	(
		fistamp nvarchar(25), ftstamp nvarchar(25), data nvarchar(8), adoc nvarchar(10)
		,ref nvarchar(18), design nvarchar(100), qtt numeric(5), stns bit
		,iva numeric(5,2), tabiva numeric(1), epv numeric(13,3)
		,ousrinis nvarchar(8), ousrdata nvarchar(8), ousrhora nvarchar(8), usrinis nvarchar(8), usrdata nvarchar(8), usrhora nvarchar(8)
	)




	/* Popular tabela  de compras a fornecedor */
	insert into @MPComprasForn
		select
			fnstamp		= 'ADM' + left(replace(newid(),'-',''), 21)
			,*
		from (
			select
				fostamp		= 	'VF_' + RTRIM(LTRIM(STR(@armazem))) + "_" + convert(varchar,x.ano)
								+ replicate('0',2-len(x.mes)) + convert(varchar,x.mes)
								+ '_' + ltrim(str(@site_nr))
				,data		= convert(varchar,x.ano)
								+ replicate('0',2-len(x.mes)) + convert(varchar,x.mes)
								+ '01'

				,obrano		=  'VF_' + RTRIM(LTRIM(STR(@armazem))) + "_" + RTRIM(LTRIM(STR(convert(numeric,right(x.ano,2)+ right(x.mes,2)))))
				,fornecNo
				,ref
				,codigo		= ref
				,design
				,qtt        = x.qttEntrada
				,stns
				,iva		= x.iva
				,tabiva		= x.tabiva
				,epv		= x.pct
				,lobs       = @obsFornecEntrada

				,ousrinis	='ADM'
				,ousrdata	= convert(varchar,x.ano) 
								+ replicate('0',2-len(x.mes)) + convert(varchar,x.mes)
								+ '01'
				,ousrhora	= @hora
				,usrinis	= 'ADM'
				,usrdata	= convert(varchar,x.ano) 
								+ replicate('0',2-len(x.mes)) + convert(varchar,x.mes)
								+ '01'
				,usrhora	= @hora
				,obs = @obsFornecEntrada
			from (
				select
					ref			= prd_cod
					,design		= left(rtrim(ltrim(isnull(st.design,''))),99)
					,stns		= st.stns
					,data		= ano_mes + '01'
					,fornecNo   = @idFornecedor
					,ano		= left(ano_mes,4)
					,mes		= right(ano_mes,2)
					,qttSaida   = convert(int,qt_dispensas)
					,qttEntrada	= convert(int,qt_compras)
					,pct		= st.epcusto
					,iva		= taxasiva.taxa
					,tabiva		= taxasiva.codigo
				from
					st (nolock)
					inner join sif2k_histVendas	hv (nolock) on st.ref collate SQL_Latin1_General_CP1_CI_AI = hv.prd_cod
					inner join taxasiva (nolock) on taxasiva.codigo = st.tabiva
				where
					st.site_nr = @site_nr
					and convert(int,qt_compras)>0
						
		
			) x

		) xx
		group by
			fostamp, data, obrano, ref, codigo,  design,fornecNo, qtt, stns, iva, tabiva, epv,lobs,obs, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
		order by
			data, ref

	
		declare @maxNumBoDev int = 1	
		select  @maxNumBoDev= MAX(obrano) from bo(nolock) where year(dataobra) = year(GETDATE()) and ndos = @ndoc_df

		/* Popular tabela  de devolucoes a fornecedor */
		insert into @MPDevForn
		select
			bistamp			= 'ADM' + left(replace(newid(),'-',''), 21)
			,*
		from (
			select
				bostamp		= 	'DF_' + RTRIM(LTRIM(STR(@armazem))) + "_" + convert(varchar,x.ano)
								+ replicate('0',2-len(x.mes)) + convert(varchar,x.mes)
								+ '_' + ltrim(str(@site_nr))
				,data		= convert(varchar,x.ano)
								+ replicate('0',2-len(x.mes)) + convert(varchar,x.mes)
								+ '01'

				,obrano		=  convert(numeric,right(x.ano,2)+ right(x.mes,2)) + @maxNumBoDev
				,fornecNo
				,ref
				,codigo		= ref
				,design
				,qtt        = abs(x.qttEntrada)
				,stns
				,iva		= x.iva
				,tabiva		= x.tabiva
				,epv		= x.pct
				,lobs       = @obsFornecSaida

				,ousrinis	='ADM'
				,ousrdata	= convert(varchar,x.ano) 
								+ replicate('0',2-len(x.mes)) + convert(varchar,x.mes)
								+ '01'
				,ousrhora	= @hora
				,usrinis	= 'ADM'
				,usrdata	= convert(varchar,x.ano) 
								+ replicate('0',2-len(x.mes)) + convert(varchar,x.mes)
								+ '01'
				,usrhora	= @hora
				,obs = @obsFornecSaida
			from (
				select
					ref			= prd_cod
					,design		= left(rtrim(ltrim(isnull(st.design,''))),99)
					,stns		= st.stns
					,data		= ano_mes + '01'
					,fornecNo   = @idFornecedor
					,ano		= left(ano_mes,4)
					,mes		= right(ano_mes,2)
					,qttSaida   = convert(int,qt_dispensas)
					,qttEntrada	= convert(int,qt_compras)
					,pct		= st.epcusto
					,iva		= taxasiva.taxa
					,tabiva		= taxasiva.codigo
				from
					st (nolock)
					inner join sif2k_histVendas	hv (nolock) on st.ref collate SQL_Latin1_General_CP1_CI_AI = hv.prd_cod
					inner join taxasiva (nolock) on taxasiva.codigo = st.tabiva
				where
					st.site_nr = @site_nr
					and convert(int,qt_compras)<0
							
			) x

		) xx
		group by
			bostamp, data, obrano, ref, codigo,  design,fornecNo, qtt, stns, iva, tabiva, epv,lobs,obs, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
		order by
			data, ref



	/* Popular tabela com todos os movimentos de saida de produtos */
	insert into @MPVendas
		select
			fistamp		= 'ADM' + left(replace(newid(),'-',''), 21)
			,*
		from (
			select
				ftstamp		= 	'VEN_' + RTRIM(LTRIM(STR(@armazem)))  + "_" + convert(varchar,x.ano)
								+ replicate('0',2-len(x.mes)) + convert(varchar,x.mes)
								+ '_' + ltrim(str(@site_nr))
				,data		= convert(varchar,x.ano)
								+ replicate('0',2-len(x.mes)) + convert(varchar,x.mes)
								+ '01'

				,adoc		= convert(numeric,right(x.ano,2)+ right(x.mes,2)) + @inc

				,ref
				,design
				,qtt		= x.qtt
				,stns

				,iva		= x.iva
				,tabiva		= x.tabiva
				,epv		= x.pct

				,ousrinis	='ADM'
				,ousrdata	= convert(varchar,x.ano) 
								+ replicate('0',2-len(x.mes)) + convert(varchar,x.mes)
								+ '01'
				,ousrhora	= @hora
				,usrinis	= 'ADM'
				,usrdata	= convert(varchar,x.ano) 
								+ replicate('0',2-len(x.mes)) + convert(varchar,x.mes)
								+ '01'
				,usrhora	= @hora
			from (
				select
					ref			= prd_cod
					,design		= st.design
					,stns		= st.stns
					,data		= ano_mes + '01'
					,ano		= left(ano_mes,4)
					,mes		= right(ano_mes,2)
					,qtt		= convert(int,qt_dispensas)
					,pct		= st.epcusto
					,iva		= taxasiva.taxa
					,tabiva		= taxasiva.codigo
				from
					st (nolock)
					inner join sif2k_histVendas	hv (nolock) on st.ref collate SQL_Latin1_General_CP1_CI_AI = hv.prd_cod
					inner join taxasiva (nolock) on taxasiva.codigo = st.tabiva
				where
					st.site_nr = @site_nr
					and convert(int,qt_dispensas)!=0
			) x

		) xx
		group by
			ftstamp, data, adoc, ref, design, qtt, stns, iva, tabiva, epv, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
		order by
			data, ref
			

	-- limpar Vendas que foram migradas

	delete from  @MPVendas 
	where ftstamp in (select ftstamp from ft(nolock)  where site = @loja and ndoc=@ndoc_s)


	delete from  @MPComprasForn 
	where fostamp in (select fostamp from fo(nolock)  where site = @loja and doccode = @ndoc_vf)



	delete from  @MPDevForn 
	where bostamp in (select bostamp from bo(nolock)  where site = @loja and nmdos = @nmdoc_df)



	-- gravar as vendas


	INSERT INTO FT
		(
		ftstamp
		,pais
		,ndoc
		,nmdoc
		,tipodoc
		,fdata
		,ftano
		,pdata
		,cdata
		,saida
		,chora
		,fno			
		
		,no			
		,nome
		,nome2
		,ncont
		,tipo
		,zona
		,moeda
		,memissao
				
		,vendedor
		,vendnm
		,[site]
		,pnome
		,pno
		,u_nratend
		
		,totqtt
		,qtt1
		,etotal
		,ettiliq
		,ettiva
		
		,ivatx1
		,ivatx2
		,ivatx3
		,ivatx4
		,ivatx5
		,ivatx6
		,ivatx7
		,ivatx8
		,ivatx9
		
		,ousrinis
		,ousrdata
		,ousrhora
		,usrinis
		,usrdata
		,usrhora
		
		,final
		) 
	select
		ftstamp			= ftstamp
		,pais			= 1
		,ndoc			= @ndoc_s
		,nmdoc			= @nmdoc_s
		,tipodoc		= @tipodoc_s
		,fdata			= data
		,ftano			= YEAR(data)
		,pdata			= data
		,cdata			= data
		,saida			= left(@hora,5)
		,chora			= left(replace(@hora,':',''),4)
		,fno			= adoc				
		
		,no				= 199
		,nome			= (select nome from b_utentes (nolock) where no=199)
		,nome2			= (select nome2 from b_utentes (nolock) where no=199)
		,ncont			= (select ncont from b_utentes (nolock) where no=199)
		,tipo			= (select tipo from b_utentes (nolock) where no=199)
		,zona			= (select zona from b_utentes (nolock) where no=199)
		,moeda			= (select moeda from b_utentes (nolock) where no=199)
		,memissao		= left((select moeda from b_utentes (nolock) where no=199),4)
		
		,vendedor		= 0
		,vendnm			= 'ADM'
		,[site]			= @loja
		,pnome			= 'Terminal 99'
		,pno			= 99
		,u_nratend		= '0000000199'
				
		,totqtt			= sum(qtt)
		,qtt1			= sum(qtt)
		,etotal			= ROUND(sum(
								epv*qtt
							),2)
		,ettiliq		= ROUND(sum(
								case when iva!=0 then (epv*qtt) / iva else epv*qtt end
							),2)
		,ettiva			= ROUND(sum(
								case when iva!=0 then (epv*qtt) - ((epv*qtt) / iva) else 0 end
						),2)
						
		,ivatx1			= (select taxa from taxasiva (nolock) where codigo = 1)
		,ivatx2			= (select taxa from taxasiva (nolock) where codigo = 2)
		,ivatx3			= (select taxa from taxasiva (nolock) where codigo = 3)
		,ivatx4			= (select taxa from taxasiva (nolock) where codigo = 4)
		,ivatx5			= (select taxa from taxasiva (nolock) where codigo = 5)
		,ivatx6			= (select taxa from taxasiva (nolock) where codigo = 6)
		,ivatx7			= (select taxa from taxasiva (nolock) where codigo = 7)
		,ivatx8			= (select taxa from taxasiva (nolock) where codigo = 8)
		,ivatx9			= (select taxa from taxasiva (nolock) where codigo = 9)
		
		,ousrinis
		,ousrdata
		,ousrhora
		,usrinis
		,usrdata
		,usrhora
		
		,final			= @finalCli
	from
		@MPVendas mp
	group by
		ftstamp, adoc
		,data, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
	order by
		ftstamp
	

	INSERT INTO ft2
		(
		ft2stamp	
		,vdollocal
		,vdlocal
		
		,ousrinis
		,ousrdata
		,ousrhora
		,usrinis
		,usrdata
		,usrhora
		
		,obsdoc
		)
	select
		ft2stamp		= ftstamp
		,vdollocal		= 'Caixa'
		,vdlocal		= 'C'
				
		,ousrinis		= ousrinis
		,ousrdata		= ousrdata
		,ousrhora		= ousrhora
		,usrinis		= usrinis
		,usrdata		= usrdata
		,usrhora		= usrhora
		
		,obsdoc			= final
	from
		ft
	where
		ft.ndoc = @ndoc_s
		and site = @loja
		and final = @finalCli
	order by
		ftstamp
		


	/* Criar Linhas */
	INSERT INTO fi
		(
		fistamp
		,ndoc
		,nmdoc
		,fno
		
		,ref
		,design
		,qtt
		,epv
		,u_epvp
		,iva
		,ivaincl
		,tabiva
		
		,etiliquido
		,eslvu
		,esltt

		,armazem
		,rdata
		,cpoc
		
		,lrecno
		,lordem
		,ftstamp
		,stipo
		,tipodoc
		,familia
		,stns
		
		,fivendedor
		,fivendnm
		,epvori
		,zona

		,ousrinis
		,ousrdata
		,ousrhora
		,usrinis
		,usrdata
		,usrhora
		)						
	select
		fistamp			= fistamp
		,ndoc			= @ndoc_s
		,nmdoc			= @nmdoc_s
		,fno			= adoc
		
		,ref
		,design
		,qtt
		,epv			= epv
		,u_epvp			= epv
		,iva			= iva
		,ivaincl		= 1
		,tabiva	
		
		,etiliquido		= epv * qtt
		,eslvu			= case when iva!=0 then epv / iva else epv end
		,esltt			= epv * qtt
		
		,armazem		= @armazem
		,rdata			= data
		,cpoc			= 1
		
		,lrecno			= fistamp
		,lordem			= 100
		,ftstamp		= ftstamp
		,stipo			= 1
		,tipodoc		= @tipodoc_s
		,familia		= isnull((select familia from st (nolock) where st.ref=mp.ref and st.site_nr = @site_nr),'99')
		,stns			= 1
		
		,fivendedor		= 0
		,fivendnm		= 'ADM'
		,epvori			= epv * qtt
		,zona			= (select zona from b_utentes (nolock) where no=199)

		,ousrinis		= 'ADM'
		,ousrdata		= data
		,ousrhora		= @hora
		,usrinis		= 'ADM'
		,usrdata		= data
		,usrhora		= @hora
	from
		@MPVendas mp
	order by
		ftstamp



	-- insere devolucoes a fornecedor

	INSERT INTO BO (
		bostamp, ndos, nmdos, obrano, dataobra
		,nome, nome2, no, estab, morada, local, codpost, tipo, tpdesc, ncont, boano, etotaldeb, dataopen
		,ecusto, etotal, moeda, memissao, origem, site, vendedor, vendnm, sqtt14
		,ebo_1tvall, ebo_2tvall, ebo_totp1, ebo_totp2
		,ebo11_bins, ebo12_bins, ebo21_bins, ebo22_bins, ebo31_bins, ebo32_bins, ebo41_bins, ebo42_bins, ebo51_bins, ebo52_bins
		,ebo11_iva, ebo12_iva, ebo21_iva, ebo22_iva, ebo31_iva, ebo32_iva, ebo41_iva, ebo42_iva, ebo51_iva, ebo52_iva
		,ocupacao, obs, datafinal, ccusto, pnome, pno
		,ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, fechada
		)
	select
		bostamp
		,ndos		= @ndoc_df
		,nmdos		= @nmdoc_df
		,obrano
		,dataobra	= mp.data

		,nome		= fl.nome
		,nome2		= fl.nome2
		,no			= fl.no
		,estab		= fl.estab
		,morada		= fl.morada
		,local		= fl.local
		,codpost	= fl.codpost
		,tipo		= fl.tipo
		,tpdesc		= fl.tpdesc
		,ncont		= fl.ncont
		,boano		= YEAR(mp.data)
		,etotaldeb	= sum(epv*qtt / (mp.iva/100+1))
		,dataopen	= mp.data

		,ecusto		= sum(epv)
		,etotal		= sum(epv*qtt)
		,moeda		= fl.moeda
		,memissao	= fl.moeda
		,origem		= 'BO'
		,site		= @loja
		,vendedor	= 1
		,vendnm		= 'ADM'
		,sqtt14		= sum(mp.qtt)

		,ebo_1tvall	= sum(mp.qtt * mp.epv)
		,ebo_2tvall	= sum(mp.qtt * mp.epv)
		,ebo_totp1	= sum(mp.qtt * mp.epv)
		,ebo_totp2	= sum(mp.qtt * mp.epv)

		-- total base
		,ebo11_bins	= sum(case when mp.tabiva=1 then mp.qtt * mp.epv / (mp.iva/100+1) else 0 end)
		,ebo12_bins	= sum(case when mp.tabiva=1 then mp.qtt * mp.epv / (mp.iva/100+1) else 0 end)
		,ebo21_bins	= sum(case when mp.tabiva=2 then mp.qtt * mp.epv / (mp.iva/100+1) else 0 end)
		,ebo22_bins	= sum(case when mp.tabiva=2 then mp.qtt * mp.epv / (mp.iva/100+1) else 0 end)
		,ebo31_bins = sum(case when mp.tabiva=3 then mp.qtt * mp.epv / (mp.iva/100+1) else 0 end) 
		,ebo32_bins = sum(case when mp.tabiva=3 then mp.qtt * mp.epv / (mp.iva/100+1) else 0 end) 
		,ebo41_bins = sum(case when mp.tabiva=4 then mp.qtt * mp.epv / (mp.iva/100+1) else 0 end) 
		,ebo42_bins = sum(case when mp.tabiva=4 then mp.qtt * mp.epv / (mp.iva/100+1) else 0 end) 
		,ebo51_bins = sum(case when mp.tabiva=5 then mp.qtt * mp.epv / (mp.iva/100+1) else 0 end) 
		,ebo52_bins = sum(case when mp.tabiva=5 then mp.qtt * mp.epv / (mp.iva/100+1) else 0 end)

		-- total iva
		,ebo11_iva	= sum(case when mp.tabiva=1 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end)
		,ebo12_iva	= sum(case when mp.tabiva=1 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end)
		,ebo21_iva	= sum(case when mp.tabiva=2 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end)
		,ebo22_iva	= sum(case when mp.tabiva=2 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end)
		,ebo31_iva	= sum(case when mp.tabiva=3 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end)
		,ebo32_iva	= sum(case when mp.tabiva=3 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end)
		,ebo41_iva	= sum(case when mp.tabiva=4 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end)
		,ebo42_iva	= sum(case when mp.tabiva=4 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end)
		,ebo51_iva	= sum(case when mp.tabiva=5 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end)
		,ebo52_iva	= sum(case when mp.tabiva=5 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end)

		,ocupacao	= 4
		,mp.obs
		,datafinal	= mp.data
		,ccusto		= sum(mp.epv)
		,pnome		= @terminalNome
		,pno		= @terminal

		,ousrinis	= 'ADM'
		,ousrdata	= mp.data
		,ousrhora	= @hora
		,usrinis	= 'ADM'
		,usrdata	= mp.data
		,usrhora	= @hora
		,fechada    = 0
	from
		@MPDevForn  mp
		inner join fl (nolock) on fl.no = mp.no and fl.estab=0
	group by
		mp.bostamp, mp.obrano, mp.data, mp.ousrdata, mp.usrdata, mp.obs
		,fl.nome, fl.nome2, fl.no, fl.estab, fl.morada, fl.local, fl.codpost, fl.tipo, fl.tpdesc, fl.ncont, fl.moeda
	order by
		bostamp


	INSERT INTO bo2 (
		bo2stamp
		,autotipo, pdtipo, etotalciva, u_doccont, ETOTIVA
		,telefone, email, armazem
		,ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
		)
	select
		bo2stamp	= bostamp

		,autotipo	= 1
		,pdtipo		= 1
		,etotalciva	= bo.etotal
		,u_doccont	= 0
		,etotiva	= bo.ebo11_iva + bo.ebo21_iva + bo.ebo31_iva + ebo41_iva + ebo51_iva

		,telefone	= fl.telefone
		,email		= fl.email

		,armazem	=  @armazem



		,ousrinis	= bo.ousrinis
		,ousrdata	= bo.ousrdata
		,ousrhora	= bo.ousrhora
		,usrinis	= bo.usrinis
		,usrdata	= bo.usrdata
		,usrhora	= bo.usrhora
	from
		bo				(nolock)
		inner join fl	(nolock) on fl.no = bo.no and fl.estab=0		
	where
		bo.obs like @obsFornecSaida
		and bo.site = @loja and bo.dataobra >= DATEADD(month, @monthLeft, GETDATE()) and bo.ndos=@ndoc_df 
	order by
		bostamp

	--/* Criar Linhas de Entrada */
	INSERT INTO bi (
		bistamp, bostamp, nmdos, ndos, obrano
		,ref, codigo, design, qtt, uni2qtt
		,iva, tabiva, armazem, stipo, cpoc
		,familia, no, nome, local, morada, codpost, epu, edebito, epcusto, ettdeb, ecustoind, edebitoori, u_upc
		,rdata, dataobra, dataopen, lordem, lobs
		,vendedor, vendnm
		,ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
		)
	select
		bistamp			= mp.bistamp
		,bostamp		= mp.bostamp
		,nmdos			= @nmdoc_df
		,ndos			= @ndoc_df
		,obrano			= mp.obrano
		
		,ref			= mp.ref
		,codigo			= mp.ref
		,design			= mp.design
		,qtt			= mp.qtt
		,uni2qtt		= mp.qtt
		
		,iva			= mp.iva
		,tabiva			= mp.tabiva
		,armazem		= @armazem
		,stipo			= 4
		,cpoc			= st.cpoc
		,familia		= st.familia
		,no				= fl.no
		,nome			= fl.nome
		,local			= fl.local
		,morada			= fl.morada
		,codpost		= fl.codpost
		,epu			= mp.epv
		,edebito		= mp.epv
		,ecusto			= mp.epv
		,ettdeb			= mp.epv * mp.qtt
		,ecustoind		= mp.epv
		,edebitoori		= mp.epv
		,u_upc			= mp.epv
		
		,rdata			= mp.data
		,dataobra		= mp.data
		,dataopen		= mp.data
		,lordem			= 100
		,lobs			= lobs
		
		,vendedor		= 1
		,vendnm			= 'ADM'
		
		,ousrinis		= 'ADM'
		,ousrdata		= mp.data
		,ousrhora		= @hora
		,usrinis		= 'ADM'
		,usrdata		= mp.data
		,usrhora		= @hora
	from
		@MPDevForn mp
		inner join st (nolock) on st.ref = mp.ref and st.site_nr = @site_nr
		inner join fl (nolock) on fl.no = mp.no and fl.estab=0
	order by
		bostamp	

		
	
	--/* Criar Linhas secund�rias */
	insert into bi2 (
		bi2stamp, bostamp, morada, local, codpost
		)
	select
		bi.bistamp
		,bi.bostamp
		,bi.morada
		,bi.local
		,bi.codpost
	from
		bi
		inner join bo on bo.bostamp=bi.bostamp
	where
		bo.obs like @obsFornecSaida
		and bo.site = @loja and bo.dataobra >= DATEADD(month, @monthLeft, GETDATE()) and bo.ndos=@ndoc_df 

	-- Fim das devolucoes a fornecedor


	-- inicio de criacao de vossas facturas a fornecedor



	--Gravacao docs fornecedor - V/factura
	INSERT INTO FO (
		fostamp, doccode, docnome, adoc, docdata
		,nome, nome2, no, estab, morada, local, codpost, tipo, tpdesc, ncont, foano, ettiliq, pdata
		, etotal, moeda, memissao, site,final,eivain,ettiva
		,eivav1, eivav2, eivav3, eivav4, eivav5, eivav6, eivav7,eivav8, eivav9
		,ccusto,pnome, pno
		,ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, pais, u_status, data
		)
	select
		fostamp
		,doccode	= @ndoc_vf
		,docnome	= @nmdoc_vf
		,adoc       = mp.obrano
		,docdata	= mp.data
		,nome		= fl.nome
		,nome2		= fl.nome2
		,no			= fl.no
		,estab		= fl.estab
		,morada		= fl.morada
		,local		= fl.local
		,codpost	= fl.codpost
		,tipo		= fl.tipo
		,tpdesc		= fl.tpdesc
		,ncont		= fl.ncont
		,foano		= YEAR(mp.data)
		,ettiliq	= sum(epv*qtt / (mp.iva/100+1))
		,pdata		= mp.data
		,etotal		= sum(epv*qtt)
		,moeda		= fl.moeda
		,memissao	= fl.moeda
		,site		= @loja
		,final      = @obsFornecEntrada
		,eivain     = sum(epv*qtt / (mp.iva/100+1))
		,ettiva		= sum(case when mp.tabiva=1 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end) +
					  sum(case when mp.tabiva=2 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end) +
					  sum(case when mp.tabiva=3 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end) +
					  sum(case when mp.tabiva=4 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end) +
					  sum(case when mp.tabiva=5 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end) +
					  sum(case when mp.tabiva=6 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end) +
					  sum(case when mp.tabiva=7 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end) +
					  sum(case when mp.tabiva=8 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end) +
					  sum(case when mp.tabiva=9 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end) 
		-- total iva
		,eivav1		= sum(case when mp.tabiva=1 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end)
		,eivav2		= sum(case when mp.tabiva=2 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end)
		,eivav3		= sum(case when mp.tabiva=3 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end)
		,eivav4		= sum(case when mp.tabiva=4 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end)
		,eivav5		= sum(case when mp.tabiva=5 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end)
		,eivav6		= sum(case when mp.tabiva=6 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end)
		,eivav7		= sum(case when mp.tabiva=7 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end)
		,eivav8		= sum(case when mp.tabiva=8 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end)
		,eivav9		= sum(case when mp.tabiva=9 then (mp.qtt * mp.epv) - (mp.qtt * mp.epv / (mp.iva/100+1)) else 0 end)
		,ccusto		= sum(mp.epv)
		,pnome		= @terminalNome
		,pno		= @terminal
		,ousrinis	= 'ADM'
		,ousrdata	= mp.data
		,ousrhora	= @hora
		,usrinis	= 'ADM'
		,usrdata	= mp.data
		,usrhora	= @hora
		,pais       = fl.pais
		,u_status   = 'A'
		,data	= mp.data
	from
		@MPComprasForn mp
		inner join fl (nolock) on fl.no = mp.no and fl.estab=0
	group by
		mp.fostamp, mp.obrano, mp.data, mp.ousrdata, mp.usrdata, mp.obs
		,fl.nome, fl.nome2, fl.no, fl.estab, fl.morada, fl.local, fl.codpost, fl.tipo, fl.tpdesc, fl.ncont, fl.moeda, fl.pais
	order by
		fostamp
	


	 
	INSERT INTO fo2 (
		fo2stamp
		,ivatx1, ivatx2, ivatx3, ivatx4, ivatx5, ivatx6, ivatx7, ivatx8, ivatx9
		,u_docfl, u_docflno
		,usrinis, usrdata, u_armazem
		)
	select
		fo2stamp	= fostamp
		,ivatx1 = (select taxa from taxasiva (nolock) where codigo=1)
		,ivatx2 = (select taxa from taxasiva (nolock) where codigo=2)
		,ivatx3 = (select taxa from taxasiva (nolock) where codigo=3)
		,ivatx4 = (select taxa from taxasiva (nolock) where codigo=4)
		,ivatx5 = (select taxa from taxasiva (nolock) where codigo=5)
		,ivatx6 = (select taxa from taxasiva (nolock) where codigo=6)
		,ivatx7 = (select taxa from taxasiva (nolock) where codigo=7)
		,ivatx8 = (select taxa from taxasiva (nolock) where codigo=8)
		,ivatx9 = (select taxa from taxasiva (nolock) where codigo=9)
		,u_docfl	= isnull(fl.nome,'')
		,u_docflno	= isnull(fo.no,0)
		,usrinis	= fo.usrinis
		,usrdata	= fo.usrdata
		,u_armazem	= @armazem

	from
		fo				(nolock)
		inner join fl	(nolock) on fl.no = fo.no and fl.estab=0
	where
		fo.final like @obsFornecEntrada
		and site = @loja
	order by
		fostamp




	/* Criar Linhas */
	INSERT INTO fn (
		fnstamp, fostamp, docNome, adoc
		,ref, codigo, design, qtt, uni2qtt
		,stns,iva, tabiva, armazem ,lordem
		,familia, epv, etiliquido,esltt
		,ousrinis, ousrdata, ousrhora,usrinis, usrdata, usrhora 
		)
	select
		fnstamp			= mp.fnstamp
		,bostamp		= mp.fostamp
		,docNome		= @nmdoc_vf
		,adoc			= mp.obrano		
		,ref			= mp.ref
		,codigo			= mp.ref
		,design			= mp.design
		,qtt			= mp.qtt
		,uni2qtt		= mp.qtt
		,stns		    = isnull(st.stock,0)
		,iva			= mp.iva
		,tabiva			= mp.tabiva
		,armazem		= @armazem
		,lordem	        = '1000'
		,familia        = isnull(st.familia,99)
		,epv			= mp.epv
		,etiliquido		= mp.epv * mp.qtt
		,esltt          = mp.epv * mp.qtt 		
		,ousrinis		= 'ADM'
		,ousrdata		= mp.data
		,ousrhora		= @hora
		,usrinis		= 'ADM'
		,usrdata		= mp.data
		,usrhora		= @hora
	from
		@MPComprasForn mp
		inner join st (nolock) on st.ref = mp.ref and st.site_nr = @site_nr
		inner join fl (nolock) on fl.no = mp.no and fl.estab=0
	order by
		fostamp	
	
	
	
	print ''
	print '-------------------------------------------'
	print 'Documentos de Entrada de Fornecedor Migrados'
	print '-------------------------------------------'
	print ''
	


	--Gravacao docs fornecedor - V/factura

	update 
		st 
	set 
		stock = CONVERT(int,sif2k_prod.SAC)
		,epcusto = case 
					-- 1� cenario: Produtos sem EPCUSTO, iva diferente de zero, pre�o diferente de zero
					when isnull(convert(numeric(13,3),pcu),0) = 0 and isnull(abs(convert(numeric(13,3),pvp)), 0) != 0 and taxasiva.codigo in (1, 2, 3)
						then convert(numeric(13,3), round(convert(numeric(13,3),convert(numeric(13,3),pvp)) / (taxasiva.taxa/100+1),2))
										
					---- 2� cenario: Produtos sem EPCUSTO, iva IGUAL a zero, pre�o diferente de zero. Avalia-se TAXA igual a 0% porque ha varias linhas na TAXASIVA para 0%
					when isnull(convert(numeric(13,3),pcu),0)=0 and isnull(abs(convert(numeric(13,3),pvp)),0)!=0 and taxasiva.taxa=0
						then convert(numeric(13,3), round(convert(numeric(13,3),pvp),2))
										
					-- 3� cenario: PCU original da farmacia
					else
						abs(isnull(convert(numeric(13,3),pcu),0))
				end
		,epcult = case 
					-- 1� cenario: Produtos sem EPCUSTO, iva diferente de zero, pre�o diferente de zero
					when isnull(convert(numeric(13,3),pcu),0) = 0 and isnull(abs(convert(numeric(13,3),pvp)), 0) != 0 and taxasiva.codigo in (1, 2, 3)
						then convert(numeric(13,3), round(convert(numeric(13,3),convert(numeric(13,3),pvp)) / (taxasiva.taxa/100+1),2))
										
					---- 2� cenario: Produtos sem EPCUSTO, iva IGUAL a zero, pre�o diferente de zero. Avalia-se TAXA igual a 0% porque ha varias linhas na TAXASIVA para 0%
					when isnull(convert(numeric(13,3),pcu),0)=0 and isnull(abs(convert(numeric(13,3),pvp)),0)!=0 and taxasiva.taxa=0
						then convert(numeric(13,3), round(convert(numeric(13,3),pvp),2))
										
					-- 3� cenario: PCU original da farmacia
					else
						abs(isnull(convert(numeric(13,3),pcu),0))
				end
		,epcpond        = abs(isnull(convert(numeric(13,3),pcp),0))
		,epv1			= isnull(abs(convert(numeric(13,3),pvp)),0)
	from 
		st(nolock)
	inner join 
		sif2k_prod on sif2k_prod.CPR = st.ref
	left join 
		taxasiva on convert(numeric(5,2),sif2k_prod.IVA) = taxasiva.taxa
	where 
		st.site_nr = @site_nr



	print ''
	print '-------------------------------------------'
	print 'Movimentos de produto Migrados'
	print '-------------------------------------------'
	print ''
	
	--enable triggers

	--EXEC('ALTER TABLE bi ENABLE TRIGGER tr_bi_delete'); 
	--EXEC('ALTER TABLE bi ENABLE TRIGGER tr_bi_insert'); 
	--EXEC('ALTER TABLE bi ENABLE TRIGGER tr_bi_update'); 


	--EXEC('ALTER TABLE fi ENABLE TRIGGER tr_fi_delete'); 
	--EXEC('ALTER TABLE fi ENABLE TRIGGER tr_fi_insert'); 
	--EXEC('ALTER TABLE fi ENABLE TRIGGER tr_fi_update'); 


		
	--EXEC('ALTER TABLE sl ENABLE TRIGGER tr_sl_delete'); 
	--EXEC('ALTER TABLE sl ENABLE TRIGGER tr_sl_insert'); 
	--EXEC('ALTER TABLE sl ENABLE TRIGGER tr_sl_update'); 


END



Go
Grant Execute on dbo.up_migracao_sifarma_stocks to Public
Grant control on dbo.up_migracao_sifarma_stocks to Public
Go
