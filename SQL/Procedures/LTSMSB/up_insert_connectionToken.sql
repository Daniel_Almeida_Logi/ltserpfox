/* inserir informação do token actual 
	


exec up_insert_connectionToken 'DSADSADS','ADM'

*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_insert_connectionToken]') IS NOT NULL
	drop procedure dbo.up_insert_connectionToken
go

create procedure [dbo].[up_insert_connectionToken]
	 @token								AS VARCHAR(36),
	 @inis								AS VARCHAR(100)


/* with encryption */
AS
SET NOCOUNT ON
BEGIN
	DELETE connectionTokenDbs WHERE usrdata <= DATEADD(DAY, -5, GETDATE()) 
	
	INSERT INTO connectionTokenDbs(stamp,token,[ousrdata],[ousrinis],[usrdata],[usrinis])
	values(LEFT(NEWID(),36),@token,GETDATE(),@inis,GETDATE(),@inis)

END 
Grant Execute on dbo.up_insert_connectionToken to Public
Grant control on dbo.up_insert_connectionToken to Public
GO