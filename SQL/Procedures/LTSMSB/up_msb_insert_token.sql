
/* 
	Insere Access Token do sms Pro
 	exec up_msb_insert_token 'BearerToken','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpc3MiOiJ1cm46XC9cL21wcm8tbTJtLWF1dGhvcml6YXRpb24iLCJleHAiOjE3MzA4MTA5NzksImlhdCI6MTczMDgwNzM3OSwiY2xpZW50X2lkIjoiMDAxNTdiN2MtMmQ4Ny00MzExLWIwMDMtZmRiNzQ5ZjExMDlmIiwianRpIjoiOWZmMGRhMzAtYzNmNi00MjQzLThjN2EtMGUwY2ZlNWQ4MTRiIn0.aaMWhAl7ZYDN8QkP5UBQd9b4kbnbHtFiizhVkaexCFIfmJCPzLpWtnk--nQE-OkgJcHEv3Ve1nm60k2Ln1y5Iw',1730807379923,3599,'approved'
	exec up_msb_insert_token 'BearerToken','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9',1731067674,3599,'approved'
	select * from smsTokenInfo
*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_msb_insert_token]') IS NOT NULL
	drop procedure dbo.up_msb_insert_token
go

create procedure [dbo].up_msb_insert_token
	 @token_type							AS VARCHAR(254),
	 @access_token							AS VARCHAR(1500),
	 @issued_at								AS NUMERIC(19,0),
	 @expires_in							AS NUMERIC(19,0),
	 @status								AS VARCHAR(254)

AS
BEGIN

	delete from smsTokenInfo where access_token = @access_token

	insert into smsTokenInfo(token_type, access_token, issued_at, expires_in, status)
	values (@token_type, @access_token, @issued_at, @expires_in, @status)

END



Go
Grant Execute on dbo.up_msb_insert_token to Public
Grant control on dbo.up_msb_insert_token to Public
Go




