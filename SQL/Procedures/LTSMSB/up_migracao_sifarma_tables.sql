
/* 
	Cria tabela das migracao para o sifarma
	exec up_migracao_sifarma_tables

*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_migracao_sifarma_tables]') IS NOT NULL
	drop procedure dbo.up_migracao_sifarma_tables
go

create procedure [dbo].up_migracao_sifarma_tables

/* WITH ENCRYPTION */ 

AS
BEGIN

	IF  EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'sif2k_forn')	
		drop table sif2k_forn

	IF  EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'sif2k_cli')	
		drop table sif2k_cli

	IF  EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'sif2k_prod')	
		drop table sif2k_prod

	IF  EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'sif2k_vdnormal')	
		drop table sif2k_vdnormal

	IF  EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'sif2k_recibos')	
		drop table sif2k_recibos
	
	
	IF  EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'sif2k_vdsusp')	
		drop table sif2k_vdsusp

	
	IF  EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'sif2k_histConsumo')	
		drop table sif2k_histConsumo
	
	IF  EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'sif2k_histVendas')	
		drop table sif2k_histVendas

    IF  EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'sif2k_movimentos_prd')	
		drop table sif2k_movimentos_prd

	IF  EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'sif2k_devolucoes')	
		drop table sif2k_devolucoes
	
	IF  EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'sif2k_cli_perfil')	
		drop table sif2k_cli_perfil

			
	IF  EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'sif2k_recibos')	
		drop table sif2k_recibos
	
	IF  EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'infoprex')	
		drop table infoprex

	IF  EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'sif2k_prod_alternativo')	
		drop table sif2k_prod_alternativo
	

	IF  EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'sif2k_vendas')	
		drop table sif2k_vendas
		
	IF  EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'infoprex')	
		drop table infoprex
		
			
			
	CREATE TABLE [dbo].[infoprex](
		[CPR] [float] NULL,
		[NOM] [nvarchar](255) NULL,
		[FAP] [nvarchar](255) NULL,
		[LOCALIZACAO] [nvarchar](255) NULL,
		[SAC] [float] NULL,
		[STM] [float] NULL,
		[SMI] [float] NULL,
		[QTE] [float] NULL,
		[DUV] [datetime] NULL,
		[DUC] [datetime] NULL,
		[PVP] [float] NULL,
		[PCU] [float] NULL,
		[IVA] [float] NULL,
		[CAT] [nvarchar](255) NULL,
		[CT1] [nvarchar](255) NULL,
		[GEN] [nvarchar](255) NULL,
		[GPR] [float] NULL,
		[CLA] [nvarchar](255) NULL,
		[TPR] [nvarchar](255) NULL,
		[CAR] [nvarchar](255) NULL,
		[GAM] [nvarchar](255) NULL,
		[V(0)] [float] NULL,
		[V(1)] [float] NULL,
		[V(2)] [float] NULL,
		[V(3)] [float] NULL,
		[V(4)] [float] NULL,
		[V(5)] [float] NULL,
		[V(6)] [float] NULL,
		[V(7)] [float] NULL,
		[V(8)] [float] NULL,
		[V(9)] [float] NULL,
		[V(10)] [float] NULL,
		[V(11)] [float] NULL,
		[V(12)] [float] NULL,
		[V(13)] [float] NULL,
		[V(14)] [float] NULL,
		[V(15)] [float] NULL,
		[V(16)] [float] NULL,
		[V(17)] [float] NULL,
		[V(18)] [float] NULL,
		[V(19)] [float] NULL,
		[V(20)] [float] NULL,
		[V(21)] [float] NULL,
		[V(22)] [float] NULL,
		[V(23)] [float] NULL,
		[DTVAL] [nvarchar](255) NULL,
		[FPD] [nvarchar](255) NULL,
		[LAD] [nvarchar](255) NULL,
		[PRATELEIRA] [nvarchar](255) NULL,
		[GAMA] [nvarchar](255) NULL,
		[GRUPOHOMOGENEO] [nvarchar](255) NULL,
		[INACTIVO] [nvarchar](255) NULL,
		[PVP5] [float] NULL,
		[CNPEM] [float] NULL,
		[FORMA_FARM_ABREV] [nvarchar](255) NULL,
		[DT_CRIACAO] [datetime] NULL
	) ON [PRIMARY]
	
		/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[infoprex]') AND name = N'IX_infoprex_cpr')
	CREATE NONCLUSTERED INDEX [IX_infoprex_cpr] ON [dbo].[infoprex]
	(
		[cpr] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	



	CREATE TABLE [dbo].[sif2k_prod](
		[CPR] [nvarchar](18) NULL
		,[NOM] [nvarchar](255) NULL
		,[FAP] [nvarchar](255) NULL
		,[SAC] [numeric](5, 0) NULL
		,[DTVAL] [nvarchar](20) NULL
		,[STM] [numeric](5, 0) NULL
		,[SMI] [numeric](5, 0) NULL
		,[PVP] [numeric](13, 3) NULL
		,[PCU] [numeric](13, 3) NULL
		,[PCP] [numeric](13, 3) NULL
		,[IVA] [numeric](3, 0) NULL
		,[APAGADO] [nvarchar](10) NULL
		,[ANULADO] [nvarchar](10) NULL
		,[GPR] [nvarchar](10) NULL
		,[CLA] [nvarchar](10) NULL
		,[GAM] [nvarchar](200) NULL
		,[PRATELEIRA] [nvarchar](200) NULL
		,[GAMA] [nvarchar](200) NULL
		,[forn_farmacia_id] [numeric](16, 0) NULL
		,[localizanome] [nvarchar](255) NULL
		,[encomendavel] [nvarchar](10) NULL
		,[ousrdata]   datetime  NULL
	) ON [PRIMARY]


	/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sif2k_prod]') AND name = N'IX_sif2k_prod_cpr')
	CREATE NONCLUSTERED INDEX [IX_sif2k_prod_cpr] ON [dbo].[sif2k_prod]
	(
		[cpr] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
	ALTER TABLE sif2k_prod ADD CONSTRAINT DF_sif2k_prod_ousrdata DEFAULT GETDATE() FOR ousrdata
	


	
	CREATE TABLE [dbo].[sif2k_cli](
		[nome] [nvarchar](254) NULL,
		[num_cliente] [nvarchar] (20) null,
		[sexo] [nvarchar](2) NULL,
		[titulo] [nvarchar](10) NULL,
		[num_cartao_ute] [nvarchar](100) NULL,
		[morada] [nvarchar](200) NULL,
		[dt_nasc] [nvarchar](30) NULL,
		[bi] [nvarchar](15) NULL,
		[dt_bi] [nvarchar](30) NULL,
		[nif] [nvarchar](30) NULL,
		[tel_residencia] [nvarchar](22) NULL,
		[tel_trabalho] [nvarchar](22) NULL,
		[telemovel] [nvarchar](22) NULL,
		[email] [nvarchar](50) NULL,
		[sit] [nvarchar](5) NULL,
		[tipo_pessoa] [nvarchar](5) NULL,
		[dt_criacao] [nvarchar](30) NULL,
		[limite_credito] [nvarchar](13) NULL,
		[apagado]  [nvarchar](13) NULL,
		[obervacao] [nvarchar](254) NULL,
		[visivel] [nvarchar](13) NULL,
		[consent_1] [nvarchar](13) NULL,
		[consent_2] [nvarchar](13) NULL,
		[consent_3] [nvarchar](13) NULL,
		[codPost1]  [nvarchar](13) NULL,
		[codPost2] [nvarchar](13) NULL,
		[localidade] [nvarchar](100) NULL,

	) ON [PRIMARY]

	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sif2k_cli]') AND name = N'IX_sif2k_cli_num_cliente')
	CREATE NONCLUSTERED INDEX IX_sif2k_cli_num_cliente ON [dbo].sif2k_cli
	(
		[num_cliente] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]



	
	CREATE TABLE [dbo].[sif2k_cli_perfil](
		[nome] [nvarchar](254) NULL,
		[num_cliente] [nvarchar] (20) null,
		[bi] [nvarchar](15) NULL,
		[nif] [nvarchar](15) NULL,
		[codPerfil] [nvarchar](100) NULL,
		[nomePerfil] [nvarchar](254) NULL,
	) ON [PRIMARY]


	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sif2k_cli_perfil') AND name = N'IX_sif2k_cli_perfil_num_cliente')
	CREATE NONCLUSTERED INDEX IX_sif2k_cli_perfil_num_cliente ON [dbo].sif2k_cli_perfil
	(
		[num_cliente] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	

	
	CREATE TABLE [dbo].[sif2k_Forn](
		[no] [nvarchar](20) NULL,
		[nome] [nvarchar](254) NULL,
		[morada] [nvarchar](55) NULL,
		[apagado] [nvarchar](130) NULL,
		[local] [nvarchar](255) NULL,
		[telefone] [nvarchar](30) NULL,
		[ncont] [nvarchar](30) NULL,
		[estado] [nvarchar](30) NULL,
		[idt] [nvarchar](255) NULL,
		[ousrdata] [nvarchar](30) NULL

	) ON [PRIMARY]


		IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sif2k_Forn') AND name = N'IX_sif2k_Forn_no')
	CREATE NONCLUSTERED INDEX IX_sif2k_Forn_no ON [dbo].sif2k_Forn
	(
		[no] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	


	CREATE TABLE [dbo].[sif2k_vdsusp](
		[IDT] [nvarchar](30) NULL,
		[DATAHORA] [nvarchar](30) NULL,
		[NUM_VENDA] [nvarchar](20) NULL,
		[NUM_DOC] [nvarchar](20) NULL,
		[NUM_CLIENTE] [nvarchar](20) NULL,
		[NIF_UTENTE] [nvarchar](20) NULL,
		[CODIGO] [nvarchar](30) NULL,
		[DESIGNACAO] [nvarchar](220) NULL,
		[IVA] [numeric](7,2) NULL,
		[QT] [numeric](6, 0) NULL,
		[QT_FALTA] [numeric](6, 0) NULL,
		[PRECO] [numeric](13,3) NULL,
		[PRECO_ESC] [numeric](13,3) NULL,
		[TOTAL] [numeric](13,3) NULL,
		[TOTAL_ESC] [numeric](13,3) NULL,
		[REGULAR] [nvarchar](30) NULL,
		[CREDITADO] [numeric](13,3) NULL,
		[CREDITADO_ESC] [numeric](13,3) NULL,
		[PAGO] [numeric](13,3) NULL,
		[SUM_QT] [numeric](5, 0) NULL,
		[SUM_QT_FALTA] [numeric](5, 0) NULL,
		[SUM_TOTAL] [numeric](13,3) NULL,
		[SUM_TOTAL_ESC] [numeric](13,3) NULL,
		[SUM_PAGO] [numeric](13,3) NULL,
		[SUM_PAGO_ESC] [numeric](13,3) NULL,
		[SUM_CREDITADO] [numeric](13,3) NULL,
		[SUM_CREDITADO_ESC] [numeric](13,3) NULL,
		[UTILIZADOR] [nvarchar](100) NULL
	) ON [PRIMARY]


	
	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sif2k_vdsusp]') AND name = N'IX_sif2k_vdsusp_NUM_CLIENTE')
	CREATE NONCLUSTERED INDEX IX_sif2k_vdsusp_NUM_CLIENTE ON [dbo].sif2k_vdsusp
	(
		[NUM_CLIENTE] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sif2k_vdsusp]') AND name = N'IX_sif2k_vdsusp_CODIGO')
	CREATE NONCLUSTERED INDEX IX_sif2k_vdsusp_CODIGO ON [dbo].sif2k_vdsusp
	(
		[CODIGO] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	


	CREATE TABLE [dbo].[sif2k_histConsumo](
			[DATAHORA] [nvarchar] (30) NULL,
			[NUM_VENDA] [nvarchar](20) NULL,
			[NUM_DOC] [nvarchar](20) NULL,
			[NUM_CLIENTE] [nvarchar](20) NULL,
			[NOME_CLIENTE] [nvarchar](220) NULL,
			[NIF_UTENTE] [nvarchar](20) NULL,
			[SERVICO] [nvarchar](254) NULL,
			[CODIGO] [nvarchar](20) NULL,
			[DESIGNACAO] [nvarchar](220) NULL,
			[QT] [numeric](5, 0) NULL,
			[UTILIZADOR] [nvarchar](100) NULL
		) ON [PRIMARY]


	

	
	CREATE TABLE [dbo].[sif2k_vdnormal](
		[DATAHORA] [nvarchar](30) NULL,
		[NUM_VENDA] [nvarchar](20) NULL,
		[NUM_CLIENTE] [nvarchar](20) NULL,
		[CODIGO] [nvarchar](20) NULL,
		[DESIGNACAO] [nvarchar](220) NULL,
		[IVA] [numeric](13,3) NULL,
		[QT] [numeric](5, 0) NULL,
		[QT_FALTA] [numeric](5, 0) NULL,
		[PRECO] [numeric](13,3) NULL,
		[TOTAL] [numeric](13,3) NULL,
		[CREDITADO] [numeric](13,3) NULL,
		[DEBITO] [numeric](13,3) NULL,
		[CREDITO] [numeric](13,3) NULL,
		[REMANESCENTE] [numeric](13,3) NULL,
		[UTILIZADOR] [nvarchar](100) NULL,
		[DTIPO] [nvarchar](30) NULL,
		[TIPO] [nvarchar](10) NULL,
		[documentos_idt] [nvarchar](100) NULL,

	) ON [PRIMARY]

	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sif2k_vdnormal]') AND name = N'IX_sif2k_vdnormal_NUM_CLIENTE')
	CREATE NONCLUSTERED INDEX IX_sif2k_vdnormal_NUM_CLIENTE ON [dbo].sif2k_vdnormal
	(
		[NUM_CLIENTE] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sif2k_vdnormal]') AND name = N'IX_sif2k_vdnormal_CODIGO')
	CREATE NONCLUSTERED INDEX IX_sif2k_vdnormal_CODIGO ON [dbo].sif2k_vdnormal
	(
		[CODIGO] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	


	
	
	CREATE TABLE [dbo].[sif2k_recibos](
		[DATAHORA] [nvarchar](30) NULL,
		[NUM_VENDA] [nvarchar](20) NULL,
		[NUM_CLIENTE] [nvarchar](20) NULL,
		[CODIGO] [nvarchar](20) NULL,
		[DESIGNACAO] [nvarchar](220) NULL,
		[IVA] [numeric](13,3) NULL,
		[QT] [numeric](5, 0) NULL,
		[QT_FALTA] [numeric](5, 0) NULL,
		[PRECO] [numeric](13,3) NULL,
		[TOTAL] [numeric](13,3) NULL,
		[CREDITADO] [numeric](13,3) NULL,
		[DEBITO] [numeric](13,3) NULL,
		[CREDITO] [numeric](13,3) NULL,
		[REMANESCENTE] [numeric](13,3) NULL,
		[UTILIZADOR] [nvarchar](100) NULL,
		[DTIPO] [nvarchar](30) NULL,
		[TIPO] [nvarchar](10) NULL,
		[LIQUIDADOS] [NVARCHAR](100) NULL,
		[VENDAS_ID] [numeric](13,0) NULL,
		[num_doc] [NVARCHAR](100) NULL,
		[documentos_id_anul] [NVARCHAR](100) NULL,
		[documentos_id_rec] [NVARCHAR](100) NULL,
		[DOCID] [NVARCHAR](100) NULL,
		[valor_reg] [numeric](13,3) NULL,
		[documento_cred_id] [NVARCHAR](100) NULL,
		
		
		

	) ON [PRIMARY]

	
	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].sif2k_recibos') AND name = N'IX_sif2k_recibos_NUM_CLIENTE')
	CREATE NONCLUSTERED INDEX IX_sif2k_recibos_NUM_CLIENTE ON [dbo].sif2k_recibos
	(
		[NUM_CLIENTE] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sif2k_recibos]') AND name = N'IX_sif2k_recibos_CODIGO')
	CREATE NONCLUSTERED INDEX IX_sif2k_recibos_CODIGO ON [dbo].sif2k_recibos
	(
		[CODIGO] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	


	CREATE TABLE dbo.sif2k_histVendas(
		prd_cod nvarchar (18) NULL,
		ano_mes nvarchar(8) NULL,
		qt_dispensas numeric(5) NULL,
		qt_compras numeric(7) NULL,
		ousrdata   datetime  NULL
	) ON [PRIMARY]

	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sif2k_histVendas]') AND name = N'IX_sif2k_histVendas_prd_cod')
	CREATE NONCLUSTERED INDEX IX_sif2k_histVendas_prd_cod ON [dbo].sif2k_histVendas
	(
		prd_cod ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
	
	ALTER TABLE sif2k_histVendas ADD CONSTRAINT DF_sif2k_histVendas_ousrdata DEFAULT GETDATE() FOR ousrdata

	

	CREATE TABLE dbo.sif2k_movimentos_prd(
		prd_cod nvarchar (18) NULL,
		qt_dispensas numeric(5) NULL,
		qt_compras numeric(7) NULL,
		dataMov nvarchar (18) NULL,
	) ON [PRIMARY]

	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sif2k_movimentos_prd]') AND name = N'IX_sif2k_movimentos_prd_cod')
	CREATE NONCLUSTERED INDEX IX_sif2k_movimentos_prd_cod ON [dbo].sif2k_movimentos_prd
	(
		prd_cod ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	




	
	CREATE TABLE [dbo].[sif2k_vendas](
		[IDT] [nvarchar](30) NULL,
		[DATAHORA] [nvarchar](30) NULL,
		[NUM_VENDA] [nvarchar](20) NULL,
		[NUM_DOC] [nvarchar](20) NULL,
		[NUM_CLIENTE] [nvarchar](20) NULL,
		[NIF_UTENTE] [nvarchar](20) NULL,
		[CODIGO] [nvarchar](30) NULL,
		[DESIGNACAO] [nvarchar](220) NULL,
		[IVA] [numeric](7,2) NULL,
		[QT] [numeric](6, 0) NULL,
		[QT_FALTA] [numeric](6, 0) NULL,
		[PRECO] [numeric](13,3) NULL,
		[PRECO_ESC] [numeric](13,3) NULL,
		[TOTAL] [numeric](13,3) NULL,
		[TOTAL_ESC] [numeric](13,3) NULL,
		[REGULAR] [nvarchar](30) NULL,
		[CREDITADO] [numeric](13,3) NULL,
		[CREDITADO_ESC] [numeric](13,3) NULL,
		[PAGO] [numeric](13,3) NULL,
		[SUM_QT] [numeric](5, 0) NULL,
		[SUM_QT_FALTA] [numeric](5, 0) NULL,
		[SUM_TOTAL] [numeric](13,3) NULL,
		[SUM_TOTAL_ESC] [numeric](13,3) NULL,
		[SUM_PAGO] [numeric](13,3) NULL,
		[SUM_PAGO_ESC] [numeric](13,3) NULL,
		[SUM_CREDITADO] [numeric](13,3) NULL,
		[SUM_CREDITADO_ESC] [numeric](13,3) NULL,
		[UTILIZADOR] [nvarchar](100) NULL,
		[TIPO] [nvarchar](10) NULL
	) ON [PRIMARY]


	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sif2k_vendas]') AND name = N'IX_sif2k_vendas_NUM_CLIENTE')
	CREATE NONCLUSTERED INDEX IX_sif2k_vendas_NUM_CLIENTE ON [dbo].sif2k_vendas
	(
		[NUM_CLIENTE] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sif2k_vendas]') AND name = N'IX_sif2k_vendas_CODIGO')
	CREATE NONCLUSTERED INDEX IX_sif2k_vendas_CODIGO ON [dbo].sif2k_vendas
	(
		[CODIGO] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	



	CREATE TABLE [dbo].[sif2k_devolucoes](
			DT_GUIA varchar(30) NULL
			,NUM_GUIA varchar(20) NULL
			,SIT varchar(2) NULL
			,CODPROD varchar(18) NULL
			,DSPNOMEPROD varchar(220) NULL
			,QT numeric(5,0) NULL
			,QT_FALTA numeric(7,0) NULL
			,PRECO numeric(13,3) NULL
			,TAXA_IVA numeric(6,2) NULL
			,MOT_DEVOLUCAO varchar(100) NULL
			,NUM_DOC_ORIGEM varchar(20) NULL
			,TIPOVENDA varchar(2) NULL
			,NUM_FORNECEDOR varchar(10) NULL
			,NOMEFORNECEDOR varchar(220) NULL
			,MORADAFORNECEDOR varchar(250) NULL
			,FARMACIAID varchar(10) NULL
	
		) ON [PRIMARY]



		IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sif2k_devolucoes]') AND name = N'IX_sif2k_devolucoes_CODPROD')
		CREATE NONCLUSTERED INDEX sif2k_devolucoes_CODPROD ON [dbo].sif2k_devolucoes
		(
			[CODPROD] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
		


		CREATE TABLE [dbo].[sif2k_prod_alternativo](
			cpr varchar(255) NULL
			,cod varchar(255) NULL
	
		) ON [PRIMARY]

		IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sif2k_prod_alternativo]') AND name = N'IX_sif2k_prod_alternativo_cpr')
		CREATE NONCLUSTERED INDEX IX_sif2k_prod_alternativo_cpr ON [dbo].sif2k_prod_alternativo
		(
			[cpr] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	





END



Go
Grant Execute on dbo.up_migracao_sifarma_tables to Public
Grant control on dbo.up_migracao_sifarma_tables to Public
Go








