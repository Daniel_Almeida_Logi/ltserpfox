/* SP Cancel Compart
D. Almeida, 2021-06-08
--------------------------------
valida se pode cancelar a comparticipacao
Retorna valores de comparticipacao


up_msb_validateCancelCompart 'e843304f-9469-496d-9ce2-03fc4664b07','14230','1','100'



*/
	
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO






IF OBJECT_ID('[dbo].[up_msb_validateCancelCompart]') IS NOT NULL
		DROP PROCEDURE [dbo].up_msb_validateCancelCompart ;
GO




CREATE PROCEDURE [dbo].up_msb_validateCancelCompart	
		 @contributionCorrelationId		VARCHAR(100)
		 ,@pharmacyCode		            VARCHAR(18)
		 ,@test                         bit
		 ,@receiverId                   VARCHAR(10)
		  


	
		 		 
	/* WITH ENCRYPTION */
AS

SET NOCOUNT ON
	
	SET @contributionCorrelationId  =   left(rtrim(ltrim(isnull(@contributionCorrelationId,''))),100)
	SET @pharmacyCode				=   left(rtrim(ltrim(isnull(@pharmacyCode,''))),10)
	set @test                       =   isnull(@test,0)
	SET @receiverId                 =   left(rtrim(ltrim(isnull(@receiverId,''))),10)

	declare @respStamp varchar(36) = ''
	declare @card varchar(36) = ''

	if(ISNUMERIC(@pharmacyCode)=0)
	begin
		select 409 as code , "Bad Request Invalid Phamacy" as descr
		return
	end




	select 
		top 1
		@respStamp=rtrim(ltrim(isnull(ext_compart_response.stamp,''))), 
		@card=rtrim(ltrim(isnull(ext_compart_request.cardCode,''))) 
	from ext_compart_response(nolock) 
	inner join ext_compart_request(nolock) on  ext_compart_request.stamp = ext_compart_response.stampRequest
	where 
		ext_compart_response.contributionCorrelationId = @contributionCorrelationId 
		and ext_compart_response.test = @test
		and ext_compart_response.contributionCorrelationId!=''
		and ext_compart_response.anulationReqStamp=''
		and ext_compart_request.reqType = 2
		and ext_compart_response.code = 200
		and ext_compart_request.id_ext_efr = @receiverId
		and convert(int,ext_compart_request.id_ext_pharmacy) = convert(int,@pharmacyCode)
	

	if(isnull(@respStamp,'')='')
	begin
		select 409  as code, "Bad request Invalid Correlationid" as descr
		return 
	end


	select 200  as code, @card  as descr




	


GO
GRANT EXECUTE on dbo.up_msb_validateCancelCompart TO PUBLIC
GRANT Control on dbo.up_msb_validateCancelCompart TO PUBLIC
GO




