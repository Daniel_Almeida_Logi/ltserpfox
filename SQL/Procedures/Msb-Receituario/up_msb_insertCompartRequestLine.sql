/* SP View Prescrition (Insert)
D. Almeida, 2021-06-08
--------------------------------
up_msb_insertCompartRequestLine
SP para gravar logs de pedidos de comparticipacao

exec up_msb_insertCompartRequestLine '8c434749-8cb3-4eae-b580-717efeccbff', 'ADMDDDDIGGG', 2, 15.0 , '2494086', '','',4,6.0,0.0,0;

*/
	
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO




IF OBJECT_ID('[dbo].[up_msb_insertCompartRequestLine]') IS NOT NULL
		DROP PROCEDURE [dbo].up_msb_insertCompartRequestLine ;
GO


CREATE PROCEDURE [dbo].up_msb_insertCompartRequestLine	
		  @reqStamp			            VARCHAR(36)
		 ,@token			            VARCHAR(100)		
		 ,@lineId			            VARCHAR(100)
		 ,@pvp		                    DECIMAL(19,6)
		 ,@productNhrn			        VARCHAR(100)
		 ,@prescriptionExceptions 		VARCHAR(MAX)
		 ,@prescriptionDispatch 		VARCHAR(MAX)
		 ,@qtt                          decimal(19,2)
		 ,@snsValue                     decimal(19,2)
		 ,@vatRate                      decimal(19,2)
		 ,@vatValue                     decimal(19,2)
	 



		
		 		 
	/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

SET @reqStamp		        =   left(rtrim(ltrim(isnull(@reqStamp,''))),36)
SET @token		            =   left(rtrim(ltrim(isnull(@token,''))),100)
SET @lineId			        =   left(rtrim(ltrim(isnull(@lineId,''))),100)
SET @productNhrn		    =   left(rtrim(ltrim(isnull(@productNhrn,''))),100)
SET @prescriptionExceptions	=   left(rtrim(ltrim(isnull(@prescriptionExceptions,''))),100)
SET @prescriptionDispatch	=   left(rtrim(ltrim(isnull(@prescriptionDispatch,''))),100)
SET @qtt	                =   isnull(@qtt,0)
SET @snsValue	            =   isnull(@snsValue,0)
SET @vatRate	            =   isnull(@vatRate,0)
SET @vatValue	            =   isnull(@vatValue,0)


		
INSERT INTO [dbo].[ext_compart_request_l]
           ([stamp]
           ,[stampReq]
           ,[lineNumber]
           ,[pvpValue]
           ,[prescriptionExceptions]
           ,[prescriptionDispatch]
           ,[productCode]
           ,[quantity]
           ,[snsValue]
           ,[vatRate]
           ,[vatValue]
           ,[token])
     VALUES
           (left(NEWID(),36)
           ,@reqStamp
           ,@lineId
           ,@pvp
           ,@prescriptionExceptions
           ,@prescriptionDispatch
           ,@productNhrn
           ,@qtt
           ,@snsValue
           ,@vatRate
           ,@vatValue
           ,@token
           )
	



GO
GRANT EXECUTE on dbo.up_msb_insertCompartRequestLine TO PUBLIC
GRANT Control on dbo.up_msb_insertCompartRequestLine TO PUBLIC
GO