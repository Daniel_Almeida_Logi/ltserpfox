/* SP View Prescrition (Insert)
D. Almeida, 2020-05-18
--------------------------------
up_msb_viewInsertPrescriptionLine
SP para gravar logs de consulta de cada lina da receita no msb

 exec up_msb_viewInsertPrescriptionLine 'ADMDDDDIGGG', '101100000275861010032', '', '50036432' , '2020-02-14T15:05:19.000Z'; 

*/
	
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO




IF OBJECT_ID('[dbo].[up_msb_viewInsertPrescriptionLine]') IS NOT NULL
		DROP PROCEDURE [dbo].up_msb_viewInsertPrescriptionLine ;
GO


CREATE PROCEDURE [dbo].up_msb_viewInsertPrescriptionLine	
		  @token			            VARCHAR(100)		
		 ,@lineId			            VARCHAR(100)
		 ,@productNhrn			        VARCHAR(20)		
		 ,@productCnpem			        VARCHAR(20)
		 ,@expDate			            VARCHAR(100)		 



		
		 		 
	/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

SET @token		            =   left(rtrim(ltrim(isnull(@token,''))),39)
SET @lineId			        =   left(rtrim(ltrim(isnull(@lineId,''))),26)
SET @productNhrn		    =   left(rtrim(ltrim(isnull(@productNhrn,''))),7)
SET @productCnpem	        =   left(rtrim(ltrim(isnull(@productCnpem,''))),8)
SET @expDate		        =   rtrim(ltrim(isnull(@expDate,'')))

	

		
INSERT INTO [dbo].[Dispensa_Eletronica_D]
           ([token]
           ,[id]
           ,[medicamento_cod]
           ,[medicamento_cnpem]
           ,[data_caducidade]
		   )

     VALUES
           (@token
           ,@lineId
           ,@productNhrn
           ,@productCnpem
           ,@expDate
        )
	

	



GO
GRANT EXECUTE on dbo.up_msb_viewInsertPrescriptionLine TO PUBLIC
GRANT Control on dbo.up_msb_viewInsertPrescriptionLine TO PUBLIC
GO