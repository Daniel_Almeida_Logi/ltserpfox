/* SP View Prescrition (Insert)
D. Almeida, 2020-05-18
--------------------------------
up_msb_viewInsertPrescriptionHeader
SP para grvar logs de consulta de receita no msb

exec up_msb_viewInsertPrescriptionHeader 'ADMDDDDIGGG', '99001', 'lts',  '' , '', '1011000002758610101', '782230', '8439', '[100003010001] Pedido processado com sucesso.', '2020-02-14T15:05
:19.000Z', '999999999'

*/
	
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO




IF OBJECT_ID('[dbo].[up_msb_viewInsertPrescriptionHeader]') IS NOT NULL
		DROP PROCEDURE [dbo].up_msb_viewInsertPrescriptionHeader ;
GO


CREATE PROCEDURE [dbo].up_msb_viewInsertPrescriptionHeader			
		  @token			            VARCHAR(254)
		 ,@senderId				        VARCHAR(254)		
		 ,@entityId			            VARCHAR(10)
		 ,@entityVat			        VARCHAR(200)		 
		 ,@benefNr			            VARCHAR(200)
		 ,@prescriptionId			    VARCHAR(200)
		 ,@acessPin				        VARCHAR(20)
		 ,@optionPin				    VARCHAR(20)
		 ,@resultView				    VARCHAR(4000)
		 ,@prescriptionDate				VARCHAR(50)
		 ,@senderVat				    VARCHAR(20)
		 ,@senderName				    VARCHAR(100)
		 ,@receiverId				    VARCHAR(100)
		 ,@receiverName				    VARCHAR(100)



		
		 		 
	/* WITH ENCRYPTION */
AS

SET NOCOUNT ON



SET @token		        =   left(rtrim(ltrim(isnull(@token,''))),39)
SET @senderId			=   left(rtrim(ltrim(isnull(@senderId,''))),39)
SET @entityVat		    =   left(rtrim(ltrim(isnull(@entityVat,''))),39)
SET @entityId	        =   left(rtrim(ltrim(isnull(@entityId,''))),39)
SET @senderVat		    =   left(rtrim(ltrim(isnull(@senderVat,''))),39)
SET @prescriptionId		=   left(rtrim(ltrim(isnull(@prescriptionId,''))),19)
SET @acessPin	        =   left(rtrim(ltrim(isnull(@acessPin,''))),19)
SET @optionPin			=   left(rtrim(ltrim(isnull(@optionPin,''))),19)
SET @resultView			=   left(rtrim(ltrim(isnull(@resultView,''))),199)
SET @prescriptionDate	=   rtrim(ltrim(isnull(@prescriptionDate,'')))
SET @benefNr            =   left(rtrim(ltrim(isnull(@benefNr,''))),19)
set @receiverName	    =   left(rtrim(ltrim(isnull(@receiverName,''))),29)




		INSERT INTO [dbo].[Prescription_request]
           ([token]
           ,[senderId]
           ,[senderVat]
           ,[entityId]
           ,[entityVat]
           ,[benefNr]
           ,[prescriptionId]
           ,[acessPin]
           ,[optionPin]
		   ,[senderName]
		   ,[receiverId]
		   ,[receiverName]
           )
     VALUES
           (@token
           ,@senderId
		   ,@senderVat
           ,@entityId		   
           ,@entityVat
		   ,@benefNr
           ,@prescriptionId
           ,@acessPin
           ,@optionPin
		   ,@senderName	
		   ,@receiverId	
		   ,@receiverName	
           )


	
	    INSERT INTO [dbo].[Dispensa_Eletronica]
			   ([token]
			   ,[resultado_consulta_descr]
			   ,[receita_data]
			   ,[receita_nr])
		 VALUES
			   (
			   @token
			   ,@resultView
			   ,@prescriptionDate
			   ,@prescriptionId
			   )
	





GO
GRANT EXECUTE on dbo.up_msb_viewInsertPrescriptionHeader TO PUBLIC
GRANT Control on dbo.up_msb_viewInsertPrescriptionHeader TO PUBLIC
GO