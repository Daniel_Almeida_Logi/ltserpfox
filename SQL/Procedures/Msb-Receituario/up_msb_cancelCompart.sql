/* SP Cancel Compart
D. Almeida, 2021-06-08
--------------------------------
up_msb_cancelCompart
Retorna valores de comparticipacao


up_msb_cancelCompart '6bd4674a-474c-4393-a8aa-28355a22414','99001','0','abcd','100'



*/
	
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO






IF OBJECT_ID('[dbo].[up_msb_cancelCompart]') IS NOT NULL
		DROP PROCEDURE [dbo].up_msb_cancelCompart ;
GO




CREATE PROCEDURE [dbo].up_msb_cancelCompart	
		 @contributionCorrelationId		VARCHAR(100)
		 ,@pharmacyCode		            VARCHAR(18)
		 ,@test                         bit
		 ,@reqStamp                     VARCHAR(36)
		 ,@receiverId                   VARCHAR(10)
		  


		 


	
		 		 
	/* WITH ENCRYPTION */
AS

SET NOCOUNT ON
	
	SET @contributionCorrelationId  =   left(rtrim(ltrim(isnull(@contributionCorrelationId,''))),100)
	SET @pharmacyCode				=   left(rtrim(ltrim(isnull(@pharmacyCode,''))),10)
	set @test                       =   isnull(@test,0)
	SET @reqStamp                   =   left(rtrim(ltrim(isnull(@reqStamp,''))),36)
	SET @receiverId                 =   left(rtrim(ltrim(isnull(@receiverId,''))),10)

	declare @respStamp varchar(36) = ''

	if(ISNUMERIC(@pharmacyCode)=0)
	begin
		select 409 as code , "Bad Request Invalid Phamacy" as descr
		return
	end




	select 
		top 1
		@respStamp=rtrim(ltrim(isnull(ext_compart_response.stamp,''))) 
	from ext_compart_response(nolock) 
	inner join ext_compart_request(nolock) on  ext_compart_request.stamp = ext_compart_response.stampRequest
	where 
		ext_compart_response.contributionCorrelationId = @contributionCorrelationId 
		and ext_compart_response.test = @test
		and ext_compart_response.contributionCorrelationId!=''
		and ext_compart_response.anulationReqStamp=''
		and ext_compart_request.reqType = 2
		and ext_compart_response.code = 200
		and ext_compart_request.id_ext_efr = @receiverId
		and convert(int,ext_compart_request.id_ext_pharmacy) = convert(int,@pharmacyCode)
	

	if(isnull(@respStamp,'')='')
	begin
		select 409  as code, "Bad request Invalid Correlationid" as descr
		return 
	end


	update ext_compart_response set anulationReqStamp = @reqStamp where stamp = @respStamp

	declare @count int = 0
	select @count = count(*) from ext_compart_response where stamp = @respStamp and anulationReqStamp = @reqStamp

	if(isnull(@count,0)>0)
		select 200  as code, "OK" as descr
	else
		select 500  as code, "Internal Server Error" as descr


	


GO
GRANT EXECUTE on dbo.up_msb_cancelCompart TO PUBLIC
GRANT Control on dbo.up_msb_cancelCompart TO PUBLIC
GO




