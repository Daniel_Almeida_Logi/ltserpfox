/* SP Validate Request Compart 
D. Almeida, 2021-06-08
--------------------------------
up_msb_getCompartValues
Retorna valores de comparticipacao


up_msb_getCompartValues '100','1888888','1','1'


*/
	
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO






IF OBJECT_ID('[dbo].[up_msb_getCompartValues]') IS NOT NULL
		DROP PROCEDURE [dbo].up_msb_getCompartValues ;
GO




CREATE PROCEDURE [dbo].up_msb_getCompartValues	
		 @receiverId				    VARCHAR(100)
		 ,@productNhrn		            VARCHAR(18)
		 ,@qtt                          decimal(9,2)
		 ,@pvp                          decimal(19,6)


		 


	
		 		 
	/* WITH ENCRYPTION */
AS

SET NOCOUNT ON


		SET @receiverId				    =   left(rtrim(ltrim(isnull(@receiverId,''))),36)
		SET @productNhrn			    =   left(rtrim(ltrim(isnull(@productNhrn,''))),36)
		SET @qtt			            =   round(isnull(@qtt,1),2)
		SET @pvp			            =   round(isnull(@pvp,0),2)

		declare @compartValue   decimal(19,2)  = 0.00
		declare @compartPerc    decimal(9,2)   = 0.00   
		declare @contaPvp int = 0

		declare @unitPrice decimal(10,2) = round(round(@pvp,2)/round(@qtt,2),2)


		select 
			@contaPvp = count(*) from hist_precos(nolock) 
		where 
			ref = @productNhrn  
			and round(preco,2) = round(@unitPrice,2)  
			and id_preco = 1 
			and (isnull(data_fim,'3000-01-01')>=getdate() or data_fim='1900-01-01')
		
		if(isnull(@contaPvp,0)=0)
		begin
			select 0.00 as compartValue, 0.00 as compartPerc
			return
		end	
		
		
		select 
		 top 1	
			@compartValue = case when grppreco = 'pvp' then  isnull(round(round(cptvalext.compart,2) * @qtt,2),0) end
		from 
			fprod(nolock)
		inner join 
			cptvalext on  fprod.grupo = cptvalext.id_cpt_grp
		where	
			 fprod.ref = @productNhrn  

		if(isnull(@compartValue,0)<=0)
			set @compartValue = 0.00
		


		set @compartPerc = (@compartValue /round((@pvp),2)) * 100


		if(isnull(@compartPerc,0)<=0)
			set @compartPerc = 0.00

		if(isnull(@compartPerc,0)>=100)
			set @compartPerc = 100.00
	

		select @compartValue as compartValue, @compartPerc as compartPerc


GO
GRANT EXECUTE on dbo.up_msb_getCompartValues TO PUBLIC
GRANT Control on dbo.up_msb_getCompartValues TO PUBLIC
GO