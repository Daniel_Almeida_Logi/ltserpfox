/* SP Validate Request Compart Card
D. Almeida, 2022-09-19
--------------------------------
up_msb_validateRequestCard
Valida cartoes de comparticipação
cartao
codigoEntidade
test


exec up_msb_validateRequestCard '171871681', '109', '0', 12;

 up_msb_validateRequestCard


*/
	
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO




IF OBJECT_ID('[dbo].[up_msb_validateRequestCard]') IS NOT NULL
		DROP PROCEDURE [dbo].up_msb_validateRequestCard ;
GO




CREATE PROCEDURE [dbo].up_msb_validateRequestCard	
		  @card			                VARCHAR(100)
		 ,@receiverId				    VARCHAR(36)
		 ,@test                         bit = 0
		 ,@monthGap                     int = 10

		 		 
	/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

set @monthGap = -6
--set @monthGap = ISNULL(@monthGap,10) * -1

		declare @conta int = 0

		if(LEN(@card)!=9 or ISNUMERIC(@card)=0)
		begin
			select 409 as code  , "Card Invalid" as descr
			return
		end
		
		


		select 
			@conta = COUNT(*)
		from ext_compart_request(nolock)
		inner join ext_compart_response (nolock ) on ext_compart_request.stamp=ext_compart_response.stampRequest
		where 
			cardCode = @card
			and id_ext_efr = @receiverId
			and date>=DATEADD(month, @monthGap ,GETDATE())
			and reqType = 2 and ext_compart_request.test = @test
			and ext_compart_response.contributionCorrelationId!='' and ext_compart_response.anulationReqStamp=''
		
		

			 
		if(@conta=0)
			select 200 as code  , "OK" as descr
		else
			select 409 as code  , "Card Already Used" as descr



			 


GO
GRANT EXECUTE on dbo.up_msb_validateRequestCard TO PUBLIC
GRANT Control on dbo.up_msb_validateRequestCard TO PUBLIC
GO