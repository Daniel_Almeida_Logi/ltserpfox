/* SP Insert Request Header (Insert)
D. Almeida, 2021-06-08
--------------------------------
up_msb_insertCompartRequestHeader
SP para gravar logs de consulta de pedidos de comparticipacao

 exec up_msb_insertCompartRequestHeader '0548f559-2dfb-41c5-9300-8bc37d3af9e', 'ADMDDDD1IGGG1', '234230', '3' , 'Anula', '','23423','','4d6c2df0-7cf1-4f03-88d9-37c60d219
673','lts-pharmacy','999999999','savida','014','LTS','lts-1.0.0','lts','1','999999999';



*/
	
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO




IF OBJECT_ID('[dbo].[up_msb_insertCompartRequestHeader]') IS NOT NULL
		DROP PROCEDURE [dbo].up_msb_insertCompartRequestHeader ;
GO


CREATE PROCEDURE [dbo].up_msb_insertCompartRequestHeader	
		  @stamp			            VARCHAR(36)		
		 ,@token			            VARCHAR(100)
		 ,@senderId				        VARCHAR(50)	
		 ,@reqType	                    int
		 ,@reqDescr	                    VARCHAR(254)
		 ,@cardCode                     VARCHAR(254)
		 ,@pharmacyCode                 VARCHAR(10)
		 ,@saleCorrelationId            VARCHAR(100)
		 ,@contributionCorrelationId    VARCHAR(100)
		 ,@senderName				    VARCHAR(255)
		 ,@senderVat				    VARCHAR(20)
		 ,@receiverName				    VARCHAR(254)
		 ,@receiverId				    VARCHAR(10)
		 ,@entityName			        VARCHAR(254)
		 ,@entityApp			        VARCHAR(254)
		 ,@entityId			            VARCHAR(50)			 		 
		 ,@test			                bit
		 ,@receiverCode                 VARCHAR(200)	

		 



		
		 		 
	/* WITH ENCRYPTION */
AS

SET NOCOUNT ON


SET @stamp						=   left(rtrim(ltrim(isnull(@stamp,''))),36)
SET @token						=   left(rtrim(ltrim(isnull(@token,''))),100)
SET @senderId					=   left(rtrim(ltrim(isnull(@senderId,''))),50)
set @reqType					=   isnull(@reqType,0)
set @reqDescr					=   left(rtrim(ltrim(isnull(@reqDescr,''))),254)
set @cardCode					=   left(rtrim(ltrim(isnull(@cardCode,''))),254)
set @pharmacyCode				=   left(rtrim(ltrim(isnull(@pharmacyCode,''))),10)
set @saleCorrelationId			=   left(rtrim(ltrim(isnull(@saleCorrelationId,''))),100)
set @contributionCorrelationId  =   left(rtrim(ltrim(isnull(@contributionCorrelationId,''))),100)
set @senderName                 =   left(rtrim(ltrim(isnull(@senderName,''))),255)
set @senderVat                  =   left(rtrim(ltrim(isnull(@senderVat,''))),20)
set @receiverName               =   left(rtrim(ltrim(isnull(@receiverName,''))),254)
set @receiverId                 =   left(rtrim(ltrim(isnull(@receiverId,''))),10)
set @receiverCode               =   left(rtrim(ltrim(isnull(@receiverCode,''))),200)
set @entityName                 =   left(rtrim(ltrim(isnull(@entityName,''))),10)
set @entityApp                  =   left(rtrim(ltrim(isnull(@entityApp,''))),10)
set @entityId                   =   left(rtrim(ltrim(isnull(@entityId,''))),10)
set @test                       =   isnull(@test,0)






		INSERT INTO [dbo].[ext_compart_request]
           ([stamp]
           ,[id_ext_pharmacy]
           ,[reqType]
           ,[reqDescr]
           ,[cardCode]
           ,[pharmacyCode]
           ,[saleCorrelationId]
           ,[contributionCorrelationId]
           ,[name_ext_pharmacy]
           ,[vatNr_ext_pharmacy]
           ,[name_ext_entity]
           ,[id_ext_efr]
		   ,[code_ext_entity]
           ,[name_ext_efs]
           ,[app_ext_efs]
           ,[id_ext_efs]
           ,[token]
           ,[test]
		   )
     VALUES
           (@stamp
           ,@senderId
           ,@reqType
           ,@reqDescr
           ,@cardCode
		   ,@pharmacyCode
           ,@saleCorrelationId
           ,@contributionCorrelationId
		   ,@senderName
           ,@senderVat
           ,@receiverName
           ,@receiverId
		   ,@receiverCode
           ,@entityName
           ,@entityApp
           ,@entityId
           ,@token
           ,@test
		   
		   
		   )
	


GO
GRANT EXECUTE on dbo.up_msb_insertCompartRequestHeader TO PUBLIC
GRANT Control on dbo.up_msb_insertCompartRequestHeader TO PUBLIC
GO