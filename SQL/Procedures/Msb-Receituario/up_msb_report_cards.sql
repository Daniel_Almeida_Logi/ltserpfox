/*	
	retorna dados de cart�es utilizados

    select * from ext_compart_request where reqtype= 2

	exec up_msb_report_cards  'E62C4A17-0AB9-4A85-A3B3-63D8C3007C8X','1900-01-01 00:00','3000-01-01 00:00',1

*/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_msb_report_cards]') IS NOT NULL
	drop procedure up_msb_report_cards
go


Create procedure [dbo].up_msb_report_cards
	 @apiKey               VARCHAR(50)
	 ,@dateInit             dateTime 
	 ,@dateEnd              dateTime 
	 ,@test                 bit
	 ,@pageNumber           int = 1


/* with encryption */
AS
	SET NOCOUNT ON

	if(isnull(@pageNumber,0)<1)
		set @pageNumber = 1	

	DECLARE @sql varchar(max)
	DECLARE @orderBy VARCHAR(max)
	


	declare @receiverId varchar(10) = ''

	declare @topMax int = 10000000

	set @apiKey               = rtrim(ltrim(isnull(@apiKey,'')))
	set @dateInit             = isnull(@dateInit,'1900-01-01 00:00')
	set @dateEnd              = isnull(@dateEnd,'3000-01-01 00:00')
	set @test                 = isnull(@test,0)


		
	set @orderBy = ' ) X 
				 ORDER BY date DESC
				 OFFSET ' + convert(varchar(10),@topMax) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
				 FETCH NEXT  ' + convert(varchar(10),@topMax) + ' ROWS ONLY '


	select 
		@receiverId=rtrim(ltrim(isnull(idAfp,'')))
	from [10.100.200.16].E01322A.dbo.ext_esb_api
	where apiKey = @apiKey



	set @receiverId = isnull(@receiverId,'')

	if(@receiverId='')
		return
	

	select @sql = N' 				
				SELECT '  + convert(varchar(10),@topMax) +' as pageSize,  (COUNT(*) OVER() / ' +  convert(varchar(10),@topMax) +') + 1 as pageTotal, '
					+ convert(varchar(10),@PageNumber) +' as pageNumber, *
				FROM (
					
					select 
						''card'' = rtrim(ltrim(isnull(ext_compart_request.cardCode,'''')))
						,''pharmacyCode'' = id_ext_pharmacy
						,''date'' = convert(varchar,ext_compart_request.date,20) 
						,''cancelled'' = case when isnull(ext_compart_response.anulationReqStamp,'''') != '''' then convert(bit,1) else convert(bit,0) end
					from ext_compart_response(nolock) 
					inner join ext_compart_request(nolock) on  ext_compart_request.stamp = ext_compart_response.stampRequest
					where 
						 ext_compart_response.test = ' + rtrim(ltrim(STR(@test))) + '
						and ext_compart_response.contributionCorrelationId!=''''
						and ext_compart_request.reqType = 2
						and ext_compart_response.code = 200
						and ext_compart_request.id_ext_efr = ''' + @receiverId + '''
						and ext_compart_request.date >= ''' +  convert(varchar,@dateInit,20) + ''' and ext_compart_request.date<= ''' + convert(varchar,@dateEnd,20) +'''
					'
	

	select @sql = @sql + @orderBy
	print @sql
	EXECUTE (@sql)


GO
Grant Execute On up_msb_report_cards to Public
Grant Control On up_msb_report_cards to Public
GO

