/* SP Insert Request Header (Insert)
D. Almeida, 2021-06-08
--------------------------------
up_msb_insertCompartResponseHeader
SP para gravar logs de consulta das respostas de comparticipacao



 exec up_msb_insertCompartResponseHeader '85345f78-f0ed-4740-86f7-1ba5077eb87', 'a8593647-cd4e-4225-858f-7148dd12037', '', '', '', '0', 409, 'Linha: (0) : S502 - N�mero
de utente inv�lido; ';



*/
	
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO




IF OBJECT_ID('[dbo].[up_msb_insertCompartResponseHeader]') IS NOT NULL
		DROP PROCEDURE [dbo].up_msb_insertCompartResponseHeader ;
GO


CREATE PROCEDURE [dbo].up_msb_insertCompartResponseHeader	
		  @stamp			            VARCHAR(36)
		 ,@reqStamp			            VARCHAR(36)		
		 ,@saleCorrelationId			VARCHAR(100)	
		 ,@trackingId	                VARCHAR(100)	
		 ,@contributionCorrelationId    VARCHAR(254)
		 ,@test                         Bit
		 ,@code                         VARCHAR(36)
		 ,@descr                        VARCHAR(254)
		


		
		 		 
	/* WITH ENCRYPTION */
AS

SET NOCOUNT ON


SET @stamp						=   left(rtrim(ltrim(isnull(@stamp,''))),36)
SET @reqStamp					=   left(rtrim(ltrim(isnull(@reqStamp,''))),36)
SET @saleCorrelationId			=   left(rtrim(ltrim(isnull(@saleCorrelationId,''))),100)
SET @trackingId					=   left(rtrim(ltrim(isnull(@trackingId,''))),100)
SET @contributionCorrelationId	=   left(rtrim(ltrim(isnull(@contributionCorrelationId,''))),100)
SET @test	                    =   isnull(@test,0)
SET @code	                    =   left(rtrim(ltrim(isnull(@code,''))),36)
SET @descr	                    =   left(rtrim(ltrim(isnull(@descr,''))),254)






		INSERT INTO [dbo].[ext_compart_response]
           ([stamp]
           ,[stampRequest]
           ,[saleCorrelationId]
           ,[trackingId]
           ,[contributionCorrelationId]
           ,[test]
		   ,[code]
		   ,[descr]
		   )
     VALUES
           (@stamp
           ,@reqStamp
           ,@saleCorrelationId
           ,@trackingId
           ,@contributionCorrelationId
           ,@test
		   ,@code
		   ,@descr
           )
	


GO
GRANT EXECUTE on dbo.up_msb_insertCompartResponseHeader TO PUBLIC
GRANT Control on dbo.up_msb_insertCompartResponseHeader TO PUBLIC
GO