/* SP Validate Request Compart 
D. Almeida, 2021-06-08
--------------------------------
up_msb_validateRequestCompart
Valida condicoes de comparticipação
Pvp
Codigo infarmed da farmacia
cnp

 exec up_msb_validateRequestCompart '1.00','11983', '109','7261487 ',1


 exec up_msb_validateRequestCompart 12.0, '99001', 'lts', '2494086',2; 

 
*/

 

	
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO




IF OBJECT_ID('[dbo].[up_msb_validateRequestCompart]') IS NOT NULL
		DROP PROCEDURE [dbo].up_msb_validateRequestCompart ;
GO




CREATE PROCEDURE [dbo].up_msb_validateRequestCompart	
		  @pvp			                decimal(10,2)	
		 ,@pharmacyCode			        VARCHAR(36)
		 ,@receiverId				    VARCHAR(36)
		 ,@productNhrn		            VARCHAR(18)
		 ,@qtt                          decimal(9,2)


		 


	
		 		 
	/* WITH ENCRYPTION */
AS

SET NOCOUNT ON


		SET @productNhrn				=   left(rtrim(ltrim(isnull(@productNhrn,''))),18)
		SET @pvp			            =   isnull(@pvp,0)
		SET @pharmacyCode			    =   left(rtrim(ltrim(isnull(@pharmacyCode,''))),36)
		SET @receiverId			        =   left(rtrim(ltrim(isnull(@receiverId,''))),36)
		SET @qtt			            =   isnull(@qtt,1)

		

	
		declare @contaCodePh int  = 0
		declare @contaCodeEntity int  = 0
		declare @contaRef int  = 0


		if(@qtt<=0)
		begin
			select 409 as code , "Bad Request Invalid Quantity" as descr
			return
		end


		declare @unitPrice decimal(10,2) = round(round(@pvp,2)/round(@qtt,2),2)
	

		if(ISNUMERIC(@pharmacyCode)=0)
		begin
			select 409 as code , "Bad Request Invalid Phamacy" as descr
			return
		end



		select @contaCodePh = count(*) from b_utentes(nolock) where  ISNUMERIC(id) = 1 and convert(int,id) = convert(int,@pharmacyCode) and inactivo = 0 and id!='' and tipo = 'Farmácia' 

		if(isnull(@contaCodePh,0)=0)
		begin
			select 409  as code, "Bad Request Invalid Phamacy" as descr
			return
		end



		select @contaCodeEntity = count(*) from ext_compart_entity_org(nolock) where codOrg = @receiverId and inactive = 0

		if(isnull(@contaCodeEntity,0)=0)
		begin
			select 409  as code , "Bad Request Invalid Entity" as descr
			return
		end



				
		select 
		  @contaRef = COUNT(*)
		from 
			fprod(nolock)
		inner join 
			cptvalext on  fprod.grupo = cptvalext.id_cpt_grp
		where	
			 fprod.ref = @productNhrn and cptvalext.id_cpt_org = @receiverId
	
	    if(isnull(@contaRef,0)=0)
		begin
			select 409  as code , "Bad Request Invalid Product Nhrn: " + @productNhrn as descr
			return
		end


		select 200 as code  , "OK" as descr


GO
GRANT EXECUTE on dbo.up_msb_validateRequestCompart TO PUBLIC
GRANT Control on dbo.up_msb_validateRequestCompart TO PUBLIC
GO