/* SP Insert Response Header (Insert)
D. Almeida, 2021-06-08
--------------------------------
up_msb_insertCompartResponseLine
SP para gravar logs de consulta das respostas de comparticipacao

 exec up_msb_insertCompartResponseLine '7a23f4ba-8765-4ba2-829f-2d974310a92', '5440987', 2.84, 32.0 , 2;

*/
	
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO




IF OBJECT_ID('[dbo].[up_msb_insertCompartResponseLine]') IS NOT NULL
		DROP PROCEDURE [dbo].up_msb_insertCompartResponseLine ;
GO


CREATE PROCEDURE [dbo].up_msb_insertCompartResponseLine	
		  @respStamp			      VARCHAR(36)		
		 ,@productNhrn			      VARCHAR(50)	
		 ,@contributedValue	          decimal(19,6)
		 ,@contributedPerc            decimal(19,6)
		 ,@lineNumber                 VARCHAR(50)
		


		
		 		 
	/* WITH ENCRYPTION */
AS

SET NOCOUNT ON


SET @respStamp					=   left(rtrim(ltrim(isnull(@respStamp,''))),36)
SET @productNhrn				=   left(rtrim(ltrim(isnull(@productNhrn,''))),50)
SET @contributedValue			=   isnull(@contributedValue,0)
SET @contributedPerc			=   isnull(@contributedPerc,0)
SET @lineNumber              	=   left(rtrim(ltrim(isnull(@lineNumber,''))),50)




	INSERT INTO [dbo].[ext_compart_response_l]
           ([stamp]
           ,[stampResponse]
           ,[productCode]
           ,[contributedValue]
           ,[contributedPercentage]
           ,[lineNumber])
     VALUES
           (left(newid(),36)
           ,@respStamp
           ,@productNhrn
           ,@contributedValue
           ,@contributedPerc
           ,@lineNumber)


GO
GRANT EXECUTE on dbo.up_msb_insertCompartResponseLine TO PUBLIC
GRANT Control on dbo.up_msb_insertCompartResponseLine TO PUBLIC
GO