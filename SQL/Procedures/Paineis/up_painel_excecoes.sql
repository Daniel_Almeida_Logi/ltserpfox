/*
	FUNCAO:	Retorna dados para preencher o painel de exepcoes do atendimento
	SP:		up_painel_excecoes
	Data:	2020-01-16
	Autor:  Daniel Almeida
	
	select *from panel_exceptions
	
	

	Param Entrada:

		@dem int - se receita eletronica
			1 - DEM
			0 - PAPEL
		   -1 - TODAS 
		    2 - PEMH

		@prodType varchar(100) - classifica��o do producto

	Retorna lista de exepcoes para preencher o painel no atendimento


	-- exec up_painel_excecoes 1, '',1,'1','14'
	-- select * from [panel_exceptions]



*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_painel_excecoes]') IS NOT NULL
	drop procedure dbo.up_painel_excecoes
go

create procedure dbo.up_painel_excecoes

@dem int = -1,
@prodType varchar(100) = '',
@ModuleId int =1,
@Familia varchar(10) = '',
@tipoBonus varchar(10) = ''
	
/* WITH ENCRYPTION */
AS



BEGIN

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempPainel'))
	DROP TABLE #TempPainel
	
	select 
		"sel" = convert(bit,0)
		,"descr" =convert(varchar(254),pEx.descr) 
		,"stamp" = pEx.stamp
		,"code" = convert(varchar(254),pEx.code)		
		,"default" = pEx.[default]
		,"type" = pEx.[type]
		,"prescType" = pEx.prescType
		,"multiVisible" = pEx.multiVisible
		,"multiSel"     = pEx.multi_sel
	into 
		#TempPainel
	from 
		panel_exceptions pEx (nolock)
	
	where
		pEx.visible = 1 and pEx.moduleId=@ModuleId
		and pEx.prodType  = case when @prodType = '' then pEx.prodType else @prodType end
		and (pEx.prescType = 'TODOS' 
			 or pEx.prescType = (case 
									when @dem = -1 then pEx.prescType
									when @dem = 0 then 'PAPEL'
									when @dem = 1 then 'DEM'
									when @dem = 2 then 'PEMH'
								end)
			 )
		
		order by [type] desc, 	pEx.descr asc

	
	/* se producto nao for do infarmed, remover op��o indisponivel */
	if(isnull(@Familia,'')!='1' and isnull(@Familia,'')!='2' and isnull(@Familia,'')!='58')
		delete from  #TempPainel where type='INDISPONIVEL'


	delete from  #TempPainel where type='BONUS' and code!=@tipoBonus

	update  #TempPainel set sel = 1 where type='BONUS' and code=@tipoBonus
	
	/*devolver resultset*/
	select *from #TempPainel
		
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempPainel'))
	DROP TABLE #TempPainel
			
END
GO



Grant Execute On dbo.up_painel_excecoes to Public
Grant control On dbo.up_painel_excecoes to Public
GO


