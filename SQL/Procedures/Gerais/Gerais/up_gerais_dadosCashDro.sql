/*
	retorna informação do tpa para o terminal actual
	exec [dbo].[up_gerais_dadosCashDro] 32, 'Loja 1'

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_dadosCashDro]') IS NOT NULL
	drop procedure dbo.up_gerais_dadosCashDro
go

create procedure dbo.up_gerais_dadosCashDro
@tno integer 
,@site varchar(8) 

/* WITH ENCRYPTION */
AS

	select 
	 cashdro.stamp
	 ,cashdro.[address]
	 ,cashdro.[user]

	from 
		cashdro_map(nolock) cashdro
	inner join 
		B_Terminal(nolock) bt ON bt.tstamp = cashdro.termstamp
	WHERE
		bt.no=@tno 
		and site=@site



GO
Grant Execute On dbo.up_gerais_dadosCashDro to Public
Grant Control On dbo.up_gerais_dadosCashDro to Public
Go











