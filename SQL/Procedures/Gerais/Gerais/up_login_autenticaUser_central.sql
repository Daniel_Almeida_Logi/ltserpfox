/*

 exec up_login_autenticaUser_central 'Farm@L0g1', 'sa'
 exec up_login_autenticaUser 'Farm@L0g1', 'sa'
 exec up_login_autenticaUser_central 'Farm@L0g1', 'josecosta@logitools.p'
 
 select * from [user]
 select * from b_parameters where name like '%período%'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_login_autenticaUser_central]') IS NOT NULL
	drop procedure dbo.up_login_autenticaUser_central
go

create procedure dbo.up_login_autenticaUser_central
@pass varchar(30),
@user varchar(30)


/* WITH ENCRYPTION */ 
AS 
BEGIN 
	
	OPEN SYMMETRIC KEY UPWD_Key_01
	DECRYPTION BY CERTIFICATE userpassword;
	
	select 
		cast(name as varchar(100)) as name
		,pwpos=CONVERT(varchar(50), DecryptByKey([password]))
		,cast(id_bd as varchar(10)) as id_bd
		,cast(us_no as numeric(5,0)) as us_no
		,userno=1
		,iniciais='ADM'
		,grupo='Administrador'
		,nome='Administrador de Sistema'
		,vendnm='sa'

	from 
		[user] (nolock)
	where
		(([user].username = @user)
		or ([user].email = @user)
		or (cast([user].nif as varchar(30)) = @user)
		or (cast([user].telem as varchar(30)) = @user)
		or ([user].id_bd+cast([user].us_no as varchar(4)) = @user))
		and (CONVERT(varchar(50), DecryptByKey([password])) COLLATE Latin1_General_CS_AS) = (@pass COLLATE Latin1_General_CS_AS)
		--and us.userpass = case when @pass = '' then us.userpass else @pass end 
		and [user].inactivo = 0

	close SYMMETRIC KEY UPWD_Key_01
END


GO
Grant Execute on dbo.up_login_autenticaUser_central to Public
Grant Control on dbo.up_login_autenticaUser_central to Public
GO
--------------------------------------------------------------