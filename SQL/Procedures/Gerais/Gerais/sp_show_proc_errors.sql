
/****** Object:  StoredProcedure [dbo].[sp_ReplicateDocumentos]    Script Date: 10/26/2021 2:09:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- exec [sp_ReplicateDocumentos] '172.20.40.6\SQLEXPRESS', 1, 'ATLANTICO'

-- =============================================

if OBJECT_ID('[dbo].[sp_show_proc_errors]') IS NOT NULL
	drop procedure dbo.sp_show_proc_errors
go

create PROCEDURE [dbo].[sp_show_proc_errors]

@site			varchar(20)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @sql varchar(max)

set @sql = N'

IF OBJECT_ID(''tempdb..#NOTIFERR'') IS NOT NULL DROP TABLE #NOTIFERR

;with cte1 as (
select site, ft.ftstamp ,ft.nmdoc, ft.fno, ft.no, ft.nome, ft.fdata ,''Fatura Seguradora sem Fatura a cliente'' as erro from ft (nolock) 
inner join fi (nolock) on fi.ftstamp=ft.ftstamp
where 
	ft.nmdoc =''Ft. Seguradoras'' 
	and fdata>getdate()-30
	and ofistamp not in (select fistamp from fi (nolock) where rdata>getdate()-30)
	and ref<>'''' and qtt>0
union all
select  site, ft.ftstamp ,ft.nmdoc, ft.fno, ft.no, ft.nome, ft.fdata ,''Fatura Seguradora sem Fatura a cliente'' as erro from ft (nolock) 
inner join fi (nolock) on fi.ftstamp=ft.ftstamp
where 
	ft.nmdoc =''Ft. Seguradoras'' 
	and fdata>getdate()-30
	and ofistamp =''''
	and ref<>'''' and qtt>0
union all
select  site, ft.ftstamp ,ft.nmdoc, ft.fno, ft.no, ft.nome, ft.fdata ,''Documento cliente com comparticipa��o sem fatura a seguradora'' as erro from ft (nolock)
inner join ft2 (nolock) on ft.ftstamp=ft2.ft2stamp
inner join fi (nolock) on fi.ftstamp=ft.ftstamp
where u_abrev<>''''
and ref<>'''' and qtt>0
and fi.fistamp not in (select ofistamp from fi where nmdoc =''Ft. Seguradoras'' and ref<>'''' and qtt>0 and rdata>getdate()-30)
and fdata>getdate()-30
and ft.nmdoc in (''Fatura Recibo'', ''Fatura'')
and u_txcomp<>100
and fi.fistamp not in (select ofistamp from fi where nmdoc =''Reg. a Cliente'' and ref<>'''' and qtt>0 and rdata>getdate()-30)
)
select * into #NOTIFERR from cte1
--select * from #NOTIFERR

declare @siteenc varchar(30)
		,@notif_enc bit
		,@utilnotif varchar(200)
		,@grpnotif varchar(200)

		IF((SELECT 
				COUNT(*) 
			FROM #NOTIFERR ) > 0)
		BEGIN
			set @siteenc = '''+@site+''' 
			set @notif_enc = (select top 1 bool from B_Parameters_site with (nolock) where stamp=''ADM0000000115'' and site=@siteenc)
			IF @notif_enc = 1
			begin
				--declare @utilnotif varchar(200)
				set @utilnotif = (select textValue = ltrim(rtrim(isnull(textValue,''''))) from B_Parameters_site with (nolock) where stamp=''ADM0000000113'' and site=@siteenc)
				if @utilnotif<>''''
				begin
					insert into notificacoes
						(stamp
						,origem
						,usrorigem
						,destino
						,usrdestino
						,grpdestino,ecradestino,stampdestino,mensagem,recebido,tiponotif,data,site)
					select 
						left(newid(),21)
						,''Online''
						,''''
						,''''
						,b_us.iniciais
						,''''
						,''FT''
						,#NOTIFERR.ftstamp
						,#NOTIFERR.erro +  '' Documento '' + ltrim(rtrim(#NOTIFERR.nmdoc)) + '' '' + str(#NOTIFERR.fno) + '' de '' + CONVERT(varchar, #NOTIFERR.fdata, 103)
						,0
						,''Erro Processamento''
						,getdate()
						,@siteenc
						from #NOTIFERR
							inner join b_us (nolock) on iniciais in (Select * from dbo.up_splitToTable(@utilnotif,'',''))
						
				end
				--declare @grpnotif varchar(200)
				set @grpnotif = (select textValue = ltrim(rtrim(isnull(textValue,''''))) from B_Parameters_site with (nolock) where stamp=''ADM0000000114'' and site=@siteenc)
				if @grpnotif<>''''
				begin
					insert into notificacoes
						(stamp
						,origem
						,usrorigem
						,destino
						,usrdestino
						,grpdestino,ecradestino,stampdestino,mensagem,recebido,tiponotif,data,site)
					select 
						left(newid(),21)
						,''Online''
						,''''
						,''''
						,b_us.iniciais
						,''''
						,''FT''
						,#NOTIFERR.ftstamp
						,#NOTIFERR.erro +  '' Documento '' + ltrim(rtrim(#NOTIFERR.nmdoc)) + '' '' + str(#NOTIFERR.fno) + '' de '' + CONVERT(varchar, #NOTIFERR.fdata, 103)
						,0
						,''Erro Processamento''
						,getdate()
						,@siteenc
						from #NOTIFERR
						inner join b_us (nolock) on grupo in (Select * from dbo.up_splitToTable(@grpnotif,'',''))

				end
			end
		end

'
print @sql

execute (@sql)
