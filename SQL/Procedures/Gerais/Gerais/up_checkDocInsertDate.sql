 /* 
	
	Verifica se j� foi criado algum documento para a serie depois da data indicada
	EXEC up_checkDocInsertDate 1, '20231012'


*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_checkDocInsertDate]') IS NOT NULL
	DROP PROCEDURE dbo.up_checkDocInsertDate
GO

CREATE PROCEDURE dbo.up_checkDocInsertDate
	@ndoc NUMERIC(5),
	@fdata DATETIME

AS

	 SELECT 
		CASE 
			WHEN EXISTS (SELECT 1 FROM ft(nolock) WHERE ndoc = @ndoc and fdata > @fdata)
				THEN CAST( 0 AS BIT) 
			ELSE
				CAST(1 AS bit)
		END as checkDocInsertDate

GO
GRANT EXECUTE on dbo.up_checkDocInsertDate TO PUBLIC
GRANT Control on dbo.up_checkDocInsertDate TO PUBLIC
GO