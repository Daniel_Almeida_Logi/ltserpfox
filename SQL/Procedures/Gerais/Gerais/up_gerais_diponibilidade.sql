-- up_gerais_disponibilidades 'Recursos', 4, 0
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_disponibilidades]') IS NOT NULL
	drop procedure dbo.up_gerais_disponibilidades
go

create procedure dbo.up_gerais_disponibilidades
@tipo varchar(30)
,@no numeric(10,0)
,@estab numeric(3,0)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	Select 
		seriestamp
		,serieno
		,serienome
		,no
		,estab
		,nome
		,dirClinico
		,dirServico
		,dataInicio = CONVERT(date, dataFim)
		,sessoes
		,ousrinis
		,ousrdata
		,ousrhora
		,usrinis
		,usrdata
		,usrhora
		,horaInicio
		,horafim
		,segunda
		,terca
		,quarta
		,quinta
		,sexta
		,sabado
		,sempre
		,tododia
		,domingo
		,dataFim = CONVERT(date, dataFim)
		,dataIniRep
		,duracao
		,tipo
	from 
		b_series 
	where 
		tipo = @tipo
		and no = @no
		and estab = @estab

GO
Grant Execute on dbo.up_gerais_disponibilidades to Public
Grant Control on dbo.up_gerais_disponibilidades to Public
Go