-- Listar Parāmetros
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_parametersVisiveissite]') IS NOT NULL
	drop procedure dbo.up_parametersVisiveissite
go

create procedure dbo.up_parametersVisiveissite

@site as varchar(20),
@user as varchar (10) = ''
/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select
	convert(numeric,REPLACE(REPLACE(stamp,'ADM',''),'CLI','1')) as id,*
from 
	b_parameters_site (nolock)
Where
	visivel = case when @user='ADM' THEN visivel ELSE 1 END
	and site = @site 
order by 
	convert(numeric,REPLACE(REPLACE(stamp,'ADM',''),'CLI','1'))
	


GO
Grant Execute on dbo.up_parametersVisiveissite to Public
Grant Control on dbo.up_parametersVisiveissite to Public
Go