--select * from AberturaEmpresa

/*
Procedure :
		recebe um comando sql e executa em todas as farmacias com os diferentes ip�s 
		Para o preenchimento dp comando sql � necess�rio nos from colocar sempre 
		[XXXIP].mecofarma.dbo.tabela ou tabelas em quest�o e tem de ser neste formato
		exec up_gerais_runSqlAllDatabasesMecofarma 'select * from [xxxIP].mecofarma.dbo.empresa'
*/

 
 SET ANSI_NULLS ON
GO

if OBJECT_ID('[dbo].[up_gerais_runSqlAllDatabasesMecofarma]') IS NOT NULL
	DROP PROCEDURE dbo.up_gerais_runSqlAllDatabasesMecofarma
GO

CREATE PROCEDURE dbo.up_gerais_runSqlAllDatabasesMecofarma

	@sqlToRun		varchar(max) = ''

AS
SET NOCOUNT ON

If OBJECT_ID('tempdb.dbo.#ipToConnect') IS NOT NULL
		DROP TABLE #ipToConnect
If OBJECT_ID('tempdb.dbo.#resultFinal') IS NOT NULL
		DROP TABLE #resultFinal

IF (@sqlToRun = '')
BEGIN
	print 'Missing Command for SQL'
END
ELSE
BEGIN
	SELECT 
		site,
		ipaddress
	into #ipToConnect
	FROM
		AberturaEmpresa(nolock)

	DECLARE @ip_next	varchar(100) 
	DECLARE @site_next	varchar(100)
	DECLARE @Command	nvarchar(max) 
	DECLARE database_cursor CURSOR FOR 
	SELECT
		site,
		 ipaddress
	FROM
		#ipToConnect 

	OPEN database_cursor 

	declare @sqlClean varchar(max) = @sqlToRun

	FETCH NEXT FROM database_cursor INTO @site_next, @ip_next 

	WHILE @@FETCH_STATUS = 0 
	BEGIN  
		 set @Command = ''
		 set @sqlToRun = @sqlClean

		 set @sqlToRun = 'select ' + ''''  + @site_next +'''AS LOJA' + ',' + ''''+ @ip_next+'''AS IP' + ' '  
		 + REPLACE(@sqlToRun,'[XXXIP]','['+@ip_next+'\SQLEXPRESS]')
		

		 SELECT @Command = @sqlToRun
		 print @Command
		 EXEC sp_executesql @Command

		 FETCH NEXT FROM database_cursor INTO @site_next, @ip_next 
	END 

	CLOSE database_cursor 
	DEALLOCATE database_cursor 

END

If OBJECT_ID('tempdb.dbo.#ipToConnect') IS NOT NULL
		DROP TABLE #ipToConnect
If OBJECT_ID('tempdb.dbo.#resultFinal') IS NOT NULL
		DROP TABLE #resultFinal

GRANT EXECUTE on dbo.up_gerais_runSqlAllDatabasesMecofarma TO PUBLIC
GRANT Control on dbo.up_gerais_runSqlAllDatabasesMecofarma TO PUBLIC
GO
