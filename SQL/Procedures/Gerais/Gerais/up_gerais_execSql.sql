/*
	exec up_gerais_execSql 'xxx',1,'update st set design = ''1asasasasasasasaasasasasasass1'' where ref = ''5440987'''
	exec up_gerais_execSql 'xxx', 1, 'update B_utentes1 set moeda = ''EURO''', 'select * from bo', '', '', '', '', '', '', ''
	exec up_gerais_execSql 'select * from st', 1, 'select * from boda', '', '', '', ''
	
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_execSql]') IS NOT NULL
	DROP PROCEDURE [dbo].up_gerais_execSql
GO

CREATE PROCEDURE [dbo].up_gerais_execSql
	@descricao varchar(25) -- Descri��o da Opera��o a efectuar
	,@returnResult bit -- 1 = devolve resultset de debug se existir erro, 0 = n�o devolve nada
	,@sql1 varchar(max) = '' -- Batch SQL
	,@sql2 varchar(max) = ''
	,@sql3 varchar(max) = ''
	,@sql4 varchar(8000) = ''
	,@sql5 varchar(8000) = ''
	,@sql6 varchar(8000) = ''
	,@sql7 varchar(8000) = ''
	,@sql8 varchar(8000) = ''
	,@sql9 varchar(8000) = ''
	,@debug bit = 0	-- em vez de executar faz print � intru��o

/* WITH ENCRYPTION */
AS

-- Valida��o de Parametros
IF LTRIM(RTRIM(@descricao)) = ''
BEGIN
	RAISERROR ('A descri��o n�o pode ser vazia!', 16, 1);
	RETURN;
END

IF LTRIM(RTRIM(@sql1)) = ''
BEGIN
	RAISERROR ('O par�metro @sql1 n�o pode ser vazio!', 16, 1) ;
	RETURN ;
END

-- Cria variaveis internas
DECLARE
	 @NomeTran varchar(60) = @descricao + ' - ' + App_name() + ' - ' + convert(varchar,@@SPID) + ' - ' + Left(newid(),5)
	,@Tentativas int = 2
	,@comandoSQL nvarchar(max) = ''
	 ,@ErrorMessage NVARCHAR(4000) = '';

WHILE (@Tentativas > 0)
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION @NomeTran
		
			-- Concatenar String para execu��o
			SET @ComandoSQL = '--SQL Batch 1--' + Char(13) + Ltrim(Rtrim(@sql1)) + Char(13) +
							  '--SQL Batch 2--' + Char(13) + Ltrim(Rtrim(@sql2)) + Char(13) +
							  '--SQL Batch 3--' + Char(13) + Ltrim(Rtrim(@sql3)) + Char(13) +
							  '--SQL Batch 4--' + Char(13) + Ltrim(Rtrim(@sql4)) + Char(13) +
							  '--SQL Batch 5--' + Char(13) + Ltrim(Rtrim(@sql5)) + Char(13) +
							  '--SQL Batch 6--' + Char(13) + Ltrim(Rtrim(@sql6)) + Char(13) +
							  '--SQL Batch 7--' + Char(13) + Ltrim(Rtrim(@sql7)) + Char(13) +
							  '--SQL Batch 8--' + Char(13) + Ltrim(Rtrim(@sql8)) + Char(13) +
							  '--SQL Batch 9--' + Char(13) + Ltrim(Rtrim(@sql9));

			-- Executa o comando
			IF @debug = 0
				execute sp_executeSQL @ComandoSQL;
			ELSE
				print @ComandoSQL;
									
			-- Coloca o numero de tentativas a 0
			SET @Tentativas = 0;

			-- Confirma a transac��o
			COMMIT TRANSACTION @NomeTran

	END TRY
	BEGIN CATCH
		DECLARE
		@ErrorSeverity INT
		,@ErrorState INT;
		IF (ERROR_NUMBER() = 1205) -- se o erro for um deadlock, tentar novamente
		BEGIN
			-- Espera 2 segundos (formato - HH:MM:SS.mm)
			WAITFOR DELAY '00:00:2';
			-- Actualiza contador de tentativas
			SET @Tentativas -= 1;
		END
		ELSE
        BEGIN
			if @returnResult = 1
			begin
				

				SELECT  @ErrorMessage = ERROR_MESSAGE()
						,@ErrorSeverity = ERROR_SEVERITY()
						,@ErrorState = ERROR_STATE();

				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
			end

            SET @Tentativas = 0;
        END

		-- ROLLBACK
		IF XACT_STATE() <> 0
		begin
			ROLLBACK TRANSACTION @NomeTran;
			insert into B_eLog(tipo,status,mensagem,origem,data)
			values('R','A',isnull(left(ERROR_MESSAGE(),4000),''),'RB',getdate())

			insert into B_eLog(tipo,status,mensagem,origem,data)
			values('R','A',@comandoSQL,'RB',getdate())
		end
	END CATCH
END

GO
Grant Execute on dbo.up_gerais_execSql to Public
Grant Control on dbo.up_gerais_execSql to Public
GO





