
/****** 
	Retorna informacao contida num QRCODE

	Recebe String e retora um result set

	up_getQrCodeInfo '010560035902244621100891767899023172305311016228321#7145100359', '#'

 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
if OBJECT_ID('[dbo].up_getQrCodeInfo') IS NOT NULL
	drop procedure dbo.up_getQrCodeInfo
go


create PROCEDURE [dbo].up_getQrCodeInfo
@code				varchar(200),
@delimitador		varchar(1) = '#'

AS
SET NOCOUNT ON	

declare @productCode varchar(100) = ''
declare @packSn varchar(100) = ''
declare @batchId varchar(100) = ''
declare @ref varchar(18) = ''
declare @countryId varchar(3) = ''
declare @batchDate varchar(6) = ''

select  
	"productCode" = @productCode,
	"packSn"      = @packSn,
	"batchId"     = @batchId,
	"ref"         = @ref,
	"countryId"   = @countryId,
	"batchDate"   = @batchDate
	
GO

Grant Execute On dbo.up_getQrCodeInfo to Public
Grant Control On dbo.up_getQrCodeInfo to Public


