/* Correr sql em todas as bds
	

	exec up_gerais_runAllDatabasesGripe '20210501', '20211231'


*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_gerais_runAllDatabasesGripe]') IS NOT NULL
	drop procedure up_gerais_runAllDatabasesGripe
go

create PROCEDURE up_gerais_runAllDatabasesGripe
	@dateInit datetime,
	@dateEnd  datetime

	
/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

	If OBJECT_ID('tempdb.dbo.#temp') IS NOT NULL
		DROP TABLE #temp
	
	If OBJECT_ID('tempdb.dbo.#resultVac') IS NOT NULL
		DROP TABLE #resultVac
	


	set @dateEnd = @dateEnd + '23:59:00'
	DECLARE @DB_Name varchar(100) 
	DECLARE @Command nvarchar(max) 

	create table #resultVac
	(
		infarmed Varchar(20), 
		qtt      int ,
		ref  Varchar(20)
	)

	DECLARE database_cursor CURSOR FOR 
	SELECT name 
	FROM MASTER.sys.databases 
	where 
		state = 0 and 
		(name like 'F%' and name not like '%FFS_%' and  len(name) <=7) -- bds que interessam
	order by name desc

	OPEN database_cursor 

	FETCH NEXT FROM database_cursor INTO @DB_Name 

	WHILE @@FETCH_STATUS = 0 
	BEGIN 
		
		set @Command='
		insert into #resultVac
		 select 
			empresa.infarmed,
			  sum(case when fi.tipodoc=3 then  fi.qtt*-1 else fi.qtt end) as qtt,
			fi.ref
		
		 from ' +@DB_Name + '.dbo.ft ft(nolock)
		 inner join ' +@DB_Name + '.dbo.fi fi (nolock) on ft.ftstamp = fi.ftstamp
		 inner join ' +@DB_Name + '.dbo.empresa empresa (nolock) on empresa.site = ft.site
		 inner join ' +@DB_Name + '.dbo.fprod fprod (nolock) on fprod.cnp = fi.ref
		 where  tipoempresa = ''FARMACIA''  and assfarm = ''AFP''
		 and fprod.ref=''5726229''
		 and ft.tipodoc in (1,3)  and fdata>='''+convert(varchar, @dateInit, 120)+''' and fdata <= '''+convert(varchar, @dateEnd, 120)+'''
		 group by infarmed, fi.ref
		 '
		 
		 print @Command
		 EXEC sp_executesql @Command 

	FETCH NEXT FROM database_cursor INTO @DB_Name 
	END 

	CLOSE database_cursor 
	DEALLOCATE database_cursor 



	select sum(qtt) as Qtt from #resultVac


	If OBJECT_ID('tempdb.dbo.#resultVac') IS NOT NULL
		DROP TABLE #resultVac

	
	If OBJECT_ID('tempdb.dbo.#temp') IS NOT NULL
		DROP TABLE #temp
	
GO
Grant Execute On up_gerais_runAllDatabasesGripe to Public
Grant Control On up_gerais_runAllDatabasesGripe to Public
GO

