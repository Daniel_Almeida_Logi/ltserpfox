/*

 exec up_login_autenticaUser_local 'Farm@L0g1', 'sa', 'Loja 1'
 exec up_gerais_trocaEmpresa 1,'Adminsitrador'
 
 select * from b_us
 select * from b_parameters where name like '%período%'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_login_autenticaUser_local]') IS NOT NULL
	drop procedure dbo.up_login_autenticaUser_local
go

create procedure dbo.up_login_autenticaUser_local
@pass varchar(30),
@user varchar(30),
@site varchar(20)

/* WITH ENCRYPTION */ 
AS 
BEGIN 
	
	select 
		iniciais
		--,pwpos = userpass
		,pwpos=password
		,userno
		,nome
		,grupo
		,vendnm = username
		,drcedula
		,drclprofi
	from 
		b_us as us (nolock)
	where
		us.username = case when @user = '' then us.username else @user end
		--and (CONVERT(varchar(50), DecryptByKey(Encryptedpassw)) COLLATE Latin1_General_CS_AS) = case when (@pass COLLATE Latin1_General_CS_AS) = '' then (us.userpass COLLATE Latin1_General_CS_AS) else (@pass COLLATE Latin1_General_CS_AS) end
		--and us.userpass = case when @pass = '' then us.userpass else @pass end 
		and us.inactivo = 0

END


GO
Grant Execute on dbo.up_login_autenticaUser_local to Public
Grant Control on dbo.up_login_autenticaUser_local to Public
GO
--------------------------------------------------------------