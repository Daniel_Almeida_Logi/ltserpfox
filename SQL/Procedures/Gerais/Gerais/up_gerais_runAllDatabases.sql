/* Correr sql em todas as bds
	
	declare @idBd varchar(max) = 'P18305A,F13130A,F16096A' -- vazio corre em todas
	declare @sql varchar(max) = N' 

		declare @ref varchar(18) = ''5440987''
		select * from st(nolock) where ref=@ref

	'
	exec up_gerais_runAllDatabases @sql, @idBd ,''

	declare @tipoEmpresa varchar(100) = '' -- vazio corre em todas
	declare @idBd varchar(max) = '' -- vazio corre em todas
	declare @sql varchar(max) = N' 

		declare @ref varchar(18) = ''5440987''
		select * from st(nolock) where ref=@ref

	'
	exec up_gerais_runAllDatabases @sql, @idBd @tipoEmpresa

	declare @idBd varchar(max) = 'P18305A,F13130A'
	Select items from dbo.up_splitToTable(@idBd, ',')



*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_gerais_runAllDatabases]') IS NOT NULL
	drop procedure up_gerais_runAllDatabases
go

create PROCEDURE up_gerais_runAllDatabases
	@sql varchar(max) = '',
	@id varchar(max) = '',
	@tipoempresa varchar(50) = ''

	
/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

	If OBJECT_ID('tempdb.dbo.#temp') IS NOT NULL
		DROP TABLE #temp
	
	DECLARE @DB_Name varchar(100) 
	DECLARE @Command nvarchar(max) 
	DECLARE @sql_ nvarchar(max)

   
   set @id = rtrim(ltrim(isnull(@id,'')))


	DECLARE database_cursor CURSOR FOR 
	SELECT name 
	FROM MASTER.sys.sysdatabases 
	where 
		(name like 'F%'  or name like 'C%' or name like 'P%') -- bds que interessam
		and DATABASEPROPERTYEX('master', 'Status')  = 'ONLINE' AND version IS NOT NULL
	order by name desc

	OPEN database_cursor 

	FETCH NEXT FROM database_cursor INTO @DB_Name 

	WHILE @@FETCH_STATUS = 0 
	BEGIN 
		 set @sql_ = ltrim(rtrim(isnull(@sql,''))) 
		 set @Command=''
		 SELECT @Command = 'SELECT ' + '''' + @DB_Name + '''' + ' as BaseDados, site as loja, nomecomp as nome ,id_lt as idLoja, tipoempresa as tipo FROM ' +@DB_Name + '.dbo.empresa(nolock)'
		  if(@tipoempresa!='')
            SELECT @Command = @Command + ' where  tipoempresa= ''' + @tipoempresa + '''   '
		 set @Command  = @Command + char(13) + ' USE '  + @DB_Name
		 set @Command = @Command + char(13) + @sql_
		 print @Command
		 EXEC sp_executesql @Command 

		 FETCH NEXT FROM database_cursor INTO @DB_Name 
	END 

	CLOSE database_cursor 
	DEALLOCATE database_cursor 

	
GO
Grant Execute On up_gerais_runAllDatabases to Public
Grant Control On up_gerais_runAllDatabases to Public
GO