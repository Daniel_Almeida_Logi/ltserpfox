-- lista de documentos de uma referÍncia
-- exec up_gerais_verificaIdioma 'pt-PT'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_verificaIdioma]') IS NOT NULL
	drop procedure dbo.up_gerais_verificaIdioma
go

create procedure dbo.up_gerais_verificaIdioma
@lingua as varchar(50)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	DECLARE @sql varchar(max) = ''

	select @sql = @sql + N'	
		Select 
			valorOriginal
			,valor = valor_' + @lingua + '
			,lingua = '''+@lingua+'''
		From
			b_idioma (nolock)
		'

	print @sql
	execute (@sql)

GO
Grant Execute On dbo.up_gerais_verificaIdioma to Public
Grant Control On dbo.up_gerais_verificaIdioma to Public
GO