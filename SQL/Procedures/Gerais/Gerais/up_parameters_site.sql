-- exec up_parameters_site 'Loja 1'

-- Listar Parāmetros
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_parameters]') IS NOT NULL
	drop procedure dbo.up_parameters_site
go

create procedure dbo.up_parameters_site

@site as varchar(15)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select
	convert(numeric,REPLACE(REPLACE(stamp,'ADM',''),'CLI','1')) as id
	,LTRIM(RTRIM(stamp)) as stamp
	,name
	,type 
	,left(convert(varchar(254),Textvalue),254) as textvalue
	,numvalue
	,bool
	,oDate
	,ldate
	,visivel
	,MostraTextValue ,MostraNumValue, MostraBool, ListaTextValue
	,Unidades
	,obs
from 
	b_parameters_site (nolock)
where
	site = @site
order by 
	convert(numeric,REPLACE(REPLACE(stamp,'ADM',''),'CLI','1'))
	

GO
Grant Execute on dbo.up_parameters_site to Public
Grant Control on dbo.up_parameters_site to Public
Go