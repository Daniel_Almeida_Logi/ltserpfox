/*
Funcao para retornar Dados de comparticipações complementar

exec up_gerais_compartCompl '01'
*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_gerais_compartCompl]') IS NOT NULL
	drop procedure dbo.up_gerais_compartCompl
go

create procedure dbo.up_gerais_compartCompl
@codigo			VARCHAR(55)


AS
SET NOCOUNT ON
	
	select	cptpla.compl
			,iif( (select codigo from cptpla x where x.codigo = cptpla.compl) is null
					,''
					,(select cptorgabrev from cptpla x where x.codigo =cptpla.compl)
			) as codigocomp
			,cptpla.cptorgabrev
	from cptpla (nolock) 
	where codigo=@codigo

GO
Grant Execute on dbo.up_gerais_compartCompl to Public
Grant control on dbo.up_gerais_compartCompl to Public
GO
