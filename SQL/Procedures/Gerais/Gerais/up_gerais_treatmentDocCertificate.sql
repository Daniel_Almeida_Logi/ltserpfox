/*
	Program for Tretament variables for certificate Docuemnts 


				exec up_gerais_treatmentDocCertificate 'Loja 1', 'ADMBA367022-4B50-401F-AD8', 'ADMB36E8160-1CCC-4107-B97',
			 	'ADMBCEE20BD-0CAA-4357-B2B', 1078900, 0, '1_1078900_202246157', '1_1078900_2022.PDF', 1,
			 	1,'DOCCERT', 'C:\Logitools\Versoes\22_15\ldata\Cache\PDF\', 'Factura_91_ADMA40F8371-1199-4CC0-B70.pdf', 'C:\Logitools\Versoes\22_15\ldata\Cache\PDF\', 'Factura_91_ADMA40F8371-1199-4CC0-B70_signed.pdf', '', 0,
			 	1.00000, 'BASIC' 
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


IF OBJECT_ID('[dbo].[up_gerais_treatmentDocCertificate]') IS NOT NULL
	DROP procedure dbo.up_gerais_treatmentDocCertificate
GO

CREATE procedure dbo.up_gerais_treatmentDocCertificate
	@site								varchar(254),
	@personReceiverToken				varchar(254),
	@token								varchar(254),
	@documentsToken						varchar(254),
	@no									int,
	@estab								int,
	@externalReference					varchar(254),
	@fileName							varchar(254),
	@serviceTeste						bit,
	@type								int,
	@typeDesc							varchar(254),
	@pathOrigem							varchar(254),
	@fileOrigem							varchar(254), 
	@pathDestino						varchar(254),
	@fileDestino						varchar(254),
	@operationID						varchar(254),
	@attachDocuments					bit,
	@typePlan							int,
	@descPlan							varchar(30)

AS
BEGIN
	
	DECLARE @tipo														varchar(200)
	DECLARE @id															int
	DECLARE @entidade													varchar(200)
	DECLARE @valor														varchar(200)
	DECLARE @valorVisibleSignatureInfo									varchar(200)
	DECLARE @valorAarea													varchar(200)
	DECLARE @valorAreaBackgroundColor									varchar(200)
	DECLARE @valorFont													varchar(200)
	DECLARE @valorImageType												varchar(200)
	DECLARE @valorImageValue											varchar(200)
	DECLARE @valorLocationType											varchar(200)
	DECLARE @valorLocationValue											varchar(200)
	DECLARE @valorOneOffTemplate										varchar(200)
	DECLARE @valorTemplate												varchar(200)
	DECLARE @valorEntityType											varchar(200)
	DECLARE @valorEntityTypeDesc										varchar(200)
	DECLARE @valorDescription											varchar(200)
	DECLARE @valorPreserInfoAlias										varchar(200)
	DECLARE @valorPreserInfoContent										varchar(200)
	DECLARE @valorPreserInfoAttachDocuments								varchar(200)
	DECLARE @valorEmailDeliveryStatus									varchar(200)
	DECLARE @valorApplyTimestamp										varchar(200)
	DECLARE @valorLocation												varchar(200)
	DECLARE @valorReason												varchar(200)
	DECLARE @valorSignatureType											varchar(200)
	DECLARE @valorCanonicalTransform									varchar(200)
	DECLARE @valorSignatureLocation										varchar(200)
	DECLARE @valorSignatureTransform									varchar(200)
	DECLARE @valorReturnSignedContent									varchar(200)
	DECLARE @valorContentType											varchar(200)
	DECLARE @emailUtente												varchar(254) = 'tecnico@lts.pt'
	DECLARE @stampDocsCertificateRequest_PersonRec						varchar(36) = left(NEWID(),36)
	DECLARE @stampDocsCertificateRequest_docs							varchar(36) = left(NEWID(),36)
	DECLARE @resultInserts												bit			= 0



	set @typePlan	= isnull(@typePlan,1)
	set @descPlan	= isnull(@descPlan,'')

/*	SELECT
		@emailUtente = email
	FROM
		b_utentes(nolock)
	where
		no = @no and estab = @estab
*/
	DECLARE database_cursor CURSOR FOR 
		SELECT 
			tipo,
			id,
			entidade,
			valor
		FROM
			configura_doc_certificar(NOLOCK)
		WHERE
			configura_doc_certificar.site = @site

	OPEN database_cursor 
	FETCH NEXT FROM database_cursor INTO @tipo, @id, @entidade, @valor

	WHILE @@FETCH_STATUS = 0 
	BEGIN  
			IF @id = 1
				BEGIN
					 set @valorVisibleSignatureInfo = @valor
				END
			IF @id = 2
				BEGIN
					 set @valorAarea = @valor
				END
			IF @id = 3
				BEGIN
					 set @valorAreaBackgroundColor = @valor
				END
			IF @id = 4
				BEGIN
					 set @valorFont = @valor
				END
			IF @id = 5
				BEGIN
					 set @valorImageType = ''
				END
			IF @id = 6
				BEGIN
					 set @valorImageValue = ''
				END
			IF @id = 7
				BEGIN
					 set @valorLocationType = @valor
				END
			IF @id = 8
				BEGIN
					 set @valorLocationValue = @valor
				END
			IF @id = 9
				BEGIN
					 set @valorOneOffTemplate = @valor
				END
			IF @id = 10
				BEGIN
					 set @valorTemplate = @valor
				END
			IF @id = 11
				BEGIN
					 set @valorEntityType = @valor
				END
			IF @id = 12
				BEGIN
					 set @valorEntityTypeDesc = @valor
				END
			IF @id = 13
				BEGIN
					 set @valorDescription = @valor
				END
			IF @id = 14
				BEGIN
					 set @valorPreserInfoAlias = @valor
				END
			IF @id = 15
				BEGIN
					 set @valorPreserInfoContent = @valor
				END
			IF @id = 16
				BEGIN
					 set @valorPreserInfoAttachDocuments = @valor
				END
			IF @id = 17
				BEGIN
					 set @valorEmailDeliveryStatus = @valor
				END
			IF @id = 18
				BEGIN
					 set @valorApplyTimestamp = @valor
				END
			IF @id = 19
				BEGIN
					 set @valorLocation = @valor
				END
			IF @id = 20
				BEGIN
					 set @valorReason = @valor
				END
			IF @id = 21
				BEGIN
					 set @valorSignatureType = @valor
				END
			IF @id = 22
				BEGIN
					 set @valorCanonicalTransform = @valor
				END
			IF @id = 23
				BEGIN
					 set @valorSignatureLocation = @valor
				END
			IF @id = 24
				BEGIN
					 set @valorSignatureTransform = @valor
				END
			IF @id = 25
				BEGIN
					 set @valorReturnSignedContent = @valor
				END
			IF @id = 26
				BEGIN
					 set @valorContentType = @valor
				END

	FETCH NEXT FROM database_cursor INTO @tipo, @id, @entidade, @valor
	END 
	
	CLOSE database_cursor 
	DEALLOCATE database_cursor 

	IF (NOT EXISTS (SELECT * FROM docsCertificateRequest(NOLOCK) WHERE token = @token or personReceiverToken = @personReceiverToken or documentsToken = @documentsToken))
	BEGIN
		insert into docsCertificateRequest (token,documentsToken,externalReference,type,
											typeDesc,certifyingEntity,certifyingEntityName,description
											,returnSignedContent,preserInfoAlias,preserInfoContent,preserInfoAttachDocuments,
											personReceiverToken,site,test, operationId, attachDocuments,
											ousrdata,ousrinis,usrdata,usrinis,accountType,accountTypeDesc)  
	
		values (@token,@documentsToken,@externalReference,@type,
				@typeDesc,@valorEntityType,@valorEntityTypeDesc,@valorDescription,
				@valorReturnSignedContent,@valorPreserInfoAlias,@valorPreserInfoContent,@valorPreserInfoAttachDocuments,
				@personReceiverToken,@site,@serviceTeste, @operationID, @attachDocuments, 
				getdate(),'',getdate(),'',@typePlan,@descPlan) 
	END


	IF (NOT EXISTS (SELECT * FROM docsCertificateRequest_PersonRec(NOLOCK) 
					WHERE personReceiverToken = @personReceiverToken ))
	BEGIN
		insert into docsCertificateRequest_PersonRec (stamp,personReceiverToken,deliveryStatus,email,
														ousrdata,ousrinis,usrdata,usrinis)
		values (@stampDocsCertificateRequest_PersonRec,@personReceiverToken,@valorEmailDeliveryStatus,@emailUtente,
				getdate(),'',getdate(),'') 

	END
	
	IF (NOT EXISTS (SELECT * FROM docsCertificateRequest_docs(NOLOCK) 
					WHERE  documentsToken = @documentsToken))
	BEGIN	
		insert into docsCertificateRequest_docs (stamp,documentsToken,externalReference,name,nameDocToSign,
												filePath,filePathToSign, contentType,applyTimestamp,location
												,reason,signatureType,area,areaBackgroundColor,
												font,imageType,imageValue,locationType,
												locationValue,oneOffTemplate,templateType,canonicalTransform,
												signatureLocation,signatureTransform,
												ousrdata,ousrinis,usrdata,usrinis)

		values(@stampDocsCertificateRequest_docs,@documentsToken,@externalReference,@fileDestino,@fileOrigem,
		@pathDestino,@pathOrigem,@valorContentType,@valorApplyTimestamp,@valorLocation
		,@valorReason,@valorSignatureType,@valorAarea,@valorAreaBackgroundColor,
		@valorFont,@valorImageType,@valorImageValue,@valorLocationType,
		@valorLocationValue,@valorOneOffTemplate,@valorTemplate,@valorCanonicalTransform,
		@valorSignatureLocation,@valorSignatureTransform
		,getdate(),'',getdate(),'') 
	END

	set @resultInserts = 
		CASE WHEN
			(SELECT count(*)   from docsCertificateRequest(NOLOCK) WHERE token = @token) > 0 and 
			(SELECT count(*) from docsCertificateRequest_PersonRec(NOLOCK) where stamp = @stampDocsCertificateRequest_PersonRec) > 0 and 
			(SELECT count(*)	from docsCertificateRequest_docs(NOLOCK) WHERE stamp = @stampDocsCertificateRequest_docs) > 0
		THEN 
			1
		END

	select @resultInserts as resultInserts

END

GO
GRANT EXECUTE on dbo.up_gerais_treatmentDocCertificate TO PUBLIC
GRANT CONTROL on dbo.up_gerais_treatmentDocCertificate TO PUBLIC
GO