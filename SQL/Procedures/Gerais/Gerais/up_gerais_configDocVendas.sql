/* Dados para variaveis de documentos de venda @atendimento

	up_gerais_configDocVendas 'Loja 1'
	up_gerais_configDocVendas 'Loja 2'
	up_gerais_configDocVendas 'Loja 3'
	exec up_gerais_configDocVendas 'Loja 4'

*/
		
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_gerais_configDocVendas]') IS NOT NULL
	drop procedure dbo.up_gerais_configDocVendas
GO

create procedure dbo.up_gerais_configDocVendas

@site varchar(60)

/* WITH ENCRYPTION */

AS
	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosEmpresa'))
		DROP TABLE #dadosEmpresa
	
	-- dados de documento configurados na empresa empresa
	select 
		top 1 serie_vd, serie_fact, serie_regcli, serie_EntSNS, serie_Ent, site
	into 
		#dadosEmpresa
	from 
		empresa 
	where
		site = @site

	-- Result Set final
	select top 1
		serie_vdNm     = (select nmdoc from td where ndoc = #dadosEmpresa.serie_vd)
		,#dadosEmpresa.serie_vd
		,serie_factNm   = (select nmdoc from td where ndoc = #dadosEmpresa.serie_fact)
		,#dadosEmpresa.serie_fact
		,serie_regcliNm = (select nmdoc from td where ndoc = #dadosEmpresa.serie_regcli)
		,#dadosEmpresa.serie_regcli
		,serie_EntSNSNm = (select nmdoc from td where ndoc = #dadosEmpresa.serie_EntSNS)
		,#dadosEmpresa.serie_EntSNS
		,serie_EntNm    = (select nmdoc from td where ndoc = #dadosEmpresa.serie_Ent)
		,#dadosEmpresa.serie_Ent
	from 
		td
		inner join #dadosEmpresa on td.site = #dadosEmpresa.site
		
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosEmpresa'))
		DROP TABLE #dadosEmpresa

GO
Grant Execute on dbo.up_gerais_configDocVendas to Public
Grant control on dbo.up_gerais_configDocVendas to Public
GO