IF EXISTS (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('dbo.up_gerais_devolve_primeiro_e_ultimo_nome'))
begin 
	DROP PROCEDURE up_gerais_devolve_primeiro_e_ultimo_nome
end

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jos� Moreira
-- Create date: 2022/11/15
-- Description:	devolve primeiro + ultimo nome, primeiro nome, ultimo nome
-- Example: exec up_devolve_primeiro_e_ultimo_nome 'Arroz de Pato'
-- =============================================
create PROCEDURE up_gerais_devolve_primeiro_e_ultimo_nome
	@nome varchar(4000)
AS
BEGIN
	select
		ltrim(rtrim(left(rtrim(ltrim(@nome)),charindex(' ',rtrim(ltrim(@nome)) + ' ')))) + ' ' + ltrim(rtrim(RIGHT(rtrim(ltrim(@nome)), (CHARINDEX(' ',REVERSE(rtrim(ltrim(@nome))),0))))) FIRST_AND_LAST_NAME, 
		--LEN(ltrim(rtrim(left(rtrim(ltrim(@nome)),charindex(' ',rtrim(ltrim(@nome)) + ' ')))) + ' ' + ltrim(rtrim(RIGHT(rtrim(ltrim(@nome)), (CHARINDEX(' ',REVERSE(rtrim(ltrim(@nome))),0)))))),
		ltrim(rtrim(left(rtrim(ltrim(@nome)),charindex(' ',rtrim(ltrim(@nome)) + ' ')))) FIRST_NAME, 
		--LEN(ltrim(rtrim(left(rtrim(ltrim(@nome)),charindex(' ',rtrim(ltrim(@nome)) + ' '))))),
		ltrim(rtrim(RIGHT(rtrim(ltrim(@nome)), (CHARINDEX(' ',REVERSE(rtrim(ltrim(@nome))),0))))) as LAST_NAME 
		--,len(ltrim(rtrim(RIGHT(rtrim(ltrim(@nome)), (CHARINDEX(' ',REVERSE(rtrim(ltrim(@nome))),0)))))),
		--charindex(' ',rtrim(ltrim(rtrim(ltrim(@nome)))) + ' ')
END
GO
