-- Dados do Cliente --
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_dadosCliente]') IS NOT NULL
	drop procedure dbo.up_gerais_dadosCliente
go

create procedure dbo.up_gerais_dadosCliente

@no numeric(10),
@estab numeric(5)

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

select top 1 
	nome, nome2, no, estab, moeda, fax, telefone, esaldo,
	morada, local, codpost, ncont, tipo, email, url,
	codigop, bino, zona, tipo, ccusto, tpdesc
from b_utentes (nolock)
where no = @no and estab=@estab


GO
Grant Execute on dbo.up_gerais_dadosCliente to Public
Grant Control on dbo.up_gerais_dadosCliente to Public
GO