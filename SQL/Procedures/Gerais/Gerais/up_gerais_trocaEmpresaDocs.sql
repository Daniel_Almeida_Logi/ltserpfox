/*  Dados das empresas Documentos

	 exec up_gerais_trocaEmpresaDocs 56, 'Operadores'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_gerais_trocaEmpresaDocs]') IS NOT NULL
	drop procedure up_gerais_trocaEmpresaDocs
go

create PROCEDURE up_gerais_trocaEmpresaDocs
	@userno numeric(5,0),
	@grupo varchar(20)

AS

declare @userSiteDefault varchar(20)
set @userSiteDefault = (select loja from b_us(nolock) where userno = @userno)

select 
	distinct 
	Local = RTRIM(LTRIM(empresa.site))
	,Designacao = nomabrv 
	,siteno = empresa.no
	,armazem1= (select top 1 armazem from empresa_arm (nolock) Where	empresa_arm.empresa_no = empresa.no	order by armazem)
from 
	empresa (nolock)
	left join (Select b_pfu.* From b_pf(nolock) inner join b_pfu(nolock) on b_pfu.pfstamp = b_pf.pfstamp where resumo = 'Importar Documentos' and descricao = 'Permite importar documentos' and grupo = 'Documentos' and tipo = 'Atribui Acesso') as pfu on RTRIM(LTRIM(pfu.site)) = RTRIM(LTRIM(empresa.site))
	left join (Select b_pfg.* From b_pf(nolock) inner join b_pfg(nolock) on b_pfg.pfstamp = b_pf.pfstamp where resumo = 'Importar Documentos' and descricao = 'Permite importar documentos' and grupo = 'Documentos' and tipo = 'Atribui Acesso') as pfg on RTRIM(LTRIM(pfg.site)) = RTRIM(LTRIM(empresa.site))
Where
	(isnull(pfu.userno,9999) = @userno Or ISNULL(pfg.nome,'') = @grupo)
	OR empresa.site = @userSiteDefault 
order by
	RTRIM(LTRIM(empresa.site))

GO
Grant Execute On up_gerais_trocaEmpresaDocs to Public
Grant Control On up_gerais_trocaEmpresaDocs to Public
GO