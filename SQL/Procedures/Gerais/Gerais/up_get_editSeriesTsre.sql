/* 
	
	Devolve series da Tsre por loja
	EXEC up_get_editSeriesTsre 'Loja 1'


*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_editSeriesTsre]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_editSeriesTsre
GO

CREATE PROCEDURE dbo.up_get_editSeriesTsre	
	@site VARCHAR(60)

AS

	SELECT 
		CAST('CM1' AS varchar(4)) as tabela,
		cmdesc as nome,
		cm1stamp as stamp,
		resetNumeracaoAnual,
		resetNumeracaoAnual as resetNumeracaoAnualOri,
		ATCUD
	FROM	
		cm1(nolock)
	WHERE
		tiposaft <> ''

	UNION ALL

	SELECT 
		CAST('TSRE' AS varchar(4)) as tabela,
		nmdoc as nome,
		tsrestamp as stamp,
		resetNumeracaoAnual,
		resetNumeracaoAnual as resetNumeracaoAnualOri,
		ATCUD
	FROM	
		tsre(nolock)
	WHERE
		codsaft <> ''
		and (site = @site or site='')

GO
GRANT EXECUTE on dbo.up_get_editSeriesTsre TO PUBLIC
GRANT Control on dbo.up_get_editSeriesTsre TO PUBLIC
GO