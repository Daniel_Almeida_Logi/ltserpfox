/*
	Levanta dados de utilizador
	exec up_gerais_getUser '123'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_getUser]') IS NOT NULL
	drop procedure dbo.up_gerais_getUser
go

create procedure dbo.up_gerais_getUser
	@pass varchar(20)


/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT 
		* 
	FROM 
		b_us (nolock) 
	WHERE 
		userpass = @pass

GO
Grant Execute On dbo.up_gerais_getUser to Public
Grant Control On dbo.up_gerais_getUser to Public
GO