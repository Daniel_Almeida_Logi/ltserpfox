/*

	Devolve todos os armazens associados a lojas

	exec up_gerais_getArmazens

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



if OBJECT_ID('[dbo].[up_gerais_getArmazens]') IS NOT NULL
	drop procedure dbo.up_gerais_getArmazens
go

create procedure dbo.up_gerais_getArmazens
AS

	Select 
		DISTINCT 
		arm.armazem
	from 
		empresa_arm(nolock) as arm
		join empresa(nolock) on arm.empresa_no = empresa.no


GO
Grant Execute On dbo.up_gerais_getArmazens to Public
Grant Control On dbo.up_gerais_getArmazens to Public
Go