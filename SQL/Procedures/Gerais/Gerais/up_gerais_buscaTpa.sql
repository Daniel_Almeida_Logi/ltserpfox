/*
Funcao para retornar TPA's da Loja

exec up_gerais_buscaTpa 'Loja 1'
*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_gerais_buscaTpa]') IS NOT NULL
	drop procedure dbo.up_gerais_buscaTpa
go

create procedure dbo.up_gerais_buscaTpa
@site			VARCHAR(55)


AS
SET NOCOUNT ON
	

	select 
		CAST(0 AS BIT) AS sel
		,term.no as termNo
		,tpa.model
		,tpa.version
		,tpa.specification_version as termSpecVersion
		,tpa.port
		,tpa.stamp as tpaStamp
		,term.site
		,term.terminal
		,ISNULL(tpa.ip, '') as TPA
	from 
		B_Terminal (nolock) AS term
		left join tpa_map(nolock) as tpa on tpa.termstamp = term.tstamp
	where 1=1
		and tpa.ip <> ''
		and term.site = @site
	order by 
		term.site
		,term.no


GO
Grant Execute on dbo.up_gerais_buscaTpa to Public
Grant control on dbo.up_gerais_buscaTpa to Public
GO
