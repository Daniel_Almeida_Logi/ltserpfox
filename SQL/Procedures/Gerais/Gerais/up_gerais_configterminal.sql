/* SP para CONFIGURAR variaveis associadas ao terminal 
	


*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_gerais_configTerminal]') IS NOT NULL
    DROP PROCEDURE dbo.up_gerais_configTerminal
GO

CREATE procedure dbo.up_gerais_configTerminal
	 @ClientName	varchar(50) = '',	/* Valor default */
	 @Conta			varchar(30) = '',	/* Valor default */
	 @SessionName	varchar(30) = ''	/* Valor default */
AS
	
/**********************
1 - Valida Parametros
***********************/
IF LTRIM(RTRIM(@Conta)) = ''
BEGIN		
	RAISERROR ('A conta de utilizador n�o pode ser vazia.', 16, 1)
	RETURN ;
END

IF LTRIM(RTRIM(@SessionName)) = ''
BEGIN		
	RAISERROR ('O tipo de sess�o n�o pode ser vazio.', 16, 1)
	RETURN ;
END


/*****************************
2 - Determina Tipo de Cliente
******************************/
DECLARE @TipoCliente SMALLINT

IF LEFT(@ClientName,1) = '.'
	SET @TipoCliente = 1 ; -- HTML5 / JAVA
ELSE
	SET @TipoCliente = 2 ; -- Windows

/*****************************
3 - Determina Tipo de Sessao
******************************/
DECLARE @TipoSessao SMALLINT

IF @SessionName LIKE 'RDP*'
	SET @TipoSessao = 1 ; -- RDP
ELSE
	SET @TipoSessao = 2 ; -- LOCAL	
	

/*****************************
4 - Devolve resultados
******************************/

IF OBJECT_ID('tempdb.dbo.#temp_Terminal') IS NOT NULL
	DROP TABLE #temp_Terminal ;

SELECT *
INTO #temp_Terminal
FROM(
	SELECT 
		*
	FROM 
		[DBO].[B_Terminal]
	WHERE
		[B_Terminal].[machine] = CASE WHEN @TipoCliente = 1 THEN [B_Terminal].[machine] ELSE @ClientName END
		AND [B_Terminal].[no] = CASE WHEN @TipoCliente = 1 THEN 90 ELSE [B_Terminal].[no] END 
)x


	
/************************************
5 - Verifica o numero de resultados 
*************************************/
IF @@ROWCOUNT = 0 /* Se n�o tiver resultados insere o terminal */
BEGIN
	declare @tsstamp as varchar(25) 
	set @tsstamp = LEFT(NEWID(),25)

	INSERT INTO [dbo].[B_Terminal](
		[tstamp],
		[machine],
		[terminal],
		[no],
		[armazem],
		[Site],
		[Atendimento],
		[TS],
		[pathAnexos],
		[pathExport],
		[pathReport]
		
	)
	SELECT
		[tstamp] = @tsstamp ,
		[machine] = CASE WHEN @TipoCliente = 1 THEN '' ELSE @ClientName END ,
		[terminal] = CASE WHEN @TipoCliente = 1 
						THEN 'Terminal 90' 
						ELSE ISNULL((SELECT 'Terminal ' + Convert(varchar,(MAX([no]) + 1)) FROM [dbo].[B_Terminal] WHERE [no] < 999),'Terminal 1')
					 END,		
		[no] = CASE WHEN @TipoCliente = 1 
					THEN 90 
					ELSE ISNULL((SELECT (MAX([no]) + 1) FROM [dbo].[B_Terminal] WHERE [no] < 999),CASE WHEN @TipoCliente = 1 THEN 90 ELSE 1 END)
			   END,	
		[armazem] = isnull((select top 1 armazem from empresa_arm inner join empresa on empresa.no = empresa_arm.empresa_no where empresa.conta = @Conta order by empresa_arm.armazem),1),
		[Site] = isnull((select top 1 site from empresa where conta = @Conta),'Loja 1'),
		[Atendimento] = CASE WHEN @TipoCliente = 1 THEN 0 ELSE 1 END ,	
		[ts] = CASE WHEN @TipoCliente = 2 THEN 1 ELSE 0 END	,
		'\\tsclient\C\Logitools',
		'\\tsclient\C\Logitools',
		'\\tsclient\C\Logitools'
	
	
	/* Devolve resultados */
	SELECT * FROM [dbo].[B_Terminal] where [tstamp] = @tsstamp
	
	
END
ELSE
BEGIN
	SELECT * FROM #temp_Terminal
	
	/*******************************
	 Remove tabelas temporarias
	*******************************/
	IF OBJECT_ID('tempdb.dbo.#temp_Terminal') IS NOT NULL
		DROP TABLE #temp_Terminal ;
END


--/*****************************
--2 - Determina Tipo de Cliente
--******************************/
--DECLARE @TipoCliente SMALLINT

--IF LEFT(@ClientName,1) = '.'
--	SET @TipoCliente = 1 ; -- HTML5 / JAVA
--ELSE
--	SET @TipoCliente = 2 ; -- Windows

--/*****************************
--3 - Determina Tipo de Sessao
--******************************/
--DECLARE @TipoSessao SMALLINT

--IF @SessionName LIKE 'RDP*'
--	SET @TipoSessao = 1 ; -- RDP
--ELSE
--	SET @TipoSessao = 2 ; -- LOCAL	
	

--/*****************************
--4 - Devolve resultados
--******************************/
--DEVOLVE_RESULTADOS:

--IF OBJECT_ID('tempdb.dbo.#temp_Terminal') IS NOT NULL
--	DROP TABLE #temp_Terminal ;

--SELECT *
--INTO #temp_Terminal
--FROM(
--	SELECT 
--		*
--	FROM 
--		[DBO].[B_Terminal]
--	WHERE
--		[B_Terminal].[machine] = CASE WHEN @TipoCliente = 1 THEN [B_Terminal].[machine] ELSE @ClientName END
--		AND [B_Terminal].[no] = CASE WHEN @TipoCliente = 1 THEN 90 ELSE [B_Terminal].[no] END 
--)x
	
--/************************************
--5 - Verifica o numero de resultados 
--*************************************/
--IF @@ROWCOUNT = 0 /* Se n�o tiver resultados insere o terminal */
--BEGIN
--	INSERT INTO [dbo].[B_Terminal](
--		[tstamp],
--		[machine],
--		[terminal],
--		[no],
--		[armazem],
--		[Site],
--		[Atendimento],
--		[TS]
		
--	)
--	SELECT
--		[tstamp] = LEFT(NEWID(),25) ,
--		[machine] = CASE WHEN @TipoCliente = 1 THEN '' ELSE @ClientName END ,
--		[terminal] = CASE WHEN @TipoCliente = 1 
--						THEN 'Terminal 90' 
--						ELSE ISNULL((SELECT 'Terminal ' + Convert(varchar,(MAX([no]) + 1)) FROM [dbo].[B_Terminal] WHERE [no] < 89),'Terminal 1')
--					 END,		
--		[no] = CASE WHEN @TipoSessao = 1 
--					THEN 90 
--					ELSE ISNULL((SELECT (MAX([no]) + 1) FROM [dbo].[B_Terminal] WHERE [no] < 89),CASE WHEN @TipoCliente = 1 THEN 90 ELSE 1 END)
--			   END,	
--		[armazem] = isnull((select top 1 armazem1 from B_Lojas where conta = @Conta),1),
--		[Site] = isnull((select top 1 site from B_Lojas where conta = @Conta),'Loja 1'),
--		[Atendimento] = CASE WHEN @TipoCliente = 1 THEN 1 ELSE 0 END ,	
--		[ts] = CASE WHEN @TipoSessao = 1 THEN 1 ELSE 0 END	
	
--	/* Devolve resultados novamente */
--	GOTO DEVOLVE_RESULTADOS
--END
--ELSE
--BEGIN
--	SELECT * FROM #temp_Terminal
	
--	/*******************************
--	 Remove tabelas temporarias
--	*******************************/
--	IF OBJECT_ID('tempdb.dbo.#temp_Terminal') IS NOT NULL
--		DROP TABLE #temp_Terminal ;
--END