/*
exec up_reports_numChamadas '20221028','20221228',''
*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


if OBJECT_ID('[dbo].[up_reports_numChamadas]') IS NOT NULL
	drop procedure dbo.up_reports_numChamadas
go

Create procedure [dbo].[up_reports_numChamadas]
	@dataIni				DATETIME
	,@dataFim				DATETIME
	,@site					VARCHAR(254)

AS

	select
		RIGHT(ItemPath, CHARINDEX('/',REVERSE(ItemPath))-1)  as name , 
		count(*) as Chamadas
	from 
		ExecutionLog3 (nolock)
	WHERE 
		CONVERT(DATE,TimeStart) between @dataIni and @dataFim and 
		UserName like (case when @site='' then '%%' else '%'+@site+'%' end)
	group by 
		ItemPath
	order by 
		count(*) desc

GO
Grant Execute On dbo.up_reports_numChamadas to Public
Grant control On dbo.up_reports_numChamadas to Public
GO