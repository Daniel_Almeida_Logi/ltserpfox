/*
	Mapa de Registo de Sa�das de Psicotr�picos (Gest�o de Psico / Benzo)
	
	 exec up_get_sitePorArmazem 1
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_get_sitePorArmazem]') IS NOT NULL
    DROP PROCEDURE dbo.up_get_sitePorArmazem
GO

CREATE PROCEDURE dbo.up_get_sitePorArmazem

	@armazem INT

/* with encryption */
AS

SET NOCOUNT ON
	
	SELECT 
		site
		,no
	FROM 
		empresa_arm(nolock)
	INNER JOIN
		empresa(nolock) ON empresa_arm.empresa_no = empresa.no
	WHERE
		empresa_arm.armazem = @armazem

GO
Grant Execute On dbo.up_get_sitePorArmazem to Public
Grant Control On dbo.up_get_sitePorArmazem to Public
go