-- Dados das lojas --
-- �ltima Altera��o: 2012-08-03 --
-- exec up_gerais_dadosLojas ''

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_gerais_dadosLojas]') IS NOT NULL
	drop procedure up_gerais_dadosLojas
go

create PROCEDURE up_gerais_dadosLojas

@site nvarchar(69)

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

if (@site='')
begin
	select
		no,ref, site, status, serie_vd, serie_fact, serie_regcli, morada, empresa, contribuinte = ncont, entidade, basedados, codigo, localPresc, pagcentral, prescricao, siteposto, infarmed, serie_ent, serie_entSNS, anfpassfx
	from
		empresa (nolock)
	where
		site = case when @site='' then site else @site end
		and status != 'I'
		
	union all

	select 
		0,'','','',0,0,0,'',0,'',0,'','','',0,0,'',0,0,0,''
end
else
begin
	select
		no, ref, site, status, serie_vd, serie_fact, serie_regcli, morada, empresa, contribuinte = ncont, entidade, basedados, codigo, localPresc, pagcentral, prescricao, serie_ent, serie_entSNS, anfpassfx
	from
		empresa (nolock)
	where
		site = case when @site='' then site else @site end
end


GO
Grant Execute On up_gerais_dadosLojas to Public
Grant Control On up_gerais_dadosLojas to Public
GO