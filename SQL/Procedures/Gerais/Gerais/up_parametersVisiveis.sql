-- Listar Parāmetros
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_parametersVisiveis]') IS NOT NULL
	drop procedure dbo.up_parametersVisiveis
go

create procedure dbo.up_parametersVisiveis
	@user as varchar (10) = ''
/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select
	convert(numeric,REPLACE(REPLACE(stamp,'ADM',''),'CLI','1')) as id,*
from 
	b_parameters (nolock)
Where
	visivel = case when @user='ADM' THEN visivel ELSE 1 END
order by 
	convert(numeric,REPLACE(REPLACE(stamp,'ADM',''),'CLI','1'))
	


GO
Grant Execute on dbo.up_parametersVisiveis to Public
Grant Control on dbo.up_parametersVisiveis to Public
Go