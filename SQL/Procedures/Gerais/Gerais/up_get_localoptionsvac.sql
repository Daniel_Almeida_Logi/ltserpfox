/* 
	Procedere para retornar op��es da farm�cia para vacinacao
	exec up_get_localoptionsvac 'VACINACAO_LOCAL_ANATOMICO'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_localoptionsvac]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_localoptionsvac
GO

CREATE PROCEDURE dbo.up_get_localoptionsvac
	@tipo				varchar(60)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT
		--code as id
		NTILE(100) OVER (ORDER BY code) id
		,campo as description
		 
	FROM
		b_multidata(NOLOCK)
	WHERE
		tipo = @tipo
	ORDER BY
		campo ASC

GO
GRANT EXECUTE on dbo.up_get_localoptionsvac TO PUBLIC
GRANT Control on dbo.up_get_localoptionsvac TO PUBLIC
GO