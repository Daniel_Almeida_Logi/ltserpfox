/*
	
	exec up_gerais_filtro_docs_regularizacoes 

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_filtro_docs_regularizacoes]') IS NOT NULL
	drop procedure dbo.up_gerais_filtro_docs_regularizacoes
go

create procedure dbo.up_gerais_filtro_docs_regularizacoes

/* WITH ENCRYPTION */

AS
	; with cte as (
	select '' as tipo, 1 as ordem
	union
	select 'Suspensas' as tipo, 1 as ordem
	union
	select 'Cr�ditos' as tipo, 2 as ordem
	union
	select 'Reservas' as tipo, 3 as ordem
	union
	select 'Encomendas' as tipo, 4 as ordem)
	select tipo from cte order by ordem


GO
Grant Execute On dbo.up_gerais_filtro_docs_regularizacoes to Public
Grant Control On dbo.up_gerais_filtro_docs_regularizacoes to Public
GO