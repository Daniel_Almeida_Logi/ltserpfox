/*
	Insere registo nas Ocorrencias
	exec up_gerais_registaOcorrencia 'xxx','yyyy','tipo',1,'asdsad','ovalor','dvalor',1,'terminal 99','loja 99'
	
	exec up_gerais_registaOcorrencia '14143171P6yGVha2r3Hm', '', 'CashGuard', 2, 'Levantamentos de valor', '500', '', 10, 'Cashguard', 'Loja 1'
	
	String sql = "exec up_gerais_registaOcorrencia "
                + "'" + stamp + "'"
                + ", '" + linkStamp + "'"
                + ", 'CashGuard'"
                + ", 2"
                + ", '" + descr + "'"
                + ", '" + valor + "'"
                + ", ''"
                + ", " + vendedor
                + ", 'Cashguard'"
                + ", '" + Conf.getParameter("Database_Connection", 4) + "'";
                
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_registaOcorrencia]') IS NOT NULL
	drop procedure dbo.up_gerais_registaOcorrencia
go

create procedure dbo.up_gerais_registaOcorrencia
	@stamp varchar(30)
	,@linkStamp varchar(30)
	,@tipo varchar(60)
	,@grau numeric(5)
	,@descr varchar(max)
	,@oValor varchar(60)
	,@dValor varchar(254)
	,@user numeric(5)
	,@terminal varchar(30)
	,@site varchar(20)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	INSERT INTO	b_ocorrencias 
		(Stamp,linkStamp,Tipo,Grau,descr,oValor,dValor,usr,date,terminal,site)
	VALUES 
		(
		@stamp
		,@linkStamp
		,@tipo
		,@grau
		,@descr
		,@oValor
		,@dValor
		,@user
		,GETDATE()
		,@terminal
		,@site
		)

GO
Grant Execute On dbo.up_gerais_registaOcorrencia to Public
Grant Control On dbo.up_gerais_registaOcorrencia to Public
GO