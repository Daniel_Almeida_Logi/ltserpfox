
/****** Object:  StoredProcedure [dbo].[sp_insert_utente_loja_dc_atendimento]    Script Date: 8/24/2021 10:19:53 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
if OBJECT_ID('[dbo].[sp_insert_cartao_loja_dc]') IS NOT NULL
	drop procedure dbo.sp_insert_cartao_loja_dc
go
create PROCEDURE [dbo].[sp_insert_cartao_loja_dc]
@stamp		varchar(50)
,@no		numeric(20,0)
,@estab		numeric(20,0)
,@nrcartao	varchar(30)

AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON

	declare @nrcartaoatual varchar(10)
	set @nrcartaoatual = (select nrcartao from b_utentes where no=@no and estab=@estab)

	BEGIN TRY  
		IF(1=1 )
		BEGIN
		  BEGIN TRAN
			update b_fidel set inactivo=1 where nrcartao=@nrcartaoatual

			update b_utentes set nrcartao=@nrcartao where no=@no and estab=@estab
						
			insert into B_fidel (fstamp, clstamp, clno, clestab, nrcartao, validade, ousrdata, usrdata)
			select left(newid(),21), @stamp, @no, @estab, @nrcartao, '30001231', getdate(), getdate()

		
		  COMMIT TRAN
		END
	END TRY 
	BEGIN CATCH  
	   ROLLBACK TRAN
	END CATCH  
		SET XACT_ABORT OFF
--select no from b_utentes where utstamp=@stamp
print 'cart�o criado'
END
