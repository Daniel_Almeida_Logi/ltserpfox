/*

	recebe token com informação a imprimir

	exec up_gerais_getInfoToPrint 'adm'


	exec up_gerais_getInfoToPrint 'adm'

	
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



if OBJECT_ID('[dbo].[up_gerais_getInfoToPrint]') IS NOT NULL
	drop procedure dbo.up_gerais_getInfoToPrint
go

create procedure dbo.up_gerais_getInfoToPrint
	@token varchar(25) = ''

/* WITH ENCRYPTION */

AS
	begin 
		
		set @token = RTRIM(LTRIM(isnull(@token,'')))

		select 
			token,
			docName,
			imagePath,
			printerName,
			[date],
			[order],
			feedcut
		from 
			image_printPos(nolock)
		where 
			token = @token
		
	end
GO
Grant Execute On dbo.up_gerais_getInfoToPrint to Public
Grant Control On dbo.up_gerais_getInfoToPrint to Public
GO