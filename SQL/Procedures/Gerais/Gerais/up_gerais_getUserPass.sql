/*
	Levanta dados de utilizador
	exec up_gerais_getUserPass 'sa','123'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_getUserPass]') IS NOT NULL
	drop procedure dbo.up_gerais_getUserPass
go

create procedure dbo.up_gerais_getUserPass
	@user varchar(30)
	,@pass varchar(20)


/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT 
		* 
	FROM 
		b_us (nolock) 
	WHERE 
		userpass = @pass and username=@user

GO
Grant Execute On dbo.up_gerais_getUserPass to Public
Grant Control On dbo.up_gerais_getUserPass to Public
GO