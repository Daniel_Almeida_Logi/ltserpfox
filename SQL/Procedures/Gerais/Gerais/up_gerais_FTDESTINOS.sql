-- SP de Origens no Documento de Facturação
-- exec up_gerais_FTDESTINOS '',''
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_FTDESTINOS]') IS NOT NULL
	drop procedure dbo.up_gerais_FTDESTINOS
go

create procedure dbo.up_gerais_FTDESTINOS
@Fistamp as varchar(25),
@Ftstamp as varchar(25)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	;with cte1 as (
		Select		
			Tipo = 'FT', 
			Doc = RTRIM(LTRIM(Fi.nmdoc)) + ' Nr.' + RTRIM(LTRIM(STR(Fi.fno)))
			,Informacao = 'Ref.: ' + RTRIM(LTRIM(Ref)) + ' | Total Valor: ' + RTRIM(LTRIM(STR(etiliquido,9,2))) + ' | Total Qtt: ' + RTRIM(LTRIM(STR(Qtt))) + ' | Data: ' + Left(Convert(varchar,Fdata,121),10) + ' | ' +  Rtrim(Ltrim(Ft.Ftstamp))
			,stamporidest = Ft.Ftstamp
		From	
			Fi (nolock)
			Inner Join Ft (nolock) On ft.ftstamp = fi.ftstamp
		Where	
			ofistamp = @Fistamp /*Fi.Fistamp*/
	), cte2 as (	
		select 
			DISTINCT Tipo = 'RE',
			Doc = 'Recibo: ' + RTRIM(LTRIM(Re.nmdoc)) + ' Nr.' + RTRIM(LTRIM(STR(Re.rno))),
			Informacao = 'Por Regularizar.: ' + RTRIM(LTRIM(STR(rl.eval,9,2))) + ' | Regularizado: ' + RTRIM(LTRIM(STR(erec,9,2))) + ' | Data: ' + Left(Convert(varchar,re.rdata,121),10) + ' | ' + Rtrim(Ltrim(re.restamp))
			,stamporidest = re.restamp
		from 
			rl (nolock)
			inner join fi (nolock) on fi.ftstamp=rl.ccstamp
			inner join re (nolock) on re.restamp=rl.restamp
		where 
			ftstamp = @Ftstamp  /*Ft.Ftstamp*/
	)
	Select 
		RowNumber = ROW_NUMBER() OVER (ORDER BY stamporidest)
		, *
	From	
	(
		Select * From cte1 
		UNION ALL
		Select * From cte2
	) x
	

GO
Grant Execute On dbo.up_gerais_FTDESTINOS to Public
Grant Control On dbo.up_gerais_FTDESTINOS to Public
GO