-- SP de Origens no Documento de Facturação
-- exec up_gerais_DOCORIGENS_FO 'FO','','','','',''
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_DOCORIGENS_FO]') IS NOT NULL
	drop procedure dbo.up_gerais_DOCORIGENS_FO
go

create procedure dbo.up_gerais_DOCORIGENS_FO
@oFnstamp as varchar(25),
@Bistamp as varchar(25)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	
	;with cte1 as (
		Select	
			Tipo = 'FO',
			Doc = RTRIM(LTRIM(FN.docnome)) + ' Nr.' + RTRIM(LTRIM(fn.adoc)),
			Informacao = 'Ref.: ' + RTRIM(LTRIM(Ref)) + ' | Total Valor: ' + RTRIM(LTRIM(STR(etiliquido,9,2))) + ' | Total Qtt: ' + RTRIM(LTRIM(STR(Qtt))) + ' | Data: ' + Left(Convert(varchar,docdata,121),10) + ' | ' + Rtrim(Ltrim(fn.fostamp))  
			,stamporidest = Fo.fostamp
		From	
			FN (nolock)
			INNER JOIN Fo (nolock) On fn.fostamp = fo.fostamp
		Where	
			fnstamp = @oFnstamp /*Fn.oFnstamp*/
	), cte2 as (
		Select 	
			Tipo = 'BO', 
			Doc = RTRIM(LTRIM(Bi.nmdos)) + ' Nr.' + RTRIM(LTRIM(STR(bi.obrano))),
			Informacao = 'Ref.: ' + RTRIM(LTRIM(Ref)) + ' | Total Valor: ' + RTRIM(LTRIM(STR(ettdeb,9,2))) + ' | Total Qtt: ' + RTRIM(LTRIM(STR(Qtt))) + ' | Data: ' + Left(Convert(varchar,dataobra,121),10) + ' | ' + Rtrim(Ltrim(Bi.bostamp))
			,stamporidest = Bostamp
		From    	
			bi (nolock)
		Where 
			bistamp = @Bistamp /*Fn.Bistamp*/
	)	
	Select 
		RowNumber = ROW_NUMBER() OVER (ORDER BY stamporidest)
		, *
	From	
	(
		Select * From cte1 
		UNION ALL
		Select * From cte2
	) x

GO
Grant Execute On dbo.up_gerais_DOCORIGENS_FO to Public
Grant Control On dbo.up_gerais_DOCORIGENS_FO to Public
GO