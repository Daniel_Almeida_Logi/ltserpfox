-- Procurar Parametro
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_parameters_procura]') IS NOT NULL
	drop procedure dbo.up_parameters_procura
go

create procedure dbo.up_parameters_procura

@stamp varchar(30)

/* WITH ENCRYPTION */


AS

SET NOCOUNT ON

select
	stamp 
	,bool
	,textValue
	,numValue
from 
	b_parameters (nolock)
where
	stamp=@stamp

GO
Grant Execute on dbo.up_parameters_procura to Public
Grant Control on dbo.up_parameters_procura to Public
Go