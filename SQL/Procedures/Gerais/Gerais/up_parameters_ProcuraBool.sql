-- Procurar Valor L�gico nos Par�metros
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_parameters_ProcuraBool]') IS NOT NULL
	drop procedure dbo.up_parameters_ProcuraBool
go

create procedure dbo.up_parameters_ProcuraBool

@stamp varchar(30)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select bool from b_parameters where stamp=@stamp


GO
Grant Execute on dbo.up_parameters_ProcuraBool to Public
Grant Control on dbo.up_parameters_ProcuraBool to Public
Go