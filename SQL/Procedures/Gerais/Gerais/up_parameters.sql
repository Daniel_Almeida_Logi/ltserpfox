-- exec up_parameters

-- Listar Parāmetros
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_parameters]') IS NOT NULL
	drop procedure dbo.up_parameters
go

create procedure dbo.up_parameters

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select
	convert(numeric,REPLACE(REPLACE(stamp,'ADM',''),'CLI','1')) as id
	,RTRIM(LTRIM(stamp)) AS stamp
	,name
	,type 
	,left(convert(varchar(254),Textvalue),254) as textvalue
	,numvalue
	,bool
	,oDate
	,ldate
	,visivel
	,MostraTextValue ,MostraNumValue, MostraBool, ListaTextValue
	,Unidades
	,obs
from 
	b_parameters (nolock)
order by 
	convert(numeric,REPLACE(REPLACE(stamp,'ADM',''),'CLI','1'))
	

GO
Grant Execute on dbo.up_parameters to Public
Grant Control on dbo.up_parameters to Public
Go