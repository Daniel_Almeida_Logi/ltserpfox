-- Procurar Valor Texto nos Parâmetros
-- exec up_parameters_procuratexto 'adm0000000114'



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



if OBJECT_ID('[dbo].[up_parameters_ProcuraTexto]') IS NOT NULL
	drop procedure dbo.up_parameters_ProcuraTexto
go

create procedure dbo.up_parameters_ProcuraTexto

@stamp varchar(30)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select rtrim(ltrim(textValue)) as textValue from b_parameters where stamp=@stamp


GO
Grant Execute on dbo.up_parameters_ProcuraTexto to Public
Grant Control on dbo.up_parameters_ProcuraTexto to Public
GO