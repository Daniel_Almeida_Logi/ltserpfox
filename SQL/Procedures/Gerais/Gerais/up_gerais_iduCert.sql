-- C�digo de certifica��o para as impress�es --
-- exec up_gerais_iduCert 'ADM2C9B772D-119D-4FEE-867'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_iduCert]') IS NOT NULL
	drop procedure dbo.up_gerais_iduCert
go

create procedure dbo.up_gerais_iduCert

@stamp varchar(30)

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

Select	
	 parte1 = codHash
	,parte2 = Convert(varchar(254), case when (select pais from empresa where empresa.site =  x.site)  = 'Angola'
										then 'Processado por programa validado n.'
										else 'Processado por programa certificado n.' 
									end
									+ nrCert + 
									case when (select top 1 isnull(nmdoc,'') from ft (nolock) where @stamp = ft.Ftstamp) = 'Venda Manual'
										then ' - Copia do documento original-' + Rtrim(versaochave) + '-' + Rtrim(invoicetype) + 'M'
										else ''
									end)
	,temCert
	,versaochave
	,invoicetype
From (
	Select	
		codHash			=	SUBSTRING (hash,1,1) + SUBSTRING (hash,11,1) + SUBSTRING (hash,21,1) + SUBSTRING (hash,31,1)
		,nrCert			=	isnull((select textvalue from B_Parameters (nolock) where stamp='ADM0000000078'),'')
		,temCert		=	isnull((select bool from B_Parameters (nolock) where stamp='ADM0000000078'),'')
		,versaochave	=	versaochave
		,invoicetype	=	invoicetype
		,site
	From	B_cert (nolock)
	Where stamp = @stamp
) x


GO
Grant Execute on dbo.up_gerais_iduCert to Public
Grant Control on dbo.up_gerais_iduCert to Public
GO