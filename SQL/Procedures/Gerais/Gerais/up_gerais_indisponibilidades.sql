-- exec up_gerais_indisponibilidades 1
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_indisponibilidades]') IS NOT NULL
	drop procedure dbo.up_gerais_indisponibilidades
go

create procedure dbo.up_gerais_indisponibilidades
@userno numeric(6,0)


/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	Select 
		*
	from 
		b_us_indisponibilidades
	where 
		userno = @userno
	Order by dataInicio, dataFim, horaInicio, horafim	


GO
Grant Execute on dbo.up_gerais_indisponibilidades to Public
Grant Control on dbo.up_gerais_indisponibilidades to Public
Go