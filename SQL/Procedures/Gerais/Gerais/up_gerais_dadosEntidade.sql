-- Dados da Entidade --
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_dadosEntidade]') IS NOT NULL
	drop procedure dbo.up_gerais_dadosEntidade
go

create procedure dbo.up_gerais_dadosEntidade

@site varchar(69)

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

select nome, no, ncont, morada, local, codpost, telefone ,tlmvl, fax,codigo, email
from ag (nolock)
where no = isnull((select entidade from empresa (nolock) where site=@site),0)


GO
Grant Execute on dbo.up_gerais_dadosEntidade to Public
Grant Control on dbo.up_gerais_dadosEntidade to Public
GO