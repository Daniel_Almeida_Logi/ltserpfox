-- SP de Origens no Documento de Facturação
-- exec up_gerais_FTORIGENS 'ADM12061164878.295000016'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_FTORIGENS]') IS NOT NULL
	drop procedure dbo.up_gerais_FTORIGENS
go

create procedure dbo.up_gerais_FTORIGENS
@oFistamp as varchar(25),
@bistamp as varchar(25)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	


;With cte1 as (
	Select	
		Tipo = 'FT',
		Doc = RTRIM(LTRIM(Fi.nmdoc)) + ' Nr.' + RTRIM(LTRIM(STR(Fi.fno))),
		Informacao = 'Ref.: ' + RTRIM(LTRIM(Ref)) + ' | Total Valor: ' + RTRIM(LTRIM(STR(etiliquido,9,2))) + ' | Total Qtt: ' + RTRIM(LTRIM(STR(Qtt))) + ' | Data: ' + Left(Convert(varchar,Fdata,121),10) + ' | ' + Rtrim(Ltrim(fi.ftstamp)),
		stamporidest  = Ft.ftstamp 
	From	
		Fi (nolock) 
		Inner Join Ft (nolock) On ft.ftstamp = fi.ftstamp
	Where	
		fistamp = @oFistamp
), cte2 as (
	Select 	
		Tipo = 'BO',
		Doc = RTRIM(LTRIM(Bi.nmdos)) + ' Nr.' + RTRIM(LTRIM(STR(bi.obrano))),
		Informacao = 'Ref.: ' + RTRIM(LTRIM(Ref)) + ' | Total Valor: ' + RTRIM(LTRIM(STR(ettdeb,9,2))) + ' | Total Qtt: ' + RTRIM(LTRIM(STR(Qtt))) + ' | Data: ' + Left(Convert(varchar,dataobra,121),10) + ' | ' + Rtrim(Ltrim(bi.bostamp)),
		stamporidest = Bostamp
	From    
		bi (nolock)
	Where   
		bistamp = @Bistamp /*&&Fi.Bistamp*/
)

Select 
	RowNumber = ROW_NUMBER() OVER (ORDER BY stamporidest)
	, *
From	
(
	Select * From cte1 
	UNION ALL
	Select * From cte2
) x

GO
Grant Execute On dbo.up_gerais_FTORIGENS to Public
Grant Control On dbo.up_gerais_FTORIGENS to Public
GO