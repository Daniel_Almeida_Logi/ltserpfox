/*  Dados das empresas
	�ltima Altera��o: 2015-06-08 - Alterado para mostrar o Nome da Empresa e n�o a designa��o social

	 exec up_gerais_trocaEmpresa 1, 'Administrador'

	 exec up_gerais_trocaEmpresa 0, ''


	Select * from b_pf where grupo = 'Empresa' and resumo like '%- Acesso'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_gerais_trocaEmpresa]') IS NOT NULL
	drop procedure up_gerais_trocaEmpresa
go

create PROCEDURE up_gerais_trocaEmpresa

@userno numeric(5,0)
,@grupo varchar(20)

/* WITH ENCRYPTION */

AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempPfu'))
		DROP TABLE #dadosVendasBase
		
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempPfg'))
		DROP TABLE #tempPfg
			
	Select 
		b_pfu.*
	into
		#tempPfu
	From
		b_pf
		inner join b_pfu on b_pfu.pfstamp = b_pf.pfstamp
	Where
		b_pf.resumo  = 'Acesso � empresa'
		
	Select 
		b_pfg.*
	into
		#tempPfg
	From
		b_pf
		inner join b_pfg on b_pfg.pfstamp = b_pf.pfstamp
	Where
		b_pf.resumo  = 'Acesso � empresa'
		
		
	declare @userSiteDefault varchar(20)
	set @userSiteDefault = (select loja from b_us where userno = @userno)

	select 
		distinct 
		Local = RTRIM(LTRIM(empresa.site))
		,Designacao = nomabrv 
		,siteno = empresa.no
		,armazem1= (select top 1 armazem from empresa_arm Where	empresa_arm.empresa_no = empresa.no	order by armazem)
	from 
		empresa 
		left join #tempPfu pfu (nolock) on RTRIM(LTRIM(pfu.site)) = RTRIM(LTRIM(empresa.site))
		left join #tempPfg pfg (nolock) on RTRIM(LTRIM(pfg.site)) = RTRIM(LTRIM(empresa.site))
	Where
		(isnull(pfu.userno,9999) = @userno Or ISNULL(pfg.nome,'') = @grupo)
		OR empresa.site = @userSiteDefault /* Tem sempre acesso � loja default do utilizador */
	order by
		RTRIM(LTRIM(empresa.site))
		

GO
Grant Execute On up_gerais_trocaEmpresa to Public
Grant Control On up_gerais_trocaEmpresa to Public
GO