/* 
	
	Devolve series da TS por loja
	EXEC up_get_editSeriesTs 'Loja 1'


*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_editSeriesTs]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_editSeriesTs
GO

CREATE PROCEDURE dbo.up_get_editSeriesTs 	
	@site VARCHAR(60)

AS

	SELECT 
		CAST('TS' AS varchar(2)) as tabela,
		nmdos as nome,
		tsstamp as stamp,
		resetNumeracaoAnual,
		resetNumeracaoAnual as resetNumeracaoAnualOri,
		atcud
	FROM 
		ts(nolock)
	WHERE
		1 = (CASE 
				WHEN site = '' AND NOT EXISTS (SELECT 1 FROM ts(nolock) as tts WHERE tts.nmdos = ts.nmdos AND tts.tsstamp <> ts.tsstamp AND tts.site <> '') 
					THEN 1 
				ELSE
					(CASE WHEN site = @site THEN 1 ELSE 0 END)  
			END)
	ORDER BY
		nmdos ASC

GO
GRANT EXECUTE on dbo.up_get_editSeriesTs TO PUBLIC
GRANT Control on dbo.up_get_editSeriesTs TO PUBLIC
GO
