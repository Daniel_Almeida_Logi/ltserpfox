/*
	exec [up_gerais_no_ext_OCP]
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_no_ext_OCP]') IS NOT NULL
	drop procedure dbo.[up_gerais_no_ext_OCP]
go

Create procedure [dbo].[up_gerais_no_ext_OCP]
@nr_fl			VARCHAR(55) = '0'


AS
SET NOCOUNT ON
	
	select top 1 
	fl.no_ext
	from fl (nolock) 
	where nome like '%OCP%' and no_ext <>''

GO
Grant Execute On dbo.up_gerais_no_ext_OCP to Public
Grant Control On dbo.up_gerais_no_ext_OCP to Public
Go
