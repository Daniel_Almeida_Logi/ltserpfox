-- lista de documentos de uma referÍncia
-- exec up_gerais_listaLinhasDoc 'asd', 'FT'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_listaLinhasDoc]') IS NOT NULL
	drop procedure dbo.up_gerais_listaLinhasDoc
go

create procedure dbo.up_gerais_listaLinhasDoc

@stamp		varchar(25),
@tabela		varchar(2)

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

	if @tabela = 'FO'
	begin
		SELECT
			REF			= ref
		    ,DESIGN     = design
		    ,EPV        = epv
		    ,TVENDA     = etiliquido
		    ,QTT        = qtt
		    ,qttmov		= pbruto
		FROM
			fn (nolock)
		WHERE
			fostamp= @stamp
	end
	else
	if @tabela = 'BO'
	begin
		SELECT
			REF			= ref
		    ,DESIGN     = design
		    ,EPV        = edebito
		    ,TVENDA     = ettdeb
		    ,QTT        = qtt
		    ,qttmov		= pbruto
		FROM
			bi (nolock)
		WHERE
			bostamp= @stamp
	end
	else
	if @tabela = 'FT'
	begin
		SELECT
			REF			= ref
		    ,DESIGN     = design
		    ,EPV        = epv
		    ,TVENDA     = etiliquido
		    ,QTT        = qtt
		    ,qttmov		= qtt
		FROM
			fi (nolock)
		WHERE
			ftstamp= @stamp
	end

GO
Grant Execute On dbo.up_gerais_listaLinhasDoc to Public
Grant Control On dbo.up_gerais_listaLinhasDoc to Public
GO