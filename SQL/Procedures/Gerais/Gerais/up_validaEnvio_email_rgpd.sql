/* 
	exec up_validaEnvio_email_rgpd 'gau@mass.pt',22,0
	exec up_validaEnvio_email_rgpd 'fgrseixas@gmail.com;suporte@logitools.pt',107902511,0
	exec up_validaEnvio_email_rgpd 'fgrseixas@gmail.',107902511,0
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_validaEnvio_email_rgpd]') IS NOT NULL
	drop procedure dbo.up_validaEnvio_email_rgpd
go

create procedure dbo.up_validaEnvio_email_rgpd
	@email  VARCHAR (254),
	@no		INT,
	@estab	INT,
	@tabela	VARCHAR(100) = ''

/* WITH ENCRYPTION */
AS	
		IF OBJECT_ID('tempdb.dbo.#emailClienteSplit') IS NOT NULL
			DROP TABLE #emailClienteSplit

		DECLARE @deveEnviar bit = 0 
		DECLARE @ncont varchar(100) = '' 
		DECLARE @authEmail bit = 0
		DECLARE @emailCliente	VARCHAR(254) = ''
		DECLARE @countCliente INT = 0
		DECLARE @countEntrada INT = 0

		set @tabela = dbo.alltrimIsNull(@tabela)

		if (@tabela='FO' or @tabela='BO' or @tabela='PO')
		begin
			select CONVERT(bit, 1) as deveEnviar
			return
		end


		
		SET @countEntrada = (Select COUNT(items) from dbo.up_splitToTable(@email,';'))

		SELECT 
			@ncont = dbo.alltrimIsNull(ncont),
			@authEmail = case when autorizado = 1 and autoriza_emails=1 AND email !='' and @email!=''  then 1 else 0 end,
			@emailCliente = dbo.alltrimIsNull(email)
		FROM
			b_utentes(NOLOCK)
		WHERE
			b_utentes.no = @no
			AND b_utentes.estab = @estab

		print @authEmail
		if(left(@ncont,1) in ('5','6'))
		begin
			select CONVERT(bit, 1) as deveEnviar
		end
		else begin

		SELECT * INTO #emailClienteSplit FROM  dbo.up_splitToTable(@emailCliente,';')
		SELECT @countCliente=COUNT(*) FROM #emailClienteSplit
			WHERE #emailClienteSplit.items in (Select items from dbo.up_splitToTable(@email,';'))

			IF(@countCliente = @countEntrada)
			BEGIN
				 select isnull(@authEmail,0) as deveEnviar
			END
			ELSE
			BEGIN
				select  CONVERT(bit, 0) as deveEnviar
			END
		end
		
	
		IF OBJECT_ID('tempdb.dbo.#emailClienteSplit') IS NOT NULL
			DROP TABLE #emailClienteSplit
GO
Grant Execute on dbo.up_validaEnvio_email_rgpd to Public
Grant Control on dbo.up_validaEnvio_email_rgpd to Public
GO
