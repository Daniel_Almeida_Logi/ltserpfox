-- exec up_gerais_disponibilidades 'utilizadores',1,0
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_disponibilidades]') IS NOT NULL
	drop procedure dbo.up_gerais_disponibilidades
go

create procedure dbo.up_gerais_disponibilidades
@tipo varchar(30)
,@no numeric(10,0)
,@estab numeric(3,0)

/* WITH ENCRYPTION */
AS

	Select 
		id_disp=convert(int,ROW_NUMBER() OVER(ORDER BY dataInicio
											,dataFim
											,horaInicio
											,horafim ))
		,seriestamp
		,serieno
		,serienome
		,no
		,estab
		,nome
		,dirClinico
		,dirServico
		,dataInicio
		,sessoes
		,ousrinis
		,ousrdata
		,ousrhora
		,usrinis
		,usrdata
		,usrhora
		,horaInicio
		,horafim
		,segunda
		,terca
		,quarta
		,quinta
		,sexta
		,sabado
		,sempre
		,tododia
		,domingo
		,dataFim
		,dataIniRep
		,duracao
		,tipo
		,site
		,servicos = case when exists (Select top 1 seriestamp from b_us_disp_resursos where b_us_disp_resursos.seriestamp = b_series.seriestamp) then '...' else '' end 
	from 
		b_series 
	where 
		tipo = @tipo
		and no = @no
		and estab = @estab
	Order by 
		dataInicio
		,dataFim
		,horaInicio
		,horafim
	


GO
Grant Execute on dbo.up_gerais_disponibilidades to Public
Grant Control on dbo.up_gerais_disponibilidades to Public
Go