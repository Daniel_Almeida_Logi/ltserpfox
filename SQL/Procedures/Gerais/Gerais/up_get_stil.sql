/* 
	exec up_get_stil '',0,1
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_stil]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_stil
GO

CREATE PROCEDURE dbo.up_get_stil
	
	@sticstamp		VARCHAR(36),
	@valida			BIT,
	@pesquisa		BIT

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON
	
	DECLARE @sql VARCHAR(4000) = ''
	
	SET @sql = @sql + N'
	SELECT 
		pesquisa		= ''' + CONVERT(VARCHAR(1), @valida) +'''
		,ltValida		=  '''+ CONVERT(VARCHAR(1),@pesquisa) +'''
		,stil.ref
		,stil.design
		,data
		,stock
		,sticstamp
		,armazem
		,zona
		,pcpond
		,epcpond
		,lordem
		,u_qtt
		,exportado
		,lote
		,psico
		,benzo
	FROM stil (NOLOCK)
	INNER JOIN fprod(NOLOCK) ON stil.ref = fprod.ref
	WHERE
		1 = 1 '
	IF @sticstamp != ''
		SET @sql = @sql + N' AND sticstamp = '''+ @sticstamp +''' '
	
	PRINT @sql
	EXEC(@sql)
GO
GRANT EXECUTE on dbo.up_get_stil TO PUBLIC
GRANT Control on dbo.up_get_stil TO PUBLIC
GO