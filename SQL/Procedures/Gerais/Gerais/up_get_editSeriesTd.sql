/* 
	
	Devolve series da Td por loja
	EXEC up_get_editSeriesTd 'Loja 1'


*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_editSeriesTd]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_editSeriesTd
GO

CREATE PROCEDURE dbo.up_get_editSeriesTd	
	@site VARCHAR(60)

AS

	SELECT 
		CAST('TD' AS varchar(2)) as tabela,
		nmdoc as nome,
		tdstamp as stamp,
		resetNumeracaoAnual,
		resetNumeracaoAnual as resetNumeracaoAnualOri,
		ATCUD
	FROM 
		td(nolock)
	WHERE
		site = @site
	ORDER BY
		nmdos ASC

GO
GRANT EXECUTE on dbo.up_get_editSeriesTd TO PUBLIC
GRANT Control on dbo.up_get_editSeriesTd TO PUBLIC
GO
