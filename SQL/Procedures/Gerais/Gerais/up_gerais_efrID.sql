/*  
	Procedore para selecionar o efrid dinamicamente 
	
	exec up_gerais_efrID '1'
*/

if OBJECT_ID('[dbo].[up_gerais_efrID]') IS NOT NULL
	drop procedure dbo.up_gerais_efrID
go

create procedure [dbo].[up_gerais_efrID]
	@no			int = 0

AS

			SELECT 
				efrID = rtrim(ltrim(isnull(efrID,''))),
				abrev = rtrim(ltrim(isnull(abrev,''))),
				fatEletronica =  (select top 1 e_compart from cptpla(nolock) where cptpla.cptorgstamp = cptorg.cptorgstamp and inactivo = 0)
			FROM 
				cptorg(nolock)
			WHERE
				u_no = @no
GO
Grant Execute on dbo.up_gerais_efrID to Public
Grant control on dbo.up_gerais_efrID to Public
GO