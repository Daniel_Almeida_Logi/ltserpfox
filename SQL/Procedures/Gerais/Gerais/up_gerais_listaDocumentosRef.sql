-- lista de documentos de uma referÍncia
-- exec up_gerais_listaDocumentosRef '8168617', 'Venda a Dinheiro', '20000101', '30000101', 'FT'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_listaDocumentosRef]') IS NOT NULL
	drop procedure dbo.up_gerais_listaDocumentosRef
go

create procedure dbo.up_gerais_listaDocumentosRef

@ref		varchar(18),
@nmdoc		varchar(24),
@dataini	datetime,
@datafim	datetime,
@tabela		varchar(2),
@porRef		bit			= 1,
@no			numeric(10)	= 0,
@estab		numeric(3)	= 0

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

if @porRef = 1
begin
	if @tabela = 'FO'
	begin
		SELECT
			DOCDATA			= convert(varchar,fo.docdata,102)
			,HORA			= fo.ousrhora
			,DOCUMENTO	 	= fo.docnome
			,NDOC			= fo.adoc
			,VALOR			= fo.etotal
			,QTTOTAL		= (select SUM(QTT) from fn (nolock) where fn.fostamp=fo.fostamp)
			,IDADE			= datediff(d,fo.docdata,GETDATE())
			,OPERADOR		= b_us.username
			,SEL			= CONVERT(bit,0)
			,STAMP			= fo.fostamp
			,STATUS			= fo.u_status
		FROM 
			fo (nolock)
			inner join b_us (nolock) on fo.ousrinis = iniciais
		WHERE
			fo.fostamp in (select distinct fn.fostamp from fn (nolock) where fn.fostamp=fo.fostamp and fn.ref=@ref)
			and fo.docnome = @nmdoc and fo.docdata between @dataIni and @dataFim
		order by
			fo.docdata, fo.ousrhora
	end 
	else
	if @tabela = 'BO'
	begin
		SELECT
			DOCDATA			= convert(varchar,bo.dataobra,102)
			,HORA			= bo.ousrhora
			,DOCUMENTO 		= bo.nmdos
			,NDOC			= convert(varchar,bo.obrano)
			,VALOR			= bo.etotal
			,QTTOTAL		= (select SUM(QTT) from bi (nolock) where bi.bostamp=bo.bostamp)
			,IDADE			= datediff(d,bo.dataobra,GETDATE())
			,OPERADOR		= b_us.username
			,SEL			= CONVERT(bit,0)
			,STAMP			= bo.bostamp
			,STATUS			= case when bo.fechada=1 then 'F' else 'A' end
		FROM
			BO (nolock)
			inner join b_us (nolock) on bo.ousrinis= iniciais
		WHERE
			bo.bostamp in (select distinct bi.bostamp from bi (nolock) where bi.bostamp=bo.bostamp and bi.ref=@ref)
			and bo.nmdos=@nmdoc and bo.dataobra between @dataini and @datafim
		order by
			bo.dataobra, bo.ousrhora
	end
	else
	if @tabela = 'FT'
	begin
		SELECT
			DOCDATA			= convert(varchar,ft.fdata,102)
			,HORA			= ft.ousrhora
			,DOCUMENTO 		= ft.nmdoc
			,NDOC			= convert(varchar,ft.fno)
			,VALOR			= ft.etotal
			,QTTOTAL		= (select SUM(QTT) from fi (nolock) where fi.ftstamp=ft.ftstamp)
			,IDADE			= datediff(d,ft.fdata,GETDATE())
			,OPERADOR		= b_us.username
			,SEL			= CONVERT(bit,0)
			,STAMP			= ft.ftstamp
			,STATUS			= 'N'
		FROM
			ft (nolock)
			inner join b_us (nolock) on ft.ousrinis = iniciais
		WHERE
			ft.ftstamp in (select distinct fi.ftstamp from fi(nolock) where ft.ftstamp=fi.ftstamp and fi.ref=@ref)
			and ft.nmdoc = @nmdoc and ft.fdata between @dataini and @datafim
		order by
			ft.fdata, ft.ousrhora
	end
end
else
begin
	if @tabela = 'FO'
	begin
		SELECT
			DOCDATA			= convert(varchar,fo.docdata,102)
			,HORA			= fo.ousrhora
			,DOCUMENTO	 	= fo.docnome
			,NDOC			= fo.adoc
			,VALOR			= fo.etotal
			,QTTOTAL		= (select SUM(QTT) from fn (nolock) where fn.fostamp=fo.fostamp)
			,IDADE			= datediff(d,fo.docdata,GETDATE())
			,OPERADOR		= b_us.username
			,SEL			= CONVERT(bit,0)
			,STAMP			= fo.fostamp
			,STATUS			= fo.u_status
		FROM 
			fo (nolock)
			inner join b_us (nolock) on fo.ousrinis = iniciais
		WHERE
			fo.docdata between @dataIni and @dataFim
			and fo.docnome = @nmdoc 
			and fo.no = @no and fo.estab = @estab
		order by
			fo.docdata, fo.ousrhora
	end 
	else
	if @tabela = 'BO'
	begin
		SELECT
			DOCDATA			= convert(varchar,bo.dataobra,102)
			,HORA			= bo.ousrhora
			,DOCUMENTO 		= bo.nmdos
			,NDOC			= convert(varchar,bo.obrano)
			,VALOR			= bo.etotal
			,QTTOTAL		= (select SUM(QTT) from bi (nolock) where bi.bostamp=bo.bostamp)
			,IDADE			= datediff(d,bo.dataobra,GETDATE())
			,OPERADOR		= b_us.username
			,SEL			= CONVERT(bit,0)
			,STAMP			= bo.bostamp
			,STATUS			= case when bo.fechada=1 then 'F' else 'A' end
		FROM
			BO (nolock)
			inner join b_us (nolock) on bo.ousrinis= iniciais
		WHERE
			bo.dataobra between @dataini and @datafim
			and bo.nmdos = @nmdoc
			and bo.no = @no and bo.estab = @estab
		order by
			bo.dataobra, bo.ousrhora
	end
	else
	if @tabela = 'FT'
	begin
		SELECT
			DOCDATA			= convert(varchar,ft.fdata,102)
			,HORA			= ft.ousrhora
			,DOCUMENTO 		= ft.nmdoc
			,NDOC			= convert(varchar,ft.fno)
			,VALOR			= ft.etotal
			,QTTOTAL		= (select SUM(QTT) from fi (nolock) where fi.ftstamp=ft.ftstamp)
			,IDADE			= datediff(d,ft.fdata,GETDATE())
			,OPERADOR		= b_us.username
			,SEL			= CONVERT(bit,0)
			,STAMP			= ft.ftstamp
			,STATUS			= 'N'
		FROM
			ft (nolock)
			inner join b_us (nolock) on ft.ousrinis = iniciais
		WHERE
			ft.fdata between @dataini and @datafim
			and ft.nmdoc = @nmdoc
			and ft.no = @no and ft.estab = @estab
		order by
			ft.fdata, ft.ousrhora
	end
end

GO
Grant Execute On dbo.up_gerais_listaDocumentosRef to Public
Grant Control On dbo.up_gerais_listaDocumentosRef to Public
GO