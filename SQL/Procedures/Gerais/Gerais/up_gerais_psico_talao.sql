/*

	exec up_gerais_psico_talao 'ADMFBE6602F-DD6D-4614-B75'
	
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_psico_talao]') IS NOT NULL
	drop procedure dbo.up_gerais_psico_talao
go

create procedure dbo.up_gerais_psico_talao
	@stamp varchar(30)


/* WITH ENCRYPTION */

AS
	begin 
		select 
			B_dadosPsico.* 
			, ft.fdata
			, ft.fno
			, ft.ndoc
			, ft2.u_receita
			, ft.vendedor
			, ft.vendnm
			, fi.design
			, fi.ref
			, fi.design
			, fi.qtt
			, fi.u_psicont

		from B_dadosPsico (nolock)
		inner join ft (nolock) on ft.ftstamp=B_dadosPsico.ftstamp
		inner join ft2 (nolock) on ft.ftstamp=ft2.ft2stamp
		inner join fi (nolock) on fi.ftstamp=ft.ftstamp
		where B_dadosPsico.ftstamp=@stamp
			and fi.u_psico=1
	end
GO
Grant Execute On dbo.up_gerais_psico_talao to Public
Grant Control On dbo.up_gerais_psico_talao to Public
GO