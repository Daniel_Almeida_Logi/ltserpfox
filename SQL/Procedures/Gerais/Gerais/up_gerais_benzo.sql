/*
	lista de documentos de uma referÍncia
	exec up_gerais_psico 1, 'Loja 1'
	0 Entrada ; 1 Saida
	exec up_gerais_benzo 1, 'Loja 1'
	exec up_gerais_benzo 0, 'Loja 1'
	exec up_gerais_benzo 1, 'Loja 2'
	exec up_gerais_benzo 0, 'Loja 2'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_benzo]') IS NOT NULL
	drop procedure dbo.up_gerais_benzo
go

create procedure dbo.up_gerais_benzo
	@tipo bit
	,@site varchar(20)


/* WITH ENCRYPTION */

AS
	IF @tipo = 1 /*Saida*/
	BEGIN
		Select 
			IsNull(Max(ctd),0) ct
		from (
			select 
				ctd = isnull(max(u_bencont),0) 
			from 
				fi (nolock)
				inner join ft (nolock) on ft.ftstamp = fi.ftstamp
				inner join td(nolock)   on td.ndoc = ft.ndoc
			Where
				ft.site = @site	and u_bencont!=0
				and td.cmsl !=0 -- movimenta stock
			
			union all
			
			Select 
				IsNull(Max(u_bencont),0) ctd
			from 
				bi (nolock)
				inner join bo (nolock) on bo.bostamp = bi.bostamp
				inner join ts (nolock) on bo.ndos = ts.ndos
			Where
				bo.site = @site and u_bencont!=0
				and ts.cmstocks!=0  -- movimenta stock
		)xx
	END
	IF @tipo = 0 /*Entrada*/
	BEGIN
	
		Select 
			ct = IsNull(Max(u_bencont),0)
		FROM 
			fn (nolock)
			inner join fo (nolock) on fn.fostamp = fo.fostamp
		Where
			fo.site = @site and u_bencont!=0
			
	END

GO
Grant Execute On dbo.up_gerais_benzo to Public
Grant Control On dbo.up_gerais_benzo to Public
GO