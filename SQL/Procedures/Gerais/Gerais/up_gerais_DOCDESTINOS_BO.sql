-- SP de Destinos no Documentos 
-- exec up_gerais_DOCDESTINOS_BO  'FRr6F48B4C5-F668-48F0-947'

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_DOCDESTINOS_BO]') IS NOT NULL
	drop procedure dbo.up_gerais_DOCDESTINOS_BO
go

create procedure dbo.up_gerais_DOCDESTINOS_BO
@Bistamp as varchar(55)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	;with cte1 as (
		Select 	
			Tipo = 'BO', 
			Doc = RTRIM(LTRIM(Bi.nmdos)) + ' Nr.' + RTRIM(LTRIM(STR(bi.obrano))),
			Informacao = 'Ref.: ' + RTRIM(LTRIM(Ref)) + ' | Total Valor: ' + RTRIM(LTRIM(STR(ettdeb,9,2))) + ' | Total Qtt: ' + RTRIM(LTRIM(STR(Qtt))) + ' | Data: ' + Left(Convert(varchar,dataobra,121),10) + ' | ' + Rtrim(Ltrim(bi.bostamp))
			,stamporidest = Bostamp
		From    	
			bi (nolock)
		Where  
			obistamp = @Bistamp /*Bi.Bistamp*/
	), cte2 as (
	
		Select	
			Tipo = 'FO', 
			Doc = RTRIM(LTRIM(FN.docnome)) + ' Nr.' + RTRIM(LTRIM(fn.adoc)),
			Informacao = 'Ref.: ' + RTRIM(LTRIM(Ref)) + ' | Total Valor: ' + RTRIM(LTRIM(STR(etiliquido,9,2))) + ' | Total Qtt: ' + RTRIM(LTRIM(STR(Qtt))) + ' | Data: ' + Left(Convert(varchar,data,121),10) + ' | ' + Rtrim(Ltrim(fn.fostamp))
			,stamporidest = Fn.fostamp 
		From	
			FN (nolock)
			--INNER JOIN Fo (nolock) On fn.fostamp = fo.fostamp
		Where	
			bistamp = @Bistamp /*Bi.Bistamp*/
	), cte3 as (
			
		Select  
			Tipo = 'FT', 
			Doc = RTRIM(LTRIM(Fi.nmdoc)) + ' Nr.' + RTRIM(LTRIM(STR(Fi.fno))),
			Informacao = 'Ref.: ' + RTRIM(LTRIM(Ref)) + ' | Total Valor: ' + RTRIM(LTRIM(STR(etiliquido,9,2))) + ' | Total Qtt: ' + RTRIM(LTRIM(STR(Qtt))) + ' | Data: ' + Left(Convert(varchar,rdata,121),10) + ' | ' + Rtrim(Ltrim(fi.ftstamp))
			,stamporidest = Fi.ftstamp
		from	
			fi (nolock)
			--Inner Join Ft (nolock) On ft.ftstamp = fi.ftstamp
		Where	
			bistamp = @Bistamp /*Bi.Bistamp*/
			or fi.fistamp = (select fistamp from bi2 where bi2stamp = @bistamp)
	)
	
	Select 
		RowNumber = ROW_NUMBER() OVER (ORDER BY stamporidest)
		, *
	From	
	(
		Select * From cte1 
		UNION ALL
		Select * From cte2
		UNION ALL
		Select * From cte3 
	) x

GO
Grant Execute On dbo.up_gerais_DOCDESTINOS_BO to Public
Grant Control On dbo.up_gerais_DOCDESTINOS_BO to Public
GO