-- lista de documentos
-- exec up_gerais_listaDocumentos '1', 'administrador'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_listaDocumentos]') IS NOT NULL
	drop procedure dbo.up_gerais_listaDocumentos
go

create procedure dbo.up_gerais_listaDocumentos

@user numeric(6),
@grupo varchar(20)

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON



	declare @site as varchar(60)
	set @site = isnull((select top 1 loja from b_us where userno = @user),'')

	SELECT DISTINCT
		NOMEDOC
		,resumo		= RTRIM(LTRIM(NOMEDOC)) +' - Visualizar'
		,tabela
	FROM (
		SELECT nomedoc = NMDOS, tabela = 'BO' FROM TS (nolock)
		UNION ALL
		SELECT nomedoc = CMDESC, tabela = 'FO' FROM CM1 (nolock) WHERE NAOFO = 0
		UNION ALL
		SELECT nomedoc = nmdoc, tabela = 'FT' FROM TD (nolock)
	) a
	where
		dbo.up_PerfilFacturacao(@user, @grupo, RTRIM(LTRIM(NOMEDOC)) + ' - Visualizar',@site) = 0
	ORDER BY
		NOMEDOC ASC

GO
Grant Execute On dbo.up_gerais_listaDocumentos to Public
Grant Control On dbo.up_gerais_listaDocumentos to Public
GO