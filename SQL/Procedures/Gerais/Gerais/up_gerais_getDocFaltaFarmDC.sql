

/*
Procedure :
		recebe um comando sql e executa em todas as farmacias com os diferentes ip�s 
		Para o preenchimento dp comando sql � necess�rio nos from colocar sempre 
		[XXXIP].mecofarma.dbo.tabela ou tabelas em quest�o e tem de ser neste formato
		exec up_gerais_getDocFaltaFarmDC 'select * from [xxxIP].mecofarma.dbo.empresa'

		declare @sql varchar(max) = 'select @outputFromExec = count(*) from [xxxIP].mecofarma.dbo.Rep_Control with (nolock) 
		where Table_Name=''FT'' AND Processed=0 and Operation=''I'''
		exec up_gerais_getDocFaltaFarmDC	@sql, ''
*/


 SET ANSI_NULLS ON
GO

if OBJECT_ID('[dbo].[up_gerais_getDocFaltaFarmDC]') IS NOT NULL
	DROP PROCEDURE dbo.up_gerais_getDocFaltaFarmDC
GO

CREATE PROCEDURE dbo.up_gerais_getDocFaltaFarmDC

	@sqlToRun		varchar(max) = '',
	@site		varchar(max) = ''

AS
SET NOCOUNT ON

If OBJECT_ID('tempdb.dbo.#resultFinal') IS NOT NULL
		DROP TABLE #resultFinal

Declare @resultPing				int
DECLARE @sqlClean				varchar(max) = @sqlToRun
DECLARE @resultWorked			nvarchar(max)
DECLARE @siteFailure			varchar(max) = ''

IF (@sqlToRun = '')
BEGIN
	print 'Missing Command for SQL'
END
ELSE
BEGIN

	Create Table #resultFinal(
		loja		varchar(254),
		docFalta	varchar(254),
		ip_next		varchar(254)
	)

	DECLARE @ip_next	varchar(100) 
	DECLARE @site_next	varchar(100)
	DECLARE database_cursor CURSOR FOR 
	SELECT
		 site,
		 ipaddress
	FROM
		AberturaEmpresa(nolock)
	where 
		(AberturaEmpresa.site in (Select items from dbo.up_splitToTable(@site,';')) or @site = '')

	OPEN database_cursor 

	FETCH NEXT FROM database_cursor INTO @site_next, @ip_next 

	WHILE @@FETCH_STATUS = 0 
	BEGIN  
		 set @sqlToRun = @sqlClean
		 set @resultWorked = ''

	BEGIN TRY

		set @resultWorked = REPLACE(@sqlToRun,'[XXXIP]','['+@ip_next+'\SQLEXPRESS]') 
		
		DECLARE @docFalta int 
		exec sp_executesql @resultWorked, N'@outputFromExec varchar out', @docFalta out

		INSERT INTO #resultFinal
		Values(@site_next,CONCAT('Faltam integrar: ',@docFalta),@ip_next)

	END TRY
	BEGIN CATCH

			INSERT INTO #resultFinal
			Values(@site_next,'N�o foi possivel obter resposta do site',@ip_next)

			set @siteFailure = @siteFailure + ';'+@site_next 
			
	END CATCH

		 FETCH NEXT FROM database_cursor INTO @site_next, @ip_next 
	END 

	CLOSE database_cursor 
	DEALLOCATE database_cursor 

	set @siteFailure =  ISNULL(STUFF( @siteFailure, 1, 1, ''),'')

	SELECT top 100
	   ' <u>Loja:</u> ' + isnull(loja,AberturaEmpresa.site) as Loja ,
	    '<u>Docomentos Falta:</u>' +  isnull(docFalta,'N�o foi possivel obter resposta do site')  as docFalta
	FROM #resultFinal
		right join AberturaEmpresa(nolock) on AberturaEmpresa.site = #resultFinal.loja
		where 
		(AberturaEmpresa.site in (Select items from dbo.up_splitToTable(@site,';')) or @site = '')

END

If OBJECT_ID('tempdb.dbo.#resultFinal') IS NOT NULL
		DROP TABLE #resultFinal

GRANT EXECUTE on dbo.up_gerais_getDocFaltaFarmDC TO PUBLIC
GRANT Control on dbo.up_gerais_getDocFaltaFarmDC TO PUBLIC
GO
