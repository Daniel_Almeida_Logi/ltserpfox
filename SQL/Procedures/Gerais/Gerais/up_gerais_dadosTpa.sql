/*
	retorna informação do tpa para o terminal actual
	exec [dbo].[up_gerais_dadosTpa] 24, 'Loja 1'

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_dadosTpa]') IS NOT NULL
	drop procedure dbo.up_gerais_dadosTpa
go

create procedure dbo.up_gerais_dadosTpa
@tno integer 
,@site varchar(8) 

/* WITH ENCRYPTION */
AS

	select 
	 tpa.termstamp
	 ,bt.machine
	 ,bt.terminal
	 ,tpa.ip
	 ,tpa.port
	 ,tpa.version
	 ,tpa.model
	 ,tpa.specification_version

	from 
		tpa_map(nolock) tpa
	inner join 
		B_Terminal(nolock) bt ON bt.tstamp = tpa.termstamp
	WHERE
		bt.no=@tno 
		and site=@site



GO
Grant Execute On dbo.up_gerais_dadosTpa to Public
Grant Control On dbo.up_gerais_dadosTpa to Public
Go











