/*

	exec up_gerais_getImpressoes 'V/Factura', '',''

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_gerais_getImpressoes]') IS NOT NULL
    DROP procedure dbo.up_gerais_getImpressoes
GO

CREATE procedure dbo.up_gerais_getImpressoes
	@documento VARCHAR(50),
	@tipoCliente VARCHAR(50),
	@nomeImpressao VARCHAR(50) = ''

AS

	Select 
		nomeimpressao,
		descricao, 
		documento,
		temduplicados, 
		numduplicados, 
		ID, 
		idupordefeito, 
		tipocliente,
		lineCursor,
		lineFilter
	From 
		b_impressoes (nolock) 
	Where  
		nomeimpressao!= '' 
		AND documento = @documento
		AND tipocliente in (@tipoCliente,'')
		AND  nomeimpressao = CASE WHEN @nomeImpressao = '' THEN nomeimpressao ELSE @nomeImpressao END
	ORDER BY 
		idupordefeito DESC


GO
GRANT EXECUTE on dbo.up_gerais_getImpressoes TO PUBLIC
GRANT Control on dbo.up_gerais_getImpressoes TO PUBLIC
GO