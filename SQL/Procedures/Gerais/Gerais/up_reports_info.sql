/*
reports que foram chamados 
quanto demorou 
quem chamou 
parametros
byteCount 
RowCount

exec up_reports_info '20221128','20221228',2000,''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_reports_info]') IS NOT NULL
	drop procedure dbo.up_reports_info
go

Create procedure [dbo].[up_reports_info]
	@dataIni				DATETIME
	,@dataFim				DATETIME
	,@minDurationTime		INT
	,@site					VARCHAR(254)

AS
	SELECT
		 [Parameters]															AS Parametros
		 ,UserName																AS Utilizador
		 ,CONVERT(time,TimeEnd - TimeStart)										AS TempoEspera
		 ,ByteCount																AS Tamanho
		 ,[RowCount]															AS NrLinhas
		 ,[Status]																AS Estado
	FROM 
		ExecutionLog2 (NOLOCK)
	WHERE 
		CONVERT(DATE,TimeStart) between @dataIni and @dataFim AND
		TimeDataRetrieval >= @minDurationTime and 
		UserName like (case when @site='' then '%%' else '%'+@site+'%' end)


GO
Grant Execute On dbo.up_reports_info to Public
Grant control On dbo.up_reports_info to Public
GO