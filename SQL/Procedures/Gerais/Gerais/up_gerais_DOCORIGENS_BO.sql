-- SP de Origens no Documento de Facturação
-- exec up_gerais_DOCORIGENS_BO 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_DOCORIGENS_BO]') IS NOT NULL
	drop procedure dbo.up_gerais_DOCORIGENS_BO
go

create procedure dbo.up_gerais_DOCORIGENS_BO
@oBistamp as varchar(55),
@cabstamp as varchar(25),
@fnstamp as varchar(25)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	;with cte1 as (
		Select 	
			Tipo = 'BO'
			,Doc = RTRIM(LTRIM(Bi.nmdos)) + ' Nr.' + RTRIM(LTRIM(STR(bi.obrano))),
			Informacao = 'Ref.: ' + RTRIM(LTRIM(Ref)) + ' | Total Valor: ' + RTRIM(LTRIM(STR(ettdeb,9,2))) + ' | Total Qtt: ' + RTRIM(LTRIM(STR(Qtt))) + ' | Data: ' + Left(Convert(varchar,dataobra,121),10) + ' | ' + Rtrim(Ltrim(bi.bostamp))
			,stamporidest= Bostamp
		From    
			bi (nolock)
		Where   
			bistamp = @oBistamp /*Bi.oBistamp*/
	), cte2 as (
		Select	
			Tipo = 'FO', 
			Doc = RTRIM(LTRIM(FN.docnome)) + ' Nr.' + RTRIM(LTRIM(fn.adoc)),
			Informacao = 'Ref.: ' + RTRIM(LTRIM(Ref)) + ' | Total Valor: ' + RTRIM(LTRIM(STR(etiliquido,9,2))) + ' | Total Qtt: ' + RTRIM(LTRIM(STR(Qtt))) + ' | Data: ' + Left(Convert(varchar,docdata,121),10) + ' | ' + Rtrim(Ltrim(fn.fostamp))
			,stamporidest = Fo.fostamp
		From	
			FN (nolock)
			INNER JOIN Fo (nolock) On fn.fostamp = fo.fostamp
		Where	
			fnstamp =  @oBistamp /*Bi.oBistamp*/
	
	), cte3 as (
			Select  
				Tipo = 'FO',
				Doc = RTRIM(LTRIM(FN.docnome)) + ' Nr.' + RTRIM(LTRIM(fn.adoc)),
				Informacao = 'Ref.: ' + RTRIM(LTRIM(Ref)) + ' | Total Valor: ' + RTRIM(LTRIM(STR(sum(etiliquido),9,2))) + ' | Total Qtt: ' + RTRIM(LTRIM(STR(sum(Qtt)))) + ' | Data: ' + Left(Convert(varchar,docdata,121),10) + ' | ' + Rtrim(Ltrim(fn.fostamp))
				,stamporidest = Fo.fostamp
			From   
				FN (nolock)
				INNER JOIN Fo (nolock) On fn.fostamp = fo.fostamp
			Where    
				fn.fostamp = (Select bo2.u_fostamp from bo2 where bo2stamp = @cabstamp) /*cabDoc.cabstamp*/
			group by 
				fn.docnome, fn.adoc, fn.ref, fo.docdata, fo.fostamp, fn.fostamp 
			
	), cte4 as (
		Select	
			Tipo = 'FO',
			Doc = RTRIM(LTRIM(FN.docnome)) + ' Nr.' + RTRIM(LTRIM(fn.adoc)),
			Informacao = 'Ref.: ' + RTRIM(LTRIM(Ref)) + ' | Total Valor: ' + RTRIM(LTRIM(STR(etiliquido,9,2))) + ' | Total Qtt: ' + RTRIM(LTRIM(STR(Qtt))) + ' | Data: ' + Left(Convert(varchar,docdata,121),10) + ' | ' + Rtrim(Ltrim(fn.fostamp))
			,stamporidest = Fo.fostamp
		From	
			FN (nolock)
			INNER JOIN Fo (nolock) On fn.fostamp = fo.fostamp
		Where	
			fnstamp = @fnstamp /*Bi2.fnstamp*/
	)
	
	Select 
		RowNumber = ROW_NUMBER() OVER (ORDER BY stamporidest)
		, *
	From	
	(
		Select * From cte1 
		UNION ALL
		Select * From cte2
		UNION ALL
		Select * From cte3 
		UNION ALL
		Select * From cte4
	) x


GO
Grant Execute On dbo.up_gerais_DOCORIGENS_BO to Public
Grant Control On dbo.up_gerais_DOCORIGENS_BO to Public
GO