/* SP que carrega Dados da Empresa no cursor uCrsE1
	
alterada a 20200824, Jg para contemplar multiloja

RESUMO: 
	* se o input site for apenas uma loja traz um s� registo com essa loja (site) e sera esse registo a ser colocado no report.
	* se o input for mais que uma loja, separado por virgula, traz todos os registos da tabela empresa. 

A ideia de trazer todos � para poder ser comparado no Reporting Services com o site default, mysite.

Esta tabela preenche o cabe�alho dos reports e deve indicar em cima o site default se seleccionarem mais que uma loja.
Isto �, se escolherem 'Loja 2,Loja 3' o cabe�alho em cima deve ser o da loja 1 (pensando que esta � a default).
O valor da loja default � definido pelo form no Logitools atrav�s da passagem da variavel mysite.


EXECUCAO:

	exec up_gerais_dadosEmpresa 'BRISAS'
	exec up_gerais_dadosEmpresa 'Loja 2'
	exec up_gerais_dadosEmpresa 'Loja 1'
	exec up_gerais_dadosEmpresa 'Loja 2,Loja 3'



*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_dadosEmpresa]') IS NOT NULL
	drop procedure dbo.up_gerais_dadosEmpresa
go

create procedure dbo.up_gerais_dadosEmpresa

@site varchar(254)

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

--n�o tem virgula, ou seja � apenas um site
IF CHARINDEX(',', @site) = 0 
BEGIN
	select
		nomecomp
		,tipoempresa
		,u_dirtec = dirtec
		,empresa.morada
		,codpost
		,telefone
		,fax
		,ctacttel
		,ncont
		,contacto
		,local
		,pais
		,u_infarmed = infarmed
		,u_assfarm = assfarm
		,u_codfarm = codfarm
		,ecapsocial = capsocial
		,email
		,url
		,moradaLoja	= empresa.morada
		,ncontLoja	= empresa.ncont
		,siteLoja	= empresa.site
		,siteno		= empresa.no
		,nrCert		= ISNULL((select textvalue from B_Parameters (NOLOCK) where stamp='ADM0000000078'),'')
		,sede		= ISNULL((select top 1 textvalue from B_Parameters (nolock) where stamp='ADM0000000137'),'')
		,ccusto		= empresa.ccusto
		,empresa.posto
		,empresa.siteposto
		,empresa.nib
		,localPresc
		,localcod
		,localnm
		,prescricao
		,imagem
		,imagemiso9001 = 'C:\Logitools\Clientes\MECOFARMA\Logos\ISO9001PB.jpg'
		,estab
		,motivo_isencao_iva
		,codmotiseimp
		,empresa.capsocial
		,empresa.consreg
		,empresa.id_lt
		,armazem1 = (select top 1 armazem from empresa_arm Where	empresa_arm.empresa_no = empresa.no	order by armazem)
		,multiempresa = case when (SELECT COUNT(nomecomp) FROM empresa) > 1 then 'Sim' Else 'Nao' END
		,cae
		,plafond_cliente --  valor plafonde do cliente
		,controla_plafond -- se controla plafond do cliente
		,pagcentral --  se usa pagamentos centralizados
		,descpredef -- se usa descontos predefinidos
		,ordemimpressaoreceita  -- ordem de impress�o de receita SNS // Entidade Complementar
		,pme_lider -- indica se empresa � pme Lider (imprime simbolo nos documentos de faturacao)
		,gestao_cx_operador -- 1 gest�o de caixa por operador
		,cod_swift -- cod_swift banco
		,utilizadorDT -- credenciais DT
		,passwordDT -- credenciais DT
		,centroArb  -- informa��o dos centros de arbitragem para resolu��o de litigios
		,obrigaConf -- obrigada confer�ncia de produtos em vendas com DEM no atendimento
		,reservasAdi -- % de adiantamento predefinido nas reservas
		,impRecOrdenada -- impress�o ordenada das receitas no atendimento
		,empresa.has_ticket_integration
		,empresa.senhaMotivoSemAtendimento -- 	se pergunta o motivo para o qual nao existiu atendimento ao chamar a proxima senha
		,empresa.usaTpa -- valida se a empresa tem o tpa ativo
		,empresa.usaCashDro -- valida se a empresa tem o cashdro ativo
		,mailsign
		,usacoms 
		,nomabrv
		,abrev
		,codsinave
		,logoetiqueta
		,logorodape
		,nib2
		,cod_swift2
		,banco2
	from
		empresa (nolock)
	where
		empresa.site=@site		
END
ELSE
BEGIN
	select
		nomecomp
		,tipoempresa
		,u_dirtec = dirtec
		,empresa.morada
		,codpost
		,telefone
		,fax
		,ctacttel
		,ncont
		,contacto
		,local
		,pais
		,u_infarmed = infarmed
		,u_assfarm = assfarm
		,u_codfarm = codfarm
		,ecapsocial = capsocial
		,email
		,url
		,moradaLoja	= empresa.morada
		,ncontLoja	= empresa.ncont
		,siteLoja	= empresa.site
		,siteno		= empresa.no
		,nrCert		= ISNULL((select textvalue from B_Parameters (NOLOCK) where stamp='ADM0000000078'),'')
		,sede		= ISNULL((select top 1 textvalue from B_Parameters (nolock) where stamp='ADM0000000137'),'')
		,ccusto		= empresa.ccusto
		,empresa.posto
		,empresa.siteposto
		,empresa.nib
		,localPresc
		,localcod
		,localnm
		,prescricao
		,imagem
		,imagemiso9001 = 'C:\Logitools\Clientes\MECOFARMA\Logos\ISO9001PB.jpg'
		,estab
		,motivo_isencao_iva
		,empresa.capsocial
		,empresa.consreg
		,empresa.id_lt
		,armazem1 = (select top 1 armazem from empresa_arm Where	empresa_arm.empresa_no = empresa.no	order by armazem)
		,multiempresa = case when (SELECT COUNT(nomecomp) FROM empresa) > 1 then 'Sim' Else 'Nao' END
		,cae
		,plafond_cliente --  valor plafonde do cliente
		,controla_plafond -- se controla plafond do cliente
		,pagcentral --  se usa pagamentos centralizados
		,descpredef -- se usa descontos predefinidos
		,ordemimpressaoreceita  -- ordem de impress�o de receita SNS // Entidade Complementar
		,pme_lider -- indica se empresa � pme Lider (imprime simbolo nos documentos de faturacao)
		,gestao_cx_operador -- 1 gest�o de caixa por operador
		,cod_swift -- cod_swift banco
		,utilizadorDT -- credenciais DT
		,passwordDT -- credenciais DT
		,centroArb  -- informa��o dos centros de arbitragem para resolu��o de litigios
		,obrigaConf -- obrigada confer�ncia de produtos em vendas com DEM no atendimento
		,reservasAdi -- % de adiantamento predefinido nas reservas
		,impRecOrdenada -- impress�o ordenada das receitas no atendimento
		,empresa.has_ticket_integration
		,empresa.senhaMotivoSemAtendimento -- 	se pergunta o motivo para o qual nao existiu atendimento ao chamar a proxima senha
		,empresa.usaTpa -- valida se a empresa tem o tpa ativo
		,empresa.usaCashDro -- valida se a empresa tem o cashdro ativo
		,mailsign
		,usacoms 
		,nomabrv
		,abrev
		,nib2
		,cod_swift2
		,banco2
	from
		empresa (nolock)
	--where empresa.site in (Select items from dbo.up_splitToTable(@site, ',')) 
END



GO
Grant Execute on dbo.up_gerais_dadosEmpresa to Public
Grant Control on dbo.up_gerais_dadosEmpresa to Public
GO