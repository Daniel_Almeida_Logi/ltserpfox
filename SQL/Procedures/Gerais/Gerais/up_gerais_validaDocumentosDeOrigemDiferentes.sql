/*
	Author: Daniel Almeida
	data: 2022-11-16

	Valida se ofistamp � o mesmo em atendimentos em clientes sem multivenda

	exec up_gerais_validaDocumentosDeOrigemDiferentes  'ADM3FF179D1-2E71-4BC5-A5E;ADM9BE4BE94-9856-4905-AF3'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_validaDocumentosDeOrigemDiferentes]') IS NOT NULL
	drop procedure dbo.up_gerais_validaDocumentosDeOrigemDiferentes 
go

create procedure dbo.up_gerais_validaDocumentosDeOrigemDiferentes
	@ofistamp varchar(max)


/* WITH ENCRYPTION */
AS
SET NOCOUNT ON
	declare @conta  int = 0
	if(len(@ofistamp)>0)
	begin

		select 
			@conta = count(distinct ftstamp) 
		from 
			fi(nolock)
		where fi.fistamp in (select items from dbo.up_splitToTable(@ofistamp,';'))
	end else begin
		select @conta = 0
	end

	if(@conta>1)
		select CONVERT(bit,1) as resultado
	else
		select CONVERT(bit,0) as resultado

GO
Grant Execute On dbo.up_gerais_validaDocumentosDeOrigemDiferentes  to Public
Grant Control On dbo.up_gerais_validaDocumentosDeOrigemDiferentes  to Public
GO

