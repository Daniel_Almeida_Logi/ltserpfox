-- SP de Destinos no Documentos 
-- exec up_gerais_DOCDESTINOS_FO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_DOCDESTINOS_FO]') IS NOT NULL
	drop procedure dbo.up_gerais_DOCDESTINOS_FO
go

create procedure dbo.up_gerais_DOCDESTINOS_FO
@fnstamp as varchar(55),
@cabstamp as varchar(25)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	;with cte1 as (
		Select	
			Tipo = 'FO',
			Doc = RTRIM(LTRIM(FN.docnome)) + ' Nr.' + RTRIM(LTRIM(fn.adoc)),
			Informacao = 'Ref.: ' + RTRIM(LTRIM(Ref)) + ' | Total Valor: ' + RTRIM(LTRIM(STR(etiliquido,9,2))) + ' | Total Qtt: ' + RTRIM(LTRIM(STR(Qtt))) + ' | Data: ' + Left(Convert(varchar,data,121),10) + ' | ' + Rtrim(Ltrim(fn.fostamp))
			,stamporidest = Fn.fostamp
		From	
			FN (nolock)
			--INNER JOIN Fo On fn.fostamp = fo.fostamp
		Where	
			ofnstamp = @fnstamp /*Fn.Fnstamp*/
	), cte2 as (
		Select 	Tipo = 'BO',
				Doc = RTRIM(LTRIM(Bi.nmdos)) + ' Nr.' + RTRIM(LTRIM(STR(bi.obrano))),
				Informacao = 'Ref.: ' + RTRIM(LTRIM(Ref)) + ' | Total Valor: ' + RTRIM(LTRIM(STR(ettdeb,9,2))) + ' | Total Qtt: ' + RTRIM(LTRIM(STR(Qtt))) + ' | Data: ' + Left(Convert(varchar,dataobra,121),10) + ' | ' + Rtrim(Ltrim(Bi.bostamp))
				,stamporidest = bi.Bostamp
		From    	
			bi (nolock)
			inner join bi2 (nolock) on bi.bistamp = bi2.bi2stamp
		Where  
			bi2.fnstamp = @fnstamp /*Fn.Fnstamp*/	
	), cte3 as (	
		Select 	
			Tipo = 'BO', 
			Doc = RTRIM(LTRIM(Bi.nmdos)) + ' Nr.' + RTRIM(LTRIM(STR(bi.obrano))),
			Informacao = 'Ref.: ' + RTRIM(LTRIM(Ref)) + ' | Total Valor: ' + RTRIM(LTRIM(STR(ettdeb,9,2))) + ' | Total Qtt: ' + RTRIM(LTRIM(STR(Qtt))) + ' | Data: ' + Left(Convert(varchar,dataobra,121),10) + ' | ' + Rtrim(Ltrim(Bi.bostamp))
			,stamporidest = bi.Bostamp
		From    	
			bi (nolock)
			--inner join bi2 (nolock) on bi.bistamp = bi2.bi2stamp
			inner join bo2 (nolock) on bo2.bo2stamp = bi.bostamp
		Where  
			bo2.u_fostamp = @cabstamp /*cabDoc.cabstamp*/
	)
	
	Select 
		RowNumber = ROW_NUMBER() OVER (ORDER BY stamporidest)
		, *
	From	
	(
		Select * From cte1 
		UNION ALL
		Select * From cte2
		UNION ALL
		Select * From cte3 
	) x
	


GO
Grant Execute On dbo.up_gerais_DOCDESTINOS_FO to Public
Grant Control On dbo.up_gerais_DOCDESTINOS_FO to Public
GO