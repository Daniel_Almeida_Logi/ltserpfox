-- lista de documentos de uma referÍncia
-- exec up_gerais_gerarIdentifiers 36
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_gerarIdentifiers]') IS NOT NULL
	drop procedure dbo.up_gerais_gerarIdentifiers
go

create procedure dbo.up_gerais_gerarIdentifiers
@tamanho as int

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	Select left(NEWID(),@tamanho) as identifier

GO
Grant Execute On dbo.up_gerais_gerarIdentifiers to Public
Grant Control On dbo.up_gerais_gerarIdentifiers to Public
GO