/*

	exec up_gerais_getDataMatrix '21T1FR65KPAGRKKKF#7145731476#10MRM20221-50A#010560095171456517251130', '#'

	use ltdic
	exec up_gerais_getDataMatrix '01036603985029057142923589#10CH317P#17231031215F7YM8GTKNBE15', '#'

	
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_getDataMatrix]') IS NOT NULL
	drop procedure dbo.up_gerais_getDataMatrix
go

create procedure dbo.up_gerais_getDataMatrix
	@string varchar(200) = '',
	@gs varchar(3) = '#'


/* WITH ENCRYPTION */

AS
	begin 
		set @string = RTRIM(LTRIM(isnull(@string,'')))


		If OBJECT_ID('tempdb.dbo.#tmpDM') IS NOT NULL
			drop table #tmpDM;

		If OBJECT_ID('tempdb.dbo.#tmpFinalResultado') IS NOT NULL
			drop table #tmpFinalResultado;

		CREATE TABLE #tmpDM (
			usado       bit,
			tamanho     int,
			delimitador varchar(20),
			id          int,
			codigo      varchar(20) ,
			countryCode bit
		)



		CREATE TABLE #tmpFinalResultado (
			codEanProd    varchar(20),
			loteId        varchar(20),
			loteData      varchar(6),
			idSerial      varchar(20),
			cnp           varchar(20),
			valido        bit,
			countryCode	  varchar(10)
		)

		/*
		insert into #tmpDM
		values
		(0,14,'01',1,''), -- codEanProd
		(0,20,'21',2,''), -- serial number
		(0,20,'10',3,''), -- loteId
		(0,6,'17',4,''),  -- loteData
		(0,20,'714',5,''),--cnp
		(0,6,'11',6,'') -- prodData para ignorar, corre
		*/

		INSERT INTO #tmpDM
		SELECT * FROM regrasDM(nolock) order by id desc

		declare @conta int = 0
		declare @size int = 0
		
		select @size = COUNT(*) from #tmpDM

		-- correr o ciclo de size passos
		WHILE @conta < @size  
		begin	
			
			declare @initPos     int = 0
			declare @finalPos    int = 0
			declare @currentPos  int = 0

	
			-- guardar cada um dos elementos do datamatrix numa tabela temporaria
			-- corta a substring se encontrar o tamanho maximo de cada elemento ou elemento GS
			-- validar a quest�o de nao encontrar o GS, sen�o compara qualquer valor com 0, e o min vai ser sempre 0
			update
				#tmpDM
			set
				codigo = SUBSTRING(@string, LEN(#tmpDM.delimitador)+1, (SELECT MIN(x) FROM (VALUES (case when CHARINDEX(@gs,@string) > 0 then CHARINDEX(@gs,@string) else 999999 end ),(#tmpDM.tamanho+(LEN(#tmpDM.delimitador)+1))) AS value(x))-(LEN(#tmpDM.delimitador)+1)),
				usado = 1,
				@initPos = LEN(#tmpDM.delimitador)+1,
				@finalPos =(SELECT MIN(x) FROM (VALUES (case when CHARINDEX(@gs,@string)>0 then CHARINDEX(@gs,@string) else 999999 end  ),(#tmpDM.tamanho+(LEN(#tmpDM.delimitador)+1))) AS value(x))
			from #tmpDM
			where 
				#tmpDM.delimitador like  left(@string,LEN(#tmpDM.delimitador))
				and usado=0
		

		
			set @currentPos = @initPos + @finalPos

			--vai eliminado as partes da string j� usadas
			set @string = SUBSTRING(@string,@finalPos,LEN(@string)) 

			if(CHARINDEX(@gs,@string))=1 --remove gs se existir
			begin	
				set @string = SUBSTRING(@string,2,LEN(@string)) 
			end


			set @conta=@conta+1
		end		

		-- guarda o resultado numa tabela para devolver a resposta	
		insert into	#tmpFinalResultado(codEanProd,idSerial,loteId,loteData,cnp, valido, countryCode)
		values 
		(
			(select top 1 rtrim(ltrim(isnull(codigo,''))) from  #tmpDM where id=1  and usado=1),
			(select top 1 rtrim(ltrim(isnull(codigo,''))) from  #tmpDM where id=2  and usado=1),
			(select top 1 rtrim(ltrim(isnull(codigo,''))) from  #tmpDM where id=3  and usado=1),
			(select top 1 rtrim(ltrim(isnull(codigo,''))) from  #tmpDM where id=4  and usado=1),
			(select top 1 rtrim(ltrim(isnull(codigo,''))) from  #tmpDM where id in (5,7) and usado=1),
			0,
			(select TOP 1 delimitador from #tmpDM WHERE countrycode = 1 and usado = 1)

		)

		--Valida se os dados s�o consistentes.
		-- codEanProd tem de ter 14 digitos
		-- idSerial tem de estar preechido e ter 20 ou menos caracteres
		-- loteId tem de estar preechido e ter 20 ou menos caracteres
		-- loteData tem de estar preechido de ter 4 ou 6 caracteres, se tiver 4 tem de se acrescentar o ultimo dia do mes
		-- cnp n�o � obrigatorio

		update 
			#tmpFinalResultado
		set 
			valido = 1
		where
			LEN(codEanProd)=14 and
			LEN(idSerial)>0    and LEN(idSerial)<=20    and
			LEN(loteId)>0      and LEN(loteId)<=20      and
			LEN(loteData)>=4   and LEN(loteData)<=6    




		select 
			codEanProd as pc,
			loteData   as data,
			loteId     as lote,
			idSerial   as sn,
			cnp        as ref,
			valido     as valido,
			'GTIN'     as [schema],
			countrycode
		from #tmpFinalResultado

		If OBJECT_ID('tempdb.dbo.#tmpDM') IS NOT NULL
			drop table #tmpDM;

		If OBJECT_ID('tempdb.dbo.#tmpFinalResultado') IS NOT NULL
			drop table #tmpFinalResultado;


		
	end
GO
Grant Execute On dbo.up_gerais_getDataMatrix to Public
Grant Control On dbo.up_gerais_getDataMatrix to Public
GO