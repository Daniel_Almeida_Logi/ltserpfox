/* Dados das lojas
	
	exec up_gerais_armazensDisponiveis 'Loja 2', '1000646'
	exec up_gerais_armazensDisponiveis 'Loja 1', '9999992'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_gerais_armazensDisponiveis]') IS NOT NULL
	drop procedure up_gerais_armazensDisponiveis
go

create PROCEDURE up_gerais_armazensDisponiveis
	@site varchar(60)
	,@ref varchar(18)
	
/* WITH ENCRYPTION */

AS
SET NOCOUNT ON
	
	declare @pais as varchar(50)
	select @pais=LTRIM(RTRIM(textValue)) from B_Parameters_SITE (NOLOCK) where stamp='ADM0000000050' AND site = @site

	IF (@pais = 'Portugal')
	BEGIN
		declare @ncont as varchar(20)
		set @ncont = ISNULL((select ncont from empresa (NOLOCK) where site = @site),0)	
		select
			empresa_arm.armazem 
		from
			empresa_arm (nolock)
			inner join empresa (nolock) on empresa.no = empresa_arm.empresa_no
			inner join st (nolock) on st.site_nr = empresa.no 
		Where
			empresa.ncont = @ncont
			and (st.ref = @ref or @ref = '')
		group by 
			empresa_arm.armazem
	END
	ELSE
	BEGIN
		select
			empresa_arm.armazem
		from
			empresa_arm (nolock)
	END  
	
GO
Grant Execute On up_gerais_armazensDisponiveis to Public
Grant Control On up_gerais_armazensDisponiveis to Public
GO