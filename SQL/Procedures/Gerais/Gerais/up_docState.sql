/* 
	
	up_docState 'ADM05C2A486-B9F0-4933-ABB'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_docState]') IS NOT NULL
	DROP PROCEDURE dbo.up_docState
GO

CREATE PROCEDURE dbo.up_docState
	 @ftstamp		varchar(50)


AS

SET NOCOUNT ON


	SELECT 
		aceite, faturacao_eletronica_med_d.* 
	FROM 
		faturacao_eletronica_med(nolock)
	INNER JOIN faturacao_eletronica_med_d(nolock) on faturacao_eletronica_med_d.id_faturacao_eletronica_med = faturacao_eletronica_med.id
	WHERE 
		ftstamp=@ftstamp
	ORDER BY data desc
	
GO
GRANT EXECUTE on dbo.up_docState TO PUBLIC
GRANT Control on dbo.up_docState TO PUBLIC
GO


