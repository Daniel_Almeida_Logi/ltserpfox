/* Fazer rebuild a uma base de dados
	exec up_gerais_rebuildIndexsTable 'ltdev30'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_gerais_rebuildIndexsTable]') IS NOT NULL
	drop procedure up_gerais_rebuildIndexsTable
go

create PROCEDURE up_gerais_rebuildIndexsTable
	 @databaseName varchar(100) = ''
	
/* WITH ENCRYPTION */

AS
		DECLARE @Database NVARCHAR(255)   
		DECLARE @Table NVARCHAR(255)  
		DECLARE @cmd NVARCHAR(1000)  
		DECLARE @error int =0 
		DECLARE DatabaseCursor CURSOR READ_ONLY FOR  
		SELECT name FROM master.sys.databases   
		WHERE name IN (@databaseName)  -- databases
		AND state = 0 -- database is online
		AND is_in_standby = 0 -- database is not read only for log shipping
		ORDER BY 1  
		OPEN DatabaseCursor  
		FETCH NEXT FROM DatabaseCursor INTO @Database  
		WHILE @@FETCH_STATUS = 0  
		BEGIN  
		   SET @cmd = 'DECLARE TableCursor CURSOR READ_ONLY FOR SELECT ''['' + table_catalog + ''].['' + table_schema + ''].['' +  
		   table_name + '']'' as tableName FROM [' + @Database + '].INFORMATION_SCHEMA.TABLES WHERE table_type = ''BASE TABLE'''   
		   -- create table cursor  
		   EXEC (@cmd)  
		   OPEN TableCursor   
		   FETCH NEXT FROM TableCursor INTO @Table   
		   WHILE @@FETCH_STATUS = 0   
		   BEGIN
			  BEGIN TRY   
				 SET @cmd = 'ALTER INDEX ALL ON ' + @Table + ' REBUILD' 
				 PRINT @cmd -- uncomment if you want to see commands
				 EXEC (@cmd) 
			  END TRY
			  BEGIN CATCH
				 PRINT '---'
				 PRINT @cmd
				 PRINT ERROR_MESSAGE() 


				 IF  EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'dbInfo')
				 BEGIN
					 Insert into [DocsLogitools].dbo.dbInfo (stamp,dbName,serviceId,serviceIdDesc,statusID,statusIDDesc,ousrdata)
					 Values(LEFT(NEWID(),36),@databaseName,1,'DBCLEAN',1 ,'Error ' + @cmd + ' ' + ERROR_MESSAGE(),GETDATE())
				 END
				 set @error=1
				 PRINT '---'
			  END CATCH
			  FETCH NEXT FROM TableCursor INTO @Table   
		   END   
		   CLOSE TableCursor   
		   DEALLOCATE TableCursor  
		   FETCH NEXT FROM DatabaseCursor INTO @Database  
		END  
		CLOSE DatabaseCursor   
		DEALLOCATE DatabaseCursor

		if (@error =0) and  EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'dbInfo')
		begin 
			Insert into [DocsLogitools].dbo.dbInfo (stamp,dbName,serviceId,serviceIdDesc,statusID,statusIDDesc,ousrdata)
			Values(LEFT(NEWID(),36),@databaseName,1,'DBCLEAN',0 ,'Sucess rebuildIndexsTable ',GETDATE())
		end 

GO
Grant Execute On up_gerais_rebuildIndexsTable to Public
Grant Control On up_gerais_rebuildIndexsTable to Public
GO








