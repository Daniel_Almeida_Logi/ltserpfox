-- Procurar Valor N�merico nos Par�metros
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_parameters_ProcuraNum]') IS NOT NULL
	drop procedure dbo.up_parameters_ProcuraNum
go

create procedure dbo.up_parameters_ProcuraNum

@stamp varchar(30)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select numValue from b_parameters where stamp=@stamp


GO
Grant Execute on dbo.up_parameters_ProcuraNum to Public
Grant Control on dbo.up_parameters_ProcuraNum to Public
Go