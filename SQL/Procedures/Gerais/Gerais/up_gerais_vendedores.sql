-- Lista vendedores
-- exec up_gerais_vendedores
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_vendedores]') IS NOT NULL
	drop procedure dbo.up_gerais_vendedores
go

create procedure dbo.up_gerais_vendedores

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	
	SELECT
		cmdesc,
		cm
	FROM(
			select
				 cmdesc = 'Todos'
				,cm = 0
				,ordem=1
			UNION ALL
			select
				 cmdesc = nome
				,cm = userno
				,ordem =2
			from 
				b_us (nolock) 
			where 
				b_us.grupo not in ('Administrador','Contabilidade')
		) as x
	order by 
		ordem asc,
		cmdesc asc

GO
Grant Execute On dbo.up_gerais_vendedores to Public
Grant Control On dbo.up_gerais_vendedores to Public
GO