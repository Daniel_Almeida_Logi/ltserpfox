/* 
	

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_getFileXML]') IS NOT NULL
	drop procedure dbo.up_gerais_getFileXML
go

create procedure dbo.up_gerais_getFileXML
@id VARCHAR(254)

AS

	SELECT
		respDoc.externalReference,
		respDoc.signedContent	
	FROM
		docsCertificateResponse(nolock) as resp
		join docsCertificateResponse_docs(nolock) as respDoc ON resp.documentsToken = respDoc.documentsToken 
	WHERE
		resp.id = @id

GO
Grant Execute On dbo.up_gerais_getFileXML to Public
Grant Control On dbo.up_gerais_getFileXML to Public
Go