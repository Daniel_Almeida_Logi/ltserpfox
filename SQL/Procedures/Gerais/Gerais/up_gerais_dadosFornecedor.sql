-- Dados do Fornecedor --
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_dadosFornecedor]') IS NOT NULL
	drop procedure dbo.up_gerais_dadosFornecedor
go

create procedure dbo.up_gerais_dadosFornecedor

@no numeric(10),
@estab numeric(5)

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

select top 1 
	nome, nome2, no, u_no, u_entrp, estab, moeda, fax, telefone, esaldo,
	morada, local, codpost, ncont, tipo, email, url,
	u_idclfl, u_clpass, u_ipfl, u_ipport, pais,ccusto, tpdesc
from fl (nolock)
where no = @no and estab=@estab


GO
Grant Execute on dbo.up_gerais_dadosFornecedor to Public
Grant Control on dbo.up_gerais_dadosFornecedor to Public
GO