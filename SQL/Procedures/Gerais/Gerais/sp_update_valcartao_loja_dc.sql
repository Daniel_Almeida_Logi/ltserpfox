
/****** Object:  StoredProcedure [dbo].[sp_insert_utente_loja_dc_atendimento]    Script Date: 8/24/2021 10:19:53 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
if OBJECT_ID('[dbo].[sp_update_valcartao_loja_dc]') IS NOT NULL
	drop procedure dbo.sp_update_valcartao_loja_dc
go
create PROCEDURE [dbo].[sp_update_valcartao_loja_dc]

@nrcartao	varchar(30)
,@valcartao	numeric(20,2)

AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON
	BEGIN TRY  
		IF(1=1 )
		BEGIN
		  BEGIN TRAN

			UPDATE b_fidel 
			SET valcartao = valcartao + @valcartao 
			, valcartaohist = valcartaohist + @valcartao
			WHERE b_fidel.nrcartao=@nrcartao
		
		  COMMIT TRAN
		END
	END TRY 
	BEGIN CATCH  
	   ROLLBACK TRAN
	END CATCH  
		SET XACT_ABORT OFF
--select no from b_utentes where utstamp=@stamp
print 'cart�o atualizado'
END
