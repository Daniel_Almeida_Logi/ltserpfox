/* Ver se o cliente tem notificações

 exec up_gerais_verifica_notif 'ADM', ''
*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_verifica_notif]') IS NOT NULL
	drop procedure dbo.up_gerais_verifica_notif
go

create procedure dbo.up_gerais_verifica_notif
	@user varchar (10),
	@grupo varchar (10)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select 
			stamp
		from notificacoes (NOLOCK)
		WHERE (usrdestino=@user OR grpdestino=@grupo)
			and recebido=0
	 


GO
Grant Execute on dbo.up_gerais_verifica_notif to Public
Grant Control on dbo.up_gerais_verifica_notif to Public
GO


