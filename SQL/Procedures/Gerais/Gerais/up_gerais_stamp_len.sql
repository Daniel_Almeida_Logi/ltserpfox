-- lista de documentos de uma referÍncia
-- exec up_gerais_stamp_len 6
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_stamp_len]') IS NOT NULL
	drop procedure dbo.up_gerais_stamp_len
go

create procedure dbo.up_gerais_stamp_len
@len as numeric(15,0)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	Select left(NEWID(),@len) as stamp

GO
Grant Execute On dbo.up_gerais_stamp_len to Public
Grant Control On dbo.up_gerais_stamp_len to Public
GO