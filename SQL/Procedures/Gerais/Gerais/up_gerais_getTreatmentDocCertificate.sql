/*
	Retorna resposta ao serviço de assinatura de documentos.


	exec up_gerais_getTreatmentDocCertificate 'ADM6FEFB861-1329-4932-8C1','d97228b8-ac2a-499c-9cab-7474f957497'
	exec up_gerais_getTreatmentDocCertificate 'ADM44D005BF-3C7B-45FB-A08','ADM8B31BA09-F739-45AE-8AF'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_gerais_getTreatmentDocCertificate]') IS NOT NULL
	DROP procedure dbo.up_gerais_getTreatmentDocCertificate
GO

CREATE procedure dbo.up_gerais_getTreatmentDocCertificate	
	@token				VARCHAR(36),
	@externalRef		VARCHAR(36),
	@isXML				BIT = 0

AS

	IF @isXML = 0
	BEGIN

		SELECT 
			top 1
			docsCertificateResponse_docs.signStatusId	as estado,
			docsCertificateResponse.causeMessage		as erroMessage,
			docsCertificateResponse_docs.stamp	        as stamp,
			docsCertificateResponse.id as id,
			docsCertificateResponse.causeMessage as mensagem
		FROM 
			docsCertificateResponse(nolock)
			INNER JOIN docsCertificateResponse_docs(nolock) on  docsCertificateResponse.documentsToken = docsCertificateResponse_docs.documentsToken
		WHERE 
			token = @token and 
			docsCertificateResponse_docs.externalReference = @externalRef
		order by docsCertificateResponse.ousrdata desc
	
	END
	ELSE
	BEGIN

		SELECT 
			top 1
			statusID as estado,
			errorCodeDescription as erroMessage,
			'' as stamp,
			id,
			docsCertificateResponse.causeMessage as mensagem
		FROM 
			docsCertificateResponse(nolock)
		WHERE 
			token = @token
		order by docsCertificateResponse.ousrdata desc

	END

GO
GRANT EXECUTE on dbo.up_gerais_getTreatmentDocCertificate TO PUBLIC
GRANT CONTROL on dbo.up_gerais_getTreatmentDocCertificate TO PUBLIC
GO