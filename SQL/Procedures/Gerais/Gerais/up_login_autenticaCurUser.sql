SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_login_autenticaCurUser]') IS NOT NULL
	drop procedure dbo.up_login_autenticaCurUser
go

create procedure dbo.up_login_autenticaCurUser
@pass varchar(100),
@user numeric(6,0)

/* WITH ENCRYPTION */ 
AS 
BEGIN 

	SELECT
		*
	FROM
		b_us(nolock)
	WHERE
		userno = @user
		and password = @pass

END
GO
Grant Execute on dbo.up_login_autenticaCurUser to Public
Grant Control on dbo.up_login_autenticaCurUser to Public
GO