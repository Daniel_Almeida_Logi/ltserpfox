
/* Dados das lojas
	
	declare @result int
	exec up_verifica_ConecaoEstado '172.20.40.6','ATLANTICO',500,4 ,'' , @result output
	print @result 

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_verifica_ConecaoEstado]') IS NOT NULL
	drop procedure up_verifica_ConecaoEstado
go

CREATE PROCEDURE up_verifica_ConecaoEstado
	@server				VARCHAR(60),
	@site				VARCHAR(60),
	@max_ping			NUMERIC(7,0)=NULL,
	@pingNumber			NUMERIC(7,0)=NULL,
	@jobName			VARCHAR(100)='',
    @result				bit output



AS
SET NOCOUNT ON

If OBJECT_ID('tempdb.dbo.#resultFinal') IS NOT NULL
		DROP TABLE #resultFinal

	Create Table #resultFinal(
		outputRes	int
	)


	BEGIN TRY
		EXEC  [up_verifica_Conecao] @server,@site,@max_ping,@pingNumber,@jobName,@result output

		insert into #resultFinal
		values(@result)
	END TRY
	BEGIN CATCH
		set @result = 0

		insert into #resultFinal
		values(@result)
	END CATCH

	 return select top 1 outputRes as result from #resultFinal


	If OBJECT_ID('tempdb.dbo.#resultFinal') IS NOT NULL
		DROP TABLE #resultFinal

GO
Grant Execute On up_verifica_ConecaoEstado to Public
Grant Control On up_verifica_ConecaoEstado to Public
GO