/* Correr sql em todas as bds para limpar tabelas de logs
	exec up_gerais_runAllDatabasesClean
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_gerais_runAllDatabasesClean]') IS NOT NULL
	drop procedure up_gerais_runAllDatabasesClean
go

create PROCEDURE up_gerais_runAllDatabasesClean

	
/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

	
	
	DECLARE @DB_Name varchar(100) = ''
	DECLARE @DB_NameLogs varchar(100) = ''
	DECLARE @DB_NameF varchar(100) = ''
	DECLARE @DB_id smallint = -1
	DECLARE @sql nvarchar(max) = ''
	DECLARE @sql_1 nvarchar(max) = ''
	DECLARE @sql2 nvarchar(max) = ''
	DECLARE @sql1 nvarchar(max) = ''
	declare @date datetime = getdate()-30
			DECLARE @error int =0 

	declare @currentBd varchar(50) = ''
	
	SELECT @currentBd = DB_NAME() 

	set @currentBd = isnull(@currentBd,'DocsLogitools')

	DECLARE database_cursor CURSOR FOR 
	SELECT name 
	FROM MASTER.sys.sysdatabases 
	where 
		(
	    name like 'F%'  or name like 'P%'   
		) -- bds que interessam
		and DATABASEPROPERTYEX('master', 'Status')  = 'ONLINE' AND version IS NOT NULL
	order by name desc

	OPEN database_cursor 

	FETCH NEXT FROM database_cursor INTO @DB_Name 

	WHILE @@FETCH_STATUS = 0 
	BEGIN 
		 print @DB_Name

		  set @DB_id = (SELECT DB_ID(@DB_Name))

		  set @DB_NameF = (SELECT name FROM sys.master_files WHERE database_id = @DB_id AND type = 0) 

		 set @DB_NameLogs = (SELECT name FROM sys.master_files WHERE database_id = @DB_id AND type = 1) 

		 set @sql = '
		    IF(OBJECT_ID('''+@DB_Name+'.dbo.ext_esb_orders'') IS NOT NULL)
			BEGIN
				delete  from '+@DB_Name+'.dbo.ext_esb_orders where date_cre < '''+convert(varchar,@date,120)+'''
			END
			IF(OBJECT_ID('''+@DB_Name+'.dbo.ext_esb_orders_campaigns'') IS NOT NULL)
			BEGIN
				delete  from   '+@DB_Name+'.dbo.ext_esb_orders_campaigns   where ousrdata < '''+convert(varchar,@date,120)+'''
			END
			IF(OBJECT_ID('''+@DB_Name+'.dbo.ext_esb_orders_campaigns_d'') IS NOT NULL)
			BEGIN
				delete  from  '+@DB_Name+'.dbo.ext_esb_orders_campaigns_d  where ousrdata < '''+convert(varchar,@date,120)+'''
			END
			iF(OBJECT_ID('''+@DB_Name+'.dbo.ext_esb_orders_consult'') IS NOT NULL)
			BEGIN
				delete from  '+@DB_Name+'.dbo.ext_esb_orders_consult  where ousrdata < '''+convert(varchar,@date,120)+'''
			END
			iF(OBJECT_ID('''+@DB_Name+'.dbo.ext_esb_orders_consult_states'') IS NOT NULL)
			BEGIN
				delete from  '+@DB_Name+'.dbo.ext_esb_orders_consult_states  where ousrdata < '''+convert(varchar,@date,120)+'''
			END
			iF(OBJECT_ID('''+@DB_Name+'.dbo.ext_esb_orders_consultinvoice'') IS NOT NULL)
			BEGIN
				delete from  '+@DB_Name+'.dbo.ext_esb_orders_consultinvoice   where ousrdata < '''+convert(varchar,@date,120)+'''
			END
			iF(OBJECT_ID('''+@DB_Name+'.dbo.ext_esb_orders_consultinvoice_d'') IS NOT NULL)
			BEGIN
				delete from  '+@DB_Name+'.dbo.ext_esb_orders_consultinvoice_d   where ousrdata < '''+convert(varchar,@date,120)+'''
			END
			iF(OBJECT_ID('''+@DB_Name+'.dbo.ext_esb_orders_consultinvoice_incVats'') IS NOT NULL)
			BEGIN
				delete from  '+@DB_Name+'.dbo.ext_esb_orders_consultinvoice_incVats   where ousrdata < '''+convert(varchar,@date,120)+'''
			END
			iF(OBJECT_ID('''+@DB_Name+'.dbo.ext_esb_orders_consultinvoice_ordersInc'') IS NOT NULL)
			BEGIN
				delete from '+@DB_Name+'.dbo.ext_esb_orders_consultinvoice_ordersInc   where ousrdata < '''+convert(varchar,@date,120)+'''
			END
			iF(OBJECT_ID('''+@DB_Name+'.dbo.ext_esb_orders_d'') IS NOT NULL)
			BEGIN
				delete from '+@DB_Name+'.dbo.ext_esb_orders_d   where ousrdata < '''+convert(varchar,@date,120)+'''
			END
			iF(OBJECT_ID('''+@DB_Name+'.dbo.ext_esb_orders_detailDestination'') IS NOT NULL)
			BEGIN
				delete from  '+@DB_Name+'.dbo.ext_esb_orders_detailDestination  where ousrdata < '''+convert(varchar,@date,120)+'''
			END
			iF(OBJECT_ID('''+@DB_Name+'.dbo.bi2'') IS NOT NULL)
			BEGIN
				delete '+@DB_Name+'.dbo.bi2 from '+@DB_Name+'.dbo.bi2 inner join '+@DB_Name+'.dbo.bo on '+@DB_Name+'.dbo.bo.bostamp = '+@DB_Name+'.dbo.bi2.bostamp inner join '+@DB_Name+'.dbo.ts on  '+@DB_Name+'.dbo.bo.ndos = '+@DB_Name+'.dbo.ts.ndos where '+@DB_Name+'.dbo.bo.usrdata <= dateadd(m,-6,getdate()) and '+@DB_Name+'.dbo.ts.codigoDoc = 38
			END
			iF(OBJECT_ID('''+@DB_Name+'.dbo.bo2'') IS NOT NULL)
			BEGIN
				delete '+@DB_Name+'.dbo.bo2 from '+@DB_Name+'.dbo.bo2 inner join '+@DB_Name+'.dbo.bo on '+@DB_Name+'.dbo.bo2.bo2stamp = '+@DB_Name+'.dbo.bo.bostamp inner join '+@DB_Name+'.dbo.ts on  '+@DB_Name+'.dbo.bo.ndos = '+@DB_Name+'.dbo.ts.ndos where '+@DB_Name+'.dbo.bo.usrdata <= dateadd(m,-6,getdate()) and '+@DB_Name+'.dbo.ts.codigoDoc = 38 
			END
			iF(OBJECT_ID('''+@DB_Name+'.dbo.bi'') IS NOT NULL)
			BEGIN
				delete '+@DB_Name+'.dbo.bi from '+@DB_Name+'.dbo.bi inner join '+@DB_Name+'.dbo.bo on '+@DB_Name+'.dbo.bo.bostamp = '+@DB_Name+'.dbo.bi.bostamp inner join '+@DB_Name+'.dbo.ts on  '+@DB_Name+'.dbo.bo.ndos = '+@DB_Name+'.dbo.ts.ndos where '+@DB_Name+'.dbo.bo.usrdata <= dateadd(m,-6,getdate()) and '+@DB_Name+'.dbo.ts.codigoDoc = 38
			END 
			iF(OBJECT_ID('''+@DB_Name+'.dbo.bo_totais'') IS NOT NULL)
			BEGIN
				delete '+@DB_Name+'.dbo.bo_totais from '+@DB_Name+'.dbo.bo_totais inner join '+@DB_Name+'.dbo.bo on '+@DB_Name+'.dbo.bo.bostamp = '+@DB_Name+'.dbo.bo_totais.stamp inner join '+@DB_Name+'.dbo.ts on  '+@DB_Name+'.dbo.bo.ndos = '+@DB_Name+'.dbo.ts.ndos where '+@DB_Name+'.dbo.bo.usrdata <= dateadd(m,-6,getdate()) and '+@DB_Name+'.dbo.ts.codigoDoc = 38 
			END
			
			iF(OBJECT_ID('''+@DB_Name+'.dbo.bo'') IS NOT NULL)
			BEGIN
				delete '+@DB_Name+'.dbo.bo from '+@DB_Name+'.dbo.bo inner join '+@DB_Name+'.dbo.ts on  '+@DB_Name+'.dbo.bo.ndos = '+@DB_Name+'.dbo.ts.ndos where bo.usrdata <= dateadd(m,-6,getdate()) and ts.codigoDoc = 38  
			END '

			set @sql_1 =' 
			iF(OBJECT_ID('''+@DB_Name+'.dbo.bonus_d'') IS NOT NULL)
			BEGIN
				delete '+@DB_Name+'.dbo.bonus_d from '+@DB_Name+'.dbo.bonus_d inner join '+@DB_Name+'.dbo.bonus on  '+@DB_Name+'.dbo.bonus.stamp = '+@DB_Name+'.dbo.bonus_d.bonus_stamp where bonus.ativo=0  
			END

			iF(OBJECT_ID('''+@DB_Name+'.dbo.bonus'') IS NOT NULL)
			BEGIN
				delete  from '+@DB_Name+'.dbo.bonus where bonus.ativo=0  
			END

			
			use '+ @DB_Name +'
			ALTER DATABASE ['+ @DB_Name +'] SET RECOVERY SIMPLE
			DBCC SHRINKFILE ('+ @DB_NameF +', 1)
			ALTER DATABASE ['+ @DB_Name +'] SET RECOVERY FULL
			use '+ @currentBd +'	

		'
		BEGIN TRY
			print @sql 
			EXEC sp_executesql @sql
			print @sql_1
			EXEC sp_executesql @sql_1

		END TRY
		BEGIN CATCH 
			IF  EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'dbInfo')
			BEGIN  
				Insert into [DocsLogitools].dbo.dbInfo (stamp,dbName,serviceId,serviceIdDesc,statusID,statusIDDesc,ousrdata)
				Values(LEFT(NEWID(),36),@DB_Name,1,'DBCLEAN',1 ,'Error '+ ERROR_MESSAGE(),GETDATE())
			END
			set @error=1

		END CATCH; 



		EXEC  up_gerais_rebuildIndexsTable @DB_Name


		set @sql1  = 'use '+ @DB_Name +'
			ALTER DATABASE ['+ @DB_Name +'] SET RECOVERY SIMPLE
			DBCC SHRINKFILE ('+ @DB_NameLogs +', 1)
			ALTER DATABASE ['+ @DB_Name +'] SET RECOVERY FULL
			use '+ @currentBd +'
			
			'	
		BEGIN TRY
			print @sql
			EXEC sp_executesql @sql1
		END TRY
		BEGIN CATCH 
			IF  EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'dbInfo')
			BEGIN 
			 Insert into [DocsLogitools].dbo.dbInfo (stamp,dbName,serviceId,serviceIdDesc,statusID,statusIDDesc,ousrdata)
			 Values(LEFT(NEWID(),36),@DB_Name,1,'DBCLEAN',1 ,'Error ' + ERROR_MESSAGE(),GETDATE())
			END
			 set @error=1
		END CATCH; 

		set @sql2 = 'use '+ @DB_Name +' ALTER DATABASE ['+ @DB_Name +'] SET RECOVERY SIMPLE DBCC SHRINKFILE ('+ @DB_NameF +', 1) ALTER DATABASE ['+ @DB_Name +'] SET RECOVERY FULL'
	
		BEGIN TRY	
			print @sql2
			EXEC sp_executesql @sql2 
		END TRY
		BEGIN CATCH  
			IF  EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'dbInfo')
			BEGIN
				Insert into [DocsLogitools].dbo.dbInfo (stamp,dbName,serviceId,serviceIdDesc,statusID,statusIDDesc,ousrdata)
				Values(LEFT(NEWID(),36),@DB_Name,1,'DBCLEAN',1 ,'Error ' + ERROR_MESSAGE(),GETDATE())
			END
			set @error=1
		END CATCH; 

		FETCH NEXT FROM database_cursor INTO @DB_Name 
	
	END 

	CLOSE database_cursor 
	DEALLOCATE database_cursor 

	if (@error =0) and  EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'dbInfo')
	begin 
		Insert into [DocsLogitools].dbo.dbInfo (stamp,dbName,serviceId,serviceIdDesc,statusID,statusIDDesc,ousrdata)
		Values(LEFT(NEWID(),36),@DB_Name,1,'DBCLEAN',0 ,'Sucess up_gerais_runAllDatabasesClean ',GETDATE())
	end 

	
	If OBJECT_ID('tempdb.dbo.#temp') IS NOT NULL
		DROP TABLE #temp
	
GO
Grant Execute On up_gerais_runAllDatabasesClean to Public
Grant Control On up_gerais_runAllDatabasesClean to Public
GO






