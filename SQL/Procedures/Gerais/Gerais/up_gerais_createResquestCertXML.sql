/*

	Gera requests para o servi�o de Certifica��o de XML

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_gerais_createResquestCertXML]') IS NOT NULL
	DROP procedure dbo.up_gerais_createResquestCertXML
GO

CREATE procedure dbo.up_gerais_createResquestCertXML	
	@token varchar(36),
    @documentsToken varchar(36),
    @personReceiverToken varchar(36),
    @externalReference varchar(50),
    @path varchar(254),
	@ftstamp varchar(30),
	@teste int,
	@id VARCHAR(254),
	@request VARCHAR(40),
	@type INT = 2

AS

	DECLARE @doc VARCHAR(25),
			@site VARCHAR(60)

	SET @doc = (SELECT (CASE WHEN tiposaft = 'NC' THEN 'CreditNote' ELSE 'Invoice' END) FROM td(nolock) WHERE ndoc = (SELECT ndoc FROM ft(NOLOCK) WHERE ftstamp = @ftstamp))

	SET @site = (SELECT site FROM ft(nolock) WHERE ftstamp = @ftstamp)

	IF @request = 'CREATE'
	BEGIN

		insert into docsCertificateRequest (token,documentsToken,externalReference,type,typeDesc,certifyingEntity,certifyingEntityName,description
						,returnSignedContent,preserInfoAlias,preserInfoContent,preserInfoAttachDocuments,personReceiverToken,
						site,test,ousrdata,ousrinis,usrdata,usrinis,accountType,accountTypeDesc, operationID)
      
		values (@token,@documentsToken,@externalReference,@type,'DOCCERTXML',1,'MULTICERT','Assinatura Digital Via Multicert',
			1,'document_pretty_name.xml',0,0,@personReceiverToken,@site,@teste,getdate(),'',getdate(),'',2,'PREMIUM', @id) 
      

		insert into docsCertificateRequest_PersonRec (stamp,personReceiverToken,deliveryStatus,email,ousrdata,ousrinis,usrdata,usrinis)
			values (left(NEWID(),36),@personReceiverToken,'SENT','tecnico@lts.pt',getdate(),'',getdate(),'') 


		insert into docsCertificateRequest_docs (stamp,documentsToken,ftstamp,externalReference,name,nameDocToSign,filePath,filePathToSign,contentType,applyTimestamp,location
			,reason,signatureType,area,areaBackgroundColor,font,imageType,imageValue,locationType,locationValue
			,oneOffTemplate,templateType,canonicalTransform,signatureLocation,signatureTransform,ousrdata,ousrinis,usrdata,usrinis)
			values(left(NEWID(),36),@documentsToken,@ftstamp,@externalReference,@externalReference+'_signed.xml' ,@externalReference+'.xml',@path,@path,'text/xml',1,'Assinatura Digital Via Multicert'
			,'Assinatura Digital: Decreto-Lei  n� 123/2018','XAdES','width=200,height=400','transparent','color=#000000,size=11','','','COORDINATE','page=1,x=100,y=200'
			,'Digitally Signed by $certificate.getCommonName().','ONE_OFF','EXCLUSIVE_WITH_COMMENTS',@doc,'ENVELOPED',getdate(),'',getdate(),'') 

	END

	IF @request = 'SEND'
	BEGIN

		insert into docsCertificateRequest (token,documentsToken,externalReference,type,typeDesc,certifyingEntity,certifyingEntityName,description
                        ,returnSignedContent,preserInfoAlias,preserInfoContent,preserInfoAttachDocuments,personReceiverToken,
                        site,test,ousrdata,ousrinis,usrdata,usrinis,accountType,accountTypeDesc)
         
         values (@token,@documentsToken,@externalReference,5,'DOCSENDXML',2,'FE-AP','Assinatura Digital Via Multicert',
               1,'',0,0,'',@site,@teste,getdate(),'',getdate(),'',2,'PREMIUM') 
         

         insert into docsCertificateRequest_docs (stamp,documentsToken,ftstamp,externalReference,name,nameDocToSign,filePath,filePathToSign,contentType,applyTimestamp,location
               ,reason,signatureType,area,areaBackgroundColor,font,imageType,imageValue,locationType,locationValue
               ,oneOffTemplate,templateType,canonicalTransform,signatureLocation,signatureTransform,ousrdata,ousrinis,usrdata,usrinis)
               values(left(NEWID(),36),@documentsToken,@ftstamp,@externalReference,'' ,@externalReference+'_signed.xml','',@path,'text/xml',1,'Assinatura Digital Via Multicert'
               ,'Assinatura Digital: Decreto-Lei  n� 123/2018','XAdES','width=200,height=400','transparent','color=#000000,size=11','','','COORDINATE','page=1,x=100,y=200'
               ,'Digitally Signed by $certificate.getCommonName().','ONE_OFF','EXCLUSIVE_WITH_COMMENTS',@doc,'ENVELOPED',getdate(),'',getdate(),'')

	END

	IF @request = 'CHECK'
	BEGIN

        insert into docsCertificateRequest (token,documentsToken,externalReference,type,typeDesc,certifyingEntity,certifyingEntityName,description
                        ,returnSignedContent,preserInfoAlias,preserInfoContent,preserInfoAttachDocuments,personReceiverToken,
                        site,test,ousrdata,ousrinis,usrdata,usrinis,accountType,accountTypeDesc,operationID)
            
        values (@token,'',@externalReference,4,'GETDOC',2,'FE-AP','Assinatura Digital Via Multicert',
                1,'',0,0,'',@site,@teste,getdate(),'',getdate(),'',2,'PREMIUM',@id) 

	END


GO
GRANT EXECUTE on dbo.up_gerais_createResquestCertXML TO PUBLIC
GRANT CONTROL on dbo.up_gerais_createResquestCertXML TO PUBLIC
GO