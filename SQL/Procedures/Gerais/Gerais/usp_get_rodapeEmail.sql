/*  envia informação do rodape do site 
	EXEC usp_get_rodapeEmail 'ADM','Loja 1'
	b_us
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[usp_get_rodapeEmail]') IS NOT NULL
	drop procedure dbo.usp_get_rodapeEmail 
go

CREATE PROCEDURE [dbo].[usp_get_rodapeEmail]
	@inicial		VARCHAR(3),
	@site			VARCHAR(20)
AS
SET NOCOUNT ON

	DECLARE @nome VARCHAR(150)
	Declare @rodape VARCHAR(3000) 
	

	IF(EXISTS(SELECT  object_id FROM sys.tables WHERE NAME = 'b_us'))
	BEGIN
		SELECT @nome= rtrim(ltrim(nome)) FROM b_us (NOLOCK) WHERE iniciais=@inicial and loja=@site
	END 
	ELSE
	BEGIN 
	set @nome ='Logitools'

	END 
	SELECT 

			CAST(
			+ CHAR(13)+CHAR(10) +	 ISNULL(@nome,'') + 
			+ CHAR(13)+CHAR(10) +  + ISNULL(nomecomp,'') +
			+ CHAR(13)+CHAR(10) + 'NIF: '  + ISNULL(ncont,'') + 
			+ CHAR(13)+CHAR(10)  + ISNULL(morada,'') +
			+ CHAR(13)+CHAR(10) + ISNULL(codpost,'') +
			+ CHAR(13)+CHAR(10) + 'telef: ' + ISNULL(telefone,'') +
			+ CHAR(13)+CHAR(10) + 'email: ' + ISNULL(email,'') 	
			+ CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10) + 'Esta mensagem foi enviada automaticamente através do software Logitools, pelo remetente acima indicado. Por favor não responda ao mesmo.'
			+ CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10) + '_________________________________________________________________________________________________________________________________________________________'
			+ CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10) + 'Esta mensagem é enviada de acordo com a legislação Europeia em vigor sobre o envio de mensagens comerciais, ao abrigo da Directiva 2000/31/CE do Parlamento Europeu e Relatório A5-0270/2001 do Parlamento Europeu, e não pode ser considerado "SPAM", pois está claramente identificada pelo seu emissor. Ao abrigo do Regulamento Geral da Proteção de Dados, o destinatário poderá a qualquer momento proceder à retificação ou cancelamento dos seus dados pessoais. Se pretender eliminar os seus dados, envie mensagem de resposta com o título “remover”.'
			+ CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10) + 'AVISO LEGAL Esta mensagem e quaisquer documentos anexos seus podem conter informação confidencial sujeita a sigilo profissional para uso exclusivo do(s) seu(s) destinatário(s). Cabe ao destinatário assegurar a verificação da existência de vírus ou erros, uma vez que a informação contida pode ser intercetada ou corrompida. Se não for o destinatário, não deverá usar, distribuir ou copiar este e-mail, devendo proceder à sua eliminação e informar o emissor. É estritamente proibido o uso, a distribuição, cópia ou qualquer forma de disseminação não autorizada do conteúdo desta mensagem.'
			+ CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10) + 'DISCLAIMER This message, as well as any attachments to it, may contain confidential information for exclusive use of the intended recipients. The recipients are responsible for the verification of the existence of viruses or errors, since the information transmitted could have been intercepted or in any way corrupted. If you´re not the intended recipient, you cannot use, distribute or copy this message, and you should destroy it and inform the originator of it. It´s strictly prohibited the use, distribution copy or any other form of unauthorized dissemination of this message´s content.'AS VARCHAR(3000)) rodape ,
			+ CHAR(13)+CHAR(10) + site as site,
			+ CHAR(13)+CHAR(10) + @inicial as iniciais 
	FROM empresa (nolock) 
	WHERE site =@site


	--select@rodape as rodape

GO
GRANT EXECUTE ON dbo.usp_get_rodapeEmail to Public
GRANT CONTROL ON dbo.usp_get_rodapeEmail to Public
GO