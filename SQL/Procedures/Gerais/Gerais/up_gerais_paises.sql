-- Lista de Pa�ses --
-- exec up_gerais_paises
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_paises]') IS NOT NULL
	drop procedure dbo.up_gerais_paises
go

create procedure dbo.up_gerais_paises

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

select 
	distinct CAST(0 as bit)as sel,
	nome = nomePais,
	nomeabrv = code 
from b_cli_tabela_pais (nolock)
order by nome


GO
Grant Execute on dbo.up_gerais_paises to Public
Grant Control on dbo.up_gerais_paises to Public
GO