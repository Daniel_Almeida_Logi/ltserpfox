-- Procura Mensagens do utilizador
-- exec up_mensagens_pesquisa 'adm'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_mensagens_pesquisa]') IS NOT NULL
	drop procedure dbo.up_mensagens_pesquisa
go

create procedure dbo.up_mensagens_pesquisa

@iniciais varchar(3)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

SELECT  
	Resumo 
	,descricao
	,mgstamp
	,visto
	,resposta
	,de
	,data
	,stamp
	
FROM
	mg (nolock)
--where
--	para = @iniciais
order by
	data desc

GO
Grant Execute on dbo.up_mensagens_pesquisa to Public
Grant Control on dbo.up_mensagens_pesquisa to Public
Go


