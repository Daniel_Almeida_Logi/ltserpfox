-- Dados das lojas --
-- �ltima Altera��o: 2015-06-30
-- exec up_gerais_armazensDisponiveisLista 'Loja 1', '1', 'asd'


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_gerais_armazensDisponiveisLista]') IS NOT NULL
	drop procedure up_gerais_armazensDisponiveisLista
go

create PROCEDURE up_gerais_armazensDisponiveisLista
	@site varchar(60)
	,@userno int
	,@grupo	varchar(60)

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON
	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#Empresas'))
		DROP TABLE #Empresas
	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempPfu'))
		DROP TABLE #tempPfu
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempPfg'))
		DROP TABLE #tempPfg
		
		
			
	Select 
		b_pfu.*
	into
		#tempPfu
	From
		b_pf (nolock)
		inner join b_pfu (nolock) on b_pfu.pfstamp = b_pf.pfstamp
	Where
		b_pf.resumo  = 'Acesso � empresa'
		
	Select 
		b_pfg.*
	into
		#tempPfg
	From
		b_pf (nolock)
		inner join b_pfg (nolock) on b_pfg.pfstamp = b_pf.pfstamp
	Where
		b_pf.resumo  = 'Acesso � empresa'
		
	declare @userSiteDefault varchar(20)
	set @userSiteDefault = (select loja from b_us where userno = @userno)

	select 
		distinct 
		site_nr = empresa.no
	into
		#Empresas
	from 
		empresa (nolock)
		left join b_pf (nolock) on RTRIM(LTRIM(b_pf.resumo)) = RTRIM(LTRIM(empresa.site)) + ' - Acesso '
		left join #tempPfu pfu (nolock) on RTRIM(LTRIM(pfu.site)) = RTRIM(LTRIM(empresa.site))
		left join #tempPfg pfg (nolock) on RTRIM(LTRIM(pfg.site)) = RTRIM(LTRIM(empresa.site))
	Where
		(isnull(pfu.userno,9999) = @userno Or ISNULL(pfg.nome,'') = @grupo) 
		or empresa.site = @userSiteDefault /*Tem sempre acesso � loja default do utilizador*/

	--
	select
		sel	= convert(bit,0)
		,empresa_arm.armazem
	from
		empresa_arm (nolock)
		inner join #Empresas on #empresas.site_nr = empresa_arm.empresa_no	

GO
Grant Execute On up_gerais_armazensDisponiveisLista to Public
Grant Control On up_gerais_armazensDisponiveisLista to Public
GO