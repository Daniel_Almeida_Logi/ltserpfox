/* Correr sql em todas as bds
	

	exec up_gerais_runAllDatabasesAdminGripe '20210501', '20211231'


*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_gerais_runAllDatabasesAdminGripe]') IS NOT NULL
	drop procedure up_gerais_runAllDatabasesAdminGripe
go

create PROCEDURE up_gerais_runAllDatabasesAdminGripe
	@dateInit datetime,
	@dateEnd  datetime

	
/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

	If OBJECT_ID('tempdb.dbo.#temp') IS NOT NULL
		DROP TABLE #temp
	
	If OBJECT_ID('tempdb.dbo.#resultAdmin') IS NOT NULL
		DROP TABLE #resultAdmin
	


	set @dateEnd = @dateEnd + '23:59:00'
	DECLARE @DB_Name varchar(100) 
	DECLARE @Command nvarchar(max) 

	create table #resultAdmin
	(
		qtt      int 
	)

	DECLARE database_cursor CURSOR FOR 
	SELECT name 
	FROM MASTER.sys.databases 
	where 
		state = 0 and 
		(name like 'F%' and name not like '%FFS_%' and  len(name) <=7) -- bds que interessam
	order by name desc

	OPEN database_cursor 

	FETCH NEXT FROM database_cursor INTO @DB_Name 

	WHILE @@FETCH_STATUS = 0 
	BEGIN 
		
		set @Command='
		insert into #resultAdmin
			select count(*) as qtt from ' +@DB_Name + '.dbo.B_vacinacao B_vacinacao(nolock) 
			inner join   ' +@DB_Name + '.dbo.empresa  empresa(nolock) on empresa.no=B_vacinacao.site_nr
			where  tipoempresa = ''FARMACIA''  and assfarm = ''AFP'' and		
			comercial_id=''5726229'' and send = 1 and result = 1
			and dataAdministracao>='''+convert(varchar, @dateInit, 120)+''' and dataAdministracao <= '''+convert(varchar, @dateEnd, 120)+'''

		 '
		 
		 print @Command
		 EXEC sp_executesql @Command 

	FETCH NEXT FROM database_cursor INTO @DB_Name 
	END 

	CLOSE database_cursor 
	DEALLOCATE database_cursor 


	select sum(qtt) as Qtt from #resultAdmin


	If OBJECT_ID('tempdb.dbo.#resultAdmin') IS NOT NULL
		DROP TABLE #resultAdmin

	
	If OBJECT_ID('tempdb.dbo.#temp') IS NOT NULL
		DROP TABLE #temp
	
GO
Grant Execute On up_gerais_runAllDatabasesAdminGripe to Public
Grant Control On up_gerais_runAllDatabasesAdminGripe to Public
GO

