/* SP que devolve os totais do valor de cart�ao em cada cliente
	
	up_gerais_at_devolve_total_cartao 'adada','Loja 1'


*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_at_devolve_total_cartao]') IS NOT NULL
	drop procedure dbo.up_gerais_at_devolve_total_cartao
go

create procedure dbo.up_gerais_at_devolve_total_cartao

@site varchar(20)
,@u_nratend varchar(20)

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

	select
	 valcartao = isnull(sum(ft2.valcartao),0),
	 valcartaoutilizado = isnull(sum(ft2.valcartaoutilizado),0)
	from ft2(nolock)
	inner join ft(nolock) on ft.ftstamp = ft2.ft2stamp
	where ft.u_nratend = @u_nratend and ft.site=@site


GO
Grant Execute on dbo.up_gerais_at_devolve_total_cartao to Public
Grant Control on dbo.up_gerais_at_devolve_total_cartao to Public
GO