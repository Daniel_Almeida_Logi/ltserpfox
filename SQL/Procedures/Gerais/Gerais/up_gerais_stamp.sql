-- lista de documentos de uma referÍncia
-- exec up_gerais_stamp 'ADM'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_stamp]') IS NOT NULL
	drop procedure dbo.up_gerais_stamp
go

create procedure dbo.up_gerais_stamp
@iniciais as varchar(50)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	Select left(@iniciais,3) + left(NEWID(),22) as stamp

GO
Grant Execute On dbo.up_gerais_stamp to Public
Grant Control On dbo.up_gerais_stamp to Public
GO