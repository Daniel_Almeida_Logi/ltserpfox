-- lista taxas de iva para ser usada nas vendas / documentos
-- exec up_gerais_taxasIVA
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_taxasIVA]') IS NOT NULL
	drop procedure dbo.up_gerais_taxasIVA
go

create procedure dbo.up_gerais_taxasIVA

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON	

select 
	 iva1 = isnull((select taxa from taxasiva (nolock) where codigo = 1),0)
	,iva2 = isnull((select taxa from taxasiva (nolock) where codigo = 2),0)
	,iva3 = isnull((select taxa from taxasiva (nolock) where codigo = 3),0)
	,iva4 = isnull((select taxa from taxasiva (nolock) where codigo = 4),0)
	,iva5 = isnull((select taxa from taxasiva (nolock) where codigo = 5),0)
	,iva6 = isnull((select taxa from taxasiva (nolock) where codigo = 6),0)
	,iva7 = isnull((select taxa from taxasiva (nolock) where codigo = 7),0)
	,iva8 = isnull((select taxa from taxasiva (nolock) where codigo = 8),0)
	,iva9 = isnull((select taxa from taxasiva (nolock) where codigo = 9),0)

GO
Grant Execute On dbo.up_gerais_taxasIVA to Public
Grant Control On dbo.up_gerais_taxasIVA to Public
GO