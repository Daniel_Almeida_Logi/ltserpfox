/*

 exec up_login_autenticaUser_local 'Farm@L0g1', 'sa', 'Loja 1'

 exec up_login_autenticaUser_altera '383887CDCE0B96FBF1C846EAB3C14F72', '', 'Loja 1'

 exec up_gerais_trocaEmpresa 1,'Adminsitrador'
 
 select * from b_us where password='383887CDCE0B96FBF1C846EAB3C14F72'
 select * from b_parameters where name like '%período%'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_login_autenticaUser_altera]') IS NOT NULL
	drop procedure dbo.up_login_autenticaUser_altera
go

create procedure dbo.up_login_autenticaUser_altera
@pass varchar(50),
@user varchar(30),
@site varchar(20)

/* WITH ENCRYPTION */ 
AS 
BEGIN 
	
	select 
		iniciais
		,pwpos=password
		,userno
		,nome
		,grupo
		,vendnm = username
		,drcedula
		,drclprofi
	from 
		b_us as us (nolock)
	where
		us.password = @pass
		and us.inactivo = 0

END


GO
Grant Execute on dbo.up_login_autenticaUser_altera to Public
Grant Control on dbo.up_login_autenticaUser_altera to Public
GO
--------------------------------------------------------------