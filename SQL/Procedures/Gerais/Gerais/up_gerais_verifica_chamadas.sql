/* Ver se o cliente tem notificações

 exec up_gerais_verifica_chamadas 'ADM', ''
 exec up_gerais_verifica_chamadas 'ADM', 'Administrador'
*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_verifica_chamadas]') IS NOT NULL
	drop procedure dbo.up_gerais_verifica_chamadas
go

create procedure dbo.up_gerais_verifica_chamadas
	@user varchar (10),
	@grupo varchar (10)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

If OBJECT_ID('tempdb.dbo.#temp_telef') IS NOT NULL
	drop table #temp_telef;

select 
		distinct 
			stampdestino
			, mensagem
			, isnull((select top 1 nome from b_utentes (nolock) where telefone=stampdestino or tlmvl=stampdestino),'') as nomecl
			, isnull((select top 1 nome from fl (nolock) where telefone=stampdestino or tlmvl=stampdestino),'') as nomefl
			, isnull((select top 1 nome from telefones (nolock) where telefone=stampdestino),'') as nometlf
		into #temp_telef
		from notificacoesRapidas (NOLOCK)
		WHERE recebido=0
			and tiponotif='Chamada'

select 
	stampdestino
	, cast(mensagem as varchar(200)) as mensagem
	, case when nomecl<>'' 
		then nomecl
		else case when nomefl<>''
				then nomefl
				else nometlf
			end
	end as nome
	, case when nomecl<>'' 
		then 'CL'
		else case when nomefl<>''
				then 'FL'
				else case when nometlf<>''
						then 'TLF'
						else ''
					end 
			end
	end as tipo
from #temp_telef

GO
Grant Execute on dbo.up_gerais_verifica_chamadas to Public
Grant Control on dbo.up_gerais_verifica_chamadas to Public
GO


