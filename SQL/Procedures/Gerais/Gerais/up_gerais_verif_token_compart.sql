-- procura se receita já foi efetivada no mesmo
-- exec up_gerais_verif_token_compart 'ADM288E53DA-7418-4E9A-A88'
--		exec up_gerais_verif_token_compart 'ADMFA016C51-58D9-4439-A41'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerais_verif_token_compart]') IS NOT NULL
	drop procedure dbo.up_gerais_verif_token_compart
go

create procedure dbo.up_gerais_verif_token_compart
@token as varchar(50)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	select token from ft_compart (nolock) where utilizado=1  and type=1 and token=@token order by ousrdata desc
	 
GO
Grant Execute On dbo.up_gerais_verif_token_compart to Public
Grant Control On dbo.up_gerais_verif_token_compart to Public
GO