/*
	apaga registo no mapa
	
	up_robot_consis_deleteMap 25,2
	select * from robo_consis_map
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_robot_consis_deleteMap]') IS NOT NULL
	drop procedure dbo.up_robot_consis_deleteMap
go

create procedure dbo.up_robot_consis_deleteMap
	@armario smallint
	,@canal smallint


/* with encryption */
AS

	DELETE 
		robo_consis_map 
	Where 
		Armario = @armario
		and Canal = @canal

GO
Grant Execute on dbo.up_robot_consis_deleteMap to Public
Grant Control on dbo.up_robot_consis_deleteMap to Public