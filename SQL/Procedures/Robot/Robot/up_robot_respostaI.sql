-- Procura Resposta do Robot à Mensagem 'I'
-- exec up_robot_respostaI 'EXC105 ', 1
-- exec up_robot_respostaI '22X1/1059737', 1

-- exec up_robot_respostaI 'A.FAC1234567', 0
-- exec up_robot_respostaI 'daniel123', 0
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_robot_respostaI]') IS NOT NULL
	drop procedure dbo.up_robot_respostaI
go

create procedure dbo.up_robot_respostaI

@no varchar(30),
@robo int = 0

/* with encryption */
AS

	SELECT convert(varchar(100),id) as id, Order_State,
		obrano	= ltrim(rtrim(delivery_number)),
		qtt		= Quantity,
		ref			= case when @robo=0
							then convert(varchar,convert(int,ltrim(Packing_List_Number)))
							when @robo=1
							then ltrim(Product_Code)
							else convert(varchar,convert(int,ltrim(Packing_List_Number)))
						end,
/*		design	= ISNULL(design,''), */
		timeExpired, posto, oData, conferido,
		product_id, Product_Code, 
		(CASE WHEN [date] <> '000000' THEN convert (datetime,  Stuff(Stuff([date],5,0,'.'),3,0,'.'), 4) ELSE CONVERT(datetime, '19000101') END) AS dtVal
	FROM B_movtec_robot (nolock)
/*	left join st (nolock) on st.ref=isnull((select top 1 st.ref from st (nolock) left join bc (nolock) on bc.ref=st.ref
											where (st.ref=convert(varchar,convert(int,ltrim(Packing_List_Number))) 
												OR bc.codigo=convert(varchar,convert(int,ltrim(Packing_List_Number)))
												OR st.codigo=convert(varchar,convert(int,ltrim(Packing_List_Number))))
												AND st.inactivo=0)
									,'') */
	where msg_id='i'
		--and Order_State in ('02', '03', '00', '06', '07') /*considerar 01 e 05*/
		and Order_State = '06'
		and ltrim(Delivery_Number)=@no
		and id not in (select id from B_movtec_robot (nolock)
						where msg_id='i' and order_state='03' and ltrim(Delivery_Number)=@no
							and id not in (select top 1 id from B_movtec_robot (nolock)
										where msg_id='i' and order_state='03' and ltrim(Delivery_Number)=@no order by id desc)
						)
--	Order by id	
	union
	 SELECT convert(varchar(100),ordernumber), '0' + isnull(convert(varchar(10),state),'') as Order_State,
		obrano	= cast(ltrim(rtrim(DeliveryNumber)) as varchar(15)),
		qtt		= cast(ltrim(rtrim(Quantity)) as varchar(15)),
		ref			= case when ltrim(rtrim(isnull(productNhrn,''))) = '' then  productCode else ltrim(rtrim(productNhrn)) end,
		0 as timeExpired, '' as posto
		,oData = case when(year(date))<2010 then '1900-01-01 00:00:00.000' else date end,
		conferido,
		product_id = case when ltrim(rtrim(isnull(productNhrn,''))) = '' then  productCode else ltrim(rtrim(productNhrn)) end,	
		productCode = case when ltrim(rtrim(isnull(productNhrn,''))) = '' then  productCode else ltrim(rtrim(productNhrn)) end,
		(CASE WHEN YEAR(robot_response_payload.[date]) in (1900,2000) THEN CONVERT(datetime, '19000101') ELSE CONVERT(datetime, robot_response_payload.[date]) END) as dtVal
	FROM robot_response_payload (nolock)
	inner join robot_response(nolock) on robot_response.stamp = robot_response_payload.responseStamp
	where 
		cast(ltrim(rtrim(DeliveryNumber)) as varchar(15)) = @no and state in (7) and conferido = 0  and robot_response.reqType ='charge'
	Order by id	

				
GO
Grant Execute on dbo.up_robot_respostaI to Public
Grant Control on dbo.up_robot_respostaI to Public
Go





