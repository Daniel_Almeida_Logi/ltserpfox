-- Gerar documento de Trf. entre Armaz�ns para Robot
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_robot_TrfEntreArmazens]') IS NOT NULL
	drop procedure dbo.up_robot_TrfEntreArmazens
go

create procedure dbo.up_robot_TrfEntreArmazens

@ref varchar(18),
@qtt numeric(10,2),
@site varchar(60),
@vendedor numeric(6),
@vendnm varchar(60),
@armazemDest numeric(5)

/* with encryption */
AS

	/* calcular valores para bo */
	declare @epcusto numeric(13,3), @tabiva numeric(2), @etotaldeb numeric(13,3)
	declare @taxaiva numeric(6,2), @totalComIva numeric(13,3)
	declare @valoriva1 numeric(10,3), @valoriva2 numeric(10,3), @valoriva3 numeric(10,3), @valoriva4 numeric(10,3), @valoriva5 numeric(10,3)
	declare @totalSemIva1 numeric(10,3), @totalSemIva2 numeric(10,3), @totalSemIva3 numeric(10,3), @totalSemIva4 numeric(10,3), @totalSemIva5 numeric(10,3)
	set @epcusto			= (select epcusto from st (nolock) where st.ref=@ref)
	set @tabiva			= (select tabiva from st (nolock) where st.ref=@ref)
	set @etotaldeb		= 0
	set @totalComIva		= 0
	set @totalSemIva1	= 0
	set @totalSemIva2	= 0
	set @totalSemIva3	= 0
	set @totalSemIva4	= 0
	set @totalSemIva5	= 0
	set @taxaIva			= 0
	set @valorIva1		= 0
	set @valorIva2		= 0
	set @valorIva3		= 0
	set @valorIva4		= 0
	set @valorIva5		= 0
	
	/* Calcular os valores base de incid�ncia e valor do iva por taxa de iva */
	set @etotaldeb=@epcusto*@qtt
	
	if @tabiva=1
	Begin
		set @taxaIva		= (select convert(float,replace((select valor from para1 where descricao='ativa1'),',','.'),4)/100)
		set @valorIva1		= (@epcusto*@qtt*@taxaIva)
		set @totalSemIva1	= (@epcusto*@qtt)
		set @totalComIva	= @epcusto*@qtt*(@taxaIva+1)
	End
	Else
		if @tabiva=2
		Begin
			set @taxaIva		= (select convert(float,replace((select valor from para1 where descricao='ativa2'),',','.'),4)/100)
			set @valorIva2		= (@epcusto*@qtt*@taxaIva)
			set @totalSemIva2	= (@epcusto*@qtt)
			set @totalComIva	= @epcusto*@qtt*(@taxaIva+1)
		End
		Else
			if @tabiva=3
			begin
				set @taxaIva		= (select convert(float,replace((select valor from para1 where descricao='ativa3'),',','.'),4)/100)
				set @valoriva3		= (@epcusto*@qtt*@taxaIva)
				set @totalSemIva3	= (@epcusto*@qtt)
				set @totalComIva	= @epcusto*@qtt*(@taxaIva+1)
			End
			Else
				if @tabiva=4
				Begin
					set @taxaIva		= (select convert(float,replace((select valor from para1 where descricao='ativa4'),',','.'),4)/100)
					set @valoriva4		= (@epcusto*@qtt*@taxaIva)
					set @totalSemIva4	= (@epcusto*@qtt)
					set @totalComIva	= @epcusto*@qtt*(@taxaIva+1)
				End
				Else
					if @tabiva=5
					Begin
						set @taxaIva		= (select convert(float,replace((select valor from para1 where descricao='ativa5'),',','.'),4)/100)
						set @valoriva5		= (@epcusto*@qtt*@taxaIva)
						set @totalSemIva5	= (@epcusto*@qtt)
						set @totalComIva	= @epcusto*@qtt*(@taxaIva+1)
					End
	
	
	/* ESCREVER CABE�ALHOS BO / BO2 */
	declare @bostamp varchar(25), @ndos numeric(5), @nmdos varchar(50), @nrdoc numeric(10), @nome varchar(60), @no numeric(8)
	set @ndos		= (select ndos from ts where ts.ndos=7)
	set @nmdos		= (select nmdos from ts where ts.ndos=7)
	set @nrdoc		= (select ISNULL(max(obrano),0) from bo where ndos=@ndos)+1
	set @bostamp	= 'ADM'+LEFT(newid(),22)
	set @nome		= (select nome from ag where codigo='Robo')
	set @no			= (select no from ag where codigo='Robo')
	
	Insert into bo (bostamp, ndos, nmdos, obrano, dataobra, 
					nome, no, estab, tipo,
					etotaldeb, dataopen, boano, ecusto, etotal, moeda, memissao, origem, site,
					vendedor, vendnm, obs,
					sqtt14, ebo_1tvall, ebo_2tvall, ebo_totp1, ebo_totp2,
					ebo11_bins, ebo12_bins, ebo21_bins, ebo22_bins, ebo31_bins, ebo32_bins, ebo41_bins, ebo42_bins, ebo51_bins, ebo52_bins,
					ebo11_iva, ebo12_iva, ebo21_iva, ebo22_iva, ebo31_iva, ebo32_iva, ebo41_iva, ebo42_iva, ebo51_iva, ebo52_iva,
					ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora)
	Values	(@bostamp, @ndos, @nmdos, @nrDoc, convert(varchar,GETDATE(),102),
			@nome, @no, 0, 'Robo',
			@etotaldeb, convert(varchar,GETDATE(),102), year(getdate()), @etotaldeb, @etotaldeb, 'PTE ou EURO', 'EURO', 'BO', @site,
			@vendedor, @vendnm, 'Trf. de Stock do Robo',
			@qtt, @etotaldeb, @etotaldeb, @etotaldeb, @etotaldeb,
			@totalSemIva1, @totalSemIva1, @totalSemIva2, @totalSemIva2, @totalSemIva3, @totalSemIva3, @totalSemIva4, @totalSemIva4, @totalSemIva5, @totalSemIva5,
			@valorIva1, @valorIva1, @valorIva2, @valorIva2, @valorIva3, @valorIva3, @valorIva4, @valorIva4, @valorIva5, @valorIva5,
			'ADM', convert(varchar,getdate(),102), convert(varchar,getdate(),8), 'ADM', convert(varchar,getdate(),102), convert(varchar,getdate(),8) )

	Insert into bo2 (bo2stamp, autotipo, pdtipo, etotalciva,
					ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora)
	Values (@bostamp, 1, 1, @totalComIva,
			'ADM', convert(varchar,getdate(),102), convert(varchar,getdate(),8), 'ADM', convert(varchar,getdate(),102), convert(varchar,getdate(),8) )
	

	/* Preencher Linhas BI / BI2 */
	declare @bistamp varchar(25), @codigo varchar(25), @design varchar(60), @qttAltern numeric(10), @stock numeric(13,2), @armazem numeric(5)
	declare @cpoc numeric(5), @familia numeric(5)
	set @bistamp		= 'ADM'+LEFT(newid(),22)
	set @codigo		= (select codigo from st (nolock) where ref=@ref)
	set @design		= (select design from st (nolock) where ref=@ref)
	set @qttAltern		= convert(int,(@qtt * (select isnull(conversao,0) from st where ref=@ref)))
	set @stock			= (select stock from st (nolock) where ref=@ref)
	set @armazem		= (select convert(int,numValue) from B_Parameters where stamp='ADM0000000039')
	set @cpoc			= (select cpoc from st where ref=@ref)
	set @familia		= (select familia from st where ref=@ref)

	Insert into bi (bistamp, bostamp, nmdos, obrano, ref, codigo, design, qtt, qtt2, uni2qtt,
					u_stockact, iva, tabiva, armazem, ar2mazem, stipo, ndos, cpoc, familia,
					no, nome,
					epu, edebito, eprorc, epcusto, ettdeb, ecustoind, edebitoori, u_upc,
					rdata, dataobra, dataopen, resfor, lordem,
					ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora)
	Values (@bistamp, @bostamp, @nmdos, @nrDoc, @ref, @codigo, @design, @qtt, 0, @qttAltern,
			@stock, @taxaIva, @tabiva, @armazem, @armazemDest, 4, @ndos, @cpoc, @familia,
			@no, @nome,
			@epcusto, @epcusto, @epcusto, @epcusto, @epcusto*@qtt, @epcusto, @epcusto, @epcusto,
			convert(varchar,getdate(),102), convert(varchar,getdate(),102), convert(varchar,getdate(),102), 1, 1000,
			'ADM', convert(varchar,getdate(),102), convert(varchar,getdate(),8), 'ADM', convert(varchar,getdate(),102), convert(varchar,getdate(),8) )
						
	/* Preencher Linhas da Bi2 */
	Insert into bi2 (bi2stamp, bostamp)
	Values (@bistamp, @bostamp)
	

GO
Grant Execute on dbo.up_robot_TrfEntreArmazens to Public
Grant Control on dbo.up_robot_TrfEntreArmazens to Public
Go