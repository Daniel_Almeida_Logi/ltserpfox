-- Procurar Recepções Robóticas Pendentes
-- exec up_robot_recPendentes '',0
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_robot_recPendentes]') IS NOT NULL
	drop procedure dbo.up_robot_recPendentes
go

create procedure dbo.up_robot_recPendentes
@id as VARCHAR(15) = '',
@pendente as int = 0

/* with encryption */
AS

--select distinct
--	ltrim(rtrim(Delivery_Number)) as nrConf,
--	convert(bit,0) as alterar,
--	convert(bit,0) as fechar
--from
--	B_movtec_robot mr (nolock)
--where 
--	isnull((select top 1 isnull(conferido,0)
--			from B_movtec_robot (nolock) mr2
--			where mr2.Delivery_Number=mr.Delivery_Number and conferido=1),0)=0
--	and mr.Delivery_Number!=''
--	and ltrim(rtrim(mr.Delivery_Number))!='interno'

/*

	If OBJECT_ID('tempdb.dbo.#mr') IS NOT NULL
		drop table #mr;		
	If OBJECT_ID('tempdb.dbo.#mr2_t') IS NOT NULL
		drop table #mr2_t;	

	with resultados as(
		select 
			conferido, cast(ltrim(rtrim(Delivery_Number)) as varchar(15)) as Delivery_Number
		from 
			B_movtec_robot (nolock)
		union 

		select   
			conferido, 
			cast(ltrim(rtrim(DeliveryNumber)) as varchar(15)) as Delivery_Number
		from 
			robot_response (nolock) 
		inner join  
			robot_response_payload(nolock) on  robot_response.stamp = robot_response_payload.responseStamp
		where 
			reqType='charge' 
			and state = 7 
			and conferido=0 
			and deliveryNumber not like 'int%' and deliveryNumber<>'' 
			and  PATINDEX('%[0-9]%',deliveryNumber) > 0 


		)
	select 
		* 
	into
		#mr
	from resultados

			
	select 
		distinct Delivery_Number
	into
		#mr2_t
	from 
		#mr (nolock)
	where conferido = 1
	


	select distinct
		ltrim(rtrim(mr.Delivery_Number)) as nrConf,
		convert(bit,0) as alterar,
		convert(bit,0) as fechar
	from
		#mr mr
	where 
		mr.Delivery_Number!=''
		and ltrim(rtrim(mr.Delivery_Number))!='interno'
		and mr.Delivery_Number not in (Select Delivery_Number from #mr2_t)

	If OBJECT_ID('tempdb.dbo.#mr') IS NOT NULL
		drop table #mr;		
	If OBJECT_ID('tempdb.dbo.#mr2_t') IS NOT NULL
		drop table #mr2_t;	

*/

	SELECT 
		*
		,convert(bit,0) as alterar
		,convert(bit,0) as fechar
		,(CASE WHEN conferido = 1 THEN 'Fechado' ELSE 'Por Conferir' END) as estado
		,(
			CASE 
				WHEN ori = 1 
					THEN (select top 1 CONVERT(VARCHAR,MAX(oData), 20) from B_movtec_robot(nolock) where ltrim(rtrim(Delivery_Number)) = nrConf AND LTRIM(RTRIM(product_Code)) <> '' )
				ELSE
					(select CONVERT(VARCHAR,MAX(ousrdata), 20) from robot_response_payload(nolock) where cast(ltrim(rtrim(DeliveryNumber)) as varchar(15)) = nrConf AND LTRIM(RTRIM(productCode)) <> '' and ousrdata>getdate()-15)
			END
		 ) as ultEntr
		,(
			CASE 
				WHEN ori = 1 
					THEN (select COUNT(DISTINCT LTRIM(RTRIM(Product_Code))) from B_movtec_robot(nolock) where cast(ltrim(rtrim(Delivery_Number)) as varchar(15)) = nrConf AND LTRIM(RTRIM(product_Code)) <> '' )
				ELSE
					(select COUNT(DISTINCT LTRIM(RTRIM(ProductCode))) from robot_response_payload(nolock) where cast(ltrim(rtrim(DeliveryNumber)) as varchar(15)) = nrConf AND LTRIM(RTRIM(productCode)) <> '' and ousrdata>getdate()-15) 
			END
		 ) as robot
		                                      
	FROM(	
		select 
			conferido
			,cast(ltrim(rtrim(Delivery_Number)) as varchar(15)) as nrConf
			,ISNULL((
					select 
						COUNT(DISTINCT fn.ref)
					from 
						fn(nolock) 
						join fo(nolock) on fo.fostamp = fn.fostamp 
					where 
						fo.adoc = cast(ltrim(rtrim(Delivery_Number)) as varchar(15))
						and fn.ref <> ''
						and fn.qtt = fn.qtrec
						and fn.ref in (select distinct x.Product_Code from B_movtec_robot(nolock) as x where cast(ltrim(rtrim(x.Delivery_Number)) as varchar(15)) = cast(ltrim(rtrim(B_movtec_robot.Delivery_Number)) as varchar(15)) )
			 ),0) as documento
			, 1 as ori
			
		from 
			B_movtec_robot (nolock)
		where
			conferido= (case when @pendente = 0 then 0 else conferido end)
			and Delivery_Number <> ''
			and Product_Code <> ''
			--and cast(ltrim(rtrim(Delivery_Number)) as varchar(15)) not in (select cast(ltrim(rtrim(DeliveryNumber)) as varchar(15)) from robot_response_payload(nolock))
		group by 
			delivery_Number, conferido

		union 

		select   
			conferido
			,cast(ltrim(rtrim(DeliveryNumber)) as varchar(15)) as nrConf
			,ISNULL((
					select 
						COUNT(DISTINCT fn.ref)
					from 
						fn(nolock) 
						join fo(nolock) on fo.fostamp = fn.fostamp 
					where 
						fo.adoc = cast(ltrim(rtrim(DeliveryNumber)) as varchar(15))
						and fn.ref <> ''
						and fn.qtt = fn.qtrec
						and fn.ref in (select distinct x.ProductCode from robot_response_payload(nolock) as x where cast(ltrim(rtrim(x.DeliveryNumber)) as varchar(15)) = cast(ltrim(rtrim(robot_response_payload.DeliveryNumber)) as varchar(15)) and x.ousrdata>getdate()-15 )
			 ),0) as documento
			, 2 as ori
			
		from 
			robot_response (nolock) 
		inner join  
			robot_response_payload(nolock) on  robot_response.stamp = robot_response_payload.responseStamp
		where 
			reqType='charge' 
			and state = 7 
			and ProductCode <> ''
			and conferido= (case when @pendente = 0 then 0 else conferido end)
			and deliveryNumber not like 'int%' and deliveryNumber<>'' 
			and  PATINDEX('%[0-9]%',deliveryNumber) > 0 
			and robot_response_payload.ousrdata>getdate()-15
			and robot_response.ousrdata>getdate()-15
		group by 
			deliveryNumber, conferido
	) AS X
	WHERE
		ltrim(rtrim(nrConf))!='interno'
		and nrConf = (CASE WHEN @id <> '' then @id ELSE nrConf END)
	
GO
Grant Execute on dbo.up_robot_recPendentes to Public
Grant Control on dbo.up_robot_recPendentes to Public
GO

