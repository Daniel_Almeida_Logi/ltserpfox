-- Apaga registos da tabela de registo das Comunicações com Robot
-- exec up_robot_delTable '099'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_robot_delTable]') IS NOT NULL
	drop procedure dbo.up_robot_delTable
go

create procedure dbo.up_robot_delTable

@posto varchar(8),
@msgId varchar(1) = ''

/* with encryption */
AS

delete
	B_movtec_robot
where
	posto!='str'
	and	convert(int,posto)=convert(int,@posto)
	and msg_id = case when @msgId='' then msg_id else @msgId end

GO
Grant Execute on dbo.up_robot_delTable to Public
Grant Control on dbo.up_robot_delTable to Public
Go