-- Procura Resposta do Robot � Mensagem 'S'
-- exec up_robot_respostaS 99
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_robot_respostaS]') IS NOT NULL
	drop procedure dbo.up_robot_respostaS
go

create procedure dbo.up_robot_respostaS

@posto varchar(8)

/* with encryption */
AS

	SELECT top 1 id, machine_state, timeExpired, posto
	FROM B_movtec_robot (nolock)
	where msg_id='s' and convert(varchar,oData,102)=convert(varchar,GETDATE(),102)
		and convert(int,posto)=convert(int,@posto)
	Order by id desc 
		

GO
Grant Execute on dbo.up_robot_respostaS to Public
Grant Control on dbo.up_robot_respostaS to Public
go