/*
	atualiza registo no mapa
	
	up_robot_consis_updateMap '8168617', 1, 174, 11, 1, 1, 174
	select * from robo_consis_map
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_robot_consis_updateMap]') IS NOT NULL
	drop procedure dbo.up_robot_consis_updateMap
go

create procedure dbo.up_robot_consis_updateMap
	@ref varchar(8)
	,@armario smallint
	,@canal smallint
	,@prateleira smallint
	,@stock tinyint
	,@armario_orig tinyint
	,@canal_orig smallint

/* with encryption */
AS

	update 
		robo_consis_map 
	SET 
		ref = @ref
		,Armario = @armario
		,Canal= @canal
		,Prateleira = @prateleira
		,stock = @stock
	Where
		Armario = @armario_orig
		and Canal= @canal_orig

GO
Grant Execute on dbo.up_robot_consis_updateMap to Public
Grant Control on dbo.up_robot_consis_updateMap to Public