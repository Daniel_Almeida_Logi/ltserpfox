/*
	insere registo no mapa
	
	up_robot_consis_insertMap '8168617', 1, 174, 11, 1
	select * from robo_consis_map
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_robot_consis_insertMap]') IS NOT NULL
	drop procedure dbo.up_robot_consis_insertMap
go

create procedure dbo.up_robot_consis_insertMap
	@ref varchar(8)
	,@armario smallint
	,@canal smallint
	,@prateleira tinyint
	,@stock tinyint

/* with encryption */
AS

	insert into robo_consis_map
	select
		@ref, @armario, @canal, @prateleira, @stock

GO
Grant Execute on dbo.up_robot_consis_insertMap to Public
Grant Control on dbo.up_robot_consis_insertMap to Public



