-- Procura Resposta do Robot � Mensagem 'K'
-- exec up_robot_respostaK '4', 1
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_robot_respostaK]') IS NOT NULL
	drop procedure dbo.up_robot_respostaK
go

create procedure dbo.up_robot_respostaK

@posto varchar(8),
@robo int = 0

/* with encryption */
AS

SELECT	mv.id,
			mv.Num_Foll,
			ostart_ref	= mv.Start_Product_Code,
			oref			= mv.product_code, /*serve de diagnostico para o robo tecnilab */
			start_ref		= case when @robo=0
									then (substring(ltrim(rtrim(mv.Start_Product_Code)),6,13))
									when @robo=1
									then Start_Product_Code
									else (substring(ltrim(rtrim(mv.Start_Product_Code)),6,13))
								end,
			ref				= case when @robo=0
									then ltrim(substring(ltrim(rtrim(mv.Product_Code)),6,13))
									when @robo=1
									then ltrim(mv.Product_Code)
									else ltrim(substring(ltrim(rtrim(mv.Product_Code)),6,13))
								end,
			mv.product_id,
			stock			= convert(int,mv.Stock),
			design		= ISNULL(st.design,''),
			codinter		= /*case when @robo=0*/ /*Condi��o tempor�ria por causa da tecnilab, � preciso rever isto.*/
								case when 1=0
									then ltrim(substring(ltrim(rtrim(mv.Product_Code)),0,6))
									else '03401' /*sempre o mesmo com este robo. N�o entra nas mensagens */
								end,
			mv.TimeExpired,
			mv.Posto,
			mv.oData,
			mv.conferido,
			mv.Order_State
FROM
	B_movtec_robot mv (nolock)
	left join st (nolock) on st.ref= case when @robo=0 then right(LTRIM(rtrim(mv.product_code)),7) else LTRIM(Product_Code) end 
where 
	mv.msg_id='k' and mv.conferido=0 and convert(int,Posto)=convert(int,@posto)
Order by
	id

GO
Grant Execute on dbo.up_robot_respostaK to Public
Grant Control on dbo.up_robot_respostaK to Public
GO