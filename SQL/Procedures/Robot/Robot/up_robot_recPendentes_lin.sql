-- Procurar Recep��es Rob�ticas Pendentes
-- exec up_robot_recPendentes_Lin '888'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_robot_recPendentes_Lin]') IS NOT NULL
	drop procedure dbo.up_robot_recPendentes_Lin
go

create procedure dbo.up_robot_recPendentes_Lin
@nrConf as VARCHAR(15)

/* with encryption */
AS

	SELECT 
		*
		,ISNULL((X.robot - X.documento),0) as diff
	FROM(
		SELECT
			LTRIM(RTRIM(product_Code)) as ref
			,(select top 1 design from st(nolock) where ref = LTRIM(RTRIM(product_code))) as design
			,SUM(CONVERT(NUMERIC(14,4), Quantity)) AS robot
			,ISNULL((select SUM(qtRec) from fn(nolock) join fo(nolock) on fo.fostamp = fn.fostamp where fo.adoc = cast(ltrim(rtrim(B_movtec_robot.Delivery_Number)) as varchar(15)) and ref = LTRIM(RTRIM(product_code))),0) as documento
		FROM
			B_movtec_robot(nolock)
		WHERE
			cast(ltrim(rtrim(Delivery_Number)) as varchar(15)) = @nrConf
			and Product_Code <> ''
			and order_state = '06'
		GROUP BY LTRIM(RTRIM(Product_Code)), cast(ltrim(rtrim(B_movtec_robot.Delivery_Number)) as varchar(15))

		UNION ALL

		SELECT
			LTRIM(RTRIM(productCode)) as ref
			,(select top 1 design from st(nolock) where ref = LTRIM(RTRIM(productcode))) as design
			,SUM(CONVERT(NUMERIC(14,4), Quantity)) AS robot
			,ISNULL((select SUM(qtRec) from fn(nolock) join fo(nolock) on fo.fostamp = fn.fostamp where fo.adoc = cast(ltrim(rtrim(DeliveryNumber)) as varchar(15)) and ref = LTRIM(RTRIM(productcode))),0) as documento
		FROM
			robot_response_payload
		WHERE
			cast(ltrim(rtrim(DeliveryNumber)) as varchar(15)) = @nrConf
			and productCode <> ''
		GROUP BY LTRIM(RTRIM(ProductCode)), cast(ltrim(rtrim(DeliveryNumber)) as varchar(15))
	) as X
		
	
GO
Grant Execute on dbo.up_robot_recPendentes_Lin to Public
Grant Control on dbo.up_robot_recPendentes_Lin to Public
GO

