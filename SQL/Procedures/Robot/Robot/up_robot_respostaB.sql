-- Procura Resposta do Robot � Mensagem 'B'
-- exec up_robot_respostaB '4', 1
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_robot_respostaB]') IS NOT NULL
	drop procedure dbo.up_robot_respostaB
go

create procedure dbo.up_robot_respostaB

@posto varchar(8),
@robot int = 0

/* with encryption */
AS

select
	top 1
	id
	,ref					= case when @robot=0 
									then substring(Product_Code,6,15)
									when @robot=1
									then Product_Code
									else substring(Product_Code,6,15)
								end					
	,ref2				= case when @robot=0 
									then Product_Code
									when @robot=1
									then product_id
									else Product_Code
								end
	,stock				= convert(int,stock)
	,storage_place
	,stock_at_place	= convert(int,stock_at_place)
from
	B_movtec_robot (nolock)
where
	msg_id='b'
	and convert(varchar,oData,102)=convert(varchar,GETDATE(),102)
	and convert(int,posto)=convert(int,@posto)
order by
	id desc

GO
Grant Execute on dbo.up_robot_respostaB to Public
Grant Control on dbo.up_robot_respostaB to Public
Go