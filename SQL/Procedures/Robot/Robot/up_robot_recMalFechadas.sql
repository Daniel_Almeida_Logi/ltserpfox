-- Procurar Recep��es Rob�ticas Mal Fechadas
-- exec up_robot_recMalFechadas ''
-- 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_robot_recMalFechadas]') IS NOT NULL
	drop procedure dbo.up_robot_recMalFechadas
go

create procedure dbo.up_robot_recMalFechadas

@nrRec varchar(25)

/* with encryption */
AS

If OBJECT_ID('tempdb.dbo.#mr2_t') IS NOT NULL
	drop table #mr2_t;	
		
with resultados as(
	select 
		distinct cast(ltrim(rtrim(Delivery_Number)) as varchar(15)) as Delivery_Number
	from 
		B_movtec_robot (nolock)
	where 
		conferido=1
	union 
	select 
		distinct cast(ltrim(rtrim(DeliveryNumber)) as varchar(15)) as Delivery_Number
	from 
		robot_response_payload
	where 
		deliveryNumber<>''
		and conferido=1
		and State= 7
	)
select 
	* 
into
	#mr2_t
from resultados



select distinct
	ltrim(rtrim(Delivery_Number)) as nrConf,
	convert(bit,0) as alterar
from
	B_movtec_robot mr (nolock)
where
	--isnull((select top 1 isnull(delivery_number, '') 
	--		from B_movtec_robot (nolock) mr2
	--		where mr2.Delivery_Number = mr.Delivery_Number and conferido = 1 and Num_Foll = ''),'')!=''
	mr.Delivery_Number in (Select Delivery_Number from #mr2_t)
	and mr.Delivery_Number!='' and ltrim(rtrim(mr.Delivery_Number))!='interno'
	and mr.Delivery_Number!=@nrRec
	and Order_State='06'
	and conferido = 1
union
	select distinct

		left(ltrim(rtrim(isnull(DeliveryNumber,''))),254) as nrConf,
		convert(bit,0) as alterar
	from
		robot_response_payload mr (nolock)
	where
		--isnull((select top 1 isnull(delivery_number, '') 
		--		from B_movtec_robot (nolock) mr2
		--		where mr2.Delivery_Number = mr.Delivery_Number and conferido = 1 and Num_Foll = ''),'')!=''
		mr.DeliveryNumber in (Select Delivery_Number from #mr2_t)
		and mr.DeliveryNumber!='' 
		and mr.DeliveryNumber!=@nrRec
		and State=7
		and conferido = 0
		and  PATINDEX('%[0-9]%',deliveryNumber) > 0
		and deliveryNumber not like 'int%' 

If OBJECT_ID('tempdb.dbo.#mr2_t') IS NOT NULL
	drop table #mr2_t;	

GO
Grant Execute on dbo.up_robot_recMalFechadas to Public
Grant Control on dbo.up_robot_recMalFechadas to Public
GO