-- Procura Resposta do Robot � Mensagem 'A'
-- exec up_robot_respostaA '4', 1, 1
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_robot_respostaA]') IS NOT NULL
	drop procedure dbo.up_robot_respostaA
go

create procedure dbo.up_robot_respostaA

@posto varchar(8),
@top int,
@robot int = 0

/* with encryption */
AS

	select
		top(@top)
		id
		,order_state
		,ref		= case when @robot=0 
							then substring(Product_Code,6,15)
							when @robot=1
							then ltrim(Product_Code)
							else substring(Product_Code,6,15)
						end
		,ref2		= case when @robot=0 
							then Product_Code
							when @robot=1
							then product_id
							else Product_Code
						end
		,stock	= convert(int,Quantity)
	from
		B_movtec_robot (nolock)
	where
		msg_id='a'
		and convert(varchar,oData,102)=convert(varchar,GETDATE(),102)
		and Order_State='04'
		and convert(int,posto)=convert(int,@posto)
	order by id desc
		
GO
Grant Execute on dbo.up_robot_respostaA to Public
Grant Control on dbo.up_robot_respostaA to Public
go