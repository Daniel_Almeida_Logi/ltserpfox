/*
	Procura Resposta do Robot
	
	exec up_robot_consis_getMap '',0,0,0
	select * from robo_consis_map
	
	exec up_robot_consis_getMap '8372532',7,0,816861

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_robot_consis_getMap]') IS NOT NULL
	drop procedure dbo.up_robot_consis_getMap
go

create procedure dbo.up_robot_consis_getMap
@ref varchar(8)
,@armario int
,@canal int
,@prateleira int
,@site_nr tinyint =1


/* with encryption */
AS

	select 
		robo_consis_map.ref, armario, prateleira, canal, robo_consis_map.stock, st.design
	from 
		robo_consis_map (nolock)
		left join st on robo_consis_map.ref = st.ref and st.site_nr = @site_nr		
	where
		robo_consis_map.ref = case when @ref = '' then robo_consis_map.ref else @ref end
		and armario = case when @armario = 0 then armario else @armario end
		and canal = case when @canal = 0 then canal else @canal end
		and prateleira = case when @prateleira = 0 then prateleira else @prateleira end
	ORDER BY 
		armario, canal, prateleira, ref

GO
Grant Execute on dbo.up_robot_consis_getMap to Public
Grant Control on dbo.up_robot_consis_getMap to Public