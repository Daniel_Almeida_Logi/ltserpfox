/*
	Atualiza stock do robo
	
	up_robot_consis_updateStock 01, 0173, 5
	select * from robo_consis_map
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_robot_consis_updateStock]') IS NOT NULL
	drop procedure dbo.up_robot_consis_updateStock
go

create procedure dbo.up_robot_consis_updateStock
	@armario smallint
	,@canal int
	,@qttDispensa int

/* with encryption */
AS

	update
		robo_consis_map
	set
		stock = case 
					when stock + @qttDispensa < 0 then 0
					else stock + @qttDispensa
					end
	where
		Armario = @armario
		and Canal = @canal

GO
Grant Execute on dbo.up_robot_consis_updateStock to Public
Grant Control on dbo.up_robot_consis_updateStock to Public