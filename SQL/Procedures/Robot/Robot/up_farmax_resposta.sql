-- Procura Resposta do Robot
-- up_farmax_resposta '1', 25
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_farmax_resposta]') IS NOT NULL
	drop procedure dbo.up_farmax_resposta
go

create procedure dbo.up_farmax_resposta

@posto varchar(8),
@top int

/* with encryption */
AS

	select 
		top(@top) id,
		ref		= ltrim(rtrim(Product_Code)),
		stock	= convert(int,Quantity)
	from 
		B_movtec_robot (nolock)
	where 
		convert(varchar,oData,102)=convert(varchar,GETDATE(),102)
		and posto=@posto
		and msg_id=''
	order by 
		id desc

GO
Grant Execute on dbo.up_farmax_resposta to Public
Grant Control on dbo.up_farmax_resposta to Public