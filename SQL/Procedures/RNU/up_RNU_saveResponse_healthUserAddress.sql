/* save response RNU

	exec up_RNU_saveResponse_healthUserAddress ''

	
*/

if OBJECT_ID('[dbo].[up_RNU_saveResponse_healthUserAddress]') IS NOT NULL
	drop procedure dbo.up_RNU_saveResponse_healthUserAddress
go

create procedure [dbo].up_RNU_saveResponse_healthUserAddress
		 @addressStamp					AS varchar(36)
		,@NumberProcessAddress 			AS varchar(100)
		,@RoadType 						AS varchar(150)
		,@RoadTypeDesc 					AS varchar(100)
		,@Road							AS varchar(254)
		,@TypeBuilding 					AS varchar(100)
		,@TypeBuildingDesc 				AS varchar(254)
		,@Door 							AS varchar(30)
		,@TypeSide						AS varchar(100)
		,@TypeSideDesc 					AS varchar(254)
		,@Floor 						AS varchar(30)
		,@Place 						AS varchar(70)
		,@Location 						AS varchar(100)
		,@dResidence					AS varchar(100)
		,@dResidenceDesc 				AS varchar(254)
		,@cResidence 					AS varchar(100)
		,@cResidenceDesc 				AS varchar(254)
		,@ParishResidence 				AS varchar(100)
		,@ParishResidenceDesc			AS varchar(254)
		,@PostalCode 					AS varchar(50)
		,@SequencePostcard 				AS varchar(50)
		,@PostalLocation 				AS varchar(100)
		,@ForeignAddress 				AS varchar(254)
		,@ForeignRegion					AS varchar(100)
		,@ForeignCity 					AS varchar(100)
		,@ForeignLocation 				AS varchar(100)
		,@ForeignPostalCode 			AS varchar(50)
		,@ForeignCountry 				AS varchar(100)
		,@foreignCountryDesc			AS varchar(254)


/* with encryption */

AS
SET NOCOUNT ON

DECLARE @user VARCHAR(20)
DECLARE @dateT DATETIME

	SET @user = 'ADM'
	set @dateT= GETDATE()
	
	IF(NOT EXISTS(SELECT * FROM ext_RNU_RespHealthU_Address  WHERE stamp=@addressStamp))
	BEGIN
		INSERT INTO ext_RNU_RespHealthU_Address(stamp ,nProcessAddress ,RoadType ,RoadTypeDesc ,Road,TypeBuilding ,TypeBuildingDesc ,Door ,TypeSide,TypeSideDesc ,Floor ,Place ,Location ,dResidence,dResidenceDesc 
			,cResidence ,cResidenceDesc ,ParishResidence ,ParishResidenceDesc,PostalCode ,SequencePostcard ,PostalLocation,ForeignAddress ,ForeignRegion,ForeignCity ,foreignLocation,ForeignPostalCode ,ForeignCountry,foreignCountryDesc,ousrdata,ousrinis,usrdata,usrinis )
		VALUES (@addressStamp ,@NumberProcessAddress ,@RoadType ,@RoadTypeDesc ,@Road,@TypeBuilding ,@TypeBuildingDesc ,@Door ,@TypeSide,@TypeSideDesc ,@Floor ,@Place ,@Location ,@dResidence,@dResidenceDesc 
				,@cResidence ,@cResidenceDesc ,@ParishResidence ,@ParishResidenceDesc,@PostalCode ,@SequencePostcard ,@PostalLocation,@ForeignAddress ,@ForeignRegion,@ForeignCity ,@foreignLocation,@ForeignPostalCode ,@ForeignCountry,@foreignCountryDesc,@dateT,@user,@dateT,@user)
		
	END 
	ELSE 
	BEGIN 

		UPDATE ext_RNU_RespHealthU_Address
		SET
			 stamp						=	@addressStamp
			,nProcessAddress 			=	@NumberProcessAddress
			,RoadType 					=	@RoadType
			,RoadTypeDesc 				=	@RoadTypeDesc
			,Road						=	@Road
			,TypeBuilding 				=	@TypeBuilding
			,TypeBuildingDesc 			=	@TypeBuildingDesc
			,Door 						=	@Door
			,TypeSide					=	@TypeSide
			,TypeSideDesc 				=	@TypeSideDesc
			,Floor 						=	@Floor
			,Place 						=	@Place
			,Location 					=	@Location
			,dResidence					=	@dResidence
			,dResidenceDesc 			=	@dResidenceDesc
			,cResidence 				=	@cResidence
			,cResidenceDesc 			=	@cResidenceDesc
			,ParishResidence 			=	@ParishResidence
			,ParishResidenceDesc		=	@ParishResidenceDesc
			,PostalCode 				=	@PostalCode
			,SequencePostcard 			=	@SequencePostcard
			,PostalLocation 			=	@PostalLocation
			,ForeignAddress 			=	@ForeignAddress
			,ForeignRegion				=	@ForeignRegion
			,ForeignCity 				=	@ForeignCity
			,ForeignLocation 			=	@ForeignLocation
			,ForeignPostalCode 		    =	@ForeignPostalCode
			,ForeignCountry 			=	@ForeignCountry
			,foreignCountryDesc		    =	@foreignCountryDesc
			, usrdata				    =   @dateT
			,usrinis				    =   @user
		where stamp = @addressStamp

	END 


GO
Grant Execute on dbo.up_RNU_saveResponse_healthUserAddress to Public
Grant control on dbo.up_RNU_saveResponse_healthUserAddress to Public
GO