/* save response RNU

	exec up_RNU_saveResponse_healthUserRespEnts ''

	
*/

if OBJECT_ID('[dbo].[up_RNU_saveResponse_healthUserRespEnts]') IS NOT NULL
	drop procedure dbo.up_RNU_saveResponse_healthUserRespEnts
go

create procedure [dbo].up_RNU_saveResponse_healthUserRespEnts
		 @stamp				AS varchar(36)
		,@token 			AS varchar(36)
		,@codeEnt			AS varchar(100)
		,@codeEntDesc		AS varchar(254)
		,@nBenef			AS varchar(100)
		,@typecard			AS varchar(100)
		,@typecardDesc		AS varchar(254)
		,@numbercard		AS varchar(100)
		,@country			AS varchar(100)
		,@countryDesc		AS varchar(254)
		,@dateValidCard		AS datetime
		,@startDate			AS datetime
		,@enddate			AS datetime
		,@issuanceDate		AS datetime
		,@changeDate		AS datetime
/* with encryption */

AS
SET NOCOUNT ON

DECLARE @user VARCHAR(20)
DECLARE @dateT DATETIME

	SET @user = 'ADM'
	set @dateT= GETDATE()
	
	IF(NOT EXISTS(SELECT * FROM ext_RNU_RespHealthU_RespEnts  WHERE stamp=@stamp))
	BEGIN
		INSERT INTO ext_RNU_RespHealthU_RespEnts(stamp ,token ,codeEnt,codeEntDesc,nBenef,typecard,typecardDesc,numbercard,country,countryDesc,validCardDate,begindate,enddate,issuanceDate,changeDate,ousrdata,ousrinis,usrdata,usrinis)
		VALUES (@stamp ,@token ,@codeEnt,@codeEntDesc,@nBenef,@typecard,@typecardDesc,@numbercard,@country,@countryDesc,@dateValidCard,@startDate,@enddate,@issuanceDate,@changeDate,@dateT,@user,@dateT,@user)
		
	END 
	ELSE 
	BEGIN 

		UPDATE ext_RNU_RespHealthU_RespEnts
		SET										 	
				 stamp			= @stamp			
				,token 			= @token 			
				,codeEnt		= @codeEnt			
				,codeEntDesc	= @codeEntDesc		
				,nBenef			= @nBenef			
				,typecard		= @typecard		
				,typecardDesc	= @typecardDesc	
				,numbercard		= @numbercard		
				,country		= @country			
				,countryDesc	= @countryDesc		
				,validCardDate	= @dateValidCard	
				,begindate		= @startDate		
				,enddate		= @enddate			
				,issuanceDate	= @issuanceDate	
				,changeDate		= @changeDate
				,usrinis		= @dateT
				,usrdata		= @user	
		where stamp = @stamp
	END 


GO
Grant Execute on dbo.up_RNU_saveResponse_healthUserRespEnts to Public
Grant control on dbo.up_RNU_saveResponse_healthUserRespEnts to Public
GO