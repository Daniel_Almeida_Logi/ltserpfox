/* retorna a informacao a lista de todos o token 

	exec up_get_RNURequest_to '3E6687A5-3C4A-40A3-9399-6D4334F0ECC1'
	exec up_get_RNURequest_to 
*/
	

if OBJECT_ID('[dbo].[up_get_RNURequest_to]') IS NOT NULL
	drop procedure dbo.up_get_RNURequest_to
go

create procedure dbo.up_get_RNURequest_to
	@toToken			 varchar(36)

/* WITH ENCRYPTION */ 
AS
BEGIN


	SELECT 
		 stamp									AS stamp 
		,toToken								AS toToken
		,entityName								AS entityName
		,keyEntityTypeId						AS keyEntityTypeId
		,genericCodeType						AS genericCodeType
		,genericCodeTypeDesign					AS genericCodeTypeDesign
		,CodeCipher								AS CodeCipher
		,CodeDesignCipher						AS CodeDesignCipher
	FROM 
		ext_RNU_Request_to (nolock)
	WHERE toToken= @toToken

END
GO
Grant Execute on dbo.up_get_RNURequest_to to Public
Grant control on dbo.up_get_RNURequest_to to Public
GO