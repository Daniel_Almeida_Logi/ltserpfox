/* retorna a informacao de request RNU

	exec up_get_RNURequest '04B2BD7A-C166-41BB-A85D-D18D79D3A541'
	exec up_get_RNURequest 
*/
	

if OBJECT_ID('[dbo].[up_get_RNURequest]') IS NOT NULL
	drop procedure dbo.up_get_RNURequest
go

create procedure dbo.up_get_RNURequest
	@token			 varchar(36)

/* WITH ENCRYPTION */ 
AS
BEGIN


	SELECT 
		 token																		AS token
		,site																		AS site
		,searchDetail															    AS searchDetail
		,snsNumber																    AS sns
		,nicsNumber																	AS nics
		,operationalNumber														    AS operationalN
		,healthCenter																AS healthCenter
		,fullName																    AS fullName
		,ownNames																	AS ownNames
		,surname																	AS surname
		,countryNat																	AS countryNat
		,disNat																		AS disNat
		,cNat																		AS cNat
		,parishNat																	AS parishNat
		,countryNac																	AS countryNac
		,sex																		AS sex
		,birthDate																	AS birthDate
		,typeDocIdent																AS typeDocIdent
		,nDocIdent																	AS nDocIdent
		,CASE WHEN  ISNULL(nif,'')='' THEN 0 ELSE nif END							AS nif
		,niss																		AS niss
		,ageof																		AS ageof
		,ageA																		AS ageA
		,historicFlag																AS historicFlag
		,historicBeginDate															AS historicBeginDate
		,historicEndDate															AS historicEndDate
		,respEntityCode																AS respEntityCode
		,respNBen																	AS respNBen
		,respTypeCard																AS respTypeCard
		,respNCard																	AS respNCard
		,respCountryCode															AS respCountryCode
		,toToken																	AS toToken
		,entityNameFrom																AS entityNameFrom
		,keyfromEntId																AS keyfromEntId
		,cipherCode																	AS cipherCode
		,cipherDesign																AS cipherDesign
		,genericCodeTypeFrom														AS genericCodeTypeFrom
		,genericCodeTypeDesignFrom													AS genericCodeTypeDesignFrom
		,princ																		AS princ
		,genericType																AS genericType
		,genericTypeDesign															AS genericTypeDesign
		,genericTypeOpe																AS genericTypeOpe
		,genericTypeDesignOpe														AS genericTypeDesignOpe
		,sentOn																		AS sentOn
		,activatedOn																AS dateActivatedOn
		,keyorder																	AS keyorder
		,keyRelatedOrder															AS keyRelatedOrder
		,test																		AS test
		,(SELECT top 1 assfarm from EMPRESA where site=ext_RNU_Request.site )		AS assocCode
	FROM 
		ext_RNU_Request (nolock)
	WHERE token= @token

END
GO
Grant Execute on dbo.up_get_RNURequest to Public
Grant control on dbo.up_get_RNURequest to Public
GO