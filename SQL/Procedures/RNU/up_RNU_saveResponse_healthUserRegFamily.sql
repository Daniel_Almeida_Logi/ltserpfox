/* save response RNU

	exec up_RNU_saveResponse_healthUserRegProfissional ''

	
*/

if OBJECT_ID('[dbo].[up_RNU_saveResponse_healthUserRegFamily]') IS NOT NULL
	drop procedure dbo.up_RNU_saveResponse_healthUserRegFamily
go

create procedure [dbo].up_RNU_saveResponse_healthUserRegFamily
			  @stamp			   AS varchar(36)
			 ,@Token 			   AS varchar(36)
			 ,@ProcessNumber 	   AS varchar(100)
			 ,@HealthCenter		   AS varchar(100)
			 ,@HealthCenterDesc    AS varchar(254)
			 ,@Telephone 		   AS varchar(30)
			 ,@AddressFamily 	   AS varchar(100)
			 ,@BeginDate 		   AS datetime
			 ,@EndDate 			   AS datetime
			 ,@elementsToken		   AS varchar(36)


/* with encryption */

AS
SET NOCOUNT ON

DECLARE @user VARCHAR(20)
DECLARE @dateT DATETIME

	SET @user = 'ADM'
	set @dateT= GETDATE()
	
	IF(NOT EXISTS(SELECT * FROM ext_RNU_RespHealthU_RegF  WHERE stamp=@stamp))
	BEGIN
		INSERT INTO ext_RNU_RespHealthU_RegF( Token ,ProcessNumber ,HealthCenter,HealthCenterDesc,Telephone,familyAddress ,BeginDate,EndDate,elementsToken,ousrdata,ousrinis,usrdata,usrinis)
		VALUES ( @Token ,@ProcessNumber,@HealthCenter,@HealthCenterDesc,@Telephone,@AddressFamily,@BeginDate,@EndDate,@elementsToken,@dateT,@user,@dateT,@user	)
		
	END 
	ELSE 
	BEGIN 

		UPDATE ext_RNU_RespHealthU_RegF
		SET								 
			Token 			 =  @Token 			 
			,ProcessNumber 	 =	@ProcessNumber 	 
			,HealthCenter	 =	@HealthCenter		 
			,HealthCenterDesc =	@HealthCenterDesc  
			,Telephone 		 =	@Telephone 		 
			,familyAddress 	 =	@AddressFamily 	 
			,BeginDate 		 =	@BeginDate 		 
			,EndDate 		 =	@EndDate 			 
			,elementsToken	 =	@elementsToken	
			,usrdata		 = @dateT
			,usrinis		 = @user			
		where stamp = @stamp
	END 


GO
Grant Execute on dbo.up_RNU_saveResponse_healthUserRegFamily to Public
Grant control on dbo.up_RNU_saveResponse_healthUserRegFamily to Public
GO