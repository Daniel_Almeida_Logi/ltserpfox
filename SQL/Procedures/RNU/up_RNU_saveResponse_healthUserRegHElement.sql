/* save response RNU

	exec up_RNU_saveResponse_healthUserRegHElement ''

	
*/

if OBJECT_ID('[dbo].[up_RNU_saveResponse_healthUserRegHElement]') IS NOT NULL
	drop procedure dbo.up_RNU_saveResponse_healthUserRegHElement
go

create procedure [dbo].up_RNU_saveResponse_healthUserRegHElement
			  @stamp				as varchar(36)
			 ,@token 				as varchar(36)
			 ,@Operationalnumber	as numeric(20,0)
			 ,@Snsnumber			as numeric(23,0)
			 ,@Name					as varchar(150)
			 ,@BirthDate			as datetime
			 ,@Degreekinship		as varchar(50)
			 ,@DegreekinshipDesc	as varchar(254)
			 ,@BeginDate			as datetime
			 ,@EndDate				as datetime
			 ,@doctorToken 			as varchar(36)
			 ,@nurseToken			as varchar(36)

/* with encryption */

AS
SET NOCOUNT ON

DECLARE @user VARCHAR(20)
DECLARE @dateT DATETIME

	SET @user = 'ADM'
	set @dateT= GETDATE()
	
	IF(NOT EXISTS(SELECT * FROM ext_RNU_RespHealthU_RegFElem  WHERE stamp=@stamp))
	BEGIN
		INSERT INTO ext_RNU_RespHealthU_RegFElem(stamp,elementsToken,operationalnumber,sns,name,birthDate,degreekinship,degreekinshipDesc,beginDate,endDate,doctorToken,nurseToken,ousrdata,ousrinis,usrdata,usrinis)
		VALUES (  @stamp ,@token ,@Operationalnumber,@Snsnumber,@Name,@BirthDate,@Degreekinship,@DegreekinshipDesc,@BeginDate,@EndDate,@doctorToken ,@nurseToken,@dateT,@user,@dateT,@user	)
		
	END 
	ELSE 
	BEGIN 

		UPDATE ext_RNU_RespHealthU_RegFElem
		SET										 
			 elementsToken			=@token 				
			,operationalnumber		=@Operationalnumber	
			,sns					=@Snsnumber			
			,name					=@Name					
			,birthDate				=@BirthDate			
			,degreekinship			=@Degreekinship		
			,degreekinshipDesc		=@DegreekinshipDesc	
			,beginDate				=@BeginDate			
			,endDate				=@EndDate				
			,doctorToken			=@doctorToken 			
			,nurseToken				=@nurseToken			
			,usrdata				=@dateT
			,usrinis		 	    =@user	
		where stamp = @stamp
	END 


GO
Grant Execute on dbo.up_RNU_saveResponse_healthUserRegHElement to Public
Grant control on dbo.up_RNU_saveResponse_healthUserRegHElement to Public
GO