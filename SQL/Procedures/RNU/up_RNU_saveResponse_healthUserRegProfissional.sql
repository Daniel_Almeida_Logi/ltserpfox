/* save response RNU

	exec up_RNU_saveResponse_healthUserRegProfissional ''

	
*/

if OBJECT_ID('[dbo].[up_RNU_saveResponse_healthUserRegProfissional]') IS NOT NULL
	drop procedure dbo.up_RNU_saveResponse_healthUserRegProfissional
go

create procedure [dbo].up_RNU_saveResponse_healthUserRegProfissional
			  @stamp 			  AS varchar(36)
			 ,@Token 			  AS varchar(36)
			 ,@OrderNumber		  AS varchar(100)
			 ,@Name      		  AS varchar(150)
			 ,@BeginDate		  AS datetime
			 ,@EndDate			  AS datetime
			 ,@type 			  AS varchar(100)
			 ,@typeDesc 		  AS varchar(150)


/* with encryption */

AS
SET NOCOUNT ON

DECLARE @user VARCHAR(20)
DECLARE @dateT DATETIME

	SET @user = 'ADM'
	set @dateT= GETDATE()
	
	IF(NOT EXISTS(SELECT * FROM ext_RNU_RespHealthU_RegProf  WHERE stamp=@stamp))
	BEGIN
		INSERT INTO ext_RNU_RespHealthU_RegProf( stamp ,token ,OrderNumber,Name,BeginDate,EndDate,type ,typeDesc,ousrdata,ousrinis,usrdata,usrinis)
		VALUES ( @stamp ,@Token ,@OrderNumber,@Name,@BeginDate,@EndDate,@type ,@typeDesc,@dateT,@user,@dateT,@user )
		
	END 
	ELSE 
	BEGIN 

		UPDATE ext_RNU_RespHealthU_RegProf
		SET
			 token			= 	@Token
			,OrderNumber	= 	@OrderNumber	
			,Name			= 	@Name      	
			,BeginDate		= 	@BeginDate	
			,EndDate		= 	@EndDate		
			,type 			= 	@type 		
			,typeDesc 		=   @typeDesc 	
			,usrdata		= @dateT
			,usrinis		= @user	
		where stamp = @stamp
	END 


GO
Grant Execute on dbo.up_RNU_saveResponse_healthUserRegProfissional to Public
Grant control on dbo.up_RNU_saveResponse_healthUserRegProfissional to Public
GO