/* save response RNU

	exec up_RNU_saveResponse_healthUserContact ''

	
*/

if OBJECT_ID('[dbo].[up_RNU_saveResponse_healthUserRegistration]') IS NOT NULL
	drop procedure dbo.up_RNU_saveResponse_healthUserRegistration
go

create procedure [dbo].up_RNU_saveResponse_healthUserRegistration
		 @registrationStamp			AS varchar(36)	
		,@registrationToken 		AS varchar(36)
		,@DateRegistration			AS datetime
		,@BeginDate 				AS datetime
		,@EndDate					AS datetime
		,@TypeRegistration			AS varchar(50)
		,@TypeRegistrationDesc		AS varchar(254)
		,@HealthUserType 			AS varchar(50)
		,@HealthUserTypeDesc		AS varchar(254)
		,@HealthCenter 				AS varchar(100)
		,@HealthCenterDesc			AS varchar(254)
		,@RegistrationState			AS varchar(100)
		,@ReasonClose				AS varchar(100)
		,@ReasonCloseDesc			AS varchar(254)
		,@Deadline 					AS datetime
		,@ChangeDate 				AS datetime
		,@familiesToken 			AS varchar(36)
		,@doctorsToken 				AS varchar(36)
		,@nursesToken				AS varchar(36)


/* with encryption */

AS
SET NOCOUNT ON

DECLARE @user VARCHAR(20)
DECLARE @dateT DATETIME

	SET @user = 'ADM'
	set @dateT= GETDATE()
	
	IF(NOT EXISTS(SELECT * FROM ext_RNU_RespHealthU_Reg  WHERE stamp=@registrationStamp))
	BEGIN
		INSERT INTO ext_RNU_RespHealthU_Reg(stamp ,token ,registrationDate,BeginDate ,EndDate,registrationType,registrationTypeDesc,HealthUserType ,HealthUserTypeDesc,HealthCenter 
									,HealthCenterDesc,state,ReasonClose,ReasonCloseDesc ,Deadline ,ChangeDate ,familiesToken ,doctorsToken ,nursesToken,ousrdata,ousrinis,usrdata,usrinis )
		VALUES (@registrationStamp ,@registrationToken ,@DateRegistration,@BeginDate ,@EndDate,@TypeRegistration,@TypeRegistrationDesc,@HealthUserType ,@HealthUserTypeDesc,@HealthCenter 
									,@HealthCenterDesc,@RegistrationState,@ReasonClose,@ReasonCloseDesc,@Deadline ,@ChangeDate ,@familiesToken ,@doctorsToken ,@nursesToken,@dateT,@user,@dateT,@user)
		
	END 
	ELSE 
	BEGIN 

		UPDATE ext_RNU_RespHealthU_Reg
		SET
			stamp				   =@registrationStamp     
			,token 				   =@registrationToken 	  
			,registrationDate	   =@DateRegistration	  
			,BeginDate 			   =@BeginDate 			  
			,EndDate			   =@EndDate			  
			,registrationType	   =@TypeRegistration	  
			,registrationTypeDesc  =@TypeRegistrationDesc 
			,HealthUserType 	   =@HealthUserType 	  
			,HealthUserTypeDesc	   =@HealthUserTypeDesc	  
			,HealthCenter 		   =@HealthCenter 		  
			,HealthCenterDesc	   =@HealthCenterDesc	  
			,state				   =@RegistrationState	  
			,ReasonClose		   =@ReasonClose		  
			,ReasonCloseDesc	   =@ReasonCloseDesc	  
			,Deadline 			   =@Deadline 			  
			,ChangeDate 		   =@ChangeDate 		  
			,familiesToken 		   =@familiesToken 		  
			,doctorsToken 		   =@doctorsToken 		  
			,nursesToken 		   =@nursesToken 		  
			,usrdata			   = @dateT
			,usrinis			   = @user	
		where stamp = @registrationStamp
	END 


GO
Grant Execute on dbo.up_RNU_saveResponse_healthUserRegistration to Public
Grant control on dbo.up_RNU_saveResponse_healthUserRegistration to Public
GO