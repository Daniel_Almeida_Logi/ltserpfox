/* save response RNU
exec up_RNU_saveResponse_To '22058309-3d86-4323-84ce-b655d80b953','0049f84d-f305-46a8-b674-e87171ce56d','','','','','',''

	
*/

if OBJECT_ID('[dbo].[up_RNU_saveResponse_To]') IS NOT NULL
	drop procedure dbo.up_RNU_saveResponse_To
go

create procedure [dbo].up_RNU_saveResponse_To
		 @stamp					AS varchar(36)
		,@tokenToResp			AS VARCHAR(36)
		,@name 					AS VARCHAR(150)
		,@fromkeyid 			AS VARCHAR(150)
		,@fromkeyCipherCode 	AS VARCHAR(100)
		,@fromkeyCipherDesign 	AS VARCHAR(254)
		,@typeCode				AS VARCHAR(100)
		,@typeCodeDesign		AS VARCHAR(254)

/* with encryption */

AS
SET NOCOUNT ON

DECLARE @user VARCHAR(20)
DECLARE @dateT DATETIME

	SET @user = 'ADM'
	set @dateT= GETDATE()
	
	IF(NOT EXISTS(SELECT * FROM ext_RNU_Resp_To  WHERE stamp=@stamp))
	BEGIN
		INSERT INTO ext_RNU_Resp_To(stamp,tokenToResp,name,keyId,cipherCode,cipherDesign,typeCode,typeDesign,ousrdata,ousrinis,usrdata,usrinis)
		VALUES(@stamp,@tokenToResp,@name,@fromkeyid ,@fromkeyCipherCode ,@fromkeyCipherDesign,@typeCode,@typeCodeDesign ,@dateT,@user ,@dateT,@user)
	END 
	ELSE 
	BEGIN 

		UPDATE ext_RNU_Resp_To
		SET
				tokenToResp		= @tokenToResp
				,name			= @name
				,keyId			= @fromkeyid
				,cipherCode		= @fromkeyCipherCode
				,cipherDesign	= @fromkeyCipherDesign
				,typeCode		= @typeCode
				,typeDesign		= @typeCodeDesign
				,usrdata		= @dateT
				,usrinis		= @user
		where tokenToResp = @tokenToResp

	END 




GO
Grant Execute on dbo.up_RNU_saveResponse_To to Public
Grant control on dbo.up_RNU_saveResponse_To to Public
GO