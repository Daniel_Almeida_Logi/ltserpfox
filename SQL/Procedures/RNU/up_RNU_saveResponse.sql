/* get request RNU

exec up_RNU_saveResponse '04B2BD7A-C166-41BB-A85D-D18D79D3A541',0,'','N','','','','','','','','','','','','2021-09-21 11:06:55','2021-09-21 11:06:55','WC12-3_00-589420d9-d7fd-4286-9b91-17032563206d','','9d1c7828-bf98-43a1-a384-7b6fcd36855'
exec up_RNU_saveResponse '04B2BD7A-C166-41BB-A85D-D18D79D3A541',0,'','N','','','','','','','','','','','','2021/09/21 12:17:11 PM','2021/09/21 12:17:11 PM','WC12-3_00-b7f45b6d-ac81-48b0-8236-aee612e341e1','','3c7a7211-f639-4823-9fab-743bf1b4654'

*/

if OBJECT_ID('[dbo].[up_RNU_saveResponse]') IS NOT NULL
	drop procedure dbo.up_RNU_saveResponse
go

create procedure [dbo].up_RNU_saveResponse
		 @token					AS varchar(36)
		,@codeMessage			AS numeric(13,0)
		,@descriptionMessage	AS VARCHAR(254)
		,@moreRecords 			AS VARCHAR(254)
		,@name 					AS VARCHAR(150)
		,@fromtypeCode 			AS VARCHAR(100)
		,@fromtypeDesign 		AS VARCHAR(254)
		,@fromkeyid 			AS VARCHAR(100)
		,@fromkeyCipherCode 	AS VARCHAR(100)
		,@fromkeyCipherDesign 	AS VARCHAR(254)
		,@princ					AS VARCHAR(100)
		,@typeCode				AS VARCHAR(100)
		,@TypeDesign 			AS VARCHAR(254)
		,@OperationCode			AS VARCHAR(100)
		,@OperationDesign 		AS VARCHAR(254)
		,@SentOn				AS DATETIME
		,@ActivatedOn			AS DATETIME
		,@Keyorder				AS VARCHAR(254)
		,@KeyRelatedOrder		AS VARCHAR(254)
		,@tokenToResp			AS VARCHAR(36)


/* with encryption */

AS
SET NOCOUNT ON

DECLARE @user VARCHAR(20)
DECLARE @dateT DATETIME

	SET @user = 'ADM'
	set @dateT= GETDATE()
	
	IF(NOT EXISTS(SELECT * FROM ext_RNU_Resp WHERE token=@token))
	BEGIN
		INSERT INTO ext_RNU_Resp (token,codeMessage,descMessage,moreRecords,fromName,fromTypeCode,fromTypeDesign,fromKeyid,fromKeyCipherCode,fromKeyCipherDesign,princ,typeCode,typeDesign,operationCode, operationDesign,sentOn,activatedOn,keyorder,keyRelatedOrder,tokenToResp,ousrdata,ousrinis,usrdata,usrinis)
		VALUES(@token,@codeMessage,@descriptionMessage,@moreRecords ,@name ,@fromtypeCode ,@fromtypeDesign ,@fromkeyid ,@fromkeyCipherCode ,@fromkeyCipherDesign ,@princ,@typeCode,@TypeDesign ,@OperationCode,@OperationDesign ,@SentOn,@ActivatedOn,@Keyorder,@KeyRelatedOrder,@tokenToResp,@dateT,@user ,@dateT,@user)
	END 
	ELSE 
	BEGIN 

		UPDATE ext_RNU_Resp
		SET
			 codeMessage			= @codeMessage
			,descMessage			= @descriptionMessage
			,moreRecords			= @moreRecords
			,fromName				= @name
			,fromTypeCode			= @fromtypeCode
			,fromTypeDesign			= @fromtypeDesign	
			,fromKeyid				= @fromkeyid
			,fromKeyCipherCode		= @fromkeyCipherCode
			,fromKeyCipherDesign	= @fromkeyCipherDesign
			,princ					= @princ
			,typeCode				= @typeCode
			,typeDesign				= @TypeDesign
			,operationCode			= @OperationCode
			,operationDesign		= @operationDesign
			,sentOn					= @SentOn
			,activatedOn			= @ActivatedOn
			,keyorder				= @Keyorder
			,keyRelatedOrder		= @KeyRelatedOrder
			,tokenToResp			= @tokenToResp
			,usrdata				= @dateT
			,usrinis				= @user
		where token =@token

	END 




GO
Grant Execute on dbo.up_RNU_saveResponse to Public
Grant control on dbo.up_RNU_saveResponse to Public
GO