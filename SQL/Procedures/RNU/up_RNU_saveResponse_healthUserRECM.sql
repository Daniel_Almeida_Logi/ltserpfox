/* save response RNU

	exec up_RNU_saveResponse_healthUserRECM ''

	
*/

if OBJECT_ID('[dbo].[up_RNU_saveResponse_healthUserRECM]') IS NOT NULL
	drop procedure dbo.up_RNU_saveResponse_healthUserRECM
go

create procedure [dbo].up_RNU_saveResponse_healthUserRECM
				@stamp				  AS varchar(36)
				,@token 			  AS varchar(36)
				,@Code 				  AS varchar(100)
				,@Desc 				  AS varchar(254)
				,@BeginDate		      AS datetime
				,@EndDate 			  AS datetime
				,@ChangeDate 		  AS datetime
				,@ExpirationDate	  AS datetime
/* with encryption */

AS
SET NOCOUNT ON

DECLARE @user VARCHAR(20)
DECLARE @dateT DATETIME

	SET @user = 'ADM'
	set @dateT= GETDATE()
	
	IF(NOT EXISTS(SELECT * FROM ext_RNU_RespHealthU_RECM  WHERE stamp=@stamp))
	BEGIN
		INSERT INTO ext_RNU_RespHealthU_RECM(stamp,token,code,codeDesc,beginDate,endDate,changeDate,expirationDate,ousrdata,ousrinis,usrdata,usrinis)
		VALUES (  @stamp ,@token ,@Code,@Desc,@BeginDate,@EndDate,@ChangeDate,@ExpirationDate,@dateT,@user,@dateT,@user	)
		
	END 
	ELSE 
	BEGIN 

		UPDATE ext_RNU_RespHealthU_RECM
		SET										 	
			 token 			  =	@token 		
			,code 			  =	@Code 			
			,codeDesc 		  =	@Desc 			
			,beginDate		  =	@BeginDate		
			,endDate 		  =	@EndDate 		
			,changeDate 	  =	@ChangeDate 	
			,expirationDate	  =	@ExpirationDate
			,usrinis		  = @user
			,usrdata		  = @dateT
		where stamp = @stamp
	END 


GO
Grant Execute on dbo.up_RNU_saveResponse_healthUserRECM to Public
Grant control on dbo.up_RNU_saveResponse_healthUserRECM to Public
GO