/* save response RNU

	exec up_RNU_saveResponse_healthUserContact ''

	
*/

if OBJECT_ID('[dbo].[up_RNU_saveResponse_healthUserContact]') IS NOT NULL
	drop procedure dbo.up_RNU_saveResponse_healthUserContact
go

create procedure [dbo].up_RNU_saveResponse_healthUserContact
		 @stamp 					   AS varchar(36)
		,@token 					   AS varchar(36)
		,@Name 						   AS varchar(150)
		,@Phone						   AS varchar(50)
		,@MobilePhone				   AS varchar(50)
		,@OtherTypeContact			   AS varchar(100)
		,@NumberOtherTypeContact	   AS varchar(100)
		,@obs						   AS varchar(254)
		,@Email						   AS varchar(100)
		,@ChangeDate				   AS datetime


/* with encryption */

AS
SET NOCOUNT ON

DECLARE @user VARCHAR(20)
DECLARE @dateT DATETIME

	SET @user = 'ADM'
	set @dateT= GETDATE()
	
	IF(NOT EXISTS(SELECT * FROM ext_RNU_RespHealthU_Contacts  WHERE stamp=@stamp))
	BEGIN
		INSERT INTO ext_RNU_RespHealthU_Contacts(stamp,token,name,phone,mobilePhone,otherTypeContact,numberOtherTypeContact,obs,email,changeDate,ousrdata,ousrinis,usrdata,usrinis )
		VALUES (@stamp,@token,@name,@phone,@mobilePhone,@otherTypeContact,@numberOtherTypeContact,@obs,@email,@changeDate,@dateT,@user,@dateT,@user)
		
	END 
	ELSE 
	BEGIN 

		UPDATE ext_RNU_RespHealthU_Contacts
		SET
			 @token 				  = @token
			,@Name 					  = @Name
			,@Phone					  = @Phone
			,@MobilePhone			  = @MobilePhone
			,@OtherTypeContact		  = @OtherTypeContact
			,@NumberOtherTypeContact  = @NumberOtherTypeContact
			,@obs					  = @obs
			,@Email					  = @Email
			,@ChangeDate			  = @ChangeDate 
			,usrdata				  = @dateT
			,usrinis				  = @user  
		where stamp = @stamp
	END 


GO
Grant Execute on dbo.up_RNU_saveResponse_healthUserContact to Public
Grant control on dbo.up_RNU_saveResponse_healthUserContact to Public
GO