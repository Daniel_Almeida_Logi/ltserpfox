/* save response RNU

exec up_RNU_saveResponse_healthUser 'e461c39a-dd2c-4f7e-bbde-6dce6f338f4','04B2BD7A-C166-41BB-A85D-D18D79D3A541',398623804,3113400990497,120,207844674,0,'UTENTE SERVI�OS INTEGRA��ES RNU NACIONAL TESTE','','','1999-08-28 12:00:00','N','','','','','','','PT','Portugal','','','','','','','','','','','','','','','','','','','','','','','','','','N','N','2017-10-24 05:29:35','2020-05-12 09:40:20','2018-02-07 12:16:20','f053d4bf-2c87-4d4b-a51b-983a0c1bdde','76ee517f-9f59-4c95-9dc8-8edf6c1f6cc','','','702be14a-c9d3-4796-a04f-64dab9ddbf6','ba5088ca-72fc-477e-88f5-3048781652c'

	
*/

if OBJECT_ID('[dbo].[up_RNU_saveResponse_healthUser]') IS NOT NULL
	drop procedure dbo.up_RNU_saveResponse_healthUser
go

create procedure [dbo].up_RNU_saveResponse_healthUser
	 @stampHealthUser			AS varchar(36)
	,@token						AS varchar(36)
	,@snsNumber					AS NUMERIC(23,0)
	,@nicsNumber				AS NUMERIC(23,0)
	,@OperationalNumber			AS NUMERIC(23,0)
	,@Nif						AS NUMERIC(23,0)
	,@Niss						AS NUMERIC(23,0)
	,@FullName					AS varchar(254)
	,@OwnNames					AS varchar(254)
	,@Surname					AS varchar(254)
	,@BirthDate					AS DATETIME
	,@Death						AS varchar(20)
	,@DeathDate					AS DATETIME
	,@ResidenceVisa				AS varchar(100)
	,@ValidityVisaResidence		AS DATETIME
	,@ChangeDate				AS DATETIME
	,@Sex						AS varchar(30)
	,@CountryNac				AS varchar(100)
	,@CountryNacDesc			AS varchar(254)
	,@DisNat					AS varchar(100)
	,@DisNatDesc				AS varchar(254)
	,@cNat						AS varchar(100)
	,@cNatDesc					AS varchar(254)
	,@ParishNat					AS varchar(100)
	,@ParishNatDesc				AS varchar(254)
	,@RegionNat					AS varchar(100)
	,@LocationNat				AS varchar(100)
	,@CityNat					AS varchar(100)
	,@CountryNat				AS varchar(100)
	,@CountryNatDesc			AS varchar(254)
	,@TypeDocIdent				AS varchar(50)
	,@TypeDocIdentDesc          AS varchar(254)
	,@nDocIdent					AS varchar(254)
	,@ProfessionCode			AS varchar(100)
	,@ProfessionDesc			AS varchar(254)
	,@ProfessionStatusCode      AS varchar(100)
	,@ProfessionStatusDesc		AS varchar(254)
	,@HabLiterary				AS varchar(100)
	,@HabLiteraryDesc			AS varchar(100)
	,@OwnnamesFather			AS varchar(100)
	,@SurnameFather				AS varchar(100)
	,@OwnnamesMother			AS varchar(100)
	,@SurnameMother				AS varchar(100)
	,@CitizenCard				AS varchar(100)
	,@Duplicate					AS varchar(100)
	,@PotDuplicate				AS varchar(100)
	,@ChangeDateBenef			AS DATETIME
	,@ChangeDateContacts		AS DATETIME
	,@ChangeDateRespEnt			AS DATETIME
	,@addressStamp				AS varchar(36)
	,@listContactsToken			AS varchar(36)
	,@registrationToken			AS varchar(36)
	,@healthExemptionFeeToken	AS varchar(36)
	,@healthExemptionRECMToken  AS varchar(36)
	,@healthUserOtherToken		AS varchar(36)
	,@healthUserRespEntToken	AS varchar(36)

/* with encryption */

AS
SET NOCOUNT ON

DECLARE @user VARCHAR(20)
DECLARE @dateT DATETIME

	SET @user = 'ADM'
	set @dateT= GETDATE()
	
	IF(NOT EXISTS(SELECT * FROM ext_RNU_RespHealthU  WHERE stamp=@stampHealthUser))
	BEGIN
		INSERT INTO ext_RNU_RespHealthU(stamp ,token ,sns,nics ,OperationalNumber,Nif,Niss,FullName,OwnNames,Surname,BirthDate,Death,DeathDate,ResidenceVisa,visaResidenceDate,ChangeDate,Sex,CountryNac
	,CountryNacDesc,DisNat,DisNatDesc,cNat,cNatDesc,ParishNat,ParishNatDesc,RegionNat,LocationNat,CityNat,CountryNat,CountryNatDesc,TypeDocIdent,TypeDocIdentDesc,nDocIdent,ProfessionCode
	,ProfessionDesc,ProfessionStatusCode,ProfessionStatusDesc,HabLiterary,HabLiteraryDesc,OwnnamesFather,SurnameFather,OwnnamesMother,SurnameMother,CitizenCard,Duplicate,PotDuplicate,ChangeDateBenef
	,ChangeDateContacts,ChangeDateRespEnt,addressStamp,listContactsToken ,registrationToken ,healthExemptionFeeToken ,healthExemptionRECMToken ,healthUserOtherToken,healthUserRespEntToken,ousrdata,ousrinis,usrdata,usrinis)
		VALUES(	 @stampHealthUser ,@token ,@snsNumber,@nicsNumber ,@OperationalNumber,@Nif,@Niss,@FullName,@OwnNames,@Surname,@BirthDate,@Death,@DeathDate,@ResidenceVisa,@ValidityVisaResidence,@ChangeDate,@Sex,@CountryNac
				,@CountryNacDesc,@DisNat,@DisNatDesc,@cNat,@cNatDesc,@ParishNat,@ParishNatDesc,@RegionNat,@LocationNat,@CityNat,@CountryNat,@CountryNatDesc,@TypeDocIdent,@TypeDocIdentDesc,@nDocIdent,@ProfessionCode
				,@ProfessionDesc,@ProfessionStatusCode,@ProfessionStatusDesc,@HabLiterary,@HabLiteraryDesc,@OwnnamesFather,@SurnameFather,@OwnnamesMother,@SurnameMother,@CitizenCard,@Duplicate,@PotDuplicate,@ChangeDateBenef
				,@ChangeDateContacts,@ChangeDateRespEnt,@addressStamp,@listContactsToken ,@registrationToken ,@healthExemptionFeeToken ,@healthExemptionRECMToken ,@healthUserOtherToken,@healthUserRespEntToken,@dateT,@user,@dateT,@user )
	END 
	ELSE 
	BEGIN 

		UPDATE ext_RNU_RespHealthU
		SET
			token					 = @token
			,sns					 = @snsNumber
			,nics 					 = @nicsNumber
			,OperationalNumber		 = @OperationalNumber
			,Nif					 = @Nif
			,Niss					 = @Niss
			,FullName				 = @FullName
			,OwnNames				 = @OwnNames
			,Surname				 = @Surname
			,BirthDate				 = @BirthDate
			,Death					 = @Death
			,DeathDate				 = @DeathDate
			,ResidenceVisa			 = @ResidenceVisa
			,visaResidenceDate		 = @ValidityVisaResidence
			,ChangeDate				 = @ChangeDate
			,Sex					 = @Sex
			,CountryNac				 = @CountryNac
			,CountryNacDesc			 = @CountryNacDesc
			,DisNat					 = @DisNat
			,DisNatDesc				 = @DisNatDesc
			,cNat					 = @cNat
			,cNatDesc				 = @cNatDesc
			,ParishNat				 = @ParishNat
			,ParishNatDesc			 = @ParishNatDesc
			,RegionNat				 = @RegionNat
			,LocationNat			 = @LocationNat
			,CityNat				 = @CityNat
			,CountryNat				 = @CountryNat
			,CountryNatDesc			 = @CountryNatDesc
			,TypeDocIdent			 = @TypeDocIdent
			,TypeDocIdentDesc		 = @TypeDocIdentDesc
			,nDocIdent				 = @nDocIdent
			,ProfessionCode			 = @ProfessionCode
			,ProfessionDesc			 = @ProfessionDesc
			,ProfessionStatusCode	 = @ProfessionStatusCode
			,ProfessionStatusDesc	 = @ProfessionStatusDesc
			,HabLiterary			 = @HabLiterary
			,HabLiteraryDesc		 = @HabLiteraryDesc
			,OwnnamesFather			 = @OwnnamesFather
			,SurnameFather			 = @SurnameFather
			,OwnnamesMother			 = @OwnnamesMother
			,SurnameMother			 = @SurnameMother
			,CitizenCard			 = @CitizenCard
			,Duplicate				 = @Duplicate
			,PotDuplicate			 = @PotDuplicate
			,ChangeDateBenef		 = @ChangeDateBenef
			,ChangeDateContacts		 = @ChangeDateContacts
			,ChangeDateRespEnt		 = @ChangeDateRespEnt
			,addressStamp			 = @addressStamp
			,listContactsToken 		 = @listContactsToken
			,registrationToken 		 = @registrationToken
			,healthExemptionFeeToken = @healthExemptionFeeToken
			,healthExemptionRECMToken= @healthExemptionRECMToken
			,healthUserOtherToken	 = @healthUserOtherToken
			,healthUserRespEntToken  = @healthUserRespEntToken
			,usrdata				 = @dateT
			,usrinis				 = @user
		where stamp = @stampHealthUser

	END 


GO
Grant Execute on dbo.up_RNU_saveResponse_healthUser to Public
Grant control on dbo.up_RNU_saveResponse_healthUser to Public
GO