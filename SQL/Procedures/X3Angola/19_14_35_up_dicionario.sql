-- SP:	up_dicionario
-- exec	up_dicionario '000248','Loja 1'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_dicionario]') IS NOT NULL
	drop procedure dbo.up_dicionario
go

create PROCEDURE [dbo].[up_dicionario]

@ref	varchar(25),
@site   varchar(15) = 'Loja 1'

/* with encryption */
AS
SET NOCOUNT ON

DECLARE @PAIS VARCHAR(55)
DECLARE @control BIT
SET @control =0

select @PAIS = textValue from B_Parameters_site where  stamp = 'ADM0000000050' and site = @site

if (@PAIS='Portugal')
BEGIN
	select
		fprod.*
		,departamento = isnull(b_famDepartamentos.design ,'')
		,seccao = isnull(b_famSeccoes.design,'')
		,categoria = isnull(b_famCategorias.design,'')
		,famfamilia =  isnull(b_famFamilias.design,'')
		,posologia_orientativa = isnull(posologia.posologia_orientativa,'')
	from
		fprod (nolock)
		left join b_famDepartamentos (nolock) on fprod.depstamp = b_famDepartamentos.depstamp
		left join b_famSeccoes (nolock) on fprod.secstamp = b_famSeccoes.secstamp
		left join b_famCategorias (nolock) on fprod.catstamp = b_famCategorias.catstamp
		left join b_famFamilias (nolock) on fprod.famstamp = b_famFamilias.famstamp
			left join posologia (nolock) on posologia.id_med = fprod.medid
	where
		cnp=@ref
END
ELSE
BEGIN
	DECLARE @codigo varchar(40)
	DECLARE emp_cursor CURSOR FOR
	SELECT codigo  FROM bc where ref = @ref
	OPEN emp_cursor
	FETCH NEXT FROM emp_cursor INTO @codigo

	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosDicionario'))
	DROP TABLE #dadosDicionario


	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF (EXISTS(SELECT 1 FROM fprod (NOLOCK) WHERE cnp =@codigo))
		BEGIN
			SET @control = 1
				select
					fprod.*
					,departamento = isnull(b_famDepartamentos.design ,'')
					,seccao = isnull(b_famSeccoes.design,'')
					,categoria = isnull(b_famCategorias.design,'')
					,famfamilia =  isnull(b_famFamilias.design,'')
					,posologia_orientativa = isnull(posologia.posologia_orientativa,'')
				from
					fprod (nolock)
					left join b_famDepartamentos (nolock) on fprod.depstamp = b_famDepartamentos.depstamp
					left join b_famSeccoes (nolock) on fprod.secstamp = b_famSeccoes.secstamp
					left join b_famCategorias (nolock) on fprod.catstamp = b_famCategorias.catstamp
					left join b_famFamilias (nolock) on fprod.famstamp = b_famFamilias.famstamp
						left join posologia (nolock) on posologia.id_med = fprod.medid
				where
					cnp=@codigo
			BREAK;
		END
		FETCH NEXT FROM emp_cursor INTO @codigo   
	END
	CLOSE emp_cursor
	DEALLOCATE emp_cursor

	IF (@control = 0) -- caso não tenha nenhum tem return uma linha vazia para 
	BEGIN
			select
				fprod.*
				,departamento = isnull(b_famDepartamentos.design ,'')
				,seccao = isnull(b_famSeccoes.design,'')
				,categoria = isnull(b_famCategorias.design,'')
				,famfamilia =  isnull(b_famFamilias.design,'')
				,posologia_orientativa = isnull(posologia.posologia_orientativa,'')
			from
				fprod (nolock)
				left join b_famDepartamentos (nolock) on fprod.depstamp = b_famDepartamentos.depstamp
				left join b_famSeccoes (nolock) on fprod.secstamp = b_famSeccoes.secstamp
				left join b_famCategorias (nolock) on fprod.catstamp = b_famCategorias.catstamp
				left join b_famFamilias (nolock) on fprod.famstamp = b_famFamilias.famstamp
				left join posologia (nolock) on posologia.id_med = fprod.medid
			where
				cnp=@ref
	END 
END 

Go
Grant Execute on dbo.up_dicionario to Public
Grant Control on dbo.up_dicionario to Public
Go