use mecofarma
go


UPDATE b_utentes
SET   eplafond = ut1.eplafond
FROM  b_utentes(nolock)
	  INNER JOIN [172.20.90.17].mecofarma.dbo.b_utentes ut1(nolock) ON ut1.utstamp=b_utentes.utstamp
WHERE  b_utentes.eplafond <> ut1.eplafond
   

go