
IF NOT EXISTS (SELECT * FROM ExternalIntegrationDates WHERE NAME='X3INTART')
BEGIN 
	INSERT INTO ExternalIntegrationDates (x3stamp,name,date,timeStamp)
	VALUES((SELECT LEFT(NEWID(),36)),'X3INTART','1900-01-01 00:00:00.000','0')
END 	  
	  
IF NOT EXISTS (SELECT * FROM ExternalIntegrationDates WHERE NAME='X3INTPVP')
BEGIN 
	INSERT INTO ExternalIntegrationDates (x3stamp,name,date,timeStamp)
	VALUES((SELECT LEFT(NEWID(),36)),'X3INTPVP','1900-01-01 00:00:00.000','0')
END 	  	  

IF NOT EXISTS (SELECT * FROM ExternalIntegrationDates WHERE NAME='X3INTALT')
BEGIN 
	INSERT INTO ExternalIntegrationDates (x3stamp,name,date,timeStamp)
	VALUES((SELECT LEFT(NEWID(),36)),'X3INTALT',getdate(),'0')
END 	  		  

IF NOT EXISTS (SELECT * FROM ExternalIntegrationDates WHERE NAME='X3INTDIV')
BEGIN 
	INSERT INTO ExternalIntegrationDates (x3stamp,name,date,timeStamp)
	VALUES((SELECT LEFT(NEWID(),36)),'X3INTDIV',getdate(),'0')
END 	

IF NOT EXISTS (SELECT * FROM ExternalIntegrationDates WHERE NAME='X3INTBPC')
BEGIN 
	INSERT INTO ExternalIntegrationDates (x3stamp,name,date,timeStamp)
	VALUES((SELECT LEFT(NEWID(),36)),'X3INTBPC',getdate(),'0')
END 
IF NOT EXISTS (SELECT * FROM ExternalIntegrationDates WHERE NAME='X3INTMOV')
BEGIN 
	INSERT INTO ExternalIntegrationDates (x3stamp,name,date,timeStamp)
	VALUES((SELECT LEFT(NEWID(),36)),'X3INTMOV',getdate(),'0')
END 	
IF NOT EXISTS (SELECT * FROM ExternalIntegrationDates WHERE NAME='X3INTREC')
BEGIN 
	INSERT INTO ExternalIntegrationDates (x3stamp,name,date,timeStamp)
	VALUES((SELECT LEFT(NEWID(),36)),'X3INTREC',getdate(),'0')
END 		