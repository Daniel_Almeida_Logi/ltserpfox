IF (NOT EXISTS (SELECT 1 FROM B_modoPag_moeda WHERE codigoExt = '1' AND mpStamp='ADM001' ))
BEGIN
	UPDATE B_modoPag_moeda SET codigoExt = '1' WHERE mpStamp='ADM001'
END 
IF (NOT EXISTS (SELECT 1 FROM B_modoPag_moeda WHERE codigoExt = '2' AND mpStamp='ADM003' ))
BEGIN
	UPDATE B_modoPag_moeda SET codigoExt = '2' WHERE mpStamp='ADM003'
END
IF (NOT EXISTS (SELECT 1 FROM B_modoPag_moeda WHERE codigoExt = '6' AND mpStamp='ADM005'))
BEGIN
	UPDATE B_modoPag_moeda SET codigoExt = '6' WHERE mpStamp='ADM005'
END 

IF (NOT EXISTS (SELECT 1 FROM B_modoPag_moeda WHERE codigoExt = '5' AND mpStamp='ADM006'))
BEGIN
	UPDATE B_modoPag_moeda SET codigoExt = '5' WHERE mpStamp='ADM006'
END 