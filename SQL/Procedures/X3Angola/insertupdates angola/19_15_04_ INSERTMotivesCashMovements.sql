


IF NOT EXISTS (SELECT * FROM MotivesCashMovements WHERE id='1')
BEGIN 
	INSERT INTO MotivesCashMovements (token,id,descr,type,descrType,ousrdata)
	VALUES((SELECT LEFT(NEWID(),36)),'1','Combustíveis','2','Saída',GETDATE())
END 

IF NOT EXISTS (SELECT * FROM MotivesCashMovements WHERE id='2')
BEGIN 
	INSERT INTO MotivesCashMovements (token,id,descr,type,descrType,ousrdata)
	VALUES((SELECT LEFT(NEWID(),36)),'2','Alimentação','2','Saída',GETDATE())
END 

IF NOT EXISTS (SELECT * FROM MotivesCashMovements WHERE id='3')
BEGIN 
	INSERT INTO MotivesCashMovements (token,id,descr,type,descrType,ousrdata)
	VALUES((SELECT LEFT(NEWID(),36)),'3','Higiene e Segurança','2','Saída',GETDATE())
END 

IF NOT EXISTS (SELECT * FROM MotivesCashMovements WHERE id='4')
BEGIN 
	INSERT INTO MotivesCashMovements (token,id,descr,type,descrType,ousrdata)
	VALUES((SELECT LEFT(NEWID(),36)),'4','Material de Escritório','2','Saída',GETDATE())
END 

IF NOT EXISTS (SELECT * FROM MotivesCashMovements WHERE id='5')
BEGIN 
	INSERT INTO MotivesCashMovements (token,id,descr,type,descrType,ousrdata)
	VALUES((SELECT LEFT(NEWID(),36)),'5','Comunicação','2','Saída',GETDATE())
END 

IF NOT EXISTS (SELECT * FROM MotivesCashMovements WHERE id='6')
BEGIN 
	INSERT INTO MotivesCashMovements (token,id,descr,type,descrType,ousrdata)
	VALUES((SELECT LEFT(NEWID(),36)),'6','Água','2','Saída',GETDATE())
END 	

