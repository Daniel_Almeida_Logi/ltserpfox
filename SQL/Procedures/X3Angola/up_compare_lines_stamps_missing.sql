---- 
--Compara stamps de linhas da bi e da fi entre tabelas de servidores diferentes

--exec up_compare_lines_fields_missing 'MACULUSSO', '[172.20.90.17].mecofarma', '[172.20.30.6\SQLEXPRESS].mecofarma', '20230101', '20230301', 'bi'
--exec up_compare_lines_fields_missing 'MACULUSSO', '[172.20.30.6\SQLEXPRESS].mecofarma', '[172.20.90.17].mecofarma', '20230101', '20230301', 'fi'




if OBJECT_ID('[dbo].[up_compare_lines_fields_missing]') IS NOT NULL
	drop procedure dbo.up_compare_lines_fields_missing
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



create procedure dbo.up_compare_lines_fields_missing

  @site				as varchar(40)
, @dbOrigem			as varchar(254)
, @dbDestino		as varchar(254)
, @dataini			as varchar(100)
, @datafim			as varchar(100)
, @tabelas			as varchar(255)



/* WITH ENCRYPTION */
AS
	declare @sqlCreateTable varchar(MAX) = ''
	declare @sqlFi          varchar(MAX) = ''
	declare @sqlBi			varchar(MAX) = ''
	declare @sqlFinal	    varchar(MAX) = ''
	declare @armazem        int = 0

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosLinhasFaltaTemp'))
		DROP TABLE #dadosLinhasFaltaTemp

	select 
		@armazem = empresa_arm.armazem 
	from 
		empresa_arm(nolock) 
	inner join 
		empresa (nolock) on empresa.no = empresa_arm.empresa_no
	where 
		site = @site


	select @sqlCreateTable = N'
		
		create table #dadosLinhasFaltaTemp (	
			atributo				 varchar(254) 
			,stamp					 varchar(254) 		
			,tipoDoc			     varchar(254)
			,numero			         varchar(254)
			,data			         datetime
			,stampCab			     varchar(254)	

		) 
	
	'

	if (charindex('fi', @tabelas) > 0)
	begin
		set @sqlFi = @sqlFi + ' 
		insert into #dadosLinhasFaltaTemp
			( 
			  atributo,
			  stamp,
			  tipoDoc,
			  numero,
			  data,
			  stampCab		
			)
		select 
			''Linhas Vendas Fi'' as atributo,
			 origem.fistamp as stamp,
			 origem.nmdoc as tipoDoc,
			 origem.fno as Numero,
			 origem.ousrdata + origem.ousrhora as data,
			 origem.ftstamp as stampCab
		from ' + @dbOrigem + '.dbo.fi origem with  (nolock) 
		left join ' + @dbDestino + '.dbo.fi destino with (nolock)  on origem.fistamp=destino.fistamp
		where 
			origem.ousrdata >= '''+ @dataini +''' 
			and origem.ousrdata <= '''+ @datafim +'''
			and destino.fistamp is  null
			and origem.armazem = '+ convert(varchar(3),@armazem)+'
			order by origem.ousrdata + origem.ousrhora asc
		'

	end

	select @sqlBi = N''

	if (charindex('bi', @tabelas) > 0)
	begin

		set @sqlBi = @sqlBi + '
		insert into #dadosLinhasFaltaTemp
			( 
			  atributo,
			  stamp,
			  tipoDoc,
			  numero,
			  data,
			  stampCab		
			) 
		select 
			''Linhas Vendas Bi'' as atributo,
			 origem.bistamp as stamp,
			 origem.nmdos as tipoDoc,
			 origem.obrano as Numero,
			 origem.ousrdata + origem.ousrhora as data,
			 origem.bostamp as stampCab
		from ' + @dbOrigem + '.dbo.bi origem with  (nolock) 
		left join ' + @dbDestino + '.dbo.bi destino with (nolock)  on origem.bistamp=destino.bistamp
		where 
			origem.ousrdata >= '''+ @dataini +''' 
			and origem.ousrdata <= '''+ @datafim +'''
			and destino.bistamp is  null
			and origem.armazem = '+ convert(varchar(3),@armazem)+'
			order by origem.ousrdata + origem.ousrhora asc
		'
	end

	select @sqlFinal = N'		
		select * from #dadosLinhasFaltaTemp 
		'


	
	print @sqlCreateTable
	print @sqlFi
	print @sqlBi
	print @sqlFinal
	EXECUTE (@sqlCreateTable + @sqlFi + @sqlBi + @sqlFinal)


	SET ANSI_NULLS OFF
	GO
	SET QUOTED_IDENTIFIER OFF
	GO

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosLinhasFaltaTemp'))
		DROP TABLE #dadosLinhasFaltaTemp

	
 
GO
Grant Execute On dbo.up_compare_lines_fields_missing to Public
Grant Control On dbo.up_compare_lines_fields_missing to Public
GO


