
/****** Object:  Table [dbo].[x3_Logs]    Script Date: 02/10/2019 14:17:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'x3_Logs'))
BEGIN

	CREATE TABLE [dbo].[x3_Logs](
		[stamp] [varchar](36) NOT NULL,
		[type] VARCHAR (50) NOT NULL,
		[name]  VARCHAR (100) NOT NULL,
		[date] DATETIME,
		[description] varchar(1000),
		[tableName] VARCHAR (50) NOT NULL,
		[registerStamp] VARCHAR (50) NOT NULL,
	 CONSTRAINT [PK_x3_Logs] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[x3_Logs] ADD  CONSTRAINT [DF_x3_Logs_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[x3_Logs] ADD  CONSTRAINT [DF_x3_Logs_type]  DEFAULT ('') FOR [type]

	ALTER TABLE [dbo].[x3_Logs] ADD  CONSTRAINT [DF_x3_Logs_name]  DEFAULT ('') FOR [name]

	ALTER TABLE [dbo].[x3_Logs] ADD  CONSTRAINT [DF_x3_Logs_date]  DEFAULT (getdate()) FOR [date]
																					
	ALTER TABLE [dbo].[x3_Logs] ADD  CONSTRAINT [DF_x3_Logs_description]  DEFAULT ('') FOR [description]

	ALTER TABLE [dbo].[x3_Logs] ADD  CONSTRAINT [DF_x3_Logs_tableName]  DEFAULT ('') FOR [tableName]

	ALTER TABLE [dbo].[x3_Logs] ADD  CONSTRAINT [DF_x3_Logs_registerStamp]  DEFAULT ('') FOR [registerStamp]
END										
											