
/****** Object:  Table [dbo].[ExternalCommunicationLogsHist]    Script Date: 14/01/2020 14:47:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'ExternalCommunicationLogsHist'))
BEGIN

	CREATE TABLE [dbo].[ExternalCommunicationLogsHist](
		[stamp]				VARCHAR	(36)	NOT NULL,
		[type]				VARCHAR (50)	NOT NULL,
		[name]				VARCHAR (100)	NOT NULL,
		[date]				DATETIME,
		[description]		VARCHAR	(1000),
		[tableName]			VARCHAR (50)	NOT NULL,
		[registerStamp]		VARCHAR (50)	NOT NULL,
		[site]				VARCHAR (60)	NOT NULL,
		[externalResponse]	VARCHAR (MAX),
	 CONSTRAINT [PK_ExternalCommunicationLogsHist] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[ExternalCommunicationLogsHist] ADD  CONSTRAINT [DF_ExternalCommunicationLogsHist_stamp]				DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[ExternalCommunicationLogsHist] ADD  CONSTRAINT [DF_ExternalCommunicationLogsHist_type]				DEFAULT ('') FOR [type]

	ALTER TABLE [dbo].[ExternalCommunicationLogsHist] ADD  CONSTRAINT [DF_ExternalCommunicationLogsHist_name]				DEFAULT ('') FOR [name]

	ALTER TABLE [dbo].[ExternalCommunicationLogsHist] ADD  CONSTRAINT [DF_ExternalCommunicationLogsHist_date]				DEFAULT (getdate()) FOR [date]
																					
	ALTER TABLE [dbo].[ExternalCommunicationLogsHist] ADD  CONSTRAINT [DF_ExternalCommunicationLogsHist_description]		DEFAULT ('') FOR [description]

	ALTER TABLE [dbo].[ExternalCommunicationLogsHist] ADD  CONSTRAINT [DF_ExternalCommunicationLogsHist_tableName]			DEFAULT ('') FOR [tableName]

	ALTER TABLE [dbo].[ExternalCommunicationLogsHist] ADD  CONSTRAINT [DF_ExternalCommunicationLogsHist_registerStamp]		DEFAULT ('') FOR [registerStamp]

	ALTER TABLE [dbo].[ExternalCommunicationLogsHist] ADD  CONSTRAINT [DF_ExternalCommunicationLogsHist_site]				DEFAULT ('') FOR [site]

	ALTER TABLE [dbo].[ExternalCommunicationLogsHist] ADD  CONSTRAINT [DF_ExternalCommunicationLogsHist_externalResponse]	DEFAULT ('') FOR [externalResponse]

END										
						
							