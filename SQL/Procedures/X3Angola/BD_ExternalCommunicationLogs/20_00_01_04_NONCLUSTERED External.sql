
USE [LogsExternal]
GO
CREATE NONCLUSTERED INDEX ['IX_ExternalCommunicationLogs_relLogs']
ON [dbo].[ExternalCommunicationLogs] ([type],[name],[tableName],[registerStamp],[site])

GO