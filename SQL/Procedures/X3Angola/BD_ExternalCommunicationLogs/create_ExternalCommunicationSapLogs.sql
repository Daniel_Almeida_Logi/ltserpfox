USE [LogsExternal]
GO

/****** Object:  Table [dbo].[ExternalCommunicationSapLogs]    Script Date: 23/02/2024 15:32:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ExternalCommunicationSapLogs](
	[stamp] [varchar](36) NOT NULL,
	[type] [varchar](50) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[date] [datetime] NULL,
	[description] [varchar](1000) NULL,
	[tableName] [varchar](50) NOT NULL,
	[registerStamp] [varchar](50) NOT NULL,
	[site] [varchar](60) NOT NULL,
	[externalResponse] [varchar](max) NOT NULL
 CONSTRAINT [PK_ExternalCommunicationSapLogs_stamp] PRIMARY KEY CLUSTERED 
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[ExternalCommunicationSapLogs] ADD  CONSTRAINT [DF_ExternalCommunicationSapLogs_stamp]  DEFAULT ('') FOR [stamp]
GO

ALTER TABLE [dbo].[ExternalCommunicationSapLogs] ADD  CONSTRAINT [DF_ExternalCommunicationSapLogs_type]  DEFAULT ('') FOR [type]
GO

ALTER TABLE [dbo].[ExternalCommunicationSapLogs] ADD  CONSTRAINT [DF_ExternalCommunicationSapLogs_name]  DEFAULT ('') FOR [name]
GO

ALTER TABLE [dbo].[ExternalCommunicationSapLogs] ADD  CONSTRAINT [DF_ExternalCommunicationSapLogs_date]  DEFAULT (getdate()) FOR [date]
GO

ALTER TABLE [dbo].[ExternalCommunicationSapLogs] ADD  CONSTRAINT [DF_ExternalCommunicationSapLogs_description]  DEFAULT ('') FOR [description]
GO

ALTER TABLE [dbo].[ExternalCommunicationSapLogs] ADD  CONSTRAINT [DF_ExternalCommunicationSapLogs_tableName]  DEFAULT ('') FOR [tableName]
GO

ALTER TABLE [dbo].[ExternalCommunicationSapLogs] ADD  CONSTRAINT [DF_ExternalCommunicationSapLogs_registerStamp]  DEFAULT ('') FOR [registerStamp]
GO

ALTER TABLE [dbo].[ExternalCommunicationSapLogs] ADD  CONSTRAINT [DF_ExternalCommunicationSapLogs_site]  DEFAULT ('') FOR [site]
GO

ALTER TABLE [dbo].[ExternalCommunicationSapLogs] ADD  CONSTRAINT [DF_ExternalCommunicationSapLogs_externalResponse]  DEFAULT ('') FOR [externalResponse]
GO




