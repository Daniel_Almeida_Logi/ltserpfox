/*  Inserir artigo 

	exec x3_servico_Inserirx3_log 

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[x3_servico_Inserirx3_log]') IS NOT NULL
	drop procedure dbo.x3_servico_Inserirx3_log
go

CREATE PROCEDURE [dbo].[x3_servico_Inserirx3_log]
	@type			VARCHAR(18),
	@name			VARCHAR(100),
	@descr			VARCHAR(1000),
	@tableName		VARCHAR(50),
	@registerStamp	VARCHAR(50),
	@site			VARCHAR(60),
	@xmlinput		VARCHAR(MAX)=''
AS
SET NOCOUNT ON
DECLARE @stamp VARCHAR(36)
set @stamp=''
DECLARE @getdate VARCHAR(36)
SET  @getdate = getdate()
SELECT @stamp=ISNULL(stamp,'') FROM ExternalCommunicationLogs WHERE name=@name  AND  type=@type AND tableName=@tableName AND registerStamp = @registerStamp
IF (@stamp = '' OR len(@stamp)=0)
BEGIN
	BEGIN TRANSACTION;
		SET @stamp = LEFT(NEWID(),36)
		INSERT INTO  ExternalCommunicationLogs (stamp,[type],[name],[date],[description],[tableName],registerStamp,site, externalResponse)
		values (@stamp,@type,@name,@getdate,@descr,@tableName,@registerStamp,@site,@xmlinput)
		INSERT INTO ExternalCommunicationLogsHist(stamp,type,name,date,description,tableName,registerStamp,site,externalResponse)
		values (@stamp,@type,@name,@getdate,@descr,@tableName,@registerStamp,@site,@xmlinput)
	COMMIT  TRANSACTION;
END
ELSE
BEGIN
	BEGIN TRANSACTION;
		UPDATE ExternalCommunicationLogs SET date=@getdate, [description] =@descr , externalResponse=@xmlinput WHERE stamp=@stamp
		UPDATE ExternalCommunicationLogsHist SET date=@getdate, [description] =@descr , externalResponse=@xmlinput WHERE stamp=@stamp
	COMMIT  TRANSACTION;
END
IF ((@type='INSERIDO' OR @TYPE='ATUALIZADO') AND EXISTS(SELECT stamp FROM ExternalCommunicationLogs WHERE name=@name  AND  type='ERRO' AND tableName=@tableName AND registerStamp = @registerStamp))
BEGIN 
	SELECT @stamp=stamp FROM ExternalCommunicationLogs WHERE name=@name  AND  type='ERRO' AND tableName=@tableName AND registerStamp = @registerStamp AND site = @site
	DELETE ExternalCommunicationLogs WHERE stamp=@stamp
END


GO
GRANT EXECUTE ON dbo.x3_servico_Inserirx3_log to Public
GRANT CONTROL ON dbo.x3_servico_Inserirx3_log to Public
GO