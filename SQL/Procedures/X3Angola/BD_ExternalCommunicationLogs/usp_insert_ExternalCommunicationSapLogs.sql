/*  Inserir artigo 

	exec usp_insert_ExternalCommunicationSapLogs 
	ExternalCommunicationLogs
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[usp_insert_ExternalCommunicationSapLogs]') IS NOT NULL
	drop procedure dbo.usp_insert_ExternalCommunicationSapLogs
go

CREATE PROCEDURE [dbo].[usp_insert_ExternalCommunicationSapLogs]
	@type			VARCHAR(18),
	@name			VARCHAR(100),
	@descr			VARCHAR(1000),
	@tableName		VARCHAR(50),
	@registerStamp	VARCHAR(50),
	@site			VARCHAR(60),
	@xmlinput		VARCHAR(MAX)=''
AS
SET NOCOUNT ON
DECLARE @stamp VARCHAR(36)

set @stamp=''
DECLARE @getdate VARCHAR(36)
SET  @getdate = getdate()
SELECT @stamp=ISNULL(stamp,'') FROM ExternalCommunicationSapLogs WHERE name=@name  AND  type=@type AND tableName=@tableName AND registerStamp = @registerStamp
IF (@stamp = '' OR len(@stamp)=0)
BEGIN
	BEGIN TRANSACTION;
		SET @stamp = LEFT(NEWID(),36)
		INSERT INTO  ExternalCommunicationSapLogs (stamp,[type],[name],[date],[description],[tableName],registerStamp,site, externalResponse)
		values (@stamp,@type,@name,@getdate,@descr,@tableName,@registerStamp,@site,@xmlinput)

	COMMIT  TRANSACTION;
END
ELSE
BEGIN
	BEGIN TRANSACTION;
		UPDATE ExternalCommunicationSapLogs SET date=@getdate, [description] =@descr , externalResponse=@xmlinput , [type]=@type WHERE stamp=@stamp
	COMMIT  TRANSACTION;
END
IF ((@type='INSERIDO' OR @TYPE='ATUALIZADO') AND EXISTS(SELECT stamp FROM ExternalCommunicationSapLogs WHERE name=@name  AND  type='ERRO' AND tableName=@tableName AND registerStamp = @registerStamp))
BEGIN 
	SELECT @stamp=stamp FROM ExternalCommunicationSapLogs WHERE name=@name  AND  type='ERRO' AND tableName=@tableName AND registerStamp = @registerStamp AND site = @site
	DELETE ExternalCommunicationSapLogs WHERE stamp=@stamp
END


GO
GRANT EXECUTE ON dbo.usp_insert_ExternalCommunicationSapLogs to Public
GRANT CONTROL ON dbo.usp_insert_ExternalCommunicationSapLogs to Public
GO