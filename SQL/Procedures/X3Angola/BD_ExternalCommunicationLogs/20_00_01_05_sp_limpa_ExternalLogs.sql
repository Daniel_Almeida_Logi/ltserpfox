/*  apaga os registo maiores que um mes na ExternalCommunicationLogs

*/
USE [LogsExternal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
if OBJECT_ID('[dbo].[sp_limpa_ExternalLogs]') IS NOT NULL
	drop procedure dbo.sp_limpa_ExternalLogs
go

CREATE PROCEDURE [dbo].[sp_limpa_ExternalLogs]
AS
SET NOCOUNT ON
	delete ExternalCommunicationLogs where date < DATEADD(m,-1,GETDATE())
GO 
GRANT EXECUTE ON dbo.sp_limpa_ExternalLogs to Public
GRANT CONTROL ON dbo.sp_limpa_ExternalLogs to Public
GO

