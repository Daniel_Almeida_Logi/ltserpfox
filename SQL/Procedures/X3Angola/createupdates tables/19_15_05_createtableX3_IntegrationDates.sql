

/****** Object:  Table [dbo].[TPA_Message]    Script Date: 27/08/2019 12:55:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON



IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'X3_IntegrationDates'))
BEGIN

	CREATE TABLE [dbo].[X3_IntegrationDates](
		[x3stamp] [varchar](40) NOT NULL,
		[name] [varchar](254) NOT NULL,
		[date] [datetime] NOT NULL,
		[timeStamp] [varchar](40) NOT NULL,
	 CONSTRAINT [PK_X3_IntegrationDates] PRIMARY KEY CLUSTERED 
	(
		[name] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[X3_IntegrationDates] ADD  CONSTRAINT [DF_X3_IntegrationDates_x3stamp]  DEFAULT ('') FOR [x3stamp]

	ALTER TABLE [dbo].[X3_IntegrationDates] ADD  CONSTRAINT [DF_X3_IntegrationDates_name]  DEFAULT ('') FOR [name]

	ALTER TABLE [dbo].[X3_IntegrationDates] ADD  CONSTRAINT [DF_X3_IntegrationDates_timeStamp]  DEFAULT ('0') FOR [timeStamp]
END