SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'X3_Communication'))
BEGIN

	CREATE TABLE [dbo].[X3_Communication](
		[token] [varchar](36) NOT NULL,
		[name] [varchar](40) NOT NULL,
		[ref] [varchar](18) NOT NULL,
		[qtt] NUMERIC(11,2) NOT NULL,
		[message] [varchar](254) NOT NULL,
		[ousrdata] DATETIME
	 CONSTRAINT [PK_X3_Communication] PRIMARY KEY CLUSTERED 
	(
		[token] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[X3_Communication] ADD  CONSTRAINT [DF_X3_Communication_token]  DEFAULT ('') FOR [token]

	ALTER TABLE [dbo].[X3_Communication] ADD  CONSTRAINT [DF_X3_Communication_name]  DEFAULT ('') FOR [name]

	ALTER TABLE [dbo].[X3_Communication] ADD  CONSTRAINT [DF_X3_Communication_ref]  DEFAULT ('') FOR [ref]

	ALTER TABLE [dbo].[X3_Communication] ADD  CONSTRAINT [DF_X3_Communication_qtt]  DEFAULT ((-9999999)) FOR [qtt]

	ALTER TABLE [dbo].[X3_Communication] ADD  CONSTRAINT [DF_X3_Communication_ousrdata]  DEFAULT (GETDATE())  FOR [ousrdata]
	
	ALTER TABLE [dbo].[X3_Communication] ADD  CONSTRAINT [DF_X3_Communication_message]  DEFAULT ('') FOR [message]
END