SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'MotivesCashMovements'))
BEGIN

	CREATE TABLE [dbo].[MotivesCashMovements](
		[token] [varchar](36) NOT NULL,
		[id] [varchar](36)  NOT NULL,
		[descr] [varchar](100) NOT NULL,
		[type] int NOT NULL,
		[descrType] [varchar](254),
		[ousrdata] DATETIME
	 CONSTRAINT [PK_MotivesCashMovements] PRIMARY KEY CLUSTERED 
	(
		[token] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[MotivesCashMovements] ADD  CONSTRAINT [DF_MotivesCashMovements_token]  DEFAULT ('') FOR [token]

	ALTER TABLE [dbo].[MotivesCashMovements] ADD  CONSTRAINT [DF_MotivesCashMovements_descr]  DEFAULT ('') FOR [descr]

	ALTER TABLE [dbo].[MotivesCashMovements] ADD  CONSTRAINT [DF_MotivesCashMovements_type]  DEFAULT ('') FOR [type]

	ALTER TABLE [dbo].[MotivesCashMovements] ADD  CONSTRAINT [DF_MotivesCashMovements_descrType]  DEFAULT ('') FOR [descrType]

	ALTER TABLE [dbo].[MotivesCashMovements] ADD  CONSTRAINT [DF_MotivesCashMovements_ousrdata]  DEFAULT (GETDATE())  FOR [ousrdata]

END