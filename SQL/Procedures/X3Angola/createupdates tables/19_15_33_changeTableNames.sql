
IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'X3_Communication'))
BEGIN
	EXEC sp_rename 'X3_Communication', 'ExternalCommunication'
END 

IF(EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'X3_IntegrationDates'))
BEGIN
	EXEC sp_rename 'X3_IntegrationDates', 'ExternalIntegrationDates'
END

IF(EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'X3_CommunicationExchange'))
BEGIN
	EXEC sp_rename 'X3_CommunicationExchange', 'ExternalCommunicationExchange'
END