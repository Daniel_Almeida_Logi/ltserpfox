/****** Object:  Index [IX_b_utentes_no_ext]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[b_utentes]') AND name = N'IX_b_utentes_no_ext')
CREATE NONCLUSTERED INDEX [IX_b_utentes_no_ext] ON [dbo].[b_utentes]
(
	[no_ext] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


