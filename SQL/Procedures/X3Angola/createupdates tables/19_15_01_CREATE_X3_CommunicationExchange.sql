SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'X3_CommunicationExchange'))
BEGIN
	
	CREATE TABLE [dbo].[X3_CommunicationExchange](
		[token] [varchar](36) NOT NULL,
		[date]	 DATETIME,
		[manual] [bit] NOT NULL,
		[ousrdate] DATETIME,
		[message] [varchar](254) NOT NULL,
	 CONSTRAINT [PK_X3_CommunicationExchange] PRIMARY KEY CLUSTERED 
	(
		[token] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[X3_CommunicationExchange] ADD  CONSTRAINT [DF_X3_CommunicationExchange_token]  DEFAULT ('') FOR [token]

	ALTER TABLE [dbo].[X3_CommunicationExchange] ADD  CONSTRAINT [DF_X3_CommunicationExchange_date]  DEFAULT ('') FOR [date]

	ALTER TABLE [dbo].[X3_CommunicationExchange] ADD  CONSTRAINT [DF_X3_CommunicationExchange_ousrdate]  DEFAULT (GETDATE())  FOR [ousrdate]
	
	ALTER TABLE [dbo].[X3_CommunicationExchange] ADD  CONSTRAINT [DF_X3_CommunicationExchange_manual]  DEFAULT (0) FOR [manual]

	ALTER TABLE [dbo].[X3_CommunicationExchange] ADD  CONSTRAINT [DF_X3_CommunicationExchange_message]  DEFAULT ('') FOR [message]
END