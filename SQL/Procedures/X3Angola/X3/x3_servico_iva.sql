/*  Inserir artigo 

	exec x3_servico_iva 'NOR',14,'AO','20191001','Nacional taxa Normal'
	exec x3_servico_iva 'STD',2.0,'AO','20191001','Nacional taxa Red  F50 52  ','NR','NOR'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[x3_servico_iva]') IS NOT NULL
	drop procedure dbo.x3_servico_iva
go

CREATE PROCEDURE [dbo].[x3_servico_iva]
	@codigoReg		VARCHAR(40),
	@taxa			NUMERIC(5,2),
	@AbrevPais		VARCHAR(60),
	@DataInicio		DATETIME,
	@descricao		VARCHAR(60),
	@codTaxa		varchar(18),
	@codArt			varchar(30)

AS
SET NOCOUNT ON
	DECLARE @desgin VARCHAR(1000)
	DECLARE @tabiva   NUMERIC(4,1)
	DECLARE @desctaxa VARCHAR(60)
	DECLARE @taxasivastamp VARCHAR(25)
	DECLARE @regivastamp VARCHAR(25)

	IF(@codigoReg like '%ISE%')
	BEGIN
		SET @desctaxa = 'Isenta'
		SET @codigoReg   = 'ISE'
	END
	ELSE IF (@codigoReg like '%STD%')
	BEGIN
		IF (@codTaxa like '%NN%')
		BEGIN
			SET @desctaxa = 'Normal'
			SET @codigoReg   = 'NOR'
		END
		ELSE IF (@codTaxa like '%NR%')
		BEGIN
			SET @desctaxa = 'Reduzida'
			SET @codigoReg   = 'RED'
		END 
	END
	ELSE
	BEGIN 	
		SET @desctaxa = @descricao
	END 

	SELECT 	
	@tabiva=ISNULL(taxasiva.codigo,0) 
	FROM taxasiva (NOLOCK) 
		INNER JOIN regiva (NOLOCK)  ON taxasiva.codigo = regiva.tabiva
	WHERE regiva.codigo =@codigoReg AND  u_cod = @codTaxa 

	IF(@tabiva !=0)
	BEGIN
		IF(EXISTS(SELECT 1 FROM taxasiva  (NOLOCK) WHERE codigo = @tabiva and u_actdate < @DataInicio and @DataInicio<=GETDATE()))
		BEGIN
			SELECT @taxasivastamp = taxasivastamp FROM taxasiva  WHERE codigo = @tabiva

			UPDATE taxasiva
				SET
				taxa		= @taxa,
				u_actdate	= @DataInicio,
				usrinis		= 'ADM',
				usrdata     = (SELECT CONVERT(date, getdate())),
				usrhora     = (SELECT CONVERT(VARCHAR(8), GETDATE(), 114)),
				u_cod       = @codTaxa,
				u_codeExt   = @codArt
			WHERE codigo = @tabiva
			SET @desgin = 'Atualizado com sucesso na taxasiva com o codigo '+ @codigoReg
			EXEC LogsExternal.dbo.x3_servico_Inserirx3_log 'ATUALIZADO','X3INTIVA',@desgin , 'taxasiva',@taxasivastamp,''

			SELECT @regivastamp = regivastamp FROM regiva  WHERE tabiva = @tabiva

			UPDATE regiva
			SET
				taxa	   = @taxa,
				desctaxa   = @desctaxa,
				descricao  = @AbrevPais,
				usrinis		= 'ADM',
				usrdata     = (SELECT CONVERT(date, getdate())),
				usrhora     = (SELECT CONVERT(VARCHAR(8), GETDATE(), 114))
			WHERE tabiva = @tabiva

			SET @desgin = 'Atualizado com sucesso na regiva com o codigo '+ @codigoReg
			EXEC LogsExternal.dbo.x3_servico_Inserirx3_log 'ATUALIZADO','X3INTIVA',@desgin,'regiva',@regivastamp,''
		END   
	END

GO
GRANT EXECUTE ON dbo.x3_servico_iva to Public
GRANT CONTROL ON dbo.x3_servico_iva to Public
GO