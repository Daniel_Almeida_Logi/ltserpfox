/*  informacao dos artigos para gravar documento no x3 
	exec x3_servico_InforDocDetalhesArt  'ADMF7521F4C-A9BB-46E2-BE2'
	exec x3_servico_InforDocDetalhesArt  'ADM1F33632B-3B4D-43AA-BC0'
	exec x3_servico_InforDocDetalhesArt  'ADM21ABF03A-243A-4D2C-96C'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[x3_servico_InforDocDetalhesArt]') IS NOT NULL
	drop procedure dbo.x3_servico_InforDocDetalhesArt
go

CREATE PROCEDURE [dbo].[x3_servico_InforDocDetalhesArt]
	@ftStamp	        VARCHAR(36)
	
AS
SET NOCOUNT ON

	
	SELECT 
		  FI.ref																																			AS nArtigo
		, 'UNF'																																				AS unidadeVenda
		, FI.qtt																																			AS quantidadeArtigo
		, Cast(Round(case when fi.ivaincl = 1 then ((ABS(fi.etiliquido) / (fi.iva/100+1)))/fi.qtt else (ABS(fi.etiliquido))/FI.qtt end,2) AS DECIMAL(20,2))	AS precoBruto
		, taxasiva.u_codeExt																																AS taxaNivel
		, Cast(Round(case when fi.ivaincl = 1 then (ABS(fi.etiliquido) / (fi.iva/100+1)) else ABS(fi.etiliquido) end,2)	AS DECIMAL(20,2))					AS montanteTotalLinhaSiva
		, Cast(Round(case when fi.ivaincl = 1 then ABS(fi.etiliquido) else ABS(fi.etiliquido) + (ABS(fi.etiliquido)*(fi.iva/100)) end,2)AS DECIMAL(20,2))	AS montanteTotalLinhaCiva
		, Cast(Round(case when fi.ivaincl = 1 then	ABS(fi.etiliquido) - (ABS(fi.etiliquido) / (fi.iva/100+1)) else fi.etiliquido 
		+ (ABS(fi.etiliquido)*(fi.iva/100)) - ABS(fi.etiliquido) end,2)		AS DECIMAL(20,2))																AS montanteLinhaIva
		, ISNULL(FI2.valcativa,0)																															AS montanteCativado
		, ''																																				AS categoriaCativado -- vai vazio pois o campo é meramente informativo
	FROM FT	(NOLOCK)
			INNER JOIN FI (NOLOCK)			ON FT.ftstamp = FI.ftstamp
			LEFT  JOIN FI2 (NOLOCK)			ON FI2.fistamp = FI.fistamp
			INNER JOIN taxasiva (NOLOCK)	ON FI.tabiva = taxasiva.codigo
	WHERE  FT.ftstamp =@ftStamp AND ref !='' and ref<>'V999999' and ref<>'V000001' and qtt!=0
		
		
GO
GRANT EXECUTE ON dbo.x3_servico_InforDocDetalhesArt to Public
GRANT CONTROL ON dbo.x3_servico_InforDocDetalhesArt to Public
GO