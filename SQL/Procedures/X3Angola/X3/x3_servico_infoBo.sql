-- 
--  exec x3_servico_infoBo '','F03','F032209RCI00000010','20220924'

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[x3_servico_infoBo]') IS NOT NULL
	drop procedure dbo.x3_servico_infoBo
go

CREATE PROCEDURE [dbo].[x3_servico_infoBo]
	@codExtOri			VARCHAR(20),
	@codExtDes			VARCHAR(20),
	@codDocExt			Varchar(60),
	@date				date
	
AS
SET NOCOUNT ON
	DECLARE @boStamp VARCHAR(25)
	DECLARE @atualizar TINYINT 
	DECLARE @codExt VARCHAR(20)
	DECLARE @obrano numeric(10,0)
	DECLARE @no	INT 

	IF(EXISTS(SELECT * FROM BO2 WHERE codext =@codDocExt))
	BEGIN
	print 1

		SELECT @boStamp = bo2stamp FROM BO2  (NOLOCK) WHERE codext =@codDocExt
		select @obrano = obrano FROM BO  (NOLOCK) WHERE bostamp=@boStamp
		SET @atualizar = 1

		print @boStamp

				print @obrano

		if(@obrano is null or isnull(@obrano,0)=0)
		begin
			delete BO2 WHERE codext =@codDocExt
			delete bi2 where bi2stamp in (select bistamp from bi (NOLOCK) where bostamp=@boStamp)
			delete bi where bostamp=@boStamp

			SET @boStamp = LEFT(NEWID(),25)
			SET @atualizar = 0
			SET @obrano = (SELECT ISNULL(MAX(obrano)+ 1,1)  from bo  (NOLOCK) WHERE boano = (select YEAR(@date)) AND ndos = 7 )	
		end 

	END 
	ELSE
	BEGIN
		print 2
		SET @boStamp = LEFT(NEWID(),25)
		SET @atualizar = 0
		SET @obrano = (SELECT ISNULL(MAX(obrano)+ 1,1)  from bo WHERE boano = (select YEAR(@date)) AND ndos = 7 )	
	END 

	SET @codExt = '0'
	SET @no = 0
	IF((SELECT [SITE] FROM EMPRESA WHERE siteExt =@codExtDes) !=NULL)
	BEGIN
		print 242
		SELECT @codExt=[SITE],@no=NO FROM EMPRESA WHERE siteExt =@codExtDes
	END
	ELSE IF((SELECT [SITE] FROM EMPRESA WHERE siteExt =@codExtOri)!=NULL)
	BEGIN
			print 245
		SELECT @codExt=[SITE], @no=NO FROM EMPRESA WHERE siteExt =@codExtOri
	END

	print 24

	SELECT
		@boStamp																									AS bostamp,
		@atualizar																									AS atualizar,
		(SELECT nmdos FROM ts WHERE ndos = 7)																		AS nmdos,
		7																											AS ndos,
		(select nome from AG where no=1)																			AS nome,
		1																											AS [NO], -- NUMERO DA MECOFARMA
		@codExt																										AS site,
		@obrano																										AS obrano,
		(select YEAR(@date))																						AS ano,	
		@no																											AS siteNr
	
GO
GRANT EXECUTE ON dbo.x3_servico_infoBo to Public
GRANT CONTROL ON dbo.x3_servico_infoBo to Public
GO
