/* 
	get doc number to integrate 
	exec usp_servico_getDocNumberToIntegrate 
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[usp_servico_getDocNumberToIntegrate]') IS NOT NULL
	drop procedure dbo.usp_servico_getDocNumberToIntegrate
go

CREATE PROCEDURE [dbo].[usp_servico_getDocNumberToIntegrate]	
AS
SET NOCOUNT ON
	
	SELECT TOP 1 
		stamp							AS stamp ,
		docNumber						AS docNumber,
		numLines						AS numLines,
		ext_boDoc.docType				AS docType,
		ext_boDoc.timestamp             AS timestamp
	FROM ext_boDoc (nolock)
		INNER JOIN DOCMOV (nolock) ON  ext_boDoc.docType= DOCMOV.docType
	WHERE ext_boDoc.imported =0 AND  DOCMOV.mov !='E' and DOCMOV.active=1
		AND  dateadd(hour,2,ext_boDoc.IntegrationDate) < GETDATE()
	ORDER BY ext_boDoc.IntegrationDate ASC
		
GO
GRANT EXECUTE ON dbo.usp_servico_getDocNumberToIntegrate to Public
GRANT CONTROL ON dbo.usp_servico_getDocNumberToIntegrate to Public
GO 