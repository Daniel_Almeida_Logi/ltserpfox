/* 
	insert or update line 
	exec usp_servico_insDocNumberToIntegrate 'F012002RCI00000004','R',1			
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[usp_servico_insDocNumberToIntegrate]') IS NOT NULL
	drop procedure dbo.usp_servico_insDocNumberToIntegrate
go

CREATE PROCEDURE [dbo].[usp_servico_insDocNumberToIntegrate]
	@docNumber		VARCHAR(100),
	@docType		VARCHAR(100),
	@numLines		INT,
	@timeStamp      VARCHAR(36)
		
AS
SET NOCOUNT ON
	
	IF(NOT EXISTS(SELECT * FROM ext_boDoc WHERE docNumber=@docNumber AND docType=@docType))
	BEGIN 
		INSERT INTO ext_boDoc(stamp,docNumber,docType,numLines,imported,ousrinis,ousrdata,usrinis,usrdata,timestamp)
		VALUES (LEFT(NEWID(),36),@docNumber,@docType,@numLines,0,'ADM',GETDATE(),'ADM',GETDATE(),@timeStamp)
	END 
	ELSE If (SELECT numLines FROM ext_boDoc WHERE docNumber=@docNumber AND docType=@docType )<= @numLines
	BEGIN
		UPDATE ext_boDoc
		SET
			docNumber		=@docNumber,
			docType			=@docType,
			numLines		=@numLines,
			imported		=0,
			usrinis			= 'ADM',
			usrdata			= GETDATE(),
			timestamp		= @timeStamp
		WHERE docNumber=@docNumber AND docType=@docType

	END 



	
GO
GRANT EXECUTE ON dbo.usp_servico_insDocNumberToIntegrate to Public
GRANT CONTROL ON dbo.usp_servico_insDocNumberToIntegrate to Public
GO 



