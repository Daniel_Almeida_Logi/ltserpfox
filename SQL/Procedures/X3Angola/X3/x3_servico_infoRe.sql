-- exec x3_servico_infoRe 'FLJ','F01',1,2019,2019,''
--VLJ-F01-201900000117

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[x3_servico_infoRe]') IS NOT NULL
	drop procedure dbo.x3_servico_infoRe
go

CREATE PROCEDURE [dbo].[x3_servico_infoRe]
	@nmDocExt		VARCHAR(30),
	@siteExt		VARCHAR(20),
	@fno			NUMERIC(10,0),
	@ftano			NUMERIC(4,0),
	@anoDoc			NUMERIC(4,0),
	@docNumber		VARCHAR(20)
AS
SET NOCOUNT ON
	DECLARE @ftstamp VARCHAR(25)
	DECLARE @restamp VARCHAR(25)
	DECLARE @atualizar TINYINT
	DECLARE @nome VARCHAR(80)
	DECLARE @desgin VARCHAR(1000)
	DECLARE @site VARCHAR(20)
	Declare @ndoc NUMERIC(6,0)

	SELECT @site=site FROM empresa WHERE siteExt = @siteExt

	SELECT @ndoc=ndoc FROM TD WHERE SITE= @site and nmDocExt = @nmDocExt

	SELECT @ftstamp = ftstamp FROM FT (NOLOCK) WHERE  ndoc = @ndoc AND fno = @fno AND  SITE = @site AND ftano=@ftano

	IF(EXISTS(SELECT * FROM re (NOLOCK)  WHERE fref = @docNumber))
	BEGIN
		SELECT @restamp = restamp FROM re (NOLOCK)  WHERE fref = @docNumber
		SET @atualizar = 1
	END 
	ELSE
	BEGIN
		SET @restamp = LEFT(NEWID(),25)
		SET @atualizar = 0
	END 

	SELECT 
		@restamp																				AS restamp,
		@ftstamp																				AS ftstamp,
		(SELECT nmdoc FROM tsre (nolock) WHERE ndoc=6)											AS nmdoc,
		6																						AS ndoc,
		(SELECT ISNULL(MAX(rno)+ 1,1) FROM re (nolock) WHERE reano = @anoDoc AND ndoc=6)		AS rno,
		nome																					AS nomeCliente,
		no																						AS no,
		morada																					AS morada,
		local																					AS local,
		codpost																					AS codPost,
		ncont																					AS ncont,
		'IMP'																					AS pnome,
		'00'																					AS pno ,
		zona																					AS zona,
		site																					AS site,
		1																						AS pais,
		@atualizar																				AS atualizar,
		estab																					AS estab,
		(SELECT textValue from B_Parameters_site where stamp='ADM0000000003' and site = @site)	AS moedaSistema,
		1                                                                                       AS processo,
		1																						AS vendedor,
		'Administrador de Sis'																	AS vendnm
	FROM 
		FT (NOLOCK)
	WHERE ftstamp = @ftstamp

GO
GRANT EXECUTE ON dbo.x3_servico_infoRe to Public
GRANT CONTROL ON dbo.x3_servico_infoRe to Public
GO