/*  Inserir artigo 

	exec x3_servico_creditocliente 'N001393',0,8354
	 exec x3_servico_creditocliente 'N001393',9.99999999E8,0.0
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[x3_servico_creditocliente]') IS NOT NULL
	drop procedure dbo.x3_servico_creditocliente
go

CREATE PROCEDURE [dbo].[x3_servico_creditocliente]
	@numeroUtente		VARCHAR(20),
	@Limitecredito		NUMERIC(19,6),
	@Credito			NUMERIC(19,6)

AS
SET NOCOUNT ON

	DECLARE @desgin VARCHAR(1000)
	DECLARE @utstamp VARCHAR(25)

	IF(EXISTS(SELECT 1 FROM b_utentes (NOLOCK) WHERE no_ext= @numeroUtente))
	BEGIN
		UPDATE b_utentes
		SET
			eplafond = @Limitecredito,
			esaldo   = @Credito
		WHERE no_ext = @numeroUtente 
		SELECT @utstamp=ISNULL(utstamp,'') FROM b_utentes (NOLOCK) WHERE no_ext= @numeroUtente
		SET @desgin = 'Atualizado com sucesso o limite de credito do utente: ' + @numeroUtente
		EXEC LogsExternal.dbo.x3_servico_Inserirx3_log 'ATUALIZADO','YCREDCCC',@desgin, 'b_utentes',@utstamp,''
	END
	ELSE 
	BEGIN
		SET @desgin = 'O número de utente: ' + @numeroUtente +'ainda não foi criado logo não foi atualizado.'
		EXEC LogsExternal.dbo.x3_servico_Inserirx3_log 'ERRO','YCREDCCC',@desgin , 'b_utentes','',''
	END 
GO
GRANT EXECUTE ON dbo.x3_servico_creditocliente to Public
GRANT CONTROL ON dbo.x3_servico_creditocliente to Public
GO