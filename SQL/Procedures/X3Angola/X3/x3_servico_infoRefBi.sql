SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
-- exec x3_servico_infoRefBi '022658','','F02','20190608','','AOA',''
-- exec x3_servico_infoRefBi '021784','','F02','20190608','','AOA','4BEDE90C-B0A1-4896-AE3D-A',1000
-- exec x3_servico_infoRefBi '001129','F09','F14','20200116','AOA','AOA','F5010EF1-A683-4658-AE86-5',1000

if OBJECT_ID('[dbo].[x3_servico_infoRefBi]') IS NOT NULL
	drop procedure dbo.x3_servico_infoRefBi
go

CREATE PROCEDURE [dbo].[x3_servico_infoRefBi]
	@ref				VARCHAR(18),
	@siteOri            VARCHAR(20),
	@siteDes            VARCHAR(20),
	@data				date,
	@moedaOri			VARCHAR(3),
	@moedaDes			VARCHAR(3),
	@boStamp            VARCHAR(25),
	@lordem				NUMERIC(10,0)=0
	
AS
SET NOCOUNT ON
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosCambio'))
		DROP TABLE #dadosCambio

	create table #dadosCambio(
		cambio NUMERIC(19,12)
	)

	DECLARE @site_nrOri TINYINT
	DECLARE @site_nrDes TINYINT
	DECLARE @site_nr   TINYINT
	DECLARE @atualizar  TINYINT
	DECLARE @bistamp VARCHAR(25)
	DECLARE @cambio NUMERIC(19,12)
	
	set @site_nr=''
	IF(EXISTS(SELECT no FROM EMPRESA WHERE siteExt = @siteOri))
	BEGIN
		SELECT @site_nrOri = no FROM EMPRESA WHERE siteExt = @siteOri
		set @site_nr =LTRIM(RTRIM(@site_nrOri))
	END


	IF(EXISTS(SELECT no FROM EMPRESA WHERE siteExt = @siteDes))	
	BEGIN
		SELECT @site_nrDes = no FROM EMPRESA WHERE siteExt = @siteDes
		IF(@site_nr ='')
		BEGIN
			set @site_nr = LTRIM(RTRIM(@site_nrDes))
		END
	END
	
	
	SET @cambio = 1;
	IF(@moedaOri !=@moedaDes)
	BEGIN 
		INSERT #dadosCambio
		EXEC up_cambioEntreMoedas @moedaOri, @moedaDes ,@Data

		IF ((SELECT count(cambio) from #dadosCambio)!=0)
		BEGIN 
			SET @cambio = (SELECT TOP 1 cambio from #dadosCambio)
		END
	END

	IF(EXISTS(SELECT * FROM BI WHERE bostamp =@boStamp AND lordem= @lordem))
	BEGIN
		SELECT TOP 1 @bistamp = bistamp FROM BI WHERE bostamp =@boStamp AND lordem= @lordem
		SET @atualizar = 1
	END 
	ELSE
	BEGIN
		SET @bistamp =LEFT(NEWID(),25)
		SET @atualizar = 0
	END 


SELECT TOP 1
	 @bistamp								AS bistamp
	,st.ref									AS ref
	,design									AS design
	,familia								AS familia
	,tabiva									AS tabiva
	,taxa									AS taxa
	,(SELECT nmdos FROM ts WHERE ndos = 7)	AS nmdos
	,7										AS ndos
	,ISNULL(@site_nrOri,0)                  AS ori
	,ISNULL(@site_nrDes,0)                  AS des
	,@cambio                                AS cambio
	,qttembal								AS qttembal
	,@atualizar								AS atualizar
FROM st
	INNER JOIN taxasiva ON taxasiva.codigo = st.tabiva 
WHERE st.ref = @ref  and st.site_nr = @site_nr

IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosCambio'))
	DROP TABLE #dadosCambio

GO
GRANT EXECUTE ON dbo.x3_servico_infoRefBi to Public
GRANT CONTROL ON dbo.x3_servico_infoRefBi to Public
GO
