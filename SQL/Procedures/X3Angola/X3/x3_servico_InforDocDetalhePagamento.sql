

/****** Object:  StoredProcedure [dbo].[x3_servico_InforDocDetalhePagamento]    Script Date: 07/09/2022 16:25:09 ******/
-- exec x3_servico_InforDocDetalhePagamento '223061233215XJ','VIANAPARK','frBF326544-A381-4606-B77 ','FLJ' 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



if OBJECT_ID('[dbo].[x3_servico_InforDocDetalhePagamento]') IS NOT NULL
    DROP PROCEDURE dbo.x3_servico_InforDocDetalhePagamento
GO


CREATE PROCEDURE [dbo].[x3_servico_InforDocDetalhePagamento]
	@u_nratend	        VARCHAR(36),
	@site				VARCHAR(60),
	@ftstamp            VARCHAR(25),
	@DocExtT			VARCHAR(10)
AS
SET NOCOUNT ON

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosCambio'))
		DROP TABLE #dadosCambio

	create table #dadosCambio(
		cambio NUMERIC(19,12)
	)

	DECLARE @moedaSistema VARCHAR(3)
	DECLARE @cambEUR   NUMERIC(19,12)
	DECLARE @cambDOL   NUMERIC(19,12)
	DECLARE @data	   DATETIME
	DECLARE @no			NUMERIC(10,2)

	if((select ft.etotal from ft(nolock) where ftstamp=@ftstamp)!=0)
	begin
print(1)
		IF(@DocExtT = 'FLJ')
		BEGIN
			SELECT 
				TOP 1
				@u_nratend=CASE WHEN(cc.restamp !='') THEN re.u_nratend ELSE @u_nratend END ,
				@no = FT.NO
			FROM 
					   FT (NOLOCK)
			INNER JOIN B_pagCentral (NOLOCK) ON B_pagCentral.nrAtend = FT.u_nratend
			INNER JOIN CC			(NOLOCK) ON CC.ftstamp           = FT.ftstamp
			INNER JOIN RL			(NOLOCK) ON RL.ccstamp           = CC.ccstamp
			INNER JOIN RE			(NOLOCK) ON RE.restamp           = RL.restamp
			WHERE FT.ftstamp = @ftstamp AND edebf > 0 and re.nmdoc!='X3'
		END
		ELSE 
		BEGIN
			SELECT 
				TOP 1
				@no = FT.NO
			FROM 
					   FT (NOLOCK)
			INNER JOIN B_pagCentral (NOLOCK) ON B_pagCentral.nrAtend = FT.u_nratend
			WHERE FT.ftstamp = @ftstamp
		END 

	

		SELECT @data=uData FROM B_pagCentral (NOLOCK) WHERE nrAtend = @u_nratend

		SET @moedaSistema = (SELECT textValue from B_Parameters_site where stamp='ADM0000000003' and site = @site)
	
		INSERT #dadosCambio
		EXEC  up_cambioEntreMoedas @moedaSistema ,'EUR',@data
		SET @cambEUR = (SELECT TOP 1 cambio from #dadosCambio)

		DELETE #dadosCambio

		INSERT #dadosCambio
		EXEC  up_cambioEntreMoedas @moedaSistema ,'USD',@data
		SET @cambDOL = (SELECT TOP 1 cambio from #dadosCambio)
		print @cambDOL
		print @DocExtT

		-- Faturas seguradoras e Nota Crédito Seguradoras
		IF (@DocExtT = 'FCO') or (@DocExtT = 'CCO')
		begin 
			-- PLAFOND
			SELECT top 1
					'3'																									AS modoPagamento
				, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar
				, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)   where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
				, ROUND(ABS(FT.etotal),2) 																				AS montantePagamento
				, ROUND(ABS(FT.etotal),2)																				AS montanteBasePagamento
			FROM B_pagCentral (NOLOCK)
				INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp --AND FT.NO= ft.no
			WHERE  B_pagCentral.nrAtend =@u_nratend and B_pagCentral.site =@site and @DocExtT in ('FCO', 'CCO') 
		end

		-- Regularizações de clientes
		IF (@DocExtT = 'DLJ')
		BEGIN
	print(1)
			IF ( EXISTS(SELECT top 1 1 FROM B_pagCentral (NOLOCK) INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM001'
					WHERE  B_pagCentral.nrAtend =@u_nratend  AND  ABS(B_pagCentral.ntCredito)= ABS(B_pagCentral.evdinheiro) AND B_pagCentral.site =@site))
			BEGIN 
				SELECT top 1
					  B_modoPag_moeda.codigoExt																				AS modoPagamento
					, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
					, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)   where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
					, ROUND(ABS(case when B_pagCentral.ntCredito<>ft.etotal then ft.etotal else B_pagCentral.ntCredito end),2)																	AS montantePagamento
					, ROUND(ABS(case when B_pagCentral.ntCredito<>ft.etotal then ft.etotal else B_pagCentral.ntCredito end),2)																	AS montanteBasePagamento
					FROM B_pagCentral (NOLOCK)
						INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM001'
						INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp --
				WHERE  B_pagCentral.nrAtend =@u_nratend  AND  ABS(B_pagCentral.ntCredito)= ABS(B_pagCentral.evdinheiro) AND B_pagCentral.site =@site-- AND B_pagCentral.no=@no

			END 
			ELSE
			BEGIN
				-- MOEDA do sistema 
				SELECT top 1
					  B_modoPag_moeda.codigoExt																				AS modoPagamento
					, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
					, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)   where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
					, ROUND(ABS(B_pagCentral.evdinheiro),2)																	AS montantePagamento
					, ROUND(ABS(B_pagCentral.evdinheiro),2)																	AS montanteBasePagamento
					FROM B_pagCentral (NOLOCK)
						INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM001'
						INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp --
					WHERE  B_pagCentral.nrAtend =@u_nratend AND ABS(B_pagCentral.evdinheiro)>0 AND B_pagCentral.site =@site  AND B_pagCentral.no=@no
	
				UNION ALL 
				-- MULTICAIXA
				SELECT top 1
					  B_modoPag_moeda.codigoExt																				AS modoPagamento
					, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
					, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)  where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
					, ROUND(ABS(B_pagCentral.epaga2),2)																		AS montantePagamento
					, ROUND(ABS( B_pagCentral.epaga2),2)																	AS montanteBasePagamento
				FROM B_pagCentral (NOLOCK)
					INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM003'
					INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp --
				WHERE  B_pagCentral.nrAtend =@u_nratend AND (ABS(B_pagCentral.epaga2)>0)  AND B_pagCentral.site =@site  --AND B_pagCentral.no=@no

				UNION ALL 
				-- cartao CF
				SELECT top 1
					  B_modoPag_moeda.codigoExt																				AS modoPagamento
					, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
					, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)  where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
					, ROUND(ABS( B_pagCentral.epaga1),2)																	AS montantePagamento
					, ROUND(ABS( B_pagCentral.epaga1),2)																	AS montanteBasePagamento
				FROM B_pagCentral (NOLOCK)
					INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM004'
					INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp 
				WHERE  B_pagCentral.nrAtend =@u_nratend AND ( ABS(B_pagCentral.epaga1)>0)  AND B_pagCentral.site =@site  AND B_pagCentral.no=@no

				UNION ALL
					-- REF. MB
				SELECT top 1
					  B_modoPag_moeda.codigoExt																				AS modoPagamento
					, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
					, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)  where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
					, ROUND(ABS(B_pagCentral.epaga3),2) 																	AS montantePagamento
					, ROUND(ABS(B_pagCentral.epaga3),2) 																	AS montanteBasePagamento
				FROM B_pagCentral (NOLOCK)
					INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM005'
					INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp 
				WHERE  B_pagCentral.nrAtend =@u_nratend AND ABS(B_pagCentral.epaga3)>0  AND B_pagCentral.site =@site  AND B_pagCentral.no=@no

	
				UNION ALL
					-- transferencia Bancaria
				SELECT top 1
					  B_modoPag_moeda.codigoExt																				AS modoPagamento
					, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
					, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)  where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
					, ROUND(ABS(B_pagCentral.echtotal),2) 																	AS montantePagamento
					, ROUND(ABS(B_pagCentral.echtotal),2) 																	AS montanteBasePagamento
				FROM B_pagCentral (NOLOCK)
					INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM002'
					INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp 
				WHERE  B_pagCentral.nrAtend =@u_nratend AND ABS(B_pagCentral.echtotal)>0  AND B_pagCentral.site =@site  AND B_pagCentral.no=@no

				UNION ALL 
				-- MOEDA USD
				SELECT 
					  B_modoPag_moeda.codigoExt																				AS modoPagamento
					, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar
					, B_modoPag_moeda.design																				AS divisaPagamento
					, ROUND(ABS(B_pagCentral.epaga4) * @cambDOL,2)															AS montantePagamento
					, ROUND(ABS(B_pagCentral.epaga4),2)																		AS montanteBasePagamento
				FROM B_pagCentral (NOLOCK)
					INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM006'
					INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp 
				WHERE  B_pagCentral.nrAtend =@u_nratend and ABS(B_pagCentral.epaga4)>0 AND B_pagCentral.site =@site  AND B_pagCentral.no=@no

				UNION ALL 
				-- MOEDA EUR
				SELECT top 1
					  B_modoPag_moeda.codigoExt																				AS modoPagamento
					, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar
					, B_modoPag_moeda.design																				AS divisaPagamento
					, ROUND(ABS(B_pagCentral.epaga5) * @cambEUR,2)															AS montantePagamento
					, ROUND(ABS(B_pagCentral.epaga5),2)																		AS montanteBasePagamento
				FROM B_pagCentral (NOLOCK)
					INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM007'
					INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp 
				WHERE  B_pagCentral.nrAtend =@u_nratend and ABS(B_pagCentral.epaga5)>0 AND B_pagCentral.site =@site  AND B_pagCentral.no=@no

				UNION ALL 
				-- PLAFOND
				SELECT top 1
					  '3'																									AS modoPagamento
					, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar
					, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)   where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
					, ROUND(ABS(FT.etotal) - ABS(B_pagCentral.Total),2) 													AS montantePagamento
					, ROUND(ABS(FT.etotal) - ABS(B_pagCentral.Total),2)														AS montanteBasePagamento
				FROM B_pagCentral (NOLOCK)
					INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp 
				WHERE  B_pagCentral.nrAtend =@u_nratend  AND B_pagCentral.Total != FT.etotal AND B_pagCentral.site =@site and @DocExtT in ('FLJ','FCO','CCO','CLJ') AND B_pagCentral.no=@no

				UNION ALL 
				-- quando é Regularização de credito de venda a dinheiro com NTCREDITO preenchidos 
				SELECT top 1
					  B_modoPag_moeda.codigoExt																														AS modoPagamento
					, ''																																			AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
					, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)   where stamp = 'ADM0000000003' AND site =@site)											AS divisaPagamento
					, ROUND(ABS(case when B_pagCentral.ntCredito<>ft.etotal then ft.etotal else B_pagCentral.ntCredito end),2)	 - ROUND(ABS(B_pagCentral.epaga5) * @cambEUR,2)	
					  - ROUND(ABS(B_pagCentral.epaga4) * @cambDOL,2) - ROUND(ABS(B_pagCentral.echtotal),2) - ROUND(ABS(B_pagCentral.epaga3),2)
					  - ROUND(ABS(B_pagCentral.epaga2 +  B_pagCentral.epaga1),2)-	ROUND(ABS(B_pagCentral.evdinheiro),2)											AS montantePagamento
					, ROUND(ABS(case when B_pagCentral.ntCredito<>ft.etotal then ft.etotal else B_pagCentral.ntCredito end),2)- ROUND(ABS(B_pagCentral.epaga5) * @cambEUR,2)	
					  - ROUND(ABS(B_pagCentral.epaga4) * @cambDOL,2) - ROUND(ABS(B_pagCentral.echtotal),2) - ROUND(ABS(B_pagCentral.epaga3),2)
					  - ROUND(ABS(B_pagCentral.epaga2 +  B_pagCentral.epaga1),2)-	ROUND(ABS(B_pagCentral.evdinheiro),2)											AS montanteBasePagamento
					FROM B_pagCentral (NOLOCK)
						INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM001'
						INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp --
				WHERE  B_pagCentral.nrAtend =@u_nratend  AND  ABS(B_pagCentral.ntCredito)>0 AND B_pagCentral.site =@site --and @DocExtT in ('VLJ','DLJ') 
				and ROUND(ABS(case when B_pagCentral.ntCredito<>ft.etotal then ft.etotal else B_pagCentral.ntCredito end),2)	 - ROUND(ABS(B_pagCentral.epaga5) * @cambEUR,2)	
					  - ROUND(ABS(B_pagCentral.epaga4) * @cambDOL,2) - ROUND(ABS(B_pagCentral.echtotal),2) - ROUND(ABS(B_pagCentral.epaga3),2)
					  - ROUND(ABS(B_pagCentral.epaga2 +  B_pagCentral.epaga1),2)-	ROUND(ABS(B_pagCentral.evdinheiro),2) <>0

			END
		END

		-- Fatura Recibo
		if  (@DocExtT = 'VLJ')
		begin
				print '2'
				--print @no
				-- MOEDA do sistema 

				SELECT top 1
					  B_modoPag_moeda.codigoExt																				AS modoPagamento
					, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
					, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)   where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
					, cast((case when B_pagCentral.epaga1+B_pagCentral.epaga2+B_pagCentral.echtotal+B_pagCentral.epaga3+B_pagCentral.epaga4+B_pagCentral.epaga5=0 and B_pagCentral.evdinheiro<>ft.etotal then ROUND(ABS(ft.etotal ),2) else ROUND((B_pagCentral.evdinheiro ),2) end) as numeric(15,2))	AS montantePagamento
					, case when B_pagCentral.epaga1+B_pagCentral.epaga2+B_pagCentral.echtotal+B_pagCentral.epaga3+B_pagCentral.epaga4+B_pagCentral.epaga5=0 and B_pagCentral.evdinheiro<>ft.etotal then ROUND(ABS(ft.etotal ),2) else ROUND((B_pagCentral.evdinheiro ),2) end	AS montanteBasePagamento
					--, ROUND(ABS(B_pagCentral.evdinheiro),2)																	AS montantePagamento
					--, ROUND(ABS(B_pagCentral.evdinheiro),2)																	AS montanteBasePagamento
					FROM B_pagCentral (NOLOCK)
						INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM001'
						INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp 
					WHERE  B_pagCentral.nrAtend =@u_nratend AND ABS(B_pagCentral.evdinheiro)<>0  AND B_pagCentral.site =@site  AND B_pagCentral.no=@no --and B_pagCentral.ntCredito=0
	
				UNION ALL 
				-- MULTICAIXA
				SELECT top 1
					  B_modoPag_moeda.codigoExt																				AS modoPagamento
					, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
					, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)  where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
					, cast((case when B_pagCentral.evdinheiro+B_pagCentral.epaga1+B_pagCentral.echtotal+B_pagCentral.epaga3+B_pagCentral.epaga4+B_pagCentral.epaga5=0 and B_pagCentral.epaga2<>ft.etotal then ROUND(ABS(ft.etotal ),2) else ROUND(ABS(B_pagCentral.epaga2 ),2) end) as numeric(15,2))	AS montantePagamento
					, case when B_pagCentral.evdinheiro+B_pagCentral.epaga1+B_pagCentral.echtotal+B_pagCentral.epaga3+B_pagCentral.epaga4+B_pagCentral.epaga5=0 and B_pagCentral.epaga2<>ft.etotal then ROUND(ABS(ft.etotal ),2) else ROUND(ABS(B_pagCentral.epaga2 ),2) end	AS montanteBasePagamento
				FROM B_pagCentral (NOLOCK)
					INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM003'
					INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp 
				WHERE  B_pagCentral.nrAtend =@u_nratend AND (ABS(B_pagCentral.epaga2)<>0 )  AND B_pagCentral.site =@site  AND B_pagCentral.no=@no

				UNION ALL 
				-- cartão CF
				SELECT top 1
					  B_modoPag_moeda.codigoExt																				AS modoPagamento
					, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
					, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)  where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
					, cast((ROUND(ABS( B_pagCentral.epaga1),2)	) as numeric(15,2))											AS montantePagamento
					, ROUND(ABS( B_pagCentral.epaga1),2)												AS montanteBasePagamento
				FROM B_pagCentral (NOLOCK)
					INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM004'
					INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp 
				WHERE  B_pagCentral.nrAtend =@u_nratend AND ( ABS(B_pagCentral.epaga1)>0)  AND B_pagCentral.site =@site  AND B_pagCentral.no=@no

				UNION ALL
					-- Ref. MB
				SELECT top 1
					  B_modoPag_moeda.codigoExt																				AS modoPagamento
					, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
					, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)  where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
					, cast((ROUND(ABS(B_pagCentral.epaga3),2) ) as numeric(15,2))																	AS montantePagamento
					, ROUND(ABS(B_pagCentral.epaga3),2) 																	AS montanteBasePagamento
				FROM B_pagCentral (NOLOCK)
					INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM005'
					INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp 
				WHERE  B_pagCentral.nrAtend =@u_nratend AND ABS(B_pagCentral.epaga3)>0  AND B_pagCentral.site =@site  AND B_pagCentral.no=@no

	
				UNION ALL
					-- transferencia Bancaria
				SELECT top 1
					  B_modoPag_moeda.codigoExt																				AS modoPagamento
					, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
					, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)  where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
					, cast((case when B_pagCentral.evdinheiro+B_pagCentral.epaga1+B_pagCentral.epaga2+B_pagCentral.epaga3+B_pagCentral.epaga4+B_pagCentral.epaga5+B_pagCentral.ntCredito=0 and B_pagCentral.echtotal<>ft.etotal then ROUND(ABS(ft.etotal ),2) else ROUND(ABS(B_pagCentral.echtotal ),2) end) as numeric(15,2))	AS montantePagamento
					, case when B_pagCentral.evdinheiro+B_pagCentral.epaga1+B_pagCentral.epaga2+B_pagCentral.epaga3+B_pagCentral.epaga4+B_pagCentral.epaga5+B_pagCentral.ntCredito=0 and B_pagCentral.echtotal<>ft.etotal then ROUND(ABS(ft.etotal ),2) else ROUND(ABS(B_pagCentral.echtotal ),2) end	AS montanteBasePagamento
				FROM B_pagCentral (NOLOCK)
					INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM002'
					INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp 
				WHERE  B_pagCentral.nrAtend =@u_nratend AND ABS(B_pagCentral.echtotal)>0  AND B_pagCentral.site =@site  AND B_pagCentral.no=@no

				UNION ALL 
				-- MOEDA USD
				SELECT 
					  B_modoPag_moeda.codigoExt																				AS modoPagamento
					, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar
					, B_modoPag_moeda.design																				AS divisaPagamento
					, cast((ROUND(ABS(B_pagCentral.epaga4) * @cambDOL,2)	) as numeric(15,2))														AS montantePagamento
					, ROUND(ABS(B_pagCentral.epaga4),2)																		AS montanteBasePagamento
				FROM B_pagCentral (NOLOCK)
					INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM006'
					INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp 
				WHERE  B_pagCentral.nrAtend =@u_nratend and ABS(B_pagCentral.epaga4)>0 AND B_pagCentral.site =@site  AND B_pagCentral.no=@no

				UNION ALL 
				-- MOEDA EUR
				SELECT top 1
					  B_modoPag_moeda.codigoExt																				AS modoPagamento
					, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar
					, B_modoPag_moeda.design																				AS divisaPagamento
					, cast((ROUND(ABS(B_pagCentral.epaga5) * @cambEUR,2)) as numeric(15,2))															AS montantePagamento
					, ROUND(ABS(B_pagCentral.epaga5),2)																		AS montanteBasePagamento
				FROM B_pagCentral (NOLOCK)
					INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM007'
					INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp 
				WHERE  B_pagCentral.nrAtend =@u_nratend and ABS(B_pagCentral.epaga5)>0 AND B_pagCentral.site =@site  AND B_pagCentral.no=@no

				--UNION ALL 
				---- PLAFOND
				--SELECT top 1
				--	  '3'																									AS modoPagamento
				--	, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar
				--	, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)   where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
				--	, ROUND(ABS(FT.etotal) - ABS(B_pagCentral.Total),2) 													AS montantePagamento
				--	, ROUND(ABS(FT.etotal) - ABS(B_pagCentral.Total),2)														AS montanteBasePagamento
				--FROM B_pagCentral (NOLOCK)
				--	INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp AND FT.NO= (case when @DocExtT='FCO' then ft.no else B_pagCentral.NO end) 
				--WHERE  B_pagCentral.nrAtend =@u_nratend  AND B_pagCentral.Total != FT.etotal AND B_pagCentral.site =@site and @DocExtT in ('FLJ','FCO','CCO','CLJ') 

				UNION ALL 
				-- quando é Regularização de credito de venda a dinheiro com NTCREDITO preenchidos 
				SELECT top 1
					  B_modoPag_moeda.codigoExt																						AS modoPagamento
					, ''																											AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
					, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)   where stamp = 'ADM0000000003' AND site =@site)			AS divisaPagamento
					, cast((case when B_pagCentral.ntCredito = ft.etotal then ROUND(ABS(B_pagCentral.ntCredito),2) else round(ft.etotal,2) end ) as numeric(15,2))	AS montantePagamento
					, case when B_pagCentral.ntCredito = ft.etotal then ROUND(ABS(B_pagCentral.ntCredito),2) else round(ft.etotal,2) end		AS montanteBasePagamento
					FROM B_pagCentral (NOLOCK)
						INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM001'
						INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp 
				WHERE  B_pagCentral.nrAtend =@u_nratend  AND  ABS(B_pagCentral.ntCredito)>0 AND B_pagCentral.site =@site AND B_pagCentral.evdinheiro =0  AND B_pagCentral.no=@no and B_pagCentral.epaga2=0

			end

		-- Fatura 
		if  (@DocExtT = 'FLJ')
		begin
				DECLARE @totatend   NUMERIC(19,4)
				DECLARE @totcred	NUMERIC(19,4)
				DECLARE @mudaatend  bit = 0
				DECLARE @SYNCDate	DATETIME


				SELECT TOP 1  @SYNCDate = (case when sync_date !='1900-01-01 00:00:00.000' then sync_date else '3000-12-31 00:00:00.000' end) FROM table_sync_status (NOLOCK) WHERE regstamp=@ftstamp


				create table #nratend(
				 u_nratend varchar(36) COLLATE SQL_Latin1_General_CP1_CI_AI

				)

				select @totatend = evdinheiro+epaga2+epaga1+epaga3+echtotal+epaga4+epaga5+ntCredito from B_pagCentral (nolock) where nrAtend= (select u_nratend from ft (nolock) where ftstamp=@ftstamp)
				set @totcred = isnull((SELECT top 1 ROUND(ABS(FT.etotal) - ABS(B_pagCentral.Total),2) 
					FROM B_pagCentral (NOLOCK) 
					INNER JOIN FT (NOLOCK) ON  FT.ftstamp = @ftstamp AND FT.NO= B_pagCentral.NO 
					WHERE  B_pagCentral.nrAtend =@u_nratend  AND B_pagCentral.Total != FT.etotal AND B_pagCentral.site =@site),0)

				if (select COUNT(rlstamp) from rl inner join re on re.restamp=rl.restamp where ccstamp = (select ccstamp from cc where ftstamp=@ftstamp) AND (re.rdata+ re.ousrhora) <=@SYNCDate)>0
				begin 
					print 'Alter nratend'
					insert into #nratend(u_nratend)
					select  
						nrAtend 
					from B_pagCentral (nolock) 
					LEFT JOIN FT (NOLOCK) ON  B_pagCentral.nrAtend = FT.u_nratend AND FT.NO= B_pagCentral.NO 
					 where nrAtend in ( select  u_nratend from re (nolock) where restamp in (select restamp from rl where ccstamp=@ftstamp and erec<>0)) and ISNULL(ft.tipodoc,0) not in (3,5)
					 order by oData desc
															--and CONVERT(varchar, rdata, 112)=(select CONVERT(varchar, fdata, 112) from ft where ft.ftstamp=@ftstamp)))
					--update ft set u_nratend=@u_nratend where ftstamp=@ftstamp
					set @mudaatend = 0
				end 


				if((select count(*) from #nratend)=0)
				begin
					insert into #nratend(u_nratend)
					values (@u_nratend)
				end 
				PRINT @mudaatend

				--else
				--begin
					print '2'
					-- MOEDA do sistema 
					SELECT top 1
						  B_modoPag_moeda.codigoExt																				AS modoPagamento
						, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
						, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)   where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
						, cast(ROUND(ABS(SUM(B_pagCentral.evdinheiro)),2)		as numeric(15,2))	AS montantePagamento
						, cast(ROUND(ABS(SUM(B_pagCentral.evdinheiro)),2)	as numeric(15,2))		AS montanteBasePagamento
						FROM B_pagCentral (NOLOCK)
							INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM001'
						WHERE  B_pagCentral.nrAtend in (select u_nratend from #nratend) AND ABS(B_pagCentral.evdinheiro)>0 AND B_pagCentral.site =@site -- AND B_pagCentral.no=@no
						GROUP BY  B_modoPag_moeda.codigoExt		

					UNION ALL 
					-- MULTICAIXA
					SELECT top 1
						  B_modoPag_moeda.codigoExt																				AS modoPagamento
						, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
						, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)  where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
						, cast(ROUND(ABS(SUM(B_pagCentral.epaga2)),2) as numeric(15,2))	AS montantePagamento
						, cast(ROUND(ABS(SUM(B_pagCentral.epaga2)),2) as numeric(15,2))	AS montanteBasePagamento
						--, ROUND(ABS(B_pagCentral.epaga2 ),2)												AS montantePagamento
						--, ROUND(ABS( B_pagCentral.epaga2 ),2)												AS montanteBasePagamento
					FROM B_pagCentral (NOLOCK)
						INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM003'
					WHERE  B_pagCentral.nrAtend  in (select u_nratend from #nratend)  AND (ABS(B_pagCentral.epaga2)>0 ) AND B_pagCentral.site =@site -- AND B_pagCentral.no=@no
					GROUP BY  B_modoPag_moeda.codigoExt		

					UNION ALL 
					-- cartao CF
					SELECT top 1
						  B_modoPag_moeda.codigoExt																				AS modoPagamento
						, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
						, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)  where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
						, cast(ROUND(ABS(SUM(B_pagCentral.epaga1)),2)	as numeric(15,2))	AS montantePagamento
						, cast(ROUND(ABS(SUM(B_pagCentral.epaga1)),2)	as numeric(15,2))	AS montanteBasePagamento
					FROM B_pagCentral (NOLOCK)
						INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM004'
					WHERE  B_pagCentral.nrAtend  in (select u_nratend from #nratend)  AND ( ABS(B_pagCentral.epaga1)>0)  AND B_pagCentral.site =@site -- AND B_pagCentral.no=@no
					GROUP BY  B_modoPag_moeda.codigoExt		

					UNION ALL
						-- Ref. MB
					SELECT top 1
						  B_modoPag_moeda.codigoExt																				AS modoPagamento
						, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
						, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)  where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
						, cast(ROUND(ABS(SUM(B_pagCentral.epaga3)),2)	as numeric(15,2))	AS montantePagamento
						,CAST( ROUND(ABS(SUM(B_pagCentral.epaga3)),2)	as numeric(15,2))	AS montanteBasePagamento
						--, ROUND(ABS(B_pagCentral.epaga3),2) 																	AS montantePagamento
						--, ROUND(ABS(B_pagCentral.epaga3),2) 																	AS montanteBasePagamento
					FROM B_pagCentral (NOLOCK)
						INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM005'
					WHERE  B_pagCentral.nrAtend  in (select u_nratend from #nratend)  AND ABS(B_pagCentral.epaga3)>0  AND B_pagCentral.site =@site -- AND B_pagCentral.no=@no
					GROUP BY  B_modoPag_moeda.codigoExt		
	
					UNION ALL
						-- transferencia Bancaria
					SELECT top 1
						  B_modoPag_moeda.codigoExt																				AS modoPagamento
						, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
						, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)  where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
						, cast(ROUND(ABS(SUM(B_pagCentral.echtotal)),2)	as numeric(15,2))	AS montantePagamento
						, cast(ROUND(ABS(SUM(B_pagCentral.echtotal)),2)	as numeric(15,2))	AS montanteBasePagamento
						--, ROUND(ABS(B_pagCentral.echtotal),2) 																	AS montantePagamento
						--, ROUND(ABS(B_pagCentral.echtotal),2) 																	AS montanteBasePagamento
					FROM B_pagCentral (NOLOCK)
						INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM002'
					WHERE  B_pagCentral.nrAtend in (select u_nratend from #nratend)  AND ABS(B_pagCentral.echtotal)>0  AND B_pagCentral.site =@site  --AND B_pagCentral.no=@no
					GROUP BY  B_modoPag_moeda.codigoExt		

					UNION ALL 
					-- MOEDA USD
					SELECT 
						  B_modoPag_moeda.codigoExt																				AS modoPagamento
						, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar
						, B_modoPag_moeda.design																				AS divisaPagamento
						, cast(ROUND(ABS(SUM(B_pagCentral.epaga4)) * @cambDOL,2) 	as numeric(15,2))															AS montantePagamento
						, cast(ROUND(ABS(SUM(B_pagCentral.epaga4)),2) 	as numeric(15,2))																		AS montanteBasePagamento
					FROM B_pagCentral (NOLOCK)
						INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM006'
					WHERE  B_pagCentral.nrAtend  in (select u_nratend from #nratend) and ABS(B_pagCentral.epaga4)>0 AND B_pagCentral.site =@site -- AND B_pagCentral.no=@no
					GROUP BY  B_modoPag_moeda.codigoExt	, B_modoPag_moeda.design		

					UNION ALL 
					-- MOEDA EUR
					SELECT top 1
						  B_modoPag_moeda.codigoExt																				AS modoPagamento
						, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar
						, B_modoPag_moeda.design																				AS divisaPagamento
						, cast(ROUND(ABS(SUM(B_pagCentral.epaga5)) * @cambEUR,2)		as numeric(15,2))														AS montantePagamento
						, cast(ROUND(ABS(SUM(B_pagCentral.epaga5)),2)	as numeric(15,2))																		AS montanteBasePagamento
					FROM B_pagCentral (NOLOCK)
						INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM007'
					WHERE  B_pagCentral.nrAtend  in (select u_nratend from #nratend)  and ABS(B_pagCentral.epaga5)>0 AND B_pagCentral.site =@site -- AND B_pagCentral.no=@no
					GROUP BY  B_modoPag_moeda.codigoExt	, B_modoPag_moeda.design		

					UNION ALL 
					-- PLAFOND
			
				SELECT top 1
						  '3'																									AS modoPagamento
						, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar
						, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)   where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
						--, ROUND(ABS(FT.etotal) - ABS(B_pagCentral.Total),2) 													AS montantePagamento
						--, ROUND(ABS(FT.etotal) - ABS(B_pagCentral.Total),2)														AS montanteBasePagamento
						--select restamp from rl where ccstamp=@ftstamp and CONVERT(varchar, rdata, 112)=(select CONVERT(varchar, fdata, 112) from ft where ft.ftstamp=@ftstamp)
						, cast(ROUND(ABS(FT.etotal) - isnull((select sum(rl.erec) from rl inner join re on re.restamp=rl.restamp where ccstamp=@ftstamp and olcodigo!='R00002' and (rl.rdata+ rl.ousrhora)<=@SYNCDate and re.ndoc!=6),0) + (case when (@mudaatend=1 ) then ISNULL((select ntCredito from B_pagCentral (nolock) where nrAtend in (select u_nratend from #nratend)),0) else 0 end),2) 	as numeric(15,2))			AS montantePagamento
						, cast(ROUND(ABS(FT.etotal) - isnull((select sum(rl.erec) from rl inner join re on re.restamp=rl.restamp where ccstamp=@ftstamp and olcodigo!='R00002' and (rl.rdata+ rl.ousrhora)<=@SYNCDate and re.ndoc!=6),0) + (case when (@mudaatend=1 )then ISNULL((select ntCredito from B_pagCentral (nolock) where nrAtend in (select u_nratend from #nratend)),0) else 0 end),2)	as numeric(15,2))		AS montanteBasePagamento
					FROM B_pagCentral (NOLOCK)
						INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp --AND FT.NO= (case when @DocExtT='FCO' then ft.no else B_pagCentral.NO end)-- 
					WHERE  B_pagCentral.nrAtend  in (select u_nratend from #nratend)   --AND B_pagCentral.Total != FT.etotal 
					AND B_pagCentral.site =@site --and @DocExtT in ('FLJ','FCO','CCO','CLJ') 
			
					--UNION ALL 
					-- quando é Regularização de credito de venda a dinheiro com NTCREDITO preenchidos 
					--SELECT top 1
					--	  B_modoPag_moeda.codigoExt																						AS modoPagamento
					--	, ''																											AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
					--	, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)   where stamp = 'ADM0000000003' AND site =@site)			AS divisaPagamento
					--	, case when B_pagCentral.ntCredito = ft.etotal then ROUND(ABS(B_pagCentral.ntCredito),2) else ft.etotal end 	AS montantePagamento
					--	, case when B_pagCentral.ntCredito = ft.etotal then ROUND(ABS(B_pagCentral.ntCredito),2) else ft.etotal end		AS montanteBasePagamento
					--	FROM B_pagCentral (NOLOCK)
					--		INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM001'
					--		INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp-- 
					--WHERE  B_pagCentral.nrAtend =@u_nratend  AND  ABS(B_pagCentral.ntCredito)>0 AND B_pagCentral.site =@site AND B_pagCentral.evdinheiro >=0 -- AND B_pagCentral.no=@no
				--end
			end
		
		-- Nota de crédito
		if  (@DocExtT = 'CLJ')
		begin
				print '2'
				-- MOEDA do sistema 
				SELECT top 1
					  B_modoPag_moeda.codigoExt																				AS modoPagamento
					, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
					, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)   where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
					, ROUND(ABS(B_pagCentral.evdinheiro),2)																	AS montantePagamento
					, ROUND(ABS(B_pagCentral.evdinheiro),2)																	AS montanteBasePagamento
					FROM B_pagCentral (NOLOCK)
						INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM001'
						INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp 
					WHERE  B_pagCentral.nrAtend =@u_nratend AND ABS(B_pagCentral.evdinheiro)>0 AND B_pagCentral.site =@site  AND B_pagCentral.no=@no
	
				UNION ALL 
				-- MULTICAIXA
				SELECT top 1
					  B_modoPag_moeda.codigoExt																				AS modoPagamento
					, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
					, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)  where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
					, ROUND(ABS(B_pagCentral.epaga2 ),2)												AS montantePagamento
					, ROUND(ABS( B_pagCentral.epaga2 ),2)												AS montanteBasePagamento
				FROM B_pagCentral (NOLOCK)
					INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM003'
					INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp 
				WHERE  B_pagCentral.nrAtend =@u_nratend AND (ABS(B_pagCentral.epaga2)>0 )  AND B_pagCentral.site =@site  AND B_pagCentral.no=@no

				UNION ALL 
				-- cartao CF
				SELECT top 1
					  B_modoPag_moeda.codigoExt																				AS modoPagamento
					, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
					, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)  where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
					, ROUND(ABS( B_pagCentral.epaga1),2)												AS montantePagamento
					, ROUND(ABS( B_pagCentral.epaga1),2)												AS montanteBasePagamento
				FROM B_pagCentral (NOLOCK)
					INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM004'
					INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp 
				WHERE  B_pagCentral.nrAtend =@u_nratend AND ( ABS(B_pagCentral.epaga1)>0)  AND B_pagCentral.site =@site  AND B_pagCentral.no=@no

				UNION ALL
					-- Ref. MB
				SELECT top 1
					  B_modoPag_moeda.codigoExt																				AS modoPagamento
					, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
					, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)  where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
					, ROUND(ABS(B_pagCentral.epaga3),2) 																	AS montantePagamento
					, ROUND(ABS(B_pagCentral.epaga3),2) 																	AS montanteBasePagamento
				FROM B_pagCentral (NOLOCK)
					INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM005'
					INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp 
				WHERE  B_pagCentral.nrAtend =@u_nratend AND ABS(B_pagCentral.epaga3)>0  AND B_pagCentral.site =@site  AND B_pagCentral.no=@no

	
				UNION ALL
					-- transferencia Bancaria
				SELECT top 1
					  B_modoPag_moeda.codigoExt																				AS modoPagamento
					, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
					, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)  where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
					, ROUND(ABS(B_pagCentral.echtotal),2) 																	AS montantePagamento
					, ROUND(ABS(B_pagCentral.echtotal),2) 																	AS montanteBasePagamento
				FROM B_pagCentral (NOLOCK)
					INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM002'
					INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp 
				WHERE  B_pagCentral.nrAtend =@u_nratend AND ABS(B_pagCentral.echtotal)>0  AND B_pagCentral.site =@site  AND B_pagCentral.no=@no

				UNION ALL 
				-- MOEDA USD
				SELECT 
					  B_modoPag_moeda.codigoExt																				AS modoPagamento
					, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar
					, B_modoPag_moeda.design																				AS divisaPagamento
					, ROUND(ABS(B_pagCentral.epaga4) * @cambDOL,2)															AS montantePagamento
					, ROUND(ABS(B_pagCentral.epaga4),2)																		AS montanteBasePagamento
				FROM B_pagCentral (NOLOCK)
					INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM006'
					INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp 
				WHERE  B_pagCentral.nrAtend =@u_nratend and ABS(B_pagCentral.epaga4)>0 AND B_pagCentral.site =@site  AND B_pagCentral.no=@no

				UNION ALL 
				-- MOEDA EUR
				SELECT top 1
					  B_modoPag_moeda.codigoExt																				AS modoPagamento
					, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar
					, B_modoPag_moeda.design																				AS divisaPagamento
					, ROUND(ABS(B_pagCentral.epaga5) * @cambEUR,2)															AS montantePagamento
					, ROUND(ABS(B_pagCentral.epaga5),2)																		AS montanteBasePagamento
				FROM B_pagCentral (NOLOCK)
					INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM007'
					INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp 
				WHERE  B_pagCentral.nrAtend =@u_nratend and ABS(B_pagCentral.epaga5)>0 AND B_pagCentral.site =@site  AND B_pagCentral.no=@no

				UNION ALL 
				-- PLAFOND
				SELECT top 1
					  '3'																									AS modoPagamento
					, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar
					, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)   where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
					, ROUND(ABS(FT.etotal) - ABS(B_pagCentral.Total),2) 													AS montantePagamento
					, ROUND(ABS(FT.etotal) - ABS(B_pagCentral.Total),2)														AS montanteBasePagamento
				FROM B_pagCentral (NOLOCK)
					INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp-- AND FT.NO= (case when @DocExtT='FCO' then ft.no else B_pagCentral.NO end)-- 
				WHERE  B_pagCentral.nrAtend =@u_nratend  AND B_pagCentral.Total != FT.etotal AND B_pagCentral.site =@site-- and @DocExtT in ('FLJ','FCO','CCO','CLJ') 

				--UNION ALL 
				---- quando é Regularização de credito de venda a dinheiro com NTCREDITO preenchidos 
				--SELECT top 1
				--	  B_modoPag_moeda.codigoExt																						AS modoPagamento
				--	, ''																											AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
				--	, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)   where stamp = 'ADM0000000003' AND site =@site)			AS divisaPagamento
				--	, case when B_pagCentral.ntCredito = ft.etotal then ROUND(ABS(B_pagCentral.ntCredito),2) else ft.etotal end 	AS montantePagamento
				--	, case when B_pagCentral.ntCredito = ft.etotal then ROUND(ABS(B_pagCentral.ntCredito),2) else ft.etotal end		AS montanteBasePagamento
				--	FROM B_pagCentral (NOLOCK)
				--		INNER JOIN B_modoPag_moeda (NOLOCK) ON  B_modoPag_moeda.mpStamp = 'ADM001'
				--		INNER JOIN FT			   (NOLOCK) ON  FT.ftstamp = @ftstamp 
				--WHERE  B_pagCentral.nrAtend =@u_nratend  AND  ABS(B_pagCentral.ntCredito)>0 AND B_pagCentral.site =@site AND B_pagCentral.evdinheiro >=0  AND B_pagCentral.no=@no
			end

	end 
	else
	begin
		SELECT top 1
		  B_modoPag_moeda.codigoExt																				AS modoPagamento
		, ''																									AS numeroPagamento -- enviar a vazio para já pois o x3 não esta a utilizar 
		, (SELECT textvalue FROM  B_Parameters_site (NOLOCK)   where stamp = 'ADM0000000003' AND site =@site)	AS divisaPagamento
		, ROUND(ABS(0),2)																						AS montantePagamento
		, ROUND(ABS(0),2)																						AS montanteBasePagamento
		FROM  B_modoPag_moeda (NOLOCK) 
		where  B_modoPag_moeda.mpStamp = 'ADM001'

	end 

GO
GRANT EXECUTE ON dbo.x3_servico_InforDocDetalhePagamento to Public
GRANT CONTROL ON dbo.x3_servico_InforDocDetalhePagamento to Public
GO
	
