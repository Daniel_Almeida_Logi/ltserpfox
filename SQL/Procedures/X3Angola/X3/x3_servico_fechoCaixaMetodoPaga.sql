/* 
	exec x3_servico_fechoCaixaMetodoPaga 'BAIXA','20191211'
	exec x3_servico_fechoCaixaMetodoPaga 'BAIXA','20200116'
	SELECT * FROM SS WHERE DATA
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[x3_servico_fechoCaixaMetodoPaga]') IS NOT NULL
	drop procedure dbo.x3_servico_fechoCaixaMetodoPaga
go

CREATE PROCEDURE [dbo].[x3_servico_fechoCaixaMetodoPaga]
	@site				varchar(20),
	@data				date
AS
SET NOCOUNT ON
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosCambio'))
		DROP TABLE #dadosCambio
	create table #dadosCambio(
		cambio NUMERIC(19,12)
	)
	DECLARE @moedaSistema VARCHAR(3)
	DECLARE @cambEUR   NUMERIC(19,12)
	DECLARE @cambDOL   NUMERIC(19,12)
	DECLARE @fundoCaixa		NUMERIC(13,3)
	DECLARE @evdinheiro		NUMERIC(13,3)
	DECLARE @multicaixa		NUMERIC(13,3)
	DECLARE @RefMB			NUMERIC(13,3)
	DECLARE @dinheiroUSD	NUMERIC(13,3)
	DECLARE @cartaoCF		NUMERIC(13,3)
	DECLARE @dinheiroEUR	NUMERIC(13,3)
	DECLARE @TRF			NUMERIC(13,3)
	SET @moedaSistema = (SELECT textValue from B_Parameters_site where stamp='ADM0000000003' and site = @site)
	INSERT #dadosCambio
	EXEC  up_cambioEntreMoedas @moedaSistema ,'EUR',@data
	SET @cambEUR = (SELECT TOP 1 cambio from #dadosCambio)
	DELETE #dadosCambio
	INSERT #dadosCambio
	EXEC  up_cambioEntreMoedas @moedaSistema ,'USD',@data
	SET @cambDOL = (SELECT TOP 1 cambio from #dadosCambio)
	SELECT 
		@fundoCaixa		=SUM(ISNULL(SS.fundoCaixa,0)),
		@evdinheiro		=SUM(ISNULL(SS.evdinheiro,0)),
		@multicaixa		=SUM(ISNULL(SS.epaga1,0)), ------ isto esta assim porque nÃ£o mudaram em angola as colunas para colocar direito os valores b_pagcentral.epaga1 = ss.epaga2
		@cartaoCF		=SUM(ISNULL(SS.epaga2,0)),------ isto esta assim porque nÃ£o mudaram em angola as colunas para colocar direito os valores b_pagcentral.epaga2 = ss.epaga1
		@RefMB			=SUM(ISNULL(SS.fecho_epaga3,0)),
		@dinheiroUSD	=Round(SUM(ISNULL(SS.fecho_epaga4,0)) * @cambDOL,2),
		@dinheiroEUR	=Round(SUM(ISNULL(SS.fecho_epaga5,0)) * @cambEUR,2),
		@TRF			=SUM(ISNULL(SS.echtotal,0))
	FROM SS
	WHERE  SS.dabrir = @data  AND SS.site =@site
	SELECT 
		mPaga,
		valorCont,
		valorContPos,
		moedaSistema,
		valorContPosMoedaSis,
		comissoes,
		totalDespesas,
		montanteEmitido,
		valorReforcado,
		bpcnum
	FROM (
	-- MOEDA do sistema 
		SELECT 
			'CX'												AS mPaga,
			@fundoCaixa											AS valorCont,
			@fundoCaixa											AS valorContPos,
			@moedaSistema										AS moedaSistema,
			@fundoCaixa											AS valorContPosMoedaSis,
			0.00												AS comissoes,
			0.00												AS totalDespesas,
			0.00												AS montanteEmitido,
			0.00												AS valorReforcado,
			''													AS bpcnum
		FROM  B_fechoDia		(NOLOCK)	 
		WHERE (SELECT CONVERT(date, B_fechoDia.dataAbriu)) = @data AND B_fechoDia.site =@site  AND B_fechoDia.dataFecho > '1900-01-01 00:00:00.000'
		UNION ALL 
		-- MOEDA do sistema 
		SELECT 
			codigoExt											 AS mPaga,
			@evdinheiro											 AS valorCont,
			SUM(B_pagCentral.evdinheiro)						 AS valorContPos,
			@moedaSistema										 AS moedaSistema,
			SUM(B_pagCentral.evdinheiro )						 AS valorContPosMoedaSis,
			0.00												 AS comissoes,
			0.00												 AS totalDespesas,
			0.00												 AS montanteEmitido,
			0.00												 AS valorReforcado,
			''													 AS bpcnum
		FROM B_pagCentral				(NOLOCK)	
			INNER JOIN B_modoPag_moeda	(NOLOCK)	ON  B_modoPag_moeda.mpStamp = 'ADM001'
			INNER JOIN B_fechoDia		(NOLOCK)	ON (SELECT CONVERT(date, B_fechoDia.dataAbriu)) = @data and B_fechoDia.site = @site
		WHERE  (SELECT CONVERT(date,  B_pagCentral.oData))=@data  AND nrAtend NOT LIKE 'CX%'  AND B_fechoDia.dataFecho > '1900-01-01 00:00:00.000'  AND B_pagCentral.site =@site
		GROUP BY codigoExt
		UNION ALL 
		-- MULTICAIXA
		SELECT 
			codigoExt												AS mPaga,
			@multicaixa												AS valorCont, -- multicaixa Ã© multibanco e visa 
			SUM( B_pagCentral.epaga2)								AS valorContPos,
			@moedaSistema											AS moedaSistema,
			SUM( B_pagCentral.epaga2)								AS valorContPosMoedaSis,
			0.00													AS comissoes,
			0.00													AS totalDespesas,
			0.00													AS montanteEmitido,
			0.00													AS valorReforcado,
			''														AS bpcnum
		FROM B_pagCentral				(NOLOCK)	
			INNER JOIN B_modoPag_moeda	(NOLOCK)	ON  B_modoPag_moeda.mpStamp = 'ADM003'
			INNER JOIN B_fechoDia		(NOLOCK)	ON (SELECT CONVERT(date, B_fechoDia.dataAbriu)) = @data and B_fechoDia.site = @site
		WHERE  (SELECT CONVERT(date,B_pagCentral.oData))=@data AND nrAtend NOT LIKE 'CX%' AND B_fechoDia.dataFecho > '1900-01-01 00:00:00.000'  AND B_pagCentral.site =@site
		GROUP BY codigoExt
		UNION ALL 

		-- cartao CF
		SELECT 
			codigoExt												AS mPaga,
			@cartaoCF												AS valorCont, -- cartao CF
			SUM(B_pagCentral.epaga1 )								AS valorContPos,
			@moedaSistema											AS moedaSistema,
			SUM(B_pagCentral.epaga1 )								AS valorContPosMoedaSis,
			0.00													AS comissoes,
			0.00													AS totalDespesas,
			0.00													AS montanteEmitido,
			0.00													AS valorReforcado,
			''														AS bpcnum
		FROM B_pagCentral				(NOLOCK)	
			INNER JOIN B_modoPag_moeda	(NOLOCK)	ON  B_modoPag_moeda.mpStamp = 'ADM004'
			INNER JOIN B_fechoDia		(NOLOCK)	ON (SELECT CONVERT(date, B_fechoDia.dataAbriu)) = @data and B_fechoDia.site = @site
		WHERE  (SELECT CONVERT(date,B_pagCentral.oData))=@data AND nrAtend NOT LIKE 'CX%' AND B_fechoDia.dataFecho > '1900-01-01 00:00:00.000'  AND B_pagCentral.site =@site and codigoExt!=''
		GROUP BY codigoExt
		UNION ALL 
		-- Ref. MB
		SELECT 
			codigoExt											AS mPaga,
			@RefMB												AS valorCont,
			SUM(B_pagCentral.epaga3)							AS valorContPos,
			@moedaSistema										AS moedaSistema,
			SUM(B_pagCentral.epaga3)							AS valorContPosMoedaSis,
			0.00												AS comissoes,
			0.00												AS totalDespesas,
			0.00												AS montanteEmitido,
			0.00												AS valorReforcado,
			''													AS bpcnum
		FROM B_pagCentral				(NOLOCK)	
			INNER JOIN B_modoPag_moeda  (NOLOCK)	ON  B_modoPag_moeda.mpStamp = 'ADM005'
			INNER JOIN B_fechoDia		(NOLOCK)	ON (SELECT CONVERT(date, B_fechoDia.dataAbriu)) = @data and B_fechoDia.site = @site
		WHERE  (SELECT CONVERT(date,  B_pagCentral.oData))=@data AND nrAtend NOT LIKE 'CX%' AND B_fechoDia.dataFecho > '1900-01-01 00:00:00.000' AND B_pagCentral.site =@site
		GROUP BY codigoExt
		UNION ALL 
		-- Transferencia Bancaria
		SELECT 
			codigoExt											AS mPaga,
			@TRF												AS valorCont,
			SUM(B_pagCentral.echtotal)							AS valorContPos,
			@moedaSistema										AS moedaSistema,
			SUM(B_pagCentral.echtotal)							AS valorContPosMoedaSis,
			0.00												AS comissoes,
			0.00												AS totalDespesas,
			0.00												AS montanteEmitido,
			0.00												AS valorReforcado,
			''													AS bpcnum
		FROM B_pagCentral				(NOLOCK)	
			INNER JOIN B_modoPag_moeda  (NOLOCK)	ON  B_modoPag_moeda.mpStamp = 'ADM002'
			INNER JOIN B_fechoDia		(NOLOCK)	ON (SELECT CONVERT(date, B_fechoDia.dataAbriu)) = @data and B_fechoDia.site = @site
		WHERE  (SELECT CONVERT(date,  B_pagCentral.oData))=@data AND nrAtend NOT LIKE 'CX%' AND B_fechoDia.dataFecho > '1900-01-01 00:00:00.000' AND B_pagCentral.site =@site
		GROUP BY codigoExt
		UNION ALL 
		-- MOEDA USD
		SELECT  
				codigoExt											AS mPaga,
				@dinheiroUSD										AS valorCont,
				Round(SUM(B_pagCentral.epaga4) * @cambDOL,2)		AS valorContPos,
				'USD'												AS moedaSistema,
				Round(SUM(B_pagCentral.epaga4),2)					AS valorContPosMoedaSis,
				0.00												AS comissoes,
				0.00												AS totalDespesas,
				0.00												AS montanteEmitido,
				0.00												AS valorReforcado,
				''													AS bpcnum
		FROM B_pagCentral				(NOLOCK)	
			INNER JOIN B_modoPag_moeda	(NOLOCK)	ON  B_modoPag_moeda.mpStamp = 'ADM006'
			INNER JOIN B_fechoDia		(NOLOCK)	ON (SELECT CONVERT(date, B_fechoDia.dataAbriu)) = @data and B_fechoDia.site = @site
		WHERE  (SELECT CONVERT(date,  B_pagCentral.oData))=@data AND nrAtend NOT LIKE 'CX%' AND B_fechoDia.dataFecho > '1900-01-01 00:00:00.000' AND B_pagCentral.site =@site
		GROUP BY codigoExt
		UNION ALL 
		-- MOEDA EUR
		SELECT 
				codigoExt											AS mPaga,
				@dinheiroEUR										AS valorCont,
				Round(SUM(B_pagCentral.epaga5) * @cambEUR,2)		AS valorContPos,
				'EUR'												AS moedaSistema,
				Round(SUM(B_pagCentral.epaga5),2) 					AS valorContPosMoedaSis,
				0.00												AS comissoes,
				0.00												AS totalDespesas,
				0.00												AS montanteEmitido,
				0.00												AS valorReforcado,
				''													AS bpcnum
		FROM B_pagCentral				(NOLOCK)	
			INNER JOIN B_modoPag_moeda	(NOLOCK)	ON  B_modoPag_moeda.mpStamp = 'ADM007'
			INNER JOIN B_fechoDia		(NOLOCK)	ON (SELECT CONVERT(date, B_fechoDia.dataAbriu)) = @data and B_fechoDia.site = @site
		WHERE  (SELECT CONVERT(date,  B_pagCentral.oData))=@data AND nrAtend NOT LIKE 'CX%' AND B_fechoDia.dataFecho > '1900-01-01 00:00:00.000' AND B_pagCentral.site =@site
		GROUP BY codigoExt
	) AS metodoPAgamentoDia
	WHERE   valorContPos !=0
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosCambio'))
	DROP TABLE #dadosCambio

GO
GRANT EXECUTE ON dbo.x3_servico_fechoCaixaMetodoPaga to Public
GRANT CONTROL ON dbo.x3_servico_fechoCaixaMetodoPaga to Public
GO