/*  Inserir artigo 

	exec x3_servico_InserirALT 
	exec x3_servico_InserirALT '023496','A','4131442','UN'

	SELECT * FROM BC
	DELETE BC
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[x3_servico_InserirALT]') IS NOT NULL
	drop procedure dbo.x3_servico_InserirALT
go

CREATE PROCEDURE [dbo].[x3_servico_InserirALT]
	@referencia		VARCHAR(18),
	@act			VARCHAR(10),
	@codigoALT		VARCHAR (40),
	@unidade		VARCHAR(4) --  para ja nao estamos a utilizar a unidade 
AS
SET NOCOUNT ON
	DECLARE @ststamp VARCHAR(25),@designacao VARCHAR(100)
	DECLARE @desgin VARCHAR(1000)
	DECLARE @bcstamp VARCHAR(25)
	DECLARE @site VARCHAR(60)
	DECLARE @site_nr INT
	DECLARE @ref  VARCHAR(18)
	if(@act='A')
	BEGIN
		DECLARE emp_cursor CURSOR FOR
		SELECT no AS site_nr, site FROM empresa (NOLOCK)
		OPEN emp_cursor
		FETCH NEXT FROM emp_cursor INTO @site_nr , @site
		WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT @ststamp= ststamp, @designacao = design  FROM ST WHERE ref= @referencia  and site_nr = @site_nr
			IF (@ststamp IS NOT NULL)
			BEGIN
				IF(EXISTS(SELECT 1 FROM bc (NOLOCK) WHERE codigo = @codigoALT  AND site_nr = site_nr AND ref != @referencia ))
				BEGIN
						DECLARE @newdesgin VARCHAR(100)
							DECLARE @newStstamp VARCHAR(100)
					SELECT @ref = REF, @bcstamp=bcstamp from bc WHERE codigo = @codigoALT AND site_nr = site_nr 
					select top 1 @newStstamp=ststamp,@newdesgin= design from st (nolock) where ref= @referencia AND site_nr = site_nr 
					update bc  set  REF=@referencia , ststamp=@newStstamp , design=@newdesgin   WHERE codigo = @codigoALT AND site_nr = site_nr 
				END
				ELSE IF (NOT EXISTS(SELECT 1 FROM bc (NOLOCK) WHERE codigo = @codigoALT  AND site_nr = @site_nr AND ref = @referencia ))
				BEGIN
					SET @bcstamp = LEFT(NEWID(),25)
					INSERT INTO bc (bcstamp , ref, design , codigo , ststamp , ousrdata , ousrhora,site_nr,qtt) 
					VALUES (@bcstamp,@referencia,@designacao,@codigoALT,@ststamp,(SELECT CONVERT(date, getdate())),(SELECT CONVERT(VARCHAR(8), GETDATE(), 114)),@site_nr,1)
					SET @desgin = 'A Referência alternativa: ' + @codigoALT +' foi inserida com sucesso'
					EXEC LogsExternal.dbo.x3_servico_Inserirx3_log 'INSERIDO','X3INTALT' ,@desgin, 'bc', @bcstamp,@site
				END
			END 
			ELSE
			BEGIN 
					SET @desgin = 'A Referência: ' +@referencia +' não existe nos artigos.' + 'Referência alternativa: ' + @codigoALT
					EXEC LogsExternal.dbo.x3_servico_Inserirx3_log 'ERRO','X3INTALT' ,@desgin, 'bc', '',@site
			END 
			FETCH NEXT FROM emp_cursor INTO  @site_nr , @site
		END
		CLOSE emp_cursor
		DEALLOCATE emp_cursor
	END
	ELSE
	BEGIN
		DELETE FROM BC WHERE ref = @referencia AND codigo=@codigoALT
		SET @desgin = 'A Referência alternativa: ' + @codigoALT +' foi apagada dos alternativos do artigo com a Referência ' + @referencia 
		EXEC LogsExternal.dbo.x3_servico_Inserirx3_log 'Actualizado','X3INTALT' ,@desgin, 'bc', '',''
	END

GO
GRANT EXECUTE ON dbo.x3_servico_InserirALT to Public
GRANT CONTROL ON dbo.x3_servico_InserirALT to Public
GO