/* 
	exec servico_fechoCaixaSaidas 'BAIXA','20191216'
		exec servico_fechoCaixaSaidas 'ATLANTICO','20200114'
	SELECT * FROM SS WHERE DATA
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[servico_fechoCaixaSaidas]') IS NOT NULL
	drop procedure dbo.servico_fechoCaixaSaidas
go

CREATE PROCEDURE [dbo].[servico_fechoCaixaSaidas]
	@site				varchar(20),
	@data				date
AS
SET NOCOUNT ON
	
IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosCambio'))
		DROP TABLE #dadosCambio

	create table #dadosCambio(
		cambio NUMERIC(19,12)
	)

	DECLARE @moedaSistema VARCHAR(3)
	DECLARE @cambEUR   NUMERIC(19,12)
	DECLARE @cambDOL   NUMERIC(19,12)

	SET @moedaSistema = (SELECT textValue from B_Parameters_site where stamp='ADM0000000003' and site = @site)
	
	INSERT #dadosCambio
	EXEC  up_cambioEntreMoedas @moedaSistema ,'EUR',@data
	SET @cambEUR = (SELECT TOP 1 cambio from #dadosCambio)

	DELETE #dadosCambio

	INSERT #dadosCambio
	EXEC  up_cambioEntreMoedas @moedaSistema ,'USD',@data
	SET @cambDOL = (SELECT TOP 1 cambio from #dadosCambio)

	SELECT 
		nrMotivoMov	+ '-' +	MotivesCashMovements.descr																							'codigoDesc',
		CONCAT(YEAR(oData) , REPLACE(B_pagCentral.nrAtend, 'CX',''))																						'numDoc'  ,                  
		''																																		'numDocOri',
		SUM(ABS(ISNULL(B_pagCentral.evdinheiro,0)) + ABS(ISNULL(B_pagCentral.epaga2,0)) + ABS(ISNULL(B_pagCentral.epaga1,0))
		+ABS(ISNULL(B_pagCentral.epaga3,0))+ Round(ABS(ISNULL(B_pagCentral.epaga4,0)) * @cambDOL,2) 
		+ Round(ABS(ISNULL(B_pagCentral.epaga5,0)) * @cambEUR,2))																				'valor',
		''																																		'outros'
	FROM B_pagCentral
	INNER JOIN MotivesCashMovements ON B_pagCentral.nrMotivoMov = MotivesCashMovements.ID
	WHERE nrAtend like 'CX%' AND B_pagCentral.nrMotivoMov !='' AND  B_pagCentral.Total <0 AND 
	(SELECT CONVERT(date,  B_pagCentral.oData))=@data AND B_pagCentral.site =@site 
	GROUP BY nrMotivoMov ,MotivesCashMovements.descr,CONCAT(YEAR(oData) , 	REPLACE(B_pagCentral.nrAtend, 'CX','')		)
	ORDER BY nrMotivoMov ASC


	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosCambio'))
	DROP TABLE #dadosCambio

GO 
GRANT EXECUTE ON dbo.servico_fechoCaixaSaidas to Public
GRANT CONTROL ON dbo.servico_fechoCaixaSaidas to Public
GO


