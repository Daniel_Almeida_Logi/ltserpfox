/*  Inserir artigo 

	exec x3_servico_InserirPVP  '002102', 1, '002102', 9.0,'USD' , 1
	exec x3_servico_InserirPVP '014385',0,'014385',24.05,'USD',1
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[x3_servico_InserirPVP]') IS NOT NULL
	drop procedure dbo.x3_servico_InserirPVP
go

CREATE PROCEDURE [dbo].[x3_servico_InserirPVP]
	@referencia		VARCHAR(18),
	@inativo		BIT,
	@codigoBarras	VARCHAR (40),
	@montante       NUMERIC(19,6),
	@moeda          VARCHAR(3),
	@codTarifa      VARCHAR(20),
	@incluiva		BIT = 1
AS
SET NOCOUNT ON
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosCambio'))
		DROP TABLE #dadosCambio
	create table #dadosCambio(
		cambio NUMERIC(19,12)
	)
	DECLARE @ststamp VARCHAR(25),@designacao VARCHAR(100) 
	DECLARE @desgin VARCHAR(1000)
	DECLARE @epv1Anterior NUMERIC(19,6)
	DECLARE @site_nr INT
	DECLARE @site VARCHAR(60)
	DECLARE @Data DATETIME
	DECLARE @histprecos_refStamp VARCHAR(36)
	DECLARE @montanteMoedaSistema NUMERIC(19,6)
	DECLARE @moedaSistema VARCHAR(3)
	Declare @cambio  NUMERIC(19,12)
	DECLARE @bcstamp VARCHAR(25)
	DECLARE @ref  VARCHAR(18)
	DECLARE @taxasivaPred  NUMERIC(5,0) 
	DECLARE @usrinis VARCHAR(30)
	DECLARE @usrdata DATETIME
	DECLARE @usrhora VARCHAR(8)
	set @taxasivaPred =2
	IF(@codTarifa='010005')
	BEGIN
		set @taxasivaPred =1
	END
	SET	@usrinis  = 'ADM'
	SET	@usrdata  = (SELECT CONVERT(date, getdate()))
	SET	@usrhora  = (SELECT CONVERT(VARCHAR(8), GETDATE(), 114))
	DECLARE emp_cursor CURSOR FOR
		SELECT no, site AS site_nr FROM empresa (NOLOCK) where taxasivaPred=@taxasivaPred
	OPEN emp_cursor
	FETCH NEXT FROM emp_cursor INTO @site_nr , @site
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @moedaSistema = (SELECT textValue from B_Parameters_site where stamp='ADM0000000003' and site = @site)
		set @Data = getdate()
		SET @montanteMoedaSistema = 0
		INSERT #dadosCambio
		EXEC up_cambioEntreMoedas @moeda, @moedaSistema ,@Data
		IF ((SELECT count(cambio) from #dadosCambio)=0)
		BEGIN 
			SET @desgin = 'O artigo com a Referência: ' + @referencia + ' não foi inserido pois não existe cambio entre a moeda : ' + @moeda + ' para a moeda: ' + @moedaSistema + ' na loja: ' + @site
			exec LogsExternal.dbo.x3_servico_Inserirx3_log 'ERRO','X3INTPVP',@desgin, 'CB','',@site
		END 
		ELSE 
		BEGIN
			SET @cambio = (SELECT TOP 1 cambio from #dadosCambio)
			SET @montanteMoedaSistema = Round((@montante * @cambio),2)
			IF(ISNULL((SELECT ISNULL(epv1,0) FROM st (NOLOCK) WHERE ref = @referencia AND site_nr = @site_nr ),0) != Round(@montanteMoedaSistema,2))
			BEGIN
			SELECT @epv1Anterior=epv1,@ststamp = ststamp FROM st (NOLOCK) WHERE  ref = @referencia AND site_nr = site_nr
				UPDATE   st 
					SET  
						inactivo		= @inativo,
						epv1			= Round(@montanteMoedaSistema,2),
						ivaincl			= @incluiva,
						epv1Ext			= @montante,
						epv1ExtMoeda	= @moeda,
						usrinis			= @usrinis,
						usrdata			= @usrdata,
						usrhora			= @usrhora
				WHERE ref = @referencia AND site_nr = @site_nr
				SET @desgin = 'O artigo com a Referência: ' + @referencia + ''  +' na loja: ' + @site
				exec LogsExternal.dbo.x3_servico_Inserirx3_log 'ATUALIZADO','X3INTPVP',@desgin, 'st',@ststamp,@site
				SET @histprecos_refStamp = LEFT(NEWID(),36)
				IF(@epv1Anterior !=@montanteMoedaSistema)
				BEGIN
					INSERT INTO histprecos_ref (stamp,ref,preco_anterior,preco_novo,ousrdata,site_nr)
					VALUES (@histprecos_refStamp,@referencia,@epv1Anterior,@montanteMoedaSistema,GETDATE(),@site_nr)
					SET @desgin = 'Inserido com sucesso o novo preço no histórico de preços com a Referência: ' + @referencia + ' na loja: ' + @site
					exec LogsExternal.dbo.x3_servico_Inserirx3_log 'INSERIDO','X3INTPVP',@desgin, 'histprecos_ref' , @histprecos_refStamp,@site
				END
				IF (@codigoBarras !='' and @codigoBarras !=@referencia)
				BEGIN
					SELECT @ststamp= ststamp, @designacao = design  FROM ST (NOLOCK) WHERE ref= @referencia  and site_nr = @site_nr
					IF (NOT EXISTS(SELECT 1 FROM bc (NOLOCK) WHERE codigo = @codigoBarras  AND site_nr = site_nr AND ref = @referencia )) 
					BEGIN
						INSERT INTO bc (bcstamp , ref, design , codigo , ststamp , ousrdata , ousrhora,site_nr,qtt) 
						VALUES (LEFT(NEWID(),25),@referencia,@designacao,@codigoBarras,@ststamp,(SELECT CONVERT(date, getdate())),(SELECT CONVERT(VARCHAR(8), GETDATE(), 114)),@site_nr,1)
					END
					ELSE IF(EXISTS(SELECT 1 FROM bc (NOLOCK) WHERE codigo = @codigoBarras  AND site_nr = site_nr AND ref != @referencia ))
					BEGIN 
						SELECT @ref = REF, @bcstamp=bcstamp from bc (NOLOCK) WHERE codigo = @codigoBarras AND site_nr = site_nr 
						SET @desgin = 'A Referência alternativa: ' + @codigoBarras +' já foi inserida para a referencia '+ @ref + " logo não foi inserida para a referência " +@referencia
						EXEC LogsExternal.dbo.x3_servico_Inserirx3_log 'ERRO','X3INTALT' ,@desgin , 'bc',@bcstamp,@site
					END
				END 
			END 
			ELSE
			BEGIN
				SET @desgin ='A Referência: ' + @referencia + ' não existe na loja: ' + @site
				exec LogsExternal.dbo.x3_servico_Inserirx3_log 'ERRO','X3INTPVP',@desgin,'st','',@site 
			END
			DELETE #dadosCambio
		END
		FETCH NEXT FROM emp_cursor INTO @site_nr , @site   
	END 
	CLOSE emp_cursor
	DEALLOCATE emp_cursor
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosCambio'))
		DROP TABLE #dadosCambio

GO
GRANT EXECUTE ON dbo.x3_servico_InserirPVP to Public
GRANT CONTROL ON dbo.x3_servico_InserirPVP to Public
GO