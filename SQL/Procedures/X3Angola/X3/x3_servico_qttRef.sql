/*  Inser 

	exec x3_servico_qttRef 
	
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[x3_servico_qttRef]') IS NOT NULL
	drop procedure dbo.x3_servico_qttRef
go

CREATE PROCEDURE [dbo].[x3_servico_qttRef]
	@token				VARCHAR(36),
	@nameservice		VARCHAR(40),
	@ref				VARCHAR(18),
	@qtt				NUMERIC(11,2),
	@unidade			VARCHAR(40),
	@message			VARCHAR(254)=''
AS
SET NOCOUNT ON
	DECLARE @qttembal NUMERIC(10,2)
	DECLARE @desgin VARCHAR(1000)
	IF(@unidade = 'UN')
	BEGIN
		SELECT @qttembal =qttembal FROM ST WHERE ref= @ref
	END
	ELSE
	BEGIN -- caso nao seja caixas não precisamos de multiplicar pela quantidade da caixa 
		SET  @qttembal = 1
	END  
	IF (@qttembal=0)
	BEGIN 
		SET @desgin = 'A quantidade por embalagem está a zero, que faz com que a quantidade de comprimidos fique a zero. O valor enviado pelo x3 é ' + @qtt
		SET @message = @desgin
		EXEC LogsExternal.dbo.x3_servico_Inserirx3_log 'AVISO','X3INTSTO',@desgin,'ExternalCommunication', token,''
	END 
	SET @qtt = @qtt * @qttembal
	IF(NOT EXISTS(SELECT TOP 1 1 FROM ExternalCommunication (NOLOCK) WHERE token = @token))
	BEGIN
		INSERT INTO ExternalCommunication(token,name,ref,qtt,message) 
		values(@token,@nameservice,@ref,@qtt,@message)
		EXEC LogsExternal.dbo.x3_servico_Inserirx3_log 'INSERIDO','X3INTSTO','Foi recebido o stock com sucesso.','ExternalCommunication', token,''
	END
GO 
GRANT EXECUTE ON dbo.x3_servico_qttRef to Public
GRANT CONTROL ON dbo.x3_servico_qttRef to Public
GO
