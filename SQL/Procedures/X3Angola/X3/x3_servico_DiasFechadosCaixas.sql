/* 
	exec x3_servico_DiasFechadosCaixas 'ATLANTICO'
	SELECT * FROM SS WHERE DATA
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[x3_servico_DiasFechadosCaixas]') IS NOT NULL
	drop procedure dbo.x3_servico_DiasFechadosCaixas
go

CREATE PROCEDURE [dbo].[x3_servico_DiasFechadosCaixas]
	@site				varchar(20)
AS
SET NOCOUNT ON
	
	SELECT 
		DISTINCT (CONVERT(VARCHAR(10), dabrir, 112)) AS date 
	FROM ss (NOLOCK)
	INNER JOIN B_fechoDia	(NOLOCK) ON (SELECT CONVERT(date, B_fechoDia.dataAbriu)) = (SELECT CONVERT(date, ss.dabrir)) and ss.site= B_fechoDia.site
	WHERE exportado = 0 and ss.SITE =@site AND B_fechoDia.site=@site  AND fechado=1
	ORDER BY date ASC  

GO 
GRANT EXECUTE ON dbo.x3_servico_DiasFechadosCaixas to Public
GRANT CONTROL ON dbo.x3_servico_DiasFechadosCaixas to Public
GO
