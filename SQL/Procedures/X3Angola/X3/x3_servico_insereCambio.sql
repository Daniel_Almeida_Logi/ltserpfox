/*  Inserir artigo 

	exec x3_servico_insereCambio 'AOA', 'EUR', '20130501', 0.00787
	exec x3_servico_insereCambio 'AOA','ZAR','20190701',0.038461536
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[x3_servico_insereCambio]') IS NOT NULL
	drop procedure dbo.x3_servico_insereCambio
go

CREATE PROCEDURE [dbo].[x3_servico_insereCambio]
	@moedaOrigem		VARCHAR(3),
	@moedaDestino		VARCHAR(3),
	@date               DATETIME,
	@Cambio				NUMERIC(19,12)

AS
SET NOCOUNT ON
	DECLARE @desgin VARCHAR(1000)
	DECLARE @cbstamp VARCHAR(25)
	SET @cbstamp = LEFT(NEWID(),25)


	IF(NOT EXISTS(SELECT * FROM CB WHERE moeda = @moedaDestino AND moedaOrigem =@moedaOrigem AND data = @date and cambio =@Cambio))
	BEGIN
		INSERT INTO CB (cbstamp,pais,moeda,data,cambio,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora, moedaOrigem)
		VALUES(@cbstamp,@moedaDestino,@moedaDestino,@date,@Cambio,'ADM',(SELECT CONVERT(date, getdate())),(SELECT CONVERT(VARCHAR(8), GETDATE(), 114)),'ADM',(SELECT CONVERT(date, getdate())),(SELECT CONVERT(VARCHAR(8), GETDATE(), 114)),@moedaOrigem)
		
		SET @desgin = 'Inserido Câmbio com sucesso a moeda de origem: ' + @moedaOrigem + " e moeda destino: "+  @moedaDestino + ' data: '+ CONVERT(VARCHAR(50),@date)
		EXEC LogsExternal.dbo.x3_servico_Inserirx3_log 'INSERIDO','X3INTDIV',@desgin, 'CB',@cbstamp,''

	END

GO
GRANT EXECUTE ON dbo.x3_servico_insereCambio to Public
GRANT CONTROL ON dbo.x3_servico_insereCambio to Public
GO

