/*  da o combio entre duas moeda que esta a data que inserimos  

	exec up_cambioEntreMoedas '','EUR','2019-11-20'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cambioEntreMoedas]') IS NOT NULL
	drop procedure dbo.up_cambioEntreMoedas
go

CREATE PROCEDURE [dbo].[up_cambioEntreMoedas]
	@moedaOrigem		VARCHAR(3),
	@moedaDestino		Varchar(3),
	@data				DATETIME
AS
SET NOCOUNT ON

SELECT TOP 1
	cambio
FROM CB(nolock)
WHERE CB.moedaOrigem= @moedaOrigem and CB.moeda = @moedaDestino
		AND DATA <= @data
ORDER BY [data] DESC,
		 [usrdata] DESC,
		 [usrhora] DESC


GO
GRANT EXECUTE ON dbo.up_cambioEntreMoedas to Public
GRANT CONTROL ON dbo.up_cambioEntreMoedas to Public
GO