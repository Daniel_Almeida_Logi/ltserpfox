
/* 



 exec dbo.GerarCodigoUnicocptpla


*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[gerarCodigoUnicocptpla]') IS NOT NULL
	drop procedure dbo.gerarCodigoUnicocptpla
go

CREATE PROCEDURE  [dbo].[gerarCodigoUnicocptpla]
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @NovoCodigo VARCHAR(3);

    -- Gera um c�digo aleat�rio
    SET @NovoCodigo = 
        CHAR(65 + (ABS(CHECKSUM(NEWID())) % 26)) +
        CHAR(65 + (ABS(CHECKSUM(NEWID(), NEWID())) % 26)) +
        CHAR(65 + (ABS(CHECKSUM(NEWID(), NEWID(), NEWID())) % 26));

    -- Verifica se o c�digo j� existe na tabela
    WHILE EXISTS (SELECT 1 FROM cptpla WHERE codigo = @NovoCodigo)
    BEGIN
        -- Se existir, gera um novo c�digo
        SET @NovoCodigo = 
            CHAR(65 + (ABS(CHECKSUM(@NovoCodigo + CAST(NEWID() AS VARCHAR(MAX)))) % 26)) +
            CHAR(65 + (ABS(CHECKSUM(@NovoCodigo + CAST(NEWID() AS VARCHAR(MAX)))) % 26)) +
            CHAR(65 + (ABS(CHECKSUM(@NovoCodigo + CAST(NEWID() AS VARCHAR(MAX)))) % 26));
    END

    select  @NovoCodigo as codigo -- Exibe o novo c�digo gerado
END;

					
GO
GRANT EXECUTE ON dbo.gerarCodigoUnicocptpla to Public
GRANT CONTROL ON dbo.gerarCodigoUnicocptpla to Public
GO