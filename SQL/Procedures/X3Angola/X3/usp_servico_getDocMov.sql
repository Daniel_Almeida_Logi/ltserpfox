/*  dado um tipo de documento para integrar , retorna a origem e destino consoante o documento . Se não tiver esse movimento retorna erro pois nao esta configurado 
	exec usp_servico_getDocMov  'DR' ,'F01','F02'
	exec usp_servico_getDocMov  'E' ,'F01','F02'
	exec usp_servico_getDocMov  'T' ,'F01','F02'
	exec usp_servico_getDocMov  'R' ,'F01','F02'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[usp_servico_getDocMov]') IS NOT NULL
	drop procedure dbo.usp_servico_getDocMov
go

CREATE PROCEDURE [dbo].[usp_servico_getDocMov]
	@docT	        VARCHAR(36),
	@ori			VARCHAR(36),
	@dest			VARCHAR(36)
	
AS
SET NOCOUNT ON
	DECLARE @mov		varchar(100)
	DECLARE @movDescr	varchar(100)

	IF(EXISTS(SELECT 1 FROM docMov where docType =@docT AND active =1))
	BEGIN
		SELECT TOP 1 @mov=mov, @movDescr = movDescr FROM docMov where docType =@docT AND active =1
		

		IF(@mov ='ORI')
		BEGIN
			SET @dest =''
		END
		ELSE if (@mov ='DEST') 
		BEGIN
			SET @ori =''
		END
		ELSE if (@mov ='S') 
		BEGIN
			DECLARE @swap VARCHAR(100)
			SET @swap	= @ori
			SET @ori	= @dest
			SET @dest	= @swap
		END
		ELSE if (@mov ='SWAPD') 
		BEGIN
			SET @ori	= @dest
			SET @dest	= ''
		END
		ELSE if (@mov ='SWAPO') 
		BEGIN
			SET @ori	= ''
			SET @dest	= @ori
		END

	END
	ELSE
	BEGIN
		SET @ori = ''
		SET @dest = ''
		SET @mov ='E'
		SET @movDescr = 'Não existe este movimento para o documento definido'		
	END 

	SELECT 
			@ori		AS ori ,
			@dest		AS dest ,
			@mov		AS mov,
			@movDescr	AS movDescr
		
GO
GRANT EXECUTE ON dbo.usp_servico_getDocMov to Public
GRANT CONTROL ON dbo.usp_servico_getDocMov to Public
GO