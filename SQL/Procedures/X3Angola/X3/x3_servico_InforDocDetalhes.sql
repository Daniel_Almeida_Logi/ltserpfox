/*  informacao dos artigos para documento no x3 
	exec x3_servico_InforDocDetalhes  
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[x3_servico_InforDocDetalhes]') IS NOT NULL
	drop procedure dbo.x3_servico_InforDocDetalhes
go

CREATE  PROCEDURE [dbo].[x3_servico_InforDocDetalhes]
	@ftStamp	        VARCHAR(36),
	@site				VARCHAR(60),
	@DocExtT			VARCHAR(10)
	
AS
SET NOCOUNT ON

DECLARE @noEstabelecimento	VARCHAR(30)
DECLARE @DocOrigem			VARCHAR(20)
DECLARE @ofistamp			VARCHAR(25)
DECLARE @ftStampOri			VARCHAR(25)
DECLARE @ftStampOriSeg		VARCHAR(25)
DECLARE @c2codpost			VARCHAR(45)
DECLARE @nomeBene			VARCHAR(80)
DECLARE @Benef				VARCHAR(120)

SELECT @noEstabelecimento = siteExt FROM EMPRESA (NOLOCK) WHERE site = @site

SET @DocOrigem=''
IF(@DocExtT='CLJ' OR @DocExtT ='CCO' OR @DocExtT ='DLJ')
BEGIN
	SELECT TOP 1 @ofistamp=LTRIM(RTRIM(ofistamp)) FROM FI (NOLOCK) WHERE ftstamp = @ftStamp and ofistamp !=''

	IF(@ofistamp!='')
	BEGIN
		SELECT TOP 1 @ftStampOri=ftstamp FROM FI (NOLOCK) WHERE fistamp = @ofistamp

		SELECT 
			@DocOrigem = TD.nmdocExt +'-'+ @noEstabelecimento + '-' + SUBSTRING(convert(varchar,ft.ftano),3, 4)  + '-'
		+  RIGHT('00000'+ISNULL(CONVERT(varchar,FT.fno),'0'),6)
		FROM FT (NOLOCK)
			INNER JOIN TD        (NOLOCK) ON FT.ndoc = TD.ndoc
		WHERE  FT.ftstamp =@ftStampOri AND FT.site = @site
	END
	ELSE
	BEGIN
		SELECT 
			@DocOrigem = FT2.u_docorig
		FROM FT (NOLOCK)
			LEFT JOIN FT2 (NOLOCK) ON FT.ftstamp	= FT2.ft2stamp
			INNER JOIN TD        (NOLOCK) ON FT.ndoc = TD.ndoc
		WHERE  FT.ftstamp =@ftStamp AND FT.site = @site
	END
	 		
END

set @Benef=''
IF(@DocExtT='FCO' OR @DocExtT ='CCO')
BEGIN
	SELECT @ftStampOriSeg = stamporigproc, @c2codpost=c2codpost  FROM ft2 (NOLOCK) WHERE ft2stamp = @ftStamp
	if(@ftStampOriSeg !='')
	BEGIN
		SELECT @nomeBene=nome  FROM FT (NOLOCK) WHERE ftstamp = @ftStampOriSeg	
		set @Benef ='Beneficiario: ' + @nomeBene  + ' N.º ' + @c2codpost
	END 
	ELSE
	BEGIN 
		SELECT @nomeBene=contacto  FROM ft2 (NOLOCK) WHERE ft2stamp = @ftStamp	
		set @Benef ='Beneficiario: ' + @nomeBene  + ' N.º ' + @c2codpost

	END

END


SELECT 
	  @noEstabelecimento																											AS nEstabelecimento 
	, TD.nmdocExt																													AS tipoDoc
	, TD.nmdocExt +'-'+ @noEstabelecimento + '-' + SUBSTRING(convert(varchar,ft.ftano),3, 4)  + '-'
	+  RIGHT('00000'+ISNULL(CONVERT(varchar,FT.fno),'0'),6) 																		AS nDoc 
	,   isnull(TD.tiposaft,'FT') + '/' + convert(varchar,FT.ndoc) 
	+ (case when ft.ftano=2020 then '' else convert(varchar,FT.ftano) end )  + '/' + CONVERT(varchar,FT.fno)						AS refDoc -- sistema de retalho
	, (SELECT CONVERT(varchar, fdata,112))																							AS dataDoc 
	, CASE WHEN (B_UTENTES.no_ext ='' or B_UTENTES.no_ext=NULL or B_UTENTES.no_ext='0') THEN 'ZCLFIN' ELSE	B_UTENTES.no_ext END	AS nCliente 
	, @noEstabelecimento																											AS rep
	, 2																																AS idTaxaPreco -- enviar sempre valor dois
	, FT.moeda																														AS idCur
	, CASE WHEN (TD.lancasl=1) THEN 2 ELSE 1 END																					AS stomflg -- enviar sempre valor dois
	, @DocOrigem																													AS documentoOrigem
	, @Benef																														AS dadosBeneficiario 
FROM FT	(NOLOCK)
	LEFT JOIN B_UTENTES (NOLOCK) ON FT.no	= B_UTENTES.no
	INNER JOIN TD        (NOLOCK) ON FT.ndoc = TD.ndoc
WHERE  FT.ftstamp =@ftStamp AND FT.site = @site


