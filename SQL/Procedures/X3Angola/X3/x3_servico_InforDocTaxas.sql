/*  informacao das taxas para gravar documento no x3 
	exec x3_servico_InforDocTaxas 'ADMB5B69E2D-FC12-40CE-A6F'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[x3_servico_InforDocTaxas]') IS NOT NULL
	drop procedure dbo.x3_servico_InforDocTaxas
go

CREATE PROCEDURE [dbo].[x3_servico_InforDocTaxas]
	@ftStamp	        VARCHAR(36)
	
AS
SET NOCOUNT ON

	SELECT 
		 taxasiva.u_cod																																	                       AS codigoTaxa
	   , Cast(SUM(CASE WHEN fi.ivaincl = 1 then Round((ABS(fi.etiliquido) / (ABS(fi.iva)/100+1)),2) ELSE Round(ABS(fi.etiliquido),2) END)AS DECIMAL(20,2))			           AS montanteTotalTaxaSiva
	   , Cast(SUM(CASE WHEN fi.ivaincl = 1 then	Round(ABS(fi.etiliquido) - (ABS(fi.etiliquido) / (ABS(fi.iva)/100+1)),2 )ELSE                                             
				Round(fi.etiliquido + (ABS(fi.etiliquido)*(fi.iva/100)) - ABS(fi.etiliquido),2) END)AS DECIMAL(20,2))												           AS montanteTaxa
	   , Cast(SUM(CASE WHEN fi.ivaincl = 1 THEN Round(ABS(fi.etiliquido),2) ELSE Round(ABS(fi.etiliquido) + (ABS(fi.etiliquido)*(ABS(fi.iva)/100)),2) END)AS DECIMAL(20,2))	   AS montanteTotalTaxaCiva
	FROM FI (NOLOCK)
		INNER JOIN taxasiva (NOLOCK) ON FI.tabiva = taxasiva.codigo
	WHERE  FI.ftstamp =@ftStamp and fi.ref<>'V999999'
	GROUP BY taxasiva.u_cod 




GO
GRANT EXECUTE ON dbo.x3_servico_InforDocTaxas to Public
GRANT CONTROL ON dbo.x3_servico_InforDocTaxas to Public
GO