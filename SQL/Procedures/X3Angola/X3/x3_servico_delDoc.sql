SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
-- apaga um documento que foi importado do x3 .
if OBJECT_ID('[dbo].[x3_servico_delDoc]') IS NOT NULL
	drop procedure dbo.x3_servico_delDoc
go

CREATE PROCEDURE [dbo].[x3_servico_delDoc]
	@codDocExt      Varchar(60)
	
AS
SET NOCOUNT ON
	DECLARE @boStamp VARCHAR(25)

	IF(EXISTS(SELECT * FROM BO2 WHERE codext =@codDocExt))
	BEGIN
		SELECT @boStamp = bo2stamp FROM BO2 WHERE codext =@codDocExt
		DELETE BI	WHERE bostamp  = @boStamp
		DELETE BI2	WHERE bostamp  = @boStamp
		DELETE BO	WHERE bostamp  =  @boStamp
		DELETE BO2	WHERE bo2stamp =  @boStamp
	END 

GO
GRANT EXECUTE ON dbo.x3_servico_delDoc to Public
GRANT CONTROL ON dbo.x3_servico_delDoc to Public
GO