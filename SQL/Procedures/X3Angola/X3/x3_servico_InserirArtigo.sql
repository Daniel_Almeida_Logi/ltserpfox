/*  Inserir artigo 

	exec x3_servico_InserirArtigo '014987',1,'VET CINTO EQUITACAO','UN','NOR',46,'Laboratorio B A Farma  Lda','0','014987'
	exec x3_servico_InserirArtigo'022921',0,'PROZIS 2 WEEK CUT   BURN 45 CAPS   DAY','UN','EXO',7067,'BodyFit  Lda','0','022921',45.0,45.0
	exec x3_servico_InserirArtigo'010949',1,'PANTOPRAZOL 40MG 56 COMP','UN','ISE2',64,'Mercafar Dist  Farmace  S A','5038229','010949',14.0,56.0,0

	exec x3_servico_InserirArtigo'015127',1,'FILTRO ANTIBACTERIANO ADULTO','UN','NOR',95,'Visecomdata   Medical Supplier  Lda','0','015127',1.0,0.0,false

	exec x3_servico_InserirArtigo'009254',0,'BBC PORTA REFEICOES TERMICO','UN','ESP',23,'Dorel France  S A','0','009254',1.0,1.0,false,MB06
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[x3_servico_InserirArtigo]') IS NOT NULL
	drop procedure dbo.x3_servico_InserirArtigo
go

CREATE PROCEDURE [dbo].[x3_servico_InserirArtigo]
	@referencia		VARCHAR(18),
	@inativo		BIT,
	@designacao     VARCHAR(100),
	@unidade		VARCHAR(4),
	@codIva			VARCHAR(40),
	@fornec			NUMERIC(9,1),
	@fornecedor		VARCHAR(80),
	@codigoCNP		VARCHAR (40),
	@codigoBarras	VARCHAR (40),
	@qttminvd       NUMERIC (10,2),
	@qttembal       NUMERIC (10,2),
	@serv           BIT,
	@famCodigo		VARCHAR (254)='',
	@farm7			varchar(254)='',
	@farm8			varchar(254)='',
	@obs			varchar(254)='',
	@tsicod4		varchar(18)='',
	@tsicod4desc	varchar(60)='',
	@ycontrsub		varchar(254)='',
	@ycontrsubDesc	varchar(254)='',
	@dciId			varchar(10)='',
	@dciDesc		varchar(254)=''
AS
SET NOCOUNT ON
	DECLARE @ststamp VARCHAR(25)
	DECLARE @desgin  VARCHAR(1000)
	DECLARE @tabiva  NUMERIC(5,1)
	DECLARE @taxaPred NUMERIC(5,0)
	DECLARE @site VARCHAR(60)
	DECLARE @bcstamp VARCHAR(25)
	DECLARE @ref  VARCHAR(18)


	DECLARE @ousrinis VARCHAR(30)
	DECLARE @ousrdata DATETIME
	DECLARE @ousrhora VARCHAR(8)
	DECLARE @usrinis VARCHAR(30)
	DECLARE @usrdata DATETIME
	DECLARE @usrhora VARCHAR(8)
	DECLARE @famstamp VARCHAR(4)
	DECLARE @psico bit 
	DECLARE @familia VARCHAR(18) 
	DECLARE @faminome VARCHAR(60)
	set @psico=0

	if(@ycontrsub='04')
	begin 
		set @psico=1
	end 

	SET @famstamp=''

	SET @ousrinis = 'ADM'
	SET	@ousrdata = (SELECT CONVERT(date, getdate()))
	SET	@ousrhora = (SELECT CONVERT(VARCHAR(8), GETDATE(), 114))
	SET	@usrinis  = 'ADM'
	SET	@usrdata  = (SELECT CONVERT(date, getdate()))
	SET	@usrhora  = (SELECT CONVERT(VARCHAR(8), GETDATE(), 114))

	IF(@famCodigo !='')
	BEGIN
		IF(not exists(select * from b_famFamilias where design=@famCodigo))
		BEGIN
			SET @famstamp = convert(VARCHAR(4), ISNULL((SELECT MAX(isnull(convert(int,famstamp),0)) +1 from b_famFamilias),1))
			insert into b_famFamilias (famstamp,design,inactivo)
			values (@famstamp,@famCodigo,0)
		END 
		ELSE 
		BEGIN
			SET @famstamp= (select TOP 1 famstamp from b_famFamilias where design=@famCodigo)
		END 
	END

	IF (@tsicod4desc !='' AND EXISTS(SELECT nome FROM stfami WHERE nome = @tsicod4desc))
		BEGIN 


			set @faminome =@tsicod4desc 
			set @familia=(SELECT top 1 ref FROM stfami WHERE nome = @tsicod4desc) 			
		END 
		ELSE IF (@tsicod4desc !='' AND not EXISTS(SELECT nome FROM stfami WHERE nome = @tsicod4desc))
		BEGIN
				insert into stfami (stfamistamp,ref,nome,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,exportado)
				values (LEFT(NEWID(),25),@tsicod4,@tsicod4desc,'ADM',(SELECT CONVERT(date, getdate())),(SELECT CONVERT(VARCHAR(8), GETDATE(), 114)),'ADM',(SELECT CONVERT(date, getdate())),(SELECT CONVERT(VARCHAR(8), GETDATE(), 114)),0)

			set @faminome =@tsicod4desc 
			set @familia=@tsicod4 
		END 
		ELSE
		BEGIN
             SET @familia = "99"
             SET @faminome = "Outros"
		END 


	SET @unidade = 'UNF' -- O logitools trabalha com comprimidos e nao com caixas 

	IF (@fornec != 0)
	BEGIN
		IF NOT EXISTS(SELECT nome FROM fl (NOLOCK) WHERE NO = @fornec)
		BEGIN 
			INSERT INTO fl (flstamp,nome,no)
			VALUES(LEFT(NEWID(),25),@fornecedor,@fornec)
		END
	END 

	DECLARE @site_nr INT
	DECLARE emp_cursor CURSOR FOR
	SELECT no AS site_nr, site FROM empresa (NOLOCK)
	OPEN emp_cursor
	FETCH NEXT FROM emp_cursor INTO @site_nr , @site

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @tabiva = -1

		SELECT @taxaPred = taxasivaPred FROM empresa WHERE no = @site_nr

		SELECT @tabiva =codigo FROM taxasiva WHERE u_codeExt = @codIva and taxasivaPred in (@taxaPred,0)

		IF(@tabiva != -1)
		BEGIN
			IF NOT EXISTS(SELECT ststamp FROM ST (NOLOCK) WHERE ref = @referencia and site_nr = @site_nr)
			BEGIN
				SET @ststamp = LEFT(NEWID(),25)
				DECLARE @tipoCliente VARCHAR(350) ,@u_fonte VARCHAR(1)
				DECLARE @IVAINCL BIT,@ivapcincl BIT ,@iva1incl BIT ,@iva2incl BIT ,@iva3incl BIT ,@iva4incl BIT ,@iva5incl	BIT ,@qlook	BIT , @baixr BIT , @compnovo BIT, @clinica BIT, @sujinv BIT 
				DECLARE @epv1 NUMERIC(13,6),@marg1	NUMERIC(13,3) ,@pcusto NUMERIC(13,5) ,@marg2 NUMERIC(13,3) ,@epcpond NUMERIC(13,6) ,@epcult NUMERIC(13,6) ,@marg3 NUMERIC(13,3), @marg4 NUMERIC(13,3), @conversao NUMERIC(8,7) 
				DECLARE @validade DATETIME
				DECLARE @u_tipoetiq INT
			 
				SELECT @tipoCliente=textValue FROM B_Parameters (NOLOCK) WHERE stamp = 'ADM0000000082'
				IF ((@tipoCliente = '') OR  (@tipoCliente = 'FARMACIA'))
				BEGIN 
					SET @IVAINCL	= 1
					SET @ivapcincl	= 0
					SET @iva1incl	= 1
					SET @iva2incl	= 1
					SET @iva3incl	= 1
					SET @iva4incl	= 1
					SET @iva5incl	= 1
				END
				ELSE
				BEGIN
					SET @IVAINCL	= 0
					SET @ivapcincl	= 0
					SET @iva1incl	= 0
					SET @iva2incl	= 0
					SET @iva3incl	= 0
					SET @iva4incl	= 0
					SET @iva5incl	= 0
				END
				
				SET @u_fonte  = "I"
				SET @qlook    = 0
				SET @baixr    = 0
				SET @compnovo = 0
				SET @clinica  = 0
				SET @sujinv   = 0   
				SET @epv1		= 0
				SET @marg1		= 0
				SET @pcusto		= 0
				SET @marg2		= 0
				SET @epcpond	= 0
				SET @epcult		= 0
				SET @marg3		= 0
				SET @marg4		= 0
				SET @validade   ='1900-01-01' 
				SET @u_tipoetiq = 1
				SET @conversao  = 1 

				INSERT INTO ST(ststamp,ref,inactivo,design,unidade,tabiva,fornec,fornecedor,site_nr,IVAINCL,ivapcincl,iva1incl,iva2incl,iva3incl,iva4incl,iva5incl,epv1,marg1,pcusto,marg2,epcpond,epcult,marg3,marg4,validade ,u_tipoetiq,conversao,qlook,stns,baixr,compnovo,clinica,sujinv,u_fonte,familia,faminome,qttminvd ,qttembal,codCNP,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora, u_famstamp,usr1,u_lab,obs,usr3,usr3desc)
				VALUES (@ststamp,@referencia,@inativo, @designacao,@unidade,@tabiva, @fornec,@fornecedor,@site_nr,@IVAINCL,@ivapcincl,@iva1incl,@iva2incl,@iva3incl,@iva4incl,@iva5incl,@epv1,@marg1,@pcusto,@marg2,@epcpond,@epcult,@marg3,@marg4,@validade ,@u_tipoetiq,@conversao,@qlook,@serv,@baixr,@compnovo,@clinica,@sujinv,@u_fonte,@familia,@faminome,@qttminvd,@qttembal,@codigoCNP,@ousrinis,@ousrdata,@ousrhora,@usrinis,@usrdata,@usrhora,@famstamp,@farm8,@farm7,@obs,@ycontrsub,@ycontrsubDesc)

		
				SET @desgin = 'Inserido artigo com sucesso com a Referência: ' + @referencia
				EXEC LogsExternal.dbo.x3_servico_Inserirx3_log 'INSERIDO','X3INTART' ,@desgin , 'ST', @ststamp,@site
		
			END	
			ELSE 
			BEGIN
				SET @ststamp = (SELECT ststamp FROM ST (NOLOCK) WHERE ref= @referencia  and site_nr = @site_nr)
				UPDATE ST
				SET 
					inactivo	= @inativo,
					design		= @designacao,
					unidade		= @unidade,
					tabiva		= @tabiva,
					fornec		= @fornec,
					fornecedor  = @fornecedor,
					qttminvd    = @qttminvd,
					qttembal    = @qttembal,
					stns		= @serv,
					codCNP		= @codigoCNP,
					usrinis		= @usrinis,
					usrdata		= @usrdata,
					usrhora		= @usrhora,
					u_famstamp	= @famstamp,
					usr1		= @farm8,
					u_lab		= @farm7,
					obs			= @obs,
					familia		= @familia,
					faminome	= @famiNOME,
					usr3		= @ycontrsub,
					usr3desc	= @ycontrsubDesc
				WHERE  ref		= @referencia  and site_nr = @site_nr
			
				SET @desgin = 'O artigo com a Referência: ' + @referencia + ' foi atualizado com sucesso.'
				EXEC LogsExternal.dbo.x3_servico_Inserirx3_log 'ATUALIZADO','X3INTART',@desgin, 'ST', @ststamp,@site

			END 

			IF (@codigoBarras !='' and @codigoBarras !=@referencia)
			BEGIN
			    IF (NOT EXISTS(SELECT 1 FROM bc (NOLOCK) WHERE codigo = @codigoBarras  AND site_nr = site_nr AND ref = @referencia )) 
				BEGIN
					INSERT INTO bc (bcstamp , ref, design , codigo , ststamp , ousrinis ,ousrdata , ousrhora,usrinis,usrdata,usrhora ,site_nr,qtt) 
					VALUES (LEFT(NEWID(),25),@referencia,@designacao,@codigoBarras,@ststamp,'ADM',(SELECT CONVERT(date, getdate())),(SELECT CONVERT(VARCHAR(8), GETDATE(), 114)),'ADM',(SELECT CONVERT(date, getdate())),(SELECT CONVERT(VARCHAR(8), GETDATE(), 114)),@site_nr,1)		
				END
				ELSE IF(EXISTS(SELECT 1 FROM bc (NOLOCK) WHERE codigo = @codigoBarras  AND site_nr = site_nr AND ref != @referencia ))
				BEGIN 
					SELECT @ref = REF, @bcstamp=bcstamp from bc WHERE codigo = @codigoBarras AND site_nr = site_nr 

					SET @desgin = 'A Referência alternativa: ' + @codigoBarras +' já foi inserida para a referencia '+ @ref + " logo não foi inserida para a referência " +@referencia
					EXEC LogsExternal.dbo.x3_servico_Inserirx3_log 'ERRO','X3INTALT' ,@desgin , 'bc',@bcstamp,@site
				END
			END    
		END
		ELSE 
		BEGIN	
			SET @desgin = 'O artigo com a Referência: ' + @referencia + ' tem um CODIVA que não existe ' + @codIva
			EXEC LogsExternal.dbo.x3_servico_Inserirx3_log 'ERRO','X3INTART',@desgin, 'ST','',@site
		END 
		FETCH NEXT FROM emp_cursor INTO  @site_nr , @site
	END
	CLOSE emp_cursor
	DEALLOCATE emp_cursor

	IF ( EXISTS (SELECT cnp from fprod(NOLOCK) WHERE cnp=@codigoCNP) )
	BEGIN 
		IF(EXISTS (SELECT cnp from fprod(NOLOCK) WHERE cnp=@codigoCNP and dcipt_id = @dciId and dci =@dciDesc))
		BEGIN
			update fprod set 
					cnp =@codigoCNP,
					nome =@designacao,
					design =@designacao,
					dcipt_id = @dciId ,
					dci =@dciDesc,
					usrinis='ADM',
					usrdata = (SELECT CONVERT(date, getdate())) ,
					usrhora=(SELECT CONVERT(VARCHAR(8), GETDATE(), 114)), 
					psico =@psico
			where cnp = @codigoCNP
		END
	END 
	ELSE 
	BEGIN 
		INSERT INTO fprod (fprodstamp, cnp,nome,design,grupo,cptgrpstamp,dcipt_id,dci,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,psico)
		VALUES ('ADM' + ltrim(str(@codigoCNP)),@codigoCNP,@designacao,@designacao,'ST','ADMST',@dciId,@dciDesc,'ADM',(SELECT CONVERT(date, getdate())),(SELECT CONVERT(VARCHAR(8), GETDATE(), 114)),'ADM',(SELECT CONVERT(date, getdate())),(SELECT CONVERT(VARCHAR(8), GETDATE(), 114)),@psico) 
	END


GO
GRANT EXECUTE ON dbo.x3_servico_InserirArtigo to Public
GRANT CONTROL ON dbo.x3_servico_InserirArtigo to Public
GO