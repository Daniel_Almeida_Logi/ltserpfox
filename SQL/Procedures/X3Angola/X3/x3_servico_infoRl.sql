-- 
-- exec x3_servico_infoRl 'FLJ','F01',1,2019,2019,'REC-CBF1907/00002',2192.0,2192.0,2192.0,'20190701'
-- 

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[x3_servico_infoRl]') IS NOT NULL
	drop procedure dbo.x3_servico_infoRl
go

CREATE PROCEDURE [dbo].[x3_servico_infoRl]
	@nmDocExt			VARCHAR(30),
	@siteExt			VARCHAR(20),
	@fno				NUMERIC(10,0),
	@ftano				NUMERIC(4,0),
	@anoDoc				NUMERIC(4,0),
	@moedaDoc			VARCHAR(3),
	@valO				NUMERIC(19,6),
	@valD				NUMERIC(19,6),
	@valR				NUMERIC(19,6),
	@datE				DATETIME
AS
SET NOCOUNT ON
	DECLARE @ftstamp VARCHAR(25)
	DECLARE @restamp VARCHAR(25)
	DECLARE @moedaSistema VARCHAR(3)
	Declare @cambio  NUMERIC(19,12)
	DECLARE @site VARCHAR(20)
	Declare @ndoc NUMERIC(6,0)

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosCambio'))
		DROP TABLE #dadosCambio

	create table #dadosCambio(
		cambio NUMERIC(19,12)
	)

	SELECT @site=site FROM empresa WHERE siteExt = @siteExt
	
	SELECT @ndoc=ndoc FROM TD WHERE SITE= @site and nmDocExt = @nmDocExt

	SELECT @ftstamp = ftstamp FROM FT (NOLOCK) WHERE ndoc = @ndoc AND fno = @fno AND  site = @site AND ftano = @ftano

	SET @moedaSistema = (SELECT textValue from B_Parameters_site where stamp='ADM0000000003' and site = @site)

	SET @cambio = 1;
	IF(@moedaDoc !=@moedaSistema)
	BEGIN 
		INSERT #dadosCambio
		EXEC up_cambioEntreMoedas @moedaDoc, @moedaSistema ,@datE

		IF ((SELECT count(cambio) from #dadosCambio)!=0)
		BEGIN 
			SET @cambio = (SELECT TOP 1 cambio from #dadosCambio)
		END
	END

	SELECT 
		cc.ccstamp																				AS ccstamp,
		LEFT(NEWID(),25)																		AS rlstamp,
		6																						AS ndoc,
		datalc																					AS datalc,
		dataven																					AS dataven,
		cc.cm																					AS cm,
		cc.eivav1																				AS eivav1,
		cc.eivav2																				AS eivav2,
		cc.eivav3																				AS eivav3,
		cc.eivav4																				AS eivav4,
		cc.eivav5																				AS eivav5,
		cc.eivav6																				AS eivav6,
		cc.eivav7																				AS eivav7,
		cc.eivav8																				AS eivav8,
		cc.eivav9																				AS eivav9,
		ISNULL(ROUND(@valO *@cambio,2),0)														AS valorO,
		ISNULL(ROUND(@valR *@cambio,2),0)														AS valorR,
		ISNULL(ROUND(@valD *@cambio,2),0)														AS valorD,
		FT.nmdoc																				AS cdesc,
		cc.nrdoc																				AS nrdoc,
		@moedaSistema																			AS moeda
	FROM 
		FT (NOLOCK)
		INNER JOIN cc ON CC.ftstamp = FT.ftstamp
    WHERE FT.ftstamp = @ftstamp

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosCambio'))
	DROP TABLE #dadosCambio
GO
GRANT EXECUTE ON dbo.x3_servico_infoRl to Public
GRANT CONTROL ON dbo.x3_servico_infoRl to Public
GO