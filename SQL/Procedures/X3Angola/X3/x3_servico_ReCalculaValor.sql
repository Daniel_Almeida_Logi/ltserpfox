/*  Inserir artigo 

	-- EXEC x3_servico_ReCalculaValor '20200304'
	SELECT * FROM CB order by data desc
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



if OBJECT_ID('[dbo].[x3_servico_ReCalculaValor]') IS NOT NULL
    drop procedure dbo.x3_servico_ReCalculaValor
go

 

CREATE PROCEDURE [dbo].[x3_servico_ReCalculaValor]
    @data               DATE,
    @manual                bit=0
AS
SET NOCOUNT ON
    DECLARE @desgin VARCHAR(254)
    DECLARE @token VARCHAR(36)
    DECLARE @moedaOrigem VARCHAR(3)
    DECLARE @cambio NUMERIC(19,12)
    DECLARE @erro BIT
    SET @erro=0
    IF (@data = '')
    BEGIN
        SELECT TOP 1 @data = data FROM CB (NoLOCK) ORDER BY [data] DESC,[usrdata] DESC,[usrhora] DESC
    END
    IF (NOT EXISTS(SELECT * FROM ExternalCommunicationExchange (NoLOCK) WHERE date = @data) or @manual =1)
    BEGIN
        SET @token = (SELECT LEFT(NEWID(),36))
        IF(EXISTS(SELECT * FROM CB (NoLOCK) WHERE CONVERT(varchar, data, 112)=CONVERT(varchar, @data, 112)))
        BEGIN    
                DECLARE emp_cursor CURSOR FOR
                SELECT DISTINCT moedaOrigem from CB (NoLOCK) WHERE CONVERT(varchar, data, 112)=CONVERT(varchar, @data, 112) and CB.moeda IN ((SELECT textValue from B_Parameters_site (NoLOCK) where stamp='ADM0000000003' and site = site))
                OPEN emp_cursor
                FETCH NEXT FROM emp_cursor INTO @moedaOrigem     
                WHILE @@FETCH_STATUS = 0
                BEGIN
                    SELECT TOP 1
                        @cambio =    cambio 
                    from CB (NoLOCK)
                    WHERE CONVERT(varchar, cb.data, 112)=CONVERT(varchar, @data, 112) and CB.moeda IN ((SELECT textValue from B_Parameters_site (NOLOCK) where stamp='ADM0000000003' and site = site)) AND CB.moedaOrigem = @moedaOrigem
                    ORDER BY usrdata DESC , usrhora DESC
                    BEGIN TRY
                        BEGIN TRANSACTION; 
                            INSERT INTO histprecos_ref (stamp,ref,preco_anterior,preco_novo,ousrdata,site_nr)
                            SELECT (LEFT(NEWID(),36)),ref,epv1,ROUND(epv1Ext * @cambio,2),GETDATE(),site_nr FROM ST(NOLOCK) WHERE  epv1ExtMoeda = @moedaOrigem
							IF EXISTS(SELECT * FROM sys.objects WHERE [name]='Rep_ST_Control' AND [type]= 'TR')
							BEGIN
								ALTER TABLE [mecofarma].[dbo].[st] DISABLE TRIGGER Rep_ST_Control
							END
							 UPDATE ST SET epv1=  ROUND(epv1Ext * @cambio,2), usrdata =GETDATE() , usrhora = CONVERT(VARCHAR(8), GETDATE(), 114)    WHERE epv1ExtMoeda = @moedaOrigem
							IF EXISTS(SELECT * FROM sys.objects WHERE [name]='Rep_ST_Control' AND [type]= 'TR')
							BEGIN
								ALTER TABLE [mecofarma].[dbo].[st] ENABLE TRIGGER Rep_ST_Control
							END
                        COMMIT TRANSACTION; 
                    END TRY
                    BEGIN CATCH
                            ROLLBACK TRANSACTION
                            set @erro=1
                            SET @desgin = 'Existe novos câmbio mas falhou a fazer update dos produtos. É necessario actualizar os valores !!!!!'         
                            EXEC LogsExternal.dbo.x3_servico_Inserirx3_log 'ERRO','X3INTDIV',@desgin, 'st','',''
                    END CATCH
                    FETCH NEXT FROM emp_cursor INTO @moedaOrigem   
                END 
                CLOSE emp_cursor
                DEALLOCATE emp_cursor
            IF (@erro=0)
            BEGIN
                SET @desgin = 'Inserido com sucesso o novos preços no histórico de preços devido a mudança de cambio '
                exec LogsExternal.dbo.x3_servico_Inserirx3_log 'INSERIDO','X3INTDIV',@desgin, 'histprecos_ref' ,'',''
                SET @desgin = 'O Serviço de calculo do valor de preço da tabela ST foi chamado e foi actulizado com o novo câmbio'         
                EXEC x3_Inserir_x3_CommunicationExchange @token,@data ,@manual, @desgin
                EXEC LogsExternal.dbo.x3_servico_Inserirx3_log 'ATUALIZADO','X3INTDIV',@desgin, 'st','',''
            END
        END
        ELSE
        BEGIN     
            SET @desgin = 'O Serviço de calculo do valor de preço da tabela ST foi chamado mas não existia novo câmbio neste momento.'             
            EXEC x3_Inserir_x3_CommunicationExchange @token,@data ,@manual, @desgin
            EXEC LogsExternal.dbo.x3_servico_Inserirx3_log 'INSERIDO','X3INTDIV',@desgin, 'st','',''
        END 
    END
GO
GRANT EXECUTE ON dbo.x3_servico_ReCalculaValor to Public
GRANT CONTROL ON dbo.x3_servico_ReCalculaValor to Public
GO