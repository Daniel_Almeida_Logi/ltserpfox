USE [mecofarma]
GO
/****** Object:  StoredProcedure [dbo].[x3_servico_DocEnviarPorTipo]    Script Date: 14/08/2024 18:10:27 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE [dbo].[x3_servico_DocEnviarPorTipo]
		@nomeServiço		VARCHAR(18), 
		@SoSite				int=0
AS
SET NOCOUNT ON
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#Empresa'))
		DROP TABLE #Empresa
		/* Dados Base Vendas  */
	create table #Empresa (
		site varchar(100)
	)
	IF(@SoSite=1)
	BEGIN
		insert into #Empresa (site)
		select site  from empresa where site = ISNULL((select rtrim(ltrim(textValue)) from B_Parameters(nolock) where stamp = 'ADM0000000352' ), '')
	END
	ELSE IF (@SoSite=2)
	BEGIN
		insert into #Empresa (site)
		select site from empresa where site in (Select items from dbo.up_splitToTable(ISNULL((select ltrim(rtrim(textValue)) from B_Parameters(nolock) where stamp = 'ADM0000000375' ), ''), ','))  
											and site not in ( ISNULL((select ltrim(rtrim(textValue)) from B_Parameters(nolock) where stamp = 'ADM0000000352' ), ''))
	END 
	ELSE 
	BEGIN
		insert into #Empresa (site)
		select site from empresa where site not in (Select items from dbo.up_splitToTable(ISNULL((select ltrim(rtrim(textValue)) from B_Parameters(nolock) where stamp = 'ADM0000000375' ), ''), ','))  
														and site not in ( ISNULL((select ltrim(rtrim(textValue)) from B_Parameters(nolock) where stamp = 'ADM0000000352' ), ''))
	END 
	IF(@nomeServiço != 'SIHFLJ')
	BEGIN
		IF(@nomeServiço != 'SIHVLJ')
		BEGIN
			SELECT 
				ftStamp,
				nrAtendimento,
				[site],
				docExt
			FROM (
				SELECT 
					ft.ftstamp					AS ftStamp
				   ,ft.u_nratend				AS nrAtendimento
				   ,ft.site						AS [site]
				   ,TD.nmDocExt					AS docExt
				   , FT.usrdata
				   , FT.usrhora	
				FROM FT (NOLOCK)
					INNER JOIN TD				(NOLOCK) ON FT.ndoc			= TD.ndoc
					INNER JOIN FI				(NOLOCK) ON FT.ftstamp		= FI.ftstamp AND FT.numLinhas = (select count(*) from fi where ftstamp = FT.ftstamp )	
					INNER JOIN B_pagCentral		(NOLOCK) ON FT.u_nratend	= B_pagCentral.nrAtend
					INNER JOIN B_UTENTES		(NOLOCK) ON FT.no			= B_UTENTES.no
					INNER JOIN taxasiva			(NOLOCK) ON FI.tabiva		= taxasiva.codigo
					INNER JOIN table_sync_status(NOLOCK) ON ft.ftstamp		= table_sync_status.regstamp  AND table_sync_status.[table]='FT'
				WHERE table_sync_status.sync = 0 AND TD.nmDocExt =  SUBSTRING(@nomeServiço,4,6) AND len(B_UTENTES.nome) > 0 and ft.etotal !=0
					AND (FT.site in(select site from #Empresa) )
				GROUP BY ft.ftstamp,	
							 ft.u_nratend,
							 ft.site,
							 FT.usrdata,
							 FT.usrhora,
							 TD.nmDocExt	
				--ORDER BY FT.usrdata ASC,
				--		 FT.usrhora	ASC
				UNION ALL 
				SELECT 
					ft.ftstamp					AS ftStamp
				   ,ft.u_nratend				AS nrAtendimento
				   ,ft.site						AS [site]
				   ,TD.nmDocExt					AS docExt
				   , FT.usrdata
				   , FT.usrhora	
				FROM FT (NOLOCK)
					INNER JOIN TD				(NOLOCK) ON FT.ndoc			= TD.ndoc
					INNER JOIN FI				(NOLOCK) ON FT.ftstamp		= FI.ftstamp AND FT.numLinhas = (select count(*) from fi where ftstamp = FT.ftstamp )	
					LEFT JOIN B_pagCentral		(NOLOCK) ON FT.u_nratend	= B_pagCentral.nrAtend
					INNER JOIN B_UTENTES		(NOLOCK) ON FT.no			= B_UTENTES.no
					INNER JOIN taxasiva			(NOLOCK) ON FI.tabiva		= taxasiva.codigo
					INNER JOIN table_sync_status(NOLOCK) ON ft.ftstamp		= table_sync_status.regstamp  AND table_sync_status.[table]='FT'
				WHERE table_sync_status.sync = 0 AND TD.nmDocExt =  SUBSTRING(@nomeServiço,4,6) AND len(B_UTENTES.nome) > 0 and ft.etotal =0
						AND (FT.site in(select site from #Empresa ))
				GROUP BY ft.ftstamp,	
							 ft.u_nratend,
							 ft.site,
							 FT.usrdata,
							 FT.usrhora,
							 TD.nmDocExt	
				--ORDER BY FT.usrdata ASC,
				--		 FT.usrhora	ASC
			)X 
			order by usrdata ASC , usrhora	ASC
		END
		ELSE
		BEGIN 
			SELECT 
				ftStamp,
				nrAtendimento,
				[site],
				docExt
			FROM (
				SELECT 
					ft.ftstamp					AS ftStamp
				   ,ft.u_nratend				AS nrAtendimento
				   ,ft.site						AS [site]
				   ,TD.nmDocExt					AS docExt
				   , FT.usrdata
				   , FT.usrhora	
				FROM FT (NOLOCK)
					INNER JOIN TD				(NOLOCK) ON FT.ndoc			= TD.ndoc
					INNER JOIN FI				(NOLOCK) ON FT.ftstamp		= FI.ftstamp AND FT.numLinhas = (select count(*) from fi where ftstamp = FT.ftstamp )	
					INNER JOIN B_pagCentral		(NOLOCK) ON FT.u_nratend	= B_pagCentral.nrAtend
					INNER JOIN B_UTENTES		(NOLOCK) ON FT.no			= B_UTENTES.no
					INNER JOIN taxasiva			(NOLOCK) ON FI.tabiva		= taxasiva.codigo
					INNER JOIN table_sync_status(NOLOCK) ON ft.ftstamp		= table_sync_status.regstamp  AND table_sync_status.[table]='FT'
				WHERE table_sync_status.sync = 0 AND TD.nmDocExt =  SUBSTRING(@nomeServiço,4,6) AND len(B_UTENTES.nome) > 0 AND B_pagCentral.ssstamp !='' AND FT.etotal !=0
					AND (FT.site in(select site from #Empresa ))
				GROUP BY ft.ftstamp,	
							 ft.u_nratend,
							 ft.site,
							 FT.usrdata,
							 FT.usrhora,
							 TD.nmDocExt	
				--ORDER BY FT.usrdata ASC,
				--		 FT.usrhora	ASC
				UNION ALL 
							SELECT 
					ft.ftstamp					AS ftStamp
				   ,ft.u_nratend				AS nrAtendimento
				   ,ft.site						AS [site]
				   ,TD.nmDocExt					AS docExt
				   , FT.usrdata
				   , FT.usrhora	
				FROM FT (NOLOCK)
					INNER JOIN TD				(NOLOCK) ON FT.ndoc			= TD.ndoc
					INNER JOIN FI				(NOLOCK) ON FT.ftstamp		= FI.ftstamp AND FT.numLinhas = (select count(*) from fi where ftstamp = FT.ftstamp )	
					LEFT JOIN B_pagCentral		(NOLOCK) ON FT.u_nratend	= B_pagCentral.nrAtend
					INNER JOIN B_UTENTES		(NOLOCK) ON FT.no			= B_UTENTES.no
					INNER JOIN taxasiva			(NOLOCK) ON FI.tabiva		= taxasiva.codigo
					INNER JOIN table_sync_status(NOLOCK) ON ft.ftstamp		= table_sync_status.regstamp  AND table_sync_status.[table]='FT'
				WHERE table_sync_status.sync = 0 AND TD.nmDocExt =  SUBSTRING(@nomeServiço,4,6) AND len(B_UTENTES.nome) > 0 AND FT.etotal =0
				AND (FT.site in(select site from #Empresa ))
				GROUP BY ft.ftstamp,	
							 ft.u_nratend,
							 ft.site,
							 FT.usrdata,
							 FT.usrhora,
							 TD.nmDocExt	
			)X 
			order by usrdata ASC , usrhora	ASC
		END 
	END
	ELSE
	BEGIN
	SELECT 
		ftStamp,
		nrAtendimento,
		[site],
		docExt
	FROM (
		SELECT 
			ft.ftstamp					AS ftStamp
		   ,ft.u_nratend				AS nrAtendimento
		   ,ft.site						AS [site]
		   ,TD.nmDocExt					AS docExt
		   , FT.usrdata
		   , FT.usrhora	
		FROM FT (NOLOCK)
			INNER JOIN TD				(NOLOCK) ON FT.ndoc				 = TD.ndoc
			INNER JOIN FI				(NOLOCK) ON FT.ftstamp			 = FI.ftstamp AND FT.numLinhas = (select count(*) from fi where ftstamp = FT.ftstamp )	
			INNER JOIN B_pagCentral		(NOLOCK) ON FT.u_nratend		 = B_pagCentral.nrAtend
			INNER JOIN B_UTENTES		(NOLOCK) ON FT.no				 = B_UTENTES.no
			INNER JOIN taxasiva			(NOLOCK) ON FI.tabiva			 = taxasiva.codigo
			INNER JOIN CC				(NOLOCK) ON CC.ftstamp           = FT.ftstamp
			INNER JOIN table_sync_status(NOLOCK) ON ft.ftstamp			 = table_sync_status.regstamp  AND table_sync_status.[table]='FT'
			LEFT  JOIN RL				(NOLOCK) ON RL.ccstamp           = CC.ccstamp
			LEFT  JOIN RE				(NOLOCK) ON RE.restamp           = RL.restamp
		WHERE table_sync_status.sync = 0 AND TD.nmDocExt =  SUBSTRING(@nomeServiço,4,6)  AND  len(B_UTENTES.nome) > 0  AND ((cc.restamp !='' AND edebf >= 0) OR cc.restamp ='') AND FT.etotal !=0
				AND (FT.site in(select site from #Empresa ))
		GROUP BY ft.ftstamp,	
					 ft.u_nratend,
					 ft.site,
					 FT.usrdata,
					 FT.usrhora,
					 TD.nmDocExt	
		UNION ALL 
		SELECT 
			ft.ftstamp					AS ftStamp
		   ,ft.u_nratend				AS nrAtendimento
		   ,ft.site						AS [site]
		   ,TD.nmDocExt					AS docExt
		   , FT.usrdata
		   , FT.usrhora	
		FROM FT (NOLOCK)
			INNER JOIN TD				(NOLOCK) ON FT.ndoc				 = TD.ndoc
			INNER JOIN FI				(NOLOCK) ON FT.ftstamp			 = FI.ftstamp AND FT.numLinhas = (select count(*) from fi where ftstamp = FT.ftstamp )	
			Left JOIN B_pagCentral		(NOLOCK) ON FT.u_nratend		 = B_pagCentral.nrAtend
			INNER JOIN B_UTENTES		(NOLOCK) ON FT.no				 = B_UTENTES.no
			INNER JOIN taxasiva			(NOLOCK) ON FI.tabiva			 = taxasiva.codigo
			INNER JOIN CC				(NOLOCK) ON CC.ftstamp           = FT.ftstamp
			INNER JOIN table_sync_status(NOLOCK) ON ft.ftstamp			 = table_sync_status.regstamp  AND table_sync_status.[table]='FT'
			LEFT  JOIN RL				(NOLOCK) ON RL.ccstamp           = CC.ccstamp
			LEFT  JOIN RE				(NOLOCK) ON RE.restamp           = RL.restamp
		WHERE table_sync_status.sync = 0 AND TD.nmDocExt =  SUBSTRING(@nomeServiço,4,6)  AND  len(B_UTENTES.nome) > 0  AND ((cc.restamp !='' AND edebf >= 0) OR cc.restamp ='') AND FT.etotal =0
				AND (FT.site in(select site from #Empresa ))
		GROUP BY ft.ftstamp,	
					 ft.u_nratend,
					 ft.site,
					 FT.usrdata,
					 FT.usrhora,
					 TD.nmDocExt
		)X 
		order by usrdata ASC , usrhora	ASC
	END 
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#Empresa'))
		DROP TABLE #Empresa
