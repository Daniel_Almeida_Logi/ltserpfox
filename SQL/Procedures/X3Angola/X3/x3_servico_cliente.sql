

/* 
select inactivo,* from b_utentes where no<200
select * from cptorg where 
select  * from B_utentes where no_ext='N009222'
	exec x3_servico_cliente 'N007211','Triunfal Seguros(AT-Saude+)','5417082848','Angola','17530101','Av. 4 de Fevereiro','','','','','','','','','20191113',0.0,'Heidimaura Africano',1,50.0,'AOA',0.1,0,1,0,1,'Angola','AO','HAF','SEG',0
	exec x3_servico_cliente 'E000103','Henner - GMC','FR48323377739','Portugal','17530101','Lagoas Park, Edifício 11','','','2740244','','','PORTO SALVO','','','20180511',9999999.0,'Magda Correia (Sage)',0,0.0,'AOA',0.1,0,1,0,1,'Angola','AO','MC','SEG',0
	exec x3_servico_cliente 'N009222','UNISAÚDE-GESTÃO DE SAÚDE, LDA','5000892440','Angola','17530101','Edifício Unisaúde- Rua dos Mirantes Larg','','','0499','','','LUANDA - LUANDA','','','20231226',10000.0,'Aneth Garcia',0,0.0,'AOA',0.1,0,1,0,1,'Angola','AO','APGAR','SEG',0 

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[x3_servico_cliente]') IS NOT NULL
	drop procedure dbo.x3_servico_cliente
go

CREATE PROCEDURE  [dbo].[x3_servico_cliente]
 @no_ext		AS VARCHAR(20)
, @nome			AS VARCHAR(80)
, @ncont		AS VARCHAR(20)
, @local		AS VARCHAR(45)
, @dtnasc		AS DATE
, @morada		AS VARCHAR(55) 
, @telef		AS VARCHAR(20)
, @obs			AS VARCHAR(MAX)
, @codpost		AS VARCHAR(45) 
, @bino         AS VARCHAR(20)
, @tlmvl		AS VARCHAR(20)
, @zona         AS VARCHAR(20)
, @sexo			AS VARCHAR(1)
, @email		AS VARCHAR(45)
, @ousrdata     AS DATE
, @plafond		AS NUMERIC(19,6)
, @nome2        AS VARCHAR(55)
, @cativ        AS BIT 
, @cativPerc    AS NUMERIC(6,2)
, @moeda		AS VARCHAR(10)
, @acre			AS NUMERIC(15,2)
, @inactivo		AS BIT
, @notif		AS BIT = 1  
, @forceact		AS BIT = 0
, @pais			AS NUMERIC(5,0) = 1
, @descp		AS VARCHAR(25) = "ANGOLA"
, @abrevp		AS VARCHAR(10) = "AO"
, @ousrinis		AS VARCHAR(255) = "ADM"
, @tipoC		AS VARCHAR(20) = ""
, @nocredit     AS BIT=0
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON
	DECLARE @tipo AS NUMERIC(10,0)
	DECLARE @desgin VARCHAR(1000)
	DECLARE @utstamp VARCHAR(25)
	DECLARE @Data DATETIME
	Declare @cambio  NUMERIC(19,12)
	DECLARE @moedaSistema VARCHAR(3)
	Declare @entCompart  bit 
	/* valida numero externo
	   senão existir, valida pelo nif (campo obrigatório)
	*/
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosCambio'))
		DROP TABLE #dadosCambio

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadoscptpla'))
		DROP TABLE #dadoscptpla

	create table #dadosCambio(
		cambio NUMERIC(19,12)
	)

	create table #dadoscptpla(
		code varchar(3)
	)

	IF(@no_ext) = ''
	BEGIN 
		SET @tipo  = (SELECT 
						TOP 1 COUNT(NO) AS NO  
					  FROM 
						b_utentes(NOLOCK) 
					  WHERE 			
						b_utentes.ncont = ltrim(rtrim(@ncont))
					  ORDER BY 
						NO
					  ASC			
					)
	END
	ELSE 
	BEGIN --    se existir, valida pelo numero externo
		SET @tipo  = (SELECT 
						TOP 1 COUNT(NO) AS NO  
					  FROM 
						b_utentes(NOLOCK) 
					  WHERE 			
						LTRIM(RTRIM(@no_ext)) = no_ext 
					  ORDER BY 
						NO			 	
					  ASC			
					)
	END

	-- saber a moeda do cliente 
	set @Data = getdate()
	SET @moedaSistema = (SELECT TOP 1 textValue from B_Parameters_site where stamp='ADM0000000003')
	INSERT #dadosCambio
	EXEC up_cambioEntreMoedas 'USD', @moedaSistema ,@Data


	IF ((SELECT count(cambio) from #dadosCambio)=0)
	BEGIN 
		SET @desgin = 'Não foi inserido pois não existe cambio entre a moeda : ' + 'USD' + ' para a moeda: ' + @moedaSistema 
		exec LogsExternal.dbo.x3_servico_Inserirx3_log 'ERRO','X3INTBPC',@desgin, '','',''
	END 
	ELSE 
	BEGIN

	SET @cambio = (SELECT TOP 1 cambio from #dadosCambio)
	SET @plafond = Round((@plafond * @cambio),2)
	SET @morada = REPLACE(@morada,'''',' ')
	SET @local = REPLACE(@local,'''',' ')
	SET @obs = REPLACE(@obs,'''',' ')
	SET @entCompart = (CASE WHEN @tipoC = 'SEG' THEN 1 ELSE 0 END )


	IF @tipo=0  -- insert do novo cliente 
	BEGIN
		IF @tipoC in ('SEG','FUNC','REF','RM')
		BEGIN
			Declare @no Numeric(15,0)
			if(@tipoC ='SEG' AND (SELECT  MAX(NO)+1  FROM b_utentes (NOLOCK) WHERE NO<200)<200)
			BEGIN
				SET @no =  (SELECT  MAX(NO)+1  FROM b_utentes (NOLOCK) WHERE NO<200)
			END
			ELSE 
			BEGIN
				SET @no = (SELECT MAX(NO)+1 FROM b_utentes(NOLOCK) where NO>200 and  no<999999)
			END
			SET @utstamp =left(newid(), 25)


			-- create utente 

			INSERT INTO b_utentes (utstamp,no, no_ext, nome, ncont, telefone, tlmvl, morada, [local] , codpost
				, email, bino, nascimento, ousrdata, ousrhora, ousrinis, sexo, tipo, descp, codigop, notif, descpNat
				, codigopNat, obs, zona, pais, usrdata, usrhora, usrinis, eplafond,nome2,cativa
				,perccativa,nocredit, moeda, inactivo, soleitura,entCompart 
			)
			SELECT 
				  @utstamp, @no, @no_ext, @nome, @ncont, @telef, @tlmvl, @morada, @local, @codpost
				, @email, @bino, @dtnasc, @ousrdata,(SELECT CONVERT(VARCHAR(8), GETDATE(), 114)),@ousrinis, @sexo, @tipoC, @descp, @abrevp, @notif, @descp
				, @abrevp, @obs, @zona, @pais,(SELECT CONVERT(date, getdate())),(SELECT CONVERT(VARCHAR(8), GETDATE(), 114)), @ousrinis, @plafond, @nome2, @cativ    
				, @cativPerc, @nocredit, @moeda, @inactivo, 1 ,@entCompart

			SET @desgin = 'Inserido com sucesso o novo cliente com o número externo: ' + @no_ext
			EXEC LogsExternal.dbo.x3_servico_Inserirx3_log 'INSERIDO','X3INTBPC',@desgin,'b_utentes', @utstamp,''

			

				DECLARE  @stamp varchar (25)
				DECLARE  @code varchar (22)
				DECLARE  @codeCptpla varchar (3)
				set @stamp = (case when (len(@no)<=3) then 'ADM'+right('00'+cast(@no AS VARCHAR(3)),3) else 'ADM' + cast(@no AS VARCHAR(22)) end)
				set @code = (case when (len(@no)<=3) then FORMAT(@no, '000') else cast(@no AS VARCHAR(22)) end)


				INSERT into #dadoscptpla
				exec dbo.GerarCodigoUnicocptpla

				set @codeCptpla = (select top 1 code from #dadoscptpla )

				delete #dadoscptpla


			-- criar segurador na cptorg if not exist 
			IF(@tipoC ='SEG'  AND NOT EXISTS(SELECT * FROM cptorg WHERE u_no = LTRIM(RTRIM(@no))))
			BEGIN	
				INSERT INTO cptorg (cptorgstamp, nome, abrev, dataass, ncont, ousrdata, usrdata, u_no, u_assfarm, codExt, percadic)
				SELECT @stamp
					   , LEFT(nome,80)
					   , (case when (len(no)<=3) then FORMAT(no, '000') else cast(no AS VARCHAR(22)) end)
					   , ousrdata
					   , ncont
					   , ousrdata
					   , usrdata
					   , no
					   , 'TOD'
					   , ''
					   , @acre
				FROM b_utentes (NOLOCK) WHERE no=@no
			END 

				-- criar segurador na cptpla  if not exist 
			IF(@tipoC ='SEG'  AND NOT EXISTS(SELECT * FROM cptpla WHERE cptplastamp = @stamp))
			BEGIN
					INSERT INTO cptpla (cptplastamp, codigo, design, cptorgstamp, cptorgabrev, tlotestamp, tlotedescr, inactivo, simples, nrctlt, maxembrct, maxembmed, maxembuni, modrct, manipulado, ousrdata, usrdata, maxembcartao,imprime )
					SELECT @stamp
						   , @codeCptpla
						   , LEFT(nome,50)
						   ,@stamp
						   , @code
						   , 'ADM0', 'Normal', 0, 1, no, 999, 999, 999, 'S', 0, ousrdata, usrdata, 999,0
					FROM b_utentes (NOLOCK) where no=@no
			END	

		END
		ELSE 
		BEGIN 
			SET @desgin = 'O cliente com o numero ' + @no_ext + ' não foi inserido porque não importamos o tipo ' + @tipoC
			EXEC LogsExternal.dbo.x3_servico_Inserirx3_log 'AVISO','X3INTBPC',@desgin,'b_utentes', '',''
		END 
	END
	ELSE
	BEGIN

			-- vai buscar o numero do utente que existe 
			DECLARE @utno NUMERIC(10,0)
			IF(@no_ext) = ''
			BEGIN
				if(@tipoC !='SEG')
				BEGIN
					SELECT @utno=(SELECT 
									TOP 1 NO 
									FROM 
										b_utentes(NOLOCK) 
									WHERE
										b_utentes.ncont = LTRIM(RTRIM(@ncont))					
									ORDER BY
										inactivo,NO
									DESC
								 )
				END
				ELSE
				BEGIN
						SELECT @utno=ISNULL((SELECT 
									TOP 1 NO 
									FROM 
										b_utentes(NOLOCK) 
									WHERE
										b_utentes.ncont = LTRIM(RTRIM(@ncont))	AND NO<200				
									ORDER BY
										inactivo,NO
									DESC
								 ),-1)

						IF(@utno>200 OR @utno=-1)
						BEGIN
							SELECT @utno=(SELECT 
										TOP 1 NO 
										FROM 
											b_utentes(NOLOCK) 
										WHERE
											b_utentes.ncont = LTRIM(RTRIM(@ncont))					
										ORDER BY
											inactivo,NO
										DESC
									 )
						end 

				END 
			END
			ELSE 
			BEGIN
				if(@tipoC !='SEG')
				BEGIN
					SELECT @utno=(SELECT 
									TOP 1 NO 
									FROM 
										b_utentes(NOLOCK) 
									WHERE
										ltrim(rtrim(@no_ext)) = no_ext				
									ORDER BY
											inactivo,NO
									DESC
								 )
				END
				ELSE
				BEGIN
						SELECT @utno=ISNULL((SELECT 
									TOP 1 NO 
									FROM 
										b_utentes(NOLOCK) 
									WHERE
										ltrim(rtrim(@no_ext)) = no_ext AND NO<200				
									ORDER BY
											inactivo,NO
									DESC
								 ),-1)

						IF(@utno>200 OR @utno=-1)
						BEGIN
							SELECT @utno=(SELECT 
											TOP 1 NO 
											FROM 
												b_utentes(NOLOCK) 
											WHERE
												ltrim(rtrim(@no_ext)) = no_ext				
											ORDER BY
													inactivo,NO
											DESC
										 )
						end 

				END 
			END


 			-- caso mudem uma seguradora para outro tipo de cliente
			IF ((SELECT tipo FROM b_utentes WHERE b_utentes.no = LTRIM(RTRIM(@utno)))='SEG' AND @tipoC !='SEG')
			BEGIN
				update cptorg set inactivo=1 where u_no=LTRIM(RTRIM(@utno))
				update cptpla set inactivo=1 where cptorgstamp=(select cptorgstamp from cptorg where u_no=LTRIM(RTRIM(@utno)))
			END
			ELSE IF (@tipoC ='SEG' and exists(select 1 from b_utentes where no=@utno) and @utno>200  and (SELECT  MAX(NO)+1  FROM b_utentes (NOLOCK) WHERE NO<200)<200)
			BEGIN
				Declare @newno Numeric(10,0)

				IF @utno>200  
				BEGIN
					
					if( (SELECT  MAX(NO)+1  FROM b_utentes (NOLOCK) WHERE NO<200)<200)
					BEGIN
						SET @newno =  (SELECT  MAX(NO)+1  FROM b_utentes (NOLOCK) WHERE NO<200)
					END
					ELSE 
					BEGIN
						SET @newno = (SELECT MAX(NO)+1 FROM b_utentes(NOLOCK) where no<999999)
					END

					update b_utentes set inactivo=1 where no=@utno

					INSERT INTO b_utentes (
							utstamp, no, no_ext, nome, ncont, telefone, tlmvl, morada, [local] , codpost
							, email, bino, nascimento, ousrdata, ousrhora, ousrinis, sexo, tipo, descp, codigop, notif, descpNat
							, codigopNat, obs, zona, pais, usrdata, usrhora, usrinis, eplafond,nome2,cativa
							,perccativa,nocredit, moeda, inactivo, soleitura, entCompart
						)
						select 
							LEFT(NEWID(),21), @newno, no_ext, nome, ncont, telefone, tlmvl, morada, [local], codpost
							, email, bino, nascimento, ousrdata, ousrhora, ousrinis, sexo, tipo, descp, codigop, notif, descpNat
							, codigopNat, obs, zona, pais, usrdata, usrhora, usrinis, eplafond,nome2,cativa
							,perccativa,nocredit, moeda, inactivo, soleitura, @entCompart
						from b_utentes where no=@utno and estab=0

				end

				if @newno <> @utno 
				begin
				update cptorg set inactivo=1 where u_no=LTRIM(RTRIM(@utno))
				update cptpla set inactivo=1 where cptorgstamp=(select cptorgstamp from cptorg where u_no=LTRIM(RTRIM(@utno)))
					select @utno = @newno
				end

				IF(EXISTS(SELECT * FROM cptorg WHERE u_no=LTRIM(RTRIM(@utno))))
				BEGIN
					update cptorg set inactivo=0 where u_no=LTRIM(RTRIM(@utno))
					update cptpla set inactivo=0 where cptorgstamp=(select cptorgstamp from cptorg where u_no=LTRIM(RTRIM(@utno)))
				END
				ELSE
				BEGIN
					set @stamp = (case when (len(@utno)<=3) then 'ADM'+right('00'+cast(@utno AS VARCHAR(3)),3) else 'ADM' + cast(@utno AS VARCHAR(22)) end)
					set @code = (case when (len(@utno)<=3) then FORMAT(@utno, '000') else cast(@utno AS VARCHAR(22)) end)
					
				INSERT into #dadoscptpla
				exec dbo.GerarCodigoUnicocptpla

				set @codeCptpla = (select top 1 code from #dadoscptpla )
			
				delete #dadoscptpla


					INSERT INTO cptpla (cptplastamp, codigo, design, cptorgstamp, cptorgabrev, tlotestamp, tlotedescr, inactivo, simples, nrctlt, maxembrct, maxembmed, maxembuni, modrct, manipulado, ousrdata, usrdata, maxembcartao,imprime )
					SELECT @stamp
					   , @codeCptpla
					   , LEFT(nome,50)
					   ,@stamp
					   , @code
						   , 'ADM0', 'Normal', 0, 1, no, 999, 999, 999, 'S', 0, ousrdata, usrdata, 999,0
					FROM b_utentes (NOLOCK) where no=@utno
					INSERT INTO cptorg (cptorgstamp, nome, abrev, dataass, ncont, ousrdata, usrdata, u_no, u_assfarm, codExt, percadic)
					SELECT @stamp, LEFT(nome,80) , @code , ousrdata, ncont, ousrdata, usrdata, no, 'TOD', '', @acre
					FROM b_utentes (NOLOCK) WHERE no=@utno
				END 
			END 
			ELSE IF (@tipoC ='SEG')
			BEGIN
				print @utno
				update cptorg set inactivo=@inactivo where u_no=LTRIM(RTRIM(@utno))
				update cptpla set inactivo=@inactivo where cptorgstamp=(select cptorgstamp from cptorg where u_no=LTRIM(RTRIM(@utno)))
			END

			


			-- update da informação na b_utentes . 
				UPDATE
					b_utentes
				SET
				no_ext = CASE WHEN @forceact=1 THEN
								@no_ext
							ELSE
								CASE WHEN @no_ext='' THEN no_ext ELSE @no_ext END
							END	
				,nome = CASE WHEN 
								(SELECT COUNT(ccstamp) FROM cc(NOLOCK) WHERE no=@utno) > 0
							THEN
								nome
							ELSE
								CASE WHEN @forceact=1 THEN
									@nome
								ELSE
									CASE WHEN @nome='' THEN nome ELSE @nome END
								END 
							END 
					,ncont = CASE WHEN 
								(SELECT count(ccstamp) FROM cc(NOLOCK) WHERE no=@utno) > 0
							THEN
								ncont
							ELSE
								CASE WHEN @forceact=1 THEN
									@ncont
								ELSE
									CASE WHEN @ncont='' THEN ncont ELSE @ncont END
								END 
							END 
					, telefone = CASE WHEN @forceact=1 THEN
									@telef
								ELSE
									 CASE WHEN @telef='' THEN telefone ELSE @telef END
								END
					, tlmvl =	CASE WHEN @forceact=1 THEN
									 @tlmvl
								ELSE
									CASE WHEN @tlmvl='' THEN tlmvl ELSE @tlmvl END
								END 
					, morada =	CASE WHEN @forceact=1 THEN
									@morada
								ELSE
									CASE WHEN @morada='' THEN morada ELSE @morada END
								END
					, [local] =	CASE WHEN @forceact=1 THEN
									@local
								ELSE
									CASE WHEN @local='' THEN [local] ELSE @local END
								END
					, codpost = CASE WHEN @forceact=1 THEN
									@codpost
								ELSE
									CASE WHEN @codpost='' THEN codpost ELSE @codpost END
								END
					, email =   CASE WHEN @forceact=1 THEN
									@email
								ELSE
									CASE WHEN @email='' THEN email ELSE @email END
								END
					, bino =    CASE WHEN @forceact=1 THEN
									@bino
								 ELSE
									CASE WHEN @bino='' THEN bino ELSE @bino END
								 END
					, nascimento = CASE WHEN @forceact=1 THEN
										@dtnasc
								   ELSE
										CASE WHEN YEAR(@dtnasc)=1900 THEN nascimento ELSE @dtnasc END
								   END
					, ousrdata = CASE WHEN @forceact=1 THEN
										@ousrdata
								 ELSE
										CASE WHEN YEAR(@ousrdata)=1900 THEN ousrdata ELSE @ousrdata END
								 END
					, ousrinis = CASE WHEN @forceact=1 THEN
										@ousrinis
								 ELSE
										CASE WHEN @ousrinis='' THEN ousrinis ELSE @ousrinis END
								 END
					, sexo     = CASE WHEN @forceact=1 THEN
										@sexo
								 ELSE
										CASE WHEN @sexo='' THEN sexo ELSE @sexo END
								 END
					, tipo     = @tipoC
					, descp    = CASE WHEN @forceact=1 THEN
										@descp
								 ELSE
										CASE WHEN @descp='' THEN descp ELSE @descp END
								 END
					,codigop   = CASE WHEN @forceact=1 THEN
										@abrevp
								 ELSE
										CASE WHEN @abrevp='' THEN codigop ELSE @abrevp END
								 END
					, notif    = CASE WHEN @forceact=1 THEN
										@notif
								 ELSE
										CASE WHEN @notif='' THEN notif ELSE @notif END
								 END
					, descpNat = CASE WHEN @forceact=1 THEN
										@descp
								 ELSE
										CASE WHEN @descp='' THEN descpNat ELSE @descp END
								 END
					, codigopNat = CASE WHEN @forceact=1 THEN
										@abrevp
								 ELSE
										CASE WHEN @abrevp='' THEN codigopNat ELSE @abrevp END
								 END
					, obs =      CASE WHEN @forceact=1 THEN
									@obs
								 ELSE
									CASE WHEN @obs='' THEN obs ELSE @obs END
								 END
					, eplafond = @plafond
					, zona	   = CASE WHEN @forceact=1 THEN
									@zona
								 ELSE
									CASE WHEN @zona='' THEN zona ELSE @zona END
								 END
					, pais      =  @pais
					, usrdata 	= (SELECT CONVERT(date, getdate())) 
					, usrhora   = (SELECT CONVERT(VARCHAR(8), GETDATE(), 114))
					, usrinis   = @ousrinis
					, nocredit  = @nocredit
					, moeda     = @moeda
					, inactivo  = @inactivo
					, nome2		= CASE WHEN @forceact=1 THEN
									@nome2
								 ELSE
									CASE WHEN @nome2='' THEN nome2 ELSE @nome2 END
								 END
					, cativa	= @cativ
					, perccativa = @cativPerc
					,entCompart  = @entCompart
				WHERE
					b_utentes.no = LTRIM(RTRIM(@utno))

					-- colocar percentagem caso mude no update 
				IF(@tipoC ='SEG' AND EXISTS(SELECT * FROM cptorg WHERE u_no = LTRIM(RTRIM(@utno))))
				BEGIN
					UPDATE cptorg SET percadic = @acre WHERE u_no = LTRIM(RTRIM(@utno))
				END

				-- guardar nos logs 
				SELECT  @utstamp =utstamp FROM b_utentes WHERE b_utentes.no = LTRIM(RTRIM(@utno))
				SET @desgin = 'Atualizado com sucesso o cliente com o número externo: '+ @no_ext
				EXEC LogsExternal.dbo.x3_servico_Inserirx3_log  'ATUALIZADO','X3INTBPC',@desgin,b_utentes,@utstamp,''	
	END

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosCambio'))
		DROP TABLE #dadosCambio
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadoscptpla'))
		DROP TABLE #dadoscptpla


	END		

GO
GRANT EXECUTE ON dbo.x3_servico_cliente to Public
GRANT CONTROL ON dbo.x3_servico_cliente to Public
GO