/*  

	exec x3_servico_Inserir_x3_CommunicationExchange 

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[x3_Inserir_x3_CommunicationExchange]') IS NOT NULL
	drop procedure dbo.x3_Inserir_x3_CommunicationExchange
go

CREATE PROCEDURE [dbo].[x3_Inserir_x3_CommunicationExchange]
	@token			VARCHAR(36),
	@data			VARCHAR(100),
	@manual			BIT,
	@message		VARCHAR(254)

AS
SET NOCOUNT ON

INSERT INTO ExternalCommunicationExchange (token,date,manual,ousrdate, message)
values (@token,@data ,@manual , GETDATE(), @message)

GO
GRANT EXECUTE ON dbo.x3_Inserir_x3_CommunicationExchange to Public
GRANT CONTROL ON dbo.x3_Inserir_x3_CommunicationExchange to Public
GO