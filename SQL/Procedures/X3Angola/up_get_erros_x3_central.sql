-- up_get_erros_x3_central 'BAIXA', '19000101', '20210217', 'ERRO'

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_get_erros_x3_central]') IS NOT NULL
	drop procedure dbo.up_get_erros_x3_central
go



create procedure dbo.up_get_erros_x3_central

  @site				as varchar(40)
, @dataini			as datetime
, @datafim			as datetime
, @tipo				as varchar(40)

/* WITH ENCRYPTION */
AS

	select 
	ISNULL(a.data_envio, '19000101') as dtenvio
	,a.loja
	,a.tipo
	,a.transid doc
	,a.data
	,a.doc_X3 as docx3
	,a.Rf_Ng as ref
	,ISNULL((select sum(d.qtt) from fi (nolock) d where d.ftstamp = a.ftstamp and d.ref = a.Rf_Ng ), 0) qtt
	,ISNULL(a.erro, '') AS erro
	from (                 
		select
			t.tiposaft tipo
			,t.tiposaft+'/'+convert(varchar(4),f.ndoc)+'/'+convert(varchar(6),f.fno) transid
			,convert(varchar(8),f.fdata,112) data
			,(select u.no_ext from b_utentes u where u.no = f.no) cod_X3
			,e.siteext loja
			,e.site descricao
			,f.ftstamp
			,t.nmdocext tipo_doc
			,t.nmdocext+'-'+e.siteext+'-'+substring(convert(varchar(4),f.ftano),3,2)+'-'+convert(varchar(6),format(f.fno, '000000')) doc_X3
			,substring(l.description,70,200)  erro
			,CASE WHEN l.description LIKE '%Stock negativo%'
				THEN substring(convert(varchar(max),l.description),charindex('Stock negativo',convert(varchar(max),l.description))-7,6)
				ELSE ''
				END Rf_Ng
			,l.date data_envio
		from ft f (NOLOCK)
			join empresa e (NOLOCK) ON f.site = e.site
			join td t (NOLOCK) on f.ndoc = t.ndoc and t.site = e.site
			join table_sync_status s (NOLOCK) on s.regstamp = f.ftstamp
			left join LogsExternal.dbo.ExternalCommunicationLogs L(nolock) on l.registerStamp = f.ftstamp
		where
		t.tiposaft <> 'PP'
		and t.nmdoc NOT LIKE '&Proforma&'
		and s.sync=0
		and f.site=@site
		and f.fdata between @dataini and @datafim
		and ISNULL(L.type, 'PORINTEGRAR') = @tipo	
	) a
	order by  a.loja asc, a.data  asc
								
 
GO
Grant Execute On dbo.up_get_erros_x3_central to Public
Grant Control On dbo.up_get_erros_x3_central to Public
GO
