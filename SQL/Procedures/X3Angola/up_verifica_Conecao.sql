
/* Dados das lojas
	
	declare @result int
	exec up_verifica_Conecao '9.9.9.9','ATLANTICO',500,4 ,'' , @result output
	print @result 

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_verifica_Conecao]') IS NOT NULL
	drop procedure up_verifica_Conecao
go

CREATE PROCEDURE up_verifica_Conecao
	@server				VARCHAR(60),
	@site				VARCHAR(60),
	@max_ping			NUMERIC(7,0)=NULL,
	@pingNumber			NUMERIC(7,0)=NULL,
	@jobName			VARCHAR(100)='',
    @res                int output



AS
SET NOCOUNT ON


        set @max_ping = ISNULL(@max_ping,(SELECT CONVERT(NUMERIC(5,0),textValue) from B_Parameters_site WHERE stamp='ADM0000000103' AND site=@site))
        set @pingNumber = ISNULL(@pingNumber,(SELECT CONVERT(NUMERIC(5,0),textValue) from B_Parameters_site WHERE stamp='ADM0000000104' AND site=@site))
        declare @command varchar(100) = 'ping ' + '-n ' + CONVERT(VARCHAR(5),@pingNumber) + ' '
        declare @field varchar(100) = 'average'
        declare @fieldLike varchar(100) = '%'+@field+'%'
        DECLARE @pingAverage NUMERIC(7,0) = @max_ping+1
        DECLARE @status varchar(50)



        IF OBJECT_ID('tempdb.dbo.#tempPing') IS NOT NULL
            DROP TABLE #tempPing ;


        CREATE TABLE #tempPing
        (
           result VARCHAR(200)
      
        )


        SET @command = @command + @server





        INSERT INTO #tempPing 
        EXEC master.dbo.xp_cmdshell @command



    --    SELECT result,
    --            SUBSTRING(result,CHARINDEX(@field,result),LEN(result)) as searched,
    --            SUBSTRING(SUBSTRING(result,CHARINDEX(@field,result),LEN(result)), PATINDEX('%[0-9]%', SUBSTRING(result,CHARINDEX(@field,result),LEN(result))), PATINDEX('%[0-9][^0-9]%', SUBSTRING(result,CHARINDEX(@field,result),LEN(result)) + 't') - PATINDEX('%[0-9]%', 
    --            SUBSTRING(result,CHARINDEX(@field,result),LEN(result))) + 1) AS Number
    --    from #tempPing
    --    where result like @fieldLike



        SELECT         
                @pingAverage = ISNULL( CONVERT(NUMERIC(5,0), SUBSTRING(SUBSTRING(result,CHARINDEX(@field,result),LEN(result)), PATINDEX('%[0-9]%', SUBSTRING(result,CHARINDEX(@field,result),LEN(result))), PATINDEX('%[0-9][^0-9]%', SUBSTRING(result,CHARINDEX(@field,result),LEN(result)) + 't') - PATINDEX('%[0-9]%', 
                SUBSTRING(result,CHARINDEX(@field,result),LEN(result))) + 1)), @max_ping +1)
        from #tempPing
        where result like @fieldLike
    
        IF(@pingAverage<=@max_ping)
        BEGIN 
            set @status ='OK'
            set @res=1
        END
        ELSE
        BEGIN
            IF (CONVERT(TIME(0),GETDATE()) between '00:00:00' and '05:45:00' and (select COUNT(*) #tempPing)>0)
            BEGIN
                SET @status ='NOT OK , but its time to go'
                set @res =1
            END 
            ELSE
            BEGIN
                set @status ='NOT OK'
                set @res = 0
            END


        END 
        INSERT INTO LogsExternal.dbo.connectionTimesLogs (connectionStamp,[server],pingNumber,averagePing,averagePingAllowed,[site],[status],[Date],[name])
        VALUES (left(NEWID(),36),@server,@pingNumber,@pingAverage,@max_ping,@site,@status,GETDATE(),@jobName)

				select * from #tempPing

		IF OBJECT_ID('tempdb.dbo.#tempPing') IS NOT NULL
            DROP TABLE #tempPing ;

        return @res 

 
GO
Grant Execute On up_verifica_Conecao to Public
Grant Control On up_verifica_Conecao to Public
GO