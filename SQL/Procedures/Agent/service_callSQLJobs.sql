/*  call sql jobs 

exec service_callSQLJobs 1 , 'BRISAS'

select textValue from B_Parameters(nolock) where stamp = 'ADM0000000352' 

	SELECT  *
FROM msdb.dbo.sysjobs j
JOIN msdb.dbo.sysjobhistory jh ON jh.job_id = j.job_id
where convert(datetime, convert(varchar(10),jh.run_date)+' ' + convert(varchar(10),format(run_time,'00:00:00')), 126) >=DATEADD(mi,-2,GETDATE()) 
ORDER BY jh.run_date DESC, jh.run_time DESC, step_id Desc;

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
if OBJECT_ID('[dbo].[service_callSQLJobs]') IS NOT NULL
	DROP PROCEDURE dbo.service_callSQLJobs
GO

CREATE PROCEDURE [dbo].[service_callSQLJobs]
		@id				INT , 
		@site			VARCHAR(50)
	
AS
SET NOCOUNT ON

	DECLARE @sitenr varchar(5)
	DECLARE @sqljobs VARCHAR(254)
	DECLARE @run_status INT
	DECLARE @dateS VARCHAR(20)
	DECLARE @dateJob DATETIME
	DECLARE @RES  INT
	 
	
	SELECT @dateS =CONVERT(varchar(20),CONVERT(date, GETDATE()))

 	SET @RES =1

	select @site = (case when associado = 1 then associadoSite else empresa.site end)   from empresa (NOLOCK) INNER JOIN AberturaEmpresa (NOLOCK)  ON empresa.no = AberturaEmpresa.sitenr where empresa.site=@site

	PRINT @site


	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosjobs'))
		DROP TABLE #dadosjobs

		select @sitenr = RIGHT('00'+CONVERT(varchar,no),2) from empresa (nolock) where site=@site

    SELECT 
		case WHEN typeExec=0  THEN jobsExecNamesL.name 
			 WHEN  typeExec=1 THEN @sitenr + ' - '+ @site + ' - ' + jobsExecNamesL.name 
			 WHEN  typeExec=2 THEN '00' + ' - '+ 'CENTRAL' + ' - ' + jobsExecNamesL.name 
			ELSE jobsExecNamesL.name END	as sqlJobs

	into #dadosjobs
	FROM 
		jobsExecNames (nolock)
	INNER JOIN jobsExecNamesL (nolock) on jobsExecNames.token = jobsExecNamesL.tokenjobsExecName
	where id=@id and GETDATE() between CONVERT(datetime,(@dateS +' ' +jobsExecNames.beginHour)) and CONVERT(datetime,(@dateS +' ' +jobsExecNames.endHour)) 
		order by sequence asc

	DECLARE emp_cursor CURSOR FOR
	SELECT  sqljobs from #dadosjobs 
	OPEN emp_cursor
	FETCH NEXT FROM emp_cursor INTO @sqljobs 
	set @RUN_STATUS=0
	WHILE @@FETCH_STATUS = 0
	BEGIN
	print @sqljobs

		IF(@RUN_STATUS=1)
		BEGIN
			print 'NEXT'
			print @RUN_STATUS
			print @dateJob
			FETCH NEXT FROM emp_cursor INTO @sqljobs
		END

		IF(@RUN_STATUS !=-1) -- -1 � sinal que o anterior job ainda n�o correu 
		BEGIN
			print 'inicio'
			set @dateJob = DATEADD(ss,-2,GETDATE()) 
			EXEC msdb.dbo.sp_start_job @sqljobs
			print @sqljobs
			set @RUN_STATUS=-1 
		END
	
			set @RUN_STATUS = isnull(( SELECT top 1  run_status FROM msdb.dbo.sysjobs (nolock) j
			JOIN msdb.dbo.sysjobhistory (nolock) jh ON jh.job_id = j.job_id
			where name= @sqljobs and (convert(datetime, convert(varchar(10),jh.run_date)+' ' + convert(varchar(10),format(run_time,'00:00:00')), 126) >=@dateJob)
			ORDER BY jh.run_date DESC, jh.run_time DESC , step_id Desc ),-1) -- -1 se ainda n�o correu
		

		   if (@RUN_STATUS >0) 
			BEGIN
			print 'estou aqui e vou para o proximo'
				set @RUN_STATUS=1 -- coloco a 1 para ir para o proximo 
			END
			ELSE IF (DATEDIFF(mi,@dateJob,GETDATE())>=2) 
			BEGIN
				SET @RES =-1;-- retorno -1 s� para escrever nos logs que falhou o job a correr ou demorou mais do que 5 min a correr 
				set @RUN_STATUS=1 -- coloco a 1 para ir para o proximo 
			END
			ELSE
			BEGIN
			print 'estou aqui'
				WAITFOR DELAY '00:00:25'; -- esperar 10 segundos para voltar a ver se o job ja correu 
			END 
	
	END 
	
	CLOSE emp_cursor
	DEALLOCATE emp_cursor
	
		if((select count(*) from #dadosjobs)=0)
		begin
			set @RES =2 
		end 
	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosjobs'))
		DROP TABLE #dadosjobs

	select @RES
GO
GRANT EXECUTE ON dbo.service_callSQLJobs to Public
GRANT CONTROL ON dbo.service_callSQLJobs to Public
GO