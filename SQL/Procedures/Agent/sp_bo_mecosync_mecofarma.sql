
-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- exec [sp_bo_mecosync_mecofarma]
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


if OBJECT_ID('[dbo].[sp_bo_mecosync_mecofarma]') IS NOT NULL
	drop procedure sp_bo_mecosync_mecofarma
go
create PROCEDURE [dbo].[sp_bo_mecosync_mecofarma]
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
declare @sql varchar(max),
	@sql01 varchar(max),
    @sql02 varchar(max),
    @sql03 varchar(max),
	@sql030 varchar(max),
	@sql04 varchar(max),
	@sql040 varchar(max),
	@sql05 varchar(max),
	@sql050 varchar(max),
	@resultsbo varchar(max),
	@resultsbo2 varchar(max),
	@resultsbi varchar(max),
	@resultsbi2 varchar(max)
SELECT @resultsbo = coalesce(@resultsbo + ',', '') +  convert(varchar(30),'['+COLUMN_NAME+']') 
FROM INFORMATION_SCHEMA.COLUMNS (nolock)
WHERE TABLE_NAME = N'bo' and COLUMNPROPERTY(object_id(TABLE_SCHEMA+'.'+TABLE_NAME), COLUMN_NAME, 'IsIdentity') <> 1
SELECT @resultsbo2 = coalesce(@resultsbo2 + ',', '') +  convert(varchar(30),'['+COLUMN_NAME+']') 
FROM INFORMATION_SCHEMA.COLUMNS (nolock)
WHERE TABLE_NAME = N'bo2' and COLUMNPROPERTY(object_id(TABLE_SCHEMA+'.'+TABLE_NAME), COLUMN_NAME, 'IsIdentity') <> 1
SELECT @resultsbi = coalesce(@resultsbi + ',', '') +  convert(varchar(30),'bi.'+COLUMN_NAME+'') 
FROM INFORMATION_SCHEMA.COLUMNS (nolock)
WHERE TABLE_NAME = N'bi' and COLUMNPROPERTY(object_id(TABLE_SCHEMA+'.'+TABLE_NAME), COLUMN_NAME, 'IsIdentity') <> 1
SELECT @resultsbi2 = coalesce(@resultsbi2 + ',', '') +  convert(varchar(30),'bi2.'+COLUMN_NAME+'') 
FROM INFORMATION_SCHEMA.COLUMNS (nolock)
WHERE TABLE_NAME = N'bi2' and COLUMNPROPERTY(object_id(TABLE_SCHEMA+'.'+TABLE_NAME), COLUMN_NAME, 'IsIdentity') <> 1
set @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON
	IF OBJECT_ID(''tempdb..#BO'') IS NOT NULL DROP TABLE #BO
	IF OBJECT_ID(''tempdb..#BO2'') IS NOT NULL DROP TABLE #BO2
	IF OBJECT_ID(''tempdb..#BO22'') IS NOT NULL DROP TABLE #BO22
	IF OBJECT_ID(''tempdb..#BOALT'') IS NOT NULL DROP TABLE #BOALT
	IF OBJECT_ID(''tempdb..#BONOTIF'') IS NOT NULL DROP TABLE #BONOTIF
	IF OBJECT_ID(''tempdb..#BONOTIFU'') IS NOT NULL DROP TABLE #BONOTIFU
	select bostamp INTO #BO from mecosync.dbo.bo boo (nolock) where boo.bostamp not in (select bostamp from bo (nolock))	
	--select * from #BO
	BEGIN TRY  
	IF((SELECT COUNT(*) FROM #BO ) > 0)
	BEGIN
	  BEGIN TRAN
	  PRINT ''INSERT''
		delete from bo where bostamp in (select bostamp from #BO)
		delete from bo2 where bo2stamp in (select bostamp from #BO)
		delete from bi where bostamp in (select bostamp from #BO)
		delete from bi2 where bostamp in (select bostamp from #BO)
		'
set @sql01 = N'
		INSERT INTO [mecofarma].[dbo].[BO] ('+substring(@resultsbo,1,len(@resultsbo)) +')
		SELECT ' + substring(@resultsbo,1,len(@resultsbo)) + ' FROM mecosync.dbo.BO (NOLOCK) 
		WHERE bostamp IN (SELECT bostamp from #BO)
		'
set @sql02 = N'
		INSERT INTO [mecofarma].[dbo].[BO2] ('+substring(@resultsbo2,1,len(@resultsbo2)) +')
		SELECT ' + substring(@resultsbo2,1,len(@resultsbo2)) + ' FROM mecosync.dbo.BO2 (NOLOCK) 
		WHERE bo2stamp IN (SELECT bostamp from #BO)'
set @sql03 = N'
		INSERT INTO [mecofarma].[dbo].[BI] ('+substring(@resultsbi,1,len(@resultsbi)) +')
		SELECT ' + substring(@resultsbi,1,len(@resultsbi)) + ' FROM mecosync.dbo.BI (NOLOCK) 
		WHERE bostamp IN (SELECT bostamp from #BO)'
set @sql030 = N'
		INSERT INTO [mecofarma].[dbo].[BI2] ('+substring(@resultsbi2,1,len(@resultsbi2)) +')
		SELECT ' + substring(@resultsbi2,1,len(@resultsbi2)) + ' FROM mecosync.dbo.BI2 (NOLOCK) 
		WHERE bostamp IN (SELECT bostamp from #BO)
	COMMIT TRAN
	END
	END TRY 
	BEGIN CATCH  
	   ROLLBACK TRAN
	END CATCH  
	SET XACT_ABORT OFF '
set @sql04 = N'
	select boo.bostamp INTO #BOALT from mecosync.dbo.bo boo (nolock) 
	inner join bo (nolock) on bo.bostamp=boo.bostamp
	where boo.usrdata + boo.usrhora >bo.usrdata + bo.usrhora
	select * from #BOALT 
	BEGIN TRY  
	IF((SELECT COUNT(*) FROM #BOALT) > 0)
	BEGIN
	  BEGIN TRAN
		UPDATE T
		SET T.[nmdos] = U.[nmdos], T.[obrano] = U.[obrano], T.[dataobra] = U.[dataobra], T.[nome] = U.[nome], T.[totaldeb] = U.[totaldeb], T.[etotaldeb] = U.[etotaldeb], T.[tipo] = U.[tipo], T.[datafinal] = U.[datafinal], T.[sqtt14] = U.[sqtt14], T.[vendedor] = U.[vendedor], T.[vendnm] = U.[vendnm], T.[no] = U.[no], T.[boano] = U.[boano], T.[dataopen] = U.[dataopen], T.[obs] = U.[obs], T.[trab1] = U.[trab1], T.[ndos] = U.[ndos], T.[moeda] = U.[moeda], T.[estab] = U.[estab], T.[morada] = U.[morada], T.[local] = U.[local], T.[codpost] = U.[codpost], T.[ncont] = U.[ncont], T.[logi1] = U.[logi1], T.[ccusto] = U.[ccusto], T.[etotal] = U.[etotal], T.[ecusto] = U.[ecusto], T.[ebo_1tvall] = U.[ebo_1tvall], T.[ebo_2tvall] = U.[ebo_2tvall], T.[ebo11_bins] = U.[ebo11_bins], T.[ebo11_iva] = U.[ebo11_iva], T.[ebo21_bins] = U.[ebo21_bins], T.[ebo21_iva] = U.[ebo21_iva], T.[ebo31_bins] = U.[ebo31_bins], T.[ebo31_iva] = U.[ebo31_iva], T.[ebo41_bins] = U.[ebo41_bins], T.[ebo41_iva] = U.[ebo41_iva], T.[ebo51_bins] = U.[ebo51_bins], T.[ebo51_iva] = U.[ebo51_iva], T.[ebo12_bins] = U.[ebo12_bins], T.[ebo12_iva] = U.[ebo12_iva], T.[ebo22_bins] = U.[ebo22_bins], T.[ebo22_iva] = U.[ebo22_iva], T.[ebo32_bins] = U.[ebo32_bins], T.[ebo32_iva] = U.[ebo32_iva], T.[ebo42_bins] = U.[ebo42_bins], T.[ebo42_iva] = U.[ebo42_iva], T.[ebo52_bins] = U.[ebo52_bins], T.[ebo52_iva] = U.[ebo52_iva], T.[ebo_totp1] = U.[ebo_totp1], T.[ebo_totp2] = U.[ebo_totp2], T.[memissao] = U.[memissao]
		, T.[nome2] = U.[nome2], T.[origem] = U.[origem], T.[site] = U.[site], T.[pnome] = U.[pnome], T.[pno] = U.[pno], T.[ocupacao] = U.[ocupacao], T.[tpdesc] = U.[tpdesc], T.[ousrinis] = U.[ousrinis], T.[ousrdata] = U.[ousrdata], T.[ousrhora] = U.[ousrhora], T.[usrinis] = U.[usrinis], T.[usrdata] = U.[usrdata], T.[usrhora] = U.[usrhora], T.[u_dataentr] = U.[u_dataentr], T.[ebo61_iva] = U.[ebo61_iva], T.[fref] = U.[fref], T.[ncusto] = U.[ncusto], T.[ultfact] = U.[ultfact], T.[exportado] = U.[exportado], T.[tabIva] = U.[tabIva], T.[ebo61_bins] = U.[ebo61_bins], T.[ebo62_bins] = U.[ebo62_bins], T.[ebo62_iva] = U.[ebo62_iva], T.[edescc] = U.[edescc], T.[fechada] =U.[fechada],T.[datafecho] =U.[datafecho]    
		FROM [mecofarma].[dbo].[BO] as T
		INNER JOIN mecosync.dbo.BO (NOLOCK) AS U on t.bostamp = U.bostamp
		WHERE U.bostamp IN (SELECT distinct bostamp FROM #BOALT)'
set @sql040 = N'
		UPDATE T
		SET T.[autotipo]=U.[autotipo],T.[pdtipo]=U.[pdtipo],T.[usrinis]=U.[usrinis],T.[usrdata]=U.[usrdata],T.[usrhora]=U.[usrhora],T.[armazem]=U.[armazem],T.[xpdviatura]=U.[xpdviatura],T.[xpddata]=U.[xpddata],T.[xpdhora]=U.[xpdhora],T.[morada]=U.[morada],T.[codpost]=U.[codpost],T.[telefone]=U.[telefone],T.[contacto]=U.[contacto],T.[email]=U.[email],T.[etotalciva]=U.[etotalciva],T.[etotiva]=U.[etotiva],T.[u_class]=U.[u_class],T.[u_doccont]=U.[u_doccont],T.[u_fostamp]=U.[u_fostamp],T.[ebo71_IVA]=U.[ebo71_IVA],T.[ebo81_IVA]=U.[ebo81_IVA],T.[ebo91_IVA]=U.[ebo91_IVA],T.[ATDocCode]=U.[ATDocCode],T.[exportado]=U.[exportado],T.[ebo71_bins]=U.[ebo71_bins],T.[ebo72_bins]=U.[ebo72_bins],T.[ebo72_iva]=U.[ebo72_iva],T.[ebo81_bins]=U.[ebo81_bins],T.[ebo82_bins]=U.[ebo82_bins],T.[ebo82_iva]=U.[ebo82_iva],T.[ebo91_bins]=U.[ebo91_bins],T.[ebo92_bins]=U.[ebo92_bins],T.[ebo92_iva]=U.[ebo92_iva],T.[IVATX1]=U.[IVATX1],T.[IVATX2]=U.[IVATX2],T.[IVATX3]=U.[IVATX3],T.[IVATX4]=U.[IVATX4],T.[IVATX5]=U.[IVATX5],T.[IVATX6]=U.[IVATX6],T.[IVATX7]=U.[IVATX7],T.[IVATX8]=U.[IVATX8],T.[IVATX9]=U.[IVATX9],T.[nrReceita]=U.[nrReceita],T.[codext]=U.[codext],T.[u_nratend]=U.[u_nratend],T.[morada_ent]=U.[morada_ent],T.[localidade_ent]=U.[localidade_ent],T.[codpost_ent]=U.[codpost_ent],T.[pais_ent]=U.[pais_ent],T.[modo_envio]=U.[modo_envio],T.[pagamento]=U.[pagamento],T.[pinAcesso]=U.[pinAcesso],T.[pinOpcao]=U.[pinOpcao],T.[nrviasimp]=U.[nrviasimp],T.[momento_pagamento]=U.[momento_pagamento]
		,T.[ebo101_bins]= U.[ebo101_bins],T.[ebo111_bins]= U.[ebo111_bins],T.[ebo121_bins]= U.[ebo121_bins],T.[ebo131_bins]= U.[ebo131_bins],T.[ebo101_iva]= U.[ebo101_iva],T.[ebo111_iva]= U.[ebo111_iva],T.[ebo121_iva]= U.[ebo121_iva],T.[ebo131_iva]= U.[ebo131_iva],T.[ivatx10]= U.[ivatx10],T.[ivatx11]= U.[ivatx11],T.[ivatx12]= U.[ivatx12],T.[ivatx13]= U.[ivatx13],T.[status]= U.[status]
		FROM [mecofarma].[dbo].[BO2] as T
		INNER JOIN mecosync.dbo.BO2 (NOLOCK) AS U on t.bo2stamp = U.bo2stamp
		WHERE U.bo2stamp IN (SELECT distinct bostamp FROM #BOALT)
		'
set @sql05 = N'
		delete from bi where bostamp in (select bostamp from #BOALT)
		delete from bi2 where bostamp in (select bostamp from #BOALT)
		INSERT INTO [mecofarma].[dbo].[BI] ('+substring(@resultsbi,1,len(@resultsbi)) +')
		SELECT ' + substring(@resultsbi,1,len(@resultsbi)) + ' FROM mecosync.dbo.BI (NOLOCK) 
		WHERE bostamp IN (SELECT bostamp from #BOALT)
		'
set @sql050 = N'
		INSERT INTO [mecofarma].[dbo].[BI2] ('+substring(@resultsbi2,1,len(@resultsbi2)) +')
		SELECT ' + substring(@resultsbi2,1,len(@resultsbi2)) + ' FROM mecosync.dbo.BI2 (NOLOCK) 
		WHERE bostamp IN (SELECT bostamp from #BOALT)
	COMMIT TRAN
	END
	END TRY 
	BEGIN CATCH  
	   ROLLBACK TRAN
	END CATCH  
	SET XACT_ABORT OFF	
		'
execute (@sql+@sql01+@sql02+@sql03+@sql030+@sql04+@sql040+@sql05+@sql050)
