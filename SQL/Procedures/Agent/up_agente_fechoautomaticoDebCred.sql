-- FUNCAO		: Fecha automaticamente Notas de Cr�dito e D�bito (vossas e nossas)
-- Pressupostos	: Fecha todas as Notas de Cr�dito e de D�bito que estejam associadas a outros documentos.
-- exec lt_fechoautomaticoDebCred


----------------------------------------
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_agente_fechoautomaticoDebCred]') IS NOT NULL
	drop procedure dbo.up_agente_fechoautomaticoDebCred
go

create procedure dbo.up_agente_fechoautomaticoDebCred

/* WITH ENCRYPTION */
AS

Begin
	Begin Try
		begin tran FechoDebCred
			
			SET NOCOUNT ON
			set language portuguese

			DECLARE @FOSTAMP as varchar(38)
			
			DECLARE uFechaDebCred CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
			SELECT	distinct fo.FOSTAMP
			from fn (nolock)
			inner join fo (nolock) on fo.fostamp=fn.fostamp
			WHERE	fo.doccode in (2, 3, 57, 100)
				AND fo.u_status = 'A'
				and (fnstamp in (select ofnstamp
										from fn fn2 (nolock)
										where fn2.ofnstamp=fn.fnstamp
									)
					OR
					fnstamp in (select fnstamp
									from bi2 (nolock)
									where bi2.fnstamp=fn.fnstamp
					
									)
					OR
					ofnstamp!=''
					OR
					bistamp!=''
					)

			OPEN	uFechaDebCred
			FETCH NEXT FROM uFechaDebCred INTO @FOSTAMP
			
			WHILE @@FETCH_STATUS = 0
			BEGIN
				UPDATE FO SET U_STATUS = 'F' WHERE FOSTAMP = @FOSTAMP
				print 'Documento com stamp: ' + @fostamp + ' Fechado'
	
				FETCH NEXT FROM uFechaDebCred INTO @FOSTAMP
			End
			
		commit tran FechoDebCred
	End try

	Begin Catch
		if @@TRANCOUNT>0
			rollback tran FechoDebCred
		
		/* Limpar cursores de mem�ria caso existam */
		If CURSOR_STATUS('global','uFechaDebCred')!=-3
		Begin
			if CURSOR_STATUS('global','uFechaDebCred')!=-1
				Close uFechaDebCred
			Deallocate uFechaDebCred
		End
		
		SELECT ERROR_NUMBER()  AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity, ERROR_STATE()  AS ErrorState,
			ERROR_PROCEDURE() AS ErrorProcedure, ERROR_LINE()  AS ErrorLine, ERROR_MESSAGE() AS ErrorMessage;
		
		insert into B_elog (tipo, status, mensagem, origem)
		values ('E', 'A', ERROR_MESSAGE(), 'up_agente_fechoautomaticoDebCred')
		
		return 
	end catch

End


GO
Grant Execute On dbo.up_agente_fechoautomaticoDebCred to Public
Grant control On dbo.up_agente_fechoautomaticoDebCred to Public
GO
---------------------------------------------------------------

