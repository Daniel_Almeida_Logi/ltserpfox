
-- =============================================
-- Author:		Jos� Costa
-- Create date: 2020
-- Description:	Replicate Data From TB - CX Ext Server to local server 
-- exec [sp_ReplicateDocumentos_dadossite_remote] '172.20.40.6\SQLEXPRESS', 'ATLANTICO'

-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


if OBJECT_ID('[dbo].[sp_ReplicateDocumentos_dadossite_remote]') IS NOT NULL
	drop procedure sp_ReplicateDocumentos_dadossite_remote
go
create PROCEDURE [dbo].[sp_ReplicateDocumentos_dadossite_remote]

@server			varchar(60)
,@site			varchar(20)

AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @sql varchar(max),
@sql1 varchar(max),
@sql2 varchar(max)
	
set @sql = N'
SET NOCOUNT ON
SET XACT_ABORT ON
	
	IF OBJECT_ID(''tempdb..#BOALT'') IS NOT NULL DROP TABLE #BOALT
	
	select  bo.bostamp
	, (select no from empresa (nolock) where site=boo.site) AS sitenoalt
	, boo.fechada as fechada 
	, boo.datafecho as datafecho 
	, boo2.status as statusalt 
	, boo.usrdata as usrdata 
	, boo.usrhora as usrhora 
	 INTO #BOALT from bo (nolock) 
	 inner join bo2 (nolock) on bo2.bo2stamp=bo.bostamp 
	 inner join ['+@server+'].[mecofarma].[dbo].[BO] BOO with (nolock) on boo.bostamp=bo.bostamp
	 inner join ['+@server+'].[mecofarma].[dbo].[BO2] BOO2 with (nolock) on boo2.bo2stamp=bo.bostamp
	 where bo.site='''+@site+''' and bo.ndos=41
	 and bo.ousrdata > getdate()-30
	 and boo.ousrdata > getdate()-30
	 and bo2.ousrdata > getdate()-30
	 and boo2.ousrdata > getdate()-30
	 and bo2.status <> ''''
	 and (bo.fechada <> boo.fechada
			or bo2.status <> boo2.status)
	 and CAST((cast (bo.usrdata as date)) AS DATETIME) + CAST(bo.usrhora AS DATETIME)<(CAST((cast (BOO.usrdata as date)) AS DATETIME) + CAST(BOO.usrhora AS DATETIME) )
	 and bo.dataobra>getdate()-15
	select * from #BOALT
	
	BEGIN TRY  
	IF((SELECT COUNT(*) FROM #BOALT ) > 0)
	BEGIN
	  BEGIN TRAN
	  PRINT ''UPDATE CENTRAL''
		ALTER TABLE bo DISABLE TRIGGER rep_bo_control

		update bo set fechada=#boalt.fechada, usrhora=#boalt.usrhora, usrdata=#boalt.usrdata, datafecho=#boalt.usrhora
		from bo inner join #BOALT on bo.bostamp=#BOALT.bostamp

		update bo2 set status=#boalt.statusalt, usrhora=#boalt.usrhora, usrdata=#boalt.usrdata
		from bo2 inner join #BOALT on bo2.bo2stamp=#BOALT.bostamp

		ALTER TABLE bo ENABLE TRIGGER rep_bo_control
		
	  COMMIT TRAN
	END
	END TRY 
	BEGIN CATCH  
	   ROLLBACK TRAN
	END CATCH '
	 
set @sql1 = N'
	IF OBJECT_ID(''tempdb..#BOALT1'') IS NOT NULL DROP TABLE #BOALT1

	select  bo.bostamp
	, (select no from empresa (nolock) where site=boo.site) AS sitenoalt
	, boo.site as sitealt 
	, boo.usrdata as usrdata 
	, boo.usrhora as usrhora 
	 INTO #BOALT1 from bo (nolock) 
	 inner join bo2 (nolock) on bo2.bo2stamp=bo.bostamp 
	 inner join ['+@server+'].[mecofarma].[dbo].[BO] BOO with (nolock) on boo.bostamp=bo.bostamp
	 inner join ['+@server+'].[mecofarma].[dbo].[BO2] BOO2 with (nolock) on boo2.bo2stamp=bo.bostamp
	 where bo.site='''+@site+''' and bo.ndos=41
	 and bo.ousrdata > getdate()-30
	 and boo.ousrdata > getdate()-30
	 and bo2.ousrdata > getdate()-30
	 and boo2.ousrdata > getdate()-30
	 and bo2.status <> ''''
	 and (bo.site <> boo.site)
	 --and CAST((cast (bo.usrdata as date)) AS DATETIME) + CAST(bo.usrhora AS DATETIME)<(select CAST((cast (BOO.usrdata as date)) AS DATETIME) + CAST(BOO.usrhora AS DATETIME) from [172.20.40.6\SQLEXPRESS].[mecofarma].[dbo].[BO] BOO with (nolock) where BOO.bostamp=bo.bostamp)
	select * from #BOALT1
	
	BEGIN TRY  
	IF((SELECT COUNT(*) FROM #BOALT1 ) > 0)
	BEGIN
	  BEGIN TRAN
	  PRINT ''UPDATE CENTRAL SITE''

		update bo set site=#boalt1.sitealt, usrhora=#boalt1.usrhora, usrdata=#boalt1.usrdata
		from bo inner join #BOALT1 on bo.bostamp=#BOALT1.bostamp

		update bi set armazem=#boalt1.sitenoalt
		from bi inner join #BOALT1 on bi.bostamp=#BOALT1.bostamp

		insert into Rep_Control_site (Table_Name,Identifier,Operation,Processed,Site,Date)
		select ''BO'',#BOALT1.bostamp,''I'',0,#boalt1.sitealt,GETDATE()  from #boalt1
		
	  COMMIT TRAN
	END
	END TRY 
	BEGIN CATCH  
	   ROLLBACK TRAN
	END CATCH '

--set @sql2 = N'
--
--	IF OBJECT_ID(''tempdb..#BOALT2'') IS NOT NULL DROP TABLE #BOALT2
--	
--	SET NOCOUNT ON
--SET XACT_ABORT ON
--	
--	IF OBJECT_ID(''tempdb..#BOALT2'') IS NOT NULL DROP TABLE #BOALT2
--	
--	select  bo.bostamp
--	, bo.fechada  as fechada 
--	, bo.datafecho as datafecho 
--	, bo2.status as statusalt 
--	, bo.usrdata as usrdata
--	,bo.usrhora as usrhora
--	 INTO #BOALT2 from bo (nolock) 
--	 inner join bo2 (nolock) on bo2.bo2stamp=bo.bostamp 
--	 inner join ['+@server+'].[mecofarma].[dbo].[BO] BOO with (nolock) on boo.bostamp=bo.bostamp
--	 inner join ['+@server+'].[mecofarma].[dbo].[BO2] BOO2 with (nolock) on boo2.bo2stamp=bo.bostamp
--	 where bo.site='''+@site+''' and bo.ndos=41
--	 and bo.ousrdata > getdate()-30
--	 and boo.ousrdata > getdate()-30
--	 and bo2.ousrdata > getdate()-30
--	 and boo2.ousrdata > getdate()-30
--	 and bo2.status <> ''''
--	 and ( bo.fechada <> boo.fechada 
--			or bo2.status <> boo2.status )
--	 and CAST((cast (bo.usrdata as date)) AS DATETIME) + CAST(bo.usrhora AS DATETIME)>( CAST((cast (BOO.usrdata as date)) AS DATETIME) + CAST(BOO.usrhora AS DATETIME) )
--	select * from #BOALT2
--	
--	BEGIN TRY  
--	IF((SELECT COUNT(*) FROM #BOALT2 ) > 0)
--	BEGIN
--	  BEGIN TRAN
--	  PRINT ''UPDATE FARMACIA''
--
--		DECLARE @bostamp VARCHAR(60)
--
--		declare bo_cursor cursor for
--		select bostamp as siteemp from #BOALT2
--
--		open bo_cursor
--		fetch next from bo_cursor into @bostamp
--
--		while @@FETCH_STATUS = 0
--		begin
--						
--			declare @stamp varchar(70),
--					@fechada varchar(1),
--					@usrhora varchar(10),
--					@usrdata varchar(12),
--					@datafecho varchar(12),
--					@statusalt varchar(20)
--
--			select 
--					@stamp		= bo.bostamp,
--					@fechada	= cast(bo.fechada as varchar(1)),
--					@usrhora	= bo.usrhora,
--					@usrdata	= CONVERT(varchar, bo.usrdata,112),
--					@datafecho	= CONVERT(varchar, bo.datafecho, 112),
--					@statusalt	= bo2.status
--			from bo (nolock) 
--			inner join bo2 (nolock) on bo.bostamp=bo2.bo2stamp
--			where bostamp = @bostamp
--
--			declare @bostampexec varchar(max)
--
--			SELECT @bostampexec = ''EXEC [mecofarma].[dbo].remote_bo_fecha  ''''''+ @stamp + '''''', '''''' + @fechada + '''''', '''''' + @usrhora + '''''' , '''''' + @usrdata + ''''''	, '''''' + @datafecho + '''''' , '''''' + @statusalt + '''''' ''
--			print @bostampexec
--			EXEC (@bostampexec) AT ['+@server+']
--					
--
--			fetch next from bo_cursor into @bostamp
--		
--		end
--		close bo_cursor
--		deallocate bo_cursor
--
--		--update ['+@server+'].[mecofarma].[dbo].[BO] set fechada=#boalt2.fechada, usrhora=#boalt2.usrhora, usrdata=#boalt2.usrdata, datafecho=#boalt2.datafecho
--		--from ['+@server+'].[mecofarma].[dbo].[BO] boo inner join #BOALT2 on boo.bostamp=#BOALT2.bostamp
--
--		--update ['+@server+'].[mecofarma].[dbo].[BO2] set status=#boalt2.statusalt, usrhora=#boalt2.usrhora, usrdata=#boalt2.usrdata
--		--from ['+@server+'].[mecofarma].[dbo].[BO2] boo2 inner join #BOALT2 on boo2.bo2stamp=#BOALT2.bostamp
--		
--	  COMMIT TRAN
--	END
--	END TRY 
--	BEGIN CATCH  
--	   ROLLBACK TRAN
--	END CATCH 
--'
print @sql+@sql1 --+@sql2


execute (@sql+@sql1) -- +@sql2
