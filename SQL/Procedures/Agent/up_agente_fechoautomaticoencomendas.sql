/* ID:		FECHO AUTOMATICO DE ENCOMENDAS

	exec up_agente_fechoautomaticoencomendas

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_agente_fechoautomaticoencomendas]') IS NOT NULL
	drop procedure dbo.up_agente_fechoautomaticoencomendas
go

create procedure dbo.up_agente_fechoautomaticoencomendas

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

Begin
	Begin Try
		begin tran FechoEnc
			set language portuguese

			DECLARE @BOSTAMP varchar(254),@NUM_ENTRADA	numeric(9,0),@DATAENTREGA datetime,@NOME varchar(254)
			DECLARE @NO numeric(9,0),@ESTAB	numeric(4,0),@MORADA varchar(254),@LOCAL varchar(254),@CODPOST varchar(254)
			DECLARE @NCONT varchar(254),@DATA datetime,@DOCDATA datetime,@FOANO varchar(254),@PDATA datetime,@MOEDA varchar(254)
			DECLARE @MEMISSAO varchar(254),@MOEDA2 varchar(254),@ollocal varchar(254),@telocal varchar(254),@FINAL varchar(254)
			DECLARE @ousrinis varchar(254),@ousrdata datetime,@ousrhora varchar(8),@usrinis varchar(254),@usrdata datetime
			DECLARE @usrhora varchar(8),@DIAS_PARAM int,@DIAS_DIFF int,@NEW_STAMP varchar(254),@NEW_STAMP_LINHAS varchar(254)
			DECLARE @DOCCONT int,@BISTAMP varchar(254),@ref varchar(254),@oref varchar(254),@codigo as varchar(254),@design varchar(254)
			DECLARE @stns int,@usr1 varchar(254),@usr2 varchar(254),@usr3 varchar(254),@usr4 varchar(254),@usr5 varchar(254)
			DECLARE @usr6 varchar(254),@adoc as varchar(254),@unidade	varchar(254),@iva numeric(12,4),@ivaincl int,@tabiva int
			DECLARE @armazem int, @MYLORDEM	int, @mycontrol int

			SELECT @mycontrol = 0

			DECLARE EncomendasCursor CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
			SELECT	BOSTAMP,U_DATAENTR as DATAENTREGA,NOME,NO,ESTAB,MORADA,LOCAL,CODPOST,NCONT,
					GETDATE() as DATA,GETDATE() as DOCDATA,YEAR(GETDATE()) as FOANO,GETDATE() as PDATA,
					'PTE ou EURO' as MOEDA,'EURO' as MEMISSAO,'PTE ou EURO' as MOEDA2,'Caixa' as ollocal,
					'C' as telocal,'Documento Criado via Fecho Autom�tico de Encomendas' as FINAL,
					'ADM' as ousrinis,convert(varchar,getdate(),102) as ousrdata,convert(varchar,getdate(),8) as ousrhora,'ADM' as usrinis,
					convert(varchar,getdate(),102) as usrdata,convert(varchar,getdate(),8) as usrhora
			FROM	BO (nolock)
			WHERE	NDOS = 2 /* ENCOMENDA A FORNECEDOR */
					AND FECHADA = 0
			OPEN	EncomendasCursor

			FETCH NEXT FROM EncomendasCursor INTO @BOSTAMP,@DATAENTREGA,@NOME,@NO,@ESTAB,@MORADA,@LOCAL,@CODPOST,@NCONT,@DATA,@DOCDATA,@FOANO,@PDATA,@MOEDA,@MEMISSAO,@MOEDA2,@ollocal,@telocal,@FINAL,@ousrinis,@ousrdata,@ousrhora,@usrinis,@usrdata,@usrhora

			WHILE @@FETCH_STATUS = 0
			BEGIN

				
				select @NUM_ENTRADA = isnull((SELECT MAX(CONVERT(int,ADOC)) + 1 FROM FO WHERE DOCCODE = 102),1)
				select @DIAS_PARAM = (SELECT numValue FROM B_Parameters WHERE stamp = 'ADM0000000059')
				select @DOCCONT =	(select ISNULL(MAX(fo2.u_doccont),0)+1 as doccont from fo2 (nolock) inner join fo (nolock) on fo.fostamp=fo2.fo2stamp where fo.doccode=102)
				
				/* VERIFICA A DIFEREN�A DA DATA DE ENTREGA + NUM dIAS DEFINIDA NO PARAMETRO RELATIVAMENTE � DATA ACTUAL */
				select @DIAS_DIFF = DATEDIFF(dd,GETDATE(),@DATAENTREGA + @DIAS_PARAM)
					
				PRINT 'DATA DE ENTREGA:' + CONVERT(varchar,@DATAENTREGA)
				PRINT 'DATA ACTUAL: ' + CONVERT(varchar,GETDATE())
				PRINT 'DIAS PARAMETRIZA��O PARA O FECHO: ' + CONVERT(varchar,@DIAS_PARAM)
				PRINT 'DIFEREN�A: ' + CONVERT(varchar,@DIAS_DIFF)
				
				IF @DIAS_DIFF <= 1
				BEGIN
				
					/**************** FECHA ENCOMENDA *******************************************/
					UPDATE BO SET FECHADA = 1 WHERE BOSTAMP = @BOSTAMP
					update bi set fechada = 1 where bostamp = @BOSTAMP 
					print 'ENCOMENDA FECHADA COM SUCESSO: ' + @BOSTAMP
					/**************** CRIA N/GUIA ENTREGA **************************************/
					
					/* CRIAR NOVO STAMP */
					SET @NEW_STAMP = 'ADM' + LEFT(newid(),22)	
						
						
					/* ESCREVE CABE�ALHO */
					INSERT INTO FO (fostamp,docnome,doccode,nome,no,estab,adoc,morada,local,
						codpost,ncont,data,docdata,foano,pdata,moeda,memissao,moeda2,ollocal,
						telocal,final,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,U_STATUS 
					)
					Values (@NEW_STAMP,'N/Guia Entrada',102,@NOME,@NO,@ESTAB,@NUM_ENTRADA,@MORADA,
						@LOCAL,@CODPOST,@NCONT,@DATA,@DOCDATA,@FOANO,@PDATA,@MOEDA,@MEMISSAO,@MOEDA2,
						@ollocal,@telocal,@FINAL,@ousrinis,@ousrdata,@ousrhora,@usrinis,@usrdata,@usrhora,'F'
					)
									
					INSERT INTO fo2(fo2stamp,ousrinis, ousrdata,ousrhora, usrinis, usrdata, usrhora,u_doccont)
					values(@NEW_STAMP,@ousrinis,@ousrdata,@ousrhora,@usrinis,@usrdata,@usrhora,@DOCCONT)
						
					/* ESCREVE LINHAS */
						
					DECLARE EncomendasLinhasCursor CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
					SELECT	BISTAMP,ref,ref as oref,codigo,design,stns,usr1,
								usr2,usr3,usr4,	usr5,usr6,unidade,iva,ivaincl,tabiva,armazem
					FROM	BI
					WHERE	BOSTAMP = @BOSTAMP
					OPEN	EncomendasLinhasCursor

					FETCH NEXT FROM EncomendasLinhasCursor INTO @BISTAMP,@ref,@oref,@codigo,@design,@stns,@usr1,@usr2,@usr3,@usr4,@usr5,@usr6,@unidade,@iva,@ivaincl,@tabiva,@armazem
					WHILE @@FETCH_STATUS = 0
					BEGIN
						/* CRIAR NOVO STAMP_LINHAS */
						SET @NEW_STAMP_LINHAS = 'ADM' + LEFT(newid(),22)	
						SET	@MYLORDEM = 1
						
						INSERT INTO fn (fnstamp,fostamp,bistamp,data,ref,oref,codigo,design,stns,usr1,usr2,usr3,usr4,
								usr5,usr6,docnome,adoc,unidade,iva,ivaincl,tabiva,armazem,lordem,ousrinis,ousrdata,
								ousrhora,usrinis,usrdata,usrhora)
						Values
								(
								@NEW_STAMP_LINHAS,@NEW_STAMP,@bistamp,GETDATE(),@ref,@oref,@codigo,@design,
								@stns,@usr1,@usr2,@usr3,@usr4,@usr5,@usr6,'N/Guia Entrada',@NUM_ENTRADA,
								@unidade,@iva,@ivaincl,@tabiva,@armazem,@MYLORDEM,@ousrinis,@ousrdata,@ousrhora,
								@usrinis,@usrdata,@usrhora
								)
							
						SET @MYLORDEM = @MYLORDEM + 1
					
						FETCH NEXT FROM EncomendasLinhasCursor INTO @BISTAMP,@ref,@oref,@codigo,@design,@stns,@usr1,@usr2,@usr3,@usr4,@usr5,@usr6,@unidade,@iva,@ivaincl,@tabiva,@armazem
					END	
					
					CLOSE EncomendasLinhasCursor
					DEALLOCATE EncomendasLinhasCursor
				END
				
				FETCH NEXT FROM EncomendasCursor INTO @BOSTAMP,@DATAENTREGA,@NOME,@NO,@ESTAB,@MORADA,@LOCAL,@CODPOST,@NCONT,@DATA,@DOCDATA,@FOANO,@PDATA,@MOEDA,@MEMISSAO,@MOEDA2,@ollocal,@telocal,@FINAL,@ousrinis,@ousrdata,@ousrhora,@usrinis,@usrdata,@usrhora
			END
			
		commit tran FechoEnc
	END TRY
		
	Begin Catch
		if @@TRANCOUNT>0
			rollback tran FechoEnc
		
		/* Limpar cursores de mem�ria caso existam */
		If CURSOR_STATUS('global','EncomendasCursor')!=-3
		Begin
			if CURSOR_STATUS('global','EncomendasCursor')!=-1
				Close uFechaFac
			Deallocate uFechaFac
		End
		
		SELECT ERROR_NUMBER()  AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity, ERROR_STATE()  AS ErrorState,
			ERROR_PROCEDURE() AS ErrorProcedure, ERROR_LINE()  AS ErrorLine, ERROR_MESSAGE() AS ErrorMessage;
		
		insert into B_elog (tipo, status, mensagem, origem)
		values ('E', 'A', ERROR_MESSAGE(), 'up_agente_fechoautomaticoencomendas')
		
		return
	end catch
end

GO
Grant Execute on dbo.up_agente_fechoautomaticoencomendas to Public
grant control on dbo.up_agente_fechoautomaticoencomendas to public
GO

-------------------------------------------------------------------------