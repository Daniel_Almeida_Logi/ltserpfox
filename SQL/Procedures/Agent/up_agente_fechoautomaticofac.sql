-- ID					:
-- FUNCAO			: Fecha automaticamente Facturas
-- SP					: up_agente_fechoautomaticofac
-- Data				: 2010-09-29
-- Pressupostos	: So fecha Facturas com documento de origem e j� conferidas ou sem diverg�ncias
----------------------------------------

--exec up_agente_fechoautomaticofac

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_agente_fechoautomaticofac]') IS NOT NULL
	drop procedure dbo.up_agente_fechoautomaticofac
go

create procedure dbo.up_agente_fechoautomaticofac

/* WITH ENCRYPTION */
AS

Begin
	Begin Try
		begin tran FechoFac
			
			SET NOCOUNT ON
			set language portuguese

			DECLARE @FOSTAMP as varchar(38), @data as datetime
			declare @fechoAuto bit, @Dias_param as int, @DIAS_DIFF as int
			
			set @fechoAuto = isnull((SELECT bool FROM B_Parameters WHERE stamp = 'ADM0000000063'),0)
			
			If @fechoAuto=1
			begin
				set @Dias_param = isnull((SELECT numValue FROM B_Parameters WHERE stamp = 'ADM0000000063'),1)

				DECLARE uFechaFac CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
				SELECT	FOSTAMP, data
				FROM	FO (nolock)
				WHERE	fo.doccode = 55 AND fo.data <= getdate() AND fo.u_status = 'A'
					and fostamp not in (select fostamp
										from fn (nolock)
										where ( ((fn.u_qttenc!=fn.qtt or fn.u_upc!=fn.u_upce)
												and (fn.data <= getdate()) and fn.marcada=0 )
												OR (	(select Sum(case when ofnstamp='' then 0 else 1 end) from fn fnx (nolock) where fnx.fostamp=fn.fostamp)=0
														AND
														(select Sum(case when bistamp='' then 0 else 1 end) from fn fny (nolock) where fny.fostamp=fn.fostamp)=0
													)
												)
												and fn.adoc=fo.adoc and fo.docnome=fn.docnome
										)

				OPEN	uFechaFac
				FETCH NEXT FROM uFechaFac INTO @FOSTAMP, @data

				WHILE @@FETCH_STATUS = 0
				BEGIN
					
					/* Validar se deve fechar a encomenda */
					select @DIAS_DIFF = DATEDIFF(dd,GETDATE(),@data + @Dias_param)
					
					if @DIAS_DIFF<=0
					begin
						UPDATE FO SET U_STATUS = 'F' WHERE FOSTAMP = @FOSTAMP
						print 'Factura com Stamp: ' + @fostamp + ' Fechada'
					end

					FETCH NEXT FROM uFechaFac INTO @FOSTAMP, @data
				END
			END
			
		commit tran FechoFac
	End try

	Begin Catch
		if @@TRANCOUNT>0
			rollback tran FechoFac
		
		/* Limpar cursores de mem�ria caso existam */
		If CURSOR_STATUS('global','uFechaFac')!=-3
		Begin
			if CURSOR_STATUS('global','uFechaFac')!=-1
				Close uFechaFac
			Deallocate uFechaFac
		End
		
		SELECT ERROR_NUMBER()  AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity, ERROR_STATE()  AS ErrorState,
			ERROR_PROCEDURE() AS ErrorProcedure, ERROR_LINE()  AS ErrorLine, ERROR_MESSAGE() AS ErrorMessage;
		
		insert into B_elog (tipo, status, mensagem, origem)
		values ('E', 'A', ERROR_MESSAGE(), 'up_agente_fechoautomaticofac')
		
		return 
	end catch

End


GO
Grant Execute On dbo.up_agente_fechoautomaticofac to Public
Grant control On dbo.up_agente_fechoautomaticofac to Public
GO
---------------------------------------------------------------