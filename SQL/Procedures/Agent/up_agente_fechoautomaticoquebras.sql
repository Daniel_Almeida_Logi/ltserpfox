
/* FUNCAO:	Fecha automaticamente o Dossier de Quebras 18 quando a diferen�a entre a data actual e a datta do dossier � inferior a Zero

	exec up_agente_fechoautomaticoquebras

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_agente_fechoautomaticoquebras]') IS NOT NULL
	drop procedure dbo.up_agente_fechoautomaticoquebras
go

create procedure dbo.up_agente_fechoautomaticoquebras
/* WITH ENCRYPTION */

AS
SET NOCOUNT ON
set language portuguese


DECLARE @BOSTAMP as varchar(254)

DECLARE QuebrasDocCursor CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
SELECT	BOSTAMP
FROM	BO (nolock)
WHERE	NDOS = 18
		AND DATEDIFF(dd,GETDATE(),BO.DATAOBRA) < 0
		AND FECHADA = 0

OPEN	QuebrasDocCursor
FETCH NEXT FROM QuebrasDocCursor INTO @BOSTAMP

WHILE @@FETCH_STATUS = 0
BEGIN
	UPDATE BO SET FECHADA = 1 WHERE BOSTAMP = @BOSTAMP

FETCH NEXT FROM QuebrasDocCursor INTO @BOSTAMP
END

CLOSE QuebrasDocCursor
DEALLOCATE QuebrasDocCursor


GO
Grant Execute On dbo.up_agente_fechoautomaticoquebras to Public
Grant control On dbo.up_agente_fechoautomaticoquebras to Public
GO
--------------------------------------------------------------------------------