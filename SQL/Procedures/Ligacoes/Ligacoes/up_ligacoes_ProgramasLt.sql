/* Lista Programas do grupo de utilizadores */
-- exec up_ProgramasLt 'Contabilidade'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_ligacoes_ProgramasLt]') IS NOT NULL
	drop procedure up_ligacoes_ProgramasLt
go

create PROCEDURE up_ligacoes_ProgramasLt
@nome as char(25)

/* WITH ENCRYPTION */ 

AS 
BEGIN 
		
	Select name 
	from b_uglt
	inner join ug on b_uglt.ugstamp = ug.ugstamp
	Where nome = @nome

END

GO
Grant Execute On up_ligacoes_ProgramasLt to Public
Grant Control On up_ligacoes_ProgramasLt to Public
GO
