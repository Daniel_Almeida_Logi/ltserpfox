/* Lista Programas j� associados ao grupo de utilizadores */
-- exec up_ListLicControlUg 'Administrador'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_ligacoes_ListLicControlUg]') IS NOT NULL
	drop procedure up_ligacoes_ListLicControlUg
go

create PROCEDURE up_ligacoes_ListLicControlUg
@ugstamp as char(25)

/* WITH ENCRYPTION */ 

AS 
BEGIN 
		
	Select name,[desc] from b_uglt Where ugstamp = @ugstamp order by [desc]

END

GO
Grant Execute On up_ligacoes_ListLicControlUg to Public
Grant Control On up_ligacoes_ListLicControlUg to Public
GO