/* 
	Adiciona Licen�as de Utiliza��o 
	
	select * from b_control_lt
	select * from b_control_ac

	truncate table b_control_lt
	truncate table b_control_ac
	delete b_control_lt
	delete b_control_ac
*/

declare @endtime as datetime, @nrLicensas as int
set @endtime= (select dateadd(mm,994,getdate()))
set @nrLicensas = 1


select 
	'exec up_ligacoes_addLicControl ''' + b.[name] + ''', ''' + b.[desc] + ''', ' + ltrim(str(@nrLicensas)) + ', ''' + convert(varchar,@endtime) + ''''
from
	b_control_lt b
		

-- M�dulos Clinica
--exec up_ligacoes_addLicControl 'ME','MARCACOES',@nrLicensas,@endtime
--exec up_ligacoes_addLicControl 'MC','MCDT',@nrLicensas,@endtime 
--exec up_ligacoes_addLicControl 'PE','PRESCRICAO_ELECTRONICA',@nrLicensas,@endtime
--exec up_ligacoes_addLicControl 'RC','REGISTO_CLINICO',@nrLicensas,@endtime
--exec up_ligacoes_addLicControl 'BP','BIOPARAMETROS',@nrLicensas,@endtime
--exec up_ligacoes_addLicControl 'MAN','MANIPULA��O',@nrLicensas,@endtime
--exec up_ligacoes_addLicControl 'PS','PROBLEMAS_SAUDE',@nrLicensas,@endtime

-- Modulos Base
exec up_ligacoes_addLicControl 'PCENTRAL','PCENTRAL',@nrLicensas,@endtime
exec up_ligacoes_addLicControl 'ACESSOS','GESTAO_ACESSOS',@nrLicensas,@endtime
exec up_ligacoes_addLicControl 'SISTEMA','CONFIGURACOES_SISTEMA',@nrLicensas,@endtime 
exec up_ligacoes_addLicControl 'PORTAL','PORTAL',@nrLicensas,@endtime 
exec up_ligacoes_addLicControl 'ESPECIALIS','GESTAO_ESPECIALISTAS',@nrLicensas,@endtime
exec up_ligacoes_addLicControl 'ANALISES','PAINEL_ANALISES',@nrLicensas,@endtime 
exec up_ligacoes_addLicControl 'PROC','PROCEDIMENTOS_INTERNOS',@nrLicensas,@endtime

-- Modulos Gest�o 
exec up_ligacoes_addLicControl 'CLI','GESTAO_CLIENTES',@nrLicensas,@endtime
exec up_ligacoes_addLicControl 'UTENTES','GESTAO_UTENTES',@nrLicensas,@endtime
exec up_ligacoes_addLicControl 'FACT','FACTURACAO',@nrLicensas,@endtime 

-- Modulos Fornecedores e Stocks
exec up_ligacoes_addLicControl 'FOR','GESTAO_FORNECEDORES',@nrLicensas,@endtime  
exec up_ligacoes_addLicControl 'DOC','DOCUMENTOS',@nrLicensas,@endtime 
exec up_ligacoes_addLicControl 'INV','INVENTARIO_LT',@nrLicensas,@endtime -- Controlado por BD
exec up_ligacoes_addLicControl 'PAGFOR','PAGAMENTOS FORNECEDOR',@nrLicensas,@endtime
exec up_ligacoes_addLicControl 'ST','GESTAO_STOCKS',@nrLicensas,@endtime 

-- M�dulos Farm�cia
exec up_ligacoes_addLicControl 'ATEND','ATENDIMENTO',@nrLicensas,@endtime 
exec up_ligacoes_addLicControl 'PSICOBENZO','GESTAO_PSICO_BENZO',@nrLicensas,@endtime 
exec up_ligacoes_addLicControl 'REC','RECEITUARIO',@nrLicensas,@endtime 

-- CRM
exec up_ligacoes_addLicControl 'PROMOCOES','PROMOCOES',@nrLicensas,@endtime -- -- Controlado por BD
exec up_ligacoes_addLicControl 'SMS','ALERTAS_SMS',@nrLicensas,@endtime -- Controlado por BD
exec up_ligacoes_addLicControl 'CRM','CRM',@nrLicensas,@endtime -- Controlado por BD

-- M�dulos Associado
--exec up_ligacoes_addLicControl 'ASS','ASSOCIADOS',@nrLicensas,@endtime

-- M�dulos Adicionais
--exec up_ligacoes_addLicControl 'ARQ_DIGITA','ARQUIVO_DIGITAL',@nrLicensas,@endtime -- Controlado por BD
--exec up_ligacoes_addLicControl 'PAGCENTRAL','PAGAMENTOS_CENTRALIZADOS',@nrLicensas,@endtime -- Controlado por BD




