/* Verifica Integridade das Licen�as */
/* exec up_ligacoes_verifyIntgLicControl 'mc' */
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_ligacoes_verifyIntgLicControl]') IS NOT NULL
	drop procedure up_ligacoes_verifyIntgLicControl
go

create PROCEDURE up_ligacoes_verifyIntgLicControl
@stamp as nvarchar(25)


/* WITH ENCRYPTION */ 
AS 
BEGIN 
	
	
	select	
		*
	from	
		b_control_lt 
	Where	
		name = @stamp  
		AND lt != HASHBYTES('SHA1', convert(varchar(20),isnull(time,'19000101'))+RTRIM(LTRIM(name)) + LTRIM(RTRIM(endtime))+RTRIM(LTRIM(convert(nvarchar(3),value))))
	
END

GO
Grant Execute On up_ligacoes_verifyIntgLicControl to Public
Grant Control On up_ligacoes_verifyIntgLicControl to Public
GO