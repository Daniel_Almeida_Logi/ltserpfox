/*
	select * from b_control_lt

	exec up_ligacoes_addLicControl 'PS','PROB. SAUDE',999,'20150101'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_ligacoes_addLicControl]') IS NOT NULL
	drop procedure up_ligacoes_addLicControl
go

create PROCEDURE up_ligacoes_addLicControl
	@stamp as nvarchar(25),
	@desc as  nvarchar(50),
	@value as int,
	@endtime as datetime


/* WITH ENCRYPTION */ 

AS 
BEGIN 
	
	;with cte3 (loginame, [program_name], [login_time]) as (
		Select 
			loginame, [program_name], [login_time]
		FROM 
			sys.sysprocesses
		Where 
			spid = @@spid
	)

	
	insert into b_control_lt
	Select	
		HASHBYTES('SHA1', convert(varchar(20),isnull((Select top 1 login_time from cte3),'19000101'))+RTRIM(LTRIM(@stamp)) +LTRIM(RTRIM(@endtime))+ RTRIM(LTRIM(convert(nvarchar(4),@value))))
			,@stamp,@desc,@value, isnull((Select top 1 login_time from cte3),'19000101'),@endtime

END

GO
Grant Execute On up_ligacoes_addLicControl to Public
Grant Control On up_ligacoes_addLicControl to Public
GO