/* Verifica Integridade das Licen�as */
/* exec up_ligacoes_verifyIntgLicControlAll */
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_ligacoes_verifyIntgLicControlAll]') IS NOT NULL
	drop procedure up_ligacoes_verifyIntgLicControlAll
go

create PROCEDURE up_ligacoes_verifyIntgLicControlAll

/* WITH ENCRYPTION */ 
AS 
BEGIN 
	
	
	select	
		*
	from	
		b_control_lt 
	Where	
		lt != HASHBYTES('SHA1', convert(varchar(20),isnull(time,'19000101'))+RTRIM(LTRIM(name)) + LTRIM(RTRIM(endtime))+ RTRIM(LTRIM(convert(nvarchar(3),value))))
	
END

GO
Grant Execute On up_ligacoes_verifyIntgLicControlAll to Public
Grant Control On up_ligacoes_verifyIntgLicControlAll to Public
GO