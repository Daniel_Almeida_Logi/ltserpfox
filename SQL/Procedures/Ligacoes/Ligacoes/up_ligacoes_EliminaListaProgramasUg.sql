/* Elimina Registos associados ao grupo para porterior grava��o */
-- exec up_EliminaListaProgramasUg '123'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_ligacoes_EliminaListaProgramasUg]') IS NOT NULL
	drop procedure up_ligacoes_EliminaListaProgramasUg
go

create PROCEDURE up_ligacoes_EliminaListaProgramasUg
@ugstamp as char(25)

/* WITH ENCRYPTION */ 

AS 
BEGIN 
	
	
	delete from b_uglt Where ugstamp = @ugstamp

END

GO
Grant Execute On up_ligacoes_EliminaListaProgramasUg to Public
Grant Control On up_ligacoes_EliminaListaProgramasUg to Public
GO