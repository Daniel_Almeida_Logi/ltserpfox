/* Verifica Número de Ligações Utilização 

	 exec up_ligacoes_verifyControl 'FACT' 


	 select *from b_control_ac
	 
	 select *from b_us

	 select *from sys.sysprocesses where   program_name like '%'+ db_name()+'%'
 */
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_ligacoes_verifyControl]') IS NOT NULL
	drop procedure up_ligacoes_verifyControl
go

create PROCEDURE up_ligacoes_verifyControl
@stamp as nvarchar(25)



/* WITH ENCRYPTION */

AS 
BEGIN 

	/* Elimina Ligações que já não estão ligadas */	
	delete  ac
	from b_control_ac ac
	LEFT JOIN sys.sysprocesses  on  convert(varchar(20),id)+convert(varchar,logintime,21)+convert(varchar(20),id)  =   convert(varchar(20),spid) + convert(varchar,login_time,21) + convert(varchar(20),spid)
	Where	
		 spid is null and
		 lt != '0x77F8560A290EE87A3913F9C6F1AD162C17167625'

	
	select	
		isnull(sum(value),0) as connections
		,isnull((
			select 
				value 
			from 
				b_control_lt (nolock) 
			where 
				name = @stamp
				and Datediff(mi,getdate(),endtime) > 0
		),0) as allowed
		,isnull((select endtime from b_control_lt(nolock) where name = @stamp),DATEADD(dd,-1,getdate())) as endtime
	from	
		b_control_ac  (nolock)
	Where	
		name = @stamp  
		and  b_control_ac.id != @@SPID
		and Datediff(mi,getdate(), isnull((select endtime from b_control_lt(nolock) where name = @stamp),DATEADD(dd,-1,getdate()))) > 0

END

GO
Grant Execute On up_ligacoes_verifyControl to Public
Grant Control On up_ligacoes_verifyControl to Public
GO		