/* Lista Programas Licenciados ao cliente */
-- exec up_ListLicControl ''
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_ligacoes_ListLicControl]') IS NOT NULL
	drop procedure up_ligacoes_ListLicControl
go

create PROCEDURE up_ligacoes_ListLicControl
@ugstamp as char(25)

/* WITH ENCRYPTION */ 

AS 
BEGIN 
	
	
	Select	name,[desc] 
	from	b_control_lt 
	where	name not in (select name from b_uglt Where ugstamp = @ugstamp)
	order by [desc]

END

GO
Grant Execute On up_ligacoes_ListLicControl to Public
Grant Control On up_ligacoes_ListLicControl to Public
GO