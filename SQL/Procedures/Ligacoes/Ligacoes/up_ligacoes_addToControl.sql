/* Adiciona Utiliza��o 
 
 exec up_ligacoes_addToControl 'LT2' 

 */
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_ligacoes_addToControl]') IS NOT NULL
	drop procedure up_ligacoes_addToControl
go

create PROCEDURE up_ligacoes_addToControl
@stamp as nvarchar(25)


/* WITH ENCRYPTION */ 
AS 
BEGIN 
	
	/* Se o SPID ainda n�o tiver sido adicionado*/
	IF (@@spid not in (Select id from b_control_ac (nolock) where name = @stamp))
	BEGIN
		
		;with cte1 as (
			Select loginame as LoginName, program_name, login_time
			FROM sys.sysprocesses
			Where spid = @@spid
		)
	
		insert into b_control_ac
		Select @@spid, @stamp , 1, HASHBYTES('SHA1', convert(varchar(20),isnull((Select top 1 login_time from cte1),'19000101'))+RTRIM(LTRIM(@stamp)) + RTRIM(LTRIM(convert(nvarchar(3),1)))), 
				isnull((Select top 1 LoginName from cte1),''),isnull((Select top 1 login_time from cte1),'19000101'),isnull((Select top 1 program_name from cte1),'')
	END
END

GO
Grant Execute On up_ligacoes_addToControl to Public
Grant Control On up_ligacoes_addToControl to Public
GO