/* Verifica Integridade das Liga��es  */
/* exec up_verifyIntgControlAll  */
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_ligacoes_verifyIntgControlAll]') IS NOT NULL
	drop procedure up_ligacoes_verifyIntgControlAll
go

create PROCEDURE up_ligacoes_verifyIntgControlAll


/* WITH ENCRYPTION */ 

AS 
BEGIN 
	
	IF (SELECT count(id) as valor from b_control_ac Where name = 'LT2' And lt = HASHBYTES('SHA1', convert(varchar(20),isnull(logintime,'19000101'))+RTRIM(LTRIM(name)) + RTRIM(LTRIM(convert(nvarchar(3),value))))) = 1
	BEGIN
		select	*,HASHBYTES('SHA1', convert(varchar(20),isnull(logintime,'19000101'))+RTRIM(LTRIM(name)) + RTRIM(LTRIM(convert(nvarchar(3),value))))
		from	b_control_ac 
		Where	lt != HASHBYTES('SHA1', convert(varchar(20),isnull(logintime,'19000101'))+RTRIM(LTRIM(name)) + RTRIM(LTRIM(convert(nvarchar(3),value))))
	END
	ELSE
	BEGIN
		select	id= 0, nem = '', value = 0, lt = 'invalid'
	END
END

GO
Grant Execute On up_ligacoes_verifyIntgControlAll to Public
Grant Control On up_ligacoes_verifyIntgControlAll to Public
GO