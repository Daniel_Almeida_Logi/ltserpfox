/* 
	Procedere para guardar as informacoes do administrationWays
	exec up_get_sendSearchPrice 'D21DE067-8B82-4256-885E-D8C39D9DF568',''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_sendSearchPrice]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_sendSearchPrice
GO

CREATE PROCEDURE dbo.up_get_sendSearchPrice

	@token			AS VARCHAR(36),
	@nrAtendimento	AS VARCHAR(36) 

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	DECLARE @sql	varchar(4000) = ''
	SET @sql = @sql + N'
	SELECT 
		stamp
		,token
		,ref
		,site
		,teste
	FROM 
		sendSearchPrice (NOLOCK)
	 WHERE 
		1=1'
	IF @token != ''
		SET @sql = @sql + N' AND token  = ''' + @token + ''''
	
	IF @nrAtendimento != ''
		SET @sql = @sql + N' AND nrAtendimento = ''' + @nrAtendimento + ''''
		
	PRINT(@sql)
	exec(@sql)
GO
GRANT EXECUTE on dbo.up_get_sendSearchPrice TO PUBLIC
GRANT Control on dbo.up_get_sendSearchPrice TO PUBLIC
GO