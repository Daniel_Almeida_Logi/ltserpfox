/* 
	Procedere para guardar as informacoes do administrationWays
	exec up_save_searchPricePvp '44a84caa-8e9d-4b09-89bb-55bd7ba50a0','2DCF26AB-A61B-435C-B981-5EFD0E7C96B3','5589767',44.17

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_searchPricePvp]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_searchPricePvp
GO

CREATE PROCEDURE dbo.up_save_searchPricePvp
	@stamp			AS VARCHAR(36),
	@token			AS VARCHAR(36),
	@ref			AS VARCHAR(18),
	@pvp			AS NUMERIC(16,4)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	IF( NOT EXISTS (SELECT * FROM searchPricePvp (NOLOCK) WHERE stamp  = @stamp))
	BEGIN
		INSERT INTO searchPricePvp(stamp ,token ,ref ,pvp
									,ousrdata, ousrinis, usrdata, usrinis)
		VALUES (@stamp, @token, @ref, @pvp
				,getdate(),'ADM',getdate(),'ADM')
	END

GO
GRANT EXECUTE on dbo.up_save_searchPricePvp TO PUBLIC
GRANT Control on dbo.up_save_searchPricePvp TO PUBLIC
GO