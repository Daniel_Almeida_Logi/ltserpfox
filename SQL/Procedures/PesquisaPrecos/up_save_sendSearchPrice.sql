/* 
	Procedere para guardar as informacoes do administrationWays
	exec up_save_sendSearchPrice '','','','',0

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_sendSearchPrice]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_sendSearchPrice
GO

CREATE PROCEDURE dbo.up_save_sendSearchPrice
	@stamp			AS VARCHAR(36),
	@token			AS VARCHAR(36),
	@ref			AS VARCHAR(18),
	@site			AS VARCHAR(60),
	@teste			AS BIT,
	@nrAtendimento	AS VARCHAR(36)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	IF( NOT EXISTS (SELECT * FROM sendSearchPrice (NOLOCK) WHERE stamp  = @stamp))
	BEGIN
		INSERT INTO sendSearchPrice(stamp, token, ref, site, teste, nrAtendimento
									,ousrdata, ousrinis, usrdata, usrinis)
		VALUES (@stamp, @token, @ref, @site, @teste, @nrAtendimento
				,getdate(),'ADM',getdate(),'ADM')
	END

GO
GRANT EXECUTE on dbo.up_save_sendSearchPrice TO PUBLIC
GRANT Control on dbo.up_save_sendSearchPrice TO PUBLIC
GO