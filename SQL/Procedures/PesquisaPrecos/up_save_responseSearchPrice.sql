/* 
	Procedere para guardar as informacoes do administrationWays
	exec up_save_responseSearchPrice '','','','',0

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_responseSearchPrice]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_responseSearchPrice
GO

CREATE PROCEDURE dbo.up_save_responseSearchPrice
	@stamp					AS VARCHAR(36),
	@token					AS VARCHAR(36),
	@tokenSearchPricePvp	AS VARCHAR(36),
	@ref					AS VARCHAR(18),
	@priceMax				AS NUMERIC(16,2),
	@fourLowestPrice		AS NUMERIC(16,2),
	@priceRefere			AS NUMERIC(16,2),
	@priceNotified			AS NUMERIC(16,2),
	@taxCompart				AS NUMERIC(3,0)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	IF( NOT EXISTS (SELECT * FROM responseSearchPrice (NOLOCK) WHERE stamp  = @stamp))
	BEGIN
		INSERT INTO responseSearchPrice(stamp ,token ,tokenSearchPricePvp ,ref
										,priceMax ,fourLowestPrice ,priceRefere ,priceNotified
										,taxCompart
									,ousrdata, ousrinis, usrdata, usrinis)
		VALUES (@stamp, @token, @tokenSearchPricePvp, @ref,
				@priceMax, @fourLowestPrice, @priceRefere, @priceNotified,
				@taxCompart
				,getdate(),'ADM',getdate(),'ADM')
	END

GO
GRANT EXECUTE on dbo.up_save_responseSearchPrice TO PUBLIC
GRANT Control on dbo.up_save_responseSearchPrice TO PUBLIC
GO