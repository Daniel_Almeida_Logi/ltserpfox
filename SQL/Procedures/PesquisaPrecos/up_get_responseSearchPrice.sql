/* 
	Procedere para guardar as informacoes do administrationWays
	exec up_get_responseSearchPrice ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_responseSearchPrice]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_responseSearchPrice
GO

CREATE PROCEDURE dbo.up_get_responseSearchPrice

	@token					AS VARCHAR(36)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	SELECT 
		stamp
		,token
		,tokenSearchPricePvp
		,ref
		,priceMax
		,fourLowestPrice
		,priceRefere
		,priceNotified
		,taxCompart 
	FROM 
		responseSearchPrice (NOLOCK)
	WHERE
		token  = @token

GO
GRANT EXECUTE on dbo.up_get_responseSearchPrice TO PUBLIC
GRANT Control on dbo.up_get_responseSearchPrice TO PUBLIC
GO