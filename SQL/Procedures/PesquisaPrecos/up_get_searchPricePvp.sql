/* 
	Procedere para guardar as informacoes do administrationWays
	exec up_get_searchPricePvp '','','','',0

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_searchPricePvp]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_searchPricePvp
GO

CREATE PROCEDURE dbo.up_get_searchPricePvp

	@token			AS VARCHAR(36)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	SELECT
		 stamp
		,token
		,ref
		,pvp 
	FROM
		searchPricePvp (NOLOCK) 
	WHERE 
		token  = @token

GO
GRANT EXECUTE on dbo.up_get_searchPricePvp TO PUBLIC
GRANT Control on dbo.up_get_searchPricePvp TO PUBLIC
GO