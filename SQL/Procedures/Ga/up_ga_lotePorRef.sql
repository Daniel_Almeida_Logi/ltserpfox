/*
	exec [up_ga_lotePorRef] 5440987
	
	
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_ga_lotePorRef]') IS NOT NULL
	drop procedure up_ga_lotePorRef
go

create PROCEDURE up_ga_lotePorRef
	@ref varchar(18)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select 
	ref
	,'lote' = '[' + CONVERT(varchar,armazem) + ']' + lote
	,'stock' = convert(varchar,convert(numeric(13,0),stock))
	,'validade' = LEFT(convert(date,validade),7)
	,'PCT' = isnull(pct,0)
	,'EPV1' = isnull(epv1,0)
	,'ultmov' = convert(varchar,convert(date, st_lotes.usrdata))
	,'ultpreco' = isnull(convert(varchar,convert(date, st_lotes_precos.usrdata)),convert(varchar,convert(date, st_lotes.usrdata)))
	,'id' = ROW_NUMBER() over(partition by lote + convert(varchar,armazem) order by lote)
	,'agrupar' = CONVERT(bit,0)
	,'nomeLote' = st_lotes.lote
from 
	st_lotes (nolock)
	left join st_lotes_precos (nolock) on st_lotes.stamp = st_lotes_precos.stampLote
where 
	ref = @ref
order by 
	st_lotes.usrdata + st_lotes.usrhora desc
	,st_lotes.lote
	,st_lotes_precos.usrdata + st_lotes_precos.usrhora desc

GO
Grant Execute On up_ga_lotePorRef to Public
Grant Control On up_ga_lotePorRef to Public
GO
