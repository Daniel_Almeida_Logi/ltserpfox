--exec up_ImportSAFT 'Julho'

-- Verificar se a stored procedure existe e apagar, se necessário
IF OBJECT_ID('dbo.up_ImportSAFT', 'P') IS NOT NULL
   DROP PROCEDURE dbo.up_ImportSAFT; 
GO

-- Criar a stored procedure para importar o ficheiro SAF-T e excluir o ficheiro após o processamento
CREATE PROCEDURE dbo.up_ImportSAFT
  @fileName NVARCHAR(255) -- Parâmetro de entrada: Nome do ficheiro XML
AS
BEGIN
    -- Habilitar xp_cmdshell se não estiver habilitado
    EXEC sp_configure 'show advanced options', 1;
    RECONFIGURE;
    EXEC sp_configure 'xp_cmdshell', 1;
    RECONFIGURE;

    -- Definir o caminho fixo e concatenar com o nome do ficheiro
    DECLARE @filePath NVARCHAR(255);
    SET @filePath = '\\st01\ArquivoDigital\SAFTs\' + @fileName + '.xml';
	PRINT @filepath
	
    -- Variável para armazenar o resultado da verificação do ficheiro
    DECLARE @fileExists INT;

    -- Verificar se o ficheiro existe
    EXEC xp_fileexist @filePath, @fileExists OUTPUT;

    -- Se o ficheiro não existir, retornar uma mensagem e encerrar a procedure
    IF @fileExists = 0
    BEGIN
        PRINT 'Ficheiro não existe';
        RETURN;
    END

    -- Carregar o ficheiro XML para uma variável SQL Server
    DECLARE @xmlData XML;
    DECLARE @sql NVARCHAR(MAX);

    -- Construir a consulta dinâmica para carregar o XML
    SET @sql = '
        SELECT @xmlData = CONVERT(XML, BulkColumn) 
        FROM OPENROWSET(BULK ''' + @filePath + ''', SINGLE_BLOB) AS x;';

    -- Executar a consulta dinâmica e armazenar o resultado em @xmlData
    EXEC sp_executesql @sql, N'@xmlData XML OUTPUT', @xmlData OUTPUT;

-- Inserir dados na tabela Header se não existirem duplicatas
WITH XMLNAMESPACES ('urn:OECD:StandardAuditFile-Tax:PT_1.04_01' AS ns)
INSERT INTO Header (AuditFileVersion, CompanyID, TaxRegistrationNumber, TaxAccountingBasis, 
                    CompanyName, BusinessName, FiscalYear, StartDate, EndDate, CurrencyCode, 
                    DateCreated, TaxEntity, ProductCompanyTaxID, SoftwareCertificateNumber)
SELECT
    Header.value('(ns:AuditFileVersion)[1]', 'NVARCHAR(50)'),
    Header.value('(ns:CompanyID)[1]', 'NVARCHAR(50)'),
    Header.value('(ns:TaxRegistrationNumber)[1]', 'NVARCHAR(50)'),
    Header.value('(ns:TaxAccountingBasis)[1]', 'NVARCHAR(5)'),
    Header.value('(ns:CompanyName)[1]', 'NVARCHAR(255)'),
    Header.value('(ns:BusinessName)[1]', 'NVARCHAR(255)'),
    Header.value('(ns:FiscalYear)[1]', 'INT'),
    Header.value('(ns:StartDate)[1]', 'DATE'),
    Header.value('(ns:EndDate)[1]', 'DATE'),
    Header.value('(ns:CurrencyCode)[1]', 'NVARCHAR(10)'),
    Header.value('(ns:DateCreated)[1]', 'DATE'),
    Header.value('(ns:TaxEntity)[1]', 'NVARCHAR(50)'),
    Header.value('(ns:ProductCompanyTaxID)[1]', 'NVARCHAR(50)'),
    Header.value('(ns:SoftwareCertificateNumber)[1]', 'NVARCHAR(50)')
FROM @xmlData.nodes('/ns:AuditFile/ns:Header') AS T(Header)
WHERE NOT EXISTS (
    SELECT 1 FROM Header 
    WHERE CompanyID = Header.value('(ns:CompanyID)[1]', 'NVARCHAR(50)')
      AND StartDate = Header.value('(ns:StartDate)[1]', 'DATE')
      AND EndDate = Header.value('(ns:EndDate)[1]', 'DATE')
);


-- Inserir dados na tabela CompanyAddress se não existirem duplicatas
WITH XMLNAMESPACES ('urn:OECD:StandardAuditFile-Tax:PT_1.04_01' AS ns)
INSERT INTO CompanyAddress (AddressDetail, City, PostalCode, Region, Country)
SELECT
    Address.value('(ns:AddressDetail)[1]', 'NVARCHAR(255)'),
    Address.value('(ns:City)[1]', 'NVARCHAR(100)'),
    Address.value('(ns:PostalCode)[1]', 'NVARCHAR(20)'),
    Address.value('(ns:Region)[1]', 'NVARCHAR(100)'),
    Address.value('(ns:Country)[1]', 'NVARCHAR(10)')
FROM @xmlData.nodes('/ns:AuditFile/ns:Header/ns:CompanyAddress') AS T(Address)
WHERE NOT EXISTS (
    SELECT 1 FROM CompanyAddress 
    WHERE AddressDetail = Address.value('(ns:AddressDetail)[1]', 'NVARCHAR(255)')
      AND City = Address.value('(ns:City)[1]', 'NVARCHAR(100)')
      AND PostalCode = Address.value('(ns:PostalCode)[1]', 'NVARCHAR(20)')
      AND Region = Address.value('(ns:Region)[1]', 'NVARCHAR(100)')
      AND Country = Address.value('(ns:Country)[1]', 'NVARCHAR(10)')
);


-- Inserir dados na tabela Customer se não existirem duplicatas
WITH XMLNAMESPACES ('urn:OECD:StandardAuditFile-Tax:PT_1.04_01' AS ns)
INSERT INTO Customer (CustomerID, AccountID, CustomerTaxID, CompanyName, BillingAddressDetail, 
                      BillingCity, BillingPostalCode, BillingRegion, BillingCountry, SelfBillingIndicator)
SELECT
    Customer.value('(ns:CustomerID)[1]', 'NVARCHAR(50)'),
    Customer.value('(ns:AccountID)[1]', 'NVARCHAR(50)'),
    Customer.value('(ns:CustomerTaxID)[1]', 'NVARCHAR(50)'),
    Customer.value('(ns:CompanyName)[1]', 'NVARCHAR(255)'),
    Customer.value('(ns:BillingAddress/ns:AddressDetail)[1]', 'NVARCHAR(255)'),
    Customer.value('(ns:BillingAddress/ns:City)[1]', 'NVARCHAR(100)'),
    Customer.value('(ns:BillingAddress/ns:PostalCode)[1]', 'NVARCHAR(20)'),
    Customer.value('(ns:BillingAddress/ns:Region)[1]', 'NVARCHAR(100)'),
    Customer.value('(ns:BillingAddress/ns:Country)[1]', 'NVARCHAR(10)'),
    Customer.value('(ns:SelfBillingIndicator)[1]', 'BIT')
FROM @xmlData.nodes('/ns:AuditFile/ns:MasterFiles/ns:Customer') AS T(Customer)
WHERE NOT EXISTS (
    SELECT 1 FROM Customer 
    WHERE CustomerID = Customer.value('(ns:CustomerID)[1]', 'NVARCHAR(50)')
);


-- Inserir dados na tabela Invoice se não existirem duplicatas
WITH XMLNAMESPACES ('urn:OECD:StandardAuditFile-Tax:PT_1.04_01' AS ns)
INSERT INTO Invoice (InvoiceNo, DocumentStatus, Hash, HashControl, Period, InvoiceDate, 
                     InvoiceType, SpecialRegimes, SourceID, SystemEntryDate, CustomerID, InvoiceTotal)
SELECT
    Invoice.value('(ns:InvoiceNo)[1]', 'NVARCHAR(50)'),
    Invoice.value('(ns:DocumentStatus/ns:InvoiceStatus)[1]', 'NVARCHAR(50)'),
    Invoice.value('(ns:Hash)[1]', 'NVARCHAR(255)'),
    Invoice.value('(ns:HashControl)[1]', 'NVARCHAR(50)'),
    Invoice.value('(ns:Period)[1]', 'INT'),
    Invoice.value('(ns:InvoiceDate)[1]', 'DATE'),
    Invoice.value('(ns:InvoiceType)[1]', 'NVARCHAR(50)'),
    Invoice.value('(ns:SpecialRegimes)[1]', 'NVARCHAR(255)'),
    Invoice.value('(ns:SourceID)[1]', 'NVARCHAR(50)'),
    Invoice.value('(ns:SystemEntryDate)[1]', 'DATETIME'),
    Invoice.value('(ns:CustomerID)[1]', 'NVARCHAR(50)'),
    Invoice.value('(ns:DocumentTotals/ns:GrossTotal)[1]', 'DECIMAL(18, 2)')
FROM @xmlData.nodes('/ns:AuditFile/ns:SourceDocuments/ns:SalesInvoices/ns:Invoice') AS T(Invoice)
WHERE NOT EXISTS (
    SELECT 1 FROM Invoice 
    WHERE InvoiceNo = Invoice.value('(ns:InvoiceNo)[1]', 'NVARCHAR(50)')
);


-- Inserir dados na tabela LineItem (Linhas de Fatura) se não existirem duplicatas
WITH XMLNAMESPACES ('urn:OECD:StandardAuditFile-Tax:PT_1.04_01' AS ns)
INSERT INTO LineItem (InvoiceNo, LineNumber, ProductCode, Quantity, UnitPrice, CreditAmount, 
                      TaxType, TaxCountryRegion, TaxCode, TaxPercentage)
SELECT
    Invoice.value('(ns:InvoiceNo)[1]', 'NVARCHAR(50)'),
    Line.value('(ns:LineNumber)[1]', 'INT'),
    Line.value('(ns:ProductCode)[1]', 'NVARCHAR(50)'),
    Line.value('(ns:Quantity)[1]', 'DECIMAL(18, 4)'),
    Line.value('(ns:UnitPrice)[1]', 'DECIMAL(18, 4)'),
    Line.value('(ns:CreditAmount)[1]', 'DECIMAL(18, 2)'),
    Line.value('(ns:Tax/ns:TaxType)[1]', 'NVARCHAR(50)'),
    Line.value('(ns:Tax/ns:TaxCountryRegion)[1]', 'NVARCHAR(10)'),
    Line.value('(ns:Tax/ns:TaxCode)[1]', 'NVARCHAR(10)'),
    Line.value('(ns:Tax/ns:TaxPercentage)[1]', 'DECIMAL(5, 2)')
FROM @xmlData.nodes('/ns:AuditFile/ns:SourceDocuments/ns:SalesInvoices/ns:Invoice/ns:Line') AS T(Line)
CROSS APPLY @xmlData.nodes('/ns:AuditFile/ns:SourceDocuments/ns:SalesInvoices/ns:Invoice') AS T2(Invoice)
WHERE NOT EXISTS (
    SELECT 1 FROM LineItem 
    WHERE InvoiceNo = Invoice.value('(ns:InvoiceNo)[1]', 'NVARCHAR(50)')
      AND LineNumber = Line.value('(ns:LineNumber)[1]', 'INT')
);

-- Inserir dados na tabela Products se não existirem duplicatas
WITH XMLNAMESPACES ('urn:OECD:StandardAuditFile-Tax:PT_1.04_01' AS ns)
INSERT INTO Products (ProductCode, ProductDescription, ProductGroup, ProductType)
SELECT
    Product.value('(ns:ProductCode)[1]', 'NVARCHAR(50)'),
    Product.value('(ns:ProductDescription)[1]', 'NVARCHAR(255)'),
    Product.value('(ns:ProductGroup)[1]', 'NVARCHAR(50)'),
    Product.value('(ns:ProductType)[1]', 'NVARCHAR(50)')
FROM @xmlData.nodes('/ns:AuditFile/ns:MasterFiles/ns:Product') AS T(Product)
WHERE NOT EXISTS (
    SELECT 1 FROM Products 
    WHERE ProductCode = Product.value('(ns:ProductCode)[1]', 'NVARCHAR(50)')
);

-- Inserir dados na tabela TaxTable se não existirem duplicatas
WITH XMLNAMESPACES ('urn:OECD:StandardAuditFile-Tax:PT_1.04_01' AS ns)
INSERT INTO TaxTable (TaxType, TaxCountryRegion, TaxCode, Description, TaxPercentage)
SELECT
    Tax.value('(ns:TaxType)[1]', 'NVARCHAR(50)'),
    Tax.value('(ns:TaxCountryRegion)[1]', 'NVARCHAR(10)'),
    Tax.value('(ns:TaxCode)[1]', 'NVARCHAR(10)'),
    Tax.value('(ns:Description)[1]', 'NVARCHAR(255)'),
    Tax.value('(ns:TaxPercentage)[1]', 'DECIMAL(5, 2)')
FROM @xmlData.nodes('/ns:AuditFile/ns:MasterFiles/ns:TaxTable/ns:TaxTableEntry') AS T(Tax)
WHERE NOT EXISTS (
    SELECT 1 FROM TaxTable 
    WHERE TaxType = Tax.value('(ns:TaxType)[1]', 'NVARCHAR(50)')
      AND TaxCountryRegion = Tax.value('(ns:TaxCountryRegion)[1]', 'NVARCHAR(10)')
      AND TaxCode = Tax.value('(ns:TaxCode)[1]', 'NVARCHAR(10)')
	  AND TaxPercentage = Tax.value('(ns:TaxPercentage)[1]', 'DECIMAL(5, 2)')
);

-- Inserir dados na tabela MovementOfGoods se não existirem duplicatas
WITH XMLNAMESPACES ('urn:OECD:StandardAuditFile-Tax:PT_1.04_01' AS ns)
INSERT INTO MovementOfGoods (DocumentNumber, MovementDate, MovementType, CustomerID)
SELECT
    Movement.value('(ns:DocumentNumber)[1]', 'NVARCHAR(50)'),
    Movement.value('(ns:MovementDate)[1]', 'DATE'),
    Movement.value('(ns:MovementType)[1]', 'NVARCHAR(50)'),
    Movement.value('(ns:CustomerID)[1]', 'NVARCHAR(50)')
FROM @xmlData.nodes('/ns:AuditFile/ns:SourceDocuments/ns:MovementOfGoods/ns:StockMovement') AS T(Movement)
WHERE NOT EXISTS (
    SELECT 1 FROM MovementOfGoods 
    WHERE DocumentNumber = Movement.value('(ns:DocumentNumber)[1]', 'NVARCHAR(50)')
      AND MovementDate = Movement.value('(ns:MovementDate)[1]', 'DATE')
	  AND CustomerID = Movement.value('(ns:CustomerID)[1]', 'NVARCHAR(50)')
);
-- Inserir dados na tabela Payments se não existirem duplicatas
WITH XMLNAMESPACES ('urn:OECD:StandardAuditFile-Tax:PT_1.04_01' AS ns)
INSERT INTO Payments (PaymentRefNo, PaymentType, PaymentDate, CustomerID, PaymentTotal)
SELECT
    Payment.value('(ns:PaymentRefNo)[1]', 'NVARCHAR(50)'),
    Payment.value('(ns:PaymentType)[1]', 'NVARCHAR(50)'),
    Payment.value('(ns:PaymentDate)[1]', 'DATE'),
    Payment.value('(ns:CustomerID)[1]', 'NVARCHAR(50)'),
    Payment.value('(ns:DocumentTotals/ns:GrossTotal)[1]', 'DECIMAL(18, 2)')
FROM @xmlData.nodes('/ns:AuditFile/ns:SourceDocuments/ns:Payments/ns:Payment') AS T(Payment)
WHERE NOT EXISTS (
    SELECT 1 FROM Payments 
    WHERE PaymentRefNo = Payment.value('(ns:PaymentRefNo)[1]', 'NVARCHAR(50)')
);


    -- Após o processamento, apagar o ficheiro
    DECLARE @deleteCommand NVARCHAR(255);
    SET @deleteCommand = 'DEL ' + @filePath;

    -- Executar o comando de exclusão usando xp_cmdshell
    EXEC xp_cmdshell @deleteCommand;

    -- Desabilitar xp_cmdshell após o uso
    EXEC sp_configure 'xp_cmdshell', 0;
    RECONFIGURE;

	PRINT 'Ficheiro importado com sucesso';
END;
GO
