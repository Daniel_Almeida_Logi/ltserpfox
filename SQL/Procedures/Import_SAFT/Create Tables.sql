-- Apagar as tabelas, caso existam
IF OBJECT_ID('dbo.Header', 'U') IS NOT NULL DROP TABLE dbo.Header;
IF OBJECT_ID('dbo.CompanyAddress', 'U') IS NOT NULL DROP TABLE dbo.CompanyAddress;
IF OBJECT_ID('dbo.Customer', 'U') IS NOT NULL DROP TABLE dbo.Customer;
IF OBJECT_ID('dbo.Invoice', 'U') IS NOT NULL DROP TABLE dbo.Invoice;
IF OBJECT_ID('dbo.LineItem', 'U') IS NOT NULL DROP TABLE dbo.LineItem;
IF OBJECT_ID('dbo.Products', 'U') IS NOT NULL DROP TABLE dbo.Products;
IF OBJECT_ID('dbo.TaxTable', 'U') IS NOT NULL DROP TABLE dbo.TaxTable;
IF OBJECT_ID('dbo.MovementOfGoods', 'U') IS NOT NULL DROP TABLE dbo.MovementOfGoods;
IF OBJECT_ID('dbo.Payments', 'U') IS NOT NULL DROP TABLE dbo.Payments;

-- Criação da tabela Header
CREATE TABLE Header (
    AuditFileVersion NVARCHAR(50),
    CompanyID NVARCHAR(50),
    TaxRegistrationNumber NVARCHAR(50),
    TaxAccountingBasis NVARCHAR(5),
    CompanyName NVARCHAR(255),
    BusinessName NVARCHAR(255),
    FiscalYear INT,
    StartDate DATE,
    EndDate DATE,
    CurrencyCode NVARCHAR(10),
    DateCreated DATE,
    TaxEntity NVARCHAR(50),
    ProductCompanyTaxID NVARCHAR(50),
    SoftwareCertificateNumber NVARCHAR(50)
);

-- Criação da tabela CompanyAddress
CREATE TABLE CompanyAddress (
    AddressDetail NVARCHAR(255),
    City NVARCHAR(100),
    PostalCode NVARCHAR(20),
    Region NVARCHAR(100),
    Country NVARCHAR(10)
);

-- Criação da tabela Customer
CREATE TABLE Customer (
    CustomerID NVARCHAR(50),
    AccountID NVARCHAR(50),
    CustomerTaxID NVARCHAR(50),
    CompanyName NVARCHAR(255),
    BillingAddressDetail NVARCHAR(255),
    BillingCity NVARCHAR(100),
    BillingPostalCode NVARCHAR(20),
    BillingRegion NVARCHAR(100),
    BillingCountry NVARCHAR(10),
    SelfBillingIndicator BIT
);

-- Criação da tabela Invoice
CREATE TABLE Invoice (
    InvoiceNo NVARCHAR(50),
    DocumentStatus NVARCHAR(50),
    Hash NVARCHAR(255),
    HashControl NVARCHAR(50),
    Period INT,
    InvoiceDate DATE,
    InvoiceType NVARCHAR(50),
    SpecialRegimes NVARCHAR(255),
    SourceID NVARCHAR(50),
    SystemEntryDate DATETIME,
    CustomerID NVARCHAR(50),
    InvoiceTotal DECIMAL(18, 2)
);

-- Criação da tabela LineItem
CREATE TABLE LineItem (
    InvoiceNo NVARCHAR(50),
    LineNumber INT,
    ProductCode NVARCHAR(50),
    Quantity DECIMAL(18, 4),
    UnitPrice DECIMAL(18, 4),
    CreditAmount DECIMAL(18, 2),
    TaxType NVARCHAR(50),
    TaxCountryRegion NVARCHAR(10),
    TaxCode NVARCHAR(10),
    TaxPercentage DECIMAL(5, 2)
);

-- Criação da tabela Products
CREATE TABLE Products (
    ProductCode NVARCHAR(50),
    ProductDescription NVARCHAR(255),
    ProductGroup NVARCHAR(50),
    ProductType NVARCHAR(50)
);

-- Criação da tabela TaxTable
CREATE TABLE TaxTable (
    TaxType NVARCHAR(50),
    TaxCountryRegion NVARCHAR(10),
    TaxCode NVARCHAR(10),
    Description NVARCHAR(255),
    TaxPercentage DECIMAL(5, 2)
);

-- Criação da tabela MovementOfGoods
CREATE TABLE MovementOfGoods (
    DocumentNumber NVARCHAR(50),
    MovementDate DATE,
    MovementType NVARCHAR(50),
    CustomerID NVARCHAR(50)
);

-- Criação da tabela Payments
CREATE TABLE Payments (
    PaymentRefNo NVARCHAR(50),
    PaymentType NVARCHAR(50),
    PaymentDate DATE,
    CustomerID NVARCHAR(50),
    PaymentTotal DECIMAL(18, 2)
);
