
/* 
	Otimiza SQL extrator externo
		
	exec up_otimize_database

*/


SET ANSI_NULLS OFF 
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_otimize_database]') IS NOT NULL
	drop procedure dbo.up_otimize_database
go


create procedure dbo.up_otimize_database



/* WITH ENCRYPTION */ 

AS
BEGIN


	ALTER INDEX ALL ON [dbo].[stocks] REBUILD
	ALTER INDEX ALL ON [dbo].[acertos] REBUILD
	ALTER INDEX ALL ON [dbo].[clientes] REBUILD
	ALTER INDEX ALL ON [dbo].[devolucoes] REBUILD
	ALTER INDEX ALL ON [dbo].[pagamentos] REBUILD
	ALTER INDEX ALL ON [dbo].[sellIns] REBUILD
	ALTER INDEX ALL ON [dbo].[sellOuts] REBUILD
	DBCC SHRINKDATABASE ('msb-extractor', 10);
	DBCC SHRINKDATABASE ('msb-extractor-test', 10);


END


Go
Grant Execute on dbo.up_otimize_database to Public
Grant control on dbo.up_otimize_database to Public
Go