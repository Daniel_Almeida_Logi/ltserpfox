USE [msb-extractor]
GO
/****** Object:  StoredProcedure [dbo].[up_sync_dados_ado_devolucoes]    Script Date: 01/02/2023 11:17:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER procedure [dbo].[up_sync_dados_ado_devolucoes]

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON


DECLARE @primtable    SYSNAME,
        @sectable     SYSNAME,
        @txtSQL         VARCHAR(max),
		@sql varchar(max),
		@sql1 varchar(max),
		@sql2 varchar(max),
		@result varchar(max)

DECLARE @myTable TABLE
(
    txtSQL VARCHAR(MAX)
)


------------------ DEVOLUCOES ----------------
PRINT 'DEVOLUCOES'
SELECT  @primtable = 'devolucoes',
        @sectable = 'devolucoes'

INSERT INTO @myTable
( 
    [txtSQL] 
)
SELECT  'ALTER TABLE [msb-extractor].dbo.[' + 
    @sectable + 
    '] ADD [' + 
    a.[name] + 
    '] [' + 
    typ.[name] + 
    ']' + 
    CASE typ.[name]
        WHEN 'decimal' THEN '(' + CAST(a.[precision] AS VARCHAR(20)) + ',' + CAST(a.[scale] AS VARCHAR(20)) + ')'
        WHEN 'numeric' THEN '(' + CAST(a.[precision] AS VARCHAR(20)) + ',' + CAST(a.[scale] AS VARCHAR(20)) + ')'
        WHEN 'varchar' THEN '(' + CAST(a.[max_length] AS VARCHAR(20)) + ')'
        WHEN 'char' THEN '(' + CAST(a.[max_length] AS VARCHAR(20)) + ')'
        WHEN 'nvarchar' THEN '(' + CAST(a.[max_length] AS VARCHAR(20)) + ')'
        WHEN 'nchar' THEN '(' + CAST(a.[max_length] AS VARCHAR(20)) + ')'
        WHEN 'binary' THEN '(' + CAST(a.[max_length] AS VARCHAR(20)) + ')'
        WHEN 'varbinary' THEN '(' + CAST(a.[max_length] AS VARCHAR(20)) + ')'
        ELSE ''
    END
FROM    (
          SELECT    col.*
          FROM      sys.tables tbl
                    INNER JOIN sys.columns col
                        ON tbl.[object_id] = col.[object_id]
          WHERE     tbl.[name] = @primtable
        ) a
        LEFT JOIN (
                    SELECT  col.*
                    FROM    [10.100.207.10].[msb-extractor].sys.tables tbl
                            INNER JOIN [10.100.207.10].[msb-extractor].sys.columns  col
                                ON tbl.[object_id] = col.[object_id]
                    WHERE   tbl.[name] = @sectable
                  ) b
            ON a.[name] = b.[name]
        INNER JOIN sys.types typ
            ON a.[system_type_id] = typ.[system_type_id]
WHERE   b.name IS NULL

WHILE EXISTS
(
    SELECT TOP 1 1
    FROM @myTable
)
BEGIN
	
    SELECT TOP 1 @txtSQL = txtSQL FROM @myTable
    DELETE FROM @myTable WHERE [txtSQL] = @txtSQL
	--print @txtSQL
    EXEC (@txtSQL) at [10.100.207.10]
END


SELECT @result = coalesce(@result + ',', '') +  convert(varchar(30),'['+COLUMN_NAME+']') 
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = N'devolucoes' and COLUMNPROPERTY(object_id(TABLE_SCHEMA+'.'+TABLE_NAME), COLUMN_NAME, 'IsIdentity') <> 1


set @sql = N'
INSERT INTO [10.100.207.10].[msb-extractor].dbo.devolucoes ('+substring(@result,1,len(@result)) +')'
set @sql1 = N'
SELECT '+substring(@result,1,len(@result)) + ' '
set @sql2 = N'
FROM devolucoes (nolock)
WHERE stamp not IN (SELECT stamp from [10.100.207.10].[msb-extractor].dbo.devolucoes with (nolock))'

--print @sql+@sql1+@sql2
exec (@sql+@sql1+@sql2)
print('FIM DEVOLUCOES')


