
/* 
	HMR Catalogo de productos
	Devolve informação de todos os productos
		
	exec [up_extractorStocks]   'Loja 1'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_extractorStocks]') IS NOT NULL
	drop procedure dbo.up_extractorStocks
go


create procedure dbo.up_extractorStocks

@site  as varchar(18)




/* WITH ENCRYPTION */ 

AS
BEGIN
	declare @siteNr int = 0
	declare @date varchar(20) = CONVERT(varchar,getdate(),23)
	declare @time varchar(8) = CONVERT(VARCHAR(5), GETDATE(), 108) 
	declare @id  varchar(36) =  ltrim(rtrim('id_' + CONVERT(varchar,getdate(),120) + '_' + @site));



	set @siteNr = (select top 1 no from empresa(nolock) where site=@site)



	Select
	    distinct
		"id" = @id,
		"date" = @date,
		"time" = @time,
		"dateLastBuy" = isnull(CONVERT(varchar,st.uintr,23),'1900-01-01'),
		"ean" = "",	
		"name" = st.design,
		"nhrn" = st.ref,
		"stock" = case when st.stns=1 then 0 else  st.stock end,
		"pref" = convert(decimal(16,2),isnull(fprod.pref,0.00)),
		"pcl" =  convert(decimal(16,2),isnull(st.epcult,0.00)),
		"expiryDate" = CONVERT(varchar,st.validade,23),
		"vat"   = convert(decimal(6,2),isnull(taxasiva.taxa/100,0),2),
		"supplier" = isnull(st.fornecedor,''),
		"supplierVat" = isnull((select ncont from fl(nolock) where fl.no = st.fornec and fl.estab=st.fornestab),''),
		"lab" = isnull(st.u_lab,''),
		"pcp" =  convert(decimal(16,2),isnull(st.epcpond,0.00)),
		"pct" =  convert(decimal(16,2),isnull(st.epcusto,0.00))					
	From st(nolock)
	left join fprod(nolock) on fprod.ref=st.ref
	left join taxasiva(nolock) on st.tabiva = taxasiva.codigo

	where 
		st.site_nr = @siteNr
		and st.inactivo = 0	
	order by 
		st.ref



END



Go
Grant Execute on dbo.up_extractorStocks to Public
Grant control on dbo.up_extractorStocks to Public
Go

