
/* 
	Devolve informação de todos os clientes
	exec [up_extractorClients]   '2021-05-21'

*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_extractorClients]') IS NOT NULL
	drop procedure dbo.[up_extractorClients]
go

create procedure [dbo].[up_extractorClients]


@date  as date


/* WITH ENCRYPTION */ 

AS
BEGIN
	

	select 
		"id" = utstamp,
		"clientId" = RTRIM(LTRIM(STR(no))) + "." + RTRIM(LTRIM(STR(estab))),
		"clientName" = nome,
		"vatNr" = ncont,
		"gender" = sexo,
		"birthdate" = CONVERT(varchar,nascimento,23),
		"compartEntity" = despla,
		"phone" = telefone,
		"selPhone" = tlmvl,
		"email" = email,
		"snsNr" = nrss,
		"benefNr" = nbenef,
		"status" = case when inactivo = 1 then 0 else 1 end
	from
		b_utentes(nolock)
	where 
		usrdata>=@date
	
	

END


Go
Grant Execute on dbo.[up_extractorClients] to Public
Grant control on dbo.[up_extractorClients] to Public
Go

