/* 
	Extractor COMPRAS
	Devolve informacao de devolução de fornecedores
		


	exec up_extractorReturn  'Loja 1','2020-01-28'





*/

SET ANSI_NULLS OFF 
GO
SET QUOTED_IDENTIFIER OFF
GO




if OBJECT_ID('[dbo].[up_extractorReturn]') IS NOT NULL
	drop procedure dbo.up_extractorReturn
go

create procedure dbo.up_extractorReturn


@site  as varchar(18),
@data  as date

/* WITH ENCRYPTION */ 

AS
BEGIN



	DECLARE @site_nr INT
	SELECT  @site_nr=no from empresa(nolock) where site = @site

	declare @seconds int = 5


	Select
		distinct 

		"id"         =  bi.bistamp,
		"docType"    =  "GD",
		"docNumber"  = rtrim(ltrim(STR(bo.ndos))) + "-" +  rtrim(ltrim(STR(bo.obrano))),
		"docName"    = bo.nmdos,
		"date"       = isnull(CONVERT(varchar,bo.ousrdata,23),'1900-01-01')
 		,"nhrn"      = rtrim(ltrim(bi.ref))
		,"name"      = ltrim(rtrim(isnull(bi.design,'')))
		,"ean"       = ''
		,"qtt"       = abs(convert(int,bi.qtt)) - abs(convert(int,bi.u_bonus)) 
		,"pva"       = convert(decimal(16,2),bi.ettdeb/case when bi.qtt = 0 then 1 else bi.qtt end)
		,"stock"	 = convert(int,
									(ISNULL((
										SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
										FROM	sl (nolock)
										WHERE	ref = bi.REF AND    convert(datetime,convert(varchar(10),sl.ousrdata,120)+' ' + sl.ousrhora)   <=   Dateadd(ss,@seconds,convert(datetime,convert(varchar(10),bo.ousrdata,120)+' ' + bo.ousrhora)) 
										and armazem in  (select armazem from empresa_arm(nolock) where empresa_no = @site_nr) 
									),0) )
							)
		,"reason"      = case when  dbo.alltrimIsNull(bi.lobs) like '%motivo%' and bo.nmdos like '%Devol%' then  dbo.alltrimIsNull(bi.lobs) else '' end
		,"expiryDate"  = CONVERT(varchar,st.validade,23)
		,"supplier"    = isnull(st.fornecedor,'')
		,"supplierVat" = isnull((select ncont from fl(nolock) where fl.no = st.fornec and fl.estab=st.fornestab),'')
		,"lab"         = isnull(st.u_lab,'')
		,"qtBonus"     = convert(int,bi.u_bonus)
		,"vat"         = convert(decimal(6,2),isnull(taxasiva.taxa/100,0),2)
		,"discoutValue"= 0
		,"pct"           = convert(decimal(16,2),isnull(bi.edebito,0.00))
		,"pcl"           = convert(decimal(16,2),isnull(bi.u_upc,0.00))
		,"pcp"           = convert(decimal(16,2),isnull(sl.epcpond,0.00))
	From
		empresa lojas (nolock)
		inner join bo (nolock) on bo.site = Lojas.site
		inner join bi (nolock) on bi.bostamp = bo.bostamp
		inner join ts (nolock) on ts.ndos = bo.ndos
		inner join st(nolock)  on st.ref = bi.ref 
		left join taxasiva (nolock) on taxasiva.codigo = bi.tabiva
		left join sl(nolock) on sl.bistamp = bi.bistamp

	Where
		Lojas.site	= @site
		and bo.ousrdata = @data
		and ts.STOCKS =	1
		and ts.bdempresas = 'FL'
		and bo.site  = @site
		and bo.nmdos like  '%Devol. a Fornecedor%'
		and bi.ref!=''
		and st.site_nr = @site_nr
	
	


	

END

Go
Grant Execute on dbo.up_extractorReturn to Public
Grant control on dbo.up_extractorReturn to Public
Go




