/*
	Dispensas
	Devolve informação do detalhe de dispensa



	exec up_extractorSellOut  'Loja 1','2021-05-21'



	select *from ft(nolock) order by ousrdata desc

	
*/






SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_extractorSellOut]') IS NOT NULL
	drop procedure dbo.up_extractorSellOut
go



create procedure dbo.up_extractorSellOut
	@site  as varchar(18),
	@data  as date


	
/* WITH ENCRYPTION */

AS
BEGIN
	
	if Object_id('tempdb.dbo.#temp_extratorSellOut') is not null
		drop table #temp_extratorSellOut

	declare @susp bit = 0

	declare @seconds int = 5



	DECLARE @site_nr INT
	SELECT  @site_nr=no from empresa(nolock) where site = @site

	
	Select
		
		 id
		,nhrn
		,name
		,ean 
		,vat 
		,date 
		,pvp
		,qtt
		,pref
		,stock
		,compartValue
		,discountValue = case when docType= 'NC' then 0 else discountValue end
		,discountPerc = case when docType= 'NC' then 0 else discountPerc end
		,compartEntity
		,compartValue1
		,compartEntity1
		,docType
		,clientId
		,clientName
		,docName
		,docNumber
		,time
		,nratend
		,pcp
									

	From (
		Select
			distinct
		      "id"         = fi.fistamp
			 ,"nhrn"	   = RTRIM(LTRIM((case when fi.ref='' then fi.oref else fi.ref end)))
			 ,"name"       = fi.design										
			 ,"ean"        = isnull((select top 1 ltrim(rtrim(isnull(productCode,''))) from fi_trans_info(nolock) where fi_trans_info.recStamp = fi.fistamp),'')
			 ,"vat"        = convert(decimal(6,2),isnull(taxasiva.taxa/100,0),2)
			 ,date         = isnull(CONVERT(varchar,ft.fdata,23),'1900-01-01')
			 ,time         = isnull(CONVERT(varchar(5),ft.ousrhora,23),'00:00')     
			 ,pvp		   = abs(convert(decimal(19,2),fi.u_epvp))
			 ,discountValue  = case when abs(convert(decimal(19,2),convert(decimal(19,2),fi.u_epvp)*fi.qtt) -   convert(decimal(19,2),fi.etiliquido) -  convert(decimal(19,2),isnull(fi.u_ettent2,0)) - convert(decimal(19,2),isnull(fi.u_ettent1,0))) <=0 then 0
                                            else abs(convert(decimal(19,2),convert(int,fi.u_epvp)*fi.qtt) -   convert(decimal(19,2),fi.etiliquido) -  convert(decimal(19,2),isnull(fi.u_ettent2,0)) - convert(decimal(19,2),isnull(fi.u_ettent1,0))) end								    
			 ,qtt           = convert(int,case when ft.tipodoc=3 then fi.qtt*(-1) else fi.qtt end)
			 ,pref          = case when isnull((select top 1 pref from Dispensa_Eletronica_DD(nolock) where Dispensa_Eletronica_DD.id = fi.id_Dispensa_Eletronica_D and fi.id_Dispensa_Eletronica_D!=''),0) = 0
								 then 
									isnull(convert(decimal(19,2),fi.u_epref),0) 
								 else
									convert(decimal(19,2),isnull((select top 1 pref from Dispensa_Eletronica_DD(nolock) where Dispensa_Eletronica_DD.id = fi.id_Dispensa_Eletronica_D and fi.id_Dispensa_Eletronica_D!=''),0))	
								 end
			 ,stock			= convert(int,
											(ISNULL((
												SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
												FROM	sl (nolock)
												WHERE	ref = fi.REF AND    convert(datetime,convert(varchar(10),sl.ousrdata,120)+' ' +  sl.ousrhora)   <=  Dateadd(ss,@seconds,convert(datetime,convert(varchar(10),ft.ousrdata,120)+' ' + ft.ousrhora)) 
												and armazem in  (select armazem from empresa_arm(nolock) where empresa_no = @site_nr) 
											),0) )
									)
			
 			,compartValue  = convert(decimal(19,2),fi.u_ettent1) 
			,compartEntity = ft2.u_design 
			,compartValue1  =  convert(decimal(19,2),fi.u_ettent2)
			,compartEntity1 =  ft2.u_design2
			,fi.ousrdata
			,docType       = case when ft.tipodoc = 3 then 'NC' else 'FT' end
			,clientId      = rtrim(ltrim(STR(ft.no))) + "." + rtrim(ltrim(STR(ft.estab)))
			,clientName    = '***'
			,docName       = ft.nmdoc
			,docNumber     = rtrim(ltrim(STR(ft.ndoc))) + "-" +  rtrim(ltrim(STR(ft.fno)))
			,discountPerc  = isnull(fi.desc2,0)
			,pcp		   = abs(convert(decimal(19,2),fi.epcp))
			,nratend       = rtrim(ltrim(isnull(ft.u_nratend,'')))


		From fi (nolock)
			inner join ft (nolock)	on fi.ftstamp = ft.ftstamp
			left  join  ft2 (nolock)	on ft.ftstamp = ft2.ft2stamp
			left  join fprod (nolock) on fprod.ref = fi.ref
			left  join taxasiva(nolock) on fi.tabiva = taxasiva.codigo
		Where
				ft.fdata = @data
				and (fi.ref != '' or fi.oref!='')
				and ft.site = @site
				and ft.tipodoc !=4
	) a
	order by
			ousrdata desc
	

	if Object_id('tempdb.dbo.#temp_extratorSellOut') is not null
		drop table #temp_extratorSellOut




END

GO
Grant Execute on dbo.up_extractorSellOut to Public
Grant control on dbo.up_extractorSellOut to Public
Go



