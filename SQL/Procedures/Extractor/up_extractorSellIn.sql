/* 
	Extractor COMPRAS
	Devolve informacao de linhas das Compras, encomendas a fornecedor
		


	exec up_extractorSellIn  'Loja 1','2020-02-06'





*/

SET ANSI_NULLS OFF 
GO
SET QUOTED_IDENTIFIER OFF
GO



if OBJECT_ID('[dbo].[up_extractorSellIn]') IS NOT NULL
	drop procedure dbo.up_extractorSellIn
go

create procedure dbo.up_extractorSellIn


@site  as varchar(18),
@data  as date

/* WITH ENCRYPTION */ 

AS
BEGIN



	DECLARE @site_nr INT
	SELECT  @site_nr=no from empresa(nolock) where site = @site

	declare @seconds int = 5
	


	Select
		distinct 

		"id"            =  fn.fnstamp
		,"docType"       =  case when fo.docnome like  '%V/Factura%'       then  'REC'   
								        when fo.docnome like  '%V/Nt. Crédito%'   then  'ARE' 
										when fo.docnome like  '%V/Nt. Débito%'    then  'ARE' 
										when fo.docnome like  '%V/Vd. Dinhei.%'   then  'ARE' 
										when fo.docnome like  '%V/Guia%'          then  'REC' 
										else 'RDF' end
		,"docNumber"     = rtrim(ltrim(STR(fo.doccode))) + "-" +fo.adoc
		,"docName"       = fo.docnome
		,"date"          = isnull(CONVERT(varchar,fo.ousrdata,23),'1900-01-01')
		,"time"          = CONVERT(varchar(5),fo.ousrhora)
 		,"nhrn"         = rtrim(ltrim(fn.ref))
		,"name"         = ltrim(rtrim(isnull(fn.design,'')))
		,"ean"          = ''
		,"qtt"          = convert(int,fn.qtt) - convert(int,u_bonus)
		,"pva"          = convert(int,fn.etiliquido/case when fn.qtt = 0 then 1 else fn.qtt end)
		,"stock"			= convert(int,
								(ISNULL((
									SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
									FROM	sl (nolock)
									WHERE	ref = fn.REF AND    convert(datetime,convert(varchar(10),sl.ousrdata,120)+' ' + sl.ousrhora)   <=  Dateadd(ss,@seconds,convert(datetime,convert(varchar(10),fo.ousrdata,120)+' ' + fo.ousrhora)) 
									and  armazem in  (select armazem from empresa_arm(nolock) where empresa_no = @site_nr) 
								),0) )
						)
		,"qttBonus"     = convert(int,u_bonus)
		,"qtEnc"        = convert(int,isnull((select top 1  qtt from bi(nolock) where bistamp = fn.bistamp order by ousrdata desc),0))
		,"expiryDate"    = CONVERT(varchar,st.validade,23)
		,"supplier"      = isnull(st.fornecedor,'')
		,"supplierVat"   = isnull((select ncont from fl(nolock) where fl.no = st.fornec and fl.estab=st.fornestab),'')
		,"lab"           = isnull(st.u_lab,'')
		,"discountValue" = 0
		,"vat"           = convert(decimal(6,2),isnull(taxasiva.taxa/100,0),2)
		,"pct"           = convert(decimal(16,2),isnull(fn.epv,0.00))
		,"pcl"           = convert(decimal(16,2),isnull(fn.u_upc,0.00))
		,"discountPerc"  = isnull(fn.desconto,0)
		,"pcp"           = convert(decimal(16,2),isnull(sl.epcpond,0.00))
	From
		empresa	lojas (nolock)
		inner join fo (nolock)  on fo.site = Lojas.site
		inner join fn (nolock)  on fn.fostamp = fo.fostamp
		inner join st (nolock)  on st.ref = fn.ref and st.site_nr = @site_nr 
		inner join cm1 (nolock) on cm1.cm = fo.doccode
		left join taxasiva(nolock) on fn.tabiva = taxasiva.codigo
		left join sl(nolock) on sl.fnstamp = fn.fnstamp
		
	Where
		Lojas.site	    = @site
		and fo.ousrdata	 = @data
		and cm1.FOLANSL = 1
		and fo.site   = @site
	


	

END

Go
Grant Execute on dbo.up_extractorSellIn to Public
Grant control on dbo.up_extractorSellIn to Public
Go



