/* 
	HMR MOVIMENTOS
	Devolve informação da client que envia os dados
		
	exec up_extractorSender  'Loja 1'

*/

SET ANSI_NULLS OFF 
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_extractorSender]') IS NOT NULL
	drop procedure dbo.up_extractorSender
go


create procedure dbo.up_extractorSender


@site  as varchar(18)


/* WITH ENCRYPTION */ 

AS
BEGIN
	

	select 
		"id" = infarmed,
		"name" = nomabrv,
		"vatNr" = ncont,
		"postalCode" = codpost
	from
		empresa(nolock)
	where 
		site = @site
	
	

END

Go
Grant Execute on dbo.up_extractorSender to Public
Grant control on dbo.up_extractorSender to Public
Go