
/* 
	Acertos de stock
		
	exec [up_extractorMovimentosProd]   'Loja 1','2021-06-16'


*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_extractorMovimentosProd]') IS NOT NULL
	drop procedure dbo.up_extractorMovimentosProd
go


create procedure dbo.up_extractorMovimentosProd

@site  as varchar(18),
@data  as date





/* WITH ENCRYPTION */ 

AS
BEGIN
	declare @siteNr int = 0



	set @siteNr = (select top 1 no from empresa(nolock) where site=@site)


	Select
	    distinct
	
			 "id"           = sl.slstamp
			,"date"         = isnull(CONVERT(varchar,sl.ousrdata,23),'1900-01-01')
			,"nhrn"         = rtrim(ltrim(isnull(sl.ref,''))) 
			,"name"         = ltrim(rtrim(sl.design))	
			,"qtt"          = convert(int,sl.qtt)
			,"reason"       = ""
			,"stock"        = convert(int,
											(ISNULL((
												SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
												FROM	sl (nolock) sl1
												WHERE	sl1.ref = sl.REF AND    convert(datetime,convert(varchar(10),sl1.ousrdata,120)+' ' + sl1.ousrhora)   <=  convert(datetime,convert(varchar(10),sl.ousrdata,120)+' ' + sl.ousrhora) 
												and armazem in  (select armazem from empresa_arm(nolock) where empresa_no = @siteNr) 	
											),0) )
										)
			,"ean"           = ''
			,"expiryDate"    = CONVERT(varchar,st.validade,23)
			,"supplier"      = isnull(st.fornecedor,'')
			,"supplierVat"   = isnull((select ncont from fl(nolock) where fl.no = st.fornec and fl.estab=st.fornestab),'')
			,"lab"           = isnull(st.u_lab,'')
			,"discountValue" = 0
			,"vat"           = convert(decimal(6,2),isnull(taxasiva.taxa/100,0),2)
			,"pcl"           = convert(decimal(16,2),isnull(sl.epcpond,0.00))
			,"pcp"           = convert(decimal(16,2),isnull(sl.epcpond,0.00))


		From sl(nolock)
			inner join st (nolock)  on st.ref = sl.ref and st.site_nr = @siteNr 
			left join taxasiva(nolock) on st.tabiva = taxasiva.codigo
		where 
			sl.ousrdata = @data
			and sl.armazem = @siteNr
			and (cmdesc like '%Acerto%' or  cmdesc like '%interno%' or cmdesc like '%consumo%') 
		
			




END





Go
Grant Execute on dbo.up_extractorMovimentosProd to Public
Grant control on dbo.up_extractorMovimentosProd to Public
Go

