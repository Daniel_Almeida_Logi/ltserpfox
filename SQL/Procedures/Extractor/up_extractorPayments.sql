/*
	Dispensas
	Devolve informação de pagamento da venda



	exec up_extractorPayments '21364171214XL2','Loja 1'





	select *from b_pagcentral(nolock) where nrAtend = '213631731427KJ' order by oData desc

	
*/






SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_extractorPayments]') IS NOT NULL
	drop procedure dbo.up_extractorPayments
go



create procedure dbo.up_extractorPayments
	@nrAtend as varchar(100),
	@site    as varchar(18)

	
/* WITH ENCRYPTION */

AS
BEGIN
	

	If OBJECT_ID('tempdb.dbo.#tbltemp') IS NOT NULL
			drop table #tbltemp ;

				

	DECLARE @sql varchar(max) = ''
	


	--------------
	--PaymentInfo

	declare @epaga1 varchar(50) 
	declare @epaga1_ varchar(50) 
	declare @evdinheiro varchar(50)
	declare @evdinheiro_ varchar(50)
	declare @echtotal varchar(50)
	declare @echtotal_ varchar(50)
	declare @epaga2 varchar(50) 
	declare @epaga2_ varchar(50) 
	declare @epaga3 varchar(50) 
	declare @epaga3_ varchar(50) 
	declare @epaga4 varchar(50) 
	declare @epaga4_ varchar(50) 
	declare @epaga5 varchar(50) 
	declare @epaga5_ varchar(50) 
	declare @epaga6 varchar(50) 
	declare @epaga6_ varchar(50) 

	DECLARE @textvalue varchar(15)


	DECLARE @id_lt varchar(100) =''	
	DECLARE @site_nr INT
	select @id_lt = rtrim(ltrim(id_lt)), @site_nr= no from empresa(nolock) where site = @site


	DECLARE @downloadURL VARCHAR(100) = ''
	SELECT  @downloadURL = rtrim(ltrim(textValue)) FROM B_Parameters(nolock) WHERE stamp='ADM0000000313'


	DECLARE @imagemURL varchar(300) 
	set   @imagemURL = @downloadURL + @id_lt
	
	select @textvalue = textvalue from B_Parameters where stamp='ADM0000000260'
	IF @textvalue = 'EURO'
	begin
		select @epaga1 = design, @epaga1_ = right(campoFact,len(campoFact)-4) from B_modoPag(nolock) where campoFact like '%epaga1%'  
		select @evdinheiro = design, @evdinheiro_ = right(campoFact,len(campoFact)-4)  from B_modoPag(nolock) where campoFact like '%evdinheir%'  
		select @echtotal = design, @echtotal_ = right(campoFact,len(campoFact)-3)  from B_modoPag(nolock) where campoFact like '%echtotal%'  
		select @epaga2 = design, @epaga2_ = right(campoFact,len(campoFact)-4) from B_modoPag(nolock) where campoFact like '%epaga2%'  
		select @epaga3 = design, @epaga3_ = right(campoFact,len(campoFact)-4) from B_modoPag(nolock) where campoFact like '%epaga3%'  
		select @epaga4 = design, @epaga4_ = right(campoFact,len(campoFact)-4) from B_modoPag(nolock) where campoFact like '%epaga4%'  
		select @epaga5 = design, @epaga5_ = right(campoFact,len(campoFact)-4) from B_modoPag(nolock) where campoFact like '%epaga5%' 
		select @epaga6 = design, @epaga6_ = right(campoFact,len(campoFact)-4) from B_modoPag(nolock) where campoFact like '%epaga6%'  
	end
	ELSE
	begin
		select @epaga1 = design, @epaga1_ = right(campoFact,len(campoFact)-4) from B_modoPag_moeda(nolock) where campoFact like '%epaga1%'  
		select @evdinheiro = design, @evdinheiro_ = right(campoFact,len(campoFact)-4)  from B_modoPag_moeda(nolock) where campoFact like '%evdinheir%'  
		select @echtotal = design, @echtotal_ = right(campoFact,len(campoFact)-3)  from B_modoPag_moeda(nolock) where campoFact like '%echtotal%'  
		select @epaga2 = design, @epaga2_ = right(campoFact,len(campoFact)-4) from B_modoPag_moeda(nolock) where campoFact like '%epaga2%' 
		select @epaga3 = design, @epaga3_ = right(campoFact,len(campoFact)-4) from B_modoPag_moeda(nolock) where campoFact like '%epaga3%'  
		select @epaga4 = design, @epaga4_ = right(campoFact,len(campoFact)-4) from B_modoPag_moeda(nolock) where campoFact like '%epaga4%'  
		select @epaga5 = design, @epaga5_ = right(campoFact,len(campoFact)-4) from B_modoPag_moeda(nolock) where campoFact like '%epaga5%' 
		select @epaga6 = design, @epaga6_ = right(campoFact,len(campoFact)-4) from B_modoPag_moeda(nolock) where campoFact like '%epaga6%'  
	end

	
	--------------
		
	set	@sql = @sql + ' 

		create table #tbltemp
		(
			code varchar(100),
			total decimal(16,2)
		)

		insert into #tbltemp
		select top 1 ''' + lower(isnull(@epaga1,'')) + ''', ' + @epaga1_ + '
			from 
				B_pagCentral(nolock)
			where 
				site = '''+ @site + '''
				and  nrAtend='''+ @nrAtend + '''
			order by oData desc

		insert into #tbltemp
		select top 1 ''' + lower(isnull(@epaga2,'')) + ''', ' + @epaga2_ + '
			from 
				B_pagCentral(nolock)
			where 
				site = '''+ @site + '''
				and  nrAtend='''+ @nrAtend + '''
			order by oData desc

			
		insert into #tbltemp
		select top 1 ''' + lower(isnull(@epaga3,'')) + ''', ' + @epaga3_ + '
			from 
				B_pagCentral(nolock)
			where 
				site = '''+ @site + '''
				and  nrAtend='''+ @nrAtend + '''
			order by oData desc
		

		insert into #tbltemp
		select top 1 ''' + lower(isnull(@epaga4,'')) + ''', ' + @epaga4_ + '
			from 
				B_pagCentral(nolock)
			where 
				site = '''+ @site + '''
				and  nrAtend='''+ @nrAtend + '''
			order by oData desc


		insert into #tbltemp
		select top 1 ''' + lower(isnull(@epaga5,'')) + ''', ' + @epaga5_ + '
			from 
				B_pagCentral(nolock)
			where 
				site = '''+ @site + '''
				and  nrAtend='''+ @nrAtend + '''
			order by oData desc


		insert into #tbltemp
		select top 1 ''' + lower(isnull(@epaga6,'')) + ''', ' + @epaga6_ + '
			from 
				B_pagCentral(nolock)
			where 
				site = '''+ @site + '''
				and  nrAtend='''+ @nrAtend + '''
			order by oData desc
		

		insert into #tbltemp
		select top 1 ''' + lower(isnull(@evdinheiro,'')) + ''', ' + @evdinheiro_ + '
			from 
				B_pagCentral(nolock)
			where 
				site = '''+ @site + '''
				and  nrAtend='''+ @nrAtend + '''
			order by oData desc
		

		 insert into #tbltemp
		select top 1 ''' + lower(isnull(@echtotal,'')) + ''', ' + @echtotal_ + '
			from 
				B_pagCentral(nolock)
			where 
				site = '''+ @site + '''
				and  nrAtend='''+ @nrAtend + '''
			order by oData desc

		 select *
		 from  #tbltemp
		'
	

	print @sql
		
	EXECUTE (@sql)


				



	If OBJECT_ID('tempdb.dbo.#tbltemp') IS NOT NULL
			drop table #tbltemp ;





END

GO
Grant Execute on dbo.up_extractorPayments to Public
Grant control on dbo.up_extractorPayments to Public
Go



