/* save parameters from every database 
	

*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_parameters]') IS NOT NULL
	drop procedure dbo.up_save_parameters
go

create procedure [dbo].[up_save_parameters]
	 @id		VARCHAR(254)
	,@name		VARCHAR(254)
	,@value		VARCHAR(254)
	,@type		int
	,@typeDesc  VARCHAR(50)
	,@id_lt		VARCHAR(50) 
	,@site		VARCHAR(50) 
	,@dbName	VARCHAR(50) 

AS
SET NOCOUNT ON
BEGIN	
	Declare @date datetime
	set @date = GETDATE()

	IF(NOT EXISTS (SELECT * FROM [parameters] where id = @id and type=@type and id_lt=@id_lt and site = @site and dbName=@dbName))
	BEGIN
		INSERT INTO  [parameters] (stamp,id,name,value,type,typeDesc,id_lt,site,dbName,ousrdata,usrdata)
		VALUES(LEFT(NEWID(),36),@id,@name,@value,@type,@typeDesc,@id_lt,@site,@dbName,@date, @date)


	END
	ELSE 
	BEGIN
		update [parameters]
			SET
				value = @value,
				name= @name,
				usrdata = @date
			WHERE id = @id and type=@type and id_lt=@id_lt and site = @site and dbName=@dbName
	
	END



END 
Grant Execute on dbo.up_save_parameters to Public
Grant control on dbo.up_save_parameters to Public
GO