/* inserir informação de uma conexao ou varias 
	


exec up_get_connectionDBS ''
exec up_get_connectionDBS 'LTDEV30'
exec up_get_connectionDBS 'LTDEV30,LtDemo'


*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_connectionDBS]') IS NOT NULL
	drop procedure dbo.up_get_connectionDBS
go

create procedure [dbo].[up_get_connectionDBS]
	@ID_CL								AS VARCHAR(max)


/* with encryption */
AS
SET NOCOUNT ON
BEGIN	
	DECLARE  @token			AS VARCHAR(36)

	If OBJECT_ID('tempdb.dbo.#dadosNomesDB') IS NOT NULL
		DROP TABLE #dadosNomesDB

	CREATE TABLE #dadosNomesDB
	(
		name VARCHAR(50),
	)

	SELECT TOP 1 @token =token from connectionTokenDbs (NOLOCK) order by usrdata desc

	IF(@ID_CL!='')
	BEGIN
		insert into #dadosNomesDB (name)
		SELECT value FROM STRING_SPLIT(@ID_CL, ',');
	END
	ELSE
	BEGIN
		insert into #dadosNomesDB (name)
		SELECT name FROM connectionDBS WHERE token=@TOKEN
	END 


		SELECT 
			distinct server, dbname, port,
			databaseCon_jdbc='jdbc:sqlserver://' + server + ':' + Convert(varchar(10),port) + ';applicationName=LTSConnection;DatabaseName=' + dbname
		FROM connectionDBS (NOLOCK)
		WHERE TOKEN=@TOKEN AND NAME IN (SELECT NAME FROM #dadosNomesDB) 

	

	If OBJECT_ID('tempdb.dbo.#dadosNomesDB') IS NOT NULL
		DROP TABLE #dadosNomesDB

END 
Grant Execute on dbo.up_get_connectionDBS to Public
Grant control on dbo.up_get_connectionDBS to Public
GO