/* get date da ultima 
	
exec up_get_parametersConnections 'ADM0000000146' ,3,'1'
exec up_get_parametersConnections 'ADM0000000146' ,3,''

*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_parametersConnections]') IS NOT NULL
	drop procedure dbo.up_get_parametersConnections
go

create procedure [dbo].[up_get_parametersConnections]
		@id				varchar(254),
		@type           int ,
		@value			VARCHAR(254)
/* with encryption */
AS
SET NOCOUNT ON
BEGIN	

	DECLARE @token VARCHAR(36)

	SET @token = (SELECT top 1 token  from connectionTokenDbs (NOLOCK) order by usrdata desc)


	SELECT 
		[parameters].id,
		[parameters].name,
		databaseCon_jdbc='jdbc:sqlserver://' + connectionDBS.server + ':' + Convert(varchar(10),port) + ';applicationName=LTSConnection;DatabaseName=' + connectionDBS.dbname,
		STUFF((SELECT ';' + site
				FROM [parameters] par
					INNER JOIN connectionDBS on par.dbName = connectionDBS.dbname and connectionDBS.name= par.id_lt
				WHERE id=@id AND connectionDBS.token = @token and type=@type and (value=@value or @value='') and par.dbName = [parameters].dbName
				Group by par.id ,par.name,par.site,par.type,par.value,connectionDBS.dbname,connectionDBS.server,connectionDBS.port, par.dbName 
				FOR XML PATH('')), 1, 1, '') as site,
		[parameters].type,
		[parameters].value
		FROM [parameters]
			INNER JOIN connectionDBS on [parameters].dbName = connectionDBS.dbname and connectionDBS.name= [parameters].id_lt
	WHERE id=@id  and type=@type and (value=@value or @value='')
	Group by [parameters].id ,[parameters].name,[parameters].type,[parameters].value,connectionDBS.dbname,connectionDBS.server,connectionDBS.port ,[parameters].dbName
	order by [connectionDBS].dbname desc

END 
Grant Execute on dbo.up_get_parametersConnections to Public
Grant control on dbo.up_get_parametersConnections to Public
GO

