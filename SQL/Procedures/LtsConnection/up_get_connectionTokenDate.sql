/* get date da ultima 
	


exec up_get_connectionTokenDate 


*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_connectionTokenDate]') IS NOT NULL
	drop procedure dbo.up_get_connectionTokenDate
go

create procedure [dbo].[up_get_connectionTokenDate]

/* with encryption */
AS
SET NOCOUNT ON
BEGIN	
	SELECT TOP 1 date=usrdata , timeToCallNext = (CASE WHEN DATEADD(DAY, 1, usrdata) < GETDATE() then 1 else 0 end)  from connectionTokenDbs (NOLOCK) order by usrdata desc
END 
Grant Execute on dbo.up_get_connectionTokenDate to Public
Grant control on dbo.up_get_connectionTokenDate to Public
GO