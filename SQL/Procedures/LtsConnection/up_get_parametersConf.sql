/* get parametersConf 
	
	parametersConf

exec up_get_parametersConf

*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_parametersConf]') IS NOT NULL
	drop procedure dbo.up_get_parametersConf
go

create procedure [dbo].[up_get_parametersConf]


/* with encryption */
AS
SET NOCOUNT ON
BEGIN	

	SELECT 
	   conf.tableID,
	  STUFF((SELECT ';' + id
		FROM configurationParameters (NOLOCK)  conf1
		WHERE conf1.tableID = conf.tableID AND conf1.type = conf.type AND conf1.typeDesc = conf.typeDesc
		Group by tableID ,type,typeDesc,id
		FOR XML PATH('')), 1, 1, '') as id,
			type,
			typedesc
	FROM configurationParameters (NOLOCK)  conf
	Group by tableID ,type,typeDesc
	ORDER BY 1

END 
Grant Execute on dbo.up_get_parametersConf to Public
Grant control on dbo.up_get_parametersConf to Public
GO
