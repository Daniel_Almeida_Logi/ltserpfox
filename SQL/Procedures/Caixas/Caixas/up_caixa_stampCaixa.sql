-- Ver Stamp e c�digo saco da Caixa --
--- Nota: cursor para:
----- listagem de stamp de caixas; sacos de dinheiro; altera��o do c�digo do saco
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_caixa_stampCaixa]') IS NOT NULL
	drop procedure dbo.up_caixa_stampCaixa
go

create procedure dbo.up_caixa_stampCaixa

@data DATETIME,
@site varchar(60),
@op	numeric(6,0)

/* WITH ENCRYPTION */
AS

	select 
		cxstamp
		,sacoDinheiro
		,sacoDinheiro as sacoDinheiroD
		,fuserno
	from 
		cx (nolock)
	where 
		fechada=1 
		and site=@site 
		and fuserno=@op 
		and dabrir=@data


GO
Grant Execute on dbo.up_caixa_stampCaixa to Public
Grant Control on dbo.up_caixa_stampCaixa to Public
Go
