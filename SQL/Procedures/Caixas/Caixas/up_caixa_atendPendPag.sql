-- Ver Atendimentos Pendentes de Pagamento  --
-- exec up_caixa_atendPendPag '', 0, 0, 'Loja 1'

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_caixa_atendPendPag]') IS NOT NULL
	drop procedure dbo.up_caixa_atendPendPag
go

create procedure dbo.up_caixa_atendPendPag

@nrAtend varchar(20),
@nrVendedor numeric(5),
@nrTerminal numeric(5),
@site as varchar(35)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select
	stamp,
	nrAtend, total, nrVendas, ano, b_pagCentral.no, b_pagCentral.estab,
	isnull(cm3.cmdesc,'') + ' [' + CONVERT(varchar,b_pagCentral.vendedor) + ']' as vendedor, terminal,
	convert(varchar,oData,102) as oData,
	CONVERT(bit,0) as sel,
	cliente = '[' + CONVERT(varchar,b_pagCentral.no) + ']' + '['+CONVERT(varchar,b_pagCentral.estab) + ']' + ' ' + b_utentes.nome
	,vendedorNome = isnull(cm3.cmdesc,'')
from
	B_pagCentral (nolock)
	left join cm3 (nolock) on cm3.cm=B_pagCentral.vendedor
	inner join b_utentes on b_utentes.no=B_pagCentral.no and b_utentes.estab=B_pagCentral.estab
where
	fechado = 0 and abatido = 0
	and nrAtend	= case when @nrAtend='' then nrAtend else @nrAtend end
	and b_pagCentral.vendedor = case when @nrVendedor=0 then b_pagCentral.vendedor else @nrVendedor end
	and terminal = case when @nrTerminal=0 then terminal else @nrTerminal end
	and b_pagCentral.site = @site
order by
	oData desc

GO
Grant Execute on dbo.up_caixa_atendPendPag to Public
Grant Control on dbo.up_caixa_atendPendPag to Public
GO