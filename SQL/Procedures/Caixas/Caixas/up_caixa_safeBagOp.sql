-- Operadores com Abertura - safeBag por Operador
--- Nota: pode ser necess�rio alterar esta procedure quando se adicionar novos meios de pagamento
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_caixa_safeBagOp]') IS NOT NULL
	drop procedure dbo.up_caixa_safeBagOp
go

create procedure dbo.up_caixa_safeBagOp

@Data DATETIME,
@site varchar(60)

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

/* Gudardar c�digo da loja */
declare @loja varchar(60),
		@situacao varchar(12),
		@dinheiro char(8),
		@cheque char(8)
		
select
	 @loja		= isnull((select ref from empresa (nolock) where site=@site),'')
	,@situacao	= (select case when (select count(numero_safebag)
									 from B_moneyFile_registos moneyFile_registos (nolock) 
										inner join cx (nolock) on cx.sacoDinheiro=moneyFile_registos.numero_safebag
									 where data_de_venda=@data AND sacoDinheiro!=0 AND
									 cx.fechada=1 and cx.dabrir=@data and cx.site=@site
									 AND substring(id_esegur_dep, patindex('%[^0]%',id_esegur_dep), 16)=@loja) > 0
							   then 'Encerrado' 
							   else 'Aberto' 
						  end)
	,@dinheiro	= isnull((select ref from B_modoPag where design like 'Dinheiro'),'0')
	,@cheque	= isnull((select ref from B_modoPag where design like 'Cheques'),'0')



select data, loja, fusername, fuserno,
	evdinheiro	= sum(evdinheiro), 
	echtotal	= sum(echtotal),
	sum(epaga1) as epaga1, sum(epaga2) as epaga2, /*sum(epaga3) as epaga3, sum(epaga4) as epaga4, sum(epaga5) as epaga5,*/
	efundocx	= sum(efundocx),
	valorSaco	= sum(valorSaco+saldo),
	total_caixa	= sum(total_caixa),
	situacao	= @situacao,
	quebra		= sum(valorSaco+saldo) - sum(evdinheiro + echtotal + efundocx)
from (
select DATA, loja, pnome, pno, fusername, fuserno,
	sum(evdinheiro) as evdinheiro, 
	sum(echtotal) as echtotal, 
	efundocx	= case when sacoDinheiro='0' then 0 else efundocx end,
	sum(epaga1) as epaga1, sum(epaga2) as epaga2, /*sum(epaga3) as epaga3, sum(epaga4) as epaga4, sum(epaga5) as epaga5,*/
	valorSaco as valorSaco,
	sum(saldo) as saldo,
	total_caixa = sum(evdinheiro + echtotal + epaga1 + epaga2 /*+ epaga3 + epaga4 + epaga5*/),
	sacoDinheiro
from (
	select dabrir as data, cx.site as loja, cx.pnome, cx.pno, fusername, fuserno,
		evdinheiro, echtotal, epaga1, epaga2, /*epaga3, epaga4, epaga5,*/
		efundocx =isnull((select top 1 efundocx from fcx fcx1 (nolock)
					inner join cx cx1 (nolock) on fcx1.cxstamp=cx1.cxstamp
					where fcx1.operacao='F' and cx1.fechada=1 and cx1.dabrir=@data and cx1.site=@site and cx1.fuserno=cx.fuserno and fcx1.efundocx!=0
					order by cxid),0),
		valorSaco = isnull(( select sum(valor_meiopagamento)
								from b_moneyFile_registos mfr (nolock)
								inner join cx cx1 (nolock) on cx1.sacodinheiro = mfr.numero_safebag
								where data_de_venda=@data and sacoDinheiro!='0'
								AND substring(id_esegur_dep, patindex('%[^0]%',id_esegur_dep), 16)=@loja
								AND cx1.sacoDinheiro=cx.sacoDinheiro and cx1.cxstamp=cx.cxstamp
								AND (substring(codigo_meiopagamento,4,2)=@cheque OR substring(codigo_meiopagamento,4,2)=@dinheiro) )
						,0),
		saldo = (cx.saldod+cx.saldoc),
		cx.cxstamp, cx.sacoDinheiro
	from cx (nolock)
	inner join fcx (nolock) on fcx.cxstamp=cx.cxstamp
	where fcx.operacao='F' and cx.fechada=1 and cx.dabrir=@Data and cx.site=@site
) as x
group by data, loja, pnome, pno, fusername, fuserno, efundocx, sacoDinheiro, valorsaco
) as xx
group by data, loja, fusername, fuserno


GO
Grant Execute on dbo.up_caixa_safeBagOp to Public
Grant Control on dbo.up_caixa_safeBagOp to Public
Go