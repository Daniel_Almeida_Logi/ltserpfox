/*
	Verificar se um determinado atendimento estรก pendente de pagamento
	
	exec up_caixa_cashguard_pagamentoPendente 'Loja 1', '20217172658UNT','cashguard'

	select * from b_pagcentral where fechado=0 and abatido=0

	select * from B_pagCentral_maquinas where fechado=0 and nrAtend='20191141708CU7'

	exec up_caixa_cashguard_pagamentoPendente 'Loja 1', '19192151205C1B','cashguard1'

	select * from b_parameters_site where stamp like '%42%' 


	select * from b_parameters where bool = 0 and textValue='' where 

	
		

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_caixa_cashguard_pagamentoPendente]') IS NOT NULL
	drop procedure dbo.up_caixa_cashguard_pagamentoPendente
GO

create procedure dbo.up_caixa_cashguard_pagamentoPendente
	@site varchar (20),
	@nrAtend varchar (20),
	@robot varchar (255) = ''

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	declare 
		@esperaAtendimento bit = 0,
		@porTerminal bit = 0,
		@atributoPagCental varchar(50) = '',
		@apenasDinheiro bit = 0


		set @robot = RTRIM(LTRIM(@robot))


	select top 1 @esperaAtendimento = bool from B_Parameters_site(nolock)  where stamp='ADM0000000041' and site = @site
	select top 1 @porTerminal = bool, @atributoPagCental = isnull(RTRIM(LTRIM(textValue)),'') from B_Parameters_site(nolock)  where stamp='ADM0000000042' and site = @site
	select top 1 @apenasDinheiro = bool from B_Parameters_site(nolock)  where stamp='ADM0000000119' and site = @site

	set @apenasDinheiro = isnull(@apenasDinheiro,0)

	if(@esperaAtendimento=0)
	begin


		SELECT
			nomeCliente = isnull(cl.nome,'')
			,nomeVendedor = isnull(b_us.username,'')
			,pc.[stamp]
			,pc.[nrAtend]
			,pc.[nrVendas]
			,"Total" = case when  @apenasDinheiro = 1 then pc.[evdinheiro] else pc.[Total] end
			,pc.[ano]
			,pc.[no]
			,pc.[estab]
			,pc.[vendedor]
			,pc.[nome]
			,pc.[terminal]
			,pc.[terminal_nome]
			,pc.[dinheiro]
			,pc.[mb]
			,pc.[visa]
			,pc.[cheque]
			,pc.[troco]
			,pc.[oData]
			,pc.[uData]
			,pc.[fechado]
			,pc.[abatido]
			,pc.[fechaStamp]
			,pc.[fechaUser]
			,pc.[evdinheiro]
			,pc.[epaga1]
			,pc.[epaga2]
			,pc.[echtotal]
			,pc.[evdinheiro_semTroco]
			,pc.[etroco]
			,pc.[total_bruto]
			,pc.[devolucoes]
			,pc.[ntCredito]
			,pc.[creditos]
			,pc.[cxstamp]
			,pc.[ssstamp]
			,pc.[site]
			,pc.[obs]
			,pc.[epaga3]
			,pc.[epaga4]
			,pc.[epaga5]
			,pc.[epaga6]
			,pc.[exportado]
			,pc.[pontosAt]
			,pc.[campanhas]
			,pc.[id_contab]
		FROM
			b_pagcentral pc (nolock)
			left join B_utentes cl (nolock) on cl.no = pc.no and cl.estab = pc.estab
			left join b_us (nolock) on b_us.userno = pc.vendedor
			left join B_Terminal bt (nolock) on bt.no = pc.terminal
		WHERE
			pc.nrAtend = @nrAtend
			AND pc.fechado = 0 AND pc.abatido =0
			AND pc.site = @site
			and LOWER(bt.cash_machine) = case when @porTerminal = 1 then @robot else LOWER(bt.cash_machine)  end
			and pc.evdinheiro != case when @atributoPagCental = 'evdinheiro' then 0 else -999999 end
	end else begin

		SELECT
			top 1
			nomeCliente = isnull(cl.nome,'')
			,nomeVendedor = isnull(b_us.username,'')
			,pc.[stamp]
			,pc.[nrAtend]
			,pc.[nrVendas]
			,"Total" = case when  @apenasDinheiro = 1 then pc.[evdinheiro] else pc.[Total] end
			,pc.[ano]
			,pc.[no]
			,pc.[estab]
			,pc.[vendedor]
			,pc.[nome]
			,pc.[terminal]
			,pc.[terminal_nome]
			,pc.[dinheiro]
			,pc.[mb]
			,pc.[visa]
			,pc.[cheque]
			,pc.[troco]
			,pc.[oData]
			,pc.[uData]
			,pc.[fechado]
			,pc.[abatido]
			,pc.[fechaStamp]
			,pc.[fechaUser]
			,pc.[evdinheiro]
			,pc.[epaga1]
			,pc.[epaga2]
			,pc.[echtotal]
			,pc.[evdinheiro_semTroco]
			,pc.[etroco]
			,pc.[total_bruto]
			,pc.[devolucoes]
			,pc.[ntCredito]
			,pc.[creditos]
			,pc.[cxstamp]
			,pc.[ssstamp]
			,pc.[site]
			,pc.[obs]
			,pc.[epaga3]
			,pc.[epaga4]
			,pc.[epaga5]
			,pc.[epaga6]
			,pc.[exportado]
			,pc.[pontosAt]
			,pc.[campanhas]
			,pc.[id_contab]
		FROM
			B_pagCentral_maquinas pc (nolock)
			left join B_utentes cl (nolock) on cl.no = pc.no and cl.estab = pc.estab
			left join b_us (nolock) on b_us.userno = pc.vendedor
			left join B_Terminal bt (nolock) on bt.no = pc.terminal
		WHERE
			pc.nrAtend = @nrAtend
			AND pc.fechado = 0   AND pc.abatido =0
			AND pc.site = @site
			and LOWER(bt.cash_machine) = case when @porTerminal = 1 then @robot else LOWER(bt.cash_machine)  end
			AND pc.evdinheiro != case when @atributoPagCental = 'evdinheiro' then 0 else -999999 end
		order by uData desc
	end



GO
Grant Execute on dbo.up_caixa_cashguard_pagamentoPendente to Public
Grant Control on dbo.up_caixa_cashguard_pagamentoPendente to Public
GO