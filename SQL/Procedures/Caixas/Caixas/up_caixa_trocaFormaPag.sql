-- Listagem dos meios de pagamento para Troca de Formas de Pagamento --
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_caixa_trocaFormaPag]') IS NOT NULL
	drop procedure dbo.up_caixa_trocaFormaPag
go

create procedure dbo.up_caixa_trocaFormaPag

/* WITH ENCRYPTION */

AS

	select 
		ref
		,design
		,convert(money,0) as origem
		,convert(money,0) as destino
	from 
		B_modoPag (nolock)
	where 
		permiteTroca=1

GO
Grant Execute on dbo.up_caixa_trocaFormaPag to Public
Grant Control on dbo.up_caixa_trocaFormaPag to Public
Go
