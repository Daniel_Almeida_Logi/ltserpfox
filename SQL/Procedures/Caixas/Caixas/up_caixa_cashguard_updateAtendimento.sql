/*
	Abate um atendimento pendente de pagamento
	
	exec up_caixa_cashguard_updateAtendimento 'Loja 1' ,'0000025099' ,'fechastamp' ,1,10,0,0,0,0,0,1
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_caixa_cashguard_updateAtendimento]') IS NOT NULL
	drop procedure dbo.up_caixa_cashguard_updateAtendimento
go

create procedure dbo.up_caixa_cashguard_updateAtendimento
	@site varchar (20)
	,@nratend varchar (20)
	,@stamp varchar (25)
	,@user numeric(5)
	,@dinheiro numeric(13,3)
	,@visa numeric(13,3)
	,@mb numeric(13,3)
	,@vale numeric(13,3)
	,@cheque numeric(13,3)
	,@troco numeric(13,3)
	,@dinheiro_sem_troco numeric(13,3)
	,@fechado bit

/* WITH ENCRYPTION */
AS

	
	declare @esperaAtendimento bit = 0
	declare @apenasDinheiro bit = 0

	select top 1 @esperaAtendimento = bool from B_Parameters_site(nolock)  where stamp='ADM0000000041' and site = @site
	select top 1 @apenasDinheiro = bool from B_Parameters_site(nolock)  where stamp='ADM0000000119' and site = @site

	set @apenasDinheiro = isnull(@apenasDinheiro,0)
	
	if(@esperaAtendimento=0)
	begin
		UPDATE
			b_pagcentral
		SET
			evdinheiro = @dinheiro
			,epaga1 = case when @apenasDinheiro = 1 then epaga1 + @visa else @visa end
			,epaga2 = case when @apenasDinheiro = 1 then epaga2 + @mb else @mb end 
			,epaga3 = case when @apenasDinheiro = 1 then epaga3 + @vale else @vale end  
			,echtotal = case when @apenasDinheiro = 1 then echtotal+@cheque else @cheque end  
			,etroco = @troco
			,evdinheiro_semTroco = @dinheiro_sem_troco
			,fechaStamp = @stamp
			,fechaUser = @user
			,fechado = @fechado
			,uData = getdate()
		FROM
			b_pagcentral (nolock)
		WHERE
			 nrAtend = @nratend
			 and fechado = 0 AND abatido = 0
			 AND SITE= @site
	
	  end else begin

		UPDATE
			B_pagCentral_maquinas
		SET
			evdinheiro = @dinheiro
			,epaga1 = @visa
			,epaga2 = @mb
			,epaga3 = @vale
			,echtotal = @cheque
			,etroco = @troco
			,evdinheiro_semTroco = @dinheiro_sem_troco
			,fechaStamp = @stamp
			,fechaUser = @user
			,fechado = @fechado
			,uData = getdate()
		FROM
			B_pagCentral_maquinas (nolock)
		WHERE
			 nrAtend = @nratend
			 and fechado = 0 AND abatido = 0
			 AND SITE= @site

	  end

GO
Grant Execute on dbo.up_caixa_cashguard_updateAtendimento to Public
Grant Control on dbo.up_caixa_cashguard_updateAtendimento to Public
GO