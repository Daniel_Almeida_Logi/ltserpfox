-- Ver total por pagar  --
-- exec up_caixa_totalPorPagar 'Loja 1'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_caixa_totalPorPagar]') IS NOT NULL
	drop procedure dbo.up_caixa_totalPorPagar
go

create procedure dbo.up_caixa_totalPorPagar

@site as varchar(50)

/* WITH ENCRYPTION */
AS

	select 
		isnull(SUM(total),0) as total
		,count(fechado) as nrAtend
	from 
		B_pagCentral (nolock)
	where 
		fechado = 0
		and site = @site

GO
Grant Execute on dbo.up_caixa_totalPorPagar to Public
Grant Control on dbo.up_caixa_totalPorPagar to Public
GO