-- Ver Registos do Dia --
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_caixa_registosDia]') IS NOT NULL
	drop procedure dbo.up_caixa_registosDia
go

create procedure dbo.up_caixa_registosDia

@data datetime,
@site varchar (60)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select 
	* 
from 
	B_fechoDia (nolock)
where 
	convert(varchar,dataAbriu,112)=@data 
	and site=@site


GO
Grant Execute on dbo.up_caixa_registosDia to Public
Grant Control on dbo.up_caixa_registosDia to Public
Go