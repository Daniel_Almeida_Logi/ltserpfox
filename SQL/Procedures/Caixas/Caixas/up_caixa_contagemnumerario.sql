/*
	Com 2 parametros de entrada para fazer a contagens das notas e moedas 
	no fecho de caixa.
	O @stamp - stamp da se��o 
	O @tipoNumerario - correponde a moedas ou notas

	exec up_caixa_contagemnumerario '',''
*/
 if OBJECT_ID('[dbo].[up_caixa_contagemnumerario]') IS NOT NULL
	drop procedure dbo.up_caixa_contagemnumerario
go

create procedure dbo.up_caixa_contagemnumerario
	@tipoNumerario		as varchar(50) = ''
	,@stamp				as varchar(50) = ''

/* WITH ENCRYPTION */
AS
	Select 
		id,
		contagem, 
		ssstamp
	From 
		contnumerario(nolock)
	Where
		tipo = ISNULL(LTRIM(@tipoNumerario),'') and
		ssstamp = ISNULL(LTRIM(@stamp),'')
	order by
		id asc


GO
Grant Execute On dbo.up_caixa_contagemnumerario to Public
Grant Control On dbo.up_caixa_contagemnumerario to Public
GO


