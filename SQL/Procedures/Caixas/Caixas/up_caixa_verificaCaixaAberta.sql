/* Ver Se Existe Caixa Aberta 

	exec up_caixa_verificaCaixaAberta 'Loja 2', 'Terminal 97', 0

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_caixa_verificaCaixaAberta]') IS NOT NULL
	drop procedure dbo.up_caixa_verificaCaixaAberta
go

create procedure dbo.up_caixa_verificaCaixaAberta
	@site		varchar (20),
	@term		varchar (30), -- o nome do parametro � @term mas caso @gestaoOp seja 1 recebe o nome do operador q abre a sess�o de caixa 
	@gestaoOp   bit

/* WITH ENCRYPTION */
AS
	if @gestaoOp = 0
		begin
			select 
				top 1 
				 cx.cxstamp
				,cx.ausername
				,dabrir	= convert(varchar,cx.dabrir,104)
				,ss.ssstamp
			from 
				cx (nolock)
				inner join ss (nolock) on ss.cxstamp = cx.cxstamp
			where 
				cx.site			= @site
				and cx.pnome	= @term 
				and cx.fechada  = 0
			order by 
				cx.cxid desc
		end
	else
		begin
			select 
				top 1 
				 ss.ssstamp
				,ss.ausername
				,dabrir	= convert(varchar,ss.dabrir,104)
			from 
				ss (nolock)
				
			where 
				ss.site				= @site
				and ss.ausername	= @term 
				and ss.fechada		= 0
			order by 
				cxid desc
		
		end

GO
Grant Execute on dbo.up_caixa_verificaCaixaAberta to Public
Grant Control on dbo.up_caixa_verificaCaixaAberta to Public
GO