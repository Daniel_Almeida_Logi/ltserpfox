/*
	Levanta atendimento de pagamentos centralizados
	exec up_caixa_cashguard_getStamp 'loja 1','0000026899'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_caixa_cashguard_getStamp]') IS NOT NULL
	drop procedure dbo.up_caixa_cashguard_getStamp
go

create procedure dbo.up_caixa_cashguard_getStamp
	@site varchar(20)
	,@nrAtend varchar(20)

/* WITH ENCRYPTION */
AS

	SELECT 
		* 
	FROM 
		b_pagcentral
	WHERE 
		abatido = 0 and fechado = 0 
		and nrAtend = @nrAtend
		and site = @site

GO
Grant Execute On dbo.up_caixa_cashguard_getStamp to Public
Grant Control On dbo.up_caixa_cashguard_getStamp to Public
GO