/*
	Verificar se um determinado atendimento est� pendente de pagamento

	exec up_caixa_cashguard_pagamentosPendente1 'Loja 1',''


	 use F04160A
    exec up_caixa_cashguard_pagamentosPendente 'Loja 1',''

	
		SELECT
			nomeCliente = isnull(cl.nome,'')
			,pc.*
		FROM
			B_pagCentral_maquinas pc (nolock)
			left join B_utentes cl (nolock) on cl.no = pc.no and cl.estab = pc.estab
		where
			fechado = 0 and abatido=0 and evdinheiro != 0

			select *  from B_Parameters_site(nolock) order by stamp desc



*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_caixa_cashguard_pagamentosPendente]') IS NOT NULL
	drop procedure dbo.up_caixa_cashguard_pagamentosPendente
go


create procedure dbo.up_caixa_cashguard_pagamentosPendente
	@site varchar (20),
	@robot varchar (254) = ''

/* WITH ENCRYPTION */
AS
	declare 
		@esperaAtendimento bit = 0,
		@porTerminal bit = 0,
		@atributoPagCental varchar(50) = '',
		@apenasDinheiro bit = 0

	set @robot = LOWER(RTRIM(LTRIM(@robot)))

	select top 1 @esperaAtendimento = bool from B_Parameters_site(nolock)  where stamp='ADM0000000041' and site = @site
	select top 1 @porTerminal = bool, @atributoPagCental = isnull(RTRIM(LTRIM(textValue)),'') from B_Parameters_site(nolock)  where stamp='ADM0000000042' and site = @site
	select top 1 @apenasDinheiro = bool from B_Parameters_site(nolock)  where stamp='ADM0000000119' and site = @site

	set @apenasDinheiro = isnull(@apenasDinheiro,0)

	if(@esperaAtendimento=0)
	begin
		SELECT *
		FROM 
		(
		SELECT
			top 1000
			nomeCliente = isnull(cl.nome,'')
			,pc.[stamp]
			,pc.[nrAtend]
			,pc.[nrVendas]
			,"Total" = case when @apenasDinheiro = 1 then pc.[evdinheiro] else pc.[Total] end
			,pc.[ano]
			,pc.[no]
			,pc.[estab]
			,pc.[vendedor]
			,pc.[nome]
			,pc.[terminal]
			,pc.[terminal_nome]
			,pc.[dinheiro]
			,pc.[mb]
			,pc.[visa]
			,pc.[cheque]
			,pc.[troco]
			,pc.[oData]
			,pc.[uData]
			,pc.[fechado]
			,pc.[abatido]
			,pc.[fechaStamp]
			,pc.[fechaUser]
			,pc.[evdinheiro]
			,pc.[epaga1]
			,pc.[epaga2]
			,pc.[echtotal]
			,pc.[evdinheiro_semTroco]
			,pc.[etroco]
			,pc.[total_bruto]
			,pc.[devolucoes]
			,pc.[ntCredito]
			,pc.[creditos]
			,pc.[cxstamp]
			,pc.[ssstamp]
			,pc.[site]
			,pc.[obs]
			,pc.[epaga3]
			,pc.[epaga4]
			,pc.[epaga5]
			,pc.[epaga6]
			,pc.[exportado]
			,pc.[pontosAt]
			,pc.[campanhas]
			,pc.[id_contab]
			,rn =  ROW_NUMBER() over (partition by nrAtend order by nrAtend asc)	
		FROM
			b_pagcentral pc (nolock)
			left join B_utentes cl (nolock) on cl.no = pc.no and cl.estab = pc.estab
			left join B_Terminal bt (nolock) on bt.no = pc.terminal
		WHERE
			pc.fechado = '0' AND pc.abatido ='0'
			and pc.site = @site 
			and LOWER(bt.cash_machine) = case when @porTerminal = 1 then @robot else LOWER(bt.cash_machine)  end
			and pc.evdinheiro != case when @atributoPagCental = 'evdinheiro' then 0 else -999999 end
		
		)as b
		where b.rn = 1
		order by
			b.oData


	 end else begin


	 select *
	 from 
		(SELECT
			top 1000
			nomeCliente = isnull(cl.nome,'')
				,pc.[stamp]
			,pc.[nrAtend]
			,pc.[nrVendas]
			,"Total" = case when @apenasDinheiro=1 then pc.[evdinheiro] else pc.[Total] end
			,pc.[ano]
			,pc.[no]
			,pc.[estab]
			,pc.[vendedor]
			,pc.[nome]
			,pc.[terminal]
			,pc.[terminal_nome]
			,pc.[dinheiro]
			,pc.[mb]
			,pc.[visa]
			,pc.[cheque]
			,pc.[troco]
			,pc.[oData]
			,pc.[uData]
			,pc.[fechado]
			,pc.[abatido]
			,pc.[fechaStamp]
			,pc.[fechaUser]
			,pc.[evdinheiro]
			,pc.[epaga1]
			,pc.[epaga2]
			,pc.[echtotal]
			,pc.[evdinheiro_semTroco]
			,pc.[etroco]
			,pc.[total_bruto]
			,pc.[devolucoes]
			,pc.[ntCredito]
			,pc.[creditos]
			,pc.[cxstamp]
			,pc.[ssstamp]
			,pc.[site]
			,pc.[obs]
			,pc.[epaga3]
			,pc.[epaga4]
			,pc.[epaga5]
			,pc.[epaga6]
			,pc.[exportado]
			,pc.[pontosAt]
			,pc.[campanhas]
			,pc.[id_contab]
			,rn =  ROW_NUMBER() over (partition by nrAtend order by nrAtend asc)	
		FROM
			B_pagCentral_maquinas pc (nolock)
			left join B_utentes cl (nolock) on cl.no = pc.no and cl.estab = pc.estab
			left join B_Terminal bt (nolock) on bt.no = pc.terminal
		WHERE
			pc.fechado = '0' AND pc.abatido ='0'
			and pc.site = @site
			and LOWER(bt.cash_machine) = case when @porTerminal = 1 then @robot else LOWER(bt.cash_machine)  end
			and pc.evdinheiro != case when @atributoPagCental = 'evdinheiro' then 0 else -999999 end
		
	
		)as b
		where b.rn = 1
		order by
			b.oData

	 end



GO
Grant Execute on dbo.up_caixa_cashguard_pagamentosPendente to Public
Grant Control on dbo.up_caixa_cashguard_pagamentosPendente to Public
GO




