/*
	Insere um atendimento e/ou movimento de caixa
	
	exec up_caixa_cashguard_insertAtendimento 'Loja 1', 'Cx00000099', '10.33', 1, 99, 'xxx', 'xxx', 'xxx'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_caixa_cashguard_insertAtendimento]') IS NOT NULL
	drop procedure dbo.up_caixa_cashguard_insertAtendimento
go

create procedure dbo.up_caixa_cashguard_insertAtendimento
	@site varchar (20)
	,@nratend varchar (20)
	,@total numeric(5)
	,@vendedor numeric(5)
	,@terminal numeric(5)
	,@fechaStamp varchar(28)
	,@cxstamp varchar(28)
	,@ssstamp varchar(28)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

insert into 
	B_pagCentral
	([stamp], [nrAtend], [nrVendas], [Total], [ano], [no], [estab], [vendedor], [nome], [terminal], [terminal_nome], [fechado]
	,[fechaStamp], [fechaUser], [evdinheiro], [evdinheiro_semTroco], [total_bruto], [cxstamp], [ssstamp], [site])
select 
	stamp					= ltrim(rtrim(str(year(getdate())))) + @nratend
	,nrAtend				= @nrAtend
	,nrVendas				= 0
	,Total					= @total
	,ano					= year(getdate())
	,[no]					= 200
	,estab					= 0
	,vendedor				= @vendedor
	,nome					= (select nome from b_utentes where no=200 and estab=0)
	,terminal				= @terminal
	,terminal_nome			= (select top 1 terminal from b_terminal where no=@terminal)
	,fechado				= 1
	,fechaStamp				= @fechaStamp
	,fechaUser				= @vendedor
	,evdinheiro				= @total
	,evdinheiro_semTroco	= @total
	,total_bruto			= @total
	,cxstamp				= @cxstamp
	,ssstamp				= @ssstamp
	,[site]					= @site
	

GO
Grant Execute on dbo.up_caixa_cashguard_insertAtendimento to Public
Grant Control on dbo.up_caixa_cashguard_insertAtendimento to Public
GO