/*
	Abate um atendimento pendente de pagamento
	
	exec up_caixa_cashguard_abateAtendimento 'Loja 1', '0000025099', 'fechastamp'

	select * from B_pagCentral_maquinas where fechado = 0
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_caixa_cashguard_abateAtendimento]') IS NOT NULL
	drop procedure dbo.up_caixa_cashguard_abateAtendimento
go

create procedure dbo.up_caixa_cashguard_abateAtendimento
	@site varchar (20)
	,@nratend varchar (20)
	,@stamp varchar (25)

/* WITH ENCRYPTION */
AS

		
	declare @esperaAtendimento bit = 0

	select top 1 @esperaAtendimento = bool from B_Parameters_site(nolock)  where stamp='ADM0000000041' and site = @site

	if(@esperaAtendimento=0)
	begin
		UPDATE
			b_pagcentral
		SET
			abatido = 1
			,fechado = 1
			,fechaStamp = @stamp
			,uData = getdate()
		FROM 
			b_pagcentral (nolock)
		WHERE
			nrAtend = @nratend
			and fechado = 0 
			AND abatido = 0
			and site = @site

	  end else begin

		UPDATE
			B_pagCentral_maquinas
		SET
			abatido = 1
			,fechaStamp = @stamp
			,uData = getdate()
		FROM 
			B_pagCentral_maquinas (nolock)
		WHERE
			nrAtend = @nratend
			and fechado = 0 
			AND abatido = 0
			and site = @site

	  end

GO
Grant Execute on dbo.up_caixa_cashguard_abateAtendimento to Public
Grant Control on dbo.up_caixa_cashguard_abateAtendimento to Public
GO