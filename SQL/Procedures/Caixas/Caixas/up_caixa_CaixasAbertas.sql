/* Ver Caixas Abertas 

	 exec up_caixa_CaixasAbertas 'Loja 1', 'PRO', 1

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_caixa_CaixasAbertas]') IS NOT NULL
	drop procedure dbo.up_caixa_CaixasAbertas
go

create procedure dbo.up_caixa_CaixasAbertas

	@site varchar (60)
	,@ausername varchar(30)
	,@gestaoOp   bit

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON	

	if @gestaoOp = 0
		begin
			select 
				cx.cxstamp
				,cx.site
				,cx.pnome
				,cx.pno
				,cx.cxno
				,cx.cxano
				,cx.ausername
				,cx.auserno
				,(cx.ausername+' ['+CONVERT(varchar,cx.auserno)+']') usa
				,cx.dabrir
				,cx.habrir
				,cx.fusername
				,cx.fuserno
				,(cx.fusername+' ['+CONVERT(varchar,cx.fuserno)+']') usf
				,cx.dfechar
				,cx.hfechar
				,(convert(varchar,cx.dfechar,102)+' '+cx.hfechar) as dataFecho
				,(convert(varchar,cx.dabrir,102)+' '+cx.habrir) as dataAbrir
				,cx.fechada
				,cx.sacoDinheiro
				,cx.fdStamp
				,efundocx	= cx.fundocaixa
				,'sel' = convert(bit,0)
			from 
				cx (nolock)
			where 
				cx.site=@site
				and cx.fechada = 0
			order by 
				dataAbrir
				,cx.pno
			end
	else
		begin
			select 
				ss.ssstamp
				,ss.site
				,ss.pnome
				,ss.pno
				,ss.ausername
				,ss.auserno
				,(ss.ausername+' ['+CONVERT(varchar,ss.auserno)+']') usa
				,ss.dabrir
				,ss.habrir
				,ss.fusername
				,ss.fuserno
				,(ss.fusername+' ['+CONVERT(varchar,ss.fuserno)+']') usf	
				,ss.dfechar
				,ss.hfechar
				,(convert(varchar,ss.dfechar,102)+' '+ss.hfechar) as dataFecho
				,(convert(varchar,ss.dabrir,102)+' '+ss.habrir) as dataAbrir
				,ss.fechada
				,fdStamp = ''
				,sacodinheiro = ''
				,'sel' = convert(bit,0)
				,nrAtendimento = 0
				,valorVendas = 0
				,valorReg = 0
				,valorCred = 0
				,valorCaixa = 0
				--,efundocx	= cx.fundocaixa
				--,cx.comissaoTPA
			from
				ss
			where 
				--ss.ausername = @ausername
				ss.fechada = 0
				and convert(date,ss.dabrir) != convert(date,getdate())
			--order by 
		end


GO
Grant Execute on dbo.up_caixa_CaixasAbertas to Public
Grant Control on dbo.up_caixa_CaixasAbertas to Public
GO