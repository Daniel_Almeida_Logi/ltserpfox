/* Ver Caixas do Dia 

	exec up_caixa_FechoDia '20190107', 'Loja 1', 1

	exec up_caixa_FechoDia '20230725', 'Loja 1', 1

	exec up_caixa_FechoDia '20180512', 'Loja 5', 1

	exec up_caixa_FechoDia '20230227', 'Loja 1', 0

	select * from cx
	
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_caixa_FechoDia]') IS NOT NULL
	drop procedure dbo.up_caixa_FechoDia
go

create procedure dbo.up_caixa_FechoDia

@data	  datetime,
@site	  varchar (60),
@gestaoOp bit

/* WITH ENCRYPTION */
AS
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosPagCentral'))
		DROP TABLE #dadosPagCentral

	create table #dadosPagCentral (
		ssstamp				char(25) 
		,Total_Dinheiro		numeric(13,3)
		,Total_Multibanco	numeric(13,3)
		,Total_Visa			numeric(13,3)
		,Total_Cheques		numeric(13,3)
		,Total_Devolucoes	numeric(13,3)
		,Total_Credito		numeric(13,3)
		,Nr_Vendas			numeric(5)
		,Nr_Atend			numeric(5)
		,Total_Caixa		numeric(13,3)
		,epaga3				numeric(13,3)
		,epaga4				numeric(13,3)
		,epaga5				numeric(13,3)
		,epaga6				numeric(13,3)
		,total_movExtra		numeric(13,3)
		,cxstamp			varchar(25)
		,totCompart			numeric(13,3)
	)

	-- se for a data de hoje mostra caixas abertas hoje + caixas abertas em dias anteriores
	IF @data = (select convert(varchar(35),getdate(),112))
		begin
			IF @gestaoOp = 0
				begin
					select 
						cx.cxstamp
						,cx.site
						,cx.pnome
						,cx.pno
						,cx.cxno
						,cx.cxano
						,cx.ausername
						,cx.auserno
						,(cx.ausername+' ['+CONVERT(varchar,cx.auserno)+']') usa
						--,cx.dabrir
						,(convert(varchar,cx.dabrir,102)+' '+cx.habrir) as dabrir
						,cx.habrir
						,cx.fusername
						,cx.fuserno
						,(cx.fusername+' ['+CONVERT(varchar,cx.fuserno)+']') usf
						,cx.dfechar
						,cx.hfechar
						,(convert(varchar,cx.dfechar,102)+' '+cx.hfechar) as dataFecho
						,cx.fechada
						,cx.sacoDinheiro
						,cx.fdStamp
						,efundocx	= cx.fundocaixa
						,'sel' = convert(bit,0)
						,cx.comissaoTPA
						,nrAtendimento = 0
						,valorVendas = 0
						,valorReg = 0
						,valorCred = 0
						,valorCaixa = 0
						,totCompart		= ISNULL((SELECT sum((u_ettent1 * (CASE WHEN ft.tipoDoc = 3 THEN -1 ELSE 1 END)) + (u_ettent2 * (CASE WHEN ft.tipoDoc = 3 THEN -1 ELSE 1 END))) FROM fi(NOLOCK) INNER JOIN ft(nolock) on ft.ftstamp = fi.ftstamp INNER JOIN td(nolock) on td.ndoc = ft.ndoc WHERE ft.cxstamp = cx.cxstamp and ft.anulado = 0 and fi.composto = 0 and CONVERT(varchar, ft.fdata, 112) = dabrir and ft.site = cx.site),0)
					from 
						cx (nolock)
					where 
						(dabrir = @data or fechada = 0)
						and cx.site = @site
					order by 
						cx.pno
						,cx.habrir

					print '1'

				end
			else
			
				--exec up_caixa_FechoDia '20191205', 'ATLANTICO', 1
				begin
					insert into #dadosPagCentral
					select 
						ss.ssstamp
						,Total_Dinheiro		= sum(case when left(pc.nrAtend,2) = 'Cx' then 0 else pc.evdinheiro end) --sum(pc.evdinheiro)
						,Total_Multibanco	= sum(pc.epaga2)
						,Total_Visa			= sum(pc.epaga1)
						,Total_Cheques		= sum(pc.echtotal)
						,Total_Devolucoes	= sum(pc.devolucoes)
						,Total_Credito		= sum(pc.creditos)--(select isnull(sum(erec),0) from rl where ccstamp in (select ccstamp from cc where ftstamp in (select ftstamp from ft where u_nratend=pc.nratend)) and rl.rdata=ss.dfechar)
						,Nr_Vendas			= sum(pc.nrvendas)
						,Nr_Atend			= count(distinct pc.nratend)
						,Total_Caixa		= sum(case when left(pc.nrAtend,2) = 'Cx' then 0 else pc.evdinheiro end) + sum(pc.epaga2) + sum(pc.epaga1) + sum(pc.echtotal) + sum(pc.epaga3) + sum(pc.epaga4) + sum(pc.epaga5) + sum(pc.epaga6)
						,epaga3				= sum(pc.epaga3)
						,epaga4				= sum(pc.epaga4)
						,epaga5				= sum(pc.epaga5)
						,epaga6				= sum(pc.epaga6)
						,total_movExtra		= sum(case when left(pc.nrAtend,2) = 'Cx' then pc.evdinheiro else 0 end)
						,ss.cxstamp
						,totCompart =		0
					from 
						ss (nolock) 
						inner join B_pagCentral (nolock) pc on pc.Ssstamp = ss.Ssstamp
					where 
						(dabrir = @data or fechada = 0)
						and ss.site = @site
					group by
						ss.ssstamp
						,ss.site
						,ss.dabrir
						, pc.nrAtend
						, ss.dfechar
						, ss.cxstamp
					-- 
					
				
					select 
						 ss.ssstamp
						,ss.site
						,ss.pnome
						,ss.pno
						,ss.ausername
						,ss.auserno
						,(ss.ausername+' ['+CONVERT(varchar,ss.auserno)+']') usa
						--,ss.dabrir
						,(convert(varchar,ss.dabrir,102)+' '+ss.habrir) as dabrir
						,ss.habrir
						,ss.fusername
						,ss.fuserno
						,(ss.fusername+' ['+CONVERT(varchar,ss.fuserno)+']') usf
						,ss.dfechar
						,ss.hfechar
						,(convert(varchar,ss.dfechar,102)+' '+ss.hfechar) as dataFecho
						,ss.fechada
						,fdStamp	  
						,sacodinheiro 
						,'sel' = convert(bit,0)
						,nrAtendimento	= sum(isnull(#dadosPagCentral.Nr_Atend,0))
						,valorVendas	= sum(isnull(#dadosPagCentral.Total_Caixa,0) + isnull(#dadosPagCentral.Total_Credito,0))
						,valorReg		= sum(isnull(#dadosPagCentral.Total_Devolucoes,0))
						,valorCred		= sum(isnull(#dadosPagCentral.Total_Credito,0))
						,valorCaixa		= sum(isnull(#dadosPagCentral.Total_Caixa,0) + isnull(#dadosPagcentral.total_movExtra,0))
						,totCompart		= ISNULL((SELECT sum((u_ettent1 * (CASE WHEN ft.tipoDoc = 3 THEN -1 ELSE 1 END)) + (u_ettent2 * (CASE WHEN ft.tipoDoc = 3 THEN -1 ELSE 1 END))) FROM fi(NOLOCK) INNER JOIN ft(nolock) on ft.ftstamp = fi.ftstamp INNER JOIN td(nolock) on td.ndoc = ft.ndoc WHERE ft.ssstamp = ss.ssstamp and ft.anulado = 0 and fi.composto = 0 and CONVERT(varchar, ft.fdata, 112) = dabrir and ft.site = ss.site),0)
					from 
						ss (nolock)
						left join #dadosPagCentral on ss.cxstamp COLLATE SQL_Latin1_General_CP1_CI_AI = #dadosPagCentral.cxstamp COLLATE SQL_Latin1_General_CP1_CI_AI
					where 
						(dabrir = @data or fechada = 0)
						AND ss.site = @site
						AND ss.ssstamp = (case 
											when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
											then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
											else ss.ssstamp
										end)
					group by
						ss.ssstamp
						,ss.site
						,ss.pnome
						,ss.pno
						,ss.ausername
						,ss.auserno
						,(ss.ausername+' ['+CONVERT(varchar,ss.auserno)+']') 
						--,ss.dabrir
						,(convert(varchar,ss.dabrir,102)+' '+ss.habrir) 
						,ss.habrir
						,ss.fusername
						,ss.fuserno
						,(ss.fusername+' ['+CONVERT(varchar,ss.fuserno)+']')
						,ss.dfechar
						,ss.hfechar
						,(convert(varchar,ss.dfechar,102)+' '+ss.hfechar) 
						,ss.fechada
						,fdStamp	  
						,sacodinheiro 
						,ss.cxstamp
						,ss.dabrir
					order by 
						ss.habrir
				end
				print('2')
			end
	ELSE -- data diferente de hoje, mostra caixa abertas na data seleccionada
		begin
			IF @gestaoOp = 0
				begin
					select 
						cx.cxstamp
						,cx.site
						,cx.pnome
						,cx.pno
						,cx.cxno
						,cx.cxano
						,cx.ausername
						,cx.auserno
						,(cx.ausername+' ['+CONVERT(varchar,cx.auserno)+']') usa
						--,cx.dabrir
						,(convert(varchar,cx.dabrir,102)+' '+cx.habrir) as dabrir
						,cx.habrir
						,cx.fusername
						,cx.fuserno
						,(cx.fusername+' ['+CONVERT(varchar,cx.fuserno)+']') usf
						,cx.dfechar
						,cx.hfechar
						,(convert(varchar,cx.dfechar,102)+' '+cx.hfechar) as dataFecho
						,cx.fechada
						,cx.sacoDinheiro
						,cx.fdStamp
						,efundocx	= cx.fundocaixa
						,'sel' = convert(bit,0)
						,cx.comissaoTPA
						,nrAtendimento = 0
						,valorVendas = 0
						,valorReg = 0
						,valorCred = 0
						,valorCaixa = 0
						,totCompart		= ISNULL((SELECT sum((u_ettent1 * (CASE WHEN ft.tipoDoc = 3 THEN -1 ELSE 1 END)) + (u_ettent2 * (CASE WHEN ft.tipoDoc = 3 THEN -1 ELSE 1 END))) FROM fi(NOLOCK) INNER JOIN ft(nolock) on ft.ftstamp = fi.ftstamp INNER JOIN td(nolock) on td.ndoc = ft.ndoc WHERE ft.cxstamp = cx.cxstamp and ft.anulado = 0 and fi.composto = 0 and CONVERT(varchar, ft.fdata, 112) = dabrir and ft.site = cx.site),0)
					from 
						cx (nolock)
					where 
						dabrir = @data
						and cx.site = @site
					order by 
						cx.pno
						,cx.habrir
				end
			else
				begin
				
				--exec up_caixa_FechoDia '20191205', 'ATLANTICO', 1

				print 1

					insert into #dadosPagCentral
					select 
						ss.ssstamp
						,Total_Dinheiro		= sum(case when left(pc.nrAtend,2) = 'Cx' then 0 else pc.evdinheiro end) --sum(pc.evdinheiro)
						,Total_Multibanco	= sum(pc.epaga2)
						,Total_Visa			= sum(pc.epaga1)
						,Total_Cheques		= sum(pc.echtotal)
						,Total_Devolucoes	= sum(pc.devolucoes)
						,Total_Credito		= sum(pc.creditos)--(select isnull(sum(erec),0) from rl where ccstamp in (select ccstamp from cc where ftstamp in (select ftstamp from ft where u_nratend=pc.nratend)) and rl.rdata=ss.dfechar)
						,Nr_Vendas			= sum(pc.nrvendas)
						,Nr_Atend			= count(distinct pc.nratend)
						,Total_Caixa		= sum(pc.evdinheiro) + sum(pc.epaga2) + sum(pc.epaga1) + sum(pc.echtotal) + sum(pc.epaga3) + sum(pc.epaga4) + sum(pc.epaga5) + sum(pc.epaga6)
						,epaga3				= sum(pc.epaga3)
						,epaga4				= sum(pc.epaga4)
						,epaga5				= sum(pc.epaga5)
						,epaga6				= sum(pc.epaga6)
						,total_movExtra		= sum(case when left(pc.nrAtend,2) = 'Cx' then pc.evdinheiro else 0 end)
						,ss.cxstamp
						,totCompart =		0
					from 
						ss (nolock) 
						inner join B_pagCentral (nolock) pc  on pc.Ssstamp COLLATE SQL_Latin1_General_CP1_CI_AI  = ss.Ssstamp COLLATE SQL_Latin1_General_CP1_CI_AI 
					where 
						dabrir = @data
						and ss.site = @site
					group by
						ss.ssstamp
						,ss.site
						,ss.dabrir
						, pc.nrAtend
						, ss.dfechar
						, ss.cxstamp
			---------------------------------

							print 2

					select 
						 ss.ssstamp
						,ss.site
						,ss.pnome
						,ss.pno
						,ss.ausername
						,ss.auserno
						,(ss.ausername+' ['+CONVERT(varchar,ss.auserno)+']') usa
						--,ss.dabrir
						,(convert(varchar,ss.dabrir,102)+' '+ss.habrir) as dabrir
						,ss.habrir
						,ss.fusername
						,ss.fuserno
						,(ss.fusername+' ['+CONVERT(varchar,ss.fuserno)+']') usf
						,ss.dfechar
						,ss.hfechar
						,(convert(varchar,ss.dfechar,102)+' '+ss.hfechar) as dataFecho
						,ss.fechada
						,fdStamp	  
						,sacodinheiro 
						,'sel' = convert(bit,0)
						,nrAtendimento	= sum(isnull(#dadosPagCentral.Nr_Atend,0))
						,valorVendas	= sum(isnull(#dadosPagCentral.Total_Caixa,0) + isnull(#dadosPagCentral.Total_Credito,0))
						,valorReg		= sum(isnull(#dadosPagCentral.Total_Devolucoes,0))
						,valorCred		= sum(isnull(#dadosPagCentral.Total_Credito,0))
						,valorCaixa		= sum(case when isnull(#dadosPagCentral.Total_Caixa,0) <> isnull(#dadosPagcentral.total_movExtra,0) then isnull(#dadosPagCentral.Total_Caixa,0) + isnull(#dadosPagcentral.total_movExtra,0) else isnull(#dadosPagCentral.Total_Caixa,0) end)
						,totCompart		= ISNULL((SELECT sum((u_ettent1 * (CASE WHEN ft.tipoDoc = 3 THEN -1 ELSE 1 END)) + (u_ettent2 * (CASE WHEN ft.tipoDoc = 3 THEN -1 ELSE 1 END))) FROM fi(NOLOCK) INNER JOIN ft(nolock) on ft.ftstamp = fi.ftstamp INNER JOIN td(nolock) on td.ndoc = ft.ndoc WHERE ft.ssstamp = ss.ssstamp and ft.anulado = 0 and fi.composto = 0 and CONVERT(varchar, ft.fdata, 112) =dabrir and ft.site = ss.site),0)
					from 
						ss (nolock)
						left join #dadosPagCentral on ss.cxstamp COLLATE SQL_Latin1_General_CP1_CI_AI = #dadosPagCentral.cxstamp COLLATE SQL_Latin1_General_CP1_CI_AI
					where 
						dabrir = @data 
						AND ss.site = @site
						AND ss.ssstamp = (case 
											when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
											then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
											else ss.ssstamp
										end)
					group by
						ss.ssstamp
						,ss.site
						,ss.pnome
						,ss.pno
						,ss.ausername
						,ss.auserno
						,(ss.ausername+' ['+CONVERT(varchar,ss.auserno)+']') 
						--,ss.dabrir
						,(convert(varchar,ss.dabrir,102)+' '+ss.habrir) 
						,ss.habrir
						,ss.fusername
						,ss.fuserno
						,(ss.fusername+' ['+CONVERT(varchar,ss.fuserno)+']')
						,ss.dfechar
						,ss.hfechar
						,(convert(varchar,ss.dfechar,102)+' '+ss.hfechar) 
						,ss.fechada
						,fdStamp	  
						,sacodinheiro 
						,ss.cxstamp
						,ss.dabrir
					order by 
						ss.habrir

							print 3
				end
			end	
		--	exec up_caixa_FechoDia '20180419', 'Loja 1', 1
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosPagCentral'))
			DROP TABLE #dadosPagCentral

GO
Grant Execute on dbo.up_caixa_FechoDia to Public
Grant Control on dbo.up_caixa_FechoDia to Public
GO

