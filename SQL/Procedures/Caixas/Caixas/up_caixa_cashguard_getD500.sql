/*
	Checks to see if user is allowed to dispense 500 cash
	exec up_caixa_cashguard_getD500 '123'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_caixa_cashguard_getD500]') IS NOT NULL
	drop procedure dbo.up_caixa_cashguard_getD500
go

create procedure dbo.up_caixa_cashguard_getD500
	@pass varchar(20)


/* WITH ENCRYPTION */
AS

	SELECT 
		d500 
	FROM
		b_us (nolock) 
	WHERE 
		userpass = @pass

GO
Grant Execute On dbo.up_caixa_cashguard_getD500 to Public
Grant Control On dbo.up_caixa_cashguard_getD500 to Public
GO