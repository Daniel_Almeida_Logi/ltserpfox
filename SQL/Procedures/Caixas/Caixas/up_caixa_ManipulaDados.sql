-- Manipula��o de Dados por Operador --
-- Nota: Sempre que se Adicionarem meios de pagamento � necess�rio alterar este query
-- exec up_caixa_ManipulaDados '20110801', 'Loja 2', '1'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_caixa_ManipulaDados]') IS NOT NULL
	drop procedure dbo.up_caixa_ManipulaDados
go

create procedure dbo.up_caixa_ManipulaDados

@data DATETIME,
@site varchar(60),
@op numeric(6,0)

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

declare  @loja varchar(60)
		,@dinheiro char(8)
		,@cheque char(8)
		,@efundocx numeric(13,2)
		,@cxstamp varchar(28)
		
;with
	cteCX (data,loja,pnome,pno,fusername,fuserno,sacoDinheiro,evdinheiro,echtotal,epaga2,epaga1,devolucoes,efundocx,saldo,cxstamp,cxid) as
	(
		select 
			 'data'			=	dabrir
			,'loja'			=	cx.site 
			,'pnome'		=	cx.pnome
			,'pno'			=	cx.pno
			,'fusername'	=	fusername
			,'fuserno'		=	fuserno
			,'sacoDinheiro'	=	cx.sacoDinheiro
			,'evdinheiro'	=	fcx.evdinheiro
			,'echtotal'		=	fcx.echtotal
			,'epaga2'		=	fcx.epaga2
			,'epaga1'		=	fcx.epaga1
			,'devolucoes'	=	fcx.devolucoes
			,'efundocx'		=	fcx.efundocx
			,'saldo'		=	cx.saldod
			,'cxstamp'		=	isnull(cx.cxstamp,'')
			,'cxid'			=	cx.cxid
		from cx (nolock)
			inner join fcx (nolock) on fcx.cxstamp=cx.cxstamp
		where fcx.operacao='F' and cx.fechada=1 and cx.dabrir=@data
		    and cx.site=@site and cx.fuserno=@op
		   /*  and fcx.efundocx!=0 */
	),
	
	cteModoPag (ref,design,permitetroca) as
	(
		select
			 isnull(ref,'0')
			,design
			,permitetroca
		from B_modoPag (nolock)
	)

/* guardar c�digo da loja e c�digos dos meios de pagamento  */
select
	 @loja		=	isnull((select ref from empresa (nolock) where site=@site),'')
	,@dinheiro	=	(select ref from cteModoPag where design like 'Dinheiro')
	,@cheque	=	(select ref from cteModoPag where design like 'Cheques')
	/*  guardar fundo e stamp da primeira caixa */
	,@efundocx	=	isnull((select top 1 efundocx from fcx (nolock)
							inner join cx (nolock) on fcx.cxstamp=cx.cxstamp
							where fcx.operacao='F' and cx.fechada=1 and cx.dabrir=@data
								and cx.site=@site and cx.fuserno=@op and fcx.efundocx!=0
							order by cxid)
							,0)
	,@cxstamp	=	(select top 1 cxstamp from cteCX where efundocx!=0 order by cxid)

select
	meioPag, DATA, loja, fusername, fuserno,
	total_venda			= sum(total_venda),
	total_devolucao	= sum(total_devolucao),
	efundocx			= case when meiopag='Dinheiro' then sum(efundocx) else 0 end,
	valorSaco			= sum(valorsaco),
	saldo					= sum(saldo),
	quebra				= case when meioPag='Dinheiro' then sum(valorSaco + saldo) - sum(total_venda + efundocx)
									when meioPag='Cheques'  then sum(valorSaco + saldo) - sum(total_venda)
									else 0
							end,
	tipomodopag		=  isnull((select permiteTroca from cteModoPag where design=xxx.meioPag),0),
	cxstamp				= @cxstamp
from (
	select
		meioPag, DATA, loja, pnome, pno, fusername, fuserno,
		sum(xx.total_venda) as total_venda,
		valorSaco,
		sum(xx.devolucoes) as total_devolucao,
		sum(xx.saldo) as saldo,
		efundocx	= case when sacoDinheiro='0' 
								then 0 
								else @efundocx
					  end
	from (
		select
			meioPag, DATA, loja, pnome, pno, fusername, fuserno,
			sacoDinheiro,
			sum(Venda) as total_venda,
			valorSaco = case when meioPag='Dinheiro'
								then isnull(( select sum(valor_meiopagamento)
											  from B_moneyFile_registos mfr (nolock)
												inner join cx cx1 (nolock) on cx1.sacodinheiro = mfr.numero_safebag
											  where data_de_venda=@data
												AND sacoDinheiro!='0'
												AND substring(id_esegur_dep, patindex('%[^0]%',id_esegur_dep), 16)=@loja
												AND substring(codigo_meiopagamento,4,2)=@dinheiro
												AND cx1.sacoDinheiro=x.sacoDinheiro
												AND cx1.cxstamp=x.cxstamp)
										,0)
							when meioPag='Cheques'
								then isnull(( select sum(valor_meiopagamento)
											  from b_moneyFile_registos mfr (nolock)
												inner join cx cx1 (nolock) on cx1.sacodinheiro = mfr.numero_safebag
											  where data_de_venda=@data 
												AND sacoDinheiro!='0'
												AND substring(id_esegur_dep, patindex('%[^0]%',id_esegur_dep), 16)=@loja
												AND substring(codigo_meiopagamento,4,2)=@cheque 
												AND cx1.sacoDinheiro=x.sacoDinheiro 
												AND cx1.cxstamp=x.cxstamp)
										,0)
								else 0 
						end,
			devolucoes, saldo
		from (
					
				select 'Dinheiro' as meioPag, cteCX.data, cteCx.loja, cteCx.pnome, cteCx.pno, cteCx.fusername, cteCx.fuserno, cteCx.sacoDinheiro,
					cteCx.evdinheiro as Venda, cteCx.devolucoes, cteCx.efundocx, cteCx.saldo, cteCx.cxstamp
				from cteCx
				
				union all
							
				select 'Cheques' as meioPag, cteCX.data, cteCx.loja, cteCx.pnome, cteCx.pno, cteCx.fusername, cteCx.fuserno, cteCx.sacoDinheiro,
					cteCX.echtotal as Venda, 0 as devolucoes, cteCX.efundocx, cteCX.saldo, cteCX.cxstamp
				from cteCX
				
				union all
				
				select 'MultiBanco' as meioPag, cteCX.data, cteCx.loja, cteCx.pnome, cteCx.pno, cteCx.fusername, cteCx.fuserno, cteCx.sacoDinheiro,
					cteCX.epaga2 as Venda, 0 as devolucoes, cteCx.efundocx, cteCx.saldo, cteCx.cxstamp
				from cteCX
				
				union all
				
				select 'Visa' as meioPag, cteCX.data, cteCx.loja, cteCx.pnome, cteCx.pno, cteCx.fusername, cteCx.fuserno, cteCx.sacoDinheiro,
					cteCX.epaga1 as Venda, 0 as devolucoes, cteCX.efundocx, cteCX.saldo, cteCX.cxstamp
				from cteCX
				
			) as x
			group by meioPag, data, loja, pnome, pno, fusername, fuserno, sacoDinheiro ,x.saldo,x.devolucoes ,x.cxstamp /*x.valorSaco,*/
	) as xx
	group by meioPag, DATA, loja, pnome, pno, fusername, fuserno, sacoDinheiro, valorSaco
) as xxx
group by meioPag, data, loja, fusername, fuserno

GO
Grant Execute on dbo.up_caixa_ManipulaDados to Public
Grant Control on dbo.up_caixa_ManipulaDados to Public
GO