-- Ver Stamp do Dia --
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_caixa_stampDia]') IS NOT NULL
	drop procedure dbo.up_caixa_stampDia
go

create procedure dbo.up_caixa_stampDia
@site varchar (60)

/* WITH ENCRYPTION */
AS


select 
	top 1 fdStamp 
from 
	B_fechoDia (nolock)
where 
	fechado=0 
	and site = @site
order by 
	dataAbriu desc


GO
Grant Execute on dbo.up_caixa_stampDia to Public
Grant Control on dbo.up_caixa_stampDia to Public
GO