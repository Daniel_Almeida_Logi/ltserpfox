/* Dados para os tal�es de fecho de caixa - sess�o de caixa por operador e sess�o de caixa por terminal

	 exec up_caixa_dadosParaTaloesFecho 'ADMFB805E16-5822-4579-82D', 'Loja 1'
	 
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_caixa_dadosParaTaloesFecho]') IS NOT NULL
	drop procedure dbo.up_caixa_dadosParaTaloesFecho
go

create procedure dbo.up_caixa_dadosParaTaloesFecho

	@cxstamp varchar(28)
	,@site varchar(30)

/* WITH ENCRYPTION */

AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosPagCentral'))
		DROP TABLE #dadosPagCentral
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFT'))
		DROP TABLE #dadosFT
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosPagCentralSs'))
		DROP TABLE #dadosPagCentralSs
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFTSs'))
		DROP TABLE #dadosFTSs
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRE'))
		DROP TABLE #dadosRE
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRESs'))
		DROP TABLE #dadosRESs


	/* Verificar Modos de Pagamento configurados na BD do Cliente */
	declare @modopag1 as varchar(60), @modopag2 as varchar(60), @modopag3 as varchar(60), @modopag4 as varchar(60), @modopag5 as varchar(60), @modopag6 as varchar(60), @modopag7 as varchar(60), @modopag8 as varchar(60), @gestaoCxOperador bit

	select @modopag1 = design from B_modoPag where ref = '01'
	select @modopag2 = design from B_modoPag where ref = '02'
	select @modopag3 = design from B_modoPag where ref = '03'
	select @modopag4 = design from B_modoPag where ref = '04'
	select @modopag5 = design from B_modoPag where ref = '05'
	select @modopag6 = design from B_modoPag where ref = '06'
	select @modopag7 = design from B_modoPag where ref = '07'
	select @modopag8 = design from B_modoPag where ref = '08'
	
	set @gestaoCxOperador = (select gestao_cx_operador from empresa where site = @site)

	IF @gestaoCxOperador = 0
		begin
			/* Calculo de Valores Registados na B_PagCentral */
			select 
				-- Valores Contabilizados pelo Sistema
				cxstamp
				,'sistemaDinheiro'	= Sum(evdinheiro)
				,'sistemaMB'		= Sum(epaga2)
				,'sistemaVisa'		= Sum(epaga1)
				,'sistemaCheques'	= Sum(echtotal)
				,'sistemaEpaga3'	= Sum(epaga3)
				,'sistemaEpaga4'	= Sum(epaga4)
				,'sistemaEpaga5'	= Sum(epaga5)
				,'sistemaEpaga6'	= Sum(epaga6)
				,'nrVendas'			= Sum(nrvendas)
				,'nrAtendimentos'	= Count(distinct nratend) 
				,'devolucoes'		= Sum(devolucoes)
				,'totalCaixa'		= Sum(total)
				,'totalCred'		= Sum(creditos)
			into 
				#dadosPagCentral
			from 
				b_pagcentral (nolock)
			where
				b_pagcentral.cxstamp = @cxstamp
			group by
				b_pagcentral.cxstamp

			/*select 
				cxstamp
				,'sistemaDinheiro'	= Sum(evdinheiro)
				,'sistemaMB'		= Sum(epaga2)
				,'sistemaVisa'		= Sum(epaga1)
				,'sistemaCheques'	= Sum(echtotal)
				,'sistemaEpaga3'	= 0
				,'sistemaEpaga4'	= 0
				,'sistemaEpaga5'	= 0
				,'sistemaEpaga6'	= 0
				,'nrVendas'			= 0
				,'nrAtendimentos'	= 0
				,'devolucoes'		= 0
				,'totalCaixa'		= sum(etotal)
				,'totalCred'		= 0
			INTO 
				#dadosRE
			from 
				re(nolock)
			WHERE
				nmdoc = 'Normal'
				and cxstamp = @cxstamp
			GROUP BY
				cxstamp*/
			
			/* Calculo de Valores Registados na FT */
			select 
				-- Valores Contabilizados pelo Sistema
				cxstamp
				,'totalDesconto'	= Sum(ft.edescc)
				,'totalVendas'		= Sum(ft.etotal)
				,'totCompart'		= (Select SUM(u_ettent1 + u_ettent2) from fi(nolock) where fi.ftstamp in (select fft.ftstamp from ft(nolock) as fft where fft.cxstamp = @cxstamp))
			into 
				#dadosFT
			from 
				ft  (nolock)
			where
				ft.cxstamp = @cxstamp
			group by 
				ft.cxstamp

			/* Result Set Final */
			select
				'auserno'			=	cx.auserno
				,'ausername'		=	cx.ausername
				,'fuserno'			=	cx.fuserno
				,'fusername'		=	cx.fusername
				,'pnome'			=	cx.pnome
				,'pno'				=	cx.pno
				,'supervisor_no'	=	cm3.cm1
				,'supervisor_nome'  =	Case when cm3.cm = cm3.cm1 then isnull(cmdesc,'') else '' end	
				,'sacoDinheiro'		=	cx.sacoDinheiro
				,'efundocx'			=	cx.fundocaixa
				,'dabrir'			=	cx.dabrir
				,'habrir'			=	cx.habrir
				,'dfechar'			=	cx.dfechar
				,'hfechar'			=	cx.hfechar
				,'obs'				=	cx.causa
				-- Tipos de Pagamento Dispon�veis
				,modopag1			= @modopag1
				,modopag2			= @modopag2
				,modopag3			= @modopag3
				,modopag4			= @modopag4
				,modopag5			= @modopag5
				,modopag6			= @modopag6
				,modopag7			= @modopag7
				,modopag8			= @modopag8
				-- Valores Contabilizados pelo Operador
				,'dinheiro'			= Sum(fecho_dinheiro)
				,'multibanco'		= Sum(fecho_mb)
		 		,'visa'				= Sum(fecho_visa)
				,'cheques'			= Sum(fecho_cheques)
				,'epaga3'			= Sum(fecho_epaga3)
				,'epaga4'			= Sum(fecho_epaga4)
				,'epaga5'			= Sum(fecho_epaga5)
				,'epaga6'			= Sum(fecho_epaga6)
				,'comissaoTpa'		= Sum(comissaoTPA)
				,'totalcontagem'    = Sum(fecho_dinheiro) +  Sum(fecho_mb) + Sum(fecho_visa) + Sum(fecho_cheques) +  Sum(fecho_epaga3) +  Sum(fecho_epaga4) +  Sum(fecho_epaga5)+  Sum(fecho_epaga6)
				-- Resumo Opera�es
				,'nr_clientes'		= ISNULL((SELECT 
											COUNT( DISTINCT cl) 
										FROM(
											SELECT distinct convert(varchar,no)+convert(varchar,estab) as cl FROM ft(nolock) inner join cx (nolock) on cx.cxstamp = ft.cxstamp where cx.cxstamp = @cxstamp and ft.no <> 200
											UNION ALL
											SELECT distinct convert(varchar,no)+convert(varchar,estab) as cl FROM re(nolock) where re.cxstamp = @cxstamp and re.nmdoc = 'Normal' and re.no <> 200
										) as x),0)
									  + ISNULL((SELECT COUNT(*) FROM ft(nolock) inner join cx (nolock) on cx.cxstamp = ft.cxstamp where cx.cxstamp = @cxstamp and ft.no = 200),0)
									  + ISNULL((SELECT COUNT(*) FROM re(nolock) where re.cxstamp = @cxstamp and re.nmdoc = 'Normal' and re.no = 200),0)
				-- Dados Registados pelo Software e Resumo Opera��es
				,isnull(#dadosPagCentral.sistemaDinheiro,0) as SistemaDinheiro
				,isnull(#dadosPagCentral.sistemaMB,0) as SistemaMB
				,isnull(#dadosPagCentral.sistemaVisa,0) as SistemaVisa
				,isnull(#dadosPagCentral.sistemaCheques,0) as SistemaCheques
				,isnull(#dadosPagCentral.sistemaEpaga3, 0) as SistemaEpaga3
				,isnull(#dadosPagCentral.sistemaEpaga4, 0) as SistemaEpaga4
				,isnull(#dadosPagCentral.sistemaEpaga5, 0) as SistemaEpaga5
				,isnull(#dadosPagCentral.sistemaEpaga6, 0) as SistemaEpaga6
				,isnull(#dadosPagCentral.nrVendas, 0) as nrVendas
				,isnull(#dadosPagCentral.nrAtendimentos, 0) as nrAtendimentos
				,isnull(#dadosPagCentral.devolucoes, 0) as devolucoes
				,isnull(#dadosPagCentral.totalCaixa, 0) as totalCaixa
				,totalDesconto	= isnull(#dadosFT.totalDesconto,0)
				,totalVendas	= isnull(#dadosFT.totalVendas,0)
				,'vmv'			= isnull((#dadosFT.totalVendas /#dadosPagCentral.nrVendas),0)
				,totCompart		= isnull(#dadosFT.totCompart,0)
				,totalCred		= isnull(#dadosPagCentral.totalCred,0)
				,cambUsKz = ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'USD', cx.dfechar),1)
				,cambEuKz = ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'EUR', cx.dfechar),1)
 			from 
				cx (nolock)
				left join cm3 (nolock) on cm3.cm = cx.fuserno
				left join  #dadosPagCentral on cx.cxstamp = #dadosPagCentral.cxstamp
				left join #dadosFT on cx.cxstamp = #dadosFT.cxstamp
			where 
				cx.cxstamp = @cxstamp 
				and cx.fechada = 1
			group by 
				cx.fuserno, cx.fusername, cx.auserno, cx.ausername, cx.pnome, cx.pno, cm3.cm1, 
				cx.sacoDinheiro, cx.fundocaixa, cx.dabrir, cx.habrir, cm3.cm, cm3.cmdesc, cx.dfechar, cx.hfechar, cx.causa
				,#dadosPagCentral.sistemaDinheiro, #dadosPagCentral.sistemaMB, #dadosPagCentral.sistemaVisa, #dadosPagCentral.sistemaCheques ,#dadosPagCentral.sistemaEpaga3, #dadosPagCentral.sistemaEpaga4, #dadosPagCentral.sistemaEpaga5
				,#dadosPagCentral.sistemaEpaga6, #dadosPagCentral.nrVendas, #dadosPagCentral.nrAtendimentos , #dadosPagCentral.devolucoes, #dadosPagCentral.totalCaixa
				,#dadosFT.totalDesconto, #dadosFT.totalVendas, #dadosFT.totCompart, #dadosPagCentral.totalCred
				,cx.usrdata, cx.usrhora
			order by 
				cx.usrdata desc ,cx.usrhora desc
		end
	else
		begin
			/* Calculo de Valores Registados na B_PagCentral */
			select 
				-- Valores Contabilizados pelo Sistema
				ssstamp
				,'sistemaDinheiro'	= Sum(evdinheiro)
				,'sistemaMB'		= Sum(epaga2)
				,'sistemaVisa'		= Sum(epaga1)
				,'sistemaCheques'	= Sum(echtotal)
				,'sistemaEpaga3'	= Sum(epaga3)
				,'sistemaEpaga4'	= Sum(epaga4)
				,'sistemaEpaga5'	= Sum(epaga5)
				,'sistemaEpaga6'	= Sum(epaga6)
				,'nrVendas'			= Sum(nrvendas)
				,'nrAtendimentos'	= Count(distinct nratend) 
				,'devolucoes'		= Sum(devolucoes)
				,'totalCaixa'		= Sum(total)
				,'totalCred'		= Sum(creditos)
			into 
				#dadosPagCentralSs
			from 
				b_pagcentral (nolock)
			where
				b_pagcentral.ssstamp = @cxstamp
			group by
				b_pagcentral.ssstamp

			/*select 
				ssstamp
				,'sistemaDinheiro'	= Sum(evdinheiro)
				,'sistemaMB'		= Sum(epaga2)
				,'sistemaVisa'		= Sum(epaga1)
				,'sistemaCheques'	= Sum(echtotal)
				,'sistemaEpaga3'	= 0
				,'sistemaEpaga4'	= 0
				,'sistemaEpaga5'	= 0
				,'sistemaEpaga6'	= 0
				,'nrVendas'			= 0
				,'nrAtendimentos'	= 0
				,'devolucoes'		= 0
				,'totalCaixa'		= sum(etotal)
				,'totalCred'		= 0
			INTO 
				#dadosRESs
			from 
				re(nolock)
			WHERE
				nmdoc = 'Normal'
				and ssstamp = @cxstamp
			GROUP BY
				ssstamp*/
			
			/* Calculo de Valores Registados na FT */
			select 
				-- Valores Contabilizados pelo Sistema
				ssstamp
				,'totalDesconto'	= Sum(ft.edescc)
				,'totalVendas'		= Sum(ft.etotal)
				,'totCompart'		= (Select SUM(u_ettent1 + u_ettent2) from fi(nolock) where fi.ftstamp in (select fft.ftstamp from ft(nolock) as fft where fft.ssstamp = @cxstamp))
			into 
				#dadosFTSs
			from 
				ft (nolock)
			where
				ft.ssstamp = @cxstamp
			group by 
				ft.ssstamp

			/* Result Set Final */
			select
				'auserno'			=	ss.auserno
				,'ausername'		=	ss.ausername
				,'fuserno'			=	ss.fuserno
				,'fusername'		=	ss.fusername
				,'pnome'			=	ss.pnome
				,'pno'				=	ss.pno
				,'supervisor_no'	=	cm3.cm1
				,'supervisor_nome'  =	Case when cm3.cm = cm3.cm1 then isnull(cmdesc,'') else '' end	
				,'sacoDinheiro'		=	ss.sacoDinheiro
				,'efundocx'			=	ss.fundocaixa
				,'dabrir'			=	ss.dabrir
				,'habrir'			=	ss.habrir
				,'dfechar'			=	ss.dfechar
				,'hfechar'			=	ss.hfechar
				,'obs'				=	ss.causa
				-- Tipos de Pagamento Dispon�veis
				,modopag1			= @modopag1
				,modopag2			= @modopag2
				,modopag3			= @modopag3
				,modopag4			= @modopag4
				,modopag5			= @modopag5
				,modopag6			= @modopag6
				,modopag7			= @modopag7
				,modopag8			= @modopag8
				-- Valores Contabilizados pelo Operador
				,'dinheiro'			= Sum(evdinheiro)
				,'multibanco'		= Sum(epaga1)
				,'visa'				= Sum(epaga2)
				,'cheques'			= Sum(echtotal)
				,'epaga3'			= Sum(fecho_epaga3)
				,'epaga4'			= Sum(fecho_epaga4)
				,'epaga5'			= Sum(fecho_epaga5)
				,'epaga6'			= Sum(fecho_epaga6)
				,'comissaoTpa'		= Sum(comissaoTPA)
				,'totalcontagem'    = Sum(evdinheiro) +  Sum(epaga1) + Sum(epaga2) + Sum(echtotal) +  Sum(fecho_epaga3) +  Sum(fecho_epaga4) +  Sum(fecho_epaga5)+  Sum(fecho_epaga6)
				-- Resumo Opera�es
				,'nr_clientes'		= ISNULL((SELECT 
											COUNT( DISTINCT cl) 
										FROM(
											SELECT distinct convert(varchar,no)+convert(varchar,estab) as cl FROM ft(nolock) inner join ss (nolock) on ss.ssstamp = ft. ssstamp where ss.ssstamp = @cxstamp and ft.no <> 200
											UNION ALL
											SELECT distinct convert(varchar,no)+convert(varchar,estab) as cl FROM re(nolock) where ssstamp = @cxstamp and re.nmdoc = 'Normal' and re.no <> 200
										) as x), 0)
										+ ISNULL((select COUNT(*) FROM ft(nolock) inner join ss (nolock) on ss.ssstamp = ft. ssstamp where ss.ssstamp = @cxstamp and ft.no = 200),0)
										+ ISNULL((select COUNT(*) FROM re(nolock) where ssstamp = @cxstamp and re.nmdoc = 'Normal' and re.no = 200),0)

				-- Dados Registados pelo Software e Resumo Opera��es
				,isnull(#dadosPagCentralSs.sistemaDinheiro,0) as SistemaDinheiro
				,isnull(#dadosPagCentralSs.sistemaMB,0)	as SistemaMB
				,isnull(#dadosPagCentralSs.sistemaVisa,0)-- + isnull(#dadosRESs.sistemaVisa,0) 
				as SistemaVisa
				,isnull(#dadosPagCentralSs.sistemaCheques,0) as SistemaCheques
				,isnull(#dadosPagCentralSs.sistemaEpaga3, 0) as SistemaEpaga3
				,isnull(#dadosPagCentralSs.sistemaEpaga4, 0) as SistemaEpaga4
				,isnull(#dadosPagCentralSs.sistemaEpaga5, 0) as SistemaEpaga5
				,isnull(#dadosPagCentralSs.sistemaEpaga6, 0) as SistemaEpaga6
				,isnull(#dadosPagCentralSs.nrVendas, 0) as nrVendas
				,isnull(#dadosPagCentralSs.nrAtendimentos, 0) as nrAtendimentos
				,isnull(#dadosPagCentralSs.devolucoes, 0) as devolucoes
				,isnull(#dadosPagCentralSs.totalCaixa, 0) as totalCaixa
				,totalDesconto	= isnull(#dadosFTSs.totalDesconto,0)
				,totalVendas	= isnull(#dadosFTSs.totalVendas,0)
				,'vmv'			= isnull((#dadosFTSs.totalVendas /#dadosPagCentralSs.nrVendas),0)
				,totCompart		= isnull(#dadosFTSs.totCompart,0)
				,totalCred		= isnull(#dadosPagCentralSs.totalCred,0)
				,cambUsKz = ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'USD', ss.dfechar),1)
				,cambEuKz = ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'EUR', ss.dfechar),1)
 			from 
				ss (nolock)
				left join cm3 (nolock) on cm3.cm = ss.fuserno
				left join #dadosPagCentralSs on ss.ssstamp = #dadosPagCentralSs.ssstamp
				left join #dadosFTSs on ss.ssstamp = #dadosFTSs.ssstamp
			where 
				ss.ssstamp = @cxstamp 
				and ss.fechada = 1
			group by 
				ss.fuserno, ss.fusername, ss.auserno, ss.ausername, ss.pnome, ss.pno, cm3.cm1, 
				ss.sacoDinheiro, ss.fundocaixa, ss.dabrir, ss.habrir, cm3.cm, cm3.cmdesc, ss.dfechar, ss.hfechar, ss.causa
				,#dadosPagCentralSs.sistemaDinheiro, #dadosPagCentralSs.sistemaMB, #dadosPagCentralSs.sistemaVisa, #dadosPagCentralSs.sistemaCheques ,#dadosPagCentralSs.sistemaEpaga3, #dadosPagCentralSs.sistemaEpaga4, #dadosPagCentralSs.sistemaEpaga5
				,#dadosPagCentralSs.sistemaEpaga6, #dadosPagCentralSs.nrVendas, #dadosPagCentralSs.nrAtendimentos , #dadosPagCentralSs.devolucoes, #dadosPagCentralSs.totalCaixa
				,#dadosFTSs.totalDesconto, #dadosFTSs.totalVendas, #dadosFTSs.totCompart, #dadosPagCentralSs.totalCred
				,ss.usrdata, ss.usrhora
			order by 
				ss.usrdata desc ,ss.usrhora desc
		end


	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosPagCentral'))
		DROP TABLE #dadosPagCentral
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFT'))
		DROP TABLE #dadosFT
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosPagCentral'))
		DROP TABLE #dadosPagCentralSs
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFT'))
		DROP TABLE #dadosFTSs
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosReSs'))
		DROP TABLE #dadosReSs
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRe'))
		DROP TABLE #dadosRe

GO
Grant Execute on dbo.up_caixa_dadosParaTaloesFecho to Public
Grant control on dbo.up_caixa_dadosParaTaloesFecho to Public
GO