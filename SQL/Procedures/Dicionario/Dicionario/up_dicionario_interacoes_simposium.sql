-- Author: Daniel Almeida
-- Date: 2024-09-19

-- Pesquisa intera��es de medicamentos via Api do Simposium
--
--use ltdev30
--exec up_dicionario_interacoes_simposium  'ADM608B28E1-1441-415C-812',1,0
--


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_dicionario_interacoes_simposium]') IS NOT NULL
	drop procedure up_dicionario_interacoes_simposium
go

create PROCEDURE up_dicionario_interacoes_simposium
	 @token	varchar (36),
	 @siteNr int, 
	 @nivel int

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	SELECT 
		st1.ref AS ref,                 -- Primeiro produto
		st1.design AS design1,           -- Designa��o do primeiro produto
		st2.ref AS cnp2,                 -- Segundo produto
		st2.design AS Design,            -- Designa��o do segundo produto
		0 AS med_class_b,                -- Valor fixo, conforme indicado
		simposiumRespInteractions.level AS grau,  -- Grau da intera��o
		simposiumRespInteractions.infoDescr AS explicacao,  -- Explica��o da intera��o
		simposiumRespInteractions.advice AS conselho,       -- Conselho m�dico
		convert(varchar(254),isnull(simposiumRespInteractions.severityDescr,'')) as descr

	FROM 
		simposiumResp (NOLOCK)
		INNER JOIN simposiumRespInteractions (NOLOCK) 
			ON simposiumRespInteractions.token = simposiumResp.tokenInteraction
		INNER JOIN simposiumRespInteractionsItems AS simposiumRespInteractionsItems1 (NOLOCK) 
			ON simposiumRespInteractionsItems1.token = simposiumRespInteractions.tokenItems
		INNER JOIN simposiumRespInteractionsItems AS simposiumRespInteractionsItems2 (NOLOCK) 
			ON simposiumRespInteractionsItems2.token = simposiumRespInteractions.tokenItems 
		   AND simposiumRespInteractionsItems1.id < simposiumRespInteractionsItems2.id  -- Garante que o par � �nico (ordena��o)
		INNER JOIN st AS st1 (NOLOCK) 
			ON st1.ref = RTRIM(LTRIM(CONVERT(VARCHAR(18), simposiumRespInteractionsItems1.id))) AND st1.site_nr = @siteNr
		INNER JOIN st AS st2 (NOLOCK) 
			ON st2.ref = RTRIM(LTRIM(CONVERT(VARCHAR(18), simposiumRespInteractionsItems2.id))) AND st2.site_nr = @siteNr
	WHERE
		simposiumResp.token = @token
		and simposiumRespInteractions.level>=@nivel
	ORDER BY 
		grau desc 


	
	
go
grant execute on up_dicionario_interacoes_simposium to public 			
grant execute on up_dicionario_interacoes_simposium to public 
go