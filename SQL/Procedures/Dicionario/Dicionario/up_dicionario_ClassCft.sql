-- exec	up_dicionario_ClassCft '8168617'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_dicionario_ClassCft]') IS NOT NULL
	drop procedure dbo.up_dicionario_ClassCft
go

create PROCEDURE [dbo].[up_dicionario_ClassCft]

@cnp	varchar(25)

/* with encryption */
AS
SET NOCOUNT ON

Select
	a.cftcode,
	descricao,
	mecanismo_accao,
	espectro_accao,
	advertencias_precaucoes,
	efeitos_secundarios,
	conducao
from b_cft as a
inner join b_cftfp as b on a.cftcode = b.cftcode
where cnp=@cnp

Go
Grant Execute on dbo.up_dicionario_ClassCft to Public
Grant Control on dbo.up_dicionario_ClassCft to Public
Go
