-- SP:	up_dicionario_substancias
-- exec	up_dicionario_substancias '8168617'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_dicionario_substancias]') IS NOT NULL
	drop procedure dbo.up_dicionario_substancias
go

create PROCEDURE [dbo].[up_dicionario_substancias]

@cnp	varchar(25)

/* with encryption */
AS
SET NOCOUNT ON

Select 
	class1,
	case when tipo=1 then 'DCI' 
		 when tipo=2 or tipo=4 then 'Excipiente'
		 when tipo=3 then 'Revestimento' else '' end as denominacao,
	case when tipo=1 then '1' 
	     when tipo=2 or tipo=4 then '2'
		 when tipo=3 then '3' else '4' end as ordem,
	descricao,
	qtt
from
	B_moleculasfp (nolock)
inner join
	B_moleculas (nolock) on B_moleculasfp.moleculaid=B_moleculas.moleculaid
where 
	cnp=@cnp
order by
	ordem

Go
Grant Execute on dbo.up_dicionario_substancias to Public
Grant Control on dbo.up_dicionario_substancias to Public
Go