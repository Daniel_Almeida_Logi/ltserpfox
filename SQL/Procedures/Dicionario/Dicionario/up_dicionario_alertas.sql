-- exec	up_dicionario_alertas '8168617'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_dicionario_alertas]') IS NOT NULL
	drop procedure dbo.up_dicionario_alertas
go

create PROCEDURE [dbo].up_dicionario_alertas

@cnp	varchar(25)

/* with encryption */
AS
SET NOCOUNT ON

	select 
		distinct
		fprod.cnp
		,via_admin	= isnull(va.descr,'')
		,tipo = isnull(ma.tipo,'')
		,descr = isnull(ma.descr,'')
	from 
		fprod (nolock)
		inner join med_alertas ma (nolock) on fprod.medid = ma.id_med
		left join vias_admin va (nolock) on va.id = ma.id_vias_admin
	where 
		fprod.cnp=@cnp
	order by
		tipo

Go
Grant Execute on dbo.up_dicionario_alertas to Public
Grant Control on dbo.up_dicionario_alertas to Public
Go