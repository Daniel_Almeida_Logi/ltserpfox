-- SP:	exec up_dicionario '5440987','Loja 1'
-- exec	up_dicionario '000248','ATLANTICO'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_dicionario]') IS NOT NULL
	drop procedure dbo.up_dicionario
go

create PROCEDURE [dbo].[up_dicionario]

@ref	varchar(25),
@site   varchar(15)=''

/* with encryption */
AS
SET NOCOUNT ON

DECLARE @PAIS VARCHAR(55)
Declare @codeCNP VARCHAR(18)
DECLARE @control BIT
SET @control =0

if(@site='')
	select TOP 1  @PAIS = textValue from B_Parameters_site(nolock) where  stamp = 'ADM0000000050' 
else 
	select @PAIS = textValue from B_Parameters_site(nolock)  where  stamp = 'ADM0000000050' and site = @site
 


set @codeCNP=@ref
if (@PAIS !='Portugal')
BEGIN
	SELECT @codeCNP = (case when codCNP='' then ref else codCNP end) FROM st where ref=@ref
END

	select
		fprod.*
		--,departamento = isnull(b_famDepartamentos.design ,'')
		--,seccao = isnull(b_famSeccoes.design,'')
		--,categoria = isnull(b_famCategorias.design,'')

		--carregar classificacao do hmr - 20200910 JG		
		, ISNULL(A.descr,'') as departamento
		, ISNULL(B.descr,'') as seccao
		, ISNULL(C.descr,'') as categoria
		, ISNULL(D.descr,'') as segmento
		,famfamilia =  isnull(b_famFamilias.design,'')
		,posologia_orientativa = isnull(posologia.posologia_orientativa,'')
	from
		fprod (nolock)
		--left join b_famDepartamentos (nolock) on fprod.depstamp = b_famDepartamentos.depstamp
		--left join b_famSeccoes (nolock) on fprod.secstamp = b_famSeccoes.secstamp
		--left join b_famCategorias (nolock) on fprod.catstamp = b_famCategorias.catstamp
		--carregar classificacao do hmr - 20200910 JG		
		left join grande_mercado_hmr (nolock) A on A.id = fprod.id_grande_mercado_hmr
		left join mercado_hmr (nolock) B on B.id = fprod.id_mercado_hmr
		left join categoria_hmr (nolock) C on C.id = fprod.id_categoria_hmr
		left join segmento_hmr (nolock) D on D.id = fprod.id_segmento_hmr

		left join b_famFamilias (nolock) on fprod.famstamp = b_famFamilias.famstamp
		left join posologia (nolock) on (posologia.ref = fprod.ref and posologia.ref!='')
	where
		cnp=@codeCNP


Go
Grant Execute on dbo.up_dicionario to Public
Grant Control on dbo.up_dicionario to Public
Go