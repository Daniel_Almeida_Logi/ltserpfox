-- exec	up_dicionario_classATC '8168617'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_dicionario_classATC]') IS NOT NULL
	drop procedure dbo.up_dicionario_classATC
go

create PROCEDURE [dbo].[up_dicionario_classATC]

@cnp	varchar(25)

/* with encryption */
AS
SET NOCOUNT ON


Select
	a.atccode,
	descricao,
	atcmini,
	ac_farmac,
	indica,
	efectind
from b_atc as a (nolock)
	inner join b_atcfp as b (nolock) on a.atccode = b.atccode
where cnp=@cnp

Go
Grant Execute on dbo.up_dicionario_classATC to Public
Grant Control on dbo.up_dicionario_classATC to Public
Go