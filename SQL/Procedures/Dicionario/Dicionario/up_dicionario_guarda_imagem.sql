-- Daniel Almeida
-- criado 2020-09-11
-- Valida se existe imagem no dicionario e cria reigsto na anexo
-- SP:	exec up_dicionario_guarda_imagem '2225556','Loja 1'
-- exec	up_dicionario_guarda_imagem '7468132','Loja 1'
--select * from anexos where filename like '%7468132%'







SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_dicionario_guarda_imagem]') IS NOT NULL
	drop procedure dbo.up_dicionario_guarda_imagem 
go

create PROCEDURE [dbo].up_dicionario_guarda_imagem

@ref	varchar(25),
@site   varchar(20)

/* with encryption */
AS
SET NOCOUNT ON
	

	Declare @pathDic   varchar(254) = ''
	Declare @siteNo    int = 0


	set @site = isnull(@site,'')

	select 
		@siteNo=ltrim(rtrim(no)) 
	from 
		empresa(nolock)
	where 
		site = @site

	
	set @siteNo = isnull(@siteNo,0)

	select 
		top 1
		@pathDic=ltrim(rtrim(isnull(textValue,''))) 
	from 
		B_Parameters_site(nolock) 
	where 
		site=@site and 
		stamp='ADM0000000106'


	If OBJECT_ID('tempdb.dbo.#tempDir') IS NOT NULL
		DROP TABLE #tempDir
				

	CREATE TABLE #tempDir
	(
		sub varchar(100),
		depth INT,
		fileB bit
	)



	Declare @pathDicRef   varchar(254) = ''
	set  @pathDicRef = @pathDic + '\st\' 
		

	insert into  #tempDir
	EXEC xp_dirtree @pathDicRef , 2, 1


	declare @refFinal varchar(18) = ''

	select  @refFinal = isnull(left(ltrim(rtrim(sub)),7),'') from #tempDir
	where fileB = 1 and sub like '%.png' and len(ltrim(rtrim(sub)))=11 and sub like '%' + @ref + '%'


	if   not exists (select * from anexos(nolock) inner join st(nolock) on st.ststamp = anexos.regstamp where   anexos.tipo='DIC' and st.site_nr=@siteNo and st.ref=@ref)
		
		begin
			
				
		if exists (select *from st(nolock) where ref=@ref and site_nr=@siteNo) and isnull(@refFinal,'') !=''
				
		begin
					
			INSERT INTO [dbo].[anexos]
					([anexosstamp]
					,[regstamp]
					,[tabela]
					,[filename]
					,[descr]
					,[keyword]
					,[protected]
					,[ousrdata]
					,[ousrinis]
					,[usrdata]
					,[usrinis]
					,[tipo]
					,[validade]
					,[readOnly])
				VALUES
					(LEFT(newid(),30)
					,(select ltrim(rtrim(ststamp)) from st(nolock) where ref=@ref and site_nr=@siteNo )
					,'st'
					,@ref + '.png'
					,'Ref: ' + @ref
					,@ref
					,0
					,getdate()
					,'ADM'
					,getdate()
					,'ADM'
					,'DIC'
					,'1900-01-01'
					,1
					)


		end
	end

	

			

If OBJECT_ID('tempdb.dbo.#tempDir') IS NOT NULL
	DROP TABLE #tempDir
			


Go
Grant Execute on dbo.up_dicionario_guarda_imagem to Public
Grant Control on dbo.up_dicionario_guarda_imagem to Public
Go


