-- exec	up_dicionario_dci '8168617'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_dicionario_dci]') IS NOT NULL
	drop procedure dbo.up_dicionario_dci
go

create PROCEDURE [dbo].[up_dicionario_dci]

@cnp	varchar(25)

/* with encryption */
AS
SET NOCOUNT ON


Select 
	moleculasx.descricao, 
	ctidefx.descricao AS patol,
	ctinvlx.descricao AS nvl,
	ctiexpx.descricao as frase 
FROM b_ctifp ctifpx (nolock)
	INNER JOIN b_ctiexp ctiexpx (nolock) ON ctifpx.ctiexpstamp = ctiexpx.ctiexpstamp
	INNER JOIN b_ctidef ctidefx (nolock) ON ctifpx.ctidefstamp = ctidefx.ctiefpstamp
	INNER JOIN b_ctinvl ctinvlx (nolock) ON ctifpx.ctinvlstamp = ctinvlx.ctinvlstamp 
	INNER JOIN b_moleculas moleculasx (nolock) ON ctifpx.moleculaID = moleculasx.moleculaID
	INNER JOIN b_moleculasfp moleculasfpx (nolock) ON ctifpx.moleculaID = moleculasfpx.moleculaID
where cnp=@cnp

Go
Grant Execute on dbo.up_dicionario_dci to Public
Grant Control on dbo.up_dicionario_dci to Public
Go