/*
	exec	up_dicionario_InteracoesMedicamentos '3202785'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_dicionario_InteracoesMedicamentos]') IS NOT NULL
	drop procedure dbo.up_dicionario_InteracoesMedicamentos
go

create PROCEDURE [dbo].up_dicionario_InteracoesMedicamentos

@cnp	varchar(25)

/* with encryption */
AS
SET NOCOUNT ON
	
	select 
		fprod.cnp
		,fprod_2.cnp
		,DesignMedInteracao = fprod_2.design
		,med_interacoes.med_class_b
		,med_interacoes.grau
		,med_interacoes.explicacao
		,med_interacoes.conselho
	from 
		fprod (nolock)
		inner join med_interacoes_lnk (nolock) on fprod.medid = med_interacoes_lnk.id_med_a 
		inner join med_interacoes (nolock) on med_interacoes_lnk.id_med_interacoes = med_interacoes.id
		left join fprod (nolock) as fprod_2 on fprod_2.medid = med_interacoes_lnk.id_med_b
	where
		fprod.cnp=@cnp
		and fprod_2.u_nomerc = 1
		
	union all
	
	select 
		fprod.cnp
		,fprod_2.cnp
		,DesignMedInteracao = fprod_2.design
		,med_interacoes.med_class_b
		,med_interacoes.grau
		,med_interacoes.explicacao
		,med_interacoes.conselho
	from 
		fprod (nolock)
		inner join med_interacoes_lnk (nolock) on fprod.medid = med_interacoes_lnk.id_med_b
		inner join med_interacoes (nolock) on med_interacoes_lnk.id_med_interacoes = med_interacoes.id
		left join fprod (nolock) as fprod_2 on fprod_2.medid = med_interacoes_lnk.id_med_a
	where
		fprod.cnp=@cnp
		and fprod_2.u_nomerc = 1
		
	order by
		DesignMedInteracao
		
	
Go
Grant Execute on dbo.up_dicionario_InteracoesMedicamentos to Public
Grant Control on dbo.up_dicionario_InteracoesMedicamentos to Public
Go
