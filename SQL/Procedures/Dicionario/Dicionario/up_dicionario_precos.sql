-- SP:	up_dicionario_precos
-- exec	up_dicionario_precos '8168617'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_dicionario_precos]') IS NOT NULL
	drop procedure dbo.up_dicionario_precos
go

create PROCEDURE [dbo].[up_dicionario_precos]

@cnp	varchar(25)

/* with encryption */
AS
SET NOCOUNT ON

SELECT 
	'Tipo'	= grupo, 
	'Preco' = epreco,
	'Data'	=Convert(char(10),data,104),
	fprecostamp
FROM 
	fpreco (nolock) 
WHERE
	cnp=@cnp
	AND UPPER(grupo) IN ('PVP','REF','PENS')
ORDER BY (CASE WHEN grupo='PVP' THEN 1 WHEN grupo='REF' THEN 2 WHEN grupo='PENS' THEN 3 ELSE 4 END)

Go
Grant Execute on dbo.up_dicionario_precos to Public
Grant Control on dbo.up_dicionario_precos to Public
Go