-- exec up_dicionario_ultActualizacao
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_dicionario_ultActualizacao]') IS NOT NULL
	drop procedure dbo.up_dicionario_ultActualizacao
go

Create PROCEDURE [dbo].up_dicionario_ultActualizacao

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON	


select top 1 
	'data' = convert(varchar,date,111) + ' ' + left(convert(varchar,date,108),5)
from 
	B_ocorrencias (nolock)
where 
	descr like '%Actualização de Dicionário%'
order by 
	date desc

GO
Grant Execute on dbo.up_dicionario_ultActualizacao to Public
Grant Control on dbo.up_dicionario_ultActualizacao to Public
go