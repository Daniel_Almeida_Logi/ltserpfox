-- exec up_Dicionario_pesquisaRef2 100, '', '', '', '', '', '', 0,'50074806'
-- exec up_dicionario_pesquisaRef2 500,'ben','','',''	,''	,''	,0	,''
-- exec up_Dicionario_pesquisaRef2 0, 'xxxxx', '', '', '', '', '', 0, ''
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_dicionario_pesquisaRef2]') IS NOT NULL
	drop procedure dbo.up_dicionario_pesquisaRef2
go

Create PROCEDURE [dbo].up_dicionario_pesquisaRef2
@top				int,
@ref				varchar(18),
@DCI				varchar(254),
@NOME_COMERCIAL		varchar(100),
@Embalagem			varchar(254),
@DOSAGEM			varchar(254),
@FORMA_TERAPEUTICA	varchar(100),
@incluir			bit,
@cnpem				varchar(8) = ''

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON
	

;with
	cte3 (nome,ref) as (
		Select	nome,ref
		From	stfami (nolock)
	)

Select top (@top)
	sel						= convert(bit,0)
	,seldci					= convert(bit,0)
	,REF					= fprod.cnp
	,DESCRICAO				= fprod.descricao
	,DCI					= fprod.dci
	,descr					= fprod.nome + ' ' + convert(varchar,dosuni) + ' ' + fformasdescr +  ' ' + fprod.descricao
	,NOME_COMERCIAL			= fprod.nome
	,DOSAGEM				= dosuni
	,FORMA_FARMACEUTICA		= fformasdescr
	,GRUPO_TERAPEUTICO		= '' /*Tabela n�o alimentada*/
	,DIMENSAO_EMBALAGEM		= fprod.descricao
	,TAXA_COMPARTICIPACAO	= isnull(cptval.pct,0)
	,PVP					= isnull(case when pvpcalc=0 then pvporig else pvpcalc end,0)
	,PUT					= fprod.precouni
	,UTENTE					= ROUND(isnull(case when pvpcalc=0 then pvporig else pvpcalc end,0) - (isnull(case when pvpcalc=0 then pvporig else pvpcalc end,0)*(isnull(cptval.pct,0)/100)),2)
	,PSNS					= ROUND(isnull(case when pvpcalc=0 then pvporig else pvpcalc end,0)*(isnull(cptval.pct,0)/100),2)
	,codGrupoH				= grphmgcode
	,designGrupoH			= grphmgdescr
	,fabricanteno			= titaim
	,fabricantenome			= titaimdescr
	,familia				= u_familia
	,faminome				= isnull(cte3.nome,'')
	,generico 
	,psico 
	,benzo
	,manipulado
	,protocolo
	,tipembdescr
	,estadocomercial		= sitcomdescr
	,posologia				= u_posprog
	,ESTADO					= estaimdescr
	,grupo					= fprod.grupo
	,pmu					= fprod.precouni
	,num_unidades			= num_unidades
	,tipo_duracao			= fprod.tiptratdescr
	,lista_dcis				= fprod.dci
	,cnpem			
From	
	fprod (nolock)
	left join cptval (nolock)	on fprod.grupo=cptval.grupo and cptplacode='01'
	left join cte3				on u_familia=cte3.ref
WHERE  	
	(fprod.cnp	like @ref + '%' or design like @ref + '%')
	and fprod.cnpem = case when @cnpem = '' then fprod.cnpem else @cnpem end
	and fprod.dci like case when @dci<>'' then '%' + @DCI +'%' else fprod.dci end
	and fprod.nome like '%' + case when @NOME_COMERCIAL	= '' THEN fprod.nome + '%' ELSE @NOME_COMERCIAL + '%' END	
	and fformasdescr like '%' + case when @FORMA_TERAPEUTICA = ''  THEN fformasdescr + '%' ELSE	@FORMA_TERAPEUTICA + '%' END		
	and fprod.descricao = case when @Embalagem = '' then  fprod.descricao else @Embalagem end 
	and convert(varchar(254),fprod.dosuni) = case when @DOSAGEM = '' then  convert(varchar(254),fprod.dosuni) else @DOSAGEM end
	and (
		estaimdescr like case when @incluir=1 then '%%' else 'Autorizado' end OR manipulado=1 OR estaimdescr = ''
	)
	
ORDER BY 
	case when precouni=0 then 9999999 else precouni end
	,case when isnull(pvporig,0)=0 then 9999999 else isnull(pvporig,0) end
	,NOME_COMERCIAL  asc

GO
Grant Execute on dbo.up_dicionario_pesquisaRef2 to Public
Grant Control on dbo.up_dicionario_pesquisaRef2 to Public
go