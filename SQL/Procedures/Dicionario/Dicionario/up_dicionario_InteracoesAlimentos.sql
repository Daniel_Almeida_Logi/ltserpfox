-- exec	up_dicionario_InteracoesAlimentos '8168617'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_dicionario_InteracoesAlimentos]') IS NOT NULL
	drop procedure dbo.up_dicionario_InteracoesAlimentos
go

create PROCEDURE [dbo].up_dicionario_InteracoesAlimentos

@cnp	varchar(25)

/* with encryption */
AS
SET NOCOUNT ON
	
	select 
		fprod.cnp
		,alim.tipo
		,alim.efeito
		,alim.explicacao
	from 
		fprod (nolock)
		inner join med_interacoes_alimentos alim (nolock) on fprod.medid = alim.id_med
	where 
		fprod.cnp=@cnp
	order by
		alim.tipo

Go
Grant Execute on dbo.up_dicionario_InteracoesAlimentos to Public
Grant Control on dbo.up_dicionario_InteracoesAlimentos to Public
Go