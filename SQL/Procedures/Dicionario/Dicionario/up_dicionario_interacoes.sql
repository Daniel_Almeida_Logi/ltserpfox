-- exec	up_dicionario_interacoes '8168617'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_dicionario_interacoes]') IS NOT NULL
	drop procedure dbo.up_dicionario_interacoes
go

create PROCEDURE [dbo].[up_dicionario_interacoes]

@cnp	varchar(25)

/* with encryption */
AS
SET NOCOUNT ON

Select 
	moleculas1.descricao AS molecula1,
	moleculas2.descricao AS molecula2,
	moleculasfp2.cnp AS cnp2,
	fprod.design,
	intmedx.meddef AS tipo,
	intexpx.expdef AS frase,
	intexpx.predef AS prescricao,
	intexpx.disdef AS dispensa
FROM
	B_moleculasfp AS moleculasfp1 (nolock)
	INNER JOIN B_intexp as intexpx (nolock)
	INNER JOIN B_intfp as intfpx (nolock) ON intexpx.intexpstamp = intfpx.intexpstamp
	INNER JOIN B_intmed as intmedx (nolock) ON intfpx.intmedstamp = intmedx.intmedstamp ON moleculasfp1.moleculaID = intfpx.molecula1
	INNER JOIN B_moleculas AS moleculas1 (nolock) ON intfpx.molecula1 = moleculas1.moleculaID
	INNER JOIN B_moleculas AS moleculas2 (nolock) ON intfpx.molecula2 = moleculas2.moleculaID
	INNER JOIN B_moleculasfp AS moleculasfp2 (nolock) ON intfpx.molecula2 = moleculasfp2.moleculaID
	INNER JOIN fprod (nolock) ON moleculasfp2.cnp = fprod.cnp
WHERE
	moleculasfp1.cnp = @cnp
order by
	molecula1, cnp2, molecula2

Go
Grant Execute on dbo.up_dicionario_interacoes to Public
Grant Control on dbo.up_dicionario_interacoes to Public
Go