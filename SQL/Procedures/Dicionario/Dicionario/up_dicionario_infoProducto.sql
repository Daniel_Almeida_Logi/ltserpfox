
/*

Retorna informação do producto no dicionario

exec up_dicionario_infoProducto '2000396'


*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_dicionario_infoProducto]') IS NOT NULL
	drop procedure dbo.up_dicionario_infoProducto
go



create procedure dbo.up_dicionario_infoProducto

  @ref as varchar(50)



/* WITH ENCRYPTION */
AS

	select 
		top 1 
		"ref"  = isnull(emb.descr,''),
		"name" = isnull(emb.descr,''),
		"desc" = isnull(emb.descr,''),
		"comerc" = isnull(emb.comercializado,0),
		"lab" = isnull(lab.descr,''),
		"brand" = isnull(marca.descr,'')
	from emb(nolock)
	left join lab(nolock) on emb.id_lab = lab.id and emb.source = lab.source
	left join marca(nolock) on emb.id_marca = marca.id 
	where ref=@ref
	order by emb.data_alt desc
		
				
 
GO
Grant Execute On dbo.up_dicionario_infoProducto to Public
Grant Control On dbo.up_dicionario_infoProducto to Public
GO


