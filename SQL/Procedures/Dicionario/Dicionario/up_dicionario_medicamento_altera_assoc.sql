/*
	
	BD	   : Cimi
	Author : DA
	Criado : 2022-04-06

	Cria uma tabela de alteração a certos atributos de medicamentos
	Usada pela associacao

	--Infarmed
	select * from EMBALAGEM
	select * from PRODUTO
	select * from REF_ESTADO_COMERC
	select * from REF_ESTADO_AIM
	--LtDic
	select * from [emb_comunica_assoc]
	delete from emb_comunica_assoc

	exec up_dicionario_medicamento_altera_assoc ''
	exec up_dicionario_medicamento_altera_assoc 'Autorizado'
	exec up_dicionario_medicamento_altera_assoc 'Não Renovado'
	exec up_dicionario_medicamento_altera_assoc 'Suspenso'
	exec up_dicionario_medicamento_altera_assoc 'Revogado'
	exec up_dicionario_medicamento_altera_assoc 'Caducado'

*/




if OBJECT_ID('[up_dicionario_medicamento_altera_assoc]') IS NOT NULL
	drop procedure up_dicionario_medicamento_altera_assoc
GO



create procedure up_dicionario_medicamento_altera_assoc


	@estadoAim	varchar(25)

AS
	SET NOCOUNT ON

	If OBJECT_ID('tempdb.dbo.#emb_estados') IS NOT NULL
		drop table #emb_estados;

	If OBJECT_ID('tempdb.dbo.#emb_diff') IS NOT NULL
		drop table #emb_diff;

	DECLARE @command nvarchar(max) = ''

	set @estadoAim = ltrim(rtrim(isnull(@estadoAim,'')))


	declare @nameDb varchar(100) = 'Infarmed_' + ltrim(rtrim(CONVERT(CHAR(8),getdate(),112)))




	--se não existir bd do infarmed
	IF NOT EXISTS(SELECT * FROM sys.databases WHERE name = @nameDb)
	begin
		select 
			'CNP',
			'Nome Prod',
			'Designação',
			'Estado Comercialização',
			'Estado Titular AIM',
			'Estado Embalagem'

		UNION ALL

		SELECT 
		[cnp]
		  ,[nome]
		  ,[design]
		  ,[estado_comerc]
		  ,[estado_titular_aim]
		  ,[estado_emb]
		from 
			[emb_comunica_assoc]
		where
			1=0

		return 
	end
		 

	create table #emb_estados
	(
		cnp                Varchar(20), 
		nome               Varchar(1000) NULL, 
		design             Varchar(1000) NULL,
		estado_comerc      Varchar(1000),
		estado_titular_aim Varchar(1000),
		estado_emb         Varchar(1000)
	)

	create table #emb_diff
	(
		cnp                Varchar(20), 
		nome               Varchar(1000) NULL, 
		design             Varchar(1000) NULL,
		estado_comerc      Varchar(1000),
		estado_titular_aim Varchar(1000),
		estado_emb         Varchar(1000)
	)
		


	--dados da bd do infarmed
	set @command='

	insert into #emb_estados
	select 
		EMBALAGEM.NR_REGISTO as cnp ,
		PRODUTO.NOME as nome,  
		EMBALAGEM.DESCR as design , 
		REF_ESTADO_COMERC.DESCR as estado_comerc, 
		REF_ESTADO_AIM.DESCR as estado_titular_aim,
		estado_emb.DESCR as estado_emb
	from ' +@nameDb + '.dbo.EMBALAGEM(nolock)
	left join ' +@nameDb + '.dbo.PRODUTO(nolock) on PRODUTO.PROD_ID = EMBALAGEM.PROD_ID
	left join ' +@nameDb + '.dbo.COMERCIALIZACAO(nolock) on COMERCIALIZACAO.EMB_ID = EMBALAGEM.EMB_ID
	left join ' +@nameDb + '.dbo.REF_ESTADO_COMERC(nolock) on REF_ESTADO_COMERC.EST_COMERC_ID =COMERCIALIZACAO.ESTADO_COMERC_ID
	left join ' +@nameDb + '.dbo.REF_ESTADO_AIM(nolock)  on REF_ESTADO_AIM.ESTADO_AIM_ID =PRODUTO.ESTADO_AIM_ID
	left join ' +@nameDb + '.dbo.REF_ESTADO_EMB(nolock) estado_emb on estado_emb.ESTADO_EMB_ID = EMBALAGEM.ESTADO_EMB_ID
	where isnull(DATA_INICIO,''1900-01-01'')<getdate() and  isnull(DATA_FIM,''3000-01-01'')>=getdate()'

	BEGIN TRY  		 
		print @command
		EXEC sp_executesql @command 
	END TRY  
	BEGIN CATCH
		select 
			'CNP',
			'Nome Prod',
			'Designação',
			'Estado Comercialização',
			'Estado Titular AIM',
			'Estado Embalagem'

		UNION ALL

		SELECT 
		[cnp]
		  ,[nome]
		  ,[design]
		  ,[estado_comerc]
		  ,[estado_titular_aim]
		  ,[estado_emb]
		from 
			[emb_comunica_assoc]
		where
			1=0

		return 
	END CATCH;   

	insert into #emb_diff (cnp,[estado_comerc],[estado_titular_aim],[estado_emb])  
	SELECT [cnp]
			  ,[estado_comerc]
			  ,[estado_titular_aim]
			  ,[estado_emb]
	FROM 
		[#emb_estados](nolock)
	where 
		[estado_titular_aim] = case when @estadoAim = ''  then  [estado_titular_aim] else @estadoAim end
	EXCEPT
	SELECT [cnp]
			  ,[estado_comerc]
			  ,[estado_titular_aim]
			  ,[estado_emb]
	FROM [emb_comunica_assoc](nolock)
	where 
		[estado_titular_aim] = case when @estadoAim = ''  then  [estado_titular_aim] else @estadoAim end

	if(@estadoAim='')
		delete from  [emb_comunica_assoc]
	else
		delete from  [emb_comunica_assoc] where  [estado_titular_aim]  = @estadoAim

	
	INSERT INTO [dbo].[emb_comunica_assoc]
			([cnp]
			,[nome]
			,[design]
			,[estado_comerc]
			,[estado_titular_aim]
			,[estado_emb]
			,[data_alt])
		SELECT [cnp]
			,[nome]
			,[design]
			,[estado_comerc]
			,[estado_titular_aim]
			,[estado_emb]
			,getdate()
		FROM #emb_estados
		where 
		[estado_titular_aim] = case when @estadoAim = ''  then  [estado_titular_aim] else @estadoAim end
	

	update #emb_diff set
		nome=ltrim(rtrim(isnull(#emb_estados.nome,''))),
		design=ltrim(rtrim(isnull(#emb_estados.design,'')))
	from   
		#emb_diff
	inner join #emb_estados on  #emb_estados.cnp = #emb_diff.cnp


	select 
			'CNP',
			'Nome Prod',
			'Designação',
			'Estado Comercialização',
			'Estado Titular AIM',
			'Estado Embalagem'

	UNION ALL

	select 
		[cnp]
		,[nome]
		,[design]
		,[estado_comerc]
		,[estado_titular_aim]
		,[estado_emb]
	 from #emb_diff

	If OBJECT_ID('tempdb.dbo.#emb_diff') IS NOT NULL
		drop table #emb_diff;


	If OBJECT_ID('tempdb.dbo.#emb_estados') IS NOT NULL
		drop table #emb_estados;

GO


GO
Grant Execute On dbo.up_dicionario_medicamento_altera_assoc to Public
Grant Control On dbo.up_dicionario_medicamento_altera_assoc to Public
Go


