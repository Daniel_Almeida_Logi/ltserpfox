-- exec	up_dicionario_contraindicacoes '8168617'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_dicionario_contraindicacoes]') IS NOT NULL
	drop procedure dbo.up_dicionario_contraindicacoes
go

create PROCEDURE [dbo].[up_dicionario_contraindicacoes]

@cnp	varchar(25)

/* with encryption */
AS
SET NOCOUNT ON


SELECT 
	moleculasx.descricao,
	inasenx.descricao AS sentido,
	inaalmx.descricao AS aliment,
	inaexpx.descricao AS frase
FROM b_inafp inafpx (nolock)
	INNER JOIN b_inaexp inaexpx (nolock) ON inafpx.inaexpstamp = inaexpx.inaexpstamp
	INNER JOIN b_inaalm inaalmx (nolock) ON inafpx.inaalmstamp = inaalmx.inaalmstamp
	INNER JOIN b_inasen inasenx (nolock)ON inafpx.inasenstamp = inasenx.inasenstamp 
	INNER JOIN b_moleculas moleculasx (nolock) ON inafpx.moleculaID = moleculasx.moleculaID
	INNER JOIN b_moleculasfp moleculasfpx (nolock) ON inafpx.moleculaID = moleculasfpx.moleculaID
where moleculasfpx.cnp=@cnp

Go
Grant Execute On  up_dicionario_contraindicacoes to Public
Grant Control On  up_dicionario_contraindicacoes to Public
Go