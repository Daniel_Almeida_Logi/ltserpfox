/* 
	Get dados servicesAuthentication
	exec up_get_servicesAuthentication 1

*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_servicesAuthentication]') IS NOT NULL
	drop procedure dbo.up_get_servicesAuthentication
go

create procedure [dbo].[up_get_servicesAuthentication]
	 @tipo		AS INT					

AS
BEGIN	
	SELECT 
		top 1
		convert(varchar(4000),acessToken) as acessToken,
		convert(varchar(4000),refreshToken) as refreshToken,
		endDateToken,
		DATEDIFF(day, GETDATE(), endDateToken) AS dateToRefresh
	FROM
		servicesAuthentication(NOLOCK)
	WHERE
		typeNr = @tipo and
		endDateToken > GETDATE()
	ORDER BY 
		endDateToken DESC
END

GO
Grant Execute on dbo.up_get_servicesAuthentication to Public
Grant control on dbo.up_get_servicesAuthentication to Public
GO

