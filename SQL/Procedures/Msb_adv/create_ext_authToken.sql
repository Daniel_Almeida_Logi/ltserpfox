
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'ext_authToken'))
BEGIN


CREATE TABLE [dbo].[ext_authToken](
	[stamp] [varchar](36) NOT NULL,
	[acessToken] [varchar](max) NOT NULL,
	[tokenType] [varchar](100) NOT NULL,
	[expiresIn] Numeric(10,0) NULL,
	[dateLimit] [datetime] NULL,
	[test] bit NULL,
	[receiverID] [varchar](50) NOT NULL,
	[ousrdata] [datetime] NULL,
	[ousrinis] [varchar](30) NULL,
	[usrdata] [datetime] NULL,
	[usrinis] [varchar](30) NULL,
 CONSTRAINT [PK_ext_authToken] PRIMARY KEY CLUSTERED 
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[ext_authToken] ADD  CONSTRAINT [DF_ext_authToken_acessToken]  DEFAULT ('') FOR [acessToken]

ALTER TABLE [dbo].[ext_authToken] ADD  CONSTRAINT [DF_ext_authToken_tokenType]  DEFAULT ('') FOR [tokenType]

ALTER TABLE [dbo].[ext_authToken] ADD  CONSTRAINT [DF_ext_authToken_expiresIn]  DEFAULT ('0') FOR [expiresIn]

ALTER TABLE [dbo].[ext_authToken] ADD  CONSTRAINT [DF_ext_authToken_dateLimit]  DEFAULT ('') FOR [dateLimit]

ALTER TABLE [dbo].[ext_authToken] ADD  CONSTRAINT [DF_ext_authToken_test]  DEFAULT ('0') FOR [test]

ALTER TABLE [dbo].[ext_authToken] ADD  CONSTRAINT [DF_ext_authToken_receiverID]  DEFAULT ('') FOR [receiverID]


ALTER TABLE [dbo].[ext_authToken] ADD  CONSTRAINT [DF_ext_authToken_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

ALTER TABLE [dbo].[ext_authToken] ADD  CONSTRAINT [DF_ext_authToken_ousrinis]  DEFAULT ('') FOR [ousrinis]

ALTER TABLE [dbo].[ext_authToken] ADD  CONSTRAINT [DF_ext_authToken_usrdata]  DEFAULT (getdate()) FOR [usrdata]

ALTER TABLE [dbo].[ext_authToken] ADD  CONSTRAINT [DF_ext_authToken_usrinis]  DEFAULT ('') FOR [usrinis]

END	
