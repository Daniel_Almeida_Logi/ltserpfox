/* busca o ultimo token case esteja em tempo valido 


	exec up_getAuthTokenValid '072.a','1'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_getAuthTokenValid]') IS NOT NULL
	DROP procedure dbo.up_getAuthTokenValid
GO

CREATE procedure dbo.up_getAuthTokenValid	
	@receiverID	 VARCHAR(50),
	@test		 BIT = 0

AS

	SELECT TOP 1 
		acessToken,
		tokenType,
		expiresIn

	FROM ext_authToken
		WHERE receiverID = @receiverID
		AND    test      = @test
		AND dateadd(minute, -10, dateLimit) > GETDATE()
	ORDER BY dateLimit desc 

GO
GRANT EXECUTE on dbo.up_getAuthTokenValid TO PUBLIC
GRANT CONTROL on dbo.up_getAuthTokenValid TO PUBLIC
GO