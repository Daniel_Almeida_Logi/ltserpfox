
--code to validate if the procedure exists, if exists, drops procedure
if OBJECT_ID('[dbo].[up_getDocumentConf]') IS NOT NULL
	drop procedure dbo.up_getDocumentConf
go


-- =============================================
CREATE PROCEDURE dbo.up_getDocumentConf
@dataini  as varchar(50),
@nmdoc as varchar(50),
@loja as varchar(50)
AS 
BEGIN 
	print '@dataini - ' + @dataini
	print '@nmdoc - ' + @nmdoc
	print '@loja - ' + @loja

	select 
		ft.ftstamp,
		(select	
			top 1
			transactionCode
		from 
			faturacao_eletronica_med (nolock)
		where
			faturacao_eletronica_med.ftstamp = ft.ftstamp
			and transactionCode != '') as transactionCode,
		year(fdata) as ano,
		month(fdata) as mes,
		empresa.site,
		nmdoc,
		empresa.id_lt as Id_Cl
	from 
		ft (nolock) 
	inner join 
		empresa (nolock) on ft.site = empresa.site		
	where 
		(nmdoc like @nmdoc or nmdoc like '%Factura SNS%')
		and fdata >= @dataini
		and empresa.site like @loja
		and ftstamp in (
						select top 1 
							ftstamp 
						from 
							faturacao_eletronica_med (nolock) 
						where 
							aceite = 1 and
							 transactionCode != '' 
							and	faturacao_eletronica_med.ftstamp = ft.ftstamp 
						order by	
							faturacao_eletronica_med.data desc)
END
GO
