/* retorna a informacao de um pedido 

	exec up_get_SinaveRequest '3E6687A5-3C4A-40A3-9399-6D4334F0ECC1'
	exec up_get_SinaveRequest '26F50B4A-BA07-49C6-905B-81357473B409'
*/
	

if OBJECT_ID('[dbo].[up_get_SinaveRequest]') IS NOT NULL
	drop procedure dbo.up_get_SinaveRequest
go

create procedure dbo.up_get_SinaveRequest
	@token			 varchar(36)

/* WITH ENCRYPTION */ 
AS
BEGIN


	SELECT 
		token																									                                     AS TOKEN,
		(select userSinave from empresa where empresa.site = ext_sinave.site )																		 AS username, 
		(select pwSinave from empresa where empresa.site = ext_sinave.site )																		 AS password, 
		 isnull(ext_sinave.codSinave ,'')																											 AS codValid,
		 requestType																																 AS requestType,
		 rnu																																		 AS rnu,
		 healthUsername																																 AS healthUsername,
		 healthUserCodPostalBase																													 AS healthUserCodPostalBase,
		 healthUserCodPostalSuffix																													 AS healthUserCodPostalSuffix,
		 healthUserInfCli																															 AS healthUserInfCli,
		 healthNameFinResp																															 AS healthNameFinResp,
		 healthUsernBenefEntidFinResp																												 AS healthUsernBenefEntidFinResp,
		 healthUserId																																 AS healthUserId,
		 healthUserContact																															 AS healthUserContact,
		 healthUserBirthDate																														 AS healthUserBirthDate,
		 healthUserFResid																															 AS healthUserFResid,
		 healthUserCountry																															 AS healthUserCountry,
		 healthUserCountryBorn																														 AS healthUserCountryBorn,
		 healthUserCountryResid																														 AS healthUserCountryResid,
		 healthUsersex																																 AS healthUsersex,
		 healthUserTypeDocIdent																														 AS healthUserTypeDocIdent,
		 niflab																																		 AS niflab,
		 codlabDep																																	 AS codlabDep,
		 codValidDate																																 AS codValidDate,
		 regCodOri                                                                                                                                   AS regCodOri,
		 regCodOriDate																																 AS regCodOriDate,
		 nHealthInstitutionEpisode																													 AS nHealthInstitutionEpisode,
		 nHealthInstitutionProcess																													 AS nHealthInstitutionProcess,
		 nInternalRequisition																														 AS nInternalRequisition,
		 nNationalRequisition																														 AS nNationalRequisition, 
		 outpatientCodLocalHarvest																													 AS outpatientCodLocalHarvest,
		 outpatientCodPostalBase																													 AS outpatientCodPostalBase,
		 outpatientCodPostalSuffix																													 AS outpatientCodPostalSuffix,
		 establishmentCodLocalPresc																													 AS establishmentCodLocalPresc,
		 establishmentCodPostalBase																													 AS establishmentCodPostalBase,
		 establishmentCodPostalSuffix																												 AS establishmentCodPostalSuffix,
		 establishmentInstPrescF																													 AS establishmentInstPrescF,
		 establishmentInstPrescNIF																													 AS establishmentInstPrescNIF,
		 establishmentInstPrescName																													 AS establishmentInstPrescName,
		 establishmentInstPrescriberCard																											 AS establishmentInstPrescriberCard,
		 establishmentEntProfPresc																													 AS establishmentEntProfPresc,
		 establishmentNameCliPresc																													 AS establishmentNameCliPresc,
		 siteTest																																	 AS siteTest
	FROM 
		ext_sinave (nolock)
	WHERE token= @token

END
GO
Grant Execute on dbo.up_get_SinaveRequest to Public
Grant control on dbo.up_get_SinaveRequest to Public
GO