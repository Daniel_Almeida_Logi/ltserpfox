/* 

	exec up_get_SinaveListDiseaseProd 'AF53F054-58A0-4351-9D5A-5BC801F41853'
	exec up_get_SinaveListDiseaseProd 
*/
	

if OBJECT_ID('[dbo].[up_get_SinaveListDiseaseProd]') IS NOT NULL
	drop procedure dbo.up_get_SinaveListDiseaseProd
go

create procedure dbo.up_get_SinaveListDiseaseProd
	@productToken			 varchar(36)

/* WITH ENCRYPTION */ 
AS
BEGIN


	SELECT 
		stamp																									                                     AS stamp,
		productToken																									                             AS productToken,
		prodCod																																		 AS prodCod,
		prodDateHarvest																															     AS prodDateHarvest,
		prodDescAdic																																 AS prodDescAdic,
		prodName																																	 AS prodName,
		prodObs																																		 AS prodObs,
		prodTable																																	 AS prodTable,
		analysisToken																																 AS analysisToken
	FROM 
		[ext_sinave_Disease_prod] (nolock)
	WHERE productToken= @productToken

END
GO
Grant Execute on dbo.up_get_SinaveListDiseaseProd to Public
Grant control on dbo.up_get_SinaveListDiseaseProd to Public
GO