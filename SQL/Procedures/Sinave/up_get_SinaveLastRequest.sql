/* retorna ultimo pedido do sinave para popular tabelas

	exec up_get_SinaveLastRequest 'fv.codval'
*/
	




if OBJECT_ID('[dbo].[up_get_SinaveLastRequest]') IS NOT NULL
	drop procedure dbo.up_get_SinaveLastRequest
go

create procedure dbo.up_get_SinaveLastRequest
	@codSinave as varchar(100)
	 

/* WITH ENCRYPTION */ 
AS
BEGIN


	SELECT 
		top 1
		 rtrim(ltrim(isnull(ext_sinave.token,'')))																																AS TOKEN,
		 rtrim(ltrim(isnull(ext_sinave.infofinalidade,'')))																														AS infofinalidade,
		 rtrim(ltrim(isnull(ext_sinave.batch_id,'')))	 																														AS batch_id,
		 rtrim(ltrim(isnull(ext_sinave.batch_valdate,'')))																														AS batch_valdate,
		 isnull((select   isnull(max( case when isnumeric(es.regCodOri) = 1 then es.regCodOri+1 else 1 end ),1)  from ext_sinave es(nolock) where codSinave=@codSinave),1)		AS regCodOri,
		 ext_sinave.ousrdata																																					AS ousrdata, 
		 rtrim(ltrim(isnull(ext_sinave_Disease_ProdAnalysis.name,'')) )																											AS analiseName,
		 rtrim(ltrim(isnull(ext_sinave_Disease_ProdAnalysis.codTest,'')) )																										AS analiseCode,
		 rtrim(ltrim(isnull(ext_sinave_Disease_ProdAnalysis.manufacturername,'')) )																								AS manufacturername,
		 rtrim(ltrim(isnull(ext_sinave_Disease_ProdAnalysis.manufacturerCode,'')) )																								AS manufacturerCode
	FROM 
		ext_sinave (nolock)
		left join ext_sinave_Disease (nolock) on ext_sinave.token = ext_sinave_Disease.token
		left join ext_sinave_Disease_Prod (nolock) on ext_sinave_Disease_Prod.productToken = ext_sinave_Disease.productToken
		left join ext_sinave_Disease_ProdAnalysis (nolock) on ext_sinave_Disease_ProdAnalysis.analysisToken = ext_sinave_Disease_Prod.analysisToken

	order by ext_sinave.ousrdata desc

END

Grant Execute on dbo.up_get_SinaveLastRequest to Public
Grant control on dbo.up_get_SinaveLastRequest to Public
GO






