/* 

	exec up_get_SinaveListAgent ''
	exec up_get_SinaveListAgent 
*/
	

if OBJECT_ID('[dbo].[up_get_SinaveListAgent]') IS NOT NULL
	drop procedure dbo.up_get_SinaveListAgent
go

create procedure dbo.up_get_SinaveListAgent
	@agentToken		 varchar(36)

/* WITH ENCRYPTION */ 
AS
BEGIN


	SELECT 
		stamp                                 AS stamp,
		agentToken							  AS agentToken,
		codAgent							  AS codAgent,
		nEst								  AS nEst,
		name								  AS name,
		obs									  AS obs,
		tram								  AS tram,
		tableAgent							  AS tableAgent,
		antimicrobialsToken					  AS antimicrobialsToken
	FROM 
		[ext_sinave_Disease_ProdAnalysisTech_A] (nolock)
	WHERE agentToken= @agentToken

END
GO
Grant Execute on dbo.up_get_SinaveListAgent to Public
Grant control on dbo.up_get_SinaveListAgent to Public
GO