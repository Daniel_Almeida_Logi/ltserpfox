/* 

	exec up_get_SinaveListAntimicrobials ''
	exec up_get_SinaveListAntimicrobials 
*/
	

if OBJECT_ID('[dbo].[up_get_SinaveListAntimicrobials]') IS NOT NULL
	drop procedure dbo.up_get_SinaveListAntimicrobials
go

create procedure dbo.up_get_SinaveListAntimicrobials
	@antimicrobialsToken		 varchar(36)

/* WITH ENCRYPTION */ 
AS
BEGIN


	SELECT 
		stamp                                 AS stamp,
		antimicrobialsToken					  AS antimicrobialsToken,
		codAntibiotic						  AS codAntibiotic,
		mic									  AS mic,
		nameAntibiotic						  AS nameAntibiotic,
		obsAntibiotic						  AS obsAntibiotic,
		mResistances						  AS mResistances,
		tsa									  AS tsa,
		tableAntibiotics					  AS tableAntibiotics
	FROM 
		[ext_sinave_Disease_ProdAnalysisTech_A_anti] (nolock)
	WHERE antimicrobialsToken= @antimicrobialsToken

END
GO
Grant Execute on dbo.up_get_SinaveListAntimicrobials to Public
Grant control on dbo.up_get_SinaveListAntimicrobials to Public
GO