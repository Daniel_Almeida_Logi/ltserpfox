/* 

exec up_Sinave_saveResponse '26F50B4A-BA07-49C6-905B-81357473B409',20160005,'O campo data_hora_envio_exame � de preenchimento obrigat�rio no formato �yyyy-mm-ddThh:mm:ss�','513794603_37411389',77583,0

*/
	

if OBJECT_ID('[dbo].[up_Sinave_saveResponse]') IS NOT NULL
	drop procedure dbo.up_Sinave_saveResponse
go

create procedure dbo.up_Sinave_saveResponse
	@token		 VARCHAR(36),
	@messageCode INT,
	@message	 VARCHAR(254),
	@regO		 VARCHAR(100),
	@regD        NUMERIC(10,0),
	@send		 INT
/* WITH ENCRYPTION */ 
AS
BEGIN
	DECLARE @datet datetime 
	DECLARE @regSinave VARCHAR(19)
	SET @regSinave =''
	set @datet=GETDATE()
	if(@send=1)
	BEGIN
		SET @regSinave =  convert(varchar(10),@regD) + 'X'
		SET @regSinave =  left( @regSinave+ '000000000000000000', 19)
	END

	IF(NOT EXISTS(SELECT * FROM ext_sinaveResponse(nolock) WHERE token=@token))
	BEGIN
		insert into ext_sinaveResponse(token,messageCode,[message],[send],regO,regD,regSinave,ousrdata,ousrinis,usrdata,usrinis)
		VALUES (@token,@messageCode,@message,@send,@regO,@regD,@regSinave,@datet,'ADM',@datet,'ADM')
	END
	ELSE
	BEGIN

		if exists(select top 1 * from ext_sinaveResponse(nolock) where send = 1 and len(regSinave)=19)
		begin			
				UPDATE ext_sinaveResponse
				set
					[message]='Comunica��o repetida',
					[send] = 1
				WHERE token=@token
		end
		else begin 
				UPDATE ext_sinaveResponse
				set
					[messageCode] =@messageCode,
					[message]=message,
					[send] =@send,
					[regO] = @regO,
					[regD] =  @regD,
					[usrdata] =@datet,
					[usrinis] ='ADM'
				WHERE token=@token

		end



	END 





END
GO
Grant Execute on dbo.up_Sinave_saveResponse to Public
Grant control on dbo.up_Sinave_saveResponse to Public
GO