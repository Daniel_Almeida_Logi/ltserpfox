/* 

	exec up_get_SinaveListTechnique '40F9B180-3B26-4B69-A99C-E45DAAEE0FD4'
exec up_get_SinaveListTechnique '40F9B180-3B26-4B69-A99C-E45DAAEE0FD4'
*/
	

if OBJECT_ID('[dbo].[up_get_SinaveListTechnique]') IS NOT NULL
	drop procedure dbo.up_get_SinaveListTechnique
go

create procedure dbo.up_get_SinaveListTechnique
	@techToken		 varchar(36)

/* WITH ENCRYPTION */ 
AS
BEGIN


	SELECT 
		stamp                                 AS stamp,
		techToken							  AS techToken,
		techCod								  AS techCod,
		name								  AS name,
		obs									  AS obs,
		codCPALRes							  AS codCPALRes,
		resQualitative						  AS resQualitative,
		resQuantitative						  AS resQuantitative,
		resObs								  AS resObs,
		referenceValue						  AS referenceValue,
		agentToken							  AS agentToken,
		subAgentToken						  AS subAgentToken
	FROM 
		[ext_sinave_Disease_ProdAnalysisTech] (nolock)
	WHERE techToken= @techToken

END
GO
Grant Execute on dbo.up_get_SinaveListTechnique to Public
Grant control on dbo.up_get_SinaveListTechnique to Public
GO