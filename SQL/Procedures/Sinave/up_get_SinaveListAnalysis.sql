/* 

	exec up_get_SinaveListAnalysis ''
	exec up_get_SinaveListAnalysis 
*/
	

if OBJECT_ID('[dbo].[up_get_SinaveListAnalysis]') IS NOT NULL
	drop procedure dbo.up_get_SinaveListAnalysis
go

create procedure dbo.up_get_SinaveListAnalysis
	@analysisToken		 varchar(36)

/* WITH ENCRYPTION */ 
AS
BEGIN


	SELECT 
		stamp										AS stamp,
		analysisToken								AS analysisToken,
		code										AS analysisCode,
		codInv										AS analysisCodInv,
		tableInv									AS analysisTableInv,
		manufacturerCode							AS analysisManufacturerCode,
		[name]										AS analysisName,
		Obs										    AS analysisObs,
		codTest										AS analysisCodTest,
		techToken									AS techToken
	FROM 
		[ext_sinave_Disease_ProdAnalysis] (nolock)
	WHERE analysisToken= @analysisToken

END
GO
Grant Execute on dbo.up_get_SinaveListAnalysis to Public
Grant control on dbo.up_get_SinaveListAnalysis to Public
GO