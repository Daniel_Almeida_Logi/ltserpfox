/* 

	exec up_get_SinaveListDisease ''
	exec up_get_SinaveListDisease 
*/
	

if OBJECT_ID('[dbo].[up_get_SinaveListDisease]') IS NOT NULL
	drop procedure dbo.up_get_SinaveListDisease
go

create procedure dbo.up_get_SinaveListDisease
	@token			 varchar(36)

/* WITH ENCRYPTION */ 
AS
BEGIN


	SELECT 
		stamp																									                                     AS stamp,
		token																									                                     AS TOKEN,
		diseaseCode																																	 AS diseaseCode,
		productToken																																 AS productToken
	FROM 
		[ext_sinave_Disease] (nolock)
	WHERE token= @token

END
GO
Grant Execute on dbo.up_get_SinaveListDisease to Public
Grant control on dbo.up_get_SinaveListDisease to Public
GO