/* 

	exec up_get_SinaveListSubAgent ''
	exec up_get_SinaveListSubAgent 
*/
	

if OBJECT_ID('[dbo].[up_get_SinaveListSubAgent]') IS NOT NULL
	drop procedure dbo.up_get_SinaveListSubAgent
go

create procedure dbo.up_get_SinaveListSubAgent
	@subAgentToken		 varchar(36)

/* WITH ENCRYPTION */ 
AS
BEGIN


	SELECT 
		stamp                                 AS stamp,
		subAgentToken						  AS subAgentToken,
		specAgent							  AS specAgent,
		descr								  AS descr,
		namelistSpecAgent					  AS namelistSpecAgent,
		obs									  AS obs,
		tableAgentSpec						  AS tableAgentSpec
	FROM 
		[ext_sinave_Disease_ProdAnalysisTech_S] (nolock)
	WHERE subAgentToken= @subAgentToken

END
GO
Grant Execute on dbo.up_get_SinaveListSubAgent to Public
Grant control on dbo.up_get_SinaveListSubAgent to Public
GO