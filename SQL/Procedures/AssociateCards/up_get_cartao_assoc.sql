/*
	cria linhas de registo de cartoes usados para determinada entidade
	EXEC up_get_cartao_assoc '32000100','8', '18856'
	EXEC up_get_cartao_assoc '3','8', '18856'
	EXEC up_get_cartao_assoc '501','8', '09999'

	
	select 
		*
	 from 
	   [LTSSQL01].[E01322A].dbo.ext_cards_assoc cards
	where 
	   cards.cardId = '2205000201'
	   and convert(date,cards.expiryDate) >= convert(date,getdate())
	   and convert(int,replace(cards.cptorgstamp,'ADM','')) = convert(int,8)
	

*/




SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_cartao_assoc]') IS NOT NULL
	drop procedure dbo.up_get_cartao_assoc
go

create procedure dbo.up_get_cartao_assoc
	@cardId       VARCHAR(100),
	@entityId     VARCHAR(20),
	@infarmedCode VARCHAR(30)


/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#clientTemp'))
		DROP TABLE #clientTemp
	
	declare  @conta int = 0

	select 
		*
	into
		#clientTemp
	from 
		[LTSSQL01].[E01322A].dbo.b_utentes b_u
	where 
		b_u.id!='' 
		and b_u.inactivo = 0 
		and b_u.tipo = 'Farm�cia' 
		and ISNUMERIC(b_u.id) = 1



	 select 
		@conta = COUNT(*)	
	 from 
		#clientTemp b_u
	 where
		convert(int,b_u.id)=convert(int,@infarmedCode)

	--valida id na bd da empresa
	if(@conta=0)
	begin
		select result = 0, obs = 'Erro na valida��o'
		return 
	end


	select 
		@conta = count(*)	
	 from 
	   [LTSSQL01].[E01322A].dbo.ext_cards_assoc cards
	where 
	   cards.cardId = @cardId
	   and convert(date,cards.expiryDate) >= convert(date,getdate())
	   and convert(int,replace(cards.cptorgstamp,'ADM','')) = convert(int,@entityId)
	


	if(@conta=0)
	begin
		select result = 0, obs = 'Cart�o Inv�lido'
		return 
	end

	select result = 1, obs = 'Validado com sucesso'

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#clientTemp'))
		DROP TABLE #clientTemp
	

GO
Grant Execute on dbo.up_get_cartao_assoc to Public
Grant Control on dbo.up_get_cartao_assoc to Public
GO


