/*
	cria linhas de registo de cartoes usados para determinada entidade
	EXEC up_esb_orders_card_insert '999999991','0018856','109','AFP','AFP',10
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_esb_orders_card_insert]') IS NOT NULL
	drop procedure dbo.up_esb_orders_card_insert
go

create procedure dbo.up_esb_orders_card_insert
	@card VARCHAR(36),
	@infarmedCode int,
	@entityCode VARCHAR(36),
	@entityDescr  VARCHAR(100),	
	@ass varchar(10),
	@monthGap int = 10
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#clientTemp'))
		DROP TABLE #clientTemp
	

	declare @conta int = 0
	
	set @card = ltrim(rtrim(isnull(@card,'')))
	set @entityCode = ltrim(rtrim(isnull(@entityCode,'')))
	set @entityDescr = ltrim(rtrim(isnull(@entityDescr,'')))
	
	SELECT 
		@conta = count(*)  
	FROM 
		AssociateCard_cards(nolock)  
	where 
		rtrim(ltrim(nrCartao))=@card and
		rtrim(ltrim(idOrg))=@entityCode 
		and ousrdata > DATEADD(month, @monthGap*-1, GETDATE())


	-- cart�o usado nos ultimos x meses
	if(@conta>0)
	begin
		select result = 0
		return 
	end


	set @conta = 0
	select 
		*
	into
		#clientTemp
	from 
		[LTSSQL01].[E01322A].dbo.b_utentes b_u

	where 
		b_u.id!='' 
		and b_u.inactivo = 0 
		and b_u.tipo = 'Farm�cia' 
		and ISNUMERIC(b_u.id) = 1

	select 
		@conta = COUNT(*)	
	 from 
		#clientTemp b_u
	 where
		convert(int,b_u.id)=convert(int,@infarmedCode)



	--valida id na bd da empresa
	if(@conta=0)
	begin
		select result = 0
		return 
	end
	
	-- se n�o foi utilizado	
	insert into AssociateCard_cards(nrCartao,idOrg,descr,ass,inactivo,data,ousrdata,infarmedCode)
	values(@card,@entityCode,@entityDescr,@ass,0,GETDATE(),GETDATE(),@infarmedCode)
	
    set @conta = 0
	SELECT 
		@conta = count(*)  
	FROM 
		AssociateCard_cards(nolock)  
	where 
		rtrim(ltrim(nrCartao))=@card and
		rtrim(ltrim(idOrg))=@entityCode 
		and ousrdata > DATEADD(month, @monthGap*-1, GETDATE())

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#clientTemp'))
		DROP TABLE #clientTemp
	
	
	--se criada devolve
	if(@conta>0)
	begin
		select result = 1
		return 
	end


   
GO
Grant Execute on dbo.up_esb_orders_card_insert to Public
Grant Control on dbo.up_esb_orders_card_insert to Public
GO

