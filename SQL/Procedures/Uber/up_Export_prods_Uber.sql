/****** Object:  StoredProcedure [dbo].[up_Export_prods_Uber]  

exec up_Export_prods_Uber 'FFerrSilva_NS','Loja 1','u_local2','robot',1
update b_parameters_site set numvalue = 0 WHERE stamp='ADM0000000222'
Script Date: 23/04/2024 11:31:42 ******/


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_Export_prods_Uber]') IS NOT NULL
	drop procedure dbo.up_Export_prods_Uber
go

create procedure [dbo].up_Export_prods_Uber
@site varchar (100),													--loja
@filtro varchar (100),													--campo na st que se quer filtrar
@descrfiltro varchar (100),												--valor para o qual se filtrar
@stkseguranca varchar (10),												--Para enviar o "Out of Stock" = 0 STK > STKSEGURANCA
@headers bit															--se contém cabeçalhos



/* with encryption */
AS
SET NOCOUNT ON
/* Elimina Tabelas */

DECLARE @UUID varchar (100) = 'd2a3fe50-9130-5475-8399-813655ba7766'			--Enviado pela UBER
DECLARE @no varchar (3)= (select no from empresa (nolock) where site =@site)
--PRINT @no
DECLARE @sql varchar (max)
DECLARE @headersdescr varchar (max) = '''UUID'',''[UPC / EAN]'',''[New Price]'',''[Out of Stock?]'''
DECLARE @margemextra varchar (5) = 
		(select CONVERT(varchar,numvalue) from B_Parameters_site WHERE stamp='ADM0000000222' and site = @site)

BEGIN
IF @headers = 1 

Select @sql = '
SELECT '+@headersdescr+'
UNION ALL
select '''+@UUID+'''
		,ref
		, CONVERT(VARCHAR,CONVERT(numeric (10,2),epv1*(1+('+@margemextra+'/100))))
		, CASE WHEN (CONVERT(VARCHAR,CONVERT(numeric (10,2),stock))) > (CONVERT(VARCHAR,CONVERT(numeric (10,2),'+@stkseguranca+')))
						THEN ''0''				
						ELSE ''1''
			END
		from st (nolock)
			where inactivo = 0 
				and site_nr = '+@no+' 
				and '+@filtro+' like ''%'+@descrfiltro+'%'''						
		+ CHAR(13) + CHAR(10)

ELSE
Select @sql = '
select '''+@UUID+'''
		,ref
		, CONVERT(VARCHAR,CONVERT(numeric (10,2),epv1*(1+('+@margemextra+'/100))))
		, CASE WHEN (CONVERT(VARCHAR,CONVERT(numeric (10,2),stock))) > (CONVERT(VARCHAR,CONVERT(numeric (10,2),'+@stkseguranca+')))
						THEN ''0''				
						ELSE ''1''
			END
		from st (nolock)
			where inactivo = 0 
				and site_nr = '+@no+' 
				and '+@filtro+' like ''%'+@descrfiltro+'%'''	
END
PRINT @sql
EXEC (@sql)


GO
Grant Execute on dbo.up_Export_prods_Uber to Public
Grant Control on dbo.up_Export_prods_Uber to public
GO
