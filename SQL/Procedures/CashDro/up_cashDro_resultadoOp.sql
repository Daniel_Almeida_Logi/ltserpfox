/*
	retorna resultado da operacao
	exec [dbo].[up_cashdro_resultadoOp] 'ADMB34085EE-D7BE-4183-A55'

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

If OBJECT_ID('tempdb.dbo.#CashDroMsgDTemp') IS NOT NULL
		DROP TABLE #CashDroMsgDTemp
GO


if OBJECT_ID('[dbo].[up_cashdro_resultadoOp]') IS NOT NULL
	drop procedure dbo.up_cashdro_resultadoOp
go


create procedure dbo.up_cashdro_resultadoOp

@token varchar(25) 

/* WITH ENCRYPTION */
AS
	
	SELECT 
		TOP 1
		cashdro_code
		,cashdro_data
		,cashDroFinalStatus
		,cashdro_msg_id
	INTO
		#CashDroMsgDTemp
	FROM
		cashdro_msg_d(NOLOCK)
	WHERE
		cashdro_msg_d.token=@token
	ORDER BY 
		cashdro_msg_d.ousrdata DESC


	SELECT  
		TOP 1
		cashdro_msg.code 
		,cashdro_msg.msg
		,cashdro_msg.request
		,"cashdro_code" = (SELECT cashdro_code FROM #CashDroMsgDTemp) 
		,"cashdro_data" = (SELECT cashdro_data FROM #CashDroMsgDTemp) 	
		,"cashDroFinalStatus" =  (SELECT cashDroFinalStatus FROM #CashDroMsgDTemp) 		
		,"cashdro_msg_id" =  (SELECT cashdro_msg_id FROM #CashDroMsgDTemp) 
	FROM 
		cashdro_msg(NOLOCK) 
	WHERE
		cashdro_msg.token=@token	
	ORDER BY 
		cashdro_msg.ousrdata DESC

		
	If OBJECT_ID('tempdb.dbo.#CashDroMsgDTemp') IS NOT NULL
		DROP TABLE #CashDroMsgDTemp
	GO

GO
Grant Execute On dbo.up_cashdro_resultadoOp to Public
Grant Control On dbo.up_cashdro_resultadoOp to Public
Go



