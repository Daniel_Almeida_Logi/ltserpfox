/****** Object:  StoredProcedure [dbo].[up_sap_InterfaceVendas]    Script Date: 20/03/2023 10:10:35 

exec up_sap_InterfaceVendas '20231119'

******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[up_sap_InterfaceVendas]
@data as datetime

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON
	
	;with cteDados as (

		--declare @data varchar(8)
		--set @data= '20131204'
		
		Select 
			[Aplica��o] = '07'
			,[N� do documento] = REPLICATE(' ',2-LEN(RTRIM(LTRIM(STR(ft.ndoc))))) + RTRIM(LTRIM(STR(ft.ndoc))) 
								+ REPLICATE('0',8-LEN(RTRIM(LTRIM(STR(ft.fno))))) + RTRIM(LTRIM(STR(ft.fno))) /*2 - Serie + 8 num doc */
			,[Data] = 
					RTRIM(LTRIM(STR(YEAR(fdata)))) 
					+ REPLICATE('0',2-LEN(RTRIM(LTRIM(STR(MONTH(fdata)))))) + RTRIM(LTRIM(STR(MONTH(fdata)))) 
					+ REPLICATE('0',2-LEN(RTRIM(LTRIM(STR(DAY(fdata)))))) + RTRIM(LTRIM(STR(DAY(fdata))))
			,[ClienteNo] = REPLICATE(' ',6-LEN(RTRIM(LTRIM(STR(ft.no))))) + RTRIM(LTRIM(STR(ft.no)))
			,[Cliente] = REPLICATE(' ',80-LEN(RTRIM(LTRIM(ft.nome)))) + RTRIM(LTRIM(ft.nome))
			,[ClienteNIF] = REPLICATE(' ',20-LEN(RTRIM(LTRIM(ft.ncont)))) + RTRIM(LTRIM(ft.ncont)
			,[ClienteMorada] = REPLICATE(' ',55-LEN(RTRIM(LTRIM(b_utentes.morada)))) + RTRIM(LTRIM(b_utentes.morada))
			,[ClienteCP] = REPLICATE(' ',45-LEN(RTRIM(LTRIM(b_utentes.codpost)))) + RTRIM(LTRIM(b_utentes.codpost))
			,[Servi�o] = REPLICATE(' ',4-LEN(RTRIM(LTRIM(STR(fi.cpoc))))) + RTRIM(LTRIM(STR(fi.cpoc)))
			,[CHAR4] = '    '
			,[Valor] = REPLICATE(' ',10-LEN(RTRIM(LTRIM(STR(convert(int,SUM(fi.ETILIQUIDO)*100)))))) 
					+ RTRIM(LTRIM(STR(convert(int,SUM(fi.ETILIQUIDO)*100))))
			,[IVA] = REPLICATE(' ',4-len(convert(varchar,(convert(int,fi.iva*100)))))+ convert(varchar,(convert(int,fi.iva*100)))
			,[Atribui��o] = REPLICATE(' ',18-LEN(RTRIM(LTRIM(STR(convert(int,ROUND(SUM(fi.ETILIQUIDO) - (SUM(fi.ETILIQUIDO) / (fi.iva/100+1)),2)*100))))))
					+RTRIM(LTRIM(STR(convert(int,ROUND(SUM(fi.ETILIQUIDO) - (SUM(fi.ETILIQUIDO) / (fi.iva/100+1)),2)*100))))
		from
			ft (nolock)
			inner join td (nolock) on td.ndoc = ft.ndoc
			inner join fi (nolock) on ft.ftstamp = fi.ftstamp
			inner join b_utentes (nolock) on ft.no = b_utentes.no and ft.estab = b_utentes.estab
		where
			ft.anulado = 0
			and td.tipodoc != 4
			and (
				/* Negar Fatura Entidades */ 
				(td.ndoc != 73 	and u_tipodoc != 5 and ft.fdata = @data)
				
				OR 
				/* Faturas entidades */
				(td.ndoc = 73 
				and ft.fdata = case
								when day(@data) = 8 
									then convert(datetime,right('00'+convert(varchar(2),MONTH(getdate())),2)+'/01/'+convert(varchar(4),year(getdate()))) -1 /* ultimo dia do m�s anterior */
								else '19000102'
								end)
									
				OR
				/* caso especial para as faturas resumo */
				(td.u_tipodoc = 5 and day(@data) = 7
					and fdata between (convert(datetime,right('00'+convert(varchar(2),MONTH(getdate())),2)+'/01/'+convert(varchar(4),year(getdate()))) -1) and @data)
				
				OR
				/* No dia 8, todas as notas de cr�dito com data de cria��o entre dia 1 e 7 e com data de fatura��o menor que a data de cria��o */
				(u_tipodoc = 7 and day(@data) = 8
					and ft.ousrdata between left(convert(varchar,@data,112),6)+'01' and left(convert(varchar,@data,112),6)+'07'
					and fdata < ft.ousrdata)

		
			)
			--and ft.fno=127215 
		group by
			ft.ndoc, ft.fno, ft.fdata, ft.no, ft.nome,ft.ncont,b_utentes.morada,b_utentes.codpost, fi.iva, fi.cpoc
	)

	Select
		valor = [Aplica��o]+[N� do documento]+[Data]+[ClienteNo]+[Cliente]+[ClienteNIF]+[ClienteMorada]+[ClienteCP]+[Servi�o]+[CHAR4]+[Valor]+[IVA]+[Atribui��o]
	from
		cteDados
GO
Grant Execute on dbo.up_sap_InterfaceVendas to Public
grant control on dbo.up_sap_InterfaceVendas to public
GO
----------------------------------------------------------


/*
	Interface RECEBIMENTOS  - Dia anterior

	exec up_sap_InterfaceRecebimentos '20160224'
	
	select 
		* 
		--sum(total)
	from b_pagcentral pc (nolock) 
		inner join cx (nolock) on cx.cxstamp=pc.cxstamp 
	where dabrir='20151231'
		--nratend='0002186801' 
		
-- Validar se existem pagamentos de cr�dito ---
select * from re where rdata='20151231'
-----------------------------------------------

	select
		--ft.ftstamp,ft.fdata,ft.nmdoc,ft.fno,total = convert(int,ft.etotal*100)
		ft.vendnm,vendedor,ndoc,total = convert(int,sum(etotal)*100)
	from
		--b_pagcentral (nolock) pc
		cx --on cx.cxstamp=pc.cxstamp
		inner join ft (nolock) on ft.cxstamp=cx.cxstamp--pc.nratend = ft.u_nratend 
		--left join ft2 (nolock) on ft2.ft2stamp = ft.ftstamp
		--left join td (nolock) on td.ndoc = ft.ndoc
		--left join B_pagCentral pc (nolock) on pc.nratend = ft.u_nratend 
	where
		--cx.dabrir = '20151231'
		ft.fdata = '20151231'
		--and convert(varchar,pc.odata,112) = '20151231'
		--and (isnull(pc.ano,year(getdate())) = YEAR('20151231') or pc.ano is null)
		--and	ft.anulado = 0
		and ft.vendedor=14
	group by
		ft.vendnm,vendedor,ndoc
	order by
		vendnm
		--ft.fno
		
	select cx.dabrir,cx.dfechar, ft.fdata from b_pagcentral pc (nolock) inner join cx (nolock) on cx.cxstamp=pc.cxstamp inner join ft (nolock) on ft.cxstamp=cx.cxstamp
	where ft.vendedor=14 and convert(date,pc.odata)='20151231'
	
	problemas:
		. Caixas n�o fechadas no pr�prio dia ex: dia 31 dezembro, uma das caixas era de dia 30 com o valor de 13.94 que representam a diferen�a entre o valor 322.89 (relat�rio de caixa do dia 31) e 336.83
		. Fatura, no dia 31 a Maria Joao Silva emitiu uma Fatura de 21.65� este valor aparece neste relat�rio mas n�o entrou em caixa.
		. S� deve enviar valores de caixas fechadas (Serafim, Filomena.
			. Ou seja, caixas de dia 30 foram fechadas no dia 31, logo documentos de venda cairam nas caixas de dia 30
	
	select * from re where rdata='20151231'
*/

if OBJECT_ID('[dbo].[up_sap_InterfaceRecebimentos]') IS NOT NULL
	drop procedure dbo.up_sap_InterfaceRecebimentos
go

create procedure dbo.up_sap_InterfaceRecebimentos
@data as datetime

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

; with cteDados as (

	Select
		[Aplica��o] = '07'
		,[N� do documento] = REPLICATE(' ',2-LEN(RTRIM(LTRIM(STR(ft.ndoc))))) + RTRIM(LTRIM(STR(ft.ndoc)))
							+ REPLICATE('0',8-LEN(RTRIM(LTRIM(STR(ft.fno))))) + RTRIM(LTRIM(STR(ft.fno))) /*2 - Serie + 8 num doc */
		,[Tipo Pagamento] = '01'
							--case
							--	when pc.evdinheiro != 0 then '  01' -- Dinheiro
							--	when pc.evdinheiro = 0 and pc.epaga2 != 0 then '  03' -- MB
							--	when pc.evdinheiro = 0 and pc.epaga2 = 0 and pc.epaga1 != 0 then '  04' -- Visa
							--	when pc.evdinheiro = 0 and pc.epaga2 = 0 and pc.epaga1 = 0 and pc.echtotal != 0 then '  02' -- Cheque
							--	else '  01'
							--	end
		,[Valor] = REPLICATE(' ',13-LEN(convert(varchar,convert(int,ft.etotal*100)))) + convert(varchar,convert(int,ft.etotal*100))
		,[Atribui��o] = REPLICATE(' ',18-LEN(RTRIM(LTRIM(STR(ft.fno))))) + RTRIM(LTRIM(STR(ft.fno)))
	from
		ft (nolock)
		--inner join ft2 (nolock) on ft2.ft2stamp = ft.ftstamp
		inner join td (nolock) on td.ndoc = ft.ndoc
		--inner join cx (nolock) on ft.cxstamp=cx.cxstamp--pc.nratend = ft.u_nratend 
		--inner join B_pagCentral pc (nolock) on pc.nratend = ft.u_nratend 
												--and convert(varchar,pc.oData,112) = @data and pc.vendedor = ft.vendedor -- workaround para evitar duplicados
												--and pc.no=ft.no and pc.estab=ft.estab
	where
		ft.fdata = @data
		--cx.dabrir = @data
		--and (isnull(pc.ano,year(getdate())) = YEAR(@data) or pc.ano is null)
		and	ft.anulado = 0
		and u_tipodoc in (2,8,10,11)
		and ft.no > 199
		--and ft.fno=127215
)

Select
	--*
	valor = [Aplica��o]+[N� do documento]+[Tipo Pagamento] + [Valor] + [Atribui��o]
From
	cteDados
	

GO
Grant Execute on dbo.up_sap_InterfaceRecebimentos to Public
grant control on dbo.up_sap_InterfaceRecebimentos to public
GO
---------------------------------------------------------------

/* 
	Interface Compras  - Dia anterior
	
	exec up_sap_InterfaceCompras '20170829'
	
	select docdata,etotal,* from fo where adoc='3357' and no=582
	select ref, design, epv, qtt, iva, ivaincl, etiliquido, desconto,desc2,u_bonus,* from fn where fostamp='PD97A0014E-C244-49CD-ACD'
*/

if OBJECT_ID('[dbo].[up_sap_InterfaceCompras]') IS NOT NULL
	drop procedure dbo.up_sap_InterfaceCompras
go

create procedure dbo.up_sap_InterfaceCompras
@data as datetime

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON


	;WITH cteDados as (
	
		/* cabe�alho */
		Select 
			fo.fostamp
			,fo.docdata
			,fo.ousrdata
			,[ID Linha] = '1'
			,[Data Documento] = convert(varchar,fo.docdata,112)
			,[Tipo Documento] = convert(varchar,fo.doccode)
			,[N� da fatura] = fo.adoc
			,[N� da encomenda] = ''
			,[Fornecedor] = convert(varchar,fo.no)
			,[Moeda] = 'EUR'
			,[Tipo produto] = ''
			,[Montante] = ''
			,[Bloqueio fatura] = convert(varchar,fo.bloqpag)
			,[Cond. pagamento] = isnull((select top 1 tpstamp from tp where descricao = fo.tpdesc),'')
			,[Codigo IVA] = ''
			,[Taxa IVA] = ''
			,[IVA Intracomunit�rio] = case when  pais = 2 then '1' else '' end
			,[Texto item] = convert(varchar,foid)
			,[Atribui��o] = ''
		from
			fo (nolock)
		where
			doccode in (3,55,58)
			and fo.ousrdata = @data

		union all

		/* linhas */
		Select 
			fo.fostamp,
			fo.docdata,
			fo.ousrdata,
			[ID Linha] = '2'
			,[Data Documento] = ''
			,[Tipo Documento] = ''
			,[N� da fatura] = ''
			,[N� da encomenda] = isnull((convert(varchar,bi.obrano)),'')
			,[Fornecedor] = ''
			,[Moeda] = ''
			,[Tipo produto] = '1'
			--,[Montante] = convert(varchar,convert(int,fn.etiliquido*100))
			,[Montante] = convert(varchar,
							case when fn.ivaincl=1 then convert(int,(fn.etiliquido / (fn.iva/100+1))*100) else convert(int,fn.etiliquido*100) end)
			,[Bloqueio fatura] = convert(varchar,fo.bloqpag)
			,[Cond. pagamento] = ''
			,[Codigo IVA] = convert(varchar,fn.tabiva)
			,[Taxa IVA] = convert(varchar,convert(int,fn.iva*100))
			,[IVA Intracomunit�rio] = case when  pais = 2 then '1' else '' end
			,[Texto item] = convert(varchar,foid)
			,[Atribui��o] = ''
		from 
			fo (nolock)
			inner join fn (nolock) on fo.fostamp = fn.fostamp
			left join bi (nolock) on bi.bistamp = fn.bistamp
		where 
			doccode in (3,55,58)
			and fo.ousrdata = @data

		union all	

		/* RESUMO IVA*/
		Select
			fo.fostamp,
			fo.docdata, 
			fo.ousrdata,
			[ID Linha] = '3'
			,[Data Documento] = ''
			,[Tipo Documento] = ''
			,[N� da fatura] = ''
			,[N� da encomenda] = ''
			,[Fornecedor] = ''
			,[Moeda] = ''
			,[Tipo produto] = ''
			,[Montante] = convert(varchar,convert(int,fo.eivav1*100))
			,[Bloqueio fatura] = convert(varchar,fo.bloqpag)
			,[Cond. pagamento] = ''
			,[Codigo IVA] = '1'
			,[Taxa IVA] = convert(varchar,convert(int,ivatx1*100))
			,[IVA Intracomunit�rio] = case when  pais = 2 then '1' else '' end
			,[Texto item] = convert(varchar,foid)
			,[Atribui��o] = ''
		from 
			fo (nolock) 
			inner join fo2 (nolock) on fo2stamp = fostamp
		where 
			doccode in (3,55,58)
			and eivav1 != 0
			and fo.ousrdata = @data

		union all

		Select 
			fo.fostamp,
			fo.docdata,
			fo.ousrdata,
			[ID Linha] = '3'
			,[Data Documento] = ''
			,[Tipo Documento] = ''
			,[N� da fatura] = ''
			,[N� da encomenda] = ''
			,[Fornecedor] = ''
			,[Moeda] = ''
			,[Tipo produto] = ''
			,[Montante] = convert(varchar,convert(int,fo.eivav2*100))
			,[Bloqueio fatura] = convert(varchar,fo.bloqpag)
			,[Cond. pagamento] = ''
			,[Codigo IVA] = '2'
			,[Taxa IVA] = convert(varchar,convert(int,ivatx2*100))
			,[IVA Intracomunit�rio] = case when  pais = 2 then '1' else '' end
			,[Texto item] = convert(varchar,foid)
			,[Atribui��o] = ''
		from 
			fo (nolock)
			inner join fo2 (nolock) on fo2stamp = fostamp
		where 
			doccode in (3,55,58)
			and eivav2 != 0
			and fo.ousrdata = @data

		union all

		Select 
			fo.fostamp,
			fo.docdata,
			fo.ousrdata,
			[ID Linha] = '3'
			,[Data Documento] = ''
			,[Tipo Documento] = ''
			,[N� da fatura] = ''
			,[N� da encomenda] = ''
			,[Fornecedor] = ''
			,[Moeda] = ''
			,[Tipo produto] = ''
			,[Montante] = convert(varchar,convert(int,fo.eivav3*100))
			,[Bloqueio fatura] = convert(varchar,fo.bloqpag)
			,[Cond. pagamento] = ''
			,[Codigo IVA] = '3'
			,[Taxa IVA] = convert(varchar,convert(int,ivatx3*100))
			,[IVA Intracomunit�rio] = case when  pais = 2 then '1' else '' end
			,[Texto item] = convert(varchar,foid)
			,[Atribui��o] = ''
		from 
			fo (nolock)
			inner join fo2 (nolock) on fo2stamp = fostamp
		where 
			doccode in (3,55,58)
			and eivav3 != 0
			and fo.ousrdata = @data
		
		union all

		Select 
			fo.fostamp,
			fo.docdata,
			fo.ousrdata,
			[ID Linha] = '3'
			,[Data Documento] = ''
			,[Tipo Documento] = ''
			,[N� da fatura] = ''
			,[N� da encomenda] = ''
			,[Fornecedor] = ''
			,[Moeda] = ''
			,[Tipo produto] = ''
			,[Montante] = convert(varchar,convert(int,fo.eivav4*100))
			,[Bloqueio fatura] = convert(varchar,fo.bloqpag)
			,[Cond. pagamento] = ''
			,[Codigo IVA] = '4'
			,[Taxa IVA] = convert(varchar,convert(int,ivatx4*100))
			,[IVA Intracomunit�rio] = case when  pais = 2 then '1' else '' end
			,[Texto item] = convert(varchar,foid)
			,[Atribui��o] = ''
		from 
			fo (nolock) 
			inner join fo2 (nolock) on fo2stamp = fostamp
		where 
			doccode in (3,55,58)
			and eivav4 != 0
			and fo.ousrdata = @data
		
		union all

		Select 
			fo.fostamp,
			fo.docdata,
			fo.ousrdata,
			[ID Linha] = '3'
			,[Data Documento] = ''
			,[Tipo Documento] = ''
			,[N� da fatura] = ''
			,[N� da encomenda] = ''
			,[Fornecedor] = ''
			,[Moeda] = ''
			,[Tipo produto] = ''
			,[Montante] = convert(varchar,convert(int,fo.eivav5*100))
			,[Bloqueio fatura] = convert(varchar,fo.bloqpag)
			,[Cond. pagamento] = ''
			,[Codigo IVA] = '5'
			,[Taxa IVA] = convert(varchar,convert(int,ivatx5*100))
			,[IVA Intracomunit�rio] = case when  pais = 2 then '1' else '' end
			,[Texto item] = convert(varchar,foid)
			,[Atribui��o] = ''
		from 
			fo (nolock)
			inner join fo2 (nolock) on fo2stamp = fostamp
		where 
			doccode in (3,55,58)
			and eivav5 != 0
			and fo.ousrdata = @data
		
		union all

		Select 
			fo.fostamp,
			fo.docdata,
			fo.ousrdata,
			[ID Linha] = '3'
			,[Data Documento] = ''
			,[Tipo Documento] = ''
			,[N� da fatura] = ''
			,[N� da encomenda] = ''
			,[Fornecedor] = ''
			,[Moeda] = ''
			,[Tipo produto] = ''
			,[Montante] = convert(varchar,convert(int,fo.eivav6*100))
			,[Bloqueio fatura] = convert(varchar,fo.bloqpag)
			,[Cond. pagamento] = ''
			,[Codigo IVA] = '6'
			,[Taxa IVA] = convert(varchar,convert(int,ivatx6*100))
			,[IVA Intracomunit�rio] = case when  pais = 2 then '1' else '' end
			,[Texto item] = convert(varchar,foid)
			,[Atribui��o] = ''
		from 
			fo (nolock)
			inner join fo2 (nolock) on fo2stamp = fostamp
		where 
			doccode in (3,55,58)
			and eivav6 != 0
			and fo.ousrdata = @data
		
		union all

		Select 
			fo.fostamp,
			fo.docdata,
			fo.ousrdata,
			[ID Linha] = '3'
			,[Data Documento] = ''
			,[Tipo Documento] = ''
			,[N� da fatura] = ''
			,[N� da encomenda] = ''
			,[Fornecedor] = ''
			,[Moeda] = ''
			,[Tipo produto] = ''
			,[Montante] = convert(varchar,convert(int,fo.eivav7*100))
			,[Bloqueio fatura] = convert(varchar,fo.bloqpag)
			,[Cond. pagamento] = ''
			,[Codigo IVA] = '7'
			,[Taxa IVA] = convert(varchar,convert(int,ivatx7*100))
			,[IVA Intracomunit�rio] = case when  pais = 2 then '1' else '' end
			,[Texto item] = convert(varchar,foid)
			,[Atribui��o] = ''
		from 
			fo (nolock)
			inner join fo2 (nolock) on fo2stamp = fostamp
		where 
			doccode in (3,55,58)
			and eivav7 != 0	
			and fo.ousrdata = @data
			
		union all

		Select 
			fo.fostamp,
			fo.docdata,
			fo.ousrdata,
			[ID Linha] = '3'
			,[Data Documento] = ''
			,[Tipo Documento] = ''
			,[N� da fatura] = ''
			,[N� da encomenda] = ''
			,[Fornecedor] = ''
			,[Moeda] = ''
			,[Tipo produto] = ''
			,[Montante] = convert(varchar,convert(int,fo.eivav8*100))
			,[Bloqueio fatura] = convert(varchar,fo.bloqpag)
			,[Cond. pagamento] = ''
			,[Codigo IVA] = '8'
			,[Taxa IVA] = convert(varchar,convert(int,ivatx8*100))
			,[IVA Intracomunit�rio] = case when  pais = 2 then '1' else '' end
			,[Texto item] = convert(varchar,foid)
			,[Atribui��o] = ''
		from 
			fo (nolock)
			inner join fo2 (nolock) on fo2stamp = fostamp
		where 
			doccode in (3,55,58)
			and eivav8 != 0	
			and fo.ousrdata = @data
		
		union all

		Select
			fo.fostamp,
			fo.docdata, 
			fo.ousrdata,
			[ID Linha] = '3'
			,[Data Documento] = ''
			,[Tipo Documento] = ''
			,[N� da fatura] = ''
			,[N� da encomenda] = ''
			,[Fornecedor] = ''
			,[Moeda] = ''
			,[Tipo produto] = ''
			,[Montante] = convert(varchar,convert(int,fo.eivav9*100))
			,[Bloqueio fatura] = convert(varchar,fo.bloqpag)
			,[Cond. pagamento] = ''
			,[Codigo IVA] = '9'
			,[Taxa IVA] = convert(varchar,convert(int,ivatx9*100))
			,[IVA Intracomunit�rio] = case when  pais = 2 then '1' else '' end
			,[Texto item] = convert(varchar,foid)
			,[Atribui��o] = ''
		from 
			fo (nolock)
			inner join fo2 (nolock) on fo2stamp = fostamp
		where 
			doccode in (3,55,58)
			and eivav9 != 0
			and fo.ousrdata = @data
	)
	
	--select * from cteDados
	
	Select
 		valor = [ID Linha]
		+ REPLICATE(' ',8-len([Data Documento]))+[Data Documento]
		+ REPLICATE(' ',3-len([Tipo Documento])) + [Tipo Documento]
		+ REPLICATE(' ',20-len([N� da fatura])) + [N� da fatura]
		+ REPLICATE(' ',10-len([N� da encomenda])) + [N� da encomenda]
		+ REPLICATE(' ',10-len([Fornecedor])) + [Fornecedor]
		+ REPLICATE(' ',5-len([Moeda])) + [Moeda]
		+ REPLICATE(' ',1-len([Tipo produto]))+[Tipo produto]
		+ REPLICATE(' ',10-len([Montante])) + [Montante]
		+ REPLICATE(' ',1-len([Bloqueio fatura]))+[Bloqueio fatura]
		+ REPLICATE(' ',25-len(RTRIM(LTRIM([Cond. pagamento]))))+ RTRIM(LTRIM([Cond. pagamento]))
		+ REPLICATE(' ',1-len([Codigo IVA]))+[Codigo IVA]
		+ REPLICATE(' ',4-len([Taxa IVA])) + [Taxa IVA]
		+ REPLICATE(' ',1-len([IVA Intracomunit�rio]))+[IVA Intracomunit�rio]
		+ REPLICATE(' ',50-len([Texto item]))+[Texto item]
		+ REPLICATE(' ',18-len([Atribui��o]))+[Atribui��o]
	From
		cteDados
	Order by
		ousrdata, fostamp, [ID Linha]
	

GO
Grant Execute on dbo.up_sap_InterfaceCompras to Public
grant control on dbo.up_sap_InterfaceCompras to public
GO
----------------------------------------------------------


/* 
	Interface Caixa  - Dia anterior
	
	exec up_sap_InterfaceCaixa '20151216' 
*/

if OBJECT_ID('[dbo].[up_sap_InterfaceCaixa]') IS NOT NULL
	drop procedure dbo.up_sap_InterfaceCaixa
go

create procedure dbo.up_sap_InterfaceCaixa
	@data as datetime

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

declare @evdinheiro varchar(20), @epaga1 varchar(20),  @epaga2 varchar(20),  @echtotal varchar(20), @epaga3 varchar(20)
set @evdinheiro = 'DINHEIRO'
set @epaga1 = 'VISA'
set @epaga2 = 'MB'
set @echtotal = 'CHEQUE'
set @epaga3 = 'VALES'


;with cteDados as (

	/* DINHEIRO */
	select
		[Data documento] = CONVERT(varchar,x.dabrir,112)
		,[Empresa] = ' 407'
		,[Data lan�amento] =  CONVERT(varchar,x.dabrir,112)
		,[Refer�ncia] = convert(varchar,x.pno) + '-01'
		,[Conta Banco] = ''
		,[Montante a Banco] = RTRIM(LTRIM(STR(convert(int,x.evdinheiro*100))))
		,[Montante Comiss�o] = ''
		,[Divis�o] = ''
		,[Centro Custo] = ''
		,[texto] = @evdinheiro
		,[Sobras] =	RTRIM(LTRIM(STR(convert(int,(
						case
							when x.fecho_dinheiro > x.evdinheiro then x.fecho_dinheiro - x.evdinheiro
							else 0
							end
					)*100))))
		,[Quebras] = RTRIM(LTRIM(STR(convert(int,(
						case 
							when x.fecho_dinheiro < x.evdinheiro then x.evdinheiro - x.fecho_dinheiro
							else 0
							end
					)*100))))
	from (
		select
			cx.dabrir, cx.pnome, cx.pno
			,fecho_dinheiro = sum(cx.fecho_dinheiro)
			,evdinheiro = sum(pc.evdinheiro)
		from
			cx (nolock)
			inner join b_pagcentral pc (nolock) on pc.cxstamp = cx.cxstamp
		where
			cx.dabrir = @data 
			and cx.fechada=1
		group by
			cx.dabrir, cx.pnome, cx.pno
		having
			sum(pc.evdinheiro) != 0
	) x


	Union all
	
	/*MB*/	
	select
		[Data documento] = CONVERT(varchar,x.dabrir,112)
		,[Empresa] = ' 407'
		,[Data lan�amento] =  CONVERT(varchar,x.dabrir,112)
		,[Refer�ncia] = convert(varchar,x.pno) + '-03'
		,[Conta Banco] = ''
		,[Montante a Banco] = RTRIM(LTRIM(STR(convert(int,x.epaga2*100))))
		,[Montante Comiss�o] = RTRIM(LTRIM(STR(convert(int,x.comissaoTPA*100))))
		,[Divis�o] = ''
		,[Centro Custo] = ''
		,[texto] = @epaga2
		,[Sobras] =	RTRIM(LTRIM(STR(convert(int,(
						case
							when x.fecho_mb > x.epaga2 then x.fecho_mb - x.epaga2
							else 0
							end
					)*100))))
		,[Quebras] = RTRIM(LTRIM(STR(convert(int,(
						case 
							when x.fecho_mb < x.epaga2 then x.epaga2 - x.fecho_mb
							else 0
							end
					)*100))))
	from (
		select
			cx.dabrir, cx.pnome, cx.pno
			,comissaoTPA = sum(cx.comissaoTPA)
			,fecho_mb = sum(cx.fecho_mb)
			,epaga2 = sum(pc.epaga2)
		from
			cx (nolock)
			inner join b_pagcentral pc (nolock) on pc.cxstamp = cx.cxstamp
		where
			cx.dabrir = @data 
			and cx.fechada=1
		group by
			cx.dabrir, cx.pnome, cx.pno
		having
			sum(pc.epaga2) != 0
	) x


	Union all
	
	/*VISA*/	
	select	
		[Data documento] = CONVERT(varchar,x.dabrir,112)
		,[Empresa] = ' 407'
		,[Data lan�amento] =  CONVERT(varchar,x.dabrir,112)
		,[Refer�ncia] = convert(varchar,x.pno) + '-02'
		,[Conta Banco] = ''
		,[Montante a Banco] = RTRIM(LTRIM(STR(convert(int,x.epaga1*100))))
		,[Montante Comiss�o] = ''
		,[Divis�o] = ''
		,[Centro Custo] = ''
		,[texto] = @epaga1
		,[Sobras] =	RTRIM(LTRIM(STR(convert(int,(
						case
							when x.fecho_visa > x.epaga1 then x.fecho_visa - x.epaga1
							else 0
							end
					)*100))))
		,[Quebras] = RTRIM(LTRIM(STR(convert(int,(
						case 
							when x.fecho_visa < x.epaga1 then x.epaga1 - x.fecho_visa
							else 0
							end
					)*100))))
	from (
		select
			cx.dabrir, cx.pnome, cx.pno
			,fecho_visa = sum(cx.fecho_visa)
			,epaga1 = sum(pc.epaga1)
		from
			cx (nolock)
			inner join b_pagcentral pc (nolock) on pc.cxstamp = cx.cxstamp
		where
			cx.dabrir = @data 
			and cx.fechada=1
		group by
			cx.dabrir, cx.pnome, cx.pno
		having
			sum(pc.epaga1) != 0
	) x
		

	Union all
	
	/*CHEQUE*/	
	select	
		[Data documento] = CONVERT(varchar,x.dabrir,112)
		,[Empresa] = ' 407'
		,[Data lan�amento] =  CONVERT(varchar,x.dabrir,112)
		,[Refer�ncia] = convert(varchar,x.pno) + '-02'
		,[Conta Banco] = ''
		,[Montante a Banco] = RTRIM(LTRIM(STR(convert(int,x.echtotal*100))))
		,[Montante Comiss�o] = ''
		,[Divis�o] = ''
		,[Centro Custo] = ''
		,[texto] = @echtotal
		,[Sobras] =	RTRIM(LTRIM(STR(convert(int,(
						case
							when x.fecho_cheques > x.echtotal then x.fecho_cheques - x.echtotal
							else 0
							end
					)*100))))
		,[Quebras] = RTRIM(LTRIM(STR(convert(int,(
						case 
							when x.fecho_cheques < x.echtotal then x.echtotal - x.fecho_cheques
							else 0
							end
					)*100))))
	from (
		select
			cx.dabrir, cx.pnome, cx.pno
			,fecho_cheques = sum(cx.fecho_cheques)
			,echtotal = sum(pc.echtotal)
		from
			cx (nolock)
			inner join b_pagcentral pc (nolock) on pc.cxstamp = cx.cxstamp
		where
			cx.dabrir = @data 
			and cx.fechada=1
		group by
			cx.dabrir, cx.pnome, cx.pno
		having
			sum(pc.echtotal) != 0
	) x

	union all

	/*
		VALES
	*/
	select	
		[Data documento] = CONVERT(varchar,x.dabrir,112)
		,[Empresa] = ' 407'
		,[Data lan�amento] =  CONVERT(varchar,x.dabrir,112)
		,[Refer�ncia] = convert(varchar,x.pno) + '-01'
		,[Conta Banco] = ''
		,[Montante a Banco] = RTRIM(LTRIM(STR(convert(int,x.epaga3*100))))
		,[Montante Comiss�o] = ''
		,[Divis�o] = ''
		,[Centro Custo] = ''
		,[texto] = @epaga3
		,[Sobras] =	RTRIM(LTRIM(STR(convert(int,(
						case
							when x.fecho_epaga3 > x.epaga3 then x.fecho_epaga3 - x.epaga3
							else 0
							end
					)*100))))
		,[Quebras] = RTRIM(LTRIM(STR(convert(int,(
						case 
							when x.fecho_epaga3 < x.epaga3 then x.epaga3 - x.fecho_epaga3
							else 0
							end
					)*100))))
	from (
		select
			cx.dabrir, cx.pnome, cx.pno
			,fecho_epaga3 = sum(cx.fecho_epaga3)
			,epaga3 = sum(pc.epaga3)
		from
			cx (nolock)
			inner join b_pagcentral pc (nolock) on pc.cxstamp = cx.cxstamp
		where
			cx.dabrir = @data 
			and cx.fechada=1
		group by
			cx.dabrir, cx.pnome, cx.pno
		having
			sum(pc.epaga3) != 0
	) x
)

Select
	 valor =
		[Data documento]
		+ [Empresa]
		+ [Data lan�amento]
		+ REPLICATE(' ',16-len(REPLICATE('0',6-LEN([Refer�ncia]))+[Refer�ncia])) + REPLICATE('0',6-LEN([Refer�ncia]))+[Refer�ncia]
		+ REPLICATE(' ',10 -len([Conta Banco])) + [Conta Banco]
		+ REPLICATE(' ',13 -len([Montante a Banco])) + [Montante a Banco]
		+ REPLICATE(' ',13 -len([Montante Comiss�o])) + [Montante Comiss�o]
		+ REPLICATE(' ',4 -len([Divis�o])) + [Divis�o]
		+ REPLICATE(' ',10 -len([Centro Custo])) + [Centro Custo]
		+ REPLICATE(' ',50 -len([texto])) + [texto]
		+ REPLICATE(' ',13 -len([Sobras])) + [Sobras]
		+ REPLICATE(' ',13 -len([Quebras])) + [Quebras]
from
	cteDados

go
Grant Execute on dbo.up_sap_InterfaceCaixa to Public
Grant control on dbo.up_sap_InterfaceCaixa to public
go
---------------------------------------------------------

/****** Object:  StoredProcedure [dbo].[up_sap_InterfaceVendas]    Script Date: 17/02/2025 17:52:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[up_sap_InterfaceVendas]
@data as datetime

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON
	
	;with cteDados as (

		--declare @data varchar(8)
		--set @data= '20131204'
		
		Select 
			[Aplica��o] = '07'
			,[N� do documento] = REPLICATE(' ',2-LEN(RTRIM(LTRIM(STR(ft.ndoc))))) + RTRIM(LTRIM(STR(ft.ndoc))) 
								+ REPLICATE('0',8-LEN(RTRIM(LTRIM(STR(ft.fno))))) + RTRIM(LTRIM(STR(ft.fno))) /*2 - Serie + 8 num doc */
			,[Data] = 
					RTRIM(LTRIM(STR(YEAR(fdata)))) 
					+ REPLICATE('0',2-LEN(RTRIM(LTRIM(STR(MONTH(fdata)))))) + RTRIM(LTRIM(STR(MONTH(fdata)))) 
					+ REPLICATE('0',2-LEN(RTRIM(LTRIM(STR(DAY(fdata)))))) + RTRIM(LTRIM(STR(DAY(fdata))))
			,[ClienteNo] = REPLICATE(' ',6-LEN(RTRIM(LTRIM(STR(ft.no))))) + RTRIM(LTRIM(STR(ft.no)))
			,[Cliente] = REPLICATE(' ',80-LEN(RTRIM(LTRIM(ft.nome)))) + RTRIM(LTRIM(ft.nome))
			,[ClienteNIF] = REPLICATE(' ',20-LEN(RTRIM(LTRIM(ft.ncont)))) + RTRIM(LTRIM(ft.ncont))
			,[ClienteMorada] = REPLICATE(' ',55-LEN(RTRIM(LTRIM(b_utentes.morada)))) + RTRIM(LTRIM(b_utentes.morada))
			,[ClienteCP] = REPLICATE(' ',45-LEN(RTRIM(LTRIM(b_utentes.codpost)))) + RTRIM(LTRIM(b_utentes.codpost))
			,[Servi�o] = REPLICATE(' ',4-LEN(RTRIM(LTRIM(STR(fi.cpoc))))) + RTRIM(LTRIM(STR(fi.cpoc)))
			,[CHAR4] = '    '
			,[Valor] = REPLICATE(' ',10-LEN(RTRIM(LTRIM(STR(convert(int,SUM(fi.ETILIQUIDO)*100)))))) 
					+ RTRIM(LTRIM(STR(convert(int,SUM(fi.ETILIQUIDO)*100))))
			,[IVA] = REPLICATE(' ',4-len(convert(varchar,(convert(int,fi.iva*100)))))+ convert(varchar,(convert(int,fi.iva*100)))
			,[Atribui��o] = REPLICATE(' ',18-LEN(RTRIM(LTRIM(STR(convert(int,ROUND(SUM(fi.ETILIQUIDO) - (SUM(fi.ETILIQUIDO) / (fi.iva/100+1)),2)*100))))))
					+RTRIM(LTRIM(STR(convert(int,ROUND(SUM(fi.ETILIQUIDO) - (SUM(fi.ETILIQUIDO) / (fi.iva/100+1)),2)*100))))
		from
			ft (nolock)
			inner join td (nolock) on td.ndoc = ft.ndoc
			inner join fi (nolock) on ft.ftstamp = fi.ftstamp
			inner join b_utentes (nolock) on ft.no = b_utentes.no and ft.estab = b_utentes.estab
		where
			ft.anulado = 0
			and td.tipodoc != 4
			and (
				/* Negar Fatura Entidades */ 
				(td.ndoc != 73 	and u_tipodoc != 5 and ft.fdata = @data)
				
				OR 
				/* Faturas entidades */
				(td.ndoc = 73 
				and ft.fdata = case
								when day(@data) = 8 
									then convert(datetime,right('00'+convert(varchar(2),MONTH(getdate())),2)+'/01/'+convert(varchar(4),year(getdate()))) -1 /* ultimo dia do m�s anterior */
								else '19000102'
								end)
									
				OR
				/* caso especial para as faturas resumo */
				(td.u_tipodoc = 5 and day(@data) = 7
					and fdata between (convert(datetime,right('00'+convert(varchar(2),MONTH(getdate())),2)+'/01/'+convert(varchar(4),year(getdate()))) -1) and @data)
				
				OR
				/* No dia 8, todas as notas de cr�dito com data de cria��o entre dia 1 e 7 e com data de fatura��o menor que a data de cria��o */
				(u_tipodoc = 7 and day(@data) = 8
					and ft.ousrdata between left(convert(varchar,@data,112),6)+'01' and left(convert(varchar,@data,112),6)+'07'
					and fdata < ft.ousrdata)

		
			)
			--and ft.fno=127215 
		group by
			ft.ndoc, ft.fno, ft.fdata, ft.no, ft.nome,ft.ncont,b_utentes.morada,b_utentes.codpost, fi.iva, fi.cpoc
	)

	Select
		valor = [Aplica��o]+[N� do documento]+[Data]+[ClienteNo]+[Cliente]+[ClienteNIF]+[ClienteMorada]+[ClienteCP]+[Servi�o]+[CHAR4]+[Valor]+[IVA]+[Atribui��o]
	from
		cteDados

------------------------------------------------------------------------------------------------------
/*
	Interface Vendas 2 - Dia anterior

	Nota: Valores em iva
	
	exec up_sap_InterfaceVendas2 '20160224','20160224'
	exec up_relatorio_vendas_base_detalhe '20151216', '20151216', ''

*/

if OBJECT_ID('[dbo].[up_sap_InterfaceVendas2]') IS NOT NULL
	drop procedure dbo.up_sap_InterfaceVendas2
go

create procedure dbo.up_sap_InterfaceVendas2
	@dataIni as datetime
	,@dataFim as datetime

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	create table #IV2 (	nrAtend varchar(20) collate SQL_Latin1_General_CP1_CI_AI
						,ftstamp varchar(25) collate SQL_Latin1_General_CP1_CI_AI
						,fdata datetime, no numeric(10), estab numeric(3)
						,tipo varchar(20) collate SQL_Latin1_General_CP1_CI_AI
						,ndoc numeric(3), fno numeric(10), tipodoc numeric(2), u_tipodoc numeric(2)
						,ref varchar(18) collate SQL_Latin1_General_CP1_CI_AI, design varchar(100) collate SQL_Latin1_General_CP1_CI_AI
						,familia varchar(18) collate SQL_Latin1_General_CP1_CI_AI
						,u_epvp numeric(15,3), epcpond numeric(19,6), iva numeric(5,2), qtt numeric(11,3)
						,etiliquido numeric(19,6), etiliquidoSiva numeric(19,6), ettent1 numeric(13,3), ettent2 numeric(13,3)
						,ettent1siva numeric(13,3), ettent2siva numeric(13,3), desconto numeric(6,2), descvalor numeric(19,6), descvale numeric(19,6)
						,ousrhora varchar(8) collate SQL_Latin1_General_CP1_CI_AI, ousrinis varchar(3) collate SQL_Latin1_General_CP1_CI_AI
						,vendnm varchar(20) collate SQL_Latin1_General_CP1_CI_AI
						,u_ltstamp varchar(25) collate SQL_Latin1_General_CP1_CI_AI, u_ltstamp2 varchar(25), ecusto numeric(19,6) 
						,loja  varchar(50),loja_nr  numeric(5,0) )
						
	insert #IV2
	exec up_relatorio_vendas_base_detalhe @dataIni, @dataFim, ''
	
	--select data 
	--		+ ';' + convert(varchar,tipodoc) + ';' + convert(varchar,nrdoc) + ';' + operador + ';' + familia + ';' + lab + ';' + marca + ';' + ref + ';' + design 
	--		+ ';' + convert(varchar,pvp) + ';' + convert(varchar,iva) + ';' + convert(varchar,qt) + ';' + convert(varchar,utente) 
	--		+ ';' + convert(varchar,comp) + ';' + convert(varchar,ntAtend) + ';' + convert(varchar,pcp) + ';' + convert(varchar,descCom) + ';' + convert(varchar,descVale)
	--from (
		select
			data, tipodoc, nrdoc, operador
			,familia		= isnull(st.faminome,'')
			,lab			= isnull(st.u_lab,'')
			,marca			= isnull(st.usr1,'')
			,x.ref
			,x.design
			,pvp			= CONVERT(numeric(13,2),pvp)
			,iva
			,qt				= CONVERT(int,qt)
			,utente			= convert(numeric(13,2),round(utente,2))
			,comp			= convert(numeric(13,2),round(comp,2))
			,ntAtend		= nrAtendimentos
			,pcp			= convert(numeric(13,2),round(pcp,2))
			,descCom		= convert(numeric(13,2),round(descCom,2))
			,descVale		= convert(numeric(13,2),round(descvale,2))
			--,bb				= convert(numeric(13,2),round(bb,2))
			--,mbpv			= convert(numeric(13,2),round(
			--					case
			--						when pcp > 0 then (((utente + comp) / (pcp * qt)) - 1) * 100
			--						else 0
			--						end
			--					,2)) 
		from (
			select 
				data			= replace(convert(varchar,fdata,102),'.','-')
				,tipoDoc		= ndoc
				,nrDoc			= fno
				,operador		= ousrinis + ' - ' + vendnm
				,ref			= ref
				,design			= design
				,pvp			= (u_epvp/(iva/100+1))
				,iva			= iva
				,qt				= sum(qtt)
				,utente			= sum(etiliquidoSiva)
				,comp			= sum(ettent1siva + ettent2siva)
				,nrAtendimentos	= count(distinct nrAtend)

				,pcp			= sum(epcpond)
				,descCom		= sum(descvalor)
				,descVale		= sum(descvale)
				--,bb				= sum((etiliquido + ettent1 + ettent2) - (epcpond * qtt))
			from
				#IV2
			group by
				fdata, ndoc, fno, ousrinis, vendnm, nrAtend, ref, design, u_epvp, iva
		) x
		inner join st (nolock) on st.ref = x.ref collate SQL_Latin1_General_CP1_CI_AI and st.site_nr=1
	--) xx



GO
Grant Execute on dbo.up_sap_InterfaceVendas2 to Public
Grant Control on dbo.up_sap_InterfaceVendas2 to public
GO
----------------------------------------------------------