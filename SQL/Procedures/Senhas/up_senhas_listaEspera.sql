--exec up_senhas_listaEspera 'ADM0280F9D0-BBF4-4D48-BF4'

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_senhas_listaEspera]') IS NOT NULL
	drop procedure dbo.up_senhas_listaEspera 
go

create procedure dbo.up_senhas_listaEspera 
@token varchar (30)

/* WITH ENCRYPTION */
AS

	select 
		department_id
		,tickets_count
		,start_date
		,name
		, waiting_minutes = CONVERT(varchar, DATEADD(ss, waiting_minutes, 0), 108)  --Hours, Minutes, Seconds
	from 
		ext_xopvision_waiting_queue (nolock)
	where 
		token = @token



GO
Grant Execute On dbo.up_senhas_listaEspera to Public
Grant Control On dbo.up_senhas_listaEspera to Public
Go


