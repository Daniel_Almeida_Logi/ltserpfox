if exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME='ticket_request' )
begin
 drop table  ticket_request

end

				



CREATE TABLE [dbo].[ticket_request](
	[token] [varchar](40) NOT NULL,
	[stamp] [varchar](40) NOT NULL,
	[senderId] [varchar](40) NOT NULL,
	[senderName] [varchar](250) NOT NULL,
	[senderVat] [varchar](40) NOT NULL,
	[receiverId] [varchar](40) NOT NULL,
	[receiverName] [varchar](250) NOT NULL,
	[receiverVat] [varchar](40) NOT NULL,
	[entityId] [varchar](40) NOT NULL,
	[entityName] [varchar](250) NOT NULL,
	[entityApp] [varchar](40) NOT NULL,
	[clientId] [varchar](200) NOT NULL,
	[clientNumber] [int] NOT NULL,
	[clientDep] [int] NOT NULL,
	[clientVat] [varchar](40) NOT NULL,
	[clientSns] [varchar](40) NOT NULL,
	[clientEmail] [varchar](200) NOT NULL,
	[clientName] [varchar](200) NOT NULL,
	[clientLastName] [varchar](200) NOT NULL,
	[clientIp] [varchar](200) NOT NULL,
	[ticketType] [varchar](200) NOT NULL,
	[ticketTypeId] [varchar](200) NOT NULL,
	[ticketId] [varchar](200) NOT NULL,
	[ticketDate] [varchar](200) NOT NULL,
	[site] [varchar](20) NOT NULL,
	[date] [datetime] NOT NULL,
	[reqType] [int] NOT NULL,
	[status] [int] NOT NULL,
 CONSTRAINT [pk_ticket_request] PRIMARY KEY CLUSTERED 
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_ticket_request_token]    Script Date: 08/02/2021 18:03:06 ******/
CREATE NONCLUSTERED INDEX [IX_ticket_request_token] ON [dbo].[ticket_request]
(
	[token] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_token]  DEFAULT ('') FOR [token]
GO
ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_stamp]  DEFAULT ('') FOR [stamp]
GO
ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_senderId]  DEFAULT ('') FOR [senderId]
GO
ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_senderName]  DEFAULT ('') FOR [senderName]
GO
ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_senderVat]  DEFAULT ('') FOR [senderVat]
GO
ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_receiverId]  DEFAULT ('') FOR [receiverId]
GO
ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_receiverName]  DEFAULT ('') FOR [receiverName]
GO
ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_receiverVat]  DEFAULT ('') FOR [receiverVat]
GO
ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_entityId]  DEFAULT ('') FOR [entityId]
GO
ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_entityName]  DEFAULT ('') FOR [entityName]
GO
ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_entityApp]  DEFAULT ('') FOR [entityApp]
GO
ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_clientId]  DEFAULT ('') FOR [clientId]
GO
ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_clientNumber]  DEFAULT ((0)) FOR [clientNumber]
GO
ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_clientDep]  DEFAULT ((0)) FOR [clientDep]
GO
ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_clientVat]  DEFAULT ('') FOR [clientVat]
GO
ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_clientSns]  DEFAULT ('') FOR [clientSns]
GO
ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_clientEmail]  DEFAULT ('') FOR [clientEmail]
GO
ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_clientName]  DEFAULT ('') FOR [clientName]
GO
ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_clientLastName]  DEFAULT ('') FOR [clientLastName]
GO
ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_clientIp]  DEFAULT ('') FOR [clientIp]
GO
ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_ticketType]  DEFAULT ('') FOR [ticketType]
GO
ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_site]  DEFAULT ('') FOR [site]
GO
ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_date]  DEFAULT (getdate()) FOR [date]
GO
ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_reqType]  DEFAULT ((0)) FOR [reqType]
GO
ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_status]  DEFAULT ((0)) FOR [status]

GO

ALTER TABLE [dbo].[ticket_request] ADD  CONSTRAINT [DF_ticket_request_ticketTypeId]  DEFAULT ('') FOR [ticketTypeId]
GO
