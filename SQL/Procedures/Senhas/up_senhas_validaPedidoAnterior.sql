
/*

Daniel Almeida - 20210208

Sp para validar ultimo pedido de senhas de um determinado cliente


select * from ticket_request(nolock)

exec up_senhas_validaPedidoAnterior


*/






SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_senhas_validaPedidoAnterior]') IS NOT NULL
	drop procedure dbo.up_senhas_validaPedidoAnterior
go



create procedure dbo.up_senhas_validaPedidoAnterior
 @senderId          as varchar(40)
, @clientId         as varchar(200)
, @clientEmail      as varchar(200)
, @clientIp         as varchar(200)
, @ticketType       as varchar(200)
, @ticketTypeId     as varchar(200)
, @site             as varchar(20)
, @reqType          as int
, @timeMinus        as int
, @receiverId       as varchar(40)




/* WITH ENCRYPTION */
AS

	set @clientId       = rtrim(ltrim(isnull(@clientId,'')))
	set @clientEmail    = rtrim(ltrim(isnull(@clientEmail,'')))
	set @clientIp       = rtrim(ltrim(isnull(@clientIp,'')))
	set @ticketType     = rtrim(ltrim(isnull(@ticketType,'')))
	set @ticketTypeId   = rtrim(ltrim(isnull(@ticketTypeId,'')))
	set @site           = rtrim(ltrim(isnull(@site,'')))
	set @reqType        = isnull(@reqType,0)
	set @timeMinus      = isnull(@timeMinus,15)
	set @senderId       = rtrim(ltrim(isnull(@senderId,'')))
	set @receiverId      = rtrim(ltrim(isnull(@receiverId,'')))

	declare @count int = 0

	set @timeMinus = @timeMinus * -1

	if(ltrim(rtrim(@clientEmail))!='')
	begin
		select 
			@count = count(*) 
		from
			ticket_request(nolock)
		where 
			date>=DATEADD(MINUTE,@timeMinus,GETDATE())
			and ticketTypeId  =  @ticketTypeId
			and reqType       =  @reqType
			and site          =  @site	
			and senderId      =  @senderId	
			and receiverId    =  @receiverId	
			and clientEmail    = @clientEmail
			and status = 200
	end			
	else begin
		if(ltrim(rtrim(@clientIp))!='')
		select 
			@count = count(*) 
		from
			ticket_request(nolock)
		where 
			date>=DATEADD(MINUTE,@timeMinus,GETDATE())
			and ticketTypeId  =  @ticketTypeId
			and reqType       =  @reqType
			and site          =  @site	
			and senderId      =  @senderId	
			and receiverId    =  @receiverId	
			and clientIp      = @clientIp
			and status = 200
		

	end

	set @count = isnull(@count,0)

	
	If @count > 0 
		select getdate() AS DateMessag, 0 As StatMessag, 'Impossível Cria Senha' As DescMessag,  @site as site;	
	ELSE	
		select getdate() AS DateMessag, 1 As StatMessag,  'Possível Cria Senha'   As DescMessag,  @site as site;	
								
 
GO
Grant Execute On dbo.up_senhas_validaPedidoAnterior to Public
Grant Control On dbo.up_senhas_validaPedidoAnterior to Public
GO
