
/****** 
	devolve o response transaction id correspondente
	Object:  UserDefinedFunction [dbo].[uf_getTransactionResponseId]  Script Date: 18/03/2020 14:54:52
	select dbo.[uf_getTransactionResponseId]('JAE00C2A72D-232C-4C3F-A17')




 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[uf_getTransactionResponseId]') IS NOT NULL
    DROP FUNCTION [dbo].uf_getTransactionResponseId
GO



CREATE FUNCTION [dbo].uf_getTransactionResponseId 
(	
	@token varchar(36),
	@ftstamp  varchar(36)
)
RETURNS varchar(100)
AS
BEGIN
	
	declare @id varchar(100) = ''
	set @token = ltrim(rtrim(isnull(@token,'')))

	if(@token='')
		return ''
	
	select 	
		@id = 	rtrim(ltrim(isnull(returnContributionCorrelationId,''))) 
			
	from
		ft_compart(nolock)
	where
		token = @token
		and ftstamp = @ftstamp
		and type = 2

	return isnull(@id,'')
END
GO


