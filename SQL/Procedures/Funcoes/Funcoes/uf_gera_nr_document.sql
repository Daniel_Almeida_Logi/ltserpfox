
/****** 
	retorna o numero da factura
	Object:  UserDefinedFunction [dbo].[uf_gera_nr_document]  Script Date: 06/12/2021 14:54:52

	facturas

	select dbo.[uf_gera_nr_document]('ADM09D44A6B-9601-43D9-B64','FT','Loja 1')

	recibos

	select dbo.[uf_gera_nr_document]('ADM900DCB82-2084-49D8-A33','RE','Loja 1')



 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



if OBJECT_ID('[dbo].[uf_gera_nr_document]') IS NOT NULL
    DROP FUNCTION [dbo].uf_gera_nr_document
GO



CREATE FUNCTION [dbo].uf_gera_nr_document 
(	
	@stamp varchar(36),
	@table varchar(10),
	@site  varchar(20) 

)
RETURNS  varchar(60)
AS
BEGIN
	
	declare @docNo varchar(60) = ''	


	if(@table='FT')
	begin 
		select 
			top 1
			 @docNo = left(rtrim(ltrim(convert(varchar,ft.ndoc))) + '/' + rtrim(ltrim(CONVERT(varchar,ft.fno))) + '/' + RIGHT(rtrim(ltrim(CONVERT(varchar,ft.ftano))),2),60) 
		from 
			ft(nolock)
		inner join  
			B_cert(nolock) on ft.ftstamp = B_cert.stamp
		where 
			B_cert.stamp = @stamp and
			B_cert.site=@site
	end else if(@table='RE')
	begin
		select 
			top 1
			 @docNo = left(rtrim(ltrim(convert(varchar,re.ndoc))) + '/' + rtrim(ltrim(CONVERT(varchar,re.rno))) + '/' + RIGHT(rtrim(ltrim(CONVERT(varchar,re.reano))),2),60) 
		from 
			re(nolock)
		inner join  
			B_cert(nolock) on re.restamp = B_cert.stamp
		where 
			B_cert.stamp = @stamp and
			B_cert.site=@site

	end


	return ltrim(rtrim(isnull(@docNo,''))) 


end
GO









