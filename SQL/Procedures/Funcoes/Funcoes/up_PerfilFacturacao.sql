/* fun��o de Controlo de Perfis 

	exec up_PerfilFacturacao 1, '', ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_PerfilFacturacao]') IS NOT NULL
	drop function dbo.up_PerfilFacturacao
go

CREATE FUNCTION  [dbo].[up_PerfilFacturacao] (@userno varchar(254),@grupo varchar(254), @resumo varchar(254), @site varchar(60)= 'Loja 1')
RETURNS int
BEGIN     
    Declare @retorno as int    
	Set @retorno = (
		SELECT 	
			COUNT(*) as cont
		FROM	
			b_pf pf (nolock)
			left join b_pfu pfu (nolock) on pfu.pfstamp=pf.pfstamp
			left join b_pfg pfg (nolock) on pfg.pfstamp=pf.pfstamp
		WHERE 	
			pf.resumo = @resumo
			And (isnull(pfu.userno,9999) = @userno Or ISNULL(pfg.nome,'') = @grupo) 
			and (pfu.site = @site or pfg.site = @site)
	)    
    
    RETURN @retorno
END

GO
Grant Execute on dbo.up_PerfilFacturacao to Public
grant control on dbo.up_PerfilFacturacao to Public
GO

