SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cursorCallshrinkBD]') IS NOT NULL
	DROP PROCEDURE dbo.up_cursorCallshrinkBD
GO

CREATE PROCEDURE dbo.up_cursorCallshrinkBD

AS
SET NOCOUNT ON



DECLARE @DB_Name varchar(100) 
DECLARE @Command nvarchar(200) 
DECLARE database_cursor CURSOR FOR 
SELECT * 
FROM MASTER.sys.sysdatabases 
WHERE name like 'F%' or  name like 'P%'  or  name like 'C%' 

OPEN database_cursor 

FETCH NEXT FROM database_cursor INTO @DB_Name 

WHILE @@FETCH_STATUS = 0 
BEGIN 
     SELECT @Command = 'SELECT ' + '''' + @DB_Name + '''' + ', SF.filename, SF.size FROM sys.sysfiles SF'

     EXEC up_cursorCallshrinkBD @DB_Name

     FETCH NEXT FROM database_cursor INTO @DB_Name 
END 

CLOSE database_cursor 
DEALLOCATE database_cursor 



GRANT EXECUTE on dbo.up_shrinkBD TO PUBLIC
GRANT Control on dbo.up_shrinkBD TO PUBLIC
GO