SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_UpdateFieldsLine]') IS NOT NULL
	drop function dbo.up_UpdateFieldsLine
go

CREATE function [dbo].[up_UpdateFieldsLine] (@table sysname, @alias varchar(4)) returns varchar(max) as
begin
	
	DECLARE 
		@Counter			INT,
		@TotalCounter		INT,
		@name				SYSNAME,
		@oldname			varchar(max)

	DECLARE @Fields TABLE (ID INT IDENTITY (1,1) PRIMARY KEY, name SYSNAME)
	INSERT INTO @Fields SELECT name FROM sys.columns WHERE [object_id] = OBJECT_ID(@table) order by name;

	select 
		@Counter = 1,
		@totalcounter=0,
		@TotalCounter = COUNT(id) FROM @Fields
		
	WHILE @Counter <= @TotalCounter
		BEGIN
			
			SELECT @name = name FROM @Fields WHERE id = @Counter
			
			select 
				@oldname = isnull(@oldname,'') + @name + '=' + (case when @alias<>'' then @alias else 'b' end) + '.' + @name + (case when @counter=@totalcounter then '' else ',' end),
				@Counter = @Counter + 1

		END
	
	return(@oldname)
end
GO
Grant Execute On dbo.up_UpdateFieldsLine to Public
grant control on dbo.up_UpdateFieldsLine to Public
go