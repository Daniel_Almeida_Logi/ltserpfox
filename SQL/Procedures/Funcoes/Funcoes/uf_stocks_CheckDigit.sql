/*
	FUN��O PARA CALCULAR O CHECKDIGIT DO SIFARMA CLASSICO
	PASSAR APENAS CNPS COME�ADOS NAO POR 1!!!!
*/
if OBJECT_ID('[dbo].[uf_stocks_CheckDigit]') IS NOT NULL
	drop function dbo.uf_stocks_CheckDigit
go

create function [dbo].[uf_stocks_CheckDigit] (@ref varchar(6)) returns varchar(7) as
begin

	declare 
		@cnp numeric(6) = @ref,
		@counter int = 1,
		@lencounter int = 0,
		@var int,
		@vartotal int = 0,
		@varfinal float = 0
		
	select
		@lencounter = len(@cnp)
		
	while @counter <= @lencounter
		begin
		
			/* Valor inicial da variavel*/
			select @var = substring(convert(varchar,@cnp),@counter,1)
			
			/* Validar 1� posi��o apenas*/
			if @counter=1
				select @var = case  when @var not in ('2','3','4','5') then 0 else @var end	
			
			/* Verificar se o n� da posi��o � PAR */
			if (@counter % 2) = 0  
				select @var = @var * 2
			
			/* Se o valor VAR for superior a 9, somar os dois digitos do n�mero (ex: 12 = 1+2) */
			if @var > 9
				select @var = convert(int,substring(convert(varchar,@var),1,1)) + convert(int,substring(convert(varchar,@var),2,1))

			/* Somar valor ao total */
			select @vartotal = @var + @vartotal
			
			/* Proxima posi��o */ 
			select @counter = @counter + 1
		
		end	
							
		
		/* Subtrair 10 valores at� o valor ser menor que DEZ */				 
		while @vartotal > 10
		begin
			select @vartotal = @vartotal - 10
		end
				
		/* Se o resultado final for POSITIVO, subtrair 10 valores; caso contrario o resultado � este */		
		if @vartotal > 0
			select @varfinal = 10 - @vartotal
		else
			select @varfinal = @vartotal
		
		return @varfinal	
end

GO

grant execute on uf_stocks_CheckDigit to public
grant control on uf_stocks_CheckDigit to public
go