/* 
	SP retorna ShrinkBD 
	
	up_shrinkBD  'sucesso'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_shrinkBD]') IS NOT NULL
	DROP PROCEDURE dbo.up_shrinkBD
GO

CREATE PROCEDURE dbo.up_shrinkBD
@bdReceiver varchar(50)

AS
SET NOCOUNT ON

--para verificar o nome dos ficheiros que para fazer shrink
SELECT TYPE_DESC, NAME, size, max_size, growth, is_percent_growth, type 
FROM sys.database_files

--criar uma conta para eleminar pelo menos ate 20% dos logs 

DECLARE @backuplocation AS NVARCHAR(256)
DECLARE @logfile AS NVARCHAR(256)
DECLARE @shrinktosize AS INT

exec('USE '+ @bdReceiver)

--esta em MB 
SET @shrinktosize = 1000
SELECT @logfile=name from sys.database_files WHERE type = 1 

-- Shrink the log file
EXECUTE('DBCC SHRINKFILE(' + @logfile + ', ' + @shrinktosize + ')')



-- backup solotion
--SET @backuplocation = 'C:\Databases\Backups\SiteAudit_Backup.bak' 

--realizar o backup da base de dados para o path fornecido
--BACKUP DATABASE test TO DISK = @backuplocation WITH INIT;

GRANT EXECUTE on dbo.up_shrinkBD TO PUBLIC
GRANT Control on dbo.up_shrinkBD TO PUBLIC
GO


