
/****** 
	transforma uma referencia com letras numa por defeito
	select dbo.uf_replaceRef('valorMed')
	select dbo.uf_replaceRef('0213.0549')
	select dbo.uf_replaceRef('0000000')
	select dbo.uf_replaceRef('5440987')


 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[uf_replaceRef]') IS NOT NULL
    DROP FUNCTION [dbo].uf_replaceRef
GO



CREATE FUNCTION [dbo].uf_replaceRef 
(	
	@ref varchar(18)
)
RETURNS  varchar(18)
AS
BEGIN
	
	declare @val  varchar(18) = '00000000'

	set @ref =  REPLACE(@ref, ',', '-')
	set @ref =  REPLACE(@ref, '.', '-')



	if ISNUMERIC(@ref)=1
		set @val = @ref


	return isnull(ltrim(rtrim(@val)),'00000000')

END
GO



