/****** 
	Autor: Daniel Almeida
	Data : 2023-06-22

<<<<<<< HEAD
	Remove todos os caracteres n�o numericos de uma string
=======
	Remove todos os caracteres não numericos de uma string
>>>>>>> 7a9ec60b4c7edb8ba3cd7166a6b7f288ce5d18b8

	declare @str varchar(10) = '15s.1s'
	select dbo.[uf_StripNonNumerics](@str)


 ******/


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[uf_StripNonNumerics]') IS NOT NULL
	drop function dbo.uf_StripNonNumerics
go

CREATE FUNCTION [dbo].[uf_StripNonNumerics]
(
  @Temp varchar(255)
)
RETURNS varchar(255)
AS
Begin

    Declare @KeepValues as varchar(50)
    Set @KeepValues = '%[^0-9]%'
    While PatIndex(@KeepValues, @Temp) > 0
        Set @Temp = Stuff(@Temp, PatIndex(@KeepValues, @Temp), 1, '')

    Return @Temp
End
GO
Grant Execute On dbo.uf_StripNonNumerics to Public
grant control on dbo.uf_StripNonNumerics to Public
go