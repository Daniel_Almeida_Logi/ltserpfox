/****** 


	convert a data para a timezone enviada por paramentro


	entrada 
		@date datetime       - data inicial
		@@timeZone1 varchar  - timeZone inicial
		@@timeZone2 varchar  - timeZone final


	saida
		@dateConverted datetime       - data depois de convertida

	Lista de timezones suportadas
	SELECT * FROM sys.time_zone_info

	teste:
	select dbo.[uf_Convert_TimeZone](dateadd(hour, 1,getdate()),'W. Europe Standard Time','GMT Standard Time')

 ******/



if OBJECT_ID('[dbo].[uf_Convert_TimeZone]') IS NOT NULL
    DROP FUNCTION [dbo].[uf_Convert_TimeZone]
GO


CREATE FUNCTION [dbo].[uf_Convert_TimeZone] (@date datetime,@timeZone1 varchar(100),@timeZone2 varchar(100))
RETURNS datetime
AS
BEGIN
	declare @dateEnd datetime;
	SELECT @dateEnd = (@date at time zone @timeZone1) AT TIME ZONE @timeZone2
	return @dateEnd
END


GO


