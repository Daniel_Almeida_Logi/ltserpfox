
/****** 
	valida se deve calcular o fee
	Object:  UserDefinedFunction [dbo].[uf_calcFee]  Script Date: 18/03/2020 14:54:52
	select dbo.[uf_calcFee](10.213)



 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[uf_calcFee]') IS NOT NULL
    DROP FUNCTION [dbo].uf_calcFee
GO



CREATE FUNCTION [dbo].uf_calcFee 
(	
	@fee numeric(19,6)
)
RETURNS numeric(19,2)
AS
BEGIN
	
	declare @val bit = 0	
	declare @feeFinal  numeric(19,6) = @fee

	set @val = (select top 1 isnull(bool,0) from B_Parameters (nolock) where stamp='ADM0000000325')

	if(isnull(@val,0)=0)
		set @feeFinal=   0.00


	return round(isnull(@feeFinal,@fee),2)
END
GO



