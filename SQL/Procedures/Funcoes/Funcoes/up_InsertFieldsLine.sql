SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_InsertFieldsLine]') IS NOT NULL
	drop function dbo.up_InsertFieldsLine
go

CREATE function [dbo].[up_InsertFieldsLine] (@table sysname, @alias varchar(4)) returns varchar(max) as
begin

	DECLARE
		@Counter			INT,
		@TotalCounter		INT,
		@name				varchar(max),
		@oldname			varchar(max)

	DECLARE @Fields TABLE	(ID INT IDENTITY (1,1) PRIMARY KEY, name SYSNAME)
	INSERT INTO @Fields SELECT name FROM sys.columns WHERE [object_id] = OBJECT_ID(@table) order by name;

	select
		@Counter = 1,
		@TotalCounter = COUNT(id) FROM @Fields
		
	WHILE @Counter <= @TotalCounter
		BEGIN
			/* Ler o nome do campo a ser seleccionado agora */
			SELECT @name = name FROM @Fields WHERE id = @Counter
			
			/* Juntar a esse nome o anterior, que no caso do 1 campo ser� NULL		*/
			if @alias <> ''
				begin
					if @counter = 1
					   select @oldname = @alias + '.' + @name
					else	
						select @oldname = ISNULL(@oldname,'') + ',' + @alias + '.' + @name
				end
			else
				begin
					if @counter = 1
					   select @oldname =  @name
					else	
						select @oldname = ISNULL(@oldname,'') + ',' + @name 
				end
			/* ler o pr�ximo campo	*/
			select @Counter = @Counter + 1
		END
	
	/* Retornar o valor final STRING concatenado */
	RETURN (@oldname)
end
GO
Grant Execute On dbo.up_InsertFieldsLine to Public
grant control on dbo.up_InsertFieldsLine to Public
go
