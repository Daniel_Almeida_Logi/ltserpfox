/****** 

	retorna classificacoes infarmed MSRM, MNSRM-EF, MNSR

	SELECT * FROM uf_getMedicamentosInfarmed()

 ******/




SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[uf_getMedicamentosInfarmed]') IS NOT NULL
	drop function dbo.uf_getMedicamentosInfarmed
go

CREATE function [dbo].uf_getMedicamentosInfarmed () returns  TABLE   as

RETURN  
    SELECT ref as id, nome FROM stfami(nolock) WHERE  ref in (1,2,58)

GO

grant control on dbo.uf_getMedicamentosInfarmed to Public
go