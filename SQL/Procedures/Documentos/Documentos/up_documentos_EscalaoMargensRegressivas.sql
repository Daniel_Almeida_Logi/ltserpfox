-- Painel de Conferência dos Encomendas/App de Compras --
-- exec up_documentos_EscalaoMargensRegressivas '8168617'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_documentos_EscalaoMargensRegressivas]') IS NOT NULL
	drop procedure dbo.up_documentos_EscalaoMargensRegressivas
go

create PROCEDURE [dbo].up_documentos_EscalaoMargensRegressivas
@ref varchar(18)

/* WITH ENCRYPTION */
AS


;With cte1(versao,escalao,PVAmin,PVAmax,PVPmin,PVPmax,mrgGross,mrgGrossTipo,mrgFarm,FeeFarm,feeFarmCiva,txCom,factorPond) as (
	Select	versao,escalao,PVAmin,PVAmax,PVPmin,PVPmax,mrgGross,mrgGrossTipo,mrgFarm,FeeFarm,feeFarmCiva,txCom,factorPond
	from	b_mrgRegressivas (nolock)
)


select	(Select top 1 escalao from cte1 Where pvporig between PVPmin and PVPmax order by versao desc) as escalao,
		pvporig as pvpciva,		
		pvporig - (pvporig*(taxa/100)) as pvpsiva,
		fprod.u_tabiva,
		taxasiva.taxa,
		(Select top 1 factorPond from cte1 Where pvporig between PVPmin and PVPmax order by versao desc) as factorPond,
		(Select top 1 feeFarmCiva from cte1 Where pvporig between PVPmin and PVPmax order by versao desc) as feeFarmCiva,
		(Select top 1 txCom from cte1 Where pvporig between PVPmin and PVPmax order by versao desc) as txCom,
		(Select top 1 mrgGross from cte1 Where pvporig between PVPmin and PVPmax order by versao desc) as mrgGross
from	fprod (nolock)
inner join fpreco (nolock) on fpreco.cnp=fprod.ref
inner join taxasiva (nolock) on taxasiva.codigo = fprod.u_tabiva
where	fpreco.grupo='pvp'
		and fprod.cnp = @ref

go
Grant Execute on dbo.up_documentos_EscalaoMargensRegressivas to Public
Grant Control on dbo.up_documentos_EscalaoMargensRegressivas to Public
go