-- Elimina Documentos 
-- exec up_documentos_EliminaDocumento 'ADM10080182913.485000003','FO'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_documentos_EliminaDocumento]') IS NOT NULL
	drop procedure dbo.up_documentos_EliminaDocumento
go
create procedure dbo.up_documentos_EliminaDocumento

@cabstamp as varchar(100),
@tipo as varchar(2)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

DECLARE @pais varchar = ''

SELECT @pais = textValue FROM B_Parameters_site(NOLOCK) WHERE stamp='ADM0000000050'

IF @tipo = 'BO'
BEGIN
	BEGIN TRANSACTION
		Delete From BO	Where BOSTAMP	=	@cabstamp
		Delete From BO2 Where BO2STAMP	=	@cabstamp
		
		Delete From BI	Where BOSTAMP	=	@cabstamp
		Delete From BI2 Where BOSTAMP	=	@cabstamp
		/*IF @pais = 'ANGOLA'
		BEGIN
			IF EXISTS (SELECT name FROM master.sys.databases WHERE name = N'mecosync')
			BEGIN
				IF (EXISTS (SELECT * FROM mecosync.INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'BO'))
				BEGIN
 					DELETE FROM mecosync.dbo.bo WHERE mecosync.dbo.bo.bostamp = @cabstamp
				END
				IF (EXISTS (SELECT * FROM mecosync.INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'BO2'))
				BEGIN
 					DELETE FROM mecosync.dbo.bo2 WHERE mecosync.dbo.bo2.bo2stamp = @cabstamp
				END
					IF (EXISTS (SELECT * FROM mecosync.INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Bi'))
				BEGIN
 					DELETE FROM mecosync.dbo.bi WHERE mecosync.dbo.bi.bostamp = @cabstamp
				END
					IF (EXISTS (SELECT * FROM mecosync.INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Bi2'))
				BEGIN
 					DELETE FROM mecosync.dbo.bi2 WHERE mecosync.dbo.bi2.bostamp = @cabstamp
				END
			 END
			END*/
	COMMIT
END

IF @tipo = 'FO'
BEGIN
	BEGIN TRANSACTION

		IF ISNULL((select fo.doccode from fo(nolock) where fostamp = @cabstamp), 0) = 112
		BEGIN

			UPDATE
				FC
			SET
				ecredf = 0,
				edebf = 0
			WHERE
				fc.fostamp in (select distinct fostamp from fn(nolock) where fnstamp in (select ffn.ofnstamp from fn(nolock) as ffn where ffn.fostamp = @cabstamp))

		END

		Delete From FO	Where FOSTAMP	=	@cabstamp
		Delete From FO2 Where FO2STAMP	=	@cabstamp
		Delete From FN_Honorarios Where fnstamp in (select fnstamp from fn Where FOSTAMP = @cabstamp)
		Delete From FN	Where FOSTAMP	=	@cabstamp
		
	COMMIT
END


GO
Grant Execute On dbo.up_documentos_EliminaDocumento to Public
Grant Control On dbo.up_documentos_EliminaDocumento to Public
GO