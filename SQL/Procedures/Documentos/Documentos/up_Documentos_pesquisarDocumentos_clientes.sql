/* SP Pesquisa Documentos

	exec up_Documentos_pesquisarDocumentos_clientes 300, '', '',0, -1,  '19000101',  '20201230','', 1,  'Administrador', '','A','ATLANTICO'
	exec up_Documentos_pesquisarDocumentos_clientes 1000, '', '', 0, -1, '20190101', '20191231', 'Devol. a Fornecedor', 0, '', '','A','Loja 1',-1
	exec up_Documentos_pesquisarDocumentos_clientes 30, '', '', 0, -1,  '20190601',  '20190630','N/Nt. Crédito,V/Factura Resumo', 1,  'Administrador', '','','Loja 1',-1
	exec up_Documentos_pesquisarDocumentos_clientes 30, '', '', 0, -1,  '20190701',  '20190730','N/Nt. Crédito,V/Factura Resumo', 1,  'Administrador', '','','Loja 1',-1
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_pesquisarDocumentos_clientes]') IS NOT NULL
	DROP PROCEDURE [dbo].[up_Documentos_pesquisarDocumentos_clientes] ;
GO

CREATE PROCEDURE [dbo].[up_Documentos_pesquisarDocumentos_clientes]
	 @topo			INT
	,@entidade		VARCHAR(55)
	,@numdoc		VARCHAR(20)
	,@no			NUMERIC(10)
	,@estab			NUMERIC(3)
	,@dataini		DATETIME
	,@datafim		DATETIME
	,@doc			VARCHAR(MAX)
	,@user			NUMERIC(5)
	,@group			VARCHAR(20)
	,@design		VARCHAR(60)
	,@fechada		VARCHAR(1)
	,@site			VARCHAR(60)
	,@exportado		INT = -1
	,@designProd	VARCHAR(60) = ''

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

declare @sql varchar(max)
select @sql = N'

	If OBJECT_ID(''tempdb.dbo.#temp_documentos'') IS NOT NULL
		drop table #temp_documentos ;
	

	DECLARE	
	@ref varchar(18) = LEFT('''+@design+''',17)

	-- Permissőes 
	SELECT *
	INTO #temp_Documentos
	FROM (
		SELECT
			 [tabela] = ''BO''
			,[documento] = [Ts].[nmdos]
		FROM
			[dbo].[Ts]
		where 
			bdempresas=''CL''
	) as X 
	WHERE
		(SELECT count(B_pf.pfstamp)
		 FROM [dbo].[B_pf](nolock)
	 		LEFT JOIN [dbo].[b_pfu] (nolock) ON [B_pfu].[pfstamp] = [B_pf].[pfstamp]
			LEFT JOIN [dbo].[b_pfg] (nolock) ON [B_pfg].[pfstamp] = [B_pf].[pfstamp]
		 WHERE B_pf.resumo = ([x].[documento] + '' - Visualizar'')
			AND (ISNULL([B_pfu].[userno],9999) = '+convert(varchar,@user)+' or ISNULL([B_pfg].[nome],'''') = '''+@group+''')
		) = 0

	--
	DECLARE @Documentos TABLE(
		doc varchar(24)
	)

	IF '''+@doc+''' <> ''''
		BEGIN
			INSERT INTO @Documentos
			SELECT * FROM dbo.up_splitToTable('''+@doc+''','','')
		END
	ELSE
		BEGIN
			INSERT INTO @Documentos
			SELECT [documento] FROM #temp_Documentos
		END

	--select * from #temp_Documentos

	-- Result Set Final
	SELECT TOP ('+CONVERT(varchar,@topo)+') *
	FROM 
	(
		SELECT	
			DISTINCT
			sel			= CONVERT(bit,0),
			[Tipodoc]	= ''BO'',
			[QttOT]		= SUM([Bi].[qtt]) OVER(PARTITION BY [Bi].[bostamp]),'
select @sql = @sql + N'
			[Escolha]	= 0,
			[Operador]	= [Bo].[vendnm],
			[CabStamp]	= [Bo].[bostamp],
			[Datav]		= [Bo].[dataobra],
			[Data]		= CONVERT(varchar,[Bo].[dataobra],102),
			u_dataentr,
			cor = case when CONVERT(VARCHAR(8),u_dataentr,112)>CONVERT(VARCHAR(8),getdate(),112)
							then ''G''
							else case when CONVERT(VARCHAR(8),GETDATE(),112)=CONVERT(VARCHAR(8),u_dataentr,112) and u_dataentr>getdate()
									then ''Y''
									else ''R'' end 
							end ,
			[Bo].[ousrdata], 
			[Bo].[ousrhora],
			[Bo].[ousrinis],
			[Documento]	= [Bo].[nmdos],
			[Numdoc]	= Convert(VARCHAR(100), [Bo].[obrano])+(case when bo2.codext<>'''' then '' | ''+bo2.codext else '''' end),
			[Entidade]	= UPPER([Bo].[nome]),
			[No]		= [Bo].[no],
			[Estab]		= [Bo].[estab],
			[Total]		= [etotal],
			bo.ncont,
			bo.etotaldeb,
			bo2.etotiva,
			[Site],
			bo.exportado,
			estado = CASE WHEN bo.fechada = 0 THEN ''A'' ELSE ''F'' END,
			enviada = Bo.LOGI1
			--,info = case when bo.nmdos= ''Encomenda de Cliente'' then ''Envio:'' + trim(modo_envio) + ''| Pagamento:'' + trim(pagamento) + '' | Status:'' + trim(status) + '' | Nr. Enc:'' + trim(codext) else '''' end
			--,info=trim(bo.codpost)+''-''+trim(bo.morada)
			,info=rtrim(ltrim(bo2.codpost))+''-''+rtrim(ltrim(bo2.morada))
			,modo_envio = case when modo_envio like ''%store%'' then ''Entrega Loja'' else ''Entrega Domicilio'' end
			,pagamento
			,status
			,codext
			,bo.morada
			,[anexos] = case when (select count(anexosstamp) from anexos (nolock) where tabela=''documentos'' and regstamp=bo.bostamp)=0 then 0 else 1 end
			,ISNULL((select top 1 stateDATE from ext_esb_orders_consult_states(nolock) where state = ''Previsão Inicial de Entrega'' and stampPedido = bo.bostamp order by ousrdata desc), '''') as dataEntrB2B
		FROM
       		[dbo].[BO] (nolock)
			LEFT JOIN [dbo].[Bo2]  (nolock) ON [Bo].[bostamp]=[Bo2].[bo2stamp]
       		INNER JOIN [dbo].[Bi]  (nolock) ON [Bo].[bostamp]=[Bi].[bostamp]'
select @sql = @sql + N'
			INNER JOIN [#temp_Documentos] on [Bo].[nmdos] = [#temp_Documentos].[documento] --AND tabela = ''BO''
			INNER JOIN @documentos ON [@documentos].[doc] = [Bo].[nmdos]
		WHERE
       		[Bo].[dataobra] BETWEEN '''+convert(varchar,@dataini)+''' AND '''+convert(varchar,@datafim)+''''
if @design != ''		
select @sql = @sql + N'
   			AND ([Bi].[design] LIKE '''+@design+''' +''%'' OR [Bi].[ref] LIKE @ref+''%'')'
if @designProd != ''		
select @sql = @sql + N'
			AND [Bi].[design] LIKE '''+@designProd+''' + ''%'''
if @entidade != ''		
select @sql = @sql + N'
			AND [Bo].[nome] LIKE '''+@entidade+''' + ''%'''
if @no != 0		
select @sql = @sql + N'
			AND [Bo].[no] = CASE WHEN '+convert(varchar,@no)+' = 0 THEN [Bo].[no] ELSE '+convert(varchar,@no)+' END'
if @no != -1		
select @sql = @sql + N'
			AND [Bo].[estab] = CASE WHEN '+convert(varchar,@estab)+' = -1 THEN [Bo].[estab] ELSE '+convert(varchar,@estab)+' END'
select @sql = @sql + N'
			AND [Bo].[fechada] = CASE WHEN '''+@fechada+''' = ''A'' THEN 0 WHEN '''+@fechada+''' = ''F'' then 1 ELSE [Bo].[fechada] END
			AND [Bo].[site] = CASE WHEN '''+@site+''' = '''' THEN [Bo].[site] ELSE '''+@site+''' END'
if @numdoc != ''		
select @sql = @sql + N'
			AND (convert(varchar,[Bo].[obrano]) = CASE WHEN '''+convert(varchar,@numdoc)+''' = '''' THEN convert(varchar,[Bo].[obrano]) ELSE '''+convert(varchar,@numdoc)+''' END 
				or
				[Bo2].[codext] = CASE WHEN '''+convert(varchar,@numdoc)+''' = '''' THEN convert(varchar,[Bo2].[codext]) ELSE '''+convert(varchar,@numdoc)+''' END )'
select @sql = @sql + N'
			and bo.exportado = case when '+convert(varchar,@exportado)+' = -1 then bo.exportado else '+convert(varchar,@exportado)+' end
	
	)x
	ORDER BY data DESC, ousrdata DESC,ousrhora DESC


	If OBJECT_ID(''tempdb.dbo.#temp_documentos'') IS NOT NULL
		drop table #temp_documentos ;'

print @sql
execute (@sql)


GO
GRANT EXECUTE on dbo.up_Documentos_pesquisarDocumentos_clientes TO PUBLIC
GRANT Control on dbo.up_Documentos_pesquisarDocumentos_clientes TO PUBLIC
GO