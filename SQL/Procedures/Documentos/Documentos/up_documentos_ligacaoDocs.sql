-- Importa Documentos - Dados Diversos Dados Documento Fornecedor
-- exec up_documentos_ligacaoDocs 'ADM10102244411.346595293'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_documentos_ligacaoDocs]') IS NOT NULL
	drop procedure dbo.up_documentos_ligacaoDocs
go

create procedure dbo.up_documentos_ligacaoDocs

@stamp as varchar(50)

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON	

/*	BI	*/
IF EXISTS ( select top 1 bistamp from bi (nolock) where obistamp = @stamp)	
	Begin
		select 'Dossiers Internos' as msg
		return 
	end	
else
	begin
/*	FN	*/	
		IF EXISTS (select top 1 ofnstamp from fn (nolock) where ofnstamp = @stamp)
			begin
				select 'Compras' as msg
				return
			end	
		else
/*	FI	*/
		begin
			IF EXISTS (select top 1 fistamp from fi (nolock) where bistamp = @stamp)
				begin
					select 'Facturação' as msg
					return
				end	
			else
/*	PL	*/
				begin
					IF EXISTS (select top 1 plstamp	from pl (nolock) where fcstamp  = @stamp)
						begin
							select 'Pagamentos' as msg
							return
						end	
					else
						begin
							select '' as msg
							return
						end	
				end	
		end	
	end	

GO
Grant Execute On dbo.up_documentos_ligacaoDocs to Public
Grant Control On dbo.up_documentos_ligacaoDocs to Public
GO