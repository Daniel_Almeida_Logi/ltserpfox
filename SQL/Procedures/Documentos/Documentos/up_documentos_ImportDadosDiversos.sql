-- Importa Documentos - Dados Diversos
-- exec up_documentos_ImportDadosDiversos 'ADM10102244411.346595293'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_documentos_ImportDadosDiversos]') IS NOT NULL
	drop procedure dbo.up_documentos_ImportDadosDiversos
go

create procedure dbo.up_documentos_ImportDadosDiversos

@cabstamp as varchar(100)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	
	
	Select    Top 1 convert(varchar(254),Bi.Obrano) as Num, Bi.Nmdos as Doc
	From    Fn (nolock)
	Inner Join Bi (nolock) ON fn.bistamp = bi.bistamp
	Inner Join fo2 (nolock) ON fo2.fo2stamp = fn.fostamp
	Where    Fn.fostamp = @cabstamp

	UNION ALL
	
	Select    Top 1 a.Adoc as Num,a.docnome as Doc
	From    Fn (nolock)
	Inner Join fn (nolock) a On a.fnstamp = fn.ofnstamp
	Inner Join fo2 (nolock) ON fo2.fo2stamp = fn.fostamp
	Where    Fn.fostamp = @cabstamp
	
	UNION ALL
	
	Select    Top 1 convert(varchar(254),Bi.Obrano) as Num,Bi.Nmdos as Doc
	From    Bi (nolock)
	Inner Join Bi2 (nolock) On bi2.bi2stamp = bi.bistamp
	Inner Join FN (nolock) ON fn.bistamp = bi2.fnstamp
	Where    Bi.Bostamp = @cabstamp
	
	UNION ALL
	
	Select    Top 1 convert(varchar(254),a.Obrano) as num, a.nmdos as Doc
	From    Bi (nolock)
	Inner Join Bi (nolock) a On a.bistamp = bi.obistamp
	Where    Bi.bostamp = @cabstamp 
	
GO
Grant Execute On dbo.up_documentos_ImportDadosDiversos to Public
Grant Control On dbo.up_documentos_ImportDadosDiversos to Public
GO