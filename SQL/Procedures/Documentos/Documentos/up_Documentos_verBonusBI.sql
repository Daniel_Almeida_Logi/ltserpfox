/* SP para verificar b�nus disponiveis no Painel de B�nus - cabe�alho BO

	 exec up_Documentos_verBonusBI 'RPFB431B66-A55D-445A-801'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_verBonusBI]') IS NOT NULL
	drop procedure dbo.up_Documentos_verBonusBI
go

create PROCEDURE [dbo].up_Documentos_verBonusBI
@stamp varchar(25)

/* WITH ENCRYPTION */
AS

select 
	distinct bi.ref, bi.design, bi.lobs as Bonus,
		bo.nome as Fornecedor, bo.no as NrFornecedor, 
		st.ststamp, 
		convert(varchar,bo.datafinal,102) as validade,
		pesquisa=convert(bit,0)
from 
	bi (nolock)
	inner join bo (nolock) on bi.bostamp = bo.bostamp
	inner join st (nolock) on bi.ref = st.ref
where 
	bo.ndos=35
	and bo.datafinal >= convert(date,getdate())
	and bi.ref in (select distinct ref from bi where bostamp=@stamp and ref != '')
order by validade

go
Grant Execute on dbo.up_Documentos_verBonusBI to Public
Grant Control on dbo.up_Documentos_verBonusBI to Public
go