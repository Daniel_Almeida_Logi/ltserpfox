/* 
	exec up_trfArm_valida_StockValidade 'ADMF5DCF14D-B1E2-41E1-A70','1' ,'2', 'Loja 1'
	exec up_trfArm_valida_StockValidade 'ADM0015F4BC-DBCB-4C9C-879','1' ,'3', 'Loja 1'
*/

IF OBJECT_ID('[dbo].[up_trfArm_valida_StockValidade]') IS NOT NULL
	DROP PROCEDURE dbo.up_trfArm_valida_StockValidade
GO

CREATE PROCEDURE [dbo].[up_trfArm_valida_StockValidade]

	@tokenTempFilter	AS VARCHAR(254),
	@armOrigem			AS VARCHAR(5),
	@armDestino			AS VARCHAR(5),
	@siteOri			AS VARCHAR(34)

AS
SET NOCOUNT ON
	If OBJECT_ID('tempdb.dbo.#filtroRef') IS NOT NULL
		drop table #filtroRef
	If OBJECT_ID('tempdb.dbo.#stockValidade') IS NOT NULL
		drop table #stockValidade

	SELECT 
		valueText,
		site
	INTO
		#filtroRef
	FROM 
		tempFilters(NOLOCK)
	WHERE
		token = @tokenTempFilter
	
	SELECT 
		ref				= st.ref,				
		armOri			= @armOrigem,
		stockOri		= ISNULL(st.stock,0),
		validadeOri		= CONVERT(DATE,st.validade),
		armDest			= @armDestino,
		stockDest		= 0,
		validadeDest	= CONVERT(DATE,'19000101'),
		resValidade		=  CONVERT(DATE,'19000101')
	INTO 
		#stockValidade
	FROM
		st(NOLOCK)
		INNER JOIN #filtroRef ON #filtroRef.valueText = st.ref
		INNER JOIN empresa(NOLOCK) ON empresa.site = #filtroRef.site and empresa.no = st.site_nr
		INNER JOIN empresa_arm(NOLOCK) ON empresa.no = empresa_arm.empresa_no
	WHERE
		empresa_arm.armazem = @armOrigem  
		AND empresa.site = @siteOri

	UPDATE  #stockValidade 
		SET 
			#stockValidade.stockDest	= ISNULL(st.stock,0),
			#stockValidade.validadeDest	= st.validade
		FROM
			st(NOLOCK)
			INNER JOIN #filtroRef ON #filtroRef.valueText = st.ref
			INNER JOIN #stockValidade ON #stockValidade.ref = st.ref
			INNER JOIN empresa(NOLOCK) ON  empresa.no = st.site_nr
			INNER JOIN empresa_arm(NOLOCK) ON empresa.no = empresa_arm.empresa_no
		WHERE
			empresa_arm.armazem = @armDestino
			AND #stockValidade.ref = st.ref
			AND empresa_arm.armazem = @armDestino

	UPDATE #stockValidade
		SET 
			resValidade = CASE 
				WHEN #stockValidade.stockDest <=0
					THEN validadeOri
					ELSE (CASE 
							WHEN #stockValidade.validadeOri >= #stockValidade.validadeDest
								THEN  validadeDest
							ELSE validadeOri
								END)
				END

		UPDATE ST
			SET 
				st.validade  = #stockValidade.resValidade
		FROM 
			st(NOLOCK)
			INNER JOIN #filtroRef ON #filtroRef.valueText = st.ref
			INNER JOIN #stockValidade ON #stockValidade.ref = st.ref
			INNER JOIN empresa(NOLOCK) ON  empresa.no = st.site_nr
			INNER JOIN empresa_arm(NOLOCK) ON empresa.no = empresa_arm.empresa_no
		WHERE 
			st.ref = #stockValidade.ref
			AND empresa_arm.armazem = @armDestino

	If OBJECT_ID('tempdb.dbo.#filtroRef') IS NOT NULL
		drop table #filtroRef
	If OBJECT_ID('tempdb.dbo.#stockValidade') IS NOT NULL
		drop table #stockValidade
GO
Grant Execute on dbo.up_trfArm_valida_StockValidade to Public
Grant control on dbo.up_trfArm_valida_StockValidade to Public
GO