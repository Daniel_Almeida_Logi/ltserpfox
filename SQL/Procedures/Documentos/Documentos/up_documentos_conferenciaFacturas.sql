-- Painel de Confer�ncia dos Litigios --
-- exec up_documentos_conferenciaFacturas '20100101', '20111201', '', '', 0
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_documentos_conferenciaFacturas]') IS NOT NULL
	drop procedure dbo.up_documentos_conferenciaFacturas
go

create PROCEDURE [dbo].up_documentos_conferenciaFacturas

	@dataIni datetime,
	@datafim datetime,
	@nrDoc varchar(49),
	@ref varchar(40),
	@acesso bit,
	@site_nr tinyint

/* WITH ENCRYPTION */
AS

select 
	docdata=convert(varchar,fo.docdata,102), 
	dochora=left(fo.ousrhora,5),
	fo.adoc,
	fo.doccode,
	fn.ref, 
	fn.codigo, 
	fn.oref, 
	fn.design, 
	fn.cpoc, 
	fn.iva, 
	fn.fnstamp,
	fo.fostamp,
	fn.ofnstamp, 
	fn.bistamp, 
	fo.nome, 
	fo.no, 
	fo.estab,
	fo.morada,
	fo.local, 
	fo.codpost,
	fo.ncont, 
	fo.tipo, 
	fo.moeda, 
	fo.pais,
	fo.ccusto,
	fn.tabiva,
	fn.ivaincl,
	fn.stns, 
	fn.unidade,
	fn.uni2qtt, 
	fn.armazem,
	fn.usr1,
	fn.usr2,
	fn.usr3,
	fn.usr4,
	fn.usr5,			
	fn.usr6,
	fn.u_stockact,
	fn.u_epv1act,
	'conversao' = 
		case 
			when isnull(B_fp.conversao,0)!=0 then B_fp.conversao
			else ISNULL(st.conversao,1)
		end,
	'odata' =
		ISNULL(
			case 
				when fn.bistamp!='' then convert(varchar,bo2.dataobra,102)
				when fn.ofnstamp!='' then convert(varchar,fo2.docdata,102)
				else '' 
			end,
		''),	
	'ohora'	 = 
		ISNULL(
			case 
				when fn.bistamp!='' then left(bo2.ousrhora,5)
				when fn.ofnstamp!='' then left(fo2.ousrhora,5)
				else ''
			end,
		''),
	'oDocNome' =
		ISNULL(
			case
				when fn.bistamp!='' then bo2.nmdos
				when fn.ofnstamp!='' then fo2.docnome
				else ''
			end,
		''),
	
	'psico'		=	isnull(fprod.psico,convert(bit,0)),
	'benzo'		=	isnull(fprod.benzo,convert(bit,0)),
	fn.u_psicont, 
	fn.u_bencont,
	/* valores Encomenda/Guia (retirados da fn ou da bi) */
	qtenc			= fn.u_qttenc, 
	upcenc		= fn.u_upce, 
	totalenc		= fn.u_qttenc*fn.u_upce,
	epvenc		= case when fn.ref!='' and fn.ofnstamp='' and fn.bistamp='' then 0 else isnull(isnull(fn2.epv, bi.edebito),0) end,
	bonusenc	= case when fn.ref!='' and fn.ofnstamp='' and fn.bistamp='' then 0 else isnull(isnull(fn2.u_bonus, bi.u_bonus),0) end,
	desc1enc	= case when fn.ref!='' and fn.ofnstamp='' and fn.bistamp='' then 0 else isnull(isnull(fn2.desconto, bi.desconto),0) end,
	desc2enc	= case when fn.ref!='' and fn.ofnstamp='' and fn.bistamp='' then 0 else isnull(isnull(fn2.desc2, bi.desc2),0) end,
	desc3enc	= case when fn.ref!='' and fn.ofnstamp='' and fn.bistamp='' then 0 else isnull(isnull(fn2.desc3, bi.desc3),0) end,
	desc4enc	= case when fn.ref!='' and fn.ofnstamp='' and fn.bistamp='' then 0 else isnull(isnull(fn2.desc4, bi.desc4),0) end,
	/*  valores factura */
	qtfac			= fn.qtt, 
	upcfac		= fn.u_upc,
	totalfac		= fn.etiliquido,
	epvfac		= fn.epv,
	bonusfac	= fn.u_bonus,
	desc1fac	= fn.desconto, 
	desc2fac	= fn.desc2,
	desc3fac	= fn.desc3,
	desc4fac	= fn.desc4,
	/* valores finais */
	qtf				= isnull(cfs.qtf,0), 
	upcf			= isnull(cfs.upcf,convert(money,0)), 
	bonusf		= isnull(cfs.bonusf,0),
	desc1f		= isnull(cfs.desc1f,convert(money,0)), 
	desc2f		= isnull(cfs.desc2f,convert(money,0)), 
	desc3f		= isnull(cfs.desc3f,convert(money,0)),
	desc4f		= isnull(cfs.desc4f,convert(money,0)), 
	totalf			= isnull(cfs.totalf,convert(money,0)),
	marcadaf	= fn.marcada, 
	fmarcada	= isnull(cfs.pf,fn.fmarcada)

from
	fn								(nolock)
	
	inner join fo					(nolock) on fo.fostamp=fn.fostamp
	left join fn fn2				(nolock) on fn.ofnstamp=fn2.fnstamp
	left join fo fo2				(nolock) on fn2.fostamp=fo2.fostamp

	left join bi					(nolock) on fn.bistamp=bi.bistamp
	left join bi bi2				(nolock) on fn.bistamp=bi2.bistamp
	left join bo bo2				(nolock) on bi2.bostamp=bo2.bostamp
	
	left join B_confFacSave cfs		(nolock) on cfs.fnstamp=fn.fnstamp
	
	left join fprod					(nolock) on fn.ref=fprod.cnp
	left join st					(nolock) on (st.ref=fn.ref or st.ref=fn.oref) and st.site_nr = @site_nr
	left join b_fp					(nolock) on (b_fp.ref=fn.ref or b_fp.ref=fn.oref) and b_fp.no=fo.no and b_fp.estab=fo.estab and b_fp.status='Y'
	
where 
	(fn.u_qttenc!=fn.qtt or fn.u_upc!=fn.u_upce) /* and (fn.u_qttenc!=0 or fn.u_upce!=0) */
	and (fo.docdata between @dataIni and @datafim)
	and (
		(fn.ref = case when @ref='' then fn.ref else @ref end) OR (fn.oref	= case when @ref='' then fn.ref else @ref end)
		)
	and fo.adoc	= case when @nrDoc='' then fo.adoc else @nrDoc end
	and fo.doccode = 55
	and fn.marcada = 0
	and fn.fnstamp = case when @acesso=0 then fn.fnstamp else cfs.fnstamp end
	/* n�o incluir documentos sem origem (facturas isoladas) */
	and (
		(select Sum(case when ofnstamp='' then 0 else 1 end) from fn fnx (nolock) where fnx.fostamp=fo.fostamp)>0
		OR
		(select Sum(case when bistamp='' then 0 else 1 end) from fn fny (nolock) where fny.fostamp=fo.fostamp)>0
		)
	
order by
	fo.docdata, fo.adoc

go
Grant Execute on dbo.up_documentos_conferenciaFacturas to Public
Grant Control on dbo.up_documentos_conferenciaFacturas to Public
go