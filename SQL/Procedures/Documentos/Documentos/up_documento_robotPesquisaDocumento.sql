/*
    retorna informação da v. factura / v. factura med

	campo 1 - numero do documento
	campo 2 - id da base da loja

	exec up_documento_robotPesquisaDocumento  'LS990228-12322222222', 'ltdev30'


	exec up_documento_robotPesquisaDocumento  'LS990228-02322222222', 'ltdev30'


*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_documento_robotPesquisaDocumento]') IS NOT NULL
	drop procedure dbo.up_documento_robotPesquisaDocumento
go

create procedure dbo.up_documento_robotPesquisaDocumento

@nDoc varchar(100),
@id_lt varchar(100)

/* WITH ENCRYPTION */
AS

	SET NOCOUNT ON

	declare @no int

	select top 1 @no = no from empresa(nolock) where id_lt = @id_lt


	select
		top 1
		"number" = isnull(adoc,''),
		"productCode" = isnull(ref,''),
		"expiryDateV" = isnull(CONVERT(VARCHAR(8), u_validade, 112), '19000101'),
		"expiryDateDtV" = isnull(CONVERT(VARCHAR(8), u_dtval, 112), '19000101'),
		"quantity" = qtt,
		"docName" = docnome
	from     
		fn (nolock) 
	where
		(docnome = 'V/Factura' or docnome = 'V/Factura Med.')
		and adoc = @nDoc
	order by usrdata desc

	

GO
Grant Execute On dbo.up_documento_robotPesquisaDocumento to Public
Grant Control On dbo.up_documento_robotPesquisaDocumento to Public
GO





