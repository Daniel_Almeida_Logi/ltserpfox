/* Painel de B�nus - cabe�alho FO

	exec up_Documentos_verBonusFN ''

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_verBonusFN]') IS NOT NULL
	drop procedure dbo.up_Documentos_verBonusFN
go

create PROCEDURE [dbo].up_Documentos_verBonusFN
@stamp varchar(25)

/* WITH ENCRYPTION */
AS

select 
	distinct bi.ref, bi.design, bi.lobs as Bonus,
		bo.nome as Fornecedor, bo.no as NrFornecedor, 
		st.ststamp, 
		convert(varchar,bo.datafinal,102) as validade,
		pesquisa=convert(bit,0)
from 
	bi (nolock)
	inner join bo (nolock) on bi.bostamp=bo.bostamp
	inner join st (nolock) on bi.ref=st.ref
where 
	bo.ndos = 35
	and bo.datafinal >= convert(date,getdate())
	and bi.ref in (select distinct ref from fn where fostamp=@stamp and ref != '')
order by validade

go
Grant Execute on dbo.up_Documentos_verBonusFN to Public
Grant Control on dbo.up_Documentos_verBonusFN to Public
go