/* Painel de Importação de Documentos

	 exec up_documentos_ImportActualizaLinhas 0,0,'ssa47725819-2932-44EE-B99'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_documentos_ImportActualizaLinhas]') IS NOT NULL
	drop procedure dbo.up_documentos_ImportActualizaLinhas
go

create procedure dbo.up_documentos_ImportActualizaLinhas

@quantidade		int,
@quantidadeMov	int,
@cabstamp as varchar(MAX)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

DECLARE @ul_tipo AS TABLE (cab VARCHAR(25))


INSERT INTO 
	@ul_tipo (cab)
SELECT 
	y.value
FROM 
	dbo.SplitString(@cabStamp, ';') x
	cross apply dbo.SplitString(x.value, ';') y

SELECT * FROM
(
	SELECT
		stamp		= fn.FNSTAMP
		,COPIAR		= 0
		,fn.REF
		,fn.DESIGN
		,fn.EPV
		,fn.QTT
		,fn.ETILIQUIDO
		,qttmov		= fn.pbruto
		,cabStamp = fo.fostamp
		,docno = fo.adoc
		,tipo = 'FO'
		,fo.nome
		,fo.no
		,fo.estab
		,fo.doccode as docNum
		,fn.u_upc
		,fo.docnome as nmdoc
	FROM
		FN (nolock)
		JOIN fo(nolock) on fo.fostamp = fn.fostamp
	WHERE
		fo.fostamp in (SELECT cab FROM @ul_tipo)
		and fn.qtt > case when @quantidade = 1 then 0 else  fn.qtt-1 end
		and fn.pbruto < case when @quantidadeMov = 1 then fn.qtt else fn.pbruto + 1 end
	
	UNION ALL

	SELECT
		stamp		= bi.bistamp
		,COPIAR		= 0
		,bi.REF
		,bi.DESIGN
		,epv		= bi.EDEBITO
		,bi.QTT
		,etiliquido	= bi.ETTDEB
		,qttmov		= bi.pbruto
		,cabStamp = bi.bostamp
		,docno = LTRIM(RTRIM(bo.obrano))
		,tipo = 'BO'
		,bo.nome
		,bo.no
		,bo.estab
		,bo.ndos as docNum
		,bi.u_upc
		,bo.nmdos as nmdoc
	FROM
		BI (nolock)
		JOIN bo(nolock) ON bi.bostamp = bo.bostamp
	WHERE
		bo.bostamp in (SELECT cab FROM @ul_tipo)
		and bi.qtt > case when @quantidade = 1 then 0 else  bi.qtt-1 end
		and bi.qtt2 < case when @quantidadeMov = 1 then bi.qtt else  bi.qtt2 + 1 end
		-- and bi.pbruto < case when @quantidadeMov = 1 then bi.qtt else  bi.pbruto + 1 end


	UNION ALL

	SELECT
		stamp		= fi.fistamp
		,COPIAR		= 0
		,fi.REF
		,fi.DESIGN
		,epv		= fi.epv
		,fi.QTT
		,etiliquido	= fi.etiliquido
		,qttmov		= fi.pbruto
		,cabStamp = fi.ftstamp
		,docno = LTRIM(RTRIM(ft.fno))
		,tipo = 'FT'
		,ft.nome
		,ft.no
		,ft.estab
		,ft.ndoc as docNum
		,0 as u_upc
		,ft.nmdoc
	FROM
		FI (nolock)
		JOIN ft(nolock) ON fi.ftstamp = ft.ftstamp
	WHERE
		ft.ftstamp in (SELECT cab FROM @ul_tipo)
		and fi.qtt > case when @quantidade = 1 then 0 else  fi.qtt-1 end
		and fi.pbruto < case when @quantidadeMov = 1 then fi.qtt else  fi.pbruto + 1 end

) as x
ORDER BY cabStamp

GO
Grant Execute On dbo.up_documentos_ImportActualizaLinhas to Public
Grant Control On dbo.up_documentos_ImportActualizaLinhas to Public
GO
