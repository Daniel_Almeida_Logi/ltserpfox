/* Cria Crusor BI 

	exec up_Documentos_linhas 'FO','ADM03D2FDDB-2C6A-4E94-B59', 0 

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_linhas]') IS NOT NULL
	drop procedure dbo.up_Documentos_linhas
go

create procedure dbo.up_Documentos_linhas
	@tipo varchar(2)
	,@stamp varchar(25)
	,@site_nr tinyint
	,@definition bit = 0 

/* WITH ENCRYPTION */
AS

	IF @tipo = 'FO'
	BEGIN
	
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFprodFO'))
		DROP TABLE #dadosFprodFO

		--
		Select 
			cnp
			,psico
			,benzo
			,pvporig
		into 
			#dadosFprodFO
		From 
			fprod (nolock)
		Where
			fprod.cnp in (select fn.ref from fn(nolock) where fn.fostamp = @stamp)
			
		--
		SELECT 
			pesquisa = convert(bit,0),
			usalote = isnull(st.usalote,convert(bit,0))
			,psico = ISNULL(fprod.psico,convert(bit,0))
			,benzo = ISNULL(fprod.benzo ,convert(bit,0))
			,fn.* 
			,data_h = isnull(convert(varchar,fn_honorarios.data,112),'')
			,servmrstamp = isnull(servmrstamp,'')
			,EntidadeNome = isnull(EntidadeNome,'')
			,EntidadeNo = isnull(EntidadeNo,0)
			,Utente = isnull(Utente,'')
			,UtenteNo = isnull(UtenteNo,0)
			,pvp= isnull(pvp,0.00)
			,ValorUtente= isnull(ValorUtente,0.00)
			,ValorEntidade= isnull(ValorEntidade,0.00)
			,despesa = isnull(despesa,0.00)
			,honorario= isnull(honorario,0.00)
			,HonorarioDeducao= isnull(HonorarioDeducao,0.00)
			,HonorarioValor= isnull(HonorarioValor,0.00)
			,HonorarioBi= isnull(HonorarioBi,'')
			,HonorarioTipo= isnull(HonorarioTipo,'')
			,userno= isnull(userno,0)
			,id_cpt_val_cli= isnull(id_cpt_val_cli,'')
			,convencao = isnull(convencao,'')
			,updated = CONVERT(bit,0)
			,pvpval = LTRIM(RTRIM(CASE WHEN fn.docnome='V/Factura Med.' THEN dbo.uf_defineCorPvp(fn.ref, fn.u_pvp) ELSE '255,255,255' END))
			,stkatualloja=st.stock
			,descant=fn.desconto
			,totalant=fn.etiliquido
			,alttotal=convert(bit,0)
			,pctant = fn.epv
			,pvporig	= (case when (convert(numeric(13,3), isnull(fprod.pvporig, 0))) = 0 then 0 else st.epv1 end)
			,desp_lin_orig = fn.val_desp_lin
			,dt_val_orig = fn.u_validade
			,qttembal=case when st.qttembal<>0 then round(fn.qtt/st.qttembal,2) else fn.qtt end
		FROM 
			FN (nolock) 
			left join st (nolock) on fn.ref = st.ref and st.site_nr = @site_nr
			left join #dadosFprodFO fprod (nolock) on st.ref = fprod.cnp
			left join fn_honorarios (nolock) on fn.fnstamp = fn_honorarios.fnstamp 
		WHERE 
			fn.fostamp = @stamp
		ORDER BY 
			lordem
			
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFprod'))
		DROP TABLE #dadosFprod
	END
	IF @tipo = 'BO'
	BEGIN
	
	
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFprodBO'))
		DROP TABLE #dadosFprodBO
		Select 
			cnp
			,psico
			,benzo
			,pvporig
		into 
			#dadosFprodBO
		From 
			fprod (nolock)
		Where
			fprod.cnp in (select bi.ref from bi(nolock) where bi.bostamp = @stamp)
	
		SELECT 
			pesquisa = convert(bit,0)
			,usalote = isnull(st.usalote,convert(bit,0))
			,psico = ISNULL(fprod.psico,convert(bit,0))
			,benzo = ISNULL(fprod.benzo ,convert(bit,0))
			,lab = convert(varchar(120),st.u_lab)
			,validade = convert(datetime,'19000101')
			,bi.*
			,updated = CONVERT(bit,0)
			,pvpval='255,255,255'
			,stkatualloja=st.stock
			,descant=bi.desconto
			,totalant=bi.ettdeb
			,alttotal=convert(bit,0)
			,pctant = 0.00 
			,pvporig	= (case when (convert(numeric(13,3), isnull(fprod.pvporig, 0))) = 0 then 0 else st.epv1 end)
			,ViaVerde = 0 --ISNULL(case when emb_via_verde.ref IS NULL then convert(bit,0) else convert(bit,1) end, convert(bit,0))
			,desp_lin_orig = 0
			,qttembal=case when st.qttembal<>0 then round(bi.qtt/st.qttembal,2) else bi.qtt end
			,isnull((select nrreceita from bi2 (nolock) where bi2.bi2stamp=bi.bistamp),'') as nrreceita
		FROM 
			BI (nolock) 
			left join st (nolock) on bi.ref = st.ref and st.site_nr = @site_nr
			left join #dadosFprodBO fprod (nolock) on st.ref = fprod.cnp
			--left join emb_via_verde (nolock) on rtrim(ltrim(st.ref)) = rtrim(ltrim(emb_via_verde.ref))
		WHERE 
			bi.bostamp = @stamp
		ORDER BY 
			bi.lordem

		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFprod'))
		DROP TABLE #dadosFprod
	END

GO
Grant Execute On dbo.up_Documentos_linhas to Public
Grant Control On dbo.up_Documentos_linhas to Public
Go 
