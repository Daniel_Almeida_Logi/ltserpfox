/* SP Pesquisa Documentos

	exec up_Documentos_pesquisarDocumentos_export '20211201',  '20220130','Loja 1'
	
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_pesquisarDocumentos_export]') IS NOT NULL
	DROP PROCEDURE [dbo].[up_Documentos_pesquisarDocumentos_export] ;
GO

CREATE PROCEDURE [dbo].[up_Documentos_pesquisarDocumentos_export]
	@dataini		DATETIME
	,@datafim		DATETIME
	,@site			VARCHAR(60)
	

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

declare @sql varchar(max)
		,@sql1 varchar(max)
select @sql = N'

	-- Result Set Final
	select 
		id,	data, hora,	documento, numdoc, ncont, nome, no, estab, qttTot, biiva, valiva, total, op, contab, edescc, moeda,	pais, dataven, biiva0, biiva6, valiva6, biiva13, valiva13, biiva23, valiva23	
	from
		(select 
			ordem		= 1,
			id			= ''ID'',
			data		= ''DATA'',
			hora		= ''HORA'',
			documento	= ''DOCUMENTO'',
			numdoc		= ''NDOC'',
			ncont		= ''NIF'',
			nome		= ''NOME'',
			no			= ''NO'',
			estab		= ''ESTAB'',
			qttTot		= ''QTOT'',
			biiva		= ''BI'',
			valiva		= ''IVA'',
			total		= ''TOTAL'',
			op			= ''OP'',
			contab		= ''CONTAB'',
			edescc		= ''DESCOM'',
			moeda		= ''MOEDA'',
			pais		= ''PAIS'',
			dataven		= ''DVENCIM'',
			biiva0		= ''TIVAISE '', 
			biiva6		= ''TIVARED'',
			valiva6		= ''IVARED'',
			biiva13		= ''TIVAINT'',
			valiva13	= ''IVAINT'',
			biiva23		= ''TIVANOR'',
			valiva23	= ''IVANOR''
		union all '
select @sql1 = N'
		--SELECT  distinct ordem=2,str(ROW_NUMBER() OVER(ORDER BY [data] DESC,hora DESC ),10,0) AS id,*
		SELECT  *
		FROM 
		(
			SELECT 	DISTINCT
				ordem		= 2,
				id			= fo.fostamp,
				data		= CONVERT(varchar,Fo.docdata,102),
				hora		= Fo.ousrhora,
				documento	= Fo.docnome,
				numdoc		= Ltrim(Rtrim(Fo.adoc)),
				ncont		= fo.ncont,
				nome		= UPPER(Fo.nome),
				no			= str(Fo.no,15,0),
				estab		= str(Fo.estab,15,0),
				qttTot		= str(cast(SUM(Fn.qtt) OVER(PARTITION BY Fo.fostamp) as numeric(10,0)),10,0),
				biiva		= str(cast(case when fo.docnome=''V/Nt. Crédito'' then fo.eivain*(-1) else fo.eivain end as numeric(15,2)),15,2),
				valiva		= str(cast(case when fo.docnome=''V/Nt. Crédito'' then fo.ettiva*(-1) else fo.ettiva end as numeric(15,2)),15,2),
				total		= str(cast(etotal as numeric(15,2)),15,2),
				op			= Fo.ousrinis,
				contab		= case when fo.exportado=1 then ''S'' else ''N'' end,
				edescc		= str(cast(Fo.edescc as numeric(15,2)),15,2),
				moeda		= fo.moeda,
				pais		= case when fl.pais=1 then ''NACIONAL'' else (case when fl.pais=2 then ''UE'' else ''OUTROS'' end) end,
				dataven		= CONVERT(varchar,Fo.pdata,102),
				biiva0		= str(cast(isnull(round(SUM(case when fn.tabiva=4 then (case when fn.ivaincl=0 then [Fn].[etiliquido] else (FN.ETILIQUIDO/(FN.iva/100+1)) end) else 0 end) OVER(PARTITION BY [Fo].[fostamp]),2),0) as numeric(15,2)),15,2), 
				biiva6		= str(cast(isnull(round(SUM(case when fn.tabiva=1 then (case when fn.ivaincl=0 then [Fn].[etiliquido] else (FN.ETILIQUIDO/(FN.iva/100+1)) end) else 0 end) OVER(PARTITION BY [Fo].[fostamp]),2),0) as numeric(15,2)),15,2) ,
				valiva6		= str(cast(isnull(round(SUM(case when fn.tabiva=1 then (case when fn.ivaincl=0 then (FN.ETILIQUIDO*(FN.iva/100)) else fn.etiliquido - (FN.ETILIQUIDO/(FN.iva/100+1)) end) end) OVER(PARTITION BY [Fo].[fostamp]),2),0) as numeric(15,2)),15,2),
				biiva13		= str(cast(isnull(round(SUM(case when fn.tabiva=3 then (case when fn.ivaincl=0 then [Fn].[etiliquido] else (FN.ETILIQUIDO/(FN.iva/100+1)) end) else 0 end) OVER(PARTITION BY [Fo].[fostamp]),2),0) as numeric(15,2)),15,2) ,
				valiva13	= str(cast(isnull(round(SUM(case when fn.tabiva=3 then (case when fn.ivaincl=0 then (FN.ETILIQUIDO*(FN.iva/100)) else fn.etiliquido - (FN.ETILIQUIDO/(FN.iva/100+1)) end) end) OVER(PARTITION BY [Fo].[fostamp]),2),0) as numeric(15,2)),15,2),
				biiva23		= str(cast(isnull(round(SUM(case when fn.tabiva=2 then (case when fn.ivaincl=0 then [Fn].[etiliquido] else (FN.ETILIQUIDO/(FN.iva/100+1)) end) else 0 end) OVER(PARTITION BY [Fo].[fostamp]),2),0) as numeric(15,2)),15,2) ,
				valiva23	= str(cast(isnull(round(SUM(case when fn.tabiva=2 then (case when fn.ivaincl=0 then (FN.ETILIQUIDO*(FN.iva/100)) else fn.etiliquido - (FN.ETILIQUIDO/(FN.iva/100+1)) end) end) OVER(PARTITION BY [Fo].[fostamp]),2),0) as numeric(15,2)),15,2)
			FROM
				Fo (nolock)
				INNER JOIN Fn (nolock) ON Fo.fostamp = Fn.fostamp
				inner join fl (nolock) on fl.no=fo.no and fl.estab=fo.estab
			WHERE
				(Fo.usrdata BETWEEN '''+convert(varchar,@dataini)+''' AND '''+convert(varchar,@datafim)+''')
				AND Fo.site = CASE WHEN '''+@site+''' = '''' THEN Fo.site ELSE '''+@site+''' END
				and fo.docnome in (''V/Factura'', ''V/Factura Med.'',''V/Nt. Crédito'', ''V/Nt. Débito'', ''N/Nt. Débito'')	
		)x
		)y
		order by ordem, id,data DESC,hora DESC'

print @sql+@sql1
execute (@sql+@sql1)
--	exec up_Documentos_pesquisarDocumentos_export '20220101',  '20220130','Loja 1'

GO
GRANT EXECUTE on dbo.up_Documentos_pesquisarDocumentos_export TO PUBLIC
GRANT Control on dbo.up_Documentos_pesquisarDocumentos_export TO PUBLIC
GO