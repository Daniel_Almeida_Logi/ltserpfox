-- Condi��o De Pagamento - Lista Documentos
-- exec up_Documentos_CondPag 201,0,'','20101223'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_CondPag]') IS NOT NULL
	drop procedure dbo.up_Documentos_CondPag
go
create procedure dbo.up_Documentos_CondPag

@no as numeric(9,0),
@estab as numeric(2,0),
@condpag as varchar(150),
@data as  datetime

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

SELECT	
	VENCIMENTO
	,CASE WHEN VENCIMENTO = 1 THEN ISNULL((SELECT VENCIMENTO FROM FL (nolock) WHERE NO = @no AND estab = @estab),0) ELSE DIAS END as DIAS
	,DATEADD(DAY,CASE WHEN VENCIMENTO = 1 THEN ISNULL((SELECT VENCIMENTO FROM FL (nolock) WHERE NO = @no AND estab = @estab),0) ELSE DIAS END,@data) as DATAVENCIMENTO
FROM	
	TP (nolock)
WHERE	
	DESCRICAO = @condpag


GO
Grant Execute On dbo.up_Documentos_CondPag to Public
Grant Control On dbo.up_Documentos_CondPag to Public
GO