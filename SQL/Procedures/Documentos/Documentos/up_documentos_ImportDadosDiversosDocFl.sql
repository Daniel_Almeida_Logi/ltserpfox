-- Importa Documentos - Dados Diversos Dados Documento Fornecedor
-- exec up_documentos_ImportDadosDiversosDocFl 'ADM10102244411.346595293'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_documentos_ImportDadosDiversosDocFl]') IS NOT NULL
	drop procedure dbo.up_documentos_ImportDadosDiversosDocFl
go
create procedure dbo.up_documentos_ImportDadosDiversosDocFl

@cabstamp as varchar(100)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	
	Select	fo2.u_docfl, fo2.u_docflno
	From	Fo2 (nolock)
	Where	Fo2.fo2stamp = @cabstamp 
	
GO
Grant Execute On dbo.up_documentos_ImportDadosDiversosDocFl to Public
Grant Control On dbo.up_documentos_ImportDadosDiversosDocFl to Public
GO