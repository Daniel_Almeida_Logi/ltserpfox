/*

	

*/

IF OBJECT_ID('[dbo].[up_documentos_syncEncSiteCentral]') IS NOT NULL
	DROP procedure dbo.up_documentos_syncEncSiteCentral
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure dbo.up_documentos_syncEncSiteCentral
	@token VARCHAR(30),
	@bdCentral	VARCHAR(50),
	@user VARCHAR(30)

AS
	DECLARE @stamps VARCHAR(MAX) = ''
	DECLARE @estadosLocal TABLE (bostamp varchar(30), modo_envio VARCHAR(50), pagamento VARCHAR(50), status VARCHAR(50), usrdata DATE, usrhora VARCHAR(8))
	DECLARE @estadosCentral TABLE (bostamp varchar(30), modo_envio VARCHAR(50), pagamento VARCHAR(50), status VARCHAR(50), usrdata DATE, usrhora VARCHAR(8))

	DECLARE @bostampLocal varchar(30), @modo_envioLocal VARCHAR(50), @pagamentoLocal VARCHAR(50), @statusLocal VARCHAR(50), @usrdataLocal DATE, @usrhoraLocal VARCHAR(8)
	DECLARE @bostampCentral varchar(30), @modo_envioCentral VARCHAR(50), @pagamentoCentral VARCHAR(50), @statusCentral VARCHAR(50), @usrdataCentral DATE, @usrhoraCentral VARCHAR(8)

	DECLARE @sql VARCHAR(MAX)
	DECLARE @updateCentral VARCHAR(MAX) = ''
	DECLARE @updateLocal VARCHAR(MAX) = ''
	DECLARE @cabUpdate VARCHAR(MAX) = ''

	DECLARE @count NUMERIC(2)

	SELECT @stamps = COALESCE( @stamps + (CASE WHEN @stamps = '' THEN '' ELSE ', ' END), '') + '''' + valueText + ''''
	FROM tempFilters(nolock)
	WHERE tempfilters.token = @token

	INSERT INTO @estadosLocal
	select
		bo.bostamp,
		bo2.modo_envio,
		bo2.pagamento,
		bo2.status,
		bo.usrdata,
		bo.usrhora
	from
		bo2(nolock) 
		inner join bo(nolock) on bo2.bo2stamp = bo.bostamp
		join tempFilters(nolock) on bo2.bo2stamp = tempFilters.valueText
	where 
		tempfilters.token = @token

	SET @sql = N'

		select 
			bo.bostamp,
			bo2.modo_envio,
			bo2.pagamento,
			bo2.status,
			bo.usrdata,
			bo.usrhora
		from
			' + @bdCentral + '.dbo.bo2 WITH (nolock) 
			join ' + @bdCentral + '.dbo.bo WITH (nolock) on bo2.bo2stamp = bo.bostamp
		where 
			bo2.bo2stamp in (' + @stamps + ')
	'

	--print @sql

	INSERT INTO @estadosCentral
	EXEC (@sql)

	DECLARE uc_status CURSOR FOR
	SELECT 
		*
	FROM
		@estadosLocal as localBD
		inner join @estadosCentral as centralBD ON localBD.bostamp = centralBD.bostamp
	WHERE
		localBD.status <> centralBD.status
		OR localBD.modo_envio <> centralBD.modo_envio
		OR localBD.pagamento <> centralBD.pagamento

	OPEN uc_status

	FETCH NEXT FROM uc_status INTO @bostampLocal, @modo_envioLocal, @pagamentoLocal, @statusLocal, @usrdataLocal, @usrhoraLocal,
	@bostampCentral, @modo_envioCentral, @pagamentoCentral, @statusCentral, @usrdataCentral, @usrhoraCentral

	WHILE @@FETCH_STATUS = 0
	BEGIN

		SET @cabUpdate = 'DECLARE @tabUpdate AS TABLE (bostamp VARCHAR(30), status VARCHAR(50), modo_envio VARCHAR(50), pagamento VARCHAR(50), usrdata DATE, usrhora VARCHAR(8), usrinis VARCHAR(30))
		INSERT INTO @tabUpdate VALUES'


		IF @usrdataLocal > @usrdataCentral OR (@usrdataLocal = @usrdataCentral AND @usrhoraLocal > @usrhoraCentral)
		BEGIN

			SET @updateCentral = @updateCentral + (CASE WHEN @updateCentral = '' THEN @cabUpdate ELSE ',' END) 
			+ '(''' + @bostampCentral + ''', ''' + @statusLocal + ''', ''' + @modo_envioLocal + ''', ''' + @pagamentoLocal + ''', ''' + CONVERT(VARCHAR, @usrdataLocal, 112) + ''', ''' + @usrhoraLocal + ''', ''' + @user + ''')'

			PRINT 'Update Central'

		END


		IF @usrdataCentral > @usrdataLocal OR (@usrdataLocal = @usrdataCentral AND @usrhoraCentral > @usrhoraLocal)
		BEGIN
		
			SET @updateLocal = @updateLocal + (CASE WHEN @updateLocal = '' THEN @cabUpdate ELSE ',' END) 
			+ '(''' + @bostampLocal + ''', ''' + @statusCentral + ''', ''' + @modo_envioCentral + ''', ''' + @pagamentoCentral + ''', ''' + CONVERT(VARCHAR, @usrdataCentral, 112) + ''', ''' + @usrhoraCentral + ''', ''' + @user + ''')'

			PRINT 'Update Local'

		END

		FETCH NEXT FROM uc_status INTO @bostampLocal, @modo_envioLocal, @pagamentoLocal, @statusLocal, @usrdataLocal, @usrhoraLocal,
		@bostampCentral, @modo_envioCentral, @pagamentoCentral, @statusCentral, @usrdataCentral, @usrhoraCentral
	END

	CLOSE uc_status
	DEALLOCATE uc_status


	IF @updateCentral <> ''
	BEGIN

		DECLARE @sqlCentral VARCHAR(MAX) = @updateCentral + 
			'UPDATE ' + @bdCentral + '.dbo.bo2 SET bo2.status=up.status,bo2.modo_envio=up.modo_envio,bo2.pagamento=up.pagamento,bo2.usrdata=up.usrdata,bo2.usrhora=up.usrhora,bo2.usrinis=up.usrinis
			FROM ' + @bdCentral + '.dbo.bo2 INNER JOIN @tabUpdate up ON bo2.bo2stamp=up.bostamp
			UPDATE ' + @bdCentral + '.dbo.bo SET bo.usrdata=up.usrdata,bo.usrhora=up.usrhora,bo.usrinis=up.usrinis
			FROM ' + @bdCentral + '.dbo.bo INNER JOIN @tabUpdate up ON bo.bostamp=up.bostamp'

		EXEC(@sqlCentral)

	END

	IF @updateLocal <> ''
	BEGIN

		DECLARE @sqlLocal VARCHAR(MAX) = @updateLocal + 
			'UPDATE bo2 SET bo2.status=up.status,bo2.modo_envio=up.modo_envio,bo2.pagamento=up.pagamento,bo2.usrdata=up.usrdata,bo2.usrhora=up.usrhora,bo2.usrinis=up.usrinis
			FROM bo2 INNER JOIN @tabUpdate up ON bo2.bo2stamp=up.bostamp
			UPDATE bo SET bo.usrdata=up.usrdata,bo.usrhora=up.usrhora,bo.usrinis=up.usrinis
			FROM bo INNER JOIN @tabUpdate up ON bo.bostamp=up.bostamp'

		EXEC(@sqlLocal)

	END

GO
GRANT EXECUTE on dbo.up_documentos_syncEncSiteCentral TO PUBLIC
GRANT CONTROL on dbo.up_documentos_syncEncSiteCentral TO PUBLIC
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO