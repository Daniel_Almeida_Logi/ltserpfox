-- Obt�m dados da Entidade
-- exec up_documentos_pesqEntidade 'AG','nome',0,'0000','0000','Loja 1'

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_documentos_pesqEntidade]') IS NOT NULL
	drop procedure dbo.up_documentos_pesqEntidade
go

create procedure dbo.up_documentos_pesqEntidade
@tipo as varchar(2),
@nome as varchar(55),
@no as numeric(9,0),
@ncont as varchar(20),
@telefone as varchar(60),
@site as varchar(20)


/* WITH ENCRYPTION */


AS
SET NOCOUNT ON	
	IF @tipo = 'AG'
	BEGIN
		Select 
			sel = 0,
			nome,
			no,
			estab = 0,
			esaldo = 0			
		From
			ag (nolock)
		Where
			nome like @nome + '%'
			and no = case when @no = 0 then no else @no end
			and ncont = case when @ncont = '' then ncont else @ncont end
			and telefone = case when @telefone = '' then telefone else @telefone end
	END
	IF @tipo = 'CL'
	BEGIN
		Select
			sel = 0,
			nome,
			no,
			estab,
			esaldo
		From
			b_utentes (nolock)
		Where 
			nome like @nome + '%'
			and no = case when @no = 0 then no else @no end
			and ncont = case when @ncont = '' then ncont else @ncont end
			and telefone = case when @telefone = '' then telefone else @telefone end
			
	END
	IF @tipo = 'FL'
	BEGIN
		Select
			sel = 0,
			nome,
			no,
			estab,
			esaldo
		From
			fl (nolock)
		Where
			nome like @nome + '%'
			and no = case when @no = 0 then no else @no end
			and ncont = case when @ncont = '' then ncont else @ncont end
			and telefone = case when @telefone = '' then telefone else @telefone end
			
	END
	
GO
Grant Execute On dbo.up_documentos_pesqEntidade to Public
Grant Control On dbo.up_documentos_pesqEntidade to Public
GO