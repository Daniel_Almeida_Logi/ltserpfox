
-- Obt�m dados do Documento
-- exec up_documentos_pedidoRegularizacao 'ADM53D8D2A8-54D9-4AFC-82A'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_documentos_pedidoRegularizacao]') IS NOT NULL
	drop procedure dbo.up_documentos_pedidoRegularizacao
go

create procedure dbo.up_documentos_pedidoRegularizacao
@stamp as varchar(25)

/* WITH ENCRYPTION */

AS
	
	/* Dados da Encomenda*/
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosEnc'))
		DROP TABLE #dadosEnc
	select 
		bistamp
		,qtt
		,u_bonus
		,desconto
		,desc2
		,ettdeb 
	into
		#dadosEnc
	from 
		bi (nolock)
	where 
		bi.ndos = 2 /*Encomenda Fornecedor*/
		and bistamp in (select bistamp from fn where fostamp = @stamp and bistamp != '')

	select 
		fn.ref
		,fn.design

		/*Encomenda*/
		,fn.u_qttenc
		,totalEnc = ISNULL(Enc.ettdeb,0)
		/*Fatura*/
		,fn.qtt
		,totalFt = fn.etiliquido
		/*Recebido*/
		,fn.qtrec
		,totalRec = convert(numeric(19,2),ROUND(case when fn.qtrec = 0 then 0 else (fn.etiliquido/fn.qtt)*fn.qtrec end,2))
		/* Diferen�a*/
		,difQt = fn.qtrec-fn.qtt
		,difValor = convert(numeric(19,2),ROUND(case when fn.qtrec = 0 then 0 else (fn.etiliquido/fn.qtt)*fn.qtrec end - fn.etiliquido,2))
	from 
		fn (nolock)
		left join #dadosEnc as Enc on fn.bistamp = Enc.bistamp
	where 
		fn.fostamp = @stamp
		and fn.ref != ''
		--and fn.bistamp != ''
		and ((fn.qtt-fn.qtrec) != 0 or (fn.u_qttenc - fn.qtrec) != 0 or (case when fn.qtrec = 0 then 0 else (fn.etiliquido/fn.qtt)*fn.qtrec end - fn.etiliquido) != 0)

GO
Grant Execute On dbo.up_documentos_pedidoRegularizacao to Public
Grant Control On dbo.up_documentos_pedidoRegularizacao to Public
GO