-- Obt�m dados do Documento
-- exec up_documentos_dadosDoc 'fdtgdtert'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_documentos_dadosDoc]') IS NOT NULL
	drop procedure dbo.up_documentos_dadosDoc
go

create procedure dbo.up_documentos_dadosDoc

@stamp as varchar(50)

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON	


	SELECT TOP 1 
		nomedoc, tipoDoc
	FROM (
		SELECT
			nmdos as nomedoc
			,tipoDoc = 'BO' 
		FROM bo (nolock)
		WHERE bostamp=@stamp
		
		union all
		
		SELECT
			docnome as nomedoc
			,tipoDoc = 'FO' 
		FROM 
			fo (nolock)
		WHERE
			fostamp=@stamp
	)x


GO
Grant Execute On dbo.up_documentos_dadosDoc to Public
Grant Control On dbo.up_documentos_dadosDoc to Public
GO