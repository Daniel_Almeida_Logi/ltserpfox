-- Obt�m Condi��es de Pagamentos de Fernecedores disponiveis
-- exec up_documentos_ConPagFornec
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_documentos_ConPagFornec]') IS NOT NULL
	drop procedure dbo.up_documentos_ConPagFornec
go

create procedure dbo.up_documentos_ConPagFornec
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	SELECT	
		DESCRICAO 
	FROM
		TP (nolock)
	WHERE
		tipo = 2 
	ORDER BY 
		DESCRICAO

GO
Grant Execute On dbo.up_documentos_ConPagFornec to Public
Grant Control On dbo.up_documentos_ConPagFornec to Public
GO