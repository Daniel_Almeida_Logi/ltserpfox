-- Listagem de Op��es diversas
-- exec up_documentos_OpcoesDiversas
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_documentos_OpcoesDiversas]') IS NOT NULL
	drop procedure dbo.up_documentos_OpcoesDiversas
go

create procedure dbo.up_documentos_OpcoesDiversas

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	Select 
		[desc] = 'Importar Artigos por Tabela de Iva', 
		id = 1
	Union All
	Select 
		[desc] = 'Listagem de Produtos Encomendados, N�o Recepcionados', 
		id = 2
	Union All
	Select 
		[desc] = 'Imprimir Tal�o de Reserva', 
		id = 3

GO
Grant Execute On dbo.up_documentos_OpcoesDiversas to Public
Grant Control On dbo.up_documentos_OpcoesDiversas to Public
GO