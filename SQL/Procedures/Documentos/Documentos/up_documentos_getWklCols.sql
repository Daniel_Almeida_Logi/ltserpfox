
/*	
	Inventário para enviar para a AT

	exec up_documentos_getWklCols 7, 'BO'

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_documentos_getWklCols]') IS NOT NULL
	drop procedure dbo.up_documentos_getWklCols
go

create procedure dbo.up_documentos_getWklCols
	@wkdoc NUMERIC(5),
	@tipo VARCHAR(10),
	@doc VARCHAR(50)
/* WITH ENCRYPTION */
AS
BEGIN

	SELECT	
		wkreadonly = case when wkfield like '%iva%' and wkfield!='fn.ivaincl' then CONVERT(bit,1) else wkreadonly end
		,wkfield = CASE WHEN wkfield = 'bi.qtt*bi.epcusto' THEN 'bi.ettdeb' ELSE wkfield END
		,wktitle
		,u_ordem as Ordem
		,u_largura as Largura
		,u_mascara as mascara
	FROM
		WKLCOLS (nolock) a 
		INNER JOIN WK (nolock) b ON a.wkstamp = b.wkstamp
	WHERE 
		--wkdoc = @wkdoc
		wkdocdescr = @doc
		and u_ordem != 0
		and WKFIELD <> 'WKOrigens'
		and WKTITLE <> 'Centro Analítico' 
		and B.wktipo = 'S' + LTRIM(RTRIM(@tipo))
	ORDER BY 
		u_ordem ASC


END


GO
Grant Execute on dbo.up_documentos_getWklCols to Public
Grant control on dbo.up_documentos_getWklCols to Public
Go