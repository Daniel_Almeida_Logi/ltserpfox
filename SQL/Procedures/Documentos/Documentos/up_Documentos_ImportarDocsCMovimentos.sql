-- Painel de Importacao de Documentos
-- exec up_Documentos_ImportarDocsCMovimentos '20140101','20130101'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_ImportarDocsCMovimentos]') IS NOT NULL
	drop procedure dbo.up_Documentos_ImportarDocsCMovimentos
go

create PROCEDURE [dbo].up_Documentos_ImportarDocsCMovimentos
@dataIni datetime,
@dataFim datetime

/* WITH ENCRYPTION */
AS


Select	
	distinct docnome
from	
	fo (nolock)
Where
	fo.data between @dataIni and @dataFim
union all 
Select	
	distinct nmdos
from	
	bo (nolock)
Where
	bo.dataobra between @dataIni and @dataFim

go
Grant Execute on dbo.up_Documentos_ImportarDocsCMovimentos to Public
Grant Control on dbo.up_Documentos_ImportarDocsCMovimentos to Public
go