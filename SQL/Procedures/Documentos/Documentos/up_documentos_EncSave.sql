SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_documentos_EncSave]') IS NOT NULL
	drop procedure dbo.up_documentos_EncSave
go

create procedure dbo.up_documentos_EncSave

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	Select
		sel			= CONVERT(bit,0)
		,stamp
		,data		= CONVERT(varchar,data,102) + '  ' + hora
		,uData		= CONVERT(varchar,udata,102) + '  ' + uhora
		,del			= CONVERT(bit,0)
		,tipoenc, ousr, usr
	from
		b_encAutoSave (nolock)
	group by
		stamp, data, hora, udata, uhora, tipoenc, ousr, usr
	order by
		data
	
GO
Grant Execute On dbo.up_documentos_EncSave to Public
Grant Control On dbo.up_documentos_EncSave to Public
GO	