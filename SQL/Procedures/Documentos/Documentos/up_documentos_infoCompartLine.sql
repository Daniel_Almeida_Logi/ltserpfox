/* Comparticipação

	exec up_documentos_infoCompartLine 'ADMD7075EC7-B992-4170-855','124321',''

 */
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_documentos_infoCompartLine]') IS NOT NULL
    drop procedure dbo.up_documentos_infoCompartLine
go
create procedure dbo.up_documentos_infoCompartLine

@token as varchar(50)
,@ftstamp as varchar(50)
,@codAcess as varchar(50)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

    SELECT
		 fi_compart.fistamp,
		 fi_compart.hasPrescription,
		 fi_compart.lineNumber,
		 fi_compart.pvpValue,
		 fi_compart.prescriptionExceptions,
		 fi_compart.prescriptionDispatch,
		 fi_compart.prescriptionPVPValue,
		 fi_compart.ref,
		 fi_compart.quantity,
		 fi_compart.snsValue,
		 fi_compart.uncontributedValue,
		 fi_compart.codCNP,
		 fi_compart.percIva,
		 fi_compart.ivaIncl,
		 fi_compart.refDescription,
		 fi_compart.unitValue,
		 fi_compart.qttminvd,
		 fi_compart.qttembal,
		 fi_compart.percCmp
    FROM
		fi_compart (nolock) 

    WHERE
		fi_compart.token = @token     
		and fi_compart.ftstamp = @ftstamp
		and fi_compart.codacesso  = case when ltrim(rtrim(@codAcess))='' then  fi_compart.codacesso else  @codAcess end
    
GO
Grant Execute On dbo.up_documentos_infoCompartLine to Public
Grant Control On dbo.up_documentos_infoCompartLine to Public
GO
