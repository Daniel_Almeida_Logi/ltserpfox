SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_documentos_calcTotsResumo]') IS NOT NULL
	drop procedure dbo.up_documentos_calcTotsResumo
go

create procedure dbo.up_documentos_calcTotsResumo
@stamps as Varchar(MAX)

AS

	SET NOCOUNT ON

	DECLARE @ul_lin AS TABLE (stamp VARCHAR(25))


	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#basesInc'))
		DROP TABLE #basesInc



	INSERT INTO 
		@ul_lin (stamp)
	SELECT 
		y.value
	FROM 
		dbo.SplitString(@stamps, ';') x
		cross apply dbo.SplitString(x.value, ';') y

	SELECT 
		SUM((CASE WHEN ivaincl = 1 THEN (etiliquido/(taxa/100+1)) ELSE (ETILIQUIDO) END) * (case when doccode in (3,105) then -1 else 1 end)) as baseinc
		,tabIva
	INTO 
		#basesInc
	FROM
	(
		select 
			fn.iva as taxa,
			fn.etiliquido,
			fo.doccode,
			fn.ivaincl,
			fn.tabiva
		FROM
			fn(NOLOCK)
			join fo(nolock) on fo.fostamp = fn.fostamp
		WHERE
			fn.FOSTAMP IN (SELECT DISTINCT FFN.FOSTAMP FROM FN(NOLOCK) AS FFN WHERE FFN.FNSTAMP IN (SELECT STAMP FROM @ul_lin))	
			and fn.qtt>0
	) as x
	GROUP BY taxa, tabiva

	SELECT 
		*
	from
	(
		SELECT 
			taxa
			,sum(valIva) as valorIvaOriginal
			,sum(valIva) as valorIva
			,ISNULL((select baseInc from #basesInc where tabIva = x.tabIva), 0) as BaseInc
			,0000000000.00 as arredondamento
			,0000000000.00 as valoraposarredondamento
			,tabIva
		FROM
		(
			select
				eivav1 * (case when doccode in (3,105) then -1 else 1 end) as valIva
				,1 as tabIva
				,(select taxa from taxasIva(nolock) where codigo = 1) as taxa
			FROM
				FO(nolock)
			WHERE
				FO.FOSTAMP IN (SELECT DISTINCT FFN.FOSTAMP FROM FN(NOLOCK) AS FFN WHERE FFN.FNSTAMP IN (SELECT STAMP FROM @ul_lin))	

			UNION ALL

			select
				eivav2 * (case when doccode in (3,105) then -1 else 1 end) as valIva
				,2 as tabIva
				,(select taxa from taxasIva(nolock) where codigo = 2) as taxa
			FROM
				FO(nolock)
			WHERE
				FO.FOSTAMP IN (SELECT DISTINCT FFN.FOSTAMP FROM FN(NOLOCK) AS FFN WHERE FFN.FNSTAMP IN (SELECT STAMP FROM @ul_lin))	

			UNION ALL

			select
				eivav3 * (case when doccode in (3,105) then -1 else 1 end) as valIva
				,3 as tabIva
				,(select taxa from taxasIva(nolock) where codigo = 3) as taxa
			FROM
				FO(nolock)
			WHERE
				FO.FOSTAMP IN (SELECT DISTINCT FFN.FOSTAMP FROM FN(NOLOCK) AS FFN WHERE FFN.FNSTAMP IN (SELECT STAMP FROM @ul_lin))	

			UNION ALL

			select
				eivav4 * (case when doccode in (3,105) then -1 else 1 end) as valIva
				,4 as tabIva
				,(select taxa from taxasIva(nolock) where codigo = 4) as taxa
			FROM
				FO(nolock)
			WHERE
				FO.FOSTAMP IN (SELECT DISTINCT FFN.FOSTAMP FROM FN(NOLOCK) AS FFN WHERE FFN.FNSTAMP IN (SELECT STAMP FROM @ul_lin))	

			UNION ALL

			select
				eivav5 * (case when doccode in (3,105) then -1 else 1 end) as valIva
				,5 as tabIva
				,(select taxa from taxasIva(nolock) where codigo = 5) as taxa
			FROM
				FO(nolock)
			WHERE
				FO.FOSTAMP IN (SELECT DISTINCT FFN.FOSTAMP FROM FN(NOLOCK) AS FFN WHERE FFN.FNSTAMP IN (SELECT STAMP FROM @ul_lin))	
	
			UNION ALL

			select
				eivav6 * (case when doccode in (3,105) then -1 else 1 end) as valIva
				,6 as tabIva
				,(select taxa from taxasIva(nolock) where codigo = 6) as taxa
			FROM
				FO(nolock)
			WHERE
				FO.FOSTAMP IN (SELECT DISTINCT FFN.FOSTAMP FROM FN(NOLOCK) AS FFN WHERE FFN.FNSTAMP IN (SELECT STAMP FROM @ul_lin))	

			UNION ALL

			select
				eivav7 * (case when doccode in (3,105) then -1 else 1 end) as valIva
				,7 as tabIva
				,(select taxa from taxasIva(nolock) where codigo = 7) as taxa
			FROM
				FO(nolock)
			WHERE
				FO.FOSTAMP IN (SELECT DISTINCT FFN.FOSTAMP FROM FN(NOLOCK) AS FFN WHERE FFN.FNSTAMP IN (SELECT STAMP FROM @ul_lin))	

			UNION ALL

			select
				eivav8 * (case when doccode in (3,105) then -1 else 1 end) as valIva
				,8 as tabIva
				,(select taxa from taxasIva(nolock) where codigo = 8) as taxa
			FROM
				FO(nolock)
			WHERE
				FO.FOSTAMP IN (SELECT DISTINCT FFN.FOSTAMP FROM FN(NOLOCK) AS FFN WHERE FFN.FNSTAMP IN (SELECT STAMP FROM @ul_lin))	

			UNION ALL

			select
				eivav9 * (case when doccode in (3,105) then -1 else 1 end) as valIva
				,9 as tabIva
				,(select taxa from taxasIva(nolock) where codigo = 9) as taxa
			FROM
				FO(nolock)
			WHERE
				FO.FOSTAMP IN (SELECT DISTINCT FFN.FOSTAMP FROM FN(NOLOCK) AS FFN WHERE FFN.FNSTAMP IN (SELECT STAMP FROM @ul_lin))	
		) as x
		group by taxa, tabIva
	) as y
	WHERE BaseInc <> 0

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#basesInc'))
		DROP TABLE #basesInc


GO
Grant Execute On dbo.up_documentos_calcTotsResumo to Public
Grant Control On dbo.up_documentos_calcTotsResumo to Public
Go