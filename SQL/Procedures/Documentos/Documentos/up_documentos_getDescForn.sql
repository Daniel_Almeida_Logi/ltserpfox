/* 
	Devolve o desconto de fornecedor presente no documento Condi��es Comerciais de Fornecedor mais recente
	exec up_documentos_getDescForn '5440987', 1, 0

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_documentos_getDescForn]') IS NOT NULL
	DROP PROCEDURE dbo.up_documentos_getDescForn
GO

CREATE PROCEDURE dbo.up_documentos_getDescForn
	@ref VARCHAR(18),
	@no NUMERIC(9),
	@estab NUMERIC(5)

AS
SET NOCOUNT ON	

	SELECT ISNULL((
	SELECT 
		TOP 1 bi.desconto 
	FROM 
		bi(NOLOCK) 
		join bo(nolock) ON bi.bostamp COLLATE DATABASE_DEFAULT= bo.bostamp COLLATE DATABASE_DEFAULT 
		join ts(nolock) ON ts.ndos = bo.ndos 
	WHERE 
		ts.codigoDoc = 39 
		and bi.ref = @ref
		AND bo.no = @no
		AND bo.fechada = 0
		and bo.estab = @estab
	ORDER BY 
		bi.ousrdata desc, bi.ousrhora desc), CAST(0 as numeric(6,2))) as desconto

GO
GRANT EXECUTE on dbo.up_documentos_getDescForn TO PUBLIC
GRANT Control on dbo.up_documentos_getDescForn TO PUBLIC
GO