-- Condi��o De Pagamento - Lista Documentos
-- exec up_Documentos_CondPagFornecedor 201,0
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_CondPagFornecedor]') IS NOT NULL
	drop procedure dbo.up_Documentos_CondPagFornecedor
go
create procedure dbo.up_Documentos_CondPagFornecedor

@no as numeric(9,0),
@estab as numeric(2,0)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

SELECT	TP.VENCIMENTO,TP.descricao as CONDPAG,
		CASE WHEN TP.VENCIMENTO = 1 THEN ISNULL((SELECT FL.VENCIMENTO FROM FL (nolock) WHERE NO = @no AND estab = @estab),0) ELSE DIAS END as DIAS,
		DATEADD(DAY,CASE WHEN TP.VENCIMENTO = 1 THEN ISNULL((SELECT FL.VENCIMENTO FROM FL (nolock)WHERE NO = @no AND estab = @estab),0) ELSE DIAS END,GETDATE()) as DATAVENCIMENTO
FROM	
	TP (nolock)
INNER JOIN
	fl (nolock) ON tp.descricao = fl.tpdesc
WHERE
	FL.no = @no AND FL.estab = @estab

GO
Grant Execute On dbo.up_Documentos_CondPagFornecedor to Public
Grant Control On dbo.up_Documentos_CondPagFornecedor to Public
GO