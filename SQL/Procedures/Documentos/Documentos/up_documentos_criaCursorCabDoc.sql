-- Procurar Entidade  (Documentos)
-- exec up_documentos_criaCursorCabDoc 'BO','ADM12082741252.849250816'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_documentos_criaCursorCabDoc]') IS NOT NULL
	drop procedure dbo.up_documentos_criaCursorCabDoc
GO

create procedure dbo.up_documentos_criaCursorCabDoc 

@tipo varchar(20),
@cabstamp char(25)

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

IF @tipo = 'FO'
BEGIN
	Select 
		cabStamp		= FO.FOSTAMP
		,doc			= FO.docnome
		,numinternodoc	= FO.doccode
		,numdoc			= FO.adoc
		,nome			= FO.NOME
		,nome2
		,no				= FO.NO
		,estab			= FO.ESTAB
		,condpag		= tpdesc
		,moeda
		,datavenc		= FO.PDATA	
		,datadoc		= FO.Docdata
		,dataentrega	= FO.Docdata /*N�o aplicavel em Docs de Compras*/
		,dataInterna	= FO.DATA
		,estadodoc		= FO.U_STATUS
		,contadordoc	= U_DOCCONT 
		,DESCFIN		= FO.FIN
		,VALORDESCFIN	= FO.EFINV
		,DOCFL			= FO2.U_DOCFL 
		,DOCFLNO		= FO2.U_DOCFLNO
		,baseinc		= FO.ETTILIQ
		,iva			= FO.ETTIVA
		,total			= FO.ETOTAL
		,valorIva1		= FO.EIVAV1   
		,valorIva2		= FO.EIVAV2   
		,valorIva3		= FO.EIVAV3   
		,valorIva4		= FO.EIVAV4   
		,valorIva5		= FO.EIVAV5   
		,valorIva6		= FO.EIVAV6   
		,valorIva7		= FO.EIVAV7   
		,valorIva8		= FO.EIVAV8   
		,valorIva9		= FO.EIVAV9
		,dataregisto	= FO.ousrdata
		,horaregisto	= FO.ousrhora
		,userregisto	= FO.ousrinis
		,dataaltera		= FO.usrdata
		,horaaltera		= FO.usrhora
		,useraltera		= FO.usrinis
		,armazem		= FO2.U_ARMAZEM
		,logi1			= 0  /*N�o aplicavel em Docs de Compras*/
		,morada			= FO.Morada
		,[local]		= FO.Local
		,codpost		= FO.codPost
		,tipo
		,ncont			= FO.ncont
		,foid			= FO.foid
		,u_class		= '' /*N�o aplicavel em Docs de Compras*/
		,ccusto			= FO.ccusto
		,obs			= FO.FINAL
		,plano			= fo.Plano
		,datafinal		= FO.Docdata /*N�o aplicavel em Docs de Compras*/
		,xpddata		= '' /*N�o aplicavel em Docs de Compras*/
		,xpdhora		= '' /*N�o aplicavel em Docs de Compras*/
		,xpdviatura		= '' /*N�o aplicavel em Docs de Compras*/
		,xpdmorada		= '' /*N�o aplicavel em Docs de Compras*/
		,xpdcontacto	= '' /*N�o aplicavel em Docs de Compras*/
		,xpdcodpost		= '' /*N�o aplicavel em Docs de Compras*/
		,xpdemail		= '' /*N�o aplicavel em Docs de Compras*/
		,xpdtelefone	= '' /*N�o aplicavel em Docs de Compras*/
		,email			= '' /*N�o aplicavel em Docs de Compras*/
		,telefone		= '' /*N�o aplicavel em Docs de Compras*/
		,totalnservico	= 0
	FROM 
		fo (nolock)
		inner join fo2 (nolock) on fo.fostamp = fo2.fo2stamp		
	where
		fo.fostamp = @cabstamp		
END

IF @tipo = 'BO'
BEGIN
	Select 
		cabstamp		= bostamp
		,doc			= nmdos
		,numinternodoc	= ndos
		,numdoc			= convert(varchar(20),OBRANO)
		,nome
		,nome2
		,no
		,estab
		,condpag		= tpdesc
		,moeda
		,datavenc		= dataobra	/*N�o aplicavel nos DIs*/
		,datadoc		= dataobra
		,dataentrega	= u_dataentr
		,dataInterna	= dataobra	/*N�o aplicavel nos DIs*/
		,estadodoc		= case when FECHADA = 1 then 'F' else 'A' end
		,contadordoc	= U_DOCCONT
		,DESCFIN		= 0			/*N�o aplicavel nos DIs*/
		,VALORDESCFIN	= 0			/*N�o aplicavel nos DIs*/
		,DOCFL			= ''		/*N�o aplicavel nos DIs*/  
		,DOCFLNO		= 0				/*N�o aplicavel nos DIs*/
		,baseinc		= etotaldeb
		,iva			= ETOTIVA
		,total			= etotal
		,valorIva1		= EBO11_IVA
		,valorIva2		= BO.EBO21_IVA
		,valorIva3		= BO.EBO31_IVA
		,valorIva4		= BO.EBO41_IVA
		,valorIva5		= BO.EBO51_IVA
		,valorIva6		= BO.EBO61_IVA
		,valorIva7		= BO2.EBO71_IVA
		,valorIva8		= BO2.EBO81_IVA
		,valorIva9		= BO2.EBO91_IVA
		,dataregisto	= BO.ousrdata
		,horaregisto	= BO.ousrhora
		,userregisto	= BO.ousrinis
		,dataaltera		= BO.usrdata
		,horaaltera		= BO.usrhora
		,useraltera		= BO.usrinis
		,armazem		= BO2.ARMAZEM
		,logi1			= BO.logi1
		,morada			= BO.Morada
		,[local]		= BO.Local
		,codpost		= BO.CodPost
		,ncont			= BO.ncont
		,tipo
		,foid			= 0		/*N�o aplicavel nos DIs*/
		,u_class		= BO2.U_CLASS
		,ccusto			= BO.ccusto
		,obs			= BO.obs
		,plano			= CONVERT(bit,0)  /*N�o aplicavel nos DIs*/
		,datafinal		= BO.datafinal
		,xpddata		= BO2.xpddata
		,xpdhora		= BO2.xpdhora
		,xpdviatura		= BO2.xpdviatura
		,xpdmorada		= BO2.morada
		,xpdcontacto	= BO2.contacto
		,xpdcodpost		= BO2.codpost
		,xpdemail		= BO2.email
		,xpdtelefone	= BO2.telefone
		,email
		,telefone
		,totalnservico	= 0		
	from 
		bo (nolock)
		inner join bo2 (nolock) on bo.bostamp = bo2.bo2stamp
	where
		bo.bostamp = @cabstamp
END


GO
Grant Execute On dbo.up_documentos_criaCursorCabDoc to Public
Grant Control On dbo.up_documentos_criaCursorCabDoc to Public
Go