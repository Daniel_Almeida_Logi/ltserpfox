/* 
	exec up_empresa_infoCompartEmpresa 1, 'BICSEG'

*/	


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_empresa_infoCompartEmpresa]') IS NOT NULL
	DROP PROCEDURE [dbo].[up_empresa_infoCompartEmpresa] ;
GO


CREATE PROCEDURE [dbo].[up_empresa_infoCompartEmpresa]
	@site		INT ,
	@efrid		VARCHAR(50)

 /*WITH ENCRYPTION */
AS
SET NOCOUNT ON
	DECLARE @pais varchar(100)

	SELECT @pais = UPPER(textValue) FROM B_Parameters_site WHERE STAMP = 'ADM0000000050'

	SELECT 
		CASE WHEN @pais = 'ANGOLA' THEN ISNULL((SELECT code FROM codesExtSite WHERE noSite = @site AND id=@efrid),-1) ELSE RIGHT('0000'+ISNULL(CONVERT(varchar,infarmed),'0'),5) END  infarmed,
		assfarm,
		nomecomp,
		id_lt,ncont 
	FROM empresa (NOLOCK) WHERE no= @site

GO
Grant Execute On dbo.up_empresa_infoCompartEmpresa to Public
Grant control on dbo.up_empresa_infoCompartEmpresa to Public
GO


