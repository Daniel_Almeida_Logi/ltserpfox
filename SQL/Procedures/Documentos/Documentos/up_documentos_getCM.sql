/*

	Retorna configurações do documento CM1

	exec up_documentos_getCM 5

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


IF OBJECT_ID('[dbo].[up_documentos_getCM]') IS NOT NULL
	DROP procedure dbo.up_documentos_getCM
GO

CREATE procedure dbo.up_documentos_getCM
@cm AS NUMERIC(5)

AS

	SELECT 
		*
	FROM
		cm1(nolock)
	WHERE
		cm = @cm
	
GO
GRANT EXECUTE on dbo.up_documentos_getCM TO PUBLIC
GRANT CONTROL on dbo.up_documentos_getCM TO PUBLIC
GO