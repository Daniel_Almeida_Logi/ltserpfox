-- SP de Origens no Documento de Dossiers Internos
-- exec up_documentos_origens_BO 'ADMB76CBB19-B006-4FC1-AB8', ''

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_documentos_origens_BO]') IS NOT NULL
	drop procedure dbo.up_documentos_origens_BO
go

create procedure dbo.up_documentos_origens_BO
@fnstamp as varchar(25),
@oBistamp as varchar(55),
@fistamp as varchar(25) = ''
	
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	;with cte1 as (
		Select 	
			Tipo = 'BO'
			,Doc = RTRIM(LTRIM(Bi.nmdos)) + ' Nr.' + RTRIM(LTRIM(STR(bi.obrano)))
			, qtt
			, pct = bi.edebito
			, CONVERT(varchar,dataobra,102) as [Data]
			,stamporidest = bi.Bostamp
			,ofnstamp = (select fnstamp from bi2(nolock) where bi2stamp = bistamp)
			,obistamp = bi.obistamp 
			,ofistamp = bi2.fistamp
			,pcl = bi.ettdeb
			,desconto = bi.desconto
			,total = bi.ettdeb
		From    
			bi (nolock)
			join bi2(nolock) on bi2.bi2stamp = bi.bistamp
		Where   
			bistamp = @oBistamp /*Bi.oBistamp*/
				
	), cte2 as (
		Select	
			Tipo = 'FO'
			,Doc = RTRIM(LTRIM(FN.docnome)) + ' Nr.' + RTRIM(LTRIM(fn.adoc))
			,Qtt
			,PCT = fn.epv
			,CONVERT(varchar,docdata,102) as [Data]
			,stamporidest = Fo.fostamp
			,ofnstamp = fn.ofnstamp
			,obistamp = fn.bistamp
			,ofistamp = fn.fistamp
			,pcl = fn.u_upc
			,desconto = fn.desconto
			,total = fn.etiliquido 
		From	
			FN (nolock)
			INNER JOIN Fo (nolock) On fn.fostamp = fo.fostamp
		Where	
			fnstamp = @fnstamp /*Bi2.fnstamp*/
	), cte3 as (
		Select	
			Tipo = 'FT'
			,Doc = RTRIM(LTRIM(FT.NMDOC)) + ' Nr.' + RTRIM(LTRIM(ft.fno))
			,Qtt
			,PCT = fi.epv
			,CONVERT(varchar,ft.fdata,102) as [Data]
			,stamporidest = Ft.ftstamp
			,ofnstamp = fi2.fnstamp
			,obistamp = fi.bistamp
			,ofistamp = fi.ofistamp
			,pcl = fi.etiliquido
			,desconto = fi.desconto
			,total = ft.ettiliq 
		From	
			Fi (nolock)
			JOIN fi2(nolock) on fi2.fistamp = fi.fistamp
			INNER JOIN Ft (nolock) On ft.ftstamp = fi.ftstamp
		Where	
			fi.fistamp = @fistamp /*Bi2.fnstamp*/
	)

	
	Select 
		RowNumber = ROW_NUMBER() OVER (ORDER BY stamporidest)
		, *
	From	
	(
		Select * From cte1 
		UNION ALL
		Select * From cte2
		UNION ALL
		Select * From cte3
	) x

GO
Grant Execute On dbo.up_documentos_origens_BO to Public
Grant Control On dbo.up_documentos_origens_BO to Public
GO