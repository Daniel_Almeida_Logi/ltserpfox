/* Painel de Importa��o de Documentos - Lista Documentos
 
	exec up_documentos_ImportActualizaDocs 5, 'Encomenda a Fornecedor', 1, 1, '20170101', '20211001', 'Loja 1','vic der'
	select no, nome,* from bo where dataobra='20170920'

 */




SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_documentos_ImportActualizaDocs]') IS NOT NULL
    drop procedure dbo.up_documentos_ImportActualizaDocs
go
create procedure dbo.up_documentos_ImportActualizaDocs

@no as numeric(9,0),
@docnome as varchar(MAX),
@filtra as int,
@filtraEstado as int,
@dataIni as datetime,
@dataFim as datetime,
@site as varchar(20),
@prod as varchar(99) = ''



/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

DECLARE @ul_tipo AS TABLE (cab VARCHAR(25))


INSERT INTO 
	@ul_tipo (cab)
SELECT 
	y.value
FROM 
	dbo.SplitString(@docnome, ',') x
	cross apply dbo.SplitString(x.value, ',') y

SET @prod = isnull(ltrim(rtrim(@prod)),'')
SET @prod = REPLACE(@prod,' ','%')

Select *
FROM (
    /* compras */
    SELECT
		distinct
		QTTOT = (select SUM(QTT) from fn where fn.fostamp = fo.fostamp),
		Operador    = cxusername,
		fo.ousrhora as HORA,
		'FO' as TIPO,
		U_STATUS as ESTADO,
		fo.FOSTAMP as CABSTAMP,
		0 as COPIAR,
		fo.DOCNOME as TIPODOC,
		fo.ADOC as DOCUMENTO,
		fo.data as data,
		ETOTAL as VALOR,
		u_copref = convert(numeric(1,0),cm1.u_copref)
		,FO.nome as Entidade
		,encomenda = convert(varchar(60),'')
		, 0 as no
		, 0 as estab
    FROM
		FO (nolock) 
		inner join fn(nolock) on fn.fostamp = fo.fostamp
		inner join cm1 (nolock) on cm1.cm = fo.doccode
    WHERE
		FO.docnome in (select cab from @ul_tipo)
		AND FO.no = case when @filtra=1 then @no else fo.no end
		AND u_status    = case when @filtraEstado=1 then 'A' else u_status end
		AND Fo.docdata between @dataIni And @dataFim
		AND fo.site = @site
		AND 
			(fn.ref = case when @prod='' then fn.ref else ltrim(rtrim(left(@prod,18))) end
			Or
			fn.design like case when @prod='' then fn.design else + '%'+  @prod +'%' end)


    /* Dossiers */
    UNION ALL
    
    SELECT 
		distinct 
		(select SUM(QTT) from bi where bi.bostamp = bo.bostamp) as QTTOT,
        --bo.vendnm as OPERADOR,
		(select iniciais from b_us(nolock) where userno = bo.vendedor) as OPERADOR,
        bo.ousrhora as HORA,
        'BO' as TIPO,
        CASE WHEN bo.FECHADA = 0 THEN 'A' ELSE 'F' END as ESTADO,
        bo.BOSTAMP as CABSTAMP,
        0 as COPIAR,
        bo.NMDOS as TIPODOC,
        CONVERT(varchar,bo.OBRANO) as DOCUMENTO,
        bo.dataobra as DATA,
        ETOTAL as VALOR,
        u_copref = convert(numeric(1,0),ts.u_copref)
        ,BO.nome as Entidade
        ,encomenda = convert(varchar(60),'')
		, 0 as no
		, 0 as estab
    FROM 
		BO (nolock)
		inner join BI (nolock) on BI.bostamp = BO.bostamp
		inner join ts (nolock) on ts.ndos = bo.ndos
    Where 
		BO.nmdos in (select cab from @ul_tipo)
		AND BO.NO = case when @filtra=1 then @no else bo.no end
		AND bo.fechada    = case when @filtraEstado=1 then 0 else bo.fechada end
		AND bo.dataobra between @dataIni And @dataFim
		AND bo.site = @site
		AND 
			(bi.ref = case when @prod='' then bi.ref else ltrim(rtrim(left(@prod,18))) end
			Or
			bi.design like case when @prod='' then bi.design else + '%'+  @prod +'%' end)

	--Documentos de Fatura��o
    UNION ALL
    
    SELECT 
		distinct 
		SUM(fi.QTT) as QTTOT,
        --bo.vendnm as OPERADOR,
		(select iniciais from b_us(nolock) where userno = ft.vendedor) as OPERADOR,
        ft.ousrhora as HORA,
        'FT' as TIPO,
        'A' as ESTADO,
        FT.FTSTAMP as CABSTAMP,
        0 as COPIAR,
        FT.NMDOC as TIPODOC,
        CONVERT(varchar,FT.FNO) as DOCUMENTO,
        FT.FDATA as DATA,
        ETOTAL as VALOR,
        u_copref = convert(numeric(1,0),tD.u_copref)
        ,FT.nome as Entidade
        ,encomenda = convert(varchar(60),'')
		, 0 as no
		, 0 as estab
    FROM 
		FT (nolock)
		inner join FI (nolock) on FI.Ftstamp = Ft.Ftstamp
		inner join td (nolock) on td.ndoc = ft.ndoc
    Where 
		Ft.nmdoc in (select cab from @ul_tipo)
		AND ft.NO = case when @filtra=1 then @no else ft.no end
		AND ft.fdata between @dataIni And @dataFim
		AND ft.site = @site
		AND 
			(fi.ref = case when @prod='' then fi.ref else ltrim(rtrim(left(@prod,18))) end
			Or
			fi.design like case when @prod='' then fi.design else + '%'+  @prod +'%' end)
	group by
		ft.vendedor, ft.ousrhora, ft.FTSTAMP, ft.nmdoc, ft.Fno, ft.fdata,ft.ETOTAL, td.u_copref, ft.ousrinis, ft.nome

) a
ORDER BY 
	DATA + HORA DESC
    
GO
Grant Execute On dbo.up_documentos_ImportActualizaDocs to Public
Grant Control On dbo.up_documentos_ImportActualizaDocs to Public
GO


