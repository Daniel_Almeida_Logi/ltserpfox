/* SP que controla ligacoes a documentos
	
	 exec up_Documentos_verificaLigacoes 'FO','ADMC1B12E3E-A1DB-490F-8A6'
	 exec up_Documentos_verificaLigacoes 'FO','ADM6B2C7635-F6C6-4CE5-946'
	 
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_Documentos_verificaLigacoes]') IS NOT NULL
	drop procedure dbo.up_Documentos_verificaLigacoes
GO

create procedure dbo.up_Documentos_verificaLigacoes
@tipo varchar(2)
,@stamp varchar(25)

/* WITH ENCRYPTION */
AS

	IF @tipo = 'BO'
	BEGIN

		Select 
			bi.bistamp
		from 
			bi (nolock)
			left join bi (nolock) bi_destino on  bi.bistamp = bi_destino.obistamp
			left join fn (nolock) fn_destino on bi.bistamp = fn_destino.bistamp
		where 
			bi.bostamp = @stamp
			and (bi_destino.bistamp is not null or fn_destino.bistamp is not null)

	END
	ELSE
	BEGIN
		
		Select 
			fn.fnstamp,
			bi_destino.nmdos
		from 
			fn (nolock)
			left join fn (nolock) fn_destino on  fn.fnstamp = fn_destino.ofnstamp
			left join bi2 (nolock) bi2_destino on fn.fnstamp = bi2_destino.fnstamp
			left join bi (nolock) bi_destino on bi_destino.bistamp = bi2_destino.bi2stamp
			left join ts (nolock) on bi_destino.ndos = ts.ndos 
		where 
			fn.fostamp = @stamp 
			and (
				fn_destino.ofnstamp is not null 
				or 
				(bi2_destino.fnstamp is not null and isnull(ts.cmstocks,0) != 0)
				)	
			and fn_destino.docnome != 'V/Factura'
			and bi_destino.nmdos != 'Devol. a Fornecedor'
			and bi_destino.nmdos != 'Encomenda a Fornecedor'
				
	END
	
GO
Grant Execute On dbo.up_Documentos_verificaLigacoes to Public
Grant Control On dbo.up_Documentos_verificaLigacoes to Public
GO
