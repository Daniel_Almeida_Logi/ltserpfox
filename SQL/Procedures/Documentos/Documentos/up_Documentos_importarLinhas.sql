-- Importar Linhas
-- exec up_Documentos_importarLinhas 'BI','FRrF1C15EE2-C35B-4BAA-943'
-- exec up_touch_hist_precos '4297180'
-- select fprod.pvporig,* from fprod where fprod.cnp = '4297180'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



if OBJECT_ID('[dbo].[up_Documentos_importarLinhas]') IS NOT NULL
	drop procedure dbo.up_Documentos_importarLinhas
go

create procedure dbo.up_Documentos_importarLinhas
@tipo varchar(2),
@stamp varchar(30)

/* WITH ENCRYPTION */
AS

IF @tipo = 'FN'
BEGIN
	Select 
		usalote = ISNULL(st.usalote,convert(bit,0))
		,qttmov = ISNULL(CASE WHEN FN.ref <> '' THEN (SELECT SUM(QTT) as QTT FROM FN (nolock) as FNDEST WHERE FNDEST.ofnstamp = FN.fnstamp) ELSE 0 END,0) 
		,pvporig	= (convert(numeric(13,3), isnull(fprod.pvporig, 0)))
		,validade = cast(year(st.validade) as varchar(4)) + right('0'+cast(month(st.validade) as varchar(2)),2)
		,epv1 = case when (convert(numeric(13,3), isnull(fprod.pvporig, 0)))=0 then 0 else st.epv1 end
		,fo.data
		,st.familia as famiSt
		,fn.*
	From 
		FN (nolock)
		inner join fo (nolock) on fn.fostamp = fo.fostamp
		left join st (nolock) on st.ref = fn.ref or st.ref = fn.oref
		left join empresa on st.site_nr = empresa.no
		left join fprod (nolock) on fprod.cnp=st.ref
	Where 
		FNSTAMP = @stamp
		and fo.site = empresa.site
		
END

IF @tipo = 'BI'
BEGIN
	Select 
		usalote = ISNULL(st.usalote,convert(bit,0))
		,qttcli = ISNULL(st.qttcli,convert(bit,0))
		,qttmov = ISNULL(CASE WHEN BI.ref <> '' THEN (SELECT SUM(QTT) as QTT FROM FN (nolock) as FNDEST WHERE FNDEST.bistamp = BI.bistamp) ELSE 0 END,0)
		,pvporig	= (convert(numeric(13,3), isnull(fprod.pvporig, 0)))
		,validade = cast(year(st.validade) as varchar(4)) + right('0'+cast(month(st.validade) as varchar(2)),2)
		,epv1 = case when (convert(numeric(13,3), isnull(fprod.pvporig, 0)))=0 then 0 else st.epv1 end
		,st.familia as famiSt
		,bi.*
		
	From 
		BI (nolock)
		inner join bo (nolock) on bi.bostamp = bo.bostamp 
		left join st (nolock) on st.ref = bi.ref
		left join empresa on st.site_nr = empresa.no
		left join fprod (nolock) on fprod.cnp=st.ref
	Where 
		BISTAMP = @stamp
		and bo.site = empresa.site
END

IF @tipo = 'FI'
BEGIN
	Select 
		usalote = ISNULL(st.usalote,convert(bit,0))
		,qttcli = ISNULL(st.qttcli,convert(bit,0))
		,qttmov = ISNULL(CASE WHEN fI.ref <> '' THEN (SELECT SUM(QTT) as QTT FROM FN (nolock) as FNDEST WHERE FNDEST.fistamp = fi.fistamp) ELSE 0 END,0)
		,pvporig	= (convert(numeric(13,3), isnull(fprod.pvporig, 0)))
		,validade = cast(year(st.validade) as varchar(4)) + right('0'+cast(month(st.validade) as varchar(2)),2)
		,epv1 = case when (convert(numeric(13,3), isnull(fprod.pvporig, 0)))=0 then 0 else st.epv1 end
		,ft.fdata
		,st.familia as famiSt
		,epvloja = case when isnull(fi.eslvu,0)=0 then fi.etiliquido else fi.eslvu end
		,fi.*

		
	From 
		FI (nolock)
		inner join ft (nolock) on fi.ftstamp = ft.ftstamp 
		left join st (nolock) on st.ref = fi.ref
		left join empresa on st.site_nr = empresa.no
		left join fprod (nolock) on fprod.cnp=st.ref
	Where 
		FISTAMP = @stamp
		and ft.site = empresa.site
END

GO
Grant Execute On dbo.up_Documentos_importarLinhas to Public
Grant Control On dbo.up_Documentos_importarLinhas to Public
Go



