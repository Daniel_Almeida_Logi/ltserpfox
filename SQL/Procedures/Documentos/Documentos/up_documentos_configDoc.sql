-- Obt�m dados do Documento
-- exec up_documentos_configDoc 'V/Factura', 'Loja 1'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_documentos_configDoc]') IS NOT NULL
	drop procedure dbo.up_documentos_configDoc
go

create procedure dbo.up_documentos_configDoc

@doc as varchar(50),
@site as varchar(10) = ''

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON	
	
	SELECT Top 1
		*
	FROM (
		SELECT
			CONVERT(bit,1) as NRSEQ, 
			NMDOS as NOMEDOC, 
			NDOS as numinternodoc, 
			bdempresas, 
			'BO' as tipoDoc, 
			QPRECOCUSTO, 
			QPRECO, 
			tsstamp as stamp,
			'TS' as ECRAN
			,folansl = stocks
			,TRFA
			,FOSL = CONVERT(bit,0)
			,folanfc = CONVERT(bit,0)
			,armazem
			,site
		FROM
			TS (nolock)
		
		UNION ALL
		
		Select
			NRSEQ, 
			NOMEDOC = CMDESC, 
			cm as numinternodoc, 
			'FL' as bdempresas, 
			'FO' as tipoDoc, 
			QPRECOCUSTO =0, 
			QPRECO = 0, 
			cm1stamp  as stamp,
			'CM1' as ECRAN
			,folansl
			,TRFA = CONVERT(bit,0)
			,FOSL
			,folanfc
			,armazem = 0 
			,'' as site
		FROM
			CM1 (nolock)
		WHERE
			NAOFO = 0
	) a
	WHERE 
		a.NOMEDOC = @doc
		and 1 = (CASE WHEN @site = '' OR a.site = '' THEN 1 ELSE (CASE WHEN a.site <> '' AND a.site = @site THEN 1 END) END)
	ORDER BY
		a.NOMEDOC

GO
Grant Execute On dbo.up_documentos_configDoc to Public
Grant Control On dbo.up_documentos_configDoc to Public
GO