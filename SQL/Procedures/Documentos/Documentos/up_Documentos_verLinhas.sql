-- Ver Linhas de um documento compras/dossiers
-- exec up_Documentos_verLinhas 'ADM10080161391,725011296 '
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_verLinhas]') IS NOT NULL
	drop procedure dbo.up_Documentos_verLinhas
go

create procedure dbo.up_Documentos_verLinhas

@stamp varchar(30)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON	

select	nomedoc	= docnome,
		numdoc	= adoc,
		ref,
		design,
		preco	= fn.epv,
		qtt		= fn.qtt,
		uni2qtt,
		u_bonus,
		desconto, desc2, desc3, desc4,
		u_upc,
		total	= etiliquido
from 
	fn (nolock)
where 
	fostamp=@stamp
union all
select	nomedoc	= nmdos,
		numdoc	= CONVERT(varchar,obrano),
		ref,
		design,
		preco	= bi.edebito,
		qtt		= bi.qtt,
		uni2qtt,
		u_bonus,
		desconto, desc2, desc3, desc4,
		u_upc,
		total	= ettdeb
from 
	bi (nolock)
where
	bostamp=@stamp

GO
Grant Execute On dbo.up_Documentos_verLinhas to Public
Grant Control On dbo.up_Documentos_verLinhas to Public
Go
