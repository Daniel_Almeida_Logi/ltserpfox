-- Procurar Entidade  (Documentos)
-- exec up_documentos_pesquisaEntidades '', 0, '', '', ''
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_documentos_pesquisaEntidades]') IS NOT NULL
	drop procedure dbo.up_documentos_pesquisaEntidades
GO

create procedure dbo.up_documentos_pesquisaEntidades 

@nome varchar(60),
@no numeric(10,0),
@tipo varchar(20),
@tele varchar(60),
@ncont varchar(20)

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

Select	
	sel		= CONVERT(bit,0)
	,nome, no
	,codigo, local, telefone, tlmvl, codpost, ncont, morada
From
	AG (nolock)
Where
	nome like @nome + '%'
	and no = case when @no > 0 then @no else no end
	and codigo like @tipo + '%'
	AND ((telefone like @tele + '%') OR (tlmvl like @tele + '%'))
	AND (ncont like @ncont + '%')
	AND inactivo = 0 
Order by
	nome

GO
Grant Execute On dbo.up_documentos_pesquisaEntidades to Public
Grant Control On dbo.up_documentos_pesquisaEntidades to Public
Go