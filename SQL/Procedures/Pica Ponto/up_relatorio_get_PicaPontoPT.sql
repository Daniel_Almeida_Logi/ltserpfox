/* 
	devolve a informacao dos produtos para a designa��o do pica ponto em portuges

	exec up_relatorio_get_PicaPontoPT 


*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_relatorio_get_PicaPontoPT]') IS NOT NULL
	DROP PROCEDURE dbo.up_relatorio_get_PicaPontoPT
GO

CREATE PROCEDURE dbo.up_relatorio_get_PicaPontoPT

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON
select  distinct  sel = convert(bit,0) , convert(varchar,inOutCode) as id,	
	CASE when inOutCode = 0    then 'Entrada'  
	when inOutCode = 1    then 'Saida'
	when inOutCode = 2    then 'Entrada Almo�o'
	when inOutCode = 3	  then 'Saida Almo�o'
	when inOutCode = 4	  then 'Hora Extra Entrada'
	when inOutCode = 5	  then 'Hora Extra Saida' end as design 
from clock_response_payload_line(nolock) where inOutCode>=0
	

Go
Grant Execute on dbo.up_relatorio_get_PicaPontoPT to Public
Grant Control on dbo.up_relatorio_get_PicaPontoPT to Public
Go