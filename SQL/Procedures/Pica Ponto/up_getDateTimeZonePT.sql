/* SP retorna dataPicaPonto
	
	up_getDatetimeZonePT  '2022-03-27 08:25:39','724'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_getDatetimeZonePT]') IS NOT NULL
	DROP PROCEDURE dbo.up_getDatetimeZonePT
GO

CREATE PROCEDURE dbo.up_getDatetimeZonePT
	 @input_date	datetime
	,@countryCode   varchar(30)
AS

SET NOCOUNT ON

declare @summerTime	varchar(5)
declare @winterTime varchar(5)
declare @mes int
declare @last_day_of_the_month  date
declare @last_sunday_of_the_month date
DECLARE @dateFinal datetime
set DATEFIRST 7 -- to be on the safe side
-- Get last day of the especif month
select @last_day_of_the_month = dateadd(dd,datepart(dd,@input_date)*-1,dateadd(mm,1,@input_date))
-- get last sunday of the month
select @last_sunday_of_the_month = dateadd(dd,(datepart(dw,@last_day_of_the_month)*-1)+1,@last_day_of_the_month)
-- get the espefic month 
select @mes = month(@input_date)

-- from the country code i get the winter and summer Time
SELECT @summerTime= summerTime, @winterTime = winterTime  FROM timeZones(nolock) where countryCode = @countryCode


-- Summer Time
IF((@mes > 3  and @mes < 10) or (@mes = 3 and @input_date >= @last_sunday_of_the_month) or (@mes = 10 and @input_date < @last_sunday_of_the_month) )
BEGIN
	IF(@summerTime >= 0)
		BEGIN
			set @dateFinal =   DATEADD(hour, +Cast(@summerTime AS int),@input_date) 
		END
	ELSE
		BEGIN 
			set @dateFinal =   DATEADD(hour, CAST(@summerTime AS int),@input_date) 
		END

END

-- Winter Time
IF(((@mes >= 1 and @mes < 3)  or (@mes > 10 and @mes <= 12)) or (@mes = 3 and @input_date < @last_sunday_of_the_month) or (@mes = 10 and @input_date >= @last_sunday_of_the_month) )
	IF(@summerTime >= 0)
		BEGIN

			set @dateFinal =   DATEADD(hour, +Cast(@winterTime AS int),@input_date) 
		END
	ELSE
		BEGIN 
			set @dateFinal =   DATEADD(hour, +Cast(@winterTime AS int),@input_date)
		END

	select @dateFinal AS DATAFINAL


GRANT EXECUTE on dbo.up_getDatetimeZonePT TO PUBLIC
GRANT Control on dbo.up_getDatetimeZonePT TO PUBLIC
GO


