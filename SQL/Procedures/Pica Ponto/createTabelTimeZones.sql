IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'timeZones'))
BEGIN

	CREATE TABLE [dbo].[timeZones](
		[countryCode]				[varchar](36) NOT NULL,
		[Alpha2Code]				[varchar](2) NOT NULL,
		[Alpha3Code]				[varchar](3),
		[country]					[varchar](50),
		[summerTime]				[numeric](4,0),
		[winterTime]				[numeric](4,0),
		[ousrdata]					[datetime],
		[ousrinis]					[varchar](30),
		[usrdata]					[datetime],
		[usrinis]					[varchar](30)

	 CONSTRAINT [PK_timeZones] PRIMARY KEY CLUSTERED 
	(
		[countryCode] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[timeZones] ADD  CONSTRAINT [DF_timeZones_countryCode]		DEFAULT ('') FOR [countryCode]

	ALTER TABLE [dbo].[timeZones] ADD  CONSTRAINT [DF_timeZones_Alpha2Code]			DEFAULT ('') FOR [Alpha2Code]
	
	ALTER TABLE [dbo].[timeZones] ADD  CONSTRAINT [DF_timeZones_Alpha3Code]			DEFAULT ('') FOR [Alpha3Code]
	
	ALTER TABLE [dbo].[timeZones] ADD  CONSTRAINT [DF_timeZones_country]			DEFAULT ('') FOR [country]
		
	ALTER TABLE [dbo].[timeZones] ADD  CONSTRAINT [DF_timeZones_summerTime]			DEFAULT (0) FOR [summerTime]
	
	ALTER TABLE [dbo].[timeZones] ADD  CONSTRAINT [DF_timeZones_winterTime]			DEFAULT (0) FOR [winterTime]
	
	ALTER TABLE [dbo].[timeZones] ADD  CONSTRAINT [DF_timeZones_ousrdata]		DEFAULT (getdate()) FOR [ousrdata]
	
	ALTER TABLE [dbo].[timeZones] ADD  CONSTRAINT [DF_timeZones_ousrinis]		DEFAULT ('') FOR [ousrinis]
	
	ALTER TABLE [dbo].[timeZones] ADD  CONSTRAINT [DF_timeZones_usrdata]		DEFAULT (getdate()) FOR [usrdata]
	
	ALTER TABLE [dbo].[timeZones] ADD  CONSTRAINT [DF_timeZones_usrinis]		DEFAULT ('') FOR [usrinis]

END	



IF (NOT EXISTS (SELECT * FROM timeZones WHERE countryCode = 724 and Alpha2Code = 'ES' ))
BEGIN
		
INSERT INTO [dbo].[timeZones]([countryCode], [Alpha2Code], [Alpha3Code], [country], [summerTime], [winterTime],
							 [ousrdata], [ousrinis], [usrdata], [usrinis])			
SELECT '724','ES','ESP','Spain',-0,+1,getdate(),'',getdate(),''
END

IF (NOT EXISTS (SELECT * FROM timeZones WHERE countryCode = 208 and Alpha2Code = 'DK' ))
BEGIN
INSERT INTO [dbo].[timeZones]([countryCode], [Alpha2Code], [Alpha3Code], [country],[summerTime], [winterTime],
							[ousrdata], [ousrinis], [usrdata], [usrinis])			
SELECT '208','DK','DNK','Denmark',-2,-3,getdate(),'',getdate(),''
END