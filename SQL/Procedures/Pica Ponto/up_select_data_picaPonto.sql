/* SP retorna dataPicaPonto
	
	up_select_data_picaPonto 1, '2022-03-25 08:25:39'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_select_data_picaPonto]') IS NOT NULL
	DROP PROCEDURE dbo.up_select_data_picaPonto
GO

CREATE PROCEDURE dbo.up_select_data_picaPonto
	 @siteNr		int,
	 @data          datetime 

AS

SET NOCOUNT ON

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#finalSet'))
		DROP TABLE #finalSet

	create table #finalSet(
		dataFinal datetime
	)
	declare @idPicaPonto varchar(20) = ''
	declare @dateFinal datetime 
	declare @teste datetime

	DECLARE @site varchar(18) = ''
	SET @site = (select  isnull(site,'') from empresa(nolock) where no = @siteNr)	
				print @data
	select 
		top 1
		@idPicaPonto=rtrim(ltrim(isnull(textValue,''))) 
	from B_Parameters_site(nolock) 
	where
		stamp = 'ADM0000000095' 
		and bool = 1
		and site = @site

	set @idPicaPonto = isnull(@idPicaPonto,'')

	if(LOWER(@idPicaPonto)='zkteko')
	begin
	
		INSERT INTO #finalSet
			exec up_getDatetimeZonePT @data,'724'
		set @dateFinal = (select * from #finalSet)
	end else begin
		set @dateFinal = @data
	end		
	

	select dataFinal = convert(varchar,@dateFinal,120)

IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#finalSet'))
	DROP TABLE #finalSet
	
GO
GRANT EXECUTE on dbo.up_select_data_picaPonto TO PUBLIC
GRANT Control on dbo.up_select_data_picaPonto TO PUBLIC
GO


