/*
	retorna resultado da operacao
	exec [dbo].[up_tpa_resultadoOp] 'BALBLA'

*/




SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_tpa_resultadoOp]') IS NOT NULL
	drop procedure dbo.up_tpa_resultadoOp
go

create procedure dbo.up_tpa_resultadoOp

@token varchar(25) 

/* WITH ENCRYPTION */
AS

	select 
		top 1
		tpa_msg.code
		,tpa_msg.msg
		,"StatusTpa" = tpa_msg_d.posStatusDescription
		,"opMessage" = tpa_msg_d.operatorMessage
	from 
		tpa_msg(nolock) 

	left join 
		tpa_msg_d(nolock) on tpa_msg_d.token=tpa_msg.token
	WHERE
		tpa_msg.token=@token
	order by 
		tpa_msg.ousrdata desc
	


GO
Grant Execute On dbo.up_tpa_resultadoOp to Public
Grant Control On dbo.up_tpa_resultadoOp to Public
Go













