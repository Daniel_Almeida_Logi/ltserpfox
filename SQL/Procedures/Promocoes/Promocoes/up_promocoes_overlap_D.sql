---- exec	up_promocoes_overlap_d '','', 'Loja 1', '20110331', '20110331'
--SET ANSI_NULLS OFF
--GO
--SET QUOTED_IDENTIFIER OFF
--GO

--if OBJECT_ID('[dbo].[up_promocoes_overlap_d]') IS NOT NULL
--	drop procedure dbo.up_promocoes_overlap_d
--go



--Create procedure dbo.up_promocoes_overlap_d

--@cstamp		varchar(28),
--@ref		varchar(18),
--@Loja		varchar(10),
--@Dataini	datetime,
--@Datafim	datetime

--/* WITH ENCRYPTION */
--AS
--SET NOCOUNT ON

--Select	B_PromoD.ref as stref, B_PromoD.descr as stdescr,B_PromoC.ref,B_PromoC.design,B_promoC.tipoDescr,B_PromoC.dataIni,B_promoC.dataFim,case when B_promoC.Overlap=1 then 'Sim' else 'N�o' END as Overlap
--From	B_promoC (nolock)
--inner join B_promoD (nolock) on B_promoC.cStamp = B_promoD.cStamp
--Where	status = 'A'
--		And B_promoC.dataIni between  @Dataini and @Datafim
--		And B_promoC.datafim between  @Dataini and @Datafim
--		and B_promoC.cStamp != @cstamp
--		and B_promoC.loja = @Loja
--		AND B_promoD.ref = @ref
		

--GO

--Grant execute on dbo.up_promocoes_overlap_D to public
--Grant control on dbo.up_promocoes_overlap_D to public