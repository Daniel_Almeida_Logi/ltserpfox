-- exec up_promocoes_overlap_o '','7002501', '01', '20110406','20110406'

--SET ANSI_NULLS OFF
--GO
--SET QUOTED_IDENTIFIER OFF
--GO

--if OBJECT_ID('[dbo].[up_promocoes_overlap_o]') IS NOT NULL
--	drop procedure dbo.up_promocoes_overlap_o
--go


--Create procedure dbo.up_promocoes_overlap_o

--@cstamp		varchar(28),
--@ref		varchar(18),
--@Loja		varchar(10),
--@Dataini	datetime,
--@Datafim	datetime

--/* WITH ENCRYPTION */
--AS
--SET NOCOUNT ON

--Select	B_PromoO.ref as stref, B_PromoO.descr as stdescr,B_PromoC.ref,B_PromoC.design,B_promoC.tipoDescr,B_PromoC.dataIni,B_promoC.dataFim,case when B_promoC.Overlap=1 then 'Sim' else 'N�o' END as Overlap
--From	B_promoC (nolock)
--inner join B_promoO (nolock) on B_promoC.cStamp = B_promoO.cStamp
--Where	status = 'A'
--		And B_promoC.dataIni between  @Dataini and @Datafim
--		And B_promoC.datafim between  @Dataini and @Datafim
--		and B_promoC.cStamp != @cstamp
--		and B_promoC.loja = @Loja
--		AND B_promoO.ref = @ref
		
--GO

--Grant execute on dbo.up_promocoes_overlap_o to public
--Grant control on dbo.up_promocoes_overlap_o to public