-- Pesquisa de Promo��es
-- exec up_promocoes_pesquisa 100,'','','19000101','30000101',0
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_promocoes_pesquisa]') IS NOT NULL
    drop procedure dbo.up_promocoes_pesquisa
go

create procedure dbo.up_promocoes_pesquisa

@top		int,
@ref		varchar(7),
@design		varchar(200),
@de			datetime,
@a			datetime,
@status		bit,
@site		varchar(60)

/* WITH ENCRYPTION */
AS

SELECT  TOP (@top)
    CONVERT(bit,0) as sel
    ,'Ref'		= B_promoC.ref
    ,'Design'	= B_promoC.design
    ,'ValEntre'	= convert(varchar,B_promoC.dataIni,105) + ' a ' + convert(varchar,B_promoC.dataFim,105)
    ,'Estado'	= B_promoC.status
    ,'Tipo'		= B_promoC.tipo
from B_promoC (nolock)
where
	B_promoC.ref like @ref+'%'
    and B_promoC.design like @design+'%'
    and B_promoC.dataIni >= @de 
    and B_promoC.dataFim <= @a
    and B_promoC.status = (case when @status = 1 then 'Activo' else B_promoC.status end)
	and B_promoC.site = @site
	
GO
Grant Execute on dbo.up_promocoes_pesquisa to Public
Grant Control on dbo.up_promocoes_pesquisa to Public
GO