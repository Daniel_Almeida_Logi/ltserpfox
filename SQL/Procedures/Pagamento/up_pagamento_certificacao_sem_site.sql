

/****** Object:  StoredProcedure [dbo].[up_pagamento_certificacao_sem_site]    Script Date: 04/02/2020 12:12:19 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

create procedure [dbo].[up_pagamento_certificacao_sem_site]		
	@stamp		 varchar(30)
	,@date		 varchar(20)
	,@time		 varchar(8)
	,@ndoc		 numeric(5)
	,@total		 varchar(30)
	,@site		 varchar(20)
	,@fno		 numeric(9)	= 0
	,@docId		 varchar(2)	= 'FT'
As
	declare @docType		varchar(10);			-- Tipo de Documento (FR; FT; NC)
	declare @docNr			varchar(40);			-- n� doc + ndoc - corresponde ao InvoceNo
	declare @hash			varchar(240);			-- hash antiga
	declare @newHash		varchar(240);			-- hash nova
	declare @version		varchar(40) = '1.00000' -- versao certificacao est�tico aqui
	declare @docNrAnt		varchar(40)				-- N� Ultima Venda Guardada em sistema
	declare @docPreffix		varchar(1)=''
	-- Define Tipo de Doc 
	-- 1 - TD configura��o documentos fatura��o
	-- 2 - TS configura��o dossies internos
	-- 3 - CM1 configura��o movimentos (mercadorias/valores) (inclui configura��o dos recibos)
	---Tabela RE
	IF @docId='RE'
	begin
		IF @fno<1
				set @fno = (select rno from re (nolock) where restamp = @stamp); 
			set @docType = (select tiposaft from cm1 (nolock) where cm = @ndoc); 
			set @docPreffix='R';
	end	else begin
		if @docId='BO' -- Tabela BO
		begin	
			IF @fno<1
				set @fno = (select obrano from bo (nolock) where bostamp = @stamp);
			set @docType = (select tiposaft from ts (nolock) where ndos = @ndoc); 
			set @docPreffix='B'
		end else begin -- Tabela FT
			IF @fno<1
				set @fno = (select fno from ft (nolock) where ftstamp = @stamp);
			set @docType = (select top 1 tiposaft from td (nolock) where ndoc = @ndoc);
		end
	end
	--apenas executa se o campo tiposaft estiver preenchido
	if(len(@docType)>1)
	begin
		-- Criar Novo Invoice No
		set @docNr = @docPreffix + convert(varchar(10),@ndoc) + '/' + convert(varchar(10),@fno)
		set @docNrAnt = @docPreffix + convert(varchar(10),@ndoc) + '/' + convert(varchar(10),@fno-1)
		-- Usa hash documento anterior para calculo da nova hash - 
		set @hash  = ISNULL(
						(select 
							hash = c.hash
						 from 
							B_cert c (nolock)
						 where 
							YEAR(c.date)  = YEAR(@DATE)
							and c.invoiceNo = @docNrAnt
							and c.site = @site
							and  c.invoiceType = @docType
							/* condi��o aqui tem de ser revista, RC n�o entram para SAFT pq � que s�o certificados ?? */
						--	and c.invoicetype != 'RC'\
						)
						,'')
		-- Altera data para o formato "obrigat�rio"
		set @date = SUBSTRING(@date,1,4) + '-' + SUBSTRING(@date,5,2) + '-'+ SUBSTRING(@date,7,2) 
		-- Calcula Nova Hash
		set @newHash =  @date +';'+ @time +';'+ @docNr +';'+ @total +';'+ @hash
		--Insere dados na tabela de certificacao
		insert into b_cert
			(stamp, date, systemDate, systemTime
	        ,invoiceType, invoiceNo, total
	        ,hash
	        ,site, versaochave)
		values
			(@stamp, @date, @date, @time
	        ,@docType, @docNr, @total
	        ,dbo.up_gerarhash(@newhash)
	        ,@site, @version)
	end
GO


