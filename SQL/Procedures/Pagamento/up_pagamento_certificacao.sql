 /* SP Certificacao Vendas Feitas Atendimento - corre no pagamento
	
	exec up_pagamento_certificacao 'ADM93A12D70-145D-4F18-AEE', '20151207', '18:28:48', 3, 'FT', 2.790000, 'Loja 1', 65

	select ISNULL(MAX(ft.fno),0) + 1 from ft where ft.ftano = Year('20151204') and ft.ndoc= 3

	exec up_pagamento_certificacao 'ADM9E31B76F-3366-4B46-A80', '20190626', '09:08:08', 1, '2.790000', 'Loja 1', 252
	
 */
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO





if OBJECT_ID('[dbo].[up_pagamento_certificacao]') IS NOT NULL
	drop procedure dbo.up_pagamento_certificacao
go
		

create procedure dbo.up_pagamento_certificacao		
	@stamp		 varchar(30)
	,@date		 varchar(20)
	,@time		 varchar(8)
	,@ndoc		 numeric(6)
	,@total		 varchar(30)
	,@site		 varchar(20)
	,@fno		 numeric(9)	= 0
	,@docId		 varchar(2)	= 'FT'

As
	declare @docType		varchar(10);			-- Tipo de Documento (FR; FT; NC)
	declare @docNr			varchar(40);			-- n� doc + ndoc - corresponde ao InvoceNo
	declare @hash			varchar(240);			-- hash antiga
	declare @newHash		varchar(240);			-- hash nova
	declare @version		varchar(40) = '1.00000' -- versao certificacao est�tico aqui
	declare @docNrAnt		varchar(40)				-- N� Ultima Venda Guardada em sistema
	declare @docPreffix		varchar(1)=''

	

	-- Define Tipo de Doc 
	-- 1 - TD configura��o documentos fatura��o
	-- 2 - TS configura��o dossies internos
	-- 3 - CM1 configura��o movimentos (mercadorias/valores) (inclui configura��o dos recibos)

	---Tabela RE
	IF @docId='RE'
	begin
		IF @fno<1
				set @fno = (select rno from re (nolock) where restamp = @stamp); 
			
			set @docType = (select tiposaft from cm1 (nolock) where cm = @ndoc); 
			set @docPreffix='R';

	end	else begin
		if @docId='BO' -- Tabela BO
		begin	
		
			IF @fno<1
				set @fno = (select obrano from bo (nolock) where bostamp = @stamp);

			set @docType = (select tiposaft from ts (nolock) where ndos = @ndoc); 
			set @docPreffix='B'
			
		end else begin -- Tabela FT
			IF @fno<1
				set @fno = (select fno from ft (nolock) where ftstamp = @stamp);
		

			set @docType = (select tiposaft from td (nolock) where ndoc = @ndoc AND site = @site);
		end
	end



	--apenas executa se o campo tiposaft estiver preenchido
	if(len(@docType)>1)
	begin
		

		-- Criar Novo Invoice No
		set @docNr = @docPreffix + convert(varchar(10),@ndoc) + '/' + convert(varchar(10),@fno)
		set @docNrAnt = @docPreffix + convert(varchar(10),@ndoc) + '/' + convert(varchar(10),@fno-1)


	

		-- Usa hash documento anterior para calculo da nova hash - 
		set @hash  = ISNULL(
						(select top 1
							hash = c.hash
						 from 
							B_cert c (nolock)
						 where 
							YEAR(c.date)  = YEAR(@DATE)
							and c.invoiceNo = @docNrAnt
							and c.site = @site
							and  c.invoiceType = @docType
							/* condi��o aqui tem de ser revista, RC n�o entram para SAFT pq � que s�o certificados ?? */
						--	and c.invoicetype != 'RC'\
						)
						,'')
	
	
		-- Altera data para o formato "obrigat�rio"
		set @date = SUBSTRING(@date,1,4) + '-' + SUBSTRING(@date,5,2) + '-'+ SUBSTRING(@date,7,2) 
	
		-- Calcula Nova Hash
		set @newHash =  @date +';'+ @time +';'+ @docNr +';'+ @total +';'+ @hash


		
		--Insere dados na tabela de certificacao
		insert into b_cert
			(stamp, date, systemDate, systemTime
	        ,invoiceType, invoiceNo, total
	        ,hash
	        ,site, versaochave)
		values
			(@stamp, @date, @date, @time
	        ,@docType, @docNr, @total
	        ,dbo.up_gerarhash(@newhash)
	        ,@site, @version)
	
	end






GO
Grant Execute On dbo.up_pagamento_certificacao to Public
Grant control On dbo.up_pagamento_certificacao to Public
GO



			
	
			

	
	