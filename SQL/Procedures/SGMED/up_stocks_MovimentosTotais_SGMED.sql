/****** Object:  StoredProcedure [dbo].[up_stocks_MovimentosTotais_SGMED]    Script Date: 19/02/2025 12:08:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

/****** Object:  StoredProcedure [dbo].[up_stocks_MovimentosTotais_SGMED]    Script Date: 08/11/2024 18:00:32 ******/

ALTER procedure [dbo].[up_stocks_MovimentosTotais_SGMED]
@armazem numeric(5,0),
@dataIni datetime,
@dataFim datetime,
@modo bit,
@lote varchar(30),
@movimento varchar(15)='Todos'
/* WITH ENCRYPTION */
AS
	If OBJECT_ID('tempdb.dbo.#SaldoAnterior') IS NOT NULL
		drop table #SaldoAnterior
	If OBJECT_ID('tempdb.dbo.#TempMovimentos') IS NOT NULL
		drop table #TempMovimentos
		If OBJECT_ID('tempdb.dbo.#tempsl') IS NOT NULL
		drop table #tempsl
	select 
		sl.ref
		,saldoAnterior = SUM(Case When cm <50 Then qtt Else - qtt End)
		,entradasAnterior = sum(case 
									when cm < 50 then 
										case when qtt < 0 then 0 else round(qtt,0) end
										else case when qtt < 0 and cm > 50 then	qtt * -1 else round(0,0) end
								end
							)
		,saidasAnterior = sum(case 
									when cm > 50 and qtt > 0 
										then round(qtt,0) 
										else case 
												when cm < 50 and qtt < 0
													then ROUND(qtt*-1,0)
													else round(0,0) 
											end
										end
							)
	into 
		#SaldoAnterior	
	from 
		sl (nolock) 
	WHere 
		sl.datalc <  @dataIni 
		and sl.armazem = @armazem  
		and sl.lote like case when @lote = '' then sl.lote else @lote + '%' end	
	group by ref	

	SELECT
		ref
		,sl.cm
		,sl.slstamp
		,ROW_NUMBER() over(order by sl.ref,  sl.datalc asc, sl.ousrhora asc) as numero
		,qtt
	Into
		#TempMovimentos
	FROM
		sl (nolock)
	WHERE
		 sl.datalc between  @dataIni and @dataFim
		and sl.armazem = @armazem
		and sl.lote like case when @lote = '' then sl.lote else @lote + '%' end
	group by ref , sl.cm,sl.slstamp,qtt,sl.datalc, sl.ousrhora

	SELECT 

		SALDO				= isnull((select SALDOANTERIOR from #SaldoAnterior where #SaldoAnterior.ref= b.ref),0) + 
							Case when @modo=1
								then
									isnull((
												Select sum(case when a.cm < 50 then a.qtt else - a.qtt end) 
												From   (select * from #TempMovimentos where #TempMovimentos.ref = b.ref)a
														inner join sl (nolock) on sl.slstamp = a.slstamp and a.ref= sl.ref
														Where a.numero <= b.numero
										   ),0)
								else
									0 
							End
		,ENTRADAS			= 
							case when b.cm < 50 
								then 
									case when b.qtt < 0
										then
											0
										else
											round(b.qtt,0) 
										end
								else 
									case when b.qtt < 0 and b.cm > 50
										then
											0
										else
											b.qtt * -1 
										end
								end
		,SAIDAS				= 
							case when b.cm > 50 and b.qtt < 0
								then 
									ROUND(b.qtt*-1,0) 
								else 
									case when b.cm < 50 and b.qtt < 0
										then
											ROUND(b.qtt,0)
										else
											round(0,0) 
										end
								end
		,*
	into #dadossl
	From (	
		select
			ROW_NUMBER() over(order by a.ref, a.datalc asc, a.ousrhora asc) as numero
			,ISNULL(Case When a.cm <50 Then a.qtt Else - a.qtt End,0) as qtt
			,ISNULL(a.ref,'') as ref
			,ISNULL(a.design ,'') as design
			,ISNULL(slstamp,'') as slstamp
			,ISNULL(convert(varchar,datalc,102),'') as datalc
			,ISNULL(cm,'') as cm
			,ISNULL(cmdesc,'') as cmdesc
			,ISNULL(adoc,'') as adoc
			,ISNULL(nome + (case when a.bistamp='' then '' else (' - '+ (select codext from bo2 (nolock) where bo2.bo2stamp=(select bostamp from bi (nolock) where bi.bistamp=a.bistamp))) end),'') as nome
			,ISNULL(a.armazem,0) as armazem
			,pcl		= ISNULL(case when a.fistamp<>'' then	
							(select ecusto from fi (nolock) where fi.fistamp=a.fistamp)
						else
							case when a.fnstamp<>'' then
								(select u_upc from fn (nolock) where fn.fnstamp=a.fnstamp)
							else
								(select epu from bi (nolock) where bi.bistamp=a.bistamp)
							end 
						end ,0)
			,pcltotal	 = ISNULL((case when a.fistamp<>'' then	
							(select ecusto from fi (nolock) where fi.fistamp=a.fistamp)
						else
							case when a.fnstamp<>'' then
								(select u_upc from fn (nolock) where fn.fnstamp=a.fnstamp)
							else
								(select epu from bi (nolock) where bi.bistamp=a.bistamp)
							end 
						end )*a.qtt,0)
			,ISNULL(epcpond,0) as PCP
			,ISNULL((epcpond*a.qtt),0) as PCPTOTAL
			,ISNULL(((u_epvp-(u_epvp*(desconto/100)))/(1+(iva/100)))*a.qtt,0) as 'PV'
			,a.ousrinis
			,a.ousrhora
			,a.ousrdata
			,CONVERT(bit,0) as verdoc
			,origem 
			,ISNULL(a.fistamp,'') as fistamp
			,ISNULL(a.bistamp,'') as bistamp
			,ISNULL(a.fnstamp,'') as fnstamp
			,ISNULL(a.sticstamp,'') as sticstamp
			,ISNULL((select top 1 username from b_us where a.ousrinis=b_us.iniciais),'') as username
			,ISNULL(a.lote,'') as lote
		from
			sl (nolock) a
				left join fi (nolock) on a.fistamp = fi.fistamp
		where
			 a.datalc between @dataIni and @dataFim
			and a.armazem = @armazem
			and a.lote like case when @lote = '' then a.lote else @lote + '%' end
		) b
	order by
		 ref asc,
		datalc asc,
		ousrhora asc
	/* Elimina entradas se tipo de movimentos=entradas*/
	if @movimento like '%Entradas%' 
	delete from #dadossl where saidas>0
	/* Elimina saídas se tipo de movimentos=saídas*/
	if @movimento like '%Saídas%' 
	delete from #dadossl where entradas>0
	
	SELECT 
	'Saldo'				 AS 'Saldo',
	'Entradas'			 AS 'Entradas',
	'Saidas'			 AS 'Saidas',
    0		             AS 'numero',
    'qtt'                AS 'qtt',
    'ref'                AS 'ref',
    'design'             AS 'design',
    'slstamp'            AS 'slstamp',
    'datalc'             AS 'datalc',
    'cm'                 AS 'cm',
    'cmdesc'             AS 'cmdesc',
    'adoc'               AS 'adoc',
    'nome'               AS 'nome',
    'armazem'            AS 'armazem',
    'pcl'                AS 'pcl',
    'pcltotal'           AS 'pcltotal',
    'PCP'                AS 'PCP',
    'PCPTOTAL'           AS 'PCPTOTAL',
    'PV'                 AS 'PV',
    'ousrinis'           AS 'ousrinis',
    'ousrhora'           AS 'ousrhora',
    'ousrdata'           AS 'ousrdata',
    'verdoc'             AS 'verdoc',
    'origem'             AS 'origem',
    'fistamp'            AS 'fistamp',
    'bistamp'            AS 'bistamp',
    'fnstamp'            AS 'fnstamp',
    'sticstamp'          AS 'sticstamp',
    'username'           AS 'username',
    'lote'               AS 'lote',
    'Motivo quebra'      AS 'Motivo quebra',
    'Familia'            AS 'Familia',
    'Margem Venda AOA'   AS 'Margem Venda AOA',
    'Margem Venda %'     AS 'Margem Venda %',
    'Margem atual AOA'   AS 'Margem atual AOA',
    'Margem atual %'     AS 'Margem atual %'


	UNION ALL

	select  CONVERT(varchar(100),#dadossl.SALDO)												AS 'Saldo',
			CONVERT(varchar(100),#dadossl.ENTRADAS)												AS 'Entradas',
			CONVERT(varchar(100),#dadossl.SAIDAS)												AS 'Saidas',
			#dadossl.numero																		AS 'numero',
			CONVERT(varchar(100),#dadossl.qtt)													AS 'qtt',
			CONVERT(varchar(100),#dadossl.ref)													AS 'ref',
			CONVERT(varchar(100),#dadossl.design)												AS 'design',
			CONVERT(varchar(100),#dadossl.slstamp)												AS 'slstamp',
			CONVERT(varchar(100),#dadossl.datalc)												AS 'datalc',
			CONVERT(varchar(100),#dadossl.cm)													AS 'cm',
			CONVERT(varchar(100),#dadossl.cmdesc)												AS 'cmdesc',
			CONVERT(varchar(100),#dadossl.adoc)													AS 'adoc',
			CONVERT(varchar(100),#dadossl.nome)													AS 'nome',
			CONVERT(varchar(100),#dadossl.armazem)												AS 'armazem',
			CONVERT(varchar(100),#dadossl.pcl)													AS 'pcl',
			CONVERT(varchar(100),#dadossl.pcltotal)												AS 'pcltotal',
			CONVERT(varchar(100),#dadossl.PCP)													AS 'PCP',
			CONVERT(varchar(100),#dadossl.PCPTOTAL)												AS 'PCPTOTAL',
			CONVERT(varchar(100),#dadossl.PV)													AS 'PV',
			CONVERT(varchar(100),#dadossl.ousrinis)												AS 'ousrinis',
			CONVERT(varchar(100),#dadossl.ousrhora)												AS 'ousrhora',
			CONVERT(varchar(100),#dadossl.ousrdata)												AS 'ousrdata',
			CONVERT(varchar(100),#dadossl.verdoc)												AS 'verdoc',
			CONVERT(varchar(100),#dadossl.origem)												AS 'origem',
			CONVERT(varchar(100),#dadossl.fistamp)												AS 'fistamp',
			CONVERT(varchar(100),#dadossl.bistamp)												AS 'bistamp',
			CONVERT(varchar(100),#dadossl.fnstamp)												AS 'fnstamp',
			CONVERT(varchar(100),#dadossl.sticstamp)											AS 'sticstamp',
			CONVERT(varchar(100),#dadossl.username)												AS 'username',
			CONVERT(varchar(100),#dadossl.lote)													AS 'lote',
			CONVERT(varchar(100),ISNULL(u_mquebra,''))											AS 'Motivo quebra',
			CONVERT(varchar(100),ISNULL(faminome,''))											AS 'Familia',	
			CONVERT(varchar(15),ISNULL(CASE												   
					WHEN PCPTOTAL >0 and PV > 0												  
						THEN CAST((PV-PCPTOTAL) as decimal (12,2))							 
																							   
					WHEN PCPTOTAL <0 and PV < 0
						THEN CAST((PV-PCPTOTAL) as decimal (12,2))

					WHEN PCPTOTAL = 0 and pcltotal < 0 and PV < 0 --nos casos em que o PCP é zero mas ha pcl negativo, faz margem com pcl
						THEN CAST((PV-pcltotal) as decimal (12,2))

					WHEN PCPTOTAL <=0 and PV >= 0 and pcltotal >= 0 --nos casos de PCP negativo mas com pcl em condições, faz margem com pcl
						THEN CAST((PV-pcltotal) as decimal (12,2))
					ELSE
						CAST((PV-PCPTOTAL) as decimal (12,2))

				END	,0))																			as 'Margem Venda AOA'
			,CONVERT(varchar(15),ISNULL(CASE 
					WHEN PCPTOTAL != 0
						THEN CAST(((PV-PCPTOTAL)/PCPTOTAL)*100 AS decimal (12,2))
					ELSE 100							
				END,0))																				as 'Margem Venda %' 
			,CONVERT(varchar(15),ISNULL(marg1,0))													as 'Margem atual AOA'
			,CONVERT(varchar(15),ISNULL(marg3,0))													as 'Margem atual %' 

	 from #dadossl
	 inner join st (nolock)
				on #dadossl.ref = st.ref
	 left join bi (nolock) on #dadossl.bistamp = bi.bistamp
		 where site_nr = @armazem
		 order by     
			numero ASC

	If OBJECT_ID('tempdb.dbo.#SaldoAnterior') IS NOT NULL
		drop table #SaldoAnterior
	If OBJECT_ID('tempdb.dbo.#TempMovimentos') IS NOT NULL
		drop table #TempMovimentos
		If OBJECT_ID('tempdb.dbo.#tempsl') IS NOT NULL
		drop table #tempsl

