/* SP Listagem Stocks especial para o Grupo SGMED
João Barbosa, 2024-10-08
--------------------------------

Pesquisa nas tabelas: 	
 ST

OUTPUT: 
---------
Loja	ref			design									stock	pvp				validade				pcp				pcl				pct
1		7755231     019-ESTABILIZADOR TORNOZELO C/PLACAS	3.000	39872.140000	2049-12-31 00:00:00.000	29534.920000	29534.920000	29534.920000

EXECUÇÃO DA SP:
---------------
exec up_stocks_SGMED 


*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_stocks_SGMED]') IS NOT NULL
    DROP PROCEDURE [dbo].[up_stocks_SGMED];
GO

CREATE PROCEDURE [dbo].[up_stocks_SGMED]
AS
BEGIN
    -- Subquery com coluna auxiliar para ordenação
    SELECT 
        Loja,
        REF,
        DESIGN,
        STOCK,
        PVP,
        validade,
        PCP,
        PCL,
        PCT
    FROM (
        -- Headers com prioridade 0
        SELECT 
            0                 AS OrderColumn, -- Coluna auxiliar para ordenar
            'LOJA'            AS Loja,
            'REF'             AS REF,
            'DESIGN'          AS DESIGN,
            'STOCK'           AS STOCK,
            'PVP'             AS PVP,
            'validade'        AS validade,
            'PCP'             AS PCP,
            'PCL'             AS PCL,
            'PCT'             AS PCT
        UNION ALL
        -- Dados reais com prioridade 1
        SELECT 
            1                 AS OrderColumn,
            CONVERT(VARCHAR, site_nr)   AS Loja,
            CONVERT(VARCHAR, ref)       AS REF,
            design                      AS DESIGN,
            CONVERT(VARCHAR, stock)     AS STOCK,
            CONVERT(VARCHAR, epv1)      AS PVP,
            CONVERT(VARCHAR, validade)  AS validade,
            CONVERT(VARCHAR, epcpond)   AS PCP,
            CONVERT(VARCHAR, epcult)    AS PCL,
            CONVERT(VARCHAR, epcusto)   AS PCT
        FROM 
            st (NOLOCK)
    ) AS OrderedData
    ORDER BY 
        OrderColumn ASC, -- Headers no topo
        Loja ASC, 
        DESIGN ASC;      -- Dados reais ordenados por Loja e DESIGN
END;
GO

