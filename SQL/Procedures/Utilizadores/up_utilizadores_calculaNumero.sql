/*
	
	exec up_utilizadores_calculaNumero

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_utilizadores_calculaNumero]') IS NOT NULL
	drop procedure dbo.up_utilizadores_calculaNumero
go

create procedure dbo.up_utilizadores_calculaNumero

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	Select 
		userno = isnull(max(userno),0)+1
	From
		b_us (nolock) 	
	
		
GO
Grant Execute On dbo.up_utilizadores_calculaNumero to Public
Grant Control On dbo.up_utilizadores_calculaNumero to Public
GO
