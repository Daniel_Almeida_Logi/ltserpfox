/*

	 exec up_utilizadores_Convencoes 224

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_utilizadores_Convencoes]') IS NOT NULL
	drop procedure dbo.up_utilizadores_Convencoes
go

create procedure dbo.up_utilizadores_Convencoes
@userno as numeric(5,0)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	select 
		cpt_conv.descr, cpt_conv.site, data_cri
	from 
		cpt_conv_us
		inner join cpt_conv on cpt_conv.id = cpt_conv_us.id_cpt_conv
	where
		cpt_conv_us.id_us = @userno
	Order by
		cpt_conv.descr, cpt_conv.site
			
GO
Grant Execute On dbo.up_utilizadores_Convencoes to Public
Grant Control On dbo.up_utilizadores_Convencoes to Public
GO
