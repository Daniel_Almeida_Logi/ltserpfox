/**
 exec up_utilizadores_dadosAtualizarCentral 'po'
**/
--code to validate if the procedure exists, if exists, drops procedure
if OBJECT_ID('[dbo].[up_utilizadores_dadosAtualizarCentral]') IS NOT NULL
	drop procedure dbo.up_utilizadores_dadosAtualizarCentral
go

CREATE PROCEDURE up_utilizadores_dadosAtualizarCentral
	@iniciais VARCHAR(3)

AS
BEGIN

	SELECT 
		grupo ,ugstamp ,inactivo ,supermsg ,superact ,d500 ,nome ,tipo ,morada ,local ,codpost ,tlmvl ,email ,dtnasc ,sexo
		,nacion ,tratamento ,obs ,ncont ,drno ,drestab ,drcedula ,drinscri ,drordem ,drclprofi ,usrdata ,usrhora ,usrinis ,lingua /*,loja*/
		,departamento ,area ,HonorarioDeducao ,HonorarioValor ,HonorarioBi ,HonorarioTipo ,retencao ,aceita_termos ,dt_aceita_termos ,Encryptedpassw ,password ,primacesso
		,extensaotelefone ,emailprofissional ,codSinave ,data_alt_pw
	FROM 
		b_us(nolock) 
	WHERE 
		iniciais = @iniciais

END


GO
Grant Execute on dbo.up_utilizadores_dadosAtualizarCentral to Public
Grant control on dbo.up_utilizadores_dadosAtualizarCentral to Public
Go

