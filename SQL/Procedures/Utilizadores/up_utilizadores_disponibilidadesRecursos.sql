-- exec up_utilizadores_disponibilidadesRecursos 'utilizadores',1,0
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_utilizadores_disponibilidadesRecursos]') IS NOT NULL
	drop procedure dbo.up_utilizadores_disponibilidadesRecursos
go

create procedure dbo.up_utilizadores_disponibilidadesRecursos
@tipo varchar(30)
,@no numeric(10,0)
,@estab numeric(3,0)



/* WITH ENCRYPTION */
AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosDisp'))
		DROP TABLE #dadosDisp
	
	Select 
		id_disp=convert(int,ROW_NUMBER() OVER(ORDER BY dataInicio
											,dataFim
											,horaInicio
											,horafim ))
		,seriestamp
	into
		#dadosDisp
	from 
		b_series 
	where 
		tipo = @tipo
		and no = @no
		and estab = @estab
	Order by 
		dataInicio
		,dataFim
		,horaInicio
		,horafim

	select 
		id_disp
		,b_us_disp_resursos.* 
	from 
		b_us_disp_resursos (nolock)
		inner join #dadosDisp b_series on b_us_disp_resursos.seriestamp = b_series.seriestamp
	order by 
		id_disp
GO
Grant Execute on dbo.up_utilizadores_disponibilidadesRecursos to Public
Grant Control on dbo.up_utilizadores_disponibilidadesRecursos to Public
Go
