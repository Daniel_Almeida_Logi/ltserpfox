/*

 exec up_utilizadores_ultimo

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_utilizadores_ultimo]') IS NOT NULL
	drop procedure dbo.up_utilizadores_ultimo
go

create procedure dbo.up_utilizadores_ultimo


/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	Select
		top 1 userno
	From
		b_us
	order 
		by 
			usrdata desc, usrhora desc
GO
Grant Execute On dbo.up_utilizadores_ultimo to Public
Grant Control On dbo.up_utilizadores_ultimo to Public
GO
