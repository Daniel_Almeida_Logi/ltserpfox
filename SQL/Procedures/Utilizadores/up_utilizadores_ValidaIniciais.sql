/*

	 exec up_utilizadores_ValidaEliminacao 1

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_utilizadores_ValidaIniciais]') IS NOT NULL
	drop procedure dbo.up_utilizadores_ValidaIniciais
go

create procedure dbo.up_utilizadores_ValidaIniciais
@user numeric(6)
,@iniciais varchar(3)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON


	if exists (SELECT top 1 stamp = bo.bostamp FROM bo (nolock) where bo.vendedor=@user)
	begin
		select existe=1
	end 
	else 
	if exists (SELECT top 1 stamp = ft.ftstamp FROM ft (nolock) where ft.vendedor=@user)
	begin
		select existe=1
	end
	else 
	if exists (SELECT top 1 stamp = fo.fostamp FROM fo (nolock) where fo.ousrinis=@iniciais)
	begin
		select existe=1
	end else begin
		select existe=0 where 1!=1
	end
		
	
		
GO
Grant Execute On dbo.up_utilizadores_ValidaIniciais to Public
Grant Control On dbo.up_utilizadores_ValidaIniciais to Public
GO
