/*

	exec up_utilizadores_pesquisa '',0,'','','','','FR'
	
	select * from b_us where userno in (select userno from b_usesp where especialidade like 'ENFERMAGEM')
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_utilizadores_pesquisa]') IS NOT NULL
	drop procedure dbo.up_utilizadores_pesquisa
go

CREATE PROCEDURE dbo.up_utilizadores_pesquisa
@nome				AS VARCHAR(20),
@userno				AS NUMERIC(5),
@login				AS VARCHAR(20),
@grupo				AS VARCHAR(20),
@especialidade		AS VARCHAR(50) = '',
@site				AS VARCHAR(60) = '',
@iniciais			AS VARCHAR(25) = ''

/* WITH ENCRYPTION */
AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosEsp'))
		DROP TABLE #dadosEsp

	DECLARE @ocultar bit = 1
	IF(@iniciais = 'ADM' OR @iniciais = 'PRO' OR @iniciais = 'ONL')
		SET @ocultar = 0 			

print @ocultar

	SELECT 
		userno 
	INTO 
		#dadosEsp
	FROM 
		b_usesp 
	WHERE 
		especialidade LIKE @especialidade +'%'

	SELECT
		sel = CONVERT(BIT,0)
		,*
	FROM
		b_us
	WHERE
		nome LIKE @nome +'%'
		AND userno = CASE WHEN @userno = 0 THEN userno ELSE @userno END
		AND username LIKE @login +'%'
		AND grupo = CASE WHEN @grupo = '' THEN grupo ELSE @grupo END
		AND b_us.loja = CASE WHEN @site = '' THEN b_us.loja ELSE @site END
		AND (userno in (select userno from #dadosEsp) OR @especialidade = '')
		AND 1=(
             CASE WHEN @ocultar = 1 and iniciais not in ('ADM','PRO', 'ONL') THEN 1
				  WHEN @ocultar = 0 and iniciais  in (iniciais) THEN 1
                  ELSE 0 END
            )

GO
Grant Execute On dbo.up_utilizadores_pesquisa to Public
Grant Control On dbo.up_utilizadores_pesquisa to Public
GO
