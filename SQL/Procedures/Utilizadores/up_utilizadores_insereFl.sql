/*
	exec up_utilizadores_insereFl 1
	
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_utilizadores_insereFl]') IS NOT NULL
	drop procedure dbo.up_utilizadores_insereFl
go

create procedure dbo.up_utilizadores_insereFl
@userno as numeric(5)

/* WITH ENCRYPTION */
AS

	insert into fl (
		flstamp
		,nome
		,no
		,moeda
		,morada
		,local
		,codpost
		,tlmvl
		,ncont
		,u_no
	)
	select 
		flstamp = LEFT(NEWID(),25)
		,nome = b_us.nome
		,no = isnull((select MAX(no) from fl),0) ++1
		,moeda = 'EURO'
		,morada
		,local
		,codpost
		,tlmvl
		,ncont
		,u_no = userno
	from 
		b_us
	where
		b_us.userno = @userno	

GO
Grant Execute On dbo.up_utilizadores_insereFl to Public
Grant Control On dbo.up_utilizadores_insereFl to Public
GO
