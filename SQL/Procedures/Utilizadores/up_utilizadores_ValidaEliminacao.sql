/*

	 exec up_utilizadores_ValidaEliminacao 1

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_utilizadores_ValidaEliminacao]') IS NOT NULL
	drop procedure dbo.up_utilizadores_ValidaEliminacao
go

create procedure dbo.up_utilizadores_ValidaEliminacao
@userno as numeric(5,0)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	Select
		count(ftstamp) cont
	From
		ft (nolock)
	Where
		vendedor = @userno
	
		
GO
Grant Execute On dbo.up_utilizadores_ValidaEliminacao to Public
Grant Control On dbo.up_utilizadores_ValidaEliminacao to Public
GO
