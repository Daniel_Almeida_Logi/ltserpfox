/* 
	Procedere para guardar as informacoes do administrationWays
	exec up_get_Dispensa_Eletronica_D_posologia ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_Dispensa_Eletronica_D_posologia]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_Dispensa_Eletronica_D_posologia
GO

CREATE PROCEDURE dbo.up_get_Dispensa_Eletronica_D_posologia

	@token			AS VARCHAR(40)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	SELECT
		stamp
		,tokenPosologia
		,tokenPosologiaTipo
		,descricao
		,duracaoProlongada
	FROM
		Dispensa_Eletronica_D_posologia (NOLOCK) 
	WHERE 
		tokenPosologia  = @token

GO
GRANT EXECUTE on dbo.up_get_Dispensa_Eletronica_D_posologia TO PUBLIC
GRANT Control on dbo.up_get_Dispensa_Eletronica_D_posologia TO PUBLIC
GO