/* 
	Procedere para guardar as informacoes do administrationWays
	exec up_save_Dispensa_Eletronica_D_Assinatura '','','',''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_Dispensa_Eletronica_D_Assinatura]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_Dispensa_Eletronica_D_Assinatura
GO

CREATE PROCEDURE dbo.up_save_Dispensa_Eletronica_D_Assinatura
	@stamp					AS VARCHAR(36),
	@tokenAssinatura		AS VARCHAR(40),
	@tokenNrlinha			AS VARCHAR(40),
	@infoAssinatura			AS VARCHAR(max)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	IF( NOT EXISTS (SELECT * FROM Dispensa_Eletronica_D_Assinatura (NOLOCK) WHERE stamp  = @stamp))
	BEGIN
		INSERT INTO Dispensa_Eletronica_D_Assinatura(stamp, tokenAssinatura, tokenNrlinha, infoAssinatura 
									,ousrdata, ousrinis, usrdata, usrinis)
		VALUES (@stamp,@tokenAssinatura,@tokenNrlinha, @infoAssinatura 
				,getdate(),'ADM',getdate(),'ADM')
	END

GO
GRANT EXECUTE on dbo.up_save_Dispensa_Eletronica_D_Assinatura TO PUBLIC
GRANT Control on dbo.up_save_Dispensa_Eletronica_D_Assinatura TO PUBLIC
GO