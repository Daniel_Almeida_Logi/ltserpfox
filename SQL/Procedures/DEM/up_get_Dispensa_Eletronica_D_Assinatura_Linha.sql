/* 
	Procedere para guardar as informacoes do administrationWays
	exec up_get_Dispensa_Eletronica_D_Assinatura_Linha ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_Dispensa_Eletronica_D_Assinatura_Linha]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_Dispensa_Eletronica_D_Assinatura_Linha
GO

CREATE PROCEDURE dbo.up_get_Dispensa_Eletronica_D_Assinatura_Linha

	@token			AS VARCHAR(40)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	SELECT
		stamp
		,tokenNrlinha
		,nrLinha
	FROM
		Dispensa_Eletronica_D_Assinatura_Linha (NOLOCK) 
	WHERE 
		tokenNrlinha = @token

GO
GRANT EXECUTE on dbo.up_get_Dispensa_Eletronica_D_Assinatura_Linha TO PUBLIC
GRANT Control on dbo.up_get_Dispensa_Eletronica_D_Assinatura_Linha TO PUBLIC
GO