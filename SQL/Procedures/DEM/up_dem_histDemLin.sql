/*
	
	exec up_dem_histDemLin  '75c6968e-6c3e-4c15-9efb-94a4e15c3f86', 1
	exec up_dem_histDemLin  ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_dem_histDemLin]') IS NOT NULL
	drop procedure dbo.up_dem_histDemLin
go

create procedure dbo.up_dem_histDemLin
	@Token VARCHAR(40),
	@comDispense BIT = 0

AS
BEGIN

	SELECT
		ISNULL(medicamento_cod, '') as medicamento_cod
		,ISNULL(medicamento_cnpem, '') AS medicamento_cnpem
		,CONVERT(VARCHAR(254),medicamento_descr) AS medicamento_descr
		,CONVERT(VARCHAR,data_caducidade, 112) AS data_caducidade
		,CONVERT(VARCHAR,dataDispensa, 112) as dataDispensa
		,numRegistoDispensado
		,ISNULL((select TOP 1 design FROM fprod(nolock) where cnp = numRegistoDispensado), '') AS designDispensado
		,(CASE WHEN CONVERT(VARCHAR, dataDispensa, 112) <> '19000101' THEN CAST(1 AS bit) ELSE CAST(0 AS bit) END) as dispensado
		,CONVERT(VARCHAR(40), token) AS token
		,ISNULL(Dispensa_Eletronica_D.id, '') as id
		,left(ISNULL(Dispensa_Eletronica_D.posologia,''),200) as posologia
	FROM 
		Dispensa_Eletronica_D(nolock)
	WHERE
		Token = @Token
		and 1 = (CASE WHEN @comDispense = 0 THEN 1 ELSE (CASE WHEN CONVERT(varchar, dataDispensa, 112) = '19000101' THEN 1 ELSE 0 END) END)


END

GO
Grant Execute on dbo.up_dem_histDemLin to Public
Grant Control on dbo.up_dem_histDemLin to Public
GO