/*
	
	exec up_dem_histDemCab  'D66AF523-AD92-4434-BD68-0A75931D3517', 1
	exec up_dem_histDemCab  ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_dem_histDemCab]') IS NOT NULL
	drop procedure dbo.up_dem_histDemCab
go

create procedure dbo.up_dem_histDemCab
	@groupToken VARCHAR(50),
	@comDispense BIT = 0


AS
BEGIN

	SELECT
		receita_nr
		,CONVERT(VARCHAR, receita_data, 112) AS receita_data
		,token
		,cast(0 as bit) as sel
		,utente_descr
	FROM
		Dispensa_Eletronica(nolock)
	WHERE
		groupToken = @groupToken
		and resultado_consulta_cod = 'CPF0001'
		and 1 = (CASE WHEN @comDispense = 0 THEN 1 ELSE (CASE WHEN (select count(*) FROM Dispensa_Eletronica_D(nolock) where token = Dispensa_Eletronica.token and convert(varchar, dataDispensa,112) = '19000101') <> 0 THEN 1 ELSE 0 END) END)
	ORDER BY
		receita_data DESC

END

GO
Grant Execute on dbo.up_dem_histDemCab to Public
Grant Control on dbo.up_dem_histDemCab to Public
GO