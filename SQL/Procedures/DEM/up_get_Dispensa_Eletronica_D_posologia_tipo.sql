/* 
	Procedere para guardar as informacoes do administrationWays
	exec up_get_Dispensa_Eletronica_D_posologia_tipo ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_Dispensa_Eletronica_D_posologia_tipo]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_Dispensa_Eletronica_D_posologia_tipo
GO

CREATE PROCEDURE dbo.up_get_Dispensa_Eletronica_D_posologia_tipo

	@token			AS VARCHAR(40)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	SELECT
		stamp
		,tokenPosologiaTipo
		,tokenPosologiaTipoToma
		,intervalo
		,duracaoUnidade
		,duracaoValor
		,descanso
	FROM
		Dispensa_Eletronica_D_posologia_tipo (NOLOCK) 
	WHERE 
		tokenPosologiaTipo  = @token

GO
GRANT EXECUTE on dbo.up_get_Dispensa_Eletronica_D_posologia_tipo TO PUBLIC
GRANT Control on dbo.up_get_Dispensa_Eletronica_D_posologia_tipo TO PUBLIC
GO