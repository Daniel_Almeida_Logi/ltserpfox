/* 
	determina o lote em que fica alojada a receita
	
	exec up_dem_tipoLote '67D809006C931454E05390E2CA0ACB53' 
	select * From dispensa_eletronica_dd where token = '67D809006C931454E05390E2CA0ACB53'

	exec up_dem_tipoLote 'AD10F1016DBBD31CE05390E2CA0A9513' 
	select * From dispensa_eletronica_dd where token = '46E962B7BC950E0BE05390E2CA0AA423'
*/

if OBJECT_ID('[dbo].[up_dem_tipoLote]') IS NOT NULL
	drop procedure dbo.up_dem_tipoLote
go

create procedure dbo.up_dem_tipoLote
	@token			varchar(40)

/* WITH ENCRYPTION */
AS
	-- caso o pvp, % comp sejam diferentes das enviadas pelo servi�o de valida��o coloca a receita nos lotes com erro
	declare @temErroLinhas		as bit = 0
			--,@temErroPVP		as bit = 0
			--,@temErroPref		as bit = 0
			,@temErroComp		as bit = 0
			--,@temErroPercComp	as bit = 0
			--,@temErroCompDip	as bit = 0

	select top 1
		@temErroLinhas = 1
		--@temErroLinhas = case when isnull(retorno_erro_cod,'') != '' then 1 else 0 end
		--,@temErroPVP = case when retorno_pvp is not null and pvp != retorno_pvp  then 1 else 0 end
		--,@temErroPref = case when retorno_pref is not null and pref != retorno_pref then 1 else 0 end
		,@temErroComp = 1
		--,@temErroComp = case when retorno_comp_sns is not null and (comp_sns != retorno_comp_sns and retorno_comp_diploma_tx != 100) then 1 else 0 end
		--,@temErroPercComp = case when retorno_comp_sns_tx is not null and comp_sns_tx != retorno_comp_sns_tx then 1 else 0 end	
		--,@temErroCompDip = case when retorno_comp_diploma_tx is not null and comp_diploma_tx != retorno_comp_diploma_tx then 1 else 0 end
		--set @temErroLinhas = case when exists (Select id from Dispensa_Eletronica_DD (nolock) where Dispensa_Eletronica_DD.token = @token and isnull(retorno_erro_cod,'') != '') then 1 else 0 end
		--set @temErroPVP = case when exists (Select id from Dispensa_Eletronica_DD (nolock) where Dispensa_Eletronica_DD.token = @token and retorno_pvp is not null and pvp != retorno_pvp and retorno_pvp != 0 and pvp != 0) then 1 else 0 end
		--set @temErroPref = case when exists (Select id from Dispensa_Eletronica_DD (nolock) where Dispensa_Eletronica_DD.token = @token and retorno_pref is not null and pref != retorno_pref and retorno_pref != 0 and pref != 0) then 1 else 0 end
		--set @temErroComp = case when exists (Select id from Dispensa_Eletronica_DD (nolock) where Dispensa_Eletronica_DD.token = @token and retorno_comp_sns is not null and comp_sns != retorno_comp_sns) then 1 else 0 end
		--set @temErroPercComp = case when exists (Select id from Dispensa_Eletronica_DD (nolock) where Dispensa_Eletronica_DD.token = @token and retorno_comp_sns_tx is not null and comp_sns_tx != retorno_comp_sns_tx) then 1 else 0 end
		--set @temErroCompDip = case when exists (Select id from Dispensa_Eletronica_DD (nolock) where Dispensa_Eletronica_DD.token = @token and retorno_comp_diploma_tx is not null and comp_diploma_tx != retorno_comp_diploma_tx) then 1 else 0 end
	from
		Dispensa_Eletronica_DD (nolock)
	left join ft2(nolock) on ft2.token = Dispensa_Eletronica_DD.token  
	left join fi(nolock) on fi.id_Dispensa_Eletronica_D = Dispensa_Eletronica_DD.id and fi.ftstamp = ft2.ft2stamp
	where
		Dispensa_Eletronica_DD.token = @token
		and (
			isnull(retorno_erro_cod,'') != ''
			OR			
			(isnull(retorno_comp_sns,0)  != comp_sns and isnull(retorno_info_prestacao,'') != '')
			or
			fi.u_ettent1 != case when ft2.u_abrev ='SNS' then isnull(retorno_comp_sns,0) else fi.u_ettent1 end
			or
			 fi.u_ettent2 != case when ft2.u_abrev2='SNS' then isnull(retorno_comp_sns,0) else fi.u_ettent2 end
		)
		and (select top 1 u_abrev from ft2(nolock) where token = @token) not in ('ASTELLAS', 'Ferring', 'LUNDBECK', 'PSOPortugal','FARMIVESTVYDURA','SNU')
	
	--select * From dispensa_eletronica_dd where token = '46E962B7BC950E0BE05390E2CA0AA423'
	Select 
		Top 1
		receita_tipo
		,resultado_comprovativo_registo
		,tipoLote = case 
						when isnull(resultado_comprovativo_registo,'') = '' then '96' 
						when receita_tipo = 'RSP' and (@temErroLinhas   = 1 or @temErroComp = 1) then '96'
						when receita_tipo = 'RSP' and @temErroLinhas   = 0 then '97'
						when receita_tipo != 'RSP' and (@temErroLinhas  = 1 or @temErroComp = 1) then '98'
						when receita_tipo != 'RSP' and @temErroLinhas  = 0 then '99'
					end
	from 
		Dispensa_Eletronica(nolock) 
	where 
		Dispensa_Eletronica.token = @token
	order by 
		data_cre desc

GO
Grant Execute On dbo.up_dem_tipoLote to Public
Grant Control On dbo.up_dem_tipoLote to Public
Go
--	select * From dispensa_eletronica where token = '67D809006C931454E05390E2CA0ACB53'