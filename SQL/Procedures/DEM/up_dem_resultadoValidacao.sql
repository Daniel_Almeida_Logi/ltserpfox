/*
	exec up_dem_resultadoValidacao 'ADM4up_dem_resultadoValidacaoADFCB9E-B5FA-4161-980'
*/
if OBJECT_ID('[dbo].[up_dem_resultadoValidacao]') IS NOT NULL
	drop procedure dbo.up_dem_resultadoValidacao
go

create procedure dbo.up_dem_resultadoValidacao
	@token varchar(40)

/* WITH ENCRYPTION */
AS

	select 
		resultado_validacao_cod = isnull(resultado_validacao_cod,'')
		,resultado_validacao_descr = isnull(resultado_validacao_descr,'')
		,receita_nr
		,receita_tipo
	from 
		Dispensa_Eletronica (nolock)
	where 
		Dispensa_Eletronica.token = @token

GO
Grant Execute On dbo.up_dem_resultadoValidacao to Public
Grant Control On dbo.up_dem_resultadoValidacao to Public
Go