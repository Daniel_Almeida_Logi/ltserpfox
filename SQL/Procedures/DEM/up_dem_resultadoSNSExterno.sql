/*
	exec up_dem_resultadoSNSExterno 'ADMCC2FFE6E-E65D-4782-B5E'
*/
if OBJECT_ID('[dbo].[up_dem_resultadoSNSExterno]') IS NOT NULL
	drop procedure dbo.up_dem_resultadoSNSExterno
go

create procedure dbo.up_dem_resultadoSNSExterno
	@token varchar(40)

/* WITH ENCRYPTION */
AS

	select 
		resultado_validacao_cod = isnull(resultado_validacao_cod,'')
		,resultado_validacao_descr = isnull(resultado_validacao_descr,'')
		,resultado_efetivacao_cod = isnull(resultado_efetivacao_cod,'')
		,resultado_efetivacao_descr = isnull(resultado_efetivacao_descr,'')
		,resultado_anulacao_cod = isnull(resultado_anulacao_cod,'')
		,resultado_anulacao_descr = isnull(resultado_anulacao_descr,'')
		,receita_nr
		,receita_tipo
		,token = rtrim(ltrim(isnull(Dispensa_Eletronica.token,'')))
		,receita = rtrim(ltrim(isnull(Dispensa_Eletronica.receita_nr,'')))
		,resultado_comprovativo_registo  = convert(varchar(254),rtrim(ltrim(isnull(Dispensa_Eletronica.resultado_comprovativo_registo,''))))
	from 
		Dispensa_Eletronica (nolock)
	where 
		Dispensa_Eletronica.token = @token

GO
Grant Execute On dbo.up_dem_resultadoSNSExterno to Public
Grant Control On dbo.up_dem_resultadoSNSExterno to Public
Go

