

/*
	exec up_dem_resultadoValidacaoLinhas '1C313D6AC2FF02B2E06390E2CA0AD89E'

	Nota: erro D083 nas linhas n�o deve gerar alerta
*/
if OBJECT_ID('[dbo].[up_dem_resultadoValidacaoLinhas]') IS NOT NULL
	drop procedure dbo.up_dem_resultadoValidacaoLinhas
go

create procedure dbo.up_dem_resultadoValidacaoLinhas
	@token varchar(40)

/* WITH ENCRYPTION */
AS

	select 
		resultado_validacao_cod = isnull(resultado_validacao_cod,'')
		,resultado_validacao_descr = isnull(resultado_validacao_descr,'')
		,receita_nr
		,retorno_erro_cod =    case when dbo.uf_dem_valida_execao_erro_linhas(efr_cod,retorno_erro_cod)=1   then '' else isnull(retorno_erro_cod,'')  end
		,retorno_erro_descr =  case when dbo.uf_dem_valida_execao_erro_linhas(efr_cod,retorno_erro_cod)=1 or  isnull(retorno_erro_cod,'')=''  then '' else '[' + ISNULL(Dispensa_Eletronica_DD.ref,'') + ']' + ' - ' + ISNULL(Dispensa_Eletronica_DD.retorno_erro_descr,'') end
		,id
		,ref
		,Dispensa_Eletronica.token
		,retorno_info_prestacao
		,retorno_comp_sns
		,retorno_comp_sns_tx
		,retorno_comp_diploma_tx
		,comp_sns
		,round(comp_sns_tx,0) as  comp_sns_tx
		,round(comp_diploma_tx,0) as comp_diploma_tx

	
	from 
		Dispensa_Eletronica (nolock)
		inner join Dispensa_Eletronica_DD (nolock) on Dispensa_Eletronica.token = Dispensa_Eletronica_DD.token
	where 
		Dispensa_Eletronica.token = @token

GO
Grant Execute On dbo.up_dem_resultadoValidacaoLinhas to Public
Grant Control On dbo.up_dem_resultadoValidacaoLinhas to Public
Go


