/* 
	Procedere para guardar as informacoes do administrationWays
	exec up_get_Dispensa_Eletronica_D_Assinatura ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_Dispensa_Eletronica_D_Assinatura]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_Dispensa_Eletronica_D_Assinatura
GO

CREATE PROCEDURE dbo.up_get_Dispensa_Eletronica_D_Assinatura

	@token			AS VARCHAR(40)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	SELECT
		stamp
		,tokenAssinatura
		,tokenNrlinha
		,infoAssinatura
	FROM
		Dispensa_Eletronica_D_Assinatura (NOLOCK) 
	WHERE 
		tokenAssinatura = @token

GO
GRANT EXECUTE on dbo.up_get_Dispensa_Eletronica_D_Assinatura TO PUBLIC
GRANT Control on dbo.up_get_Dispensa_Eletronica_D_Assinatura TO PUBLIC
GO