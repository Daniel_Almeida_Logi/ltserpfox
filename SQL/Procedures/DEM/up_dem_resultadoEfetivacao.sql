/*
	exec up_dem_resultadoEfetivacao 'B4C3A8400062F208E05390E2CA0A6887'
*/
if OBJECT_ID('[dbo].[up_dem_resultadoEfetivacao]') IS NOT NULL
	drop procedure dbo.up_dem_resultadoEfetivacao
go

create procedure dbo.up_dem_resultadoEfetivacao
	@token varchar(40)

/* WITH ENCRYPTION */
AS
	select 
		resultado_efetivacao_cod = isnull(resultado_efetivacao_cod,''), 
		resultado_efetivacao_descr= isnull(resultado_efetivacao_descr,''), 
		resultado_comprovativo_registo = isnull(resultado_comprovativo_registo,'') ,
		receita_nr,
		resultado_comprovativo_registo_xs = convert(varchar(200),isnull(resultado_comprovativo_registo,''))  
	from 
		Dispensa_Eletronica (nolock) 
	where 
		Dispensa_Eletronica.token = @token


GO
Grant Execute On dbo.up_dem_resultadoEfetivacao to Public
Grant Control On dbo.up_dem_resultadoEfetivacao to Public
Go