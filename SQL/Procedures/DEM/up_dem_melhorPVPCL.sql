

if OBJECT_ID('[dbo].[up_dem_melhorPVPCL]') IS NOT NULL
	drop procedure dbo.up_dem_melhorPVPCL
go

create procedure dbo.up_dem_melhorPVPCL
	@cnpem varchar(8)
	,@site_nr tinyint

/* WITH ENCRYPTION */
AS

	Select 
		st.ref,epv1-epcult, stock, dci, st.design
	from 
		st (nolock)
		inner join fprod (nolock) on st.ref = fprod.cnp	
	where 
		cnpem = @cnpem
		AND st.site_nr = @site_nr
	order by
		epv1-epcult desc, stock desc
		
		
GO
Grant Execute On dbo.up_dem_melhorPVPCL to Public
Grant Control On dbo.up_dem_melhorPVPCL to Public
Go