/* SP para mostrar informa��o da consulta de dispensa cruzada com a informa��o retornada pelo servi�o de valida��o
	
	select * from dispensa_eletronica where receita_nr = '1011000028183486706'

	use ltstag
	exec up_dem_consultaDispensa 'D5F35BA5907FA993E05390E2CA0AE99A' 



*/
if OBJECT_ID('[dbo].[up_dem_consultaDispensa]') IS NOT NULL
	drop procedure dbo.up_dem_consultaDispensa
go

create procedure dbo.up_dem_consultaDispensa
	@token varchar(40)


/* WITH ENCRYPTION */
AS
	If OBJECT_ID('tempdb.dbo.#tempReceitasDispensadas') IS NOT NULL
		drop table #tempReceitasDispensadas;

	select 
		-- informa��o consultada
		d.token
		,d.id 
		,isnull(d.medicamento_cnpem,'')								as cnpemConsulta
		,isnull(d.medicamento_cod,'')								as cnpConsulta
		,rtrim(ltrim(isnull(d.medicamento_descr,'')))				as descrConsulta
	
		
		-- informa��o dispensada
		,case when isnull(d_detalhe.ref,'') ='' then 0 else 1  end  as linha_dispensada 
		,isnull(d.lote, 0)											as loteEfetivado
		,isnull(d_detalhe.ref,'')                                   as dispensa_ref 
		,isnull(d_detalhe.ref,'')                                   as ref 
		,isnull(isnull(d.pvp, d_detalhe.pvp),0)						as dispensa_pvp
		,isnull(isnull(d.pref, d_detalhe.pref),0)					as dispensa_pref
		,isnull(isnull(d.pvp5, d_detalhe.pvp5),0)					as dispensa_pvp5
		,isnull(isnull(d.comp, d_detalhe.comp_sns),0)				as dispensa_comp
		,isnull(isnull(d.tx_comp, d_detalhe.comp_sns_tx),0)			as dispensa_txComp
		,isnull(d.diploma_cod,isnull(d_detalhe.diploma_cod,''))		as dispensa_diploma
		-- informa��o retorno SNS servi�o valida��o
		,isnull(d_detalhe.retorno_erro_descr,'')					as retorno_erro_descr
		,isnull(d_detalhe.retorno_pvp,0)							as retorno_pvp
		,isnull(d_detalhe.retorno_pref,0)							as retorno_pref
		,isnull(d_detalhe.retorno_pvp5,0)							as retorno_pvp5
		,isnull(d_detalhe.retorno_pmax,0)							as retorno_pmax
		,isnull(d_detalhe.retorno_comp_sns,0)						as retorno_comp_sns
		,isnull(d_detalhe.retorno_comp_sns_tx,0)					as retorno_comp_sns_tx
		,isnull(d_detalhe.diploma_cod,'')							as diploma_cod
		,isnull(d_detalhe.retorno_comp_diploma_tx,0)				as retorno_comp_diploma_tx
		,convert(datetime,isnull(d.data_caducidade,'19000101'))     as validade_linha
		,isnull(d.posologia,'')										as posologia

	into 
		#tempReceitasDispensadas
	from 
		Dispensa_Eletronica_D d (nolock)
		left join  Dispensa_Eletronica_DD d_detalhe (nolock) on d_detalhe.ID = d.ID AND d_detalhe.TOKEN = D.TOKEN
		left join dispensa_eletronica (nolock) on dispensa_eletronica.token = d_detalhe.token
	where 
		d.token = @token		


	select
		*
	from
		#tempReceitasDispensadas
	order by linha_dispensada desc
	

	If OBJECT_ID('tempdb.dbo.#tempReceitasDispensadas') IS NOT NULL
		drop table #tempReceitasDispensadas;
GO
Grant Execute On dbo.up_dem_consultaDispensa to Public
Grant Control On dbo.up_dem_consultaDispensa to Public
Go 



