
/*

	select * from dispensa_eletronica where token = '55BC3B064A6F0A88E05390E2CA0A2114'

	exec up_dem_informacaoMedico '55BC3B064A6F0A88E05390E2CA0A2114'
	exec up_dem_informacaoMedico ''
	select * from b_parameters(nolock) WHERE stamp='ADM0000000279'

	update b_parameters set bool=1 WHERE stamp='ADM0000000279'
*/
if OBJECT_ID('[dbo].[up_dem_informacaoMedico]') IS NOT NULL
	drop procedure dbo.up_dem_informacaoMedico
go




create procedure dbo.up_dem_informacaoMedico
	@token  varchar (100)

	/* WITH ENCRYPTION */
	AS
	
		select 
			top 1
			'prescritor_nome' = ISNULL(prescritor_nome,'')
			,'prescritor_contacto'=ISNULL(prescritor_contacto,'')
			,'utente_nome' = ISNULL(utente_descr,'')
			,'utente_contacto'=ISNULL(utente_contacto,'')
			,'especialidade' =   ISNULL((SELECT top 1 especialidade FROM medico_especialidade(nolock) WHERE id= Dispensa_Eletronica.prescritor_especialidade),'')
			,'local_presc' = ISNULL(local_presc_descr,'')
		from 
			Dispensa_Eletronica (nolock) 
		where 
			Dispensa_Eletronica.token = @token
				

GO
Grant Execute On dbo.up_dem_informacaoMedico to Public
Grant Control On dbo.up_dem_informacaoMedico to Public
Go 

