/* 
	Procedere para guardar as informacoes do administrationWays
	exec up_save_Dispensa_Eletronica_D_posologia_tipo '','','','',0

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_Dispensa_Eletronica_D_posologia_tipo]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_Dispensa_Eletronica_D_posologia_tipo
GO

CREATE PROCEDURE dbo.up_save_Dispensa_Eletronica_D_posologia_tipo
	@stamp						AS VARCHAR(36),
	@tokenPosologiaTipo			AS VARCHAR(40),
	@tokenPosologiaTipoToma		AS VARCHAR(40),
	@intervalo					AS INT,
	@duracaoUnidade				AS VARCHAR(100),
	@duracaoValor				AS VARCHAR(100),
	@descanso					AS VARCHAR(100)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	IF( NOT EXISTS (SELECT * FROM Dispensa_Eletronica_D_posologia_tipo (NOLOCK) WHERE stamp  = @stamp))
	BEGIN
		INSERT INTO Dispensa_Eletronica_D_posologia_tipo(stamp, tokenPosologiaTipo, tokenPosologiaTipoToma
									,intervalo, duracaoUnidade, duracaoValor, descanso
									,ousrdata, ousrinis, usrdata, usrinis)
		VALUES (@stamp, @tokenPosologiaTipo, @tokenPosologiaTipoToma 
				,@intervalo, @duracaoUnidade, @duracaoValor, @descanso
				,getdate(),'ADM',getdate(),'ADM')
	END

GO
GRANT EXECUTE on dbo.up_save_Dispensa_Eletronica_D_posologia_tipo TO PUBLIC
GRANT Control on dbo.up_save_Dispensa_Eletronica_D_posologia_tipo TO PUBLIC
GO