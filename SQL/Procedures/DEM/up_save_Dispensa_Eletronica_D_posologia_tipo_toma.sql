/* 
	Procedere para guardar as informacoes do administrationWays
	exec up_save_Dispensa_Eletronica_D_posologia_tipo_toma '','','','',0

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_Dispensa_Eletronica_D_posologia_tipo_toma]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_Dispensa_Eletronica_D_posologia_tipo_toma
GO

CREATE PROCEDURE dbo.up_save_Dispensa_Eletronica_D_posologia_tipo_toma
	@stamp					AS VARCHAR(36),
	@tokenPosologiaTipoToma AS VARCHAR(40),
	@toma					AS INT,
	@qtdValor				AS VARCHAR(100),
	@qtdUnidade				AS VARCHAR(100),
	@frequenciaValor		AS VARCHAR(100),
	@frequenciaUnidade		AS VARCHAR(100)
							
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	IF( NOT EXISTS (SELECT * FROM Dispensa_Eletronica_D_posologia_tipo_toma (NOLOCK) WHERE stamp  = @stamp))
	BEGIN
		INSERT INTO Dispensa_Eletronica_D_posologia_tipo_toma(stamp, tokenPosologiaTipoToma, toma, qtdValor
									,qtdUnidade, frequenciaValor, frequenciaUnidade
									,ousrdata, ousrinis, usrdata, usrinis)
		VALUES (@stamp, @tokenPosologiaTipoToma, @toma, @qtdValor,
				@qtdUnidade, @frequenciaValor, @frequenciaUnidade 
				,getdate(),'ADM',getdate(),'ADM')
	END

GO
GRANT EXECUTE on dbo.up_save_Dispensa_Eletronica_D_posologia_tipo_toma TO PUBLIC
GRANT Control on dbo.up_save_Dispensa_Eletronica_D_posologia_tipo_toma TO PUBLIC
GO