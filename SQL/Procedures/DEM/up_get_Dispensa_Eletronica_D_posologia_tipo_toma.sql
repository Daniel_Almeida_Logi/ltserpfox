/* 
	Procedere para guardar as informacoes do administrationWays
	exec up_get_Dispensa_Eletronica_D_posologia_tipo_toma ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_Dispensa_Eletronica_D_posologia_tipo_toma]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_Dispensa_Eletronica_D_posologia_tipo_toma
GO

CREATE PROCEDURE dbo.up_get_Dispensa_Eletronica_D_posologia_tipo_toma

	@token			AS VARCHAR(40)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	SELECT
		stamp
		,tokenPosologiaTipoToma
		,toma
		,qtdValor
		,qtdUnidade
		,frequenciaValor
		,frequenciaUnidade
	FROM
		Dispensa_Eletronica_D_posologia_tipo_toma (NOLOCK) 
	WHERE 
		tokenPosologiaTipoToma  = @token

GO
GRANT EXECUTE on dbo.up_get_Dispensa_Eletronica_D_posologia_tipo_toma TO PUBLIC
GRANT Control on dbo.up_get_Dispensa_Eletronica_D_posologia_tipo_toma TO PUBLIC
GO