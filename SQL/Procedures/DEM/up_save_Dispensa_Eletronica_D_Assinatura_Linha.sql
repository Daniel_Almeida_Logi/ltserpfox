/* 
	Procedere para guardar as informacoes do administrationWays
	exec up_save_Dispensa_Eletronica_D_Assinatura_Linha '','',''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_Dispensa_Eletronica_D_Assinatura_Linha]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_Dispensa_Eletronica_D_Assinatura_Linha
GO

CREATE PROCEDURE dbo.up_save_Dispensa_Eletronica_D_Assinatura_Linha
	@stamp					AS VARCHAR(36),
	@tokenNrlinha			AS VARCHAR(40),
	@nrLinha				AS VARCHAR(254)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	IF( NOT EXISTS (SELECT * FROM Dispensa_Eletronica_D_Assinatura_Linha (NOLOCK) WHERE stamp  = @stamp))
	BEGIN
		INSERT INTO Dispensa_Eletronica_D_Assinatura_Linha(stamp, tokenNrlinha, nrLinha 
									,ousrdata, ousrinis, usrdata, usrinis)
		VALUES (@stamp, @tokenNrlinha, @nrLinha
				,getdate(),'ADM',getdate(),'ADM')
	END

GO
GRANT EXECUTE on dbo.up_save_Dispensa_Eletronica_D_Assinatura_Linha TO PUBLIC
GRANT Control on dbo.up_save_Dispensa_Eletronica_D_Assinatura_Linha TO PUBLIC
GO