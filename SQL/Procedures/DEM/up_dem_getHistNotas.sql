/*

	exec up_dem_getHistNotas 'ADM222A1386-8DDF-4AAB-AAE'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_dem_getHistNotas]') IS NOT NULL
	drop procedure dbo.up_dem_getHistNotas
go

create procedure [dbo].[up_dem_getHistNotas]
	@token VARCHAR(100) = '',
	@nrLinha VARCHAR(100) = '',
	@nrReceita VARCHAR(19) = '',
	@nome VARCHAR(160) = '',
	@design VARCHAR(400) = ''
	

AS
BEGIN	

	IF @design <> ''
	BEGIN

		SET @design = REPLACE(@design, '$', '$$')
		SET @design = REPLACE(@design, '[', '$[')
		SET @design = REPLACE(@design, ']', '$]')
		SET @design = REPLACE(@design, '%', '$%')
		SET @design = REPLACE(@design, '_', '$_')

	END

	DECLARE @sql VARCHAR(MAX) = N'
		SELECT 
			logs_com.dataCom
			,logs_com.infoCom
			,logs_com.estadoCom
			,logs_com.id
			,logs_com.texto
			,logs_com.dataNota
			,logs_com.token 
			,x.receita
			,x.utente
			,x.design
			,ISNULL((SELECT TOP 1 prescritor_nome FROM Dispensa_Eletronica(nolock) WHERE receita_nr = x.receita AND prescritor_nome <> ''''), '''') as medico
		FROM
			(
			select 
				DISTINCT			
				logs_com.stamp
				,ISNULL(Dispensa_Eletronica.receita_nr, '''') as receita
				,ISNULL(Dispensa_Eletronica.utente_descr, '''') as utente
				,CAST(ISNULL(Dispensa_Eletronica_d.medicamento_descr, '''') as VARCHAR(254)) as design
			from 
				logs_com(nolock)
				left join Dispensa_Eletronica_d(nolock) on logs_com.id = Dispensa_Eletronica_d.id
				left join Dispensa_Eletronica(nolock) on Dispensa_Eletronica_d.token = Dispensa_Eletronica.token
			where 
				logs_com.tipo= ''NOTAS_TERAPEUTICAS''
				and logs_com.token = ''' + @token + ''''

		+

		(CASE
			WHEN @nome <> ''
			THEN ' and Dispensa_Eletronica.utente_descr LIKE ''%' + LTRIM(RTRIM(@nome)) + '%'''
			ELSE ''
		END)

		+

		(CASE
			WHEN @design <> ''
			THEN ' and CAST(ISNULL(Dispensa_Eletronica_d.medicamento_descr, '''') as VARCHAR(254)) LIKE ''%' + LTRIM(RTRIM(@design)) + '%'' ESCAPE ''$'''
			ELSE ''
		END)

		+

		(CASE 

			WHEN @nrLinha <> '' 
			THEN ' and logs_com.id = ''' + @nrLinha + ''''
			WHEN @nrReceita <> '' 
			--THEN ' and logs_com.id in (SELECT lin.id FROM Dispensa_Eletronica_d(nolock) as lin inner join Dispensa_Eletronica(nolock) as rec ON lin.token = rec.token WHERE rec.receita_nr = ''' + @nrReceita + ''' )' 
			then ' and Dispensa_Eletronica.receita_nr = ''' + @nrReceita + ''''
			ELSE '' 
		END)

		+
		'
			) as X
			JOIN logs_com(nolock) ON X.stamp = logs_com.stamp'


	SET @sql = @sql + N'
		order by 
			dataNota desc'

	PRINT @sql
	EXEC (@sql)

END

GO
Grant Execute on dbo.up_dem_getHistNotas to Public
Grant control on dbo.up_dem_getHistNotas to Public
GO