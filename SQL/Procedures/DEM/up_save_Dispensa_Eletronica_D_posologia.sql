/* 
	Procedere para guardar as informacoes do administrationWays
	exec up_save_Dispensa_Eletronica_D_posologia '','','','',0

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_Dispensa_Eletronica_D_posologia]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_Dispensa_Eletronica_D_posologia
GO

CREATE PROCEDURE dbo.up_save_Dispensa_Eletronica_D_posologia
	@stamp					AS VARCHAR(36),
	@tokenPosologia			As VARCHAR(40),
	@tokenPosologiaTipo		As VARCHAR(40),
	@descricao				As VARCHAR(254),
	@duracaoProlongada		As VARCHAR(254)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	IF( NOT EXISTS (SELECT * FROM Dispensa_Eletronica_D_posologia (NOLOCK) WHERE stamp  = @stamp))
	BEGIN
		INSERT INTO Dispensa_Eletronica_D_posologia(stamp, tokenPosologia, tokenPosologiaTipo, descricao,		
									duracaoProlongada
									,ousrdata, ousrinis, usrdata, usrinis)
		VALUES (@stamp, @tokenPosologia, @tokenPosologiaTipo, @descricao
				,@duracaoProlongada
				,getdate(),'ADM',getdate(),'ADM')
	END

GO
GRANT EXECUTE on dbo.up_save_Dispensa_Eletronica_D_posologia TO PUBLIC
GRANT Control on dbo.up_save_Dispensa_Eletronica_D_posologia TO PUBLIC
GO