
/*

	Devolve plano MCDT

	up_dem_planoMCDT 'd6c9caab-5139-4d95-987b-3ceaefaa00f3'

*/
if OBJECT_ID('[dbo].[up_dem_planoMCDT]') IS NOT NULL
	drop procedure dbo.up_dem_planoMCDT
go

create procedure dbo.up_dem_planoMCDT
	@token  varchar (40)

	AS

	DECLARE 
		@receita_tipo VARCHAR(4),
		@pais_migrante VARCHAR(2),
		@doenca_profissional BIT = 0,
		@plano VARCHAR(3) = '',
		@nif varchar(50)


	SELECT 
		@receita_tipo = ISNULL(receita_tipo, '')
		,@pais_migrante = ISNULL(pais_migrante, '')
		,@nif = ISNULL(entidadeFacturacaoCod,'')
	FROM
		Dispensa_Eletronica(NOLOCK)
	WHERE
		token = @token

	IF @receita_tipo = 'RSP'
	BEGIN
		SELECT plano from cptplaMCDT(nolock) WHERE receita_tipo = @receita_tipo AND nifEntidade = @nif
	END
	ELSE
	BEGIN
		if(@pais_migrante!='PT')
			SELECT plano from cptplaMCDT(nolock) WHERE receita_tipo = @receita_tipo AND nifEntidade = @nif  AND pais_migrante != 'PT' 
		else begin
			SELECT plano from cptplaMCDT(nolock) WHERE receita_tipo = @receita_tipo AND nifEntidade = @nif  AND doenca_profissional = @doenca_profissional and pais_migrante='PT'
		end 

	END


GO
Grant Execute On dbo.up_dem_planoMCDT to Public
Grant Control On dbo.up_dem_planoMCDT to Public
Go 