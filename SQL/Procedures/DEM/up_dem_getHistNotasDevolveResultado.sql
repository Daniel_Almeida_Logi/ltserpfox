/*

	exec up_dem_getHistNotasDevolveResultado 'ADM222A1386-8DDF-4AAB-AAE'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_dem_getHistNotasDevolveResultado]') IS NOT NULL
	drop procedure dbo.up_dem_getHistNotasDevolveResultado
go

create procedure [dbo].up_dem_getHistNotasDevolveResultado
	@token VARCHAR(100) = ''

	

AS
BEGIN	
		set @token = dbo.alltrimIsNull(@token)

		select 
			top 1
			codigo,
			descricao
		from 
			logs_com(nolock)
		where token = @token

END

GO
Grant Execute on dbo.up_dem_getHistNotasDevolveResultado to Public
Grant control on dbo.up_dem_getHistNotasDevolveResultado to Public
GO