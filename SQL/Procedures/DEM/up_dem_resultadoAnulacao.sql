/*
	exec up_dem_resultadoAnulacao 'ADM4up_dem_resultadoValidacaoADFCB9E-B5FA-4161-980'
*/
if OBJECT_ID('[dbo].[up_dem_resultadoAnulacao]') IS NOT NULL
	drop procedure dbo.up_dem_resultadoAnulacao
go



create procedure dbo.up_dem_resultadoAnulacao
	@receita varchar(19)
	,@token varchar(40)

/* WITH ENCRYPTION */
AS

	select 
		Top 1
		resultado_anulacao_cod = isnull(resultado_anulacao_cod,''), 
		resultado_anulacao_descr= isnull(resultado_anulacao_descr,'')
	from 
		Dispensa_Eletronica (nolock)
	where 
		receita_nr = @receita
		and token = @token
		and isnull(resultado_anulacao_cod,'') != ''		
	order by
		data_cre desc

GO
Grant Execute On dbo.up_dem_resultadoAnulacao to Public
Grant Control On dbo.up_dem_resultadoAnulacao to Public
Go