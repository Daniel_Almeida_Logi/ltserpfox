/*  get informação da facturacao 
EXEC up_get_InfoResumeFact '068',2022,03,1
EXEC up_get_InfoResumeFact '070',2021,11,1
EXEC up_get_InfoResumeFact '062',2023,03,1
EXEC up_get_InfoResumeFact '008',2022,03,1
exec up_get_InfoResumeFact '008',2022,03,0
exec up_get_InfoResumeFact '008',2022,01,1
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_InfoResumeFact]') IS NOT NULL
	drop procedure dbo.up_get_InfoResumeFact
go

CREATE PROCEDURE [dbo].[up_get_InfoResumeFact]
	@idefr		VARCHAR(10),
	@year		smallint,
	@month		tinyint,
	@tipoDoc	int
AS
SET NOCOUNT ON
	declare @nomecptorg varchar(254)
	declare @docTypeRes int
	declare @sendXMLRes bit
	declare @dateInt datetime
	declare @dateEnd datetime
	declare @date datetime


	SELECT  @date =  DATEFROMPARTS (@year, @month, 1);
	SELECT  @dateInt =  EOMONTH(@date)

	SELECT @nomecptorg=REPLACE(abrev,'/',' ') , @docTypeRes= docTypeRes,@sendXMLRes=sendXMLRes, @dateEnd= DATEFROMPARTS (@year, @month, diaLimiteFact) FROM cptorg(nolock) where cptorgstamp='ADM' +@idefr


	set @dateEnd = DATEADD(month, 1, @dateEnd)

	SELECT 
		 Case when @docTypeRes=4  then '' else invoiceXmlPath end as  invoiceXML,
		case when  @docTypeRes=3  then @nomecptorg + '_' + CONVERT(VARCHAR(2),RIGHT(@year, 2))  +  RIGHT('00'+ CAST(month as varchar(2)),2)  else @nomecptorg + '_' + CONVERT(VARCHAR(4),@year) + '_' + CONVERT(VARCHAR(2),@month) end  AS nomecptorg,
	    case when  @docTypeRes=3  then RIGHT('00000'+CAST(id_ext_pharmacy AS VARCHAR(5)),5)  +  '_' + REPLACE(docNr,'/','-') 
			else REPLACE(RIGHT('00000'+CAST(id_ext_pharmacy AS VARCHAR(5)),5) +  '_' + code_ext_entity+'_' + CONVERT(VARCHAR(4),@year) +  CONVERT(VARCHAR(2),@month) + '_' +  docNr,'/','-') end as docNr,
		@docTypeRes as typeDoc,
		@sendXMLRes as sendXML,
		CONVERT(VARCHAR(4),ext_esb_doc.year) +  CONVERT(VARCHAR(2),ext_esb_doc.month) as monthFact , 
		 RIGHT('00000'+CAST(id_ext_pharmacy AS VARCHAR(5)),5) AS codFarm,
		 ousrdata ,
		 Case when  @docTypeRes=4 then	stamp			 else ''   end as stamp , 
		 case when  @docTypeRes=3 then  invoicePdfPath	 else ''   end as invoicePdfPath
	FROM ext_esb_doc (nolock)  
		where 
			id_ext_efr= @idefr 
			and tipoDoc = @tipoDoc
			and convert(date,ousrdata)>=@dateInt and convert(date,ousrdata)<=@dateEnd 
			and test=0
	

GO
GRANT EXECUTE ON dbo.up_get_InfoResumeFact to Public
GRANT CONTROL ON dbo.up_get_InfoResumeFact to Public
GO
