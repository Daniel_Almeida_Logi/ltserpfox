/*  get informação da facturacao  detalhada
EXEC up_get_InfoResumeFactInfo '07cb4039-38e6-47c2-a80c-2e34a90c51' 

EXEC up_get_InfoResumeFactInfo '2b3b46b8-6167-48e8-9af5-a583bca6bb'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_InfoResumeFactInfo]') IS NOT NULL
	drop procedure dbo.up_get_InfoResumeFactInfo
go

CREATE PROCEDURE [dbo].[up_get_InfoResumeFactInfo]
	@stamp varchar(36)
AS
SET NOCOUNT ON

	SELECT 
		RIGHT('00000'+CAST(id_ext_pharmacy AS VARCHAR(5)),5)											AS farmCode,
		name_ext_pharmacy																				AS farmName,
		docNr																							AS FactNumber, 
		docDate																							AS issuranceDate, 
		ext_esb_prescription.prescriptionId																AS prescriptionId,
		ext_esb_prescription.date																		AS dispenseDate,
		CASE WHEN isnull(ext_esb_prescription_d.nameProd,'') !='' THEN ext_esb_prescription_d.nameProd
		ELSE  isnull(fprod.design,'') END 																AS product,
		beneficiaryNr																					AS transactionId,
		ext_esb_prescription_d.ref																		AS cnp,
		ext_esb_prescription_d.totalPVU																	AS amountPaidUser,
		ext_esb_prescription_d.totalPVP																	AS pvp,
		ext_esb_prescription_d.totalEFR																	AS comparticipation,
		ext_esb_prescription_d.totalSNS																	AS comparticipationSNS
	FROM ext_esb_doc (nolock)
		inner join ext_esb_prescription	(nolock)	ON ext_esb_doc.stamp = ext_esb_prescription.stamp_ext_esb_doc
		inner join ext_esb_prescription_d (nolock)	ON ext_esb_prescription.stamp = ext_esb_prescription_d.stamp_ext_esb_prescription
		LEFT join fprod	(nolock)				    ON  ext_esb_prescription_d.ref= fprod.ref		
	where 
		ext_esb_doc.stamp=@stamp
	order by docNr, ext_esb_prescription_d.ref 

GO
GRANT EXECUTE ON dbo.up_get_InfoResumeFactInfo to Public
GRANT CONTROL ON dbo.up_get_InfoResumeFactInfo to Public
GO
