/*
	exec up_tipoVacinacao
*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_tipoVacinacao]') IS NOT NULL
	drop procedure dbo.up_tipoVacinacao
go

create procedure dbo.up_tipoVacinacao

AS
BEGIN	
	
		SELECT 
			id = 1
			,Descricao = 'Registo, no caso de as inoculações serem registadas no sistema e imediatamente enviadas'
	UNION ALL
		SELECT
			id = 2
			,Descricao = 'Transcrição, para registar vacinas administradas no passado.'

END

Grant Execute on dbo.up_tipoVacinacao to Public
Grant control on dbo.up_tipoVacinacao to Public
GO