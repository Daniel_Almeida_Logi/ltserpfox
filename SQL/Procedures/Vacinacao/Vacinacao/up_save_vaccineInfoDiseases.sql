/* 
	Procedere para guardar as informacoes do vaccineInfoDiseases
	exec up_save_vaccineInfoDiseases ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_vaccineInfoDiseases]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_vaccineInfoDiseases
GO

CREATE PROCEDURE dbo.up_save_vaccineInfoDiseases
	@stamp						VARCHAR(36),
	@token						VARCHAR(36),
	@id							INT,
	@description				VARCHAR(254)
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

		IF( NOT EXISTS (SELECT * FROM vaccineInfoDiseases (NOLOCK) WHERE stamp  = @stamp))
	BEGIN
		INSERT INTO vaccineInfoDiseases(stamp, token, id, description
									,ousrdata, ousrinis, usrdata, usrinis)
		VALUES (@stamp, @token, @id, @description
				,getdate(),'ADM',getdate(),'ADM')

	END 

GO
GRANT EXECUTE on dbo.up_save_vaccineInfoDiseases TO PUBLIC
GRANT Control on dbo.up_save_vaccineInfoDiseases TO PUBLIC
GO