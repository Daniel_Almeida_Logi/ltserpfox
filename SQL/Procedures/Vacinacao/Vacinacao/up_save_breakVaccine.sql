/* 
	Procedere para guardar as informacoes do sendVaccine_resp
	exec up_save_breakVaccine ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_breakVaccine]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_breakVaccine
GO

CREATE PROCEDURE dbo.up_save_breakVaccine

	@stamp				AS VARCHAR(36),
	@vaccineCode		AS VARCHAR(254),
	@type				AS INT,
	@date				AS DATE,
	@infarmedCode		AS VARCHAR(18),
	@dosage				AS NUMERIC,
	@units				AS INT,
	@lot				AS VARCHAR(50),
	@motiveId			AS INT,
	@destinyId			AS INT,
	@locationCd			AS INT,
	@location			AS VARCHAR(254),
	@professional		AS VARCHAR(254),
	@professionalCd		AS VARCHAR(254),
	@inis				AS VARCHAR(30)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	


	INSERT INTO sendVaccineBreak(stamp,vaccineCode,type,date,infarmedCode,dosage,units,lot,motiveId,destinyId
	,locationCd,location,professional,professionalCd
	,ousrdata,ousrinis,usrdata,usrinis
	)
	VALUES (@stamp, @vaccineCode, @type, @date, @infarmedCode, @dosage, @units, @lot, @motiveId, @destinyId,
			@locationCd, @location, @professional, @professionalCd
			,getdate(),@inis,getdate(),@inis)


GO
GRANT EXECUTE on dbo.up_save_breakVaccine TO PUBLIC
GRANT Control on dbo.up_save_breakVaccine TO PUBLIC
GO