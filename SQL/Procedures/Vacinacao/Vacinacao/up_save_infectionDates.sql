/* 
	Procedere para guardar as informacoes do infectionDates
	exec up_save_infectionDates ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_infectionDates]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_infectionDates
GO

CREATE PROCEDURE dbo.up_save_infectionDates
	@stamp			VARCHAR(36),
	@token			VARCHAR(36),
	@infectionDate	DATETIME,
	@numInfection	INTEGER
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

		IF( NOT EXISTS (SELECT * FROM infectionDates (NOLOCK) WHERE stamp  = @stamp))
	BEGIN
		INSERT INTO infectionDates(stamp, token, infectionDate, numInfection 
									,ousrdata, ousrinis, usrdata, usrinis)
		VALUES (@stamp, @token, @infectionDate, @numInfection
				,getdate(),'ADM',getdate(),'ADM')

	END 

GO
GRANT EXECUTE on dbo.up_save_infectionDates TO PUBLIC
GRANT Control on dbo.up_save_infectionDates TO PUBLIC
GO