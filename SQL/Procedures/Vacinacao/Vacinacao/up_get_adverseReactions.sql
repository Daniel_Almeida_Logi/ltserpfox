/* 
	
	exec up_get_adverseReactions '6e3dbf19-ccae-4064-894d-9460d0d1e1b'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_adverseReactions]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_adverseReactions
GO

CREATE PROCEDURE dbo.up_get_adverseReactions
	@token VARCHAR(36) 

AS

	SELECT 
		AdverseReactions.*
	FROM
		AdverseReactions(nolock)
		JOIN registrations(nolock) ON registrations.adverseReactionsToken = AdverseReactions.token
	WHERE
		registrations.token = @token

GO
GRANT EXECUTE on dbo.up_get_adverseReactions TO PUBLIC
GRANT Control on dbo.up_get_adverseReactions TO PUBLIC
GO