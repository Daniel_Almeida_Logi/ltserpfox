/* 
	Procedere para marcar vacina como enviada com sucessp
	exec up_updateSuccessVaccination '','01F70B8A-FF26-4A98-AFC0-4D8E12E0A693'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].up_updateSuccessVaccination') IS NOT NULL
	DROP PROCEDURE dbo.up_updateSuccessVaccination
GO

CREATE PROCEDURE dbo.up_updateSuccessVaccination
	
	@correlationId		VARCHAR(100) = '' ,
	@token 				VARCHAR(100) = ''
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	declare @saveStamp varchar(100) = ''
	declare @breakStamp varchar(100) = ''

	select 
		top 1 
		@saveStamp=rtrim(ltrim(sendVaccine.saveStamp)), 
		@breakStamp=rtrim(ltrim(sendVaccine.breakStamp)) 
	from 
		sendVaccine(nolock) 
	where
		sendVaccine.token =  @token
	and (sendVaccine.saveStamp!='' or sendVaccine.breakStamp!='')

	set @saveStamp = rtrim(ltrim(isnull(@saveStamp,'')))
	set @breakStamp = rtrim(ltrim(isnull(@breakStamp,'')))



	if(@saveStamp!='' or @breakStamp!='')
	begin
		UPDATE 
			b_vacinacao 
		SET  
			send= 1,
			result= 1,
			dateSend = getdate(),
			correlationId = ltrim(rtrim(@correlationId))
		where
			b_vacinacao.vacinacaostamp = @saveStamp or b_vacinacao.vacinacaostamp = @breakStamp
	end
	
GO
GRANT EXECUTE on dbo.up_updateSuccessVaccination TO PUBLIC
GRANT Control on dbo.up_updateSuccessVaccination TO PUBLIC
GO

