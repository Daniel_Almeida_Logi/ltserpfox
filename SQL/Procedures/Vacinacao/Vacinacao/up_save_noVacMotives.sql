/* 
	Procedere para guardar as informacoes do noVacMotives
	exec up_save_noVacMotives ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_noVacMotives]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_noVacMotives
GO

CREATE PROCEDURE dbo.up_save_noVacMotives
	@stamp						VARCHAR(36),
	@token						VARCHAR(36),
	@id							int,
	@vaccineCode				VARCHAR(50),
	@description				VARCHAR(254),
	@stratDate					VARCHAR(254),
	@endDate					VARCHAR(254),
	@noVaccinationMotiveGroup	VARCHAR(254),
	@noVaccinMotiveGroupDesc	VARCHAR(254),
	@obs						VARCHAR(254)
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

		IF( NOT EXISTS (SELECT * FROM sendVaccineRespNoVacMotives (NOLOCK) WHERE stamp  = @stamp))
	BEGIN
		INSERT INTO sendVaccineRespNoVacMotives(stamp, token,id, vaccineCode, description, stratDate, endDate, noVaccinMotiveGroup
								,noVaccinMotiveGroupDesc, obs, 
								ousrdata, ousrinis, usrdata, usrinis)
		VALUES (@stamp, @token, @id, @vaccineCode, @description, @stratDate, @endDate, @noVaccinationMotiveGroup
				,@noVaccinMotiveGroupDesc, @obs
				,getdate(),'ADM',getdate(),'ADM')

	END 

GO
GRANT EXECUTE on dbo.up_save_noVacMotives TO PUBLIC
GRANT Control on dbo.up_save_noVacMotives TO PUBLIC
GO