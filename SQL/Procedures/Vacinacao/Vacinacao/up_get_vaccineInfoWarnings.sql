/* 
	Procedere para retornar as informacoes do sendVaccineResp
	exec up_get_vaccineInfoWarnings 'acc4bb43-5b73-43fb-98cd-063e7c7e30f'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_vaccineInfoWarnings]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_vaccineInfoWarnings
GO

CREATE PROCEDURE dbo.up_get_vaccineInfoWarnings
	@token				VARCHAR(36)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT
		stamp
		,token
		,id
		,description
		,ousrdata
		,ousrinis
		,usrdata
		,usrinis
	FROM
		vaccineInfoWarnings(NOLOCK)
	WHERE
		token = @token

GO
GRANT EXECUTE on dbo.up_get_vaccineInfoWarnings TO PUBLIC
GRANT Control on dbo.up_get_vaccineInfoWarnings TO PUBLIC
GO