/* 
	Procedere para retornar as informacoes do sendVaccineResp
	exec up_get_commercialNameLots '8a69c29e-67e5-4b80-b0da-0bee74871fd'
	exec up_get_commercialNameLots ''
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_commercialNameLots]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_commercialNameLots
GO

CREATE PROCEDURE dbo.up_get_commercialNameLots
	@token				VARCHAR(36)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT
		code,
		CONVERT(DATETIME, endDate) AS endDate
	FROM
		commercialNameLots(NOLOCK)
	WHERE
		token = @token
		AND CONVERT(DATETIME, endDate) > GETDATE() -1
	ORDER BY
		code asc

GO
GRANT EXECUTE on dbo.up_get_commercialNameLots TO PUBLIC
GRANT Control on dbo.up_get_commercialNameLots TO PUBLIC
GO