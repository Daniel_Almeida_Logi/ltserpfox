/* 
	Procedere para guardar as informacoes do vaccineInfo
	exec up_save_vaccineInfo ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_vaccineInfo]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_vaccineInfo
GO

CREATE PROCEDURE dbo.up_save_vaccineInfo
	@stamp									VARCHAR(36),
	@token									VARCHAR(36),
	@id										INT,
	@code									VARCHAR(50),
	@name									VARCHAR(254),
	@vaccineTypeId							INT,
	@vaccineTypeDesc						VARCHAR(254),
	@vaccineType							VARCHAR(254),
	@vaccineInfoNoVaccinMotiveToken			VARCHAR(36),
	@vaccineInfoDiseasesToken				VARCHAR(36),
	@vaccineInfoWarningsToken				VARCHAR(36),
	@vaccineInfoAdverseReactionToken		VARCHAR(36),
	@vaccineInfoAnatomicLocationToken		VARCHAR(36),
	@vaccineInfoAdministrationWayToken		VARCHAR(36)
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

		IF( NOT EXISTS (SELECT * FROM vaccineInfo (NOLOCK) WHERE stamp  = @stamp))
	BEGIN
		INSERT INTO vaccineInfo(stamp, token, id, code, name, vaccineTypeId, vaccineTypeDesc, vaccineType
								,sendVaccineRespNoVacMotivesToken, vaccineInfoDiseasesToken, vaccineInfoWarningsToken
								,vaccineInfoAdverseReactionToken, vaccineInfoAnatomicLocationToken, vaccineInfoAdministrationWayToken
									,ousrdata, ousrinis, usrdata, usrinis)
		VALUES (@stamp, @token, @id, @code, @name, @vaccineTypeId, @vaccineTypeDesc, @vaccineType
				,@vaccineInfoNoVaccinMotiveToken, @vaccineInfoDiseasesToken, @vaccineInfoWarningsToken
				,@vaccineInfoAdverseReactionToken, @vaccineInfoAnatomicLocationToken, @vaccineInfoAdministrationWayToken
				,getdate(),'ADM',getdate(),'ADM')

	END 

GO
GRANT EXECUTE on dbo.up_save_vaccineInfo TO PUBLIC
GRANT Control on dbo.up_save_vaccineInfo TO PUBLIC
GO