/* 
	Procedere para retornar as informacoes do sendVaccineResp
	exec up_get_locationAdms 0

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_locationAdms]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_locationAdms
GO

CREATE PROCEDURE dbo.up_get_locationAdms
	@inactive				BIT

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT
		id
		,description
	 
	FROM
		locationAdms(NOLOCK)
	WHERE
		inactive = @inactive

GO
GRANT EXECUTE on dbo.up_get_locationAdms TO PUBLIC
GRANT Control on dbo.up_get_locationAdms TO PUBLIC
GO