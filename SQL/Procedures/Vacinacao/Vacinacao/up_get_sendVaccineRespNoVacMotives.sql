/* 
	Procedere para retornar as informacoes do sendVaccineResp
	exec up_get_sendVaccineRespNoVacMotives '519712aa-93ff-45c8-abcc-ef894a94579'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_sendVaccineRespNoVacMotives]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_sendVaccineRespNoVacMotives
GO

CREATE PROCEDURE dbo.up_get_sendVaccineRespNoVacMotives
	@token				VARCHAR(36)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT
		stamp
		,token
		,id
		,vaccineCode
		,description
		,stratDate
		,endDate
		,noVaccinMotiveGroup
		,noVaccinMotiveGroupDesc
		,obs
		,ousrdata
		,ousrinis
		,usrdata
		,usrinis
	FROM
		sendVaccineRespNoVacMotives(NOLOCK)
	WHERE
		token = @token

GO
GRANT EXECUTE on dbo.up_get_sendVaccineRespNoVacMotives TO PUBLIC
GRANT Control on dbo.up_get_sendVaccineRespNoVacMotives TO PUBLIC
GO