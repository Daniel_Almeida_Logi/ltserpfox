/* 
	Procedere para guardar as informacoes do commercialNameDosage
	exec up_save_commercialNameDosage ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_commercialNameDosage]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_commercialNameDosage
GO

CREATE PROCEDURE dbo.up_save_commercialNameDosage
	@stamp			VARCHAR(36),
	@token			VARCHAR(36),
	@dose			FLOAT

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON
	
		IF( NOT EXISTS (SELECT * FROM commercialNameDosage (NOLOCK) WHERE stamp  = @stamp))
	BEGIN
		INSERT INTO commercialNameDosage(stamp, token, dose
											,ousrdata, ousrinis, usrdata, usrinis)
		VALUES (@stamp, @token, @dose
				,getdate(),'ADM',getdate(),'ADM')

	END 

GO
GRANT EXECUTE on dbo.up_save_commercialNameDosage TO PUBLIC
GRANT Control on dbo.up_save_commercialNameDosage TO PUBLIC
GO