/* 
	Procedere para retornar as informacoes do sendVaccineResp
	exec up_get_vaccineInfoAnatomicLocation '1cb15066-dc99-4673-ae8b-4767757bbcc'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_vaccineInfoAnatomicLocation]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_vaccineInfoAnatomicLocation
GO

CREATE PROCEDURE dbo.up_get_vaccineInfoAnatomicLocation
	@token				VARCHAR(36)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT
		stamp
		,token
		,id
		,description
		,side
		,sideDesc
		,hasSide
		,ousrdata
		,ousrinis
		,usrdata
		,usrinis
	FROM
		vaccineInfoAnatomicLocation(NOLOCK)
	WHERE
		token = @token

GO
GRANT EXECUTE on dbo.up_get_vaccineInfoAnatomicLocation TO PUBLIC
GRANT Control on dbo.up_get_vaccineInfoAnatomicLocation TO PUBLIC
GO