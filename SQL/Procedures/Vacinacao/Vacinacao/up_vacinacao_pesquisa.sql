-- exec	up_vacinacao_pesquisa  '20110214','20200911','','', '', 3
-- exec	up_vacinacao_pesquisa  '','','',''
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_vacinacao_pesquisa]') IS NOT NULL
	drop procedure dbo.up_vacinacao_pesquisa
go


CREATE procedure dbo.up_vacinacao_pesquisa

@Dataini		datetime,
@Datafim		datetime,
@Cliente		varchar(60),
@Administrante	varchar(60),
@tipoReg		varchar(50) = '',
@site_nr		int

/* with encryption */

AS
SET NOCOUNT ON

	select sel=CAST(0 as bit),*
	From B_vacinacao (nolock)
	Where dataAdministracao between @Dataini and @DataFim
			and nome like '%'+@Cliente+'%'
			and Administrante like '%'+@Administrante+'%'
			and 1 = (CASE WHEN @tipoReg = '' THEN 1 ELSE (CASE WHEN tipoReg = @tipoReg THEN 1 ELSE 0 END) END)
			and site_nr = @site_nr
	order by convert(int,rtrim(no)) desc

	
Go
Grant Execute on dbo.up_vacinacao_pesquisa to Public
Grant Control on dbo.up_vacinacao_pesquisa to Public
Go