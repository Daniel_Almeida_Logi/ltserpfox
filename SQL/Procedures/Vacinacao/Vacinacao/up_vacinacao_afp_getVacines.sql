
-- Retorna dados a enviar pelo AFP connect relativo as vacinas
-- exec     up_vacinacao_afp_getVacines


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_vacinacao_afp_getVacines]') IS NOT NULL
      drop procedure dbo.up_vacinacao_afp_getVacines
go



CREATE procedure dbo.up_vacinacao_afp_getVacines

/* with encryption */

AS
SET NOCOUNT ON

      SELECT 
        "key" = isnull([key],'') 
        ,"stamp" = isnull([stamp],'')
      ,[nics]
      ,[sns]
      ,'vaccineCode' =  case when [commercialNameID] = '5686860' then 'GripeSNS'
						when [commercialNameID] = '5726229' then 'Gripe'
						when [commercialNameID] = '5651161' then 'Pn23'
						when [commercialNameID] = '5552047' then 'MenB'
						when [commercialNameID] = '5460662' then 'Men ACWY conjugada'
						when [commercialNameID] = '5658828' then 'HPV9'
						when [commercialNameID] = '2568186' then 'VHA'        		
						else ''  end
      ,[type]
      ,[date]
      ,[commercialNameID] 
      ,[dosage]
      ,[units]
      ,[lot]
      ,[anatomicLocationID]
      ,[administrationWayID]
      ,[obs]
      ,[locationMasterCd]
      ,"locationMaster" = isnull([locationMaster],'')
      ,[locationCd]
      ,"location" = isnull([location],'')
      ,[professionalCd]
      ,"professional" = isnull([professional],'')
      ,[enviado]
      ,[dateSend]
  FROM [dbo].[ext_esb_vaccines](nolock)
  where enviado=0 and ousrdata >= dateadd(day,-30, getdate())

Go
Grant Execute on dbo.up_vacinacao_afp_getVacines to Public
Grant Control on dbo.up_vacinacao_afp_getVacines to Public
Go

           