/* 
	Procedere para guardar as informacoes do sendVaccines_caches
	exec up_save_sendVaccines_caches ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_sendVaccines_caches]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_sendVaccines_caches
GO

CREATE PROCEDURE dbo.up_save_sendVaccines_caches

	@timeHours			NUMERIC(18,0)
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	INSERT INTO sendVaccinesCache(stamp, date, dateExp, timeHours
								,ousrdata, ousrinis, usrdata, usrinis)
	VALUES (LEFT(NEWID(),36), GETDATE(), DATEADD(HH,+ @timeHours,GETDATE()), @timeHours
			,getdate(),'ADM',getdate(),'ADM')

GO
GRANT EXECUTE on dbo.up_save_sendVaccines_caches TO PUBLIC
GRANT Control on dbo.up_save_sendVaccines_caches TO PUBLIC
GO