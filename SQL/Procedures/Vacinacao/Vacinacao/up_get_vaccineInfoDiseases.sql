/* 
	Procedere para retornar as informacoes do sendVaccineResp
	exec up_get_vaccineInfoDiseases '31dbc9b0-8ea3-478a-aa4c-f633fd91900'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_vaccineInfoDiseases]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_vaccineInfoDiseases
GO

CREATE PROCEDURE dbo.up_get_vaccineInfoDiseases
	@token				VARCHAR(36)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT
		stamp
		,token
		,id
		,description
		,ousrdata
		,ousrinis
		,usrdata
		,usrinis
	FROM
		vaccineInfoDiseases(NOLOCK)
	WHERE
		token = @token

GO
GRANT EXECUTE on dbo.up_get_vaccineInfoDiseases TO PUBLIC
GRANT Control on dbo.up_get_vaccineInfoDiseases TO PUBLIC
GO