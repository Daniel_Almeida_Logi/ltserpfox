/* 
	Procedere para retornar as informacoes do sendVaccineResp
	exec up_get_infectionDates '519712aa-93ff-45c8-abcc-ef894a94579'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_infectionDates]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_infectionDates
GO

CREATE PROCEDURE dbo.up_get_infectionDates
	@token				VARCHAR(36)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT
		stamp
		,token
		,infectionDate
		,numInfection
		,ousrdata
		,ousrinis
		,usrdata
		,usrinis	 
	FROM
		infectionDates(NOLOCK)
	WHERE
		token = @token

GO
GRANT EXECUTE on dbo.up_get_infectionDates TO PUBLIC
GRANT Control on dbo.up_get_infectionDates TO PUBLIC
GO