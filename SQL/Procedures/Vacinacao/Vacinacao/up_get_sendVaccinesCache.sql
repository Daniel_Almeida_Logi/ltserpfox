/* 
	Procedere para retornar as informacoes do sendVaccineResp
	exec up_get_sendVaccinesCache 

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_sendVaccinesCache]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_sendVaccinesCache
GO

CREATE PROCEDURE dbo.up_get_sendVaccinesCache

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT
		stamp
		,date
		,dateExp
		,timeHours
		,ousrdata
		,ousrinis
		,usrdata
		,usrinis
	FROM
		sendVaccinesCache(NOLOCK)

GO
GRANT EXECUTE on dbo.up_get_sendVaccinesCache TO PUBLIC
GRANT Control on dbo.up_get_sendVaccinesCache TO PUBLIC
GO