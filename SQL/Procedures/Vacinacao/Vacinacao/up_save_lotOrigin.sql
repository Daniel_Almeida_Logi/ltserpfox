/* 
	Procedere para guardar as informacoes do lotOrigin
	exec up_save_lotOrigin ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_lotOrigin]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_lotOrigin
GO

CREATE PROCEDURE dbo.up_save_lotOrigin
	@stamp			VARCHAR(36),
	@id				INT,
	@description	VARCHAR(254),
	@inactive		BIT
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	IF( NOT EXISTS (SELECT * FROM lotOrigin (NOLOCK) WHERE id  = @id))
	BEGIN
		INSERT INTO lotOrigin(stamp, id, description, inactive
									,ousrdata, ousrinis, usrdata, usrinis)
		VALUES (@stamp, @id, @description,  @inactive
				,getdate(),'ADM',getdate(),'ADM')

	END 
	ELSE
	BEGIN
		UPDATE lotOrigin SET inactive=@inactive, description = @description WHERE id  = @id
	END

GO
GRANT EXECUTE on dbo.up_save_lotOrigin TO PUBLIC
GRANT Control on dbo.up_save_lotOrigin TO PUBLIC
GO