/* 
	Procedere para guardar as informacoes do registrations
	exec up_save_registrations ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_registrations]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_registrations
GO

CREATE PROCEDURE dbo.up_save_registrations
	@stamp						VARCHAR(36),
	@token						VARCHAR(36),
	@adverseReactionsToken		VARCHAR(36),
	@vaccineCode				VARCHAR(50),
	@date						DATE,
	@inocNumber					INT,
	@comercialNameDesc			VARCHAR(254),
	@infarmedCode				INT,
	@last						BIT
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

		IF( NOT EXISTS (SELECT * FROM registrations (NOLOCK) WHERE stamp  = @stamp))
	BEGIN
		INSERT INTO registrations(stamp, token, adverseReactionsToken,  vaccineCode, date, inocNumber, comercialNameDesc, infarmedCode
									,ousrdata, ousrinis, usrdata, usrinis, last)
		VALUES (@stamp, @token, @adverseReactionsToken,  @vaccineCode, @date, @inocNumber, @comercialNameDesc, @infarmedCode
				,getdate(),'ADM',getdate(),'ADM',@last)

	END 

GO
GRANT EXECUTE on dbo.up_save_registrations TO PUBLIC
GRANT Control on dbo.up_save_registrations TO PUBLIC
GO