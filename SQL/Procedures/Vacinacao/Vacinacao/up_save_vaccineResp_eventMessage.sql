/* 
	Procedere para guardar as informacoes do vaccineResp_eventMessage
	exec up_save_vaccineResp_eventMessage ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_vaccineResp_eventMessage]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_vaccineResp_eventMessage
GO

CREATE PROCEDURE dbo.up_save_vaccineResp_eventMessage
	@stamp			VARCHAR(36),
	@token			VARCHAR(36),
	@code			VARCHAR(254),
	@description	VARCHAR(254)
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

		IF( NOT EXISTS (SELECT * FROM sendVaccineRespEventMessage (NOLOCK) WHERE stamp  = @stamp))
	BEGIN
		INSERT INTO sendVaccineRespEventMessage(stamp, token, code, description
									,ousrdata, ousrinis, usrdata, usrinis)
		VALUES (@stamp, @token, @code, @description
				,getdate(),'ADM',getdate(),'ADM')

	END 

GO
GRANT EXECUTE on dbo.up_save_vaccineResp_eventMessage TO PUBLIC
GRANT Control on dbo.up_save_vaccineResp_eventMessage TO PUBLIC
GO