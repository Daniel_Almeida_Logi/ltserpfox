/* 
	Procedere para guardar as informacoes do vaccineInfoAnatomicLocation
	exec up_save_vaccineInfoAnatomicLocation ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_vaccineInfoAnatomicLocation]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_vaccineInfoAnatomicLocation
GO

CREATE PROCEDURE dbo.up_save_vaccineInfoAnatomicLocation
	@stamp						VARCHAR(36),
	@token						VARCHAR(36),
	@id							INT,
	@description				VARCHAR(254),
	@side						INT,
	@sideDesc					VARCHAR(254),
	@hasSide					BIT
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

		IF( NOT EXISTS (SELECT * FROM vaccineInfoAnatomicLocation (NOLOCK) WHERE stamp  = @stamp))
	BEGIN
		INSERT INTO vaccineInfoAnatomicLocation(stamp, token, id, description, side, sideDesc, hasSide
									,ousrdata, ousrinis, usrdata, usrinis)
		VALUES (@stamp, @token, @id, @description, @side, @sideDesc, @hasSide
				,getdate(),'ADM',getdate(),'ADM')

	END 

GO
GRANT EXECUTE on dbo.up_save_vaccineInfoAnatomicLocation TO PUBLIC
GRANT Control on dbo.up_save_vaccineInfoAnatomicLocation TO PUBLIC
GO