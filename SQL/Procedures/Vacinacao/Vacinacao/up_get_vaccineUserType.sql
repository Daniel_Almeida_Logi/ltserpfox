/* 
	Procedere para retornar as informacoes do sendVaccineResp
	exec up_get_vaccineUserType 0

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_vaccineUserType]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_vaccineUserType
GO

CREATE PROCEDURE dbo.up_get_vaccineUserType
	@inactive				BIT

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT
		id
		,description	 
	FROM
		vaccineUserType(NOLOCK)
	WHERE
		inactive = @inactive

GO
GRANT EXECUTE on dbo.up_get_vaccineUserType TO PUBLIC
GRANT Control on dbo.up_get_vaccineUserType TO PUBLIC
GO