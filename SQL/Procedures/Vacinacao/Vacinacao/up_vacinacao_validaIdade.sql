/* 
	
	Verifica se tem de validar a idade de utente para este tipo de utente
	EXEC up_vacinacao_validaIdade '130'


*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_vacinacao_validaIdade]') IS NOT NULL
	DROP PROCEDURE dbo.up_vacinacao_validaIdade
GO

CREATE PROCEDURE dbo.up_vacinacao_validaIdade	
	@typeUtente VARCHAR(100)

AS

	SELECT 
		CASE 
			WHEN @typeUtente IN ('130')
				THEN CAST( 0 AS bit)
			ELSE
				CAST( 1 AS bit)
		END as valIdade

GO
GRANT EXECUTE on dbo.up_vacinacao_validaIdade TO PUBLIC
GRANT Control on dbo.up_vacinacao_validaIdade TO PUBLIC
GO