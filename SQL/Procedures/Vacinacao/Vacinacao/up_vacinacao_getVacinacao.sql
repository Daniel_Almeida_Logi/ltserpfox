/* 

	EXEC up_vacinacao_getVacinacao ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_vacinacao_getVacinacao]') IS NOT NULL
	DROP PROCEDURE dbo.up_vacinacao_getVacinacao
GO

CREATE PROCEDURE dbo.up_vacinacao_getVacinacao
	@no VARCHAR(5) = ''

AS
BEGIN

	SELECT 
		b_vacinacao.*,  
		vias_admin.descr,
		ISNULL((SELECT campo FROM b_multidata(nolock) WHERE tipo='VACINACAO_MOTIVO_QUEBRA' AND code = b_vacinacao.motiveId), '') as motivoBreak,
		ISNULL((SELECT campo FROM b_multidata(nolock) WHERE tipo='VACINACAO_DESTINO' AND code = b_vacinacao.destinyId), '') as destinoBreak,
		(CASE WHEN B_vacinacao.send = 1 AND B_vacinacao.result = 1 THEN CAST(1 as BIT) ELSE CAST(0 as BIT) END) as comunicado
	FROM 
		b_vacinacao (nolock)
		LEFT JOIN vias_admin(nolock) on b_vacinacao.viaAdmin_id = vias_admin.id
	WHERE
		no = @no

END

GO
GRANT EXECUTE on dbo.up_vacinacao_getVacinacao TO PUBLIC
GRANT Control on dbo.up_vacinacao_getVacinacao TO PUBLIC
GO
