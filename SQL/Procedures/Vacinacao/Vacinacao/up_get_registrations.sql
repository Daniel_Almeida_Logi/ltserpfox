/* 
	Procedere para retornar as informacoes do sendVaccineResp
	exec up_get_registrations '2c71f303-70ef-4572-a50c-d464a907f02'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_registrations]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_registrations
GO

CREATE PROCEDURE dbo.up_get_registrations
	@token				VARCHAR(36)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT
		distinct
		token
		,vaccineCode
		,date
		,inocNumber
		,comercialNameDesc
		,infarmedCode
		,adverseReactionsToken
	FROM
		registrations(NOLOCK)
	WHERE
		token = @token
	ORDER BY
		date DESC

GO
GRANT EXECUTE on dbo.up_get_registrations TO PUBLIC
GRANT Control on dbo.up_get_registrations TO PUBLIC
GO