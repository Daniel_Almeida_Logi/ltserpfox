/* 
	Procedere para retornar as informacoes do sendVaccineResp
	exec up_get_vaccineInfo 'd0f0f737-6ee3-4bb2-8f24-adb2bcf7980'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_vaccineInfo]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_vaccineInfo
GO

CREATE PROCEDURE dbo.up_get_vaccineInfo
	@token				VARCHAR(36)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT
		stamp
		,token
		,id
		,code
		,name
		,vaccineTypeId
		,vaccineTypeDesc
		,vaccineType
		,sendVaccineRespNoVacMotivesToken
		,vaccineInfoDiseasesToken
		,vaccineInfoWarningsToken
		,vaccineInfoAdverseReactionToken
		,vaccineInfoAnatomicLocationToken
		,vaccineInfoAdministrationWayToken
		,ousrdata
		,ousrinis
		,usrdata
		,usrinis
	FROM
		vaccineInfo(NOLOCK)
	WHERE
		token = @token

GO
GRANT EXECUTE on dbo.up_get_vaccineInfo TO PUBLIC
GRANT Control on dbo.up_get_vaccineInfo TO PUBLIC
GO