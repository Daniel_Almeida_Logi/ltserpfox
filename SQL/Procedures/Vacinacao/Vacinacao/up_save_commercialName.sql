/* 
	Procedere para guardar as informacoes do commercialName
	exec up_save_commercialName ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_commercialName]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_commercialName
GO

CREATE PROCEDURE dbo.up_save_commercialName
	@stamp						VARCHAR(36),
	@token						VARCHAR(36),
	@id							INT,
	@name						VARCHAR(254),
	@gender						VARCHAR(254),
	@adminisForeign				BIT,
	@certificate				BIT,
	@active						BIT,
	@commercialNameDosageToken	VARCHAR(36),
	@commercialNameLotsToken	VARCHAR(36),
	@commercialNameCodeToken	VARCHAR(36)
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

		IF( NOT EXISTS (SELECT * FROM commercialName (NOLOCK) WHERE stamp  = @stamp))
	BEGIN
		INSERT INTO commercialName(stamp, token, id, name, gender, adminisForeign, certificate, active
									,commercialNameDosageToken, commercialNameLotsToken ,commercialNameCodeToken
									,ousrdata, ousrinis, usrdata, usrinis)
		VALUES (@stamp, @token, @id, @name, @gender, @adminisForeign, @certificate, @active
				,@commercialNameDosageToken, @commercialNameLotsToken, @commercialNameCodeToken
				,getdate(),'ADM',getdate(),'ADM')

	END 

GO
GRANT EXECUTE on dbo.up_save_commercialName TO PUBLIC
GRANT Control on dbo.up_save_commercialName TO PUBLIC
GO