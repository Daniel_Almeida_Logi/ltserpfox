/* 
	Procedere para guardar as informacoes do sendVaccine_resp
	exec up_save_sendVaccine_resp ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_sendVaccine_resp]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_sendVaccine_resp
GO

CREATE PROCEDURE dbo.up_save_sendVaccine_resp
	@token						VARCHAR(36),
	@correlationId				VARCHAR(254),
	@statusCode					INT,
	@statusCodeDesc				VARCHAR(254),
	@vaccineInfoToken			VARCHAR(36),
	@commercialNameToken		VARCHAR(36),
	@lastRegistrationStamp		VARCHAR(36),
	@registrationsToken			VARCHAR(36),
	@noVacMotivesToken			VARCHAR(36),
	@infectionDatesToken		VARCHAR(36),
	@patiObs					VARCHAR(254)
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

		IF( NOT EXISTS (SELECT * FROM sendVaccineResp (NOLOCK) WHERE token  = @token))
	BEGIN
		INSERT INTO sendVaccineResp(token, correlationId, statusCode, statusCodeDesc, vaccineInfoToken
									,commercialNameToken, lastRegistrationStamp, registrationsToken, sendVaccineRespNoVacMotivesToken
									,infectionDatesToken,patiObs
									,ousrdata, ousrinis, usrdata, usrinis)
		VALUES (@token, @correlationId, @statusCode, @statusCodeDesc, @vaccineInfoToken
				,@commercialNameToken, @lastRegistrationStamp, @registrationsToken, @noVacMotivesToken
				,@infectionDatesToken, @patiObs
				,getdate(),'ADM',getdate(),'ADM')
	END 

GO
GRANT EXECUTE on dbo.up_save_sendVaccine_resp TO PUBLIC
GRANT Control on dbo.up_save_sendVaccine_resp TO PUBLIC
GO