/* 
	Procedere para guardar as informacoes do anatomicLocations
	exec up_save_vaccineUserType ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_vaccineUserType]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_vaccineUserType
GO

CREATE PROCEDURE dbo.up_save_vaccineUserType
	@stamp			VARCHAR(36),
	@id				INT,
	@code			VARCHAR(254),
	@description	VARCHAR(254),
	@UserValue		INT,
	@inactive		BIT
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	IF( NOT EXISTS (SELECT * FROM vaccineUserType (NOLOCK) WHERE  id  = @id))
	BEGIN
		INSERT INTO vaccineUserType(stamp, id, code, description, UserValue, inactive,
									ousrdata, ousrinis, usrdata, usrinis)
		VALUES (@stamp, @id, @code, @description, @UserValue, @inactive
				,getdate(),'ADM',getdate(),'ADM')
	END 
	ELSE
	BEGIN
		UPDATE vaccineUserType SET inactive=@inactive, description = @description WHERE id  = @id
	END
GO
GRANT EXECUTE on dbo.up_save_vaccineUserType TO PUBLIC
GRANT Control on dbo.up_save_vaccineUserType TO PUBLIC
GO