/* 
	Procedere para guardar as informacoes do commercialNameLots
	exec up_save_commercialNameLots ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_commercialNameLots]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_commercialNameLots
GO

CREATE PROCEDURE dbo.up_save_commercialNameLots
	@stamp			VARCHAR(36),
	@token			VARCHAR(36),
	@code			VARCHAR(100),
	@endDate		DATE

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

		IF( NOT EXISTS (SELECT * FROM commercialNameLots (NOLOCK) WHERE stamp  = @stamp))
	BEGIN
		INSERT INTO commercialNameLots(stamp, token, code, endDate
											,ousrdata, ousrinis, usrdata, usrinis)
		VALUES (@stamp, @token, @code, @endDate
				,getdate(),'ADM',getdate(),'ADM')

	END 

GO
GRANT EXECUTE on dbo.up_save_commercialNameLots TO PUBLIC
GRANT Control on dbo.up_save_commercialNameLots TO PUBLIC
GO