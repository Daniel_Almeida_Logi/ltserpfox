/* 
	Procedere para guardar as informacoes do sendVaccines_caches
	exec up_updateInactiveTablesVaccinesGeneral 

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_updateInactiveTablesVaccinesGeneral]') IS NOT NULL
	DROP PROCEDURE dbo.up_updateInactiveTablesVaccinesGeneral
GO

CREATE PROCEDURE dbo.up_updateInactiveTablesVaccinesGeneral
	
	@table		VARCHAR(100) = '',
	@inactive	BIT
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	
	
	IF(@table = 'anatomicLocations')
	BEGIN
		UPDATE  anatomicLocations  SET inactive = @inactive
	END
	IF(@table = 'administrationWays')
	BEGIN
		UPDATE	administrationWays SET inactive = @inactive
	END
	IF(@table = 'lotOrigin')
	BEGIN
		UPDATE	lotOrigin SET inactive = @inactive
	END
	IF(@table = 'locationAdms')
	BEGIN
		UPDATE	locationAdms SET inactive = @inactive
	END
	IF(@table = 'side')
	BEGIN
		UPDATE	side SET inactive = @inactive
	END
	IF(@table = '')
	BEGIN
		UPDATE  anatomicLocations  SET inactive = @inactive
		UPDATE	administrationWays SET inactive = @inactive
		UPDATE	lotOrigin		   SET inactive = @inactive
		UPDATE	locationAdms	   SET inactive = @inactive
		UPDATE	side			   SET inactive = @inactive
	END		 
	
GO
GRANT EXECUTE on dbo.up_updateInactiveTablesVaccinesGeneral TO PUBLIC
GRANT Control on dbo.up_updateInactiveTablesVaccinesGeneral TO PUBLIC
GO