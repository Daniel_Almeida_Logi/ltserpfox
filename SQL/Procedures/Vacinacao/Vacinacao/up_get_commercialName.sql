/* 
	Procedere para retornar as informacoes do sendVaccineResp
	exec up_get_commercialName 'bb72a293-bfd1-44de-b357-381c9f66558', '5440987'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_commercialName]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_commercialName
GO

CREATE PROCEDURE dbo.up_get_commercialName
	@token				VARCHAR(36),
	@ref				VARCHAR(20) = ''

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT
		commercialName.stamp
		,commercialName.token
		,id
		,name
		,gender
		,adminisForeign
		,certificate
		,active
		,commercialNameDosageToken
		,commercialNameLotsToken
		,commercialNameCodeToken
		,commercialName.ousrdata
		,commercialName.ousrinis
		,commercialName.usrdata
		,commercialName.usrinis
		,commercialNameCode.code
	FROM
		commercialName(NOLOCK)
	join 
		commercialNameCode on commercialName.commercialNameCodeToken = commercialNameCode.token
	WHERE
		commercialName.token = @token
		and commercialName.active = 1
		and commercialNameCode.code = (CASE WHEN @ref <> '' THEN @ref ELSE commercialNameCode.code END)
	order by commercialName.ousrdata desc

GO
GRANT EXECUTE on dbo.up_get_commercialName TO PUBLIC
GRANT Control on dbo.up_get_commercialName TO PUBLIC
GO