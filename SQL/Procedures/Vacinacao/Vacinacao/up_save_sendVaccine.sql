/* 
	Procedere para guardar as informacoes do sendVaccine_resp
	exec up_save_sendVaccine ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_sendVaccine]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_sendVaccine
GO

CREATE PROCEDURE dbo.up_save_sendVaccine
	@token						VARCHAR(36),
	@search						VARCHAR(18),
	@profId						VARCHAR(254),
	@profName					VARCHAR(254),
	@code						VARCHAR(254),
	@senderAssoc				VARCHAR(10),
	@senderAssocId				VARCHAR(25),
	@senderId					VARCHAR(25),
	@senderName					VARCHAR(100),
	@type						INT,
	@typeDesc					VARCHAR(50),
	@typeSend					INT,
	@typeSendDesc				VARCHAR(50),
	@teste						BIT,
	@site						VARCHAR(60),
	@saveStamp					VARCHAR(36),
	@breakStamp					VARCHAR(36),
	@inis						VARCHAR(30),
	@infarmedCode VARCHAR(18),
	@dosage FLOAT,
	@units INT,
	@motiveID INT,
	@destinyId INT,
	@removeStamp VARCHAR(36) = '',
	@motivoAnulado TEXT = ''

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	


		INSERT INTO sendVaccine(token, search, profId, profName
								,code, senderAssoc, senderAssocId, senderId
								, senderName, type, typeDesc, typeSend
								, typeSendDesc, teste, site, saveStamp, breakStamp
								,ousrdata, ousrinis, usrdata, usrinis, 
								infarmedCode, dosage, units, motiveID, destinyId, removeStamp, motivoAnulado)
		VALUES (@token, @search,  @profId, @profName
				,@code, @senderAssoc, @senderAssocId, @senderId
				,@senderName, @type, @typeDesc, @typeSend
				,@typeSendDesc, @teste,@site,  @saveStamp, @breakStamp
				,getdate(),@inis,getdate(),@inis, 
				@infarmedCode, @dosage,	@units, @motiveID, @destinyId, @removeStamp, @motivoAnulado)


GO
GRANT EXECUTE on dbo.up_save_sendVaccine TO PUBLIC
GRANT Control on dbo.up_save_sendVaccine TO PUBLIC
GO