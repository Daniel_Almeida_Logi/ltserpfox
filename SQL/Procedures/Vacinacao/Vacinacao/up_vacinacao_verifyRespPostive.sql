/* 
	Procedere para verificar se teve resposta e foi comunicada com sucesso
	exec up_vacinacao_verifyRespPostive 'ADM111A111A-11A1-BB-A18'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_vacinacao_verifyRespPostive]') IS NOT NULL
	DROP PROCEDURE dbo.up_vacinacao_verifyRespPostive
GO

CREATE PROCEDURE dbo.up_vacinacao_verifyRespPostive
	@stamp				VARCHAR(36)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT TOP 1  saveStamp
	FROM 
		sendVaccine(nolock) 
	INNER JOIN sendVaccineResp on sendVaccine.token = sendVaccineResp.token		
	WHERE sendVaccine.saveStamp  = @stamp 
			AND  sendVaccineResp.statusCode='200'
			AND typeSend=3
			AND sendVaccine.saveStamp!= ''
	ORDER BY sendVaccine.ousrdata desc

GO
GRANT EXECUTE on dbo.up_vacinacao_verifyRespPostive TO PUBLIC
GRANT Control on dbo.up_vacinacao_verifyRespPostive TO PUBLIC
GO