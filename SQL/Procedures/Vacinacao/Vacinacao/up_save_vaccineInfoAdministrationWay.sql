/* 
	Procedere para guardar as informacoes do vaccineInfoAdministrationWay
	exec up_save_vaccineInfoAdministrationWay ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_vaccineInfoAdministrationWay]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_vaccineInfoAdministrationWay
GO

CREATE PROCEDURE dbo.up_save_vaccineInfoAdministrationWay
	@stamp						VARCHAR(36),
	@token						VARCHAR(36),
	@id							INT,
	@description				VARCHAR(254)
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

		IF( NOT EXISTS (SELECT * FROM vaccineInfoAdministrationWay (NOLOCK) WHERE stamp  = @stamp))
	BEGIN
		INSERT INTO vaccineInfoAdministrationWay(stamp, token, id, description
									,ousrdata, ousrinis, usrdata, usrinis)
		VALUES (@stamp, @token, @id, @description
				,getdate(),'ADM',getdate(),'ADM')

	END 

GO
GRANT EXECUTE on dbo.up_save_vaccineInfoAdministrationWay TO PUBLIC
GRANT Control on dbo.up_save_vaccineInfoAdministrationWay TO PUBLIC
GO