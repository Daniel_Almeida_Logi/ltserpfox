/* 
	Procedere para retornar as informacoes do sendVaccineResp
	exec up_get_lotOrigin 0

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_lotOrigin]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_lotOrigin
GO

CREATE PROCEDURE dbo.up_get_lotOrigin
	@inactive				BIT

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT
		id
		,description
		 
	FROM
		lotOrigin(NOLOCK)
	WHERE
		inactive = @inactive

GO
GRANT EXECUTE on dbo.up_get_lotOrigin TO PUBLIC
GRANT Control on dbo.up_get_lotOrigin TO PUBLIC
GO