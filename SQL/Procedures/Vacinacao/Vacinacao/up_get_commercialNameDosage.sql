/* 
	Procedere para retornar as informacoes do sendVaccineResp
	exec up_get_commercialNameDosage ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_commercialNameDosage]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_commercialNameDosage
GO

CREATE PROCEDURE dbo.up_get_commercialNameDosage
	@token				VARCHAR(36)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT
		stamp
		,token
		,dose
		,ousrdata
		,ousrinis
		,usrdata
		,usrinis
	FROM
		commercialNameDosage(NOLOCK)
	WHERE
		token = @token

GO
GRANT EXECUTE on dbo.up_get_commercialNameDosage TO PUBLIC
GRANT Control on dbo.up_get_commercialNameDosage TO PUBLIC
GO