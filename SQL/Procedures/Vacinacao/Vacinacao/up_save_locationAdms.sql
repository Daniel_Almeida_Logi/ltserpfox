/* 
	Procedere para guardar as informacoes do locationAdms
	exec up_save_locationAdms ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_locationAdms]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_locationAdms
GO

CREATE PROCEDURE dbo.up_save_locationAdms
	@stamp			VARCHAR(36),
	@id				INT,
	@description	VARCHAR(254),
	@inactive		BIT
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	IF( NOT EXISTS (SELECT * FROM locationAdms (NOLOCK) WHERE id  = @id))
	BEGIN
		INSERT INTO locationAdms(stamp, id, description, inactive
									,ousrdata, ousrinis, usrdata, usrinis)
		VALUES (@stamp, @id, @description,  @inactive
				,getdate(),'ADM',getdate(),'ADM')
	END 
	ELSE
	BEGIN
		UPDATE locationAdms SET inactive=@inactive, description = @description WHERE id  = @id
	END
GO
GRANT EXECUTE on dbo.up_save_locationAdms TO PUBLIC
GRANT Control on dbo.up_save_locationAdms TO PUBLIC
GO