/* 
	Procedere para guardar as informacoes do commercialNameCode
	exec up_save_commercialNameCode ''

	commercialName
	sendvaccine
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_commercialNameCode]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_commercialNameCode
GO

CREATE PROCEDURE dbo.up_save_commercialNameCode
	@stamp			VARCHAR(36),
	@token			VARCHAR(36),
	@code			VARCHAR(254)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

		IF( NOT EXISTS (SELECT * FROM commercialNameCode (NOLOCK) WHERE stamp  = @stamp))
	BEGIN
		INSERT INTO commercialNameCode(stamp, token, code, description
											,ousrdata, ousrinis, usrdata, usrinis)
		VALUES (@stamp, @token, @code, ''
				,getdate(),'ADM',getdate(),'ADM')

	END 

GO
GRANT EXECUTE on dbo.up_save_commercialNameCode TO PUBLIC
GRANT Control on dbo.up_save_commercialNameCode TO PUBLIC
GO