/* 
	Procedere para retornar as informacoes do sendVaccineResp
	exec up_get_commercialNameCode 'c511c4e7-cfe2-44b6-ac77-5bce19b162a'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_commercialNameCode]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_commercialNameCode
GO

CREATE PROCEDURE dbo.up_get_commercialNameCode
	@token				VARCHAR(36)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT
		stamp
		,token
		,code
		,description
		,ousrdata
		,ousrinis
		,usrdata
		,usrinis
	FROM
		commercialNameCode(NOLOCK)
	WHERE
		token = @token

GO
GRANT EXECUTE on dbo.up_get_commercialNameCode TO PUBLIC
GRANT Control on dbo.up_get_commercialNameCode TO PUBLIC
GO