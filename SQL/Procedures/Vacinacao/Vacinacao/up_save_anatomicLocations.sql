/* 
	Procedere para guardar as informacoes do anatomicLocations
	exec up_save_anatomicLocations ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_anatomicLocations]') IS NOT NULL
	DROP PROCEDURE dbo.up_save_anatomicLocations
GO

CREATE PROCEDURE dbo.up_save_anatomicLocations
	@stamp			VARCHAR(36),
	@id				INT,
	@description	VARCHAR(254),
	@side			INT,
	@sideDesc		VARCHAR(254),
	@hasSide		BIT,
	@inactive		BIT
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	IF( NOT EXISTS (SELECT * FROM anatomicLocations (NOLOCK) WHERE  id  = @id))
	BEGIN
		INSERT INTO anatomicLocations(stamp, id, description, side, sideDesc, hasSide, inactive
									,ousrdata, ousrinis, usrdata, usrinis)
		VALUES (@stamp, @id, @description, @side, @sideDesc, @hasSide, @inactive
				,getdate(),'ADM',getdate(),'ADM')
	END 
	ELSE
	BEGIN
		UPDATE anatomicLocations SET inactive=@inactive, description = @description WHERE id  = @id
	END
GO
GRANT EXECUTE on dbo.up_save_anatomicLocations TO PUBLIC
GRANT Control on dbo.up_save_anatomicLocations TO PUBLIC
GO