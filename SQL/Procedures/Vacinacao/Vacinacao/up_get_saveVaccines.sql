/* 
	Procedere para retornar as informacoes do sendVaccineResp
	exec up_get_saveVaccines 0
	 exec up_get_saveVaccines 'ADM00151AD4-C668-4A5F-9C2' 
*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_saveVaccines]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_saveVaccines
GO

CREATE PROCEDURE dbo.up_get_saveVaccines
	@stamp				VARCHAR(36)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT
		ISNULL(vacinacaostamp,'')					AS vacinacaostamp
		,ISNULL(no,'0')								AS no
		,ISNULL(Administrante,'')					AS administrante
		,dataAdministracao							AS dataAdministracao
		,ISNULL(nome,'')							AS nome
		,dataNasc									AS dataNasc
		,ISNULL(contacto,'')						AS contacto
		,ISNULL(Descricao,'')						AS descricao
		,ISNULL(lote,'')							AS lote
		,ISNULL(nrVenda,'')							AS nrVenda
		,ISNULL(obs,'')								AS obs
		,ISNULL(ousrinis,'')						AS ousrinis
		,ousrdata									AS ousrdata
		,ousrhora									AS ousrhora
		,ISNULL(usrinis,'')							AS usrinis
		,usrdata									AS usrdata
		,usrhora									AS usrhora
		,ISNULL(nrSns,'0')							AS nrSns
		,ISNULL(codeVacina,'')						AS codeVacina
		,ISNULL(unidades,0)							AS unidades
		,ISNULL(dosagem,0)							AS dosagem
		,ISNULL(localAnatomico_id,'0')				AS localAnatomico_id
		,ISNULL(viaAdmin_id,'0')					AS viaAdmin_id
		,ISNULL(site_nr,0)							AS site_nr
		,dateSend									AS dateSend
		,send										AS send
		,ISNULL(comercial_id,'')					AS comercial_id
		,ISNULL(result,0)							AS result
		,autResidencia								AS autResidencia
		,ISNULL(typeUtente,'')						AS typeUtente
		,ISNULL(localAdmistracao,'')				AS localAdmistracao
		,ISNULL(numInoculacao,'0')					AS numInoculacao
		,ISNULL(typeVacinacao,'')					AS typeVacinacao
		,ISNULL(lateralidade,'0')					AS lateralidade
		,ISNULL(typeUtenteDesc,'')					AS typeUtenteDesc
		,ISNULL(localAdmistracaoDesc,'')			AS localAdmistracaoDesc
		,ISNULL(typeVacinacaoDesc,'')				AS typeVacinacaoDesc
		,ISNULL(lateralidadeDesc,'')				AS lateralidadeDesc
		,ISNULL(localAnatomico_idDesc,'')			AS localAnatomico_idDesc
		,ISNULL(viaAdminDesc,'')					AS viaAdminDesc
		,ISNULL(tipoReg,'')							AS tipoReg
		,validade		 							AS validade
		,motiveId		 							AS motiveId
		,destinyId		 							AS destinyId
		,ISNULL(correlationId,'')		 			AS correlationId
		,nics										AS nics	
		,anulado									AS anulado	
		,ISNULL(motivoAnulado,'')					AS motivoAnulado
		,ISNULL(usrAnulado,'')						AS usrAnulado
		,ISNULL(profIdAnulado,'')					AS profIdAnulado
		,dataAnulado								AS dataAnulado
	FROM
		b_vacinacao (nolock)
	WHERE
		vacinacaostamp = @stamp

GO
GRANT EXECUTE on dbo.up_get_saveVaccines TO PUBLIC
GRANT Control on dbo.up_get_saveVaccines TO PUBLIC
GO