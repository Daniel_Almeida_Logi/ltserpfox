/* 
	Procedere para retornar as informacoes do sendVaccineResp
	exec up_get_anatomicLocations 0

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_anatomicLocations]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_anatomicLocations
GO

CREATE PROCEDURE dbo.up_get_anatomicLocations
	@inactive				BIT

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT
		id
		,description
		 
	FROM
		anatomicLocations(NOLOCK)
	WHERE
		inactive = @inactive

GO
GRANT EXECUTE on dbo.up_get_anatomicLocations TO PUBLIC
GRANT Control on dbo.up_get_anatomicLocations TO PUBLIC
GO