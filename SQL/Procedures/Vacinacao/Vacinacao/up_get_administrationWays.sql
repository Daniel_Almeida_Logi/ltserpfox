/* 
	Procedere para retornar as informacoes do sendVaccineResp
	exec up_get_administrationWays 0

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_administrationWays]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_administrationWays
GO

CREATE PROCEDURE dbo.up_get_administrationWays
	@inactive				BIT

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT
		id
		,description
		 
	FROM
		administrationWays(NOLOCK)
	WHERE
		inactive = @inactive

GO
GRANT EXECUTE on dbo.up_get_administrationWays TO PUBLIC
GRANT Control on dbo.up_get_administrationWays TO PUBLIC
GO