/* 
	Procedere para retornar as informacoes do sendVaccine
	exec up_get_sendVaccine '001E74B1-1AD5-43C9-A8FD-19EE853E6768'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_sendVaccine]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_sendVaccine
GO

CREATE PROCEDURE dbo.up_get_sendVaccine
	@token				VARCHAR(36)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT
		token						AS token
		,ISNULL(search,'')			AS search
		,ISNULL(profId,'')			AS profId
		,ISNULL(profName,'')		AS profName
		,ISNULL(code,'')			AS code
		,senderAssoc				AS senderAssoc
		,senderAssocId				AS senderAssocId
		,senderId					AS senderId
		,senderName					AS senderName
		,ISNULl([type],0)			AS type
		,ISNULl(typeDesc,'')		AS typeDesc
		,ISNULl(typeSend,0)			AS typeSend
		,ISNULl(typeSendDesc,'')	AS typeSendDesc
		,ISNULL(teste,0)			AS teste
		,ISNULL(site,'')			AS site
		,ISNULL(saveStamp,'')		AS saveStamp
		,ISNULL(breakStamp,'')		AS breakStamp
		,ISNULL(removeStamp,'')		AS removeStamp
		,ISNULL(motivoAnulado,'')	AS motivoAnulado
		,ousrdata					 
		,ousrinis					 
		,usrdata					 
		,usrinis			 
	FROM
		sendVaccine(NOLOCK)
	WHERE
		sendVaccine.token = @token

GO
GRANT EXECUTE on dbo.up_get_sendVaccine TO PUBLIC
GRANT Control on dbo.up_get_sendVaccine TO PUBLIC
GO