/* 
	Procedere para retornar as informacoes do sendVaccineResp
	exec up_get_sendVaccineResp '3325274B-02A7-4E2B-94F8-05A61F222DBB'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_sendVaccineResp]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_sendVaccineResp
GO

CREATE PROCEDURE dbo.up_get_sendVaccineResp
	@token				VARCHAR(36)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT
		token
		,correlationId
		,statusCode
		,statusCodeDesc
		,vaccineInfoToken
		,commercialNameToken
		,lastRegistrationStamp
		,registrationsToken
		,sendVaccineRespNoVacMotivesToken
		,infectionDatesToken
		,patiObs
		,ousrdata
		,ousrinis
		,usrdata
		,usrinis		 
	FROM
		sendVaccineResp(NOLOCK)
	WHERE
		sendVaccineResp.token = @token

GO
GRANT EXECUTE on dbo.up_get_sendVaccineResp TO PUBLIC
GRANT Control on dbo.up_get_sendVaccineResp TO PUBLIC
GO