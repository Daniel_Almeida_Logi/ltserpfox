/* 
	Procedere para retornar as informacoes do sendVaccineResp
	exec up_get_vaccineInfoAdverseReaction ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_vaccineInfoAdverseReaction]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_vaccineInfoAdverseReaction
GO

CREATE PROCEDURE dbo.up_get_vaccineInfoAdverseReaction
	@token				VARCHAR(36)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT
		stamp
		,token
		,id
		,description
		,ousrdata
		,ousrinis
		,usrdata
		,usrinis
	FROM
		vaccineInfoAdverseReaction(NOLOCK)
	WHERE
		token = @token

GO
GRANT EXECUTE on dbo.up_get_vaccineInfoAdverseReaction TO PUBLIC
GRANT Control on dbo.up_get_vaccineInfoAdverseReaction TO PUBLIC
GO