/* 
	Procedere para guardar as informacoes do sendVaccines_caches
	exec up_checkToUpdate_sendVaccinesCache 

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_checkToUpdate_sendVaccinesCache]') IS NOT NULL
	DROP PROCEDURE dbo.up_checkToUpdate_sendVaccinesCache
GO

CREATE PROCEDURE dbo.up_checkToUpdate_sendVaccinesCache

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

	DECLARE @timeLeft BIT

	SELECT
		@timeLeft = case when (GETDATE() >= dateExp)  THEN 1 ELSE 0 END 
	FROM
		sendVaccinesCache(NOLOCK)
	ORDER BY
		ousrdata desc

		select ISNULL(@timeLeft,1) as timeLeft

GO
GRANT EXECUTE on dbo.up_checkToUpdate_sendVaccinesCache TO PUBLIC
GRANT Control on dbo.up_checkToUpdate_sendVaccinesCache TO PUBLIC
GO