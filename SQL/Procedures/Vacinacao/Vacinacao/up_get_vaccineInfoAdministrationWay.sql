/* 
	Procedere para retornar as informacoes do sendVaccineResp
	exec up_get_vaccineInfoAdministrationWay '8d426222-857d-4b86-ac73-63ed3d9245d'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_vaccineInfoAdministrationWay]') IS NOT NULL
	DROP PROCEDURE dbo.up_get_vaccineInfoAdministrationWay
GO

CREATE PROCEDURE dbo.up_get_vaccineInfoAdministrationWay
	@token				VARCHAR(36)

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	SELECT
		stamp
		,token
		,id
		,description
		,ousrdata
		,ousrinis
		,usrdata
		,usrinis
	FROM
		vaccineInfoAdministrationWay(NOLOCK)
	WHERE
		token = @token

GO
GRANT EXECUTE on dbo.up_get_vaccineInfoAdministrationWay TO PUBLIC
GRANT Control on dbo.up_get_vaccineInfoAdministrationWay TO PUBLIC
GO