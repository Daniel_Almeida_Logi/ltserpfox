/* Relat�rio de extrato de cartao

	exec up_relatorio_iqvia 'Loja 1', '20240417', '20240417','IQVIA-ASMA/DPOC'

*/
 
if OBJECT_ID('[dbo].[up_relatorio_iqvia]') IS NOT NULL
	drop procedure dbo.up_relatorio_iqvia
go
CREATE PROCEDURE [dbo].[up_relatorio_iqvia]
    @site           VARCHAR(36),
    @dataini        DATETIME = '19000101',
    @datafim        DATETIME = '19000101',
    @questionario   VARCHAR(254)
AS
SET NOCOUNT ON
   
    DECLARE @sortedColumns NVARCHAR(MAX) = ''
    DECLARE @columns NVARCHAR(MAX) = ''
    DECLARE @sql NVARCHAR(MAX) = ''
    DECLARE @columnNames NVARCHAR(MAX) = ''

    -- Criar uma tabela tempor�ria para armazenar as perguntas
    CREATE TABLE #TempPerguntas (
        pergunta VARCHAR(255),
        ordem   INT
    )

    -- Inserir perguntas distintas na tabela tempor�ria
    INSERT INTO #TempPerguntas (pergunta, ordem)
    SELECT DISTINCT 
        pergunta, 
        ordem
    FROM 
        quest_respostas(NOLOCK)
    INNER JOIN 
        quest_pergunta(NOLOCK) ON quest_respostas.quest_perguntaStamp = quest_pergunta.quest_perguntastamp
    INNER JOIN 
        Quest(NOLOCK) ON Quest.QuestStamp =  quest_respostas.questStamp
    WHERE 
        (Quest.Descr IN (SELECT items FROM dbo.up_splitToTable(@questionario, ',')) OR @questionario = '')

    -- Ordenar as perguntas na tabela tempor�ria
    SELECT 
        @sortedColumns = COALESCE(@sortedColumns + ', ', '') + QUOTENAME(pergunta)
    FROM 
        #TempPerguntas
    ORDER BY 
        ordem

    -- Construir a lista de colunas ordenadas
    SET @columns = @sortedColumns
	SET @columns =STUFF(@columns, 1, 1, '')

    -- Construir a lista de nomes das colunas
    SELECT 
        @columnNames = COALESCE(@columnNames + ', ', '') + '''' + pergunta + ''''
    FROM 
        #TempPerguntas
    ORDER BY 
        ordem

	 -- Remover a primeira v�rgula, se houver
    SET @columnNames = STUFF(@columnNames, 1, 1, '')

    -- Consulta din�mica
    SET @sql = '
	SELECT 
        ''Data'','+ @columnNames +', ''Ref'', ''Codfarm'', ''Nomabrv''
    UNION ALL
    SELECT DISTINCT
         ousrdata,' + @columns + ', ref, codfarm, nomabrv
    FROM (
        SELECT 
            CONVERT(VARCHAR(10), quest_respostas.ousrdata, 103)  +'' ''+
        CONVERT(VARCHAR(8), quest_respostas.ousrdata, 108) AS ousrdata,
            quest_pergunta.pergunta, 
            ISNULL(quest_respostas.resposta,'''') AS resposta,
            quest_respostas.ref,
            empresa.codfarm,
            empresa.nomabrv,
			quest_respostas.quest_respostas_grpStamp as grpstamp
        FROM
            quest_respostas(NOLOCK)
            LEFT JOIN fi(NOLOCK) ON fi.fistamp = quest_respostas.fistamp
            LEFT JOIN ft(NOLOCK) ON ft.ftstamp = fi.ftstamp
            INNER JOIN empresa(NOLOCK) ON empresa.site = quest_respostas.site
            left JOIN quest_pergunta(NOLOCK) ON quest_pergunta.quest_perguntastamp = quest_respostas.quest_perguntaStamp
            INNER JOIN Quest(NOLOCK) ON Quest.QuestStamp = quest_respostas.questStamp
        WHERE
            quest_respostas.site = '''+ @site +'''
            AND  CONVERT(VARCHAR, quest_respostas.ousrdata,23)    BETWEEN '''+CONVERT(VARCHAR, @dataIni,23)+ '''  AND
                                     '''+CONVERT(VARCHAR, @dataFim,23)+''' 
            AND (Quest.Descr IN (SELECT items FROM dbo.up_splitToTable('''+ @questionario +''', '''')) OR '''+ @questionario +''' = '''')
    ) AS Source
    PIVOT (
        MAX(resposta)
        FOR pergunta IN (' + @columns + ')
    ) AS PivotTable

   '

    -- Executar consulta
    PRINT @sql
    EXEC (@sql)
GO

GRANT EXECUTE ON dbo.up_relatorio_iqvia TO Public
GRANT CONTROL ON dbo.up_relatorio_iqvia TO Public
