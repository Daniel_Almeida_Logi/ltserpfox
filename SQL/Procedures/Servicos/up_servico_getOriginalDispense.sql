/*
	
 valida se existiu alguma dispensa para a embalagem no ultimos minutos

 exec up_servico_getOriginalDispense '05600937100221','BATCH000000000000004','M1N2O3P4Q5R600082083'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_servico_getOriginalDispense]') IS NOT NULL
	drop procedure dbo.up_servico_getOriginalDispense
go

create procedure dbo.up_servico_getOriginalDispense
@productCode varchar(255) = '',
@batchId varchar(255) = '',
@packSerialNumber varchar(255) = ''



/* WITH ENCRYPTION */ 
AS 
SET NOCOUNT ON

BEGIN 

	select 
		top 1
		nmvsTrxId = isnull(nmvsTrxId,'')		
	from 
		fi_trans_info (nolock) as fi_trans_info 
	where
		fi_trans_info.productCode = @productCode    
		and fi_trans_info.batchId = @batchId 
		and fi_trans_info.packSerialNumber = @packSerialNumber 
		and fi_trans_info.code='NMVS_SUCCESS'  and (fi_trans_info.reqType='DISPENSE' Or fi_trans_info.reqType='DISPENSE_MANUAL')
	order by ousrdata desc


END


GO
Grant Execute on dbo.up_servico_getOriginalDispense to Public
Grant Control on dbo.up_servico_getOriginalDispense to Public
GO


