
/*  
	D.Almeida, 2020-10-14

	Sp para inserir notificações

    up_servico_insere_notificacao


	delete from notificacoes where stamp='D5793D70-A335-46D7-B6C8-2EBF5B'

	select * from notificacoes order by stamp desc

	
	declare @site varchar(100) = 'Loja 1'

	declare @notif bit
	set @notif = (select bool from B_Parameters_site (nolock) where stamp='ADM0000000115' and site=@site)


	IF @notif = 1
	begin

		declare @utilnotif varchar(200)
		set @utilnotif = (select isnull(textValue,'') from B_Parameters_site (nolock) where stamp='ADM0000000113' and site=@site)

		declare @grpnotif varchar(200)
		set @grpnotif = (select isnull(textValue,'') from B_Parameters_site (nolock) where stamp='ADM0000000114' and site=@site)

		exec up_servico_insere_notificacao '','Loja 1',0,'Online','','',
		@utilnotif,
		@grpnotif,
		'b_utentes','Criação de novo cliente nº: 201 estab: 0','Novo Cliente','ADM0383FB6D-7C8B-421C-A82'	
	end


	select *from b_parameters_site order by stamp desc
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

 

if OBJECT_ID('[dbo].[up_servico_insere_notificacao]') IS NOT NULL
    drop procedure dbo.up_servico_insere_notificacao
go

 

CREATE PROCEDURE[dbo].[up_servico_insere_notificacao]
    @historyid          VARCHAR(100),
    @site               VARCHAR(20),
    @recebido           BIT ,
    @origem             VARCHAR(200),
    @usrorigem          VARCHAR(200),
    @destino            VARCHAR(200),
    @usrdestino         VARCHAR(200),
    @grpdestino         VARCHAR(200),
    @ecradestino        VARCHAR(200),
    @mensagem           VARCHAR(MAX),
    @tiponotif          VARCHAR(200),
	@stampdestino       VARCHAR(36)

 

AS
SET NOCOUNT ON

 
    DECLARE @data datetime 

	if(@stampdestino ='')
	BEGIN
		set @stampdestino = LEFT(NEWID(),36)
	END 

    set @data = GETDATE()



    IF( NOT EXISTS(SELECT * FROM notificacoes(nolock) WHERE stampdestino=@stampdestino))
    BEGIN



        INSERT INTO  notificacoes (stamp,origem,usrorigem,destino,usrdestino,grpdestino,ecradestino,stampdestino,mensagem,recebido,tiponotif,data)
		select 
			LEFT(NEWID(),30),
			@origem,
			@usrorigem,
			@destino,
			'',
			items,
			@ecradestino,
			@stampdestino,
			@mensagem,
			@recebido,
			@tiponotif,
			@data
		from dbo.up_splitToTable(@grpdestino,',')


		
        INSERT INTO  notificacoes (stamp,origem,usrorigem,destino,usrdestino,grpdestino,ecradestino,stampdestino,mensagem,recebido,tiponotif,data)
		select 
			LEFT(NEWID(),30),
			@origem,
			@usrorigem,
			@destino,
			items,
			'',
			@ecradestino,
			@stampdestino,
			@mensagem,
			@recebido,
			@tiponotif,
			@data
		from dbo.up_splitToTable(@usrdestino,',')

    END    


      

GO
GRANT EXECUTE ON dbo.up_servico_insere_notificacao to Public
GRANT CONTROL ON dbo.up_servico_insere_notificacao to Public
GO
