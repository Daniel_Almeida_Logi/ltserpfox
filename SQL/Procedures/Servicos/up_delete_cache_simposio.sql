/*
	EXEC up_delete_cache_simposio 'drug', 'drug'
*/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_delete_cache_simposio]') IS NOT NULL
    drop procedure dbo.up_delete_cache_simposio
go

create procedure dbo.up_delete_cache_simposio

	@group								AS VARCHAR(254),
	@subGroup							AS VARCHAR(254)

/* WITH ENCRYPTION */
AS

	DECLARE @tempDays			AS INT = 0
	DECLARE @tableToDelete		AS VARCHAR(254) = ''
	DECLARE @sql				AS VARCHAR(4000) = ''

	SELECT 
		@tempDays			= timeDays,
		@tableToDelete		= [table]
	FROM
		control_cache_simposio(NOLOCK)
	WHERE
		LTRIM(RTRIM(UPPER([group]))) = LTRIM(RTRIM(UPPER(@group)))
		AND LTRIM(RTRIM(UPPER([subGroup]))) = LTRIM(RTRIM(UPPER(@subGroup)))
	
	SET @sql = @sql + N'
		DELETE FROM 
			'+  @tableToDelete +' 
		WHERE
			[ousrdata] <= '''+ CONVERT(varchar, CONVERT(DATE,dateadd(D, @tempDays * -1, getdate()))) +''' ' 

	PRINT @sql
	EXEC(@sql)
GO
Grant Execute on dbo.up_delete_cache_simposio to Public
Grant Control on dbo.up_delete_cache_simposio to Public
go