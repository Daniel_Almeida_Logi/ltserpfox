
/*  
	José Simões, 2021-01-18

	Sp para inserir notificaçõesRapidas e nas notificações

    exec up_servico_insere_notificacaoRapidas '' ,'Loja 1',0,'Central Telefonica' ,'','','','ADM','logsPhoneCalls','Chamada do número 2455657876','Chamada','83aa624b-28c1-4dfb-bf3d-8551'


	
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

 

if OBJECT_ID('[dbo].[up_servico_insere_notificacaoRapidas]') IS NOT NULL
    drop procedure dbo.up_servico_insere_notificacaoRapidas
go

 

CREATE PROCEDURE[dbo].[up_servico_insere_notificacaoRapidas]
    @historyid          VARCHAR(100),
    @site               VARCHAR(20),
    @recebido           BIT ,
    @origem             VARCHAR(200),
    @usrorigem          VARCHAR(200),
    @destino            VARCHAR(200),
    @usrdestino         VARCHAR(200),
    @grpdestino         VARCHAR(200),
    @ecradestino        VARCHAR(200),
    @mensagem           VARCHAR(MAX),
    @tiponotif          VARCHAR(200),
	@stampdestino       VARCHAR(36)

 

AS
SET NOCOUNT ON

 
    DECLARE @data datetime 

	if(@stampdestino ='')
	BEGIN
		set @stampdestino = LEFT(NEWID(),36)
	END 

    set @data = GETDATE()


		
    INSERT INTO  notificacoesRapidas (stamp,origem,usrorigem,destino,usrdestino,grpdestino,ecradestino,stampdestino,mensagem,recebido,tiponotif,data)
	values(LEFT(NEWID(),30),@origem,@usrorigem,@destino,@usrdestino,@grpdestino,@ecradestino,@stampdestino,@mensagem,@recebido,@tiponotif,@data)

	INSERT INTO  notificacoes (stamp,origem,usrorigem,destino,usrdestino,grpdestino,ecradestino,stampdestino,mensagem,recebido,tiponotif,data)
	values(LEFT(NEWID(),30),@origem,@usrorigem,@destino,@usrdestino,@grpdestino,@ecradestino,@stampdestino,@mensagem,1,@tiponotif,@data)

 
	delete notificacoesRapidas WHERE data <= DATEADD(minute, -1, GETDATE())
GO
GRANT EXECUTE ON dbo.up_servico_insere_notificacaoRapidas to Public
GRANT CONTROL ON dbo.up_servico_insere_notificacaoRapidas to Public
GO
