
/*  
exec servico_getExtensoesVaziaSites 'Loja 2'
exec servico_getExtensoesVaziaSites 'Loja 1'
exec servico_getExtensoesVaziaSites ''
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[servico_getExtensoesVaziaSites]') IS NOT NULL
	drop procedure dbo.servico_getExtensoesVaziaSites
go

CREATE PROCEDURE [dbo].[servico_getExtensoesVaziaSites]
	@site VARCHAR(300) =''
AS
SET NOCOUNT ON

	If OBJECT_ID('tempdb.dbo.#temp_Sites') IS NOT NULL
			drop table #temp_Sites;	
		
	create  table #temp_Sites (
		[siteTemp] varchar(30)
	)
	SET @site		=   rtrim(ltrim(isnull(@site,'')))


	IF(@site <> '')
	BEGIN 
	print 1
		INSERT INTO #temp_Sites (siteTemp)
		SELECT * FROM dbo.up_splitToTable(@site,';')
	END
	ELSE
	BEGIN 
		INSERT INTO #temp_Sites (siteTemp)
		SELECT site FROM Empresa

		
	END  

	SELECT top 1 
			''						AS ext,
			site					AS site,
			''						AS design,
			2						AS type
	FROM empresa	
	WHERE site COLLATE DATABASE_DEFAULT IN  (SELECT siteTemp FROM  #temp_Sites)
	ORDER BY SITE ASC -- � muito importante que esteja ordenado para ler

	 
	If OBJECT_ID('tempdb.dbo.#temp_Sites') IS NOT NULL
			drop table #temp_Sites;	

GO
GRANT EXECUTE ON dbo.servico_getExtensoesVaziaSites to Public
GRANT CONTROL ON dbo.servico_getExtensoesVaziaSites to Public
GO



