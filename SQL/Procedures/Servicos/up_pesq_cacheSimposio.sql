/*
	EXEC up_pesq_cacheSimposio 'drug', 'drug','ref','5440987'
*/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_pesq_cacheSimposio]') IS NOT NULL
    drop procedure dbo.up_pesq_cacheSimposio
go

create procedure dbo.up_pesq_cacheSimposio

	@group								AS VARCHAR(254),
	@subGroup							AS VARCHAR(254),
	@attr							    AS VARCHAR(254),
	@itemId								AS VARCHAR(254)


/* WITH ENCRYPTION */
AS


	DECLARE @sql				VARCHAR(4000) = ''
	DECLARE @tempDays			AS INT = 0
	DECLARE @tableToDelete		AS VARCHAR(254) = ''

	SELECT
		@tempDays			= timeDays, 
		@tableToDelete		= [table]
	FROM
		control_cache_simposio(NOLOCK)
	WHERE
		LTRIM(RTRIM(UPPER([group]))) = LTRIM(RTRIM(UPPER(@group)))
		AND LTRIM(RTRIM(UPPER([subGroup]))) = LTRIM(RTRIM(UPPER(@subGroup)))

	SET @sql = @sql + N'
		SELECT 
			*
		FROM
			'+@tableToDelete+'(NOLOCK)
		WHERE
			'+@attr+' =  '+@itemId+'
			AND [ousrdata] >= '''+ CONVERT(varchar, CONVERT(DATE,dateadd(D, @tempDays * -1, getdate()))) +''''

	PRINT @sql
	EXEC(@sql)
GO
Grant Execute on dbo.up_pesq_cacheSimposio to Public
Grant Control on dbo.up_pesq_cacheSimposio to Public
go

