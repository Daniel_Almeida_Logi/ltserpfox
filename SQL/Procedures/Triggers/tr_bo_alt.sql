
-- ================================================
-- Template generated from Template Explorer using:
-- Create Trigger (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- See additional Create Trigger templates for more
-- examples of different Trigger statements.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Jose Simoes>
-- Create date: <18/11/2021>
-- Description:	<trigger to insert in bo_alt>
-- =============================================
if OBJECT_ID('[dbo].[tr_bo_alt]') IS NOT NULL
	DROP trigger [dbo].tr_bo_alt;
GO

CREATE TRIGGER [dbo].[tr_bo_alt] ON [dbo].[bo]
   AFTER INSERT, UPDATE, DELETE
AS 
BEGIN
	SET NOCOUNT ON;

	Declare @Type varchar(50)=''
	DECLARE @bostamp varchar(25)
	DECLARE @status varchar(50)
	DECLARE @autoriza_emails bit 
	DECLARE @site VARCHAR(20)
	DECLARE @email VARCHAR(100)

	IF EXISTS (SELECT * FROM inserted) and  EXISTS (SELECT * FROM deleted)
	BEGIN
		set  @Type = 'UPDATE'
		INSERT into [bo_alt] (bostamp,nmdos,obrano,dataobra,nome,totaldeb,etotaldeb,tipo,datafinal,sqtt14,vendedor,vendnm,no,boano,dataopen,fechada,obs,trab1,ndos,moeda,estab,morada,local,codpost,ncont
						,logi1,ccusto,etotal,ecusto,ebo_1tvall,ebo_2tvall,ebo11_bins,ebo11_iva,ebo21_bins,ebo21_iva,ebo31_bins,ebo31_iva,ebo41_bins,ebo41_iva,ebo51_bins,ebo51_iva,ebo12_bins,ebo12_iva,
						ebo22_bins,ebo22_iva,ebo32_bins,ebo32_iva,ebo42_bins,ebo42_iva,ebo52_bins,ebo52_iva,ebo_totp1,ebo_totp2,memissao,nome2,origem,site,pnome,pno,ocupacao,tpdesc,ousrinis,ousrdata,
						ousrhora,usrinis,usrdata,usrhora,u_dataentr,ebo61_iva,fref,ncusto,ultfact,datafecho,exportado,tabIva,ebo61_bins,ebo62_bins,ebo62_iva,edescc,valcartao,alteracao,alteracaoData)
		select 	bostamp,nmdos,obrano,dataobra,nome,totaldeb,etotaldeb,tipo,datafinal,sqtt14,vendedor,vendnm,no,boano,dataopen,fechada,obs,trab1,ndos,moeda,estab,morada,local,codpost,ncont
						,logi1,ccusto,etotal,ecusto,ebo_1tvall,ebo_2tvall,ebo11_bins,ebo11_iva,ebo21_bins,ebo21_iva,ebo31_bins,ebo31_iva,ebo41_bins,ebo41_iva,ebo51_bins,ebo51_iva,ebo12_bins,ebo12_iva,
						ebo22_bins,ebo22_iva,ebo32_bins,ebo32_iva,ebo42_bins,ebo42_iva,ebo52_bins,ebo52_iva,ebo_totp1,ebo_totp2,memissao,nome2,origem,site,pnome,pno,ocupacao,tpdesc,ousrinis,ousrdata,
						ousrhora,usrinis,usrdata,usrhora,u_dataentr,ebo61_iva,fref,ncusto,ultfact,datafecho,exportado,tabIva,ebo61_bins,ebo62_bins,ebo62_iva,edescc,valcartao,@type,GETDATE()
		from inserted			


	END
	ELSE IF EXISTS(SELECT * FROM inserted)
	BEGIN
		set @Type = 'INSERT'
		INSERT into [bo_alt] (bostamp,nmdos,obrano,dataobra,nome,totaldeb,etotaldeb,tipo,datafinal,sqtt14,vendedor,vendnm,no,boano,dataopen,fechada,obs,trab1,ndos,moeda,estab,morada,local,codpost,ncont
						,logi1,ccusto,etotal,ecusto,ebo_1tvall,ebo_2tvall,ebo11_bins,ebo11_iva,ebo21_bins,ebo21_iva,ebo31_bins,ebo31_iva,ebo41_bins,ebo41_iva,ebo51_bins,ebo51_iva,ebo12_bins,ebo12_iva,
						ebo22_bins,ebo22_iva,ebo32_bins,ebo32_iva,ebo42_bins,ebo42_iva,ebo52_bins,ebo52_iva,ebo_totp1,ebo_totp2,memissao,nome2,origem,site,pnome,pno,ocupacao,tpdesc,ousrinis,ousrdata,
						ousrhora,usrinis,usrdata,usrhora,u_dataentr,ebo61_iva,fref,ncusto,ultfact,datafecho,exportado,tabIva,ebo61_bins,ebo62_bins,ebo62_iva,edescc,valcartao,alteracao,alteracaoData)
		select 	bostamp,nmdos,obrano,dataobra,nome,totaldeb,etotaldeb,tipo,datafinal,sqtt14,vendedor,vendnm,no,boano,dataopen,fechada,obs,trab1,ndos,moeda,estab,morada,local,codpost,ncont
						,logi1,ccusto,etotal,ecusto,ebo_1tvall,ebo_2tvall,ebo11_bins,ebo11_iva,ebo21_bins,ebo21_iva,ebo31_bins,ebo31_iva,ebo41_bins,ebo41_iva,ebo51_bins,ebo51_iva,ebo12_bins,ebo12_iva,
						ebo22_bins,ebo22_iva,ebo32_bins,ebo32_iva,ebo42_bins,ebo42_iva,ebo52_bins,ebo52_iva,ebo_totp1,ebo_totp2,memissao,nome2,origem,site,pnome,pno,ocupacao,tpdesc,ousrinis,ousrdata,
						ousrhora,usrinis,usrdata,usrhora,u_dataentr,ebo61_iva,fref,ncusto,ultfact,datafecho,exportado,tabIva,ebo61_bins,ebo62_bins,ebo62_iva,edescc,valcartao,@type,GETDATE()
		from inserted	
	END
	ElSE IF EXISTS(SELECT * FROM deleted)
	BEGIN
		set @Type = 'DELETE'
		INSERT into [bo_alt] (bostamp,nmdos,obrano,dataobra,nome,totaldeb,etotaldeb,tipo,datafinal,sqtt14,vendedor,vendnm,no,boano,dataopen,fechada,obs,trab1,ndos,moeda,estab,morada,local,codpost,ncont
						,logi1,ccusto,etotal,ecusto,ebo_1tvall,ebo_2tvall,ebo11_bins,ebo11_iva,ebo21_bins,ebo21_iva,ebo31_bins,ebo31_iva,ebo41_bins,ebo41_iva,ebo51_bins,ebo51_iva,ebo12_bins,ebo12_iva,
						ebo22_bins,ebo22_iva,ebo32_bins,ebo32_iva,ebo42_bins,ebo42_iva,ebo52_bins,ebo52_iva,ebo_totp1,ebo_totp2,memissao,nome2,origem,site,pnome,pno,ocupacao,tpdesc,ousrinis,ousrdata,
						ousrhora,usrinis,usrdata,usrhora,u_dataentr,ebo61_iva,fref,ncusto,ultfact,datafecho,exportado,tabIva,ebo61_bins,ebo62_bins,ebo62_iva,edescc,valcartao,alteracao,alteracaoData)
		select 	bostamp,nmdos,obrano,dataobra,nome,totaldeb,etotaldeb,tipo,datafinal,sqtt14,vendedor,vendnm,no,boano,dataopen,fechada,obs,trab1,ndos,moeda,estab,morada,local,codpost,ncont
						,logi1,ccusto,etotal,ecusto,ebo_1tvall,ebo_2tvall,ebo11_bins,ebo11_iva,ebo21_bins,ebo21_iva,ebo31_bins,ebo31_iva,ebo41_bins,ebo41_iva,ebo51_bins,ebo51_iva,ebo12_bins,ebo12_iva,
						ebo22_bins,ebo22_iva,ebo32_bins,ebo32_iva,ebo42_bins,ebo42_iva,ebo52_bins,ebo52_iva,ebo_totp1,ebo_totp2,memissao,nome2,origem,site,pnome,pno,ocupacao,tpdesc,ousrinis,ousrdata,
						ousrhora,usrinis,usrdata,usrhora,u_dataentr,ebo61_iva,fref,ncusto,ultfact,datafecho,exportado,tabIva,ebo61_bins,ebo62_bins,ebo62_iva,edescc,valcartao,@type,GETDATE()
		from deleted

	END


	SELECT 
		 @bostamp= inserted.bostamp,
		 @status=bo2.status,
		 @autoriza_emails=b_utentes.autoriza_emails,
		 @site= inserted.site,
		 @email= case when ISNULL(bo2.email,'') = '' then b_utentes.email else bo2.email end 
	from inserted
			 INNER JOIN bo2			ON inserted.bostamp =   bo2.bo2stamp
			 INNER JOIN b_utentes ON inserted.no = b_utentes.NO  and inserted.estab= b_utentes.estab
	where ndos=41 and bo2.status !=''


	if(@email!='' AND ISNULL(@bostamp,'')!='' AND (SELECT TOP 1 bool FROM B_Parameters_site  WHERE stamp='ADM0000000146' and site=@site)=1 AND NOT EXISTS(select * from docsToSendSchedule where tableName='BO' and tableStamp=@bostamp and status=@status) AND @autoriza_emails=1 )
	BEGIN
		DECLARE @tokenDocTosend varchar(36)
		SET @tokenDocTosend = LEFT(NEWID(),36)

		INSERT INTO docsToSendSchedule (stamp,tokenDocToSend,tableName,tableStamp,status,typeUser,typeUserDesc,ousrdata,sent)
			VALUES (LEFT(NEWID(),36), @tokenDocTosend,'BO',@bostamp,@status,1,'Cliente' ,GETDATE(),0)


		INSERT INTO docsToSend(TOKEN,id,site,nameDoc,usp,typeDocSave,typeSend,frequency,typeExec,dateNextCall,state,toSend,visibility,delimiter,zip)
		values(@tokenDocTosend,(select max(id) + 100 from docsToSend),@site, 'Envio de email de encomenda','up_get_EmailOrdersToSend','HTML','EMAIL','UNICO','ORDERS',GETDATE(),1,@email,1,'',0)

		
		Declare @emailEmpresa varchar(100)

		select @emailEmpresa = email_bcc from empresa where site=@site

		IF(@emailEmpresa!='' and @status IN (select items from dbo.up_splitToTable((SELECT textValue FROM B_Parameters_site WHERE STAMP='ADM0000000147' AND site=@site),';')))
		BEGIN 
			DECLARE @tokenDocTosendP varchar(36)
			SET @tokenDocTosendP = LEFT(NEWID(),36)

		
			INSERT INTO docsToSendSchedule (stamp,tokenDocToSend,tableName,tableStamp,status,typeUser,typeUserDesc,ousrdata,sent)
			VALUES (LEFT(NEWID(),36), @tokenDocTosendP,'BO',@bostamp,@status,2,'Farmacia' ,GETDATE(),0)

			INSERT INTO docsToSend(TOKEN,id,site,nameDoc,usp,typeDocSave,typeSend,frequency,typeExec,dateNextCall,state,toSend,visibility,delimiter,zip)
			values(@tokenDocTosendP,(select max(id) + 20 from docsToSend),@site, 'Envio de email de encomenda-Farmacia','up_get_EmailOrdersToSend','HTML','EMAIL','UNICO','ORDERS',GETDATE(),1,@emailEmpresa,1,'',0)

		END
	END 

END

GO
