
/****** Object:  Trigger [dbo].[tr_bi_alt]    Script Date: 02/02/2023 17:26:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Jose Simoes>
-- Create date: <18/11/2021>
-- Description:	<trigger to insert in bi_alt>
-- =============================================
if OBJECT_ID('[dbo].[tr_bi_alt]') IS NOT NULL
	DROP trigger [dbo].tr_bi_alt;
GO

CREATE TRIGGER  [dbo].[tr_bi_alt] ON [dbo].[bi]
   AFTER INSERT, UPDATE, DELETE
AS 
BEGIN
	SET NOCOUNT ON;

	Declare @Type varchar(50)='';
	IF EXISTS (SELECT * FROM inserted) and  EXISTS (SELECT * FROM deleted)
	BEGIN
		set  @Type = 'UPDATE'
		INSERT into [bi_alt] (bistamp,nmdos,obrano,ref,design,qtt,qtt2,iva,tabiva,armazem,stipo,no,ndos,forref,rdata,lobs,fechada,datafinal,dataobra,dataopen,resfor,rescli,ar2mazem,lrecno,lordem,local,morada,codpost,nome,vendedor,vendnm,lote,uni2qtt
							,epu,edebito,eprorc,epcusto,ettdeb,adoc,binum1,binum2,codigo,cpoc,obistamp,oobistamp,familia,desconto,desc2,desc3,desc4,ccusto,num1,pbruto,ecustoind,bostamp,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,edebitoori,u_mquebra
							,u_epv1act,u_stockact,u_bencont,u_psicont,u_bonus,u_upc,u_reserva,unidade,ivaincl,estab,nopat,usr1,usr2,usr3,usr4,usr5,usr6,stns,emconf,marcada,cativo,resusr,resrec,fno,nmdoc,ndoc,partes2,partes,oftstamp,fdata,ofostamp,debito,
							ttdeb,slvumoeda,bifref,ncusto,slvu,eslvu,sltt,esltt,cor,tam,producao,composto,trocaequi,slttmoeda,pcusto,u_desccom,u_nodesc,exportado,binum3,qtRec,diploma,descval,exepcaoTrocaMed,alteracao,alteracaoData)
		select 	bistamp,nmdos,obrano,ref,design,qtt,qtt2,iva,tabiva,armazem,stipo,no,ndos,forref,rdata,lobs,fechada,datafinal,dataobra,dataopen,resfor,rescli,ar2mazem,lrecno,lordem,local,morada,codpost,nome,vendedor,vendnm,lote,uni2qtt
							,epu,edebito,eprorc,epcusto,ettdeb,adoc,binum1,binum2,codigo,cpoc,obistamp,oobistamp,familia,desconto,desc2,desc3,desc4,ccusto,num1,pbruto,ecustoind,bostamp,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,edebitoori,u_mquebra
							,u_epv1act,u_stockact,u_bencont,u_psicont,u_bonus,u_upc,u_reserva,unidade,ivaincl,estab,nopat,usr1,usr2,usr3,usr4,usr5,usr6,stns,emconf,marcada,cativo,resusr,resrec,fno,nmdoc,ndoc,partes2,partes,oftstamp,fdata,ofostamp,debito,
							ttdeb,slvumoeda,bifref,ncusto,slvu,eslvu,sltt,esltt,cor,tam,producao,composto,trocaequi,slttmoeda,pcusto,u_desccom,u_nodesc,exportado,binum3,qtRec,diploma,descval,exepcaoTrocaMed,@type,GETDATE()
		from inserted			


	END
	ELSE IF EXISTS(SELECT * FROM inserted)
	BEGIN
		set @Type = 'INSERT'
		INSERT into [bi_alt] (bistamp,nmdos,obrano,ref,design,qtt,qtt2,iva,tabiva,armazem,stipo,no,ndos,forref,rdata,lobs,fechada,datafinal,dataobra,dataopen,resfor,rescli,ar2mazem,lrecno,lordem,local,morada,codpost,nome,vendedor,vendnm,lote,uni2qtt
							,epu,edebito,eprorc,epcusto,ettdeb,adoc,binum1,binum2,codigo,cpoc,obistamp,oobistamp,familia,desconto,desc2,desc3,desc4,ccusto,num1,pbruto,ecustoind,bostamp,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,edebitoori,u_mquebra
							,u_epv1act,u_stockact,u_bencont,u_psicont,u_bonus,u_upc,u_reserva,unidade,ivaincl,estab,nopat,usr1,usr2,usr3,usr4,usr5,usr6,stns,emconf,marcada,cativo,resusr,resrec,fno,nmdoc,ndoc,partes2,partes,oftstamp,fdata,ofostamp,debito,
							ttdeb,slvumoeda,bifref,ncusto,slvu,eslvu,sltt,esltt,cor,tam,producao,composto,trocaequi,slttmoeda,pcusto,u_desccom,u_nodesc,exportado,binum3,qtRec,diploma,descval,exepcaoTrocaMed,alteracao,alteracaoData)
		select 	bistamp,nmdos,obrano,ref,design,qtt,qtt2,iva,tabiva,armazem,stipo,no,ndos,forref,rdata,lobs,fechada,datafinal,dataobra,dataopen,resfor,rescli,ar2mazem,lrecno,lordem,local,morada,codpost,nome,vendedor,vendnm,lote,uni2qtt
							,epu,edebito,eprorc,epcusto,ettdeb,adoc,binum1,binum2,codigo,cpoc,obistamp,oobistamp,familia,desconto,desc2,desc3,desc4,ccusto,num1,pbruto,ecustoind,bostamp,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,edebitoori,u_mquebra
							,u_epv1act,u_stockact,u_bencont,u_psicont,u_bonus,u_upc,u_reserva,unidade,ivaincl,estab,nopat,usr1,usr2,usr3,usr4,usr5,usr6,stns,emconf,marcada,cativo,resusr,resrec,fno,nmdoc,ndoc,partes2,partes,oftstamp,fdata,ofostamp,debito,
							ttdeb,slvumoeda,bifref,ncusto,slvu,eslvu,sltt,esltt,cor,tam,producao,composto,trocaequi,slttmoeda,pcusto,u_desccom,u_nodesc,exportado,binum3,qtRec,diploma,descval,exepcaoTrocaMed,@type,GETDATE()
		from inserted	
	END
	ElSE IF EXISTS(SELECT * FROM deleted)
	BEGIN
		set @Type = 'DELETE'
		INSERT into [bi_alt] (bistamp,nmdos,obrano,ref,design,qtt,qtt2,iva,tabiva,armazem,stipo,no,ndos,forref,rdata,lobs,fechada,datafinal,dataobra,dataopen,resfor,rescli,ar2mazem,lrecno,lordem,local,morada,codpost,nome,vendedor,vendnm,lote,uni2qtt
							,epu,edebito,eprorc,epcusto,ettdeb,adoc,binum1,binum2,codigo,cpoc,obistamp,oobistamp,familia,desconto,desc2,desc3,desc4,ccusto,num1,pbruto,ecustoind,bostamp,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,edebitoori,u_mquebra
							,u_epv1act,u_stockact,u_bencont,u_psicont,u_bonus,u_upc,u_reserva,unidade,ivaincl,estab,nopat,usr1,usr2,usr3,usr4,usr5,usr6,stns,emconf,marcada,cativo,resusr,resrec,fno,nmdoc,ndoc,partes2,partes,oftstamp,fdata,ofostamp,debito,
							ttdeb,slvumoeda,bifref,ncusto,slvu,eslvu,sltt,esltt,cor,tam,producao,composto,trocaequi,slttmoeda,pcusto,u_desccom,u_nodesc,exportado,binum3,qtRec,diploma,descval,exepcaoTrocaMed,alteracao,alteracaoData)
		select 	bistamp,nmdos,obrano,ref,design,qtt,qtt2,iva,tabiva,armazem,stipo,no,ndos,forref,rdata,lobs,fechada,datafinal,dataobra,dataopen,resfor,rescli,ar2mazem,lrecno,lordem,local,morada,codpost,nome,vendedor,vendnm,lote,uni2qtt
							,epu,edebito,eprorc,epcusto,ettdeb,adoc,binum1,binum2,codigo,cpoc,obistamp,oobistamp,familia,desconto,desc2,desc3,desc4,ccusto,num1,pbruto,ecustoind,bostamp,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,edebitoori,u_mquebra
							,u_epv1act,u_stockact,u_bencont,u_psicont,u_bonus,u_upc,u_reserva,unidade,ivaincl,estab,nopat,usr1,usr2,usr3,usr4,usr5,usr6,stns,emconf,marcada,cativo,resusr,resrec,fno,nmdoc,ndoc,partes2,partes,oftstamp,fdata,ofostamp,debito,
							ttdeb,slvumoeda,bifref,ncusto,slvu,eslvu,sltt,esltt,cor,tam,producao,composto,trocaequi,slttmoeda,pcusto,u_desccom,u_nodesc,exportado,binum3,qtRec,diploma,descval,exepcaoTrocaMed,@type,GETDATE()
		from deleted

		IF EXISTS (SELECT name FROM master.sys.databases WHERE name = N'mecosync')
		begin 

			IF (EXISTS (SELECT *  FROM mecosync.INFORMATION_SCHEMA.TABLES  WHERE     TABLE_NAME = 'BI'))
			BEGIN	
				delete from mecosync.dbo.bi where mecosync.dbo.bi.bistamp in (select bistamp from deleted)
			END

		END 

	END


END
