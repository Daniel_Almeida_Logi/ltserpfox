SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Jose Simoes>
-- Create date: <18/11/2021>
-- Description:	<trigger to insert in bo2_alt>
-- =============================================
if OBJECT_ID('[dbo].[tr_bo2_alt]') IS NOT NULL
	DROP trigger [dbo].tr_bo2_alt;
GO

CREATE TRIGGER  [dbo].[tr_bo2_alt] ON [dbo].[bo2]
   AFTER INSERT, UPDATE, DELETE
AS 
BEGIN
	SET NOCOUNT ON;

	Declare @Type varchar(50)='';
	IF EXISTS (SELECT * FROM inserted) and  EXISTS (SELECT * FROM deleted)
	BEGIN
		set  @Type = 'UPDATE'
		INSERT into [bo2_alt] (bo2stamp,autotipo,pdtipo,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,armazem,xpdviatura,xpddata,xpdhora,morada,codpost,telefone,contacto,email,etotalciva,etotiva,u_class,u_doccont,u_fostamp,ebo71_IVA,ebo81_IVA,ebo91_IVA
							   ,ATDocCode,exportado,ebo71_bins,ebo72_bins,ebo72_iva,ebo81_bins,ebo82_bins,ebo82_iva,ebo91_bins,ebo92_bins,ebo92_iva,IVATX1,IVATX2,IVATX3,IVATX4,IVATX5,IVATX6,IVATX7,IVATX8,IVATX9,nrReceita,codext
							   ,morada_ent,localidade_ent,codpost_ent,pais_ent,status,modo_envio,pagamento,u_nratend,pinAcesso,pinOpcao,nrviasimp,momento_pagamento,ebo101_bins,ebo111_bins,ebo121_bins,ebo131_bins,ebo101_iva,ebo111_iva,ebo121_iva,ebo131_iva
							   ,ivatx10,ivatx11,ivatx12,ivatx13,integrado,dataIntegrado,iddistribuidor,ticketId,alteracao,alteracaoData)
		select 	bo2stamp,autotipo,pdtipo,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,armazem,xpdviatura,xpddata,xpdhora,morada,codpost,telefone,contacto,email,etotalciva,etotiva,u_class,u_doccont,u_fostamp,ebo71_IVA,ebo81_IVA,ebo91_IVA
							   ,ATDocCode,exportado,ebo71_bins,ebo72_bins,ebo72_iva,ebo81_bins,ebo82_bins,ebo82_iva,ebo91_bins,ebo92_bins,ebo92_iva,IVATX1,IVATX2,IVATX3,IVATX4,IVATX5,IVATX6,IVATX7,IVATX8,IVATX9,nrReceita,codext
							   ,morada_ent,localidade_ent,codpost_ent,pais_ent,status,modo_envio,pagamento,u_nratend,pinAcesso,pinOpcao,nrviasimp,momento_pagamento,ebo101_bins,ebo111_bins,ebo121_bins,ebo131_bins,ebo101_iva,ebo111_iva,ebo121_iva,ebo131_iva
							   ,ivatx10,ivatx11,ivatx12,ivatx13,integrado,dataIntegrado,iddistribuidor,ticketId,@type,GETDATE()
		from inserted			


	END
	ELSE IF EXISTS(SELECT * FROM inserted)
	BEGIN
		set @Type = 'INSERT'
		INSERT into [bo2_alt] (bo2stamp,autotipo,pdtipo,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,armazem,xpdviatura,xpddata,xpdhora,morada,codpost,telefone,contacto,email,etotalciva,etotiva,u_class,u_doccont,u_fostamp,ebo71_IVA,ebo81_IVA,ebo91_IVA
							   ,ATDocCode,exportado,ebo71_bins,ebo72_bins,ebo72_iva,ebo81_bins,ebo82_bins,ebo82_iva,ebo91_bins,ebo92_bins,ebo92_iva,IVATX1,IVATX2,IVATX3,IVATX4,IVATX5,IVATX6,IVATX7,IVATX8,IVATX9,nrReceita,codext
							   ,morada_ent,localidade_ent,codpost_ent,pais_ent,status,modo_envio,pagamento,u_nratend,pinAcesso,pinOpcao,nrviasimp,momento_pagamento,ebo101_bins,ebo111_bins,ebo121_bins,ebo131_bins,ebo101_iva,ebo111_iva,ebo121_iva,ebo131_iva
							   ,ivatx10,ivatx11,ivatx12,ivatx13,integrado,dataIntegrado,iddistribuidor,ticketId,alteracao,alteracaoData)
		select 	bo2stamp,autotipo,pdtipo,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,armazem,xpdviatura,xpddata,xpdhora,morada,codpost,telefone,contacto,email,etotalciva,etotiva,u_class,u_doccont,u_fostamp,ebo71_IVA,ebo81_IVA,ebo91_IVA
							   ,ATDocCode,exportado,ebo71_bins,ebo72_bins,ebo72_iva,ebo81_bins,ebo82_bins,ebo82_iva,ebo91_bins,ebo92_bins,ebo92_iva,IVATX1,IVATX2,IVATX3,IVATX4,IVATX5,IVATX6,IVATX7,IVATX8,IVATX9,nrReceita,codext
							   ,morada_ent,localidade_ent,codpost_ent,pais_ent,status,modo_envio,pagamento,u_nratend,pinAcesso,pinOpcao,nrviasimp,momento_pagamento,ebo101_bins,ebo111_bins,ebo121_bins,ebo131_bins,ebo101_iva,ebo111_iva,ebo121_iva,ebo131_iva
							   ,ivatx10,ivatx11,ivatx12,ivatx13,integrado,dataIntegrado,iddistribuidor,ticketId,@type,GETDATE()
		from inserted	
	END
	ElSE IF EXISTS(SELECT * FROM deleted)
	BEGIN
		set @Type = 'DELETE'
		INSERT into [bo2_alt] (bo2stamp,autotipo,pdtipo,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,armazem,xpdviatura,xpddata,xpdhora,morada,codpost,telefone,contacto,email,etotalciva,etotiva,u_class,u_doccont,u_fostamp,ebo71_IVA,ebo81_IVA,ebo91_IVA
							   ,ATDocCode,exportado,ebo71_bins,ebo72_bins,ebo72_iva,ebo81_bins,ebo82_bins,ebo82_iva,ebo91_bins,ebo92_bins,ebo92_iva,IVATX1,IVATX2,IVATX3,IVATX4,IVATX5,IVATX6,IVATX7,IVATX8,IVATX9,nrReceita,codext
							   ,morada_ent,localidade_ent,codpost_ent,pais_ent,status,modo_envio,pagamento,u_nratend,pinAcesso,pinOpcao,nrviasimp,momento_pagamento,ebo101_bins,ebo111_bins,ebo121_bins,ebo131_bins,ebo101_iva,ebo111_iva,ebo121_iva,ebo131_iva
							   ,ivatx10,ivatx11,ivatx12,ivatx13,integrado,dataIntegrado,iddistribuidor,ticketId,alteracao,alteracaoData)
		select 	bo2stamp,autotipo,pdtipo,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,armazem,xpdviatura,xpddata,xpdhora,morada,codpost,telefone,contacto,email,etotalciva,etotiva,u_class,u_doccont,u_fostamp,ebo71_IVA,ebo81_IVA,ebo91_IVA
							   ,ATDocCode,exportado,ebo71_bins,ebo72_bins,ebo72_iva,ebo81_bins,ebo82_bins,ebo82_iva,ebo91_bins,ebo92_bins,ebo92_iva,IVATX1,IVATX2,IVATX3,IVATX4,IVATX5,IVATX6,IVATX7,IVATX8,IVATX9,nrReceita,codext
							   ,morada_ent,localidade_ent,codpost_ent,pais_ent,status,modo_envio,pagamento,u_nratend,pinAcesso,pinOpcao,nrviasimp,momento_pagamento,ebo101_bins,ebo111_bins,ebo121_bins,ebo131_bins,ebo101_iva,ebo111_iva,ebo121_iva,ebo131_iva
							   ,ivatx10,ivatx11,ivatx12,ivatx13,integrado,dataIntegrado,iddistribuidor,ticketId,@type,GETDATE()
		from deleted

		IF EXISTS (SELECT name FROM master.sys.databases WHERE name = N'mecosync')
		begin 

			IF (EXISTS (SELECT *  FROM mecosync.INFORMATION_SCHEMA.TABLES  WHERE     TABLE_NAME = 'BO2'))
			BEGIN	
				delete from mecosync.dbo.bo2 where mecosync.dbo.bo2.bo2stamp in (select 	bo2stamp from deleted)
			END

		END 
	END


END
