
/****** Object:  Trigger [dbo].[tr_bi2_alt]    Script Date: 02/02/2023 17:27:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Jose Simoes>
-- Create date: <18/11/2021>
-- Description:	<trigger to insert in bi2_alt>
-- =============================================
if OBJECT_ID('[dbo].[tr_bi2_alt]') IS NOT NULL
	DROP trigger [dbo].tr_bi2_alt;
GO

CREATE TRIGGER [dbo].[tr_bi2_alt] ON [dbo].[bi2]
   AFTER INSERT, UPDATE, DELETE
AS 
BEGIN
	SET NOCOUNT ON;

	Declare @Type varchar(50)='';
	IF EXISTS (SELECT * FROM inserted) and  EXISTS (SELECT * FROM deleted)
	BEGIN
		set  @Type = 'UPDATE'
		INSERT into [bi2_alt] (bi2stamp,fnstamp,bostamp,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,morada,local,codpost,fistamp,exportado,valeNr,idCampanha,qttembal,valcartao,nrreceita,alteracao,alteracaoData)
		select 	bi2stamp,fnstamp,bostamp,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,morada,local,codpost,fistamp,exportado,valeNr,idCampanha,qttembal,valcartao,nrreceita,@type,GETDATE()
		from inserted			


	END
	ELSE IF EXISTS(SELECT * FROM inserted)
	BEGIN
		set @Type = 'INSERT'
		INSERT into [bi2_alt] (bi2stamp,fnstamp,bostamp,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,morada,local,codpost,fistamp,exportado,valeNr,idCampanha,qttembal,valcartao,nrreceita,alteracao,alteracaoData)
		select 	bi2stamp,fnstamp,bostamp,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,morada,local,codpost,fistamp,exportado,valeNr,idCampanha,qttembal,valcartao,nrreceita,@type,GETDATE()
		from inserted	
	END
	ElSE IF EXISTS(SELECT * FROM deleted)
	BEGIN
		set @Type = 'DELETE'
		INSERT into [bi2_alt] (bi2stamp,fnstamp,bostamp,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,morada,local,codpost,fistamp,exportado,valeNr,idCampanha,qttembal,valcartao,nrreceita,alteracao,alteracaoData)
		select 	bi2stamp,fnstamp,bostamp,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,morada,local,codpost,fistamp,exportado,valeNr,idCampanha,qttembal,valcartao,nrreceita,@type,GETDATE()
		from deleted

		IF EXISTS (SELECT name FROM master.sys.databases WHERE name = N'mecosync')
		begin 

			IF (EXISTS (SELECT *  FROM mecosync.INFORMATION_SCHEMA.TABLES  WHERE     TABLE_NAME = 'BI2'))
			BEGIN
				delete from mecosync.dbo.bi2 where mecosync.dbo.bi2.bi2stamp in (select 	bi2stamp from deleted)

			END

		END 


	END


END
