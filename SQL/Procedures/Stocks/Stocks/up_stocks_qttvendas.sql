/*
	exec up_stocks_qttvendas 1,'3514080', 1
*/
IF OBJECT_ID('[dbo].[up_stocks_qttvendas]') IS NOT NULL
	drop procedure [dbo].[up_stocks_qttvendas]
GO

Create PROCEDURE [dbo].[up_stocks_qttvendas]
	@armazem	numeric(5,0),
	@ref		varchar(18),
	@site_nr	int

/* WITH ENCRYPTION */
AS

	If OBJECT_ID('tempdb.dbo.#temp_Movimentos') IS NOT NULL
		drop table #temp_Movimentos 
	If OBJECT_ID('tempdb.dbo.#Armazens') IS NOT NULL
		drop table #Armazens 
		
	/* armazens disponíveis */
	select
		distinct armazem
	into
		#Armazens
	From
		empresa
		inner join empresa_arm on empresa.no = empresa_arm.empresa_no
	Where
		empresa.no = case when @site_nr = -1 then empresa.no else @site_nr end

	/* Cria variaveis internas */
	Declare
		 @InicioAnoActual	datetime = DATEADD(yy, DATEDIFF(yy,0,getdate()), 0)
		,@FimAnoActual		datetime = DATEADD(MILLISECOND, -3,DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()) + 1, 0))
		,@InicioAnoAnterior datetime = DATEADD(YEAR, DATEDIFF(YEAR, 0,DATEADD(YEAR, -1, GETDATE())), 0)
		,@FimAnoAnterior	datetime = DATEADD(MILLISECOND, -3, DATEADD(YEAR,DATEDIFF(YEAR, 0, DATEADD(YEAR, -1, GETDATE())) + 1, 0)) ;

	/* Cria tabela temporaria */
	Create table #temp_Movimentos (
		[slstamp] [char](25) NOT NULL,
		[cm] [numeric](6,0) NOT NULL,
		[ref] [varchar](18) NOT NULL,
		[qtt] [numeric](11,3) NOT NULL DEFAULT (0),
		[datalc] [datetime] NOT NULL,
		[Tipo] [char](1) NOT NULL
	)

	/* Insert dos dados */
	Insert #temp_Movimentos
	SELECT	
		 slstamp
		,cm
		,ref
		,qtt = case 
					when (origem = 'FT' or cmdesc = 'Cons. interno') then case when cm < 50 then -qtt else qtt end
					when (origem = 'FO' OR cm = 55) then case when cm < 50 then qtt else -qtt end
					when (origem = 'IM') then qtt 
					else 0
			   end
		,datalc
		,Tipo = Case 
					When (origem = 'FT') then 'S'
					When (cmdesc = 'Cons. interno') then 'S'
					When (origem = 'FO') then 'E'
					When (cm = 55) Then 'E'
					When (origem = 'IM') then 'S'
				end
	FROM
		sl (nolock)
	where	
		(origem = 'FT' or cmdesc = 'Cons. interno' or origem = 'FO' OR cm = 55 or origem = 'IM')
		and ref = @ref
		and armazem in (select armazem from #Armazens)
		and datalc between @InicioAnoAnterior and @FimAnoActual


	/* Junta o resultado dos querys */
	/* saidas do ano corrente */
	select	YEAR(GETDATE()) as ANO,'S' as Tipo, 
		 'JAN' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'S' and (datalc >= @InicioAnoActual) and (datalc < DateAdd(mm,1,@InicioAnoActual)))
		,'FEV' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'S' and (datalc >= DateAdd(mm,1,@InicioAnoActual)) and (datalc < DateAdd(mm,2,@InicioAnoActual)))
		,'MAR' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'S' and (datalc >= DateAdd(mm,2,@InicioAnoActual)) and (datalc < DateAdd(mm,3,@InicioAnoActual)))
		,'ABR' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'S' and (datalc >= DateAdd(mm,3,@InicioAnoActual)) and (datalc < DateAdd(mm,4,@InicioAnoActual)))
		,'MAI' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'S' and (datalc >= DateAdd(mm,4,@InicioAnoActual)) and (datalc < DateAdd(mm,5,@InicioAnoActual)))
		,'JUN' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'S' and (datalc >= DateAdd(mm,5,@InicioAnoActual)) and (datalc < DateAdd(mm,6,@InicioAnoActual)))
		,'JUL' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'S' and (datalc >= DateAdd(mm,6,@InicioAnoActual)) and (datalc < DateAdd(mm,7,@InicioAnoActual)))
		,'AGO' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'S' and (datalc >= DateAdd(mm,7,@InicioAnoActual)) and (datalc < DateAdd(mm,8,@InicioAnoActual)))
		,'SET' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'S' and (datalc >= DateAdd(mm,8,@InicioAnoActual)) and (datalc < DateAdd(mm,9,@InicioAnoActual)))
		,'OUT' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'S' and (datalc >= DateAdd(mm,9,@InicioAnoActual)) and (datalc < DateAdd(mm,10,@InicioAnoActual)))
		,'NOV' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'S' and (datalc >= DateAdd(mm,10,@InicioAnoActual)) and (datalc < DateAdd(mm,11,@InicioAnoActual)))
		,'DEZ' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'S' and (datalc >= DateAdd(mm,11,@InicioAnoActual)) and (datalc <= @FimAnoActual))

	UNION ALL
	
	/* entradas do ano corrente */
	select	YEAR(GETDATE()) as ANO,'E' as Tipo, 
		 'JAN' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'E' and (datalc >= @InicioAnoActual) and (datalc < DateAdd(mm,1,@InicioAnoActual)))
		,'FEV' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'E' and (datalc >= DateAdd(mm,1,@InicioAnoActual)) and (datalc < DateAdd(mm,2,@InicioAnoActual)))
		,'MAR' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'E' and (datalc >= DateAdd(mm,2,@InicioAnoActual)) and (datalc < DateAdd(mm,3,@InicioAnoActual)))
		,'ABR' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'E' and (datalc >= DateAdd(mm,3,@InicioAnoActual)) and (datalc < DateAdd(mm,4,@InicioAnoActual)))
		,'MAI' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'E' and (datalc >= DateAdd(mm,4,@InicioAnoActual)) and (datalc < DateAdd(mm,5,@InicioAnoActual)))
		,'JUN' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'E' and (datalc >= DateAdd(mm,5,@InicioAnoActual)) and (datalc < DateAdd(mm,6,@InicioAnoActual)))
		,'JUL' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'E' and (datalc >= DateAdd(mm,6,@InicioAnoActual)) and (datalc < DateAdd(mm,7,@InicioAnoActual)))
		,'AGO' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'E' and (datalc >= DateAdd(mm,7,@InicioAnoActual)) and (datalc < DateAdd(mm,8,@InicioAnoActual)))
		,'SET' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'E' and (datalc >= DateAdd(mm,8,@InicioAnoActual)) and (datalc < DateAdd(mm,9,@InicioAnoActual)))
		,'OUT' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'E' and (datalc >= DateAdd(mm,9,@InicioAnoActual)) and (datalc < DateAdd(mm,10,@InicioAnoActual)))
		,'NOV' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'E' and (datalc >= DateAdd(mm,10,@InicioAnoActual)) and (datalc < DateAdd(mm,11,@InicioAnoActual)))
		,'DEZ' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'E' and (datalc >= DateAdd(mm,11,@InicioAnoActual)) and (datalc <= @FimAnoActual))

	UNION ALL
		
	/* saidas do ano anterior */
	Select YEAR(GETDATE())-1 as ANO,'S' as Tipo, 
		 'JAN' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'S' and (datalc >= @InicioAnoAnterior) and (datalc < DateAdd(mm,1,@InicioAnoAnterior)))
		,'FEV' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'S' and (datalc >= DateAdd(mm,1,@InicioAnoAnterior)) and (datalc < DateAdd(mm,2,@InicioAnoAnterior)))
		,'MAR' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'S' and (datalc >= DateAdd(mm,2,@InicioAnoAnterior)) and (datalc < DateAdd(mm,3,@InicioAnoAnterior)))
		,'ABR' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'S' and (datalc >= DateAdd(mm,3,@InicioAnoAnterior)) and (datalc < DateAdd(mm,4,@InicioAnoAnterior)))
		,'MAI' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'S' and (datalc >= DateAdd(mm,4,@InicioAnoAnterior)) and (datalc < DateAdd(mm,5,@InicioAnoAnterior)))
		,'JUN' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'S' and (datalc >= DateAdd(mm,5,@InicioAnoAnterior)) and (datalc < DateAdd(mm,6,@InicioAnoAnterior)))
		,'JUL' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'S' and (datalc >= DateAdd(mm,6,@InicioAnoAnterior)) and (datalc < DateAdd(mm,7,@InicioAnoAnterior)))
		,'AGO' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'S' and (datalc >= DateAdd(mm,7,@InicioAnoAnterior)) and (datalc < DateAdd(mm,8,@InicioAnoAnterior)))
		,'SET' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'S' and (datalc >= DateAdd(mm,8,@InicioAnoAnterior)) and (datalc < DateAdd(mm,9,@InicioAnoAnterior)))
		,'OUT' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'S' and (datalc >= DateAdd(mm,9,@InicioAnoAnterior)) and (datalc < DateAdd(mm,10,@InicioAnoAnterior)))
		,'NOV' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'S' and (datalc >= DateAdd(mm,10,@InicioAnoAnterior)) and (datalc < DateAdd(mm,11,@InicioAnoAnterior)))
		,'DEZ' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'S' and (datalc >= DateAdd(mm,11,@InicioAnoAnterior)) and (datalc < @InicioAnoActual))

	UNION ALL

	/* entradas do ano anterior */
	select	YEAR(GETDATE())-1 as ANO,'E' as Tipo, 
		 'JAN' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'E' and (datalc >= @InicioAnoAnterior) and (datalc < DateAdd(mm,1,@InicioAnoAnterior)))
		,'FEV' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'E' and (datalc >= DateAdd(mm,1,@InicioAnoAnterior)) and (datalc < DateAdd(mm,2,@InicioAnoAnterior)))
		,'MAR' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'E' and (datalc >= DateAdd(mm,2,@InicioAnoAnterior)) and (datalc < DateAdd(mm,3,@InicioAnoAnterior)))
		,'ABR' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'E' and (datalc >= DateAdd(mm,3,@InicioAnoAnterior)) and (datalc < DateAdd(mm,4,@InicioAnoAnterior)))
		,'MAI' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'E' and (datalc >= DateAdd(mm,4,@InicioAnoAnterior)) and (datalc < DateAdd(mm,5,@InicioAnoAnterior)))
		,'JUN' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'E' and (datalc >= DateAdd(mm,5,@InicioAnoAnterior)) and (datalc < DateAdd(mm,6,@InicioAnoAnterior)))
		,'JUL' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'E' and (datalc >= DateAdd(mm,6,@InicioAnoAnterior)) and (datalc < DateAdd(mm,7,@InicioAnoAnterior)))
		,'AGO' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'E' and (datalc >= DateAdd(mm,7,@InicioAnoAnterior)) and (datalc < DateAdd(mm,8,@InicioAnoAnterior)))
		,'SET' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'E' and (datalc >= DateAdd(mm,8,@InicioAnoAnterior)) and (datalc < DateAdd(mm,9,@InicioAnoAnterior)))
		,'OUT' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'E' and (datalc >= DateAdd(mm,9,@InicioAnoAnterior)) and (datalc < DateAdd(mm,10,@InicioAnoAnterior)))
		,'NOV' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'E' and (datalc >= DateAdd(mm,10,@InicioAnoAnterior)) and (datalc < DateAdd(mm,11,@InicioAnoAnterior)))
		,'DEZ' = (select ISNULL(SUM(qtt),0) From #temp_Movimentos WHERE tipo = 'E' and (datalc >= DateAdd(mm,11,@InicioAnoAnterior)) and (datalc < @InicioAnoActual))


	/* Remove tabelas temporarias */
	If OBJECT_ID('tempdb.dbo.#temp_Movimentos') IS NOT NULL
		drop table #temp_Movimentos 
	If OBJECT_ID('tempdb.dbo.#Armazens') IS NOT NULL
		drop table #Armazens 

GO
GRANT EXECUTE on dbo.up_stocks_qttvendas TO PUBLIC
GRANT Control on dbo.up_stocks_qttvendas TO PUBLIC
GO