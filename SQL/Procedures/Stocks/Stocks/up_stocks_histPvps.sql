-- Hist�rico de PVPs --
-- exec up_stocks_histPvps '20100211', '20190311','','','Loja 2'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_histPvps]') IS NOT NULL
	drop procedure dbo.up_stocks_histPvps
go

CREATE PROCEDURE dbo.up_stocks_histPvps

@dataIni	datetime,
@dataFim	datetime,
@ref		as varchar(30),
@local		as varchar(254),
@site		as varchar(20)

/* WITH ENCRYPTION */
AS

	Select
		B_historicopvp.ref
		,dataaltpvp as data --em casos de altera��es programadas alterado para mostrar a verdadeira data de altera��o, e n�o a data em que foi criada a programa��o de altera��o. Nos outros casos, data = dataaltpvp
		,B_historicopvp.local
		,opvp
		,dpvp
		,oMc
		,dMc
		,oBb
		,dBb
		,st.design
		,b_us.nome				as 'nomeoperador'
	From	
		B_historicopvp (nolock)
		INNER JOIN empresa (nolock) ON empresa.site = B_historicopvp.site
		INNER JOIN st (nolock) on st.ref = B_historicopvp.ref and empresa.no = st.site_nr
		LEFT JOIN b_us (nolock) on b_us.userno = B_historicopvp.usr

	Where
		convert(date,B_historicopvp.dataaltpvp,102) between @dataIni and @dataFim
		and B_historicopvp.site = @site
		and B_historicopvp.ref = (Case When @ref = '' Then B_historicopvp.ref Else @ref end)
		and B_historicopvp.local = (Case When @local = '' Then B_historicopvp.local Else @local end)
		and B_historicopvp.dpvp > 0

GO
Grant Execute on dbo.up_stocks_histPvps to Public
Grant Control on dbo.up_stocks_histPvps to Public
go