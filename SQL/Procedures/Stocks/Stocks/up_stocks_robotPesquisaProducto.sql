/*
    retorna informação do producto para devolver ao robot

	campo 1 - referencia do producto
	campo 2 - id da base da loja

	exec up_stocks_robotPesquisaProducto  '1000158', 'ltdev30'

	exec up_stocks_robotPesquisaProducto '
	', 'ltdev30';


		exec up_stocks_robotPesquisaProducto '8058090008347', 'F01025A';


		select *from b_utentes where ref='6916478'


 	exec up_stocks_robotPesquisaProducto '', 'F01025A'

*/





SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_robotPesquisaProducto]') IS NOT NULL
	drop procedure dbo.up_stocks_robotPesquisaProducto
go

create procedure dbo.up_stocks_robotPesquisaProducto

@ref varchar(100),
@id_lt varchar(100)

/* WITH ENCRYPTION */
AS

	SET NOCOUNT ON

	declare @no int
	declare @site varchar(100)=''

	select top 1 @no = no, @site = site from empresa(nolock) where id_lt = @id_lt
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempStRobot'))
		DROP TABLE #tempStRobot


	set @site = isnull(@site,'')
	--Cria ficha ao inicio 
	IF  EXISTS (SELECT 1 FROM B_Parameters_site(nolock) WHERE stamp='ADM0000000206' and site=@site and bool=1)
	BEGIN
		exec up_stocks_criaSt @ref, @no, @id_lt, 0
	END



	create table #tempStRobot
	(
		productCode    VARCHAR(18),
		expiryDate     VARCHAR(8),
		name           VARCHAR(100),
		packageUnit    VARCHAR(10),
		kindOfOffering VARCHAR(10),
		local VARCHAR(254),


	)

	insert into #tempStRobot
	select
		top 1
		isnull(fprod.ref,''),
		isnull(CONVERT(VARCHAR(8), st.validade, 112), '19000101'),
		rtrim(ltrim(st.design)),
		rtrim(ltrim(convert(varchar(10),isnull(fprod.dosuni,'')))),
		isnull(UPPER(LEFT(RTRIM(LTRIM(fprod.fformasabrev)),3)),''),
		st.[local]

	from     
		st (nolock)
		left join  fprod(nolock) on st.ref = fprod.ref
	where
		st.ref = @ref and site_nr = @no

	
	

	if(select count(*) from #tempStRobot) = 0
	begin
	
	    insert into #tempStRobot
		select
		top 1
			isnull(st.ref,''),
			isnull(CONVERT(VARCHAR(8), st.validade, 112), '19000101'),
			rtrim(ltrim(st.design)),
			rtrim(ltrim(convert(varchar(10),isnull(fprod.dosuni,'')))),
			isnull(UPPER(LEFT(RTRIM(LTRIM(fprod.fformasabrev)),3)),''),
			st.[local]
		from     
			bc (nolock)
			left join  st(nolock) on st.ref = bc.ref and bc.site_nr = st.site_nr
			left join  fprod(nolock) on st.ref = fprod.ref
		where
			bc.codigo = @ref and st.site_nr = @no and bc.codigo !=''
	end



	--Cria ficha ao fim 
	if(select count(*) from #tempStRobot) = 0
	begin
		IF  EXISTS (SELECT 1 FROM B_Parameters_site(nolock) WHERE stamp='ADM0000000206' and site=@site and bool=0)
		BEGIN
			exec up_stocks_criaSt @ref, @no, @id_lt, 0



			insert into #tempStRobot
			select
				top 1
				isnull(fprod.ref,''),
				isnull(CONVERT(VARCHAR(8), st.validade, 112), '19000101'),
				rtrim(ltrim(st.design)),
				rtrim(ltrim(convert(varchar(10),isnull(fprod.dosuni,'')))),
				isnull(UPPER(LEFT(RTRIM(LTRIM(fprod.fformasabrev)),3)),''),
				st.[local]
			from     
				st (nolock)
				left join  fprod(nolock) on st.ref = fprod.ref
			where
				st.ref = @ref and site_nr = @no
		END
	END	


	select 
		*
	from 
		#tempStRobot

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempStRobot'))
		DROP TABLE #tempStRobot

GO
Grant Execute On dbo.up_stocks_robotPesquisaProducto to Public
Grant Control On dbo.up_stocks_robotPesquisaProducto to Public
GO



