-- Visualiza Detalhe de Atributos em determinado Produto; --
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_AtributosDetalhes]') IS NOT NULL
	drop procedure dbo.up_stocks_AtributosDetalhes
go

create procedure dbo.up_stocks_AtributosDetalhes
@ref varchar(254)

/* WITH ENCRYPTION */
AS

	SELECT 
		TIPO
		,DESCR as ATRIBUTO 
	from 
		B_statrib (NOlOCK) 
	WHERE 
		B_statrib.REF = @ref             

GO
Grant Execute On dbo.up_stocks_AtributosDetalhes to Public
Grant Control On dbo.up_stocks_AtributosDetalhes to Public
GO
