/* Pesquisa de Validades nas Vendas nos Lotes 

	 exec up_stocks_pesquisaValidadesLotes '', '', 0, '', 1, 'st', '','','',1,'a,b,c'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_pesquisaValidadesLotes]') IS NOT NULL
	drop procedure dbo.up_stocks_pesquisaValidadesLotes
go

create procedure dbo.up_stocks_pesquisaValidadesLotes

@prod			varchar(60) = '',
@validade		datetime,
@svalidade		bit,
@familia		varchar(60) = '',
@stock			numeric(18,2),
@painel			varchar(30) = '',
@doc			varchar(40) = '',
@validade2		datetime,
@lote			as varchar(60) = '',
@site_nr		as tinyint,
@localizacao	varchar(254) = ''

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#localizacoes'))
		DROP TABLE #localizacoes
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFinais'))
		DROP TABLE #dadosFinais	

		create table #localizacoes (local varchar(254))


	IF(@localizacao = '')
		begin
			insert #localizacoes (local)
			select descr from locais(nolock) union select '' as descr from locais(nolock)
		end 
	else IF @localizacao = 'Sem Localiza��o'
		begin 
			insert into #localizacoes (local)
			values ('')
		end
	else
		begin
			insert #localizacoes (local)
			select items from dbo.up_splitToTable(@localizacao,',')
		end

create table #dadosFinais (
		sel				bit
		,ref			varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI	
		,design			varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,stock			numeric(18,2)
		,epv1			numeric(19,6)
		,validade		varchar(10)
		,validade2		varchar(10)
		,faminome		varchar(100)
		,classUcrs2		varchar(150)
		,comp			bit									
		,generico		bit
		,GRPHMG			varchar(254)	
		,psico			bit
		,benzo			bit
		,ststamp		char(25)
		,ptoenc			numeric(10,3)
		,stmax			numeric(20,13)
		,marg1			numeric(16,3)
		,local			varchar(60)
		,u_local		varchar(60)
		,u_local2		varchar(60)
		,obs			varchar(254)
		,Val			datetime
		,dci			varchar(254)	
		,lote			varchar(60)
		)

if @painel='ST'
begin
	insert into #dadosFinais
	Select distinct
		 sel		= convert(bit,0)
		,ref		= st.ref
		,design		= st.design
		,stock		= isnull(dbo.unityConverter(st.stock,'stocksUnityToBox', @site_nr ,st.ref),0)
		,epv1		= st.epv1
		,validade	= left(convert(varchar, ISNULL(st_lotes.validade,st.validade), 102),7)
		,validade2	= left(convert(varchar, ISNULL(st_lotes.validade,st.validade), 102),7)
		,faminome	= st.faminome
		,classUcrs2		= st.usr2desc
		,comp		= isnull(
						(case when (left(cptgrp.descri��o,3)!='N�o' and cptgrp.u_diploma1='' and cptgrp.u_diploma2='' and cptgrp.u_diploma3='') AND cptgrp.grupo!='ST'
							then 1
							else 0
						end)
						,0)
		,generico	= IsNull(fprod.generico,0)
		,GRPHMG		= case when fprod.grphmgcode='iGH0000'
						  then fprod.grphmgcode
						  else fprod.grphmgcode + ' - ' + fprod.grphmgdescr
					  end
		,psico		= isnull(fprod.psico,0)
		,benzo		= isnull(fprod.benzo,0)
		,ststamp	= st.ststamp
		,ptoenc		= st.ptoenc
		,stmax		= st.stmax
		,marg1		= st.marg1
		,local		= st.local
		,u_local	= st.u_local
		,u_local2	= st.u_local2
		,obs		= (case when (select bool from B_Parameters(nolock) where stamp ='ADM0000000354')= 1 then ISNULL(convert(varchar(254),u_nota1),'') else ISNULL(convert(varchar(254),obs),'') end)
		,Val		= @validade
		,dci		= isnull(fprod.dci,'')
		,lote		= isnull(lote,'')
	from
		st (nolock)
		left join fprod  (nolock) on st.ref=fprod.cnp
		left join cptgrp (nolock) on cptgrp.grupo=fprod.grupo
		left join st_lotes (nolock) on st.ref = st_lotes.ref
	where  
		st.site_nr = @site_nr
		AND(
			(st.design like @prod+'%' ) OR 
			(st.ref like @prod+'%') OR 
			(st.usr1 like @prod+'%') 
		)
		AND (
			st.validade between @validade2 and @validade
			OR 
			st.validade = case when @svalidade = 1 then '19000101' else '18000101' end
			)
		AND st.inactivo = 0
		AND st.faminome like @familia+'%'
		And st.stock >= @stock
		and isnull(st_lotes.lote,'') = case when @lote = '' then isnull(st_lotes.lote,'') else @lote end
		and st.stns=0
		and ( (st.local  COLLATE DATABASE_DEFAULT  in  (select  local from #localizacoes(nolock)))
					     	or  (st.u_local COLLATE DATABASE_DEFAULT  in  (select local from #localizacoes(nolock)))
						   or  (st.u_local2 COLLATE DATABASE_DEFAULT  in  (select local from #localizacoes(nolock)))
						)	
	order by 
		st.design
end

if @painel='DOCUMENTOS'
begin
	insert into #dadosFinais
	Select distinct
		sel			= convert(bit,0)
		,ref		= st.ref
		,design		= st.design
		,stock		= isnull(dbo.unityConverter(st.stock,'stocksUnityToBox', @site_nr ,st.ref),0)
		,epv1		= st.epv1
		,validade	= isnull(left(convert(varchar, st_lotes.validade, 102),7),'1900.01')
		,validade2	= isnull(left(convert(varchar, st_lotes.validade, 102),7),'1900.01')
		,faminome	= st.faminome
		,classUcrs2		= st.usr2desc
		,comp		= isnull((case when (left(cptgrp.descri��o,3)!='N�o' and cptgrp.u_diploma1='' and cptgrp.u_diploma2='' and cptgrp.u_diploma3='') AND cptgrp.grupo!='ST'
								  then 1
								  else 0
								end)
							  ,0)
		,generico	= IsNull(fprod.generico,0)
		,GRPHMG		= case when fprod.grphmgcode='GH0000'
						then fprod.grphmgcode
						else fprod.grphmgcode + ' - ' + fprod.grphmgdescr
					  end
		,psico		= isnull(fprod.psico,0)
		,benzo		= isnull(fprod.benzo,0)
		,ststamp	= st.ststamp
		,ptoenc		= st.ptoenc
		,stmax		= st.stmax
		,marg1		= st.marg1
		,local		= st.local
		,u_local	= st.u_local
		,u_local2	= st.u_local2
		,obs		= (case when (select bool from B_Parameters(nolock) where stamp ='ADM0000000354')= 1 then ISNULL(convert(varchar(254),u_nota1),'') else ISNULL(convert(varchar(254),obs),'') end)
		,Val		= @validade
		,dci		= isnull(fprod.dci,'')
		,lote		= isnull(st_lotes.lote,'')
	from
		st (nolock)
		inner join fn (nolock) on fn.ref=st.ref or fn.oref=st.ref
		left join fprod  (nolock) on st.ref=fprod.cnp
		left join cptgrp (nolock) on cptgrp.grupo=fprod.grupo
		left join st_lotes (nolock) on st.ref = st_lotes.ref
	where  
		st.site_nr = @site_nr
		AND(
			(st.design like @prod+'%') OR 
			(st.ref like @prod+'%') OR
			 (st.usr1 like @prod+'%')
		)
		AND (
			st.validade between @validade2 and @validade
			AND
			st.validade = case when @svalidade = 1 then '19000101' else '18000101' end
			)
		AND st.inactivo=0 
		AND st.faminome like @familia+'%'
		And st.stock>=@stock
		and fn.fostamp = @doc
		and isnull(st_lotes.lote,'') = case when @lote = '' then isnull(st_lotes.lote,'') else @lote end
		and st.stns=0
		and ( (st.local  COLLATE DATABASE_DEFAULT  in  (select  local from #localizacoes(nolock)))
					     	or  (st.u_local COLLATE DATABASE_DEFAULT  in  (select local from #localizacoes(nolock)))
						   or  (st.u_local2 COLLATE DATABASE_DEFAULT  in  (select local from #localizacoes(nolock)))
						)
	order by 
		st.design
end

	if @localizacao='Sem Localiza��o'
		begin
			delete  from #dadosFinais where local !='' or u_local !='' or u_local2 !=''	
		end

	select * from #dadosFinais


	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#localizacoes'))
		DROP TABLE #localizacoes
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFinais'))
		DROP TABLE #dadosFinais	
GO
Grant Execute On dbo.up_stocks_pesquisaValidadesLotes to Public
Grant control On dbo.up_stocks_pesquisaValidadesLotes to Public
GO