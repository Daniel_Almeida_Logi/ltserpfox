/*
    grava informação de stocks de productos

	campo 1 - stamp
	campo 2 - referencia do producto - 000 - fim de inventario
	campo 3 - token do pedido
	campo 4 - força a inserção de linha de fim de stock
	campo 5 - maximo de referencias enviadas


	exec up_stocks_roboInsereUltimoStock  'admxpto', '000', 'tokenxpto', 1, 0
	exec up_stocks_roboInsereUltimoStock  'admxpto1', '000', 'tokenxpto1', 0, 5408



*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_roboInsereUltimoStock]') IS NOT NULL
	drop procedure dbo.up_stocks_roboInsereUltimoStock
go

create procedure dbo.up_stocks_roboInsereUltimoStock

@stamp varchar(100),
@ref varchar(100),
@token varchar(100),
@force bit,
@maxRefs int


/* WITH ENCRYPTION */
AS


		
		/* se force = 1 então deve ser  guardado o fim de inventario */

		if(@force=1)
		begin

			INSERT INTO [dbo].[robot_response_payload_stock]
			 (stamp
			 ,token
			 ,productCode
			 ,lastRequest)
			 VALUES
			 (@stamp
			 ,@token
			 ,@ref
			 ,1
			 )

		end
		else
		begin

			/* se force = 0 então deve ser avaliado se já chegou ao fim */

			declare @no int = -1		
			
			select 
				@no = count(*) 
			from 
				robot_response_payload_stock(nolock) 
			where 
				token = @token

			/* se já guardou todas as referncias deve marcar como fim de inventario */

			if(@no >= @maxRefs)
			begin

					INSERT INTO [dbo].[robot_response_payload_stock]
					 (stamp
					 ,token
					 ,productCode
					 ,lastRequest)
					 VALUES
					 (@stamp
					 ,@token
					 ,@ref
					 ,1
					 )

			end


		end






GO
Grant Execute On dbo.up_stocks_roboInsereUltimoStock to Public
Grant Control On dbo.up_stocks_roboInsereUltimoStock to Public
GO



