/*
	 exec up_stocks_imagensProd '8B74718F-4206-4169-BBE9-5' , '1', 'imagemProd'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_imagensProd]') IS NOT NULL
	drop procedure dbo.up_stocks_imagensProd
go

create procedure dbo.up_stocks_imagensProd
@stamp varchar(28),
@siteno int,
@tipo varchar(50) = ''


/* WITH ENCRYPTION */
AS
	
	declare @site varchar(14) = ''
	declare @caminho varchar(254) = ''
	declare @caminhoDic varchar(254) = ''
	declare @dicImages bit = 0
	declare @dicText varchar(254) = ''
	
	SET NOCOUNT ON

	select 
		top 1
		@site = isnull(empresa.site,'Loja 1')
	from
		empresa(nolock) 
	where
		no = @siteno

	select 
		top 1 @caminho = ltrim(rtrim(textValue)) 
	from 
		B_Parameters_site(nolock) 
	where stamp='ADM0000000010' and site = @site

	select 
		top 1 @caminhoDic = ltrim(rtrim(textValue)) 
	from 
		B_Parameters_site(nolock) 
	where stamp='ADM0000000106' and site = @site

	select 
		top 1 @dicImages = isnull(bool,0)
	from 
		B_Parameters_site(nolock) 
	where stamp='ADM0000000107' and site = @site


	if(isnull(@dicImages,0)=1)
	  set @dicText = 'DIC'
	else
	  set @dicText = 'XXXXXXXXXXXXXXXXXXXXX'
	

	select 
	   imagePath = case 
						when UPPER(ltrim(rtrim(tipo)))!='DIC' 
						then  left(ltrim(rtrim(isnull(@caminho,'') +'\'+tabela+'\' + filename)),254) 
						else  left(ltrim(rtrim(isnull(@caminhoDic,'') +'\'+tabela+'\' + filename)),254) 
					end
	from 
		anexos(nolock)
	where 
		regStamp=@stamp and
		(tipo = case when @tipo = '' then tipo else @tipo end or UPPER(ltrim(rtrim(tipo)))=@dicText) and 
		(isnull(validade,'3000-01-01') >=  GETDATE() or validade='1900-01-01')
		
	order by ousrdata desc



GO
Grant Execute On dbo.up_stocks_imagensProd to Public
Grant Control On dbo.up_stocks_imagensProd to Public
GO


