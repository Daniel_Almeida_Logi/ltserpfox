/* Cria��o de Refrencias autom�ticas

	 exec up_stocks_refautomatica
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_refautomatica]') IS NOT NULL
    drop procedure dbo.up_stocks_refautomatica
go

create procedure dbo.up_stocks_refautomatica

/* WITH ENCRYPTION */
AS

	IF not exists (Select Top 1 textValue from B_PARAMETERS where stamp = 'ADM0000000082' and textValue = 'CLINICA')
	BEGIN
		DECLARE @TEXTVALUE		AS varchar(100)
		DECLARE @INICIO			AS varchar(1)
		DECLARE @TAMANHO		AS int
		DECLARE @VALOR_INCIAL	AS varchar(10)

		SET @TEXTVALUE		= (SELECT TEXTVALUE FROM B_PARAMETERS (NOLOCK) WHERE	STAMP = 'ADM0000000160')
		SET @INICIO			= (SELECT LEFT(TEXTVALUE,1) FROM B_PARAMETERS (NOLOCK) WHERE	STAMP = 'ADM0000000160')
		SET @TAMANHO		= (SELECT LEN(TEXTVALUE)-LEN(REPLACE(TEXTVALUE,'N','')) FROM	 B_PARAMETERS (NOLOCK) WHERE	STAMP = 'ADM0000000160')
		SET @VALOR_INCIAL	= (SELECT REPLACE(TEXTVALUE,'N',0) FROM	B_PARAMETERS (NOLOCK) WHERE	STAMP = 'ADM0000000160')
		
		SELECT	convert(varchar(20),MAX(ref) +1) AS REF 
		FROM	(
			SELECT	@VALOR_INCIAL AS ref
			UNION	ALL 
			SELECT	ISNULL(replace(REF,'.',''),0) AS NEWREF 
			FROM	ST (NOLOCK)
			WHERE	LEFT(LTRIM(RTRIM(REF)),1)	=@INICIO
					AND LEN(LTRIM(RTRIM(REF)))	=@TAMANHO+1
					AND isnumeric(st.ref) = 1
		) AS X
	END
	ELSE
	BEGIN
		Select
			ref = convert(varchar,MAX(REF) + 1)
		From
			st (nolock)
		WHERE
			isnumeric(st.ref) = 1
	END

GO
Grant Execute on dbo.up_stocks_refautomatica to Public
Grant Control on dbo.up_stocks_refautomatica to Public
go
