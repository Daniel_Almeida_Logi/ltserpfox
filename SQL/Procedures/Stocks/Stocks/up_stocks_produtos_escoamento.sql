/* 
Sp usava para prenecher um campos de prpodutos em escoamento do lado do fox 

exec up_stocks_produtos_escoamento '7133660'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_stocks_produtos_escoamento]') IS NOT NULL
DROP PROCEDURE dbo.up_stocks_produtos_escoamento
GO

Create Procedure up_stocks_produtos_escoamento

@ref	as varchar(18)
	
AS
SET NOCOUNT ON

Select 
	cnp,
	convert(datetime,data_inicio) as data_inicio,
	convert(datetime, data_fim) as data_fim,
	CAST(descr as varchar(200)) as descr, 
	convert(bit,1)   as sel  
From 
	escoamento(nolock)
	left join motivo_escoamento(nolock) on  motivo_escoamento.id = escoamento.motivo_escoamento_id
Where
	data_inicio<=getdate() and  escoamento.motivo_escoamento_id = 5 and 
	cnp = ISNULL(Ltrim(@ref),'') and
	isnull(data_fim,'3000-01-01') >=getdate()
order by data_fim asc


GO
Grant Execute On up_stocks_produtos_escoamento to Public
Grant Control On up_stocks_produtos_escoamento to Public
GO

	