/* Pesquisa stock em todas as lojas
		
		campo 1 - referencia do producto
		campo 2 - inclui inactivos
		campo 3 - loja preferencial



		exec up_stock_pesquisaStocksTodasLojas '1996551110', 0, 'Loja 2'


*/



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



if OBJECT_ID('[dbo].[up_stock_pesquisaStocksTodasLojas]') IS NOT NULL
    drop procedure dbo.up_stock_pesquisaStocksTodasLojas
go

create procedure dbo.up_stock_pesquisaStocksTodasLojas

@ref		varchar(255)
,@incInativo	bit
,@loja		varchar(255)

AS

	SET NOCOUNT ON

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#st_stocks'))
			DROP TABLE #st_stocks

	
	declare @no int

	select top 1 @no = no from empresa(nolock) where site = @loja



	select    
		s.ref, 
		stock = (select sum(st1.stock)  - sum (st1.qttcli) from  st (nolock) st1  where st1.ref = s.ref) ,
		'stamp' = CONVERT(varchar(255), NEWID() ),
		s.epv1,  s.epcusto,      iva = taxasiva.taxa, s.design,  s.usr1, s.familia, 
		s.validade,s.ousrdata, s.ousrhora,s.inactivo, s.obs
	into 
		#st_stocks	
	from     
		 st (nolock)   as s 
	inner join taxasiva (nolock) on taxasiva.codigo=s.tabiva 
	where  
		s.ref = case when @ref = '' then  s.ref else @ref end
		and inactivo= case when @incInativo = 1 then  inactivo else 0 end
		and s.site_nr = @no

		

	DELETE FROM #st_stocks WHERE stamp IN (
		SELECT stamp FROM (
			SELECT 
				stamp,ROW_NUMBER() OVER (PARTITION BY ref ORDER BY ref) AS [ItemNumber]
			FROM 
				#st_stocks
		) a WHERE ItemNumber > 1 -- Keep only the first unique item
	)


	select * from #st_stocks

	 IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#st_stocks'))
			DROP TABLE #st_stocks


GO
	Grant Execute on dbo.up_stock_pesquisaStocksTodasLojas to Public
	Grant Control on dbo.up_stock_pesquisaStocksTodasLojas to Public
go

