/*Lista Grupos Homogeneo com informação de stock actual e Vendas Ult Mes*/
/* exec up_stocks_CorrigeSA 1  */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_CorrigeSA]') IS NOT NULL
    drop procedure dbo.up_stocks_CorrigeSA
go

create procedure dbo.up_stocks_CorrigeSA
@site_nr tinyint,
@result bit = 0

/* WITH ENCRYPTION */
AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteArmazens'))
		DROP TABLE #cteArmazens

	select 
		armazem 
	into
		#cteArmazens
	from 
		empresa_arm 
	where 
		empresa_no = @site_nr
		

	DECLARE @cteReservasClientes TABLE (ref varchar(18),armazem numeric(5),qtt numeric(9));
	insert into	@cteReservasClientes
	select 	bi.ref, bi.armazem, qtt = bi.qtt-bi.qtt2 from ts inner join bi on bi.ndos = ts.ndos inner join bo on bo.bostamp=bi.bostamp where ts.rescli = 1 and bi.fechada = 0 and bo.fechada=0

	DECLARE @cteReservasFornecedores TABLE (ref varchar(18),armazem numeric(5),qtt numeric(9));
	insert into	@cteReservasFornecedores
	select bi.ref,bi.armazem,qtt = bi.qtt-bi.qtt2 from ts inner join bi on bi.ndos = ts.ndos inner join bo on bo.bostamp=bi.bostamp where ts.resfor = 1 and bi.fechada = 0 and bo.fechada=0


	delete from sa where sa.armazem in (select armazem from #cteArmazens)
	insert into sa ([sastamp], [stock], [ref], [armazem], [rescli], [resfor], [qttrec], [qttacin], [rescat], [ousrinis], [ousrdata], [ousrhora], [usrinis], [usrdata], [usrhora])
	Select 
		sastamp = LEFT(NEWID(),25)
		,stock = isnull((select SUM(case when cm<50 then qtt else -qtt end) from sl where ref = st.ref and armazem = cteArmazens.armazem),0)
		,ref
		,cteArmazens.armazem
		,rescli = isnull((select SUM(qtt) from @cteReservasClientes cteReservasClientes where ref = st.ref and cteArmazens.armazem = cteReservasClientes.armazem),0)
		,resfor = isnull((select SUM(qtt) from @cteReservasFornecedores cteReservasFornecedores where ref = st.ref and cteArmazens.armazem = cteReservasFornecedores.armazem),0)
		,qttrec = 0
		,qttacin = 0
		,rescat = 0
		,ousrinis = 'ADM'
		,ousrdata = convert(varchar,getdate(),102)
		,ousrhora = convert(varchar,getdate(),108)
		,usrinis = 'ADM'
		,usrdata = convert(varchar,getdate(),102)
		,usrhora = convert(varchar,getdate(),108)	
	from 
		st, #cteArmazens cteArmazens
	where
		st.site_nr = @site_nr
	order by 
		ref,armazem
		
	/**/	
	update st set stock = isnull((select SUM(case when cm <50 then qtt else -qtt end) from sl where sl.ref = st.ref and sl.armazem in (select armazem from #cteArmazens)),0) from st	where st.site_nr = @site_nr
	update st set qttcli= isnull((select SUM(rescli) from sa where sa.ref=st.ref and sa.armazem in (select armazem from #cteArmazens)),0), qttfor= isnull((select SUM(resfor) from sa where sa.ref=st.ref and sa.armazem in (select armazem from #cteArmazens)),0) where st.site_nr = @site_nr
	update st set cativado=	isnull((select sum(bi.qtt-bi.qtt2) from bi (nolock)
										INNER JOIN ts (nolock) ON bi.ndos = ts.ndos
										inner join bo (nolock) on bo.bostamp=bi.bostamp
										where ts.cativast=1 and bi.fechada=0 and bo.fechada=0 and bi.armazem=st.site_nr and bi.ref=st.ref),0)
		from st (nolock) where  site_nr=@site_nr
	update sa set cativado=	isnull((select sum(bi.qtt-bi.qtt2) from bi (nolock)
							INNER JOIN ts (nolock) ON bi.ndos = ts.ndos
							inner join bo (nolock) on bo.bostamp=bi.bostamp
							where ts.cativast=1 and bi.fechada=0 and bo.fechada=0 and bi.armazem=sa.armazem and bi.ref=sa.ref),0)
			from sa (nolock) where armazem in (select armazem from #cteArmazens)
	
	if(@result=1)
		select result = 1

	/**/
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteArmazens'))
		DROP TABLE #cteArmazens
GO
Grant Execute on dbo.up_stocks_CorrigeSA to Public
Grant Control on dbo.up_stocks_CorrigeSA to Public
go
