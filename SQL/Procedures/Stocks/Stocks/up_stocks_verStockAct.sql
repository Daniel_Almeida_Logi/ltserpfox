-- Ver o Stock actual de um produto
-- exec up_stocks_verStockAct '8168617',1
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_verStockAct]') IS NOT NULL
	drop procedure dbo.up_stocks_verStockAct
go

create procedure dbo.up_stocks_verStockAct

@ref varchar(60)
,@site_nr tinyint

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	select 
		stock 
	from 
		st (nolock) 
	where 
		st.ref = @ref
		and st.site_nr = @site_nr

GO
Grant Execute On dbo.up_stocks_verStockAct to Public
Grant Control On dbo.up_stocks_verStockAct to Public
GO
