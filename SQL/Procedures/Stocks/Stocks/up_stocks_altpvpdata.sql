/*	Sp alteração de PVP Produto data indicada

	exec up_stocks_altpvpdata 'Loja 1', 1
*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_altpvpdata]') IS NOT NULL
	drop procedure dbo.up_stocks_altpvpdata
Go

create procedure dbo.up_stocks_altpvpdata
	 @site		as varchar(55)
	,@site_nr	tinyint

	
/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST'))
		DROP TABLE #dadosAltPVP

	/* Criar Tabela Temp com a inf dos produtos a atualizar */
	select 
		b_historicopvp.ref
		,dpvp
		,dMc
		,dDc
		,dMb
		,dBb
		,b_historicopvp.local
		,data
		,dataaltpvp
		,site_nr
		,st.design
		,st.epv1
		,st.stock
	into
		#dadosAltPVP
	from
		b_historicopvp (nolock)
		inner join st (nolock) on st.ref = b_historicopvp.ref
	where
		dataaltpvp = cast(convert(varchar(10), getdate(), 110) as datetime) 
		and dataaltpvp > cast(convert(varchar(10), data, 110) as datetime)
		and convert(numeric(9,2), st.epv1) !=  convert(numeric(9,2), b_historicopvp.dpvp)
		and b_historicopvp.site = @site
		and st.site_nr = @site_nr

	/* Caso existam registos atualiza Tabela de Produtos apenas para os casos aplicaveis */
	IF (exists (select 1 from #dadosAltPVP))
		update
			st 
		set 
			epv1	 = dpvp
			,marg1	 = dMc
			,marg2   = dDc
			,marg3 	 = dMb
			,marg4 	 = dBb
		from
			st
		inner join	
			#dadosAltPVP on st.ref = #dadosAltPVP.ref 
		where
			#dadosAltPVP.local = 'Conferência de PVPs'
			and st.site_nr = #dadosAltPVP.site_nr

	select * from #dadosAltPVP

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST'))
		DROP TABLE #dadosAltPVP

Go
Grant Execute On dbo.up_stocks_altpvpdata to Public
Grant Control On dbo.up_stocks_altpvpdata to Public
Go