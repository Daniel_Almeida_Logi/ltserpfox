/*
	 exec up_stocks_dadosRef '7484691 ' , '1'

	  exec up_stocks_dadosRef '5440987' , '1'


*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_dadosRef]') IS NOT NULL
	drop procedure dbo.up_stocks_dadosRef
go

create procedure dbo.up_stocks_dadosRef
@ref varchar(28),
@siteno tinyint

/* WITH ENCRYPTION */
AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosArmazens'))
		DROP TABLE #dadosArmazens
	
	SET NOCOUNT ON

	/* Cálculo de MC */
	declare @tipomc bit = (select bool from B_Parameters_site (nolock) where stamp='ADM0000000129' and site=(select site from empresa (nolock) where no=@siteno))

	-- Armazens disponiveis por empresa
	select 
		empresa_no
	into
		#dadosArmazens
	from
		empresa_arm 
	inner join 
		empresa on empresa_arm.empresa_no = empresa.no 
	where
		empresa.ncont = (select ncont from empresa where no = @siteno)

	                           
	SELECT TOP 1 
		st.ststamp
		,st.ref
		,st.design
		,st.familia
		,st.stock
		,st.cativado
		,stockEmpresa = (select sum(stock) from st (nolock) where ref = @ref and site_nr in (select * from #dadosArmazens) )
		,stockGrupo = (select sum(stock) from st (nolock) where ref = @ref)
		,epv1 = st.epv1
		,st.forref
		,st.fornecedor
		,st.usr1
		,st.validade
		,st.usaid
		,st.uintr
		,st.usrqtt
		,st.eoq
		,st.pcult
		,st.pvultimo
		,st.unidade
		,st.ptoenc
		,st.tabiva
		,st.local
		,st.fornec
		,st.fornestab
		,st.qttfor
		,st.qttcli
		,st.qttrec
		,st.udata
		,st.pcusto
		,st.pcpond
		,st.qttacin
		,st.qttacout
		,st.stmax
		,st.stmin
		,st.obs
		,st.codigo
		,st.uni2
		,st.conversao
		,st.ivaincl
		,st.nsujpp
		,st.ecomissao
		,st.imagem
		,st.cpoc
		,st.containv
		,st.contacev
		,st.contareo
		,st.contacoe
		,st.peso
		,st.bloqueado
		,st.fobloq
		,st.mfornec
		,st.mfornec2
		,st.pentrega
		,st.consumo
		,st.baixr
		,st.despimp
		--,st.marg1
		,marg1=case when @tipomc=0 
						then	case when st.epcusto=0 then 0 else round(((round(st.epv1 / CASE WHEN ((isnull(taxasiva.taxa,0) / 100) + 1) = 0 THEN 1 ELSE ((isnull(taxasiva.taxa,0) / 100) + 1) END,2)/st.epcusto)-1)*100,0) end
						else	case when epv1=0 then 0 else round((1-(st.epcusto/round(st.epv1 / CASE WHEN ((isnull(taxasiva.taxa,0) / 100) + 1) = 0 THEN 1 ELSE ((isnull(taxasiva.taxa,0) / 100) + 1) END,2)))*100,0) end
						end  
		,st.marg2
		,st.marg3
		,st.marg4
		,st.diaspto
		,st.diaseoq
		,st.clinica
		,st.pbruto
		,st.volume
		,st.faminome
		,st.qttesp
		,st.epcusto
		,st.epcpond
		,st.epcult
		,st.epvultimo
		,st.iva1incl
		,st.iva2incl
		,st.iva3incl
		,st.iva4incl
		,st.iva5incl
		,st.ivapcincl
		,st.stns
		,st.url
		,st.iecasug
		,st.iecaref
		,st.iecarefnome
		,st.iecaisref
		,st.qlook
		,st.qttcat
		,st.compnovo
		,st.ousrinis
		,st.ousrdata
		,st.ousrhora
		,st.usrinis
		,st.usrdata
		,st.usrhora
		,st.marcada
		,st.contaieo
		,st.inactivo
		,st.sujinv
		,st.u_nota1
		,st.u_impetiq
		,st.u_local
		,st.u_local2
		,st.u_lab
		,st.u_tipoetiq
		,st.u_validavd
		,st.u_aprov
		,st.u_fonte
		,st.u_validarc
		,st.u_servmarc
		,st.u_duracao
		,st.u_secstamp
		,st.u_catstamp
		,st.u_famstamp
		,st.u_depstamp
		,st.u_segstamp
		,st.usalote
		,st.rateamento
		,st.exportado
		,st.u_codint
		,st.mrsimultaneo
		
		--,departamento = isnull(b_famDepartamentos.design ,'')
		--,seccao = isnull(b_famSeccoes.design,'')
		--,categoria = isnull(b_famCategorias.design,'')
		
		,famfamilia =  isnull(b_famFamilias.design,'')
		,iva = (select top 1 taxa  from taxasiva (nolock) where codigo = tabiva)
		,site = isnull(empresa.site,'')
		,arredondaPVP = convert(bit,0)
		,st.site_nr
		,txcobertura = '0'
		,txrotacao = '0'
		,st.refEntidade
		,st.DesignEntidade
		,st.qttminvd
		,st.qttembal
		,st.profundidade
		,st.altura
		,convert(varchar(100),LTRIM(RTRIM(ISNULL(st.designOnline,'')))) as designOnline
		,convert(varchar(4000),LTRIM(RTRIM(ISNULL(st.caractOnline,'')))) as caractOnline
		,convert(varchar(4000),LTRIM(RTRIM(ISNULL(st.apresentOnline,'')))) as apresentOnline
		,st.novidadeOnline
		,st.tempoIndiponibilidade
		,st.dispOnline
		,st.portesGratis
		,st.qttembenc
		--carregar classificacao do hmr - 20200910 JG		
		, ISNULL(A.descr,'') as departamento
		, ISNULL(B.descr,'') as seccao
		, ISNULL(C.descr,'') as categoria
		, ISNULL(D.descr,'') as segmento
		--,st.codmotiseimp
		--,st.motiseimp
		,(CASE 
			WHEN st.codmotiseimp <> ''
				THEN st.codmotiseimp
			WHEN taxasiva.multidataStamp <> ''
				THEN (select code_motive from b_multidata(nolock) where multidatastamp = taxasiva.multidataStamp)
			ELSE
				st.codmotiseimp
		END) as codmotiseimp
		,(CASE 
			WHEN st.motiseimp <> ''
				THEN st.motiseimp
			WHEN taxasiva.multidataStamp <> ''
				THEN (select campo from b_multidata(nolock) where multidatastamp = taxasiva.multidataStamp)
			ELSE
				st.motiseimp
		END) as motiseimp
		,st.precoSobConsulta
		--,CONVERT(varchar, st.usrdata, 111) as dataAlt
		--,st.usrhora as horaAlt
		--,st.usrinis as inisAlt
		,ISNULL((select top 1 CONVERT(varchar, data, 111) from user_logs(nolock) where [table] = 'ST' and regstamp = st.ststamp order by data desc), '') as dataAlt
		,ISNULL((select top 1 LEFT(CONVERT(time, data),8) from user_logs(nolock) where [table] = 'ST' and regstamp = st.ststamp order by data desc), '') as horaAlt
		,ISNULL((select top 1 ousrinis from user_logs(nolock) where [table] = 'ST' and regstamp = st.ststamp order by data desc), '') as inisAlt
		,st.pim
		,st.epv1_final
		,convert(varchar(20), convert(date, st.dataIniPromo,108)) as dataIniPromo
		,convert(varchar(20), convert(date, st.dataFimPromo,108)) as dataFimPromo
		,st.questionario
	FROM 
		st (nolock) 
		--left join b_famDepartamentos (nolock) on st.u_depstamp = b_famDepartamentos.depstamp
		--left join b_famSeccoes (nolock) on st.u_secstamp = b_famSeccoes.secstamp
		--left join b_famCategorias (nolock) on st.u_catstamp = b_famCategorias.catstamp
		
		left join b_famFamilias (nolock) on st.u_famstamp = b_famFamilias.famstamp
		left join empresa(nolock) on empresa.no = st.site_nr
		left join taxasiva (nolock) on taxasiva.codigo=st.tabiva

		/*
		--carregar classificacao do hmr - 20200910 JG
		left join fprod (nolock) K on k.ref = st.ref
		left join grande_mercado_hmr (nolock) A on A.id = K.id_grande_mercado_hmr
		left join mercado_hmr (nolock) B on B.id = K.id_mercado_hmr
		left join categoria_hmr (nolock) C on C.id = K.id_categoria_hmr
		left join segmento_hmr (nolock) D on D.id = K.id_segmento_hmr
		*/

		--carregar classificacao do hmr - 20200910 JG		
		left join grande_mercado_hmr (nolock) A on A.id = st.u_depstamp
		left join mercado_hmr (nolock) B on B.id = st.u_secstamp
		left join categoria_hmr (nolock) C on C.id = st.u_catstamp 
		left join segmento_hmr (nolock) D on D.id = st.u_segstamp
		
	where 
		st.ref = @ref
		and st.site_nr = @siteno

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosArmazens'))
		DROP TABLE #dadosArmazens

GO
Grant Execute On dbo.up_stocks_dadosRef to Public
Grant Control On dbo.up_stocks_dadosRef to Public
GO


