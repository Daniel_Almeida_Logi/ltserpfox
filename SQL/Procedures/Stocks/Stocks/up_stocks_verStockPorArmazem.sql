-- Ver o Stock de um produto em determinado armazem
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_verStockPorArmazem]') IS NOT NULL
	drop procedure dbo.up_stocks_verStockPorArmazem
go

create procedure dbo.up_stocks_verStockPorArmazem

@ref varchar(60),
@armazem numeric(4,0)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	select isnull((select stock from sa (nolock) where ref=@ref and armazem=@armazem),0) as stock

GO
Grant Execute On dbo.up_stocks_verStockPorArmazem to Public
Grant Control On dbo.up_stocks_verStockPorArmazem to Public
GO
