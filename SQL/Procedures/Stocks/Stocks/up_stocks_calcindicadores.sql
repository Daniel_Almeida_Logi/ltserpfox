/* Sp para calculo de indicadores - Formulas ainda t�m de ser revistas Lu�s Leal 01052015
	
	exec up_stocks_calcindicadores '5440987', 'Loja 1', 1
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_calcindicadores]') IS NOT NULL
	drop procedure dbo.up_stocks_calcindicadores
go

create procedure [dbo].[up_stocks_calcindicadores]
	@ref			varchar(254)
	,@site			as varchar(55)
	,@site_nr		tinyint
	
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON
	If @ref = ''
		Begin
			select 
				ref				= ''
				,stMedio		= 0
				,valorCobStock  = 0
				,txRotacao		= 0
				,txCobertura	= 0
		END
	ELSE
		BEGIN
		/* Elimina Tabelas */
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#diasMes'))
			DROP TABLE #diasMes
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#EmpresaArm'))
			DROP TABLE #EmpresaArm
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.##stindicadores'))
			DROP TABLE #stindicadores 
		/* Define Armaz�ns a Consultar */ 
		select 
			armazem 
		into
			#EmpresaArm
		from 
			empresa 
			inner join empresa_arm on empresa.no = empresa_arm.empresa_no 
		where 
			empresa.site = case when @site = '' then empresa.site else @site end
		/* Calc Stock dia */
		Declare @dataactual as datetime, @dataInicio datetime, @nrdias numeric (3)
		set  @dataInicio	= DATEADD(month, -1,getdate())
		set  @dataactual	= getdate()
		set @nrdias			= (SELECT DATEDIFF(day, @dataInicio, @dataactual))
		SELECT getdate() as data, 0 as stock, 0 as qttvendida, 0 as qttcomprada into #diasMes where 1 =0
		declare @stock as numeric(18,2), @qttvendida numeric(18,2), @qttcomprada as numeric(18,2), @stockatual as numeric(18,2)
		set @stock		 = ISNULL((select stock from st where ref = @ref and site_nr = @site_nr),0)
		set @qttvendida  = 0.00
		set @qttcomprada = 0.00
		set  @stockatual = ISNULL((select stock from st where ref = @ref and site_nr = @site_nr),0)
		/*************************/
		WHILE @dataInicio <= @dataactual
		BEGIN
			INSERT INTO #diasMes
			SELECT 
				(@DATAACTUAL)
				,@stock + isnull((Select sum(qtt) from sl (nolock) left join #EmpresaArm on sl.armazem = #EmpresaArm.armazem where ref = @ref and convert(varchar,datalc,112) = convert(varchar,@DATAACTUAL,112)),0)
				,@qttvendida 
				,@qttcomprada
			SET @DATAACTUAL = DATEADD(dd,-1,@DATAACTUAL)
			SET @stock = @stock + cONVERT(NUMERIC(18,2),isnull((Select sum(qtt) from sl (nolock) left join #EmpresaArm on sl.armazem = #EmpresaArm.armazem where ref = @ref and convert(varchar,datalc,112) = convert(varchar,@DATAACTUAL,112)),0))
			SET @qttvendida = cONVERT(NUMERIC(18,2),isnull ((select 
										sum(CASE WHEN TD.tipodoc = 3 THEN -qtt ELSE qtt END) 
									  from
										fi (nolock)
										INNER JOIN FT (nolock) on FI.FTSTAMP = FT.FTSTAMP
										INNER JOIN TD (nolock) on TD.NDOC = FT.NDOC
										LEFT JOIN #EmpresaArm on fi.armazem = #EmpresaArm.armazem
									  where
										ref = @ref 
										and convert(varchar,rdata,112) = convert(varchar,@DATAACTUAL,112)),0))
			SET @qttcomprada = isnull ((select 
											sum(fn.qtt)
										from
											fn (nolock)
											LEFT JOIN #EmpresaArm on fn.armazem = #EmpresaArm.armazem
										where
											ref = @ref 
											and convert(varchar,data,112) = convert(varchar,@DATAACTUAL,112)),0)
		END
		/* Calc Valores para apresentar */
		select 
			ref				= @ref
			,qttvendida		= SUM(qttvendida)
			,qttcomprada	= SUM(qttcomprada)
			,stMedio		= convert(numeric(10,0),ISNULL(sum(stock)/@nrdias,0))
		into
			#stindicadores
		from
			#diasMes
		select ref
				,txRotacao		= convert(numeric(10,2),
									case	when stMedio > 0
											then SUM(qttvendida)*100/stMedio 
											else 0
											end)
				,valorCobStock  = convert(numeric(10,0),
									case	when (sum(qttvendida)/@nrdias) <=0  
											then 0 
											ELSE (@stockatual / (sum(qttvendida)/@nrdias)) 
											END)
				,txCobertura	= convert(numeric(10,2),
									case	when sum(qttcomprada) > 1
											then sum(qttvendida)*100/sum(qttcomprada)
											ELSE 999
											END)
		 from #stindicadores
		 group by ref,stMedio
		/* Elimina Tabelas */
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#diasMes'))
			DROP TABLE #diasMes
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#EmpresaArm'))
			DROP TABLE #EmpresaArm
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.##stindicadores'))
			DROP TABLE #stindicadores
	End 
GO
Grant Execute on dbo.up_stocks_calcindicadores to Public
Grant control on dbo.up_stocks_calcindicadores to Public
GO