/*Ver Factor de Convers�o por fornecedor

	up_stocks_factorConversaoFl '5563770', 389942137, 1

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_factorConversaoFl]') IS NOT NULL
	drop procedure dbo.up_stocks_factorConversaoFl
go

create procedure dbo.up_stocks_factorConversaoFl

@ref		varchar(18),
@flno		numeric(9),
@site_nr	tinyint

/* WITH ENCRYPTION */

AS

	select 
		case when isnull((select conversao from B_fp where ref=@ref and no=@flno),0)=0
				then conversao
			else isnull((select conversao from B_fp where ref=@ref and no=@flno),0)
			end as conversao
	from 
		st (nolock)
	where 
		st.ref = @ref
		and st.site_nr = @site_nr


GO
Grant Execute on dbo.up_stocks_factorConversaoFl to Public
Grant Control on dbo.up_stocks_factorConversaoFl to Public
Go
