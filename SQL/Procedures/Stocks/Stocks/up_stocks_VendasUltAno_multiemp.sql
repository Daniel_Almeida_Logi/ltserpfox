/* ListaEntradas e Saidas do Ultimo ano para uma ref*/
/* 		exec up_stocks_VendasUltAno_multiemp '1,2,3','5440987',1 */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_VendasUltAno_multiemp]') IS NOT NULL
    drop procedure dbo.up_stocks_VendasUltAno_multiemp
go

create procedure dbo.up_stocks_VendasUltAno_multiemp
@armazem varchar(50),
@ref as varchar(18),
@site_nr tinyint



/* WITH ENCRYPTION */
AS

	If OBJECT_ID('tempdb.dbo.#Armazens') IS NOT NULL
		drop table #Armazens 
		
	/* armazens disponíveis */
	select
		distinct armazem
	into
		#Armazens
	From
		empresa
		inner join empresa_arm on empresa.no = empresa_arm.empresa_no
	Where
		empresa.no in  (select items from dbo.up_splitToTable(@armazem,',')) 



	;with 
		cteSL(slstamp,cm,ref,qtt,datalc) as (
			SELECT	
				slstamp,
				sl.cm,
				sl.ref,
				sl.qtt,
				sl.datalc
			FROM	
				sl (nolock)
				inner join st on st.ref = sl.ref and st.site_nr = @site_nr
			where	
				sl.ref = @ref
				and (origem = 'FT' or cmdesc = 'Cons. interno')
				and armazem in (select armazem from #Armazens)
				and YEAR(datalc) between YEAR(GETDATE())-1 and YEAR(GETDATE()
		)



	), cteAno as (
		Select Ano = YEAR(GETDATE())-1, mes = 1, CONVERT(datetime,STR(YEAR(GETDATE())-1) + '0101') as data
		union all 
		Select Ano = YEAR(GETDATE())-1, mes = 2, CONVERT(datetime,STR(YEAR(GETDATE())-1) + '0201')
		union all
		Select Ano = YEAR(GETDATE())-1, mes = 3, CONVERT(datetime,STR(YEAR(GETDATE())-1) + '0301')
		union all
		Select Ano = YEAR(GETDATE())-1, mes = 4, CONVERT(datetime,STR(YEAR(GETDATE())-1) + '0401')
		union all 
		Select Ano = YEAR(GETDATE())-1, mes = 5, CONVERT(datetime,STR(YEAR(GETDATE())-1) + '0501')
		union all
		Select Ano = YEAR(GETDATE())-1, mes = 6, CONVERT(datetime,STR(YEAR(GETDATE())-1) + '0601')
		union all
		Select Ano = YEAR(GETDATE())-1, mes = 7, CONVERT(datetime,STR(YEAR(GETDATE())-1) + '0701')
		union all 
		Select Ano = YEAR(GETDATE())-1, mes = 8, CONVERT(datetime,STR(YEAR(GETDATE())-1) + '0801')
		union all
		Select Ano = YEAR(GETDATE())-1, mes = 9, CONVERT(datetime,STR(YEAR(GETDATE())-1) + '0901')
		union all
		Select Ano = YEAR(GETDATE())-1, mes = 10, CONVERT(datetime,STR(YEAR(GETDATE())-1) + '1001')
		union all
		Select Ano = YEAR(GETDATE())-1, mes = 11, CONVERT(datetime,STR(YEAR(GETDATE())-1) + '1101')
		union all 
		Select Ano = YEAR(GETDATE())-1, mes = 12, CONVERT(datetime,STR(YEAR(GETDATE())-1) + '1201')
		union all		
		Select Ano = YEAR(GETDATE()), mes = 1, CONVERT(datetime,STR(YEAR(GETDATE())) + '0101')
		union all 
		Select Ano = YEAR(GETDATE()), mes = 2, CONVERT(datetime,STR(YEAR(GETDATE())) + '0201')
		union all
		Select Ano = YEAR(GETDATE()), mes = 3, CONVERT(datetime,STR(YEAR(GETDATE())) + '0301')
		union all
		Select Ano = YEAR(GETDATE()), mes = 4, CONVERT(datetime,STR(YEAR(GETDATE())) + '0401')
		union all 
		Select Ano = YEAR(GETDATE()), mes = 5, CONVERT(datetime,STR(YEAR(GETDATE())) + '0501')
		union all
		Select Ano = YEAR(GETDATE()), mes = 6, CONVERT(datetime,STR(YEAR(GETDATE())) + '0601')
		union all
		Select Ano = YEAR(GETDATE()), mes = 7, CONVERT(datetime,STR(YEAR(GETDATE())) + '0701')
		union all 
		Select Ano = YEAR(GETDATE()), mes = 8, CONVERT(datetime,STR(YEAR(GETDATE())) + '0801')
		union all
		Select Ano = YEAR(GETDATE()), mes = 9, CONVERT(datetime,STR(YEAR(GETDATE())) + '0901')
		union all
		Select Ano = YEAR(GETDATE()), mes = 10, CONVERT(datetime,STR(YEAR(GETDATE())) + '1001')
		union all
		Select Ano = YEAR(GETDATE()), mes = 11, CONVERT(datetime,STR(YEAR(GETDATE())) + '1101')
		union all
		Select Ano = YEAR(GETDATE()), mes = 12, CONVERT(datetime,STR(YEAR(GETDATE())) + '1201')
		
	), cteMovimentosAgrupados as (
		Select 
			ref
			,ano = year(datalc)
			,mes = month(datalc)
			,entradas = sum(isnull(case when cm < 50 then qtt else 0 end,0))
			,saidas = sum(isnull(case when cm > 50 then qtt else 0 end,0))
		from 
			cteSL
		Where 
			ref = @ref
		Group by 
			year(datalc)
			,month(datalc)
			,ref
	
	)
	
	Select 
		cteAno.ano
		,cteAno.mes
		,entradas = isnull(cteMovimentosAgrupados.entradas,0)
		,saidas = isnull(cteMovimentosAgrupados.saidas,0)
	From 
		cteAno
		left join cteMovimentosAgrupados on cteAno.Ano = cteMovimentosAgrupados.ano and cteAno.mes = cteMovimentosAgrupados.mes
	Where
		cteAno.data between dateadd(MONTH,-1,dateadd(year,-1,GETDATE())) and GETDATE()
	Order by
			cteAno.ano, cteAno.mes, ref

     If OBJECT_ID('tempdb.dbo.#Armazens') IS NOT NULL
		drop table #Armazens 
	
GO
Grant Execute on dbo.up_stocks_VendasUltAno_multiemp to Public
Grant Control on dbo.up_stocks_VendasUltAno_multiemp to Public
go