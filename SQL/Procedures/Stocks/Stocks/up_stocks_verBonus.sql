/* Listar bonus de fornecedor do produto 

	exec up_stocks_verBonus 1, '6046532'
	select * from ext_esb_orders_campaigns_d

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_verBonus]') IS NOT NULL
	drop procedure dbo.up_stocks_verBonus
go

create procedure dbo.up_stocks_verBonus
@armazem numeric(5,0),
@ref varchar(60)

/* WITH ENCRYPTION */
AS

	select 
		distinct bi.ref
		,st.design
		--,bi.u_bonus as Bonus
		,cast(bi.lobs as varchar(200)) as Bonus
		--,bo.nome as Fornecedor
		,case when fl.nome2='' then fl.nome else fl.nome2 end as Fornecedor
		,bo.no as NrFornecedor
		,st.ststamp
		,convert(varchar,bo.datafinal,102) as validade
		,cast('' as varchar(250)) as infoconsulta
		,cast('' as varchar(250)) as disp
		,cast(0.00 as numeric(15,2)) as pct
		,cast(0.00 as numeric(15,2)) as dc
		,cast(0.00 as numeric(15,2)) as pcl
		,cast('' as varchar(50)) as dtentrega
		,cast('' as varchar(250)) as productOffered
		,cast(0 as numeric(10,0)) as qttmin
		,cast(0.00 as numeric(10,0)) as qttmax
		,convert(varchar,'19000101',102) as beginDate
		,convert(varchar,bo.datafinal,102) as endDate
		,cast(0 as numeric(10,0)) as qttmult
		,cast(0 as numeric(10,0)) as qttbonus
		,st.epv1 as pvp
		,cast('' as varchar(200)) as conditions
		,'MANUAL' as origem
		,obs = dbo.alltrimIsNull(st.obs)
		,u_nota1 = dbo.alltrimIsNull(st.u_nota1) 
	from
		bi (nolock)
		inner join bo (nolock) on bi.bostamp=bo.bostamp
		left join bo2 (nolock) on bo2.bo2stamp=bo.bostamp
		inner join st (nolock) on bi.ref=st.ref  and st.site_nr = (case when @armazem <> 0 then @armazem else (select no from empresa(nolock) where site = bo.site) end)
		inner join fl (nolock) on fl.no=bo.no and fl.estab=bo.estab
	where 
		bo.ndos=35 
		and bi.ref=@ref 
		and bo.datafinal >= convert(date,getdate())
		--and bi.armazem = case when @armazem=0 then bi.armazem else @armazem end -- corrigido em 08-08-2018 estava mas nas linhas dos dossiers - armazém <> nr loja na bo
		and bo.site= case when @armazem=0 then bo.site else (select site from empresa where no=@armazem) end
		and isnull(bo2.codext,'')=''
	--	and bo2.status=''

	union all

		select 
			
			bonus_d.ref,
			st.design
			--,(case when packagingBonus>0 then (case when multipleAcquisition=0 then 'Emb. Bonus' else 'Emb. Bonus+Mult. Aquis.' end) else (case when multipleAcquisition=0 then '' else 'Mult. Aquis.' end) end) as bonus
			,cast(descrOffer as varchar(200)) as bonus
			--,bo.nome
			,case when fl.nome2='' then fl.nome else fl.nome2 end as Fornecedor
			,bonus.no_fl as NrFornecedor
			,st.ststamp
			,convert(varchar,bonus.endDate,102) as validade
			,cast('' as varchar(250)) as infoconsulta
			,cast('' as varchar(250)) as disp
			,cast(0.00 as numeric(15,2)) as pct
			,cast(discount as numeric(15,2)) as dc
			,cast(pvu as numeric(15,2)) as pcl
			,cast('' as varchar(50)) as dtentrega
			, productOffered
			,cast(qttmin as numeric(10,0)) as qttmin
			,cast(qttmax as numeric(10,0)) as qttmax
			--, descrOffer
			,convert(varchar,bonus.beginDate,102) as  beginDate
			,convert(varchar,bonus.endDate,102) as endDate
			,cast(multipleAcquisition as numeric(10,0)) as qttmult
			,cast(packagingBonus as numeric(10,0)) as qttbonus
			,cast(0 as numeric(10,0)) AS PVP
			,cast('' as varchar(200)) as conditions
			,'B2B' as origem
			,obs = dbo.alltrimIsNull(st.obs)
			,u_nota1 = dbo.alltrimIsNull(st.u_nota1) 
		from bonus (nolock)
		inner join bonus_d (nolock) on bonus.stamp=bonus_d.bonus_stamp
		inner join empresa (nolock) on bonus.site=empresa.site
		inner join empresa_arm (nolock) on empresa_arm.armazem=@armazem and empresa_arm.empresa_no = empresa.no
		inner join st (nolock) on bonus_d.ref=st.ref and empresa.no=st.site_nr
		inner join fl (nolock) on fl.no=bonus.no_fl and fl.estab=bonus.estab_fl
		where 
			 bonus_d.ref=@ref 
			and (getdate() between bonus.beginDate and (case when year(bonus.endDate)!='9999' then DATEADD(dd, +1, bonus.endDate) else bonus.endDate  end) or bonus.endDate='19000101')
			and bonus.ativo=1

			
GO
Grant Execute On dbo.up_stocks_verBonus to Public
Grant Control On dbo.up_stocks_verBonus to Public
GO