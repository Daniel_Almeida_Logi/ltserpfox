/*
	Confer�ncia da PVPs
	
	exec up_stocks_conferenciaPvpsLote 1, 0, '', '', '',99999,'','Loja 1', '>', '0'
	
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_conferenciaPvpsLote]') IS NOT NULL
	drop procedure dbo.up_stocks_conferenciaPvpsLote
go

create procedure dbo.up_stocks_conferenciaPvpsLote
	 @armazem			numeric(5,0)
	,@fac				bit
	,@ref				varchar(60)
	,@familia			varchar(60)
	,@fostamp			varchar(30)
	,@top				int
	,@lote				varchar(60)
	,@site				varchar(20)
	,@operadorLogico	VARCHAR(5)
	,@stock				NUMERIC(13,3)


/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST'))
		DROP TABLE #tmp_mrgRegressivas
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST'))
		DROP TABLE #dadosST
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST'))
		DROP TABLE #dadosST1
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST'))
		DROP TABLE #dadosST1
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST'))
		DROP TABLE #cteSaidas
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST'))
		DROP TABLE #cteSaidasRef
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST'))
		DROP TABLE #cteSaidasGH
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST'))
		DROP TABLE #cteGH

	
	-- definir site_nr
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)
	
	--
	Select	
		versao,escalao,PVAmin,PVAmax,PVPmin,PVPmax,
		mrgGross,mrgGrossTipo,mrgFarm,FeeFarm,feeFarmCiva,
		txCom,factorPond
	into
		#tmp_mrgRegressivas
	from
		b_mrgRegressivas (nolock)


if @fac = 0
begin
		--
		SELECT	
			sl.ref,
			qtt,
			grphmgcode
		into
			#cteSaidas
		FROM	
			sl (nolock)
			inner join fprod (nolock) on sl.ref = fprod.cnp
		where	
			cm > 50
			and year(sl.datalc) = YEAR(DATEADD(MONTH,-1,getdate()))
			AND month(sl.datalc) = MONTH(DATEADD(MONTH,-1,getdate()))
		
		--
		Select	
			ref,
			qtt = sum(qtt)
		into
			#cteSaidasRef
		FROM	
			#cteSaidas
		Group by
			ref

		--
		Select	
			grphmgcode,
			qtt = sum(qtt)
		into
			#cteSaidasGH
		FROM	
			#cteSaidas
		Group by
			grphmgcode

		--
		Select 
			stock = sum(stock)
			,grphmgcode
		into
			#cteGH
		From
			st (nolock)
			inner join fprod (nolock) on st.ref = fprod.cnp
		where
			site_nr = @site_nr
		Group by
			grphmgcode
		
	select
		distinct 
		st.ref
		,design		= st.design
		,stock		= isnull(st_lotes.stock,isnull(sa.stock,st.stock))
		,epcusto	= isnull(st_lotes_precos.pct,st.epcusto)
		,st.epcpond
		,st.ivaincl
		,iva		= isnull(taxasiva.taxa,0)
		,pvp		= isnull(st_lotes_precos.epv1,st.epv1)
		,epcult		= st.epcult
		,pvpf		= isnull(st_lotes_precos.epv1,st.epv1)
		,mrg		= st.marg1
		,mrg2		= st.marg2
		,mrg3		= st.marg3
		,mrg4		= st.marg4
		,mrgf		= st.marg1
		,pcl		= st.epcult
		,calc		= CONVERT(bit,0)
		,grupo		= isnull(fprod.grupo, '')
		,pvdic		= isnull(fprod.pvporig, 0.00) / CASE WHEN ((isnull(taxasiva.taxa,0) / 100) + 1) = 0 THEN 1 ELSE ((isnull(taxasiva.taxa,0) / 100) + 1) END
		,pvpdic		= isnull(fprod.pvporig, 0.00)
		,pvpref 	= isnull(fprod.pref, 0.00)
		,mrgbruta	= st.marg3
		,mrgbrutaf	= CONVERT(numeric(6,2), 0)
		,desccom	= st.marg2
		,desccomf	= CONVERT(numeric(6,2), 0)
		,benbruto	= st.marg4
		,benbrutof	= st.marg4
		,st.ststamp
		,st.qlook
		,pvpmax		= convert(numeric(13,3), isnull(fprod.pvpmax, 0))
		,pvporig	= convert(numeric(13,3), isnull(fprod.pvporig, 0))
		,[local]	= case when st.u_local = '' then st.local else st.local + ',' + st.u_local end
		,escalao	= isnull((Select top 1 escalao from #tmp_mrgRegressivas Where pvporig between PVPmin and PVPmax order by versao desc),0)
		,mrgFarm	= isnull((Select top 1 mrgFarm from #tmp_mrgRegressivas Where pvporig between PVPmin and PVPmax order by versao desc),0)
		,feeFarm	= isnull((Select top 1 feeFarm from #tmp_mrgRegressivas Where pvporig between PVPmin and PVPmax order by versao desc),0)
		,pvpmaxre	= convert(numeric(13,3), isnull(fprod.pvpmaxre, 0))
		,pv			= st.epv1 / CASE WHEN ((isnull(taxasiva.taxa,0) / 100) + 1) = 0 THEN 1 ELSE ((isnull(taxasiva.taxa,0) / 100) + 1) END
		,mbspvp		= (1-(st.epcpond/
		CASE WHEN (st.epv1 / ((isnull(taxasiva.taxa,0) / 100) + 1)) = 0
		THEN 1 ELSE
		(st.epv1 / ((isnull(taxasiva.taxa,0) / 100) + 1))
		END
		))*100
		,fprod.grphmgcode
		,stockGH			= isnull(#cteGH.stock,0)
		,qttcli		= isnull(sa.rescli,st.qttcli)
		,qttfor		= isnull(sa.resfor,st.qttfor)
		,qttVendUltMes = isnull(#cteSaidasRef.qtt,0)
		,GHqttVendUltMes = isnull(#cteSaidasGH.qtt,0)
		,stmax
		,st.ptoenc
		,lote		= isnull(st_lotes.lote,'')
		,PVPLote	= isnull(st_lotes_precos.epv1,0)
		,AltPVPLote = isnull(convert(varchar,st_lotes_precos.usrdata,102),'1900.01.01')
		,HoraAltPVPLote = isnull(st_lotes_precos.usrhora,'')
		,atualizaPVPFicha = CONVERT(bit,0)
		,cnpem		= isnull(fprod.cnpem,'')
		,empresa.site
	into 
		#dadosST 
	from
		st (nolock)
		inner join empresa on empresa.no = st.site_nr
		left join sa (nolock) on sa.ref = st.ref and sa.armazem = @armazem
		left join fprod (nolock) on fprod.cnp=st.ref
		left join taxasiva (nolock) on taxasiva.codigo=st.tabiva
		left join #cteSaidasRef on #cteSaidasRef.ref = st.ref
		left join #cteSaidasGH on #cteSaidasGH.grphmgcode = fprod.grphmgcode
		left join #cteGH on #cteGH.grphmgcode = fprod.grphmgcode
		left join st_lotes (nolock) on st.ref = st_lotes.ref
		left join st_lotes_precos (nolock) on st_lotes_precos.stampLote = st_lotes.stamp
	where 
		(st.ref=@ref or st.design like '%'+@ref+'%')
		and site_nr = @site_nr
		and st.faminome = case when @familia='' then st.faminome else @familia end
		and isnull(st_lotes.lote,'') = case when @lote = '' then isnull(st_lotes.lote,'') else @lote end	

	if @stock != -999999
	begin
	
	if @operadorLogico = '>'
		delete from #dadosST where stock <= @stock
	if @operadorLogico = '<'
		delete from #dadosST where stock >= @stock
	if @operadorLogico = '='
		delete from #dadosST where stock != @stock
	end

	Select 
		top (@top)
		sel = CONVERT(BIT,0)
		,* 
	from 
		#dadosST st	
	order by
		st.design, isnull(lote,'') ,isnull(convert(varchar,AltPVPLote,102),'1900.01.01') desc, isnull(HoraAltPVPLote,'') desc

end
else
begin

	select distinct 
		st.ref
		,design			= st.design
		,stock			= isnull(st_lotes.stock,isnull(sa.stock,st.stock))
		,epcusto		= isnull(st_lotes_precos.pct,st.epcusto)
		,st.epcpond
		,st.ivaincl
		,iva			= isnull(taxasiva.taxa,0)
		,pvp			= isnull(st_lotes_precos.epv1,st.epv1)
		,epcult			= st.epcult
		,pvpf			= CASE WHEN fn.u_pvp = 0 THEN isnull(st_lotes_precos.epv1,st.epv1) ELSE fn.u_pvp END
		,mrg			= st.marg1
		,mrg2			= st.marg2
		,mrg3			= st.marg3
		,mrg4			= st.marg4
		,mrgf			= st.marg1
		,pcl			= st.epcult
		,calc			= CONVERT(bit,0)
		,grupo			= isnull(fprod.grupo, '')
		,pvdic			= isnull(fprod.pvporig, 0.00) / CASE WHEN ((isnull(taxasiva.taxa,0) / 100) + 1) = 0 THEN 1 ELSE ((isnull(taxasiva.taxa,0) / 100) + 1) END
		,pvpdic			= isnull(fprod.pvporig, 0.00)
		,pvpref 		= isnull(fprod.pref, 0.00)
		,mrgbruta		= st.marg3
		,mrgbrutaf		= CONVERT(numeric(6,2), 0)
		,desccom		= st.marg2
		,desccomf		= CONVERT(numeric(6,2), 0)
		,benbruto		= st.marg4
		,benbrutof		= st.marg4
		,st.ststamp
		,st.qlook
		,pvpmax			= convert(numeric(13,3), isnull(fprod.pvpmax, 0))
		,pvporig		= convert(numeric(13,3), isnull(fprod.pvporig, 0))
		,[local]		= case when st.u_local = '' then st.local else st.local + ',' + st.u_local end
		,escalao		= isnull((Select top 1 escalao from #tmp_mrgRegressivas Where pvporig between PVPmin and PVPmax order by versao desc),0)
		,mrgFarm		= isnull((Select top 1 mrgFarm from #tmp_mrgRegressivas Where pvporig between PVPmin and PVPmax order by versao desc),0)
		,feeFarm		= isnull((Select top 1 feeFarm from #tmp_mrgRegressivas Where pvporig between PVPmin and PVPmax order by versao desc),0)
		,pvpmaxre		= convert(numeric(13,3), isnull(fprod.pvpmaxre, 0))
		,pv	= st.epv1 / CASE WHEN ((isnull(taxasiva.taxa,0) / 100) + 1) = 0 THEN 1 ELSE ((isnull(taxasiva.taxa,0) / 100) + 1) END
		,mbspvp	= (1-(st.epcpond/
		CASE WHEN (st.epv1 / ((isnull(taxasiva.taxa,0) / 100) + 1)) = 0
		THEN 1 ELSE
		(st.epv1 / ((isnull(taxasiva.taxa,0) / 100) + 1))
		END
		))*100
		,fprod.grphmgcode
		,stockgh = 0 /*n�o usado com este parametro */
		,qttcli = isnull(sa.rescli,st.qttcli)
		,qttfor = isnull(sa.resfor,st.qttfor)
		,qttVendUltMes = 0 /*n�o usado com este parametro */
		,GHqttVendUltMes = 0 /*n�o usado com este parametro */
		,stmax
		,st.ptoenc
		,lote = isnull(st_lotes.lote,'')
		,PVPLote = isnull(st_lotes_precos.epv1,0)
		,AltPVPLote = isnull(convert(varchar,st_lotes_precos.usrdata,102),'1900.01.01')
		,HoraAltPVPLote = isnull(st_lotes_precos.usrhora,'')
		,atualizaPVPFicha = CONVERT(bit,0)
		,cnpem = isnull(fprod.cnpem,'')
		,empresa.site
	into 
		#dadosST1
	from
		st (nolock)
		inner join empresa on empresa.no = st.site_nr
		left join sa (nolock) on sa.ref = st.ref and sa.armazem = @armazem
		inner join fn (nolock) on fn.ref=st.ref or fn.oref=st.ref
		inner join fo (nolock) on fo.fostamp=fn.fostamp
		left join fprod (nolock) on fprod.cnp=st.ref
		left join taxasiva (nolock) on taxasiva.codigo=st.tabiva
		left join st_lotes (nolock) on st.ref = st_lotes.ref
		left join st_lotes_precos (nolock) on st_lotes_precos.stampLote = st_lotes.stamp
	where
		(st.ref=@ref or st.design like '%'+@ref+'%')
		and site_nr = @site_nr
		and fo.fostamp=@fostamp and fn.qtt>0
		and st.faminome = case when @familia='' then st.faminome else @familia end
		and isnull(st_lotes.lote,'') = case when @lote = '' then isnull(st_lotes.lote,'') else @lote end

	if @stock != -999999
	begin
		if @operadorLogico = '>'
			delete from #dadosST1 where stock <= @stock
		if @operadorLogico = '<'
			delete from #dadosST1 where stock >= @stock
		if @operadorLogico = '='
			delete from #dadosST1 where stock != @stock
	end

	Select 
		top (@top)
		sel = CONVERT(BIT,0)
		,* 
	from 
		#dadosST1 st	
	order by
		st.design, isnull(lote,''),isnull(convert(varchar,AltPVPLote,102),'1900.01.01') desc,isnull(HoraAltPVPLote,'') desc

end

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST'))
	DROP TABLE #tmp_mrgRegressivas
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST'))
	DROP TABLE #dadosST
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST'))
	DROP TABLE #dadosST1
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST'))
	DROP TABLE #dadosST1
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST'))
	DROP TABLE #cteSaidas
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST'))
	DROP TABLE #cteSaidasRef
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST'))
	DROP TABLE #cteSaidasGH
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST'))
	DROP TABLE #cteGH

GO
Grant Execute on dbo.up_stocks_conferenciaPvpsLote to Public
Grant Control on dbo.up_stocks_conferenciaPvpsLote to Public
go	
