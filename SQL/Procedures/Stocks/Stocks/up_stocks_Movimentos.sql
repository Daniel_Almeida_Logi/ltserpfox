/* Movimentos de Stocks 

	 exec up_stocks_Movimentos '5746342',1, '20180815', '20181115',0,'',''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


IF OBJECT_ID('[dbo].[up_stocks_Movimentos]') IS NOT NULL
	drop procedure dbo.up_stocks_Movimentos
go

CREATE procedure dbo.up_stocks_Movimentos

@ref varchar (18),
@armazem numeric(5,0),
@dataIni datetime,
@dataFim datetime,
@modo bit,
@lote varchar(30),
@movimento varchar(15)='Todos'

/* WITH ENCRYPTION */
AS

	If OBJECT_ID('tempdb.dbo.#SaldoAnterior') IS NOT NULL
		drop table #SaldoAnterior
	If OBJECT_ID('tempdb.dbo.#TempMovimentos') IS NOT NULL
		drop table #TempMovimentos
		
		
	select 
		saldoAnterior = SUM(Case When cm <50 Then qtt Else - qtt End)
		,entradasAnterior = sum(case 
									when cm < 50 then
										case when qtt < 0 then 0 else round(qtt,0) end
										else case when qtt < 0 and cm > 50 then	qtt * -1 else round(0,0) end
								end
							)
		,saidasAnterior = sum(case 
									when cm > 50 and qtt > 0 
										then round(qtt,0) 
										else case 
												when cm < 50 and qtt < 0
													then ROUND(qtt*-1,0)
													else round(0,0) 
											end
										end
							)
	into 
		#SaldoAnterior	
	from 
		sl (nolock) 
	WHere 
		sl.datalc <  @dataIni 
		and sl.ref = @ref 
		and sl.armazem = @armazem  
		and sl.lote like case when @lote = '' then sl.lote else @lote + '%' end	
		--and sl.cm between 
		--	(case when @movimento like '%Todos%' then 1 else (case when @movimento like '%Entradas%' then 1 else (case when @movimento like '%Sa�das%' then 51 else 0 end ) end) end) 
		--	and 
		--	(case when @movimento like '%Todos%' then 99 else (case when @movimento like '%Entradas%' then 50 else (case when @movimento like '%Sa�das%' then 99 else 0 end ) end) end)
		

	SELECT
		sl.cm
		,sl.slstamp
		,ROW_NUMBER() over(order by  sl.datalc asc, sl.ousrhora asc) as numero
		,qtt
	Into
		#TempMovimentos
	FROM
		sl (nolock)
	WHERE
		sl.ref=@ref
		and sl.datalc between  @dataIni and @dataFim
		and sl.armazem = @armazem
		and sl.lote like case when @lote = '' then sl.lote else @lote + '%' end
		--and sl.cm between 
		--	(case when @movimento like '%Todos%' then 1 else (case when @movimento like '%Entradas%' then 1 else (case when @movimento like '%Sa�das%' then 51 else 0 end ) end) end) 
		--	and 
		--	(case when @movimento like '%Todos%' then 99 else (case when @movimento like '%Entradas%' then 50 else (case when @movimento like '%Sa�das%' then 99 else 0 end ) end) end)
		
	

	SELECT 
		SALDOANTERIOR		= (select SALDOANTERIOR from #SaldoAnterior)
		,ENTRADASANTERIOR	= (select ENTRADASANTERIOR from #SaldoAnterior)
		,SAIDASANTERIOR		= (select SAIDASANTERIOR from #SaldoAnterior)
		,SALDO				= 
							Case when @modo=1
								then
									isnull((
												Select sum(case when a.cm < 50 then a.qtt else - a.qtt end) 
												From   (select * from #TempMovimentos)a
														inner join sl (nolock) on sl.slstamp = a.slstamp
														Where a.numero <= b.numero
										   ),0)
								else
									0 
							End
		,ENTRADAS			= 
							case when b.cm < 50 
								then 
									case when b.qtt < 0
										then
											0
										else
											round(b.qtt,0) 
										end
								else 
									case when b.qtt < 0 and b.cm > 50
										then
											b.qtt * -1
										else
											round(0,0) 
										end
								end
		,SAIDAS				= 
							case when b.cm > 50 and b.qtt > 0
								then 
									round(b.qtt,0) 
								else 
									case when b.cm < 50 and b.qtt < 0
										then
											ROUND(b.qtt*-1,0)
										else
											round(0,0) 
										end
								end
		,*
	into #dadossl
	From (	
		select
			ROW_NUMBER() over(order by  sl.datalc asc, sl.ousrhora asc) as numero
			,qtt 
			,ref
			,design 
			,slstamp
			,convert(varchar,datalc,102) as datalc
			,cm
			,cmdesc as cmdesc
			,adoc 
			,nome + (case when bistamp='' then '' else (' - '+ (select codext from bo2 (nolock) where bo2.bo2stamp=(select bostamp from bi (nolock) where bi.bistamp=sl.bistamp))) end) as nome
			,sl.armazem
			--,evu as PCL
			,pcl	= case when sl.fistamp<>'' then	
							(select ecusto from fi (nolock) where fi.fistamp=sl.fistamp)
						else
							case when sl.fnstamp<>'' then
								(select u_upc from fn (nolock) where fn.fnstamp=sl.fnstamp)
							else
								case when sl.bistamp<>'' then
									(select u_upc from bi (nolock) where bi.bistamp=sl.bistamp)
								else
									sl.evu
								end
							end 
						end 
			--,ett as PCLTOTAL
			,pcltotal= (case when sl.fistamp<>'' then	
							(select ecusto from fi (nolock) where fi.fistamp=sl.fistamp)
						else
							case when sl.fnstamp<>'' then
								(select u_upc from fn (nolock) where fn.fnstamp=sl.fnstamp)
							else
								case when sl.bistamp<>'' then
									(select u_upc from bi (nolock) where bi.bistamp=sl.bistamp)
								else
									sl.ett
								end 
							end 
						end )*qtt
			,epcpond as PCP
			,(epcpond*qtt) as PCPTOTAL
			,ousrinis
			,sl.ousrhora
			,sl.ousrdata
			,CONVERT(bit,0) as verdoc
			,origem 
			,fistamp
			,bistamp
			,fnstamp
			,sticstamp
			,(select top 1 username from b_us where sl.ousrinis=b_us.iniciais) as username
			,lote
			,(CASE 
				WHEN origem = 'FT' 
					THEN (SELECT u_epvp FROM fi(nolock) WHERE fistamp = sl.fistamp) 
				ELSE 0 
			END) as pvpOri
		from
			sl (nolock) 
		where
			sl.ref=@ref 
			and sl.datalc between @dataIni and @dataFim
			and sl.armazem = @armazem
			and sl.lote like case when @lote = '' then sl.lote else @lote + '%' end
			--and sl.cm between 
			--	(case when @movimento like '%Todos%' then 1 else (case when @movimento like '%Entradas%' then 1 else (case when @movimento like '%Sa�das%' then 51 else 0 end ) end) end) 
			--	and 
			--	(case when @movimento like '%Todos%' then 99 else (case when @movimento like '%Entradas%' then 50 else (case when @movimento like '%Sa�das%' then 99 else 0 end ) end) end)
	) b
	order by
		datalc asc
		,ousrhora asc


	/* Elimina entradas se tipo de movimentos=entradas*/
	if @movimento like '%Entradas%' 
	delete from #dadossl where saidas>0

	/* Elimina sa�das se tipo de movimentos=sa�das*/
	if @movimento like '%Sa�das%' 
	delete from #dadossl where entradas>0

	select * from #dadossl order by datalc asc ,ousrhora asc
GO
Grant Execute on dbo.up_stocks_Movimentos to Public
Grant Control on dbo.up_stocks_Movimentos to Public
go
