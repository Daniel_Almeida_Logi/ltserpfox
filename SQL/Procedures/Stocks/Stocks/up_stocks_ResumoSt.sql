/*
	exec up_stocks_ResumoSt
		 @armazem = 0
		,@fac = 0
		,@ref = '8168617'
		,@familia = ''
		,@fostamp = null
		,@top = 999
		,@siteno = 1

	exec up_stocks_ResumoSt 1, 0,'5440987', '', '', 1, 1
*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_ResumoSt]') IS NOT NULL
	DROP PROCEDURE dbo.up_stocks_ResumoSt
GO

CREATE PROCEDURE dbo.up_stocks_ResumoSt 
@armazem numeric(5,0),
@fac bit,
@ref varchar(60),
@familia varchar(60),
@fostamp varchar(30),
@top int,
@site_nr tinyint

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosMrgRegressivas'))
		DROP TABLE #dadosMrgRegressivas
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSaidas'))
		DROP TABLE #dadosSaidas
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSaidas'))
		DROP TABLE #dadosSaidas
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSaidasRef'))
		DROP TABLE #dadosSaidasRef
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSaidasRef'))
		DROP TABLE #dadosSaidasRef	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSaidasGH'))
		DROP TABLE #dadosSaidasGH	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosGH'))
		DROP TABLE #dadosGH		
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosDesign'))
		DROP TABLE #dadosDesign
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosStSite'))
		DROP TABLE #dadosStSite

	/* Periodo de Escomaento */
	declare @escoamento_farma bit = 
			case when ISNULL((select max(data) from hist_precos (nolock) where hist_precos.ref= @ref and id_preco=1 and data <= convert(date,GETDATE())),'1900-01-01') > (GETDATE()-120) then convert(bit,1) else convert(bit,0) end

	/* Cálculo de MC */
	declare @tipomc bit = (select bool from B_Parameters_site (nolock) where stamp='ADM0000000129' and site=(select site from empresa (nolock) where no=@site_nr))

	
	/* Dados Ref*/
	select 
		ref	 
		,stockEmp = sum(stock)  
	into 
		#dadosDesign
	from 
		st (nolock)
	where 
		st.ref = @ref 
	group by 
		st.ref

	select 
		ref	 
		,stockArm = sum(stock)  
	into 
		#dadosStSite
	from 
		st (nolock)
	where 
		st.ref = @ref 
		and site_nr = @site_nr
	group by 
		st.ref

	/* escaloes mrgs regressivas */
	Select
		versao
		,escalao
		,PVPmin
		,PVPmax
		,factorPond
		,factorPond2
		,mrgFarm
		,FeeFarm
	into
		#dadosMrgRegressivas
	from
		b_mrgRegressivas (nolock)
	where
		versao >= 2
		
	/* Dados saídas*/
	SELECT	
		sl.ref,
		qtt,
		grphmgcode = isnull(grphmgcode,'')
	INTO
		#dadosSaidas
	FROM	
		sl (nolock)
		left join fprod (nolock) on sl.ref = fprod.cnp
	where	
		sl.ref = @ref 
		and cm > 50
		AND armazem = case when @armazem = 0 then armazem else @armazem END
		and year(sl.datalc) = YEAR(DATEADD(MONTH,-1,getdate()))
		AND month(sl.datalc) = MONTH(DATEADD(MONTH,-1,getdate()))
			 
	
	/* dados saídas */	
	Select	
		ref,
		qtt = sum(qtt)
	INTO
		#dadosSaidasRef
	FROM	
		#dadosSaidas
	Group by
		ref
		
	/* dados saídas por grphmg */
	Select	
		grphmgcode,
		qtt = sum(qtt)
	into
		#dadosSaidasGH
	FROM	
		#dadosSaidas
	Group by
		grphmgcode
		

	/* stock por grp homogeneo */
	Select
		 --stock = sum(isnull(sa.stock,st.stock))
		 stock = ISNULL(sum(st.stock),0)
		,grphmgcode
	into 
		#dadosGH
	From
		[dbo].[st] (nolock)
		inner join [dbo].[fprod] on st.ref = fprod.cnp
		--left join [dbo].[sa] (nolock) on st.ref = sa.ref and sa.armazem=@armazem
	where
		grphmgcode!='GH0000' and grphmgcode!=''
		and (st.site_nr = @site_nr) 
	Group by
		grphmgcode
	
	/* Result Set Final */	
	select top(@top) 
		site = isnull(empresa.site,'')
		,st.ref
		,design		= st.design
		,stock = isnull(sa.stock,st.stock)
		,st.epcusto
		,st.epcpond
		,st.ivaincl
		,iva		= isnull(taxasiva.taxa,0)
		,pvp		= st.epv1
		,epcult		= st.epcult
		,pvpf		= CONVERT(numeric(13,2), 0)
		--,mrg		= st.marg1
		,mrg		= case when @tipomc=0 
						then	case when st.epcusto=0 then 0 else round(((round(st.epv1 / CASE WHEN ((isnull(taxasiva.taxa,0) / 100) + 1) = 0 THEN 1 ELSE ((isnull(taxasiva.taxa,0) / 100) + 1) END,2)/st.epcusto)-1)*100,0) end
						else	case when epv1=0 then 0 else round((1-(st.epcusto/round(st.epv1 / CASE WHEN ((isnull(taxasiva.taxa,0) / 100) + 1) = 0 THEN 1 ELSE ((isnull(taxasiva.taxa,0) / 100) + 1) END,2)))*100,0) end
						end  
		--,mrg2		= st.marg2
		,mrg2		= case when st.epcusto=0 then 0 else (1-(st.epcult/st.epcusto))*100 end
		,mrg3		= st.marg3
		,mrg4		= st.marg4
		,mrgf		= convert(numeric(6,2), 0)
		,pcl		= st.epcult
		,calc		= CONVERT(bit,0)
		,grupo		= isnull(fprod.grupo, '')
		,pvdic		= isnull(fprod.pvporig, 0.00) / CASE WHEN ((isnull(taxasiva.taxa,0) / 100) + 1) = 0 THEN 1 ELSE ((isnull(taxasiva.taxa,0) / 100) + 1) END
		,pvpdic		= isnull(fprod.pvporig, 0.00)
		,pvpref 	= isnull(fprod.pref, 0.00)
		,mrgbruta	= st.marg3
		,mrgbrutaf	= CONVERT(numeric(6,2), 0)
		,desccom	= st.marg2
		,desccomf	= CONVERT(numeric(6,2), 0)
		,benbruto	= st.marg4
		,benbrutof	= CONVERT(numeric(13,2), 0)
		,st.ststamp
		,st.qlook
		,pvpmax		= convert(numeric(13,3), isnull(fprod.pvpmax, 0))
		,pvporig	= convert(numeric(13,3), isnull(fprod.pvporig, 0))
		,[local]	= case when st.u_local = '' then st.local else st.local + ',' + st.u_local end
		,escalao	= isnull((Select top 1 escalao from #dadosMrgRegressivas Where pvporig between PVPmin and PVPmax order by versao desc),0)
		,factorPond	= isnull((Select top 1 factorPond from #dadosMrgRegressivas Where pvporig between PVPmin and PVPmax order by versao desc),0)
		,factorPond2 = isnull((Select top 1 factorPond2 from #dadosMrgRegressivas Where pvporig between PVPmin and PVPmax order by versao desc),0)
		,mrgFarm	= isnull((Select top 1 mrgFarm from #dadosMrgRegressivas Where pvporig between PVPmin and PVPmax order by versao desc),0)
		,feeFarm	= isnull((Select top 1 feeFarm from #dadosMrgRegressivas Where pvporig between PVPmin and PVPmax order by versao desc),0)
		,pvpmaxre	= convert(numeric(13,3), isnull(fprod.pvpmaxre, 0))
		,pv	= round(st.epv1 / CASE WHEN ((isnull(taxasiva.taxa,0) / 100) + 1) = 0 THEN 1 ELSE ((isnull(taxasiva.taxa,0) / 100) + 1) END,2)
		,mbspvp	= (1-(st.epcpond/
					CASE WHEN (st.epv1 / ((isnull(taxasiva.taxa,0) / 100) + 1)) = 0
					THEN 1 ELSE
					(st.epv1 / ((isnull(taxasiva.taxa,0) / 100) + 1))
					END
					))*100
		,fprod.grphmgcode
		,stockGH			= isnull(#dadosGH.stock,0)
		--,qttcli = isnull(sa.rescli,st.qttcli)
		,qttcli = isnull(st.qttcli,0)
		--,qttfor = isnull(sa.resfor,st.qttfor)
		,qttfor = isnull(st.qttfor,0)
		,qttVendUltMes = isnull(#dadosSaidasRef.qtt,0)
		,GHqttVendUltMes = isnull(#dadosSaidasGH.qtt,0)
		,stmax
		,st.ptoenc
		,lote = ''
		,cnpem = isnull(fprod.cnpem,'')
		,u_escoacompart
		,sitcomdata = convert(date, sitcomdata) 
		,sitcomdescr
		,escoamento_farma = @escoamento_farma
		,psico = isnull(fprod.psico,convert(bit,0))
		,benzo = isnull(fprod.benzo,convert(bit,0))
		,validade
		,#dadosDesign.stockEmp
		,#dadosStSite.stockArm
	from
		st (nolock)
		left join sa (nolock) on sa.ref = st.ref and sa.armazem = @armazem
		left join empresa (nolock) on empresa.no = st.site_nr 
		left join fprod (nolock) on fprod.cnp=st.ref
		left join taxasiva (nolock) on taxasiva.codigo=st.tabiva
		left join #dadosSaidasRef on #dadosSaidasRef.ref = st.ref
		left join #dadosSaidasGH on #dadosSaidasGH.grphmgcode = fprod.grphmgcode
		left join #dadosGH	on #dadosGH.grphmgcode = fprod.grphmgcode
		left join #dadosDesign on #dadosDesign.ref = st.ref
		left join #dadosStSite on #dadosStSite.ref = st.ref
	where 
		st.ref in (select ref from #dadosDesign)
		and st.faminome = case when @familia='' then st.faminome else @familia end
		and st.site_nr = @site_nr
	order by 
		st.design
	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosMrgRegressivas'))
		DROP TABLE #dadosMrgRegressivas
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSaidas'))
		DROP TABLE #dadosSaidas
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSaidas'))
		DROP TABLE #dadosSaidas
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSaidasRef'))
		DROP TABLE #dadosSaidasRef
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSaidasRef'))
		DROP TABLE #dadosSaidasRef	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSaidasGH'))
		DROP TABLE #dadosSaidasGH	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosGH'))
		DROP TABLE #dadosGH		
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosDesign'))
		DROP TABLE #dadosDesign
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosStSite'))
		DROP TABLE #dadosStSite

GO
Grant Execute on dbo.up_stocks_ResumoSt to Public
Grant Control on dbo.up_stocks_ResumoSt to Public
go	
