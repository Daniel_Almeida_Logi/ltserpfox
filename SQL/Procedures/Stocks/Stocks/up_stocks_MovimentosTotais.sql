/* Movimentos de Stocks de todos 

	 exec up_stocks_MovimentosTotais 1, '19000101', '20210121',1,'',''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


IF OBJECT_ID('[dbo].[up_stocks_MovimentosTotais]') IS NOT NULL
	drop procedure dbo.up_stocks_MovimentosTotais
go

CREATE procedure dbo.up_stocks_MovimentosTotais
@armazem numeric(5,0),
@dataIni datetime,
@dataFim datetime,
@modo bit,
@lote varchar(30),
@movimento varchar(15)='Todos'

/* WITH ENCRYPTION */
AS

	If OBJECT_ID('tempdb.dbo.#SaldoAnterior') IS NOT NULL
		drop table #SaldoAnterior
	If OBJECT_ID('tempdb.dbo.#TempMovimentos') IS NOT NULL
		drop table #TempMovimentos
		
		
	select 
		sl.ref
		,saldoAnterior = SUM(Case When cm <50 Then qtt Else - qtt End)
		,entradasAnterior = sum(case 
									when cm < 50 then 
										case when qtt < 0 then 0 else round(qtt,0) end
										else case when qtt < 0 and cm > 50 then	qtt * -1 else round(0,0) end
								end
							)
		,saidasAnterior = sum(case 
									when cm > 50 and qtt > 0 
										then round(qtt,0) 
										else case 
												when cm < 50 and qtt < 0
													then ROUND(qtt*-1,0)
													else round(0,0) 
											end
										end
							)
	into 
		#SaldoAnterior	
	from 
		sl (nolock) 
	WHere 
		sl.datalc <  @dataIni 
		and sl.armazem = @armazem  
		and sl.lote like case when @lote = '' then sl.lote else @lote + '%' end	
		--and sl.cm between 
		--	(case when @movimento like '%Todos%' then 1 else (case when @movimento like '%Entradas%' then 1 else (case when @movimento like '%Saídas%' then 51 else 0 end ) end) end) 
		--	and 
		--	(case when @movimento like '%Todos%' then 99 else (case when @movimento like '%Entradas%' then 50 else (case when @movimento like '%Saídas%' then 99 else 0 end ) end) end)
	group by ref	

	SELECT
		ref
		,sl.cm
		,sl.slstamp
		,ROW_NUMBER() over(order by sl.ref,  sl.datalc asc, sl.ousrhora asc) as numero
		,qtt
	Into
		#TempMovimentos
	FROM
		sl (nolock)
	WHERE
		 sl.datalc between  @dataIni and @dataFim
		and sl.armazem = @armazem
		and sl.lote like case when @lote = '' then sl.lote else @lote + '%' end
		--and sl.cm between 
		--	(case when @movimento like '%Todos%' then 1 else (case when @movimento like '%Entradas%' then 1 else (case when @movimento like '%Saídas%' then 51 else 0 end ) end) end) 
		--	and 
		--	(case when @movimento like '%Todos%' then 99 else (case when @movimento like '%Entradas%' then 50 else (case when @movimento like '%Saídas%' then 99 else 0 end ) end) end)
	group by ref , sl.cm,sl.slstamp,qtt,sl.datalc, sl.ousrhora
	
	--select * from #TempMovimentos 




	SELECT 
		--SALDOANTERIOR		=(select SALDOANTERIOR from #SaldoAnterior where #SaldoAnterior.ref= b.ref)
		--,ENTRADASANTERIOR	=(select ENTRADASANTERIOR from #SaldoAnterior where #SaldoAnterior.ref= b.ref)
		--,SAIDASANTERIOR		=(select SAIDASANTERIOR from #SaldoAnterior where #SaldoAnterior.ref= b.ref)
		SALDO				= isnull((select SALDOANTERIOR from #SaldoAnterior where #SaldoAnterior.ref= b.ref),0) + 
							Case when @modo=1
								then
									isnull((
												Select sum(case when a.cm < 50 then a.qtt else - a.qtt end) 
												From   (select * from #TempMovimentos where #TempMovimentos.ref = b.ref)a
														inner join sl (nolock) on sl.slstamp = a.slstamp and a.ref= sl.ref
														Where a.numero <= b.numero
										   ),0)
								else
									0 
							End
		,ENTRADAS			= 
							case when b.cm < 50 
								then 
									case when b.qtt < 0
										then
											0
										else
											round(b.qtt,0) 
										end
								else 
									case when b.qtt < 0 and b.cm > 50
										then
											b.qtt * -1
										else
											round(0,0) 
										end
								end
		,SAIDAS				= 
							case when b.cm > 50 and b.qtt > 0
								then 
									round(b.qtt,0) 
								else 
									case when b.cm < 50 and b.qtt < 0
										then
											ROUND(b.qtt*-1,0)
										else
											round(0,0) 
										end
								end
		,*
	into #dadossl
	From (	
		select
			ROW_NUMBER() over(order by sl.ref, sl.datalc asc, sl.ousrhora asc) as numero
			,qtt 
			,ref
			,design 
			,slstamp
			,convert(varchar,datalc,102) as datalc
			,cm
			,cmdesc as cmdesc
			,adoc 
			,nome + (case when bistamp='' then '' else (' - '+ (select codext from bo2 (nolock) where bo2.bo2stamp=(select bostamp from bi (nolock) where bi.bistamp=sl.bistamp))) end) as nome
			,sl.armazem
			--,evu as PCL
			--,ett as PCLTOTAL
			,pcl	= case when sl.fistamp<>'' then	
							(select ecusto from fi (nolock) where fi.fistamp=sl.fistamp)
						else
							case when sl.fnstamp<>'' then
								(select u_upc from fn (nolock) where fn.fnstamp=sl.fnstamp)
							else
								(select u_upc from bi (nolock) where bi.bistamp=sl.bistamp)
							end 
						end 
			,pcltotal= (case when sl.fistamp<>'' then	
							(select ecusto from fi (nolock) where fi.fistamp=sl.fistamp)
						else
							case when sl.fnstamp<>'' then
								(select u_upc from fn (nolock) where fn.fnstamp=sl.fnstamp)
							else
								(select u_upc from bi (nolock) where bi.bistamp=sl.bistamp)
							end 
						end )*qtt
			,epcpond as PCP
			,(epcpond*qtt) as PCPTOTAL
			,ousrinis
			,sl.ousrhora
			,sl.ousrdata
			,CONVERT(bit,0) as verdoc
			,origem 
			,fistamp
			,bistamp
			,fnstamp
			,sticstamp
			,(select top 1 username from b_us where sl.ousrinis=b_us.iniciais) as username
			,lote
		from
			sl (nolock) 
		where
			 sl.datalc between @dataIni and @dataFim
			and sl.armazem = @armazem
			and sl.lote like case when @lote = '' then sl.lote else @lote + '%' end
			--and sl.cm between 
			--	(case when @movimento like '%Todos%' then 1 else (case when @movimento like '%Entradas%' then 1 else (case when @movimento like '%Saídas%' then 51 else 0 end ) end) end) 
			--	and 
			--	(case when @movimento like '%Todos%' then 99 else (case when @movimento like '%Entradas%' then 50 else (case when @movimento like '%Saídas%' then 99 else 0 end ) end) end)
	) b
	order by
		 ref asc,
		datalc asc,
		ousrhora asc

	


	/* Elimina entradas se tipo de movimentos=entradas*/
	if @movimento like '%Entradas%' 
	delete from #dadossl where saidas>0

	/* Elimina saídas se tipo de movimentos=saídas*/
	if @movimento like '%Saídas%' 
	delete from #dadossl where entradas>0

	Select ref,design,datalc,doc,ndoc,ENTRADAS,SAIDAS,saldo,nome, PCL, PCLTOTAL , PCP, PCPTOTAL,OPERADOR
	from
	(
		SELECT 1 as ord,'Referencia' AS ref,'designação' AS design,'data' AS datalc , 'Doc.' AS doc ,'Nº Documento' AS ndoc,'Entradas' AS Entradas,'Saídas' AS Saidas,'Saldo' AS Saldo,'Nome' AS Nome,'PCL' AS PCL,'PCL TOTAL' AS PCLTOTAL,'PCP' AS PCP,'PCP TOTAL' AS PCPTOTAL,'OPERADOR' AS OPERADOR
		UNION ALL
		select 2 as ord, ref AS ref, design ,CONVERT(VARCHAR(30),datalc) AS datalc ,cmdesc as doc, CONVERT(VARCHAR(100),adoc) as ndoc, CONVERT(VARCHAR(30),ENTRADAS)  as ENTRADAS, CONVERT(VARCHAR(30),SAIDAS) as SAIDAS, CONVERT(VARCHAR(30),saldo) as saldo, nome, CONVERT(VARCHAR(30),pcl),CONVERT(VARCHAR(30),PCLTOTAL),CONVERT(VARCHAR(30),pcp),CONVERT(VARCHAR(30),PCPTOTAL), ousrinis as operador  from #dadossl 
		UNION ALL
		SELECT 3 ord, 'Total' AS ref,'' AS design,'' AS datalc , '' AS doc ,'' AS ndoc,CONVERT(VARCHAR(30),SUM(ENTRADAS)) AS Entradas,CONVERT(VARCHAR(30),SUM(SAIDAS)) AS Saidas,CONVERT(VARCHAR(30),SUM(SALDO))  AS Saldo,'' AS Nome,'' AS PCL,CONVERT(VARCHAR(30),SUM(PCLTOTAL)) AS PCLTOTAL,'' AS PCP,CONVERT(VARCHAR(30),SUM(PCPTOTAL)) AS PCPTOTAL,'' AS OPERADOR from #dadossl 
	 ) c
	 order by
		c.ord asc,
		 c.ref asc,
		c.datalc asc

GO
Grant Execute on dbo.up_stocks_MovimentosTotais to Public
Grant Control on dbo.up_stocks_MovimentosTotais to Public
go
  