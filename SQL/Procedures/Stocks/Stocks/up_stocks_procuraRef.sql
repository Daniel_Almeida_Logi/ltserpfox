/*
	Levanta informação do artigo
	exec up_stocks_procuraRef ' 5440987', 1, 0, 0

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_procuraRef]') IS NOT NULL
	drop procedure dbo.up_stocks_procuraRef
go

create procedure dbo.up_stocks_procuraRef
	@ref varchar (40)
	,@site_nr tinyint
	,@nr_fl numeric(10) = 0
	,@dep_fl numeric(3) = 0

/* WITH ENCRYPTION */
AS

	select top 1 
		st.ststamp
		,st.ref
		,st.design
		,st.familia
		,stock = case when st.stns=1 then 0 else st.stock end
		,epv1 = st.epv1
		,st.forref
		,st.fornecedor
		,st.usr1
		,st.validade
		,st.usaid
		,st.uintr
		,st.usrqtt
		,st.eoq
		,st.pcult
		,st.pvultimo
		,st.unidade
		,st.ptoenc
		,st.tabiva
		,st.local
		,st.fornec
		,st.fornestab
		,st.qttfor
		,st.qttcli
		,st.qttrec
		,st.udata
		,st.pcusto
		,st.pcpond
		,st.qttacin
		,st.qttacout
		,st.stmax
		,st.stmin
		,st.obs
		,st.codigo
		,st.uni2
		,st.conversao
		,st.ivaincl
		,st.nsujpp
		,st.ecomissao
		,st.imagem
		,st.cpoc
		,st.containv
		,st.contacev
		,st.contareo
		,st.contacoe
		,st.peso
		,st.bloqueado
		,st.fobloq
		,st.mfornec
		,st.mfornec2
		,st.pentrega
		,st.consumo
		,st.baixr
		,st.despimp
		,st.marg1
		,st.marg2
		,st.marg3
		,st.marg4
		,st.diaspto
		,st.diaseoq
		,st.clinica
		,st.pbruto
		,st.volume
		,st.faminome
		,st.qttesp
		,epcusto = st.epcusto
		,st.epcpond
		,st.epcult
		,st.epvultimo
		,st.iva1incl
		,st.iva2incl
		,st.iva3incl
		,st.iva4incl
		,st.iva5incl
		,st.ivapcincl
		,st.stns
		,st.url
		,st.iecasug
		,st.iecaref
		,st.iecarefnome
		,st.iecaisref
		,st.qlook
		,st.qttcat
		,st.compnovo
		,st.ousrinis
		,st.ousrdata
		,st.ousrhora
		,st.usrinis
		,st.usrdata
		,st.usrhora
		,st.marcada
		,st.contaieo
		,st.inactivo
		,st.sujinv
		,st.u_nota1
		,st.u_impetiq
		,st.u_local
		,st.u_local2
		,st.u_lab
		,st.u_tipoetiq
		,st.u_validavd
		,st.u_aprov
		,st.u_fonte
		,st.u_validarc
		,st.u_servmarc
		,st.u_duracao
		,st.u_secstamp
		,st.u_catstamp
		,st.u_famstamp
		,st.u_depstamp
		,st.usalote
		,st.rateamento
		,st.exportado
		,st.u_codint
		,st.mrsimultaneo
		,iva = (select top 1 taxa  from taxasiva (nolock) where codigo = tabiva)
		,site = isnull(empresa.site,'')
		,psico = ISNULL(fprod.psico,convert(bit,0))
		,benzo = ISNULL(fprod.benzo ,convert(bit,0))
		,descFin_pcl = case 
						when isnull(pvporig,0)<=0 or isnull(protocolo,1)=1 then 0
						else isnull((select [desc]
									from fl_descontos fld (nolock)
									where fld.site_nr = @site_nr
										and fld.nr_fl=@nr_fl and fld.dep_fl=@dep_fl
										and fld.escalao = isnull((Select top 1 escalao from b_mrgRegressivas (nolock) Where pvporig between PVPmin and PVPmax order by versao desc),0)
									),0)
						end
		,cnpem = isnull(fprod.cnpem,'')
		,pvpdic	= convert(numeric(13,3), isnull(fprod.pvporig, 0))
		,fprod.dispositivo_seguranca
		,st.qttminvd
		,st.qttembal
		,st.codmotiseimp
		,st.motiseimp
		,st.qttEmbEnc
		,convert(varchar(20), convert(date, st.dataIniPromo,108)) as dataIniPromo
		,convert(varchar(20), convert(date, st.dataFimPromo,108)) as dataFimPromo
	from
		st (nolock)
		inner join empresa (nolock) on empresa.no = st.site_nr
		left join fprod (nolock) on st.ref = fprod.cnp
	where
		(
			st.ref= LTRIM(RTRIM(@ref))
			OR st.ref in (Select bc.ref from bc (nolock) where bc.codigo = LTRIM(RTRIM(@ref)) and bc.site_nr = @site_nr)
		)
		/*AND st.inactivo = 0 --validado no fox */
		AND st.site_nr = @site_nr

GO
Grant Execute on dbo.up_stocks_procuraRef to Public
Grant Control on dbo.up_stocks_procuraRef to Public
GO
