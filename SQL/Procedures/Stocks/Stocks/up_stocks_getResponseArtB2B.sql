SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_getResponseArtB2B]') IS NOT NULL
	drop procedure dbo.up_stocks_getResponseArtB2B
go

create procedure dbo.up_stocks_getResponseArtB2B
@token VARCHAR(254),
@site as INT 

AS

	select 
        ststamp, 
        st.design,
        Ext_esb_orders_d.ref, 
        'PVP: ' + cast(cast(pvp as numeric(15,2)) as varchar(20)) + ' ; PCT: ' + cast(cast(pct as numeric(15,2)) as varchar(20))+ ' ; Disponib.: ' + cast(cast(existence as numeric(10,0)) as varchar(20)) + ' - ' + existenceDesc + ' ; Dt. Entrega: ' + CONVERT(varchar, deliveryDate, 103) as result, 
        LTRIM(existenceDesc) + (CASE WHEN existenceDesc = 'ND' THEN (CASE WHEN reason <> '' THEN ' - ' + LTRIM(RTRIM(reason)) ELSE '' END) ELSE '' END) as existenceDesc, 
        pct, 
        isnull(Ext_esb_orders_d.desc1,0) as desc1, 
        epcpond, 
        right('0'+cast(month(deliveryDate) as varchar(3)),2)+'.'+right('0'+cast(day(deliveryDate) as varchar(3)),2)+' '+left(CONVERT(varchar, deliveryDate, 108),2)+':'+substring(CONVERT(varchar, deliveryDate, 108),4,2) as deliveryDate, 
        (case when fl.nome2='' then fl.nome else fl.nome2 end) as nome2, 
        fl.no,
        Ext_esb_orders_d.pvp,
		Ext_esb_orders_d.pcl,
		Ext_esb_orders_d.conditions
    from 
        Ext_esb_orders_d (nolock) 
        inner join st (nolock) on st.ref=Ext_esb_orders_d.ref and st.site_nr = @site
        inner join fl (nolock) on fl.no=Ext_esb_orders_d.nr_fl and fl.estab=Ext_esb_orders_d.flestab
    where 
		token=@token

GO
Grant Execute On dbo.up_stocks_getResponseArtB2B to Public
Grant Control On dbo.up_stocks_getResponseArtB2B to Public
GO