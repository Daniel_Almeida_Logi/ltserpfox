/* exec up_stocks_pesquisaRecursos '',0 */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_pesquisaRecursos]') IS NOT NULL
    drop procedure up_stocks_pesquisaRecursos
go

create PROCEDURE up_stocks_pesquisaRecursos
@nome varchar(254),
@no as numeric(9,2)

/* WITH ENCRYPTION */
AS

	select 
		sel = CONVERT(bit,0)
		,nome
		,no
		,tipo
		,grupo
		,recursosstamp
	from 
		recursos (nolock)
	Where
		nome = case when @nome = '' then nome else @nome end
		and no = case when @no = 0 then no else @no end
	
GO
Grant Execute On up_stocks_pesquisaRecursos to Public
Grant Control On up_stocks_pesquisaRecursos to Public
go
