/*
	Hist�rico de Pre�os de Refer�ncia

	exec up_stocks_hpr '4600284'
	exec up_stocks_hpr '5440987', 0
	

	
	
	select
		PIC	= convert(numeric(9,2),preco)
		,data
		,data_fim = case when data_fim = '19000101' then '' else convert(varchar,data_fim) end
	from
		hist_precos hp (nolock)
	where
		hp.ref = '5000203'
		and hp.id_preco = 1
		--and '20130921' >= hp.data --and (@data <= hp.data_fim or data_fim = '19000101')
	group by
		data, id_preco, preco, data_fim, ativo
	order by
		data desc
	
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_hpr]') IS NOT NULL
    drop procedure dbo.up_stocks_hpr
go

create procedure dbo.up_stocks_hpr
	@ref char(18)
	,@atendimento int = 0
	,@data datetime  = '19000101'

/* WITH ENCRYPTION */
AS
	
	IF @atendimento = 0
	begin
	
		select
			ref
			,data
			,id_preco
			,preco
			,data_fim = case when data_fim='19000101' then '30000101' else data_fim end
			,ativo
		into
			#tmp_hp
		from
			hist_precos (nolock)
		where
			ref = @ref

			

		select distinct
			ano, mes, dia
			,pve, pve_val, pve_val_data = case when pve_val_data='3000.01.01' then 'N/D' else pve_val_data end
			,pref, pref_val, pref_val_data = case when pref_val_data='3000.01.01' then 'N/D' else pref_val_data end
			,pt5, pt5_val, pt5_val_data = case when pt5_val_data='3000.01.01' then 'N/D' else pt5_val_data end
			,pt4, pt4_val, pt4_val_data = case when pt4_val_data='3000.01.01' then 'N/D' else pt4_val_data end
			,pma, pma_val, pma_val_data = case when pma_val_data='3000.01.01' then 'N/D' else pma_val_data end
		from (
			select
				ano			= year(data)
				,mes		= month(data)
				,dia		= day(data)
				,pve		= isnull(case
										-- case seja pre�o de venda / notificado
										when (id_preco = 1 or (id_preco = 602 and preco != 0)) and data_fim < data then 0
										when (id_preco = 1 or (id_preco = 602 and preco != 0)) and preco is not null then preco
										-- caso n�o seja pre�o de venda / notificado
										else (select top 1 isnull(preco,0) from #tmp_hp hp2 where id_preco in (1, 602) and hp2.data <= hp.data and hp2.data_fim >= hp.data order by data desc, id_preco desc) end,0)
				,pve_val	= convert(bit,isnull(case
										when id_preco in (1, 602) then ativo
										else (select top 1 ativo from #tmp_hp hp2 where id_preco in (1, 602) and hp2.data<=hp.data and hp2.data_fim>=hp.data order by data desc)
										end
									,0))
				,pve_val_data	= isnull(case
										when id_preco = 1 then convert(varchar,data_fim,102)
										else (select top 1 convert(varchar,data_fim,102) from #tmp_hp hp2 where (id_preco=1 or id_preco = 602) and hp2.data<=hp.data order by data desc)
										end
									,'')

				,pref		= isnull(case 
										when id_preco = 101 and data_fim < data then 0
										when id_preco = 101 and preco is not null then preco
										else (select top 1 isnull(preco,0) from #tmp_hp hp2 where id_preco=101 and hp2.data<=hp.data and hp2.data_fim>=hp.data order by data desc) end,0)
				,pref_val	= convert(bit,isnull(case
										when id_preco = 101 and (data_fim < convert(date,getdate()) or data_fim < data) then 0
										else (select top 1 
												case when (isnull(data_fim,getdate()-1) < convert(date,getdate()) or data_fim < data) then 0 else 1 end 
												from #tmp_hp hp2 where id_preco=101 and hp2.data<=hp.data and hp2.data_fim>=hp.data order by data desc) 
										end
									,0))
				,pref_val_data	= isnull(case
										when id_preco = 101 then convert(varchar,data_fim,102)
										else (select top 1 convert(varchar,data_fim,102) from #tmp_hp hp2 where id_preco=101 and hp2.data<=hp.data order by data desc)
										end
									,'')

				,pt5		= isnull(case 
										when id_preco = 301 and data_fim < data then 0
										when id_preco = 301 and preco is not null then preco
										else (select top 1 isnull(preco,0) from #tmp_hp hp2 where id_preco=301 and hp2.data<=hp.data and hp2.data_fim>=hp.data order by data desc) end,0)
				,pt5_val	= convert(bit,isnull(case
										when id_preco = 301 and (data_fim < convert(date,getdate()) or data_fim < data) then 0
										else (select top 1 
												case when (isnull(data_fim,getdate()-1) < convert(date,getdate()) or data_fim < data) then 0 else 1 end 
												from #tmp_hp hp2 where id_preco=301 and hp2.data<=hp.data and hp2.data_fim>=hp.data order by data desc) 
										end
									,0))
				,pt5_val_data	= isnull(case
										when id_preco = 301 then convert(varchar,data_fim,102)
										else (select top 1 convert(varchar,data_fim,102) from #tmp_hp hp2 where id_preco=301 and hp2.data<=hp.data order by data desc)
										end
									,'')
				,pt4		= isnull(case 
										when id_preco = 603 and data_fim < data then 0
										when id_preco = 603 and preco is not null then preco
										else (select top 1 isnull(preco,0) from #tmp_hp hp2 where id_preco=603 and hp2.data<=hp.data and hp2.data_fim>=hp.data order by data desc) end,0)
				,pt4_val	= convert(bit,isnull(case
										when id_preco = 603 and (data_fim < convert(date,getdate()) or data_fim < data) then 0
										else (select top 1 
												case when (isnull(data_fim,getdate()-1) < convert(date,getdate()) or data_fim < data) then 0 else 1 end 
												from #tmp_hp hp2 where id_preco=603 and hp2.data<=hp.data and hp2.data_fim>=hp.data order by data desc) 
										end
									,0))
				,pt4_val_data	= isnull(case
										when id_preco = 603 then convert(varchar,data_fim,102)
										else (select top 1 convert(varchar,data_fim,102) from #tmp_hp hp2 where id_preco=603 and hp2.data<=hp.data order by data desc)
										end
									,'')

				,pma		= isnull(case
										when id_preco = 401 and data_fim < data then 0
										when id_preco = 401 and preco is not null then preco
										else (select top 1 isnull(preco,0) from #tmp_hp hp2 where id_preco=401 and hp2.data<=hp.data and hp2.data_fim>=hp.data order by data desc) end,0)
				,pma_val	= convert(bit,isnull(case
								when id_preco = 401 then ativo
								else (select top 1 ativo from #tmp_hp hp2 where id_preco=401 and hp2.data<=hp.data and hp2.data_fim>=hp.data order by data desc)
								end
							,0))
				,pma_val_data	= isnull(case
										when id_preco = 401 then convert(varchar,data_fim,102)
										else (select top 1 convert(varchar,data_fim,102) from #tmp_hp hp2 where id_preco=401 and hp2.data<=hp.data order by data desc)
										end
									,'')

				,id_preco
			from
				#tmp_hp hp
			group by
				data, id_preco, preco, data_fim, ativo
		) x
		order by
			ano desc, mes desc, dia desc
	
	end else begin
	
		IF @atendimento = 1
		begin

			select
				PIC	= convert(numeric(9,2),preco)
				,'PRECO_VALIDO'        = case when hp.ativo = 1 then 'SIM' else 'NAO' END
				,'DATA_FIM_ESCOAMENTO' = CASE WHEN YEAR(HP.data_fim_escoamento) <= 2000 THEN '' ELSE convert(varchar,hp.data_fim_escoamento,101) end
			from
				hist_precos hp (nolock)
			where
				hp.ref = @ref
				and (hp.id_preco = 1 or (hp.id_preco = 602 and preco != 0)) -- alterado para considerar tamb�m os Pre�os notificados
			group by
				data, id_preco, preco, data_fim, ativo, DATA_FIM_ESCOAMENTO
			order by
				data desc
		
		end else begin
			
			/* Pic � data, n�o pode ser validados pelo campo ativo, mas pelo intervalo de datas, existe um problema com a data de fim que por vezes n�o � atualizada pelo infarmed */
			select
				PIC	= convert(numeric(9,2),preco)
				,data_inicio = convert(varchar,data,102)
				,data_fim = case when data_fim = '19000101' then '' else convert(varchar,data_fim,102) end
				,'PRECO_VALIDO' = case when hp.ativo = 1 then 'SIM' else 'NAO' END
				,'DATA_FIM_ESCOAMENTO' = CASE WHEN YEAR(HP.data_fim_escoamento) <= 2000 THEN '' ELSE convert(varchar,hp.data_fim_escoamento,101) end
			from
				hist_precos hp (nolock)
			where
				hp.ref = @ref
				and (hp.id_preco = 1 or (hp.id_preco = 602 and preco != 0)) -- alterado para considerar tamb�m os Pre�os notificados
				and data <= @data
				/*and (data_fim >= @data or data_fim = '19000101') A data de fim nao esta correcta no dicionario*/
			group by
				data, id_preco, preco, data_fim, ativo, DATA_FIM_ESCOAMENTO
			order by
				data desc
				
				
		end
	End

	if OBJECT_ID('tempdb.dbo.#tmp_hp') is not null
		drop table #tmp_hp
		
GO
Grant Execute on dbo.up_stocks_hpr to Public
Grant Control on dbo.up_stocks_hpr to Public
GO
