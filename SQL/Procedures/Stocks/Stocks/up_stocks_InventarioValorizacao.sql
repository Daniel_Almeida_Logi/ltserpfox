/* exec up_stocks_InventarioValorizacao '8168617','20131231',1,'PCL','' */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_InventarioValorizacao]') IS NOT NULL
    drop procedure up_stocks_InventarioValorizacao
go

create PROCEDURE up_stocks_InventarioValorizacao
@ref varchar(18),
@data as datetime,
@armazem as numeric(5,0),
@valorizacao as varchar(3),
@lote as varchar(30)

/* WITH ENCRYPTION */
AS
	declare @siteNr int = 1
	select top 1 @siteNr=empresa_no from empresa_arm(nolock) where armazem = @armazem
	 

	IF (select count(stamp) from b_parameters(nolock) where stamp = 'ADM0000000211' and bool = 1) = 0 and @lote=''
	BEGIN /*Sem Lote*/
		IF @valorizacao = 'PCP'
		BEGIN
		
			select top 1
				epreco = epcpond
				,preco = pcpond
			from
				st (nolock)
			where
				ref = @ref
				and site_nr = @siteNr

		END
		ELSE
		BEGIN /*PCL*/
			
			select top 1 
				epreco = epcult
				,preco = pcult
			from	
				st (nolock) 
			where	
				ref = @ref
				and site_nr = @siteNr
				
		END
	END
	ELSE
	BEGIN /*Com Lote*/
		IF @valorizacao = 'PCP'
		BEGIN
			select top 1 
				epreco = isnull(sl.epcpond, 0)
				,preco = isnull(sl.pcpond, 0)
			from	
				sl (nolock) 
			where	
				ref = @ref
				and datalc <= @data
				and sl.armazem = @armazem
				and sl.epcpond != 0
				and sl.lote = @lote
			order by 
				datalc + ousrhora desc
		END
		ELSE
		BEGIN /*PCL*/
			SELECT TOP 1 
				epreco = isnull(sl.EVU ,0)
				,preco = isnull(sl.VU ,0)
			FROM	
				SL (nolock) 
			WHERE	
				(ORIGEM = 'FO' OR ORIGEM = 'IF')
				AND SL.DATALC <= @data
				AND SL.REF = @ref
				AND EVU != 0
				And sl.armazem = @armazem
				and sl.lote = @lote
			ORDER BY 
				datalc + ousrhora DESC
		END
	END

GO
Grant Execute On up_stocks_InventarioValorizacao to Public
Grant Control On up_stocks_InventarioValorizacao to Public
go
