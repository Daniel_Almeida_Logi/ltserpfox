/* exec up_stocks_InventarioInserirNaoConferido '' ,2*/ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_InventarioInserirNaoConferido]') IS NOT NULL
    drop procedure up_stocks_InventarioInserirNaoConferido
go

create PROCEDURE up_stocks_InventarioInserirNaoConferido
@ListaSticstamp varchar(max)
,@site_nr tinyint

/* WITH ENCRYPTION */
AS

	--print @ListaSticstamp
	If OBJECT_ID('tempdb.dbo.#RefsConferidas') IS NOT NULL
		drop table #RefsConferidas
		
	Select 
		distinct ref 
	into
		#RefsConferidas
	from 
		stil (nolock) 
		Inner join stic (nolock) on stic.sticstamp = stil.sticstamp 
	Where 
		stic.sticstamp in (select items from dbo.up_SplitToTable(@ListaSticstamp,','))
	


	IF (select count(stamp) from b_parameters where stamp = 'ADM0000000211' and bool = 1) = 0 or (select top 1 tipoempresa from empresa)<>'ARMAZEM'
	BEGIN
	
		Select 
			st.ref
			,st.design
			,lote = ''
		FROM 
			st (nolock)
		Where 	
			ST.site_nr = @site_nr
			and st.ref not in (select #RefsConferidas.ref from #RefsConferidas)
			and st.stns=0
	END
	ELSE
	BEGIN
		
		declare @tempSlSt table (ref varchar(18), design varchar(60), lote varchar(30))
		insert into @tempSlSt 
		Select 
			distinct st.ref
			,st.design
			,lote = sl.lote
		From 
			sl (nolock) 
			inner join st (nolock) on sl.ref = st.ref
		where 
			st.site_nr = @site_nr
			and st.stns = 0
								
		declare @tempStil table (sticstamp varchar(25), ref varchar(18), lote varchar(30))
		insert into @tempStil 
		Select distinct sticstamp, ref, Lote From stil (nolock) where stil.sticstamp in (select items from dbo.up_SplitToTable(@ListaSticstamp,','))				
						
		Select 
			 slSt.ref, slSt.design, lote = slSt.lote
		from 
			@tempSlSt slSt
			left join @tempStil stil on slSt.ref = stil.ref and slSt.lote = stil.lote
		where
			stil.ref is null
	END

GO
Grant Execute On up_stocks_InventarioInserirNaoConferido to Public
Grant Control On up_stocks_InventarioInserirNaoConferido to Public
go
