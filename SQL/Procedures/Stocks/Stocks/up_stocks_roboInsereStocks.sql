/*
    grava informação de stocks de productos

	campo 1 - stamp
	campo 2 - referencia do producto
	campo 3 - token do pedido
	campo 4 - quantidade em stock (robot)
	campo 5 - fim de stock (robot)
	campo 6 - numero de referncias totais do inventario (robot)

	exec up_stocks_roboInsereStocks  '01010', '5440986', '000', '10',5408,0 




*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_roboInsereStocks]') IS NOT NULL
	drop procedure dbo.up_stocks_roboInsereStocks
go

create procedure dbo.up_stocks_roboInsereStocks

@stamp varchar(100),
@ref varchar(100),
@token varchar(100),
@quantity int,
@maxRefs int,
@lastRequest bit



/* WITH ENCRYPTION */
AS

	SET NOCOUNT ON

		
		INSERT INTO [dbo].[robot_response_payload_stock]
		 (stamp
		 ,token
		 ,productCode
		 ,quantity
		 ,lastRequest
		 ,maxRefs)
		 VALUES
		 (@stamp
		 ,@token
		 ,@ref
		 ,@quantity
		 ,@lastRequest
		 ,@maxRefs
		 )

	select 
		"no" = count(*) 
	from 
		robot_response_payload_stock(nolock) 
	where 
		productCode= @ref and token = @token



GO
Grant Execute On dbo.up_stocks_roboInsereStocks to Public
Grant Control On dbo.up_stocks_roboInsereStocks to Public
GO



