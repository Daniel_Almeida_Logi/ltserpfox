/* SP para criar cursor na conf. PVP
	
	exec up_stocks_conferenciaPvps 1, 0, '5440987', '', '', 30,'Loja 1', '>', '0', '', 1
	exec up_stocks_conferenciaPvps 0, 1, '', '', 'FRr971E9B01-CB62-4004-B34', 999999, 'Loja 1', '>', -9999999, '', 0

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_conferenciaPvps]') IS NOT NULL
	DROP PROCEDURE dbo.up_stocks_conferenciaPvps
GO

CREATE PROCEDURE dbo.up_stocks_conferenciaPvps
	 @armazem			NUMERIC(5,0)
	,@fac				bit
	,@ref				varchar(60)
	,@familia			varchar(60)
	,@fostamp			varchar(25)
	,@top				int
	,@site				varchar(20)
	,@operadorlogico	VARCHAR(5)
	,@stock				NUMERIC(13,3)
	,@lote				varchar(60)
	,@pvpInvalido		bit 
	,@tipo				int = 0

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST'))
	DROP TABLE #dadosST
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST1'))
	DROP TABLE #dadosST1
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST2'))
	DROP TABLE #dadosST2

	-- definir se faz consulta na FO ou na BO
	declare @tipodoc as tinyint = 0
	if exists (select bostamp from bo(nolock) where bostamp = @fostamp) set @tipodoc = 1
	
	/* Cálculo de MC */
	declare @tipomc bit = (select bool from B_Parameters_site (nolock) where stamp='ADM0000000129' and site=(select site from empresa (nolock) where site=@site)) 

	IF @fac=0
	BEGIN		
		SELECT 
			st.ref
			,design				= st.design
			,stock				= ISNULL(sa.stock,st.stock)
			,st.epcusto
			,st.epcpond
			,st.ivaincl
			,iva				= ISNULL(taxasiva.taxa,0)
			,pvp				= st.epv1
			,epcult				= st.epcult
			,pvpf				= st.epv1
			--,mrg				= st.marg1
			,mrg				= case when @tipomc=0 
									then	case when st.epcusto=0 then 0 else round(((round(st.epv1 / CASE WHEN ((isnull(taxasiva.taxa,0) / 100) + 1) = 0 THEN 1 ELSE ((isnull(taxasiva.taxa,0) / 100) + 1) END,2)/st.epcusto)-1)*100,0) end
									else	case when st.epv1=0 then 0 else round((1-(st.epcusto/round(st.epv1 / CASE WHEN ((isnull(taxasiva.taxa,0) / 100) + 1) = 0 THEN 1 ELSE ((isnull(taxasiva.taxa,0) / 100) + 1) END,2)))*100,0) end
									end  
			,mrg2				= st.marg2
			,mrg3				= st.marg3
			,mrg4				= st.marg4
			--,mrgf				= st.marg1
			,mrgf				= case when @tipomc=0 
									then	case when st.epcusto=0 then 0 else round(((round(st.epv1 / CASE WHEN ((isnull(taxasiva.taxa,0) / 100) + 1) = 0 THEN 1 ELSE ((isnull(taxasiva.taxa,0) / 100) + 1) END,2)/st.epcusto)-1)*100,0) end
									else	case when st.epv1=0 then 0 else round((1-(st.epcusto/round(st.epv1 / CASE WHEN ((isnull(taxasiva.taxa,0) / 100) + 1) = 0 THEN 1 ELSE ((isnull(taxasiva.taxa,0) / 100) + 1) END,2)))*100,0) end
									end 
			,pcl				= st.epcult
			,calc				= CONVERT(bit,0)
			,grupo				= ISNULL(fprod.grupo, '')
			,pvdic				= ISNULL(fprod.pvporig, 0.00) / CASE WHEN [taxa].[Percentagem] = 0 THEN 1 ELSE [taxa].[Percentagem] END
			,pvpdic				= ISNULL(fprod.pvporig, 0.00)
			,pvpref 			= ISNULL(fprod.pref, 0.00)
			,mrgbruta			= st.marg3
			,mrgbrutaf			= CONVERT(NUMERIC(6,2), 0)
			,desccom			= st.marg2
			--,desccomf			= CONVERT(NUMERIC(6,2), 0)
			,desccomf			= CONVERT(NUMERIC(12,2), st.marg2)
			,benbruto			= st.marg4
			,benbrutof			= st.marg4
			,st.ststamp
			,st.qlook
			,pvpmax				= CONVERT(NUMERIC(13,3), ISNULL(fprod.pvpmax, 0))
			,pvporig			= CONVERT(NUMERIC(13,3), ISNULL(fprod.pvporig, 0))
			,[local]			= CASE WHEN st.u_local = '' THEN st.local ELSE st.local + ',' + st.u_local END
			,escalao			= [MrgReg].escalao
			,mrgFarm			= [MrgReg].mrgFarm
			,feeFarm			= [MrgReg].feeFarm
			,pvpmaxre			= CONVERT(NUMERIC(13,3), ISNULL(fprod.pvpmaxre, 0))
			,pv					= st.epv1 / CASE WHEN [taxa].[Percentagem] = 0 THEN 1 ELSE [taxa].[Percentagem] END
			,mbspvp				= (1-(st.epcpond /
												CASE WHEN (st.epv1 / [taxa].[Percentagem]) = 0
														THEN 1 
														ELSE (st.epv1 / [taxa].[Percentagem])
												END
												))*100
			,fprod.grphmgcode
			,qttcli				= ISNULL(sa.rescli,st.qttcli)
			,qttfor				= ISNULL(sa.resfor,st.qttfor)
			,stmax
			,st.ptoenc
			,lote				= isnull(st_lotes.lote,'')
			,PVPLote			= isnull(st_lotes_precos.epv1,0)
			,AltPVPLote			= isnull(convert(varchar,st_lotes_precos.usrdata,102),'1900.01.01')
			,HoraAltPVPLote		= isnull(st_lotes_precos.usrhora,'')
			,atualizaPVPFicha	= CONVERT(bit,0)
			,DataAltPVPFicha	= getdate()
			,cnpem = isnull(fprod.cnpem,'')
			,empresa.site
			,mbspvpf				= (1-(st.epcpond /
												CASE WHEN (st.epv1 / [taxa].[Percentagem]) = 0
														THEN 1 
														ELSE (st.epv1 / [taxa].[Percentagem])
												END
												))*100
			,marg2F = case when st.epcusto=0 then 0 else (1-(st.epcult/st.epcusto))*100 end
		INTO
			#dadosST
		FROM
			[dbo].[st](nolock)
			INNER JOIN empresa(nolock) on empresa.no = st.site_nr
			LEFT JOIN [dbo].[sa](nolock) ON sa.ref = st.ref AND sa.armazem = @armazem
			LEFT JOIN [dbo].[fprod](nolock) ON fprod.cnp=st.ref
			LEFT JOIN [dbo].[taxasiva] (nolock) ON taxasiva.codigo=st.tabiva
			LEFT JOIN st_lotes (nolock) on st.ref = st_lotes.ref and st_lotes.site = @site
			LEFT JOIN st_lotes_precos (nolock) on st_lotes_precos.stampLote = st_lotes.stamp
			OUTER APPLY (
				SELECT TOP 1
					ISNULL(escalao,0) as escalao,
					ISNULL(mrgFarm,0) as mrgFarm,
					ISNULL(feeFarm,0) as feeFarm
				FROM 
					[dbo].[b_mrgRegressivas](nolock)
				WHERE 
					pvporig BETWEEN PVPmin AND PVPmax 
				ORDER BY 
					versao DESC
			) AS [MrgReg]
			CROSS APPLY (
				SELECT 
					(ISNULL(taxasiva.taxa,0) / 100) + 1 AS [Percentagem]
			) AS [taxa]
		WHERE
			(st.ref=left(@ref,18) OR st.design LIKE '%'+@ref+'%')
			AND st.faminome = CASE WHEN @familia = '' THEN st.faminome ELSE @familia END
			AND st.inactivo = 0
			AND empresa.site = @site
			and isnull(st_lotes.lote,'') = case when @lote = '' then isnull(st_lotes.lote,'') else @lote end	
	
		if @stock != -999999
		begin
			if @operadorlogico = '>'
				delete from #dadosST where stock <= @stock
			if @operadorlogico = '<'
				delete from #dadosST where stock >= @stock
			if @operadorlogico = '='
				delete from #dadosST where stock != @stock
		end
	
		if @pvpInvalido = 1
		begin
			delete from #dadosST where pvpf in (select hp.preco from hist_precos(nolock) hp where ((hp.id_preco=1 or hp.id_preco=602) and hp.ativo=1)  and hp.ref=#dadosST.ref) or pvpdic = 0
		end
		SELECT *
		FROM(
			Select 
				top (@top)
				sel = CONVERT(BIT,0)
				,* 
			from 
				#dadosST st	
			order by 
				st.design, isnull(lote,'') ,isnull(convert(varchar,AltPVPLote,102),'1900.01.01') desc, isnull(HoraAltPVPLote,'') desc
		) as x
		where
			1 = (case 
				when @tipo = 0 then 1
				when @tipo = 1 then (case when pvpdic > 0 then 1 else 0 end)
				when @tipo = 2 then (case when pvpdic = 0 then 1 else 0 end)
				else 0
			end)


	END
	ELSE
	BEGIN
	print'a'
		-- Utilizada tabela FO
		IF @tipodoc = 0
		begin 
		print'b'
			SELECT distinct 
				st.ref
				,design				= st.design
				,stock				= ISNULL(sa.stock,st.stock)
				,st.epcusto
				,st.epcpond
				,st.ivaincl
				,iva				= ISNULL(taxasiva.taxa,0)
				,pvp				= st.epv1
				,epcult				= st.epcult
				,pvpf				= CASE WHEN fn.u_pvp = 0 THEN st.epv1 ELSE fn.u_pvp END
				--,mrg				= st.marg1
				,mrg				= case when @tipomc=0 
									then	case when st.epcusto=0 then 0 else round(((round(st.epv1 / CASE WHEN ((isnull(taxasiva.taxa,0) / 100) + 1) = 0 THEN 1 ELSE ((isnull(taxasiva.taxa,0) / 100) + 1) END,2)/st.epcusto)-1)*100,0) end
									else	case when st.epv1=0 then 0 else round((1-(st.epcusto/round(st.epv1 / CASE WHEN ((isnull(taxasiva.taxa,0) / 100) + 1) = 0 THEN 1 ELSE ((isnull(taxasiva.taxa,0) / 100) + 1) END,2)))*100,0) end
									end  
				,mrg2				= st.marg2
				,mrg3				= st.marg3
				,mrg4				= st.marg4
				--,mrgf				= st.marg1
				,mrgf				= case when @tipomc=0 
									then	case when st.epcusto=0 then 0 else round(((round(st.epv1 / CASE WHEN ((isnull(taxasiva.taxa,0) / 100) + 1) = 0 THEN 1 ELSE ((isnull(taxasiva.taxa,0) / 100) + 1) END,2)/st.epcusto)-1)*100,0) end
									else	case when st.epv1=0 then 0 else round((1-(st.epcusto/round(st.epv1 / CASE WHEN ((isnull(taxasiva.taxa,0) / 100) + 1) = 0 THEN 1 ELSE ((isnull(taxasiva.taxa,0) / 100) + 1) END,2)))*100,0) end
									end
				,pcl				= st.epcult
				,calc				= CONVERT(bit,0)
				,grupo				= ISNULL(fprod.grupo, '')
				,pvpdic				= ISNULL(fprod.pvporig, 0.00)
				,pvpref				= ISNULL(fprod.pref, 0.00)
				,mrgbruta			= st.marg3
				,mrgbrutaf			= CONVERT(NUMERIC(6,2), 0)
				,desccom			= st.marg2
				--,desccomf			= CONVERT(NUMERIC(6,2), 0)
				,desccomf			= CONVERT(NUMERIC(12,2), st.marg2)
				,benbruto			= st.marg4
				,benbrutof			= st.marg4
				,st.ststamp
				,st.qlook
				,pvpmax				= CONVERT(NUMERIC(13,3), ISNULL(fprod.pvpmax, 0))
				,pvporig			= CONVERT(NUMERIC(13,3), ISNULL(fprod.pvporig, 0))
				,[local]			= CASE WHEN st.u_local = '' THEN st.local ELSE st.local + ',' + st.u_local END
				,escalao			= [MrgReg].escalao
				,mrgFarm			= [MrgReg].mrgFarm
				,feeFarm			= [MrgReg].feeFarm
				,pvpmaxre			= CONVERT(NUMERIC(13,3), ISNULL(fprod.pvpmaxre, 0))
				,pv					= st.epv1 / CASE WHEN [taxa].[Percentagem] = 0 THEN 1 ELSE [taxa].[Percentagem] END
				,mbspvp				= (1-(st.epcpond / 
														CASE WHEN (st.epv1 / [taxa].[Percentagem]) = 0
																THEN 1 
																ELSE (st.epv1 / [taxa].[Percentagem])
														END ))*100
				,fprod.grphmgcode
				,qttcli				= ISNULL(sa.rescli,st.qttcli)
				,qttfor				= ISNULL(sa.resfor,st.qttfor)
				,stmax
				,st.ptoenc
				,lote				= isnull(st_lotes.lote,'')
				,PVPLote			= isnull(st_lotes_precos.epv1,0)
				,AltPVPLote			= isnull(convert(varchar,st_lotes_precos.usrdata,102),'1900.01.01')
				,HoraAltPVPLote		= isnull(st_lotes_precos.usrhora,'')
				,atualizaPVPFicha	= CONVERT(bit,0)
				,DataAltPVPFicha	= getdate()
				,cnpem = isnull(fprod.cnpem,'')
				,empresa.site
				,mbspvpf				= (1-(st.epcpond / 
														CASE WHEN (st.epv1 / [taxa].[Percentagem]) = 0
																THEN 1 
																ELSE (st.epv1 / [taxa].[Percentagem])
														END ))*100
				,marg2F = case when st.epcusto=0 then 0 else (1-(st.epcult/st.epcusto))*100 end
			into 
				#dadosST1
			from
				st(nolock)
				inner join empresa(nolock) on empresa.no = st.site_nr
				LEFT JOIN sa(nolock) ON sa.ref = st.ref AND sa.armazem = @armazem
				INNER JOIN fn (nolock) ON fn.ref=st.ref OR fn.oref=st.ref
				INNER JOIN fo(nolock) ON fo.fostamp=fn.fostamp
				LEFT JOIN fprod (nolock) ON fprod.cnp=st.ref
				LEFT JOIN taxasiva(nolock) ON taxasiva.codigo=st.tabiva
				left join st_lotes (nolock) on st.ref = st_lotes.ref and st_lotes.site = @site
				left join st_lotes_precos (nolock) on st_lotes_precos.stampLote = st_lotes.stamp
				OUTER APPLY (
					SELECT TOP 1
						ISNULL(escalao,0) AS escalao,
						ISNULL(mrgFarm,0) AS mrgFarm,
						ISNULL(feeFarm,0) AS feeFarm
					FROM 
						[dbo].[b_mrgRegressivas](nolock)
					WHERE 
						pvporig BETWEEN PVPmin AND PVPmax 
					ORDER BY 
						versao DESC
				) as [MrgReg]

				CROSS APPLY (
					SELECT (ISNULL(taxasiva.taxa,0) / 100) + 1 as [Percentagem]
				) AS [taxa]
			WHERE
				(
					st.ref = CASE WHEN @ref = '' THEN st.ref ELSE @ref END
					OR
					st.design LIKE CASE WHEN @ref = '' THEN st.design ELSE '%'+@ref+'%' END
				)
				AND fo.fostamp=@fostamp and fn.qtt>0
				AND st.faminome = CASE WHEN @familia='' THEN st.faminome ELSE @familia END
				AND st.inactivo = 0
				and isnull(st_lotes.lote,'') = case when @lote = '' then isnull(st_lotes.lote,'') else @lote end
				and fo.site=@site
				and st.site_nr = (select no from empresa (nolock) where site=@site)
	
			if @stock != -999999
			begin
	
			if @operadorlogico = '>'
				delete from #dadosST1 where stock <= @stock
			if @operadorlogico = '<'
				delete from #dadosST1 where stock >= @stock
			if @operadorlogico = '='
				delete from #dadosST1 where stock != @stock
			end

			if @pvpInvalido = 1
			begin
				delete from #dadosST1 where pvpf in (select hp.preco from hist_precos(nolock) hp where ((hp.id_preco=1 or hp.id_preco=602) and hp.ativo=1) and hp.ref=#dadosST1.ref) or pvpdic = 0
			end

			SELECT *
			FROM(
				Select 
					top (@top)
					sel = CONVERT(BIT,0)
					,* 
				from 
					#dadosST1 st	
				order by
					st.design, isnull(lote,''),isnull(convert(varchar,AltPVPLote,102),'1900.01.01') desc,isnull(HoraAltPVPLote,'') desc
			) as x
			where
				1 = (case 
					when @tipo = 0 then 1
					when @tipo = 1 then (case when pvpdic > 0 then 1 else 0 end)
					when @tipo = 2 then (case when pvpdic = 0 then 1 else 0 end)
					else 0
				end)

		END
		ELSE
		BEGIN
			-- Utiliza a Tabela BO
			SELECT distinct 
					st.ref
					,design				= st.design
					,stock				= ISNULL(sa.stock,st.stock)
					,st.epcusto
					,st.epcpond
					,st.ivaincl
					,iva				= ISNULL(taxasiva.taxa,0)
					,pvp				= st.epv1
					,epcult				= st.epcult
					,pvpf				= st.epv1 
					--,mrg				= st.marg1
					,mrg				= case when @tipomc=0 
									then	case when st.epcusto=0 then 0 else round(((round(st.epv1 / CASE WHEN ((isnull(taxasiva.taxa,0) / 100) + 1) = 0 THEN 1 ELSE ((isnull(taxasiva.taxa,0) / 100) + 1) END,2)/st.epcusto)-1)*100,0) end
									else	case when st.epv1=0 then 0 else round((1-(st.epcusto/round(st.epv1 / CASE WHEN ((isnull(taxasiva.taxa,0) / 100) + 1) = 0 THEN 1 ELSE ((isnull(taxasiva.taxa,0) / 100) + 1) END,2)))*100,0) end
									end  
					,mrg2				= st.marg2
					,mrg3				= st.marg3
					,mrg4				= st.marg4
					--,mrgf				= st.marg1
					,mrgf				= case when @tipomc=0 
									then	case when st.epcusto=0 then 0 else round(((round(st.epv1 / CASE WHEN ((isnull(taxasiva.taxa,0) / 100) + 1) = 0 THEN 1 ELSE ((isnull(taxasiva.taxa,0) / 100) + 1) END,2)/st.epcusto)-1)*100,0) end
									else	case when st.epv1=0 then 0 else round((1-(st.epcusto/round(st.epv1 / CASE WHEN ((isnull(taxasiva.taxa,0) / 100) + 1) = 0 THEN 1 ELSE ((isnull(taxasiva.taxa,0) / 100) + 1) END,2)))*100,0) end
									end
					,pcl				= st.epcult
					,calc				= CONVERT(bit,0)
					,grupo				= ISNULL(fprod.grupo, '')
					,pvpdic				= ISNULL(fprod.pvporig, 0.00)
					,pvpref				= ISNULL(fprod.pref, 0.00)
					,mrgbruta			= st.marg3
					,mrgbrutaf			= CONVERT(NUMERIC(6,2), 0)
					,desccom			= st.marg2
					--,desccomf			= CONVERT(NUMERIC(6,2), 0)
					,desccomf			= CONVERT(NUMERIC(12,2), st.marg2)
					,benbruto			= st.marg4
					,benbrutof			= st.marg4
					,st.ststamp
					,st.qlook
					,pvpmax				= CONVERT(NUMERIC(13,3), ISNULL(fprod.pvpmax, 0))
					,pvporig			= CONVERT(NUMERIC(13,3), ISNULL(fprod.pvporig, 0))
					,[local]			= CASE WHEN st.u_local = '' THEN st.local ELSE st.local + ',' + st.u_local END
					,escalao			= [MrgReg].escalao
					,mrgFarm			= [MrgReg].mrgFarm
					,feeFarm			= [MrgReg].feeFarm
					,pvpmaxre			= CONVERT(NUMERIC(13,3), ISNULL(fprod.pvpmaxre, 0))
					,pv					= st.epv1 / CASE WHEN [taxa].[Percentagem] = 0 THEN 1 ELSE [taxa].[Percentagem] END
					,mbspvp				= (1-(st.epcpond / 
															CASE WHEN (st.epv1 / [taxa].[Percentagem]) = 0
																	THEN 1 
																	ELSE (st.epv1 / [taxa].[Percentagem])
															END ))*100
					,fprod.grphmgcode
					,qttcli				= ISNULL(sa.rescli,st.qttcli)
					,qttfor				= ISNULL(sa.resfor,st.qttfor)
					,stmax
					,st.ptoenc
					,lote				= isnull(st_lotes.lote,'')
					,PVPLote			= isnull(st_lotes_precos.epv1,0)
					,AltPVPLote			= isnull(convert(varchar,st_lotes_precos.usrdata,102),'1900.01.01')
					,HoraAltPVPLote		= isnull(st_lotes_precos.usrhora,'')
					,atualizaPVPFicha	= CONVERT(bit,0)
					,DataAltPVPFicha	= getdate()
					,cnpem = isnull(fprod.cnpem,'')
					,empresa.site
					,mbspvpf			= (1-(st.epcpond / 
															CASE WHEN (st.epv1 / [taxa].[Percentagem]) = 0
																	THEN 1 
																	ELSE (st.epv1 / [taxa].[Percentagem])
															END ))*100
					,marg2F = case when st.epcusto=0 then 0 else (1-(st.epcult/st.epcusto))*100 end
				into 
					#dadosST2
				from
					st(nolock)
					inner join empresa(nolock) on empresa.no = st.site_nr
					LEFT JOIN sa(nolock) ON sa.ref = st.ref AND sa.armazem = @armazem
					INNER JOIN bi(nolock) ON bi.ref = st.ref 
					INNER JOIN bo (nolock)ON bo.bostamp = bi.bostamp
					LEFT JOIN fprod(nolock) ON fprod.cnp=st.ref
					LEFT JOIN taxasiva (nolock)ON taxasiva.codigo=st.tabiva
					left join st_lotes (nolock) on st.ref = st_lotes.ref and st_lotes.site = @site
					left join st_lotes_precos (nolock) on st_lotes_precos.stampLote = st_lotes.stamp
					OUTER APPLY (
						SELECT TOP 1
							ISNULL(escalao,0) AS escalao,
							ISNULL(mrgFarm,0) AS mrgFarm,
							ISNULL(feeFarm,0) AS feeFarm
						FROM 
							[dbo].[b_mrgRegressivas]
						WHERE 
							pvporig BETWEEN PVPmin AND PVPmax 
						ORDER BY 
							versao DESC
					) as [MrgReg]

					CROSS APPLY (
						SELECT (ISNULL(taxasiva.taxa,0) / 100) + 1 as [Percentagem]
					) AS [taxa]
				WHERE
					(
						st.ref = CASE WHEN @ref = '' THEN st.ref ELSE @ref END
						OR
						st.design LIKE CASE WHEN @ref = '' THEN st.design ELSE '%'+@ref+'%' END
					)
					AND bo.bostamp = @fostamp and bi.qtt > 0
					AND st.faminome = CASE WHEN @familia='' THEN st.faminome ELSE @familia END
					AND st.inactivo = 0
					and isnull(st_lotes.lote,'') = case when @lote = '' then isnull(st_lotes.lote,'') else @lote end
					and empresa.site = @site
					and st.site_nr = (select no from empresa (nolock) where site=@site)
	
				if @stock != -999999
				begin
	
				if @operadorlogico = '>'
					delete from #dadosST2 where stock <= @stock
				if @operadorlogico = '<'
					delete from #dadosST2 where stock >= @stock
				if @operadorlogico = '='
					delete from #dadosST2 where stock != @stock
				end

				if @pvpInvalido = 1
				begin
					delete from #dadosST2 where pvpf in (select hp.preco from hist_precos(nolock) hp where ((hp.id_preco=1 or hp.id_preco=602) and hp.ativo=1) and hp.ref=#dadosST2.ref) or pvpdic = 0
				end
				SELECT *
				FROM
				(
					Select 
						top (@top)
						sel = CONVERT(BIT,0)
						,* 
					from 
						#dadosST2 st	
					order by
						st.design, isnull(lote,''),isnull(convert(varchar,AltPVPLote,102),'1900.01.01') desc,isnull(HoraAltPVPLote,'') desc
				)
				as X
				where
					1 = (case 
						when @tipo = 0 then 1
						when @tipo = 1 then (case when pvpdic > 0 then 1 else 0 end)
						when @tipo = 2 then (case when pvpdic = 0 then 1 else 0 end)
						else 0
					end)

			END

	END

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST'))
	DROP TABLE #dadosST
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST1'))
	DROP TABLE #dadosST1
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST2'))
	DROP TABLE #dadosST2
	
GO
GRANT EXECUTE on dbo.up_stocks_conferenciaPvps TO PUBLIC
GRANT Control on dbo.up_stocks_conferenciaPvps TO PUBLIC
GO