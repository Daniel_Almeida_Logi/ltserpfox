-- DESIGN  --
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_design]') IS NOT NULL
	drop procedure dbo.up_stocks_design
go

create procedure dbo.up_stocks_design
	@ref varchar(254)
	,@site_nr tinyint

/* WITH ENCRYPTION */
AS

	SET NOCOUNT ON

	SELECT 
		st.design
	FROM 
		ST (nolock) 
	WHERE 
		ST.REF = @REF
		and st.site_nr = @site_nr

GO
Grant Execute On dbo.up_stocks_design to Public
Grant Control On dbo.up_stocks_design to Public
GO
