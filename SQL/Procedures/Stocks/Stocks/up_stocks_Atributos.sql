-- Listagem de Atributos 1; --
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_Atributos]') IS NOT NULL
	drop procedure dbo.up_stocks_Atributos
go

create procedure dbo.up_stocks_Atributos

/* WITH ENCRYPTION */
AS

	select 
		distinct CONVERT(bit,0) as escolha
		,design as Atributo
		,tipo 
	from 
		B_Atributos (nolock)

GO
Grant Execute On dbo.up_stocks_Atributos to Public
Grant Control On dbo.up_stocks_Atributos to Public
GO
