/* exec up_stocks_componentes  '123',1 */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_componentes ]') IS NOT NULL
    drop procedure up_stocks_componentes 
go

create PROCEDURE up_stocks_componentes 
@ref varchar(18)
,@site_nr as tinyint



/* WITH ENCRYPTION */
AS

	select 
		 st_componentes.ref_st
		 ,st.ref
		 ,st.design
		 ,st_componentes.qtt
	from 
		st_componentes (nolock) 
		inner join st (nolock) on st_componentes.ref = st.ref
	where
		 st_componentes.ref_st = @ref
		 and st_componentes.site_nr = @site_nr
		 and st.site_nr = @site_nr
	
	
GO
Grant Execute On up_stocks_componentes  to Public
Grant Control On up_stocks_componentes  to Public
go