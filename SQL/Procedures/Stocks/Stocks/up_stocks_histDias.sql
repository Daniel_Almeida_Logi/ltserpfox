/*  Devolve o histórico de artigos comprados por cliente

	EXEC up_stocks_histDias 234, 0, 0
*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_histDias]') IS NOT NULL
	DROP PROCEDURE [dbo].[up_stocks_histDias] ;
GO

CREATE PROCEDURE [dbo].[up_stocks_histDias]
	@no NUMERIC(10) = 234,
	@estab NUMERIC(3) = 0,
	@diffHoraria NUMERIC(6) = 0

AS

SET NOCOUNT ON

SELECT distinct
	ref,
	ISNULL((
		SELECT 
			LTRIM(DATEDIFF(DAY, MAX(fft.fdata), GETDATE()))
		FROM 
			ft(nolock) as fft
			join fi(nolock) as ffi on fft.ftstamp = ffi.ftstamp
		WHERE
			ffi.ref = fi.ref	
			and fft.no =  @no
			and fft.estab = @estab		
	), '') as lastBuy
from
	fi (nolock) 
	inner join ft (nolock) on fi.ftstamp = ft.ftstamp 
where
	ft.no = @no and ft.estab = @estab
	and ftano > YEAR(dateadd(HOUR, @diffHoraria, getdate()))-3
	and fi.ref != ''

GO
GRANT EXECUTE on dbo.up_stocks_histDias TO PUBLIC
GRANT Control on dbo.up_stocks_histDias TO PUBLIC
GO 