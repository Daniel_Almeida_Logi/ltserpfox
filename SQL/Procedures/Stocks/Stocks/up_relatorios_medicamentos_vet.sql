/*	Relat�rio de medicamentos veterin�rios

	 exec up_relatorios_medicamentos_vet '20100815', '20301115', 'sa�das', '','','Loja 1'
	 exec up_relatorios_medicamentos_vet '20100815', '20301115', 'todos', '','','Loja 1'
	 exec up_relatorios_medicamentos_vet '20100815', '20301115', 'entradas', '','','Loja 1'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


IF OBJECT_ID('[dbo].[up_relatorios_medicamentos_vet]') IS NOT NULL
	DROP PROCEDURE dbo.up_relatorios_medicamentos_vet
GO

CREATE PROCEDURE dbo.up_relatorios_medicamentos_vet

@dataIni datetime,
@dataFim datetime,
@movimento varchar(18),
@ref varchar(18),
@lote varchar(30),
@SITE varchar(18)

AS

IF OBJECT_ID('tempdb.dbo.#TempMovimentos') IS NOT NULL
	DROP TABLE #TempMovimentos

DECLARE @sitenr int = 0 
SELECT @sitenr = no FROM empresa (nolock)
WHERE site = @SITE

SELECT
	datahora = CONVERT(varchar, sl.datalc, 111),
	st.ref, 
	st.design, 
	sl.lote,
	validade = CASE 
                   WHEN sl.fistamp != '' THEN CONVERT(varchar, fi2.dataValidade, 111)
                   WHEN sl.bistamp != '' THEN '1900/01/01'
                   WHEN sl.fnstamp != '' THEN '1900/01/01'
                   ELSE '1900/01/01'
               END,

	ENTRADAS = CASE 
					WHEN sl.cm < 50 THEN 
						CASE WHEN sl.qtt < 0 THEN 0 ELSE CAST(sl.qtt AS int) END
					ELSE 
						CASE WHEN sl.qtt < 0 AND CAST(sl.cm AS int) > 50 THEN CAST(sl.qtt * -1 AS int) ELSE ROUND(0, 0) END
			   END,

	SAIDAS = CASE 
					WHEN sl.cm > 50 AND sl.qtt > 0 THEN CAST(sl.qtt AS int)
					ELSE 
						CASE WHEN sl.cm < 50 AND sl.qtt < 0 THEN CAST(sl.qtt AS int) ELSE ROUND(0, 0) END
				END,

	sl.nome, 
	sl.adoc,

	receita = CASE 
                   WHEN sl.fistamp != '' THEN ft2.u_receita
                   WHEN sl.bistamp != '' THEN ''
                   WHEN sl.fnstamp != '' THEN ''
                   ELSE ''
               END

	

INTO #TempMovimentos
FROM sl (nolock)
INNER JOIN st ON st.ref = sl.ref 
	AND sl.armazem IN (SELECT armazem FROM empresa_arm (nolock) WHERE empresa_no = @sitenr)

	
left join fi(nolock) on fi.fistamp = sl.fistamp and sl.fistamp != ''
left join fi2(nolock) on fi2.fistamp = sl.fistamp and sl.fistamp != '' 
left join ft2(nolock) on ft2.ft2stamp = fi.ftstamp and sl.fistamp != ''
left join bi(nolock )on bi.bistamp = sl.bistamp and sl.bistamp != ''
left join fn(nolock) on fn.fnstamp = sl.fnstamp and sl.fnstamp != ''


WHERE st.familia IN (7, 97, 98) and st.faminome like '%Veterin�ria%' -- medicamentos veterin�rios
	AND sl.datalc BETWEEN @dataIni AND @dataFim AND (@ref = '' OR sl.ref = @ref) AND (@lote = '' OR sl.lote = @lote) 
                   
	print @movimento

IF lower(@movimento)  = 'entradas' 
BEGIN
	SELECT *, qtt = CAST(abs(ENTRADAS) AS int)  FROM #TempMovimentos WHERE ENTRADAS > 0
	ORDER BY datahora DESC		
END
ELSE IF lower(@movimento) = 'sa�das'
BEGIN
	SELECT *, qtt = CAST(SAIDAS AS int) FROM #TempMovimentos WHERE SAIDAS > 0
	ORDER BY datahora DESC
END
ELSE
BEGIN
	SELECT *, 
	qtt = Entradas  + (Saidas)* -1
	FROM #TempMovimentos
	ORDER BY datahora DESC
END

IF OBJECT_ID('tempdb.dbo.#TempMovimentos') IS NOT NULL
	DROP TABLE #TempMovimentos

GO

GRANT EXECUTE ON dbo.up_relatorios_medicamentos_vet TO Public
GRANT CONTROL ON dbo.up_relatorios_medicamentos_vet TO Public
GO
