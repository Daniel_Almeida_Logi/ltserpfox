/*Lista Grupos Homogeneo com informa��o de stock actual e Vendas Ult Mes*/
/* exec up_stocks_GprHomInfo 1   */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_GprHomInfo]') IS NOT NULL
    drop procedure dbo.up_stocks_GprHomInfo
go

create procedure dbo.up_stocks_GprHomInfo
@site_nr tinyint

/* WITH ENCRYPTION */
AS

	If OBJECT_ID('tempdb.dbo.#dadosSt') IS NOT NULL
		drop table #dadosSt
	If OBJECT_ID('tempdb.dbo.#dadosFt') IS NOT NULL
		drop table #dadosFt
		
	Select 
		st.ref 
		,st.stock
	into
		#dadosSt
	From
		st (nolock)
	where
		st.site_nr = @site_nr
	
	
	Select	
		ref
		,qtt = SUM(case when ft.tipodoc!=3 then fi.qtt else -fi.qtt end) 
	into
		#dadosFt
	FROM	
		FI (nolock)
		INNER JOIN FT (nolock) ON FI.FTSTAMP = FT.FTSTAMP
		INNER JOIN TD (nolock) ON TD.NDOC = FT.NDOC
	WHERE	
		ref != ''
		AND td.tipodoc != 4 /* Exclui documentos que n�o s�o integrados na contabilidade */
		AND fi.composto=0 
		AND ft.anulado=0
		/* Filtros  */
		AND ft.fdata between DATEADD(mm,-1,getdate()) And getdate()
	GROUP BY
		ref
	


	Select 
		distinct sel		= CONVERT(bit,0), 
		'Cod'				= grphmgcode, 
		'Desc'				= grphmgdescr,
		'Stock_GH'			= sum(stock),
		'Vendas_Ult_Mes'	= sum(isnull(#dadosFt.qtt,0))
	from 
		fprod (nolock)
		inner join #dadosSt on #dadosSt.ref = fprod.cnp
		left join #dadosFt on fprod.cnp = #dadosFt.ref
	where 
		grphmgcode != '' 
	Group by
		grphmgcode, grphmgdescr
	order by 
		grphmgcode

	If OBJECT_ID('tempdb.dbo.#dadosSt') IS NOT NULL
		drop table #dadosSt
	If OBJECT_ID('tempdb.dbo.#dadosFt') IS NOT NULL
		drop table #dadosFt

GO
Grant Execute on dbo.up_stocks_GprHomInfo to Public
Grant Control on dbo.up_stocks_GprHomInfo to Public
go
