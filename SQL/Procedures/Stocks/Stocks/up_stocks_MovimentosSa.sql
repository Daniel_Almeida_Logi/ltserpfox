/* Movimentos por Armazém 

	 exec up_stocks_MovimentosSa '5440987', 'Loja 1', '16', 'Supervisores'
	 exec up_stocks_MovimentosSa '000001', 'Loja 1', '1', 'Supervisores'
	 exec up_stocks_MovimentosSa '5440987', '', '1', 'Administradores',0
	 exec up_stocks_MovimentosSa '8168617', 'Loja 1', '1', 'Administradores',0
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_MovimentosSa]') IS NOT NULL
	drop procedure dbo.up_stocks_MovimentosSa
go

create procedure dbo.up_stocks_MovimentosSa
	@ref			VARCHAR (30)
	,@site			VARCHAR (60)
	,@userno		INT
	,@grupo			VARCHAR(60)
	,@permVisReport BIT			= 1

/* WITH ENCRYPTION */
AS
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#Empresas'))
		DROP TABLE #Empresas
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#armazens'))
		DROP TABLE #armazens
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempPfu'))
		DROP TABLE #tempPfu
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempPfg'))
		DROP TABLE #tempPfg

	CREATE TABLE #Empresas(
		site_nr VARCHAR(55)
	)

	DECLARE @resume		VARCHAR(254)
	IF(@permVisReport = 1)
	BEGIN
		SET @resume  = 'Relatório Extrato de Stock por Armazém'	
	END
	ELSE
	BEGIN
		SET @resume  = 'Gestão de stock - Visualizar stock armazém'
	END 

	DECLARE @stkcaixa AS BIT
	SET @stkcaixa = (SELECT bool FROM B_Parameters_site WHERE stamp='ADM0000000067' and site=@site)

	DECLARE @stcativoretira AS BIT
	SET @stcativoretira = (SELECT bool FROM B_Parameters_site WHERE stamp='ADM0000000098' and site=@site)
	
	DECLARE @userSiteDefault VARCHAR(60)
	SET @userSiteDefault = (ISNULL((SELECT loja FROM b_us WHERE userno = @userno),'Loja 1'))
			
	SELECT 
		b_pfu.*
	INTO
		#tempPfu
	FROM
		b_pf
		inner join b_pfu on b_pfu.pfstamp = b_pf.pfstamp
	WHERE
		b_pf.resumo  in( @resume)
		and	ISNULL(b_pfu.userno,9999) = CASE WHEN @userno=0 THEN  b_pfu.userno ELSE @userno END
				
	SELECT 
		b_pfg.*
	INTO
		#tempPfg
	FROM
		b_pf
		inner join b_pfg on b_pfg.pfstamp = b_pf.pfstamp
	WHERE
		b_pf.resumo  in(@resume)
		AND ISNULL(b_pfg.nome,'') = CASE WHEN @grupo='' THEN b_pfg.nome ELSE @grupo END

	--SELECT * FROM #tempPfu
	--SELECT * FROM #tempPfg

	IF(@permVisReport = 0)
	BEGIN
		INSERT INTO #Empresas(site_nr)
		SELECT  DISTINCT
			empresa.no
		FROM empresa 
		WHERE 
		(empresa.site  IN ( SELECT RTRIM(LTRIM(pfu.site)) FROM #tempPfu pfu )
		OR empresa.site  IN ( SELECT RTRIM(LTRIM(pfg.site)) FROM #tempPfg pfg ))
		OR empresa.site = CASE WHEN @stkcaixa=0 THEN @userSiteDefault ELSE  empresa.site END /*Tem sempre acesso à loja default do utilizador*/

	END
	ELSE
	BEGIN 
		INSERT INTO #Empresas(site_nr)
		SELECT  DISTINCT
			 empresa.no
		FROM 
			empresa 
		WHERE 
		empresa.site NOT IN ( SELECT RTRIM(LTRIM(pfu.site)) FROM #tempPfu pfu )
		AND empresa.site NOT IN ( SELECT RTRIM(LTRIM(pfg.site)) FROM #tempPfg pfg )

	END 
	 
	SELECT
		armazem,
		empresa_no
	INTO 
		#armazens
	FROM
		empresa_arm
		INNER JOIN #Empresas ON empresa_arm.empresa_no = #empresas.site_nr
	
	SELECT DISTINCT 
		ARMAZEM			= sa.armazem
		,EMPRESA		= empresa.nomecomp
		,LOJA			= empresa.site
		,STOCK			= CASE WHEN @stcativoretira=0 THEN ISNULL(sa.stock,0)
									ELSE ISNULL(sa.stock-sa.cativado,0) END 
		,EPCPOND		= ROUND(st.epcpond,2)
		,EPCPONDTOTAL	= ROUND(st.epcpond * st.stock,2)
		,RESFOR			= sa.resfor
		,RESCLI			= sa.rescli
		,stocklamina	= CASE WHEN @stcativoretira=0 THEN  
							CASE WHEN st.qttminvd=0 THEN 0 
								 ELSE CAST(ROUND(ISNULL(sa.stock,0) / st.qttminvd,2) AS NUMERIC(10,2)) END 
						ELSE
							CASE WHEN st.qttminvd=0 THEN 0 
								 ELSE CAST(ROUND(ISNULL(sa.stock-sa.cativado,0) / st.qttminvd,2) AS NUMERIC(10,2)) END 
						END
		,stockcaixa		= CASE WHEN @stcativoretira=0 THEN 
							CASE WHEN st.qttembal=0 THEN 0 
								 ELSE CAST(ROUND(ISNULL(sa.stock,0) / st.qttembal, 2) AS NUMERIC(10,2)) END
						ELSE
							CASE WHEN st.qttembal=0 THEN 0 
								 ELSE CAST(ROUND(ISNULL(sa.stock-sa.cativado,0) / st.qttembal, 2) AS NUMERIC(10,2)) END
						END
		, st.epv1
		, st.stmax
		, st.ptoenc
		, CONVERT(VARCHAR,st.validade,23) AS validade
		, diasmov = DATEDIFF(day,st.usaid, getdate())
						
	FROM
		sa
		INNER JOIN #armazens			ON sa.armazem = #armazens.armazem
		INNER JOIN empresa (NOLOCK)		ON empresa.no = #armazens.empresa_no
		INNER JOIN st (NOLOCK)			ON sa.ref = st.ref AND st.site_nr = #armazens.empresa_no
	WHERE
		st.ref = @ref
		AND sa.armazem IN (SELECT armazem FROM #armazens)
	GROUP by
		sa.armazem, sa.stock, sa.cativado, st.stock, st.epcpond, sa.resfor, sa.rescli,
		empresa.nomecomp, empresa.site, st.qttminvd, st.qttembal, st.epv1 , st.stmax, st.ptoenc, st.validade, sa.usrdata, st.usaid
	ORDER BY
		empresa.site,sa.armazem

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#Empresas'))
		DROP TABLE #Empresas
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#armazens'))
		DROP TABLE #armazens
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempPfu'))
		DROP TABLE #tempPfu
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempPfg'))
		DROP TABLE #tempPfg

GO
Grant Execute on dbo.up_stocks_MovimentosSa to Public
Grant Control on dbo.up_stocks_MovimentosSa to Public
GO
