-- Insere Atributos 3
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_atributosInsere]') IS NOT NULL
	drop procedure dbo.up_stocks_atributosInsere
go

create procedure dbo.up_stocks_atributosInsere

@ref varchar(254),
@tipo varchar(254),
@atributo varchar(254)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

INSERT INTO B_STATRIB (atStamp,REF, TIPO, DESCR)
VALUES ('ADM'+ LEFT(NEWID(),22),@ref,@tipo,@atributo)					                                    

GO
Grant Execute On dbo.up_stocks_atributosInsere to Public
Grant Control On dbo.up_stocks_atributosInsere to Public
GO
