/*
Pesquisa de artigos
up_stocks_criaSt '7021535', 1
up_stocks_criaSt '1000067', 0, 'Loja 1', 1



** Retorna produtos sem ficha **
select fprod.cnp, st.ref from fprod(nolock)
left JOIN st ON st.ref=fprod.cnp
WHERE st.ref is null


alterada em 20200918 JG para contemplar as tabelas do HMR 

select *from fprod(nolock) where ref='54409'

delete from st where ref='5299102'


select *from b_parameters_site where stamp='ADM0000000050'

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_stocks_criaSt]') IS NOT NULL
    DROP procedure dbo.up_stocks_criaSt
GO

CREATE procedure dbo.up_stocks_criaSt
	@ref as varchar(18)
	,@site_nr tinyint
	,@site varchar(18) = ''
	,@result tinyint = 0

	
/* WITH ENCRYPTION */
AS
	set @result = isnull(@result,0)
	set @site   = rtrim(ltrim(isnull(@site,'')))

	if(@site!='')
		SELECT @site_nr=no from empresa(nolock) where site = @site




	if not exists (select ref from st where ref = @ref and site_nr = @site_nr)
	begin 
		declare @tipoCliente as varchar(50)
		declare @nrForn as numeric(6,0) = 0
		declare @nrFornPref as numeric(6,0) = 0
		declare @estabForn as numeric(3,0) = 0
		declare @nomeForn as varchar(255) = ''
		declare @temFornPref as bit = 0

		set @tipoCliente = UPPER((select top 1 textValue from B_Parameters where stamp = 'ADM0000000082'))

		/* Valida se o parametro est� activo de fornecedor preferencial est� activo*/
		select 
			@nrFornPref=ISNULL(CONVERT(int,numValue), 0)
			,@temFornPref = bool
		 from B_Parameters where stamp = 'ADM0000000272'


		 /* retorna dados fornecedor preferencial*/
		 IF(@temFornPref = 1) AND (select inactivo from fl(nolock) where fl.no = @nrFornPref and estab = 0) = 0
		 BEGIN
			select 
				@nomeForn=nome
				,@nrForn=no
				,@estabForn=estab
			from 
				fl (nolock) 
			where 
				fl.no = @nrFornPref
				and estab = 0
		END



		insert into st (
			site_nr
			,ref
			,design
			,tabiva
			,familia
			,faminome
			,usr1

			,u_depstamp
			,u_secstamp
			,u_catstamp
			,u_segstamp

			,u_famstamp
			,u_lab
			,u_fonte
			,epv1
			,u_impetiq
			,ivaincl
			,ivapcincl
			,iva1incl
			,iva2incl
			,iva3incl
			,iva4incl
			,iva5incl
			,ststamp
			,fornec 
			,fornecedor
			,fornestab
			,u_tipoetiq
			,designOnline
			,caractOnline
			,apresentOnline
			,usrhora

			,codmotiseimp
			,motiseimp
		)
		select 
			top 1 
			site_nr = @site_nr
			,ref = @ref
			,design = fprod.design
			,tabiva = fprod.u_tabiva
			,familia = fprod.u_familia
			,faminome = case when stfami.nome is null then '' else stfami.nome end
			,usr1 = fprod.u_marca

			--,u_depstamp = fprod.depstamp
			--,u_secstamp = fprod.secstamp
			--,u_catstamp = fprod.catstamp
			--,u_segstamp = fprod.catstamp			
			,u_depstamp = convert(varchar(4),fprod.id_grande_mercado_hmr)
			,u_secstamp = convert(varchar(4),fprod.id_mercado_hmr)
			,u_catstamp = convert(varchar(4),fprod.id_categoria_hmr)
			,u_segstamp = convert(varchar(4),fprod.id_segmento_hmr)

			,u_famstamp = fprod.famstamp
			,u_lab = fprod.titaimdescr
			,u_fonte = 'D'
			,epv1 = fprod.pvporig
			,u_impetiq =  case when fprod.pvporig > 0 then 1 else 0 end
			,ivaincl = case when @tipocliente = 'FARMACIA' or @tipocliente = '' then 1 else 0 end
			,ivapcincl = case when @tipocliente = 'FARMACIA' or @tipocliente = '' then 1 else 0 end
			,iva1incl = case when @tipocliente = 'FARMACIA' or @tipocliente = '' then 1 else 0 end
			,iva2incl = case when @tipocliente = 'FARMACIA' or @tipocliente = '' then 1 else 0 end
			,iva3incl = case when @tipocliente = 'FARMACIA' or @tipocliente = '' then 1 else 0 end
			,iva4incl = case when @tipocliente = 'FARMACIA' or @tipocliente = '' then 1 else 0 end
			,iva5incl = case when @tipocliente = 'FARMACIA' or @tipocliente = '' then 1 else 0 end
			,ststamp = LEFT(NEWID(),25)
			,@nrForn
			,@nomeForn
			,@estabForn
			,u_tipoetiq=1
			,convert(varchar(100),LTRIM(RTRIM(ISNULL(fprod.designOnline,'')))) 
			,convert(varchar(4000),LTRIM(RTRIM(ISNULL(fprod.caractOnline,'')))) 
			,convert(varchar(4000),LTRIM(RTRIM(ISNULL(fprod.apresentOnline,'')))) 
			,convert(varchar, getdate(),108)

			,codmotiseimp	= 
			CASE WHEN (select TOP 1 taxa from taxasiva(nolock) INNER JOIN fprod(nolock) 
					on fprod.u_tabiva = taxasiva.codigo where fprod.cnp = @ref) > 0 
						THEN '' 
			ELSE (select codmotiseimp from empresa(nolock) where no = @site_nr) END
			,motiseimp		= 
					CASE WHEN (select TOP 1 taxa from taxasiva(nolock) INNER JOIN fprod(nolock) 
								on fprod.u_tabiva = taxasiva.codigo where fprod.cnp = @ref) >0 
								THEN '' 
									ELSE (select motivo_isencao_iva from empresa(nolock) where no = @site_nr) END
		from 
			fprod (nolock)
			left join stfami (nolock) on stfami.ref = fprod.u_familia
		where 
			fprod.cnp = @ref
	end


	if(@result=1)
	begin
		declare @count int = 0
		select @count = count(ref) from st(nolock) where ref = @ref and site_nr = @site_nr

		if(@count>0)
			select getdate() AS DateMessag, 1 AS StatMessag, 1 AS ActionMessage, 'New Product' As DescMessag
		else
			select getdate() AS DateMessag, 0 AS StatMessag, 0 AS ActionMessage, 'No product' As DescMessag
	end
GO
GRANT EXECUTE on dbo.up_stocks_criaSt TO PUBLIC
GRANT Control on dbo.up_stocks_criaSt TO PUBLIC
GO
