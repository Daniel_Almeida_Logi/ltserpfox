-- Elimina Atributo; --
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_atributosElimina]') IS NOT NULL
	drop procedure dbo.up_stocks_atributosElimina
go

create procedure dbo.up_stocks_atributosElimina

@ref varchar(254),
@tipo varchar(254),
@atributo varchar(254)

/* WITH ENCRYPTION */
AS

	DELETE FROM B_STATRIB WHERE REF = @ref AND TIPO = @tipo AND DESCR = @atributo                               

GO
Grant Execute On dbo.up_stocks_atributosElimina to Public
Grant Control On dbo.up_stocks_atributosElimina to Public
GO
