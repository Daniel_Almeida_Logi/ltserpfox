/* Produtos Mesmo grupo Homogeneo e que existem na ST*/
/* exec up_stocks_GprHom '8128306',1 */
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_GprHom]') IS NOT NULL
    drop procedure dbo.up_stocks_GprHom
go

create procedure dbo.up_stocks_GprHom
	@ref as char(18)
	,@site_nr as tinyint

/* WITH ENCRYPTION */
AS

	;With cteST as (
		Select 
			grphmgcode 
		from
			fprod (nolock)
		Where
			ref = @ref
	)
	
	Select 
		ref			= st.ref
		,design		= st.design
		,stock		= st.stock
		,pvdic		= case when pvpcalc=0 then pvporig else pvpcalc end
		,grphmgcode
		,grphmgdescr
		,t45 =(case 
					when fprod.pvporig <= fprod.preco_acordo and fprod.preco_acordo > 0 
						then 'T4'
					when ranking=1 and grphmgcode != 'GH0000' and grphmgcode != '' and fprod.pvporig <= fprod.pvpmaxre 
						then 'T5' 
					else ''
				end)
	from
		st (nolock)
		inner join fprod  (nolock) on st.ref=fprod.cnp
	where 
		st.site_nr = @site_nr
		and fprod.grphmgcode = isnull((Select top 1 grphmgcode from cteST),'')
	order by 
		ref

GO
Grant Execute on dbo.up_stocks_GprHom to Public
Grant Control on dbo.up_stocks_GprHom to Public
go
