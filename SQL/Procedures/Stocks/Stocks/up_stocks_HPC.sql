/*
	exec up_stocks_HPC 1, '5440987', 1
	exec up_stocks_HPC 0,'5440987', -1

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_HPC]') IS NOT NULL
	drop procedure dbo.up_stocks_HPC
go

create procedure dbo.up_stocks_HPC
@armazem numeric(5,0),
@ref char(18),
@site_nr int

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	If OBJECT_ID('tempdb.dbo.#tempSL') IS NOT NULL
		drop table #tempSL
	If OBJECT_ID('tempdb.dbo.#tempTop') IS NOT NULL
		drop table #tempTop
	If OBJECT_ID('tempdb.dbo.#Armazens') IS NOT NULL
		drop table #Armazens 
		
		
	select
		distinct armazem
	into
		#Armazens
	From
		empresa
		inner join empresa_arm on empresa.no = empresa_arm.empresa_no
	Where
		empresa.no = case when @site_nr = -1 then empresa.no else @site_nr end
		
	select 
		sl.nome
		,'id' = ROW_NUMBER() over (partition by sl.nome order by sl.datalc desc)
		,sl.evu
		,sl.datalc
		,sl.adoc
		,sl.cmdesc
		,sl.qtt
	into 
		#tempSL
	from 
		sl (nolock)
	where
		sl.ref = @ref
		and sl.armazem in (select armazem from #Armazens)
		and sl.cm < 50
	

	select
		nome
		,evu
		,datalc
		,adoc
		,cmdesc
		,qtt
	into
		#tempTop
	from 
		#tempSL
	where
		id <= 5

	
	SELECT *
	FROM (
		select
			nome
			,[evu] = convert(varchar,convert(decimal(16,2), evu))
	
			,cmdesc
			,adoc
			,[datalc] = convert(date,datalc)
			,qtt
		from 
			#tempTop
	/*
		UNION All

		SELECT 
			nome,
			[evu] = convert(varchar,convert(decimal(16,2), fn.epv)),
			fo.docnome as cmdesc,
			fo.adoc,
			MAX(docdata) as DataLC,
			0 as qtt
		FROM 
			fo(nolock)
			join fn(nolock) on fo.fostamp = fn.fostamp
		where
			1=1
			and fn.ref = @ref
			and fn.qtt = 0
			and fo.doccode IN (55,101,102)
			and not exists (select 
								1 
							from 
								fo(nolock) as ffo 
								join fn(nolock) as ffn on ffo.fostamp = ffn.fostamp
							where
								ffn.ref = fn.ref
								and ffo.no = fo.no
								and ffo.doccode IN (55,101,102)
								and DATEDIFF(DAY,fo.doccode, ffo.doccode) > 0
								)
		GROUP BY fo.no, fo.nome, fn.epv, fo.docnome, fo.adoc*/
	) AS X
	ORDER BY convert(date,X.datalc) desc
		
	If OBJECT_ID('tempdb.dbo.#tempSL') IS NOT NULL
		drop table #tempSL
	If OBJECT_ID('tempdb.dbo.#tempTop') IS NOT NULL
		drop table #tempTop

GO
Grant Execute On dbo.up_stocks_HPC to Public
Grant Control On dbo.up_stocks_HPC to Public
GO