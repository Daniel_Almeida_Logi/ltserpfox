/* stocks por Armazém 

	 exec up_stocks_Sa_central '5440987'
	  exec up_stocks_CorrigeSA_global 

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_Sa_central]') IS NOT NULL
	drop procedure dbo.up_stocks_Sa_central
go

create procedure dbo.up_stocks_Sa_central
	@ref varchar (30)
	

/* WITH ENCRYPTION */
AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#armazens'))
		DROP TABLE #armazens
	
	declare @stcativoretira as bit
	set @stcativoretira = (select top 1 bool from B_Parameters_site where stamp='ADM0000000098')

	Select
		armazem
	into 
		#armazens
	From
		empresa_arm

	--	
	SELECT
		distinct 
		ARMAZEM			= sa.armazem
		,EMPRESA		= empresa.nomecomp
		,LOJA			= empresa.site
		,STOCK			= case when @stcativoretira=0 then 
							ISNULL(sa.stock,0)
						else
							ISNULL(sa.stock-sa.cativado,0)
						end 
		,EPCPOND		= ROUND(st.epcpond,2)
		,EPCPONDTOTAL	= ROUND(st.epcpond * st.stock,2)
		,RESFOR			= sa.resfor
		,RESCLI			= sa.rescli
		,stocklamina	= case when @stcativoretira=0 then  
							case when st.qttminvd=0 then 0 else cast(round(ISNULL(sa.stock,0) / st.qttminvd,2) as numeric(10,2)) end 
						else
							case when st.qttminvd=0 then 0 else cast(round(ISNULL(sa.stock-sa.cativado,0) / st.qttminvd,2) as numeric(10,2)) end 
						end
		,stockcaixa		= case when @stcativoretira=0 then 
							case when st.qttembal=0 then 0 else cast(round(ISNULL(sa.stock,0) / st.qttembal, 2) as numeric(10,2)) end
						else
							case when st.qttembal=0 then 0 else cast(round(ISNULL(sa.stock-sa.cativado,0) / st.qttembal, 2) as numeric(10,2)) end
						end
		, st.epv1
		, st.stmax
		, st.ptoenc
		, convert(varchar,st.validade,23) as validade
	FROM
		sa
		inner join empresa_arm on sa.armazem = empresa_arm.armazem
		inner join empresa on empresa.no = empresa_arm.empresa_no
		inner join st (nolock) on sa.ref = st.ref AND st.site_nr = empresa.no
	WHERE
		st.ref = @ref
		and sa.armazem in (select armazem from #armazens)
	GROUP by
		sa.armazem, sa.stock, sa.cativado, st.stock, st.epcpond, sa.resfor, sa.rescli,
		empresa.nomecomp, empresa.site, st.qttminvd, st.qttembal, st.epv1 , st.stmax , st.ptoenc, st.validade
	ORDER BY
		sa.armazem

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#armazens'))
		DROP TABLE #armazens

GO
Grant Execute on dbo.up_stocks_MovimentosSa to Public
Grant Control on dbo.up_stocks_MovimentosSa to Public
GO
