-- Pesquisa de Produtos no Dicion�rio --
/*
	exec up_stocks_pesquisaNoDic '', '', '',0
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_pesquisaNoDic]') IS NOT NULL
	drop procedure dbo.up_stocks_pesquisaNoDic
go

create procedure dbo.up_stocks_pesquisaNoDic

@ref varchar(60),
@grphmg varchar(60),
@pactivo varchar(60),
@armazem numeric(5) = 0

/* WITH ENCRYPTION */
AS

	If OBJECT_ID('tempdb.dbo.#Fprod') IS NOT NULL
		drop table #Fprod

	select 
		sel				= convert(bit,0)
		,fprod.ref
		,design			= ltrim(rtrim(fprod.design))
		,stock			= isnull(st.stock,0)
		,epv1			= isnull(fprod.pvporig,0)
		,validade		= isnull(convert(varchar,st.validade,102),'')
		,faminome		= isnull(st.faminome,'')
		,comp			= isnull( (case when (left(cptgrp.descri��o,3)!='N�o' and cptgrp.u_diploma1='' and cptgrp.u_diploma2='' and cptgrp.u_diploma3='') AND cptgrp.grupo!='ST'
										then 1
										else 0 
							   end) ,0)
		,fprod.generico, fprod.psico, fprod.benzo, fprod.u_nomerc
		,grphmg			= case when fprod.grphmgcode='GH0000' then fprod.grphmgcode
									else fprod.grphmgcode + ' - ' + fprod.grphmgdescr
								end
		,fprod.titaimdescr
		,sitcomdescr	= isnull(sitcomfpx.sitcomdescr,'')
		,pactivo			= fprod.dci
	into
		#Fprod
	from 
		fprod (nolock)
		left join st (nolock) on st.ref=fprod.cnp
		left join cptgrp (nolock) on cptgrp.grupo=fprod.grupo
		left join B_sitcomfp sitcomfpx	(nolock) on st.ref=sitcomfpx.cnp
	where 
		(fprod.cnp like @ref+'%' or fprod.design like @ref+'%')
		AND fprod.grphmgdescr like '%'+@grphmg+'%'
		and fprod.dci like '%'+@pactivo+'%'


	select 
		top 50 * 
	from
		#Fprod
	order by
		stock desc
		,design


	If OBJECT_ID('tempdb.dbo.#Fprod') IS NOT NULL
		drop table #Fprod

GO
Grant Execute On dbo.up_stocks_pesquisaNoDic to Public
Grant Control on dbo.up_stocks_pesquisaNoDic to Public
GO
