-- Verificar existencia de Tipos e Atributos 2
-- exec up_stocks_atributosExistencia '8168617', 'arte corpo', 'cabe�a'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_atributosExistencia]') IS NOT NULL
	drop procedure dbo.up_stocks_atributosExistencia
go

create procedure dbo.up_stocks_atributosExistencia

@ref varchar(254),
@tipo varchar(254),
@atributo varchar(254)


/* WITH ENCRYPTION */
AS

	SELECT
		valor = case when isnull(max(atstamp),'')='' then 0 else 1 end
	FROM
		B_STATRIB
	WHERE
		REF = @ref AND tipo = @tipo AND Descr = @atributo


GO
Grant Execute On dbo.up_stocks_atributosExistencia to Public
Grant Control On dbo.up_stocks_atributosExistencia to Public
GO
