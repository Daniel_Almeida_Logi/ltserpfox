-- Ver dados "Actuais" para as compras
-- exec up_stocks_verDadosActuaisPorArmazem '8168617',1,0,0
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if  OBJECT_ID('[dbo].[up_stocks_verDadosActuaisPorArmazem]') IS NOT NULL
	drop procedure dbo.up_stocks_verDadosActuaisPorArmazem
go

create procedure dbo.up_stocks_verDadosActuaisPorArmazem

@ref varchar(60),
@armazem numeric(4,0),
@no numeric(10),
@estab numeric(6),
@site_nr tinyint 


/* WITH ENCRYPTION */
AS

	select
		st.epv1
		,st.marg1 
		,st.validade
		,st.epcusto
		,stock = isnull(sa.stock,st.stock)
		,rescli
		,resfor
	from
		st (nolock)
		left join sa (nolock) on sa.ref=st.ref
	where
		st.ref = @ref
		and st.site_nr = @site_nr
		and sa.armazem = @armazem
	
GO
Grant Execute On dbo.up_stocks_verDadosActuaisPorArmazem to Public
Grant Control On dbo.up_stocks_verDadosActuaisPorArmazem to Public
GO
