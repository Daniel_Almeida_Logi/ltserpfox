/* SP Pesquisa de artigos na tabela de consulta de produtos a fornecedores ext_esb_products [CUIDADO COM ALTERAÇÕES, ANALISAR SEMPRE A PERFORMANCE DA QUERY]

			exec up_stocks_pesquisaESB 
			30
			,'' ,'' ,'' ,'' ,'' ,0 ,'' ,0 ,0 ,0 ,1
			,0 ,0 ,'' ,0 ,0 ,0 ,0 ,0 ,'' ,0 ,'',''
			,'ADM1B5B5405-628E-4FC3-90C'
			select * from ext_esb_products nolock where token = 'ADM1B5B5405-628E-4FC3-90C' order by date_cre desc
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_stocks_pesquisaESB]') IS NOT NULL
    DROP procedure dbo.up_stocks_pesquisaESB
GO

CREATE procedure dbo.up_stocks_pesquisaESB
	@top				INT
	,@artigo			VARCHAR(60)
	,@familia			VARCHAR(60)
	,@lab				VARCHAR(120)
	,@marca				VARCHAR(200)
	,@dci				VARCHAR(120)
	,@servicos			int = 0
	,@operadorLogico	VARCHAR(5)
	,@stock				NUMERIC(13,3)
	,@generico			int 
	,@inactivo			bit
	,@armazem			numeric(5)
	,@servMarc			bit
	,@atendimento		bit = 0
	,@recurso			VARCHAR(120) = ''
	,@site_nr			tinyint
	,@parafarmacia		bit 
	,@comFicha			bit = 0
	,@t5				bit = 0
	,@txIva				numeric(5,2) = -1
	,@fornecedor		varchar(80) = ''
	,@diassmov			int = 0
	,@class2			varchar(max) = ''
	,@tipoProduto		varchar(254) 
	,@esbToken			varchar(35)
	
/* WITH ENCRYPTION */
AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosArmazens'))
		DROP TABLE #dadosArmazens	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt2'))
		DROP TABLE #dadosSt2	
	
	/* variaveis auxiliares */		
	declare @familiaNo as varchar(18)
	set @familiaNo = isnull((Select top 1 stfami.ref from stfami (nolock) where stfami.nome = @familia),'')
				
	Declare @refExcluir varchar(200)
	if @atendimento = 1 and (select top 1 bool from B_Parameters where stamp='ADM0000000219') = 1
		set @refExcluir = (select top 1 rtrim(ltrim(textValue)) from B_Parameters where stamp = 'ADM0000000219')
	else
		set @refExcluir = char(39)+'vazio'+CHAR(39)

	-- tipo produto
	declare @tipoProdutoID as varchar(4)
	set @tipoProdutoID = (select famstamp from b_famFamilias (nolock) where upper(rtrim(ltrim(design))) = upper(@tipoProduto))

	-- classificacao 2
	declare @Class2departamento as varchar(254)
	set @Class2departamento = isnull((select items from dbo.up_SplitToTableCId(@class2,';') where id = 1),'')
	declare @Class2departamentoID as varchar(4)
	set @Class2departamentoID = (select depstamp from b_famDepartamentos (nolock) where upper(rtrim(ltrim(design))) = upper(ltrim(rtrim(@Class2departamento))))

	--
	declare @Class2seccao as varchar(254)
	set @Class2seccao = isnull((select items from dbo.up_SplitToTableCId(@class2,';') where id = 2),'')
	declare @Class2seccaoID  as varchar(4)
	set @Class2seccaoID = (select secstamp from b_famSeccoes (nolock) where upper(rtrim(ltrim(design))) = upper(ltrim(rtrim(@Class2seccao))) and depstamp = @Class2departamentoID)

	--
	declare @Class2categoria as varchar(254)
	set @Class2categoria = isnull((select items from dbo.up_SplitToTableCId(@class2,';') where id = 3),'')
	declare @Class2categoriaID as varchar(4)
	set @Class2categoriaID = (select catstamp from b_famCategorias (nolock) where upper(rtrim(ltrim(design))) = upper(ltrim(rtrim(@Class2categoria))) and secstamp = @Class2seccaoID)


	/* Armazens disponiveis por empresa */
	select 
		empresa_no
	into
		#dadosArmazens
	from
		empresa_arm (nolock)
	inner join 
		empresa on empresa_arm.empresa_no = empresa.no 
	where
		empresa.ncont = (select ncont from empresa where no = @site_nr)
		
	/* Temp Table para permitir usar If statement */
	--create table #dadosSt2 (
	--	sel				bit
	--	,ref			varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
	--	,design			varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
	--	,temFicha		bit
	--	,stock			numeric(13,3)
	--	,epv1			numeric(19,6)
	--	,validade		varchar(10)
	--	,usaid			datetime
	--	,faminome		varchar(100)
	--	,familia		varchar(18)
	--	,comp			bit
	--	,generico		bit
	--	,psico			bit 
	--	,benzo			bit
	--	,ststamp		char(25)
	--	,ptoenc			numeric(10,3)
	--	,stmax			numeric(13,3)
	--	,marg1			numeric(16,3)
	--	,local			varchar(20)
	--	,u_local		varchar(60)
	--	,u_local2		varchar(60)
	--	,cnpem			varchar(8)
	--	,marg3			numeric(16,3)
	--	,marg4			numeric(16,3)
	--	,t5				bit
	--	,textosel		varchar(254)
	--	,u_lab			varchar(120)
	--	,usr1			varchar(20)
	--	,stns			bit 
	--	,inactivo		bit
	--	,dci			varchar(120)
	--	,site			varchar(18)
	--	,qttcli			numeric(13,3)
	--	,mfornec		numeric(6,2)
	--	,mfornec2		numeric(6,2)
	--	,u_duracao		numeric(8,0)
	--	,mrsimultaneo   int
	--	,especialidade	varchar(254)
	--	,obs			varchar(254)
	--	,marcada		bit
	--	,iva			numeric(5,2)
	--	,fornecedor		varchar(80)
	--	,hst			bit
	--	,u_familia		varchar(18)
	--	)
	
	/* Result Set Para Pesquisas Feitas Apenas a Produtos com Ficha */
	Select 
			sel	= convert(bit,0)
			,esb.ref 
			,design = ltrim(rtrim(isnull(esb.descr,fprod.design)))
			,temFicha = case when st.ref is null then convert(bit,0) else convert(bit,1) end
			,stock = case when st.stns = 0 then isnull(st.stock,0) else 0 end
			,stockEmpresa = esb.status--, case when st.stns = 0 then isnull(esb.stock,0) else 0 end
			,epv1 = isnull(isnull(esb.pvp,fprod.pvporig),0)
			,validade = '1900.01.01'
			,usaid = isnull(st.usaid, '1900.01.01')
			,faminome = isnull(st.faminome,'')
			,familia = isnull(st.familia, fprod.u_familia)
			,comp = isnull( (case 
								when (left(cptgrp.descricao,3)!='Não' and cptgrp.u_diploma1='' and cptgrp.u_diploma2='' and cptgrp.u_diploma3='') AND cptgrp.grupo!='ST'
							then 1
							else 0
					 end)
				  ,0)
			,generico		= IsNull(fprod.generico,0)
			,'psico'		= isnull(fprod.psico,0)
			,'benzo'		= isnull(fprod.benzo,0)
			,'ststamp'		= isnull(st.ststamp,'')
			,'ptoenc'		= isnull(st.ptoenc,0)
			,'stmax'		= isnull(st.stmax,0)
			,'marg1'		= isnull(st.marg1,0)
			,'local'		= isnull(st.local,'')
			,'u_local'		= isnull(st.u_local,'')
			,'u_local2'		= isnull(st.u_local2,'')
			,cnpem = convert(varchar(8),ISNULL(fprod.cnpem,''))
			,marg3 = ROUND(isnull(st.marg3,0),2)
			,marg4 = ROUND(isnull(st.marg4,0),2)
			,t5 = case when ranking=1 and grphmgcode != 'GH0000' and grphmgcode != '' and fprod.pvporig <= fprod.pvpmaxre then convert(bit,1) else convert(bit,0) end
			,textosel = ''
			,u_lab = ISNULL(st.u_lab,fprod.titaimdescr)
			,usr1 = ISNULL(st.usr1,fprod.u_marca)
			,stns
			,st.inactivo
			,dci = ISNULL(dci,'')
			,site
			,qttcli
			,mfornec
			,mfornec2
			,u_duracao
			,mrsimultaneo
			,especialidade = isnull((select top 1 nome from b_cli_stRecursos where b_cli_stRecursos.ref = st.ref and  tipo = 'Especialidade'),'')
			,obs = isnull(st.obs,'')
			,st.marcada
			,iva = isnull(taxasiva.taxa, 0)
			,st.fornecedor
			,hst = convert(bit,0)
			,fprod.u_familia
			,t45 = case when fprod.pvporig <= fprod.preco_acordo and fprod.preco_acordo > 0 then "T4"
						when ranking=1 and grphmgcode != 'GH0000' and grphmgcode != '' and fprod.pvporig <= fprod.pvpmaxre then "T5" 
						else ""
				   end
			,pctf = esb.pct
			,dispositivo_seguranca = isnull(fprod.dispositivo_seguranca,0)
			,qttminvd
			,qttembal  
			,epvqttminvd = round(qttminvd * st.epv1,2)
			,epvqttembal = round(qttembal * st.epv1,2)
			,tipo =  isnull(b_famFamilias.design,'')
			,histDias = ''
			,histDiasOrdem = 9999999999
			,convert(varchar(20), convert(date, st.dataIniPromo,108)) as dataIniPromo
			,convert(varchar(20), convert(date, st.dataFimPromo,108)) as dataFimPromo
		into 
			#dadosSt2
		From 
			ext_esb_products esb (nolock) 
			left join st (nolock) on esb.ref = st.ref 
			full join fprod (nolock) on st.ref = fprod.cnp
			left join cptgrp (nolock) on cptgrp.grupo = fprod.grupo
			left join empresa (nolock) on st.site_nr = empresa.no
			left join taxasiva (nolock) on st.tabiva = taxasiva.codigo
			left join b_famFamilias (nolock) on st.u_famstamp = b_famFamilias.famstamp
		Where
			esb.token = @esbToken	
			and st.site_nr  = @site_nr

	/* Tratamento de Dados para Result Set - Eliminar Tabela Temp. parece ter melhor performance do que colocar filtros na query*/
	if @familia != ''
		delete from #dadosSt2 where familia != @familiaNo
	if @lab != ''
		delete from #dadosSt2 where u_lab != @lab 
	if @marca != ''
		delete from #dadosSt2 where usr1 != @marca
	if @dci != ''
		delete from #dadosSt2 where isnull(dci,'''') not like @dci +'%'
	if @servicos = 1
		delete from #dadosSt2 where stns != 1
	if @servicos = 2
		delete from #dadosSt2 where stns = 1
	if @generico = 1
		delete from #dadosSt2 where generico != 1
	if @generico = 0
		delete from #dadosSt2 where generico != 0
	if @inactivo = 0
		delete from #dadosSt2 where inactivo != 0
	if @stock != -999999
	begin
		if @operadorLogico = 'maior'
			delete from #dadosSt2 where stock <= @stock and #dadosSt2.stns != 1
		if @operadorLogico = 'menor'
			delete from #dadosSt2 where stock >= @stock and #dadosSt2.stns != 1
		if @operadorLogico = 'igual'
			delete from #dadosSt2 where stock != @stock and #dadosSt2.stns != 1
	end
	if @atendimento = 1 and @refExcluir != char(39)+'vazio'+CHAR(39)
		delete from #dadosSt2 where ref in (select items from dbo.up_SplitToTable(@refExcluir,','))
	if @parafarmacia = 1
		delete from #dadosSt2 where familia = 1 or u_familia = 1 or u_familia = 58 or familia=58
	if @t5 = 1
		delete from #dadosSt2 where t5 = 0		
	if @recurso != ''
		delete from #dadosSt2 where ref not in (select ref from b_cli_stRecursos where nome = @recurso)
	if @txIva != -1
		delete from #dadosSt2 where iva != @txIva
	if @fornecedor != ''
	begin 
		if @fornecedor = 'S/ Fornecedor'		
			delete from #dadosSt2 where fornecedor != ''
		else 
			delete from #dadosSt2 where fornecedor != @fornecedor
	end
		
	/* result set final */
	/* Caso seja multi Loja */
	IF (select count(*) empresa_no from #dadosArmazens) > 1
		BEGIN
			Select 
				top (@top) st2.*
				,diassmov = DATEDIFF(day,st2.usaid, getdate())
				--,stockempresa = isnull(x.stockempresa,0)
				,StockGrupo  = isnull(y.StockGrupo,0)
			From
				#dadosSt2 st2

				left outer join (select 
									ref
									,StockEmpresa = ''--sum(stock) 
								from 
									st (nolock)
								where 
									site_nr in (select * from #dadosArmazens)
									and stns = 0
								group by 
									ref) x on st2.ref = x.ref

				left outer join (select 
									ref
									,StockGrupo = sum(stock) 
								from 
									st (nolock)
								where 
									stns = 0
								group by 
									ref) y on st2.ref = y.ref

			order by
				st2.marcada desc, st2.stock desc /* prioridade */
		END
	ELSE /* Caso seja apenas 1 Loja */
		BEGIN
			Select 
				top (@top) st2.*
				,diassmov = DATEDIFF(day,st2.usaid, getdate())
				--,stockEmpresa = isnull(st2.stock,0)
				,StockGrupo   = isnull(st2.stock,0)
			From
				#dadosSt2 st2
			order by
				st2.marcada desc, st2.stock desc /* prioridade */
		END

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosArmazens'))
		DROP TABLE #dadosArmazens	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt2'))
		DROP TABLE #dadosSt2	

GO
GRANT EXECUTE on dbo.up_stocks_pesquisaESB TO PUBLIC
GRANT Control on dbo.up_stocks_pesquisaESB TO PUBLIC
GO