/*
	select *from st
	 exec up_stocks_campanhaOnline  '00000001' , '1'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_campanhaOnline ]') IS NOT NULL
	drop procedure dbo.up_stocks_campanhaOnline 
go

create procedure dbo.up_stocks_campanhaOnline 
@ref varchar(28),
@siteno tinyint

/* WITH ENCRYPTION */
AS

	If OBJECT_ID('tempdb.dbo.#dadosCampanha') IS NOT NULL
		DROP TABLE #dadosCampanha

	declare @conta int = 0

	declare @emptyDate DateTime = Convert(Datetime, '1900.01.01', 120)


	select 
		isnull(campanhas_online.idTipo,0) as id,
		isnull(round(valorOferta,2),0) as valorOferta,
		isnull(round(valorCompra,2),0) as valorCompra,
		isnull(campanhas_online_st.dataInicio,@emptyDate) as dataInicio,
		isnull(campanhas_online_st.dataFim,@emptyDate) as dataFim
	into
		#dadosCampanha
	from 
		campanhas_online(nolock)
	inner join campanhas_online_st(nolock) on campanhas_online.idTipo=campanhas_online_st.idTipoCampanha
	where 
		campanhas_online_st.ref=ltrim(rtrim(@ref)) 
		and campanhas_online_st.site_nr=@siteno 
		and campanhas_online.activo=1 
		--and campanhas_online_st.activo=1 

	select @conta = count(*)from #dadosCampanha

	if(@conta=0)
		select 0 as id ,0 as valorOferta,0 as valorCompra, @emptyDate as dataInicio, @emptyDate as dataFim
	else
		select top 1  id,valorOferta,valorCompra,dataInicio,dataFim from #dadosCampanha
		
	If OBJECT_ID('tempdb.dbo.#dadosCampanha') IS NOT NULL
		DROP TABLE #dadosCampanha


GO
Grant Execute On dbo.up_stocks_campanhaOnline  to Public
Grant Control On dbo.up_stocks_campanhaOnline  to Public
GO


