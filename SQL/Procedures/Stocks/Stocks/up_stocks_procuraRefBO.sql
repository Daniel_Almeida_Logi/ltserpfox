/*
	Levanta informação do artigo
	exec up_stocks_procuraRefBO '1001887', 1, 0, 0

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_procuraRefBO]') IS NOT NULL
	drop procedure dbo.up_stocks_procuraRefBO
go

create procedure dbo.up_stocks_procuraRefBO
	@ref varchar (40)
	,@siteno tinyint
	,@no numeric(10,0) = 0
	,@estab numeric(3,0) = 0

/* WITH ENCRYPTION */
AS

	select top 1 
		st.ststamp
		,st.ref
		,st.design
		,st.familia
		,stock = case when st.stns=1 then 0 else st.stock end
		,epv1 = st.epv1
		,st.forref
		,st.fornecedor
		,st.usr1
		,st.validade
		,st.usaid
		,st.uintr
		,st.usrqtt
		,st.eoq
		,st.pcult
		,st.pvultimo
		,st.unidade
		,st.ptoenc
		,st.tabiva
		,st.local
		,st.fornec
		,st.fornestab
		,st.qttfor
		,st.qttcli
		,st.qttrec
		,st.udata
		,st.pcusto
		,st.pcpond
		,st.qttacin
		,st.qttacout
		,st.stmax
		,st.stmin
		,st.obs
		,st.codigo
		,st.uni2
		,st.conversao
		,ivaincl = st.ivaincl
		,st.nsujpp
		,st.ecomissao
		,st.imagem
		,st.cpoc
		,st.containv
		,st.contacev
		,st.contareo
		,st.contacoe
		,st.peso
		,st.bloqueado
		,st.fobloq
		,st.mfornec
		,st.mfornec2
		,st.pentrega
		,st.consumo
		,st.baixr
		,st.despimp
		,st.marg1
		,st.marg2
		,st.marg3
		,st.marg4
		,st.diaspto
		,st.diaseoq
		,st.clinica
		,st.pbruto
		,st.volume
		,st.faminome
		,st.qttesp
		,epcusto = st.epcusto
		,st.epcult
		,st.epcpond
		,st.epvultimo
		,st.iva1incl
		,st.iva2incl
		,st.iva3incl
		,st.iva4incl
		,st.iva5incl
		,st.ivapcincl
		,st.stns
		,st.url
		,st.iecasug
		,st.iecaref
		,st.iecarefnome
		,st.iecaisref
		,st.qlook
		,st.qttcat
		,st.compnovo
		,st.ousrinis
		,st.ousrdata
		,st.ousrhora
		,st.usrinis
		,st.usrdata
		,st.usrhora
		,st.marcada
		,st.contaieo
		,st.inactivo
		,st.sujinv
		,st.u_nota1
		,st.u_impetiq
		,st.u_local
		,st.u_local2
		,st.u_lab
		,st.u_tipoetiq
		,st.u_validavd
		,st.u_aprov
		,st.u_fonte
		,st.u_validarc
		,st.u_servmarc
		,st.u_duracao
		,st.u_secstamp
		,st.u_catstamp
		,st.u_famstamp
		,st.u_depstamp
		,empresa.site
		,st.usalote
		,st.rateamento
		,st.exportado
		,st.u_codint
		,st.mrsimultaneo
		,iva = (select top 1 taxa  from taxasiva (nolock) where codigo = tabiva)
		,site = isnull(empresa.site,'')
		,psico = ISNULL(fprod.psico,convert(bit,0))
		,benzo = ISNULL(fprod.benzo ,convert(bit,0)) 
		,pclfornec = ISNULL(tabelaPrecos.pcl,0)
		,viaVerde = case 
						when emb_via_verde.ref IS NULL then 0 
						when emb_via_verde.tipo='VVM' then 1
						when emb_via_verde.tipo='VMH' then 2
						else 0 
						end
		,st.qttminvd
		,st.qttembal
		,fprod.dispositivo_seguranca
		,st.qttEmbEnc
	from 
		st (nolock)
		inner join empresa on empresa.no = st.site_nr
		left join bc (nolock) on bc.ref=st.ref AND bc.site_nr = @siteno
		left join fprod (nolock) on st.ref = fprod.cnp
		left join tabelaPrecos on st.ref = tabelaPrecos.ref and tabelaPrecos.fornec =@no and tabelaPrecos.fornestab =@estab 
		left join emb_via_verde (nolock) on rtrim(ltrim(st.ref)) = rtrim(ltrim(emb_via_verde.ref))
	where 
		st.site_nr = @siteno
		AND st.inactivo=0
		AND (st.ref=@ref OR (bc.codigo=@ref OR st.codigo=@ref)) 

GO
Grant Execute on dbo.up_stocks_procuraRefBO to Public
Grant Control on dbo.up_stocks_procuraRefBO to Public
GO
