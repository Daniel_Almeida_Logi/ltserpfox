/*Lista Grupos Homogeneo com informação de stock actual e Vendas Ult Mes*/
/* exec up_stocks_Corrige_enc_pend_cl 1, '5440987'  */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_Corrige_enc_pend_cl]') IS NOT NULL
    drop procedure dbo.up_stocks_Corrige_enc_pend_cl
go

create procedure dbo.up_stocks_Corrige_enc_pend_cl
@site_nr tinyint,
@ref varchar(30)

/* WITH ENCRYPTION */
AS

	DECLARE @cteReservasClientes TABLE (ref varchar(18),armazem numeric(5),qtt numeric(9));
	insert into	@cteReservasClientes
	select 	bi.ref, bi.armazem, qtt = sum(bi.qtt-bi.qtt2) from ts (nolock) inner join bi  (nolock) on bi.ndos = ts.ndos inner join bo  (nolock) on bo.bostamp=bi.bostamp where ts.rescli = 1 and bi.fechada = 0 and bo.fechada=0 and bi.ref=@ref
	group by ref, bi.armazem

	--DECLARE @cteReservasFornecedores TABLE (ref varchar(18),armazem numeric(5),qtt numeric(9));
	--insert into	@cteReservasFornecedores
	--select bi.ref,bi.armazem,qtt = sum(bi.qtt-bi.qtt2) from ts (nolock) inner join bi (nolock) on bi.ndos = ts.ndos inner join bo (nolock) on bo.bostamp=bi.bostamp where ts.resfor = 1 and bi.fechada = 0 and bo.fechada=0 and bi.ref=@ref
	--group by ref, bi.armazem


	update sa set 
		rescli = isnull((select SUM(qtt) from @cteReservasClientes cteReservasClientes where ref = @ref and cteReservasClientes.armazem = @site_nr),0)
		--,resfor = isnull((select SUM(qtt) from @cteReservasFornecedores cteReservasFornecedores where ref = @ref and cteReservasFornecedores.armazem = @site_nr),0)
	from 
		sa (nolock)--, #cteArmazens cteArmazens
	where
		sa.armazem = @site_nr
		and sa.ref = @ref
		
	/**/	
	update st set
				qttcli= isnull((select SUM(qtt) from @cteReservasClientes cteReservasClientes where ref = @ref and cteReservasClientes.armazem = @site_nr),0) 
				--,qttfor = isnull((select SUM(qtt) from @cteReservasFornecedores cteReservasFornecedores where ref = @ref and cteReservasFornecedores.armazem  = @site_nr),0)
		where st.site_nr = @site_nr and ref=@ref
	
	--exec up_stocks_Corrige_enc_pend_cl 1, '5440987'

GO
Grant Execute on dbo.up_stocks_Corrige_enc_pend_cl to Public
Grant Control on dbo.up_stocks_Corrige_enc_pend_cl to Public
go
