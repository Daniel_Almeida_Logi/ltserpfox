-- Procurar Dados Ref para Inventario --
-- exec up_stocks_dadosRefInv '8168617',1
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_dadosRefInv]') IS NOT NULL
	drop procedure dbo.up_stocks_dadosRefInv
go

create procedure dbo.up_stocks_dadosRefInv

@ref varchar (30)
,@site_nr tinyint

/* WITH ENCRYPTION */
AS

				
	select top 1
		st.ref, 
		st.design, 
		st.stock, 
		st.epcusto, 
		st.epcpond, 
		inactivo
	from
		st (nolock)
	where
		st.site_nr = @site_nr
		and st.ref=@ref 
		and stns=0

GO
Grant Execute on dbo.up_stocks_dadosRefInv to Public
Grant Control on dbo.up_stocks_dadosRefInv to Public
go
