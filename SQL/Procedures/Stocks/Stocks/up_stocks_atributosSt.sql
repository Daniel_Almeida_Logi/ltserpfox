-- Listagem de Referencias para Atributos
-- exec up_stocks_atributosSt 30, '', '', '', '', 1
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_atributosSt]') IS NOT NULL
	drop procedure dbo.up_stocks_atributosSt
go

create procedure dbo.up_stocks_atributosSt
@top int,
@ref varchar(254),
@designacao varchar(254),
@marca varchar(200),
@familia varchar(254),
@site_nr tinyint


/* WITH ENCRYPTION */
AS


	select 
		TOP(@top) 
		CONVERT(bit,0) as escolha
		,REF
		,DESIGN
		,FAMINOME as FAMILIA
		,USR1 as MARCA 
	from 
		ST (nolock)
	WHERE 
		st.REF LIKE @ref+'%' 
		AND DESIGN LIKE @designacao+'%' 
		AND USR1 LIKE @marca+'%' 
		AND FAMINOME LIKE @familia+'%' 
		and st.site_nr = @site_nr
	ORDER BY 
		REF
		,DESIGN
		,USR1	                                    

GO
Grant Execute On dbo.up_stocks_atributosSt to Public
Grant Control On dbo.up_stocks_atributosSt to Public
GO
