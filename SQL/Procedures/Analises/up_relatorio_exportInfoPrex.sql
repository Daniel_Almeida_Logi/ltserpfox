/* 
	
1. CPR - C�digo do produto
2. NOM - Nome do produto
3. FAP - Forma de apresenta��o do produto
4. LOCALIZACAO - Loja em que est� a consultar o produto
5. SAC - Stock actual
6. STM - Stock m�ximo
7. SMI - Stock minimo
8. QTE - Quantidade encomendada
9. DUV - Data da ultima venda
10. DUC - Data da ultima compra
11. PVP - Pre�o de venda ao publico
12. PCU - Pre�o de custo
13. IVA - Taxa de IVA aplic�vel
14. CAT - Familia do produto
15. CT1 - Sub-familia do produto
16. GEN - Gen�rico ("S" � gen�rico, "N" n�o � gen�rico)
17. GPR - Grossista prefer�ncial (c�digo do grossista)
18. CLA - C�digo do laborat�rio
19. TPR - Tipo de receita aplic�vel
20. CAR - C�digo da arruma��o (prateleira)
21. GAM - Gama do produto
22. V(nn) - Vendas do "nn" m�s anterior � data corrente (at� 24 colunas)
23. DTVAL - Prazo de validade
24. FPD - Fornecedor Prefer�ncial Designa��o
25. LAD - Laborat�rio AIM Designa��o
26. PRATELEIRA -Nome da Prateleira
27. GAMA - Nome da Gama
28. GRUPOHOMOGENEO - Grupo Homog�neo
29. INACTIVO - Estado do Produto na Farm�cia
30. PVP5 - Pre�o Top5
31. CNPEM - c�digo CNPEM

	exec up_relatorio_exportInfoPrex '20230401','20230428', 'Loja 1', 0 
	exec up_relatorio_exportInfoPrex '20200301','20200331', 'Loja 1', 0 
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_exportInfoPrex]') IS NOT NULL
	drop procedure dbo.up_relatorio_exportInfoPrex
go

create procedure dbo.up_relatorio_exportInfoPrex
	@dataIni						AS DATETIME
	,@dataFim						AS DATETIME
	,@site							AS VARCHAR(55)
	,@incluiHistorico				AS BIT = 0
	
/* with encryption */
AS
BEGIN

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase

	/* Dados Base Vendas  */
	CREATE TABLE #dadosVendasBase (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,2)
        ,epcpond numeric(19,2)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,2)
        ,etiliquidoSiva numeric(19,2)
        ,ettent1 numeric(13,2)
        ,ettent2 numeric(13,2)
        ,ettent1siva numeric(13,2)
        ,ettent2siva numeric(13,2)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,2)
        ,descvale numeric(19,2)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)
	)
    
    INSERT #dadosVendasBase
    EXEC up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site, 0

	/* Define se vais mostrar resultados abaixo do Cliente 199 */
	DECLARE @Entidades AS BIT
	SET @Entidades = CASE WHEN (SELECT tipoempresa FROM empresa WHERE site = @site) in ('FARMACIA', 'PARAFARMACIA') THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END

	If @Entidades = 1
	DELETE FROM #dadosVendasBase WHERE #dadosVendasBase.no BETWEEN 1 AND 198

	SELECT
		CPR				= 'CPR'
		,NOM			= 'NOM'
		,FAP			= 'FAP'
		,LOCALIZACAO	= 'LOCALIZACAO'
		,SAC			= 'SAC'
		,STM			= 'STM'
		,SMI			= 'SMI'
		,QTE			= 'QTE'
		,DUV			= 'DUV'
		,DUC			= 'DUC'
		,PVP			= 'PVP'
		,PCU			= 'PCU'
		,IVA			= 'IVA'
		,CAT			= 'CAT'
		,CT1			= 'CT1'
		,GEN			= 'GEN'
		,GPR			= 'GPR'
		,CLA			= 'CLA'
		,TPR			= 'TPR'
		,CAR			= 'CAR'
		,GAM			= 'GAM'
		,V_0			= 'V_0'
		,V_1			= 'V_1'
		,V_2			= 'V_2'
		,V_3			= 'V_3'
		,V_4			= 'V_4'
		,V_5			= 'V_5'
		,V_6			= 'V_6'
		,V_7			= 'V_7'
		,V_8			= 'V_8'
		,V_9			= 'V_9'
		,V_10			= 'V_10'
		,V_11			= 'V_11'
		,V_12			= 'V_12'
		,V_13			= 'V_13'
		,V_14			= 'V_14'
		,V_15			= 'V_15'
		,V_16			= 'V_16'
		,V_17			= 'V_17'
		,V_18			= 'V_18'
		,V_19			= 'V_19'
		,V_20			= 'V_20'
		,V_21			= 'V_21'
		,V_22			= 'V_22'
		,V_23			= 'V_23'
		,DTVAL			= 'DTVAL'
		,FPD			= 'FPD'
		,LAD			= 'LAD'
		,PRATELEIRA		= 'PRATELEIRA'
		,GAMA			= 'GAMA'
		,GRUPOHOMOGENEO = 'GRUPOHOMOGENEO'
		,INACTIVO		= 'INACTIVO'
		,PVP5			= 'PVP5'
		,CNPEM			= 'CNPEM'
	UNION ALL
	--exec up_relatorio_exportInfoPrex '20171001','20171018', 'Loja 1', 0 
	SELECT
		CPR = st.ref
		,NOM = CONVERT(VARCHAR(100),REPLACE(REPLACE(st.design,'"',''), CHAR(9), ' '))
		,FAP = CONVERT(VARCHAR(100),REPLACE(REPLACE(st.design,'"',''), CHAR(9), ' ')) 
		,LOCALIZACAO = LEFT(REPLACE(CAST(ISNULL(empresa.site,'') AS VARCHAR(30)), CHAR(9), ' '),30)
		,SAC = LEFT(REPLACE(CAST(convert(INT,st.stock) AS VARCHAR(10)), CHAR(9), ' '),10)
		,STM = LEFT(REPLACE(CAST(convert(INT,st.stmax) AS VARCHAR(10)), CHAR(9), ' '),10)
		,SMI = LEFT(REPLACE(CAST(convert(INT,st.ptoenc) AS VARCHAR(10)), CHAR(9), ' '),10)
		,QTE = LEFT(REPLACE(CAST(convert(INT,st.qttfor) AS VARCHAR(10)), CHAR(9), ' '),10)
		,DUV = LEFT(REPLACE(CONVERT(VARCHAR, st.usaid, 105), CHAR(9), ' '),250)
		,DUC = LEFT(REPLACE(CONVERT(VARCHAR, st.uintr, 105), CHAR(9), ' '),250)
		,PVP = CAST(CAST(st.epv1 AS NUMERIC(10,2)) AS VARCHAR(15))
		,PCU = CAST(CAST(st.epcult AS NUMERIC(10,2)) AS VARCHAR(15)) /*PCL*/
		,IVA = CAST(CONVERT(INT,ISNULL(taxasiva.taxa,0)) AS VARCHAR(5))
		,CAT = LEFT(REPLACE(st.faminome, CHAR(9), ' '),250)
		,CT1 = 'N/D'
		,GEN = ISNULL(CASE WHEN fprod.generico = 0 THEN 'N' ELSE 'S' END, 'N')
		,GPR = CAST(st.fornec AS VARCHAR(10))
		,CLA = 'N/D'
		,TPR = 'N/D'
		,CAR = 'N/D'
		,GAM = 'N/D'
		,V_0  = CAST(ISNULL(CONVERT(INT,(SELECT SUM(qtt) FROM #dadosVendasBase WHERE #dadosVendasBase.ref = st.ref AND fdata BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) and DATEADD(MILLISECOND, -3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)))),0) AS VARCHAR(10))
		,V_1  = CAST(ISNULL(CONVERT(INT,(SELECT SUM(qtt) FROM #dadosVendasBase WHERE #dadosVendasBase.ref = st.ref AND fdata BETWEEN DATEADD(MM,-1,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) and DATEADD(MM,-1,DATEADD(MILLISECOND, -3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0))))),0) AS VARCHAR(10))
		,V_2  = CAST(ISNULL(CONVERT(INT,(SELECT SUM(qtt) FROM #dadosVendasBase WHERE #dadosVendasBase.ref = st.ref AND fdata BETWEEN DATEADD(MM,-2,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) and DATEADD(MM,-2,DATEADD(MILLISECOND, -3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0))))),0) AS VARCHAR(10))
		,V_3  = CAST(ISNULL(CONVERT(INT,(SELECT SUM(qtt) FROM #dadosVendasBase WHERE #dadosVendasBase.ref = st.ref AND fdata BETWEEN DATEADD(MM,-3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) and DATEADD(MM,-3,DATEADD(MILLISECOND, -3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0))))),0) AS VARCHAR(10))
		,V_4  = CAST(ISNULL(CONVERT(INT,(SELECT SUM(qtt) FROM #dadosVendasBase WHERE #dadosVendasBase.ref = st.ref AND fdata BETWEEN DATEADD(MM,-4,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) and DATEADD(MM,-4,DATEADD(MILLISECOND, -3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0))))),0) AS VARCHAR(10))
		,V_5  = CAST(ISNULL(CONVERT(INT,(SELECT SUM(qtt) FROM #dadosVendasBase WHERE #dadosVendasBase.ref = st.ref AND fdata BETWEEN DATEADD(MM,-5,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) and DATEADD(MM,-5,DATEADD(MILLISECOND, -3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0))))),0) AS VARCHAR(10))
		,V_6  = CAST(ISNULL(CONVERT(INT,(SELECT SUM(qtt) FROM #dadosVendasBase WHERE #dadosVendasBase.ref = st.ref AND fdata BETWEEN DATEADD(MM,-6,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) and DATEADD(MM,-6,DATEADD(MILLISECOND, -3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0))))),0) AS VARCHAR(10))
		,V_7  = CAST(ISNULL(CONVERT(INT,(SELECT SUM(qtt) FROM #dadosVendasBase WHERE #dadosVendasBase.ref = st.ref AND fdata BETWEEN DATEADD(MM,-7,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) and DATEADD(MM,-7,DATEADD(MILLISECOND, -3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0))))),0) AS VARCHAR(10))
		,V_8  = CAST(ISNULL(CONVERT(INT,(SELECT SUM(qtt) FROM #dadosVendasBase WHERE #dadosVendasBase.ref = st.ref AND fdata BETWEEN DATEADD(MM,-8,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) and DATEADD(MM,-8,DATEADD(MILLISECOND, -3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0))))),0) AS VARCHAR(10))
		,V_9  = CAST(ISNULL(CONVERT(INT,(SELECT SUM(qtt) FROM #dadosVendasBase WHERE #dadosVendasBase.ref = st.ref AND fdata BETWEEN DATEADD(MM,-9,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) and DATEADD(MM,-9,DATEADD(MILLISECOND, -3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0))))),0) AS VARCHAR(10))
		,V_10 = CAST(ISNULL(CONVERT(INT,(SELECT SUM(qtt) FROM #dadosVendasBase WHERE #dadosVendasBase.ref = st.ref AND fdata BETWEEN DATEADD(MM,-10,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) and DATEADD(MM,-10,DATEADD(MILLISECOND, -3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0))))),0) AS VARCHAR(10))
		,V_11 = CAST(ISNULL(CONVERT(INT,(SELECT SUM(qtt) FROM #dadosVendasBase WHERE #dadosVendasBase.ref = st.ref AND fdata BETWEEN DATEADD(MM,-11,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) and DATEADD(MM,-11,DATEADD(MILLISECOND, -3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0))))),0) AS VARCHAR(10))
		,V_12 = CAST(ISNULL(CONVERT(INT,(SELECT SUM(qtt) FROM #dadosVendasBase WHERE #dadosVendasBase.ref = st.ref AND fdata BETWEEN DATEADD(MM,-12,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) and DATEADD(MM,-12,DATEADD(MILLISECOND, -3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0))))),0) AS VARCHAR(10))
		,V_13 = CAST(ISNULL(CONVERT(INT,(SELECT SUM(qtt) FROM #dadosVendasBase WHERE #dadosVendasBase.ref = st.ref AND fdata BETWEEN DATEADD(MM,-13,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) and DATEADD(MM,-13,DATEADD(MILLISECOND, -3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0))))),0) AS VARCHAR(10))
		,V_14 = CAST(ISNULL(CONVERT(INT,(SELECT SUM(qtt) FROM #dadosVendasBase WHERE #dadosVendasBase.ref = st.ref AND fdata BETWEEN DATEADD(MM,-14,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) and DATEADD(MM,-14,DATEADD(MILLISECOND, -3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0))))),0) AS VARCHAR(10))
		,V_15 = CAST(ISNULL(CONVERT(INT,(SELECT SUM(qtt) FROM #dadosVendasBase WHERE #dadosVendasBase.ref = st.ref AND fdata BETWEEN DATEADD(MM,-15,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) and DATEADD(MM,-15,DATEADD(MILLISECOND, -3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0))))),0) AS VARCHAR(10))
		,V_16 = CAST(ISNULL(CONVERT(INT,(SELECT SUM(qtt) FROM #dadosVendasBase WHERE #dadosVendasBase.ref = st.ref AND fdata BETWEEN DATEADD(MM,-16,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) and DATEADD(MM,-16,DATEADD(MILLISECOND, -3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0))))),0) AS VARCHAR(10))
		,V_17 = CAST(ISNULL(CONVERT(INT,(SELECT SUM(qtt) FROM #dadosVendasBase WHERE #dadosVendasBase.ref = st.ref AND fdata BETWEEN DATEADD(MM,-17,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) and DATEADD(MM,-17,DATEADD(MILLISECOND, -3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0))))),0) AS VARCHAR(10))
		,V_18 = CAST(ISNULL(CONVERT(INT,(SELECT SUM(qtt) FROM #dadosVendasBase WHERE #dadosVendasBase.ref = st.ref AND fdata BETWEEN DATEADD(MM,-18,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) and DATEADD(MM,-18,DATEADD(MILLISECOND, -3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0))))),0) AS VARCHAR(10))
		,V_19 = CAST(ISNULL(CONVERT(INT,(SELECT SUM(qtt) FROM #dadosVendasBase WHERE #dadosVendasBase.ref = st.ref AND fdata BETWEEN DATEADD(MM,-19,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) and DATEADD(MM,-19,DATEADD(MILLISECOND, -3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0))))),0) AS VARCHAR(10))
		,V_20 = CAST(ISNULL(CONVERT(INT,(SELECT SUM(qtt) FROM #dadosVendasBase WHERE #dadosVendasBase.ref = st.ref AND fdata BETWEEN DATEADD(MM,-20,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) and DATEADD(MM,-20,DATEADD(MILLISECOND, -3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0))))),0) AS VARCHAR(10))
		,V_21 = CAST(ISNULL(CONVERT(INT,(SELECT SUM(qtt) FROM #dadosVendasBase WHERE #dadosVendasBase.ref = st.ref AND fdata BETWEEN DATEADD(MM,-21,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) and DATEADD(MM,-21,DATEADD(MILLISECOND, -3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0))))),0) AS VARCHAR(10))
		,V_22 = CAST(ISNULL(CONVERT(INT,(SELECT SUM(qtt) FROM #dadosVendasBase WHERE #dadosVendasBase.ref = st.ref AND fdata BETWEEN DATEADD(MM,-22,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) and DATEADD(MM,-22,DATEADD(MILLISECOND, -3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0))))),0) AS VARCHAR(10))
		,V_23 = CAST(ISNULL(CONVERT(INT,(SELECT SUM(qtt) FROM #dadosVendasBase WHERE #dadosVendasBase.ref = st.ref AND fdata BETWEEN DATEADD(MM,-23,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) and DATEADD(MM,-23,DATEADD(MILLISECOND, -3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0))))),0) AS VARCHAR(10))
		,DTVAL= CAST(ISNULL(CONVERT(INT,(SELECT SUM(qtt) FROM #dadosVendasBase WHERE #dadosVendasBase.ref = st.ref AND fdata BETWEEN DATEADD(MM,-24,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) and DATEADD(MM,-24,DATEADD(MILLISECOND, -3,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0))))),0) AS VARCHAR(10))
		,FPD = CASE WHEN st.fornecedor = '' THEN 'N/D' ELSE LEFT(REPLACE(st.fornecedor, CHAR(9), ' '),250) END
		,LAD = CASE WHEN st.u_lab = '' THEN 'N/D' ELSE LEFT(REPLACE(st.u_lab, CHAR(9), ' '),250) END
		,PRATELEIRA = CASE WHEN st.local = '' THEN 'N/D' ELSE LEFT(REPLACE(st.local, CHAR(9), ' '),250) END
		,GAMA = 'N/D'
		,GRUPOHOMOGENEO = ISNULL(LEFT(REPLACE(fprod.grphmgcode, CHAR(9), ' '),250),'GH0000')
		,INACTIVO = CASE WHEN st.inactivo = 0 THEN 'N' ELSE 'S' END
		,PVP5 = LEFT(REPLACE(CAST(ISNULL(fprod.pvpmax, '0.00') AS VARCHAR(15)), CHAR(9), ' '),15)
		,CNPEM = LEFT(REPLACE(CAST(ISNULL(fprod.cnpem,'0') AS VARCHAR(15)), CHAR(9), ' '),15)
	FROM 
		st (nolock)
		INNER JOIN empresa (NOLOCK) ON empresa.no = st.site_nr
		LEFT JOIN taxasiva (NOLOCK) ON st.tabiva  = taxasiva.codigo
		LEFT JOIN fprod	   (NOLOCK) ON st.ref	  = fprod.cnp
	WHERE
		empresa.site = @site
		AND st.stns = 0
		
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	END
GO
Grant Execute on dbo.up_relatorio_exportInfoPrex to Public
Grant control on dbo.up_relatorio_exportInfoPrex to Public
GO