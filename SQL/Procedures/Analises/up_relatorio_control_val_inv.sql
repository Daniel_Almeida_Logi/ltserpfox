-- Relatório Conferencia Inventário antes Lançamento
-- exec up_relatorio_control_val_inv 'LY','Loja 1','',''

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_control_val_inv]') IS NOT NULL
	drop procedure dbo.up_relatorio_control_val_inv
go

create procedure dbo.up_relatorio_control_val_inv
@descricao		varchar(60)
,@site			varchar(55)	
,@familia		varchar(100)
,@valult		bit	

/* with encryption */

AS 
BEGIN

	Select 
		stic.descricao
		,stil.ref
		,stil.design
		,stil.armazem
		,stockInventario = stil.stock
		--,stockActualArmazem = isnull(sa.stock,0)
		--,difQt = stil.stock - isnull(sa.stock,0)
		--,stil.zona
		, st.validade
	from	
		stic (nolock)
		inner join stil (nolock) on stic.sticstamp = stil.sticstamp
		left join sa (nolock) on stil.ref = sa.ref and stil.armazem = sa.armazem
		inner join st on st.ref=stil.ref and st.site_nr=sa.armazem
	Where
		stic.descricao = @descricao
		and st.familia like (case when @familia='' then '%%' else @familia end)
		and st.validade<(case when @valult=1 then stic.data else '22000101' end)
		and year(st.validade) <> (case when @valult=1 then 1900 else 0000 end)
	Order by 
		stil.ref

END

GO
Grant Execute on dbo.up_relatorio_control_val_inv to Public
Grant control on dbo.up_relatorio_control_val_inv to Public
Go