/*
	
	exec up_relatorio_getEntidades

*/

SET ANSI_NULLS OFF 
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_getEntidades]') IS NOT NULL
	drop procedure dbo.up_relatorio_getEntidades
go


CREATE PROCEDURE dbo.up_relatorio_getEntidades


AS
BEGIN

	SELECT 
		CAST(0 as bit) as sel,
		nome,
		CAST(REPLACE(cptorgStamp, 'ADM', '') AS varchar(3)) as num
	FROM
		cptorg(nolock)
	WHERE
		docTypeRes <> 0
		OR sendXMLRes <> 0

END

Go
Grant Execute on dbo.up_relatorio_getEntidades to Public
Grant control on dbo.up_relatorio_getEntidades to Public
Go