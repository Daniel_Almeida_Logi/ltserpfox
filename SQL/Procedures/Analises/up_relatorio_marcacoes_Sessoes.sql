-- SP: up_relatorio_marcacoes_Sessoes
-- exec	up_relatorio_marcacoes_Sessoes ''

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_marcacoes_Sessoes]') IS NOT NULL
	drop procedure dbo.up_relatorio_marcacoes_Sessoes
go

Create procedure dbo.up_relatorio_marcacoes_Sessoes
@stamp as uniqueidentifier


/* with encryption */
AS

	Select 
		sessao,
		dataConvertida = convert(varchar,data,102)
		,hinicio
		,hfim
		,design
		,ref
		,recursonome
		,drnome
		,div
		,estado
		,b_cli_mr.nome
	from	
		b_cli_mr 
		inner join b_series ON b_series.serieno = b_cli_mr.serieno
	where 
		b_series.seriestamp = @stamp 
	order by 
		b_cli_mr.data, b_cli_mr.hinicio, b_cli_mr.hfim, b_cli_mr.ref


Go
Grant Execute on dbo.up_relatorio_marcacoes_Sessoes to Public
Grant Control on dbo.up_relatorio_marcacoes_Sessoes to Public
Go