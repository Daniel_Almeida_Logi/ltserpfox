/* Calculo Inventário Detalhado 

	exec up_relatorio_conferencia_inventarioData '20210625','6633073','','Loja 2', '','','',-9999
	exec up_relatorio_conferencia_inventarioData '20250102','','','Loja 1', '','','',-999,0,0

*/	

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('[dbo].[up_relatorio_conferencia_inventarioData]') IS NOT NULL
	drop procedure dbo.up_relatorio_conferencia_inventarioData
GO

CREATE PROCEDURE [dbo].[up_relatorio_conferencia_inventarioData]
	@data as datetime,
	@ref as varchar(18),
	@design as varchar(60),
	@site varchar(60),
	@local1 varchar(20),
	@local2 varchar(60),
	@local3 varchar(60),
	@stockData numeric(9,0),
	@psicotropicos bit,
	@benzodiazepinas bit

/* with encryption */

AS 	
	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosStockData'))
		DROP TABLE #dadosStockData
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosEvuData'))
		DROP TABLE #dadosEvuData
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosEvuData2'))
		DROP TABLE #dadosEvuData2
	
	
	/* Calc Loja e Armazens */
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)

	select 
		armazem
	into
		#Armazens
	From
		empresa (nolock)
		inner join empresa_arm (nolock) on empresa.no = empresa_arm.empresa_no
	where
		empresa.site = case when @site = '' then empresa.site else @site end		


	/* Calc stock à data */
	SELECT	
		sl.ref
		,stockadata = SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
		,sl.armazem
	INTO 
		#dadosStockData
	FROM	
		sl (nolock)
		inner join empresa_arm (nolock) on empresa_arm.armazem = sl.armazem 
		inner join empresa (nolock) on empresa.no = empresa_arm.empresa_no
	WHERE	
		[sl].[datalc] <= @data
		AND [sl].[qtt] != 0
		AND [sl].[ref] LIKE @ref + '%'
		and empresa.site = @site	
	Group by 
		sl.ref, sl.armazem;
		

	/* Calc PCL à data */
	
	WITH RankedData AS (
	    SELECT 
	        sl.ref,
	        ROUND(evu, 2) AS pcl,
	        sl.armazem,
	        sl.datalc,
	        ROW_NUMBER() OVER (PARTITION BY sl.ref ORDER BY sl.datalc DESC, sl.ousrhora DESC, sl.evu desc )
			/*Acrescentado o evu devido a docs com duas linhas da mm ref com um dos preços a zero. o ROW_NUMBER ora apanhava um, ora outro*/
				AS rn
	    FROM sl (NOLOCK)
	    INNER JOIN empresa_arm (NOLOCK) ON empresa_arm.armazem = sl.armazem 
	    INNER JOIN empresa (NOLOCK) ON empresa.no = empresa_arm.empresa_no
	    WHERE [sl].[datalc] <= @data  
	        AND [EVU] != 0  
	        AND [sl].[ref] LIKE @ref + '%'  
	        AND [origem] IN ('FO', 'IF') 
	        AND empresa.site = @site 
	)
	SELECT 
	    ref, 
	    pcl, 
	    armazem, 
	    datalc AS DataUltimaMovimentacao
	INTO #dadosEvuData
	FROM RankedData
	WHERE rn = 1;  


	/* Preparar Result Set */
	Select
		#dadosStockData.ref
		,STOCKADATA = #dadosStockData.STOCKADATA
		,#dadosEvuData.pcl
		,#dadosStockData.armazem
	INTO 
		#dadosEvuData2
	From
		#dadosStockData
		left join #dadosEvuData on #dadosStockData.ref = #dadosEvuData.ref
	Where
		ISNULL(#dadosStockData.STOCKADATA,0) >= @stockData
		

	/* Result set */
	SELECT
		 [armazem] = isnull(#dadosEvuData2.armazem,0)
		,[st].[ref]
		,[st].[design]
		,[STOCKADATA] = ISNULL(#dadosEvuData2.STOCKADATA,0)
		,[PclAData] = ISNULL(#dadosEvuData2.[pcl],[st].[EPCULT])
		,[Total] = ROUND(ISNULL(#dadosEvuData2.STOCKADATA * ISNULL(#dadosEvuData2.[pcl],[st].[EPCULT]),0),2)
		,[local1] = @local1
		,[local2] = @local2
		,[local3] = @local3
		,[psico] = ISNULL(fprod.psico,0)
        ,[benzo] = ISNULL(fprod.benzo,0)
	FROM
		[dbo].[st] (nolock)
		left join #dadosEvuData2 on [st].ref = #dadosEvuData2.ref
		left join fprod (nolock) on  fprod.cnp = [st].ref

	WHERE
		[st].[stns] = 0
		AND [st].[local] = CASE WHEN @local1 = '' THEN [st].[local] ELSE @local1 END
		AND [st].[u_local] = CASE WHEN @local2 = '' THEN [st].[u_local] ELSE @local2 END
		AND [st].[u_local2] = CASE WHEN @local3 = '' THEN [st].[u_local2] ELSE @local3 END
		AND ISNULL(#dadosEvuData2.STOCKADATA,0) >= @stockData
		AND [st].[ref] = case when @ref = '' then [st].[ref] else @ref end
		AND [st].[design] LIKE @design + '%'
		AND [st].site_nr = @site_nr
        AND (@psicotropicos = 0 OR fprod.psico = 1)
        AND (@benzodiazepinas = 0 OR fprod.benzo = 1)

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosStockData'))
		DROP TABLE #dadosStockData
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosEvuData'))
		DROP TABLE #dadosEvuData
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosEvuData2'))
		DROP TABLE #dadosEvuData2

GO
Grant Execute on dbo.up_relatorio_conferencia_inventarioData to Public
Grant control on dbo.up_relatorio_conferencia_inventarioData to Public
Go