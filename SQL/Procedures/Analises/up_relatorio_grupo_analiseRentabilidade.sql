/*
	Analise rentabilidade Grupo,
	compara valores de Fee, Sellout, Encomendas e Faturas
	exec up_relatorio_grupo_analiseRentabilidade '20140101','20150101','','',0,''
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_grupo_analiseRentabilidade]') IS NOT NULL
	drop procedure dbo.up_relatorio_grupo_analiseRentabilidade
go

create procedure dbo.up_relatorio_grupo_analiseRentabilidade
@dataIni as datetime
,@dataFim as datetime
,@tipo as varchar(20)
,@u_lab as varchar(150)
,@no numeric(10,0)
,@site as varchar(60)
/* WITH ENCRYPTION */
AS
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempSellOut'))
	BEGIN
		DROP TABLE #TempSellOut
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempEncFornec'))
	BEGIN
		DROP TABLE #TempEncFornec
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempMeses'))
	BEGIN
		DROP TABLE #TempMeses
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempEncClientes'))
	BEGIN
		DROP TABLE #TempEncClientes
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempfaturaClientes'))
	BEGIN
		DROP TABLE #TempfaturaClientes
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempCondComercForn'))
	BEGIN
		DROP TABLE #TempCondComercForn
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempConfEntregaClientes'))
	BEGIN
		DROP TABLE #TempConfEntregaClientes
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempDados'))
	BEGIN
		DROP TABLE #TempDados
	END
	;WITH cteMeses AS (
		SELECT dt = @dataIni, ano = YEAR(@dataIni), mes = MONTH(@dataIni)
		UNION ALL
		SELECT 
			DATEADD(mm, 1, dt), ano = YEAR(DATEADD(mm, 1, dt)), mes = MONTH(DATEADD(mm, 1, dt))
		FROM 
			cteMeses s
		WHERE 
			DATEADD(mm, 1, dt) <= @dataFim
	)
	select 	
		ultimoDiaMes = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,dt)+1,0))
		,* 
	INTO #TempMeses
	from	
		cteMeses
	select 
		bi.ref
		,bi.bistamp
		,PVP_encCliente = bi.edebito
		,QTT_encCliente = bi.qtt
		,bo.dataobra
		,bo.ousrhora
		,bo.nmdos
		,bo.nome
		,bo.no
		,bo.estab
		,bo.obrano
	INTO 
		#TempEncClientes
	from 
		bi (nolock)
		inner join bo (nolock) on bi.bostamp = bo.bostamp
	where 
		bo.ndos = 41
		and (@no = 0 or bo.no = @no)
	select 
		ref = #TempEncClientes.ref
		,bi.bistamp
		,PVP_encCliente = #TempEncClientes.PVP_encCliente
		,QTT_encCliente = #TempEncClientes.QTT_encCliente
		,dataobra = #TempEncClientes.dataobra
		,ousrhora = #TempEncClientes.ousrhora
		,nome = #TempEncClientes.nome
		,no = #TempEncClientes.no
		,estab = #TempEncClientes.estab
		,obrano = #TempEncClientes.obrano
		,nmdos = #TempEncClientes.nmdos
	INTO 
		#TempConfEntregaClientes
	from 
		bi (nolock)
		inner join bo (nolock) on bi.bostamp = bo.bostamp
		left join #TempEncClientes on #TempEncClientes.bistamp = bi.obistamp
	where 
		bo.ndos = 43
		and (@no = 0 or bo.no = @no)
	Select 
		fi.ref
		,TotalLinha_faturaCliente = 
			CASE WHEN Fi.IVAINCL = 0
						THEN FI.ETILIQUIDO
						ELSE FI.ETILIQUIDO / ((Fi.iva / 100)+1)
					END
		,QTT_faturaCliente = case when ft.tipodoc = 3 then fi.qtt*-1 else fi.qtt end
		,ft.fdata
		,ft.ousrhora
		,ano = year(ft.fdata)
		,mes = month(ft.fdata)
		,ft.nome
		,ft.no
		,ft.estab
		,ft.tipo
		,fi.bistamp
		,ft.site
		,ft.fno
		,ft.nmdoc
	INTO 
		#TempfaturaClientes
	from 
		fi (nolock)
		inner join ft (nolock) on ft.ftstamp = fi.ftstamp
	where
		ft.tipodoc != 4
		and ft.fdata between @dataIni and @dataFim
		and ft.tipo = case when @tipo = '' then ft.tipo else @tipo end
		and (@no = 0 or ft.no = @no)
	select 
		bi.ref
		,PVP_sellout = bi.edebito
		,QTT_sellout = bi.qtt
		,bo.dataobra
		,bo.ousrhora
		,ano = year(bo.dataobra)
		,mes = month(bo.dataobra)
		,bo.nome
		,bo.no
		,bo.estab
		,bo.obrano
	INTO 
		#TempSellOut
	from 
		bi (nolock)
		inner join bo (nolock) on bi.bostamp = bo.bostamp
	where 
		bo.ndos = 44 
		and (@no = 0 or bo.no = @no)
	select 
		bi.ref
		,FEE_A = bi.binum3
		,FEE_K = bi.num1
		,bo.dataobra
		,bo.ousrhora
		,ano = year(bo.dataobra)
		,mes = month(bo.dataobra) 
	INTO 
		#TempCondComercForn
	from 
		bi (nolock)
		inner join bo (nolock) on bi.bostamp = bo.bostamp
	where 
		bo.ndos = 45
	select 
		bi.ref
		,PCLK_sellout = bi.u_upc
		,bo.dataobra
		,bo.ousrhora
		,ano = year(bo.dataobra)
		,mes = month(bo.dataobra)
	INTO 
		#TempEncFornec
	from 
		bi (nolock)
		inner join bo (nolock) on bi.bostamp = bo.bostamp
	where 
		bo.ndos = 2
		and bo.dataobra between @dataIni and @dataFim
	Select 
		#TempMeses.ano
		,#TempMeses.mes
		,nome = isnull(#TempfaturaClientes.nome,'')
		,no = isnull(#TempfaturaClientes.no,0)
		,estab = isnull(#TempfaturaClientes.estab,0)
		,tipo = RTRIM(LTRIM(isnull(#TempfaturaClientes.tipo,'')))
		,st.ref
		,st.design
		,u_lab = RTRIM(LTRIM(st.u_lab))
		,docCliente = isnull(#TempfaturaClientes.nmdoc,0)
		,data_faturaCliente = isnull(#TempfaturaClientes.fdata,'')
		,Numero_faturaCliente = isnull(#TempfaturaClientes.fno,0)
		,PVP_faturaCliente = isnull(#TempfaturaClientes.TotalLinha_faturaCliente,0)
		,PVPsIva_faturaCliente = (isnull(#TempfaturaClientes.TotalLinha_faturaCliente,0)/ (isnull(taxasiva.taxa,0)/100+1))
		,QTT_faturaCliente = isnull(#TempfaturaClientes.QTT_faturaCliente,0)
		,TOTAL_faturaCliente = (
			isnull(#TempfaturaClientes.TotalLinha_faturaCliente,0)
		)
		,TOTALsIva_faturaCliente = (
			(isnull(#TempfaturaClientes.TotalLinha_faturaCliente,0)/ (isnull(taxasiva.taxa,0)/100+1))
			*
			isnull(#TempfaturaClientes.QTT_faturaCliente,0)
		)
		,Encomenda = isnull(isnull(#TempEncClientes.nmdos,#TempConfEntregaClientes.nmdos),'')	
		,EncObrano = isnull(isnull(#TempEncClientes.obrano,#TempConfEntregaClientes.obrano),0)
		,EncData = isnull(isnull(#TempEncClientes.dataobra,#TempConfEntregaClientes.dataobra),'19000101')
		,PVP_encCliente	= isnull(isnull(#TempEncClientes.PVP_encCliente,#TempConfEntregaClientes.PVP_encCliente),0)	
		,PVPsIva_encCliente	= (isnull(isnull(#TempEncClientes.PVP_encCliente,#TempConfEntregaClientes.PVP_encCliente),0) / (isnull(taxasiva.taxa,0)/100+1))
		,QTT_encCliente	= isnull(isnull(#TempEncClientes.QTT_encCliente,#TempConfEntregaClientes.QTT_encCliente),0)
		,TOTAL_encCliente = (
			isnull(isnull(#TempEncClientes.PVP_encCliente,#TempConfEntregaClientes.PVP_encCliente),0)	
			*
			isnull(isnull(#TempEncClientes.QTT_encCliente,#TempConfEntregaClientes.QTT_encCliente),0)
		)
		,TOTALsIva_encCliente = (
			(isnull(isnull(#TempEncClientes.PVP_encCliente / (isnull(taxasiva.taxa,0)/100+1),#TempConfEntregaClientes.PVP_encCliente  / (isnull(taxasiva.taxa,0)/100+1)),0)	)
			*
			isnull(isnull(#TempEncClientes.QTT_encCliente,#TempConfEntregaClientes.QTT_encCliente),0)
		)
		,FEE_A = isnull(
							(select 
								top 1 FEE_A 
							from 
								#TempCondComercForn 
							where 
								#TempCondComercForn.ref = st.ref 
								and #TempCondComercForn.dataobra <= #TempMeses.ultimoDiaMes
							order by 
								#TempCondComercForn.dataobra desc
								,#TempCondComercForn.ousrhora desc 
							) 
						,0)
		,FEE_K = isnull(
							(select 
								top 1 FEE_K 
							from 
								#TempCondComercForn 
							where 
								#TempCondComercForn.ref = st.ref 
								and #TempCondComercForn.dataobra <= #TempMeses.ultimoDiaMes
							order by 
								#TempCondComercForn.dataobra desc
								,#TempCondComercForn.ousrhora desc 
							) 
						,0)
		,PVP_sellout = isnull(
							(select 
								top 1 PVP_sellout 
							from 
								#TempSellOut 
							where 
								#TempSellOut.ref = st.ref 
								and #TempSellOut.dataobra <= #TempMeses.ultimoDiaMes
								and #TempSellOut.no = #TempfaturaClientes.no 
								and #TempSellOut.estab = #TempfaturaClientes.estab 
							order by 
								#TempSellOut.dataobra desc
								,#TempSellOut.ousrhora desc 
							) 
						,0)
		,PVPsIva_sellout = (
							isnull(
								(select 
									top 1 PVP_sellout 
								from 
									#TempSellOut 
								where 
									#TempSellOut.ref = st.ref 
									and #TempSellOut.dataobra <= #TempMeses.ultimoDiaMes
									and #TempSellOut.no = #TempfaturaClientes.no 
									and #TempSellOut.estab = #TempfaturaClientes.estab 
								order by 
									#TempSellOut.dataobra desc
									,#TempSellOut.ousrhora desc 
								) 
							,0) 
							/ 
							(isnull(taxasiva.taxa,0)/100+1)
						) 
		,PCLK_sellout = isnull(
							(select 
								top 1 PCLK_sellout 
							from 
								#TempEncFornec 
							where 
								#TempEncFornec.ref = st.ref 
								and #TempEncFornec.dataobra <= #TempMeses.ultimoDiaMes
							order by 
								#TempEncFornec.dataobra desc
								,#TempEncFornec.ousrhora desc 
							) 
						,0)
		,QTT_sellout = isnull(
							(select 
								SUM(QTT_sellout)
							from 
								#TempSellOut 
							where 
								#TempSellOut.ref = st.ref 
								and #TempSellOut.dataobra <= #TempMeses.ultimoDiaMes
								and #TempSellOut.no = #TempfaturaClientes.no 
								and #TempSellOut.estab = #TempfaturaClientes.estab 
								and #TempSellOut.ano = #TempfaturaClientes.ano 
								and #TempSellOut.mes = #TempfaturaClientes.mes 
							) 
						,0)
	Into #TempDados				
	from 
		#TempMeses
		,st (nolock)
		left join #TempfaturaClientes on st.ref = #TempfaturaClientes.ref 
		left join #TempEncClientes on #TempEncClientes.bistamp = #TempfaturaClientes.bistamp
		left join #TempConfEntregaClientes on #TempConfEntregaClientes.bistamp = #TempfaturaClientes.bistamp
		left join taxasiva (nolock) on st.tabiva = taxasiva.codigo
	Where
		#TempfaturaClientes.tipo = case when @tipo = '' then #TempfaturaClientes.tipo else @tipo end
		and st.u_lab = case when @u_lab = '' then st.u_lab else @u_lab end
		and #TempfaturaClientes.site = case when @site = '' then #TempfaturaClientes.site else @site end
	select 
		anomes = RTRIM(LTRIM(STR(ano))) + '/' + RTRIM(LTRIM(STR(mes)))
		,*
		,EncKValor = (TOTAL_encCliente * FEE_K) / 100
		,EncAValor = (TOTAL_encCliente * FEE_A) / 100
		,FacturaKValor = (TOTAL_faturaCliente * FEE_K) / 100
		,FacturaAValor = (TOTAL_faturaCliente * FEE_A) /100
	From	
		#TempDados 
	where 
		(@no = 0 or #TempDados.no = @no)
	order by
		no
		,estab
		,ref
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempSellOut'))
	BEGIN
		DROP TABLE #TempSellOut
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempEncFornec'))
	BEGIN
		DROP TABLE #TempEncFornec
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempMeses'))
	BEGIN
		DROP TABLE #TempMeses
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempEncClientes'))
	BEGIN
		DROP TABLE #TempEncClientes
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempfaturaClientes'))
	BEGIN
		DROP TABLE #TempfaturaClientes
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempCondComercForn'))
	BEGIN
		DROP TABLE #TempCondComercForn
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempDados'))
	BEGIN
		DROP TABLE #TempDados
	END
		
		
GO
Grant Execute On dbo.up_relatorio_grupo_analiseRentabilidade to Public
Grant Control On dbo.up_relatorio_grupo_analiseRentabilidade to Public
Go

