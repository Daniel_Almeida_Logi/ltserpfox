/* Utilizada no mesmo report da sp: up_relatorio_compras_resumoIva */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_compras_resumoIvaDocs]') IS NOT NULL
	drop procedure dbo.up_relatorio_compras_resumoIvaDocs
go

create procedure dbo.up_relatorio_compras_resumoIvaDocs
@documentos as varchar(254)

/* WITH ENCRYPTION */ 
AS

BEGIN

select 
	distinct cm,cmdesc 
from
	cm1 (nolock)
where 
	convert(varchar,cm1.cm) in (select items from dbo.up_splitToTable(@documentos,','))
union all
select 
	distinct cm, cmdesc
from
	sl (nolock)
where 
	convert(varchar,sl.cm) in (select items from dbo.up_splitToTable(@documentos,',') where items in (48,98))	
END

GO
Grant Execute on dbo.up_relatorio_compras_resumoIvaDocs to Public
Grant control on dbo.up_relatorio_compras_resumoIvaDocs to Public
Go