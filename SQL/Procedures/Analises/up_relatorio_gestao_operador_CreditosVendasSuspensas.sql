/* Relatório Vendas Suspensas Operador  

	exec up_relatorio_gestao_operador_CreditosVendasSuspensas '','','20210101','20220411','','', 0,1
	Ft.cobrado - controla documentos Supensos
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_operador_CreditosVendasSuspensas]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_operador_CreditosVendasSuspensas
go

create procedure dbo.up_relatorio_gestao_operador_CreditosVendasSuspensas
@no			varchar(9),
@estab		varchar(9),
@dataIni	datetime,
@dataFim	datetime,
@operador	varchar(9),
@site		varchar(20),
@pendentes	bit,
@cPlano		bit

/* WITH ENCRYPTION */
AS
	/* Calcula o valor à linha devido a regularizações parciais por produto*/
	select 
		ft.ftstamp
		,etiliquido = 
			case 
				when fi.desconto=0 and fi.u_descval=0 
					then round(fi.epv*(fi.qtt - isnull(FiQTT.qtt,0)),2) * (case when ft.tipodoc=3 then -1 else 1 end)
				else
					(round(fi.epv * (fi.qtt - isnull(FiQTT.qtt,0)) 
					- (fi.epv * (fi.qtt - isnull(FiQTT.qtt,0)) * (fi.desconto / 100.0)),2)
					- (fi.u_descval)) * (case when ft.tipodoc = 3 then -1 else 1 end)
			end	
		,ValorComp = u_ettent1 + u_ettent2
		,ValorCompReg = isnull(((u_ettent1 + u_ettent2) / fi.qtt) *  fiQTT.qtt,0)
	into 
		#TempSupsOperadorRegParcialProduto			
	from 
		ft (nolock)
		inner join fi (nolock) on ft.ftstamp = fi.ftstamp
		left join (
			select 
				ofistamp, 
				qtt = sum(fi.qtt)
			from 
				fi (nolock)
			where ndoc != 75
			group by
				ofistamp
		) as FiQTT on fi.fistamp = FiQTT.ofistamp
	where 
		(convert(varchar(9),ft.no) = case when @no= '' then convert(varchar(9),ft.no) else @no end)
		and (convert(varchar(9),ft.estab) = case when @estab='' then convert(varchar(9),ft.estab) else @estab end)
		and (convert(varchar(9),ft.vendedor) = case when @operador='' then convert(varchar(9),ft.vendedor) else @operador end)
		and ft.fdata between @dataIni and @dataFim
		and ft.site = case when @site = '' then ft.site else @site end
		and ft.cobrado = 1 and fi.qtt!=0 /* Suspenso */
		--and fi.qtt-isnull(FiQTT.qtt,0) > 0
		--select * from #TempSupsOperadorRegParcialProduto
	--
	Select 
		convert(varchar,ft.fdata,102) as datalc
		,ft.ousrhora
		,nmdoc = isnull(case when ft.cobrado=1 then ft.nmdoc + ' (S)' else ft.nmdoc end,'')
		,ft.fno
		,ft.no
		,ft.estab
		,ft.ftstamp
		,ndoc = isnull(ft.ndoc,0)
		,tipodoc = isnull(ft.tipodoc,0)
		,cliente = '[' + CONVERT(varchar,ft.no) +'][' + CONVERT(varchar,ft.estab) + '] ' + ft.nome
		,operador = ft.vendnm + ' [' + CONVERT(varchar,ft.vendedor) + ']'
		,valorVenda = ft.etotal
		,valorReg = 
			Case 
				when  isnull(ft.ndoc,0) = 1 then isnull(edebf - ecredf,ft.etotal)
				else
					ft.etotal -- - isnull((Select SUM(etiliquido) from #TempSupsOperadorRegParcialProduto where #TempSupsOperadorRegParcialProduto.ftstamp = ft.ftstamp),0)
			end
		,valorNreg = 
			Case 
				when  isnull(ft.ndoc,0) = 1 then isnull((cc.edeb-cc.edebf)-(cc.ecred-cc.ecredf),ft.etotal)
				else
					0 -- isnull((Select SUM(etiliquido) from #TempSupsOperadorRegParcialProduto where #TempSupsOperadorRegParcialProduto.ftstamp = ft.ftstamp),0)
			end
		,valorComp = isnull((Select SUM(ValorComp) from #TempSupsOperadorRegParcialProduto where #TempSupsOperadorRegParcialProduto.ftstamp = ft.ftstamp),0)
		,valorCompReg = isnull((Select SUM(ValorCompReg) from #TempSupsOperadorRegParcialProduto where #TempSupsOperadorRegParcialProduto.ftstamp = ft.ftstamp),0)
		,qttVenda = (select SUM(qtt) from fi where fi.ftstamp = ft.ftstamp)
		,qttReg = 
			isnull(Case when isnull(ft.ndoc,0) = 1 then
				(select SUM(eaquisicao) from fi where fi.ftstamp = ft.ftstamp)
			Else	
				(
				select 
					qtt = sum(fi.qtt)
				from 
					fi (nolock)
				where 
					ndoc != 75
					and	ofistamp in (select fistamp from fi where fi.ftstamp = ft.ftstamp) 
				)	
			end,0) 
		,qttNReg = 
			isnull((select SUM(qtt) from fi where fi.ftstamp = ft.ftstamp)
			-(
				Case when isnull(ft.ndoc,0) = 1 then
					(select SUM(eaquisicao) from fi where fi.ftstamp = ft.ftstamp) 
				Else	
					(
					select 
						qtt = sum(fi.qtt)
					from 
						fi (nolock)
					where 
						ndoc != 75
						and	ofistamp in (select fistamp from fi where fi.ftstamp = ft.ftstamp) 
					)	
				end 
			),0)
		,codigo = ft2.u_codigo
		,fi.fistamp as VendasSuspensas
		,fi.ofistamp
	INTO
		#TempSupsOperador
	from 
		ft (nolock)
		inner join fi (nolock) on fi.ftstamp = ft.ftstamp
		inner join ft2 (nolock) on ft2.ft2stamp = ft.ftstamp
		left join cc (nolock) on ft.ftstamp = cc.ftstamp
	where 
		(convert(varchar(9),ft.no) = case when @no='' then convert(varchar(9),ft.no) else @no end)
		and (convert(varchar(9),ft.estab) = case when @estab='' then convert(varchar(9),ft.estab) else @estab end)
		and (convert(varchar(9),ft.vendedor) = case when @operador='' then convert(varchar(9),ft.vendedor) else @operador end)
		and ft.fdata between @dataIni and @dataFim
		and ft.site = case when @site = '' then ft.site else @site end
		and ft.cobrado = 1 /* Suspenso */
		and (ft2.u_codigo != case when @cPlano = 1 then '' else '^[[:space:]]*$' end )
	order by 
		ft.no
		,ft.fdata desc
		,ft.ousrhora desc
	/* Result Set Final */
	IF @pendentes = 1
		Select distinct
			datalc
			,ousrhora
			,nmdoc
			,fno
			,cliente
			,valorVenda
			,ValorComp
			,valorCompReg
			,valorReg
			,valorNreg
			,no
			,estab
			,ftstamp
			,ndoc
			,tipodoc
			,operador
			,qttVenda
			,qttReg
			,qttNReg
			,codigo 
		from 
			#TempSupsOperador	
		Where
			VendasSuspensas not in (select fi.ofistamp from fi) 
	ELSE
		Select distinct
			datalc
			,ousrhora
			,nmdoc
			,fno
			,cliente
			,valorVenda
			,ValorComp
			,valorCompReg
			,valorReg
			,valorNreg
			,no
			,estab
			,ftstamp
			,ndoc
			,tipodoc
			,operador
			,qttVenda
			,qttReg
			,qttNReg
			,codigo 
		from 
			#TempSupsOperador	
	ENDIF
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempSupsOperador'))
	BEGIN
		DROP TABLE #TempSupsOperador
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempSupsOperadorRegParcialProduto'))
	BEGIN
		DROP TABLE #TempSupsOperadorRegParcialProduto
	END

GO
Grant Execute On dbo.up_relatorio_gestao_operador_CreditosVendasSuspensas to Public
Grant Control On dbo.up_relatorio_gestao_operador_CreditosVendasSuspensas to Public
Go

