/* Relatório Credito Dividas Clientes

 exec up_relatorio_credito_DividasClientes '20110101','20120101','',0,'Loja 1',''

*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_relatorio_credito_DividasClientes]') IS NOT NULL
	drop procedure dbo.up_relatorio_credito_DividasClientes
go

create procedure dbo.up_relatorio_credito_DividasClientes
@dataIni as datetime,
@dataFim as datetime,
@nome as varchar(55),
@no as numeric(10,0),
@site varchar(55),
@tipo varchar (20)
	
/* with encryption */
AS
BEGIN	

Select
	'CCSTAMP'		= cc.ccstamp, 
	'FTSTAMP'		= cc.ftstamp, 
	'NO'			= cc.no, 
	'NOME'			= cc.nome, 
	'DATALC'		= convert(varchar,cc.datalc,102), 
	'DATAVEN'		= convert(varchar,cc.dataven,102),
	'ECRED'			= cc.ecred, 
	'EDEB'			= cc.edeb, 
	'ORIGEM'		= cc.origem, 
	'MOEDA'			= cc.moeda, 
	'CMDESC'		= cc.cmdesc, 
	'FNO'			= ft.fno, 
	'ULTDOC'		= cc.ultdoc,
	'INTID'			= cc.intid, 
	'SALDO'			= ISNULL((SElect	SUM((a.ecred-a.ecredf))- SUM((a.edeb-a.edebf)) 
									From	fc a 
									Where	a.no = cc.no and a.estab = cc.estab
											and (abs((a.ecred-a.ecredf)-(a.edeb-a.edebf)) > 0.010000)
											and (a.ousrdata + a.ousrhora <= cc.ousrdata + cc.ousrhora))
							,0),			
	'DOCDATA'		= convert(varchar,isnull(ft.fdata,'19000101'),102), 
	'OUSRHORA'		= cc.ousrhora, 
	'USERNAME'		= (Select top 1 username from b_us where cc.ousrinis=b_us.iniciais), 
	'OUSRINIS'		= cc.ousrinis,
	'IEMISSAO'		= datediff(d,ft.fdata,GETDATE()),
	'IVENCIMENTO'	= datediff(d,cc.dataven,GETDATE()),
	'VALOR'			= (cc.ecred-cc.ecredf)-(cc.edeb-cc.edebf), 
	'VERDOC'		= CONVERT(bit,0),
	LOJA			= ft.site
from
	cc (nolock) 
	left join b_utentes (nolock) on cc.no = b_utentes.no and cc.estab=b_utentes.estab
	left join ft (nolock) on ft.ftstamp=cc.ftstamp
where
	cc.datalc between @dataIni and @dataFim
	and (ECRED - ECREDF) + (EDEB - EDEBF) != 0
	and ft.site = (case when @site = '' Then ft.site else @site end)
	and cc.no = case when @no = 0 then cc.no else @no end
	and cc.nome = case when @nome = '' then cc.nome else @nome end
	and b_utentes.tipo = case when @tipo = '' then b_utentes.tipo else @tipo end
	
order by
	no, datalc, ousrhora

END
GO
Grant Execute on dbo.up_relatorio_credito_DividasClientes to Public
Grant control on dbo.up_relatorio_credito_DividasClientes to Public
GO
