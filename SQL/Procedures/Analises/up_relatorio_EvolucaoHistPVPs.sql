-- Relatório Histórico PVP's
-- exec up_relatorio_EvolucaoHistPVPs '20000101','20150101',''

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_EvolucaoHistPVPs]') IS NOT NULL
	drop procedure dbo.up_relatorio_EvolucaoHistPVPs
go

create procedure dbo.up_relatorio_EvolucaoHistPVPs
@dataIni		as datetime,
@dataFim		as datetime,
@ref			as varchar(254)

/* with encryption */

AS 
BEGIN

	set language portuguese

	;with 
		cteHist as (
			Select 
				*
			from 
				a_HistPrecosInf
			where 
				PRECOID = 1 Or PRECOID = 501
	),
	ctefprod as (
		Select 
			cnp, generico
		from
			fprod (nolock)
	)
	,cteVendas as (
			SELECT	
				ref = fi.ref, 
				design = fi.design,
				qtd = case when ft.tipodoc!=3 then fi.qtt else -fi.qtt end
			FROM	ft (nolock)
					inner join fi (nolock) on ft.ftstamp=fi.ftstamp
					inner join st on fi.ref=st.ref
					inner join td (nolock) on td.ndoc = ft.ndoc
			WHERE	ft.no > 199
					and (ft.fdata between @dataIni and @dataFim) 
					and	ft.anulado=0 
					and (td.tipodoc!=4 or u_tipodoc=4) AND u_tipodoc != 1 AND u_tipodoc != 5 
	),
	cteRefs as (
		Select 
			st.ref
			,st.design
			,fprod.generico
			
		From
			st (nolock)
			inner join fprod (nolock) on st.ref = fprod.cnp
	)

	select 
		ref
		,design
		,generico
		,PVP2007 = isnull((
			Select 
				top 1 preco 
			From
				cteHist
			Where
				cteHist.CNP = cteRefs.ref
				and ano = 2007
				and PRECOID = 1
		),0)
		,PVP2008 = isnull((
				Select 
					top 1 preco 
				From
					cteHist
				Where
					cteHist.CNP = cteRefs.ref
					and ano = 2008
					and PRECOID = 1
		),0)
		,PVP2009 = isnull((
				Select 
					top 1 preco 
				From
					cteHist
				Where
					cteHist.CNP = cteRefs.ref
					and ano = 2009
					and PRECOID = 1
		),0),PVP2010 = isnull((
				Select 
					top 1 preco 
				From
					cteHist
				Where
					cteHist.CNP = cteRefs.ref
					and ano = 2010
					and PRECOID = 1
					 
		),0),PVP2011 = isnull((
				Select 
					top 1 preco 
				From
					cteHist
				Where
					cteHist.CNP = cteRefs.ref
					and ano = 2011
				order by
					PRECOID desc
					 
		),0),PVP2012 = isnull((
				Select 
					top 1 preco 
				From
					cteHist
				Where
					cteHist.CNP = cteRefs.ref
					and ano = 2012
				order by
					PRECOID desc
					 
		),0),PVP2013 = isnull((
				Select 
					top 1 preco 
				From
					cteHist
				Where
					cteHist.CNP = cteRefs.ref
					and ano = 2013
				order by
					PRECOID desc
					 
		),0)
		, qtt = isnull((
			Select 
				sum(qtd)
			From
				cteVendas
			Where
				cteVendas.ref = cteRefs.ref
		),0)
		
	from 
		cteRefs
	Where 
		ref = case when @ref = '' then ref else @ref end
		
END

GO
Grant Execute on dbo.up_relatorio_EvolucaoHistPVPs to Public
Grant control on dbo.up_relatorio_EvolucaoHistPVPs to Public
GO