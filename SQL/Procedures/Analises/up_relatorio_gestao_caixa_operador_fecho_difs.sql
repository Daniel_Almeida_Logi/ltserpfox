/* Relatório Caixas Operador

	exec up_relatorio_gestao_caixa_operador_fecho_difs '20220908','20220908', 0, 999,'FORTALEZA'

 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_caixa_operador_fecho_difs]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_caixa_operador_fecho_difs
go


create procedure [dbo].up_relatorio_gestao_caixa_operador_fecho_difs
	@dataIni DATETIME
	,@dataFim DATETIME
	,@opIni	as numeric(9,0)
	,@opFim	as numeric(9,0)
	,@site varchar(30)

/* with encryption */

AS

	declare @usaPagCentral bit
	declare @gestaoCxOperador bit
	set @usaPagCentral = ISNULL((select bool from b_parameters where stamp='ADM0000000079'),0)
	set @gestaoCxOperador = (select gestao_cx_operador from empresa where site = @site)

	IF @gestaoCxOperador = 0
		begin
			select
				cxstamp
				,cx.site
				,Data = convert(varchar,cx.dabrir,102)
				,dfechar = convert(varchar,cx.dfechar,102)
				,habrir
				,hfechar		
				,cx.pnome
				,cx.pno
				,Vendedor			= cx.fuserno
				,Nome				= cx.fusername
				,cx.fecho_dinheiro
				,cx.fecho_mb
				,cx.fecho_visa
				,cx.fecho_cheques
				,cx.fecho_epaga3
				,cx.fecho_epaga4
				,cx.fecho_epaga5
				,cx.fecho_epaga6
				,cx.fundocaixa
				,cx.fechada
				,fecho_total =  cx.fecho_dinheiro + cx.fecho_mb + cx.fecho_visa + cx.fecho_cheques + cx.fecho_epaga3 + cx.fecho_epaga4 + cx.fecho_epaga5 + cx.fecho_epaga6
				,obs = causa
			from 
				cx (nolock)
			where 
				cx.dabrir between @dataini and @DataFim 
				and cx.fuserno >= @opIni
				and cx.fuserno <= @opFim
				and cx.site = case when @site = '' then cx.site else @site end 
				and cx.fechada = 1
			order by cx.fusername
		end
	else
		begin
			with diferencas as (
				select
						ss.site
						,Data				= convert(varchar,ss.dabrir,102)
						,Vendedor			= ss.fuserno
						,Nome				= ss.fusername
						,Dinheiro		= sum(case when left(pc.nrAtend,2) = 'Cx' then Total else pc.evdinheiro end) *(-1)
						,Multibanco	= sum(pc.epaga2)*(-1)
						,Visa			= sum(pc.epaga1)*(-1)
						,Cheques		= sum(pc.echtotal)*(-1)
						,epaga3				= sum(pc.epaga3)*(-1)
						,epaga4				= sum(pc.epaga4)*(-1)
						,epaga5				= sum(pc.epaga5)*(-1)
						,epaga6				= sum(pc.epaga6)*(-1)
					--	,total_movExtra		= sum(case when left(pc.nrAtend,2) = 'Cx' then pc.evdinheiro else 0 end)
					from 
						ss (nolock) 
						inner join B_pagCentral pc on pc.ssstamp = ss.ssstamp
					--	inner join ft on ft.u_nratend=pc.nrAtend
					where 
						ss.dabrir between @dataini and @DataFim 
						and ss.site = case when @site = '' then ss.site else @site end 
						and ss.fuserno >= @opIni
						and ss.fuserno <= @opFim
						and ss.fechada = 1
						and pc.ignoraCaixa = 0
						--AND ss.ssstamp = (case 
						--					when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
						--					then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
						--					else ss.ssstamp
						--				end)
					group by 
						ss.site
						,ss.dabrir
						,ss.fuserno
						,ss.fusername
						,ss.pnome
						,ss.pno
			
				union all

				select
					ss.site
					,Data = convert(varchar,ss.dabrir,102)
					,Vendedor			= ss.fuserno
					,Nome				= ss.fusername
					,Dinheiro = ss.evdinheiro
					,Multibanco		= ss.epaga1
					,Visa		= ss.epaga2
					,Cheques  = ss.echtotal
					,epaga3 = ss.fecho_epaga3
					,epaga4 = ss.fecho_epaga4
					,epaga5 = ss.fecho_epaga5
					,epaga6 = ss.fecho_epaga6
				from 
					ss (nolock)
				where 
					ss.dabrir between @dataini and @DataFim 
					and ss.fuserno >= @opIni
					and ss.fuserno <= @opFim
					and ss.site = case when @site = '' then ss.site else @site end 
					and ss.fechada = 1
					AND ss.ssstamp = (case 
										when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
										then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
										else ss.ssstamp
									end)
			)
			select 
				site,
				data,
				vendedor,
				nome,
				dinheiro = cast(sum(dinheiro) as numeric(15,2)),
				multibanco = cast(sum(multibanco) as numeric(15,2)),
				visa = cast(sum(visa) as numeric(15,2)),
				cheques = cast(sum(cheques) as numeric(15,2)),
				epaga3 = cast(sum(epaga3) as numeric(15,2)),
				epaga4 = cast(sum(epaga4) as numeric(15,2)),
				epaga5 = cast(sum(epaga5) as numeric(15,2)),
				epaga6 = cast(sum(epaga6) as numeric(15,2))
			from diferencas
			group by site,data,	Vendedor,nome
			order by nome
	end
	
GO
Grant Execute On dbo.up_relatorio_gestao_caixa_operador_fecho_difs to Public
Grant control On dbo.up_relatorio_gestao_caixa_operador_fecho_difs to Public
GO