/* Relat�rio Caixas Operador

	exec up_relatorio_gestao_caixa_operador_fecho_difs_ang '20221020','20221020', 0,999,''

 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_caixa_operador_fecho_difs_ang]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_caixa_operador_fecho_difs_ang
go


create procedure [dbo].up_relatorio_gestao_caixa_operador_fecho_difs_ang
	@dataIni DATETIME
	,@dataFim DATETIME
	,@opIni	as numeric(9,0)
	,@opFim	as numeric(9,0)
	,@site varchar(30)

/* with encryption */

AS

	select
		 ft.fdata
		, ft.vendedor
		, ft.site
		, ft.pnome
		, ft.pno
		, ft.ndoc
		, sum(case when ft.nmdoc='Nota de Cr�dito'  then -pc.creditos else  pc.creditos end) as creditos
	into
		#vendasCreditos
	from ft (nolock) 
	inner join b_pagcentral (nolock) pc on ft.u_nratend = pc.nrAtend and ft.site = pc.site
	INNER JOIN TD (NOLOCK) ON ft.ndoc = TD.ndoc 
	 where nmDocExt NOT  in ('CCO', 'FCO')
	  and  fdata between @dataini and @DataFim  
	group by ft.fdata, ft.vendedor, ft.site , ft.pnome, ft.pno, ft.ndoc,ft.u_nratend,ft.nmdoc, ft.nmdoc


	declare @usaPagCentral bit
	declare @gestaoCxOperador bit
	set @usaPagCentral = ISNULL((select bool from b_parameters where stamp='ADM0000000079'),0)
	set @gestaoCxOperador = (select gestao_cx_operador from empresa where site = @site)

	IF @gestaoCxOperador = 0
		begin
			select
				cx.cxstamp
				,cx.site
				,Data = convert(varchar,cx.dabrir,102)
				,dfechar = convert(varchar,cx.dfechar,102)
				,habrir
				,hfechar		
				,cx.pnome
				,cx.pno
				,Vendedor			= cx.fuserno
				,Nome				= cx.fusername
				,cx.fecho_dinheiro
				,cx.fecho_mb
				,cx.fecho_visa
				,cx.fecho_cheques
				,cx.fecho_epaga3
				,cx.fecho_epaga4
				,cx.fecho_epaga5
				,cx.fecho_epaga6
				,cx.fundocaixa
				,cx.fechada
				,fecho_total =  cx.fecho_dinheiro + cx.fecho_mb + cx.fecho_visa + cx.fecho_cheques + cx.fecho_epaga3 + cx.fecho_epaga4 + cx.fecho_epaga5 + cx.fecho_epaga6
				,obs = causa
				,Total_Credito		= ISNULL((select SUM(ISNULL(#vendasCreditos.creditos,0)) from #vendasCreditos(NOLOCK) where cx.site=#vendasCreditos.site and cx.dabrir=#vendasCreditos.fdata and cx.fuserno=#vendasCreditos.vendedor),0)
			from 
				cx (nolock)
			where 
				cx.dabrir between @dataini and @DataFim 
				and cx.fuserno >= @opIni
				and cx.fuserno <= @opFim
				and cx.site = case when @site = '' then cx.site else @site end 
				and cx.fechada = 1
			group by 
				cx.cxstamp
				,cx.site
				,cx.dabrir
				,cx.pnome
				,cx.pno
				,fundocaixa
				,dfechar
				,habrir
				,hfechar	
				,cx.pnome
				,cx.pno
				,cx.fuserno
				,cx.fusername
				,cx.fecho_dinheiro
				,cx.fecho_mb
				,cx.fecho_visa
				,cx.fecho_cheques
				,cx.fecho_epaga3
				,fecho_epaga4		
				,fecho_epaga5		
				,cx.fecho_epaga6
				,cx.fundocaixa
				,cx.fechada
				,cx.causa
				order by fusername
		end
	else
		begin
			with diferencas as (
				select
						ss.site
						,Data				= convert(varchar,ss.dabrir,102)
						,Vendedor			= ss.fuserno
						,Nome				= ss.fusername
						,Dinheiro			= (sum(case when left(pc.nrAtend,2) = 'Cx' then 0 else pc.evdinheiro end) *(-1)) - sum(case when pc.nrAtend like 'CX%' then pc.total else 0 end)
						,Multibanco			= sum(pc.epaga2)*(-1)
						,Visa				= sum(pc.epaga1)*(-1)
						,Cheques			= sum(pc.echtotal)*(-1)
						,epaga3				= sum(pc.epaga3)*(-1)
						,epaga4				= sum(pc.epaga4)*(-1)
						,epaga5				= sum(pc.epaga5)*(-1)
						,epaga6				= sum(pc.epaga6)*(-1)
						,Total_Credito		= ISNULL((select SUM(ISNULL(#vendasCreditos.creditos,0)) from #vendasCreditos(NOLOCK) where ss.site=#vendasCreditos.site and ss.dabrir=#vendasCreditos.fdata and pc.vendedor=#vendasCreditos.vendedor),0)
					from 
						ss (nolock) 
						inner join B_pagCentral pc on pc.ssstamp = ss.ssstamp
					where 
						ss.dabrir between @dataini and @DataFim 
						and ss.site = case when @site = '' then ss.site else @site end 
						and ss.fuserno >= @opIni
						and ss.fuserno <= @opFim
						and ss.fechada = 1
						and pc.ignoraCaixa = 0
					group by 
						ss.site
						,ss.dabrir
						,ss.fuserno
						,ss.fusername
						,ss.pnome
						,ss.pno
						,pc.vendedor
			
				union all

				select
					ss.site
					,Data = convert(varchar,ss.dabrir,102)
					,Vendedor			= ss.fuserno
					,Nome				= ss.fusername
					,Dinheiro = ss.evdinheiro - ss.fundoCaixa
					,Multibanco		= ss.epaga1
					,Visa		= ss.epaga2
					,Cheques  = ss.echtotal
					,epaga3 = ss.fecho_epaga3
					,epaga4 = ss.fecho_epaga4
					,epaga5 = ss.fecho_epaga5
					,epaga6 = ss.fecho_epaga6
					,Total_Credito		= ISNULL((select SUM(ISNULL(#vendasCreditos.creditos,0)) from #vendasCreditos(NOLOCK) where ss.site=#vendasCreditos.site and ss.dabrir=#vendasCreditos.fdata and ss.fuserno =#vendasCreditos.vendedor),0)
				from 
					ss (nolock)
				where 
					ss.dabrir between @dataini and @DataFim 
					and ss.fuserno >= @opIni
					and ss.fuserno <= @opFim
					and ss.site = case when @site = '' then ss.site else @site end 
					and ss.fechada = 1
					AND ss.ssstamp = (case 
										when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
										then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by usrdata desc, usrhora  desc)
										else ss.ssstamp
									end)
			)
			select 
				site,
				data,
				vendedor,
				nome,
				dinheiro = cast(sum(dinheiro) as numeric(15,2)),
				multibanco = cast(sum(multibanco) as numeric(15,2)),
				visa = cast(sum(visa) as numeric(15,2)),
				cheques = cast(sum(cheques) as numeric(15,2)),
				epaga3 = cast(sum(epaga3) as numeric(15,2)),
				epaga4 = cast(sum(epaga4) as numeric(15,2)),
				epaga5 = cast(sum(epaga5) as numeric(15,2)),
				epaga6 = cast(sum(epaga6) as numeric(15,2)),
				Total_Credito = cast(sum(epaga6) - Total_Credito     as numeric(15,2))
			from diferencas
			group by site,data,	Vendedor,nome, Total_Credito
			order by Nome
	end
	
GO
Grant Execute On dbo.up_relatorio_gestao_caixa_operador_fecho_difs_ang to Public
Grant control On dbo.up_relatorio_gestao_caixa_operador_fecho_difs_ang to Public
GO