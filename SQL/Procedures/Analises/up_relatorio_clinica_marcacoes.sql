/*
	 exec up_relatorio_clinica_marcacoes '19000101', '20190101','', ''
*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_relatorio_clinica_marcacoes]') IS NOT NULL
    drop procedure up_relatorio_clinica_marcacoes
go

create PROCEDURE up_relatorio_clinica_marcacoes
@dataIni as datetime
,@dataFim as datetime
,@especialista varchar(100) = ''
,@site varchar(60) = ''
/* WITH ENCRYPTION */
AS
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosEspecialista'))
		DROP TABLE #dadosEspecialista
	Select 
		mrstamp 
	into
		#dadosEspecialista
	from 
		marcacoesRecurso (nolock)
	where 
		marcacoesRecurso.nome = case when @especialista = '' then marcacoesRecurso.nome else @especialista end
		and marcacoesRecurso.tipo = 'Utilizadores'
	Select 
		data
		,hinicio
		,hfim
		,utente = marcacoes.nome
		,marcacoes.no
		,marcacoes.obs
		,id_us
		,especialidadeServicos = convert(varchar(254),left(isnull(substring((select distinct ', '+nome AS 'data()' from b_cli_stRecursos where tipo = 'Especialidade' and b_cli_stRecursos.ref in(select ref from marcacoesServ where marcacoesServ.mrstamp = marcacoes.mrstamp) for xml path('')),3, 255) ,''),254))
		,especialistas = convert(varchar(254),left(isnull(substring((select distinct ', '+nome AS 'data()' from marcacoesRecurso where marcacoesRecurso.mrstamp = marcacoes.mrstamp for xml path('')),3, 255) ,''),254))
		,servicos = convert(varchar(254),left(isnull(substring((select distinct ', '+design AS 'data()' from marcacoesServ where marcacoesServ.mrstamp = marcacoes.mrstamp for xml path('')),3, 255) ,''),254))
		,contacto = RTRIM(LTRIM(isnull(b_utentes.telefone,'') + ' ' + isnull(b_utentes.tlmvl,'') + ' ' + isnull(b_utentes.email,'')))
		,local = marcacoes.site
		, estado
	from 
		marcacoes
		left join b_utentes on marcacoes.no = b_utentes.no and marcacoes.estab = b_utentes.estab
	where
		convert(varchar,data,112) between @dataIni and @dataFim
		and (@especialista = '' or marcacoes.mrstamp in (Select mrstamp from #dadosEspecialista))
		and marcacoes.site = case when @site = '' then marcacoes.site else @site end 
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosEspecialista'))
		DROP TABLE #dadosEspecialista	
GO
Grant Execute On up_relatorio_clinica_marcacoes to Public
Grant Control On up_relatorio_clinica_marcacoes to Public
go
