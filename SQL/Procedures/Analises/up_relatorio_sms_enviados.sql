/*
SMS Enviadas

exec up_relatorio_sms_enviados -1,-1,'','19000101','20250121'


*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_sms_enviados]') IS NOT NULL
    drop procedure dbo.up_relatorio_sms_enviados
go

create procedure dbo.up_relatorio_sms_enviados

@total		int,
@status		int,
@nome		varchar(250),
@dataA		datetime,
@dataDe		datetime

/* WITH ENCRYPTION */
AS

/*
Declare @remetente varchar(20)

SELECT 
    @remetente = LTRIM(RTRIM(ISNULL(textValue, '')))
FROM 
    empresa (NOLOCK)
INNER JOIN 
    b_parameters_site (NOLOCK) 
    ON b_parameters_site.site = empresa.site
WHERE 
    no = @site
    AND stamp = 'ADM0000000026'
*/

select 
	username
	,total
	,status
	,name
	,[dataenvio] = convert(date,dataenvio,101)
	,horaenvio
	,case when name='RGPD' then '' else mensagem end as mensagem
	,listTlm
from 
	b_sms_fact (nolock)
where
	total = case when @total>=0 then @total else total end
	and status = case when @status>=0 then @status else status end
	and name like case when @nome='' then name else '%'+@nome+'%' end
	and dataenvio between @dataA and @dataDe
	--and remetente = case where @remetente then @remetente else remetente end 
order by
	dataenvio asc
	,horaenvio asc

GO
Grant Execute on dbo.up_relatorio_sms_enviados to Public
Grant Control on dbo.up_relatorio_sms_enviados to Public
go
