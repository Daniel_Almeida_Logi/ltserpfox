/* Relatório Vendas Ano

	 exec up_relatorio_vendas_ano '2020','2020','','','','','','','Loja 1',''

	 exec up_relatorio_vendas_ano '2020','2020','','','','','','MSRM','Loja 1','' 

*/
if OBJECT_ID('[dbo].[up_relatorio_vendas_ano]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_ano
go

create procedure [dbo].[up_relatorio_vendas_ano]
	@anoIni int
	,@anoFim int
	,@op varchar(max)
	,@ref varchar(254)
	,@lab varchar(100)
	,@marca	varchar(200)
	,@familia varchar(max)
	,@atributosFam varchar(max)
	,@site varchar(55)
	,@design varchar(60)

/* with encryption */

AS
SET NOCOUNT ON

	declare @dataIni as datetime = CONVERT(VARCHAR(4), @anoIni) + '-01-01'
	declare @dataFim as datetime = CONVERT(VARCHAR(4), @anoFim) + '-12-31'

	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
		DROP TABLE #dadosFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRefs'))
		DROP TABLE #dadosRefs
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
		DROP TABLE #dadosSt	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
		DROP TABLE #EmpresaArm	
	
	SET @atributosFam = (CASE WHEN (@atributosFam IS NULL OR @atributosFam = '') THEN ';;;' ELSE @atributosFam END)

	DECLARE @departamento VARCHAR(254) = ''
	SET @departamento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 1),'') 
	DECLARE @sessao       VARCHAR(254) = ''
	set @sessao = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 2) ,'')
	DECLARE @categoria    VARCHAR(254) = ''
	SET @categoria = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 3),'') 
	DECLARE @segmento     VARCHAR(254) = ''
	SET @segmento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 4),'') 

	/* Calc Loja e Armazens */
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)

	select 
		armazem
	into
		#EmpresaArm
	From
		empresa
		inner join empresa_arm on empresa.no = empresa_arm.empresa_no
	where
		empresa.site = case when @site = '' then empresa.site else @site end	

	/* Define se vais mostrar resultados abaixo do Cliente 199 */
	declare @Entidades as bit
	set @Entidades = case when (select tipoempresa from empresa where site = @site) in ('FARMACIA', 'PARAFARMACIA') then convert(bit,1) else convert(bit,0) end

	/* Calc Familia*/
	Select 
		distinct ref 
	into 
		#dadosFamilia
	From 
		stfami (nolock)  
	Where 
		ref in (Select items from dbo.up_splitToTable(@familia,',')) 
		Or @familia = '0' 
		or @familia = ''

	/* Calc Operadores */
	Select
		iniciais, cm = userno, username
	into
		#dadosOperadores
	from
		b_us (nolock)
	Where
		userno in (select items from dbo.up_splitToTable(@op,','))
		or @op= '0'
		or @op= ''

	/* Calc Ref's */
	Select 
		ref
	into
		#dadosRefs
	from
		st (nolock)
	Where
		ref in (select items from dbo.up_splitToTable(@ref,','))
		or @ref= ''
		AND site_nr = @site_nr

	/* Calc Dados Produto  */
	Select
		st.ref
		,st.design
		,usr1
		,u_lab	
		, ISNULL(A.descr,'') as departamento
		, ISNULL(B.descr,'') as seccao
		, ISNULL(C.descr,'') as categoria
		, ISNULL(D.descr,'') as segmento

		,dispdescr = ISNULL(dispdescr,'')
		,generico = ISNULL(generico,CONVERT(bit,0))
		,psico = ISNULL(psico,CONVERT(bit,0))
		,benzo = ISNULL(benzo,CONVERT(bit,0))
		,protocolo = ISNULL(protocolo,CONVERT(bit,0))
		,dci = ISNULL(dci,'')
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 1) valDepartamento
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 2) valSeccao
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 3) valCategoria
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 4) valSegmento
	INTO 
		#dadosSt
	From
		st (nolock)
		left join fprod (nolock) on st.ref = fprod.cnp	
		left join grande_mercado_hmr (nolock) A on A.id = st.u_depstamp
		left join mercado_hmr (nolock) B on B.id = st.u_secstamp
		left join categoria_hmr (nolock) C on C.id = st.u_catstamp 
		left join segmento_hmr (nolock) D on D.id = st.u_segstamp	
	Where	
		st.u_lab = case when @lab = '' then u_lab else @lab end
		AND st.usr1 = case when @marca = '' then usr1 else @marca end		
		AND site_nr = @site_nr

	/* Dados Base Vendas  */
	create table #dadosVendasBase (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(3,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)
	)
    
    insert #dadosVendasBase
    exec up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site

	/* Elimina Vendas tendo em conta tipo de cliente*/
	If @Entidades = 1
	delete from #dadosVendasBase where #dadosVendasBase.no < 199




	/* Calc Result Set */
	Select 
		[nrAtend]
		,[ftstamp]
		,[fdata]
		,[iva]
		,[etiliquido]
		,[etiliquidoSiva]
		,[ettent1]
		,[ettent2]
		,[ettent1siva]
		,[ettent2siva]
	From
		#dadosVendasBase 
		Left Join #dadosSt on #dadosVendasBase.ref = #dadosSt.ref
	WHERE
		#dadosVendasBase.fdata between @dataIni and @dataFim
		and #dadosVendasBase.u_tipodoc not in (1, 5)
		and isnull(#dadosSt.u_lab,'') = CASE When @lab = '' Then isnull(#dadosSt.u_lab,'') Else @lab End
		and isnull(#dadosSt.usr1,'') = CASE When @marca = '' Then isnull(#dadosSt.usr1,'') Else @marca End
		and #dadosVendasBase.ousrinis in (select iniciais from #dadosOperadores)
		and #dadosVendasBase.ref in (Select ref from #dadosRefs)
		and #dadosVendasBase.familia in (Select ref from #dadosFamilia)
		and #dadosVendasBase.design like case when @design = '' then #dadosVendasBase.design else @design + '%' end
		AND departamento = (CASE WHEN @departamento = '' THEN departamento ELSE @departamento END)
		AND seccao = (CASE WHEN @sessao = '' THEN seccao ELSE  @sessao END)
		AND categoria = (CASE WHEN @categoria = '' THEN categoria ELSE  @categoria END)
		AND segmento = (CASE WHEN @segmento = '' THEN segmento ELSE  @segmento END) 

	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
		DROP TABLE #dadosFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRefs'))
		DROP TABLE #dadosRefs
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
		DROP TABLE #dadosSt	


GO
Grant Execute on dbo.up_relatorio_vendas_ano to Public
Grant control on dbo.up_relatorio_vendas_ano to Public
GO