-- exec up_relatorio_exportTermal ''

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_exportTermal]') IS NOT NULL
	drop procedure dbo.up_relatorio_exportTermal
go

create procedure dbo.up_relatorio_exportTermal
@design as varchar(60)

/* with encryption */

AS 
BEGIN
	
	Select 	
		cpr = 'CPR'
		,nom = 'NOM'
		,fap = 'FAP'
		,sac = 'SAC'
		,stm = 'STM'
		,smi = 'SMI'
		,qte = 'QTE'
		,duv = 'DUV'
		,duc = 'DUC'
		,pvp = 'PVP'
		,pcu = 'PCU'
		,iva = 'IVA'
		,cat = 'CAT'
		,ct1 = 'CT1'
		,gen = 'GEN'
		,gpr = 'GPR'
		,cla = 'CLA'
		,tpr = 'TPR'
		,car = 'CAR'
		,gam = 'GAM'
		,[v(0)] = 'V(0)'
		,[v(1)] = 'V(1)'
		,[v(2)] = 'V(2)'
		,[v(3)] = 'V(3)'
		,[v(4)] = 'V(4)'
		,[v(5)] = 'V(5)'
		,[v(6)] = 'V(6)'
		,[v(7)] = 'V(7)'
		,[v(8)] = 'V(8)'
		,[v(9)] = 'V(9)'
		,[v(10)] = 'V(10)'
		,[v(11)] = 'V(11)'
		,[v(12)] = 'V(12)'
		,[v(13)] = 'V(13)'
		,[v(14)] = 'V(14)'
		,[v(15)] = 'V(15)'
		,[v(16)] = 'V(16)'
		,[v(17)] = 'V(17)'
		,[v(18)] = 'V(18)'
		,[v(19)] = 'V(19)'
		,[v(20)] = 'V(20)'
		,[v(21)] = 'V(21)'
		,[v(22)] = 'V(22)'
		,[v(23)] = 'V(23)'
		,dtval = 'DTVAL'
		,fpd = 'FPD'
		,lad = 'LAD'
		,prateleira = 'PRATELEIRA'
		,gama = 'GAMA'
		,grupoHomogeneo = 'GRUPOHOMOGENEO'
		,inactivo = 'INACTIVO'
	union all 
	Select 	
		cpr = ltrim(rtrim(st.ref))
		,nom = ltrim(rtrim(st.design))
		,fap = isnull(ltrim(rtrim(fformasdescr)),'')
		,sac = ''
		,stm = convert(varchar,st.stmax)
		,smi = convert(varchar,st.stmin)
		,qte = convert(varchar,st.eoq)
		,duv = ''
		,duc = ''
		,pvp = ltrim(rtrim(convert(varchar,REPLACE(Convert(money,round(st.epv1,2)),',','.'))))
		,pcu = ltrim(rtrim(convert(varchar,REPLACE(Convert(money,round(st.epcusto,2)),',','.'))))
		,iva = ltrim(rtrim(convert(varchar,REPLACE(Convert(money,round(taxasiva.taxa,2)),',','.'))))
		,cat = ''
		,ct1 = ''
		,gen = case when fprod.generico=1 then 'S' else 'N' end
		,gpr = ''
		,cla = ''
		,tpr = ''
		,car = ''
		,gam = ltrim(rtrim(u_local))
		,[v(0)] = ''
		,[v(1)] = ''
		,[v(2)] = ''
		,[v(3)] = ''
		,[v(4)] = ''
		,[v(5)] = ''
		,[v(6)] = ''
		,[v(7)] = ''
		,[v(8)] = ''
		,[v(9)] = ''
		,[v(10)] = ''
		,[v(11)] = ''
		,[v(12)] = ''
		,[v(13)] = ''
		,[v(14)] = ''
		,[v(15)] = ''
		,[v(16)] = ''
		,[v(17)] = ''
		,[v(18)] = ''
		,[v(19)] = ''
		,[v(20)] = ''
		,[v(21)] = ''
		,[v(22)] = ''
		,[v(23)] = ''
		,dtval = ''
		,fpd = ltrim(rtrim(st.fornecedor)) 
		,lad = ltrim(rtrim(st.u_lab))
		,prateleira = ''
		,gama = ltrim(rtrim(st.u_local2))
		,grupoHomogeneo = LTRIM(rtrim(isnull(fprod.grphmgdescr,'')))
		,inactivo = case when st.inactivo=1 then 'S' else 'N' end
	From
		st (nolock)
		left join fprod (nolock) on st.ref = fprod.cnp
		left join taxasiva (nolock) on st.tabiva = taxasiva.codigo
	Where 
		st.design like @design + '%'
		and st.familia != 1
END

GO
Grant Execute on dbo.up_relatorio_exportTermal to Public
Grant control on dbo.up_relatorio_exportTermal to Public
GO