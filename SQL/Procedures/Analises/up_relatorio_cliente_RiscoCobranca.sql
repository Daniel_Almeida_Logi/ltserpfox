/*
 Relatório Credito Dividas Clientes
 Última alteração : 2012.01.18

 alteração a 2020-08-05 por JG: contemplar input de multiselecao de sites
 
 
 exec up_relatorio_cliente_RiscoCobranca '','Entidade','Loja 1'
 exec up_relatorio_cliente_RiscoCobranca '','Medicamentosa','Loja 1'
 exec up_relatorio_cliente_RiscoCobranca '','Medicamentosa','Loja 2'
 exec up_relatorio_cliente_RiscoCobranca '','Medicamentosa','Loja 1,Loja 2'


 exec up_relatorio_cliente_RiscoCobranca '','','Loja 1'

 
 exec up_relatorio_cliente_RiscoCobranca '','Medicamentosa',''
 exec up_relatorio_cliente_RiscoCobranca '','','Loja 1,Loja 3'


*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_relatorio_cliente_RiscoCobranca]') IS NOT NULL
	drop procedure dbo.up_relatorio_cliente_RiscoCobranca
go

create procedure dbo.up_relatorio_cliente_RiscoCobranca
@nome as varchar(55),
@tipo varchar(55),
@site varchar(254)
/* with encryption */
AS
BEGIN	


if @site = ''
		set @site = isnull((select  convert(varchar(254),(select 
							site + ', '
						from 
							empresa (nolock)							
						FOR XML PATH(''))) as no),0)

;with cteVendas as (
	Select 
		b_utentes.nome
		,b_utentes.no
		,b_utentes.estab
		,b_utentes.tipo
		,b_utentes.esaldo
		,dataUltVenda = MAX(ft.fdata)
		,ft.site
	From
		ft (nolock)
		inner join b_utentes (nolock) on b_utentes.no = b_utentes.no and ft.estab = b_utentes.estab
	Where
		ft.no != 200
		and b_utentes.esaldo < 0
	Group
		by
			b_utentes.nome
			,b_utentes.no
			,b_utentes.estab
			,b_utentes.tipo
			,b_utentes.esaldo
			,ft.site
)
Select 
	*
	,NrMesesSVendas = DATEDIFF(MM,dataUltVenda,GETDATE())
From 
	cteVendas
Where
	cteVendas.nome = case when @nome = '' then cteVendas.nome else @nome end
	--and cteVendas.site = case when @site = '' then cteVendas.site else @site end
	and cteVendas.site in (Select items from dbo.up_splitToTable(@site, ',')) 
	and cteVendas.tipo = case when @tipo = '' then cteVendas.tipo else @tipo end
Order
	by cteVendas.nome
END

GO
Grant Execute on dbo.up_relatorio_cliente_RiscoCobranca to Public
Grant control on dbo.up_relatorio_cliente_RiscoCobranca to Public
GO