-- Relatório Conferencia Inventário antes Lançamento
-- exec up_relatorio_DiferencasInventarioAntesLancamento 'Inventario1',''

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_DiferencasInventarioAntesLancamento]') IS NOT NULL
	drop procedure dbo.up_relatorio_DiferencasInventarioAntesLancamento
go

create procedure dbo.up_relatorio_DiferencasInventarioAntesLancamento
@descricao		varchar(60)
,@site			varchar(55)

/* with encryption */

AS 
BEGIN

	Select 
		stic.descricao
		,stil.ref
		,stil.design
		,stil.armazem
		,stockInventario = stil.stock
		,stockActualArmazem = isnull(sa.stock,0)
		,difQt = stil.stock - isnull(sa.stock,0)
		,stil.zona
	from	
		stic (nolock)
		inner join stil (nolock) on stic.sticstamp = stil.sticstamp
		left join sa (nolock) on stil.ref = sa.ref and stil.armazem = sa.armazem
	Where
		descricao = @descricao
	Order by 
		stil.stock - isnull(sa.stock,0) desc

END

GO
Grant Execute on dbo.up_relatorio_DiferencasInventarioAntesLancamento to Public
Grant control on dbo.up_relatorio_DiferencasInventarioAntesLancamento to Public
Go