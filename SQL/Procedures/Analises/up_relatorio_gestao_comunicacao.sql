/* 
	devolve a quantidade vendida de um determinado mes de um tipo 

	exec up_relatorio_gestao_comunicacao '', '', '','',''


*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_comunicacao]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_comunicacao
go

create procedure dbo.up_relatorio_gestao_comunicacao
@ano		INT,
@mes		INT,
@infarmed	VARCHAR(20),
@tipo		VARCHAR(10),
@localidade	VARCHAR(45)
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON
	DECLARE @tipoInt int;
	SET @tipoInt = CASE WHEN @tipo ='' THEN 0 ELSE CONVERT(INT, RTRIM(LTRIM(@tipo))) END
	SELECT
		b_utentes.id, 
		b_utentes.nome,
		qtt				AS quantidade,
		comm.month		AS mes ,
		comm.year		AS ano,
		b_utentes.local AS Localidade
	FROM
		ext_esb_communications comm (nolock)
		INNER JOIN b_utentes (nolock) ON REPLACE(LTRIM(REPLACE(comm.senderId ,'0',' ')),' ','0') = b_utentes.id 
	WHERE
		comm.month					= CASE WHEN @mes		= '' THEN comm.month ELSE @mes END 
		AND comm.year				= CASE WHEN @ano		= '' THEN comm.year ELSE @ano END
		AND comm.senderId			= CASE WHEN @infarmed	= '' THEN comm.senderId ELSE @infarmed END
		AND comm.comunicationType	= CASE WHEN @tipoInt	= 0	 THEN comm.comunicationType ELSE @tipo END 
		AND b_utentes.local			= CASE WHEN @localidade = '' THEN b_utentes.local ELSE @localidade END
		AND valid = 1 and test = 0 
	ORDER BY 
		comm.year desc ,
		comm.month desc
	
	
Go
Grant Execute on dbo.up_relatorio_gestao_comunicacao to Public
Grant Control on dbo.up_relatorio_gestao_comunicacao to Public
Go