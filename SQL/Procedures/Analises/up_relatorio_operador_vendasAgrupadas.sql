/* Relatório Vendas Desconto Produto

	exec up_relatorio_operador_vendasAgrupadas '20190701', '20210617','Loja 1'
	exec up_relatorio_operador_vendasAgrupadas '20190701', '20210617','Loja 2'
	exec up_relatorio_operador_vendasAgrupadas '20190701', '20210617','Loja 1,Loja 2'
	exec up_relatorio_operador_vendasAgrupadas '20210101', '20210621', 'Loja 1'
*/

if OBJECT_ID('[dbo].[up_relatorio_operador_vendasAgrupadas]') IS NOT NULL
	drop procedure dbo.up_relatorio_operador_vendasAgrupadas
go

create procedure [dbo].[up_relatorio_operador_vendasAgrupadas]
	 @dataIni		datetime
	,@dataFim		datetime
	,@site			varchar(55)


/* with encryption */

AS
SET NOCOUNT ON

	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadorSuma'))
		DROP TABLE #dadosOperadorSuma

	/* Dados Base Vendas  */
	create table #dadosVendasBase (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)
	)
	declare @Entidades as bit

	set @Entidades = case when (select tipoempresa from empresa where site = @site) in ('FARMACIA', 'PARAFARMACIA') then convert(bit,1) else convert(bit,0) end

    insert #dadosVendasBase
    exec up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site

		
	/* Elimina Vendas tendo em conta tipo de cliente*/
	If @Entidades = 1
	delete from #dadosVendasBase where #dadosVendasBase.no < 199

	SELECT 
		vendnm,
		SUM(CASE WHEN (FPROD.generico=1 and FPROD.pvporig>0)THEN  (etiliquidoSiva + ettent1siva + ettent2siva)	ELSE 0 END ) 					 AS vendasGenerico, 
		SUM(CASE WHEN (FPROD.generico=1 and FPROD.pvporig>0)THEN (ecusto) ELSE 0 END ) 															 AS custoGenerico,
		SUM(CASE WHEN (FPROD.generico=1 and FPROD.pvporig>0)THEN (etiliquidoSiva + ettent1siva + ettent2siva) - (ecusto) ELSE 0 END ) 			 AS margemGenerico,
		SUM(CASE WHEN (FPROD.pvporig=0)THEN  (etiliquidoSiva + ettent1siva + ettent2siva)	ELSE 0 END ) 										 AS vendasPSPM, 
		SUM(CASE WHEN (FPROD.pvporig=0)THEN (ecusto) ELSE 0 END ) 																				 AS custoPSPM,
		SUM(CASE WHEN (FPROD.pvporig=0)THEN (etiliquidoSiva + ettent1siva + ettent2siva) - (ecusto) ELSE 0 END ) 								 AS margemPSPM , 
		SUM((etiliquidoSiva + ettent1siva + ettent2siva))																						 AS VendasTotal,
		sum((ecusto))																															 AS custoTotal,
		sum((etiliquidoSiva + ettent1siva + ettent2siva) - (ecusto))																			 AS margemTotal
	INTO #dadosOperadorSuma
	FROM #dadosVendasBase
		INNER JOIN FPROD (NOLOCK) ON FPROD.cnp= #dadosVendasBase.REF
	GROUP BY vendnm

	
	SELECT
		vendnm																															AS vendedor, 
		vendasGenerico																													AS vendasGenerico, 
		custoGenerico																													AS custoGenerico,
		margemGenerico																													AS margemGenerico,
		Round((CASE WHEN vendasGenerico!=0  THEN (margemGenerico / vendasGenerico)*100 ELSE 0 END),2)									AS margemPercGenerico,
		vendasPSPM																														AS vendasPSPM, 
		custoPSPM																														AS custoPSPM,
		margemPSPM																														AS margemPSPM, 
		Round( (CASE WHEN vendasPSPM!=0  THEN (margemPSPM / vendasPSPM)*100 ELSE 0 END),2)												AS margemPercPSPM,
		vendasGenerico + vendasPSPM																										AS vendasTotal,
		custoGenerico + custoPSPM																										AS custoTotal, 
		margemGenerico + margemPSPM																										AS margemTotal, 
		Round( (CASE WHEN (vendasGenerico + vendasPSPM)!=0 THEN  ((margemGenerico + margemPSPM)/ (vendasGenerico + vendasPSPM))*100 ELSE 0 END),2)	AS margemPercTotal
	FROM #dadosOperadorSuma



	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadorSuma'))
		DROP TABLE #dadosOperadorSuma

GO
Grant Execute on dbo.up_relatorio_operador_vendasAgrupadas to Public
Grant control on dbo.up_relatorio_operador_vendasAgrupadas to Public
GO