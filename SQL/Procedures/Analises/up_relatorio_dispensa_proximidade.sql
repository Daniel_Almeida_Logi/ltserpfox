/* 

	exec up_relatorio_dispensa_proximidade '19000101','20280908','','Loja 1','','',''
	exec up_relatorio_dispensa_proximidade '20250101','20280908','','Loja 1','107902511','','',''
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_dispensa_proximidade]') IS NOT NULL
	DROP PROCEDURE dbo.up_relatorio_dispensa_proximidade
GO

CREATE PROCEDURE dbo.up_relatorio_dispensa_proximidade
@dataIni		AS DATETIME,
@dataFim		AS DATETIME,
@ref			AS VARCHAR(254),
@site			AS VARCHAR(55),
@no				AS VARCHAR(254) = '',
@receita		AS VARCHAR(254),
@prod			AS bit,
@serv			AS bit
/* with encryption */
AS 
SET NOCOUNT ON
BEGIN 	

	SELECT 
		 ft.ousrdata												AS data,
		 b_utentes.nome												AS cliente,
		 b_utentes.no												AS ncliente,
	 	 b_utentes.nbenef											AS nutente,
		 fi.nmdoc												    AS doc,
		 fi.ref														AS ref,
		 fi.fno														AS ndoc,
		 u_receita													AS nreceita,
		 fi.design													AS design,
		 fi.qtt														AS qtd,
		 fi2.idEpisodio												AS nepisodio,
		 fi2.idProcesso												AS nprocesso,
		 CONCAT(ft2.obsCl,' ', ft2.obsdoc,' ', ft2.obsInt)			AS obs
	FROM  ft (NOLOCK)
		INNER JOIN ft2(nolock) on ft2.ft2stamp = ft.ftstamp
		INNER JOIN fi (nolock) on fi.ftstamp = ft2.ft2stamp
		INNER JOIN fi2 (nolock) on fi2.fistamp = fi.fistamp
		INNER JOIN b_utentes (nolock) on ft.no = b_utentes.no and b_utentes.estab = ft.estab
		INNER JOIN empresa (nolock) on ft.site = empresa.site
		inner join st (nolock) on st.ref = fi.ref and site_nr = empresa.no
	WHERE 
		SUBSTRING(ft2.u_receita, 2, 2) = '11'
		and ft.site = @site
		and ft.ousrdata between @dataIni and @dataFim
		and ft2.u_receita = CASE WHEN @receita = '' THEN ft2.u_receita ELSE @receita END
		and ft.no = CASE WHEN @no = '' THEN ft.no ELSE @no END
		and (@ref = '' OR fi.ref IN (select items from up_SplitToTable(@ref, ',')))
		and st.stns =  CASE
							WHEN @prod = 1 AND @serv = 0 THEN 0
							WHEN @prod = 0 AND @serv = 1 THEN 1
							ELSE st.stns
						END
END
GO
Grant Execute on dbo.up_relatorio_dispensa_proximidade to Public
Grant control on dbo.up_relatorio_dispensa_proximidade to Public
GO