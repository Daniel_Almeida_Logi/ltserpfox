-- Relatório Contabilidade Vendas
-- exec up_relatorio_gestao_contab_vendasContab '20100101','20130101',''
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_contab_vendasContab]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_contab_vendasContab
go

create procedure dbo.up_relatorio_gestao_contab_vendasContab
@dataIni as datetime,
@dataFim as datetime,
@site varchar(60)
/* with encryption */
AS
	SELECT	
			EMPRESA		= empresa.EMPRESA
			,LOJA		= empresa.site
			,NUMLOJA	= empresa.ref
			,FDATA		as  DATADOC,
			FT.NOME		as NOMECLI,
			FT.NO		as NUMCLI,
			FT.NMDOC	as DOCUMENTO, 
			FT.FNO		as NUMDOC,
			FI.REF,
			DESIGN,
			QTT,
			UNIDADE,
			fi.TABIVA,
			IVA,
			DESCONTO,
			DESC2,
			ETILIQUIDO as 'UTENTE',
			/* CASE WHEN (SELECT COUNT(*) AS VALOR FROM CTLTRCT WHERE CTLTRCTstamp = FT.u_ltstamp) > 0 THEN u_ettent1 ELSE 0 END +	CASE WHEN (SELECT COUNT(*) AS VALOR FROM CTLTRCT WHERE CTLTRCTstamp = FT.u_ltstamp2) > 0 THEN u_ettent2 ELSE 0 END as COMPARTICIPACAO, */
			CASE
				WHEN TD.tipodoc = 3 THEN /* DEV. A CLIENTE */
					((CASE WHEN FI.ivaincl = 1	THEN (FI.u_EPVP/(1+(IVA/100))) ELSE FI.u_EPVP END) * qtt )*-1
				ELSE
					(CASE WHEN FI.ivaincl = 1	THEN (FI.u_EPVP/(1+(IVA/100))) ELSE FI.u_EPVP END) * qtt
			END as EPVP,
			ISNULL(CASE WHEN TD.tipodoc = 3 THEN /* DEV. A CLIENTE */
					(	SELECT	TOP 1 EVU 
						FROM	SL (nolock) 
						WHERE	(ORIGEM = 'FO' OR ORIGEM = 'IF')
								AND SL.DATALC <= FT.FDATA
								AND SL.REF = FI.REF
								AND EVU !=0
						ORDER BY datalc DESC
					) *qtt *-1
				ELSE
					(	SELECT	TOP 1 EVU 
						FROM	SL (nolock) 
						WHERE	(ORIGEM = 'FO' OR ORIGEM = 'IF')
								AND SL.DATALC <= FT.FDATA
								AND SL.REF = FI.REF
								AND EVU !=0
						ORDER BY datalc DESC
					) *qtt 
			END,0)	 as PCL,
			(	CASE
					WHEN TD.tipodoc = 3 THEN /* DEV. A CLIENTE */
						((CASE WHEN FI.ivaincl = 1	THEN (FI.u_EPVP/(1+(IVA/100))) ELSE FI.u_EPVP END) * qtt )*-1
					ELSE
						(CASE WHEN FI.ivaincl = 1	THEN (FI.u_EPVP/(1+(IVA/100))) ELSE FI.u_EPVP END) * qtt
				END 
				-
				CASE WHEN TD.tipodoc = 3 THEN /* DEV. A CLIENTE */
					(	SELECT	TOP 1 EVU 
						FROM	SL (nolock) 
						WHERE	(ORIGEM = 'FO' OR ORIGEM = 'IF')
								AND SL.DATALC <= FT.FDATA
								AND SL.REF = FI.REF
								AND EVU !=0
						ORDER BY datalc DESC
					) *qtt *-1
				ELSE
					ISNULL((	SELECT	TOP 1 EVU 
						FROM	SL (nolock) 
						WHERE	(ORIGEM = 'FO' OR ORIGEM = 'IF')
								AND SL.DATALC <= FT.FDATA
								AND SL.REF = FI.REF
								AND EVU !=0
						ORDER BY datalc DESC
					) *qtt ,0)
				END
			) as MARGEM,
			CASE WHEN ft.cobrado=1 THEN 'SUSPENSA'
				ELSE ''
			END as ESTADO
	FROM	
		FT (nolock) 
		INNER JOIN FI ON FI.FTSTAMP = FT.FTSTAMP
		INNER JOIN TD ON FT.NDOC = TD.NDOC
		left join empresa_arm on empresa_arm.armazem = fi.armazem
		Left join empresa on empresa.no = empresa_arm.empresa_no
	WHERE	
		(FT.tipodoc!=4 or u_tipodoc=4) 
		AND U_TIPODOC <> 1 AND U_TIPODOC <> 5 
		AND ft.anulado=0 
		AND (fi.ref != '' OR qtt != 0 OR etiliquido !=0)
		AND FT.FDATA >= @dataIni AND FT.FDATA <= @dataFim
		AND stns=0 and ft.no>199
		and ft.site = (case when @site = '' Then ft.site else @site end)
	ORDER BY 
		FT.FDATA, FT.ousrhora

GO
Grant Execute on dbo.up_relatorio_gestao_contab_vendasContab to Public
Grant Control on dbo.up_relatorio_gestao_contab_vendasContab to Public
Go
