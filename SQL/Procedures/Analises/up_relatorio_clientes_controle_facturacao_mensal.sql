/* Relatório Clientes Controle Facturacao Mensal
	
	exec up_relatorio_clientes_controle_facturacao_mensal '20151001' , '20161231', 0, 0, '', 0, 0, 1 ,'Loja 1'
	
*/ 

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_relatorio_clientes_controle_facturacao_mensal]') IS NOT NULL
	drop procedure dbo.up_relatorio_clientes_controle_facturacao_mensal
go

create procedure dbo.up_relatorio_clientes_controle_facturacao_mensal
	@dataIni			as datetime,
	@dataFim			as datetime,
	@no					as numeric(9,0),
	@estab				as numeric(9,0),
	@tipoCliente		varchar(20),
	@ivaInc				bit, 
	@incNcredito		bit,
	@entfac				bit,
	@site				varchar(55)
/* with encryption */
AS
BEGIN	
	set language portuguese	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosfaturacao'))
		DROP TABLE #dadosfaturacao
	/* Prepara result set faturacao */
	select
		ft.no
		,ft.estab
		,saldo = isnull(sum(case when @ivaInc = 1 then 
											case when fi.ivaincl = 1 then fi.etiliquido else fi.etiliquido + (fi.etiliquido*(fi.iva/100)) end
									   else
											case when fi.ivaincl = 1 then (fi.etiliquido / (fi.iva/100+1)) else fi.etiliquido end			
									   end),0)
		,ano = YEAR(ft.fdata)
		,mes = UPPER(DATENAME(month,ft.fdata))
		,mesNo = month(ft.fdata)
	into 
		#dadosfaturacao
	from
		ft (nolock) 
		inner join fi (nolock) on ft.ftstamp = fi.ftstamp
		inner join td (nolock) on td.ndoc = ft.ndoc
	where
		fi.composto = 0
		and ft.anulado = 0
		and ft.tipodoc != 4 
		and ft.tipodoc != (case when @incNcredito = 0 then 3 else 4 end)
		and ft.site = (case when @site = '' Then ft.site else @site end)
		and ft.no = case when @no = 0 then ft.no else @no end
		and ft.estab = case when @estab = 0 then ft.estab else @estab end
		and fdata between @dataIni and @dataFim
	group by 
		ft.no
		,ft.estab
		,YEAR(ft.fdata)
		,UPPER(DATENAME(month,ft.fdata))
		,month(ft.fdata)
	/* result set final */
	select 
		nome = b.nome + '[' + LTRIM(STR(b.no)) + '][' +  LTRIM(STR(b.estab))  +']'
		,b.no
		,b.estab
		,b.Tipo
		,saldo = isnull(#dadosfaturacao.saldo, 0)
		,ano = isnull(#dadosfaturacao.ano, (select min(ano) from #dadosfaturacao)) 
		,mes = isnull(#dadosfaturacao.mes, UPPER(DATENAME(month,@dataIni)))
		,mesOrder = isnull(#dadosfaturacao.mesNo,month(@dataIni))
	from 
		b_utentes b (nolock)
		left join #dadosfaturacao on b.no = #dadosfaturacao.no and b.estab = #dadosfaturacao.estab
	where
		b.tipo = case when @tipoCliente = '' then b.tipo else @tipoCliente end
		and b.inactivo = 0
		and b.no = case when @no = 0 then b.no else @no end
		and b.estab = case when @estab = 0 then b.estab else @estab end
		and b.entfact = case when @entfac = 0 then entfact else 1 end
	order by 
		b.nome
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosfaturacao'))
		DROP TABLE #dadosfaturacao
END
GO
Grant Execute on dbo.up_relatorio_clientes_controle_facturacao_mensal to Public
Grant control on dbo.up_relatorio_clientes_controle_facturacao_mensal to Public
GO

