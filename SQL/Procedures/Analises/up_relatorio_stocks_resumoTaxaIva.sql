/* Calculo para valor e qtt stock dividido por Tx. Iva

	exec up_relatorio_stocks_resumoTaxaIva '', '', '', '', 'Loja 1', '', '20210625', 0, -9999
	exec up_relatorio_stocks_resumoTaxaIva '6633073', '', '', '', 'Loja 2', '', '20210625', 0, -9999
*/
	

if OBJECT_ID('[dbo].[up_relatorio_stocks_resumoTaxaIva]') IS NOT NULL
	drop procedure dbo.up_relatorio_stocks_resumoTaxaIva
go

create procedure dbo.up_relatorio_stocks_resumoTaxaIva
	@ref			 varchar(254),
	@familia		 varchar(max),
	@lab			 varchar(254),
	@marca			 varchar(200),
	@site			 varchar(55),
	@atributosFam	 varchar(max),
	@data			 datetime,
	@incluiInactivos bit, 
	@stockData		 numeric(9,0)
/* WITH ENCRYPTION */ 
AS
BEGIN
	If OBJECT_ID('tempdb.dbo.#tmp_taxasIva') IS NOT NULL
		drop table #tmp_taxasIva;
	If OBJECT_ID('tempdb.dbo.#tmp_Lojas') IS NOT NULL
		drop table #tmp_Lojas;
	If OBJECT_ID('tempdb.dbo.#tmp_Familia') IS NOT NULL
		drop table #tmp_Familia;
	If OBJECT_ID('tempdb.dbo.#tmp_St') IS NOT NULL
		drop table #tmp_St;
	If OBJECT_ID('tempdb.dbo.#tmp_Dados') IS NOT NULL
		drop table #tmp_Dados;
	If OBJECT_ID('tempdb.dbo.#tmp_StockData') IS NOT NULL
		drop table #tmp_StockData;
	If OBJECT_ID('tempdb.dbo.#tmp_PCLData2') IS NOT NULL
		drop table #tmp_PCLData2;
	If OBJECT_ID('tempdb.dbo.#tmp_PCLData') IS NOT NULL
		drop table #tmp_PCLData;

	SET @atributosFam = (CASE WHEN (@atributosFam IS NULL OR @atributosFam = '') THEN ';;;' ELSE @atributosFam END)

	DECLARE @departamento VARCHAR(254) = ''
	SET @departamento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 1),'') 
	DECLARE @sessao       VARCHAR(254) = ''
	set @sessao = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 2) ,'')
	DECLARE @categoria    VARCHAR(254) = ''
	SET @categoria = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 3),'') 
	DECLARE @segmento     VARCHAR(254) = ''
	SET @segmento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 4),'') 

	/* Calc Loja e Armazens */
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)
	select 
		armazem,
		empresa_no
	into
		#Armazens
	From
		empresa
		inner join empresa_arm on empresa.no = empresa_arm.empresa_no
	where
		empresa.site = case when @site = '' then empresa.site else @site end	
	/* Calc Pcl à data*/
	Select
		ref
		,pcl_data = ROUND(sl.evu,2)
		,ROW_NUMBER() OVER(Partition by sl.ref ORDER BY sl.datalc + sl.usrhora DESC)  as id
	into
		#tmp_PCLData2
	From	
		sl (nolock)
		inner join #Armazens on sl.armazem = #Armazens.armazem
	Where	
		(ORIGEM = 'FO' OR ORIGEM = 'IF')
		AND sl.datalc <= @data 
		AND sl.evu != 0
		AND REF = case when @ref = '' then ref else @ref end

	Select
		*
	into
		#tmp_PCLData
	From 
		#tmp_PCLData2
	where 
		id = 1
	/* Calc Stock à data */
	SELECT	
		sl.ref
		,sl.armazem
		,stock_data = SUM(CASE WHEN sl.cm < 50 THEN sl.qtt ELSE -sl.qtt END)
	Into
		#tmp_StockData
	FROM	
		sl (nolock)
		inner join St (nolock) on sl.ref= st.ref  and st.site_nr = ISNULL((select no from empresa where site = @site),0)
		inner join #Armazens on sl.armazem = #Armazens.armazem
	where
		sl.datalc <= @data and St.stns=0
		AND sl.ref = case when @ref = '' then sl.ref else @ref end
		and St.site_nr = @site_nr
	Group by
		sl.ref
		,sl.armazem
	/* Estado (inativas?) das refs */
	/*remove ref with  < @stockData */
	delete #tmp_StockData where stock_data < @stockData

	/* Calc Taxas Iva disponíveis */
	select 
		codigo
		,taxa
	into
		#tmp_taxasIva
	from 
		taxasiva
	/* Calc Familias Infarmed*/
	Select 
		distinct ref 
	into
		#tmp_Familia
	From
		stfami (nolock)
	Where
		ref in (Select items from dbo.up_splitToTable(@familia,','))
		or @familia = '0' 
		or @familia = ''	
	/* Calc Produtos ST */
	Select
		st.ref 
		,st.design
		,faminome
		,familia
		,usr1
		,u_lab
		,tabiva
		,epcpond
		,epcult
		,PCL	=
			ROUND(ISNULL((
				SELECT	
					TOP 1 EVU 
				FROM	
					 SL (nolock) 
					 inner join #Armazens on sl.armazem = #Armazens.armazem
				WHERE	
					(ORIGEM = 'FO' OR ORIGEM = 'IF')
					AND SL.REF = ST.REF
					AND SL.DATALC <= @data
					AND EVU !=0
				ORDER BY 
					sl.datalc + sl.ousrhora DESC
			),st.EPCULT),2)
		,epcusto
		,epv1
		,marg1
		,stns	
		, ISNULL(A.descr,'') as departamento
		, ISNULL(B.descr,'') as seccao
		, ISNULL(C.descr,'') as categoria
		, ISNULL(D.descr,'') as segmento
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 1) valDepartamento
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 2) valSeccao
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 3) valCategoria
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 4) valSegmento
		,inactivo = st.inactivo
	Into
		#tmp_St
	From
		st (nolock)  		
		left join grande_mercado_hmr (nolock) A on A.id = st.u_depstamp
		left join mercado_hmr (nolock) B on B.id = st.u_secstamp
		left join categoria_hmr (nolock) C on C.id = st.u_catstamp 
		left join segmento_hmr (nolock) D on D.id = st.u_segstamp	
	where 
		site_nr =  @site_nr
		AND st.ref = case when @ref = '' then st.ref else @ref end

	/* Preparar Result Final*/

	Select
		 sa.ref
		,Sum(sa.stock) stock
		,cteSt.design
		,pcl = cteSt.PCL
		,total = cteSt.PCL * Sum(sa.stock)
		,cteSt.faminome
		,iva = ISNULL(cteTaxasiva.taxa,0)
		,tabiva
		,stock_data =case when CONVERT(DATE,@data) = CONVERT(DATE,GETDATE()) then Sum(sa.stock) else   ISNULL(Sum(#tmp_StockData.stock_data),0) END 
		,pcl_data =  case when CONVERT(DATE,@data) = CONVERT(DATE,GETDATE()) then cteSt.PCL  else   ISNULL((#tmp_PCLData.pcl_data),cteSt.PCL)END
		,lab = cteSt.u_lab
		,marca = cteSt.usr1
	into
		#tmp_Dados
	From
		#tmp_St  cteSt (nolock)
		left join sa   on sa.ref = cteSt.ref
		left join #tmp_taxasIva as cteTaxasiva on cteSt.tabiva = cteTaxasiva.codigo
		left join #tmp_StockData on  #tmp_StockData.ref = sa.ref and #tmp_StockData.armazem = sa.armazem
		left join #tmp_PCLData on #tmp_PCLData.ref = sa.ref
		inner join #Armazens on #Armazens.armazem = sa.armazem
		inner join empresa on empresa.no = #Armazens.empresa_no
	Where
		cteSt.stns = 0
		and cteSt.inactivo = case when @incluiInactivos = 1 then cteSt.inactivo else 0 end
		and cteSt.ref = case when @ref = '' then cteSt.ref else @ref end 
		and cteSt.u_lab = case when @lab = '' then u_lab else @lab end
		and usr1 = case when @marca = '' then usr1 else @marca end 
		and (cteSt.familia in (Select ref from #tmp_Familia) or familia = 0 or @familia = '')
		AND departamento = (CASE WHEN @departamento = '' THEN departamento ELSE @departamento END)
		AND seccao = (CASE WHEN @sessao = '' THEN seccao ELSE  @sessao END)
		AND categoria = (CASE WHEN @categoria = '' THEN categoria ELSE  @categoria END)
		AND segmento = (CASE WHEN @segmento = '' THEN segmento ELSE  @segmento END) 
		and empresa.site = @site

		and sa.stock >= case when (CONVERT(DATE,GETDATE())=@data) then @stockData else -9999 end 
	group by 
		 sa.ref
		,cteSt.design
		,cteSt.PCL
		,cteSt.faminome
		,cteTaxasiva.taxa
		,tabiva
		,#tmp_PCLData.pcl_data
		,cteSt.u_lab
		,cteSt.usr1
	
	--	
	Select
		 tabiva
		,ref
		,design
		,stock_data
		,pcl_data
		,total_data =  stock_data * pcl_data 
		,stock
		,pcl
		,total
		,iva
		,faminome
		,lab
		,marca
	From
		#tmp_Dados	
	order by 
		stock_data desc

	If OBJECT_ID('tempdb.dbo.#tmp_taxasIva') IS NOT NULL
		drop table #tmp_taxasIva;
	If OBJECT_ID('tempdb.dbo.#tmp_Lojas') IS NOT NULL
		drop table #tmp_Lojas;
	If OBJECT_ID('tempdb.dbo.#tmp_Familia') IS NOT NULL
		drop table #tmp_Familia;
	If OBJECT_ID('tempdb.dbo.#tmp_St') IS NOT NULL
		drop table #tmp_St;
	If OBJECT_ID('tempdb.dbo.#tmp_Dados') IS NOT NULL
		drop table #tmp_Dados;
	If OBJECT_ID('tempdb.dbo.#tmp_StockData') IS NOT NULL
		drop table #tmp_StockData;
	If OBJECT_ID('tempdb.dbo.#tmp_PCLData2') IS NOT NULL
		drop table #tmp_PCLData2;
	If OBJECT_ID('tempdb.dbo.#tmp_PCLData') IS NOT NULL
		drop table #tmp_PCLData;

END

GO
Grant Execute on dbo.up_relatorio_stocks_resumoTaxaIva to Public
Grant control on dbo.up_relatorio_stocks_resumoTaxaIva to Public
GO