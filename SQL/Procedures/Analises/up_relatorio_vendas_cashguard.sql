/* 
	exec up_relatorio_vendas_cashguard '20230301','20230329','Loja 1','56,0'
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_relatorio_vendas_cashguard]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_cashguard
go

create procedure dbo.up_relatorio_vendas_cashguard
	@dataIni		DATETIME
	,@dataFim		DATETIME
	,@site			VARCHAR(254)
	,@op			VARCHAR(254) = ''

/* WITH ENCRYPTION */ 
AS
BEGIN
	-- 0 representa selecionar todos
	IF 0 IN (select items from dbo.up_splitToTable(@op,','))
	BEGIN
		SET @op = ''
	END

	select
		ano			= YEAR(CONVERT(DATE,odata,102))
		,mes		= MONTH(CONVERT(DATE,odata,102))
		,data		= CONVERT(DATE,odata,102)
		,atendimento = nrAtend
		,nr_de_Vendas = nrVendas
		,total 
		,dinheiro	= evdinheiro
		,MB			= epaga2
		,Visa		= epaga1 
		,Cheque		= echtotal
		,epaga3		
		,epaga4	
		,epaga5	
		,epaga6
		,terminal	=  terminal_nome
		,vendedor	= nome
		,fechado	= CASE WHEN fechado=1 THEN 'Sim' ELSE 'Não' END
		,abatido	= CASE WHEN abatido=1 THEN 'Sim' ELSE 'Não' END
		,Loja		= site
		,cod_pag_cashguard	= fechaStamp
	FROM
		b_pagcentral (NOLOCK)
	WHERE
		CONVERT(DATE,odata,102) BETWEEN @dataIni AND @dataFim
		AND site = CASE WHEN @site = '' THEN site ELSE @site END 
		AND B_pagCentral.ignoraCaixa = 0
		AND (b_pagcentral.vendedor IN 
			(select items from dbo.up_splitToTable(@op,','))or @op= '' )
	ORDER BY
		udata
END

GO
Grant Execute on dbo.up_relatorio_vendas_cashguard to Public
Grant control on dbo.up_relatorio_vendas_cashguard to Public
Go 

