/* Relatório Preçário de artigos

	exec up_relatorio_precario 'Atlantico',0,999999
	exec up_relatorio_precario 'Atlantico',1

	
*/

if OBJECT_ID('[dbo].[up_relatorio_precario]') IS NOT NULL
	drop procedure dbo.up_relatorio_precario
go

create procedure [dbo].up_relatorio_precario
	@site			varchar(55)
	,@comstock		bit
	,@limart		varchar(30)

/* with encryption */

AS
SET NOCOUNT ON
	
	/* Calc Loja e Armazens */
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)

	/* Calc Informação Produto */
	Select
		top (CAST(@limart as integer))
		st.ref
		, st.design
		, round(st.epv1 * st.qttembal,2) as prembal
		, round(st.epv1 * st.qttminvd,2) as prlamina
		, cast(qttminvd as numeric(15,0)) as qttminvd
		, taxasiva.taxa
	From
		st (nolock)
		inner join taxasiva (nolock) on taxasiva.codigo=st.tabiva
	Where
		site_nr = @site_nr
		and stock > (case when @comstock = 0 then -999 else 0 end)
	order by
		design

GO
Grant Execute on dbo.up_relatorio_precario to Public
Grant control on dbo.up_relatorio_precario to Public
GO