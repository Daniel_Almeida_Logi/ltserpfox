-- exec up_relatorio_DescontosPredefinidosClientes '',0

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_DescontosPredefinidosClientes]') IS NOT NULL
	drop procedure dbo.up_relatorio_DescontosPredefinidosClientes
go

create procedure dbo.up_relatorio_DescontosPredefinidosClientes
@nome	as varchar(55),
@desconto as numeric(9,2)

/* with encryption */

AS 
BEGIN

Select 
	nome
	,no
	,estab
	,desconto
	,esaldo
	,ultvenda
from 
	b_utentes (nolock)
where
	nome like @nome + '%'	
	and desconto >= @desconto
END

GO
Grant Execute on dbo.up_relatorio_DescontosPredefinidosClientes to Public
Grant control on dbo.up_relatorio_DescontosPredefinidosClientes to Public
GO