/* Análise Escaloes - Margens Regressivas

	exec up_relatorio_gestao_escaloes_mrgRegressivas '20140101', '20150131', 'Loja 1'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_escaloes_mrgRegressivas]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_escaloes_mrgRegressivas
go

create procedure [dbo].up_relatorio_gestao_escaloes_mrgRegressivas
@dataIni as datetime,
@dataFim as datetime,
@site as varchar(55)
 /* WITH ENCRYPTION */
AS
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosMrg'))
		DROP TABLE #dadosMrg
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFprod'))
		DROP TABLE #dadosFprod
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFi'))
		DROP TABLE #dadosFi
	-- #dadosMrg
	Select	
		versao
		,escalao
		,PVAmin
		,PVAmax
		,PVPmin
		,PVPmax
		,mrgGross
		,mrgGrossTipo
		,mrgFarm
		,FeeFarm
		,feeFarmCiva
		,txCom
		,factorPond
	into 
		#dadosMrg
	from	
		b_mrgRegressivas (nolock)
	-- #dadosFprod
	select	
		fprod.cnp
		,escalao = (Select top 1 escalao from #dadosMrg Where pvporig between PVPmin and PVPmax order by versao desc)
		,pvpciva = pvporig 
		,pvpsiva =  pvporig - (pvporig*(taxa/100))
		,fprod.u_tabiva
		,taxasiva.taxa
		,factorPond = (Select top 1 factorPond from #dadosMrg Where pvporig between PVPmin and PVPmax order by versao desc) 
		,feeFarmCiva = (Select top 1 feeFarmCiva from #dadosMrg Where pvporig between PVPmin and PVPmax order by versao desc)
		,txCom = (Select top 1 txCom from #dadosMrg Where pvporig between PVPmin and PVPmax order by versao desc)  
		,mrgGross = (Select top 1 mrgGross from #dadosMrg Where pvporig between PVPmin and PVPmax order by versao desc)
		,medid
	into 
		#dadosFprod
	from	
		fprod (nolock)
		inner join fpreco (nolock) on fpreco.cnp=fprod.ref
		inner join taxasiva (nolock) on taxasiva.codigo = fprod.u_tabiva
	where	
		fpreco.grupo='pvp'
	-- #dadosFi
	select	
		Total = etiliquido
		,FI.EPCP
		,FI.ECUSTO
		,FI.qtt
		,ettent1 = u_ettent1
		,ettent2 = u_ettent2 
		/* CALCULO DA MARGEM CONSIDERAMOS SEMPRE ESTE CAMPO MESMO QUE NÃO TENHAM LIGAÇÃO, DEVIDO À ELIMINAçÂO DE RECEITAS E POSTERIOR INSERÇÃO O TOTAL DA LINHA JÁ É NEGATIVO NAS REGULARIZAÇÔES */
		,Margem	= 
				(FI.ETILIQUIDO + (
					CASE WHEN TD.tipodoc = 3 THEN /* DEV. A CLIENTE */
						(Fi.u_ettent1 * -1) + (Fi.u_ettent2 * -1)
					ELSE
						(Fi.u_ettent1 + Fi.u_ettent2)
					END
				) - 
				(CASE WHEN TD.tipodoc = 3 THEN /* DEV. A CLIENTE */
					(CASE WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt ELSE FI.EPCP * qtt END)*-1
				ELSE
					(CASE WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt ELSE FI.EPCP * qtt END) 
				END) 
				) 
		,ref
		,td.tipodoc
		,pcusto = (CASE WHEN FI.EPCP = 0 THEN FI.ECUSTO ELSE FI.EPCP END) 
	into
		#dadosFi
	from	
		fi (nolock) inner join ft (nolock) on fi.ftstamp = ft.ftstamp  
		inner join td (nolock) on td.ndoc = fi.ndoc
		left outer join #dadosFprod on #dadosFprod.cnp = fi.ref
	where	
		fdata between @dataIni and @dataFim AND medid != 0
	Select	
		 escalao = isnull(escalao,0)
		,valor = sum(etiliquido)
		,percentagemVendasEscalao = ((sum(etiliquido)*100) / (select SUM(total) from #dadosFi)) 
		/*X=MARG*100/TOT*/
		,MargemMediaEscalao = (Select AVG(
					CASE WHEN	(total + ettent1 + ettent2) > 0
						 THEN	(ROUND(margem,2)*100) / (total + ettent1 + ettent2)
						 ELSE	0
					END
		) from #dadosFi Where ref in (select cnp from #dadosFprod as cte22 where cte22.escalao = #dadosFprod.escalao))  				
	from	
		fi (nolock)
		inner join ft (nolock) on ft.ftstamp = fi.ftstamp
		left outer join #dadosFprod on #dadosFprod.cnp = fi.ref
	where
		fdata between @dataIni and @dataFim
		and ft.tipodoc !=4 AND #dadosFprod.medid != 0
	Group by escalao
	Order by escalao
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosMrg'))
		DROP TABLE #dadosMrg
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFprod'))
		DROP TABLE #dadosFprod
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFi'))
		DROP TABLE #dadosFi

Grant Execute on dbo.up_relatorio_gestao_escaloes_mrgRegressivas to Public
Grant Control on dbo.up_relatorio_gestao_escaloes_mrgRegressivas to Public
go