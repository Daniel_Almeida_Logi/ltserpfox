/* Relatório Qt. Vendida Mes Agrup. Iva     
		exec up_relatorio_vendas_base_detalhe  '19000101', '20181231', 'Loja 1'
	select fi.familia,ft.fdata,fi.rdata,ft.no,* from 
	fi inner join ft on ft.ftstamp=fi.ftstamp inner join st on st.ref=fi.ref
	where fi.ref='5440987' and ft.fdata between '20140101' and '20170131' and ft.site='Loja 3' and st.site_nr=3
	exec up_relatorio_vendas_qtVendidaMesAgrupIva '19000101', '20181231', '', '', '', '', 'Loja 1'
	
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_vendas_qtVendidaMesAgrupIva]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_qtVendidaMesAgrupIva
go

create procedure dbo.up_relatorio_vendas_qtVendidaMesAgrupIva
@dataIni	as datetime,
@dataFim	as datetime,
@ref		as varchar(254),
@design		as varchar(254),
@familia	as varchar(max),
@lab		as varchar(254),
@site		as varchar(55)

/* with encryption */
AS 
BEGIN

	set language portuguese

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
	DROP TABLE #dadosSt
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
	DROP TABLE #dadosFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVenda_1'))
	DROP TABLE #dadosVenda_1
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.##EmpresaArm'))
	DROP TABLE #EmpresaArm
	
		/* Calc Loja e Armazens */
		declare @site_nr as tinyint
		set @site_nr = ISNULL((select no from empresa where site = @site),0)

		select 
			armazem
		into
			#EmpresaArm
		From
			empresa
			inner join empresa_arm on empresa.no = empresa_arm.empresa_no
		where
			empresa.site = case when @site = '' then empresa.site else @site end	
		
		/* Calc dados ST */
		Select 
			ref COLLATE SQL_Latin1_General_CP1_CI_AI as REF
			,stock		
			,st.u_lab
			,st.epv1
			,st.inactivo
			,epcult
			,st.stmax
			,st.ptoenc
			,departamento = isnull(b_famDepartamentos.design ,'')
			,seccao = isnull(b_famSeccoes.design,'')
			,categoria = isnull(b_famCategorias.design,'')
			,famfamilia =  isnull(b_famFamilias.design,'')
		into 
			#dadosST
		From
			st (nolock)
			left join b_famDepartamentos (nolock) on st.u_depstamp = b_famDepartamentos.depstamp
			left join b_famSeccoes (nolock) on st.u_secstamp = b_famSeccoes.secstamp
			left join b_famCategorias (nolock) on st.u_catstamp = b_famCategorias.catstamp
			left join b_famFamilias (nolock) on st.u_famstamp = b_famFamilias.famstamp
		where 
			site_nr = @site_nr
		
		--select * from #dadosST where ref='5440987'

		/* Calc Familias Infarmed Produtos */
		Select 
			distinct ref, nome
		into 
			#dadosFamilia
		From 
			stfami (nolock)  
		Where 
			ref in (Select items from dbo.up_splitToTable(@familia,',')) 
			Or @familia = '0' 
			Or @familia = ''
		
		/* Define se vais mostrar resultados abaixo do Cliente 199 */
		declare @Entidades as bit
		set @Entidades = case when (select tipoempresa from empresa where site = @site) in ('FARMACIA', 'PARAFARMACIA') then convert(bit,1) else convert(bit,0) end
		
		/* Dados Base Vendas  */
		create table #dadosVendasBase (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100)
		,familia varchar(18)
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,site varchar(10)
		,site_nr numeric(3,0)
		)
		
		/* Elimina Vendas tendo em conta tipo de cliente*/
		insert #dadosVendasBase
		exec up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site
		
		--select * from #dadosVendasBase where ref='5440987'
		
		If @Entidades = 1
			delete from #dadosVendasBase where #dadosVendasBase.no < 199	
		
		/* Calc Result Set*/
		SELECT	'ANO'			= year(#dadosVendasBase.fdata)
				,'MES'			= STR(MONTH(#dadosVendasBase.fdata)) + ' - ' + LEFT(UPPER(datename(month,#dadosVendasBase.fdata)),3)
				,'REF'			= #dadosVendasBase.ref
				,'DESIGN'		= #dadosVendasBase.design
				,'QTD'			= sum(#dadosVendasBase.qtt)
				,'STOCKACTUAL'	= #dadosSt.stock
				,'STOCKMAX'		= #dadosST.stmax
				,'PTOENC'		= #dadosSt.ptoenc
				,'FAMILIA'		= #dadosVendasBase.familia
				,'FAMINOME'		= #dadosFamilia.nome
				,'LAB'			= #dadosST.u_lab
				,'EPV1'			= #dadosST.epv1
				,'INACTIVO'		= #dadosST.inactivo
				,'PCL'			= #dadosST.epcult
				,'iva'			= #dadosVendasBase.iva
		into
			#dadosVenda_1
		FROM	
			#dadosVendasBase (nolock)
			inner join #dadosST on #dadosVendasBase.ref COLLATE SQL_Latin1_General_CP1_CI_AI = #dadosST.ref collate SQL_Latin1_General_CP1_CI_AI
			inner join td (nolock) on td.ndoc = #dadosVendasBase.ndoc
			inner join #dadosfamilia on #dadosVendasBase.familia COLLATE SQL_Latin1_General_CP1_CI_AI = #dadosfamilia.ref COLLATE SQL_Latin1_General_CP1_CI_AI
		WHERE	
			--(#dadosVendasBase.fdata between @dataIni and @dataFim) and 
			(#dadosVendasBase.ref COLLATE SQL_Latin1_General_CP1_CI_AI in (Select items from dbo.up_splitToTable(@ref,',')) or @ref = '')
			and #dadosVendasBase.DESIGN like @design + '%'
			and #dadosVendasBase.familia in (Select ref COLLATE SQL_Latin1_General_CP1_CI_AI from #dadosfamilia)
			and isnull(#dadosST.u_lab,'') like @lab+'%'
		GROUP BY 
			year(fdata), datename(month,fdata), MONTH(fdata), #dadosVendasBase.iva, #dadosVendasBase.ref ,#dadosVendasBase.design, #dadosST.stock
			, #dadosVendasBase.familia, #dadosfamilia.nome, #dadosST.u_lab ,#dadosST.epv1, #dadosST.inactivo, #dadosST.epcult, #dadosST.stmax , #dadosST.ptoenc
			--, #dadosVendasBase.qtt
		ORDER BY 
			REF

		select * from #dadosVenda_1

		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
			DROP TABLE #dadosSt
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
			DROP TABLE #dadosFamilia
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVenda_1'))
			DROP TABLE #dadosVenda_1
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.##EmpresaArm'))
			DROP TABLE #EmpresaArm
	
END

GO
Grant Execute on dbo.up_relatorio_vendas_qtVendidaMesAgrupIva to Public
Grant control on dbo.up_relatorio_vendas_qtVendidaMesAgrupIva to Public
GO