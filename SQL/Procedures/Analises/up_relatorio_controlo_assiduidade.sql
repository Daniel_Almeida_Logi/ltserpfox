/*
Relat�rio Controlo Assiduidade

exec up_relatorio_controlo_assiduidade '20220101','20220630','0',''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_relatorio_controlo_assiduidade]') IS NOT NULL
DROP PROCEDURE dbo.up_relatorio_controlo_assiduidade
GO

Create Procedure up_relatorio_controlo_assiduidade
	@dataIni				DATETIME
	,@dataFim				DATETIME 
	,@op					Varchar(254)
	,@tipoPicagem			Varchar(254)

	
AS
SET NOCOUNT ON

SET @dataIni = @dataIni + '00:00:00:01'
SET @dataFim = @dataFim + '23:59:59'

select distinct b_us.nome, inOutDate as 'Data_Hora',
CASE 
	when inOutCode = 0    then 'Entrada' 
	when inOutCode = 1    then 'Saida'
	when inOutCode = 2    then 'Entrada Almo�o'
	when inOutCode = 3	  then 'Saida Almo�o'
	when inOutCode = 4	  then 'Hora Extra Entrada'
	when inOutCode = 5	  then 'Hora Extra Saida'
	else ''
end as 'Tipo_Picagem'
from clock_response_payload_line  line
inner join b_us(nolock) on b_us.userno = line.userId
where type='timeRegister'
and inOutDate >=  convert(varchar,@dataIni) 
and inOutDate <=  convert(varchar,@dataFim)
and (b_us.userno in (select items from dbo.up_splitToTable(@op,','))or @op= '' or @op= '0')
and (inOutCode in (select items from dbo.up_splitToTable(@tipoPicagem,',')) or @tipoPicagem= '')
order by   inOutDate desc

GO
Grant Execute On up_relatorio_controlo_assiduidade to Public
Grant Control On up_relatorio_controlo_assiduidade to Public
GO
