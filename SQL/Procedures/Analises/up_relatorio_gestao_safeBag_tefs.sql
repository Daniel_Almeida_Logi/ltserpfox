-- Relatório TEFS
-- exec up_relatorio_gestao_safeBag_tefs '20100101','20130101',''

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_safeBag_tefs]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_safeBag_tefs
go

create procedure dbo.up_relatorio_gestao_safeBag_tefs
@DataIni DATETIME,
@DataFim DATETIME,
@site varchar(55)
/* with encryption */
AS
SET NOCOUNT ON
SELECT	(Select REF FROM empresa a (nolock) WHERE a.site = LOJANOME) as LOJANUM,
		LOJANOME,PNOME as Terminal,MAX(valorTpa) as ValorTPA,SUM(visa_mb) as Venda,SUM(visa_mb)- MAX(valorTpa) as Diferenca
FROM (
	SELECT	
		CX.SITE as LOJANOME
		,cx.cxstamp
		,fcx.fcxstamp
		,cx.valorTpa
		,CX.pnome
		,fcx.evdinheiro
		,fcx.echtotal
		,fcx.epaga1 as visa
		,fcx.epaga2 as mb
		,fcx.epaga1 + fcx.epaga2 as visa_mb 
	FROM
		CX (nolock)
		inner join fcx (nolock) on cX.cxstamp = fcx.cxstamp AND fcx.operacao = 'F'
	WHERE	
		cx.dabrir BETWEEN @dataIni AND @dataFim
		and cx.site = (case when @site = '' Then cx.site else @site end)
) a
GROUP BY
	LOJANOME,PNOME
ORDER BY
	LOJANUM, LOJANOME, PNOME

GO
Grant Execute on dbo.up_relatorio_gestao_safeBag_tefs to Public
Grant control on dbo.up_relatorio_gestao_safeBag_tefs to Public
GO