
/* Devolve VEndas Pim tiMedi



	exec up_relatorio_vendas_tiMedi '20231004','20231004', 'Loja 1', '', 1,0,0

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_vendas_tiMedi]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_tiMedi
go

create procedure dbo.up_relatorio_vendas_tiMedi
	@dataIni			DATETIME,
	@dataFim			DATETIME,
	@site				VARCHAR(20),
	@tipoCli			VARCHAR(256),
	@incExport			BIT = 0,
	@clientePim			BIT = 1,
	@produtoPim			BIT = 1
/* WITH ENCRYPTION */

AS

	If OBJECT_ID('tempdb.dbo.#dadosVendasPim') IS NOT NULL
		DROP TABLE #dadosVendasPim

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosTipoCliente'))
		DROP TABLE #dadosTipoCliente	

	create  table #dadosTipoCliente (
		[tipo] varchar(20)
	)

	IF(@tipoCli='')
	BEGIN

		INSERT INTO #dadosTipoCliente (tipo)
		SELECT distinct tipo FROM b_utentes
	END 
	ELSE
	BEGIN
		INSERT INTO #dadosTipoCliente (tipo)
		SELECT distinct tipo FROM b_utentes(nolock) Where b_utentes.tipo in (Select items as tipo from dbo.up_splitToTable(@tipoCli,','))
	END 


	
	SELECT 
		  emp.nomecomp                                                                         as HOSPITAL_NAME,
		  codfarm                                                                              as HOSPITAL_ID,
		  --dbo.alltrimIsNull(STR(ut.no)) +'.'+ dbo.alltrimIsNull(STR(ut.estab))                 as PATIENT_ID, 		
		  CASE	WHEN ISNULL(u_hclstamp,'') != '' -- Quando Utente != Cliente
				THEN dbo.alltrimIsNull(STR(ut2.no)) +'.'+ dbo.alltrimIsNull(STR(ut2.estab))
				ELSE dbo.alltrimIsNull(STR(ut.no)) +'.'+ dbo.alltrimIsNull(STR(ut.estab))
		  END																                   as PATIENT_ID, 		
		  --(select first_name from dbo.uf_gerais_devolve_primeiro_e_ultimo_nome(ft.nome))       as PATIENTE_1ST_NAME,
		  CASE	WHEN ISNULL(u_hclstamp,'') != '' -- Quando Utente != Cliente
				THEN (select first_name from dbo.uf_gerais_devolve_primeiro_e_ultimo_nome(ut2.nome))
				ELSE (select first_name from dbo.uf_gerais_devolve_primeiro_e_ultimo_nome(ft.nome))
		  END																			       as PATIENTE_1ST_NAME, 
		  --(select last_name from dbo.uf_gerais_devolve_primeiro_e_ultimo_nome(ft.nome))        as PATIENT_LAST_NAME, 
		  CASE	WHEN ISNULL(u_hclstamp,'') != '' -- Quando Utente != Cliente
				THEN (select last_name from dbo.uf_gerais_devolve_primeiro_e_ultimo_nome(ut2.nome))
				ELSE (select last_name from dbo.uf_gerais_devolve_primeiro_e_ultimo_nome(ft.nome))
		  END																			       as PATIENT_LAST_NAME, 
		--dbo.alltrimIsNull(STR(ft.ndoc)) +'-'+ dbo.alltrimIsNull(STR(ft.fno))                 as INVOICE_NUMBER,
		  dbo.alltrimIsNull(STR(ft.fno))                                                       as INVOICE_NUMBER,  
		  CONVERT(VARCHAR(10),ft.ousrdata, 103) + ' '  + convert(VARCHAR(8), ft.ousrdata, 14)  as INVOICE_DATE, 
		  fi.ref                                                                               as PRODUCT_CODE,
		  IsNull(convert(int,fi.qtt),0)														   as QTT,
		  ''                                                                                   as LINE_NUMBER,
		  ''                                                                                   as EMPTY_FIELD_2, 
		  ''                                                                                   as BATCH_NUMBER,
		  ''                                                                                   as SOCIAL_SECURITY_NUMBER, 
		  ''                                                                                   as ADDRESS,
		  ''                                                                                   as EMPTY_FIELD_1, 
		  ''                                                                                   as PATIENT_POSTAL_CODE, 
		  ''                                                                                   as PATIENT_CITY,
		  ''                                                                                   as ROOM_NUMBER,
		  ''                                                                                   as PHARMACY_NAME,
		  ''                                                                                   as PHARMACY_ID, 
		  ''                                                                                   as SALES_IS, 
		  ''                                                                                   as SOFTWARE_NAME,
		  ''                                                                                   as DOCTOR_NAME, 
		  ''                                                                                   as DOCTOR_ID ,
		  ''                                                                                   as SALE_DATE, 
		  ''                                                                                   as SALE_NUMBER,
		  CASE WHEN fi.tipodoc = 3 THEN convert(int,ISNULL(fi.qtt,0)*-1 )
								   ELSE convert(int,ISNULL(fi.qtt,0)) END						   AS NUMBER_OF_BOXES_INVOICE						
		into
			#dadosVendasPim
		from 
			ft (nolock) 
		inner join 
			ft2 (nolock) on ft.ftstamp = ft2.ft2stamp
		inner join 
			td (nolock) on td.ndoc = ft.ndoc
		inner join 
			fi (nolock) on ft.ftstamp = fi.ftstamp
		inner join
			b_utentes ut (nolock) on ft.no = ut.no and ft.estab = ut.estab
		left JOIN --para o case quando utente != cliente (ft.U_HCLSTAMP != '')
			b_utentes ut2 (NOLOCK) on ft.u_hclstamp = ut2.utstamp
		inner join 
			empresa emp (nolock) on ft.site = emp.site
		inner join 
			empresa_arm (nolock) on empresa_arm.armazem = emp.no
		inner join
			st (nolock) on fi.ref = st.ref and st.site_nr = empresa_arm.empresa_no
		WHERE
			 ft.fdata>= @dataIni and  ft.fdata<=@dataFim
			 AND ft.site = case when @site = '' then ft.site else @site end
		     AND ft2.exportado = case when @incExport = 1 then ft2.exportado else 0 end 
		     AND (ut.tipo COLLATE DATABASE_DEFAULT in (select tipo COLLATE DATABASE_DEFAULT from #dadosTipoCliente )
				OR ut2.tipo COLLATE DATABASE_DEFAULT in (select tipo COLLATE DATABASE_DEFAULT from #dadosTipoCliente )
				)
			 AND td.tipodoc in (1,3) 
			 and fi.ref!=''
			 and fi.stns = 0
			 and fi.qtt>0
			 AND ft.no> 200
			 AND ut.pim = case when @clientePim = 1 then 1 else ut.pim end 
			 AND st.pim = case when @produtoPim = 1 then 1 else st.pim end 
		ORDER BY
			 ft.fdata ASC
			,ft.ousrhora ASC
			,ft.fno  ASC
			,fi.ref	   ASC


		select
			convert(varchar(100),HOSPITAL_NAME),
			convert(varchar(100),HOSPITAL_ID),
			convert(varchar(100),PATIENT_ID),
			convert(varchar(100),PATIENTE_1ST_NAME),
			convert(varchar(100),PATIENT_LAST_NAME),		
			convert(varchar(100),SOCIAL_SECURITY_NUMBER),	
			convert(varchar(100),ADDRESS),
			convert(varchar(100),EMPTY_FIELD_1),
			convert(varchar(100),PATIENT_POSTAL_CODE),
			convert(varchar(100),PATIENT_CITY),	 
			convert(varchar(100),ROOM_NUMBER),
			convert(varchar(100),PHARMACY_NAME),	 
			convert(varchar(100),PHARMACY_ID),
			convert(varchar(100),SALES_IS), 
			convert(varchar(100),SOFTWARE_NAME),  
			convert(varchar(100),DOCTOR_NAME),
			convert(varchar(100),DOCTOR_ID),
			convert(varchar(100),SALE_DATE),
			convert(varchar(100),SALE_NUMBER),
			convert(varchar(100),INVOICE_NUMBER),
			convert(varchar(100),INVOICE_DATE),
			convert(varchar(100),LINE_NUMBER),
			convert(varchar(100),PRODUCT_CODE),
			convert(varchar(100),EMPTY_FIELD_2),		
			convert(varchar(100),NUMBER_OF_BOXES_INVOICE)
		from #dadosVendasPim

	If OBJECT_ID('tempdb.dbo.#dadosVendasPim') IS NOT NULL
		DROP TABLE #dadosVendasPim

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosTipoCliente'))
		DROP TABLE #dadosTipoCliente	

GO
Grant Execute On dbo.up_relatorio_vendas_tiMedi to Public
Grant Control On dbo.up_relatorio_vendas_tiMedi to Public
Go


