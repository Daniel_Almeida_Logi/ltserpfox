/* Relat�rio Dividas Fornecedores

	exec up_relatorio_fornecedores_dividasaFornecedores '20100101','20130101',''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_fornecedores_dividasaFornecedores]') IS NOT NULL
	drop procedure dbo.up_relatorio_fornecedores_dividasaFornecedores
go

Create procedure dbo.up_relatorio_fornecedores_dividasaFornecedores
@dataIni as datetime,
@dataFim as datetime,
@site	varchar(60),
@dataVenc as datetime


/* with encryption */
AS
SET NOCOUNT ON

select
	'FCSTAMP'		= fc.fcstamp, 
	'FOSTAMP'		= fc.fostamp, 
	'NO'			= fc.no, 
	'NOME'			= fc.nome, 
	'DATALC'		= convert(varchar,fc.datalc,102), 
	'DATAVEN'		= convert(varchar,fc.dataven,102),
	'ECRED'			= fc.ecred, 
	'EDEB'			= fc.edeb, 
	'ORIGEM'		= fc.origem, 
	'MOEDA'			= fc.moeda, 
	'CMDESC'		= fc.cmdesc, 
	'ADOC'			= fc.adoc, 
	'ULTDOC'		= fc.ultdoc,
	'INTID'			= fc.intid, 
	'SALDO'			= ISNULL((SElect	SUM((a.ecred-a.ecredf))- SUM((a.edeb-a.edebf)) 
									From	fc a 
									Where	a.no = fc.no and a.estab = fc.estab
											and (abs((a.ecred-a.ecredf)-(a.edeb-a.edebf)) > 0.010000)
											and (a.ousrdata + a.ousrhora <= fc.ousrdata + fc.ousrhora))
							,0),			
	'DOCDATA'		= convert(varchar,isnull(fo.docdata,'19000101'),102), 
	'OUSRHORA'		= fc.ousrhora, 
	'USERNAME'		= (Select top 1 username from b_us where fc.ousrinis=b_us.iniciais), 
	'OUSRINIS'		= fc.ousrinis,
	'IEMISSAO'		= datediff(d,fo.docdata,GETDATE()),
	'IVENCIMENTO'	= datediff(d,fc.dataven,GETDATE()),
	'VALOR'			= (fc.ecred-fc.ecredf)-(fc.edeb-fc.edebf), 
	'VERDOC'		= CONVERT(bit,0),
	LOJA			= site
from
	fc (nolock) 
	left join fo (nolock) on fc.fostamp=fo.fostamp
where
	fo.docdata between @dataIni and @dataFim
	and (abs((fc.ecred-fc.ecredf)-(fc.edeb-fc.edebf)) > 0.010000)
	and fo.site = (case when @site = '' Then fo.site else @site end)
	and fc.dataven <= @dataVenc
	and fc.cm !=112 -- n�o pode entrar a V/ Fatura Resumo
order by
	no, datalc, ousrhora

Go
Grant Execute on dbo.up_relatorio_fornecedores_dividasaFornecedores to Public
Grant Control on dbo.up_relatorio_fornecedores_dividasaFornecedores to Public
Go
