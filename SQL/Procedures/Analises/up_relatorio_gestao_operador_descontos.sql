/* Relatório Descontos por Operador 

	exec up_relatorio_gestao_operador_descontos '20160501', '20160930', 0, '', '', 0, 'Loja 1', ''

	exec up_relatorio_gestao_operador_descontos '20160501', '20160930', 0, '', '', 0, 'Loja 1', 'MSRM' 
	exec up_relatorio_gestao_operador_descontos '20190101', '20210617', 0, '', '', 0, 'Loja 2', ''
	exec up_relatorio_gestao_operador_descontos '20190101', '20210617', 0, '', '', 0, 'Loja 1', ''
	exec up_relatorio_gestao_operador_descontos '20190101', '20210617', 0, '', '', 0, 'Loja 1,Loja 2', ''
	exec up_relatorio_gestao_operador_descontos '20200915', '20220727', 0, '', 'bene - mais um carrada de caracteres so para encher e ver se parte!', 0, 'Loja 1', 'MSRM'

	exec up_relatorio_gestao_operador_descontos @dataIni='1900-01-01 00:00:00',@dataFim='2022-06-27 00:00:00',@op=N'',@site=N'Loja 1',@lab=N'',@familia=N'',@marca=N'bene - mais um carrada de caracteres so para encher e ver se parte!',@atributosFam=N''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_operador_descontos]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_operador_descontos
go

create procedure dbo.up_relatorio_gestao_operador_descontos
@dataIni	as  datetime,
	@dataFim	as  datetime,
	@op			as varchar(max),
	@lab		as varchar(254),
	@marca		as  varchar(200),
	@familia	as varchar(max),
	@site		as varchar(254),
	@atributosFam as varchar(max)
/* with encryption */
AS 
BEGIN
	set language  portuguese
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
	DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFI'))
	DROP TABLE #dadosFI
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#EmpresaArm'))
	DROP TABLE #EmpresaArm
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
	DROP TABLE #dadosOperadores
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
	DROP TABLE #dadosFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
	DROP TABLE #dadosSt
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dados_1'))
	DROP TABLE #dados_1
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#Empresasite_nr'))
	DROP TABLE #Empresasite_nr

	SET @atributosFam = (CASE WHEN (@atributosFam IS NULL OR @atributosFam = '') THEN ';;;' ELSE @atributosFam END)

	DECLARE @departamento VARCHAR(254) = ''
	SET @departamento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 1),'') 
	DECLARE @sessao       VARCHAR(254) = ''
	set @sessao = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 2) ,'')
	DECLARE @categoria    VARCHAR(254) = ''
	SET @categoria = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 3),'') 
	DECLARE @segmento     VARCHAR(254) = ''
	SET @segmento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 4),'') 
		

	IF @site = ''
	BEGIN
		SET @site = isnull((select  convert(varchar(254),(select 
						site + ', '
					from 
						empresa (nolock)							
					FOR XML PATH(''))) as no),0)
	END


		/* Calc Loja e Armazens */

		SELECT
			empresa.NO AS [NO]
		INTO #Empresasite_nr
		FROM empresa
			WHERE empresa.site in (Select items from dbo.up_splitToTable(@site, ',')) 

		--declare @site_nr as tinyint
		--set @site_nr = ISNULL((select no from empresa where site = @site),0)
		
		select 
			armazem
		into
			#EmpresaArm
		From
			empresa
			inner join empresa_arm on empresa.no = empresa_arm.empresa_no
		where
			empresa.site in (Select items from dbo.up_splitToTable(@site, ',')) 
		/* Calc nº de Atend */
		Select 
			distinct ft.u_nratend 
		into 
			#dadosFI
		From 
			fi (nolock)
			inner join ft (nolock) on fi.ftstamp = ft.ftstamp 
		where 
			fi.epromo = 1 
			and fdata between @dataIni and @dataFim
			and ft.site IN  (Select items from dbo.up_splitToTable(@site, ',')) 
		/* Calc Operadores  */

		Select
			iniciais
			,cm = userno
			,username
		into
			#dadosOperadores
		from
			b_us (nolock)
		Where
			userno in (select items from dbo.up_splitToTable(@op,','))
			or @op= '0'
			or @op= ''
		/* Calc Familias Infarmed Produtos */


		Select 
			distinct ref 
		into 
			#dadosFamilia
		From 
			stfami (nolock)  
		Where 
			ref in (Select items from dbo.up_splitToTable(@familia,',')) 
			Or @familia = '0' 
			Or @familia = ''
		/* Calc Dados Produto */
		Select 
			st.ref
			,st.stock
			,st.u_lab
			,st.epv1
			,st.inactivo
			,epcult
			,st.stmax
			,st.ptoenc
			,usr1 = isnull(st.usr1,'')		
			, ISNULL(A.descr,'') as departamento
			, ISNULL(B.descr,'') as seccao
			, ISNULL(C.descr,'') as categoria
			, ISNULL(D.descr,'') as segmento
			,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 1) valDepartamento
			,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 2) valSeccao
			,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 3) valCategoria
			,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 4) valSegmento
			, EMPRESA.site		as site
		into 
			#dadosST
		From
			st (nolock)
			INNER JOIN EMPRESA ON st.site_nr = EMPRESA.no	
			left join grande_mercado_hmr (nolock) A on A.id = st.u_depstamp
			left join mercado_hmr (nolock) B on B.id = st.u_secstamp
			left join categoria_hmr (nolock) C on C.id = st.u_catstamp 
			left join segmento_hmr (nolock) D on D.id = st.u_segstamp		
		where
			site_nr IN  (SELECT NO FROM #Empresasite_nr)
		/* Define se vais mostrar resultados abaixo do Cliente 199 */


		declare @Entidades as bit

		if ((select count(tipoempresa) from empresa where site in (Select items from dbo.up_splitToTable(@site, ',')) and tipoempresa not in ('FARMACIA', 'PARAFARMACIA'))>1)
		BEGIN
			SET @Entidades = 0
		
		END 
		ELSE
		BEGIN 
			SET @Entidades = 1
		END 



		--set @Entidades = case when (select tipoempresa from empresa where site in (Select items from dbo.up_splitToTable(@site, ',')) ) in ('FARMACIA', 'PARAFARMACIA') then convert(bit,1) else convert(bit,0) end
		/* Dados Base Vendas  */
		create table #dadosVendasBase (
			nrAtend varchar(20)
			,ftstamp varchar(25)
			,fdata datetime
			,[no] numeric(10,0)
			,estab numeric(5,0)
			,tipo varchar(25)
			,ndoc numeric(3,0)
			,fno numeric(10,0)
			,tipodoc numeric(2)
			,u_tipodoc numeric(2)
			,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
			,design varchar(100)
			,familia varchar(18)
			,u_epvp numeric(15,3)
			,epcpond numeric(19,6)
			,iva numeric(5,2)
			,qtt numeric(11,3)
			,etiliquido numeric(19,6)
			,etiliquidoSiva numeric(19,6)
			,ettent1 numeric(13,3)
			,ettent2 numeric(13,3)
			,ettent1siva numeric(13,3)
			,ettent2siva numeric(13,3)
			,desconto numeric(6,2)
			,descvalor numeric(19,6)
			,descvale numeric(19,6)
			,ousrhora varchar(8)
			,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
			,vendnm varchar(20)
			,u_ltstamp varchar(25)
			,u_ltstamp2 varchar(25)
			,ecusto numeric(19,6)
			,loja  varchar(50)
			,loja_nr  numeric(5,0)
		)

		insert #dadosVendasBase
		exec up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site
		/* Elimina Vendas tendo em conta tipo de cliente*/

	

		If @Entidades = 1
		delete from #dadosVendasBase where #dadosVendasBase.no < 199
		/* Calc Result Set */
		Select	
			#dadosVendasBase.fdata
			,ano = YEAR(#dadosVendasBase.fdata)
			,mes = month(#dadosVendasBase.fdata)
			,dia = day(#dadosVendasBase.fdata)
			,td.nmdoc
			,#dadosVendasBase.fno
			,#dadosVendasBase.desconto
			,#dadosVendasBase.ref
			,#dadosVendasBase.design
			,#dadosVendasBase.qtt
			,descCom =CASE WHEN #dadosVendasBase.[tipodoc] != 3 THEN ABS(#dadosVendasBase.[descvalor]) ELSE ABS([descvalor])*-1 END
			,u_descval = #dadosVendasBase.descvale
			,TotalLinha = #dadosVendasBase.etiliquido
			,comp = #dadosVendasBase.ettent1 + #dadosVendasBase.ettent2
			,#dadosVendasBase.ousrinis
			,hora = #dadosVendasBase.ousrhora
			,operador = #dadosVendasBase.vendnm
		into 
			#dados_1
		from 
			#dadosVendasBase
			inner join td on #dadosVendasBase.ndoc = td.ndoc
			inner join #dadosST on #dadosVendasBase.ref  COLLATE DATABASE_DEFAULT = #dadosST.ref COLLATE DATABASE_DEFAULT and  #dadosVendasBase.loja COLLATE DATABASE_DEFAULT = #dadosST.site COLLATE DATABASE_DEFAULT
		where
			(#dadosVendasBase.descvalor != 0 or #dadosVendasBase.desconto != 0 or #dadosVendasBase.descvale != 0)
			and #dadosST.u_lab = Case When @lab = '' Then #dadosST.u_lab Else @lab End 
			and #dadosST.usr1 = CASE When @marca = '' Then isnull(#dadosST.usr1,'') Else @marca End
			and #dadosVendasBase.ousrinis in (select iniciais COLLATE DATABASE_DEFAULT from #dadosOperadores)
			and #dadosVendasBase.familia in (Select ref COLLATE DATABASE_DEFAULT from #dadosFamilia)
			AND departamento = (CASE WHEN @departamento = '' THEN departamento ELSE @departamento END)
			AND seccao = (CASE WHEN @sessao = '' THEN seccao ELSE  @sessao END)
			AND categoria = (CASE WHEN @categoria = '' THEN categoria ELSE  @categoria END)
			AND segmento = (CASE WHEN @segmento = '' THEN segmento ELSE  @segmento END)
		 
		select * from #dados_1

		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
			DROP TABLE #dadosVendasBase
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFI'))
			DROP TABLE #dadosFI
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#EmpresaArm'))
			DROP TABLE #EmpresaArm
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
			DROP TABLE #dadosOperadores
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
			DROP TABLE #dadosFamilia
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
			DROP TABLE #dadosSt
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dados_1'))
			DROP TABLE #dados_1
END

GO
Grant Execute on dbo.up_relatorio_gestao_operador_descontos to Public
Grant control on dbo.up_relatorio_gestao_operador_descontos to Public
GO

