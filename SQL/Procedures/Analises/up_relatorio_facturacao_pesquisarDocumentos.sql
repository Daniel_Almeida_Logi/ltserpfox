-- 

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_facturacao_pesquisarDocumentos]') IS NOT NULL
	drop procedure dbo.up_relatorio_facturacao_pesquisarDocumentos
go

create procedure [dbo].[up_relatorio_facturacao_pesquisarDocumentos]
@topo		int,
@entidade	varchar(55),
@numdoc		numeric(10,0),
@no			numeric(10,0),
@estab		numeric(3,0),
@dataini	datetime,
@datafim	datetime,
@doc		varchar(20),
@user		numeric(6),
@group		varchar(50),
@design		varchar(60),
@atend		varchar(20),
@receita	varchar(20),
@site		varchar(20),
@ncont		varchar(20),
@exportado	int = -1
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON
declare @sql varchar(max)
select @sql = N'
;with
	cte1(nmdoc)as
	(
		select 
			nmdoc 
		from 
			td (nolock)
		where 
			dbo.up_PerfilFacturacao('+convert(varchar,@user)+', '''+@group+''', RTRIM(LTRIM(td.nmdoc)) + '' - '' + ''Visualizar'', '''+@site+''') = 0
	)
select top '+CONVERT(varchar,@topo)+'
	* 
from (
	SELECT
		Tipodoc		= ''FT'',
		Fdata		= ft.fdata,
		QTTOT		= ft.totqtt,
		ESCOLHA		= convert(bit,0),
		Operador	= FT.VENDNM,
		CabStamp	= FT.ftstamp,
		Datav		= ft.fdata,
		Data		= CONVERT(varchar,ft.fdata,102),
		OUSRDATA	= FT.OUSRDATA, 
		OUSRHORA	= FT.OUSRHORA,
		OUSRINIS	= FT.OUSRINIS,
		Documento	= FT.NMDOC,
		Numdoc		= FT.FNO,
		Entidade	= (case when b_utentes.removido=0 then UPPER(FT.NOME) else ''*********'' end),
		NO			= FT.NO, 
		Estab		= FT.ESTAB,
		Total		= ft.etotal,
		u_nratend	= FT.u_nratend,
		u_receita	= ft2.u_receita,'
if @design != ''		
select @sql = @sql + N'
		id			= ROW_NUMBER() over(partition by ft.ftstamp order by fdata),'
if @design = ''		
select @sql = @sql + N'
		id			= ROW_NUMBER() over(order by fdata, ft.ousrhora),'
select @sql = @sql + N'
		ft.ncont,
		ft.ettiliq,
		ft.ettiva,
		ft.exportado
	FROM FT (nolock)'
if @design != ''		
select @sql = @sql + N'
		inner join FI (nolock) on ft.ftstamp=fi.ftstamp and (fi.rdata between '''+convert(varchar,@dataini)+''' AND '''+convert(varchar,@datafim)+''')'
select @sql = @sql + N'	inner join ft2 (nolock) on ft2.ft2stamp=ft.ftstamp
		inner join b_utentes (nolock) on b_utentes.no=ft.no and b_utentes.estab=ft.estab
	WHERE
		(ft.fdata between '''+convert(varchar,@dataini)+''' AND '''+convert(varchar,@datafim)+''')
		and ft.nmdoc in (select nmdoc from cte1)'
if @entidade != ''
select @sql = @sql + N'
		and ft.nome LIKE '''+@entidade+'''+''%'''
if @design != ''		
select @sql = @sql + N'
		and (fi.design like '''+@design+'''+''%'' OR fi.ref like '''+@design+'''+''%'')'
if @receita != ''
select @sql = @sql + N'
		and ft2.u_receita = '''+@receita+''''
if @site != ''
select @sql = @sql + N'
		and ft.site = '''+@site+''''
if @numdoc != 0
select @sql = @sql + N'
		and ft.fno = '+convert(varchar,@numdoc)
if @atend != ''
select @sql = @sql + N'
		and u_nratend = '''+@atend+''''
if @doc != ''
select @sql = @sql + N'
		and ft.nmdoc = '''+@doc+''''
if @no != 0
select @sql = @sql + N'
		AND ft.NO = '+convert(varchar,@no)
if @estab != -1
select @sql = @sql + N'
		AND ft.ESTAB = '+convert(varchar,@estab)
if @ncont != ''
select @sql = @sql + N'
		and ft.ncont = '''+@ncont+''''
if @exportado != -1
select @sql = @sql + N'
		and ft.exportado = ' +convert(varchar,@exportado)
select @sql = @sql + N' 
		)x'
if @design != ''		
select @sql = @sql + N' 
		where x.id = 1'
select @sql = @sql + N' 
		order by convert(varchar,ousrdata,102)+OUSRHORA desc'
print @sql
execute (@sql)

GO
Grant Execute on dbo.up_relatorio_facturacao_pesquisarDocumentos to Public
Grant control on dbo.up_relatorio_facturacao_pesquisarDocumentos to Public
GO