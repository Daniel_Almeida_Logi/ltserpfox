/* Relatório Inventário com diferentes tipos de valorização e outros filtros

	exec up_relatorio_apuramento_inventario '20180901', '20220627', '1', 0, '', '', '', '', 'bene - mais um carrada de caracteres so para encher e ver se parte!', '', 'Loja 1', '', 0, 0, 0

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_apuramento_inventario]') IS NOT NULL
	drop procedure dbo.up_relatorio_apuramento_inventario
go

create procedure dbo.up_relatorio_apuramento_inventario
	@dataini		datetime,
	@datafim		datetime,
	@armazem	int,
	@incluiInactivos bit,
	@ref		varchar(254),
	@design		varchar(254),
	@familia	varchar(max),
	@lab		varchar(254),
	@marca		varchar(200),
	@grphmgcode	varchar(6),
	@site		varchar(55),
	@atributosFam varchar(max),
	@inluiPCL bit,
	@inluiPCT bit,
	@inluiPCP bit
/* with encryption */
AS 
BEGIN
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempFamilia'))
		DROP TABLE #TempFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempSL'))
		DROP TABLE #TempSL
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempFN'))
		DROP TABLE #TempFN
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempLOJAS'))
		DROP TABLE #TempLOJAS
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#st_inativos'))
		DROP TABLE #st_inativos
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempInventario'))
		DROP TABLE #TempInventario
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#EmpresaArm'))
		DROP TABLE #EmpresaArm

	/* Calc Loja e Armazens */
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)
	select 
		armazem
	into
		#EmpresaArm
	From
		empresa
		inner join empresa_arm on empresa.no = empresa_arm.empresa_no
	where
		empresa.site = case when @site = '' then empresa.site else @site end	
	/* Calc Familia */
	Select 
		distinct ref 
	INTO
		#TempFamilia
	From
		stfami (nolock)
	Where
		ref in (Select items from dbo.up_splitToTable(@familia,','))
		Or @familia = ''
	/* Calc Movimentos */
	SELECT
		slstamp ,cm ,cmdesc ,ref ,qtt ,epcpond ,evu ,datalc ,ousrhora ,ORIGEM ,armazem
	INTO
		#TempSl
	FROM
		sl (nolock)
	Where
		datalc <= @datafim
		and sl.armazem = @armazem
	/* Calc movimentos inventário */
	SELECT
		*
	INTO
		#TempSlmov
	FROM
		sl (nolock)
	Where
		datalc between @dataini and @datafim
		and sl.armazem = @armazem
		and origem='IF'
	/* Calc Compras */
	SELECT	
		fnstamp
		,epv
		,ref
		,fo.data
		,fo.docnome
		,FOLANSL
		,fn.ousrhora
	INTO
		#TempFN
	FROM
		fn (nolock)
		inner join fo (nolock) on fo.fostamp = fn.fostamp
		Inner join cm1 (nolock) on cm1.cmdesc = fn.docnome
	Where	
		fn.docnome IN ('N/Guia Entrada','V/Factura','V/Guia Transp.')
		and cm1.FOLANSL = 1
		and	fo.data <= @datafim
		and fo.site = case when @site = '' then fo.site else @site end
	/* Calc  Estado (inativas?) das refs */
	select 
		ref, inativo
	into
		#st_inativos
	from (
		select
			ref
			,inativo
			,data
			,row = row_number() over(partition by ref order by data desc)
		from
			st_inativos
		where
			st_inativos.data <= @datafim
	) x
	where
		row = 1
	/* Calc Inventario */
	SELECT
		#TempSlmov.ARMAZEM
		,#TempSlmov.REF
		,st.DESIGN
		,#TempSlmov.qtt
		,#TempSlmov.cmdesc
		,#TempSlmov.nome
		,convert(varchar, #TempSlmov.datalc, 102) as datalc
		,PCL_DATA = 
			ROUND(ISNULL((
				SELECT	
					TOP 1 EVU 
				FROM	
					#TempSl SL (nolock) 
				WHERE	
					(ORIGEM = 'FO' OR ORIGEM = 'IF')
					AND SL.DATALC <= @datafim
					AND SL.REF = #TempSlmov.REF
					AND EVU !=0
					AND sl.armazem in (select armazem from #EmpresaArm)
				ORDER BY 
					sl.datalc + sl.ousrhora DESC
			),st.EPCULT),2) 
		,PCP_DATA	= 
			ROUND(ISNULL((
				SELECT
					TOP 1 epcpond 
				FROM
					#TempSl
				WHERE
					ref=st.ref 
					AND datalc <= @datafim 
					AND #TEMPSL.ref = #TempSlmov.REF
					AND #TEMPSL.armazem in (select armazem from #EmpresaArm)
				ORDER BY 
					datalc + ousrhora DESC
			), st.EPCPOND),2)
		,PCT_DATA		=
			ISNULL((
				SELECT
					TOP 1 epv
				FROM
					#TempFN cteFn
				WHERE
					cteFn.REF = #TempSlmov.REF
					AND cteFn.epv > 0
					AND cteFn.data <= @datafim
				ORDER BY
					cteFn.data + cteFn.ousrhora DESC
			),st.EPCUSTO)
			,PVP_DATA	=
			ISNULL((
				SELECT
					TOP 1 u_epvp
				FROM
					fi 
				WHERE
					ref=#TempSlmov.ref 
					AND rdata <= @datafim 
					AND fi.armazem in (select armazem from #EmpresaArm)
					and fi.nmdoc in ('Venda a Dinheiro','Factura')
				ORDER BY
					rdata + ousrhora DESC
			),st.EPV1) 
		,st.stns
		,st.familia
		,PRECOCUSTOPONDERADO_ACTUAL = st.EPCPOND
		,ULTIMOPRECOCUSTO_ACTUAL = st.EPCULT
		,PRECOCUSTODETABELA_ACTUAL = st.EPCUSTO
		,PRECOVENDA1 = st.epv1
		,IVA = (SELECT TAXA FROM taxasiva (nolock) WHERE codigo = TABIVA)
		,ST.UINTR as ULTIMAENTRADA
		,ST.USAID as ULTIMASAIDA
		,ST.u_lab
		,st.faminome
		,marca = st.usr1
		,grphmgcode = isnull(grphmgcode,'')
		,grphmgdescr = isnull(grphmgdescr,'')
		,local1 = st.local
		,local2 = st.u_local
		,local3 = st.u_local2
		--,departamento = isnull(b_famDepartamentos.design ,'')
		--,seccao = isnull(b_famSeccoes.design,'')
		--,categoria = isnull(b_famCategorias.design,'')
		--,famfamilia =  isnull(b_famFamilias.design,'')
		
		--carregar classificacao do hmr - 20200923 JG		
		, ISNULL(A.descr,'') as departamento
		, ISNULL(B.descr,'') as seccao
		, ISNULL(C.descr,'') as categoria
		, ISNULL(D.descr,'') as segmento

		
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 1) valDepartamento
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 2) valSeccao
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 3) valCategoria
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 4) valSegmento
	INTO
		#TempInventario
	FROM
		#TempSlmov (nolock)
		INNER JOIN ST (nolock) ON #TempSlmov.REF = ST.REF
		left join fprod (nolock) on fprod.cnp = st.ref
		left join #st_inativos sti on sti.ref = st.ref
		--left join b_famDepartamentos (nolock) on st.u_depstamp = b_famDepartamentos.depstamp
		--left join b_famSeccoes (nolock) on st.u_secstamp = b_famSeccoes.secstamp
		--left join b_famCategorias (nolock) on st.u_catstamp = b_famCategorias.catstamp
		--left join b_famFamilias (nolock) on st.u_famstamp = b_famFamilias.famstamp

		--carregar classificacao do hmr - 20200923 JG		
		left join grande_mercado_hmr (nolock) A on A.id = st.u_depstamp
		left join mercado_hmr (nolock) B on B.id = st.u_secstamp
		left join categoria_hmr (nolock) C on C.id = st.u_catstamp 
		left join segmento_hmr (nolock) D on D.id = st.u_segstamp		
	WHERE
		st.stns = 0
		AND st.site_nr = @site_nr
		and st.ref like @ref + '%'
		and st.DESIGN like @design + '%'
		and isnull(sti.inativo,0) = case when @incluiInactivos = 1 then isnull(sti.inativo,0) else 0 end


	/* Result Set */
	Select
		*
	From
		#TempInventario cteInventario
	where 
		cteInventario.armazem in (select armazem from #EmpresaArm)
		and cteInventario.stns = 0
		and	cteInventario.ref LIKE @ref + '%' 
		and cteInventario.design = case when @design = '' then cteInventario.design else @design end
		and armazem = @armazem
		and (@familia = '' or cteInventario.familia in (Select ref from #TempFamilia))
		and cteInventario.u_lab = case when @lab = '' then cteInventario.u_lab else @lab end 
		and marca = case when @marca = '' then marca else @marca end
		and grphmgcode = case when @grphmgcode = '' then grphmgcode else @grphmgcode end 
		and 
		(
			(departamento	= valDepartamento or valDepartamento is null)
			and (seccao		= valSeccao or valSeccao is null)
			and (categoria	= valCategoria or valCategoria is null)
			and (segmento	= valSegmento or valSegmento is null)
		)
	Order by
		datalc
		,cmdesc
		,ref
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempFamilia'))
		DROP TABLE #TempFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempSL'))
		DROP TABLE #TempSL
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempFN'))
		DROP TABLE #TempFN
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#st_inativos'))
		DROP TABLE #st_inativos
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempInventario'))
		DROP TABLE #TempInventario
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#EmpresaArm'))
		DROP TABLE #EmpresaArm
END
GO
Grant Execute on dbo.up_relatorio_apuramento_inventario to Public
Grant control on dbo.up_relatorio_apuramento_inventario to Public
Go