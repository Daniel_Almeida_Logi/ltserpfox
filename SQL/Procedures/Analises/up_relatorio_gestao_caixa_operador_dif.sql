/* Relatório Caixas Operador

	exec up_relatorio_gestao_caixa_operador_dif '20191236','20200316',0,999, 'ATLANTICO'
	exec up_relatorio_gestao_caixa_operador_dif '20200417','20200417',0,999, 'Loja 1'
	exec up_relatorio_gestao_caixa_operador_dif '20200317','20200317',0,999, 'ATLANTICO'
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_caixa_operador_dif]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_caixa_operador_dif
go


create procedure [dbo].up_relatorio_gestao_caixa_operador_dif
	@dataIni	DATETIME
	,@dataFim	DATETIME
	,@opIni		AS NUMERIC(9,0)
	,@opFim		AS NUMERIC(9,0)
	,@site		VARCHAR(30)
/* with encryption */
AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosMOV'))
		DROP TABLE #dadosMOV

	DECLARE @usaPagCentral BIT
	DECLARE @gestaoCxOperador BIT
	SET @usaPagCentral =  ISNULL((SELECT bool FROM b_parameters WHERE stamp='ADM0000000079'),0)
	SET @gestaoCxOperador = (SELECT gestao_cx_operador FROM empresa WHERE site = @site)
	IF @gestaoCxOperador = 0
		BEGIN
			SELECT
				 site			
				,pnome			
				,pno			
				,Vendedor		
				,Nome			
				,Data			
				,dfechar		
				,fecho_dinheiro
				,fecho_mb		
				,fecho_visa		
				,fecho_cheques	
				,fecho_epaga3	
				,fecho_epaga4	
				,fecho_epaga5	
				,fecho_epaga6	
				,fundocaixa		
				,fecho_total	
				,obs			
				,registo		
				,total_movExtra			
				FROM
					(SELECT
							 site				= cx.site	
							,pnome				= cx.pnome			
							,pno				= cx.pno
							,Vendedor			= cx.fuserno
							,Nome				= cx.fusername
							,Data				= CONVERT(VARCHAR,cx.dabrir,102) + ' ' + habrir
							,dfechar			= CONVERT(VARCHAR,cx.dfechar,102) + ' ' + hfechar	
							,fecho_dinheiro		= cx.fecho_dinheiro
							,fecho_mb			= cx.fecho_mb
							,fecho_visa			= cx.fecho_visa
							,fecho_cheques		= cx.fecho_cheques
							,fecho_epaga3		= cx.fecho_epaga3
							,fecho_epaga4		= cx.fecho_epaga4
							,fecho_epaga5		= cx.fecho_epaga5
							,fecho_epaga6		= cx.fecho_epaga6
							,fundocaixa			= cx.fundocaixa
							,fecho_total		= cx.fecho_dinheiro + cx.fecho_mb + cx.fecho_visa + cx.fecho_cheques + cx.fecho_epaga3 + cx.fecho_epaga4 + cx.fecho_epaga5 + cx.fecho_epaga6 + cx.fundocaixa
							,obs				= causa
							,registo		    ='Contado'
							,total_movExtra     =0
						FROM 
							cx (NOLOCK)
						WHERE 
							cx.dabrir between @dataini and @DataFim 
							and cx.fuserno >= @opIni
							and cx.fuserno <= @opFim
							and cx.site = CASE WHEN @site = '' THEN cx.site ELSE @site END 
							and cx.fechada = 1
					

					UNION ALL 
							SELECT
								 site				= cx.site
								,pnome				= cx.pnome
								,pno				= cx.pno
								,Vendedor			= pc.vendedor
								,Nome				= pc.nome
								,Data				= CONVERT(VARCHAR,cx.dabrir,102) + ' ' + habrir
								,dfechar			= CONVERT(VARCHAR,cx.dfechar,102) + ' ' + hfechar
								,fecho_dinheiro		= SUM(CASE WHEN left(pc.nrAtend,2) = 'Cx' THEN 0 ELSE pc.evdinheiro END)*(-1) --sum(pc.evdinheiro)
								,fecho_mb			= SUM(pc.epaga2)
								,fecho_visa			= SUM(pc.epaga1)
								,fecho_cheques		= SUM(pc.echtotal)
								,fecho_epaga3		= SUM(pc.epaga3)
								,fecho_epaga4		= sum(pc.epaga4 * ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'USD', pc.oData),1)) --USD
								,fecho_epaga5		= sum(pc.epaga5 * ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'EUR', pc.oData),1))--EUR
								,fecho_epaga6		= SUM(pc.epaga6)
								,fundocaixa			= (cx.fundocaixa)		
								,fecho_total		= (SUM(pc.evdinheiro) + SUM(pc.epaga2) + SUM(pc.epaga1) + SUM(pc.echtotal) + SUM(pc.epaga3) + SUM(pc.epaga4) + SUM(pc.epaga5) + SUM(pc.epaga6) +  cx.fundocaixa)
								,obs				= causa
								,registo			= 'Registado'
								,total_movExtra		= SUM(CASE WHEN left(pc.nrAtend,2) = 'Cx' THEN pc.evdinheiro ELSE 0 END)
							from 
								cx (nolock) 
								inner join B_pagCentral pc on pc.cxstamp = cx.cxstamp
								--inner join ft on ft.u_nratend=pc.nrAtend 
							where 
								cx.dabrir between @dataini and @DataFim 
								and cx.site = (case when @site = '' then cx.site else @site end )
								and pc.vendedor >= @opIni
								and pc.vendedor <= @opFim
								and cx.fechada = 1
								and pc.ignoraCaixa = 0
								--and ft.u_nratend <>''
							group by 
								cx.site
							   ,cx.pnome
							   ,cx.pno
							   ,pc.vendedor
							   ,pc.nome
							   ,cx.dabrir
							   ,cx.habrir
							   ,cx.dfechar
							   ,cx.hfechar
							   ,cx.fundocaixa
							   ,cx.causa

					UNION ALL 

						SELECT
							 site				= cx.site
							,pnome				= cx.pnome
							,pno				= cx.pno
							,Vendedor			= cx.fuserno
							,Nome				= cx.fusername
							,Data				= CONVERT(VARCHAR,cx.dabrir,102) + ' ' + habrir
							,dfechar			= CONVERT(VARCHAR,cx.dfechar,102) + ' ' + hfechar	
							,fecho_dinheiro		= SUM(CASE WHEN left(pc.nrAtend,2) = 'Cx' THEN 0 ELSE ISNULL(pc.evdinheiro,0) END)
							,Multibanco			= (SUM(ISNULL(pc.epaga2,0)))
							,fecho_mb			= (SUM(ISNULL(pc.epaga1,0)))
							,fecho_cheques		= (SUM(ISNULL(pc.echtotal,0)))
							,fecho_epaga3		= (SUM(ISNULL(pc.epaga3,0)))
							,fecho_epaga4		= sum(pc.epaga4 * ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'USD', pc.oData),1)) --USD
							,fecho_epaga5		= sum(pc.epaga5 * ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'EUR', pc.oData),1))--EUR
							,fecho_epaga6		= (SUM(ISNULL(pc.epaga6,0)))
							,fundocaixa			= (cx.fundocaixa)
							,fecho_total		= (SUM(CASE WHEN left(pc.nrAtend,2) = 'Cx' THEN 0 ELSE ISNULL(pc.evdinheiro,0) END) + SUM(ISNULL(pc.epaga2,0)) 
													+ SUM(ISNULL(pc.epaga1,0)) + SUM(ISNULL(pc.echtotal,0)) + SUM(ISNULL(pc.epaga3,0)) + SUM(ISNULL(pc.epaga4,0)) 
													+ SUM(ISNULL(pc.epaga5,0)) + SUM(ISNULL(pc.epaga6,0)) + (cx.fundocaixa))
							,obs				= causa
							,registo			='Registado'
							,total_movExtra		= sum(case when left(pc.nrAtend,2) = 'Cx' then pc.evdinheiro else 0 end)
						FROM 
							cx (NOLOCK) 
							left join B_pagCentral pc ON pc.cxstamp = cx.cxstamp
						--	inner join ft on ft.u_nratend=pc.nrAtend
						WHERE 
							cx.dabrir between  @dataini and @DataFim 
							and cx.site = case when @site = '' then cx.site else @site end 
							AND cx.cxstamp NOT IN (SELECT DISTINCT B_pagCentral.ssstamp FROM B_pagCentral WHERE (SELECT CONVERT(date,oData)) between  @dataini and @DataFim  ) 
							and cx.fechada = 1
							and pc.ignoraCaixa = 0
						GROUP BY 
							 cx.site
							,cx.dabrir
							,cx.fuserno
							,cx.fusername
							,cx.pnome
							,cx.pno
							,cx.habrir
							,cx.dfechar
							,cx.hfechar
							,cx.fundoCaixa
							,cx.causa

					)AS ResultadoCaixa
			ORDER BY Nome ASC, 
					 pno ASC, 
					 Data ASC,
					 registo Desc 


		END
	ELSE
		BEGIN

			SELECT 
				 ss.ssstamp
				,total_movExtra		= sum(case when left(pc.nrAtend,2) = 'Cx' then pc.evdinheiro else 0 end)
			INTO
				#dadosMOV
			FROM 
				ss (NOLOCK) 
				left join B_pagCentral pc ON pc.ssstamp = ss.ssstamp
			WHERE 
				ss.dabrir between @dataini and @DataFim 
				and ss.fuserno >= @opIni
				and ss.fuserno <= @opFim
				and ss.site = CASE WHEN @site = '' then ss.site ELSE @site END 
				and ss.fechada = 1
				AND ss.ssstamp = (case 
									when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
									then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
									else ss.ssstamp
								end)
			group by 
				ss.ssstamp


			SELECT
				 site			
				,pnome			
				,pno			
				,Vendedor		
				,Nome			
				,Data			
				,dfechar		
				,fecho_dinheiro
				,fecho_mb		
				,fecho_visa		
				,fecho_cheques	
				,fecho_epaga3	
				,fecho_epaga4	
				,fecho_epaga5	
				,fecho_epaga6	
				,fundocaixa		
				,fecho_total	
				,obs			
				,registo		
				,total_movExtra	
				FROM
					(SELECT
						 site				= ss.site
						,pnome				= ss.pnome
						,pno				= ss.pno
						,Vendedor			= ss.fuserno
						,Nome				= ss.fusername
						,Data				= CONVERT(varchar,ss.dabrir,102)  + ' ' + habrir
						,dfechar			= CONVERT(varchar,ss.dfechar,102) + ' ' + hfechar	
						,fecho_dinheiro		= ss.evdinheiro
						,fecho_mb			= ss.epaga1
						,fecho_visa			= ss.epaga2
						,fecho_cheques		= ss.echtotal
						,fecho_epaga3		= ss.fecho_epaga3
						,fecho_epaga4		= ss.fecho_epaga4
						,fecho_epaga5		= ss.fecho_epaga5
						,fecho_epaga6		= ss.fecho_epaga6
						,fundocaixa			= ss.fundocaixa
						,fecho_total		= ss.evdinheiro + ss.epaga1 + ss.epaga2 + ss.echtotal + ss.fecho_epaga3 + ss.fecho_epaga4 + ss.fecho_epaga5 + ss.fecho_epaga6 + ss.fundocaixa
						,obs				= causa
						,registo			='Contado'
						,total_movExtra     = (select total_movExtra from #dadosMOV where #dadosMOV.ssstamp= ss.ssstamp)
					FROM 
						ss (NOLOCK)					
					WHERE 
						ss.dabrir between @dataini and @DataFim 
						and ss.fuserno >= @opIni
						and ss.fuserno <= @opFim
						and ss.site = CASE WHEN @site = '' then ss.site ELSE @site END 
						and ss.fechada = 1
						AND ss.ssstamp = (case 
											when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
											then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
											else ss.ssstamp
										end)

				UNION ALL

					SELECT
						 site				= ss.site
						,pnome				= ss.pnome
						,pno				= ss.pno
						,Vendedor			= pc.vendedor
						,Nome				= pc.nome
						,Data				= CONVERT(VARCHAR,ss.dabrir,102) + ' ' + habrir
						,dfechar			= CONVERT(VARCHAR,ss.dfechar,102) + ' ' + hfechar	
						,fecho_dinheiro		= SUM(CASE WHEN left(pc.nrAtend,2) = 'Cx' THEN 0 ELSE pc.evdinheiro END)
						,Multibanco			= SUM(pc.epaga2)
						,fecho_mb			= SUM(pc.epaga1)
						,fecho_cheques		= SUM(pc.echtotal)
						,fecho_epaga3		= SUM(pc.epaga3)
						,fecho_epaga4		= sum(pc.epaga4 * ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'USD', pc.oData),1)) --USD
						,fecho_epaga5		= sum(pc.epaga5 * ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'EUR', pc.oData),1))--EUR
						,fecho_epaga6		= SUM(pc.epaga6)
						,fundocaixa			= (ss.fundocaixa)
						,fecho_total		= (SUM(ISNULL(pc.evdinheiro,0)) + SUM(pc.epaga2) + SUM(pc.epaga1) + SUM(pc.echtotal) + SUM(pc.epaga3) + SUM(pc.epaga4) + SUM(pc.epaga5) + SUM(pc.epaga6) + ss.fundocaixa)
						,obs				= causa
						,registo			='Registado'
						,total_movExtra		= sum(case when left(pc.nrAtend,2) = 'Cx' then pc.evdinheiro else 0 end)
					FROM 
						ss (NOLOCK) 
						inner join B_pagCentral pc ON pc.ssstamp = ss.ssstamp
					--	inner join ft on ft.u_nratend=pc.nrAtend
					WHERE 
						ss.dabrir between @dataini and @DataFim 
						and ss.site = case when @site = '' then ss.site else @site end 
						and pc.vendedor >= @opIni
						and pc.vendedor <= @opFim
						and ss.fechada = 1
						and pc.ignoraCaixa = 0
						AND ss.ssstamp = (case 
											when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
											then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
											else ss.ssstamp
										end)
					GROUP BY 
						ss.site
						,ss.dabrir
						,pc.vendedor
						,pc.nome
						,ss.pnome
						,ss.pno
						,ss.habrir
						,ss.dfechar
						,ss.hfechar
						,ss.fundoCaixa
						,ss.causa

		UNION ALL 

			SELECT
						 site				= ss.site
						,pnome				= ss.pnome
						,pno				= ss.pno
						,Vendedor			= ss.fuserno
						,Nome				= ss.fusername
						,Data				= CONVERT(VARCHAR,ss.dabrir,102) + ' ' + habrir
						,dfechar			= CONVERT(VARCHAR,ss.dfechar,102) + ' ' + hfechar	
						,fecho_dinheiro		= SUM(CASE WHEN left(pc.nrAtend,2) = 'Cx' THEN 0 ELSE ISNULL(pc.evdinheiro,0) END)
						,Multibanco			= SUM(ISNULL(pc.epaga2,0))
						,fecho_mb			= SUM(ISNULL(pc.epaga1,0))
						,fecho_cheques		= SUM(ISNULL(pc.echtotal,0))
						,fecho_epaga3		= SUM(ISNULL(pc.epaga3,0))
						,fecho_epaga4		= sum(pc.epaga4 * ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'USD', pc.oData),1)) --USD
						,fecho_epaga5		= sum(pc.epaga5 * ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'EUR', pc.oData),1))--EUR
						,fecho_epaga6		= SUM(ISNULL(pc.epaga6,0))
						,fundocaixa			= (ss.fundocaixa)
						,fecho_total		= (SUM(ISNULL(pc.evdinheiro,0)) + SUM(ISNULL(pc.epaga2,0)) + SUM(ISNULL(pc.epaga1,0)) + SUM(ISNULL(pc.echtotal,0)) + SUM(ISNULL(pc.epaga3,0)) + SUM(ISNULL(pc.epaga4,0)) + SUM(ISNULL(pc.epaga5,0)) + SUM(ISNULL(pc.epaga6,0)) + ss.fundocaixa)
						,obs				= causa
						,registo			='Registado'
						,total_movExtra		= sum(case when left(pc.nrAtend,2) = 'Cx' then pc.evdinheiro else 0 end)
					FROM 
						ss (NOLOCK) 
						left join B_pagCentral pc ON pc.ssstamp = ss.ssstamp
					--	inner join ft on ft.u_nratend=pc.nrAtend
					WHERE 
						ss.dabrir between  @dataini and @DataFim 
						and ss.site = case when @site = '' then ss.site else @site end 
						AND ss.ssstamp NOT IN (SELECT DISTINCT B_pagCentral.ssstamp FROM B_pagCentral WHERE (SELECT CONVERT(date,oData)) between  @dataini and @DataFim  ) 
						and ss.fechada = 1
						and pc.ignoraCaixa = 0
						/*AND ss.ssstamp = (case 
											when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
											then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
											else ss.ssstamp
										end)*/
					GROUP BY 
						ss.site
						,ss.dabrir
						,ss.fuserno
						,ss.fusername
						,ss.pnome
						,ss.pno
						,ss.habrir
						,ss.dfechar
						,ss.hfechar
						,ss.fundoCaixa
						,ss.causa

			)AS ResultadoCaixa
			ORDER BY Nome ASC, 
					 pno ASC, 
					 Data ASC,
					 registo Desc 


		END
			
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosMOV'))
		DROP TABLE #dadosMOV

GO
GRANT EXECUTE ON dbo.up_relatorio_gestao_caixa_operador_dif TO PUBLIC
GRANT CONTROL ON dbo.up_relatorio_gestao_caixa_operador_dif TO PUBLIC
GO