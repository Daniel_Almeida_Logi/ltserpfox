/*    */
if OBJECT_ID('[dbo].[up_relatorio_gestao_caixa_detalheMovimentosExtra]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_caixa_detalheMovimentosExtra
go

create procedure [dbo].[up_relatorio_gestao_caixa_detalheMovimentosExtra]
@dataIni		datetime
,@dataFim		datetime
,@op			varchar(max)
,@nrAtend		varchar(20)
,@site			varchar(55)
/* with encryption */
AS
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores
	/* Operadores */
	Select
		iniciais
		,cm = userno
		,username
	into
		#dadosOperadores
	from
		b_us (nolock)
	Where
		userno in (select items from dbo.up_splitToTable(@op,','))
		or @op= '0'
		or @op= ''
	select 
		NOPR = vendedor
		,b_us.usrinis
		,DATA = convert(date,oData)
		,HORA = convert(varchar,oData,108)
		,NAT = nrAtend
		,NTM = terminal
		,VLAT = total
		,DINHEIRO = evdinheiro
		,MB = B_pagCentral.epaga2
		,VISA = B_pagCentral.epaga1
		,CHEQUE = echtotal
		,B_pagCentral.obs
		,tipo = case when total < 0 then 'S' else 'E' end
	from 
		B_pagCentral (nolock)
		left join b_us (nolock) on B_pagCentral.vendedor = b_us.userno
	where
		convert(date,B_pagCentral.oData) between @dataIni and @dataFim
		and B_pagCentral.vendedor in (select cm from #dadosOperadores)
		and (B_pagCentral.nrAtend = @nrAtend or @nrAtend = '')
		and B_pagCentral.site = @site
		and B_pagCentral.nrAtend like 'CX%'
		and B_pagCentral.ignoraCaixa = 0
	Order by
		oData,nrAtend
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores
	
GO
Grant Execute on dbo.up_relatorio_gestao_caixa_detalheMovimentosExtra to Public
GO
Grant control on dbo.up_relatorio_gestao_caixa_detalheMovimentosExtra to Public
GO