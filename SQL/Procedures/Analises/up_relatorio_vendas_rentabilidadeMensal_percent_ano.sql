/* 
	Relatório Vendas Mensais & Relatório Evolução Rentabilidade Mensal - cálculo de diferencas em percentagem
	
	exec up_relatorio_vendas_rentabilidadeMensal_percent_ano 2017, 1, 12, '', '', '', '', '', 'Loja 1'
*/ 

SET ANSI_NULLS OFF

GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_vendas_rentabilidadeMensal_percent_ano]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_rentabilidadeMensal_percent_ano
go

create procedure dbo.up_relatorio_vendas_rentabilidadeMensal_percent_ano
    @ano			 int,
	@mesIni		     int,
	@mesFim			 int,
	@lab			 varchar(120),
	@marca			 varchar(200),
	@op				 varchar(max),
	@atributosFam    varchar(max),
	@familia		 varchar(max),
	@site			 varchar(120),
	@client numeric(10) = 0,
	@estab numeric(5) = -1
	/* with encryption */
AS
SET NOCOUNT ON
	declare @dataIni datetime, @dataFim datetime, @dataIniant datetime, @dataFimant datetime
	set @dataIni = CONVERT(VARCHAR(4), @ano) 
					+ '-' + REPLICATE('0',2-LEN(@mesIni)) + CONVERT(VARCHAR(4), @mesIni) + '-01'
	set @dataFim = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,CONVERT(VARCHAR(4), @ano) 
					+ '-' + REPLICATE('0',2-LEN(@mesFim)) + CONVERT(VARCHAR(4), @mesFim) + '-01')+1,0))
	set @dataIniant = CONVERT(VARCHAR(4), @ano-1) 
					+ '-' + REPLICATE('0',2-LEN(@mesIni)) + CONVERT(VARCHAR(4), @mesIni) + '-01'
	set @dataFimant = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,CONVERT(VARCHAR(4), @ano-1) 
					+ '-' + REPLICATE('0',2-LEN(@mesFim)) + CONVERT(VARCHAR(4), @mesFim) + '-01')+1,0))
	/* Elimina Tabelas */
	If OBJECT_ID('tempdb.dbo.#dadosVendasBase') IS NOT NULL
		DROP TABLE #dadosVendasBase
	If OBJECT_ID('tempdb.dbo.#dadosVendasBaseant') IS NOT NULL
		DROP TABLE #dadosVendasBaseant
	If OBJECT_ID('tempdb.dbo.#dadosFamilia') IS NOT NULL
		DROP TABLE #dadosFamilia
	If OBJECT_ID('tempdb.dbo.#dadosOperadores') IS NOT NULL
		DROP TABLE #dadosOperadores
	If OBJECT_ID('tempdb.dbo.#dadosRefs') IS NOT NULL
		DROP TABLE #dadosRefs
	If OBJECT_ID('tempdb.dbo.#dadosSt') IS NOT NULL
		DROP TABLE #dadosSt
	If OBJECT_ID('tempdb.dbo.#EmpresaArm') IS NOT NULL
		DROP TABLE #EmpresaArm
	If OBJECT_ID('tempdb.dbo.#vendasano') IS NOT NULL
		DROP TABLE #vendasano
	If OBJECT_ID('tempdb.dbo.#vendasanoant') IS NOT NULL
		DROP TABLE #vendasanoant
	/* Calc Loja e Armazens */
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)
	select 
		armazem
	into
		#EmpresaArm
	From
		empresa
		inner join empresa_arm on empresa.no = empresa_arm.empresa_no
	where
		empresa.site = case when @site = '' then empresa.site else @site end
	/* Calc Dados Produto ST */
	Select
		st.ref
		,st.design
		,usr1
		,u_lab
		,departamento = isnull(b_famDepartamentos.design ,'')
		,seccao = isnull(b_famSeccoes.design,'')
		,categoria = isnull(b_famCategorias.design,'')
		,famfamilia =  isnull(b_famFamilias.design,'')
		,dispdescr = ISNULL(dispdescr,'')
		,generico = ISNULL(generico,CONVERT(bit,0))
		,psico = ISNULL(psico,CONVERT(bit,0))
		,benzo = ISNULL(benzo,CONVERT(bit,0))
		,protocolo = ISNULL(protocolo,CONVERT(bit,0))
		,dci = ISNULL(dci,'')
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 1) valDepartamento
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 2) valSeccao
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 3) valCategoria
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 4) valFamfamilia
	INTO 
		#dadosSt
	From
		st (nolock)
		left join fprod (nolock) on st.ref = fprod.cnp
		left join b_famDepartamentos (nolock) on st.u_depstamp = b_famDepartamentos.depstamp
		left join b_famSeccoes (nolock) on st.u_secstamp = b_famSeccoes.secstamp
		left join b_famCategorias (nolock) on st.u_catstamp = b_famCategorias.catstamp
		left join b_famFamilias (nolock) on st.u_famstamp = b_famFamilias.famstamp
	Where	
		st.u_lab = case when @lab = '' then u_lab else @lab end
		AND st.usr1 = case when @marca = '' then usr1 else @marca end	
		AND site_nr = @site_nr
	/*  Calc Familias */
	Select 
		distinct ref 
	into 
		#dadosFamilia
	From 
		stfami (nolock)  
	Where 
		ref in (Select items from dbo.up_splitToTable(@familia,',')) 
		Or @familia = '0' 
		or @familia = ''
	/* Calc Operadores */
	Select
		iniciais, cm = userno, username
	into
		#dadosOperadores
	from
		b_us (nolock)
	Where
		userno in (select items from dbo.up_splitToTable(@op,','))
		or @op= '0'
		or @op= ''	
	/* Define se vais mostrar resultados abaixo do Cliente 199 */
	declare @Entidades as bit
	set @Entidades = case when (select tipoempresa from empresa where site = @site) in ('FARMACIA', 'PARAFARMACIA') then convert(bit,1) else convert(bit,0) end
	/* Dados Base Vendas  */
	create table #dadosVendasBase (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
       ,ecusto numeric(19,6)
	   	,loja  varchar(50)
		,loja_nr  numeric(5,0)
	)
    insert #dadosVendasBase
    exec up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site
	/* Dados Base Vendas ano anterior */
	create table #dadosVendasBaseant (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(3,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
       ,ecusto numeric(19,6)
	   	,loja  varchar(50)
		,loja_nr  numeric(5,0)
	)
    insert #dadosVendasBaseant
    exec up_relatorio_vendas_base_detalhe @dataIniant, @dataFimant, @site
	/* Elimina Vendas tendo em conta tipo de cliente*/
	If @Entidades = 1
	delete from #dadosVendasBase where #dadosVendasBase.no < 199
	If @Entidades = 1
	delete from #dadosVendasBaseant where #dadosVendasBaseant.no < 199
	Select 
		[nrAtend]
		,[ftstamp]
		,[fdata]
		,[no]
		,[ndoc]
		,[fno]
		,[tipodoc]
		,[u_tipodoc]
		,vb.[ref]
		,vb.[design]
		,[u_epvp]
		,[epcpond]
		,[iva]
		,[qtt]
		,[etiliquido]
		,[etiliquidoSIva]
		,[ettent1]
		,[ettent2]
		,[ettent1SIva]
		,[ettent2SIva]
		,[desconto]
		,[descvalor]
		,[descvale]
		,[ousrhora]
		,[ousrinis]
		,[vendnm]
		,[u_ltstamp]
		,[u_ltstamp2]
		,[estab]
	into
		#vendasano
	From
		#dadosVendasBase vb
		left join #dadosSt on vb.ref = #dadosSt.ref
	Where
		vb.fdata between @dataIni and @dataFim
		and vb.u_tipodoc not in (1, 5)
		and isnull(#dadosSt.u_lab,'') = CASE When @lab = '' Then isnull(#dadosSt.u_lab,'') Else @lab End
		and isnull(#dadosSt.usr1,'') = CASE When @marca = '' Then isnull(#dadosSt.usr1,'') Else @marca End
		and (vb.ousrinis in (select iniciais from #dadosOperadores) or @op= '0' or @op= '' )
		and vb.familia in (Select ref from #dadosFamilia) 
		and vb.[no] = case when @client > 0 then @client else vb.[no] End
		and vb.estab = case when @estab > = 0 then @estab else vb.estab End
		/* Tratamento Familias*/
		and
		(
			(departamento	= valDepartamento or valDepartamento is null)
			and (seccao		= valSeccao or valSeccao is null)
			and (categoria	= valCategoria or valCategoria is null)
			and (famfamilia	= valFamfamilia or valFamfamilia is null)
		)
		and (vb.no > 199	or @Entidades = 0)
	Select 
		[nrAtend]
		,[ftstamp]
		,[fdata]
		,[no]
		,[ndoc]
		,[fno]
		,[tipodoc]
		,[u_tipodoc]
		,vbant.[ref]
		,vbant.[design]
		,[u_epvp]
		,[epcpond]
		,[iva]
		,[qtt]
		,[etiliquido]
		,[etiliquidoSIva]
		,[ettent1]
		,[ettent2]
		,[ettent1SIva]
		,[ettent2SIva]
		,[desconto]
		,[descvalor]
		,[descvale]
		,[ousrhora]
		,[ousrinis]
		,[vendnm]
		,[u_ltstamp]
		,[u_ltstamp2]
		,[estab]
	into
		#vendasanoant
	From
		#dadosVendasBaseant vbant
		left join #dadosSt on vbant.ref = #dadosSt.ref
	Where
		vbant.fdata between @dataIniant and @dataFimant
		and vbant.u_tipodoc not in (1, 5)
		and isnull(#dadosSt.u_lab,'') = CASE When @lab = '' Then isnull(#dadosSt.u_lab,'') Else @lab End
		and isnull(#dadosSt.usr1,'') = CASE When @marca = '' Then isnull(#dadosSt.usr1,'') Else @marca End
		and (vbant.ousrinis in (select iniciais from #dadosOperadores) or @op= '0' or @op= '' )
		and vbant.familia in (Select ref from #dadosFamilia) 
		and vbant.[no] = case when @client > 0 then @client else vbant.[no] End
		and vbant.estab = case when @estab > = 0 then @estab else vbant.estab End
		/* Tratamento Familias*/
		and
		(
			(departamento	= valDepartamento or valDepartamento is null)
			and (seccao		= valSeccao or valSeccao is null)
			and (categoria	= valCategoria or valCategoria is null)
			and (famfamilia	= valFamfamilia or valFamfamilia is null)
		)
		and (vbant.no > 199	or @Entidades = 0)
	Select 
		case when (select sum(#vendasanoant.etiliquidoSIva) from #vendasanoant ) <>0 then 
				isnull(case when ((sum(etiliquidoSIva)-(select sum(#vendasanoant.etiliquidoSIva) from #vendasanoant ))/(select sum(#vendasanoant.etiliquidoSIva) from #vendasanoant ))*100 > 0
				then ((sum(etiliquidoSIva)-(select sum(#vendasanoant.etiliquidoSIva) from #vendasanoant ))/(select sum(#vendasanoant.etiliquidoSIva) from #vendasanoant ))*100
				else ((((select sum(#vendasanoant.etiliquidoSIva) from #vendasanoant )-sum(etiliquidoSIva))/(select sum(#vendasanoant.etiliquidoSIva) from #vendasanoant ))*100)*(-1)
				end, 0)
			else
				0
			end 
			as etiliquidoSIva
		,case when (select sum(#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva) from #vendasanoant ) <>0 then
				isnull(case when ((sum(ettent1SIva+ettent2SIva)-(select sum(#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva) from #vendasanoant ))/(select sum(#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva) from #vendasanoant ))*100 > 0
				then ((sum(ettent1SIva+ettent2SIva)-(select sum(#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva) from #vendasanoant ))/(select sum(#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva) from #vendasanoant ))*100
				else ((((select sum(#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva) from #vendasanoant )-sum(ettent1SIva+ettent2SIva))/(select sum(#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva) from #vendasanoant ))*100)*(-1)
				end ,0)
			else
				0
			end
			as vdpvent	
		, case when (select sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva) from #vendasanoant )<>0 then
				isnull(case when ((sum(etiliquidoSIva+ettent1SIva+ettent2SIva)-(select sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva) from #vendasanoant ))/(select sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva) from #vendasanoant ))*100 > 0
				then ((sum(etiliquidoSIva+ettent1SIva+ettent2SIva)-(select sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva) from #vendasanoant ))/(select sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva) from #vendasanoant ))*100
				else ((((select sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva) from #vendasanoant )-sum(etiliquidoSIva+ettent1SIva+ettent2SIva))/(select sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva) from #vendasanoant ))*100)*(-1)
				end, 0)
			else
				0
			end
			as vdpv		
		, case when (select sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva-#vendasanoant.epcpond) from #vendasanoant ) <>0 then
				isnull(case when ((sum(etiliquidoSIva+ettent1SIva+ettent2SIva-epcpond)-(select sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva-#vendasanoant.epcpond) from #vendasanoant ))/(select sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva-#vendasanoant.epcpond) from #vendasanoant ))*100 > 0
				then ((sum(etiliquidoSIva+ettent1SIva+ettent2SIva-epcpond)-(select sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva-#vendasanoant.epcpond) from #vendasanoant ))/(select sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva-#vendasanoant.epcpond) from #vendasanoant ))*100
				else ((((select sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva-#vendasanoant.epcpond) from #vendasanoant )-sum(etiliquidoSIva+ettent1SIva+ettent2SIva-epcpond))/(select sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva-#vendasanoant.epcpond) from #vendasanoant ))*100)*(-1)
				end, 0)
			else
				0
			end
			as bb
		, case when (select sum(case when ((#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva)<= 0 or ((#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva)-(#vendasanoant.epcpond))<0) then 0 else ((#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva)-(#vendasanoant.epcpond))*100/(#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva) end) from #vendasanoant )<>0 then
				isnull(case when ((sum(case when ((etiliquidoSIva + ettent1SIva + ettent2SIva)<= 0 or ((etiliquidoSIva + ettent1SIva + ettent2SIva)-(epcpond))<0) then 0 else ((etiliquidoSIva + ettent1SIva + ettent2SIva)-(epcpond))*100/(etiliquidoSIva + ettent1SIva + ettent2SIva) end)-(select sum(case when ((#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva)<= 0 or ((#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva)-(#vendasanoant.epcpond))<0) then 0 else ((#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva)-(#vendasanoant.epcpond))*100/(#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva) end) from #vendasanoant ))/(select sum(case when ((#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva)<= 0 or ((#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva)-(#vendasanoant.epcpond))<0) then 0 else ((#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva)-(#vendasanoant.epcpond))*100/(#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva) end) from #vendasanoant ))*100 > 0
				then ((sum(case when ((etiliquidoSIva + ettent1SIva + ettent2SIva)<= 0 or ((etiliquidoSIva + ettent1SIva + ettent2SIva)-(epcpond))<0) then 0 else ((etiliquidoSIva + ettent1SIva + ettent2SIva)-(epcpond))*100/(etiliquidoSIva + ettent1SIva + ettent2SIva) end)-(select sum(case when ((#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva)<= 0 or ((#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva)-(#vendasanoant.epcpond))<0) then 0 else ((#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva)-(#vendasanoant.epcpond))*100/(#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva) end) from #vendasanoant ))/(select sum(case when ((#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva)<= 0 or ((#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva)-(#vendasanoant.epcpond))<0) then 0 else ((#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva)-(#vendasanoant.epcpond))*100/(#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva) end) from #vendasanoant ))*100
				else ((((select sum(case when ((#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva)<= 0 or ((#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva)-(#vendasanoant.epcpond))<0) then 0 else ((#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva)-(#vendasanoant.epcpond))*100/(#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva) end) from #vendasanoant )-sum(case when ((etiliquidoSIva + ettent1SIva + ettent2SIva)<= 0 or ((etiliquidoSIva + ettent1SIva + ettent2SIva)-(epcpond))<0) then 0 else ((etiliquidoSIva + ettent1SIva + ettent2SIva)-(epcpond))*100/(etiliquidoSIva + ettent1SIva + ettent2SIva) end))/(select sum(case when ((#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva)<= 0 or ((#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva)-(#vendasanoant.epcpond))<0) then 0 else ((#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva)-(#vendasanoant.epcpond))*100/(#vendasanoant.etiliquidoSIva + #vendasanoant.ettent1SIva + #vendasanoant.ettent2SIva) end) from #vendasanoant ))*100)*(-1)
				end, 0)
			else
				0
			end
			as mbpv
--	exec up_relatorio_vendas_rentabilidadeMensal_percent_ano 2016, 1, 12, '', 'A-Derma ', '', '', '', 'Loja 1'
		, case when (select sum(#vendasanoant.etiliquido) from #vendasanoant )<>0 then
				isnull(case when ((sum(etiliquido)-(select sum(#vendasanoant.etiliquido) from #vendasanoant ))/(select sum(#vendasanoant.etiliquido) from #vendasanoant ))*100 > 0
				then ((sum(etiliquido)-(select sum(#vendasanoant.etiliquido) from #vendasanoant ))/(select sum(#vendasanoant.etiliquido) from #vendasanoant ))*100
				else ((((select sum(#vendasanoant.etiliquido) from #vendasanoant )-sum(etiliquido))/(select sum(#vendasanoant.etiliquido) from #vendasanoant ))*100)*(-1)
				end,0)
			else
				0
			end
			as vdpvpcl
		, case when (select sum(#vendasanoant.ettent1+#vendasanoant.ettent2) from #vendasanoant )<>0 then
				isnull(case when ((sum(ettent1+ettent2)-(select sum(#vendasanoant.ettent1+#vendasanoant.ettent2) from #vendasanoant ))/(select sum(#vendasanoant.ettent1+#vendasanoant.ettent2) from #vendasanoant ))*100 > 0
				then ((sum(ettent1+ettent2)-(select sum(#vendasanoant.ettent1+#vendasanoant.ettent2) from #vendasanoant ))/(select sum(#vendasanoant.ettent1+#vendasanoant.ettent2) from #vendasanoant ))*100
				else ((((select sum(#vendasanoant.ettent1+#vendasanoant.ettent2) from #vendasanoant )-sum(ettent1+ettent2))/(select sum(#vendasanoant.ettent1+#vendasanoant.ettent2) from #vendasanoant ))*100)*(-1)
				end,0)
			else
				0
			end
			as vdpvpent	
--	exec up_relatorio_vendas_rentabilidadeMensal_percent_ano 2016, 1, 12, '', 'A-Derma ', '', '', '', 'Loja 1'
		, case when (select sum(#vendasanoant.etiliquido+#vendasanoant.ettent1+#vendasanoant.ettent2) from #vendasanoant )<>0 then
				isnull(case when ((sum(etiliquido+ettent1+ettent2)-(select sum(#vendasanoant.etiliquido+#vendasanoant.ettent1+#vendasanoant.ettent2) from #vendasanoant ))/(select sum(#vendasanoant.etiliquido+#vendasanoant.ettent1+#vendasanoant.ettent2) from #vendasanoant ))*100 > 0
				then ((sum(etiliquido+ettent1+ettent2)-(select sum(#vendasanoant.etiliquido+#vendasanoant.ettent1+#vendasanoant.ettent2) from #vendasanoant ))/(select sum(#vendasanoant.etiliquido+#vendasanoant.ettent1+#vendasanoant.ettent2) from #vendasanoant ))*100
				else ((((select sum(#vendasanoant.etiliquido+#vendasanoant.ettent1+#vendasanoant.ettent2) from #vendasanoant )-sum(etiliquido+ettent1+ettent2))/(select sum(#vendasanoant.etiliquido+#vendasanoant.ettent1+#vendasanoant.ettent2) from #vendasanoant ))*100)*(-1)
				end,0)
			else
				0
			end
			as vdpvp
		, case when CAST((select count(distinct (case when #vendasanoant.tipodoc=1 then #vendasanoant.ftstamp else NULL end)) from #vendasanoant )AS NUMERIC(10,2))<>0 then
				isnull(case when ((count(distinct (case when tipodoc=1 then ftstamp else NULL end))-(select count(distinct (case when #vendasanoant.tipodoc=1 then #vendasanoant.ftstamp else NULL end)) from #vendasanoant ))/CAST((select count(distinct (case when #vendasanoant.tipodoc=1 then #vendasanoant.ftstamp else NULL end)) from #vendasanoant )AS NUMERIC(10,2)))*100 > 0
				then ((count(distinct (case when tipodoc=1 then ftstamp else NULL end))-(select count(distinct (case when #vendasanoant.tipodoc=1 then #vendasanoant.ftstamp else NULL end)) from #vendasanoant ))/CAST((select count(distinct (case when #vendasanoant.tipodoc=1 then #vendasanoant.ftstamp else NULL end)) from #vendasanoant )AS NUMERIC(10,2)))*100
				else ((((select count(distinct (case when #vendasanoant.tipodoc=1 then #vendasanoant.ftstamp else NULL end)) from #vendasanoant )-count(distinct (case when tipodoc=1 then ftstamp else NULL end)))/CAST((select count(distinct (case when #vendasanoant.tipodoc=1 then #vendasanoant.ftstamp else NULL end)) from #vendasanoant )AS NUMERIC(10,2)))*100)*(-1)
				end,0)
			else
				cast (0.00 as numeric(10,2))
			end
			as nrvd
		, case when CAST((select count(distinct (case when #vendasanoant.tipodoc=3 then #vendasanoant.ftstamp else NULL end)) from #vendasanoant )AS NUMERIC(10,2))<>0 then 
				isnull(case when ((count(distinct (case when tipodoc=3 then ftstamp else NULL end))-(select count(distinct (case when #vendasanoant.tipodoc=3 then #vendasanoant.ftstamp else NULL end)) from #vendasanoant ))/CAST((select count(distinct (case when #vendasanoant.tipodoc=3 then #vendasanoant.ftstamp else NULL end)) from #vendasanoant )AS NUMERIC(10,2)))*100 > 0
				then ((count(distinct (case when tipodoc=3 then ftstamp else NULL end))-(select count(distinct (case when #vendasanoant.tipodoc=3 then #vendasanoant.ftstamp else NULL end)) from #vendasanoant ))/CAST((select count(distinct (case when #vendasanoant.tipodoc=3 then #vendasanoant.ftstamp else NULL end)) from #vendasanoant )AS NUMERIC(10,2)))*100
				else ((((select count(distinct (case when #vendasanoant.tipodoc=3 then #vendasanoant.ftstamp else NULL end)) from #vendasanoant )-count(distinct (case when tipodoc=3 then ftstamp else NULL end)))/CAST((select count(distinct (case when #vendasanoant.tipodoc=3 then #vendasanoant.ftstamp else NULL end)) from #vendasanoant ) AS numeric(10,2)))*100)*(-1)
				end,0)
			else
				cast (0.00 as numeric(10,2))
			end
			as nrdev
		, case when cast((select count(distinct (#vendasanoant.nrAtend)) from #vendasanoant ) as numeric(10,2))>0 then 
				isnull(case when ((count(distinct (nrAtend))-(select count(distinct (#vendasanoant.nrAtend)) from #vendasanoant ))/cast((select count(distinct (#vendasanoant.nrAtend)) from #vendasanoant ) as numeric(10,2)))*100 > 0
				then ((count(distinct (nrAtend))-(select count(distinct (#vendasanoant.nrAtend)) from #vendasanoant ))/cast((select count(distinct (#vendasanoant.nrAtend)) from #vendasanoant ) as numeric(10,2)))*100
				else ((((select count(distinct (#vendasanoant.nrAtend)) from #vendasanoant )-count(distinct (nrAtend)))/cast((select count(distinct (#vendasanoant.nrAtend)) from #vendasanoant ) as  numeric(10,2)))*100)*(-1)
				end,0)
			else
				cast (0.00 as numeric(10,2))
			end
			as nrat
		,case when cast((select count(distinct (#vendasanoant.fdata)) from #vendasanoant ) as numeric(10,2))>0 then
				isnull(case when ((count(distinct (fdata))-(select count(distinct (#vendasanoant.fdata)) from #vendasanoant ))/cast((select count(distinct (#vendasanoant.fdata)) from #vendasanoant ) as numeric(10,2)))*100 > 0
				then ((count(distinct (fdata))-(select count(distinct (#vendasanoant.fdata)) from #vendasanoant ))/cast((select count(distinct (#vendasanoant.fdata)) from #vendasanoant ) as numeric(10,2)))*100
				else ((((select count(distinct (#vendasanoant.fdata)) from #vendasanoant )-count(distinct (fdata)))/cast((select count(distinct (#vendasanoant.fdata)) from #vendasanoant ) as numeric(10,2)))*100)*(-1)
				end,0)
			else
				cast (0.00 as numeric(10,2))
			end
			as nrda
		,case when (select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva-#vendasanoant.epcpond)/count(distinct (#vendasanoant.vendnm))) from #vendasanoant )<>0 then
				isnull(case when (((sum(etiliquidoSIva+ettent1SIva+ettent2SIva-epcpond)/count(distinct (vendnm)))-(select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva-#vendasanoant.epcpond)/count(distinct (#vendasanoant.vendnm))) from #vendasanoant ))/(select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva-#vendasanoant.epcpond)/count(distinct (#vendasanoant.vendnm))) from #vendasanoant ))*100 > 0
				then (((sum(etiliquidoSIva+ettent1SIva+ettent2SIva-epcpond)/count(distinct (vendnm)))-(select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva-#vendasanoant.epcpond)/count(distinct (#vendasanoant.vendnm))) from #vendasanoant ))/(select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva-#vendasanoant.epcpond)/count(distinct (#vendasanoant.vendnm))) from #vendasanoant ))*100
				else ((((select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva-#vendasanoant.epcpond)/count(distinct (#vendasanoant.vendnm))) from #vendasanoant )-(sum(etiliquidoSIva+ettent1SIva+ettent2SIva-epcpond))/count(distinct (vendnm)))/(select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva-#vendasanoant.epcpond)/count(distinct (#vendasanoant.vendnm))) from #vendasanoant ))*100)*(-1)
				end,0)
			else
				cast (0.00 as numeric(10,2))
			end
			as bbop
		, case when (select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva)/count(distinct (#vendasanoant.vendnm))) from #vendasanoant ) <>0 then
				isnull(case when (((sum(etiliquidoSIva+ettent1SIva+ettent2SIva)/count(distinct (vendnm)))-(select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva)/count(distinct (#vendasanoant.vendnm))) from #vendasanoant ))/(select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva)/count(distinct (#vendasanoant.vendnm))) from #vendasanoant ))*100 > 0
				then (((sum(etiliquidoSIva+ettent1SIva+ettent2SIva)/count(distinct (vendnm)))-(select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva)/count(distinct (#vendasanoant.vendnm))) from #vendasanoant ))/(select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva)/count(distinct (#vendasanoant.vendnm))) from #vendasanoant ))*100
				else ((((select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva)/count(distinct (#vendasanoant.vendnm))) from #vendasanoant )-(sum(etiliquidoSIva+ettent1SIva+ettent2SIva))/count(distinct (vendnm)))/(select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva)/count(distinct (#vendasanoant.vendnm))) from #vendasanoant ))*100)*(-1)
				end,0)
			else
				cast (0.00 as numeric(10,2))
			end
			as vdpvop
		, case when (select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva)/count(distinct (case when #vendasanoant.tipodoc=1 then #vendasanoant.ftstamp else NULL end))) from #vendasanoant ) <>0 then 
				isnull(case when (((sum(etiliquidoSIva+ettent1SIva+ettent2SIva)/count(distinct (case when tipodoc=1 then ftstamp else NULL end)))-(select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva)/count(distinct (case when #vendasanoant.tipodoc=1 then #vendasanoant.ftstamp else NULL end))) from #vendasanoant ))/(select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva)/count(distinct (case when #vendasanoant.tipodoc=1 then #vendasanoant.ftstamp else NULL end))) from #vendasanoant ))*100 > 0
				then (((sum(etiliquidoSIva+ettent1SIva+ettent2SIva)/count(distinct (case when tipodoc=1 then ftstamp else NULL end)))-(select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva)/count(distinct (case when #vendasanoant.tipodoc=1 then #vendasanoant.ftstamp else NULL end))) from #vendasanoant ))/(select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva)/count(distinct (case when #vendasanoant.tipodoc=1 then #vendasanoant.ftstamp else NULL end))) from #vendasanoant ))*100
				else ((((select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva)/count(distinct (case when #vendasanoant.tipodoc=1 then #vendasanoant.ftstamp else NULL end))) from #vendasanoant )-(sum(etiliquidoSIva+ettent1SIva+ettent2SIva))/count(distinct (case when tipodoc=1 then ftstamp else NULL end)))/(select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva)/count(distinct (case when #vendasanoant.tipodoc=1 then #vendasanoant.ftstamp else NULL end))) from #vendasanoant ))*100)*(-1)
				end,0)
			else
				cast (0.00 as numeric(10,2))
			end
			as vdmdpv
		, case when (select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva)/count(distinct (#vendasanoant.nrAtend))) from #vendasanoant )<>0 then
				isnull(case when (((sum(etiliquidoSIva+ettent1SIva+ettent2SIva)/count(distinct (nrAtend)))-(select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva)/count(distinct (#vendasanoant.nrAtend))) from #vendasanoant ))/(select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva)/count(distinct (#vendasanoant.nrAtend))) from #vendasanoant ))*100 > 0
				then (((sum(etiliquidoSIva+ettent1SIva+ettent2SIva)/count(distinct (nrAtend)))-(select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva)/count(distinct (#vendasanoant.nrAtend))) from #vendasanoant ))/(select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva)/count(distinct (#vendasanoant.nrAtend))) from #vendasanoant ))*100
				else ((((select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva)/count(distinct (#vendasanoant.nrAtend))) from #vendasanoant )-(sum(etiliquidoSIva+ettent1SIva+ettent2SIva))/count(distinct (nrAtend)))/(select (sum(#vendasanoant.etiliquidoSIva+#vendasanoant.ettent1SIva+#vendasanoant.ettent2SIva)/count(distinct (#vendasanoant.nrAtend))) from #vendasanoant ))*100)*(-1)
				end,0)
			else
				cast (0.00 as numeric(10,2))
			end
			as atmdpv
	From
		#vendasano
	--exec up_relatorio_vendas_rentabilidadeMensal_percent_ano 2017, 1, 12, '', '', '', '', '', 'Loja 1'
	/* Elimina Tabelas */
	If OBJECT_ID('tempdb.dbo.#dadosVendasBase') IS NOT NULL
		DROP TABLE #dadosVendasBase
	If OBJECT_ID('tempdb.dbo.#dadosVendasBaseant') IS NOT NULL
		DROP TABLE #dadosVendasBaseant
	If OBJECT_ID('tempdb.dbo.#dadosFamilia') IS NOT NULL
		DROP TABLE #dadosFamilia
	If OBJECT_ID('tempdb.dbo.#dadosOperadores') IS NOT NULL
		DROP TABLE #dadosOperadores
	If OBJECT_ID('tempdb.dbo.#dadosRefs') IS NOT NULL
		DROP TABLE #dadosRefs
	If OBJECT_ID('tempdb.dbo.#dadosSt') IS NOT NULL
		DROP TABLE #dadosSt
	If OBJECT_ID('tempdb.dbo.#EmpresaArm') IS NOT NULL
		DROP TABLE #EmpresaArm
	If OBJECT_ID('tempdb.dbo.#vendasano') IS NOT NULL
		DROP TABLE #vendasano
	If OBJECT_ID('tempdb.dbo.#vendasanoant') IS NOT NULL
		DROP TABLE #vendasanoant

GO
Grant Execute on dbo.up_relatorio_vendas_rentabilidadeMensal_percent_ano to Public
Grant control on dbo.up_relatorio_vendas_rentabilidadeMensal_percent_ano to Public
GO