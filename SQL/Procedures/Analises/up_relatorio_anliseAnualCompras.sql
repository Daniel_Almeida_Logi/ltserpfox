/* relatório evolução anual de compras
	
	exec up_relatorio_anliseAnualCompras 2016,'28','Loja 1'
	select * from cm1 where debito = 0
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_anliseAnualCompras]') IS NOT NULL
	drop procedure dbo.up_relatorio_anliseAnualCompras
go

create procedure dbo.up_relatorio_anliseAnualCompras
@ano			as numeric(4,0)
,@fornecedor	as varchar(80)
,@site			as varchar(55)
/* with encryption */
AS 
BEGIN
	select 
		fornecedor = RTRIM(fo.nome)  + '[' + LTRIM(STR(fo.no)) + '][' +  LTRIM(STR(fo.estab))  +']'
		,month(fo.data) as mes
		,sum(case when cm1.debito=0 then fo.ivain else -fo.ivain end) as valor
		,sum(case when cm1.debito=0 then fo.eivain else -fo.eivain end) as evalor 
	from 
		fo (nolock) 
		--left join fo2 (nolock) on fo2.fo2stamp=fo.fostamp /*and fo2.anulado=0 */
		left join cm1 (nolock) on cm1.cm=fo.doccode  
	where 
		year(fo.data) = @ano 
		AND fo.site = (case when @site = '' Then fo.site else @site end)
		AND fo.[no] = CASE WHEN @Fornecedor = 0 THEN fo.no ELSE @Fornecedor END
	group by 
		fo.nome
		,fo.no
		,fo.estab
		,month(fo.data) 
	order by 
		fo.nome
		,fo.no
		,fo.estab
		,month(fo.data) 
END
GO
Grant Execute on dbo.up_relatorio_anliseAnualCompras to Public
Grant control on dbo.up_relatorio_anliseAnualCompras to Public
GO