-- Relatório Margem Cartão Cliente
-- exec up_relatorio_vendas_MrgCartaoCliente '20110101','20120101',''
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_vendas_MrgCartaoCliente]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_MrgCartaoCliente
go

create procedure dbo.up_relatorio_vendas_MrgCartaoCliente
@dataIni as datetime,
@dataFim as datetime,
@site varchar(55)
/* WITH ENCRYPTION */ 
AS
BEGIN
;with 
	cte1 (clstamp) as (
		SELECT	clstamp 
		FROM	B_fidel (nolock)
	)
Select	
	tipo
	,data = (convert(varchar(254),ano) + '-' + CASE WHEN len(convert(varchar(254),mes)) != 2 THEN '0'+convert(varchar(254),mes) ELSE convert(varchar(254),mes) END)
	,nome
	,no
	,estab
	,cartao = CASE WHEN n.utstamp IN (select clstamp from cte1) THEN 'SIM' ELSE 'NÃO' END
	,venda	= SUM(etiliquido+u_ettent1+u_ettent2)
	,epcpond = SUM(EPCPOND)
	,margem = SUM(etiliquido+u_ettent1+u_ettent2)-SUM(EPCPOND)
	,margem_perc = ROUND((CASE WHEN (SUM(EPCPOND)) > 0	THEN ((SUM(etiliquido+u_ettent1+u_ettent2) / SUM(EPCPOND))-1) * 100	ELSE 0 END),2)
	,nrAtend = (SELECT	
					nrvendas = COUNT(distinct ft.u_nratend)
				FROM	
					ft (nolock)
					inner join td (nolock) on td.ndoc = ft.ndoc 
				where	
					ft.fdata between @dataIni and @dataFim 
					AND U_TIPODOC not in (3,1,5) 
					AND YEAR(ft.fdata) = N.ano
					AND MONTH(ft.fdata) = N.mes
					AND ft.no = N.NO
					AND ft.estab = N.estab
	)
	,qtt = SUM(qtt)
FROM(		
	Select	
		b_utentes.utstamp
		,b_utentes.tipo
		,ano = YEAR(fdata)
		,mes = MONTH(fdata) 
		,b_utentes.nome
		,b_utentes.no
		,b_utentes.estab
		,etiliquido	= CASE WHEN Fi.IVAINCL = 0
						THEN FI.ETILIQUIDO
						ELSE FI.ETILIQUIDO / ((Fi.iva / 100)+1)
					END
		,u_ettent1 = CASE WHEN Ft.tipodoc = 3 THEN
						CASE WHEN Fi.IVAINCL = 0
							THEN u_ettent1 * -1
							ELSE (u_ettent1 / ((Fi.iva / 100)+1)) * (-1)
						END
					ELSE
						CASE WHEN Fi.IVAINCL = 0
							THEN u_ettent1 
							ELSE (u_ettent1 / ((Fi.iva / 100)+1))
						END
					END
		,u_ettent2 = CASE WHEN Ft.tipodoc = 3 THEN
						CASE WHEN Fi.IVAINCL = 0
							THEN u_ettent2 * -1
							ELSE (u_ettent2 / ((Fi.iva / 100)+1)) * (-1)
						END
					ELSE
						CASE WHEN Fi.IVAINCL = 0
							THEN u_ettent2 
							ELSE (u_ettent2 / ((Fi.iva / 100)+1))
						END
					END
		,epcpond = CASE WHEN FT.tipodoc = 3 THEN /* DEV. A CLIENTE */
							(CASE WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt ELSE FI.EPCP * qtt END)*-1
						ELSE
							(CASE WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt ELSE FI.EPCP * qtt END) 
					END
		,fi.qtt	
	From	
		b_utentes (nolock)
		INNER	JOIN FT (nolock) ON FT.NO = b_utentes.no and ft.estab = b_utentes.estab
		INNER	JOIN FI (nolock) ON ft.ftstamp = fi.ftstamp
	Where	
		ft.fdata between @dataIni and @dataFim
		and ft.no > 199
		and ft.site = (case when @site = '' Then ft.site else @site end)
) N
Group By N.utstamp,N.tipo,N.ano,N.mes,N.nome,N.no,N.estab
Order By tipo, ano, mes, nome 
END


GO
Grant Execute on dbo.up_relatorio_vendas_MrgCartaoCliente to Public
Grant control on dbo.up_relatorio_vendas_MrgCartaoCliente to Public
Go
