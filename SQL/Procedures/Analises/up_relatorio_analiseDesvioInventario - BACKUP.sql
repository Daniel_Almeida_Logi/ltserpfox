/**  Relat�rio Desvio Invent�rio - Pressuposto deve ser analisar o invent�rio seleccionar para verificar diferen�as antes / ap�s contagem
	
	exec up_relatorio_analiseDesvioInventario '20161128', '21:14:00','20161128','21:15:05','ADM99020F4E-4C12-4A0B-AA7', 1, 1, 1, 'Loja 1'

	select * from stic where data='2016-11-28 00:00:00.000'
	select * from stil where sticstamp = 'ADM99020F4E-4C12-4A0B-AA7'
	select * from sl where sticstamp='VF9C8027BF-3728-4B25-953'
	
**/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_analiseDesvioInventario]') IS NOT NULL
	drop procedure dbo.up_relatorio_analiseDesvioInventario
go

create procedure dbo.up_relatorio_analiseDesvioInventario

@dataIni		as datetime,
@horaIni		as varchar(8) = '00:00:01',
@dataFim		as datetime,
@horaFim		as varchar(8) = '23:59:59',
@inventario1	as varchar(35),
@pcl			as bit,
@pcp			as bit,
@pct			as bit,
@site			as varchar(60)

/* WITH ENCRYPTION */

AS
BEGIN
	SET ANSI_WARNINGS OFF
	declare @stic1stamp as varchar(254)
	set @stic1stamp =  @inventario1
		
	declare @site_nr as tinyint 
	set @site_nr = isnull((Select top 1 no from empresa (nolock) where empresa.site = @site),0)
	
	If OBJECT_ID('tempdb.dbo.#Armazens') IS NOT NULL
		drop table #Armazens 
		
	select
		distinct armazem
	into
		#Armazens
	From
		empresa
		inner join empresa_arm on empresa.no = empresa_arm.empresa_no
	Where
		empresa.site = @site
	
	/* considera os inventarios escolhidos */
	
		;with 
		cte1 (ref) as (
			Select	distinct ref
			from	stil (nolock)
			Where	sticstamp in (@stic1stamp)
		)


		Select	a.ref,a.design,
				a.STOCKADATA as stockAntes, b.STOCKADATA as stockDepois, abs(a.STOCKADATA - b.STOCKADATA) as DevioAbsQtt,
				EPCPONDANTES, EPCPONDDEPOIS,  abs(EPCPONDDEPOIS- EPCPONDANTES) as DevioAbsEPCPOND,
				PCTANTES, PCTDEPOIS, abs(PCTANTES- PCTDEPOIS) as DevioAbsPCT,
				PCLANTES, PCLDEPOIS, abs(PCLANTES- PCLDEPOIS) as DevioAbsPCL,
				a.Zona, 
				qttInventariada, 
				taxaiva, pvp
		From
		(
		SELECT	ST.REF,
				DESIGN,
				STOCKADATA		= ISNULL((		
									SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END )
									FROM	sl (nolock)
									WHERE	ref = ST.REF AND datalc+usrhora <= @dataIni+ @horaIni
											AND armazem in (select armazem from #Armazens)
									),0),
				EPCPONDANTES	= case when (select top 1 round(epcpond,2) from sl (nolock)
											where ref=st.ref and datalc+usrhora <= @dataIni + @horaIni
													AND armazem in (select armazem from #Armazens)
													 order by datalc+usrhora desc)>0
										then 
											(select top 1 round(epcpond,2) from sl (nolock)
											where ref=st.ref and  datalc+usrhora <= @dataIni + @horaIni
											AND armazem in (select armazem from #Armazens)
											 order by datalc+usrhora desc)
											*
											ISNULL((		
											SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END )
											FROM	sl (nolock)
											WHERE	ref = ST.REF AND datalc+usrhora <= @dataIni+ @horaIni
													AND armazem in (select armazem from #Armazens)
											),0)
										when (select top 1 round(epcpond,2) from sl (nolock)
											where ref=st.ref and  datalc+usrhora <= @dataIni+ @horaIni
													AND armazem in (select armazem from #Armazens)
											 order by datalc+usrhora desc)<0
										then
											(select top 1 round(evu,2) from sl (nolock)
											where ref=st.ref and  datalc+usrhora <= @dataIni+ @horaIni
													AND armazem in (select armazem from #Armazens)
											 order by datalc+usrhora desc)
											*
											ISNULL((		
											SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END )
											FROM	sl (nolock)
											WHERE	ref = ST.REF AND datalc+usrhora <= @dataIni+ @horaIni
													AND armazem in (select armazem from #Armazens)
											),0)
										else
											st.epcusto *
											ISNULL((
											SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END )
											FROM	sl (nolock)
											WHERE	ref = ST.REF AND datalc+usrhora <= @dataIni+ @horaIni
													AND armazem in (select armazem from #Armazens)
											),0)
										end,
				PCTANTES		=	
									/* No CASO DE SER NULO CONSIDERA INVENTARIO */
									ISNULL((SELECT	TOP 1 epv 
									FROM	FN (nolock) 
									Inner join cm1 (nolock) on cm1.cmdesc = fn.docnome
									WHERE	FN.REF = ST.REF
											AND epv > 0
											AND data + fn.ousrhora <= @dataIni+ @horaIni
											/*
											 Documentos para Actualizar PCT: 
													 N/Guia Transporte
													 V/Guia Transporte
													 N/Guia Entrada
													 V/Factura
											 E mexer em Stock
											*/
											AND fn.docnome IN ('N/Guia Entrada','V/Factura','V/Guia Transp.')
											and FOLANSL = 1
											AND armazem in (select armazem from #Armazens)
									ORDER BY data + fn.ousrhora DESC
									),0)
									*
									ISNULL((		
										SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END )
										FROM	sl (nolock)
										WHERE	ref = ST.REF AND datalc+usrhora <= @dataIni+ @horaIni
												AND armazem in (select armazem from #Armazens)
									),0)
									,
				PCLANTES		=	ISNULL(
										(SELECT	TOP 1 U_UPC 
										FROM	SL (nolock) 
										inner join fn (nolock) on sl.fnstamp = fn.fnstamp
										WHERE	(ORIGEM = 'FO' OR ORIGEM = 'IF')
												AND sl.datalc + sl.ousrhora <= @dataIni+ @horaIni
												AND SL.REF = ST.REF
												AND U_UPC > 0
												AND (cmdesc = 'V/Factura' or cmdesc ='V/Guia Transp.' Or cmdesc ='N/G.Entrada' or ORIGEM = 'IF') 
												AND sl.armazem in (select armazem from #Armazens)
										ORDER BY sl.datalc + sl.ousrhora DESC 
									),0)
									*
									ISNULL((		
										SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END )
										FROM	sl (nolock)
										WHERE	ref = ST.REF AND datalc+usrhora <= @dataIni+ @horaIni
												AND armazem in (select armazem from #Armazens)
									),0)
									,
				Zona			= ISNULL(
									(Select top 1 zona From stil c (nolock)
									Where (sticstamp = @stic1stamp) and c.ref = ST.ref
									order by usrdata)
								,''),
				qttinventariada	= ISNULL(
									(Select sum(stock) From stil c (nolock)
									Where (sticstamp = @stic1stamp) and c.ref = ST.ref
									group by c.ref)
								,0),								
				taxaiva			= (select taxa from taxasiva (nolock) where codigo=st.tabiva),
				pvp				= st.epv1
		FROM	ST (nolock)
		WHERE	
			ST.stns = 0
			AND ST.ref IN (Select ref from cte1)
			and st.site_nr = @site_nr
		) a
		LEFT JOIN 
		(
		SELECT	ST.REF,
				DESIGN,
				STOCKADATA		= ISNULL((		
									SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END )
									FROM	sl (nolock)
									WHERE	ref = ST.REF AND datalc+usrhora <= @dataFim+ @horaFim
											AND armazem in (select armazem from #Armazens)
									),0),
				epcponddepois	= case when (select top 1 round(epcpond,2) from sl (nolock)
											where ref=st.ref and  datalc+usrhora <= @dataFim+ @horaFim
													AND armazem in (select armazem from #Armazens)
													order by datalc+usrhora desc)>0
										then
											(select top 1 round(epcpond,2) from sl (nolock)
											where ref=st.ref and  datalc+usrhora <= @dataFim+ @horaFim
													AND armazem in (select armazem from #Armazens)
													 order by datalc+usrhora desc)
											* 
											ISNULL((		
											SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END )
											FROM	sl (nolock)
											WHERE	ref = ST.REF AND datalc+usrhora <= @dataFim+ @horaFim
													AND armazem in (select armazem from #Armazens)
											),0)
										when (select top 1 round(epcpond,2) from sl (nolock)
											where ref=st.ref and  datalc+usrhora <= @dataFim+ @horaFim
													AND armazem in (select armazem from #Armazens)
													order by datalc+usrhora desc)<0
										then
											(select top 1 round(evu,2) from sl (nolock)
											where ref=st.ref and  datalc+usrhora <= @dataFim+ @horaFim
													AND armazem in (select armazem from #Armazens)
													order by datalc+usrhora desc)
											* 
											ISNULL((		
											SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END )
											FROM	sl (nolock)
											WHERE	ref = ST.REF AND datalc+usrhora <= @dataFim+ @horaFim
													AND armazem in (select armazem from #Armazens)
											),0)
										else
											st.epcusto *
											ISNULL((		
											SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END )
											FROM	sl (nolock)
											WHERE	ref = ST.REF AND datalc+usrhora <= @dataFim+ @horaFim
													AND armazem in (select armazem from #Armazens)
											),0)
										end,
				PCTDEPOIS		=	ISNULL((SELECT	TOP 1 epv 
									FROM	FN (nolock) 
									Inner join cm1 (nolock) on cm1.cmdesc = fn.docnome
									WHERE	FN.REF = ST.REF
											AND epv > 0
											AND fn.data + fn.ousrhora <= @dataFim+ @horaFim
											/*
											 Documentos para Actualizar PCT: 
													 N/Guia Transporte
													 V/Guia Transporte
													 N/Guia Entrada
													 V/Factura
											 E mexer em Stock
											*/
											AND fn.docnome IN ('N/Guia Entrada','V/Factura','V/Guia Transp.')
											and FOLANSL = 1
											AND armazem in (select armazem from #Armazens)
									ORDER BY fn.data + fn.ousrhora DESC
									),0)
									*
									ISNULL((		
										SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END )
										FROM	sl (nolock)
										WHERE	ref = ST.REF AND datalc+usrhora <= @dataFim+ @horaFim
												AND armazem in (select armazem from #Armazens)
									),0),
				PCLDEPOIS		=	ISNULL(
										(SELECT	TOP 1 U_UPC 
										FROM	SL (nolock) 
										inner join fn (nolock) on fn.fnstamp = sl.fnstamp
										WHERE	(ORIGEM = 'FO' OR ORIGEM = 'IF')
												AND sl.datalc + sl.ousrhora <= @dataFim+ @horaFim
												AND SL.REF = ST.REF
												AND U_UPC > 0
												AND (cmdesc = 'V/Factura' or cmdesc ='V/Guia Transp.' Or cmdesc ='N/G.Entrada' or ORIGEM = 'IF') 
												AND sl.armazem in (select armazem from #Armazens)									
										ORDER BY sl.datalc + sl.ousrhora DESC 
									),0)
									*
									ISNULL((		
										SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END )
										FROM	sl (nolock)
										WHERE	ref = ST.REF AND datalc+usrhora <= @dataFim+ @horaFim
												AND armazem in (select armazem from #Armazens)
									),0)								
									,
				Zona			= (Select top 1 zona From stil c (nolock)
									Where (sticstamp = @stic1stamp) and c.ref = ST.ref
									order by usrdata)
		FROM	
			ST (nolock)
		WHERE	
			ST.stns = 0
			and st.site_nr = @site_nr
			AND ST.ref IN (Select ref from cte1)
		) b
		ON a.ref = b.ref
		Order by DevioAbsQtt Desc
		SET ANSI_WARNINGS ON	
END

GO
Grant Execute on dbo.up_relatorio_analiseDesvioInventario to Public
Grant control on dbo.up_relatorio_analiseDesvioInventario to Public
Go