/*  Relatório de ocorrencias	

	exec up_relatorio_gestao_ocorrencias '19990701','20241225','Lotes','', 'Loja 1'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_ocorrencias]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_ocorrencias
go

Create PROCEDURE dbo.up_relatorio_gestao_ocorrencias
@dataIni	datetime,
@dataFim	datetime,
@tipo		as varchar(254),
@grau		as varchar(254),
@site		as varchar(60)
/* with encryption */
AS 
BEGIN
SET NOCOUNT ON
	Select	
		[Stamp]
      ,[linkStamp]
      ,[Tipo]
      ,[Grau]
      ,[descr] = CASE 
                    WHEN tipo = 'Lotes' 
                    THEN descr + ' NR: ' + ISNULL((
                       SELECT TOP 1 ft2.u_receita 
                    FROM ft2 
                    LEFT JOIN B_ocorrencias b ON ft2.ft2stamp = b.linkStamp
                    WHERE b.Stamp = a.Stamp
                    ), '')
                    ELSE descr 
                  END
      ,[oValor]
      ,[dValor]
      ,[usr]
      ,[date]
      ,[terminal]
      ,[site]
      ,[nValor]
      ,[linkStampDest]
      ,[nrcartao_ant]
      ,[nrcartao_novo]
      ,[tiporeg]
      ,[tiporegdesc] ,(select username From b_us (nolock) Where userno = a.usr) username
	From	
		B_ocorrencias a
	Where	
		convert(varchar,a.date,112) between @dataIni and @dataFim
		And Tipo = Case when @Tipo = '' Then Tipo else LTRIM(RTRIM(@Tipo)) end
		And Grau = Case when @Grau = '' Then Grau else @Grau end
		and (site = case when @site = '' then site else @site end or site = '')
	order by
		date desc
end

GO
Grant Execute On dbo.up_relatorio_gestao_ocorrencias to Public
Grant control On dbo.up_relatorio_gestao_ocorrencias to Public
GO