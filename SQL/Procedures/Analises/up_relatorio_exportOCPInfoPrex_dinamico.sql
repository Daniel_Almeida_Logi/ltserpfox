/* 
	
1. CPR - C�digo do produto
2. NOM - Nome do produto
3. FAP - Forma de apresenta��o do produto
4. LOC - Loja em que est� a consultar o produto
5. SAC - Stock actual
6. STM - Stock m�ximo
7. SMI - Stock minimo
8. QTE - Quantidade encomendada
9. DUV - Data da ultima venda
10. DUC - Data da ultima compra
11. PVP - Pre�o de venda ao publico
12. PCU - Pre�o de custo
13. IVA - Taxa de IVA aplic�vel
14. CAT - Familia do produto
15. CT1 - Sub-familia do produto
16. GEN - Gen�rico ("S" � gen�rico, "N" n�o � gen�rico)
17. GPR - Grossista prefer�ncial (c�digo do grossista)
18. CLA - C�digo do laborat�rio
19. TPR - Tipo de receita aplic�vel
20. CAR - C�digo da arruma��o (prateleira)
21. GAM - Gama do produto
22. V(nn) - Vendas do "nn" m�s anterior � data corrente (at� 24 colunas)
23. DTVAL - Prazo de validade
24. FPD - Fornecedor Prefer�ncial Designa��o
25. LAD - Laborat�rio AIM Designa��o
26. PRATELEIRA -Nome da Prateleira
27. GAMA - Nome da Gama
28. GRUPOHOMOGENEO - Grupo Homog�neo
29. INACTIVO - Estado do Produto na Farm�cia
30. PVP5 - Pre�o Top5
31. CNPEM - c�digo CNPEM

	exec up_relatorio_exportOCPInfoPrex_dinamico '20190101','20190131', 'Loja 1', 0 
	exec up_relatorio_exportOCPInfoPrex_dinamico '20230712', 'Loja 1'

	exec up_relatorio_exportOCPInfoPrex_dinamico '', 'Loja 1'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_exportOCPInfoPrex_dinamico]') IS NOT NULL
	drop procedure dbo.up_relatorio_exportOCPInfoPrex_dinamico
go

create procedure dbo.up_relatorio_exportOCPInfoPrex_dinamico
	 @dataFim						as datetime
	 ,@site							as varchar(55)
	
/* with encryption */
AS
BEGIN

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase

	DECLARE @dataIni DATETIME 
	DECLARE @fornecs varchar(250) 

	DECLARE @fornecsActiv varchar(250) 

	select top 1 @fornecs= RTRIM(LTRIM(textValue)) FROM B_Parameters_site  WHERE stamp='ADM0000000207'  and site=@site


	SET @dataIni = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dataFim)-1, 0)
	set @dataFim = @dataFim + CAST('23:59:59:997' AS datetime)

	print  @dataIni
	print  @dataFim


	/* Dados Base Vendas  */
	create table #dadosVendasBase (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,2)
        ,epcpond numeric(19,2)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,2)
        ,etiliquidoSiva numeric(19,2)
        ,ettent1 numeric(13,2)
        ,ettent2 numeric(13,2)
        ,ettent1siva numeric(13,2)
        ,ettent2siva numeric(13,2)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,2)
        ,descvale numeric(19,2)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)
	)
    
    insert #dadosVendasBase
    exec up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site, 0

	/* Define se vais mostrar resultados abaixo do Cliente 199 */
	declare @Entidades as bit
	set @Entidades = case when (select tipoempresa from empresa where site = @site) in ('FARMACIA', 'PARAFARMACIA') then convert(bit,1) else convert(bit,0) end

	If @Entidades = 1
	delete from #dadosVendasBase where #dadosVendasBase.no between 1 and 198

	
	declare @sql varchar(max)
	declare @sqlWhere varchar(max)=''

	select @sql = N'
	
	select
		CPR =''CPR''
		,NOM = ''NOM''
		,SAC = ''SAC''
		,LOC = ''LOC''
		,V01 = ''V01''
		,V0 = ''V0''
		
		union all
		
	select
		 CPR = st.ref
		,NOM = convert(varchar(100),REPLACE(REPLACE(st.design,''"'',''''), char(9), '' ''))
		,SAC = LEFT(REPLACE(CAST(convert(int,st.stock) AS VARCHAR(10)), char(9), '' ''),10)
		,LOC = LEFT(REPLACE(CAST(ISNULL(empresa.nomecomp,'''') AS VARCHAR(256)), char(9), '' ''),254)
		
		,V01 = CAST(isnull(convert(int,(select SUM(qtt) from #dadosVendasBase where #dadosVendasBase.ref = st.ref and fdata between DATEADD(MONTH, DATEDIFF(MONTH, 0, '''+ convert(varchar,@dataFim) +''')-1, 0) and  DATEADD(MONTH, DATEDIFF(MONTH, -1, '''+  convert(varchar,@dataFim) +''')-1, -1) + CAST(''23:59:59:997'' AS datetime))),0) AS VARCHAR(10))
		,V0  = CAST(isnull(convert(int,(select SUM(qtt) from #dadosVendasBase where #dadosVendasBase.ref = st.ref and fdata between DATEADD(MONTH, DATEDIFF(MONTH, 0,'''+ convert(varchar,@dataFim) +'''), 0) and '''+ convert(varchar,@dataFim) +''')),0) AS VARCHAR(10))
		'

	set @sqlWhere = '
	from 
		st (nolock)
		inner join empresa (nolock) on empresa.no = st.site_nr
		left join taxasiva (nolock) on st.tabiva = taxasiva.codigo
		left join fprod (nolock) on st.ref = fprod.cnp
	Where
		st.stns = 0 
		and inactivo=0
		and(isnull( convert(varchar(10),st.fornec), '''') in (select isnull( convert(varchar(10),no),'''') from fl where convert(varchar(10),no) in ( (Select items from dbo.up_splitToTable('''+@fornecs+''' ,'';'')))) or '''+@fornecs+''' = '''')
		'

		If (@site !='')
		BEGIN
			set @sqlWhere += ' and empresa.site = ''' + @site +''''
		END	
	

	print @sql + @sqlWhere -- + @sql1
	execute (@sql  + @sqlWhere/* +@sql1*/ )		

	--select * from #dadosVendasBase

	/* Elimina Tabelas */ 
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase

	END

GO
Grant Execute on dbo.up_relatorio_exportOCPInfoPrex_dinamico to Public
Grant control on dbo.up_relatorio_exportOCPInfoPrex_dinamico to Public
GO