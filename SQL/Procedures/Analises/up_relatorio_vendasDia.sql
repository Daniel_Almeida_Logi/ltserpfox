/* Relatório Vendas Dia
	
	 exec up_relatorio_vendasDia '20150301', '20150331', '', '', '', '', '', '', 'Loja 1', ''
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_vendasDia]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendasDia
go

create procedure dbo.up_relatorio_vendasDia
@dataIni		as  datetime,
@dataFim		as  datetime,
@op				as  varchar(max),
@hora			as  varchar(2),
@ref			as	varchar(100),
@lab			as	varchar(100),
@marca			as  varchar(200),
@familia		as varchar(max),
@site			as varchar(55),
@dci			as varchar(254)
/* WITH ENCRYPTION */
AS
BEGIN
SET LANGUAGE PORTUGUESE
declare @tempcteFamilia table (ref varchar(18))
insert into @tempcteFamilia 
Select distinct ref From stfami (nolock) Where ref in (Select items from dbo.up_splitToTable(@familia,',')) Or @familia = '0' or @familia = ''
declare @tempcteOperadores table (iniciais varchar(3), cm numeric(5,0),  username varchar(30))
insert into @tempcteOperadores 
Select iniciais, cm = userno,username	from b_us (nolock) Where userno in (select items from dbo.up_splitToTable(@op,',')) or @op= '' or @op= '0'
;With 
	cteFprod as (
		Select 
			cnp, dispdescr, medid, generico,
			psico, benzo, protocolo, dci
		from 
			fprod (nolock)  
	)
SELECT	'DATA'					= a.fdata, 
		'UTENTE'				= SUM(ETILIQUIDO), 
		'COMPARTICIPACAO'		= SUM(ettent1) + SUM(ettent2),
		'DIANOME'				= UPPER(DateName(dw,a.fdata)),
		'TOTAL'					= SUM(ETILIQUIDO) + SUM(ettent1) + SUM(ettent2)
		,nrAtend = (
			SELECT	
				nrvendas = COUNT(distinct ft.u_nratend)
			FROM	
				ft (nolock)
				inner join td (nolock) on td.ndoc = ft.ndoc 
			where	
				ft.fdata=a.fdata 
				AND U_TIPODOC not in (3,1,5,7,6)
				AND ft.no>199
				AND (Select Top 1 LEFT(b.ousrhora,2) From Ft b (nolock) WHERE b.u_nratend = ft.u_nratend and b.ftano=ft.ftano) = LEFT(a.ousrhora,2)
				AND (Select Top 1 b.ousrinis From Ft b (nolock) WHERE b.u_nratend = ft.u_nratend and b.ftano=ft.ftano) = a.ousrinis
		)
		,nrVendas = (
			SELECT	
				SUM(case when tipodoc=1 /*and ndoc!=72 and ndoc!=73*/ then 1 else 0 end) as NrVendas 
			FROM	
				FT (nolock) 
			WHERE	
				ft.fdata=a.fdata 
				and ft.no>199
				AND LEFT(ft.ousrhora,2) = LEFT(a.ousrhora,2)
				AND ft.ousrinis = a.ousrinis
				and ft.ndoc not in (select distinct serie_ent from empresa (nolock)) /*fatura entidades*/
				and ft.ndoc not in (select distinct serie_entSNS from empresa (nolock)) /*fatura SNS*/
			)
		,nrVendascReceita = (
			SELECT	
				SUM(case when u_ltstamp != '' OR  u_ltstamp2 != '' then 1 else 0 end) as nrVendascReceita 
			FROM	
				FT (nolock) 
			WHERE	
				ft.fdata=a.fdata 
				and ft.no>199
				AND LEFT(ft.ousrhora,2) = LEFT(a.ousrhora,2)
				AND ft.ousrinis = a.ousrinis
			)			
		,'EPVP'				= ROUND(SUM(U_EPVP),2),
		'EPCP'				= ROUND(SUM(EPCPOND),2),
		'MARGEM'			= ROUND(SUM(MARGEM),2),
		/*X=MARG*100/TOT*/
		'MARGEM_PERC'		= (CASE WHEN (SUM(ETILIQUIDO) + SUM(ettent1) + SUM(ettent2)) > 0
									THEN 								
											(ROUND(SUM(MARGEM),2)*100) / (SUM(ETILIQUIDO) + SUM(ettent1) + SUM(ettent2))
									ELSE	0
							END),
		/*ROUND((CASE WHEN (SUM(EPCPOND)) > 0	THEN ((SUM(U_EPVP) / SUM(EPCPOND))-1) * 100	ELSE 0 END),2),*/
		'TotalDescontos'	= SUM(TotalDescontos),
		'ousrinis'			= ousrinis + '-' +(SElect top 1 username from b_us (nolock) Where b_us.iniciais = a.ousrinis),
		'username'			= (SElect top 1 username from b_us (nolock) Where b_us.iniciais = a.ousrinis),
		'Hora'				= LEFT(ousrhora,2)
FROM(	
		/* DOCS DE FACTURACAO */
		SELECT	
			fdata
			,YEAR(FT.fdata) as Ano
			,MONTH(FT.FDAta) as Mes
			,FT.NMDOC
			,FI.ETILIQUIDO as ETILIQUIDO
			,ettent1 = CASE WHEN ft.tipodoc = 3 THEN fi.u_ettent1*-1 ELSE fi.u_ettent1 END
			,ettent2 = CASE WHEN ft.tipodoc = 3  THEN fi.u_ettent2*-1 ELSE fi.u_ettent2 END
			,CASE WHEN TD.tipodoc = 3 THEN /* DEV. A CLIENTE */
					(CASE WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt ELSE FI.EPCP * qtt END)*-1
				ELSE
					(CASE WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt ELSE FI.EPCP * qtt END) 
				END	 as EPCPOND
			,CASE
				WHEN TD.tipodoc = 3 THEN /* DEV. A CLIENTE */
					FI.u_EPVP * qtt * (-1)
				ELSE
					FI.u_EPVP * qtt
				END as u_EPVP
			/*	CALCULO DA MARGEM CONSIDERAMOS SEMPRE ESTE CAMPO MESMO QUE NÃO TENHAM LIGAÇÃO,  DEVIDO À ELIMINAçÂO DE RECEITAS E POSTERIOR INSERÇÃOO TOTAL DA LINHA JÁ É NEGATIVO NAS REGULARIZAÇÔES */
			,(FI.ETILIQUIDO + (
					CASE WHEN TD.tipodoc = 3 THEN /* DEV. A CLIENTE */
						(Fi.u_ettent1*-1) + (Fi.u_ettent2*-1)
					ELSE
						(Fi.u_ettent1 + Fi.u_ettent2)
					END
			) - 
			(CASE WHEN TD.tipodoc = 3 THEN /* DEV. A CLIENTE */
				(CASE WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt ELSE FI.EPCP * qtt END)*-1
			ELSE
				(CASE WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt ELSE FI.EPCP * qtt END) 
			END) 
			)as MARGEM
			,CASE
				WHEN TD.tipodoc = 3 THEN /* DEV. A CLIENTE */
					(CASE WHEN fi.desconto between 0.01 and 99.99 then (fi.epv*fi.qtt)-(fi.etiliquido)
						  WHEN fi.desconto=100 then fi.epv*fi.qtt else 0 
					END)*-1
				ELSE
					(CASE 
						WHEN fi.desconto between 0.01 and 99.99 then (fi.epv*fi.qtt)-(fi.etiliquido)
						WHEN fi.desconto=100 then fi.epv*fi.qtt else 0 
					END)
					END	 as TotalDescontos
			,'ousrhora'			= Ft.ousrhora
			,'ousrinis'			= Ft.ousrinis
		FROM	
			FI (nolock) 
			INNER JOIN FT (nolock) ON FI.FTSTAMP = FT.FTSTAMP
			INNER JOIN TD (nolock) ON TD.NDOC = FT.NDOC
			Left Join st  (nolock) ON fi.ref = st.ref
			left join cteFprod on fi.ref = cteFprod.cnp
		WHERE	
			(td.tipodoc!=4 or u_tipodoc=4) AND u_tipodoc != 1 AND u_tipodoc != 5
			and ft.no > 199
			AND fdata>= @dataIni and fdata <= @dataFim
			AND fi.composto=0 
			AND ft.anulado=0
			and LEFT(ft.ousrhora,2) = CASE WHEN @hora = '' THEN LEFT(ft.ousrhora,2) ELSE @hora END
			/*and (ft.ousrinis in (select items from dbo.up_splitToTable(@op,',')) or @op= '0' or @op= '')	*/
			and (ft.ousrinis in (select iniciais from @tempcteOperadores) or @op= '')
			and fi.ref = Case When @ref = '' Then fi.ref else @ref End
			and isnull(st.u_lab,'') = CASE When @lab = '' Then isnull(st.u_lab,'') Else @lab End
			and isnull(st.usr1,'') = CASE When @marca = '' Then isnull(st.usr1,'') Else @marca End
			and fi.familia in (Select ref from @tempcteFamilia)
			and ft.site = (case when @site = '' Then ft.site else @site end)
			and isnull(dci,'') = case when @dci = '' then isnull(dci,'') else @dci end
) a
GROUP BY 
	a.fdata, Ano, Mes, ousrinis, LEFT(ousrhora,2)
END


GO
Grant Execute on dbo.up_relatorio_vendasDia to Public
Grant control on dbo.up_relatorio_vendasDia to Public
GO