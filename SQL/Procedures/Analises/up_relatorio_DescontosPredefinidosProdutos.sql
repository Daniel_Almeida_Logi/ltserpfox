/*
Alterado a 2020-08-07 por JG: inclus�o de input site, permitindo multiselecao

 exec up_relatorio_DescontosPredefinidosProdutos '','', '', '', '15',0,0,'Loja 1'
  exec up_relatorio_DescontosPredefinidosProdutos '','', '', '', '15',0,0,'Loja 2'
   exec up_relatorio_DescontosPredefinidosProdutos '','', '', '', '15',0,0,'Loja 1,Loja 2'

*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_DescontosPredefinidosProdutos]') IS NOT NULL
	drop procedure dbo.up_relatorio_DescontosPredefinidosProdutos
go

create procedure dbo.up_relatorio_DescontosPredefinidosProdutos
@ref	as varchar(18),
@design	as varchar(254),
@lab			as	varchar(100),
@marca			as  varchar(200),
@familia		as varchar(max),
@descValor as numeric(9,2),
@descPerc as numeric(9,2),
@site as varchar(254) = ''


/* with encryption */

AS 
BEGIN

	declare @site_nr varchar(20)

	set @site_nr = isnull((select  convert(varchar(254),(select 
		convert(varchar,no) + ', '
	from 
		empresa (nolock)							
	Where 
		site in (Select items from dbo.up_splitToTable(@site, ','))
	FOR XML PATH(''))) as no),0)

	

	declare @tempcteFamilia table (ref varchar(18))
	insert into @tempcteFamilia 
	Select distinct ref From stfami (nolock) Where ref in (Select items from dbo.up_splitToTable(@familia,',')) Or @familia = '0' or @familia = ''

	Select 
		ref
		,design
		,stock
		,faminome
		,familia
		,lab = u_lab
		,marca = usr1
		,PRECOCUSTOPONDERADO_ACTUAL = st.EPCPOND
		,ULTIMOPRECOCUSTO_ACTUAL = st.EPCULT
		,PRECOCUSTODETABELA_ACTUAL = st.EPCUSTO
		,PRECOVENDA1 = epv1
		,IVA = (SELECT TAXA FROM taxasiva (nolock) WHERE codigo = TABIVA)
		,st.UINTR as ULTIMAENTRADA
		,st.USAID as ULTIMASAIDA
		,descontoPercentagem = mfornec
		,descontoValor = mfornec2
	from	
		st
	where
		st.ref like @ref + '%'
		and st.design like @design + '%'
		and isnull(st.u_lab,'') = CASE When @lab = '' Then isnull(st.u_lab,'') Else @lab End
		and isnull(st.usr1,'') = CASE When @marca = '' Then isnull(st.usr1,'') Else @marca End
		and st.familia in (Select ref from @tempcteFamilia)
		and st.mfornec >= @descPerc
		and st.mfornec2 >= @descValor 
		and site_nr in (Select items from dbo.up_splitToTable(@site_nr, ',')) 
END





GO
Grant Execute on dbo.up_relatorio_DescontosPredefinidosProdutos to Public
Grant control on dbo.up_relatorio_DescontosPredefinidosProdutos to Public
GO