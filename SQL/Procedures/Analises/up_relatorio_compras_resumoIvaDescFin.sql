-- exec up_relatorio_compras_resumoIvaDescFin '20100301', '20190329', '55,101', 'Loja 1'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_compras_resumoIvaDescFin]') IS NOT NULL
	drop procedure dbo.up_relatorio_compras_resumoIvaDescFin
go

create procedure dbo.up_relatorio_compras_resumoIvaDescFin
@dataIni as date,
@dataFim as date,
@documentos as varchar(max),
@site varchar(55)

/* WITH ENCRYPTION */ 
AS
BEGIN
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosComprasSDescFin'))
		DROP TABLE #dadosComprasSDescFin
	select	
		year(fo.docdata) as Ano,
		month(fo.docdata) as Mes, 
		fo.docnome as Documento,
		fo.nome,
		fo.no,
		efinv = SUM(fo.efinv)
	from 
		fo (nolock)
		inner join cm1 (nolock) on cm1.cm=fo.doccode
	where	
		fo.docdata between @dataIni and @dataFim
		and fo.doccode != 101
		AND convert(varchar,cm1.cm) in (select items from dbo.up_splitToTable(@documentos,','))
		and fo.site = (case when @site = '' Then fo.site else @site end)
		and fo.efinv > 0
	group by 
		year(fo.docdata), month(fo.docdata), fo.doccode, fo.docnome,fo.nome,fo.no
	order by 
		ano, mes, fo.docnome
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosComprasSDescFin'))
		DROP TABLE #dadosComprasSDescFin
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosDescFin'))
		DROP TABLE #dadosDescFin
END

GO
Grant Execute on dbo.up_relatorio_compras_resumoIvaDescFin to Public
Grant control on dbo.up_relatorio_compras_resumoIvaDescFin to Public
Go	