/* Relatório Venda Medicamentos Genéricos  

	exec up_relatorio_vendas_medicamentosgenericos '20150101','20151231','Loja 1', 1, 0, '', 1,''
	exec up_relatorio_vendas_medicamentosgenericos '20160101','20160531','Loja 1', 1, 0, 'MYLAN', 0,''
	exec up_relatorio_vendas_medicamentosgenericos '20221001','20221028','Loja 1', 1, 0, 'Bene Farmacêutica', 0,'','',''
	exec up_relatorio_vendas_medicamentosgenericos '20221111','20221111','Loja 1', 0, 0, '', 0,'','',''
	
*/

if OBJECT_ID('[dbo].[up_relatorio_vendas_medicamentosgenericos]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_medicamentosgenericos
go

create procedure [dbo].[up_relatorio_vendas_medicamentosgenericos]
	 @dataIni						as datetime
	,@dataFim						as datetime
	,@site							as varchar(55)
	,@ivaincl						as bit = 0
	,@comparticipado				as bit = 0
	,@lab							as varchar(254)
	,@incluiHistorico				as bit
	,@dci							as varchar(max)
	,@cnpem							as varchar(max)					
	,@grphmgcode					as varchar(max)	
/* with encryption */
AS
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dados_1'))
		DROP TABLE #dados_1
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.##dados_1Aux'))
		DROP TABLE #dados_1Aux
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dados_2'))
		DROP TABLE #dados_2
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.##dados_2Aux'))
		DROP TABLE #dados_2Aux
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dados_2'))
		DROP TABLE #dadosST
	/* Calc Loja e Armazens */
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa (nolock) where site = @site),0)
	select 
		armazem
	into
		#EmpresaArm
	From
		empresa(nolock) 
		inner join empresa_arm(nolock)  on empresa.no = empresa_arm.empresa_no
	where
		empresa.site = case when @site = '' then empresa.site else @site end	
	/* Dados Stock */
	Select 
		stock = SUM(isnull(st.stock,0))  
		,fprod.cnpem
		,site_nr
	into 
		#dadosST
	From
		st (nolock)
		inner join fprod (nolock) on st.ref = fprod.cnp
	where 
		 (isnull(fprod.cnpem,'')		 in (Select items from dbo.up_splitToTable(@cnpem,';')) or @cnpem = '') 
		AND site_nr = @site_nr
	group by 
		fprod.cnpem
		,st.site_nr
	order by 
		fprod.cnpem


	/* Define se vais mostrar resultados abaixo do Cliente 199 */
	declare @Entidades as bit
	set @Entidades = case when (select tipoempresa from empresa (nolock)  where site = @site) in ('FARMACIA', 'PARAFARMACIA') then convert(bit,1) else convert(bit,0) end
	/* Dados Base Vendas  */
	create table #dadosVendasBase (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)
	)
    insert #dadosVendasBase
    exec up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site, @incluiHistorico

	/* Elimina Vendas tendo em conta tipo de cliente*/
	If @Entidades = 1 and @incluiHistorico = 0
		delete from #dadosVendasBase where #dadosVendasBase.no < 199

	/* Só Vendas com comparticipacao */
	IF @comparticipado = 1 
	begin
		delete from #dadosVendasBase where (ettent1 = 0 and ettent2 = 0)
	end
--select * from #dadosVendasBase 
	/* Calc Result Set com filtros */
	Select 
		#dadosVendasBase.* 
		,fprod.cnpem
		,fprod.generico
		,fprod.grphmgdescr
		,fprod.grphmgcode 
		,stockCNPEM = ISNULL(#dadosST.stock,0)
	into
		#dados_1
	from 
		#dadosVendasBase
		inner join fprod (nolock) on #dadosVendasBase.ref = fprod.cnp
		inner join st (nolock)	  on #dadosVendasBase.ref = st.ref and #dadosVendasBase.loja_nr = st.site_nr
		left join #dadosST		  on #dadosST.cnpem = fprod.cnpem and #dadosST.site_nr= st.site_nr
	Where
		fprod.u_familia					 in (SELECT id FROM uf_getMedicamentosInfarmed())
		and(isnull( st.u_lab,'')		 in (Select items from dbo.up_splitToTable(@lab,';')) or @lab = '')
		and (isnull(fprod.dcipt_id,'')	 in (Select items from dbo.up_splitToTable(@dci,';')) or @dci = '')
		and (isnull(fprod.cnpem,'')		 in (Select items from dbo.up_splitToTable(@cnpem,';')) or @cnpem = '')
		and (isnull(fprod.grphmgcode,'') in (Select items from dbo.up_splitToTable(@grphmgcode,';')) or @grphmgcode = '') 
		and st.site_nr = @site_nr
		and	st.ref = fprod.cnp			
--select * from #dados_1  where cnpem='50004271'
	/* Calc Result Set sem filtros */
	Select 
		#dadosVendasBase.* 
		,fprod.cnpem
		,fprod.generico
		,fprod.grphmgdescr
		,fprod.grphmgcode
		,stockCNPEM = ISNULL(#dadosST.stock,0)
	into
		#dados_1Aux
	from 
		#dadosVendasBase
		inner join fprod (nolock) on #dadosVendasBase.ref = fprod.cnp
		inner join st (nolock)	  on #dadosVendasBase.ref = st.ref and #dadosVendasBase.loja_nr = st.site_nr
		left join #dadosST		  on #dadosST.cnpem = fprod.cnpem and #dadosVendasBase.loja_nr = #dadosST.site_nr
	Where
		fprod.u_familia in (1,2,58)
		and (isnull(fprod.dcipt_id,'')	 in (Select items from dbo.up_splitToTable(@dci,';')) or @dci = '')
		and (isnull(fprod.cnpem,'')		 in (Select items from dbo.up_splitToTable(@cnpem,';')) or @cnpem = '')
		and (isnull(fprod.grphmgcode,'') in (Select items from dbo.up_splitToTable(@grphmgcode,';')) or @grphmgcode = '') 
		and	st.ref = fprod.cnp			
		and st.site_nr = @site_nr
--select * from #dados_1Aux where cnpem='50004271'
	-- com filtros
	;with ucrsaux1 as (
		select
			#dados_1.cnpem
			,#dados_1.grphmgcode
			,GrpHMG =  #dados_1.grphmgdescr
			,#dados_1.stockCNPEM
			,qtt_mngen = case when #dados_1.generico = 0 then SUM(#dados_1.qtt) else 0 end
			,qtt_mgen = case when #dados_1.generico = 1 then SUM(#dados_1.qtt) else 0 end
			,valor_mngen = case when #dados_1.generico = 0 
					then case when @ivaincl = 1 
					then #dados_1.etiliquido + #dados_1.ettent1 + #dados_1.ettent2 
					else #dados_1.etiliquidoSiva + #dados_1.ettent1siva + #dados_1.ettent2siva end else 0 end
			,valor_mgen = case when #dados_1.generico = 1 
					then case when @ivaincl = 1 
					then #dados_1.etiliquido + #dados_1.ettent1 + #dados_1.ettent2 
					else #dados_1.etiliquidoSiva + #dados_1.ettent1siva + #dados_1.ettent2siva end else 0 end
	From
		#dados_1
		inner join #dadosST on #dados_1.cnpem = #dadosST.cnpem 
	Group by
		#dados_1.cnpem, #dados_1.grphmgdescr,#dados_1.grphmgcode, #dados_1.stockCNPEM , #dados_1.generico , #dados_1.qtt , #dados_1.etiliquido , #dados_1.ettent1 , #dados_1.ettent2 , #dados_1.etiliquidoSiva , #dados_1.ettent1siva , #dados_1.ettent2siva
	)
	select  cnpem				as cnpem,
			GrpHMG				as GrpHMG,
			stockCNPEM,
			sum(qtt_mngen)		as qtt_mngen,
			sum(qtt_mgen)		as qtt_mgen,
			sum(valor_mngen)	as valor_mngen,
			sum(valor_mgen)		as valor_mgen
	into
		#dados_2
	From
		ucrsaux1
	Group by
		cnpem, GrpHMG, stockCNPEM 
--select  * from #dados_2 where cnpem='50004271'
	-- sem filtros
	;with ucrsaux2 as (
		select
		Cnpem_Total = #dados_1Aux.cnpem
		,#dados_1Aux.grphmgcode
		,GrpHMG_Total = #dados_1Aux.grphmgdescr 
		,StockCnpem_Total = #dados_1Aux.stockCNPEM
		,qtt_mngen_Total = case when #dados_1Aux.generico = 0 then #dados_1Aux.qtt else 0 end
		,qtt_mgen_Total = case when #dados_1Aux.generico = 1 then #dados_1Aux.qtt else 0 end
		,valor_mngen_Total = case when #dados_1Aux.generico = 0
			 then case when @ivaincl = 1 
				then #dados_1Aux.etiliquido + #dados_1Aux.ettent1 + #dados_1Aux.ettent2 
				else #dados_1Aux.etiliquidoSiva + #dados_1Aux.ettent1siva + #dados_1Aux.ettent2siva end else 0 end
		,valor_mgen_Total = case when #dados_1Aux.generico = 1 
			then case when @ivaincl = 1 
				then #dados_1Aux.etiliquido + #dados_1Aux.ettent1 + #dados_1Aux.ettent2 
				else #dados_1Aux.etiliquidoSiva + #dados_1Aux.ettent1siva + #dados_1Aux.ettent2siva end else 0 end
	From
		#dados_1Aux
	)
	select  Cnpem_Total				as Cnpem_Total,
			GrpHMG_Total			as GrpHMG_Total,
			StockCnpem_Total		as StockCnpem_Total,
			sum(qtt_mngen_Total)	as qtt_mngen_Total,
			sum(qtt_mgen_Total)		as qtt_mgen_Total,
			sum(valor_mngen_Total)	as valor_mngen_Total,
			sum(valor_mgen_Total)	as valor_mgen_Total
	into
		#dados_2Aux
	From
		ucrsaux2
	Group by
		Cnpem_Total, GrpHMG_Total, StockCnpem_Total

	-- result set final
	Select  
		#dados_2.Cnpem
		,GrpHMG = case when #dados_2.Cnpem = 0 then '' else GrpHMG end  -- quando é zero é de varios grupos e nao apenas um por isso vai a vazio
		,stockCNPEM = SUM(stockCNPEM)  
		,qtt_mngen
		,perc_qtt_mngen = case when (qtt_mngen+qtt_mgen) = 0 then 0 else qtt_mngen/(qtt_mngen+qtt_mgen) end * 100
		,qtt_mgen
		,perc_qtt_mgen = case when (qtt_mngen+qtt_mgen) = 0 then 0 else qtt_mgen/(qtt_mngen+qtt_mgen) end * 100
		,valor_mngen
		,valor_mngen_Total 
		,perc_valor_mngen = case when (valor_mngen+valor_mgen) = 0 then 0 else valor_mngen/(valor_mngen+valor_mgen) end * 100
		,perc_valor_mngen_Total = case when @lab = '' then 100 else case when (valor_mngen_Total) = 0 then 0 else (valor_mngen/valor_mngen_Total) end * 100 END -- valor novo 
		,valor_mgen
		,valor_mgen_Total 
		,perc_valor_mgen = case when (valor_mngen+valor_mgen) = 0 then 0 else valor_mgen/(valor_mngen+valor_mgen) end * 100
		,perc_valor_mgen_Total = case when @lab = '' then 100 else case when (valor_mgen_Total) = 0 then 0 else (valor_mgen/valor_mgen_Total) end * 100 END -- valor nova
	From	
		#dados_2Aux
		inner join #dados_2 on #dados_2Aux.cnpem_total = #dados_2.cnpem and #dados_2Aux.GrpHMG_Total = #dados_2.GrpHMG
	group by 
		#dados_2.cnpem, GrpHMG, qtt_mngen, qtt_mgen, valor_mngen, valor_mgen, valor_mngen_Total, valor_mgen_Total
	Order by
		cnpem, GrpHMG, qtt_mngen, qtt_mgen, valor_mngen, valor_mgen, valor_mngen_Total, valor_mgen_Total

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dados_1'))
		DROP TABLE #dados_1
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.##dados_1Aux'))
		DROP TABLE #dados_1Aux
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dados_2'))
		DROP TABLE #dados_2
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.##dados_2Aux'))
		DROP TABLE #dados_2Aux
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dados_2'))
		DROP TABLE #dadosST

GO
Grant Execute on dbo.up_relatorio_vendas_medicamentosgenericos to Public
Grant control on dbo.up_relatorio_vendas_medicamentosgenericos to Public
GO