/*
	exec up_relatorio_consignacao '20170101','20170601', 'Loja 1', '' 
*/
if OBJECT_ID('[dbo].[up_relatorio_consignacao]') IS NOT NULL
	drop procedure dbo.up_relatorio_consignacao
go


create procedure dbo.up_relatorio_consignacao
@dataIni		as datetime
,@dataFim		as datetime
,@site			as varchar(20)
,@tipoProduto	as varchar(254)

/* with encryption */
AS

	SET NOCOUNT ON

	Select 
		fo.nome
		,fo.data
		,fn.fnstamp
		,fn.ref
		,fn.design
		,fn.qtt
		--,qtt = isnull((Select SUM(qtt) from fn (nolock) as fn2 where fn2.ofnstamp = fn.fnstamp and fn.docnome = 'V/Guia Transp.'),0)
		,qttFact = isnull((Select SUM(qtt) from fn (nolock) as fn2 where fn2.ofnstamp = fn.fnstamp and fn2.docnome = 'V/Factura'),0)
		,qttDevolv = isnull((Select SUM(qtt) from bi (nolock) as fn2 where fn2.obistamp = fn.fnstamp and fn2.nmdos = 'Devol. a Fornecedor'),0)
		,iva = fn.iva
		,tipoProduto =  isnull(b_famFamilias.design,'')
	from
		fo (nolock)
		inner join fn (nolock) on fo.fostamp = fn.fostamp
		inner join st (nolock) on st.ref = fn.ref
		left join b_famFamilias (nolock) on st.u_famstamp = b_famFamilias.famstamp
	Where
		consignacao = 1
		and fo.docdata between @dataIni and @dataFim
		and fo.site = @site
		and fn.ref != ''
		and st.site_nr = (select no from empresa (nolock) where site = @site)
		and (b_famFamilias.design in (Select * from dbo.up_splitToTable(@tipoProduto,','))
		Or @tipoProduto = '')

GO
Grant Execute on dbo.up_relatorio_consignacao to Public
Grant control on dbo.up_relatorio_consignacao to Public
GO
