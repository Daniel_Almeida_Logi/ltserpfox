-- Pesquisar Recibos

-- exec up_relatorio_recibos 0, -1, '20221201',  '20230101', 'Loja 1','', 0
-- select * from re

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_recibos]') IS NOT NULL
	drop procedure dbo.up_relatorio_recibos
go

create procedure [dbo].[up_relatorio_recibos]
--@entidade	varchar(55),
@no			numeric(10,0),
@estab		numeric(3,0),
@dataini	datetime,
@datafim	datetime,
@site		varchar(20),
@ncont		varchar(20),
@movcaixa	bit
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON
declare @sql varchar(max)
select @sql = N'
	select
		isnull(B_pagCentral.stamp,'''') as B_pagCentralstamp 
		, nmdoc
		, re.rno
		, CONVERT(varchar, re.rdata, 102) as rdata
		, left(cast(re.ousrhora as varchar(8)),5) as ousrhora
		, re.nome
		, cast(re.etotal as numeric(15,2)) as etotal
		, cast(re.efinv as numeric(15,2)) as descfinanceiro
		, re.no
		, re.estab
		, re.ncont
		, replace(replace(re.desc1,''              '','' ''),''      '','' '') as desc1
		, tipo=(case when B_pagCentral.evdinheiro<>0 then (select design from B_modoPag where campoFact=''ft2.evdinheiro'')+'':''+cast(cast(B_pagCentral.evdinheiro as numeric(10,2)) as varchar(10))+''€''+char(13) + CHAR(10) else '''' end)
		+(case when B_pagCentral.echtotal<>0 then ''Cheque:''+cast(cast(B_pagCentral.echtotal as numeric(10,2)) as varchar(10))+''€''+CHAR(13) + CHAR(10) else '''' end)
		+(case when B_pagCentral.epaga1<>0 then (select design from B_modoPag where campoFact=''ft2.epaga1'')+'':''+cast(cast(B_pagCentral.epaga1 as numeric(10,2)) as varchar(10))+''€''+CHAR(13) + CHAR(10) else '''' end)
		+(case when B_pagCentral.epaga2<>0 then (select design from B_modoPag where campoFact=''ft2.epaga2'')+'':''+cast(cast(B_pagCentral.epaga2 as numeric(10,2)) as varchar(10))+''€''+char(13) + CHAR(10) else '''' end)
		+(case when B_pagCentral.epaga3<>0 then (select design from B_modoPag where campoFact=''ft2.epaga3'')+'':''+cast(cast(B_pagCentral.epaga3 as numeric(10,2)) as varchar(10))+''€''+char(13) + CHAR(10) else '''' end)
		+(case when B_pagCentral.epaga4<>0 then (select design from B_modoPag where campoFact=''ft2.epaga4'')+'':''+cast(cast(B_pagCentral.epaga4 as numeric(10,2)) as varchar(10))+''€''+char(13) + CHAR(10) else '''' end)
		+(case when B_pagCentral.epaga5<>0 then (select design from B_modoPag where campoFact=''ft2.epaga5'')+'':''+cast(cast(B_pagCentral.epaga5 as numeric(10,2)) as varchar(10))+''€''+char(13) + CHAR(10) else '''' end)
		+(case when B_pagCentral.epaga6<>0 then (select design from B_modoPag where campoFact=''ft2.epaga6'')+'':''+cast(cast(B_pagCentral.epaga6 as numeric(10,2)) as varchar(10))+''€''+char(13) + CHAR(10) else '''' end)
		, re.site
		, cast(re.epaga1 as numeric(15,2)) as visa
		, cast(re.epaga2 as numeric(15,2)) as multibanco
	from re (nolock)
	left join B_pagCentral (nolock) on re.u_nratend=B_pagCentral.nrAtend
	WHERE
		(re.u_tipodoc=1 or re.nmdoc='''')
		and (re.rdata between '''+convert(varchar,@dataini)+''' AND '''+convert(varchar,@datafim)+''')'
if @site != ''
select @sql = @sql + N'
		and re.site = '''+@site+''''
--if @entidade != ''
--select @sql = @sql + N'
--		and re.nome = '''+@entidade+''''
if @no != 0
select @sql = @sql + N'
		AND re.NO = '+convert(varchar,@no)
if @estab != -1
select @sql = @sql + N'
		AND re.ESTAB = '+convert(varchar,@estab)
if @ncont != ''
select @sql = @sql + N'
		and re.ncont = '''+@ncont+''''
if @movcaixa = 1
select @sql = @sql + N'
		and isnull(B_pagCentral.stamp,'''')<>'''''
-- exec up_relatorio_recibos 0, -1, '20220913',  '20220913', 'Loja 1','', 1
select @sql = @sql + N'
order by convert(varchar,rdata,102)+OUSRHORA, rno desc '
--print (@sql)
execute (@sql)
	
GO
Grant Execute On dbo.up_relatorio_recibos to Public
Grant Control On dbo.up_relatorio_recibos to Public
Go