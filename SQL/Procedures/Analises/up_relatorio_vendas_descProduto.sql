/* Relatório Vendas Desconto Produto
	exec up_relatorio_vendas_descProduto '20190701', '20210617', '', '', '', '', '', '', '', '', 'Loja 1', '', '', 1, '>', '0'
	exec up_relatorio_vendas_descProduto '20190701', '20210617', '', '', '', '', '', '', '', '', 'Loja 2', '', '', 1, '>', '0'
	exec up_relatorio_vendas_descProduto '20230331', '20230331', '', '', '', '', '', '', '', '', 'Loja 1', '', '', 0, '>', '0' 
	exec up_relatorio_vendas_descProduto '20230511', '20230512', '', '', '', '', '', '', '', '', 'Loja 1', '', '', 0, '>', '0' 

*/

IF OBJECT_ID('[dbo].[up_relatorio_vendas_descProduto]') IS NOT NULL
	DROP PROCEDURE dbo.up_relatorio_vendas_descProduto
GO

CREATE PROCEDURE [dbo].[up_relatorio_vendas_descProduto]
	@dataIni		DATETIME
	,@dataFim		DATETIME
	,@horaini		VARCHAR(2)
	,@horafim		VARCHAR(2)
	,@op			VARCHAR(MAX)
	,@ref			VARCHAR(MAX)
	,@lab			VARCHAR(150)
	,@marca			VARCHAR(200)
	,@familia		VARCHAR(MAX)
	,@atributosFam	VARCHAR(MAX)
	,@site			VARCHAR(254)
	,@design		VARCHAR(100)
	,@tipoDoc		VARCHAR(MAX)
	,@descontoTag	BIT
	,@descontoOp	VARCHAR(2)
	,@desconto		VARCHAR(10)

/* with encryption */

AS
SET NOCOUNT ON

	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
		DROP TABLE #dadosFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRefs'))
		DROP TABLE #dadosRefs
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
		DROP TABLE #dadosSt	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosTd'))
		DROP TABLE #dadosTd	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseFiltro'))
		DROP TABLE #dadosVendasBaseFiltro	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#Empresasite_nr'))
		DROP TABLE #Empresasite_nr


	SET @atributosFam = (CASE WHEN (@atributosFam IS NULL OR @atributosFam = '') THEN ';;;' ELSE @atributosFam END)

	DECLARE @departamento VARCHAR(254) = ''
	SET @departamento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 1),'') 
	DECLARE @sessao       VARCHAR(254) = ''
	set @sessao = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 2) ,'')
	DECLARE @categoria    VARCHAR(254) = ''
	SET @categoria = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 3),'') 
	DECLARE @segmento     VARCHAR(254) = ''
	SET @segmento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 4),'') 

	SELECT
		empresa.NO AS [NO]
	INTO #Empresasite_nr
	FROM empresa
		WHERE empresa.site IN (SELECT items FROM dbo.up_splitToTable(@site, ',')) 

	SELECT 
		armazem
	INTO
		#EmpresaArm
	FROM
		empresa
		INNER JOIN empresa_arm on empresa.no = empresa_arm.empresa_no
	WHERE
		empresa.site IN (SELECT items FROM dbo.up_splitToTable(@site, ',')) 

	/* Calc Documentos */
	SELECT
		ndoc
		,nmdoc
		,tipodoc
		,u_tipodoc
		,site 
	INTO
		#dadosTd
	FROM
		td
	WHERE
		CONVERT(VARCHAR(9),td.ndoc) IN (SELECT items FROM dbo.up_splitToTable(@tipoDoc,',')) OR @tipoDoc = ''

	/* Calc familias */
	SELECT DISTINCT 
		ref
		, nome
	INTO
		#dadosFamilia
	FROM
		stfami (NOLOCK)
	WHERE
		ref in (SELECT items FROM dbo.up_splitToTable(@familia,',')) Or @familia = '0' OR @familia = ''

	/* Calc operadores */
	SELECT
		iniciais
		, cm = userno
		, username
	INTO
		#dadosOperadores
	FROM
		b_us (NOLOCK)
	WHERE
		userno IN (SELECT items FROM dbo.up_splitToTable(@op,',')) OR @op= '0' OR @op= ''

	/* Calc Ref ST */
	SELECT 
		ref
	INTO
		#dadosRefs
	FROM
		st (NOLOCK)
	WHERE
		ref IN (SELECT items FROM dbo.up_splitToTable(@ref,',')) OR @ref= ''
		AND site_nr IN  (SELECT NO FROM #Empresasite_nr)
	
	/* Calc Informação Produto */
	SELECT
		st.ref
		,st.design
		,st.epcusto
		,st.epcult
		,usr1
		,u_lab	
		, ISNULL(A.descr,'') AS departamento
		, ISNULL(B.descr,'') AS seccao
		, ISNULL(C.descr,'') AS categoria
		, ISNULL(D.descr,'') AS segmento
		,dispdescr	= ISNULL(dispdescr,'')
		,generico	= ISNULL(generico,CONVERT(bit,0))
		,psico		= ISNULL(psico,CONVERT(bit,0))
		,benzo		= ISNULL(benzo,CONVERT(bit,0))
		,protocolo	= ISNULL(protocolo,CONVERT(bit,0))
		,dci		= ISNULL(dci,'')
		,(SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 1) valDepartamento
		,(SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 2) valSeccao
		,(SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 3) valCategoria
		,(SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 4) valSegmento
		,EMPRESA.site		AS site
	INTO
		#dadosSt
	From
		st (nolock)
		INNER JOIN EMPRESA ON st.site_nr = EMPRESA.no
		LEFT JOIN fprod (NOLOCK) ON st.ref = fprod.cnp		
		LEFT JOIN grande_mercado_hmr (NOLOCK) A ON A.id = st.u_depstamp
		LEFT JOIN mercado_hmr (NOLOCK) B ON B.id = st.u_secstamp
		LEFT JOIN categoria_hmr (NOLOCK) C ON C.id = st.u_catstamp 
		LEFT JOIN segmento_hmr (NOLOCK) D ON D.id = st.u_segstamp	
	WHERE
		st.u_lab = CASE WHEN @lab = '' THEN u_lab ELSE @lab END
		AND st.usr1 = CASE WHEN @marca = '' THEN usr1 ELSE @marca END
		AND site_nr IN  (SELECT NO FROM #Empresasite_nr)

	/* Define se vais mostrar resultados abaixo do Cliente 199 */
	DECLARE @Entidades AS BIT
		IF ((SELECT COUNT(tipoempresa) FROM empresa WHERE site IN (SELECT items FROM dbo.up_splitToTable(@site, ',')) 
				AND tipoempresa NOT IN ('FARMACIA', 'PARAFARMACIA'))>=1)
		BEGIN
			SET @Entidades = 0
		END 
		ELSE
		BEGIN 
			SET @Entidades = 1
		END 

	/* Dados Base Vendas  */
	CREATE TABLE #dadosVendasBase (
		nrAtend			VARCHAR(20)
		,ftstamp		VARCHAR(25)
		,fdata			DATETIME
		,[no]			NUMERIC(10,0)
		,estab			NUMERIC(5,0)
		,tipo			VARCHAR(25)
		,ndoc			NUMERIC(6,0)
		,fno			NUMERIC(10,0)
		,tipodoc		NUMERIC(2)
		,u_tipodoc		NUMERIC(2)
        ,ref			VARCHAR(18) COLLATE SQL_LATIN1_GENERAL_CP1_CI_AI
        ,design			VARCHAR(100) COLLATE SQL_LATIN1_GENERAL_CP1_CI_AI
		,familia		VARCHAR(18) COLLATE SQL_LATIN1_GENERAL_CP1_CI_AI
        ,u_epvp			NUMERIC(15,3)
        ,epcpond		NUMERIC(19,6)
        ,iva			NUMERIC(5,2)
        ,qtt			NUMERIC(11,3)
        ,etiliquido		NUMERIC(19,6)
        ,etiliquidoSiva NUMERIC(19,6)
        ,ettent1		NUMERIC(13,3)
        ,ettent2		NUMERIC(13,3)
        ,ettent1siva	NUMERIC(13,3)
        ,ettent2siva	NUMERIC(13,3)
        ,desconto		NUMERIC(6,2)
        ,descvalor		NUMERIC(19,6)
        ,descvale		NUMERIC(19,6)
        ,ousrhora		VARCHAR(8)
        ,ousrinis		VARCHAR(3) COLLATE SQL_LATIN1_GENERAL_CP1_CI_AI
        ,vendnm			VARCHAR(20)
        ,u_ltstamp		VARCHAR(25)
        ,u_ltstamp2		VARCHAR(25)
        ,ecusto			NUMERIC(19,6)
		,loja			VARCHAR(50)
		,loja_nr		NUMERIC(5,0)
	)

    INSERT #dadosVendasBase
    EXEC up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site
		
	/* Elimina Vendas tendo em conta tipo de cliente*/
	If @Entidades = 1
	DELETE FROM #dadosVendasBase WHERE #dadosVendasBase.no < 199
--select * from #dadosVendasBase where ref='8113837'
--select * from #dadosSt  where ref='7393702'
	/* Preparar Result Set */
	SELECT
		#dadosVendasBase.[ref]
		,#dadosVendasBase.[design]
		,#dadosVendasBase.[familia]
		,#dadosfamilia.[nome]													AS faminome
		,ROUND(#dadosSt.epcusto,2)												AS epcusto
		,ROUND(#dadosSt.epcult,2)												AS epcult
		,ROUND([epcpond],2)														AS epcpond
		,[iva]
		,desconto
		,#dadosVendasBase.u_epvp												AS pvpFicha
		,SUM(#dadosVendasBase.[qtt])											AS qtt
		,ROUND(#dadosSt.epcusto *  SUM(#dadosVendasBase.[qtt]),2)				AS totalSubepcusto
		,ROUND(#dadosSt.epcult * SUM(#dadosVendasBase.[qtt]),2)					AS totalSubepcult
		,ROUND(#dadosVendasBase.u_epvp * SUM(#dadosVendasBase.[qtt]),2)			AS totalSubPvp
		,[usr1]
		,[u_lab]
		,[departamento]
		,[seccao]
		,[categoria]
		,segmento
		,fdata
		,ROUND(SUM(#dadosVendasBase.etiliquido + #dadosVendasBase.ettent1 + #dadosVendasBase.ettent2),2) AS vdpvp
		,ROUND(ABS(IIF(#dadosVendasBase.ettent1 <=0,#dadosVendasBase.u_epvp - ABS(descvalor), 
				SUM((#dadosVendasBase.etiliquido + #dadosVendasBase.ettent1 + #dadosVendasBase.ettent2)))),2) AS vdpvpUni
		
	INTO
		#dadosVendasBaseFiltro
	FROM
		#dadosVendasBase 
		INNER JOIN #dadosTd ON #dadosTd.ndoc = #dadosVendasBase.ndoc
		LEFT JOIN #dadosSt ON #dadosVendasBase.ref COLLATE DATABASE_DEFAULT = #dadosSt.ref COLLATE DATABASE_DEFAULT AND  #dadosVendasBase.loja COLLATE DATABASE_DEFAULT = #dadosST.site  COLLATE DATABASE_DEFAULT
		LEFT JOIN #dadosFamilia ON #dadosVendasBase.familia COLLATE DATABASE_DEFAULT = #dadosfamilia.ref COLLATE DATABASE_DEFAULT
	WHERE
		#dadosVendasBase.tipo != 'ENTIDADE'
		AND #dadosVendasBase.u_tipodoc NOT IN (1, 5)
		AND ISNULL(#dadosSt.u_lab,'') = CASE When @lab = '' Then isnull(#dadosSt.u_lab,'') Else @lab End
		AND ISNULL(#dadosSt.usr1,'') = CASE When @marca = '' Then isnull(#dadosSt.usr1,'') Else @marca End
		AND LEFT(#dadosVendasBase.ousrhora,2) 
				BETWEEN (CASE WHEN @horaini='' THEN '0' ELSE @horaini END)
						AND
						(CASE WHEN @horafim='' THEN '24' ELSE @horafim END)
		AND #dadosVendasBase.ousrinis in (select iniciais COLLATE DATABASE_DEFAULT from #dadosOperadores)
		AND (#dadosVendasBase.ref in (Select ref COLLATE DATABASE_DEFAULT  from #dadosRefs) or #dadosVendasBase.ref='')
		AND isnull(#dadosVendasBase.familia,'99') in (Select ref COLLATE DATABASE_DEFAULT from #dadosFamilia)
		AND #dadosVendasBase.design like case when @design = '' then #dadosVendasBase.design else @design + '%' end
		AND departamento = (CASE WHEN @departamento = '' THEN departamento ELSE @departamento END)
		AND seccao = (CASE WHEN @sessao = '' THEN seccao ELSE  @sessao END)
		AND categoria = (CASE WHEN @categoria = '' THEN categoria ELSE  @categoria END)
		AND segmento = (CASE WHEN @segmento = '' THEN segmento ELSE  @segmento END) 
	GROUP BY
	     #dadosVendasBase.[ref]
		,#dadosVendasBase.[design]
		,#dadosVendasBase.[familia]
		,#dadosfamilia.[nome]
		,#dadosSt.epcusto
		,#dadosSt.epcult
		,[epcpond]
		,[iva]
		,desconto
		,#dadosVendasBase.u_epvp
		,[qtt]
		,[usr1]
		,[u_lab]
		,[departamento]
		,[seccao]
		,[categoria]
		,[segmento]
		,fdata 
		,#dadosVendasBase.etiliquido 
		,#dadosVendasBase.ettent1 
		,#dadosVendasBase.ettent2
		,descvalor
--select * from #dadosVendasBaseFiltro where ref='7393702'
	/* result set final com base no desconto */
	IF @descontoTag = 0 
	BEGIN 
		SELECT 
			* 
		FROM 
			#dadosVendasBaseFiltro 
	End

	IF @descontoTag = 1 AND @descontoOp = '<'
	BEGIN 
		SELECT 
			* 
		FROM 
			#dadosVendasBaseFiltro 
		WHERE
			 desconto < @desconto
	End

	IF @descontoTag = 1 AND @descontoOp = '='
	BEGIN 
		SELECT 
			* 
		FROM 
			#dadosVendasBaseFiltro 
		WHERE
			 desconto = @desconto
	End 

	IF @descontoTag = 1 AND @descontoOp = '>'
	BEGIN 
		SELECT 
			* 
		FROM 
			#dadosVendasBaseFiltro 
		WHERE
			 desconto > @desconto
	End

	IF @descontoTag = 1 AND @descontoOp = '>='
	BEGIN 
		SELECT 
			* 
		FROM 
			#dadosVendasBaseFiltro 
		WHERE
			 desconto >= @desconto
	End

	IF @descontoTag = 1 AND @descontoOp = '<='
	BEGIN 
		SELECT 
			* 
		FROM 
			#dadosVendasBaseFiltro 
		WHERE
			 desconto <= @desconto
	End


	--select * from #dadosVendasBaseFiltro 

	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
		DROP TABLE #dadosFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRefs'))
		DROP TABLE #dadosRefs
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
		DROP TABLE #dadosSt
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosTd'))
		DROP TABLE #dadosTd
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.##dadosVendasBaseFiltro'))
		DROP TABLE #dadosVendasBaseFiltro

GO
Grant Execute on dbo.up_relatorio_vendas_descProduto to Public
Grant control on dbo.up_relatorio_vendas_descProduto to Public
GO