/* Relat�rio de Dividas de Clientes Mensal      
	
	exec up_relatorio_credito_AgENDaDividasClientes 0, 0, '20230410', 1, 0,'Loja 1', 0, ''

	select esaldo,* from b_utentes WHERE no = 5817 order by estab asc
	
*/ 

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_relatorio_credito_AgENDaDividasClientes]') IS NOT NULL
	drop procedure dbo.up_relatorio_credito_AgENDaDividasClientes
go

create procedure dbo.up_relatorio_credito_AgENDaDividasClientes
@no					AS VARCHAR(9),
@estab				AS NUMERIC(5,0),
@dataIni			AS DATETIME,
@excluiEntidades	AS BIT,
@saldo				AS NUMERIC(9,2),
@site				VARCHAR(55),
@excluiInativos		AS BIT = 0,
@tipoCliente		VARCHAR(254)

/* with encryption */
AS
BEGIN	

	SET LANGUAGE portuguese	

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteSaldoAnterior'))
		DROP TABLE #cteSaldoAnterior
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteCC'))
		DROP TABLE #cteCC
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteDadosCompletos'))
		DROP TABLE #cteDadosCompletos
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteRecebidoMesAtual'))
		DROP TABLE #cteRecebidoMesAtual


	SET @no = CASE WHEN @no = '' THEN '0' ELSE @no END

	-- temp table com Saldo Anterior
	SELECT
		cc.nome, 
		cc.no,
		cc.estab, 
		saldo = ISNULL((SUM((ecred-ecredf))- SUM((edeb-edebf)))*-1,0)
	INTO 
		#cteSaldoAnterior
	FROM 
		cc (NOLOCK)
		left join ft (NOLOCK) on ft.ftstamp = cc.ftstamp 
		left join re (NOLOCK) on re.restamp = cc.restamp 
	WHERE 
		(cc.edeb-cc.edebf-(cc.ecred-cc.ecredf)) != 0 
		and cc.no = CASE WHEN @no = 0 THEN cc.no ELSE @no END
		and cc.estab = CASE WHEN @estab = 0 THEN cc.estab ELSE @estab END
		and cc.datalc < @dataIni
		and (
			ft.site = (CASE WHEN @site = '' THEN ft.site ELSE @site END)
			Or
			re.site = (CASE WHEN @site = '' THEN ft.site ELSE @site END)
			)
	GROUP BY
		cc.nome
		,cc.moeda
		,cc.no
		,cc.estab
	ORDER BY 
		cc.no, cc.estab, cc.nome, cc.moeda
	
	-- temp table Conta Corrente 
	SELECT 
		cc.moeda
		,nome = cc.nome + '[' + RTRIM(LTRIM(STR(cc.no))) + '][' +  RTRIM(LTRIM(STR(cc.estab)))  +']'
		,cc.no
		,cc.estab
		,saldo = ISNULL((SUM((ecred-ecredf))- SUM((edeb-edebf)))*-1,0)
		,ano = YEAR(cc.datalc)
		,mes = right('0'+UPPER(MONTH(cc.datalc)),2) + ' - ' + UPPER(DATENAME(month,cc.datalc))
	INTO 
		#cteCC
	FROM 
		cc (NOLOCK)
		left join ft (NOLOCK) on ft.ftstamp = cc.ftstamp 
		left join re (NOLOCK) on re.restamp = cc.restamp 
	WHERE 
		(cc.edeb-cc.edebf-(cc.ecred-cc.ecredf)) != 0 
		and cc.no = CASE WHEN @no = 0 THEN cc.no ELSE @no END
		and cc.estab = CASE WHEN @estab = 0 THEN cc.estab ELSE @estab END
		and cc.datalc >= @dataIni
		and (
			ft.site = (CASE WHEN @site = '' THEN ft.site ELSE @site END)
			Or
			re.site = (CASE WHEN @site = '' THEN ft.site ELSE @site END)
		)
	GROUP BY 
		cc.nome
		,cc.moeda
		,cc.no
		,cc.estab
		,YEAR(cc.datalc)
		,right('0'+UPPER(MONTH(cc.datalc)),2) + ' - ' + UPPER(DATENAME(month,cc.datalc))
	ORDER BY 
		cc.no, cc.estab, cc.nome, cc.moeda

	-- temp table com dados totais
	Select 
		saldoAnterior = ISNULL(#cteSaldoAnterior.saldo,0)
		,nome = nome
		,no
		,estab
		,saldo = ISNULL(#cteSaldoAnterior.saldo,0) 
		,ano =  'Anterior a ' + LEFT(CONVERT(VARCHAR, @dataIni, 120), 10) 
		,mes = '' 
	INTO 
		#cteDadosCompletos
	FROM 
		#cteSaldoAnterior
	
	UNION ALL 
			
	SELECT 
		saldoAnterior = 0
		,b_utentes.nome
		,b_utentes.no
		,b_utentes.estab
		,saldo = ISNULL(#cteCC.saldo,0)
		,ano =  CASE WHEN ano is null THEN 'Anterior a ' + LEFT(CONVERT(VARCHAR, @dataIni, 120), 10) 
										ELSE 'Z_' + STR(ano) END
		,mes =   CASE WHEN mes is null THEN '' ELSE mes END
	FROM 
		b_utentes (NOLOCK)
		LEFT JOIN #cteCC ON b_utentes.no = #cteCC.no  AND b_utentes.estab = #cteCC.estab

	-- recebido mes em que � gerado o relat�rio (na loja seleccionada)
	SELECT 
		no
		,estab
		,TotalRecebido = sum(etotal) 
	INTO 
		#cteRecebidoMesAtual
	FROM 
		re (NOLOCK)
	WHERE 
		re.no = CASE WHEN @no = 0 THEN no ELSE @no END
		AND re.estab = CASE WHEN @estab = 0 THEN estab ELSE @estab END
		AND re.ousrdata between (DATEADD(month, DATEDIFF(month, 0, getdate()), 0)) 
			AND (DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,getdate())+1,0)))
		AND re.site = @site
	GROUP BY 
		no
		,estab	

	-- result set final 
	SELECT 
		#cteDadosCompletos.saldoAnterior
		,#cteDadosCompletos.ano
		,#cteDadosCompletos.mes
		,#cteDadosCompletos.saldo
		,nome = LTRIM(RTRIM(#cteDadosCompletos.nome)) + '[' + RTRIM(LTRIM(STR(#cteDadosCompletos.no))) + '][' +  RTRIM(LTRIM(STR(#cteDadosCompletos.estab)))  +']'
		,ISNULL(#cteRecebidoMesAtual.totalrecebido,0)				AS recebidomesatual
	FROM 
		#cteDadosCompletos
		LEFT JOIN b_utentes (NOLOCK) ON b_utentes.no = #cteDadosCompletos.no AND #cteDadosCompletos.estab = b_utentes.estab
		LEFT JOIN #cteRecebidoMesAtual ON #cteRecebidoMesAtual.no = #cteDadosCompletos.no 
										AND #cteRecebidoMesAtual.estab = #cteDadosCompletos.estab
	WHERE
		#cteDadosCompletos.no > CASE WHEN @excluiEntidades = 1 THEN 200 ELSE 0  END
		AND b_utentes.no = CASE WHEN @no = 0 THEN b_utentes.no ELSE @no END
		AND b_utentes.estab = CASE WHEN @estab = 0 THEN b_utentes.estab ELSE @estab END
		AND isnull(b_utentes.esaldo,0) != @saldo
		AND b_utentes.inactivo = CASE WHEN @excluiInativos = 1 THEN 0 ELSE b_utentes.inactivo END
		AND (b_utentes.tipo in (Select items from dbo.up_splitToTable(@tipoCliente,',')) OR @tipoCliente='') 
		AND (#cteDadosCompletos.saldo != 0 OR ISNULL(#cteRecebidoMesAtual.totalrecebido,0) != 0)

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteSaldoAnterior'))
		DROP TABLE #cteSaldoAnterior
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteCC'))
		DROP TABLE #cteCC
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteDadosCompletos'))
		DROP TABLE #cteDadosCompletos
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteRecebidoMesAtual'))
		DROP TABLE #cteRecebidoMesAtual


END
GO
Grant Execute on dbo.up_relatorio_credito_AgENDaDividasClientes to Public
Grant control on dbo.up_relatorio_credito_AgENDaDividasClientes to Public
GO
