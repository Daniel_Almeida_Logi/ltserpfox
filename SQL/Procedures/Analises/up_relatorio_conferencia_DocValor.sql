-- Relatório Conferencia Documentos Valor
-- exec up_relatorio_conferencia_DocValor '20110101','20130101', 'F', ''

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_relatorio_conferencia_DocValor]') IS NOT NULL
	drop procedure dbo.up_relatorio_conferencia_DocValor
go

create procedure dbo.up_relatorio_conferencia_DocValor
@dataIni datetime,
@dataFim datetime,
@status  varchar(1),
@site	 varchar(55),
@fornecedor varchar(55)=0,
@estab as numeric(5,0) = -1,
@op				 varchar(max)=''
/* with encryption */
AS 
BEGIN

	SET NOCOUNT ON

	If OBJECT_ID('tempdb.dbo.#dadosOperadores') IS NOT NULL
		DROP TABLE #dadosOperadores

	SELECT
		iniciais, cm = userno, username
	INTO
		#dadosOperadores
	FROM
		b_us (NOLOCK)
	WHERE
		userno in (select items from dbo.up_splitToTable(@op,','))
		or @op= '0'
		or @op= ''	


	SELECT	
		(Select TOP 1 REF FROM empresa (nolock) WHERE site = @site) as CODLOJA,
		(Select TOP 1 SITE FROM empresa (nolock) WHERE site = @site) as LOJA,
		no as NUMFORNECEDOR,
		FO.docnome AS TIPO_DOC,
		FO.ADOC as NUMDOCUMENTO,
		FO2.U_DOCCONT as NUMLANCAMENTO,
		FO.DATA as DATA_DOCUMENTO,
		iva as VAT_CODE,
		fn.etiliquido  as COST_VAT,  
		(fn.etiliquido*(iva/100)) as VALOR_IVA,
		0 as ECO_VALUE,
		fn.etiliquido  + (fn.etiliquido*(iva/100)) as TOTALLINHA, 
		ETOTAL as TOTALDOC,
		U_CLASS as U_DOCTYPE,
		ISNULL((SELECT U_CLASS FROM FN (nolock) DOCORIGEM INNER jOIN cm1 (nolock) ON cm1.cmdesc = DOCORIGEM.docnome  WHERE DOCORIGEM.fnstamp = FN.ofnstamp),'') as SIGLA_ORIGEM,
		ISNULL((SELECT RTRIM(LTRIM(DOCNOME)) FROM FN DOCORIGEM (nolock) WHERE DOCORIGEM.fnstamp = FN.ofnstamp),'') as DOC_ORIGEM,
		ISNULL((SELECT LTRIM(RTRIM(ADOC)) FROM FN DOCORIGEM (nolock) WHERE DOCORIGEM.fnstamp = FN.ofnstamp),'') as NUMDOC_ORIGEM,
		ISNULL((SELECT DOCORIGEM.etiliquido FROM FN DOCORIGEM (nolock) WHERE DOCORIGEM.fnstamp = FN.ofnstamp),0) as TOTAL_ORIGEM    
	FROM	
		FN (nolock) 
		INNER JOIN FO  (nolock) ON FN.fostamp = FO.FOSTAMP
		INNER JOIN FO2 (nolock) ON FO2.FO2STAMP = FO.FOSTAMP
		INNER JOIN CM1 (nolock) ON FO.docnome  = cm1.cmdesc
	WHERE	
		U_STATUS = case when @status = '' then u_status else @status end 
		AND (FO.[no] = CASE WHEN @Fornecedor = 0 THEN FO.no ELSE @Fornecedor END)
		AND (FO.estab = CASE WHEN @estab>-1 then  @estab else FO.estab  end)
		AND FO.ousrdata BETWEEN @dataIni and @dataFim
		AND (fo.ousrinis in (select iniciais from #dadosOperadores) or @op= '0' or @op= '' )
		AND FOLANFC = 1 /* LANÇA EM CC */
		AND NAOFO = 0
		AND (FN.qtt != 0 OR fn.etiliquido !=0)
		and fo.site=@site
	UNION ALL
		SELECT	(Select TOP 1 REF FROM empresa  (nolock) WHERE site = @site) as CODLOJA,
				(Select TOP 1 SITE FROM empresa (nolock) WHERE site = @site) as LOJA,
				BO.NO as NUMFORNECEDOR,
				BO.NMDOS AS TIPO_DOC,
				CONVERT(varchar,BO.OBRANO) as NUMDOCUMENTO,
				0 as NUMLANCAMENTO,
				BO.ousrdata as DATA_DOCUMENTO,iva as VAT_CODE,
				BI.ettdeb  as COST_VAT,  
				(BI.ettdeb*(iva/100)) as VALOR_IVA,
				0 as ECO_VALUE,
				BI.ettdeb  + (BI.ettdeb*(iva/100)) as TOTALLINHA, 
				ETOTAL as TOTALDOC,
				'CR' as U_DOCTYPE,
				ISNULL((SELECT U_CLASS FROM FN DOCORIGEM (nolock) INNER JOIN CM1 (nolock) ON CM1.CMDESC = DOCORIGEM.docnome WHERE DOCORIGEM.fnstamp = BI2.fnstamp),'') as SIGLA_ORIGEM,
				ISNULL((SELECT RTRIM(LTRIM(DOCNOME)) FROM FN DOCORIGEM (nolock) WHERE DOCORIGEM.fnstamp = BI2.fnstamp),'') as DOC_ORIGEM,
				ISNULL((SELECT LTRIM(RTRIM(ADOC)) FROM FN DOCORIGEM (nolock) WHERE DOCORIGEM.fnstamp = BI2.fnstamp),'') as NUMDOC_ORIGEM,
				ISNULL((SELECT DOCORIGEM.etiliquido FROM FN DOCORIGEM (nolock) WHERE DOCORIGEM.fnstamp = BI2.fnstamp),0) as TOTAL_ORIGEM
		FROM	
			BI (nolock) 
			INNER JOIN BO  (nolock) ON BI.BOSTAMP = BO.BOSTAMP
			INNER JOIN BI2 (nolock) ON BI.BISTAMP = BI2.BI2STAMP
			INNER JOIN BO2 (nolock) ON BO2.BO2STAMP = BO.BOSTAMP
			INNER JOIN TS  (nolock) ON BO.ndos  = TS.ndos
		WHERE	
			BO.fechada = case when @status = '' then bo.fechada else (case when @status = 'A' then 1 else 0 end) end 
			AND (bo.[no] = CASE WHEN @Fornecedor = 0 THEN bo.no ELSE @Fornecedor END)
			AND (bo.estab = CASE WHEN @estab>-1 then  @estab else bo.estab  end)
			AND (bo.ousrinis in (select iniciais from #dadosOperadores) or @op= '0' or @op= '' )
			AND BO.dataobra BETWEEN @dataIni and @dataFim
			AND BO.NDOS = 38 /* PEDIDO DE CREDITO */
			AND (BI.qtt != 0 OR BI.ettdeb !=0)
			and bo.site=@site
	ORDER BY DATA_DOCUMENTO,NUMDOCUMENTO	
	END

	If OBJECT_ID('tempdb.dbo.#dadosOperadores') IS NOT NULL
		DROP TABLE #dadosOperadores

GO
Grant Execute On dbo.up_relatorio_conferencia_DocValor to Public
Grant control On dbo.up_relatorio_conferencia_DocValor to Public
GO