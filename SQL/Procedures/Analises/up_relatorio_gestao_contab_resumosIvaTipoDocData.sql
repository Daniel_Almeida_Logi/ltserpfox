/*
	Relatório Resumos Iva Tipo Doc - 

	exec up_relatorio_gestao_contab_resumosIvaTipoDocData '20170301','20170301','Loja 1',0,0
	exec up_relatorio_gestao_contab_resumosIvaTipoDocData '20200106','20200108','Loja 1',0,0
	exec up_relatorio_gestao_contab_resumosIvaTipoDocData '20200106','20201231','Loja 1',0,0
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_contab_resumosIvaTipoDocData]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_contab_resumosIvaTipoDocData
go

Create PROCEDURE [dbo].up_relatorio_gestao_contab_resumosIvaTipoDocData
	@dataIni datetime,
	@dataFim datetime,
	@site as varchar(55),
	@excluiEntidades bit = 0,
	@soEntidades bit = 0
/* with encryption */
AS 
	SET NOCOUNT ON
	SELECT
		Documento	= RTRIM(Ft.nmdoc),
		SERVICO		= FI.stns,
		TAXA		= FI.IVA,
		DESCONTOS	= CASE WHEN ft.tipodoc != 3 THEN
						SUM(case
								when (fi.desconto between 0.01 and 99.99) then (fi.epv*fi.qtt)-(CASE WHEN etiliquido <0 THEN etiliquido*-1 ELSE etiliquido END)
								when fi.desconto=100 then fi.epv*fi.qtt else 0 
								end + fi.u_descval)
					  ELSE
						  SUM(case
								when (fi.desconto between 0.01 and 99.99) then (fi.epv*fi.qtt)-(CASE WHEN etiliquido <0 THEN etiliquido*-1 ELSE etiliquido END)
								when fi.desconto=100 then fi.epv*fi.qtt else 0 
								end + fi.u_descval
							) *-1
					  END,
		VALORIVA	= SUM(CASE
							WHEN Fi.ivaincl = 1 
								THEN fi.etiliquido - (Fi.ETILIQUIDO/(Fi.iva/100+1))
							ELSE (Fi.ETILIQUIDO*(Fi.iva/100)) 
							END
						),
		BASEINC		= SUM(CASE
							WHEN Fi.ivaincl = 1
								THEN (Fi.ETILIQUIDO/(Fi.iva/100+1))
							ELSE fi.ETILIQUIDO 
							END
						),
		TOTAL		= SUM(CASE
							WHEN Fi.ivaincl = 1 
								THEN fi.etiliquido - (Fi.ETILIQUIDO/(Fi.iva/100+1))
							ELSE (Fi.ETILIQUIDO*(Fi.iva/100)) 
							END
							+
							CASE
								WHEN Fi.ivaincl = 1
									THEN (Fi.ETILIQUIDO/(Fi.iva/100+1))
								ELSE fi.ETILIQUIDO 
								END
						),
		CONVERT(date, fdata)  fdata
	FROM	
		FT (nolock) 
		INNER JOIN td (nolock) ON td.ndoc = ft.ndoc 
		INNER JOIN fi (nolock) ON fi.ftstamp = ft.ftstamp
	WHERE	
		fdata between @dataIni and @dataFim
		and anulado = 0 
		and td.tiposaft!=''
		and ((fi.qtt <> 0 or fi.etiliquido <> 0) OR  (ft.anulado = 1 ))
		and ft.site = (case when @site = '' Then ft.site else @site end)
		and ft.no >= case when @excluiEntidades = 1 then 199 else 0 end 
		and ft.no < case when @soEntidades = 1 then 199 else 999999999 end
		and td.tipodoc!=4
	Group By
		RTRIM(Ft.nmdoc),FI.IVA,ft.tipodoc, fi.stns,CONVERT(date, fdata)
	ORDER BY 
		TAXA, fi.stns, Documento

GO
Grant Execute On dbo.up_relatorio_gestao_contab_resumosIvaTipoDocData to Public
Grant control On dbo.up_relatorio_gestao_contab_resumosIvaTipoDocData to Public
GO