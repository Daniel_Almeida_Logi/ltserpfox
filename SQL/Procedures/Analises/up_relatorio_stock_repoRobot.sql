/* Relatório Encomendas Robot - productos em falta

	exec up_relatorio_stock_repoRobot 'Loja 1'
	
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_stock_repoRobot]') IS NOT NULL
	drop procedure dbo.up_relatorio_stock_repoRobot
go

create procedure [dbo].up_relatorio_stock_repoRobot
	@site varchar(30)

/* with encryption */

AS

	declare @token varchar(36)
	declare @sitenr numeric(2,0)


	set @site = rtrim(ltrim(@site)) 

	set @sitenr = ISNULL((select no from empresa where site=@site),0)

	/* ultimo inventário a data de hoje */


	select top 1 @token = token from robot_response_payload_stock 
	where CONVERT(VARCHAR(10), ousrdata, 105)  = CONVERT(VARCHAR(10), getdate(), 105) and lastRequest = 1
	order by ousrdata desc

	set @token = rtrim(ltrim(@token))

	/* Calc artigos inventariados */
	select 
		distinct 
		"ref" = RTRIM(LTRIM(st.ref)),  
		"design" = RTRIM(LTRIM(st.design)),  
		"stockArm" = CONVERT(int,st.stock),  
		"stockRobot" = stkRobot.quantity,  
		"capMaxRobot" = st.robotStockMax,  
		"diffStock" =  
				case when  st.robotStockMax > 0  then  st.robotStockMax -  stkRobot.quantity
					else 0
				end
		,diffStock2 =  
				case when  st.robotStockMax > 0  then  
					case when  (CONVERT(int,st.stock)-stkRobot.quantity) <= (st.robotStockMax-stkRobot.quantity) then (CONVERT(int,st.stock)-stkRobot.quantity) 					
						else  (st.robotStockMax-stkRobot.quantity)	
					end																			
				else 0
				end


	from st(nolock)
	inner join robot_response_payload_stock(nolock) stkRobot on RTRIM(LTRIM(stkRobot.productCode)) = RTRIM(LTRIM(st.ref))
	where st.site_nr = @sitenr and rtrim(ltrim(stkRobot.token))=@token


GO
Grant Execute On dbo.up_relatorio_stock_repoRobot to Public
Grant control On dbo.up_relatorio_stock_repoRobot to Public
GO

