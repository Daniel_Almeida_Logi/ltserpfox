/* Relat�rio 

 
	exec up_relatorio_comunicacao_indisponibilidade_infarmed '20220101', '20300217', 'Loja 1'
	

*/

if OBJECT_ID('[dbo].[up_relatorio_comunicacao_indisponibilidade_infarmed]') IS NOT NULL
	drop procedure dbo.up_relatorio_comunicacao_indisponibilidade_infarmed
go

create procedure [dbo].[up_relatorio_comunicacao_indisponibilidade_infarmed]
	@dataini		datetime = '1900-01-01'
	,@datafim		datetime = '1900-01-01'	
	,@site			varchar(20)
	
		

/* with encryption */
AS
SET NOCOUNT ON


declare @stmt varchar(max)

declare @site_nr int
select @site_nr = no from empresa where site = @site  

set @stmt = N' 	
			SELECT 
				CONVERT(VARCHAR(100),A.ref) ref ,
				 B.design, 
				 A.nrAttend,
				 A.prescriptionId,
				 CONVERT(VARCHAR,A.date,23) as data,
				 CONVERT(VARCHAR,A.date,24) as hora,
				 CONVERT(VARCHAR,A.siteNr),
				 case when c.code = ''200'' then ''Sim'' else ''N�o'' end as comunicado 
			FROM
				missingProductsInfo (nolock) A
			LEFT JOIN 
				ext_esb_missingProducts_result(nolock) c on A.stamp = c.reqStamp
			LEFT JOIN 
				st (NOLOCK) B  on CONVERT(VARCHAR(100),B.ref) = CONVERT(VARCHAR(100),A.ref) 
									and B.site_nr =  COALESCE(NULLIF(' + CONVERT(VARCHAR,@site_nr) + ',0), sitenr)  
			WHERE								
				convert(varchar,date,23) between ''' + CONVERT(VARCHAR,@dataini,23) + ''' and ''' + CONVERT(VARCHAR,@datafim,23) + ''' 				
				and sitenr =  COALESCE(NULLIF(' + CONVERT(VARCHAR,@site_nr) + ',0), sitenr) 
			ORDER BY date desc
			'
		
Print(@stmt)
exec(@stmt)


GO
Grant Execute on dbo.up_relatorio_comunicacao_indisponibilidade_infarmed to Public
Grant control on dbo.up_relatorio_comunicacao_indisponibilidade_infarmed to Public
GO

