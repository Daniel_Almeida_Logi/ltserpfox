/* Lista kits enviados

	exec up_analises_pesquisa_kits_pts '', '', ''

	use E01322A
	select * from ext_esb_communications(nolock) 
	select inactivo,id,* from b_utentes(nolock)  where id !='' and inactivo = 0

*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_analises_pesquisa_kits_pts]') IS NOT NULL
	drop procedure dbo.up_analises_pesquisa_kits_pts
go


create procedure dbo.up_analises_pesquisa_kits_pts

@ano		int = 0,
@mes		int = 0,
@codigo		varchar(20) =''



/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	if(@codigo!='')
	begin
		set @codigo =   REPLACE(LTRIM(REPLACE(@codigo,'0',' ')),' ','0') 
	end




	select
		"Codigo Infarmed" = id,
		"Kits Vendidos" = qtt,
		"Nome Farm�cia" = ut.nome,
		"M�s comunica��o" = comm.month,
		"Ano comunica��o" = comm.year,
		"Data comunica��o" = comm.ousrdata

	from
		ext_esb_communications comm (nolock)
	inner join  
		b_utentes ut(nolock) on	
		REPLACE(LTRIM(REPLACE(comm.senderId,'0',' ')),' ','0') =  REPLACE(LTRIM(REPLACE(ut.id,'0',' ')),' ','0') 
	where
		comm.month = case when @mes = '' then comm.month else @mes  end
		and  comm.year = case when @ano = '' then comm.year else @ano end
		and  REPLACE(LTRIM(REPLACE(comm.senderId,'0',' ')),' ','0') = case when @codigo = '' then  REPLACE(LTRIM(REPLACE(comm.senderId,'0',' ')),' ','0') else @codigo  end
		and valid = 1 and test = 0 and ut.inactivo = 0
	
	
Go
Grant Execute on dbo.up_analises_pesquisa_kits_pts to Public
Grant Control on dbo.up_analises_pesquisa_kits_pts to Public
Go

