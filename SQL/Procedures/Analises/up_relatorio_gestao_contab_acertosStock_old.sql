/* Relatório Contabilidade Acerto Stock

	 exec up_relatorio_gestao_contab_acertosStock  '20150501', '20161231', '', '', 0, 'Loja 1'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_contab_acertosStock]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_contab_acertosStock
go

create procedure dbo.up_relatorio_gestao_contab_acertosStock
@dataIni DATETIME,
@dataFim DATETIME,
@motivo	 varchar(254),
@ref	 varchar(254),
@total	 numeric(9,2),	
@site	 varchar(60)
/* WITH ENCRYPTION */ 
AS
BEGIN
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosUltPreco'))
		DROP TABLE #dadosUltPreco
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosArmazem'))
		DROP TABLE #dadosArmazem
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosMov'))
		DROP TABLE #dadosMov
	/* Consulta Preco SL Preco */
	SELECT	
		ref
		,datalc
		,evu
		,ousrhora
	into
		#dadosUltPreco
	FROM	
		SL (nolock) 
	WHERE	
		sl.origem in ('FO','IF')
		and evu != 0
		and SL.DATALC BETWEEN @DataIni AND @DataFim 
	/* Consulta Armazens */			
	Select 
		empresa_arm.armazem
		,site
		,ref
		,empresa
	into
		#dadosArmazem
	FROM 
		empresa (nolock) 
		inner join empresa_arm on empresa.no = empresa_arm.empresa_no
	Where 
		site = @site
	declare @empresa as varchar(20)
	set @empresa = (select top 1 empresa from #dadosarmazem)
	SELECT	
		EMPRESA		= @empresa
		,LOJA		= @site
		,NUMLOJA	= @ref
		,SL.DATALC as DATA
		,CMDESC    as DOCUMENTO
		,sl.adoc   as NUMDOC
		,SL.REF
		,sl.DESIGN
		,TIPOMOV = 
			CASE 
				WHEN cm = 14 AND SL.QTT > 0 THEN 'EC' /*acerto de stock*/
				WHEN cm = 14 AND SL.QTT < 0 THEN 'SC' /*acerto de stock*/
				WHEN cm = 47 THEN 'EI' /*Entrada p/Inventário*/
				WHEN cm = 92 THEN 'SI' /*Saida p/Inventário*/
				ELSE ''
			END
		,MQUEBRA = 
			CASE 
				WHEN	sl.bistamp = '' THEN 'Inventário' 
				ELSE	bi.U_MQUEBRA 
			END 
		,sl.QTT
		,UNIDADE = (SELECT top 1 UNIDADE FROM ST (nolock) WHERE ST.REF LIKE SL.REF)
		,PCL = 
			CASE 
				WHEN	bi.bistamp = '' THEN EVU 
				ELSE	ISNULL((SELECT	
									TOP 1 a.EVU 
								FROM	
									#dadosUltPreco a (nolock) 
								WHERE	
									a.DATALC <= sl.DATALC
									AND a.REF = SL.REF
								ORDER BY 
									a.datalc desc, a.ousrhora DESC
							),sl.evu) 
			END
		,IVA = isnull(bi.IVA,0)
	INTO 
		#dadosMov
	FROM
		sl (nolock)
		left join bi (nolock) on sl.bistamp = bi.bistamp
	WHERE	
		cm in (14, 47, 92)
		and SL.DATALC BETWEEN @DataIni and @DataFim
		and sl.ref = case when @ref = '' then sl.ref else @ref end
		and sl.armazem in (select armazem from #dadosArmazem)
	Select 
		* 
		,TOTAL = PCL * QTT
	from 
		#dadosMov
	Where
		#dadosMov.MQUEBRA = case when @motivo = '' then #dadosMov.MQUEBRA else @motivo end
		and (PCL * QTT) >= @total
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosUltPreco'))
		DROP TABLE #dadosUltPreco
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosArmazem'))
		DROP TABLE #dadosArmazem
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosMov'))
		DROP TABLE #dadosMov
	END

GO
Grant Execute on dbo.up_relatorio_gestao_contab_acertosStock to Public
GO
Grant control on dbo.up_relatorio_gestao_contab_acertosStock to Public
GO