-- exec up_relatorio_DispIndispUs '', 'LOja 1'
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_relatorio_DispIndispUs]') IS NOT NULL
    drop procedure up_relatorio_DispIndispUs
go

create PROCEDURE up_relatorio_DispIndispUs
@especialista varchar(100) = ''
,@site varchar(60) = ''


/* WITH ENCRYPTION */
AS

	Select 
		stamp = LTRIM(b_series.seriestamp)
		,tipo = 'DISPONIBILIDADE'
		,Local = site
		,Especialista = b_series.nome
		,Descricao = serienome
		,dataInicio
		,dataFim
		,horaInicio
		,horafim
		,segunda
		,terca
		,quarta
		,quinta
		,sexta
		,sabado
		,domingo
		,duracao
		,ref  = isnull(b_us_disp_resursos.ref,'')
		,design = isnull(b_us_disp_resursos.nome,'')
	from 
		b_series (nolock)
		left join b_us_disp_resursos (nolock) on b_series.seriestamp = b_us_disp_resursos.seriestamp
	where 
		b_series.tipo = 'utilizadores'
		and b_series.nome = case when @especialista = '' then b_series.nome else @especialista end
		and b_series.site = case when @site = '' then b_series.site else @site end
		
		
	union all

	Select
		stamp = LTRIM(STR(b_us_Indisponibilidades.id))
		,tipo = 'INDISPONIBILIDADE'
		,Local = site
		,Especialista = b_us.nome
		,Descricao = design
		,dataInicio
		,dataFim
		,horaInicio
		,horafim
		,segunda
		,terca
		,quarta
		,quinta
		,sexta
		,sabado
		,domingo
		,duracao = 0
		,ref  = ''
		,design = ''
	From
		b_us_Indisponibilidades (nolock)
		inner join b_us  (nolock) on b_us.userno = b_us_Indisponibilidades.userno
		and b_us.nome = case when @especialista = '' then b_us.nome else @especialista end
		and b_us_Indisponibilidades.site = case when @site = '' then b_us_Indisponibilidades.site else @site end
	order by 
		Especialista, tipo, dataInicio, ref,design


GO
Grant Execute On up_relatorio_DispIndispUs to Public
Grant Control On up_relatorio_DispIndispUs to Public
go
