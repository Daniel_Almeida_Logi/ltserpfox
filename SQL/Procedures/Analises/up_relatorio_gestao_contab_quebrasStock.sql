-- Relatório Contabilidade Vendas - deprecated
-- exec up_relatorio_gestao_contab_quebrasStock '20100101','20130101','','',0,'Loja 1'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_contab_quebrasStock]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_contab_quebrasStock
go

create procedure dbo.up_relatorio_gestao_contab_quebrasStock
@dataIni DATETIME,
@dataFim DATETIME,
@motivo	 varchar(254),
@ref	 varchar(254),
@total	 numeric(9,2),	
@site	 varchar(60)
/* WITH ENCRYPTION */ 
AS
BEGIN
	declare @site_nr as tinyint
	set @site_nr = (select no from empresa where site = @site)
	SELECT	
		EMPRESA		= empresa.empresa
		,LOJA		= empresa.site
		,NUMLOJA	= empresa.ref
		,SL.DATALC as DATA,
		CMDESC as DOCUMENTO,
		ADOC as NUMDOC,
		SL.REF,
		DESIGN,
		CASE 
			WHEN CMDESC = 'Acerto de Stock' AND SL.QTT > 0 THEN 'SC' 
			WHEN CMDESC = 'Acerto de Stock' AND SL.QTT < 0 THEN 'EC' 
			WHEN CMDESC = 'Entrada p/Inventário' THEN 'EI' 
			WHEN CMDESC = 'Saida P/Inventário' THEN 'SI'
			ELSE ''
		END as TIPOMOV,
		CASE 
			WHEN	bistamp = '' THEN 'Inventário' 
			ELSE	ISNULL((SELECT bi.U_MQUEBRA FROM BO (nolock) INNER JOIN BI (nolock) ON BO.BOSTAMP = BI.BOSTAMP WHERE BI.bistamp = sl.BISTAMP ),'') 
		END as MQUEBRA,
		QTT,
		(SELECT UNIDADE FROM ST (nolock) WHERE ST.REF LIKE SL.REF and st.site_nr = @site_nr) as UNIDADE,
		CASE 
			WHEN	bistamp = '' THEN EVU 
			ELSE	ISNULL((SELECT	TOP 1 a.EVU 
							FROM	SL a (nolock) 
							WHERE	(a.ORIGEM = 'FO' OR a.ORIGEM = 'IF')
									AND a.DATALC <= sl.DATALC
									AND a.REF = SL.REF
							ORDER BY a.datalc DESC
						),0) 
		END as PCL, 
		CASE 
			WHEN	bistamp = '' THEN EVU 
			ELSE	ISNULL((SELECT	TOP 1 a.EVU 
							FROM	SL a (nolock)
							WHERE	(a.ORIGEM = 'FO' OR a.ORIGEM = 'IF')
									AND a.DATALC <= sl.DATALC
									AND a.REF = SL.REF
							ORDER BY a.datalc DESC
						),0) 
		END * QTT as TOTAL
	FROM
		SL (nolock)
		inner join empresa_arm on empresa_arm.armazem = sl.armazem
		inner join empresa on empresa_arm.empresa_no = empresa.no
	WHERE	
		(CMDESC = 'Acerto de Stock' OR CMDESC LIKE '%INVENT%')
		AND SL.DATALC BETWEEN @DataIni AND @DataFim
	END

GO
Grant Execute on dbo.up_relatorio_gestao_contab_quebrasStock to Public
GO
Grant control on dbo.up_relatorio_gestao_contab_quebrasStock to Public
GO
