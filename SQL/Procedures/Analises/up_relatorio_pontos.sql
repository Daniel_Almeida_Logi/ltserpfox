-- Análise: 
-- exec up_relatorio_pontos '20150101','20161231',234,-1
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_pontos]') IS NOT NULL
	drop procedure dbo.up_relatorio_pontos
go

create procedure dbo.up_relatorio_pontos
@dataIni as datetime,
@dataFim as datetime,
@no numeric(9,0),
@estab numeric(3,0)
/* with encryption */
AS
	Select 
		*
	From (
		select 
			ft.no
			,ft.estab
			,tipo= 'VALOR'
			,ousrdata = fi.ousrdata + fi.ousrhora
			,descricao = ' Documento: ' + ft.nmdoc + ' nr. ' + RTRIM(LTRIM(STR(ft.fno))) + '; Produto: ' + fi.design + ' (Valor:' + RTRIM(LTRIM(STR(fi.etiliquido,9,2))) + ')'
			,fi.campanhas 
			,fi.pontos
		from 
			fi  (nolock)
			inner join ft (nolock) on fi.ftstamp = ft.ftstamp
		where 
			pontos != 0
			and ft.no = @no		
			--Devolve todos os dependentes se o estab for < 0
			and ft.estab = case when @estab>-1 then  @estab else ft.estab  end
			and ft.ousrdata between @dataIni and @dataFim
		union all
		Select 
			no
			,estab
			,tipo= 'ATENDIMENTO'
			,isnull((select top 1 ousrdata + ousrhora from ft (nolock) where ft.u_nratend = B_pagCentral.nrAtend),'19000101')
			,descricao = ' Atendimento: ' + nrAtend
			,B_pagCentral.campanhas
			,pontos = pontosAT
		from
			B_pagCentral  (nolock)
		where
			pontosAt != 0
			and B_pagCentral.no = @no
			--Devolve todos os dependentes se o estab for < 0
			and B_pagCentral.estab  = case when @estab>-1 then  @estab else B_pagCentral.estab  end
			and isnull((select top 1 ousrdata from ft (nolock) where ft.u_nratend = B_pagCentral.nrAtend),'19000101') between @dataIni and @dataFim
		union all
		Select
			no
			,estab
			,tipo= 'VENDA'
			,ft.ousrdata + ft.ousrhora
			,descricao = ' Documento: ' + ft.nmdoc + ' nr. ' + RTRIM(LTRIM(STR(ft.fno)))
			,ft.campanhas
			,pontos = pontosVd
		from 
			ft (nolock) 
		where 
			pontosVd != 0
			and ft.no = @no
			--Devolve todos os dependentes se o estab for < 0
			and ft.estab  = case when @estab>-1 then  @estab else ft.estab  end
			and ft.ousrdata between @dataIni and @dataFim
		union all
		Select
			no
			,estab
			,tipo= 'VALE'
			,vale.ousrdata
			,descricao = ' Emissão vale: ' + ref + ' , valor: ' + RTRIM(LTRIM(STR(vale.valor,9,2))) + ', pontos: ' + RTRIM(LTRIM(STR(vale.pontos)))
			,campanhas = ''
			,pontos = vale.pontos * -1
		from
			B_fidelvale vale (nolock)
			inner join b_utentes (nolock) on b_utentes.utstamp=vale.clstamp
		where 
			b_utentes.no = @no
			--Devolve todos os dependentes se o estab for < 0
			and b_utentes.estab  = case when @estab>-1 then  @estab else b_utentes.estab  end
			and convert(date,vale.ousrdata) between @dataIni and @dataFim
		union all
		Select
			no
			,estab
			,tipo= 'VALE'
			,vale.usrdata
			,descricao = ' Utilização vale: ' + ref + ' , valor: ' + RTRIM(LTRIM(STR(vale.valor,9,2))) + ', pontos: ' + RTRIM(LTRIM(STR(vale.pontos)))
			,campanhas = ''
			,pontos = 0
		from
			B_fidelvale vale (nolock)
			inner join b_utentes (nolock) on b_utentes.utstamp=vale.clstamp
		where 
			b_utentes.no = @no
			--Devolve todos os dependentes se o estab for < 0
			and b_utentes.estab  = case when @estab>-1 then  @estab else b_utentes.estab  end
			and convert(date,vale.usrdata) between @dataIni and @dataFim
			and vale.abatido = 1
		union all
		Select
			no
			,estab
			,tipo= 'VALE'
			,vale.usrdata
			,descricao = ' Anulação vale: ' + ref + ' , valor: ' + RTRIM(LTRIM(STR(vale.valor,9,2))) 
			,campanhas = ''
			,pontos = 0
		from
			B_fidelvale vale (nolock)
			inner join b_utentes (nolock) on b_utentes.utstamp=vale.clstamp
		where 
			b_utentes.no = @no
			--Devolve todos os dependentes se o estab for < 0
			and b_utentes.estab  = case when @estab>-1 then  @estab else b_utentes.estab  end
			and convert(date,vale.usrdata) between @dataIni and @dataFim
			and vale.anulado = 1
		union all 
		Select 
			no
			,estab
			,tipo= 'VALE'
			,vale.usrdata
			,descricao = b_ocorrencias.descr + '; Valor Antes:' + b_ocorrencias.ovalor + '; Valor Depois:' + b_ocorrencias.dValor
			,campanhas = ''
			,pontos = nValor 
		from 
			B_fidelvale vale (nolock)
			inner join b_utentes (nolock) on b_utentes.utstamp=vale.clstamp
			inner join b_ocorrencias (nolock) on b_ocorrencias.linkstamp = vale.fvstamp
		where
			b_utentes.no = @no
			--Devolve todos os dependentes se o estab for < 0
			and b_utentes.estab  = case when @estab>-1 then  @estab else b_utentes.estab  end
			and convert(date,b_ocorrencias.date) between @dataIni and @dataFim
		union all 
		Select 
			no
			,estab
			,tipo= 'TRANSFERENCIA'
			,vale.usrdata
			,descricao = b_ocorrencias.descr + '; Valor Antes:' + b_ocorrencias.ovalor + '; Valor Depois:' + b_ocorrencias.dValor
			,campanhas = ''
			,pontos = nValor 
		from 
			B_fidelvale vale (nolock)
			inner join b_utentes (nolock) on b_utentes.utstamp=vale.clstamp
			inner join b_ocorrencias (nolock) on b_ocorrencias.linkstamp = vale.clstamp
		where
			b_utentes.no = @no
			--Devolve todos os dependentes se o estab for < 0
			and b_utentes.estab  = case when @estab>-1 then  @estab else b_utentes.estab  end
			and convert(date,b_ocorrencias.date) between @dataIni and @dataFim
		union all
		Select 
			no
			,estab
			,tipo= 'ALTERAÇÃO CARTÃO'
			,usrdata = b_ocorrencias.date
			,descricao = b_ocorrencias.descr + '; Valor Antes:' + b_ocorrencias.ovalor + '; Valor Depois:' + b_ocorrencias.dValor
			,campanhas = ''
			,pontos = nValor 
		from 
			b_utentes (nolock)
			inner join b_ocorrencias (nolock) on b_ocorrencias.linkstamp = b_utentes.utstamp
		where
			b_utentes.no = @no
			--Devolve todos os dependentes se o estab for < 0
			and b_utentes.estab  = case when @estab>-1 then  @estab else b_utentes.estab  end
			and convert(date,b_ocorrencias.date) between @dataIni and @dataFim
	) x
	order by 
		x.ousrdata 

GO
Grant Execute on dbo.up_relatorio_pontos to Public
Grant Control on dbo.up_relatorio_pontos to Public
Go