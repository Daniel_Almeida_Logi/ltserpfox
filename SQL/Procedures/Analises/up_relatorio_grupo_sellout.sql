
-- SP: up_relatorio_grupo_sellout
-- exec	up_relatorio_grupo_sellout '19000101','20150101', 'Sidefarma - Sociedade Industrial de Expansão Farmacêutica, S.A.', 'Loja 1'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_grupo_sellout]') IS NOT NULL
	drop procedure dbo.up_relatorio_grupo_sellout
go

Create procedure dbo.up_relatorio_grupo_sellout
@dataIni datetime,
@dataFim datetime,
@lab varchar(120),
@site varchar(55)


/* with encryption */
AS

	Select
		bo.dataobra 
		,bo.nome
		,bo.no
		,bo.estab
		,bi.ref
		,bi.design
		,qtt = SUM(bi.qtt)
		,lab = isnull(fprod.titaimdescr,'')
		,marca = isnull(st.usr1,'')
	from 
		bo (nolock)
		inner join bi (nolock) on bo.bostamp = bi.bostamp
		left join st (nolock) on bi.ref = st.ref 
		left join fprod on  bi.ref = fprod.ref 
	where 
		bo.ndos = 44
		and bo.dataobra between @dataIni and @dataFim
		and isnull(fprod.titaimdescr,'') like '%' + @lab +'%'
	group by
		bo.dataobra 
		,bo.nome
		,bo.no
		,bo.estab
		,bi.ref
		,bi.design
		,fprod.titaimdescr
		,st.usr1

Go
Grant Execute on dbo.up_relatorio_grupo_sellout to Public
Grant Control on dbo.up_relatorio_grupo_sellout to Public
Go


