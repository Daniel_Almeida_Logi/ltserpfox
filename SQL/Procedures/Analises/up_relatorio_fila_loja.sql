/*
	exec  up_relatorio_fila_loja '20220101','20220601', 'Loja 1','43',0
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_fila_loja]') IS NOT NULL
	drop procedure  dbo.up_relatorio_fila_loja
go

create procedure dbo.up_relatorio_fila_loja
	@dataIni		datetime
	,@dataFim		datetime
	,@site			varchar(55)
	,@op			varchar(max) = ''
	,@tempomaior	numeric(9,2) = 0

/* with encryption */

AS
SET NOCOUNT ON


	if(@op='0')		
		set @op = ''




	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadospausa'))
		DROP TABLE #dadospausa
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadostotais'))
		DROP TABLE #dadostotais
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosfinais'))
		DROP TABLE #dadosfinais
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOp'))
		DROP TABLE #dadosOp
	
	create table #dadosOp(op numeric(6,0))


	if(@op!='') 
	begin
		insert #dadosOp
		select items as op from dbo.up_splitToTable(@op,',')
	end else begin
		insert #dadosOp		
		select userno as op from b_us(nolock) where inactivo = 0
	end
	



	set language portuguese

	/* Preparar Resultados */
	select
		convert(date, ext_xopvision_queue_management.start_date, 112) as data
		, (select COUNT(aux1.token) 
			from ext_xopvision_queue_management(nolock) aux1 
			where ISNULL(aux1.nrAtend,'')='' and YEAR(aux1.creation_date)>2017
				and CONVERT(varchar, aux1.start_date, 112)= convert(varchar, ext_xopvision_queue_management.start_date, 112)) as desistencias
		, COUNT(B_pagCentral.nrAtend) as nratend
		, cast(isnull(avg(DATEDIFF(MINUTE, ext_xopvision_queue_management.creation_date,  ext_xopvision_queue_management.start_date)),0) as numeric(5,0)) as tmedesp
		, cast(isnull(max(DATEDIFF(MINUTE, ext_xopvision_queue_management.creation_date,  ext_xopvision_queue_management.start_date)),0) as numeric(5,0)) as tmaxesp
		, cast(avg(DATEDIFF(MINUTE, ext_xopvision_queue_management.start_date,  B_pagCentral.oData)) as numeric(5,0)) as tmedatendimento
		, cast(max(DATEDIFF(MINUTE, ext_xopvision_queue_management.start_date,  B_pagCentral.oData)) as numeric(5,0)) as tmaxatendimento
	into
		#dadosfinais
	from 
		ext_xopvision_queue_management (nolock)
		inner join B_pagCentral (nolock) on B_pagCentral.nrAtend=ext_xopvision_queue_management.nrAtend
		inner join 	#dadosOp  on #dadosOp.op = B_pagCentral.vendedor
	where
		convert(date, ext_xopvision_queue_management.start_date, 112) between @dataIni and @dataFim
		and isnull(ext_xopvision_queue_management.ticket_id, '')<>''
		and isnull(ext_xopvision_queue_management.nrAtend, '')<>''
		and (select COUNT(u_nratend) from ft(nolock) where ft.u_nratend=B_pagCentral.nrAtend)=1
		and B_pagCentral.site=@site
		and ext_xopvision_queue_management.token not in (select token from ext_xopvision_breakmotive(nolock) where ext_xopvision_breakmotive.start_date between @dataIni and @dataFim)
	
	group by
		convert(date, ext_xopvision_queue_management.start_date, 112) 
		,convert(varchar, ext_xopvision_queue_management.start_date, 112) 
	order by 
		 convert(varchar, ext_xopvision_queue_management.start_date, 112)
		 ,convert(date, ext_xopvision_queue_management.start_date, 112) 


	if @tempomaior >= 0
	delete from #dadosfinais where tmedesp < @tempomaior 
		
	select 
		upper(datename(weekday, data)) as diasemana
		,* 
	from 
		#dadosfinais
	WHERE
		data between @dataIni and @dataFim
	order by 
		diasemana


	SET LANGUAGE us_english
	
	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadospausa'))
		DROP TABLE #dadospausa
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadostotais'))
		DROP TABLE #dadostotais
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosfinais'))
		DROP TABLE #dadosfinais
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOp'))
		DROP TABLE #dadosOp
		
--up_relatorio_fila_loja '20180205', '20180210' ,'Loja 1'
--up_relatorio_fila_loja '20180207', 'Loja 1'


GO
Grant Execute On dbo.up_relatorio_fila_loja to Public
Grant control On dbo.up_relatorio_fila_loja to Public
GO
