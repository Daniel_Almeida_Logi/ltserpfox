/* Relatório Detalhe Vendas & Relatório Vendas Diarias & Relatório Vendas Familia

	exec up_relatorio_vendas_campanhas '20200101', '20200130', 0, 999, 'Loja 1', '6884940', ''


	exec up_relatorio_vendas_campanhas '20220621', '20220701', 0, 999, 'Loja 1', '', ''	 
	exec up_relatorio_vendas_campanhas '20230501', '20230513', 0, 999, 'Loja 1', '', ''
*/

if OBJECT_ID('[dbo].[up_relatorio_vendas_campanhas]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_campanhas
go

create procedure [dbo].[up_relatorio_vendas_campanhas]
	@dataIni		DATETIME		
	,@dataFim		DATETIME		
	,@opIni			NUMERIC(9,0)	
	,@opFim			NUMERIC(9,0)	
	,@site			VARCHAR(30)
	,@ref			VARCHAR(30)
	,@campanha		VARCHAR(240)	
	--,@incliva		bit=0

	

/* with encryption */
AS
SET NOCOUNT ON

/* Elimina Tabelas */
IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#VendasCampanhas'))
	DROP TABLE #VendasCampanhas
	

	DECLARE @stmt VARCHAR(MAX)
	
	
	SET @stmt = N' 	
			SELECT				
				CONVERT(VARCHAR,ft.fdata,23) AS fdata	
				, ft.ftstamp
				, ft.fno
				, ft.nmdoc
				, ft.vendedor
				, ft.vendnm
				, ft.ousrinis 
				, ft.site
				, ft.pnome
				, ft.pno
				, fi.ref
				, fi.design
				, fi.epv * SUM(CASE WHEN ft.tipodoc = 3 THEN -qtt ELSE qtt END) AS epv
				,SUM(CASE WHEN ft.tipodoc=1 THEN 1 ELSE 0 END)			AS vendas 
				,SUM(CASE WHEN ft.tipodoc=3 THEN 1 ELSE 0 END)			AS dev 		
				,LTRIM(RTRIM(fi.campanhas))								AS campanhas
				, SUM(CASE WHEN ft.tipodoc = 3 THEN -qtt ELSE qtt END)	AS Qtt				
				, SUM(etiliquido)										AS etiliquido
				,descvalor	=  CASE WHEN ft.tipodoc!=3 THEN ((CASE 
								WHEN fi.desconto = 100 
									THEN fi.epv * fi.qtt 
								WHEN (fi.desconto BETWEEN 0.01 AND 99.99) 
									THEN (((fi.epv * fi.qtt) - ABS(etiliquido)) * -1)
								ELSE 0 
								END)
								-u_descval
								) ELSE
									ABS(((CASE 
									WHEN fi.desconto = 100 
										THEN fi.epv * fi.qtt 
									WHEN (fi.desconto BETWEEN 0.01 AND 99.99) 
										THEN (((fi.epv * fi.qtt) - ABS(etiliquido)) * -1)
									ELSE 0 
									END)
									-u_descval
									))
								END
				, SUM(fi.pontos)										AS pontos	
				,fi.lrecno	                                            As recno	
			INTO
				#VendasCampanhas
			FROM 
				ft(NOLOCK)
			INNER JOIN fi(nolock) ON ft.ftstamp = fi.ftstamp
			WHERE 
				LEN(fi.campanhas) > 0 					
				AND fdata BETWEEN ''' + CONVERT(VARCHAR,@dataIni) + ''' AND ''' + CONVERT(VARCHAR,@dataFim) + ''' 
				AND vendedor >= ' + CONVERT(VARCHAR,@opIni) + ' AND vendedor <= ' + CONVERT(VARCHAR,@opFim) + '
				--and	site = (CASE WHEN ''' + @site + ''' = '''' THEN site ELSE ''' + @site + ''' END )	
				AND site =  COALESCE(NULLIF(''' + @site + ''',''''), site) 
			'
					
	IF @ref != '' 
	BEGIN
		SET @stmt =  @stmt + N' AND fi.ref = COALESCE(NULLIF(''' + @ref + ''',''''), ref) '
	END
		
	IF @campanha != '' 
	BEGIN			
		DECLARE @v VARCHAR(240)	
		DECLARE @v2 VARCHAR(240)	
		
		SET @v = (SELECT '''%,' + items + '-%'' OR fi.campanhas like ' FROM dbo.up_splitToTable(@campanha, ',') FOR XML PATH(''))
		--remover o ultimo or fi.campanhas like
		SET @v = LEFT(@v,  LEN(@v)-20)
		SET @v2 = REPLACE(@v,'%,','')		
		SET @stmt =  @stmt + N' AND ( fi.campanhas LIKE ' + @v + ' OR fi.campanhas like ' + @v2 + ' )'
	END	
	

SET @stmt =  @stmt + N'
						GROUP BY 		
							ft.fdata
							, ft.ftstamp
							, ft.fno
							, ft.nmdoc
							, fi.ref
							, fi.design
							, ft.vendnm
							, ft.vendedor
							, ft.ousrinis
							, ft.site 
							, ft.pnome
							, ft.pno
							, fi.campanhas
							, fi.desconto
							, fi.epv
							, fi.qtt
							, fi.u_descval
							, etiliquido
							, fi.lrecno
							, ft.tipodoc				
						ORDER BY 
							fdata DESC
						'
	
SET @stmt =  @stmt + N' SELECT * FROM #VendasCampanhas '
	
Print(@stmt)
exec(@stmt)

/* Elimina Tabelas */
IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#VendasCampanhas'))
	DROP TABLE #VendasCampanhas
	

GO
Grant Execute on dbo.up_relatorio_vendas_campanhas to Public
Grant control on dbo.up_relatorio_vendas_campanhas to Public
GO