/* Relatório Cartão Familia Cliente

	 exec up_relatorio_vendas_FamiliaCartaoCliente '20110101','20120101',''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_vendas_FamiliaCartaoCliente]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_FamiliaCartaoCliente
go

create procedure dbo.up_relatorio_vendas_FamiliaCartaoCliente
@dataIni as datetime,
@dataFim as datetime,
@site varchar(55)
/* WITH ENCRYPTION */ 
AS
BEGIN
;with 
	cte1 (clstamp) as (
		SELECT	clstamp 
		FROM	B_fidel (nolock)
	),
	cte2 (ref,nome) as (
		SELECT ref,nome FROM STFAMI (nolock) 
	)
	Select	'FAMILIA'			= ISNULL((Select nome from cte2 Where cte2.ref= familia),''),
			/* QTT*/
			'QTCCARTAO'			= ROUND(SUM(QTCCARTAO),2),
			'QTSCARTAO'			= ROUND(SUM(QTSCARTAO),2),
			'QTTOTAL'			= ROUND(SUM(QTCCARTAO) + SUM(QTSCARTAO),2),
			'PERC_QTCCARTAO'	= CASE WHEN (SUM(QTCCARTAO) + SUM(QTSCARTAO)) > 0 THEN
											ROUND((SUM(QTCCARTAO)*100) / (SUM(QTCCARTAO) + SUM(QTSCARTAO)),2)
										ELSE 0
								END,
			'PERC_QTSCARTAO'	= 100 - 
								  CASE WHEN (SUM(QTCCARTAO) + SUM(QTSCARTAO)) > 0 THEN
										ROUND((SUM(QTCCARTAO)*100) / (SUM(QTCCARTAO) + SUM(QTSCARTAO)),2)
									   ELSE 0
								END,
			/*VALOR*/
			'VALORCCARTAO'		= ROUND(SUM(VALORCCARTAO),2),
			'VALORSCARTAO'		= ROUND(SUM(VALORSCARTAO),2),
			'VALORTOTAL'		= ROUND(SUM(VALORCCARTAO) + SUM(VALORSCARTAO),2),
			'PERC_VALORCCARTAO'	= CASE WHEN (SUM(VALORCCARTAO) + SUM(VALORSCARTAO)) > 0 THEN 
											ROUND((SUM(VALORCCARTAO)*100) / (SUM(VALORCCARTAO) + SUM(VALORSCARTAO)),2)
										ELSE 0
									END,
			'PERC_VALORSCARTAO'	= 100 - 
								(CASE WHEN (SUM(VALORCCARTAO) + SUM(VALORSCARTAO)) > 0 THEN
									ROUND((SUM(VALORCCARTAO)*100) / (SUM(VALORCCARTAO) + SUM(VALORSCARTAO)),2)
									ELSE 0
								END)
			,'BB_CCARTAO' = SUM(CASE WHEN TEMCARTAO = 'CARTAO' THEN (etiliquido+u_ettent1+u_ettent2)-epcp ELSE 0 END)
			,'BB_SCARTAO' = SUM(CASE WHEN TEMCARTAO = 'NCARTAO' THEN (etiliquido+u_ettent1+u_ettent2)-epcp ELSE 0 END)
	FROM(		
		Select	'TEMCARTAO'		= (CASE WHEN (select COUNT(clstamp) from cte1 Where cte1.clstamp = b_utentes.utstamp) > 0 THEN 'CARTAO' ELSE 'NCARTAO' END),
				'QTCCARTAO'		= (CASE WHEN (select COUNT(clstamp) from cte1 Where cte1.clstamp = b_utentes.utstamp) > 0 THEN qtt ELSE 0 END),
				'QTSCARTAO'		= (CASE WHEN (select COUNT(clstamp) from cte1 Where cte1.clstamp = b_utentes.utstamp) > 0 THEN 0 ELSE qtt END),
				'VALORCCARTAO'	= (CASE WHEN (select COUNT(clstamp) from cte1 Where cte1.clstamp = b_utentes.utstamp) > 0 THEN etiliquido + u_ettent1 + u_ettent2 ELSE 0 END),
				'VALORSCARTAO'	= (CASE WHEN (select COUNT(clstamp) from cte1 Where cte1.clstamp = b_utentes.utstamp) > 0 THEN 0 ELSE etiliquido + u_ettent1 + u_ettent2 END),
				FAMILIA
				,etiliquido	= CASE WHEN Fi.IVAINCL = 0
							THEN FI.ETILIQUIDO
							ELSE FI.ETILIQUIDO / ((Fi.iva / 100)+1)
						END
				,u_ettent1 = CASE WHEN Ft.tipodoc = 3 THEN
						CASE WHEN Fi.IVAINCL = 0
								THEN u_ettent1 * -1
								ELSE (u_ettent1 / ((Fi.iva / 100)+1)) * (-1)
							END
						ELSE
							CASE WHEN Fi.IVAINCL = 0
								THEN u_ettent1 
								ELSE (u_ettent1 / ((Fi.iva / 100)+1))
							END
						END
				,u_ettent2 = CASE WHEN Ft.tipodoc = 3 THEN
						CASE WHEN Fi.IVAINCL = 0
								THEN u_ettent2 * -1
								ELSE (u_ettent2 / ((Fi.iva / 100)+1)) * (-1)
							END
						ELSE
							CASE WHEN Fi.IVAINCL = 0
								THEN u_ettent2 
								ELSE (u_ettent2 / ((Fi.iva / 100)+1))
							END
						END
				,fi.epcp
		From	b_utentes (nolock)
		INNER	JOIN FT (nolock) ON FT.NO = b_utentes.no and ft.estab = b_utentes.estab
		INNER	JOIN FI (nolock) ON ft.ftstamp = fi.ftstamp
		Where	ft.fdata 
				between @dataIni and @dataFim
				and ft.no > 199
				and ft.site = (case when @site = '' Then ft.site else @site end)
	) N
	Group By FAMILIA
	ORDER By FAMILIA
END
GO
Grant Execute on dbo.up_relatorio_vendas_FamiliaCartaoCliente to Public
Grant control on dbo.up_relatorio_vendas_FamiliaCartaoCliente to Public
Go