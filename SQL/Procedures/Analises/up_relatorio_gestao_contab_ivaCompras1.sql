-- Relatório Contabilidade Resumo Iva Compras
-- exec up_relatorio_gestao_contab_ivaCompras1 '20100101','20130101',''

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_contab_ivaCompras1]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_contab_ivaCompras1
go

create procedure dbo.up_relatorio_gestao_contab_ivaCompras1

@dataIni as date,
@dataFim as date,
@documentos as varchar(254)

/* WITH ENCRYPTION */ 

AS
BEGIN

declare @comando varchar(max)

Set @comando = 
'select	year(fo.docdata) as Ano,
		month(fo.docdata) as Mes, 
		fo.docnome as Documento,
		fo.adoc,
		fo.nome,
		fo.no,
		fn.iva,
		sum( case when cm1.debito=0 and fn.ivaincl=0	then  ROUND((FN.ETILIQUIDO*(FN.iva/100)),2)
			 when cm1.debito=1 and fn.ivaincl=0			then -ROUND((FN.ETILIQUIDO*(FN.iva/100)),2)
			 when cm1.debito=0 and fn.ivaincl=1			then  ROUND((fn.etiliquido - (FN.ETILIQUIDO/(FN.iva/100+1))),2)
			 when cm1.debito=1 and fn.ivaincl=1			then -ROUND((fn.etiliquido - (FN.ETILIQUIDO/(FN.iva/100+1))),2)
			 else 0 end
		) as VALORIVA,
		sum( case when cm1.debito=0 and fn.ivaincl=0	then  ROUND(FN.ETILIQUIDO,2)
			 when cm1.debito=1 and fn.ivaincl=0			then -ROUND(FN.ETILIQUIDO,2)
			 when cm1.debito=0 and fn.ivaincl=1			then  ROUND((FN.ETILIQUIDO/(FN.iva/100+1)),2)
			 when cm1.debito=1 and fn.ivaincl=1			then -ROUND((FN.ETILIQUIDO/(FN.iva/100+1)),2)
			 else 0 end
		) as BASEINC
from fn (nolock) 
inner join fo (nolock)  on fo.fostamp=fn.fostamp
inner join cm1 (nolock) on cm1.cm=fo.doccode
where	fo.docdata between '+char(39)+convert(varchar(102),@dataIni)+char(39)+' AND '+char(39)+convert(varchar(102),@dataFim)+char(39)+' 
		and fo.doccode != 101
		and convert(varchar,cm1.cm) in ('+char(39)+REPLACE(@documentos,'_',char(39)+','+CHAR(39))+char(39)+')
group by year(fo.docdata), month(fo.docdata), fo.doccode, fo.docnome,fo.adoc,fo.nome,fo.no,fn.iva
order by ano, mes, fo.docnome, fo.adoc'

exec sp_executesql @comando
				
END

GO
Grant Execute on dbo.up_relatorio_gestao_contab_ivaCompras1 to Public
Grant control on dbo.up_relatorio_gestao_contab_ivaCompras1 to Public
Go
