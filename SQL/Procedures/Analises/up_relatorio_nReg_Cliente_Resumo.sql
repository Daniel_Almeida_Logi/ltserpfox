/*
	exec up_relatorio_nReg_Cliente_Resumo '19000101', '20231212', 0, 'Loja 1', '', '', 1, 0
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
--code to validate if the procedure exists, if exists, drops procedure
if OBJECT_ID('[dbo].[up_relatorio_nReg_Cliente_Resumo]') IS NOT NULL
	drop procedure dbo.up_relatorio_nReg_Cliente_Resumo
go
Create PROCEDURE [dbo].[up_relatorio_nReg_Cliente_Resumo]
	@dataIni			DATETIME,
	@dataFim			DATETIME,
	@no					NUMERIC(10),
	@site				VARCHAR(55),
	@tipo				VARCHAR(20),
	@suspensas			VARCHAR(50)='',
	@excluiEntidades	BIT	= 0,
	@saldoData			BIT = 0
/* with encryption */
AS
SET NOCOUNT ON



--print 'declaring variables'

Declare @sqlCommand						varchar (8000)		= '',
		@sqlCommandPart1				varchar (8000)		= '',
		@sqlCommandPart2				varchar (8000)		= '',
		@sqlCommandPart3				varchar (8000)		= '',
		@sqlCommandPart4				varchar (8000)		= '',
		@sqlDropTemTables				varchar (8000)		= '',
		@B_utentesTipo					varchar(100)		= '',
		@B_utentesNo					varchar(100)		= '',
		@ccNo							varchar(100)		= '',
		@dataIniD						varchar(100)		= convert(varchar, @dataini, 120),
		@dataFimD						varchar(100)		= convert(varchar, @dataFim, 120),
		@saldoDataB						varchar(1)			= convert(varchar, @saldoData),
		@suspensasV						varchar(100)		= '',
		@siteV							varchar(400)		= ''

print 'delete temp tables'
set @sqlDropTemTables = '
IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N''tempdb.dbo.#dadosRL''))
	DROP TABLE #dadosRL
IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N''tempdb.dbo.#dadosRL''))
	DROP TABLE #cteDados
IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N''tempdb.dbo.#cteDadosSTOCKDATA''))
	DROP TABLE #cteDadosSTOCKDATA
IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N''tempdb.dbo.#dadosRLAcertos''))
	DROP TABLE #dadosRLAcertos'

--print '@tipo'

if @tipo != '' 
begin
	set @B_utentesTipo = 'and b_utentes.tipo = ''' + @tipo +''''
end 

--print '@excluiEntidades'

if @excluiEntidades = 1 
begin 
	set @B_utentesNo = 'and b_utentes.no > 200'
end
else
begin
	set @B_utentesNo = 'and b_utentes.no > b_utentes.no - 1'
end 

--print '@no'

if @no != 0 
begin
	set @ccNo = 'and cc.no = ' + convert(varchar, @no)
end

--print '@suspensasV'

if @suspensas = 'Suspensas'
begin
	set @suspensasV = ' and  ft.cobrado = 1'
end
if @suspensas = 'Não Suspensas'
begin
	set @suspensasV = ' and  ft.cobrado = 0'
end 

--print '@siteV'

if @site != ''
begin
	set @siteV = 'and ( ft.site = ''' + @site + ''' or re.site = ''' + @site + ''' or cc.site = ''' + @site + ''')' 
end

print '@sqlCommandPart1'

set @sqlCommandPart1 = ' 
--@sqlCommandPart1
 SELECT 
	cc.ccstamp			AS ccstamp,
	ABS(SUM(rl.erec))	AS total
INTO
	#dadosRLAcertos
FROM  
	cc (nolock)	
LEFT JOIN 
	rl (nolock)	ON rl.ccstamp=cc.ccstamp
WHERE
	rl.dataven <= '''' + GETDATE() + '''' and cc.nrdoc=0
group by 
	cc.ccstamp

CREATE INDEX ix_dadosRLAcertos_ccstamp
	ON #dadosRLAcertos (ccstamp);
CREATE INDEX ix_dadosRLAcertos_total
	ON #dadosRLAcertos (total);

IF(' + @saldoDataB + ' =1)
BEGIN 
	SELECT 
		cc.ccstamp				AS ccstamp,
		ABS(SUM(rl.erec))		AS total
	INTO 
		#dadosRL
	FROM 
		ft (nolock)
		INNER JOIN 
			cc		(nolock)	ON cc.ftstamp=ft.ftstamp
		LEFT JOIN 
			rl		(nolock)	ON rl.ccstamp=cc.ccstamp
	WHERE
		rdata <= ''' + @dataFimD + ''' AND ''' + @saldoDataB + ''' =1
	 group by 
		cc.ccstamp

	CREATE INDEX ix_dadosRL_ccstamp
		ON #dadosRL (ccstamp);
	CREATE INDEX ix_dadosRL_total
		ON #dadosRL (total);

	select 	
		ROW_NUMBER() over(order by cc.datalc, cc.ousrhora, cc.cm, cc.nrdoc asc)									AS numero,
		RTRIM(ft.nome) + ''['' + convert(varchar,ft.no)+ '']''+ ''['' + convert(varchar,ft.estab)+ '']''				AS nome,
		cc.ccstamp																								AS CCSTAMP, 
		convert(varchar,cc.datalc,102)																			AS DATALC,
		convert(varchar,cc.dataven,102)																			AS DATAVEN,
		cc.edeb																									AS EDEB,
		cc.ecred																								AS ECRED,
		cc.edebf																								AS EDEBF,
		cc.ecredf																								AS ECREDF,
		(cc.edeb-cc.edebf)-(cc.ecred-cc.ecredf)																	AS VALOR,
		cc.moeda																								AS MOEDA,
		cc.cmdesc																								AS CMDESC,
		cc.nrdoc																								AS NRDOC,
		cc.ultdoc																								AS ULTDOC,
		cc.intid																								AS INTID,
		cc.faccstamp																							AS FACSTAMP,
		cc.cbbno																								AS CBBNO,
		cc.origem																								AS ORIGEM,
		cc.ousrhora																								AS OUSRHORA,
		cc.ousrinis																								AS OUSRINIS,
		datediff(d,cc.datalc,GETDATE())																			AS IEMISSAO,
		datediff(d,cc.datalc,GETDATE())																			AS IVENCIMENTO,
		isnull(ft.site,re.site)																					AS SITE,
		ft.tpdesc,
		ft.no,
		ft.estab,
		ft.nome																									AS FTNOME
	INTO #cteDadosSTOCKDATA
	from
		ft (nolock)
		left join cc (nolock) on cc.ftstamp=ft.ftstamp
		left join re (nolock) on re.restamp = cc.restamp
		left join b_utentes (nolock) on cc.no = b_utentes.no and cc.estab = b_utentes.estab 
'	

print '@sqlCommandPart2'

set @sqlCommandPart2 = ' 
	--@sqlCommandPart2
	where
		(abs((cc.ecred-cc.ecredf)-(cc.edeb-cc.edebf)) > 0.010000)
		and ft.anulado is not null and  cc.restamp	!=''''
		and ft.fdata between ''' + @dataIniD + ''' and ''' + @dataFimD + ''''+
		@B_utentesTipo + ' ' +
		@B_utentesNo + ' ' +
		@ccNo + ' ' +
		@suspensasV + ' ' +
		@siteV + '
	union all 
		select 	
			ROW_NUMBER() over(order by cc.datalc, cc.ousrhora, cc.cm, cc.nrdoc asc)																AS numero,						
			RTRIM(ft.nome) + ''['' + convert(varchar,ft.no)+ '']''+ ''['' + convert(varchar,ft.estab)+ '']''									AS nome,
			cc.ccstamp																															AS CCSTAMP, 
			convert(varchar,cc.datalc,102)																										AS DATALC,
			convert(varchar,cc.dataven,102)																										AS DATAVEN,
			cc.edeb																																AS EDEB,
			cc.ecred																															AS ECRED,
			case when cc.edeb>0 then isnull(#dadosRL.total,0) else 0 end 																		AS EDEBF,
			case when cc.ecred>0 then isnull(#dadosRL.total,0) else 0 end 																		AS ECREDF,
			case when ft.tipodoc=3 then (-cc.ecred)+isnull(#dadosRL.total,0) else (cc.ecred+cc.edeb)-isnull(#dadosRL.total,0) end				AS VALOR,
			cc.moeda																															AS MOEDA,
			cc.cmdesc																															AS CMDESC,
			cc.nrdoc																															AS NRDOC,
			cc.ultdoc																															AS ULTDOC,
			cc.intid																															AS INTID,
			cc.faccstamp																														AS FACSTAMP,
			cc.cbbno																															AS CBBNO,
			cc.origem																															AS ORIGEM,
			cc.ousrhora																															AS OUSRHORA,
			cc.ousrinis																															AS OUSRINIS,
			datediff(d,cc.datalc,GETDATE())																										AS IEMISSAO,
			datediff(d,cc.datalc,GETDATE())																										AS IVENCIMENTO,
			isnull(ft.site,re.site)																												AS SITE,
			ft.tpdesc,
			ft.no,
			ft.estab,
			ft.nome																																AS FTNOME	
		from
			ft (nolock)
			INNER JOIN cc (nolock) on cc.ftstamp=ft.ftstamp
			LEFT JOIN re (nolock) on re.restamp = cc.restamp
			LEFT JOIN b_utentes (nolock) on cc.no = b_utentes.no and cc.estab = b_utentes.estab 
			LEFT JOIN #dadosRL (nolock)	ON #dadosRL.ccstamp=cc.ccstamp
		where
			((cc.ecred+cc.edeb)-isnull(#dadosRL.total,0) > 0.010000 or(ft.tipodoc=3 and (-cc.ecred)+isnull(#dadosRL.total,0)<0.0000))
			and ft.anulado is not null  and  cc.restamp=''''  AND ' + @saldoDataB + ' =1
			and ft.fdata between ''' + @dataIniD + ''' and ''' + @dataFimD + '''' +
			@B_utentesTipo + ' ' +
			@B_utentesNo + ' ' +
			@ccNo + ' ' +
			@suspensasV + ' ' +
			@siteV + ' 
			'

--print '@sqlCommandPart3'

set @sqlcommandpart3 = ' 
	--@sqlcommandpart3
	union all 
		select 	
			ROW_NUMBER() over(order by cc.datalc, cc.ousrhora, cc.cm, cc.nrdoc asc)									AS numero,
			RTRIM(cc.nome) + ''['' + convert(varchar,cc.no)+ '']''+ ''['' + convert(varchar,cc.estab)+ '']''		AS nome,
			cc.ccstamp																								AS CCSTAMP, 
			convert(varchar,cc.datalc,102)																			AS DATALC,
			convert(varchar,cc.dataven,102)																			AS DATAVEN,
			cc.edeb																									AS EDEB,
			cc.ecred																								AS ECRED,
			cc.edebf																								AS EDEBF,
			cc.ecredf																								AS ECREDF,
			(-cc.ecred)+isnull(#dadosRLAcertos.total,0)																AS VALOR,
			cc.moeda																								AS MOEDA,
			cc.cmdesc																								AS CMDESC,
			cc.nrdoc																								AS NRDOC,
			cc.ultdoc																								AS ULTDOC,
			cc.intid																								AS INTID,
			cc.faccstamp																							AS FACSTAMP,
			cc.cbbno																								AS CBBNO,
			cc.origem																								AS ORIGEM,
			cc.ousrhora																								AS OUSRHORA,
			cc.ousrinis																								AS OUSRINIS,
			datediff(d,cc.datalc,GETDATE())																			AS IEMISSAO,
			datediff(d,cc.datalc,GETDATE())																			AS IVENCIMENTO,
			''''																									AS SITE,
			'''',
			cc.no,
			cc.estab,
			cc.nome																									AS FTNOME
		from  cc (nolock)
			LEFT JOIN b_utentes (nolock) ON cc.no = b_utentes.no and cc.estab = b_utentes.estab 
			LEFT JOIN #dadosRLAcertos (nolock)	 ON #dadosRLAcertos.ccstamp=cc.ccstamp
		where
			 cc.ftstamp =''''  AND (-cc.ecred)+isnull(#dadosRLAcertos.total,0) < 0.00 and nrdoc=0 ' + 
			@B_utentesTipo + ' ' +
			@B_utentesNo + ' ' +
			@ccNo + '		


	Select 
		*, saldo = 
			isnull((Select 
						sum((-cc.edeb+cc.ecred)+total) 
					From 
						(SELECT	
							cc.ccstamp
							,ROW_NUMBER() over(order by cc.datalc,cc.ousrhora, cc.cm, cc.nrdoc) as numero
							,edeb
							,ecred
							,#dadosRL.total
						FROM
							cc					(nolock)
							left join ft		(nolock) on cc.ftstamp=ft.ftstamp
							left join #dadosRL	(nolock) on cc.ccstamp= #dadosRL.ccstamp
						WHERE	
						--	RTRIM(ft.nome) + ''['' + convert(varchar,ft.no)+ '']''+ ''['' + convert(varchar,ft.estab)+ '']'' = #cteDadosSTOCKDATA.nome
							ft.estab = #cteDadosSTOCKDATA.estab 
							and ft.no = #cteDadosSTOCKDATA.no
							and ft.nome = #cteDadosSTOCKDATA.ftnome								
							and (abs((cc.ecred-cc.ecredf)-(cc.edeb-cc.edebf)) > 0.010000)
							and ft.anulado=0
						) a 
						inner join cc (nolock) on cc.ccstamp = a.ccstamp
					Where 
						a.numero <= #cteDadosSTOCKDATA.numero
									and cc.dataven between ''' + @dataIniD + ''' and ''' + @dataFimD + '''
			),0)
	From	
		#cteDadosSTOCKDATA (nolock)
	order by 
		nome, numero
END'

set @sqlcommandpart4 = '
--@sqlcommandpart4
ELSE 
BEGIN 
	select 	
		ROW_NUMBER() over(order by cc.datalc, cc.ousrhora, cc.cm, cc.nrdoc asc)										AS numero,
		RTRIM(ft.nome) + ''['' + convert(varchar,ft.no)+ '']''+ ''['' + convert(varchar,ft.estab)+ '']''			AS nome,
		cc.ccstamp																									AS CCSTAMP, 																						
		convert(varchar,cc.datalc,102)																				AS DATALC,
		convert(varchar,cc.dataven,102)																				AS DATAVEN,
		cc.edeb																										AS EDEB,
		cc.ecred																									AS ECRED,
		cc.edebf																									AS EDEBF,
		cc.ecredf																									AS ECREDF,
		(cc.edeb-cc.edebf)-(cc.ecred-cc.ecredf)																		AS VALOR,
		cc.moeda																									AS MOEDA,
		cc.cmdesc																									AS CMDESC,
		cc.nrdoc																									AS NRDOC,
		cc.ultdoc																									AS ULTDOC,
		cc.intid																									AS INTID,
		cc.faccstamp																								AS FACSTAMP,
		cc.cbbno																									AS CBBNO,
		cc.origem																									AS ORIGEM,
		cc.ousrhora																									AS OUSRHORA,
		cc.ousrinis																									AS OUSRINIS,
		datediff(d,cc.datalc,GETDATE())																				AS IEMISSAO,
		datediff(d,cc.datalc,GETDATE())																				AS IVENCIMENTO,
		isnull(ft.site,re.site)																						AS SITE,
		ft.tpdesc,
		ft.no,
		ft.estab,
		ft.nome																										AS FTNOME
	INTO #cteDados
	from
		ft (nolock)
		left join cc (nolock) on cc.ftstamp=ft.ftstamp
		left join re (nolock) on re.restamp = cc.restamp
		left join b_utentes (nolock) on cc.no = b_utentes.no and cc.estab = b_utentes.estab 
	where
		(abs((cc.ecred-cc.ecredf)-(cc.edeb-cc.edebf)) > 0.010000)
		and ft.anulado is not null
		and ft.fdata between ''' + @dataIniD + ''' and ''' + @dataFimD + '''' +
		@B_utentesTipo + ' ' +
		@B_utentesNo + ' ' +
		@ccNo + ' ' +
		@suspensasV + ' ' +
		@siteV + '
	union all 
		select 	
			ROW_NUMBER() over(order by cc.datalc, cc.ousrhora, cc.cm, cc.nrdoc asc)										AS numero,
			RTRIM(cc.nome) + ''['' + convert(varchar,cc.no)+ '']''+ ''['' + convert(varchar,cc.estab)+ '']''			AS nome,
			cc.ccstamp 																									AS CCSTAMP, 	
			convert(varchar,cc.datalc,102)																				AS DATALC,
			convert(varchar,cc.dataven,102)																				AS DATAVEN,
			cc.edeb																										AS EDEB,
			cc.ecred																									AS ECRED,
			cc.edebf																									AS EDEBF,
			cc.ecredf																									AS ECREDF,
			(-cc.ecred)+isnull(#dadosRLAcertos.total,0)																	AS VALOR,
			cc.moeda																									AS MOEDA,
			cc.cmdesc																									AS CMDESC,
			cc.nrdoc																									AS NRDOC,
			cc.ultdoc																									AS ULTDOC,
			cc.intid																									AS INTID,
			cc.faccstamp																								AS FACSTAMP,
			cc.cbbno																									AS CBBNO,
			cc.origem																									AS ORIGEM,
			cc.ousrhora																									AS OUSRHORA,
			cc.ousrinis																									AS OUSRINIS,
			datediff(d,cc.datalc,GETDATE())																				AS IEMISSAO,
			datediff(d,cc.datalc,GETDATE())																				AS IVENCIMENTO,
			''''																										AS SITE,
			'''',
			cc.no,
			cc.estab,
			cc.nome																										AS FTNOME
		from  cc (nolock)
			LEFT JOIN b_utentes (nolock) ON cc.no = b_utentes.no and cc.estab = b_utentes.estab 
			LEFT JOIN #dadosRLAcertos (nolock)	 ON #dadosRLAcertos.ccstamp=cc.ccstamp
		where
			cc.ftstamp =''''  AND (-cc.ecred)+isnull(#dadosRLAcertos.total,0) < 0.00 and nrdoc=0 '
			+ @B_utentesTipo + ' ' +
			@B_utentesNo + ' ' +
			@ccNo + '
	Select
		*, saldo = 
		isnull((Select 
					sum((cc.edeb-cc.edebf)-(cc.ecred-cc.ecredf)) 
				From 
					(SELECT	
						cc.ccstamp
						,ROW_NUMBER() over(order by cc.datalc,cc.ousrhora, cc.cm, cc.nrdoc) as numero
						,edeb
						,ecred
					FROM
						cc (nolock)
						left join ft (nolock) on cc.ftstamp=ft.ftstamp
					WHERE	
						--RTRIM(ft.nome) + ''['' + convert(varchar,ft.no)+ '']''+ ''['' + convert(varchar,ft.estab)+ '']'' = #cteDados.nome
						ft.estab = #cteDados.estab 
						and ft.no = #cteDados.no		
						and ft.nome = #cteDados.ftnome	
						and (abs((cc.ecred-cc.ecredf)-(cc.edeb-cc.edebf)) > 0.010000)
						and ft.anulado=0
					) a 
					inner join cc (nolock) on cc.ccstamp = a.ccstamp
				Where 
					a.numero <= #cteDados.numero
								and cc.dataven between ''' + @dataIniD + ''' and ''' + @dataFimD + '''
		),0)
	From	
		#cteDados (nolock)
	order by 
		nome, numero
END
'

print @sqlDropTemTables
print @sqlCommandPart1 
print @sqlCommandPart2 
print @sqlcommandpart3 
print @sqlCommandPart4
print @sqlDropTemTables

--print 'executing SQL...'

EXEC(@sqlDropTemTables + @sqlCommandPart1 + @sqlCommandPart2 + @sqlcommandpart3  + @sqlCommandPart4 + @sqlDropTemTables)

--print 'SQL executed'
 
