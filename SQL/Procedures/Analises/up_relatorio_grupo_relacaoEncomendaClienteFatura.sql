/*
	Encomendas Não Facturadas
	exec up_relatorio_grupo_relacaoEncomendaClienteFatura '20140701','20150101','Keiretsu','',0,0,0,'Loja 1'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_grupo_relacaoEncomendaClienteFatura]') IS NOT NULL
	drop procedure dbo.up_relatorio_grupo_relacaoEncomendaClienteFatura
go

create procedure dbo.up_relatorio_grupo_relacaoEncomendaClienteFatura
@dataIni as datetime
,@dataFim as datetime
,@tipo as varchar(20)
,@u_lab as varchar(150)
,@no numeric(10,0)
,@fechada as int
,@obrano as numeric(10,0)
,@site as varchar(60)
/* WITH ENCRYPTION */
AS
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempEncClientes'))
	BEGIN
		DROP TABLE #TempEncClientes
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempfaturaClientes'))
	BEGIN
		DROP TABLE #TempfaturaClientes
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempConfEntregaClientes'))
	BEGIN
		DROP TABLE #TempConfEntregaClientes
	END
	select 
		bi.ref
		,bi.bistamp
		,PVP_encCliente = bi.edebito
		,QTT_encCliente = bi.qtt
		,bo.dataobra
		,bo.ousrhora
		,bo.nmdos
		,bo.nome
		,bo.no
		,bo.estab
		,bo.obrano
		,estado = case 
					when bo.fechada = 1 then 'FECHADA' 
					ELSE 'ABERTA' 
				END
		,st.u_lab
		,st.design
		,b_utentes.tipo
	INTO 
		#TempEncClientes
	from 
		bi (nolock)
		inner join bo (nolock) on bi.bostamp = bo.bostamp
		left join st (nolock) on st.ref = bi.ref
		left join b_utentes (nolock) on b_utentes.no = bo.no and b_utentes.estab = bo.estab
	where 
		bo.ndos = 41
		and bo.dataobra between @dataIni and @dataFim
		and bo.no = case when @no = 0 then bo.no else @no end
		and bo.obrano = case when @obrano = 0 then bo.obrano else @obrano end
		and bo.fechada = case 
						when @fechada = -1 then bo.fechada 
						else @fechada 
					end
		and isnull(b_utentes.tipo,'') = case when @tipo = '' then isnull(b_utentes.tipo,'') else @tipo end
		and st.u_lab = case when @u_lab = '' then st.u_lab else @u_lab end
		and bo.site = case when @site = '' then bo.site else @site end
	select 
		ref
		,bi.bistamp
		,bi.obistamp
		,bo.dataobra 
		,bo.nome
		,bo.no 
		,bo.estab 
		,bo.obrano
		,bo.nmdos 
	INTO 
		#TempConfEntregaClientes
	from 
		bi (nolock)
		inner join bo (nolock) on bi.bostamp = bo.bostamp
	where 
		bo.ndos = 43
		and bo.site = case when @site = '' then bo.site else @site end
	Select 
		fi.ref
		,TotalLinha_faturaCliente = 
			CASE WHEN Fi.IVAINCL = 0
						THEN FI.ETILIQUIDO
						ELSE FI.ETILIQUIDO / ((Fi.iva / 100)+1)
					END
		,PVP_faturaCliente = fi.epv
		,QTT_faturaCliente = case when ft.tipodoc = 3 then fi.qtt*-1 else fi.qtt end
		,ft.fdata
		,ft.ousrhora
		,ano = year(ft.fdata)
		,mes = month(ft.fdata)
		,ft.nome
		,ft.no
		,ft.estab
		,ft.tipo
		,fi.bistamp
		,ft.site
		,ft.fno
		,ft.nmdoc
	INTO 
		#TempfaturaClientes
	from 
		fi (nolock)
		inner join ft (nolock) on ft.ftstamp = fi.ftstamp
	where
		ft.tipodoc != 4
		and ft.site = case when @site = '' then ft.site else @site end
	Select 
		/* Encomenda */
		Encomenda = #TempEncClientes.nmdos	
		,EncObrano = #TempEncClientes.obrano
		,EncData = #TempEncClientes.dataobra
		,EncEstado = #TempEncClientes.estado
		/* Cliente */
		,nome = #TempEncClientes.nome
		,no = #TempEncClientes.no
		,estab = #TempEncClientes.estab
		,tipo = RTRIM(LTRIM(isnull(#TempEncClientes.tipo,'')))
		/* Produto */
		,#TempEncClientes.ref
		,#TempEncClientes.design
		,u_lab =#TempEncClientes.u_lab
		/* Confirmacao */
		,docConfirmEntrega = isnull(#TempConfEntregaClientes.nmdos,0)
		,data_ConfirmEntrega = isnull(#TempConfEntregaClientes.dataobra,'')
		,Numero_ConfirmEntrega = isnull(#TempConfEntregaClientes.obrano,0)
		/* Faturacao */
		,docCliente = case when isnull(#TempfaturaClientes.nmdoc,'') = '' then isnull(a.nmdoc,'') else isnull(#TempfaturaClientes.nmdoc,'') end
		,data_faturaCliente = case when isnull(#TempfaturaClientes.fdata,'') = '' then isnull(a.fdata,'') else isnull(#TempfaturaClientes.fdata,'') end
		,Numero_faturaCliente = case when isnull(#TempfaturaClientes.fno,0) = 0 then isnull(a.fno,0) else isnull(#TempfaturaClientes.fno,0) end 
		/*Quantidade*/
		,QTT_encCliente	= #TempEncClientes.QTT_encCliente
		,QTT_faturaCliente = isnull(#TempfaturaClientes.QTT_faturaCliente,0) + isnull(a.QTT_faturaCliente,0)
		/* Precos */
		,PVP_encCliente 
		,PVP_faturaCliente = isnull(#TempfaturaClientes.PVP_faturaCliente,0) + isnull(a.PVP_faturaCliente,0)
	from 
		#TempEncClientes
		left join #TempConfEntregaClientes on #TempConfEntregaClientes.obistamp = #TempEncClientes.bistamp
		left join #TempfaturaClientes on #TempConfEntregaClientes.bistamp = #TempfaturaClientes.bistamp
		left join #TempfaturaClientes as a on #TempEncClientes.bistamp = a.bistamp
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempEncClientes'))
	BEGIN
		DROP TABLE #TempEncClientes
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempfaturaClientes'))
	BEGIN
		DROP TABLE #TempfaturaClientes
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempConfEntregaClientes'))
	BEGIN
		DROP TABLE #TempConfEntregaClientes
	END
		
GO
Grant Execute On dbo.up_relatorio_grupo_relacaoEncomendaClienteFatura to Public
Grant Control On dbo.up_relatorio_grupo_relacaoEncomendaClienteFatura to Public
Go

---------------------------------------------------------------
