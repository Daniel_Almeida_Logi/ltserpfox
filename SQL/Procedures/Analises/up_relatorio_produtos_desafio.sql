/* Relatório Detalhe relatorio produtos desafio

	exec up_relatorio_produtos_desafio '20171101', '20171130', '', '', '', '', '', '', 'Loja 1', '', '', '' 

	exec up_relatorio_produtos_desafio '20171101', '20171130', '', '', '', '', '', 'MSRM', 'Loja 1', '', '', ''

	exec up_relatorio_produtos_desafio '20200924', '20200924', '', '', '', '', '', '', 'Loja 1', '', '', ''
	exec up_relatorio_produtos_desafio '20211010', '20211014', '', '', 'Generis;Mylan', '', '', 'MSRM', 'Loja 1', '', '', '',0,''


	exec up_relatorio_vendas_base_detalhe '20151208', '20151208', 'Loja 1'

	exec up_relatorio_produtos_desafio @dataIni='2022-01-01 00:00:00',@dataFim='2022-06-27 00:00:00',@op=N'',@ref=N'',@lab=N'',@marca=N'',@familia=N'',@atributosFam=N'',@site=N'Loja 1',@design=N'',@tipoDoc=N'',@generico=N'',@tipoProduto=N''
	
*/

if OBJECT_ID('[dbo].[up_relatorio_produtos_desafio]') IS NOT NULL
	drop procedure dbo.up_relatorio_produtos_desafio
go

create procedure [dbo].[up_relatorio_produtos_desafio]
	@dataIni		datetime
	,@dataFim		datetime
	,@op			varchar(max)
	,@ref			varchar(max)=''
	,@lab			varchar(254)=''
	,@marca			varchar(200)=''
	,@familia		varchar(max)=''
	,@atributosFam	varchar(max)=''
	,@site			varchar(55)
	,@design		varchar(80)=''
	,@tipoDoc		varchar(max)=''
	,@generico		varchar(18) = ''
	,@incliva		as bit=0
	,@tipoProduto	as varchar(254) =''
/* with encryption */
AS
SET NOCOUNT ON
	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
		DROP TABLE #dadosFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosMarcas'))
		DROP TABLE #dadosMarcas
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRefs'))
		DROP TABLE #dadosRefs
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosLab'))
		DROP TABLE #dadosLab	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
		DROP TABLE #dadosSt	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosTd'))
		DROP TABLE #dadosTd	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseFiltro'))
		DROP TABLE #dadosVendasBaseFiltro	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.##dadosVendasBaseFiltro'))
		DROP TABLE #dadosVendasBasetot	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseQuotaLab'))
		DROP TABLE #dadosVendasBaseQuotaLab	

	SET @atributosFam = (CASE WHEN (@atributosFam IS NULL OR @atributosFam = '') THEN ';;;' ELSE @atributosFam END)

	DECLARE @departamento VARCHAR(254) = ''
	SET @departamento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 1),'') 
	DECLARE @sessao       VARCHAR(254) = ''
	set @sessao = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 2) ,'')
	DECLARE @categoria    VARCHAR(254) = ''
	SET @categoria = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 3),'') 
	DECLARE @segmento     VARCHAR(254) = ''
	SET @segmento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 4),'') 

	/* Convereter Valor Generico*/
	IF @generico = 'GENERICO'
		set @generico = 1
	IF @generico = 'NÃO GENERICO'
		set @generico = 0
	/* Calc Loja e Armazens */
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)
	select 
		armazem
	into
		#EmpresaArm
	From
		empresa
		inner join empresa_arm on empresa.no = empresa_arm.empresa_no
	where
		empresa.site = case when @site = '' then empresa.site else @site end	
	/* Calc Documentos */
	Select
		ndoc
		,nmdoc
		,tipodoc
		,u_tipodoc
	into
		#dadosTd
	from
		td
	Where
		convert(varchar(9),td.ndoc) in (Select items from dbo.up_splitToTable(@tipoDoc,','))
		or @tipoDoc = ''

	/* Calc familias */
	Select
		distinct ref, nome
	into
		#dadosFamilia
	From
		stfami (nolock)
	Where
		ref in (Select items from dbo.up_splitToTable(@familia,','))
		Or @familia = '0'
		or @familia = ''
	/* Calc operadores */
	Select
		iniciais, cm = userno, username
	into
		#dadosOperadores
	from
		b_us (nolock)
	Where
		userno in (select items from dbo.up_splitToTable(@op,','))
		or @op= '0'
		or @op= ''

	/* Calc lab */

	Select DISTINCT
		st.u_lab
	into
		#dadoslab
	from
		st (nolock)
	Where
		u_lab in (select items from dbo.up_splitToTable(@lab,';'))
		or @lab= '0'
		or @lab= ''
	/* Calc marcas */

	Select DISTINCT
		st.usr1
	into
		#dadosMarcas
	from
		st (nolock)
	Where
		usr1 in (select items from dbo.up_splitToTable(@marca,','))
		or @marca= '0'
		or @marca= ''
	/* Calc Ref ST */
	Select 
		ref
	into
		#dadosRefs
	from
		st (nolock)
	Where
		ref in (select items from dbo.up_splitToTable(@ref,',')) 
		OR @ref= ''
		AND site_nr = @site_nr
	/* Calc Informação Produto */
	Select
		st.ref
		,st.design
		,usr1
		,u_lab	
		, ISNULL(A.descr,'') as departamento
		, ISNULL(B.descr,'') as seccao
		, ISNULL(C.descr,'') as categoria
		, ISNULL(D.descr,'') as segmento

		,dispdescr = ISNULL(dispdescr,'')
		,generico = ISNULL(generico,CONVERT(bit,0))
		,psico = ISNULL(psico,CONVERT(bit,0))
		,benzo = ISNULL(benzo,CONVERT(bit,0))
		,protocolo = ISNULL(protocolo,CONVERT(bit,0))
		,dci = ISNULL(dci,'')
		,grphmgcode = ISNULL(grphmgcode,'')
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 1) valDepartamento
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 2) valSeccao
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 3) valCategoria
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 4) valSegmento
		,isnull(b_famFamilias.design,'') as tipoProduto
	INTO
		#dadosSt
	From
		st (nolock)
		left join fprod (nolock) on st.ref = fprod.cnp
		left join b_famFamilias (nolock) on st.u_famstamp = b_famFamilias.famstamp	
		left join grande_mercado_hmr (nolock) A on A.id = st.u_depstamp
		left join mercado_hmr (nolock) B on B.id = st.u_secstamp
		left join categoria_hmr (nolock) C on C.id = st.u_catstamp 
		left join segmento_hmr (nolock) D on D.id = st.u_segstamp		
	Where
		(st.u_lab in (select u_lab from #dadosLab) or @lab='')
		and (st.usr1 in (select usr1 from #dadosMarcas) or @marca='')
		AND site_nr = @site_nr
	/* Define se vais mostrar resultados abaixo do Cliente 199 */
	declare @Entidades as bit
	set @Entidades = case when (select tipoempresa from empresa where site = @site) in ('FARMACIA', 'PARAFARMACIA') then convert(bit,1) else convert(bit,0) end
	/* Dados Base Vendas  */
	create table #dadosVendasBase (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(3,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)
	)
    insert #dadosVendasBase
    exec up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site
	--exec up_relatorio_vendas_base_detalhe '20171121', '20171121', 'Loja 1'

	/* Elimina Vendas tendo em conta tipo de cliente*/
	If @Entidades = 1
	delete from #dadosVendasBase where #dadosVendasBase.no < 199
	/* Preparar Result Set */
	Select
		[nrAtend]
		,[ftstamp]
		,[fdata]
		,[no]
		,#dadosVendasBase.[ndoc]
		,#dadosTd.[nmdoc]
		,[fno]
		,#dadosVendasBase.[tipodoc]
		,#dadosVendasBase.[u_tipodoc]
		,#dadosVendasBase.[ref]
		,#dadosVendasBase.[design]
		,#dadosVendasBase.[familia]
		,#dadosfamilia.[nome] as faminome
		,[u_epvp]
		,[epcpond]
		,[iva]
		,[qtt]
		,[etiliquido]
		,[etiliquidoSiva]
		,[ettent1]
		,[ettent2]
		,[ettent1Siva]
		,[ettent2Siva]
		,[desconto]
		,[descvalor]
		,[descvale]
		,[ousrhora]
		,[ousrinis]
		,[vendnm]
		,[u_ltstamp]
		,[u_ltstamp2]
		,[usr1]
		,[u_lab]
		,[departamento]
		,[seccao]
		,[categoria]
		,segmento
		,[dispdescr]
		,[generico]
		,[psico]
		,[benzo]
		,[protocolo]
		,[dci]
		,[valDepartamento]
		,[valSeccao]
		,[valCategoria]
		,[valSegmento]
	into
		#dadosVendasBaseFiltro
	From
		#dadosVendasBase 
		inner join #dadosTd on #dadosTd.ndoc = #dadosVendasBase.ndoc
		left Join #dadosSt on #dadosVendasBase.ref = #dadosSt.ref
		left join #dadosFamilia on #dadosVendasBase.familia = #dadosfamilia.ref
	WHERE
		#dadosVendasBase.tipo != 'ENTIDADE'
		AND #dadosVendasBase.u_tipodoc not in (1, 5)
		and (isnull(#dadosSt.u_lab,'') in (select u_lab from #dadosLab) or @lab='')
		and (isnull(#dadosSt.USR1,'') in (select usr1 from #dadosMarcas) or @marca='')
		and #dadosVendasBase.ousrinis in (select iniciais from #dadosOperadores)
		and (#dadosVendasBase.ref in (Select ref from #dadosRefs) or #dadosVendasBase.ref='')
		and isnull(#dadosVendasBase.familia,'99') in (Select ref from #dadosFamilia)
		and #dadosVendasBase.design like case when @design = '' then #dadosVendasBase.design else @design + '%' end
		and isnull(#dadosSt.generico,0) = case when @generico = '' then isnull(#dadosSt.generico,0) else @generico end
		and (isnull(#dadosSt.tipoProduto, '') in (select items from dbo.up_splitToTable(@tipoProduto,','))  or @tipoProduto = '')
		AND departamento = (CASE WHEN @departamento = '' THEN departamento ELSE @departamento END)
		AND seccao = (CASE WHEN @sessao = '' THEN seccao ELSE  @sessao END)
		AND categoria = (CASE WHEN @categoria = '' THEN categoria ELSE  @categoria END)
		AND segmento = (CASE WHEN @segmento = '' THEN segmento ELSE  @segmento END) 

	select 
		ousrinis
		, ref
		, design 
		, cast(sum(qtt) as numeric(10,0)) as qtt
		--, cast(avg(etiliquidoSiva+ettent1siva+ettent2siva) as numeric(10,2)) as valindsiva
		, cast(avg(u_epvp-(u_epvp*(iva/100))) as numeric(10,2)) as valindsiva
		, cast(avg(u_epvp) as numeric(10,2)) as valind
		--, cast(avg(etiliquido+ettent1+ettent2) as numeric(10,2)) as valind
		, cast(sum(qtt*(u_epvp-(u_epvp*(iva/100)))) as numeric(10,2)) as valtotsiva
		, cast(sum(qtt*(u_epvp)) as numeric(10,2)) as valtot
		, cast (Round (cast (Sum(etiliquidoSiva+ettent1Siva+ettent2Siva-epcpond) as numeric(19,6)),2) as numeric (19,2)) as margem
		,cast((case when Sum(etiliquidoSiva+ettent1Siva+ettent2Siva) <= 0 or Sum(etiliquidoSiva+ettent1Siva+ettent2Siva)-Sum(epcpond)<0 then 0 else (Sum(etiliquidoSiva+ettent1Siva+ettent2Siva)-Sum(epcpond))*100/Sum(etiliquidoSiva + ettent1Siva + ettent2Siva) end) as numeric(10,2)) as margemperc
	from 
		#dadosVendasBaseFiltro 
	group by 
		ousrinis
		, ref
		, design

	

	--select * from #dadosVendasBaseFiltro
	--exec up_relatorio_produtos_desafio '20171101', '20171130', '', '', '', '', '', '', 'Loja 1', '', '', ''
	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
		DROP TABLE #dadosFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosMarcas'))
		DROP TABLE #dadosMarcas
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRefs'))
		DROP TABLE #dadosRefs
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosLab'))
		DROP TABLE #dadosLab	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
		DROP TABLE #dadosSt
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosTd'))
		DROP TABLE #dadosTd
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.##dadosVendasBaseFiltro'))
		DROP TABLE #dadosVendasBaseFiltro	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.##dadosVendasBaseFiltro'))
		DROP TABLE #dadosVendasBasetot	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseQuotaLab'))
		DROP TABLE #dadosVendasBaseQuotaLab	
		
GO
Grant Execute on dbo.up_relatorio_produtos_desafio to Public
Grant control on dbo.up_relatorio_produtos_desafio to Public
GO