/* Relatório Detalhe contentores valormed

	exec up_relatorio_contentores_valormed '20230101', '20230725', 'Loja 6'
	
*/




if OBJECT_ID('[dbo].[up_relatorio_contentores_valormed]') IS NOT NULL
	drop procedure dbo.up_relatorio_contentores_valormed
go

create procedure [dbo].[up_relatorio_contentores_valormed]
	@dataIni		datetime
	,@dataFim		datetime
	,@site			varchar(55)

/* with encryption */

AS
SET NOCOUNT ON

	/* Calc Loja e Armazens */
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)

	declare @codigofarm as numeric(10,0)
	set @codigofarm = ISNULL((select infarmed from empresa where site = @site),0)

	declare @nomecomp as varchar(255)
	set @nomecomp = ISNULL((select nomecomp from empresa where site = @site),'')

	declare @niffarm as varchar(16)
	set @niffarm = ISNULL((select ncont from empresa where site = @site),'')

	declare @codpostfarm as varchar(16)
	set @codpostfarm = ISNULL((select codpost from empresa where site = @site),'')

	declare @localfarm as varchar(16)
	set @localfarm = ISNULL((select local from empresa where site = @site),'')

	declare @teleffarm as varchar(16)
	set @teleffarm = ISNULL((select telefone from empresa where site = @site),'')

	declare @moradafarm as varchar(16)
	set @moradafarm = ISNULL((select morada from empresa where site = @site),'')

	/* Preparar Result Set */
	select 
	'"'+cast(bi.lote as varchar(16))+'"' as 'Container_SerialNumber'
	, '"'+CONVERT(VARCHAR(32), st_lotes.ousrdata, 126)+'"' as 'Container_OccuredOn'
	, '"'+''+'"' as 'Container_Identifier'
	, '"'+'0'+'"' as 'Container_IsCanceled'
	, '"'+''+'"' as 'Container_CanceledOn'
	, '"'+left(CONVERT(VARCHAR(32), st_lotes.usrdata, 126),10)+'T'+cast(st_lotes.ousrhora as varchar(8))+'"' as 'Container_UpdateOn'
	, '"'+cast(@codigofarm as varchar(8))+'"' as 'Pharmacy_InfarmedCode'
	, '"'+cast(@nomecomp as varchar(255))+'"' as 'Pharmacy_Name'
	, '"'+cast(@niffarm as varchar(16))+'"' as 'Pharmacy_VatNumber'
	, '"'+cast(@codpostfarm as varchar(16))+'"' as 'Pharmacy_PostalCode'
	, '"'+cast(@localfarm as varchar(64))+'"' as 'Pharmacy_Locale'
	, '"'+cast(@teleffarm as varchar(32))+'"' as 'Pharmacy_PhoneNumber'
	, '"'+cast(@moradafarm as varchar(255))+'"' as 'Pharmacy_Address'
	, '"'+''+'"' as 'Pharmacy_District'
	, '"'+''+'"' as 'Pharmacy_County'
	, '"'+''+'"' as 'Pharmacy_Parish'
	, '"'+cast(fl.codinfarmed as varchar(8))+'"' as 'Retailer_InfarmedCode'
	, '"'+cast(bo.nome as varchar(255))+'"' as 'Retailer_Name'
	, '"'+cast(bo.morada as varchar(255))+'"' as 'Retailer_Address'
	, '"'+cast(bo.ncont as varchar(16))+'"' as 'Retailer_VATNumber'
	, '"'+''+'"' as 'Retailer_ParentInfarmedCode'
	from 
		bi (nolock)
	inner join st_lotes (nolock) on bi.ref=st_lotes.ref and bi.lote=st_lotes.lote
	inner join bo (nolock) on bo.bostamp=bi.bostamp
	inner join fl (nolock) on fl.no=bo.no and fl.estab=bo.estab
	left join  ts on ts.ndos = bo.ndos
	where 
		(bi.ref='VALORMED' 
		or bi.ref = '7877647')
		and ts.codigoDoc = 35 -- devolução a fornecedor
		and bi.armazem = @site_nr
		and bi.rdata > = @dataIni and  bi.rdata<=@dataFim

GO
Grant Execute on dbo.up_relatorio_contentores_valormed to Public
Grant control on dbo.up_relatorio_contentores_valormed to Public
GO


