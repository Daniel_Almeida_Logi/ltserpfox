/*

Alterado a 2020-08-05 , JG: multiselecao para site

exec up_relatorio_stocks_validacaoPVPsMSRM '','','',''
exec up_relatorio_stocks_validacaoPVPsMSRM '','','Loja 1',''
exec up_relatorio_stocks_validacaoPVPsMSRM '','','Loja 2',''
exec up_relatorio_stocks_validacaoPVPsMSRM '','','Loja 3',''

exec up_relatorio_stocks_validacaoPVPsMSRM '','','Loja 2,Loja 3',''
exec up_relatorio_stocks_validacaoPVPsMSRM '','','Loja 1,Loja 3',''


*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_relatorio_stocks_validacaoPVPsMSRM]') IS NOT NULL
	DROP procedure dbo.up_relatorio_stocks_validacaoPVPsMSRM
GO

CREATE procedure dbo.up_relatorio_stocks_validacaoPVPsMSRM
@ref	 varchar(50),
@familia varchar(max),
@site	varchar(254),
@atributosFam varchar(max)
/* WITH ENCRYPTION */ 
AS
BEGIN
	SET @atributosFam = (CASE WHEN (@atributosFam IS NULL OR @atributosFam = '') THEN ';;;' ELSE @atributosFam END)

	DECLARE @departamento VARCHAR(254) = ''
	SET @departamento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 1),'') 
	DECLARE @sessao       VARCHAR(254) = ''
	set @sessao = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 2) ,'')
	DECLARE @categoria    VARCHAR(254) = ''
	SET @categoria = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 3),'') 
	DECLARE @segmento     VARCHAR(254) = ''
	SET @segmento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 4),'') 


	if @site = ''
		set @site = isnull((select  convert(varchar(254),(select 
							site + ', '
						from 
							empresa (nolock)							
						FOR XML PATH(''))) as no),0)

	declare @site_nr varchar(20)
	set @site_nr = isnull((select  convert(varchar(254),(select 
							convert(varchar,no) + ', '
						from 
							empresa (nolock)							
						Where 
							site in (Select items from dbo.up_splitToTable(@site, ','))
						FOR XML PATH(''))) as no),0)




;With 
	cteFamilia as (
		Select 
			distinct ref 
		From
			stfami (nolock)
		Where
			ref in (Select items from dbo.up_splitToTable(@familia,','))
			or @familia = '0' 
			or @familia = ''	
	),
	cteSt as (
		Select
			ref 
			,st.design
			,faminome
			,familia
			,usr1
			,u_lab
			,tabiva
			,epcpond
			,epv1
			,marg1	
			, ISNULL(A.descr,'') as departamento
			, ISNULL(B.descr,'') as seccao
			, ISNULL(C.descr,'') as categoria
			, ISNULL(D.descr,'') as segmento
			,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 1) valDepartamento
			,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 2) valSeccao
			,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 3) valCategoria
			,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 4) valSegmento		
			, site_nr
		From
			st (nolock)	
			left join grande_mercado_hmr (nolock) A on A.id = st.u_depstamp
			left join mercado_hmr (nolock) B on B.id = st.u_secstamp
			left join categoria_hmr (nolock) C on C.id = st.u_catstamp 
			left join segmento_hmr (nolock) D on D.id = st.u_segstamp
	)

	

Select
	cteSt.ref
	,cteSt.design
	,cteSt.familia
	,cteSt.faminome
	,pvp = epv1
	,pvpmax	= convert(numeric(13,3), isnull(fprod.pvpmax, 0))
	,pic = convert(numeric(13,3), isnull(fprod.pvporig, 0))
	,mbspvp	= (1-(cteSt.epcpond/
		CASE WHEN (cteSt.epv1 / ((isnull(taxasiva.taxa,0) / 100) + 1)) = 0
		THEN 1 ELSE
		(cteSt.epv1 / ((isnull(taxasiva.taxa,0) / 100) + 1))
		END
		))*100
	,MrgCom = cteSt.marg1
	,iva = taxasiva.taxa
	, site_nr
from	
	cteSt
	inner join fprod (nolock) on cteSt.ref = fprod.cnp
	inner join fpreco (nolock) on fprod.cnp=fpreco.cnp and fpreco.grupo='pvp'
	left join taxasiva (nolock) on taxasiva.codigo=cteSt.tabiva
Where
	(epv1 > pvpmax or epv1 > pvporig)
	and cteSt.ref = case when @ref = '' then cteSt.ref else @ref end 
	and cteSt.familia in (Select ref from cteFamilia)
	AND departamento = (CASE WHEN @departamento = '' THEN departamento ELSE @departamento END)
	AND seccao = (CASE WHEN @sessao = '' THEN seccao ELSE  @sessao END)
	AND categoria = (CASE WHEN @categoria = '' THEN categoria ELSE  @categoria END)
	AND segmento = (CASE WHEN @segmento = '' THEN segmento ELSE  @segmento END)
	and cteSt.site_nr in (Select items from dbo.up_splitToTable(@site_nr, ','))

Order by
	ref
END


GO
GRANT EXECUTE on dbo.up_relatorio_stocks_validacaoPVPsMSRM TO PUBLIC
GRANT CONTROL on dbo.up_relatorio_stocks_validacaoPVPsMSRM TO PUBLIC
GO