-- Relatório Valor Médio Venda
-- exec up_relatorio_vendas_valorMedio 2010,2010,1,1,''

if OBJECT_ID('[dbo].[up_relatorio_vendas_valorMedio]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_valorMedio
go

create procedure dbo.up_relatorio_vendas_valorMedio
@anoIni	int,
@anoFim	int,
@mesIni	int,
@mesFim	int,
@site varchar(30)
/* with encryption */
AS
SET NOCOUNT ON
set language portuguese
	select 
		mesNome = UPPER(DateName(mm,DATEADD(mm,MONTH(fdata),-1)))
		,Mes = MONTH(fdata)
		,Ano = year(fdata)
		,AvgTotal = convert(money,avg(total),2)
		,AvgBB = AVG(total-epcpond)
	from (
			/* DOCS DE FACTURACAO */
		SELECT
			fdata
			,epcpond = CASE WHEN TD.tipodoc = 3 THEN /* DEV. A CLIENTE */
							(CASE WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt ELSE FI.EPCP * qtt END)*(-1)
							ELSE
							(CASE WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt ELSE FI.EPCP * qtt END) 
						END
			,total = (etiliquido + 
						(CASE WHEN  (SELECT COUNT(*) AS VALOR FROM CTLTRCT (nolock) WHERE CTLTRCTstamp = ft.u_ltstamp) > 0 THEN u_ettent1 ELSE 0 END ) +
						 (CASE WHEN (SELECT COUNT(*) AS VALOR FROM CTLTRCT (nolock) WHERE CTLTRCTstamp = ft.u_ltstamp2) > 0 THEN u_ettent2 ELSE 0 END)
					)
		FROM
			FI (nolock) 
			INNER JOIN ft (nolock) ON FI.FTSTAMP = ft.ftstamp
			INNER JOIN TD (nolock) ON TD.NDOC = ft.ndoc
		WHERE
			(ft.tipodoc!=4 or u_tipodoc=4) 
			AND u_tipodoc <> 1 
			AND u_tipodoc <> 5 
			AND fi.composto=0 AND ft.anulado=0 and ft.no>199
			and YEAR(fdata) between @AnoIni and @AnoFim
			and MONTH(fdata) between @MesIni and @MesFim
			and ft.tipodoc != 4 
			and ft.anulado=0 
			and ft.tipodoc=1 
			and ft.nmdoc!='Factura SNS' 
			and ft.nmdoc!='Factura Entidades'
			and ft.site = case when @site='' then ft.site else @site end	
		UNION ALL
		/* RECEITAS */
		SELECT
			fdata
			,epcpond	= CASE WHEN TD.tipodoc = 3 THEN /* DEV. A CLIENTE */
							(CASE WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt ELSE FI.EPCP * qtt END)*(-1)
							ELSE
							(CASE WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt ELSE FI.EPCP * qtt END) 
						END
			,total = (0 + 
						(CASE WHEN  (SELECT COUNT(*) AS VALOR FROM CTLTRCT (nolock) WHERE CTLTRCTstamp = ft.u_ltstamp) > 0 THEN u_ettent1 ELSE 0 END ) +
						 (CASE WHEN (SELECT COUNT(*) AS VALOR FROM CTLTRCT (nolock) WHERE CTLTRCTstamp = ft.u_ltstamp2) > 0 THEN u_ettent2 ELSE 0 END)
					)
		FROM
			FI (nolock) 
			INNER JOIN ft (nolock) ON FI.FTSTAMP = ft.FTSTAMP 
			INNER JOIN TD (nolock) ON TD.NDOC = ft.NDOC
		WHERE
			U_TIPODOC = 3 
			and YEAR(fdata) between @AnoIni and @AnoFim
			and MONTH(fdata) between @MesIni and @MesFim
			and ft.site = case when @site='' then ft.site else @site end
	) as x
	group by YEAR(fdata), MONTH(fdata)
set language english

GO
Grant Execute on dbo.up_relatorio_vendas_valorMedio to Public
Grant control on dbo.up_relatorio_vendas_valorMedio to Public
GO