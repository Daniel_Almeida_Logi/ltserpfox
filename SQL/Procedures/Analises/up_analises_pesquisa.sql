/* Lista asnalises pesquisa

	exec up_analises_pesquisa ''
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_analises_pesquisa]') IS NOT NULL
	drop procedure dbo.up_analises_pesquisa
go

create procedure dbo.up_analises_pesquisa
@descricao as varchar(60)

/* WITH ENCRYPTION */

AS
BEGIN

	Select
		sel = CONVERT(bit,0),
		ordem,
		grupo,
		subgrupo,
		descricao,
		report,
		formatoExp,
		perfil,
		objectivo = CONVERT(varchar(254),objectivo),
		comandofox
	from
		B_analises (nolock)
	where
		/*grupo like '%' +  @descricao + '%'
		or subgrupo like '%' +  @descricao + '%'*/
		descricao like '%' +  @descricao + '%'
		and estado=1
	order by
		grupo, descricao

END

GO
Grant Execute on dbo.up_analises_pesquisa to Public
Grant Control on dbo.up_analises_pesquisa to Public
GO