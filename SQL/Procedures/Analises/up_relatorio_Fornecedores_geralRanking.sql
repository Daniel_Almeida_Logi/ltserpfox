-- exec up_relatorio_Fornecedores_geralRanking '','','','Loja 1','',1,'farga'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_Fornecedores_geralRanking]') IS NOT NULL
	drop procedure dbo.up_relatorio_Fornecedores_geralRanking
go

create procedure dbo.up_relatorio_Fornecedores_geralRanking
	 @dataIni		datetime
	,@dataFim		datetime
	,@familia		varchar(max)=''
	,@site			varchar(55)
	,@generico		varchar(18) = ''
	,@incliva		bit=0
	,@lab			varchar(max)=''

/* with encryption */

AS 
BEGIN
	SET NOCOUNT ON

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
	DROP TABLE #dadosFamilia

	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadoslab'))
	DROP TABLE #dadoslab

	IF @dataIni = ''
	BEGIN
		SET @dataIni = '1900-01-01'
	END
	IF @dataFim = ''
	BEGIN
		SET @dataFim = convert(varchar(10), GETDATE(), 120)
	END
		/* Calc Loja e Armazens */
	DECLARE @site_nr as tinyint
	SET @site_nr = ISNULL((SELECT no FROM empresa WHERE site = @site),0)

		/* Convereter Valor Generico*/
	IF @generico = 'GENERICO'
		SET @generico = 1
	IF @generico = 'N�O GENERICO'
		SET @generico = 0


	/* Calc familias */
	SELECT
		DISTINCT ref, nome
	INTO
		#dadosFamilia
	FROM
		stfami (nolock)
	WHERE
		ref in (Select items from dbo.up_splitToTable(@familia,','))
		Or @familia = '0'
		or @familia = ''



	/* Calc lab */
	SELECT
		DISTINCT u_lab as lab
	INTO
		#dadosst
	FROM
		st (nolock)
	WHERE
		u_lab in (Select items from dbo.up_splitToTable(@lab,';'))
		or @lab = ''

		
	SELECT DISTINCT
		 fo.no																								AS numeroFornecedor
		,fo.nome																							AS nomeFornecedor
		,SUM(FN.qtt)																						AS quantidade
		,CASE WHEN  @incliva = '1' THEN SUM(fn.etiliquido)  ELSE  SUM((fn.etiliquido / (fn.iva/100+1))) END	AS valor 
	FROM 
		fo					(nolock) 
		INNER JOIN  FN		(nolock)	ON FO.FOSTAMP = FN.FOSTAMP
		INNER JOIN  FL		(nolock)	ON fo.no = FL.no
		INNER JOIN  ST		(nolock)	ON (FN.ref	 = ST. ref  AND ST.site_nr= @site_nr)
		INNER JOIN  #dadosFamilia		ON ST.familia = #dadosfamilia.ref
		LEFT  JOIN  fprod	(nolock)	ON FN.ref = fprod.ref
	WHERE 
		fo.data			  BETWEEN @dataIni and @dataFim
		AND fo.site		   =(CASE WHEN @site = ''     THEN fo.site		  ELSE @site END)
		AND fo.doccode	   in (55, 100, 105, 3, 58) 
		AND fprod.generico = CASE WHEN @generico = '' THEN fprod.generico ELSE @generico END
		AND ST.u_lab in (Select lab from #dadosst ) 
	GROUP BY
		fo.no,
		fo.nome
	ORDER BY 
		valor DESC

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
	DROP TABLE #dadosFamilia

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadoslab'))
	DROP TABLE #dadoslab
	
END

GO
Grant Execute on dbo.up_relatorio_Fornecedores_geralRanking to Public
Grant control on dbo.up_relatorio_Fornecedores_geralRanking to Public
GO