/* Relatório Detalhe Relatório de vales descontados

	exec up_relatorio_compras_vendas '20240627', '20240627', 'Loja 1' 
	exec up_relatorio_compras_vendas '20240304', '20240304', 'Loja 1' 
	
*/

if OBJECT_ID('[dbo].[up_relatorio_compras_vendas]') IS NOT NULL
	drop procedure up_relatorio_compras_vendas
go

create procedure [dbo].[up_relatorio_compras_vendas]
	@dataIni		DATETIME
	,@dataFim		DATETIME
	,@site			VARCHAR(55)
/* with encryption */
AS
SET NOCOUNT ON
	SET LANGUAGE Português 
	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosvendas'))
		DROP TABLE #dadosvendas	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadoscompras'))
		DROP TABLE #dadoscompras
			
	/* Calc Loja e Armazens */
	DECLARE @site_nr AS TINYINT
	SET @site_nr = ISNULL((SELECT NO FROM empresa WHERE site = @site),0)

	/* Calc vendas */
	SELECT 
		fdata
		,ISNULL(SUM(etotal),0) AS total
	INTO
		#dadosvendas
	FROM 
		ft(NOLOCK) 
	INNER JOIN 
		td(NOLOCK) ON ft.ndoc = td.ndoc 
	WHERE 
		fdata BETWEEN @dataIni AND @dataFim
		--AND td.tipodoc NOT IN (1, 5)
		AND ft.site = (CASE WHEN @site = '' THEN ft.site ELSE @site END)
	GROUP BY 
		fdata

	/* Calc compras */
	SELECT 
		datalc
		,ISNULL(SUM(fc.ecred-fc.edeb),0) AS valor
	into
		#dadoscompras
	FROM 
		fc(NOLOCK)
	INNER JOIN 
		fo(NOLOCK) on fo.fostamp = fc.fostamp
	INNER JOIN 
		cm1(NOLOCK) on fo.doccode = cm1.cm 
	WHERE 
		fc.datalc BETWEEN @dataIni AND @dataFim
		AND fo.site = (CASE WHEN @site = '' THEN fo.site ELSE @site end)
		AND cm1.codigoDoc NOT IN(80)
	GROUP BY 
		datalc


	/* Preparar Result Set */
	SELECT 
		[Date] = (DATEADD(Day,Number,@dataIni))
		,diasemana=DATENAME(WEEKDAY, DATEADD(Day,Number,@dataIni))
		,vendas=CAST(ISNULL((SELECT total FROM #dadosvendas 
								WHERE #dadosvendas.fdata=DATEADD(Day,Number,@dataIni)),0)   AS NUMERIC(15,2))
		,compras=CAST(ISNULL((SELECT valor FROM #dadoscompras 
								WHERE #dadoscompras.datalc=DATEADD(Day,Number,@dataIni)),0) AS NUMERIC(15,2))
		,cv=CAST(CASE WHEN 
				ISNULL((SELECT total FROM #dadosvendas WHERE #dadosvendas.fdata=DATEADD(Day,Number,@dataIni)),0)<>0 
				THEN 
					CASE WHEN ISNULL((SELECT valor FROM #dadoscompras WHERE #dadoscompras.datalc=DATEADD(Day,Number,@dataIni)),0)=0 
					THEN 0
					ELSE ((((ISNULL((SELECT valor FROM #dadoscompras 
									WHERE #dadoscompras.datalc=DATEADD(Day,Number,@dataIni)),0)*100))/
											ISNULL((SELECT total FROM #dadosvendas 
											WHERE #dadosvendas.fdata=DATEADD(Day,Number,@dataIni)),0)) )
					END
				ELSE 
					0
				END
				AS NUMERIC(15,2))
	FROM  
		master..spt_values 
	WHERE 
		Type='P'
		AND DATEADD(day,Number,@dataIni) <= @dataFim

	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosvendas'))
		DROP TABLE #dadosvendas	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadoscompras'))
		DROP TABLE #dadoscompras		

GO
Grant Execute on dbo.up_relatorio_compras_vendas to Public
Grant control on dbo.up_relatorio_compras_vendas to Public
GO