-- Relatório Conferência Ajustes
-- exec up_relatorio_conferencia_ajustes '20110101','20120101'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_conferencia_ajustes]') IS NOT NULL
	drop procedure dbo.up_relatorio_conferencia_ajustes
go

create procedure dbo.up_relatorio_conferencia_ajustes
@dataIni datetime,
@dataFim datetime,
@site varchar(55)
/* with encryption */
AS 
BEGIN
SELECT	
	CODLOJA = (select top 1 ref from empresa where site = @site)
	,LOJA = (select top 1 site from empresa where  site = @site)
	,NUMLANCAMENTO = FOID
	,NUMEROFACTURA = FO.ADOC
	,REFERENCIA = CASE	WHEN (CASE WHEN REF = '' THEN OREF ELSE '' END) != '' THEN (CASE WHEN REF = '' THEN OREF ELSE '' END)
						ELSE (SELECT TOP 1 GUIA.REF FROM FN GUIA (nolock) WHERE GUIA.ofnstamp = FN.fnstamp)
				END
	,NUMEROENCOMENDA = ISNULL((SELECT CAST(OBRANO as VARCHAR(254)) FROM BI (nolock) as ENCLIN WHERE ENCLIN.BISTAMP = (SELECT top 1 bistamp FROM FN GUIA (nolock) WHERE GUIA.fnstamp = FN.ofnstamp)),'')
	,NUMERORECEPCAO = (SELECT TOP 1 GUIA.ADOC FROM FN GUIA (nolock) WHERE GUIA.fnstamp = FN.ofnstamp)
	,DATARECEPCAO = (SELECT TOP 1 GUIA.DATA FROM FN GUIA (nolock) WHERE GUIA.fnstamp = FN.ofnstamp)
	,UNIT_COST_RECEPCAO = (SELECT TOP 1 GUIA.U_UPC FROM FN GUIA (nolock) WHERE GUIA.fnstamp = FN.ofnstamp)
	,QTD_RECEPCIONADA = (SELECT TOP 1 GUIA.QTT FROM FN GUIA (nolock) WHERE GUIA.fnstamp = FN.ofnstamp)
	,UNIT_COST_FACTURA = U_UPC
	,QTD_FACTURADA = QTT
	/* VERIFICA SE O DOCUMENTO GERADO PARA AJUSTE FOI PEDIDO DE CREDITO OU GUIA DE ENTRADA */
	,DOC_AJUSTE = (SELECT TOP 1 RTRIM(LTRIM(DOCNOME)) + ': ' + LTRIM(RTRIM(ADOC)) FROM FN GUIADEAJUSTE (nolock) WHERE GUIADEAJUSTE.ofnstamp = FN.fnstamp)
	,AJUSTE_VALOR = (SELECT TOP 1 SUM(u_UPC) FROM FN GUIADEAJUSTE (nolock) WHERE GUIADEAJUSTE.ofnstamp = FN.fnstamp)
	,AJUSTE_QUANTIDADE = (SELECT TOP 1 SUM(QTT) FROM FN GUIADEAJUSTE (nolock) WHERE GUIADEAJUSTE.ofnstamp = FN.fnstamp)
	,DATA_AJUSTE = (SELECT TOP 1 DATA FROM FN GUIADEAJUSTE (nolock) WHERE GUIADEAJUSTE.ofnstamp = FN.fnstamp)
FROM	
	FN (nolock)
	INNER JOIN FO (nolock) ON FO.FOSTAMP = FN.FOSTAMP
WHERE	
	FO.doccode = 55
	AND FO.docdata BETWEEN @dataIni AND @dataFim	
	AND		
	((SELECT SUM(CASE WHEN QTT > 0 THEN u_UPC ELSE -u_upc END) FROM FN GUIADEAJUSTE (nolock) WHERE GUIADEAJUSTE.ofnstamp = FN.fnstamp) != 0
	OR
	(SELECT SUM(QTT) FROM FN GUIADEAJUSTE (nolock) WHERE GUIADEAJUSTE.ofnstamp = FN.fnstamp)!=0)
ORDER BY 
	(SELECT TOP 1 DATA FROM FN GUIADEAJUSTE (nolock) WHERE GUIADEAJUSTE.ofnstamp = FN.fnstamp)
END


GO
Grant Execute on dbo.up_relatorio_conferencia_ajustes to Public
GO
Grant Control on dbo.up_relatorio_conferencia_ajustes to Public
GO