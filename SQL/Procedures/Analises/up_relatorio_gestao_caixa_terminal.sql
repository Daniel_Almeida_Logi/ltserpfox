-- Relatório Caixas Terminal 
-- 
/*
	exec up_relatorio_gestao_caixa_terminal '20160901','20160930','Loja 1'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_caixa_terminal]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_caixa_terminal
go

create procedure dbo.up_relatorio_gestao_caixa_terminal
@DataIni DATETIME,
@DataFim DATETIME,
@site varchar(50)
/* with encryption */
AS
SET NOCOUNT ON
declare @usaPagCentral bit
set @usaPagCentral = ISNULL((select bool from b_parameters where stamp='ADM0000000079'),0)
select	
	cx.site
	,NUMLOJA = isnull((Select TOP 1 REF FROM empresa (nolock) WHERE site = cx.site),'')
	,data = convert(varchar,cx.dabrir,102)
	,cx.pnome
	,cx.pno
	,cx.auserno
	,cx.ausername
	,cx.fuserno 
	,evdinheiro		= sum(pc.evdinheiro)
	,epaga2			= sum(pc.epaga2)
	,epaga1			= sum(pc.epaga1)
	,echtotal		= sum(pc.echtotal)
	,epaga3			= sum(pc.epaga3)
	,epaga4			= sum(pc.epaga4)
	,epaga5			= sum(pc.epaga5)
	,epaga6			= sum(pc.epaga6)
	,devolucoes		= sum(pc.devolucoes)
	,creditos		= sum(pc.creditos)
	,totalcaixa		= sum(pc.evdinheiro + pc.epaga1 + pc.epaga2 + pc.echtotal + pc.epaga3 + pc.epaga4 + pc.epaga5 + pc.epaga6)
	,Nr_Vendas		= sum(pc.nrvendas)
	,Nr_Atend		= count(distinct pc.nratend)
	,fecho_dinheiro	= cx.fecho_dinheiro
	,fecho_visa		= cx.fecho_visa
	,fecho_mb		= cx.fecho_mb
	,fecho_cheques	= cx.fecho_cheques
	,fecho_epaga3	= cx.fecho_epaga3
	,fecho_epaga4	= cx.fecho_epaga4
	,fecho_epaga5	= cx.fecho_epaga5
	,fecho_epaga6	= cx.fecho_epaga6
	,pendente		= sum(case when @usaPagCentral=1 
										then (case when fechado=0 and abatido=0
													then total
													else 0 end)
							else 0 end)
	,cx.comissaoTPA
from
	cx (nolock)
	inner join B_pagCentral pc on pc.cxstamp=cx.cxstamp
where 
	cx.dabrir between @dataini and @DataFim
	and cx.site = case when @site='TODAS' then cx.site else @site end
	and cx.fechada=1
	and pc.ignoraCaixa = 0
group by 
	cx.dabrir, cx.pnome, cx.pno, cx.site, cx.auserno, cx.ausername, cx.fuserno, cx.fuserno, cx.comissaoTPA, 
	cx.fecho_dinheiro, cx.fecho_visa, cx.fecho_mb, cx.fecho_cheques, cx.fecho_epaga3, cx.fecho_epaga4, cx.fecho_epaga5, cx.fecho_epaga6


GO
Grant Execute on dbo.up_relatorio_gestao_caixa_terminal to Public
Grant control on dbo.up_relatorio_gestao_caixa_terminal to Public
Go