/* Relatório Preçário de artigos

	exec up_relatorio_faltas_balcao 'Todas','20250201','20250225'


*/

if OBJECT_ID('[dbo].[up_relatorio_faltas_balcao]') IS NOT NULL
	drop procedure dbo.up_relatorio_faltas_balcao
go

create procedure [dbo].up_relatorio_faltas_balcao
	@site			varchar(55)
	,@dataIni		datetime
	,@dataFim		datetime

/* with encryption */

AS
SET NOCOUNT ON
	
	Declare @central bit 

	set @central = 	(select bool from B_Parameters where stamp='ADM0000000353')


	IF(@central=0)
	BEGIN
		Select 
			ref
			,design
			,b_us.nome as 'vendnm'
			,bo.site
			,COUNT(bistamp) as nrvezes
			,SUM(CAST(qtt as INTEGER)) as qtt
		From
			bi (nolock)
			inner join bo (nolock) on bo.bostamp=bi.bostamp
			inner join b_us (nolock) on bi.ousrinis = b_us.iniciais
		Where
			bo.ndos=48
			and bo.dataobra between @dataIni and @dataFim
			and bo.site =case when @site='TODAS' then bo.site else @site end
		group by 
			ref
			,design
			,b_us.nome
			,bo.site
		order by
			design
	END
	ELSE
	BEGIN
		SELECT  
			ref,
			design,
			vendnm,
			site	  as site,
			COUNT(bistamp)  as nrvezes
			--falta na tabela campo qtt
			From [regFaltasCentralizadas]
		where	ndos=48
			and dataobra between @dataIni and @dataFim
			and [regFaltasCentralizadas].site =case when @site='TODAS' then [regFaltasCentralizadas].site else @site end

		group by 
			ref
			,design
			,vendnm
			,site
		order by
			design
	END 

GO
Grant Execute on dbo.up_relatorio_faltas_balcao to Public
Grant control on dbo.up_relatorio_faltas_balcao to Public
GO
