/* Lista Horas apoio a an�lises

	exec up_listagem_clientes_aniversarios 100,'Loja 1'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_listagem_clientes_aniversarios]') IS NOT NULL
    drop procedure dbo.up_listagem_clientes_aniversarios
go

create procedure dbo.up_listagem_clientes_aniversarios
@dias int,
@site varchar(60)

/* WITH ENCRYPTION */
AS


	;with cteUltimaVisita as(
		Select 
			no
			,estab
			,datalc
		From ( 
				Select	
					no
					,estab
					,datalc
					,id = ROW_NUMBER() over (partition by cc.no order by datalc desc)
				from	
					cc (nolock)
		) a
		WHere 
			id = 1
	)			

	Select	
		nome
		,b_utentes.no
		,b_utentes.estab
		,nascimento
		,idade = datediff(yy,nascimento,GETDATE())
		,local
		,codpost
		,morada
		,profissao = profi
		,telefone
		,tlmvl
		,email
		,ultimavisita = isnull(cteUltimaVisita.datalc,CONVERT(datetime,'19000101'))
	From
		b_utentes (nolock)
		left join cteUltimaVisita on b_utentes.no = cteUltimaVisita.no and b_utentes.estab = cteUltimaVisita.estab
	Where
		/* Proximo aniversario com data depois de hoje */
		case when dateadd(yy,datediff(yy,nascimento,GETDATE()),nascimento) > dateadd(dd,datediff(dd,0,GETDATE()),0)
			then dateadd(yy,datediff(yy,nascimento,GETDATE()),nascimento)
			else  dateadd(yy,datediff(yy,nascimento,GETDATE())+1,nascimento)
		end  
		between
		/* Hoje */
		dateadd(dd,datediff(dd,0,GETDATE()),0) and
		/* x dias depois */
		dateadd(dd,datediff(dd,0,GETDATE()+ @dias),0)
		and nascimento != '19000101'
		and inactivo = 0
	Order by
		month(nascimento), day(nascimento)		
GO
Grant Execute on dbo.up_listagem_clientes_aniversarios to Public
Grant Control on dbo.up_listagem_clientes_aniversarios to Public
go
