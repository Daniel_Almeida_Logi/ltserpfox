/* listagem Vendas Suspensas Operador  

	exec up_listagem_vendasSuspensas '20210101','20211227','Loja 1',''
	exec up_listagem_vendasSuspensas '20210101','20211227','Loja 1','Normal'
	exec up_listagem_vendasSuspensas '20210101','20211227','Loja 1','Normal,VIP'
	exec up_listagem_vendasSuspensas '20210101','20211227','Loja 1','Normal,Bruno,Subs�dio,Medicamentosa,Medicare 10%;Distribuidor,Normal,ENTIDADE,Assoc,BC,ASSOCIADOS,VIP'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF 
GO

if OBJECT_ID('[dbo].[up_listagem_vendasSuspensas]') IS NOT NULL
	drop procedure dbo.up_listagem_vendasSuspensas
go

create procedure dbo.up_listagem_vendasSuspensas
@dataIni	DATETIME,
@dataFim	DATETIME,
@site		VARCHAR(20),
@tipoCli	VARCHAR(256)
/* WITH ENCRYPTION */
AS

	If OBJECT_ID('tempdb.dbo.#dadosVendasSuspensas') IS NOT NULL
		DROP TABLE #dadosVendasSuspensas

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosTipoCliente'))
		DROP TABLE #dadosTipoCliente	

	create  table #dadosTipoCliente (
		[tipo] varchar(20)
	)

	IF(@tipoCli='')
	BEGIN

		INSERT INTO #dadosTipoCliente (tipo)
		SELECT distinct tipo FROM b_utentes
	END 
	ELSE
	BEGIN
		INSERT INTO #dadosTipoCliente (tipo)
		SELECT tipo FROM b_utentes Where b_utentes.tipo in (Select items as tipo from dbo.up_splitToTable(@tipoCli,','))
	END 


	
	SELECT 
		Convert(varchar(20),FT.no)																													AS num,
		FT.nome																																		AS nome,
		CASE WHEN u_hclstamp !='' THEN (SELECT convert(varchar(20),no) + '.'+ convert(varchar(20),estab)  FROM B_UTENTES WHERE  utstamp=u_hclstamp) 
				ELSE (convert(varchar(20),ft.no) + '.'+ convert(varchar(20),ft.estab) )  END														AS cliCod,
		CASE WHEN u_hclstamp !='' THEN (SELECT nome  FROM b_utentes WHERE  utstamp=u_hclstamp) ELSE ft.nome end										AS nomeCli,
		'TI-MED'																																	AS cliref,
		Convert(varchar(20),fi.fno)																													AS numVen,
		Convert(varchar(20),ref)																													AS ref,
		Convert(varchar(20),design)																													AS nomeProd,
		Convert(int,qtt)																															AS qtt 
		--(SELECT ISNULL(SUM(ofi.qtt),0) AS [qtt] FROM [dbo].[fi] as [ofi] (nolock) inner join td (nolock) as otd on ofi.ndoc = otd.ndoc 
		--	where [fi].[fistamp] = [ofi].[ofistamp] AND [otd].[u_tipodoc] != 3 AND [otd].[nmdoc] not like '%Segur%')								AS qttReg

	into
		#dadosVendasSuspensas
	FROM 
		FT
		INNER JOIN fi				ON FT.ftstamp		= FI.ftstamp
		INNER JOIN b_utentes		ON b_utentes.no		= FT.no and b_utentes.estab=ft.estab	
	WHERE	
		 ft.fdata between @dataIni and @dataFim
		AND ft.site = case when @site = '' then ft.site else @site end
		AND FT.cobrado=1  /* Suspenso */
		AND	b_utentes.tipo in (select tipo from #dadosTipoCliente )
		--AND  b_utentes.tipo = (CASE WHEN @tipoCli='' THEN b_utentes.tipo ELSE @tipoCli END )
	ORDER BY
		 ft.fdata ASC
		,ft.ousrhora ASC
		,ft.fno  ASC
		,fi.ref	   ASC
	

	SELECT 
		'grupoid',
		'gruponome',
		'clicod',
		'clinome',
		'cliref',
		'linhaid',
		'prodcod',
		'prodnome',
		'quantidade'
	UNION ALL 

	select
		num,nome,cliCod,nomeCli,cliref,numVen,ref,nomeProd,Convert(varchar(20),qtt) as qtt  from #dadosVendasSuspensas
	--select
	--	num,nome,cliCod,nomeCli,cliref,numVen,ref,nomeProd,Convert(varchar(20),qtt- qttReg) as qtt  from #dadosVendasSuspensas where qtt- qttReg >0

	If OBJECT_ID('tempdb.dbo.#dadosVendasSuspensas') IS NOT NULL
		DROP TABLE #dadosVendasSuspensas

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosTipoCliente'))
		DROP TABLE #dadosTipoCliente	

GO
Grant Execute On dbo.up_listagem_vendasSuspensas to Public
Grant Control On dbo.up_listagem_vendasSuspensas to Public
Go

