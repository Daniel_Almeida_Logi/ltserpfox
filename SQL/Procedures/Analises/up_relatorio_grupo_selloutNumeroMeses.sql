-- SP: up_relatorio_grupo_selloutNumeroMeses
-- exec	up_relatorio_grupo_selloutNumeroMeses '20110101','20150101', ''
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_grupo_selloutNumeroMeses]') IS NOT NULL
	drop procedure dbo.up_relatorio_grupo_selloutNumeroMeses
go

Create procedure dbo.up_relatorio_grupo_selloutNumeroMeses
@dataIni datetime,
@dataFim datetime,
@site varchar(55)


/* with encryption */
AS

	
		Select
			bo.nome
			,nmeses = COUNT(bostamp)
		from 
			bo (nolock)
		where 
			bo.ndos = 44
			and bo.dataobra between @dataIni and @dataFim
		Group by nome
	
Go
Grant Execute on dbo.up_relatorio_grupo_selloutNumeroMeses to Public
Grant Control on dbo.up_relatorio_grupo_selloutNumeroMeses to Public
Go
