/*
	Análise: Devoluções Pendentes a Fornecedores
	
	exec up_relatorio_conferencia_DevolPendFornec 333,'20150601','20160219', 1,'Loja 1'

*/
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_conferencia_DevolPendFornec]') IS NOT NULL
	drop procedure dbo.up_relatorio_conferencia_DevolPendFornec
go

create procedure dbo.up_relatorio_conferencia_DevolPendFornec
@fornecedor varchar(55),
@dataIni datetime,
@dataFim datetime,
@incl bit,
@site varchar(20),
@estab as numeric(5,0) = -1

/* with encryption */

AS
SET NOCOUNT ON

	SELECT	
		bo.bostamp,
		bo.dataobra as data,
		RTRIM(LTRIM(bo.nmdos)) as nmdos,
		bo.obrano,
		bo.nome, 
		bo.no,
		bo.estab, 
		bo.morada, 
		bo.ncont,
		convert(varchar,bo.dataobra,102) as dataobra, 
		bi.ref, 
		bi.design, 
		bi.qtt, 
		bi.qtt2,
		bi.qtt - bi.qtt2 as qttpendente,
		bi.edebito,
		ettdeb	= (bi.qtt-bi.qtt2) * bi.edebito,
		bo.obs, 
		bi.lobs, 
		bi.vendedor, 
		bi.vendnm,
		(select top 1 isnull(b_us.username,'') From b_us (nolock) where b_us.iniciais=bi.usrinis) as username
		,PVP_Actual = isnull(st.epv1,0)
		,bi.iva
	FROM 
		bi (nolock)
		inner join bo (nolock) on bo.bostamp=bi.bostamp
		inner join empresa_arm on bi.armazem = empresa_arm.armazem
		left join st (nolock) on st.ref = bi.ref and st.site_nr = empresa_arm.empresa_no
	WHERE
		(bi.nmdos='Devol. a Fornecedor' or bo.nmdos='Pedido de Regularização' or bo.nmdos='Ajuste de Regularização')
		and bo.fechada=0
		/*AND bi.bistamp not in (select bistamp from fn (nolock))*/
		AND (bi.qtt - bi.QTT2) > CASE WHEN	@incl = 0 THEN 0 ELSE -9999999 END
		/*AND bo.nome = CASE WHEN @Fornecedor = '' THEN bo.nome ELSE @Fornecedor END*/
		AND (bo.[no] = CASE WHEN @Fornecedor = 0 THEN bo.no ELSE @Fornecedor END)
		AND (bo.estab = CASE WHEN @estab>-1 then  @estab else bo.estab  end)
		and bo.dataobra between @dataIni and @dataFim
		and bo.site = (case when @site = '' Then bo.site else @site end)
	ORDER BY 
		bo.nome, bo.[no], bo.obrano

GO
Grant Execute on dbo.up_relatorio_conferencia_DevolPendFornec to Public
Grant control on dbo.up_relatorio_conferencia_DevolPendFornec to Public
GO