--exec up_relatorio_pausas_fila_espera '20180503','20180503', 'Loja 1' 
/****** Object:  StoredProcedure [dbo].[up_relatorio_pausas_fila_espera]    Script Date: 06/21/2018 11:42:10 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER procedure [dbo].[up_relatorio_pausas_fila_espera]
@dataIni		datetime,
@dataFim		datetime,
@site			varchar(60)

/* WITH ENCRYPTION */ 

AS
BEGIN

	--/* Elimina Tabelas */
	--IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
	--	DROP TABLE #dadosOperadores

	--/* Calc operadores */
	--Select
	--	iniciais, cm = userno, username, nome
	--into
	--	#dadosOperadores
	--from
	--	b_us (nolock)

	--/* Calc Loja e Armazens */
	--declare @site_nr as tinyint
	--set @site_nr = ISNULL((select no from empresa where site = @site),0)

	/* Preparar Result Set Final */
	with dados_analise (motive_id, description, utilizador, contagem, tottempo)
	as
	(
	select 
		ext_ticket_occurency.motive_id
		, ext_ticket_occurency.description
		, isnull((select top 1 nome from b_us where ext_xopvision_breakmotive.ousrinis=b_us.iniciais),'') as utilizador
		--, isnull(ousrinis,'') as utilizador
		, count(ext_xopvision_breakmotive.token) as contagem
		, isnull(cast(sum(ext_xopvision_breakmotive.duration) as numeric(6,0)),0) as tottempo
	from 
		ext_ticket_occurency (nolock)
	left join
		ext_xopvision_breakmotive (nolock)
		on 
			ext_ticket_occurency.motive_id=ext_xopvision_breakmotive.motiveid 
			and isnull(ext_xopvision_breakmotive.duration,0)<>0
			and ext_xopvision_breakmotive.sitenr=(ISNULL((select no from empresa where site = @site),0))
			and convert(varchar, ext_xopvision_breakmotive.start_date, 112) between convert(varchar, @dataIni, 112) and convert(varchar, @dataFim, 112)
			--and ext_xopvision_breakmotive.start_date between @dataIni and @dataFim
	where
		ext_ticket_occurency.type='pausa'
	group by 
		ext_ticket_occurency.motive_id
		, ext_ticket_occurency.description 
		, ousrinis
	--order by ext_ticket_occurency.motive_id 
	union
	select 
		ext_ticket_occurency.motive_id
		, ext_ticket_occurency.description
		, isnull((select top 1 nome from b_us where ext_xopvision_breakmotive.ousrinis=b_us.iniciais),'') as utilizador
		, count(ext_xopvision_breakmotive.token) as contagem
		, isnull(cast(sum(ext_xopvision_breakmotive.duration) as numeric(6,0)),0) as tottempo
	from 
		ext_ticket_occurency (nolock)
	left join
		ext_xopvision_breakmotive (nolock)
		on 
			ext_ticket_occurency.motive_id=ext_xopvision_breakmotive.motiveid 
			and (isnull(ext_xopvision_breakmotive.duration,0)=0 and ext_xopvision_breakmotive.motiveid=12)
			and ext_xopvision_breakmotive.sitenr=(ISNULL((select no from empresa where site = @site),0))
			and convert(varchar, ext_xopvision_breakmotive.start_date, 112) between convert(varchar, @dataIni, 112) and convert(varchar, @dataFim, 112)
			--and ext_xopvision_breakmotive.start_date between @dataIni and @dataFim
	where
		ext_ticket_occurency.type='pausa'
	group by 
		ext_ticket_occurency.motive_id
		, ext_ticket_occurency.description 
		, ousrinis
	UNION
	select 
		99 as motive_id
		, 'Não fechadas' as description
		, isnull((select top 1 nome from b_us where ext_xopvision_breakmotive.ousrinis=b_us.iniciais),'') as utilizador
		--, isnull(ousrinis,'') as utilizador
		, count(ext_xopvision_breakmotive.token) as contagem
		, isnull(cast(sum(ext_xopvision_breakmotive.duration) as numeric(6,0)),0) as tottempo
	from 
		ext_ticket_occurency (nolock)
	left join
		ext_xopvision_breakmotive (nolock)
		on 
			ext_ticket_occurency.motive_id=ext_xopvision_breakmotive.motiveid 
			and (isnull(ext_xopvision_breakmotive.duration,0)=0 and ext_xopvision_breakmotive.motiveid<>12)
			and ext_xopvision_breakmotive.sitenr=(ISNULL((select no from empresa where site = @site),0))
			and convert(varchar, ext_xopvision_breakmotive.start_date, 112) between convert(varchar, @dataIni, 112) and convert(varchar, @dataFim, 112)
			--and ext_xopvision_breakmotive.start_date between @dataIni and @dataFim
	where
		ext_ticket_occurency.type='pausa'
	group by 
		ext_ticket_occurency.motive_id
		, ext_ticket_occurency.description 
		, ousrinis
	--order by ext_ticket_occurency.motive_id 
	)
	select 
		motive_id
		, description
		,utilizador
		, SUM(contagem) as contagem
		, SUM(tottempo) as tottempo
		,(select COUNT(distinct(u_nratend)) 
			from ft (nolock)
			INNER JOIN TD	(nolock) on TD.NDOC = FT.NDOC
			where ft.ousrinis=(select top 1 iniciais from b_us where b_us.nome=dados_analise.utilizador)
			and ft.tipo != 'ENTIDADE'
			AND td.u_tipodoc not in (1, 5)
			and convert(varchar, ft.fdata, 112) between convert(varchar, @dataIni, 112) and convert(varchar, @dataFim, 112)
			) as nratend
	from 
		dados_analise
	group by 
		motive_id
		, description
		,utilizador

END

