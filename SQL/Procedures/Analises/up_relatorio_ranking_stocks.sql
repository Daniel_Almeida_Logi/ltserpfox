-- Relatório Ranking Stocks
-- exec	up_relatorio_ranking_stocks '20100101','20130101','',''
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_relatorio_ranking_stocks]') IS NOT NULL
	DROP procedure dbo.up_relatorio_ranking_stocks
GO

CREATE procedure dbo.up_relatorio_ranking_stocks
@DATAINI as datetime,
@DATAFIM as datetime,
@FAMILIA as varchar(254),
@site as varchar(60)
/* WITH ENCRYPTION */ 
AS
BEGIN
	SET LANGUAGE PORTUGUESE
	;with cte1 (stamp1) as (
		Select	
			valor = ctltrctstamp
		From	
			ctltrct (nolock) as x
			inner join ft (nolock) as y on x.ctltrctstamp=y.u_ltstamp
	),cte2 (stamp2) as (
		Select 
			valor = ctltrctstamp
		From
			ctltrct (nolock) as w
			inner join ft (nolock) as z on w.ctltrctstamp=z.u_ltstamp2
	), cteVendas as (
		Select	
			fdata
			,ft.nmdoc
			,fi.etiliquido
			,ettent1	= CASE WHEN cte1.stamp1 is not null THEN u_ettent1 ELSE 0 END
			,ettent2	= CASE WHEN cte2.stamp2 is not null THEN u_ettent2 ELSE 0 END
			,epcpond	= CASE WHEN TD.tipodoc = 3 THEN /* DEV. A CLIENTE */
								(CASE WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt ELSE FI.EPCP * qtt END)*(-1)
								ELSE
								(CASE WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt ELSE FI.EPCP * qtt END) 
						END
			,fi.armazem
			,fi.qtt
			,fi.ref
			,fi.familia
		FROM	
			fi (nolock)
			inner join ft (nolock) ON FI.FTSTAMP = FT.FTSTAMP
			inner join td (nolock) ON TD.NDOC = FT.NDOC
			left join cte1 on cte1.stamp1=ft.u_ltstamp
			left join cte2 on cte2.stamp2=ft.u_ltstamp2
		WHERE	
			td.tipodoc != 4 /* Exclui documentos que não são integrados na contabilidade */
			AND fi.composto=0 
			AND ft.anulado=0
			/* Filtros  */
			AND ft.fdata between @dataIni And @dataFim
			AND ft.site = (case when @site = '' Then ft.site else @site end)
	),
	cteInsercaoReceitas as (
		/* RECEITAS */
		SELECT	
			fdata
			,ft.nmdoc
			,etiliquido		= 0
			,ettent1		= CASE WHEN cte1.stamp1 is not null THEN u_ettent1 ELSE 0 END
			,ettent2		= CASE WHEN cte2.stamp2 is not null THEN u_ettent2 ELSE 0 END
			,epcpond		= 0
			,fi.armazem
			,fi.qtt
			,fi.ref
			,fi.familia
		FROM 
			FI (nolock)
			INNER JOIN FT (nolock) ON FI.FTSTAMP = FT.FTSTAMP
			INNER JOIN TD (nolock) ON TD.NDOC = FT.NDOC
			left join cte1 on cte1.stamp1=ft.u_ltstamp
			left join cte2 on cte2.stamp2=ft.u_ltstamp2		
		WHERE	
			U_TIPODOC = 3 
			AND ft.fdata between @dataIni And @dataFim
			and ft.no>199
			and ft.site = (case when @site = '' Then ft.site else @site end)
	)
		Select * from cteVendas
		UNION ALL
		Select * from cteInsercaoReceitas
END


GO
GRANT EXECUTE on dbo.up_relatorio_ranking_stocks TO PUBLIC
GRANT CONTROL on dbo.up_relatorio_ranking_stocks TO PUBLIC
GO
