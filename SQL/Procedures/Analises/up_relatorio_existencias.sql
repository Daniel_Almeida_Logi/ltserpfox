/*
Rel�torio de Existencias

exec up_relatorio_existencias 'Loja 1',0

nota:	@tipoRel existe para quando se envia pelo sendDocuments pois necessita de cabe�alhos
*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_relatorio_existencias]') IS NOT NULL
drop procedure dbo.up_relatorio_existencias
go

create procedure up_relatorio_existencias
@site				varchar(254),
@tipoRel			int=0

/* WITH ENCRYPTION */
AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#resultFinal'))
		DROP TABLE #resultFinal

		SELECT DISTINCT
			st.ref											AS 'CNP',
			st.design										AS 'Nome', 
			   ISNULL(STUFF((SELECT ', ' + Ltrim(Rtrim(bc.codigo)) 
          FROM bc 
          WHERE 	bc.ref = st.ref 
						and bc.site_nr=st.site_nr
          FOR XML PATH('')), 1, 1, ''),'')						AS 'CodAlternativo', 
			stock											AS 'Stock',
			epcult											AS 'PCL', 
			epcusto											AS 'PCT',
			st.epv1											AS 'PVP',
			st.faminome										AS 'Familia',
			getdate()										AS 'DataExtracao'
		INTO 
			#resultFinal
		FROM st (NOLOCK)
			inner join empresa(NOLOCK) on empresa.no = st.site_nr
			left join bc (NOLOCK) on bc.ref = st.ref
		WHERE (isnull(empresa.site,'') in (Select items from dbo.up_splitToTable(@site,',')) or @site = '')
			and st.inactivo = 0

	IF(@tipoRel = 0)
	BEGIN
		SELECT 
			* 
		FROM 
			#resultFinal
	END
	ELSE
	BEGIN
		SELECT
			'CNP'				AS 'CNP',
			'Nome'				AS 'Nome', 
			'CodAlternativo'	AS 'CodAlternativo',
			'Stock'				AS 'Stock',
			'PCL'				AS 'PCL', 
			'PCT'				AS 'PCT',
			'PVP'				AS 'PVP',
			'Familia'			AS 'Familia',
			'DataExtracao'		AS 'DataExtracao'
		UNION ALL
		SELECT 
			CNP,
			Nome, 
			CodAlternativo,
			convert(varchar(254),Stock),
			convert(varchar(254),PCL), 
			convert(varchar(254),PCT),
			convert(varchar(254),PVP),
			Familia,
			convert(varchar(50),DataExtracao)
		FROM 
			#resultFinal
	END


	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#resultFinal'))
		DROP TABLE #resultFinal

GO
Grant Execute On dbo.up_relatorio_existencias to Public
Grant Control On dbo.up_relatorio_existencias to Public
Go

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

