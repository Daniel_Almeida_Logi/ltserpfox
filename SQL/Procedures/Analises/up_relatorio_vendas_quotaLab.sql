/* Relatório Vendas quota por laboratório

	exec up_relatorio_vendas_quotaLab '19000101','20190912','','','Loja 1', 1, '', 0, 0,'',''
	exec up_relatorio_vendas_quotaLab '20230201','20230202','','','Loja 1', 1, '', 0, 0,'Bial',''	 
	exec up_relatorio_vendas_quotaLab '20230101','20230227','','','Loja 1', 0, '', 0, 0,'',''	
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_vendas_quotaLab]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_quotaLab
go

create procedure dbo.up_relatorio_vendas_quotaLab
@dataIni		as datetime,
@dataFim		as datetime,
@familia		as varchar(max),
@atributosFam	as varchar(max),
@site			as varchar(55),
@ivaincl		as bit = 1, 
@generico		varchar(18),
@incluiHistorico	bit,
@simplificado		bit,
@lab			as varchar(254),
@marca			as varchar(254)
/* with encryption */
AS 
BEGIN
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempcteFamilia'))
		DROP TABLE #tempcteFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteSt'))
		DROP TABLE #cteSt
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteSeries'))
		DROP TABLE #cteSeries
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteDados_1'))
		DROP TABLE #cteDados_1
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteDados_2'))
		DROP TABLE #cteDados_2
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteDados_3'))
		DROP TABLE #cteDados_3
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#resultFinal'))
		DROP TABLE #resultFinal
	
	SET @atributosFam = (CASE WHEN (@atributosFam IS NULL OR @atributosFam = '') THEN ';;;' ELSE @atributosFam END)

	DECLARE @departamento VARCHAR(254) = ''
	SET @departamento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 1),'') 
	DECLARE @sessao       VARCHAR(254) = ''
	set @sessao = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 2) ,'')
	DECLARE @categoria    VARCHAR(254) = ''
	SET @categoria = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 3),'') 
	DECLARE @segmento     VARCHAR(254) = ''
	SET @segmento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 4),'') 

	/* Convereter Valor Generico*/
	IF @generico = 'GENERICO'
		set @generico = 1
	IF @generico = 'NĂO GENERICO'
		set @generico = 0
	/* Calc Loja e Armazens */
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)
	/* Calc Familia Infarmed */
	Select 
		distinct ref, nome
	into
		#tempcteFamilia
	From 
		stfami (nolock) 
	Where 
		ref in (Select items from dbo.up_splitToTable(@familia,','))
		Or @familia = '0' 
		or @familia = ''
	/* Calc Dados ST*/
	Select
		st.ref 
		,st.design
		,usr1
		,u_lab = isnull(u_lab,'') 	
		, ISNULL(A.descr,'') as departamento
		, ISNULL(B.descr,'') as seccao
		, ISNULL(C.descr,'') as categoria
		, ISNULL(D.descr,'') as segmento
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 1) valDepartamento
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 2) valSeccao
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 3) valCategoria
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 4) valSegmento
		,generico = isnull(fprod.generico,0)
	into
		#cteSt
	From
		st (nolock)
		left join fprod (nolock) on st.ref = fprod.cnp		
		left join grande_mercado_hmr (nolock) A on A.id = st.u_depstamp
		left join mercado_hmr (nolock) B on B.id = st.u_secstamp
		left join categoria_hmr (nolock) C on C.id = st.u_catstamp 
		left join segmento_hmr (nolock) D on D.id = st.u_segstamp	
	Where	
		site_nr = @site_nr	
		and stns = 0
		and st.inactivo = 0

	/* Calc Series faturacao */
	Select 
		*
	into 
		#cteSeries 
	from (
		select 
			distinct serie_Ent from empresa (nolock)
		union all 
		select 
			distinct serie_EntSNS from empresa (nolock)	
	) as x

	/* Define se vais mostrar resultados abaixo do Cliente 199 */
	declare @Entidades as bit
	set @Entidades = case when (select tipoempresa from empresa where site = @site) in ('FARMACIA', 'PARAFARMACIA') then convert(bit,1) else convert(bit,0) end
	
	create table #dadosVendasBase (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)
	)
    insert #dadosVendasBase
    exec up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site, @incluiHistorico
	
	/* Elimina Vendas tendo em conta tipo de cliente*/
	If @Entidades = 1 and @incluiHistorico = 0
		delete from #dadosVendasBase where #dadosVendasBase.no < 199
	
	/* DOCS DE FACTURACAO */
	SELECT
		NrVendas = CASE WHEN fi.tipodoc=1 and fi.ndoc not in (select * from #cteSeries) THEN 1 ELSE 0 END
		,fi.fdata
		,Ano = YEAR(fi.fdata)
		,Mes = MONTH(fi.FDAta)
		,etiliquido_cIva = fi.etiliquido
		,etiliquido_sIva = fi.etiliquidoSiva
		,u_ettent1_sIva  = fi.ettent1siva
		,u_ettent2_sIva	 = fi.ettent2siva
		,ettent1		 = ettent1
		,ettent2		 = ettent2
		,epcpond		= epcpond
		,u_EPVP			= u_epvp
		,margemSiva		= (Fi.etiliquidoSiva + fi.ettent1siva + fi.ettent2siva) - epcpond
		,margem			= (etiliquidoSiva + ettent1siva + ettent2siva) - epcpond
		,TotalDescontos =  descvalor
		,'ousrhora'		= fi.ousrhora
		,'ousrinis'		= fi.ousrinis
		,faminome		= isnull(#tempcteFamilia.nome,'')
		,qtt			=  CASE WHEN fi.tipodoc = 3 THEN -qtt ELSE qtt END
		,fi.ref
		,fi.no
		,u_lab = isnull(u_lab,'')
		,usr1 = ISNULL(usr1,'')
	INTO
		#cteDados_1
	FROM	
		#dadosVendasBase fi (nolock) 
		Left Join #cteSt ON fi.ref = #cteSt.ref
		Left join #tempcteFamilia on fi.familia = #tempcteFamilia.ref
	WHERE	
		(fi.tipodoc!=4 or fi.u_tipodoc=4) AND fi.u_tipodoc != 1 AND fi.u_tipodoc != 5 
		AND fdata between @dataIni and @dataFim
		AND fi.familia in (Select ref from #tempcteFamilia)
		AND departamento = (CASE WHEN @departamento = '' THEN departamento ELSE @departamento END)
		AND seccao = (CASE WHEN @sessao = '' THEN seccao ELSE  @sessao END)
		AND categoria = (CASE WHEN @categoria = '' THEN categoria ELSE  @categoria END)
		AND segmento = (CASE WHEN @segmento = '' THEN segmento ELSE  @segmento END) 
		and isnull(#cteSt.generico,0) = case when @generico = '' then isnull(#cteSt.generico,0) else @generico end
		and (fi.no >= 199	or @Entidades = 0)
		and (ISNULL(usr1,'') in (Select items from dbo.up_splitToTable(@marca, ',')) or @marca = '')

--select * from #cteDados_1 

	/* declara varial que vai guardar o total de vendas no periodo p dps calcular a % de vendas */
	declare @totalVendasPeriodo as numeric(19,6)
	set @totalVendasPeriodo = (SELECT SUM(CASE WHEN  @ivaincl = 1 THEN  (etiliquido_cIva + ettent1 + ettent2)
									ELSE (etiliquido_sIva + u_ettent1_sIva + u_ettent2_sIva)END)as total from #cteDados_1)

	/* Prepara Result Set */
		SELECT	
			MARGEM				= ROUND(SUM(isnull(margem,0)),2)
			,TOTAL				= ROUND(SUM(CASE WHEN @ivaincl = 1 THEN (etiliquido_cIva + ettent1 + ettent2)
												ELSE (etiliquido_sIva + u_ettent1_sIva + u_ettent2_sIva)END),2)
			,PERCENTTOTAL       = ROUND((SUM(CASE WHEN @ivaincl = 1 THEN (etiliquido_cIva + ettent1 + ettent2)
												ELSE (etiliquido_sIva + u_ettent1_sIva + u_ettent2_sIva)END)
												* 100) / @totalVendasPeriodo,2)
			,TOTAL_sIVA			= ROUND(case when @ivaincl = 0 then SUM(etiliquido_sIva) + SUM(u_ettent1_sIva) + SUM(u_ettent2_sIva) 
										else SUM(etiliquido_cIva) + SUM(ettent1) + SUM(ettent2) end,2)
			,totalSempreSIva	= ROUND(SUM(etiliquido_sIva) + SUM(u_ettent1_sIva) + SUM(u_ettent2_sIva),2) 
			,UTENTE				= ROUND(SUM(case when @ivaincl = 1 then etiliquido_cIva else etiliquido_sIva end),2)
			,COMPARTICIPACAO	= ROUND(SUM(CASE WHEN @ivaincl = 1 THEN ettent1 + ettent2 ELSE u_ettent1_sIva + u_ettent2_sIva END),2)
			,EPVP				= ROUND(SUM(u_epvp),2)
			,EPCP				= ROUND(SUM(epcpond),2)
			,nrVendas			= SUM(NrVendas)
			,qttVendida			= convert(int,sum(qtt))
			,laboratorio		= u_lab
		INTO 
			#cteDados_3
		FROM
			#cteDados_1 a
		GROUP BY 
			u_lab
		ORDER BY 
			u_lab asc

	-- prepara perce set final	
	Select
		nrLabs = (select count(*) from #cteDados_3)
		,MARGEM_PERC = CASE WHEN TOTAL_sIVA <= 0 or MARGEM < 0 then 0 else (((totalSempreSIva - EPCP)*100)/totalSempreSIva) end
	,*
	INTO 
		#resultFinal
	From
		#cteDados_3

--select * from #resultFinal

	SELECT
		*
	FROM
		#resultFinal
	WHERE 
		(ISNULL(laboratorio,'') in (Select items from dbo.up_splitToTable(@lab,';')) or @lab = '')

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempcteFamilia'))
		DROP TABLE #tempcteFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteSt'))
		DROP TABLE #cteSt
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteSeries'))
		DROP TABLE #cteSeries
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteDados_1'))
		DROP TABLE #cteDados_1
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteDados_2'))
		DROP TABLE #cteDados_2
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteDados_3'))
		DROP TABLE #cteDados_3
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#resultFinal'))
		DROP TABLE #resultFinal
END


GO
Grant Execute on dbo.up_relatorio_vendas_quotaLab to Public
Grant control on dbo.up_relatorio_vendas_quotaLab to Public
GO