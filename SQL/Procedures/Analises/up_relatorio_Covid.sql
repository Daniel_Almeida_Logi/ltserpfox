/* 
	

	exec up_relatorio_Covid '20210501', '20210724','Loja 1'
	exec up_relatorio_Covid '20210531', '20210606','Loja 1'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_relatorio_Covid]') IS NOT NULL
	drop procedure up_relatorio_Covid
go

create PROCEDURE up_relatorio_Covid
	@dateInit datetime,
	@dateEnd  datetime,
	@site     VARCHAR(max)

	
/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

	set @dateEnd = @dateEnd + '23:59:00'

		 select 
			fi.ref,
			floor(sum(case when fi.tipodoc=3 then  fi.qtt*-1 else fi.qtt end)) as qtt		
		 from ft (nolock)
		 inner join fi  (nolock)     on ft.ftstamp = fi.ftstamp
		 inner join empresa (nolock) on empresa.site = ft.site
		 left  join fprod(nolock)    on fprod.ref=fi.ref
		 where  
			ft.SITE=@site AND 
			fprod.grupo='SR' AND ft.tipodoc in (1,3)  AND fdata between @dateInit and @dateEnd
		 group by  fi.ref
		 
GO
Grant Execute On up_relatorio_Covid to Public
Grant Control On up_relatorio_Covid to Public
GO



