/*

Linhas dispensadas sem picagem de qrcode

select * from fi where ftstamp = 'fBAE3720E-BEB3-4CF9-8AD'

select * from fi_trans_info_faltas

exec up_relatorio_sem_picagem '22134887667001'
*/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('[dbo].[up_sem_picagem ]') IS NOT NULL
	drop procedure dbo.up_sem_picagem 
GO

CREATE PROCEDURE [dbo].up_sem_picagem 
	@nrAtendimento varchar(60)

/* with encryption */
AS
SET NOCOUNT ON

	DECLARE @ftstamp VARCHAR(50);
	DECLARE @ref VARCHAR(50);
	DECLARE @dispositivoSeguranca BIT;

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempFi'))
		DROP TABLE #tempFi

	-- Obter os registros de 'fi' relacionados ao número de atendimento
	SELECT 
		fi.ref, 
		fi.fistamp, 
		fi.lote, 
		fi2.dataValidade, 
		fi2.productCodeEmbal, 
		fi.fivendedor, 
		fi.fivendnm, 
		ft.site,
		ft.pno,
		ft.pnome
	INTO #tempFi
	FROM fi
	LEFT JOIN fi2 ON fi.fistamp = fi2.fistamp
	INNER JOIN fprod ON fprod.cnp = fi.ref
	INNER JOIN ft ON ft.ftstamp = fi.ftstamp
	WHERE 
		ft.u_nratend = @nrAtendimento -- Filtrar pelo número de atendimento
		AND fprod.dispositivo_seguranca = 1; -- Somente registros com dispositivo de segurança ativo


	-- Verificar se a tabela temporária possui registros
	IF EXISTS(SELECT 1 FROM #tempFi)
	BEGIN
		-- Processar cada registro na tabela temporária
		INSERT INTO fi_trans_info_faltas(fi_trans_info_faltasStamp, fistamp, lote, dataValidade, packsn, productCode, operadorno, operador, site, terminal, terminalno, atendimento, ousrdata)
		SELECT 
			LEFT(NEWID(), 25), 
			fi.fistamp, 
			fi.lote, 
			fi.dataValidade, 
			'', 
			'', 
			fi.fivendedor, 
			fi.fivendnm, 
			fi.site,
			pnome,
			pno,
			@nrAtendimento,
			GETDATE()
		FROM #tempFi fi
		LEFT JOIN fi_trans_info fiInfo ON fi.fistamp = fiInfo.recStamp
		WHERE 
			fiInfo.recStamp IS NULL -- Não foi picado
			AND NOT EXISTS (
				SELECT 1 
				FROM fi_trans_info_faltas faltas
				WHERE faltas.fistamp = fi.fistamp
			);

		
	END

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempFi'))
		DROP TABLE #tempFi
GO
Grant Execute On up_sem_picagem  to Public
Grant Control On up_sem_picagem  to Public
GO