/*	
	Inventário para enviar para a AT

	exec up_relatorio_at_inventario '20191231',1

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
if OBJECT_ID('[dbo].[up_relatorio_at_inventario]') IS NOT NULL
	drop procedure dbo.up_relatorio_at_inventario
go

create procedure dbo.up_relatorio_at_inventario
	@data datetime
	,@site_nr tinyint
/* WITH ENCRYPTION */
AS
BEGIN
	If OBJECT_ID('tempdb.dbo.#tmp_ean') IS NOT NULL
		drop table #tmp_ean;
	-- armazem da empresa
	--declare @armazem numeric(3) 
	--set @armazem = (select top 1 armazem from empresa_arm (nolock) where empresa_no = @site_nr) 
	-- códigos alternativos (EAN's)
	select ref,ean into #tmp_ean from (
		select
			st.ref
			,ean = bc.codigo
			, st.epcpond
			,r = row_number() over (partition by st.ref order by len(bc.codigo) desc)
		from
			st (nolock)
			inner join bc (nolock) on bc.ref=st.ref
		where
			st.site_nr = @site_nr
			and len(bc.codigo) in (13,8)
	) x
	where r = 1
	select
		ProductCategory
		,ProductCode = rtrim(ProductCode), ProductDescription = rtrim(ProductDescription), ProductNumberCode = rtrim(ProductNumberCode)
		,ClosingStockQuantity, UnitOfMeasure
	from (
		Select
			ProductCategory = 'M'
			,ProductCode = st.ref
			,ProductDescription = st.design
			,ProductNumberCode = case when ean.ean is null or ean.ean='' then st.ref else ean.ean end
			,ClosingStockQuantity = REPLACE(CONVERT(varchar(30),convert(numeric(15,2),SUM(Case When cm <50 Then qtt Else -qtt End)), 0), '.', ',')
			,UnitOfMeasure = 'Caixa'
		--	,ClosingStockValue = REPLACE(CONVERT(varchar(30),convert(numeric(15,2),SUM(Case When cm <50 Then qtt Else -qtt End)* ISNULL(dbo.uf_retorna_valorizacao_produto(st.epcpond, st.epcult, st.epcusto),0)), 0), '.', ',')
		from
			sl (nolock)
			inner join st (nolock) on st.ref=sl.ref and st.site_nr = @site_nr
			left join #tmp_ean ean on ean.ref=st.ref
		Where
			datalc <= @data
			--and st.inactivo=0
			and st.stns=0
			and sl.armazem in  (select top 1 armazem from empresa_arm (nolock) where empresa_no = @site_nr) 
		group by
			st.ref, st.design, ean.ean, st.epcpond, st.epcult, st.epcusto 
	) x
	where
		convert(numeric(15,2),REPLACE(CONVERT(varchar(30), ClosingStockQuantity, 0), ',', '.')) > 0
	order by
		ProductCode
	If OBJECT_ID('tempdb.dbo.#tmp_ean') IS NOT NULL
		drop table #tmp_ean;
END


GO
Grant Execute on dbo.up_relatorio_at_inventario to Public
Grant control on dbo.up_relatorio_at_inventario to Public
Go