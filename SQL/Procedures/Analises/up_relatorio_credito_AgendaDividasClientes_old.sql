/* Relat�rio de Dividas de Clientes Mensal      
	
	exec up_relatorio_credito_AgendaDividasClientes 201, 0, '20171211', 0, 0,'Loja 1', 0, ''

	select esaldo,* from b_utentes where no = 5817 order by estab asc
	
*/ 

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_relatorio_credito_AgendaDividasClientes]') IS NOT NULL
	drop procedure dbo.up_relatorio_credito_AgendaDividasClientes
go

create procedure dbo.up_relatorio_credito_AgendaDividasClientes
@no					as varchar(9),
@estab				as numeric(5,0),
@dataIni			as datetime,
@excluiEntidades	bit,
@saldo				as numeric(9,2),
@site				varchar(55),
@excluiInativos		bit = 0,
@tipoCliente		varchar(20)

/* with encryption */
AS
BEGIN	

	set language portuguese	

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteSaldoAnterior'))
		DROP TABLE #cteSaldoAnterior
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteCC'))
		DROP TABLE #cteCC
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteDadosCompletos'))
		DROP TABLE #cteDadosCompletos
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteRecebidoMesAtual'))
		DROP TABLE #cteRecebidoMesAtual


	set @no = case when @no = '' then '0' else @no end

	-- temp table com Saldo Anterior
	select
		cc.nome, 
		cc.no,
		cc.estab, 
		saldo = isnull((SUM((ecred-ecredf))- SUM((edeb-edebf)))*-1,0)
	into 
		#cteSaldoAnterior
	from 
		cc (nolock)
		left join ft (nolock) on ft.ftstamp = cc.ftstamp 
		left join re (nolock) on re.restamp = cc.restamp 
	where 
		--(cc.edeb-cc.edebf-(cc.ecred-cc.ecredf)) != 0 
		--and 
		cc.no = case when @no = 0 then cc.no else @no end
		and cc.estab = case when @estab = 0 then cc.estab else @estab end
		and cc.datalc < @dataIni
		and (
			ft.site = (case when @site = '' Then ft.site else @site end)
			Or
			re.site = (case when @site = '' Then ft.site else @site end)
			)
	Group by
		cc.nome
	--	,cc.moeda
		,cc.no
		,cc.estab
	order by 
		cc.no, cc.estab, cc.nome--, cc.moeda
	
	-- temp table Conta Corrente 
	select 
		cc.moeda
		,nome = cc.nome + '[' + LTRIM(STR(cc.no)) + '][' +  LTRIM(STR(cc.estab))  +']'
		,cc.no
		,cc.estab
		,saldo = isnull((SUM((ecred-ecredf))- SUM((edeb-edebf)))*-1,0)
		,ano = YEAR(cc.datalc)
		,mes = upper(month(cc.datalc)) + ' - ' + UPPER(DATENAME(month,cc.datalc))
	into 
		#cteCC
	from 
		cc (nolock)
		left join ft (nolock) on ft.ftstamp = cc.ftstamp 
		left join re (nolock) on re.restamp = cc.restamp 
	where 
		--(cc.edeb-cc.edebf-(cc.ecred-cc.ecredf)) != 0 
		--and 
		cc.no = case when @no = 0 then cc.no else @no end
		and cc.estab = case when @estab = 0 then cc.estab else @estab end
		and cc.datalc >= @dataIni
		and (
			ft.site = (case when @site = '' Then ft.site else @site end)
			Or
			re.site = (case when @site = '' Then ft.site else @site end)
		)
	group by 
		cc.nome
		,cc.moeda
		,cc.no
		,cc.estab
		,year(cc.datalc)
		,upper(month(cc.datalc)) + ' - ' + UPPER(DATENAME(month,cc.datalc))
	order by 
		cc.no, cc.estab, cc.nome, cc.moeda

	-- temp table com dados totais
	Select 
		saldoAnterior = isnull(#cteSaldoAnterior.saldo,0)
		,nome = nome
		,no
		,estab
		,saldo = isnull(#cteSaldoAnterior.saldo,0) 
		,ano =  'Anterior a ' + LEFT(CONVERT(VARCHAR, @dataIni, 120), 10) 
		,mes = '' 
	into 
		#cteDadosCompletos
	from 
		#cteSaldoAnterior
	
	union all 
			
	Select 
		saldoAnterior = 0
		,b_utentes.nome
		,b_utentes.no
		,b_utentes.estab
		,saldo = isnull(#cteCC.saldo,0)
		,ano =  case when ano is null then 'Anterior a ' + LEFT(CONVERT(VARCHAR, @dataIni, 120), 10) else 'Z_' + STR(ano) end
		,mes =   case when mes is null then '' else mes end
	from 
		b_utentes (nolock)
		left join #cteCC on b_utentes.no = #cteCC.no  and b_utentes.estab = #cteCC.estab

	-- recebido mes em que � gerado o relat�rio (na loja seleccionada)
	select 
		no
		,estab
		,TotalRecebido = sum(etotal) 
	into 
		#cteRecebidoMesAtual
	from 
		re (nolock)
	where 
		re.no = case when @no = 0 then no else @no END
		AND re.estab = case when @estab = 0 then estab else @estab END
		AND re.ousrdata between (DATEADD(month, DATEDIFF(month, 0, getdate()), 0)) and (DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,getdate())+1,0)))
		and re.site = @site
	group by 
		no
		,estab	

	-- result set final 
	select 
		#cteDadosCompletos.saldoAnterior
		,#cteDadosCompletos.ano
		,#cteDadosCompletos.mes
		,saldo=(select sum((cc1.edeb-cc1.edebf)-(cc1.ecred-cc1.ecredf)) from cc cc1 where cc1.no=#cteDadosCompletos.no and cc1.estab=#cteDadosCompletos.estab and cc1.datalc <= @dataIni)
		--,#cteDadosCompletos.saldo
		,nome = LTRIM(RTRIM(#cteDadosCompletos.nome)) + '[' + LTRIM(STR(#cteDadosCompletos.no)) + '][' +  LTRIM(STR(#cteDadosCompletos.estab))  +']'
		,ISNULL(#cteRecebidoMesAtual.totalrecebido,0) as recebidomesatual
	from 
		#cteDadosCompletos
		left join b_utentes (nolock) on b_utentes.no = #cteDadosCompletos.no and #cteDadosCompletos.estab = b_utentes.estab
		left join #cteRecebidoMesAtual on #cteRecebidoMesAtual.no = #cteDadosCompletos.no and #cteRecebidoMesAtual.estab = #cteDadosCompletos.estab
	Where
		b_utentes.no > case when @excluiEntidades = 1 then 200 else b_utentes.no - 1  end
		and b_utentes.no = case when @no = 0 then b_utentes.no else @no end
		and b_utentes.estab = case when @estab = 0 then b_utentes.estab else @estab end
		and isnull(b_utentes.esaldo,0) >= @saldo
		and b_utentes.inactivo = case when @excluiInativos = 1 then 0 else b_utentes.inactivo end
		and b_utentes.tipo = case when @tipoCliente = '' then b_utentes.tipo else @tipoCliente end
		/* Condi��o correta ?? - feito assim para LTS*/
			and (#cteDadosCompletos.saldo != 0 or ISNULL(#cteRecebidoMesAtual.totalrecebido,0) != 0)

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteSaldoAnterior'))
		DROP TABLE #cteSaldoAnterior
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteCC'))
		DROP TABLE #cteCC
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteDadosCompletos'))
		DROP TABLE #cteDadosCompletos
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteRecebidoMesAtual'))
		DROP TABLE #cteRecebidoMesAtual


END
GO
Grant Execute on dbo.up_relatorio_credito_AgendaDividasClientes to Public
Grant control on dbo.up_relatorio_credito_AgendaDividasClientes to Public
GO
