/* Lista an�lises para Painel

	 exec up_analises_Lista

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_analises_Lista]') IS NOT NULL
	drop procedure dbo.up_analises_Lista
go

create procedure dbo.up_analises_Lista

/* WITH ENCRYPTION */

AS 
BEGIN

	;with ctePerfis as(
		select 
			pf.resumo
			,pfustamp = isnull(pfu.pfustamp,'')
			,pfgstamp = isnull(pfg.pfgstamp,'')
			,pf.pfstamp
			,username = isnull(pfu.username,'')
			,userno = isnull(pfu.userno,0)
			,grupo = ISNULL(pfg.nome,'')
		from 
			b_pf pf (nolock)
			left join b_pfu pfu (nolock) on pfu.pfstamp=pf.pfstamp
			left join b_pfg pfg (nolock) on pfg.pfstamp=pf.pfstamp
	)

	Select 
		pesquisa = CONVERT(bit,0),
		ordem,
		grupo, 
		subgrupo, 
		descricao,
		report,
		formatoExp,
		perfil,
		comandoFox,
		objectivo = CONVERT(varchar(254),objectivo)
	from 
		B_analises (nolock) 
	where 
		estado = 1
	order by
		grupo, descricao

END

GO
Grant Execute on dbo.up_analises_Lista to Public
GO
Grant Control on dbo.up_analises_Lista to Public
GO