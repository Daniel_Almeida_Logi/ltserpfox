-- Relat�rio Conferencia Invent�rio antes Lan�amento
-- exec up_relatorio_controle_pic '20170101','20180130'

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_controle_pic]') IS NOT NULL
	drop procedure dbo.up_relatorio_controle_pic
go

create procedure dbo.up_relatorio_controle_pic
@dataIni		as datetime
,@dataFim		as datetime

/* with encryption */

AS 
BEGIN

	select
	   'Doc' = ft.nmdoc
	   ,fi.fno
	   ,fi.ref
	   ,fi.design
	   ,'Prefvenda' = fi.u_epref
	  -- ,'Dem' = case when ISNULL(LTRIM(RTRIM(fi.id_dispensa_eletronica_d )),'')!='' then 1
	--		   else 0
	--		   end
	   ,Tipolote1 = u_tlote
	   ,Tipolote2 = u_tlote2
	   ,fi.u_epvp
		--,activo = isnull((SELECT top 1 preco from hist_precos where ativo=1 and hist_precos.ref= fi.ref and hist_precos.preco=fi.u_epvp),-1)
		,hist_precos.preco as precohist
		,fi.ousrdata
		,fi.pic
    
	from fi(nolock)
	inner join
		ft(nolock) on ft.ftstamp=fi.ftstamp
	left join
		hist_precos(nolock) on hist_precos.ref=(SELECT top 1 ref from hist_precos(nolock) where ativo=1 and hist_precos.ref= fi.ref and hist_precos.preco=fi.u_epvp)
	WHERE      
	   fi.ousrdata >= @dataIni
	   AND  fi.ousrdata <= @dataFim
	   AND (ft.nmdoc='Venda a Dinheiro' or  ft.nmdoc='Inser��o de Receita' or ft.nmdoc='Factura')
	   AND hist_precos.preco is null
	   AND fi.pic>0
	   and (u_tlote not in ('','96', '97', '98', '99') or u_tlote2 not in ('','96', '97', '98', '99'))


-- exec up_relatorio_controle_pic '20170101','20180130'
END

GO
Grant Execute on dbo.up_relatorio_controle_pic to Public
Grant control on dbo.up_relatorio_controle_pic to Public
Go