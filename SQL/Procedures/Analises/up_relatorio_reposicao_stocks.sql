/* Relatório Reposição de Stocks

	up_relatorio_reposicao_stocks '20201130', '20211130', '0', '24', '', '', '','Loja 1',-999999

	exec up_relatorio_vendas_base_detalhe '20180130', '20211231', 'Loja 1'
	
*/

if OBJECT_ID('[dbo].[up_relatorio_reposicao_stocks]') IS NOT NULL
	drop procedure dbo.up_relatorio_reposicao_stocks
go

create procedure [dbo].[up_relatorio_reposicao_stocks]
	@dataIni		datetime
	,@dataFim		datetime
	,@horaini		varchar(2)=''
	,@horafim		varchar(2)=''
	,@local1		varchar(90)=''
	,@local2		varchar(90)=''
	,@familia		varchar(90)=''
	,@site			varchar(55)=''
	,@stock			numeric(9,2)

/* with encryption */

AS
SET NOCOUNT ON

	declare @horainiVal NUMERIC(2,0)
	declare @horafimVal NUMERIC(2,0)

	IF(@horaini!='')
	BEGIN 
		SET @horainiVal = CONVERT(NUMERIC(2,0),@horaini)
	END
	ELSE
	BEGIN
		SET @horainiVal = CONVERT(NUMERIC(2,0),0)
	END
	IF(@horafim!='')
	BEGIN 
	print 1
		SET @horafimVal = CONVERT(NUMERIC(2,0),@horafim)
	END
	ELSE
	BEGIN
		print 2
		SET @horafimVal = CONVERT(NUMERIC(2,0),0)
	END 


	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRefs'))
		DROP TABLE #dadosRefs
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
		DROP TABLE #dadosSt	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseFiltro'))
		DROP TABLE #dadosVendasBaseFiltro	
	
	/* Calc Loja e Armazens */
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)

	/* Dados Base Vendas  */
	create table #dadosVendasBase (
		nrAtend varchar(20) -- passei de 14 para 16 em 20200824 pois truncava dados no chamamento da up_relatorio_vendas_base_Detalhe..
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)

	)
    
    insert #dadosVendasBase
    exec up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site

	/* Elimina Vendas tendo em conta tipo de cliente*/
	delete from #dadosVendasBase where #dadosVendasBase.no < 199

	/* Preparar Result Set */
	select 
		  #dadosVendasBase.ref
		, #dadosVendasBase.design
		, st.local
		, st.u_local
		, st.stock
		,ISNULL(SUM(#dadosVendasBase.qtt),0) as vendas
	from 
		#dadosVendasBase (nolock) 
		INNER JOIN TD (nolock) on TD.NDOC = #dadosVendasBase.NDOC
		INNER join st (nolock) on #dadosVendasBase.ref=st.ref 

	where 
	#dadosVendasBase.fdata+#dadosVendasBase.ousrhora  between DATEADD(hour,@horainiVal, @dataIni) and DATEADD(hour,@horafimVal,@dataFim) 
		and #dadosVendasBase.tipodoc between 1 and 3
		and st.stns=0
		and td.u_tipodoc not in (1, 5)
		and st.familia like (case when @familia='' then '%%' else @familia end)
		and st.local = (case when @local1='' then st.local else @local1 end) 
		and st.u_local = (case when @local2='' then st.u_local else @local2 end)
		and isnull(st.stock,0) > @stock
		AND st.site_nr = @site_nr
	group by
		#dadosVendasBase.ref ,#dadosVendasBase.design , st.local, st.u_local , st.stock
	order by 
		#dadosVendasBase.ref 
		
--up_relatorio_reposicao_stocks '20180101', '20181231', '', '', 'aaa', '', '','Loja 1'
	--	up_relatorio_reposicao_stocks '20180130', '20180130', '', '', '1', 'Loja 1'
	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRefs'))
		DROP TABLE #dadosRefs
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
		DROP TABLE #dadosSt

GO
Grant Execute on dbo.up_relatorio_reposicao_stocks to Public
Grant control on dbo.up_relatorio_reposicao_stocks to Public
GO