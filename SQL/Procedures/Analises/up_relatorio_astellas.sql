/* 
	devolve a informacao de Astellas 

	exec up_relatorio_astellas '2019', '8', '',''


*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_relatorio_astellas]') IS NOT NULL
	DROP PROCEDURE dbo.up_relatorio_astellas
GO

CREATE PROCEDURE dbo.up_relatorio_astellas
@ano			INT,
@mes			INT,
@infarmed		VARCHAR(18),
@tipoProduto	VARCHAR(18)


/* WITH ENCRYPTION */
AS
SET NOCOUNT ON
DECLARE @infarmedid as INT
DECLARE @totalValorEFR as numeric(19,3)
SET @infarmedid =0
IF(ISNUMERIC(@infarmed)=1)
begin
	SET @infarmedid = CONVERT(INT,@infarmed)
END

SELECT 
	@totalValorEFR=SUM(doc.totalEFR )
from   ext_esb_doc	(nolock) doc 
where id_ext_efr = '054' -- astellas 
	and doc.month = CASE WHEN @mes	= '' THEN doc.month ELSE @mes END 
	and doc.year  = CASE WHEN @ano	= '' THEN doc.year  ELSE @ano END
	and doc.id_ext_pharmacy			= CASE WHEN @infarmedid = 0  THEN doc.id_ext_pharmacy ELSE @infarmedid	END
	and invalid = 0 and test = 0

SELECT  
	doc.id_ext_pharmacy											AS codigoFarmacia,
	ISNULL(LTRIM(RTRIM(b_utentes.nome)) ,doc.name_ext_pharmacy)	AS nomeFarmacia,
	doc.address_ext_pharmacy									AS moradaFarmacia,
	doc.docNr													AS numeroFactura,
	doc.docDate													AS dataFactura,
	doc.totalEFR												AS valorFacturaEFR,
	presc_d.ref													AS codigoProduto,
	presc_d.totalPVP											AS totalBetVeso,
	presc_d.authorizationCode									AS codigoSeguranca,
	@totalValorEFR												AS totalFacturaEFR
FROM	   ext_esb_doc				(nolock) doc 
LEFT JOIN  ext_esb_prescription		(nolock) presc	 ON presc.stamp_ext_esb_doc =doc.stamp
LEFT JOIN  ext_esb_prescription_d   (nolock) presc_d ON presc_d.stamp_ext_esb_prescription =presc.stamp
INNER JOIN b_utentes                (nolock)         on b_utentes.id = CONVERT(VARCHAR(10),doc.id_ext_pharmacy)
WHERE id_ext_efr = '054' -- astellas 
	and doc.month = CASE WHEN @mes	= '' THEN doc.month ELSE @mes END 
	and doc.year  = CASE WHEN @ano	= '' THEN doc.year  ELSE @ano END
	and doc.id_ext_pharmacy			= CASE WHEN @infarmedid = 0  THEN doc.id_ext_pharmacy ELSE @infarmedid	END
	and presc_d.ref					= CASE WHEN @tipoProduto = 0 THEN presc_d.ref		  ELSE @tipoProduto	END 
	and invalid = 0 and test = 0
	and b_utentes.inactivo = 0
	ORDER BY 
		ISNULL(LTRIM(RTRIM(b_utentes.nome)) ,doc.name_ext_pharmacy) ASC

Go
Grant Execute on dbo.up_relatorio_astellas to Public
Grant Control on dbo.up_relatorio_astellas to Public
Go
