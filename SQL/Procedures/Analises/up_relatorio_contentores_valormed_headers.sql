/* Relatório Detalhe contentores valormed

	exec up_relatorio_contentores_valormed_headers
	
*/

if OBJECT_ID('[dbo].[up_relatorio_contentores_valormed_headers]') IS NOT NULL
	drop procedure dbo.up_relatorio_contentores_valormed_headers
go

create procedure [dbo].[up_relatorio_contentores_valormed_headers]

/* with encryption */

AS
SET NOCOUNT ON

	/* Preparar Result Set */
	select
	 'Container_SerialNumber' as 'Container_SerialNumber'
	, 'Container_OccuredOn' as 'Container_OccuredOn'
	, 'Container_Identifier' as 'Container_Identifier'
	, 'Container_IsCanceled' as 'Container_IsCanceled'
	, 'Container_CanceledOn' as 'Container_CanceledOn'
	, 'Container_UpdateOn' as 'Container_UpdateOn'
	, 'Pharmacy_InfarmedCode' as 'Pharmacy_InfarmedCode'
	, 'Pharmacy_Name' as 'Pharmacy_Name'
	, 'Pharmacy_VatNumber' as 'Pharmacy_VatNumber'
	, 'Pharmacy_PostalCode' as 'Pharmacy_PostalCode'
	, 'Pharmacy_Locale' as 'Pharmacy_Locale'
	, 'Pharmacy_PhoneNumber' as 'Pharmacy_PhoneNumber'
	, 'Pharmacy_Address' as 'Pharmacy_Address'
	, 'Pharmacy_District' as 'Pharmacy_District'
	, 'Pharmacy_County' as 'Pharmacy_County'
	, 'Pharmacy_Parish' as 'Pharmacy_Parish'
	, 'Retailer_InfarmedCode' as 'Retailer_InfarmedCode'
	, 'Retailer_Name' as 'Retailer_Name'
	, 'Retailer_Address' as 'Retailer_Address'
	, 'Retailer_VATNumber' as 'Retailer_VATNumber'
	, 'Retailer_ParentInfarmedCode' as 'Retailer_ParentInfarmedCode'


GO
Grant Execute on dbo.up_relatorio_contentores_valormed_headers to Public
Grant control on dbo.up_relatorio_contentores_valormed_headers to Public
GO