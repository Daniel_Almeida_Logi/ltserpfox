/* Relatório de extrato de cartao
	exec up_relatorio_ValorEmCartao '234', '0','20220407','20220623'
 	exec up_relatorio_ValorEmCartao '234', '0','20240513','20240513'

	exec up_relatorio_ValorEmCartao '20442', '0','19000101','20210528'
	exec up_relatorio_ValorEmCartao '234', '0','20220101','20220601'
	exec up_relatorio_ValorEmCartao '107902511', '1','20210427','20210428'
	exec up_relatorio_ValorEmCartao '107902511', '0','20150101','20220731'

*/
 
if OBJECT_ID('[dbo].[up_relatorio_ValorEmCartao]') IS NOT NULL
	drop procedure dbo.up_relatorio_ValorEmCartao
go

create procedure [dbo].[up_relatorio_ValorEmCartao]
	 @no			NUMERIC(10,0)
	 ,@estab			NUMERIC(3,0)=0
	 ,@dataini		DATETIME = '19000101'
	 ,@datafim		DATETIME = '19000101'	
	

/* with encryption */
AS
SET NOCOUNT ON

	DECLARE @nrcartao VARCHAR(30)
	DECLARE @textValue VARCHAR(30)

	select @textValue=textValue from B_Parameters (nolock) where stamp='ADM0000000302'

	IF (@textValue ='Valor')
	BEGIN

		SELECT TOP 1 @nrcartao = nrcartao FROM b_utentes(nolock) where no = @no and estab =@estab -- de futuro alguns clientes dependentes podem ficar com o cartão do dependente 0 e devem contar para os pontos .
		set @datafim = @datafim + '23:59:59.997'

		--print @nrcartao
	
		Declare @valorInicio NUMERIC(15,2)
		Declare @valorFT NUMERIC(15,2)
		Declare @valorb_ocorr NUMERIC(15,2)
		DECLARE @totalPrimeiroDia NUMERIC(20,2)
		SET @valorInicio = ISNULL((SELECT top 1 ISNULL(valcartaoini,0) from b_fidel (nolock) where nrcartao =@nrcartao),0)


		SELECT 
			@valorFT = ISNULL(sum(FI.valcartao),0)	
		from FT (NOLOCK)
			INNER JOIN b_utentes(NOLOCK) ON  FT.no = b_utentes.no and FT.estab =  b_utentes.estab  
			INNER JOIN ft2 (NOLOCK) ON FT.ftstamp= ft2.ft2stamp
			INNER JOIN FI (NOLOCK) ON FT.ftstamp= fI.ftstamp
		WHERE FI.valcartao !=0 AND  b_utentes.nrcartao=@nrcartao
				and FT.fdata < @dataini + '00:00:00.000' 

	-- entidades facturadoras 
		SELECT 
				@valorFT = ISNULL(sum(FI.valcartao),0) + @valorFT	
			from FT (NOLOCK)
				INNER JOIN b_utentes(NOLOCK) ON  FT.u_hclstamp = b_utentes.utstamp
				INNER JOIN ft2 (NOLOCK) ON FT.ftstamp= ft2.ft2stamp
				INNER JOIN FI (NOLOCK) ON FT.ftstamp= fI.ftstamp
			WHERE FI.valcartao !=0  and b_utentes.nrcartao=@nrcartao and fi.ofistamp ='' 
				and FT.fdata < @dataini + '00:00:00.000'  and u_hclstamp !=''

			-- entidades regularização 
			SELECT 
				@valorFT = ISNULL(sum(FI.valcartao),0) + @valorFT
			from FT (NOLOCK)
				 INNER JOIN FI			(NOLOCK) ON	fi.ftstamp = Ft.ftstamp 
				 INNER JOIN fi fii		(NOLOCK) ON fii.fistamp = Fi.ofistamp 
				 INNER JOIN FT ftt		(NOLOCK) ON ftt.ftstamp= fii.ftstamp
				INNER JOIN b_utentes	(NOLOCK) ON  b_utentes.utstamp = ftt.u_hclstamp
			WHERE FI.valcartao !=0  and b_utentes.nrcartao=@nrcartao and FI.ofistamp !=''
				and FT.fdata < @dataini + '00:00:00.000'  and ftt.u_hclstamp !=''
			
			/* ft data vou buscar o valor, verifico o registo e sumo os valores desse dia */

	print @valorFT

		SELECT 
				@valorb_ocorr = ISNULL(SUM(B_ocorrencias.nValor),0)
			from B_ocorrencias (NOLOCK)
				INNER JOIN b_utentes(NOLOCK) ON  B_ocorrencias.linkStamp = b_utentes.utstamp and  B_ocorrencias.Tipo='Cartão Cliente'
			WHERE   
				 B_ocorrencias.date <= @dataini + '00:00:00.000'  and b_utentes.nrcartao=@nrcartao


		SET @totalPrimeiroDia = ISNULL(@valorInicio,0) + ISNULL(@valorFT,0) + ISNULL(@valorb_ocorr,0)

		print @valorb_ocorr
		print @totalPrimeiroDia

		SELECT 
			 data,
			 DocFno,
			 REF,
			 isNull(valor,0) as valor, 
			 sum( isNull( valor, 0))  over (order by t.data,docFNO ,ref asc	) as saldo
		 from (

	 		SELECT TOP 1
				  DATEADD(DAY,-1 ,@dataini) + '23:59:59.997'					AS data,
				 ''																AS DocFno,
				 isNull(@totalPrimeiroDia,0)									AS valor,
				 ''																AS REF
			FROM b_utentes(NOLOCK) 
			WHERE   b_utentes.nrcartao=@nrcartao

			union all

			
			SELECT 
				 FT.fdata + FT.ousrhora											AS data,
				 CONVERT(VARCHAR(20),ft.fno)									AS DocFno,
				 isnull(FI.valcartao,0)											AS valor,
				 ref															AS REF
			from FT (NOLOCK)
				INNER JOIN b_utentes(NOLOCK) ON  FT.no = b_utentes.no and FT.estab =  b_utentes.estab  
				INNER JOIN ft2 (NOLOCK) ON FT.ftstamp= ft2.ft2stamp
				INNER JOIN FI (NOLOCK) ON FT.ftstamp= fI.ftstamp
			WHERE FI.valcartao !=0  
				and FT.fdata BETWEEN @dataini AND @datafim and u_hclstamp =''
				AND  b_utentes.nrcartao=@nrcartao



			union all

			-- entidades facturadoras 
			SELECT 
				 FT.fdata + FT.ousrhora											AS data,
				 CONVERT(VARCHAR(20),ft.fno)									AS DocFno,
				 isnull(FI.valcartao,0)											AS valor,
				 ref															AS REF
			from FT (NOLOCK)
				INNER JOIN b_utentes(NOLOCK) ON  FT.u_hclstamp = b_utentes.utstamp
				INNER JOIN ft2 (NOLOCK) ON FT.ftstamp= ft2.ft2stamp
				INNER JOIN FI (NOLOCK) ON FT.ftstamp= fI.ftstamp
			WHERE FI.valcartao !=0   and fi.ofistamp ='' 
				and FT.fdata BETWEEN @dataini AND @datafim and u_hclstamp !=''
				AND  b_utentes.nrcartao=@nrcartao

			union all

			-- entidades regularização 
			SELECT 
				 FT.fdata + FT.ousrhora											AS data,
				 CONVERT(VARCHAR(20),ft.fno)									AS DocFno,
				 isnull(FI.valcartao,0)											AS valor,
				 fii.ref														AS REF
			from FT (NOLOCK)
				 INNER JOIN FI			(NOLOCK) ON	fi.ftstamp = Ft.ftstamp 
				 INNER JOIN fi fii		(NOLOCK) ON fii.fistamp = Fi.ofistamp 
				 INNER JOIN FT ftt		(NOLOCK) ON ftt.ftstamp= fii.ftstamp
				INNER JOIN b_utentes	(NOLOCK) ON  b_utentes.utstamp = ftt.u_hclstamp
			WHERE FI.valcartao !=0  and  FI.ofistamp !=''
				and FT.fdata BETWEEN @dataini AND @datafim and ftt.u_hclstamp !=''
				AND  b_utentes.nrcartao=@nrcartao
			union all

			SELECT 
				B_ocorrencias.date																		AS data,
				 ''																						AS DocFno,
				 isnull(B_ocorrencias.nValor,0)															AS valor,
				''																						AS REF
			from B_ocorrencias (NOLOCK)
				INNER JOIN b_utentes(NOLOCK) ON  B_ocorrencias.linkStamp = b_utentes.utstamp and  B_ocorrencias.Tipo='Cartão Cliente'
			WHERE   
				 B_ocorrencias.date BETWEEN @dataini AND @datafim AND  b_utentes.nrcartao=@nrcartao  and oValor not like '%pts%' -- infelizmente não existe forma de ver se e valor ou pontos a nao ser com a leitura do like da string se um dia mudarem o save na b_ocorrencia tera de ser ajustado
			 ) as T
		order by t.data asc	
	END


GO
Grant Execute on dbo.up_relatorio_ValorEmCartao to Public
Grant control on dbo.up_relatorio_ValorEmCartao to Public
GO
