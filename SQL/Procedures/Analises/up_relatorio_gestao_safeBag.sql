-- Relatório TEFS
-- exec up_relatorio_gestao_safeBag '20100101','20130101',''

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_safeBag]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_safeBag
go

create procedure dbo.up_relatorio_gestao_safeBag
@DataIni DATETIME,
@DataFim DATETIME,
@site varchar(60)
/* with encryption */
AS
SET NOCOUNT ON
declare @loja varchar(60)
set @loja = isnull((select ref from empresa (nolock) where site=@site),'')
select data_de_venda																										as data,
	(select site from empresa (nolock) where ref=substring(id_esegur_dep, patindex('%[^0]%',id_esegur_dep), 16))	as loja,
	substring(id_esegur_dep, patindex('%[^0]%',id_esegur_dep), 16)															as codLoja, 
	numero_safebag																											as safebag,
	substring(codigo_meiopagamento,4,2)																						as meioPag,
	isnull((select design from B_modoPag (nolock) where ref=substring(codigo_meiopagamento,4,2)),'Desconhecido')	as meioPagNome,
	sum(valor_meiopagamento)																								as valorPag
from B_moneyFile (nolock)
inner join B_moneyFile_registos (nolock) on B_moneyFile.id=B_moneyFile_registos.moneyFile_id
inner join empresa (nolock) on empresa.ref=substring(id_esegur_dep, patindex('%[^0]%',id_esegur_dep), 16)
where data_de_venda between @DataIni and @dataFim
	AND substring(id_esegur_dep, patindex('%[^0]%',id_esegur_dep), 16)=case when @site='TODAS' then substring(id_esegur_dep, patindex('%[^0]%',id_esegur_dep), 16) else @loja end/*@loja*/
group by data_de_venda, id_esegur_dep, numero_safebag, codigo_meiopagamento
order by loja, data


GO
Grant Execute on dbo.up_relatorio_gestao_safeBag to Public
Grant control on dbo.up_relatorio_gestao_safeBag to Public
GO