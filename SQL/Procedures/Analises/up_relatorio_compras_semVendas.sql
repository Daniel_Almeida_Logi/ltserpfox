/*	Relatório de Compras sem Vendas
	exec up_relatorio_compras_semVendas '20140101','20141231', '20140101','20150228', 'Loja 1'
	select * from fo
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_compras_semVendas]') IS NOT NULL
	drop procedure dbo.up_relatorio_compras_semVendas
go

create procedure dbo.up_relatorio_compras_semVendas
@dataCompraIni date
	,@dataCompraFim date
	,@dataVendaIni date
	,@dataVendaFim date
	,@site varchar(20)
/* WITH ENCRYPTION */ 
AS
BEGIN
	If OBJECT_ID('tempdb.dbo.#temp_fnx') IS NOT NULL
		drop table #temp_fnx;
	select
		fn.ref
		,fn.ofnstamp
		,qtt=sum(fn.qtt)
	into
		#temp_fnx
	from
		fn (nolock)
	where
		ofnstamp!='' and fn.data>=@dataCompraIni
	group by
		fn.ref, fn.ofnstamp
	select
		docdata = convert(date,fo.docdata)
		,fo.docnome
		,fo.adoc
		,fo.nome
		,fo.no
		,fn.ref
		,qtt = convert(int,isnull(fn.qtt-fnx.qtt,fn.qtt))
		,valor = fn.etiliquido - (fn.u_upc * isnull(fnx.qtt,0))
		,operador = b_us.nome
	from
		fo (nolock)
		inner join fn (nolock) on fo.fostamp=fn.fostamp
		inner join b_us (nolock) on b_us.iniciais = fo.ousrinis
		left join fi (nolock) on fi.ref=fn.ref and fi.tipodoc=1 and fi.rdata between @dataVendaIni and @dataVendaFim
		left join #temp_fnx fnx on fnx.ofnstamp=fn.fnstamp
	where
		fo.doccode in (55,101)
		and docdata between @dataCompraIni and @dataCompraFim
		and fo.site = @site
		and isnull(fn.qtt-fnx.qtt,fn.qtt) > 0
		and fi.ref is null
	order by
		docdata, fo.no, fo.adoc
	If OBJECT_ID('tempdb.dbo.#temp_fnx') IS NOT NULL
		drop table #temp_fnx;
END

GO
Grant Execute on dbo.up_relatorio_compras_semVendas to Public
Grant control on dbo.up_relatorio_compras_semVendas to Public
Go