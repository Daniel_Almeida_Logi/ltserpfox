/* Relat�rio com registo de inativa��o e ativa��o (nas devolu��es dos c�digos)

	exec up_relatorio_Informacao_NMVO '19000101','20280908','','Loja 1','','',''
	exec up_relatorio_Informacao_NMVO '19000101','20280908','','Loja 1','','','','',''
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_Informacao_NMVO]') IS NOT NULL
	DROP PROCEDURE dbo.up_relatorio_Informacao_NMVO
GO

CREATE PROCEDURE dbo.up_relatorio_Informacao_NMVO
@dataIni		AS DATETIME,
@dataFim		AS DATETIME,
@ref			AS VARCHAR(254),
@site			AS VARCHAR(55),
@status			AS VARCHAR(254),
@acao			AS VARCHAR(254),
@op				AS VARCHAR(254),
@productCode	AS VARCHAR(254),
@lote			AS VARCHAR(254) = ''
/* with encryption */
AS 
SET NOCOUNT ON
BEGIN 

	DECLARE @site_nr int

	set @site_nr = (SELECT NO FROM EMPRESA WHERE SITE=@site)

	SELECT 
		fi_trans_info.ousrdata										AS data, 
		fi_trans_info.productNhrn									AS ref, 
		ISNULL(st.design,'')										AS design,
		fi_trans_info.packSerialNumber								AS nserie,
		fi_trans_info.productCode									AS codProd,
		CONVERT(DATE,fi_trans_info.batchExpiryDate)					AS datavalidade,
		fi_trans_info.batchId										AS lote,
		CASE
			WHEN fi_trans_info.state ='ACTIVE'	 THEN 'Ativo'
			WHEN fi_trans_info.state ='INACTIVE' THEN 'Inativo'
			ELSE fi_trans_info.state END							AS estado,
		fi.nmdoc												    AS documento,
		fi.fno														AS ndocumento,
		fi_trans_info.site,
		ISNULL(nmvoAcaoTraducao.design,'')							AS tipo,
		fi_trans_info.description									AS descricao,
		fi_trans_info.alertIdReturn									AS alerta,
		fi_trans_info.Ousrinis,
		fi_trans_info.posTerminal
	FROM 
		fi_trans_info (NOLOCK)
		LEFT JOIN nmvoAcaoTraducao(NOLOCK) ON fi_trans_info.reqType = nmvoAcaoTraducao.codigoAcao
		LEFT JOIN fi  (NOLOCK) ON fi_trans_info.recStamp = FI.fistamp
		LEFT JOIN ft  (NOLOCK)  ON ft.ftstamp = fi.ftstamp AND ft.site = @site
		LEFT JOIN st  (NOLOCK) ON fi_trans_info.productNhrn = ST.ref AND ST.site_nr = @site_nr
		INNER JOIN b_us(nolock) on b_us.iniciais = fi_trans_info.Ousrinis
	WHERE 
		CONVERT(DATE,fi_trans_info.ousrdata) BETWEEN @dataIni AND @dataFim 
		AND fi_trans_info.productNhrn = CASE WHEN @ref = '' THEN fi_trans_info.productNhrn ELSE @ref END 
		AND fi_trans_info.packSerialNumber = CASE WHEN @productCode = '' THEN fi_trans_info.packSerialNumber ELSE @productCode END 		
		AND ST.site_nr = @site_nr
		AND fi_trans_info.site IN ('', @site)
		AND (nmvoAcaoTraducao.design IN (SELECT items from dbo.up_splitToTable(@acao,',')) OR @acao='') 
		AND (b_us.userno in (select items from dbo.up_splitToTable(@op,','))or @op= '' or @op= '0')
		AND (
    		(@status = 'S\Erro' AND code IN ('NMVS_SUCCESS','NMVS_OK'))
   			OR (@status = 'C\Erro' AND code NOT IN ('NMVS_SUCCESS','NMVS_OK'))
   			OR (@status = 'C\Alerta' AND alertIdReturn != '') --??
    		OR (@status = '')
     		)
		AND (@lote = '' OR fi_trans_info.batchId = @lote)
	ORDER BY 
		fi_trans_info.ousrdata ASC



END
GO
Grant Execute on dbo.up_relatorio_Informacao_NMVO to Public
Grant control on dbo.up_relatorio_Informacao_NMVO to Public
GO