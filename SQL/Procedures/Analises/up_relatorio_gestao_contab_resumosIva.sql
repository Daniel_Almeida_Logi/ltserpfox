/* 
	Relatório Resumos Iva
	Alterado 2015-05-13 p suportar cenários de venda produtos s/ iva no atendimento - Luís

	exec up_relatorio_gestao_contab_resumosIva '20171001', '20171031', 'Loja 1'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_contab_resumosIva]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_contab_resumosIva
go

Create PROCEDURE [dbo].up_relatorio_gestao_contab_resumosIva
	@dataIni datetime,
	@dataFim datetime,
	@site as varchar(55),
	@excluiEntidades bit = 0
/* with encryption */
AS 
	SET NOCOUNT ON
	/*	FTSTAMP, nmdoc, Fno, BASEINC, VALORIVA, TOTAL, data as fdata */
	SELECT	
		Numero		= Ft.fno,
		Cliente		= rtrim(ft.nome) + ' [' + CONVERT(varchar,ft.[no]) + '][' + CONVERT(varchar,ft.estab) + ']',
		Data		= ft.fdata,
		Documento	= RTRIM(Ft.nmdoc),
		TAXA		= FI.IVA,
		DESCONTOS	= CASE
						WHEN ft.tipodoc != 3 THEN
							SUM(ABS(case
									when (fi.desconto between 0.01 and 99.99) then (fi.epv*fi.qtt)-(CASE WHEN etiliquido <0 THEN etiliquido*-1 ELSE etiliquido END)
									when fi.desconto=100 then fi.epv*fi.qtt else 0 
									end - Fi.u_descval))
						ELSE
							SUM(ABS(case
									when (fi.desconto between 0.01 and 99.99) then (fi.epv*fi.qtt)-(CASE WHEN etiliquido <0 THEN etiliquido*-1 ELSE etiliquido END)
									when fi.desconto=100 then fi.epv*fi.qtt else 0 
									end - Fi.u_descval
								)) * -1
						  END,
		VALORIVA	=		SUM(fi.etiliquido - (fi.etiliquido / (1 + (Fi.iva / 100)))),
							--SUM(CASE
							--WHEN Fi.ivaincl = 1 
							--	THEN fi.etiliquido - (Fi.ETILIQUIDO/(Fi.iva/100+1))
							--ELSE (Fi.ETILIQUIDO*(Fi.iva/100)) 
							--END),
		BASEINC		= SUM(Fi.ETILIQUIDO / (1 + (Fi.iva / 100))),
						--SUM(CASE
						--	WHEN Fi.ivaincl = 1
						--		THEN (Fi.ETILIQUIDO/(Fi.iva/100+1))
						--	ELSE fi.ETILIQUIDO 
						--	END),
		TOTAL		= SUM(fi.etiliquido - (fi.etiliquido / (1 + (Fi.iva / 100))) + Fi.ETILIQUIDO / (1 + (Fi.iva / 100)))
						--SUM(CASE
							--WHEN Fi.ivaincl = 1 
							--	THEN fi.etiliquido - (Fi.ETILIQUIDO/(Fi.iva/100+1))
							--ELSE (Fi.ETILIQUIDO*(Fi.iva/100)) 
							--END
							--+
						   --CASE
							--WHEN Fi.ivaincl = 1
							--	THEN (Fi.ETILIQUIDO/(Fi.iva/100+1))
							--ELSE fi.ETILIQUIDO 
							--END)
	FROM	
		FT (nolock) 
		INNER JOIN td (nolock) ON td.ndoc = ft.ndoc 
		INNER JOIN fi (nolock) ON fi.ftstamp = ft.ftstamp
	WHERE	
		ft.fdata between @dataIni and @dataFim
		and anulado=0 
		and td.tiposaft!=''
		and ((fi.qtt <> 0 or fi.etiliquido <> 0) OR  ( ft.anulado = 1 ))
		and ft.site = case when @site = '' then ft.site else @site end
		and ft.no >= case when @excluiEntidades = 1 then 199 else 0 end 
		and td.tipodoc!=4
	Group By 
		RTRIM(Ft.nmdoc), FI.IVA, Ft.ftstamp, Ft.Fno, ft.fdata, Ft.nome, Ft.no, ft.estab,ft.tipodoc 


GO
Grant Execute On dbo.up_relatorio_gestao_contab_resumosIva to Public
Grant control On dbo.up_relatorio_gestao_contab_resumosIva to Public
GO