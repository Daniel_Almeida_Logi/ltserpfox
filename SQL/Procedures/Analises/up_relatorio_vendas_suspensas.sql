/* Relatório Vendas Suspensas

 exec up_relatorio_vendas_suspensas '20140109','20180109','',0,'Loja 1'

*/



if OBJECT_ID('[dbo].[up_relatorio_vendas_suspensas]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_suspensas
go

create procedure dbo.up_relatorio_vendas_suspensas
@dataIni as datetime,
@dataFim as datetime,
@nome as varchar(55),
@op	as varchar(max),
@site as varchar(30)
/* with encryption */
AS
SET NOCOUNT ON
	;with cteDescVale as (
		Select 
			distinct ft.u_nratend 
		From 
			fi (nolock) 
			inner join ft (nolock) on fi.ftstamp = ft.ftstamp 
		where 
			fi.epromo = 1 
			and fdata between @dataIni and @dataFim
	),
	cteOperadores as (
		Select 
			iniciais
			,userno as cm
			,username
		from 
			b_us 
		Where
			userno in (select items from dbo.up_splitToTable(@op,',')) or @op= '0' or @op= ''
	)
	select	
		ft.ftstamp 
		,fdata = convert(varchar,ft.fdata,102) 
		,ft.ousrhora
		,nmdoc = case when ft.cobrado=1 then ft.nmdoc + ' (S)' else ft.nmdoc end
		,ft.ndoc
		,ft.fno 
		,ft.nome
		,ft.[no] 
		,ft.estab 
		,fi.ref
		,fi.design 
		,fi.desconto 
		,fi.u_comp
		,qtt = fi.qtt-isnull((select Sum(qtt) as qtt from fi fix (nolock) where fi.fistamp=fix.ofistamp),0)
		,qtt2 = qtt
		,etiliquido = fi.epv*(fi.qtt-isnull((select Sum(qtt) as qtt from fi fix (nolock) where fi.fistamp=fix.ofistamp),0))
		,fi.epv
		,fi.u_epvp
		,fi.u_epref 
		,fi.u_txcomp
		,fi.iva
		,fi.u_psicont 
		,fi.u_bencont
		,ft.vendnm
		,ft.vendedor
		,descontos	= 
				CASE WHEN ft.tipodoc != 3 THEN
					case when (fi.desconto between 0.01 and 99.99) then (fi.epv*fi.qtt)-(CASE WHEN etiliquido <0 THEN etiliquido*-1 ELSE etiliquido END)
						when fi.desconto=100 then fi.epv*fi.qtt else 0 
					end
				ELSE
					case when (fi.desconto between 0.01 and 99.99) then (fi.epv*fi.qtt)-(CASE WHEN etiliquido <0 THEN etiliquido*-1 ELSE etiliquido END)
						when fi.desconto=100 then fi.epv*fi.qtt else 0 
					end * -1
				END
		,comp = (CASE WHEN (SELECT COUNT(*) AS VALOR FROM CTLTRCT (nolock) WHERE CTLTRCTstamp = ft.u_ltstamp) > 0 THEN u_ettent1 ELSE 0 END
				+ CASE WHEN (SELECT COUNT(*) AS VALOR FROM CTLTRCT (nolock) WHERE CTLTRCTstamp = ft.u_ltstamp2) > 0 THEN u_ettent2 ELSE 0 END)
		,total = (etiliquido + 
					(CASE WHEN  (SELECT COUNT(*) AS VALOR FROM CTLTRCT (nolock) WHERE CTLTRCTstamp = ft.u_ltstamp) > 0 THEN u_ettent1 ELSE 0 END ) +
					 (CASE WHEN (SELECT COUNT(*) AS VALOR FROM CTLTRCT (nolock) WHERE CTLTRCTstamp = ft.u_ltstamp2) > 0 THEN u_ettent2 ELSE 0 END)
				)
		,u_descval = case when ft.u_nratend in (select u_nratend from cteDescVale) THEN u_descval ELSE 0 END /* Só descontos em valor e que correspondem a um vale*/
		/*,ft2.u_codigo*/
	from 
		ft (nolock)
		inner join fi  (nolock) on ft.ftstamp=fi.ftstamp
		/*inner join ft2 (nolock) on ft.ftstamp=ft2.ft2stamp*/
	where 
		ft.cobrado=1 
		and ft.anulado=0 
		and (ft.fdata between @dataIni  and @dataFim)
		and ft.nome = case when @nome = '' then ft.nome else @nome end
		AND (ft.tipodoc=1 or ft.nmdoc='Factura Acordo') 
		and (fi.qtt-isnull((select Sum(qtt) as qtt from fi fix (nolock) where fi.fistamp=fix.ofistamp),0) )>0
		and ft.nmdoc!='Factura SNS' 
		and ft.nmdoc!='Factura Entidades'
		and ft.vendedor = CASE WHEN @op = 0 THEN vendedor Else @op END
		and site = case when @site='' then site else @site end
		and ft.ousrinis in (select iniciais from cteOperadores)

GO
Grant Execute on dbo.up_relatorio_vendas_suspensas to Public
Grant control on dbo.up_relatorio_vendas_suspensas to Public
GO
