/* Relatório Recapitulativo Vendas - usado para preenchimento do mapa de recapitulativo de vendas a enviar para as finanças

	exec up_relatorio_vendas_recapitulativo '20150101','20161231', 500, 'Loja 1'

*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_relatorio_vendas_recapitulativo]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_recapitulativo
go

create procedure dbo.up_relatorio_vendas_recapitulativo
	@dataIni		as datetime,
	@dataFim		as datetime,
	@valorVendas	as numeric(10,0),
	@site			varchar(55),
	@excluiEntidades bit
	
/* with encryption */
AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase

BEGIN	
	
	-- select identico ao vendas base detalhe
	select 
		sum(etotal) as TotalVendas
		,ft.nome	as NomeCliente
		,ft.no		as NoCliente
		,ft.ncont   as NoCont
	into 
		#dadosVendasBase
	from 
		ft (nolock)
		inner join td (nolock) on td.ndoc = ft.ndoc
	where 
		fdata between @dataIni and @dataFim
		and no > case when @excluiEntidades = 1 then 200 else 0  end
		and ft.ncont != '999999990'
		and ft.anulado = 0
		and (FT.tipodoc != 4 or u_tipodoc = 4)
		and ft.site = (case when @site = '' Then ft.site else @site end)

	group by 
		ft.ncont, ft.nome, ft.no

	
	-- result set final
	select 
		* 
	from
		 #dadosVendasBase
	where
		TotalVendas >= @valorVendas

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
END

GO
Grant Execute on dbo.up_relatorio_vendas_recapitulativo to Public
Grant control on dbo.up_relatorio_vendas_recapitulativo to Public
GO
