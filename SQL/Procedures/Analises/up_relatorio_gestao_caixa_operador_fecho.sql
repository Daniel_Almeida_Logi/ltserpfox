/* Relatório Caixas Operador

	exec up_relatorio_gestao_caixa_operador_fecho '20230131','20230131',0,999, 'Loja 1'

 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_caixa_operador_fecho]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_caixa_operador_fecho
go


create procedure [dbo].up_relatorio_gestao_caixa_operador_fecho
	@dataIni DATETIME
	,@dataFim DATETIME
	,@opIni	as numeric(9,0)
	,@opFim	as numeric(9,0)
	,@site varchar(30)
/* with encryption */
AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#resultFinalCaixasSS'))
		DROP TABLE #resultFinalCaixasSS

	DECLARE @usaPagCentral BIT
	DECLARE @gestaoCxOperador BIT
	SET @usaPagCentral = ISNULL((SELECT bool FROM b_parameters WHERE stamp='ADM0000000079'),0)
	SET @gestaoCxOperador = (SELECT gestao_cx_operador FROM empresa WHERE site = @site)

	IF @gestaoCxOperador = 0
		BEGIN
			SELECT
				cxstamp
				,cx.site
				,Data = CONVERT(VARCHAR,cx.dabrir,102)
				,dfechar = CONVERT(VARCHAR,cx.dfechar,102)
				,habrir
				,hfechar		
				,cx.pnome
				,cx.pno
				,Vendedor			= cx.fuserno
				,Nome				= cx.fusername
				,cx.fecho_dinheiro
				,cx.fecho_mb
				,cx.fecho_visa
				,cx.fecho_cheques
				,cx.fecho_epaga3
				,cx.fecho_epaga4
				,cx.fecho_epaga5
				,cx.fecho_epaga6
				,cx.fundocaixa
				,cx.fechada
				,fecho_total =  cx.fecho_dinheiro + cx.fecho_mb + cx.fecho_visa + cx.fecho_cheques + cx.fecho_epaga3 + cx.fecho_epaga4 + cx.fecho_epaga5 + cx.fecho_epaga6
				,obs = causa
			FROM 
				cx (NOLOCK)
			WHERE 
				cx.dabrir between @dataini and @DataFim 
				and cx.fuserno >= @opIni
				and cx.fuserno <= @opFim
				and cx.site = CASE WHEN @site = '' THEN cx.site ELSE @site END 
				and cx.fechada = 1
			ORDER BY cx.fusername
		END
	ELSE
		BEGIN
			SELECT
				ssstamp
				,ss.site
				,Data = CONVERT(VARCHAR,ss.dabrir,102)
				,dfechar = CONVERT(VARCHAR,ss.dfechar,102)
				,habrir
				,hfechar		
				,ss.pnome
				,ss.pno
				,Vendedor			= ss.fuserno
				,Nome				= ss.fusername
				,fecho_dinheiro = ss.evdinheiro
				,fecho_mb		= ss.epaga1
				,fecho_visa		= ss.epaga2
				,fecho_cheques  = ss.echtotal
				,ss.fecho_epaga3
				,ss.fecho_epaga4
				,ss.fecho_epaga5
				,ss.fecho_epaga6
				,ss.fundocaixa
				,ss.fechada
				,fecho_total =  ss.evdinheiro + ss.epaga1 + ss.epaga2 + ss.echtotal + ss.fecho_epaga3 + ss.fecho_epaga4 + ss.fecho_epaga5 + ss.fecho_epaga6
				,obs = causa
				,ROW_NUMBER() OVER(PARTITION BY ss.pno,ss.dabrir + habrir, ss.fuserno  ORDER BY usrdata + usrhora desc)  as linhas
			INTO
				#resultFinalCaixasSS
			FROM 
				ss (NOLOCK)
			WHERE 
				ss.dabrir between @dataini and @DataFim 
				and ss.fuserno >= @opIni
				and ss.fuserno <= @opFim
				and ss.site = CASE WHEN @site = '' THEN ss.site ELSE @site END 
				and ss.fechada = 1
			ORDER BY ss.fusername

			SELECT 
				* 
			FROM 
				#resultFinalCaixasSS ss 
			WHERE 
				linhas=1 
			ORDER BY Nome

		END
	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#resultFinalCaixasSS'))
		DROP TABLE #resultFinalCaixasSS

GO
Grant Execute On dbo.up_relatorio_gestao_caixa_operador_fecho to Public
Grant control On dbo.up_relatorio_gestao_caixa_operador_fecho to Public
GO