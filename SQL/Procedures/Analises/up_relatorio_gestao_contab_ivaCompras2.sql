SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_contab_ivaCompras2]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_contab_ivaCompras2
go

create procedure dbo.up_relatorio_gestao_contab_ivaCompras2
@documentos as varchar(254)

/* WITH ENCRYPTION */ 

AS
BEGIN

/*select distinct cm,cmdesc from cm1*/
declare @comando varchar(max)
Set @comando = 'select distinct cm,cmdesc from cm1 (nolock) Where convert(varchar,cm1.cm) in ('+char(39)+REPLACE(@documentos,'_',char(39)+','+CHAR(39))+char(39)+')'
exec sp_executesql @comando
			
END

GO
Grant Execute on dbo.up_relatorio_gestao_contab_ivaCompras2 to Public
Grant control on dbo.up_relatorio_gestao_contab_ivaCompras2 to Public
Go