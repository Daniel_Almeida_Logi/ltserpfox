/* 
	devolve a informacao dos produtos para o filtro do parametro

	exec up_relatorio_get_ProdutosParam '054'


*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_relatorio_get_ProdutosParam]') IS NOT NULL
	DROP PROCEDURE dbo.up_relatorio_get_ProdutosParam
GO

CREATE PROCEDURE dbo.up_relatorio_get_ProdutosParam

@id_ext_efr VARCHAR(10)


/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

SELECT  
	DISTINCT sel=convert(bit,0), 
	convert(varchar(10),ref) AS opcao 
FROM	   ext_esb_doc	(NOLOCK) doc 
LEFT JOIN  ext_esb_prescription (NOLOCK) presc	 ON presc.stamp_ext_esb_doc =doc.stamp 
LEFT JOIN  ext_esb_prescription_d   (NOLOCK) presc_d ON presc_d.stamp_ext_esb_prescription =presc.stamp 
WHERE id_ext_efr = @id_ext_efr
	

Go
Grant Execute on dbo.up_relatorio_get_ProdutosParam to Public
Grant Control on dbo.up_relatorio_get_ProdutosParam to Public
Go
