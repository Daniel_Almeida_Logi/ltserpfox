/* Relatório que mostra produtos sem rotação

Alterado a 2020-08-07 , JG: multiselecao para site

 
	exec up_relatorio_stocks_PeriodoSemMovimentos '20160101','20160331','','1,99','GENERIS,SANOFI,APCEUTICALS,TECNIFAR','','Loja 1','',0, 'GENERICO'

	exec up_relatorio_stocks_PeriodoSemMovimentos '2020','20190331','','1,99','','','Loja 1','',0, ''

	         

	exec up_relatorio_stocks_PeriodoSemMovimentos '20160101','20190331','','1','','','','',0, ''
	exec up_relatorio_stocks_PeriodoSemMovimentos '20160101','20190331','','1,99','','','Loja 1,Loja 2','',0, ''


	exec up_relatorio_stocks_PeriodoSemMovimentos '20160101','20190331','5440987','1,99','','','Loja 1','',0, ''
	exec up_relatorio_stocks_PeriodoSemMovimentos '20160101','20190331','5440987','1,99','','','Loja 2','',0, ''
	exec up_relatorio_stocks_PeriodoSemMovimentos '20160101','20190331','5440987','1,99','','','Loja 3','',0, ''  
	exec up_relatorio_stocks_PeriodoSemMovimentos '20160101','20190331','5440987','1,99','','','Loja 2,Loja 3','',0, ''
	exec up_relatorio_stocks_PeriodoSemMovimentos '20160101','20190331','5440987','1,99','','','Loja 1,Loja 2,Loja 3','',0, ''

	
	exec up_relatorio_stocks_PeriodoSemMovimentos '20160101','20190331','','1,99','','','Loja 1','',0, ''
	exec up_relatorio_stocks_PeriodoSemMovimentos '20160101','20190331','','1,99','','','Loja 2','',0, ''
	exec up_relatorio_stocks_PeriodoSemMovimentos '20160101','20190331','','1,99','','','Loja 3','',0, ''  
	exec up_relatorio_stocks_PeriodoSemMovimentos '20160101','20190331','','1,99','','','Loja 2,Loja 3','',0, ''
	exec up_relatorio_stocks_PeriodoSemMovimentos '20160101','20190331','','1,99','','','Loja 1,Loja 2,Loja 3','',0, ''

	exec up_relatorio_stocks_PeriodoSemMovimentos '20160101','20160331','','1,99','GENERIS,SANOFI,APCEUTICALS,TECNIFAR','','Loja 1,Loja 2','',0, 'GENERICO'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_relatorio_stocks_PeriodoSemMovimentos]') IS NOT NULL
	DROP procedure dbo.up_relatorio_stocks_PeriodoSemMovimentos
GO

CREATE procedure dbo.up_relatorio_stocks_PeriodoSemMovimentos
@dataIni		datetime,
@dataFim		datetime,
@ref			varchar(18),
@familia		varchar(max),
@lab			varchar(120),
@marca			varchar(200),
@site			varchar(254),
@atributosFam   varchar(max),
@stock			numeric(9,2),
@generico		varchar(18)

/* WITH ENCRYPTION */ 

AS
BEGIN

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosComMov'))
		DROP TABLE #DadosComMov
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosFami'))
		DROP TABLE #DadosFami
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosMarca'))
		DROP TABLE #DadosMarca
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosLab'))
		DROP TABLE #DadosLab
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosST'))
		DROP TABLE #DadosST

	/* Calc Loja e Armazens */
	--declare @site_nr as tinyint
	--set @site_nr = ISNULL((select no from empresa where site = @site),0)

	declare @site_nr varchar(20)
	set @site_nr = isnull((select  convert(varchar(254),(select 
							convert(varchar,no) + ', '
						from 
							empresa (nolock)							
						Where 
							site in (Select items from dbo.up_splitToTable(@site, ','))
						FOR XML PATH(''))) as no),0)

--print @site_nr
--Select items from dbo.up_splitToTable(@site_nr, ',')
--Select items from dbo.up_splitToTable(@site, ',')


	/* Convereter Valor Generico*/
	IF @generico = 'GENERICO'
		set @generico = 1
	IF @generico = 'NÃO GENERICO'
		set @generico = 0
		
	/* Tabela Movimentos*/
	Select 
		sl.ref
		,sa = sum(case when cm <50 then qtt else -qtt end)
		, st.stock
		, st.epv1
		, st.uintr
		, st.usaid 
		, empresa.no
	into 
		#DadosComMov
	From
		sl (nolock)
		left join empresa_arm (nolock) on empresa_arm.armazem = sl.armazem
		left join empresa (nolock) on empresa_arm.empresa_no = empresa.no
		inner join st (nolock) on sl.ref=st.ref
	Where
		datalc between @dataIni and @dataFim
		and (empresa.site is null or empresa.site  in (Select items from dbo.up_splitToTable(@site, ',')) )
		and sl.cm in (75,76,77)
		and st.stock>0
	group by
		sl.ref,empresa.no, sl.armazem, st.stock
		,st.epv1
		, st.uintr
		, st.usaid 

	
	
--Select * from #DadosComMov where ref='5440987'




	/* Tabela Familia */
	Select 
		distinct ref 
	into 
		#DadosFami
	From
		stfami (nolock)
	Where
		ref in (Select items from dbo.up_splitToTable(@familia,','))
		or @familia = '0' 
		or @familia = ''	
	-- Select * from #DadosFami

	/* Tabela Marca */
	Select 
		distinct usr1 
	into
		#DadosMarca
	From
		st (nolock)
	Where
		usr1 in (Select items from dbo.up_splitToTable(@marca,','))
		or @marca = ''	
	-- Select * from #DadosMarca

	/* Tabela Labs */
	Select 
		distinct u_lab 
	into 
		#DadosLab
	From
		st (nolock)
	Where
		u_lab in (Select items from dbo.up_splitToTable(@lab,','))
		or @lab = ''	
	-- Select * from #DadosLab

	/* Tabela DadosST */
		Select
			st.ref 
			,st.design
			,faminome
			,familia
			,usr1
			,u_lab
			,fprod.generico
			,tabiva
			,epcpond
			,epv1
			,marg1
			,stock
			,UINTR
			,USAID
			,departamento = isnull(b_famDepartamentos.design ,'')
			,seccao = isnull(b_famSeccoes.design,'')
			,categoria = isnull(b_famCategorias.design,'')
			,famfamilia =  isnull(b_famFamilias.design,'')
			,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 1) valDepartamento
			,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 2) valSeccao
			,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 3) valCategoria
			,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 4) valFamfamilia
			,site_nr
		into 
			#DadosST
		From
			st (nolock)
			inner join fprod (nolock) on st.ref = fprod.cnp
			left join b_famDepartamentos (nolock) on st.u_depstamp = b_famDepartamentos.depstamp
			left join b_famSeccoes (nolock) on st.u_secstamp = b_famSeccoes.secstamp
			left join b_famCategorias (nolock) on st.u_catstamp = b_famCategorias.catstamp
			left join b_famFamilias (nolock) on st.u_famstamp = b_famFamilias.famstamp
		where 
			site_nr in (Select items from dbo.up_splitToTable(@site_nr, ','))

--Select * from #DadosST where ref='5440987'

		/* Preparar Result Set Final */
		Select 
			#dadosST.ref
			,#dadosST.design
			,#dadosST.faminome
			,marca = #dadosST.usr1
			,lab = #dadosST.u_lab
			,Generico
			,stock = isnull(sa.stock,0)
			,pvp = #dadosST.epv1
			,#DadosST.UINTR
			,#DadosST.USAID
			,#DadosComMov.no as site_nr
		from 
			#DadosST
			left join sa on sa.ref = #DadosST.ref and sa.stock = #DadosST.stock			
			left join empresa_arm on empresa_arm.armazem = sa.armazem
			left join empresa on empresa_arm.empresa_no = empresa.no
			inner join #DadosComMov on #DadosComMov.ref=#DadosST.ref and #DadosComMov.no = #DadosST.site_nr
				and #DadosComMov.stock=sa.stock and #DadosComMov.epv1=#DadosST.epv1 and #DadosComMov.uintr=#DadosST.uintr and #DadosComMov.usaid=#DadosST.usaid
		Where
			--#DadosST.ref in (select ref from #DadosComMov)
			#DadosST.ref = case when @ref = '' then #DadosST.ref else @ref end 			
			and #DadosST.familia in (Select ref from #DadosFami)
			and #DadosST.usr1 in (Select usr1 from #DadosMarca)
			and #DadosST.u_lab in (Select u_lab from #DadosLab)
			and #DadosST.generico = case when @generico = '' then  #DadosST.generico else @generico end
			and isnull(sa.stock,0) > @stock
			and 
			(
				(departamento	= valDepartamento or valDepartamento is null)
				and (seccao		= valSeccao or valSeccao is null)
				and (#DadosST.categoria	= valCategoria or valCategoria is null)
				and (famfamilia	= valFamfamilia or valFamfamilia is null)
			)
			and (empresa.site is null or empresa.site  in (Select items from dbo.up_splitToTable(@site, ',')) )
			--order by empresa.site
			--and #DadosST.ref='5440987' 
			

		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosComMov'))
			DROP TABLE #DadosComMov
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosFami'))
			DROP TABLE #DadosFami
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosMarca'))
			DROP TABLE #DadosMarca
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosLab'))
			DROP TABLE #DadosLab
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosST'))
			DROP TABLE #DadosST

END

GO
GRANT EXECUTE on dbo.up_relatorio_stocks_PeriodoSemMovimentos TO PUBLIC
GRANT CONTROL on dbo.up_relatorio_stocks_PeriodoSemMovimentos TO PUBLIC
GO