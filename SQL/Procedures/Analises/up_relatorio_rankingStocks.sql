-- exec up_relatorio_rankingStocks '20190417','20190417','Saidas Acumuladas', 'Loja 1'
-- exec up_relatorio_rankingStocks '20190417','20190417','Saidas Acumuladas', 'Loja 2'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_rankingStocks]') IS NOT NULL
	drop procedure dbo.up_relatorio_rankingStocks
go

create procedure dbo.up_relatorio_rankingStocks
@dataIni as datetime 
,@dataFim as datetime
,@tipo as varchar(254)
,@site	as varchar(55)
/* with encryption */
AS 
BEGIN
	declare @site_nr as tinyint
	set @site_nr = (select no from empresa where site = @site)
	declare @total as numeric(14,3)
	IF @tipo = 'Saidas Acumuladas'
	Begin 
		set @total = (
			select
				total = sum(sl.ett * (case when sl.qtt < 0 then -1 else 1 end))
			from 
				sl (nolock) 
				inner join st (nolock) on st.ref = sl.ref   
			where 
				sl.trfa = 0 
				and sl.cm>50 
				and datalc between @dataIni and @dataFim
		)
		select
			sl.ref
			, (select design from st (nolock) where st.ref = sl.ref and st.site_nr = @site_nr) as descricao
			, sum(sl.ett * (case when sl.qtt < 0 then -1 else 1 end)) as valor
			, percentDoTotal = (sum(sl.ett * (case when sl.qtt < 0 then -1 else 1 end)) * 100 / @total)
		from 
			sl (nolock) 
			inner join st (nolock) on st.ref = sl.ref   
		where 
			sl.trfa = 0 
			and sl.cm>50 
			and datalc between @dataIni and @dataFim
		group by 
			sl.ref  
	end
	IF @tipo = 'Entradas Acumuladas'
	Begin 
		set @total = (
			select
				total = sum(sl.ett * (case when sl.qtt < 0 then -1 else 1 end))
			from 
				sl (nolock) 
				inner join st (nolock) on st.ref = sl.ref   
			where 
				sl.trfa = 0 
				and sl.cm<50 
				and datalc between @dataIni and @dataFim
		)
		select
			sl.ref
			, (select design from st (nolock) where st.ref = sl.ref and st.site_nr = @site_nr) as descricao
			, sum(sl.ett * (case when sl.qtt < 0 then -1 else 1 end)) as valor
			, percentDoTotal = (sum(sl.ett * (case when sl.qtt < 0 then -1 else 1 end)) * 100 / @total)
		from 
			sl (nolock) 
			inner join st (nolock) on st.ref = sl.ref   
		where 
			sl.trfa = 0 
			and sl.cm<50 
			and datalc between @dataIni and @dataFim
		group by 
			sl.ref  
	end
END


GO
Grant Execute on dbo.up_relatorio_rankingStocks to Public
Grant control on dbo.up_relatorio_rankingStocks to Public
GO


