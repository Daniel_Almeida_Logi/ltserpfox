/*
	relatório justificações técnicas  

	exec up_relatorio_justificacoes_tecnicas '20220101','20250625','Loja 1','',''
	exec up_relatorio_justificacoes_tecnicas '20250114','20250114','Loja 1','','JT05,JT07'
	
	(Select items from dbo.up_splitToTable(@codjustificacao, ','))
*/	

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('[dbo].[up_relatorio_justificacoes_tecnicas]') IS NOT NULL
    DROP PROCEDURE dbo.up_relatorio_justificacoes_tecnicas
GO

CREATE PROCEDURE [dbo].[up_relatorio_justificacoes_tecnicas]
    @dataIni AS datetime = '19000101',
    @dataFim AS datetime = '19000101',
    @site VARCHAR(60),
    @op INT = '',
    @codJustificacao VARCHAR(60) = ''
AS
SET NOCOUNT ON

DECLARE @sql NVARCHAR(MAX)
DECLARE @params NVARCHAR(MAX)
DECLARE @likeFilter NVARCHAR(MAX) = ''

-- Construir o filtro de LIKE dinamicamente
IF @codJustificacao <> ''
BEGIN
    SELECT @likeFilter = @likeFilter + 
        CASE WHEN @likeFilter = '' THEN '' ELSE ' OR ' END + 
        'justificacao_tecnica_cod LIKE ''%' + items + '%'''
    FROM dbo.up_splitToTable(@codJustificacao, ',')
END




SET @sql = '
    SELECT
        armazem AS loja,
        fi.nmdoc AS documento,
        fi.fno AS nrDocumento,
        ref AS ref,
        design AS designacao,
        fivendedor AS nrOp,  
        fivendnm AS operador,
        fi.ousrdata AS data,
        fi.ousrhora AS hora,
        justificacao_tecnica_cod AS codJustificacao,
        justificacao_tecnica_descr AS justificacao
    FROM
        fi (NOLOCK)
        LEFT JOIN fi2 (NOLOCK) ON fi.fistamp = fi2.fistamp
        LEFT JOIN ft (NOLOCK) ON ft.ftstamp = fi.ftstamp
    WHERE 1 = 1' + 
        CASE WHEN @codJustificacao = '' 
                 THEN 
					' and justificacao_tecnica_cod <>'''''
                 ELSE 
					  ' AND (' + @likeFilter + ')'
            
				 END
		
		+
        'AND fivendedor = (
            CASE WHEN @op = '''' 
                 THEN fivendedor 
                 ELSE @op END
        )
        AND (u_ltstamp != '''' OR u_ltstamp2 != '''')
        AND CONVERT(DATE, fi.ousrdata) >= @dataIni
        AND CONVERT(DATE, fi.ousrdata) <= @dataFim
        AND ft.u_lote != 0
        AND justificacao_tecnica_cod != ''''
        AND justificacao_tecnica_descr != ''''
        AND ft.site = @site
    ORDER BY
        armazem ASC,
        fi.ousrdata ASC,
        fi.ousrhora ASC,
        fi.fno ASC
'

SET @params = N'@dataIni DATETIME, @dataFim DATETIME, @site VARCHAR(60), @op INT, @codJustificacao VARCHAR(60)'
print @sql
EXEC sp_executesql @sql, @params, @dataIni, @dataFim, @site, @op, @codJustificacao
GO

GRANT EXECUTE ON up_relatorio_justificacoes_tecnicas TO PUBLIC
GRANT CONTROL ON up_relatorio_justificacoes_tecnicas TO PUBLIC
GO
