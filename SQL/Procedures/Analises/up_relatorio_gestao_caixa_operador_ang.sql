/****** Object:  StoredProcedure [dbo].[up_relatorio_gestao_caixa_operador_ang]    Script Date: 9/15/2022 2:20:33 PM
	EXEC up_relatorio_gestao_caixa_operador_ang '20221011','20221011', 0, 999,''
******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



if OBJECT_ID('[dbo].[up_relatorio_gestao_caixa_operador_ang]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_caixa_operador_ang
go


Create procedure [dbo].[up_relatorio_gestao_caixa_operador_ang]
	@dataIni DATETIME
	,@dataFim DATETIME
	,@opIni	as numeric(9,0)
	,@opFim	as numeric(9,0)
	,@site varchar(30)

/* with encryption */
AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#resultFundosCaixasSS'))
		DROP TABLE #resultFundosCaixasSS

	declare @usaPagCentral bit
	declare @gestaoCxOperador bit
	set @usaPagCentral = ISNULL((select bool from b_parameters where stamp='ADM0000000079'),0)
	set @gestaoCxOperador = (select gestao_cx_operador from empresa where site = @site)
		/* Calc vendas */
	select
		ft.fdata
		, ft.vendedor
		, ft.site
		, ft.pnome
		, ft.pno
		, ft.ndoc
		,sum(case when ft.tipodoc=1 then 1 else 0 end) as vendas 
		,sum(case when ft.tipodoc=3 then 1 else 0 end) as dev
		,ft.ssstamp
	into
		#vendasdev
	from ft (nolock)
	inner join td (nolock) on td.ndoc = ft.ndoc 
	where fdata between @dataini and @DataFim 
	group by ft.fdata, ft.vendedor, ft.site , ft.pnome, ft.pno, ft.ndoc,ft.u_nratend,ft.nmdoc,ft.ssstamp

	select
		 ft.fdata
		, ft.vendedor
		, ft.site
		, ft.pnome
		, ft.pno
		, ft.ndoc
		, sum(case when ft.nmdoc='Nota de Cr�dito'  then -pc.creditos else  pc.creditos end) as creditos
		,ft.ssstamp
	into
		#vendasCreditos
	from ft (nolock) 
	inner join b_pagcentral (nolock) pc on ft.u_nratend = pc.nrAtend and ft.site = pc.site
	INNER JOIN TD (NOLOCK) ON ft.ndoc = TD.ndoc 
	 where nmDocExt NOT  in ('CCO', 'FCO')
	  and  fdata between @dataini and @DataFim  
	group by ft.fdata, ft.vendedor, ft.site , ft.pnome, ft.pno, ft.ndoc,ft.u_nratend,ft.nmdoc, ft.nmdoc,ft.ssstamp

	select 
			ss.fuserno
			,ss.fusername
			,ROW_NUMBER() OVER(PARTITION BY ss.fuserno,ss.fusername  ORDER BY usrdata + usrhora desc)  as linhas
			, fundoCaixa
			,  convert(varchar,ss.dabrir,102) as dAbrir
			,ssstamp
	into #resultFundosCaixasSS
	from ss(nolock)
	where 		ss.dabrir between @dataini and @DataFim 
				and ss.site = case when @site = '' then ss.site else @site end 
				and ss.fuserno >= @opIni
				and ss.fuserno<= @opFim
				and ss.fechada = 1

	IF @gestaoCxOperador = 0
		begin
			select
				cx.site
				,Data				= convert(varchar,cx.dabrir,102)
				,bt.xopvision_terminal_id														as 'balcao'
				,cx.pno
				,Vendedor			= pc.vendedor
				,Nome				= pc.nome
				,Total_Dinheiro		= sum(pc.evdinheiro)
				,Total_Multibanco	= sum(pc.epaga2)
				,Total_Visa			= sum(pc.epaga1)
				,Total_Cheques		= sum(pc.echtotal)
				,Total_Devolucoes	= sum(pc.devolucoes)
				,Total_Credito		= ISNULL((select SUM(ISNULL(#vendasCreditos.creditos,0)) from #vendasCreditos(NOLOCK) where cx.site=#vendasCreditos.site and cx.dabrir=#vendasCreditos.fdata and pc.vendedor=#vendasCreditos.vendedor),0)
				,Nr_Vendas			= Isnull((select sum((vendas)) from #vendasdev(NOLOCK) INNER JOIN TD (NOLOCK) ON #vendasdev.ndoc = TD.ndoc  where nmDocExt NOT  in ('CCO', 'FCO') and cx.site=#vendasdev.site and cx.dabrir=#vendasdev.fdata and pc.vendedor=#vendasdev.vendedor),0)
				,Nr_Dev				= Isnull((select sum((dev)) from #vendasdev(NOLOCK) INNER JOIN TD (NOLOCK) ON #vendasdev.ndoc = TD.ndoc  where nmDocExt NOT  in ('CCO') and cx.site=#vendasdev.site and cx.dabrir=#vendasdev.fdata and pc.vendedor=#vendasdev.vendedor),0)
				,Nr_Atend			= count(distinct pc.nratend)
				,Total_Caixa		= sum(pc.evdinheiro) + sum(pc.epaga2) + sum(pc.epaga1) + sum(pc.echtotal) + sum(pc.epaga3) + sum(pc.epaga4) + sum(pc.epaga5) + sum(pc.epaga6)
				,Pendente			= sum(case when @usaPagCentral=1 
												then (case when fechado=0 and abatido=0
											 				then pc.total
															else 0 end)
									else 0 end)
				,epaga3				= sum(pc.epaga3) 
				,epaga4				= sum(pc.epaga4 * ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'USD', pc.oData),1)) --USD
				,epaga5				= sum(pc.epaga5 * ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'EUR', pc.oData),1))--EUR
				,epaga6				= sum(pc.epaga6)
				,total_movExtra		= sum(case when pc.nrAtend like 'CX%' then pc.total else 0 end)
				,fundocaixa
			from 
				cx (nolock) 
				inner join B_pagCentral pc  on pc.cxstamp = cx.cxstamp
				left join B_Terminal bt	on cx.pnome =  bt.terminal and cx.site = bt.Site
			where 
				cx.dabrir between @dataini and @DataFim 
				and pc.site = (case when @site = '' then pc.site else @site end )
				and pc.vendedor >= @opIni
				and pc.vendedor <= @opFim
				and cx.fechada = 1
				and pc.ignoraCaixa = 0
			group by 
				cx.site
				,cx.dabrir
				,pc.vendedor
				,pc.nome
				,bt.xopvision_terminal_id
				,cx.pno
				,fundocaixa
				,pc.vendedor 
				,pc.nome
			order by fusername
		end
	else 
		begin
			select 
				ss.site
				,Data				= convert(varchar,ss.dabrir,102)
				,bt.xopvision_terminal_id														as 'balcao'
				,ss.pno
				,Vendedor			= ss.fuserno
				,Nome				= ss.fusername
				,Total_Dinheiro		= sum(pc.evdinheiro)
				,Total_Multibanco	= sum(pc.epaga2)
				,Total_Visa			= sum(pc.epaga1)
				,Total_Cheques		= sum(pc.echtotal)
				,Total_Devolucoes	= sum(pc.devolucoes)
				,Total_Credito		= ISNULL((select SUM(ISNULL(#vendasCreditos.creditos,0)) from #vendasCreditos(NOLOCK) where ss.site=#vendasCreditos.site and ss.dabrir=#vendasCreditos.fdata and ss.fuserno=#vendasCreditos.vendedor  and #vendasCreditos.ssstamp =ss.ssstamp),0)
				,Nr_Vendas			= Isnull((select sum((vendas)) from #vendasdev(NOLOCK) INNER JOIN TD (NOLOCK) ON #vendasdev.ndoc = TD.ndoc  where nmDocExt NOT  in ('CCO', 'FCO') and ss.site=#vendasdev.site and ss.dabrir=#vendasdev.fdata and ss.fuserno=#vendasdev.vendedor and #vendasdev.ssstamp =ss.ssstamp),0)
				,Nr_Dev				= Isnull((select sum((dev)) from #vendasdev(NOLOCK) INNER JOIN TD (NOLOCK) ON #vendasdev.ndoc = TD.ndoc  where nmDocExt NOT  in ('CCO') and ss.site=#vendasdev.site and ss.dabrir=#vendasdev.fdata and ss.fuserno=#vendasdev.vendedor and #vendasdev.ssstamp =ss.ssstamp),0)
				,Nr_Atend			= count(distinct pc.nratend)
				,Total_Caixa		= sum(pc.evdinheiro) + sum(pc.epaga2) + sum(pc.epaga1) + sum(pc.echtotal) + sum(pc.epaga3) + sum(pc.epaga4) + sum(pc.epaga5) + sum(pc.epaga6)
				,Pendente			= sum(case when @usaPagCentral=1 
												then (case when fechado=0 and abatido=0
															then pc.total
															else 0 end)
									else 0 end)
				,epaga3				= sum(pc.epaga3)
				,epaga4				= sum(pc.epaga4 * ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'USD', pc.oData),1)) --USD
				,epaga5				= sum(pc.epaga5 * ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'EUR', pc.oData),1))--EUR
				,epaga6				= sum(pc.epaga6)
				,total_movExtra		= sum(case when pc.nrAtend like 'CX%' then pc.total else 0 end)
				,fundocaixa			 = #resultFundosCaixasSS.fundoCaixa
			from 
				ss (nolock)
				inner join #resultFundosCaixasSS on  #resultFundosCaixasSS.fusername = ss.fusername and #resultFundosCaixasSS.fuserno = ss.fuserno and #resultFundosCaixasSS.dAbrir = convert(varchar,ss.dabrir,102) and #resultFundosCaixasSS.ssstamp = ss.ssstamp
				inner join B_pagCentral pc on pc.ssstamp = ss.ssstamp 
				left join B_Terminal bt	on ss.pnome =  bt.terminal and ss.site = bt.Site						      
			where 
				ss.dabrir between @dataini and @DataFim 
				and ss.site = case when @site = '' then ss.site else @site end 
				and ss.fuserno >= @opIni
				and ss.fuserno<= @opFim
				and ss.fechada = 1
				and pc.ignoraCaixa = 0
			group by 
				ss.site
				,ss.dabrir
				,ss.fuserno
				,ss.fusername
				,bt.xopvision_terminal_id
				,ss.pno
				,#resultFundosCaixasSS.fundocaixa
				,pc.nome
				,ss.ssstamp
			order by ss.fusername
		end

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#resultFundosCaixasSS'))
		DROP TABLE #resultFundosCaixasSS

GO
Grant Execute On dbo.up_relatorio_gestao_caixa_operador_ang to Public
Grant control On dbo.up_relatorio_gestao_caixa_operador_ang to Public
GO