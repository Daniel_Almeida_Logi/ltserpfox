
-- up_relatorio_tempos_atendimento '20180620', '20180820', 0, '', 'Loja 1'

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[up_relatorio_tempos_atendimento]
	@dataIni		datetime
	, @dataFim		datetime
	, @tempomaior	numeric(10,0)
	, @op			varchar(max)
	, @site			varchar(55)

/* with encryption */

AS
SET NOCOUNT ON

	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores

	/* Calc operadores */
	Select
		iniciais, cm = userno, username, nome
	into
		#dadosOperadores
	from
		b_us (nolock)
	Where
		userno in (select items from dbo.up_splitToTable(@op,','))
		or @op= '0'
		or @op= ''

	select 
		cast((DATEDIFF(MINUTE, ext_xopvision_queue_management.start_date,  B_pagCentral.oData)) as numeric(5,0)) as duracao
		,ext_xopvision_queue_management.start_date
		,B_pagCentral.oData
		,B_pagCentral.nome
		,B_pagCentral.nratend
		, B_pagCentral.terminal
	from 
		ext_xopvision_queue_management (nolock)
	inner join 
		B_pagCentral (nolock) on B_pagCentral.nrAtend=ext_xopvision_queue_management.nrAtend
	where  
		CONVERT(varchar, start_date, 102) between CONVERT(varchar, @dataIni, 102) and CONVERT(varchar, @dataFim, 102)
		and isnull(ext_xopvision_queue_management.ticket_id, '')<>''
		and isnull(ext_xopvision_queue_management.nrAtend, '')<>''
		and (select COUNT(u_nratend) from ft where ft.u_nratend=B_pagCentral.nrAtend)=1
		and cast((DATEDIFF(MINUTE, ext_xopvision_queue_management.start_date,  B_pagCentral.oData)) as numeric(5,0)) > @tempomaior
		and B_pagCentral.vendedor in (select distinct cm from #dadosOperadores)
	--order by
	--	ext_xopvision_queue_management.start_date desc
		
	union
	
	select 
		cast(isnull(duration,0) as numeric(5,0)) as duracao
		,start_date 
		,start_date as odata
		, (select top 1 nome from #dadosOperadores where ext_xopvision_breakmotive.ousrinis=#dadosOperadores.iniciais) as nome
		, 'PAUSA' as nratend
		,  terminal
	from 
		ext_xopvision_breakmotive (nolock)
	where 
		isnull(duration,0)<>0
		and CONVERT(varchar, start_date, 102) between CONVERT(varchar, @dataIni, 102) and CONVERT(varchar, @dataFim, 102)
		and duration > @tempomaior
	
	order by
		start_date

	-- up_relatorio_tempos_atendimento '20180620', '20180620', 1, '', 'Loja 1'
	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores

