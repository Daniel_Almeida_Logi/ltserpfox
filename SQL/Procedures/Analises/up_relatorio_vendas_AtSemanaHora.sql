/* Relatório Vendas Fluxo Atendimento Diário

	 exec up_relatorio_vendas_AtSemanaHora 2019, 11,'Loja 1'
	 exec up_relatorio_vendas_AtSemanaHora 2019, 1,'Loja 1'
*/
if OBJECT_ID('[dbo].[up_relatorio_vendas_AtSemanaHora]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_AtSemanaHora
go

create procedure [dbo].up_relatorio_vendas_AtSemanaHora
	@ano int
	,@mes int
	,@site varchar(55)


/* with encryption */

AS
SET NOCOUNT ON
	SET DATEFIRST 1
	/* Calculo da Data Incio e Fim devido a peridos de outros meses que devem ser incluidos */
	declare @data as datetime =  dateadd(day, 1 - 1,dateadd(month,@mes - 1,dateadd(year,@ano - 2000,'2000-01-01'))) 
	declare @primeiroDiaMes as datetime = (SELECT DATEADD(dd,-(DAY(@data)-1),@data))
	declare @semanaDoprimeiroDiaMes as int = (select DATEPART(week,@primeiroDiaMes))
	declare @ultimoDiaMes as datetime = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@data)+1,0))
	declare @semanaDoUltimoDiaMes as int = (select DATEPART(week,@ultimoDiaMes))

	/* primeiro Dia da primeira semana (Pode calhar no mes anterior) */
	declare @dataIni as datetime = (SELECT dateadd(day, 1,DATEADD(wk, DATEDIFF(wk, 6, '1/1/' + convert(varchar(4),@ano)) + (@semanaDoprimeiroDiaMes-1), 6)))
	/* Ultimo Dia da ultima Semana (Pode calhar no mes seguinte) */
	declare @dataFim as datetime = (SELECT dateadd(day, 7,DATEADD(wk, DATEDIFF(wk, 6, '1/1/' + convert(varchar(4),@ano)) + (@semanaDoUltimoDiaMes-1), 6)))

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
	DROP TABLE #dadosVendasBase
	
	/* Define se vais mostrar resultados abaixo do Cliente 199 */
	declare @Entidades as bit
	set @Entidades = case when (select tipoempresa from empresa where site = @site) in ('FARMACIA', 'PARAFARMACIA') then convert(bit,1) else convert(bit,0) end

	/* Dados Base Vendas  */
	create table #dadosVendasBase (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(3,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100)
		,familia varchar(18)
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)
	)
    
    insert #dadosVendasBase
    exec up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site

	/* Elimina Vendas tendo em conta tipo de cliente*/
	If @Entidades = 1
	delete from #dadosVendasBase where #dadosVendasBase.no < 199

	/* Calc Result Set*/
	Select 
		#dadosVendasBase.fdata
		,#dadosVendasBase.nrAtend
		,semana = DATEPART(week,#dadosVendasBase.fdata)
		,dataInicioSemana = (SELECT dateadd(day, 1,DATEADD(wk, DATEDIFF(wk, 6, '1/1/' + convert(varchar(4),YEAR(#dadosVendasBase.fdata))) + (DATEPART(week,#dadosVendasBase.fdata)-1), 6)))
		,dataFimSemana = (SELECT dateadd(day, 7,DATEADD(wk, DATEDIFF(wk, 6, '1/1/' + convert(varchar(4),YEAR(#dadosVendasBase.fdata))) + (DATEPART(week,#dadosVendasBase.fdata)-1), 6)))
		,ousrhora
	from 
		#dadosVendasBase
	WHERE #dadosVendasBase.fdata >=@primeiroDiaMes AND #dadosVendasBase.fdata <= @ultimoDiaMes
	order by #dadosVendasBase.fdata asc , #dadosVendasBase.ousrhora asc
	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase

GO 
Grant Execute on dbo.up_relatorio_vendas_AtSemanaHora to Public
Grant control on dbo.up_relatorio_vendas_AtSemanaHora to Public
GO