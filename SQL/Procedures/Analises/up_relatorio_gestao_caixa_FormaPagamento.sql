-- exec up_relatorio_gestao_caixa_FormaPagamento '20100101','20130101',''

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_caixa_FormaPagamento]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_caixa_FormaPagamento
go

create procedure dbo.up_relatorio_gestao_caixa_FormaPagamento
@dataIni DATETIME,
@dataFim datetime,
@site varchar(60)
/* with encryption */
AS
SET NOCOUNT ON
declare @loja varchar(60), @dinheiro char(8), @cheque char(8)/*, @efundocx numeric(13,2), @cxstamp varchar(28)*/
/* guardar código da loja e códigos dos meios de pagamento */
set @loja = isnull((select ref from empresa (nolock) where site=@site),'')
set @dinheiro = isnull((select ref from B_modoPag (nolock) where design like 'Dinheiro'),'0')
set @cheque = isnull((select ref from B_modoPag (nolock) where design like 'Cheques'),'0')
SELECT	data,meioPag,SUM(total_venda) as total_venda,SUM(efundocx) as efundocx,loja,
		SUM(valorSaco) as valorSaco,SUM(total_devolucao) as total_devolucao,SUM(quebra) as quebra
FROM	(
	select meioPag, DATA, loja, fusername, fuserno,
		total_venda		= sum(total_venda) , 
		total_devolucao	= sum(total_devolucao),
		efundocx		= sum(efundocx),
		valorSaco		= sum(valorsaco),
		saldo				= sum(saldo),
		quebra			= case when meioPag='Dinheiro' 
									then sum(valorSaco + saldo) - (sum(total_venda) + sum(efundocx))
								when meioPag='Cheques' then sum(valorSaco + saldo) - sum(total_venda)
								else 0 end,
		tipomodopag		=  isnull((select permiteTroca from b_modoPag (nolock) where design=xxx.meioPag),0)
		/*cxstamp			= @cxstamp*/
	from (
		select meioPag, DATA, loja, pnome, pno, fusername, fuserno,
			sum(xx.total_venda) as total_venda,
			valorSaco,
			sum(xx.devolucoes) as total_devolucao,
			sum(xx.saldo) as saldo,
			efundocx = case when meiopag='Dinheiro'
						then case when sacodinheiro='0'
									then 0 
									else isnull((select top 1 efundocx from fcx (nolock)
												inner join cx (nolock) on fcx.cxstamp=cx.cxstamp
												where fcx.operacao='F' and cx.fechada=1 and cx.dabrir=xx.data /*and cx.dabrir<=@dataFim*/
													and cx.site=@site and fcx.efundocx!=0
													and cx.fuserno=xx.fuserno
												order by cxid)
											,0)
									end
						else 0 end
		from (
			select meioPag, DATA, loja, pnome, pno, fusername, fuserno,
				sacoDinheiro,
				sum(Venda) as total_venda,
				valorSaco = case when meioPag='Dinheiro'
								then isnull(( select sum(valor_meiopagamento)
											from b_moneyFile_registos mfr (nolock)
											inner join cx cx1 (nolock) on cx1.sacodinheiro = mfr.numero_safebag
											where data_de_venda=x.data /*AND data_de_venda<=@dataFim*/
											and sacoDinheiro!='0'
											AND substring(id_esegur_dep, patindex('%[^0]%',id_esegur_dep), 16)=@loja
											AND substring(codigo_meiopagamento,4,2)=@dinheiro AND cx1.sacoDinheiro=x.sacoDinheiro and cx1.cxstamp=x.cxstamp)
										,0)
								when meioPag='Cheques'
								then isnull(( select sum(valor_meiopagamento)
											from b_moneyFile_registos mfr (nolock)
											inner join cx cx1 (nolock) on cx1.sacodinheiro = mfr.numero_safebag
											where data_de_venda=x.data /*AND data_de_venda<=@dataFim*/
											and sacoDinheiro!='0'
											AND substring(id_esegur_dep, patindex('%[^0]%',id_esegur_dep), 16)=@loja
											AND substring(codigo_meiopagamento,4,2)=@cheque AND cx1.sacoDinheiro=x.sacoDinheiro and cx1.cxstamp=x.cxstamp)
										,0)
								else 0 end,
				devolucoes, saldo
			from (
				select 'Dinheiro' as meioPag, dabrir as data, cx.site as loja, cx.pnome, cx.pno, fusername, fuserno, cx.sacoDinheiro,
					fcx.evdinheiro as Venda, fcx.devolucoes, fcx.efundocx, cx.saldod as saldo, cx.cxstamp
				from cx (nolock)
				inner join fcx (nolock) on fcx.cxstamp=cx.cxstamp
				where fcx.operacao='F' and cx.fechada=1 and cx.dabrir>=@dataIni AND cx.dabrir<=@dataFim
				and cx.site=@site
				/* and cx.fuserno=@op */
				union all
				select 'Cheques' as meioPag, dabrir as data, cx.site as loja, cx.pnome, cx.pno, fusername, fuserno, cx.sacoDinheiro,
					fcx.echtotal as Venda, 0 as devolucoes, fcx.efundocx, cx.saldoc as saldo, cx.cxstamp
				from cx (nolock)
				inner join fcx (nolock) on fcx.cxstamp=cx.cxstamp
				where fcx.operacao='F' and cx.fechada=1 and cx.dabrir>=@dataIni and cx.dabrir<=@dataFim
					and cx.site=@site
					/* and cx.fuserno=@op */
				union all
				select 'MultiBanco' as meioPag, dabrir as data, cx.site as loja, cx.pnome, cx.pno, fusername, fuserno, cx.sacoDinheiro,
					fcx.epaga2 as Venda, 0 as devolucoes, fcx.efundocx, 0 as saldo, cx.cxstamp
				from cx (nolock)
				inner join fcx (nolock) on fcx.cxstamp=cx.cxstamp
				where fcx.operacao='F' and cx.fechada=1 and cx.dabrir>=@dataIni and cx.dabrir<=@dataFim
				and cx.site=@site
				/* and cx.fuserno=@op */
				union all
				select 'Visa' as meioPag, dabrir as data, cx.site as loja, cx.pnome, cx.pno, fusername, fuserno, cx.sacoDinheiro,
					fcx.epaga1 as Venda, 0 as devolucoes, fcx.efundocx, 0 as saldo, cx.cxstamp
				from cx (nolock)
				inner join fcx (nolock) on fcx.cxstamp=cx.cxstamp
				where fcx.operacao='F' and cx.fechada=1 and cx.dabrir>=@dataIni AND cx.dabrir<=@dataFim
						and cx.site=@site
						/* and cx.fuserno=@op */
			) as x
			group by meioPag, data, loja, pnome, pno, fusername, fuserno, sacoDinheiro ,x.saldo,x.devolucoes ,x.cxstamp /*x.valorSaco,*/
		) as xx
		group by meioPag, DATA, loja, pnome, pno, fusername, fuserno, sacoDinheiro, valorSaco
	) as xxx
	group by meioPag, data, loja, fusername, fuserno
) b
GROUP BY data,meioPag,loja
ORDER BY data

GO
Grant Execute on dbo.up_relatorio_gestao_caixa_FormaPagamento to Public
Grant control on dbo.up_relatorio_gestao_caixa_FormaPagamento to Public
GO