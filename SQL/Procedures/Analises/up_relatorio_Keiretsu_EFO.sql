-- exec up_relatorio_Keiretsu_EFO 'ADM9EADE76F-7AF5-4EC5-9AC'

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_Keiretsu_EFO]') IS NOT NULL
	drop procedure dbo.up_relatorio_Keiretsu_EFO
go

create procedure dbo.up_relatorio_Keiretsu_EFO
@encstamp as uniqueidentifier
/* with encryption */
AS 
BEGIN
;With cte1 as (
	Select	
		versao
		,escalao
		,PVAmin
		,PVAmax
		,PVPmin
		,PVPmax
		,mrgGross
		,mrgGrossTipo
		,mrgFarm
		,FeeFarm
		,feeFarmCiva
		,txCom
		,factorPond
	from	
		b_mrgRegressivas (nolock)
),
cte2 as (
	select	
		fprod.cnp,
		(Select top 1 escalao from cte1 Where pvporig between PVPmin and PVPmax order by versao desc) as escalao,
		pvporig as pvpciva,		
		pvporig - (pvporig*(taxa/100)) as pvpsiva,
		fprod.u_tabiva,
		taxasiva.taxa,
		(Select top 1 factorPond from cte1 Where pvporig between PVPmin and PVPmax order by versao desc) as factorPond,
		(Select top 1 feeFarmCiva from cte1 Where pvporig between PVPmin and PVPmax order by versao desc) as feeFarmCiva,
		(Select top 1 txCom from cte1 Where pvporig between PVPmin and PVPmax order by versao desc) as txCom,
		(Select top 1 mrgGross from cte1 Where pvporig between PVPmin and PVPmax order by versao desc) as mrgGross
		,generico
	from	
		fprod (nolock)
		inner join fpreco (nolock) on fpreco.cnp=fprod.ref
		inner join taxasiva (nolock) on taxasiva.codigo = fprod.u_tabiva
	where	
		fpreco.grupo='pvp'
),ctePVA as (
	select
		cnp
		,generico
		,PVA = 
			CASE 
				when  escalao = 1 then (pvpciva/1.48)+(0.004*pvpsiva)/*1º Escalão - PVA até 5€ (PVP máximo 7.40€)*/
				when  escalao = 2 then ((pvpciva-0.12)/1.4532) + (0.004*pvpsiva) /*PVA entre 5.01€ e 7€ (PVP entre 7.41€ e 10.29€)*/
				when  escalao = 3 then ((pvpciva-0.21)/1.4367) + (0.004*pvpsiva) /*PVA entre 7.01€ e 10€ (PVP entre 10.30€ e 14.58€)*/
				when  escalao = 4 then ((pvpciva-0.48)/1.4038) + (0.004*pvpsiva) /*PVA entre 10.01€ e 20€ (PVP entre 14.59€ e 28.56€)*/
				when  escalao = 5 then ((pvpciva-1.22)/1.3580) + (0.004*pvpsiva) /*PVA entre 20.01€ e 50€ (PVP entre 28.57€ e 69.12€)*/
				when  escalao = 6 then ((pvpciva-15.91)/1.0643) + (0.004*pvpsiva) /*PVA >= 50.01€ (PVP >= 69.13)*/
			END
	from 
		cte2 
)
Select 
	st.ref
	,bi.design
	,Laboratorio = bo.nome
	,QE = qtt  
	,BN = u_bonus
	,QT = qtt + u_bonus
	,DCBN = case when u_bonus = 0 then 0 else ROUND((100*u_bonus)/(qtt + u_bonus),2) end
	,DC = desconto
	,DF = 0
	,PCT = case when ctePVA.generico = 1 then ctePVA.PVA else st.epcusto end  /* No caso de ser generico PVA, nos restantes casos PCT */
	,IVA = bi.iva
	,st.stock
from 
	bo (nolock)
	inner join bi (nolock) on bo.bostamp = bi.bostamp
	inner join st (nolock) on bi.ref = st.ref
	left join ctePVA on bi.ref = ctePVA.cnp
where 
	bo.bostamp = @encstamp
END

GO
Grant Execute on dbo.up_relatorio_Keiretsu_EFO to Public
GO
Grant control on dbo.up_relatorio_Keiretsu_EFO to Public
GO

-------------------------------------------------------------------------------------------
