/*
 Relatório Registo Vacinação

 exec up_relatorio_vacinacao_registo '20231015','20231015','','','Loja 1','','','Enviado'
 exec up_relatorio_vacinacao_registo '20230501','20231210','','','Loja 1','','','Anulado'
 exec up_relatorio_vacinacao_registo '20230501','20231210','','','Loja 1','','','Nao Comunicado' 
 exec up_relatorio_vacinacao_registo '20230501','20231210','','','Loja 1','','','Anulado,Enviado'
 exec up_relatorio_vacinacao_registo '20230501','20231210','','','Loja 1','','','Anulado,Nao Comunicado'
 exec up_relatorio_vacinacao_registo '20231207','20231207','','','Loja 1','','','Enviado,Nao Comunicado'
 exec up_relatorio_vacinacao_registo '20230501','20231210','','','Loja 1','','','Enviado,Nao Comunicado,Anulado'
*/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_relatorio_vacinacao_registo]') IS NOT NULL
	drop procedure dbo.up_relatorio_vacinacao_registo
go

create procedure dbo.up_relatorio_vacinacao_registo

@dataIni		DATETIME,
@dataFim		DATETIME,
@cliente		VARCHAR(60),
@administrante	VARCHAR(60),
@site			VARCHAR(55),
@tipo			VARCHAR(254),
@vacina			VARCHAR(254),
@status			VARCHAR(254)

/* with encryption */

AS
SET NOCOUNT ON
	
	
	DECLARE @site_nr VARCHAR(20)
	SET @site_nr = ISNULL((SELECT no FROM empresa (NOLOCK) WHERE site=@site),0)

	IF ('Enviado' in (Select items from dbo.up_splitToTable(@status, ',')) AND
		'Anulado' in (Select items from dbo.up_splitToTable(@status, ',')) AND
		'Nao Comunicado' in (Select items from dbo.up_splitToTable(@status, ',')))
		SET @status = ''

	DECLARE @sql VARCHAR(4000) = ''
	
	SET @sql = @sql + N'
		SELECT 
		   sel = CAST(0 as bit)
		  ,[vacinacaostamp]
		  ,[no]
		  ,[Administrante]
		  ,[dataAdministracao]
		  ,[nome]
		  ,[dataNasc]
		  ,[contacto]
		  ,[Descricao]
		  ,B_vacinacao.[lote]
		  ,[nrVenda]
		  ,[obs]
		  ,B_vacinacao.[ousrinis]
		  ,B_vacinacao.[ousrdata]
		  ,B_vacinacao.[ousrhora]
		  ,B_vacinacao.[usrinis]
		  ,B_vacinacao.[usrdata]
		  ,B_vacinacao.[usrhora]
		  ,[nrSns]				= RTRIM(LTRIM(ISNULL([nrSns],'''')))
		  ,[codeVacina]
		  ,[unidades]
		  ,[dosagem]
		  ,[localAnatomico_id]
		  ,[viaAdmin_id]
		  ,[site_nr]
		  ,[comercial_id]		= RTRIM(LTRIM(ISNULL([comercial_id],'''')))
		  ,[dateSend]
		  ,[send]				= CASE WHEN anulado = 1 THEN ''Anulado'' 
										ELSE CASE WHEN result = 0
												THEN ''Não Comunicado''
												ELSE ''Enviado'' END
										END 
		  ,[tipoReg]
		  ,[correlationId]
		  ,faturado = dbo.uf_vacinacaoVerificaFaturado(ISNULL(cptorgabrev,''''),codeVacina,correlationId,tipoReg)
		FROM 
			B_vacinacao (NOLOCK)
			left join ctltrct on ctltrct.receita = B_vacinacao.correlationId and B_vacinacao.correlationId != ''''
		WHERE 
			dataAdministracao BETWEEN '''+convert(varchar,@dataini)+''' AND '''+convert(varchar,@datafim)+'''
			AND site_nr = '''+convert(varchar,@site_nr)+''''

		IF @cliente !=''
			SET @sql = + @sql + N' AND nome LIKE  ''%'' + '''+convert(varchar,@cliente)+''' + ''%'' '
		
		IF 	@Administrante != ''
			SET @sql = + @sql + N' AND Administrante LIKE  ''%'' + '''+convert(varchar,@Administrante)+''' + ''%'' ' 
			
		IF 	@tipo!= ''
			SET @sql = + @sql + N' AND tipoReg IN (SELECT items FROM dbo.up_splitToTable( '''+ @tipo +''','',''))'
			
		IF @vacina != ''
			SET @sql = + @sql + N' AND codeVacina IN (SELECT items FROM dbo.up_splitToTable('''+ @vacina +''','',''))'
		
		IF @status != ''
		BEGIN
			IF ('Enviado' in (Select items from dbo.up_splitToTable(@status, ',')) AND
				'Anulado' in (Select items from dbo.up_splitToTable(@status, ',')))
			BEGIN
				SET @sql = + @sql + N' AND [send] = 1 AND result = 1'
			END
			ELSE
			BEGIN
				IF ('Enviado' in (Select items from dbo.up_splitToTable(@status, ',')))
				BEGIN
					IF(@tipo = '') AND 'Nao Comunicado' not in (Select items from dbo.up_splitToTable(@status, ','))
					BEGIN
					print 44
						SET @sql = + @sql + N' AND [send] = 1 AND anulado = 0 AND result=1'
					END
					ELSE
					BEGIN
						IF ('Enviado' in (Select items from dbo.up_splitToTable(@status, ',')) AND
						'Nao Comunicado' in (Select items from dbo.up_splitToTable(@status, ',')))
						BEGIN
						print 1
							SET @sql = + @sql + N' AND anulado = 0'
						END
						ELSE
						BEGIN
						print 2
							SET @sql = + @sql + N' AND [send] = 1 AND anulado = 0 '
						END
					END
				END
				ELSE
				BEGIN
					IF ('Anulado' in (Select items from dbo.up_splitToTable(@status, ',')) AND
						'Nao Comunicado' in (Select items from dbo.up_splitToTable(@status, ',')))	
					BEGIN
						SET @sql = + @sql + N' AND (result = 0 OR anulado = 1)'
					END
					ELSE
					IF ('Anulado' in (Select items from dbo.up_splitToTable(@status, ',')))
					BEGIN
						SET @sql = + @sql + N' AND anulado = 1'
					END
				END
			END
			IF ('Enviado' in (Select items from dbo.up_splitToTable(@status, ',')) AND
				'Nao Comunicado' in (Select items from dbo.up_splitToTable(@status, ',')))
			BEGIN
			print 3
				SET @sql = + @sql + N' AND anulado = 0'
			END
			ELSE
			BEGIN
				IF ('Nao Comunicado' in (Select items from dbo.up_splitToTable(@status, ',')) AND
				'Anulado' not in (Select items from dbo.up_splitToTable(@status, ',')))	
				BEGIN	
				print 4
					SET @sql = + @sql + N' AND result = 0'
				END
			END
		END

	SET @sql = + @sql + N'
	ORDER BY 
		CONVERT(INT,RTRIM(no))'	

	PRINT @sql
	EXEC(@sql)
GO
Grant Execute on dbo.up_relatorio_vacinacao_registo to Public
Grant control on dbo.up_relatorio_vacinacao_registo to Public
GO
