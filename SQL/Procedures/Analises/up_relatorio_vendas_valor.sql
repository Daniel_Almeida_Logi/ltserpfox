-- Relatório Valor Vendas
-- exec up_relatorio_vendas_valor '20160603','20160603','','','','','Loja 1','','Tramadol'
if OBJECT_ID('[dbo].[up_relatorio_vendas_valor]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_valor
go

create procedure dbo.up_relatorio_vendas_valor
@dataIni as datetime,
@dataFim as datetime,
@familia as varchar(max),
@lab as varchar(200),
@categoria as varchar(100),
@marca as varchar(200),
@site as varchar(30),
@atributosFam as varchar(max),
@dci as varchar(254)
/* with encryption */
AS
SET NOCOUNT ON
declare @tempCteNumAtend table (u_nratend varchar(20))
insert into @tempCteNumAtend 
Select distinct ft.u_nratend From fi (nolock) inner join ft (nolock) on fi.ftstamp = ft.ftstamp where fi.epromo = 1 and fdata between @dataIni and @dataFim
declare @tempcteFamilia table (ref varchar(18))
insert into @tempcteFamilia 
Select distinct ref From stfami (nolock) Where ref in (Select items from dbo.up_splitToTable(@familia,',')) Or @familia = '0' or @familia = ''
;With 
	cteFprod as (
		Select 
			cnp, dispdescr, medid, generico,
			psico, benzo, protocolo, dci
		from 
			fprod (nolock)  
	), 
	cteST as(
		Select 
			st.ref
			,faminome
			,familia
			,u_lab
			,stock
			,usr1
			,departamento = isnull(b_famDepartamentos.design ,'')
			,seccao = isnull(b_famSeccoes.design,'')
			,categoria = isnull(b_famCategorias.design,'')
			,famfamilia =  isnull(b_famFamilias.design,'')
			,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 1) valDepartamento
			,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 2) valSeccao
			,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 3) valCategoria
			,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 4) valFamfamilia
			,dispdescr
			,medid
			,generico
			,psico
			,benzo
			,protocolo
			,dci = isnull(dci,'')
		From 
			st (nolock)
			left join b_famDepartamentos (nolock) on st.u_depstamp = b_famDepartamentos.depstamp
			left join b_famSeccoes (nolock) on st.u_secstamp = b_famSeccoes.secstamp
			left join b_famCategorias (nolock) on st.u_catstamp = b_famCategorias.catstamp
			left join b_famFamilias (nolock) on st.u_famstamp = b_famFamilias.famstamp
			left join cteFprod on cteFprod.cnp = st.ref
		Where
			familia in (Select ref from @tempcteFamilia)
			and u_lab = case when @lab = '' then u_lab else @lab end
			and usr1 = case when @marca = '' then usr1 else @marca end
	), 
	cteFacturacao as (
		/* DOCS DE FACTURACAO */
		SELECT
			u_descval = case when ft.u_nratend in (select u_nratend from @tempCteNumAtend) THEN u_descval ELSE 0 END /* Só descontos em valor e que correspondem a um vale*/	
			,ft.site
			,fi.ref
			,fi.design
			,fi.familia
			,faminome = isnull(cteST.faminome,'')
			,laboratorio = isnull(cteST.u_lab,'')
			,usr1 = isnull(cteST.usr1,'')
			,stock_actual = isnull(cteST.stock,0)
			,ft.nmdoc
			,etiliquidoSIva = (ETILIQUIDO/(fi.iva/100+1))
			,etiliquido
			,epcpond = CASE WHEN TD.tipodoc = 3 THEN /* DEV. A CLIENTE */
							(CASE WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt ELSE FI.EPCP * qtt END)*(-1)
							ELSE
							(CASE WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt ELSE FI.EPCP * qtt END) 
						END
			,descontos	= 
					CASE WHEN ft.tipodoc != 3 THEN
						case when (fi.desconto between 0.01 and 99.99) then (fi.epv*fi.qtt)-(CASE WHEN etiliquido <0 THEN etiliquido*-1 ELSE etiliquido END)
							when fi.desconto=100 then fi.epv*fi.qtt else 0 
						end
					ELSE
						case when (fi.desconto between 0.01 and 99.99) then (fi.epv*fi.qtt)-(CASE WHEN etiliquido <0 THEN etiliquido*-1 ELSE etiliquido END)
							when fi.desconto=100 then fi.epv*fi.qtt else 0 
						end * -1
					END
			,ettent1SIva = case when ft.tipodoc = 3 then (u_ettent1/(fi.iva/100+1))*-1 else (u_ettent1/(fi.iva/100+1)) end
			,ettent2SIva = case when ft.tipodoc = 3 then (u_ettent2/(fi.iva/100+1))*-1 else (u_ettent2/(fi.iva/100+1)) end
			,ettent1 = case when ft.tipodoc = 3 then u_ettent1*-1 else u_ettent1 end
			,ettent2 = case when ft.tipodoc = 3 then u_ettent2*-1 else u_ettent2 end
			,total = (etiliquido + 
						(case when ft.tipodoc = 3 then u_ettent1*-1 else u_ettent1 end) +
						(case when ft.tipodoc = 3 then u_ettent2*-1 else u_ettent2 end)
					  )
			,totalsemiva = (
							(ETILIQUIDO/(fi.iva/100+1)) +
							(case when ft.tipodoc = 3 then (u_ettent1/(fi.iva/100+1))*-1 else (u_ettent1/(fi.iva/100+1)) end ) +
							(case when ft.tipodoc = 3 then (u_ettent2/(fi.iva/100+1))*-1 else (u_ettent2/(fi.iva/100+1)) end )
							)
			,qtt = case when ft.tipodoc!=3 then fi.qtt else -fi.qtt end
			,dispdescr
			,medid
			,generico
			,psico
			,benzo
			,protocolo
			,dci
		FROM
			FI (nolock) 
			INNER JOIN ft (nolock) ON FI.FTSTAMP = ft.ftstamp
			INNER JOIN TD (nolock) ON TD.NDOC = ft.ndoc
			LEFT JOIN cteST on cteST.ref = fi.ref
		WHERE
			(td.tipodoc!=4 or u_tipodoc=4) AND u_tipodoc != 1 AND u_tipodoc != 5 
			AND ft.fdata >= @dataIni and ft.fdata <= @dataFim
			AND fi.composto=0 AND ft.anulado=0 
			and ft.no>199
			and ft.site = case when @site='' then ft.site else @site end	
			/* Tratamento Familias*/
			and 
				(
					(departamento	= valDepartamento or valDepartamento is null)
					and (seccao		= valSeccao or valSeccao is null)
					and (categoria	= valCategoria or valCategoria is null)
					and (famfamilia	= valFamfamilia or valFamfamilia is null)
				)
	)
		SELECT
			site
			,a.ref
			,a.design
			,familia
			,faminome
			,laboratorio
			,utente = SUM(etiliquidoSIva)
			,comparticipacao = SUM(ettent1SIva) + SUM(ettent2SIva)
			,total = SUM(totalsemiva)
			,epcpond = SUM(epcpond)
			,descCom = SUM(descontos)
			,descVale = SUM(u_descval)
			,MrgBruta = round(sum(Total-epcpond),2)
			,MrgBrutaPerc = case when sum(Total)>0 and sum(Total-epcpond)>0 then round((sum(Total)-sum(epcpond))*100 / sum(Total),2)
				 when sum(Total)>0 and sum(Total-epcpond)<0 then round((sum(epcpond)-sum(Total))*100 / -sum(epcpond),2)
				 when sum(Total)<0 and sum(Total-epcpond)>0 then round((sum(epcpond)-sum(Total))*100 / -sum(epcpond),2)
				 when sum(Total)<0 and sum(Total-epcpond)<0 then round((sum(Total)-sum(epcpond))*100 / -sum(Total),2)
				 else 0 end
			,stock_actual
			,qtt = SUM(qtt)
			,gen = isnull((SELECT TOP 1 generico FROM fprod (nolock) WHERE a.ref=fprod.cnp),0)
			,marca = usr1
			,BBPrev			= (SUM(etiliquidoSIva) + SUM(ettent1SIva) + SUM(ettent2SIva)) - SUM(epcpond)
			,MBPVPrev		= (CASE WHEN (SUM(etiliquidoSIva) + SUM(ettent1SIva) + SUM(ettent2SIva)) > 0
									THEN ((SUM(etiliquidoSIva) + SUM(ettent1SIva) + SUM(ettent2SIva)-SUM(epcpond))*100) / (SUM(etiliquidoSIva) + SUM(ettent1SIva) + SUM(ettent2SIva))
									ELSE 0
								END)
		FROM (	
			Select 
				* 
			from 
				cteFacturacao
		) a
		WHERE
			familia in (Select ref from @tempcteFamilia)
			and laboratorio = case when @lab = '' then laboratorio else @lab end
			and site = case when @site='' then site else @site end
			and usr1 = case when @marca = '' then usr1 else @marca end
			and isnull(dci,'') = case when @dci = '' then isnull(dci,'') else @dci end
			and isnull(dispdescr,'') like case when @categoria='MEDICAMENTOS' then 'M%' else '%' end /* so medicamentos */
			and isnull(medid,0)=case when @categoria='PARAFARMÁCIA' then 0 else isnull(medid,0) end /*  so parafarmacia	*/		
			and isnull(generico,0)=case when @categoria='GENÉRICOS' then 1 else isnull(generico,0) end /* so genericos */
			and isnull(generico,0)=case when @categoria='NÃO GENERICOS' then 0 else isnull(generico,0) end /* so nao genericos */
			and isnull(dispdescr,'')=case when @categoria='MSRM' then 'MSRM' else isnull(dispdescr,'') end /*  so MSRM */
			and isnull(dispdescr,'')=case when @categoria='MNSRM' then 'MNSRM' else isnull(dispdescr,'') end /*  so MNSRM */
			and isnull(psico,0)=case when @categoria='PSICOTRÓPICOS' then 1 else isnull(psico,0) end /*  so psico */
			and isnull(benzo,0)=case when @categoria='BENZODIAZEPINAS' then 1 else isnull(benzo,0) end /*  so benzo */
			and isnull(protocolo,0)=case when @categoria='PROTOCOLO' then 1 else isnull(protocolo,0) end /*  so protocolo */
		group by 
			a.ref, a.design, familia, faminome, laboratorio, stock_actual, [site], usr1

GO
Grant Execute on dbo.up_relatorio_vendas_valor to Public
Grant control on dbo.up_relatorio_vendas_valor to Public
GO