/* Relatório Caixas Operador

	exec up_relatorio_gestao_caixa_operador_dif_Detalhes '20200417','20200417', 0, 999, 'Loja 1'
	exec up_relatorio_gestao_caixa_operador_dif_Detalhes '20200414','20200414', 0, 999, 'Loja 1'
	exec up_relatorio_gestao_caixa_operador_dif_Detalhes '20200317','20200317', 0, 999, 'ATLANTICO'


	exec up_relatorio_gestao_caixa_operador_dif_Detalhes '20220601','20220630', 0, 999, 'Loja 1'


 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_caixa_operador_dif_Detalhes]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_caixa_operador_dif_Detalhes
go

create procedure [dbo].up_relatorio_gestao_caixa_operador_dif_Detalhes
	@dataIni DATETIME
	,@dataFim DATETIME
	,@opIni	as numeric(9,0)
	,@opFim	as numeric(9,0)
	,@site varchar(30)

/* with encryption */

AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dados'))
		DROP TABLE #dados

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dados1'))
		DROP TABLE #dados1

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosCaixa'))
		DROP TABLE #dadosCaixa

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSS'))
		DROP TABLE #dadosSS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosPCSS'))
		DROP TABLE #dadosPCSS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#vendasPVP'))
		DROP TABLE #vendasPVP

	declare @usaPagCentral bit
	declare @gestaoCxOperador bit
	set @usaPagCentral = ISNULL((select bool from b_parameters where stamp='ADM0000000079'),0)
	set @gestaoCxOperador = (select gestao_cx_operador from empresa where site = @site)

	 


		/* Calc vendas */
	select
		ft.fdata
		, ft.vendedor
		, ft.site
		, ft.pnome
		, ft.pno
		,ISNULL(SUM(case when ft.tipodoc=1 then 1 else 0 end),0) as vendas 
		,ISNULL(SUM(case when ft.tipodoc=3 then 1 else 0 end),0) as dev
		,ISNULL(SUM(edescc),0)                                   as desconto 
	into
		#vendasdev
	from ft
	where fdata between @dataini and @DataFim 
	group by ft.fdata, ft.vendedor, ft.site , ft.pnome, ft.pno

	/* Calc PVP */
	select
		ft.fdata
		, ft.vendedor
		, ft.site
		, ft.pnome
		, ft.pno
		,SUM(ISNULL(etiliquido,0) + (case when ft.tipodoc = 3 then ISNULL(u_ettent1*-1,0) else ISNULL(u_ettent1,0) end) + (case when ft.tipodoc = 3 then ISNULL(u_ettent2*-1,0) else ISNULL(u_ettent2,0) end)) AS vd_PVP
	into
		#vendasPVP
	from ft
	INNER join fi on ft.ftstamp = fi.ftstamp 
	where fdata between @dataini and @DataFim 
	group by ft.fdata, ft.vendedor, ft.site , ft.pnome, ft.pno



IF @gestaoCxOperador = 0
		begin

		SELECT 
			site
			,pnome
			,pno
			,Vendedor			
			,Nome				
			, Turno           = CASE WHEN Sum(DATEDIFF(hour,dabrir,dfechar))=0 THEN 1 ELSE Sum(DATEDIFF(hour,dabrir,dfechar)) END				
			,caixa			  = SUM(ISNULL(caixa,0))            
			,difcaixa         = SUM(ISNULL(difcaixa,0))			
			,total_movExtra   = SUM(ISNULL(total_movExtra,0))	
			,vd_PVP           =	SUM(ISNULL(vd_PVP,0))	
			,Total_Credito    = SUM(ISNULL(Total_Credito,0))	
			,Total_Devolucoes = SUM(ISNULL(Total_Devolucoes,0))	
			,Nr_Dev			  =	SUM(ISNULL(Nr_Dev,0))
			,vDesc            =	SUM(ISNULL(Nr_Dev,0))
			,Nr_Vendas		  =	SUM(ISNULL(Nr_Vendas,0))
			,Nr_Atend         = SUM(ISNULL(Nr_Atend,0))		
			,v_at_med         = CASE WHEN SUM(Nr_Atend) = 0  THEN vd_PVP ELSE vd_PVP/SUM(Nr_Atend) END
			,v_at_h			  = CASE WHEN Sum(DATEDIFF(hour,dabrir,dfechar))=0 THEN vd_PVP ELSE  vd_PVP/ Sum(DATEDIFF(hour,dabrir,dfechar)) END
			,n_at_h           = CASE WHEN Sum(DATEDIFF(hour,dabrir,dfechar))=0 THEN SUM(Nr_Atend) ELSE  CAST( SUM(CAST(Nr_Atend AS numeric(5,2)))/ Sum(DATEDIFF(hour,dabrir,dfechar)) AS numeric(5,2)) END     
		into #dadosCaixa
		FROM (
			select
				 cx.site
				,cx.pnome
				,cx.pno
				,Vendedor			= pc.vendedor
				,Nome				= pc.nome
				,dabrir             = (CAST(CAST(cx.dabrir As Date) As DateTime) + CAST(CAST(cx.habrir As Time) As DateTime))
				,dfechar            =(CAST(CAST(cx.dfechar As Date) As DateTime) + CAST(CAST(cx.hfechar As Time) As DateTime))
				--,Turno				= CASE WHEN DATEDIFF(hour,(CAST(CAST(cx.dabrir As Date) As DateTime) + CAST(CAST(cx.habrir As Time) As DateTime)),(CAST(CAST(cx.dfechar As Date) As DateTime) + CAST(CAST(cx.hfechar As Time) As DateTime))) =0 THEN 1 ELSE DATEDIFF(hour,(CAST(CAST(cx.dabrir As Date) As DateTime) + CAST(CAST(cx.habrir As Time) As DateTime)),(CAST(CAST(cx.dfechar As Date) As DateTime) + CAST(CAST(cx.hfechar As Time) As DateTime))) END
				,caixa              = (SUM(ISNULL(pc.evdinheiro,0)) + SUM(ISNULL(pc.epaga2,0)) + SUM(ISNULL(pc.epaga1,0)) + SUM(ISNULL(pc.echtotal,0)) + SUM(ISNULL(pc.epaga3,0)) + SUM(ISNULL(pc.epaga4,0)) + SUM(ISNULL(pc.epaga5,0)) + SUM(ISNULL(pc.epaga6,0)) + SUM(ISNULL(cx.fundocaixa,0)))
				,difcaixa			= (SUM(ISNULL(cx.fecho_dinheiro,0)) + SUM(ISNULL(cx.fecho_mb,0)) + SUM(ISNULL(cx.fecho_visa,0)) + SUM(ISNULL(cx.fecho_cheques,0)) + SUM(ISNULL(cx.fecho_epaga3,0)) + SUM(ISNULL(cx.fecho_epaga4,0)) + SUM(ISNULL(cx.fecho_epaga5,0)) + SUM(ISNULL(cx.fecho_epaga6,0)))
									  - (SUM(ISNULL(pc.evdinheiro,0)) + SUM(ISNULL(pc.epaga2,0)) + SUM(ISNULL(pc.epaga1,0)) + SUM(ISNULL(pc.echtotal,0)) + SUM(ISNULL(pc.epaga3,0)) + SUM(ISNULL(pc.epaga4,0)) + SUM(ISNULL(pc.epaga5,0)) + SUM(ISNULL(pc.epaga6,0)))
				,total_movExtra		= SUM(case when left(pc.nrAtend,2) = 'Cx' then pc.evdinheiro else 0 end)
				,vd_PVP             = (SELECT ISNULL((vd_PVP),0) FROM #vendasPVP WHERE cx.site=#vendasPVP.site and cx.dabrir=#vendasPVP.fdata and pc.vendedor=#vendasPVP.vendedor and cx.pnome=#vendasPVP.pnome and cx.pno=#vendasPVP.pno)
				,Total_Credito		= SUM(ISNULL(pc.creditos,0))
				,Total_Devolucoes	= SUM(ISNULL(pc.devolucoes,0))  
				,Nr_Dev				= (SELECT ISNULL((dev),0)	  FROM #vendasdev WHERE cx.site=#vendasdev.site and cx.dabrir=#vendasdev.fdata and pc.vendedor=#vendasdev.vendedor and cx.pnome=#vendasdev.pnome and cx.pno=#vendasdev.pno)
				,vDesc              = (SELECT ISNULL(desconto,0)  FROM #vendasdev WHERE cx.site=#vendasdev.site and cx.dabrir=#vendasdev.fdata and pc.vendedor=#vendasdev.vendedor and cx.pnome=#vendasdev.pnome and cx.pno=#vendasdev.pno)
				,Nr_Vendas			= (SELECT ISNULL((vendas),0)  FROM #vendasdev WHERE cx.site=#vendasdev.site and cx.dabrir=#vendasdev.fdata and pc.vendedor=#vendasdev.vendedor and cx.pnome=#vendasdev.pnome and cx.pno=#vendasdev.pno)
				,Nr_Atend			= ISNULL(COUNT(distinct pc.nratend),0)
			from 
				cx (nolock) 
				inner join B_pagCentral pc on pc.cxstamp = cx.cxstamp
				--inner join ft on ft.u_nratend=pc.nrAtend 
			where 
				cx.dabrir between @dataini and @DataFim 
				and cx.site = (case when @site = '' then cx.site else @site end )
				and pc.vendedor >= @opIni
				and pc.vendedor <= @opFim
				and cx.fechada = 1
				and pc.ignoraCaixa = 0
				--and ft.u_nratend <>''
			group by 
				cx.site
				,cx.dabrir
				,pc.vendedor
				,pc.nome
				,cx.pnome
				,cx.pno
				,cx.dabrir
				,cx.dfechar
				,cx.habrir
				,cx.hfechar
		) resultcx
		group by site,
				 pnome,
				 pno,
				 vendedor,
				 nome,
				 vd_PVP

		SELECT 
			 site
			,Vendedor			
			,Nome				
			,Turno           =Sum(Turno)           
			,caixa			 =Sum(caixa)			  
			,difcaixa        =Sum(difcaixa)         
			,total_movExtra  =Sum(total_movExtra)   
			,vd_PVP          =Sum(vd_PVP)           
			,Total_Credito   =Sum(Total_Credito)    
			,Total_Devolucoes=Sum(Total_Devolucoes) 
			,Nr_Dev			 =Sum(Nr_Dev)			  
			,vDesc           =Sum(vDesc)            
			,Nr_Vendas		 =Sum(Nr_Vendas)		  
			,Nr_Atend        =Sum(Nr_Atend)         
			,v_at_med        =Sum(v_at_med)         
			,v_at_h			 =Sum(v_at_h)			  
			,n_at_h          =Sum(n_at_h)           

		FROM #dadosCaixa	
		GROUP BY site,
				 pnome,
				 pno,
				 vendedor,
				 nome,
				 total_movExtra,
				 vd_PVP,
				 Total_Credito,
				 Total_Devolucoes,
				 Nr_Dev,
				 vDesc,
				 Nr_Vendas,
				 Nr_Atend


		END
	ELSE 
		BEGIN
		
			select
				 ss.ssstamp
				,auserno
				,ss.site
				,Turno				= CASE WHEN DATEDIFF(hour,(CAST(CAST(ss.dabrir As Date) As DateTime) + CAST(CAST(ss.habrir As Time) As DateTime)),(CAST(CAST(ss.dfechar As Date) As DateTime) + CAST(CAST(ss.hfechar As Time) As DateTime))) =0 THEN 1 ELSE DATEDIFF(hour,(CAST(CAST(ss.dabrir As Date) As DateTime) + CAST(CAST(ss.habrir As Time) As DateTime)),(CAST(CAST(ss.dfechar As Date) As DateTime) + CAST(CAST(ss.hfechar As Time) As DateTime))) END
				,caixaSS			= SUM(ss.evdinheiro) + SUM(ss.epaga1) + SUM(ss.epaga2) + SUM(ss.echtotal) + SUM(ss.fecho_epaga3) + SUM(ss.fecho_epaga4) + SUM(ss.fecho_epaga5) + SUM(ss.fecho_epaga6) -- + SUM(ss.fundoCaixa)		
				,fundoCaixaSS		= ss.fundoCaixa	
			into 
				#dadosSS
			from 
				ss (nolock) 
			where 
				ss.dabrir between @dataini and @DataFim 
				and ss.site = case when @site = '' then ss.site else @site end
				and ss.auserno >= @opIni
				and ss.auserno <= @opFim 
				and ss.fechada = 1
				AND ss.ssstamp = (case 
									when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
									then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
									else ss.ssstamp
								end)
			group by 
				ss.ssstamp
				,ss.site
				,auserno
				,ss.dabrir
				,ss.habrir
				,ss.dfechar
				,ss.hfechar
				,ss.fundoCaixa	


				 
		
			SELECT
					 site               = pc.site
					,ssstamp            = pc.ssstamp
					,Vendedor			= pc.vendedor
					,Nome				= pc.nome
					,caixaPC			= (SUM(ISNULL(pc.evdinheiro,0))  + SUM(pc.epaga2) + SUM(pc.epaga1) + SUM(pc.echtotal) + SUM(pc.epaga3) + SUM(pc.epaga4) + SUM(pc.epaga5) + SUM(pc.epaga6)) --+ SUM(ss.fundoCaixa)
					,total_movExtra		= SUM(case when left(pc.nrAtend,2) = 'Cx' then pc.evdinheiro else 0 end)
					,Total_Credito		= SUM(pc.creditos)
					,Total_Devolucoes	= SUM(pc.devolucoes)  
					,Nr_Atend			= ISNULL(COUNT(distinct pc.nratend),0)
					,fundoCaixaPC		= ss.fundoCaixa	
			INTO 
				#dadosPC
			from 
				ss (nolock) 
				inner join B_pagCentral pc on pc.ssstamp = ss.ssstamp
			where 
				ss.dabrir between @dataini and @DataFim 
				and ss.site = case when @site = '' then ss.site else @site end 
				and pc.vendedor >= @opIni
				and pc.vendedor <= @opFim
				and ss.fechada = 1
				and pc.ignoraCaixa = 0
				AND ss.ssstamp = (case 
									when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
									then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
									else ss.ssstamp
								end)
		    group by 
				pc.site
			   ,pc.ssstamp
			   ,pc.vendedor
			   ,pc.nome
			   ,ss.fundoCaixa

			SELECT 
			     SITE= SS.SITE
				,Vendedor=PC.Vendedor
				,Nome= PC.Nome
				,Turno= SUM(Turno)
				,caixa= SUM(caixaPC) + SUM(fundoCaixaPC)
				,difcaixa = SUM(caixaSS) - SUM(caixaPC)
				,total_movExtra   = SUM(ISNULL(PC.total_movExtra,0))
				,Total_Credito		= SUM(PC.Total_Credito)
				,Total_Devolucoes	= SUM(PC.Total_Devolucoes)
				,Nr_Atend			= SUM(pc.Nr_Atend)
			INTO 
				#dadosPCSS	
		    FROM #dadosSS SS
			INNER JOIN  #dadosPC PC ON SS.ssstamp= PC.ssstamp
			GROUP BY
				 SS.SITE
				,PC.Vendedor
				,PC.Nome 
			


			SELECT 
			     SITE
				,Vendedor
				,Nome
				,Turno
				,caixa
				,difcaixa
				,total_movExtra 
				,Total_Credito	
				,Total_Devolucoes
				,Nr_Atend		
				,Nr_Dev		= (SELECT SUM(ISNULL((dev),0)) FROM #vendasdev WHERE PCSS.site=#vendasdev.site and PCSS.vendedor=#vendasdev.vendedor )
				,vDesc      = (SELECT SUM(ISNULL(desconto,0))  FROM #vendasdev WHERE PCSS.site=#vendasdev.site and PCSS.vendedor=#vendasdev.vendedor)
				,Nr_Vendas	= (SELECT SUM(ISNULL((vendas),0)) FROM #vendasdev WHERE PCSS.site=#vendasdev.site and PCSS.vendedor=#vendasdev.vendedor) --PCSS.dabrir=#vendasdev.fdata
				,vd_PVP     = (SELECT Sum(ISNULL((vd_PVP),0)) FROM #vendasPVP WHERE PCSS.site=#vendasPVP.site and PCSS.vendedor=#vendasPVP.vendedor)
			INTO 
				#dados	
		    FROM #dadosPCSS PCSS

			GROUP BY
			     SITE
				,Vendedor
				,Nome
				,Turno
				,caixa
				,difcaixa
				,total_movExtra 
				,Total_Credito	
				,Total_Devolucoes
				,Nr_Atend		



		SELECT 
			 site
			,Vendedor			
			,Nome				
			,Turno           =Turno           
			,caixa			 =caixa			  
			,difcaixa        =difcaixa         
			,total_movExtra  =total_movExtra   
			,vd_PVP          =vd_PVP           
			,Total_Credito   =Total_Credito    
			,Total_Devolucoes=Total_Devolucoes 
			,Nr_Dev			 =Nr_Dev			  
			,vDesc           =vDesc            
			,Nr_Vendas		 =Nr_Vendas		  
			,Nr_Atend        =Nr_Atend         
			,v_at_med         = CASE WHEN SUM(ISNULL(Nr_Atend,0)) = 0  THEN ISNULL(vd_PVP,0) ELSE ISNULL(vd_PVP,0)/Nr_Atend END
			,v_at_h			  = CASE WHEN Turno=0 THEN ISNULL(vd_PVP,0) ELSE  ISNULL(vd_PVP,0)/ Turno END
			,n_at_h           = CASE WHEN Turno=0 THEN SUM(ISNULL(Nr_Atend,0)) ELSE CAST( SUM(CAST(ISNULL(Nr_Atend,0) AS numeric(9,2)))/ Turno AS numeric(9,2)) END    			
		FROM #dados	
			group by site
			 ,vendedor
			 ,nome	
			 ,Turno           
			 ,caixa			 
			 ,difcaixa        
			 ,total_movExtra  
			 ,vd_PVP          
			 ,Total_Credito   
			 ,Total_Devolucoes
			 ,Nr_Dev			 
			 ,vDesc           
			 ,Nr_Vendas		 
			 ,Nr_Atend
			
		END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dados'))
		DROP TABLE #dados
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosCaixa'))
		DROP TABLE #dadosCaixa
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSS'))
		DROP TABLE #dadosSS
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosPC'))
		DROP TABLE #dadosPC
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosPCSS'))
		DROP TABLE #dadosPCSS
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#vendasPVP'))
		DROP TABLE #vendasPVP

GO
Grant Execute On dbo.up_relatorio_gestao_caixa_operador_dif_Detalhes to Public
Grant control On dbo.up_relatorio_gestao_caixa_operador_dif_Detalhes to Public
GO


