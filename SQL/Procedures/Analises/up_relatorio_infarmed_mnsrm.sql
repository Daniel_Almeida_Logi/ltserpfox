/* Relatório Comunicação MNSRM Infarmed

	exec up_relatorio_infarmed_mnsrm '20240101','20241231','Loja 1'

	NOTA:  ATENÇÃO SE FOR ALTERADO ESTE RELATORIO TERÁ DE SER MUDADO TAMBEM ESTA SP up_relatorio_infarmed_mnsrmSendDocuments
	
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_infarmed_mnsrm]') IS NOT NULL
	drop procedure dbo.up_relatorio_infarmed_mnsrm
go

create procedure dbo.up_relatorio_infarmed_mnsrm
@dataIni as datetime,
@dataFim as datetime,
@site as varchar(60)
/* with encryption */
AS
SET NOCOUNT ON
declare @site_nr as tinyint
set @site_nr = ISNULL((select no from empresa where site = @site),0)
select 
	ANO
	,MES
	,x.Ref
	,convert(integer,round(sum(x.qtt),0)) as qtt
	,convert(varchar,REPLACE(Convert(money,round(st.epv1,2)),',','.')) as PVP
from (
    select 
		YEAR(FT.FDATA) as ANO
		,MONTH(FT.FDATA) as MES
		,fi.ref
		,fi.design
		,case when ft.tipodoc!=3 then fi.qtt else -fi.qtt end as Qtt
        ,ft.ndoc
        ,ft.fno
    from
		ft (nolock)
		inner join fi (nolock) on ft.ftstamp=fi.ftstamp
		inner join st (nolock) on fi.ref = st.ref AND st.site_nr = @site_nr
		inner join fprod (nolock) on fprod.ref = fi.ref
    where 
		(ft.fdata between @dataIni and @dataFim) 
		AND ft.site = @site
        AND ft.anulado=0 
        AND ft.ndoc not in (select distinct serie_ent from empresa (nolock)) /*fatura entidades*/
		AND ft.ndoc not in (select distinct serie_entSNS from empresa (nolock)) /*fatura SNS*/
        AND ft.tipodoc<>4
        AND (convert(int,fprod.u_familia) = 2)
        AND fprod.protocolo != 1
		AND FI.QTT > 0
	) as x
	inner join st on x.ref = st.ref AND st.site_nr = @site_nr
	where 
		x.Qtt > 0
	group by
		x.ref, x.design, st.faminome, st.epcpond, st.epv1, st.stock,ANO,MES
	order by 
		ref desc

GO
Grant Execute on dbo.up_relatorio_infarmed_mnsrm to Public
GO
Grant Control on dbo.up_relatorio_infarmed_mnsrm to Public
Go