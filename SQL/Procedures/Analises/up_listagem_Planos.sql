/* Listagem Planos

exec up_Listagem_Planos 'Loja 1'

 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Listagem_Planos]') IS NOT NULL
	drop procedure dbo.up_Listagem_Planos
go

create procedure dbo.up_Listagem_Planos

@site  as varchar(15)
/* with encryption */
AS

DECLARE @assFarm varchar(20)

set @assFarm = (SELECT assfarm from empresa where site= @site)


SET NOCOUNT ON
select 
	cptpla.cptorgabrev
	,cptorg.nome
	,cptpla.codigo
	,cptpla.design
	,cptpla.tlotecode
	,cptpla.tlotedescr
	,compl = isnull(cptpla.compl,'') 
	,compldescr = isnull(cptpla2.design,'')
	,cptorg.u_assfarm
	,cptpla.inactivo
from 
	cptpla (nolock)
	inner join cptorg (nolock) on cptorg.cptorgstamp=cptpla.cptorgstamp
	left join cptpla cptpla2 (nolock)  on cptpla.compl=cptpla2.codigo
where 
	cptpla.inactivo=0 
	AND (cptorg.u_assfarm=@assFarm or cptorg.u_assfarm='TOD')
	AND cptpla.dataFim >= GETDATE()
order by 
	cptorg.nome, cptpla.design

GO
Grant Execute on dbo.up_Listagem_Planos to Public
Grant control on dbo.up_Listagem_Planos to Public
GO
