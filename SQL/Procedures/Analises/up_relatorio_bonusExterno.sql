/* Relatório com bonus 

	exec up_relatorio_bonusExterno '20230901','','Loja 1,Loja 2','873'

 
	exec up_relatorio_bonusExterno '20230917','','',
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_bonusExterno]') IS NOT NULL
	DROP PROCEDURE dbo.up_relatorio_bonusExterno
GO

CREATE PROCEDURE dbo.up_relatorio_bonusExterno
@date			AS DATETIME,
@ref			AS VARCHAR(254),
@site			AS VARCHAR(max),
@no				AS VARCHAR(max)
/* with encryption */
AS 
SET NOCOUNT ON
BEGIN 

	SELECT
		fl.nome, 
		fl.no, 
		fl.estab, 
		bonus.site,
		bonus.campaignCode,
		bonus.campaignName,	
		bonus_d.ref, 
		ISNULL(ST.design,ISNULL(fprod.design,'')) as desig,
		bonus_d.qttmin as qttmin,
		bonus_d.qttMax as qttmax,
		bonus_d.multipleAcquisition   as multipleAcquisition,
		bonus_d.packagingBonus as packagingBonus,
		bonus_d.pvu as pcl,
		bonus_d.discount as discount,
		bonus_d.descrOffer as bonus,
		bonus.beginDate as iniDate, 
		bonus.endDate  as endDate
	FROM bonus (nolock)
		INNER JOIN bonus_d	 (nolock)	ON bonus.stamp= bonus_d.bonus_stamp
		INNER JOIN fl		 (nolock)	ON bonus.no_fl = fl.no and bonus.estab_fl = fl.estab
		INNER JOIN empresa	 (nolock)	ON empresa.site = bonus.site
		left JOIN ST		 (nolock)	ON st.ref= bonus_d.ref and st.site_nr= empresa.no
		left JOIN fprod		 (nolock)	ON fprod.cnp= bonus_d.ref  
	WHERE (@no='' or bonus.no_fl in (SELECT * FROM up_SplitToTable(@no,','))) and @date <=CONVERT(DATE,bonus.endDate) and @date>=CONVERT(DATE,bonus.beginDate)
	and bonus.site in (SELECT * FROM up_SplitToTable(@site,','))
	  AND (@ref='' OR bonus_d.ref=@ref)
	order by 
		fl.nome, 
		fl.no, 
		fl.estab,
		bonus.site, 
		bonus.campaignCode,
		bonus.campaignName,	
		bonus_d.ref, 
		bonus_d.position



END
GO
Grant Execute on dbo.up_relatorio_bonusExterno to Public
Grant control on dbo.up_relatorio_bonusExterno to Public
GO