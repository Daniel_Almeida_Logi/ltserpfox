/* Camapanhas SMS Enviadas - BD Logitools
	
 exec up_relatorio_sms_enviadosLogitools -1,-1,'','19000101','30000101'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_sms_enviadosLogitools]') IS NOT NULL
    drop procedure dbo.up_relatorio_sms_enviadosLogitools
go

create procedure dbo.up_relatorio_sms_enviadosLogitools

@total		int,
@status		int,
@nome		varchar(250),
@dataA		datetime,
@dataDe		datetime,
@username	varchar(50)

/* WITH ENCRYPTION */
AS

select 
	username
	,total
	,status
	,name
	,[dataenvio] = convert(date,dataenvio,101)
	,horaenvio
	,mensagem
from 
	b_sms_fact (nolock)
where
	total = case when @total>=0 then @total else total end
	and status = case when @status>=0 then @status else status end
	and name like case when @nome='' then name else '%'+@nome+'%' end
	and dataenvio between @dataA and @dataDe
	and username like case when @username='' then username else '%'+@username+'%' end
order by
	dataenvio desc
	,horaenvio desc
	,campaignId desc

GO
Grant Execute on dbo.up_relatorio_sms_enviadosLogitools to Public
Grant Control on dbo.up_relatorio_sms_enviadosLogitools to Public
go
