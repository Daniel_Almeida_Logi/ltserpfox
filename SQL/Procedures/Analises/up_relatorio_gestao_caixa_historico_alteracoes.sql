/*

	Mostra alterações de caixas fechadas

	exec up_relatorio_gestao_caixa_historico_alteracoes '20210914', '20210915','Loja 1'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_caixa_historico_alteracoes]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_caixa_historico_alteracoes
go

create procedure [dbo].up_relatorio_gestao_caixa_historico_alteracoes
@dataIni  datetime, 
@dataFim  datetime,
@site	  varchar (60) 

AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosPagCentral'))
		DROP TABLE #dadosPagCentral

	create table #dadosPagCentral (
		ssstamp				char(25) 
		,Total_Dinheiro		numeric(13,3)
		,Total_Multibanco	numeric(13,3)
		,Total_Visa			numeric(13,3)
		,Total_Cheques		numeric(13,3)
		,Total_Devolucoes	numeric(13,3)
		,Total_Credito		numeric(13,3)
		,Nr_Vendas			numeric(5)
		,Nr_Atend			numeric(5)
		,Total_Caixa		numeric(13,3)
		,epaga3				numeric(13,3)
		,epaga4				numeric(13,3)
		,epaga5				numeric(13,3)
		,epaga6				numeric(13,3)
		,total_movExtra		numeric(13,3)
	)


	insert into #dadosPagCentral
		select 
			ss.ssstamp
			,Total_Dinheiro		= sum(case when left(pc.nrAtend,2) = 'Cx' then 0 else pc.evdinheiro end)
			,Total_Multibanco	= sum(pc.epaga1)
			,Total_Visa			= sum(pc.epaga2)
			,Total_Cheques		= sum(pc.echtotal)
			,Total_Devolucoes	= sum(pc.devolucoes)
			,Total_Credito		= sum(pc.creditos)
			,Nr_Vendas			= sum(pc.nrvendas)
			,Nr_Atend			= count(distinct pc.nratend)
			,Total_Caixa		= sum(case when left(pc.nrAtend,2) = 'Cx' then 0 else pc.evdinheiro end) + sum(pc.epaga2) + sum(pc.epaga1) + sum(pc.echtotal) + sum(pc.epaga3) + sum(pc.epaga4) + sum(pc.epaga5) + sum(pc.epaga6)
			,epaga3				= sum(pc.epaga3)
			,epaga4				= sum(pc.epaga4)
			,epaga5				= sum(pc.epaga5)
			,epaga6				= sum(pc.epaga6)
			,total_movExtra		= sum(case when left(pc.nrAtend,2) = 'Cx' then pc.evdinheiro else 0 end)
		from 
			ss (nolock) 
			inner join B_pagCentral (nolock) pc on pc.Ssstamp = ss.Ssstamp
		where 
			dabrir BETWEEN @dataIni and @dataFim
			and ss.site = @site
		group by
			ss.ssstamp
			,ss.site
			,ss.dabrir
			, pc.nrAtend
			, ss.dfechar
		-- 
					
		SELECT
			ssstamp
			,site
			,pnome
			,pno
			,(CASE WHEN x.isAlt = 0 then usa else '' END) as usa
			,(CASE WHEN x.isAlt = 0 then dabrir else '' END) as dabrir
			,(CASE WHEN x.isAlt = 0 then usf else '' END) as usf
			,(CASE WHEN x.isAlt = 0 then dataFecho else '' END) as dataFecho
			,(CASE WHEN x.isAlt = 1 then AlteradoPor else '' END) as AlteradoPor
			,(CASE WHEN x.isAlt = 1 then dataAlterado else '' END) as dataAlterado
			,(CASE WHEN x.isAlt = 1 then evdinheiro else evOri END) as evdinheiro
			,epaga2
			,epaga1
			,echtotal
			,epaga3
			,epaga4
			,epaga5
			,epaga6
			,comissaoTPA
			,sacodinheiro 
			,obs
			,cxstamp
			,ousrdata
			,ousrhora
		FROM
			(				
			select 
				ss.ssstamp
				,ss.site
				,ss.pnome
				,ss.pno
				,(ss.ausername+' ['+CONVERT(varchar,ss.auserno)+']') usa
				,(convert(varchar,ss.dabrir,102) + ' ' + ss.habrir) as dabrir
				,(ss.fusername+' ['+CONVERT(varchar,ss.fuserno)+']') usf
				,(convert(varchar,ss.dfechar,102) + ' ' + ss.hfechar) as dataFecho
				,(select (LTRIM(RTRIM(nome)) +' ['+CONVERT(varchar,b_us.userno)+']') from b_us(nolock) where iniciais = ss.ousrinis) as AlteradoPor
				,(convert(varchar,ss.ousrdata,102) + ' ' + ss.ousrhora) as dataAlterado
				,(CASE 
					WHEN (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp) <> 1
						THEN 
						(CASE 
							WHEN (select top 1 ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata asc, ousrhora asc) <> ss.ssstamp
								THEN 1
							ELSE 
								0
						END)
					ELSE
						0
				END) as isAlt
				,(CASE WHEN ISNULL(Total_Dinheiro, ss.evdinheiro) <> 0 THEN ISNULL(Total_Dinheiro, ss.evdinheiro) ELSE ss.evdinheiro  END) as evdinheiro
				,(CASE WHEN ISNULL(#dadosPagCentral.Total_Multibanco, ss.epaga1) <> 0 THEN ISNULL(#dadosPagCentral.Total_Multibanco, ss.epaga1) ELSE ss.epaga1 END) as epaga2
				,(CASE WHEN ISNULL(#dadosPagCentral.Total_Visa, ss.epaga2) <> 0 THEN ISNULL(#dadosPagCentral.Total_Visa, ss.epaga2) ELSE ss.epaga2 END) as epaga1
				,(CASE WHEN ISNULL(#dadosPagCentral.Total_Cheques, ss.echtotal) <> 0 THEN ISNULL(#dadosPagCentral.Total_Cheques, ss.echtotal) ELSE ss.echtotal END) as echtotal
				,(CASE WHEN ISNULL(#dadosPagCentral.epaga3, 0) <> 0 THEN ISNULL(#dadosPagCentral.epaga3, 0) ELSE ss.fecho_epaga3 END) as epaga3
				,(CASE WHEN ISNULL(#dadosPagCentral.epaga4, 0) <> 0 THEN ISNULL(#dadosPagCentral.epaga4, 0) ELSE ss.fecho_epaga4 END) as epaga4
				,(CASE WHEN ISNULL(#dadosPagCentral.epaga5, 0) <> 0 THEN ISNULL(#dadosPagCentral.epaga5, 0) ELSE ss.fecho_epaga5 END) as epaga5
				,(CASE WHEN ISNULL(#dadosPagCentral.epaga6, 0) <> 0 THEN ISNULL(#dadosPagCentral.epaga6, 0) ELSE ss.fecho_epaga6 END) as epaga6
				,ss.comissaoTPA
				,sacodinheiro 
				,ss.causa as obs
				,ss.cxstamp
				,ss.ousrdata
				,ss.ousrhora
				,ss.evdinheiro as evOri
			from 
				ss (nolock)
				left join #dadosPagCentral on ss.ssstamp COLLATE SQL_Latin1_General_CP1_CI_AI = #dadosPagCentral.ssstamp COLLATE SQL_Latin1_General_CP1_CI_AI 
			where 
				dabrir BETWEEN @dataIni and @dataFim
				AND ss.site = @site
				and ss.fechada = 1
				and exists (select 1 from ss(nolock) as ssb where ssb.ssstamp <> ss.ssstamp and ssb.cxstamp = ss.cxstamp)
			group by
				ss.ssstamp
				,ss.site
				,ss.pnome
				,ss.pno
				,(ss.ausername+' ['+CONVERT(varchar,ss.auserno)+']') 
				,ss.habrir
				,(ss.fusername+' ['+CONVERT(varchar,ss.fuserno)+']')
				,ss.dfechar
				,ss.hfechar
				,(convert(varchar,ss.dfechar,102)+' '+ss.hfechar) 
				,ss.fechada
				,fdStamp	  
				,sacodinheiro 
				,Total_Dinheiro
				,ss.evdinheiro
				,ss.cxstamp
				,ss.ousrdata
				,ss.ousrhora
				,ss.epaga1
				,ss.epaga2
				,epaga3
				,epaga4
				,epaga5
				,epaga6
				,ss.ousrinis
				,Total_Visa
				,Total_Multibanco
				,ss.dabrir
				,#dadosPagCentral.Total_Cheques
				,ss.echtotal
				,ss.causa
				,ss.comissaoTPA
				,ss.fecho_epaga3
				,ss.fecho_epaga4
				,ss.fecho_epaga5
				,ss.fecho_epaga6
			) as x
			order by 
				cxstamp,
				ousrdata asc,
				ousrhora asc


	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosPagCentral'))
	DROP TABLE #dadosPagCentral

GO
Grant Execute On dbo.up_relatorio_gestao_caixa_historico_alteracoes to Public
Grant control On dbo.up_relatorio_gestao_caixa_historico_alteracoes to Public
GO