/* Relatório Documentos por Operador  

Alterado a 2020-08-07 por JG: contemplar multiselecao de sites  

	exec up_relatorio_gestao_operador_Documentos '20170804','20220627','','1','','','bene - mais um carrada de caracteres so para encher e ver se parte!','','','Loja 1', 0 
	exec up_relatorio_gestao_operador_Documentos '20170804','20170808','','1','','','','','','Loja 2', 0
	exec up_relatorio_gestao_operador_Documentos '20170804','20170808','','1','','','','','','Loja 1,Loja 2', 0

	exec up_relatorio_gestao_operador_Documentos '20200104','20200802','','1','','','','','','Loja 1', 0
	exec up_relatorio_gestao_operador_Documentos '20200104','20200802','','1','','','','','','Loja 2', 0
	exec up_relatorio_gestao_operador_Documentos '20200104','20200802','','1','','','','','','Loja 1,Loja 2', 0

	exec up_relatorio_gestao_operador_Documentos '20170804','20170808','','1','','','','','MSRM','Loja 1', 0
	exec up_relatorio_gestao_operador_Documentos '20200104','20200802','','1','','','','','MSRM','Loja 2', 0


	exec up_relatorio_gestao_operador_Documentos '20200924', '20200924','','1','','','','','','Loja 1', 0
	exec up_relatorio_gestao_operador_Documentos '20200924', '20200924','','1','','','','','MSRM','Loja 1', 0


	select * from b_us
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_operador_Documentos]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_operador_Documentos
go

create procedure dbo.up_relatorio_gestao_operador_Documentos
@dataIni		as datetime,
@dataFim		as datetime,
@tipoDoc		as varchar(max),
@op				as varchar(max),
@ref			as varchar(254),
@lab			as varchar(100),
@marca			as varchar(200),
@familia		as varchar(max),
@atributosFam	as varchar(max),
@site			as varchar(254),
@compart100		as bit
/* with encryption */
AS 
BEGIN
SET LANGUAGE PORTUGUESE
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#utentes'))
		DROP TABLE #utentes
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
		DROP TABLE #dadosFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST'))
		DROP TABLE #dadosST
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#EmpresaArm'))
		DROP TABLE #EmpresaArm
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dados_1'))
		DROP TABLE #dados_1

	SET @atributosFam = (CASE WHEN (@atributosFam IS NULL OR @atributosFam = '') THEN ';;;' ELSE @atributosFam END)

	DECLARE @departamento VARCHAR(254) = ''
	SET @departamento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 1),'') 
	DECLARE @sessao       VARCHAR(254) = ''
	set @sessao = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 2) ,'')
	DECLARE @categoria    VARCHAR(254) = ''
	SET @categoria = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 3),'') 
	DECLARE @segmento     VARCHAR(254) = ''
	SET @segmento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 4),'') 


	declare @site_nr varchar(20)
	set @site_nr = isnull((select  convert(varchar(254),(select 
							convert(varchar,no) + ', '
						from 
							empresa (nolock)							
						Where 
							site in (Select items from dbo.up_splitToTable(@site, ','))
						FOR XML PATH(''))) as no),0)

	if @site = ''
		set @site = isnull((select  convert(varchar(254),(select 
							site + ', '
						from 
							empresa (nolock)							
						FOR XML PATH(''))) as no),0)


	select 
		armazem
	into
		#EmpresaArm
	From
		empresa
		inner join empresa_arm on empresa.no = empresa_arm.empresa_no
	where
		--empresa.site = case when @site = '' then empresa.site else @site end	
		empresa.site in (Select items from dbo.up_splitToTable(@site, ',')) 





	/* Calc Familias Infarmed Produtos */
	Select 
		distinct ref, nome
	into 
		#dadosFamilia
	From 
		stfami (nolock)  
	Where 
		ref in (Select items from dbo.up_splitToTable(@familia,',')) 
		Or @familia = '0' 
		Or @familia = ''
	
	
	/* Calc Operadores  */
	Select
		iniciais
		,cm = userno
		,username
	into
		#dadosOperadores
	from
		b_us (nolock)
	Where
		userno in (select items from dbo.up_splitToTable(@op,','))
		or @op= '0'
		or @op= ''
	--select * from #dadosOperadores
	
	
	/* Calc Dados ST */
	Select
		ref 
		,st.design
		,usr1
		,u_lab
		,epcusto		
		, ISNULL(A.descr,'') as departamento
		, ISNULL(B.descr,'') as seccao
		, ISNULL(C.descr,'') as categoria
		, ISNULL(D.descr,'') as segmento
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 1) valDepartamento
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 2) valSeccao
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 3) valCategoria
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 4) valSegmento
		,site_nr
		
	into 
		#dadosST
	From
		st (nolock)	
		left join grande_mercado_hmr (nolock) A on A.id = st.u_depstamp
		left join mercado_hmr (nolock) B on B.id = st.u_secstamp
		left join categoria_hmr (nolock) C on C.id = st.u_catstamp 
		left join segmento_hmr (nolock) D on D.id = st.u_segstamp		
	Where	
		st.u_lab = case when @lab = '' then u_lab else @lab end
		And st.usr1 = case when @marca = '' then usr1 else @marca end	
		--and site_nr = @site_nr	
		and site_nr in (Select items from dbo.up_splitToTable(@site_nr, ','))

	

	/* Define se vais mostrar resultados abaixo do Cliente 199
	declare @Entidades as bit
	set @Entidades = case when (select tipoempresa from empresa where site in (Select items from dbo.up_splitToTable(@site, ',')) ) in ('FARMACIA', 'PARAFARMACIA') then convert(bit,1) else convert(bit,0) end


	declare @site varchar(200)
	set @site='Loja 1,Loja 2'
	select tipoempresa from empresa where site in (Select items from dbo.up_splitToTable(@site, ','))
	 */



	/* Dados Base Vendas  */
	create table #dadosVendasBase (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(150)
		,familia varchar(18)
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
		--alterado de (6,2) para (19,6)
		,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)
	)

   
    insert #dadosVendasBase
    exec up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site
	--exec up_relatorio_vendas_base_detalhe '20150401', '20150531', 'Loja 1'
	--exec up_relatorio_vendas_base_detalhe '20200104', '20200802', 'Loja 1'


	/* Elimina Vendas tendo em conta tipo de cliente*/
	--If @Entidades = 1
	delete from #dadosVendasBase where #dadosVendasBase.no < 199



	--select   * from #EmpresaArm
	--select   * from #dadosST
	--select   * from #dadosVendasBase


	/* result Set */
	SELECT

		#dadosVendasBase.ousrinis 
		,#dadosVendasBase.ousrhora
		,#dadosVendasBase.fdata
		,td.nmdoc
		,#dadosVendasBase.fno
		,#dadosVendasBase.ref
		,#dadosVendasBase.design
		,faminome = #dadosFamilia.nome
		,lab = isnull(#dadosST.u_lab,'')
		,marca = isnull(#dadosST.usr1,'')
		,#dadosVendasBase.qtt
		,descCom = CASE WHEN #dadosVendasBase.[tipodoc] != 3 THEN ABS(#dadosVendasBase.[descvalor]) ELSE ABS(#dadosVendasBase.[descvalor])*-1 END
		,#dadosVendasBase.etiliquido
		,#dadosVendasBase.ettent1	
		,#dadosVendasBase.ettent2	
		,#dadosVendasBase.epcpond
		,#dadosVendasBase.u_epvp			
		,margem = (#dadosVendasBase.ETILIQUIDO + (
				CASE WHEN TD.tipodoc = 3 THEN (#dadosVendasBase.ettent1 * -1) + (#dadosVendasBase.ettent2*-1) ELSE (#dadosVendasBase.ettent1 + #dadosVendasBase.ettent2) END) )
				-
				CASE WHEN TD.tipodoc = 3 THEN (#dadosST.epcusto * #dadosVendasBase.qtt) *-1 ELSE  #dadosST.epcusto * #dadosVendasBase.qtt END
		,TotalDescontos = 	CASE WHEN TD.tipodoc = 3 THEN (CASE WHEN #dadosVendasBase.desconto between 0.01 and 99.99 then ((#dadosVendasBase.u_epvp * #dadosVendasBase.qtt) - (#dadosVendasBase.etiliquido))*-1
																WHEN #dadosVendasBase.desconto=100 then (#dadosVendasBase.u_epvp*#dadosVendasBase.qtt)*-1
																ELSE 0 
															END)*-1
							ELSE
								(CASE 
									WHEN #dadosVendasBase.desconto between 0.01 and 99.99 then (#dadosVendasBase.u_epvp*#dadosVendasBase.qtt)-(#dadosVendasBase.etiliquido)
									WHEN #dadosVendasBase.desconto=100 then #dadosVendasBase.u_epvp*#dadosVendasBase.qtt
									ELSE 0 
								END)
							END

		,  #dadosST.site_nr
	into 
		#dados_1
	FROM	
		#dadosVendasBase  
		INNER JOIN TD (nolock) ON TD.NDOC = #dadosVendasBase.NDOC
		inner join #dadosFamilia on #dadosVendasBase.familia = #dadosFamilia.ref
		left Join #dadosST ON #dadosVendasBase.ref = #dadosST.ref and #dadosST.site_nr = #dadosVendasBase.loja_nr
		left join #dadosOperadores on #dadosVendasBase.ousrinis = #dadosOperadores.iniciais
	WHERE		
	--exec up_relatorio_gestao_operador_Documentos '20171013','20171013','','1','','','','','','Loja 1', 0
		(td.nmdoc in (select items from dbo.up_splitToTable(@tipoDoc,',')) or @tipoDoc = '')
		and (#dadosVendasBase.ref in (Select items from dbo.up_splitToTable(@ref,',')) or @ref = '')
		and isnull(#dadosST.u_lab,'') = CASE When @lab = '' Then isnull(#dadosST.u_lab,'') Else @lab End
		and isnull(#dadosST.usr1,'') = CASE When @marca = '' Then isnull(#dadosST.usr1,'') Else @marca End
		and #dadosVendasBase.familia in (Select ref from #dadosFamilia)
		AND departamento = (CASE WHEN @departamento = '' THEN departamento ELSE @departamento END)
		AND seccao = (CASE WHEN @sessao = '' THEN seccao ELSE  @sessao END)
		AND categoria = (CASE WHEN @categoria = '' THEN categoria ELSE  @categoria END)
		AND segmento = (CASE WHEN @segmento = '' THEN segmento ELSE  @segmento END)
		and #dadosVendasBase.etiliquido = 
			Case when @compart100 = 1 
				then  0  
			else 
				#dadosVendasBase.etiliquido 
			end 
		and (#dadosVendasBase.ettent1 + #dadosVendasBase.ettent2) != Case when @compart100 = 1 then 0 else -99999999999 end 
		and #dadosOperadores.cm = case when @op = '' then #dadosOperadores.cm else @op end 
	Order by
		#dadosVendasBase.fdata, #dadosVendasBase.ousrhora  

		

	select * from #dados_1


	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#utentes'))
		DROP TABLE #utentes
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
		DROP TABLE #dadosFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST'))
		DROP TABLE #dadosST
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#EmpresaArm'))
		DROP TABLE #EmpresaArm
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dados_1'))
		DROP TABLE #dados_1
END


GO
Grant Execute on dbo.up_relatorio_gestao_operador_Documentos to Public
Grant control on dbo.up_relatorio_gestao_operador_Documentos to Public
GO