/* Relatório que mostra produtos sem rotação
 
	exec up_relatorio_produtos_sem_consumo '20191101','20191122','','','Bene Farmacêutica','','Loja 1','',-9999, '', '20191122'

	exec up_relatorio_produtos_sem_consumo '20200801','20200810','','','Bene Farmacêutica','','Loja 1','',-9999, '', '20191122'



		exec up_relatorio_produtos_sem_consumo '19000101','20200810','','','','','Loja 3','',-9999, '', '20191122'
	exec up_relatorio_produtos_sem_consumo '19000101','20200810','','','','','Loja 2','',-9999, '', '20191122'
	exec up_relatorio_produtos_sem_consumo '19000101','20200810','','','','','Loja 2,Loja 3','',-9999, '', '20191122'

	exec up_relatorio_produtos_sem_consumo '20170101','20171210','','','','','Loja 3','',-9999, '', '20170101'
	exec up_relatorio_produtos_sem_consumo '20170101','20171210','','','','','Loja 2','',-9999, '', '20170101'
	exec up_relatorio_produtos_sem_consumo '20170101','20171210','','','','','Loja 2,Loja 3','',-9999, '', '20170101'


*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_relatorio_produtos_sem_consumo]') IS NOT NULL
	DROP procedure dbo.up_relatorio_produtos_sem_consumo
GO

CREATE procedure dbo.up_relatorio_produtos_sem_consumo
@dataIni		datetime,
@dataFim		datetime,
@ref			varchar(18),
@familia		varchar(max),
@lab			varchar(120),
@marca			varchar(200),
@site			varchar(60),
@atributosFam   varchar(max),
@stock			numeric(9,2),
@generico		varchar(18),
@dataucompra		datetime
/* WITH ENCRYPTION */ 
AS
BEGIN
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosComMov'))
		DROP TABLE #DadosComMov
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosFami'))
		DROP TABLE #DadosFami
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosMarca'))
		DROP TABLE #DadosMarca
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosLab'))
		DROP TABLE #DadosLab
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosST'))
		DROP TABLE #DadosST
	/* Calc Loja e Armazens */
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)
	/* Convereter Valor Generico*/
	IF @generico = 'GENERICO'
		set @generico = 1
	IF @generico = 'NÃO GENERICO'
		set @generico = 0
	/* Tabela Movimentos*/
	Select 
		sl.ref
		,sa = sum(case when cm <50 then qtt else -qtt end)
	into 
		#DadosComMov
	From
		sl (nolock)
		left join empresa_arm (nolock) on empresa_arm.armazem = sl.armazem
		left join empresa (nolock) on empresa_arm.empresa_no = empresa.no
	Where
		datalc between @dataIni and @dataFim
		and (empresa.site is null or empresa.site = @site)
		and sl.cm in (75,76,77)
	group by
		sl.ref,sl.armazem
	
	
	--Select * from #DadosComMov
	


	/* Tabela Familia */
	Select 
		distinct ref 
	into 
		#DadosFami
	From
		stfami (nolock)
	Where
		ref in (Select items from dbo.up_splitToTable(@familia,','))
		or @familia = '0' 
		or @familia = ''	
	-- Select * from #DadosFami
	/* Tabela Marca */
	Select 
		distinct usr1 
	into
		#DadosMarca
	From
		st (nolock)
	Where
		usr1 in (Select items from dbo.up_splitToTable(@marca,','))
		or @marca = ''	
	-- Select * from #DadosMarca
	/* Tabela Labs */
	Select 
		distinct u_lab 
	into 
		#DadosLab
	From
		st (nolock)
	Where
		u_lab in (Select items from dbo.up_splitToTable(@lab,','))
		or @lab = ''	
	-- Select * from #DadosLab
	/* Tabela DadosST */
		Select
			st.ref 
			,st.design
			,faminome
			,familia
			,usr1
			,u_lab
			,fprod.generico
			,tabiva
			,epcpond
			,epv1
			,marg1
			,stock
			,UINTR
			,USAID
			,departamento = isnull(b_famDepartamentos.design ,'')
			,seccao = isnull(b_famSeccoes.design,'')
			,categoria = isnull(b_famCategorias.design,'')
			,famfamilia =  isnull(b_famFamilias.design,'')
			,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 1) valDepartamento
			,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 2) valSeccao
			,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 3) valCategoria
			,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 4) valFamfamilia
			, epcult
		into 
			#DadosST
		From
			st (nolock)
			left join fprod (nolock) on st.ref = fprod.cnp
			left join b_famDepartamentos (nolock) on st.u_depstamp = b_famDepartamentos.depstamp
			left join b_famSeccoes (nolock) on st.u_secstamp = b_famSeccoes.secstamp
			left join b_famCategorias (nolock) on st.u_catstamp = b_famCategorias.catstamp
			left join b_famFamilias (nolock) on st.u_famstamp = b_famFamilias.famstamp
		where 
			site_nr = @site_nr
			and stock>0
			and (isnull((select top 1 datalc from sl where cm in (1,2,6) and sl.ref=st.ref order by datalc desc),'19000101')<@dataucompra or st.ref in (select distinct ref from sl where nome like '%Inventário Migração SIF2000%'))
	
	
--select * from 	#DadosST
	
	
	
		/* Preparar Result Set Final */
		
		Select 
			#dadosST.ref
			,design
			,faminome
			,marca = usr1
			,lab = u_lab
			,Generico
			,stock = SUM(isnull(sa.stock,0))
			,pvp = epv1
			,epv1val = SUM(isnull(sa.stock,0)*epv1)
			,#DadosST.UINTR
			,#DadosST.USAID
			,#DadosST.epcult
			,epcultval= SUM(isnull(sa.stock,0)*#DadosST.epcult)
		from 
			#DadosST
			left join sa on sa.ref = #DadosST.ref 
			left join empresa_arm on empresa_arm.armazem = sa.armazem
			left join empresa on empresa_arm.empresa_no = empresa.no
		Where
			#DadosST.ref not in (select ref from #DadosComMov)
			and #DadosST.ref = case when @ref = '' then #DadosST.ref else @ref end 
			and #DadosST.familia in (Select ref from #DadosFami)
			and #DadosST.usr1 in (Select usr1 from #DadosMarca)
			and #DadosST.u_lab in (Select u_lab from #DadosLab)
			and #DadosST.generico = case when @generico = '' then  #DadosST.generico else @generico end
			and isnull(sa.stock,0) > @stock
			and 
			(
				(departamento	= valDepartamento or valDepartamento is null)
				and (seccao		= valSeccao or valSeccao is null)
				and (categoria	= valCategoria or valCategoria is null)
				and (famfamilia	= valFamfamilia or valFamfamilia is null)
			)
			and (empresa.site is null or empresa.site = @site)
		GROUP BY 
			#dadosST.ref
			,design
			,faminome
			,usr1
			,u_lab
			,Generico
			,Epv1
			,#DadosST.UINTR
			,#DadosST.USAID
			,#DadosST.epcult
		order by ref




		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosComMov'))
			DROP TABLE #DadosComMov
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosFami'))
			DROP TABLE #DadosFami
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosMarca'))
			DROP TABLE #DadosMarca
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosLab'))
			DROP TABLE #DadosLab
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosST'))
			DROP TABLE #DadosST
END

GO
GRANT EXECUTE on dbo.up_relatorio_produtos_sem_consumo TO PUBLIC
GRANT CONTROL on dbo.up_relatorio_produtos_sem_consumo TO PUBLIC
GO