/* Relatório Caixas Operador

	exec up_relatorio_gestao_caixa_operador '20221221','20221221', 0, 999, 'Loja 1'
	exec up_relatorio_gestao_caixa_operador '20221212','20221212', 0, 999, 'Loja 1'
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_caixa_operador]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_caixa_operador
go

create procedure [dbo].up_relatorio_gestao_caixa_operador
	@dataIni		DATETIME
	,@dataFim		DATETIME
	,@opIni			AS NUMERIC(9,0)
	,@opFim			AS NUMERIC(9,0)
	,@site			VARCHAR(30)

/* with encryption */

AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#vendasdevcx'))
		DROP TABLE #vendasdevcx
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#vendasdevss'))
		DROP TABLE #vendasdevss
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#vendasdevulocoesss'))
		DROP TABLE #vendasdevulocoesss
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#vendasdevulocoescx'))
		DROP TABLE #vendasdevulocoescx

	DECLARE @usaPagCentral		BIT
	DECLARE @gestaoCxOperador	BIT
	SET @usaPagCentral		= ISNULL((select bool from b_parameters(nolock) where stamp='ADM0000000079'),0)
	SET @gestaoCxOperador	=  (select gestao_cx_operador from empresa(nolock) where site = @site)

	IF @gestaoCxOperador = 0
		BEGIN
			SELECT
				cx.dabrir																						AS fdata
				, ft.vendedor
				, ft.site
				, ft.pnome
				, ft.pno
				,ft.ndoc
				,SUM(case when ft.tipodoc=1 then 1 else 0 end)													AS vendas 
				,ft.cxstamp
			INTO
				#vendasdevcx
			FROM 
				ft (nolock)
				INNER JOIN cx (nolock)	on cx.cxstamp = ft.cxstamp  
			WHERE 
				cx.dabrir between @dataini and @DataFim
				and ft.cxstamp !=''
				and ft.no>199 
			GROUP BY cx.dabrir, ft.vendedor, ft.site , ft.pnome, ft.pno, ft.ndoc,ft.u_nratend,ft.nmdoc,ft.cxstamp, FT.ftstamp

			SELECT 
				cx.dabrir																						AS fdata
				, ft.vendedor
				, ft.site
				, ft.pnome
				, ft.pno
				,ft.ndoc
				,(CASE WHEN fi.ofistamp != '' and ref !='' and ref != 'R000001'
					and (select count(*) from fi(nolock) fii where fii.ofistamp = fi.fistamp and fii.ref !='' and 
					 fii.ref != 'R000001' and tipodoc = 1)=0 
					THEN 1  ELSE 0 END)																		AS dev
				,ft.cxstamp
				,ft.ftstamp
			INTO
				#vendasdevulocoescx
			 FROM 
				ft (nolock)
				INNER JOIN fi(nolock)	ON fi.ftstamp = ft.ftstamp
				INNER JOIN td (nolock)	on td.ndoc = ft.ndoc
				INNER JOIN cx (nolock)	on cx.cxstamp = ft.cxstamp  
			WHERE 
				cx.dabrir between @dataini and @DataFim
				and ft.cxstamp !=''
				and td.tipodoc=3
			GROUP BY 
				cx.dabrir, ft.vendedor, ft.site , ft.pnome, ft.pno, ft.ndoc,ft.u_nratend,ft.nmdoc,ft.cxstamp, FT.ftstamp, 
					fi.fistamp, fi.ofistamp, fi.ref
				
			delete #vendasdevulocoescx where dev < 1	

			select
				cx.site
				,Data				= convert(varchar,cx.dabrir,102)
				,cx.pnome
				,cx.pno
				,Vendedor			= pc.vendedor
				,Nome				= pc.nome
				,Total_Dinheiro		= sum(case when left(pc.nrAtend,2) = 'Cx' then 0 else pc.evdinheiro end) 
				,Total_Multibanco	= sum(pc.epaga2)
				,Total_Visa			= sum(pc.epaga1)
				,Total_Cheques		= sum(pc.echtotal)
				,Total_Devolucoes	= sum(pc.devolucoes)
				,Total_Credito		= SUM(pc.creditos)
				,Nr_Vendas			= Isnull((select sum((vendas)) 
											from #vendasdevcx(NOLOCK) INNER JOIN TD (NOLOCK) ON #vendasdevcx.ndoc = TD.ndoc 
											where cx.site=#vendasdevcx.site and #vendasdevcx.fdata >= cx.dabrir 
											and  #vendasdevcx.vendedor = pc.vendedor and #vendasdevcx.cxstamp =cx.cxstamp),0)
				,Nr_Dev				= Isnull((select sum((dev)) 
										 from #vendasdevulocoescx(NOLOCK) INNER JOIN TD (NOLOCK) ON #vendasdevulocoescx.ndoc = TD.ndoc  
										 where cx.site=#vendasdevulocoescx.site and #vendasdevulocoescx.fdata >= cx.dabrir   
										and  #vendasdevulocoescx.vendedor = pc.vendedor and #vendasdevulocoescx.cxstamp =cx.cxstamp),0)
				,Nr_Atend			= count(distinct pc.nratend)
				,Total_Caixa		= sum(pc.evdinheiro) + sum(pc.epaga2) + sum(pc.epaga1) + sum(pc.echtotal) + sum(pc.epaga3) + sum(pc.epaga4) + sum(pc.epaga5) + sum(pc.epaga6)
				,Pendente			= sum(case when @usaPagCentral=1 
												then (case when fechado=0 and abatido=0
											 				then pc.total
															else 0 end)
									else 0 end)
				,epaga3				= sum(pc.epaga3) 
				,epaga4				= sum(pc.epaga4 * ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'USD', pc.oData),1)) --USD
				,epaga5				= sum(pc.epaga5 * ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'EUR', pc.oData),1))--EUR
				,epaga6				= sum(pc.epaga6)
				,total_movExtra		= sum(case when left(pc.nrAtend,2) = 'Cx' then pc.total else 0 end)
			FROM 
				cx (nolock) 
				inner join B_pagCentral(nolock) pc  on pc.cxstamp = cx.cxstamp and pc.site = cx.site
			WHERE 
				cx.dabrir between @dataini and @DataFim 
				and cx.site = (case when @site = '' then pc.site else @site end )
				and pc.vendedor >= @opIni
				and pc.vendedor<= @opFim
				and cx.fechada = 1
				and pc.ignoraCaixa = 0
			GROUP BY 
				cx.site
				,cx.dabrir
				,pc.vendedor
				,pc.nome	
				,cx.pno
				,cx.pnome
				,cx.cxstamp
			ORDER BY
				 pc.vendedor
		END
	ELSE 
		BEGIN

			SELECT
				ss.dabrir																						AS fdata
				, ft.vendedor
				, ft.site
				, ft.pnome
				, ft.pno
				,ft.ndoc
				,SUM(case when ft.tipodoc=1 then 1 else 0 end)													AS vendas 
				,ft.ssstamp
			INTO
				#vendasdevss
			FROM 
				ft (nolock)
				INNER JOIN td (nolock)	on td.ndoc = ft.ndoc
				INNER JOIN ss (nolock)	on ss.ssstamp = ft.ssstamp  
			WHERE 
				ss.dabrir between @dataini and @DataFim
				and ft.ssstamp !=''
				and ft.no>199 
				and ft.ssstamp !=''
			GROUP BY ss.dabrir, ft.vendedor, ft.site , ft.pnome, ft.pno, ft.ndoc,ft.u_nratend,ft.nmdoc,ft.ssstamp, FT.ftstamp

			SELECT  
				ss.dabrir																						AS fdata
				, ft.vendedor
				, ft.site
				, ft.pnome
				, ft.pno
				,ft.ndoc
				,(CASE WHEN fi.ofistamp != '' and ref !=''  and ref != 'R000001' 
					and (select count(*) from fi(nolock) fii where fii.ofistamp = fi.fistamp and fii.ref !=''  
					and fii.ref != 'R000001'and tipodoc = 1)=0 
					THEN 1  ELSE 0 END)																		AS dev
				,ft.ssstamp
				,ft.ftstamp
			INTO
				#vendasdevulocoesss
			 FROM 
				ft (nolock)
				 INNER JOIN fi(nolock)	ON fi.ftstamp = ft.ftstamp
				INNER JOIN td (nolock)	on td.ndoc = ft.ndoc
				INNER JOIN ss (nolock)	on ss.ssstamp = ft.ssstamp  
			WHERE 
				ss.dabrir between @dataini and @DataFim 
				and ft.ssstamp !=''
				and ft.tipodoc=3
			GROUP BY ss.dabrir, ft.vendedor, ft.site , ft.pnome, ft.pno, ft.ndoc,ft.u_nratend,ft.nmdoc,ft.ssstamp, FT.ftstamp,
					fi.fistamp, fi.ofistamp, fi.ref
				
			delete #vendasdevulocoesss where dev < 1

			SELECT 
				ss.site
				,Data				= convert(varchar,ss.dabrir,102)
				,ss.pnome
				,ss.pno
				,Vendedor			= pc.vendedor
				,Nome				= pc.nome
				,Total_Dinheiro		= sum(case when left(pc.nrAtend,2) = 'Cx' then 0 else pc.evdinheiro end) 
				,Total_Multibanco	= sum(pc.epaga2)
				,Total_Visa			= sum(pc.epaga1)
				,Total_Cheques		= sum(pc.echtotal)
				,Total_Devolucoes	= sum(pc.devolucoes)
				,Total_Credito		= SUM(pc.creditos)
				,Nr_Vendas			= Isnull((select sum((vendas)) 
											from #vendasdevss(NOLOCK) INNER JOIN TD (NOLOCK) ON #vendasdevss.ndoc = TD.ndoc 
											where ss.site=#vendasdevss.site and #vendasdevss.fdata >= ss.dabrir 
											and  #vendasdevss.vendedor = pc.vendedor and #vendasdevss.ssstamp =ss.ssstamp),0)
				,Nr_Dev				= Isnull((select sum((dev)) 
											  from #vendasdevulocoesss(NOLOCK) INNER JOIN TD (NOLOCK) ON #vendasdevulocoesss.ndoc = TD.ndoc  
											  where ss.site=#vendasdevulocoesss.site and #vendasdevulocoesss.fdata >= ss.dabrir   
											  and  #vendasdevulocoesss.vendedor = pc.vendedor and #vendasdevulocoesss.ssstamp =ss.ssstamp),0)
				,Nr_Atend			= ISNULL(COUNT(distinct pc.nratend),0)
				,Total_Caixa		= sum(pc.evdinheiro) + sum(pc.epaga2) + sum(pc.epaga1) + sum(pc.echtotal) + sum(pc.epaga3) + sum(pc.epaga4) + sum(pc.epaga5) + sum(pc.epaga6)
				,Pendente			= sum(case when @usaPagCentral=1 
												then (case when fechado=0 and abatido=0
															then pc.total
															else 0 end)
									else 0 end)
				,epaga3				= sum(pc.epaga3)
				,epaga4				= sum(pc.epaga4 * ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'USD', pc.oData),1)) --USD
				,epaga5				= sum(pc.epaga5 * ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'EUR', pc.oData),1))--EUR
				,epaga6				= sum(pc.epaga6)
				,total_movExtra		= sum(case when left(pc.nrAtend,2) = 'Cx' then pc.total else 0 end)
			FROM 
				ss (nolock)
				INNER JOIN B_pagCentral(nolock) pc on pc.ssstamp = ss.ssstamp 	and pc.site = ss.site				      
			WHERE 
				ss.dabrir between @dataini and @DataFim 
				and ss.site = case when @site = '' then ss.site else @site end 
				and pc.vendedor >= @opIni
				and pc.vendedor <= @opFim
				and ss.fechada = 1
				and pc.ignoraCaixa = 0
			GROUP BY 
				ss.site
				,ss.dabrir
				,pc.vendedor
				,pc.nome				
				,ss.pno
				,ss.pnome
				,ss.ssstamp
			ORDER BY
				 pc.vendedor
		END




	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#vendasdevcx'))
		DROP TABLE #vendasdevcx
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#vendasdevss'))
		DROP TABLE #vendasdevss
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#vendasdevulocoesss'))
		DROP TABLE #vendasdevulocoesss
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#vendasdevulocoescx'))
		DROP TABLE #vendasdevulocoescx
GO
Grant Execute On dbo.up_relatorio_gestao_caixa_operador to Public
Grant control On dbo.up_relatorio_gestao_caixa_operador to Public
GO