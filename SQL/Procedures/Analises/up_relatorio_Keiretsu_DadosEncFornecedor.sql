-- exec up_relatorio_Keiretsu_DadosEncFornecedor 'ADM9EADE76F-7AF5-4EC5-9AC'

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_Keiretsu_DadosEncFornecedor]') IS NOT NULL
	drop procedure dbo.up_relatorio_Keiretsu_DadosEncFornecedor
go

create procedure dbo.up_relatorio_Keiretsu_DadosEncFornecedor
@encstamp as uniqueidentifier


/* with encryption */
AS 
BEGIN

Select 
	boEncFornec.obrano
	,boEncFornec.dataobra
	,nome = RTRIM(LTRIM(boEncFornec.nome)) + ' [' + LTRIM(STR(boEncFornec.no)) + ']' + '[' + LTRIM(STR(boEncFornec.estab)) + ']'
From
	bo boEncFornec
Where 
	bostamp = @encstamp

END

GO
Grant Execute on dbo.up_relatorio_Keiretsu_DadosEncFornecedor to Public
GO
Grant control on dbo.up_relatorio_Keiretsu_DadosEncFornecedor to Public
GO


