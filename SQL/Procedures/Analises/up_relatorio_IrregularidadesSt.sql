/* Relatório Irregularidades Stock
	
	exec up_relatorio_irregularidadesSt '', 0, 0, 0, '>', 1, 1, '>', 0, 0, '>' , 0, '<' , 0 , 0, 0, 'Loja 1', 1
exec up_relatorio_irregularidadesSt @ref=N'',@activos=N'-1',@pvpTag=0,@pvp=N'0',@pvpOp=N'>',@stockTag=1,@stock=N'0',@stockOp=N'>',@pctTag=0,@pct=N'0',@pctOp=N'>',@margemTag=0,@margemOp=N'>',@margem=N'0',@DiasSemMovimentos=0,@Familia1_IvaDif6=0,@site=N'Loja 1',@pvpDifPvpInfarmed=0,@pclTag=0,@pcl=N'0',@pclOp=N'>'
	
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_irregularidadesSt]') IS NOT NULL
	drop procedure dbo.up_relatorio_irregularidadesSt
go

create procedure dbo.up_relatorio_irregularidadesSt
	@ref as varchar(254)
	,@activos as varchar(10)
	,@pvpTag as bit
	,@pvp as varchar(10)
	,@pvpOp as varchar(1)
	,@stockTag as bit
	,@stock as varchar(10)
	,@stockOp as varchar(1)
	,@pctTag as bit
	,@pct as varchar(10)
	,@pctOp as varchar(1)
	,@margemTag as bit
	,@margemOp as varchar(1)
	,@margem varchar(10)
	,@DiasSemMovimentos as numeric(9,2)
	,@Familia1_IvaDif6 as bit
	,@site as varchar(55)
	,@pvpDifPvpInfarmed as bit
	,@pclTag as bit
	,@pcl as varchar(10)
	,@pclOp as varchar(2)
/* WITH ENCRYPTION */
AS
BEGIN
declare @sql as varchar(max), @site_nr as tinyint
set @site_nr = (select no from empresa where site = @site)
set @sql = N'
; with cteMovimentos as (
	Select
		ref, max(datalc) as DataUltimoMovimento
	from
		sl (nolock)
	Where
		 armazem in (select armazem from empresa inner join empresa_arm on empresa.no = empresa_arm.empresa_no where empresa.site =''' + @site+''') 
	group by
		ref
)
Select
	st.ref
	,st.design
	,pvp = st.epv1
	,pct = st.epcusto
	,stock 
	,DataUltimoMovimento = isnull(DataUltimoMovimento,''19000101'')
	,faminome
	,taxa = isnull(taxasiva.taxa,0)
	,st.epv1
	,pvporig = isnull(fprod.pvporig,0)
	,dataaltpreco = (SELECT TOP 1 data FROM hist_precos(nolock) WHERE id_preco=1 and hist_precos.ref = st.ref order by data desc)
	,st.marg3
	,pcl = st.epcult
From
	st (nolock)
	left join taxasiva (nolock) on taxasiva.codigo = st.tabiva
	left join cteMovimentos on st.ref = cteMovimentos.ref
	left join fprod (nolock) on st.ref = fprod.cnp
Where
	st.site_nr = ''' + RTRIM(LTRIM(STR(@site_nr))) + '''
	and st.ref != ''''
	and st.stns = 0
'
 --Adicionar Condições
IF @ref != ''
	select @sql = @sql + 'and (st.ref in (Select items from dbo.up_splitToTable(''' + @ref+''','','')) or ''' + @ref+''' = '''') '
IF @pvpTag = 1
	select @sql = @sql + ' and epv1 ' + @pvpOp + ' ' + @pvp
IF @stockTag = 1
	Select @sql = @sql + ' and (select SUM(stock) from sa where sa.ref = st.ref and armazem in (select armazem from empresa inner join empresa_arm on empresa.no = empresa_arm.empresa_no where empresa.site =''' + @site+''')) ' + @stockOp + ' ' + @stock  
IF @pclTag = 1
	Select @sql = @sql + ' and epcult ' + @pclOp + ' ' + @pcl
IF @pctTag = 1
	Select @sql = @sql + ' and epcusto ' + @pctOp + ' ' + @pct
IF @activos != -1 AND @activos!=''
	select @sql = @sql + 'and st.inactivo ='+ @activos
IF @DiasSemMovimentos != 0
	select @sql = @sql + ' and (st.ref not in (Select ref from sl (nolock) where armazem in (select armazem from empresa inner join empresa_arm on empresa.no = empresa_arm.empresa_no where empresa.site =''' + @site+''' ) and datalc > DATEADD(dd,-' + convert(varchar(100),@DiasSemMovimentos) + ',GETDATE())))'
IF @Familia1_IvaDif6 != 0
	select @sql = @sql + ' and (familia = 1 and tabiva != 1)'
IF @pvpDifPvpInfarmed != 0
	select @sql = @sql + ' and (st.epv1 != fprod.pvporig and fprod.pvporig is not null and fprod.pvporig != 0)'
IF @margemTag = 1
	Select @sql = @sql + ' and marg3 ' + @margemOp + ' ' + @margem

--select @sql = @sql + ' order by st.ref'
print @sql
execute (@sql)
END

GO
Grant Execute on dbo.up_relatorio_irregularidadesSt to Public
Grant control on dbo.up_relatorio_irregularidadesSt to Public
Go
