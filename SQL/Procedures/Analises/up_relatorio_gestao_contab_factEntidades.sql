/* Relatório Faturacao Entidades

	exec up_relatorio_gestao_contab_factEntidades '20220304','20220304','Loja 1',0,1,0
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_contab_factEntidades]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_contab_factEntidades
go

create procedure dbo.up_relatorio_gestao_contab_factEntidades
@dataIni as datetime,
@dataFim as datetime,
@site	 varchar(60),
@naoInclIva as bit = 0,
@soEntidades bit = 0,
@excluiEntidades bit = 0
/* with encryption */
AS
SET NOCOUNT ON
	SELECT
		EMPRESA		= empresa.EMPRESA
		,LOJA		= empresa.site
		,NUMLOJA	= empresa.ref
		,FDATA		as DATADOC
		,FT.NOME	as NOMECLI
		,FT.NO		as NUMCLI
		,FT.NMDOC	as DOCUMENTO
		,FT.FNO		as NUMDOC
		,FI.REF
		,DESIGN
		,QTT
		,UNIDADE
		,fi.TABIVA
		,IVA
		,DESCONTO
		,DESC2
		,VALOR	= CASE WHEN @naoInclIva = 1 
			then 
				-- alterado para colocar os valores das notas de crédito na negativo
				-- CASE WHEN td.tipodoc = 3 then (case when fi.ivaincl = 1 then (fi.etiliquido / (fi.iva/100+1)) else fi.etiliquido end)*-1 else (case when fi.ivaincl = 1 then (fi.etiliquido / (fi.iva/100+1)) else fi.etiliquido end) end
				case when fi.ivaincl = 1 then (fi.etiliquido / (fi.iva/100+1)) else fi.etiliquido end
			else
				-- CASE WHEN td.tipodoc = 3 then (case when fi.ivaincl = 1 then fi.etiliquido else fi.etiliquido + (fi.etiliquido*(fi.iva/100)) end)*-1 else (case when fi.ivaincl = 1 then fi.etiliquido else fi.etiliquido + (fi.etiliquido*(fi.iva/100)) end) end
				case when fi.ivaincl = 1 then fi.etiliquido else fi.etiliquido + (fi.etiliquido*(fi.iva/100)) end
			end
	FROM	
		FT (nolock) 
		INNER JOIN FI (nolock) ON FI.FTSTAMP = FT.FTSTAMP
		INNER JOIN TD (nolock) ON FT.NDOC = TD.NDOC
		Left join empresa_arm on empresa_arm.armazem = FI.armazem
		Left join empresa on empresa.no = empresa_arm.empresa_no
	WHERE	
		fdata between @dataIni and @dataFim
		and anulado = 0 
		and td.tiposaft!=''
		and ((fi.qtt <> 0 or fi.etiliquido <> 0) OR  (ft.anulado = 1 ))
		and ft.site = (case when @site = '' Then ft.site else @site end)
		and ft.no >= case when @excluiEntidades = 1 then 198 else 0 end 
		and ft.no <= case when @soEntidades = 1 then 198 else 999999999 end
		and td.tipodoc!=4
	ORDER BY 
		FDATA

GO
Grant Execute on dbo.up_relatorio_gestao_contab_factEntidades to Public
GO
Grant Control on dbo.up_relatorio_gestao_contab_factEntidades to Public
Go