/*
	exec up_relatorio_HonorariosEmitidos '19000101','20190601','','','',''
*/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_relatorio_HonorariosEmitidos]') IS NOT NULL
    drop procedure up_relatorio_HonorariosEmitidos
go

create PROCEDURE up_relatorio_HonorariosEmitidos
@dataIni	datetime
,@dataFim	datetime
,@especialista varchar(30)
,@utente	varchar(55)
,@entidade	varchar(55)
,@ref	varchar(18)


/* WITH ENCRYPTION */
AS

	select 
		Especialista = Convert(varchar(254),RTRIM(LTRIM(B_seriesRecurso.nome)) + ' [' + CONVERT(varchar,B_seriesRecurso.no) + ']')
		,EspecialistaNome = B_seriesRecurso.nome
		,EspecialistaNo = B_seriesRecurso.no
		,Entidade = isnull(B_utentes.nome,'')
		,EntidadeNo = isnull(B_utentes.no,0)
		,EntidadeEstab = isnull(B_utentes.estab,0)
		,marcacoes.mrno
		,dataInicio = marcacoesServ.dataInicio
		,dataFim = marcacoesServ.dataFim
		,marcacoesServ.hinicio
		,marcacoesServ.hfim
		,marcacoes.nome
		,marcacoes.no
		,marcacoes.estab
		,marcacoesServ.ref
		,marcacoesServ.design
		,marcacoesServ.pvp
		,marcacoesServ.qtt
		,marcacoesServ.total
		,totalLinha = marcacoesServ.pvp * marcacoesServ.qtt
		,tipoCompart = isnull(cp.tipoCompart,'')
		,compart = isnull(cp.compart,0)
		,maximo = isnull(cp.maximo,0)
		,marcacoes.obs
		,valorUtente = marcacoesServ.total
		,valorEntidade = marcacoesServ.compart
		,valorHonorario
	from 
		marcacoes
		inner join marcacoesServ on marcacoes.mrstamp = marcacoesServ.mrstamp
		inner join b_series on marcacoes.serieno = b_series.serieno
		left join B_seriesRecurso on B_seriesRecurso.seriestamp = b_series.seriestamp and B_seriesRecurso.tipo = 'Utilizadores'	and marcacoesServ.ref = B_seriesRecurso.ref
		left join marcacoesComp on marcacoesServ.servmrstamp = marcacoesCOmp.servmrstamp
		left join cp on cp.id = marcacoesComp.idcp
		left join B_utentes on cp.utstamp = B_utentes.utstamp
	Where
		marcacoesServ.dataInicio between @dataIni and @dataFim
		and isnull(B_seriesRecurso.no,0) != 0	
		and (isnull(B_seriesRecurso.nome,'') =  @especialista or @especialista = '')
		and (isnull(B_seriesRecurso.nome,'') =  @especialista or @especialista = '')
		and (marcacoes.nome = @utente or @utente = '')
		and (B_utentes.nome = @entidade or @entidade = '')
		and (marcacoesServ.ref = @ref or @ref = '')
		and marcacoesServ.honorario = 1

	
GO
Grant Execute On up_relatorio_HonorariosEmitidos to Public
Grant Control On up_relatorio_HonorariosEmitidos to Public
GO