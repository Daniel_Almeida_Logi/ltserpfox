
/*
Alterado a 2020-08-07 por JG: inclus�o de input site, permitindo multiselecao

exec up_relatorio_clinica_listagemUtilizadores '', '', '','Loja 1'
exec up_relatorio_clinica_listagemUtilizadores '', '', '','Loja 2'
exec up_relatorio_clinica_listagemUtilizadores '', '', '','Loja 1,Loja 2'


*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_relatorio_clinica_listagemUtilizadores]') IS NOT NULL
    drop procedure up_relatorio_clinica_listagemUtilizadores
go

create PROCEDURE up_relatorio_clinica_listagemUtilizadores
@nome as varchar(20) = '',
@grupo as varchar(20) = '',
@especialidade varchar(50) = '',
@site varchar(254)= ''

/* WITH ENCRYPTION */
AS

	if @site = ''
		set @site = isnull((select  convert(varchar(254),(select 
							site + ', '
						from 
							empresa (nolock)							
						FOR XML PATH(''))) as no),0)

	Select
		b_us.nome
		,no = b_us.userno
		,especialidade = isnull(b_usesp.especialidade,'')
		,grupo
		,sitedefault = b_us.loja
	From
		b_us
		left join b_usesp on b_usesp.userno = b_us.userno
	Where
		nome like @nome +'%'
		and grupo = case when @grupo = '' then grupo else @grupo end
		--and b_us.loja = case when @site = '' then b_us.loja else @site end
		and b_us.loja  in (Select items from dbo.up_splitToTable(@site, ',')) 
		and (b_usesp.especialidade = @especialidade	OR @especialidade = '')
	order by 
		nome
		
GO
Grant Execute On dbo.up_relatorio_clinica_listagemUtilizadores to Public
Grant Control On dbo.up_relatorio_clinica_listagemUtilizadores to Public
GO
