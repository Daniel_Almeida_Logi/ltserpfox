/* Relat�rio Evolucao Vendas 
	exec up_relatorio_vendas_evolucao '20190701', '20210617','Loja 1'
	exec up_relatorio_vendas_evolucao '20190701', '20210617','Loja 2'
	exec up_relatorio_vendas_evolucao '20211001', '20211031','Loja 1'
	exec up_relatorio_vendas_evolucao '20221001', '20221031', 'Loja 1'
*/

if OBJECT_ID('[dbo].[up_relatorio_vendas_evolucao]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_evolucao
go

create procedure [dbo].[up_relatorio_vendas_evolucao]
	 @dataIni		datetime
	,@dataFim		datetime
	,@site			varchar(55)


/* with encryption */

AS
SET NOCOUNT ON

	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseHomo'))
		DROP TABLE #dadosVendasBaseHomo
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseCalculos'))
		DROP TABLE #dadosVendasBaseCalculos
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseHomoCalculos'))
		DROP TABLE #dadosVendasBaseHomoCalculos
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendaCalculo'))
		DROP TABLE #dadosVendaCalculo
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendaIvas'))
		DROP TABLE #dadosVendaIvas
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendaIvasHomo'))
		DROP TABLE #dadosVendaIvasHomo

	DECLARE @totalValorStock		NUMERIC(15,4)
	DECLARE @totalValorStockHomo	NUMERIC(15,4)
	DECLARE @dataIniHomo			DATETIME
	DECLARE @dataFimHomo			DATETIME


	SET @dataIniHomo = DATEADD(YEAR,-1,@dataIni)
	set @dataFimHomo = DATEADD(YEAR,-1,@dataFim)


	declare @tipomoeda varchar(3)
	declare @perc varchar(1)
	select top 1 @tipomoeda = textValue from B_Parameters_site where   stamp='ADM0000000004'
	set @perc ='%'

		/* Calc Loja e Armazens */
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)

	select 
		armazem
	into
		#Armazens
	From
		empresa (nolock)
		inner join empresa_arm (nolock) on empresa.no = empresa_arm.empresa_no
	where
		empresa.site = case when @site = '' then empresa.site else @site end		

	
		SELECT
			sl.ref ,
			SUM(CASE WHEN sl.datalc <= @dataFim  THEN (CASE WHEN CM < 50 THEN QTT ELSE -QTT END ) ELSE 0 END) AS STOCK_DATA,
			SUM(CASE WHEN sl.datalc <= @dataFimHomo  THEN (CASE WHEN CM < 50 THEN QTT ELSE -QTT END ) ELSE 0 END) AS STOCK_DATAHOMO,
			sl.armazem
		INTO
			#dadosStockData
		FROM	
			 st (nolock) 
			 LEFT join sl (nolock)  ON  st.REF = SL.ref AND st.site_nr= @site_nr
		WHERE	
			sl.ref = st.ref
			and sl.datalc <= @dataFim 
			and sl.armazem in (select armazem from #Armazens)
		GROUP BY sl.ref ,sl.armazem


	/* Calc PCL � data */
	SELECT 
		sl.ref
		,pcl = Round(evu,2)
		,sl.armazem
		,id = ROW_NUMBER() OVER(PARTITION BY sl.ref ORDER BY sl.datalc + sl.ousrhora DESC)
	INTO 
		#dadosEvuData	
	FROM 
		sl (nolock)
		inner join #Armazens (nolock) on #Armazens.armazem = sl.armazem 
	WHERE 
		[sl].[datalc] <= @dataFim
		AND [EVU] != 0
		AND [origem] IN ('FO','IF')


	/* Calc PCL � data */
	SELECT 
		sl.ref
		,pcl = Round(evu,2)
		,sl.armazem
		,id = ROW_NUMBER() OVER(PARTITION BY sl.ref ORDER BY sl.datalc + sl.ousrhora DESC)
	INTO 
		#dadosEvuDataHomo	
	FROM 
		sl (nolock)
		inner join #Armazens (nolock) on #Armazens.armazem = sl.armazem 
	WHERE 
		[sl].[datalc] <= @dataFimHomo
		AND [EVU] != 0
		AND [origem] IN ('FO','IF')
		
	/* Preparar Result Set */
	Select
		@totalValorStock =SUM(ROUND(ISNULL(#dadosStockData.STOCK_DATA * ISNULL(#dadosEvuData.[pcl],[st].[EPCULT]),0),2))
	From
		#dadosStockData
		LEFT JOIN #dadosEvuData on #dadosStockData.ref = #dadosEvuData.ref AND #dadosStockData.armazem = #dadosEvuData.armazem
		INNER JOIN ST  ON #dadosStockData.ref = ST.ref  AND st.site_nr= @site_nr
	Where
		#dadosEvuData.id = 1 or #dadosEvuData.id IS NULL 
	

	Select
		@totalValorStockHomo =SUM(ROUND(ISNULL(#dadosStockData.STOCK_DATAHOMO * ISNULL(#dadosEvuDataHomo.[pcl],[st].[EPCULT]),0),2))
	From
		#dadosStockData
		LEFT JOIN #dadosEvuDataHomo on #dadosStockData.ref = #dadosEvuDataHomo.ref AND #dadosStockData.armazem = #dadosEvuDataHomo.armazem
		INNER JOIN ST  ON #dadosStockData.ref = ST.ref  AND st.site_nr= @site_nr
	Where
		#dadosEvuDataHomo.id = 1 or #dadosEvuDataHomo.id IS NULL 

print @totalValorStock
print @totalValorStockHomo

	/* Dados Base Vendas  */
	create table #dadosVendasBase (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)
	)

	create table #dadosVendasBaseHomo (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)
	)


		create table #dadosVendaCalculo (
			 Familia varchar(100)
			,Rubrica varchar(100)
			,Periodo numeric(19,4)
			,Homologo numeric(19,4)
			,Evol numeric(19,4)
		)

	declare @Entidades as bit
	set @Entidades = case when (select  tipoempresa from empresa where site = @site) in ('FARMACIA', 'PARAFARMACIA') then convert(bit,1) else convert(bit,0) end

    insert #dadosVendasBase
    exec up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site

    insert #dadosVendasBaseHomo
    exec up_relatorio_vendas_base_detalhe @dataIniHomo, @dataFimHomo, @site

	If @Entidades = 1
	begin
		delete from #dadosVendasBase where #dadosVendasBase.no < 199
		delete from #dadosVendasBaseHomo where #dadosVendasBaseHomo.no < 199
	end

	
	SELECT 
		ISNULL(SUM(ISNULL(etiliquidoSiva + ettent1siva + ettent2siva,0)),0)                                                                                          AS Vendas,
	    ISNULL(SUM(ISNULL(etiliquidoSiva + ettent1siva + ettent2siva - #dadosVendasBase.epcpond,0)),0)																 AS MARGEM ,
		@totalValorStock																																			 AS StockValor,
		ISNULL(SUM(ISNULL(CASE WHEN (A.id = 1) THEN  (etiliquidoSiva + ettent1siva + ettent2siva)	ELSE 0 END ,0)),0) 												 AS MSRM,
		ISNULL(SUM(ISNULL(CASE WHEN (FPROD.generico=0 and (A.id = 1 or A.id = 2)) THEN  (etiliquidoSiva + ettent1siva + ettent2siva)	ELSE 0 END,0) ),0)			 AS MM,
		ISNULL(SUM(ISNULL(CASE WHEN (FPROD.generico=1) THEN  (etiliquidoSiva + ettent1siva + ettent2siva)	ELSE 0 END ,0)),0) 										 AS MG,
		ISNULL(SUM(ISNULL(CASE WHEN (A.id = 2) THEN  (etiliquidoSiva + ettent1siva + ettent2siva)	ELSE 0 END ,0)),0) 												 AS MNSRM,						
		ISNULL(SUM(ISNULL(CASE WHEN (A.id = 3) THEN  (etiliquidoSiva + ettent1siva + ettent2siva)	ELSE 0 END ,0)),0)  											 AS PSBE				
	into #dadosVendasBaseCalculos
	FROM #dadosVendasBase
		left join st	(nolock) ON st.ref= #dadosVendasBase.REF 
		left join FPROD (nolock) ON fprod.cnp=#dadosVendasBase.ref
		left join grande_mercado_hmr (nolock) A on A.id = st.u_depstamp

	SELECT 
		ISNULL(SUM(ISNULL(etiliquidoSiva + ettent1siva + ettent2siva,0)),0)                                                                                          AS VendasHomo,
		ISNULL(SUM(ISNULL(etiliquidoSiva + ettent1siva + ettent2siva - #dadosVendasBaseHomo.epcpond,0)),0)															 AS MARGEMHomo ,
		@totalValorStockHomo																																		 AS StockValorHomo,
		ISNULL(SUM(ISNULL(CASE WHEN (A.id = 1) THEN  (etiliquidoSiva + ettent1siva + ettent2siva)	ELSE 0 END ,0)),0)												 AS MSRMHomo,
		ISNULL(SUM(ISNULL(CASE WHEN (FPROD.generico=0 and (A.id = 1 or A.id = 2)) THEN  (etiliquidoSiva + ettent1siva + ettent2siva)	ELSE 0 END,0) ),0) 			 AS MMHomo,
		ISNULL(SUM(ISNULL(CASE WHEN (FPROD.generico=1) THEN  (etiliquidoSiva + ettent1siva + ettent2siva)	ELSE 0 END ,0)),0) 										 AS MGHomo,
		ISNULL(SUM(ISNULL(CASE WHEN (A.id = 2) THEN  (etiliquidoSiva + ettent1siva + ettent2siva)	ELSE 0 END ,0)),0) 												 AS MNSRMHomo,						
		ISNULL(SUM(ISNULL(CASE WHEN (A.id = 3) THEN  (etiliquidoSiva + ettent1siva + ettent2siva)	ELSE 0 END ,0)),0) 												 AS PSBEHomo		
	into #dadosVendasBaseHomoCalculos
	FROM  #dadosVendasBaseHomo 
		left join st	(nolock) ON st.ref= #dadosVendasBaseHomo.REF 
		left join FPROD (nolock) ON fprod.cnp=#dadosVendasBaseHomo.ref
		left join grande_mercado_hmr (nolock) A on A.id = st.u_depstamp

	select 
		iva,
		SUM(ISNULL(etiliquidoSiva + ettent1siva + ettent2siva,0)) AS VALOR 
	INTO #dadosVendaIvas
	FROM #dadosVendasBase	 
		GROUP BY iva
	
	select 
		iva,
		SUM(ISNULL(etiliquidoSiva + ettent1siva + ettent2siva,0)) AS VALOR 
	INTO #dadosVendaIvasHomo
	FROM #dadosVendasBaseHOMO
		GROUP BY iva

		-- Valores
	INSERT INTO #dadosVendaCalculo (Familia,Rubrica,Periodo,Homologo,Evol) 
		SELECT 'Venda' , 'Valor' , #dadosVendasBaseCalculos.Vendas, #dadosVendasBaseHomoCalculos.vendasHomo , 
				(CASE WHEN #dadosVendasBaseHomoCalculos.vendasHomo!= 0 
					  THEN  (#dadosVendasBaseCalculos.Vendas/ #dadosVendasBaseHomoCalculos.vendasHomo -1)*100  
					  ELSE #dadosVendasBaseCalculos.Vendas END)	
		FROM  #dadosVendasBaseCalculos , #dadosVendasBaseHomoCalculos  UNION ALL
		SELECT 'Venda' ,'Margem' ,  #dadosVendasBaseCalculos.MARGEM , #dadosVendasBaseHomoCalculos.MARGEMHomo,	
				(CASE WHEN #dadosVendasBaseHomoCalculos.MARGEMHomo!= 0 
					  THEN  (#dadosVendasBaseCalculos.MARGEM/ #dadosVendasBaseHomoCalculos.MARGEMHomo -1)*100  
					  ELSE #dadosVendasBaseCalculos.MARGEM END)
		FROM  #dadosVendasBaseCalculos , #dadosVendasBaseHomoCalculos  UNION ALL
		SELECT 'Stock' ,'Valor',#dadosVendasBaseCalculos.StockValor,#dadosVendasBaseHomoCalculos.StockValorHomo,
				(CASE WHEN #dadosVendasBaseHomoCalculos.StockValorHomo!= 0 
					  THEN  (#dadosVendasBaseCalculos.StockValor/ #dadosVendasBaseHomoCalculos.StockValorHomo -1)*100  
					  ELSE #dadosVendasBaseCalculos.StockValor END)	
		FROM  #dadosVendasBaseCalculos , #dadosVendasBaseHomoCalculos  UNION ALL
		SELECT 'Stock' ,'Peso Stock',  
				(CASE WHEN #dadosVendasBaseCalculos.Vendas!= 0 
					 THEN  (#dadosVendasBaseCalculos.StockValor/ #dadosVendasBaseCalculos.Vendas)*100  
					 ELSE #dadosVendasBaseCalculos.StockValor END),
					 (CASE WHEN #dadosVendasBaseHomoCalculos.VendasHomo!= 0 
						 THEN  (#dadosVendasBaseHomoCalculos.StockValorHomo/ #dadosVendasBaseHomoCalculos.VendasHomo)*100  
						 ELSE #dadosVendasBaseHomoCalculos.StockValorHomo END),0	
		FROM  #dadosVendasBaseCalculos , #dadosVendasBaseHomoCalculos  UNION ALL
		SELECT 'MSRM' ,'Valor',	#dadosVendasBaseCalculos.MSRM,#dadosVendasBaseHomoCalculos.MSRMHomo,
				(CASE WHEN #dadosVendasBaseHomoCalculos.MSRMHomo!= 0 
					  THEN (#dadosVendasBaseCalculos.MSRM/ #dadosVendasBaseHomoCalculos.MSRMHomo - 1)*100	
					  else #dadosVendasBaseCalculos.MSRM END)
		FROM  #dadosVendasBaseCalculos , #dadosVendasBaseHomoCalculos  UNION ALL
		SELECT 'MSRM' ,'Peso Stock',
				(CASE WHEN #dadosVendasBaseCalculos.StockValor!= 0 
					  THEN  (#dadosVendasBaseCalculos.MSRM/ #dadosVendasBaseCalculos.StockValor)*100  
					  ELSE #dadosVendasBaseCalculos.MSRM END),
					  (CASE WHEN #dadosVendasBaseHomoCalculos.StockValorHomo!= 0 
							THEN  (#dadosVendasBaseHomoCalculos.MSRMHomo/ #dadosVendasBaseHomoCalculos.StockValorHomo)*100  
							ELSE #dadosVendasBaseHomoCalculos.MSRMHomo END),0				
		FROM  #dadosVendasBaseCalculos , #dadosVendasBaseHomoCalculos  UNION ALL
		SELECT 'MM' ,'Valor',#dadosVendasBaseCalculos.MM,#dadosVendasBaseHomoCalculos.MMHomo,
				(CASE WHEN #dadosVendasBaseHomoCalculos.MMHomo!= 0  
					  THEN (#dadosVendasBaseCalculos.MM/ #dadosVendasBaseHomoCalculos.MMHomo - 1)*100  
					  ELSE  #dadosVendasBaseCalculos.MM END)
		FROM  #dadosVendasBaseCalculos , #dadosVendasBaseHomoCalculos  UNION ALL
		SELECT 'MM' ,'Peso Stock',
				(CASE WHEN #dadosVendasBaseCalculos.StockValor!= 0 
					  THEN  (#dadosVendasBaseCalculos.MM/ #dadosVendasBaseCalculos.StockValor)*100  
					  ELSE #dadosVendasBaseCalculos.MM END),
					  (CASE WHEN #dadosVendasBaseHomoCalculos.StockValorHomo!= 0 
							THEN  (#dadosVendasBaseHomoCalculos.MMHomo/ #dadosVendasBaseHomoCalculos.StockValorHomo)*100 
							ELSE #dadosVendasBaseHomoCalculos.MMHomo END),0						
		FROM  #dadosVendasBaseCalculos , #dadosVendasBaseHomoCalculos  UNION ALL
		SELECT 'MG' ,'Valor',#dadosVendasBaseCalculos.MG,#dadosVendasBaseHomoCalculos.MGHomo,
				(CASE WHEN #dadosVendasBaseHomoCalculos.MGHomo!= 0  
					  THEN (#dadosVendasBaseCalculos.MG/ #dadosVendasBaseHomoCalculos.MGHomo - 1)*100   
					  ELSE #dadosVendasBaseCalculos.MG  END)
		FROM  #dadosVendasBaseCalculos , #dadosVendasBaseHomoCalculos  UNION ALL
		SELECT 'MG' ,'Peso Stock',
				(CASE WHEN #dadosVendasBaseCalculos.StockValor!= 0 
					  THEN  (#dadosVendasBaseCalculos.MG/ #dadosVendasBaseCalculos.StockValor)*100  
					  ELSE #dadosVendasBaseCalculos.MG END),
					  (CASE WHEN #dadosVendasBaseHomoCalculos.StockValorHomo!= 0 
							THEN  (#dadosVendasBaseHomoCalculos.MGHomo/ #dadosVendasBaseHomoCalculos.StockValorHomo)*100  
							ELSE #dadosVendasBaseHomoCalculos.MGHomo END),0						
		FROM  #dadosVendasBaseCalculos , #dadosVendasBaseHomoCalculos  UNION ALL
		SELECT 'MNSRM' ,'Valor',#dadosVendasBaseCalculos.MNSRM,#dadosVendasBaseHomoCalculos.MNSRMHomo,
				(CASE WHEN #dadosVendasBaseHomoCalculos.MNSRMHomo!= 0  
					  THEN (#dadosVendasBaseCalculos.MNSRM/ #dadosVendasBaseHomoCalculos.MNSRMHomo - 1)*100
					  ELSE #dadosVendasBaseCalculos.MNSRM   END)
		FROM  #dadosVendasBaseCalculos , #dadosVendasBaseHomoCalculos  UNION ALL
		SELECT 'MNSRM' ,'Peso Stock',
				(CASE WHEN #dadosVendasBaseCalculos.StockValor!= 0 
					  THEN  (#dadosVendasBaseCalculos.MNSRM/ #dadosVendasBaseCalculos.StockValor)*100  
					  ELSE #dadosVendasBaseCalculos.MNSRM END),
					  (CASE WHEN #dadosVendasBaseHomoCalculos.StockValorHomo!= 0 
							THEN  (#dadosVendasBaseHomoCalculos.MNSRMHomo/ #dadosVendasBaseHomoCalculos.StockValorHomo)*100  
							ELSE #dadosVendasBaseHomoCalculos.MNSRMHomo END),0		
		FROM  #dadosVendasBaseCalculos , #dadosVendasBaseHomoCalculos  UNION ALL
		SELECT 'PSBE' ,'Valor',#dadosVendasBaseCalculos.PSBE,#dadosVendasBaseHomoCalculos.PSBEHomo,
				(CASE WHEN #dadosVendasBaseHomoCalculos.PSBEHomo!= 0  
					  THEN (#dadosVendasBaseCalculos.PSBE/ #dadosVendasBaseHomoCalculos.PSBEHomo - 1)*100   
					  ELSE  #dadosVendasBaseCalculos.PSBE	END)
		FROM  #dadosVendasBaseCalculos , #dadosVendasBaseHomoCalculos  UNION ALL
		SELECT 'PSBE' ,'Peso Stock',
				(CASE WHEN #dadosVendasBaseCalculos.StockValor!= 0 
					  THEN  (#dadosVendasBaseCalculos.PSBE/ #dadosVendasBaseCalculos.StockValor)*100  
					  ELSE #dadosVendasBaseCalculos.PSBE END),
					  (CASE WHEN #dadosVendasBaseHomoCalculos.StockValorHomo!= 0 
							THEN  (#dadosVendasBaseHomoCalculos.PSBEHomo/ #dadosVendasBaseHomoCalculos.StockValorHomo)*100  
							ELSE #dadosVendasBaseHomoCalculos.PSBEHomo END),0				
		FROM  #dadosVendasBaseCalculos , #dadosVendasBaseHomoCalculos 

		-- ivas 
	INSERT INTO #dadosVendaCalculo (Familia,Rubrica,Periodo,Homologo,Evol) 		
		SELECT CONVERT(VARCHAR(5),ISNULL(#dadosVendaIvas.iva,#dadosVendaIvasHomo.iva)) ,'VALOR', ISNULL(#dadosVendaIvas.VALOR,0),
			 ISNULL(#dadosVendaIvasHomo.VALOR,0),
			 (CASE WHEN ISNULL(#dadosVendaIvasHomo.VALOR,0)!= 0 
				   THEN  (ISNULL(#dadosVendaIvas.VALOR,0)/ #dadosVendaIvasHomo.VALOR - 1)*100  
				   ELSE ISNULL(#dadosVendaIvas.VALOR,0) END)
		FROM 
			#dadosVendaIvas 
			FULL OUTER join #dadosVendaIvasHomo on  #dadosVendaIvas.iva = #dadosVendaIvasHomo.iva 
		order by  
			ISNULL(#dadosVendaIvas.iva,#dadosVendaIvasHomo.iva)
	
	
		UPDATE #dadosVendaCalculo set Evol = (CASE WHEN #dadosVendaCalculo.Homologo!= 0 
												   THEN  (#dadosVendaCalculo.Periodo/ #dadosVendaCalculo.Homologo -1)*100  
												   ELSE #dadosVendaCalculo.Periodo END) WHERE Rubrica ='Peso Stock' 


		SELECT  
			FAMILIA, 
			rubrica,
			CASE WHEN Rubrica ='Peso Stock' THEN CONVERT(VARCHAR(30),cast(round(Periodo,2) as NUMERIC(20,2))) +' '+ '%' ELSE CONVERT(VARCHAR(30),cast(round(Periodo,2) as NUMERIC(20,2))) +' '+ @tipomoeda END  AS Periodo,
			CASE WHEN Rubrica ='Peso Stock' THEN CONVERT(VARCHAR(30),cast(round(Homologo,2) as NUMERIC(20,2))) +' '+ '%' ELSE CONVERT(VARCHAR(30),cast(round(Homologo,2) as NUMERIC(20,2))) +' '+ @tipomoeda END  AS Homologo,
			Evol
		FROM 
			#dadosVendaCalculo


	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadorSuma'))
		DROP TABLE #dadosOperadorSuma

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseHomo'))
		DROP TABLE #dadosVendasBaseHomo

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendaCalculo'))
		DROP TABLE #dadosVendaCalculo

GO
Grant Execute on dbo.up_relatorio_vendas_evolucao to Public
Grant control on dbo.up_relatorio_vendas_evolucao to Public
GO