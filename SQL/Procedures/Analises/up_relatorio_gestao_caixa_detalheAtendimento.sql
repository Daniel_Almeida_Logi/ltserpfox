

-- Relatório 
-- exec up_relatorio_gestao_caixa_detalheAtendimento '20230401','20230417','','','Loja 1'
-- 
-- OBS:

SET ANSI_NULLS OFF

GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_caixa_detalheAtendimento]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_caixa_detalheAtendimento
go

create procedure dbo.up_relatorio_gestao_caixa_detalheAtendimento
@dataIni		datetime
,@dataFim		datetime
,@op			varchar(max)
,@nrAtend		varchar(20)
,@site			varchar(55)



/* with encryption */

AS


	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores

	/* Operadores */
	Select
		iniciais
		,cm = userno
		,username
	into
		#dadosOperadores
	from
		b_us (nolock)
	Where
		userno in (select items from dbo.up_splitToTable(@op,','))
		or @op= '0'
		or @op= ''

	-- Testa se é por caixa ou por operador
	declare @cxOperador TINYINT = 0

	select  
		@cxOperador=gestao_cx_operador 
	from  
		empresa 
	where 
		site = @site

	

	if(@cxOperador=1)
		begin
			
			select 
				NOPR = vendedor
				,b_us.usrinis
				,DATA = convert(date,oData)
				,HORA = convert(varchar,oData,108)
				,NAT = nrAtend
				,NTM = terminal
				,VLAT = total
				,DINHEIRO = B_pagCentral.evdinheiro
				,MB = B_pagCentral.epaga2
				,VISA = B_pagCentral.epaga1
				,CHEQUE = B_pagCentral.echtotal
				,VALES = B_pagCentral.epaga3
				,epaga4 * ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'USD', B_pagCentral.oData),1)  as epaga4--USD
				,epaga5 * ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'EUR', B_pagCentral.oData),1)  as epaga5--EUR
				,epaga6
			from 
				B_pagCentral   (nolock)
				--inner join cx (nolock) on cx.cxstamp = B_pagCentral.cxstamp
				INNER JOIN ss  (nolock) ON ss.ssstamp = B_pagCentral.ssstamp
				left join b_us (nolock) ON B_pagCentral.vendedor = b_us.userno
			where
				ss.fechada = 1
				and ss.dabrir between @dataIni and @dataFim
				--and convert(date,B_pagCentral.oData) between @dataIni and @dataFim
				and B_pagCentral.vendedor in (select cm from #dadosOperadores)
				and (B_pagCentral.nrAtend = @nrAtend or @nrAtend = '')
				and B_pagCentral.site = @site
				and B_pagCentral.ignoraCaixa = 0

			Order by
					DATA,HORA,NAT	
		end
	 else 
		begin
		
			select 
				NOPR = vendedor
				,b_us.usrinis
				,DATA = convert(date,oData)
				,HORA = convert(varchar,oData,108)
				,NAT = nrAtend
				,NTM = terminal
				,VLAT = total
				,DINHEIRO = B_pagCentral.evdinheiro
				,MB = B_pagCentral.epaga2
				,VISA = B_pagCentral.epaga1
				,CHEQUE = B_pagCentral.echtotal
				,VALES = B_pagCentral.epaga3
				,epaga4 * ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'USD', B_pagCentral.oData),1)  as epaga4--USD
				,epaga5 * ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'EUR', B_pagCentral.oData),1)  as epaga5--EUR
				,epaga6
			from 
				B_pagCentral	(nolock)
				INNER JOIN cx	(nolock) ON cx.cxstamp = B_pagCentral.cxstamp
				LEFT JOIN b_us	(nolock) ON B_pagCentral.vendedor = b_us.userno
			where
				cx.fechada = 1
				and cx.dabrir between @dataIni and @dataFim
				--and convert(date,B_pagCentral.oData) between @dataIni and @dataFim
				and B_pagCentral.vendedor in (select cm from #dadosOperadores)
				and (B_pagCentral.nrAtend = @nrAtend or @nrAtend = '')
				and B_pagCentral.site = @site
				and B_pagCentral.ignoraCaixa = 0
			Order by
				DATA,HORA,NAT
		end

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores
	
GO
Grant Execute on dbo.up_relatorio_gestao_caixa_detalheAtendimento to Public
Grant control on dbo.up_relatorio_gestao_caixa_detalheAtendimento to Public
GO

