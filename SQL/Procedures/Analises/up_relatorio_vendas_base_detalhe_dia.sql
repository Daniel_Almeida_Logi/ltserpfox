/* Relatório Base para Calculo de Vendas - A sua alteração tem implicações em vários relatórios 

	select * from hist_vendas where ref='5440987'
	exec up_relatorio_vendas_base_Detalhe_dia '20170101', '20171231','Loja 1'
	exec up_relatorio_vendas_base_detalhe_dia '20150101','20150201', 'F_Uniao', 1

*/

if OBJECT_ID('[dbo].[up_relatorio_vendas_base_Detalhe_dia]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_base_Detalhe_dia
go

create procedure [dbo].[up_relatorio_vendas_base_Detalhe_dia]
	@dataIni			datetime
	,@dataFim			datetime
	,@site				varchar(55)


/* with encryption */

AS
SET NOCOUNT ON

	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosNumAtend'))
		DROP TABLE #dadosNumAtend

	/* Id do site */
	declare @site_nr varchar(20)
	set @site_nr = isnull((select no from empresa (nolock) where site=@site),0)

	/*  Usado no calculo de numero de atendimentos; Usado para validar existencia de Vales ao atendimento */
	Select distinct
		ft.u_nratend
	into
		#dadosNumAtend
	From
		fi (nolock)
		inner join ft (nolock) on fi.ftstamp = ft.ftstamp
	where
		fi.epromo = 1
		and fdata between @dataIni and @dataFim

	begin

		SELECT
			ft.fdata
			,ft.no
			,ft.estab
			,b_utentes.Tipo
			,ft.ndoc
			,ft.fno
			,td.tipodoc
			,td.u_tipodoc
			,ref			= case when fi.ref='' then fi.oref else fi.ref end
			,fi.design
			,fi.familia
			,u_epvp			= u_epvp
			,epcpond		= (CASE 
								WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt 
								ELSE FI.EPCP * qtt 
								END)
								*
								(CASE WHEN TD.tipodoc = 3 THEN (-1) else 1 end)
			,fi.iva
			,qtt			= CASE WHEN TD.tipodoc = 3 THEN -qtt ELSE qtt END
			,etiliquido		= case when fi.ivaincl = 1 then fi.etiliquido else fi.etiliquido + (fi.etiliquido*(fi.iva/100)) end
			,etiliquidoSIva = case when fi.ivaincl = 1 then (fi.etiliquido / (fi.iva/100+1)) else fi.etiliquido end
			,ettent1		= case when ft.tipodoc = 3 then u_ettent1*-1 else u_ettent1 end
			,ettent2		= case when ft.tipodoc = 3 then u_ettent2*-1 else u_ettent2 end
			,ettent1SIva	= case when ft.tipodoc = 3 then (u_ettent1/(fi.iva/100+1))*-1 else (u_ettent1/(fi.iva/100+1)) end
			,ettent2SIva	= case when ft.tipodoc = 3 then (u_ettent2/(fi.iva/100+1))*-1 else (u_ettent2/(fi.iva/100+1)) end
			,fi.desconto
			,descvalor		= ((case 
								when fi.desconto = 100 then fi.epv * fi.qtt 
								when (fi.desconto between 0.01 and 99.99) then (fi.epv * fi.qtt) - abs(etiliquido)
								else 0 
								end)
								+
								u_descval
								)
								*
								(CASE WHEN TD.tipodoc = 3 THEN (-1) else 1 end)
			-- Só descontos em valor e que correspondem a um vale
			,descvale		= case  
								when ft.u_nratend in (select u_nratend from #dadosNumAtend) THEN u_descval 
								ELSE 0 
								END
			,ousrhora		= Ft.ousrhora
			,ousrinis		= ft.ousrinis
			,vendnm			= ft.vendnm
			,ft.u_ltstamp
			,ft.u_ltstamp2
			,ecusto			= (fi.ecusto * qtt) * (CASE WHEN TD.tipodoc = 3 THEN (-1) else 1 end)
			,ft.site
			,site_nr = empresa.no
			, st.stock	
			, st.stmax
			, st.ptoenc
			, st.u_lab
		into 
			#dadosreport
		FROM
			FI				(nolock)
			INNER JOIN FT	(nolock) on FI.FTSTAMP = FT.FTSTAMP
			INNER JOIN TD	(nolock) on TD.NDOC = FT.NDOC
			INNER JOIN B_UTENTES (nolock) on ft.no = b_utentes.no and ft.estab = b_utentes.estab
			INNER join empresa (nolock) on empresa.site = ft.site
			inner join st on st.ref=fi.ref and st.site_nr=fi.armazem
		WHERE
			fi.composto = 0
			AND ft.anulado = 0
			AND (FT.tipodoc != 4 or u_tipodoc = 4)
			and ft.site = (case when @site = '' Then ft.site else @site end)
			AND fdata between @dataIni and @dataFim
			and fi.ref<>''

		select 
			ref
			, design
			, cast(stock as numeric(10,2)) as stock
			, cast(stmax as numeric(10,2)) as stmax
			, cast(ptoenc as numeric(10,2)) as ptoenc
			, u_lab
			, left(CONVERT(varchar, fdata, 121),10) as data
			, cast(sum(qtt) as numeric(10,0)) as qtt
			, cast(avg(etiliquidoSIva+ettent1SIva+ettent2SIva) as numeric(15,2)) as VDPV
			, cast(avg(ecusto) as numeric(15,2)) as VDPCL
			--,* 
		from 
			#dadosreport
		group by
			ref, design, fdata, stock, stmax, ptoenc, u_lab
		order by
			ref, fdata
	end 
	
	-- exec up_relatorio_vendas_base_Detalhe_dia '20170101', '20171231','Loja 1'
	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosNumAtend'))
		DROP TABLE #dadosNumAtend

GO
Grant Execute on dbo.up_relatorio_vendas_base_Detalhe_dia to Public
Grant control on dbo.up_relatorio_vendas_base_Detalhe_dia to Public
GO