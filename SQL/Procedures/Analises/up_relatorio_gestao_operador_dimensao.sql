/* Relatório operador dimensao
	 exec up_relatorio_gestao_operador_dimensao 
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_operador_dimensao]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_operador_dimensao
go

create procedure dbo.up_relatorio_gestao_operador_dimensao
@site	as varchar(55)
/* WITH ENCRYPTION */

AS
BEGIN

Select	
	'Utilizador' = username,
	'NumeroUtilizador' = userno,
	'Cod. Operador' = userno,
	'Nome'			= ISNULL(NOME,'')
from	
	b_US (nolock)
where
	inactivo = 0
					
END

GO
Grant Execute on dbo.up_relatorio_gestao_operador_dimensao to Public
Grant control on dbo.up_relatorio_gestao_operador_dimensao to Public
Go