/* Relatório Preçário de artigos

	exec up_relatorio_precario_seg 'Atlantico',0,'ENSAAT'
	exec up_relatorio_precario 'Atlantico',1

	
*/

if OBJECT_ID('[dbo].[up_relatorio_precario_seg]') IS NOT NULL
	drop procedure dbo.up_relatorio_precario_seg
go

create procedure [dbo].up_relatorio_precario_seg
	@site			varchar(55)
	,@comstock		bit
	,@seguradora	varchar(55)
	,@limart		varchar(30)

/* with encryption */

AS
SET NOCOUNT ON
	
	/* Calc Loja e Armazens */
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)

	/* Calc acréscimo da seguradora */
	declare @acrescimo as numeric(10,2)
			, @nomeseg as varchar(100)
	set @acrescimo = 1+ISNULL((select percadic from cptorg (nolock) where abrev=@seguradora),0)
	set @nomeseg = ISNULL((select nome from cptorg (nolock) where abrev=@seguradora),'')
	--print @acrescimo

	/* Calc Informação Produto */
	Select
		top (CAST(@limart as integer))
		st.ref
		, st.design
		, round((st.epv1 * st.qttembal)*@acrescimo,2) as prembal
		, round((st.epv1 * st.qttminvd)*@acrescimo,2) as prlamina
		, cast(qttminvd as numeric(15,0)) as qttminvd
		, taxasiva.taxa
		, @nomeseg as nome
	From
		st (nolock)
		inner join taxasiva (nolock) on taxasiva.codigo=st.tabiva
	Where
		site_nr = @site_nr
		and stock > (case when @comstock = 0 then -999 else 0 end)
	order by
		design

GO
Grant Execute on dbo.up_relatorio_precario_seg to Public
Grant control on dbo.up_relatorio_precario_seg to Public
GO