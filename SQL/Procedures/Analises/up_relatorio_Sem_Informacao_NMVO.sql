/* 

	exec up_relatorio_Sem_Informacao_NMVO '20240830','20280930','Loja 1','98'
	exec up_relatorio_Sem_Informacao_NMVO '19000101','20280908','Loja 1','56'

	exec up_relatorio_Sem_Informacao_NMVO '20250114','20250115','Loja 1',''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_Sem_Informacao_NMVO]') IS NOT NULL
	DROP PROCEDURE dbo.up_relatorio_Sem_Informacao_NMVO
GO

CREATE PROCEDURE dbo.up_relatorio_Sem_Informacao_NMVO
@dataIni		AS DATETIME = '19000101',
@dataFim		AS DATETIME = '19000101',
@site			AS VARCHAR(55) = '',
@op				AS VARCHAR(254) = ''
/* with encryption */
AS 
SET NOCOUNT ON
BEGIN 
	select
	inf_faltas.ousrdata,
	operador,
	fi.nmdoc,
	fi.fno as ndoc,
	fi.ref,
	fi.design,
	fi.qtt 
	from fi_trans_info_faltas inf_faltas (nolock)
	inner join fi  ON inf_faltas.fistamp = fi.fistamp
	where 
			inf_faltas.site = (CASE WHEN @site = '' THEN inf_faltas.site ELSE @site END)
			AND operadorno = (CASE WHEN @op = '' THEN operadorno ELSE @op END)
			AND inf_faltas.ousrdata >= CAST(@dataIni AS DATE)
			AND inf_faltas.ousrdata < DATEADD(DAY, 1, CAST(@dataFim AS DATE)) 
	order by inf_faltas.ousrdata asc
END
GO
Grant Execute on dbo.up_relatorio_Sem_Informacao_NMVO to Public
Grant control on dbo.up_relatorio_Sem_Informacao_NMVO to Public
GO