/*
Relátorio de Vendas Vacinas

exec up_relatorio_vendas_vacinas '','20220920','20221006','6770289,7021535'

*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



if OBJECT_ID('[dbo].[up_relatorio_vendas_vacinas]') IS NOT NULL
drop procedure dbo.up_relatorio_vendas_vacinas
go

create procedure up_relatorio_vendas_vacinas
@codInfa					VARCHAR (60) = '',
@dataIni					datetime = '',
@dataFim					datetime = '30000101',
@ref						VARCHAR(60)

/* WITH ENCRYPTION */
AS

  IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#prepareResult'))
        DROP TABLE #prepareResult

   IF(@dataIni < '20220920')
     begin
        SET @dataIni = '20220920'
     END
     
    select
         pharmacyCode                                                                                                    as code
         ,productCode                                                                                                    as ref
    into
        #prepareResult
    from
        [SQLLOGITOOLS].[LTSMSB].dbo.ext_compart_response  ext_compart_response (nolock)
        inner join [SQLLOGITOOLS].[LTSMSB].dbo.ext_compart_request ext_compart_request (nolock)
            on ext_compart_response.stampRequest = ext_compart_request.stamp
        inner join [SQLLOGITOOLS].[LTSMSB].dbo.ext_compart_response_l ext_compart_response_l(nolock)
            on ext_compart_response_l.stampResponse = ext_compart_response.stamp
    where
        ext_compart_request.id_ext_efr = '109'
        and ext_compart_request.reqType = 2 and anulationReqStamp='' and ext_compart_response.contributionCorrelationId!=''
        and ext_compart_request.ousrdata between @dataIni and @dataFim
        and ext_compart_request.test = 0
        and (isnull(productCode,'') in (Select items from dbo.up_splitToTable(@ref,',')) or @ref = '')
		and (isnull(pharmacyCode,'') in (Select items from dbo.up_splitToTable(@codInfa,',')) or @codInfa = '')

       SELECT
            COUNT(ref) as countVacinas,
            code,
            isnull((select TOP 1 nome from b_utentes(nolock) where ISNUMERIC(id)=1 and convert(int,id) = convert(int,code) order by ousrdata desc),'')    as nome,
			#prepareResult.ref as ref
        FROM #prepareResult
        group by code , #prepareResult.ref

   IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#prepareResult'))
        DROP TABLE #prepareResult

GO
Grant Execute On dbo.up_relatorio_vendas_vacinas to Public
Grant Control On dbo.up_relatorio_vendas_vacinas to Public
Go

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO