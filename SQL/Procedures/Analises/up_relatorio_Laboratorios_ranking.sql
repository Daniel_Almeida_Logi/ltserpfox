-- exec up_relatorio_Laboratorios_ranking '','','','Loja 1','',1
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_Laboratorios_ranking]') IS NOT NULL
	drop procedure dbo.up_relatorio_Laboratorios_ranking
go

create procedure dbo.up_relatorio_Laboratorios_ranking
 @dataIni		datetime
	,@dataFim		datetime
	,@familia		varchar(max)=''
	,@site			varchar(55)
	,@generico		varchar(18) = ''
	,@incliva		bit=0
/* with encryption */
AS 
BEGIN
	SET NOCOUNT ON
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
	DROP TABLE #dadosFamilia
	IF @dataIni = ''
	BEGIN
		SET @dataIni = '1900-01-01'
	END
	IF @dataFim = ''
	BEGIN
		SET @dataFim = convert(varchar(10), GETDATE(), 120)
	END
		/* Calc Loja e Armazens */
	DECLARE @site_nr as tinyint
	SET @site_nr = ISNULL((SELECT no FROM empresa WHERE site = @site),0)
		/* Convereter Valor Generico*/
	IF @generico = 'GENERICO'
		SET @generico = 1
	IF @generico = 'NÃO GENERICO'
		SET @generico = 0
	/* Calc familias */
	SELECT
		DISTINCT ref, nome
	INTO
		#dadosFamilia
	FROM
		stfami (nolock)
	WHERE
		ref in (Select items from dbo.up_splitToTable(@familia,','))
		Or @familia = '0'
		or @familia = ''
	SELECT DISTINCT
		 st.u_lab																							AS nomeLaboratorio
		,SUM(FN.qtt)																						AS quantidade
		,CASE WHEN  @incliva = '1' THEN SUM(fn.etiliquido)  ELSE  SUM((fn.etiliquido / (fn.iva/100+1))) END	AS valor 
	FROM 
		fo					(nolock) 
		INNER JOIN  FN		(nolock)	ON FO.FOSTAMP = FN.FOSTAMP
		INNER JOIN  ST		(nolock)	ON (FN.ref	 = ST. ref  AND ST.site_nr= @site_nr)
		INNER JOIN  #dadosFamilia		ON ST.familia = #dadosfamilia.ref
		LEFT  JOIN  fprod	(nolock)	ON FN.ref = fprod.ref
	WHERE 
		fo.data BETWEEN @dataIni and @dataFim
		AND fo.site = (CASE WHEN @site = '' THEN fo.site ELSE @site END)
		AND fo.doccode in (55, 100, 105, 3, 58) 
		AND fprod.generico = CASE WHEN @generico = '' THEN fprod.generico ELSE @generico END
	GROUP BY
		ST.u_lab
	ORDER BY 
		valor DESC
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
	DROP TABLE #dadosFamilia
END

GO
Grant Execute on dbo.up_relatorio_Laboratorios_ranking to Public
Grant control on dbo.up_relatorio_Laboratorios_ranking to Public
GO


