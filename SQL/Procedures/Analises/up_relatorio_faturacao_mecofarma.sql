
/*
 Relat�rio Resumo fatura��o Mecofarma 

 exec up_relatorio_faturacao_mecofarma '','20231218','atlantico'
*/



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_relatorio_faturacao_mecofarma]') IS NOT NULL
	drop procedure dbo.up_relatorio_faturacao_mecofarma
go

create procedure dbo.up_relatorio_faturacao_mecofarma

@dataIni		DATETIME = getdate,
@dataFim		DATETIME = getdate,
@site			VARCHAR(254)

/* with encryption */

AS
SET NOCOUNT ON
	
	
	
	DECLARE @sql VARCHAR(4000) = ''
	
	SET @sql = @sql + N'
	
		SELECT 
			------------------CABE�ALHO--------------------
			(select ndoc from uf_nDoc_Mecofarma (ft.ftstamp))as NDOCFT
 			,td.tiposaft
			,iif(fi.ofistamp = ''''
				, ft.u_nratend
				, (select nr_atendimento from uf_nDoc_Mecofarma ((select fi_temp.ftstamp from fi(nolock) fi_temp where fi_temp.fistamp = fi.ofistamp)))
			)as nr_atendimento
			,iif(fi.ofistamp = ''''
				, ft2.u_receita
				, (select nr_receita from uf_nDoc_Mecofarma ((select fi_temp.ftstamp from fi(nolock) fi_temp where fi_temp.fistamp = fi.ofistamp)))
			)as u_receita
			,iif(fi.ofistamp = ''''
				, ft2.u_nbenef
				, (select nr_cli from uf_nDoc_Mecofarma ((select fi_temp.ftstamp from fi(nolock) fi_temp where fi_temp.fistamp = fi.ofistamp)))
			)as nr_cli 
			,iif(fi.ofistamp = ''''
				, ft2.u_nbenef
				, (select nr_cartao from uf_nDoc_Mecofarma ((select fi_temp.ftstamp from fi(nolock) fi_temp where fi_temp.fistamp = fi.ofistamp)))
			)as nr_cartao
			,ft.nome
			,ft.fdata
			,Cast(round(ft.etotal,2) as numeric(19,2)) as etotal
			,(case
				when td.tiposaft = ''NC''
					then
						isnull((select ndoc from uf_nDoc_Mecofarma ((select fi_temp.ftstamp from fi(nolock) fi_temp where fi_temp.fistamp = fi.ofistamp))),'''')
					else
						''''
					end) as nDocOri
			------------LINHAS-----------------------------
			,fi.ndoc
			,fi.lordem
			,fi.design
			,cast(abs(fi.qtt) as numeric(19)) as qtt
			,''Comp'' as unidade
			,Cast(round(fi.epv,2) as numeric(19,2)) as epv
			,Cast(round(fi.etiliquido,2) as numeric(19,2)) as etiliquido 

		FROM ft(nolock)
		left outer join ft2(nolock) on ft2.ft2stamp = ft.ftstamp
		left outer join fi(nolock) on fi.ftstamp = ft.ftstamp
		left outer join td(nolock) on td.ndoc = ft.ndoc
		left outer join empresa(nolock) on empresa.site = ft.site
		WHERE 
			ft.fdata BETWEEN '''+convert(varchar,@dataini)+''' AND '''+convert(varchar,@datafim)+'''
			AND ft.site = '''+convert(varchar,@site)+'''
			and td.tiposaft <>'''''
			

	SET @sql = + @sql + N'
	ORDER BY 
		ft.NDOC, ft.no, fdata desc'	

	PRINT @sql
	EXEC(@sql)
GO
Grant Execute on dbo.up_relatorio_faturacao_mecofarma to Public
Grant control on dbo.up_relatorio_faturacao_mecofarma to Public
GO
