/* 

	exec up_relatorio_clientes_ranking '1900-01-01','2025-01-23','','',0,1,''
	
*/

if OBJECT_ID('[dbo].[up_relatorio_clientes_ranking]') IS NOT NULL
	drop procedure dbo.up_relatorio_clientes_ranking
go

create procedure [dbo].up_relatorio_clientes_ranking
	 @dataIni		datetime
	,@dataFim		datetime
	,@familia		varchar(max)=''
	,@site			varchar(55)
	,@estab			numeric(9,0)
	,@incliva		bit=0
	,@tipo	varchar(20)
/* with encryption */
AS
SET NOCOUNT ON
	IF @dataIni = ''
	BEGIN
		SET @dataIni = '1900-01-01'
	END
	IF @dataFim = ''
	BEGIN
		SET @dataFim = convert(varchar(10), GETDATE(), 120)
	END
	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
		DROP TABLE #dadosFamilia
	/* Calc Loja  */
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)
		/* Calc familias */
	Select
		distinct ref, nome
	into
		#dadosFamilia
	From
		stfami (nolock)
	Where
		ref in (Select items from dbo.up_splitToTable(@familia,','))
		Or @familia = '0'
		or @familia = ''
	/* Define se vais mostrar resultados abaixo do Cliente 199 */
	declare @Entidades as bit
	set @Entidades = case when (select tipoempresa from empresa where site = @site) in ('FARMACIA', 'PARAFARMACIA') then convert(bit,1) else convert(bit,0) end
	/* Dados Base Vendas  */
	create table #dadosVendasBase (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)

		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)
	)
    insert #dadosVendasBase
    exec up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site,1
	/* Elimina Vendas tendo em conta tipo de cliente*/
	If @Entidades = 1
	delete from #dadosVendasBase where #dadosVendasBase.no <= 200
	SELECT 
		#dadosVendasBase.no															AS numeroCliente,
		B_UTENTES.nome																AS nomeCliente ,
		Sum(qtt)																	AS quantidade,
		CASE WHEN  @incliva = '0' THEN Sum(etiliquidoSiva) ELSE Sum(etiliquido) END AS valor,
		Sum(etiliquidoSiva)															AS totalEtiliquidoSiva,
		Sum(ettent1SIva)															AS totalEttent1SIva,
		SUM(ettent2SIva)															AS totalEttent2SIva,
		Sum(epcpond)																AS totalEpcpond,
		count(DISTINCT nrAtend)														AS numeroAtendimento,
		SUM(
			CASE 
				WHEN @incliva = '1' THEN ettent1 + ettent2
				ELSE ettent1SIva + ettent2SIva
			END
			) 																		AS valorEntidade

	FROM #dadosVendasBase 
	INNER JOIN B_UTENTES (nolock) on #dadosVendasBase.no = B_UTENTES.no and #dadosVendasBase.estab = B_UTENTES.estab
	INNER JOIN #dadosFamilia on #dadosVendasBase.familia = #dadosfamilia.ref
	WHERE 	    
		#dadosVendasBase.estab = CASE WHEN @estab = 0 THEN #dadosVendasBase.estab ELSE @estab END
		and b_utentes.tipo = case when @tipo = '' then b_utentes.tipo else @tipo end 
	GROUP BY
	 #dadosVendasBase.no,
	 B_UTENTES.nome
	ORDER BY 
		valor DESC
	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
		DROP TABLE #dadosFamilia

GO
Grant Execute on dbo.up_relatorio_clientes_ranking to Public
Grant control on dbo.up_relatorio_clientes_ranking to Public
GO