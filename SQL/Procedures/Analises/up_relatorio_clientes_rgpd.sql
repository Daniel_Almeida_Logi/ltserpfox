/* Relatório Vendas Suspensas

exec up_relatorio_clientes_rgpd 'Todas','TODOS'
select * from ft

*/



if OBJECT_ID('[dbo].[up_relatorio_clientes_rgpd]') IS NOT NULL
	drop procedure dbo.up_relatorio_clientes_rgpd
go

create procedure dbo.up_relatorio_clientes_rgpd
@site	 varchar(55),
@Ativo varchar(10)='TODOS' /* foi colocado a dizer activo em vez de inativo para ficar em concordancia com o relatorio*/
/* with encryption */

AS
SET NOCOUNT ON


	select
		no
		, nome
		, tlmvl
		, email
		, (select count(ftstamp) from ft (nolock) where ft.no=b_utentes.no and (fdata between GETDATE()-1095 and GETDATE()) and ft.site= (case when @site = 'Todas' then ft.site else @site end)) as nrmov3anos
		, (case when (select count(ftstamp) from ft (nolock) where ft.no=b_utentes.no and (fdata between GETDATE()-1095 and GETDATE()) and ft.site= (case when @site = 'Todas' then ft.site else @site end) ) = 0 then 'X' else '' end) as smov3anos
		, (select count(ftstamp) from ft (nolock) where ft.no=b_utentes.no and (fdata between GETDATE()-730 and GETDATE()) and ft.site= (case when @site = 'Todas' then ft.site else @site end) ) as nrmov2anos
		, (case when (select count(ftstamp) from ft (nolock) where ft.no=b_utentes.no and (fdata between GETDATE()-730 and GETDATE()) and ft.site= (case when @site = 'Todas' then ft.site else @site end) ) = 0 then 'X' else '' end) as smov2anos
		, nascimento
		, datediff(year, nascimento, getdate()) as idade
		, (case when datediff(year, nascimento, getdate()) < 18 then 'X' else '' end) as menor
		, (select count(bu.utstamp) from b_utentes bu (nolock) where bu.no=b_utentes.no and bu.estab<>0 ) as nrdependentes
		, (select count(bu.utstamp) from b_utentes bu (nolock) where bu.no=b_utentes.no and bu.estab<>0 and datediff(year, bu.nascimento, getdate()) < 18) as nrdepmenores
		, (case when (select count(bu.utstamp) from b_utentes bu (nolock) where bu.no=b_utentes.no and bu.estab<>0 and datediff(year, bu.nascimento, getdate()) < 18)>0 then 'X' else '' end)as depmenores
		, (case when year(nascimento)=1900 then 'X' else '' end) as nasc1900
		, (select 
				count(ft.ftstamp) 
			from 
				ft (nolock)
				inner join fi (nolock) on ft.ftstamp=fi.ftstamp
			where 
				ft.no = b_utentes.no
				and ft.cobrado=1 
				and ft.anulado=0 
				and ft.fdata>(getdate()-30) 
				and (ft.tipodoc=1 or ft.nmdoc='Factura Acordo') 
				and (fi.qtt-isnull((select Sum(qtt) as qtt from fi fix (nolock) where fi.fistamp=fix.ofistamp),0) )>0
				and ft.nmdoc!='Factura SNS' 
				and ft.nmdoc!='Factura Entidades'
				and ft.site = (case when @site = 'Todas' then ft.site else @site end)
			) as nrvdsuspmen30d
		,(case when (select 
				count(ft.ftstamp) 
			from 
				ft (nolock)
				inner join fi (nolock) on ft.ftstamp=fi.ftstamp
			where 
				ft.no = b_utentes.no
				and ft.cobrado=1 
				and ft.anulado=0 
				and ft.fdata>(getdate()-30) 
				and (ft.tipodoc=1 or ft.nmdoc='Factura Acordo') 
				and (fi.qtt-isnull((select Sum(qtt) as qtt from fi fix (nolock) where fi.fistamp=fix.ofistamp),0) )>0
				and ft.nmdoc!='Factura SNS' 
				and ft.nmdoc!='Factura Entidades'
				and ft.site = (case when @site = 'Todas' then ft.site else @site end)
			)>0 then 'X' else '' end) as vdsuspmen30d
		, (case when autorizado=1 then 'X' else '' end) as autorizado
		, (case when autoriza_sms=1 then 'X' else '' end) as autoriza_sms
		, (case when autoriza_emails=1 then 'X' else '' end) as autoriza_emails
		,inactivo
	from 
		b_utentes (nolock)
	where 
		estab=0
		and no>200
		and removido=0
		and inactivo = case when @Ativo = 'TODOS' then inactivo else (case when @Ativo = 'NÃO' then 1 else 0  END) end 
	order by 
		no
GO
Grant Execute on dbo.up_relatorio_clientes_rgpd to Public
Grant control on dbo.up_relatorio_clientes_rgpd to Public
GO
