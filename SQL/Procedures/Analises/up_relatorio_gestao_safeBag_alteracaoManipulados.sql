/* Relatório Alteracao Manipulados

	exec up_relatorio_gestao_safeBag_alteracaoManipulados '20100101','20130101',''
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_safeBag_alteracaoManipulados]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_safeBag_alteracaoManipulados
go

create procedure dbo.up_relatorio_gestao_safeBag_alteracaoManipulados
@dataIni DATETIME,
@dataFim datetime,
@site varchar(60)
/* with encryption */

AS
SET NOCOUNT ON

select
	cx.dabrir, B_regFormasPag.*
from 
	B_regFormasPag (nolock)
	inner join cx (nolock) on cx.cxstamp=B_regFormasPag.cxstamp
where 
	(cx.dabrir between @dataIni and @dataFim)
    and cx.site = case when @site='' then cx.site else @site end
order by 
	dabrir

GO
Grant Execute on dbo.up_relatorio_gestao_safeBag_alteracaoManipulados to Public
Grant control on dbo.up_relatorio_gestao_safeBag_alteracaoManipulados to Public
GO
