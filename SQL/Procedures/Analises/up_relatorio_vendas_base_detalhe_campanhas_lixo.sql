/* Relatório Base para Calculo de Vendas incluindo Campanhas

	exec up_relatorio_vendas_base_detalhe_campanhas '20200101','20201112','Loja 1', 0

*/





if OBJECT_ID('[dbo].[up_relatorio_vendas_base_detalhe_campanhas]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_base_detalhe_campanhas
go

create procedure [dbo].[up_relatorio_vendas_base_detalhe_campanhas]
	@dataIni			datetime
	,@dataFim			datetime
	,@site				varchar(200)
	,@incluiHistorico	bit = 0


/* with encryption */

AS
SET NOCOUNT ON

	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosNumAtend'))
		DROP TABLE #dadosNumAtend

	/* Id do site */
	declare @site_nr varchar(20)
	set @site_nr = isnull((select  convert(varchar(254),(select 
							convert(varchar,no) + ', '
						from 
							empresa (nolock)							
						Where 
							site in (Select items from dbo.up_splitToTable(@site, ','))
						FOR XML PATH(''))) as no),0)
	
	--print @site_nr
	--print @site
	
	
	if @site = ''
		set @site = isnull((select  convert(varchar(254),(select 
							site + ', '
						from 
							empresa (nolock)							
						FOR XML PATH(''))) as no),0)

	--print @site
			


	/*  Usado no calculo de numero de atendimentos; Usado para validar existencia de Vales ao atendimento */
	Select distinct
		ft.u_nratend
	into
		#dadosNumAtend
	From
		fi (nolock)
		inner join ft (nolock) on fi.ftstamp = ft.ftstamp
	where
		fi.epromo = 1
		and fdata between @dataIni and @dataFim

	/* DOCS DE FACTURACAO */
	if @incluiHistorico = 0
	begin

		SELECT
			nrAtend			= ft.u_nratend
			,ft.ftstamp
			,ft.fdata
			,ft.no
			,ft.estab
			,b_utentes.Tipo
			,ft.ndoc
			,ft.fno
			,td.tipodoc
			,td.u_tipodoc
			,ref			= case when fi.ref='' then fi.oref else fi.ref end
			,fi.design
			,fi.familia
			,u_epvp			= u_epvp
			,epcpond		= (CASE 
								WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt 
								ELSE FI.EPCP * qtt 
								END)
								*
								(CASE WHEN TD.tipodoc = 3 THEN (-1) else 1 end)
			,fi.iva
			,qtt			= CASE WHEN TD.tipodoc = 3 THEN -qtt ELSE qtt END
			,etiliquido		= case when fi.ivaincl = 1 then fi.etiliquido else fi.etiliquido + (fi.etiliquido*(fi.iva/100)) end
			,etiliquidoSIva = case when fi.ivaincl = 1 then (fi.etiliquido / (fi.iva/100+1)) else fi.etiliquido end
			,ettent1		= case when ft.tipodoc = 3 then u_ettent1*-1 else u_ettent1 end
			,ettent2		= case when ft.tipodoc = 3 then u_ettent2*-1 else u_ettent2 end
			,ettent1SIva	= case when ft.tipodoc = 3 then (u_ettent1/(fi.iva/100+1))*-1 else (u_ettent1/(fi.iva/100+1)) end
			,ettent2SIva	= case when ft.tipodoc = 3 then (u_ettent2/(fi.iva/100+1))*-1 else (u_ettent2/(fi.iva/100+1)) end
			,fi.desconto
			,descvalor		= ((case 
								when fi.desconto = 100 then fi.epv * fi.qtt 
								when (fi.desconto between 0.01 and 99.99) then (fi.epv * fi.qtt) - abs(etiliquido)
								else 0 
								end)
								--+
								-u_descval
								)
								*
								(CASE WHEN TD.tipodoc = 3 THEN (-1) else 1 end)
			-- Só descontos em valor e que correspondem a um vale
			,descvale		= case  
								when ft.u_nratend in (select u_nratend from #dadosNumAtend) THEN u_descval 
								ELSE 0 
								END
			,ousrhora		= Ft.ousrhora
			,ousrinis		= ft.ousrinis
			,vendnm			= ft.vendnm
			,ft.u_ltstamp
			,ft.u_ltstamp2
			,ecusto			= (fi.ecusto * qtt) * (CASE WHEN TD.tipodoc = 3 THEN (-1) else 1 end)
			,ft.site
			,site_nr = empresa.no	
			,fi.campanhas
			,fi.pontos
			,fi.valcartao
		FROM
			FI				(nolock)
			INNER JOIN FT	(nolock) on FI.FTSTAMP = FT.FTSTAMP
			INNER JOIN TD	(nolock) on TD.NDOC = FT.NDOC
			INNER JOIN B_UTENTES (nolock) on ft.no = b_utentes.no and ft.estab = b_utentes.estab
			INNER join empresa (nolock) on empresa.site = ft.site
		WHERE
			fi.composto = 0
			AND ft.anulado = 0
			AND (FT.tipodoc != 4 or u_tipodoc = 4)
			AND fdata between @dataIni and @dataFim
			--and ft.site in (case when @site = '' Then ft.site else Select items from dbo.up_splitToTable(@site, ',') end)
			and ft.site in (Select items from dbo.up_splitToTable(@site, ',')) 
			
	
	end else begin

		SELECT
			nrAtend			= ft.u_nratend
			,ft.ftstamp
			,ft.fdata
			,ft.no
			,ft.estab
			,b_utentes.Tipo
			,ft.ndoc
			,ft.fno
			,td.tipodoc
			,td.u_tipodoc
			,ref			= case when fi.ref='' then fi.oref else fi.ref end
			,fi.design
			,fi.familia
			,u_epvp			= u_epvp
			,epcpond		= (CASE 
								WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt 
								ELSE FI.EPCP * qtt 
								END)
								*
								(CASE WHEN TD.tipodoc = 3 THEN (-1) else 1 end)
			,fi.iva
			,qtt			= CASE WHEN TD.tipodoc = 3 THEN -qtt ELSE qtt END
			,etiliquido		= case when fi.ivaincl = 1 then fi.etiliquido else fi.etiliquido + (fi.etiliquido*(fi.iva/100)) end
			,etiliquidoSIva = case when fi.ivaincl = 1 then (fi.etiliquido / (fi.iva/100+1)) else fi.etiliquido end

			,ettent1		= case when ft.tipodoc = 3 then u_ettent1*-1 else u_ettent1 end
			,ettent2		= case when ft.tipodoc = 3 then u_ettent2*-1 else u_ettent2 end
			,ettent1SIva	= case when ft.tipodoc = 3 then (u_ettent1/(fi.iva/100+1))*-1 else (u_ettent1/(fi.iva/100+1)) end
			,ettent2SIva	= case when ft.tipodoc = 3 then (u_ettent2/(fi.iva/100+1))*-1 else (u_ettent2/(fi.iva/100+1)) end
			,fi.desconto
			,descvalor		= ((case 
								when fi.desconto = 100 then fi.epv * fi.qtt 
								when (fi.desconto between 0.01 and 99.99) then (fi.epv * fi.qtt) - abs(etiliquido)
								else 0 
								end)
								-u_descval
								)
								*
								(CASE WHEN TD.tipodoc = 3 THEN (-1) else 1 end)
			-- Só descontos em valor e que correspondem a um vale
			,descvale		= case when ft.u_nratend in (select u_nratend from #dadosNumAtend) THEN u_descval ELSE 0 END
			,ousrhora		= Ft.ousrhora
			,ousrinis		= ft.ousrinis
			,vendnm			= ft.vendnm
			,ft.u_ltstamp
			,ft.u_ltstamp2
			,ecusto = (fi.ecusto * qtt) * (CASE WHEN TD.tipodoc = 3 THEN (-1) else 1 end)
			,ft.site
			,site_nr = empresa.no	
			,fi.campanhas
			,fi.pontos
			,fi.valcartao
		FROM
			FI				(nolock)
			INNER JOIN FT	(nolock) on FI.FTSTAMP = FT.FTSTAMP
			INNER JOIN TD	(nolock) on TD.NDOC = FT.NDOC
			INNER JOIN B_UTENTES (nolock) on ft.no = b_utentes.no and ft.estab = b_utentes.estab
			INNER join empresa (nolock) on empresa.site = ft.site
		WHERE
			fi.composto = 0
			AND ft.anulado = 0
			AND (FT.tipodoc != 4 or u_tipodoc = 4)
			AND fdata between @dataIni and @dataFim

			--and ft.site in (case when @site = '' Then ft.site else (Select items from dbo.up_splitToTable(@site, ',')) end)			
			and	ft.site in (Select items from dbo.up_splitToTable(@site, ','))			
			
		
		union all

		SELECT
			nrAtend			= '0000000199'
			,ftstamp		= 'x'
			,fdata			= dateadd(mm, (hv.ano - 1900) * 12 + hv.mes - 1 , case when hv.dia=0 then 28 else hv.dia end - 1)
			,no				= 0
			,estab			= 0
			,Tipo			= 'x'
			,ndoc			= 3
			,fno			= 0
			,tipodoc		= 1
			,u_tipodoc		= 8
			,ref			= hv.ref
			,design			= st.design
			,familia		
			,u_epvp			= hv.pvp
			,epcpond		= CASE
								WHEN st.epcpond = 0 THEN st.epcult * hv.qt
								ELSE st.epcpond * hv.qt
								END
			,iva			= taxasiva.taxa
			,qtt			= hv.qt
			,etiliquido		= hv.pvp*hv.qt
			,etiliquidoSIva = hv.pvp / (taxasiva.taxa/100+1)
			,ettent1		= 0
			,ettent2		= 0
			,ettent1SIva	= 0
			,ettent2SIva	= 0
			,desconto		= 0
			,descvalor		= 0
			,descvale		= 0
			,ousrhora		= hv.hora
			,ousrinis		= 'ADM'
			,vendnm			= 'ADM'
			,u_ltstamp		= ''
			,u_ltstamp2		= ''
			,ecusto = 0
			,hv.site
			,site_nr = st.site_nr
			,campanhas		= ''
			,pontos			= 0
			,valcartao		= 0	
		FROM
			hist_vendas hv	(nolock)
			inner join empresa(nolock) on empresa.site = hv.site
			inner join st on st.ref = hv.ref and st.site_nr = empresa.no
			inner join taxasiva on taxasiva.codigo = st.tabiva
		WHERE		
			
			--hv.site in (case when @site = '' Then hv.site else (Select items from dbo.up_splitToTable(@site, ',')) end)
			hv.site in (Select items from dbo.up_splitToTable(@site, ','))
			
			--and st.site_nr in (case when @site = '' Then st.site_nr else @site_nr end)
			and st.site_nr in (Select items from dbo.up_splitToTable(@site_nr, ','))
			and dateadd(mm, (hv.ano - 1900) * 12 + hv.mes - 1 , case when hv.dia=0 then 28 else hv.dia end - 1)	between @dataIni and @dataFim
	end
	




	
	
	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosNumAtend'))
		DROP TABLE #dadosNumAtend

GO
Grant Execute on dbo.up_relatorio_vendas_base_detalhe_campanhas to Public
Grant control on dbo.up_relatorio_vendas_base_detalhe_campanhas to Public
GO