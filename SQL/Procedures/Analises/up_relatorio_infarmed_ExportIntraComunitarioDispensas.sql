-- Relatório Quantidades Vendidas INFARMED
-- exec	up_relatorio_infarmed_ExportIntraComunitarioDispensas 2020,07,'Loja 1',1
-- Quantidades Dispensadas: Ano, Mês, Nº Registo, Quantidade dispensada
-- ex. 2013,05,5019302,123213
-- Listagem Para Farmácias

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_infarmed_ExportIntraComunitarioDispensas]') IS NOT NULL
	drop procedure dbo.up_relatorio_infarmed_ExportIntraComunitarioDispensas
go

create procedure dbo.up_relatorio_infarmed_ExportIntraComunitarioDispensas

@ano as numeric(4),
@mes as numeric(2),
@site as varchar(60),
@armazem as bit



/* with encryption */
AS

SET NOCOUNT ON
	
	Declare @site_nr int = 0

    select top 1 @site_nr=isnull(no,0) from empresa(nolock) where site =isnull(@site,'')

		
	IF @armazem = 0
	BEGIN
		select 
			ano = year(ft.fdata)
			,mes = convert(varchar(2),replicate('0',2-len(month(ft.fdata))) + convert(varchar,month(ft.fdata)))
			,fi.ref
			,qttDispensada = convert(int,SUM(case when ft.tipodoc!=3 then fi.qtt else -fi.qtt end))
		from
			ft (nolock)
			inner join fi (nolock) on ft.ftstamp=fi.ftstamp
			inner join st (nolock) on fi.ref=st.ref and st.site_nr=@site_nr
			inner join fprod (nolock) on fprod.cnp = fi.ref
			inner join st_infarmed_intraconsumos on st_infarmed_intraconsumos.ref=st.ref
		where 
			year(ft.fdata) = @ano
			AND month(ft.fdata) = @mes
			and ft.site = @site
			AND ft.anulado = 0 
			and ft.tipodoc != 4
		Group by
			year(ft.fdata)
			,month(ft.fdata)
			,fi.ref
		Order by 
			year(ft.fdata)
			,month(ft.fdata)
			,ref
	END
	ELSE
	BEGIN
		select 
			ano = year(ft.fdata)
			,mes = convert(varchar(2),replicate('0',2-len(month(ft.fdata))) + convert(varchar,month(ft.fdata)))
			,fi.ref
			,qttDispensada = convert(int,SUM(case when ft.tipodoc!=3 then fi.qtt else -fi.qtt end))
			,nif = B_utentes.ncont
			,entidade =  convert(varchar(254),REPLACE(B_utentes.nome,',',' '))
			,pais = case when isnull(b_cli_tabela_pais.code2,'') = '' then 'PRT' else b_cli_tabela_pais.code2 end
		from
			ft (nolock)
			inner join fi (nolock) on ft.ftstamp=fi.ftstamp
			inner join st (nolock) on fi.ref=st.ref  and st.site_nr=@site_nr
			inner join fprod (nolock) on fprod.cnp = fi.ref
			inner join st_infarmed_intraconsumos on st_infarmed_intraconsumos.ref=st.ref
			inner join B_utentes on ft.no = B_utentes.no and B_utentes.estab = ft.estab
			left join b_cli_tabela_pais on b_cli_tabela_pais.code = b_utentes.codigop
		where 
			year(ft.fdata) = @ano
			AND month(ft.fdata) = @mes
			and ft.site = @site
			AND ft.anulado = 0 
			and ft.tipodoc != 4
		Group by
			year(ft.fdata)
			,month(ft.fdata)
			,fi.ref
			,B_utentes.ncont
			,B_utentes.nome
			,b_cli_tabela_pais.code2
		Order by 
			year(ft.fdata)
			,month(ft.fdata)
			,ref
	END

GO
Grant Execute on dbo.up_relatorio_infarmed_ExportIntraComunitarioDispensas to Public
GO
Grant Control on dbo.up_relatorio_infarmed_ExportIntraComunitarioDispensas to Public
Go
