/* 
	exec up_relatorio_conferencia_QtEncVsQtRec '', '', 0, 'Loja 1','20230420', '20230421'
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_relatorio_conferencia_QtEncVsQtRec]') IS NOT NULL
	DROP PROCEDURE dbo.up_relatorio_conferencia_QtEncVsQtRec
GO

CREATE PROCEDURE dbo.up_relatorio_conferencia_QtEncVsQtRec
	@documento		AS VARCHAR(254),
	@num			AS VARCHAR(254),
	@sorecZero		AS BIT,
	@site			AS VARCHAR(120),
	@dataIni		AS DATETIME,
	@dataFim		AS DATETIME
/* WITH ENCRYPTION */ 
AS
BEGIN
	Select	
		nome = UPPER(nome)
		,no
		,estab
		,fo.docdata
		,fo.docnome
		,adoc = (SELECT bi.obrano FROM bi (NOLOCK) WHERE bi.bistamp=fn.bistamp)
		,ref
		,design
		,qtt
		,qttenc = (SELECT bi.qtt FROM bi (NOLOCK) WHERE bi.bistamp=fn.bistamp)
		,'User' = UPPER(fo.ousrinis)
	FROM	
		fn (NOLOCK) 
		INNER JOIN fo (NOLOCK) ON fo.fostamp = fn.fostamp 
	WHERE	
		qtt != (SELECT bi.qtt FROM bi (NOLOCK) WHERE bi.bistamp=fn.bistamp)
		AND fo.adoc		= CASE WHEN @num = '' THEN fo.adoc ELSE @num END 
		AND fo.docnome	= CASE WHEN @documento = '' Then fo.docnome ELSE @documento END
		AND fn.qtt		= CASE WHEN @sorecZero = 1 Then 0 Else qtt End
		AND fo.site		= CASE WHEN @site = '' THEN fo.site ELSE @site END 
		AND fo.docdata BETWEEN @dataIni AND @dataFim
	ORDER BY 
		fo.docdata
END

GO
Grant Execute on dbo.up_relatorio_conferencia_QtEncVsQtRec to Public
Grant control on dbo.up_relatorio_conferencia_QtEncVsQtRec to Public
Go
