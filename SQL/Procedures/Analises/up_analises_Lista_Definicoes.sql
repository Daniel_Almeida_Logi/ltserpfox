-- Lista an�lises para Painel Defini��o
-- exec up_analises_Lista_Definicoes 47

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_analises_Lista_Definicoes]') IS NOT NULL
	drop procedure dbo.up_analises_Lista_Definicoes
go

create procedure dbo.up_analises_Lista_Definicoes
@ordem as int


/* WITH ENCRYPTION */


AS 
BEGIN
	Select 
		ordem,
		nome,
		label,
		tipo,
		comando,
		fordem,
		vdefault,
		vdefaultsql,
		vdefaultfox,
		colunas,
		colunasWidth,
		obrigatorio,
		firstempty,
		mascara,
		formato,
		multiselecao,
		comandoesql,
		visivel
	from 
		B_analises_config (nolock) 
	Where
		ordem = @ordem
	order by
		fordem
	
END

GO
Grant Execute on dbo.up_analises_Lista_Definicoes to Public
GO
Grant Control on dbo.up_analises_Lista_Definicoes to Public
GO
