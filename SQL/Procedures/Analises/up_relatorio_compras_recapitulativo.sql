/* Relatório Recapitulativo Compras  - usado para preenchimento do mapa de recapitulativo de compras a enviar para as finanças

	exec up_relatorio_compras_recapitulativo '20160601','20160630', 500, 'Loja 1'

*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_relatorio_compras_recapitulativo]') IS NOT NULL
	drop procedure dbo.up_relatorio_compras_recapitulativo
go

create procedure dbo.up_relatorio_compras_recapitulativo
	@dataIni		as datetime,
	@dataFim		as datetime,
	@valorCompras	as numeric(10,0),
	@site			varchar(55)
	
/* with encryption */
AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosComprasBase'))
		DROP TABLE #dadosComprasBase

BEGIN

	-- select identico ao relatório compras familia
	select 
		'TotalVendas' = sum(CASE WHEN DEBITO=1 THEN ETILIQUIDO*-1 ELSE ETILIQUIDO END),
		'NomeFornecedor' = fo.nome,													 
		'NoFornecedor'   = fo.no,														
		'NoCont'      = fo.ncont													 
	into 
		#dadosComprasBase
	from 
		FO (nolock) 
		INNER JOIN FN (nolock) ON FO.FOSTAMP = FN.FOSTAMP
		INNER JOIN cm1 (nolock) ON  cm1.cm = fo.doccode
	where 
		cm1.cm in (55,56,3)
		and Fo.docdata between @dataIni and @dataFim
		and fo.site = case when @site = '' then fo.site else @site end
		and (fn.qtt !=0 or fn.ETILIQUIDO!=0)

	group by 
		fo.ncont, fo.nome, fo.no

	
	-- result set final
	select 
		* 
	from
		 #dadosComprasBase
	where
		TotalVendas >= @valorCompras

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosComprasBase'))
		DROP TABLE #dadosComprasBase

END

GO
Grant Execute on dbo.up_relatorio_compras_recapitulativo to Public
Grant control on dbo.up_relatorio_compras_recapitulativo to Public
GO
