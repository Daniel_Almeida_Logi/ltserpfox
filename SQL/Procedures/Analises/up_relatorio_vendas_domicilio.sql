/* Relatório Vendas Domicilio
 
	exec up_relatorio_vendas_domicilio '20170101','20171231','','',''
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_relatorio_vendas_domicilio]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_domicilio
GO

create procedure dbo.up_relatorio_vendas_domicilio
@dataIni as datetime,
@dataFim as datetime,
@nomeCliente as varchar(55),
@nomeTransportador as varchar(50),
@site varchar(55)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select  
	ft_doc = ft.nmdoc
	,ft_data = ft.fdata
	,ft_numdoc = ft.fno
	,ft_cliente	= '[' + convert(varchar(9),no) + '] ' + '[' + convert(varchar(9),estab) + '] ' + nome
	,ft2_contacto = ft2.contacto
	,ft2_u_viatura = ft2.u_viatura 
	,ft2_morada = LTRIM(RTRIM(ft2.morada)) + ' ' + LTRIM(RTRIM(ft2.codpost)) + ' ' + LTRIM(RTRIM(ft2.local))
	,ft2_telefone =	ft2.telefone
	,ft2_email = ft2.email
	,ft2_horaentrega = ft2.horaentrega
	,fi_ref = fi.ref
	,fi_design = fi.design
	,fi_qtt = fi.qtt
	,etiliquido		= case when fi.ivaincl = 1 then fi.etiliquido else fi.etiliquido + (fi.etiliquido*(fi.iva/100)) end
	,etiliquidoSIva = case when fi.ivaincl = 1 then (fi.etiliquido / (fi.iva/100+1)) else fi.etiliquido end
	,ettent1		= case when ft.tipodoc = 3 then u_ettent1*-1 else u_ettent1 end
	,ettent2		= case when ft.tipodoc = 3 then u_ettent2*-1 else u_ettent2 end
	,ettent1SIva	= case when ft.tipodoc = 3 then (u_ettent1/(fi.iva/100+1))*-1 else (u_ettent1/(fi.iva/100+1)) end
	,ettent2SIva	= case when ft.tipodoc = 3 then (u_ettent2/(fi.iva/100+1))*-1 else (u_ettent2/(fi.iva/100+1)) end
	,ft.ftstamp
	,fi.qtt*st.epv1 as vdpvp
from 
	ft2 (nolock)
	inner join ft (nolock) on ft2.ft2stamp = ft.ftstamp
	inner join fi (nolock) on ft.ftstamp = fi.ftstamp
	inner join st on st.ref=fi.ref and st.site_nr=(select no from empresa where site=@site)
where   
	ft2.u_vddom = 1
	and fdata between @dataIni and @dataFim
	and ft2.contacto = Case when @nomeTransportador = '' then ft2.contacto else @nomeTransportador end
	and '[' + convert(varchar(9),no) + '] ' + '[' + convert(varchar(9),estab) + '] ' + nome = case when @nomeCliente = '' then  '[' + convert(varchar(9),no) + '] ' + '[' + convert(varchar(9),estab) + '] ' + nome else @nomeCliente end
	and ft.site = (case when @site = '' Then ft.site else @site end)


GO
Grant Execute On dbo.up_relatorio_vendas_domicilio to Public
Grant Control On dbo.up_relatorio_vendas_domicilio to Public
GO
