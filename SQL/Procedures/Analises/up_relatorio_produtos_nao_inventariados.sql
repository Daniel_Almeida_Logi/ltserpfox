/* Relatório Caixas Operador

	exec up_relatorio_produtos_nao_inventariados '20170101','20171231', 'Loja 1'
	
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_produtos_nao_inventariados]') IS NOT NULL
	drop procedure dbo.up_relatorio_produtos_nao_inventariados
go

create procedure [dbo].up_relatorio_produtos_nao_inventariados
	@dataIni DATETIME
	,@dataFim DATETIME
	,@site varchar(30)

/* with encryption */

AS

	declare @armazem numeric(2,0)
	declare @sitenr numeric(2,0)
	set @armazem = ISNULL((select armazem from empresa_arm where empresa_no=(select no from empresa where site=@site)),0)
	set @sitenr = ISNULL((select no from empresa where site=@site),0)
	

	/* Calc artigos inventariados */
	select 
		distinct ref 
	into
		#arigosinventariados	
	from stil 
	where 
		sticstamp in (select sticstamp from stic where data between '20170101' and '20171231') 
		and armazem=@armazem

	select 
		ref
		,design
		, stock
		, local
		, u_local
		, u_local2
	from 
		st 
	where 
		site_nr=@sitenr
		and ref not in (select ref from #arigosinventariados)
		and stns=0
		and inactivo=0
		and bloqueado=0

		/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #arigosinventariados
GO
Grant Execute On dbo.up_relatorio_produtos_nao_inventariados to Public
Grant control On dbo.up_relatorio_produtos_nao_inventariados to Public
GO