-- Relatório TEFS
-- exec up_relatorio_gestao_safeBag_quebrasOperador '20100101','20130101',''

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_relatorio_gestao_safeBag_quebrasOperador]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_safeBag_quebrasOperador
GO

create procedure dbo.up_relatorio_gestao_safeBag_quebrasOperador
@dataIni DATETIME,
@dataFim datetime,
@site varchar(60)
/* with encryption */
AS 
SET NOCOUNT ON
DECLARE @NUMLOJA varchar(60)
SET @NUMLOJA = (SELECT REF FROM empresa (nolock) WHERE site = @site)
declare @loja varchar(60), @situacao varchar(12), @dinheiro char(8), @cheque char(8)
set @loja = isnull((select ref from empresa (nolock) where site=@site),'')
set @situacao = (select case when (select count(numero_safebag)
									from B_moneyFile_registos moneyFile_registos (nolock) 
									inner join cx (nolock) on cx.sacoDinheiro=moneyFile_registos.numero_safebag
									where data_de_venda>=@dataIni AND data_de_venda<=@dataFim AND sacoDinheiro!=0 AND
									cx.fechada=1 and cx.dabrir>=@dataIni AND cx.dabrir>=@dataFim AND  cx.site=@site
									AND substring(id_esegur_dep, patindex('%[^0]%',id_esegur_dep), 16)=@loja)
								> 0
								then 'Encerrado' else 'Aberto' end)
set @dinheiro = isnull((select ref from B_modoPag (nolock) where design like 'Dinheiro'),'0')
set @cheque   = isnull((select ref from B_modoPag (nolock)  where design like 'Cheques'),'0')
select	data, loja,@NUMLOJA as numloja, fusername nome, fuserno codigo,
		sum(valorSaco+saldo) - (sum(evdinheiro + echtotal + efundocx)) as somaquebra,
		ABS(sum(valorSaco+saldo) - (sum(evdinheiro + echtotal + efundocx))) as modulosomaquebra,
		sobra = SUM(case when (valorSaco-(evdinheiro+efundocx))>0 then (valorSaco-(evdinheiro+efundocx)) else 0 end),
		falta = SUM(case when (valorSaco-(evdinheiro+efundocx))<0 then (valorSaco-(evdinheiro+efundocx))*(-1) else 0 end)
from (
select	DATA, loja, pnome, pno, fusername, fuserno,
		sum(evdinheiro) as evdinheiro, 
		sum(echtotal) as echtotal, 
		efundocx	= case when sacoDinheiro='0' then 0 else efundocx end,
		sum(epaga1) as epaga1, sum(epaga2) as epaga2, /*sum(epaga3) as epaga3, sum(epaga4) as epaga4, sum(epaga5) as epaga5,*/
		valorSaco as valorSaco,
		sum(saldo) as saldo,
		total_caixa = sum(evdinheiro + echtotal + epaga1 + epaga2 /*+ epaga3 + epaga4 + epaga5*/),
		sacoDinheiro
from (
		select dabrir as data, cx.site as loja,
			 cx.pnome, cx.pno, fusername, fuserno,
			evdinheiro, echtotal, epaga1, epaga2,/* epaga3, epaga4, epaga5,*/
			efundocx =isnull((select top 1 efundocx from fcx fcx1 (nolock)
						inner join cx cx1 (nolock) on fcx1.cxstamp=cx1.cxstamp
						where fcx1.operacao='F' and cx1.fechada=1 and cx1.dabrir>=@dataIni AND cx1.dabrir<=@dataFim 
							and cx1.site=@site and cx1.fuserno=cx.fuserno and fcx1.efundocx!=0
						order by cxid),0),
			valorSaco = isnull(( select sum(valor_meiopagamento)
									from b_moneyFile_registos mfr (nolock)
									inner join cx cx1 (nolock) on cx1.sacodinheiro = mfr.numero_safebag
									where data_de_venda>=@dataIni AND data_de_venda<= @dataFim and sacoDinheiro!='0'
									AND substring(id_esegur_dep, patindex('%[^0]%',id_esegur_dep), 16)=@loja
									AND cx1.sacoDinheiro=cx.sacoDinheiro and cx1.cxstamp=cx.cxstamp
									AND (substring(codigo_meiopagamento,4,2)=@cheque OR substring(codigo_meiopagamento,4,2)=@dinheiro) )
							,0),
			saldo = (cx.saldod+cx.saldoc),
			cx.cxstamp, cx.sacoDinheiro
		from cx (nolock)
		inner join fcx (nolock) on fcx.cxstamp=cx.cxstamp
		where fcx.operacao='F' and cx.fechada=1 and cx.dabrir>=@dataIni AND cx.dabrir<= @dataFim and cx.site=@site
) as x
group by data, loja, pnome, pno, fusername, fuserno, efundocx, sacoDinheiro, valorsaco
) as xx
group by data, loja, fusername, fuserno


GO
Grant Execute on dbo.up_relatorio_gestao_safeBag_quebrasOperador to Public
Grant control on dbo.up_relatorio_gestao_safeBag_quebrasOperador to Public
GO