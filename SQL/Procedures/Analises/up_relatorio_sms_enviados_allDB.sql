-- Camapanhas SMS Enviadas para a totalidade de DBs
-- 2020-11-03
-- exec up_relatorio_sms_enviados_allDB -1,-1,'','20190101','20201031'

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_sms_enviados_allDB]') IS NOT NULL
    drop procedure dbo.up_relatorio_sms_enviados_allDB
go

create procedure dbo.up_relatorio_sms_enviados_allDB

@total		int,
@status		int,
@nome		varchar(250),
@dataA		datetime,
@dataDe		datetime

/* WITH ENCRYPTION */
AS


IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#b_sms_fact_all'))
		DROP TABLE #b_sms_fact_all

DECLARE @dbName sysname
declare @stmt varchar(max)
set @stmt = ''
declare @stmt1 varchar(max)
declare @stmt2 varchar(max)
declare @stmt3 varchar(max)
declare @stmt4 varchar(max)

DECLARE AllDBCursor CURSOR  STATIC LOCAL FOR
  SELECT   name FROM     MASTER.dbo.sysdatabases
  WHERE    name NOT IN ('master','tempdb','model','msdb') ORDER BY name

OPEN AllDBCursor; 
FETCH  AllDBCursor INTO @dbName;

WHILE (@@FETCH_STATUS = 0) -- loop through all db-s 
  BEGIN
			select @stmt1 = 'use ' + @dbName + ' ; if exists ( SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N''b_sms_fact'' )	begin'
										
			set @stmt2 =  ' declare @dbName_ varchar(100); select @dbName_=''' + @dbName + ''' from ' + @dbName + '.dbo.b_sms_fact (nolock) '	

			set @stmt4 =  ' select ''' + @dbName + ''' as DB, * from ' + @dbName + '.dbo.b_sms_fact (nolock)'		
			
			select @stmt3 = @stmt1 + @stmt2 + ' end '

			exec(@stmt3)
			if @@ROWCOUNT>0
			begin
				set @stmt = @stmt + @stmt4 + ' UNION ALL '
			end							       
    FETCH  AllDBCursor   INTO @dbName	
  END -- while 

CLOSE AllDBCursor; 
DEALLOCATE AllDBCursor;

--print (@stmt)
SET @stmt = left(@stmt,len(@stmt)- 9)  /*retirar o ultimo UNION ALL*/

create table #b_sms_fact_all (
	db varchar(50) NOt NULL,	
	[stamp] [char](25) NOT NULL,
	[campaignId] [int] NOT NULL,
	[name] [varchar](250) NOT NULL,
	[listId] [int] NOT NULL,
	[username] [varchar](50) NOT NULL,
	[listTlm] [text] NOT NULL,
	[total] [int] NOT NULL,
	[mensagem] [varchar](459) NOT NULL,
	[dataenvio] [datetime] NOT NULL,
	[horaenvio] [varchar](8) NOT NULL,
	[status] [int] NOT NULL
) 

insert #b_sms_fact_all
exec (@stmt)


/*result set final*/
select 
	db	
	,username
	,total
	,status
	,name
	,[dataenvio] = convert(date,dataenvio,101)
	,horaenvio
	,case when name='RGPD' then '' else mensagem end as mensagem
	,listTlm
from 
	#b_sms_fact_all (nolock) 
where
	total = case when @total>=0 then @total else total end
	and status = case when @status>=0 then @status else status end
	and name like case when @nome='' then name else '%'+@nome+'%' end
	and dataenvio between @dataA and @dataDe
order by
	db,
	dataenvio desc
	,horaenvio desc


--sum(total) para cada DB
--select db,sum(total) as total_all from #b_sms_fact_all (nolock) group by db


IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#b_sms_fact_all'))
		DROP TABLE #b_sms_fact_all



GO
Grant Execute on dbo.up_relatorio_sms_enviados_allDB to Public
Grant Control on dbo.up_relatorio_sms_enviados_allDB to Public
go