/*
	 exec up_valida_mecosync 
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_valida_mecosync]') IS NOT NULL
	drop procedure dbo.up_valida_mecosync
go
create procedure dbo.up_valida_mecosync

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON	

DECLARE @existMecoSync  AS BIT = 0 

IF EXISTS (SELECT name FROM master.sys.databases WHERE name = N'mecosync')
BEGIN
	set @existMecoSync = 1
END

SELECT @existMecoSync	AS valido

GO
Grant Execute On dbo.up_valida_mecosync to Public
Grant Control On dbo.up_valida_mecosync to Public
GO