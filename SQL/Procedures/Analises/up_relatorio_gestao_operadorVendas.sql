/* Relatório Vendas Por Operador

	exec up_relatorio_gestao_operadorVendas '20160118','20160121','0','','','bene - mais um carrada de caracteres so para encher e ver se parte!','','','Loja 1','','', 1, '', 0 
	exec up_relatorio_gestao_operadorVendas '20171016','20171016','0','','','','','','Loja 1','','', 1, '', 1

	exec up_relatorio_gestao_operadorVendas '20160118','20160121','0','','','','','MSRM','Loja 1','','', 1, '', 0

	exec up_relatorio_gestao_operadorVendas '20200924','20200924','0','','','','','','Loja 1','','', 0, '', 0
	exec up_relatorio_gestao_operadorVendas '20200924','20200924','0','','','','','MSRM','Loja 1','','', 1, '', 0


	select * from b_us
	
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_operadorVendas]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_operadorVendas
go

create procedure dbo.up_relatorio_gestao_operadorVendas
@dataIni		as datetime,
@dataFim		as datetime,
@op				as varchar(max),
@ref			as varchar(254),
@lab			as varchar(100),
@marca			as varchar(200),
@familia		as varchar(max),
@atributosFam	as varchar(max),
@site			as varchar(55),
@design			as varchar(60),
@iva			as varchar(20),
@ivaincl		as bit = 1,
@generico		varchar(18),
@agrupaLab		as bit
/* with encryption */
AS 
BEGIN
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempCteNumAtend'))
		DROP TABLE #tempCteNumAtend
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempcteFamilia'))
		DROP TABLE #tempcteFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempcteOperadores'))
		DROP TABLE #tempcteOperadores
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteSt'))
		DROP TABLE #cteSt
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteSeries'))
		DROP TABLE #cteSeries
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteDados_1'))
		DROP TABLE #cteDados_1
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteDados_2'))
		DROP TABLE #cteDados_2
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteDados_3'))
		DROP TABLE #cteDados_3

	SET @atributosFam = (CASE WHEN (@atributosFam IS NULL OR @atributosFam = '') THEN ';;;' ELSE @atributosFam END)

	DECLARE @departamento VARCHAR(254) = ''
	SET @departamento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 1),'') 
	DECLARE @sessao       VARCHAR(254) = ''
	set @sessao = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 2) ,'')
	DECLARE @categoria    VARCHAR(254) = ''
	SET @categoria = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 3),'') 
	DECLARE @segmento     VARCHAR(254) = ''
	SET @segmento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 4),'')

	/* Convereter Valor Generico*/
	IF @generico = 'GENERICO'
		set @generico = 1
	IF @generico = 'NÃO GENERICO'
		set @generico = 0
	/* Calc Loja e Armazens */
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)
	/* Calc Nº Atend */	
	Select 
		distinct ft.u_nratend 
	Into
		#tempCteNumAtend
	From 
		fi (nolock) 
		inner join ft (nolock) on fi.ftstamp = ft.ftstamp 
	where
		fi.epromo = 1 
		and fdata between @dataIni and @dataFim
	/* Calc Familia Infarmed */
	Select 
		distinct ref, nome
	into
		#tempcteFamilia
	From 
		stfami (nolock) 
	Where 
		ref in (Select items from dbo.up_splitToTable(@familia,','))
		Or @familia = '0' 
		or @familia = ''
	/* Calc Operadores */
	Select 
		iniciais
		,cm = userno
		,username
	into
		#tempcteOperadores
	from 
		b_us (nolock) 
	Where 
		userno in (select items from dbo.up_splitToTable(@op,',')) 
		or @op= '0' 
		or @op= ''
	/* Calc Dados ST*/
	Select
		st.ref 
		,st.design
		,usr1
		,u_lab	
		, ISNULL(A.descr,'') as departamento
		, ISNULL(B.descr,'') as seccao
		, ISNULL(C.descr,'') as categoria
		, ISNULL(D.descr,'') as segmento_
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 1) valDepartamento
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 2) valSeccao
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 3) valCategoria
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 4) valSegmento
		,generico = isnull(fprod.generico,0)
	into
		#cteSt
	From
		st (nolock)
		left join fprod (nolock) on st.ref = fprod.cnp	
		left join grande_mercado_hmr (nolock) A on A.id = st.u_depstamp
		left join mercado_hmr (nolock) B on B.id = st.u_secstamp
		left join categoria_hmr (nolock) C on C.id = st.u_catstamp 
		left join segmento_hmr (nolock) D on D.id = st.u_segstamp		
	Where	
		st.u_lab = case when @lab = '' then u_lab else @lab end
		And st.usr1 = case when @marca = '' then usr1 else @marca end	
		and site_nr = @site_nr	
	/* Calc Series faturacao */
	Select 
		*
	into 
		#cteSeries 
	from (
		select 
			distinct serie_Ent from empresa (nolock)
		union all 
		select 
			distinct serie_EntSNS from empresa (nolock)	
	) as x
	/* Define se vais mostrar resultados abaixo do Cliente 199 */
	declare @Entidades as bit
	set @Entidades = case when (select tipoempresa from empresa where site = @site) in ('FARMACIA', 'PARAFARMACIA') then convert(bit,1) else convert(bit,0) end
	/* DOCS DE FACTURACAO */
	SELECT
		NrVendas = CASE WHEN ft.tipodoc=1 and ft.ndoc not in (select * from #cteSeries) THEN 1 ELSE 0 END
		,NrDev = CASE WHEN ft.tipodoc=3 and ft.ndoc not in (select * from #cteSeries) THEN 1 ELSE 0 END
		,u_descval = CASE WHEN ft.u_nratend in (select u_nratend from #tempCteNumAtend) THEN u_descval ELSE 0 END /* Só descontos em valor e que correspondem a um vale*/	
		,fdata
		,Ano = YEAR(FT.fdata)
		,Mes = MONTH(FT.FDAta)
		,ft.nmdoc
		/* valores com e sem iva devido ao facto do relatório ter a opção de incluir ou não IVA */
		,etiliquido_cIva = CASE WHEN Fi.ivaincl = 1 THEN Fi.etiliquido ELSE Fi.etiliquido + (Fi.etiliquido*(fi.iva/100)) END 
		,etiliquido_sIva = CASE WHEN Fi.IVAINCL = 0 THEN Fi.ETILIQUIDO ELSE Fi.ETILIQUIDO / ((Fi.iva / 100)+1) END
		/* valores sem Iva */
		,u_ettent1_sIva  = CASE WHEN Ft.tipodoc = 3	THEN CASE WHEN Fi.IVAINCL = 0 THEN u_ettent1 * -1 ELSE (u_ettent1 / ((Fi.iva / 100)+1)) * (-1) END
													ELSE CASE WHEN Fi.IVAINCL = 0 THEN u_ettent1 ELSE (u_ettent1 / ((Fi.iva / 100)+1))END
						   END
		,u_ettent2_sIva	 = CASE WHEN Ft.tipodoc = 3 THEN CASE WHEN Fi.IVAINCL = 0 THEN u_ettent2 * -1 ELSE (u_ettent2 / ((Fi.iva / 100)+1)) * (-1) END
													ELSE CASE WHEN Fi.IVAINCL = 0 THEN u_ettent2 ELSE (u_ettent2 / ((Fi.iva / 100)+1)) END
						   END
		,ettent1		 = CASE WHEN ft.tipodoc = 3 THEN fi.u_ettent1*-1 ELSE fi.u_ettent1 END
		,ettent2		 = CASE WHEN ft.tipodoc = 3 THEN fi.u_ettent2*-1 ELSE fi.u_ettent2 END
		/* Preço de Custo é sempre sem IVA (valores de Preços de Custo no software são sempre s/ Iva) */
		,epcpond		= CASE WHEN TD.tipodoc = 3 THEN /* DEV. A CLIENTE */ (CASE WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt ELSE FI.EPCP * qtt END)*-1
												   ELSE (CASE WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt ELSE FI.EPCP * qtt END) 
						  END
		,u_EPVP			= CASE WHEN TD.tipodoc = 3 THEN /* DEV. A CLIENTE */ FI.u_EPVP * qtt * (-1)
												   ELSE	FI.u_EPVP * qtt
						  END
		/* CALCULO DA MARGEM CORRESPONDE AO BB NO RELATÓRIO CONSIDERAMOS SEMPRE ESTE CAMPO MESMO QUE NÃO TENHAM LIGAÇÃO, DEVIDO À ELIMINAçÂO DE RECEITAS E POSTERIOR INSERÇÃO O TOTAL DA LINHA JÁ É NEGATIVO NAS REGULARIZAÇÔES */
	--	,margem_cIva	=  (FI.ETILIQUIDO + (CASE WHEN TD.tipodoc = 3 THEN /* DEV. A CLIENTE */(Fi.u_ettent1*-1) + (Fi.u_ettent2*-1) ELSE (Fi.u_ettent1 + Fi.u_ettent2) END)
	--						- 
	--						(CASE WHEN TD.tipodoc = 3 THEN /* DEV. A CLIENTE */	(CASE WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt ELSE FI.EPCP * qtt END) * -1 
	--												  ELSE (CASE WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt ELSE FI.EPCP * qtt END) END)) 
		,margem			=  (CASE WHEN Fi.IVAINCL = 0 THEN Fi.ETILIQUIDO ELSE Fi.ETILIQUIDO / ((Fi.iva / 100)+1) END) + (CASE WHEN Ft.tipodoc = 3 THEN CASE WHEN Fi.IVAINCL = 0 THEN u_ettent1 * -1 ELSE (u_ettent1 / ((Fi.iva / 100)+1)) * (-1) END
																																				 ELSE CASE WHEN Fi.IVAINCL = 0 THEN u_ettent1 ELSE (u_ettent1 / ((Fi.iva / 100)+1))END
																														END) + 
																														(CASE WHEN Ft.tipodoc = 3 THEN CASE WHEN Fi.IVAINCL = 0 THEN u_ettent2 * -1 ELSE (u_ettent2 / ((Fi.iva / 100)+1)) * (-1) END
																															   					 ELSE CASE WHEN Fi.IVAINCL = 0 THEN u_ettent2 ELSE (u_ettent2 / ((Fi.iva / 100)+1)) END
																														END)
							- 
							(CASE WHEN TD.tipodoc = 3 THEN /* DEV. A CLIENTE */	(CASE WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt ELSE FI.EPCP * qtt END) * -1 
													  ELSE (CASE WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt ELSE FI.EPCP * qtt END) END)
		/* Calculo Total Valor Descontos */
		,TotalDescontos = CASE WHEN TD.tipodoc = 3 THEN /* DEV. A CLIENTE */ (ABS(CASE WHEN fi.desconto between 0.01 and 99.99 then (fi.epv*fi.qtt)-ABS(fi.etiliquido) WHEN fi.desconto=100 then fi.epv*fi.qtt else 0 END - fi.u_descval))*-1
												   ELSE	(ABS(CASE WHEN fi.desconto between 0.01 and 99.99 then (fi.epv*fi.qtt)-ABS(fi.etiliquido) WHEN fi.desconto=100 then fi.epv*fi.qtt else 0 END -  fi.u_descval))
						  END
		,'ousrhora'			= Ft.ousrhora
		,'ousrinis'			= Ft.ousrinis
		,faminome			= isnull(#tempcteFamilia.nome,'')
		,qtt				=  CASE WHEN TD.tipodoc = 3 THEN -qtt ELSE qtt END
		,fi.ref
		,fi.ivaincl
		,ft.no
		,u_lab
	INTO
		#cteDados_1
	FROM	
		FI (nolock) 
		INNER JOIN FT (nolock) ON FI.FTSTAMP = FT.FTSTAMP
		INNER JOIN TD (nolock) ON TD.NDOC = FT.NDOC
		Left Join #cteSt ON fi.ref = #cteSt.ref
		Left join #tempcteFamilia on fi.familia = #tempcteFamilia.ref
	WHERE	
		(FT.tipodoc ! =4 or u_tipodoc = 4) 
		AND U_TIPODOC <> 1 
		AND U_TIPODOC <> 5 
		AND fdata >= @dataIni and fdata <= @dataFim
		AND fi.composto=0 
		AND ft.anulado=0
		and ft.ousrinis in (select iniciais from #tempcteOperadores)
		and (fi.ref in (Select items from dbo.up_splitToTable(@ref,',')) or @ref = '')
		and isnull(#cteSt.u_lab,'') = CASE When @lab = '' Then isnull(#cteSt.u_lab,'') Else @lab End
		and isnull(#cteSt.usr1,'') = CASE When @marca = '' Then isnull(#cteSt.usr1,'') Else @marca End
		and fi.familia in (Select ref from #tempcteFamilia)
		and ft.site = (case when @site = '' Then ft.site else @site end)
		AND departamento = (CASE WHEN @departamento = '' THEN departamento ELSE @departamento END)
		AND seccao = (CASE WHEN @sessao = '' THEN seccao ELSE  @sessao END)
		AND categoria = (CASE WHEN @categoria = '' THEN categoria ELSE  @categoria END)
		AND segmento = (CASE WHEN @segmento = '' THEN segmento ELSE  @segmento END)
		and fi.design like case when @design = '' then fi.design else @design + '%' end
		and fi.iva = case when @iva = '' then fi.iva else convert(numeric(5,2),@iva) end
		and isnull(#cteSt.generico,0) = case when @generico = '' then isnull(#cteSt.generico,0) else @generico end
	/* Elimina Vendas tendo em conta tipo de cliente*/
	If @Entidades = 1
	delete from #cteDados_1 where #cteDados_1.no < 199
	/* Prepara Result Set */
	IF @agrupaLab = 0
		SELECT	
			UTENTE				= SUM(case when @ivaincl = 1 then etiliquido_cIva else etiliquido_sIva end)
			,COMPARTICIPACAO	= SUM(case when @ivaincl = 1 then ettent1 else u_ettent1_sIva end) + SUM(case when @ivaincl = 1 then ettent2 else u_ettent2_sIva end)
			,TOTAL				= SUM(case when @ivaincl = 1 then etiliquido_cIva else etiliquido_sIva end) + SUM(case when @ivaincl = 1 then ettent1 else u_ettent1_sIva end) + SUM(case when @ivaincl = 1 then ettent2 else u_ettent2_sIva end)
			,TOTAL_sIVA			= SUM(etiliquido_sIva) + SUM(u_ettent1_sIva) + SUM(u_ettent2_sIva)
			,EPVP				= ROUND(SUM(u_epvp),2)
			,EPCP				= ROUND(SUM(epcpond),2)
			,MARGEM				= ROUND(SUM(isnull(margem,0)),2)
			,DescCom			= SUM(TotalDescontos)
			,DescVale			= SUM(u_descval)
			,ousrinis			= ousrinis + '-' +(SElect top 1 username from b_us (nolock) Where b_us.iniciais = a.ousrinis)
			,faminome
			,nrVendas			= SUM(NrVendas)
			,nrDev				= SUM(NrDev)
			,qttVendida			= sum(qtt)
			--,u_lab
		into
			#cteDados_2	
		FROM
			#cteDados_1 a
		GROUP BY 
			ousrinis, faminome
	else
		SELECT	
			UTENTE				= SUM(case when @ivaincl = 1 then etiliquido_cIva else etiliquido_sIva end)
			,COMPARTICIPACAO	= SUM(case when @ivaincl = 1 then ettent1 else u_ettent1_sIva end) + SUM(case when @ivaincl = 1 then ettent2 else u_ettent2_sIva end)
			,TOTAL				= SUM(case when @ivaincl = 1 then etiliquido_cIva else etiliquido_sIva end) + SUM(case when @ivaincl = 1 then ettent1 else u_ettent1_sIva end) + SUM(case when @ivaincl = 1 then ettent2 else u_ettent2_sIva end)
			,TOTAL_sIVA			= SUM(etiliquido_sIva) + SUM(u_ettent1_sIva) + SUM(u_ettent2_sIva)
			,EPVP				= ROUND(SUM(u_epvp),2)
			,EPCP				= ROUND(SUM(epcpond),2)
			,MARGEM				= ROUND(SUM(isnull(margem,0)),2)
			,DescCom			= SUM(TotalDescontos)
			,DescVale			= SUM(u_descval)
			,ousrinis			= ousrinis + '-' +(SElect top 1 username from b_us (nolock) Where b_us.iniciais = a.ousrinis)
			--,faminome
			,nrVendas			= SUM(NrVendas)
			,nrDev				= SUM(NrDev)
			,qttVendida			= sum(qtt)
			,faminome			= u_lab
		into
			#cteDados_3
		FROM
			#cteDados_1 a
		GROUP BY 
			ousrinis, u_lab
	-- result set final	
	IF @agrupaLab = 0
		Select
			MARGEM_PERC = CASE WHEN TOTAL_sIVA <= 0 or MARGEM < 0 then 0 else MARGEM*100/TOTAL_sIVA end
			,*
		From
			#cteDados_2
	ELSE
		Select
			MARGEM_PERC = CASE WHEN TOTAL_sIVA <= 0 or MARGEM < 0 then 0 else MARGEM*100/TOTAL_sIVA end
		,*
		From
			#cteDados_3
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempCteNumAtend'))
		DROP TABLE #tempCteNumAtend
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempcteFamilia'))
		DROP TABLE #tempcteFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempcteOperadores'))
		DROP TABLE #tempcteOperadores
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteSt'))
		DROP TABLE #cteSt
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteSeries'))
		DROP TABLE #cteSeries
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteDados_1'))
		DROP TABLE #cteDados_1
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteDados_2'))
		DROP TABLE #cteDados_2
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteDados_3'))
		DROP TABLE #cteDados_3
END


GO
Grant Execute on dbo.up_relatorio_gestao_operadorVendas to Public
Grant control on dbo.up_relatorio_gestao_operadorVendas to Public
GO