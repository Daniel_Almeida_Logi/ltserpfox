/*
	exec up_relatorio_centralFechosCaixas '20230101', '20230901', 'ATLANTICO'
	exec up_relatorio_centralFechosCaixas '20230501', '20230901', 'Loja 1'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_centralFechosCaixas]') IS NOT NULL
	drop procedure dbo.up_relatorio_centralFechosCaixas
go

create procedure dbo.up_relatorio_centralFechosCaixas
	@dataIni	DATETIME,
	@dataFim	DATETIME,
	@site		VARCHAR(254)
/* with encryption */
AS
BEGIN

select 
			  Loja				=	'Loja'					
			, Data				=	'Data'					
			, Utilizador		=	'Utilizador'			
			, fundoCaixa		=	'fundoCaixa'			
			, Num				=	'Num'					
			, Num_Conta			=	'Num_Conta'			
			, Dif_Num			=	'Dif_Num'				
			, MB				=	'MB'					
			, MB_Conta			=	'MB_Conta'				
			, Dif_MB			=	'Dif_MB'				
			, Cfidel			=	'Cfidel'				
			, Cfidel_Conta		=	'Cfidel_Conta'			
			, Dif_Cfidel		=	'Dif_Cfidel'			
			, Transf			=	'Transf'				
			, Transf_Conta		=	'Transf_Conta'
			, Dif_Transf		=	'Dif_Transf'	
			, RefMB				=	'RefMB'
			, RefMB_conta		=	'RefMB_conta'
			, USD				=	'USD'
			, USD_conta			=	'USD_conta'
			, EUR				=	'EUR'
			, EUR_conta			=	'EUR_conta'					
			, Devol				=	'Devol'				
			, Devol_Conta		=	'Devol_Conta'			
			, Dif_Devol			=	'Dif_Devol'			
			, Creditos			=	'Creditos'				
			, Creditos_Conta	=	'Creditos_Conta'		
			, Dif_Creditos		=	'Dif_Creditos'			
	 		, Dif_Total			=	'Dif_Total'			
Union all
	select 
		Loja		= b.site					
		,Data		= convert(varchar(12),b.dabrir)
		,Utilizador = b.ausername																	
		,fundoCaixa = convert(varchar(100),(select SUM(ss.fundoCaixa)
			from ss (nolock) ss
			where ss.fechada = 1
				and ss.dabrir = b.dabrir
				and ss.dabrir between @dataini and @DataFim
				and ss.ausername = b.ausername
				and ss.site = b.site
				and ss.site in (case when @site = '' Then ss.site else (Select items from dbo.up_splitToTable(@site, ',')) end)
				and ss.ssstamp = (case when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
									then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
									else ss.ssstamp
									end)))
		,Num = convert(varchar(100),sum(p.evdinheiro)) 													
		,Num_Conta = convert(varchar(100),(select SUM(ss.evdinheiro) - SUM(ss.fundoCaixa)
			from ss (nolock) ss
			where ss.fechada = 1
				and ss.dabrir = b.dabrir
				and ss.dabrir between @dataini and @DataFim
				and ss.ausername = b.ausername
				and ss.site = b.site
				and ss.site in (case when @site = '' Then ss.site else (Select items from dbo.up_splitToTable(@site, ',')) end)
				and ss.ssstamp = (case when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
									then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
									else ss.ssstamp
									end)))
		,Dif_Num = convert(varchar(100),((select SUM(ss.evdinheiro) - SUM(ss.fundoCaixa)
			from ss (nolock) ss
			where ss.fechada = 1
				and ss.dabrir = b.dabrir
				and ss.dabrir between @dataini and @DataFim
				and ss.ausername = b.ausername
				and ss.site = b.site
				and ss.site in (case when @site = '' Then ss.site else (Select items from dbo.up_splitToTable(@site, ',')) end)
				and ss.ssstamp = (case when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
									then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
									else ss.ssstamp
									end)) - sum(p.evdinheiro))) 
																							
		,MB = convert(varchar(100),sum(p.epaga2)) 																		
		,MB_Conta = convert(varchar(100),(select sum(ss.epaga1) 
			from ss (nolock) ss
			where ss.fechada = 1
				and ss.dabrir = b.dabrir
				and ss.dabrir between @dataini and @DataFim
				and ss.ausername = b.ausername
				and ss.site = b.site
				and ss.site in (case when @site = '' Then ss.site else (Select items from dbo.up_splitToTable(@site, ',')) end)
				and ss.ssstamp = (case when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
									then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
									else ss.ssstamp
									end)))
		,Dif_MB = convert(varchar(100),(sum(p.epaga2) - (select sum(ss.epaga1) 
			from ss (nolock) ss
			where ss.fechada = 1
				and ss.dabrir = b.dabrir
				and ss.dabrir between @dataini and @DataFim
				and ss.ausername = b.ausername
				and ss.site = b.site
				and ss.site in (case when @site = '' Then ss.site else (Select items from dbo.up_splitToTable(@site, ',')) end)
				and ss.ssstamp = (case when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
									then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
									else ss.ssstamp
									end))))
																						
		,Cfidel = convert(varchar(100),sum(p.epaga1)) 																		
		,Cfidel_Conta = convert(varchar(100),(select sum(ss.epaga2)
			from ss (nolock) ss
			where ss.fechada = 1
				and ss.dabrir = b.dabrir
				and ss.dabrir between @dataini and @DataFim
				and ss.ausername = b.ausername
				and ss.site = b.site
				and ss.site in (case when @site = '' Then ss.site else (Select items from dbo.up_splitToTable(@site, ',')) end)
				and ss.ssstamp = (case 	when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
										then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
										else ss.ssstamp
                                        end)))
		,Dif_Cfidel = 	convert(varchar(100),( sum(p.epaga1)  - (select sum(ss.epaga2)
			from ss (nolock) ss
			where ss.fechada = 1
				and ss.dabrir = b.dabrir
				and ss.dabrir between @dataini and @DataFim
				and ss.ausername = b.ausername
				and ss.site = b.site
				and ss.site in (case when @site = '' Then ss.site else (Select items from dbo.up_splitToTable(@site, ',')) end)
				and ss.ssstamp = (case 	when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
										then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
										else ss.ssstamp
                                        end))))							
																						
		,Transf = convert(varchar(100),sum(p.echtotal)) 																	
		,Transf_Conta = convert(varchar(100),(select sum(ss.echtotal)
			from ss (nolock) ss
			where ss.fechada = 1
				and ss.dabrir = b.dabrir
				and ss.dabrir between @dataini and @DataFim
				and ss.ausername = b.ausername
				and ss.site = b.site
				and ss.site in (case when @site = '' Then ss.site else (Select items from dbo.up_splitToTable(@site, ',')) end)
				and ss.ssstamp = (case 	when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
										then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
										else ss.ssstamp
                                        end)))
		,Dif_Transf = 	convert(varchar(100),(sum(p.echtotal)  - (select sum(ss.echtotal)
			from ss (nolock) ss
			where ss.fechada = 1
				and ss.dabrir = b.dabrir
				and ss.dabrir between @dataini and @DataFim
				and ss.ausername = b.ausername
				and ss.site = b.site
				and ss.site in (case when @site = '' Then ss.site else (Select items from dbo.up_splitToTable(@site, ',')) end)
				and ss.ssstamp = (case 	when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
										then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
										else ss.ssstamp
                                        end))))							
																						
		,RefMB = convert(varchar(100),sum(p.epaga3)) 																		
		,RefMB_conta = convert(varchar(100),(select sum(ss.fecho_epaga3)
			from ss (nolock) ss
			where ss.fechada = 1
				and ss.dabrir = b.dabrir
				and ss.dabrir between @dataini and @DataFim
				and ss.ausername = b.ausername
				and ss.site = b.site
				and ss.site in (case when @site = '' Then ss.site else (Select items from dbo.up_splitToTable(@site, ',')) end)
				and ss.ssstamp = (case 	when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
										then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
										else ss.ssstamp
                                        end)))												
		,USD = convert(varchar(100),sum(p.epaga4)) 																		
		,USD_conta = convert(varchar(100),(select sum(ss.fecho_epaga4)
			from ss (nolock) ss
			where ss.fechada = 1
				and ss.dabrir = b.dabrir
				and ss.dabrir between @dataini and @DataFim
				and ss.ausername = b.ausername
				and ss.site = b.site
				and ss.site in (case when @site = '' Then ss.site else (Select items from dbo.up_splitToTable(@site, ',')) end)
				and ss.ssstamp = (case 	when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
										then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
										else ss.ssstamp
                                        end)))											
		,EUR = convert(varchar(100),sum(p.epaga5)) 																		
		,EUR_conta = convert(varchar(100),(select sum(ss.fecho_epaga5)
			from ss (nolock) ss
			where ss.fechada = 1
				and ss.dabrir = b.dabrir
				and ss.dabrir between @dataini and @DataFim
				and ss.ausername = b.ausername
				and ss.site = b.site
				and ss.site in (case when @site = '' Then ss.site else (Select items from dbo.up_splitToTable(@site, ',')) end)
				and ss.ssstamp = (case 	when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
										then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
										else ss.ssstamp
                                        end)))												
		,Devol = convert(varchar(100),sum(p.devolucoes)) 																	
		,Devol_Conta = convert(varchar(100),(select sum(ss.devolucoes)
			from ss (nolock) ss
			where ss.fechada = 1
				and ss.dabrir = b.dabrir
				and ss.dabrir between @dataini and @DataFim
				and ss.ausername = b.ausername
				and ss.site = b.site
				and ss.site in (case when @site = '' Then b.site else (Select items from dbo.up_splitToTable(@site, ',')) end)
				and ss.ssstamp = (case	when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
										then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
										else ss.ssstamp
                                        end)))
		,Dif_Devol = convert(varchar(100),(sum(p.devolucoes) - (select sum(ss.devolucoes)
			from ss (nolock) ss
			where ss.fechada = 1
				and ss.dabrir = b.dabrir
				and ss.dabrir between @dataini and @DataFim
				and ss.ausername = b.ausername
				and ss.site = b.site
				and ss.site in (case when @site = '' Then b.site else (Select items from dbo.up_splitToTable(@site, ',')) end)
				and ss.ssstamp = (case	when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
										then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
										else ss.ssstamp
                                        end))))								
																						
		,Creditos = convert(varchar(100),sum(p.creditos)) 																	
		,Creditos_Conta = convert(varchar(100),(select sum(ss.creditos)
			from ss (nolock) ss
			where ss.fechada = 1
				and ss.dabrir = b.dabrir
				and ss.dabrir between @dataini and @DataFim 
				and ss.ausername = b.ausername
				and ss.site = b.site
				and ss.site in (case when @site = '' Then ss.site else (Select items from dbo.up_splitToTable(@site, ',')) end)
				and ss.ssstamp = (case 	when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
										then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
                                        else ss.ssstamp
                                        end)))
		,Dif_Creditos =  convert(varchar(100),(sum(p.creditos) - (select sum(ss.creditos)
			from ss (nolock) ss
			where ss.fechada = 1
				and ss.dabrir = b.dabrir
				and ss.dabrir between @dataini and @DataFim 
				and ss.ausername = b.ausername
				and ss.site = b.site
				and ss.site in (case when @site = '' Then ss.site else (Select items from dbo.up_splitToTable(@site, ',')) end)
				and ss.ssstamp = (case 	when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
										then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
                                        else ss.ssstamp
                                        end))))
		,Dif_Total = convert(varchar(100),((sum(p.evdinheiro) + sum(p.epaga2) + sum(p.epaga1) + sum(p.echtotal) - sum(p.devolucoes))	-
						((select SUM(ss.evdinheiro) - SUM(ss.fundoCaixa)
			from ss (nolock) ss
			where ss.fechada = 1
				and ss.dabrir = b.dabrir
				and ss.dabrir between @dataini and @DataFim
				and ss.ausername = b.ausername
				and ss.site = b.site
				and ss.site in (case when @site = '' Then ss.site else (Select items from dbo.up_splitToTable(@site, ',')) end)
				and ss.ssstamp = (case when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
									then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
									else ss.ssstamp
									end)) +
									(select sum(ss.epaga1) 
			from ss (nolock) ss
			where ss.fechada = 1
				and ss.dabrir = b.dabrir
				and ss.dabrir between @dataini and @DataFim
				and ss.ausername = b.ausername
				and ss.site = b.site
				and ss.site in (case when @site = '' Then ss.site else (Select items from dbo.up_splitToTable(@site, ',')) end)
				and ss.ssstamp = (case when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
									then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
									else ss.ssstamp
									end)) +
									(select sum(ss.epaga2)
			from ss (nolock) ss
			where ss.fechada = 1
				and ss.dabrir = b.dabrir
				and ss.dabrir between @dataini and @DataFim
				and ss.ausername = b.ausername
				and ss.site = b.site
				and ss.site in (case when @site = '' Then ss.site else (Select items from dbo.up_splitToTable(@site, ',')) end)
				and ss.ssstamp = (case 	when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
										then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
										else ss.ssstamp
                                        end)) +
										(select sum(ss.echtotal)
			from ss (nolock) ss
			where ss.fechada = 1
				and ss.dabrir = b.dabrir
				and ss.dabrir between @dataini and @DataFim
				and ss.ausername = b.ausername
				and ss.site = b.site
				and ss.site in (case when @site = '' Then ss.site else (Select items from dbo.up_splitToTable(@site, ',')) end)
				and ss.ssstamp = (case 	when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
										then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
										else ss.ssstamp
                                        end)) -
										(select sum(ss.devolucoes)
			from ss (nolock) ss
			where ss.fechada = 1
				and ss.dabrir = b.dabrir
				and ss.dabrir between @dataini and @DataFim
				and ss.ausername = b.ausername
				and ss.site = b.site
				and ss.site in (case when @site = '' Then b.site else (Select items from dbo.up_splitToTable(@site, ',')) end)
				and ss.ssstamp = (case	when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
										then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
										else ss.ssstamp
                                        end)))))										
		from ss (nolock) b 
		inner join b_pagcentral(NOLOCK) p
		on  p.ssstamp = b.ssstamp
		where b.fechada = 1
			and b.site in (case when @site = '' Then b.site else (Select items from dbo.up_splitToTable(@site, ',')) end)
			and p.site in (case when @site = '' Then b.site else (Select items from dbo.up_splitToTable(@site, ',')) end)
			and b.dabrir between @dataini and @DataFim 
		group by b.site,b.dabrir, b.ausername 
		--order by Loja asc,Data asc, Utilizador  asc

END

GO
Grant Execute on dbo.up_relatorio_centralFechosCaixas to Public
Grant control on dbo.up_relatorio_centralFechosCaixas to Public
GO