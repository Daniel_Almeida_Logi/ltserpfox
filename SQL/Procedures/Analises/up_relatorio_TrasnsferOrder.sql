/* Relatório transferências entre farmácias Grupo 

	exec up_relatorio_TrasnsferOrder '19000101', '20190601', 13,'Loja 1'
	
*/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_relatorio_TrasnsferOrder]') IS NOT NULL
    drop procedure up_relatorio_TrasnsferOrder
go

create PROCEDURE up_relatorio_TrasnsferOrder
@dataIni			datetime
,@dataFim			datetime
,@fornDestinoNo		varchar(5)
,@site				varchar(60)
/* WITH ENCRYPTION */
AS
	Select 
		bo.nmdos
		,bo.obrano
		,bo.dataobra
		,fornecedor = bo.nome
		,bi.ref
		,bi.design
		,bi.qtt
		,bi.ettdeb
		,morada = bo2.contacto
		,noFornDestino = substring(bo2.morada,charindex('[',bo2.morada)+1,charindex(']',bo2.morada) - case when charindex(']',bo2.morada) > 0 then +2 else 0 end)
		,nomeFornDestino = isnull((select top 1 nome from fl (nolock) where RTRIM(LTRIM(STR(no))) = substring(bo2.morada,charindex('[',bo2.morada)+1,charindex(']',bo2.morada) - case when charindex(']',bo2.morada) > 0 then +2 else 0 end) and estab = 0),'')
	from 
		bo (nolock) 
		inner join bo2 on bo2stamp = bostamp
		inner join bi (nolock) on bi.bostamp = bo.bostamp
	where 
		bo.ndos = 17 /*Devoluções a Fornecedor*/
		and bo.dataobra between @dataIni and @dataFim
		and bo.site = @site
		and @fornDestinoNo = case when @fornDestinoNo = 0 then @fornDestinoNo
								else substring(bo2.morada,charindex('[',bo2.morada)+1,charindex(']',bo2.morada) - case when charindex(']',bo2.morada) > 0 then +2 else 0 end) END

GO
Grant Execute On up_relatorio_TrasnsferOrder to Public
Grant Control On up_relatorio_TrasnsferOrder to Public
go
