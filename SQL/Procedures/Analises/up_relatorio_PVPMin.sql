
/* Relatório Vendas 

exec up_relatorio_PVPMin '20220624','20220701','','','Loja 1','','','',0,'>',0
exec up_relatorio_PVPMin '20220501','20220621','','','Loja 1','','','',1,'>',-99999
exec up_relatorio_PVPMin '20220621','20220701','','','Loja 1','','','',1,'>',0


exec up_relatorio_PVPMin '20220607','20220701','','','Loja 1','','','',1,'>=',0

exec up_relatorio_PVPMin '19000101','20220621','','','Loja 1','','','',0,'>',0
exec up_relatorio_PVPMin '19000101','20220621','','','Loja 1','','','',1,'>',0
select u_famstamp,* from st where u_famstamp!=''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_relatorio_PVPMin]') IS NOT NULL
DROP PROCEDURE dbo.up_relatorio_PVPMin
GO

CREATE PROCEDURE up_relatorio_PVPMin
	@dataIni 		DATE,
	@datafim		DATE ,
	@marca			VARCHAR (200)  = '',
	@design 		VARCHAR (254) = '',
	@site			VARCHAR(254) ,
	@tipoProduto	VARCHAR (254),
	@lab			VARCHAR (20) ,
	@ref			VARCHAR (20) = '',
	@mostraProdutosNIncluidoVendasTag	as bit = false,
	@mostraProdutosNIncluidoVendasOp	as varchar(2)='',
	@mostraProdutosNIncluidoVendas		as varchar(10)=''


/* WITH ENCRYPTION */
AS
Begin

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#altpreco'))
		DROP TABLE #altpreco

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFiltrados'))
		DROP TABLE #dadosFiltrados

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#linhasvendas'))
	DROP TABLE #linhasvendas

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#final'))
	DROP TABLE #final



--Criação Tabela Histórico PVP c/ Row number para PVP mais baixo e Data alt mais atual
select		CONVERT(DATE,hist.dataaltpvp)											as 'Data Alteração PVP'
			,hist.dpvp																as 'Novo PVP'
			,hist.opvp																as 'Anterior PVP'
			,hist.ref																as 'CNP'
			,st.design																as 'Descrição' 
			--,ROW_NUMBER() OVER(PARTITION BY hist.ref ORDER BY hist.dpvp asc)		as 'NPreco' 
			,ROW_NUMBER() OVER(PARTITION BY hist.ref ORDER BY hist.dataaltpvp desc)	as 'NAltPVP' 
			,st.stock																--adicionado
into		#altpreco	
from		b_historicopvp hist (nolock) 
inner join  empresa (nolock) on empresa.site = hist.site
inner join  st		(nolock) on st.ref = hist.ref and st.site_nr =empresa.no
left join b_famFamilias (nolock) on st.u_famstamp=b_famFamilias.famstamp
	where	hist.ref != '' and hist.ref not like 'V000%' --and hist.ref = '9998914'
	  and	hist.dpvp != 0
	 -- and	hist.site = @site
	  and	(st.usr1 = @marca or  @marca = '')

			and	st.design like 
					case 
					when @design != '' then '%' + @design + '%'
					else st.design 
					end
			--and	isnull(b_famFamilias.design, '') = case when @tipoProduto = '' then isnull(b_famFamilias.design, '') else @tipoProduto end
			and	(isnull(b_famFamilias.design,'') in (Select items from dbo.up_splitToTable(@tipoProduto,',')) or @tipoProduto = '') 
			--and	(st.ref = @ref or @ref = '')
			and (st.ref in (Select items from dbo.up_splitToTable(@ref,',')) or @ref = '')
			and	(st.u_lab = @lab or @lab='')
			and empresa.site in (Select items from dbo.up_splitToTable(@site, ',')) 
		
	--	select * from #altpreco where NAltPVP = 1

--Criar tabela com linhas de vendas, entre datas, com Row number para o PVP de venda mais baixo 

if(@datafim='' or @datafim='19000101')
begin
set @datafim =getdate()

end

select	fi.ref																			AS 'CNP'
		,fi.design																		AS 'Descrição'
		,fi.etiliquido																	AS 'PVP_min'
		,fi.rdata																		AS 'Data_Venda_PVP_Min'
		,fi.u_epvp																		AS 'PVP_Ficha_fi'
		,fi.campanhas																	AS 'Campanhas'
		,ROW_NUMBER() OVER(PARTITION BY fi.ref ORDER BY fi.etiliquido asc, fi.rdata desc)	AS 'NrLinha'
	into #linhasvendas	
	from fi (nolock) 
		inner join ft		(nolock) on ft.ftstamp = fi.ftstamp
		inner join empresa	(nolock) on ft.site = empresa.site
		inner join st		(nolock) on st.ref = fi.ref and st.site_nr =empresa.no
		inner join fprod	(nolock) on fprod.ref = st.ref
		left join b_famFamilias (nolock) on st.u_famstamp=b_famFamilias.famstamp
	where		fi.ref != 'R000001'
			and fi.etiliquido > 0
			and	(st.usr1 = @marca or  @marca = '')
			and	st.design like 
					case 
					when @design != '' then '%' + @design + '%'
					else st.design 
					end
			and	(isnull(b_famFamilias.design,'') in (Select items from dbo.up_splitToTable(@tipoProduto,',')) or @tipoProduto = '') 
			--and	(st.ref = @ref or @ref = '')
			and (st.ref in (Select items from dbo.up_splitToTable(@ref,',')) or @ref = '')
			and	(st.u_lab = @lab or @lab='')
			and fi.rdata >= @dataIni  and fi.rdata <=@datafim
			--and ft.site=@site
			and fprod.id_grande_mercado_hmr != 1
			and empresa.site in (Select items from dbo.up_splitToTable(@site, ',')) 
	--group by fi.design, fi.ref, fi.campanhas, fi.u_epvp, fi.rdata, fi.etiliquido
	order by fi.design

	--select * from #linhasvendas

--Tabela para filtrar a anterior apenas para dar o PVP mais baixo para os CNPs vendidos entre datas
	select * 
	into #final
	from #linhasvendas 
	where NrLinha = 1

	--select * from #final

		/* create #dadosSTFiltrado */
			select top 0
				*
			into
				#dadosFiltrados
			From #altpreco

		
	--Criacao do campo inmcluir nao vendidos 
	IF @mostraProdutosNIncluidoVendasTag = 1 
		BEGIN 

		if  @mostraProdutosNIncluidoVendasOp = '<'
		begin 
			insert into #dadosFiltrados
			select * from #altpreco
			where
				 #altpreco.stock < @mostraProdutosNIncluidoVendas
		End
		else if @mostraProdutosNIncluidoVendasOp = '='
		begin 
			insert into #dadosFiltrados
			select * from #altpreco
			where
				 #altpreco.stock = @mostraProdutosNIncluidoVendas
		End
		else if @mostraProdutosNIncluidoVendasOp = '>'
		begin 
			insert into #dadosFiltrados
			select * from #altpreco 
			where
				 #altpreco.stock >= @mostraProdutosNIncluidoVendas
		End
		else if @mostraProdutosNIncluidoVendasOp = '>='
		begin 
			insert into #dadosFiltrados
			select * from #altpreco
			where
				 #altpreco.stock >= @mostraProdutosNIncluidoVendas
		End
		else if @mostraProdutosNIncluidoVendasOp = '<='
		begin 
			insert into #dadosFiltrados
			select * from #altpreco
			where
				 #altpreco.stock <= @mostraProdutosNIncluidoVendas
		End
		else
		begin
			insert into #dadosFiltrados
			select * from #altpreco
		end

			select 
				 ref
				,design
				,PVP_min
				,Data_VD_PVP_MIN
				,PVP_Ficha
				,Data_Alt_PVP_Ficha
			from (
					select 	#dadosFiltrados.CNP																			AS 'ref'
					,#dadosFiltrados.Descrição																	AS 'design'
					,case 
						when #final.PVP_Min < #dadosFiltrados.[Novo PVP]
						then CONVERT(DECIMAL(10,2),#final.PVP_Min) 
						else CONVERT(DECIMAL(10,2),#dadosFiltrados.[Novo PVP])end									 AS 'PVP_min'
			--		,CONVERT(DECIMAL(10,2),#final.PVP_Min)													 AS 'PVP_Min_VD'
					,CONVERT(DATE,#final.[Data_Venda_PVP_Min])												 AS 'Data_VD_PVP_MIN'
					,CONVERT(DECIMAL(10,2),#dadosFiltrados.[Novo PVP])											 AS 'PVP_Ficha'
					,CONVERT(DATE,#dadosFiltrados.[Data Alteração PVP])											 AS 'Data_Alt_PVP_Ficha'
					,#final.Campanhas 
					,#dadosFiltrados.NAltPVP
				from #dadosFiltrados 
				 join #final on  #dadosFiltrados.CNP = #final.CNP --and #dadosFiltrados.[Novo PVP] = #final.PVP_Ficha_fi 
				where #dadosFiltrados.NAltPVP=1
					and #final.Data_Venda_PVP_Min >= @dataIni 
					and #final.Data_Venda_PVP_Min <= @datafim
				--and #altpreco.CNP = '9998914'

				union all 

				select 	#dadosFiltrados.CNP																			AS 'ref'
					,#dadosFiltrados.Descrição																	AS 'design'
					, #dadosFiltrados.[Novo PVP]																	AS 'PVP_min'
				--	,CONVERT(DECIMAL(10,2),#final.PVP_Min)													 AS 'PVP_Min_VD'
					,CONVERT(DATE,'19000101')																AS 'Data_VD_PVP_MIN'
					,CONVERT(DECIMAL(10,2),#dadosFiltrados.[Novo PVP])											 AS 'PVP_Ficha'
					,CONVERT(DATE,#dadosFiltrados.[Data Alteração PVP])											 AS 'Data_Alt_PVP_Ficha'
					,''
					,#dadosFiltrados.NAltPVP
				from #dadosFiltrados 
					LEFT JOIN #final ON #dadosFiltrados.CNP = #final.CNP  
					where #dadosFiltrados.NAltPVP=1 AND #final.CNP IS NULL 
				--and #altpreco.CNP = '9998914'
			) X
			order by X.ref desc

	End
	else
	begin 

		insert into #dadosFiltrados
		select * from #altpreco


		--Tabela final com todos os preços e, onde existam vendas, o preço mais baixo vendido, nas datas selecionadas

		--select * from #dadosFiltrados
		--select * from #final

	select 	#dadosFiltrados.CNP																			AS 'ref'
			,#dadosFiltrados.Descrição																	AS 'design'
			,case 
				when #final.PVP_Min < #dadosFiltrados.[Novo PVP]
				then CONVERT(DECIMAL(10,2),#final.PVP_Min) 
				else CONVERT(DECIMAL(10,2),#dadosFiltrados.[Novo PVP])end									 AS 'PVP_min'
		--	,CONVERT(DECIMAL(10,2),#final.PVP_Min)													 AS 'PVP_Min_VD'
			,CONVERT(DATE,#final.[Data_Venda_PVP_Min])												 AS 'Data_VD_PVP_MIN'
			,CONVERT(DECIMAL(10,2),#dadosFiltrados.[Novo PVP])											 AS 'PVP_Ficha'
			,CONVERT(DATE,#dadosFiltrados.[Data Alteração PVP])											 AS 'Data_Alt_PVP_Ficha'
			,#final.Campanhas 
			,#dadosFiltrados.NAltPVP
	from #dadosFiltrados 
	 join #final on  #dadosFiltrados.CNP = #final.CNP --and #dadosFiltrados.[Novo PVP] = #final.PVP_Ficha_fi 
		where #dadosFiltrados.NAltPVP=1
			and #final.Data_Venda_PVP_Min >= @dataIni 
			and #final.Data_Venda_PVP_Min <= @datafim
		--and #altpreco.CNP = '9998914'
	order by #dadosFiltrados.CNP desc
	end 



	If OBJECT_ID('tempdb.dbo.#altpreco') IS NOT NULL
			drop table #altpreco
	If OBJECT_ID('tempdb.dbo.#linhasvendas') IS NOT NULL
			drop table #linhasvendas
	If OBJECT_ID('tempdb.dbo.#final') IS NOT NULL
			drop table #final
	END
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFiltrados'))
		DROP TABLE #dadosFiltrados
	
GO
Grant Execute on dbo.up_relatorio_PVPMin  to Public
Grant control on dbo.up_relatorio_PVPMin  to Public
GO