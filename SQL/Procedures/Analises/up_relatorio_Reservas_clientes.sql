/* Relatório Reservas Clientes
	 
	 exec up_relatorio_Reservas_clientes '20240806', '20240806', '', 0, 'Loja 1', 0,'','C/Adiantamento'
	 exec up_relatorio_Reservas_clientes '20240806', '20240806', '', 0, 'Loja 1', 0,'','S/Adiantamento'
	  exec up_relatorio_Reservas_clientes '20240806', '20240806', '', 0, 'Loja 1', 0,'',''
	 exec up_relatorio_Reservas_clientes '', '20231221', '', 0, 'Loja 1', 1,'',''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_relatorio_Reservas_clientes]') IS NOT NULL
	DROP procedure dbo.up_relatorio_Reservas_clientes
GO

CREATE procedure dbo.up_relatorio_Reservas_clientes
	@dataIni		AS DATETIME,
	@dataFim		AS DATETIME,
	@utente			AS VARCHAR(254),
	@sopendentes	AS INT,
	@site			AS VARCHAR(30),
	@rotacao		AS INT, 
	@op				AS VARCHAR(20) = 0,
	@adiantamento	AS VARCHAR(254) = ''
	  

/* WITH ENCRYPTION */ 

AS
BEGIN

	Declare @xWhereOp varchar(100) = ''

-- C/Adiantamento
-- S/Adiantamento

	IF @rotacao = 0
	begin
		-- Result set Final
		SELECT	
			bi.REF
			,bi.DESIGN
			,vendnm = b_us.nome
			,qtt = SUM(bi.QTT)
			,qtt2 = SUM(bi.QTT2)
			,qttpendente = SUM(bi.QTT) - SUM(bi.QTT2)
			,qttfornec = ISNULL(st.qttfor,0) * ISNULL(bi.QTT,0)
			,nome = '[' + convert(varchar(9),bo.no) + '] ' + '[' + convert(varchar(9),bo.estab) + '] ' + bo.nome + ' - ' + b_utentes.tlmvl
			,datareserva = BO.DATAOBRA
			,numreserva = BO.OBRANO
			,datavalidade = BO.DATAFINAL
			,ISNULL(fi.etiliquido,0) as Adiantamento
			,st.stock	as stock
		FROM	
			BO (nolock)
			inner join ts(nolock) on bo.ndos = ts.ndos
			INNER JOIN BI (nolock) ON BI.BOSTAMP = BO.BOSTAMP
			INNER JOIN BI2  (nolock) ON BI.bistamp = BI2.bi2stamp
			inner join empresa(nolock) on bo.site= empresa.site
			inner join st(nolock) on bi.ref = st.ref and st.site_nr= empresa.no
			left join b_utentes (nolock) on b_utentes.no = bo.no and b_utentes.estab = bo.estab
			left join b_us (nolock) on bo.ousrinis = b_us.iniciais
			left join fi (nolock) on fi.fistamp = BI2.fistamp and BI2.fistamp != ''
		WHERE	
			ts.codigoDoc = 51
			AND BO.FECHADA = case when @sopendentes = 0 then bo.fechada else 0 end
			AND BO.DATAOBRA BETWEEN @dataIni AND @dataFim
			and '[' + convert(varchar(9),bo.no) + '] ' + '[' + convert(varchar(9),bo.estab) + '] ' + bo.nome = case when @utente = '' then  '[' + convert(varchar(9),bo.no) + '] ' + '[' + convert(varchar(9),bo.estab) + '] ' + bo.nome else @utente end 
			and bo.site = @site   
			and b_us.userno >= case when @op = '' or @op = '0' then  0 else @op end
			and b_us.userno <= case when @op = '' or @op = '0' then (select max(b_us.userno) from b_us(nolock)) else @op end 
			AND (
				(@adiantamento = 'C/Adiantamento' AND fi.etiliquido > 0)
				OR (@adiantamento = 'S/Adiantamento' AND ISNULL(fi.etiliquido,0) = 0)
				OR (@adiantamento = '')
				 )
		GROUP BY 
			bi.REF,bi.DESIGN,BO.NOME,BO.NO,BO.ESTAB, BO.DATAFINAL,BO.DATAOBRA,BO.OBRANO,b_utentes.tlmvl,BI2.fistamp,b_us.nome, st.stock, st.qttfor ,bi.QTT
			,fi.etiliquido
		ORDER BY 
			BO.OBRANO,bo.NO,bo.NOME,bo.ESTAB,DESIGN		
	END
	ELSE
		-- Result set Final
		SELECT	
			bi.REF
			,bi.DESIGN
			,vendnm = case when @op = '' or @op = '0' then 'Todos' else b_us.nome end 
			,qtt = SUM(bi.QTT)
			,qtt2 = SUM(bi.QTT2)
			,qttpendente = SUM(bi.QTT) - SUM(bi.QTT2)
			,qttfornec = ISNULL(st.qttfor,0)
			,nome = 'Rotação Produto'
			,datareserva = '19000101'
			,numreserva = '0'
			,datavalidade = '19000101'
			,Adiantamento = 0
			,st.stock	as stock
		FROM	
			BO (nolock)
			inner join ts(nolock) on bo.ndos = ts.ndos
			INNER JOIN BI (nolock) ON BI.BOSTAMP = BO.BOSTAMP 
			INNER JOIN empresa(nolock) on bo.site= empresa.site
			INNER JOIN st(nolock) on bi.ref = st.ref and st.site_nr= empresa.no
			left join b_us (nolock) on bo.ousrinis = b_us.iniciais
		WHERE	
			ts.codigoDoc = 51
			AND BO.DATAOBRA BETWEEN @dataIni AND @dataFim
			and bo.site = @site
			and b_us.userno >= case when @op = '' or @op = '0' then  0 else @op end
			and b_us.userno <= case when @op = '' or @op = '0' then (select max(b_us.userno) from b_us(nolock)) else @op end 
		GROUP BY 
			bi.REF,bi.DESIGN,BO.NOME,BO.NO,BO.ESTAB, BO.DATAFINAL,BO.DATAOBRA,BO.OBRANO,b_us.nome, st.stock, st.qttfor ,bi.QTT
			,b_us.userno
		ORDER BY 
			b_us.userno, qtt desc, bi.design	

END

GO
GRANT EXECUTE on dbo.up_relatorio_Reservas_clientes TO PUBLIC
GRANT CONTROL on dbo.up_relatorio_Reservas_clientes TO PUBLIC
GO