/* Relatório mapa de presenca colaborador Por dia

	exec up_relatorio_presenca_colaborador_dias '20230101','20230112','4','Loja 1'
		exec up_relatorio_presenca_colaborador_dias '20200401','20200430','51','Loja 1'

	select * from b_us
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_presenca_colaborador_dias]') IS NOT NULL
	drop procedure dbo.up_relatorio_presenca_colaborador_dias
go

create procedure dbo.up_relatorio_presenca_colaborador_dias
@dataIni		as datetime,
@dataFim		as datetime,
@op				as numeric(9,0),
@site			as varchar(55)
/* with encryption */
AS 
SET NOCOUNT ON
BEGIN 

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosHorario'))
		DROP TABLE #dadosHorario

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosPicaPonto'))
		DROP TABLE #dadosPicaPonto

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosPeriodo'))
		DROP TABLE #dadosPeriodo

DECLARE @timeMsExtra int 
DECLARE @timeMsFalta int
DECLARE @timeMsAssiduidade int
DECLARE @site_nr		int

	SELECT  @site_nr=no from empresa(nolock) where site = @site

	;WITH DateRange(DateData) AS 
	(
		SELECT @dataIni as Date
		UNION ALL
		SELECT 
		DATEADD(d,1,DateData) as DateData
		FROM DateRange 
		WHERE DateData < @dataFim
	)
	SELECT 
		convert(varchar,DateData,102) as date,
		DATENAME(weekday,DateData)  AS WEEKEND,
		row = row_number() over(partition by DateData order by DateData,horaInicio ASC),
		CONVERT(char(8),CONVERT( TIME(0), horaInicio), 108) horaInicio,
		CONVERT(char(8),CONVERT( TIME(0), horafim), 108) horafim,
		no as op
	INTO #dadosHorario
	FROM DateRange
	INNER JOIN b_series  ON ((DATENAME(weekday,DateData) = (case when (b_series.segunda=1) then 'Monday' end ) and  dataIniRep between @dataIni and @dataFim and   dataFimRep >= @dataIni and no=@op)
							or( DATENAME(weekday,DateData) =	(case when (b_series.terca=1) then 'Tuesday' end )and dataIniRep between @dataIni and @dataFim and   dataFimRep >= @dataIni and no=@op)
							or (DATENAME(weekday,DateData) = (case when (b_series.quarta=1) then 'Wednesday' end )and dataIniRep between @dataIni and @dataFim and   dataFimRep >= @dataIni and no=@op)
							or (DATENAME(weekday,DateData) = (case when (b_series.quinta=1) then 'Thursday' 	end )and dataIniRep between @dataIni and @dataFim and   dataFimRep >= @dataIni and no=@op)											
							or (DATENAME(weekday,DateData) =	(case when (b_series.sexta=1) then 'Friday' 	end )and dataIniRep between @dataIni and @dataFim and   dataFimRep >= @dataIni and no=@op)											
							or (DATENAME(weekday,DateData) =	(case when (b_series.sabado=1) then 'Saturday' end )and dataIniRep between @dataIni and @dataFim and   dataFimRep >= @dataIni and no=@op)												
							or (DATENAME(weekday,DateData) =	(case  when (b_series.domingo=1) then 'Sunday' 	end )and dataIniRep between @dataIni and @dataFim and   dataFimRep >= @dataIni and no=@op))											
	where 
	dataIniRep between @dataIni and @dataFim and   dataFimRep >= @dataIni and no=@op and convert(varchar,DateData,102) between b_series.dataIniRep and b_series.dataFimRep
		and site = @site
	order by DateData asc,
			horaInicio ASC,
			horafim ASC


	SELECT 
		CONVERT(varchar,inOutDate,102) as date,
		CONVERT(char(8), inOutDate, 108)  as entradas,
		(select top 1 convert(char(8), inOutDate, 108) from clock_response_payload_line as outdate where outdate.inOutCode = 1 and convert(varchar,outdate.inOutDate,102) = convert(varchar,line.inOutDate,102) and outdate.inOutDate >= line.inOutDate AND userId=@op  order by outdate.inOutDate asc) as saidas,
		row = row_number() over(partition by convert(varchar,line.inOutDate,102) order by inOutDate ASC),
		userId as op
	INTO
		#dadosPicaPonto
	FROM clock_response_payload_line line
	WHERE line.inOutCode = 0 and line.inOutDate between @dataIni and @dataFim AND userId=@op and siteNr = @site_nr
	ORDER BY  inOutDate ASC

	SELECT 
		*
	into #dadosPeriodo
	FROM (SELECT 
			DAY(#dadosHorario.date)																																													AS dia,
			#dadosHorario.horaInicio																																												AS previstoEntrada,
			#dadosPicaPonto.entradas																																												AS RegistadoEntrada,
			#dadosHorario.horafim																																													AS previstoSaida,
			#dadosPicaPonto.saidas																																													AS RegistadoSaida,
			''																																																		AS Extras,
			''																																																		AS Faltas,
			case when (#dadosHorario.horaInicio <#dadosPicaPonto.entradas )	then 
			--(SELECT CONVERT(VARCHAR(8), DATEADD(MS, DATEDIFF(MS, #dadosHorario.horaInicio, #dadosPicaPonto.entradas), 0), 114) ) 
			(select (case when((select LEN(convert(varchar(8),DateDiff(s, #dadosHorario.horaInicio, #dadosPicaPonto.entradas)/3600)))=1) 
						then  RIGHT('0'+ convert(varchar(8),DateDiff(s, #dadosHorario.horaInicio, #dadosPicaPonto.entradas)/3600),2) 
						else convert(varchar(8),DateDiff(s, #dadosHorario.horaInicio, #dadosPicaPonto.entradas)/3600) end) 
			 + ':'+ RIGHT('0'+ convert(varchar(8),DateDiff(s, #dadosHorario.horaInicio, #dadosPicaPonto.entradas)%3600/60),2)+':'+ RIGHT('0' + convert(varchar(8),(DateDiff(s, #dadosHorario.horaInicio, #dadosPicaPonto.entradas)%60)),2)) 			
			ELSE '' END	AS Assiduidade,
			'TR'																																																	AS tipo,
			#dadosHorario.row																																														AS turno,
			#dadosHorario.op																																														AS operador,
			0																																																		AS registaMSExtra,
			0																																																		AS registaMSFalta,
			DATEDIFF(s, #dadosHorario.horaInicio, #dadosPicaPonto.entradas)																																		    AS registaMSAssiduidade,
			#dadosHorario.date																																														AS date
		FROM 
			#dadosHorario 
			INNER JOIN  #dadosPicaPonto ON (#dadosPicaPonto.date = #dadosHorario.date AND #dadosPicaPonto.row = #dadosHorario.row and #dadosPicaPonto.op = #dadosHorario.op )
	
		UNION ALL 
	
		SELECT 
			DAY(#dadosHorario.date)																																											AS dia,
			#dadosHorario.horaInicio																																										AS previstoEntrada,
			''																																																AS RegistadoEntrada,
			#dadosHorario.horafim																																											AS previstoSaida,
			''																																																AS RegistadoSaida,
			''																																																AS Extras,
			CASE WHEN (#dadosHorario.horaInicio <#dadosHorario.horafim ) THEN 
			--(SELECT CONVERT(VARCHAR(8), DATEADD(MS, DATEDIFF(MS, #dadosHorario.horaInicio, #dadosHorario.horafim), 0), 114) ) 
				(select (case when((select LEN(convert(varchar(8),DateDiff(s, #dadosHorario.horaInicio, #dadosHorario.horafim)/3600)))=1) 
				then  RIGHT('0'+ convert(varchar(8),DateDiff(s, #dadosHorario.horaInicio, #dadosHorario.horafim)/3600),2) 
				else convert(varchar(8),DateDiff(s, #dadosHorario.horaInicio, #dadosHorario.horafim)/3600) end) 
			 + ':'+ RIGHT('0'+ convert(varchar(8),DateDiff(s, #dadosHorario.horaInicio, #dadosHorario.horafim)%3600/60),2)+':'+ RIGHT('0' + convert(varchar(8),(DateDiff(s, #dadosHorario.horaInicio, #dadosHorario.horafim)%60)),2)) 
			ELSE '' END	AS Faltas,
			''																																																AS Assiduidade,
			'F'																																																AS tipo,
			#dadosHorario.row																																												AS turno,
			#dadosHorario.op																																												AS operador,
			0																																																AS registaExtra,
			DATEDIFF(s, #dadosHorario.horaInicio, #dadosHorario.horafim)																																	AS registaMSFalta,
			0																																																AS registaMSAssiduidade,
			#dadosHorario.date																																												AS date
		FROM 
			#dadosHorario 
	            LEFT JOIN #dadosPicaPonto  ON (#dadosPicaPonto.date = #dadosHorario.date AND #dadosPicaPonto.row = #dadosHorario.row   and #dadosPicaPonto.op = #dadosHorario.op)
		WHERE   #dadosPicaPonto.entradas IS NULL and #dadosPicaPonto.saidas is null and #dadosHorario.op IS NOT NULL
	
	
		UNION ALL 
	
		SELECT 
			DAY(#dadosPicaPonto.date)																																										AS dia,
			''																																																AS previstoEntrada,
			#dadosPicaPonto.entradas																																										AS RegistadoEntrada,
			''																																																AS previstoSaida,
			#dadosPicaPonto.saidas																																											AS RegistadoSaida,

			CASE WHEN (#dadosPicaPonto.entradas <#dadosPicaPonto.saidas ) THEN 
			--(SELECT CONVERT(VARCHAR(8), DATEADD(MS, DATEDIFF(MS, #dadosPicaPonto.entradas, #dadosPicaPonto.saidas), 0), 114) ) 
			(select (case when((select LEN(convert(varchar(8),DateDiff(s, #dadosPicaPonto.entradas, #dadosPicaPonto.saidas)/3600)))=1) 
				then  RIGHT('0'+ convert(varchar(8),DateDiff(s, #dadosPicaPonto.entradas, #dadosPicaPonto.saidas)/3600),2) 
				else convert(varchar(8),DateDiff(s, #dadosPicaPonto.entradas, #dadosPicaPonto.saidas)/3600) end) 
			 + ':'+ RIGHT('0'+ convert(varchar(8),DateDiff(s, #dadosPicaPonto.entradas, #dadosPicaPonto.saidas)%3600/60),2)+':'+ RIGHT('0' + convert(varchar(8),(DateDiff(s, #dadosPicaPonto.entradas, #dadosPicaPonto.saidas)%60)),2)) 
			ELSE '' END																																															AS Extras,
			''																																																	AS Faltas,
			''																																																	AS Assiduidade,
			'EX'																																																AS tipo,
			#dadosPicaPonto.row																																													AS turno,
			#dadosPicaPonto.op																																													AS operador,
			DATEDIFF(s, #dadosPicaPonto.entradas, #dadosPicaPonto.saidas)																																		AS registaExtra,
			0																																																	AS registaMSFalta,
			0																																																	AS registaMSAssiduidade,
			#dadosPicaPonto.date																																												AS date
		FROM 
			#dadosPicaPonto  
	            LEFT JOIN #dadosHorario  ON (#dadosHorario.date = #dadosPicaPonto.date AND #dadosHorario.row = #dadosPicaPonto.row and #dadosPicaPonto.op = #dadosHorario.op)
		WHERE   #dadosHorario.horaInicio IS NULL and #dadosHorario.horafim is null  and #dadosPicaPonto.op IS NOT NULL
	) dum
	ORDER   BY dia asc


	 SELECT
		@timeMsExtra  =SUM(#dadosPeriodo.registaMSExtra),
		@timeMsFalta  =SUM(#dadosPeriodo.registaMSFalta),
		@timeMsAssiduidade =  SUM(#dadosPeriodo.registaMSAssiduidade)

	FROM #dadosPeriodo
	GROUP BY operador


	SELECT 
		cast(dia as varchar) as dia,
		date                 as date,
		previstoEntrada,
		RegistadoEntrada,
		previstoSaida,
		RegistadoSaida,
		Extras,	
		Faltas,
		Assiduidade,
		tipo,
		turno,
		operador

	FROM #dadosPeriodo

	UNION ALL 
	
	SELECT 	
		'Total'																																				AS  dia,
		'2990-01-01'																																		AS  date,
		''																																					AS	previstoEntrada,
		''																																					AS	RegistadoEntrada,
		''																																					AS	previstoSaida,
		''																																					AS	RegistadoSaida,
		 Ltrim(STR(@timeMsExtra/3600, 10) + ':' + RIGHT('0' + LTRIM(@timeMsExtra%3600/60), 2) + ':' + RIGHT('0' + LTRIM(@timeMsExtra%60), 2))				AS  extras,	
		 Ltrim(STR(@timeMsFalta/3600, 10) + ':' + RIGHT('0' + LTRIM(@timeMsFalta%3600/60), 2) + ':' + RIGHT('0' + LTRIM(@timeMsFalta%60), 2))				AS	Faltas,
		 Ltrim(STR(@timeMsAssiduidade/3600, 10) + ':' + RIGHT('0' + LTRIM(@timeMsAssiduidade%3600/60), 2) + ':' + RIGHT('0' + LTRIM(@timeMsAssiduidade%60), 2))	AS	Assiduidade,
		''																																					AS	tipo,
		1																																					AS	turno,
		@op																																					AS	operador

	order by date , turno

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosHorario'))
		DROP TABLE #dadosHorario

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosPicaPonto'))
		DROP TABLE #dadosPicaPonto

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosPeriodo'))
		DROP TABLE #dadosPeriodo

END
GO
Grant Execute on dbo.up_relatorio_presenca_colaborador_dias to Public
Grant control on dbo.up_relatorio_presenca_colaborador_dias to Public
GO