/*
	exec up_relatorio_gestaoProdutos_Stock '','','','','',0,'<','0','Loja 1',0

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_relatorio_gestaoProdutos_Stock]') IS NOT NULL
    DROP procedure dbo.up_relatorio_gestaoProdutos_Stock
GO

CREATE procedure dbo.up_relatorio_gestaoProdutos_Stock
	
	@ref				VARCHAR(254) = ''
	,@design			VARCHAR(254) = ''
	,@lab				VARCHAR(120) = ''
	,@tipoProduto		VARCHAR(254) = ''
	,@atributosFam		VARCHAR(MAX) = ''
	,@stkTag			BIT			 = 0
	,@stkOp				VARCHAR(4)	 = '>'
	,@stk				VARCHAR(10)	 = '0'
	,@site				VARCHAR(254) = ''
	,@incluiInactivos	BIT			 = 0
	
/* WITH ENCRYPTION */
AS
	If OBJECT_ID('tempdb.dbo.#filtroRef') IS NOT NULL
    		drop table #filtroRef;

	DECLARE @sqlSelect  VARCHAR(4000) = ''
	DECLARE @sqlFrom    VARCHAR(4000) = ''
	DECLARE @sqlWhere   VARCHAR(4000) = ''
	DECLARE @sqlGroupBy VARCHAR(4000) = ''

	SET @atributosFam = (CASE WHEN (@atributosFam IS NULL OR @atributosFam = '') THEN ';;;' ELSE @atributosFam END)

	Declare @departamento varchar(254)
	DECLARE @departamentoID varchar(15)
	set @departamento =  (select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 1)
	SET	 @departamentoID = ISNULL((select top 1 id from grande_mercado_hmr (nolock) where upper(rtrim(ltrim(descr))) = upper(ltrim(rtrim(@departamento)))),'')
	
	Declare @seccao varchar(254)
	DECLARE @seccaoID varchar(15)
	set @seccao =  (select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 2)
	SET @seccaoID = ISNULL((select top 1 id from mercado_hmr (nolock) where upper(rtrim(ltrim(descr))) = upper(ltrim(rtrim(@seccao)))),'')
		
	Declare @categoria varchar(254)
	DECLARE @categoriaID varchar(15)
	set @categoria =   (select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 3)
	SET @categoriaID = ISNULL((select top 1 id from categoria_hmr (nolock) where upper(rtrim(ltrim(descr))) = upper(ltrim(rtrim(@categoria)))),'')
	
	Declare @segmento varchar(254)
	DECLARE @segmentoID varchar(15)
	set @segmento =   (select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 4)
	SET @segmentoID = ISNULL((select top 1 id from segmento_hmr (nolock) where upper(rtrim(ltrim(descr))) = upper(ltrim(rtrim(@segmento)))),'')

	SELECT
		valueText,
		site
	INTO
		#filtroRef
	FROM
		tempFilters(nolock)
	WHERE
		token = @ref and
		site in (Select items from dbo.up_splitToTable(@site, ',')) 


	SET @sqlSelect = @sqlSelect + N'
	SELECT 
		st.ref						AS ref
		,ISNULL(st.design,'''')		AS design
		,ISNULL(st.stmax,0)			AS stkMax
		,ISNULL(st.ptoenc,0)		AS ptoDeEnc
		,ISNULL(SUM(st.eoq),0)		AS qtdEnc
		,ISNULL(st.qttembal,0)		AS qtdEmb
		,empresa.site				AS loja'

	SET @sqlFrom = @sqlFrom + N'
	FROM
		st(NOLOCK)
		INNER JOIN empresa(NOLOCK) ON empresa.no = st.site_nr'

	SET @sqlWhere = @sqlWhere + N' WHERE 
		empresa.site in (Select items from dbo.up_splitToTable('''+convert(varchar,@site)+''','',''))'
	IF @ref != ''
		SET @sqlWhere = @sqlWhere + N' AND st.ref in (Select valueText from #filtroRef)'

	IF @design!= ''
		SET @sqlWhere = @sqlWhere + N' AND st.design LIKE ''%'' + '''+convert(varchar,@design)+''' + ''%'' '
	
	IF @lab != ''
		SET @sqlWhere = @sqlWhere + N' AND st.u_lab = '''+convert(varchar,@lab)+''' '

	IF @tipoProduto != ''
	BEGIN
		SET @sqlFrom = @sqlFrom + N' LEFT JOIN b_famFamilias (NOLOCK) ON st.u_famstamp = b_famFamilias.famstamp'
		SET @sqlWhere = @sqlWhere + ' AND b_famFamilias.design = '''+convert(varchar,@tipoProduto)+''''
	END

	IF @departamentoID != 0
	BEGIN
		SET @sqlWhere = @sqlWhere +N' AND st.u_depstamp = '''+convert(varchar,@departamentoID)+''' '
	END
	IF @seccaoID != 0
	BEGIN
		SET @sqlWhere = @sqlWhere +N' AND st.u_secstamp = '''+convert(varchar,@seccaoID)+''''
	END
	IF @categoriaID != 0
	BEGIN
		SET @sqlWhere = @sqlWhere +N' AND st.u_catstamp = '''+convert(varchar,@categoriaID)+''' '
	END
	IF @segmentoID != 0
	BEGIN
		SET @sqlWhere = @sqlWhere +N' AND st.u_segstamp = '''+convert(varchar,@segmentoID)+''' '
	END	

	IF @stkTag = 1
		BEGIN 
			SET @sqlWhere = @sqlWhere + N' AND st.stock ' + @stkOp +  @stk
		END
	IF @incluiInactivos= 0
	BEGIN
		SET @sqlWhere = @sqlWhere + ' AND st.inactivo = 0'
	END

	SET @sqlGroupBy = @sqlGroupBy +'
	GROUP BY
		st.ref, st.design, st.stmax, st.ptoenc, st.qttembal, empresa.site'

	PRINT @sqlSelect
	PRINT @sqlFrom
	PRINT @sqlWhere
	PRINT @sqlGroupBy
	
	EXEC(@sqlSelect + @sqlFrom + @sqlWhere + @sqlGroupBy) 

	If OBJECT_ID('tempdb.dbo.#filtroRef') IS NOT NULL
    	drop table #filtroRef;
GO
GRANT EXECUTE on dbo.up_relatorio_gestaoProdutos_Stock TO PUBLIC
GRANT Control on dbo.up_relatorio_gestaoProdutos_Stock TO PUBLIC
GO