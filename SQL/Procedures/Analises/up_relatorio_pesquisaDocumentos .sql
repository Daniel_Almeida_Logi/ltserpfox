-- Pesquisar Documentos - painel de Relatorios
-- exec up_relatorio_pesquisaDocumentos 1000, '', '', 0, -1, '19000101',  '30001231', '', 1, 'Administrator', '', 'Loja 1'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_pesquisaDocumentos]') IS NOT NULL
	drop procedure dbo.up_relatorio_pesquisaDocumentos
go

create procedure dbo.up_relatorio_pesquisaDocumentos
@topo		int,
@entidade	varchar(55),
@numdoc		varchar(20),
@no			numeric(10),
@estab		numeric(3),
@dataini	datetime,
@datafim	datetime,
@doc		varchar(MAX),
@user		numeric(6),
@group		varchar(50),
@design		varchar(13),
@site varchar(55)
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON
;with
	cte1 as
	(
		select Tipodoc, sum(QTTOT) as qttot ,ESCOLHA,Operador,CabStamp,Datav,Data,OUSRDATA,OUSRHORA,Documento,Numdoc,Entidade,NO,ESTAB,total,site
		from (
			SELECT
				Tipodoc		= 'BO',
				QTTOT		= bi.qtt,
				ESCOLHA		= 0,
				Operador	= BO.VENDNM,
				CabStamp	= bo.bostamp,
				Datav		= bo.dataobra,
				Data		= CONVERT(varchar,bo.dataobra,102),
				bo.OUSRDATA, bo.OUSRHORA,
				Documento	= bo.NMDOS,
				Numdoc		= Rtrim(Ltrim(CONVERT(varchar,bo.OBRANO))),
				Entidade	= UPPER(bo.NOME),
				NO			= bo.NO,
				estab		= bo.ESTAB,
				Total		= etotal,
				site
       		FROM
       			BO (nolock)
       			inner join bi (nolock) on bo.bostamp=bi.bostamp
       		where
       			(bo.dataobra >= @dataini AND bo.dataobra <= @datafim)
   				AND (bo.nmdos in (select * from dbo.up_splitToTable(@doc,',')) or @doc= '')
   				and (design like @design+'%' OR ref like @design+'%')
				AND (CONVERT(varchar,bo.obrano) like CASE WHEN @numDoc = '' THEN CONVERT(varchar,bo.obrano) ELSE @numDoc END)
				and bo.nome LIKE @entidade + '%'
				AND bo.NO = CASE WHEN @no = 0 THEN bo.NO ELSE @no END
				AND bo.ESTAB = CASE WHEN @estab = -1 THEN bo.ESTAB ELSE @estab END
				AND dbo.up_PerfilFacturacao(@user, @group, RTRIM(LTRIM(bo.nmdos)) + ' - ' + 'Visualizar',@site) = 0
				and bo.site = case when @site = '' then bo.site else @site end
		)h
		group by Tipodoc, ESCOLHA,Operador,CabStamp,Datav,Data,OUSRDATA,OUSRHORA,Documento,Numdoc,Entidade,NO,ESTAB,total,site
	union all
		select Tipodoc, sum(QTTOT) as qttot ,ESCOLHA,Operador,CabStamp,Datav,Data,OUSRDATA,OUSRHORA,Documento,Numdoc,Entidade,NO,ESTAB,total,site
		from (
			SELECT
				Tipodoc		= 'FO',
				QTTOT		= fn.qtt,
				ESCOLHA		= 0,
				Operador	= SSUSERNAME,
				CabStamp	= FO.FOSTAMP,
				Datav		= fo.data,
				Data		= CONVERT(varchar,fo.docdata,102),
				Fo.ousrdata, fo.ousrhora,
				Documento	= fo.docnome,
				Numdoc		= Rtrim(Ltrim(fo.adoc)),
				Entidade	= UPPER(NOME),
				no			= fo.NO,
				estab		= fo.ESTAB,
				total		= etotal
				,site
			FROM
				FO (nolock)
				inner join fn (nolock) on fo.fostamp = fn.fostamp
			where
				(fo.docdata >= @dataini AND fo.docdata <= @datafim)
				AND (fo.docnome in (select * from dbo.up_splitToTable(@doc,',')) or @doc= '')
				and (design like @design+'%' OR ref like @design+'%' OR oref like @design+'%')
				AND fo.adoc like @numDoc + '%'
				and fo.nome LIKE @entidade+'%'
				AND fo.NO = CASE WHEN @no = 0 THEN fo.NO ELSE @no END
				AND fo.ESTAB = CASE WHEN @estab = -1 THEN fo.ESTAB ELSE @estab END
				AND dbo.up_PerfilFacturacao(@user, @group, RTRIM(LTRIM(fo.docnome)) + ' - ' + 'Visualizar',@site) = 0
				and fo.site = case when @site = '' then fo.site else @site end
		)h
		group by Tipodoc, ESCOLHA,Operador,CabStamp,Datav,Data,OUSRDATA,OUSRHORA,Documento,Numdoc,Entidade,NO,ESTAB,total,site
	)
	select 
		top (@topo) *
			,id = ROW_NUMBER() over (order by data, convert(varchar,cte1.OUSRDATA,102), cte1.OUSRHORA) /*convert(varchar,cte1.OUSRDATA,102), cte1.OUSRHORA)*/
	from 
		cte1
	ORDER BY data desc, convert(varchar,cte1.OUSRDATA,102) desc, cte1.OUSRHORA DESC

GO
Grant Execute On dbo.up_relatorio_pesquisaDocumentos to Public
Grant Control On dbo.up_relatorio_pesquisaDocumentos to Public
Go

