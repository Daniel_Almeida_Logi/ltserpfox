/* Relatório Credito Agenda Dividas Fornecedores Mensal

	exec up_relatorio_credito_DividasaFornecedoresMensal 'OCP - Portugal','202',-99999,'Loja 1'

*/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_relatorio_credito_DividasaFornecedoresMensal]') IS NOT NULL
	drop procedure dbo.up_relatorio_credito_DividasaFornecedoresMensal
go

create procedure dbo.up_relatorio_credito_DividasaFornecedoresMensal
@nome as varchar(55),
@dataIni as datetime,
@saldo as numeric(9,2),
@site varchar(55)
/* with encryption */
AS
BEGIN	
	set language portuguese	
	;with cteSaldoAnterior as (
		select
			fc.nome,
			fc.no,
			fc.estab, 
			saldo = SUM((fc.ecred-fc.ecredf)-(fc.edeb-fc.edebf))
		from 
			fc (nolock)
			left join fo (nolock) on fo.fostamp = fc.fostamp 
		where 
			(abs((fc.ecred-fc.ecredf)-(fc.edeb-fc.edebf)) > 0.010000)
			and fc.nome = case when @nome = '' then fc.nome else @nome end
			and fc.datalc < @dataIni
		    and fc.cm !=112 -- não pode entrar a V/ Fatura Resumo
			and (
				fo.site = (case when @site = '' Then fo.site else @site end)
			)
		group by 
			fc.no
			,fc.nome
			,fc.estab
	),
	cteCC as (
		select 
			fc.moeda
			,fc.no
			,fc.estab
			,nome = fc.nome + '[' + LTRIM(STR(fc.no)) + '][' + LTRIM(STR(fc.estab)) + ']'
			,saldo = SUM((fc.ecred-fc.ecredf)-(fc.edeb-fc.edebf))
			,ano = isnull(YEAR(fc.datalc),'')
			,mes = isnull(month(fc.datalc),'')
		from 
			fc (nolock)  
			left join fo (nolock) on fc.fostamp=fo.fostamp  
		where
			fo.docdata >= @dataIni 
			and (abs((fc.ecred-fc.ecredf)-(fc.edeb-fc.edebf)) > 0.010000)
			and fo.site = (case when @site = '' Then fo.site else @site end)
			and fc.cm !=112 -- não pode entrar a V/ Fatura Resumo
		group by 
			fc.no
			,fc.nome
			,fc.estab
			,fc.moeda
			,YEAR(fc.datalc)
			,month(fc.datalc)
	), cteDadosCompletos as (
		Select 
			saldoAnterior = isnull(cteSaldoAnterior.saldo,0)
			,nome
			,no
			,estab
			,saldo = isnull(cteSaldoAnterior.saldo,0) 
			,ano =  'Anterior a ' + LEFT(CONVERT(VARCHAR, @dataIni, 120), 10) 
			,mes = '' 
		from 
			cteSaldoAnterior
		union all 
		Select 
			saldoAnterior = 0
			,fl.nome
			,fl.no
			,fl.estab
			,saldo = cteCC.saldo
			,ano =  case when ano is null then 'Anterior a ' + LEFT(CONVERT(VARCHAR, @dataIni, 120), 10) else 'Z_' + STR(ano) end
			,mes =   case when mes is null then '' else mes end
		from 
			fl (nolock)
			left join cteCC on fl.no = cteCC.no  and fl.estab = cteCC.estab
	)
	select 
		cteDadosCompletos.* 
		,nome = cteDadosCompletos.nome + '[' + LTRIM(STR(cteDadosCompletos.no)) + '][' +  LTRIM(STR(cteDadosCompletos.estab))  +']'
	from 
		cteDadosCompletos
		left join fl (nolock) on fl.no = cteDadosCompletos.no and cteDadosCompletos.estab = fl.estab
	Where
		cteDadosCompletos.nome = case when @nome = '' then cteDadosCompletos.nome else @nome end
		and isnull(fl.esaldo,0) >= @saldo
END
GO
Grant Execute on dbo.up_relatorio_credito_DividasaFornecedoresMensal to Public
Grant control on dbo.up_relatorio_credito_DividasaFornecedoresMensal to Public
GO
