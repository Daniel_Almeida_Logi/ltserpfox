/*
	Relatório Descontos Cartão Cliente

	exec up_relatorio_vendas_DescontosCartaoCliente '20240619','20240619', 'Loja 1'

	Obs: 
		. O total faturado neste relatório corresponde ao total das vendas que incluem vales rebatidos. 
		. Não são consideradas regularizações de vendas.
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_vendas_DescontosCartaoCliente]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_DescontosCartaoCliente
go

create procedure dbo.up_relatorio_vendas_DescontosCartaoCliente
@dataIni as datetime,
@dataFim as datetime,
@site varchar(55)
/* WITH ENCRYPTION */ 
AS
BEGIN

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase

	/* Calc Loja  */
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)

	/* Define se vais mostrar resultados abaixo do Cliente 199 */
	declare @Entidades as bit
	set @Entidades = case when (select tipoempresa from empresa where site = @site) in ('FARMACIA', 'PARAFARMACIA') then convert(bit,1) else convert(bit,0) end
	/* Dados Base Vendas  */
	create table #dadosVendasBase (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)
	)
    insert #dadosVendasBase
    exec up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site,1
	/* Elimina Vendas tendo em conta tipo de cliente*/
	If @Entidades = 1
	DELETE FROM #dadosVendasBase WHERE #dadosVendasBase.no <= 200

	-- Elemino os dados que não tenham descontos 
	DELETE FROM #dadosVendasBase
	WHERE ftstamp NOT IN (
		SELECT DISTINCT ftstamp
		FROM #dadosVendasBase
		WHERE descvale > 0 OR descvalor > 0
	)

	DECLARE @pontosOuValor VARCHAR(254)	
	SET @pontosOuValor = (SELECT textValue FROM B_Parameters WHERE stamp='ADM0000000302')

	IF (@pontosOuValor ='Pontos')
	BEGIN
		;with 
			-- Vales Emitidos
			cte1 AS (
				SELECT
					vale.clstamp
					,valor    = ISNULL(SUM(valor),0)
					,totalFat = 0
				FROM
					B_fidelVale	as vale (NOLOCK)
					INNER JOIN b_utentes (NOLOCK) ON vale.clstamp=b_utentes.utstamp 
				WHERE
					anulado=0
					AND CONVERT(DATE,vale.ousrdata) BETWEEN @dataIni AND @dataFim
				GROUP BY
					vale.clstamp
			),
			-- Vales Rebatidos
			cte2 AS (
				SELECT
					vale.clstamp
					,valor2   = ISNULL(SUM(valor),0)
					,totalFat = ISNULL(SUM(ft.etotal),0)
				From
					B_fidelVale vale (NOLOCK) 
					INNER JOIN b_utentes (NOLOCK)	ON b_utentes.utstamp = vale.clstamp 
					INNER JOIN fi (NOLOCK)			ON fi.u_refvale = vale.ref
					INNER JOIN ft (NOLOCK)			ON ft.ftstamp=fi.ftstamp
				WHERE
					vale.anulado=0
					AND vale.abatido = 1
					AND CONVERT(DATE,vale.usrdata) BETWEEN @dataIni AND @dataFim
				GROUP BY
					vale.clstamp
			)
			SELECT	
				'NO' = b_utentes.no, 
				'NOME' = b_utentes.nome, 
				'ESTAB'	= b_utentes.estab,
				'DESCONTOS EMITIDOS' = ISNULL(cte1.valor,0),
				'DESCONTOS REBATIDOS' =	ISNULL(cte2.valor2,0)
				,'Vd PV Cl' = ISNULL(#dadosVendasBase.etiliquido,0)
			FROM	
				b_utentes (NOLOCK)
			LEFT JOIN 
				cte1  ON b_utentes.utstamp=cte1.clstamp
			LEFT JOIN 
				cte2  ON b_utentes.utstamp=cte2.clstamp
		INNER JOIN 
			#dadosVendasBase ON #dadosVendasBase.no = B_UTENTES.no and #dadosVendasBase.estab = B_UTENTES.estab
			WHERE
				ISNULL(cte1.valor,0) != 0 OR ISNULL(cte2.valor2,0) != 0
			ORDER BY 
				cte1.valor,b_utentes.no
	END
	ELSE
	BEGIN
		If OBJECT_ID('tempdb.dbo.#cartoes') IS NOT NULL
			DROP TABLE #cartoes
		If OBJECT_ID('tempdb.dbo.#tempValores') IS NOT NULL
			DROP TABLE #tempValores

		CREATE TABLE #cartoes(nrcartao VARCHAR(50),
								nome VARCHAR(254),
								no NUMERIC(18,0),
								estab NUMERIC(18,0),
								valorInicial NUMERIC(15,2),
								valorFt NUMERIC(15,2),
								valorOcorrencia NUMERIC(15,2),
								totalPrimeiroDia NUMERIC(15,2),
								valorAcumulado NUMERIC(15,2),
								valorUsado NUMERIC(15,2))

		INSERT INTO 
			#cartoes (nrcartao, nome, no,estab, valorInicial, valorFt, valorOcorrencia, totalPrimeiroDia, valorAcumulado, valorUsado)
		SELECT DISTINCT 
			nrcartao,
			'',
			clno,
			clestab,
			0,
			0,
			0,
			0,
			0,
			0
		FROM 
			b_fidel(nolock)
		WHERE
			inactivo = 0

		UPDATE #cartoes
		SET
				nome = b_utentes.nome
		FROM 
			b_utentes(NOLOCK)
		INNER JOIN
			#cartoes ON #cartoes.nrcartao = b_utentes.nrcartao
		WHERE
			b_utentes.no =  #cartoes.no
			AND b_utentes.estab =  #cartoes.estab

		UPDATE 
			#cartoes
		SET
			valorInicial = ISNULL(valcartaoini,0)
		FROM 
			b_fidel (NOLOCK)
		INNER JOIN
			 #cartoes ON #cartoes.nrcartao = b_fidel.nrcartao

		UPDATE #cartoes
		SET
			valorFt = ISNULL(subquery.total, 0)
		FROM
			#cartoes
		INNER JOIN (
			SELECT
				b_utentes.nrcartao,
				SUM(FI.valcartao) AS total
			FROM
				FT (NOLOCK)
			INNER JOIN
				b_utentes(NOLOCK) ON FT.no = b_utentes.no AND FT.estab = b_utentes.estab
			INNER JOIN
				#cartoes ON #cartoes.nrcartao = b_utentes.nrcartao
			INNER JOIN
				ft2 (NOLOCK) ON FT.ftstamp = ft2.ft2stamp
			INNER JOIN
				FI (NOLOCK) ON FT.ftstamp = FI.ftstamp
			WHERE
				FI.valcartao != 0
				AND FT.fdata between @dataini AND @dataFim
			GROUP BY
				b_utentes.nrcartao
		) AS subquery ON #cartoes.nrcartao = subquery.nrcartao

		-- entidades facturadoras 
		UPDATE #cartoes
		SET
			valorFt = ISNULL(total, 0) + valorFt
		FROM
			#cartoes
		INNER JOIN (
			SELECT
				b_utentes.nrcartao,
				SUM(FI.valcartao) AS total
			FROM
				FT (NOLOCK)
			INNER JOIN 
				b_utentes(NOLOCK) ON  FT.u_hclstamp = b_utentes.utstamp
			INNER JOIN
				fi (NOLOCK) ON FT.ftstamp = fI.ftstamp
			WHERE 
				FI.valcartao != 0  
				AND fi.ofistamp = '' 
				AND FT.fdata between @dataini AND @dataFim
				AND u_hclstamp != ''
			GROUP BY
				b_utentes.nrcartao
		) AS subquery ON #cartoes.nrcartao = subquery.nrcartao

		-- entidades regularização 
		UPDATE #cartoes
		SET
			valorFt = ISNULL(subquery.total, 0) + valorFt
		FROM
			#cartoes
		INNER JOIN (
			SELECT
				b_utentes.nrcartao,
				SUM(FI.valcartao) AS total
			FROM
				FT (NOLOCK)
			INNER JOIN
				FI(NOLOCK) ON fi.ftstamp = Ft.ftstamp 
			INNER JOIN
				fi fii(NOLOCK) ON fii.fistamp = Fi.ofistamp 
			INNER JOIN
				FT ftt(NOLOCK) ON ftt.ftstamp= fii.ftstamp
			INNER JOIN
				b_utentes(NOLOCK) ON b_utentes.utstamp = ftt.u_hclstamp
			WHERE 
				FI.valcartao !=0  
				AND FI.ofistamp !=''
				AND FT.fdata between @dataini AND @dataFim 
				AND ftt.u_hclstamp !=''
			GROUP BY
				b_utentes.nrcartao
		) AS subquery ON #cartoes.nrcartao = subquery.nrcartao

		-- valor de ocorrencias
		UPDATE #cartoes
		SET valorOcorrencia = ISNULL((
			SELECT 
				SUM(B_ocorrencias.nValor)
			FROM 
				B_ocorrencias (NOLOCK)
			INNER JOIN 
				b_utentes(NOLOCK) ON B_ocorrencias.linkStamp = b_utentes.utstamp AND B_ocorrencias.Tipo = 'Cartão Cliente'
			WHERE 
				#cartoes.nrcartao = b_utentes.nrcartao 
				AND  B_ocorrencias.date between @dataini AND @dataFim
			), 0)

		-- total do 1º dia
		UPDATE 
			#cartoes
		SET  
			totalPrimeiroDia  = ISNULL(valorInicial,0) + ISNULL(valorFt,0) + ISNULL(valorOcorrencia,0)
		FROM 
			#cartoes

		-- tabela temporária para calcular os valores acumulados e usados
		SELECT 
			T.nrcartao,
			SUM(CASE WHEN T.valor > 0 THEN T.valor ELSE 0 END) AS valorAcumulado,
			SUM(CASE WHEN T.valor < 0 THEN T.valor ELSE 0 END) AS valorUsado,
			SUM(valorVendaUtente) AS valorVendaUtente
		INTO 
			#tempValores
		FROM (
			SELECT 
				ISNULL(FI.valcartao, 0) AS valor,
				ISNULL(FT.total, 0) AS valorVendaUtente,
				b_utentes.nrcartao
			FROM 
				FT (NOLOCK)
			INNER JOIN 
				b_utentes (NOLOCK) ON FT.no = b_utentes.no AND FT.estab = b_utentes.estab  
			INNER JOIN 
				ft2 (NOLOCK) ON FT.ftstamp = ft2.ft2stamp
			INNER JOIN 
				FI (NOLOCK) ON FT.ftstamp = FI.ftstamp
			INNER JOIN 
				#cartoes ON #cartoes.nrcartao = b_utentes.nrcartao 
			WHERE 
				FI.valcartao != 0 
				AND FT.fdata BETWEEN @dataini AND @datafim 
				AND u_hclstamp = ''

			UNION ALL

			SELECT 
				ISNULL(FI.valcartao, 0) AS valor,
				ISNULL(FT.total, 0) AS valorVendaUtente,
				b_utentes.nrcartao
			FROM 
				FT (NOLOCK)
			INNER JOIN 
				b_utentes (NOLOCK) ON FT.u_hclstamp = b_utentes.utstamp
			INNER JOIN 
				ft2 (NOLOCK) ON FT.ftstamp = ft2.ft2stamp
			INNER JOIN 
				FI (NOLOCK) ON FT.ftstamp = FI.ftstamp
			INNER JOIN 
				#cartoes ON #cartoes.nrcartao = b_utentes.nrcartao 
			WHERE 
				FI.valcartao != 0 
				AND fi.ofistamp = '' 
				AND FT.fdata BETWEEN @dataini AND @datafim 
				AND u_hclstamp != ''

			UNION ALL

			SELECT 
				ISNULL(FI.valcartao, 0) AS valor,
				ISNULL(FT.total, 0) AS valorVendaUtente,
				b_utentes.nrcartao
			FROM 
				FT (NOLOCK)
			INNER JOIN 
				FI (NOLOCK) ON FI.ftstamp = FT.ftstamp 
			INNER JOIN 
				fi fii (NOLOCK) ON fii.fistamp = FI.ofistamp 
			INNER JOIN 
				FT ftt (NOLOCK) ON ftt.ftstamp = fii.ftstamp
			INNER JOIN 
				b_utentes (NOLOCK) ON b_utentes.utstamp = ftt.u_hclstamp
			INNER JOIN 
				#cartoes ON #cartoes.nrcartao = b_utentes.nrcartao 
			WHERE 
				FI.valcartao != 0 
				AND FI.ofistamp != '' 
				AND FT.fdata BETWEEN @dataini AND @datafim 
				AND ftt.u_hclstamp != ''

			UNION ALL

			SELECT 
				ISNULL(B_ocorrencias.nValor, 0) AS valor,
				0 AS valorVendaUtente,
				b_utentes.nrcartao
			FROM 
				B_ocorrencias (NOLOCK)
			INNER JOIN 
				b_utentes (NOLOCK) ON B_ocorrencias.linkStamp = b_utentes.utstamp AND B_ocorrencias.Tipo = 'Cartão Cliente'
			INNER JOIN 
				#cartoes ON #cartoes.nrcartao = b_utentes.nrcartao 
			WHERE 
				B_ocorrencias.date BETWEEN @dataini AND @datafim 
				AND oValor NOT LIKE '%pts%'
		) AS T
		GROUP BY T.nrcartao

		-- Em seguida, atualize a tabela #cartoes usando a tabela temporária #tempValores
		UPDATE #cartoes
		SET 
			#cartoes.valorAcumulado = #tempValores.valorAcumulado,
			#cartoes.valorUsado = #tempValores.valorUsado
		FROM 
			#cartoes 
		INNER JOIN 
			#tempValores ON #cartoes.nrcartao = #tempValores.nrcartao

		SELECT
			#cartoes.nrcartao,
			'NOME' = #cartoes.nome,
			'NO' = #cartoes.no,
			'ESTAB' = #cartoes.estab,
			valorInicial,
			'Vd PV Cl' = #dadosVendasBase.etiliquido,
			valorOcorrencia,
			totalPrimeiroDia,
			--'DESCONTOS EMITIDOS' = valorAcumulado,
			'DESCONTOS EMITIDOS' = valorUsado,
			'DESCONTOS REBATIDOS' = valorUsado
		FROM
			#cartoes
		INNER JOIN 
			B_UTENTES (NOLOCK) ON B_UTENTES.nrcartao = #cartoes.nrcartao AND B_UTENTES.no = #cartoes.no
		INNER JOIN 
			#dadosVendasBase ON #dadosVendasBase.no = B_UTENTES.no and #dadosVendasBase.estab = B_UTENTES.estab

		If OBJECT_ID('tempdb.dbo.#cartoes') IS NOT NULL
			DROP TABLE #cartoes
		If OBJECT_ID('tempdb.dbo.#tempValores') IS NOT NULL
			DROP TABLE #tempValores
	END


	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
END

GO
Grant Execute on dbo.up_relatorio_vendas_DescontosCartaoCliente to Public
Grant control on dbo.up_relatorio_vendas_DescontosCartaoCliente to Public
Go