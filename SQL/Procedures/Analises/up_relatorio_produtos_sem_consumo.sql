/* Relatório que mostra produtos sem rotação

alterado a 2020-08-10 por JG - permissão de multiselecao do site
alterado a 2020-09-23 por JG - alteracao tabelas para HMR
 
 
	exec up_relatorio_produtos_sem_consumo '20191101','20220607','','','','','Loja 1','',-9999, '', '20191122'


	exec up_relatorio_produtos_sem_consumo '19000101','20200810','','','','','Loja 3','',-9999, '', '20191122'
	exec up_relatorio_produtos_sem_consumo '19000101','20200810','','','','','Loja 2','',-9999, '', '20191122'
	exec up_relatorio_produtos_sem_consumo '19000101','20200810','','','','','Loja 2,Loja 3','',-9999, '', '20191122'

	exec up_relatorio_produtos_sem_consumo '20170101','20171210','','','','','Loja 3','',-9999, '', '20170101'
	exec up_relatorio_produtos_sem_consumo '20170101','20171210','','','','','Loja 2','',-9999, '', '20170101'
	exec up_relatorio_produtos_sem_consumo '20170101','20171210','','','','','Loja 2,Loja 3','',-9999, '', '20170101'
		
	exec up_relatorio_produtos_sem_consumo '20191101','20191122','','','Bene Farmacêutica','','Loja 1','',-9999, '', '20191122'
	exec up_relatorio_produtos_sem_consumo '20200801','20200810','','','Bene Farmacêutica','','Loja 1','',-9999, '', '20191122'


*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_relatorio_produtos_sem_consumo]') IS NOT NULL
	DROP procedure dbo.up_relatorio_produtos_sem_consumo
GO

CREATE procedure dbo.up_relatorio_produtos_sem_consumo
@dataIni		datetime,
@dataFim		datetime,
@ref			varchar(18),
@familia		varchar(max),
@lab			varchar(120),
@marca			varchar(200),
@site			varchar(254),
@atributosFam   varchar(max),
@stock			numeric(9,2),
@generico		varchar(18),
@dataucompra		datetime
/* WITH ENCRYPTION */ 
AS
BEGIN
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosComMov'))
		DROP TABLE #DadosComMov
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosFami'))
		DROP TABLE #DadosFami
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosMarca'))
		DROP TABLE #DadosMarca
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosLab'))
		DROP TABLE #DadosLab
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosST'))
		DROP TABLE #DadosST

	SET @atributosFam = (CASE WHEN (@atributosFam IS NULL OR @atributosFam = '') THEN ';;;' ELSE @atributosFam END)

	DECLARE @departamento VARCHAR(254) = ''
	SET @departamento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 1),'') 
	DECLARE @sessao       VARCHAR(254) = ''
	set @sessao = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 2) ,'')
	DECLARE @categoria    VARCHAR(254) = ''
	SET @categoria = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 3),'') 
	DECLARE @segmento     VARCHAR(254) = ''
	SET @segmento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 4),'') 
	
	if @site = ''
		set @site = isnull((select  convert(varchar(254),(select 
							site + ', '
						from 
							empresa (nolock)							
						FOR XML PATH(''))) as no),0)

	declare @site_nr varchar(20)
	set @site_nr = isnull((select  convert(varchar(254),(select 
							convert(varchar,no) + ', '
						from 
							empresa (nolock)							
						Where 
							site in (Select items from dbo.up_splitToTable(@site, ','))
						FOR XML PATH(''))) as no),0)




	/* Convereter Valor Generico*/
	IF @generico = 'GENERICO'
		set @generico = 1
	IF @generico = 'NÃO GENERICO'
		set @generico = 0
	
	
	/* Tabela Movimentos*/
	Select 
		sl.ref
		,sa = sum(case when cm <50 then qtt else -qtt end)
		, empresa.no		
	into 
		#DadosComMov
	From
		sl (nolock)
		left join empresa_arm (nolock) on empresa_arm.armazem = sl.armazem
		left join empresa (nolock) on empresa_arm.empresa_no = empresa.no
	Where
		datalc between @dataIni and @dataFim
		--and (empresa.site is null or empresa.site = @site)
		and (empresa.site is null or empresa.site  in (Select items from dbo.up_splitToTable(@site, ',')) )
		and sl.cm in (75,76,77)
	group by
		sl.ref,empresa.no, sl.armazem
		

--Select * from #DadosComMov
	
	
	/* Tabela Familia */
	Select 
		distinct ref 
	into 
		#DadosFami
	From
		stfami (nolock)
	Where
		ref in (Select items from dbo.up_splitToTable(@familia,','))
		or @familia = '0' 
		or @familia = ''	
	-- Select * from #DadosFami
	
	/* Tabela Marca */
	Select 
		distinct usr1 
	into
		#DadosMarca
	From
		st (nolock)
	Where
		usr1 in (Select items from dbo.up_splitToTable(@marca,','))
		or @marca = ''	
	-- Select * from #DadosMarca
	
	/* Tabela Labs */
	Select 
		distinct u_lab 
	into 
		#DadosLab
	From
		st (nolock)
	Where
		u_lab in (Select items from dbo.up_splitToTable(@lab,','))
		or @lab = ''	
-- Select * from #DadosLab
	
	
	/* Tabela DadosST */
		Select
			st.ref 
			,st.design
			,faminome
			,familia
			,usr1
			,u_lab
			,fprod.generico
			,tabiva
			,epcpond
			,epv1
			,marg1
			,stock
			,UINTR
			,USAID	
			, ISNULL(A.descr,'') as departamento
			, ISNULL(B.descr,'') as seccao
			, ISNULL(C.descr,'') as categoria
			, ISNULL(D.descr,'') as segmento		
			
			,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 1) valDepartamento
			,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 2) valSeccao
			,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 3) valCategoria
			,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 4) valSegmento
			, epcult
			,site_nr
		into 
			#DadosST
		From
			st (nolock)
			left join fprod (nolock) on st.ref = fprod.cnp	
			left join grande_mercado_hmr (nolock) A on A.id = st.u_depstamp
			left join mercado_hmr (nolock) B on B.id = st.u_secstamp
			left join categoria_hmr (nolock) C on C.id = st.u_catstamp 
			left join segmento_hmr (nolock) D on D.id = st.u_segstamp	
		where 
			--site_nr = @site_nr
			site_nr in (Select items from dbo.up_splitToTable(@site_nr, ','))
			and stock>0
			and (isnull((select top 1 datalc from sl where cm in (1,2,6) and sl.ref=st.ref order by datalc desc),'19000101')<@dataucompra or st.ref in (select distinct ref from sl where nome like '%Inventário Migração SIF2000%'))
		
--select * from 	#DadosST
	
		
		/* Preparar Result Set Final */		
		Select 
			#dadosST.ref
			,design
			,faminome
			,marca = usr1
			,lab = u_lab
			,Generico
			,stock = SUM(isnull(sa.stock,0))			
			,pvp = #dadosST.epv1
			,epv1val = SUM(isnull(sa.stock,0)*#dadosST.epv1)
			,#DadosST.UINTR
			,#DadosST.USAID
			,#DadosST.epcult
			,epcultval= SUM(isnull(sa.stock,0)*#DadosST.epcult)
			,empresa.no as site_nr
		from 
			#DadosST
			--left join sa on sa.ref = #DadosST.ref
			left join sa on sa.ref = #DadosST.ref and sa.stock = #DadosST.stock
			left join empresa_arm on empresa_arm.armazem = sa.armazem
			left join empresa on empresa_arm.empresa_no = empresa.no			
		Where
			#DadosST.ref not in (select ref from #DadosComMov)
			and #DadosST.ref = case when @ref = '' then #DadosST.ref else @ref end 
			and #DadosST.familia in (Select ref from #DadosFami)
			and #DadosST.usr1 in (Select usr1 from #DadosMarca)
			and #DadosST.u_lab in (Select u_lab from #DadosLab)
			and #DadosST.generico = case when @generico = '' then  #DadosST.generico else @generico end			
			and isnull(sa.stock,0) > @stock
			AND departamento = (CASE WHEN @departamento = '' THEN departamento ELSE @departamento END)
			AND seccao = (CASE WHEN @sessao = '' THEN seccao ELSE  @sessao END)
			AND categoria = (CASE WHEN @categoria = '' THEN categoria ELSE  @categoria END)
			AND segmento = (CASE WHEN @segmento = '' THEN segmento ELSE  @segmento END)
			--and (empresa.site is null or empresa.site = @site)
			and ( empresa.site  in (Select items from dbo.up_splitToTable(@site, ',')) )
		GROUP BY 
			#dadosST.ref
			,design
			,faminome
			,usr1
			,u_lab
			,Generico
			,#dadosST.Epv1
			,#DadosST.UINTR
			,#DadosST.USAID
			,#DadosST.epcult
			,empresa.no			
		order by ref





--		select top 5 * from sa
	--	select sum(stock),ref from sa group by ref order by  sum(stock) 

		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosComMov'))
			DROP TABLE #DadosComMov
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosFami'))
			DROP TABLE #DadosFami
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosMarca'))
			DROP TABLE #DadosMarca
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosLab'))
			DROP TABLE #DadosLab
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosST'))
			DROP TABLE #DadosST
END

GO
GRANT EXECUTE on dbo.up_relatorio_produtos_sem_consumo TO PUBLIC
GRANT CONTROL on dbo.up_relatorio_produtos_sem_consumo TO PUBLIC
GO