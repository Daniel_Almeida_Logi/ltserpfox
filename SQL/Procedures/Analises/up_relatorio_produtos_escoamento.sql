﻿/* Relatório de Produtos em Escoamento

exec up_relatorio_produtos_escoamento '20200101', '20220607','Loja 1','','','','',-500,''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_relatorio_produtos_escoamento]') IS NOT NULL
DROP PROCEDURE dbo.up_relatorio_produtos_escoamento
GO

Create Procedure up_relatorio_produtos_escoamento
	@dataIni				DATETIME 
	,@dataFim				DATETIME 
	,@site					VARCHAR(254)
	,@ref					VARCHAR(254) = ''
	,@lab					VARCHAR(254) = ''
	,@marca					VARCHAR(200) = ''
	,@generico				VARCHAR(18) 
	,@stock					NUMERIC(9,2)
	,@motivoescoamento		VARCHAR(254) 

	
AS
SET NOCOUNT ON

	IF @generico = 'GENERICO'
		set @generico = 1
	IF @generico = 'NĂO GENERICO'
		set @generico = 0	


	Select DISTINCT 
		data_inicio								as 'datainicio'								
		,data_fim								as 'datafim'							
		,escoamento.cnp							as 'ref'
		,st.design
		,motivo_escoamento.descr				as 'motivoEscoamento'
		,stock
		
	From escoamento(nolock)
		left join fprod(nolock) on escoamento.cnp = fprod.cnp
		inner join motivo_escoamento(nolock) on  escoamento.motivo_escoamento_id = motivo_escoamento.id
		inner join st (nolock) on fprod.ref = st.ref
		inner join empresa (nolock) on st.site_nr = empresa.no
		
	Where 
		data_inicio between  convert(varchar,@dataIni) and convert(varchar,@dataFim)
		--data_inicio <= @dataIni 
		--and data_fim >= @dataFim
		and (usr1 in (select items from dbo.up_splitToTable(@marca,',')) or @marca= '')																	
		and isnull(st.stock,0) > @stock      
		and (ISNULL(fprod.generico,0)= case when @generico = '' then ISNULL(fprod.generico,0) else @generico end)					
		and empresa.site in (Select items from dbo.up_splitToTable(@site, ','))
		and (u_lab in (select items from dbo.up_splitToTable(@lab,';'))or @lab= '')
		and (escoamento.cnp in (select items from dbo.up_splitToTable(@ref,',')) or @ref= '')
		and (descr in (select items from dbo.up_splitToTable(@motivoescoamento,',')) or @motivoescoamento= '0' or @motivoescoamento= '')

GO
Grant Execute On up_relatorio_produtos_escoamento to Public
Grant Control On up_relatorio_produtos_escoamento to Public
GO

	