/* Relat�rio de lisatagem de informacao logs_com 

	exec up_relatorio_logs_com '20210601', '20210601', '', '',''
	exec up_relatorio_logs_com '20210101', '20210715', '', 'INTEGRADOR','Doc. Recibo'
	exec up_relatorio_logs_com '20210101', '20210715', 'Loja 1', 'INTEGRADOR','Factura'

*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_logs_com]') IS NOT NULL
	drop procedure dbo.up_relatorio_logs_com
go

create procedure [dbo].[up_relatorio_logs_com]
	 @dataIni							as datetime
	,@dataFim							as datetime
	,@site								as varchar(200)
	,@tipo								as varchar(40)
	,@docTipo							as varchar(max)

	

/* with encryption */
AS
BEGIN	


	if @site = ''
		set @site = isnull((select  convert(varchar(254),(select 
						site + ', '
					from 
						empresa (nolock)							
					FOR XML PATH(''))) as no),0)

	select 
		 nDOC, 
		 num, 
		 dateExp ,
		 no,
		 estab, 
		 nome,
		 total,
		 date,
		 param,
		 tipo,
		 iva,
		 base_inc
	FROM(
	SELECT distinct
		 nmdoc																								AS  nDOC, 
		 cast(fno as varchar(20))																			AS  num, 
		 [logs_com].data																					AS  dateExp , 
		 no																									AS  no,
		 estab																								AS estab, 
		 nome																								AS nome,
		 etotal																								AS total,
		 ft.fdata																							AS date,
		 cast(texto as varchar(max))																		AS param,
		 ft.nmdoc																							AS tipo,
		 ettiva																								AS iva,
		 ettiliq																							AS base_inc
	FROM [logs_com](NOLOCK)	
		INNER JOIN FT	(NOLOCK)		ON [logs_com].regstamp = FT.ftstamp AND ORIGEM='ft'
	 where [logs_com].tipo=@tipo and [logs_com].site in (Select items from dbo.up_splitToTable(@site, ',')) 
		and  CONVERT(varchar, [logs_com].data, 112) between @dataIni and @dataFim
		and ('Doc. Factura��o' in (Select items from dbo.up_splitToTable(@docTipo, ',')) or  @docTipo ='' or ft.nmdoc in (Select items from dbo.up_splitToTable(@docTipo, ','))) 


	 UNION ALL 

	SELECT distinct
		 nrAtend																							AS  nDOC, 
		 cast(0 as varchar(20))																				AS  num, 
		 [logs_com].data																					AS  dateExp , 
		 no																									AS  no,
		 estab																								AS estab, 
		 nome																								AS nome,
		 Total																								AS total,
		 b_pagcentral.odata																					as date,
		 cast(texto as varchar(max))																		AS param,
		 'Doc. Caixa'																						AS tipo,
		 0																									AS iva,
		 0																									AS base_inc
	FROM [logs_com] (NOLOCK)	
		inner join b_pagcentral (NOLOCK) ON [logs_com].regstamp = b_pagcentral.stamp and origem ='b_pagcentral'
	 where [logs_com].tipo=@tipo and [logs_com].site in (Select items from dbo.up_splitToTable(@site, ',')) 
			and CONVERT(varchar, [logs_com].data, 112) between @dataIni and @dataFim 	
			and ('Doc. Caixa' in (Select items from dbo.up_splitToTable(@docTipo, ',')) or  @docTipo ='') 

	 UNION ALL 

	 SELECT distinct
		 nmdoc																								AS  nDOC, 
		  cast(re.RNO as varchar(20))																			AS  num, 
		 [logs_com].data																					AS  dateExp , 
		 no																									AS  no,
		 estab																								AS estab, 
		 nome																								AS nome,
		 eTotal																								AS total,
		 re.rdata																							AS date,
		 cast(texto as varchar(max))																		AS param,
		 'Doc. Recibo'																						AS tipo,
		 Sum(eivav1 + eivav2 + eivav3 + eivav4 + eivav5 + eivav6 + eivav7 + eivav8 + eivav9)				AS iva,
		 Sum(erec) - (Sum(eivav1 + eivav2 + eivav3 + eivav4 + eivav5 + eivav6 + eivav7 + eivav8 + eivav9))	AS base_inc
	FROM [logs_com](NOLOCK)	
		inner join re (NOLOCK)	 ON [logs_com].regstamp = re.restamp and origem ='re'
		inner join rl  (NOLOCK)	 ON re.restamp = rl.restamp
	 where [logs_com].tipo=@tipo and [logs_com].site in (Select items from dbo.up_splitToTable(@site, ',')) 
		and CONVERT(varchar, [logs_com].data, 112) between @dataIni and @dataFim
		and ('Doc. Recibo' in (Select items from dbo.up_splitToTable(@docTipo, ',')) or  @docTipo ='') 
	GROUP BY 
		 nmdoc,								
		 cast(re.RNO as varchar(20)),	
		 [logs_com].data,					
		 no	,								
		 estab	,							
		 nome	,							
		 eTotal	,							
		 re.rdata	,						
		 cast(texto as varchar(max))		


	 UNION ALL 

	 SELECT distinct
		 docnome																							AS  nDOC, 
		 cast(adoc as varchar(20))																			AS  num, 
		[logs_com].data																						AS  dateExp , 
		 no																									AS  no,
		 estab																								AS estab, 
		 nome																								AS nome,
		 eTotal																								AS total,
		 fo.data																							AS date,
		 cast(texto as varchar(max))																		AS param,
		 'Doc. Fornecedor'																					AS tipo,
		 ettiva																								AS iva,
		 ettiliq																							AS base_inc	 
	FROM [logs_com] (NOLOCK)	
		inner join fo (NOLOCK)	 ON [logs_com].regstamp = fo.fostamp and origem ='fo'
	 where [logs_com].tipo=@tipo and [logs_com].site in (Select items from dbo.up_splitToTable(@site, ',')) 
	 		and CONVERT(varchar, [logs_com].data, 112) between @dataIni and @dataFim
		    and ('Doc. Fornecedor' in (Select items from dbo.up_splitToTable(@docTipo, ',')) or  @docTipo ='') 

	UNION ALL 

	 SELECT distinct
		 desc1																								AS  nDOC, 
		 cast(rno as varchar(20))																			AS  num, 
		[logs_com].data																						AS  dateExp , 
		 no																									AS  no,
		 estab																								AS estab, 
		 nome																								AS nome,
		 eTotal																								AS total,
		 po.rdata																							AS date,
		 cast(texto as varchar(max))																		AS param,
		 'Doc. Pagamentos'																					AS tipo,
		 eivav1 + eivav2 + eivav3 + eivav4 + eivav5 + eivav6 + eivav7 + eivav8 + eivav9						AS iva,
		 eTotal - ( eivav1 + eivav2 + eivav3 + eivav4 + eivav5 + eivav6 + eivav7 + eivav8 + eivav9		)	AS base_inc
	FROM [logs_com] (NOLOCK)	
		inner join po (NOLOCK)	 ON [logs_com].regstamp = po.postamp and origem ='po'
	 where [logs_com].tipo=@tipo and [logs_com].site in (Select items from dbo.up_splitToTable(@site, ',')) 
	 		and CONVERT(varchar, [logs_com].data, 112) between @dataIni and @dataFim
			and ('Doc. Pagamentos' in (Select items from dbo.up_splitToTable(@docTipo, ',')) or  @docTipo ='') 
	) X
	ORDER BY  X.dateExp ASC


END

GO
Grant Execute on dbo.up_relatorio_logs_com to Public
Grant control on dbo.up_relatorio_logs_com to Public
GO