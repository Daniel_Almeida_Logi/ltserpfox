/* 
	 exec up_relatorio_vendasClientesPorMarcaEntreDatas '20180930','20190930','Medela','TODAS'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_vendasClientesPorMarcaEntreDatas]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendasClientesPorMarcaEntreDatas
go

create procedure dbo.up_relatorio_vendasClientesPorMarcaEntreDatas

@dataini	DATETIME,
@datafim	DATETIME,
@usr1		VARCHAR(254),
@site		VARCHAR(55)

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

	if(@site='TODAS')
		set @site=''

	SELECT DISTINCT  
		no		 = b_utentes.no, 
		estab	 = b_utentes.estab, 
		nome	 = b_utentes.nome, 
		telefone = b_utentes.telefone, 
		tlmvl	 = b_utentes.tlmvl, 
		email	 = b_utentes.email
	FROM fi (NOLOCK)
		INNER JOIN ft		 (NOLOCK) ON ft.ftstamp=fi.ftstamp AND ft.fdata BETWEEN @dataini AND @datafim 
        INNER JOIN td (nolock) on ft.ndoc = td.ndoc 
		INNER JOIN b_utentes (NOLOCK) ON b_utentes.no=ft.no AND b_utentes.estab=ft.estab AND b_utentes.inactivo=0
	WHERE ref IN (SELECT DISTINCT ref FROM st (NOLOCK) WHERE usr1 like '%'+@usr1+'%')
			AND fi.rdata BETWEEN @dataini AND @datafim
			AND ft.site = (case when @site = '' Then ft.site else @site end)
			AND td.tipodoc = 1
			AND ft.no > 200

	ORDER BY no, estab

Grant Execute on dbo.up_relatorio_vendasClientesPorMarcaEntreDatas to Public
Grant control on dbo.up_relatorio_vendasClientesPorMarcaEntreDatas to Public
GO