﻿/* Relatório Evolucao Vendas 
	exec up_relatorio_quotas_mercado '20200101', '20200621', 'Loja 1','','','','','','','',0
	exec up_relatorio_quotas_mercado '20200101', '20200621', 'Loja 1','','','','','','','NĂO GENERICO',0
	exec up_relatorio_quotas_mercado '20200101', '20200621', 'Loja 1','','','','','','','GENERICO',0
	exec up_relatorio_quotas_mercado '20210101', '20210621', 'Loja 1','','','','','','','',0
*/

if OBJECT_ID('[dbo].[up_relatorio_quotas_mercado]') IS NOT NULL
	drop procedure dbo.up_relatorio_quotas_mercado
go

create procedure [dbo].[up_relatorio_quotas_mercado]
	 @dataIni			DATETIME
	,@dataFim			DATETIME
	,@site				VARCHAR(55)
	,@familia			VARCHAR(60)
	,@lab				VARCHAR(120)
	,@marca				VARCHAR(200)
	,@tipoProduto		VARCHAR(254) 
	,@fornecedor		VARCHAR(80) = ''
	,@grphmgcode	    VARCHAR(6)
	,@generico		varchar(18) = ''
	,@t4t5				BIT = 0


/* with encryption */

AS
SET NOCOUNT ON

	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseHomo'))
		DROP TABLE #dadosVendasBaseHomo
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseCalculos'))
		DROP TABLE #dadosVendasBaseCalculos
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseHomoCalculos'))
		DROP TABLE #dadosVendasBaseHomoCalculos
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendaCalculo'))
		DROP TABLE #dadosVendaCalculo
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseRef'))
		DROP TABLE #dadosVendasBaseRef
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseHomoRef'))
		DROP TABLE #dadosVendasBaseHomoRef


	DECLARE @dataIniHomo			DATETIME
	DECLARE @dataFimHomo			DATETIME
	DECLARE @totalVendasQtt		    NUMERIC(15,0) 
	DECLARE @totalVendasQttHomo		NUMERIC(15,0) 
	DECLARE @totalVendasValor		NUMERIC(20,4) 		
	DECLARE @totalVendasValorHomo	NUMERIC(20,4) 	

	SET @dataIniHomo = DATEADD(YEAR,-1,@dataIni)
	set @dataFimHomo = DATEADD(YEAR,-1,@dataFim)


	IF @generico = 'GENERICO'
		set @generico = 1
	IF @generico = 'NĂO GENERICO'
		set @generico = 0	

		/* Calc Loja e Armazens */
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)

	select 
		armazem
	into
		#Armazens
	From
		empresa (nolock)
		inner join empresa_arm (nolock) on empresa.no = empresa_arm.empresa_no
	where
		empresa.site = case when @site = '' then empresa.site else @site end		

	
	SELECT
		sl.ref ,
		SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END ) AS STOCK_DATA
	INTO
		#dadosStockData
	FROM	
		 st (nolock) 
		 LEFT join sl (nolock) ON  st.REF = SL.ref AND st.site_nr= @site_nr
	WHERE	
		sl.ref = st.ref
		and sl.datalc <= @dataFim 
		and sl.armazem in (select armazem from #Armazens)
	GROUP BY sl.ref


	/* Dados Base Vendas  */
	create table #dadosVendasBase (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)
	)

	create table #dadosVendasBaseHomo (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)
	)


	declare @Entidades as bit
	set @Entidades = case when (select  tipoempresa from empresa where site = @site) in ('FARMACIA', 'PARAFARMACIA') then convert(bit,1) else convert(bit,0) end

    insert #dadosVendasBase
    exec up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site

    insert #dadosVendasBaseHomo
    exec up_relatorio_vendas_base_detalhe @dataIniHomo, @dataFimHomo, @site

	If @Entidades = 1
	delete from #dadosVendasBase where #dadosVendasBase.no < 199


	
	SELECT
		#dadosVendasBase.REF																																	AS 	REF,  
		st.design																																				AS DESIGNACAO,
	    ISNULL(CASE WHEN fprod.pvporig <= fprod.preco_acordo and fprod.preco_acordo > 0 THEN 'T4'
						WHEN ranking=1 and grphmgcode != 'GH0000' and grphmgcode != '' and fprod.pvporig <= fprod.pvpmaxre THEN 'T5'  else '' END,'')			AS T4T5 ,
		ISNULL(CASE WHEN fprod.pvporig <= fprod.preco_acordo and fprod.preco_acordo > 0 THEN 1
						WHEN ranking=1 and grphmgcode != 'GH0000' and grphmgcode != '' and fprod.pvporig <= fprod.pvpmaxre THEN 1   END,0)						AS T4T5Bit ,
		ISNULL(etiliquidoSiva + ettent1siva + ettent2siva,0)																									AS PVenda,
																													 
		#dadosVendasBase.ecusto																																	AS PCUSTO,
		#dadosVendasBase.qtt																																	AS qttVendas,						
		#dadosVendasBase.ftstamp																																AS ftstamp,
		#dadosVendasBase.tipodoc																																AS tipodoc,
		ROW_NUMBER() OVER(PARTITION BY #dadosVendasBase.ref ORDER BY #dadosVendasBase.fdata + #dadosVendasBase.ousrhora DESC)									AS id
	into #dadosVendasBaseCalculos
	FROM #dadosVendasBase
		INNER join st	(nolock) ON st.ref= #dadosVendasBase.REF and st.site_nr = @site_nr
		LEFT join FPROD (nolock) ON fprod.cnp=#dadosVendasBase.ref

	WHERE 
		 (@familia = '' or @familia = '0'  or #dadosVendasBase.familia in (Select items from dbo.up_splitToTable(@familia,',')))
		 	AND st.u_lab = CASE WHEN @lab = '' THEN st.u_lab ELSE @lab END 
			AND st.usr1 = CASE WHEN @marca = '' THEN usr1 ELSE @marca END
			AND grphmgcode = CASE WHEN @grphmgcode = '' THEN grphmgcode ELSE @grphmgcode END 
			AND (@tipoProduto = '' or @tipoProduto = '0'  or st.u_famstamp IN (Select items from dbo.up_splitToTable(@tipoProduto,',') ))
			AND st.fornecedor = CASE WHEN @fornecedor  = '' THEN st.fornecedor ELSE @fornecedor END 
			AND ISNULL(fprod.generico,0)= case when @generico = '' then ISNULL(fprod.generico,0) else @generico end

	SELECT 
		#dadosVendasBaseHomo.REF																																AS 	REF,
		st.design																																			    AS DESIGNACAO,
	    ISNULL(CASE WHEN fprod.pvporig <= fprod.preco_acordo and fprod.preco_acordo > 0 THEN 'T4'
						WHEN ranking=1 and grphmgcode != 'GH0000' and grphmgcode != '' and fprod.pvporig <= fprod.pvpmaxre THEN 'T5'  else '' END,'')			AS T4T5 ,
		ISNULL(CASE WHEN fprod.pvporig <= fprod.preco_acordo and fprod.preco_acordo > 0 THEN 1
						WHEN ranking=1 and grphmgcode != 'GH0000' and grphmgcode != '' and fprod.pvporig <= fprod.pvpmaxre THEN 1   END,0)						AS T4T5Bit ,
		ISNULL(etiliquidoSiva + ettent1siva + ettent2siva,0)																									AS PVenda,		
		#dadosVendasBaseHomo.ecusto																																AS PCUSTO,
		#dadosVendasBaseHomo.qtt																																AS qttVendas,					
		#dadosVendasBaseHomo.ftstamp																															AS ftstamp,
		#dadosVendasBaseHomo.tipodoc																															AS tipodoc,
		ROW_NUMBER() OVER(PARTITION BY #dadosVendasBaseHomo.ref ORDER BY #dadosVendasBaseHomo.fdata + #dadosVendasBaseHomo.ousrhora DESC)						AS id	
	into #dadosVendasBaseHomoCalculos
	FROM  #dadosVendasBaseHomo 
		INNER join st	(nolock) ON st.ref= #dadosVendasBaseHomo.REF  and st.site_nr = @site_nr
		LEFT join FPROD (nolock) ON fprod.cnp=#dadosVendasBaseHomo.ref
	WHERE 
		 (@familia = '' or @familia = '0'  or #dadosVendasBaseHomo.familia in (Select items from dbo.up_splitToTable(@familia,',')))
		 	AND st.u_lab = CASE WHEN @lab = '' THEN st.u_lab ELSE @lab END 
			AND st.usr1 = CASE WHEN @marca = '' THEN usr1 ELSE @marca END
			AND grphmgcode = CASE WHEN @grphmgcode = '' THEN grphmgcode ELSE @grphmgcode END 
			AND (@tipoProduto = '' or @tipoProduto = '0'  or st.u_famstamp IN (Select items from dbo.up_splitToTable(@tipoProduto,',') ))
			AND st.fornecedor = CASE WHEN @fornecedor  = '' THEN st.fornecedor ELSE @fornecedor END 
			AND ISNULL(fprod.generico,0)= case when @generico = '' then ISNULL(fprod.generico,0) else @generico end
		-- Valores

	
		SELECT
			cal.REF																																		AS REF,  
			cal.DESIGNACAO																																AS DESIGNACAO,
			cal.T4T5																																	AS T4T5,
			AUX.PVenda																																	AS PVenda,
			SUM(cal.PVenda)																																AS Vendas,																													 
			AUX.PCUSTO																																	AS PCUSTO,
			Sum(cal.qttVendas)																															AS qttVendas,						
			(select count(ftstamp) from #dadosVendasBaseCalculos where 	#dadosVendasBaseCalculos.ref= cal.ref and #dadosVendasBaseCalculos.tipodoc !=3)	AS nVendas
	into #dadosVendasBaseRef
	FROM #dadosVendasBaseCalculos cal
		INNER JOIN #dadosVendasBaseCalculos AUX ON AUX.REF= cal.REF AND AUX.id = 1
		WHERE
			cal.T4T5Bit = (CASE WHEN @t4t5 = 0 THEN cal.T4T5Bit ELSE  @t4t5 END)
	GROUP BY cal.REF, cal.DESIGNACAO, cal.T4T5 , AUX.PVenda , AUX.PCUSTO	

	SELECT
		cal.REF																																					 AS REF,  
		cal.DESIGNACAO																																			 AS DESIGNACAO,
		cal.T4T5																																				 AS T4T5,
		AUX.PVenda																																				 AS PVenda,
		SUM(cal.PVenda)																																			 AS Vendas,																													 
		AUX.PCUSTO																																				 AS PCUSTO,
		Sum(cal.qttVendas)																																		 AS qttVendas,						
		(select count(ftstamp) from #dadosVendasBaseHomoCalculos where 	#dadosVendasBaseHomoCalculos.ref= cal.ref and #dadosVendasBaseHomoCalculos.tipodoc !=3)	 AS nVendas
	into #dadosVendasBaseHomoRef
	FROM #dadosVendasBaseHomoCalculos cal
		INNER JOIN #dadosVendasBaseHomoCalculos AUX ON AUX.REF= cal.REF AND AUX.id = 1
		WHERE
			cal.T4T5Bit = (CASE WHEN @t4t5 = 0 THEN cal.T4T5Bit ELSE  @t4t5 END)
	GROUP BY cal.REF, cal.DESIGNACAO, cal.T4T5 , AUX.PVenda , AUX.PCUSTO

	select @totalVendasQtt = SUM(qttVendas) ,@totalVendasValor=SUM(Vendas)   from #dadosVendasBaseRef where qttvendas !=0
	select @totalVendasQttHomo = SUM(qttVendas) ,@totalVendasValorHomo=SUM(Vendas)   from #dadosVendasBaseHomoRef where qttvendas !=0
	
	SELECT  
		ISNULL(#dadosVendasBaseRef.REF, #dadosVendasBaseHomoRef.REF)																																	AS REF , 
		ISNULL(#dadosVendasBaseRef.DESIGNACAO , #dadosVendasBaseHomoRef.DESIGNACAO)																														AS DESIGNACAO,
		ISNULL(#dadosVendasBaseRef.T4T5 , #dadosVendasBaseHomoRef.T4T5)																																	AS T4T5,	
		ISNULL(#dadosVendasBaseRef.PVenda , 0)																																							AS PVenda,
		ISNULL(#dadosVendasBaseRef.Vendas , 0)																																							AS Vendas,
		CASE WHEN ISNULL(@totalVendasValor,0) !=0 THEN  (ISNULL(#dadosVendasBaseRef.Vendas,0) / @totalVendasValor)*100 ELSE ISNULL(#dadosVendasBaseRef.Vendas,0) END 									AS QuotaValor,
		CASE WHEN ISNULL(@totalVendasValorHomo,0) !=0 THEN (ISNULL(#dadosVendasBaseHomoRef.Vendas,0) / @totalVendasValorHomo)*100 ELSE	ISNULL(#dadosVendasBaseHomoRef.Vendas,0) END					AS QuotaHomoValor,	 
		ISNULL(#dadosVendasBaseRef.PCUSTO,0)																																							AS Custo,
		ISNULL(#dadosVendasBaseHomoRef.PCUSTO,0)																																						AS CustoHomo,
		ISNULL(#dadosVendasBaseRef.PVenda,0) - ISNULL(#dadosVendasBaseRef.PCUSTO,0)																														AS BemBruto,
		ISNULL(#dadosVendasBaseHomoRef.PVenda,0) - ISNULL(#dadosVendasBaseHomoRef.PCUSTO,0)																												AS BemBrutoHomo,
		ISNULL(#dadosVendasBaseRef.qttVendas,0)																																							AS vendasUni,
		CASE WHEN @totalVendasQtt!=0 then (ISNULL(#dadosVendasBaseRef.qttVendas,0)/@totalVendasQtt) *100 else 	ISNULL(#dadosVendasBaseRef.qttVendas,0) end												AS QuotaQTT,
		CASE WHEN @totalVendasQttHomo!=0 then(ISNULL(#dadosVendasBaseHomoRef.qttVendas,0)/@totalVendasQttHomo) *100	else ISNULL(#dadosVendasBaseHomoRef.qttVendas,0) end								AS QuotaHomoQTT,
		CASE WHEN ISNULL(#dadosVendasBaseRef.nVendas,1) !=0 then ISNULL(#dadosVendasBaseRef.qttVendas,0)/ISNULL(#dadosVendasBaseRef.nVendas,1)	else 	ISNULL(#dadosVendasBaseRef.qttVendas,0) end 	AS media,
		ISNULL(#dadosStockData.STOCK_DATA,0)																																							AS stock			
	from  	#dadosVendasBaseRef
		FULL OUTER JOIN #dadosVendasBaseHomoRef ON #dadosVendasBaseRef.REF = #dadosVendasBaseHomoRef.REF
		INNER JOIN #dadosStockData    ON #dadosStockData.REF = ISNULL(#dadosVendasBaseRef.REF, #dadosVendasBaseHomoRef.REF)	




	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadorSuma'))
		DROP TABLE #dadosOperadorSuma
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseHomo'))
		DROP TABLE #dadosVendasBaseHomo
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendaCalculo'))
		DROP TABLE #dadosVendaCalculo
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseRef'))
		DROP TABLE #dadosVendasBaseRef
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseHomoRef'))
		DROP TABLE #dadosVendasBaseHomoRef

GO
Grant Execute on dbo.up_relatorio_quotas_mercado to Public
Grant control on dbo.up_relatorio_quotas_mercado to Public
GO