/*
	exec up_relatorio_stocks_existencias5MaisBaratos  '','Loja 1'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_stocks_existencias5MaisBaratos]') IS NOT NULL
	drop procedure dbo.up_relatorio_stocks_existencias5MaisBaratos
go

create procedure dbo.up_relatorio_stocks_existencias5MaisBaratos
@gh	varchar(max),
@site		varchar(55)

/* WITH ENCRYPTION */ 

AS
BEGIN


	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#EmpresaArm'))
		DROP TABLE #EmpresaArm
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#CteDados'))
		DROP TABLE #CteDados	

	select 
		armazem 
	into
		#EmpresaArm
	from 
		empresa 
		inner join empresa_arm on empresa.no = empresa_arm.empresa_no 
	where 
		empresa.site = @site

	select
		fprod.ref
		,fprod.design
		,grphmgcode
		,grphmgdescr
		,pvpdic = pvpcalc
		,ranking
		,stock = ISNULL(sa.stock,0)
		,existeSt = case when st.ref is not null then 'SIM' else 'NÃO' end
	into
		#CteDados	
	from 
		fprod (nolock)
		left join sa (nolock) on fprod.cnp=sa.ref
		left join st (nolock) on fprod.cnp=st.ref and st.site_nr=(select no from empresa where site=@site)
	where
		sa.armazem in (select armazem from #EmpresaArm)
		and (grphmgcode != 'gh0000' and grphmgcode != '')
		and ranking = 1
		and (grphmgcode in (Select items from dbo.up_splitToTable(@gh,',')) or @gh = '')


	Select
		nRefsCStock = (select COUNT(distinct ref) from #CteDados a where a.grphmgcode = #CteDados.grphmgcode and a.stock >0)
		,*
	From
		#CteDados
	order by
		grphmgcode asc
		,pvpdic asc
		,stock desc
END

GO
Grant Execute on dbo.up_relatorio_stocks_existencias5MaisBaratos to Public
Grant control on dbo.up_relatorio_stocks_existencias5MaisBaratos to Public
Go
