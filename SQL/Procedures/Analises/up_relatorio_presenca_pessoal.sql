/* Relat�rio presen�a pessoal 
	
	exec up_relatorio_presenca_pessoal '20220318','20220401','0', '999','Loja 1'
	exec up_relatorio_presenca_pessoal '20220402','20220406','0', '999','Loja 1'
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_presenca_pessoal]') IS NOT NULL
	drop procedure dbo.up_relatorio_presenca_pessoal
go

create procedure [dbo].up_relatorio_presenca_pessoal
	 @dataIni DATETIME
	,@dataFim DATETIME
	,@opIni	as numeric(9,0)
	,@opFim	as numeric(9,0)
	,@site varchar(30)

/* with encryption */
AS


	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosHorario'))
		DROP TABLE #dadosHorario

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosPicaPonto'))
		DROP TABLE #dadosPicaPonto

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosPeriodo'))
		DROP TABLE #dadosPeriodo



	;WITH DateRange(DateData) AS 
	(
		SELECT @dataIni as Date
		UNION ALL
		SELECT 
		DATEADD(d,1,DateData) as DateData
		FROM DateRange 
		WHERE DateData < @dataFim
	)
	SELECT 
		convert(varchar,DateData,102) as date,
		DATENAME(weekday,DateData)  AS WEEKEND,
		row = row_number() over(partition by DateData,no order by DateData,horaInicio ASC),
		CONVERT(char(8),CONVERT( TIME(0), horaInicio), 108) horaInicio,
		CONVERT(char(8),CONVERT( TIME(0), horafim), 108) horafim,
		no as op
	INTO #dadosHorario
	FROM DateRange
	INNER JOIN b_series  ON ((DATENAME(weekday,DateData) = (case when (b_series.segunda=1) then 'Monday' end ) and  dataIniRep between @dataIni and @dataFim and   dataFimRep >= @dataIni and no >=@opIni and no <=@opFim)
							or( DATENAME(weekday,DateData) =	(case when (b_series.terca=1) then 'Tuesday' end )and dataIniRep between @dataIni and @dataFim and   dataFimRep >= @dataIni and no >=@opIni and no <=@opFim)
							or (DATENAME(weekday,DateData) = (case when (b_series.quarta=1) then 'Wednesday' end )and dataIniRep between @dataIni and @dataFim and   dataFimRep >= @dataIni and no >=@opIni and no <=@opFim)
							or (DATENAME(weekday,DateData) = (case when (b_series.quinta=1) then 'Thursday' 	end )and dataIniRep between @dataIni and @dataFim and   dataFimRep >= @dataIni and no >=@opIni and no <=@opFim)											
							or (DATENAME(weekday,DateData) =	(case when (b_series.sexta=1) then 'Friday' 	end )and dataIniRep between @dataIni and @dataFim and   dataFimRep >= @dataIni and no >=@opIni and no <=@opFim)											
							or (DATENAME(weekday,DateData) =	(case when (b_series.sabado=1) then 'Saturday' end )and dataIniRep between @dataIni and @dataFim and   dataFimRep >= @dataIni and no >=@opIni and no <=@opFim)												
							or (DATENAME(weekday,DateData) =	(case  when (b_series.domingo=1) then 'Sunday' 	end )and dataIniRep between @dataIni and @dataFim and   dataFimRep >= @dataIni and no >=@opIni and no <=@opFim))											
	where 
	dataIniRep between @dataIni and @dataFim and   dataFimRep >= @dataIni and no >=@opIni  and no <=@opFim and no!=0
	order by DateData asc,
			horaInicio ASC,
			horafim ASC

	
	SELECT 
		CONVERT(varchar,inOutDate,102) as date,
		CONVERT(char(8), inOutDate, 108)  as entradas,
		(select top 1 convert(char(8), inOutDate, 108) from clock_response_payload_line as outdate where outdate.inOutCode = 1 and convert(varchar,outdate.inOutDate,102) = convert(varchar,line.inOutDate,102) and outdate.inOutDate >= line.inOutDate  AND userId >=@opIni and userId <=@opFim order by outdate.inOutDate asc) as saidas,
		row = row_number() over(partition by convert(varchar,line.inOutDate,102),userId order by inOutDate ASC),
		userId as op
	INTO
		#dadosPicaPonto
	FROM clock_response_payload_line line
	WHERE line.inOutCode = 0 and line.inOutDate between @dataIni and @dataFim AND userId >=@opIni and userId <=@opFim and userId!=0
	ORDER BY  inOutDate ASC

SELECT 
		dia,
		previstoEntrada,
		RegistadoEntrada,
		previstoSaida,
		RegistadoSaida,
		Extras,
		Faltas,
		Assiduidade,
		tipo,
		turno,
		previstoms,
		registadoms,
		AssiduidadeCont = (case  when (Assiduidade>0) then 1 else 0 end ),
		Cont = case when (tipo='TR' )then  1 else 0 end ,
		operador,
		registaMSExtra,
		registaMSFalta
	INTO #dadosPeriodo
	FROM (SELECT 
			DAY(#dadosHorario.date)																																													AS dia,
			#dadosHorario.horaInicio																																												AS previstoEntrada,
			#dadosPicaPonto.entradas																																												AS RegistadoEntrada,
			#dadosHorario.horafim																																													AS previstoSaida,
			#dadosPicaPonto.saidas																																													AS RegistadoSaida,
			''																																																		AS Extras,
			''																																																		AS Faltas,
			DATEDIFF(MS, #dadosHorario.horaInicio, #dadosPicaPonto.entradas)																																		AS Assiduidade,
			'TR'																																																	AS tipo,
			#dadosHorario.row																																														AS turno,
			DATEDIFF(MS, #dadosHorario.horaInicio, #dadosHorario.horafim)																																			AS previstoms,
			DATEDIFF(MS, #dadosPicaPonto.entradas, #dadosPicaPonto.saidas)																																			AS registadoms,
			#dadosHorario.op																																														AS operador,
			0																																																		AS registaMSExtra,
			0																																																		AS registaMSFalta
		FROM 
			#dadosHorario 
			INNER JOIN  #dadosPicaPonto ON (#dadosPicaPonto.date = #dadosHorario.date AND #dadosPicaPonto.row = #dadosHorario.row and #dadosPicaPonto.op = #dadosHorario.op )
	
		UNION ALL 
	
		SELECT 
			DAY(#dadosHorario.date)																																													AS dia,
			#dadosHorario.horaInicio																																												AS previstoEntrada,
			''																																																		AS RegistadoEntrada,
			#dadosHorario.horafim																																													AS previstoSaida,
			''																																																		AS RegistadoSaida,
			''																																																		AS Extras,
			CASE WHEN (#dadosHorario.horaInicio <#dadosHorario.horafim ) THEN (SELECT CONVERT(VARCHAR(8), DATEADD(MS, DATEDIFF(MS, #dadosHorario.horaInicio, #dadosHorario.horafim), 0), 114) ) ELSE '' END			AS Faltas,
			''																																																		AS Assiduidade,
			'F'																																																		AS tipo,
			#dadosHorario.row																																														AS turno,
			0																																																		AS previstoms,
			0																																																		AS registadoms,
			#dadosHorario.op																																														AS operador,
			0																																																		AS registaExtra,
			DATEDIFF(MS, #dadosHorario.horaInicio, #dadosHorario.horafim)																																			AS registaMSFalta
		FROM 
			#dadosHorario 
				LEFT JOIN #dadosPicaPonto  ON (#dadosPicaPonto.date = #dadosHorario.date AND #dadosPicaPonto.row = #dadosHorario.row   and #dadosPicaPonto.op = #dadosHorario.op)
		WHERE   #dadosPicaPonto.entradas IS NULL and #dadosPicaPonto.saidas is null and #dadosHorario.op IS NOT NULL
	
	
			UNION ALL 
	
		SELECT 
			DAY(#dadosPicaPonto.date)																																												AS dia,
			''																																																		AS previstoEntrada,
			#dadosPicaPonto.entradas																																												AS RegistadoEntrada,
			''																																																		AS previstoSaida,
			#dadosPicaPonto.saidas																																													AS RegistadoSaida,
			CASE WHEN (#dadosPicaPonto.entradas <#dadosPicaPonto.saidas ) THEN (SELECT CONVERT(VARCHAR(8), DATEADD(MS, DATEDIFF(MS, #dadosPicaPonto.entradas, #dadosPicaPonto.saidas), 0), 114) ) ELSE '' END		AS Extras,
			''																																																		AS Faltas,
			''																																																		AS Assiduidade,
			'EX'																																																	AS tipo,
			#dadosPicaPonto.row																																														AS turno,
			0																																																		AS previstoms,
			DATEDIFF(MS, #dadosPicaPonto.entradas, #dadosPicaPonto.saidas)																																			AS registadoms,
			#dadosPicaPonto.op																																														AS operador,
			DATEDIFF(MS, #dadosPicaPonto.entradas, #dadosPicaPonto.saidas)																																			AS registaMSExtra,
			0																																																		AS registaMSFalta
		FROM 
			#dadosPicaPonto  
				LEFT JOIN #dadosHorario  ON (#dadosHorario.date = #dadosPicaPonto.date AND #dadosHorario.row = #dadosPicaPonto.row and #dadosPicaPonto.op = #dadosHorario.op)
		WHERE   #dadosHorario.horaInicio IS NULL and #dadosHorario.horafim is null  and #dadosPicaPonto.op IS NOT NULL
	) dum


	SELECT 
		(select nome from b_us where userno= #dadosPeriodo.operador)																																							AS nome,
		Cast(convert(varchar,(SUM(#dadosPeriodo.previstoms) /(1000))/(60*60))AS decimal)																																		AS previsto,																																		
		Cast(convert(varchar,(SUM(#dadosPeriodo.registaMSExtra) /(1000))/(60*60))AS decimal)																																	AS extra,																																		
		Cast(convert(varchar,(SUM(#dadosPeriodo.registadoms) /(1000))/(60*60))AS decimal)																																		AS registo,
		Cast(0	AS decimal)																																																		AS ferias,
		Cast(convert(varchar,(SUM(#dadosPeriodo.registaMSFalta) /(1000))/(60*60))AS decimal)																																	AS faltas,
		Cast(convert(varchar,(SUM(#dadosPeriodo.Assiduidade) /(1000))/(60)) AS decimal)																																			AS total,
		Cast(CASE WHEN (SUM(#dadosPeriodo.AssiduidadeCont)>0) then convert(varchar,(SUM(#dadosPeriodo.Assiduidade) /(1000))/(60)) /SUM(#dadosPeriodo.AssiduidadeCont) else 0 end AS decimal)									AS media,
	    CASE WHEN (SUM(#dadosPeriodo.Cont)>0) then convert(varchar,CAST(cast(SUM(#dadosPeriodo.AssiduidadeCont) as decimal)/cast(SUM(#dadosPeriodo.Cont)	as decimal)	* 100 AS DECIMAL(10,2))) else '0.00'end  +'%' 			AS numero
	FROM 																	
		#dadosPeriodo
	where #dadosPeriodo.operador !=0
	 GROUP BY #dadosPeriodo.operador
	 ORDER BY  (select nome from b_us where userno= #dadosPeriodo.operador)	 asc




	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosHorario'))
		DROP TABLE #dadosHorario

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosPicaPonto'))
		DROP TABLE #dadosPicaPonto

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosPeriodo'))
		DROP TABLE #dadosPeriodo


GO
Grant Execute On dbo.up_relatorio_presenca_pessoal to Public
Grant control On dbo.up_relatorio_presenca_pessoal to Public
GO