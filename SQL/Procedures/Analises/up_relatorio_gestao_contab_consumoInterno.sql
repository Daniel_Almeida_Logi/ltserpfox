/* Relatório Contabilidade Consumo Interno

	exec up_relatorio_gestao_contab_consumoInterno '20180101', '20180301','', 'Loja 1'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_contab_consumoInterno]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_contab_consumoInterno
go

create procedure dbo.up_relatorio_gestao_contab_consumoInterno
@dataIni	as datetime,
@dataFim	as datetime,
@iva		as varchar(9),
@site	varchar(60)

/* WITH ENCRYPTION */ 
AS
BEGIN
	
SELECT	
	EMPRESA,
	LOJA = empresa.site,
	BO.DATAOBRA as DATA,
	BO.NMDOS as DOCUMENTO,
	BO.OBRANO as NUMDOC,
	BI.REF,
	DESIGN,
	QTT,
	UNIDADE,
	U_UPC as PCU,
	IVA = isnull(bi.iva,0),
	U_UPC * QTT as TOTAL, 
	BO.OBS,
	cast((U_UPC*QTT)*(isnull(bi.iva,0)/100) as numeric(10,2)) as valiva
FROM	
	BO (nolock) 
	INNER JOIN BI (nolock) ON BO.bostamp = BI.BOSTAMP
	left join empresa_arm (nolock) on bi.armazem = empresa_arm.armazem
	left join empresa (nolock) on empresa.no = empresa_arm.empresa_no
WHERE	
	BO.nmdos = 'Consumo Interno'
	and BO.DATAOBRA BETWEEN @dataIni AND @dataFim
	and empresa.site = (case when @site = '' Then empresa.site else @site end)
	and isnull(bi.iva,0) = case when @iva = '' then isnull(bi.iva,0) else convert(numeric(9,2),@iva) end
		
END

GO
Grant Execute on dbo.up_relatorio_gestao_contab_consumoInterno to Public
Grant control on dbo.up_relatorio_gestao_contab_consumoInterno to Public
GO
