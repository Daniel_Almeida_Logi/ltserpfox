
/*
	exec up_relatorio_comprasFornecedores '20240627','20240627','','Loja 1',0,0,'',''

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_comprasFornecedores]') IS NOT NULL
	drop procedure dbo.up_relatorio_comprasFornecedores
go

create procedure [dbo].[up_relatorio_comprasFornecedores]
	@dataIni			AS datetime 
	,@dataFim			AS datetime	
	,@fornecedor		AS varchar(254)
	,@site				AS varchar(55)
	,@valorCompras		AS numeric(10,0) = 0
	,@incliva		    AS bit
	,@lab				AS varchar(254)	
	,@generico			AS varchar(18) = ''
/* with encryption */
AS 
BEGIN

	set @dataIni = @dataIni + '00:00:00:01'
    set @dataFim = @dataFim + '23:59:59'

	IF @generico = 'GENERICO'
		set @generico = 1
	ELSE IF @generico = 'NÃO GENERICO'
		set @generico = 0

	declare @siteNr int
	select @siteNr = empresa_arm.empresa_no
	from empresa(nolock) 
	inner join empresa_arm(nolock) on empresa.no = empresa_arm.empresa_no
	where empresa.site = @site

	declare @total as numeric(20,5)
	set @total = (
		select 
			CASE WHEN  @incliva = '1' THEN SUM(fn.etiliquido)  ELSE  SUM((fn.etiliquido / (fn.iva/100+1))) END 
		from 
		fo (nolock) 
		INNER JOIN FN (nolock) ON FO.FOSTAMP = FN.FOSTAMP
		LEFT JOIN ST (nolock) on FN.ref = st.ref and st.site_nr = @siteNr
		LEFT JOIN fl (nolock) on fl.no = fo.no and fl.estab=fo.estab
		LEFT JOIN fprod (nolock) on st.ref = fprod.cnp
		inner join cm1(nolock) on fo.docnome = cm1.codigoDocDesc
		where 
		fo.data between @dataIni and @dataFim
		and fo.site = (case when @site = '' Then fo.site else @site end)
		and (isnull(convert(varchar(254), fo.[no]),'') in (Select items from dbo.up_splitToTable(@Fornecedor,',')) or @Fornecedor = '')
		and(isnull( st.u_lab,'') in (Select items from dbo.up_splitToTable(@lab,';')) or @lab = '')
		and cm1.codigoDoc in (55, 56, 83, 81)	
		and  FN.qtt >0
		and isnull(fprod.generico,0) = case when @generico = '' then isnull(fprod.generico,0) else @generico end
		having sum(case when doccode>50 then fo.ettiliq else -fo.ettiliq end) >= @valorCompras
	)

	select 
		 NoFornecedor = fo.no
		,NoCont = fl.ncont
		,fo.nome
		,percentDoTotal =  ROUND((CASE WHEN fn.etiliquido = 0 then 0 else((CASE WHEN  @incliva = '1' 
							THEN  (CASE WHEN fn.etiliquido = 0 THEN 1 ELSE fn.etiliquido end) 
							 ELSE  ( (CASE WHEN fn.etiliquido = 0 THEN 1 ELSE fn.etiliquido end) 
								/ ISNULL((fn.iva/100+1),1)) END ) * 100) / (CASE WHEN @total = 0 THEN 1 else @total end) end ),2)
		,FN.ref
		,FN.design
		,FN.qtt																						AS quantidade
		,convert(varchar(10),fn.data,120)															AS dataVenda
		,ROUND(CASE WHEN  @incliva = '1' THEN ISNULL((ISNULL(SL.evu,fn.eslvu) * (fn.iva/100+1)),0) 
									ELSE ISNULL(SL.evu,fn.eslvu)  END	,2)				AS 'PCL'
		,fn.iva
		,fn.epv 																					
		,(CASE WHEN @total = 0 THEN 1 else @total end)
		,CASE WHEN  @incliva = '1' THEN (fn.etiliquido * (fn.iva/100+1)) ELSE  fn.etiliquido END	AS pvFicha
	from 
		fo (nolock) 
		INNER JOIN FN (nolock) ON FO.FOSTAMP = FN.FOSTAMP
		LEFT JOIN SL (nolock) on sl.fnstamp = fn.fnstamp and sl.ref = fn.ref
		LEFT JOIN ST (nolock) on st.site_nr = 1 and FN.ref = st.ref  
		LEFT JOIN fprod (nolock) on st.ref = fprod.cnp
		LEFT JOIN fl (nolock) on fl.no = fo.no and fl.estab=fo.estab
		INNER JOIN cm1(nolock) on fo.docnome = cm1.codigoDocDesc
	where 
		fo.data between @dataIni and @dataFim
		and fo.site = (case when @site = '' Then fo.site else @site end)
		and (isnull(convert(varchar(254), fo.[no]),'') in (Select items from dbo.up_splitToTable(@Fornecedor,',')) or @Fornecedor = '')
		and (isnull( st.u_lab,'')		 in (Select items from dbo.up_splitToTable(@lab,';')) or @lab = '')
		and cm1.codigoDoc in (55, 56, 83, 81)	
		and (case when doccode>50 then fo.ettiliq else -fo.ettiliq end) >= @valorCompras 
		and  FN.qtt >0
		and isnull(fprod.generico,0) = case when @generico = '' then isnull(fprod.generico,0) else @generico end
	group by 
		fo.no, fl.ncont, fo.nome, fn.etiliquido, fn.iva, fn.ref, fn.design, fn.qtt, fn.data, sl.evu, fn.epv,fn.adoc, fn.eslvu
	order by dataVenda 
END

GO
Grant Execute on dbo.[up_relatorio_comprasFornecedores] to Public
Grant control on dbo.[up_relatorio_comprasFornecedores] to Public
GO