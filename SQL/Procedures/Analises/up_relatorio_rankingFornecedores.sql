-- exec up_relatorio_rankingFornecedores '20170301','20170310','','Loja 1'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_rankingFornecedores]') IS NOT NULL
	drop procedure dbo.up_relatorio_rankingFornecedores
go

create procedure dbo.up_relatorio_rankingFornecedores
@dataIni as datetime 
,@dataFim as datetime
,@fornecedor varchar(55)
,@site	as varchar(55)

/* with encryption */

AS 
BEGIN
		
	declare @total as numeric(9,3)

	set @total = (
		select 
			total = sum(case when cm1.debito=0 then fo.eivain else -fo.eivain end)
		from 
			fo (nolock) 
			inner join fo2 (nolock) on fo2.fo2stamp=fo.fostamp /*and fo2.anulado=0 */
			inner join cm1 (nolock) on cm1.cm=fo.doccode  
		where 
			fo.data between @dataIni and @dataFim
	)

	select 
		fornecedor = RTRIM(fo.nome)  + '[' + LTRIM(STR(fo.no)) + '][' +  LTRIM(STR(fo.estab))  +']'
		,sum(case when cm1.debito=0 then fo.eivain else -fo.eivain end) as evalor 
		,percentDoTotal = round(((sum(case when cm1.debito=0 then fo.eivain else -fo.eivain end) * 100)/@total),2)
	from 
		fo (nolock) 
		inner join fo2 (nolock) on fo2.fo2stamp=fo.fostamp /*and fo2.anulado=0 */
		inner join cm1 (nolock) on cm1.cm=fo.doccode  
	where 
		fo.data between @dataIni and @dataFim
		and fo.site = (case when @site = '' Then fo.site else @site end)
		AND fo.[no] = CASE WHEN @Fornecedor = 0 THEN fo.no ELSE @Fornecedor END
	group by 
		fo.nome
		,fo.no
		,fo.estab

END

GO
Grant Execute on dbo.up_relatorio_rankingFornecedores to Public
Grant control on dbo.up_relatorio_rankingFornecedores to Public
GO