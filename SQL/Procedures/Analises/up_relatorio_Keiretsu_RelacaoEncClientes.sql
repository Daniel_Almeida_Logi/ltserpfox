
-- exec up_relatorio_Keiretsu_RelacaoEncClientes 'ADM9EADE76F-7AF5-4EC5-9AC'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_Keiretsu_RelacaoEncClientes]') IS NOT NULL
	drop procedure dbo.up_relatorio_Keiretsu_RelacaoEncClientes
go

create procedure dbo.up_relatorio_Keiretsu_RelacaoEncClientes
@encstamp as uniqueidentifier
/* with encryption */
AS 
BEGIN
Select 
	boEncCliente.nmdos
	,boEncCliente.ndos
	,boEncCliente.dataobra
	,boEncCliente.obrano
	,nome = RTRIM(LTRIM(boEncCliente.nome)) + ' [' + LTRIM(STR(boEncCliente.no)) + ']' + '[' + LTRIM(STR(boEncCliente.estab)) + ']'
	,bi.ref
	,bi.design
	,bi.qtt
	,stock = isnull(st.stock,0)
From
	encKeiretsuLnk
	inner join bo (nolock) boEncCliente on encKeiretsuLnk.enccstamp = boEncCliente.bostamp
	inner join bi (nolock) on boEncCliente.bostamp = bi.bostamp
	left join st (nolock) on st.ref = bi.ref
Where 
	encfcstamp = @encstamp
order by
	dataobra,lordem
END

GO
Grant Execute on dbo.up_relatorio_Keiretsu_RelacaoEncClientes to Public
GO
Grant control on dbo.up_relatorio_Keiretsu_RelacaoEncClientes to Public
GO