--exec up_atualiza_analises_acessos

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_atualiza_analises_acessos]') IS NOT NULL
	drop procedure dbo.up_atualiza_analises_acessos
go

create procedure [dbo].[up_atualiza_analises_acessos]

/* with encryption */

AS 
BEGIN
		
	insert into b_pf (pfstamp, codigo, resumo, descricao, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, grupo, tipo, visivel)
	select 
	substring(cast(newid() as char(36)),1,25) as pfstamp
	, (select max(codigo) from b_pf)+ROW_NUMBER() over(order by descricao) as codigo
	, descricao as resumo
	, 'Rem. acesso análise '+descricao as descricao
	,'ADM' as ousrinis
	, getdate() as ousrdata
	, '00:00:00' as ousrhora
	,'ADM' as usrinis
	, getdate() as usrdata
	, '00:00:00' as usrhora
	, 'Análises' as grupo
	, 'Remove Acesso' as tipo
	, 1 as visivel 
	from B_analises where descricao not in (select resumo from b_pf where grupo='Análises')
		
END

GO
Grant Execute on dbo.[up_atualiza_analises_acessos] to Public
Grant control on dbo.[up_atualiza_analises_acessos] to Public
GO