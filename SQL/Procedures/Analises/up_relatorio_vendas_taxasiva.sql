/* Relatório Relatório Vendas por Taxa Iva     

	exec up_relatorio_vendas_taxasiva '20150101', '20221030', 'Loja 1'

*/
if OBJECT_ID('[dbo].[up_relatorio_vendas_taxasiva]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_taxasiva
go

create procedure [dbo].[up_relatorio_vendas_taxasiva]
	@dataIni		datetime
	,@dataFim		datetime
	,@site			varchar(55)
/* with encryption */
AS
SET NOCOUNT ON
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
	DROP TABLE #dadosVendasBase
	/* Define se vais mostrar resultados abaixo do Cliente 199 */
	declare @Entidades as bit
	set @Entidades = case when (select tipoempresa from empresa where site = @site) in ('FARMACIA', 'PARAFARMACIA') then convert(bit,1) else convert(bit,0) end
	/* Dados Base Vendas  */
	create table #dadosVendasBase (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100)
		,familia varchar(18)
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)
	)
    insert #dadosVendasBase
    exec up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site
	/* Elimina Vendas tendo em conta tipo de cliente*/
	If @Entidades = 1
	delete from #dadosVendasBase where #dadosVendasBase.no < 199
	/* Prepara Result Set */
	Select 
		iva
		,dsc =  SUM(CASE WHEN #dadosVendasBase.[tipodoc] != 3 THEN ABS(#dadosVendasBase.[descvalor]) ELSE ABS(#dadosVendasBase.[descvalor])*-1 END)
		,VaDD = SUM(descvale)
		,VdPVUt = SUM(etiliquidoSiva) /*sem iva*/
		,VdPVEnt = SUM(ettent1siva+ettent2siva) /*sem iva*/
		,VdPV =  SUM(etiliquidoSiva+ettent1siva+ettent2siva) /*sem iva*/
		,VdPVPUt = SUM(etiliquido) /*com iva*/
		,VdPVPEnt = SUM(ettent1+ettent2) /*com iva*/
		,VdPVP =  SUM(etiliquido+ettent1+ettent2) /*com iva*/
	from 
		#dadosVendasBase
	Where 
		#dadosVendasBase.u_tipodoc not in (1, 5)
	Group by
		iva
	Order by
		iva
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
GO
Grant Execute on dbo.up_relatorio_vendas_taxasiva to Public
Grant control on dbo.up_relatorio_vendas_taxasiva to Public
GO