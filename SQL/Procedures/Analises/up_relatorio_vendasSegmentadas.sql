/*

exec up_relatorio_vendasSegmentadas 'Loja 1','20240101','20240229','','','','','',0

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_vendasSegmentadas]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendasSegmentadas
go

create procedure [dbo].[up_relatorio_vendasSegmentadas]
@site							VARCHAR (254),
@dataIni						DATETIME,
@dataFim						DATETIME,
@grdmerc						VARCHAR (254),
@merc							VARCHAR (254),
@segm							VARCHAR (254),
@categ							VARCHAR (254),
@marca							VARCHAR (254),
@incluiServicos					BIT
/* WITH ENCRYPTION */
AS

	If OBJECT_ID('tempdb.dbo.#dadosVendasBase') IS NOT NULL
		DROP TABLE #dadosVendasBase
	If OBJECT_ID('tempdb.dbo.#EmpresaArm') IS NOT NULL
		DROP TABLE #EmpresaArm
	If OBJECT_ID('tempdb.dbo.#dadosST') IS NOT NULL
		DROP TABLE #dadosST

	create table #dadosVendasBase (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)
	)
    insert #dadosVendasBase
    exec up_relatorio_vendas_base_Detalhe @dataIni, @dataFim, @site

	delete 
	from 
		#dadosVendasBase 
	where  
		#dadosVendasBase.no between 1 and 198  
		and #dadosVendasBase.loja_nr in (select no from empresa (nolock) WHERE tipoempresa in ('FARMACIA', 'PARAFARMACIA') )

	SELECT 
		empresa.site																	AS 'farmacia'
		,YEAR(fdata)																	AS 'ano'
		,MONTH(fdata)																	AS 'mes'
		,ft.ref																			AS 'ref'
		,ft.design																		AS 'produto'
		,SUM(ft.qtt)																	AS 'qtt'
		,SUM(ft.etiliquidoSiva)															AS 'vendas_SIva' 
		,SUM((ft.etiliquidoSiva + ft.ettent1Siva + ft.ettent2Siva) - ft.epcpond)		AS 'margem'
		,ISNULL(grande_mercado_hmr.descr,'')											AS 'grd_mercado'
		,ISNULL(mercado_hmr.descr,'')													AS 'mercado'
		,ISNULL(categoria_hmr.descr,'')													AS 'categoria'
		,ISNULL(segmento_hmr.descr,'')													AS 'segmento'
		,st.usr1																		AS 'marca'
	FROM 
		#dadosVendasBase ft
	INNER JOIN  st					(NOLOCK)		ON ft.ref = st.ref and ft.loja_nr = st.site_nr
	INNER JOIN empresa				(NOLOCK)		ON empresa.no = st.site_nr
	LEFT JOIN fprod					(NOLOCK)		ON ft.ref = fprod.cnp
	LEFT JOIN grande_mercado_hmr	(NOLOCK)		ON grande_mercado_hmr.id = fprod.id_grande_mercado_hmr
	LEFT JOIN mercado_hmr			(NOLOCK)		ON mercado_hmr.id = fprod.id_mercado_hmr
	LEFT JOIN segmento_hmr			(NOLOCK)		ON segmento_hmr.id = fprod.id_segmento_hmr
	LEFT JOIN categoria_hmr			(NOLOCK)		ON categoria_hmr.id = fprod.id_categoria_hmr	
	WHERE	
			(isnull(empresa.site ,'') in (Select items from dbo.up_splitToTable(@site,',')) or @site = '')
			AND (isnull(convert(varchar(254),grande_mercado_hmr.id),'') in (Select items from dbo.up_splitToTable(@grdmerc,',')) or @grdmerc = '')
			AND (isnull(convert(varchar(254),mercado_hmr.id),'') in (Select items from dbo.up_splitToTable(@merc,',')) or @merc = '')
			AND (isnull(convert(varchar(254),segmento_hmr.id),'') in (Select items from dbo.up_splitToTable(@segm,',')) or @segm = '')
			AND (isnull(convert(varchar(254),categoria_hmr.id),'') in (Select items from dbo.up_splitToTable(@categ,',')) or @categ = '')
			AND (isnull(st.usr1,'') in (Select items from dbo.up_splitToTable(@marca,',')) or @marca = '')
			AND ft.ref not in ('V999999', 'R000001', '9999999' )
			and	(ft.tipodoc!=4 or ft.u_tipodoc=4) AND ft.u_tipodoc != 1 AND ft.u_tipodoc != 5
			and ( ft.no = 0 or ft.no > 198 or empresa.tipoempresa = 'CLINICA' )
			and st.stns = case when @incluiServicos = 1 then st.stns else 0 end 
	GROUP BY	empresa.site
				,YEAR(fdata)
				,MONTH(fdata)
				,ft.ref
				,ft.design
				,st.faminome
				,grande_mercado_hmr.descr
				,mercado_hmr.descr
				,segmento_hmr.descr
				,categoria_hmr.descr
				,st.usr1		
	Order by [produto]

	If OBJECT_ID('tempdb.dbo.#dadosVendasBase') IS NOT NULL
		DROP TABLE #dadosVendasBase

GO
Grant Execute on dbo.[up_relatorio_vendasSegmentadas] to Public
Grant control on dbo.[up_relatorio_vendasSegmentadas] to Public
GO