/*
	
	Mecofarma, valida encomendas em falta para processa e enviar para o x3

	up_relatorio_encomendas_em_faltaProcessar  'ATLANTICO','1900-01-01', '3000-01-01'
	
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_relatorio_encomendas_em_faltaProcessar]') IS NOT NULL
	drop procedure dbo.up_relatorio_encomendas_em_faltaProcessar
go



create procedure dbo.up_relatorio_encomendas_em_faltaProcessar

  @site				as varchar(40)
, @dataini			as date
, @datafim			as date


/* WITH ENCRYPTION */
AS
	select 
		"Data"      = CONVERT(varchar,fdata, 102),
		"Documento" = nmdoc,
		"NrCliente" = fno,
		"DepCliente" = estab,
		"NomeCliente" = Nome,
		"Total Factura" = convert(numeric(16,2),etotal) 
	from ft (nolock)
	where nmdoc='Factura'
	and (ft.ftstamp in (select distinct ftstamp from fi (nolock) where bistamp<>'' and nmdoc='Factura' and fistamp not in (select ofistamp from fi(nolock) where nmdoc in ('Nota de Cr�dito','Reg. a Cliente')))
	and ftstamp in (select ftstamp from cc (nolock) where ftstamp<>'' and cmdesc='N/Factura' and edebf<ft.etotal)
	and etotal!=0
	and fdata>=@dataini and  fdata<=@datafim
	and site = case when  @site = '' then site else @site end
	)
	order by ft.fdata
								
 
GO
Grant Execute On dbo.up_relatorio_encomendas_em_faltaProcessar to Public
Grant Control On dbo.up_relatorio_encomendas_em_faltaProcessar to Public
GO


