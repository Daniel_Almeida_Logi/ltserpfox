/* 
	Retorna fee calculado para entidade SNS

	up_relatorio_vendas_valor_fee '20220101','20221231','Loja 1'

	Calcula depois dos lotes estarem fechados

*/
if OBJECT_ID('[dbo].[up_relatorio_vendas_valor_fee]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_valor_fee
go

create procedure dbo.up_relatorio_vendas_valor_fee

	 @dataIni		DATETIME
	,@dataFim		DATETIME
	,@site			VARCHAR(55)

AS

	If OBJECT_ID('tempdb.dbo.#valorfee') IS NOT NULL
		DROP TABLE #valorfee
	If OBJECT_ID('tempdb.dbo.#monthYear') IS NOT NULL
		DROP TABLE #monthYear

	DECLARE @mes			int = 0
	DECLARE @ano			int = 0

	SELECT	 DATEPART(mm, DATEADD(MONTH, x.number, @dataIni)) AS MonthName,
			 DATENAME(YEAR, DATEADD(MONTH, x.number, @dataIni)) AS YearName
	into #monthYear
	FROM    master.dbo.spt_values x
	WHERE   x.type = 'P'        
	AND     x.number <= DATEDIFF(MONTH, @dataIni, @dataFim)

	CREATE TABLE #valorfee(
		ano					int
		,mes				int
		,entidades			VARCHAR(15)
		,sel				BIT
		,cptorgstamp		CHAR(25)
		,valorFinal			NUMERIC(19,6) 
	)


	DECLARE database_cursor CURSOR FOR 
		SELECT 
			MonthName,
			YearName
		FROM
			#monthYear

	OPEN database_cursor 
	FETCH NEXT FROM database_cursor INTO   @mes, @ano

	WHILE @@FETCH_STATUS = 0 
	BEGIN  

	    insert #valorfee
		exec up_receituario_vendas_facturacao_fee @ano,@mes, @site

	FETCH NEXT FROM database_cursor INTO @mes, @ano
	END 
	
	CLOSE database_cursor 
	DEALLOCATE database_cursor 


	SELECT 
		ano			
		,mes		
		,entidades	
		,sel		
		,cptorgstamp
		,CONVERT(NUMERIC(19,2),ROUND(valorFinal,2))		AS valorFinal
	FROM
		#valorfee


	If OBJECT_ID('tempdb.dbo.#valorfee') IS NOT NULL
		DROP TABLE #valorfee
	If OBJECT_ID('tempdb.dbo.#monthYear') IS NOT NULL
		DROP TABLE #monthYear
Go
Grant Execute on dbo.up_relatorio_vendas_valor_fee to Public
Grant Control on dbo.up_relatorio_vendas_valor_fee to Public
Go
