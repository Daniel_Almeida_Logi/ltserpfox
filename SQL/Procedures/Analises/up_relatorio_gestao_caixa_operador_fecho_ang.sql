
/****** Object:  StoredProcedure [dbo].[up_relatorio_gestao_caixa_operador_fecho_ang]    Script Date: 9/15/2022 2:20:19 PM
	EXEC up_relatorio_gestao_caixa_operador_fecho_ang '20221021','20221021', 0, 999,''
******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_relatorio_gestao_caixa_operador_fecho_ang]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_caixa_operador_fecho_ang
go


Create procedure [dbo].[up_relatorio_gestao_caixa_operador_fecho_ang]
	@dataIni DATETIME
	,@dataFim DATETIME
	,@opIni	as numeric(9,0)
	,@opFim	as numeric(9,0)
	,@site varchar(30)
/* with encryption */
AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#resultFinalCaixasSS'))
		DROP TABLE #resultFinalCaixasSS

	declare @usaPagCentral bit
	declare @gestaoCxOperador bit
	set @usaPagCentral = ISNULL((select bool from b_parameters where stamp='ADM0000000079'),0)
	set @gestaoCxOperador = (select gestao_cx_operador from empresa where site = @site)
	IF @gestaoCxOperador = 0
		begin
		print 1
			select
				cx.cxstamp
				,cx.site
				,Data = convert(varchar,cx.dabrir,102)
				,dfechar = convert(varchar,cx.dfechar,102)
				,habrir
				,hfechar		
				,cx.pnome
				,cx.pno
				,Vendedor			= cx.fuserno
				,Nome				= cx.fusername
				,cx.fecho_dinheiro
				,cx.fecho_mb
				,cx.fecho_visa
				,cx.fecho_cheques
				,cx.fecho_epaga3
				,fecho_epaga4		= sum(cx.fecho_epaga4 * ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'USD', cx.dabrir),1))
				,fecho_epaga5		= sum(cx.fecho_epaga5 * ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'EUR', cx.dabrir),1))
				,cx.fecho_epaga6
				,fundocaixa			= sum(cx.fundocaixa)			
				,cx.fechada
				,fecho_total =  cx.fecho_dinheiro + cx.fecho_mb + cx.fecho_visa + cx.fecho_cheques + cx.fecho_epaga3 + cx.fecho_epaga4 + cx.fecho_epaga5 + cx.fecho_epaga6
				,obs = causa
			from 
				cx (nolock)
			where 
				cx.dabrir between @dataini and @DataFim 
				and cx.fuserno >= @opIni
				and cx.fuserno <= @opFim
				and cx.site = case when @site = '' then cx.site else @site end 
				and cx.fechada = 1
				and (cx.fecho_dinheiro + cx.fecho_mb + cx.fecho_visa + cx.fecho_cheques + cx.fecho_epaga3 + cx.fecho_epaga4 + cx.fecho_epaga5 + cx.fecho_epaga6) != 0
			group by 
				cx.cxstamp
				,cx.site
				,cx.dabrir
				,cx.pnome
				,cx.pno
				,fundocaixa
				,dfechar
				,habrir
				,hfechar	
				,cx.pnome
				,cx.pno
				,cx.fuserno
				,cx.fusername
				,cx.fecho_dinheiro
				,cx.fecho_mb
				,cx.fecho_visa
				,cx.fecho_cheques
				,cx.fecho_epaga3
				,fecho_epaga4		
				,fecho_epaga5		
				,cx.fecho_epaga6
				,cx.fundocaixa
				,cx.fechada
				,cx.causa
			order by 
				cx.fusername
		end
	else
		begin
			select
				ss.ssstamp
				,ss.site
				,Data = convert(varchar,ss.dabrir,102)
				,dfechar = convert(varchar,ss.dfechar,102)
				,habrir
				,hfechar		
				,ss.pnome
				,ss.pno
				,Vendedor				= ss.fuserno
				,Nome					= ss.fusername
				,fecho_dinheiro			= ss.evdinheiro
				,fecho_mb				= ss.epaga1
				,fecho_visa				= ss.epaga2
				,fecho_cheques			= ss.echtotal
				,ss.fecho_epaga3
				,fecho_epaga4		= sum(ss.fecho_epaga4 * ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'USD', ss.dabrir),1))	
				,fecho_epaga5		= sum(ss.fecho_epaga5 * ISNULL(dbo.uf_cambioEntreMoedas('AOA', 'EUR', ss.dabrir),1))
				,ss.fecho_epaga6
				,ss.fundocaixa
				,ss.fechada
				,fecho_total =  ss.evdinheiro + ss.epaga1 + ss.epaga2 + ss.echtotal + ss.fecho_epaga3 + ss.fecho_epaga4 + ss.fecho_epaga5 + ss.fecho_epaga6
				,obs = causa
				, ROW_NUMBER() OVER(PARTITION BY ss.pno,ss.dabrir + habrir, ss.fuserno  ORDER BY usrdata + usrhora desc)  as linhas
		into #resultFinalCaixasSS
			from 
				ss (nolock)
			where 
				ss.dabrir between @dataini and @DataFim 
				and ss.fuserno >= @opIni
				and ss.fuserno <= @opFim
				and ss.site = case when @site = '' then ss.site else @site end 
				and ss.fechada = 1
				--and (ss.evdinheiro + ss.epaga1 + ss.epaga2 + ss.echtotal + ss.fecho_epaga3 + ss.fecho_epaga4 + ss.fecho_epaga5 + ss.fecho_epaga6) !=0
								/*AND ss.ssstamp = (case 
									when (select COUNT(*) from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp ) <> 1
									then (select top 1 ssb.ssstamp from ss(nolock) as ssb where ssb.cxstamp = ss.cxstamp order by ousrdata desc, ousrhora desc)
									else ss.ssstamp
								end)*/
			group by 
				ss.ssstamp
				,ss.site
				,ss.dabrir
				,ss.pnome
				,ss.pno
				,fundocaixa
				,dfechar
				,habrir
				,hfechar
				,ss.pnome
				,ss.pno
				,ss.fuserno
				,ss.fusername
				,ss.evdinheiro
				,ss.epaga1
				,ss.epaga2
				,ss.echtotal
				,ss.fecho_epaga3
				,fecho_epaga4	
				,fecho_epaga5	
				,ss.fecho_epaga6
				,ss.fundocaixa
				,ss.fechada
				,ss.causa
				,usrdata + usrhora

			select * from #resultFinalCaixasSS ss where linhas=1 and (ss.fecho_dinheiro + ss.fecho_mb + ss.fecho_visa + ss.fecho_cheques + ss.fecho_epaga3 + ss.fecho_epaga4 + ss.fecho_epaga5 + ss.fecho_epaga6) !=0   order by Nome
		end

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#resultFinalCaixasSS'))
		DROP TABLE #resultFinalCaixasSS
GO
Grant Execute On dbo.up_relatorio_gestao_caixa_operador_fecho_ang to Public
Grant control On dbo.up_relatorio_gestao_caixa_operador_fecho_ang to Public
GO