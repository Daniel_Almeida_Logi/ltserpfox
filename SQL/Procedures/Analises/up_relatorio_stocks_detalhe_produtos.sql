/* Relatório de Tabela de Preços

	 exec up_relatorio_stocks_detalhe_produtos  '', '', '', -99999, 0, '', 'Loja 1', '', 'MNSRM'
	 
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_stocks_detalhe_produtos]') IS NOT NULL
	drop procedure dbo.up_relatorio_stocks_detalhe_produtos
go

Create PROCEDURE dbo.up_relatorio_stocks_detalhe_produtos
@ref			as varchar(18),
	@lab			as varchar(150),
	@marca			as varchar(200),
	@stock			as numeric(9,0),
	@mbnegativa		as bit,
	@iva			as varchar(9),
	@site			as varchar(60),
	@familia		as varchar(max),
	@atributosFam	as varchar(max)
/* with encryption */
AS 
BEGIN
SET NOCOUNT ON
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
			DROP TABLE #dadosFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosMrgRegressivas'))
			DROP TABLE #dadosMrgRegressivas
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST'))
			DROP TABLE #dadosST
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendas'))
			DROP TABLE #dadosVendas				
	
	SET @atributosFam = (CASE WHEN (@atributosFam IS NULL OR @atributosFam = '') THEN ';;;' ELSE @atributosFam END)

	DECLARE @departamento VARCHAR(254) = ''
	SET @departamento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 1),'') 
	DECLARE @sessao       VARCHAR(254) = ''
	set @sessao = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 2) ,'')
	DECLARE @categoria    VARCHAR(254) = ''
	SET @categoria = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 3),'') 
	DECLARE @segmento     VARCHAR(254) = ''
	SET @segmento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 4),'')
	 
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)
	--
	Select 
		distinct ref 
	into 
		#dadosFamilia
	From
		stfami (nolock)
	Where
		ref in (Select items from dbo.up_splitToTable(@familia,','))
		Or @familia = '0' Or @familia = ''	
	--
	select
		st.ref
		,st.design
		,stock		= case when st.stns = 0 then convert(int,st.stock) else 0 END
		,st.validade
		,st.usr1
		,st.u_lab
		,ultEntrada = st.uintr
		,ultSaida	= st.usaid
		,st.faminome
		,pct		= st.epcusto
		,pcl		= st.epcult
		,pcp		= st.epcpond
		,iva		= isnull(taxasiva.taxa,0)
		,pvp		= st.epv1
		,mrg		= st.marg1
		,mbspvp			= (1-(st.epcpond/CASE WHEN (st.epv1 / ((isnull(taxasiva.taxa,0) / 100) + 1)) = 0 THEN 1 ELSE
						(st.epv1 / ((isnull(taxasiva.taxa,0) / 100) + 1)) END ))*100		
		, ISNULL(A.descr,'') as departamento
		, ISNULL(B.descr,'') as seccao
		, ISNULL(C.descr,'') as categoria
		, ISNULL(D.descr,'') as segmento

		,fornecedor     = st.fornecedor
	into
		#dadosST
	from
		st (nolock)
		left join fprod (nolock) on fprod.cnp=st.ref
		left join taxasiva (nolock) on taxasiva.codigo=st.tabiva		
		left join grande_mercado_hmr (nolock) A on A.id = st.u_depstamp
		left join mercado_hmr (nolock) B on B.id = st.u_secstamp
		left join categoria_hmr (nolock) C on C.id = st.u_catstamp 
		left join segmento_hmr (nolock) D on D.id = st.u_segstamp	
	where
		st.ref = case when @ref = '' then st.ref else @ref end
		and st.familia in (Select ref from #dadosFamilia)
		and u_lab = case when @lab = '' then u_lab else @lab end
		and usr1 = case when @marca = '' then usr1 else @marca end
		and stock >= @stock
		and isnull(taxasiva.taxa,0) = case when @iva = '' then isnull(taxasiva.taxa,0) else convert(numeric(9,2),@iva) end
		and site_nr = @site_nr
	-- Result Set Final
	Select 
		#dadosST.*
	from
		#dadosST
	Where 
		mbspvp < case when @mbnegativa = 1 then 0 else 99999999999 end	
		AND departamento = (CASE WHEN @departamento = '' THEN departamento ELSE @departamento END)
		AND seccao = (CASE WHEN @sessao = '' THEN seccao ELSE  @sessao END)
		AND categoria = (CASE WHEN @categoria = '' THEN categoria ELSE  @categoria END)
		AND segmento = (CASE WHEN @segmento = '' THEN segmento ELSE  @segmento END) 
	order by 
		#dadosST.ref
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
			DROP TABLE #dadosFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosMrgRegressivas'))
			DROP TABLE #dadosMrgRegressivas
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosST'))
			DROP TABLE #dadosST
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendas'))
			DROP TABLE #dadosVendas	
END


GO
Grant Execute On dbo.up_relatorio_stocks_detalhe_produtos to Public
Grant control On dbo.up_relatorio_stocks_detalhe_produtos to Public
GO
