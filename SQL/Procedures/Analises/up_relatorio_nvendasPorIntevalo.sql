/* Relatório numero de vendas por intervalo 
	
	exec up_relatorio_nvendasPorIntevalo '20190801','20190831','Loja 1',15
	exec up_relatorio_nvendasPorIntevalo '20210401','20210401','Loja 1',30
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_nvendasPorIntevalo]') IS NOT NULL
	drop procedure dbo.up_relatorio_nvendasPorIntevalo
go

create procedure [dbo].[up_relatorio_nvendasPorIntevalo]
 @dataIni DATETIME
	,@dataFim DATETIME
	,@site varchar(30)
	,@intervalo int

/* with encryption */
AS

	DECLARE @Infarmed VARCHAR(15)

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TableDate'))
		DROP TABLE #TableDate

	--IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#infoVenda'))
	--	DROP TABLE #infoVenda

	If OBJECT_ID('tempdb.dbo.#infoVenda') IS NOT NULL
		drop table #infoVenda;	


	If OBJECT_ID('tempdb.dbo.#infob_pag') IS NOT NULL
		drop table #infob_pag;	


	SET @Infarmed= (SELECT infarmed from empresa where site=@site) 

	CREATE TABLE #TableDate (ID INT IDENTITY(0,1), TIMEVALUE DATETIME , TIMEVALUEEND DATETIME);
		
	set @datafim = @datafim + '23:59:59.997'
	;WITH CTE_DT AS 
	(
		SELECT @dataIni AS DT , DATEADD(MILLISECOND,-3,DATEADD(MINUTE,@intervalo,@dataIni)) AS TIMEVALUEEND
		UNION ALL
		SELECT DATEADD(MINUTE,@intervalo,DT) AS DT,DATEADD(MILLISECOND,-3, DATEADD(MINUTE,@intervalo *2,DT)) AS TIMEVALUEEND  FROM CTE_DT
		WHERE DT < @datafim  AND  DATEADD(MILLISECOND,3,TIMEVALUEEND) <@datafim 
	)
	INSERT INTO #TableDate
	SELECT DT,TIMEVALUEEND FROM CTE_DT
	OPTION (MAXRECURSION 0);


		/* Dados Base Vendas  */
	create table #dadosVendasBase (
		nrAtend varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AI
		,ftstamp varchar(25)
		,fdata datetime 
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8) 
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)
	)


    insert #dadosVendasBase
    exec up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site


	delete from #dadosVendasBase where #dadosVendasBase.no < 199



	select 
		CONVERT(VARCHAR(20),CONVERT(date, #TableDate.TIMEVALUE)) 									AS DATA,
		CONVERT(VARCHAR(20),CONVERT(VARCHAR(8),#TableDate.TIMEVALUE,108))							AS HORA,
		CONVERT(VARCHAR(20),COUNT(DISTINCT( ISNULL(B_pagCentral.terminal,0))))					AS POS_VENDA
	into #infob_pag
	from #TableDate	
		LEFT JOIN #dadosVendasBase								(NOLOCK)	ON  (#dadosVendasBase.fdata + #dadosVendasBase.ousrhora) between TIMEVALUE and TIMEVALUEEND
		LEFT JOIN  B_pagCentral   (NOLOCK)  on #dadosVendasBase.nrAtend = B_pagCentral.nrAtend
	GROUP BY  
		CONVERT(date, #TableDate.TIMEVALUE),CONVERT(VARCHAR(8),#TableDate.TIMEVALUE,108) ,#TableDate.TIMEVALUE 
	order by #TableDate.TIMEVALUE		

	SELECT 
		@Infarmed																													AS LOJA,
		CONVERT(VARCHAR(20),CONVERT(date, #TableDate.TIMEVALUE)) 																	AS DATA,
		CONVERT(VARCHAR(20),CONVERT(VARCHAR(8),#TableDate.TIMEVALUE,108))															AS HORA,
		CONVERT(VARCHAR(20),COUNT( distinct #dadosVendasBase.ftstamp))																AS TICKETS,
		CONVERT(VARCHAR(30),Round(SUM(#dadosVendasBase.etiliquidoSiva  + #dadosVendasBase.ettent1siva + #dadosVendasBase.ettent2siva),2))	AS VALOR_VENDA,
		CONVERT(VARCHAR(20),SUM(#dadosVendasBase.qtt))																				AS QUANTIDADE_ITENS,
		CONVERT(VARCHAR(20),0)																										AS POS_VENDA,
		#TableDate.TIMEVALUE																										AS TIMEVALUE
	INTO #infoVenda
	FROM #TableDate	
		INNER JOIN #dadosVendasBase								(NOLOCK)	ON  (#dadosVendasBase.fdata + #dadosVendasBase.ousrhora) between TIMEVALUE and TIMEVALUEEND

	GROUP BY  
		CONVERT(date, #TableDate.TIMEVALUE),CONVERT(VARCHAR(8),#TableDate.TIMEVALUE,108) ,#TableDate.TIMEVALUE 
	order by #TableDate.TIMEVALUE


	INSERT INTO #infoVenda 
	SELECT 
		'LOJA'				AS LOJA,
		'DATA'				AS DATA,
		'HORA'				AS HORA,
		'TICKETS'			AS TICKETS,
		'VALOR_VENDA'		AS VALOR_VENDA,
		'QUANTIDADE_ITENS'	AS QUANTIDADE_ITENS,
		'POS_VENDA'			AS POS_VENDA,
		'19000101'			AS TIMEVALUE

	SELECT 
		LOJA,
		#infoVenda.DATA,
		#infoVenda.HORA,
		TICKETS,
		VALOR_VENDA,
		QUANTIDADE_ITENS,
		#infob_pag.POS_VENDA
	FROM #infoVenda
	INNER JOIN #infob_pag (NOLOCK) on #infob_pag.DATA = #infoVenda.DATA and #infob_pag.HORA= #infoVenda.HORA
	ORDER BY #infoVenda.TIMEVALUE ASC


	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TableDate'))
		DROP TABLE #TableDate

	If OBJECT_ID('tempdb.dbo.#infoVenda') IS NOT NULL
		drop table #infoVenda;	

	If OBJECT_ID('tempdb.dbo.#infob_pag') IS NOT NULL
		drop table #infob_pag;	


GO
Grant Execute On dbo.up_relatorio_nvendasPorIntevalo to Public
Grant control On dbo.up_relatorio_nvendasPorIntevalo to Public
GO