/* Relatório Conatbilidade Dev Clientes

	 exec up_relatorio_gestao_operador_DevClientes '20100101','20130101','Loja 1', ''
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_operador_DevClientes]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_operador_DevClientes
go

create procedure dbo.up_relatorio_gestao_operador_DevClientes
@dataIni as datetime,
@dataFim as datetime,
@site varchar(60),
@op as varchar(max)
/* with encryption */
AS
SET NOCOUNT ON
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#tempcteOperadores'))
		DROP TABLE #tempcteOperadores
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteFix'))
		DROP TABLE #cteFix
	Select 
		iniciais
		,cm = userno
		,username
	into
		#tempcteOperadores
	from 
		b_us (nolock) 
	Where 
		userno in (select items from dbo.up_splitToTable(@op,',')) or @op= '0' or @op= ''
	--
	Select 
		td.ndoc
		,ofistamp
		,fistamp
		,qtt
	into
		#cteFix
	From 
		fi	(nolock)
		inner join td on fi.ndoc = td.ndoc	
		and td.u_tipodoc != 3
	SELECT	
		EMPRESA		= empresa.EMPRESA
		,LOJA		= empresa.site
		,NUMLOJA	= empresa.ref
		,DATADOC	= FDATA
		,NOMECLI	= FT.NOME
		,NUMCLI		= FT.NO
		,DOCUMENTO	= FT.NMDOC
		,NUMDOC		= FT.FNO
		,FI.REF
		,FI.DESIGN
		,QTT = fi.qtt-isnull(fix.qtt,0)
		,fi.UNIDADE
		,fi.TABIVA
		,fi.IVA
		,fi.DESCONTO
		,fi.DESC2
		,VALOR = (ABS(fi.ETILIQUIDO)/fi.qtt)*(fi.qtt-isnull(fix.qtt,0))
		,ft.ousrinis
	FROM	
		FT (nolock) 
		INNER JOIN FI (nolock) ON FI.FTSTAMP = FT.FTSTAMP
		INNER JOIN TD (nolock) ON FT.NDOC = TD.NDOC
		--Left join cteLojas on armazem1 = FI.armazem
		inner join empresa_arm on empresa_arm.armazem = fi.armazem
		inner join empresa on empresa.no = empresa_arm.empresa_no
		left join #cteFix fix (nolock) on fi.fistamp = fix.ofistamp 
	WHERE
		td.U_TIPODOC = 2 
		AND (fi.ref != '' OR fi.qtt != 0 OR fi.etiliquido !=0)
		and (fi.qtt-isnull(fix.qtt,0)) != 0
		AND FDATA BETWEEN @dataIni AND @dataFim
		and ft.site = (case when @site = '' Then ft.site else @site end)
		and (ft.ousrinis in (select iniciais from #tempcteOperadores ) or @op= '0' or @op= '')
	ORDER BY 
		ft.FDATA

GO
Grant Execute on dbo.up_relatorio_gestao_operador_DevClientes to Public
GO
Grant Control on dbo.up_relatorio_gestao_operador_DevClientes to Public
GO