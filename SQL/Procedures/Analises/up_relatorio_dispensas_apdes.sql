/*
Relat�rio Cart�es Farm�cias

exec up_relatorio_dispensas_apdes '20221201', '20221221',''

*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_relatorio_dispensas_apdes]') IS NOT NULL
drop procedure dbo.up_relatorio_dispensas_apdes
go

create procedure up_relatorio_dispensas_apdes
@dataIni					DATETIME	 = '',
@dataFim					DATETIME	 = '30000101',
@farmacia					VARCHAR(254) = ''

/* WITH ENCRYPTION */
AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#apdesPreparResult'))
		DROP TABLE #apdesPreparResult

     
    select
       	CONVERT(date, date)		as data,
		name_ext_pharmacy		as nome,
		cardCode				as card,
		pharmacyCode			as code                                                                                             
	INTO 
		#apdesPreparResult
    from
        [SQLLOGITOOLS].[LTSMSB].dbo.ext_compart_response  ext_compart_response (nolock)
        inner join [SQLLOGITOOLS].[LTSMSB].dbo.ext_compart_request ext_compart_request (nolock)
            on ext_compart_response.stampRequest = ext_compart_request.stamp
        inner join [SQLLOGITOOLS].[LTSMSB].dbo.ext_compart_response_l ext_compart_response_l(nolock)
            on ext_compart_response_l.stampResponse = ext_compart_response.stamp
    where
        ext_compart_request.id_ext_efr = '100'
        and ext_compart_request.reqType = 2 and anulationReqStamp='' 
		and ext_compart_response.contributionCorrelationId!=''
        and ext_compart_request.ousrdata between @dataIni and @dataFim
        and ext_compart_request.test = 0
		and  (pharmacyCode in  (Select items from dbo.up_splitToTable(@farmacia,',')) or @farmacia = '')
	order by 
		ext_compart_request.date desc
	 
	 SELECT 
		*,
		1 AS total
	FROM
		#apdesPreparResult
	ORDER by 
		code, data

	 IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#apdesPreparResult'))
		DROP TABLE #apdesPreparResult

GO
Grant Execute On dbo.up_relatorio_dispensas_apdes to Public
Grant Control On dbo.up_relatorio_dispensas_apdes to Public
Go

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO