/*
	EXEC up_relatorio_cartoes_entidades '',0
	EXEC up_relatorio_cartoes_entidades '',1
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_cartoes_entidades]') IS NOT NULL
	drop procedure dbo.up_relatorio_cartoes_entidades
go

create procedure [dbo].[up_relatorio_cartoes_entidades]
	 @entidade							AS VARCHAR(254),
	 @valido							AS BIT	= 0

/* with encryption */
AS
BEGIN	

	SELECT
		cptorg.nome						AS entidade
		,ext_cards_assoc.cardId			AS cartao
		,ext_cards_assoc.expiryDate		AS dataExpiracao
	FROM 
		ext_cards_assoc(NOLOCK)
	INNER JOIN cptorg(NOLOCK) ON cptorg.cptorgstamp = ext_cards_assoc.cptorgstamp
	INNER JOIN b_utentes(NOLOCK) ON b_utentes.no = cptorg.u_no
	WHERE
		cptorg.nome IN (SELECT items FROM dbo.up_splitToTable(@entidade,',')) or @entidade= ''
		AND 1=CASE WHEN  @valido = 1  THEN 
				(CASE WHEN ext_cards_assoc.expiryDate >= GETDATE() THEN 1 ELSE 0 END)
			ELSE 
				(CASE WHEN ext_cards_assoc.expiryDate < GETDATE() THEN 1 ELSE 0 END)
			END

								  

END

GO
Grant Execute on dbo.up_relatorio_cartoes_entidades to Public
Grant control on dbo.up_relatorio_cartoes_entidades to Public
GO