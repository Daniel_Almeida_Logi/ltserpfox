-- Lista Horas apoio a an�lises
-- exec up_relatorio_ListaHoras

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_ListaHoras]') IS NOT NULL
    drop procedure dbo.up_relatorio_ListaHoras
go

create procedure dbo.up_relatorio_ListaHoras

/* WITH ENCRYPTION */
AS

	Select sel = convert(bit,0), hora = '00'
	union all
	Select sel = convert(bit,0), hora ='01'
	union all
	Select sel = convert(bit,0), hora ='02'
	union all
	Select sel = convert(bit,0), hora ='03'
	union all
	Select sel = convert(bit,0), hora ='04'
	union all
	Select sel = convert(bit,0), hora ='05'
	union all
	Select sel = convert(bit,0), hora ='06'
	union all
	Select sel = convert(bit,0), hora ='07'
	union all
	Select sel = convert(bit,0), hora ='08'
	union all
	Select sel = convert(bit,0), hora ='09'
	union all
	Select sel = convert(bit,0), hora ='10'
	union all
	Select sel = convert(bit,0), hora ='11'
	union all
	Select sel = convert(bit,0), hora ='12'
	union all
	Select sel = convert(bit,0), hora ='13'
	union all
	Select sel = convert(bit,0), hora ='14'
	union all
	Select sel = convert(bit,0), hora ='15'
	union all
	Select sel = convert(bit,0), hora ='16'
	union all
	Select sel = convert(bit,0), hora ='17'
	union all
	Select sel = convert(bit,0), hora ='18'
	union all
	Select sel = convert(bit,0), hora ='19'
	union all
	Select sel = convert(bit,0), hora ='20'
	union all
	Select sel = convert(bit,0), hora ='21'
	union all
	Select sel = convert(bit,0), hora ='22'
	union all
	Select sel = convert(bit,0), hora ='23'
GO
Grant Execute on dbo.up_relatorio_ListaHoras to Public
Grant Control on dbo.up_relatorio_ListaHoras to Public
GO