-- Relatório Conferencia Litigios
-- exec up_relatorio_conferencia_litigios '20100101','20130101','A',''

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_conferencia_litigios]') IS NOT NULL
	drop procedure dbo.up_relatorio_conferencia_litigios
go

create procedure dbo.up_relatorio_conferencia_litigios
@dataIni DATETIME,
@dataFim datetime,
@status varchar(1),
@site	 varchar(55)
/* with encryption */
AS 
BEGIN
SELECT	
	ISNULL((Select TOP 1 EMPRESA FROM empresa (nolock) WHERE site = @site),0) as EMPRESA,
	ISNULL((Select TOP 1 SITE FROM empresa (nolock) WHERE site = @site),'') as LOJA,
	ISNULL((Select TOP 1 REF FROM empresa (nolock) WHERE site = @site),'') as CODLOJA,
	FOID as NUMLANCAMENTO,
	FO.ADOC as NUMEROFACTURA,
	CASE WHEN fn.oref = '' THEN fn.ref ELSE fn.oref END  as REFERENCIA,
	fn.DESIGN,
	ISNULL((SELECT CAST(OBRANO as VARCHAR(254)) FROM BI (nolock) as ENCLIN WHERE ENCLIN.BISTAMP = (SELECT bistamp FROM FN GUIA (nolock) WHERE GUIA.fnstamp = FN.ofnstamp)),'') as NUMEROENCOMENDA,
	ISNULL((SELECT GUIA.ADOC FROM FN GUIA (nolock) WHERE GUIA.fnstamp = FN.ofnstamp),'') as NUMERORECEPCAO,
	ISNULL((SELECT GUIA.DATA FROM FN GUIA (nolock) WHERE GUIA.fnstamp = FN.ofnstamp),'') as DATARECEPCAO,
	U_UPC as UNIT_COST_FACTURA,
	QTT as QTD_FACTURADA,
	ISNULL((SELECT GUIA.u_upc FROM FN GUIA (nolock) WHERE GUIA.fnstamp = FN.ofnstamp),0) as UNIT_COST_RECEPCAO,
	ISNULL((SELECT GUIA.qtt FROM FN GUIA (nolock) WHERE GUIA.fnstamp = FN.ofnstamp),0) as QTD_RECEPCIONADA,
	ISNULL(FN.QTT - ISNULL((SELECT GUIA.qtt FROM FN GUIA (nolock) WHERE GUIA.fnstamp = FN.ofnstamp),0),0) as AJUSTE_QUANTIDADE,
	ISNULL(FN.U_UPC - ISNULL((SELECT GUIA.u_upc FROM FN GUIA (nolock) WHERE GUIA.fnstamp = FN.ofnstamp),0),0) as AJUSTE_VALOR,
	FN.ousrdata as DATA_CRIACAO,
	FN.usrdata as DATA_FECHO,
	estado = case when FN.marcada = 0 then 'A' Else 'F' end
FROM	
	FN 
	LEFT JOIN FO (nolock) ON FN.fostamp = FO.FOSTAMP
WHERE	
	FO.doccode = 55 /* FACTURAS */
	AND FO.DATA BETWEEN @dataIni AND @dataFim
	AND FN.marcada = case when @status = '' then fn.marcada else (case when @status = 'A' then 0 Else 1 end) end
	AND (
		ISNULL(FN.QTT - ISNULL((SELECT GUIA.qtt FROM FN GUIA (nolock) WHERE GUIA.fnstamp = FN.ofnstamp),0),0) != 0
		OR
		ISNULL(FN.U_UPC - ISNULL((SELECT GUIA.u_upc FROM FN GUIA (nolock) WHERE GUIA.fnstamp = FN.ofnstamp),0),0) != 0	
	)
END

GO
Grant Execute on dbo.up_relatorio_conferencia_litigios to Public
GO
Grant control on dbo.up_relatorio_conferencia_litigios to Public
GO
