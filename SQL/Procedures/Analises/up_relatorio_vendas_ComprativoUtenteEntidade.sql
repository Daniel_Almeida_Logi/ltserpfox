/* Relatório Vendas Utentes vs Entidade

	 exec up_relatorio_vendas_ComprativoUtenteEntidade '20200401','20200531', '', '', '0', '', 'Loja 1',  ''  

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_vendas_ComprativoUtenteEntidade]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_ComprativoUtenteEntidade
go

create procedure dbo.up_relatorio_vendas_ComprativoUtenteEntidade
@dataIni		 as datetime,
@dataFim		 as datetime,
@lab			 as varchar(120),
@marca			 as varchar(200),
@op				 as varchar(max),
@familia		 as varchar(max),
@site	         as varchar(55),
@atributosFam	 varchar(max)


/* with encryption */
AS 
BEGIN

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
	DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosTd'))
	DROP TABLE #dadosTd
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#utentes'))
	DROP TABLE #utentes
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
	DROP TABLE #dadosFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
	DROP TABLE #dadosOperadores
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
	DROP TABLE #dadosSt
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#EmpresaArm'))
	DROP TABLE #EmpresaArm
	
	SET @atributosFam = (CASE WHEN (@atributosFam IS NULL OR @atributosFam = '') THEN ';;;' ELSE @atributosFam END)

	DECLARE @departamento VARCHAR(254) = ''
	SET @departamento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 1),'') 
	DECLARE @sessao       VARCHAR(254) = ''
	set @sessao = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 2) ,'')
	DECLARE @categoria    VARCHAR(254) = ''
	SET @categoria = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 3),'') 
	DECLARE @segmento     VARCHAR(254) = ''
	SET @segmento = ISNULL((SELECT items FROM dbo.up_SplitToTableCId(@atributosFam,';') WHERE id = 4),'') 

	/* Calc Loja e Armazens */
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)

	select 
		armazem
	into
		#EmpresaArm
	From
		empresa
		inner join empresa_arm on empresa.no = empresa_arm.empresa_no
	where
		empresa.site = case when @site = '' then empresa.site else @site end	


	/* Dados Base Vendas  */
	create table #dadosVendasBase (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)
	)
    
    insert #dadosVendasBase
    exec up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site

	--exec up_relatorio_vendas_base_detalhe '20150401', '20150531', 'Loja 1'

	/* Calc Documentos */
	Select 
		ndoc
		,nmdoc 
		,tipodoc
		,u_tipodoc
	into
		#dadosTd
	from 
		td

	/* Calc Utentes*/
	Select 
		[no]
		,nome
		,estab
	into 
		#utentes
	from 
		B_utentes
	
	
	/* Calc Familias Infarmed Produtos */
	Select 
		distinct ref 
	into 
		#dadosFamilia
	From 
		stfami (nolock)  
	Where 
		ref in (Select items from dbo.up_splitToTable(@familia,',')) 
		Or @familia = '0' 
		Or @familia = ''

	
	/* Calc Operadores  */
	Select
		iniciais
		,cm = userno
		,username
	into
		#dadosOperadores
	from
		b_us (nolock)
	Where
		userno in (select items from dbo.up_splitToTable(@op,','))
		or @op= '0'
		or @op= ''
		
	/* Calc Informação Produtos */	
	Select
		ref 
		,usr1
		,u_lab	
		, ISNULL(A.descr,'') as departamento
		, ISNULL(B.descr,'') as seccao
		, ISNULL(C.descr,'') as categoria
		, ISNULL(D.descr,'') as segmento
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 1) valDepartamento
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 2) valSeccao
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 3) valCategoria
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 4) valSegmento
	into 
		#dadosSt
	From
		st (nolock)	
		left join grande_mercado_hmr (nolock) A on A.id = st.u_depstamp
		left join mercado_hmr (nolock) B on B.id = st.u_secstamp
		left join categoria_hmr (nolock) C on C.id = st.u_catstamp 
		left join segmento_hmr (nolock) D on D.id = st.u_segstamp		
	Where	
		u_lab = case when @lab = '' then u_lab else @lab end
		AND usr1 = case when @marca = '' then usr1 else @marca end	
		AND site_nr = @site_nr
	

	/* Prepara Result Set */
	Select	
		ano = year(fdata)
		,mes = month(fdata)
		,fdata
		,#dadosTd.nmdoc
		,fno
		,#dadosVendasBase.no
		,#utentes.nome
		,etiliquido
		,ValorCIvaEntidade = Case when #dadosVendasBase.no < 199 then etiliquido else 0 end 
		,ValorSIvaEntidade =  Case when #dadosVendasBase.no < 199 then etiliquidoSiva else 0 end 
		,ValorIvaEntidade =  Case when #dadosVendasBase.no < 199 then etiliquido - (etiliquido/(iva/100+1)) else 0 end 
		,ValorCIvaUtente =  Case when #dadosVendasBase.no >= 199 then etiliquido else 0 end 
		,ValorSIvaUtente =  Case when #dadosVendasBase.no >= 199 then etiliquidoSiva else 0 end 
		,ValorIvaUtente =  Case when #dadosVendasBase.no >= 199 then etiliquido - (etiliquido/(iva/100+1)) else 0 end 
		,tipo = Case when #dadosVendasBase.no >= 199 then 'Utente' else 'Entidade' end
		,#dadosVendasBase.ref
		,#dadosVendasBase.design
		,qtt
		,iva
		,#dadosVendasBase.familia
		,ousrinis
		,lab = CASE WHEN #dadosSt.u_lab is not null THEN #dadosSt.u_lab	ELSE '' END
		,marca = CASE WHEN #dadosSt.usr1	is not null	THEN #dadosSt.usr1	ELSE '' END
	From
		#dadosVendasBase
		inner join #dadosTd on #dadosVendasBase.ndoc = #dadosTd.ndoc
		inner join #utentes on #dadosVendasBase.no = #utentes.no and #dadosVendasBase.estab = #utentes.estab
		left join #dadosSt on #dadosVendasBase.ref = #dadosSt.ref
	Where
		#dadosSt.u_lab = case when @lab = '' then #dadosSt.u_lab else @lab end
		and #dadosSt.usr1 = case when @marca = '' then #dadosSt.usr1 else @marca end	
		and #dadosVendasBase.ousrinis in (select iniciais from #dadosOperadores)
		and #dadosVendasBase.familia in (Select ref from #dadosFamilia)
		AND departamento = (CASE WHEN @departamento = '' THEN departamento ELSE @departamento END)
		AND seccao = (CASE WHEN @sessao = '' THEN seccao ELSE  @sessao END)
		AND categoria = (CASE WHEN @categoria = '' THEN categoria ELSE  @categoria END)
		AND segmento = (CASE WHEN @segmento = '' THEN segmento ELSE  @segmento END) 

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
	DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosTd'))
	DROP TABLE #dadosTd
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#utentes'))
	DROP TABLE #utentes
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
	DROP TABLE #dadosFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
	DROP TABLE #dadosOperadores
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
	DROP TABLE #dadosSt
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#EmpresaArm'))
	DROP TABLE #EmpresaArm

END


GO
Grant Execute on dbo.up_relatorio_vendas_ComprativoUtenteEntidade to Public
GO
Grant control on dbo.up_relatorio_vendas_ComprativoUtenteEntidade to Public
GO