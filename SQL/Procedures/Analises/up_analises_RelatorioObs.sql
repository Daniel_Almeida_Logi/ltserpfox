-- Obtem observações do Relatorio
-- exec up_analises_RelatorioObs 104

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_analises_RelatorioObs]') IS NOT NULL
	drop procedure dbo.up_analises_RelatorioObs
go

create procedure dbo.up_analises_RelatorioObs
@ordem as int

/* WITH ENCRYPTION */

AS 
BEGIN
	Select 
		comentario
	from 
		B_analises (nolock) 
	Where
		ordem = @ordem

END

GO
Grant Execute on dbo.up_analises_RelatorioObs to Public
GO
Grant Control on dbo.up_analises_RelatorioObs to Public
GO