-- Relatório Vendas Farmácia Infarmed

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_familia_vendas]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_familia_vendas
go

create procedure dbo.up_relatorio_gestao_familia_vendas
@dataIni as datetime,
@dataFim as datetime,
@site varchar(60)
/* with encryption */
AS
SET NOCOUNT ON
;with 
	cte1 (ref,nome) as (
		SELECT	ref,nome
		FROM	stfami (nolock)
	)
SELECT	'UTENTE'				= SUM(ETILIQUIDO),
		'COMPARTICIPACAO'		= SUM(u_ettent1) + SUM(u_ettent2),
		'TOTAL'					= SUM(ETILIQUIDO) + SUM(u_ettent1) + SUM(u_ettent2),
		'EPVP'					= ROUND(SUM(U_EPVP),2),
		'EPCP'					= ROUND(SUM(EPCPOND),2),
		'MARGEM'				= ROUND((SUM(ETILIQUIDO) + SUM(u_ettent1) + SUM(u_ettent2)) - SUM(EPCPOND),2),
		'MARGEM_PERC'			= ROUND((CASE WHEN ((SUM(ETILIQUIDO) + SUM(u_ettent1) + SUM(u_ettent2))) > 0 THEN (1-(SUM(EPCPOND)/(SUM(ETILIQUIDO) + SUM(u_ettent1) + SUM(u_ettent2)))) * 100	ELSE 0 END),2),
		'MARGEM_PERC_LOGITOOLS'	= ROUND((CASE WHEN (SUM(EPCPOND)) > 0	THEN (((SUM(ETILIQUIDO) + SUM(u_ettent1) + SUM(u_ettent2)) / SUM(EPCPOND))-1) * 100	ELSE 0 END),2),
		'QUANTIDADEVENDIDA'		= SUM(quantidade),
		'FAMILIA_N'				= FAMILIA_N,
		'ANO'					= year(data),
		'MES'					= month(data)
FROM(	
			SELECT	fdata as DATA,
			YEAR(FT.fdata) as Ano,
			MONTH(FT.FDAta) as Mes,
			FT.NMDOC,
			'FAMILIA_N' =	ISNULL((SELECT top 1 nome from cte1 Where fi.familia = cte1.ref),''),
			'quantidade'	=	CASE WHEN Ft.tipodoc = 3 THEN
									fi.qtt * -1
								Else
									fi.qtt
								End,
			'etiliquido'	=	CASE WHEN Fi.IVAINCL = 0
									THEN FI.ETILIQUIDO
									ELSE FI.ETILIQUIDO / ((Fi.iva / 100)+1)
								END
							,
			'u_ettent1'		=	(CASE WHEN Ft.tipodoc = 3 THEN
									CASE WHEN Fi.IVAINCL = 0
										THEN u_ettent1 * -1
										ELSE (u_ettent1 / ((Fi.iva / 100)+1)) * (-1)
									END
								ELSE
									CASE WHEN Fi.IVAINCL = 0
										THEN u_ettent1 
										ELSE (u_ettent1 / ((Fi.iva / 100)+1))
									END
								END
			),
			'u_ettent2'		= 	(CASE WHEN Ft.tipodoc = 3 THEN
									CASE WHEN Fi.IVAINCL = 0
										THEN u_ettent2 * -1
										ELSE (u_ettent2 / ((Fi.iva / 100)+1)) * (-1)
									END
								ELSE
									CASE WHEN Fi.IVAINCL = 0
										THEN u_ettent2 
										ELSE (u_ettent2 / ((Fi.iva / 100)+1))
									END
								END),
			'EPCPOND' = 		CASE WHEN TD.tipodoc = 3 THEN /* DEV. A CLIENTE */
										(CASE WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt ELSE FI.EPCP * qtt END)*-1
									ELSE
										(CASE WHEN FI.EPCP = 0 THEN FI.ECUSTO * qtt ELSE FI.EPCP * qtt END) 
								END,
			CASE
				WHEN TD.tipodoc = 3 THEN /* DEV. A CLIENTE */
					FI.u_EPVP * qtt * (-1)
				ELSE
					FI.u_EPVP * qtt
			END as u_EPVP
	FROM	FI (nolock) 
	INNER JOIN FT (nolock) ON FI.FTSTAMP = FT.FTSTAMP
	INNER JOIN TD (nolock) ON TD.NDOC = FT.NDOC
	WHERE	(FT.tipodoc!=4 or u_tipodoc=4) AND U_TIPODOC <> 1 and ft.no>199
			AND U_TIPODOC <> 5 AND fdata between @dataIni and @dataFim
			AND fi.composto=0 AND ft.anulado=0
			and ft.site = (case when @site = '' Then ft.site else @site end)
) a
GROUP BY FAMILIA_N, year(data),MONTH(data)
ORDER BY FAMILIA_N, year(data),MONTH(data)
	
GO
Grant Execute on dbo.up_relatorio_gestao_familia_vendas to Public
GO
Grant Control on dbo.up_relatorio_gestao_familia_vendas to Public
Go