
--up_relatorio_fila_operador '20180205', '20180211' ,'Loja 1'

/****** Object:  StoredProcedure [dbo].[up_relatorio_fila_operador]    Script Date: 06/21/2018 09:53:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[up_relatorio_fila_operador]
	@dataIni		datetime
	,@dataFim		datetime
	,@site			varchar(55)

/* with encryption */

AS
SET NOCOUNT ON

	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadospausa'))
		DROP TABLE #dadospausa
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadostotais'))
		DROP TABLE #dadostotais
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosfinais'))
		DROP TABLE #dadosfinais
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores

	/* Calc Loja e Armazens */
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)
	
	/* Calc operadores */
	Select
		iniciais, cm = userno, username
	into
		#dadosOperadores
	from
		b_us (nolock)
	
	/* Define se vais mostrar resultados abaixo do Cliente 199 */
	declare @Entidades as bit
	set @Entidades = case when (select tipoempresa from empresa where site = @site) in ('FARMACIA', 'PARAFARMACIA') then convert(bit,1) else convert(bit,0) end
	
	/* Calc Pausas operadores */
	select 
		userno
		, SUM(ISNULL(duration,0)) as duration
		, count(token) as nr
		, avg(ISNULL(duration,0)) as tmp
	into
		#dadospausa
	from 
		ext_xopvision_breakmotive 
	where 
		convert(varchar, ext_xopvision_breakmotive.start_date, 112) between @dataIni and @dataFim
		--ext_xopvision_breakmotive.start_date between @dataIni and @dataFim
	group by 
		userno
	
	/* Dados Base Vendas  */
	create table #dadosVendasBase (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)
	)
    
    insert #dadosVendasBase
    exec up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site
    
    /* Elimina Vendas tendo em conta tipo de cliente*/
	If @Entidades = 1
	delete from #dadosVendasBase where #dadosVendasBase.no < 199
    
    select 
		#dadosVendasBase.ousrinis
		, b_us.userno
		, SUM(u_epvp) as pvp
	into
		#dadostotais
	from 
		#dadosVendasBase
		inner join b_us on b_us.iniciais=#dadosVendasBase.ousrinis
		inner join Td on Td.ndoc = #dadosVendasBase.ndoc
		left Join St on #dadosVendasBase.ref = St.ref
		left join stFami on #dadosVendasBase.familia = stfami.ref
	where
		#dadosVendasBase.no > 199
		and #dadosVendasBase.tipo != 'ENTIDADE'
		AND #dadosVendasBase.u_tipodoc not in (1, 5)
		AND st.site_nr = @site_nr
	group by 		
		#dadosVendasBase.ousrinis
		, b_us.userno
		
	--up_relatorio_fila_operador '20180205', '20180211' ,'Loja 1'
	/* Preparar Resultados */
	select
		ext_xopvision_queue_management.userno
		, cast(avg(DATEDIFF(MINUTE, ext_xopvision_queue_management.start_date,  B_pagCentral.oData)) as numeric(5,0)) as tmatendimento
		--, cast(max(DATEDIFF(MINUTE, ext_xopvision_queue_management.start_date,  B_pagCentral.oData)) as numeric(5,0)) as tmaxatendimento
		, isnull((select duration  
			from #dadospausa 
			where ext_xopvision_queue_management.userno=#dadospausa.userno),0) as tpausa
		, isnull((select nr  
			from #dadospausa 
			where ext_xopvision_queue_management.userno=#dadospausa.userno),0) as nrpausa
		, cast(isnull((select tmp  
			from #dadospausa 
			where ext_xopvision_queue_management.userno=#dadospausa.userno),0) as numeric(10,0)) as tmp
		, COUNT(distinct(ext_xopvision_queue_management.nrAtend)) as nratend
		, cast((select SUM(pvp) 
				from #dadostotais 
				where #dadostotais.userno=ext_xopvision_queue_management.userno ) as numeric(10,2)) as pvp
	into
		#dadosfinais	
	from 
		ext_xopvision_queue_management (nolock)
		inner join B_pagCentral (nolock) on B_pagCentral.nrAtend=ext_xopvision_queue_management.nrAtend
		--inner join b_us on b_us.userno=ext_xopvision_queue_management.userno
	where
		convert(varchar, start_date, 112) between convert(varchar, @dataIni, 112) and convert(varchar, @dataFim, 112) 
		--start_date between @dataIni and @dataFim
		and isnull(ext_xopvision_queue_management.ticket_id, '')<>''
		and isnull(ext_xopvision_queue_management.nrAtend, '')<>''
		and B_pagCentral.site=@site
	group by
		ext_xopvision_queue_management.userno
	order by 
		 ext_xopvision_queue_management.userno
	
	--up_relatorio_fila_operador '20180205', '20180211' ,'Loja 1'
	/* Resultados finais*/
	select
		upper(b_us.nome) as iniciais
		, #dadosfinais.userno
		, #dadosfinais.tmatendimento
		--, #dadosfinais.tmaxatendimento
		, #dadosfinais.tpausa
		, #dadosfinais.nrpausa
		, #dadosfinais.tmp
		--, #dadosfinais.nratend
		,(select COUNT(distinct(#dadosVendasBase.nratend)) 
			from #dadosVendasBase 
			where #dadosVendasBase.ousrinis=b_us.iniciais
			and #dadosVendasBase.tipo != 'ENTIDADE'
			AND #dadosVendasBase.u_tipodoc not in (1, 5)
			and #dadosVendasBase.ousrinis in (select iniciais from #dadosOperadores)
			) as nratend
		--, #dadosfinais.pvp
		,cast(isnull((select sum(#dadosVendasBase.etiliquido+#dadosVendasBase.ettent1+#dadosVendasBase.ettent2) 
			from #dadosVendasBase 
			where #dadosVendasBase.ousrinis=b_us.iniciais
			and #dadosVendasBase.tipo != 'ENTIDADE'
			AND #dadosVendasBase.u_tipodoc not in (1, 5)
			and #dadosVendasBase.ousrinis in (select iniciais from #dadosOperadores)),0) as numeric(10,2)) as pvp
		,cast(isnull((select sum(#dadosVendasBase.qtt) 
			from #dadosVendasBase 
			where #dadosVendasBase.ousrinis=b_us.iniciais
			and #dadosVendasBase.tipo != 'ENTIDADE'
			AND #dadosVendasBase.u_tipodoc not in (1, 5)
			and #dadosVendasBase.ousrinis in (select iniciais from #dadosOperadores)),0) as numeric(10,0)) as qtt
		,cast(round(isnull((select sum(#dadosVendasBase.etiliquidoSIva + #dadosVendasBase.ettent1SIva + #dadosVendasBase.ettent2SIva)-Sum(#dadosVendasBase.epcpond)
			from #dadosVendasBase 
			where #dadosVendasBase.ousrinis=b_us.iniciais
			and #dadosVendasBase.tipo != 'ENTIDADE'
			AND #dadosVendasBase.u_tipodoc not in (1, 5)
			and #dadosVendasBase.ousrinis in (select iniciais from #dadosOperadores)),0),2) as numeric(10,2)) as bb

	from
		#dadosfinais
		inner join b_us on b_us.userno=#dadosfinais.userno
	order by
		upper(b_us.iniciais)	

--up_relatorio_fila_operador '20180205', '20180211' ,'Loja 1'
	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadospausa'))
		DROP TABLE #dadospausa
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadostotais'))
		DROP TABLE #dadostotais
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosfinais'))
		DROP TABLE #dadosfinais
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores