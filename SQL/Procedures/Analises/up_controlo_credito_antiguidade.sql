/* Relatório Controlo de crédito por antiguidade

	exec up_controlo_credito_antiguidade 0,'20201231','',''
	exec up_controlo_credito_antiguidade 0,'20210828',''
	exec up_controlo_credito_antiguidade 0,'20230307','',''
	exec up_controlo_credito_antiguidade @excluiEntidades=1,@dataFim='2023-03-08 00:00:00',@site=N'Loja 1',@tipoCliente=N''

*/

if OBJECT_ID('[dbo].[up_controlo_credito_antiguidade]') IS NOT NULL
	drop procedure dbo.up_controlo_credito_antiguidade
go

create procedure [dbo].[up_controlo_credito_antiguidade]

	@excluiEntidades	AS BIT , 
	@dataFim			AS DATETIME,
	@site				AS VARCHAR(254),
	@tipoCliente		AS VARCHAR(254)

/* with encryption */

AS
SET NOCOUNT ON


	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRL'))
		DROP TABLE #dadosRL
		
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRL'))
		DROP TABLE #cteDados
	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteDadosSTOCKDATA'))
		DROP TABLE #cteDadosSTOCKDATA

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRLAcertos'))
		DROP TABLE #dadosRLAcertos


	 	SELECT 
			  cc.ccstamp as ccstamp,
			ABS(SUM(rl.erec)) AS total
		INTO 
			#dadosRLAcertos
		FROM 
			cc (NOLOCK)	
			LEFT JOIN rl (NOLOCK)	ON rl.ccstamp=cc.ccstamp
		WHERE
		   rl.dataven <=@dataFim  AND cc.nrdoc=0
		 GROUP BY 
			cc.ccstamp



	 IF(CONVERT(DATE, GETDATE())!=CONVERT(DATE, @dataFim))
	 BEGIN 

	 	SELECT 
			  cc.ccstamp					AS ccstamp,
			ABS(SUM(rl.erec))				AS total
		INTO 
			#dadosRL
		FROM 
			ft (NOLOCK)
			INNER JOIN cc (NOLOCK)	ON cc.ftstamp=ft.ftstamp
			LEFT JOIN rl (NOLOCK)	ON rl.ccstamp=cc.ccstamp
		WHERE
		   rdata <=@dataFim 
		 GROUP BY cc.ccstamp


		SELECT 	
			numero			= ROW_NUMBER() OVER(ORDER BY cc.datalc, cc.ousrhora, cc.cm, cc.nrdoc ASC)
			,ft.no
			,ft.estab
			,nome			= RTRIM(ft.nome) + '[' + CONVERT(VARCHAR,ft.no)+ ']'+ '[' + CONVERT(VARCHAR,ft.estab)+ ']'
			,'CCSTAMP'		= cc.ccstamp,
			'DATAVEN'		= CONVERT(VARCHAR,cc.dataven,102),
			'EDEB'			= cc.edeb,
			'ECRED'			= cc.ecred,
			'EDEBF'			= cc.edebf,
			'ECREDF'		= cc.ecredf,
			'VALOR'			= (cc.edeb-cc.edebf)-(cc.ecred-cc.ecredf)
		INTO
			 #cteDadosSTOCKDATA
		FROM
			ft (NOLOCK)
			LEFT JOIN cc (NOLOCK) ON cc.ftstamp=ft.ftstamp
			LEFT JOIN re (NOLOCK) ON re.restamp = cc.restamp
			LEFT JOIN b_utentes (NOLOCK) ON cc.no = b_utentes.no AND cc.estab = b_utentes.estab 
		WHERE
			(abs((cc.ecred-cc.ecredf)-(cc.edeb-cc.edebf)) > 0.010000)
			AND ft.anulado IS NOT NULL AND  cc.restamp!='' AND ft.fdata <= @dataFim
			AND (
				ft.site = CASE WHEN @site = '' THEN ft.site ELSE @site END
				OR
				re.site = CASE WHEN @site = '' THEN re.site ELSE @site END
				)

		UNION ALL 

			SELECT 	
				numero		= ROW_NUMBER() OVER(ORDER BY cc.datalc, cc.ousrhora, cc.cm, cc.nrdoc ASC)
				,ft.no
			    ,ft.estab
				,nome		= RTRIM(ft.nome) + '[' + CONVERT(VARCHAR,ft.no)+ ']'+ '[' + CONVERT(VARCHAR,ft.estab)+ ']'
				,'CCSTAMP'	= cc.ccstamp,
				'DATAVEN'	= convert(varchar,cc.dataven,102),
				'EDEB'		= cc.edeb,
				'ECRED'		= cc.ecred,
				'EDEBF'		= CASE WHEN cc.edeb>0 THEN isnull(#dadosRL.total,0) ELSE 0 END ,
				'ECREDF'	= CASE WHEN cc.ecred>0 THEN isnull(#dadosRL.total,0) ELSE 0 END ,
				'VALOR'		= CASE WHEN ft.tipodoc=3 THEN (-cc.ecred)+isnull(#dadosRL.total,0) 
														ELSE (cc.ecred+cc.edeb)-isnull(#dadosRL.total,0) END
			FROM
				ft (NOLOCK)
				INNER JOIN cc (NOLOCK) ON cc.ftstamp=ft.ftstamp
				LEFT JOIN re (NOLOCK) ON re.restamp = cc.restamp
				LEFT JOIN b_utentes (NOLOCK) ON cc.no = b_utentes.no AND cc.estab = b_utentes.estab 
				LEFT JOIN #dadosRL (NOLOCK)	ON #dadosRL.ccstamp=cc.ccstamp
			WHERE
				((cc.ecred+cc.edeb)-ISNULL(#dadosRL.total,0) > 0.010000 OR
					(ft.tipodoc=3 AND (-cc.ecred)+isnull(#dadosRL.total,0)<0.0000))
				AND ft.anulado is not null  AND  cc.restamp=''  AND ft.fdata <= @dataFim
				AND (
					ft.site = CASE WHEN @site = '' THEN ft.site ELSE @site END
					OR
					re.site = CASE WHEN @site = '' THEN re.site ELSE @site END
					)
			UNION ALL 

			SELECT 	
				numero			= ROW_NUMBER() OVER(ORDER BY cc.datalc, cc.ousrhora, cc.cm, cc.nrdoc ASC)
				,cc.no
				,cc.estab
				,nome			= RTRIM(cc.nome) + '[' + CONVERT(VARCHAR,cc.no)+ ']'+ '[' + CONVERT(VARCHAR,cc.estab)+ ']'
				,'CCSTAMP'		= cc.ccstamp,
				'DATAVEN'		= CONVERT(VARCHAR,cc.dataven,102),
				'EDEB'			= cc.edeb,
				'ECRED'			= cc.ecred,
				'EDEBF'			= cc.edebf,
				'ECREDF'		= cc.ecredf,
				'VALOR'			= (-cc.ecred)+ISNULL(#dadosRLAcertos.total,0)
			FROM  cc (NOLOCK)
				LEFT JOIN b_utentes (NOLOCK) ON cc.no = b_utentes.no AND cc.estab = b_utentes.estab 
				LEFT JOIN #dadosRLAcertos (NOLOCK)	 ON #dadosRLAcertos.ccstamp=cc.ccstamp
			WHERE
				 cc.ftstamp =''  AND (-cc.ecred)+ISNULL(#dadosRLAcertos.total,0) < 0.00 AND nrdoc=0

			SELECT
				 b_utentes.no
				,b_utentes.estab
				, b_utentes.nome
				, CAST(SUM( CASE WHEN (DATEDIFF(DAY, dataven, @dataFim) BETWEEN 0 AND 15 AND dataven <=@dataFim) 
										THEN VALOR ELSE 0 END) AS NUMERIC(10,2)) AS 'd15'
				, CAST(SUM( CASE WHEN (DATEDIFF(DAY, dataven, @dataFim) BETWEEN 16 AND 30 AND dataven <=@dataFim) 
										THEN VALOR ELSE 0 END) AS NUMERIC(10,2)) AS 'd30'
				, CAST(SUM( CASE WHEN (DATEDIFF(DAY, dataven, @dataFim) BETWEEN 31 AND 60 AND dataven <=@dataFim) 
										THEN VALOR ELSE 0 END) AS NUMERIC(10,2)) AS 'd60'
				, CAST(SUM( CASE WHEN (DATEDIFF(DAY, dataven, @dataFim) BETWEEN 61 AND 90 AND dataven <=@dataFim) 
										THEN VALOR ELSE 0 END) AS NUMERIC(10,2)) AS 'd90'
				, CAST(SUM( CASE WHEN (DATEDIFF(DAY, dataven, @dataFim) BETWEEN 91 AND 180 AND dataven <=@dataFim) 
										THEN VALOR ELSE 0 END) AS NUMERIC(10,2)) AS 'd180'
				, CAST(SUM( CASE WHEN (DATEDIFF(DAY, dataven, @dataFim) BETWEEN 181 AND 270 AND dataven <=@dataFim) 
										THEN VALOR ELSE 0 END) AS NUMERIC(10,2)) AS 'd270'
				, CAST(SUM( CASE WHEN (DATEDIFF(DAY, dataven, @dataFim) BETWEEN 271 AND 360 AND dataven <=@dataFim) 
										THEN VALOR ELSE 0 END) AS NUMERIC(10,2)) AS 'd360'
				, CAST(SUM( CASE WHEN (DATEDIFF(DAY, dataven, @dataFim) BETWEEN 361 AND 540 AND dataven <=@dataFim) 
										THEN VALOR ELSE 0 END) AS NUMERIC(10,2)) AS 'd540'
				, CAST(SUM( CASE WHEN (DATEDIFF(DAY, dataven, @dataFim) BETWEEN 541 AND 720 AND dataven <=@dataFim) 
										THEN VALOR ELSE 0 END) AS NUMERIC(10,2)) AS 'd720'
				, CAST(SUM( CASE WHEN (DATEDIFF(DAY, dataven, @dataFim) > 720 AND dataven <=@dataFim) 
										THEN VALOR ELSE 0 END) AS NUMERIC(10,2)) AS 'm720'
				, CAST(SUM( CASE WHEN (dataven <=@dataFim) THEN VALOR ELSE 0 END) AS NUMERIC(15,2)) AS saldo
				, CAST(eplafond AS NUMERIC(15,2)) as plafond
				, CAST(SUM( CASE WHEN (dataven <=@dataFim) THEN (edeb)-(ecred) ELSE 0 END) AS NUMERIC(15,2)) AS util
				, alimite
				, CAST(SUM( CASE WHEN (DATEDIFF(DAY, dataven, @dataFim) > b_utentes.vencimento AND dataven <=@dataFim) 
										THEN VALOR ELSE 0 END) AS NUMERIC(15,2)) AS 'valorVencido'


			FROM
				b_utentes (NOLOCK)
				LEFT JOIN  #cteDadosSTOCKDATA ON #cteDadosSTOCKDATA.no= b_utentes.no 
												AND #cteDadosSTOCKDATA.estab = b_utentes.estab
			WHERE
				esaldo>0
				AND b_utentes.no > CASE WHEN @excluiEntidades = 1 THEN 200 ELSE 0  END 
				AND (b_utentes.tipo IN (SELECT items from dbo.up_splitToTable(@tipoCliente,',')) OR @tipoCliente='') 
				AND b_utentes.tipo IN  (CASE WHEN @tipoCliente = 'Sem Tipo Cliente Atribuido' 
											THEN '' ELSE b_utentes.tipo END)
			GROUP BY 	 
				b_utentes.no
				,b_utentes.estab
				, b_utentes.nome
				,eplafond
				,alimite
			ORDER BY
				b_utentes.no		

		END
		ELSE 
		BEGIN 
				SELECT 	
					numero		= ROW_NUMBER() OVER(ORDER BY cc.datalc, cc.ousrhora, cc.cm, cc.nrdoc ASC)
					,ft.no
					,ft.estab
					,nome		= RTRIM(ft.nome) + '[' + CONVERT(VARCHAR,ft.no)+ ']'+ '[' + CONVERT(VARCHAR,ft.estab)+ ']'
					,'CCSTAMP'	= cc.ccstamp,
					'DATAVEN'	= CONVERT(VARCHAR,cc.dataven,102),
					'EDEB'		= cc.edeb,
					'ECRED'		= cc.ecred,
					'EDEBF'		= cc.edebf,
					'ECREDF'	= cc.ecredf,
					'VALOR'		= (cc.edeb-cc.edebf)-(cc.ecred-cc.ecredf)
				INTO 
					#cteDados
				FROM
					ft (NOLOCK)
					LEFT JOIN cc (NOLOCK) ON cc.ftstamp=ft.ftstamp
					LEFT JOIN re (NOLOCK) ON re.restamp = cc.restamp
					LEFT JOIN b_utentes (NOLOCK) ON cc.no = b_utentes.no AND cc.estab = b_utentes.estab 
				WHERE
					(abs((cc.ecred-cc.ecredf)-(cc.edeb-cc.edebf)) > 0.010000)
					AND ft.anulado IS NOT NULL
					AND ft.fdata <= @dataFim
									AND (
					ft.site = CASE WHEN @site = '' THEN ft.site ELSE @site END
					or
					re.site = CASE WHEN @site = '' THEN re.site ELSE @site END
				)

				UNION ALL 

				SELECT 	
					numero		= ROW_NUMBER() OVER(ORDER BY cc.datalc, cc.ousrhora, cc.cm, cc.nrdoc ASC)
					,cc.no
					,cc.estab
					,nome		= RTRIM(cc.nome) + '[' + CONVERT(VARCHAR,cc.no)+ ']'+ '[' + CONVERT(VARCHAR,cc.estab)+ ']'
					,'CCSTAMP'	= cc.ccstamp,
					'DATAVEN'	= CONVERT(VARCHAR,cc.dataven,102),
					'EDEB'		= cc.edeb,
					'ECRED'		= cc.ecred,
					'EDEBF'		= cc.edebf,
					'ECREDF'	= cc.ecredf,
					'VALOR'		= (-cc.ecred)+ISNULL(#dadosRLAcertos.total,0)
				FROM  
					cc (NOLOCK)
					LEFT JOIN b_utentes (NOLOCK) ON cc.no = b_utentes.no AND cc.estab = b_utentes.estab 
					LEFT JOIN #dadosRLAcertos (NOLOCK) ON #dadosRLAcertos.ccstamp=cc.ccstamp
				WHERE
					cc.ftstamp =''  AND (-cc.ecred)+isnull(#dadosRLAcertos.total,0) < 0.00 AND nrdoc=0

			SELECT 
				 b_utentes.no
				,b_utentes.estab
				, b_utentes.nome
				, CAST(SUM( CASE WHEN (DATEDIFF(DAY, dataven, @dataFim) BETWEEN 0 AND 15 AND dataven <=@dataFim) 
										THEN VALOR ELSE 0 END) AS NUMERIC(10,2)) AS 'd15'
				, CAST(SUM( CASE WHEN (DATEDIFF(DAY, dataven, @dataFim) BETWEEN 16 AND 30 AND dataven <=@dataFim) 
										THEN VALOR ELSE 0 END) AS NUMERIC(10,2)) AS 'd30'
				, CAST(SUM( CASE WHEN (DATEDIFF(DAY, dataven, @dataFim) BETWEEN 31 AND 60 AND dataven <=@dataFim) 
										THEN VALOR ELSE 0 END) AS NUMERIC(10,2)) AS 'd60'
				, CAST(SUM( CASE WHEN (DATEDIFF(DAY, dataven, @dataFim) BETWEEN 61 AND 90 AND dataven <=@dataFim) 
										THEN VALOR ELSE 0 END) AS NUMERIC(10,2)) AS 'd90'
				, CAST(SUM( CASE WHEN (DATEDIFF(DAY, dataven, @dataFim) BETWEEN 91 AND 180 AND dataven <=@dataFim) 
										THEN VALOR ELSE 0 END) AS NUMERIC(10,2)) AS 'd180'
				, CAST(SUM( CASE WHEN (DATEDIFF(DAY, dataven, @dataFim) BETWEEN 181 AND 270 AND dataven <=@dataFim) 
										THEN VALOR ELSE 0 END) AS NUMERIC(10,2)) AS 'd270'
				, CAST(SUM( CASE WHEN (DATEDIFF(DAY, dataven, @dataFim) BETWEEN 271 AND 360 AND dataven <=@dataFim) 
										THEN VALOR ELSE 0 END) AS NUMERIC(10,2)) AS 'd360'
				, CAST(SUM( CASE WHEN (DATEDIFF(DAY, dataven, @dataFim) BETWEEN 361 AND 540 AND dataven <=@dataFim) 
										THEN VALOR ELSE 0 END) AS NUMERIC(10,2)) AS 'd540'
				, CAST(SUM( CASE WHEN (DATEDIFF(DAY, dataven, @dataFim) BETWEEN 541 AND 720 AND dataven <=@dataFim) 
										THEN VALOR ELSE 0 END) AS NUMERIC(10,2)) AS 'd720'
				, CAST(SUM( CASE WHEN (DATEDIFF(DAY, dataven, @dataFim) > 720 AND dataven <=@dataFim) 
										THEN VALOR ELSE 0 END) AS NUMERIC(10,2)) AS 'm720'
				, CAST(SUM( CASE WHEN (dataven <=@dataFim) THEN VALOR ELSE 0 END) AS NUMERIC(15,2)) AS saldo
				, CAST(eplafond AS NUMERIC(15,2)) AS plafond
				, CAST(SUM( CASE WHEN (dataven <=@dataFim) THEN (edeb)-(ecred) ELSE 0 END) AS NUMERIC(15,2)) AS util
				, alimite
				, CAST(SUM( CASE WHEN (DATEDIFF(DAY, dataven, @dataFim) > b_utentes.vencimento AND dataven <=@dataFim) 
										THEN VALOR ELSE 0 END) AS NUMERIC(10,2)) AS 'valorVencido'
			FROM
				b_utentes (NOLOCK)
				INNER JOIN  #cteDados ON #cteDados.no= b_utentes.no AND #cteDados.estab = b_utentes.estab
			WHERE
				esaldo>0
				AND b_utentes.no > CASE WHEN @excluiEntidades = 1 THEN 200 ELSE 0  END
				AND (b_utentes.tipo in (SELECT items from dbo.up_splitToTable(@tipoCliente,',')) OR @tipoCliente='')
			GROUP BY 	 
					b_utentes.no
					,b_utentes.estab
					, b_utentes.nome
					,eplafond
					,alimite
			ORDER BY
					b_utentes.no		
		END 

		
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRL'))
		DROP TABLE #dadosRL

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRL'))
		DROP TABLE #cteDados
	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#cteDadosSTOCKDATA'))
		DROP TABLE #cteDadosSTOCKDATA

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRLAcertos'))
		DROP TABLE #dadosRLAcertos


GO
Grant Execute on up_controlo_credito_antiguidade to Public
Grant control on up_controlo_credito_antiguidade to Public
GO