/* Relatório Quantidade Vendida Mensal - Relatório + Utilizado pelas Farmácias 
	
	exec up_relatorio_vendas_base_detalhe '20170129','20170129', 'Loja 1', 1

	exec up_relatorio_vendas_qtVendidaMensal '20170727', '20170728', '', '', '', '', '', 0, 'Loja 3' , 0, 0, 0, 0, '', 0, 0, 0, 0, 0, '', '', '', '', '', '', -1
	

	exec up_relatorio_vendas_qtVendidaMensal '20170727', '20170728', '', '', '', '', '', 0, 'Loja 2' , 0, 0, 0, 0, '', 0, 0, 0, 0, 0, '', '', '', '', '', '', -1

	exec up_relatorio_vendas_qtVendidaMensal '20170727', '20170728', '', '', '', '', '', 0, 'Loja 1' , 0, 0, 0, 0, '', 0, 0, 0, 0, 0, '', '', '', '', '', '', -1

	exec up_relatorio_vendas_qtVendidaMensal '20170727', '20171013', '', '', '', '', '', 0, 'TODAS' , 0, 0, 0, 0, '', 0, 0, 0, 0, 0, '', '', '', '', '', '', -1, 1
*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_vendas_qtVendidaMensal]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_qtVendidaMensal
go

create procedure dbo.up_relatorio_vendas_qtVendidaMensal
	@dataIni						as datetime
	,@dataFim						as datetime
	,@op							varchar(55)
	,@ref							as varchar(1024)
	,@design						as varchar(254)
	,@familia						as varchar(max)
	,@lab							as varchar(254)
	,@Dev							as bit
	,@site							as varchar(55)
	,@pvp							as bit
	,@stock							as bit
	,@pcl							as bit
	,@maxenc						as bit
	,@atributosFam					as varchar(max)
	,@qttvendida					as numeric(9,0)
	,@mostraProdutosNIncluidoVendas as bit
	,@incluiHistorico				bit = 0
	,@pvu							as bit = 0
	,@incluiServicos				as bit
	,@fornecedor					as varchar(254)
	,@dci							as varchar(120)
	,@tipoProduto					as varchar(254)
	,@usr1							as varchar(254)
	,@generico						varchar(18) = ''
	,@no							as varchar(9) = ''
	,@estab							as numeric(5,0) = -1
	,@ativos						as bit

	

/* with encryption */
AS
BEGIN	
	
	set language portuguese
	If OBJECT_ID('tempdb.dbo.#dadosVendasBase') IS NOT NULL
		DROP TABLE #dadosVendasBase
	If OBJECT_ID('tempdb.dbo.#dadosST') IS NOT NULL
		DROP TABLE #dadosST
	If OBJECT_ID('tempdb.dbo.#dadosFamilia') IS NOT NULL
		DROP TABLE #dadosFamilia
	If OBJECT_ID('tempdb.dbo.#qtVendidas') IS NOT NULL
		DROP TABLE #qtVendidas
	If OBJECT_ID('tempdb.dbo.#dadosGerais') IS NOT NULL
		DROP TABLE #dadosGerais
	If OBJECT_ID('tempdb.dbo.#dadosResumidos') IS NOT NULL
		DROP TABLE #dadosResumidos
	If OBJECT_ID('tempdb.dbo.#EmpresaArm') IS NOT NULL
		DROP TABLE #EmpresaArm
	If OBJECT_ID('tempdb.dbo.#dadosMarcas') IS NOT NULL
		DROP TABLE #dadosMarcas
	If OBJECT_ID('tempdb.dbo.#dadosAnoMes') IS NOT NULL
		drop table #dadosAnoMes;
	If OBJECT_ID('tempdb.dbo.#dadosOperadores') IS NOT NULL
			drop table #dadosOperadores;

	/* Calc Loja e Armazens */

	--declare @site_nr as tinyint
	--set @site_nr = ISNULL((select no from empresa where site = @site),0)

	if(@site='TODAS')
		set @site=''


		/* Converter Valor Generico*/
	IF @generico = 'GENERICO'
		set @generico = 1

	ELSE IF @generico = 'NÃO GENERICO'
		set @generico = 0


	declare @tipoempresa as varchar(50)
	set @tipoempresa = ISNULL(( Select tipoempresa from empresa  where site = @site),'')

	select 
		armazem
	into
		#EmpresaArm
	From
		empresa
		inner join empresa_arm on empresa.no = empresa_arm.empresa_no
	where
		empresa.site = case when @site = '' then empresa.site else @site  end


	/* Define se vais mostrar resultados abaixo do Cliente 199 */
	declare @Entidades as bit
	set @Entidades = case when (select tipoempresa from empresa where site = @site) in ('FARMACIA', 'PARAFARMACIA') then convert(bit,1) else convert(bit,0) end

	/* Dados Base Vendas  */
	create table #dadosVendasBase (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)

	)


    
    insert #dadosVendasBase
    exec up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site, @incluiHistorico



	/* Elimina Vendas tendo em conta tipo de cliente */
	--If @Entidades = 1
	delete 
	from 
		#dadosVendasBase 
	where  
		#dadosVendasBase.no between 1 and 198  
		and #dadosVendasBase.loja_nr in (select no from empresa WHERE tipoempresa in ('FARMACIA', 'PARAFARMACIA') )




	/* Calc Dados Produto */	
	Select 
		st.ref
		,st.design
		,stock = isnull((select SUM(stock) from sa where sa.ref = st.ref and armazem in (select armazem from #EmpresaArm)),st.stock)
		,st.u_lab
		,st.usr1
		,st.familia
		,epv1 
		,epv1Siva = case when st.ivaincl = 1 then st.epv1 / (isnull(taxasiva.taxa,0)/100+1) else st.epv1 end
		,st.inactivo
		,epcult
		,st.stmax
		,st.ptoenc
		,st.fornecedor
		,departamento = isnull(b_famDepartamentos.design ,'')
		,seccao = isnull(b_famSeccoes.design,'')
		,categoria = isnull(b_famCategorias.design,'')
		,tipoProduto =  isnull(b_famFamilias.design,'')
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 1) valDepartamento
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 2) valSeccao
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 3) valCategoria
		--,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 4) valFamfamilia
		,st.ivaincl
		,st.stns
		,fprod.dci
		,st.site_nr
		,empresa.site
	into 
		#dadosST
	From
		st (nolock)
		inner join empresa (nolock) on empresa.no = st.site_nr
		left join fprod (nolock) on st.ref = fprod.cnp
		left join b_famDepartamentos (nolock) on st.u_depstamp = b_famDepartamentos.depstamp
		left join b_famSeccoes (nolock) on st.u_secstamp = b_famSeccoes.secstamp
		left join b_famCategorias (nolock) on st.u_catstamp = b_famCategorias.catstamp
		left join b_famFamilias (nolock) on st.u_famstamp = b_famFamilias.famstamp
		left join taxasiva (nolock) on st.tabiva = taxasiva.codigo 
	where
		empresa.site  = case when @site!='' then @site else empresa.site  end
		/*marcas*/
		and (
			st.usr1 in (select items from dbo.up_splitToTable(@usr1,','))
			or @usr1 = '0'
			or @usr1 = ''
		)
		/* Tratamento produto generico*/
		and isnull(fprod.generico,0) = case when @generico = '' then isnull(fprod.generico,0) else @generico end
		-- Deferenciar se estão ativos ou não
		and st.inactivo between 0 and (case when @ativos=1 then 0 else 1 end)

	/* Calc Familias */
	Select
		distinct ref
	into
		#dadosFamilia
	From
		stfami (nolock)
	Where
		ref in (Select items from dbo.up_splitToTable(@familia,','))
		Or @familia = ''

	/* Calc operadores */
	Select
		iniciais, cm = userno, username
	into
		#dadosOperadores
	from
		b_us (nolock)
	Where
		userno in (select items from dbo.up_splitToTable(@op,','))
		or @op= '0'
		or @op= ''
	

	/* Calc Result Set */
	SELECT
		'ANO'			= year(fdata)
		,'MES'			= STR(MONTH(fdata)) + ' - ' + LEFT(UPPER(datename(month,fdata)),3)
		,'REF'			= cteSt.ref
		,'DESIGN'		= cteSt.design
		,'QTD'			= SUM(ft.qtt)
		,'STOCKACTUAL'	= cteSt.stock
		,'STOCKMAX'		= cteSt.stmax
		,'PTOENC'		= cteSt.ptoenc
		,'FAMILIA'		= ft.familia
		,'LAB'			= u_lab
		,vdpvpcl		= SUM(etiliquido)
		,vdpvp			= SUM(etiliquido + ettent1 + ettent2)
		,vdpvcl			= SUM(etiliquidoSiva)
		,vdpv			= SUM(etiliquidoSiva + ettent1Siva + ettent2Siva)
		,'INACTIVO'		= cteSt.inactivo
		,pclficha		= cteSt.epcult
		,vdpcl			= SUM(ft.ecusto)
		,servico		= cteSt.stns
		,pvFicha		= cteSt.epv1Siva
		,pvpFicha		= cteSt.epv1
		,dci			= cteSt.dci
		,usr1			= cteSt.usr1
		
	
		
	into
		#qtVendidas
	FROM
		#dadosVendasBase ft
		inner join #dadosST cteSt on ft.ref = cteSt.ref and ft.loja_nr = cteSt.site_nr
	WHERE
		ft.no = case when @no = '' then ft.no else @no end
		and (ft.no = 0 or ft.no > 198 or @tipoempresa = 'CLINICA')
		and ft.estab = case when @estab>-1 then  @estab else ft.estab  end
		and	(ft.tipodoc!=4 or ft.u_tipodoc=4) AND ft.u_tipodoc != 1 AND ft.u_tipodoc != 5 
		and (ft.ref in (Select items from dbo.up_splitToTable(@ref,',')) or @ref = '')
		and cteSt.DESIGN like @design + '%'
		and ft.familia in (Select ref from #dadosFamilia)
		/* filtrar por operador caso selecionado */
		and ft.ousrinis in (select iniciais from #dadosOperadores)
		and (isnull(cteSt.u_lab,'') in (Select items from dbo.up_splitToTable(@lab,';')) or @lab = '')
		and isnull(ctest.fornecedor,'') = case when @fornecedor = '' then isnull(ctest.fornecedor,'') else @fornecedor end
		and isnull(ctest.dci,'') = case when @dci = '' then isnull(ctest.dci,'') else @dci end
		/* tratamento tipo produto */
		and isnull(cteSt.tipoProduto, '') = case when @tipoProduto = '' then isnull(cteSt.tipoProduto, '') else @tipoProduto end
		/* Tratamento Familias*/
		and (
			(departamento	= valDepartamento or valDepartamento is null)
			and (seccao		= valSeccao or valSeccao is null)
			and (categoria	= valCategoria or valCategoria is null)
			)
		

	GROUP BY 
		year(fdata)
		,datename(MONTH,fdata)
		,MONTH(fdata)
		,cteSt.ref
		,cteSt.design
		,cteSt.stock
		,ft.familia
		,cteSt.u_lab
		,cteSt.epv1
		,cteSt.inactivo
		,cteSt.epcult
		,cteSt.stmax
		,cteSt.ptoenc
		,cteSt.epv1Siva
		,cteSt.stns
		,ctest.dci
		,cteSt.usr1
		
		

	/*Elimina Qtt Vendida Inferir a x */
	if @qttvendida > 0
	delete from #qtVendidas where ref in (select ref from #qtVendidas group by ref having sum(qtd) < @qttvendida ) 
		
	/* Elimina Servicos*/
	if @incluiServicos = 0
	delete from #qtVendidas where servico = 1




	/* Devoluções a Fornecedor */
	if @Dev = 1
	begin
		insert into #qtVendidas
		SELECT
			'ANO'			= year(bo.dataobra)
			,'MES'			= STR(MONTH(bo.dataobra)) + ' - ' + LEFT(UPPER(datename(month,bo.dataobra)),3)
			,'REF'			= cteSt.ref
			,'DESIGN'		= bi.design
			,'QTD'			= SUM(bi.qtt)
			,'STOCKACTUAL'	= cteSt.stock
			,'STOCKMAX'		= cteSt.stmax
			,'PTOENC'		= cteSt.ptoenc
			,'FAMILIA'		= ISNULL(bi.familia,99)
			,'LAB'			= u_lab
			,vdpvpcl		= 0
			,vdpvp			= 0
			,vdpvcl			= 0
			,vdpv			= 0
			,'INACTIVO'		= cteSt.inactivo
			,pclficha		= cteSt.epcult
			,vdpcl			= 0
			,servico		= cteSt.stns
			,pvFicha		= cteSt.epv1Siva
			,pvpFicha		= cteSt.epv1
			,dci			= cteSt.dci	
			,usr1			= cteSt.usr1
		FROM
			bi (nolock)
			inner join bo (nolock) on bo.bostamp=bi.bostamp
			inner join #dadosSt cteSt on bi.ref=cteSt.ref and bo.site = cteSt.site
			
		WHERE
			bo.nmdos = 'Devol. a Fornecedor'
			and bo.no = case when @no = '' then bo.no else @no end
			and bo.estab = case when @estab >-1 then bo.estab else @estab end
			and (bo.dataobra between @dataIni and @dataFim) 
			/*and BI.ref like @ref + '%' */
			and (bi.ref in (Select items from dbo.up_splitToTable(@ref,',')) or @ref = '')
			and BI.DESIGN like @design + '%'
			and bi.familia in (Select ref from #dadosFamilia)
			--and isnull(cteSt.u_lab,'') = case when @lab = '' then isnull(cteSt.u_lab,'') else @lab end
			and (isnull(cteSt.u_lab,'') in (Select items from dbo.up_splitToTable(@lab,';')) or @lab = '')
			and bo.site = (case when @site = '' Then bo.site else @site end)
			and isnull(ctest.dci,'') = case when @dci = '' then isnull(ctest.dci,'') else @dci end
		GROUP BY
			year(bo.dataobra)
			,datename(month,bo.dataobra)
			,MONTH(bo.dataobra)
			,cteSt.ref
			,bi.design
			,cteSt.stock
			,bi.familia
			,cteSt.u_lab
			,cteSt.epv1
			,cteSt.inactivo
			,cteSt.epcult
			,cteSt.stmax 
			,cteSt.ptoenc
			,cteSt.epv1Siva
			,cteSt.stns
			,cteSt.dci
			,cteSt.usr1
	end

	/* Inclui todos os produtos, mesmo os que não estão incluidos nas vendas, está separado porque esta opção é muito mais lenta */
	IF @mostraProdutosNIncluidoVendas = 1 
	BEGIN 
		
		Declare @dataactual as datetime
		set  @dataactual = DATEADD(month, DATEDIFF(month, 0, @dataIni), 0) 
			
		SELECT '19000101' as data, 1900 as ano,12 as mes INTO  #dadosAnoMes where 1=0
			
		WHILE @dataactual <= @dataFim
		BEGIN
			INSERT INTO #dadosAnoMes 
			SELECT @DATAACTUAL as data, year(@DATAACTUAL) as ano, month(@DATAACTUAL) as mes
			
			SET @DATAACTUAL = DATEADD(MM,1,@DATAACTUAL)
		END

		insert into #qtVendidas

		SELECT	
			'ANO'			= dam.ano
			,'MES'			= STR(MONTH(data)) + ' - ' + LEFT(UPPER(datename(month,data)),3)
			,'REF'			= st.ref
			,'DESIGN'		= st.design
			,'QTD'			= 0
			,'STOCKACTUAL'	= st.stock
			,'STOCKMAX'		= st.stmax
			,'PTOENC'		= st.ptoenc
			,'FAMILIA'		= st.familia
			,'LAB'			= st.u_lab
			,vdpvpcl		= 0
			,vdpvp			= 0
			,vdpvcl			= 0
			,vdpv			= 0
			,'INACTIVO'		= st.inactivo
			,pclficha		= st.epcult
			,vdpcl			= 0
			,servico		= st.stns
			,pvFicha		= st.epv1Siva
			,pvpFicha		= st.epv1
			,dci			= st.dci
			,usr1			= st.usr1
			
		from 
			#dadosAnoMes dam
			,#dadosST st
		Where
			(st.ref in (Select items from dbo.up_splitToTable(@ref,',')) or @ref = '')
			and st.DESIGN like @design + '%'
			and st.familia in (Select ref from #dadosFamilia)
			and (isnull(st.u_lab,'') in (Select items from dbo.up_splitToTable(@lab,';')) or @lab = '')
			and isnull(st.fornecedor,'') = case when @fornecedor = '' then isnull(st.fornecedor,'') else @fornecedor end
			and isnull(st.dci,'') = case when @dci = '' then isnull(st.dci,'') else @dci end
			and isnull(st.tipoProduto, '') = case when @tipoProduto = '' then isnull(st.tipoProduto, '') else @tipoProduto end
			/* Tratamento Familias*/
			and 
				(
					(departamento	= valDepartamento or valDepartamento is null)
					and (seccao		= valSeccao or valSeccao is null)
					and (categoria	= valCategoria or valCategoria is null)
				)	
	END

	Select * from #qtVendidas

	If OBJECT_ID('tempdb.dbo.#dadosVendasBase') IS NOT NULL
		DROP TABLE #dadosVendasBase
	If OBJECT_ID('tempdb.dbo.#dadosST') IS NOT NULL
		DROP TABLE #dadosST
	If OBJECT_ID('tempdb.dbo.#dadosFamilia') IS NOT NULL
		DROP TABLE #dadosFamilia
	If OBJECT_ID('tempdb.dbo.#qtVendidas') IS NOT NULL
		DROP TABLE #qtVendidas
	If OBJECT_ID('tempdb.dbo.#dadosGerais') IS NOT NULL
		DROP TABLE #dadosGerais
	If OBJECT_ID('tempdb.dbo.#dadosResumidos') IS NOT NULL
		DROP TABLE #dadosResumidos
	If OBJECT_ID('tempdb.dbo.#EmpresaArm') IS NOT NULL
		DROP TABLE #EmpresaArm
	If OBJECT_ID('tempdb.dbo.#dadosMarcas') IS NOT NULL
		DROP TABLE #dadosMarcas
	If OBJECT_ID('tempdb.dbo.#dadosAnoMes') IS NOT NULL
			drop table #dadosAnoMes;
	If OBJECT_ID('tempdb.dbo.#dadosOperadores') IS NOT NULL
			drop table #dadosOperadores;

END

GO
Grant Execute on dbo.up_relatorio_vendas_qtVendidaMensal to Public
Grant control on dbo.up_relatorio_vendas_qtVendidaMensal to Public
GO