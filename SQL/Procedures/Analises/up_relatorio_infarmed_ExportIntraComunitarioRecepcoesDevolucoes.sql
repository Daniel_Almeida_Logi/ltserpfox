-- Relatório Quantidades Compradas INFARMED
-- exec	up_relatorio_infarmed_ExportIntraComunitarioRecepcoesDevolucoes 2021,01,'Loja 1'
-- Quantidades recebidas/devolvidas: Ano, Mês, Nº Registo, Quantidade encomendada,Quantidade Recebida, Nif forenecedor, designação Fornecedor, código Pais
-- ex. 2013,05,5019302,123213
-- Listagem Para Farmácias

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_infarmed_ExportIntraComunitarioRecepcoesDevolucoes]') IS NOT NULL
	drop procedure dbo.up_relatorio_infarmed_ExportIntraComunitarioRecepcoesDevolucoes
go

create procedure dbo.up_relatorio_infarmed_ExportIntraComunitarioRecepcoesDevolucoes

@ano as numeric(4),
@mes as numeric(2),
@site as varchar(60)

/* with encryption */
AS

SET NOCOUNT ON

		Select 
			ano
			,mes
			,ref
			,qttEncomendada =  convert(int,SUM(qttEncomendada))
			,qttRecebida = convert(int,SUM(qttRecepcionada) - SUM(qttDevolvida))
			,nifFornecedor = nif
			,fornecedor = convert(varchar(254),REPLACE(fornecedor,',',' '))
			,pais = case when (isnull(pais,'') = '' or isnull(pais,'') = 'PT') then 'PRT' else pais end

		From (
					
			/* Encomendas */
			select 
				ano = year(bo.dataobra)
				,mes = convert(varchar(2),replicate('0',2-len(month(bo.dataobra))) + convert(varchar,month(bo.dataobra)))
				,nif = fl.ncont
				,fornecedor = fl.nome
				--,pais = fl.nacional
				,pais = fl.pncont
				,bi.ref
				,qttEncomendada = SUM(bi.qtt)
				,qttDevolvida = 0
				,qttRecepcionada = 0
				,devolucoes = convert(bit,0)
			from
				bo (nolock)
				inner join bi (nolock) on bo.bostamp=bi.bostamp
				inner join st (nolock) on bi.ref=st.ref and st.site_nr=(select no from empresa where site=bo.site)
				inner join fprod (nolock) on fprod.cnp = bi.ref
				inner join st_infarmed_intraconsumos (nolock) on st_infarmed_intraconsumos.ref=st.ref
				inner join fl (nolock) on  bo.no = fl.no and fl.estab = bo.estab
			where 
				year(bo.dataobra) = @ano
				AND month(bo.dataobra) = @mes
				and bo.site = @site
				and bo.ndos = 2
				and (ltrim(rtrim(fl.nome)) not like '%esgotado%' and fl.ncont!='')
				
				
			Group by
				bo.dataobra
				,bi.ref
				,fl.ncont
				,fl.nome
				--,fl.nacional
				,fl.pncont
			
			union all

			/*Documentos de Compras que movimentam stock*/
			select 
				ano = year(fo.data)
				,mes = convert(varchar(2),replicate('0',2-len(month(fo.data))) + convert(varchar,month(fo.data)))
				,nif = fl.ncont
				,fl.nome
				--,pais = fl.nacional
				,pais = fl.pncont
				,fn.ref
				,qttEncomendada = 0
				,qttDevolvida = 0
				,qttRecepcionada = SUM(case when cm1.fosl < 50 then fn.qtt else -fn.qtt end)
				,devolucoes = convert(bit,0)
			from
				fo (nolock)
				inner join fn (nolock) on fo.fostamp=fn.fostamp
				inner join cm1 (nolock) on cm1.cm = fo.doccode
				inner join st (nolock) on fn.ref=st.ref and st.site_nr=(select no from empresa where site=fo.site)
				inner join fprod (nolock) on fprod.cnp = fn.ref
				inner join st_infarmed_intraconsumos (nolock) on st_infarmed_intraconsumos.ref=st.ref
				inner join fl (nolock) on  fl.no = fo.no and fl.estab = fo.estab
			where 
				year(fo.data) = @ano
				AND month(fo.data) = @mes
				and fo.site = @site
				and cm1.folansl = 1
				and (ltrim(rtrim(fl.nome)) not like '%esgotado%' and fl.ncont!='')
			Group by
				fo.data
				,fn.ref
				,fl.ncont
				,fl.nome
				--,fl.nacional
				,pncont
				
			union all
			
			/* Devoluções */
			select 
				ano = year(bo.dataobra)
				,mes = convert(varchar(2),replicate('0',2-len(month(bo.dataobra))) + convert(varchar,month(bo.dataobra)))
				,nif = fl.ncont
				,fornecedor = fl.nome
				--,pais = fl.nacional
				,pais = fl.pncont
				,bi.ref
				,qttEncomendada = 0
				,qttDevolvida = SUM(bi.qtt)
				,qttRecepcionada = 0
				,devolucoes = convert(bit,1) 
			from
				bo (nolock)
				inner join bi (nolock) on bo.bostamp=bi.bostamp
				inner join st (nolock) on bi.ref=st.ref and st.site_nr=(select no from empresa where site=bo.site)
				inner join fprod (nolock) on fprod.cnp = bi.ref
				inner join st_infarmed_intraconsumos (nolock) on st_infarmed_intraconsumos.ref=st.ref
				inner join fl (nolock) on  bo.no = fl.no and fl.estab = bo.estab
			where 
				year(bo.dataobra) = @ano
				AND month(bo.dataobra) = @mes
				and bo.site = @site
				and bo.ndos = 17 
				and (ltrim(rtrim(fl.nome)) not like '%esgotado%' and fl.ncont!='')
			Group by
				bo.dataobra
				,bi.ref
				,fl.ncont
				,fl.nome
				--,fl.nacional
				,fl.pncont

			
		) x
		Group by
			ano
			,mes
			,nif
			,fornecedor
			,pais
			,ref
			,devolucoes /* As devolução deve estar em linhas separadas segundo a documentação*/

		order by
			ano
			,mes
			,ref
	
GO
Grant Execute on dbo.up_relatorio_infarmed_ExportIntraComunitarioRecepcoesDevolucoes to Public
GO
Grant Control on dbo.up_relatorio_infarmed_ExportIntraComunitarioRecepcoesDevolucoes to Public
Go
