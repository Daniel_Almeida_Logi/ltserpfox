
-- exec up_relatorio_MultiRelatorio 1,'Agrupado por produto'

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_MultiRelatorio]') IS NOT NULL
	drop procedure dbo.up_relatorio_MultiRelatorio
go

create procedure dbo.up_relatorio_MultiRelatorio
@ordem	as numeric(9,0),
@descricao	as varchar(254)

/* with encryption */

AS 
BEGIN

	declare @stringMulti as varchar(254)
	set @stringMulti = (select report from b_analises where ordem = @ordem)

	Select 
		relatorio = (select Top 1 items from up_SplitToTableCId(items,':')),
		descricao = (select Top 1 items from up_SplitToTableCId(items,':') where id = 2)
	from 
		up_SplitToTable(@stringMulti,';')
	Where
		(select Top 1 items from up_SplitToTableCId(items,':') where id = 2) = @descricao
		
END

GO
Grant Execute on dbo.up_relatorio_MultiRelatorio to Public
Grant control on dbo.up_relatorio_MultiRelatorio to Public
GO
