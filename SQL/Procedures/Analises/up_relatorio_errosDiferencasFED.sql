/* 
	EXEC up_relatorio_errosDiferencasFED 2023,01,'Loja 1' 
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_errosDiferencasFED]') IS NOT NULL
	drop procedure dbo.up_relatorio_errosDiferencasFED
go

create procedure dbo.up_relatorio_errosDiferencasFED
@ano		INT,
@mes		INT,
@site		VARCHAR(60)
/* with encryption */
AS 
BEGIN

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#ftMoreRecent'))
  		DROP TABLE #ftMoreRecent

	SELECT
		ft.nome																	AS nome,
		ft.fno																	AS numDoc,
		CONVERT(VARCHAR,ft.fdata,102)											AS dataDoc,
		ft.nmdoc																AS nomeDoc, 
		ISNULL(f.description, '')												AS erroDoc,
		ISNULL(fd.credValue,0)													AS valor,
		ROW_NUMBER() OVER(PARTITION BY med.data ORDER BY f.ousrdata desc, med.data desc)			AS ftRecent,
		CAST(ISNULL(fd.pathFile, '') AS VARCHAR(254)) as pathFile,
		ISNULL(cptorg.abrev, '')												AS abrev,
		ft.ftstamp,
		ISNULL(fd.xmlPrefix, '')												AS xmlPrefix,
		f.token
	INTO 
		#ftMoreRecent
	FROM 
		faturacao_eletronica_med(nolock) as med
	INNER JOIN
		ft(nolock) ON med.ftstamp = ft.ftstamp
	LEFT JOIN
		facturacao_eletronica_conferencia (NOLOCK) as f ON ft.ftstamp = f.ftstamp
	LEFT JOIN 
		facturacao_eletronica_conferencia_d (NOLOCK)fd ON f.token = fd.token
	LEFT JOIN 
		cptorg(nolock) on cptorg.u_no = ft.no 
	WHERE 
		med.ano = @ano
		AND med.mes = @mes
		AND ft.site = @site
	ORDER BY 
		f.ousrdata DESC

	SELECT 
		*
	FROM
		#ftMoreRecent
	WHERE
		ftRecent = 1
	ORDER BY
		nome asc
	

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#ftMoreRecent'))
  		DROP TABLE #ftMoreRecent
END

GO
Grant Execute on dbo.up_relatorio_errosDiferencasFED to Public
Grant control on dbo.up_relatorio_errosDiferencasFED to Public
GO
