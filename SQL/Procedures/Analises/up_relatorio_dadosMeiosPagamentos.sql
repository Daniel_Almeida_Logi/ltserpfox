/*
	exec up_relatorio_dadosMeiosPagamentos 

	update b_modoPag set inativo = 0 where ref in('05') --,'06','07','08')
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_dadosMeiosPagamentos]') IS NOT NULL
	drop procedure dbo.up_relatorio_dadosMeiosPagamentos
go

create procedure dbo.up_relatorio_dadosMeiosPagamentos

/* with encryption */
AS
BEGIN

	DECLARE @nome_epaga1	AS VARCHAR(60)
	DECLARE	@nome_epaga2	AS VARCHAR(60)
	DECLARE	@nome_epaga3	AS VARCHAR(60)
	DECLARE	@nome_epaga4	AS VARCHAR(60)
	DECLARE	@nome_epaga5	AS VARCHAR(60)
	DECLARE	@nome_epaga6	AS VARCHAR(60)
	DECLARE	@nome_epaga7	AS VARCHAR(60)
	DECLARE	@nome_epaga8	AS VARCHAR(60)

	DECLARE @imprime_epaga1 AS BIT
	DECLARE @imprime_epaga2 AS BIT
	DECLARE @imprime_epaga3 AS BIT
	DECLARE @imprime_epaga4 AS BIT
	DECLARE @imprime_epaga5 AS BIT
	DECLARE @imprime_epaga6 AS BIT
	DECLARE @imprime_epaga7 AS BIT
	DECLARE @imprime_epaga8 AS BIT

	SET @nome_epaga1 = ISNULL((SELECT design FROM B_modoPag(NOLOCK) WHERE ref = '01' AND inativo = 0),'')
	SET @nome_epaga7 = ISNULL((SELECT design FROM B_modoPag(NOLOCK) WHERE ref = '02' AND inativo = 0),'')
	SET @nome_epaga8 = ISNULL((SELECT design FROM B_modoPag(NOLOCK) WHERE ref = '04' AND inativo = 0),'')
	SET @nome_epaga2 = ISNULL((SELECT design FROM B_modoPag(NOLOCK) WHERE ref = '03' AND inativo = 0),'')
	SET @nome_epaga3 = ISNULL((SELECT design FROM B_modoPag(NOLOCK) WHERE ref = '05' AND inativo = 0),'')
	SET @nome_epaga4 = ISNULL((SELECT design FROM B_modoPag(NOLOCK) WHERE ref = '06' AND inativo = 0),'')
	SET @nome_epaga5 = ISNULL((SELECT design FROM B_modoPag(NOLOCK) WHERE ref = '07' AND inativo = 0),'')
	SET @nome_epaga6 = ISNULL((SELECT design FROM B_modoPag(NOLOCK) WHERE ref = '08' AND inativo = 0),'')

	SET @imprime_epaga1 = CASE WHEN EXISTS (SELECT design FROM B_modoPag(NOLOCK) WHERE ref = '01' and inativo = 0) 
											THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END
	SET @imprime_epaga2 = CASE WHEN EXISTS (SELECT design FROM B_modoPag(NOLOCK) WHERE ref = '02' and inativo = 0) 
											THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END
	SET @imprime_epaga7 = CASE WHEN EXISTS (SELECT design FROM B_modoPag(NOLOCK) WHERE ref = '03' and inativo = 0) 
											THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END
	SET @imprime_epaga8 = CASE WHEN EXISTS (SELECT design FROM B_modoPag(NOLOCK) WHERE ref = '04' and inativo = 0) 
											THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END
	SET @imprime_epaga3 = CASE WHEN EXISTS (SELECT design FROM B_modoPag(NOLOCK) WHERE ref = '05' and inativo = 0) 
											THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END
	SET @imprime_epaga4 = CASE WHEN EXISTS (SELECT design FROM B_modoPag(NOLOCK) WHERE ref = '06' and inativo = 0) 
											THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END
	SET @imprime_epaga5 = CASE WHEN EXISTS (SELECT design FROM B_modoPag(NOLOCK) WHERE ref = '07' and inativo = 0) 
											THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END
	SET @imprime_epaga6 = CASE WHEN EXISTS (SELECT design FROM B_modoPag(NOLOCK) WHERE ref = '08' and inativo = 0) 
											THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END

	select
		nome_epaga1	= @nome_epaga1
		,nome_epaga2	= @nome_epaga2
		,nome_epaga7	= @nome_epaga7
		,nome_epaga8	= @nome_epaga8
		,nome_epaga3	= @nome_epaga3
		,nome_epaga4	= @nome_epaga4
		,nome_epaga5	= @nome_epaga5
		,nome_epaga6	= @nome_epaga6

		,imprime_epaga1 = @imprime_epaga1
		,imprime_epaga2 = @imprime_epaga2
		,imprime_epaga7 = @imprime_epaga7
		,imprime_epaga8 = @imprime_epaga8
		,imprime_epaga3 = @imprime_epaga3
		,imprime_epaga4 = @imprime_epaga4
		,imprime_epaga5 = @imprime_epaga5
		,imprime_epaga6 = @imprime_epaga6

END

GO
Grant Execute on dbo.up_relatorio_dadosMeiosPagamentos to Public
Grant control on dbo.up_relatorio_dadosMeiosPagamentos to Public
GO