/* Relatório Caixas Operador

	exec up_relatorio_cancelamento_pagamentos '20190319','20190320', 0, 'Loja 1'
	
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_cancelamento_pagamentos]') IS NOT NULL
	drop procedure dbo.up_relatorio_cancelamento_pagamentos
go

create procedure [dbo].up_relatorio_cancelamento_pagamentos
	@dataIni DATETIME
	,@dataFim DATETIME
	,@op as numeric(9,0)
	,@site varchar(30)

/* with encryption */

AS

	select 
		cp.stamp
		, utilizador
		, userno
		, CONVERT(varchar, cp.data, 102) as data
		, CONVERT(VARCHAR(8),cp.data,108) as hora
		, valor
		, nome
		, no
		, estab
		, loja
		, terminal
		, ref
		, design
		, qtt
	from 
		cancel_pagamento cp
	inner join 
		cancel_pagamento_prod on cancel_pagamento_prod.stamp=cp.stamp
	where
		CONVERT(varchar, cp.data, 112) between  CONVERT(varchar, @dataIni, 112) and CONVERT(varchar, @dataFim, 112)
		and cp.userno = case when @op=0 then cp.userno else @op end
		and cp.loja = case when @site='' then cp.loja else @site end
	order by cp.data

GO
Grant Execute On dbo.up_relatorio_cancelamento_pagamentos to Public
Grant control On dbo.up_relatorio_cancelamento_pagamentos to Public
GO