/* Relatório Detalhe Vendas & Relatório Vendas Diarias & Relatório Vendas Familia

	exec up_relatorio_vendas_geral '20210616', '20210616', '', '', '', '', '', '', '', '', 'Loja 1', '', '', 1, '>=', '10', ''
	exec up_relatorio_vendas_geral '20210616', '20210616', '', '', '', '', '', '', '', '', 'Loja 1', '', '', 1, '<=', '10', ''
	exec up_relatorio_vendas_geral '20210616', '20210616', '', '', '', '', '', '', '', '', 'Loja 1', '', '', 1, '>', '23', ''
	exec up_relatorio_vendas_geral '20230101', '20230330', '', '', '', '', '', '', '', '', 'Loja 1', '', '', 1, '<', '11', ''
	exec up_relatorio_vendas_geral '20230101', '20230330', '', '', '', '', '', '', '', '', 'Loja 1', '', '', 0, '>=', '20', '',0,0,'>=',5
*/

if OBJECT_ID('[dbo].[up_relatorio_vendas_geral]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_geral
go

create procedure [dbo].[up_relatorio_vendas_geral]
	 @dataIni			AS datetime
	,@dataFim			AS datetime
	,@horaini			AS varchar(2)=''
	,@horafim			AS varchar(2)=''
	,@op				AS varchar(max)
	,@ref				AS varchar(max)=''
	,@lab				AS varchar(150)=''
	,@marca				AS varchar(200)=''
	,@familia			AS varchar(max)=''
	,@atributosFam		AS varchar(max)=''
	,@site				AS varchar(max)
	,@design			AS varchar(80)=''
	,@tipoDoc			AS varchar(max)=''
	,@descontoTag		AS bit=0
	,@descontoOp		AS varchar(2)=''
	,@desconto			AS varchar(10)=''
	,@generico			AS varchar(18) = ''
	,@incliva			AS bit=0
	,@descontoValorTag	AS bit=0
	,@descontoValorOp	AS varchar(2)=''
	,@descontoValor		AS varchar(10)=''  
/* with encryption */
AS
SET NOCOUNT ON
	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
		DROP TABLE #dadosFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRefs'))
		DROP TABLE #dadosRefs
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
		DROP TABLE #dadosSt	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosTd'))
		DROP TABLE #dadosTd	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseFiltro'))
		DROP TABLE #dadosVendasBaseFiltro	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseQuotaLab'))
		DROP TABLE #dadosVendasBaseQuotaLab	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#Empresasite_nr'))
		DROP TABLE #Empresasite_nr
	
	
	SET @atributosFam = (case when (@atributosFam IS null OR @atributosFam = '') THEN ';;;' else @atributosFam END)

	DECLARE @departamento VARCHAR(254) = ''
	SET @departamento = ISNULL((select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 1),'') 
	DECLARE @sessao       VARCHAR(254) = ''
	set @sessao = ISNULL((select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 2) ,'')
	DECLARE @categoria    VARCHAR(254) = ''
	SET @categoria = ISNULL((select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 3),'') 
	DECLARE @segmento     VARCHAR(254) = ''
	SET @segmento = ISNULL((select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 4),'') 
	
	/* Convereter Valor Generico*/
	IF @generico = 'GENERICO'
		set @generico = 1
	IF @generico = 'NĂO GENERICO'
		set @generico = 0
	/* Calc Loja e Armazens */
	SELECT
		empresa.NO AS [NO]
	INTO #Empresasite_nr
	FROM empresa
		WHERE empresa.site in (Select items from dbo.up_splitToTable(@site, ',')) 
	select 
		armazem
	into
		#EmpresaArm
	From
		empresa
		inner join empresa_arm on empresa.no = empresa_arm.empresa_no
	where
		empresa.site in (Select items from dbo.up_splitToTable(@site, ',')) 
	/* Calc Documentos */
	Select
		ndoc
		,nmdoc
		,tipodoc
		,u_tipodoc
		,site
	into
		#dadosTd
	from
		td
	Where
		convert(varchar(9),td.ndoc) in (Select items from dbo.up_splitToTable(@tipoDoc,','))
		or @tipoDoc = ''
	/* Calc familias */
	Select
		distinct ref, nome
	into
		#dadosFamilia
	From
		stfami (nolock)
	Where
		ref in (Select items from dbo.up_splitToTable(@familia,','))
		Or @familia = '0'
		or @familia = ''
	/* Calc operadores */
	Select
		iniciais, cm = userno, username
	into
		#dadosOperadores
	from
		b_us (nolock)
	Where
		userno in (select items from dbo.up_splitToTable(@op,','))
		or @op= '0'
		or @op= ''
	/* Calc Ref ST */
	Select 
		ref
	into
		#dadosRefs
	from
		st (nolock)
	Where
		ref in (select items from dbo.up_splitToTable(@ref,',')) 
		OR @ref= ''
		AND site_nr IN  (SELECT NO FROM #Empresasite_nr)
	/* Calc Informaçăo Produto */
	Select
		st.ref
		,st.design
		,usr1
		,u_lab	
		, ISNULL(A.descr,'') as departamento
		, ISNULL(B.descr,'') as seccao
		, ISNULL(C.descr,'') as categoria
		, ISNULL(D.descr,'') as segmento
		,dispdescr = ISNULL(dispdescr,'')
		,generico = ISNULL(generico,CONVERT(bit,0))
		,psico = ISNULL(psico,CONVERT(bit,0))
		,benzo = ISNULL(benzo,CONVERT(bit,0))
		,protocolo = ISNULL(protocolo,CONVERT(bit,0))
		,dci = ISNULL(dci,'')
		,grphmgcode = ISNULL(grphmgcode,'')
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 1) valDepartamento
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 2) valSeccao
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 3) valCategoria
		,(select items from dbo.up_SplitToTableCId(@atributosFam,';') where id = 4) valSegmento
		,EMPRESA.site		as site
	INTO
		#dadosSt
	From
		st (nolock)
		INNER JOIN EMPRESA ON st.site_nr = EMPRESA.no
		left join fprod (nolock) on st.ref = fprod.cnp	
		left join grande_mercado_hmr (nolock) A on A.id = st.u_depstamp
		left join mercado_hmr (nolock) B on B.id = st.u_secstamp
		left join categoria_hmr (nolock) C on C.id = st.u_catstamp 
		left join segmento_hmr (nolock) D on D.id = st.u_segstamp	
	Where
		st.u_lab = case when @lab = '' then u_lab else @lab end
		And st.usr1 = case when @marca = '' then usr1 else @marca end
		AND site_nr IN  (SELECT NO FROM #Empresasite_nr)
	/* Define se vais mostrar resultados abaixo do Cliente 199 */
	declare @Entidades as bit
	if ((select count(tipoempresa) from empresa where site in (Select items from dbo.up_splitToTable(@site, ',')) and tipoempresa not in ('FARMACIA', 'PARAFARMACIA'))>=1)
	BEGIN
		SET @Entidades = 0		
	END 
	ELSE
	BEGIN 
		SET @Entidades = 1
	END

	 
	/* Dados Base Vendas  */
	create table #dadosVendasBase (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AI
		,loja_nr  numeric(5,0)
	)
    insert #dadosVendasBase
    exec up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site


	/* Elimina Vendas tendo em conta tipo de cliente*/
	If @Entidades = 1
	delete from #dadosVendasBase where #dadosVendasBase.no < 199

	/* Elimina Vendas tendo em conta tipo de cliente*/
	If @Entidades = 1
	delete from #dadosVendasBase where #dadosVendasBase.no < 199

declare @sql varchar(max)
declare @sqlWhere varchar(max)=''


select @sql = N'  

	Select
		[nrAtend]
		,[ftstamp]
		,[fdata]
		,[no]
		,#dadosVendasBase.[ndoc]
		,#dadosTd.[nmdoc]
		,[fno]
		,#dadosVendasBase.[tipodoc]
		,#dadosVendasBase.[u_tipodoc]
		,#dadosVendasBase.[ref]
		,#dadosVendasBase.[design]
		,#dadosVendasBase.[familia]
		,#dadosfamilia.[nome] as faminome
		,[u_epvp]
		,[epcpond]
		,[iva]
		,[qtt]
		,[etiliquido]
		,[etiliquidoSiva]
		,[ettent1]
		,[ettent2]
		,[ettent1Siva]
		,[ettent2Siva]
		,[desconto]
		,descvalor=CASE WHEN #dadosVendasBase.[tipodoc] != 3 THEN ABS(#dadosVendasBase.[descvalor]) ELSE ABS([descvalor])*-1 END
		,[descvale]
		,[ousrhora]
		,[ousrinis]
		,[vendnm]
		,[u_ltstamp]
		,[u_ltstamp2]
		,[usr1]
		,[u_lab]
		,[departamento]
		,[seccao]
		,[categoria]
		,segmento
		,[dispdescr]
		,[generico]
		,[psico]
		,[benzo]
		,[protocolo]
		,[dci]
		,[valDepartamento]
		,[valSeccao]
		,[valCategoria]
		,[valSegmento]
		,  [desconto]																																													AS descontPerc
		, descvalor  																																													AS descontvalor
		, CASE WHEN [desconto]!=0.00 THEN 0 when descvalor !=0.00 then 1   ELSE 2 END	AS descT
		, case when [etiliquidoSiva] +[ettent1Siva] + [ettent2Siva] !=0 then ((([etiliquidoSiva] +[ettent1Siva] + [ettent2Siva] - epcpond)/([etiliquidoSiva] +[ettent1Siva] + [ettent2Siva])) * (case when #dadosVendasBase.ndoc IN (76,77) then -1 else 1 end))*100   else 0 end mbpv
	into
		#dadosVendasBaseFiltro
	From
		#dadosVendasBase 
		inner join #dadosTd on #dadosTd.ndoc = #dadosVendasBase.ndoc
		left Join #dadosSt on #dadosVendasBase.ref = #dadosSt.ref and  #dadosVendasBase.loja = #dadosST.site 
		left join #dadosFamilia on #dadosVendasBase.familia = #dadosfamilia.ref
	WHERE
		#dadosVendasBase.tipo != ''ENTIDADE''
		AND #dadosVendasBase.u_tipodoc not in (1, 5) 
				and left(#dadosVendasBase.ousrhora,2) between (case when '+ isnull((case when @horaini ='' then '0' else @horaini end),'') +'='''' then ''0'' else ''' + @horaini + ''' end)
				and (case when '+ isnull((case when @horafim ='' then '0' else @horafim end),'')+' ='''' then ''24'' else  ''' +@horafim + '''end)'
			If @lab != ''
			BEGIN
				set @sqlWhere += ' and isnull(#dadosSt.u_lab,'''') = ''' + @lab +''''
			END


			If @marca != ''
			BEGIN
				set @sqlWhere += ' and isnull(#dadosSt.usr1,'''') = ''' + @marca +'''' 
			END


			If ((select count(iniciais) from #dadosOperadores) >0)
			BEGIN
				set @sqlWhere += ' and #dadosVendasBase.ousrinis in (select iniciais from #dadosOperadores) '
			END
			If ((select count(ref) from #dadosRefs) >0)
			--If (@ref !='')
			BEGIN
				set @sqlWhere += ' and #dadosVendasBase.ref in (Select ref from #dadosRefs) '
			END
			If ((select count(ref) from #dadosFamilia) >0)
			--If (@familia !='')
			BEGIN
				set @sqlWhere += ' and isnull(#dadosVendasBase.familia,''99'') in (Select ref from #dadosFamilia) '
			END

			If (@design !='')
			BEGIN
				set @sql += ' and #dadosVendasBase.design like ''' +convert(varchar,@design)+''' + ''%''  '
			END
			If (@generico !='')
			BEGIN
				set @sqlWhere += ' and #dadosSt.generico =  ' + @generico + '' 
			END	

			If (@departamento !='')
			BEGIN
				set @sqlWhere += ' and departamento =''' + @departamento +'''' 
			END	

			If (@sessao !='')
			BEGIN
				set @sqlWhere += ' and seccao =''' + @sessao +''''
			END	

			If (@categoria !='')
			BEGIN
				set @sqlWhere += ' and categoria =''' + @categoria +'''' 
			END	

			If (@segmento !='')
			BEGIN
				set @sqlWhere += ' and segmento =''' + @segmento +''''
			END	


declare @sql1 varchar(max)
select @sql1 = N'  
		select 
			* 
		from 
			#dadosVendasBaseFiltro'
		if(@descontoTag =1)
		BEGIN
			set @sql1 += ' where  (descontPerc ' + @descontoOp + ' ' + @desconto + 'and 
				(CASE WHEN [desconto]!=0.00 THEN 0 when descvalor !=0.00 then 1   ELSE 2 END)=0 )'
		END  
			if(@descontoValorTag =1 AND @descontoTag =0)
		BEGIN
			set @sql1 += ' where  (descvalor ' + @descontoValorOp + ' ' + @descontoValor + ' and
			 (CASE WHEN [desconto]!=0.00 THEN 0 when descvalor !=0.00 then 1   ELSE 2 END)=1)'
		END 
		ELSE IF (@descontoValorTag =1 AND @descontoTag =1)
		BEGIN
			set @sql1 += ' or ( descvalor ' + @descontoValorOp + ' ' + @descontoValor + ' and 
			(CASE WHEN [desconto]!=0.00 THEN 0 when descvalor !=0.00 then 1   ELSE 2 END)=1)'
		END


	print @sql + @sqlWhere + @sql1
	execute (@sql + @sqlWhere +@sql1)		
	--select * from #dadosVendasBaseFiltro
	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
		DROP TABLE #dadosFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRefs'))
		DROP TABLE #dadosRefs
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
		DROP TABLE #dadosSt
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosTd'))
		DROP TABLE #dadosTd
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.##dadosVendasBaseFiltro'))
		DROP TABLE #dadosVendasBaseFiltro	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseQuotaLab'))
		DROP TABLE #dadosVendasBaseQuotaLab	

GO
Grant Execute on dbo.up_relatorio_vendas_geral to Public
Grant control on dbo.up_relatorio_vendas_geral to Public
GO