-- Relatório Conferencia RecSemEnc
-- exec up_relatorio_conferencia_RecSemEnc '20100101','20130101', ''

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_conferencia_RecSemEnc]') IS NOT NULL
	drop procedure dbo.up_relatorio_conferencia_RecSemEnc
go

create procedure dbo.up_relatorio_conferencia_RecSemEnc
@dataIni DATETIME,
@dataFim DATETIME,
@site	 varchar(55)
/* with encryption */
AS 
BEGIN

	SELECT	
		empresa.empresa as EMPRESA,
		empresa.site as LOJA,
		empresa.ref as NUMLOJA,
		FO.DATA as DATARECEPCAO,
		FO.ADOC as NUMRECEPCAO,
		CASE WHEN FN.ref='' THEN fn.OREF ELSE fn.ref END as REFERENCIA,
		U_UPC as UNIT_COST_RECEPCAO,
		QTT as QTD_RECEPCIONADA
	FROM	
		FN (nolock) 
		LEFT JOIN FO (nolock) ON FN.fostamp = FO.FOSTAMP
		left join empresa_arm on empresa_arm.armazem = fn.armazem
		left join empresa on empresa.no = empresa_arm.empresa_no
	WHERE	
		FO.doccode  = 102 
		AND fn.bistamp = NULL 
		AND fn.ref <> '' 
		AND FO.DATA >= CAST(@dataIni  as DATETIME) 
		AND FO.DATA <= CAST(@dataFim as DATETIME)
		
END

GO
Grant Execute On dbo.up_relatorio_conferencia_RecSemEnc to Public
GO
Grant control On dbo.up_relatorio_conferencia_RecSemEnc to Public
GO