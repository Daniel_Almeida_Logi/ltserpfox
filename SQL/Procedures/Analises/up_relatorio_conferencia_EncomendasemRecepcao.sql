-- Relatório Encomendas Em Recepção

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_relatorio_conferencia_EncomendasemRecepcao]') IS NOT NULL
	DROP procedure dbo.up_relatorio_conferencia_EncomendasemRecepcao
GO

CREATE procedure dbo.up_relatorio_conferencia_EncomendasEmRecepcao
@dataIni AS  DATETIME,
@dataFim AS  DATETIME,
@ref AS varchar(50),
@nome AS varchar(254),
@estadodoc as varchar(20),
@site varchar(60),
@enviada int
/* WITH ENCRYPTION */ 
AS
BEGIN
Select 
	ref
	,design	
	,qttEnc = qtt
	,qttMov = qtt2
	,qttFalta = qtt-qtt2
	,bo.nome
	,bo.no
	,bo.estab
	,bo.nmdos
	,bo.obrano
	,bo.dataobra
	,estado = case when bo.fechada=0 then 'Aberta' else 'Fechada' end
	,enviada = logi1  
From	
	Bo (nolock)
	inner join bi (nolock) on bi.bostamp = bo.bostamp
Where	
	Bo.nmdos = 'ENCOMENDA A FORNECEDOR'
	and qtt-qtt2 > 0
	and bo.dataobra between @dataIni and @dataFim
	and ref = CASE WHEN @REF= '' THEN REF ELSE @REF END
	and bo.nome = CASE WHEN @nome= '' THEN bo.nome ELSE @nome END
	and bo.fechada = case when @estadodoc = 'Fechada' then 1 Else 0 end
	and bo.site = (case when @site = '' Then bo.site else @site end)
	and logi1 = case when @enviada = 1 then 1 
				when @enviada = 2 then 0 
			else logi1 end
Order By
	ref asc
END

GO
GRANT EXECUTE on dbo.up_relatorio_conferencia_EncomendasemRecepcao TO PUBLIC
GRANT CONTROL on dbo.up_relatorio_conferencia_EncomendasemRecepcao TO PUBLIC
GO