/* Relatório Detalhe relatorio produtos desafio

	exec up_relatorio_produtos_generis '20170101', '20171130', 'Loja 1'

	exec up_relatorio_vendas_base_detalhe '20151208', '20151208', 'Loja 1'
	
*/

if OBJECT_ID('[dbo].[up_relatorio_produtos_generis]') IS NOT NULL
	drop procedure dbo.up_relatorio_produtos_generis
go

create procedure [dbo].[up_relatorio_produtos_generis]
	@dataIni		datetime
	,@dataFim		datetime
	,@site			varchar(55)

/* with encryption */

AS
SET NOCOUNT ON

	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
		DROP TABLE #dadosSt	
	
	/* Calc Loja e Armazens */
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)

	declare @codigofarm as numeric(10,0)
	set @codigofarm = ISNULL((select infarmed from empresa where site = @site),0)

	/* Calc Informação Produto */
	Select
		st.ref
		, st.codidt
	INTO
		#dadosSt
	From
		st (nolock)
	Where
		st.generis=1
		AND site_nr = @site_nr

	/* Dados Base Vendas  */
	create table #dadosVendasBase (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)
	)
    
    insert #dadosVendasBase
    exec up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site
	
	delete from #dadosVendasBase where #dadosVendasBase.no < 199

	/* Preparar Result Set */
	Select
		'"'+cast(@codigofarm as varchar(10))+'"' as COD_FARMACIA
		,'"'+replace(replace(replace(CONVERT(varchar, (DATEADD(month, DATEDIFF(month, 0, @dataIni), 0)), 120),'-',''),':',''),' ','')+'"' as COD_CAMPANHA
		,'"'+CONVERT(varchar, @dataIni, 112)+'"' as INI_PERIODO
		,'"'+CONVERT(varchar, @dataFim, 112)+'"' as FIM_PERIODO
		,'"'+replace(left(CONVERT(varchar, #dadosVendasBase.fdata, 120),10),'-','')+replace(#dadosVendasBase.ousrhora,':','')+'"' as DT_HR_MOV
		,'"'+cast(#dadosSt.codidt as varchar(5))+'"' as COD_EMP
		,'"'+#dadosVendasBase.ref+'"' as CODIGO
		,'"'+#dadosVendasBase.design+'"' as DESIGNACAO
		,'"'+cast(cast(#dadosVendasBase.qtt as numeric(3,0)) as varchar(3))+'"' as QT_TOT
		,'"'+cast(cast(#dadosVendasBase.qtt  as numeric(3,0)) as varchar(3))+'"' as QT_CAMP
		,'"'+replace(cast(cast(#dadosVendasBase.etiliquido + #dadosVendasBase.ettent1 + #dadosVendasBase.ettent2 as numeric(10,2)) as varchar(10)),'.','')+'"' as VPVP
	From
		#dadosVendasBase 
		right join #dadosSt on #dadosSt.ref=#dadosVendasBase.ref
	WHERE
		#dadosVendasBase.tipo != 'ENTIDADE'
		AND #dadosVendasBase.u_tipodoc not in (1, 5)
		--and #dadosVendasBase.ref in (select distinct ref from #dadosSt)

	--exec up_relatorio_produtos_generis '20170101', '20170130', 'Loja 1'

	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
		DROP TABLE #dadosSt

GO
Grant Execute on dbo.up_relatorio_produtos_generis to Public
Grant control on dbo.up_relatorio_produtos_generis to Public
GO