/* Relatório Detalhe Relatório de vales descontados

	exec up_relatorio_vales_descontados '20240104', '20240504', '', 'Loja 1', 0, 'TODAS'
	exec up_relatorio_vales_descontados '20220104', '20220504', '56', 'Loja 3', 0, 'TODAS'
	exec up_relatorio_vales_descontados '20240104', '20240504', '', 'Loja 2', 0, 'TODAS'
	exec up_relatorio_vales_descontados '20240104', '20240504', '', 'Loja 1, Loja 2', 0, 'TODAS'
	13
*/

if OBJECT_ID('[dbo].[up_relatorio_vales_descontados]') IS NOT NULL
	drop procedure up_relatorio_vales_descontados
go

create procedure [dbo].[up_relatorio_vales_descontados]
	@dataIni		datetime
	,@dataFim		datetime
	,@op			varchar(max)=''
	,@site			varchar(254)
	,@incliva		as bit=0
	,@ref			varchar(55)=''
/* with encryption */
AS
SET NOCOUNT ON
	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseFiltro'))
		DROP TABLE #dadosVendasBaseFiltro	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosftstamp'))
		DROP TABLE #dadosftstamp
			

	/* Vendas com vales */
	SELECT 
		DISTINCT ftstamp
	INTO 
		#dadosftstamp 
	FROM 
		fi (NOLOCK) 
	WHERE 
		((@ref='TODAS' AND REF IN (SELECT DISTINCT ref FROM st (NOLOCK) WHERE qlook=1))
		OR (@ref<>'TODAS' AND REF IN (SELECT items FROM dbo.up_splitToTable(@ref,','))))
		AND rdata BETWEEN @dataIni AND @dataFim

	/* Calc operadores */
	SELECT
		iniciais, cm = userno, username
	INTO
		#dadosOperadores
	FROM
		b_us (NOLOCK)
	WHERE
		userno IN (SELECT items FROM dbo.up_splitToTable(@op,','))
		OR @op= '0'
		OR @op= ''

	/* Preparar Result Set */
	SELECT 
		ft.fdata
		, ft.nmdoc
		, ft.fno
		, fi.ref
		, fi.design
		, ft.ousrinis
		, CAST((CASE WHEN st.qlook=1	THEN fi.qtt			ELSE 0 END) AS NUMERIC(10,0))			AS qtt
		, CAST((CASE WHEN fi.u_epvp<0	THEN fi.u_epvp*(-1) ELSE fi.u_epvp END) AS NUMERIC(10,2))	AS u_epvp
		, CAST(ft.ettiliq AS NUMERIC(10,2)) AS ettiliq
		, CAST(ft.etotal  AS NUMERIC(10,2)) AS etotal
		, ft.site AS site
		,etiliquido		= (SELECT SUM(CASE WHEN fii.ivaincl = 1 THEN fii.etiliquido 
								ELSE fii.etiliquido + (fii.etiliquido*(fii.iva/100)) END) 
								FROM fi fii WHERE fii.ftstamp=fi.ftstamp AND fii.ref<>fi.ref)
		,etiliquidoSIva = (SELECT SUM(CASE WHEN fii.ivaincl = 1 THEN (fii.etiliquido / (fii.iva/100+1)) 
								ELSE fii.etiliquido END)  
								FROM fi fii WHERE fii.ftstamp=fi.ftstamp AND fii.ref<>fi.ref)
		,ettent1		= (SELECT SUM(CASE WHEN ftt.tipodoc = 3 THEN fii.u_ettent1*-1 
								ELSE fii.u_ettent1 end)  
								FROM fi fii INNER JOIN ft ftt ON fii.ftstamp=ftt.ftstamp WHERE fii.ftstamp=fi.ftstamp AND fii.ref<>fi.ref)
		,ettent2		= (SELECT SUM(CASE WHEN ftt.tipodoc = 3 THEN fii.u_ettent2*-1 
								ELSE fii.u_ettent2 END)  
								FROM fi fii INNER JOIN ft ftt ON fii.ftstamp=ftt.ftstamp WHERE fii.ftstamp=fi.ftstamp AND fii.ref<>fi.ref)
		,ettent1SIva	= (SELECT SUM(CASE WHEN ftt.tipodoc = 3 THEN (fii.u_ettent1/(fii.iva/100+1))*-1 
								ELSE (fii.u_ettent1/(fii.iva/100+1)) end)  
								FROM fi fii INNER JOIN ft ftt ON fii.ftstamp=ftt.ftstamp WHERE fii.ftstamp=fi.ftstamp AND fii.ref<>fi.ref)
		,ettent2SIva	= (SELECT SUM(CASE WHEN ftt.tipodoc = 3 THEN (fii.u_ettent2/(fii.iva/100+1))*-1 
								ELSE (fii.u_ettent2/(fii.iva/100+1)) END) 
								FROM fi fii INNER JOIN ft ftt ON fii.ftstamp=ftt.ftstamp WHERE fii.ftstamp=fi.ftstamp AND fii.ref<>fi.ref)
	INTO
		#dadosVendasBaseFiltro
	FROM fi 
		INNER JOIN ft ON fi.ftstamp=ft.ftstamp
		INNER JOIN st ON fi.ref=st.ref 
				AND  st.site_nr IN (SELECT  no FROM empresa(NOLOCK) WHERE site IN (SELECT * FROM up_SplitToTable(@site,',')))
	WHERE 
		st.qlook=1  
		AND fi.ftstamp IN (SELECT ftstamp FROM #dadosftstamp)
		AND ft.ousrinis IN (SELECT iniciais FROM #dadosOperadores)
		--AND ft.site IN  (Select * from dbo.up_splitToTable(@site, ',')) 
		AND st.site_nr IN (SELECT  no FROM empresa(NOLOCK) WHERE site IN (SELECT * FROM up_SplitToTable(@site,',')))
		AND ft.ousrinis IN (SELECT iniciais FROM #dadosOperadores)
		AND ((@ref='TODAS' AND fi.REF IN (SELECT DISTINCT ref FROM st (NOLOCK) WHERE qlook=1))
			OR (@ref<>'TODAS' AND fi.REF IN (SELECT items FROM dbo.up_splitToTable(@ref,','))))
	ORDER BY 
		ft.fdata, ft.nmdoc, ft.fno

	SELECT * FROM #dadosVendasBaseFiltro

	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.##dadosVendasBaseFiltro'))
		DROP TABLE #dadosVendasBaseFiltro	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosftstamp'))
		DROP TABLE #dadosftstamp	

GO
Grant Execute on dbo.up_relatorio_vales_descontados to Public
Grant control on dbo.up_relatorio_vales_descontados to Public
GO