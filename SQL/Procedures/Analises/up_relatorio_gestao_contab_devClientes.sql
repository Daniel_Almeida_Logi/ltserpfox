-- Relatório Conatbilidade Dev Clientes
-- exec up_relatorio_gestao_contab_devClientes '20100101','20130101',''
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_contab_devClientes]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_contab_devClientes
go

create procedure dbo.up_relatorio_gestao_contab_devClientes
@dataIni as datetime,
@dataFim as datetime,
@site varchar(60)
/* with encryption */
AS
SET NOCOUNT ON
	;with 
		cteFix as (
		SElect 
			td.ndoc
			,ofistamp
			,fistamp
			,qtt
			,fi.fno
		From 
			fi	(nolock)
			inner join td on fi.ndoc = td.ndoc	
			and td.u_tipodoc != 3 
	)
	SELECT	
			EMPRESA		= empresa.EMPRESA
			,LOJA		= empresa.site
			,NUMLOJA	= empresa.ref
			,DATADOC	= FDATA
			,NOMECLI	= (CASE WHEN FT.NO=200 THEN 'CLIENTE FINAL' ELSE FT.NOME END)
			,NUMCLI		= FT.NO
			,DOCUMENTO	= FT.NMDOC
			,NUMDOC		= FT.FNO
			,FI.REF
			,DESIGN
			,QTT = (fi.qtt-isnull(fix.qtt,0))
			,UNIDADE
			,fi.TABIVA
			,IVA
			,DESCONTO
			,DESC2
			,VALOR = (ABS(fi.ETILIQUIDO)/fi.qtt)*(fi.qtt-isnull(fix.qtt,0))
			,DocOrigem = (select top 1 fno from fi(nolock) forig where fi.ofistamp= forig.fistamp)
	FROM	
		FT (nolock) 
		INNER JOIN FI (nolock) ON FI.FTSTAMP = FT.FTSTAMP
		INNER JOIN TD (nolock) ON FT.NDOC = TD.NDOC
		Left join empresa_arm (nolock) on empresa_arm.armazem = FI.armazem
		Left join empresa (nolock) on empresa_arm.empresa_no = empresa.no
		left join cteFix fix (nolock) on fi.fistamp = fix.ofistamp 
	WHERE	
		U_TIPODOC = 2 
		AND (fi.ref != '' OR fi.qtt != 0 OR etiliquido !=0)
		and (fi.qtt-isnull(fix.qtt,0)) != 0
		AND FDATA BETWEEN @dataIni AND @dataFim
		and ft.site = (case when @site = '' Then ft.site else @site end)
	ORDER BY 
		FDATA

GO
Grant Execute on dbo.up_relatorio_gestao_contab_devClientes to Public
GO
Grant Control on dbo.up_relatorio_gestao_contab_devClientes to Public
Go
