/*
	exec up_relatorio_compras_resumoIva '20220101', '20220731', '55,48', 'Loja 1', 0

	select * from fo where adoc = 'RO215/01451'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_compras_resumoIva]') IS NOT NULL
	drop procedure dbo.up_relatorio_compras_resumoIva
go

create procedure dbo.up_relatorio_compras_resumoIva
@dataIni as date,
@dataFim as date,
@documentos as varchar(max),
@site varchar(55),
@soEntidades as bit = 0
/* WITH ENCRYPTION select no, nome from fo order by no*/ 
AS
BEGIN
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosComprasSDescFin'))
		DROP TABLE #dadosComprasSDescFin
	



		(select	
			year(fo.docdata) as Ano
			,month(fo.docdata) as Mes 
			,fo.docnome as Documento
			,fo.nome as nome
			,fo.no as no
			,fn.iva  as iva
			,sum( case when cm1.debito=0 and fn.ivaincl=0	then  ROUND((FN.ETILIQUIDO*(FN.iva/100)),2)
				 when cm1.debito=1 and fn.ivaincl=0			then -ROUND((FN.ETILIQUIDO*(FN.iva/100)),2)
				 when cm1.debito=0 and fn.ivaincl=1			then  ROUND((fn.etiliquido - (FN.ETILIQUIDO/(FN.iva/100+1))),2)
				 when cm1.debito=1 and fn.ivaincl=1			then -ROUND((fn.etiliquido - (FN.ETILIQUIDO/(FN.iva/100+1))),2)
				 else 0 end
			) as VALORIVA
			,sum( case when cm1.debito=0 and fn.ivaincl=0	then  ROUND(FN.ETILIQUIDO,2)
				 when cm1.debito=1 and fn.ivaincl=0			then -ROUND(FN.ETILIQUIDO,2)
				 when cm1.debito=0 and fn.ivaincl=1			then  ROUND((FN.ETILIQUIDO/(FN.iva/100+1)),2)
				 when cm1.debito=1 and fn.ivaincl=1			then -ROUND((FN.ETILIQUIDO/(FN.iva/100+1)),2)
				 else 0 end
			) as BASEINC
			,fn.stns as stns
		from 
			fn (nolock) 
			inner join fo (nolock)  on fo.fostamp=fn.fostamp
			left join cm1 (nolock) on cm1.cm=fo.doccode
		where	
			fo.docdata between @dataIni and @dataFim
			and fo.doccode != 101
			AND convert(varchar,cm1.cm) in (select items from dbo.up_splitToTable(@documentos,','))
			and fo.site = (case when @site = '' Then fo.site else @site end)
			and fo.no < (case when @soEntidades = 1 then 199 else 999999 end)
		group by 
			year(fo.docdata), month(fo.docdata), fo.doccode, fo.docnome,fo.nome,fo.no,fn.iva, fn.stns)

	UNION all

		(select	
			year(bo.dataobra)		as Ano
			,month(bo.dataobra)		as Mes
			,sl.cmdesc				as Documento
			,sl.nome				as nome
			,bo.no					as no
			,bi.iva					as iva 
			,sum( case when sl.cm<50 and bi.ivaincl=0		then  ROUND((bi.ettdeb*(bi.iva/100)),2)
				 when sl.cm>50 and bi.ivaincl=0				then -ROUND((bi.ettdeb*(bi.iva/100)),2)
				 when sl.cm<50 and bi.ivaincl=1				then  ROUND((bi.ettdeb - (bi.ettdeb/(bi.iva/100+1))),2)
				 when sl.cm>50 and bi.ivaincl=1				then -ROUND((bi.ettdeb - (bi.ettdeb/(bi.iva/100+1))),2)
				 else 0 end
			)as VALORIVA
			,sum( case when sl.cm<50 and bi.ivaincl=0		then  ROUND(bi.ettdeb,2)
				 when sl.cm>50 and bi.ivaincl=0				then -ROUND(bi.ettdeb,2)
				 when sl.cm<50 and bi.ivaincl=1				then  ROUND((bi.ettdeb/(bi.iva/100+1)),2)
				 when sl.cm>50 and bi.ivaincl=1				then -ROUND((bi.ettdeb/(bi.iva/100+1)),2)
				 else 0 end
			)as BASEINC
			,bi.stns				as stns
		from 
			sl(nolock)
			inner join bi(nolock) on sl.bistamp = bi.bistamp 
			inner join bo(nolock) on bo.bostamp = bi.bostamp
		where	
			bo.dataobra between @dataIni and @dataFim
			AND (convert(varchar,sl.cm) in (select items from dbo.up_splitToTable(@documentos,',') where items in (48,98))) -- pedido para contar apenas as entradas e saidas de trf
			AND bo.site = (case when @site = '' Then bo.site else @site end)
			AND bo.no < (case when @soEntidades = 1 then 199 else 999999 end)
		group by 
			year(bo.dataobra), month(bo.dataobra), sl.cmdesc,sl.nome,bo.no,bi.iva, bi.stns)


/*
	select
		Ano
		,Mes
		,Documento
		,nome
		,no
		,iva
		,VALORIVA /*= (valorIva - (valorIva* (DESCFIN/100)))*/
		,BASEINC /*= (baseinc - (baseinc * (DESCFIN/100)))*/
		,stns
	From
		#dadosComprasSDescFin*/

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosComprasSDescFin'))
		DROP TABLE #dadosComprasSDescFin
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosDescFin'))
		DROP TABLE #dadosDescFin

END

GO
Grant Execute on dbo.up_relatorio_compras_resumoIva to Public
Grant control on dbo.up_relatorio_compras_resumoIva to Public
Go


	