/* Relatório Detalhe Vendas & Relatório Vendas Diarias & Relatório Vendas Familia

	exec up_relatorio_vendas_produto '20171001', '20171020', '', '', '', '5440987', 'Loja 1', 0

	exec up_relatorio_vendas_base_detalhe '20171001', '20171020', 'Loja 1' 
	
*/

if OBJECT_ID('[dbo].[up_relatorio_vendas_produto]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_produto
go

create procedure [dbo].[up_relatorio_vendas_produto]
	@dataIni		datetime
	,@dataFim		datetime
	,@horaini		varchar(2)=''
	,@horafim		varchar(2)=''
	,@op			varchar(max)=''
	,@ref			varchar(max)=''
	,@site			varchar(55)
	,@incliva		as bit=0

/* with encryption */

AS
SET NOCOUNT ON

	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
		DROP TABLE #dadosFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRefs'))
		DROP TABLE #dadosRefs
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
		DROP TABLE #dadosSt	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosTd'))
		DROP TABLE #dadosTd	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseFiltro'))
		DROP TABLE #dadosVendasBaseFiltro	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseQuotaLab'))
		DROP TABLE #dadosVendasBaseQuotaLab	

	/* Calc Loja e Armazens */
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)

	select 
		armazem
	into
		#EmpresaArm
	From
		empresa
		inner join empresa_arm on empresa.no = empresa_arm.empresa_no
	where
		empresa.site = case when @site = '' then empresa.site else @site end	

	/* Calc Documentos */
	Select
		ndoc
		,nmdoc
		,tipodoc
		,u_tipodoc
	into
		#dadosTd
	from
		td

	/* Calc familias */
	Select
		distinct ref, nome
	into
		#dadosFamilia
	From
		stfami (nolock)

	/* Calc operadores */
	Select
		iniciais, cm = userno, username
	into
		#dadosOperadores
	from
		b_us (nolock)
	Where
		userno in (select items from dbo.up_splitToTable(@op,','))
		or @op= '0'
		or @op= ''

	/* Calc Ref ST */
	Select 
		ref
	into
		#dadosRefs
	from
		st (nolock)
	Where
		ref in (select items from dbo.up_splitToTable(@ref,',')) 
		OR @ref= ''
		AND site_nr = @site_nr
	
	/* Calc Informação Produto */
	Select
		st.ref
		,st.design
		,usr1
		,u_lab
		,departamento = isnull(b_famDepartamentos.design ,'')
		,seccao = isnull(b_famSeccoes.design,'')
		,categoria = isnull(b_famCategorias.design,'')
		,famfamilia =  isnull(b_famFamilias.design,'')
		,dispdescr = ISNULL(dispdescr,'')
		,generico = ISNULL(generico,CONVERT(bit,0))
		,psico = ISNULL(psico,CONVERT(bit,0))
		,benzo = ISNULL(benzo,CONVERT(bit,0))
		,protocolo = ISNULL(protocolo,CONVERT(bit,0))
		,dci = ISNULL(dci,'')
		,grphmgcode = ISNULL(grphmgcode,'')
		,(select items from dbo.up_SplitToTableCId('',';') where id = 1) valDepartamento
		,(select items from dbo.up_SplitToTableCId('',';') where id = 2) valSeccao
		,(select items from dbo.up_SplitToTableCId('',';') where id = 3) valCategoria
		,(select items from dbo.up_SplitToTableCId('',';') where id = 4) valFamfamilia
	INTO
		#dadosSt
	From
		st (nolock)
		left join fprod (nolock) on st.ref = fprod.cnp
		left join b_famDepartamentos (nolock) on st.u_depstamp = b_famDepartamentos.depstamp
		left join b_famSeccoes (nolock) on st.u_secstamp = b_famSeccoes.secstamp
		left join b_famCategorias (nolock) on st.u_catstamp = b_famCategorias.catstamp
		left join b_famFamilias (nolock) on st.u_famstamp = b_famFamilias.famstamp
	Where
		site_nr = @site_nr

	/* Define se vais mostrar resultados abaixo do Cliente 199 */
	declare @Entidades as bit
	set @Entidades = case when (select tipoempresa from empresa where site = @site) in ('FARMACIA', 'PARAFARMACIA') then convert(bit,1) else convert(bit,0) end

	/* Dados Base Vendas  */
	create table #dadosVendasBase (
		nrAtend varchar(20)
		,ftstamp varchar(25)
		,fdata datetime
		,[no] numeric(10,0)
		,estab numeric(5,0)
		,tipo varchar(25)
		,ndoc numeric(6,0)
		,fno numeric(10,0)
		,tipodoc numeric(2)
		,u_tipodoc numeric(2)
        ,ref varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,design varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AI
		,familia varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,u_epvp numeric(15,3)
        ,epcpond numeric(19,6)
        ,iva numeric(5,2)
        ,qtt numeric(11,3)
        ,etiliquido numeric(19,6)
        ,etiliquidoSiva numeric(19,6)
        ,ettent1 numeric(13,3)
        ,ettent2 numeric(13,3)
        ,ettent1siva numeric(13,3)
        ,ettent2siva numeric(13,3)
        ,desconto numeric(6,2)
        ,descvalor numeric(19,6)
        ,descvale numeric(19,6)
        ,ousrhora varchar(8)
        ,ousrinis varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AI
        ,vendnm varchar(20)
        ,u_ltstamp varchar(25)
        ,u_ltstamp2 varchar(25)
        ,ecusto numeric(19,6)
		,loja  varchar(50)
		,loja_nr  numeric(5,0)
	)
    
    insert #dadosVendasBase
    exec up_relatorio_vendas_base_detalhe @dataIni, @dataFim, @site

	/* Elimina Vendas tendo em conta tipo de cliente*/
	If @Entidades = 1
	delete from #dadosVendasBase where #dadosVendasBase.no < 199



	/* Preparar Result Set */
	Select
		[nrAtend]
		,[ftstamp]
		,[fdata]
		,[no]
		,#dadosVendasBase.[ndoc]
		,#dadosTd.[nmdoc]
		,[fno]
		,#dadosVendasBase.[tipodoc]
		,#dadosVendasBase.[u_tipodoc]
		,#dadosVendasBase.[ref]
		,#dadosVendasBase.[design]
		,#dadosVendasBase.[familia]
		,#dadosfamilia.[nome] as faminome
		,[u_epvp]
		,[epcpond]
		,[iva]
		,[qtt]
		,[etiliquido]
		,[etiliquidoSiva]
		,[ettent1]
		,[ettent2]
		,[ettent1Siva]
		,[ettent2Siva]
		,[desconto]
		,[descvalor]
		,[descvale]
		,[ousrhora]
		,[ousrinis]
		,[vendnm]
		,[u_ltstamp]
		,[u_ltstamp2]
		,[usr1]
		,[u_lab]
		,[departamento]
		,[seccao]
		,[categoria]
		,[famfamilia]
		,[dispdescr]
		,[generico]
		,[psico]
		,[benzo]
		,[protocolo]
		,[dci]
		,[valDepartamento]
		,[valSeccao]
		,[valCategoria]
		,[valFamfamilia]
	into
		#dadosVendasBaseFiltro
	From
		#dadosVendasBase 
		inner join #dadosTd on #dadosTd.ndoc = #dadosVendasBase.ndoc
		left Join #dadosSt on #dadosVendasBase.ref = #dadosSt.ref
		left join #dadosFamilia on #dadosVendasBase.familia = #dadosfamilia.ref
	WHERE
		#dadosVendasBase.tipo != 'ENTIDADE'
		AND #dadosVendasBase.u_tipodoc not in (1, 5)
		and left(#dadosVendasBase.ousrhora,2) between (case when @horaini='' then '0' else @horaini end)
										and
								          				(case when @horafim='' then '24' else @horafim end)
		and #dadosVendasBase.ousrinis in (select iniciais from #dadosOperadores)
		and ftstamp in (select distinct  fi.ftstamp COLLATE SQL_Latin1_General_CP1_CI_AI from fi where fi.ref COLLATE SQL_Latin1_General_CP1_CI_AI in (Select ref COLLATE SQL_Latin1_General_CP1_CI_AI from #dadosRefs where ref<>'') and fi.rdata between @dataIni and @dataFim)
		and #dadosVendasBase.[ref] in (Select ref  from #dadosRefs where ref<>'')
		

	select * from #dadosVendasBaseFiltro


	/* Elimina Tabelas */
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBase'))
		DROP TABLE #dadosVendasBase
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFamilia'))
		DROP TABLE #dadosFamilia
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosOperadores'))
		DROP TABLE #dadosOperadores
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRefs'))
		DROP TABLE #dadosRefs
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
		DROP TABLE #dadosSt
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosTd'))
		DROP TABLE #dadosTd
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.##dadosVendasBaseFiltro'))
		DROP TABLE #dadosVendasBaseFiltro	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosVendasBaseQuotaLab'))
		DROP TABLE #dadosVendasBaseQuotaLab	
GO
Grant Execute on dbo.up_relatorio_vendas_produto to Public
Grant control on dbo.up_relatorio_vendas_produto to Public
GO