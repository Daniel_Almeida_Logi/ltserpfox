-- Relatório Conferencia Litigios
-- exec up_relatorio_conferencia_RecDevAberto '20100101','20130101', ''

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_conferencia_RecDevAberto]') IS NOT NULL
	drop procedure dbo.up_relatorio_conferencia_RecDevAberto
go

create procedure dbo.up_relatorio_conferencia_RecDevAberto
@dataIni as  datetime,
@dataFim as  datetime,
@site	 varchar(55)
/* with encryption */

AS 
BEGIN

(
	/* RECEPCOES */
	SELECT	
		(Select TOP 1 EMPRESA FROM empresa (nolock)  WHERE site = @site) as EMPRESA,
		(Select TOP 1 SITE FROM empresa (nolock) WHERE site = @site) as LOJA,
		CMDESC as TIPODOC,
		FO.NO as CODIGOFORNECEDOR,
		FO.NOME as NOMEFORNECEDOR,
		FO.DATA as DATADOCUMENTO,
		FO.ADOC as NUMDOCUMENTO,
		SUM(ETILIQUIDO) as TOTALDOCUMENTO,
		'' as NOTA_CRED_DEB,
		0 as NUMNOTA_CRED_DEB
	FROM	
		FO (nolock) 
		INNER JOIN FN (nolock) ON FN.FOSTAMP = FO.FOSTAMP 
		INNER JOIN CM1 (nolock) ON CM1.CM = FO.doccode
	WHERE	
		FO.doccode  = 102 
		AND u_status = 'A'
		AND FO.docdata Between @dataIni and @dataFim
	GROUP BY 
		armazem,CMDESC,NO,NOME,FO.DATA,FO.ADOC
) 

UNION ALL

(
	/* DEVOLUCOES */
	SELECT	
		(Select TOP 1 EMPRESA FROM empresa (nolock) WHERE site = @site) as EMPRESA,
		(Select TOP 1 SITE FROM empresa (nolock)WHERE site = @site) as LOJA,
		BO.NMDOS as TIPODOC,
		BO.NO as CODIGOFORNECEDOR,
		BO.NOME as NOMEFORNECEDOR,
		BO.DATAOBRA as DATADOCUMENTO,
		CAST(BO.OBRANO as VARCHAR(254)) as NUMDOCUMENTO,
		SUM(BI.ETTDEB) as TOTALDOCUMENTO,
		'' as NOTA_CRED_DEB,0 as NUMNOTA_CRED_DEB
	FROM	
		BO (nolock) 
		INNER JOIN BI (nolock) ON BO.BOSTAMP = BI.BOSTAMP
	WHERE	
		BO.ndos  = 17 AND BO.FECHADA = 0
		AND Bo.dataobra Between @dataIni and @dataFim
	GROUP BY 
		BI.armazem,BO.nmdos,BO.ndos,bo.no,bo.nome,bo.dataobra,BO.obrano
) 

END

GO
Grant Execute on dbo.up_relatorio_conferencia_RecDevAberto to Public
GO
Grant control on dbo.up_relatorio_conferencia_RecDevAberto to Public
GO
