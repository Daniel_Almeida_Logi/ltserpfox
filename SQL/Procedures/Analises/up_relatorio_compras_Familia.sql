/* Relatório Compras Familia
		 exec up_relatorio_compras_Familia '20180901','20180930', 'Loja 1' 
*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_compras_Familia]') IS NOT NULL
	drop procedure dbo.up_relatorio_compras_Familia
go

create procedure dbo.up_relatorio_compras_Familia
@dataIni as datetime,
@dataFim as datetime,
@site varchar(55)
/* WITH ENCRYPTION */ 
AS
BEGIN
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosTempFamilia'))
		DROP TABLE #dadosTempFamilia
	Select 
		st.familia
		,st.ref
		,st.faminome
		,generico = isnull(fprod.generico, convert(bit,0))
	into
		#dadosTempFamilia
	from 
		st (nolock)
		left join fprod (nolock) on fprod.cnp = st.ref
	SELECT	
		DATA = FO.DATA,
		ANO = YEAR(FO.docdata),
		MES = MONTH(FO.docdata),
		FO.DOCNOME,
		--ref = isnull(fn.REF, fn.oref),
		ref = case when fn.ref = '' then fn.oref else fn.ref end,
		fn.DESIGN,
		'FAMILIA_N'		=	ISNULL(#dadosTempFamilia.faminome,''),
		'QUANTIDADE'	=	CASE WHEN DEBITO=1 THEN FN.QTT*-1 ELSE FN.QTT END ,
		'TOTAL'			=	CASE WHEN DEBITO=1 THEN ETILIQUIDO*-1 ELSE ETILIQUIDO END,
		'GENERICO'		=	ISNULL(#dadosTempFamilia.generico,0)
	FROM	
		FO (nolock) 
		INNER JOIN FN (nolock) ON FO.FOSTAMP = FN.FOSTAMP
		INNER JOIN cm1 (nolock) ON  cm1.cm = fo.doccode
		left join #dadosTempFamilia on FN.REF = #dadosTempFamilia.REF or FN.OREF = #dadosTempFamilia.REF
	WHERE 
		--CM1.FOLANFC = 1
		cm1.cm in (55,56)
		AND Fo.docdata between @dataIni and @dataFim
		and fo.site = case when @site = '' then fo.site else @site end
		and (fn.qtt !=0 or fn.ETILIQUIDO!=0)

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosTempFamilia'))
			DROP TABLE #dadosTempFamilia
END


GO
Grant Execute on dbo.up_relatorio_compras_Familia to Public
Grant control on dbo.up_relatorio_compras_Familia to Public
Go
