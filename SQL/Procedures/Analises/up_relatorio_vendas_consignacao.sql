/*
	exec up_relatorio_vendas_consignacao '20171001','20171101', 'Loja 1'
	select * from ft where ftstamp = 'ADMBBFE5935-4EB5-4A0B-831'
	select * from fi where ftstamp = 'ADMBBFE5935-4EB5-4A0B-831'
*/
if OBJECT_ID('[dbo].[up_relatorio_vendas_consignacao]') IS NOT NULL
	drop procedure dbo.up_relatorio_vendas_consignacao
go


create procedure dbo.up_relatorio_vendas_consignacao
@dataIni		as datetime
,@dataFim		as datetime
,@site			as varchar(20)
,@tipoProduto	as varchar(254)

/* with encryption */
AS
	If OBJECT_ID('tempdb.dbo.#dadosVendas') IS NOT NULL
		drop table #dadosVendas;
	SET NOCOUNT ON

	/* Calc operadores */
	select 
		fi.ref, fi.nmdoc, fi.qtt, fi.etiliquido
	into
		#dadosVendas
	from 
		fi (nolock) 
		inner join ft on fi.ftstamp = ft.ftstamp 
	where fi.ousrdata between @dataIni and @dataFim and ft.site = @site

	
	Select distinct
		st.ref
		,st.design
		,valorVendas = isnull((
						Select 
							SUM(etiliquido) 
						from 
							#dadosVendas fi 
						where 
							fi.ref = st.ref 
							and fi.nmdoc in ('Venda a Dinheiro', 'Reg. a Cliente', 'Factura', 'Nota de Crédito', 'Nota de Débito')
							),0)
		,qttVendas = isnull((
						Select 
							SUM(qtt) 
						from 
							#dadosVendas fi (nolock) 
						where 
							fi.ref = st.ref 
							and fi.nmdoc in ('Venda a Dinheiro', 'Factura', 'Nota de Débito')
						),0)
							-
					 isnull((
						Select 
							SUM(qtt) 
						from 
							#dadosVendas fi (nolock) 
						where 
							fi.ref = st.ref 
							and fi.nmdoc in ('Reg. a Cliente', 'Nota de Crédito')
						),0)

		,iva = (select top 1 fn2.iva from fn as fn2 (nolock) where fn2.ref = fn.ref order by data desc)
	from
		fo (nolock)
		inner join fn (nolock) on fo.fostamp = fn.fostamp
		inner join st (nolock) on st.ref = fn.ref
		left join b_famFamilias (nolock) on st.u_famstamp = b_famFamilias.famstamp
	Where
		fo.consignacao = 1
		--and fo.docdata between @dataIni and @dataFim
		and fo.site = @site
		and st.ref != ''
		and st.site_nr = (select no from empresa (nolock) where site = @site)
		and (b_famFamilias.design in (Select * from dbo.up_splitToTable(@tipoProduto,','))
		Or @tipoProduto = '')

	If OBJECT_ID('tempdb.dbo.#dadosVendas') IS NOT NULL
			drop table #dadosVendas;

GO 
Grant Execute on dbo.up_relatorio_vendas_consignacao to Public
Grant control on dbo.up_relatorio_vendas_consignacao to Public
GO

