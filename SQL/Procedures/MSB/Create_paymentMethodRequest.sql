
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'paymentMethodRequest'))
BEGIN

CREATE TABLE [dbo].[paymentMethodRequest](
[stamp] [varchar](36) NOT NULL,
	[token] [varchar](36) NOT NULL,
	[username] [varchar](100) NOT NULL,
	[password] [varchar](100) NOT NULL,
	[typeId] [int] NOT NULL,
	[typeIDdesc] [varchar](254) NOT NULL,
	[typePayment] [int] NOT NULL,
	[typePaymentDesc] [varchar](254) NOT NULL,
	[receiverid] [varchar](100) NOT NULL,
	[receivername] [varchar](254) NOT NULL,
	[entityType] [varchar](50) NOT NULL,
	[amount] [numeric](20, 2) NOT NULL,
	[description] [varchar](max) NOT NULL,
	[email] [varchar](254) NOT NULL,
	[phone] [varchar](50) NOT NULL,
	[clientIdNumber] [varchar](100) NOT NULL,
	[externalReference] [varchar](254) NOT NULL,
	[name] [varchar](254) NOT NULL,
	[categoryId] [numeric](10, 0) NOT NULL,
	[callBackURL] [varchar](254) NOT NULL,
	[origin] [varchar](254) NOT NULL,
	[additionalInfo] [varchar](254) NOT NULL,
	[address] [varchar](254) NOT NULL,
	[postCode] [varchar](50) NOT NULL,
	[city] [varchar](100) NOT NULL,
	[nic] [varchar](100) NOT NULL,
	[idUserBackoffice] [numeric](10, 0) NOT NULL,
	[timeLimitDays] [numeric](10, 0) NOT NULL,
	[sendEmail] [bit] NOT NULL,
	[operationId] [varchar](254) NOT NULL,
	[startDate] [varchar](254) NOT NULL,
	[endDate] [varchar](254) NOT NULL,
	[typeGetInfo] [varchar](254) NOT NULL,
	[senderId] [varchar](254) NOT NULL,
	[senderVatNr] [varchar](254) NOT NULL,
	[senderName] [varchar](254) NOT NULL,
	[site] [varchar](100) NOT NULL,
	[test] [bit] NOT NULL,
	[ousrinis] [varchar](30) NOT NULL,
	[ousrdata] [datetime] NOT NULL,
	[usrinis] [varchar](30) NOT NULL,
	[usrdata] [datetime] NOT NULL,
	[officeKey] [varchar](254) NOT NULL,
	[channel] [varchar](254) NOT NULL,
	[fractionalPayment] [bit] NOT NULL,
	[currency] [varchar](254) NOT NULL,
	[countryCode] [varchar](254) NOT NULL,
	[sendSMS] [bit] NOT NULL,
	[errorUrl] [varchar](254) NOT NULL,
	[successUrl] [varchar](254) NOT NULL,
	[cancelUrl] [varchar](254) NOT NULL,
	[reason] [varchar](254) NOT NULL,
	[trxId] [varchar](254) NOT NULL,
	[iban] [varchar](254) NOT NULL,
	[bic] [varchar](254) NOT NULL,
	[urlPost] [varchar](max) NOT NULL,
 CONSTRAINT [PK_paymentMethodRequest] PRIMARY KEY CLUSTERED 
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_token]  DEFAULT ('') FOR [token]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_username]  DEFAULT ('') FOR [username]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_password]  DEFAULT ('') FOR [password]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_typeID]  DEFAULT ('0') FOR [typeID]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_typeIDdesc]  DEFAULT ('') FOR [typeIDdesc]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_typePayment]  DEFAULT ('0') FOR [typePayment]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_typePaymentDesc]  DEFAULT ('') FOR [typePaymentDesc]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_receiverid]  DEFAULT ('') FOR [receiverid]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_receivername]  DEFAULT ('') FOR [receivername]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_entityType]  DEFAULT ('') FOR [entityType]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_amount]  DEFAULT ('0') FOR [amount]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_description]  DEFAULT ('') FOR [description]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_email]  DEFAULT ('') FOR [email]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_phone]  DEFAULT ('') FOR [phone]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_clientIdNumber]  DEFAULT ('') FOR [clientIdNumber]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_externalReference]  DEFAULT ('') FOR [externalReference]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_name]  DEFAULT ('') FOR [name]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_categoryId]  DEFAULT ('0') FOR [categoryId]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_callBackURL]  DEFAULT ('') FOR [callBackURL]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_origin]  DEFAULT ('') FOR [origin]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_additionalInfo]  DEFAULT ('') FOR [additionalInfo]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_address]  DEFAULT ('') FOR [address]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_postCode]  DEFAULT ('') FOR [postCode]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_city]  DEFAULT ('') FOR [city]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_nic]  DEFAULT ('') FOR [nic]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_idUserBackoffice]  DEFAULT ('0') FOR [idUserBackoffice]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_timeLimitDays]  DEFAULT ('') FOR [timeLimitDays]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_sendEmail]  DEFAULT ('0') FOR [sendEmail]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_operationId]  DEFAULT ('') FOR [operationId]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_startDate]  DEFAULT ('') FOR [startDate]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_endDate]  DEFAULT ('') FOR [endDate]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_typeGetInfo]  DEFAULT ('') FOR [typeGetInfo]

ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_site]  DEFAULT ('') FOR [site]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_test]  DEFAULT ('0') FOR [test]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_ousrinis]  DEFAULT ('') FOR [ousrinis]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_usrinis]  DEFAULT ('') FOR [usrinis]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_usrdata]  DEFAULT (getdate()) FOR [usrdata]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_officeKey]  DEFAULT ('') FOR [officeKey]

ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_channel]  DEFAULT ('') FOR [channel]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_fractionalPayment]  DEFAULT ('') FOR [fractionalPayment]

ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_currency]  DEFAULT ('') FOR [currency]

ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_countryCode]  DEFAULT ('') FOR [countryCode]

ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_sendSMS]  DEFAULT ('') FOR [sendSMS]

ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_errorUrl]  DEFAULT ('') FOR [errorUrl]

ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_successUrl]  DEFAULT ('') FOR [successUrl]

ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_cancelUrl]  DEFAULT ('') FOR [cancelUrl]

ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_reason]  DEFAULT ('') FOR [reason]

ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_transactionID]  DEFAULT ('') FOR [trxId]

ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_iban]  DEFAULT ('') FOR [iban]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_bic]  DEFAULT ('') FOR [bic]
ALTER TABLE [dbo].[paymentMethodRequest] ADD  CONSTRAINT [DF_paymentMethodRequest_urlPost]  DEFAULT ('') FOR [urlPost]
END	
