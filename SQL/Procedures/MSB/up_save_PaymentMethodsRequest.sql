/* Save  PaymentMethods Request 
	

	exec up_save_PaymentMethodsRequest 

*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_PaymentMethodsRequest]') IS NOT NULL
	drop procedure dbo.up_save_PaymentMethodsRequest
go

create procedure [dbo].up_save_PaymentMethodsRequest
	@token				[varchar](36) ,
	@username			[varchar](100) ,
	@password			[varchar](100) ,
	@typeID				int, 
	@typeIDDesc			[varchar](254) , 
	@typePayment		int, 
	@typePaymentDesc	[varchar](254) , 
	@receiverid			[varchar](100),
	@receivername		[varchar](254) ,
	@entityType			[varchar](50) ,
	@amount				NUMERIC(20,2) ,
	@description		[varchar](max),
	@email				[varchar](254),
	@phone				[varchar](50),
	@clientIdNumber		[varchar](100),
	@externalReference	[varchar](254),
	@name				[varchar](254),
	@categoryId			NUMERIC(10,0),
	@callBackURL		[varchar](254),
	@origin				[varchar](254),
	@additionalInfo		[varchar](254),
	@address			[varchar](254),
	@postCode			[varchar](50),
	@city				[varchar](100),
	@nic				[varchar](100),
	@idUserBackoffice   NUMERIC(10,0),
	@timeLimitDays		NUMERIC(10,0),
	@sendEmail			bit,
	@operationId		[varchar](254),
	@startDate			[varchar](254),
	@endDate			[varchar](254),
	@typeGetInfo		[varchar](254),
	@senderId			[varchar](254),
	@senderVatNr		[varchar](254),
	@senderName			[varchar](254),
	@site				[varchar](100),
	@test				bit,
	@officeKey			[varchar](254),
	@channel			[varchar](254),
	@fractionalPayment	bit,
	@currency			[varchar](254),
	@countryCode		[varchar](254),
	@sendSMS			bit,
	@reason				[varchar](254),
	@trxId				[varchar](254),
	@iban				[varchar](254),
	@bic				[varchar](254),
	@urlPost			[varchar](max)

/* with encryption */
AS
BEGIN	
  Declare @stamp varchar(36) = LEFT(NEWID(),36)

  
	Insert into paymentMethodRequest(stamp,token,username,[password],typeID,typeIDdesc,typePayment,typePaymentDesc,receiverid,receivername,entityType,amount
				,[description],email,phone,clientIdNumber,externalReference,[name],categoryId,callBackURL,origin,additionalInfo,
				[address],postCode,city,nic,idUserBackoffice,timeLimitDays,sendEmail,operationId,startDate,endDate,typeGetInfo,
				senderId,senderVatNr,senderName,[site],test,ousrinis,ousrdata,usrinis,usrdata,officeKey,channel,fractionalPayment,currency,countryCode,sendSMS,
				reason,trxId,iban,bic,urlPost)
	values(@stamp,@token,@username,@password,@typeID,@typeIDDesc,@typePayment,@typePaymentDesc,@receiverid,@receivername,@entityType,@amount,
				@description,@email,@phone,@clientIdNumber,@externalReference,@name,@categoryId,@callBackURL,@origin,@additionalInfo,
				@address,@postCode,@city,@nic,@idUserBackoffice,@timeLimitDays,@sendEmail,@operationId,@startDate,@endDate,@typeGetInfo,
				@senderId,@senderVatNr,@senderName,@site,@test,'ADM',GETDATE(),'ADM',GETDATE(),@officeKey,@channel,@fractionalPayment,@currency,@countryCode,@sendSMS,
				@reason,@trxId,@iban,@bic,@urlPost)

		select @stamp as stamp 
END

GO
Grant Execute on dbo.up_save_PaymentMethodsRequest to Public
Grant control on dbo.up_save_PaymentMethodsRequest to Public
GO



