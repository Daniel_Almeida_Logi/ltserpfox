/* Save  Break and Save Responses
	

	exec up_save_vaccineResponse

		 exec up_save_vaccineResponse '3f9abafa-1fa3-4640-aabe-f59e642dad1', 'VHA', '', '400' , 'MODEL_ERROR_REGISTRATION_DUPLICATE - Ja existe um registo para o mesmo dia e vacinas;', '1009687000000069286', '164162069', 1, 'saveV
accine', 1, '2567782';

*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_save_vaccineResponse]') IS NOT NULL
	drop procedure dbo.up_save_vaccineResponse
go

create procedure [dbo].up_save_vaccineResponse
	@stamp								[varchar](36),
	@vaccineCode						[varchar](36),
	@token								[varchar](36),
	@code								[varchar](36),
	@descr								[varchar](max),
	@resultCorrelationId				[varchar](100),
	@sns								[varchar](100),
	@type                               int,
	@typeDescr							[varchar](max),
	@test								bit,
	@infarmed							[varchar](100)


/* with encryption */
AS
BEGIN	

	 
		INSERT INTO [dbo].[ext_vacine_response]
				   ([stamp]
				   ,[vacineCode]
				   ,[code]
				   ,[descr]
				   ,[resultCorrelationId]
				   ,[token]
				   ,[sns]
				   ,[type]
				   ,[typeDescr]
				   ,[test]
				   ,[infarmed]
				   )
		VALUES(
				LEFT(@stamp,36),
				LEFT(@vaccineCode,36),
				LEFT(@code,36),
				@descr,
				LEFT(@resultCorrelationId,100),
				LEFT(@token,200),
				LEFT(@sns,100),
				@type,
				@typeDescr,
				@test,
				@infarmed	
			)
END

GO
Grant Execute on dbo.up_save_vaccineResponse to Public
Grant control on dbo.up_save_vaccineResponse to Public
GO