USE [LTSMSB]
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'0428FF9A-1F5F-466A-9519-79580DDC083F', N'er2', N'Recusado', -1, N'Não Pago', N'1', N'HIPAY', 1, N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'14524066-F5A8-4018-A92D-EED9B693297A', N'c6', N'Em Autorização', 0, N'Aguarda Pagamento', N'1', N'HIPAY', 0, N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'181EBAF9-A3EA-4BDF-BAC6-B92AD72A28D2', N'er1', N'Recusado', -1, N'Não Pago', N'1', N'HIPAY', 1, N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'1EB0B2AB-21EA-4744-B832-0146CFCF07F4', N'ap1', N'Reembolsado', 2, N'Reembolsado', N'1', N'HIPAY', 1, N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'4B53B3EC-01C2-4871-9B29-A92C3F2749A7', N'c9', N'Operação anulada por timeout', -1, N'Não Pago', N'1', N'HIPAY', 1, N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'4EAD43A0-4562-4067-9475-44412D433096', N'c3', N'Em Autorização', 0, N'Aguarda Pagamento', N'1', N'HIPAY', 0, N'ADM', CAST(N'2023-02-28T15:20:47.737' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.737' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'585A52A7-3E7E-410C-B6DC-33AC6AF18055', N'c5', N'Recusado', -1, N'Não Pago', N'1', N'HIPAY', 1, N'ADM', CAST(N'2023-02-28T15:20:47.737' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.737' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'6608D8EE-3196-4068-85D5-46BB30A71B49', N'vp3', N'Recusado', -1, N'Não Pago', N'1', N'HIPAY', 1, N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'8851432F-1DE7-4999-9BA4-261727EE03FA', N'c8', N'Operação anulada', -1, N'Não Pago', N'1', N'HIPAY', 1, N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'911757B3-EF2D-4F80-A9F8-A29B835FF406', N'vp2', N'Recusado', -1, N'Não Pago', N'1', N'HIPAY', 1, N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'B69D6D63-11B3-462E-9D41-643891567396', N'vp1', N'Em curso', 0, N'Aguarda Pagamento', N'1', N'HIPAY', 0, N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'CC92B4A1-6F86-47A8-8518-ABBD806D4146', N'c999', N'Erro desconhecido', 0, N'Aguarda Pagamento', N'1', N'HIPAY', 0, N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'CD5A3301-B775-4FAE-BD51-9801582044EA', N'c1', N'Concluído', 1, N'Pago', N'1', N'HIPAY', 1, N'ADM', CAST(N'2023-02-28T15:20:47.720' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.720' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'CFF649F4-5D0C-4D58-85B7-409134148C46', N'c2', N'Recusado', -1, N'Não Pago', N'1', N'HIPAY', 1, N'ADM', CAST(N'2023-02-28T15:20:47.737' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.737' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'ED673945-4EE3-45CB-9DD3-6B3E68DCBB5D', N'c4', N'Recusado', -1, N'Não Pago', N'1', N'HIPAY', 1, N'ADM', CAST(N'2023-02-28T15:20:47.737' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.737' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'FE67D6F6-078F-4FD1-8F4A-89EB913703E2', N'c7', N'Operação Não encontrada', -1, N'Não Pago', N'1', N'HIPAY', 1, N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime))
GO




																																																																			


INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'911757B3-EF2D-4F80-A9F8-A29B835FF410', N'0', N'Pendente', -1, N'Não Pago', N'3', N'EuPago', 0, N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'B69D6D63-11B3-462E-9D41-643891567311', N'0', N'Processamento', 0, N'Aguarda Pagamento', N'3', N'EuPago', 0, N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'CC92B4A1-6F86-47A8-8518-ABBD806D4112', N'0', N'Transferida', 0, N'Aguarda Pagamento', N'3', N'EuPago', 0, N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.767' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'CD5A3301-B775-4FAE-BD51-980158204413', N'0', N'Paga', 1, N'Pago', N'3', N'EuPago', 1, N'ADM', CAST(N'2023-02-28T15:20:47.720' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.720' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'CFF649F4-5D0C-4D58-85B7-409134148C14', N'0', N'Reembolsada', 2, N'Reembolsado', N'3', N'EuPago', 1, N'ADM', CAST(N'2023-02-28T15:20:47.737' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.737' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'ED673945-4EE3-45CB-9DD3-6B3E68DCBB15', N'0', N'Cancelada', 3, N'Não Pago', N'3', N'EuPago', 1, N'ADM', CAST(N'2023-02-28T15:20:47.737' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.737' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'FE67D6F6-078F-4FD1-8F4A-89EB91370316', N'0', N'Expirada', 3, N'Não Pago', N'3', N'EuPago', 1, N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime))
GO

INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'FE67D6F6-078F-4FD1-8F4A-89EB91370317', N'0', N'Erro', 0, N'Aguarda Pagamento', N'3', N'EuPago', 0, N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'FE67D6F6-078F-4FD1-8F4A-89EB91370318', N'0', N'Devolvida', 3, N'Não Pago', N'3', N'EuPago', 1, N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime))
GO
INSERT [dbo].[paymentCode_ext] ([stamp], [code], [codeDesc], [type], [typeDesc], [receiverid], [receivername], [finalState], [ousrinis], [ousrdata], [usrinis], [usrdata]) VALUES (N'FE67D6F6-078F-4FD1-8F4A-89EB91370319', N'0', N'Cativa', 0, N'Aguarda Pagamento', N'3', N'EuPago', 0, N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime), N'ADM', CAST(N'2023-02-28T15:20:47.750' AS DateTime))
GO