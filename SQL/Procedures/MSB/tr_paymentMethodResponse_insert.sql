
GO
/****** Object:  Trigger [dbo].[tr_paymentMethodResponse_alt]    Script Date: 13/02/2025 17:01:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[tr_paymentMethodResponse_insert]') IS NOT NULL
	drop trigger dbo.tr_paymentMethodResponse_insert
go


create TRIGGER [dbo].[tr_paymentMethodResponse_insert] ON [dbo].[paymentMethodResponse]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	Declare @token varchar(36)=''
		Declare @stamp varchar(36)=''
	Declare @statusCode	varchar(100) =''
	Declare @statusDesc	varchar(max)  =''
	Declare @statePaymentID	 int  =0
	Declare @statePaymentIDDesc  varchar(254)  ='Aguarda Pagamento' 
	Declare @statusDate	datetime  =''
	Declare @operationID varchar(100) =''
	Declare @paid bit 
	Declare @receiverid	varchar(max)  =''
	DECLARE @amount NUMERIC(20,2)
	DECLARE @senderId VARCHAR(254)
	Declare @trxId varchar(100) =''
	
	select @token=token,@stamp=stamp,@operationID=operationId,@trxId=trxId,@receiverid=@receiverid,@amount=amount, @statusCode=statusCode , @statusDesc=statusDescription  , @statePaymentID=@statePaymentID , @statePaymentIDDesc=@statePaymentIDDesc , @paid=paid from inserted 
	

	IF ( (SELECT count(*) FROM inserted WHERE statusId in (200,201)) >=1 and @operationID!='' and (select typeId from paymentMethodRequest(nolock) where stamp=@stamp)=1 
		and (select count(*) from [paymentMethodInfo](nolock) where token = @token and operationId=@operationID)=0  )
	BEGIN

	 	IF(@paid=1)
		BEGIN
			set @statePaymentID =1 
			set @statePaymentIDDesc ='Pago' 
		END  
		ELSE if (@statusCode!='')
		BEGIN
			select   top 1 @statePaymentID=isnull(type,0), @statePaymentIDDesc=isnull(@statePaymentIDDesc,'Aguarda Pagamento')  from [paymentCode_ext](nolock) where code=@statusCode 
		END 


		Insert into [paymentMethodInfo](stamp,token,username,password,typeId, typeIDdesc, typePayment, typePaymentDesc, receiverid,receivername,entityType
				,amount,operationId,officeKey,senderId,senderVatNr,senderName,site,test,statusCode,statusDescription,statePaymentID,statePaymentIDDesc,statusDate,
				 urlPost,trxid,finalState,ousrinis,ousrdata,usrinis,usrdata)
		select 	@stamp,token,username,paymentMethodRequest.password,2,'INFOPAYMENT',typePayment,typePaymentDesc,receiverid,receivername,entityType,
				amount,@operationID,officeKey,senderId,senderVatNr,senderName,site,test,@statusCode,@statusDesc,@statePaymentID,@statePaymentIDDesc,@statusDate,
				 urlPost,'',0,ousrinis,GETDATE(),usrinis,GETDATE()
		from paymentMethodRequest(nolock) where stamp=@stamp	
	
	
	END
	ELSE IF ((SELECT count(*) FROM inserted WHERE statusId in (200,201)) >1 and (select typeId from paymentMethodRequest(nolock) where stamp=@stamp)=3 ) 
	begin

	select TOP 1 @senderId=senderId from paymentMethodRequest(nolock) where stamp=@stamp order by ousrdata desc	

		update [paymentMethodInfo]
		set 
			finalstate=0 ,
			@statusCode= -99, -- coloco a menos 3 para ser chamado e enviar o pedido quando for site 
			usrdata =getdate()
		where  trxId=@trxId and receiverid= @receiverid and senderId =@senderId

	end 
		ELSE IF ((SELECT count(*) FROM inserted WHERE statusId in (200,201)) >1 and (select typeId from paymentMethodRequest(nolock) where stamp=@stamp)=2 and @statusDesc='paga') 
		begin
		
			select TOP 1 @senderId=senderId from paymentMethodRequest(nolock) where stamp=@stamp	


		update [paymentMethodInfo]
		set 
			trxId=@trxId
		where  operationId=@operationID and receiverid= @receiverid and senderId =@senderId

		end 
END



