

/****** Object:  Table [dbo].[paymentCode_ext]    Script Date: 13/02/2025 17:31:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[paymentCode_ext](
	[stamp] [varchar](36) NOT NULL,
	[code] [varchar](50) NOT NULL,
	[codeDesc] [varchar](50) NOT NULL,
	[type] [int] NOT NULL,
	[typeDesc] [varchar](254) NOT NULL,
	[receiverid] [varchar](100) NOT NULL,
	[receivername] [varchar](254) NOT NULL,
	[finalState]	bit not null ,
	[ousrinis] [varchar](30) NOT NULL,
	[ousrdata] [datetime] NOT NULL,
	[usrinis] [varchar](30) NOT NULL,
	[usrdata] [datetime] NOT NULL,
 CONSTRAINT [PK_paymentCode_ext] PRIMARY KEY CLUSTERED 
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[paymentCode_ext] ADD  CONSTRAINT [DF_paymentCode_ext_code]  DEFAULT ('') FOR [code]
GO

ALTER TABLE [dbo].[paymentCode_ext] ADD  CONSTRAINT [DF_paymentCode_ext_codeDesc]  DEFAULT ('') FOR [codeDesc]
GO

ALTER TABLE [dbo].[paymentCode_ext] ADD  CONSTRAINT [DF_paymentCode_ext_type]  DEFAULT ('0') FOR [type]
GO

ALTER TABLE [dbo].[paymentCode_ext] ADD  CONSTRAINT [DF_paymentCode_ext_typeDesc]  DEFAULT ('') FOR [typeDesc]
GO

ALTER TABLE [dbo].[paymentCode_ext] ADD  CONSTRAINT [DF_paymentCode_ext_receiverid]  DEFAULT ('') FOR [receiverid]
GO

ALTER TABLE [dbo].[paymentCode_ext] ADD  CONSTRAINT [DF_paymentCode_ext_receivername]  DEFAULT ('') FOR [receivername]
GO

ALTER TABLE [dbo].[paymentCode_ext] ADD  CONSTRAINT [DF_paymentCode_ext_finalState]  DEFAULT ('0') FOR [finalState]
GO

ALTER TABLE [dbo].[paymentCode_ext] ADD  CONSTRAINT [DF_paymentCode_ext_ousrinis]  DEFAULT ('') FOR [ousrinis]
GO

ALTER TABLE [dbo].[paymentCode_ext] ADD  CONSTRAINT [DF_paymentCode_ext_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]
GO

ALTER TABLE [dbo].[paymentCode_ext] ADD  CONSTRAINT [DF_paymentCode_ext_usrinis]  DEFAULT ('') FOR [usrinis]
GO

ALTER TABLE [dbo].[paymentCode_ext] ADD  CONSTRAINT [DF_paymentCode_ext_usrdata]  DEFAULT (getdate()) FOR [usrdata]
GO


