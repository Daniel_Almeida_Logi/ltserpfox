/* Save  up_get_AllPaymentCodes
	

	exec up_get_AllPaymentCodes

*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_AllPaymentCodes]') IS NOT NULL
	drop procedure dbo.up_get_AllPaymentCodes
go

create procedure [dbo].[up_get_AllPaymentCodes]

/* with encryption */
AS
BEGIN	
	SET NOCOUNT ON;

		select    
			codeDesc,
			finalState  
		from [paymentCode_ext](nolock) 



END

GO
Grant Execute on dbo.up_get_AllPaymentCodes to Public
Grant control on dbo.up_get_AllPaymentCodes to Public
GO