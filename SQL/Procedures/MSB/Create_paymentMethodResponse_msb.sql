
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'paymentMethodResponse'))
BEGIN

CREATE TABLE [dbo].[paymentMethodResponse](
	[stamp] [varchar](36) NOT NULL,
	[token] [varchar](36) NOT NULL,
	[statusId] [int] NOT NULL,
	[statusDesc] [varchar](254) NOT NULL,
	[errorId] [int] NOT NULL,
	[errorDesc] [varchar](254) NOT NULL,
	[operationId] [varchar](100) NOT NULL,
	[status] [varchar](100) NOT NULL,
	[statusCode] [varchar](254) NOT NULL,
	[statusDescription] [varchar](max) NOT NULL,
	[statusDescriptionDetail] [varchar](max) NOT NULL,
	[entityType] [varchar](50) NOT NULL,
	[amount] [numeric](20, 2) NOT NULL,
	[categoryId] [numeric](10, 0) NOT NULL,
	[description] [varchar](max) NOT NULL,
	[phone] [varchar](50) NOT NULL,
	[requestDate] [datetime] NOT NULL,
	[statusDate] [datetime] NOT NULL,
	[validRequest] [bit] NOT NULL,
	[paid] [bit] NOT NULL,
	[lastPaymentDate] [varchar](50) NOT NULL,
	[totalPayments] [numeric](5, 0) NOT NULL,
	[ousrinis] [varchar](30) NOT NULL,
	[ousrdata] [datetime] NOT NULL,
	[usrinis] [varchar](30) NOT NULL,
	[usrdata] [datetime] NOT NULL,
	[transactionID] [varchar](254) NOT NULL,
	[trxId] [varchar](254) NOT NULL,
 CONSTRAINT [PK_paymentMethodResponse] PRIMARY KEY CLUSTERED 
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[paymentMethodResponse] ADD  CONSTRAINT [DF_paymentMethodResponse_token]  DEFAULT ('') FOR [token]


ALTER TABLE [dbo].[paymentMethodResponse] ADD  CONSTRAINT [DF_paymentMethodResponse_statusId]  DEFAULT ('0') FOR [statusId]


ALTER TABLE [dbo].[paymentMethodResponse] ADD  CONSTRAINT [DF_paymentMethodResponse_statusDesc]  DEFAULT ('') FOR [statusDesc]


ALTER TABLE [dbo].[paymentMethodResponse] ADD  CONSTRAINT [DF_paymentMethodResponse_errorId]  DEFAULT ('0') FOR [errorId]


ALTER TABLE [dbo].[paymentMethodResponse] ADD  CONSTRAINT [DF_paymentMethodResponse_errorDesc]  DEFAULT ('') FOR [errorDesc]

ALTER TABLE [dbo].[paymentMethodResponse] ADD  CONSTRAINT [DF_paymentMethodResponse_operationId]  DEFAULT ('') FOR [operationId]


ALTER TABLE [dbo].[paymentMethodResponse] ADD  CONSTRAINT [DF_paymentMethodResponse_status]  DEFAULT ('') FOR [status]


ALTER TABLE [dbo].[paymentMethodResponse] ADD  CONSTRAINT [DF_paymentMethodResponse_statusDescription]  DEFAULT ('') FOR [statusDescription]


ALTER TABLE [dbo].[paymentMethodResponse] ADD  CONSTRAINT [DF_paymentMethodResponse_transactionID]  DEFAULT ('') FOR [transactionID]


ALTER TABLE [dbo].[paymentMethodResponse] ADD  CONSTRAINT [DF_paymentMethodResponse_trxId]  DEFAULT ('') FOR [trxId]

END	
