USE [LTSMSB]
GO
/****** Object:  Trigger [dbo].[tr_paymentMethodResponse_insert]    Script Date: 24/02/2025 15:46:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if OBJECT_ID('[dbo].[tr_paymentMethodInfo_insertUpdate]') IS NOT NULL
	drop trigger dbo.[tr_paymentMethodInfo_insertUpdate]
go


CREATE TRIGGER  [dbo].[tr_paymentMethodInfo_insertUpdate] ON [dbo].[paymentMethodInfo]
   AFTER INSERT,UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	Declare @stamp varchar(36)

	SELECT @stamp=stamp FROM inserted   
	
	IF EXISTS (SELECT * FROM paymentMethodInfo_hist where stamp=@stamp)
	BEGIN
		update paymentMethodInfo_hist set
			username=inserted.username,
			[password]=inserted.[password],
			typeID=inserted.typeID,
			typeIDdesc=inserted.typeIDdesc,
			typePayment=inserted.typePayment,
			typePaymentDesc=inserted.typePaymentDesc,
			receiverid=inserted.receiverid,
			receivername=inserted.receivername,
			entityType=inserted.entityType,
			amount=inserted.amount,
			operationId=inserted.operationId,
			officeKey=inserted.officeKey,
			senderId=inserted.senderId,
			senderVatNr=inserted.senderVatNr,
			senderName=inserted.senderName,
			site=inserted.site,
			test=inserted.test,
			statusCode=inserted.statusCode,
			statusDescription=inserted.statusDescription,
			statePaymentID=inserted.statePaymentID,
			statePaymentIDDesc=inserted.statePaymentIDDesc,
			statusDate=inserted.statusDate,
			urlPost=inserted.urlPost,
			finalState=inserted.finalState,
			usrinis=inserted.usrinis,
			usrdata=inserted.usrdata,
			trxid =inserted.trxid
		from inserted			
			where paymentMethodInfo_hist.stamp=@stamp

	END
	ELSE
	BEGIN
			insert into paymentMethodInfo_hist(
					stamp,token,username,[password],typeID,typeIDdesc,typePayment,typePaymentDesc,receiverid,receivername,entityType,amount
					,operationId,officeKey,senderId,senderVatNr,senderName,site,test,statusCode,statusDescription,statePaymentID,statePaymentIDDesc
					,statusDate,urlPost,trxid,finalState,ousrinis,ousrdata,usrinis,usrdata)
			select 	inserted.stamp,token,username,[password],typeID,typeIDdesc,typePayment,typePaymentDesc,receiverid,receivername,entityType,amount
					,operationId,officeKey,senderId,senderVatNr,senderName,site,test,statusCode,statusDescription,statePaymentID,statePaymentIDDesc
					,statusDate,urlPost,trxid,finalState,ousrinis,ousrdata,usrinis,usrdata
			from inserted			
	END


END