/* Save  PaymentMethodsInfo
	

	exec up_update_PaymentMethods_Info
	exec up_update_PaymentMethods_Info 'FAE5187B-74AD-4C55-87A1-22DFDG3344','0','pendente',0 
*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_update_PaymentMethods_Info]') IS NOT NULL
	drop procedure dbo.up_update_PaymentMethods_Info
go

create procedure [dbo].[up_update_PaymentMethods_Info]
	@stamp				[varchar](36),
	@token				[varchar](36),
	@statusCode			[varchar](254),
	@statusDescription	[varchar](max),
	@paid				bit



/* with encryption */
AS
BEGIN	
	SET NOCOUNT ON;

	DECLARE @statePaymentID int =0 
	DECLARE @statePaymentIDDesc varchar(254) ='Aguarda Pagamento' 
	declare @finalState bit =0

	IF(@paid=1 and @statusCode!=2)
	BEGIN
		set @statePaymentID =1 
		set @statePaymentIDDesc ='Pago' 
		set @finalState =1
		set @statusCode=1
	END  
	ELSE if (@statusDescription!='')
	BEGIN
		select   top 1 @statePaymentID=isnull(type,0), @statePaymentIDDesc=isnull(typeDesc,'Aguarda Pagamento'), @finalState=isnull(finalState,0)  from [paymentCode_ext](nolock) where codeDesc=@statusDescription 
	END 

	UPDATE [paymentMethodInfo] SET 
			[statusCode] = @statusCode,
			[statusDescription] = @statusDescription,
			statePaymentID = @statePaymentID , 
			statePaymentIDDesc = @statePaymentIDDesc,
			finalState=@finalState,
			[usrinis] = 'ADM',
			[usrdata] = GETDATE()
	WHERE stamp = @stamp
 

END

GO
Grant Execute on dbo.up_update_PaymentMethods_Info to Public
Grant control on dbo.up_update_PaymentMethods_Info to Public
GO