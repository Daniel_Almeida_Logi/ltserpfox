/* get  PaymentMethods Request 
	
	exec up_paymentMethod_createPaymentInfoMsb 60
	exec up_paymentMethod_createPaymentInfoMsb 60
	exec up_paymentMethod_createPaymentInfoMsb 60
*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_paymentMethod_createPaymentInfoMsb]') IS NOT NULL
	drop procedure dbo.up_paymentMethod_createPaymentInfoMsb
go

create procedure [dbo].up_paymentMethod_createPaymentInfoMsb
	@nday				int 
AS
BEGIN
	SET NOCOUNT ON;
	delete  paymentMethodInfo where DATEDIFF (day, ousrdata, GETDATE()) >@nday
	
	update [paymentMethodInfo] set statePaymentID=-3 , statePaymentIDDesc='Expirado',finalstate=1 where  statePaymentID =0  and ousrinis = '1900-01-01 00:00:00.000' and   DATEDIFF (day, ousrdata, GETDATE()) >@nday

  
	select  stamp,token,username,[password],typeID,typeIDdesc,typePayment,typePaymentDesc,receiverid,receivername,entityType,amount
				,operationId,officeKey,senderId,senderVatNr,senderName,site,test,statusCode,statusDescription,statePaymentID,statePaymentIDDesc
				,statusDate,urlPost,finalState,ousrinis,ousrdata,usrinis,usrdata, 
				(select top 1 'jdbc:sqlserver://' + server + ':' + Convert(varchar(10),port) + ';applicationName=LTSConnection;DatabaseName=' + dbname from connectionDBS where name=senderId)  as databaseCon_jdbc
	FROM paymentMethodInfo (nolock)
	WHERE  finalstate=0 and DATEDIFF (day, ousrdata, GETDATE())  <= @nday
	order by senderId,site asc
END

GO
Grant Execute on dbo.up_paymentMethod_createPaymentInfoMsb to Public
Grant control on dbo.up_paymentMethod_createPaymentInfoMsb to Public
GO