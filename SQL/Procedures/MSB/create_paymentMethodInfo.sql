

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'paymentMethodInfo'))
BEGIN

CREATE TABLE [dbo].[paymentMethodInfo](
	[stamp] [varchar](36) NOT NULL,
	[token] [varchar](36) NOT NULL,
	[username] [varchar](100) NOT NULL,
	[password]	[varchar](100) NOT NULL,
	[typeId]	int NOT NULL, 
	[typeIDdesc] [varchar](254) NOT NULL, 
	[typePayment]	int NOT NULL, 
	[typePaymentDesc] [varchar](254) NOT NULL, 
	[receiverid] [varchar](100) NOT NULL,
	[receivername] [varchar](254) NOT NULL,
	[entityType] [varchar](50) NOT NULL,
	[amount]	numeric(20,2) NOT NULL,
	[operationId]			[varchar](254) NOT NULL,
	[officeKey]	[varchar](254) NOT NULL,
	[senderId]			[varchar](254) NOT NULL,
	[senderVatNr]			[varchar](254) NOT NULL,
	[senderName]		[varchar](254) NOT NULL,
	[site]				[varchar](100) NOT NULL,
	[test]				bit NOT NULL,
	[statusCode]			 [varchar](100) NOT NULL,
	[statusDescription]			[varchar](max) NOT NULL,
	[statePaymentID]	 int NOT NULL,
	[statePaymentIDDesc]  [varchar](254) NOT NULL,
	[statusDate]		[datetime] NOT NULL ,
	[urlPost]			[varchar](max) NOT NULL,
	[trxId]            [varchar](254) NOT NULL,
	[finalState]		bit NOT NULL,	
	[ousrinis]			[varchar](30)NOT NULL ,
	[ousrdata]			[datetime] NOT NULL ,
	[usrinis]			[varchar](30)NOT NULL ,
	[usrdata]			[datetime] NOT NULL ,
 CONSTRAINT [PK_paymentMethodInfo] PRIMARY KEY CLUSTERED 
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_token]  DEFAULT ('') FOR [token]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_username]  DEFAULT ('') FOR [username]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_password]  DEFAULT ('') FOR [password]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_typeID]  DEFAULT ('0') FOR [typeID]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_typeIDdesc]  DEFAULT ('') FOR [typeIDdesc]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_typePayment]  DEFAULT ('0') FOR [typePayment]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_typePaymentDesc]  DEFAULT ('') FOR [typePaymentDesc]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_receiverid]  DEFAULT ('') FOR [receiverid]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_receivername]  DEFAULT ('') FOR [receivername]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_entityType]  DEFAULT ('') FOR [entityType]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_amount]  DEFAULT ('0') FOR [amount]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_operationId]  DEFAULT ('') FOR [operationId]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_officeKey]  DEFAULT ('') FOR [officeKey]

ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_senderId]  DEFAULT ('') FOR [senderId]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_senderVatNr]  DEFAULT ('') FOR [senderVatNr]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_senderName]  DEFAULT ('') FOR [senderName]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_site]  DEFAULT ('') FOR [site]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_test]  DEFAULT ('0') FOR [test]


ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_statusCode]  DEFAULT ('') FOR [statusCode]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_statusDescription]  DEFAULT ('') FOR [statusDescription]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_statePaymentID]  DEFAULT ('') FOR [statePaymentID]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_statePaymentIDDesc]  DEFAULT ('') FOR [statePaymentIDDesc]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_statusDate]  DEFAULT ('') FOR [statusDate]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_urlPost]  DEFAULT ('') FOR [urlPost]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_trxId]  DEFAULT ('') FOR [trxId]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_finalState]  DEFAULT ('0') FOR [finalState]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_ousrinis]  DEFAULT ('') FOR [ousrinis]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_usrinis]  DEFAULT ('') FOR [usrinis]
ALTER TABLE [dbo].[paymentMethodInfo] ADD  CONSTRAINT [DF_paymentMethodInfo_usrdata]  DEFAULT (getdate()) FOR [usrdata]
	
END	





IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'paymentMethodInfo_hist'))
BEGIN

CREATE TABLE [dbo].[paymentMethodInfo_hist](
	[stamp] [varchar](36) NOT NULL,
	[token] [varchar](36) NOT NULL,
	[username] [varchar](100) NOT NULL,
	[password]	[varchar](100) NOT NULL,
	[typeId]	int NOT NULL, 
	[typeIDdesc] [varchar](254) NOT NULL, 
	[typePayment]	int NOT NULL, 
	[typePaymentDesc] [varchar](254) NOT NULL, 
	[receiverid] [varchar](100) NOT NULL,
	[receivername] [varchar](254) NOT NULL,
	[entityType] [varchar](50) NOT NULL,
	[amount]	numeric(20,2) NOT NULL,
	[operationId]			[varchar](254) NOT NULL,
	[officeKey]	[varchar](254) NOT NULL,
	[senderId]			[varchar](254) NOT NULL,
	[senderVatNr]			[varchar](254) NOT NULL,
	[senderName]		[varchar](254) NOT NULL,
	[site]				[varchar](100) NOT NULL,
	[test]				bit NOT NULL,
	[statusCode]			 [varchar](100) NOT NULL,
	[statusDescription]			[varchar](max) NOT NULL,
	[statePaymentID]	 int NOT NULL,
	[statePaymentIDDesc]  [varchar](254) NOT NULL,
	[statusDate]		[datetime] NOT NULL ,
	[urlPost]			[varchar](max) NOT NULL,
	[trxId]             [varchar](254) NOT NULL,
	[finalState]		bit NOT NULL,	
	[ousrinis]			[varchar](30)NOT NULL ,
	[ousrdata]			[datetime] NOT NULL ,
	[usrinis]			[varchar](30)NOT NULL ,
	[usrdata]			[datetime] NOT NULL ,
 CONSTRAINT [PK_paymentMethodInfo_hist] PRIMARY KEY CLUSTERED 
(
	[stamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_token]  DEFAULT ('') FOR [token]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_username]  DEFAULT ('') FOR [username]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_password]  DEFAULT ('') FOR [password]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_typeID]  DEFAULT ('0') FOR [typeID]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_typeIDdesc]  DEFAULT ('') FOR [typeIDdesc]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_typePayment]  DEFAULT ('0') FOR [typePayment]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_typePaymentDesc]  DEFAULT ('') FOR [typePaymentDesc]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_receiverid]  DEFAULT ('') FOR [receiverid]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_receivername]  DEFAULT ('') FOR [receivername]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_entityType]  DEFAULT ('') FOR [entityType]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_amount]  DEFAULT ('0') FOR [amount]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_operationId]  DEFAULT ('') FOR [operationId]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_officeKey]  DEFAULT ('') FOR [officeKey]

ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_senderId]  DEFAULT ('') FOR [senderId]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_senderVatNr]  DEFAULT ('') FOR [senderVatNr]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_senderName]  DEFAULT ('') FOR [senderName]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_site]  DEFAULT ('') FOR [site]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_test]  DEFAULT ('0') FOR [test]


ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_statusCode]  DEFAULT ('') FOR [statusCode]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_statusDescription]  DEFAULT ('') FOR [statusDescription]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_statePaymentID]  DEFAULT ('') FOR [statePaymentID]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_statePaymentIDDesc]  DEFAULT ('') FOR [statePaymentIDDesc]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_statusDate]  DEFAULT ('') FOR [statusDate]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_urlPost]  DEFAULT ('') FOR [urlPost]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_trxId]  DEFAULT ('') FOR [trxId]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_finalState]  DEFAULT ('0') FOR [finalState]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_ousrinis]  DEFAULT ('') FOR [ousrinis]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_usrinis]  DEFAULT ('') FOR [usrinis]
ALTER TABLE [dbo].[paymentMethodInfo_hist] ADD  CONSTRAINT [DF_paymentMethodInfo_hist_usrdata]  DEFAULT (getdate()) FOR [usrdata]
	
END	
