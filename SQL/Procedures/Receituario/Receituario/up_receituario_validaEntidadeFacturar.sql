/* 
   Valida se entidade est� bem criada para facturacao

   exec up_receituario_validaEntidadeFacturar  1,5 ,'Loja 1'
   exec up_receituario_validaEntidadeFacturar  142,0 ,'Loja 1'
   exec up_receituario_validaEntidadeFacturar  142,0 ,'Loja 2'
   exec up_receituario_validaEntidadeFacturar  72,0 ,'Loja 1'

*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_validaEntidadeFacturar]') IS NOT NULL
	drop procedure dbo.up_receituario_validaEntidadeFacturar
go

create procedure dbo.up_receituario_validaEntidadeFacturar


@no      int,
@estab   int,
@site    varchar(18)



/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	IF object_id('tempdb.dbo.#temp_efrvalida') IS NOT NULL
		DROP TABLE #temp_efrvalida;
	
	select 
		u_no,
		u_estab,
		entPorLoja  = case when isnull(cptorg.entPorLoja,0)=1 and convert(date,entPorLojaAposData)<=convert(date,getdate()) then 1 else 0 end,
		abrev = isnull(cps.cptorgAbrev,''),
		entAbrev = isnull(cptorg.abrev,'')
	into 
		#temp_efrvalida
	from 
		cptorg(nolock) 
		left join cptorg_dep_site cps(nolock) on cps.cptorgStamp=cptorg.cptorgstamp and cps.site=@site
	where 
		(cptorg.u_no=@no or cps.no=@no)
		and (cptorg.u_estab=@estab or cps.estab=@estab)


	select 
		entvalida = case 
					when entPorLoja=1 and abrev!='' then 1 --se a entidade por loja estiver preenchida
					when entPorLoja=0 and u_no>0 and u_estab>-1 then 1 --se a entidade n�o for loja tem de validar a ficha
					else 0 
					end
		,entAbrev
	from
		#temp_efrvalida

	IF object_id('tempdb.dbo.#temp_efrvalida') IS NOT NULL
		DROP TABLE #temp_efrvalida;
	

GO
Grant Execute on dbo.up_receituario_validaEntidadeFacturar to Public
Grant Execute on dbo.up_receituario_validaEntidadeFacturar to Public
go


