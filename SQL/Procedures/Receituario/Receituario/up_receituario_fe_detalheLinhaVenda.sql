/*	
	Totais de cada linha da factura as entidades

	exec up_receituario_fe_detalheLinhaVenda 2018,06,'SBSI','Tipo 97 - RSP Com Sucesso','Loja 1'
*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_fe_detalheLinhaVenda]') IS NOT NULL
	drop procedure up_receituario_fe_detalheLinhaVenda
go



Create procedure [dbo].up_receituario_fe_detalheLinhaVenda
	@ano NUMERIC(4)
	,@mes NUMERIC(2)
	,@abrev	VARCHAR(15)
	,@lote	VARCHAR(15)
	,@site varchar(20)

/* with encryption */
AS
	SET NOCOUNT ON

	
	If OBJECT_ID('tempdb.dbo.#receitas_entidade') IS NOT NULL
		drop table #receitas_entidade;
	If OBJECT_ID('tempdb.dbo.#faturacao_entidade') IS NOT NULL
		drop table #faturacao_entidade;


	select
		ctltrctstamp,  
		u_fistamp
	into 
		#receitas_entidade	
	from
		ctltrct c (nolock)
	where
		c.ano=@ano and c.mes=@mes
		and c.cptorgabrev=@abrev
		and c.u_fistamp!=''
		and c.tlote=right(left(@lote,7),2)
		and site = @site
	
	select 
		fi.u_epvp,
		fi.u_ettent1,
		fi.u_ettent2,
		fi.qtt,
		fi.fistamp
    into 
		#faturacao_entidade
	from 
		ft(nolock)
	inner join 
		#receitas_entidade re
	on 
		re.ctltrctstamp=ft.u_ltstamp or re.ctltrctstamp=ft.u_ltstamp2
	inner join 
		fi(nolock) 
	on 
		fi.ftstamp = ft.ftstamp
	where 
		fi.u_ettent1>0 or fi.u_ettent2>0


	select 
		utenteEntidade = (sum(round(f.u_epvp,2) * f.qtt)) - (sum(round(f.u_ettent1,2) + round(f.u_ettent2,2))),
		compartEntidade = sum(round(f.u_ettent1,2)),
		pvp = sum(round(f.u_epvp,2) * f.qtt)
	from 
		#faturacao_entidade f



    If OBJECT_ID('tempdb.dbo.#receitas_entidade') IS NOT NULL
		drop table #receitas_entidade;
	If OBJECT_ID('tempdb.dbo.#faturacao_entidade') IS NOT NULL
		drop table #faturacao_entidade;



GO
Grant Execute On up_receituario_fe_detalheLinhaVenda to Public
Grant Control On up_receituario_fe_detalheLinhaVenda to Public
GO