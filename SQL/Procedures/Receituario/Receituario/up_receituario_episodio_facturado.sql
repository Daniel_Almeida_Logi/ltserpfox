/* 
    Devolve se um epis�dio j� foi faturado nos �ltimos meses
    Uso: exec up_receituario_episodio_facturado '398623685',  '596412','Loja 1', '7532622'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

-- Se o procedimento j� existir, ele ser� exclu�do
IF OBJECT_ID('[dbo].[up_receituario_episodio_facturado]') IS NOT NULL
    DROP PROCEDURE up_receituario_episodio_facturado
GO

CREATE PROCEDURE [dbo].up_receituario_episodio_facturado
    @idProcesso VARCHAR(240),
    @idEpisodio VARCHAR(240),
    @site VARCHAR(20),
	@ref  VARCHAR(18)

AS
SET NOCOUNT ON

		-- Seleciona a mensagem indicando se o epis�dio foi faturado
		SELECT
			msg = 'Epis�dio ' + RTRIM(LTRIM(fi2.idEpisodio)) + 
				  ' j� faturado na ' + LTRIM(RTRIM(ISNULL(ft.nmdoc, ''))) + 
				  ' n� ' + RTRIM(LTRIM(STR(ISNULL(ft.fno, 0)))) + 
				  ' de ' + CONVERT(VARCHAR(10), fdata, 23) + '.',
			resultado = case when fi2.fistamp is not null then 1 else 0 end
		FROM 
			fi2 (NOLOCK)
		INNER JOIN  
			ft (NOLOCK) ON ft.ftstamp = fi2.ftstamp
		INNER JOIN  
			ft2 (NOLOCK) ON ft2.ft2stamp = ft.ftstamp
		INNER JOIN
			ctltrct on ft.u_ltstamp = ctltrct.ctltrctstamp
		WHERE 
			(ft.tipodoc = 1 OR ft.tipodoc = 4)  -- 1: vendas, 4: faturas/inser��es de receita
			AND fi2.idEpisodio = @idEpisodio
			AND fi2.idProcesso = @idProcesso
			AND fi2.idEpisodio != ''  -- Verifica se o idEpisodio n�o est� vazio
			AND fi2.idProcesso != ''  -- Verifica se o idProcesso n�o est� vazio
			AND ft.fdata >= GETDATE() - 180  -- �ltimos 6 meses (aproximadamente 180 dias)
			AND ft.site = @site  -- Filtra pelo site especificado

GO

-- Permiss�es
GRANT EXECUTE ON up_receituario_episodio_facturado TO Public
GRANT CONTROL ON up_receituario_episodio_facturado TO Public
GO


