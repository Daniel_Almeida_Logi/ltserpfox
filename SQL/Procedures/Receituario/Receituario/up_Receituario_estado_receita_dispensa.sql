/*

	up_Receituario_estado_receita_dispensa 'DD3B8C61A643AB78E0532198CA0AE40B'

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Receituario_estado_receita_dispensa]') IS NOT NULL
	drop procedure dbo.up_Receituario_estado_receita_dispensa
go

create procedure dbo.up_Receituario_estado_receita_dispensa
@TOKEN VARCHAR(50) = 'DD3B8C61A643AB78E0532198CA0AE40B'

AS

SET NOCOUNT ON
	
	SELECT 
		Dispensa_Eletronica.receita_nr,
		Dispensa_Eletronica_DD.*  
	FROM 
		[dbo].[Dispensa_Eletronica_DD](nolock)
		inner join Dispensa_Eletronica (nolock) ON Dispensa_Eletronica.token = Dispensa_Eletronica_DD.token
	WHERE 
		Dispensa_Eletronica_DD.token=@token

GO
Grant Execute On dbo.up_Receituario_estado_receita_dispensa to Public
Grant Control On dbo.up_Receituario_estado_receita_dispensa to Public
GO