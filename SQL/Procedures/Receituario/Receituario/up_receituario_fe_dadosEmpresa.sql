/*
	Faz levantamento dos dados da empresa para a Faturação Eletrónica de Medicamentos
	Data Alteração: 10/02/2014
	exec [dbo].[up_receituario_fe_dadosEmpresa] 'Loja 1', 'DIGNITUDE'
	exec [dbo].[up_receituario_fe_dadosEmpresa] 'Loja 1', 'SNS'
	exec [dbo].[up_receituario_fe_dadosEmpresa] 'Loja 1', '', 'ADM728D6B34-69B1-4834-BA0' 
	exec up_receituario_fe_dadosEmpresa 'Loja 4','','CSF6A92B69-F0BB-4D52-8BF'
	exec up_receituario_fe_dadosEmpresa 'Loja 1','','CSF6A92B69-F0BB-4D52-8BF'
	Nota: Esta SP tem algumas alterações que foram feitas de forma a não partir a compatibilidade com outros serviços que já a usavam

	
*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_fe_dadosEmpresa]') IS NOT NULL
	drop procedure [up_receituario_fe_dadosEmpresa]
go

Create procedure [dbo].[up_receituario_fe_dadosEmpresa]
	@site varchar(60),
	@entidadeAbrev varchar(15) = '',
	@stamp varchar(50) = ''

/* with encryption */

AS
SET NOCOUNT ON

	If OBJECT_ID('tempdb.dbo.#tmp_fe_efr') IS NOT NULL
		drop table #tmp_fe_efr;
	If OBJECT_ID('tempdb.dbo.#tmp_fe_efr_sns') IS NOT NULL
		drop table #tmp_fe_efr_sns;

	declare @clNo int= 0
	declare @clEstab int= -1
	declare @orgAbrev varchar(100) = ''


	
	-- Validar se entidade existe
	if(rtrim(ltrim(@stamp))!='')
	begin
		select @clNo=ft.no,  @clEstab=ft.estab, @orgAbrev=rtrim(ltrim(isnull(ft2.id_efr_externa,''))) from ft(nolock) inner join ft2(nolock) on ft2.ft2stamp=ft.ftstamp  where ftstamp = @stamp and ft.site = @site
		if(@clNo<=0 or @clNo>199)
			return 
	
		if(@clEstab<0)
			return 

		if(@orgAbrev='')
			return 
	end

	if(@entidadeAbrev='')
		set @entidadeAbrev = @orgAbrev
	



	-- Carregar informação SNS se existir
	select
		cl.nome
		,codigo = '001'
		,cl.morada
		,cl.local
		,cl.codpost
		,pais = 'PT'
		,cl.ncont
		,abrev = 'SNS'
	into
		#tmp_fe_efr_sns
	from
		 b_utentes cl   (NOLOCK) 
	where 
		cl.no = @clNo and cl.estab = @clEstab


	select
		cl.nome
		,codigo = right(rtrim(cptorg.cptorgstamp),3)
		,cl.morada
		,cl.local
		,cl.codpost
		,pais = 'PT'
		,cl.ncont
		,abrev = cptorg.abrev
	into
		#tmp_fe_efr
	from
		cptorg(nolock)
		INNER JOIN b_utentes cl   (NOLOCK) ON cl.no  = (select no from uf_retornaDadosEntidadeFacturacao (cptorg.abrev,@site,getdate()))    and cl.estab = (select estab from uf_retornaDadosEntidadeFacturacao (cptorg.abrev,@site,getdate()))	
	where
		cptorg.abrev = case when @entidadeAbrev = '' then cptorg.abrev else @entidadeAbrev end
		and  cl.no = @clNo and   cl.estab = @clEstab
		




	select
		nome = ltrim(rtrim(e.nomecomp))
		,alvara = e.alvara
		,nrInfarmed = e.infarmed
		,nif = e.ncont
		,e.morada
		,pais = case
					when e.pais = 'Portugal' then 'PT'
					else 'PT'
					end
		,e.concelho
		,e.codpost
		,capsocial = e.capsocial
		,contacto = isnull(e.telefone,'')
		/* Identificação da Conservatória de Registo Comercial, nr de registo e capital social da entidade emissora da fatura */
		,e.consreg 
		,e.assfarm
		,erpFolder = (select top 1 textvalue from B_Parameters (nolock) where stamp='ADM0000000015')
		,local = e.local
		/* SNS */
		,snsNome = isnull((select nome from #tmp_fe_efr_sns where codigo='001'),'')
		,snsCodigo = '001'
		,snsMorada = isnull((select morada from #tmp_fe_efr_sns where codigo='001'),'')
		,snsCidade = isnull((select local from #tmp_fe_efr_sns where codigo='001'),'')
		,snsCodPost = isnull((select codpost from #tmp_fe_efr_sns where codigo='001'),'')
		,snsPais = 'PT'
		,snsNif = isnull((select ncont from #tmp_fe_efr_sns where codigo='001'),'')
		/* Sicad */
		,sicadNome = isnull((select nome from #tmp_fe_efr where codigo='065'),'')
		,sicadCodigo = '065'
		,sicadMorada = isnull((select morada from #tmp_fe_efr where codigo='065'),'')
		,sicadCidade = isnull((select local from #tmp_fe_efr where codigo='065'),'')
		,sicadCodPost = isnull((select codpost from #tmp_fe_efr where codigo='065'),'')
		,sicadPais = 'PT'
		,sicadNif = isnull((select ncont from #tmp_fe_efr where codigo='065'),'')
		/* Outros */
		,orgNome = isnull((select top 1 nome from #tmp_fe_efr),'')
		,orgCodigo = isnull((select top 1 codigo from #tmp_fe_efr),'')
		,orgMorada = isnull((select top 1 morada from #tmp_fe_efr),'')
		,orgCidade = isnull((select top 1 local from #tmp_fe_efr),'')
		,orgCodPost = isnull((select top 1 codpost from #tmp_fe_efr),'')
		,orgPais = 'PT'
		,orgNif = isnull((select top 1 ncont from #tmp_fe_efr),'')
	from
		empresa e (nolock)
		inner join b_utentes cl (nolock) on cl.no=1 and cl.estab=0
	where
		e.site = @site

	If OBJECT_ID('tempdb.dbo.#tmp_fe_efr') IS NOT NULL
		drop table #tmp_fe_efr;

	If OBJECT_ID('tempdb.dbo.#tmp_fe_efr_sns') IS NOT NULL
		drop table #tmp_fe_efr_sns;

GO
Grant Execute On [up_receituario_fe_dadosEmpresa] to Public
Grant Control On [up_receituario_fe_dadosEmpresa] to Public
GO

