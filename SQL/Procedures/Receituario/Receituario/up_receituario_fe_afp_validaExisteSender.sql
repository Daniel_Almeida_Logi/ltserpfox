/*	
	Valida se o cliente existe
	
	exec up_receituario_fe_afp_validaExisteSender '014303',  0
	exec up_receituario_fe_afp_validaExisteSender '08170',  0
	exec up_receituario_fe_afp_validaExisteSender '08170',  1


*/




SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_fe_afp_validaExisteSender]') IS NOT NULL
	drop procedure up_receituario_fe_afp_validaExisteSender
go

Create procedure [dbo].up_receituario_fe_afp_validaExisteSender
	 @codeInfarmed  VARCHAR(10)
	,@test          bit = 0




/* with encryption */
AS
	SET NOCOUNT ON

	If OBJECT_ID('tempdb.dbo.#tempUtentes') IS NOT NULL
		DROP TABLE #tempUtentes

	set @codeInfarmed	= isnull(@codeInfarmed,'0')
	set @test			= isnull(@test,0)

	declare  @count int = 0

	declare @result bit = 1


	-- se for testes devolve sempre ok
	if(@test=1)
	begin
		select @result as 'result'
		return 
	end


	-- valida se o cliente está bloqueado pela entidade
	Select 
		id
	into 
		#tempUtentes
	From
		b_utentes(nolock) 
	Where 
		inactivo = 0 
		and id !='' 
		and ISNUMERIC(id) = 1


	Select 
		 @count = isnull(count(#tempUtentes.id),0)
	From
		#tempUtentes(nolock) 
	Where 
		convert(int,RTRIM(LTRIM(@codeInfarmed))) = convert(int,#tempUtentes.id)


	

	if(isnull(@count,0)>0)
	begin
		set @result = 1 
	end else 
	begin
		set @result = 0
	end	


	select @result as 'result'


	If OBJECT_ID('tempdb.dbo.#tempUtentes') IS NOT NULL
		DROP TABLE #tempUtentes


GO
Grant Execute On up_receituario_fe_afp_validaExisteSender to Public
Grant Control On up_receituario_fe_afp_validaExisteSender to Public
GO



