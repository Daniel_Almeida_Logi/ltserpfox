/*
	Impressão de verbetes --

	-- exec up_receituario_Verbete 2022, 06,'SNS ARS-ALGARVE', 1, 1, 1, 'Loja 1'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_Verbete]') IS NOT NULL
	drop procedure dbo.up_receituario_Verbete
go

Create procedure dbo.up_receituario_Verbete

@Ano				NUMERIC(4),
@Mes				NUMERIC(2),
@Abrev				VARCHAR(15),
@TipoLote			VARCHAR(2),
@LoteInicial		NUMERIC(9),
@LoteFinal			NUMERIC(9),
@site               varchar(254) = ''

/* with encryption */

AS
SET NOCOUNT ON

	declare @id varchar(10) = ''
	select 
		top 1
		@id = rtrim(ltrim(ISNULL(b_utentes_site.id,'')))
	from 
		b_utentes(nolock)
	inner join 
		cptorg(nolock) on cptorg.u_no = b_utentes.no
	left join 
		b_utentes_site(nolock) on b_utentes_site.no = b_utentes.no and  b_utentes_site.estab = b_utentes.estab
	where 
		cptorg.abrev = rtrim(ltrim(isnull(@Abrev,'xxxx')))
		and b_utentes_site.site in  (select * from up_SplitToTable(@site,','))
	order by 
		b_utentes_site.site asc



	 set @id = ISNULL(@id,'')

	;with
	cte_receitas as (
		select
			ctltrct.ctltrctstamp
			,ctltrct.ano
			,ctltrct.mes
			,ctltrct.cptorgabrev
			,ctltrct.cptplacode
			,lote = case when ctltrct.cptplacode='RS' then 99 else ctltrct.lote  end --numero de lote alterado a pedido do spms
			, ctltrct.tlote, ctltrct.slote, ctltrct.nreceita
			,tlotedescr = case when ctltrct.cptorgabrev = 'SNS' and ctltrct.tlote = 99 then 'Sem Tipificação' 
										when ctltrct.cptorgabrev = 'SNS' and ctltrct.tlote = 98 then 'RCP Sem Sucesso' 
										  when ctltrct.cptorgabrev = 'SNS' and ctltrct.tlote = 97 then 'RSP Com Sucesso' 
											when ctltrct.cptorgabrev = 'SNS' and ctltrct.tlote = 96 then 'RSP Sem Sucesso'
											  when isnull(mcdt,1) = 1 then   tlote.descrMCDT  
								 else 
									cptpla.tlotedescr
								 end  
			,cptpla.design
			,ctltrct.receita
			,tlote.loteId
		from
			ctltrct				(nolock)
			inner join cptpla	(nolock) on ctltrct.cptplacode=cptpla.codigo
			LEFT JOIN tlote (nolock) ON tlote.codigo = ctltrct.tlote
		where
			ano = @ano and mes = @mes
			and tlote = @TipoLote
			and ctltrct.cptorgabrev = @Abrev
			AND (lote BETWEEN @LoteInicial AND @LoteFinal)
	),
 
	cte_facturacao as (
		select
			ft.ftstamp, ft.nmdoc, ft.fno
			,fistamp, u_epvp, qtt, u_ettent1, u_ettent2, u_ltstamp, u_ltstamp2
			,fi.u_generico, fi.pvp4_fee, fi.pic, pvp4 = isnull(fprod.preco_acordo,0)
		from
			fi				(nolock)
			inner join ft	(nolock) on ft.ftstamp=fi.ftstamp
			left join fprod (nolock) on fprod.cnp = fi.ref
		where
			ftano = @Ano and month(fdata) = @Mes
			and (
				(u_ltstamp != '' and u_ettent1 > 0)
				or
				(u_ltstamp2 != '' and u_ettent2 > 0)
			)
			and ft.site in (select * from up_SplitToTable(@site,','))/*= (case when @site = '' Then ft.site else @site end)*/
	)

	SELECT
		ano
		,mes
		,cptorgabrev
		,cptplacode
		,design
			
		,slote
		,tlote
		,lote
		,nmdoc
		,fno
			
		,etiquetas	= sum(qtt)
		,pvp		= sum(pvp)
		,utente		= sum(pvp-comp)
		,comp		= sum(comp)
		,fee		= dbo.uf_calcFee(sum(pvp4_fee))
		,nreceita
		,tlotedescr
		,receita
		,id = @id
	FROM (
		SELECT distinct
			a.ano
			,a.mes
			,a.cptorgabrev
			,a.cptplacode
			,a.design
			
			,a.slote
			,tlote = a.loteId
			,a.lote
			,fact.nmdoc
			,fact.fno
			
			,qtt
			,pvp		= u_epvp*qtt
			,comp		= case when a.ctltrctstamp=fact.u_ltstamp then u_ettent1 else u_ettent2 end
			,pvp4_fee = case  
							when a.cptorgabrev = 'SNS' and a.ano = 2017 and a.mes = 1 -- condição necessário pq no mes de janeiro o pvp4 não estava a ser guardado na fi
								then case 
										when u_generico=1 and fact.pvp4 > 0 and u_epvp <= fact.pvp4 then 0.35 * qtt
										when u_generico=0 and fact.pvp4 > 0 and pic <= fact.pvp4 then 0.35 * qtt
										else 0
										end
							when a.cptorgabrev = 'SNS'
								then fact.pvp4_fee
							else
								0 
							end
			,a.nreceita
			,a.tlotedescr
			
			,fistamp
			, a.receita
		FROM
			cte_receitas a
			inner join cte_facturacao fact	ON a.ctltrctstamp = fact.u_ltstamp or a.ctltrctstamp = fact.u_ltstamp2
	) w
	where
		comp > 0
	group by
		ano, mes
		,cptorgabrev, cptplacode, design
		,slote, tlote, lote, nmdoc, fno
		,nreceita, tlotedescr, receita
	ORDER BY
		cptorgabrev, tlote, lote, nreceita

Go
Grant Execute on dbo.up_receituario_Verbete to Public
Grant Control on dbo.up_receituario_Verbete to Public
Go