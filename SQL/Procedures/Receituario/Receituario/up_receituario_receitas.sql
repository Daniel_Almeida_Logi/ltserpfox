/* Receitas

 exec up_receituario_receitas '2012','8', 'sns', '10', 1, 'Loja 1'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_receitas]') IS NOT NULL
	drop procedure dbo.up_receituario_receitas
go

Create procedure dbo.up_receituario_receitas

@Ano		NUMERIC(4)
,@Mes		NUMERIC(2)
,@abrev		varchar(15)
,@tlote		varchar(2)
,@lote		numeric(10)
,@site      varchar(254) = ''

/* with encryption */
AS
SET NOCOUNT ON

SELECT Distinct
	nreceita
from
	ctltrct (nolock)
where
	cptorgabrev	= @abrev
	and ano		= @Ano
	and mes		= @mes
	and tlote	= @tlote
	and lote	= @lote
	and site	in (select * from up_SplitToTable(@site,','))/*= @site*/
order by
	nreceita

Go
Grant Execute on dbo.up_receituario_receitas to Public
Grant Control on dbo.up_receituario_receitas to Public
Go
