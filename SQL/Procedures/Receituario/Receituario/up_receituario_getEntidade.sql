/* Retorna informacao da entidade comparticipadora

	 exec up_receituario_getEntidade 'SNS'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_getEntidade]') IS NOT NULL
	drop procedure dbo.up_receituario_getEntidade
go


create procedure dbo.up_receituario_getEntidade

@abrev varchar(10)


/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

		
	select 
		cptorg.cptorgstamp,
		b_utentes.nome,
		b_utentes.no,
		b_utentes.estab,
		desconto = isnull(b_utentes.desconto,0)
	from 
		cptorg(nolock)
	inner join 
		b_utentes(nolock)
	on b_utentes.no = cptorg.u_no and b_utentes.estab = 0
	where 
		abrev=@abrev
		and cptorg.inactivo = 0
		and b_utentes.inactivo = 0


GO
Grant Execute on dbo.up_receituario_getEntidade to Public
Grant Execute on dbo.up_receituario_getEntidade to Public
go