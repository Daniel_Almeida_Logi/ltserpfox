/* 
	Apenas envia para os id:

	066 - Dignitude
    077 - Munic�pio de �gueda
    083 - Associa��o Dignitude - Vacina��o SNS Local
    014 - Savida





	select *from ext_esb_doc where test = 1 and tipoDoc = 2 order by ousrdata desc

	update  ext_esb_doc set docNr='72-6', docdate='2020-11-23', ext_ where stamp='17f0867c-2f3f-46f1-9a19-6705ce666b' and test = 1

	update ext_esb_doc set test = 1 where stamp='7eab695e-ce92-4f23-917b-bfa70d52d7'

	select * from ext_esb_doc(nolock) where test = 0 and tokenConfirm = ''  and id_ext_efr = '066' and send=0
	order by ousrdata desc

	select *from  ext_esb_doc(nolock) where stamp='04d59fd0-a533-48cf-ac18-b45d3d4d41'

	update  ext_esb_doc set test =1  where stamp='04d59fd0-a533-48cf-ac18-b45d3d4d41'

	exec up_receituario_fe_assoc_docFaturacao  2022,02,1


	select * from b_parameters
*/




SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_fe_assoc_docFaturacao]') IS NOT NULL
	drop procedure up_receituario_fe_assoc_docFaturacao
go

CREATE procedure [dbo].up_receituario_fe_assoc_docFaturacao
	@year int
	,@month int 
	,@docType int = 1

/* with encryption */

AS
SET NOCOUNT ON
	

	/* get previous month */
	if(@docType=1)
	begin 
		if(@month=1)
		begin
			set @month = 12
			set @year = @year - 1
		end
		else  
		begin
			set @month = @month - 1
		end
	end
	 
	
	select 
	

		[stamp]
      ,"codInfarmed" = [id_ext_pharmacy]
      ,[year]
      ,[month]
      ,[docNr]
      ,[docDate]
      ,[qtLots]
      ,[qtPrescriptions]
      ,[qtProducts]
      ,[totalPVP]
      ,[totalPVU]
      ,[totalEFR]
      ,[totalSNS]
      ,[totalOthers]
	  ,[totalDiabetesProtocol]
      ,[ousrdata]
      ,"pharmacyName" = [name_ext_pharmacy]
      ,"pharmacyVat" = [vatNr_ext_pharmacy]
	  ,"pharmacyAddress" = address_ext_pharmacy
	  ,"pharmacyPostalCode" = postal_ext_pharmacy
	  ,"pharmacyRegistrationName" = corporateRegistrationName_ext_pharmacy
      ,"efrName" = [name_ext_entity]
      ,"efrVat" = [vatNr_ext_entity]
	  ,"addressEfr" = [address_ext_entity]
	  ,"efrIdAfp" = id_ext_efr
	  ,"efrIdAnf" = code_ext_entity
      ,[tipoDoc]
      ,[invoicePdf]
      ,[invoiceXml]
	  ,oDocNr
	  ,oDocDate
	  ,companyId
	  ,name_ext_pharmacy
	  ,app_ext_efs
	
	from 
		ext_esb_doc(nolock) 
	where 
		(
			send=0 -- se ainda nao foi enviada
			or
			(
				send=1 and invalid = 1
			) -- se foi enviada com erro
		)
		and test = 0
		and ext_esb_doc.year = @year and ext_esb_doc.month = @month
		and id_ext_efr in ('066','077','083','097','014','139')
		and  CONVERT(VARCHAR(MAX), invoiceXml)  !=''
		and ext_esb_doc.shouldSend = 1
	order by id_ext_efr asc



GO
Grant Execute On up_receituario_fe_assoc_docFaturacao to Public
Grant Control On up_receituario_fe_assoc_docFaturacao to Public
GO





