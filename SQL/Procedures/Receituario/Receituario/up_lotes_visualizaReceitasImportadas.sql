/* 
	SP para o painel de visualização das receitas importadas dos Postos
*/

if OBJECT_ID('[dbo].[up_lotes_visualizaReceitasImportadas]') IS NOT NULL
	drop procedure [up_lotes_visualizaReceitasImportadas]
go
   
create PROCEDURE [dbo].[up_lotes_visualizaReceitasImportadas]
	@ano numeric(4),
	@mes numeric(2),
	@dia numeric(2),
	@entidade varchar(60),
	@posto	varchar(60)
/* with encryption */
AS
begin
	
	select 
		ctltrctstamp,
		cptorgabrev,
		lote,
		tlote, 
		slote, 
		nreceita, 
		ano, 
		mes, 
		fechado, 
		u_fistamp as facturado,
		posto, 
		lote_orig, 
		no_orig
	from ctltrct (nolock)
	inner join ft (nolock) on ft.u_ltstamp=ctltrct.ctltrctstamp or ft.u_ltstamp2=ctltrct.ctltrctstamp
	where
		ano=@ano
		and mes=@mes
		and posto!=''
		and posto = case when @posto='TODOS' or @posto='' then posto else @posto end 
		and cptorgabrev = case when @entidade='TODAS' then cptorgabrev else @entidade end
		and DAY(ft.fdata) = case when @dia=0 then day(ft.fdata) else @dia end
	order by 
		ano, mes, cptorgabrev, slote, tlote, lote, nreceita
end

Go

Grant Execute On dbo.[up_lotes_visualizaReceitasImportadas] to Public
Grant control On dbo.[up_lotes_visualizaReceitasImportadas] to Public
go
