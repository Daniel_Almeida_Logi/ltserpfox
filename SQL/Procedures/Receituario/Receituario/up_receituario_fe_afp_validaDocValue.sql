/*	
	Valida se o valor da nota é menor que o total da factura

	exec up_receituario_fe_afp_validaDocValue   '00302', 'F 1/1675', 104.51, '0';
*/




SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_fe_afp_validaDocValue]') IS NOT NULL
	drop procedure up_receituario_fe_afp_validaDocValue
go

Create procedure [dbo].up_receituario_fe_afp_validaDocValue
	@infarmed	    VARCHAR(10)
	,@nrDocOriginal VARCHAR(20)
	,@totalPvp      numeric(11,3)
	,@test          bit = 0


/* with encryption */
AS
	SET NOCOUNT ON

	declare @shouldIgnore  bit = 0
	select top 1 @shouldIgnore=bool from B_Parameters(nolock) where stamp='ADM0000000360'
	set @shouldIgnore = ISNULL(@shouldIgnore,0)

	set @test = isnull(@test,0)

	if(@shouldIgnore=1)
	begin
		select "val" = 1
		return 
	end


	Select 
		"val" = count(stamp)
	From
		ext_esb_doc(nolock)
	Where 
		invalid=0 and
		convert(int,RTRIM(LTRIM(id_ext_pharmacy))) = convert(int,@infarmed) and
		RTRIM(LTRIM(docNr)) = @nrDocOriginal and
		test = @test
		
	

GO
Grant Execute On up_receituario_fe_afp_validaDocValue to Public
Grant Control On up_receituario_fe_afp_validaDocValue to Public
GO