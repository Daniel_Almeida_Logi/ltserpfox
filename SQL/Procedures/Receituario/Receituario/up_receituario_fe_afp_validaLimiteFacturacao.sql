/*	
	Valida se já ultrapssou o limite de facturação da a entidade

	exec up_receituario_fe_afp_validaLimiteFacturacao '2021','11',1,'014',0
	

*/




SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_fe_afp_validaLimiteFacturacao]') IS NOT NULL
	drop procedure [up_receituario_fe_afp_validaLimiteFacturacao]
go

Create procedure [dbo].[up_receituario_fe_afp_validaLimiteFacturacao]
	@ano            NUMERIC(4)
	,@mes           NUMERIC(4)
	,@tipodoc	    VARCHAR(10)
	,@cptorgId      VARCHAR(25)
	,@test          bit = 0



/* with encryption */
AS
	SET NOCOUNT ON

	set @test = isnull(@test,0)
	declare @dayLimiteOrg int = 10
	declare @result       int = 0

	set @cptorgId  =  RIGHT('000'+ISNULL(@cptorgId,''),3)
	set @cptorgId  = 'ADM' + rtrim(ltrim(@cptorgId))



	select
		top 1 @dayLimiteOrg = isnull(cptorg.diaLimiteFact,-1)
	from
		cptorg(nolock)
	where 
		cptorgstamp = @cptorgId


	declare @actualDay int = 0
	declare @lastDay   int = 31



	SELECT @actualDay = DAY(GETDATE()) 

	select @lastDay   = DAY(EOMONTH(GETDATE()))



	if(@actualDay=@lastDay or @actualDay<=@dayLimiteOrg)	
		set @result = 1
	else
		set @result = 0
	


	if(@test=1 or @tipodoc!=1 or @dayLimiteOrg=-1)
	begin
		set @result = 1
	end

	select @result as result

GO
Grant Execute On [up_receituario_fe_afp_validaLimiteFacturacao] to Public
Grant Control On [up_receituario_fe_afp_validaLimiteFacturacao] to Public
GO


