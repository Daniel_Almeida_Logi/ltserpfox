/*	
	Valida se o documento existe e está validado
	up_receituario_fe_afp_validaDoc '2021','99001','780-01',1

	exec up_receituario_fe_afp_validaDoc 2023, '19445', 'FME 1FME1/44', '0'




*/




SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_fe_afp_validaDoc]') IS NOT NULL
	drop procedure [up_receituario_fe_afp_validaDoc]
go

Create procedure [dbo].[up_receituario_fe_afp_validaDoc]
	@ano            NUMERIC(4)
	,@infarmed	    VARCHAR(10)
	,@nrDoc         VARCHAR(100)
	,@test          bit = 0



/* with encryption */
AS
	SET NOCOUNT ON

	set @test = isnull(@test,0)

	Select 
		"val" = count(stamp)
	From
		ext_esb_doc(nolock)
	Where 
		convert(int,RTRIM(LTRIM(id_ext_pharmacy))) = convert(int,@infarmed) and
		RTRIM(LTRIM(docNr)) = @nrDoc and
		[year] = @ano  and
		test = @test


GO
Grant Execute On [up_receituario_fe_afp_validaDoc] to Public
Grant Control On [up_receituario_fe_afp_validaDoc] to Public
GO




