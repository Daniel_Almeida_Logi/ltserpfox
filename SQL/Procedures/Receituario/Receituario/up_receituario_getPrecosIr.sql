/* Ver Preços do Produto para calcular comparticipação (Manipulação Valores Inserção de Receita)

	 exec up_receituario_getPrecosIr 0, 0, '8168617', '1'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_getPrecosIr]') IS NOT NULL
	drop procedure dbo.up_receituario_getPrecosIr
go

create procedure dbo.up_receituario_getPrecosIr

@pvp numeric(13,3),
@pref numeric(13,3),
@ref varchar (120),
@site_nr tinyint

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

SELECT 
	 cnp		=	fprod.cnp
	,ppvp		=	case when fprod.pvporig=0 then null else fprod.pvporig end --a.epreco
	,pref		=	case when @pref = 0 then null 
									--case 
									--			when fprod.pref=0 then null 
									--			else fprod.pref 
									--		end --b.epreco
						else @pref
					end
	,ppens		=	case when @pref = 0 then null 
											--	when fprod.pref=0 then null 
											--	else fprod.pref 
											--end --c.epreco
						else @pref
					end
	,pesp		=	null --d.epreco
	,pst1		=	case when @pvp = 0 then IsNull(st.epv1,0)
						else @pvp 
					end
FROM 
	fprod (nolock)
	left join st (nolock)		on st.ref = fprod.cnp and st.site_nr = @site_nr
WHERE
	fprod.cnp=@ref


GO
Grant Execute on dbo.up_receituario_getPrecosIr to Public
Grant Execute on dbo.up_receituario_getPrecosIr to Public
go
