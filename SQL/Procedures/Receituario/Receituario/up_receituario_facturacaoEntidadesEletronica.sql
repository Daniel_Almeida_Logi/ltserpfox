/* 
	valida se tem faturacao eletronica

	exec up_receituario_facturacaoEntidadesEletronica 'PEMHSNS', 'Loja 1'
	exec up_receituario_facturacaoEntidadesEletronica 'SNS', 'Loja 1'
	exec up_receituario_facturacaoEntidadesEletronica 'VacinasSNS', 'Loja 1'



*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_receituario_facturacaoEntidadesEletronica]') IS NOT NULL
	drop procedure dbo.up_receituario_facturacaoEntidadesEletronica
go

create procedure dbo.up_receituario_facturacaoEntidadesEletronica

@abrev	varchar(15),
@site	varchar(15)


/* WITH ENCRYPTION */
AS

	select 
		top 1 
			"temFactEl" = e_fact 
			,"entidadeID" = (select no from uf_retornaDadosEntidadeFacturacao (abrev,@site,getdate()))
			,"entidadeDep" = (select estab from uf_retornaDadosEntidadeFacturacao (abrev,@site,getdate()))
			,"isMcdt" = isnull(mcdt,0)
			,"isPemH" = ISNULL(pemh,0)
			,"prestacao" = isnull(prestacao,0)
	from
		cptorg (nolock)
	where
		abrev = @abrev

	
Go
Grant Execute on dbo.up_receituario_facturacaoEntidadesEletronica to Public
Grant Control on dbo.up_receituario_facturacaoEntidadesEletronica to Public
Go
