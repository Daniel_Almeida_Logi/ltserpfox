/* 
	Emissão automática de facturação às entidades 

	exec up_receituario_facturacaoEntidades 2024, 04,'Loja 1','Loja 1'

*/
if OBJECT_ID('[dbo].[up_receituario_facturacaoEntidades]') IS NOT NULL
	drop procedure dbo.up_receituario_facturacaoEntidades
go

create procedure dbo.up_receituario_facturacaoEntidades

@ano	      NUMERIC(4),
@mes	      NUMERIC(2),
@site         VARCHAR(254),
@siteActual   VARCHAR(254)

/* WITH ENCRYPTION */
AS

	SET FMTONLY OFF /* temporario ate alterar software -> deixar de usar o set ftmonly on */

	IF object_id('tempdb.dbo.#temp_receitas') IS NOT NULL
		DROP TABLE #temp_receitas;
	IF object_id('tempdb.dbo.#temp_facturacao') IS NOT NULL
		DROP TABLE #temp_facturacao;
	IF object_id('tempdb.dbo.#temp_rec_fat') IS NOT NULL
		DROP TABLE #temp_rec_fat;

	/*
		Receitas
	*/
	SELECT
		ctltrctstamp
		,ctltrct.cptorgstamp
		,ano
		,mes
		,ctltrct.cptorgabrev
		,lote
		,tlote
		,slote
		,nreceita
		,fechado
		,u_fistamp
		,descricao		= CASE WHEN ISNULL(mcdt,0) = 0 THEN  tlote.descricao ELSE tlote.descrMCDT END
		,b_utentes.no
		,b_utentes.estab
		,b_utentes.nome
		,tlote.loteId
		,mcdt			= ISNULL(mcdt,0)
		,prestacao		= ISNULL(prestacao,0) 
		,cptorg.entPorLoja
		,cptorg.entPorLojaAposData
	INTO
		#temp_receitas	
	FROM
		ctltrct					   (NOLOCK)
		INNER JOIN tlote		   (NOLOCK) ON tlote.codigo=ctltrct.tlote
		INNER JOIN cptorg		   (NOLOCK) ON ctltrct.cptorgstamp = cptorg.cptorgstamp
		INNER JOIN b_utentes	   (NOLOCK) ON b_utentes.no  = (select no from uf_retornaDadosEntidadeFacturacao (cptorgabrev,@siteActual ,getdate()))	and b_utentes.estab = (select estab from uf_retornaDadosEntidadeFacturacao (cptorgabrev,@siteActual ,getdate()))
	WHERE
		ano = @ano 
		AND mes = @mes
		AND fechado = 1
		AND u_fistamp = ''
	



	

	/*
		Faturacao
	*/
	SELECT
		ft.ftstamp
		,fistamp
		,u_epvp
		,qtt
		,epv
		,u_ettent1
		,u_ettent2
		,u_ltstamp
		,u_ltstamp2
		,iva
		,fi.u_generico
		,fi.pvp4_fee
		,fi.pic
		,pvp4		= ISNULL(fprod.preco_acordo,0)
	INTO
		#temp_facturacao		
	FROM
		fi				(NOLOCK)
		INNER JOIN ft	(NOLOCK) ON ft.ftstamp=fi.ftstamp
		LEFT JOIN fprod (NOLOCK) ON fprod.cnp = fi.ref
	WHERE
		ftano = @ano 
		AND month(fdata) = @mes
		AND (
				(u_ltstamp != '' AND u_ettent1 > 0)
				OR
				(u_ltstamp2 != '' AND u_ettent2 > 0)
			)
		AND ft.site IN (SELECT * FROM up_SplitToTable(@site,','))

	/*	
		Receitas e Faturacao
	*/
	SELECT DISTINCT
		a.ano
		,a.mes
		,entidades		= a.cptorgabrev
		,Sel			= CAST(0 AS BIT)
		,cptorgstamp	= a.cptorgstamp
		,serie			= a.slote
		,tipos			= a.loteId
		,series			= a.descricao
		,lotes			= a.lote			
		,iva			= fact.iva
		,tabiva			= (SELECT top 1 codigo FROM taxasiva (NOLOCK) WHERE taxa=fact.iva)
		,pvp			= fact.u_epvp * fact.qtt
		,comp			= CASE WHEN a.ctltrctstamp=fact.u_ltstamp THEN u_ettent1 ELSE u_ettent2 END
		,pvp4_fee		= CASE WHEN a.cptorgabrev = 'SNS' AND a.ano = 2017 AND a.mes = 1 
							THEN CASE 
									WHEN u_generico=1 AND fact.pvp4 > 0 AND u_epvp <= fact.pvp4 THEN 0.35 * qtt
									WHEN u_generico=0 AND fact.pvp4 > 0 AND pic <= fact.pvp4    THEN 0.35 * qtt
									ELSE 0
									END
						WHEN a.cptorgabrev = 'SNS'
							THEN fact.pvp4_fee
						ELSE
							0 
						END
		,no				= a.no
		,estab			= a.estab
		,nome			= a.nome
		,ctltrctstamp
		,ftstamp
		,fistamp 
		,qtt
		,tlote
		,mcdt
		,prestacao
	INTO
		#temp_rec_fat
	FROM
		#temp_receitas a
		INNER JOIN #temp_facturacao fact	ON a.ctltrctstamp = fact.u_ltstamp OR a.ctltrctstamp = fact.u_ltstamp2
	
	--Calcula o número de receitas, pois se vier varias receitas na mesma fatura com ivas diferentes ele multiplica o valor do numero
	-- de receitas

	--print @nrReceitas

	/*
		ResultSet Final
	*/
	SELECT
		ano
		,mes
		,entidades
		,sel
		,cptorgstamp
		,serie
		,tipos
		,series 
		,receitas		= COUNT(distinct ctltrctstamp)
		,lotes			= COUNT(DISTINCT lotes)
		,embalagens		= SUM(qtt)
		,iva
		,tabiva
		,pvp			= SUM(pvp)
		,comp			= SUM(comp)
		,utente			= CONVERT(NUMERIC(13,3),0.00)
		,fee			=  dbo.uf_calcFee(SUM(pvp4_fee))
		,compWithFee	= SUM(comp+ dbo.uf_calcFee(pvp4_fee))
		,evalor			= CAST(00000000000.000 AS NUMERIC(15,3))
		,no
		,estab
		,nome

		,ref			= '9999999'
		,design			= LEFT('Tipo ' + tipos + ' - ' + CONVERT(VARCHAR(80),RTRIM(LTRIM(series))),99)
		,familia		= '99'
		,fistamp		= LEFT(NEWID(),24)
		,entidadeInvert = CONVERT(bit,0)
		,tlote
		,mcdt
		,prestacao
	FROM (
		SELECT * FROM #temp_rec_fat
	) w
	WHERE
		comp > 0
	GROUP BY
		ano
		,mes
		,entidades
		,sel
		,cptorgstamp
		,serie
		,tipos
		,series
		,iva
		,tabiva
		,no
		,estab
		,nome
		,tlote
		,mcdt
		,prestacao

	UNION ALL

	SELECT
		ano
		,mes
		,entidades
		,sel
		,cptorgstamp
		,serie			= 0
		,tipos			= 0
		,series			= 'Remuneração Específica'
		,receitas		= 0
		,lotes			= 0
		,embalagens		= 0
		,iva			= 6
		,tabiva			= (SELECT TOP 1 codigo FROM taxasiva (NOLOCK) WHERE taxa=6 ORDER BY codigo)
		,pvp			= 0
		,comp			= dbo.uf_calcFee(sum(pvp4_fee))
		,utente			= 0
		,fee			= 0
		,compWithFee	= 0
		,evalor			= CAST(00000000000.000 AS NUMERIC(15,3))
		,no
		,estab
		,nome
		,ref			= '9999999'
		,design			= 'Remuneração Específica'
		,familia		= '99'
		,fistamp		= 'fee'
		,entidadeInvert = CONVERT(bit,0)
		,tlote			= ''
		,mcdt			= 0
		,prestacao		= 0
	FROM (
		SELECT * FROM #temp_rec_fat
	) w
	WHERE
		pvp4_fee > 0 
		AND comp > 0
	GROUP BY
		ano
		,mes
		,entidades
		,sel
		,cptorgstamp
		,no
		,estab
		,nome
		,mcdt
		,prestacao
	ORDER BY
		w.ano
		,w.mes
		,w.entidades
		,w.tipos
	

	IF object_id('tempdb.dbo.#temp_receitas') IS NOT NULL
		DROP TABLE #temp_receitas;
	IF object_id('tempdb.dbo.#temp_facturacao') IS NOT NULL
		DROP TABLE #temp_facturacao;
	IF object_id('tempdb.dbo.#temp_rec_fat') IS NOT NULL
		DROP TABLE #temp_rec_fat;
		   
	
Go
Grant Execute on dbo.up_receituario_facturacaoEntidades to Public
Grant Control on dbo.up_receituario_facturacaoEntidades to Public
Go
