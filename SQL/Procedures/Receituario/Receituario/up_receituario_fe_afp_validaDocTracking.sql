/*	
	Valida se o documento existe e está validado pelo stamp
*/




SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_fe_afp_validaDocTracking]') IS NOT NULL
	drop procedure up_receituario_fe_afp_validaDocTracking
go

Create procedure [dbo].up_receituario_fe_afp_validaDocTracking
	@stamp varchar(255)



/* with encryption */
AS
	SET NOCOUNT ON

	Select 
		"val" = count(stamp)
	From
		ext_esb_doc (nolock)
	Where 
		stamp = @stamp and
		error_export = 0
		


GO
Grant Execute On up_receituario_fe_afp_validaDocTracking to Public
Grant Control On up_receituario_fe_afp_validaDocTracking to Public
GO