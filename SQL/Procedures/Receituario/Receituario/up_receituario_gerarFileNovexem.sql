/*
	SP para gerar ficheiro XML de detalhe da Novexem
	> exec dbo.up_receituario_gerarFileNovexem 2012,10,'lilly'
*/
if OBJECT_ID('[dbo].[up_receituario_gerarFileNovexem]') IS NOT NULL
	drop procedure dbo.up_receituario_gerarFileNovexem
go
	
	
create procedure dbo.up_receituario_gerarFileNovexem
	@ano numeric(4,0),
	@mes numeric(2,0),
	@cptorgabrev varchar(15)
/* with encryption */
as
	begin
		set language portuguese
		set nocount on 
		
		declare 
			@stamp varchar(36)
		
		/* Ler o ultimo valor da CHAVE */
		select @stamp = (select stamp from b_ultReg where tabela='lilly')
		/* Se for NULL ou vazio usar o inicial */
		select @stamp = isnull(@stamp,'41db6bd6-1bfd-457e-8763-44c1ba5dfaf7')
			
			/* Manipular com REPLACE e RIGHT 10 e LEFT 22 */
		
		/* Campo VIRTUAL que permite dar nome no cursor PHC, apenas para isso. */
		select Dados = convert(text,(convert(varchar(max),(
		
			select
				'PharmacyID'											= (select top 1 infarmed from empresa /*where site = @site*/)
				,'PharmacyKey'											= @stamp
				,(
					select
						(
							select
								'Timestamp'								= convert(varchar,a.fdata + a.ousrhora,120)
								,'EANCode'								= convert(numeric(8,0),c.ref)
								,'CoPaycode'							= c.u_codemb
								,'PrescriptionNumber'					= convert(numeric(25,0),case when b.u_receita='' then '0000000000000000000000000' else b.u_receita end)
								,'PVPValue'								= str(convert(numeric(5,2),c.u_epvp),5,2)
								,'CoPayValue'							= str(convert(numeric(5,2),c.u_ettent1),5,2)
								,'StatePayValue'						= str(convert(numeric(5,2),c.u_ettent2),5,2)
								,'Regime'								= 
																			case
																				when d.cptplacode in ('LL','LN') then 10
																				when d.cptplacode in ('LM','LO') then 15
																				else 10
																			end
								
							from ft (nolock)				as a
							inner join ft2 (nolock)			as b		on a.ftstamp=b.ft2stamp
							inner join fi (nolock)			as c		on a.ftstamp=c.ftstamp
							inner join ctltrct (nolock)		as d		on (a.u_ltstamp=d.ctltrctstamp or a.u_ltstamp2=d.ctltrctstamp)
							inner join fprod (nolock)		as e		on c.ref=e.cnp
							where
								d.ano									= @ano
								and d.mes								= @mes
								and d.cptorgabrev						= @cptorgabrev
								and e.grupo in (
									'OK',							/* Embalagens ZYPREXA */
									'OL',
									'OM',
									'OQ',
									'OR',
									'OS',
									'OT',							
									'OP'							/* Embalagens STRATTERA */
								)
							for xml path('Sale'), type
						)
					for xml path('SaleRecords'), type
				)
			for xml path(''), root('SaleReport'), type
		
		))))
	end
go
	
grant execute on dbo.up_receituario_gerarFileNovexem to public 			
grant execute on dbo.up_receituario_gerarFileNovexem to public 
go