SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
	exec up_obter_Utente_No_from_Plano 'a'
*/
-- =============================================
-- Author:		Jos� Moreira
-- Create date: 2022-11-07
-- Description: Returns user information from the plan code
-- =============================================


if OBJECT_ID('[dbo].[up_obter_Utente_No_from_Plano]') IS NOT NULL
	drop procedure dbo.up_obter_Utente_No_from_Plano
go


CREATE PROCEDURE [dbo].[up_obter_Utente_No_from_Plano]  
	@id_plano varchar(200)
AS
BEGIN
	SELECT
		isnull(b_utentes.no, 0)as no,  
		isnull(b_utentes.estab, 0) as estab, 
		isnull(b_utentes.nome, 0) as nome, 
		isnull(b_utentes.morada, 0)morada, 
		isnull(b_utentes.local, 0)local, 
		isnull(b_utentes.codpost, 0)codpost, 
		isnull(b_utentes.ncont, 0)ncont, 
		isnull(b_utentes.telefone,0)telefone, 
		isnull(b_utentes.zona, 0)zona, 
		isnull(b_utentes.tipo, 0)tipo, 
		isnull(b_utentes.email, 0)email, 
		isnull(b_utentes.cativa, 0)cativa, 
		isnull(b_utentes.perccativa, 0)perccativa
	from 
		cptpla		(nolock)
	inner join 
		cptorg		(nolock)		on		cptorg.cptorgstamp = cptpla.cptorgstamp
	left join 
		b_utentes	(nolock)		on		b_utentes.no = cptorg.u_no  and estab = 0
	WHERE 
		cptpla.codigo = @id_plano
END

GO
Grant Execute on dbo.up_obter_Utente_No_from_Plano to Public
Grant control on dbo.up_obter_Utente_No_from_Plano to Public
Go