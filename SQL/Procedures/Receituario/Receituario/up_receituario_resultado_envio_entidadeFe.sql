/* 
	Devolve os resultados do ultimo envio da documento de FE
	up_receituario_resultado_envio_entidadeFe 'SNS',2018,7,1, 'Loja 1'
*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_resultado_envio_entidadeFe]') IS NOT NULL
	drop procedure up_receituario_resultado_envio_entidadeFe
go

CREATE procedure [dbo].up_receituario_resultado_envio_entidadeFe
    @abrev varchar(18)
	,@year int
	,@month int 
	,@docType int = 1
	,@site varchar(18)


/* with encryption */

AS
SET NOCOUNT ON
	
	IF object_id('tempdb.dbo.#tempFactCom') IS NOT NULL
		DROP TABLE #tempFactCom;


	select 
		 distinct top 10 faturacao_eletronica_med.id, faturacao_eletronica_med.data, aceite	
	into
		#tempFactCom
	from 
		faturacao_eletronica_med(nolock)
	left join faturacao_eletronica_med_d(nolock) on faturacao_eletronica_med_d.id_faturacao_eletronica_med = faturacao_eletronica_med.id
	inner join ft(nolock) on ft.ftstamp = faturacao_eletronica_med.ftstamp
	inner join ft2(nolock) on ft2.ft2stamp = ft.ftstamp
	where 
		tipodoc = @docType 
		and ft2.id_efr_externa = @abrev 
		and faturacao_eletronica_med.mes = @month
		and faturacao_eletronica_med.ano = @year
		and ft.site = @site
	order by 
		faturacao_eletronica_med.data desc


	if EXISTS(select isnull(aceite,0) from #tempFactCom where aceite = 1)
	begin
		select 
			top 1
			"200"                 as response_code,
			"Enviado com sucesso" as response_descr
		from 
			#tempFactCom
		where
			aceite = 1
	end
	else begin
		select 
			 rtrim(ltrim(response_code)) as response_code,
			 rtrim(ltrim(response_descr)) as   response_descr 
		from faturacao_eletronica_med_d(nolock)
		inner join #tempFactCom on #tempFactCom.id = faturacao_eletronica_med_d.id_faturacao_eletronica_med
	end
		

	

	IF object_id('tempdb.dbo.#tempFactCom') IS NOT NULL
		DROP TABLE #tempFactCom;


GO
Grant Execute On up_receituario_resultado_envio_entidadeFe to Public
Grant Control On up_receituario_resultado_envio_entidadeFe to Public
GO