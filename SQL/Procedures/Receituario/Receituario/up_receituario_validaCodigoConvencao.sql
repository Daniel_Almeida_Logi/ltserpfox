/*	
	Valida se o codigo de convenção se encontra preenchido

	exec up_receituario_validaCodigoConvencao 'SNS ARS-CENTRO','Loja 1'


*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_validaCodigoConvencao]') IS NOT NULL
	drop procedure up_receituario_validaCodigoConvencao
go

Create procedure [dbo].up_receituario_validaCodigoConvencao
	@abrev  varchar(20),
	@site varchar(20)
	



/* with encryption */
AS
	SET NOCOUNT ON

	set @site   = ltrim(ISNULL(@site,''))
	set @abrev  = rtrim(ISNULL(@abrev,''))



	Select 
		"id" = rtrim(ltrim(isnull(b_utentes_site.id,'')))
	From
		cptorg(nolock)
	left join 
		b_utentes (nolock) on b_utentes.no = cptorg.u_no
	left join 
		b_utentes_site(nolock) on b_utentes.no = b_utentes_site.no and b_utentes.estab = b_utentes_site.estab
	Where 
		b_utentes_site.site = @site
		and cptorg.abrev = @abrev
		

GO
Grant Execute On up_receituario_validaCodigoConvencao to Public
Grant Control On up_receituario_validaCodigoConvencao to Public
GO


