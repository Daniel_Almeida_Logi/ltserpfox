/*
	exec up_receituario_validaCompartBonus  '1', '14'
	

*/

if OBJECT_ID('[dbo].[up_receituario_validaCompartBonus]') IS NOT NULL
	drop procedure dbo.up_receituario_validaCompartBonus 
go





create procedure dbo.up_receituario_validaCompartBonus 
	@tipo				varchar(100)
	,@idExt			    varchar(100)

/* with encryption */
as


	select 
		top 1
		isnull(permiteReserva,1) as permiteReserva,
		isnull(permiteSuspensa,1) as permiteSuspensa
	from
		cptvalbonus(nolock)
	where
		tipo = isnull(@tipo,0) and
		idExt = @idExt
	order by 
		data_alt
		desc



go
grant execute on dbo.up_receituario_validaCompartBonus  to public		
grant control on dbo.up_receituario_validaCompartBonus  to public
go