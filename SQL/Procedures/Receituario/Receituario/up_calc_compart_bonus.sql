/*

	Author:		Daniel Almeida
	Create date: 01-04-2023
	Description: Retorna o valor de comparticipação BAS


	EXEC up_calc_compart_bonus 4.6956, 4.3344, 50.00
*/


IF OBJECT_ID('[dbo].[up_calc_compart_bonus]') IS NOT NULL
    DROP procedure dbo.up_calc_compart_bonus
GO

CREATE procedure dbo.up_calc_compart_bonus
	@pagEntAntesBonus NUMERIC(14,5),
	@pagUtAtensBonus NUMERIC(14,5),
	@compart NUMERIC(14,5)

AS
	declare @precisao int = 5
	select @precisao = numValue from B_Parameters(nolock) WHERE stamp='ADM0000000373'
	
	set @precisao = isnull(@precisao,5)

	--calcular compart bas sem arrendondamentos a dinheiro
	set @pagUtAtensBonus = @pagUtAtensBonus*@compart/100	
	select @pagUtAtensBonus= Round(@pagUtAtensBonus,@precisao) 

	--valor final em Dinheio
	SELECT round(@pagEntAntesBonus + @pagUtAtensBonus,2) as valor

GO
GRANT EXECUTE on dbo.up_calc_compart_bonus TO PUBLIC
GRANT Control on dbo.up_calc_compart_bonus TO PUBLIC
GO


