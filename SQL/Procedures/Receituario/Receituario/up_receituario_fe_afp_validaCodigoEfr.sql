/*	
	Valida se codigos internos e externos da entidade

	exec up_receituario_fe_afp_validaCodigoEfr '014','AA'
	

*/




SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_fe_afp_validaCodigoEfr]') IS NOT NULL
	drop procedure up_receituario_fe_afp_validaCodigoEfr
go

Create procedure [dbo].up_receituario_fe_afp_validaCodigoEfr
	@id     VARCHAR(10),
	@codigo VARCHAR(10)
	

/* with encryption */
AS
	SET NOCOUNT ON


	declare @resultado       int = 0


	set @id      = 'ADM' + rtrim(ltrim(@id))
	set @codigo  = rtrim(ltrim(isnull(@codigo,'')))



	select
		@resultado = count(*)
	from
		cptorg(nolock)
	where 
		id_anf = @codigo and
		cptorgstamp = @id

	set @resultado = isnull(@resultado,0)

	select @resultado as resultado

GO
Grant Execute On up_receituario_fe_afp_validaCodigoEfr to Public
Grant Control On up_receituario_fe_afp_validaCodigoEfr to Public
GO


