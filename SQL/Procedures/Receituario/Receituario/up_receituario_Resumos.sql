/*

 exec up_receituario_Resumos 2012,8,'TODAS','TODOS'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_Resumos]') IS NOT NULL
	drop procedure dbo.up_receituario_Resumos
go

create procedure dbo.up_receituario_Resumos

@Ano			NUMERIC,
@Mes			NUMERIC,
@Abrev			VARCHAR(15),
@TipoLote		VARCHAR(15),
@site           varchar(254) = ''

/* with encryption */

AS
SET NOCOUNT ON


Select distinct 
	 ano
	,mes
	,cptorgabrev
	,tlote
FROM
	ctltrct (nolock)
	INNER JOIN tlote (nolock) ON tlote.codigo = ctltrct.tlote
WHERE
	ano = @Ano AND mes = @Mes
	AND cptorgabrev = Case when @Abrev = 'TODAS' then cptorgabrev else @Abrev END
	AND tlote = Case when @TipoLote = 'TODOS' then tlote else @TipoLote END
	and site in (select * from up_SplitToTable(@site,','))/*= case when @site='' then site else @site end*/

group by ano, mes, cptorgabrev, tlote
ORDER BY ano, mes, cptorgabrev, tlote


GO
Grant Execute on dbo.up_receituario_Resumos to Public
Grant control on dbo.up_receituario_Resumos to Public
Go
