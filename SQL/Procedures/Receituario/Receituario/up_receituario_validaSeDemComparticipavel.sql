/* 
 Valida se deve comparticipar a dem

  exec up_receituario_validaSeDemComparticipavel  'DF',  '2708683', 'C2', '101100000311070150012'

  exec up_receituario_validaSeDemComparticipavel    'CE', '6128553','','201100000297197050011'

  exec up_receituario_validaSeDemComparticipavel    'CE', '6128553','','201100000302371640082'


  select *from dispensa_eletronica_d(nolock) where id='101100000311070150012' 

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_validaSeDemComparticipavel]') IS NOT NULL
	drop procedure dbo.up_receituario_validaSeDemComparticipavel
go

create procedure dbo.up_receituario_validaSeDemComparticipavel


@plano      varchar (3),
@ref        varchar (18),
@execcao    varchar (10),
@idlinhaDem varchar (50)



/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

declare @grupo varchar(100) = ""

	set @plano        = RTRIM(LTRIM(@plano))
	set @ref          = RTRIM(LTRIM(@ref))
	set @execcao      = RTRIM(LTRIM(@execcao))
	set @idlinhaDem   = RTRIM(LTRIM(@idlinhaDem))

	declare @compart bit = 1
	
	select 
		top 1 
		@compart = case  when rtrim(ltrim(isnull(tipo_linha,''))) = 'LOUT' then 0 else 1 end
	from 
		Dispensa_Eletronica_D(nolock)
	where 
		id=@idlinhaDem
		and rtrim(ltrim(isnull(medicamento_cod,'')))='99999'
	
	order by data desc


	set @compart = isnull(@compart,1)

	select compart = @compart
GO
Grant Execute on dbo.up_receituario_validaSeDemComparticipavel to Public
Grant Execute on dbo.up_receituario_validaSeDemComparticipavel to Public
go


