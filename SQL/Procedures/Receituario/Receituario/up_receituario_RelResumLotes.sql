/* Reports: RelacaoResumodeLotes;RelacaoResumodeLotesIDU 	
	
	 exec up_receituario_RelResumLotes 2021,9,'sns','99', 'Loja 1'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_RelResumLotes]') IS NOT NULL
	drop procedure dbo.up_receituario_RelResumLotes
go

create procedure dbo.up_receituario_RelResumLotes

@Ano			NUMERIC,
@Mes			NUMERIC,
@Abrev			VARCHAR(15),
@TipoLote		VARCHAR(2),
@site           varchar(254) = ''

/* with encryption */

AS
SET NOCOUNT ON
	

	select top 1  @TipoLote =  loteId  from tlote(nolock) where codigo=@TipoLote


	declare @id varchar(10) = ''
	select 
		top 1
		@id = rtrim(ltrim(ISNULL(b_utentes_site.id,'')))
	from 
		b_utentes(nolock)
	inner join 
		cptorg(nolock) on cptorg.u_no = b_utentes.no
	left join 
		b_utentes_site(nolock) on b_utentes_site.no = b_utentes.no and  b_utentes_site.estab = b_utentes.estab
	where 
		cptorg.abrev = rtrim(ltrim(isnull(@Abrev,'xxxx')))
		and b_utentes_site.site in  (select * from up_SplitToTable(@site,','))
	order by 
		b_utentes_site.site asc

	 set @id = ISNULL(@id,'')


	--
	select
		ctltrct.ctltrctstamp, ctltrct.ano, ctltrct.mes
		,ctltrct.cptorgabrev
		,lote = case when ctltrct.cptplacode='RS' then 99 else ctltrct.lote  end --numero de lote alterado a pedido do spms
		,ctltrct.tlote
		,tlote.loteId
	into
		#receitas
	from
		ctltrct	(nolock)
		INNER JOIN tlote (nolock) ON tlote.codigo = ctltrct.tlote
	where
		ano = @ano 
		and mes = @mes
		and loteId = @TipoLote
		and ctltrct.cptorgabrev = @abrev
		/* adicionada a condi��o fechada a pedido da farm�cia HBA para s� considerar lotes fechados LL 20160127*/
		and fechado = 1
	
	--	 
	select
		ft.ftstamp
		,fistamp, u_epvp, qtt, u_ettent1, u_ettent2, u_ltstamp, u_ltstamp2
		,fi.u_generico, fi.pvp4_fee, fi.pic, pvp4 = isnull(fprod.preco_acordo,0)
	into
		#facturacao
	from
		fi				(nolock)
		inner join ft	(nolock) on ft.ftstamp=fi.ftstamp
		left join fprod (nolock) on fprod.cnp = fi.ref
	where
		ftano = @ano and month(fdata) = @Mes
		and (
			(u_ltstamp != '' and u_ettent1 > 0)
			or
			(u_ltstamp2 != '' and u_ettent2 > 0)
		)
		and ft.site in (select * from up_SplitToTable(@site,','))/*= (case when @site = '' Then ft.site else @site end)*/
	
	--
	select
		ano
		,mes
		,cptorgabrev
		,tlote
		,lote
		
		,receitas	= COUNT(distinct ctltrctstamp)
		,etiquetas	= sum(qtt)
		,pvp		= sum(pvp)
		,utente		= sum(pvp-comp)
		,comp		= sum(comp)
		,fee		= dbo.uf_calcFee(sum(pvp4_fee))
		,id         = @id
		 
	FROM (
		SELECT distinct
			a.ano
			,a.mes
			,a.cptorgabrev
			
			,tlote = a.loteId
			,a.lote
			
			,qtt
			,pvp		= u_epvp*qtt
			,comp		= case when a.ctltrctstamp=fact.u_ltstamp then u_ettent1 else u_ettent2 end
			,pvp4_fee = case  
							when a.cptorgabrev = 'SNS' and a.ano = 2017 and a.mes = 1 -- condi??o necess?rio pq no mes de janeiro o pvp4 n?o estava a ser guardado na fi
								then case 
										when u_generico=1 and fact.pvp4 > 0 and u_epvp <= fact.pvp4 then 0.35 * qtt
										when u_generico=0 and fact.pvp4 > 0 and pic <= fact.pvp4 then 0.35 * qtt
										else 0
										end
							when a.cptorgabrev = 'SNS'
								then fact.pvp4_fee
							else
								0 
							end
			,fistamp
			,ctltrctstamp
		FROM
			#receitas a
			inner join #facturacao fact	ON a.ctltrctstamp = fact.u_ltstamp or a.ctltrctstamp = fact.u_ltstamp2
	) w
	where
		comp > 0
	group by
		ano, mes
		,cptorgabrev, tlote, lote
	ORDER BY
		ano, mes
		,cptorgabrev, tlote, lote


	If OBJECT_ID('tempdb.dbo.#receitas') IS NOT NULL
		drop table #receitas;
	
	If OBJECT_ID('tempdb.dbo.#facturacao') IS NOT NULL
		drop table #facturacao;
	
GO
Grant Execute on dbo.up_receituario_RelResumLotes to Public
Grant control on dbo.up_receituario_RelResumLotes to Public
Go
