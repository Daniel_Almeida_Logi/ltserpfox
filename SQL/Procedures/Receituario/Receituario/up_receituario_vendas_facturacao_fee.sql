/* 
	Retorna fee calculado para entidade SNS

	exec up_receituario_vendas_facturacao_fee 2022, 9,'Loja 1'

	Calcula depois dos lotes estarem fechados

*/
if OBJECT_ID('[dbo].[up_receituario_vendas_facturacao_fee]') IS NOT NULL
	drop procedure dbo.up_receituario_vendas_facturacao_fee
go

create procedure dbo.up_receituario_vendas_facturacao_fee

@ano	numeric(4),
@mes	numeric(2),
@site   varchar(254)

/* @entidade varchar(40) */

/* WITH ENCRYPTION */
AS

	SET FMTONLY OFF 

	IF object_id('tempdb.dbo.#temp_receitas') IS NOT NULL
		DROP TABLE #temp_receitas;
	IF object_id('tempdb.dbo.#temp_facturacao') IS NOT NULL
		DROP TABLE #temp_facturacao;
	IF object_id('tempdb.dbo.#temp_rec_fat') IS NOT NULL
		DROP TABLE #temp_rec_fat;

	declare @abre varchar(10) = 'SNS'  -- o fee apenas é calculado para o SNS

	/*
		Receitas
	*/
	select
		ctltrctstamp
		,ctltrct.cptorgstamp, ano, mes
		,cptorgabrev, lote, tlote, slote, nreceita
		,fechado, u_fistamp
		,descricao = CASE WHEN isnull(mcdt,0) = 0 then  tlote.descricao else tlote.descrMCDT end
		,b_utentes.no, b_utentes.nome
		,tlote.loteId
		,mcdt =  isnull(mcdt,0)
	into
		#temp_receitas	
	from
		ctltrct					(nolock)
		inner join tlote		(nolock) ON tlote.codigo=ctltrct.tlote
		INNER JOIN cptorg		(nolock) ON ctltrct.cptorgstamp = cptorg.cptorgstamp
		INNER JOIN b_utentes	(nolock) ON b_utentes.no = cptorg.u_no AND b_utentes.estab=0
	where
		ano = @ano and mes = @mes
		and fechado = 1
		and cptorg.abrev='SNS'
	

	/*
		Faturacao
	*/
	select
		ft.ftstamp, fistamp, u_epvp, qtt, epv, u_ettent1, u_ettent2, u_ltstamp, u_ltstamp2
		,iva
		,fi.u_generico, fi.pvp4_fee, fi.pic, pvp4 = isnull(fprod.preco_acordo,0)
	into
		#temp_facturacao		
	from
		fi				(nolock)
		inner join ft	(nolock) on ft.ftstamp=fi.ftstamp
		left join fprod (nolock) on fprod.cnp = fi.ref
	where
		ftano = @ano and month(fdata) = @mes
		and (
			(u_ltstamp != '' and u_ettent1 > 0)
			or
			(u_ltstamp2 != '' and u_ettent2 > 0)
		)
		and ft.site in (select * from up_SplitToTable(@site,','))/*= (case when @site = '' Then ft.site else @site end)*/


	/*	
		Receitas e Faturacao
	*/
	SELECT distinct
		a.ano
		,a.mes
		,entidades		= a.cptorgabrev
		,Sel			= Cast(0 as bit)
		,cptorgstamp	= a.cptorgstamp
		,serie			= a.slote
		,tipos			= a.loteId
		,series			= a.descricao
		/* ,receitas		= ROW_NUMBER() over (partition by ctltrctstamp order by ctltrctstamp) */
		,lotes			= a.lote
			
		,iva			= fact.iva
		,tabiva			= (select top 1 codigo from taxasiva (nolock) where taxa=fact.iva)
		,pvp			= fact.u_epvp * fact.qtt
		,comp			= case when a.ctltrctstamp=fact.u_ltstamp then u_ettent1 else u_ettent2 end
		,pvp4_fee = case  
						when a.cptorgabrev = 'SNS' and a.ano = 2017 and a.mes = 1 -- condi??o necess?rio pq no mes de janeiro o pvp4 n?o estava a ser guardado na fi
							then case 
									when u_generico=1 and fact.pvp4 > 0 and u_epvp <= fact.pvp4 then 0.35 * qtt
									when u_generico=0 and fact.pvp4 > 0 and pic <= fact.pvp4 then 0.35 * qtt
									else 0
									end
						when a.cptorgabrev = 'SNS'
							then fact.pvp4_fee
						else
							0 
						end
		,no				= a.no
		,nome			= a.nome
		,ctltrctstamp
		,ftstamp
		,fistamp /* garante que linhas iguais não sejam agrupadas pelo inner join */
		,qtt
		,tlote
		,mcdt
	into
		#temp_rec_fat
	FROM
		#temp_receitas a
		inner join #temp_facturacao fact	ON a.ctltrctstamp = fact.u_ltstamp or a.ctltrctstamp = fact.u_ltstamp2



	/*
		ResultSet Final
	*/


	-- added one more line for FEE


	SELECT
		ano, mes
		,entidades, sel
		,cptorgstamp
		,valorFinal = sum(pvp4_fee)
	FROM (
		SELECT * from #temp_rec_fat
	) w
	where
		pvp4_fee > 0 and comp > 0
	group by
		ano, mes
		,entidades, sel
		,cptorgstamp
		,no, nome, mcdt
	--
	ORDER BY
		w.ano, w.mes, w.entidades
	

	IF object_id('tempdb.dbo.#temp_receitas') IS NOT NULL
		DROP TABLE #temp_receitas;
	IF object_id('tempdb.dbo.#temp_facturacao') IS NOT NULL
		DROP TABLE #temp_facturacao;
	IF object_id('tempdb.dbo.#temp_rec_fat') IS NOT NULL
		DROP TABLE #temp_rec_fat;
		   
	
Go
Grant Execute on dbo.up_receituario_vendas_facturacao_fee to Public
Grant Control on dbo.up_receituario_vendas_facturacao_fee to Public
Go
