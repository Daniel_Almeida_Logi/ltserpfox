/* Visualizar Detalhe do Lote
	
	 exec up_receituario_detalheLote 2016, 4, SNS, 97, 1, 'Loja 1'

	exec up_receituario_detalheLote 2022, 9, 'AFP', 2, 1, 'Loja 2'

	exec up_receituario_detalheLote 2022, 9, 'AFP', 2, 1, 'Loja 2'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_detalheLote]') IS NOT NULL
	drop procedure dbo.up_receituario_detalheLote
go

create procedure dbo.up_receituario_detalheLote
	@ano		numeric(4),
	@mes		numeric(2),
	@abrev		varchar(15),
	@tlote		varchar(2),
	@lote		numeric(9),
	@site       varchar(254)

/* WITH ENCRYPTION */
AS
	declare @usaPosto bit =0


	if(len(ltrim(rtrim(isnull(@site,''))))<=6)
	begin
		select top 1 @usaPosto=isnull(posto,0) from empresa(nolock) where site=@site
	end


	;with
		cte_receitas as (
			select
				ctltrctstamp, ctltrct.cptorgstamp, ano, mes
				,cptorgabrev, lote, tlote, slote, nreceita
				,fechado, u_fistamp
				,descricao = CASE WHEN isnull(mcdt,0) = 0 then  tlote.descricao else tlote.descrMCDT end
				,cptplacode
				,ctltrct.usrinis, ctltrct.usrdata, ctltrct.usrhora
				,ctltrct.site, ctltrct.posto
				,token
				,tlote.loteId
				,receita
				,partilhado = case when (select  COUNT(*) from ext_sinave(nolock) where ext_sinave.nr_receita!='' and ext_sinave.nr_receita=receita and ext_sinave.path!='')  > 0 then convert(bit,1) else convert(bit,0) end
			from
				ctltrct (nolock)
				INNER JOIN tlote (nolock) ON tlote.codigo = ctltrct.tlote
				LEFT  JOIN   cptorg  (nolock)  ON cptorg.cptorgstamp  = ctltrct.cptorgstamp
			where
				ano = @ano and mes = @mes
				AND cptorgabrev = @abrev
				AND tlote = @tlote 
				AND lote = @lote
				and site = case when len(@site)>6 or @usaPosto=1 then site else  @site end --se for posto envia varios sites
		),
	 
		cte_facturacao as (
			select
				ft.ftstamp, fistamp, u_epvp, qtt, u_ettent1, u_ettent2, u_ltstamp, u_ltstamp2
				,fi.u_generico, fi.pvp4_fee, fi.pic, pvp4 = isnull(fprod.preco_acordo,0)
			--	,returnTrackingID = ISNULL(ftc.returnTrackingID, '')
			from
				fi (nolock)
				inner join ft (nolock)	on ft.ftstamp=fi.ftstamp
				left join fprod (nolock) on fprod.cnp = fi.ref
			--	left join ft_compart(nolock) as ftc on ft.ftstamp = ftc.ftstamp
			where
				ftano = @ano and month(fdata)= @mes
				and (
					(u_ltstamp != '' and u_ettent1 > 0)
					or
					(u_ltstamp2 != '' and u_ettent2 > 0)
				)
				and ft.site in (select * from up_SplitToTable(@site,','))/*= (case when @site = '' Then ft.site else @site end)*/
		)
	
	--select sum(comp),sum(pvp),sum(utente) from (
	select
		ano
		,mes
		,tlote
		,slote
		,lote
		,cptorgabrev
		,nreceita
		,tlote2
		,utente			= sum(pvp)-sum(comp)
		,pvp			= sum(pvp)
		,comp			= sum(comp)	
		,fee            =  dbo.uf_calcFee(sum(pvp4_fee))
		,facturado
		,ctltrctstamp
		,cptplacode
		,fechado
		,ftstamp
		,usrinis, usrdata, usrhora
		,u_ltstamp, u_ltstamp2
		,site
		,posto
		,TipoLoteSNS
		,receita
		,partilhado
	--	,returnTrackingID
	from (

		select
			ano				= c.ano
			,mes			= c.mes
			,tlote			= rtrim(c.loteId) + ' - ' + c.descricao
			,tlote2			= c.tlote
			,cptorgabrev	= c.cptorgabrev
			,slote			= c.slote
			,lote			= c.lote
			,nreceita		= c.nreceita
			,receita        = rtrim(ltrim(isnull(c.receita,'')))
			,pvp			= ft.u_epvp*ft.qtt
			,comp			= case when c.ctltrctstamp=ft.u_ltstamp then u_ettent1 else u_ettent2 end
			,pvp4_fee = case  
							when c.cptorgabrev = 'SNS' and c.ano = 2017 and c.mes = 1 -- condi??o necess?rio pq no mes de janeiro o pvp4 n?o estava a ser guardado na fi
								then case 
										when u_generico=1 and ft.pvp4 > 0 and u_epvp <= ft.pvp4 then 0.35 * qtt
										when u_generico=0 and ft.pvp4 > 0 and pic <= ft.pvp4 then 0.35 * qtt
										else 0
										end
							when c.cptorgabrev = 'SNS'
								then ft.pvp4_fee
							else
								0 
							end
			,fechado
			,facturado		= CASE WHEN u_fistamp='' THEN CAST(0 as Bit) ELSE CAST(1 As Bit) END
			,ctltrctstamp
			,cptplacode
			,ftstamp, fistamp
			,c.usrinis, c.usrdata, c.usrhora
			,ft.u_ltstamp, ft.u_ltstamp2
			,c.site, posto = case when c.posto = '' then c.site else c.posto end
			/* tipoLoteSNS � o lote retornado pelo servi�o de consulta. Mesma receita pode ter mais do que tipo de lote, mas o lote errado � sempre o lote mais baixo (96,98), 
				por isso o top 1 deve ser suficiente  para saber se a receita tem lotes inv�lidos ou n�o */
			,isnull((select top 1 lote from dispensa_eletronica_d (nolock) where token = c.token order by lote asc),tlote) as TipoLoteSNS
			,partilhado
			--,ft.returnTrackingID
		from
			cte_receitas c
			inner join cte_facturacao ft	ON c.ctltrctstamp = ft.u_ltstamp or c.ctltrctstamp = ft.u_ltstamp2
	)x
	where
		comp > 0
	group by
		ano, mes, cptorgabrev, tlote, tlote2, slote, lote
		,fechado, nreceita, facturado
		,ctltrctstamp, cptplacode
		,ftstamp, usrinis, usrdata, usrhora, u_ltstamp, u_ltstamp2
		,site,posto, TipoLoteSNS, receita, partilhado--, returnTrackingID
	--)xx
	order by
		nreceita

Go
Grant Execute on dbo.up_receituario_detalheLote to Public
Grant Control on dbo.up_receituario_detalheLote to Public
Go
