/*

	exec up_receituario_receitasList '2015','12', 'SNS', '10', 1, 1, 0, 0, 'Loja 1'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_receitasList]') IS NOT NULL
	drop procedure dbo.up_receituario_receitasList
go

Create procedure dbo.up_receituario_receitasList

@Ano			NUMERIC(4)
,@Mes			NUMERIC(2)
,@abrev			varchar(15)
,@tlote			varchar(2)
,@loteIni		numeric(10)
,@loteFim		numeric(10)
,@receitaIni	numeric(10) = 0
,@receitaFim	numeric(10) = 0
,@site          varchar(254) = '' /*este par�metro n�o � utilizado na SP????*/

/* with encryption */
AS
SET NOCOUNT ON

	select
		ft.ftstamp
		,ctltrct.ctltrctstamp
		,ctltrct.cptorgabrev, ctltrct.tlote, ctltrct.lote, ctltrct.nreceita
		,ft2.u_abrev, ft2.u_abrev2, ft.u_ltstamp, ft.u_ltstamp2
	from
		ctltrct (nolock)
		inner join ft (nolock)	on ft.u_ltstamp = ctltrct.ctltrctstamp or ft.u_ltstamp2 = ctltrct.ctltrctstamp
		inner join ft2 (nolock)	on ft2.ft2stamp = ft.ftstamp
	where
		ctltrct.ano = @ano
		and ctltrct.mes = @mes
		and ctltrct.cptorgabrev	= @abrev
		and ctltrct.tlote = @tlote
		and ctltrct.lote between @loteIni and @loteFim
		and ctltrct.nreceita between (case when @receitaIni=0 then 1 else @receitaIni end) and (case when @receitaFim=0 then 30 else @receitaFim end)
	group by
		ft.ftstamp, ctltrct.ctltrctstamp, ctltrct.cptorgabrev, ctltrct.tlote, ctltrct.lote, ctltrct.nreceita, ft2.u_abrev, ft2.u_abrev2, ft.u_ltstamp, ft.u_ltstamp2
	order by
		cptorgabrev, tlote, lote, nreceita
	

Go
Grant Execute on dbo.up_receituario_receitasList to Public
Grant Control on dbo.up_receituario_receitasList to Public
Go
