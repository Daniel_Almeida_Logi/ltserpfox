-- Gestão de Lotes
/*
	exec up_receituario_closelotes 2022, 05,'Loja 1' WITH RECOMPILE


*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_receituario_closelotes]') IS NOT NULL
	drop procedure dbo.up_receituario_closelotes
GO

create procedure dbo.up_receituario_closelotes

@ano numeric(4),
@mes numeric(2),
@site varchar(254)

/* WITH ENCRYPTION */
AS
	
	declare @usaPosto bit =0


	if(len(ltrim(rtrim(isnull(@site,''))))<=6)
	begin
		select top 1 @usaPosto=isnull(posto,0) from empresa(nolock) where site=@site
	end


	set @usaPosto = isnull(@usaPosto,0)




	;with 
	cte_receitas as (
		

		select
			ctltrctstamp, ctltrct.cptorgstamp, ano, mes
			,cptorgabrev, lote, tlote, slote, nreceita
			,fechado
			,descricao = CASE WHEN isnull(mcdt,0) = 0 then  tlote.descricao else tlote.descrMCDT end
			,u_fistamp
			,site
			,tlote.loteId
		from
			ctltrct				 (nolock)
			INNER JOIN   tlote	 (nolock)  ON tlote.codigo=ctltrct.tlote
			LEFT  JOIN   cptorg  (nolock)  ON cptorg.cptorgstamp  = ctltrct.cptorgstamp
		where
			ano=@ano and mes=@mes 
			and site = case when len(@site)>6 or @usaPosto=1 then site else  @site end --se for posto envia varios sites
		
	),


	

	cte_facturacao as (
		select
			ft.ftstamp, fistamp, u_epvp, qtt, epv, u_ettent1, u_ettent2, u_ltstamp, u_ltstamp2
		from
			fi				(nolock)
			inner join ft	(nolock) on ft.ftstamp=fi.ftstamp
		where
			ftano=@ano and month(fdata)=@mes
			and (
				(u_ltstamp != '' and u_ettent1 > 0)
				or
				(u_ltstamp2 != '' and u_ettent2 > 0)
			)
			and ft.site in (select * from up_SplitToTable(@site,','))/*= (case when @site = '' Then ft.site else @site end)*/
	)



	select
		pesquisa = CONVERT(bit,0)
		,ano
		,mes
		,cptorgabrev
		,tlote2
		,tlote
		,slote
		,lote
		,utente			= sum(pvp) - sum(comp)
		,comp			= sum(comp)
		,pvp			= sum(pvp)
		,receitas		= COUNT(distinct ctltrctstamp)
		--,buracos		= max(nreceita) - sum(case when receitas = 1 then receitas else 0 end)
		,buracos		= case when  max(nreceita) = 29 then 30 else max(nreceita) end - sum(case when receitas = 1 then receitas else 0 end) 
		/*,bt1			= convert(bit,0)*/
		,fechar			= convert(bit,0)
		,fechado
		,cptorgstamp
		,id_anf
		,e_datamatrix
		,u_fistamp
		,nome
	from (

		select
			ano				= c.ano
			,mes			= c.mes
			,cptorgabrev	= c.cptorgabrev
			,tlote2			= rtrim(c.loteId) + ' - ' + c.descricao
			,tlote			= c.tlote
			,slote			= c.slote
			,lote			= c.lote
		
			,pvp			= ft.u_epvp*ft.qtt
			,comp			= case when c.ctltrctstamp=ft.u_ltstamp then u_ettent1 else u_ettent2 end
			,receitas		= ROW_NUMBER() over (partition by ctltrctstamp order by ctltrctstamp)
			,nreceita		= c.nreceita
			,fechado		= c.fechado
			,cptorgstamp	= c.cptorgstamp
			,ctltrctstamp
			,fistamp /* garante que linhas iguais não sejam agrupadas pelo inner join*/
			,id_anf
			,e_datamatrix
			,u_fistamp
			,cp.nome
		from
			cte_receitas c
			inner join cte_facturacao ft	ON c.ctltrctstamp = ft.u_ltstamp or c.ctltrctstamp = ft.u_ltstamp2
			Left join cptorg		  cp	ON c.cptorgstamp  = cp.cptorgstamp
		where 
			(case when c.ctltrctstamp=ft.u_ltstamp then u_ettent1 else u_ettent2 end) > 0
	) x
	group by
		ano, mes, cptorgabrev, tlote, tlote2, slote, lote
		,fechado, cptorgstamp,id_anf,e_datamatrix, u_fistamp,nome
	order by
		cptorgabrev, tlote, lote


GO
Grant Execute on dbo.up_receituario_closelotes to Public
Grant Control on dbo.up_receituario_closelotes to Public
GO
