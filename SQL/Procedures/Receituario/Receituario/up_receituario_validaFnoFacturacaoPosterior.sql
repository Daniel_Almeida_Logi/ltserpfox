/*	
	Valida se existe um documento com data posterior para o tipo de documento

	exec up_receituario_validaFnoFacturacaoPosterior 30,'20220705'


*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_validaFnoFacturacaoPosterior]') IS NOT NULL
	drop procedure up_receituario_validaFnoFacturacaoPosterior
go

Create procedure [dbo].up_receituario_validaFnoFacturacaoPosterior
	@ndoc  int,
	@data varchar(20)
	



/* with encryption */
AS
	SET NOCOUNT ON

	declare @conta int = 0;

	Select 
		@conta = count(*)
	From
		ft(nolock)
	where
		 ft.ndoc = @ndoc
		 and ft.fdata > @data

	set @conta = isnull(@conta,0)

	if(@conta=0)
		select convert(bit,1) as result
	else
		select convert(bit,0) as result

GO
Grant Execute On up_receituario_validaFnoFacturacaoPosterior to Public
Grant Control On up_receituario_validaFnoFacturacaoPosterior to Public
GO


