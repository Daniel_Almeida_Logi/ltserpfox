/*	
	Valida se o valor  refacturado é menor ou igual que o facturado

	up_receituario_fe_afp_validaDocTrackingLinesValue '21a4f0ae-8e58-4d36-a2a5-4e715845e9','20a7f9f3-9387-4fc7-9113-aa33bb3e4b',100
*/




SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_fe_afp_validaDocTrackingLinesValue]') IS NOT NULL
	drop procedure up_receituario_fe_afp_validaDocTrackingLinesValue
go

Create procedure [dbo].up_receituario_fe_afp_validaDocTrackingLinesValue
	@stampPresc varchar(255),
	@stampPrescLines varchar(255),
	@returnValueLine decimal(11,3)



/* with encryption */
AS
	SET NOCOUNT ON

	declare @conta  int = 0

	Select 
		"val" = count(stamp)
	From
		ext_esb_prescription_d (nolock)
	Where 
		stamp_ext_esb_prescription = @stampPresc  
		and stamp = @stampPrescLines
		and round(abs(@returnValueLine),2)<=round(abs(ext_esb_prescription_d.totalEFR),2)
	

GO
Grant Execute On up_receituario_fe_afp_validaDocTrackingLinesValue to Public
Grant Control On up_receituario_fe_afp_validaDocTrackingLinesValue to Public
GO


