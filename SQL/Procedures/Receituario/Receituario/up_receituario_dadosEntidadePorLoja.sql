/*
	
	Daniel Almeida
	Criado: 2024-03-07

	Valida a entidade financeira responsavel associada a loja
	Depende do campo cptorg.entPorLoja


	use ltdev30
	EXEC up_receituario_dadosEntidadePorLoja 'SNS', 'Loja 1'
	    
	        
*/




IF OBJECT_ID('[dbo].[up_receituario_dadosEntidadePorLoja]') IS NOT NULL
    DROP procedure dbo.up_receituario_dadosEntidadePorLoja 
GO

CREATE procedure dbo.up_receituario_dadosEntidadePorLoja
	@abrev varchar(10),
	@site varchar(15)
	
AS
	select 
		no,
		estab
	from 
		cptorg_dep_site cps(nolock)
	where 
		cps.cptorgAbrev=@abrev
		and cps.site = @site
GO
GRANT EXECUTE on dbo.up_receituario_dadosEntidadePorLoja  TO PUBLIC
GRANT Control on dbo.up_receituario_dadosEntidadePorLoja  TO PUBLIC
GO



