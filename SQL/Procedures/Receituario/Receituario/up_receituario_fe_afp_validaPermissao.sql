/*	
	Valida se o cliente esta bloqueado para facturacao
	
	select *from [ext_esb_doc_permissions]

	exec up_receituario_fe_afp_validaPermissao '7633', '014', 1, 1


*/




SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_fe_afp_validaPermissao]') IS NOT NULL
	drop procedure up_receituario_fe_afp_validaPermissao
go

Create procedure [dbo].up_receituario_fe_afp_validaPermissao
	 @codeInfarmed  VARCHAR(10)
	,@codeOrg	    VARCHAR(10)
	,@tipoDoc       int
	,@test          bit = 0




/* with encryption */
AS
	SET NOCOUNT ON

	set @codeInfarmed	= isnull(@codeInfarmed,'0')
	set @codeOrg		= isnull(@codeOrg,'')
	set @tipoDoc		= isnull(@tipoDoc,0)
	set @test			= isnull(@test,0)

	declare  @countBlocked int = 0

	declare @result bit = 1
	set @tipoDoc = isnull(@tipoDoc,1)


	-- se for testes devolve sempre ok
	if(@test=1)
	begin
		select @result as 'result'
		return 
	end


	-- valida se o cliente está bloqueado pela entidade
	Select 
		 @countBlocked = count(perm.stamp)
	From
		ext_esb_doc_permissions(nolock) perm
	Where 
		convert(int,RTRIM(LTRIM(@codeInfarmed))) = convert(int,perm.codeInfarmed) and
		RTRIM(LTRIM(perm.codeOrg))               = @codeOrg and
		perm.blockInvoice                        = case when @tipoDoc = 1 then 1 else perm.blockInvoice    end and
		perm.blockCreditNote                     = case when @tipoDoc = 2 then 1 else perm.blockCreditNote end and
		perm.blockDebitNote                      = case when @tipoDoc = 3 then 1 else perm.blockDebitNote  end 

	

	if(isnull(@countBlocked,0)>0)
	begin
		set @result = 0 
	end else 
	begin
		set @result = 1
	end	

	select @result as 'result'


GO
Grant Execute On up_receituario_fe_afp_validaPermissao to Public
Grant Control On up_receituario_fe_afp_validaPermissao to Public
GO