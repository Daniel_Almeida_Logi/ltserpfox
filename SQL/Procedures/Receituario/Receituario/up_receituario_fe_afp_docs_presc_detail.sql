/*	
	retorna dados de prescricao

	 @stamp - Stamp da tabela ext_esb_prescription

    select * from ext_esb_prescription_d

	exec up_receituario_fe_afp_docs_presc_detail  'B8630A85-8AC0-4B4E-AC81-CE203248841C'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_fe_afp_docs_presc_detail]') IS NOT NULL
	drop procedure up_receituario_fe_afp_docs_presc_detail
go


Create procedure [dbo].up_receituario_fe_afp_docs_presc_detail
	 @stamp VARCHAR(50)

/* with encryption */
AS
	SET NOCOUNT ON


SELECT 	 
	  [stamp]
      ,[ref]
      ,[qt]
      ,[totalPVP]
      ,[totalPVU]
      ,[totalEFR]
      ,[totalSNS]
      ,[authorizationCode]
      ,[saleDate]
      ,[row]
      ,[totalOthers]
      ,[baseAmount]
      ,[vatPercentage]
      ,[vatValue]
      ,[codEmb]

  FROM [dbo].ext_esb_prescription_d(nolock)
  where
	stamp_ext_esb_prescription = @stamp
  order by row asc

	
GO
Grant Execute On up_receituario_fe_afp_docs_presc_detail to Public
Grant Control On up_receituario_fe_afp_docs_presc_detail to Public
GO


