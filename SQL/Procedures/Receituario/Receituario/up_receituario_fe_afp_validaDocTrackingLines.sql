/*	
	Valida se  as linhas de prescrição existe,

	up_receituario_fe_afp_validaDocTrackingLines '21a4f0ae-8e58-4d36-a2a5-4e715845e9','20a7f9f3-9387-4fc7-9113-aa33bb3e4b','4080341c-d14c-4845-800f-544b1cdc72'
*/





SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_fe_afp_validaDocTrackingLines]') IS NOT NULL
	drop procedure up_receituario_fe_afp_validaDocTrackingLines
go

Create procedure [dbo].up_receituario_fe_afp_validaDocTrackingLines
	@stampPresc varchar(255),
	@stampPrescLines varchar(255),
	@trackingId varchar(255)



/* with encryption */
AS
	SET NOCOUNT ON

	declare @conta  int = 0

	Select 
		"val" = count(ext_esb_prescription_d.stamp)
	From
		ext_esb_prescription_d (nolock)
	inner join ext_esb_prescription(nolock) on ext_esb_prescription.stamp = ext_esb_prescription_d.stamp_ext_esb_prescription
	inner join ext_esb_doc(nolock) on ext_esb_doc.stamp = ext_esb_prescription.stamp_ext_esb_doc
	Where 
		ext_esb_prescription_d.stamp_ext_esb_prescription = @stampPresc  
		and ext_esb_prescription_d.stamp = @stampPrescLines
		and ext_esb_doc.stamp = @trackingId
	
	
GO
Grant Execute On up_receituario_fe_afp_validaDocTrackingLines to Public
Grant Control On up_receituario_fe_afp_validaDocTrackingLines to Public
GO


