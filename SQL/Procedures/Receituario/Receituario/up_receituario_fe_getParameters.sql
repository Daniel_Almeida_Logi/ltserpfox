/* 
	retorna parametros de site e gerais

	exec up_receituario_fe_getParameters 'Loja 1'

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_fe_getParameters]') IS NOT NULL
	drop procedure up_receituario_fe_getParameters
go

CREATE procedure [dbo].up_receituario_fe_getParameters

	@site      varchar(18)   

/* with encryption */

AS
SET NOCOUNT ON
	declare @dc bit = 0
	declare @path  varchar(100) = '' 

	set @site = ISNULL(@site,'Loja 1')
	

	SELECT top 1 @dc = isnull(bool,0) FROM B_Parameters(nolock) WHERE stamp='ADM0000000323'

	SELECT top 1 @path = rtrim(ltrim(isnull(textValue,0))) FROM B_Parameters_site(nolock) WHERE stamp='ADM0000000128' and site=@site

	select isnull(@dc,0) as dc, rtrim(ltrim(isnull(@path,'')))  as [path]


GO
Grant Execute On up_receituario_fe_getParameters to Public
Grant Control On up_receituario_fe_getParameters to Public
GO