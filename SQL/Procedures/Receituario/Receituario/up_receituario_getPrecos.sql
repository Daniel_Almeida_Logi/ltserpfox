/* Ver Pre�os do Produto para calcular comparticipa��o (FUN��ES DA VENDA)

 exec up_receituario_getPrecos 0, '5440987', '1'

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_getPrecos]') IS NOT NULL
	drop procedure dbo.up_receituario_getPrecos
go

create procedure dbo.up_receituario_getPrecos

@pvp numeric(13,3),
@ref varchar (120),
@site_nr tinyint

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

SELECT 
	 cnp		=	fprod.cnp
	,ppvp		=	case when fprod.pvporig=0 then null else fprod.pvporig end --a.epreco
	,pref		=	case when fprod.pref=0 then null else fprod.pref end --b.epreco
	,ppens		=	case when fprod.pref=0 then null else fprod.pref end --c.epreco
	,pesp		=	null --d.epreco
	,pst1		=	case when @pvp = 0
						then IsNull(st.epv1,0)
						else @pvp
					end
FROM
	fprod (nolock)
	left join st (nolock)		on st.ref = fprod.cnp and st.site_nr = @site_nr
	--left join fpreco a (nolock) on a.cnp = fprod.cnp AND upper(a.grupo)='PVP'
	--left join fpreco b (nolock) on b.cnp = fprod.cnp AND upper(b.grupo)='REF'
	--left join fpreco c (nolock) on c.cnp = fprod.cnp AND upper(c.grupo)='PENS'
	--left join fpreco d (nolock) on d.cnp = fprod.cnp AND upper(d.grupo)='ESP'
WHERE
	fprod.cnp = @ref


GO
Grant Execute on dbo.up_receituario_getPrecos to Public
Grant Execute on dbo.up_receituario_getPrecos to Public
go
