/*	
	Valida se o documento existe e está validado pelo numero de documento, ano e codigo infarmed

	up_receituario_fe_afp_validaDocTrackingExternal 'FME 500/20','2021','35572','1'


*/




SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_fe_afp_validaDocTrackingExternal]') IS NOT NULL
	drop procedure up_receituario_fe_afp_validaDocTrackingExternal
go

Create procedure [dbo].up_receituario_fe_afp_validaDocTrackingExternal
	@docNr varchar(20),
	@year int,
	@code  varchar(20),
	@test varchar(1)




/* with encryption */
AS
	SET NOCOUNT ON

	Select 
		"stamp" = rtrim(ltrim(isnull(stamp,'')))
	From
		ext_esb_doc (nolock)
	Where 
		year(docDate) = @year and
		[id_ext_pharmacy] = convert(int,@code) and
		[docNr] = @docNr and
		error_export = 0 and
		test = @test
		

GO
Grant Execute On up_receituario_fe_afp_validaDocTrackingExternal to Public
Grant Control On up_receituario_fe_afp_validaDocTrackingExternal to Public
GO