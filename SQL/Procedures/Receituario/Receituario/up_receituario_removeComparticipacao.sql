/* Valida se deve remover a comparticipação para planos especiais

 exec up_receituario_removeComparticipacao  'CE', '6128553'

 exec up_receituario_removeComparticipacao  '01', '5440987'

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_removeComparticipacao]') IS NOT NULL
	drop procedure dbo.up_receituario_removeComparticipacao
go

create procedure dbo.up_receituario_removeComparticipacao


@plano varchar (3),
@ref varchar (120)




/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

declare @grupo varchar(100) = ""


	set @plano = RTRIM(LTRIM(@plano))
	set @ref = RTRIM(LTRIM(@ref))

	SELECT 
		@grupo = grupo
	FROM
		fprod (nolock)
	WHERE
		fprod.cnp = @ref


	select 
		top 1
		pct 
	from 
		cptval(nolock)
	left join 
		cptpla(nolock) ON cptpla.codigo = cptval.cptplacode
	where 
	cptplacode = @plano and grupo = @grupo
	and combinado = 1 and inactivo = 0
	/* Camaras expansoras */
	and (
		(grupo ='SC' and cptplacode !='CE')
		/* Manipulados/Diateticos */
		OR	((grupo ='DI' OR grupo='MN') and cptplacode !='47')
		/* DO - Ostomomia */ 
		OR	(grupo='SO' and cptplacode !='DO' )
	)
 

GO
Grant Execute on dbo.up_receituario_removeComparticipacao to Public
Grant Execute on dbo.up_receituario_removeComparticipacao to Public
go


