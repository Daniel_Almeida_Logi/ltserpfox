/*
	Impress�o de verbetes

	exec up_receituario_VerbeteFact 2022, 5, 'SNS ARS-CENTRO', '1', 0, 0, 'Loja 1'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_VerbeteFact]') IS NOT NULL
	drop procedure dbo.up_receituario_VerbeteFact
go

Create procedure dbo.up_receituario_VerbeteFact

@Ano				NUMERIC(4)
,@Mes				NUMERIC(2)
,@Abrev				VARCHAR(15)
,@TipoLote			VARCHAR(2)
,@LoteInicial		NUMERIC(9)
,@LoteFinal			NUMERIC(9)
,@site				varchar(254)

/* with encryption */

AS
	SET NOCOUNT ON

	-- Receitas
	select
		ctltrct.ctltrctstamp, ctltrct.ano, ctltrct.mes
		,ctltrct.cptorgabrev, ctltrct.cptplacode
		,ctltrct.lote, ctltrct.tlote, ctltrct.slote, ctltrct.nreceita
		,tlotedescr = CASE WHEN isnull(mcdt,0) = 0 then  tlote.descricao else tlote.descrMCDT end
		,cptpla.design
		,tlote.loteId
	into
		#receitas
	from
		ctltrct				(nolock)
		inner join cptpla	(nolock) on ctltrct.cptplacode=cptpla.codigo
		INNER JOIN tlote (nolock) ON tlote.codigo = ctltrct.tlote
	where
		ano = @ano and mes = @mes
		and site = case when @site='' then site else @site end
		and tlote = case when @tipoLote = '' then tlote else @tipolote end
		and ctltrct.cptorgabrev = case when @Abrev = '' then ctltrct.cptorgabrev else @Abrev end
		AND (lote BETWEEN
				(case when @LoteInicial=0 then lote else @LoteInicial end)
				AND
				(case when @LoteFinal=0 then lote else @LoteFinal end)
			)

	-- Fatura��o
	select
		ft.ftstamp, ft.nmdoc, ft.fno
		,fistamp, u_epvp, qtt, u_ettent1, u_ettent2, u_ltstamp, u_ltstamp2
	into
		#faturacao
	from
		fi						(nolock)
		inner join ft			(nolock) on ft.ftstamp = fi.ftstamp
		inner join #receitas	(nolock) on ctltrctstamp = ft.u_ltstamp or ctltrctstamp = ft.u_ltstamp2
	where
		fi.u_ettent1>0 or fi.u_ettent2>0
		/*
			ftano = @Ano and month(fdata) = @Mes
			and (
				(u_ltstamp != '' and u_ettent1 > 0)
				or
				(u_ltstamp2 != '' and u_ettent2 > 0)
			)
			--and ft.site in (select * from up_SplitToTable(@site,','))/*= (case when @site = '' Then ft.site else @site end)*/
		*/
	

	SELECT
		ano
		,mes
		,cptorgabrev
		,cptplacode
		,design
		,slote
		,tlote
		,lote
		,nmdoc
		,fno
		,etiquetas	= sum(qtt)
		,pvp		= sum(pvp)
		,utente		= sum(pvp-comp)
		,comp		= sum(comp)
		,nreceita
		,tlotedescr
	FROM (
		SELECT distinct
			a.ano
			,a.mes
			,a.cptorgabrev
			,a.cptplacode
			,a.design
			,a.slote
			,tlote = a.loteId
			,a.lote
			,fact.nmdoc
			,fact.fno
			,qtt
			,pvp		= u_epvp*qtt
			,comp		= case when a.ctltrctstamp=fact.u_ltstamp then u_ettent1 else u_ettent2 end
			,a.nreceita
			,a.tlotedescr
			,fistamp
		FROM
			#receitas a
			inner join #faturacao fact	ON a.ctltrctstamp = fact.u_ltstamp or a.ctltrctstamp = fact.u_ltstamp2
	) w
	where
		comp > 0
	group by
		ano, mes
		,cptorgabrev, cptplacode, design
		,slote, tlote, lote, nmdoc, fno
		,nreceita, tlotedescr
	ORDER BY
		cptorgabrev, tlote, lote, nreceita

Go
Grant Execute on dbo.up_receituario_VerbeteFact to Public
Grant Control on dbo.up_receituario_VerbeteFact to Public
Go