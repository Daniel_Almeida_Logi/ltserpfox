/*
	SP para gerar ficheiro de detalhe da ANF
	
	exec up_receituario_GerarFileANF 2018,04,'NOVONORDISK','Loja 1'

	
	exec up_receituario_GerarFileANF 2019,3,'SBSI','Loja 1'

		exec up_receituario_GerarFileANF 2019,3,'SCML','Loja 1'



	select * from cptorg where abrev='novartisapoio'
	update cptpla set id_anf='WH' where codigo in ('PA','PB')
	select id_anf,* from cptpla where design like '%novartis%'
*/

if OBJECT_ID('[dbo].[up_receituario_GerarFileANF]') IS NOT NULL
	drop procedure dbo.up_receituario_GerarFileANF
go

create procedure dbo.up_receituario_GerarFileANF
	@ano numeric(4,0)
	,@mes numeric(2,0)
	,@cptorgabrev varchar(15)
	,@site varchar(254)

/* with encryption */
as
	begin

		set language portuguese
		set	nocount on

		/*
		*�Criar tabela para mapear c�digos dos planos logitools com os da ANF
		*/
		declare
			@ano2				numeric(4,0)
			,@mes2				numeric(2,0)
			--,@cptorgabrev2		varchar(256)
			,@site2				varchar(20)
						
		/* Variaveis internas */
		select
			@ano2 = @ano
			,@mes2 = @mes
			,@cptorgabrev = ltrim(rtrim(@cptorgabrev))
			,@site2 = @site



		/*
			Dados das Receitas (cabe�alho)
		*/
		select
			ft.ftstamp, ft.fdata
			,ctltrct.ano, ctltrct.mes, ctltrct.slote, ctltrct.tlote, ctltrct.lote, ctltrct.nreceita, ctltrct.cptplacode, ctltrct.receita
			,ft2.u_entpresc, ft2.u_nbenef2, ft2.u_nopresc
			,TotalCompartLinhas = sum(fi.u_ettent1+fi.u_ettent2)
			,TotalReceita = sum(fi.u_epvp*fi.qtt)
			,TotalEmbalagens = sum(fi.qtt)
			,TotalPagPrim = sum(fi.u_ettent2) --Pago � entidade principal (SNS), se existir complementariedade o SNS � sempre a entidade 2
			/*
				Campo virtual, apenas serve de indicador para sabermos qual valor de Comparticipa��o usar. Nunca s�o os dois ao mesmo tempo;
				Se a receita for complementar, ser� o 2; caso contrario o 1. Assim sabemos no CTE3 qual valor seleccionar e nao ha que enganar nos totalizadores de linhas
				abaixo :)
			*/
			,CheckCompart = case
								when ft.u_ltstamp2 = ctltrct.ctltrctstamp then 2
								when ft.u_ltstamp = ctltrct.ctltrctstamp then 1
								end
		into
			#dadosReceitas
		from
			ctltrct				(nolock)
			inner join ft		(nolock) on (ft.u_ltstamp=ctltrctstamp or ft.u_ltstamp2=ctltrctstamp)
			inner join ft2		(nolock) on ft.ftstamp=ft2.ft2stamp
			inner join fi		(nolock) on fi.ftstamp=ft.ftstamp and ((fi.u_ettent1!=0 and ft.u_ltstamp=ctltrctstamp) or (fi.u_ettent2!=0 and ft.u_ltstamp2=ctltrctstamp))
		where
			ano	= @ano2
			and mes	= @mes2
			and ctltrct.cptorgabrev	= @cptorgabrev
			and ctltrct.site = @site2
			and ctltrct.u_fistamp!='' -- J� Faturado
		group by
			ft.u_ltstamp, ft.u_ltstamp2, ctltrct.ctltrctstamp, ctltrct.cptplacode
			,ft.ftstamp, ft.fdata
			,ctltrct.ano, ctltrct.mes, ctltrct.slote, ctltrct.tlote, ctltrct.lote, ctltrct.nreceita,ctltrct.receita
			,ft2.u_entpresc, ft2.u_nbenef2, ft2.u_nopresc

		/*
			tabela agrupada dos Lotes em detalhe individual
		*/
		select
			cptplacode, slote, tlote, lote
			,TotalCompartLote = sum(dr.TotalCompartLinhas)
			,TotalReceitasLote = sum(dr.TotalReceita)
			,NumReceitas = count(dr.nreceita)
			,TotalEmbalagensPorLote = sum(dr.TotalEmbalagens)
			,TotalPagPrim = sum(dr.TotalPagPrim)
			,cptpla.id_anf
		into
			#lotes
		from
			#dadosReceitas dr
			inner join cptpla (nolock) on cptpla.codigo = dr.cptplacode
		group by
			dr.cptplacode, dr.slote, dr.tlote, dr.lote, cptpla.id_anf


		/*
			Contagem: Total de Todos os lotes enviados 
		*/
		select
			TotalLotes = sum(y.QuantidadeLotes)
		into
			#totalQtPorLote
		from (
			select
				QuantidadeLotes = max(lote)
			from
				#dadosReceitas dr
			group by
				dr.tlote
		) y
	
		/*
			Dados de Linhas (detalhe), ja filtradas por valor compartiicpado na tabela tempor�ria acima
		*/
		select
			fi.ftstamp, fi.ref, fi.lordem
			,u_epvp = fi.u_epvp * fi.qtt
			,fi.u_diploma
			/*
				Valor da Entidade: devolve sempre um campo. o CTE de cabe�alho sabe se a receita � complementar ou nao, e aqui avalia-se
				se for complementar, usar o u_ettent2; caso contrario o 1
			*/
			,entidade = case
							when dr.CheckCompart=2 and fi.u_ettent2<>0 then fi.u_ettent2
							when dr.CheckCompart=1 and fi.u_ettent1<>0 then fi.u_ettent1
							/* Se nenhuma das condi�oes acima for valida, ha embs nao comparticipadas na venda, ignorar reportar zero (0). */
							else 0
							end
			,entidadePrim = case -- valor comparticipado pela entidade principal (SNS)
								when dr.CheckCompart=2 then fi.u_ettent1
								when dr.CheckCompart=1 then fi.u_ettent2
								else 0
								end
			,dr.fdata
			,fi.u_codemb
			,codUniEmb = isnull(cptpla.codUniEmb,0)
		into
			#detalhe
		from
			fi (nolock)
			inner join #dadosReceitas dr on fi.ftstamp=dr.ftstamp
			left join cptpla on cptpla.codigo = dr.cptplacode
		where
			fi.ref != ''
			and (fi.u_ettent1<>0 or fi.u_ettent2<>0)

	


		/* fix SCML, n�o enviar referencias comparticipadas exclusivamente pelo SNS */
		if(@cptorgabrev='SCML')
		begin
			delete from #detalhe where entidade = 0
		end


	

		/*
			Dados da Ultima Factura Entidades, incluindo totais de Linhas
		*/
		select top 1
			ft.ndoc, ft.fno, ft.etotal
			,TotalPVP = sum(fi.altura)
			,TotalUtente = sum(fi.largura)
			,TotalEntidade = sum(fi.altura) - sum(fi.largura)
		into
			#FatEntidades
		from
			ft (nolock)
			inner join fi (nolock) on ft.ftstamp = fi.ftstamp
		where
			ft.ndoc	= (select top 1 serie_ent from empresa (nolock) where site = @site)
			and ft.no = (select top 1 u_no from cptorg (nolock) where abrev = @cptorgabrev)
			and ft.ftano = @ano2
			and month(ft.fdata) = @mes2
			and site = @site2
		group by
			ft.ndoc, ft.fno, ft.ftano, ft.fdata, ft.etotal
		order by
			ft.fno desc



		

		/*
			SELECT que encapsula a gera��o de XML para depois se converter em TEXT
		*/
		select
			anf = convert(text,(
				select
					'<?xml version = ''1.0'' encoding = ''iso-8859-1'' ?>' + replace(convert(varchar(max),(
						select
							'CodigoFarmacia'				= replicate('0',5-len((select top 1 infarmed from empresa (nolock) where site = @site))) + convert(varchar,(select top 1 infarmed from empresa (nolock) where site = @site))
							,'NumeroFactura'				= left((select convert(varchar,fe.ndoc) + '/' + convert(varchar,fe.fno) from #fatEntidades fe),15)
							,'EntidadeResponsavel'			= isnull((select id_anf from cptorg where abrev=@cptorgabrev),'02')
															--case 
															--	when @cptorgabrev2='IASFA' then '15'
															--	when @cptorgabrev2='CGD' then '13'
															--	when @cptorgabrev2='PT/CTT' then '09'
															--	when @cptorgabrev2='PSP/SAD' then '18'
															--	when @cptorgabrev2='DSAD' then '17'
															--	when @cptorgabrev2='SBSI' then 'BA'
															--	when @cptorgabrev2='CML/SS' then '03'
															--	when @cptorgabrev2='SNQTB' then '25'
															--	when @cptorgabrev2='ASTELLAS' then 'AS'
															--	when @cptorgabrev2='MULTICARE' then 'X1'
															--	when @cptorgabrev2='LUND' or @cptorgabrev='LUNDECK' then 'QR'
															--	when @cptorgabrev2='NOVARTISAPOIO' or @cptorgabrev='NOVARTIS' then 'WP'
															--	when @cptorgabrev2='SANOFI' then 'WQ'
															--	when @cptorgabrev2='OPP' then 'WJ'
															--	when @cptorgabrev2='SCML' then '30'
															--	when @cptorgabrev2='SCML-ES' then '53'
															--	when @cptorgabrev2='SAVIDA' then 'AA'
															--	when @cptorgabrev2='MEDIS' then 'F0'
															--	when @cptorgabrev2='MEDIS/CTT' then 'JC'
															--	when @cptorgabrev2='CMO' then 'CO'
															--	when @cptorgabrev2='SBN' then '12'
															--	when @cptorgabrev2='SBC' then '11'
															--	when @cptorgabrev2='TRANQUILIDADE' then '77'
															--	when @cptorgabrev2='SIB' then 'W0'
															--	when @cptorgabrev2='LIBERTY' then 'LS'
															--	when @cptorgabrev2='INCM/SS' then '08'
															--	when @cptorgabrev2='GENERALI' then 'S1'
															--	when @cptorgabrev2='FIDELIDADE' then 'FM'
															--	when @cptorgabrev2='ESUMEDICA' then '73'
															--	when @cptorgabrev2='APL' then '04'
															--	when @cptorgabrev2='APDL' then '36'
															--	else '02'
															--	end
							,'Ano'							= @ano2
							,'Mes'							= case when len(convert(varchar,@mes2))=1 then '0'+convert(varchar,@mes2) else convert(varchar,@mes2) end
							,'QuantidadeLotesEnviados'		= convert(numeric(9,0),(select tpl.TotalLotes from #totalQtPorLote tpl))
							,'QuantidadeReceitasEnviadas'	= convert(numeric(9,0),(select count(dr.nreceita) from #dadosReceitas dr))
							,'QuantidadeEmbalagensEnviadas'	= convert(numeric(9,0),(select count(distinct(d.ref)) from #detalhe d))
							,'TotalPVPEnviado'				= convert(numeric(12,0),(select fe.TotalPVP * 100 from #fatEntidades fe))
							,'TotalPVUEnviado'				= convert(numeric(12,0),(select fe.TotalUtente * 100 from #fatEntidades fe))
							,'TotalEntidadeEnviado'			= convert(numeric(12,0),(select fe.TotalEntidade * 100 from #fatEntidades fe))
							,'TotalPagPrim'					= convert(numeric(12,0),(select sum(fe.TotalPagPrim) * 100 from #dadosReceitas fe))
							, (
								select
								( 
									select 
										/*
											Patch: Mapeamento 1 para 1 dos 7 planos de Comparticip��o actuais da ADSE, 
											conforme requisito clarificado pela ANF a 19/09/2012
										*/
										'SubSistema'					= l.id_anf
										,'TipoLote'						= left(convert(varchar,l.tlote),3)
										,'SerieLote'					= left(convert(varchar,l.slote),4)
										,'NumeroLote'					= convert(numeric(7,0),l.lote)
										,'QuantidadeReceitasEnviadas'	= convert(numeric(9,0),l.NumReceitas)
										,'QuantidadeEmbalagensEnviadas'	= convert(numeric(9,0),l.TotalEmbalagensPorLote)
										,'TotalPVPEnviado'				= convert(Numeric(12,0),l.TotalReceitasLote * 100)
										,'TotalPVUEnviado'				= convert(Numeric(12,0),(l.TotalReceitasLote- l.TotalCompartLote) * 100)
										,'TotalEntidadeEnviado'			= convert(Numeric(12,0),l.TotalCompartLote * 100)
										,'TotalPagPrim'					= convert(Numeric(12,0),l.TotalPagPrim * 100)
										,(
											SELECT (
												select
													'Local'								= left(dr.u_entpresc,20)
													,'Prescritor'						= '' /* left(cte1.u_nopresc,20) Comentado segundo indica��es da ANF */
													,'NumeroReceita'					=  case when  (@cptorgabrev='SCML') then left(dr.receita,20)  else left(dr.nreceita,20) end /* No caso SSCML deve ser enviado o numero da receita*/
													,'NumeroVia'						= 1
													,'NumeroSequencia'					= convert(numeric(3,0),row_number() over (order by dr.nreceita))
													,'QuantidadeEmbalagensEnviadas'		= convert(numeric(9,0),dr.TotalEmbalagens)
													,'TotalPVPEnviado'					= convert(numeric(12,0),sum(dr.TotalReceita) * 100)
													,'TotalPVUEnviado'					= convert(numeric(12,0),(sum(dr.TotalReceita) - sum(dr.TotalCompartLinhas))*100)
													,'TotalEntidadeEnviado'				= convert(numeric(12,0),sum(dr.TotalCompartLinhas) * 100)								
													,'NumeroUtente'						= left(convert(varchar,dr.u_nbenef2),15)
													,'TotalPagPrim'						= convert(numeric(12,0),sum(dr.TotalPagPrim) * 100)
													,(
														select (
															select
																'RegistoInfarmed'		= left(convert(varchar,d.ref),7)
																,'NumeroLinhaReceita'	= convert(numeric(4,0),row_number() over (order by d.ref))
																,'DataDispensa'			= convert(varchar,d.fdata,126)+'+00:00'
																,'PVPEnviado'			= convert(numeric(9,0),d.u_epvp * 100)
																,'PVUEnviado'			= convert(numeric(9,0),(d.u_epvp - d.entidade) * 100)
																,'ValorEntidadeEnviado'	= convert(numeric(12,0),(d.entidade)*100)
																,'NumeroDespacho'		= left(replace(d.u_diploma,' ',''),20)
																,'NumeroTransacao'		= ''
																,'PagPrim'				= convert(numeric(12,0),(d.entidadePrim)*100)
																,'CodAutorizacao'		= case when @cptorgabrev = 'NOVONORDISK' then
																								left(convert(varchar,d.u_codemb),9)
																							else
																								case when d.codUniEmb=1 then case when d.u_codemb='' then null else d.u_codemb end else null end
																							end 
															from
																#detalhe d
															where
																d.ftstamp = dr.ftstamp
															group by
																d.ftstamp, d.ref, d.lordem, d.u_epvp, d.u_diploma, d.fdata, d.entidade, d.entidadePrim, d.u_codemb, d.codUniEmb
															for
																xml path ('EmbalagemDispensada'), type
															)
														for
															xml path ('EmbalagensDispensadas'), type
													)
												from
													#dadosReceitas dr
												where
													dr.slote = l.slote
													and dr.tlote = l.tlote
													and dr.lote = l.lote
													and dr.cptplacode = l.cptplacode
												group by
													dr.ftstamp, dr.u_entpresc, dr.nreceita, dr.u_nbenef2, dr.u_nopresc, dr.TotalEmbalagens,  dr.cptplacode, dr.receita
												order by
													dr.nreceita
												for
													xml path ('ReceitaDispensada'), type
											)
											for
												xml path  ('ReceitasDispensadas'), type
										)
									from
										#lotes l
									group by	
										l.slote, l.tlote, l.lote, l.TotalReceitasLote, l.NumReceitas, l.TotalCompartLote, l.TotalEmbalagensPorLote, l.TotalPagPrim, l.cptplacode, l.id_anf
									order by
										l.slote, l.tlote, l.lote
									for 
										xml Path ('LoteMedicamentos'), type
								)
								for 
									xml path ('LotesMedicamentos'), type
							)
						for 
							xml	 path(''),  root ('FacturaMedicamentos'), type)
					),'<FacturaMedicamentos>','<FacturaMedicamentos xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNameSpaceSchemaLocation="wsFacturaMedicamentos.xsd">') /* Fim do REPLACE*/
			))
	end

go
grant execute on dbo.up_receituario_GerarFileANF to public 			
grant execute on dbo.up_receituario_GerarFileANF to public 
go