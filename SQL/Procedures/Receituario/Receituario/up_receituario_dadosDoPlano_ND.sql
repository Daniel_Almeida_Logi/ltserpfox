/* Devolver plano NOVONORDKISK

 exec up_receituario_dadosDoPlano_ND 'Loja 2'
*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_dadosDoPlano_ND]') IS NOT NULL
	drop procedure dbo.up_receituario_dadosDoPlano_ND
go

create procedure dbo.up_receituario_dadosDoPlano_ND
	@site varchar (60)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	DECLARE @codigoPlano VARCHAR(3) 

	SET @codigoPlano = (CASE 
							WHEN LTRIM(RTRIM((SELECT assfarm FROM empresa(nolock) WHERE site = @site))) = 'ANF'
								THEN 'ND1'
							ELSE 'ND'
					   END)

	EXEC up_receituario_dadosDoPlano @codigoPlano
	

GO
Grant Execute on dbo.up_receituario_dadosDoPlano_ND to Public
Grant Control on dbo.up_receituario_dadosDoPlano_ND to Public
GO