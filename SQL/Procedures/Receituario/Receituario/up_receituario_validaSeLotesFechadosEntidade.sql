/* 
 Valida se deve os lotes est�o todos fechados

			exec up_receituario_validaSeLotesFechadosEntidade 1,23 ,2024,3,'Loja 1'
  			exec up_receituario_validaSeLotesFechadosEntidade 15,0 ,2021,4,'Loja 1'


			exec up_receituario_validaSeLotesFechadosEntidade 1,0 ,2021,4,'Loja 1'




*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_validaSeLotesFechadosEntidade]') IS NOT NULL
	drop procedure dbo.up_receituario_validaSeLotesFechadosEntidade
go

create procedure dbo.up_receituario_validaSeLotesFechadosEntidade


@no      int,
@estab   int,
@ano     int,
@mes     int,
@site    varchar(18)



/* WITH ENCRYPTION */
AS

SET NOCOUNT ON
	
	declare @count int = 0
	declare @abrev varchar(15) = ''
	declare @lote  varchar(25) = ''




	;with
		
	 
		cte_facturacao as (
			select
				ft.ftstamp, fistamp, u_epvp, qtt, u_ettent1, u_ettent2, u_ltstamp, u_ltstamp2
				,fi.u_generico, fi.pvp4_fee, fi.pic, pvp4 = isnull(fprod.preco_acordo,0)
			from
				fi (nolock)
				inner join ft (nolock)	on ft.ftstamp=fi.ftstamp
				left join fprod (nolock) on fprod.cnp = fi.ref
			where
				ftano = @ano and month(fdata)= @mes
				and (
					(u_ltstamp != '' and u_ettent1 > 0)
					or
					(u_ltstamp2 != '' and u_ettent2 > 0)
				)
				and ft.site in (select * from up_SplitToTable(@site,','))/*= (case when @site = '' Then ft.site else @site end)*/
		)

	 
	select 
		@abrev = rtrim(ltrim(isnull(cptorgabrev,''))),
		@count = count(*)
	from
		ctltrct(nolock)
	INNER JOIN cptorg		(nolock) ON ctltrct.cptorgstamp = cptorg.cptorgstamp
	INNER JOIN b_utentes	(NOLOCK) ON b_utentes.no  = (select no from uf_retornaDadosEntidadeFacturacao (cptorgabrev,@site,getdate()))	and b_utentes.estab = (select estab from uf_retornaDadosEntidadeFacturacao (cptorgabrev,@site,getdate()))
	inner join cte_facturacao ft	ON ctltrct.ctltrctstamp = ft.u_ltstamp or ctltrct.ctltrctstamp = ft.u_ltstamp2
	where 
		mes = @mes
		and ano = @ano
		and fechado = 0
		and site = @site
		and b_utentes.no = @no
		and b_utentes.estab = @estab
		
	group by cptorgabrev,lote



	if isnull(@count,0)>0
	begin
		select @count as total, @abrev as abrev
	end
	else begin
		select 0 as total, '' as abrev
	end

GO
Grant Execute on dbo.up_receituario_validaSeLotesFechadosEntidade to Public
Grant Execute on dbo.up_receituario_validaSeLotesFechadosEntidade to Public
go


