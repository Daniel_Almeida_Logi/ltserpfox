/*
	SP para criar o XML enviado ao WebService da ADSE
	Notas:
	> Removido o parametro SerieFact que era opcional, deixou de funcionar no 2013 nao sei porque

	exec up_receituario_genFactEnt 'ADSE', 2015, 1, 'Loja 1'

*/

	if OBJECT_ID('[dbo].[up_receituario_genFactEnt]') IS NOT NULL
		drop procedure dbo.up_receituario_genFactEnt
	go
	
	create procedure [dbo].up_receituario_genFactEnt
		@cptorgabrev varchar(254)
		,@ano numeric(4,0)
		,@mes numeric(2,0)
		,@site varchar(254)
		
	/* with encryption */
	
	as

		begin
			
			declare @xml xml
			declare @SerieFact varchar(5) = convert(varchar(5),(select serie_ent from empresa (nolock) where site = @site))
			
			/*
				Notas:
				> A valida��o se h� receitas ou nao a enviar � feita no PHC e nao nesta SP, cujo proposito � apenas enviar o XML
				> Se o prod nao existir na FPROD temos que mapear algum valor por defeito; codigos internos da farmacia..:?
			*/

			;with
				
				/* CTEs auxiliares do "Cleaner" que sao precisos */
				Num1 (n) AS (SELECT 1 UNION ALL SELECT 1),
				Num2 (n) AS (SELECT 1 FROM Num1 AS X, Num1 AS Y),
				Num3 (n) AS (SELECT 1 FROM Num2 AS X, Num2 AS Y),
				Num4 (n) AS (SELECT 1 FROM Num3 AS X, Num3 AS Y),
				Nums (n) AS (SELECT ROW_NUMBER() OVER(ORDER BY n) FROM Num4),
				
				/* Cleaner que limpa o N de Beneficiario 1 */
				Cleaner1 AS (
					SELECT
						ft2stamp,
						u_nbenef,
						(SELECT
							CASE
								WHEN SUBSTRING(u_nbenef, n, 1) NOT LIKE '[^0-9]' THEN SUBSTRING(u_nbenef, n, 1)
								ELSE ''
							END + ''
						FROM
							Nums
						WHERE
							n <= LEN(u_nbenef)
						FOR XML PATH('')
						) AS u_nbenef_clean
					FROM ft2 (nolock)
				)
				,
				/* Cleaner que limpa o N de Beneficiario 2*/
				Cleaner2 AS (
					SELECT 
						ft2stamp,
						u_nbenef2, 
						(SELECT 
							CASE 
								WHEN SUBSTRING(u_nbenef2, n, 1) NOT LIKE '[^0-9]' THEN SUBSTRING(u_nbenef2, n, 1)
								ELSE ''
							END + ''
						FROM Nums
						WHERE n <= LEN(u_nbenef2)
						FOR XML PATH('')
						) AS u_nbenef2_clean
					FROM ft2 (nolock)
				)
				,
				/* CTE principal que constroi o XML, tabelas e campos */
				cte1(fno,num_ordem1,num_ordem2,num_farmacia,num_receita,numuniben,data_dispensa,qtd,Num_regist_Emb,ValPagarADSE_1,ValPagar_ADSE,ValPagar_BENEF,Cod_Reg_Especial,Cod_medico,Local_Emissao_Presc,ctltrctstamp) as (

					select
						ft.fno,
						/* Adicionar um numero por cada linha: Cada Tipo de Lote � igual a um digito. Presume-se que uma factura ADSE ter� at� 9 tipos de Lote distintos no m�ximo, e 99 lotes de 30 receitas cada */
						Num_Ordem1							= convert(varchar,dense_rank() over (order by tlote)) + (case when len(convert(varchar,ctltrct.lote))=1 then '0'+convert(varchar,ctltrct.lote) else convert(varchar,ctltrct.lote) end) + (case when len(convert(varchar,ctltrct.nreceita))=1 then '0'+ convert(varchar,ctltrct.nreceita) else convert(varchar,ctltrct.nreceita) end )
						/* Ordenar por FISTAMP porque na mesma tipo_lote/lote/receita/VENDA podemos ter o mesmo REF repetido */
						,'Num_Ordem2'						= row_number() over (partition by ctltrct.tlote,ctltrct.lote,ctltrct.nreceita order by fi.fistamp)
						,Num_Farmacia						= (select top 1 infarmed from empresa (nolock) where site = ft.site )
						,Num_Receita						= replicate('0',19-len(convert(varchar,ft2.u_receita))) + convert(varchar,ft2.u_receita)
						,NumUniBen							= case 
																when ft2.u_codigo2!='' then 
																	case 
																		/* Seleccionar o 2� Beneficiario JA LIMPO pelo "Cleaner2" */
																		when ft2.u_nbenef2 !='' then Cleaner2.u_nbenef2_clean 
																		else '000000000' 
																	end 
																else 
																	case 
																		/* Seleccionar o 1� Beneficiario JA LIMPO pelo "Cleaner1" */
																		when ft2.u_nbenef != '' then Cleaner1.u_nbenef_clean 
																		else '000000000' 
																	end 
																end
						,Data_Dispensa						= convert(varchar,datepart(year,ft.fdata)) + case when len(convert(varchar,datepart(month,ft.fdata)))<2 then '0' + convert(varchar,datepart(month,ft.fdata)) else convert(varchar,datepart(month,ft.fdata)) end + case when len(convert(varchar,datepart(day,ft.fdata)))< 2 then '0' + convert(varchar,datepart(day,ft.fdata)) else convert(varchar,datepart(day,ft.fdata)) end
						,Qtd								= convert(int,fi.qtt)
						,Num_regist_Emb						= case when fprod.manipulado=1 then '9999999' else fi.ref end
						/* Campo nao sai para o XML, � usado para calculo apenas e transformado noutro campo */
						,ValPagarADSE_1						= case when (fi.u_ettent1<>0 and ft.u_ltstamp=ctltrctstamp)
																	then fi.u_ettent1
																	when (fi.u_ettent2<>0 and ft.u_ltstamp2=ctltrctstamp)
																	then fi.u_ettent2
																end
						
						/* Valor comparticipado pela ADSE */ 
						,ValPagarADSE						=  replicate('0',10-len(convert(varchar,
																	case when (fi.u_ettent1<>0 and ft.u_ltstamp=ctltrctstamp)
																		then convert(int,round(fi.u_ettent1,2)*100)
																		when (fi.u_ettent2<>0 and ft.u_ltstamp2=ctltrctstamp)
																		then convert(int,convert(int,round(fi.u_ettent2,2))*100)
																	end)))
																+ 
																convert(varchar,
																	case when (fi.u_ettent1<>0 and ft.u_ltstamp=ctltrctstamp)
																		then convert(int,round(fi.u_ettent1,2)*100)
																		when (fi.u_ettent2<>0 and ft.u_ltstamp2=ctltrctstamp)
																		then convert(int,round(fi.u_ettent2,2)*100)
																	end
																)
																
						/* Valor remanescente da linha, calculado mediante o valor pago pela ADSE e assumindo que orestante � pago pelo Utente */										
						,ValPagar_BENEF						= replicate('0',10-len(convert(varchar,
																case 
																	when fi.u_ettent1<>0 and ft.u_ltstamp=ctltrctstamp
																		then convert(int,round(fi.u_epvp * fi.qtt - fi.u_ettent1,2)*100)
																	when fi.u_ettent2<>0 and ft.u_ltstamp2=ctltrctstamp
																		then convert(int,round(fi.u_epvp * fi.qtt - fi.u_ettent2,2)*100)
																end)))
																+
																convert(varchar,
																	case 
																		when fi.u_ettent1<>0 and ft.u_ltstamp=ctltrctstamp
																			then convert(int,round(fi.u_epvp * fi.qtt - fi.u_ettent1,2)*100)
																		when fi.u_ettent2<>0 and ft.u_ltstamp2=ctltrctstamp
																			then convert(int,round(fi.u_epvp * fi.qtt - fi.u_ettent2,2)*100)
																	end
																)
					
						,Cod_Reg_Especial					= ctltrct.tlote
						,Cod_medico							= case when len(ft2.u_nopresc) >= 7 then left(convert(varchar,ft2.u_nopresc),7) else replicate('',7-len(case when ft2.u_nopresc='' then '1' else convert(varchar,ft2.u_nopresc) end)) end
						,Local_Emissao_Presc				= case when len(ft2.u_entpresc) >= 7 then left(convert(varchar,ft2.u_entpresc),7) else replicate('',7-len(case when ft2.u_entpresc='' then '1' else convert(varchar,ft2.u_entpresc) end)) end
						, QtdTotRec							= ctltrct.ctltrctstamp
						
					from ft				(nolock)
					inner join ctltrct	(nolock)			on (ft.u_ltstamp=ctltrctstamp or ft.u_ltstamp2=ctltrctstamp)
					inner join ft2		(nolock)			on ft.ftstamp=ft2.ft2stamp
					
					/* CTEs que limpam os N�s; liga��o por STAMP  */
					inner join Cleaner1 					on Cleaner1.ft2stamp=ft2.ft2stamp
					inner join Cleaner2 					on Cleaner2.ft2stamp=ft2.ft2stamp
					
					inner join fi		(nolock)			on ft.ftstamp=fi.ftstamp
					left join fprod		(nolock)			on fi.ref=fprod.cnp
					where
						ctltrct.ano							=@ano
						and ctltrct.mes						=@mes
						and ctltrct.cptorgabrev				=@cptorgabrev
						and ( 
							(fi.u_ettent1<>0 and ft.u_ltstamp=ctltrctstamp) 
							or (fi.u_ettent2<>0 and ft.u_ltstamp2=ctltrctstamp)
						)
						and ft.site in (select * from up_SplitToTable(@site,','))/*= (case when @site = '' Then ft.site else @site end)*/

				)
				
			/* A conversao para TEXT nao � o ideal mas o FoxPro so lida com strings grandes assim */
			select convert(text,convert(varchar(max),(
				
				select
				
					 /*HEADER*/ 
					 (	
						select 
							'NIF'							= (select top 1 ltrim(rtrim(ncont)) from empresa (nolock))
							/* Codigos fixos do tipo de Prestador e ID da AFP fornecido pela ADSE */
							,'CodTipent'					= '00030011'
							,'CodAssPro'					= '00430003'				
							,'NumFact'						= (select isnull(max(fno),0) from ft (nolock) where ftano=@ano and datepart(month,ft.fdata)=@mes and ndoc=@SerieFact and no=(select no from b_utentes (nolock) where no=(select u_no from cptorg (nolock) where abrev=@cptorgabrev)))
							/* Esta data NAO e dinamica, � a data usada no WHERE da FT. Acrescentar o zero (0) se o digito do Mes for 1, apenas. */
							,'AnoMesFact'					= convert(varchar,@ano) + case when len(convert(varchar,@mes))=1 then '0'+convert(varchar,@mes) else convert(varchar,@mes) end
							/* Serie Factura Entidades, string fixo, filling ate 4 chars. Se nao for indicado valor ao parametro o SQL Server assume o valor 73 */
							,'NumSerie'						= replicate('0',4-len(@SerieFact)) + @SerieFact
							,'DataEnvio'					= convert(varchar,datepart(year,getdate())) + case when len(convert(varchar,datepart(month,getdate())))=1 then '0'+convert(varchar,datepart(month,getdate())) else convert(varchar,datepart(month,getdate())) end + case when len(convert(varchar,datepart(day,getdate())))=1 then '0'+convert(varchar,datepart(day,getdate())) else convert(varchar,datepart(day,getdate())) end
							/* Contar as receitas DISTINTAS, excluindo "duplicados" da complementariedade caso existam */
							,'QtdTotRec'					= replicate('0',7-len(convert(varchar,COUNT(cte1.ctltrctstamp)))) + convert(varchar,COUNT(cte1.ctltrctstamp))
							,'ValorFactura'					= replicate('0',10-len(convert(varchar,convert(int,round(sum(cte1.ValPagarADSE_1),2)*100)))) + convert(varchar,convert(int,round(sum(cte1.ValPagarADSE_1),2)*100))				
						from cte1
						for xml path('Header'), type
					)
					
					,
					
					/* BODY*/
					(
						select
							Num_Ordem1,
							Num_Ordem2,
							Num_Farmacia,
							Num_Receita,
							NumUniBen,
							Data_Dispensa,
							Qtd,
							Num_regist_Emb,
							ValPagar_ADSE,
							ValPagar_BENEF,
							Cod_Reg_Especial,
							Cod_medico,
							Local_Emissao_Presc
						from cte1
						for xml path('Body'), type
					)
				
				for xml path(''), root ('TED'), type
				
			))) as myXML
			
	end

Go
Grant Execute on dbo.up_receituario_genFactEnt to Public
Grant Control on dbo.up_receituario_genFactEnt to Public
Go