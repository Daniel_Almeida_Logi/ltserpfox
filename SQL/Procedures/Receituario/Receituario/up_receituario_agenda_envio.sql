/* 
	Insere documentos para agendar envio

	DECLARE @RunStoredProcSQL VARCHAR(1000);
    SET @RunStoredProcSQL = 'EXEC [LTSMSB].[dbo].up_receituario_agenda_envio ''ADM0121EA8D-0734-4716-8F4'', ''ADM14479606-595F-4325-96E'', ''LTDEV30'', ''2021'', ''4'',  ''Loja 1'',  ''SQL01'',  ''ADM'',''1'''
    EXEC (@RunStoredProcSQL) AT [sqllogitools];


	select *from [docsCron]
*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_agenda_envio]') IS NOT NULL
	drop procedure up_receituario_agenda_envio
go

CREATE procedure [dbo].up_receituario_agenda_envio
     @stamp     varchar(36)
	,@docStamp  varchar(36)
	,@idCli     varchar(18) 
	,@year      int 
	,@month     int  
	,@site      varchar(18)   
	,@server    varchar(18) 
	,@user      varchar(18)
	,@prio      int
/* with encryption */

AS
SET NOCOUNT ON

	delete from [docsCron] where  ousrdata < DATEADD(year, -1, GETDATE())

	delete from [docsCron] where [docStamp] = @docStamp and [clientId]=@idCli and [site]=@site

	INSERT INTO [dbo].[docsCron]
           ([stamp]
           ,[token]
           ,[docStamp]
           ,[clientId]
           ,[year]
           ,[month]
           ,[site]
           ,[ousrinis]
           ,[server]
		   ,[priority]

           )
     VALUES
           (
		   left(newid(),30)
		   ,@stamp
           ,@docStamp
           ,@idCli
           ,@year
           ,@month
           ,@site
           ,@user
           ,@server
		   ,@prio
		   )
	


GO
Grant Execute On up_receituario_agenda_envio to Public
Grant Control On up_receituario_agenda_envio to Public
GO