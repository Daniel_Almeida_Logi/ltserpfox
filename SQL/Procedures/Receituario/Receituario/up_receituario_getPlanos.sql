/* Ver Listagem de Planos (FUNÇÕES DA VENDA)

	 exec up_receituario_getPlanos 'ANF','','','',, '01',0
	 		
	 exec up_receituario_getPlanos 'ANF','','01','',0,'',1
	 

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_getPlanos]') IS NOT NULL
	drop procedure dbo.up_receituario_getPlanos
go

create procedure dbo.up_receituario_getPlanos

@ass varchar(3)
,@entidade varchar(15) = ''
,@codigo varchar(3) = ''
,@design varchar(50) = ''
,@mostrainativos bit = 0
,@complementar varchar(10) = ''
,@mostraPlanosSemComplSNS bit = 0

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	If OBJECT_ID('tempdb.dbo.#dadosPlanosTemp') IS NOT NULL
		drop table #dadosPlanosTemp;

declare @dateToConsiderEND date;
declare @dateToConsiderInit date;

set   @dateToConsiderEND = GETDATE();
set   @dateToConsiderInit = GETDATE();

if(@mostrainativos=1)
begin
	set @dateToConsiderEND = '1900-01-01'
	set @dateToConsiderInit = '3000-01-01'
end



SELECT
	sel	= 0
	,a.cptorgabrev
	,a.codigo
	,a.design
	,a.dplmsstamp
	,codigo2		= Convert(varchar(3),a.compl)
	,design2		= Isnull(b.design,'')
	,cptorgabrev2	= Isnull(b.cptorgabrev, '')
	,a.compl
	,beneficiario2 	= isnull((select beneficiario from cptpla cx (nolock) where codigo=a.compl),0)
	,a.inactivo
	,cptorg.percadic
Into
	#dadosPlanosTemp
FROM
	cptpla a (nolock)
	inner join cptorg (nolock) on cptorg.cptorgstamp=a.cptorgstamp
	left join cptpla b (nolock) on b.codigo = convert(varchar(3),a.compl)

where
	(cptorg.u_assfarm=@ass OR cptorg.u_assfarm='TOD')
	and a.inactivo = case when @mostrainativos = 1 then a.inactivo else 0 end
	and cptorg.inactivo = case when @mostrainativos = 1 then cptorg.inactivo else 0 end
	and a.cptorgabrev like @entidade  + '%'
	and a.codigo like @codigo + '%'
	and a.design like '%' + @design + '%'
	and a.compl like '%' + @complementar
	and a.dataInicio <= case when @mostrainativos = 1 then @dateToConsiderInit ELSE GETDATE() END
	and a.dataFim >= case when @mostrainativos = 1 then @dateToConsiderEND ELSE GETDATE() END

ORDER BY
	cptorgabrev




 if(@mostraPlanosSemComplSNS=1 and  (@entidade= "" and @codigo="" and @design="")  )
 begin

	SELECT * from 	#dadosPlanosTemp

	Union 

	 SELECT
		sel	= 0
		,a.cptorgabrev
		,a.codigo
		,a.design
		,a.dplmsstamp
		,codigo2		= Convert(varchar(3),a.compl)
		,design2		= Isnull(b.design,'')
		,cptorgabrev2	= Isnull(b.cptorgabrev, '')
		,a.compl
		,beneficiario2 	= isnull((select beneficiario from cptpla cx (nolock) where codigo=a.compl),0)
		,a.inactivo
		,cptorg.percadic
		
	FROM
		cptpla a (nolock)
		inner join cptorg (nolock) on cptorg.cptorgstamp=a.cptorgstamp
		left join cptpla b (nolock) on b.codigo = convert(varchar(3),a.compl)
		where
			(cptorg.u_assfarm=@ass OR cptorg.u_assfarm='TOD')
		and a.inactivo = case when @mostrainativos = 1 then a.inactivo else 0 end
		and cptorg.inactivo = case when @mostrainativos = 1 then cptorg.inactivo else 0 end
		and a.cptorgabrev != 'SNS' and a.compl=''
		and a.mostraComplementar = 1
		and a.dataInicio <= case when @mostrainativos = 1 then @dateToConsiderInit ELSE GETDATE() END
		and a.dataFim >= case when @mostrainativos = 1 then @dateToConsiderEND ELSE GETDATE() END

	 end
 else begin
	SELECT  * from 	#dadosPlanosTemp	
 end



 	If OBJECT_ID('tempdb.dbo.#dadosPlanosTemp') IS NOT NULL
		drop table #dadosPlanosTemp;

GO
Grant Execute on dbo.up_receituario_getPlanos to Public
Grant Execute on dbo.up_receituario_getPlanos to Public
go