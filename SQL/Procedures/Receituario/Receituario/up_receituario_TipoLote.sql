/*

 exec up_receituario_TipoLote '2012','8', 'sns', 'Loja 1'

 select assfarm from empresa
 
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_TipoLote]') IS NOT NULL
	drop procedure dbo.up_receituario_TipoLote
go



Create procedure dbo.up_receituario_TipoLote

@Ano		NUMERIC(4)
,@Mes		NUMERIC(2)
,@abrev		varchar(15)
,@site      varchar(254) = ''

/* with encryption */
AS
SET NOCOUNT ON

IF @abrev = 'TODAS'
begin
	set @abrev = ''
end

select 
	'TODOS' as tlote

Union ALL

Select Distinct
	tlote
from
	ctltrct (nolock)
where
	cptorgabrev = @abrev
	and ano = @ano
	and mes = @Mes
	and site in (select * from up_SplitToTable(@site,','))/*= case when @site='' then site else @site end*/
/*order by tlote*/

Go
Grant Execute on dbo.up_receituario_TipoLote to Public
Grant Control on dbo.up_receituario_TipoLote to Public
Go
