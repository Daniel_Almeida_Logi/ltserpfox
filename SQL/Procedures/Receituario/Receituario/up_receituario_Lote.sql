/* Receituário por Lote

 exec up_receituario_Lote '2012','8', 'sns', '10', 'Loja 1'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_Lote]') IS NOT NULL
	drop procedure dbo.up_receituario_Lote
go

Create procedure dbo.up_receituario_Lote

@Ano		NUMERIC(4)
,@Mes		NUMERIC(2)
,@abrev		varchar(15)
,@tlote		varchar(2)
,@site      varchar(254) = ''

/* with encryption */
AS
SET NOCOUNT ON

SELECT Distinct
	lote
from
	ctltrct (nolock)
where
	cptorgabrev	= @abrev
	and ano		= @Ano
	and mes		= @mes
	and tlote	= @tlote
	and site	in (select * from up_SplitToTable(@site,','))/*= @site*/
order by
	lote

Go
Grant Execute on dbo.up_receituario_Lote to Public
Grant Control on dbo.up_receituario_Lote to Public
Go