/*
	SP para gerar ficheiro de Cabe�aho para o XML - detalhe da ANF
	
	exec up_receituario_GerarCabFileANF 2016,03,'sandoz','Loja 1'

	exec up_receituario_GerarCabFileANF 2018,10,'SCML','Loja 1'


*/

if OBJECT_ID('[dbo].[up_receituario_GerarCabFileANF]') IS NOT NULL
	drop procedure dbo.up_receituario_GerarCabFileANF
go

create procedure dbo.up_receituario_GerarCabFileANF
	@ano numeric(4,0)
	,@mes numeric(2,0)
	,@cptorgabrev varchar(15)
	,@site  varchar(254)
	
/* with encryption */

as
	begin
		
		set language portuguese
		set	nocount on 
		
		;with

			cte1 as (
				select
					ft.ftstamp,ft.fdata,
					ctltrct.ano,ctltrct.mes,ctltrct.slote,ctltrct.tlote,ctltrct.lote,ctltrct.nreceita,
					ft2.u_entpresc,ft2.u_nbenef,ft2.u_nopresc
					,TotalCompartLinhas = sum(fi.u_ettent1+u_ettent2)
					--,TotalReceita = sum(ft.etotal)
					,TotalReceita = sum(fi.u_epvp*fi.qtt)
					,TotalEmbalagens = sum(fi.qtt)
				from
					ctltrct (nolock)
					inner join ft (nolock)	on (ft.u_ltstamp=ctltrctstamp or ft.u_ltstamp2=ctltrctstamp)
					inner join ft2 (nolock)	on ft.ftstamp=ft2.ft2stamp
					inner join fi (nolock)	on fi.ftstamp=ft.ftstamp and ((fi.u_ettent1!=0 and ft.u_ltstamp=ctltrctstamp) or (fi.u_ettent2!=0 and ft.u_ltstamp2=ctltrctstamp))
				where
					ano					= @ano
					and mes				= @mes
					and cptorgabrev		= @cptorgabrev
					and ctltrct.site	= @site
				group by
					ft.ftstamp,ft.fdata,
					ctltrct.ano,ctltrct.mes,ctltrct.slote,ctltrct.tlote,ctltrct.lote,ctltrct.nreceita,
					ft2.u_entpresc,ft2.u_nbenef,ft2.u_nopresc
			),

			cte2 (TotalLotes) as (
				select
					sum(y.QuantidadeLotes)
				from (
					select
						QuantidadeLotes = max(lote)
					from cte1
					group by
						cte1.tlote
				) y
			),
				
			cte3 as (
				select
					fi.ftstamp,fi.ref,fi.lordem
					,fi.u_epvp
					,fi.u_diploma
					,fi.u_ettent1,fi.u_ettent2
					,cte1.fdata
				from
					fi (nolock)
					inner join cte1 on fi.ftstamp=cte1.ftstamp
				where
					/* no caso da SCML, n�o devem utilizas linhas comparticipadas unicamente pelo SNS */
					1 = case when @cptorgabrev !='SCML' then 1
								else case when fi.u_ettent1 > 0 then 1 else 0 end
							end
			),
			
			cte5 as (
				SELECT TOP 1
					ft.ndoc,ft.fno,ft.etotal,
					TotalPVP = sum(fi.altura),
					TotalUtente = sum(fi.largura),
					TotalEntidade = sum(fi.altura) - sum(fi.largura) 
				from
					ft 				(nolock)
					inner join fi	(nolock) on ft.ftstamp=fi.ftstamp
				where
					ft.ndoc						 = (select serie_ent from empresa (nolock) where site = @site)
					and ft.no					 = (select u_no from cptorg (nolock) where abrev=@cptorgabrev)
					and ft.ftano				 = @ano
					and datepart(month,ft.fdata) = @mes
					and site					 = @site
				group by
					ft.ndoc,ft.fno,ft.ftano,ft.fdata,ft.etotal
				order by
					ft.fno desc
			)

		select
			anf2 =  convert(text,'<?xml version = ''1.0'' encoding = ''iso-8859-1'' ?>' + convert(varchar(max),(
				select
					'ident'				= left((select substring(textValue,1,patindex('%;%',textValue)-1) from B_Parameters where stamp='ADM0000000169'),30)
					,(
					select
						'codFarm'		= replicate('0',5-len((select top 1 infarmed from empresa (nolock) where site = @site))) + convert(varchar,(select top 1 infarmed from empresa (nolock) where site = @site))
						,'entidade'		= isnull((select id_anf from cptorg where abrev=@cptorgabrev),'02')
										--case
										--	when @cptorgabrev='IASFA' then '15'
										--	when @cptorgabrev='CGD' then '13'
										--	when @cptorgabrev='PT/CTT' then '09'
										--	when @cptorgabrev='PSP/SAD' then '18'
										--	when @cptorgabrev='DSAD' then '17'
										--	when @cptorgabrev='SBSI' then 'BA'
										--	when @cptorgabrev='CML/SS' then '03'
										--	when @cptorgabrev='SNQTB' then '25'
										--	when @cptorgabrev='ASTELLAS' then 'AS'
										--	when @cptorgabrev='MULTICARE' then 'X1'
										--	when @cptorgabrev='LUND' or @cptorgabrev='LUNDECK' then 'QR'
										--	when @cptorgabrev='NOVARTISAPOIO' or @cptorgabrev='NOVARTIS' then 'WP'
										--	when @cptorgabrev='SANOFI' then 'WQ'
										--	when @cptorgabrev='OPP' then 'WJ'
										--	when @cptorgabrev='SCML' then '30'
										--	when @cptorgabrev='SCML-ES' then '53'
										--	when @cptorgabrev='SAVIDA' then 'AA'
										--	when @cptorgabrev='MEDIS' then 'F0'
										--	when @cptorgabrev='MEDIS/CTT' then 'JC'
										--	when @cptorgabrev='CMO' then 'CO'
										--	when @cptorgabrev='SBN' then '12'
										--	when @cptorgabrev='SBC' then '11'
										--	when @cptorgabrev='TRANQUILIDADE' then '77'
										--	when @cptorgabrev='SIB' then 'W0'
										--	when @cptorgabrev='LIBERTY' then 'LS'
										--	when @cptorgabrev='INCM/SS' then '08'
										--	when @cptorgabrev='GENERALI' then 'S1'
										--	when @cptorgabrev='FIDELIDADE' then 'FM'
										--	when @cptorgabrev='ESUMEDICA' then '73'
										--	when @cptorgabrev='APL' then '04'
										--	when @cptorgabrev='APDL' then '36'
										--	else '02'
										--	end
						,'numFactura'	= left((select convert(varchar,cte5.ndoc) + '/' + convert(varchar,cte5.fno) from cte5),15)
						,'anoFact'		= @ano
						,'mesFact'		= case when len(convert(varchar,@mes))=1 then '0'+convert(varchar,@mes) else convert(varchar,@mes) end
						,'qtLotes'		= convert(numeric(9,0),(select cte2.TotalLotes from cte2))
						,'qtReceitas'	= convert(numeric(9,0),(select count(nreceita) from cte1))
						,'qtEmbalagens'	= convert(numeric(9,0),(select count(distinct(cte3.ref)) from cte3))
						,'pvpTotal'		= convert(numeric(12,0),(select cte5.TotalPVP * 100 from cte5))
						,'pvuTotal'		= convert(numeric(12,0),(select cte5.TotalUtente * 100 from cte5))
						,'entidadeTotal'= convert(numeric(12,0),(select cte5.TotalEntidade * 100 from cte5))
						,'ptDiabetes'	= 0
						, (
							SELECT
								'KEYWORD_TO_BE_REPLACED_LATER'
							FOR xml path ('byteArray'), type
						)
						FOR xml path ('Factura'), type
					)
				for xml path (''), root ('FacturaFarmacia'), type
			)))
	end

go

grant execute on dbo.up_receituario_GerarCabFileANF to public
grant execute on dbo.up_receituario_GerarCabFileANF to public
go