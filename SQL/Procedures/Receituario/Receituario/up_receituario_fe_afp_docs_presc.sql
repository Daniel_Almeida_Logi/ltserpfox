/*	
	retorna dados de prescricao

	 @stamp - Stamp da tabela ext_esb_doc

    select * from ext_esb_prescription

	exec up_receituario_fe_afp_docs_presc  'B8630A85-8AC0-4B4E-AC81-CE203248841C'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_fe_afp_docs_presc]') IS NOT NULL
	drop procedure up_receituario_fe_afp_docs_presc
go


Create procedure [dbo].up_receituario_fe_afp_docs_presc
	 @stamp VARCHAR(50)

/* with encryption */
AS
	SET NOCOUNT ON


SELECT 	 
       [lotType]
      ,[lotSerie]
      ,[lot]
      ,[prescriptionNr]
      ,[prescriptionId]
      ,[snsNr]
      ,[beneficiaryNr]
      ,[date]
      ,[obs]
      ,[responseCardId]
      ,[responsePlafonId]
      ,[responseTransactionId]
      ,[stamp]

  FROM [dbo].[ext_esb_prescription](nolock)
  where
	stamp_ext_esb_doc = @stamp



	
GO
Grant Execute On up_receituario_fe_afp_docs_presc to Public
Grant Control On up_receituario_fe_afp_docs_presc to Public
GO

