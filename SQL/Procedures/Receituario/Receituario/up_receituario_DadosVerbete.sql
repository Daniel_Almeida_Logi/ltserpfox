/*
	Levandar dados para o verbete --

	 exec up_receituario_DadosVerbete 2015, 1, 'sns', 'Loja 1'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_DadosVerbete]') IS NOT NULL
	drop procedure dbo.up_receituario_DadosVerbete
go

Create procedure dbo.up_receituario_DadosVerbete

@Ano	NUMERIC(4),
@Mes	NUMERIC(2),
@abrev	varchar(60),
@site   varchar(254) = ''

/* with encryption */
AS
SET NOCOUNT ON

IF @abrev = 'TODAS'
begin
	set @abrev = ''
end

select
	 ano
	,mes
	,cptorgabrev
	,tlote
	,MIN(lote) as min
	,MAX(lote) as max
from
	ctltrct (nolock)
WHERE
	RTRIM(LTRIM(cptorgabrev)) = (Case when @abrev != '' THEN @abrev ELSE cptorgabrev END)
	AND ano = @Ano
	and mes = @mes
	and ctltrct.site in (select * from up_SplitToTable(@site,',')) /*= (case when @site = '' Then ctltrct.site else @site end)*/
	
group by ano, mes, cptorgabrev, tlote
order by ano, mes, cptorgabrev, tlote

Go
Grant Execute on dbo.up_receituario_DadosVerbete to Public
Grant Control on dbo.up_receituario_DadosVerbete to Public
Go
