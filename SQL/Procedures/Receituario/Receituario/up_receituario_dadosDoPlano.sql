/* Ver dados do Plano de Comparticipação (STOUCHPOS)

 exec up_receituario_dadosDoPlano 'U01'

 exec up_receituario_dadosDoPlano '00'
*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_dadosDoPlano]') IS NOT NULL
	drop procedure dbo.up_receituario_dadosDoPlano
go

create procedure dbo.up_receituario_dadosDoPlano
	@codigoPlano varchar (3)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

Select
	 beneficiario	= a.beneficiario
	,protocolo		= a.protocolo
	,codigo			= a.codigo
	,design			= a.design
	,cptorgabrev	= a.cptorgabrev
	,diploma		= a.diploma
	,dplmsstamp		= a.dplmsstamp
	,codigo2		= a.compl
	,MODRCT			= a.MODRCT
	,design2		= isnull(b.design,'')
	,cptorgabrev2	= isnull(b.cptorgabrev,'')
	,compl			= a.compl
	,beneficiario2	= isnull(b.beneficiario,0)
	,fistamp		= CONVERT(varchar,'',18)
	,a.maxembrct
	,a.maxembmed
	,a.maxembuni
	,convert(datetime,'19000101') as 'dataReceita'
	,convert(datetime,'19000101') as 'dataDispensa'
	--,convert(bit,0) as 'excepcao'
	,a.cartao
	,a.maxembcartao
	,nCartao = convert(varchar(35),'')
	,obriga_nrreceita = a.obriga_nrreceita
	,obriga_CodEmb = a.codUniEmb
	,codAcesso = CONVERT(varchar(6),'')
	,codDirOpcao = CONVERT(varchar(6),'')
	,token = CONVERT(varchar(40),'')
	,id_validacaoDem = CONVERT(varchar(21),'')
	,"entidade" = CONVERT(INT, RIGHT(rtrim(c.cptorgstamp), 3))
	,"valida_card" = c.valida_card
	, a.e_compart 
	,nomeutente		= CONVERT(varchar,'',60)
	,contactoutente		= CONVERT(varchar,'',20)
	,nomemedico		= CONVERT(varchar,'',60)
	,contactomedico		= CONVERT(varchar,'',20)
	,especialidademedico		= CONVERT(varchar,'',30)
	,localprescricao		= CONVERT(varchar,'',60)
	,nrcartaosns	= CONVERT(varchar,'',30)
	,nrcartao	= CONVERT(varchar,'',30)
	,percadic = c.percadic
	,compfixa = c.compfixa
	,temSimulacao = c.temSimulacao
	,programId  = a.codExt
	,snsExterno = a.snsExterno
	,nrreceita	= CONVERT(varchar,'',30)
	,mcdt       = isnull(a.mcdt,0)
	,pergDem    = isnull(a.pergDem,0)
	,a.regexCartao
	,a.regexCartaoSample
	,a.permiteReserva
	,a.permiteBackoffice
	,a.permiteSuspensa
	,permiteVendaSemCompart = isnull(a.permiteVendaSemCompart,0)
	,ISNULL((SELECT TOP 1 desconto FROM b_utentes(nolock) WHERE no = c.u_no and estab = 0), 0) as descHabitual
	,a.inactivo
	,a.dataInicio
	,a.dataFim
	--1 ativo 
	,planoAtivo = CASE WHEN convert(date,getdate()) >= a.dataInicio AND convert(date,getdate()) <= a.dataFim  and a.inactivo = 0
						THEN 1
						ELSE 0
				  END
	,a.obrigaRegistoVacinacao
	,efr_descr = convert(varchar(100),'') 
	,a.manipulado 
FROM
	cptpla a (nolock)
	left join cptpla b (nolock) on b.codigo = a.compl
	inner join cptorg c (nolock) on a.cptorgstamp = c.cptorgstamp
WHERE
	a.codigo = @codigoPlano

option (optimize for (@codigoPlano unknown))

GO
Grant Execute on dbo.up_receituario_dadosDoPlano to Public
Grant Control on dbo.up_receituario_dadosDoPlano to Public
GO



