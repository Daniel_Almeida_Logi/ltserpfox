/* 
	comunicaca a quantidade de seringas de um mes 
		up_receituario_comunicacao_KitSeringas 2019,7,'LTDEV30'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_comunicacao_KitSeringas]') IS NOT NULL
	drop procedure dbo.up_receituario_comunicacao_KitSeringas
go

Create PROCEDURE [dbo].up_receituario_comunicacao_KitSeringas
	@ano int,
	@mes int,
	@idLoja as varchar(55)


/* with encryption */
AS 
	SET NOCOUNT ON


	SET	@idLoja = LTRIM(RTRIM(@idLoja))


	DECLARE @site VARCHAR (20)
	SET @site = (SELECT TOP 1 site FROM empresa (NOLOCK) WHERE  id_lt = @idLoja)


	;with
		cte_receitas as (
			select
				ctltrctstamp,
				lote, 
				tlote, 
				slote, 
				nreceita
			from
				ctltrct (nolock)
				INNER JOIN tlote (nolock) ON tlote.codigo = ctltrct.tlote
			where
				ano = @ano AND mes = @mes
				AND cptorgabrev = 'SICAD'
				AND fechado = 1 AND u_fistamp != ''

		),
	 
		cte_facturacao as (
			select
				 qtt,  
				 u_ltstamp, 
				 u_ltstamp2,
				 u_ettent1, 
				 u_ettent2
			from
				fi (nolock)
				inner join ft (nolock)	on ft.ftstamp=fi.ftstamp
			where
				ftano = @ano and month(fdata)= @mes
				and (
					(u_ltstamp != '' and u_ettent1 > 0)
					or
					(u_ltstamp2 != '' and u_ettent2 > 0)
				)
				and ft.site = @site  and fi.ref='6694000' 
		)
	 


		select
			[year] = @ano,
			[month] = @mes,
			qtt = SUM(FT.qtt)
		from
			cte_receitas c
			inner join cte_facturacao ft	ON c.ctltrctstamp = ft.u_ltstamp or c.ctltrctstamp = ft.u_ltstamp2

		where
			 case when c.ctltrctstamp=ft.u_ltstamp then u_ettent1 else u_ettent2 end > 0

GO
Grant Execute On dbo.up_receituario_comunicacao_KitSeringas to Public
Grant control On dbo.up_receituario_comunicacao_KitSeringas to Public
GO