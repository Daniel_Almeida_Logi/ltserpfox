/* Listar Receitas e facturas para a adse (validar beneficiários)
	
	exec up_receituario_adseValidaBenef 2012, 3, 'adse', 1

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_adseValidaBenef]') IS NOT NULL
	drop procedure dbo.up_receituario_adseValidaBenef
go

create procedure dbo.up_receituario_adseValidaBenef

@ano numeric(4),
@mes numeric(2),
@abrev varchar(30),
@site  varchar(254) = ''

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select 
	benef	= case when ft2.u_codigo2!='' then ft2.u_nbenef2 else ft2.u_nbenef end
	,cod1 = ft2.u_codigo
	,cod2 = ft2.u_codigo2
	,ctltrct.slote
	,ctltrct.tlote
	,ctltrct.lote
	,ctltrct.nreceita
	,ctltrct.fechado
	,ft.ftstamp
	,ft.fno
	,ft.ndoc
	,ft.fdata
	,cdata = case when ft.cdata='19000101' then ft.fdata else ft.cdata end		/* PATCH ADSE nao aceita data default das reinserçoes de receita sem data */
	,retCode = convert(varchar(3),'')
from
	ctltrct (nolock)
	inner join ft on ft.u_ltstamp=ctltrct.ctltrctstamp or ft.u_ltstamp2=ctltrct.ctltrctstamp
	inner join ft2 on ft2.ft2stamp=ft.ftstamp
where
	ano=@ano and mes=@mes and cptorgabrev=@abrev
	and ctltrct.site = case when @site='' then ctltrct.site else @site end 
order by
	slote,tlote,lote,nreceita

go
grant execute on dbo.up_receituario_adseValidaBenef to public		
grant control on dbo.up_receituario_adseValidaBenef to public
go
