/*

	Author:		Daniel Almeida
	Create date: 18-10-2023
	Description: Retorna a percentagem de comparticipação BAS


	EXEC up_calc_compart_bonusPerc  95.95789521, 5
*/



IF OBJECT_ID('[dbo].[up_calc_compart_bonusPerc ]') IS NOT NULL
    DROP procedure dbo.up_calc_compart_bonusPerc 
GO

CREATE procedure dbo.up_calc_compart_bonusPerc 
	@percentagem NUMERIC(19,9),
	@precisao  NUMERIC(5,0)

AS
	declare @precisao int = 5
	select @precisao = numValue from B_Parameters(nolock) WHERE stamp='ADM0000000373'
	
	set @precisao = isnull(@precisao,5)

	--calcular compart bas sem arrendondamentos a dinheiro
	set @pagUtAtensBonus = @pagUtAtensBonus*@compart/100	
	select @pagUtAtensBonus= Round(@pagUtAtensBonus,@precisao) 

	--valor final em Dinheio
	SELECT round(@pagEntAntesBonus + @pagUtAtensBonus,@precisao) as valor

GO
GRANT EXECUTE on dbo.up_calc_compart_bonusPerc  TO PUBLIC
GRANT Control on dbo.up_calc_compart_bonusPerc  TO PUBLIC
GO


