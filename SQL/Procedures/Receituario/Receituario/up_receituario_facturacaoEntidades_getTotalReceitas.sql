/* 
	Emissão automática de facturação às entidades 

	exec up_receituario_facturacaoEntidades_getTotalReceitas 'ADM8A8E8CC1-44F2-4B3F-AC0'

*/
if OBJECT_ID('[dbo].[up_receituario_facturacaoEntidades_getTotalReceitas]') IS NOT NULL
	drop procedure dbo.up_receituario_facturacaoEntidades_getTotalReceitas
go

create procedure dbo.up_receituario_facturacaoEntidades_getTotalReceitas

@ftstamp	 varchar(50)


/* WITH ENCRYPTION */
AS

	select	
		nrReceitasTotal = count(*)
	from 
		ctltrct (nolock) a
	INNER JOIN 
		 fi (nolock) on fi.bistamp = a.u_fistamp
	where 
		fi.ftstamp = @ftstamp
		and a.fechado = 1
		and a.u_fistamp != ''
	
Go
Grant Execute on dbo.up_receituario_facturacaoEntidades_getTotalReceitas to Public
Grant Control on dbo.up_receituario_facturacaoEntidades_getTotalReceitas to Public
Go


