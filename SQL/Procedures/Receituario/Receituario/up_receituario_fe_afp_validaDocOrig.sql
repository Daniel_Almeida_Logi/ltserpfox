/*	
	Valida se o documento orginal existe e encontra-se valido
	
	exec up_receituario_fe_afp_validaDocOrig   '4529', 'EAB/1071', '20210908', '0'

	
*/




SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_fe_afp_validaDocOrig]') IS NOT NULL
	drop procedure [up_receituario_fe_afp_validaDocOrig]
go

Create procedure [dbo].[up_receituario_fe_afp_validaDocOrig]
	@infarmed	    VARCHAR(10)
	,@nrOrig        VARCHAR(20)
	,@dateOrig      date
	,@test          bit = 0


/* with encryption */
AS
	SET NOCOUNT ON

	declare @shouldIgnore  bit = 0
	select top 1 @shouldIgnore=bool from B_Parameters(nolock) where stamp='ADM0000000360'
	set @shouldIgnore = ISNULL(@shouldIgnore,0)

	set @test = isnull(@test,0)


	 

	--N�o valida se a nota de credito, tem factura integrada
	--controlado pelo cliente

	if(@shouldIgnore=1)
	begin
		select "val" = 1
		return 
	end

	Select 
		"val" = count(stamp)
	From
		ext_esb_doc(nolock)
	Where 
		invalid=0 and
		convert(int,RTRIM(LTRIM(id_ext_pharmacy))) = convert(int,@infarmed) and
		RTRIM(LTRIM(docNr)) = @nrOrig and
		docDate = @dateOrig and
		test = @test
	


GO
Grant Execute On [up_receituario_fe_afp_validaDocOrig] to Public
Grant Control On [up_receituario_fe_afp_validaDocOrig] to Public
GO