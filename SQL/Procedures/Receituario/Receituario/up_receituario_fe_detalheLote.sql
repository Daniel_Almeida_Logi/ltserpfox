/*	
	Detalhe do Lote

	select * from ft where ftano=2015 and month(fdata)=11 and ndoc=72
	select * from fi where ftstamp='ari2C564F30-0CA6-4000-B5B'
	select * from cptorg
	select * from tlote order by codigo
	select * from cptpla where cptorgstamp='ADM001'
	select token,* from ctltrct where ano=2015 and mes=12 and tlote in (96,97,98,99) and cptorgabrev='sns'
	select * from cptpla where cptorgabrev='sns'
	
	exec dbo.up_receituario_fe_detalheLote 2017, 8, 'SNS', '', '', 0, 0, 'Loja 1'
	exec up_receituario_fe_detalheLote 2018,06,'SBSI', '', '', 0, 0, 'Loja 1'
	select * from tlote  where codigo in ('23','10','11','12','18','19','17','15','16','13') order by codigo
	exec up_receituario_fe_detalheLote 2018,06,'SBSI', '', '', 0, 0, 'Loja 1'
	
	exec up_receituario_fe_detalheLote 2024,02,'CGD', '', '', 0, 0, 'Loja 1'






*/




SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



if OBJECT_ID('[dbo].[up_receituario_fe_detalheLote]') IS NOT NULL
	drop procedure [up_receituario_fe_detalheLote]
go

Create procedure [dbo].[up_receituario_fe_detalheLote]
	@ano NUMERIC(4)
	,@mes NUMERIC(2)
	,@abrev	VARCHAR(15)
	,@tloteIni VARCHAR(2)
	,@tloteFim VARCHAR(2)
	,@loteIni NUMERIC(9)
	,@loteFim NUMERIC(9)
	,@site varchar(20)

/* with encryption */
AS
	SET NOCOUNT ON

	If OBJECT_ID('tempdb.dbo.#receitas') IS NOT NULL
		drop table #receitas;
	If OBJECT_ID('tempdb.dbo.#faturacao') IS NOT NULL
		drop table #faturacao;


	

	-- Get nr infarmed
	declare @cod_infarmed varchar(10)
	set @cod_infarmed = isnull((select replicate('0',5-len(infarmed)) + convert(varchar,infarmed) from empresa (nolock) where site = @site),'0')


	select
		c.ctltrctstamp
		,c.ano, c.mes
		,c.receita
		,c.cptorgstamp, c.cptorgabrev
		,c.cptplacode, lote = case when c.cptplacode='RS' then 99 else c.lote  end ----numero de lote alterado a pedido do spms
		, c.tlote, c.slote, c.nreceita
		--,p.tlotedescr
		,tlotedescr = case
						when tlote='96' then 'RSP Sem Sucesso'
						when tlote='97' then 'RSP Com Sucesso'
						when tlote='98' then 'RCP Sem Sucesso'
						when tlote='99' then 'RCP Com sucesso'
						else p.tlotedescr
						end
		,p.design
		,c.via
		,c.fechado, c.u_fistamp
		,c.usrinis, c.usrdata, c.usrhora
		,c.site, c.posto
		,comprovativo_registo = isnull(de.resultado_comprovativo_registo,'')
		,t.loteId
	into
		#receitas
	from
		ctltrct	c (nolock)
		inner join cptpla p (nolock) on c.cptplacode = p.codigo
		inner join tlote  t (nolock) ON t.codigo=c.tlote
		left join dispensa_eletronica de (nolock) on de.token = c.token
	where
		ano = @ano and mes = @mes
		and c.cptorgabrev = case when @abrev = '' then c.cptorgabrev else @abrev end
		and site = case when @site='' then c.site else @site end
		and c.u_fistamp != ''
		AND (tlote BETWEEN
				(case when @tloteIni='' then tlote else @tloteIni end)
				AND
				(case when @tloteFim='' then tlote else @tloteFim end)
			)
		AND (lote BETWEEN
				(case when @loteIni=0 then lote else @loteIni end)
				AND
				(case when @loteFim=0 then lote else @loteFim end)
			)
		and fechado = 1 

	-- create index on ctltrctstamp to optimize
	create index idx_temp_ctltrctstamp on #receitas(ctltrctstamp)	
	


	-- Faturação
	select
		ft.ftstamp
		,ft.nmdoc
		,ft.ndoc
		,ft.fno
		,cdata = case when cdata!='19000101' then ft.cdata else ft.fdata end
		,bidata = case when bidata!='19000101' then ft.bidata else ft.fdata end 
		,ft.vendnm
		,ft2.u_nbenef
		,u_nbenef2 = case when ft2.C2codpost != '' then C2codpost else ft2.u_nbenef2  end
		,ft.u_ltstamp
		,ft.u_ltstamp2
		,fi.fistamp
		,fi.ref
		,fi.u_epref
		,fi.u_epvp
		,fi.qtt
		,fi.u_diploma
		,diploma_id = isnull(convert(int,dplms.diploma_id),0)
		,fi.u_ettent1
		,fi.u_ettent2
		,fi.lordem
		,fi.pvpmaxre
		,fi.opcao
		,grphmgcode = isnull(fprod.grphmgcode,'')
		,r.*
		,generico = isnull(fprod.generico,0)
		,fi.u_codemb
		,"u_codemb_matrix" = isnull((select u_comb_emb_matrix from fi2(nolock) where fi2.fistamp=fi.fistamp),'')
		,fi.id_dispensa_eletronica_d
		,fi.u_generico, pvp4_fee = dbo.uf_calcFee(fi.pvp4_fee), fi.pic, pvp4 = isnull(fprod.preco_acordo,0)
		,fi.iva
		,"transaction_id" =dbo.uf_getTransactionResponseId(isnull(ft_compart.token,""),ft.ftstamp)		
		,"voc_id" = isnull((select 
								top 1 trackingId
							from 
								ext_esb_associatecard(nolock)  ac
							where
								ac.atendStamp = ft.u_nratend 
								and ac.id =  case when ft2.C2codpost != '' then ft2.C2codpost else ft2.u_nbenef2  end				
							order by 
								ac.ousrdata
							desc)
					,"")
		,ft.fdata
		,iDValidacaoDem = isnull(fi2.iDValidacaoDem,'')

	into
		#faturacao
	from


		ft (nolock)
		inner join #receitas r on r.ctltrctstamp = ft.u_ltstamp or r.ctltrctstamp = ft.u_ltstamp2
		inner join ft2 (nolock) on ft2.ft2stamp = ft.ftstamp
		inner join fi (nolock) on ft.ftstamp = fi.ftstamp
		left join  fi2 (nolock) on fi.fistamp = fi2.fistamp
		left join dplms (nolock) on dplms.u_design = fi.u_diploma and dplms.u_design!=''
		left join fprod	(nolock) on fprod.cnp = fi.ref
		left join ft_compart (nolock) on ft2.token_efectivaçao_compl = ft_compart.token and ft2.ft2stamp = ft_compart.ftstamp and ft_compart.anulado=0
	where
		ftano = @ano and month(fdata)= @mes
		--@Ano = case when cdata!='19000101' then year(cdata) else ft.ftano end
		--and @Mes = case when cdata!='19000101' then month(cdata) else month(fdata) end
		and (fi.u_ettent1 > 0 or fi.u_ettent2 > 0)

	-- result set final
	SELECT	
		--sum(comp),sum(pvp),sum(utente)
		*
	FROM (
		SELECT distinct
			a.ctltrctstamp
			,a.ano
			,a.mes
			,data_receita = a.cdata
			,dispensa_data  = bidata
			,a.cptorgabrev
			,a.cptplacode
			,a.design
			,a.slote
			,tlote = rtrim(a.loteId) + ' - ' + a.tlotedescr
			,tlote2 = a.tlote
			,a.lote
			,a.tlotedescr
						,receita = case 
						when a.cptorgabrev = 'SICAD'
							then @cod_infarmed
								+ replicate('0', 4 - len(a.ndoc)) + convert(varchar,a.ndoc)
								+ replicate('0', 10 - len(a.fno)) + convert(varchar,a.fno)
						else a.receita
						end
			,a.nreceita
			,a.via
			,a.comprovativo_registo
			,a.ftstamp
			,a.fistamp
			,a.u_ltstamp
			,a.u_ltstamp2
			,a.fechado
			,facturado = CASE WHEN a.u_fistamp='' THEN CAST(0 as Bit) ELSE CAST(1 As Bit) END			
			,a.site
			,posto = case when a.posto = '' then a.site else a.posto end
			,a.nmdoc
			,a.fno
			,nrUtente = case when a.ctltrctstamp=a.u_ltstamp then a.u_nbenef else a.u_nbenef2 end
			,a.lordem
			,a.ref
			,a.qtt
			,a.u_diploma
			,diploma = a.diploma_id
			,grpHomogeneo = a.grphmgcode
			,pvp = a.u_epvp * a.qtt
			,comp = case when a.ctltrctstamp = a.u_ltstamp then a.u_ettent1 else a.u_ettent2 end
			,compSNSEntidade = 
					case 
						when a.u_ettent1 > 0 and a.u_ettent2 > 0 then a.u_ettent2 
						else 0 
					end
			,compEntidade = 
					case 
						when a.u_ettent1 > 0  then a.u_ettent1 
						else 0 
					end
			,utente = (a.u_epvp * a.qtt) - (case when a.ctltrctstamp=a.u_ltstamp then a.u_ettent1 else a.u_ettent2 end)
			,utenteEntidade = (a.u_epvp * a.qtt) - (a.u_ettent1 +  a.u_ettent2) 
			,valorBase = 
					case 
						when a.u_ettent1 > 0  then round(a.u_ettent1 - (a.u_ettent1 * (iva/100) ) ,2)
						else 0
					end	
			,valorIva = 
					case 
						when a.u_ettent1 > 0  then round((a.u_ettent1 * (iva/100)) ,2)
						else 0
					end				
			,pvp4_fee = dbo.uf_calcFee(case  
							when a.cptorgabrev = 'SNS' and a.ano = 2017 and a.mes = 1 -- condição necessário pq no mes de janeiro o pvp4 não estava a ser guardado na fi
								then case 
										when u_generico=1 and a.pvp4 > 0 and u_epvp <= a.pvp4 then 0.35 * qtt
										when u_generico=0 and a.pvp4 > 0 and pic <= a.pvp4 then 0.35 * qtt
									else 0
										end
							when a.cptorgabrev = 'SNS'
								then a.pvp4_fee
							else
								0 
							end)
			,pref = a.u_epref
			,pvpmaxre = isnull(a.pvpmaxre,0)
			,operador = a.vendnm
			,a.opcao
			,a.usrinis, a.usrdata, a.usrhora
			,a.generico
			,codemb = a.u_codemb
			,codembMatrix = a.u_codemb_matrix
			,a.id_dispensa_eletronica_d
			,a.iva
			,a.transaction_id
			,a.voc_id
			,a.u_nbenef
			,a.u_nbenef2 
			,loteId
			,iDValidacaoDem
			
			

		FROM
			#faturacao a
	) w
	where
		comp > 0 
	ORDER BY
		cptorgabrev, tlote, lote, nreceita, lordem

	If OBJECT_ID('tempdb.dbo.#receitas') IS NOT NULL
		drop table #receitas;
	If OBJECT_ID('tempdb.dbo.#faturacao') IS NOT NULL
		drop table #faturacao;

GO
Grant Execute On [up_receituario_fe_detalheLote] to Public
Grant Control On [up_receituario_fe_detalheLote] to Public
GO


