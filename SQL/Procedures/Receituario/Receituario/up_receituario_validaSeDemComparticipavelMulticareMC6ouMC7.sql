/*	
	Valida se deve utilizar nova compart multicare, apartir de 2022

	up_receituario_validaSeDemComparticipavelMulticareMC6ouMC7 

	exec up_receituario_validaSeDemComparticipavelMulticareMC6ouMC7




*/




SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_validaSeDemComparticipavelMulticareMC6ouMC7]') IS NOT NULL
	drop procedure [up_receituario_validaSeDemComparticipavelMulticareMC6ouMC7]
go

Create procedure [dbo].[up_receituario_validaSeDemComparticipavelMulticareMC6ouMC7]


/* with encryption */
AS
	SET NOCOUNT ON

	declare @ano int = YEAR(getdate())
	 
	if(@ano>2021)
		select convert(bit,1) as resultado
	else
		select convert(bit,0) as resultado
GO
Grant Execute On [up_receituario_validaSeDemComparticipavelMulticareMC6ouMC7] to Public
Grant Control On [up_receituario_validaSeDemComparticipavelMulticareMC6ouMC7] to Public
GO