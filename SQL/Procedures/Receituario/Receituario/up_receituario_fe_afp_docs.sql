


/****** 

Object:  StoredProcedure [dbo].[up_receituario_fe_afp_docs]    Script Date: 22/11/2022 10:33:00 

exec up_receituario_fe_afp_docs '', 6, 2022, 1, '724C8475-842B-4E8A-8F5B-0008A3D148FE', '018',0, ''


******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_fe_afp_docs]') IS NOT NULL
	drop procedure dbo.up_receituario_fe_afp_docs
go


Create procedure [dbo].[up_receituario_fe_afp_docs]

	 @codInfarmed		VARCHAR(10)
	,@mes				NUMERIC(5,0) = 0
	,@ano				NUMERIC(5,0) = 0
	,@docId				NUMERIC(3,0)
	,@cod 				VARCHAR(50)
	,@id_ext_efr		VARCHAR(10)
	,@test				tinyint
	,@lastChangeDate	VARCHAR(20) = ''
/* with encryption */
AS

SET NOCOUNT ON

IF OBJECT_ID('ext_esb_doc_cred_deb') IS NOT NULL 
begin
   DROP TABLE [dbo].ext_esb_doc_cred_deb
end

create table ext_esb_doc_cred_deb(
	oDocNr  varchar(100), 
	docNr  varchar(100), 
	oDocDate  date
)


INSERT INTO	ext_esb_doc_cred_deb (oDocNr, docNr, oDocDate)
select oDocNr, docNr, oDocDate from	ext_esb_doc		as credDeb 	(NOLOCK) 
WHERE	credDeb.tipoDoc != 1 /*Cr�ditos e d�bitos*/ AND credDeb.test = 0



set @docId = isnull(@docId,0)

SELECT [stamp]
	  ,isnull(cast((select count(*) from ext_esb_doc_cred_deb debCred (nolock) where debCred.oDocDate = doc.DocDate and debCred.oDocNr = doc.DocNr) as bit), 0) as credDeb
	  ,[test]
      ,[id_ext_pharmacy]
      ,[id_ext_efs]
	  ,[name_ext_efs]
	  ,[app_ext_efs]
      ,[year]
      ,[month]
      ,[docNr]
      ,[docDate]
	  ,[startPeriodDate]
	  ,[endPeriodDate]
      ,[qtLots]
      ,[qtPrescriptions]
      ,[qtProducts]
      ,[totalPVP]
      ,[totalPVU]
      ,[totalEFR]
      ,[totalSNS]
      ,[atCertification]
      ,[obs]
      ,[totalOthers]
      ,[atCode]
      ,[name_ext_pharmacy]
      ,[vatNr_ext_pharmacy]
      ,[postal_ext_pharmacy]
      ,[address_ext_pharmacy]
	  ,[corporateRegistrationName_ext_pharmacy]
	  ,[city_ext_pharmacy]
      ,[city_ext_entity]
      ,[postal_ext_entity]
      ,[address_ext_entity]
      ,[id_ext_efr]
      ,[tipoDoc]
      ,[oDocNr]
	  ,[oDocDate]
  FROM [dbo].[ext_esb_doc] doc (nolock)
  inner join
		ext_esb_api(nolock) on ext_esb_api.idAfp = id_ext_efr
  where
	test = @test
	and ext_esb_api.apiKey=@cod
	and doc.tipoDoc =case when  @docId = 0 then doc.tipoDoc else @docId end
	and doc.month = case when @mes = 0 then doc.month else @mes end 
	and doc.year = case when @ano = 0 then doc.year else @ano end 
	and doc.id_ext_pharmacy = case when @codInfarmed = '' then doc.id_ext_pharmacy else @codInfarmed end
	and doc.invalid = 0
	and doc.id_ext_efr = @id_ext_efr
	and doc.ousrdata >= case when @lastChangeDate = '' then doc.ousrdata else @lastChangeDate end
 
	
	IF OBJECT_ID('ext_esb_doc_cred_deb') IS NOT NULL 
	begin
	   DROP TABLE [dbo].ext_esb_doc_cred_deb
	end

GO
Grant Execute On dbo.up_receituario_fe_afp_docs to Public
Grant Control On dbo.up_receituario_fe_afp_docs to Public
GO


