/* Valida partilha de resultados MCDTs
	
	 exec up_receituario_validaPartilhaResultado 2022, 7, 'SNS ARS-ALGARVE', 'Loja 1'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_validaPartilhaResultado]') IS NOT NULL
	drop procedure dbo.up_receituario_validaPartilhaResultado
go

create procedure dbo.up_receituario_validaPartilhaResultado
	@ano		numeric(4),
	@mes		numeric(2),
	@abrev		varchar(15),
	@site       varchar(254)

/* WITH ENCRYPTION */
AS
	declare @usaPosto bit =0


	if(len(ltrim(rtrim(isnull(@site,''))))<=6)
	begin
		select top 1 @usaPosto=isnull(posto,0) from empresa(nolock) where site=@site
	end


	;with
		cte_receitas as (
			select
				ctltrctstamp, ctltrct.cptorgstamp, ano, mes
				,cptorgabrev, lote, tlote, slote, nreceita
				,fechado, u_fistamp
				,descricao = CASE WHEN isnull(mcdt,0) = 0 then  tlote.descricao else tlote.descrMCDT end
				,cptplacode
				,ctltrct.usrinis, ctltrct.usrdata, ctltrct.usrhora
				,ctltrct.site, ctltrct.posto
				,token
				,tlote.loteId
			from
				ctltrct (nolock)
				INNER JOIN tlote (nolock) ON tlote.codigo = ctltrct.tlote
				LEFT  JOIN   cptorg  (nolock)  ON cptorg.cptorgstamp  = ctltrct.cptorgstamp
			where
				ano = @ano and mes = @mes
				AND cptorgabrev = @abrev
				and site = case when len(@site)>6 or @usaPosto=1 then site else  @site end --se for posto envia varios sites
		),
	 
		cte_facturacao as (
			select
				ft.ftstamp, fistamp, u_epvp, qtt, u_ettent1, u_ettent2, u_ltstamp, u_ltstamp2
				,fi.u_generico, fi.pvp4_fee, fi.pic, pvp4 = isnull(fprod.preco_acordo,0),u_nratend
			from
				fi (nolock)
				inner join ft (nolock)	on ft.ftstamp=fi.ftstamp
				left join fprod (nolock) on fprod.cnp = fi.ref
			where
				ftano = @ano and month(fdata)= @mes
				and (
					(u_ltstamp != '' and u_ettent1 > 0)
					or
					(u_ltstamp2 != '' and u_ettent2 > 0)
				)
				and ft.site in (select * from up_SplitToTable(@site,','))/*= (case when @site = '' Then ft.site else @site end)*/
		)
	
	--select sum(comp),sum(pvp),sum(utente) from (
	select
		ano
		,mes
		,tlote
		,slote
		,lote
		,cptorgabrev
		,nreceita
		,tlote2
		,utente			= sum(pvp)-sum(comp)
		,pvp			= sum(pvp)
		,comp			= sum(comp)	
		,fee            =  dbo.uf_calcFee(sum(pvp4_fee))
		,facturado
		,ctltrctstamp
		,cptplacode
		,fechado
		,ftstamp
		,usrinis, usrdata, usrhora
		,u_ltstamp, u_ltstamp2
		,site
		,posto
		,TipoLoteSNS
		,token
	from (

		select
			ano				= c.ano
			,mes			= c.mes
			,tlote			= rtrim(c.loteId) + ' - ' + c.descricao
			,tlote2			= c.tlote
			,cptorgabrev	= c.cptorgabrev
			,slote			= c.slote
			,lote			= c.lote
			,nreceita		= c.nreceita
			,pvp			= ft.u_epvp*ft.qtt
			,comp			= case when c.ctltrctstamp=ft.u_ltstamp then u_ettent1 else u_ettent2 end
			,pvp4_fee = case  
							when c.cptorgabrev = 'SNS' and c.ano = 2017 and c.mes = 1 -- condi??o necess?rio pq no mes de janeiro o pvp4 n?o estava a ser guardado na fi
								then case 
										when u_generico=1 and ft.pvp4 > 0 and u_epvp <= ft.pvp4 then 0.35 * qtt
										when u_generico=0 and ft.pvp4 > 0 and pic <= ft.pvp4 then 0.35 * qtt
										else 0
										end
							when c.cptorgabrev = 'SNS'
								then ft.pvp4_fee
							else
								0 
							end
			,fechado
			,facturado		= CASE WHEN u_fistamp='' THEN CAST(0 as Bit) ELSE CAST(1 As Bit) END
			,ctltrctstamp
			,cptplacode
			,ft.ftstamp, fistamp
			,c.usrinis, c.usrdata, c.usrhora
			,ft.u_ltstamp, ft.u_ltstamp2
			,c.site, posto = case when c.posto = '' then c.site else c.posto end
			/* tipoLoteSNS é o lote retornado pelo serviço de consulta. Mesma receita pode ter mais do que tipo de lote, mas o lote errado é sempre o lote mais baixo (96,98), 
				por isso o top 1 deve ser suficiente  para saber se a receita tem lotes inválidos ou não */
			,isnull((select top 1 lote from dispensa_eletronica_d (nolock) where token = c.token order by lote asc),tlote) as TipoLoteSNS
			,sinave.token
		from
			cte_receitas c
			inner join cte_facturacao ft	ON c.ctltrctstamp = ft.u_ltstamp or c.ctltrctstamp = ft.u_ltstamp2
			left join ext_sinave(nolock) sinave on (sinave.ftstamp = ft.ftstamp and sinave.ftstamp!='') or (sinave.nr_atend = ft.u_nratend and sinave.ftstamp!='') 
	)x
	where
		comp > 0
	group by
		ano, mes, cptorgabrev, tlote, tlote2, slote, lote
		,fechado, nreceita, facturado
		,ctltrctstamp, cptplacode
		,ftstamp, usrinis, usrdata, usrhora, u_ltstamp, u_ltstamp2
		,site,posto, TipoLoteSNS, token
	--)xx
	order by
		nreceita

Go
Grant Execute on dbo.up_receituario_validaPartilhaResultado to Public
Grant Control on dbo.up_receituario_validaPartilhaResultado to Public
Go
