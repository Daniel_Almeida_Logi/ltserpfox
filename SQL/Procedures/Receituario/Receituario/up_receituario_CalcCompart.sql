/*
	exec up_receituario_CalcCompart 'xxx', 0, '5440987', '48', '1',0
	exec up_receituario_CalcCompart 'xxx', 0, '5440987', '48', '1', 20 ,'ST'

*/

if OBJECT_ID('[dbo].[up_receituario_CalcCompart]') IS NOT NULL
	drop procedure dbo.up_receituario_CalcCompart
go





create procedure dbo.up_receituario_CalcCompart
	@diploma		   varchar(60)
	,@fipvp			    numeric(13,3)
	,@ref			    varchar(15)
	,@cptplacode	    varchar(6)
	,@site_nr		    tinyint
	,@mv_compartPercent numeric(5,2) = 0
	,@grpProd           varchar(2) = ''

/* with encryption */
as

	set nocount on

	set @grpProd = rtrim(ltrim(isnull(@grpProd,'')))

		
	/* Variaveis dentro da SP */
	declare
		@ranking	varchar(1)
		,@coluna	varchar(3)
		,@grupo		varchar(60)
		,@str		nvarchar(max)
		,@fipvp2	numeric(13,3)


	/* Manipula��o de PVP: Se o valor da variavel recebida for ZERO (0), ler o PVP da ST */
	select @fipvp2 = case when @fipvp=0 then (select epv1 from st (nolock) where ref=@ref and st.site_nr = @site_nr) else @fipvp end

	/* Tramento de vari�veis */
	select
		/* Adicionar Pelicas */
		@cptplacode = char(39) + left(@cptplacode,4) + char(39)
		,@grupo     = case when @grpProd='' then ISNULL((SELECT char(39) + grupo + char(39) FROM fprod fp1 (nolock) WHERE fp1.cnp=@ref),'''''') else '''' + @grpProd +  '''' end
		,@diploma   = char(39) + left(@diploma,58) + char(39)
		/*
			Validar em que coluna esta os valores de comparticipa��o quando aplicado determinado diploma
		*/
		,@coluna = isnull(convert(varchar,(select u_coluna from dplms where u_design=@diploma)),1)
		/*
			Veriricar se o medicamente est� no ranking
			PVPMAXRE: Valor at� ao qual o medicamento � comparticipado em 95% do pre�o de refer�ncia para os regimes especiais
			nos escal�es B(69%), C(37%) e D(15%)
		*/
		,@ranking =
			case
				when @fipvp2<>0 and @fipvp2 <= isnull((select pvpmaxre from fprod fp2 (nolock) where fp2.cnp=@ref),0) then 1
				else 0
			end
		--,@ref=char(39)+@ref+char(39)

	/*
		Query SQL Din�mico que vai ser executado, cujo output � devolvido pela SP
	*/

	select @str='
		SELECT
			design = RTRIM(LTRIM(design)),
			cptplacode,
			abrev = isnull(cptpla.cptorgabrev,''''),
			grupo,
			''Compl'' = IsNull(compl,''''),

			percentagem = 
				case
					-- Quando vem do MVR a percentagem de comparticipa��o � ditada pelo user
					when ' + ltrim(str(@mv_compartPercent,5,2)) + ' > 0 then ' + ltrim(str(@mv_compartPercent,5,2)) + ' 
					-- Normal
					when cptplacode in (select codigo from cptpla (nolock) where regra_pens = 1) and ' + @ranking + ' = 1 
							and (case when ' + @diploma + ' != '+'''xxx'''+' then u_p' + @coluna + ' else PCT end) not in (0,100)
					then 95 
					else 
						case 
							when ' + @diploma + ' != '+'''xxx'''+' then u_p' + @coluna + ' 
							else PCT
						end
				end

			,grppreco=
 				case 
					when cptplacode in (select codigo from cptpla (nolock) where regra_pens = 1) and ' + @ranking + ' = 1 
							and (case when ' + @diploma + ' != '+'''xxx''' + ' then u_p' + @coluna + ' else PCT end) not in (0,100)
					then (case 
							when grppreco = ''pens'' then ''ref'' 
							else grppreco 
						end)
					'+'else case 
							when '+@diploma+' != '+'''xxx'''+' 
							then (case 
									when u_g' + @coluna + ' = ''pens'' then ''ref'' 
									else u_g'+ @coluna + ' 
								end)
							else (case 
									when grppreco = ''pens'' then ''ref''
									else grppreco 
								end)
						end
				end

			,comp_s_diploma = 
				case
					-- Quando vem do MVR a percentagem de comparticipa��o � ditada pelo user
					when ' + ltrim(str(@mv_compartPercent,5,2)) + ' > 0 then ' + ltrim(str(@mv_compartPercent,5,2)) + ' 
					-- Normal
					when cptplacode in (select codigo from cptpla (nolock) where regra_pens = 1) and ' + @ranking + ' = 1 and PCT not in (0,100)
					then 95 
					else PCT
				end
				
			,diploma = ' + @diploma + '
				
			,ranking = ' + @ranking + '
			,compartMaxValue = isnull(compartMaxValue,0)

	FROM
		cptval (nolock)
		LEFT JOIN cptpla (nolock) ON cptval.cptplacode = cptpla.codigo
	WHERE
		cptplacode = ' + @cptplacode + '
		AND grupo = ' + @grupo
	
	print @str
	/* EXEC, preenche o cursor no FoxPro */
	exec sp_executesql @str
	--print @str

go
grant execute on dbo.up_receituario_CalcCompart to public		
grant control on dbo.up_receituario_CalcCompart to public
go