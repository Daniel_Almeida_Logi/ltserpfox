/* Listagen de Entidades

 exec up_receituario_listEntidades ''

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_listEntidades]') IS NOT NULL
	drop procedure dbo.up_receituario_listEntidades
go

create procedure dbo.up_receituario_listEntidades
@abrev as varchar(15)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	SELECT
		abrev, cptorgstamp
	FROM
		cptorg (nolock)
	Where
		abrev = case when @abrev = '' then abrev else @abrev end
	ORDER BY
		abrev

go
grant execute on dbo.up_receituario_listEntidades to public 			
grant execute on dbo.up_receituario_listEntidades to public 
go
