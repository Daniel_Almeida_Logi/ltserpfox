/*

 exec up_receituario_verReceitasImportadas '2012','9', '', 0, ''

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_verReceitasImportadas]') IS NOT NULL
	drop procedure dbo.up_receituario_verReceitasImportadas
go

Create procedure dbo.up_receituario_verReceitasImportadas

@Ano		NUMERIC(4)
,@Mes		NUMERIC(2)
,@abrev		varchar(15) = ''
,@dia		numeric(2) = 0
,@posto		varchar(20) = ''

/* with encryption */
AS
SET NOCOUNT ON

	select 
		ft.ftstamp
		,ft.u_ltstamp
		,ft.u_ltstamp2
		,ctltrctstamp
		,cptorgabrev
		,lote
		,tlote
		,slote
		,nreceita
		,ano
		,mes
		,fechado
		,posto
		,lote_orig
		,no_orig
		,ft.fdata
	from
		ctltrct (nolock)
		inner join ft (nolock) on ft.u_ltstamp=ctltrct.ctltrctstamp or ft.u_ltstamp2=ctltrct.ctltrctstamp
	where
		ano = @ano
		and mes = @Mes
		and posto != ''
		and posto =
					case
						when @posto = 'Todos' or @posto = ''
						then posto
						Else @posto
					end
		and cptorgabrev =
					case
						when @abrev = 'Todas' or @abrev = ''
						then cptorgabrev
						else @abrev
					end
		and DAY(ft.fdata) =
					case
						when @dia = 0
						then day(ft.fdata)
						else @dia
					end
	order by
		ano, mes, cptorgabrev, slote, tlote, lote, nreceita

Go
Grant Execute on dbo.up_receituario_verReceitasImportadas to Public
Grant Control on dbo.up_receituario_verReceitasImportadas to Public
Go
