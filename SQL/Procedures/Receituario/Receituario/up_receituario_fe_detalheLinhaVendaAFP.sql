/*	
	Totais de cada linha da factura as entidades

	exec up_receituario_fe_detalheLinhaVendaAFP 2018,10,'SBSI','1','Loja 1','tm393B99D3-85D0-4969-AAB'
*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_fe_detalheLinhaVendaAFP]') IS NOT NULL
	drop procedure up_receituario_fe_detalheLinhaVendaAFP
go




Create procedure [dbo].up_receituario_fe_detalheLinhaVendaAFP
	@ano NUMERIC(4)
	,@mes NUMERIC(2)
	,@abrev	VARCHAR(15)
	,@lote	VARCHAR(15)
	,@site varchar(20)
	,@ftstamp varchar(50)

/* with encryption */
AS
	SET NOCOUNT ON

	If OBJECT_ID('tempdb.dbo.#receitas_entidade') IS NOT NULL
		drop table #receitas_entidade;
	If OBJECT_ID('tempdb.dbo.#faturacao_entidade') IS NOT NULL
		drop table #faturacao_entidade;

	select
		ctltrctstamp,  
		u_fistamp
	into 
		#receitas_entidade	
	from
		ctltrct c (nolock)
	where
		c.ano=ano and c.mes=@mes
		and c.cptorgabrev=@abrev
		and c.u_fistamp!=''
		and c.tlote=@lote
		and site = @site
 
	select 
		fi.u_epvp,
		fi.u_ettent1,
		fi.u_ettent2,
		fi.qtt,
		fi.fistamp,
		fi.ref,
		fi.iva,
		fi.rdata,
		fi.ftstamp,
		fi.u_codemb,
		fi.design
    into 
		#faturacao_entidade
	from 
		ft(nolock)
	inner join 
		#receitas_entidade re
	on 
		re.ctltrctstamp=ft.u_ltstamp or re.ctltrctstamp=ft.u_ltstamp2
	inner join 
		fi(nolock) 
	on 
		fi.ftstamp = ft.ftstamp
	where 
		fi.u_ettent1>0  and fi.ftstamp=@ftstamp 
		
		 
	select 
		f.ref,
		f.u_codemb,
		f.qtt,
		f.ftstamp,
		f.fistamp,
		pvp = round(f.u_epvp,2) * f.qtt,
		utenteEntidade = (round(f.u_epvp,2) * f.qtt) - (round(f.u_ettent1,2) + round(f.u_ettent2,2)),
		compartEntidade = round(f.u_ettent1,2),
		compartOthers = 0,
		compSNSEntidade = 
					case 
						when f.u_ettent1 > 0 and f.u_ettent2 > 0 then f.u_ettent2 
						else 0 
					end,
		valorBase = 
					case 
						when f.u_ettent1 > 0  then round(f.u_ettent1 - (f.u_ettent1 * (iva/100) ) ,2)
						else 0
					end,
		valorIva = 
					case 
						when f.u_ettent1 > 0  then round((f.u_ettent1 * (iva/100)) ,2)
						else 0
					end,	
		f.iva,
		f.rdata,
		nameProd = f.design

	from 
		#faturacao_entidade f


	

	If OBJECT_ID('tempdb.dbo.#receitas_entidade') IS NOT NULL
		drop table #receitas_entidade;
	If OBJECT_ID('tempdb.dbo.#faturacao_entidade') IS NOT NULL
		drop table #faturacao_entidade;

GO
Grant Execute On up_receituario_fe_detalheLinhaVendaAFP to Public
Grant Control On up_receituario_fe_detalheLinhaVendaAFP to Public
GO


