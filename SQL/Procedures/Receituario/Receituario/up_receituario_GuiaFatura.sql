/* 
	Reports: Documento Guia de Fatura emitido para o CCF
	
	 exec up_receituario_GuiaFatura 2021, 7, 'SNS','Loja 5'
	 select ftstamp, * from fi where fistamp = 'CA0BCDD0-D660-41E8-A0E0-'
	 select * from ctltrct where ano = 2016 and mes = 9 and cptorgabrev = 'SNS'
	 select * from fi where fistamp in (select u_fistamp from ctltrct where ano = 2016 and mes = 9 and cptorgabrev = 'SNS')


	 select * from ft where ftstamp = 'CA0BCDD0-D660-41E8-A0E0-'
	 select * from ft where ftstamp = 'CA0BCDD0-D660-41E8-A0E0'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_GuiaFatura]') IS NOT NULL
	drop procedure dbo.up_receituario_GuiaFatura
go

create procedure dbo.up_receituario_GuiaFatura
	@Ano NUMERIC
	,@Mes NUMERIC
	,@Abrev VARCHAR(15)  = 'SNS' -- fixo para o SNS para já pq é a unica entidade que solicita esta informação
	,@site varchar(254) = ''

/* with encryption */

AS
SET NOCOUNT ON
	
	If OBJECT_ID('tempdb.dbo.#receitas') IS NOT NULL
		drop table #receitas;
	If OBJECT_ID('tempdb.dbo.#facturacao') IS NOT NULL
		drop table #facturacao;
	If OBJECT_ID('tempdb.dbo.#AuxFaturaSNS') IS NOT NULL
		drop table #AuxFaturaSNS;
	If OBJECT_ID('tempdb.dbo.#resultTemp') IS NOT NULL
		drop table #resultTemp;	
	If OBJECT_ID('tempdb.dbo.#resultFinalTemp') IS NOT NULL
		drop table #resultFinalTemp;
	



	DECLARE @DataUldiaAux date,  @DataUldia date ,@no numeric(10,0)
	SET @DataUldiaAux = (select CAST(CAST(@Ano AS VARCHAR(4)) + RIGHT('0' + CAST(@mes AS VARCHAR(2)), 2) + RIGHT('0' + CAST('28' AS VARCHAR(2)), 2)  AS DATETIME))
	set @DataUldia = (SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@DataUldiaAux)+1,0)))
	select @no=u_no from cptorg where abrev = @Abrev
	--
	select
		ctltrct.ctltrctstamp, ctltrct.ano, ctltrct.mes
		,ctltrct.cptorgabrev
		,ctltrct.lote, ctltrct.tlote
		,descricao = CASE WHEN isnull(mcdt,0) = 0 then  tlote.descricao else tlote.descrMCDT end
		,tlote.loteId
	into
		#receitas
	from
		ctltrct	(nolock)
		INNER JOIN tlote (nolock) ON tlote.codigo=ctltrct.tlote
		LEFT  JOIN   cptorg  (nolock)  ON cptorg.cptorgstamp  = ctltrct.cptorgstamp
	where
		ano = @ano 
		and mes = @mes
		and site = @site
		and ctltrct.cptorgabrev = @abrev
		and fechado = 1

	
	--
	-- nº fatura SNS; Série Fatura; Data Emissão Fatura SNS - caso esteja emitida o campo cdata corresponde ao Ano/mes do periodo da fatura
	select 
		ft.fno, fdata, ft.ndoc 
	into 
		#AuxFaturaSNS
	from
		ft
	where
		no = @no
		and cdata = @DataUldia
		and tipoDoc = 1
		and site = @site

	--	 
	select
		ft.ftstamp
		,fistamp, u_epvp, qtt, u_ettent1, u_ettent2, u_ltstamp, u_ltstamp2
	into
		#facturacao
	from
		fi (nolock)
		inner join ft (nolock) on ft.ftstamp=fi.ftstamp
		inner join #receitas a ON a.ctltrctstamp = ft.u_ltstamp or a.ctltrctstamp = ft.u_ltstamp2
	where
		ftano = @Ano and month(fdata) = @Mes
		and (
			(u_ltstamp != '' and u_ettent1 > 0)
			or
			(u_ltstamp2 != '' and u_ettent2 > 0)
		)
		--and ft.site in (select * from up_SplitToTable('Loja 2',','))/*= (case when @site = '' Then ft.site else @site end)*/
	
	--
	select
		ano
		,mes
		,rtrim(tlote) + ' - ' + descricao as tipoLote
		,Max(lote) as QttLotes
		,tlote
		,fno   = isnull((select top 1 fno  from #AuxFaturaSNS),0)
		,fdata = isnull((select top 1 fdata  from #AuxFaturaSNS),'19000101')
		,ndoc  = isnull((select top 1 ndoc  from #AuxFaturaSNS),0)
	into #resultTemp
	FROM (
		SELECT distinct
			a.ano
			,a.mes
			,a.cptorgabrev
			,tlote = rtrim(a.loteId)
			,a.lote
			,comp = case when a.ctltrctstamp=fact.u_ltstamp then u_ettent1 else u_ettent2 end
			,a.descricao
			,fistamp
			,ctltrctstamp
		FROM
			#receitas a
			inner join #facturacao fact	ON a.ctltrctstamp = fact.u_ltstamp or a.ctltrctstamp = fact.u_ltstamp2
	) w
	where
		comp > 0
	group by
		ano, mes
		,tlote, descricao
	ORDER BY
		ano, mes
		,tlote, descricao
		
	if(rtrim(ltrim(isnull(@Abrev,'')))='SNS')
	begin
		select 
			ROW_NUMBER() OVER(PARTITION BY tlote  ORDER BY tlote ASC) AS numeroLinha
			,*  
		into 
			#resultFinalTemp
		from 
			#resultTemp res
		

		update 
			#resultFinalTemp
		set
			QttLotes =   isnull((select sum(isnull(res.QttLotes,0)) from #resultFinalTemp res where res.tlote = #resultFinalTemp.tlote ),0)


		select  
			* 
		from 
			#resultFinalTemp res
		where 
			res.numeroLinha = 1
		 
	end else begin

		select * from #resultTemp

	end



	If OBJECT_ID('tempdb.dbo.#resultTemp') IS NOT NULL
		drop table #resultTemp;	
	If OBJECT_ID('tempdb.dbo.#resultFinalTemp') IS NOT NULL
		drop table #resultFinalTemp;	
	If OBJECT_ID('tempdb.dbo.#receitas') IS NOT NULL
		drop table #receitas;
	If OBJECT_ID('tempdb.dbo.#facturacao') IS NOT NULL
		drop table #facturacao;
	If OBJECT_ID('tempdb.dbo.#AuxFaturaSNS') IS NOT NULL
		drop table #AuxFaturaSNS;

GO
Grant Execute on dbo.up_receituario_GuiaFatura to Public
Grant control on dbo.up_receituario_GuiaFatura to Public
Go
