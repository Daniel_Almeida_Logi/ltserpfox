



/****** 

Object:  StoredProcedure [dbo].[up_receituario_fe_afp_docs]    Script Date: 22/11/2022 10:33:00 
 exec up_receituario_fe_afp_docs_CredDeb_notes 'FME 3/1895', '2022-06-30', '1724', '018'


******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_receituario_fe_afp_docs_CredDeb_notes]') IS NOT NULL
	drop procedure up_receituario_fe_afp_docs_CredDeb_notes
go

Create procedure [dbo].[up_receituario_fe_afp_docs_CredDeb_notes]
	@oDocNr varchar(36),
	@oDocDate datetime,
	@senderId varchar(100),
	@entityId  varchar(100)
/* with encryption */
AS

SET NOCOUNT ON
 
select 
	docnr, 
	convert(varchar(120),docdate) as docdate, 
	tipoDoc, 
	totalEFR 
from 
	[ext_esb_doc] doc (nolock) 
  where 
	tipodoc != 1      
	and odocNr = @oDocNr  
	and odocDate = @oDocDate
	and id_ext_pharmacy = @senderId
	and id_ext_efr = @entityId

GO


