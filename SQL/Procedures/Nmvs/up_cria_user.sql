/*

	 Novo utilizador NMVS

	 exec up_nmvs_criaUser 'LTDEV30', '00000', 'Avenida da boavista', '227966589', 'ltdev30', 'ltdev30.2018!_'

	 select * from utilizadores(nolock)
	 delete from utilizadores

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_nmvs_criaUser]') IS NOT NULL
	drop procedure dbo.up_nmvs_criaUser
go

create procedure dbo.up_nmvs_criaUser

@nome varchar(255),
@codigo varchar(20),
@morada varchar(255),
@nif varchar(20),
@utilizador varchar(100),
@password varchar(255)


/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

declare @masterkey varchar(200)
select @masterkey= (SELECT DB_NAME()+replace(cast(getdate() as varchar(30)),' ',''))

--Cria��o de chave prim�ria para encripta��o 
begin 
	IF NOT EXISTS (SELECT * FROM sys.symmetric_keys WHERE symmetric_key_id = 101)
	begin
		exec(' CREATE MASTER KEY ENCRYPTION BY PASSWORD = ''' + @masterkey + '''' )

		CREATE CERTIFICATE userpassword
		WITH SUBJECT = 'Users Password';
		

		CREATE SYMMETRIC KEY UPWD_Key_01
		WITH ALGORITHM = AES_256
		ENCRYPTION BY CERTIFICATE userpassword;
	

	end
end 


	OPEN SYMMETRIC KEY UPWD_Key_01
	DECRYPTION BY CERTIFICATE userpassword;

	INSERT INTO utilizadores(
				stamp,
				nome,
				codigo,
				morada,
				nif,
				utilizador,
				[password])
		 VALUES
			   (CONVERT(varchar(30), LEFT(NEWID(),30)) 
			   ,LTRIM(RTRIM(@nome))
			   ,LTRIM(RTRIM(@codigo))
			   ,LTRIM(RTRIM(@morada))
			   ,LTRIM(RTRIM(@nif))
			   ,LTRIM(RTRIM(@utilizador))
			   ,EncryptByKey(Key_GUID('UPWD_Key_01'), LTRIM(RTRIM(@password))))



	close SYMMETRIC KEY UPWD_Key_01

GO
Grant Execute on dbo.up_nmvs_criaUser to Public
Grant Control on dbo.up_nmvs_criaUser to Public
GO