/*
	
 valida se utilizador 

 exec up_nmvs_autenticaUser 'ltdev30', 'ltdev30.2018!_' 

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_nmvs_autenticaUser]') IS NOT NULL
	drop procedure dbo.up_nmvs_autenticaUser
go

create procedure dbo.up_nmvs_autenticaUser
@user varchar(30),
@pass varchar(255)


/* WITH ENCRYPTION */ 
AS 
BEGIN 
	
	OPEN SYMMETRIC KEY UPWD_Key_01
	DECRYPTION BY CERTIFICATE userpassword;
	
	select 
		top 1
		stamp,
		nome,
		tipo,
		morada,
		nif	
	from 
		utilizadores as ut (nolock)
	where
		ut.utilizador =  @user 
		and (CONVERT(varchar(50), DecryptByKey(ut.[password])) COLLATE Latin1_General_CS_AS) =  (@pass COLLATE Latin1_General_CS_AS) 
		--and us.userpass = case when @pass = '' then us.userpass else @pass end 
		and ut.inactivo = 0

	close SYMMETRIC KEY UPWD_Key_01
END


GO
Grant Execute on dbo.up_nmvs_autenticaUser to Public
Grant Control on dbo.up_nmvs_autenticaUser to Public
GO
--------------------------------------------------------------