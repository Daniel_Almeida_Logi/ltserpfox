/* get tradu��o por lingua 

	exec up_get_mvoTranslation '', ''
	exec up_get_mvoTranslation 'NMVS_NC_PCK_23', 'PT'
		exec up_get_mvoTranslation 'NMVS_NC_PCK_24', 'PT'

*/

if OBJECT_ID('[dbo].[up_get_mvoTranslation]') IS NOT NULL
	drop procedure dbo.up_get_mvoTranslation
go

create procedure [dbo].[up_get_mvoTranslation]
	 @code				AS varchar(100)
	,@language			AS varchar(100)

/* with encryption */
AS
SET NOCOUNT ON

	SET @code = ISNULL(@code,'') 
	SET @language = ISNULL(@language,'') 

	SELECT 
			stamp,
			language,
			code,
			convert(varchar(254),descr) as descr,
			ousrinis,
			ousrdata,
			usrinis,
			usrdata
	FROM 
		mvoTranslation (NOLOCK)

	WHERE 
		mvoTranslation.code = (CASE When @code = '' Then mvoTranslation.code Else @code End)
		and mvoTranslation.language= (CASE When @language = '' Then mvoTranslation.language Else @language End)
GO
Grant Execute on dbo.up_get_mvoTranslation to Public
Grant control on dbo.up_get_mvoTranslation to Public
GO