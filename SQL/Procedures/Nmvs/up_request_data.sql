/*
	
 retorna a data do ultimo mixed status request 

 exec up_nmvs_validateDateRequest '6b8389fe7d9647cdbcbfc768bdba1ecd'


 select top 1 * from G196_Request order by ousrdata desc 
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_nmvs_validateDateRequest]') IS NOT NULL
	drop procedure dbo.up_nmvs_validateDateRequest
go

create procedure dbo.up_nmvs_validateDateRequest
@id varchar(255)



/* WITH ENCRYPTION */ 
AS 
BEGIN 

	select 
		count(stamp) as no
	from 
		G196_Request as g196 (nolock)
	where
		g196.refClientTrxId = @id and
		ousrdata>dateadd(minute,-1,CONVERT(datetime,GETDATE(),120))



END


GO
Grant Execute on dbo.up_nmvs_validateDateRequest to Public
Grant Control on dbo.up_nmvs_validateDateRequest to Public
GO


