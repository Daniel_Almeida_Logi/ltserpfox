/*
	
 retorna dados de dispensas nmvs

 use [msb-concentrador]
 exec up_dados_nmvs '20190209', 1


	 
	 select 
			*
	 from G120_Request_all(nolock) request
	 left join G195_Request_Rel_all (nolock) request_related on  request_related.stamp = request.bulkStamp
	 left join  G196_Response_all (nolock) response_bulk on response_bulk.concentratorTrxId = request_related.concentratorTrxId 
	 where request.bulkstamp !=''



*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_dados_nmvs]') IS NOT NULL
	drop procedure dbo.up_dados_nmvs
go

create procedure dbo.up_dados_nmvs
@data varchar(255),
@sucesso bit








/* WITH ENCRYPTION */ 
AS 
BEGIN 
	

	;with cteDados as(
		select 
			"G110 - Verify" as "tipoPedido", nmvsCode as "codigo Resposta",description "descricao Codigo resposta",
			request.productCode as "GTIN", LEFT(CONVERT(VARCHAR, response.ousrdata, 120), 10)  as "Data",
			codigo as "codigo Infarmed", nome	as "nome farmacia", entityAPP as "aplicativo",

			"dispositivo" = 
				case when entityAPP = 'lts' then "Portal/Site" 
					 when entityId  = 'ubix' then "APP" 
					 else "Software" end

		from 
			G110_Response_all as response (nolock)
		inner join 
			G110_Request_all as request (nolock) on request.stamp = response.stampRequest
		inner join 
			company   (nolock) on company.stamp = request.senderStamp
		where
			request.ousrdata>@data and response.bulkStamp =''

		union all

		select 
			"G120 - Dispense" as "tipoPedido", nmvsCode as "codigo Resposta",description "descricao Codigo resposta",
			request.productCode as "GTIN" ,LEFT(CONVERT(VARCHAR, response.ousrdata, 120), 10)  as "Data",
			codigo as "codigo Infarmed", nome	as "nome farmacia", entityAPP as "aplicativo",
			"dispositivo" = 
				case when entityAPP = 'lts' then "Portal/Site" 
					 when entityId  = 'ubix' then "APP" 
					 else "Software" end
		from 
			G120_Response_all as response (nolock)
		inner join 
			G120_Request_all as request (nolock) on request.stamp = response.stampRequest	
		inner join 
			company   (nolock) on company.stamp = request.senderStamp
		where
			request.ousrdata>@data and response.bulkStamp =''


	   union all


		select 
			"G121 - Dispense Manual" as "tipoPedido", nmvsCode as "codigo Resposta",description "descricao Codigo resposta",
			request.productCode as "GTIN", LEFT(CONVERT(VARCHAR, response.ousrdata, 120), 10)  as "Data",
			codigo as "codigo Infarmed", nome	as "nome farmacia", entityAPP as "aplicativo",
			"dispositivo" = 
				case when entityAPP = 'lts' then "Portal/Site" 
					 when entityId  = 'ubix' then "APP" 
					 else "Software" end
		from 
			G121_Response_all as response (nolock)
		inner join 
			G121_Request_all as request (nolock) on request.stamp = response.stampRequest
		inner join 
			company   (nolock) on company.stamp = request.senderStamp
		where
			request.ousrdata>@data and response.bulkStamp =''
	
	
		union all

		select 
			"G122 - Undo Dispense" as "tipoPedido", nmvsCode as "codigo Resposta",description "descricao Codigo resposta",
			request.productCode as "GTIN", LEFT(CONVERT(VARCHAR, response.ousrdata, 120), 10)  as "Data",
			codigo as "codigo Infarmed", nome	as "nome farmacia", entityAPP as "aplicativo",
			"dispositivo" = 
				case when entityAPP = 'lts' then "Portal/Site" 
					 when entityId  = 'ubix' then "APP" 
					 else "Software" end
		from 
			G122_Response_all as response (nolock)
		inner join 
			G122_Request_all as request (nolock) on request.stamp = response.stampRequest

		inner join 
			company   (nolock) on company.stamp = request.senderStamp
		where
			request.ousrdata>@data and response.bulkStamp =''



		union all

		select 
			"G130 - Destroy" as "tipoPedido", nmvsCode as "codigo Resposta",description "descricao Codigo resposta",
			request.productCode as "GTIN", LEFT(CONVERT(VARCHAR, response.ousrdata, 120), 10)  as "Data",
			codigo as "codigo Infarmed", nome	as "nome farmacia", entityAPP as "aplicativo",
			"dispositivo" = 
				case when entityAPP = 'lts' then "Portal/Site" 
					 when entityId  = 'ubix' then "APP" 
					 else "Software" end
		from 
			G130_Response_all as response (nolock)
		inner join 
			G130_Request_all as request (nolock) on request.stamp = response.stampRequest

		inner join 
			company   (nolock) on company.stamp = request.senderStamp
		where
			request.ousrdata>@data and response.bulkStamp =''

	
		union all

		select 
			"G150 - Sample" as "tipoPedido", nmvsCode as "codigo Resposta",description "descricao Codigo resposta",
			request.productCode as "GTIN", LEFT(CONVERT(VARCHAR, response.ousrdata, 120), 10)  as "Data",
			codigo as "codigo Infarmed", nome	as "nome farmacia", entityAPP as "aplicativo",
			"dispositivo" = 
				case when entityAPP = 'lts' then "Portal/Site" 
					 when entityId  = 'ubix' then "APP" 
					 else "Software" end
		from 
			G150_Response_all as response (nolock)
		inner join 
			G150_Request_all as request (nolock) on request.stamp = response.stampRequest

		inner join 
			company   (nolock) on company.stamp = request.senderStamp
		where
			request.ousrdata>@data and response.bulkStamp =''


		union all

		select 
			"G151 - Undo Sample" as "tipoPedido", nmvsCode as "codigo Resposta",description "descricao Codigo resposta",
			request.productCode as "GTIN", LEFT(CONVERT(VARCHAR, response.ousrdata, 120), 10)  as "Data",
			codigo as "codigo Infarmed", nome	as "nome farmacia", entityAPP as "aplicativo",
			"dispositivo" = 
				case when entityAPP = 'lts' then "Portal/Site" 
					 when entityId  = 'ubix' then "APP" 
					 else "Software" end
		from 
			G151_Response_all as response (nolock)
		inner join 
			G151_Request_all as request (nolock) on request.stamp = response.stampRequest

		inner join 
			company   (nolock) on company.stamp = request.senderStamp
		where
			request.ousrdata>@data and response.bulkStamp =''
	)




	-- devolver todos os pedidos, menos os de testes
	select * from cteDados
	where [nome farmacia] !='LTDEV'
	order by   "codigo Infarmed","GTIN"   asc

END


GO
Grant Execute on dbo.up_dados_nmvs to Public
Grant Control on dbo.up_dados_nmvs to Public



