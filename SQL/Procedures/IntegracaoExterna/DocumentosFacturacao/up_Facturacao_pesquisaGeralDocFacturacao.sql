/* SP Pesquisa Documentos de Facturação
J.Gomes, 2020-04-29
--------------------------------

Listagem de Documentos de Facturação baseada em diversos campos.
e com possibilidade de filtragem acima de determinada data e hora de alteração.

Pesquisa nas tabelas: 	
Td, Ft, Ft2, Fi, b_utentes

por 16 campos:
	@number, @dep, @vatNr, @email, @ref, @design, @site, @numdoc, @seriesNumber
	, @prescriptionid, @suspenso, @lastChangeDate, @lastChangeTime
	, @dateInit, @dateEnd, @pageNumber 
	
Campos obrigatórios: nenhum

OUTPUT:
---------
IDInfo_number, IDInfo_site, IDInfo_seriesNumber, EntityInfo_number, EntityInfo_dep, EntityInfo_name
, EntityInfo_type, FinanceInfo_retailPrice,	FinanceInfo_currency, FinanceInfo_vatInc
, OperationInfo_CreationDate, OperationInfo_CreationTime, OperationInfo_lastChangeDate
, OperationInfo_lastChangeTime, GeneralInfo_docDate, GeneralInfo_prescriptionId
, GeneralInfo_attendanceNumber, StockInfo_qtt, DesignInfo_designation


EXECUÇÃO DA SP:
---------------
exec up_Facturacao_pesquisaGeralDocFacturacao '@number', '@dep', '@vatNr', '@email', '@ref', '@design'
, '@site', '@numdoc', '@seriesNumber', @prescriptionid, @suspenso
, '@lastChangeDate', '@lastChangeTime','@dateInit','@dateEnd' , @pageNumber	
	

	exec up_Facturacao_pesquisaGeralDocFacturacao  '', '','','','', 'Ben-U-Ron 1000mg 18 Comp','Loja 1','','','','','','','','','',''
	exec up_Facturacao_pesquisaGeralDocFacturacao  '329', '','','','', '','','','','','0','','','','',1
	exec up_Facturacao_pesquisaGeralDocFacturacao  '206', '','','','', 'Ben-U-Ron 1000mg 18 Comp','Loja 1','','75','','0','','','','',1,''
	exec up_Facturacao_pesquisaGeralDocFacturacao  '206', '','','','', '','Loja 1','','75','','0','2019-01-24','','','',1

	exec up_Facturacao_pesquisaGeralDocFacturacao  NULL, NULL,NULL,NULL,NULL, NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'54B739CC-A513-47C9-BAE5-909587913428'
	exec up_Facturacao_pesquisaGeralDocFacturacao  NULL, NULL,NULL,NULL,NULL, NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'D816F81D-F2F9-4F7D-9575-7D02DB102501',10,'obsCl2'
	exec up_Facturacao_pesquisaGeralDocFacturacao  26011, 0,NULL,NULL,NULL, NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL, NULL, NULL, NULL
	*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Facturacao_pesquisaGeralDocFacturacao]') IS NOT NULL
	DROP PROCEDURE [dbo].[up_Facturacao_pesquisaGeralDocFacturacao] ;
GO

CREATE PROCEDURE [dbo].[up_Facturacao_pesquisaGeralDocFacturacao]	
	 @number					VARCHAR(10)
	,@dep						VARCHAR(3)
	,@vatNr						VARCHAR(20)
	,@email						VARCHAR(50)
	,@ref						VARCHAR(18)
	,@design					VARCHAR(60)
	,@site						VARCHAR(60)	
	,@numdoc					VARCHAR(20)
	,@seriesNumber				VARCHAR(500)
	,@prescriptionid			VARCHAR(8)
	,@suspenso					VARCHAR(1)
	,@lastChangeDate			varchar(10) = '1900-01-01'
	,@lastChangeTime			varchar(8)  = '00:00:00'
	,@dateInit					varchar(10) = '1900-01-01'
	,@dateEnd					varchar(10)  = '3000-01-01'
	,@pageNumber				INT = 1
	,@orderStamp				VARCHAR(36) = ""
	,@topMax					int = 100
	,@obs						VARCHAR(250) = ""
	,@toRegulariseOnly			bit = 0
	   
/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

SET @ref				=   rtrim(ltrim(isnull(@ref,'')))
SET @number				=   rtrim(ltrim(isnull(@number,'')))
SET @dep				=   rtrim(ltrim(isnull(@dep,'')))
SET @vatNr				=   rtrim(ltrim(isnull(@vatNr,'')))
SET @design				=   rtrim(ltrim(isnull(@design,'')))
SET @prescriptionid		=   rtrim(ltrim(isnull(@prescriptionid,'')))
SET @suspenso			=   rtrim(ltrim(isnull(@suspenso,'')))
SET @numdoc				=   rtrim(ltrim(isnull(@numdoc,'')))
SET @seriesNumber		=   rtrim(ltrim(isnull(@seriesNumber,'')))
SET @site				=   rtrim(ltrim(isnull(@site,'')))
SET @email				=   rtrim(ltrim(isnull(@email,'')))
SET @lastChangeDate		=   rtrim(ltrim(isnull(@lastChangeDate,'1900-01-01')))
SET @lastChangeTime		=   rtrim(ltrim(isnull(@lastChangeTime,'00:00:00')))
SET @dateInit		    =   rtrim(ltrim(isnull(@dateInit,'1900-01-01')))
SET @dateEnd		    =   rtrim(ltrim(isnull(@dateEnd,'3000-01-01')))
SET @orderStamp         =	rtrim(ltrim(ISNULL(@orderStamp,'')))
SET @topMax             =   isnull(@topMax,100)
SET @obs                =   rtrim(ltrim(ISNULL(@obs,'')))
SET @toRegulariseOnly   =   isnull(@toRegulariseOnly,0)


if(isnull(@pageNumber,0)<1)
	set @pageNumber = 1

If OBJECT_ID('tempdb.dbo.#temp_documentos') IS NOT NULL
		drop table #temp_documentos;		
IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFT'))
	DROP TABLE #dadosFT	
IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFT2'))
	DROP TABLE #dadosFT2	
DECLARE @sql varchar(max)
DECLARE @sqlFinal varchar(max)
DECLARE @orderBy VARCHAR(8000)
DECLARE @PageSize int = @topMax


DECLARE @OrderCri VARCHAR(MAX)


IF @orderStamp = ''
BEGIN 
	SET @OrderCri = 'ORDER BY fdata desc, usrhora desc'
END 
ELSE 
BEGIN 
	SELECT @OrderCri='ORDER BY ' + instructionSql  FROM sortingTableOrder (NOLOCK) WHERE stamp = @orderStamp and inativo=0 and nameService='invoicingSearch'
END 




set @orderBy = ' ) X ' + @OrderCri +'  
				 OFFSET ' + convert(varchar(10),@PageSize) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
				 FETCH NEXT  ' + convert(varchar(10),@PageSize) + ' ROWS ONLY '

select @sql = N' SELECT * INTO #temp_Documentos
				FROM (
					SELECT
						 [tabela] = ''Ft''
						, ndoc AS seriesNumber 
						--, [documento] = [Td].[nmdoc]						
						, disp_webservice_view 
					FROM
						[dbo].[Td] (nolock)		 
				) as X 
	
				DECLARE @Documentos TABLE(
					seriesNumber int		
				)

				IF '''+@seriesNumber+''' <> ''''
					BEGIN
						INSERT INTO @Documentos
						SELECT * FROM dbo.up_splitToTable('''+@seriesNumber+''','','')
					END
				ELSE
				BEGIN
					INSERT INTO @Documentos
					SELECT seriesNumber FROM #temp_Documentos
				END

			--select * from #temp_Documentos

			-- Result Set Final						
			SELECT '  + convert(varchar(10),@PageSize) +' as pageSize,  (COUNT(*) OVER() / ' +  convert(varchar(10),@PageSize) +') + 1 as pageTotal, '
				+ convert(varchar(10),@PageNumber) +' as pageNumber, COUNT(*) OVER() as linesTotal		
			,*
			into #dadosFT
			FROM (
				SELECT 
					DISTINCT
					ft.ftstamp   as stamp
					, ft.[Site] AS IDInfo_site
					, ft.fdata
					, ft.usrhora

				FROM
       				[dbo].[ft] (nolock)	
					INNER JOIN @documentos ON [@documentos].[seriesNumber] = [ft].[ndoc]
					LEFT JOIN [dbo].[ft2] (nolock)	 ON [ft].[ftstamp]=[ft2].[ft2stamp]
       				INNER JOIN [dbo].[fi] (nolock)	 ON [ft].[ftstamp]=[fi].[ftstamp]
				WHERE
       				--disp_webservice_view = 1 
					1=1
					'


if @dateInit != '' and @dateEnd  != ''
	select @sql = @sql + N' AND ft.fdata BETWEEN ''' + @dateInit + '''  AND  ''' + @dateEnd + ''''

if @lastChangeDate != ''
	select @sql = @sql + N' AND ft.usrdata + ft.usrhora >= ''' + @lastChangeDate + ' ' + @lastChangeTime + ''''

if @design != ''		
	select @sql = @sql + N' AND ([fi].[design] LIKE '''+@design+''' +''%'' )'


if @ref != ''		
	select @sql = @sql + N' AND ([fi].[ref] LIKE ''' + @ref+'''+''%'')'

if @toRegulariseOnly = 1	
begin	
	print @toRegulariseOnly
	set @sql = @sql +  N' AND fi.qtt > isnull((select sum(fi1.qtt) from fi(nolock) fi1 where fi1.ofistamp=fi.fistamp and len(fi1.ofistamp)>0 ),0) ' 
end	


if @vatNr != ''		
	select @sql = @sql + N' AND [ft].[ncont] = '''+ @vatNr +''' '
	
if(@obs!='')
	select @sql = @sql + N' AND  [ft2].[obsInt] like ''%'+ @obs+'%'' ' 


if @lastChangeDate != ''
	select @sql = @sql + N' AND convert(datetime,convert(varchar(10),ft.usrdata,120) + '' '' + ft.usrhora)  >= ''' + @lastChangeDate + ' ' + @lastChangeTime + ''''

IF len(@email)>0 
BEGIN		
	declare @no_	int
	declare @estab_ int

	IF len(@number)>0 and len(@dep)>0
		select @no_ = no, @estab_ = estab from b_utentes where email = @email and no=@number and estab=@dep
	ELSE
		select top 1 @no_ = no, @estab_ = estab from b_utentes where email = @email			
		
	select @sql = @sql + ' AND [ft].[no]= ' + isnull(convert(varchar,@no_),0) + ' AND [ft].[estab] = ' + isnull(convert(varchar,@estab_),0)	
END

if @number != ''		
	select @sql = @sql + N' AND [ft].[no] = '+ @number +' '

if @dep != ''		
select @sql = @sql + N' AND [ft].[estab] = '+ @dep +' '

if @site != ''
	select @sql = @sql + N'	AND [ft].[site] = '''+@site+''' '

if @numdoc != ''		
	select @sql = @sql + N' AND convert(varchar,[ft].[fno]) = CASE WHEN '''+convert(varchar,@numdoc)+''' = '''' THEN convert(varchar,[ft].[fno]) 
																	ELSE '''+convert(varchar,@numdoc)+''' END'

if @prescriptionid != ''
	select @sql = @sql + N'	AND [ft2].[u_receita] = '''+@prescriptionid+''' '

if @suspenso != ''
	select @sql = @sql + N'	AND [ft].[cobrado] = '''+@suspenso+''' '

	
	set @sqlFinal = N'
				SELECT DISTINCT 
					#dadosFT.pageSize
					,#dadosFT.pageTotal
					,#dadosFT.pageNumber
					,#dadosFT.linesTotal
					,ft.ftstamp   as stamp
					, Convert(VARCHAR(100), [Ft].[fno]) AS IDInfo_number
					, ft.[Site] AS IDInfo_site
					, ft.ndoc As IDInfo_seriesNumber
					, ft.ftano AS  IDInfo_year
					, ft.no AS EntityInfo_number
					, ft.estab AS EntityInfo_dep					
					, UPPER(ft.nome) AS EntityInfo_name				
					, ''Cliente'' AS EntityInfo_type										
					, ft.etotal AS FinanceInfo_retailPrice
					, ft.memissao AS FinanceInfo_currency
					, 1 AS FinanceInfo_vatInc 
					, convert(varchar, ft.ousrdata, 23) AS OperationInfo_CreationDate
					, ft.ousrhora AS OperationInfo_CreationTime
					, convert(varchar, ft.usrdata, 23) AS OperationInfo_lastChangeDate
					, ft.usrinis AS OperationInfo_lastChangeUser 
					, ft.usrhora AS OperationInfo_lastChangeTime
					, rtrim(ltrim(isnull((select top 1 isnull(nome,'''') from b_us(nolock) where iniciais=ft.usrinis),''''))) AS OperationInfo_lastChangeUserName 
					, convert(varchar, ft.fdata, 23) AS GeneralInfo_docDate
					, ft2.u_receita AS GeneralInfo_prescriptionId
					, ft.u_nratend AS GeneralInfo_attendanceNumber
					, ft.totqtt AS StockInfo_qtt	
					, ft.nmdoc AS DesignInfo_designation	
					,ft.fdata
					,ft.usrhora
					,rtrim(ltrim(convert(varchar(max),isnull(ft2.obsdoc,'''')))) AS GeneralInfo_obs
					,rtrim(ltrim(convert(varchar(max),isnull(ft2.obsCl,'''')))) AS GeneralInfo_obsExternal
					,rtrim(ltrim(convert(varchar(max),isnull(ft2.obsInt,'''')))) AS GeneralInfo_obsInternal

				FROM
       				[dbo].[ft] (nolock)
					INNER JOIN #dadosFT on #dadosFT.stamp = ft.ftstamp and #dadosFT.IDInfo_site = ft.site
					LEFT JOIN [dbo].[ft2] (nolock)	 ON [ft].[ftstamp]=[ft2].[ft2stamp]
       				INNER JOIN [dbo].[fi] (nolock)	 ON [ft].[ftstamp]=[fi].[ftstamp]
					
					
	' 


		
	 set @sqlFinal = @sqlFinal + @OrderCri


	 print @sqlFinal

select @sql = @sql  + @orderBy + @sqlFinal
	   
print @sql
EXECUTE (@sql)

If OBJECT_ID('tempdb.dbo.#temp_documentos') IS NOT NULL
		drop table #temp_documentos ;
IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFT'))
	DROP TABLE #dadosFT	
IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFT2'))
	DROP TABLE #dadosFT2	
GO
GRANT EXECUTE on dbo.up_Facturacao_pesquisaGeralDocFacturacao TO PUBLIC
GRANT Control on dbo.up_Facturacao_pesquisaGeralDocFacturacao TO PUBLIC
GO


