/* SP Pesquisa Detalhe Documentos de Facturação (LINES)
J.Gomes, 2020-05-04
--------------------------------

Listagem detalhe de Documentos Facturação baseada no campo ftstamp proveniente da 
sp up_Facturacao_pesquisaDetalheDocFacturacaoHEAD.

Pesquisa nas tabelas: 	
fi

por 1 campo:
	'@stamp'
	
Campos obrigatórios: todos

OUTPUT:
---------
  ftstamp
, fistamp
, ofistamp
, ProductInfo_ref
, ProductInfo_design
, StockInfo_qtt
, StockInfo_warehouseSource
, StockInfo_site
, StockInfo_batch
, FinanceInfo_retailPrice
, FinanceInfo_discountPerc
, FinanceInfo_discountValue
, FinanceInfo_vatInc
, FinanceInfo_coPaymentRate
, FinanceInfo_coPaymentValue
, FinanceInfo_clientValue
, GeneralInfo_obs
, GeneralInfo_diploma
, VatRatesInfo_rate
, VatRatesInfo_incidenceBase
, VatRatesInfo_value


EXECUÇÃO DA SP:
---------------
exec up_Facturacao_pesquisaDetalheDocFacturacaoLines '@stamp'

	exec up_Facturacao_pesquisaDetalheDocFacturacaoLines  '8824064701120210528122323'


	select fno, ndoc, site,ftano from ft
	
	*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].up_Facturacao_pesquisaDetalheDocFacturacaoLines') IS NOT NULL
	DROP PROCEDURE [dbo].up_Facturacao_pesquisaDetalheDocFacturacaoLines ;
GO

CREATE PROCEDURE [dbo].up_Facturacao_pesquisaDetalheDocFacturacaoLines	
		 @stamp				VARCHAR(25),
		 @toRegulariseOnly  bit = 0
		   
	/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

SET @stamp	            =   rtrim(ltrim(isnull(@stamp,'')))
SET @toRegulariseOnly   =	isnull(@toRegulariseOnly,0)
Declare @site_nr int = 0
	
IF len(@stamp) > 0 
BEGIN
	DECLARE @sql varchar(max)


	select top 1 @site_nr= isnull(empresa.no,0) from ft(nolock)
	inner join empresa(nolock) on ltrim(rtrim(ft.site))= ltrim(rtrim(empresa.site))
	where ft.ftstamp=@stamp

	SET @sql = N' 	
			SELECT 
				ftstamp
				, fistamp
				, fistamp  as IdInfo_id
				, lordem   as IdInfo_order
				, ofistamp
				, bistamp
				, fi.ref AS ProductInfo_ref
				, fi.design AS ProductInfo_design
				, dbo.unityConverter(fi.qtt,''qttUnityToBox'', ' +Convert(varchar(10),isnull(@site_nr,0)) + ',fi.ref) AS StockInfo_qtt
				, armazem AS StockInfo_warehouseSource
				, dbo.unityConverter(u_stock,''stocksUnityToBox'', ' + convert(varchar(10),isnull(@site_nr,0)) + ',fi.ref) AS StockInfo_site
				, lote AS StockInfo_batch
				, cast(fi.etiliquido as numeric(15,2)) AS FinanceInfo_retailPrice 
				, cast(fi.epv as numeric(15,2)) AS FinanceInfo_unitRetailPrice 
				, [fi].[desconto] AS FinanceInfo_discountPerc
				, [fi].[u_descval] AS FinanceInfo_discountValue
				, 1 AS FinanceInfo_vatInc
				,case when ([fi].[qtt]-[Regularizar].[qtt])>0  then convert(bit,0) else convert(bit,1)  end   AS FinanceInfo_alreadyDelivered
				, u_txcomp AS FinanceInfo_coPaymentRate
				, u_ettent1 + u_ettent2 AS FinanceInfo_coPaymentValue
				, dbo.unityConverter(epv,''priceUnityToBox'', ' + convert(varchar(10),isnull(@site_nr,0)) + ',fi.ref) AS FinanceInfo_clientValue
				, lobs2 AS GeneralInfo_obs
				, u_diploma AS GeneralInfo_diploma
				, iva AS vatRatesInfo_rate
				, CAST(esltt AS numeric(12,2)) AS vatRatesInfo_incidenceBase
				, CAST((esltt * iva /100) AS numeric(12,2)) AS vatRatesInfo_value
				,ltrim(rtrim(ISNULL(grande_mercado_hmr.descr,''''))) AS ProductInfo_HmrInfo_dicBigMarketHmrDescr
				,ISNULL(grande_mercado_hmr.id,0) AS ProductInfo_HmrInfo_dicBigMarketHmrId
				,ISNULL(categoria_hmr.id,0) AS ProductInfo_HmrInfo_dicCategoryHmrId
				,ltrim(rtrim(ISNULL(categoria_hmr.descr,''''))) AS ProductInfo_HmrInfo_dicCategoryHmrDescr
				,ISNULL(mercado_hmr.id,0) AS ProductInfo_HmrInfo_dicMarketHmrId
				,ltrim(rtrim(ISNULL(mercado_hmr.descr,''''))) AS ProductInfo_HmrInfo_dicMarketHmrDescr
				,ISNULL(segmento_hmr.id,0) AS ProductInfo_HmrInfo_dicSegmentHmrId
				,ltrim(rtrim(ISNULL(segmento_hmr.descr,''''))) AS ProductInfo_HmrInfo_dicSegmentHmrDescr
				,ltrim(rtrim(st.usr1)) AS ProductInfo_brand
				,rtrim(ltrim(isnull(st.designOnline,''''))) AS ProductInfo_OnlineDesignation
				,rtrim(ltrim(isnull(st.caractOnline,''''))) AS ProductInfo_OnlineDescription1
				,rtrim(ltrim(isnull(st.apresentOnline,''''))) AS ProductInfo_OnlineDescription2
				,ISNULL(fprod.generico,0) AS ProductInfo_generic
				,st.tempoIndiponibilidade	AS ProductInfo_TimeUnavailability 
				,''dia''	AS ProductInfo_UnavailabilityUnit
		FROM 
				[dbo].[fi] (nolock)	
				LEFT JOIN [dbo].[st] (nolock)		 ON [st].[ref]=[fi].[ref]  and   st.site_nr=''' + Convert(varchar(10),isnull(@site_nr,0)) +'''
				LEFT JOIN [dbo].[fprod] (nolock)	 ON [fprod].[cnp]=[fi].[ref] 
				LEFT JOIN grande_mercado_hmr(nolock) ON st.u_depstamp = grande_mercado_hmr.id
				LEFT JOIN mercado_hmr(nolock)        ON st.u_secstamp = mercado_hmr.id
				LEFT JOIN categoria_hmr(nolock)      ON st.u_catstamp = categoria_hmr.id
				LEFT JOIN segmento_hmr(nolock)       ON st.u_segstamp = segmento_hmr.id
				CROSS APPLY (SELECT ISNULL(SUM(ofi.qtt),0) AS [qtt] FROM [dbo].[fi] as [ofi] (nolock) inner join td (nolock) as otd on ofi.ndoc = otd.ndoc where [fi].[fistamp] = [ofi].[ofistamp] AND [otd].[u_tipodoc] != 3 AND [otd].[nmdoc] not like (''%Segur%'') AND [otd].[nmdoc] not like (''%Import%'') AND [otd].[nmdoc] not like (''%Manual%'') ) AS [Regularizar]				
		WHERE 
       			ftstamp = ''' + @stamp + ''' 

				
				' 
	
	if @toRegulariseOnly = 1	
	begin	
		print @toRegulariseOnly
		set @sql = @sql +  N' AND fi.qtt > isnull((select sum(fi1.qtt) from fi(nolock) fi1 where fi1.ofistamp=fi.fistamp and len(fi1.ofistamp)>0 ),0) ' 
	end	
	
	--print @sql
	EXECUTE (@sql)

END
ELSE
	BEGIN
		print('Missing stamp')
	END


GO
GRANT EXECUTE on dbo.up_Facturacao_pesquisaDetalheDocFacturacaoLines TO PUBLIC
GRANT Control on dbo.up_Facturacao_pesquisaDetalheDocFacturacaoLines TO PUBLIC
GO