/* SP Pesquisa Detalhe Documentos de Facturação (HEAD)
J.Gomes, 2020-05-04
-------------------------------

Listagem detalhe de Documentos Facturação baseada em diversos campos.

Pesquisa nas tabelas: 	
Td, Ft, Ft2, Fi, b_utentes, B_Parameters, B_modoPag, B_modoPag_moeda, B_pagCentral

por 4 campos:
	'@number', '@seriesNumber', '@site', '@year'
	
Campos obrigatórios: todos

OUTPUT:
---------
IDInfo_number,	IDInfo_site,	IDInfo_seriesNumber,	IDInfo_extCode,	IDInfo_year
,	IDInfo_stamp,	EntityInfo_number,	EntityInfo_dep,	EntityInfo_name
,	EntityInfo_type,	EntityInfo_vatNr,	EntityInfo_ccNr,	EntityInfo_cardNumber
,	EntityInfo_cardNumber2,	EntityInfo_extId,	EntityInfo_ssNr,	FinanceInfo_retailPrice
,	FinanceInfo_currency,	FinanceInfo_discountValue,	FinanceInfo_compartValue
,	FinanceInfo_vatValue,	FinanceInfo_vatInc,	(No column name)
,	OperationInfo_CreationDate,	OperationInfo_CreationTime,	OperationInfo_lastChangeDate
,	OperationInfo_lastChangeTime,	GeneralInfo_docDate,	GeneralInfo_prescriptionId
,	GeneralInfo_suspendedSale,	GeneralInfo_obs,	StockInfo_qtt,	StockInfo_qttRef
,	DesignInfo_designation,	ShippingInfo_city,	ShippingInfo_address,	ShippingInfo_zipCode
,	ShippingInfo_countryCode,	ShippingInfo_carDescr,	ShippingInfo_time,	ShippingInfo_transporter
,	ShippingInfo_homeDelivery,	PaymentInfo_attendanceNumber,	PaymentInfo_visa,	PaymentInfo_cash
,	PaymentInfo_check,	PaymentInfo_atm,	PaymentInfo_voucher,	PaymentInfo_cashMachine
,	CompartInfo_codPla,	CompartInfo_descPla

EXECUÇÃO DA SP:
---------------
exec up_Facturacao_pesquisaDetalheDocFacturacaoHead '@number', '@seriesNumber', '@site', '@year'

	exec up_Facturacao_pesquisaDetalheDocFacturacaoHead  '18', '3','Loja 1','2022'

	exec up_Facturacao_pesquisaDetalheDocFacturacaoHead  '223', '75','Loja 1','2018'


	exec up_Facturacao_pesquisaDetalheDocFacturacaoHead  '14068', '3','Loja 1','2021'



	
	select fno, ndoc, site,ftano from ft

	select *from td where tipodoc = 1
	select *from td where tipodoc = 2
	select *from td where tipodoc = 3
	select *from td where tipodoc = 4

	select cm,* from cm1


	select * from td
	inner join cm1 on cm1.
	
	*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].up_Facturacao_pesquisaDetalheDocFacturacaoHead') IS NOT NULL
	DROP PROCEDURE [dbo].up_Facturacao_pesquisaDetalheDocFacturacaoHead ;
GO


CREATE PROCEDURE [dbo].up_Facturacao_pesquisaDetalheDocFacturacaoHead	
	 @numdoc			VARCHAR(10)
	,@seriesNumber	    VARCHAR(500)
	,@site				VARCHAR(60)	
	,@year				VARCHAR(4) 
	
	   
/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

SET @numdoc				=   rtrim(ltrim(isnull(@numdoc,'')))
SET @seriesNumber		=   rtrim(ltrim(isnull(@seriesNumber,'')))
SET @site				=   rtrim(ltrim(isnull(@site,'')))
SET @year				=   rtrim(ltrim(isnull(@year,'')))


IF len(@numdoc) > 0 and len(@seriesNumber) > 0 and len(@site) > 0 and len(@year) > 0
BEGIN
	If OBJECT_ID('tempdb.dbo.#temp_documentos') IS NOT NULL
			drop table #temp_documentos;
				

	DECLARE @sql varchar(max)
	DECLARE @sql1 varchar(max)

	--------------
	--PaymentInfo

	declare @epaga1 varchar(50) 
	declare @epaga1_ varchar(50) 
	declare @evdinheiro varchar(50)
	declare @evdinheiro_ varchar(50)
	declare @echtotal varchar(50)
	declare @echtotal_ varchar(50)
	declare @epaga2 varchar(50) 
	declare @epaga2_ varchar(50) 
	declare @epaga3 varchar(50) 
	declare @epaga3_ varchar(50) 
	declare @epaga4 varchar(50) 
	declare @epaga4_ varchar(50) 
	declare @epaga5 varchar(50) 
	declare @epaga5_ varchar(50) 
	declare @epaga6 varchar(50) 
	declare @epaga6_ varchar(50) 

	DECLARE @textvalue varchar(15)


	DECLARE @id_lt varchar(100) =''	
	DECLARE @site_nr INT
	select @id_lt = rtrim(ltrim(id_lt)), @site_nr= no from empresa(nolock) where site = @site


	DECLARE @downloadURL VARCHAR(100) = ''
	SELECT  @downloadURL = rtrim(ltrim(textValue)) FROM B_Parameters(nolock) WHERE stamp='ADM0000000313'


	DECLARE @imagemURL varchar(300) 
	set   @imagemURL = @downloadURL + @id_lt
	
	select @textvalue = textvalue from B_Parameters where stamp='ADM0000000260'
	IF @textvalue = 'EURO'
	begin
		select @epaga1 = design, @epaga1_ = right(campoFact,len(campoFact)-4) from B_modoPag(nolock) where campoFact like '%epaga1%'  
		select @evdinheiro = design, @evdinheiro_ = right(campoFact,len(campoFact)-4)  from B_modoPag(nolock) where campoFact like '%evdinheir%'  
		select @echtotal = design, @echtotal_ = right(campoFact,len(campoFact)-3)  from B_modoPag(nolock) where campoFact like '%echtotal%'  
		select @epaga2 = design, @epaga2_ = right(campoFact,len(campoFact)-4) from B_modoPag(nolock) where campoFact like '%epaga2%'  
		select @epaga3 = design, @epaga3_ = right(campoFact,len(campoFact)-4) from B_modoPag(nolock) where campoFact like '%epaga3%'  
		select @epaga4 = design, @epaga4_ = right(campoFact,len(campoFact)-4) from B_modoPag(nolock) where campoFact like '%epaga4%'  
		select @epaga5 = design, @epaga5_ = right(campoFact,len(campoFact)-4) from B_modoPag(nolock) where campoFact like '%epaga5%' 
		select @epaga6 = design, @epaga6_ = right(campoFact,len(campoFact)-4) from B_modoPag(nolock) where campoFact like '%epaga6%'  
	end
	ELSE
	begin
		select @epaga1 = design, @epaga1_ = right(campoFact,len(campoFact)-4) from B_modoPag_moeda(nolock) where campoFact like '%epaga1%'  
		select @evdinheiro = design, @evdinheiro_ = right(campoFact,len(campoFact)-4)  from B_modoPag_moeda(nolock) where campoFact like '%evdinheir%'  
		select @echtotal = design, @echtotal_ = right(campoFact,len(campoFact)-3)  from B_modoPag_moeda(nolock) where campoFact like '%echtotal%'  
		select @epaga2 = design, @epaga2_ = right(campoFact,len(campoFact)-4) from B_modoPag_moeda(nolock) where campoFact like '%epaga2%' 
		select @epaga3 = design, @epaga3_ = right(campoFact,len(campoFact)-4) from B_modoPag_moeda(nolock) where campoFact like '%epaga3%'  
		select @epaga4 = design, @epaga4_ = right(campoFact,len(campoFact)-4) from B_modoPag_moeda(nolock) where campoFact like '%epaga4%'  
		select @epaga5 = design, @epaga5_ = right(campoFact,len(campoFact)-4) from B_modoPag_moeda(nolock) where campoFact like '%epaga5%' 
		select @epaga6 = design, @epaga6_ = right(campoFact,len(campoFact)-4) from B_modoPag_moeda(nolock) where campoFact like '%epaga6%'  
	end

	if(rtrim(ltrim(@epaga4)) = ltrim(rtrim(@epaga5)) and rtrim(ltrim(@epaga5)) = ltrim(rtrim(@epaga6)) )
			set @epaga5 = 	@epaga5 + "."
			set @epaga6 = 	@epaga6 + ".."

	if(rtrim(ltrim(@epaga4)) = ltrim(rtrim(@epaga5)))
			set @epaga5 = 	@epaga5 + "."

	if(rtrim(ltrim(@epaga5)) = ltrim(rtrim(@epaga6)))
			set @epaga6 = 	@epaga6 + "."

	if(rtrim(ltrim(@epaga4)) = ltrim(rtrim(@epaga6)))
			set @epaga6 = 	@epaga6 + "."
	
	--------------

	SET @sql = N' 
		SELECT * INTO #temp_Documentos
		FROM (
			SELECT
				[tabela] = ''Ft''
			, ndoc AS seriesNumber 

			--, [documento] = [Td].[nmdoc]						
			, disp_webservice_view 
		FROM
			[dbo].[Td] (nolock)	 
		) as X 
	
		DECLARE @Documentos TABLE(
			seriesNumber int		
		)

		IF '''+@seriesNumber+''' <> ''''
			BEGIN
				INSERT INTO @Documentos
				SELECT * FROM dbo.up_splitToTable('''+@seriesNumber+''','','')
			END
		ELSE
		BEGIN
			INSERT INTO @Documentos
			SELECT seriesNumber FROM #temp_Documentos
		END

		select B_pagCentral.nratend AS PaymentInfo_attendanceNumber, B_pagCentral.' +  @epaga1_ + ' as [' + replace(replace(replace(replace(@epaga1,'Visa','PaymentInfo_visa'),'Multibanco','PaymentInfo_atm'),'Vales','PaymentInfo_voucher'),'Máq. de dinheiro','PaymentInfo_cashMachine') + 
			']' 			
		if @evdinheiro_ is not null	
			set  @sql = @sql + ',  B_pagCentral.' + @evdinheiro_ + ' as [' + replace(@evdinheiro,'Dinheiro','PaymentInfo_cash') + ']' 
		if @echtotal_ is not null	
			set  @sql = @sql + ',  B_pagCentral.' +  @echtotal_ + ' as [' + replace(@echtotal, 'Cheques','PaymentInfo_check') + ']' 
		if @epaga2_ is not null	
			set  @sql = @sql + ',  B_pagCentral.' +  @epaga2_ + ' as [' + replace(replace(replace(replace(@epaga2,'Visa','PaymentInfo_visa'),'Multibanco','PaymentInfo_atm'),'Vales','PaymentInfo_voucher'),'Máq. de dinheiro','PaymentInfo_cashMachine') + ']' 
		if @epaga3_ is not null	
			set  @sql = @sql + ',  B_pagCentral.' +  @epaga3_ + ' as [' + replace(replace(replace(replace(@epaga3,'Visa','PaymentInfo_visa'),'Multibanco','PaymentInfo_atm'),'Vales','PaymentInfo_voucher'),'Máq. de dinheiro','PaymentInfo_cashMachine') + ']'
		if @epaga4_ is not null	
			set  @sql = @sql + ', B_pagCentral.' +  @epaga4_ + ' as [' + replace(replace(replace(replace(@epaga4,'Visa','PaymentInfo_visa'),'Multibanco','PaymentInfo_atm'),'Vales','PaymentInfo_voucher'),'Máq. de dinheiro','PaymentInfo_cashMachine') + ']'
		if @epaga5_ is not null	
			set  @sql = @sql + ', B_pagCentral.' +  @epaga5_ + ' as [' + replace(replace(replace(replace(@epaga5,'Visa','PaymentInfo_visa'),'Multibanco','PaymentInfo_atm'),'Vales','PaymentInfo_voucher'),'Máq. de dinheiro','PaymentInfo_cashMachine') + ']'
		if @epaga6_ is not null	
			set  @sql = @sql + ', B_pagCentral.' +  @epaga6_ + ' as [' + replace(replace(replace(replace(@epaga6,'Visa','PaymentInfo_visa'),'Multibanco','PaymentInfo_atm'),'Vales','PaymentInfo_voucher'),'Máq. de dinheiro','PaymentInfo_cashMachine') + ']'
					
	set	@sql = @sql + ' 
		into #tbltemp
		from B_pagCentral
		left join ft(nolock) on ft.u_nratend = B_pagCentral.nrAtend
		where 1=1
		'

	if @site != ''
		SET @sql = @sql + N'	AND [ft].[site] = '''+@site+''' '

	if @numdoc != ''		
		SET @sql = @sql + N'  AND [ft].[fno] =  '''+convert(varchar,@numdoc)+''''
	if @year != ''
		select @sql = @sql + N'	AND [ft].[ftano] = '''+@year+''' '


	set	@sql = @sql + ' 
		SELECT 
			
			distinct	
			top 1							
				Convert(VARCHAR(100), [Ft].[fno]) AS IDInfo_number
			, ft.[Site] AS IDInfo_site
			, ft.ndoc As IDInfo_seriesNumber
			, '''' As IDInfo_extCode
			, ft.ftano AS  IDInfo_year
			, ft.ftstamp As IDInfo_stamp
			, ft.u_nrAtend AS IDInfo_attendanceNumber	
			, ft.no AS EntityInfo_number
			, ft.estab AS EntityInfo_dep					
			, UPPER(ft.nome) AS EntityInfo_name				
			, ''Cliente'' AS EntityInfo_type
			, ft.ncont AS EntityInfo_vatNr
			, ft.bino AS EntityInfo_ccNr
			, ft2.u_nbenef As EntityInfo_cardNumber
			, ft2.u_nbenef2 As EntityInfo_cardNumber2
			, b_utentes.no_ext As EntityInfo_extId
			, b_utentes.nrss As EntityInfo_ssNr	
						
			, ft.etotal AS FinanceInfo_retailPrice
			, ft.memissao AS FinanceInfo_currency
			, SUM([fi].[desconto]) OVER(PARTITION BY [fi].[fistamp]) AS FinanceInfo_discountValue
			, SUM([fi].[u_ettent1] + [fi].[u_ettent2] ) OVER(PARTITION BY [fi].[fistamp]) AS FinanceInfo_coPaymentValue
			, ft.ettiva AS FinanceInfo_vatValue 
			, 1 AS FinanceInfo_vatInc 
			, ft2.motiseimp AS FinanceInfo_taxExemption
			, case when ft.tipoDoc in(1,2) then ''D'' 
					when ft.tipoDoc  = 3  then ''C''
					else   ''O'' end	 as FinanceInfo_docType
			, isnull((SELECT top 1 isnull(valorPago,ft.etotal) from  uf_retornaValoresPagosDoc (ft.ftstamp ,''FT'')),ft.etotal)  AS FinanceInfo_valuePaid
			, isnull((SELECT top 1 isnull(valorDivida,0.00) from  uf_retornaValoresPagosDoc (ft.ftstamp ,''FT'')),0.00)  AS FinanceInfo_valueRemaining
			, convert(varchar, ft.ousrdata, 23) AS OperationInfo_CreationDate
			, ft.ousrhora AS OperationInfo_CreationTime
			, convert(varchar, ft.usrdata, 23) AS OperationInfo_lastChangeDate
			, ft.usrhora AS OperationInfo_lastChangeTime
			, ft.usrinis AS OperationInfo_lastChangeUser 
			, rtrim(ltrim(isnull((select top 1 isnull(nome,'''') from b_us(nolock) where iniciais=ft.usrinis),''''))) AS OperationInfo_lastChangeUserName   

			, convert(varchar, ft.fdata, 23) AS GeneralInfo_docDate
			, ft2.u_receita AS GeneralInfo_prescriptionId
			, ft2.u_vdsusprg AS GeneralInfo_suspendedSale
			, convert(varchar,ft2.obsdoc) AS GeneralInfo_obs
			, dbo.unityConverter(ft.totqtt,''qttUnityToBox'', ' +Convert(varchar(10),isnull(@site_nr,0)) + ',fi.ref)  AS StockInfo_qtt

			, (select dbo.ListaRefDistintasFi([fi].[fistamp])) AS StockInfo_qttRef

			, ft.nmdoc AS DesignInfo_designation	
						
			, COALESCE(NULLIF(ft2.local,''''), ft2.localidade_ent) AS ShippingInfo_city
			, COALESCE(NULLIF(ft2.morada,''''), ft2.morada_ent) AS ShippingInfo_address
			, COALESCE(NULLIF(ft2.codpost,''''), ft2.codpost_ent) AS ShippingInfo_zipCode
			, ft2.pais_ent AS ShippingInfo_countryCode	
			, isnull((SELECT top 1 isnull(resultado,''N'') from  uf_TipoDocFacturaRedeClaro (ft.ftstamp ,ft.site)),''N'')  AS FinanceInfo_docClassExtern
			,rtrim(ltrim(convert(varchar(max),isnull(ft2.obsdoc,'''')))) AS GeneralInfo_obs
			,rtrim(ltrim(convert(varchar(max),isnull(ft2.obsCl,'''')))) AS GeneralInfo_obsExternal
			,rtrim(ltrim(convert(varchar(max),isnull(ft2.obsInt,'''')))) AS GeneralInfo_obsInternal
			'
			
		
							
	SET @sql1  = N'		, ft2.u_viatura AS ShippingInfo_carDescr
			, ft2.horaentrega AS ShippingInfo_time
			, ft2.contacto AS ShippingInfo_transporter
			, ft2.u_vddom AS ShippingInfo_homeDelivery
			, ltrim(rtrim(ft.zona)) as AddressInfo_zone
			, ltrim(rtrim(ft.morada)) as AddressInfo_address	
			, ltrim(rtrim(ft.codpost)) as AddressInfo_zipCode	
			, ltrim(rtrim(ft.local)) as AddressInfo_city							
			, #tbltemp.*
			, ft2.u_codigo AS CompartInfo_codPla
			, ft2.u_design AS CompartInfo_descPla
			, ft2.u_codigo2 AS CompartInfo_codPla2
			, ft2.u_design2 AS CompartInfo_descPla2
			, convert(varchar,isnull((select usrdata from cptpla(nolock) where codigo=ft2.u_codigo),''19000101''),23) AS CompartInfo_codPlaLastChangeDate
			, convert(varchar,isnull((select usrdata from cptpla(nolock)  where codigo=ft2.u_codigo2),''19000101''),23) AS CompartInfo_codPla2LastChangeDate
			, isnull((select inactivo from cptpla(nolock)  where codigo=ft2.u_codigo),1) AS CompartInfo_codPlaInactive
			, isnull((select inactivo from cptpla(nolock)  where codigo=ft2.u_codigo2),1) AS CompartInfo_codPla2Inactive
			, ISNULL((SELECT TOP 1 ISNULL('''+ @imagemURL +''' + ''/''+ anexosstamp,'''') FROM anexos (nolock) WHERE filename like ''%.pdf'' and regstamp = ft.ftstamp order by usrdata desc),'''') AS GeneralInfo_linkPdf
			, '''+LTRIM(RTRIM(@id_lt))+'''		AS IDInfo_siteId		
		FROM 

	
				[dbo].[ft] (nolock)	
				LEFT JOIN [dbo].[ft2] (nolock)	 ON [ft].[ftstamp]=[ft2].[ft2stamp]
       			INNER JOIN [dbo].[fi] (nolock)	 ON [ft].[ftstamp]=[fi].[ftstamp]
				INNER JOIN [#temp_Documentos] on [ft].[ndoc] = [#temp_Documentos].[seriesNumber]
				INNER JOIN @documentos ON [@documentos].[seriesNumber] = [ft].[ndoc]
				inner join b_utentes  (nolock) on b_utentes.no = ft.no and b_utentes.estab=ft.estab
				left join [#tbltemp] on ft.u_nratend = [#tbltemp].PaymentInfo_attendanceNumber
		
		
			WHERE 
				1=1  '
					
	if @site != ''
		SET @sql1 = @sql1 + N'	AND [ft].[site] = '''+@site+''' '

	if @numdoc != ''		
		SET @sql1 = @sql1 + N' AND [ft].[fno] =  '''+convert(varchar,@numdoc)+''''
	if @year != ''
		select @sql1 = @sql1 + N'	AND [ft].[ftano] = '''+@year+''' '

																	
	SET @sql1 = @sql1 + N'
		ORDER BY GeneralInfo_docDate DESC, OperationInfo_CreationDate DESC,OperationInfo_CreationTime DESC;'
	   
	print @sql + @sql1
	EXECUTE (@sql + @sql1)


				


	If OBJECT_ID('tempdb.dbo.#temp_documentos') IS NOT NULL
			drop table #temp_documentos ;
	If OBJECT_ID('tempdb.dbo.#tbltemp') IS NOT NULL
			drop table #tbltemp ;

END
ELSE
	BEGIN
		print('Missing numdoc, seriesNumber, site or year')
	END


GO
GRANT EXECUTE on dbo.up_Facturacao_pesquisaDetalheDocFacturacaoHead TO PUBLIC
GRANT Control on dbo.up_Facturacao_pesquisaDetalheDocFacturacaoHead TO PUBLIC
GO


