/* SP Pesquisa Detalhe Documentos de Facturação (Rates)
J.Gomes, 2020-05-06
-------------------------------


Listagem agregada de valores de IVA,  baseada no campo ftstamp proveniente da 
sp up_Facturacao_pesquisaDetalheDocFacturacaoHead.

Pesquisa nas tabela: 	
fi

por 1 campo:
	'@stamp'
	
Campos obrigatórios: todos

OUTPUT:
---------
vatRatesInfo_taxa,	vatRatesInfo_incidenceBase,	vatRatesInfo_value


EXECUÇÃO DA SP:
---------------
exec up_Facturacao_pesquisaDetalheDocFacturacaoRates '@stamp'

	exec up_Facturacao_pesquisaDetalheDocFacturacaoRates  'FRrFC54CD78-9C6C-4EFA-B48'

	*/
	
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


IF OBJECT_ID('[dbo].[up_Facturacao_pesquisaDetalheDocFacturacaoRates]') IS NOT NULL
		DROP PROCEDURE [dbo].[up_Facturacao_pesquisaDetalheDocFacturacaoRates] ;
GO


CREATE PROCEDURE [dbo].[up_Facturacao_pesquisaDetalheDocFacturacaoRates]	
		 @stamp				VARCHAR(25)
	/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

SET @stamp	=   rtrim(ltrim(isnull(@stamp,'')))

IF len(@stamp) > 0 
BEGIN	
	DECLARE @sql varchar(500)

	select @sql = N' 	
					SELECT 
						  iva AS vatRatesInfo_rate
						--, esltt AS vatRatesInfo_incidenceBase
						, CAST(esltt AS numeric(12,2)) AS vatRatesInfo_incidenceBase
						--, esltt * iva /100 AS vatRatesInfo_value						
						, CAST((esltt * iva /100) AS numeric(12,2)) AS vatRatesInfo_value
					FROM
       					[dbo].fi(nolock)			
					WHERE
       					ftstamp = ''' + @stamp + ''' '
	--print @sql
	EXECUTE (@sql)
END
ELSE
	BEGIN
		print('Missing stamp')
	END

GO
GRANT EXECUTE on dbo.up_Facturacao_pesquisaDetalheDocFacturacaoRates TO PUBLIC
GRANT Control on dbo.up_Facturacao_pesquisaDetalheDocFacturacaoRates TO PUBLIC
GO