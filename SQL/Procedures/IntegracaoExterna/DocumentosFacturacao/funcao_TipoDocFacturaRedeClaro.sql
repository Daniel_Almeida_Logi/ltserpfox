

/****** 

	Object:  select * from dbo.uf_TipoDocFacturaRedeClaro('FR01AB30EB-E3AC-47FF-BB9 ','Loja 1')    

	use ltstag
	select * from dbo.uf_TipoDocFacturaRedeClaro('FR01AB30EB-E3AC-47FF-BB9 ','Loja 1') --1
	select * from dbo.uf_TipoDocFacturaRedeClaro('FRB669F9C1-AC68-4311-BCD','Loja 1')   -- 2
	select * from dbo.uf_TipoDocFacturaRedeClaro('FRD6498032-63F8-4FDE-8A9 ','Loja 1')     -- 3 
	select * from dbo.uf_TipoDocFacturaRedeClaro('FR25DF6117-D9A4-48C5-A66 ','Loja 1')     -- 4
	select * from dbo.uf_TipoDocFacturaRedeClaro('FR3BC05296-29D6-43F5-9F8','Loja 1')     -- 5
	select * from dbo.uf_TipoDocFacturaRedeClaro('FRA61C927F-2BCB-4D2B-A76','Loja 1')     -- N
	select * from dbo.uf_TipoDocFacturaRedeClaro('FRBEDCA4C6-6536-4171-971','Loja 1')     -- P
	select * from dbo.uf_TipoDocFacturaRedeClaro('FRF14E5A98-A313-49D7-BAD','Loja 1')     -- R
	select * from dbo.uf_TipoDocFacturaRedeClaro('FR0A473969-B920-43CC-A4A ','Loja 1')     -- M
	select * from dbo.uf_TipoDocFacturaRedeClaro('FRF14E5A98-A313-49D7-BAD  ','Loja 1')     -- R
	select * from dbo.uf_TipoDocFacturaRedeClaro('FRC2FC2080-1B2D-4778-ABA  ','Loja 1')     -- S

	select td.u_tipodoc,td.cmcc, fno,ft.nmdoc,ft.ndoc, ftano,ft.tipodoc,cobrado, ft2.u_abrev, ft2.u_codigo, ft2.u_codigo2,u_vddom,* from ft(nolock) 
	inner join ft2(nolock) on ft2.ft2stamp = ft.ftstamp
	inner join td(nolock) on td.ndoc = ft.ndoc
	where fdata>='20211215'  and fno ='60'



-- Author:		<DA>
-- Create date: <16-12-2021>
-- Description:	<Retorna tipificacao de documentos>
-- =============================================

 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





if OBJECT_ID('[dbo].[uf_TipoDocFacturaRedeClaro]') IS NOT NULL
	DROP FUNCTION [dbo].[uf_TipoDocFacturaRedeClaro];
GO



CREATE FUNCTION [dbo].[uf_TipoDocFacturaRedeClaro] 
(	
	@stamp varchar(30),
	@site varchar(30)
)
RETURNS @tempTicoDoc  TABLE   
(
	tipoDoc      varchar(3),
	suspensa     bit,
	protocolo    bit,
	devolucao    bit,
	regClient    bit,
	domiciliaria bit,
	resultado    varchar(1)
)
AS
BEGIN


		insert into @tempTicoDoc 
		select 
			top 1
			tipoDoc =    case   when td.cmcc > 0 and td.cmcc < 50   then 'F'
							  when td.cmcc > 50                    then 'C'
							  else   'O'
							  end,
			suspensa     = ISNULL(ft.cobrado,0),
			protocolo    = case when u_codigo = 'DS' OR  u_codigo2 = 'DS'  then 1 else 0 end,
			devolucao    = case when td.u_tipodoc = 7 then 1 else 0 end,
			regClient    = case when td.u_tipodoc = 2 then 1 else 0 end,
			domiciliaria = isnull(ft2.u_vddom,0),
			resultado    = ''

		from 
			ft(nolock)
		inner join 
			ft2(nolock) on ft.ftstamp = ft2stamp
		inner join 
			td(nolock) on td.ndoc = ft.ndoc and td.site = ft.site
		where 
			ftstamp = @stamp and 
			ft.site=@site and
			td.site = @site
		

		update 
			@tempTicoDoc
		set
			resultado =
					case 
						 when tipoDoc='F' and suspensa = 1     then '2'     -- "Suspensa a credito" 
						 when tipoDoc='F' and protocolo = 1    then '3'     -- "Protocolo a credito"
						 when tipoDoc='C' and devolucao = 1    then '4'     -- "Devolucao a credito"  
						 when tipoDoc='F' and domiciliaria = 1 then '5'     -- "domiciliaria a credito" 
						 when protocolo= 1                     then 'P'     -- "Protocolo" 
						 when regClient= 1                     then 'R'     -- "Reg. credito" 
						 when domiciliaria= 1                  then 'M'     -- "Domiciliaria"
						 when devolucao= 1                     then 'D'     -- "Devolucao"  
						 when tipoDoc='F'                      then '1'     -- "Normal a credito" 
						 when suspensa=1                       then 'S'     -- "Suspensa" 
						 else                                       'N'     -- "Normal" 
					end

	
	

	return
END
GO







