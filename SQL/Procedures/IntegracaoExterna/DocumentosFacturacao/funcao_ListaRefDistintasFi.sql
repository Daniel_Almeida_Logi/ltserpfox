
GO

/****** Object:  UserDefinedFunction [dbo].[ListaRefDistintasBi]    Script Date: 04/05/2020 15:08:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[ListaRefDistintasFi]') IS NOT NULL
	DROP FUNCTION [dbo].ListaRefDistintasFi;
GO




-- =============================================
-- Author:		<JG>
-- Create date: <24-04-2020>
-- Description:	<calcular total de ref distintas para determinado bostamp>
-- =============================================
CREATE FUNCTION [dbo].[ListaRefDistintasFi] 
(	
	@fistamp varchar(30)
)
RETURNS int
AS
BEGIN
	declare @aa int
	set @aa = 0
	select @aa =
		count(distinct ref) from Fi(nolock) where fistamp = ''+@fistamp+''
	
	RETURN @aa
END
GO


