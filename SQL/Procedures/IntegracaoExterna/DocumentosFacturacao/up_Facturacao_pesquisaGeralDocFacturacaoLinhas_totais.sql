/* SP Pesquisa dos totais dos Documentos de Facturação apartir das linhas
José Simões, 2020-06-09
--------------------------------

totais dos Documentos de Facturação baseada em diversos campos apartir das linhas.
e com possibilidade de filtragem acima de determinada data e hora de alteração.

Pesquisa nas tabelas: 	
Td, Ft, Ft2, Fi, b_utentes

por 16 campos:
	@number, @dep, @vatNr, @email, @ref, @design, @site, @numdoc, @seriesNumber
	, @prescriptionid, @suspenso, @lastChangeDate, @lastChangeTime
	, @dateInit, @dateEnd, @pageNumber 
	
Campos obrigatórios: nenhum



EXECUÇÃO DA SP:
---------------
exec up_Facturacao_pesquisaGeralDocFacturacaoLinhas_totais '@number', '@dep', '@vatNr', '@email', '@ref', '@design'
, '@site', '@numdoc', '@seriesNumber', @prescriptionid, @suspenso
, '@lastChangeDate', '@lastChangeTime','@dateInit','@dateEnd' , @pageNumber, @orderStamp
	

	exec up_Facturacao_pesquisaGeralDocFacturacaoLinhas_totais  '', NULL,NULL,NULL,NULL, NULL,'Loja 1',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL
	exec up_Facturacao_pesquisaGeralDocFacturacaoLinhas_totais  201, 0,NULL,NULL,NULL, NULL,'Loja 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
	exec up_Facturacao_pesquisaGeralDocFacturacaoLinhas_totais  201, 0,NULL,NULL,NULL, NULL,'Loja 1',NULL,NULL,NULL,NULL,NULL,NULL,'20200617','20200619'
	*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Facturacao_pesquisaGeralDocFacturacaoLinhas_totais]') IS NOT NULL
	DROP PROCEDURE [dbo].up_Facturacao_pesquisaGeralDocFacturacaoLinhas_totais ;
GO

CREATE PROCEDURE [dbo].up_Facturacao_pesquisaGeralDocFacturacaoLinhas_totais	
	 @number			VARCHAR(10)
	,@dep				VARCHAR(3)
	,@vatNr				VARCHAR(20)
	,@email				VARCHAR(50)
	,@ref				VARCHAR(18)
	,@design			VARCHAR(60)
	,@site				VARCHAR(60)	
	,@numdoc			VARCHAR(20)
	,@seriesNumber	    VARCHAR(500)
	,@prescriptionid	VARCHAR(8)
	,@suspenso			VARCHAR(1)
	,@lastChangeDate	varchar(10) = '1900-01-01'
	,@lastChangeTime	varchar(8)  = '00:00:00'
	,@dateInit			varchar(10) = '1900-01-01'
	,@dateEnd	        varchar(10)  = '3000-01-01'
	,@toRegulariseOnly	bit = 0

	   
/* WITH ENCRYPTION */
AS



SET NOCOUNT ON

SET @ref				=   rtrim(ltrim(isnull(@ref,'')))
SET @number				=   rtrim(ltrim(isnull(@number,'')))
SET @dep				=   rtrim(ltrim(isnull(@dep,'')))
SET @vatNr				=   rtrim(ltrim(isnull(@vatNr,'')))
SET @design				=   rtrim(ltrim(isnull(@design,'')))
SET @prescriptionid		=   rtrim(ltrim(isnull(@prescriptionid,'')))
SET @suspenso			=   rtrim(ltrim(isnull(@suspenso,'')))
SET @numdoc				=   rtrim(ltrim(isnull(@numdoc,'')))
SET @seriesNumber		=   rtrim(ltrim(isnull(@seriesNumber,'')))
SET @site				=   rtrim(ltrim(isnull(@site,'')))
SET @email				=   rtrim(ltrim(isnull(@email,'')))
SET @lastChangeDate		=   rtrim(ltrim(isnull(@lastChangeDate,'1900-01-01')))
SET @lastChangeTime		=   rtrim(ltrim(isnull(@lastChangeTime,'00:00:00')))
SET @dateInit		    =   rtrim(ltrim(isnull(@dateInit,'1900-01-01')))
SET @dateEnd		    =   rtrim(ltrim(isnull(@dateEnd,'3000-01-01')))
set @toRegulariseOnly   =	isnull(@toRegulariseOnly,0)


If OBJECT_ID('tempdb.dbo.#temp_documentos') IS NOT NULL
		drop table #temp_documentos;		


IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosCc'))
	DROP TABLE #dadosTotais

DECLARE @sql varchar(max)

DECLARE @id_lt varchar(100) =''	
DECLARE @site_nr INT
select @id_lt = id_lt, @site_nr= no from empresa where site = @site
  


select @sql = N' SELECT * INTO #temp_Documentos
				FROM (
					SELECT
						 [tabela] = ''Ft''
						, ndoc AS seriesNumber 
						--, [documento] = [Td].[nmdoc]						
						, disp_webservice_view 
					FROM
						[dbo].[Td] (nolock)	
					where
						disp_webservice_view = 1		 
				) as X 
	
				DECLARE @Documentos TABLE(
					seriesNumber int		
				)

				IF '''+@seriesNumber+''' <> ''''
					BEGIN
						INSERT INTO @Documentos
						SELECT * FROM dbo.up_splitToTable('''+@seriesNumber+''','','')
					END
				ELSE
				BEGIN
					INSERT INTO @Documentos
					SELECT seriesNumber FROM #temp_Documentos
				END

			--select * from #temp_Documentos

			-- Result Set Final						

				SELECT 
					  ft.ftstamp
					, ft.nmdoc as nmdoc
					,ISNULL(Sum(dbo.unityConverter(fi.qtt,''qttUnityToBox'', ' +Convert(varchar(10),isnull(@site_nr,0)) + ',fi.ref) ),0) AS totqtt
					,ISNULL(ft.etotal,0) AS etotal
					,ft.fdata
					, ISNULL((case when fdata BETWEEN ''' + @dateInit + '''  AND  ''' + @dateEnd + ''' then 1 else 0 end),0) as Period									
				into
					#dadosTotais	
				FROM
       				[dbo].[fi] (nolock)	
					INNER JOIN [dbo].[ft] (nolock)		 ON [ft].[ftstamp]=[fi].[ftstamp] 
					LEFT JOIN [dbo].[st] (nolock)		 ON [st].[ref]=[fi].[ref] and st.site_nr=''' + Convert(varchar(10),@site_nr) +'''
					LEFT JOIN [dbo].[ft2] (nolock)		 ON [ft].[ftstamp]=[ft2].[ft2stamp] 
					INNER JOIN @documentos				 ON [@documentos].[seriesNumber] = [ft].[ndoc]
				WHERE
       				1 = 1 and fi.ref!='''' and fi.ref!=''R000001''
					AND  ft.nmdoc != ''Inserção de Receita''  and ft.site = ''' +  @site + '''
					'

if @lastChangeDate != ''
	select @sql = @sql + N' AND ft.usrdata + ft.usrhora >= ''' + @lastChangeDate + ' ' + @lastChangeTime + ''''

if @design != ''		
	select @sql = @sql + N' AND ([fi].[design] LIKE '''+@design+''' +''%'' )'

if @ref != ''		
	select @sql = @sql + N' AND ([fi].[ref] LIKE ''' + @ref+'''+''%'')'

if @toRegulariseOnly = 1	
begin	
	print @toRegulariseOnly
	set @sql = @sql +  N' AND fi.qtt > isnull((select sum(fi1.qtt) from fi(nolock) fi1 where fi1.ofistamp=fi.fistamp and len(fi1.ofistamp)>0 ),0) ' 
end	
	

if @vatNr != ''		
	select @sql = @sql + N' AND [ft].[ncont] = '''+ @vatNr +''' '
	
if @lastChangeDate != ''
	select @sql = @sql + N' AND ft.usrdata + ft.usrhora >= ''' + @lastChangeDate + ' ' + @lastChangeTime + ''''

IF len(@email)>0 
BEGIN		
	declare @no_	int
	declare @estab_ int

	IF len(@number)>0 and len(@dep)>0
		select @no_ = no, @estab_ = estab from b_utentes where email = @email and no=@number and estab=@dep
	ELSE
		select top 1 @no_ = no, @estab_ = estab from b_utentes where email = @email			
		
	select @sql = @sql + ' AND [ft].[no]= ' + isnull(convert(varchar,@no_),0) + ' AND [ft].[estab] = ' + isnull(convert(varchar,@estab_),0)	
END

if @number != ''		
	select @sql = @sql + N' AND [ft].[no] = '+ @number +' '

if @dep != ''		
select @sql = @sql + N' AND [ft].[estab] = '+ @dep +' '

if @site != ''
	select @sql = @sql + N'	AND [ft].[site] = '''+@site+''' '

if @numdoc != ''		
	select @sql = @sql + N' AND convert(varchar,[ft].[fno]) = CASE WHEN '''+convert(varchar,@numdoc)+''' = '''' THEN convert(varchar,[ft].[fno]) 
																	ELSE '''+convert(varchar,@numdoc)+''' END'

if @prescriptionid != ''
	select @sql = @sql + N'	AND [ft2].[u_receita] = '''+@prescriptionid+''' '

if @suspenso != ''
	select @sql = @sql + N'	AND [ft].[cobrado] = '''+@suspenso+''' '

select @sql = @sql + N'	GROUP BY ft.ftstamp, 
								 ft.nmdoc, 
								 ft.totqtt,
								 ft.etotal,
								 ft.fdata			
								 '

select @sql = @sql + ' SELECT 						
							Total_ndoc			= ISNULL(COUNT (DISTINCT ftstamp),0), 
							Total_qtt			= CONVERT(INT,ISNULL(SUM(totqtt),0)),
							Total_value			= ISNULL(Round(SUM(etotal),2),0.0),
							TotalPeriod_ndoc	= SUM(Period) ,
							TotalPeriod_qtt		= CONVERT(INT,ISNULL(SUM(case when fdata BETWEEN ''' + @dateInit + '''  AND  ''' + @dateEnd + ''' then totqtt else 0 end),0)),
							TotalPeriod_value	= ISNULL(Round(SUM(case when fdata BETWEEN ''' + @dateInit + '''  AND  ''' + @dateEnd + ''' then etotal else 0 end),2),0.0)
					  FROM #dadosTotais
						'
print @sql
EXECUTE (@sql)







If OBJECT_ID('tempdb.dbo.#temp_documentos') IS NOT NULL
		drop table #temp_documentos ;

IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosCc'))
	DROP TABLE #dadosTotais

GO
GRANT EXECUTE on dbo.up_Facturacao_pesquisaGeralDocFacturacaoLinhas_totais TO PUBLIC
GRANT Control on dbo.up_Facturacao_pesquisaGeralDocFacturacaoLinhas_totais TO PUBLIC
GO