/* SP Pesquisa Detalhe Documentos de Facturação (RelatedDoc)
J.Gomes, 2020-05-07
--------------------------------

Listagem de documentos relacionados com o documento em questão.
Os documentos podem ser Origem ou Destino, conforme estejam na origem do documento 
em questão ou na criação de novos documentos a partir deste.

Pesquisa nas tabela: 	
fi, ft, bi, bo, re, rl

por 4 campos:
	@oFistamp, @bistamp, @Fistamp, @Ftstamp
	
Campos obrigatórios: pelo menos um stamp

OUTPUT:
---------
RelatedDoc_number,	RelatedDoc_site,	RelatedDoc_seriesNumber,	RelatedDoc_extCode
,	RelatedDoc_docType,	RelatedDoc_qttOrigDest,	RelatedDoc_relatedType


EXECUÇÃO DA SP:
---------------
exec up_Facturacao_pesquisaDetalheDocFacturacaoRelatedDoc @oFistamp, @bistamp, @Fistamp, @Ftstamp
	
	exec up_Facturacao_pesquisaDetalheDocFacturacaoRelatedDoc  
		'ADMBB1908EB-854F-43E0-8DD','','ADM9DC02C38-0C99-49D7-8E9','ADM02EBC9F1-19A5-4D9D-9F7'

	exec up_Facturacao_pesquisaDetalheDocFacturacaoRelatedDoc  
		'ADMBB1908EB-854F-43E0-8DD','','',''


ftstamp						fistamp						ofistamp
ADM02EBC9F1-19A5-4D9D-9F7	ADMCF1C55D3-02B2-4871-AB3	                         
ADM02EBC9F1-19A5-4D9D-9F7	ADMCB4FA994-AF24-4208-AFE	ADM6513DA16-1D36-43E9-9AD
ADM02EBC9F1-19A5-4D9D-9F7	ADM9DC02C38-0C99-49D7-8E9	ADMBB1908EB-854F-43E0-8DD



	*/
	
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO




IF OBJECT_ID('[dbo].[up_Facturacao_pesquisaDetalheDocFacturacaoRelatedDoc]') IS NOT NULL
		DROP PROCEDURE [dbo].[up_Facturacao_pesquisaDetalheDocFacturacaoRelatedDoc] ;
GO


CREATE PROCEDURE [dbo].[up_Facturacao_pesquisaDetalheDocFacturacaoRelatedDoc]	
		@oFistamp as varchar(25),
		@bistamp as varchar(25),		
		@Fistamp as varchar(25),
		@Ftstamp as varchar(25)

AS
SET NOCOUNT ON

SET @oFistamp	=   rtrim(ltrim(isnull(@oFistamp,'naoexiste')))
SET @bistamp	=   rtrim(ltrim(isnull(@bistamp,'')))
SET @Fistamp	=   rtrim(ltrim(isnull(@Fistamp,'')))
SET @Ftstamp	=   rtrim(ltrim(isnull(@Ftstamp,'')))

IF len(@oFistamp) > 0 or len(@bistamp) > 0 or len(@Fistamp) > 0 or len(@Ftstamp) > 0 
BEGIN	
	DECLARE @sql varchar(4000)

	select @sql = N' 	
		WITH 
		CTE1 AS (
			Select
				Convert(VARCHAR(100), [Fi].[fno]) AS RelatedDoc_number
				, ft.[Site] AS RelatedDoc_site
				, fi.ndoc As RelatedDoc_seriesNumber
				, '''' As RelatedDoc_extCode
				, ''TD'' AS RelatedDoc_docType
				,  dbo.unityConverter(fi.qtt ,''qttUnityToBox'', empresa.no ,Fi.ref) AS RelatedDoc_qttOrigDest
				, ''Orig'' AS RelatedDoc_relatedType
				, ft.ftano AS RelatedDoc_year


			From	
				Fi (nolock) 
				Inner Join Ft (nolock) On ft.ftstamp = fi.ftstamp
				LEFT JOIN empresa(nolock) ON empresa.site = ft.site
			Where	
				fistamp = ''' + @oFistamp + ''' 
		) '

	IF len(@Bistamp) > 15
	BEGIN
		select @sql = @sql +  N' 	
		,  CTE2 AS (
			Select
				 Convert(VARCHAR(100), [Bi].[obrano]) AS RelatedDoc_number
				, Bo.[Site] AS RelatedDoc_site
				, Bi.ndos As RelatedDoc_seriesNumber
				, '''' As RelatedDoc_extCode
				--BO fica TS
				, ''TS'' AS RelatedDoc_docType
				,  dbo.unityConverter(  Bi.qtt ,''qttUnityToBox'', empresa.no ,bi.ref)  AS RelatedDoc_qttOrigDest
				, ''Orig'' AS RelatedDoc_relatedType
				, bo.boano as RelatedDoc_year
			From	
				Bi (nolock) 	
				Inner Join Bo (nolock) On Bo.bostamp = Bi.bostamp
				LEFT JOIN empresa(nolock) ON empresa.site = bo.site
			Where	
				bistamp =  ''' + @Bistamp + ''' 
		) '
	END

	IF len(@Fistamp) > 15
	BEGIN
		select @sql = @sql + N'			
		, CTE3 AS (
			Select
				 Convert(VARCHAR(100), [Fi].[fno]) AS RelatedDoc_number
				, ft.[Site] AS RelatedDoc_site
				, fi.ndoc As RelatedDoc_seriesNumber
				, '''' As RelatedDoc_extCode
				--FT fica TD
				, ''TD'' AS RelatedDoc_docType
				, dbo.unityConverter(fi.qtt ,''qttUnityToBox'', empresa.no ,Fi.ref) AS RelatedDoc_qttOrigDest
				, ''Dest'' AS RelatedDoc_relatedType
				, ft.ftano as RelatedDoc_year

			From	
				Fi (nolock) 
				Inner Join Ft (nolock) On ft.ftstamp = fi.ftstamp
				LEFT JOIN empresa(nolock) ON empresa.site = ft.site
			Where	
				ofistamp = ''' + @Fistamp + ''' 
		) '
	END



	IF len(@Ftstamp) > 15
	BEGIN
		select @sql = @sql + N' 	
		, CTE4 as (	
			select DISTINCT 
			 Convert(VARCHAR(100), [Re].[rno]) AS RelatedDoc_number
			, Re.[Site] AS RelatedDoc_site
			, Re.ndoc As RelatedDoc_seriesNumber
			, '''' As RelatedDoc_extCode			
			, ''RE'' AS RelatedDoc_docType
			, 0 AS RelatedDoc_qttOrigDest
			, ''Dest'' AS RelatedDoc_relatedType
			, re.reano AS RelatedDoc_year

		from 
			rl (nolock)
			inner join fi (nolock) on fi.ftstamp=rl.ccstamp
			inner join re (nolock) on re.restamp=rl.restamp
			
		where 
			ftstamp = ''' + @Ftstamp + ''' 
		) '
	END

	select @sql = @sql + N' 	
		Select * From CTE1 '
		
	IF len(@Bistamp) > 15
	BEGIN
		select @sql = @sql + N' 
			UNION ALL
			Select * From CTE2 '
	END

	IF len(@Fistamp) > 15
	BEGIN
		select @sql = @sql + N'
			UNION ALL
			Select * From CTE3 '
	END

	IF len(@Ftstamp) > 15
	BEGIN
		select @sql = @sql + N'
			UNION ALL
			Select * From CTE4 '
	END
	   
	print @sql
	EXECUTE (@sql)
END
ELSE
	BEGIN
		print('Missing stamp')
	END

GO
GRANT EXECUTE on dbo.up_Facturacao_pesquisaDetalheDocFacturacaoRelatedDoc TO PUBLIC
GRANT Control on dbo.up_Facturacao_pesquisaDetalheDocFacturacaoRelatedDoc TO PUBLIC
GO