/* SP Pesquisa Documentos de Facturação apartir das linhas
D.Almeida, 2020-06-09
--------------------------------

Listagem de Documentos de Facturação baseada em diversos campos apartir das linhas.
e com possibilidade de filtragem acima de determinada data e hora de alteração.

Pesquisa nas tabelas: 	
Td, Ft, Ft2, Fi, b_utentes

por 16 campos:
	@number, @dep, @vatNr, @email, @ref, @design, @site, @numdoc, @seriesNumber
	, @prescriptionid, @suspenso, @lastChangeDate, @lastChangeTime
	, @dateInit, @dateEnd, @pageNumber 
	
Campos obrigatórios: nenhum



EXECUÇÃO DA SP:
---------------
exec up_Facturacao_pesquisaGeralDocFacturacaoLinhas '@number', '@dep', '@vatNr', '@email', '@ref', '@design'
, '@site', '@numdoc', '@seriesNumber', @prescriptionid, @suspenso
, '@lastChangeDate', '@lastChangeTime','@dateInit','@dateEnd' , @pageNumber, @orderStamp
	

	exec up_Facturacao_pesquisaGeralDocFacturacaoLinhas  '', NULL,NULL,NULL,NULL, NULL,'Loja 1',NULL,NULL,NULL,0,'2020-01-01',NULL,NULL,NULL,NULL,NULL
	exec up_Facturacao_pesquisaGeralDocFacturacaoLinhas  201, 0,NULL,NULL,NULL, NULL,'Atlantico',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
	exec up_Facturacao_pesquisaGeralDocFacturacaoLinhas  215, NULL,NULL,NULL,NULL, NULL,'Loja 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL


	EXEC up_Facturacao_pesquisaGeralDocFacturacaoLinhas 26011,0,NULL,NULL,NULL,NULL,N'Loja 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1
	*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_Facturacao_pesquisaGeralDocFacturacaoLinhas]') IS NOT NULL
	DROP PROCEDURE [dbo].up_Facturacao_pesquisaGeralDocFacturacaoLinhas ;
GO

CREATE PROCEDURE [dbo].up_Facturacao_pesquisaGeralDocFacturacaoLinhas	
	 @number			VARCHAR(10)
	,@dep				VARCHAR(3)
	,@vatNr				VARCHAR(20)
	,@email				VARCHAR(50)
	,@ref				VARCHAR(18)
	,@design			VARCHAR(60)
	,@site				VARCHAR(60)	
	,@numdoc			VARCHAR(20)
	,@seriesNumber	    VARCHAR(500)
	,@prescriptionid	VARCHAR(8)
	,@suspenso			VARCHAR(1)
	,@lastChangeDate	varchar(10) = '1900-01-01'
	,@lastChangeTime	varchar(8)  = '00:00:00'
	,@dateInit			varchar(10) = '1900-01-01'
	,@dateEnd	        varchar(10)  = '3000-01-01'
	,@pageNumber        int = 1
	,@orderStamp		VARCHAR(36) = ""
	,@toRegulariseOnly	bit = 0
	   
/* WITH ENCRYPTION */
AS



SET NOCOUNT ON

SET @ref				=   rtrim(ltrim(isnull(@ref,'')))
SET @number				=   rtrim(ltrim(isnull(@number,'')))
SET @dep				=   rtrim(ltrim(isnull(@dep,'')))
SET @vatNr				=   rtrim(ltrim(isnull(@vatNr,'')))
SET @design				=   rtrim(ltrim(isnull(@design,'')))
SET @prescriptionid		=   rtrim(ltrim(isnull(@prescriptionid,'')))
SET @suspenso			=   rtrim(ltrim(isnull(@suspenso,'')))
SET @numdoc				=   rtrim(ltrim(isnull(@numdoc,'')))
SET @seriesNumber		=   rtrim(ltrim(isnull(@seriesNumber,'')))
SET @site				=   rtrim(ltrim(isnull(@site,'')))
SET @email				=   rtrim(ltrim(isnull(@email,'')))
SET @lastChangeDate		=   rtrim(ltrim(isnull(@lastChangeDate,'1900-01-01')))
SET @lastChangeTime		=   rtrim(ltrim(isnull(@lastChangeTime,'00:00:00')))
SET @dateInit		    =   rtrim(ltrim(isnull(@dateInit,'1900-01-01')))
SET @dateEnd		    =   rtrim(ltrim(isnull(@dateEnd,'3000-01-01')))
set @orderStamp         =	rtrim(ltrim(ISNULL(@orderStamp,'')))
set @toRegulariseOnly   =	isnull(@toRegulariseOnly,0)



if(isnull(@pageNumber,0)<1)
	set @pageNumber = 1

If OBJECT_ID('tempdb.dbo.#temp_documentos') IS NOT NULL
		drop table #temp_documentos;		

DECLARE @sql varchar (MAX)= ''
DECLARE @sql1 varchar (MAX) = ''
DECLARE @sqlFrom varchar(MAX) = ''
DECLARE @sqlWhere varchar(MAX) = ''
DECLARE @orderBy VARCHAR(MAX) = ''
DECLARE @PageSize int = 100
DECLARE @OrderCri VARCHAR(MAX)


DECLARE @downloadURL VARCHAR(100)
SELECT  @downloadURL = rtrim(ltrim(textValue)) FROM B_Parameters(nolock) WHERE stamp='ADM0000000313'

DECLARE @id_lt varchar(100) =''	
DECLARE @site_nr INT
select @id_lt = rtrim(ltrim(id_lt)), @site_nr= no from empresa where site = @site

DECLARE @imagemURL varchar(300) 
set   @imagemURL = @downloadURL + @id_lt

IF @orderStamp = ''
BEGIN 
	SET @OrderCri = '
	 ORDER BY fdata DESC, usrhora DESC'
END 
ELSE 
BEGIN 
	SELECT @OrderCri='
	 ORDER BY ' + instructionSql  FROM sortingTableOrder (NOLOCK) WHERE stamp = @orderStamp and inativo=0 and nameService='invoicingSearchLines'
END 
set @orderBy = ' ) X ' + @OrderCri +'  
				 OFFSET ' + convert(varchar(10),@PageSize) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
				 FETCH NEXT  ' + convert(varchar(10),@PageSize) + ' ROWS ONLY '

select @sql = N' SELECT * INTO #temp_Documentos
				FROM (
					SELECT
						 [tabela] = ''Ft''
						, ndoc AS seriesNumber 				
						, disp_webservice_view 
					FROM
						[dbo].[Td] (nolock)	
					where
						disp_webservice_view = 1		 
				) as X 
	
				DECLARE @Documentos TABLE(
					seriesNumber int		
				)

				IF '''+@seriesNumber+''' <> ''''
					BEGIN
						INSERT INTO @Documentos
						SELECT * FROM dbo.up_splitToTable('''+@seriesNumber+''','','')
					END
				ELSE
				BEGIN
					INSERT INTO @Documentos
					SELECT seriesNumber FROM #temp_Documentos
				END				
			SELECT '  + convert(varchar(10),@PageSize) +' as pageSize,  (COUNT(*) OVER() / ' +  convert(varchar(10),@PageSize) +') + 1 as pageTotal, '
				+ convert(varchar(10),@PageNumber) +' as pageNumber, COUNT(*) OVER() as linesTotal		
			,* FROM (
				SELECT 
					  Convert(VARCHAR(100), [Ft].[fno]) AS IDInfo_number
					, ft.[Site] AS IDInfo_site
					, ft.ndoc As IDInfo_seriesNumber
					, ft.ftano AS  IDInfo_year
					, ft.no AS EntityInfo_number
					, ft.estab AS EntityInfo_dep					
					, UPPER(ft.nome) AS EntityInfo_name				
					, ''Cliente'' AS EntityInfo_type										
					
					,case when ([fi].[qtt]-[Regularizar].[qtt])>0  then convert(bit,0) else convert(bit,1)  end   AS FinanceInfo_alreadyDelivered
					, ft.memissao AS FinanceInfo_currency
					, 1 AS FinanceInfo_vatInc
					,''FinanceInfo_valuePaid''	= case when (select (edeb-ecred) from cc where cc.ftstamp=ft.ftstamp)<>0 then 
										(
										cast(case when ft.nmdoc=''Factura''
										then fi.etiliquido*(select round((edebf-ecredf)/(case when (edeb-ecred) = 0 then 1 else(edeb-ecred) end),2) from cc where cc.ftstamp=ft.ftstamp)
										else fi.etiliquido
										end as numeric(15,2)) 
										)
									else
										cast(case when ft.nmdoc=''Venda a Dinheiro'' or ft.nmdoc=''Fatura Recibo''
										then fi.etiliquido
										else 0.00
										end as numeric(15,2)) 
									end 
					, cast(fi.etiliquido as numeric(15,2)) AS FinanceInfo_retailPrice 
					, cast(fi.epv as numeric(15,2)) AS FinanceInfo_unitRetailPrice 
					, convert(varchar, ft.ousrdata, 23) AS OperationInfo_CreationDate
					, ft.ousrhora AS OperationInfo_CreationTime
					, convert(varchar, ft.usrdata, 23) AS OperationInfo_lastChangeDate
					, ft.usrinis AS OperationInfo_lastChangeUser 
					, ft.usrhora AS OperationInfo_lastChangeTime
					, convert(varchar, ft.fdata, 23) AS GeneralInfo_docDate				
					, ft.nmdoc AS DesignInfo_designation	
					, dbo.unityConverter(st.stock,''stocksUnityToBox'', ' + convert(varchar(10),isnull(@site_nr,0)) + ',fi.ref) AS GeneralInfo_StockQtt
					, fi.ref AS ProductInfo_ref
					, fi.design AS ProductInfo_design
					,ft.fdata
					,ft.usrhora
					,rtrim(ltrim(fi.posologia)) as GeneralInfo_dosage
					,dbo.unityConverter(fi.qtt,''stocksUnityToBox'', ' + convert(varchar(10),isnull(@site_nr,0)) + ',fi.ref) AS StockInfo_qtt
					,ltrim(rtrim(ISNULL(grande_mercado_hmr.descr,''''))) AS ProductInfo_HmrInfo_dicBigMarketHmrDescr
					,ISNULL(grande_mercado_hmr.id,0) AS ProductInfo_HmrInfo_dicBigMarketHmrId
					,ISNULL(categoria_hmr.id,0) AS ProductInfo_HmrInfo_dicCategoryHmrId
					,ltrim(rtrim(ISNULL(categoria_hmr.descr,''''))) AS ProductInfo_HmrInfo_dicCategoryHmrDescr
					,ISNULL(mercado_hmr.id,0) AS ProductInfo_HmrInfo_dicMarketHmrId
					,ltrim(rtrim(ISNULL(mercado_hmr.descr,''''))) AS ProductInfo_HmrInfo_dicMarketHmrDescr
					,ISNULL(segmento_hmr.id,0) AS ProductInfo_HmrInfo_dicSegmentHmrId
					,ltrim(rtrim(ISNULL(segmento_hmr.descr,''''))) AS ProductInfo_HmrInfo_dicSegmentHmrDescr
					,ltrim(rtrim(st.usr1)) AS ProductInfo_brand
					,rtrim(ltrim(isnull(st.designOnline,''''))) AS ProductInfo_OnlineDesignation
					,rtrim(ltrim(isnull(st.caractOnline,''''))) AS ProductInfo_OnlineDescription1
					,rtrim(ltrim(isnull(st.apresentOnline,''''))) AS ProductInfo_OnlineDescription2
					,ISNULL(fprod.generico,0) AS ProductInfo_generic
					,st.tempoIndiponibilidade	AS ProductInfo_TimeUnavailability 
					,''dia''	AS ProductInfo_UnavailabilityUnit '

set @sql1 = @sql1 +'
					,ISNULL((SELECT TOP 1 ISNULL('''+ @imagemURL +''' + ''/''+ anexosstamp,'''') FROM anexos (nolock) WHERE regstamp = st.ststamp  order by usrdata desc),'''') AS ProductInfo_image
					,u_ettent1 + u_ettent2 AS FinanceInfo_coPaymentValue
					,u_txcomp AS FinanceInfo_coPaymentRate 


					
					,FinanceInfo_docDescrAbrev = case  
					 when ft.nmdoc like ''%Reg%'' then ''CRED''  
					 when ft.nmdoc like ''%factura%'' then ''FT''
					 when ft.nmdoc like ''%venda%'' then ''FR''  
					 when ft.nmdoc like ''%credito%'' then ''CRED''  
					 when ft.nmdoc like ''%credito%'' then ''DEB''  
					 else  '''' end 
					,rtrim(ltrim(convert(varchar(max),isnull(ft2.obsdoc,'''')))) AS GeneralInfo_obs
					,rtrim(ltrim(convert(varchar(max),isnull(ft2.obsCl,'''')))) AS GeneralInfo_obsExternal
					,rtrim(ltrim(convert(varchar(max),isnull(ft2.obsInt,'''')))) AS GeneralInfo_obsInternal
				'
				

set @sqlFrom =' 
				FROM
       				[dbo].[fi] (nolock)	
					INNER JOIN [dbo].[ft] (nolock)		 ON [ft].[ftstamp]=[fi].[ftstamp] 
					LEFT JOIN [dbo].[st] (nolock)		 ON [st].[ref]=[fi].[ref]  and st.site_nr=''' + Convert(varchar(10),isnull(@site_nr,0)) +'''
					LEFT JOIN [dbo].[fprod] (nolock)	 ON [fprod].[cnp]=[fi].[ref] 
					LEFT JOIN [dbo].[ft2] (nolock)		 ON [ft].[ftstamp]=[ft2].[ft2stamp] 
					INNER JOIN @documentos				 ON [@documentos].[seriesNumber] = [ft].[ndoc]
					LEFT JOIN grande_mercado_hmr(nolock) ON st.u_depstamp = grande_mercado_hmr.id
					LEFT JOIN categoria_hmr(nolock)      ON st.u_secstamp = categoria_hmr.id
					LEFT JOIN mercado_hmr(nolock)        ON st.u_catstamp = mercado_hmr.id
					LEFT JOIN segmento_hmr(nolock)       ON st.u_segstamp = segmento_hmr.id
					CROSS APPLY (SELECT ISNULL(SUM(ofi.qtt),0) AS [qtt] FROM [dbo].[fi] as [ofi] (nolock) inner join td (nolock) as otd on ofi.ndoc = otd.ndoc where [fi].[fistamp] = [ofi].[ofistamp] AND [otd].[u_tipodoc] != 3 AND [otd].[nmdoc] not like (''%Segur%'') AND [otd].[nmdoc] not like (''%Import%'') AND [otd].[nmdoc] not like (''%Manual%'') ) AS [Regularizar]				
				WHERE
       				1 = 1 and fi.ref!='''' and fi.ref!=''R000001''
					AND  ft.nmdoc != ''Inserção de Receita''  and ft.site = ''' +  @site + '''
					'


if @dateInit != '' and @dateEnd  != ''
	select @sqlWhere = @sqlWhere + N' AND ft.fdata BETWEEN ''' + @dateInit + '''  AND  ''' + @dateEnd + ''''


if @lastChangeDate != ''
	select @sqlWhere = @sqlWhere + N' AND ft.usrdata + ft.usrhora >= ''' + @lastChangeDate + ' ' + @lastChangeTime + ''''

if @design != ''		
	select @sqlWhere = @sqlWhere + N' AND ([fi].[design] LIKE '''+@design+''' +''%'' )'

if @ref != ''		
	select @sqlWhere = @sqlWhere + N' AND ([fi].[ref] LIKE ''' + @ref+'''+''%'')'


if @toRegulariseOnly = 1	
begin	
	print @toRegulariseOnly
	set @sqlWhere = @sqlWhere +  N' AND fi.qtt > isnull((select sum(fi1.qtt) from fi(nolock) fi1 where fi1.ofistamp=fi.fistamp and len(fi1.ofistamp)>0 ),0) ' 
end	
	

if @vatNr != ''		
	select @sqlWhere = @sqlWhere + N' AND [ft].[ncont] = '''+ @vatNr +''' '
	
if @lastChangeDate != ''

	select @sqlWhere = @sqlWhere + N' AND convert(datetime,convert(varchar(10),ft.usrdata,120) + '' '' + ft.usrhora) >= ''' + @lastChangeDate + ' ' + @lastChangeTime + ''''

IF len(@email)>0 
BEGIN		
	declare @no_	int
	declare @estab_ int

	IF len(@number)>0 and len(@dep)>0
		select @no_ = no, @estab_ = estab from b_utentes where email = @email and no=@number and estab=@dep
	ELSE
		select top 1 @no_ = no, @estab_ = estab from b_utentes where email = @email			
		
	select @sqlWhere = @sqlWhere + ' AND [ft].[no]= ' + isnull(convert(varchar,@no_),0) + ' AND [ft].[estab] = ' + isnull(convert(varchar,@estab_),0)	
END

if @number != ''		
	select @sqlWhere = @sqlWhere + N' AND [ft].[no] = '+ @number +' '

if @dep != ''		
select @sqlWhere = @sqlWhere + N' AND [ft].[estab] = '+ @dep +' '

if @site != ''
	select @sqlWhere = @sqlWhere + N'	AND [ft].[site] = '''+@site+''' '

if @numdoc != ''		
	select @sqlWhere = @sqlWhere + N' AND convert(varchar,[ft].[fno]) = CASE WHEN '''+convert(varchar,@numdoc)+''' = '''' THEN convert(varchar,[ft].[fno]) 
																	ELSE '''+convert(varchar,@numdoc)+''' END'

if @prescriptionid != ''
	select @sqlWhere = @sqlWhere + N'	AND [ft2].[u_receita] = '''+@prescriptionid+''' '



if @suspenso != ''
	select @sqlWhere = @sqlWhere + N'	AND [ft].[cobrado] = '''+@suspenso+''' '

print @sql + @sqlFrom + @sqlWhere + @orderBy

EXECUTE (@sql + @sql1  +  @sqlFrom + @sqlWhere + @orderBy)


If OBJECT_ID('tempdb.dbo.#temp_documentos') IS NOT NULL
		drop table #temp_documentos ;

GO
GRANT EXECUTE on dbo.up_Facturacao_pesquisaGeralDocFacturacaoLinhas TO PUBLIC
GRANT Control on dbo.up_Facturacao_pesquisaGeralDocFacturacaoLinhas TO PUBLIC
GO