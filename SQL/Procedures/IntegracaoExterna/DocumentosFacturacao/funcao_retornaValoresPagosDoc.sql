
/****** 


Object:  SELECT * from   uf_retornaValoresPagosDoc('ADMA05D2B55-EC49-4B34-907','FT')    

-- Author:		<DA>
-- Create date: <15-12-2021>
-- Description:	<Retorna valores de divida e já pagos de um documento de debito ao credito>
-- =============================================

@stamp -> stamp do documento
@tabela -> tipo de tabela

select *from ft(nolock) where ftstamp = 'FRr8F9E9377-213B-4BB9-AA2'
******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[uf_retornaValoresPagosDoc]') IS NOT NULL
	DROP FUNCTION [dbo].[uf_retornaValoresPagosDoc];
GO





CREATE FUNCTION [dbo].[uf_retornaValoresPagosDoc] 
(	
	@stamp varchar(36),
	@tabela varchar(20)
)
RETURNS @tempCC  TABLE   
(
	valorPago   decimal(16,2),
	valorDivida decimal(16,2)
)
AS
begin
	
	if(@tabela='FT') begin
		insert into @tempCC 
		SELECT top 1  
			
			valorPago = case when cm in (1,2) then ROUND(edebf,2)  -- facturas e notas de debito
							 when cm in (57) then abs(ROUND(ecredf,2))*-1  --notas de credito
							 else null  
							 end,

			valorDivida = case  when cm in (1,2) then  ROUND(edeb,2)  - ROUND(edebf,2)   -- facturas e notas de debito
								when cm in (57) then  abs(ROUND(ecred,2) - ROUND(ecredf,2))*-1 --notas de credito
								else null 
							 end
		FROM CC(nolock) 
		where 
			ftstamp = @stamp 
			and origem=@tabela


		 
	end

	

	return
	
end
GO


