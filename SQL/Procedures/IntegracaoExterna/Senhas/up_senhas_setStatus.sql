
/*

Daniel Almeida - 20210208

Actualiza pedido por stamp

select * from ticket_request(nolock) where stamp='XXX'

exec up_senhas_setStatus


*/






SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_senhas_setStatus]') IS NOT NULL
	drop procedure dbo.up_senhas_setStatus
go



create procedure dbo.up_senhas_setStatus
  @stamp             as varchar(40)
 ,@status           int
 ,@ticketId         as varchar(200)
 ,@ticketDate       as varchar(200)
 ,@ticketType       as varchar(200)
 




/* WITH ENCRYPTION */
AS

	set @stamp          = rtrim(ltrim(isnull(@stamp,'')))
	set @status         = isnull(@status,0)
	set @ticketId       = rtrim(ltrim(isnull(@ticketId,'')))
	set @ticketDate     = rtrim(ltrim(isnull(@ticketDate,'')))
	set @ticketType     = rtrim(ltrim(isnull(@ticketType,'')))

	update 
		ticket_request 
	set
		status = @status,
		ticketId = @ticketId,
		ticketDate = @ticketDate,
		ticketType = @ticketType  
	where 
		stamp = @stamp
	

	declare @count int = 0
	select 
		@count = count(*) 
	from
		ticket_request(nolock)
	where 
		stamp=@stamp
	

	set @count = isnull(@count,0)

	
	If @count > 0 
		select getdate() AS DateMessag, 1 As StatMessag, 'Senha existe' As DescMessag
	ELSE	
		select getdate() AS DateMessag, 0 As StatMessag,  'Senha não Existe'   As DescMessag
								
 
GO
Grant Execute On dbo.up_senhas_setStatus to Public
Grant Control On dbo.up_senhas_setStatus to Public
GO
