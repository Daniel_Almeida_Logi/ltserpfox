
/*

Daniel Almeida - 20210208

Sp para guardar e limpar logs de pedidos de senhas

select * from ticket_request(nolock)

exec up_senhas_pedeNova


*/






SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_senhas_pedeNova]') IS NOT NULL
	drop procedure dbo.up_senhas_pedeNova
go



create procedure dbo.up_senhas_pedeNova

  @token            as varchar(40)
, @stamp            as varchar(40)

, @senderId         as varchar(40)
, @senderName       as varchar(250)
, @senderVat        as varchar(250)

, @receiverId       as varchar(40)
, @receiverName     as varchar(250)
, @receiverVat      as varchar(40)

, @entityId         as varchar(200)
, @entityName       as varchar(250)
, @entityApp        as varchar(40)

, @clientId         as varchar(200)
, @clientNumber     as int
, @clientEstab      as int
, @clientVat        as varchar(40)
, @clientSns        as varchar(40)
, @clientEmail      as varchar(200)
, @clientName       as varchar(200)
, @clientLastName   as varchar(200)
, @clientIp         as varchar(200)

, @ticketType       as varchar(200)
, @ticketTypeId     as varchar(200)

, @ticketId         as varchar(200)
, @ticketDate       as varchar(200)

, @site             as varchar(20)

, @reqType          as int

, @shouldDelete     as bit

, @timeMinus        as int

, @status       as int







/* WITH ENCRYPTION */
AS
	set @token          = rtrim(ltrim(isnull(@token,'')))

    set @stamp          = rtrim(ltrim(isnull(@stamp,'')))
	set @senderId       = rtrim(ltrim(isnull(@senderId,'')))
	set @senderName     = rtrim(ltrim(isnull(@senderName,'')))
	set @senderVat      = rtrim(ltrim(isnull(@senderVat,'')))

	set @receiverId     = rtrim(ltrim(isnull(@receiverId,'')))
	set @receiverName   = rtrim(ltrim(isnull(@receiverName,'')))
	set @receiverVat    = rtrim(ltrim(isnull(@receiverVat,'')))

	set @entityId       = rtrim(ltrim(isnull(@entityId,'')))
	set @entityName     = rtrim(ltrim(isnull(@entityName,'')))
	set @entityApp      = rtrim(ltrim(isnull(@entityApp,'')))

	set @clientId       = rtrim(ltrim(isnull(@clientId,'')))
	set @clientNumber   = isnull(@clientNumber,0)
	set @clientEstab    = isnull(@clientEstab,0)

	
	set @clientVat      = rtrim(ltrim(isnull(@clientVat,'')))
	set @clientSns      = rtrim(ltrim(isnull(@clientSns,'')))
	set @clientEmail    = rtrim(ltrim(isnull(@clientEmail,'')))
	set @clientName     = rtrim(ltrim(isnull(@clientName,'')))
	set @clientLastName = rtrim(ltrim(isnull(@clientLastName,'')))
	set @clientIp       = rtrim(ltrim(isnull(@clientIp,'')))

	set @ticketType     = rtrim(ltrim(isnull(@ticketType,'')))
	set @ticketTypeId   = rtrim(ltrim(isnull(@ticketTypeId,'')))
	set @ticketId     = rtrim(ltrim(isnull(@ticketId,'')))
	set @ticketDate     = rtrim(ltrim(isnull(@ticketDate,'')))

	set @site           = rtrim(ltrim(isnull(@site,'')))
	set @reqType        = isnull(@reqType,0)
	set @shouldDelete   = isnull(@shouldDelete,0)
	set @timeMinus      = isnull(@timeMinus,15)
	set @status         = isnull(@status,0)
	 


	set @timeMinus = @timeMinus * -1

    --limpa logs, mantem 30 dias
	if(@shouldDelete=1)
		delete from ticket_request where date<DATEADD(day,@timeMinus,getdate())

	INSERT INTO [dbo].[ticket_request]
           ([token]
           ,[stamp]
           ,[senderId]
           ,[senderName]
           ,[senderVat]
           ,[receiverId]
           ,[receiverName]
           ,[receiverVat]
           ,[entityId]
           ,[entityName]
           ,[entityApp]
           ,[clientId]
           ,[clientNumber]
           ,[clientDep]
           ,[clientVat]
           ,[clientSns]
           ,[clientEmail]
           ,[clientName]
           ,[clientLastName]
           ,[clientIp]
           ,[ticketType]
		   ,[ticketTypeId]
		   ,[ticketId]
		   ,[ticketDate]
           ,[site]
           ,[date]
           ,[reqType]
		   , [status]
		   
		   )
     VALUES
           (@token
           ,@stamp
           ,@senderId
           ,@senderName
           ,@senderVat
           ,@receiverId
           ,@receiverName
           ,@receiverVat
           ,@entityId
           ,@entityName
           ,@entityApp
           ,@clientId
           ,@clientNumber
           ,@clientEstab
           ,@clientVat
           ,@clientSns
           ,@clientEmail
           ,@clientName
           ,@clientLastName
           ,@clientIp
           ,@ticketType
		   ,@ticketTypeId
		   ,@ticketId
		   ,@ticketDate
           ,@site
           ,getdate()
           ,@reqType
		   ,@status
		   )

	declare @count int = 0


	select @count= count(*) from  [ticket_request](nolock) where stamp = @stamp
	set @count = isnull(@count,0)
	
	If @count > 0 
		select getdate() AS DateMessag, 1 As StatMessag, 'Senha Criada'        As DescMessag,  @site as site;	
	ELSE	
		select getdate() AS DateMessag, 0 As StatMessag, 'Senhas não criada'   As DescMessag,  @site as site;	

								
 
GO
Grant Execute On dbo.up_senhas_pedeNova to Public
Grant Control On dbo.up_senhas_pedeNova to Public
GO
