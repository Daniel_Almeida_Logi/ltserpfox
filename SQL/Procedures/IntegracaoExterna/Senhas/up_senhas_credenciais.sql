--exec up_senhas_credenciais 'Loja 1'

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_senhas_credenciais]') IS NOT NULL
	drop procedure dbo.up_senhas_credenciais 
go

create procedure dbo.up_senhas_credenciais 
@site varchar (30)

/* WITH ENCRYPTION */
AS

	DECLARE @hasTicket AS bit = 0
	SET @hasTicket = (select  isnull(has_ticket_integration,0) from empresa (nolock) where site = @site)	

	declare @username varchar(100) = ''
	declare @password varchar(100) = ''
	declare @local    varchar(100) = '1'


	if(@hasTicket=1)
	begin
		select @username=ltrim(rtrim(isnull(textValue,''))) from B_Parameters_site(nolock) where site = @site and stamp='ADM0000000044'
		select @password=ltrim(rtrim(isnull(textValue,''))) from B_Parameters_site(nolock) where site = @site and stamp='ADM0000000045'
		select @local=ltrim(rtrim(isnull(textValue,''))) from B_Parameters_site(nolock)    where site = @site and stamp='ADM0000000046'
	end

	select top 1 username=@username,password=@password, local = @local 


GO
Grant Execute On dbo.up_senhas_credenciais to Public
Grant Control On dbo.up_senhas_credenciais to Public
Go


