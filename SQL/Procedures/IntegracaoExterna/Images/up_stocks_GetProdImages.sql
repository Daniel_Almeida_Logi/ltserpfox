/* SP Pesquisa de imagens de Produtos por Referencia
2020-07-16, JG


Variaveis de Input:
--------------------
@ref		(facultativa- uma ref. ou n refs.)
@site_name	(obrigatória)
@allRef (traz todas as refs com apenas as refs com imagens)


Variaveis de Ouput:
--------------------
ref, OnlineImage, OnlineImage1, OnlineImage2, OnlineImage3, OnlineImage4
            

Execução da SP:
----------------
up_stocks_GetProdImages '@ref' , '@site_name', allref, pagenumber, orderstamp


	 exec up_stocks_GetProdImages '5440987' , 'loja 1',0,1,''
	 exec up_stocks_GetProdImages '5440987' , 'lojaa 1',0,1,''

	 exec up_stocks_GetProdImages '5440987' , null,1,''
	 exec up_stocks_GetProdImages '5440987' , null

	 exec up_stocks_GetProdImages '0161996' , 'loja 1'

	 exec up_stocks_GetProdImages '' , 'lojf 1',0
	 exec up_stocks_GetProdImages '' , 'lojf 1',1

	 exec up_stocks_GetProdImages null , 'loja 1',0
	 exec up_stocks_GetProdImages null , 'loja 1',1

	 exec up_stocks_GetProdImages '' , null
*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_GetProdImages]') IS NOT NULL
	drop procedure dbo.up_stocks_GetProdImages
go

CREATE PROCEDURE dbo.up_stocks_GetProdImages
	  @ref					VARCHAR(28)
	, @site_name			VARCHAR(18)
	, @allRef				bit = 0
	, @pageNumber			INT = 1
	, @orderStamp			VARCHAR(36) = ''

AS

SET @ref			=   rtrim(ltrim(isnull(@ref,'')))
SET @site_name		=   rtrim(ltrim(isnull(@site_name,'')))
SET @allRef			=   rtrim(ltrim(isnull(@allRef,0)))
SET @orderStamp     =	rtrim(ltrim(ISNULL(@orderStamp,'')))
 
if(isnull(@pageNumber,0)<1)
	set @pageNumber = 1	


DECLARE @sql varchar(max)
DECLARE @orderBy VARCHAR(8000)
DECLARE @PageSize int = 100
DECLARE @OrderCri VARCHAR(MAX)

IF @orderStamp = ''
BEGIN 
	SET @OrderCri = 'ORDER BY OnlineImage desc'
END 
ELSE 
BEGIN 
	SELECT @OrderCri='ORDER BY ' + instructionSql  FROM sortingTableOrder (NOLOCK) WHERE stamp = @orderStamp and inativo=0 and nameService='GetProdImages'
END


set @orderBy =   ' ) X ' + @OrderCri + '  				 
				 OFFSET ' + convert(varchar(10),@PageSize) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
				 FETCH NEXT  ' + convert(varchar(10),@PageSize) + ' ROWS ONLY '

	
IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosImagens'))
	DROP TABLE #dadosImagens

	
SET NOCOUNT ON		

-- variaveis auxiliares 		
DECLARE @siteno AS tinyint
declare @contador int
SElect @siteno = no from empresa(nolock) where site = @site_name	
--SET @siteno = isnull(@siteno,0)
select @contador=@@ROWCOUNT	

If @contador > 0 
BEGIN	

	DECLARE @id_lt varchar(100)	
	DECLARE @site_nr INT
	SELECT @id_lt = id_lt, @site_nr=no from empresa where site = @site_name
	SET @site_nr = isnull(@site_nr,0)
	SET @id_lt = isnull(@id_lt,'')
			
	DECLARE @downloadURL VARCHAR(100)
	SELECT @downloadURL = textValue FROM B_Parameters(nolock) WHERE stamp='ADM0000000313'
	   
	select @sql = N'	SELECT 
						--top 5 
							ref, anexos.usrdata as dd, row_number() over(partition by ref order by anexos.usrdata desc) as rowNumber ,
							''' + @downloadURL + @id_lt + '/' + '''+anexosstamp  as onlineImageURL
						INTO #dadosImagens
						FROM st(nolock)
						inner join anexos ON anexos.regstamp = st.ststamp	
						where 
							tabela = ''st'' 
							AND tipo = ''imagemProd''
							ANd site_nr = ' + convert(varchar,@site_nr) + ' 	
							AND ref = coalesce(nullif(''' + @ref + ''',''''),ref)  

					--select * FROM #dadosImagens
	   						
					SELECT '  + convert(varchar(10),@PageSize) +' as pageSize,  (COUNT(*) OVER() / ' +  convert(varchar(10),@PageSize) +') + 1 as pageTotal, '
							+ convert(varchar(10),@PageNumber) +' as pageNumber, COUNT(*) OVER() as linesTotal,*
					FROM (
					
						SELECT distinct(st.ref)	
							, ISNULL((SELECT ISNULL(onlineImageURL,'''') FROM #dadosImagens where rowNumber=1 and #dadosImagens.ref = st.ref),'''')		AS OnlineImage
							, ISNULL((SELECT ISNULL(onlineImageURL,'''') FROM #dadosImagens where rowNumber=2 and #dadosImagens.ref = st.ref),'''')		AS OnlineImage1
							, ISNULL((SELECT ISNULL(onlineImageURL,'''') FROM #dadosImagens where rowNumber=3 and #dadosImagens.ref = st.ref),'''')		AS OnlineImage2
							, ISNULL((SELECT ISNULL(onlineImageURL,'''') FROM #dadosImagens where rowNumber=4 and #dadosImagens.ref = st.ref),'''')		AS OnlineImage3
							, ISNULL((SELECT ISNULL(onlineImageURL,'''') FROM #dadosImagens where rowNumber=5 and #dadosImagens.ref = st.ref),'''')		AS OnlineImage4			
							--, ISNULL((SELECT ISNULL(dd,'''') FROM #dadosImagens where rowNumber=1 and #dadosImagens.ref = st.ref),'''')		AS dd1
							--, ISNULL((SELECT ISNULL(dd,'''') FROM #dadosImagens where rowNumber=2 and #dadosImagens.ref = st.ref),'''')		AS dd2
						FROM 
							st (nolock)
						'

	if @allRef = 1 
		select @sql = @sql + N'  left join #dadosImagens on #dadosImagens.ref = st.ref '
	else
		select @sql = @sql + N'  inner join #dadosImagens on #dadosImagens.ref = st.ref '

	select @sql = @sql + N'		where 
								st.site_nr = ' + convert(varchar,@siteno) + '
								AND st.ref = COALESCE(NULLIF(''' + @ref + ''',''''),st.ref) '
	

	select @sql = @sql + @orderBy
   
	--print @sql
	EXECUTE (@sql)
		

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosImagens'))
		DROP TABLE #dadosImagens

END
ELSE
BEGIN
	select '' pageSize, '' pageTotal,''	pageNumber,'' linesTotal, '' ref
	, '' OnlineImage,''	OnlineImage1,''	OnlineImage2,''	OnlineImage3,''	OnlineImage4	
	where 1=2 
END


GO
Grant Execute On dbo.up_stocks_GetProdImages to Public
Grant Control On dbo.up_stocks_GetProdImages to Public
GO

