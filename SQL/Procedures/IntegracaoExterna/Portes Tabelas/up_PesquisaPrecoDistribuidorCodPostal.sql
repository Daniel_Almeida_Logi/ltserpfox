
/* SP Pesquisa Preco por Cod. Postal e Distribuidor
J.Gomes, 2020-08-03
--------------------------------



select *from CodigoPostal_Distribuidor



Pesquisa nas tabela:     
CodigoPostal
CodigoPostal_Distribuidor



select *from CodigoPostal where stampCp='98015FEF-594C-4F49-84'
select *from CodigoPostal_Distribuidor where stampCp='98015FEF-594C-4F49-84'



por 2 campo:
    '@cod_postal','@id_distribuidor'
    
Campos obrigatórios: todos


OUTPUT:
---------
preco



EXECUÇÃO DA SP:
---------------
exec up_PesquisaPrecoDistribuidorCodPostal '@cod_postal','@id_distribuidor',''


    exec up_PesquisaPrecoDistribuidorCodPostal  null,null,'Loja 1',null, null, null

    exec up_PesquisaPrecoDistribuidorCodPostal  '2350-754',null,'Loja 3','5440987', 24.95,'PT'

    EXEC up_PesquisaPrecoDistribuidorCodPostal N'3750-801',1,N'Loja 1',N'6064915', null, 'ES'


    exec up_PesquisaPrecoDistribuidorCodPostal  '','1'
    EXEC up_PesquisaPrecoDistribuidorCodPostal N'SW1V 3HQ',1,N'Loja 1',N'1100247,1100247', null,'GB',0
    exec up_PesquisaPrecoDistribuidorCodPostal  '','',''


	update st set portesGratis=0 where ref='6006429'

	EXEC up_PesquisaPrecoDistribuidorCodPostal N'2200-126',1,N'Loja 1',N'5440987,1100247',20,null,0


	EXEC up_PesquisaPrecoDistribuidorCodPostal N'4600-433',NULL,N'Loja 1',N'4283883',3.3300000000000001,NULL,NULL
		EXEC up_PesquisaPrecoDistribuidorCodPostal N'4100-341',NULL,N'Loja 1',N'6021980',11,NULL,null
	
    */



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



IF OBJECT_ID('[dbo].[up_PesquisaPrecoDistribuidorCodPostal]') IS NOT NULL
        DROP PROCEDURE [dbo].[up_PesquisaPrecoDistribuidorCodPostal] ;
GO



CREATE PROCEDURE [dbo].[up_PesquisaPrecoDistribuidorCodPostal]    
         @cod_postal        VARCHAR(15),
         @id_distribuidor   VARCHAR(10),
         @site              VARCHAR(max)='',
         @lista_refs        VARCHAR(max)='',
         @preco_max         DECIMAL(19,6) = 0.0,
		 @country_code      VARCHAR(10) = 'PT',
		 @order_type        int = 0
        
    /* WITH ENCRYPTION */
AS


SET NOCOUNT ON


SET @lista_refs           =   rtrim(ltrim(isnull(@lista_refs,'')))
SET @cod_postal           =   REPLACE(rtrim(ltrim(isnull(@cod_postal,''))), ' ', '');
SET @id_distribuidor      =   rtrim(ltrim(isnull(@id_distribuidor,'')))
SET @site                 =   rtrim(ltrim(isnull(@site,'')))
SET @preco_max            =   isnull(@preco_max,0.0)
SET @country_code         =   isnull(rtrim(ltrim(@country_code)),'PT')
SET @order_type           =   isnull(@order_type,0)

declare @site_nr int = -1
declare @pagaPortes   int = -1




    select TOP 1
         @site_nr = isnull(no,-1)
    from 
        empresa(nolock)
    where 
       site=ltrim(@site)



--split da string codpostal em dois campos numericos
declare @cod4 varchar(4)
declare @cod3 varchar(4) 

if(@country_code='PT')
begin   
	set @cod4 = (Select top 1 * from dbo.up_splitToTable(@cod_postal,'-') order by  convert(int,items) desc)
	set @cod3 = (Select top 1 * from dbo.up_splitToTable(@cod_postal,'-') order by convert(int,items)  asc)
end
else begin
	set @cod4 = 0
	set @cod3 = 0
end


DECLARE @moeda AS varchar(10)
SET @moeda = (Select ltrim(rtrim(isnull(textValue,''))) from B_Parameters(nolock) where stamp='ADM0000000260')



DECLARE @sql varchar(MAX) = ''
DECLARE @sqlPortes varchar(MAX) = ''
DECLARE @vatPerc decimal(16,2) = 0.00
DECLARE @refPortes varchar(18) = 'SERVPORTES'

declare @tipoMedicamento int = 0

	if(isnull(@order_type,0)=0)
	begin
		select 
			@tipoMedicamento = count(familia)
		from 
			st(nolock)
		inner join 
			 dbo.up_splitToTable(@lista_refs,',')  on st.ref = items
		and 
			isnull(u_depstamp,'') in ('1','2')
	end

--valida se alguma ref tem portes
    If OBJECT_ID('tempdb.dbo.#temp_logistica') IS NOT NULL
        drop table #temp_logistica ;

	If OBJECT_ID('tempdb.dbo.#temp_logisticaFinal') IS NOT NULL
        drop table #temp_logisticaFinal;



	--se order_type = 1 -> encomenda com receita medica
	if(isnull(@order_type,0)=1 or @tipoMedicamento>0)
	begin
		declare @count int = 0
		select 
			@count = count(*) 
		from 
			CodigoPostal(nolock)
		where 
			CodigoPostal.cp3=@cod3 and CodigoPostal.cp4=@cod4
			and limitrofes_farma = 1
			and limitrofes_site like '%'+rtrim(ltrim(str(isnull(@site_nr,1))))+'%'
	


		if(@count=0 or @country_code!='PT')
		begin
			 select 
                    -2.00 as preco,
                    0 as id , 
                    '' as design,
                    '' as  obs,
                    '' as currency,
                    0    as vatInc,
                    0.00 as vatPerc

			return
		end
	end
     
    select 
        @pagaPortes = case when @site_nr=-1 then -1 else  count(*)  end
    from st(nolock) 
    where 
        ref in  (Select * from dbo.up_splitToTable(@lista_refs,','))
            and inactivo = 0 
            and st.site_nr = @site_nr
            and portesGratis=0


    select 
        @vatPerc = round(isnull(taxasiva.taxa,0.00),2)
     from 
        st(nolock) 
    inner join 
        taxasiva(nolock) on taxasiva.codigo = st.tabiva
     where 
        ref=@refPortes
        and site_nr = @site_nr


    
if(@pagaPortes>=0)


begin




--IF try_parse(@cod4 AS int) is not NULL and try_parse(@cod3 As int) is not NULL and len(@id_distribuidor) > 0 
    select @sql = N'             
                    select distinct preco = case when ' + convert(varchar(10),@pagaPortes)  + ' = 0 OR ' + convert(varchar(20),@preco_max) + ' >= B.preco_max then convert(decimal(16,2),round(B.precoAposPrecoMax,2)) else convert(decimal(16,2),round(B.preco,2)) end, 
                           isnull(B.iddistribuidor,0) as id, 
                           rtrim(ltrim(isnull(B.distribuidor,''''))) as design, 
                           rtrim(ltrim(isnull(B.obs,''''))) as obs,
                           '''+@moeda+''' as currency,
                           ''1'' as vatInc,  
                           '''+convert(varchar(10),@vatPerc)+''' as vatPerc


                    into #temp_logistica                    
                    from CodigoPostal(nolock) A
                    
                    inner join CodigoPostal_Distribuidor B(nolock)
                    on A.stampCP = B.stampCP                                
                    WHERE
                     1=1     
                    '

	if(isnull(@country_code,'PT')='PT')
	begin 
		if(len(@cod4)>0)

			set @sql = @sql + N' and cp4 = ' + isnull(@cod4,'''''') + ''


	--	if(len(@cod3)>0)
			--set @sql =  @sql+ N' and cp3 = ' +  isnull(@cod3,'''''') + '' 
	end

    if(len(@id_distribuidor)>0)
        set @sql = @sql +  N' and iddistribuidor = ' + @id_distribuidor + '' 


        
    set @sql = @sql +  N' and preco_activo = 1 and activo = 1 and pais = ''' + rtrim(ltrim(isnull(@country_code,'PT'))) + ''''
    
   

    set @sql = @sql + N'
        if(select  count(*) from #temp_logistica) > 0
            BEGIN
                 select 
                    *,
					ROW_NUMBER() OVER (PARTITION BY  isnull(id,0) ORDER BY  isnull(id,0) DESC) row_num
				into #temp_logisticaFinal
                from 
                    #temp_logistica 


				select * from #temp_logisticaFinal where row_num = 1

            END         
        ELSE
            BEGIN
                select 
                    -1.00 as preco,
                    0 as id , 
                    '''' as design,
                    '''' as  obs,
                    '''' as currency,
                    0    as vatInc,
                    0.00 as vatPerc
    
            END


    '



    set @sql = @sql + N'        


    If OBJECT_ID(''tempdb.dbo.#temp_logistica'') IS NOT NULL
        drop table #temp_logistica ;

		
    If OBJECT_ID(''tempdb.dbo.#temp_logisticaFinal'') IS NOT NULL
        drop table #temp_logisticaFinal;


    '



print @sql
EXECUTE (@sql)


end




GO
GRANT EXECUTE on dbo.up_PesquisaPrecoDistribuidorCodPostal TO PUBLIC
GRANT Control on dbo.up_PesquisaPrecoDistribuidorCodPostal TO PUBLIC
GO


