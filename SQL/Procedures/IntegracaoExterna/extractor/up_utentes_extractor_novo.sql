/*
Insere ou actualiza utentes via extrator
D.Almeida, 23-09-2021
revisão 23-09-2021

*/



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_extractor_novo]') IS NOT NULL
    drop procedure up_utentes_extractor_novo
go

CREATE PROCEDURE up_utentes_extractor_novo	
	

	 @stamp				varchar(36),		
	 @token				varchar(36),		
	 @comData			varchar(20),
	 @id_ext_empresa	varchar(10),	
	 @nome_ext_empresa	varchar(100),
	 @nif_ext_empresa	varchar(15), 
	 @cp_ext_empresa	varchar(50),
	 @app_ext_id		varchar(10),
	 @app_ext_app		varchar(30),
	 @app_ext_nome		varchar(30),
	 @id                varchar(50),
	 @clienteId         varchar(50),
	 @nome				varchar(254),
	 @nif				varchar(20),
	 @genero			varchar(1),
	 @dataNascimento	varchar(20),
	 @compartEntidade	varchar(254),
	 @telefone	        varchar(20),
	 @telemovel			varchar(20),
	 @email				varchar(100),
	 @snsNr				varchar(20),
	 @benefNr			varchar(20),
	 @status			bit,
	 @comStamp			varchar(36)



	
AS

	if(select count(*) from  clientes(nolock) where id_ext_empresa=@id_ext_empresa and  nif_ext_empresa=@nif_ext_empresa and clienteId = @clienteId)>0
	begin
		UPDATE 
			[dbo].[clientes]
		SET [stamp]             = @stamp
			,[token]            = @token
			,[comData]          = @comData
			,[id_ext_empresa]   = @id_ext_empresa
			,[nome_ext_empresa] = @nome_ext_empresa
			,[nif_ext_empresa]  = @nif_ext_empresa
			,[cp_ext_empresa]   = @cp_ext_empresa
			,[app_ext_id]       = @app_ext_id
			,[app_ext_app]      = @app_ext_app
			,[app_ext_nome]     = @app_ext_nome
			,[id]               = @id
			,[oData]            = getdate()
			,[clienteId]        = @clienteId
			,[nome]             = @nome
			,[nif]              = @nif
			,[genero]           = @genero
			,[dataNascimento]   = @dataNascimento
			,[compartEntidade]  = @compartEntidade
			,[telefone]         = @telefone
			,[telemovel]        = @telemovel
			,[email]            = @email
			,[snsNr]            = @snsNr
			,[benefNr]          = @benefNr
			,[status]           = @status
			,[comStamp]         = @comStamp
		 WHERE
			id_ext_empresa=@id_ext_empresa 
			and nif_ext_empresa=@nif_ext_empresa 
			and clienteId = @clienteId
	end
	else begin

		INSERT INTO [dbo].[clientes]
           ([stamp]
           ,[token]
           ,[comData]
           ,[id_ext_empresa]
           ,[nome_ext_empresa]
           ,[nif_ext_empresa]
           ,[cp_ext_empresa]
           ,[app_ext_id]
           ,[app_ext_app]
           ,[app_ext_nome]
           ,[id]
           ,[clienteId]
           ,[nome]
           ,[nif]
           ,[genero]
           ,[dataNascimento]
           ,[compartEntidade]
           ,[telefone]
           ,[telemovel]
           ,[email]
           ,[snsNr]
           ,[benefNr]
           ,[status]
		   ,[comStamp]  
		   
		   )
     VALUES
           (
		    @stamp
           ,@token
           ,@comData
           ,@id_ext_empresa
           ,@nome_ext_empresa
           ,@nif_ext_empresa
           ,@cp_ext_empresa
           ,@app_ext_id
           ,@app_ext_app
           ,@app_ext_nome
           ,@id
           ,@clienteId
           ,@nome
           ,@nif
           ,@genero
           ,@dataNascimento
           ,@compartEntidade
           ,@telefone
           ,@telemovel
           ,@email
           ,@snsNr
           ,@benefNr
           ,@status
		   ,@comStamp
		   )
	end


            
GO
Grant Execute On up_utentes_extractor_novo to Public
Grant Control On up_utentes_extractor_novo to Public
GO
			
           
			
	