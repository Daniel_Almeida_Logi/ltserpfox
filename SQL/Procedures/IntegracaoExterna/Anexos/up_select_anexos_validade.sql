/* SP inserir anexo
	
	exec up_select_anexos_validade '321654' , 'CC'
	select * from anexos where regstamp='321654'
	update anexos set validade=getdate()+30 where regstamp='321654'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_select_anexos_validade]') IS NOT NULL
	DROP PROCEDURE dbo.up_select_anexos_validade
GO

CREATE PROCEDURE dbo.up_select_anexos_validade
	 @regstamp			varchar(60) = ''
	,@tipo				varchar(25) = ''

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON
	declare @tipoAnexo varchar(254)

	select
		 @tipoAnexo = description
	from 
		regrasCartaoApiSeguradoras (nolock)
	where
		regrasCartaoApiSeguradoras.Code = @tipo

	SELECT TOP 1 
		anexos.anexosstamp,
		anexos.regstamp,
		anexos.tabela,		
		anexos.filename,
		anexos.descr,
		anexos.keyword,
		anexos.protected, 
		anexos.tipo,
		anexos.path,
		anexos.validade,
		estadoProcessoAPISeguradoras.ftstamp
	FROM  
		estadoProcessoAPISeguradoras (nolock) 
	inner join
		anexos (nolock)	on anexos.anexosstamp = estadoProcessoAPISeguradoras.stampAnexos 
	WHERE 
		anexos.tipo = @tipoAnexo
		AND anexos.validade > Getdate()
		AND anexos.regstamp = @regstamp
	ORDER  BY anexos.validade desc
	
GO
GRANT EXECUTE on dbo.up_select_anexos_validade TO PUBLIC
GRANT Control on dbo.up_select_anexos_validade TO PUBLIC
GO