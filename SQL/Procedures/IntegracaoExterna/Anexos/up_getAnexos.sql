/* SP get path file  
Daniel Almeida
--------------------------------

EXECUÇÃO DA SP:
---------------
exec up_getAnexos  'empresa','1','Loja 1'
	
	exec up_getAnexos 'fl','ADMFD4F1FF1-8BAF-4D7F-B48',''



*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


IF OBJECT_ID('[dbo].[up_getAnexos]') IS NOT NULL
		DROP PROCEDURE [dbo].up_getAnexos ;
GO



CREATE PROCEDURE [dbo].up_getAnexos
		 @table				VARCHAR(20) = '',
		 @regstamp			VARCHAR(60) = '',
		 @siteId			VARCHAR(60) = ''

		 		 
	/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	DECLARE @sql		 varchar(max) = ''
	DECLARE @downloadURL VARCHAR(100) = ''

	SELECT @downloadURL = rtrim(ltrim(textValue)) FROM B_Parameters(nolock) WHERE stamp='ADM0000000313'

	IF ISNULL(@siteId,'') = ''
		set @siteId = ''
	IF ISNULL(@table,'') = ''
		set @table = ''
	IF ISNULL(@regstamp,'') = ''
		set @regstamp = ''

	set @downloadURL = @downloadURL + @siteId + '/'

	set @sql ='
			  select
				anexosstamp																		AS AttachmentInfo_stamp
				,tabela																			AS AttachmentInfo_table
				,filename																		AS AttachmentInfo_fileName
				,case when len(filename) > 0 then
					reverse(left(reverse(filename),CHARINDEX(''.'',reverse(filename))-1))		   
					else '''' end																AS AttachmentInfo_extension  
				,keyword																		AS AttachmentInfo_keyword
				,CONCAT('''+@downloadURL+''', anexosstamp)										AS AttachmentInfo_url
				,descr																			AS AttachmentInfo_description
				,convert(varchar, anexos.ousrdata, 23)											AS OperationInfo_CreationDate 
				,left(CONVERT(time, anexos.ousrdata),8)											AS OperationInfo_CreationTime
				,convert(varchar, anexos.usrdata, 23)											AS OperationInfo_LastChangeDate 
				,left(CONVERT(time, anexos.usrdata),8)											AS OperationInfo_LastChangeTime
				,anexos.usrinis																	AS OperationInfo_lastChangeUser
				,nome 																			AS OperationInfo_LastChangeUserName
			 from 
				anexos(nolock)
				LEFT JOIN b_us(nolock) on anexos.usrinis=iniciais
			 where 1=1 
			  '
	IF @table != ''
		set @sql = @sql + ' and tabela = '''+ @table +''''
	IF @regstamp != ''
		set @sql = @sql + ' and regstamp = ''' + @regstamp + ''''


	print @sql
	EXECUTE (@sql)




GO
GRANT EXECUTE on dbo.up_getAnexos TO PUBLIC
GRANT Control on dbo.up_getAnexos TO PUBLIC
GO

