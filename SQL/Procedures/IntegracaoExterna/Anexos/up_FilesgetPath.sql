/* SP get path file  
Jose Simoes,2022-08-01
--------------------------------

EXECU��O DA SP:
---------------
exec up_FilesgetPath  'PostOffice','teste','Loja 1'
	
	
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


IF OBJECT_ID('[dbo].[up_FilesgetPath]') IS NOT NULL
		DROP PROCEDURE [dbo].up_FilesgetPath ;
GO



CREATE PROCEDURE [dbo].up_FilesgetPath
		 @table				VARCHAR(20),
		 @fileName			VARCHAR(30),
		 @site				varchar(50)


		 		 
	/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	DECLARE @id_lt varchar(100)


	SELECT @id_lt=id_lt FROM empresa WHERE site = @site

	SELECT  textValue  + '\ArquivoDigital' + '\' + @id_lt + '\' + @table as path from B_Parameters_site(nolock) WHERE stamp='ADM0000000161'  and site = @site



GO
GRANT EXECUTE on dbo.up_FilesgetPath TO PUBLIC
GRANT Control on dbo.up_FilesgetPath TO PUBLIC
GO

