/* SP inserir anexo
	
	exec up_select_anexos_utente 201 , 0
	select * from anexos where regstamp='321654'
	update anexos set validade=getdate()+30 where regstamp='321654'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_select_anexos_utente]') IS NOT NULL
	DROP PROCEDURE dbo.up_select_anexos_utente
GO

CREATE PROCEDURE dbo.up_select_anexos_utente
	 @no				numeric(20,0)
	,@estab				numeric(20,0)


/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	SELECT utstamp,
       nome,
       no,
       estab,
       ncont,
       morada,
       telefone,
       tlmvl,
       bino,
       autorizado
FROM   b_utentes (nolock)
WHERE  no = @no
       AND estab = @estab
	
GO
GRANT EXECUTE on dbo.up_select_anexos_utente TO PUBLIC
GRANT Control on dbo.up_select_anexos_utente TO PUBLIC
GO