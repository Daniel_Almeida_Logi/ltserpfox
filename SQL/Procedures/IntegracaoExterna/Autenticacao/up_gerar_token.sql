/*
	. BD			: msb-auth
	. Tabela		: token / service_user
	. Criado		: DA - 2020.06.18
	. Alterado		: DA - 2020.06.18
	. Valida se o utilizador / empresa tem acesso ao servi�o. 
	. Valida se o token est� valido. 

	 exec up_authRefreshToken 'F0D03442E23D406AA1EF3BFC85EC12E5A70380C641204D42A561DDA8B4776150', 'erp' -- valida servi�o

	 resultado
	 result > 0 - valido
	 result = 0 - sem premissoes para o servi�o
	 result = -1 - token invalido

	 . testes
	 select *from [b_utentes] where no=215 and estab=0

	 update [user] set inactivo = 0 where stamp='89FD0D60-6DFB-4788-85A9-4AD826' 
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_gerar_token]') IS NOT NULL
	drop procedure dbo.up_gerar_token
go

create procedure dbo.up_gerar_token
	@utstamp varchar(255),
	@tokenDurationDays int = 7
/* WITH ENCRYPTION */ 
AS 
BEGIN 
	set @utstamp = LTRIM(RTRIM(@utstamp)) 
	if(len(@utstamp)>0)
	begin	
		declare @token varchar(100) = ''
		declare @oAuthExpDate datetime = getdate() 
		declare @userType int = 1
		declare @number int = 0
		declare @dep int = 0

		set @token = replace(newid(), '-', '')
		set @token = @token + replace(newid(), '-', '')
		set @token = rtrim(ltrim(@token))
	
		set @oAuthExpDate = DATEADD(day, @tokenDurationDays, getdate())
		
		INSERT INTO [dbo].[token]
				   ([stamp]
				   ,[oAuthToken]
				   ,[clStamp]
				   ,[oAuthExpDate]	
				   ,[ousrdate]
				   ,[usrdate]
				   )		
			 VALUES
				   (left(newid(),30)
				   ,@token
				   ,@utstamp
				   ,@oAuthExpDate
				   ,getdate()
				   ,convert(varchar, GETDATE(), 120)
				   )
		select
			@number = no,
			@dep = estab
		from 
			b_utentes (nolock)
		where 
			utstamp = @utstamp

		select 1 as result, oAuthToken = @token, oAuthExpDate =  CONVERT(varchar,@oAuthExpDate,21), number = @number, dep = @dep, companyId = NULL, userType = @userType
	end	
	ELSE
		select -1 as result
END 

GO
	Grant Execute on dbo.up_gerar_token to Public
	Grant Control on dbo.up_gerar_token to Public
GO




