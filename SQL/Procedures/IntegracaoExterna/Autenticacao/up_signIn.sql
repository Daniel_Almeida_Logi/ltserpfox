/*
	. BD			: msb-auth
	. Tabela		: b_utentes
	. Criado		: DA - 2020.06.18
	. Alterado		: DA - 2020.06.18

	. Valida se o utilizador / empresa existe no router. 
	  Se sim devolve um token vᬩdo durante um determinado periodo de tempo.
	. Nota valida, nif ou  username ou email e password

	 exec up_signIn 'C215.0', 'pwd123' -- valida username
	 exec up_signIn '123456780', 'pwd123'  -- valida nif
	 exec up_signIn 'filiparesende@logitools.pt', 'pwd123'  -- valida email

	 exec up_signIn '', ''  -- vazio
	 exec up_signIn NULL, NULL  -- vazio
	 . testes
	 select *from [b_utentes] where no = 215 and estab=0
	 select *from token
		where [oAuthExpDate] >=getdate() 
		order by ousrdate desc
	 delete from token where  [oAuthExpDate] < getdate() 
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_signIn]') IS NOT NULL
	drop procedure dbo.up_signIn
go

create procedure dbo.up_signIn
	@user varchar(255),
	@pass varchar(255)
	
/* WITH ENCRYPTION */ 
AS 
BEGIN 
	declare @utStamp varchar(255) = ''
	declare @companyStamp varchar(255) = ''
	declare @token varchar(100) = ''
	declare @tokenDurationDays int = 1
	declare @oAuthExpDate datetime = getdate()

	declare @number int = 0
	declare @dep int = 0
	declare @companyId varchar(100) = ''
	declare @userType int = 1

	/* valida user*/
	set @user = ltrim(rtrim(@user))
	set @pass = ltrim(rtrim(@pass))

	/* limpa tokens antigos */
	delete from token where  [oAuthExpDate] < DATEADD(day, -2, getdate()) 
	
	select 
		top 1
	    @utStamp= rtrim(ltrim(ut.utstamp)),
		@dep = isnull(estab,0),
		@number = isnull(no,0)
	from 
		b_utentes as ut (nolock)
	where
		(ut.username =  @user or ut.email = @user or ut.ncont = @user)
		and  ut.password = @pass 
		and ut.inactivo = 0 
		and ut.password!='' 
	order by 
		ut.ousrdata
	desc
	
	/* Utilizador/company existe, cria e devolve token */ 
	exec up_gerar_token @utstamp
END
GO
	Grant Execute on dbo.up_signIn to Public
	Grant Control on dbo.up_signIn to Public
GO
