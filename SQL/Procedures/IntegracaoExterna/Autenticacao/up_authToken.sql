

/*

	. BD			: msb-auth
	. Tabela		: token / service_user

	. Criado		: DA - 2020.06.18
	. Alterado		: DA - 2020.06.18

	. Valida se o utilizador / empresa tem acesso ao serviço. 
	. Valida se o token está valido. 


	  

	 exec up_authToken '2762577F957747E693CE011B4073673034D37023C27F4264818439667CD3259D', 'erp' -- valida serviço

	 resultado
	 result > 0 - valido
	 result = 0 - sem premissoes para o serviçocc
	 result = -1 - token invalido

	 . testes
	 select *from [b_utentes] where no=215 and estab=0


	 update [user] set inactivo = 0 where stamp='89FD0D60-6DFB-4788-85A9-4AD826' 


*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_authToken]') IS NOT NULL
	drop procedure dbo.up_authToken
go

create procedure dbo.up_authToken
	@token varchar(255),
	@service varchar(255)
	
/* WITH ENCRYPTION */ 
AS 
BEGIN 
	set @token = ltrim(rtrim(@token))
	set @service = ltrim(rtrim(@service))

	declare @utStamp varchar(255) = ''
	declare @serviceStamp varchar(255) = ''
	declare @result int = -1

	 /* retorna stamp do serviço */
	select 
		top 1 
		@serviceStamp = ltrim(rtrim(s.stamp))
	from
		[service] s (nolock)
	where
		ltrim(rtrim(s.descr)) = @service
		
	select 
		top 1
	    @utStamp= rtrim(ltrim(isnull(t.clStamp,'')))
	from 
		[token] as t (nolock)
	where
		ltrim(rtrim(t.oAuthToken)) = @token
		and t.oAuthExpDate >= getdate()
	order by 
		t.ousrdate
	desc

	/*se existe, valida empresa*/
	if(len(@utStamp)>0)
	begin
		select 
			@result = count(*)
		from 
			service_user as su (nolock)
		left join
			[b_utentes] ut(nolock) on ltrim(rtrim(su.clStamp)) = ltrim(rtrim(ut.utstamp))
		where
			rtrim(ltrim(su.clStamp)) = @utStamp
			and rtrim(ltrim(serviceStamp)) = @serviceStamp	
			and ut.inactivo = 0	and autorizasite = 1
	end
	
	select result = @result
END

GO
	Grant Execute on dbo.up_authToken to Public
	Grant Control on dbo.up_authToken to Public
GO




