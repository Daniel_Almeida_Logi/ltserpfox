/*
	. BD			: msb-auth
	. Tabela		: token / service_user
	. Criado		: DA - 2020.06.18
	. Alterado		: DA - 2020.06.18
	. Valida se o utilizador / empresa tem acesso ao servi�o. 
	. Valida se o token est� valido. 

	 exec up_token_info '0456229CFD3945F4816659DAC210F21FF0C3F7E06CC642A48FAB9E3BFD97C5F4', 'erp'

	 resultado
	 result > 0 - valido
	 result = 0 - sem premissoes para o servi�o
	 result = -1 - token invalido

	 . testes
	 select *from [b_utentes] where no=215 and estab=0

	 update [user] set inactivo = 0 where stamp='89FD0D60-6DFB-4788-85A9-4AD826' 
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_token_info]') IS NOT NULL
	drop procedure dbo.up_token_info
go

create procedure dbo.up_token_info
	@token varchar(255),
	@service varchar(100)
/* WITH ENCRYPTION */ 
AS 
BEGIN  
	if(len(@token)>0)
	begin	 
		declare @utstamp varchar(25) = ''
		declare @oAuthExpDate datetime 
		declare @userType int = 1
		declare @number int = 0
		declare @dep int = 0 

		select
			@utstamp = clstamp,
			@oAuthExpDate = oAuthExpdate
		from 
			token (nolock)
		where 
			oAuthToken = @token

		select
			@number = no,
			@dep = estab
		from 
			b_utentes (nolock)
		where 
			utstamp = @utstamp

		select 1 as result, oAuthToken = @token, oAuthExpDate =  CONVERT(varchar,@oAuthExpDate,21), number = @number, dep = @dep, companyId = NULL, userType = @userType
	end	
	ELSE
		select -1 as result
END 

GO
	Grant Execute on dbo.up_token_info to Public
	Grant Control on dbo.up_token_info to Public
GO




