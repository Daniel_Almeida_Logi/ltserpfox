/*
	. BD			: msb-auth
	. Tabela		: token / service_user
	. Criado		: DA - 2020.06.18
	. Alterado		: DA - 2020.06.18
	. Valida se o utilizador / empresa tem acesso ao servi�o. 
	. Valida se o token est� valido. 

	 exec up_authRefreshToken '5FA58792D0A94E5F90E9B4ED948631EF512B0C01DC534EF2890E4316D0486AAB', 'erp', 3 -- valida servi�o

	 resultado
	 result > 0 - valido
	 result = 0 - sem premissoes para o servi�o
	 result = -1 - token invalido

	 . testes
	 select *from [b_utentes] where no=215 and estab=0

	 update [user] set inactivo = 0 where stamp='89FD0D60-6DFB-4788-85A9-4AD826' 
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_authRefreshToken]') IS NOT NULL
	drop procedure dbo.up_authRefreshToken
go

create procedure dbo.up_authRefreshToken
	@token varchar(255),
	@service varchar(255),
	@tokenDurationDays int = 7 

/* WITH ENCRYPTION */ 
AS 
BEGIN 
	set @token = ltrim(rtrim(@token))
	set @service = ltrim(rtrim(@service))
	
	declare @result  int  
	
	IF OBJECT_ID(N'dbo.authResult', N'U') IS NOT NULL  
	   DROP TABLE dbo.authResult 

	create table authResult(
		result int,
		oAuthToken varchar(255)
	)

	 
	insert into authResult (result)  
		exec up_authToken @token, @service

	select top 1
		@result = isnull(result, 0) 
	from 
		authResult (nolock) 

	if @result > 0 
	begin
		print 'token is valid'
		
		declare @utstamp varchar(25) 
		declare @user varchar(25) 
		declare @pass varchar(25) 
		
		update 
			token
		set 
			oAuthExpDate = DATEADD(day, -1, GETDATE())
			,usrdate = convert(varchar, GETDATE(), 120) 
		where
			oAuthToken = @token

		select 
			@utstamp = rtrim(ltrim(isnull(clStamp,''))) 
		from
			token (nolock)
		where 
			oAuthToken = @token

			print '@utstamp: ' +@utstamp 

		exec up_gerar_token @utstamp, @tokenDurationDays  
	end
	else
	begin 
		print 'token is not valid'
		select @result as result, '' as oAuthToken, '' as oAuthExpDate, -1 as number, -1 as dep, null as companyId, -1 as userType 
	end
	
	IF OBJECT_ID(N'dbo.authResult', N'U') IS NOT NULL  
	   DROP TABLE dbo.authResult 
END 

GO
	Grant Execute on dbo.up_authRefreshToken to Public
	Grant Control on dbo.up_authRefreshToken to Public
GO




