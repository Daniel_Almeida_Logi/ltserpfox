/* SP Pesquisa Verifica metodos de pagamento externos e retorna dados
Daniel Almeida, 2022-03-23
--------------------------------

Pesquisa nas tabelas: 	
modoPagExt
select *from modoPagExt



EXECUÇÃO DA SP:
---------------

exec up_dadosPagamento 'Loja 1'


select *from modoPagExt



*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_dadosPagamento]') IS NOT NULL
	DROP PROCEDURE [dbo].up_dadosPagamento;
GO

CREATE PROCEDURE [dbo].up_dadosPagamento	
	 @site	    varchar(60)

		
	
/* WITH ENCRYPTION */
AS

	DECLARE @downloadURL VARCHAR(100)
	SELECT @downloadURL = rtrim(ltrim(textValue)) FROM B_Parameters(nolock) WHERE stamp='ADM0000000313'


	DECLARE @id_lt varchar(100)	
	DECLARE @site_nr INT
	SELECT @id_lt = rtrim(ltrim(id_lt)),@site_nr=no from empresa(nolock) where site = @site



	set @downloadURL = @downloadURL  + @id_lt


	 
	
	select 
		"id"                               = ltrim(rtrim(modoPagExt.id)),
		"designation"					   = ltrim(rtrim(modoPagExt.descr)),
		"idpaymentOnDeliveryHome"          = modoPagExt.entregaCasa,
		"payNow"                           = modoPagExt.pagaAgora,
		"linkImage"                        = case when isnull(anexos.anexosstamp,'')	!='' then  @downloadURL + "/" + isnull(anexos.anexosstamp,'') else '' end		 
	from
	 modoPagExt(nolock)
	 left join anexos(nolock) on modoPagExt.stamp = anexos.regstamp and anexos.tipo='SHARED' and anexos.tabela = 'modoPagExt'
    where 
		inativo = 0
	


GO
GRANT EXECUTE on dbo.up_dadosPagamento TO PUBLIC
GRANT Control on dbo.up_dadosPagamento TO PUBLIC
GO

