/* SP Pesquisa Verifica empresa e retorna dados
Jorge Gomes, 2020-07-10
--------------------------------

Pesquisa nas tabelas: 	
empresa
select top 1 nomecomp ,  no, estab, assfarm,codfarm, ncont,* from empresa where site='Loja 1'

OUTPUT: 
---------
DateMessag				StatMessag	DescMessag		morada
2020-04-28 11:44:24.370	1			Delivery Address Deleted	06655884-B218-4243-8F34-E6458CED5A4

DateMessag				StatMessag	DescMessag						morada
2020-04-28 11:44:44.777	-1			Delivery Address Not Present	06655884-B218-4243-8F34-E6458CED5A4

DateMessag				StatMessag	ActionMessage	DescMessag	morada
2020-07-10 11:27:04.773	0			0				NO DATA	



EXECUÇÃO DA SP:
---------------
exec up_DadosEmpresa 'Atlantico',1
exec up_DadosEmpresa 'Loja 1',0
exec up_DadosEmpresa 'Loja 1',1
exec up_DadosEmpresa 'Loja 1',null


select * from empresa
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_DadosEmpresa]') IS NOT NULL
	DROP PROCEDURE [dbo].up_DadosEmpresa;
GO

CREATE PROCEDURE [dbo].[up_DadosEmpresa]	
	 @site	    varchar(60),
	 @incEmail	bit = 0

/* WITH ENCRYPTION */
AS
   
IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRodape'))
DROP TABLE #dadosRodape

create table #dadosRodape(
	rodape varchar(3000),
	site varchar(20),
	inicial VARCHAR(30)
)

-- variaveis auxiliares 		
DECLARE @siteno AS tinyint
SET @siteno = (select top 1 no from empresa(nolock) where site = @site)	
	

SET @site			=   rtrim(ltrim(isnull(@site,'')))
SET @incEmail	    =   isnull(@incEmail,0)


declare @siteUrlNewUser			as varchar(254) = ''
declare @siteUrlPw				as varchar(254) = ''
declare @paymentOnDelivery		as bit=0
declare @storePickup			as bit=0
declare @stockToConsider		as int = 1
declare @fileExtensions			as varchar(254) = ''
declare @fileSizeMax			as decimal(16,2) = 0.00
declare @emailFooter			as varchar(MAX) = ''
declare @saleWithoutStock		as bit=0
declare @paymentOnDeliveryHome	as bit=0
declare @cardTypeDesc			as varchar(254) = ''
declare @cardTypeId				as int =0
declare @accountApproval		as bit =0
declare @shouldSendEmail		as bit=0
declare @refCollection			as varchar(254) = ''
declare @descriptionColletion	as varchar(254) = ''
declare @taxaivaColletion		as numeric(5,2) = 0
declare @ivaInclColletion		as bit = 0
declare @designColletion		as varchar(254) = ''
declare @priceColletion			as numeric(19,6) = 0

set @siteUrlNewUser = (select isnull(textValue,'') from B_Parameters_site(nolock) where stamp='ADM0000000101' and site=(select site from empresa(nolock) where no=@siteno))
set @siteUrlPw = (select isnull(textValue,'') from B_Parameters_site(nolock) where stamp='ADM0000000102' and site=(select site from empresa(nolock) where no=@siteno))
set @paymentOnDelivery = (select isnull(bool,0) from B_Parameters_site(nolock) where stamp='ADM0000000105' and site=(select site from empresa(nolock) where no=@siteno))
set @storePickup = (select isnull(bool,0) from B_Parameters_site(nolock) where stamp='ADM0000000111' and site=(select site from empresa(nolock) where no=@siteno))
set @stockToConsider = (select convert(int,isnull(numValue,1)) from B_Parameters_site(nolock) where stamp='ADM0000000116' and site=(select site from empresa(nolock) where no=@siteno))
set @fileExtensions = (select isnull(textValue,'') from B_Parameters_site(nolock) where stamp='ADM0000000012' and site=(select site from empresa(nolock) where no=@siteno))
set @fileSizeMax = (select convert(int,isnull(numValue,0)) from B_Parameters_site(nolock) where stamp='ADM0000000011' and site=(select site from empresa(nolock) where no=@siteno))
set @saleWithoutStock = (select isnull(bool,0) from B_Parameters_site(nolock) where stamp='ADM0000000121' and site=(select site from empresa(nolock) where no=@siteno))
set @paymentOnDeliveryHome = (select isnull(bool,0) from B_Parameters_site(nolock) where stamp='ADM0000000122' and site=(select site from empresa(nolock) where no=@siteno))
set @cardTypeDesc = (select top 1  isnull(textValue,'')   from B_Parameters(nolock) where stamp='ADM0000000302')
set @accountApproval = (select isnull(bool,0) from B_Parameters_site(nolock) where stamp='ADM0000000118' and site=(select site from empresa(nolock) where no=@siteno))
set @shouldSendEmail = (select isnull(bool,0) from B_Parameters_site(nolock) where stamp='ADM0000000145' and site=(select site from empresa(nolock) where no=@siteno))
set @refCollection = (select isnull(textValue,'') from B_Parameters_site(nolock) where stamp='ADM0000000168' and site=(select site from empresa(nolock) where no=@siteno))
set @descriptionColletion = (select RTRIM(LTRIM(isnull(textValue,''))) from B_Parameters_site(nolock) where stamp='ADM0000000122' and site=(select site from empresa(nolock) where no=@siteno))

set @cardTypeDesc = lower(rtrim(ltrim(isnull(@cardTypeDesc,''))))

if(@cardTypeDesc='valor')
   set	@cardTypeId = 2
else if @cardTypeDesc='pontos'
	 set @cardTypeId = 1
else
	 set @cardTypeId = 0


INSERT #dadosRodape (rodape,site,inicial)
EXEC usp_get_rodapeEmail 'ADM', @site

select 
	top 1
	 @emailFooter = rtrim(ltrim(isnull(rodape,'')))
from
	#dadosRodape	

	 
if(isnull(@paymentOnDeliveryHome,0)=1)
	begin

		SELECT TOP 1
			 @taxaivaColletion = Round(isnull(taxasiva.taxa,0),2) 
			,@ivaInclColletion = isnull(st.ivaincl,1)  
			,@designColletion  = isnull(st.design,'')  
			,@priceColletion   = Round(isnull(st.epv1,0),2)
			,@refCollection    = ISNULL(st.ref,'') 
		FROM 
			st(nolock)
			left join taxasiva (nolock) on st.tabiva = taxasiva.codigo
		where 
			st.site_nr = @siteno
			and st.ref = @refCollection
			and st.inactivo = 0
	end


DECLARE @sql varchar(max) = ''


set @sql ='
select top 1 
	nomecomp as siteName,
	no as siteNumber, 
	estab as siteDep, 
	assfarm as siteAssoc, 
	codfarm as siteAssocCod,
	ncont as siteVatNr, 
	ltrim(rtrim(condGeraisDist)) as distConditions, 
	ltrim(rtrim(isnull(id_lt,''''))) as siteId, 
	rtrim(ltrim(isnull(morada,''''))) AS AddressInfo_address,
	rtrim(ltrim(isnull(codpost,''''))) AS AddressInfo_zipCode,
	rtrim(ltrim(isnull(concelho,''''))) AS AddressInfo_city,
	''' + convert(varchar(1),isnull(@cardTypeId,0)) + ''' as cardTypeId,
	''' + convert(varchar(1),isnull(@paymentOnDelivery,0)) + ''' as paymentOnDelivery,
	''' + convert(varchar(1),isnull(@storePickup,0)) + ''' as storePickup,
	''' + convert(varchar(1),isnull(@stockToConsider,0)) + ''' as stockToConsider,
	''' + convert(varchar(254),isnull(@fileExtensions,'')) + ''' as fileExtensionsAllowed,
	''' + convert(varchar(20),isnull(@fileSizeMax,0.00)) + '''   as fileMaxSize,
	''' + convert(varchar(1),isnull(@saleWithoutStock,0)) + ''' as saleWithoutStock,
	''' + convert(varchar(1),isnull(@paymentOnDeliveryHome,0)) + ''' as paymentOnDeliveryHome,
	''' + convert(varchar(1),isnull(@accountApproval,0)) + ''' as accountApproval,
	ltrim(rtrim(isnull(has_ticket_integration,0))) as hasTicketIntegration, 
	ltrim(rtrim(isnull(aboutCompany,''''))) as about, 
	rtrim(ltrim(isnull(addressCompany,''''))) AS address,
	rtrim(ltrim(isnull(contactCompany,''''))) AS contact,
	rtrim(ltrim(isnull(websiteUrl,''''))) AS websiteUrl,
	ltrim(rtrim(isnull(instagramUrl,''''))) as instagramUrl, 
	rtrim(ltrim(isnull(facebookUrl,''''))) AS facebookUrl,
	rtrim(ltrim(isnull(whatsappContact,''''))) AS whatsapp,
	''' + convert(varchar(1),isnull(@shouldSendEmail,0)) + ''' as shouldSendEmail,
	'
	if(isnull(@incEmail,0)=1)
	begin
	
	set @sql = @sql + '	ltrim(rtrim(email_sender))						as EmailInfo_sender,
		ltrim(rtrim(email_password))									as EmailInfo_password,
		ltrim(rtrim(email_smtp))										as EmailInfo_smtpServer,
		email_ssl														as EmailInfo_useSsl,
		email_tls														as EmailInfo_useTls,
		ltrim(rtrim(email_port))										as EmailInfo_port,
		email_username													as EmailInfo_username,
		isnull(mailsign,'''')											as EmailInfo_sign,
		isnull(email_cc,'''')											as EmailInfo_cc,
		isnull(email_bcc,'''')											as EmailInfo_bcc,
		''' +  @emailFooter + '''										as EmailInfo_footer,
		'

	end 

	if(isnull(@paymentOnDeliveryHome,0)=1 and @priceColletion > 0)
	begin
		set @sql = @sql + '	
			''' + RTRIM(LTRIM(isnull(@refCollection,''))) + '''									as paymentDeliveryHomeInfo_refOnDeliveryHome,
			''' + RTRIM(LTRIM(isnull(@descriptionColletion,''))) + '''							as paymentDeliveryHomeInfo_descriptionOnDeliveryHome,
			''' + convert(varchar(20),ROUND(isnull(@taxaivaColletion,0),2)) + '''				as paymentDeliveryHomeInfo_vatPercOnDeliveryHome,
			''' + convert(varchar(1),isnull(@ivaInclColletion,0)) + '''							as paymentDeliveryHomeInfo_vatIncOnDeliveryHome,
			''' + RTRIM(LTRIM(isnull(@designColletion,'')))  + '''								as paymentDeliveryHomeInfo_designOnDeliveryHome,
			''' + convert(varchar(20),CONVERT(numeric(15,2),ROUND(isnull(@priceColletion,0),2)))   + '''				
																								as paymentDeliveryHomeInfo_priceOnDeliveryHome,		
		'
	end 

	set  @sql= @sql +'
	''' + isnull(@siteUrlNewUser,'') + '''                  AS SiteInfo_urlNewUser,
	''' + isnull(@siteUrlPw,'')      + '''                  AS SiteInfo_urlNewPw,
	ltrim(rtrim(isnull(nomabrv,'''')))						AS DesignInfo_nomeAbrev,
	ltrim(rtrim(isnull(tipoempresa,'''')))					AS DesignInfo_typeEmp,
	ltrim(rtrim(isnull(distrito,'''')))						AS AddressInfo_distric,
	ltrim(rtrim(isnull(freguesia,'''')))					AS AddressInfo_parish,
	ltrim(rtrim(isnull(pais,'''')))							AS AddressInfo_country,
	isnull(dataconst,'''')									AS GeneralInfo_openData,
	isnull(datafecho,'''')									AS GeneralInfo_closeData,
	isnull(anoconst,0)									    AS GeneralInfo_companyCreationYear,
	ltrim(rtrim(isnull(telefone,'''')))						AS ContactInfo_phone,
	ltrim(rtrim(isnull(fax,'''')))							AS ContactInfo_fax,
	ltrim(rtrim(isnull(contacto,'''')))						AS ContactInfo_contact,
	ltrim(rtrim(isnull(email,'''')))						AS ContactInfo_email,
	ltrim(rtrim(isnull(ccusto,'''')))						AS GeneralInfo_centerAnalyt,
	ltrim(rtrim(isnull(asspatr,'''')))						AS GeneralInfo_associtPatro,
	ltrim(rtrim(isnull(instss,'''')))						AS FinanceInfo_instSS,
	ltrim(rtrim(isnull(cae,'''')))							AS FinanceInfo_cae,
	ltrim(rtrim(isnull(natjurid,'''')))						AS FinanceInfo_natureLegal,
	ltrim(rtrim(isnull(capsocial,'''')))					AS FinanceInfo_shareCapital,
	ltrim(rtrim(isnull(consreg,'''')))					    AS FinanceInfo_CenterBusinessRegistration,
	ltrim(rtrim(isnull(motivo_isencao_iva,'''')))			AS FinanceInfo_taxIsention,		
	isnull(sedeid,'''')										AS GeneralInfo_sedeId,
	isnull(pme_lider,0)										AS GeneralInfo_pmeLeader,
	ltrim(rtrim(isnull(nib,'''')))							AS FinanceInfo_nib,
	ltrim(rtrim(isnull(cod_swift,'''')))					AS FinanceInfo_swift,
	ltrim(rtrim(isnull(banco,'''')))					    AS FinanceInfo_bank,
	ltrim(rtrim(isnull(dirtec,'''')))						AS GeneralInfo_DirectorTec,
	ltrim(rtrim(isnull(abrev,'''')))						AS DesignInfo_abrev

		from empresa(nolock)
		where site= ''' + isnull(@site,'') + ''''

print @sql
EXECUTE (@sql)
    
IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosRodape'))
DROP TABLE #dadosRodape

GO
GRANT EXECUTE on dbo.up_DadosEmpresa TO PUBLIC
GRANT Control on dbo.up_DadosEmpresa TO PUBLIC
GO


