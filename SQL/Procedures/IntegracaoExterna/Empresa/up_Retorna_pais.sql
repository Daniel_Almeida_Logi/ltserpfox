/* SP Pesquisa Verifica Loja
Daniel Almeida, 2021-01-17
--------------------------------

Pesquisa nas tabelas: 	
 empresa

OUTPUT: 
---------
DateMessag				StatMessag	DescMessag		   site
2021-01-26 11:08:52.610	1			Operador existe		Loja 1

DateMessag				StatMessag	DescMessag			        site
2021-01-26 11:07:38.063	0			Operador  não existe	   	Loja 1


EXECUÇÃO DA SP:
---------------
exec up_Retorna_pais 'Loja 1'



select * from empresa(nolock)  where  site='Loja 1'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Retorna_pais]') IS NOT NULL
	DROP PROCEDURE [dbo].up_Retorna_pais;
GO

CREATE PROCEDURE [dbo].up_Retorna_pais	
	 @site		VARCHAR(18)
	
   
/* WITH ENCRYPTION */
AS

declare @textValue  varchar(18) = ''

SET @site=rtrim(ltrim(isnull(@site,'')))

	select 
		top 1 @textValue = lower(rtrim(ltrim(isnull(textValue,'')))) 
	from 
		B_Parameters_site(nolock) 
	where stamp='ADM0000000050' and site=@site

	select getdate() AS DateMessag, 1 As StatMessag, 1 AS ActionMessage, @textValue As DescMessag
			

GO
GRANT EXECUTE on dbo.up_Retorna_pais TO PUBLIC
GRANT Control on dbo.up_Retorna_pais TO PUBLIC
GO