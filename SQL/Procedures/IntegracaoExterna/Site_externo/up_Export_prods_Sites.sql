/****** Object:  StoredProcedure [dbo].[up_Export_prods_Sites]  

EXEC up_Export_prods_Sites 1, 30
select * from st (nolock) where inactivo = 0 and disponline = 1 and site_nr = 1

Script Date: 26/02/2025 11:31:42 ******/


-- Verificar se a Stored Procedure já existe e, se sim, apagar
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'up_Export_prods_Sites')
BEGIN
    DROP PROCEDURE up_Export_prods_Sites;
END;
GO
-- Criar a Stored Procedure
CREATE PROCEDURE up_Export_prods_Sites
    @Site_nr INT,           -- Parâmetro de entrada para o número da loja
    @Nr_dias_calc INT       -- Parâmetro de entrada para o número de dias no cálculo da média
AS
BEGIN
    -- Definir o SET NOCOUNT ON para evitar mensagens adicionais de contagem de linhas
    SET NOCOUNT ON;
	--Apagar tabelas temporárias
	IF OBJECT_ID('tempdb..#alternativos') IS NOT NULL
		DROP TABLE #alternativos;
	IF OBJECT_ID('tempdb..#temp_vendidos') IS NOT NULL
		DROP TABLE #temp_vendidos;
	IF OBJECT_ID('tempdb..#temp_N_vendidos') IS NOT NULL
		DROP TABLE #temp_N_vendidos
		   
	-- Criar a tabela temporária com os alternativos
	Select ref,
			codigo,
			ROW_NUMBER() OVER (PARTITION BY bc.ref ORDER BY bc.ousrdata DESC) as RN
		into #alternativos
		from bc (nolock)
		where len(codigo) >9;	

    -- Criar a tabela temporária com os produtos vendidos
    SELECT 
        st.site_nr																AS 'Nr.Loja',
		st.ref																	AS 'Referencia', 
		ISNULL(a.codigo,'')														AS 'EAN',
        st.design																AS 'designacao',           
        st.stock,
		st.epv1																	AS 'preco_S_desc',
        st.epv1_final															AS 'preco_C_desc', 
		CASE WHEN dataFimPromo >= getdate() AND st.mfornec = 0 AND st.mfornec2 = 0 
					THEN ''
			 WHEN dataFimPromo >= getdate() AND st.mfornec2 <> 0
					THEN CONCAT(st.mfornec2,'€') 
			 WHEN dataFimPromo >= getdate() AND st.mfornec <> 0				  
					THEN CONCAT(st.mfornec,'%') 
			 else ''
			 END																AS 'Tag desc',
        st.dataIniPromo,     
        st.dataFimPromo,     
		AVG(fi.epv)																AS 'PrecoMedio', 
        st.faminome																AS 'categoria',  
        st.usr1																	AS 'marca' 
    INTO #temp_vendidos 
    FROM st (nolock) 
		LEFT JOIN #alternativos a (nolock) ON st.ref = a.ref
        INNER JOIN fi (nolock) ON st.ref = fi.ref
        INNER JOIN td (nolock) ON td.ndoc = fi.ndoc
    WHERE st.site_nr = @Site_nr 
	  AND fi.armazem in (select armazem from empresa_arm where empresa_no = @Site_nr)
	  AND isnull(a.rn,1) = 1
      AND st.inactivo = 0      
	  AND dispOnline = 1
	  AND fi.ousrdata > getdate() - @Nr_dias_calc
	  AND td.tipodoc = 1  
	GROUP BY st.ref, a.codigo,st.design,st.stock,st.epv1,st.epv1_final,st.dataIniPromo,st.dataFimPromo,st.faminome,st.usr1,st.mfornec,st.mfornec2,st.site_nr
	
	-- Criar a tabela temporária com os produtos Não-vendidos

	    SELECT 
        st.site_nr																AS 'Nr.Loja',
		st.ref																	AS 'Referencia', 
		ISNULL(a.codigo,'')														AS 'EAN',
        st.design																AS 'designacao',           
        st.stock,
		st.epv1																	AS 'preco_S_desc',
        st.epv1_final															AS 'preco_C_desc', 
		CASE WHEN ST.dataFimPromo >= getdate() AND st.mfornec = 0 AND st.mfornec2 = 0 
					THEN ''
			 WHEN ST.dataFimPromo >= getdate() AND st.mfornec2 <> 0
					THEN CONCAT(st.mfornec2,'€') 
			 WHEN ST.dataFimPromo >= getdate() AND st.mfornec <> 0				  
					THEN CONCAT(st.mfornec,'%') 
			 else ''
			 END																AS 'Tag desc',
        st.dataIniPromo,     
        st.dataFimPromo,     
		st.epv1																	AS 'PrecoMedio', 
        st.faminome																AS 'categoria',  
        st.usr1																	AS 'marca'   
    INTO  #temp_N_vendidos 
    FROM st (nolock) 
		LEFT JOIN #alternativos a (nolock) ON st.ref = a.ref
        LEFT JOIN #temp_vendidos on #temp_vendidos.Referencia = st.ref 
    WHERE st.site_nr = @Site_nr 
	  AND #temp_vendidos.Referencia is null
	  AND isnull(a.rn,1) = 1
      AND st.inactivo = 0      
	  AND dispOnline = 1
	
	SELECT * from #temp_vendidos
	UNION
	SELECT * FROM #temp_N_vendidos
   
	--Apagar tabelas temporárias
	IF OBJECT_ID('tempdb..#alternativos') IS NOT NULL
		DROP TABLE #alternativos;
	IF OBJECT_ID('tempdb..#temp_vendidos') IS NOT NULL
		DROP TABLE #temp_vendidos;
	IF OBJECT_ID('tempdb..#temp_N_vendidos') IS NOT NULL
		DROP TABLE #temp_N_vendidos


END;
GO
GRANT EXECUTE ON dbo.up_Export_prods_Sites to Public
GRANT CONTROL ON dbo.up_Export_prods_Sites to Public
GO


