/*
PESQUISA CLIENTES DETALHE dep 
J.Simoes, 02-07-2020


Retorna informação dos dependentes de uma determinado cliente



PROCEDURE
Criada procedure up_utentes_PesquisaClientesDetalheDependente com parâmetros 

Parametros: up_utentes_PesquisaClientesDetalheDependente 'ncont','bino','no_ext','no','estab'

exec up_utentes_PesquisaClientesDetalheDependente '000000000','','','10783192','0',NULL

exec up_utentes_PesquisaClientesDetalheDependente null,'','','201','0',NULL



exec up_utentes_PesquisaClientesDetalheDependente null,null,null,null,null,'da@sapo.pt'

exec up_utentes_PesquisaClientesDetalheDependente NULL,'','','215','0',NULL


exec up_utentes_PesquisaClientesDetalheDependente NULL,'','','10783406','0',NULL
*/





SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_PesquisaClientesDetalheDependente]') IS NOT NULL
    drop procedure up_utentes_PesquisaClientesDetalheDependente
go

CREATE PROCEDURE up_utentes_PesquisaClientesDetalheDependente
	@ncont varchar(20), @bino varchar(20), @no_ext varchar(20), @no varchar(10), @estab varchar(3), @email varchar(100)

AS

DECLARE @sqlCommand VARCHAR(5000) 


SET @ncont = isnull(@ncont,'')

SET @bino = isnull(@bino,'')
SET @no_ext =isnull(@no_ext,'')
--campos numericos na tabela, não permitir input de nao numericos
SET @no = isnull(@no,0)
SET @estab = isnull(@estab,0)
SET @email =isnull(@email,'')
--não mostrar dados caso não se preencha nenhum dos campos: len(@ncont+@bino+@no_ext+@no+@estab)=0

  

IF (@no='' OR @no NOT LIKE '%[^0-9]%') AND (@estab='' OR @estab NOT LIKE '%[^0-9]%') AND (len(@ncont+@bino+@no_ext+@no+@estab+@email)>0 )
BEGIN
	select TOP 1  @no=no,@estab=estab,@estab=estab FROM dbo.b_utentes (NOLOCK)		
	where ( 
			ncont = COALESCE(NULLIF(@ncont,''), ncont) 
			AND bino = COALESCE(NULLIF(@bino,''), bino) 
			AND no_ext = COALESCE(NULLIF(@no_ext,''), no_ext) 
			AND email = COALESCE(NULLIF( @email,''), email) 
			AND (no = case when @no>0 then  COALESCE(NULLIF(@no,''), no)  else no end
				AND estab = COALESCE(NULLIF(@estab ,''), estab))
			) AND inactivo = 0 


	SET @sqlCommand = '
	 SELECT 
			b_utentes.nome															AS DepInfo_name ,
			b_utentes.no															AS DepInfo_number ,
			b_utentes.estab															AS DepInfo_dep ,
			Round(ISNULL(sum(edeb - edebf) - sum(ecred - ecredf),0),2)				AS DepInfo_balanceCC,
			b_utentes.ncont															AS DepInfo_vatNr, 
			b_utentes.nbenef														AS DepInfo_medicalNr,
			rtrim(ltrim(b_utentes.local))                                           AS AddressInfo_DepInfo_city,  
			rtrim(ltrim(b_utentes.morada))                                          AS AddressInfo_DepInfo_address,
			rtrim(ltrim(b_utentes.codpost))                                         AS AddressInfo_DepInfo_zipCode,
			rtrim(ltrim(b_utentes.zona))                                            AS AddressInfo_DepInfo_zone,
	        rtrim(ltrim(b_utentes.descp))                                           AS AddressInfo_addressInfo_countryName,
			rtrim(ltrim(b_utentes.codigop))                                         AS AddressInfo_DepInfo_countryCode,
			b_utentes.entfact														AS DepInfo_isEntityInvoicing,
			rtrim(ltrim(utstamp))													AS DepInfo_regstamp

		FROM b_utentes(nolock) 
			LEFT JOIN cc(nolock)  ON b_utentes.NO = CC.no and b_utentes.estab =cc.estab
		where
			( 
			b_utentes.no ='''+ @no +'''
			) AND inactivo = 0 and b_utentes.estab!= '''+ @estab +'''
		GROUP BY b_utentes.nome, 			
			b_utentes.no,
			b_utentes.estab,
			b_utentes.ncont,
			b_utentes.nbenef,
			b_utentes.local,
			b_utentes.morada,
			b_utentes.codpost,
			b_utentes.zona,
			b_utentes.descp,
			b_utentes.codigop,
			b_utentes.entfact,	
			b_utentes.utstamp

	ORDER by b_utentes.no asc,b_utentes.estab asc '
	PRINT @sqlCommand 
	EXEC (@sqlCommand)

END
/*
ELSE
BEGIN
	print('Tem de preencher pelo menos um campo de pesquisa com valores numéricos')
END
*/


GO
Grant Execute On up_utentes_PesquisaClientesDetalheDependente to Public
Grant Control On up_utentes_PesquisaClientesDetalheDependente to Public
go


