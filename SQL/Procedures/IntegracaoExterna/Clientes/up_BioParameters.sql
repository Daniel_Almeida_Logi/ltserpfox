/* SP Pesquisa Bio Parameters
J.Gomes, 2020-07-08
--------------------------------

Listagem de Bio Parameters
atraves da pesquisa de um cliente, por qualquer um dos campos disponiveis, procura-se os bio-parametros atraves
do utstamp.


Pesquisa nas tabelas: 	
	b_utentes, bioregistos, bioparametros
	
Campos obrigatórios: algum dos campos de pesquisa de cliente


OUTPUT:
---------
pageSize, pageTotal, pageNumber, linesTotal, bioParam_Info_name, bioParam_Info_value, bioParam_Info_referenceValue
, bioParam_Info_unit, bioParam_Info_ref, bioParam_Info_obs,	Operation_Info_lastChangeDate, Operation_Info_lastChangeTime



EXECUÇÃO DA SP:
---------------
Parametros: up_BioParameters 'ncont','bino','no_ext','no','estab', 'email', datainit, dataend, page, orderstamp

exec up_BioParameters '','','','198288077','0','','','',1,''
exec up_BioParameters '','','','215','0','',null,null,1,''
exec up_BioParameters null,null,null,'215','0',null,null,null,1,''
exec up_BioParameters '','','','215','0','','2020-01-06','2020-08-07',1,''
exec up_BioParameters '','','','215','0','','','',1,'24828522-D7E8-413A-885F-1E88C50D0BDF'

@orderstamp
BioParameters loja1: 
bioParam_Info_name -		CFC204F6-58C3-4498-A733-3B7CD1051348
bioParam_Info_name desc -	24828522-D7E8-413A-885F-1E88C50D0BDF


	*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_BioParameters]') IS NOT NULL
	DROP PROCEDURE [dbo].up_BioParameters ;
GO

CREATE PROCEDURE [dbo].[up_BioParameters]	

	  @ncont			varchar(20)
	, @bino				varchar(20)
	, @no_ext			varchar(20)
	, @no				varchar(10)
	, @estab			varchar(3)
	, @email			varchar(50)
	, @dateInit			varchar(10)  = '1900-01-01'
	, @dateEnd	        varchar(10)  = '3000-01-01'
	, @pageNumber       int = 1
	, @orderStamp		VARCHAR(36) = ''
	   
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

SET @dateInit		    =   rtrim(ltrim(isnull(@dateInit,'')))
SET @dateEnd		    =   rtrim(ltrim(isnull(@dateEnd,'')))
set @orderStamp         =	rtrim(ltrim(ISNULL(@orderStamp,'')))
--as variaveis de pesquisa de cliente nao precisam disto, tem o coalesce abaixo...


if(isnull(@pageNumber,0)<1)
	set @pageNumber = 1	

DECLARE @sql varchar(max)
DECLARE @orderBy VARCHAR(8000)
DECLARE @PageSize int = 100
DECLARE @OrderCri VARCHAR(MAX)
declare @utstamp varchar(25) =''

IF @orderStamp = ''
BEGIN 
	SET @OrderCri = ' ORDER BY bioParam_Info_name'
END 
ELSE 
BEGIN 
	SELECT @OrderCri='ORDER BY ' + instructionSql  FROM sortingTableOrder (NOLOCK) WHERE stamp = @orderStamp and inativo=0 and nameService='BioParameters'
END


select top 1 @utstamp=rtrim(ltrim(utstamp)) from b_utentes (NOLOCK) where ( 
		ncont = COALESCE(NULLIF(@ncont,''), ncont) 
		AND bino = COALESCE(NULLIF(@bino,''), bino) 
		AND no_ext = COALESCE(NULLIF(@no_ext,''), no_ext) 
		AND (no = COALESCE(NULLIF(@no,''), no) AND estab = COALESCE(NULLIF(@estab,''), estab))
		AND email = COALESCE(NULLIF(@email,''), email) 
		)
		AND inactivo = 0
		;

/*
select top 1 @utstamp=rtrim(ltrim(utstamp)) from b_utentes where  no=215 and estab=0;
*/		


set @orderBy =  @OrderCri + '
				 OFFSET ' + convert(varchar(10),@PageSize) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
				 FETCH NEXT  ' + convert(varchar(10),@PageSize) + ' ROWS ONLY '

select @sql = N' 			
			SELECT '  + convert(varchar(10),@PageSize) +' as pageSize,  (COUNT(*) OVER() / ' +  convert(varchar(10),@PageSize) +') + 1 as pageTotal, '
				+ convert(varchar(10),@PageNumber) +' as pageNumber, COUNT(*) OVER() as linesTotal	
				, nome as bioParam_Info_name 
				,isnull((select top 1 CASE WHEN valor_min>0 THEN RTRIM(LTRIM(STR(valor)))+'' - ''+RTRIM(LTRIM(STR(valor_min))) ELSE RTRIM(LTRIM(STR(valor))) END from bioregistos (nolock) where bioparametros.id=bioregistos.id_bioparametros and utstamp=''' + @utstamp + ''' order by data desc),0) as bioParam_Info_value
				, vref as bioParam_Info_referenceValue
				, unidade as bioParam_Info_unit
				,bioparametros.id as bioParam_Info_ref
				,isnull((select top 1 isnull(obs,'''') from bioregistos (nolock) where bioparametros.id=bioregistos.id_bioparametros and utstamp=''' + @utstamp + ''' order by data desc),'''') as bioParam_Info_obs
				,isnull((select top 1 CONVERT(varchar(10), data, 23) from bioregistos (nolock) 
							where bioparametros.id=bioregistos.id_bioparametros and utstamp=''' + @utstamp + ''' order by data desc),''1900-01-01'') as OperationInfo_lastChangeDate
				,isnull((select top 1 left(hora,8) from bioregistos (nolock) 
							where bioparametros.id=bioregistos.id_bioparametros and utstamp=''' + @utstamp + ''' order by data desc),''00:00:00'') as OperationInfo_lastChangeTime
			FROM bioparametros (nolock)  
			'
			
if @dateInit != '' and @dateEnd  != ''
	select @sql = @sql + N' where isnull((select top 1 CONVERT(varchar, data, 23) from bioregistos (nolock) 
							where bioparametros.id=bioregistos.id_bioparametros and utstamp=''' + @utstamp + ''' order by data desc),'''') 
							BETWEEN ''' + @dateInit + '''  AND  ''' + @dateEnd + ''''

select @sql = @sql + @orderBy

  
print @sql
EXECUTE (@sql)


GO
GRANT EXECUTE on dbo.up_BioParameters TO PUBLIC
GRANT Control on dbo.up_BioParameters TO PUBLIC
GO