/* SP Pesquisa Verifica valor em cartao
Daniel Almeida, 2021-03-02
--------------------------------

Pesquisa nas tabelas: 	
utentes 

OUTPUT: 
---------
DateMessag				StatMessag	DescMessag		
2020-04-28 11:08:52.610	1			valor valido	

DateMessag				StatMessag	DescMessag			
2020-04-28 11:07:38.063	0			valor invalido	


EXECUÇÃO DA SP:
---------------
exec up_utentes_verifica_val_cartao 201,0,10
exec up_utentes_verifica_val_cartao 199,0,0.20

EXEC up_utentes_verifica_val_cartao 1210,0,100000.0000



*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].up_utentes_verifica_val_cartao') IS NOT NULL
	DROP PROCEDURE [dbo].up_utentes_verifica_val_cartao;
GO

CREATE PROCEDURE [dbo].up_utentes_verifica_val_cartao	
	 @number	int
	,@dep		int
	,@cardMoney	numeric(15,2) = 0
	
	   
/* WITH ENCRYPTION */
AS

SET @number				=   isnull(@number,-1)
SET @dep				=   isnull(@dep,-1)
SET @cardMoney		    =   isnull(@cardMoney,0.00)

declare @total numeric(15,2) = -1




/* criacao e se não permite*/


	select 
	top 1
		@total = case when 
					isnull(b_fidel.valcartao,0)- isnull(b_fidel.valcartao_cativado,0) <= 0 
				then 
					0
				else   
					isnull(b_fidel.valcartao,0)- isnull(b_fidel.valcartao_cativado,0)
				end
	
	from 
		b_fidel(nolock)
	inner join 
		b_utentes(nolock) on b_utentes.no = b_fidel.clno and b_utentes.estab = b_fidel.clestab
	where  
		clno = @number
		and clestab= @dep
		and b_utentes.inactivo = 0
		and b_fidel.inactivo = 0
		and rtrim(ltrim(b_fidel.nrcartao)) = rtrim(ltrim(b_utentes.nrcartao))
		and rtrim(ltrim(isnull(b_utentes.nrcartao,''))) !=''
	

   if(round(@total,2)>=@cardMoney)
    begin
	 select getdate() AS DateMessag, 1 As StatMessag, 'Valor de cartao valido' As DescMessag
    end
	else begin
	  select getdate() AS DateMessag, 0 As StatMessag, 'Valor de cartao invalido' As DescMessag
	end

GO
GRANT EXECUTE on dbo.up_utentes_verifica_val_cartao TO PUBLIC
GRANT Control on dbo.up_utentes_verifica_val_cartao TO PUBLIC
GO