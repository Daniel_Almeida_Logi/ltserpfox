/* SP Pesquisa Bio Parameters Detail
J.Gomes, 2020-07-09
--------------------------------

Listagem de Bio Parameters
atraves da pesquisa de um cliente, por qualquer um dos campos disponiveis, procura-se os bio-parametros
em detalhe (historico) atraves do utstamp.


Pesquisa nas tabelas: 	
	b_utentes, bioregistos, bioparametros
	
Campos obrigatórios: algum dos campos de pesquisa de cliente


OUTPUT:
---------
pageSize, pageTotal, pageNumber, linesTotal,id, utstamp, bioParam_Info_name, bioParam_Info_value, bioParam_Info_referenceValue
, bioParam_Info_unit, bioParam_Info_ref, bioParam_Info_obs,	Operation_Info_lastChangeDate, Operation_Info_lastChangeTime


EXECUÇÃO DA SP:
---------------
Parametros: up_BioParametersDetail 'ncont','bino','no_ext','no','estab', 'email', datainit, dataend, page, ref, orderstamp

exec up_BioParametersDetail '','','','198288077','0','','','',1
exec up_BioParametersDetail '','','','215','0','',null,null,1,4,''
exec up_BioParametersDetail '','','','215','0','','','','',null,''

com orderby: exec up_BioParametersDetail '','','','215','0','','','',1,4,'BB5F036B-5809-4DD4-8922-2EDBF5EAE110'

sem ref nao devolve nada : exec up_BioParametersDetail '','','','215','0','','','',1,'',''

exec  up_BioParametersDetail NULL,NULL,NULL,215,'0',NULL

ref:
4

@orderstamp
BioParametersDetail loja1: 
DATA , Hora  -			BB5F036B-5809-4DD4-8922-2EDBF5EAE110
DATA desc, Hora desc -	646C21C1-3000-4B3B-9724-74EE91604273

	*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_BioParametersDetail]') IS NOT NULL
	DROP PROCEDURE [dbo].up_BioParametersDetail ;
GO

CREATE PROCEDURE [dbo].[up_BioParametersDetail]	

	  @ncont			varchar(20)
	, @bino				varchar(20)
	, @no_ext			varchar(20)
	, @no				varchar(10)
	, @estab			varchar(3)
	, @email			varchar(50)
	, @dateInit			varchar(10)  = '1900-01-01'
	, @dateEnd	        varchar(10)  = '3000-01-01'
	, @pageNumber       int = 1
	, @ref				VARCHAR(10) = ''
	, @orderStamp		VARCHAR(36) = ''
	
	   
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

SET @dateInit		    =   rtrim(ltrim(isnull(@dateInit,'')))
SET @dateEnd		    =   rtrim(ltrim(isnull(@dateEnd,'')))
set @orderStamp         =	rtrim(ltrim(ISNULL(@orderStamp,'')))
set @ref		        =	rtrim(ltrim(ISNULL(@ref,'')))
--as variaveis de pesquisa de cliente nao precisam disto, tem o coalesce abaixo...


if(isnull(@pageNumber,0)<1)
	set @pageNumber = 1	

DECLARE @sql varchar(max)
DECLARE @orderBy VARCHAR(8000)
DECLARE @PageSize int = 100
DECLARE @OrderCri VARCHAR(MAX)

declare @utstamp varchar(25)=''

IF @orderStamp = ''
BEGIN 
	SET @OrderCri = ' ORDER BY DATA desc, Hora desc'
END 
ELSE 
BEGIN 
	SELECT @OrderCri='ORDER BY ' + instructionSql  FROM sortingTableOrder (NOLOCK) WHERE stamp = @orderStamp and inativo=0 and nameService='BioParametersDetail'
END

print '@ncont:'+  @ncont
print '@bino:'+  @bino
print '@no_ext:'+  @no_ext
print '@no:'+  @no
print '@estab:'+  @estab
print '@email:'+  @email

select top 1 @utstamp=utstamp from b_utentes (NOLOCK) where ( 
		ncont = COALESCE(NULLIF(@ncont,''), ncont) 
		AND bino = COALESCE(NULLIF(@bino,''), bino) 
		AND no_ext = COALESCE(NULLIF(@no_ext,''), no_ext) 
		AND (no = COALESCE(NULLIF(@no,''), no)
		AND estab = COALESCE(NULLIF(@estab,''), estab))
		AND email = COALESCE(NULLIF(@email,''), email) 
		) AND inactivo = 0;

		
set @orderBy =  @OrderCri + '
				 OFFSET ' + convert(varchar(10),@PageSize) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
				 FETCH NEXT  ' + convert(varchar(10),@PageSize) + ' ROWS ONLY '

select @sql = N' 			
			SELECT '  + convert(varchar(10),@PageSize) +' as pageSize,  (COUNT(*) OVER() / ' +  convert(varchar(10),@PageSize) +') + 1 as pageTotal, '
				+ convert(varchar(10),@PageNumber) +' as pageNumber, COUNT(*) OVER()																			AS linesTotal	
				, A.id, A.utstamp
				, B.nome /*+'';''+	CASE WHEN A.valor_min>0 THEN B.NOME + '' Min.;'' + b.nome + '' Max.''			Else ''''						END	*/		as bioParam_Info_name 
				, CASE WHEN A.valor_min>0 THEN B.NOME + '' Min.;'' + b.nome + '' Max.''								Else B.nome + '';''				END			AS bioParam_Info_ChartLegend
				, CASE WHEN A.valor_min>0 THEN RTRIM(LTRIM(STR(A.valor))) +'' - ''+	 RTRIM(LTRIM(STR(A.valor_min)))	ELSE RTRIM(LTRIM(STR(A.valor)))	END			AS bioParam_Info_value
				,CASE WHEN A.valor_min>0 THEN RTRIM(LTRIM(STR(A.valor_min)))										ELSE ''''						END			AS bioParam_Info_valueMin
				,CASE WHEN A.valor_min>0 THEN RTRIM(LTRIM(STR(A.valor)))											ELSE ''''						END			AS bioParam_Info_valueMax
				, B.vref as bioParam_Info_referenceValue, B.unidade																								AS bioParam_Info_unit
				, A.id_bioparametros as bioParam_Info_ref, A.obs																								AS bioParam_Info_obs
				, convert(varchar(10), isnull(A.data,''1900-01-01''),23) as OperationInfo_creationDate, left(A.hora,8)											AS OperationInfo_creationTime
				from BioRegistos (nolock) A
				inner join bioparametros (nolock) B
				on B.id = A.id_bioparametros 
				where utstamp = ''' + @utstamp + '''
				
			'
if(@ref != '')
	select @sql = @sql + N' and B.id = ''' + @ref + ''''
	

if @dateInit != '' and @dateEnd  != ''
	select @sql = @sql + N' AND A.data BETWEEN ''' + @dateInit + '''  AND  ''' + @dateEnd + ''''


select @sql = @sql + @orderBy
	   
print @sql
EXECUTE (@sql)



GO
GRANT EXECUTE on dbo.up_BioParametersDetail TO PUBLIC
GRANT Control on dbo.up_BioParametersDetail TO PUBLIC
GO