/* 
 exec up_utente_PesquisaTratamento 206,'',0
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_utente_PesquisaTratamento]') IS NOT NULL
	DROP PROCEDURE [dbo].up_utente_PesquisaTratamento ;
GO

CREATE PROCEDURE [dbo].[up_utente_PesquisaTratamento]	

@no		int,
@site	varchar(60) = '',
@estab	int = 0

AS
SET NOCOUNT ON
Declare @siteNr int = 0 

	Select @siteNr = no
	From empresa(nolock)
	where empresa.site = @site

	SET @siteNr = ISNULL(@siteNr,0)

	Select 
		B_histConsumo.ref																						AS treatmentInfo_ref
		,B_histConsumo.design																					AS treatmentInfo_design
		,ROUND(B_histConsumo.qtt,2)																				AS treatmentInfo_quantity
		,B_histConsumo.obs																						AS treatmentInfo_note
		,convert(varchar,B_histConsumo.ousrinis,23)																AS treatmentInfo_dataIni
		,convert(varchar,B_histConsumo.ousrdata,23)																AS treatmentInfo_dataEnd
		,B_histConsumo.dci																						AS treatmentInfo_dci
		,B_histConsumo.posologia																				AS treatmentInfo_dosage
		,B_histConsumo.palmoco																					AS treatmentInfo_breakFast
		,B_histConsumo.almoco																					AS treatmentInfo_lunch
		,B_histConsumo.lanche																					AS treatmentInfo_lateLunch
		,B_histConsumo.jantar																					AS treatmentInfo_dinner
		,B_histConsumo.noite																					AS treatmentInfo_night
		,''																										AS treatmentInfo_wayAdministration
		,''																										AS treatmentInfo_groupTherapeutic
		,CONVERT(bit,0)																							AS treatmentInfo_prescribed
		,ROUND(st.stock,2)																						AS treatmentInfo_stock
		,''																										AS treatmentInfo_timeDay
		,''																										AS treatmentInfo_packing
		,''																										AS treatmentInfo_blister
		,''																										AS treatmentInfo_state
	FROM B_histConsumo(nolock)
	inner join b_utentes(nolock) on B_histConsumo.clstamp = b_utentes.utstamp
	inner join empresa(nolock) on empresa.site = B_histConsumo.site
	inner join st(nolock) on  B_histConsumo.ref = st.ref and st.site_nr= empresa.no
	where 
		b_utentes.no = @no 
		and b_utentes.estab = @estab
		and  st.site_nr = Case when @siteNr > 0 then @siteNr else st.site_nr end   
		and B_histConsumo.site = Case when @siteNr  > 0 then @site else B_histConsumo.site end 

GO
GRANT EXECUTE on dbo.up_utente_PesquisaTratamento TO PUBLIC
GRANT Control on dbo.up_utente_PesquisaTratamento TO PUBLIC
GO