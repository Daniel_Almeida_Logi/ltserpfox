/* SP Pesquisa Verifica email
Daniel Almeida, 2021-03-02
--------------------------------

Pesquisa nas tabelas: 	
utentes 

OUTPUT: 
---------
DateMessag				StatMessag	DescMessag		
2020-04-28 11:08:52.610	1			email repetido	

DateMessag				StatMessag	DescMessag			
2020-04-28 11:07:38.063	0			email repetido	


EXECUÇÃO DA SP:
---------------
exec up_utentes_verifica_email null,null,'danielalmeida@logitools.pt'


select * from b_utentes  where  email = 'danielalmeida@logitools.pt'

*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].up_utentes_verifica_email') IS NOT NULL
	DROP PROCEDURE [dbo].up_utentes_verifica_email;
GO

CREATE PROCEDURE [dbo].up_utentes_verifica_email	
	 @number	int
	,@dep		int
	,@email		VARCHAR(60)
	
	   
/* WITH ENCRYPTION */
AS

SET @number				=   isnull(@number,-1)
SET @dep				=   isnull(@dep,-1)
SET @email				=   left(rtrim(ltrim(isnull(@email,''))),45)

declare @total int = 0


declare @allow as bit = 1

set @allow = (select top 1 isnull(bool,1) from B_Parameters(nolock) where stamp='ADM0000000322')



/* criacao e se não permite*/
IF @number <= 0 and isnull(@allow,1) = 0
begin



	select 
		@total = isnull(count(email),0)
	from 
		b_utentes(nolock)
	where  
		email = @email
		and inactivo = 0
   
end


   if(@total>0 and isnull(@allow,1) = 0)
    begin
	 select getdate() AS DateMessag, 0 As StatMessag, 'Email repetido para o cliente' As DescMessag
    end
	else begin
	  select getdate() AS DateMessag, 1 As StatMessag, 'Email valido para o cliente' As DescMessag
	end

GO
GRANT EXECUTE on dbo.up_utentes_verifica_email TO PUBLIC
GRANT Control on dbo.up_utentes_verifica_email TO PUBLIC
GO