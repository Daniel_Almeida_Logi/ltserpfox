
/****** Object:  UserDefinedFunction [dbo].[splitstring]    Script Date: 25/03/2020 17:15:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*

função para repartir uma string em partes especificando a string e o símbolo com o qual a dividir

ex: SELECT * FROM dbo.splitstring('91,12,65,78,56,789', ',')


O SQL 2016 tem a função STRING_SPLIT(string, simbolo) mas para funcionar tem de ter o codigo compatibilidade SQL pelo menos a 130.
A DB ltdev tem o modo de compatibilidade 100, não sei se a alteração provocará danos.


12/02/2020

*/
if OBJECT_ID('[dbo].[splitstring]') IS NOT NULL
    DROP FUNCTION [dbo].[splitstring]
GO




CREATE FUNCTION [dbo].[splitstring] ( @stringToSplit VARCHAR(MAX), @simboloToSplit VARCHAR(1) )
RETURNS
 @returnList TABLE ([value] [nvarchar] (500))
AS
BEGIN

 DECLARE @name NVARCHAR(255)
 DECLARE @pos INT

 WHILE CHARINDEX(@simboloToSplit, @stringToSplit) > 0
 BEGIN
  SELECT @pos  = CHARINDEX(@simboloToSplit, @stringToSplit)  
  SELECT @name = SUBSTRING(@stringToSplit, 1, @pos-1)

  INSERT INTO @returnList 
  SELECT @name

  SELECT @stringToSplit = SUBSTRING(@stringToSplit, @pos+1, LEN(@stringToSplit)-@pos)
 END

 INSERT INTO @returnList
 SELECT @stringToSplit

 RETURN
END
GO


