/*


PESQUISA CLIENTES DETALHE
J.Gomes, 18-03-2020

Pesquisa detalhe a partir de 5 campos de pesquisa:
	ncont,bino,no_ext ,no ,estab 

O campo inactivo=0 é utilizado na pesquisa.
O resultado será sempre uma linha, top 1

Output dos seguintes campos:
	 nome AS name, email, nascimento AS dob
	, CASE WHEN (nascimento IS NULL or nascimento = '1900-01-01 00:00:00.000') then 0 
		else DATEDIFF(hour,nascimento,GETDATE())/8766 END AS age
	, sexo AS gender, tipo AS type
	, altura AS height, peso AS weight, profi AS job, codigop As countryCode, obs
	, dbo.ListaEstab(no) AS deps
	, no, estab, no_ext AS extId, noAssociado As assocNr, id AS assocId, nrcartao AS cardNumber
	, ncont AS vatNr, nbenef AS medicalNr, nrss AS ssNr, bino AS ccNr, tlmvl AS cellphone, telefone AS phone, fax
	, local AS city, morada AS address, codpost AS zipCode, zona AS zone, codigop AS countryCode
	, nocredit AS noCredit, naoencomenda AS noOrder, desconto AS discount, moeda AS currency
	, nib AS iban, codSwift AS swift, cativa AS vatCat, perccativa AS vatPerc, autoriza_sms AS smsAuth
	, autoriza_emails AS emailAuth, codpla AS codPla, despla AS descPla, ousrdata AS creationDate
	, usrdata AS lastChangeDate

FUNÇAO a criar dbo.ListaEstab(no):
	Um dos campos a mostrar, "deps", é o resultado de concatenação dos diferentes valores da variavel "estab"
	para o mesmo valor de "no" nas diferentes linhas, separado por virgula.
	Para tal foi criada a função escalar dbo.ListaEstab(no) com input "no" e retorno de uma lista.

	ex: no=1078970 tem 3 linhas

	CODE:
	select no, estab, dbo.ListaEstab(no) AS deps from b_utentes where no=1078970

	RESULT:
	no			estab	deps
	1078970		0		0,1,2
	1078970		1		0,1,2
	1078970		2		0,1,2


PROCEDURE
Criada procedure up_utentes_PesquisaClientesDetalhe com parâmetros 

Parametros: up_utentes_PesquisaClientesDetalhe 'ncont','bino','no_ext','no','estab'

exec up_utentes_PesquisaClientesDetalhe '000000000','','','208058','0',''

exec up_utentes_PesquisaClientesDetalhe null,'','','208058','0',NULL




'
exec up_utentes_PesquisaClientesDetalhe NULL,'','',NULL,'0',NULL


exec up_utentes_PesquisaClientesDetalhe NULL,'','','107831406','0',NULL
*/




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



if OBJECT_ID('[dbo].[up_utentes_PesquisaClientesDetalhe]') IS NOT NULL
    drop procedure up_utentes_PesquisaClientesDetalhe
go

CREATE PROCEDURE up_utentes_PesquisaClientesDetalhe
	@ncont varchar(20), 
	@bino varchar(20), 
	@no_ext varchar(20), 
	@no varchar(10),
	@estab varchar(3), 
	@email varchar(100)

AS

DECLARE @sqlCommand VARCHAR(5000) 
DECLARE @utstamp VARCHAR(25)
DECLARE @pontosCartao INT 
DECLARE @valorCartao NUMERIC(19,2) 
DECLARE @nid_Validade DATETIME = '1900-01-01'
DECLARE @nid_ValidadeText VARCHAR(10)
DECLARE @saldoCC NUMERIC(19,2)
DECLARE @atraso int 
DECLARE @nReservas int
DECLARE @vSuspensas NUMERIC(19,5) 
DECLARE @valorVales NUMERIC (20,3)
DECLARE @nrCartao VARCHAR(50)
DECLARE @eplafond NUMERIC(19,6)
DECLARE @credDisp NUMERIC(19,2)
DECLARE @siteId		VARCHAR(100) = ''

SET @ncont = isnull(@ncont,'')

SET @bino = isnull(@bino,'')
SET @no_ext =isnull(@no_ext,'')
--campos numericos na tabela, não permitir input de nao numericos
SET @no = isnull(@no,0)
SET @estab = isnull(@estab,'')
SET @email =isnull(@email,'')
--não mostrar dados caso não se preencha nenhum dos campos: len(@ncont+@bino+@no_ext+@no+@estab)=0


if(len(@estab)>0 and @no<=0)
	set @no = 'xxxxx'


select top 1 @siteId = id_lt from empresa(nolock) order by no asc


IF (@no='' OR @no NOT LIKE '%[^0-9]%') AND (@estab='' OR @estab NOT LIKE '%[^0-9]%') AND (len(@ncont+@bino+@no_ext+@no+@estab+@email)>0 )
BEGIN
	select TOP 1 @utstamp = utstamp , @no=no,@estab=estab,@eplafond=eplafond FROM dbo.b_utentes (NOLOCK)		
	where ( 
			ncont = COALESCE(NULLIF(@ncont,''), ncont) 
			AND bino = COALESCE(NULLIF(@bino,''), bino) 
			AND no_ext = COALESCE(NULLIF(@no_ext,''), no_ext) 
			AND email = COALESCE(NULLIF( @email,''), email) 
			AND (no = case when @no>0 then  COALESCE(NULLIF(@no,''), no)  else no end
				AND estab = COALESCE(NULLIF(@estab ,''), estab))
			)

			

	SELECT  top 1 @pontosCartao = isnull(b_fidel.pontosAtrib,0) - isnull(b_fidel.pontosUsa,0), @nrCartao=b_utentes.nrcartao  
	FROM b_fidel (NOLOCK)
	inner join b_utentes(nolock) on b_utentes.utstamp = b_fidel.clstamp
	where clstamp=@utstamp	and b_fidel.nrcartao = b_utentes.nrcartao

	SELECT  top 1 @valorCartao = 
			case when 
					isnull(b_fidel.valcartao,0)- isnull(b_fidel.valcartao_cativado,0) <= 0 
				then 
					0
				else   
					isnull(b_fidel.valcartao,0)- isnull(b_fidel.valcartao_cativado,0)
				end
				FROM b_fidel (NOLOCK)
				inner join b_utentes(nolock) on b_utentes.utstamp = b_fidel.clstamp
				where clstamp=@utstamp	and b_fidel.nrcartao = b_utentes.nrcartao

	SELECT  top 1 @nid_Validade= isnull(validade_fim,'19000101') FROM dispensa_eletronica_cc (NOLOCK) where utstamp=@utstamp
	SELECT         @saldoCC= Round(ISNULL(sum(edeb - edebf) - sum(ecred - ecredf),0),2)  from cc (nolock) WHERE no=@no and estab=@estab
	SELECT         @atraso = ISNULL(AVG(DATEDIFF(DAY,dataven ,GETDATE())),0) from cc(nolock) WHERE (edeb != edebf or ecred != ecredf) and dataven < GETDATE() and no=@no and estab=@estab and cmdesc not like '%N/Recibo%'
	SELECT         @nReservas= ISNULL(COUNT(*),0) from bo (NOLOCK) where nmdos ='Reserva de Cliente' AND fechada=0 AND NO=@no and estab=@estab


	SELECT			@vSuspensas=isnull(Round(ISNULL(SUM(etiliquido),0),2),0) from FT (NOLOCK)
					inner join fi(nolock) on ft.ftstamp = fi.ftstamp 
					where  COBRADO=1 AND NO=@no and estab=@estab and fi.qtt > isnull((select top 1 isnull(qtt,0) from fi fi1(nolock) where fi1.ofistamp=fi.fistamp and fi.fistamp!=''),0)
	



	IF (@nrCartao ='')
	BEGIN 
	    SELECT @valorVales = ISNULL(valor,0) from b_fidelVale (nolock) where b_fidelVale.clstamp = @utstamp and b_fidelVale.anulado = 0 and b_fidelVale.abatido = 0 and CONVERT(varchar, B_fidelvale.ousrdata, 112)<CONVERT(varchar, getdate(), 112)
	END
	ELSE 
	BEGIN 
		SELECT @valorVales = SUM(valor) from b_fidelVale (nolock) where b_fidelVale.clstamp = @utstamp and b_fidelVale.anulado = 0 and b_fidelVale.abatido = 0 and CONVERT(varchar, B_fidelvale.ousrdata, 112)<CONVERT(varchar, getdate(), 112)
	END



	/*
	SELECT  nome AS name, email, nascimento AS dob
	, CASE WHEN (nascimento IS NULL or nascimento = '1900-01-01 00:00:00.000') then 0 
		else DATEDIFF(hour,nascimento,GETDATE())/8766 END AS age
	, sexo AS gender, tipo AS type
	, altura AS height, peso AS weight, profi AS job, codigop As countryCode, obs
	, dbo.ListaEstab(no) AS deps
	, no, estab, no_ext AS extId, noAssociado As assocNr, id AS assocId, nrcartao AS cardNumber
	, ncont AS vatNr, nbenef AS medicalNr, nrss AS ssNr, bino AS ccNr, tlmvl AS cellphone, telefone AS phone, fax
	, local AS city, morada AS address, codpost AS zipCode, zona AS zone, codigop AS countryCode
	, nocredit AS noCredit, naoencomenda AS noOrder, desconto AS discount, moeda AS currency
	, nib AS iban, codSwift AS swift, cativa AS vatCat, perccativa AS vatPerc, eplafond as plafond
	, autoriza_sms AS smsAuth
	, autoriza_emails AS emailAuth, codpla AS codPla, despla AS descPla, ousrdata AS creationDate
	, usrdata AS lastChangeDate 
	FROM dbo.b_utentes (nolock) where ( 
		ncont = COALESCE(NULLIF(@ncont,''), ncont) 
		AND bino = COALESCE(NULLIF(@bino,''), bino) 
		AND no_ext = COALESCE(NULLIF(@no_ext,''), no_ext) 
		AND (no = COALESCE(NULLIF(@no,''), no) AND estab = COALESCE(NULLIF(@estab,''), estab))
		) AND inactivo = 0
	--AND no=1078970	
	ORDER by ousrdata desc
	*/

	set @nid_ValidadeText = CONVERT(varchar(10), @nid_Validade,120)
	set @credDisp = Round(ISNULL(@eplafond,0) - ISNULL(@saldoCC,0),2) 

	SET @sqlCommand = 'SELECT top 1 nome AS name, email, convert(varchar,nascimento,23) AS dob
	, CASE WHEN (nascimento IS NULL or nascimento = ''' + '1900-01-01 00:00:00.000' +''') then 0 
		else DATEDIFF(hour,nascimento,GETDATE())/8766 END AS age
	, sexo AS gender, rtrim(ltrim(tipo)) AS type, ROUND(altura,2) AS height, Round(peso,2) AS weight, rtrim(ltrim(profi)) AS job, rtrim(ltrim(codigop)) As countryCode, obs
	, dbo.ListaEstab(no) AS deps
	, case when isnull(altura,0) > 0 and ISNULL(peso,0)> 0 then ISNULL(ROUND(peso / (altura * altura),2), NULL)  else 0 end    AS imc
	, no As number, estab As dep, no_ext AS extId, rtrim(ltrim(noAssociado)) As assocNr, rtrim(ltrim(id)) AS assocId, rtrim(ltrim(nrcartao)) AS cardNumber
	, rtrim(ltrim(ncont)) AS vatNr, rtrim(ltrim(nbenef)) AS medicalNr, nrss AS ssNr, rtrim(ltrim(bino)) AS ccNr, tlmvl AS cellphone, telefone AS phone, fax
	, rtrim(ltrim(local)) AS city, rtrim(ltrim(morada)) AS address, rtrim(ltrim(codpost)) AS zipCode, rtrim(ltrim(zona)) AS zone, rtrim(ltrim(codigop)) AS countryCode
	, rtrim(ltrim(descp)) AS countryName
	, nocredit AS noCredit, naoencomenda AS noOrder, desconto AS discount, rtrim(ltrim(moeda)) AS currency
	, nib AS iban, codSwift AS swift, cativa AS vatCat, perccativa AS vatPerc, eplafond as plafond
	, autoriza_sms AS smsAuth, autoriza_emails AS emailAuth, codpla AS codPla, despla AS descPla
	, convert(varchar,ousrdata,23) AS OperationInfo_CreationDate,   convert(varchar,usrdata,23)  AS OperationInfo_LastChangeDate
	, convert(varchar(8),usrhora,24) as OperationInfo_LastChangeTime, convert(varchar(8),ousrhora,24) as OperationInfo_CreationTime
	, usrinis AS OperationInfo_LastChangeUser, convert(varchar(10),isnull(valipla2,''1900-01-01''),23) as nbeValidity
	, '+ CONVERT(VARCHAR(30),ISNULL(@pontosCartao,0)) +'							AS cardPoints
	, '+ CONVERT(VARCHAR(30),ISNULL(@valorCartao,0)) +'							    AS cardMoney
	, ' + CONVERT(varchar(30),Round(ISNULL(@valorVales,0),2)) +'					AS couponsValue
	, ' + CONVERT(varchar(10),ISNULL(@nReservas,0))	+'								AS nReservations
	,  '+ CONVERT(varchar(20),ISNULL(@vSuspensas,0))		+'						AS vSuspensions
	, '''+ @nid_ValidadeText		+'''											AS nidValidity
	, '+ CONVERT(varchar(10),ISNULL(@saldoCC,0))		+'							AS balanceCC
	, '+ CONVERT(varchar(20),ISNULL(@credDisp,0))		+'	 						AS availableCredit
	, nbenef2															            AS nbe
	, ''''																            AS cardSystem
	, '+ CONVERT(varchar(10),ISNULL(@atraso,0))+'						            AS delay
	, ''dia''																		AS delayUnit
	, case when rtrim(ltrim(password)) ='''' then 0 else 1 end                      AS hasPassword	
--	, ltrim(rtrim(password))                                                        AS password		
	, username                                                                      AS username
	, inactivo                                                                      AS inactive		
	, isnull(soleitura,0)                                                           AS readOnly
	, case when inactivo = 1 then 0 else  isnull(autorizasite,0) end				AS authorized
	, obito																			AS isDeath
	, hablit																		AS qualifications
	, autoriza_sms																	AS allowSms
	, autoriza_emails																AS allowEmails
	, ISNULL(recmtipo,'''')															AS prescritionType
	,rtrim(ltrim(nome2))															AS abrev
	,rtrim(ltrim(utstamp))															AS regstamp
	, '''+ rtrim(ltrim(@siteId))+'''												AS siteId
	FROM dbo.b_utentes (NOLOCK)
	where ( 
			utstamp ='''+ ISNULL(@utstamp,'') +'''
			) 
	ORDER by ousrdata desc'
	PRINT @sqlCommand 
	EXEC (@sqlCommand)

END

ELSE
BEGIN
	print('Tem de preencher pelo menos um campo de pesquisa com valores numéricos')
END



GO
Grant Execute On up_utentes_PesquisaClientesDetalhe to Public
Grant Control On up_utentes_PesquisaClientesDetalhe to Public
go



/*

-- Codigo para criação de função escalar com input do campo "no" e retorno de uma lista
-- com base na tabela b_utentes
-- select dbo.ListaEstab(no)

------------------usando coalesce : escolhida --------------------------
declare @aa varchar (5000)
set @aa = ''

select @aa = 
    case when @aa = ''
    then cast(estab AS nvarchar)
    else @aa + coalesce(',' + cast(estab AS nvarchar), '')
    end
  from b_utentes where no=1078900
print @aa


-----------------------usando xml-------------------------

SELECT LEFT(estab, LEN(estab) - 1)
FROM (
    select cast(estab AS nvarchar) + ', '
    FROM b_utentes
	where no=1078900
    FOR XML PATH ('')
  ) c (estab)
   
  
select no, estab, dbo.ListaEstab(no) from b_utentes  where no=1078900
select no, estab from b_utentes where no=1078900

*/