/* SP Pesquisa Verifica morada e faz delete
Jorge Gomes, 2020-07-10
--------------------------------

Pesquisa nas tabelas: 	
deliveryaddress


OUTPUT: 
---------
DateMessag				StatMessag	DescMessag		morada
2020-04-28 11:44:24.370	1			Delivery Address Deleted	06655884-B218-4243-8F34-E6458CED5A4

DateMessag				StatMessag	DescMessag						morada
2020-04-28 11:44:44.777	-1			Delivery Address Not Present	06655884-B218-4243-8F34-E6458CED5A4

DateMessag				StatMessag	ActionMessage	DescMessag	morada
2020-07-10 11:27:04.773	0			0				NO DATA	



EXECUÇÃO DA SP:
---------------
exec up_DeleteMorada '06655884-B218-4243-8F34-E6458CED5A4'
exec up_DeleteMorada '06hh655884-B218-4243-8F34-E6458CED5A4'
exec up_DeleteMorada ''
exec up_DeleteMorada null

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_DeleteMorada]') IS NOT NULL
	DROP PROCEDURE [dbo].up_DeleteMorada;
GO

CREATE PROCEDURE [dbo].[up_DeleteMorada]	
	 @moradaref	varchar(44)
		
	   
/* WITH ENCRYPTION */
AS

SET @moradaref			=   rtrim(ltrim(isnull(@moradaref,'')))

DECLARE @contador			int
DECLARE @moradaref_			varchar(44)

IF len(@moradaref) > 0 
begin
	SELECT @moradaref_= deliveryaddressstamp from deliveryaddress(nolock)	
	where deliveryaddressstamp = @moradaref

	select @contador=@@ROWCOUNT	

	If @contador > 0 
	BEGIN
		BEGIN TRANSACTION  T
			BEGIN TRY
				--select * from deliveryaddress where deliveryaddressstamp = @moradaref;
				delete from deliveryaddress where deliveryaddressstamp = @moradaref;
				select getdate() AS DateMessag, 1 As StatMessag, 'Delivery Address Deleted' As DescMessag, @moradaref_ AS morada;
				COMMIT TRANSACTION T
			END TRY

			BEGIN CATCH
				ROLLBACK TRANSACTION T				
			END CATCH
	END		
	ELSE	
		select getdate() AS DateMessag, -1 As StatMessag, 'Delivery Address Not Present' As DescMessag, @moradaref AS morada;		   	
end
else
begin
	select getdate() AS DateMessag, 0 As StatMessag, 0 AS ActionMessage, 'NO DATA' As DescMessag, @moradaref AS morada;
end

GO
GRANT EXECUTE on dbo.up_DeleteMorada TO PUBLIC
GRANT Control on dbo.up_DeleteMorada TO PUBLIC
GO





