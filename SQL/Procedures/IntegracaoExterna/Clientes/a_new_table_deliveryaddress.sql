
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'deliveryaddress'))
BEGIN   
CREATE TABLE [dbo].[deliveryaddress](
    [utstamp] [varchar](50) NOT NULL,
    [no] [numeric](15, 0) NOT NULL,
    [estab] [numeric](25, 0) NOT NULL,
    [morada] [varchar](200) NULL,
    [localidade] [varchar](150) NULL,
    [codpost] [varchar](200) NULL,
    [zona] [varchar](300) NULL,
    [pais] [varchar](50) NULL,
    [deliveryaddressstamp] [varchar](40) NOT NULL,
 CONSTRAINT [PK_deliveryaddressstamp] PRIMARY KEY CLUSTERED 
(
    [deliveryaddressstamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


 

ALTER TABLE [dbo].[deliveryaddress] ADD  CONSTRAINT [DF_deliveryaddress_utstamp]  DEFAULT ('') FOR [utstamp]


ALTER TABLE [dbo].[deliveryaddress] ADD  CONSTRAINT [DF_deliveryaddress_morada]  DEFAULT ('') FOR [morada]


ALTER TABLE [dbo].[deliveryaddress] ADD  CONSTRAINT [DF_deliveryaddress_localidade]  DEFAULT ('') FOR [localidade]


ALTER TABLE [dbo].[deliveryaddress] ADD  CONSTRAINT [DF_deliveryaddress_zona]  DEFAULT ('') FOR [zona]


ALTER TABLE [dbo].[deliveryaddress] ADD  CONSTRAINT [DF_deliveryaddress_codpost]  DEFAULT ('') FOR [codpost]




ALTER TABLE [dbo].[deliveryaddress] ADD  CONSTRAINT [DF_deliveryaddress_pais]  DEFAULT ('') FOR [pais]

end


DECLARE @constraintName VARCHAR(100),@tableName varchar(50),@columnName varchar(50),@columnType varchar(50),@columnDefault varchar(50)

select @tableName		= 'deliveryaddress'
	,@columnName		= 'deliveryaddressstamp'
	,@columnType		= 'varchar (40)'
	,@columnDefault		= '('''')'
select @constraintName	= (select o.name 
							from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id
							where o.xtype = 'd' and t.name = @tableName and c.name = @columnName
							)
							
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='dbo' 
				and TABLE_NAME=@tableName and COLUMN_NAME=@columnName)
begin		
	if @constraintName is not null
	begin
		EXEC('ALTER TABLE dbo.' + @tableName + ' DROP CONSTRAINT ' + @constraintName)
	end

	if @columnDefault is not null
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType + ' not null ' + 'constraint DF_' + @tableName + '_' + @columnName + ' DEFAULT ' + @columnDefault)
	else
		exec('ALTER TABLE dbo.' + @tableName + ' add ' + @columnName + ' ' + @columnType)
end