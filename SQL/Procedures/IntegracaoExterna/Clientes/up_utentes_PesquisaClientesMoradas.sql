/* SP Pesquisa de Morada de Clientes
2020-04-15, JG

Variaveis de Input (ambas obrigatórias):
--------------------
@no		
@estab


Variaveis de Ouput:
--------------------
DeliveryAdressInfo_ref, DeliveryAdressInfo_number,DeliveryAdressInfo_dep
, DeliveryAdressInfo_city, DeliveryAdressInfo_address, DeliveryAdressInfo_zipCode
, DeliveryAdressInfo_zone,DeliveryAdressInfo_countryCode


Execução da SP:
----------------
	up_utentes_PesquisaClientesMoradas '@number' , '@dep'

	 exec up_utentes_PesquisaClientesMoradas '201' , '0'

	 exec up_utentes_PesquisaClientesMoradas NULL , NULL

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_utentes_PesquisaClientesMoradas]') IS NOT NULL
	drop procedure dbo.up_utentes_PesquisaClientesMoradas
go

CREATE PROCEDURE dbo.up_utentes_PesquisaClientesMoradas
@number				VARCHAR(13),
@dep				VARCHAR(3)

AS

SET @number			=   rtrim(ltrim(@number))
SET @dep			=   rtrim(ltrim(@dep)) 

IF len(@number) > 0 and len(@dep) > 0
BEGIN
	SET NOCOUNT ON		
	
	SELECT 
		deliveryaddressstamp AS DeliveryAddressInfo_ref
		, no AS DeliveryAddressInfo_number
		, estab  AS DeliveryAddressInfo_dep
		, localidade  AS DeliveryAddressInfo_city
		, morada  AS DeliveryAddressInfo_address
		, codpost  AS DeliveryAddressInfo_zipCode
		, zona  AS DeliveryAddressInfo_zone
		, pais  AS DeliveryAddressInfo_countryCode	
	FROM deliveryaddress(nolock)		
	where 
		no = @number
		and estab = @dep	
END
ELSE
BEGIN
	print('Missing number and/or dep')
END


GO
Grant Execute On dbo.up_utentes_PesquisaClientesMoradas to Public
Grant Control On dbo.up_utentes_PesquisaClientesMoradas to Public
GO
