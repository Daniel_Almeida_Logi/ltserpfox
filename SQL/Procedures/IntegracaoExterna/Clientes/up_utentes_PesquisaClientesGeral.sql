/*
PESQUISA CLIENTES GERAL 
J.Gomes, 27-03-2020
V2 - possibilidade de pesquisa sem utilização de Full Text Index

Tipo de pesquisa com apenas uma caixa para especificação dos termos a pesquisar, tipo google search.
A lógica de funcionamento é:
	1) tem espaços entre as palavras: será OR lógico para pesquisa
	2) Tem + entre as palavras : será AND lógico para pesquisa

A pesquisa é feita através de utilização de um de dois métodos:
	1) utilização da pesquisa em Full Text Index, que deverá ser criado para o efeito sobre a tabela
b_utentes
	2) não utilização de Full text index

O método a utilizar é feito de acordo com parâmetros do servidor, isto é, se tem o serviço
Full Text activo ou não.

Os campos de indexação a serem incluídos no full text são (todos tipo char):
		nome, no_str, ncont, tlmvl,tlmvl_seq, telefone, telefone_seq, nrcartao, nrss
		, nbenef, nbenef2, email, no_ext, bino, local, codpost, morada

Existem campos na tabela que por serem inteiros ou por terem espaços entre os caracteres serão duplicados
na tabela como campos automáticos, computorizados, todos do tipo char a serem usados no index:
	no --> no_str
	telefone --> telefone_seq
	tlmvl --> tlmvl_seq

Output dos seguintes campos:
	 nome, no_str AS no, estab, ncont, tlmvl_seq AS tlmvl, telefone_seq As telefone
	, nrcartao, nrss, nbenef, nbenef2, email, no_ext, nascimento,
	CASE WHEN (nascimento IS NULL or nascimento = ''' + '1900-01-01 00:00:00.000' +''' ) then 0 
	else DATEDIFF(hour,nascimento,GETDATE())/8766 END AS Idade, bino,local, codpost, morada, obs

	exec up_utentes_PesquisaClientesGeral null,null,null,6,null,null
	exec up_utentes_PesquisaClientesGeral null,null,null,null,'B331A0D6-9CD2-4556-9A02-DADDF87AFE1E',1,'',1,-1
	exec up_utentes_PesquisaClientesGeral null,null,null,null,'224BB0ED-A2A6-42DB-9689-7F218C64FBF9'
	exec up_utentes_PesquisaClientesGeral '','2020-07-01','19:00:01',null,null


*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



if OBJECT_ID('[dbo].[up_utentes_PesquisaClientesGeral]') IS NOT NULL
    drop procedure up_utentes_PesquisaClientesGeral
go

CREATE PROCEDURE up_utentes_PesquisaClientesGeral
	@SearchString varchar(100) ,
	@lastChangeDate datetime = '19000101', 
	@lastChangeTime varchar(8) = '00:00:00',
	@pageNumber int = 1,
	@orderStamp		VARCHAR(36) = '',
	@status int = 1,
	@type varchar(18) = '',
	@onlyEmail bit = 0,
	@topMax int = 100
  
AS
SET NOCOUNT ON

DECLARE @Simbolo varchar(1)
DECLARE @operador varchar(5)
DECLARE @cursor VARCHAR(8000) 
DECLARE @ccampos VARCHAR(350)  
DECLARE @ctipo_value VARCHAR(100) 
DECLARE @sqlCommand VARCHAR(8000) 
DECLARE @stmt VARCHAR(8000)
DECLARE @where VARCHAR(8000)
DECLARE @orderBy VARCHAR(8000)

set @topMax = ISNULL(@topMax,100)

DECLARE @lastChangeDateTime VARCHAR(20)

DECLARE @IsFullTextInstalled int 
DECLARE @IsFulltextEnabled int
DECLARE @PageSize int = @topMax

if(isnull(@pageNumber,0)<1)
	set @pageNumber = 1


if(isnull(@status,1)!=0)
begin
	set @status = 1
end

if(isnull(@PageSize,0)<1)
	set @PageSize = 100


set @SearchString = isnull(rtrim(ltrim(@SearchString)),'')
set @lastChangeDate = ltrim(rtrim(isnull(@lastChangeDate,'19000101')))
set @lastChangeTime = ltrim(rtrim(isnull(@lastChangeTime,'00:00:00')))
SET @orderStamp	= rtrim(ltrim(ISNULL(@orderStamp,'')))
set @type = rtrim(ltrim(ISNULL(@type,'')))
set @onlyEmail = ISNULL(@onlyEmail,0)




set @lastChangeDateTime = @lastChangeDate + @lastChangeTime



/*
declare @SearchString varchar(100) 
SELECT @SearchString = 'CL23399 214898466 '  
--SELECT CHARINDEX(' ', @SearchString) as ff
--SELECT CHARINDEX('@', @SearchString)
--SELECT CHARINDEX('''', @SearchString)
--SELECT CHARINDEX('"', @SearchString)
*/

--verificar Full text index
SELECT @IsFullTextInstalled = CONVERT(int,SERVERPROPERTY('IsFullTextInstalled'))
SELECT @IsFulltextEnabled = DatabaseProperty(DB_NAME(DB_ID()), 'IsFulltextEnabled')


DECLARE @OrderCri VARCHAR(MAX)=''


IF @orderStamp = ''
BEGIN 
	SET @OrderCri = 'ORDER BY usrdata DESC, usrhora DESC'

END 
ELSE 
BEGIN 
	SELECT @OrderCri='ORDER BY ' + instructionSql  FROM sortingTableOrder (NOLOCK) WHERE stamp = @orderStamp and inativo=0 and nameService='clientSearch'
END 





--, COUNT(*) OVER() / ' + convert(varchar(10),@PageSize) + ' as pageTotal, ' + convert(varchar(10),@PageNumber) + ' as pageNumber

SET @stmt = ' SELECT
			' + convert(varchar(10),@PageSize) +' as pageSize,  (COUNT(*) OVER() / ' +  convert(varchar(10),@PageSize) +') + 1 as pageTotal, '
			+ convert(varchar(10),@PageNumber) +' as pageNumber, COUNT(*) OVER() as linesTotal
            , rtrim(ltrim(nome)) As name, no_str AS number, estab AS dep, rtrim(ltrim(ncont))													AS vatNr
			, rtrim(ltrim(tlmvl_seq)) AS cellphone, rtrim(ltrim(telefone_seq)) As phone, rtrim(ltrim(nrcartao)) AS cardNumber, rtrim(ltrim(nrss)) AS ssNr
			, rtrim(ltrim(nbenef)) AS medicalNr, rtrim(ltrim(email)) as email, rtrim(ltrim(no_ext)) AS extId, convert(varchar,nascimento,23)	AS dob
			, CASE WHEN (nascimento IS NULL or nascimento = ''' + '1900-01-01 00:00:00.000' +''' ) then NULL 
			else DATEDIFF(hour,nascimento,GETDATE())/8766 END																					AS age
			, rtrim(ltrim(bino)) AS ccNr, obs
			, convert(varchar,ousrdata,23) as OperationInfo_creationDate , convert(varchar,usrdata,23)											AS OperationInfo_lastChangeDate
			, convert(varchar(8),usrhora,24) as OperationInfo_lastChangeTime, convert(varchar(8),ousrhora,24)									AS OperationInfo_creationTime
			, usrinis																															AS OperationInfo_LastChangeUser
			, case when rtrim(ltrim(password)) ='''' then 0 else 1 end																			AS hasPassword	
		--	, ltrim(rtrim(password))																											AS password		
			, username																															AS username		
			, inactivo																															AS inactive	
			, case when inactivo = 1 then 0 else  isnull(autorizasite,0) end																											AS authorized
			, tipo																																AS type	
		    , isnull((select top 1 case when isnull(b_fidel.valcartao,0)- isnull(b_fidel.valcartao_cativado,0) <= 0 then 0
										else  
											isnull(b_fidel.valcartao,0)- isnull(b_fidel.valcartao_cativado,0)
										end
										from b_fidel(nolock) 
										where b_fidel.clstamp = b_utentes.utstamp ),0)															AS cardMoney
			,rtrim(ltrim(local)) AS city, rtrim(ltrim(morada))																					AS address
			,rtrim(ltrim(codpost))																												AS zipCode 
			,rtrim(ltrim(zona))																													AS zone
			,rtrim(ltrim(codigop))																												AS countryCode
			,rtrim(ltrim(descp))																												AS countryName
			from b_utentes(nolock) ' 


SET @sqlCommand =  @stmt + ' WHERE ' 
SET @where = '' 


	
set @orderBy =  ' ' + @OrderCri + ' ' +
				'OFFSET ' + convert(varchar(10),@PageSize) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
				 FETCH NEXT  ' + convert(varchar(10),@PageSize) + ' ROWS ONLY '




-- se nao tem pelica deve repartir a string em palavras
IF (SELECT CHARINDEX('''', @SearchString)) = 0 AND (SELECT CHARINDEX('"', @SearchString)) = 0
BEGIN
	
	IF (SELECT CHARINDEX('+', @SearchString)) = 0
	BEGIN
		--não existe o sinal + na string de pesquisa	
		SET @Simbolo = ' '	
		SET @operador = ' OR '
	END
	ELSE
	BEGIN
		--existe o sinal + na string de pesquisa	
		SET @Simbolo = '+'	
		SET @operador = ' AND '
	END

	SELECT value 
		, @operador As operador
		, CASE WHEN ISNUMERIC(value)=1  THEN 'numerico' 	
			 WHEN (SELECT CHARINDEX('@', value)) > 0 THEN 'email' 
			 ELSE 'string'  END AS tipo
		, CASE WHEN ISNUMERIC(value)=1 THEN 'no_str, ncont, nbenef, nbenef2, tlmvl, tlmvl_seq, telefone_seq, nrss, codpost, no_ext, bino' 
				WHEN (SELECT CHARINDEX('@', value)) > 0 THEN 'email' 
				ELSE 'nome, local, morada, telefone, codpost, nrcartao' END AS Campos
	into #searchtmp 
	FROM dbo.splitstring(RTRIM(LTRIM(@SearchString)), @Simbolo);


/*
		select *, RTRIM(LTRIM(value))  from  #searchtmp;
		drop table #searchtmp;		
END
*/
	
	--mantém as diferentes linhas mesmo que sejam do mesmo tipo
	SET @cursor = 'DECLARE col_cursor CURSOR FOR 
							select campos, RTRIM(LTRIM(value)) from #searchtmp' 		

	EXEC (@cursor) 
	OPEN col_cursor    
	FETCH NEXT FROM col_cursor INTO @ccampos, @ctipo_value 

	--se a DB tiver o Full Text Index instalado e activo
	IF @IsFullTextInstalled = 1 and @IsFulltextEnabled = 1
	BEGIN
		WHILE @@FETCH_STATUS = 0    
		BEGIN    
				IF @where <> '' 
				BEGIN	   
				   SET @where = @where + @operador 			   
				END

				SET @where = @where + ' CONTAINS((' + @ccampos + '), ''"*' + @ctipo_value + '*"'')' 
				FETCH NEXT FROM col_cursor INTO @ccampos, @ctipo_value 		  		   
		END 
	END
	ELSE
		--se a DB NÃO tiver Full Text Index instalado e activo, pesquisa modo tradicional
		-- pesquisa numa só coluna (com todas colunas concatenadas)
		BEGIN
			WHILE @@FETCH_STATUS = 0    
			BEGIN    
					IF @where <> '' 
					BEGIN	   
					   SET @where = @where + @operador 			   
					END

					SET @where = @where + replace(replace(@ccampos,'obs,',''),',','+ ''/'' + ') + ' like ''%' + @ctipo_value + '%''' 
					FETCH NEXT FROM col_cursor INTO @ccampos, @ctipo_value 		  		   
			END 
		END

	CLOSE col_cursor    
	DEALLOCATE col_cursor


	SET @sqlCommand = @sqlCommand + ' (' + @where + ') and  inactivo = case when isnull(' + convert(varchar(10),@status) + ',1)= 1 then 0 else inactivo end  and 
	 convert(datetime,convert(varchar(10),usrdata,120) + '' '' + usrhora) >= ''' + @lastChangeDateTime + ''''


	if(ltrim(rtrim(@type))!='')
		SET @sqlCommand = @sqlCommand +  ' and tipo= ''' + ltrim(rtrim(@type)) +''''


	
	if(ISNULL(@onlyEmail,0)=1)
		SET @sqlCommand = @sqlCommand +  ' and len(email)>0 '



	SET @sqlCommand = @sqlCommand  + @orderBy

--	print @sqlCommand
	
	EXEC (@sqlCommand)	


	drop table #searchtmp;		
END
ELSE
BEGIN
 
	-- existe o sinal ' ou " na string de pesquisa, logo pesquisa sem repartir	
	SET @ccampos = ' nome, no_str, ncont, tlmvl_seq, telefone_seq, nrcartao, nrss
					, nbenef, email, no_ext, bino, local, codpost, morada ' 
	
	--se a DB tiver o Full Text Index instalado e activo
	IF @IsFullTextInstalled = 1 and @IsFulltextEnabled = 1
	BEGIN
		SET @where = @where + ' CONTAINS((' + @ccampos + '), ''"*' + REPLACE(REPLACE(@SearchString,'"',''),'''','') + '*"'')  and inactivo = 0  and   convert(datetime,convert(varchar(10),usrdata,120) + '' '' + usrhora) >= ''' + @lastChangeDateTime + ''''
	END
	ELSE
		BEGIN
			--SET @where = @where + ' ( ' + replace(replace(@ccampos,',','+ ''/'' + '),'obs','convert(varchar,obs)') + ' like ''%' + REPLACE(REPLACE(@SearchString,'"',''),'''','') + '%'')  and inactivo = 0' 
			SET @where = @where + ' ( ' + replace(@ccampos,',','+ ''/'' + ') + ' like ''%' + REPLACE(REPLACE(@SearchString,'"',''),'''','') + '%'')  and inactivo = 0  and   convert(datetime,convert(varchar(10),usrdata,120) + '' '' + usrhora)>= ''' + @lastChangeDateTime + ''''  
		END	

	if(ltrim(rtrim(@type))!='')
		SET @where = @where +  ' and tipo= ''' + ltrim(rtrim(@type)) +''''


	if(ISNULL(@onlyEmail,0)=1)
		SET @where = @where +  ' and len(email)>0 '



	SET @sqlCommand = @sqlCommand + @where + @orderBy

	print @sqlCommand
	EXEC (@sqlCommand)	
END


 

GO
Grant Execute On up_utentes_PesquisaClientesGeral to Public
Grant Control On up_utentes_PesquisaClientesGeral to Public
GO	




