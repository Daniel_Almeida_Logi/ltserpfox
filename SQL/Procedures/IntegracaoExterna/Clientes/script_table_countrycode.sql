/****** Object:  Table [dbo].[countrycode]    Script Date: 25/03/2020 17:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'countrycode'))
BEGIN   

CREATE TABLE [dbo].[countrycode](
	[Country_Name] [nvarchar](50) NOT NULL,
	[ISO2] [nvarchar](50) NOT NULL,
	[ISO3] [nvarchar](50) NOT NULL,
	[Top_Level_Domain] [nvarchar](50) NULL,
	[FIPS] [nvarchar](50) NOT NULL,
	[ISO_Numeric] [nvarchar](50) NOT NULL,
	[GeoNameID] [int] NULL,
	[E164] [nvarchar](50) NOT NULL,
	[Phone_Code] [nvarchar](50) NOT NULL,
	[Continent] [nvarchar](50) NOT NULL,
	[Capital] [nvarchar](50) NULL,
	[Time_Zone_in_Capital] [nvarchar](50) NOT NULL,
	[Currency] [nvarchar](50) NULL,
	[Language_Codes] [nvarchar](100) NULL,
	[Languages] [nvarchar](500) NULL,
	[Area_KM2] [nvarchar](50) NOT NULL,
	[Internet_Hosts] [int] NULL,
	[Internet_Users] [int] NULL,
	[Phones_Mobile] [int] NULL,
	[Phones_Landline] [nvarchar](50) NULL,
	[GDP] [float] NULL
) ON [PRIMARY]


END
