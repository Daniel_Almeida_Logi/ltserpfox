/* SP Pesquisa Verifica Nif
Daniel Almeida, 2021-01-22
--------------------------------

Pesquisa nas tabelas: 	
utentes 

OUTPUT: 
---------
DateMessag				StatMessag	DescMessag		
2020-04-28 11:08:52.610	1			Nif valido	

DateMessag				StatMessag	DescMessag			
2020-04-28 11:07:38.063	0			Nif invalido	


EXECUÇÃO DA SP:
---------------
exec up_utentes_verifica_nif 198287981,0,'227956486'


select * from b_utentes  where  no= 215
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].up_utentes_verifica_nif') IS NOT NULL
	DROP PROCEDURE [dbo].up_utentes_verifica_nif;
GO

CREATE PROCEDURE [dbo].up_utentes_verifica_nif	
	 @number	int
	,@dep		int
	,@vatNr		VARCHAR(60)
	
	   
/* WITH ENCRYPTION */
AS

SET @number				=   isnull(@number,-1)
SET @dep				=   isnull(@dep,-1)
SET @vatNr				=   left(rtrim(ltrim(isnull(@vatNr,''))),20)

declare @total int = 0


declare @country as varchar(20) = 'Portugal'

set @country = (select top 1 isnull(textValue,'Portugal') from B_Parameters_site(nolock) where stamp='ADM0000000050')

set @country = lower(@country)

if(@vatNr='' or @country!='portugal')
begin
	select getdate() AS DateMessag, 1 As StatMessag, 'Nif não existe' As DescMessag
	return
end
/* criacao */
IF @number  <= 0
begin

	select 
		@total = isnull(count(ncont),0)
	from 
		b_utentes(nolock)
	where  
		ncont = @vatNr
		and inactivo = 0
		and ncont not like '999999%'
   
end
else begin
	/* edição */
	select 
		@total = isnull(count(ncont),0)
	from 
		b_utentes(nolock)
	where  
		ncont = @vatNr
		and inactivo = 0
		and ncont not like '999999%'
		and no!=@number
	
end

   if(@total=0)
    begin
	 select getdate() AS DateMessag, 1 As StatMessag, 'Nif valido para o cliente' As DescMessag
    end
	else begin
	  select getdate() AS DateMessag, 0 As StatMessag, 'Nif invalido para o cliente' As DescMessag
	end

GO
GRANT EXECUTE on dbo.up_utentes_verifica_nif TO PUBLIC
GRANT Control on dbo.up_utentes_verifica_nif TO PUBLIC
GO