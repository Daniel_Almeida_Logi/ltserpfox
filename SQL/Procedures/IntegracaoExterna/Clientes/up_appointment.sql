/* SP Pesquisa marcações
J.Gomes, 2020-07-17
--------------------------------





EXECUÇÃO DA SP:
---------------
Parametros: up_Appointment 'no','estab', 'site', data, page, orderstamp

2020-07-15 00:00:00.000

exec up_Appointment '215','0','Loja 1','','',1, null, null, null, null, null
exec up_Appointment '215','0','Loja 1',null,'',1, null, null, null, null, null
exec up_Appointment '215','0','Loja 1',null,'MARCADO',1, null, null, null, null, null
exec up_Appointment null,null,'Loja 1','2020-07-15','',null, null, '123456780', null, null, null
exec up_Appointment null,null,'Loja 1','2020-07-15','MARCADO',1, null, null, null, null, 'filiparesende@logitools.pt'
exec up_Appointment null,null,'Loja 1','',null,1,'E8AB2CF8-2480-463B-AF04-CCDFDDDAE01B', null, null, null, 'filiparesende@logitools.pt'
exec up_Appointment '215','0','Loja 1','','',1,'C243D5DD-8AF9-4846-B43C-BC065D062FBE', null, null, null, null
exec up_Appointment '215','0','','','',1,'E8AB2CF8-2480-463B-AF04-CCDFDDDAE01B', null, null, null, null
exec up_Appointment '215','0','',null,null,null,null, null, null, null, null


@orderstamp (ltdev30)
data  : E8AB2CF8-2480-463B-AF04-CCDFDDDAE01B
data desc: C243D5DD-8AF9-4846-B43C-BC065D062FBE


*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Appointment]') IS NOT NULL
	DROP PROCEDURE [dbo].up_Appointment ;
GO

CREATE PROCEDURE [dbo].[up_Appointment]

	  @no				varchar(10)
	, @estab			varchar(3)
	, @site				varchar(50) = ''
	, @date				varchar(10)  = ''
	, @estado			varchar(50)
	, @pageNumber       int = 1
	, @orderStamp		VARCHAR(36) = ''
	, @ncont			varchar(20)
	, @bino				varchar(20)
	, @no_ext			varchar(20)
	, @email			varchar(50)
	   
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

SET @date			    =   rtrim(ltrim(isnull(@date,'')))
set @estado				=	rtrim(ltrim(ISNULL(upper(@estado),'')))
set @orderStamp         =	rtrim(ltrim(ISNULL(@orderStamp,'')))
--as variaveis de pesquisa de cliente nao precisam disto, tem o coalesce abaixo...

	Select @site = site
	From empresa(nolock)
	where empresa.site = @site

	SET @site = ISNULL(@site,'')


if(isnull(@pageNumber,0)<1)
	set @pageNumber = 1	

DECLARE @sql varchar(max)
DECLARE @orderBy VARCHAR(8000)
DECLARE @PageSize int = 100
DECLARE @OrderCri VARCHAR(MAX)
declare @utstamp varchar(25) = ''

IF @orderStamp = ''
BEGIN 
	SET @OrderCri = 'ORDER BY data desc'
END 
ELSE 
BEGIN 
	SELECT @OrderCri='ORDER BY ' + instructionSql  FROM sortingTableOrder (NOLOCK) WHERE stamp = @orderStamp and inativo=0 and nameService='Appointment'
END


select top 1 @no=isnull(no,0), @estab=isnull(estab,0)	
 from b_utentes (NOLOCK) where ( 
		ncont = COALESCE(NULLIF(@ncont,''), ncont) 
		AND bino = COALESCE(NULLIF(@bino,''), bino) 
		AND no_ext = COALESCE(NULLIF(@no_ext,''), no_ext) 
		AND (no = COALESCE(NULLIF(@no,''), no) AND estab = COALESCE(NULLIF(@estab,''), estab))
		AND email = COALESCE(NULLIF(@email,''), email) 
		)
		AND inactivo = 0
		;


set @orderBy = ' ) X ' + @OrderCri + '  				 
				 OFFSET ' + convert(varchar(10),@PageSize) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
				 FETCH NEXT  ' + convert(varchar(10),@PageSize) + ' ROWS ONLY '

select @sql = N' 
					WITH dados as (
								select mrstamp, data,dataFim,mrno,hinicio,hfim,utstamp,nome,no,estab
									,obs,serieno,serienome,sessao,estado
									,SerieDescricao = convert(varchar(254),LEFT(serienome,245) + '' ['' + convert(varchar,sessao) + '']'')
									,site, ltrim(rtrim(SUBSTRING(combinacao,0, CHARINDEX(''+'',combinacao)))) as  combinacao
									,convert(datetime, convert(varchar(10),data,23) + '' '' + hinicio ) as dateInit
									,convert(datetime, convert(varchar(10),dataFim,23) + '' '' + hfim ) as dateEnd
									,rtrim(ltrim(isnull((select isnull(iniciais,'''') from b_us(nolock) where userno = id_us_alt ),''''))) as user_change
									,especialistas = isnull(convert(varchar(254),(select 
														marcacoesRecurso.nome + '', ''
													from 
														marcacoesRecurso (nolock) 
													Where 
														marcacoesRecurso.mrstamp = marcacoes.mrstamp
													FOR XML PATH(''''))),'''')
									,especialidade = isnull(convert(varchar(254),(select 
														B_usesp.especialidade + '', ''
													from 
														marcacoesRecurso (nolock) 
														left join B_usesp (nolock) on B_usesp.userno = marcacoesRecurso.no
													Where 
														marcacoesRecurso.mrstamp = marcacoes.mrstamp
													FOR XML PATH(''''))),'''')
									,id_us,data_cri,id_us_alt,data_alt,nivelurgencia
									,faturado = case when exists (	Select 
																		ltrim(rtrim(mrstamp)) 
																	from 
																		marcacoesServ (nolock) 
																	where 
																		marcacoesServ.mrstamp = marcacoes.mrstamp and fu = 1 
																	) then CONVERT(bit,1) 
															else CONVERT(bit,0) end
									, pvp  = (Select 
													isnull(sum(pvp),0) 
												from 
													marcacoesServ (nolock) 
												where 
													marcacoesServ.mrstamp = marcacoes.mrstamp)
									, compart  = (Select 
													isnull(sum(compart),0) 
												from 
													marcacoesServ (nolock) 
												where 
													marcacoesServ.mrstamp = marcacoes.mrstamp)
									, total  = (Select 
													isnull(sum(total),0) 
												from 
													marcacoesServ (nolock) 
												where 
													marcacoesServ.mrstamp = marcacoes.mrstamp)				
									,motivoCancelamento,talaolevantamento,LocalEntrega,EntregueTLev,designEntrega
									,dataEntrega,userEntrega,dataLevantamento
									,declaracaoPresenca = ''Para os devidos efeitos, declara-se que o Sr(ª). '' + RTRIM(LTRIM(marcacoes.nome)) + '' esteve presente nas nossas instalações no dia '' + left(CONVERT(varchar,marcacoes.data,126),10) 
									+ '' das '' + marcacoes.hinicio + '' às '' + LEFT(CONVERT(varchar,getdate(),108),5) + '' a fim de: ''
								from 
									marcacoes (nolock)		
								where
									 
									(no = COALESCE(NULLIF(''' + @no + ''',''''), no) AND estab = COALESCE(NULLIF(''' + @estab + ''',''''), estab))
									AND site = COALESCE(NULLIF(''' + @site + ''',''''), site)		
										
					)


			SELECT '  + convert(varchar(10),@PageSize) +' as pageSize,  (COUNT(*) OVER() / ' +  convert(varchar(10),@PageSize) +') + 1 as pageTotal, '
				+ convert(varchar(10),@PageNumber) +' as pageNumber, COUNT(*) OVER() as linesTotal		
			, isnull(combinacao,'''') as designation
			, pvp as basePrice
			, compart as coPaymentValue
			, total as retailPrice
			, convert(varchar(10), data,23)  as dateInit
			, hinicio as hourInit
			, isnull(mrstamp,'''') as id
			, isnull(estado,'''') as status
			, convert(varchar(10), data_cri,23) as data_cri
			, convert(varchar(10), data_alt,23) as data_alt
			, convert(varchar(8), data_cri,24) as hora_cri
			, convert(varchar(8), data_alt,24) as hora_alt	
			, DATEDIFF(minute, dateInit, dateEnd) as dateDiff
			, isnull(site,'''') as site
			, ''min'' as timeUnit
			, user_change as lastUser

			FROM (
					select 
					*
					 from dados	 	
					 where 1=1 

					'				

if @date != '' 
	select @sql = @sql + N' AND ''' + @date + '''  BETWEEN data and datafim '

if @estado != '' 
	select @sql = @sql + N' AND upper(estado) = ''' + @estado + '''   '


select @sql = @sql + @orderBy
	   
--print @sql
EXECUTE (@sql)






GO
GRANT EXECUTE on dbo.up_Appointment TO PUBLIC
GRANT Control on dbo.up_Appointment TO PUBLIC
GO


