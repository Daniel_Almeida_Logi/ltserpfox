/*
PESQUISA CLIENTES CONTA CORRENTE
J.Gomes, 24-03-2020

Pesquisa a partir de 6 campos de pesquisa:
	no ,estab, dataini, datafim, vertodos, loja 

O resultado será sempre um conjunto de linhas de documentos com débitos e créditos e respectivos balanços.
A primeira linha contém o resultado total anterior até à Dataini.


Output dos seguintes campos:
	 datalc AS CreationDate, ousrhora AS CreationTime, no AS Number, dep AS Dep, nrdoc AS DocNumber
	, serie As SerieDesc, cmdesc AS DocDesc
	, COALESCE(NULLIF(edeb,0), edebanterior) As Debit, COALESCE(NULLIF(ecred,0), ecredanterior)  AS Credit
	, (select (SUM(edebanterior) + SUM(edeb)) - (SUM(ecredanterior) + SUM(case when ecred<0 then ecred*(-1) else ecred end)) from #dadosCc a where a.numero <= #dadosCc.numero) AS Balance
	, moeda AS Currency, obs, site


PROCEDURE
Criada procedure up_clientes_PesquisaClientesCC com parâmetros 

Parametros: 
up_clientes_PesquisaClientesCC no, estab, 'dataini', 'datafim', vertodos, 'loja'

EXEC dbo.up_clientes_PesquisaClientesCC 215, 0, '2019-01-24', '2020-03-24', 0, 'Loja 1' ,2 


EXEC dbo.up_clientes_PesquisaClientesCC 215, 0, null, NULL, 0, 'Loja 1' ,0
												
EXEC dbo.up_clientes_PesquisaClientesCC 215, 0, null, NULL, 0, 'Loja 1' ,0,'786A6375-AA0C-475F-9389-497269BE7FFD'
												
EXEC dbo.up_clientes_PesquisaClientesCC 198288077, 0, null, NULL, 0, 'Loja 1' ,0,NULL


*/



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_clientes_PesquisaClientesCC]') IS NOT NULL
    drop procedure up_clientes_PesquisaClientesCC
go

CREATE PROCEDURE up_clientes_PesquisaClientesCC
	@no				NUMERIC(10),
	@estab			NUMERIC(4),
	@dataIni		datetime,
	@dataFim		datetime,	
	@verTodos		bit,
	@loja			varchar(20),
	@pageNumber		int = 1,
	@orderStamp		VARCHAR(36) = ''

AS
SET NOCOUNT ON



	SET @estab		= isnull(@estab,0)
	SET @dataIni	= isnull(@dataIni,'19000101')
	SET @dataFim	= isnull(@dataFim,'30000101')
	SET @verTodos	= isnull(@verTodos,0)
	SET @loja		= isnull(@loja,'Loja 1')
	SET @orderStamp	= rtrim(ltrim(ISNULL(@orderStamp,'')))


	DECLARE @PageSize int = 100
	DECLARE @sql varchar(max) =''
	DECLARE @orderBy VARCHAR(8000)

	if(isnull(@pageNumber,0)<1)
		SET @pageNumber = 1

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosCc'))
		DROP TABLE #dadosCc

	DECLARE @OrderCri VARCHAR(MAX) =''

	IF @orderStamp = ''
	BEGIN 
		SET @OrderCri = 'ORDER BY usrdata DESC, usrhora DESC'
	END 
	ELSE 
	BEGIN 
		SELECT @OrderCri='ORDER BY ' + instructionSql  FROM sortingTableOrder (NOLOCK) WHERE stamp = @orderStamp and inativo=0 and nameService='clientCC'
	END 

	SET @orderBy = ' ) X ' + @OrderCri +'  
				 OFFSET ' + convert(varchar(10),@PageSize) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
				 FETCH NEXT  ' + convert(varchar(10),@PageSize) + ' ROWS ONLY '


	select 
		datalc=''
		,ousrhora=''
		,dataven=''
		,serie=''
		,cmdesc='SALDO ANTERIOR'
		,nrdoc=0
		,edeb=0
		,ecred=0
		,ecredf = 0
		,edebf = 0
		,obs=''
		,moeda=''
		,ultdoc=''
		,intid=0
		,cbbno=0
		,username = ''
		,ccstamp = ''
		,no = rtrim(ltrim(b_utentes.no))
		,nome = rtrim(ltrim(b_utentes.nome))
		,facstamp=''
		,verdoc=0
		,ousrinis=''
		,saldoanterior = (select sum(a.edeb)- sum(a.ecred) from cc a (nolock) where b_utentes.no = a.no and a.estab = (case when @verTodos=1 then b_utentes.estab else @estab end) and a.datalc < @Dataini)
		,edebanterior = (select sum(a.edeb) from cc a (nolock) where b_utentes.no = a.no and a.estab = (case when @verTodos=1 then b_utentes.estab else @estab end) and a.datalc < @Dataini)
		,ecredanterior = (select sum(a.ecred) from cc a (nolock) where b_utentes.no = a.no and a.estab = (case when @verTodos=1 then b_utentes.estab else @estab end) and a.datalc < @Dataini)
		,saldoperiodo =(select sum(a.edeb)- sum(a.ecred) from cc a (nolock) where b_utentes.no = a.no and a.estab = (case when @verTodos=1 then b_utentes.estab else @estab end) and a.datalc between @Dataini and @Datafim)
		,site=''
		,dep=0
		,numero=0
		,suspensa=''
		,usrdata='1900-01-01'
		,usrhora='00:00:00' 
	into
		#dadosCc
	From
		b_utentes (nolock)
	Where
		b_utentes.no  = @no
		and b_utentes.estab = 0

	union all

	Select	
		datalc = convert(varchar,cc.datalc,23)
		,ousrhora = cc.ousrhora
		,dataven	= convert(varchar,cc.dataven,23)
		,serie = ISNULL(re.nmdoc,' ')
		,cc.cmdesc
		,cc.nrdoc
		,cc.edeb
		,cc.ecred
		,cc.ecredf
		,cc.edebf
		,cc.obs
		,cc.moeda
		,cc.ultdoc
		,cc.intid
		,cc.cbbno 
		,username = isnull(rtrim(ltrim(b_us.nome)),'')
		,cc.ccstamp
		,cc.no
		,cc.nome
		,cc.faccstamp
		,verdoc = CONVERT(bit,0)
		,cc.ousrinis
		,saldoanterior = 0
		,edebanterior = 0
		,ecredanterior = 0
		,saldoperiodo = 0
		,site = isnull(isnull(ft.site,re.site),'')
		,dep = cc.estab
		,numero = ROW_NUMBER() over(order by cc.datalc desc,cc.ousrhora, cc.cm, cc.nrdoc desc)
		,suspensa= (case when ft.cobrado=1 then 'Vd. Susp.)' else ft.nmdoc end)
		, cc.usrdata
		, cc.usrhora
	From
		b_utentes (nolock)
		left join cc (nolock) on b_utentes.no = cc.no and b_utentes.estab = cc.estab
		left join RE (nolock) ON cc.restamp = re.restamp
		left join b_us(nolock) on b_us.iniciais = cc.ousrinis
		left join ft (nolock) on cc.ftstamp = ft.ftstamp
	WHERE
		cc.no = @no
		And cc.estab=(case when @verTodos=1 then cc.estab else @estab end)
		and cc.datalc between @Dataini and @Datafim
		and (
			ft.site = case when @loja = '' then ft.site else @loja end
			or
			re.site = case when @loja = '' then re.site else @loja end
			or 
			(ft.site is null and re.site is null)
		)


			   
	select @sql = N' SELECT '  + convert(varchar(10),@PageSize) +' as pageSize,  (COUNT(*) OVER() / ' +  convert(varchar(10),@PageSize) +') + 1 as pageTotal, '
				 + convert(varchar(10),@PageNumber) +' as pageNumber, COUNT(*) OVER() as linesTotal		
				, * from (SELECT
				  datalc AS CreationDate, ousrhora AS CreationTime, dep AS Dep, nrdoc AS Number, ltrim(rtrim(serie)) As SerieDesc, ltrim(rtrim(cmdesc)) AS DocDesc
				, COALESCE(NULLIF(edeb,0), edebanterior) As Debit
				, COALESCE(NULLIF(ecred,0), ecredanterior)  AS Credit
				, (select (SUM(edebanterior) + SUM(edeb)) - (SUM(ecredanterior) + SUM(case when ecred<0 then ecred*(-1) else ecred end)) from #dadosCc a where a.numero <= #dadosCc.numero) AS Balance
				, COALESCE(NULLIF(edeb,0), edebanterior) As debitUser
				, COALESCE(NULLIF(ecred * -1 ,0), ecredanterior * -1)  AS creditUser
				--, edebanterior, ecredanterior, saldoanterior
				, moeda AS Currency, obs, site
				, usrdata
				, usrhora
				, nome
				, reg = case when (ecred - ecredf) > 0 OR (edeb - edebf) > 0 then 0 else 1 end
				,case when cmdesc like ''%N/Nt. Crédito%'' then ''CRE''  
				 when cmdesc like ''%N/Nt. Débito%'' then ''DEB''  
				 when cmdesc like ''%N/Recibo%'' then ''REC''  
				 when cmdesc like ''%Desc.Financ.%'' then ''DSC''  
				 when cmdesc like ''%N/Factura%'' then ''FTR''  
				 ELSE ''FTR'' END  AS abrev
			from 
				#dadosCc '

		select @sql = @sql + @orderBy
		
		print @sql
		EXECUTE (@sql)



GO
Grant Execute On up_clientes_PesquisaClientesCC to Public
Grant Control On up_clientes_PesquisaClientesCC to Public
GO


