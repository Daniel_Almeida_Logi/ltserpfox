/*
Insere moradas de envio
D.Almeida, 15-04-2020
revisão 15-04-2020
--------------------------------
Insere registos na tabela [deliveryaddress]
	 no (@number), estab (@dep),  deliveryaddressstamp (@ref)

É validado se o cliente existe pelo @number, @dep
Se sim será actualizada ou criada uma morada de envio consoante o valor do ref

Se a ref estiver preenchida, a morada do cliente será actualizada.
Se a ref não estiver preenchida, será criada uma nova morada de cliente.
Se a ref não existir, será criada uma nova morada de cliente.




A execução passa pelo envio de todas as variáveis que podem assumir 3 estados:
NULL						- significa que não é para fazer nada ao campo (só para o update)
''							- significa que é para colocar o campo sem nada, limpar o campo
'valor' ou valornumerico	- significa que o campo na tabela deve ser alterado para este valor

O processo inicializa pela pesquisa de um conjunto de 2 variáveis, todas preenchidas ou não:
	- no (@number), estab (@dep)  ,  deliveryaddressstamp (@ref)

Perante estas variáveis, será feita uma procura na tabela b_utentes por registos que coincidam
com respectivos valores. 


As duas variáveis no (@number), estab (@dep) se forem preenchidas para pesquisa terão de ser numéricas.

No final do processo são retornados valores após o insert ou update:
	* DateMessag, StatMessag, ActionMessage, DescMessag, ref (deliveryaddressstamp)

StatMessag: 
	1 - OK
	0 - ERROR

ActionMessage/DescMessag: 
	0 - UPDATE
	1 - INSERT
	3 - NO DATA	



ex. no INSERT:
DateMessag				StatMessag	ActionMessage	DescMessag			ref	
2020-04-04 20:06:19.207	1			1				INSERT				ADMXPTO

ex. no UPDATE:
DateMessag				StatMessag	ActionMessage	DescMessag			ref
2020-04-04 20:04:52.017	1			0				UPDATE				ADMXPTO

ex. ERRO, falta o campo dep:
DateMessag				StatMessag	ActionMessage	DescMessag			ref	
2020-04-04 20:01:35.023	0			2				NEED MORE FIELDS	ADMXPTO	

ex. ERRO, não existem registos 
DateMessag				StatMessag	ActionMessage	DescMessag	ref	
2020-04-05 19:58:21.083	0			3				NO DATA		ADMXPTO

ex. ERRO, campo number tem de ser numérico
DateMessag				StatMessag	ActionMessage	DescMessag	ref
2020-04-04 20:24:30.120	0			4				WRONG TYPE	ADMXPTO




-----------------------------------------UPDATE -----------------------------------
UPDATE dos seguintes campos:
	 utstamp AS clStamp
	,no AS number 
	,estab AS dep 
	,deliveryaddressstamp AS ref 
	,morada As address
	,localidade AS city
	,codpost AS zipCode,
	,zona AS zone
	,pais AS countryCode


Ficam fora do update o "estab"  (@dep) e o "no" (@number) pois são a chave.






-----------------------------------------INSERT -----------------------------------
No processo de insert existem duas possibilidades:
	* não é fornecido a variável "deliveryaddressstamp" (@ref)	--> insert com "deliveryaddressstamp" =left(new_id(),35)
	* é indicada a variável "deliveryaddressstamp" (@ref)		--> insert deliveryaddressstamp = @ref, senão exister, senão irá actualizar


-------------------------------------------------------------------------

PROCEDURE
Criada procedure up_utentes_ClientesUpdateInsertMoradaEnvio com parâmetros 

Parametros: 

exec up_utentes_ClientesUpdateInsertMoradaEnvio 'no','estab','morada','localidade','codpost'
,'zona','pais', 'deliveryaddressstamp'


EXECUÇÃO DA SP:
---------------
exec up_utentes_ClientesUpdateInsertMoradaEnvio 206,0,'rua do porto','Lsboa','4401-125'
,'SUL','PT', '9D863D82-D003-4035-AB02-698487CC7AB'


exec up_utentes_ClientesUpdateInsertMoradaEnvio 206,0,'rua do porto','Porto','4400-125'
,'Norte','PT', '0471242B-537B-4FF4-805A-F48D87F5C1B'


exec up_utentes_ClientesUpdateInsertMoradaEnvio NULL,NULL,'rua do porto','Lsboa','4400-125'
,'SUL','PT', NULL

exec up_utentes_ClientesUpdateInsertMoradaEnvio 204,0,NULL,NULL,NULL
,NULL,NULL, NULL





select * from [deliveryaddress]

VERIFICAÇÃO:
------------
select utstamp, usrdata, usrhora
from b_utentes 
where 
no=206 
and estab=0  
and inactivo = 0 

*/



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_ClientesUpdateInsertMoradaEnvio]') IS NOT NULL
    drop procedure up_utentes_ClientesUpdateInsertMoradaEnvio
go

CREATE PROCEDURE up_utentes_ClientesUpdateInsertMoradaEnvio	
	

	 @number			int,		
	 @dep				varchar(3),			

	--variaveis para insert/update
	 @address			varchar(55),
	 @city				varchar(45),	
	 @zipCode			varchar(45),
	 @zone				varchar(20), 
	 @countryCode		varchar(10),
	 @ref				varchar(40)

	
AS


DECLARE @operation		 int = 1 /* 1 - update, 0 - insere */
DECLARE @lastChangeDate		datetime
DECLARE @lastChangeTime		VARCHAR(8)
DECLARE @CreationDate		datetime
DECLARE @CreationTime		VARCHAR(8)
DECLARE @clStamp	     varchar(50) = '' /* stamp do cliente (b_utentes) */

--variaveis para apresentar no final
declare @refFinal varchar(40) = isnull(@ref,'')


SET @lastChangeDate = convert(varchar,getdate(),23)
SET @lastChangeTime = convert(varchar(8),getdate(),24)
SET @CreationDate = convert(varchar,getdate(),23)
SET @CreationTime = convert(varchar(8),getdate(),24)




/* valida se cliente existe */
select
	@clStamp = ltrim(rtrim(utstamp)) 
from 
	b_utentes(nolock)
where
	no = isnull(@number,0) and estab = isnull(@dep,-1)
	AND inactivo = 0




--apenas cria morada de envio se o cliente existir
IF (len(@clStamp)>0) 
		
BEGIN	
	set @ref = ltrim(rtrim(@ref))
	DECLARE @stampRef CHAR(35)  
	SET @stampRef = (select left(newid(),35))

	

	-- valida se já existe a morada de envio
	select
		@operation = count(*)
	from 
		[deliveryaddress](nolock)
	where
		utstamp = @clStamp
		and ltrim(rtrim(deliveryaddressstamp)) = @ref


	

	IF @operation = 0
	BEGIN
		SET @refFinal = @stampRef

		BEGIN TRANSACTION;
			
				
			INSERT INTO [deliveryaddress] (utstamp , no, estab,
					morada, localidade, codpost
					,zona, pais, deliveryaddressstamp
		
					) 
			VALUES (ltrim(rtrim(@clStamp)), @number, @dep
						, isnull(ltrim(rtrim(@address)),''), isnull(ltrim(rtrim(@city)),''), isnull(ltrim(rtrim(@zipCode)),'')
						, isnull(ltrim(rtrim(@zone)),''), isnull(ltrim(rtrim(@countryCode)),''), isnull(ltrim(rtrim(@stampRef)),'')
					);	

			update b_utentes
				set usrdata = @lastChangeDate, usrhora = @lastChangeTime
			where
				no = isnull(@number,0) and estab = isnull(@dep,-1)
				AND inactivo = 0
	

		COMMIT TRANSACTION;	
		--print('----- insert with sucess -----');

		--mensagem final
		select getdate() AS DateMessag, 1 AS StatMessag, 1 AS ActionMessage, 'INSERT' As DescMessag
		, @refFinal AS ref
	END /*fim ciclo insert*/

	ELSE
	
	/*  inicio ciclo update*/
	BEGIN
				print 'antes'
				print  @zone
				print @countryCode

			

				BEGIN TRANSACTION; 	
					UPDATE [deliveryaddress] SET 
					 morada				= COALESCE(rtrim(ltrim(@address)), morada)  
					,localidade			= COALESCE(rtrim(ltrim(@city)), localidade)
					,codpost		    = COALESCE(rtrim(ltrim(@zipCode)), codpost)		
					,zona				= COALESCE(rtrim(ltrim(@zone)), zona)	
					,pais				= COALESCE(rtrim(ltrim(@countryCode)), pais)	

					where 							
						no = @number
						AND estab = @dep
						AND deliveryaddressstamp = rtrim(ltrim(@ref))

						
			   update b_utentes
				set usrdata = @lastChangeDate, usrhora = @lastChangeTime
			   where
				no = isnull(@number,0) and estab = isnull(@dep,-1)
				AND inactivo = 0
					

				COMMIT TRANSACTION;	
				select getdate() AS DateMessag, 1 As StatMessag, 0 AS ActionMessage, 'UPDATE' As DescMessag,  @refFinal AS ref
	END
END
ELSE
BEGIN

	select getdate() AS DateMessag, 0 As StatMessag, 2 AS ActionMessage, 'NO DATA' As DescMessag, @refFinal AS ref
END

            
GO
Grant Execute On up_utentes_ClientesUpdateInsertMoradaEnvio to Public
Grant Control On up_utentes_ClientesUpdateInsertMoradaEnvio to Public
GO
			
           
			
	