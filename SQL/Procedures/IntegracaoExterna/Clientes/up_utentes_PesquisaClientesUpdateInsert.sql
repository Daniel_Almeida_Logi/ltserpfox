/*
PESQUISA CLIENTES UPDATE/INSERT
J.Gomes, 23-03-2020
revisão 05-04-2020
--------------------------------

Pesquisa do registo na tabela b_utentes a partir de 2 campos de pesquisa:
	 no (@number), estab (@dep) 

O campo inactivo=0 é utilizado na pesquisa assim como a situação do ncont ser diferente de '99999999%'.
Este valor é utilizado para clientes que não querem especificar o seu NIF, assim mesmo que 
existam linhas para este contribuinte não devem ser tratadas como tal e deve assumir que se 
quer inserir nova linha.

A execução passa pelo envio de todas as variáveis que podem assumir 3 estados:
NULL						- significa que não é para fazer nada ao campo (só para o update)
''							- significa que é para colocar o campo sem nada, limpar o campo
'valor' ou valornumerico	- significa que o campo na tabela deve ser alterado para este valor

O processo inicializa pela pesquisa de um conjunto de 2 variáveis, todas preenchidas ou não:
	- no (@number), estab (@dep) 

Perante estas variáveis, será feita uma procura na tabela b_utentes por registos que coincidam
com respectivos valores. 

Podem acontecer 2 situações:
	1) É enviado o number (no) e o dep (estab) : faz-se update
		a) Tem dados com mais que um registo	--> mensagem que são necessários mais parâmetros para pesquisa
		b) Não tem dados						--> mensagem de não existência de registo
		b) Tem dados com um só registo			--> pode fazer o UPDATE
	2) Não é enviado o number (no) ou o dep (estab): faz o insert
		a) number (no) NULL ou '' : insere novo registo com number=max(number)+1
		b) tem number, dep NULL ou '': insere novo registo com esse number e dep= max(dep para esse number)+1


As duas variáveis no (@number), estab (@dep) se forem preenchidas para pesquisa terão de ser numéricas.

No final do processo são retornados valores após o insert ou update:
	* DateMessag, StatMessag, ActionMessage, DescMessag, number (no), dep (estab)

StatMessag: 
	1 - OK
	0 - ERROR

ActionMessage/DescMessag: 
	0 - UPDATE
	1 - INSERT
	2 - NEED MORE FIELDS
	3 - NO DATA
	4 - WRONG TYPE
	5 - READ_ONLY


ex. no INSERT:
DateMessag				StatMessag	ActionMessage	DescMessag			number	dep
2020-04-04 20:06:19.207	1			1				INSERT				10731	0

ex. no UPDATE:
DateMessag				StatMessag	ActionMessage	DescMessag			number	dep
2020-04-04 20:04:52.017	1			0				UPDATE				212		14

ex. ERRO, falta o campo dep:
DateMessag				StatMessag	ActionMessage	DescMessag			number	dep
2020-04-04 20:01:35.023	0			2				NEED MORE FIELDS	212	

ex. ERRO, não existem registos 
DateMessag				StatMessag	ActionMessage	DescMessag	number	dep
2020-04-05 19:58:21.083	0			3				NO DATA		600		0

ex. ERRO, campo number tem de ser numérico
DateMessag				StatMessag	ActionMessage	DescMessag	number		dep
2020-04-04 20:24:30.120	0			4				WRONG TYPE	1078j3198	2

ex. ERRO, registo readOnly
DateMessag				StatMessag	ActionMessage	DescMessag	number		dep
2020-04-04 20:24:30.120	0			5				READONLY	1078j3198	2




-----------------------------------------UPDATE -----------------------------------
UPDATE dos seguintes campos:
	 nome AS name, email, nascimento AS dob
	, sexo AS gender, tipo AS type
	, altura AS height, peso AS weight, profi AS job, obs 
	, noAssociado As assocNr, id AS assocId, nrcartao AS cardNumber, nbenef AS medicalNr, nrss AS ssNr
	, tlmvl AS cellphone, telefone AS phone, fax
	, local AS city, morada AS address, codpost AS zipCode, zona AS zone, codigop AS countryCode
	, descp AS countryName
	, nocredit AS noCredit, naoencomenda AS noOrder, desconto AS discount, moeda AS currency
	, nib AS iban, codSwift AS swift, cativa AS vatCat, perccativa AS vatPerc, autoriza_sms AS smsAuth
	, autoriza_emails AS emailAuth, eplafond as plafond
	, usrdata AS lastChangeDate
	, inactivo As inative
	, ncont AS vatNr, bino AS ccNr, no_ext AS extId

Ficam fora do update o "estab"  (@dep) e o "no" (@number) pois são a chave.
O campo usrdata é gravado como sendo o getdate() apenas no update.
Os campos usrdata e ousrdate não são necessários para o update nem para o insert.

O campo countryName é gravado com base no cruzamento do countryCode e da tabela auxiliar: 
	[dbo].[countrycode] 


O Logitools só permite alterações a clientes > 200


-----------------------------------------INSERT -----------------------------------
No processo de insert existem duas possibilidades:
	* não é fornecido a variável "no" (@number)	--> insert com "no" = max(no)+1 e estab=0
	* é indicado a variável "no" (@number)		--> insert no = @number e estab = max(estab)+1 

INSERT dos seguintes campos:
	utstamp 
	, nome, email, nascimento , sexo, tipo, altura , peso, profi
	, obs, no_ext, noAssociado, id, nrcartao, nbenef , nrss, tlmvl, telefone
	, fax, local, morada, codpost, zona, codigop, descp, nocredit, naoencomenda
	, desconto, moeda, nib, codSwift, cativa, perccativa, eplafond
	, autoriza_sms, autoriza_emails, inactivo, ncont, bino, no, estab

O campo utstamp é gravado com base na sua criação naquele momento através de:
	@stampid = (select left(newid(),25))

O campo descp (@countryName) é gravado com base no cruzamento do @countryCode e da tabela auxiliar: 
	[dbo].[countrycode] 

-------------------------------------------------------------------------

PROCEDURE
Criada procedure up_utentes_PesquisaClientesUpdateInsert com parâmetros 

Parametros: 

exec up_utentes_PesquisaClientesUpdateInsert 'ncont','bino','no_ext',no,estab
, 'nome','email', 'nascimento' (yyyymmdd), 'sexo', 'tipo', altura, peso, 'profi', 'obs', noAssociado, 'id' 
, 'nrcartao', 'nbenef', 'nrss'
, 'tlmvl', 'telefone', 'fax', 'local', 'morada', 'codpost', 'zona', 'codigop'
, nocredit (0/1), naoencomenda(0/1), desconto, 'moeda', 'nib', 'codSwift', cativa(0/1), perccativa, eplafond
, autoriza_sms (0/1), autoriza_emails (0/1), inactivo (0/1)


EXECUÇÃO DA SP:
---------------
exec up_utentes_PesquisaClientesUpdateInsert NULL,NULL,NULL,NULL,'10'
,'Jorge Gomes','jorgegomes@logitools.pt', '1972-04-30', 'M', NULL, 1.72, 88, 'IT Prof.', 'novo5', NULL, NULL 
, 'CL23399', '123457643', '67891234565'
, '962988116', '224332061', '224332061', 'Paredes', 'Av. sta Marta', '4585-008', 'Aguiar Sousa', 'PT'
, 1, 1, 45, 'Euro', 'PT222299998888222', NULL, 0, NULL, 5000
, 1, 1, 0, ''


exec up_utentes_PesquisaClientesUpdateInsert null,NULL,NULL,'10783206','2'
, 'fdddf',NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL, NULL 
, NULL, NULL, NULL
, NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL
,NULL,NULL,NULL,NULL,NULL, NULL, NULL, NULL, NULL
, NULL,NULL,NULL,NULL,NULL


select usrdata,usrhora,* from b_utentes where no=10783208 and estab=0

VERIFICAÇÃO:
------------
select  nome, email, nascimento , no, estab, ncont,  sexo, tipo
, altura , peso, profi, obs, no_ext,noAssociado, id, nrcartao, nbenef , nrss
, tlmvl AS cellphone, telefone AS phone, fax, local AS city, morada AS address, codpost AS zipCode
, zona AS zone, codigop AS countryCode, descp as countryname, nocredit AS noCredit, naoencomenda AS noOrder
, desconto AS discount, moeda AS currency, nib AS iban, codSwift AS swift, cativa AS vatCat
, perccativa AS vatPerc, autoriza_sms AS smsAuth, autoriza_emails AS emailAuth,eplafond as plafond
, usrdata AS lastChangeDate, inactivo As inative
from b_utentes 
where 
--ncont='66666' and 
no=600 
and estab=0  
and inactivo = 0 order by ncont

EXEC up_utentes_PesquisaClientesUpdateInsert N'12345116',N'998655478',NULL,10783450,0,N'TESTE João Silva',N'joao.silva@visionsoft.pt',N'1990-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,N'',NULL,NULL,N'351912345',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,N'121313',N'abcdddadadsadas','2021-01-01'

select * from b_utentes where no='10783450'


*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_PesquisaClientesUpdateInsert]') IS NOT NULL
    drop procedure up_utentes_PesquisaClientesUpdateInsert
go

CREATE PROCEDURE up_utentes_PesquisaClientesUpdateInsert	
	
	 @vatNr				varchar(20),		/*ncont*/
	 @ccNr				varchar(20),		/*bino*/
	 @extId				varchar(20),		/*no_ext*/	
	 @number			varchar(10),		/*no*/
	 @dep				varchar(3),			/*estab*/

	--variaveis para insert/update
	 @name				varchar(80),
	 @email				varchar(45),
	 @dob				datetime,
	 @gender			varchar(1),
	 @type				varchar(20),
	 @height			numeric(3,2), 
	 @weight			numeric(4,2),
	 @job				varchar(60), 
	 @obs				varchar(100),
	 @assocNr			numeric(9,0), 
	 @assocId			varchar(10), 
	 @cardNumber		varchar(30), 
	 @medicalNr			varchar(50), 
	 @ssNr				varchar(20),
	 @cellphone			varchar(20),
	 @phone				varchar(20),
	 @fax				varchar(13),
	 @city				varchar(45),
	 @address			varchar(55),
	 @zipCode			varchar(45),
	 @zone				varchar(20), 
	 @countryCode		varchar(10),
	 @noCredit			bit,
	 @noOrder			bit,
	 @discount			numeric(5,2),
	 @currency			varchar(11),
	 @iban				varchar(28),
	 @swift				varchar(11),
	 @vatCat			bit,
	 @vatPerc			numeric(6,2),
	 @plafond			numeric(19,6),
	 @smsAuth			bit,
	 @emailAuth			bit,	
	 @inative			bit,
	 @nbe               varchar(200) = NULL,
	 @password          varchar(100) = NULL,
	 @nbeValidity       date = NULL,
	 @entfact           bit = NULL
	--não entram aqui: codpla AS codPla, despla AS descPla , ousrdata AS creationDate
	
AS


DECLARE @sqlCommand			VARCHAR(5000) 
DECLARE @contador			int
DECLARE @CountryName		varchar(50)
DECLARE @lastChangeDate		datetime
DECLARE @lastChangeTime		VARCHAR(8)
DECLARE @CreationDate		datetime
DECLARE @CreationTime		VARCHAR(8)
DECLARE @operator		    VARCHAR(8) = 'ONL'
DECLARE @username           VARCHAR(100) = ''


--variaveis para apresentar no final
declare @no_ int
declare @estab_ int
declare @readOnly_ bit

SET @lastChangeDate = convert(varchar,getdate(),23)
SET @lastChangeTime = convert(varchar(8),getdate(),24)
SET @CreationDate = convert(varchar,getdate(),23)
SET @CreationTime = convert(varchar(8),getdate(),24)




/*
--variaveis de controlo de existencia de cliente 

--campos numericos na tabela, não permitir input de nao numericos

EXEC up_utentes_PesquisaClientesUpdateInsert N'12345116',N'998655478',NULL,215,0,N'Filipa testes',N'',N'1990-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,N'',NULL,NULL,N'351912345',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,N'121313',N'123000',N'2021-01-01'


SET @vatNr		= '214898466'
SET @ccNr		= NULL
SET @extId		= NULL
SET @number		= 10783196
SET @dep		= 1

--variaveis para update/insert

SET @name		= 'Jorge Gomes'
SET @email		= 'j@pt'
SET @dob		= NULL
SET @gender		= NULL
SET @type		= NULL
SET @height		= NULL  /*0 para limpar*/
SET @weight		= NULL  /*0 para limpar*/
SET @job		= NULL
SET @obs		= NULL
SET	@assocNr	= NULL  /*0 para limpar*/
SET @assocId	= NULL 
SET @cardNumber	= NULL
SET	@medicalNr	= NULL
SET @ssNr		= NULL
SET @cellphone 	= NULL
SET @phone		= NULL
SET @fax 		= NULL
SET @city		= NULL
SET @address	= NULL
SET @zipCode	= NULL
SET @zone		= NULL
SET @countryCode = NULL	
SET @noCredit	= NULL
SET @noOrder	= NULL
SET @discount	= NULL  /*0 para limpar*/
SET @currency	= NULL
SET @iban		= NULL
SET @swift		= NULL
SET @vatCat		= NULL
SET @vatPerc	= NULL  /*0 para limpar*/
SET @plafond	= NULL  /*0 para limpar*/
SET @smsAuth	= NULL
SET @emailAuth	= NULL
SET @inative	= NULL
*/


--não mostrar dados caso não se preencha nenhum dos campos: len(@ncont+@bino+@no_ext+@no+@estab)=0
-- ou se o campo no e estab for não numérico
IF (@number is NULL OR @number NOT LIKE '%[^0-9]%') 
	AND (@dep is NULL OR @dep NOT LIKE '%[^0-9]%') 

BEGIN	
	/*insert no caso de não ter no (number) ou não ter estab (dep)
	sem no (number): insere novo "no" e estab=0
	sem estab (dep): insere mesmo "no" mas com estab=max(estab)+1
	*/

	declare @obriga_autorizar bit = 1
	declare @autoriza_site bit = 0
	declare @autorizaRGPD   bit = 0
	declare @contaCnc  varchar(100) = ''
	

	select top 1  @obriga_autorizar = isnull(bool,1) from B_Parameters_site(nolock)  where stamp='ADM0000000118'
	select top 1  @contaCnc = isnull(textValue,'') from B_Parameters(nolock)  where stamp='ADM0000000242'
	
	if(isnull(@extId,'')='')
		select top 1  @extId = isnull(textValue,'') from B_Parameters(nolock)  where stamp='ADM0000000273'


	set @obriga_autorizar = isnull(@obriga_autorizar,1)

	if(@obriga_autorizar=0)
		set @autoriza_site= 1
	

	if(len(rtrim(ltrim(isnull(@currency,'')))))=0
		select top 1  @currency = rtrim(ltrim(isnull(textValue,'')))  from B_Parameters(nolock)  where stamp='ADM0000000260'
	
	
	if(rtrim(ltrim(isnull(@currency,'')))='')
		set @currency = 'EURO'

	if(isnull(@emailAuth,0)=1 OR  isnull(@smsAuth,0)=1)
		set @autorizaRGPD = 1
		
	
	
	IF @number IS NULL OR @number='' OR @dep IS NULL OR @dep='' 
	BEGIN
	
		BEGIN TRANSACTION;
			DECLARE @stampid CHAR(25)  
			SET @stampid = (select left(newid(),25))	



			--calc valor de no (number) seguinte ao MAX para inserir caso @number venha sem dados
			IF @number IS NULL OR @number=''
			BEGIN
				DECLARE @no_next varchar(10)
				if(select top 1 bool from B_Parameters_site(nolock)  where stamp='ADM0000000088' )=0 --parametro criação remota
					SELECT @no_next = MAX(no)+1 from b_utentes(nolock)
				else
					SELECT @no_next = MAX(no)+1 from b_utentes(nolock) where no<1000000
			END



		
						
			INSERT INTO b_utentes (utstamp 
					, nome, email, nascimento , sexo, tipo, altura , peso, profi
					, obs, no_ext, noAssociado, id, nrcartao, nbenef , nrss, tlmvl, telefone
					, fax, local, morada, codpost, zona, codigop, descp, nocredit, naoencomenda
					, desconto, moeda, nib, codSwift, cativa, perccativa, eplafond
					, autoriza_sms, autoriza_emails, inactivo, ncont, bino, no, estab
					, usrdata, ousrdata,ousrhora, usrhora, ousrinis, usrinis, nbenef2
					, [password], password_auto_gen, valipla2, autorizasite, autorizado, entfact, conta, pais, RadicalTipoEmp
					) 
			VALUES (  @stampid
						, isnull(@name,''), isnull(@email,''), isnull(@dob,''), isnull(@gender,'')
						, isnull(@type,''), isnull(@height,0), isnull(@weight,0), isnull(@job,'')
						, isnull(@obs,''), isnull(@extId,''), isnull(@assocNr,0), isnull(@assocId,'')
						, isnull(@cardNumber,''), isnull(@medicalNr,''), isnull(@ssNr,'')
						, isnull(@cellphone,''), isnull(@phone,''), isnull(@fax,''), isnull(@city,'')
						, isnull(@address,''), isnull(@zipCode,''), isnull(@zone,''), isnull(@countryCode,'')
						, isnull(@CountryName,'')
						, isnull(@noCredit,''), isnull(@noOrder,''), isnull(@discount,0), isnull(@currency,'')
						, isnull(@iban,''), isnull(@swift,''), isnull(@vatCat,''), isnull(@vatPerc,0)
						, isnull(@plafond,0), isnull(@smsAuth,''), isnull(@emailAuth,''), isnull(@inative,'')
						, isnull(@vatNr,''), isnull(@ccNr,'')
						, COALESCE(NULLIF(@number,''), @no_next)  /*insert da variavel OU do valor maximo+1 da tabela*/
						, CASE WHEN @number IS NULL OR @number='' THEN 0 
							ELSE (select isnull(max(estab)+1,0) from b_utentes where no= @number) END
						,@lastChangeDate, @CreationDate, @CreationTime, @lastChangeTime, @operator, @operator, isnull(@nbe,'')
						,ltrim(rtrim(isnull(@password,''))),0, isnull(@nbeValidity,'1900-01-01'),isnull(@autoriza_site,0),@autorizaRGPD,isnull(@entfact,1), left(isnull(@contaCnc,''),15), 1, 1
					);
					
			--cria registo de notificação

			
			  
				declare @message varchar(200) =''
				select top 1 @number = no, @dep=estab from b_utentes(nolock) where utstamp = @stampid



				set @message = 'Criação de novo cliente nº:' + COALESCE(NULLIF(@number,''), @no_next)

				declare @utilnotif varchar(200)
				set @utilnotif = (select top 1 textValue = ltrim(rtrim(isnull(textValue,'')))  from B_Parameters_site (nolock) where stamp='ADM0000000113')

				declare @grpnotif varchar(200)
				set @grpnotif = (select top 1 textValue = ltrim(rtrim(isnull(textValue,''))) from B_Parameters_site (nolock) where stamp='ADM0000000114')

				set @stampid = ltrim(rtrim(isnull(@stampid,'')))
					
				exec up_servico_insere_notificacao '','',0,'Online','','',@utilnotif,@grpnotif,'b_utentes',@message,'Novo Cliente', @stampid
			
	
		COMMIT TRANSACTION;	
		--print('----- insert with sucess -----');


		--mensagem final
		select getdate() AS DateMessag, 1 AS StatMessag, 1 AS ActionMessage, 'INSERT' As DescMessag
		, COALESCE(NULLIF(@number,''), @no_next) AS number
		, CASE WHEN @number IS NULL OR @number='' THEN 0 
				ELSE (select isnull(max(estab),0) from b_utentes where no= @number) END AS dep;
	END /*fim ciclo insert*/

	ELSE
	
	/*  inicio ciclo update*/
	BEGIN
		select @contador=count(*), @no_ = no, @estab_ = estab, @readOnly_ = isnull(soleitura,0) from b_utentes(nolock) where ( 			
			--deixou de ser necessário pois aqui não entram null nem ''
			--(no = COALESCE(NULLIF(@number,''), no) AND estab = COALESCE(NULLIF(@dep,''), estab))
			(no = @number AND estab = @dep)
			)  AND   no>200
			group by no, estab, soleitura


		--saber nº de linhas do select anterior
		select @CountryName = upper(Country_Name) from [dbo].[countrycode](nolock) where ISO2 = @countryCode	
		
		/*se tiver mais que um registo devolve mensagem e nao faz o update
		isto não deve acontecer porque no7estab são chave primária...mas...
		*/
		If @contador > 1 
		BEGIN
				--print('Existe mais que um registo na pesquisa, são necessários mais campos de pesquisa')
				select getdate() AS DateMessag, 0 As StatMessag, 2 AS ActionMessage, 'NEED MORE FIELDS' As DescMessag, @number AS number, @dep As dep;
			END
			ELSE If isnull(@readOnly_,0) = 1
			BEGIN
				--print('Este cliente esta marcado como readOnly')
				select getdate() AS DateMessag, 0 As StatMessag, 5 AS ActionMessage, 'READ_ONLY' As DescMessag, @number AS number, @dep As dep;

		
		END
		ELSE IF @contador = 1 /*tem um registo, faz o update*/
			BEGIN	
/*				
			BEGIN TRY
*/				--verificar se existem registos na tabela cc para o no/estab fornecido
				--caso exista, devolve null para não permitir alterar o contribuinte
			
				IF (Select count(ccstamp) from cc(nolock) where no=@number and estab=@dep) > 0
				BEGIN
					SET @vatNr = NULL
				ENd

			

				BEGIN TRANSACTION; 	
					UPDATE b_utentes SET 
					 nome				= COALESCE(@name, nome)  
					, email				= COALESCE(@email, email)
					, nascimento		= COALESCE(@dob, nascimento)		
					, sexo				= COALESCE(@gender, sexo)	
					, tipo				= COALESCE(@type, tipo)	
					, altura			= COALESCE(@height, altura)	
					, peso				= COALESCE(@weight, peso)	 
					, profi				= COALESCE(@job, profi)	
					, obs				= COALESCE(@obs, obs)	
					, no_ext			= COALESCE(@extId, no_ext)	
					, noAssociado		= COALESCE(@assocNr, noAssociado)	
					, id				= COALESCE(@assocId, id)	
					, nrcartao			= COALESCE(@cardNumber, nrcartao)	
					, nbenef			= COALESCE(@medicalNr, nbenef)	
					, nrss				= COALESCE(@ssNr, nrss)	
					, tlmvl				= COALESCE(@cellphone, tlmvl)	
					, telefone			= COALESCE(@phone, telefone)	
					, fax				= COALESCE(@fax, fax)	
					, local				= COALESCE(@city, local)
					, morada			= COALESCE(@address, morada)
					, codpost			= COALESCE(@zipCode, codpost)
					, zona				= COALESCE(@zone, zona)
					, codigop			= COALESCE(@countryCode, codigop)
					, descp				= COALESCE(@CountryName, descp) 
					, nocredit			= COALESCE(@noCredit, nocredit)
					, naoencomenda		= COALESCE(@noOrder, naoencomenda)
					, desconto			= COALESCE(@discount, desconto)
					, moeda				= COALESCE(@currency, moeda)
					, nib				= COALESCE(@iban, nib)
					, codSwift			= COALESCE(@swift, codSwift)
					, cativa			= COALESCE(@vatCat, cativa)
					, perccativa		= COALESCE(@vatPerc, perccativa)
					, eplafond			= COALESCE(@plafond, eplafond)
					, autoriza_sms		= COALESCE(@smsAuth, autoriza_sms)
					, autoriza_emails	= COALESCE(@emailAuth, autoriza_emails)
					, usrdata			= COALESCE(@lastChangeDate, usrdata)
					, usrhora			= COALESCE(@lastChangeTime, usrhora)
					, inactivo			= COALESCE(@inative, inactivo)
					, ncont				= COALESCE(@vatNr, ncont)
					, bino				= COALESCE(@ccNr, bino) 	
					, usrinis			= @operator	
					, nbenef2           = COALESCE(@nbe, nbenef2) 
					, [password]        = COALESCE(@password, [password] ) 
					, password_auto_gen = 0
					, valipla2          = COALESCE(@nbeValidity, valipla2)
					, autorizado        = COALESCE(@autorizaRGPD, autorizado) 
					, entfact			= COALESCE(@entfact, entfact) 
				
					/*
					no update não pode mexer nestas variaveis, sao a chave
					, no = COALESCE(@number, no)
					, estab = COALESCE(@dep, estab)
					*/
					where (							
							--deixou de ser necessário pois aqui não entram null nem ''
							--(no = COALESCE(NULLIF(@number,''), no) AND estab = COALESCE(NULLIF(@dep,''), estab))
							(no = @number AND estab = @dep)
							) AND   no>200;
							
			
				if(@autoriza_site=1)
				begin

					UPDATE b_utentes SET autorizasite=@autoriza_site
						where (							
							--deixou de ser necessário pois aqui não entram null nem ''
							--(no = COALESCE(NULLIF(@number,''), no) AND estab = COALESCE(NULLIF(@dep,''), estab))
							(no = @number AND estab = @dep)
							) AND   no>200;
				end

			


				COMMIT TRANSACTION;	
				select getdate() AS DateMessag, 1 As StatMessag, 0 AS ActionMessage, 'UPDATE' As DescMessag, @no_ AS number, @estab_ As dep;

/*
			END TRY
			BEGIN CATCH
				SELECT   
					ERROR_NUMBER() AS ErrorNumber  
					,ERROR_MESSAGE() AS ErrorMessage;  
				IF @@TRANCOUNT > 0
				BEGIN
					ROLLBACK TRANSACTION;		
				END
			END CATCH
*/
			END
				ELSE /*não tem registo, faz o insert*/
				BEGIN
					--print('Não pode fazer o update, o registo não existe')
					select getdate() AS DateMessag, 0 As StatMessag, 3 AS ActionMessage, 'NO DATA' As DescMessag, @number AS number, @dep As dep;
				END
	END /*fim ciclo update*/
END


/*cria username estatico e valida se tem acesso ao serviço */
if(@number>0 and @dep>-1)
begin
	
	declare @clStamp varchar(50 ) = ''
	set @username ='C' + LTRIM(RTRIM(STR(@number))) + '.' +LTRIM(RTRIM(STR(@dep)))

	select @clStamp = ltrim(rtrim(utstamp)) from b_utentes(nolock)  where no=@number and estab = @dep
	
	if(@clStamp!='')
	begin
		update b_utentes set username = @username where utstamp = @clStamp

		insert into service_user
		select 
			left(NEWiD(),30),
			(select  ltrim(rtrim(stamp)) from service(nolock)  where descr='erp'),
			ltrim(rtrim(b_utentes.utstamp)),
			getdate()
		 from b_utentes(nolock)
		 left join  service_user(nolock) on service_user.clStamp = b_utentes.utstamp
		 where service_user.clStamp  is null
	
	 end  


end



ELSE
BEGIN
	--print('Campos devem ser numéricos: number e dep')
	select getdate() AS DateMessag, 0 As StatMessag, 4 AS ActionMessage, 'WRONG TYPE' As DescMessag, @number AS number, @dep As dep;
END

            
GO
Grant Execute On up_utentes_PesquisaClientesUpdateInsert to Public
Grant Control On up_utentes_PesquisaClientesUpdateInsert to Public
GO
			
           
			
	