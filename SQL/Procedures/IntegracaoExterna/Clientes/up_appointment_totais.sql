/* SP Pesquisa marcações
J.Gomes, 2020-07-17
--------------------------------





EXECUÇÃO DA SP:
---------------
Parametros: up_appointment_totais 'no','estab', 'site', data, page, orderstamp

2020-07-15 00:00:00.000

exec up_appointment_totais '215','0',null, null, null, null, null


@orderstamp (ltdev30)
data  : E8AB2CF8-2480-463B-AF04-CCDFDDDAE01B
data desc: C243D5DD-8AF9-4846-B43C-BC065D062FBE


*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_appointment_totais]') IS NOT NULL
	DROP PROCEDURE [dbo].up_appointment_totais ;
GO

CREATE PROCEDURE [dbo].up_appointment_totais

	  @no				varchar(10)
	, @estab			varchar(3)
	, @site				varchar(50) = ''
	, @ncont			varchar(20)
	, @bino				varchar(20)
	, @no_ext			varchar(20)
	, @email			varchar(50)
	   
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

	Select @site = site
	From empresa(nolock)
	where empresa.site = @site

	SET @site = ISNULL(@site,'')

DECLARE @sql varchar(max)
declare @utstamp varchar(25) = ''



select top 1 @no=isnull(no,0), @estab=isnull(estab,0)	
 from b_utentes (NOLOCK) where ( 
		ncont = COALESCE(NULLIF(@ncont,''), ncont) 
		AND bino = COALESCE(NULLIF(@bino,''), bino) 
		AND no_ext = COALESCE(NULLIF(@no_ext,''), no_ext) 
		AND (no = COALESCE(NULLIF(@no,''), no) AND estab = COALESCE(NULLIF(@estab,''), estab))
		AND email = COALESCE(NULLIF(@email,''), email) 
		)
		AND inactivo = 0
		;



select @sql = N' 


					WITH dados as (
								select mrstamp, data,dataFim,mrno,hinicio,hfim,utstamp,nome,no,estab
									,obs,serieno,serienome,sessao,estado
									,SerieDescricao = convert(varchar(254),LEFT(serienome,245) + '' ['' + convert(varchar,sessao) + '']'')
									,site, ltrim(rtrim(SUBSTRING(combinacao,0, CHARINDEX(''+'',combinacao)))) as  combinacao
									,convert(datetime, convert(varchar(10),data,23) + '' '' + hinicio ) as dateInit
									,convert(datetime, convert(varchar(10),dataFim,23) + '' '' + hfim ) as dateEnd
									,rtrim(ltrim(isnull((select isnull(iniciais,'''') from b_us(nolock) where userno = id_us_alt ),''''))) as user_change
									,isnull((select top 1 isnull(convert(datetime, convert(varchar(10),data,23) + '' '' + hinicio ) ,''1900-01-01'') from marcacoes(nolock) where data>getdate() order by convert(datetime, convert(varchar(10),data,23) + '' '' + hinicio ) asc),''1900-01-01'')  AS nextAppointmentDate
								
									, total  = (Select 
													isnull(sum(total),0) 
												from 
													marcacoesServ (nolock) 
												where 
													marcacoesServ.mrstamp = marcacoes.mrstamp)				
						
								
								from 
									marcacoes (nolock)		
								where		
									(no = COALESCE(NULLIF(''' + @no + ''',''''), no) AND estab = COALESCE(NULLIF(''' + @estab + ''',''''), estab))
									AND site = COALESCE(NULLIF(''' + @site + ''',''''), site)	
					)



			SELECT 
			 sum(round(total,2)) as retailPrice
			,''min'' as timeUnit
			, sum(case when  estado  in (''EFETUADO'') then 1 else 0 end) AS successAppointmentsTotal
			, sum(case when dateEnd<getdate() AND estado not in (''CANCELADO'',''EFETUADO'')   then 1 else 0 end)  AS missingAppointmentsTotal
			, sum(case when dateInit>getdate() AND estado not in (''CANCELADO'')   then 1 else 0 end)  AS penddingAppointmentsTotal
		    , case when convert(varchar(20),nextAppointmentDate,120) = ''1900-01-01 00:00:00'' then ''1900-01-01 00:00:00'' else convert(varchar(20),nextAppointmentDate,120)  end  as nextAppointmentDate
			FROM (
					select 
					*
					 from dados	 
					) X	
			group by x.nextAppointmentDate
							
		    '				

			



--print @sql
EXECUTE (@sql)





GO
GRANT EXECUTE on dbo.up_appointment_totais TO PUBLIC
GRANT Control on dbo.up_appointment_totais TO PUBLIC
GO


