/* SP Pesquisa Registo Clinico
J.Gomes, 2020-07-08
--------------------------------

Listagem de registo clinico
atraves da pesquisa de um cliente, por qualquer um dos campos disponiveis, procura-se o registo clinico atraves
do utstamp.


Pesquisa nas tabelas: 	
	b_utentes,registoClinico,B_cli_presc
	
Campos obrigatórios: algum dos campos de pesquisa de cliente


OUTPUT:
---------
pageSize, pageTotal, pageNumber, linesTotal, idcategoria, categoria, regstamp, utstamp, OperationInfo_Data
, OperationInfo_CreationDate, OperationInfo_CreationTime, OperationInfo_lastChangeDate,	OperationInfo_lastChangeTime
, OperationInfo_lastChangeUser, RecordInfo_SoapS, RecordInfo_SoapO, RecordInfo_SoapA,RecordInfo_SoapP
, RecordInfo_Type, RecordInfo_Attachment



EXECUÇÃO DA SP:
---------------
Parametros: up_Registo_Clinico 'ncont','bino','no_ext','no','estab', 'email', datainit, dataend, page, orderstamp


exec up_Registo_Clinico '','','','206','0','','2018-11-12' ,  '2022-11-15',1,''
exec up_Registo_Clinico '','','','206','0','','2018-11-12' ,  '2018-11-15',1,'9F66FD69-F320-4FE4-B683-B50391FC10AF'

exec up_Registo_Clinico '','','','20','0','','2018-11-12' ,  '1900-11-15',1,'9F66FD69-F320-4FE4-B683-B50391FC10AF'



@orderstamp
OperationInfo_CreationDate, OperationInfo_CreationTime :9F66FD69-F320-4FE4-B683-B50391FC10AF
OperationInfo_CreationDate desc, OperationInfo_CreationTime desc:741F63FA-0A3F-4255-AF93-542CBE09B8B2

	*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Registo_Clinico]') IS NOT NULL
	DROP PROCEDURE [dbo].up_Registo_Clinico ;
GO

CREATE PROCEDURE [dbo].[up_Registo_Clinico]	

	  @ncont			varchar(20)
	, @bino				varchar(20)
	, @no_ext			varchar(20)
	, @no				varchar(10)
	, @estab			varchar(3)
	, @email			varchar(50)
	, @dateInit			varchar(10)  = '1900-01-01'
	, @dateEnd	        varchar(10)  = '3000-01-01'
	, @pageNumber       int = 1
	, @orderStamp		VARCHAR(36) = ''
	   
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

SET @dateInit		    =   rtrim(ltrim(isnull(@dateInit,'1900-01-01')))
SET @dateEnd		    =   rtrim(ltrim(isnull(@dateEnd,'3000-01-01')))
set @orderStamp         =	rtrim(ltrim(ISNULL(@orderStamp,'')))
--as variaveis de pesquisa de cliente nao precisam disto, tem o coalesce abaixo...


if(isnull(@pageNumber,0)<1)
	set @pageNumber = 1	

DECLARE @sql varchar(max)
DECLARE @orderBy VARCHAR(8000)
DECLARE @PageSize int = 100
DECLARE @OrderCri VARCHAR(MAX)
declare @utstamp varchar(25) = ''

IF @orderStamp = ''
BEGIN 
	SET @OrderCri = 'ORDER BY OperationInfo_Date, OperationInfo_Hour'
END 
ELSE 
BEGIN 
	SELECT @OrderCri='ORDER BY ' + instructionSql  FROM sortingTableOrder (NOLOCK) WHERE stamp = @orderStamp and inativo=0 and nameService='ClinicalRecords'
END


select top 1 @utstamp=utstamp from b_utentes (NOLOCK) where ( 
		ncont = COALESCE(NULLIF(@ncont,''), ncont) 
		AND bino = COALESCE(NULLIF(@bino,''), bino) 
		AND no_ext = COALESCE(NULLIF(@no_ext,''), no_ext) 
		AND (no = COALESCE(NULLIF(@no,''), no) AND estab = COALESCE(NULLIF(@estab,''), estab))
		AND email = COALESCE(NULLIF(@email,''), email) 
		) AND inactivo = 0;


set @orderBy = ' ) X ' + @OrderCri + '  
			
				 OFFSET ' + convert(varchar(10),@PageSize) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
				 FETCH NEXT  ' + convert(varchar(10),@PageSize) + ' ROWS ONLY '

select @sql = N' 
					WITH dados as (
							Select 	regstamp, utstamp
							, convert(varchar, registoClinico.data, 23)													AS OperationInfo_Date
							, hora																						AS OperationInfo_Hour
							, convert(varchar, registoClinico.ousrdata, 23)												AS OperationInfo_CreationDate
							, registoClinico.ousrhora																	AS OperationInfo_CreationTime
							, convert(varchar, registoClinico.usrdata, 23)												AS OperationInfo_lastChangeDate
							, registoClinico.usrhora																	AS OperationInfo_lastChangeTime
							, registoClinico.usrinis																	AS OperationInfo_lastChangeUser	
							, s as RecordInfo_SoapS, cast (o as varchar(max))											AS RecordInfo_SoapO
							, cast(a as varchar(max))  as RecordInfo_SoapA, cast(p as varchar(max))						AS RecordInfo_SoapP
							, registoClinico.tipo																		AS RecordInfo_Type
							,ISNULL(b_us.nome,'''')																		AS RecordInfo_name
							,registoClinico.confidencial																AS RecordInfo_confidential


							from registoClinico(nolock) 
								LEFT JOIN B_US ON registoClinico.ousrinis = b_us.iniciais
							Where confidencial=0 and utstamp = ''' + @utstamp	+ '''

							union
						
							select
								cast(prescstamp as varchar(30)) as regstamp	, cast(utstamp as varchar(30))				AS utstamp
								, convert(varchar, B_cli_presc.data, 23)												AS OperationInfo_Date 
								, B_cli_presc.ousrhora																	AS OperationInfo_Hour
								, convert(varchar, B_cli_presc.ousrdata, 23)											AS OperationInfo_CreationDate
								, B_cli_presc.ousrhora																	AS OperationInfo_CreationTime
								, convert(varchar, B_cli_presc.usrdata, 23)												AS OperationInfo_lastChangeDate
								, B_cli_presc.usrhora																	AS OperationInfo_lastChangeTime
								, B_cli_presc.usrinis																	AS OperationInfo_lastChangeUser
								, ''Prescrição nr. '' + cast(nrprescricao as varchar(30))								AS RecordInfo_SoapS
								, cast('''' as varchar(max))  as RecordInfo_SoapO, cast('''' as varchar(max))			AS RecordInfo_SoapA
								, cast('''' as varchar(max))															AS RecordInfo_SoapP
								, B_cli_presc.tipo																		AS RecordInfo_Typo
								,ISNULL(b_us.nome,'''')																	AS RecordInfo_name
								,B_cli_presc.confidencial																AS RecordInfo_confidential

								
							from B_cli_presc(nolock) 
								LEFT JOIN B_US ON B_cli_presc.ousrinis = b_us.iniciais

							Where confidencial=0 and utstamp = ''' + @utstamp + '''				
					)


			SELECT '  + convert(varchar(10),@PageSize) +' as pageSize,  (COUNT(*) OVER() / ' +  convert(varchar(10),@PageSize) +') + 1 as pageTotal, '
				+ convert(varchar(10),@PageNumber) +' as pageNumber, COUNT(*) OVER() as linesTotal		
			, *
			FROM (
						select *, '''' as RecordInfo_Attachment from dados		
			
						--select A.regstamp, A.utstamp, A.data, A.ousrdata, A.ousrhora, A.usrdata, A.usrhora, A.usrinis
						--, A.soapS, A.soapO, A.soapA, A.soapP, A.tipo
						--, B.anexosstamp, B.tabela, B.filename 
						--from dados A
						--left join anexos B
						--on B.regstamp = A.regstamp	
					'


if @dateInit != '' and @dateEnd  != ''
	select @sql = @sql + N' where OperationInfo_Date BETWEEN ''' + @dateInit + '''  AND  ''' + @dateEnd + ''''


select @sql = @sql + @orderBy
	   
--print @sql
EXECUTE (@sql)



GO
GRANT EXECUTE on dbo.up_Registo_Clinico TO PUBLIC
GRANT Control on dbo.up_Registo_Clinico TO PUBLIC
GO