/*

Sp retorna management Availability

EXEC up_managementAvailability 1,0


*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_managementAvailability]') IS NOT NULL
	drop procedure dbo.up_managementAvailability
go

create procedure dbo.up_managementAvailability
	 @op           numeric(9,0),
	 @estab		   numeric(5,3) = 0

/* WITH ENCRYPTION */
AS

	DECLARE @lastChangeDateTime VARCHAR(20)

	DECLARE @sql varchar(max) = ''

    select @sql = 
		'
			SELECT 
				distinct
					convert(varchar,b_series.dataInicio,23)							AS scheduleInfo_initialDate
                   ,convert(varchar,b_series.dataFim,23)							AS scheduleInfo_finalDate
				   ,rtrim(ltrim(isnull(b_series.serienome,'''')))					AS scheduleInfo_desgin
				   ,rtrim(ltrim(isnull(b_series.site,'''')))						AS scheduleInfo_local
				   ,b_series.horaInicio												AS scheduleInfo_begin
				   ,b_series.horafim												AS scheduleInfo_end
				   ,b_series.serieno												AS scheduleInfo_id
				   ,b_series.segunda												AS scheduleInfo_monday
				   ,b_series.terca													AS scheduleInfo_tuesday
				   ,b_series.quarta													AS scheduleInfo_wednesday
				   ,b_series.quinta													AS scheduleInfo_thursday
				   ,b_series.sexta													AS scheduleInfo_friday
				   ,b_series.sabado													AS scheduleInfo_saturday
				   ,b_series.domingo												AS scheduleInfo_sunday
			FROM 
				b_series(nolock)
			WHERE 				
				b_series.no =''' + LTRIM(RTRIM(STR(@op))) + ''' AND 
				b_series.estab =''' + LTRIM(RTRIM(STR(@estab))) + ''' '			
																					
	select @sql = @sql
		
	print @sql

	EXECUTE (@sql)
								
 
GO
Grant Execute On dbo.up_managementAvailability to Public
Grant Control On dbo.up_managementAvailability to Public
GO
