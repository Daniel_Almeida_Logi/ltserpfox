/*
Sp retorna management unavailable

EXEC up_managementUnavailable 1
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_managementUnavailable]') IS NOT NULL
	drop procedure dbo.up_managementUnavailable
go

create procedure dbo.up_managementUnavailable
	 @op           numeric(9,0),
	 @estab        numeric(5,3) = 0

/* WITH ENCRYPTION */
AS

	DECLARE @lastChangeDateTime VARCHAR(20)

	DECLARE @sql varchar(max) = ''

    select @sql = 

		'
			SELECT 
				distinct
					convert(varchar,b_us_indisponibilidades.dataInicio,23)			AS scheduleInfo_initialDate
                   ,convert(varchar,b_us_indisponibilidades.dataFim,23)				AS scheduleInfo_finalDate
				   ,rtrim(ltrim(isnull(b_us_indisponibilidades.site,'''')))			AS scheduleInfo_local
				   ,b_us_indisponibilidades.horaInicio								AS scheduleInfo_begin
				   ,b_us_indisponibilidades.horafim									AS scheduleInfo_end
				   ,b_us_indisponibilidades.segunda									AS scheduleInfo_monday
				   ,b_us_indisponibilidades.terca									AS scheduleInfo_tuesday
				   ,b_us_indisponibilidades.quarta									AS scheduleInfo_wednesday
				   ,b_us_indisponibilidades.quinta									AS scheduleInfo_thursday
				   ,b_us_indisponibilidades.sexta									AS scheduleInfo_friday
				   ,b_us_indisponibilidades.sabado									AS scheduleInfo_saturday
				   ,b_us_indisponibilidades.domingo									AS scheduleInfo_sunday
				   ,b_us_indisponibilidades.id										AS scheduleInfo_id
			FROM 
				b_us_indisponibilidades(nolock)
			WHERE 				
				b_us_indisponibilidades.userno =''' + LTRIM(RTRIM(STR(@op))) + ''''
																					
	select @sql = @sql
		
	print @sql

	EXECUTE (@sql)
								
 
GO
Grant Execute On dbo.up_managementUnavailable to Public
Grant Control On dbo.up_managementUnavailable to Public
GO
