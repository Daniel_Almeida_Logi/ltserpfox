
/*

Daniel Almeida - 20210429

Sp para validar os registo do picaponto


select * from ticket_request(nolock)

EXEC up_operador_picaPonto N'2000-03-27 00:00',N'2030-03-27 23:59',null,null,N'Loja 1',null,1000,1

EXEC up_operador_picaPonto null,null,null,null,N'Loja 1',null,1000,1, N'2000-03-27 00:00',N'2030-03-27 23:59'

*/






SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_operador_picaPonto]') IS NOT NULL
	drop procedure dbo.up_operador_picaPonto
go



create procedure dbo.up_operador_picaPonto
	 @dataIni                     DATETIME
	,@dataFim                     DATETIME
	,@opNumber	                  as numeric(9,0)
	,@opInit                      varchar(3)
	,@site                        varchar(18)
	,@operationCode               varchar(MAX)
	,@topMax					  int 
	,@pageNumber				  int
	,@comunicationDateInit        DATETIME = '19000101'
	,@comunicationDateEnd         DATETIME = '30000101'



/* WITH ENCRYPTION */
AS
	


	set @opInit						= rtrim(ltrim(isnull(@opInit,'')))
	set @opNumber					= isnull(@opNumber,0)
	set @operationCode				= rtrim(ltrim(isnull(@operationCode,'')))
	set @topMax						= isnull(@topMax,100)
	set @pageNumber					= isnull(@pageNumber,1)
	set @dataIni					= isnull(@dataIni,'1900-01-01 00:00:00')
	set @dataFim					= isnull(@dataFim,'3000-01-01 00:00:00')
	set @comunicationDateInit		= isnull(@comunicationDateInit,'1900-01-01 00:00:00')
	set @comunicationDateEnd		= isnull(@comunicationDateEnd,'3000-01-01 00:00:00')

	if(@topMax>1000)
	 set @topMax = 1000

	if(@topMax<=0)
		set @topMax = 1



	Declare @site_nr int = 0

	select top 1 @site_nr=isnull(no,0) from empresa(nolock) where site=@site 
	DECLARE @orderBy VARCHAR(8000)
	DECLARE @OrderCri VARCHAR(MAX) =''

	SET @OrderCri = 'ORDER BY  isnull(operatorClockinOutDate,''1900-01-01 00:00:00'') ASC'


	set @orderBy = ' ) X ' + @OrderCri +'  
				 OFFSET ' + convert(varchar(10),@topMax) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
				 FETCH NEXT  ' + convert(varchar(10),@topMax) + ' ROWS ONLY '


	DECLARE @sql varchar(max) = ''


    select @sql = 

		'
		
		SELECT '  + convert(varchar(10),@topMax) +' as pageSize,  (COUNT(*) OVER() / ' +  convert(varchar(10),@topMax) +') + 1 as pageTotal, '
				+ convert(varchar(10),@PageNumber) +' as pageNumber, COUNT(*) OVER() as linesTotal		
			,* from (
			SELECT 
				distinct
			   rtrim(ltrim(isnull(B_us.iniciais,'''')))        as operatorUser,
			   rtrim(ltrim(isnull(B_us.nome,'''')))            as operatorName,
			   isnull(B_us.userno,0)                           as operatorId,
			   isnull(line.inOutCode,0)                        as operatorClockInoutCode,
			   rtrim(ltrim(isnull(line.inOutDescr,'''')))      as operatorClockinoutDescr,
			   isnull(line.inOutDate,''1900-01-01 00:00:00'')  as operatorClockinOutDate


			FROM 
				clock_response_payload_line(nolock) line
			inner join B_us(nolock) on B_us.userno = line.userId
			WHERE 
				line.inOutCode >= 0  
				and  line.inOutCode < 99
				and  siteNr = ' + convert(varchar(10),@site_nr) + ' 
				and  type=''timeRegister''
				and line.inOutDate >= ''' + convert(varchar, @dataIni, 120) + '''
				and line.inOutDate <= ''' + convert(varchar, @dataFim, 120) + '''
				and line.ousrdata >= '''  + convert(varchar, @comunicationDateInit, 120) + '''
				and line.ousrdata <= '''  + convert(varchar, @comunicationDateEnd, 120) + '''
		'

	if @opNumber > 0
		select @sql = @sql + N' AND B_us.userno = ''' +  convert(varchar(20),@opNumber)   + '''' 
	
	if @opInit != ''
		select @sql = @sql + N' AND  B_us.iniciais = ''' + @opInit + '''' 
	
	if len(rtrim(ltrim(@operationCode))) > 1
		select @sql = @sql + N' AND line.inOutCode in ( ' + @operationCode + ' ) '


																		
	select @sql = @sql + @orderBy

		
	--print @sql

	EXECUTE (@sql)
								
 
GO
Grant Execute On dbo.up_operador_picaPonto to Public
Grant Control On dbo.up_operador_picaPonto to Public
GO
