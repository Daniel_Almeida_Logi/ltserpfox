
/*

Daniel Almeida - 20210707

Sp retorna lista de operadtores


select * from b_us(nolock)

EXEC up_operador_pesquisa null,'adm',null,null,1000,1



*/






SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_operador_pesquisa]') IS NOT NULL
	drop procedure dbo.up_operador_pesquisa
go



create procedure dbo.up_operador_pesquisa
	 @name            varchar(150)
	,@user            varchar(3)
	,@lastChangeDate  datetime
	,@lastChangeTime  varchar(8)
	,@topMax          int 
	,@pageNumber      int



/* WITH ENCRYPTION */
AS
	
	DECLARE @siteId VARCHAR(100) = ''

	select top 1 @siteId = id_lt from empresa(nolock) order by no asc


	SET @name			=   rtrim(ltrim(isnull(@name,'')))
	SET @user			=   rtrim(ltrim(isnull(@user,'')))
	SET @topMax			=   isnull(@topMax,1000)
	SET @lastChangeDate =	rtrim(ltrim(isnull(@lastChangeDate,'19000101')))
	SET @lastChangeTime =	rtrim(ltrim(isnull(@lastChangeTime,'00:00:00')))
	SET @pageNumber     =	isnull(@pageNumber,1)

	DECLARE @lastChangeDateTime VARCHAR(20)

	set @lastChangeDateTime = @lastChangeDate + @lastChangeTime
 

	if(@topMax>1000)
	 set @topMax = 1000

	if(@topMax<=0)
		set @topMax = 1

	if(@pageNumber<1)
		set @pageNumber = 1


	DECLARE @orderBy VARCHAR(8000)
	DECLARE @OrderCri VARCHAR(MAX) =''

	SET @OrderCri = 'ORDER BY  isnull(OperationInfo_LastChangeDate,''1900-01-01 00:00:00'') ASC'

	


	set @orderBy = ' ) X ' + @OrderCri +'  
				 OFFSET ' + convert(varchar(10),@topMax) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
				 FETCH NEXT  ' + convert(varchar(10),@topMax) + ' ROWS ONLY '


	DECLARE @sql varchar(max) = ''


    select @sql = 

		'
		
		SELECT '  + convert(varchar(10),@topMax) +' as pageSize,  (COUNT(*) OVER() / ' +  convert(varchar(10),@topMax) +') + 1 as pageTotal, '
				+ convert(varchar(10),@PageNumber) +' as pageNumber, COUNT(*) OVER() as linesTotal		
			,* from (
			SELECT 
				distinct
				    rtrim(ltrim(isnull(B_us.iniciais,'''')))			AS IDInfo_user
					,isnull(userno,0)									AS IDInfo_number
					,rtrim(ltrim(usstamp))                              AS IDInfo_regstamp
					,'''+LTRIM(RTRIM(@siteId))+'''						AS IDInfo_siteId

				    ,rtrim(ltrim(isnull(B_us.nome,'''')))				AS PersonalInfo_name
					,isnull(B_us.inactivo,0)							AS PersonalInfo_inactive
					,rtrim(ltrim(isnull(B_us.username,'''')))			AS PersonalInfo_abrev
					,convert(varchar, b_us.dtnasc, 23)					AS PersonalInfo_birthday
					,b_us.ncont											AS PersonalInfo_vat
					,rtrim(ltrim(isnull(b_us.email,'''')))				AS PersonalInfo_email
					,rtrim(ltrim(isnull(b_us.emailprofissional,'''')))	AS PersonalInfo_professionalEmail
					,b_us.tlmvl											AS PersonalInfo_phoneNumber
					,b_us.extensaotelefone								AS PersonalInfo_phoneExtension


			   		,convert(varchar, B_us.ousrdata, 23)				AS OperationInfo_CreationDate 
					,B_us.ousrhora										AS OperationInfo_CreationTime
					,convert(varchar, B_us.usrdata, 23)					AS OperationInfo_LastChangeDate 
					,B_us.usrhora										AS OperationInfo_LastChangeTime
					,B_us.usrinis										AS OperationInfo_lastChangeUser

					,rtrim(ltrim(isnull(B_us.grupo,'''')))				AS GeneralInfo_group
					,rtrim(ltrim(isnull(B_us.departamento,'''')))		AS GeneralInfo_department
					,rtrim(ltrim(isnull(B_us.area,'''')))				AS GeneralInfo_area
					,rtrim(ltrim(isnull(b_us.lingua	,'''')))			AS GeneralInfo_language
					,rtrim(ltrim(isnull(b_us.obs,'''')))				AS GeneralInfo_obs

					,rtrim(ltrim(isnull(b_us.morada,'''')))				AS AddressInfo_address
					,rtrim(ltrim(isnull(b_us.codpost,'''')))			AS AddressInfo_zipCode
					,rtrim(ltrim(isnull(b_us.local,'''')))				AS AddressInfo_city
					,rtrim(ltrim(isnull(b_us.nacion,'''')))				AS AddressInfo_country
					

					,B_us.aceita_termos									AS RGPDInfo_termsAccepted
					,convert(varchar, B_us.dt_aceita_termos, 23) 		AS RGPDInfo_dateAccepted

					
			FROM 
				B_us(nolock) 
			WHERE 				
				convert(datetime,convert(varchar(10),B_us.usrdata,120) + '' '' + B_us.usrhora) >= ''' + @lastChangeDateTime + ''''
		

	if(ltrim(rtrim(@name))!='')
		SET @sql = @sql +  ' and B_us.nome like ''%' + ltrim(rtrim(@name)) +'%'''


	if(ltrim(rtrim(@user))!='')
		SET @sql = @sql +  ' and B_us.iniciais= ''' + ltrim(rtrim(@user)) +''''

																		
	select @sql = @sql + @orderBy

		
	print @sql

	EXECUTE (@sql)
								
 
GO
Grant Execute On dbo.up_operador_pesquisa to Public
Grant Control On dbo.up_operador_pesquisa to Public
GO
