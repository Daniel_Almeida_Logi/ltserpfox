/* SP Files Upload 
Daniel Almeida, 2020-05-11
--------------------------------

Download de ficheiro através da especificação da loja e do stamp do ficheiro.

Parametros do ficheiro obrigatórios:
	@site - Id da Loja (sender Id)
	@stamp - identificador unico da imagem
	   	 
Pesquisa nas tabela: 	
B_Parameters_site, anexos, empresa
	

OUTPUT:
---------
DateMessag				StatMessag	DescMessag									pathfile	
2020-05-12 14:46:04.897	0			Missing File		                        ''		


DateMessag				StatMessag	DescMessag		pathfile													
2020-05-12 15:06:58.103	1			Search Ok		\\st01\ArquivoDigital\LTDEV30\file.jpg	


StatMessag: 
	0 - ERROR
	1 - OK
	
DescMessag: 	
	1 - Missing File 
	
	

EXECUÇÃO DA SP:
---------------
exec up_FilesDownload '@site', '@stamp'

	exec up_FilesDownload  'LTDEV30','xpto'
	exec up_FilesDownload  'LTDEV30','ADM040D9F56-F829-417B-98B'

	exec up_FilesDownload  'LTDEV30','ADM559EBF99-A565-44CE-B24'

	exec up_FilesDownload  'LTDEV30','824D3D3F-BDD0-41F8-9D9'
	
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


IF OBJECT_ID('[dbo].[up_FilesDownload]') IS NOT NULL
		DROP PROCEDURE [dbo].up_FilesDownload ;
GO

CREATE PROCEDURE [dbo].up_FilesDownload	
		 @senderId          VARCHAR(20),
		 @stamp				VARCHAR(30)
		 		 
	/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

SET @senderId	=      rtrim(ltrim(isnull(@senderId,'')))
SET @stamp		=      rtrim(ltrim(isnull(@stamp,'')))

Declare @site          varchar(20) = ''
Declare @table         varchar(20) = ''
Declare @path          varchar(254) = ''
Declare @pathDic       varchar(254) = ''
Declare @url           varchar(254) = ''
Declare @fileName      varchar(100) = ''
Declare @dicImages     bit = 0
Declare @isDic         bit = 1
Declare @type          varchar(20) = ''
Declare @pathToShared  varchar(100) = ''
Declare @pathSaved	   varchar(254) = ''

		select 
			top 1 
				@site = ltrim(rtrim(isnull(site,'')))
			from 
				empresa(nolock)
			where 
				ltrim(rtrim(id_lt))=@senderId

		select 
		    top 1
			@pathToShared=ltrim(rtrim(isnull(textValue,''))) 
		from 
			B_Parameters_site(nolock) 
		where 
			site=@site and 
			stamp='ADM0000000161'
		
		select 
		    top 1
			@path=ltrim(rtrim(isnull(textValue,''))) 
		from 
			B_Parameters_site(nolock) 
		where 
			site=@site and 
			stamp='ADM0000000010'

		select 
		    top 1
			@pathDic=ltrim(rtrim(isnull(textValue,''))) 
		from 
			B_Parameters_site(nolock) 
		where 
			site=@site and 
			stamp='ADM0000000106'

		select 
			top 1 @dicImages = isnull(bool,0)
		from 
			B_Parameters_site(nolock) 
		where stamp='ADM0000000107' and site = @site




		select 
		    top 1
			@fileName=ltrim(rtrim(isnull(filename,''))), 
			@table = ltrim(rtrim(isnull(tabela,''))), 
			@isDic = case when UPPER(ltrim(rtrim(tipo)))='DIC' then 1 else 0 end,
			@type = UPPER(ltrim(rtrim(isnull(tipo,'')))),
			@pathSaved = UPPER(ltrim(rtrim(isnull(path,''))))
		from 
			anexos(nolock) 
		where 
			ltrim(rtrim(anexosstamp))=@stamp
		
		if(isnull(@isDic,0)=1 and @dicImages=0)
			set @fileName = ''

		-- se o caminho ja existir nao gera um novo caminho 
		if (@pathSaved = '')
		BEGIN	
			if(isnull(@isDic,0)=0)
				set @url = @path + '\' + @table +'\' + @fileName
			else
				set @url = @pathDic + '\' + @table +'\' + @fileName

			if(@type='SHARED')
				set @url = @pathToShared + '\ArquivoDigital\Shared\' + @table +'\' + @fileName
		END
		ELSE
		BEGIN
			set  @url = @pathSaved 
			set  @path = @pathSaved
		END

		if(len(@path)=0 or len(@fileName)=0)		   
	   		select getdate() AS DateMessag, 0 StatMessag, 'Missing File' AS DescMessag , '' AS pathfile
		else		   
	   		select getdate() AS DateMessag, 1 StatMessag, 'Search Ok' AS DescMessag , @url AS pathfile
		


GO
GRANT EXECUTE on dbo.up_FilesDownload TO PUBLIC
GRANT Control on dbo.up_FilesDownload TO PUBLIC
GO

