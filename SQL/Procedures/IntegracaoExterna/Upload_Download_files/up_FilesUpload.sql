/* SP Files Upload (Verify)
J.Gomes, 2020-05-13
--------------------------------

SP para validação de possibilidade de upload de documento, através da pesquisa dos parâmetros 
relacionados com o documento ou com o cliente, e feedback com valores de:
	DateMessag, StatMessage, DescMessage, stampId, pathfile, possiblefile, maxsizefile

Parametros do ficheiro obrigatórios:
	@FilesInfo_type		(valores possíveis: 1,2,3,4,5,6)
	@FilesInfo_module	(valores possíveis: 1,2,3)	
	@FilesInfo_site

	Combinações @FilesInfo_type	/ @FilesInfo_module :
		(1/1), (2/1), (3/2,3), (4/2,3), (5/2,3), (6/2,3)

Parametros do documento, se colocado terão de ser preenchidos os 4 campos.
Além disso, se preenchidos ignoram o preenchimento dos dados do cliente:
	@DocInfo_number
	@DocInfo_seriesNumber
	@DocInfo_year
	@DocInfo_type (valores possíveis: TS e TD)

Parametros do cliente:
	@ClientInfo_number e @ClientInfo_dep	ou
	@ClientInfo_vatNr						ou
	@ClientInfo_email


Pesquisa nas tabela: 	
B_Parameters_site, bo, ft, b_utentes
	


OUTPUT:
---------
DateMessag				StatMessag	DescMessag								stampId	pathfile	possiblefile	maxsizefile
2020-05-12 14:46:04.897	0			Wrong or Missing parameters/Not Find 	NULL			


DateMessag				StatMessag	DescMessag	stampId						pathfile						possiblefile								maxsizefile
2020-05-12 15:06:58.103	1			Search Ok	ADMBD63F965-F5DE-405A-8B5	\\st01\ArquivoDigital\LTDEV30	JPG GIF BMP XLS DOC XLSX DOCX PDF PNG TXT	100.00000


StatMessag: 
	0 - ERROR
	1 - OK
	
DescMessag: 	
	1 - Wrong or Missing parameters 
	2 - Wrong or Missing parameters/Not Find 
	


EXECUÇÃO DA SP:
---------------
exec up_FilesUpload '@FilesInfo_type'
	,'@FilesInfo_module'
	,'@FilesInfo_site','@DocInfo_number','@DocInfo_seriesNumber','@DocInfo_year	','@DocInfo_type'
	,'@ClientInfo_number','@ClientInfo_dep' ,'@ClientInfo_vatNr	','@ClientInfo_email'


	exec up_FilesUpload  '1','1','Loja 1','341','5','2019','TS','','','',''
	exec up_FilesUpload  '1','1','Loja 1','341','5','2019','TS','216','','',''
	exec up_FilesUpload  '3','3','Loja 1','','','','','215','0','6666','dgdg'
	exec up_FilesUpload  '3','3','Loja 1','','','','','','','123456780','dgdg'
	exec up_FilesUpload  '3','3','Loja 1','','','','','','','','filiparesende@logitools.pt'
	exec up_FilesUpload  '3','3','Loja 1','','','','','','','',''
	
	exec up_FilesUpload  '3','3','Loja 1','86','75','2020','TD','','','',''
	

	*/
	
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


IF OBJECT_ID('[dbo].[up_FilesUpload]') IS NOT NULL
		DROP PROCEDURE [dbo].[up_FilesUpload] ;
GO


CREATE PROCEDURE [dbo].[up_FilesUpload]	
		 @FilesInfo_type				VARCHAR(3)
		 --,@FilesInfo_keyword			VARCHAR(254)
		 --,@FilesInfo_obs				VARCHAR(254)
		 ,@FilesInfo_module				VARCHAR(3)
		 --,@FilesInfo_binary				VARCHAR(4000)

		 --,@FilesInfo_extension			VARCHAR(10)
		 --,@FilesInfo_filename			VARCHAR(200)
		 ,@FilesInfo_site				VARCHAR(20)

		 ,@DocInfo_number				VARCHAR(15)	= NULL
		 ,@DocInfo_seriesNumber			VARCHAR(10) = NULL
		 ,@DocInfo_year					VARCHAR(4) = NULL
		 ,@DocInfo_type					VARCHAR(10) = NULL

		 ,@ClientInfo_number			VARCHAR(15)	= NULL
		 ,@ClientInfo_dep				VARCHAR(2)	= NULL
		 ,@ClientInfo_vatNr				VARCHAR(12)	= NULL
		 ,@ClientInfo_email				VARCHAR(100) = NULL
		 		 
	/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

SET @FilesInfo_type			=   rtrim(ltrim(isnull(@FilesInfo_type,'')))
--SET @FilesInfo_keyword		=   rtrim(ltrim(isnull(@FilesInfo_keyword,'')))
--SET @FilesInfo_obs			=   rtrim(ltrim(isnull(@FilesInfo_obs,'')))
SET @FilesInfo_module		=   rtrim(ltrim(isnull(@FilesInfo_module,'')))
--SET @FilesInfo_binary		=   rtrim(ltrim(isnull(@FilesInfo_binary,'')))
--SET @FilesInfo_extension	=   rtrim(ltrim(isnull(@FilesInfo_extension,'')))
--SET @FilesInfo_filename		=   rtrim(ltrim(isnull(@FilesInfo_filename,'')))
SET @FilesInfo_site			=   rtrim(ltrim(isnull(@FilesInfo_site,'')))

SET @DocInfo_number			=   rtrim(ltrim(isnull(@DocInfo_number,'')))
SET @DocInfo_seriesNumber	=   rtrim(ltrim(isnull(@DocInfo_seriesNumber,'')))
SET @DocInfo_year			=   rtrim(ltrim(isnull(@DocInfo_year,'')))
SET @DocInfo_type			=   rtrim(ltrim(isnull(@DocInfo_type,'')))

SET @ClientInfo_number		=   rtrim(ltrim(isnull(@ClientInfo_number,'')))
SET @ClientInfo_dep			=   rtrim(ltrim(isnull(@ClientInfo_dep,'')))
SET @ClientInfo_vatNr		=   rtrim(ltrim(isnull(@ClientInfo_vatNr,'')))
SET @ClientInfo_email		=   rtrim(ltrim(isnull(@ClientInfo_email,'')))


IF (len(@FilesInfo_type) > 0 and len(@FilesInfo_module) > 0 and len(@FilesInfo_site) > 0) 
	AND
	(
		(@FilesInfo_type in (1,2) and @FilesInfo_module in (1)) 
			OR (@FilesInfo_type in (3,4,5,6) and @FilesInfo_module in (2,3))		
	)

BEGIN	
	DECLARE @stamp varchar(25) = ''
	DECLARE @table varchar(10) = ''

	DECLARE @pathfile varchar(250)
	DECLARE @possiblefile varchar(250)
	DECLARE @maxsizefile varchar(250)

	select @pathfile = textValue from B_Parameters_site(nolock) where stamp='ADM0000000010' and site = @FilesInfo_site
	select @possiblefile = textValue from B_Parameters_site(nolock) where stamp='ADM0000000012' and site = @FilesInfo_site
	select @maxsizefile = numValue from B_Parameters_site(nolock) where stamp='ADM0000000014' and site = @FilesInfo_site

	
	--ler informação DocInfo
	if len(@DocInfo_number) > 0 and len(@DocInfo_seriesNumber) > 0 and len(@DocInfo_year) > 0 and len(@DocInfo_type) > 0
	BEGIN
		if @DocInfo_type = 'TS'
		begin
			--TS
			select  @stamp = bostamp, @table = 'documentos'  from bo (nolock) where 
				obrano = @DocInfo_number
				and ndos = @DocInfo_seriesNumber
				and boano = @DocInfo_year
				and site = @FilesInfo_site			
		end
		else 
			if @DocInfo_type = 'TD'
			begin
				--TD
				select @stamp = ftstamp, @table = 'faturacao' from ft (nolock) where 
					fno = @DocInfo_number
					and ndoc = @DocInfo_seriesNumber
					and ftano = @DocInfo_year
					and site = @FilesInfo_site			
			end
			else
				print('Wrong parameters')
		
		--print (@stamp)
		
	END
	ELSE		
	--ler informação ClientInfo no/estab
		IF (len(@ClientInfo_number) > 0 and len(@ClientInfo_dep) > 0) 
		BEGIN
			select @stamp = utstamp, @table = 'b_utentes' from b_utentes(nolock) where no = @ClientInfo_number and estab = @ClientInfo_dep			
			--print (@stamp)			
		END
		ELSE		
			--ler informação ClientInfo ncont
			IF len(@ClientInfo_vatNr) > 0 
			BEGIN
				select @stamp = utstamp, @table = 'b_utentes' from b_utentes(nolock) where ncont = @ClientInfo_vatNr				
				--print (@stamp)			
			END
			ELSE		
			--ler informação ClientInfo email
				IF len(@ClientInfo_email) > 0
				BEGIN
					select @stamp = utstamp, @table = 'b_utentes' from b_utentes(nolock) where email = @ClientInfo_email
					--print (@stamp)			
				END
				ELSE
					print('Missing parameters')

/*
print (@stamp)
if len(@stamp) > 10
begin
	print @pathfile 
	print @possiblefile 
	print @maxsizefile
end
*/


select getdate() AS DateMessag
, case when len(ltrim(rtrim(@stamp))) = 0  then 0 else 1 END AS StatMessag
, case when len(ltrim(rtrim(@stamp))) = 0  then 'Wrong or Missing parameters/Not Find' else 'Search Ok' END AS DescMessag 
, @stamp AS stampId
, @table AS tableName
, case when len(ltrim(rtrim(@stamp))) > 0 then rtrim(ltrim(isnull(@pathfile,''))) + '\' + @table else '' END  AS pathfile
, case when len(ltrim(rtrim(@stamp))) > 0 then rtrim(ltrim(isnull(@possiblefile,''))) else '' ENd  AS possiblefile
, case when len(ltrim(rtrim(@stamp))) > 0 then rtrim(ltrim(isnull(@maxsizefile,0))) else '' END  AS maxsizefile



END
ELSE
	BEGIN
		--print('Wrong/Missing parameters')		
		select getdate() AS DateMessag
		, 0 StatMessag
		, 'Wrong/Missing parameters' AS DescMessag 
		, '' AS stampId
		, '' AS tableName
		, '' AS pathfile
		, '' AS possiblefile
		, '' AS maxsizefile


	END


GO
GRANT EXECUTE on dbo.up_FilesUpload TO PUBLIC
GRANT Control on dbo.up_FilesUpload TO PUBLIC
GO