/* SP Files Upload (Insert)
J.Gomes, 2020-05-11
--------------------------------

SP para upload de documento sendo inserido registo na tabela dos Anexos.

Parametros obrigatórios:
	 @FilesInfo_extension, @FilesInfo_filename		 
		  , @FilesInfo_stampId, @FilesInfo_tableName, @FilesInfo_site	

Parametros não obrigatórios:
	@FilesInfo_keyword, @FilesInfo_obs


Pesquisa nas tabela: 	
B_Parameters_site, empresa, anexos
	


OUTPUT:
---------
DateMessag				StatMessag	DescMessag			DesignInfo_designation	IdInfo_stamp				IdInfo_url																			operationInfo_creationDate	operationInfo_creationTime	operationInfo_lastChangeDate	operationInfo_lastChangeTime
2020-05-13 12:44:10.380	1			Document Inserted	file_jorge.pdf			71B28723-5C8C-4EB1-86FF-3	https://app.lts.pt:50001/api/erp/file/donwload/LTDEV30/71B28723-5C8C-4EB1-86FF-3	2020-05-13					12:44:10					2020-05-13						12:44:10
	


EXECUÇÃO DA SP:
---------------
exec up_FilesUploadInsert 
	'@FilesInfo_keyword', '@FilesInfo_obs', '@FilesInfo_extension'
	, '@FilesInfo_filename', '@FilesInfo_stampId', '@FilesInfo_tableName', '@FilesInfo_site'	

	exec up_FilesUploadInsert  'teste','teste jg','pdf','file_jorge','ADMBD63F965-F5DE-405A-8B5','documentos','Loja 1','2020-01-01',null
	
	
	select  * from anexos where regstamp='ADMBD63F965-F5DE-405A-8B5' 
    delete from anexos where regstamp='ADMBD63F965-F5DE-405A-8B5' 

	*/
	
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


IF OBJECT_ID('[dbo].[up_FilesUploadInsert]') IS NOT NULL
		DROP PROCEDURE [dbo].[up_FilesUploadInsert] ;
GO


CREATE PROCEDURE [dbo].[up_FilesUploadInsert]			
		  @FilesInfo_keyword			VARCHAR(254)
		 ,@FilesInfo_obs				VARCHAR(254)		
		 ,@FilesInfo_extension			VARCHAR(10)
		 ,@FilesInfo_filename			VARCHAR(200)		 
		 ,@FilesInfo_stampId			VARCHAR(200)
		 ,@FilesInfo_tableName			VARCHAR(200)
		 ,@FilesInfo_site				VARCHAR(20)
		 ,@FilesInfo_expDate			VARCHAR(20) = null
		 ,@FilesInfo_type			    VARCHAR(20) = null
		
		 		 
	/* WITH ENCRYPTION */
AS

SET NOCOUNT ON


SET @FilesInfo_keyword		=   rtrim(ltrim(isnull(@FilesInfo_keyword,'')))
SET @FilesInfo_obs			=   rtrim(ltrim(isnull(@FilesInfo_obs,'')))
SET @FilesInfo_extension	=   rtrim(ltrim(isnull(@FilesInfo_extension,'')))
SET @FilesInfo_filename		=   rtrim(ltrim(isnull(@FilesInfo_filename,'')))
SET @FilesInfo_stampId		=   rtrim(ltrim(isnull(@FilesInfo_stampId,'')))
SET @FilesInfo_tableName	=   rtrim(ltrim(isnull(@FilesInfo_tableName,'')))
SET @FilesInfo_site			=   rtrim(ltrim(isnull(@FilesInfo_site,'')))
SET @FilesInfo_expDate		=   rtrim(ltrim(isnull(@FilesInfo_expDate,'1900-01-01')))
SET @FilesInfo_type		    =   isnull(@FilesInfo_type,0)


DECLARE @operator		    VARCHAR(8) = 'ONL'
DECLARE @typeDescr		    VARCHAR(30) = ''


IF  len(@FilesInfo_extension) > 0 and len(@FilesInfo_filename) > 0	
	and len(@FilesInfo_stampId) > 0 and len(@FilesInfo_tableName) > 0 and len(@FilesInfo_site) > 0 
	
BEGIN	
	BEGIN TRANSACTION;
		DECLARE @pathfile varchar(250)	
		select @pathfile = textValue from B_Parameters_site(nolock) where stamp='ADM0000000010' and site = @FilesInfo_site

		
		DECLARE @downloadURL VARCHAR(100)
		SELECT @downloadURL = rtrim(ltrim(textValue)) FROM B_Parameters(nolock)  WHERE stamp='ADM0000000313'

		DECLARE @id_lt varchar(100)	
		select @id_lt = rtrim(ltrim(id_lt)) from empresa(nolock)  where site = @FilesInfo_site

		DECLARE @url varchar(500)
		set @url = rtrim(ltrim(replace(@pathfile,' ',''))) + '\' + @FilesInfo_filename + '.' + @FilesInfo_extension

		DECLARE @lastChangeDate		datetime		
		DECLARE @CreationDate		datetime					
		SET @lastChangeDate		= getdate()		
		SET @CreationDate		= getdate()

		select top 1 @typeDescr=left(isnull(ltrim(rtrim(campo)),''),30) from b_multidata(nolock) where tipo='ANEXOTIPO' and code_motive = @FilesInfo_type

			

		DECLARE @anexosstamp CHAR(30)  
		SET @anexosstamp = (select left(newid(),30))

		insert into Anexos (anexosstamp, regstamp, tabela, filename, descr,	keyword, protected
				, ousrdata, ousrinis,	usrdata, usrinis, tipo,	validade) 
		Values (@anexosstamp, @FilesInfo_stampId, @FilesInfo_tableName, @FilesInfo_filename + '.' + @FilesInfo_extension
				, @FilesInfo_obs, @FilesInfo_keyword, 0
				, @CreationDate, @operator, @lastChangeDate, @operator,@typeDescr,@FilesInfo_expDate)					

	COMMIT TRANSACTION;	
	

/*
print (@stamp)
if len(@stamp) > 10
begin
	print @pathfile 
	print @possiblefile 
	print @maxsizefile
end


*/


select getdate() AS DateMessag
, 1 AS StatMessag
, 'Document Inserted' AS DescMessag 
, @FilesInfo_filename + '.' + @FilesInfo_extension AS DesignInfo_designation
, @anexosstamp AS IdInfo_stamp
--, @url AS IdInfo_url
, @downloadURL + @id_lt + '/' + @anexosstamp AS IdInfo_url
, convert(varchar, @CreationDate, 23) AS operationInfo_creationDate
, convert(varchar(8), @CreationDate, 24) AS operationInfo_creationTime
, convert(varchar, @lastChangeDate, 23) AS operationInfo_lastChangeDate
, convert(varchar(8), @lastChangeDate, 24) AS operationInfo_lastChangeTime
, @operator AS OperationInfo_LastChangeUser




END
ELSE
	BEGIN
		print('Wrong/Missing parameters')		
		/*
		select getdate() AS DateMessag
		, 0 StatMessag
		, 'Wrong/Missing parameters' AS DescMessag 
		
		, left(@FilesInfo_binary,6) AS FilesInfo_binary
		, @FilesInfo_extension AS FilesInfo_extension
		, @FilesInfo_filename AS FilesInfo_filename
		*/

	END


GO
GRANT EXECUTE on dbo.up_FilesUploadInsert TO PUBLIC
GRANT Control on dbo.up_FilesUploadInsert TO PUBLIC
GO


