/* SP Pesquisa Documentos
Daniel Almeida, 2020-04-24
--------------------------------

Listagem de moedas suportadas pelo software


Pesquisa nas tabelas: 	
b_parameters e CB

select textValue from B_Parameters where stamp='ADM0000000260'
SELECT distinct MOEDA FROM CB (nolock)

OUTPUT: currencies
---------



EXECUÇÃO DA SP:
---------------
exec up_Documentos_pesquisaMoedas 

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_pesquisaMoedas]') IS NOT NULL
	DROP PROCEDURE [dbo].up_Documentos_pesquisaMoedas;
GO

CREATE PROCEDURE [dbo].[up_Documentos_pesquisaMoedas]	

	   
/* WITH ENCRYPTION */
AS

SET NOCOUNT ON



If OBJECT_ID('tempdb.dbo.#temp_currency') IS NOT NULL
		drop table #temp_currency;		



		SELECT distinct currency
		FROM
		(
			select 
				rtrim(ltrim(left(textValue,4))) as currency
			from 
				B_Parameters(nolock)
			where stamp='ADM0000000260'

			union all

			SELECT  
				left(MOEDA,4)  as currency
			FROM CB (nolock)

		) t





If OBJECT_ID('tempdb.dbo.#temp_currency') IS NOT NULL
		drop table temp_currency ;

GO
GRANT EXECUTE on dbo.up_Documentos_pesquisaMoedas TO PUBLIC
GRANT Control on dbo.up_Documentos_pesquisaMoedas TO PUBLIC
GO