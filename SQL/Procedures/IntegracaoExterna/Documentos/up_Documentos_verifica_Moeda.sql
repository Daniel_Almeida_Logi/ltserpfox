/* SP Pesquisa Verifica Moeda
Jorge Gomes, 2020-04-28
--------------------------------

Pesquisa nas tabelas: 	
CB, B_Parameters


OUTPUT: 
---------
DateMessag				StatMessag	DescMessag		moeda
2020-04-28 11:44:24.370	1			Moeda existe	EURO

DateMessag				StatMessag	DescMessag			moeda
2020-04-28 11:44:44.777	0			Moeda não existe	eurs


EXECUÇÃO DA SP:
---------------
exec up_Documentos_verifica_Moeda 'euro'


*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_verifica_Moeda]') IS NOT NULL
	DROP PROCEDURE [dbo].up_Documentos_verifica_Moeda;
GO

CREATE PROCEDURE [dbo].[up_Documentos_verifica_Moeda]	
	 @moeda		varchar(4)
		
	   
/* WITH ENCRYPTION */
AS

SET @moeda				=   rtrim(ltrim(isnull(@moeda,'')))

DECLARE @contador		int
DECLARE @moeda_			varchar(4)

IF len(@moeda) > 0 
begin
	SELECT @moeda_= moeda from 
	(	
	SELECT left(textValue,4) AS moeda from B_Parameters (nolock) where stamp='ADM0000000260'
	union all
	SELECT left(MOEDA,4) as moeda FROM CB (nolock)	
	) x
	where moeda = @moeda

	select @contador=@@ROWCOUNT	

	If @contador > 0 
		select getdate() AS DateMessag, 1 As StatMessag, 'Moeda existe' As DescMessag, @moeda_ AS moeda;	
	ELSE	
		select getdate() AS DateMessag, 0 As StatMessag, 'Moeda não existe' As DescMessag, @moeda AS moeda;	
	   	
end
else
begin
	select getdate() AS DateMessag, 0 As StatMessag, 3 AS ActionMessage, 'NO DATA' As DescMessag, @moeda AS moeda;
end

GO
GRANT EXECUTE on dbo.up_Documentos_verifica_Moeda TO PUBLIC
GRANT Control on dbo.up_Documentos_verifica_Moeda TO PUBLIC
GO