/* SP Pesquisa Verifica Loja
Daniel Almeida, 2021-01-17
--------------------------------

Pesquisa nas tabelas: 	
 empresa

OUTPUT: 
---------
DateMessag				StatMessag	DescMessag		   site
2020-04-28 11:08:52.610	1			Operador existe		Loja 1

DateMessag				StatMessag	DescMessag			        site
2020-04-28 11:07:38.063	0			Operador  não existe	   	Loja 1


EXECUÇÃO DA SP:
---------------
exec up_Documentos_verifica_operadorLogistico 'Loja 1',null
exec up_Documentos_verifica_operadorLogistico 'Loja 1'
exec up_Documentos_verifica_operadorLogistico 'Loja 1',1
exec up_Documentos_verifica_operadorLogistico 'Loja 1',20


select * from empresa(nolock)  where  site='Loja 1'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_verifica_operadorLogistico]') IS NOT NULL
	DROP PROCEDURE [dbo].up_Documentos_verifica_operadorLogistico;
GO

CREATE PROCEDURE [dbo].up_Documentos_verifica_operadorLogistico	
	 @site		VARCHAR(18),
	 @id		int = 0

	
	   
/* WITH ENCRYPTION */
AS


SET @site				=   rtrim(ltrim(isnull(@site,'')))
SET @id				    =   isnull(@id,0)


	DECLARE @iddistribuidor AS tinyint
	SET @iddistribuidor = (select top 1 isnull(iddistribuidor,0) from CodigoPostal_Distribuidor (nolock) where iddistribuidor = @id)	
	

	If @iddistribuidor > 0 
		select getdate() AS DateMessag, 1 As StatMessag, 'Distribuidor existe' As DescMessag,  @site as site;	
	ELSE	
		select getdate() AS DateMessag, 0 As StatMessag, 'Distribuidor não existe' As DescMessag,  @site as site;	
	   	

GO
GRANT EXECUTE on dbo.up_Documentos_verifica_operadorLogistico TO PUBLIC
GRANT Control on dbo.up_Documentos_verifica_operadorLogistico TO PUBLIC
GO