/****** Object:  UserDefinedFunction [dbo].[ListaEstab]    Script Date: 24/04/2020 15:20:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


if OBJECT_ID('[dbo].[ListaRefDistintasFn]') IS NOT NULL
    DROP FUNCTION [dbo].[ListaRefDistintasFn]
GO




-- =============================================
-- Author:		<JG>
-- Create date: <24-04-2020>
-- Description:	<calcular total de ref distintas para determinado bostamp>
-- =============================================
CREATE FUNCTION [dbo].[ListaRefDistintasFn] 
(	
	@stamp varchar(30)
)
RETURNS int
AS
BEGIN
	declare @aa int
	set @aa = 0
	select @aa =
		count(distinct ref) from fn where fostamp = ''+@stamp+''
	
	RETURN @aa
END
GO


