/* SP Pesquisa Verifica Entidade
Jorge Gomes, 2020-04-28
--------------------------------

Pesquisa nas tabelas: 	
fl, b_utentes


OUTPUT: 
---------
DateMessag				StatMessag	DescMessag		number	dep
2020-04-28 11:31:12.800	1			Entidade existe	201		1

DateMessag				StatMessag	DescMessag			number	dep
2020-04-28 11:26:45.130	0			Entidade não existe	1		1


EXECUÇÃO DA SP:
---------------
exec up_Documentos_verifica_Entidade 201,1,'Cliente'

select * from fl where no = 201 and estab = 1
select * from b_utentes where no = 201 and estab = 1

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_verifica_Entidade]') IS NOT NULL
	DROP PROCEDURE [dbo].up_Documentos_verifica_Entidade;
GO

CREATE PROCEDURE [dbo].[up_Documentos_verifica_Entidade]	
	 @number		int
	,@dep			int
	,@type          VARCHAR(20)
	
	   
/* WITH ENCRYPTION */
AS

SET @number				=   rtrim(ltrim(isnull(@number,'')))
SET @dep				=   rtrim(ltrim(isnull(@dep,'')))

DECLARE @contador		int


set @type  = isnull(@type,'')


IF len(@number) > 0 and len(@dep) > 0 and len(@type)>0
begin
	
	if(ltrim(rtrim(@type))) = "cliente"
	begin
		select   @contador =count(no) from b_utentes (nolock)
		where  no=@number and estab=@dep
	end

	if(ltrim(rtrim(@type))) = "fornecedor"
	begin
		select   @contador =count(no) from fl (nolock)
		where  no=@number and estab=@dep
	end


	If @contador > 0 
		select getdate() AS DateMessag, 1 As StatMessag, 'Entidade existe' As DescMessag, @number AS number, @dep as dep;	
	ELSE	
		select getdate() AS DateMessag, 0 As StatMessag, 'Entidade não existe' As DescMessag,  @number AS number, @dep as dep;	
	   	
end
else
begin
	select getdate() AS DateMessag, 0 As StatMessag, 3 AS ActionMessage, 'NO DATA' As DescMessag,   @number AS number, @dep as dep;
end

GO
GRANT EXECUTE on dbo.up_Documentos_verifica_Entidade TO PUBLIC
GRANT Control on dbo.up_Documentos_verifica_Entidade TO PUBLIC
GO