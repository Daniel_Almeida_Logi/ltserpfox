/* SP Pesquisa Verifica Loja
Jorge Gomes, 2020-04-28
--------------------------------

Pesquisa nas tabelas: 	
 empresa

OUTPUT: 
---------
DateMessag				StatMessag	DescMessag		site
2020-04-28 11:08:52.610	1			Site existe		Loja 1

DateMessag				StatMessag	DescMessag			site
2020-04-28 11:07:38.063	0			Site  não existe	   	Loja 1


EXECUÇÃO DA SP:
---------------
exec up_Documentos_verifica_site 'Loja 1'


select * from empresa(nolock)  where  site='Loja 1'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_verifica_site]') IS NOT NULL
	DROP PROCEDURE [dbo].up_Documentos_verifica_site;
GO

CREATE PROCEDURE [dbo].up_Documentos_verifica_site	
	 @site		VARCHAR(18)

	
	   
/* WITH ENCRYPTION */
AS


SET @site				=   rtrim(ltrim(isnull(@site,'')))

DECLARE @contador		int



IF len(@site) > 0
begin
	DECLARE @siteno AS tinyint
	SET @siteno = (select  isnull(no,0) from empresa (nolock) where site = @site)	
	

	If @siteno > 0 
		select getdate() AS DateMessag, 1 As StatMessag, 'Site existe' As DescMessag,  @site as site;	
	ELSE	
		select getdate() AS DateMessag, 0 As StatMessag, 'Site não existe' As DescMessag,  @site as site;	
	   	
end
else
begin
	select getdate() AS DateMessag, 0 As StatMessag, 3 AS ActionMessage, 'NO DATA' As DescMessag, @site as site;
end

GO
GRANT EXECUTE on dbo.up_Documentos_verifica_site TO PUBLIC
GRANT Control on dbo.up_Documentos_verifica_site TO PUBLIC
GO