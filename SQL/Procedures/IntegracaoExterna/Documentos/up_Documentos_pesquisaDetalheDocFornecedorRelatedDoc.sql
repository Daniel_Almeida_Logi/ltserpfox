/* SP Pesquisa De Documentos de fornecedor relactionados
D.Almeida, 2020-05-25
--------------------------------

Listagem de documentos relacionados com o documento em questão.
Os documentos podem ser Origem ou Destino, conforme estejam na origem do documento 
em questão ou na criação de novos documentos a partir deste.

Pesquisa nas tabela: 	
fi, ft, fn, bf,

por 2 campos:
	@oFnstamp, @fnStamp,
	
Campos obrigatórios: pelo menos um stamp

OUTPUT:
---------
RelatedDoc_number,	RelatedDoc_site,	RelatedDoc_seriesNumber,	RelatedDoc_extCode
,	RelatedDoc_docType,	RelatedDoc_qttOrigDest,	RelatedDoc_relatedType, RelatedDoc_year


EXECUÇÃO DA SP:
---------------
exec up_Documentos_pesquisaDetalheDocFornecedorRelatedDoc @oBistamp, @bistamp
	
	exec up_Documentos_pesquisaDetalheDocFornecedorRelatedDoc  
		NULL, 'ADM32F755CB-845B-449B-B41',''


		EXEC up_Documentos_pesquisaDetalheDocFornecedorRelatedDoc null, 'ADM43C50397-8A21-4F76-895',null
		EXEC up_Documentos_pesquisaDetalheDocFornecedorRelatedDoc null, null,null
		EXEC up_Documentos_pesquisaDetalheDocFornecedorRelatedDoc null, 'ADM43C50397-8A21-4F76-895','ADM43C50397-8A21-4F76-895'

		EXEC up_Documentos_pesquisaDetalheDocFornecedorRelatedDoc 'ADM43C50397-8A21-4F76-895', null,null
		
	

	*/
	
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


IF OBJECT_ID('[dbo].[up_Documentos_pesquisaDetalheDocFornecedorRelatedDoc]') IS NOT NULL
		DROP PROCEDURE [dbo].up_Documentos_pesquisaDetalheDocFornecedorRelatedDoc ;
GO


CREATE PROCEDURE [dbo].up_Documentos_pesquisaDetalheDocFornecedorRelatedDoc	
		@oFnstamp as varchar(25),
		@fnStamp  as varchar(25),	
		@biStamp  as varchar(25)


AS
SET NOCOUNT ON

SET @oFnstamp	=   rtrim(ltrim(isnull(@oFnstamp,'naoexiste')))
SET @fnStamp	=   rtrim(ltrim(isnull(@fnStamp,'naoexiste')))
SET @biStamp	=   rtrim(ltrim(isnull(@biStamp,'naoexiste')))



IF len(@oFnstamp) > 9 or len(@biStamp)>9 or len(@fnStamp)>9
BEGIN	
	DECLARE @sql  varchar(MAX) = ''
	declare @sql1 varchar(MAX) = ''
	declare @sql2 varchar(MAX) = ''
	declare @sql3 varchar(MAX) = ''

	IF len(@oFnstamp) > 15
	BEGIN
		set @sql1 = N' 	
			 
			CTE1 AS (
				Select
					Convert(VARCHAR(100), [fn].[adoc]) AS RelatedDoc_number
					, fo.[Site] AS RelatedDoc_site
					, fo.doccode As RelatedDoc_seriesNumber
					, '''' As RelatedDoc_extCode
					, ''cm1'' AS RelatedDoc_docType
					, dbo.unityConverter(  fn.qtt ,''qttUnityToBox'', empresa.no ,fn.ref) AS RelatedDoc_qttOrigDest
					, ''Orig'' AS RelatedDoc_relatedType
					, fo.foano as RelatedDoc_year
				From	
					fn (nolock) 
					Inner Join fo (nolock) On fo.fostamp = fn.fostamp
					LEFT JOIN empresa(nolock) ON empresa.site = fo.site
				Where	
					fn.fnStamp = ''' + @oFnstamp + ''' 
			) '
	END
		


	
	IF len(@bistamp) > 15
	BEGIN
	
		set @sql2 = @sql2 +  N' 	
		  CTE2 AS (
			Select
				 Convert(VARCHAR(100), [Bi].[obrano]) AS RelatedDoc_number
				, Bo.[Site] AS RelatedDoc_site
				, Bi.ndos As RelatedDoc_seriesNumber
				, '''' As RelatedDoc_extCode
				, ''TS'' AS RelatedDoc_docType
				, dbo.unityConverter(  Bi.qtt ,''qttUnityToBox'', empresa.no ,bi.ref) AS RelatedDoc_qttOrigDest
				, ''Dest'' AS RelatedDoc_relatedType
				, bo.boano as RelatedDoc_year
			From	
				Bi (nolock) 	
				Inner Join Bo (nolock) On Bo.bostamp = Bi.bostamp
				LEFT JOIN empresa(nolock) ON empresa.site = bo.site
			Where	
				bistamp =  ''' + @bistamp + ''' 
		) '
	

	END

	IF len(@fnStamp) > 15
	BEGIN
		set @sql3 = @sql3 + N' 	
			 
			CTE3 AS (
				Select
					Convert(VARCHAR(100), [fn].[adoc]) AS RelatedDoc_number
					, fo.[Site] AS RelatedDoc_site
					, fo.doccode As RelatedDoc_seriesNumber
					, '''' As RelatedDoc_extCode
					, ''cm1'' AS RelatedDoc_docType
					, dbo.unityConverter(  fn.qtt ,''qttUnityToBox'', empresa.no ,fn.ref) AS RelatedDoc_qttOrigDest
					, ''DEST'' AS RelatedDoc_relatedType
					, fo.foano as RelatedDoc_year
				From	
					fn (nolock) 
					Inner Join fo (nolock) On fo.fostamp = fn.fostamp
					LEFT JOIN empresa(nolock) ON empresa.site = fo.site
				Where	
					fn.ofnStamp = ''' + @fnStamp + ''' 
			) '
	END



	if(LEN(@sql1)>1)
	begin
		set @sql1 = N' WITH ' + @sql1 ;
		set @sql = @sql + @sql1
	end


	if(LEN(@sql2)>1)
	begin
		if(LEN(@sql)=0)					
			set @sql2 = N' WITH ' + @sql2 ;
		else
			set @sql2 = N' ,  ' + @sql2 ;
		
		set @sql = @sql + @sql2
	end


	if(LEN(@sql3)>1)
	begin
		if(LEN(@sql)=0)					
			set @sql3 = N' WITH ' + @sql3 ;
		else
			set @sql3 = N' ,  ' + @sql3 ;
		
		set @sql = @sql + @sql3
	end


	if(LEN(@sql1)>1)
		set @sql = @sql + N' Select * From CTE1  ';

	if(LEN(@sql2)>1 and LEN(@sql1)=0)
		set @sql = @sql + N' Select * From CTE2  ';

	if(LEN(@sql2)>1 and LEN(@sql1)>0)
		set @sql = @sql + N'UNION ALL   Select * From CTE2  ';

	if((LEN(@sql1)>0 or LEN(@sql2)>0)  and  LEN(@sql3)>0 )
		set @sql = @sql + N'UNION ALL   Select * From CTE3  ';
	
	if(LEN(@sql1)=0 and LEN(@sql2)=0  and  LEN(@sql3)>0 )
		set @sql = @sql + N'Select * From CTE3  ';
		

	print @sql
	EXECUTE (@sql)
END
ELSE
	BEGIN
		print('Missing stamp')
	END

GO
GRANT EXECUTE on dbo.up_Documentos_pesquisaDetalheDocFornecedorRelatedDoc TO PUBLIC
GRANT Control on dbo.up_Documentos_pesquisaDetalheDocFornecedorRelatedDoc TO PUBLIC
GO