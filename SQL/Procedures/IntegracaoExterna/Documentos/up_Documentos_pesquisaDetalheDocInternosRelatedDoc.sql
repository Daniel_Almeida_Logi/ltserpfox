/* SP Pesquisa De Documentos relactionados
D.Almeida, 2020-05-25
--------------------------------

Listagem de documentos relacionados com o documento em questão.
Os documentos podem ser Origem ou Destino, conforme estejam na origem do documento 
em questão ou na criação de novos documentos a partir deste.

Pesquisa nas tabela: 	
fi, ft, bi, bo,

por 4 campos:
	@oBistamp, @bistamp,
	
Campos obrigatórios: pelo menos um stamp

OUTPUT:
---------
RelatedDoc_number,	RelatedDoc_site,	RelatedDoc_seriesNumber,	RelatedDoc_extCode
,	RelatedDoc_docType,	RelatedDoc_qttOrigDest,	RelatedDoc_relatedType, RelatedDoc_year


EXECUÇÃO DA SP:
---------------
exec up_Documentos_pesquisaDetalheDocInternosRelatedDoc @oBistamp, @bistamp
	
	exec up_Documentos_pesquisaDetalheDocInternosRelatedDoc  
		 null,  null


		EXEC up_Documentos_pesquisaDetalheDocInternosRelatedDoc 'ADM09F10F0D-CB0F-4E3E-A66', null

	*/
	
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


IF OBJECT_ID('[dbo].[up_Documentos_pesquisaDetalheDocInternosRelatedDoc]') IS NOT NULL
		DROP PROCEDURE [dbo].up_Documentos_pesquisaDetalheDocInternosRelatedDoc ;
GO


CREATE PROCEDURE [dbo].up_Documentos_pesquisaDetalheDocInternosRelatedDoc	
		@oBistamp as varchar(25),
		@bistamp as varchar(25)		


AS
SET NOCOUNT ON

SET @oBistamp	=   rtrim(ltrim(isnull(@oBistamp,'naoexiste')))
SET @bistamp	=   rtrim(ltrim(isnull(@bistamp,'naoexiste')))



IF len(@oBistamp) > 10 or len(@bistamp) > 10
BEGIN	
	DECLARE @sql varchar(MAX) = ''

	IF len(@oBistamp) > 15
	BEGIN
		set @sql = N' 	
			WITH 
			CTE1 AS (
				Select
					Convert(VARCHAR(100), [Bi].[obrano]) AS RelatedDoc_number
					, Bo.[site] AS RelatedDoc_site
					, Bi.ndos As RelatedDoc_seriesNumber
					, '''' As RelatedDoc_extCode
					--BO fica TS
					, ''TS'' AS RelatedDoc_docType
					, dbo.unityConverter(Bi.qtt ,''qttUnityToBox'', empresa.no ,bi.ref) AS RelatedDoc_qttOrigDest
					, ''Orig'' AS RelatedDoc_relatedType
					, bo.boano as RelatedDoc_year
				From	
					bi (nolock) 
					Inner Join bo (nolock) On Bo.bostamp = bi.bostamp
					left Join empresa (nolock) On Bo.site = empresa.site
				Where	
					bistamp = ''' + @oBistamp + ''' 
			) '
	END
		



		
	if(len(@sql)=0)
		set @sql = @sql +  N' WITH '


	
	IF len(@Bistamp) > 15
	BEGIN
	  
		if(len(@sql)>10)
			set @sql = @sql +  N', '

	
		set @sql = @sql +  '	
		  CTE2 AS (
			Select
				 Convert(VARCHAR(100), [Bi].[obrano]) AS RelatedDoc_number
				, Bo.[site] AS RelatedDoc_site
				, Bi.ndos As RelatedDoc_seriesNumber
				, '''' As RelatedDoc_extCode
				, ''TS'' AS RelatedDoc_docType
				, dbo.unityConverter(  Bi.qtt ,''qttUnityToBox'', empresa.no ,bi.ref) AS RelatedDoc_qttOrigDest
				, ''Dest'' AS RelatedDoc_relatedType
				, bo.boano as RelatedDoc_year
			From	
				Bi (nolock)
				Inner Join Bo (nolock) On Bo.bostamp = Bi.bostamp
				left Join empresa (nolock) On Bo.site = empresa.site
			Where	
				obistamp =  ''' + @Bistamp + ''' 
		) '
	
		set @sql = @sql + N'			
		, CTE3 AS (
			Select
				 Convert(VARCHAR(100), [Fi].[fno]) AS RelatedDoc_number
				, ft.[Site] AS RelatedDoc_site
				, fi.ndoc As RelatedDoc_seriesNumber
				, '''' As RelatedDoc_extCode
				--FT fica TD
				, ''TD'' AS RelatedDoc_docType
				,  dbo.unityConverter(fi.qtt ,''qttUnityToBox'', empresa.no ,Fi.ref) AS RelatedDoc_qttOrigDest
				, ''Dest'' AS RelatedDoc_relatedType
				, ft.ftano as RelatedDoc_year
			From	
				Fi (nolock)
				Inner Join Ft (nolock) On ft.ftstamp = fi.ftstamp
				left Join empresa (nolock) On ft.site = empresa.site
			Where	
				bistamp = ''' + @Bistamp + ''' 
				or fi.fistamp = (select fistamp from bi2(nolock) where bi2stamp = ''' + @Bistamp + ''' )
		) '
	END






	IF len(@oBistamp) > 15
	BEGIN
		set @sql = @sql + N' 	
			Select * From CTE1 '
	END
		
	IF len(@Bistamp) > 15
	BEGIN
		IF len(@oBistamp) > 15
			set @sql = @sql + N' UNION ALL '

		set @sql = @sql + N' 			
			Select * From CTE2 
			UNION ALL
			Select * From CTE3'
	END



	print @sql
	EXECUTE (@sql)
END
ELSE
	BEGIN
		print('Missing stamp')
	END

GO
GRANT EXECUTE on dbo.up_Documentos_pesquisaDetalheDocInternosRelatedDoc TO PUBLIC
GRANT Control on dbo.up_Documentos_pesquisaDetalheDocInternosRelatedDoc TO PUBLIC
GO