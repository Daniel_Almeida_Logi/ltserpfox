/* SP Pesquisa Verifica Tipo Documento
Jorge Gomes, 2020-04-28
--------------------------------

Pesquisa nas tabelas: 	
ts, td, cm1


OUTPUT: 
---------
DateMessag				StatMessag	DescMessag					serieNumber	type	site
2020-04-28 12:33:36.880	0			TipoDocumento não existe	1			ts		loja 1

DateMessag				StatMessag	DescMessag					serieNumber	type	site
2020-04-28 12:34:06.767	0			TipoDocumento não existe	1			cl		loja 1

DateMessag				StatMessag	ActionMessage	DescMessag	serieNumber	type	site
2020-04-28 12:31:47.353	0			3				NO DATA					cl		loja 1


EXECUÇÃO DA SP:
---------------
exec up_Documentos_verifica_TipoDocumento '1','ts','loja 1'
exec up_Documentos_verifica_TipoDocumento '1','td','loja 1'
exec up_Documentos_verifica_TipoDocumento '1','cm1','loja 1'

exec up_Documentos_verifica_TipoDocumento '1','cl','loja 1'
exec up_Documentos_verifica_TipoDocumento '','cl','loja 1'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_verifica_TipoDocumento]') IS NOT NULL
	DROP PROCEDURE [dbo].up_Documentos_verifica_TipoDocumento;
GO

CREATE PROCEDURE [dbo].[up_Documentos_verifica_TipoDocumento]	
	 @serieNumber		VARCHAR(4)
	,@type				VARCHAR(3)
	,@site				VARCHAR(60) 
		
	   
/* WITH ENCRYPTION */
AS

SET @serieNumber		=   rtrim(ltrim(isnull(@serieNumber,'')))
SET @type				=   rtrim(ltrim(isnull(@type,'')))
SET @site				=   rtrim(ltrim(isnull(@site,'')))

DECLARE @contador			int
DECLARE @serieNumber_		int


IF len(@serieNumber) > 0 and len(@type) > 0 and len(@site) > 0
begin
	--DECLARE @siteno AS tinyint
	--SET @siteno = (select  no from empresa(nolock) where site = @site)

	IF @type='ts'	
		select @serieNumber_ = ndos from ts (nolock) where ndos= @serieNumber and disp_webservice_edit = 1
	ELSE
		if @type='td'
			select @serieNumber_ = ndoc from td (nolock) where ndoc= @serieNumber and site = @site and disp_webservice_edit = 1
		ELSE
			if @type='cm1'
				select @serieNumber_ = cm from cm1 (nolock) where cm= @serieNumber  and disp_webservice_edit = 1
			--else
				--select getdate() AS DateMessag, 0 As StatMessag, 3 AS ActionMessage, 'NO DATA' As DescMessag, @type AS type;
			
			
	select @contador=@@ROWCOUNT	

	If @contador > 0 
		select getdate() AS DateMessag, 1 As StatMessag, 'TipoDocumento  existe' As DescMessag, @serieNumber AS serieNumber, @type AS type, @site AS site;	
	ELSE	
		select getdate() AS DateMessag, 0 As StatMessag, 'TipoDocumento não existe' As DescMessag, @serieNumber AS serieNumber, @type AS type, @site AS site;	
	   	
end
else
begin
	select getdate() AS DateMessag, 0 As StatMessag, 3 AS ActionMessage, 'NO DATA' As DescMessag, @serieNumber AS serieNumber, @type AS type, @site AS site;
end

GO
GRANT EXECUTE on dbo.up_Documentos_verifica_TipoDocumento TO PUBLIC
GRANT Control on dbo.up_Documentos_verifica_TipoDocumento TO PUBLIC
GO