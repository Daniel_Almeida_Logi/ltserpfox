/* SP Pesquisa Verifica Produto
Jorge Gomes, 2020-04-28
--------------------------------

Pesquisa nas tabelas: 	
st, empresa

OUTPUT: 
---------
DateMessag				StatMessag	DescMessag		ref		site
2020-04-28 11:08:52.610	1			Produto existe	5440987	Loja 1

DateMessag				StatMessag	DescMessag			ref			site
2020-04-28 11:07:38.063	0			Produto não existe	544g0987	Loja 1


EXECUÇÃO DA SP:
---------------
exec up_Documentos_verifica_Produto 'SERV005','Atlantico'


select * from st  where  ref='5440987' and site_nr=1

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_verifica_Produto]') IS NOT NULL
	DROP PROCEDURE [dbo].up_Documentos_verifica_Produto;
GO

CREATE PROCEDURE [dbo].[up_Documentos_verifica_Produto]	
	 @ref		VARCHAR(18)
	,@site		VARCHAR(60) 
	
	   
/* WITH ENCRYPTION */
AS

SET @ref				=   rtrim(ltrim(isnull(@ref,'')))
SET @site				=   rtrim(ltrim(isnull(@site,'')))

DECLARE @contador		int
DECLARE @ref_			VARCHAR(18)



IF len(@ref) > 0 and len(@site) > 0
begin
	DECLARE @siteno AS tinyint
	SET @siteno = (select  no from empresa (nolock) where site = @site)	
	
	select 
		@ref_ = ref from st (nolock) 
	where  
		ref=@ref and site_nr=@siteno and inactivo = 0
   



	select @contador=@@ROWCOUNT	

	If @contador > 0 
		select getdate() AS DateMessag, 1 As StatMessag, 'Produto/site existe' As DescMessag, @ref AS ref, @site as site;	
	ELSE	
		select getdate() AS DateMessag, 0 As StatMessag, 'Produto/site não existe' As DescMessag,  @ref AS ref, @site as site;	
	   	
end
else
begin
	select getdate() AS DateMessag, 0 As StatMessag, 3 AS ActionMessage, 'NO DATA' As DescMessag,  @ref AS ref, @site as site;
end

GO
GRANT EXECUTE on dbo.up_Documentos_verifica_Produto TO PUBLIC
GRANT Control on dbo.up_Documentos_verifica_Produto TO PUBLIC
GO