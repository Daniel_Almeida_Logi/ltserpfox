/*
DOCUMENTOS DELETE
D.Almeida, 04-05-2020
revisão 04-05-2020
--------------------------------
remove as linhas de um documentos


*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_Documentos_line_delete]') IS NOT NULL
    drop procedure up_Documentos_line_delete
go



CREATE PROCEDURE up_Documentos_line_delete		
  	 --identificadores do documento
	 @stamp					   varchar(25)
	
AS

	delete from bo_totais where stamp = ltrim(rtrim(@stamp))
	delete from bi where bostamp = ltrim(rtrim(@stamp))
	delete from bi2 where bostamp = ltrim(rtrim(@stamp))

	select getdate() AS DateMessag, 1 As StatMessag, 'Apagado' As DescMessag

            
GO
Grant Execute On up_Documentos_line_delete to Public
Grant Control On up_Documentos_line_delete to Public
GO
			
           
			
	