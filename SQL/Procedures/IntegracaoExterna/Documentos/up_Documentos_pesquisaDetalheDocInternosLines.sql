/* SP Pesquisa Detalhe Documentos (LINES)
J.Gomes, 2020-04-23
--------------------------------

Listagem detalhe de Documentos internos baseada no campo bostamp proveniente da 
sp up_Documentos_pesquisaDetalheDocInternos.

Pesquisa nas tabelas: 	Bi

por 1 campo:
	'@stamp'
	
Campos obrigatórios: todos

OUTPUT:
---------
ProductInfo_ref,	ProductInfo_design,	StockInfo_qtt,	StockInfo_warehouseSource
,	StockInfo_warehouseDest,	StockInfo_site,	FinanceInfo_retailPrice
,	FinanceInfo_pcl,	FinanceInfo_pct,	FinanceInfo_discountsCommercPerc
,	VatRatesInfo_rate,	FinanceInfo_vatInc,	FinanceInfo_discountValue
,	GeneralInfo_obs,	VatRatesInfo_value,	VatRatesInfo_incidenceBase

EXECUÇÃO DA SP:
---------------
exec up_Documentos_pesquisaDetalheDocInternosLines '@stamp'

	exec up_Documentos_pesquisaDetalheDocInternosLines  'ADME011D779-2EE3-4EF7-914'



	EXEC up_Documentos_pesquisaDetalheDocInternosLines '0f35db001a124b07a2f503574'

	EXEC up_Documentos_pesquisaDetalheDocInternosLines N'0f35db001a124b07a2f503574'


	*/
	
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_Documentos_pesquisaDetalheDocInternosLines]') IS NOT NULL
		DROP PROCEDURE [dbo].[up_Documentos_pesquisaDetalheDocInternosLines] ;
GO




CREATE PROCEDURE [dbo].[up_Documentos_pesquisaDetalheDocInternosLines]	
		 @stamp				VARCHAR(25)
		   
	/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

SET @stamp	=   rtrim(ltrim(isnull(@stamp,'')))
Declare @site_nr int = 0
	
IF len(@stamp) > 0 
BEGIN	
	DECLARE @sql varchar(4000)


	select top 1 @site_nr= isnull(empresa.no,0) from bo(nolock)
	inner join empresa(nolock) on ltrim(rtrim(bo.site))= ltrim(rtrim(empresa.site))
	where bo.bostamp=@stamp


	select @sql = N' 				
					SELECT 	
						 bi.ref AS ProductInfo_ref
						, bi.design AS ProductInfo_design
						, dbo.unityConverter(bi.qtt,''qttUnityToBox'', ' + convert(varchar(10),@site_nr) + ',bi.ref)  AS StockInfo_qtt
						, bi.armazem AS StockInfo_warehouseSource
						, bi.ar2mazem AS StockInfo_warehouseDest
						, dbo.unityConverter(u_stockact,''stocksUnityToBox'', ' + convert(varchar(10),@site_nr) + ',bi.ref)  AS StockInfo_site	
						, cast( Bi.ettdeb  as numeric(15,2)) AS FinanceInfo_retailPrice
						, cast( Bi.edebito  as numeric(15,2)) AS FinanceInfo_unitRetailPrice									
						, u_upc * bi.qtt AS FinanceInfo_pcl 
						, bi.epcusto * bi.qtt AS FinanceInfo_pct					
						, convert(varchar,bi.desconto) + '','' + convert(varchar,bi.desc2) + 
							'','' + convert(varchar,bi.desc3) + '','' + convert(varchar,bi.desc4) AS FinanceInfo_discountsCommercPerc
						, bi.iva as VatRatesInfo_rate
						, 1 as FinanceInfo_vatInc
						, bi.descval as FinanceInfo_discountValue
						, lobs AS GeneralInfo_obs                                                
						,VatRatesInfo_value = case when BI.ivaincl = 1 then cast(bi.ettdeb - (bi.ettdeb/(bi.iva/100+1)) as  numeric(15,2))
									else  cast(bi.ettdeb*(bi.iva/100) as  numeric(15,2)) end


                        ,VatRatesInfo_incidenceBase  = case when BI.ivaincl = 1 then cast((BI.ettdeb/(BI.iva/100+1)) as  numeric(15,2))
									else cast(BI.ettdeb as  numeric(15,2)) end	
						,bi.bistamp
						,bi.obistamp
						,bi2.valeNr	as FinanceInfo_voucherNr
						,isnull(bi2.idCampanha,'''')	as FinanceInfo_campaignId
						,ltrim(rtrim(ISNULL(grande_mercado_hmr.descr,''''))) AS ProductInfo_HmrInfo_dicBigMarketHmrDescr
						,ISNULL(grande_mercado_hmr.id,0) AS ProductInfo_HmrInfo_dicBigMarketHmrId
						,ISNULL(categoria_hmr.id,0) AS ProductInfo_HmrInfo_dicCategoryHmrId
						,ltrim(rtrim(ISNULL(categoria_hmr.descr,''''))) AS ProductInfo_HmrInfo_dicCategoryHmrDescr
						,ISNULL(mercado_hmr.id,0) AS ProductInfo_HmrInfo_dicMarketHmrId
						,ltrim(rtrim(ISNULL(mercado_hmr.descr,''''))) AS ProductInfo_HmrInfo_dicMarketHmrDescr
						,ISNULL(segmento_hmr.id,0) AS ProductInfo_HmrInfo_dicSegmentHmrId
						,ltrim(rtrim(ISNULL(segmento_hmr.descr,''''))) AS ProductInfo_HmrInfo_dicSegmentHmrDescr
						,ltrim(rtrim(st.usr1)) AS ProductInfo_brand
						,rtrim(ltrim(isnull(st.designOnline,''''))) AS ProductInfo_OnlineDesignation
						,rtrim(ltrim(isnull(st.caractOnline,''''))) AS ProductInfo_OnlineDescription1
						,rtrim(ltrim(isnull(st.apresentOnline,''''))) AS ProductInfo_OnlineDescription2
						,ISNULL(fprod.generico,0) AS ProductInfo_generic
						,st.tempoIndiponibilidade	AS ProductInfo_TimeUnavailability 
						,''dia''	AS ProductInfo_UnavailabilityUnit
						,round(isnull(fi.u_txcomp,0),2)  AS FinanceInfo_coPaymentRate
						,round(isnull(u_ettent1 + u_ettent2,0),2)  AS FinanceInfo_coPaymentValue 
														
					FROM
       					[dbo].[Bi](nolock)	
						left join bi2(nolock)	             on bi2.bi2stamp = bistamp
						left join fi(nolock)	             on fi.bistamp = bi.bistamp
						LEFT JOIN [dbo].[st] (nolock)		 ON [st].[ref]=[Bi].[ref]  and   st.site_nr=''' + Convert(varchar(10),isnull(@site_nr,0)) +'''
						LEFT JOIN [dbo].[fprod] (nolock)	 ON [fprod].[cnp]=[Bi].[ref] 
						LEFT JOIN grande_mercado_hmr(nolock) ON st.u_depstamp = grande_mercado_hmr.id
						LEFT JOIN mercado_hmr(nolock)        ON st.u_secstamp = mercado_hmr.id
						LEFT JOIN categoria_hmr(nolock)      ON st.u_catstamp = categoria_hmr.id
						LEFT JOIN segmento_hmr(nolock)       ON st.u_segstamp = segmento_hmr.id				
					WHERE
       					bi.bostamp = ''' + @stamp + ''' '   
	--print @sql
	EXECUTE (@sql)
END
ELSE
	BEGIN
		print('Missing stamp')
	END

GO
GRANT EXECUTE on dbo.up_Documentos_pesquisaDetalheDocInternosLines TO PUBLIC
GRANT Control on dbo.up_Documentos_pesquisaDetalheDocInternosLines TO PUBLIC
GO

