/* SP Pesquisa Documentos
D.Almeida, 2020-04-23
--------------------------------

Listagem de Documentos de fornecedores baseada em diversos campos.
e com possibilidade de filtragem acima de determinada data e hora de alteração.

Pesquisa nas tabelas: 	
FO, FN, ts, fl, fl_site


	exec up_Documentos_pesquisaGeralDocFornecedor  null,null,null,null,null,null,null,null,null, null,null,null,null,5,null,null

*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_Documentos_pesquisaGeralDocFornecedor]') IS NOT NULL
	DROP PROCEDURE [dbo].up_Documentos_pesquisaGeralDocFornecedor ;
GO


	

CREATE PROCEDURE [dbo].up_Documentos_pesquisaGeralDocFornecedor	


	 @number				VARCHAR(10)
	,@dep					VARCHAR(3)
	,@vatNr					VARCHAR(20)
	,@ref					VARCHAR(18)
	,@design				VARCHAR(60)
	,@site					VARCHAR(60)
	,@status				VARCHAR(1)
	,@numdoc				VARCHAR(20)
	,@seriesNumber			VARCHAR(500)
	,@lastChangeDate		varchar(10) = '1900-01-01'
	,@lastChangeTime		VARCHAR(8)  = '00:00:00'
	,@dateInit				VARCHAR(10) = '1900-01-01'
	,@dateEnd				VARCHAR(10)  = '3000-01-01'
	,@pageNumber			int = 1
	,@orderStamp			VARCHAR(36) = ''
	,@topMax                int = 100
	   
/* WITH ENCRYPTION */
AS

SET NOCOUNT ON


SET @ref				=   rtrim(ltrim(isnull(@ref,'')))
SET @number				=   rtrim(ltrim(isnull(@number,'')))
SET @dep				=   rtrim(ltrim(isnull(@dep,'')))
SET @vatNr				=   rtrim(ltrim(isnull(@vatNr,'')))
SET @design				=   rtrim(ltrim(isnull(@design,'')))
SET @status				=   rtrim(ltrim(isnull(@status,'')))
SET @numdoc				=   rtrim(ltrim(isnull(@numdoc,'')))
SET @seriesNumber		=   rtrim(ltrim(isnull(@seriesNumber,'')))
SET @site				=   rtrim(ltrim(isnull(@site,'')))
SET @lastChangeDate		=   rtrim(ltrim(isnull(@lastChangeDate,'1900-01-01')))
SET @lastChangeTime		=   rtrim(ltrim(isnull(@lastChangeTime,'00:00:00')))
SET @dateInit		    =   rtrim(ltrim(isnull(@dateInit,'1900-01-01')))
SET @dateEnd		    =   rtrim(ltrim(isnull(@dateEnd,'3000-01-01')))
SET @orderStamp         =	rtrim(ltrim(ISNULL(@orderStamp,'')))
SET @topMax             =   isnull(@topMax,100)


if(isnull(@pageNumber,0)<1)
	set @pageNumber = 1

If OBJECT_ID('tempdb.dbo.#temp_documentos') IS NOT NULL
		drop table #temp_documentos;		
IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFO'))
	DROP TABLE #dadosFO	

DECLARE @sql varchar(max) = ''
DECLARE @sql1 varchar(max) = ''
DECLARE @sqlFinal varchar(max) = ''

DECLARE @orderBy VARCHAR(8000)
DECLARE @PageSize int = @topMax

if(@PageSize<0 or @PageSize>100)
	set @PageSize = 100

DECLARE @OrderCri VARCHAR(MAX) =''
Declare @site_nr int = 0

IF @orderStamp = ''
BEGIN 
	SET @OrderCri = 'ORDER BY data ASC, usrhora ASC'
END 
ELSE 
BEGIN 
	SELECT @OrderCri='ORDER BY ' + instructionSql  FROM sortingTableOrder (NOLOCK) WHERE stamp = @orderStamp and inativo=0 and nameService='internalDoc'
END 



DECLARE @id_lt varchar(100) =''	
select top 1 @site_nr=isnull(no,0),@id_lt = rtrim(ltrim(id_lt)) from empresa(nolock) where site=@site 

DECLARE @downloadURL VARCHAR(100) = ''
SELECT  @downloadURL = rtrim(ltrim(textValue)) FROM B_Parameters(nolock) WHERE stamp='ADM0000000313'

DECLARE @imagemURL varchar(300) 
set   @imagemURL = @downloadURL + @id_lt


set @orderBy = ' ) X ' + @OrderCri +'  
				 OFFSET ' + convert(varchar(10),@PageSize) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
				 FETCH NEXT  ' + convert(varchar(10),@PageSize) + ' ROWS ONLY '

select @sql = N' SELECT * INTO #temp_Documentos
				FROM (
					SELECT
						 [tabela] = ''FL''
						, cm AS seriesNumber 
						, [documento] = [cm1].[cmdesc]
						, disp_webservice_view 
					FROM
						[dbo].[cm1](nolock)		 
				) as X 
	
				DECLARE @Documentos TABLE(
					seriesNumber int		
				)

				IF '''+@seriesNumber+''' <> ''''
					BEGIN
						INSERT INTO @Documentos
						SELECT * FROM dbo.up_splitToTable('''+@seriesNumber+''','','')
					END
				ELSE
				BEGIN
					INSERT INTO @Documentos
					SELECT seriesNumber FROM #temp_Documentos
				END

			SELECT '  + convert(varchar(10),@PageSize) +' as pageSize,  (COUNT(*) OVER() / ' +  convert(varchar(10),@PageSize) +') + 1 as pageTotal, '
				    + convert(varchar(10),@PageNumber) +' as pageNumber, COUNT(*) OVER() as linesTotal		
				,* 				
				into #dadosFO
				from (
				SELECT DISTINCT	
					fo.data	
					,fo.usrhora
					,fo.fostamp
					,fo.site
				FROM
       				[dbo].[FO](nolock) 
					LEFT JOIN [dbo].[fo2](nolock)  ON [fo].[fostamp]=[fo2].[fo2stamp]
       				LEFT JOIN [dbo].[fn] (nolock)  ON [fo].[fostamp]=[fn].[fostamp]
					LEFT JOIN b_us(nolock)		   ON b_us.iniciais = fo.usrinis
					INNER JOIN @documentos		   ON [@documentos].[seriesNumber] = [fo].[docCode]
					INNER JOIN #temp_Documentos    ON [#temp_Documentos].[seriesNumber] = [fo].[docCode]
				WHERE
       				disp_webservice_view = 1 '


if @dateInit != '' and @dateEnd  != ''
	select @sql = @sql + N' AND fo.data BETWEEN ''' + @dateInit + '''  AND  ''' + @dateEnd + ''''




if @lastChangeDate != ''
	select @sql = @sql + N' AND 	convert(datetime,convert(varchar(10),fo.usrdata,120) + '' '' + fo.usrhora) >= ''' + @lastChangeDate + ' ' + @lastChangeTime + ''''

if @design != ''		
	select @sql = @sql + N' AND ([fn].[design] LIKE '''+@design+''' +''%'' )'

if @ref != ''		
	select @sql = @sql + N' AND ([fn].[ref] LIKE ''' + @ref+'''+''%'')'

if @vatNr != ''		
	select @sql = @sql + N' AND [fo].[ncont] = '''+ @vatNr +''' '



if @number != ''		
	select @sql = @sql + N' AND [fo].[no] = '+ @number +' '

if @dep != ''		
select @sql = @sql + N' AND [fo].[estab] = '+ @dep +' '



if @status != ''   
	select @sql = @sql + N' AND [fo].[u_status] = CASE WHEN '''+@status+''' = ''A'' THEN  ''A''
														WHEN '''+@status+''' = ''F'' then ''F'' 
														ELSE [fo].[u_status] END'

if @site != ''
	select @sql = @sql + N'	AND [fo].[site] = '''+@site+''' '


if @numdoc != ''		
	select @sql = @sql + N' AND convert(varchar,[fo].[adoc]) = CASE WHEN '''+convert(varchar,@numdoc)+''' = '''' THEN convert(varchar,[fo].[adoc]) 
																	ELSE '''+convert(varchar,@numdoc)+''' END'
	
	set @sqlFinal = '
			SELECT 	DISTINCT
					pageSize
					,pageTotal
					,pageNumber
					,linesTotal
					,fo.fostamp as stamp
				    , Convert(VARCHAR(100), [fo].[adoc]) AS IDInfo_id
					, fo.[Site] AS IDInfo_site
					, fo.docCode As IDInfo_seriesNumber
					, fo.foano AS  IDInfo_year
					, fo.no AS EntityInfo_number
					, fo.estab AS EntityInfo_dep
					, UPPER(fo.nome) AS EntityInfo_name				
					, ''Fornecedor'' AS EntityInfo_type
					, fo.etotal AS FinanceInfo_retailPrice
					, fo.moeda AS FinanceInfo_currency
					, 1 AS FinanceInfo_vatInc 
					, convert(varchar, fo.ousrdata, 23) AS OperationInfo_CreationDate
					, fo.ousrhora AS OperationInfo_CreationTime
					, convert(varchar, fo.usrdata, 23) AS OperationInfo_lastChangeDate
					, fo.usrinis    AS OperationInfo_lastChangeUser
					, fo.usrhora AS OperationInfo_lastChangeTime
					, rtrim(ltrim(isnull(b_us.nome,''''))) AS OperationInfo_lastChangeUserName 
					, (CASE WHEN isnull(fo.u_status,'''') = ''A'' THEN ''1'' ELSE ''0'' END) AS GeneralInfo_status	
					, convert(varchar, fo.data, 23) AS GeneralInfo_docDate
					, (SUM(isnull(dbo.unityConverter(qtt,''qttUnityToBox'', ' + convert(varchar(10),@site_nr) + ',fn.ref),0)) OVER(PARTITION BY [fn].[fostamp])) AS StockInfo_qtt
					, fo.docNome AS DesignInfo_designation
					, ISNULL((SELECT TOP 1 ISNULL('''+ @imagemURL +''' + ''/''+ anexosstamp,'''') FROM anexos (nolock) WHERE filename like ''%.pdf'' and regstamp = fo.fostamp order by usrdata desc),'''') AS GeneralInfo_linkPdf	
					, FO.data		
					, FO.usrhora
				FROM
       				[dbo].[FO](nolock)
					INNER JOIN #dadosFO			   ON #dadosFO.fostamp = [fo].[fostamp] AND #dadosFO.site = fo.site
					LEFT JOIN [dbo].[fo2](nolock)  ON [fo].[fostamp]=[fo2].[fo2stamp]
       				LEFT JOIN [dbo].[fn] (nolock)  ON [fo].[fostamp]=[fn].[fostamp]
					LEFT JOIN b_us(nolock)		   ON b_us.iniciais = fo.usrinis
					INNER JOIN @documentos		   ON [@documentos].[seriesNumber] = [fo].[docCode]
					INNER JOIN #temp_Documentos    ON [#temp_Documentos].[seriesNumber] = [fo].[docCode]
				WHERE
       				disp_webservice_view = 1 '


if @dateInit != '' and @dateEnd  != ''
	select @sql = @sql + N' AND fo.data BETWEEN ''' + @dateInit + '''  AND  ''' + @dateEnd + ''''




if @lastChangeDate != ''
	select @sql = @sql + N' AND 	convert(datetime,convert(varchar(10),fo.usrdata,120) + '' '' + fo.usrhora) >= ''' + @lastChangeDate + ' ' + @lastChangeTime + ''''

if @design != ''		
	select @sql = @sql + N' AND ([fn].[design] LIKE '''+@design+''' +''%'' )'

if @ref != ''		
	select @sql = @sql + N' AND ([fn].[ref] LIKE ''' + @ref+'''+''%'')'

if @vatNr != ''		
	select @sql = @sql + N' AND [fo].[ncont] = '''+ @vatNr +''' '



if @number != ''		
	select @sql = @sql + N' AND [fo].[no] = '+ @number +' '

if @dep != ''		
select @sql = @sql + N' AND [fo].[estab] = '+ @dep +' '



if @status != ''   
	select @sql = @sql + N' AND [fo].[u_status] = CASE WHEN '''+@status+''' = ''A'' THEN  ''A''
														WHEN '''+@status+''' = ''F'' then ''F'' 
														ELSE [fo].[u_status] END'

if @site != ''
	select @sql = @sql + N'	AND [fo].[site] = '''+@site+''' '


if @numdoc != ''		
	select @sql = @sql + N' AND convert(varchar,[fo].[adoc]) = CASE WHEN '''+convert(varchar,@numdoc)+''' = '''' THEN convert(varchar,[fo].[adoc]) 
																	ELSE '''+convert(varchar,@numdoc)+''' END
	' + @OrderCri

select @sql = @sql  + @orderBy + @sqlFinal + @OrderCri
	   
print @sql
EXECUTE (@sql)

If OBJECT_ID('tempdb.dbo.#temp_documentos') IS NOT NULL
		drop table #temp_documentos ;
IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosFO'))
	DROP TABLE #dadosFO	
GO
GRANT EXECUTE on dbo.up_Documentos_pesquisaGeralDocFornecedor TO PUBLIC
GRANT Control on dbo.up_Documentos_pesquisaGeralDocFornecedor TO PUBLIC
GO

