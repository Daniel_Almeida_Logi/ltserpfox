/*
EXEC up_Validar_Transicao_Estado_Encomenda N'4fe44189280448a0b2c0cd899',N'Pendente',N'508935490',N'homedelivery',N'Ref.Multibanco', 'Loja 1'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Validar_Transicao_Estado_Encomenda]') IS NOT NULL
    drop procedure dbo.[up_Validar_Transicao_Estado_Encomenda]
go

CREATE procedure [dbo].[up_Validar_Transicao_Estado_Encomenda]
	@stamp					VARCHAR(25),
	@finalDeliveryStatus	VARCHAR(255),
	@entidade				VARCHAR(50),
	@shippingMode			VARCHAR(255),
	@paymentMode			VARCHAR(255),
	@site					VARCHAR(255)

/* WITH ENCRYPTION */ 
AS 
BEGIN 
	If OBJECT_ID('tempdb.dbo.#tempResStatus') IS NOT NULL
		DROP TABLE #tempResStatus
	
	CREATE TABLE #tempResStatus(statChange BIT)
 
	DECLARE @initialDeliveryStatus		AS VARCHAR(255) = ''

	SELECT 
		@initialDeliveryStatus = bo2.status 
	FROM 
		bo2 (nolock)
		INNER JOIN bo(NOLOCK) ON bo.bostamp = bo2.bo2stamp
	 WHERE 
		bo2.bo2stamp = @stamp 
		and bo.ndos = 41

	IF @initialDeliveryStatus IS NOT NULL AND LEN(LTRIM(rtrim(@initialDeliveryStatus)))>0
	BEGIN

		exec up_vendas_online_checkStatusChange @shippingMode, @paymentMode,@entidade, @initialDeliveryStatus, @finalDeliveryStatus, @site
		 
	END
	ELSE
	BEGIN

		SELECT CAST(1 AS BIT) AS statChange
	END

	If OBJECT_ID('tempdb.dbo.#tempResStatus') IS NOT NULL
		DROP TABLE #tempResStatus
END

