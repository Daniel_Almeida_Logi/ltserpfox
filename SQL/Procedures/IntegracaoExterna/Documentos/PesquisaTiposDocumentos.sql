/*
Pesquisa Tipos de Docuemntos
J.Gomes, 2020-04-21
--------------------------------

Listagem de Tipos de Documentos, baseada na pesquisa por site (@site)
e com possibilidade de filtragem acima de determinada data e hora de alteração.

Pesquisa nas tabelas: 	ts, td, cm1 por 3 campos:
	@site
	@OperationInfo_lastChangeDate
	@OperationInfo_lastChangeTime
	
Campos obrigatórios: nenhum

OUTPUT:
---------
  DesignInfo_designation, IDInfo_seriesNumber, IdInfo_type, FinanceInfo_debt
, FinanceInfo_InOut, FinanceInfo_ccClient, FinanceInfo_cash, FinanceInfo_qttDiscount
, FinanceInfo_warehouseTransfer, FinanceInfo_bookCli, FinanceInfo_bookProvider
, FinanceInfo_ccProvider, OperationInfo_CreationDate, OperationInfo_CreationTime
, OperationInfo_lastChangeDate, OperationInfo_lastChangeTime, GeneralInfo_dispView
, GeneralInfo_dispEdit


EXECUÇÃO DA SP:
---------------
exec PesquisaTiposDocumentos '@site','@OperationInfo_lastChangeDate','@OperationInfo_lastChangeTime'


-- exec PesquisaTiposDocumentos '', '', ''
-- exec PesquisaTiposDocumentos '','1900-01-28','13:45:56'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[PesquisaTiposDocumentos]') IS NOT NULL
	drop procedure dbo.PesquisaTiposDocumentos
go

create procedure dbo.PesquisaTiposDocumentos
@site								varchar(18),
@lastChangeDate		datetime = '1900-01-01',
@lastChangeTime		varchar(8) = '00:00:00'


AS

SET NOCOUNT ON

SET @site			=   rtrim(ltrim(@site))
SET @lastChangeDate	=   rtrim(ltrim(@lastChangeDate))
SET @lastChangeTime	=   rtrim(ltrim(@lastChangeTime))
	

SELECT 
	  nmdos AS DesignInfo_designation, ndos AS IDInfo_seriesNumber, 'ts' AS IdInfo_type,'' AS FinanceInfo_debt
	, case when cmstocks >= 50 then 'Saida' else 'Entrada' END AS FinanceInfo_InOut, 0 AS FinanceInfo_ccClient
	, 0 AS FinanceInfo_cash, desconto AS FinanceInfo_qttDiscount, trfa AS FinanceInfo_warehouseTransfer
	, rescli AS FinanceInfo_bookCli, resfor AS FinanceInfo_bookProvider, 0 AS FinanceInfo_ccProvider
	, convert(varchar, ousrdata, 23) AS OperationInfo_CreationDate, ousrhora AS OperationInfo_CreationTime
	, convert(varchar, usrdata, 23) AS OperationInfo_LastChangeDate, usrhora AS OperationInfo_LastChangeTime
	, disp_webservice_view AS GeneralInfo_dispView, disp_webservice_edit AS GeneralInfo_dispEdit
	, usrinis as OperationInfo_LastChangeUser
from ts (nolock)
where disp_webservice_view = 1
	AND usrdata + usrhora >= @lastChangeDate + @lastChangeTime

UNION ALL

SELECT 
	nmdoc,ndoc,'td', case when cmcc >=50 then 'Credito' else 'Debito' END
	, case when cmsl >= 50 then 'Saida' else 'Entrada' END, lancacc
	, lancaol, 0, 0
	, 0, 0, 0
	, convert(varchar, ousrdata, 23), ousrhora 
	, convert(varchar, usrdata, 23), usrhora
	, disp_webservice_view, disp_webservice_edit
	, usrinis
from td (nolock)
where disp_webservice_view = 1 
	and site = COALESCE(NULLIF(@site,''), site)
	AND  convert(datetime,convert(varchar(10),usrdata,120)+' ' + usrhora) >= @lastChangeDate + @lastChangeTime

UNION ALL

SELECT
	cmdesc, cm, 'cm1','' 
	, case when fosl >= 50 then 'Saida' else 'Entrada' END,0
	, folanol, 0, 0
	, 0, 0, folanfc
	, convert(varchar, ousrdata, 23), ousrhora
	, convert(varchar, usrdata, 23), usrhora
	, disp_webservice_view, disp_webservice_edit
	, usrinis
from cm1 (nolock)
where disp_webservice_view = 1
	AND  convert(datetime,convert(varchar(10),usrdata,120)+' ' + usrhora) >= @lastChangeDate + @lastChangeTime


Go
Grant Execute on dbo.PesquisaTiposDocumentos to Public
Grant Control on dbo.PesquisaTiposDocumentos to Public
Go
