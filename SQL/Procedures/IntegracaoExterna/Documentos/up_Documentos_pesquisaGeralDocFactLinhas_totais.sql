/* SP Pesquisa dos totais dos Documentos de facturacao
D. Almeida, 2020-06-09
--------------------------------

totais dos Documentos internos baseada em diversos campos.
e com possibilidade de filtragem acima de determinada data e hora de alteração.

Pesquisa nas tabelas: 	
BO, Bo2,Bi, ts, b_utentes
Ft, Ft2,Fi, td, 

por 12 campos:
	@number, @dep, @vatNr, @email, @ref, @design, @site, @status, @numdoc, @seriesNumber, @docType
	, @lastChangeDate, @lastChangeTime
	



EXECUÇÃO DA SP:
---------------
exec up_Documentos_pesquisaGeralDocFactLinhas_totais '@number', '@dep', '@vatNr', '@email', '@ref', '@design'
, '@site', '@status', '@numdoc', '@seriesNumber', '@docType' , '@lastChangeDate', '@lastChangeTime','@dateInit','@dateEnd' 


	
	

	exec up_Documentos_pesquisaGeralDocFactLinhas_totais '215',NULL, NULL , NULL , NULL , NULL , 'Loja 1' , NULL ,
	 NULL , NULL , NULL ,NULL ,NULL, NULL 
	
	*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_pesquisaGeralDocFactLinhas_totais]') IS NOT NULL
	DROP PROCEDURE [dbo].[up_Documentos_pesquisaGeralDocFactLinhas_totais] 
GO

CREATE PROCEDURE [dbo].[up_Documentos_pesquisaGeralDocFactLinhas_totais]	
	 @number			VARCHAR(10)
	,@dep				VARCHAR(3)
	,@vatNr				VARCHAR(20)
	,@email				VARCHAR(50)
	,@ref				VARCHAR(18)
	,@design			VARCHAR(60)
	,@site				VARCHAR(60)
	,@status			VARCHAR(1)
	,@numdoc			VARCHAR(20)
	,@seriesNumber	    VARCHAR(500)
	,@lastChangeDate	varchar(10) = '1900-01-01'
	,@lastChangeTime	varchar(8)  = '00:00:00'
	,@dateInit			varchar(10) = '1900-01-01'
	,@dateEnd	        varchar(10)  = '3000-01-01'
	   
/* WITH ENCRYPTION */
AS

SET NOCOUNT ON




SET @ref				=   rtrim(ltrim(isnull(@ref,'')))
SET @number				=   rtrim(ltrim(isnull(@number,'')))
SET @dep				=   rtrim(ltrim(isnull(@dep,'')))
SET @vatNr				=   rtrim(ltrim(isnull(@vatNr,'')))
SET @design				=   rtrim(ltrim(isnull(@design,'')))
SET @status				=   rtrim(ltrim(isnull(@status,'')))
SET @numdoc				=   rtrim(ltrim(isnull(@numdoc,'')))
SET @seriesNumber		=   rtrim(ltrim(isnull(@seriesNumber,'')))
SET @site				=   rtrim(ltrim(isnull(@site,'')))
SET @email				=   rtrim(ltrim(isnull(@email,'')))
SET @lastChangeDate		=   rtrim(ltrim(isnull(@lastChangeDate,'1900-01-01')))
SET @lastChangeTime		=   rtrim(ltrim(isnull(@lastChangeTime,'00:00:00')))
SET @dateInit		    =   rtrim(ltrim(isnull(@dateInit,'1900-01-01')))
SET @dateEnd		    =   rtrim(ltrim(isnull(@dateEnd,'3000-01-01')))


If OBJECT_ID('tempdb.dbo.#temp_documentos') IS NOT NULL
		drop table #temp_documentos;	
		
IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosCc'))
	DROP TABLE #dadosTotais	

DECLARE @sql varchar(max)

DECLARE @id_lt varchar(100) =''	
DECLARE @site_nr INT
select @id_lt = id_lt, @site_nr= no from empresa where site = @site



select @sql = N' SELECT * INTO #temp_Documentos
				FROM (
					SELECT
						 [tabela] = ''BO''
						, ndos AS seriesNumber 
						, [documento] = [Ts].[nmdos]
						, bdempresas
						, disp_webservice_view 
					FROM
						[dbo].[Ts](nolock)	
							 
				) as X 
	
				DECLARE @Documentos TABLE(
					seriesNumber int		
				)

				IF '''+@seriesNumber+''' <> ''''
					BEGIN
						INSERT INTO @Documentos
						SELECT * FROM dbo.up_splitToTable('''+@seriesNumber+''','','')
					END
				ELSE
				BEGIN
					INSERT INTO @Documentos
					SELECT seriesNumber FROM #temp_Documentos
				END

			-- Result Set Final	
				SELECT 			
					 bo.bostamp
					,ISNULL(Sum(dbo.unityConverter(bi.qtt,''qttUnityToBox'', ' + convert(varchar(10),@site_nr) + ',bi.ref)) ,0) AS totqtt
					,ISNULL(bo.etotal,0) AS etotal
					,bo.dataobra
					,ISNULL((case when Bo.dataobra BETWEEN ''' + @dateInit + '''  AND  ''' + @dateEnd + ''' then 1 else 0 end),0) as Period			
				into
					#dadosTotais
				FROM
       				[dbo].[Bi](nolock) 
					INNER JOIN [dbo].[Bo] (nolock)  ON [Bo].[bostamp]=[Bi].[bostamp]
					LEFT JOIN [dbo].[Bo2](nolock)  ON [Bo].[bostamp]=[Bo2].[bo2stamp]      				
					INNER JOIN [#temp_Documentos] on [Bo].[ndos] = [#temp_Documentos].[seriesNumber] 
					INNER JOIN @documentos ON [@documentos].[seriesNumber] = [Bo].[ndos]
					LEFT JOIN [dbo].[st] (nolock)		 ON [st].[ref]=[Bi].[ref] and st.site_nr=''' + Convert(varchar(10),@site_nr) +'''
					LEFT JOIN [dbo].[fprod] (nolock)	 ON [fprod].[cnp]=[Bi].[ref] 
					LEFT JOIN grande_mercado_hmr(nolock) ON fprod.id_grande_mercado_hmr = grande_mercado_hmr.id
				WHERE
       				disp_webservice_view = 1 '

if @lastChangeDate != ''
	select @sql = @sql + N' AND Bo.usrdata + Bo.usrhora >= ''' + @lastChangeDate + ' ' + @lastChangeTime + ''''

if @design != ''		
	select @sql = @sql + N' AND ([Bi].[design] LIKE '''+@design+''' +''%'' )'

if @ref != ''		
	select @sql = @sql + N' AND ([Bi].[ref] LIKE ''' + @ref+'''+''%'')'

if @vatNr != ''		
	select @sql = @sql + N' AND [Bo].[ncont] = '''+ @vatNr +''' '


IF len(@email)>0 
BEGIN		
	declare @no_	int
	declare @estab_ int

	IF len(@number)>0 and len(@dep)>0
		select @no_ = no, @estab_ = estab from b_utentes where email = @email and no=@number and estab=@dep
	ELSE
		select top 1 @no_ = no, @estab_ = estab from b_utentes where email = @email			
		
	select @sql = @sql + ' AND [Bo].[no]= ' + isnull(convert(varchar,@no_),0) + ' AND [Bo].[estab] = ' + isnull(convert(varchar,@estab_),0)	
END

if @number != ''		
	select @sql = @sql + N' AND [Bo].[no] = '+ @number +' '

if @dep != ''		
select @sql = @sql + N' AND [Bo].[estab] = '+ @dep +' '



if @status != '-1'   
	select @sql = @sql + N' AND [Bo].[fechada] = CASE WHEN '''+@status+''' = ''1'' THEN 0 
														WHEN '''+@status+''' = ''0'' then 1 
														ELSE [Bo].[fechada] END'

if @site != ''
	select @sql = @sql + N'	AND [Bo].[site] = '''+@site+''' '


if @numdoc != ''		
	select @sql = @sql + N' AND convert(varchar,[Bo].[obrano]) = CASE WHEN '''+convert(varchar,@numdoc)+''' = '''' THEN convert(varchar,[Bo].[obrano]) 
																	ELSE '''+convert(varchar,@numdoc)+''' END'

select @sql = @sql + N'	GROUP BY bo.bostamp, 
								bo.etotal,
								bo.dataobra			
								 '																					  

select @sql = @sql + ' SELECT 						
							Total_ndoc			= ISNULL(COUNT (DISTINCT bostamp),0), 
							Total_qtt			= CONVERT(INT,ISNULL(SUM(totqtt),0)),
							Total_value			= ISNULL(Round(SUM(etotal),2),0.0),
							TotalPeriod_ndoc	= SUM(Period) ,
							TotalPeriod_qtt		= CONVERT(INT,ISNULL(SUM(case when dataobra BETWEEN ''' + @dateInit + '''  AND  ''' + @dateEnd + ''' then totqtt else 0 end),0)),
							TotalPeriod_value	= ISNULL(Round(SUM(case when dataobra BETWEEN ''' + @dateInit + '''  AND  ''' + @dateEnd + ''' then etotal else 0 end),2),0.0)
					  FROM #dadosTotais
					  '
	   
--print @sql
EXECUTE (@sql)

If OBJECT_ID('tempdb.dbo.#temp_documentos') IS NOT NULL
		drop table #temp_documentos ;

IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosCc'))
	DROP TABLE #dadosTotais

GO
GRANT EXECUTE on dbo.up_Documentos_pesquisaGeralDocFactLinhas_totais TO PUBLIC
GRANT Control on dbo.up_Documentos_pesquisaGeralDocFactLinhas_totais TO PUBLIC
GO