/* SP Pesquisa Verifica Documento, seja foi criado no passado
Daniel Almeida, 2020-08-14
--------------------------------

Pesquisa nas tabelas: 	
no

OUTPUT: 
---------
DateMessag				StatMessag  ActionMessage	DescMessag		   stamp		
2020-04-28 11:08:52.610	0			0                MULTIPLE DOCS	   admxpto	

DateMessag				StatMessag	ActionMessage    DescMessag		   stamp			
2020-04-28 11:07:38.063	1			1                 not Found         admxpto		

DateMessag				StatMessag	ActionMessage    DescMessag		   stamp			
2020-04-28 11:07:38.063	1			2                DOC Found         admxpto		    


EXECUÇÃO DA SP:
---------------
exec up_Documentos_verifica_documento_valida_existe  41, 'Loja 1', 2020, NULL

exec up_Documentos_verifica_documento_valida_existe  41, 'Loja 23', 2021, 'Enc443'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_verifica_documento_valida_existe]') IS NOT NULL
	DROP PROCEDURE [dbo].up_Documentos_verifica_documento_valida_existe;
GO

CREATE PROCEDURE [dbo].up_Documentos_verifica_documento_valida_existe	


	 @IdInfo_seriesNumber		int,		 /*seriesNumber*/
	 @IdInfo_site				varchar(20), /*site*/
	 @IdInfo_year			    int,         /*boano*/	
	 @IdInfo_noExt		        varchar(10)    /*noExt*/	
	
	   
/* WITH ENCRYPTION */
AS



	DECLARE @contador			int = 0
	DECLARE @obrano				numeric(10,0) = -1

	

	select 
		@contador = count(obrano), 
		@obrano = isnull(obrano,-1)
	from 
		bo(nolock) 
	inner join bo2(nolock) on bo2.bo2stamp = bo.bostamp		
	where
		ndos = isnull(@IdInfo_seriesNumber,-1) and
	--	site = ltrim(rtrim(isnull(@IdInfo_site,''))) and 
		boano = isnull(@IdInfo_year,1900) and 
		codext = ltrim(rtrim(isnull(@IdInfo_noExt,'XXXXXXXXXX')))
	group by obrano	
	



	if(@contador=0)
	 begin
		select getdate() AS DateMessag, 0 As StatMessag, 0 AS ActionMessage, 'NOT EXISTS' As DescMessag, @obrano AS Document_number, 
				@IdInfo_seriesNumber As Document_seriesNumber, @IDInfo_site as Document_site, @IDInfo_year as Document_year;
	end else begin
		select getdate() AS DateMessag, 1 As StatMessag, 1 AS ActionMessage, 'EXISTS' As DescMessag, @obrano AS Document_number, 
				@IdInfo_seriesNumber As Document_seriesNumber, @IDInfo_site as Document_site, @IDInfo_year as Document_year;	
	end

	
GO


GRANT EXECUTE on dbo.up_Documentos_verifica_documento_valida_existe TO PUBLIC
GRANT Control on dbo.up_Documentos_verifica_documento_valida_existe TO PUBLIC
GO