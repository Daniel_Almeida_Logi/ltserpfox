/* SP Pesquisa Detalhe Documentos (Rates)
J.Gomes, 2020-04-24
--------------------------------

Listagem agragada de valores de IVA,  baseada no campo bostamp proveniente da 
sp up_Documentos_pesquisaDetalheDocInternos.

Pesquisa nas tabela: 	
bo_totais

por 1 campo:
	'@stamp'
	
Campos obrigatórios: todos

OUTPUT:
---------
vatRatesInfo_taxa,	vatRatesInfo_incidenceBase,	vatRatesInfo_value


EXECUÇÃO DA SP:
---------------
exec up_Documentos_pesquisaDetalheDocInternosRates '@stamp'

	exec up_Documentos_pesquisaDetalheDocInternosRates  'ADME011D779-2EE3-4EF7-914'

	*/
	
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


IF OBJECT_ID('[dbo].[up_Documentos_pesquisaDetalheDocInternosRates]') IS NOT NULL
		DROP PROCEDURE [dbo].[up_Documentos_pesquisaDetalheDocInternosRates] ;
GO


CREATE PROCEDURE [dbo].[up_Documentos_pesquisaDetalheDocInternosRates]	
		 @stamp				VARCHAR(25)
	/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

SET @stamp	=   rtrim(ltrim(isnull(@stamp,'')))

IF len(@stamp) > 0 
BEGIN	
	DECLARE @sql varchar(1000)

	select @sql = N' 			
					SELECT 
						  taxa AS vatRatesInfo_rate
						, CAST(baseinc  AS numeric(12,2)) AS vatRatesInfo_incidenceBase
						, CAST(valoriva  AS numeric(12,2)) AS vatRatesInfo_value			
					FROM
       					[dbo].bo_totais(nolock)					
					WHERE
       					stamp = ''' + @stamp + ''' '

	--print @sql
	EXECUTE (@sql)
END
ELSE
	BEGIN
		print('Missing stamp')
	END
GO
GRANT EXECUTE on dbo.up_Documentos_pesquisaDetalheDocInternosRates TO PUBLIC
GRANT Control on dbo.up_Documentos_pesquisaDetalheDocInternosRates TO PUBLIC
GO