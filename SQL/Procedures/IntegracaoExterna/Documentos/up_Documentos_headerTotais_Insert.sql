/*
DOCUMENTOS UPDATE/INSERT
D.Almeida, 05-05-2020
revisão 05-05-2020
--------------------------------
bo
Pesquisa do registo na tabela BO_totais a partir de 1 campos de pesquisa:
	 stamp (@stamp), taxa(@rate), baseinc(@baseValue), valoriva(@vatValue)

	

exec up_Documentos_headerTotais_Insert 
 'ADMADB87AD4-C4AA-4182-94B', 23,1,0.23




*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_Documentos_headerTotais_Insert]') IS NOT NULL
    drop procedure up_Documentos_headerTotais_Insert
go



CREATE PROCEDURE up_Documentos_headerTotais_Insert	
	 

	 --identificadores do documento
	 @stamp					   varchar(25),
	 @rate                     numeric(19,6),
	 @baseValue                numeric(19,6),
	 @vatValue                 numeric(19,6)
	
AS

		INSERT INTO bo_totais(
				stamp
				,taxa
				,baseinc
				,valoriva
			)values(
				ltrim(ltrim(@stamp))
				,@rate
				,@baseValue
				,@vatValue
				)

	select getdate() AS DateMessag, 1 As StatMessag, 'Inserido' As DescMessag
         
GO
Grant Execute On up_Documentos_headerTotais_Insert to Public
Grant Control On up_Documentos_headerTotais_Insert to Public
GO
			
           
			
