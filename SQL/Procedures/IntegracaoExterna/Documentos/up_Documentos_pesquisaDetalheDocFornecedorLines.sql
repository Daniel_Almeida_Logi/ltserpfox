/* SP Pesquisa Detalhe Documentos de Fornecedor (LINES)
D.Almeida, 2021-11-10
--------------------------------

Listagem detalhe de Documentos internos baseada no campo fostamp proveniente da 
sp up_Documentos_pesquisaGeralDocFornecedor.

Pesquisa nas tabelas: 	fn

por 1 campo:
	'@stamp'
	
Campos obrigatórios: todos



	exec up_Documentos_pesquisaDetalheDocFornecedorLines  'FRr29CFBDE2-58B1-4306-AAC'

	*/
	
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_Documentos_pesquisaDetalheDocFornecedorLines]') IS NOT NULL
		DROP PROCEDURE [dbo].up_Documentos_pesquisaDetalheDocFornecedorLines ;
GO




CREATE PROCEDURE [dbo].[up_Documentos_pesquisaDetalheDocFornecedorLines]	
		 @stamp				VARCHAR(25)
		   
	/* WITH ENCRYPTION */
AS
SET NOCOUNT ON

SET @stamp	=   rtrim(ltrim(isnull(@stamp,'')))
Declare @site_nr int = 0
	
IF len(@stamp) > 0 
BEGIN	
	DECLARE @sql varchar(4000)


	select top 1 @site_nr= isnull(empresa.no,0) from fo(nolock)
	inner join empresa(nolock) on ltrim(rtrim(fo.site))= ltrim(rtrim(empresa.site))
	where fo.fostamp=@stamp


	select @sql = N' 				
					SELECT 	
						 fn.ref AS ProductInfo_ref
						, fn.design AS ProductInfo_design
						, dbo.unityConverter(qtt,''qttUnityToBox'', ' + convert(varchar(10),@site_nr) + ',fn.ref)  AS StockInfo_qtt
						, fn.armazem AS StockInfo_warehouseSource
						, dbo.unityConverter(u_stockact,''stocksUnityToBox'', ' + convert(varchar(10),@site_nr) + ',fn.ref)  AS StockInfo_site	
						, dbo.unityConverter(u_bonus,''stocksUnityToBox'', ' + convert(varchar(10),@site_nr) + ',fn.ref)  AS StockInfo_qttBonus
						, cast( fn.etiliquido  as numeric(15,2)) AS FinanceInfo_retailPrice
						, cast( fn.epv  as numeric(15,2)) AS FinanceInfo_unitRetailPrice									
						, fn.u_upc * qtt AS FinanceInfo_pcl 
						, fn.epv * qtt AS FinanceInfo_pct					
						, convert(varchar,desconto) + '','' + convert(varchar,desc2) + 
							'','' + convert(varchar,desc3) + '','' + convert(varchar,desc4) AS FinanceInfo_discountsCommercPerc
						, fn.iva as VatRatesInfo_rate
						, fn.ivaincl as FinanceInfo_vatInc
						, '''' AS GeneralInfo_obs                                                
						, VatRatesInfo_value = case when fn.ivaincl = 1 then cast(fn.etiliquido - (fn.etiliquido/(fn.iva/100+1)) as  numeric(15,2))
									else  cast(fn.etiliquido*(fn.iva/100) as  numeric(15,2)) end

                        , VatRatesInfo_incidenceBase  = case when fn.ivaincl = 1 then cast((fn.etiliquido/(fn.iva/100+1)) as  numeric(15,2))
									else cast(fn.etiliquido as  numeric(15,2)) end	
						,fnStamp
						,ofnStamp
						,bistamp
						,ltrim(rtrim(ISNULL(grande_mercado_hmr.descr,''''))) AS ProductInfo_HmrInfo_dicBigMarketHmrDescr
						,ISNULL(grande_mercado_hmr.id,0) AS ProductInfo_HmrInfo_dicBigMarketHmrId
						,ISNULL(categoria_hmr.id,0) AS ProductInfo_HmrInfo_dicCategoryHmrId
						,ltrim(rtrim(ISNULL(categoria_hmr.descr,''''))) AS ProductInfo_HmrInfo_dicCategoryHmrDescr
						,ISNULL(mercado_hmr.id,0) AS ProductInfo_HmrInfo_dicMarketHmrId
						,ltrim(rtrim(ISNULL(mercado_hmr.descr,''''))) AS ProductInfo_HmrInfo_dicMarketHmrDescr
						,ISNULL(segmento_hmr.id,0) AS ProductInfo_HmrInfo_dicSegmentHmrId
						,ltrim(rtrim(ISNULL(segmento_hmr.descr,''''))) AS ProductInfo_HmrInfo_dicSegmentHmrDescr
						,ltrim(rtrim(st.usr1)) AS ProductInfo_brand
						,rtrim(ltrim(isnull(st.designOnline,''''))) AS ProductInfo_OnlineDesignation
						,rtrim(ltrim(isnull(st.caractOnline,''''))) AS ProductInfo_OnlineDescription1
						,rtrim(ltrim(isnull(st.apresentOnline,''''))) AS ProductInfo_OnlineDescription2
						,ISNULL(fprod.generico,0) AS ProductInfo_generic
						,st.tempoIndiponibilidade	AS ProductInfo_TimeUnavailability 
						,''dia''	AS ProductInfo_UnavailabilityUnit

														
					FROM
       					[dbo].[fn](nolock)	
						LEFT JOIN [dbo].[st] (nolock)		 ON [st].[ref]=[fn].[ref]  and   st.site_nr=''' + Convert(varchar(10),isnull(@site_nr,0)) +'''
						LEFT JOIN [dbo].[fprod] (nolock)	 ON [fprod].[cnp]=[fn].[ref] 
						LEFT JOIN grande_mercado_hmr(nolock) ON st.u_depstamp = grande_mercado_hmr.id
						LEFT JOIN mercado_hmr(nolock)        ON st.u_secstamp = mercado_hmr.id
						LEFT JOIN categoria_hmr(nolock)      ON st.u_catstamp = categoria_hmr.id
						LEFT JOIN segmento_hmr(nolock)       ON st.u_segstamp = segmento_hmr.id				
					WHERE
       					fn.fostamp = ''' + @stamp + ''' '   
	--print @sql
	EXECUTE (@sql)
END
ELSE
	BEGIN
		print('Missing stamp')
	END

GO
GRANT EXECUTE on dbo.up_Documentos_pesquisaDetalheDocFornecedorLines TO PUBLIC
GRANT Control on dbo.up_Documentos_pesquisaDetalheDocFornecedorLines TO PUBLIC
GO

