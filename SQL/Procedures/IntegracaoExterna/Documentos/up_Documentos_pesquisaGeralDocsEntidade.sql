/* SP Pesquisa Documentos e Documentos de facturacao
D. Almeida, 2020-06-09
-------------------------------
Listagem de Documentos internos baseada em diversos campos.
e com possibilidade de filtragem acima de determinada data e hora de alteração.

Pesquisa nas tabelas: 	
BO, Bo2,Bi, ts, b_utentes
Ft, Ft2,Fi, td, 

por 12 campos:
	@number, @dep, @vatNr, @email, @ref, @design, @site, @status, @numdoc, @seriesNumber, @docType
	, @lastChangeDate, @lastChangeTime
	



EXECUÇÃO DA SP:
---------------
exec up_Documentos_pesquisaGeralDocsEntidade '@number', '@dep', '@vatNr', '@email', '@ref', '@design'
, '@site', '@status', '@numdoc', '@seriesNumber', '@docType' , '@lastChangeDate', '@lastChangeTime','@dateInit','@dateEnd', '@pageNumber', '@orderStamp'
,'@deliveryStatusList', '@PageSize' 

	exec up_Documentos_pesquisaGeralDocsEntidade 198287904,0, NULL , NULL , NULL , NULL , 'Loja 1' , NULL ,
	 NULL , '41' , NULL ,NULL ,NULL, NULL , NULL , '', '', '1000'



	
	*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_pesquisaGeralDocsEntidade]') IS NOT NULL
	DROP PROCEDURE [dbo].up_Documentos_pesquisaGeralDocsEntidade 
GO

CREATE PROCEDURE [dbo].up_Documentos_pesquisaGeralDocsEntidade	
	 @number			VARCHAR(10)
	,@dep				VARCHAR(3)
	,@vatNr				VARCHAR(20)
	,@email				VARCHAR(50)
	,@ref				VARCHAR(18)
	,@design			VARCHAR(60)
	,@site				VARCHAR(60)
	,@status			VARCHAR(1)
	,@numdoc			VARCHAR(20)
	,@seriesNumber	    VARCHAR(500)
	,@lastChangeDate	varchar(10) = '1900-01-01'
	,@lastChangeTime	varchar(8)  = '00:00:00'
	,@dateInit			varchar(10) = '1900-01-01'
	,@dateEnd	        varchar(10)  = '3000-01-01'
	,@pageNumber        int = 1
	,@orderStamp		VARCHAR(36) = ""
	,@deliveryStatusList    VARCHAR(1000) =''
	,@PageSize int = 1000
	   
/* WITH ENCRYPTION */
AS

SET NOCOUNT ON




SET @ref				=   rtrim(ltrim(isnull(@ref,'')))
SET @number				=   rtrim(ltrim(isnull(@number,'')))
SET @dep				=   rtrim(ltrim(isnull(@dep,'')))
SET @vatNr				=   rtrim(ltrim(isnull(@vatNr,'')))
SET @design				=   rtrim(ltrim(isnull(@design,'')))
SET @status				=   rtrim(ltrim(isnull(@status,'')))
SET @numdoc				=   rtrim(ltrim(isnull(@numdoc,'')))
SET @seriesNumber		=   rtrim(ltrim(isnull(@seriesNumber,'')))
SET @site				=   rtrim(ltrim(isnull(@site,'')))
SET @email				=   rtrim(ltrim(isnull(@email,'')))
SET @lastChangeDate		=   rtrim(ltrim(isnull(@lastChangeDate,'1900-01-01')))
SET @lastChangeTime		=   rtrim(ltrim(isnull(@lastChangeTime,'00:00:00')))
SET @dateInit		    =   rtrim(ltrim(isnull(@dateInit,'1900-01-01')))
SET @dateEnd		    =   rtrim(ltrim(isnull(@dateEnd,'3000-01-01')))
set @orderStamp         =	rtrim(ltrim(ISNULL(@orderStamp,'')))
SET @deliveryStatusList =	rtrim(ltrim(ISNULL(@deliveryStatusList,'')))

if(isnull(@pageNumber,0)<1)
	set @pageNumber = 1

If OBJECT_ID('tempdb.dbo.#temp_documentos') IS NOT NULL
		drop table #temp_documentos;		

If OBJECT_ID('tempdb.dbo.#temp_distribuidor') IS NOT NULL
		drop table #temp_distribuidor;	

DECLARE @sql varchar (MAX) = ''
DECLARE @sql1 varchar (MAX) = ''
DECLARE @sqlFrom varchar(MAX) = ''
DECLARE @sqlWhere varchar(MAX) = ''

DECLARE @orderBy VARCHAR(MAX) = ''
DECLARE @OrderCri VARCHAR(MAX)

DECLARE @downloadURL VARCHAR(100)
SELECT  @downloadURL = rtrim(ltrim(textValue)) FROM B_Parameters WHERE stamp='ADM0000000313'

DECLARE @id_lt varchar(100) =''	
DECLARE @site_nr INT

select @id_lt = rtrim(ltrim(id_lt)), @site_nr= no from empresa where site = @site


Select 
	distinct iddistribuidor
			,distribuidor 
Into 
	#temp_distribuidor 
from 
	CodigoPostal_Distribuidor (nolock)

DECLARE @imagemURL varchar(300) 
set   @imagemURL = @downloadURL + @id_lt

IF @orderStamp = ''
BEGIN 
	SET @OrderCri = '
	 ORDER BY dataobra DESC, usrhora DESC'
END 
ELSE 
BEGIN 
	SELECT @OrderCri='
	 ORDER BY ' + instructionSql  FROM sortingTableOrder (NOLOCK) WHERE stamp = @orderStamp and inativo=0 and nameService='internalDocLines'
END

set @orderBy = ' ) X ' + @OrderCri +'  
				 OFFSET ' + convert(varchar(10),@PageSize) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
				 FETCH NEXT  ' + convert(varchar(10),@PageSize) + ' ROWS ONLY '

select @sql = N' SELECT * INTO #temp_Documentos
				FROM (
					SELECT
						 [tabela] = ''BO''
						, ndos AS seriesNumber 
						, [documento] = [Ts].[nmdos]
						, bdempresas
						, disp_webservice_view 
					FROM
						[dbo].[Ts](nolock)	
							 
				) as X 
	
				DECLARE @Documentos TABLE(
					seriesNumber int		
				)

				IF '''+@seriesNumber+''' <> ''''
					BEGIN
						INSERT INTO @Documentos
						SELECT * FROM dbo.up_splitToTable('''+@seriesNumber+''','','')
					END
				ELSE
				BEGIN
					INSERT INTO @Documentos
					SELECT seriesNumber FROM #temp_Documentos
				END

			SELECT '  + convert(varchar(10),@PageSize) +' as pageSize,  (COUNT(*) OVER() / ' +  convert(varchar(10),@PageSize) +') + 1 as pageTotal, '
				    + convert(varchar(10),@PageNumber) +' as pageNumber, COUNT(*) OVER() as linesTotal		
				,* from (
				SELECT 	
					bo.bostamp as stamp
				    , Convert(VARCHAR(100), [Bo].[obrano]) AS Header_IDInfo_number
					, [Site] AS Header_IDInfo_site
					, Bo.ndos As Header_IDInfo_seriesNumber
					, Bo.boano AS  Header_IDInfo_year
					, Bo.no AS Header_EntityInfo_number
					, Bo.estab AS Header_EntityInfo_dep
					, UPPER(Bo.nome) AS Header_EntityInfo_name				
					, (CASE WHEN #temp_Documentos.bdempresas = ''CL'' THEN ''Cliente'' 
							WHEN #temp_Documentos.bdempresas = ''FL'' THEN ''Fornecedor''
							WHEN #temp_Documentos.bdempresas = ''AG'' THEN ''Armazem''  END) AS Header_EntityInfo_type
					, cast( Bi.ettdeb  as numeric(15,2)) AS Header_FinanceInfo_retailPrice
					, cast( Bi.edebito as numeric(15,2)) AS Header_FinanceInfo_unitRetailPrice	
					, Bo.moeda AS Header_FinanceInfo_currency
					, 1 AS Header_FinanceInfo_vatInc 
					, convert(varchar, Bo.ousrdata, 23) AS Header_OperationInfo_CreationDate
					, Bo.ousrhora AS Header_OperationInfo_CreationTime
					, convert(varchar, Bo.usrdata, 23) AS Header_OperationInfo_lastChangeDate
					, Bo.usrinis    AS Header_OperationInfo_lastChangeUser
					, Bo.usrhora AS Header_OperationInfo_lastChangeTime
					, (CASE WHEN bo.fechada = 0 THEN ''1'' ELSE ''0'' END) AS Header_GeneralInfo_status
					, Bo2.status AS Header_GeneralInfo_deliveryStatus
					, Bo2.pagamento AS Header_FinanceInfo_paymentType	
					,rtrim(ltrim(isnull(bo2.momento_pagamento,''''))) as Header_FinanceInfo_paymentMoment			
					, convert(varchar, Bo.dataobra, 23) AS Header_GeneralInfo_docDate
					, Bo.nmdos AS Header_DesignInfo_designation
					, Bo2.modo_envio AS ShippingInfo_mode
					, Bo2.localidade_ent AS ShippingInfo_city
					, Bo2.morada_ent AS ShippingInfo_address
					, Bo2.codpost_ent AS ShippingInfo_zipCode
					, Bo2.pais_ent AS ShippingInfo_countryCode
					, isnull((select sum(cast(round(Bi.ettdeb,2)  as numeric(15,2))) from   bi(nolock) where bostamp = BO.bostamp and ref=''SERVPORTES''),0.00)  AS ShippingInfo_shippingRetailPriceWithVat
					, isnull((select sum(cast(round(ettdeb/(((bi.iva/100) + 1)),2) as numeric(15,2))) from   bi(nolock) where bostamp = BO.bostamp and ref=''SERVPORTES''),0.00)  AS ShippingInfo_shippingRetailPriceWithoutVat
					, CONVERT(varchar,  Bo.u_dataentr , 23) AS ShippingInfo_deliveryDate				
					, CONVERT(VARCHAR(8),Bo.u_dataentr,108)  AS ShippingInfo_deliveryTime
					, Bo.logi1	AS ShippingInfo_sent
					, bo.obs AS  GeneralInfo_obs 
					, case when rtrim(ltrim(isnull(bo2.iddistribuidor,''''))) = '''' then ''0'' else  bo2.iddistribuidor end as ShippingInfo_operatorId
					, isnull((select top 1 isnull(distribuidor,'''') from  #temp_distribuidor(nolock) where iddistribuidor=bo2.iddistribuidor),'''') as ShippingInfo_operatorDesign 



					, Bi.ref AS Line_ProductInfo_ref
					
					
					
					, Bi.design AS Line_ProductInfo_design
					,Bo.dataobra
					,Bo.usrhora
					,dbo.unityConverter(st.stock,''stocksUnityToBox'', ' + convert(varchar(10),@site_nr) + ',bi.ref) AS Line_GeneralInfo_Stockqtt		
					
				   	,dbo.unityConverter(bi.qtt,''qttUnityToBox'', ' + convert(varchar(10),@site_nr) + ',bi.ref) AS Line_StockInfo_qtt				
				 '
				
				set @sql1='
					,ltrim(rtrim(ISNULL(grande_mercado_hmr.descr,''''))) AS Line_ProductInfo_HmrInfo_dicBigMarketHmrDescr
					,ISNULL(grande_mercado_hmr.id,0) AS Line_ProductInfo_HmrInfo_dicBigMarketHmrId
					,ISNULL(categoria_hmr.id,0) AS Line_ProductInfo_HmrInfo_dicCategoryHmrId
					,ltrim(rtrim(ISNULL(categoria_hmr.descr,''''))) AS Line_ProductInfo_HmrInfo_dicCategoryHmrDescr
					,ISNULL(mercado_hmr.id,0) AS Line_ProductInfo_HmrInfo_dicMarketHmrId
					,ltrim(rtrim(ISNULL(mercado_hmr.descr,''''))) AS Line_ProductInfo_HmrInfo_dicMarketHmrDescr
					,ISNULL(segmento_hmr.id,0) AS Line_ProductInfo_HmrInfo_dicSegmentHmrId
					,ltrim(rtrim(ISNULL(segmento_hmr.descr,''''))) AS Line_ProductInfo_HmrInfo_dicSegmentHmrDescr
					,ltrim(rtrim(st.usr1)) AS Line_ProductInfo_brand
					,rtrim(ltrim(isnull(st.designOnline,''''))) AS Line_ProductInfo_OnlineDesignation
					,rtrim(ltrim(isnull(st.caractOnline,''''))) AS Line_ProductInfo_OnlineDescription1
					,rtrim(ltrim(isnull(st.apresentOnline,''''))) AS Line_ProductInfo_OnlineDescription2
					,ISNULL(fprod.generico,0) AS Line_ProductInfo_generic
					,st.tempoIndiponibilidade	AS Line_ProductInfo_TimeUnavailability 
					,''dia''	AS Line_ProductInfo_UnavailabilityUnit					 
					,ISNULL((SELECT TOP 1 ISNULL('''+ @imagemURL +''' + ''/''+ anexosstamp,'''') FROM anexos (nolock) WHERE regstamp = st.ststamp order by usrdata desc),'''') AS Line_ProductInfo_image
					,round(isnull((select top 1  round(isnull(u_txcomp,0),2) from fi(nolock) where fi.bistamp=bi.bistamp),0.00),2) AS Line_FinanceInfo_coPaymentRate
					,round(isnull((select top 1 round(isnull(u_ettent1 + u_ettent2,0),2) from fi(nolock) where fi.bistamp=bi.bistamp),0.00),2)  AS Line_FinanceInfo_coPaymentValue 
					, bi.u_upc * bi.qtt AS Line_FinanceInfo_pcl 
					, bi.epcusto * bi.qtt AS Line_FinanceInfo_pct
					, cast( Bi.ettdeb  as numeric(15,2)) AS Line_FinanceInfo_retailPrice
					, cast( Bi.edebito  as numeric(15,2)) AS Line_FinanceInfo_unitRetailPrice
					,Line_VatRatesInfo_incidenceBase  = case when BI.ivaincl = 1 then cast((BI.ettdeb/(BI.iva/100+1)) as  numeric(15,2))
									else cast(BI.ettdeb as  numeric(15,2)) end	
					, bi.iva as Line_VatRatesInfo_rate	
				'


set @sqlFrom =' 
				FROM
       				[dbo].[Bi](nolock) 
					INNER JOIN [dbo].[Bo] (nolock)  ON [Bo].[bostamp]=[Bi].[bostamp]
					LEFT JOIN [dbo].[Bo2](nolock)  ON [Bo].[bostamp]=[Bo2].[bo2stamp]      				
					INNER JOIN [#temp_Documentos] on [Bo].[ndos] = [#temp_Documentos].[seriesNumber] 
					INNER JOIN @documentos ON [@documentos].[seriesNumber] = [Bo].[ndos]
					LEFT JOIN [dbo].[st] (nolock)		 ON [st].[ref]=[Bi].[ref] and st.site_nr=''' + Convert(varchar(10),isnull(@site_nr,0)) +'''
					LEFT JOIN [dbo].[fprod] (nolock)	 ON [fprod].[cnp]=[Bi].[ref] 
					LEFT JOIN grande_mercado_hmr(nolock) ON st.u_depstamp = grande_mercado_hmr.id
					LEFT JOIN mercado_hmr(nolock)        ON st.u_secstamp = mercado_hmr.id
					LEFT JOIN categoria_hmr(nolock)      ON st.u_catstamp = categoria_hmr.id
					LEFT JOIN segmento_hmr(nolock)       ON st.u_segstamp = segmento_hmr.id
					
				WHERE
       				disp_webservice_view = 1 '


				

if @dateInit != '' and @dateEnd  != ''
	select @sqlWhere = @sqlWhere + N' AND Bo.dataobra BETWEEN ''' + @dateInit + '''  AND  ''' + @dateEnd + ''''


if @lastChangeDate != ''
	select @sqlWhere = @sqlWhere + N' AND Bo.usrdata + Bo.usrhora >= ''' + @lastChangeDate + ' ' + @lastChangeTime + ''''

if @design != ''		
	select @sqlWhere = @sqlWhere + N' AND ([Bi].[design] LIKE '''+@design+''' +''%'' )'

if @ref != ''		
	select @sqlWhere = @sqlWhere + N' AND ([Bi].[ref] LIKE ''' + @ref+'''+''%'')'

if @vatNr != ''		
	select @sqlWhere = @sqlWhere + N' AND [Bo].[ncont] = '''+ @vatNr +''' '


if len(rtrim(ltrim(@deliveryStatusList))) > 2
	select @sql = @sql + N' AND [Bo2].[status] in ( ' + @deliveryStatusList + ' ) '


IF len(@email)>0 
BEGIN		
	declare @no_	int
	declare @estab_ int

	IF len(@number)>0 and len(@dep)>0
		select @no_ = no, @estab_ = estab from b_utentes where email = @email and no=@number and estab=@dep
	ELSE
		select top 1 @no_ = no, @estab_ = estab from b_utentes where email = @email			
		
	select @sqlWhere = @sqlWhere + ' AND [Bo].[no]= ' + isnull(convert(varchar,@no_),0) + ' AND [Bo].[estab] = ' + isnull(convert(varchar,@estab_),0)	
END

if @number != ''		
	select @sqlWhere = @sqlWhere + N' AND [Bo].[no] = '+ @number +' '

if @dep != ''		
select @sqlWhere = @sqlWhere + N' AND [Bo].[estab] = '+ @dep +' '



if @status != '-1'   
	select @sqlWhere = @sqlWhere + N' AND [Bo].[fechada] = CASE WHEN '''+@status+''' = ''1'' THEN 0 
														WHEN '''+@status+''' = ''0'' then 1 
														ELSE [Bo].[fechada] END'

if @site != ''
	select @sqlWhere = @sqlWhere + N'	AND [Bo].[site] = '''+@site+''' '


if @numdoc != ''		
	select @sqlWhere = @sqlWhere + N' AND convert(varchar,[Bo].[obrano]) = CASE WHEN '''+convert(varchar,@numdoc)+''' = '''' THEN convert(varchar,[Bo].[obrano]) 
																	ELSE '''+convert(varchar,@numdoc)+''' END'
																	

	   
print @sql + @sql1 + @sqlFrom + @sqlWhere + @orderBy
EXECUTE (@sql + @sql1 + @sqlFrom + @sqlWhere + @orderBy)

If OBJECT_ID('tempdb.dbo.#temp_documentos') IS NOT NULL
		drop table #temp_documentos ;

If OBJECT_ID('tempdb.dbo.#temp_distribuidor') IS NOT NULL
		drop table #temp_distribuidor;	

GO
GRANT EXECUTE on dbo.up_Documentos_pesquisaGeralDocsEntidade TO PUBLIC
GRANT Control on dbo.up_Documentos_pesquisaGeralDocsEntidade TO PUBLIC
GO
