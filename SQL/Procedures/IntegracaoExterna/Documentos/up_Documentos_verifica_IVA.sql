/* SP Pesquisa Verifica Taxas IVA
Jorge Gomes, 2020-04-28
--------------------------------

Pesquisa nas tabelas: 	
taxasiva

OUTPUT: 
---------
DateMessag				StatMessag	DescMessag		taxa
2020-04-28 10:16:16.563	1			Taxa existe		21.00

DateMessag				StatMessag	DescMessag			taxa
2020-04-28 10:44:27.307	0			Taxa não existe		221.00


EXECUÇÃO DA SP:
---------------
exec up_Documentos_verifica_IVA 6, '5440987', 'Loja 1'

*/

           
    




SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_verifica_IVA]') IS NOT NULL
	DROP PROCEDURE [dbo].up_Documentos_verifica_IVA;
GO

CREATE PROCEDURE [dbo].[up_Documentos_verifica_IVA]	
	 @taxa			numeric(5,2),
	 @ref			varchar(20),
	 @site			varchar(18)
	

/* WITH ENCRYPTION */
AS

SET @taxa	=   rtrim(ltrim(isnull(@taxa,-1)))
SET @site	=   rtrim(ltrim(isnull(@site,'')))
SET @ref	=   rtrim(ltrim(isnull(@ref,'')))

DECLARE @contador		int = 0
DECLARE @taxa_			int

SET NOCOUNT ON

if len(@taxa) > 0
begin

	DECLARE @siteno AS int
	SET @siteno = (select  no from empresa (nolock) where site = @site)	



	select 
		@taxa_ = taxa 
	from 
		st (nolock)
	inner join
		taxasiva(nolock) on taxasiva.codigo = st.tabiva
    where 
		taxa = @taxa	and site_nr = isnull(@siteno,-1) and ref=@ref




	select @contador=@@ROWCOUNT	



	If @contador > 0 
		select getdate() AS DateMessag, 1 As StatMessag, 'Taxa existe' As DescMessag, @taxa AS taxa;	
	ELSE	
		select getdate() AS DateMessag, 0 As StatMessag, 'Taxa não existe' As DescMessag, @taxa AS taxa;	
	   	
end
else
begin
	select getdate() AS DateMessag, 0 As StatMessag, 3 AS ActionMessage, 'NO DATA' As DescMessag, @taxa AS taxa;
end

GO
GRANT EXECUTE on dbo.up_Documentos_verifica_IVA TO PUBLIC
GRANT Control on dbo.up_Documentos_verifica_IVA TO PUBLIC
GO