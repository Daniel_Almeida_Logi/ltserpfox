/*
DOCUMENTOS UPDATE/INSERT
D.Almeida, 04-05-2020
revisão J.Costa 26-10-2020
--------------------------------
bo
Pesquisa do registo na tabela BO a partir de 1 campos de pesquisa:
	 stamp (@stamp)

Se o documento não existe cria novo.

Se o documento existe edita.

A execução passa pelo envio de todas as variáveis que podem assumir 3 estados:
NULL						- significa que não é para fazer nada ao campo (só para o update)
''							- significa que é para colocar o campo sem nada, limpar o campo


O processo inicializa pela pesquisa de um conjunto de 4 variáveis, todas preenchidas ou não:
	-  stamp @stamp

Perante estas variáveis, será feita uma procura na tabela bo por registos que coincidam
com respectivos valores. 

No final do processo são retornados valores após o insert ou update:
	* DateMessag, StatMessag, ActionMessage, DescMessag, number (no), dep (estab)

StatMessag: 
	1 - OK
	0 - ERROR

ActionMessage/DescMessag: 
	0 - UPDATE
	1 - INSERT
	2 - NEED MORE FIELDS
	3 - NO DATA
	4 - WRONG TYPE
	5 - MORE THAN ONE DOC


ex. no INSERT:
DateMessag				StatMessag	ActionMessage	DescMessag			number	seriesNumber  site     year
2020-04-04 20:06:19.207	1			1				INSERT				1	5                 Loja 1   2020

ex. no UPDATE:
DateMessag				StatMessag	ActionMessage	DescMessag			number	seriesNumber site        year
2020-04-04 20:04:52.017	1			0				UPDATE				1		14           Loja 1     2020



-------------------------------------------------------------------------

PROCEDURE
Criada procedure up_Documentos_header_UpdateInsert com parâmetros 


EXECUÇÃO DA SP:
---------------


EXEC up_Documentos_header_UpdateInsert
EXEC up_Documentos_header_UpdateInsert '2df2f1db77fd4b7fa33026219',N'Loja 1',41,230,2020,NULL,10783400,0,N'Ricardo Rosa',N'Cliente',N'123456789',3.12,N'EURO',NULL,0.19,NULL,N'',N'',N'Lisboa',N'Alvalade',N'1000-000',N'PT',NULL,N'2020-03-02',N'21:00:00',0,N'Pendente',N'2020-05-29',N'',NULL,N'',N'',2.94,0,0,0,0,0,0,0,0,0.18,0,0,0,0,0,0,0,0,2
exec up_Documentos_header_UpdateInsert 
 'ADMADB87AD4-C4AA-4182-943', 'Loja 1',41,NULL,2020,NULL,  --identificadores do documento
 215, 0, NULL, 'Cliente',  --entidade
 NULL, NULL, NULL, NULL, NULL, NULL , -- financeiro
 NULL, NULL, NULL, NULL, NULL, NULL , NULL, NULL, -- shipping info 
 NULL, NULL, '2020-05-06', NULL, NULL,   -- general 
 NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, -- totais base incidencia por codigo
 NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, -- totais iva por codigo
 NULL -- stock movimentado



VERIFICAÇÃO:
------------
select usrdata,usrhora,* from bo(nolock) where obrano = 4 and boano=2020 and ndos=41 and site='Loja 1'

select * from bo2(nolock) where bo2stamp='E17B8D3F-BCDB-43D2-AAA8-D'

*/




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_Documentos_header_UpdateInsert]') IS NOT NULL
    drop procedure up_Documentos_header_UpdateInsert
go



CREATE PROCEDURE up_Documentos_header_UpdateInsert	
	 
	 --identificadores do documento
	 @IdInfo_stamp			    varchar(30), /*bostamp*/
	 @IDInfo_site			    varchar(10), /*site*/	
	 @IDInfo_seriesNumber       int, /*ndos*/
	 @IDInfo_number             int, /*obrano*/
	 @IDInfo_year               int, /*boano*/
	 @IdInfo_extCode			varchar(10), /*extCode*/

	--entidade
	 @EntityInfo_number								int, /*no*/
	 @EntityInfo_dep								int, /*estab*/
	 @EntityInfo_name								varchar(80), /*nome*/
	 @EntityInfo_type								varchar(50),
	 @EntityInfo_vatNr								varchar(20),/*ncont*/
	 

	 -- financeiro
	 @FinanceInfo_retailPrice						numeric(19,6), 
	 @FinanceInfo_currency							varchar(10), 
	 @FinanceInfo_commercialDiscount				numeric(19,6),
	 @FinanceInfo_vatValue							numeric(19,6),
	 @FinanceInfo_discountValue						numeric(19,6),
	 @FinanceInfo_paymentType						varchar(50),
	 @FinanceInfo_paymentMoment                     varchar(50),


	 -- shipping info 
	 @ShippingInfo_mode			     varchar(50),
	 @ShippingInfo_city	             varchar(50), 
	 @ShippingInfo_address		     varchar(50), 
	 @ShippingInfo_zipCode		     varchar(50), 
	 @ShippingInfo_countryCode	     varchar(50),
	 @ShippingInfo_sent	             bit,
	 @ShippingInfo_deliveryDate	     date = null,
	 @ShippingInfo_deliveryTime      varchar(8) = null,
	

	  -- general 
	 @GeneralInfo_status			 bit,
	 @GeneralInfo_deliveryStatus	 varchar(50), 
	 @GeneralInfo_docDate		     date, 
	 @GeneralInfo_prescriptionId	 varchar(50),
	 @GeneralInfo_obs	             varchar(254),
	 @GeneralInfo_accessPin	         varchar(200),
	 @GeneralInfo_optionPin          varchar(200),
	 @GeneralInfo_ticketId           varchar(254),
	 @GeneralInfo_linkToPayment      varchar(254),
	 @GeneralInfo_paymentEntity		 varchar(100),
	 @GeneralInfo_paymentReference	 varchar(100),
	 @GeneralInfo_paymentValue		 numeric(19,6),
	 --34
	 
	 -- totais base incidencia por codigo
	 @TotalNoVat1		             numeric(19,6),
	 @TotalNoVat2					 numeric(19,6),		
	 @TotalNoVat3		             numeric(19,6),	
	 @TotalNoVat4		             numeric(19,6),	
	 @TotalNoVat5		             numeric(19,6),	
	 @TotalNoVat6		             numeric(19,6),	
	 @TotalNoVat7		             numeric(19,6),	
	 @TotalNoVat8		             numeric(19,6),	
	 @TotalNoVat9		             numeric(19,6),	
	 @TotalNoVat10		             numeric(20,6),	
	 @TotalNoVat11		             numeric(20,6),
	 @TotalNoVat12		             numeric(20,6),
	 @TotalNoVat13		             numeric(20,6),		
	
	 -- totais iva por codigo
	 @TotalVat1		                 numeric(19,6),
	 @TotalVat2					     numeric(19,6),		
	 @TotalVat3		                 numeric(19,6),	
	 @TotalVat4		                 numeric(19,6),	
	 @TotalVat5		                 numeric(19,6),	
	 @TotalVat6		                 numeric(19,6),	
	 @TotalVat7		                 numeric(19,6),	
	 @TotalVat8		                 numeric(19,6),	
	 @TotalVat9		                 numeric(19,6),
	 @TotalVat10	                 numeric(20,6),
	 @TotalVat11	                 numeric(20,6),
	 @TotalVat12	                 numeric(20,6),
	 @TotalVat13	                 numeric(20,6),

	 -- stock movimentado
	 @Totalqtt                       int,

	 -- address Info 
	 @AddressInfo_address			 varchar(55) = null,
	 @AddressInfo_zipCode	         varchar(50) = null, 
	 @AddressInfo_city		         varchar(43) = null,
	 @AddressInfo_zone		         varchar(20) = null,
	 @AddressInfo_countryCode		 varchar(10) = null,


	 @ShippingInfo_operatorId        varchar(10) = null,
	 @EntityInfo_email				 varchar(45) = null,
	 @EntityInfo_mobilePhone		 varchar(50) = null,
	 @EntityInfo_telephone		     varchar(50) = null



AS


DECLARE @contador			int = 0
DECLARE @stamp				varchar(50)


DECLARE @lastChangeDate		datetime
DECLARE @lastChangeTime		VARCHAR(8)
DECLARE @CreationDate		datetime
DECLARE @CreationTime		VARCHAR(8)

DECLARE @doccont            int
DECLARE @docSeriesName		varchar(24)


DECLARE @operator		    VARCHAR(8) = 'ONL'



--variaveis para apresentar no final
set @IdInfo_stamp = rtrim(ltrim(isnull(@IdInfo_stamp,'')))


declare @difhour int = 0

declare @clientType varchar(20) = ''
declare @clientVat varchar(20) = ''

declare @wareNumber int = 1

declare @defaultCurrency varchar(20) = ''



SET @lastChangeDate = convert(varchar,getdate(),23)
SET @lastChangeTime = convert(varchar(8),getdate(),24)
SET @CreationDate = convert(varchar,getdate(),23)
SET @CreationTime = convert(varchar(8),getdate(),24)

DECLARE @stampid CHAR(25)  


DECLARE @TotalNoVat	numeric(20,6) = 0.0
DECLARE @TotalVat	numeric(20,6) = 0.0

DECLARE @ShippingInfo_deliveryDateTime datetime = NULL


set @TotalNoVat = ROUND(@TotalNoVat1,2) + ROUND(@TotalNoVat2,2) +ROUND(@TotalNoVat3,2) +
ROUND(@TotalNoVat4,2) + ROUND(@TotalNoVat5,2) + ROUND(@TotalNoVat6,2) + ROUND(@TotalNoVat7,2) +
ROUND(@TotalNoVat8,2) + ROUND(@TotalNoVat9,2) + ROUND(@TotalNoVat10,2) + ROUND(@TotalNoVat11,2)
 + ROUND(@TotalNoVat12,2) + ROUND(@TotalNoVat13,2)

set @TotalVat = ROUND(@TotalVat1,2) + ROUND(@TotalVat2,2) +ROUND(@TotalVat3,2) +
ROUND(@TotalVat4,2) + ROUND(@TotalVat5,2) + ROUND(@TotalVat6,2) + ROUND(@TotalVat7,2) +
ROUND(@TotalVat8,2) + ROUND(@TotalVat9,2) + ROUND(@TotalVat10,2) + ROUND(@TotalVat11,2)
 + ROUND(@TotalVat12,2) + ROUND(@TotalVat13,2)



if(@ShippingInfo_deliveryDate is not null)
    set @ShippingInfo_deliveryDateTime =  CONVERT(DATETIME,isnull(@ShippingInfo_deliveryDate,'19000101')) + CONVERT(DATETIME,CAST(isnull(@ShippingInfo_deliveryTime,'00:00:00') as DATETIME) )



if(@IDInfo_year=null)
   set @IDInfo_year = YEAR(isnull(@GeneralInfo_docDate,'1900'))





-- O campo site é obrigatorio
IF (len(isnull(@IdInfo_site,''))>0 and (len(isnull(@EntityInfo_type,''))>0))
		
BEGIN	
	/*
		valida se deve documento já existe
	*/

	select @difhour = (select isnull(numValue,0) from B_Parameters_site(nolock) where stamp='ADM0000000008' and site=@IDInfo_site)

	select @defaultCurrency = (select ltrim(rtrim(isnull(textValue,'EURO'))) from B_Parameters(nolock) where stamp='ADM0000000260')
	/* retorna ultimo numero para esta serie*/
	select @doccont = ISNULL(MAX(bo2.u_doccont),0) 
	from bo2 (nolock)
	inner join bo (nolock) on bo.bostamp=bo2.bo2stamp
	where bo.ndos=@IDInfo_seriesNumber


	select @docSeriesName = ltrim(rtrim(isnull(nmdos,'')))
	from ts (nolock)
	where ndos = @IDInfo_seriesNumber

	select TOP 1
	 	@wareNumber = isnull(empresa_arm.armazem,1)
	from 
		empresa(nolock)
	inner join 
		empresa_arm(nolock) on empresa.no = empresa_arm.empresa_no
	where 
	   site=@IDInfo_site

	select 
		@contador = count(*) 
	from 
		bo(nolock)
	where
		bostamp = @IdInfo_stamp



	if(ltrim(rtrim(lower(@EntityInfo_type)))='cliente' and isnull(@EntityInfo_number,0)>0) 
	begin
		select 
		  top 1 
			@EntityInfo_name = case when isnull(@EntityInfo_name,'') = '' then left(ltrim(rtrim(nome)),80) else @EntityInfo_name end
			,@EntityInfo_number = isnull(no,0)
			,@EntityInfo_dep =  isnull(estab,0)
			,@clientType = left(ltrim(rtrim(isnull(tipo,''))),20)
			,@clientVat  = left(ltrim(rtrim(isnull(ncont,''))),20) 
		from b_utentes (nolock)
		where no=@EntityInfo_number and estab = @EntityInfo_dep
	end

	if(ltrim(rtrim(lower(@EntityInfo_type)))='fornecedor'  and isnull(@EntityInfo_number,0)>0)
	begin
		select 
		  top 1 
			@EntityInfo_name = case when isnull(@EntityInfo_name,'') = '' then left(ltrim(rtrim(nome)),80) else @EntityInfo_name end
			,@EntityInfo_number = isnull(no,0)
			,@EntityInfo_dep = isnull(estab,0)
			,@clientVat  = left(ltrim(rtrim(isnull(ncont,''))),20) 
		from fl (nolock)
		where no=@EntityInfo_number and estab = @EntityInfo_dep
	end

	
	/* retorna ultimo numero interno da bo2 para esta serie*/
	select @doccont = ISNULL(MAX(bo2.u_doccont),0) + 1
	from bo2 (nolock)
	inner join bo (nolock) on bo.bostamp=bo2.bo2stamp



	if(@IdInfo_stamp IS NULL OR @IdInfo_stamp=''  or @contador=0)

	BEGIN

		/* retorna ultimo numero de documento para esta serie*/
		--select @IDInfo_number = ISNULL(MAX(obrano)+1,1) 
		--from bo (nolock)
		--where bo.ndos=@IDInfo_seriesNumber and boano=@IDInfo_year

		
		DECLARE @tabNewDocNr TABLE (newDocNr NUMERIC(10,0))
		INSERT INTO @tabNewDocNr (newDocNr)
		EXEC up_get_newDocNr 'BO', @IDInfo_seriesNumber, @IDInfo_site, 1
		SET  @IDInfo_number = (SELECT newDocNr FROM @tabNewDocNr)

		Insert into bo2 (
			bo2stamp
			,autotipo
			,pdtipo
			,etotalciva
			,u_doccont
			,ETOTIVA
			,telefone
			,email
			,xpddata
			,xpdhora
			,xpdviatura
			,morada
			,codpost
			,contacto
			,armazem
			,u_class
			,ousrinis
			,ousrdata
			,ousrhora
			,usrinis
			,usrdata
			,usrhora
			,ATDocCode
			,ebo71_bins
			,ebo72_bins
			,ebo81_bins
			,ebo82_bins
			,ebo91_bins
			,ebo92_bins
			,ebo71_iva
			,ebo72_iva
			,ebo81_iva
			,ebo82_iva
			,ebo91_iva
			,ebo92_iva
			,ivatx1
			,ivatx2
			,ivatx3
			,ivatx4
			,ivatx5
			,ivatx6
			,ivatx7
			,ivatx8
			,ivatx9
			,nrReceita
			,codext
			,morada_ent
			,localidade_ent
			,codpost_ent
			,pais_ent
			,status
		    ,modo_envio
			,pagamento
			,pinAcesso
			,pinOpcao
			,momento_pagamento
			,ebo101_bins
			,ebo111_bins
			,ebo121_bins
			,ebo131_bins
			,ebo101_iva
			,ebo111_iva
			,ebo121_iva
			,ebo131_iva
			,ivatx10
			,ivatx11
			,ivatx12
			,ivatx13
			,iddistribuidor
			,ticketId
			,paymentUrl
			,paymentEntity
			,paymentReference
			,paymentValue
		)
		Values (
			ltrim(rtrim(@IdInfo_stamp))
			,1
			,1
			,ROUND(isnull(@FinanceInfo_retailPrice,0),2)
			,@doccont
			,ROUND(isnull(@FinanceInfo_vatValue,0),2)
			,left(ltrim(rtrim(isnull(@EntityInfo_telephone,''))),50)
			,left(ltrim(rtrim(isnull(@EntityInfo_email,''))),45)
			,'1900-01-01 00:00:00.000'
			,''
			,''
			,REPLACE(left(ltrim(rtrim(isnull(@ShippingInfo_address,''))),50),'''','´')
			,left(ltrim(rtrim(isnull(@ShippingInfo_zipCode,''))),45)
			,left(ltrim(rtrim(isnull(@EntityInfo_mobilePhone,''))),50)
			,@wareNumber
			,''
			,@operator
			, convert(varchar,dateadd(HOUR, @difhour, getdate()),102)
			, convert(varchar,dateadd(HOUR, @difhour, getdate()),8)
			,@operator
			, convert(varchar,dateadd(HOUR, @difhour, getdate()),102)
			, convert(varchar,dateadd(HOUR, @difhour, getdate()),8)
			,''
			,Round(isnull(@TotalNoVat7,0),2)
			,Round(isnull(@TotalNoVat7,0),2)
			,Round(isnull(@TotalNoVat8,0),2)
			,Round(isnull(@TotalNoVat8,0),2)
			,Round(isnull(@TotalNoVat9,0),2)
			,Round(isnull(@TotalNoVat9,0),2)
			,Round(isnull(@TotalVat7,0),2)
			,Round(isnull(@TotalVat7,0),2)
			,Round(isnull(@TotalVat8,0),2)
			,Round(isnull(@TotalVat8,0),2)
			,Round(isnull(@TotalVat9,0),2)
			,Round(isnull(@TotalVat9,0),2)
			,ISNULL((select top 1 taxa from taxasiva where codigo=1),0.0)
			,ISNULL((select top 1 taxa from taxasiva where codigo=2),0.0)
			,ISNULL((select top 1 taxa from taxasiva where codigo=3),0.0)
			,ISNULL((select top 1 taxa from taxasiva where codigo=4),0.0)
			,ISNULL((select top 1 taxa from taxasiva where codigo=5),0.0)
			,ISNULL((select top 1 taxa from taxasiva where codigo=6),0.0)
			,ISNULL((select top 1 taxa from taxasiva where codigo=7),0.0)
			,ISNULL((select top 1 taxa from taxasiva where codigo=8),0.0)
			,ISNULL((select top 1 taxa from taxasiva where codigo=9),0.0)
			,left(ltrim(rtrim(isnull(@GeneralInfo_prescriptionId,''))),24)
			,left(ltrim(rtrim(isnull(@IdInfo_extCode,''))),60)
			,REPLACE(left(ltrim(rtrim(isnull(@ShippingInfo_address,''))),50),'''','´')
			,left(ltrim(rtrim(isnull(@ShippingInfo_city,''))),50)
			,left(ltrim(rtrim(isnull(@ShippingInfo_zipCode,''))),60)
			,left(ltrim(rtrim(isnull(@ShippingInfo_countryCode,''))),50)
			,left(ltrim(rtrim(isnull(@GeneralInfo_deliveryStatus,''))),50)
			,left(ltrim(rtrim(isnull(@ShippingInfo_mode,''))),50)
			,left(ltrim(rtrim(isnull(@FinanceInfo_paymentType,''))),50)
			,left(ltrim(rtrim(isnull(@GeneralInfo_accessPin,''))),10)
			,left(ltrim(rtrim(isnull(@GeneralInfo_optionPin,''))),10)
			,left(ltrim(rtrim(isnull(@FinanceInfo_paymentMoment,''))),99)
			,Round(isnull(@TotalNoVat10,0.0),2)
			,Round(isnull(@TotalNoVat11,0.0),2)
			,Round(isnull(@TotalNoVat12,0.0),2)
			,Round(isnull(@TotalNoVat13,0.0),2)
			,Round(isnull(@TotalVat10,0.0),2)
			,Round(isnull(@TotalVat11,0.0),2)
			,Round(isnull(@TotalVat12,0.0),2)
			,Round(isnull(@TotalVat13,0.0),2)
			,ISNULL((select top 1 taxa from taxasiva(nolock) where codigo=10),0.0)
			,ISNULL((select top 1 taxa from taxasiva(nolock)  where codigo=11),0.0)
			,ISNULL((select top 1 taxa from taxasiva(nolock)  where codigo=12),0.0)
			,ISNULL((select top 1 taxa from taxasiva(nolock)  where codigo=13),0.0)
			,left(ltrim(rtrim(isnull(@ShippingInfo_operatorId,''))),10)
			,left(ltrim(rtrim(isnull(@GeneralInfo_ticketId,''))),254)
			,ltrim(rtrim(isnull(@GeneralInfo_linkToPayment,'')))
			,ltrim(rtrim(isnull(@GeneralInfo_paymentEntity,'')))
			,ltrim(rtrim(isnull(@GeneralInfo_paymentReference,'')))
			,ROUND(isnull(@GeneralInfo_paymentValue,0),2)
		)
	
		Insert into bo
		(	
			bostamp
			,ndos
			,nmdos
			,obrano
			,dataobra
			,nome
			,nome2
			,no
			,estab
			,morada
			,local
			,codpost
			,tipo
			,tpdesc
			,ncont
			,boano
			,etotaldeb
			,dataopen
			,ecusto
			,etotal
			,moeda
			,memissao
			,origem
			,site
			,vendedor
			,vendnm
			,sqtt14
			,ebo_1tvall
			,ebo_2tvall
			,ebo_totp1
			,ebo_totp2
			,ebo11_bins
			,ebo12_bins
			,ebo21_bins
			,ebo22_bins
			,ebo31_bins
			,ebo32_bins
			,ebo41_bins
			,ebo42_bins
			,ebo51_bins
			,ebo52_bins
			,ebo61_bins
			,ebo62_bins
			,ebo11_iva
			,ebo12_iva
			,ebo21_iva
			,ebo22_iva
			,ebo31_iva
			,ebo32_iva
			,ebo41_iva
			,ebo42_iva
			,ebo51_iva
			,ebo52_iva
			,ebo62_iva
			,ocupacao
			,fechada
			,logi1
			,u_dataentr
			,obs
			,ousrinis
			,ousrdata
			,ousrhora
			,usrinis
			,usrdata
			,usrhora
			,datafinal
			,ccusto
			,pnome
			,pno
			,tabIva
			,EDESCC
		
		)
		Values (
	
			ltrim(rtrim(@IdInfo_stamp))
			,@IDInfo_seriesNumber
			,@docSeriesName
			,@IDInfo_number
			,isnull(@GeneralInfo_docDate,'1900-01-01')
			,ltrim(rtrim(@EntityInfo_name))
			,''
			,@EntityInfo_number
			,@EntityInfo_dep
			,REPLACE(left(ltrim(rtrim(isnull(@AddressInfo_address,''))),50),'''','´')
			,left(ltrim(rtrim(isnull(@AddressInfo_city,''))),50)
			,left(ltrim(rtrim(isnull(@AddressInfo_zipCode,''))),45)
			,@clientType
			,''
			,left(ltrim(rtrim(isnull(@EntityInfo_vatNr,''))),20)
			,isnull(@IDInfo_year,'1900')
			,ROUND((isnull(@FinanceInfo_retailPrice,0) - isnull(@FinanceInfo_vatValue,0)),2)
			,convert(varchar,dateadd(HOUR, @difhour, getdate()),102)
			,ROUND(isnull(@TotalVat,0),2)
			,ROUND(isnull(@FinanceInfo_retailPrice,0),2)
			,isnull(@FinanceInfo_currency,@defaultCurrency)
			,@defaultCurrency
			,'BO'
			, @IDInfo_site
			, 1
			,left('Administrador de Sistema',20)
			,isnull(@Totalqtt,0)
			,ROUND(isnull(@TotalNoVat,0),2)
			,ROUND(isnull(@TotalNoVat,0),2)
			,ROUND(isnull(@TotalNoVat,0),2)
			,ROUND(isnull(@TotalNoVat,0),2)
			,ROUND(isnull(@TotalNoVat1,0),2)
			,ROUND(isnull(@TotalNoVat1,0),2)
			,ROUND(isnull(@TotalNoVat2,0),2)
			,ROUND(isnull(@TotalNoVat2,0),2)
			,ROUND(isnull(@TotalNoVat3,0),2)
			,ROUND(isnull(@TotalNoVat3,0),2)
			,ROUND(isnull(@TotalNoVat4,0),2)
			,ROUND(isnull(@TotalNoVat4,0),2)
			,ROUND(isnull(@TotalNoVat5,0),2)
			,ROUND(isnull(@TotalNoVat5,0),2)
			,ROUND(isnull(@TotalNoVat6,0),2)
			,ROUND(isnull(@TotalNoVat6,0),2)
			,ROUND(isnull(@TotalVat1,0),2)
			,ROUND(isnull(@TotalVat1,0),2)
			,ROUND(isnull(@TotalVat2,0),2)
			,ROUND(isnull(@TotalVat2,0),2)
			,ROUND(isnull(@TotalVat3,0),2)
			,ROUND(isnull(@TotalVat3,0),2)
			,ROUND(isnull(@TotalVat4,0),2)
			,ROUND(isnull(@TotalVat4,0),2)
			,ROUND(isnull(@TotalVat5,0),2)
			,ROUND(isnull(@TotalVat5,0),2)
			,ROUND(isnull(@TotalVat6,0),2)
			,4
			,case when isnull(@GeneralInfo_status,1) = 1 then 0 else 1 end 
			,isnull(@ShippingInfo_sent,0)
			,isnull(@ShippingInfo_deliveryDateTime,'1900-01-01')
			,isnull(@GeneralInfo_obs,'')
			,@operator
			, convert(varchar,dateadd(HOUR, @difhour, getdate()),102)
			, convert(varchar,dateadd(HOUR, @difhour, getdate()),8)
			,@operator
			, convert(varchar,dateadd(HOUR, @difhour, getdate()),102)
			, convert(varchar,dateadd(HOUR, @difhour, getdate()),8)
			, convert(varchar,dateadd(HOUR, @difhour, getdate()),102)
			, 0 -- TODO -cc
			,'Terminal 99'
			,'99'
			,0
			,ROUND(isnull(@FinanceInfo_discountValue,0),2) 
			
		)



--		--mensagem final
		select getdate() AS DateMessag, 1 As StatMessag, 1 AS ActionMessage, 'INSERT' As DescMessag, @IDInfo_number AS Document_number, 
		@IDInfo_seriesNumber As Document_seriesNumber, @IDInfo_site as Document_site, @IDInfo_year as Document_year;

		return 
	END /*fim ciclo insert*/

	ELSE
	
	/*  inicio ciclo update*/
	BEGIN
				
					
			    DECLARE @statusClosed bit = null
				print @GeneralInfo_status

				/*estado do documento*/
				if(@GeneralInfo_status is not  null)
				begin
					if(@GeneralInfo_status=1)
						set @statusClosed = 0
					else
						set @statusClosed = 1
					end
				end

			
				UPDATE	
					BO
				SET		
					dataobra	=	COALESCE(@GeneralInfo_docDate, dataobra),  
					nome		=	ltrim(rtrim(COALESCE(@EntityInfo_name, nome))),
					nome2		=	'',
					no			=	COALESCE(@EntityInfo_number, no), 
					estab		=	COALESCE(@EntityInfo_dep, estab), 
					morada		=	LEFT(LTRIM(RTRIM(REPLACE(COALESCE(@AddressInfo_address, morada),'''','´'))),55), 
					local		=	COALESCE(@AddressInfo_city, local), 
					codpost		=	COALESCE(@AddressInfo_zipCode, codpost), 
					tipo		=	COALESCE(@clientType, tipo), 
					tpdesc		=	'',
					boano		=	COALESCE(@IDInfo_year, boano), 
					etotaldeb	=	ROUND((isnull(@FinanceInfo_retailPrice,0) - isnull(@FinanceInfo_vatValue,0)),2),
					dataopen	=	convert(varchar,dateadd(HOUR, @difhour, getdate()),102), 
					ecusto		=	ROUND((isnull(@FinanceInfo_retailPrice,0) - isnull(@FinanceInfo_vatValue,0)),2),
					etotal		=	ROUND(isnull(@FinanceInfo_retailPrice,0),2),
					moeda		=	COALESCE(@FinanceInfo_currency,moeda),
					origem		=	'BO',
					site		=	@IDInfo_site,
					vendedor	=	1,
					vendnm		=	left('Administrador de Sistema',20),
					sqtt14		=	isnull(@Totalqtt,0),
					ebo_1tvall	=	ROUND(isnull(@TotalVat,0),2),
					ebo_2tvall	=	ROUND(isnull(@TotalVat,0),2),
					ebo_totp1	=	ROUND(isnull(@TotalVat,0),2),
					ebo_totp2	=	ROUND(isnull(@TotalVat,0),2),
					ebo11_bins	=	ROUND(isnull(@TotalNoVat1,0),2),
					ebo12_bins	=	ROUND(isnull(@TotalNoVat1,0),2),
					ebo21_bins	=	ROUND(isnull(@TotalNoVat2,0),2),
					ebo22_bins	=	ROUND(isnull(@TotalNoVat2,0),2),
					ebo31_bins	=	ROUND(isnull(@TotalNoVat3,0),2),
					ebo32_bins	=	ROUND(isnull(@TotalNoVat3,0),2),
					ebo41_bins	=	ROUND(isnull(@TotalNoVat4,0),2),
					ebo42_bins	=	ROUND(isnull(@TotalNoVat4,0),2),
					ebo51_bins	=	ROUND(isnull(@TotalNoVat5,0),2),
					ebo52_bins	=	ROUND(isnull(@TotalNoVat5,0),2),
					
					ebo11_iva	=	ROUND(isnull(@TotalVat1,0),2),
					ebo12_iva	=	ROUND(isnull(@TotalVat1,0),2),
					ebo21_iva	=	ROUND(isnull(@TotalVat2,0),2),
					ebo22_iva	=	ROUND(isnull(@TotalVat2,0),2),
					ebo31_iva	=	ROUND(isnull(@TotalVat3,0),2),
					ebo32_iva	=	ROUND(isnull(@TotalVat3,0),2),
					ebo41_iva	=	ROUND(isnull(@TotalVat4,0),2),
					ebo42_iva	=	ROUND(isnull(@TotalVat4,0),2),
					ebo51_iva	=	ROUND(isnull(@TotalVat5,0),2),
					ebo52_iva	=	ROUND(isnull(@TotalVat5,0),2),
					ocupacao	=	4,
					fechada		=	COALESCE(@statusClosed,fechada),
					usrinis		=	@operator,
					usrdata		=	convert(varchar,dateadd(HOUR, @difhour, getdate()),102),
					usrhora		=	convert(varchar,dateadd(HOUR, @difhour, getdate()),8),
					Logi1		=	COALESCE(@ShippingInfo_sent,Logi1),
					u_dataentr	=   COALESCE(@ShippingInfo_deliveryDateTime,u_dataentr),
					obs			=	ltrim(rtrim(COALESCE(@GeneralInfo_obs,obs))),
					datafinal	=	convert(varchar,dateadd(HOUR, @difhour, getdate()),102),
					ccusto		=	0, --'<<cabdoc.ccusto>>' TODO
					ncont       =   left(ltrim(rtrim(COALESCE(@EntityInfo_vatNr,ncont))),20),
					EDESCC      =   ROUND(isnull(@FinanceInfo_discountValue,0),2) 

				
				WHERE	
					BOSTAMP =  ltrim(rtrim(@IdInfo_stamp))

				UPDATE  
					BO2
				SET		
					autotipo	= 1,
					pdtipo		= 1,
					etotalciva	= ROUND(isnull(@FinanceInfo_retailPrice,0),2),
					u_doccont 	= @doccont,
					ETOTIVA		= ROUND(isnull(@FinanceInfo_vatValue,0),2),
					usrinis		= @operator,
					usrdata 	= convert(varchar,dateadd(HOUR, @difhour, getdate()),102),
					usrhora 	= convert(varchar,dateadd(HOUR, @difhour, getdate()),8),
					armazem 	= @wareNumber,
					morada		= REPLACE(left(ltrim(rtrim(COALESCE(@ShippingInfo_address,morada))),50),'''','´'),
					codpost		= left(ltrim(rtrim(COALESCE(@ShippingInfo_zipCode,codpost))),45),
					nrReceita   = left(ltrim(rtrim(COALESCE(@GeneralInfo_prescriptionId,''))),20),
				    pinAcesso   = left(ltrim(rtrim(COALESCE(@GeneralInfo_accessPin,''))),10),
			        pinOpcao   = left(ltrim(rtrim(COALESCE(@GeneralInfo_optionPin,''))),10),
					codext      = left(ltrim(rtrim(COALESCE(@IdInfo_extCode,codext))),50),   
					morada_ent  = REPLACE(left(ltrim(rtrim(COALESCE(@ShippingInfo_address,morada_ent))),50),'''','´'),
					localidade_ent = left(ltrim(rtrim(COALESCE(@ShippingInfo_city,localidade_ent))),50),
					codpost_ent = left(ltrim(rtrim(COALESCE(@ShippingInfo_zipCode,codpost_ent))),50),
					pais_ent = left(ltrim(rtrim(COALESCE(@ShippingInfo_countryCode,pais_ent))),50),
					status = left(ltrim(rtrim(COALESCE(@GeneralInfo_deliveryStatus,status))),50),
					modo_envio = left(ltrim(rtrim(COALESCE(@ShippingInfo_mode,modo_envio))),50),
					pagamento  = left(ltrim(rtrim(COALESCE(@FinanceInfo_paymentType,pagamento))),50),
					momento_pagamento = left(ltrim(rtrim(COALESCE(@FinanceInfo_paymentMoment,momento_pagamento))),99),
					ebo101_bins = Round(isnull(@TotalNoVat10,0.0),2),
					ebo111_bins = Round(isnull(@TotalNoVat11,0.0),2),
					ebo121_bins = Round(isnull(@TotalNoVat13,0.0),2),
					ebo131_bins = Round(isnull(@TotalNoVat13,0.0),2),
					ebo101_iva  = Round(isnull(@TotalVat10,0.0),2),
					ebo111_iva  = Round(isnull(@TotalVat11,0.0),2),
					ebo121_iva  = Round(isnull(@TotalVat12,0.0),2),
					ebo131_iva = Round(isnull(@TotalVat13,0.0),2),
					ivatx10    = ISNULL((select top 1 taxa from taxasiva(nolock) where codigo=10),0.0),
					ivatx11    =ISNULL((select top 1 taxa from taxasiva(nolock)  where codigo=11),0.0),
					ivatx12    = ISNULL((select top 1 taxa from taxasiva(nolock)  where codigo=12),0.0),
					ivatx13    =ISNULL((select top 1 taxa from taxasiva(nolock)  where codigo=13),0.0),
					iddistribuidor = left(ltrim(rtrim(COALESCE(@ShippingInfo_operatorId,iddistribuidor))),50),
					email          = left(ltrim(rtrim(COALESCE(@EntityInfo_email,email))),45),
					contacto    = left(ltrim(rtrim(isnull(@EntityInfo_mobilePhone,''))),50),
					telefone    = left(ltrim(rtrim(isnull(@EntityInfo_telephone,''))),50),
					paymentUrl = ltrim(rtrim(isnull(@GeneralInfo_linkToPayment,''))),
					paymentEntity = ltrim(rtrim(isnull(@GeneralInfo_paymentEntity,''))),
					paymentReference = ltrim(rtrim(isnull(@GeneralInfo_paymentReference,''))),
					paymentValue = ROUND(isnull(@GeneralInfo_paymentValue,0),2)

				FROM	
					BO2
				WHERE	
					BO2STAMP =  ltrim(rtrim(@IdInfo_stamp))


			
				select getdate() AS DateMessag, 1 As StatMessag, 0 AS ActionMessage, 'UPDATE' As DescMessag, @IDInfo_number AS Document_number, 
				@IDInfo_seriesNumber As Document_seriesNumber, @IDInfo_site as Document_site, @IDInfo_year as Document_year;

				return

	END
			
	 /*fim ciclo update*/

ELSE
BEGIN

	--print('Campos devem ser numéricos: number e dep')
	select getdate() AS DateMessag, 0 As StatMessag, 4 AS ActionMessage, 'WRONG TYPE' As DescMessag, @IDInfo_number AS Document_number, 
	@IDInfo_seriesNumber As Document_seriesNumber, @IDInfo_site as Document_site, @IDInfo_year as Document_year;
END

            
GO
Grant Execute On up_Documentos_header_UpdateInsert to Public
Grant Control On up_Documentos_header_UpdateInsert to Public
GO
			
           
			
	