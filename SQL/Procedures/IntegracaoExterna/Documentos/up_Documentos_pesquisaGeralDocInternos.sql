/* SP Pesquisa Documentos
J.Gomes, 2020-04-23
--------------------------------

Listagem de Documentos internos baseada em diversos campos.
e com possibilidade de filtragem acima de determinada data e hora de alteração.

Pesquisa nas tabelas: 	
BO, Bo2,Bi, ts, b_utentes

por 12 campos:
	@number, @dep, @vatNr, @email, @ref, @design, @site, @status, @numdoc, @seriesNumber
	, @lastChangeDate, @lastChangeTime
	
Campos obrigatórios: nenhum

OUTPUT:
---------
IDInfo_number, IDInfo_site, IDInfo_seriesNumber, EntityInfo_number, EntityInfo_dep, EntityInfo_name
, EntityInfo_type, FinanceInfo_retailPrice,	FinanceInfo_currency, FinanceInfo_vatInc
, OperationInfo_CreationDate, OperationInfo_CreationTime, OperationInfo_lastChangeDate
, OperationInfo_lastChangeTime,	GeneralInfo_status,	GeneralInfo_deliveryStatus
, GeneralInfo_docDate, StockInfo_qtt, DesignInfo_designation


EXECUÇÃO DA SP:
---------------
exec up_Documentos_pesquisaGeralDocInternos '@number', '@dep', '@vatNr', '@email', '@ref', '@design'
, '@site', '@status', '@numdoc', '@seriesNumber', '@lastChangeDate', '@lastChangeTime','@dateInit','@dateEnd' 


	exec up_Documentos_pesquisaGeralDocInternos  '5', '0','500666999','mail@teste.pt','5440987', 'Ben-U-Ron 1000mg 18 Comp','Loja 1',1,67,2,'2019-10-28',''
	exec up_Documentos_pesquisaGeralDocInternos  '', '','500666999','mail@teste.pt','5440987', 'Ben-U-Ron 1000mg 18 Comp','Loja 1',1,67,2,'2019-10-28',''
	exec up_Documentos_pesquisaGeralDocInternos  '', '','','mail@teste.pt','5440987', '','','','','','',''
	exec up_Documentos_pesquisaGeralDocInternos  '', '','','','', '','','','','','','','2020-01-17','2020-04-17'
	

	exec up_Documentos_pesquisaGeralDocInternos  '', '','','','', '','','','','','2020-07-20','00:00','2021-02-17','2023-08-22','','','''Cancelada_Cliente'',''Distribuicao_Pago'''


	exec up_Documentos_pesquisaGeralDocInternos  '', '','','','', '','','','','','','','2020-01-17','2020-04-17',1,'84F7E3A0-56D5-4208-A2FD-5A59FF1B561F'
	
	exec up_Documentos_pesquisaGeralDocInternos  '', '','','','', '','','','','','','','2020-01-17','2020-04-17',1,'A03E92DC-45F4-46A8-AF7C-151DC12B2B28'

	EXEC up_Documentos_pesquisaGeralDocInternos 223,0,N'21313',N'da@sapo.pt',
N'5440987',N'Ben-uh-ron',N'Loja 1',1,1,N'',N'2020-04-02',N'10:00:01',N'2020-04-02',N'2020-04-10',1
	

	
	*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_pesquisaGeralDocInternos]') IS NOT NULL
	DROP PROCEDURE [dbo].[up_Documentos_pesquisaGeralDocInternos] ;
GO


	

CREATE PROCEDURE [dbo].[up_Documentos_pesquisaGeralDocInternos]	


	 @number				VARCHAR(10)
	,@dep					VARCHAR(3)
	,@vatNr					VARCHAR(20)
	,@email					VARCHAR(50)
	,@ref					VARCHAR(18)
	,@design				VARCHAR(60)
	,@site					VARCHAR(60)
	,@status				VARCHAR(1)
	,@numdoc				VARCHAR(20)
	,@seriesNumber			VARCHAR(500)
	,@lastChangeDate		varchar(10) = '1900-01-01'
	,@lastChangeTime		VARCHAR(8)  = '00:00:00'
	,@dateInit				VARCHAR(10) = '1900-01-01'
	,@dateEnd				VARCHAR(10)  = '3000-01-01'
	,@pageNumber			int = 1
	,@orderStamp			VARCHAR(36) = ''
	,@deliveryStatusList    VARCHAR(1000) =''
	,@topMax                int = 100
	   
/* WITH ENCRYPTION */
AS

SET NOCOUNT ON


SET @ref				=   rtrim(ltrim(isnull(@ref,'')))
SET @number				=   rtrim(ltrim(isnull(@number,'')))
SET @dep				=   rtrim(ltrim(isnull(@dep,'')))
SET @vatNr				=   rtrim(ltrim(isnull(@vatNr,'')))
SET @design				=   rtrim(ltrim(isnull(@design,'')))
SET @status				=   rtrim(ltrim(isnull(@status,'')))
SET @numdoc				=   rtrim(ltrim(isnull(@numdoc,'')))
SET @seriesNumber		=   rtrim(ltrim(isnull(@seriesNumber,'')))
SET @site				=   rtrim(ltrim(isnull(@site,'')))
SET @email				=   rtrim(ltrim(isnull(@email,'')))
SET @lastChangeDate		=   rtrim(ltrim(isnull(@lastChangeDate,'1900-01-01')))
SET @lastChangeTime		=   rtrim(ltrim(isnull(@lastChangeTime,'00:00:00')))
SET @dateInit		    =   rtrim(ltrim(isnull(@dateInit,'1900-01-01')))
SET @dateEnd		    =   rtrim(ltrim(isnull(@dateEnd,'3000-01-01')))
SET @orderStamp         =	rtrim(ltrim(ISNULL(@orderStamp,'')))
SET @deliveryStatusList =	rtrim(ltrim(ISNULL(@deliveryStatusList,'')))
SET @topMax             =   isnull(@topMax,100)


if(isnull(@pageNumber,0)<1)
	set @pageNumber = 1

If OBJECT_ID('tempdb.dbo.#temp_documentos') IS NOT NULL
		drop table #temp_documentos;		
If OBJECT_ID('tempdb.dbo.#dadosBO') IS NOT NULL
		drop table #dadosBO;

DECLARE @sql varchar(max) = ''
DECLARE @sql1 varchar(max) = ''
DECLARE @sqlFinal varchar(max) = ''

DECLARE @orderBy VARCHAR(8000)
DECLARE @PageSize int = @topMax

if(@PageSize<0 or @PageSize>100)
	set @PageSize = 100

DECLARE @OrderCri VARCHAR(MAX) =''
Declare @site_nr int = 0

IF @orderStamp = ''
BEGIN 
	SET @OrderCri = 'ORDER BY dataobra ASC, usrhora ASC'
END 
ELSE 
BEGIN 
	SELECT @OrderCri='ORDER BY ' + instructionSql  FROM sortingTableOrder (NOLOCK) WHERE stamp = @orderStamp and inativo=0 and nameService='internalDoc'
END 


DECLARE @id_lt varchar(100) =''	
select top 1 @site_nr=isnull(no,0),@id_lt = rtrim(ltrim(id_lt)) from empresa(nolock) where site=@site 

DECLARE @downloadURL VARCHAR(100) = ''
SELECT  @downloadURL = rtrim(ltrim(textValue)) FROM B_Parameters(nolock) WHERE stamp='ADM0000000313'

DECLARE @imagemURL varchar(300) 
set   @imagemURL = @downloadURL + @id_lt


set @orderBy = ' ) X ' + @OrderCri +'  
				 OFFSET ' + convert(varchar(10),@PageSize) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
				 FETCH NEXT  ' + convert(varchar(10),@PageSize) + ' ROWS ONLY '

select @sql1 = N' SELECT * INTO #temp_Documentos
				FROM (
					SELECT
						 [tabela] = ''BO''
						, ndos AS seriesNumber 
						, [documento] = [Ts].[nmdos]
						, bdempresas
						, disp_webservice_view 
					FROM
						[dbo].[Ts](nolock)		 
				) as X 
	
				DECLARE @Documentos TABLE(
					seriesNumber int		
				)

				IF '''+@seriesNumber+''' <> ''''
					BEGIN
						INSERT INTO @Documentos
						SELECT * FROM dbo.up_splitToTable('''+@seriesNumber+''','','')
					END
				ELSE
				BEGIN
					INSERT INTO @Documentos
					SELECT seriesNumber FROM #temp_Documentos
				END 
				
				'

select @sql = 'SELECT '  + convert(varchar(10),@PageSize) +' as pageSize,  (COUNT(*) OVER() / ' +  convert(varchar(10),@PageSize) +') + 1 as pageTotal, '
				    + convert(varchar(10),@PageNumber) +' as pageNumber, COUNT(*) OVER() as linesTotal		
				,* 
				into #dadosBO
				from (
				SELECT 	
					DISTINCT
					Bo.dataobra
					,Bo.usrhora
					,Bo.bostamp
					,Bo.site
				FROM
       				[dbo].[BO](nolock) 
					LEFT JOIN [dbo].[Bo2](nolock)  ON [Bo].[bostamp]=[Bo2].[bo2stamp]
       				LEFT JOIN [dbo].[Bi] (nolock)  ON [Bo].[bostamp]=[Bi].[bostamp]
					INNER JOIN [#temp_Documentos] on [Bo].[ndos] = [#temp_Documentos].[seriesNumber] --AND tabela = ''BO''
					INNER JOIN @documentos ON [@documentos].[seriesNumber] = [Bo].[ndos]
				WHERE
       				disp_webservice_view = 1 '


if @dateInit != '' and @dateEnd  != ''
	select @sql = @sql + N' AND Bo.dataobra BETWEEN ''' + @dateInit + '''  AND  ''' + @dateEnd + ''''

if @lastChangeDate != ''
	select @sql = @sql + N' AND 	convert(datetime,convert(varchar(10),Bo.usrdata,120) + '' '' + Bo.usrhora) >= ''' + @lastChangeDate + ' ' + @lastChangeTime + ''''

if @design != ''		
	select @sql = @sql + N' AND ([Bi].[design] LIKE '''+@design+''' +''%'' )'

if @ref != ''		
	select @sql = @sql + N' AND ([Bi].[ref] LIKE ''' + @ref+'''+''%'')'

if @vatNr != ''		
	select @sql = @sql + N' AND [Bo].[ncont] = '''+ @vatNr +''' '


if len(rtrim(ltrim(@deliveryStatusList))) > 2
	select @sql = @sql + N' AND [Bo2].[status] in ( ' + @deliveryStatusList + ' ) '


IF len(@email)>0 
BEGIN		
	declare @no_	int
	declare @estab_ int

	IF len(@number)>0 and len(@dep)>0
		select @no_ = no, @estab_ = estab from b_utentes where email = @email and no=@number and estab=@dep
	ELSE
		select top 1 @no_ = no, @estab_ = estab from b_utentes where email = @email			
		
	select @sql = @sql + ' AND [Bo].[no]= ' + isnull(convert(varchar,@no_),0) + ' AND [Bo].[estab] = ' + isnull(convert(varchar,@estab_),0)	
END

if @number != ''		
	select @sql = @sql + N' AND [Bo].[no] = '+ @number +' '

if @dep != ''		
select @sql = @sql + N' AND [Bo].[estab] = '+ @dep +' '



if @status != '-1'   
	select @sql = @sql + N' AND [Bo].[fechada] = CASE WHEN '''+@status+''' = ''1'' THEN 0 
														WHEN '''+@status+''' = ''0'' then 1 
														ELSE [Bo].[fechada] END'

if @site != ''
	select @sql = @sql + N'	AND [Bo].[site] = '''+@site+''' '


if @numdoc != ''		
	select @sql = @sql + N' AND convert(varchar,[Bo].[obrano]) = CASE WHEN '''+convert(varchar,@numdoc)+''' = '''' THEN convert(varchar,[Bo].[obrano]) 
																	ELSE '''+convert(varchar,@numdoc)+''' END'
				
	
	set @sqlFinal = '
			SELECT 	
					DISTINCT
					pageSize
					,pageTotal
					,pageNumber
					,linesTotal
					,bo.bostamp as stamp
				    , Convert(VARCHAR(100), [Bo].[obrano]) AS IDInfo_number
					, Bo.[Site] AS IDInfo_site
					, Bo.ndos As IDInfo_seriesNumber
					, Bo.boano AS  IDInfo_year
					, Bo.no AS EntityInfo_number
					, Bo.estab AS EntityInfo_dep
					, UPPER(Bo.nome) AS EntityInfo_name				
					, (CASE WHEN #temp_Documentos.bdempresas = ''CL'' THEN ''Cliente'' 
							WHEN #temp_Documentos.bdempresas = ''FL'' THEN ''Fornecedor''
							WHEN #temp_Documentos.bdempresas = ''AG'' THEN ''Armazem''  END) AS EntityInfo_type
					, Bo.etotal AS FinanceInfo_retailPrice
					, Bo.moeda AS FinanceInfo_currency
					, 1 AS FinanceInfo_vatInc 
					, convert(varchar, Bo.ousrdata, 23) AS OperationInfo_CreationDate
					, Bo.ousrhora AS OperationInfo_CreationTime
					, convert(varchar, Bo.usrdata, 23) AS OperationInfo_lastChangeDate
					, Bo.usrinis    AS OperationInfo_lastChangeUser
					, Bo.usrhora AS OperationInfo_lastChangeTime
					, rtrim(ltrim(isnull(b_us.nome,'''') )) AS OperationInfo_lastChangeUserName 
					, (CASE WHEN bo.fechada = 0 THEN ''1'' ELSE ''0'' END) AS GeneralInfo_status
					, Bo2.status AS GeneralInfo_deliveryStatus			
					, convert(varchar, Bo.dataobra, 23) AS GeneralInfo_docDate
					, (SUM(isnull(dbo.unityConverter(qtt,''qttUnityToBox'', ' + convert(varchar(10),@site_nr) + ',bi.ref),0)) OVER(PARTITION BY [Bi].[bostamp])) AS StockInfo_qtt
					, Bo.nmdos AS DesignInfo_designation
					, Bo2.modo_envio AS ShippingInfo_mode
					, Bo2.localidade_ent AS ShippingInfo_city
					, Bo2.morada_ent AS ShippingInfo_address
					, Bo2.codpost_ent AS ShippingInfo_zipCode
					, Bo2.pais_ent AS ShippingInfo_countryCode
					, isnull((select sum(cast(round(Bi.ettdeb,2)  as numeric(15,2))) from   bi(nolock) where bostamp = BO.bostamp and ref=''SERVPORTES''),0.00)  AS ShippingInfo_shippingRetailPriceWithVat
					, isnull((select sum(cast(round(ettdeb/(((bi.iva/100) + 1)),2) as numeric(15,2))) from   bi(nolock) where bostamp = BO.bostamp and ref=''SERVPORTES''),0.00)  AS ShippingInfo_shippingRetailPriceWithoutVat
					, CONVERT(varchar,  Bo.u_dataentr , 23) AS ShippingInfo_deliveryDate				
					, CONVERT(VARCHAR(8),Bo.u_dataentr,108)  AS ShippingInfo_deliveryTime
					, Bo.logi1	AS ShippingInfo_sent
					, rtrim(ltrim(isnull(bo2.momento_pagamento,''''))) as FinanceInfo_paymentMoment
					, Bo2.pagamento AS FinanceInfo_paymentType	
					, ISNULL((SELECT TOP 1 ISNULL('''+ @imagemURL +''' + ''/''+ anexosstamp,'''') FROM anexos (nolock) WHERE filename like ''%.pdf'' and regstamp = bo.bostamp order by usrdata desc),'''') AS GeneralInfo_linkPdf	
					, BO.dataobra		
					, BO.usrhora
					, ISNULL(Bo2.nrReceita,'''') AS  generalInfo_prescriptionId
					, ISNULL(Bo2.pinAcesso,'''') AS  generalInfo_accessPin
					, ISNULL(Bo2.pinOpcao,'''') AS  generalInfo_optionPin
				FROM
       				[dbo].[BO](nolock) 
					INNER JOIN #dadosBO			   ON #dadosBO.bostamp  = Bo.bostamp and #dadosBO.site = Bo.site
					LEFT JOIN [dbo].[Bo2](nolock)  ON [Bo].[bostamp]=[Bo2].[bo2stamp]
       				LEFT JOIN [dbo].[Bi] (nolock)  ON [Bo].[bostamp]=[Bi].[bostamp]
					LEFT JOIN b_us (nolock)		   ON b_us.iniciais = bo.usrinis
					INNER JOIN [#temp_Documentos] on [Bo].[ndos] = [#temp_Documentos].[seriesNumber] --AND tabela = ''BO''
					INNER JOIN @documentos ON [@documentos].[seriesNumber] = [Bo].[ndos]
				WHERE
       				disp_webservice_view = 1 '


if @dateInit != '' and @dateEnd  != ''
	select @sql = @sql + N' AND Bo.dataobra BETWEEN ''' + @dateInit + '''  AND  ''' + @dateEnd + ''''

if @lastChangeDate != ''
	select @sql = @sql + N' AND 	convert(datetime,convert(varchar(10),Bo.usrdata,120) + '' '' + Bo.usrhora) >= ''' + @lastChangeDate + ' ' + @lastChangeTime + ''''

if @design != ''		
	select @sql = @sql + N' AND ([Bi].[design] LIKE '''+@design+''' +''%'' )'

if @ref != ''		
	select @sql = @sql + N' AND ([Bi].[ref] LIKE ''' + @ref+'''+''%'')'

if @vatNr != ''		
	select @sql = @sql + N' AND [Bo].[ncont] = '''+ @vatNr +''' '


if len(rtrim(ltrim(@deliveryStatusList))) > 2
	select @sql = @sql + N' AND [Bo2].[status] in ( ' + @deliveryStatusList + ' ) '

IF len(@email)>0 
BEGIN		

	IF len(@number)>0 and len(@dep)>0
		select @no_ = no, @estab_ = estab from b_utentes where email = @email and no=@number and estab=@dep
	ELSE
		select top 1 @no_ = no, @estab_ = estab from b_utentes where email = @email			
		
	select @sql = @sql + ' AND [Bo].[no]= ' + isnull(convert(varchar,@no_),0) + ' AND [Bo].[estab] = ' + isnull(convert(varchar,@estab_),0)	
END

if @number != ''		
	select @sql = @sql + N' AND [Bo].[no] = '+ @number +' '

if @dep != ''		
select @sql = @sql + N' AND [Bo].[estab] = '+ @dep +' '



if @status != '-1'   
	select @sql = @sql + N' AND [Bo].[fechada] = CASE WHEN '''+@status+''' = ''1'' THEN 0 
														WHEN '''+@status+''' = ''0'' then 1 
														ELSE [Bo].[fechada] END'

if @site != ''
	select @sql = @sql + N'	AND [Bo].[site] = '''+@site+''' '


if @numdoc != ''		
	select @sql = @sql + N' AND convert(varchar,[Bo].[obrano]) = CASE WHEN '''+convert(varchar,@numdoc)+''' = '''' THEN convert(varchar,[Bo].[obrano]) 
																	ELSE '''+convert(varchar,@numdoc)+''' END
	' 



select @sql = @sql + @orderBy + @sqlFinal 

print @sql1 + @sql 	+ @OrderCri

EXECUTE (@sql1 + @sql +@OrderCri )


If OBJECT_ID('tempdb.dbo.#temp_documentos') IS NOT NULL
		drop table #temp_documentos ;
If OBJECT_ID('tempdb.dbo.#dadosBO') IS NOT NULL
		drop table #dadosBO;
GO
GRANT EXECUTE on dbo.up_Documentos_pesquisaGeralDocInternos TO PUBLIC
GRANT Control on dbo.up_Documentos_pesquisaGeralDocInternos TO PUBLIC
GO

