/* SP Pesquisa Detalhe Documentos (HEAD)
J.Gomes, 2020-04-23
--------------------------------

Listagem detalhe de Documentos internos baseada em diversos campos.
e com possibilidade de filtragem acima de determinada data e hora de alteração.

Pesquisa nas tabelas: 	
Bo, Bo2,Bi, ts

por 4 campos:
	'@number', '@seriesNumber', '@site', '@year'
	
Campos obrigatórios: todos

OUTPUT:
---------
IDInfo_number, IDInfo_site, IDInfo_seriesNumber,  IDInfo_extCode, IDInfo_stamp
, EntityInfo_number, EntityInfo_dep, EntityInfo_name
, EntityInfo_type, FinanceInfo_retailPrice,	FinanceInfo_totalPcl, FinanceInfo_totalpcp
, FinanceInfo_currency, FinanceInfo_commercialDiscount, FinanceInfo_vatValue
, FinanceInfo_discountValue, FinanceInfo_paymenttype, FinanceInfo_vatInc
, OperationInfo_CreationDate, OperationInfo_CreationTime, OperationInfo_lastChangeDate
, OperationInfo_lastChangeTime,	GeneralInfo_status,	GeneralInfo_deliveryStatus
, GeneralInfo_docDate, StockInfo_qtt, StockInfo_qttref, DesignInfo_designation
, ShippingInfo_mode, ShippingInfo_city, ShippingInfo_address, ShippingInfo_zipCode
, ShippingInfo_countryCode, ShippingInfo_deliveryDate, ShippingInfo_deliveryTime

EXECUÇÃO DA SP:
---------------
exec up_Documentos_pesquisaDetalheDocFornecedor '@number', '@seriesNumber', '@site', '@year'

	exec up_Documentos_pesquisaDetalheDocFornecedor  'asdfasdasdf', '55','Loja 1','2021'




	fo
	*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_pesquisaDetalheDocFornecedor]') IS NOT NULL
	DROP PROCEDURE [dbo].up_Documentos_pesquisaDetalheDocFornecedor ;
GO

CREATE PROCEDURE [dbo].[up_Documentos_pesquisaDetalheDocFornecedor]	
	 @numdoc			VARCHAR(20)
	,@seriesNumber	    VARCHAR(500)
	,@site				VARCHAR(60)	
	,@year				VARCHAR(4) 
	
	   
/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

SET @numdoc				=   rtrim(ltrim(isnull(@numdoc,'')))
SET @seriesNumber		=   rtrim(ltrim(isnull(@seriesNumber,'')))
SET @site				=   rtrim(ltrim(isnull(@site,'')))
SET @year				=   rtrim(ltrim(isnull(@year,'')))


DECLARE @id_lt varchar(100) =''	
DECLARE @site_nr INT
select @id_lt = rtrim(ltrim(id_lt)), @site_nr= no from empresa(nolock) where site = @site


DECLARE @downloadURL VARCHAR(100) = ''
SELECT  @downloadURL = rtrim(ltrim(textValue)) FROM B_Parameters(nolock) WHERE stamp='ADM0000000313'


DECLARE @imagemURL varchar(300) 
set   @imagemURL = @downloadURL + @id_lt





IF len(@numdoc) > 0 and len(@seriesNumber) > 0  and len(@year) > 0
BEGIN
	If OBJECT_ID('tempdb.dbo.#temp_documentos') IS NOT NULL
			drop table #temp_documentos;		

	DECLARE @sql varchar(max) = ''
	DECLARE @sql1 varchar(max) = ''
	DECLARE @sqlWhere varchar(max) = ''
	DECLARE @sqlFrom varchar(max) = ''

	select @sql = N' SELECT * INTO #temp_Documentos
					FROM (
							SELECT
								 [tabela] = ''FL''
								, cm AS seriesNumber 
								, [documento] = [cm1].[cmdesc]
								, disp_webservice_view 
							FROM
								[dbo].[cm1](nolock)		 
					) as X 

					DECLARE @Documentos TABLE(
						seriesNumber int		
					)

					IF '''+@seriesNumber+''' <> ''''
						BEGIN
							INSERT INTO @Documentos
							SELECT * FROM dbo.up_splitToTable('''+@seriesNumber+''','','')
						END
					ELSE
					BEGIN
						INSERT INTO @Documentos
						SELECT seriesNumber FROM #temp_Documentos
					END



					SELECT distinct	
						 Convert(VARCHAR(100), [fo].[adoc]) AS IDInfo_id
						, fo.Site AS IDInfo_site
						, fo.docCode As IDInfo_seriesNumber
						, fo.fostamp As IDInfo_stamp
						, fo.foano as IdInfo_year
						, fo.no AS EntityInfo_number
						, fo.estab AS EntityInfo_dep
						, UPPER(fo.nome) AS EntityInfo_name
						, fo.ncont AS EntityInfo_vatNr				
						, ''Fornecedor'' AS EntityInfo_type
						, fo.etotal AS FinanceInfo_retailPrice				
						, (SUM(isnull(fn.u_upc,0)*isnull(fn.qtt,0)) OVER(PARTITION BY [fn].[fnstamp])) AS FinanceInfo_totalPcl 
						, (SUM(isnull(fn.epv,0)*isnull(fn.qtt,0)) OVER(PARTITION BY [fn].[fnstamp])) AS FinanceInfo_totalPct					
						, fo.moeda AS FinanceInfo_currency
						, fo.edescc AS FinanceInfo_commercialDiscount
						, fo.EFINV AS FinanceInfo_financialDiscount	
						, fo.ettiva AS FinanceInfo_vatValue												
						, 1 AS FinanceInfo_vatInc 
						, convert(varchar, fo.ousrdata, 23) AS OperationInfo_CreationDate
						, fo.ousrhora AS OperationInfo_CreationTime
						, convert(varchar, fo.usrdata, 23) AS OperationInfo_lastChangeDate
						, fo.usrhora AS OperationInfo_lastChangeTime
						, fo.usrinis     AS OperationInfo_lastChangeUser
						, rtrim(ltrim(isnull((select top 1 isnull(nome,'''') from b_us(nolock) where iniciais=fo.usrinis),''''))) AS OperationInfo_lastChangeUserName 
						, (CASE WHEN isnull(fo.u_status,'''') = ''A'' THEN ''1'' ELSE ''0'' END) AS GeneralInfo_status		
						, convert(varchar, fo.data, 23) AS GeneralInfo_docDate
						, SUM(dbo.unityConverter([fn].[qtt],''qttUnityToBox'', ' + convert(varchar(10),@site_nr) + ',fn.ref)) OVER(PARTITION BY [fn].[fostamp]) AS StockInfo_qtt
						, (select dbo.ListaRefDistintasFn([fn].[fostamp])) AS StockInfo_qttRef
						, fo.docNome AS DesignInfo_designation 
						'
		select @sql1 =	'
						, '''' AS  GeneralInfo_obs 
						, ltrim(rtrim(fo.morada)) as AddressInfo_address	
						, ltrim(rtrim(fo.codpost)) as AddressInfo_zipCode	
						, ltrim(rtrim(fo.local)) as AddressInfo_city 
						, ISNULL((SELECT TOP 1 ISNULL('''+ @imagemURL +''' + ''/''+ anexosstamp,'''') FROM anexos (nolock) WHERE filename like ''%.pdf'' and regstamp = fo.fostamp order by usrdata desc),'''') AS GeneralInfo_linkPdf	
						, FO.data		
						, FO.usrhora
						,'''+RTRIM(LTRIM(@id_lt))+'''     as IDInfo_siteId
						'
	
			set @sqlFrom='FROM
       					    [dbo].[FO](nolock) 
							LEFT JOIN [dbo].[fo2](nolock)  ON [fo].[fostamp]=[fo2].[fo2stamp]
       						LEFT JOIN [dbo].[fn] (nolock)  ON [fo].[fostamp]=[fn].[fostamp]
							INNER JOIN @documentos		   ON [@documentos].[seriesNumber] = [fo].[docCode]
							INNER JOIN #temp_Documentos    ON [#temp_Documentos].[seriesNumber] = [fo].[docCode]
					WHERE
       					disp_webservice_view = 1  '


	



	if @numdoc != ''		
		select @sqlWhere = @sqlWhere + N' AND convert(varchar,[fo].[adoc]) = '''+convert(varchar,@numdoc)+''''
	if @year != ''
		select @sqlWhere = @sqlWhere + N'	AND [fo].[foano] = '''+@year+''' '

																	
	select @sqlWhere = @sqlWhere + N'
		ORDER BY GeneralInfo_docDate DESC, OperationInfo_CreationDate DESC,OperationInfo_CreationTime DESC;'
	 


	print @sql + @sql1 + @sqlFrom + @sqlWhere
	EXECUTE (@sql + @sql1 + @sqlFrom + @sqlWhere)

	If OBJECT_ID('tempdb.dbo.#temp_documentos') IS NOT NULL
			drop table #temp_documentos ;

END
ELSE
	BEGIN
		print('Missing numdoc, seriesNumber, site or year')
	END


GO
GRANT EXECUTE on dbo.up_Documentos_pesquisaDetalheDocFornecedor TO PUBLIC
GRANT Control on dbo.up_Documentos_pesquisaDetalheDocFornecedor TO PUBLIC
GO




