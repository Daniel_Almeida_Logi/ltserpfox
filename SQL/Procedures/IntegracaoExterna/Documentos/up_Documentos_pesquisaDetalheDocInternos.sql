/* SP Pesquisa Detalhe Documentos (HEAD)
J.Gomes, 2020-04-23
--------------------------------

Listagem detalhe de Documentos internos baseada em diversos campos.
e com possibilidade de filtragem acima de determinada data e hora de alteração.

Pesquisa nas tabelas: 	
Bo, Bo2,Bi, ts

por 4 campos:
	'@number', '@seriesNumber', '@site', '@year'
	
Campos obrigatórios: todos

OUTPUT:
---------
IDInfo_number, IDInfo_site, IDInfo_seriesNumber,  IDInfo_extCode, IDInfo_stamp
, EntityInfo_number, EntityInfo_dep, EntityInfo_name
, EntityInfo_type, FinanceInfo_retailPrice,	FinanceInfo_totalPcl, FinanceInfo_totalpcp
, FinanceInfo_currency, FinanceInfo_commercialDiscount, FinanceInfo_vatValue
, FinanceInfo_discountValue, FinanceInfo_paymenttype, FinanceInfo_vatInc
, OperationInfo_CreationDate, OperationInfo_CreationTime, OperationInfo_lastChangeDate
, OperationInfo_lastChangeTime,	GeneralInfo_status,	GeneralInfo_deliveryStatus
, GeneralInfo_docDate, StockInfo_qtt, StockInfo_qttref, DesignInfo_designation
, ShippingInfo_mode, ShippingInfo_city, ShippingInfo_address, ShippingInfo_zipCode
, ShippingInfo_countryCode, ShippingInfo_deliveryDate, ShippingInfo_deliveryTime

EXECUÇÃO DA SP:
---------------
exec up_Documentos_pesquisaDetalheDocInternos '@number', '@seriesNumber', '@site', '@year'

	exec up_Documentos_pesquisaDetalheDocInternos  '378', '41','Loja 1','2020'

	exec up_Documentos_pesquisaDetalheDocInternos  '13', '2','','2019'
	exec up_Documentos_pesquisaDetalheDocInternos  '13', '','Loja 1','2019'
	exec up_Documentos_pesquisaDetalheDocInternos  '', '','',''
	exec up_Documentos_pesquisaDetalheDocInternos  '2', '','Loja 1','2020'
	exec up_Documentos_pesquisaDetalheDocInternos  '2922', '35','Loja 3','2022'





	*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_pesquisaDetalheDocInternos]') IS NOT NULL
	DROP PROCEDURE [dbo].[up_Documentos_pesquisaDetalheDocInternos] ;
GO

CREATE PROCEDURE [dbo].[up_Documentos_pesquisaDetalheDocInternos]	
	 @numdoc			VARCHAR(10)
	,@seriesNumber	    VARCHAR(500)
	,@site				VARCHAR(60)	
	,@year				VARCHAR(4) 
	
	   
/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

SET @numdoc				=   rtrim(ltrim(isnull(@numdoc,'')))
SET @seriesNumber		=   rtrim(ltrim(isnull(@seriesNumber,'')))
SET @site				=   rtrim(ltrim(isnull(@site,'')))
SET @year				=   rtrim(ltrim(isnull(@year,'')))


DECLARE @id_lt varchar(100) =''	
DECLARE @site_nr INT
select @id_lt = rtrim(ltrim(id_lt)), @site_nr= no from empresa(nolock) where site = @site


DECLARE @downloadURL VARCHAR(100) = ''
SELECT  @downloadURL = rtrim(ltrim(textValue)) FROM B_Parameters(nolock) WHERE stamp='ADM0000000313'


DECLARE @imagemURL varchar(300) 
set   @imagemURL = @downloadURL + @id_lt





IF len(@numdoc) > 0 and len(@seriesNumber) > 0  and len(@year) > 0
BEGIN
	If OBJECT_ID('tempdb.dbo.#temp_documentos') IS NOT NULL
			drop table #temp_documentos;		

	DECLARE @sql varchar(max) = ''
	DECLARE @sql1 varchar(max) = ''
	DECLARE @sqlWhere varchar(max) = ''
	DECLARE @sqlFrom varchar(max) = ''

	select @sql = N' SELECT * INTO #temp_Documentos
					FROM (
						SELECT
							 [tabela] = ''BO''
							, ndos AS seriesNumber 
							, [documento] = [Ts].[nmdos]
							, bdempresas
							, disp_webservice_view 
						FROM
							[dbo].[Ts](nolock)		 
					) as X 

					DECLARE @Documentos TABLE(
						seriesNumber int		
					)

					IF '''+@seriesNumber+''' <> ''''
						BEGIN
							INSERT INTO @Documentos
							SELECT * FROM dbo.up_splitToTable('''+@seriesNumber+''','','')
						END
					ELSE
					BEGIN
						INSERT INTO @Documentos
						SELECT seriesNumber FROM #temp_Documentos
					END
					SELECT distinct	top 1000		
						 Convert(VARCHAR(100), [Bo].[obrano]) AS IDInfo_number
						, [Site] AS IDInfo_site
						, Bo.ndos As IDInfo_seriesNumber
						, Bo2.codext AS IDInfo_extCode
						, Bo.bostamp As IDInfo_stamp
						, Bo.boano as IdInfo_year
						, Bo.no AS EntityInfo_number
						, Bo.estab AS EntityInfo_dep
						, UPPER(Bo.nome) AS EntityInfo_name
						, Bo.ncont AS EntityInfo_vatNr	
						, Bo2.email AS EntityInfo_email
						, Bo2.contacto AS EntityInfo_mobilePhone		
						, Bo2.telefone AS EntityInfo_telephone					
						, (CASE WHEN #temp_Documentos.bdempresas = ''CL'' THEN ''Cliente'' 
								WHEN #temp_Documentos.bdempresas = ''FL'' THEN ''Fornecedor''
								WHEN #temp_Documentos.bdempresas = ''AG'' THEN ''Armazem''  END) AS EntityInfo_type
						, Bo.etotal AS FinanceInfo_retailPrice					

						, (SUM(isnull(bi.u_upc,0)*isnull(bi.qtt,0)) OVER(PARTITION BY [Bi].[bostamp])) AS FinanceInfo_totalPcl 
						, (SUM(isnull(bi.epcusto,0)*isnull(bi.qtt,0)) OVER(PARTITION BY [Bi].[bostamp])) AS FinanceInfo_totalPct					
						, Bo.moeda AS FinanceInfo_currency
						, Bo.edescc AS FinanceInfo_commercialDiscount
						, Bo2.etotiva AS FinanceInfo_vatValue										
						, 0 AS FinanceInfo_financialDiscount
						, Bo2.pagamento AS FinanceInfo_paymentType					
						, 1 AS FinanceInfo_vatInc 
						, convert(varchar, Bo.ousrdata, 23) AS OperationInfo_CreationDate
						, Bo.ousrhora AS OperationInfo_CreationTime
						, convert(varchar, Bo.usrdata, 23) AS OperationInfo_lastChangeDate
						, Bo.usrhora AS OperationInfo_lastChangeTime
						, Bo.usrinis     AS OperationInfo_lastChangeUser
						, rtrim(ltrim(isnull((select top 1 isnull(nome,'''') from b_us(nolock) where iniciais=Bo.usrinis),''''))) AS OperationInfo_lastChangeUserName 
						, (CASE WHEN bo.fechada = 0 THEN ''1'' ELSE ''0'' END) AS GeneralInfo_status
						, Bo2.status AS GeneralInfo_deliveryStatus			
						, convert(varchar, Bo.dataobra, 23) AS GeneralInfo_docDate
						, SUM(dbo.unityConverter([Bi].[qtt],''qttUnityToBox'', ' + convert(varchar(10),@site_nr) + ',bi.ref)) OVER(PARTITION BY [Bi].[bostamp]) AS StockInfo_qtt
						, (select dbo.ListaRefDistintasBi([Bi].[bostamp])) AS StockInfo_qttRef
						, Bo.nmdos AS DesignInfo_designation						
						, Bo2.modo_envio AS ShippingInfo_mode
						, Bo2.localidade_ent AS ShippingInfo_city
						, Bo2.morada_ent AS ShippingInfo_address
						, Bo2.codpost_ent AS ShippingInfo_zipCode
						, Bo2.pais_ent AS ShippingInfo_countryCode
						, isnull((select sum(cast(round(Bi.ettdeb,2)  as numeric(15,2))) from   bi(nolock) where bostamp = BO.bostamp and ref=''SERVPORTES''),0.00)  AS ShippingInfo_shippingRetailPriceWithVat
						, isnull((select sum(cast(round(ettdeb/(((bi.iva/100) + 1)),2) as numeric(15,2))) from   bi(nolock) where bostamp = BO.bostamp and ref=''SERVPORTES''),0.00)  AS ShippingInfo_shippingRetailPriceWithoutVat
						, CONVERT(varchar,  Bo.u_dataentr , 23) AS ShippingInfo_deliveryDate				
						, CONVERT(VARCHAR(8),Bo.u_dataentr,108)  AS ShippingInfo_deliveryTime '
		select @sql1 =	', Bo.logi1	AS ShippingInfo_sent
						, bo.obs AS  GeneralInfo_obs 
						, Bo2.nrReceita	AS  GeneralInfo_prescriptionId	
						, Bo2.pinAcesso	AS  GeneralInfo_accessPin	
						, Bo2.pinOpcao	AS  GeneralInfo_optionPin
						, rtrim(ltrim(isnull(Bo2.paymentUrl,'''')))	AS  GeneralInfo_linkToPayment
						, rtrim(ltrim(isnull(Bo2.paymentEntity,'''')))	AS  GeneralInfo_paymentEntity
						, rtrim(ltrim(isnull(Bo2.paymentReference,'''')))	AS  GeneralInfo_paymentReference
						, ROUND(isnull(bo2.paymentValue,0),2) AS  GeneralInfo_paymentValue 
						, ltrim(rtrim(bo.morada)) as AddressInfo_address	
						, ltrim(rtrim(bo.codpost)) as AddressInfo_zipCode	
						, ltrim(rtrim(bo.local)) as AddressInfo_city 
						, ISNULL((SELECT TOP 1 ISNULL('''+ @imagemURL +''' + ''/''+ anexosstamp,'''') FROM anexos (nolock) WHERE filename like ''%.pdf'' and regstamp = bo.bostamp order by usrdata desc),'''') AS GeneralInfo_linkPdf	
						, rtrim(ltrim(isnull(bo2.momento_pagamento,''''))) as FinanceInfo_paymentMoment
						, case when rtrim(ltrim(isnull(bo2.iddistribuidor,''''))) = '''' then ''0'' else  bo2.iddistribuidor end as ShippingInfo_operatorId
						, isnull((select top 1 isnull(distribuidor,'''') from CodigoPostal_Distribuidor(nolock) where iddistribuidor=bo2.iddistribuidor),'''') as ShippingInfo_operatorDesign 
						,''' +  rtrim(ltrim(@id_lt)) + '''  as IDInfo_siteId
						'
	
			set @sqlFrom='
						FROM
       					[dbo].[BO](nolock) 
						LEFT JOIN [dbo].[Bo2](nolock)  ON [Bo].[bostamp]=[Bo2].[bo2stamp]
       					LEFT JOIN [dbo].[Bi](nolock)  ON [Bo].[bostamp]=[Bi].[bostamp]
						INNER JOIN [#temp_Documentos] on [Bo].[nmdos] = [#temp_Documentos].[documento] 
						INNER JOIN @documentos ON [@documentos].[seriesNumber] = [Bo].[ndos]
					WHERE
       					disp_webservice_view = 1  '


	



	if @numdoc != ''		
		select @sqlWhere = @sqlWhere + N' AND [Bo].[obrano] = ''' + convert(varchar,@numdoc) +''''
	if @year != ''
		select @sqlWhere = @sqlWhere + N'	AND [Bo].[boano] = '''+@year+''' '

																	
	select @sqlWhere = @sqlWhere + N'
		ORDER BY GeneralInfo_docDate DESC, OperationInfo_CreationDate DESC,OperationInfo_CreationTime DESC;'
	 


	print @sql + @sqlFrom + @sqlWhere
	EXECUTE (@sql + @sql1 + @sqlFrom + @sqlWhere)

	If OBJECT_ID('tempdb.dbo.#temp_documentos') IS NOT NULL
			drop table #temp_documentos ;

END
ELSE
	BEGIN
		print('Missing numdoc, seriesNumber, site or year')
	END


GO
GRANT EXECUTE on dbo.up_Documentos_pesquisaDetalheDocInternos TO PUBLIC
GRANT Control on dbo.up_Documentos_pesquisaDetalheDocInternos TO PUBLIC
GO




