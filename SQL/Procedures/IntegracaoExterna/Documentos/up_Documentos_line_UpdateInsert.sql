/*
DOCUMENTOS UPDATE/INSERT
D.Almeida, 04-05-2020
revisão 04-05-2020
--------------------------------
bo
Pesquisa do registo na tabela BI a partir do stamp
	 stamp (@stamp)

Cria sempre linhas novas.


StatMessag: 
	1 - OK
	0 - ERROR



ex. no INSERT:
DateMessag				StatMessag				
2020-04-04 20:06:19.207	1								

ex. no UPDATE:
DateMessag				StatMessag					
2020-04-04 20:04:52.017	1										



-------------------------------------------------------------------------

PROCEDURE
Criada procedure up_Documentos_line_UpdateInsert com parâmetros 

Parametros: 

exec up_Documentos_line_UpdateInsert 'ncont','bino','no_ext',no,estab
, 'nome','email', 'nascimento' (yyyymmdd), 'sexo', 'tipo', altura, peso, 'profi', 'obs', noAssociado, 'id' 
, 'nrcartao', 'nbenef', 'nrss'
, 'tlmvl', 'telefone', 'fax', 'local', 'morada', 'codpost', 'zona', 'codigop'
, nocredit (0/1), naoencomenda(0/1), desconto, 'moeda', 'nib', 'codSwift', cativa(0/1), perccativa, eplafond
, autoriza_sms (0/1), autoriza_emails (0/1), inactivo (0/1)


EXECUÇÃO DA SP:
---------------
EXEC up_Documentos_line_UpdateInsert '7e3de6102a9e412a88d571fad',N'Loja 1',
41,43,2020,N'5440987',10,1,3,20,N'5.0,13.0,23.0',5,N'teste',13,18.83,1.17

exec up_Documentos_line_UpdateInsert 
 3, 41, 'Loja 1', 2020, NULL,
 NULL, NULL, NULL, NULL, 
 NULL, NULL, NULL, NULL, NULL, NULL ,
 NULL, NULL, NULL,
 NULL, NULL, NULL, NULL, NULL, 'xpto'


 select *from bi(nolock) where bostamp='4a721905539c49be91dc6fd4d'
  select *from bi(nolock) where bostamp='4a721905539c49be91dc6fd4d'

VERIFICAÇÃO:
------------
select usrdata,usrhora,* from bo(nolock) where obrano = 3 and boano=2020 and ndos=41 and site='Loja 1'



*/



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_Documentos_line_UpdateInsert]') IS NOT NULL
    drop procedure up_Documentos_line_UpdateInsert
go




CREATE PROCEDURE up_Documentos_line_UpdateInsert	
	

	--identificadores do documento
	 @IdInfo_stamp			    varchar(30), /*bostamp*/
	 @IDInfo_site			    varchar(10), /*site*/	
	 @IDInfo_seriesNumber       int, /*ndos*/
	 @IDInfo_number             int, /*obrano*/
	 @IDInfo_year               int, /*boano*/
	

	-- producto
	 @ProductInfo_ref					varchar(19), /*no*/


	 -- stock
	 @StockInfo_qtt			             int,
	 @StockInfo_warehouseSource			 int, 
	 @StockInfo_warehouseDest			 int,

	 -- general 
	 @FinanceInfo_unitRetailPrice		 numeric(19,6),
	 @FinanceInfo_retailPrice			 numeric(19,6),
	 @FinanceInfo_discountsCommercPerc	 varchar(100), 
	 @FinanceInfo_discountValue		     numeric(19,6), 
	 @FinanceInfo_voucherNr              varchar(30),
	 @FinanceInfo_campaignId			 varchar(100) = '',


	  -- general 
	 @GeneralInfo_obs			         varchar(200),


	 -- Iva 
	 @VateRatesInfo_rate			     numeric(19,6),
	 @VateRatesInfo_incidenceBase	     numeric(19,6),
	 @VateRatesInfo_value			     numeric(19,6),


	 @FinanceInfo_cardMoney			     numeric(15,2) = 0
 
	
AS



DECLARE @contador			int
DECLARE @CountryName		varchar(50)
DECLARE @lastChangeDate		datetime
DECLARE @lastChangeTime		VARCHAR(8)
DECLARE @CreationDate		datetime
DECLARE @CreationTime		VARCHAR(8)

DECLARE @design				varchar(99)
DECLARE @stock_act			int = 0
DECLARE @codigoIva          int = 0
DECLARE @cpoc               int
DECLARE @closed	            bit = 0
--variaveis para apresentar no final

DECLARE @priceUni		    numeric(19,6)
DECLARE @priceCost		    numeric(19,6)


DECLARE @eppond             numeric(19,6)
DECLARE @epcult             numeric(19,6)
DECLARE @epcost			    numeric(19,6)
DECLARE @u_epv1act          numeric(19,6)
DECLARE @epv2               numeric(19,6)
DECLARE @epv3               numeric(19,6)
DECLARE @epv4               numeric(19,6)
DECLARE @epv5               numeric(19,6)
DECLARE @epv6               numeric(19,6)

DECLARE @qPrice             int = 0
DECLARE @qPriceCost         int = 0
DECLARE @dateDoc            datetime





declare @difhour int = 0


SET @lastChangeDate             = convert(varchar,getdate(),23)
SET @lastChangeTime             = convert(varchar(8),getdate(),24)
SET @CreationDate               = convert(varchar,getdate(),23)
SET @CreationTime               = convert(varchar(8),getdate(),24)
set @FinanceInfo_cardMoney      = isnull(@FinanceInfo_cardMoney,0.00)


DECLARE @docSeriesName		varchar(24)
DECLARE @family             varchar(18)

DECLARE @entityName         varchar(80)
DECLARE @entityNumber       int
DECLARE @entityDep          int
DECLARE @entityCity         varchar(45)
DECLARE @entityAddress      varchar(50)
DECLARE @entityZip          varchar(55)
DECLARE @finalDate          date
DECLARE @discountPerc       numeric(10,2) = 0
DECLARE @qttMinEmb          int = 1


set @GeneralInfo_obs = left(ltrim(rtrim(isnull(@GeneralInfo_obs,''))),200)


set @ProductInfo_ref = rtrim(ltrim(@ProductInfo_ref))
 
set  @FinanceInfo_discountValue = isnull(@FinanceInfo_discountValue,0)

set @FinanceInfo_voucherNr = left(rtrim(ltrim(isnull(@FinanceInfo_voucherNr,''))),30)


if(isnull(@FinanceInfo_discountsCommercPerc,'')!='')
	(Select top 1 @discountPerc = convert(numeric(10,2),isnull(items,0)) from dbo.up_splitToTable(@FinanceInfo_discountsCommercPerc,','))


if(@discountPerc=0 and  @FinanceInfo_discountValue>0) --calcular a precentagem de desconto se não estiver preenchida
begin
	set @discountPerc = round(100-((round(@FinanceInfo_retailPrice,2)/round((@FinanceInfo_unitRetailPrice*@StockInfo_qtt),2)*100)),2)
end

-- O campo site é items 
IF (len(isnull(@IdInfo_stamp,''))>0)
		
BEGIN	


	
	DECLARE @stampid CHAR(25)  
    SET @stampid = (select left(newid(),25))

	DECLARE @siteno AS tinyint
	SET @siteno = (select  no from empresa(nolock) where site = @IDInfo_site)	

	select @difhour = (select isnull(numValue,0) from B_Parameters_site(nolock) where stamp='ADM0000000008' and site=@IDInfo_site)

	
	select 
		@docSeriesName = ltrim(rtrim(isnull(nmdos,''))),
		@qPriceCost = isnull(QPRECOCUSTO,0), 
		@qPrice = isnull(QPRECO,0)
	from ts (nolock)
	where ndos = isnull(@IDInfo_seriesNumber,0)

	DECLARE @clientName         varchar(60)
	DECLARE @clientNumber       int = 0
	DECLARE @clientDep          int = 0


	DECLARE @operator		    VARCHAR(8) = 'ONL'

	select 
		top 1 
			@design = ltrim(rtrim(isnull(design,''))),
			@stock_act = case when st.stns=1 then 0 else st.stock end,
			@cpoc = isnull(st.cpoc,0),
			@family = left(ltrim(rtrim(isnull(st.familia,''))),18),
			@epcost = round(isnull(st.EPCUSTO,0),2),
			@u_epv1act  = round(isnull(st.epv1,0),2),
			@eppond =  round(isnull(st.EPCPOND,0),2),
			@epcult =  round(isnull(st.epcult,0),2),	
			@qttMinEmb = isnull(st.qttembal,1)					
	 from st(nolock)
	 where st.ref=@ProductInfo_ref and st.site_nr = @siteno


	select  top 1  @codigoIva = isnull(codigo,0) from taxasiva (nolock) where taxa = @VateRatesInfo_rate	

	select top 1
	   @clientName = left(ltrim(rtrim(isnull(bo.nome,''))),80),
	   @entityNumber = bo.no,
	   @entityDep = bo.estab,
	   @entityCity = left(ltrim(rtrim(isnull(bo.local,''))),45),
	   @entityAddress = left(ltrim(rtrim(isnull(bo.morada,''))),50),
	   @entityZip = left(ltrim(rtrim(isnull(bo.codpost,''))),55),
	   @dateDoc = isnull(bo.dataobra,'19000101'),
	   @finalDate = isnull(bo.datafinal,'19000101'),
	   @closed = isnull(fechada,1)
	 from bo(nolock)  where bostamp= @IdInfo_stamp and boano = @IDInfo_year


	 if(@qttMinEmb=0)
		set @qttMinEmb=1


	IF(@qPriceCost=0)
	BEGIN
		set @priceCost = @epcost
	END


	IF(@qPriceCost=1)
	BEGIN
	  set @priceCost = 	 @eppond
	END


	IF(@qPriceCost=2)
	BEGIN
		set @priceCost = @epcult	  
	END


	IF(@qPriceCost=3)
	BEGIN
		set @priceCost = @epcost	 
	END

	

	IF(@qPrice=0)
	BEGIN
	  set @priceUni  = 	 0
	END

	-- 1 - Preço de Venda
	IF(@qPrice=1)
	BEGIN
	  set @priceUni  = 	 @u_epv1act
	END

	--  2 - Preço de Custo
	IF(@qPrice=2)
	BEGIN
	  set @priceUni  = 	 @epcost
	END

	-- 3 - Preço Médio de Custo
	IF(@qPrice=3)
	BEGIN
	  set @priceUni  = 	 @eppond
	END

	-- ** 4 - Zero
	IF(@qPrice=4)
	BEGIN
	  set @priceUni  = 	 0
	END


	-- ** 5 - Preço de Venda 2
	IF(@qPrice=5)
	BEGIN
	  set @priceUni  = 	 0
	END


	-- ** 6 - Preço de Venda 3
	IF(@qPrice=6)
	BEGIN
	  set @priceUni  = 	 0
	END

	-- ** 7 - Preço de Venda 4
	IF(@qPrice=7)
	BEGIN
	  set @priceUni  = 	 0
	END


	-- ** 8 - Preço de Venda 5
	IF(@qPrice=8)
	BEGIN
	  set @priceUni  = 	 0
	END

	-- ** 9 - Último Preço de Custo
	IF(@qPrice=9)
	BEGIN
	  set @priceUni  = 	 @epcult
	END



	set @StockInfo_qtt = dbo.unityConverter(@StockInfo_qtt,'qttBoxToUnity', @siteno ,@ProductInfo_ref)

	
	declare @FinanceInfo_unitPrice numeric(19,6) = 0
	set @FinanceInfo_unitPrice = round((@FinanceInfo_unitRetailPrice/@qttMinEmb),2)

		INSERT into bi (bistamp, bostamp, nmdos, ndos, obrano,
					ref, codigo, design, qtt, qtt2, uni2qtt, u_psicont, u_bencont, 
					u_stockact,iva, tabiva, armazem, ar2mazem, stipo, cpoc,
					u_bonus, desconto, desc2, desc3, desc4,descval, 
					familia,no,nome,local,morada,codpost,
					epu,edebito,epcusto,ettdeb,ecustoind,edebitoori,u_upc,
					rdata,dataobra,dataopen,
					obistamp, resfor, rescli,lordem,lobs,ousrinis,
					ousrdata,ousrhora,--
					usrinis,usrdata,usrhora, datafinal,
					u_reserva, vendedor, vendnm, CCUSTO,u_epv1act, lote
					,num1, binum1, binum2, binum3, qtrec, u_mquebra,eslvu,esltt, fechada, ivaincl
			)Values (
				@stampid,ltrim(rtrim(@IdInfo_stamp)),@docSeriesName,@IDInfo_seriesNumber,@IDInfo_number,
				@ProductInfo_ref, @ProductInfo_ref, @design, isnull(@StockInfo_qtt,0), 0, isnull(@StockInfo_qtt,0), 0, 0,
				@stock_act, @VateRatesInfo_rate,@codigoIva,isnull(@StockInfo_warehouseSource,0),isnull(@StockInfo_warehouseDest,0),0,@cpoc,
				0, @discountPerc, 0, 0, 0, 0,	
				@family, @entityNumber,@clientName,@entityCity, @entityAddress, @entityZip,
				@FinanceInfo_unitPrice, @FinanceInfo_unitPrice, @priceCost, Round(@FinanceInfo_retailPrice,2), @priceUni, @priceUni, @epcult, 
				convert(varchar,dateadd(HOUR, @difhour, getdate()),102),@dateDoc, convert(varchar,dateadd(HOUR, @difhour, getdate()),102),
				'',0, 0, '1000', @GeneralInfo_obs, @operator, 
				convert(varchar,dateadd(HOUR, @difhour, getdate()),102),convert(varchar,dateadd(HOUR, @difhour, getdate()),8),
				@operator,convert(varchar,dateadd(HOUR, @difhour, getdate()),102), convert(varchar,dateadd(HOUR, @difhour, getdate()),8), @finalDate
				,0,1, @operator, '',@u_epv1act,'',
				0,0,0,0,0,'',0,0, @closed, 1
			)


			Insert into bi2 (bi2stamp, bostamp, morada, local, codpost, fnstamp, valeNr, idCampanha, valcartao) 
				Values(@stampid,@IdInfo_stamp,isnull(@entityAddress,''),isnull(@entityCity,''),isnull(@entityZip,''), '',
				isnull(@FinanceInfo_voucherNr,0), isnull(@FinanceInfo_campaignId,''), isnull(@FinanceInfo_cardMoney,0.0))


			update st set ousrdata=GETDATE() , ousrhora = CONVERT(CHAR(8),GETDATE(),108), usrinis=@operator where ref=@ProductInfo_ref and site_nr=@siteno
			

			
			if(@FinanceInfo_cardMoney!=0)	--cativa
			begin	
				--get client
				select 
					top 1  
					@clientNumber =  isnull(no,0)
					,@clientDep = isnull(estab,0)
				from 
					bo(nolock) 
				where 
					bostamp = rtrim(ltrim(@IdInfo_stamp))

				-- força update dos cliente
				update 
					b_utentes 
				set 
					usrdata = getdate()
					,usrhora = convert(varchar,getdate(),108)
				where
					no = @clientNumber and estab = @clientDep

			end
			select getdate() AS DateMessag, 1 As StatMessag, 'Inserido' As DescMessag

			


END

            
GO
Grant Execute On up_Documentos_line_UpdateInsert to Public
Grant Control On up_Documentos_line_UpdateInsert to Public
GO
			
           
			
	