/*
DOCUMENTOS Fix Doc
D.Almeida, 05-05-2020
revisão 05-05-2020
--------------------------------
bo
validacões finais do documento
	 stamp (@stamp)

	

exec up_Documentos_fixDoc 'df6bce8281a8456a8eaed3443'

select *from bi where ref='V000001' order by obrano desc



select *from  bo where bostamp='df6bce8281a8456a8eaed3443'

select *from bi2  where bostamp='df6bce8281a8456a8eaed3443'

select *from bi2(nolock)  where bostamp='df6bce8281a8456a8eaed3443'


*/


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_Documentos_fixDoc]') IS NOT NULL
    drop procedure up_Documentos_fixDoc
go

CREATE PROCEDURE up_Documentos_fixDoc	
	 
	 --identificadores do documento
	 @stamp					   varchar(25) = ''

	
AS
	declare @totalQtt  numeric(13,3) = 0
	declare @valCartao numeric(13,3) = 0



	select @totalQtt=sum(bi.qtt) from bi(nolock) where bostamp=@stamp

	select 
		@valCartao=sum(abs(round(bi2.valcartao,2))) 
	from bi2(nolock) 
		inner join  bi(nolock) on bi2.bi2stamp =bi.bistamp
	where 
		bi2.bostamp=@stamp 
		and  bi.ref='V000001'

	set @valCartao = isnull(@valCartao,0)		

	if(@totalQtt>0)
	begin
		update bo set sqtt14=@totalQtt, valcartao=round(@valCartao,2)  where bostamp=@stamp 
     end
		 


	SET @stamp =rtrim(ltrim(isnull(@stamp,'')))

	declare @docClosed bit = 0;

	 select @docClosed = isnull(fechada,1)
		from bo(nolock) 
	where 
		bostamp=ltrim(rtrim(@stamp))




	update bi set fechada = @docClosed where bostamp=@stamp 

	

         
GO
Grant Execute On up_Documentos_fixDoc to Public
Grant Control On up_Documentos_fixDoc to Public
GO
			
           
			
	