/* SP Pesquisa Detalhe Documentos (Rates)
D. Almeida, 2021-04-24
--------------------------------

Listagem agragada de valores de IVA,  baseada no campo fostamp proveniente da 
sp up_Documentos_pesquisaDetalheDocFornecedorRates.

Pesquisa nas tabela: 	
bo_totais

por 1 campo:
	'@stamp'
	
Campos obrigatórios: todos

OUTPUT:
---------
vatRatesInfo_taxa,	vatRatesInfo_incidenceBase,	vatRatesInfo_value


EXECUÇÃO DA SP:
---------------
exec up_Documentos_pesquisaDetalheDocFornecedorRates '@stamp'

	exec up_Documentos_pesquisaDetalheDocFornecedorRates  'ADME011D779-2EE3-4EF7-914'

	*/
	
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


IF OBJECT_ID('[dbo].[up_Documentos_pesquisaDetalheDocFornecedorRates]') IS NOT NULL
		DROP PROCEDURE [dbo].up_Documentos_pesquisaDetalheDocFornecedorRates ;
GO


CREATE PROCEDURE [dbo].up_Documentos_pesquisaDetalheDocFornecedorRates	
		 @stamp				VARCHAR(25)
	/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

SET @stamp	=   rtrim(ltrim(isnull(@stamp,'')))

IF len(@stamp) > 0 
BEGIN	
	DECLARE @sql varchar(1000)

	select @sql = N' 			
					SELECT 
						  taxa AS vatRatesInfo_rate
						, CAST(baseinc  AS numeric(12,2)) AS vatRatesInfo_incidenceBase
						, CAST(valoriva  AS numeric(12,2)) AS vatRatesInfo_value			
					FROM
       					[dbo].fo_totais(nolock)					
					WHERE
       					stamp = ''' + @stamp + ''' '

	--print @sql
	EXECUTE (@sql)
END
ELSE
	BEGIN
		print('Missing stamp')
	END
GO
GRANT EXECUTE on dbo.up_Documentos_pesquisaDetalheDocFornecedorRates TO PUBLIC
GRANT Control on dbo.up_Documentos_pesquisaDetalheDocFornecedorRates TO PUBLIC
GO