/* SP Pesquisa Verifica Documento
Daniel Almeida, 2020-05-04
--------------------------------

Pesquisa nas tabelas: 	
no

OUTPUT: 
---------
DateMessag				StatMessag  ActionMessage	DescMessag		   stamp		
2020-04-28 11:08:52.610	0			0                MULTIPLE DOCS	   admxpto	

DateMessag				StatMessag	ActionMessage    DescMessag		   stamp			
2020-04-28 11:07:38.063	1			1                 not Found         admxpto		

DateMessag				StatMessag	ActionMessage    DescMessag		   stamp			
2020-04-28 11:07:38.063	1			2                DOC Found         admxpto		    


EXECUÇÃO DA SP:
---------------
exec up_Documentos_verifica_documento 98, 41, 'Atlantico', 2022


*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_verifica_documento]') IS NOT NULL
	DROP PROCEDURE [dbo].up_Documentos_verifica_documento;
GO

CREATE PROCEDURE [dbo].up_Documentos_verifica_documento	

	 @IdInfo_number				int,		 /*number*/
	 @IdInfo_seriesNumber		int,		 /*seriesNumber*/
	 @IdInfo_site				varchar(20), /*site*/
	 @IdInfo_year			    int         /*boano*/	
	
	   
/* WITH ENCRYPTION */
AS

	
	DECLARE @contador			int = 0
	DECLARE @stamp				varchar(50) =''



	select 
		@contador = count(obrano), 
		@stamp = ltrim(rtrim(isnull(bostamp,'')))
	from 
		bo(nolock) 
	where
	    obrano = isnull(@IdInfo_number,-1) and
		ndos = isnull(@IdInfo_seriesNumber,-1) and
	--	site = ltrim(rtrim(isnull(@IdInfo_site,''))) and 
		boano = isnull(@IdInfo_year,1900) 
	group by bostamp	
	



	if(@contador>1)
	begin 
		select getdate() AS DateMessag, 0 As StatMessag, 0 AS ActionMessage, 'MULTIPLE DOCS' As DescMessag,  @stamp AS stamp;
		return 
	end

	

	if(len(@stamp)=0)
	begin 
	     print 'dentro'
		select getdate() AS DateMessag, 1 As StatMessag, 1 AS ActionMessage, 'NO DOC' As DescMessag,  @stamp AS stamp;
		return 
	end

	if(len(@stamp)>0 and @contador=1)
		select getdate() AS DateMessag, 1 As StatMessag, 2 AS ActionMessage, 'DOC FOUND' As DescMessag,  @stamp AS stamp;
	


GO
GRANT EXECUTE on dbo.up_Documentos_verifica_documento TO PUBLIC
GRANT Control on dbo.up_Documentos_verifica_documento TO PUBLIC
GO