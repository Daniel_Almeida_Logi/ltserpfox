/* Retorna tabIva
Jorge Gomes, 2020-05-06
--------------------------------



EXECUÇÃO DA SP:
---------------
exec up_Documentos_getTabIva 6

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Documentos_getTabIva]') IS NOT NULL
	DROP PROCEDURE [dbo].up_Documentos_getTabIva;
GO

CREATE PROCEDURE [dbo].up_Documentos_getTabIva	
	 @taxa			numeric(5,2)
	
	   
/* WITH ENCRYPTION */
AS

SET @taxa	=   rtrim(ltrim(isnull(@taxa,-1)))




SET NOCOUNT ON


	select top 1 codigo as tabIva from taxasiva (nolock) where taxa = @taxa	
	


GO
GRANT EXECUTE on dbo.up_Documentos_getTabIva TO PUBLIC
GRANT Control on dbo.up_Documentos_getTabIva TO PUBLIC
GO