/* SP Pesquisa de Produtos Geral
2020-08-13, DA

Pesquisa productos com campanhas associadas


exec up_stocks_PesquisaGeralCampanhasRef NULL,'bene - mais um carrada de caracteres so para encher e ver se parte!','Loja 1',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL, NULL, NULL

exec up_stocks_PesquisaGeralCampanhasRef '7393702',NULL,'Loja 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, NULL, NULL

*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_stocks_PesquisaGeralCampanhasRef]') IS NOT NULL
    DROP procedure dbo.up_stocks_PesquisaGeralCampanhasRef
GO

CREATE procedure dbo.up_stocks_PesquisaGeralCampanhasRef

	 @artigo						VARCHAR(120)
	,@marca							VARCHAR(200)
	,@site_name						VARCHAR(18)
	,@lastChangeDate				datetime
	,@lastChangeTime				varchar(8)
	,@pageNumber					int = 1
	,@id_grande_mercado_hmr         int = null
	,@id_mercado_hmr                int = null
	,@id_categoria_hmr              int = null
	,@id_segmento_mercado_hmr       int = null
	,@orderStamp					VARCHAR(36) = ""
	,@campanhaId                    int  = null
	,@campaignsOnly                 int  = null
	,@onlyOnline                    bit = 0
	,@pvpMin						decimal(19,6) = null
	,@pvpMax						decimal(19,6) = null
	,@onlyNew						bit = 0
	,@onlyDestak                    bit = 0

/* WITH ENCRYPTION */
AS

SET @artigo			=   rtrim(ltrim(isnull(@artigo,'')))
SET @marca			=   rtrim(ltrim(isnull(@marca,'')))
SET @site_name		=   rtrim(ltrim(@site_name))
SET @lastChangeDate =	rtrim(ltrim(isnull(@lastChangeDate,'19000101')))
SET @lastChangeTime =	rtrim(ltrim(isnull(@lastChangeTime,'00:00:00')))
 
DECLARE @lastChangeDateTime VARCHAR(20)
SET @lastChangeDate     = ltrim(rtrim(isnull(@lastChangeDate,'19000101')))
SET @lastChangeTime     = ltrim(rtrim(isnull(@lastChangeTime,'00:00:00')))
SET @lastChangeDateTime = convert(datetime,@lastChangeDate + @lastChangeTime)
SET @orderStamp         = rtrim(ltrim(ISNULL(@orderStamp,'')))
SET @campanhaId         = isnull(@campanhaId,-1)
SET @campaignsOnly      = isnull(@campaignsOnly,0)
SET @onlyOnline         = ISNULL(@onlyOnline,0)
Set @onlyNew			= ISNULL(@onlyNew,0)
Set @onlyDestak	        = ISNULL(@onlyDestak,0)



DECLARE @PageSize int = 100
DECLARE @orderBy VARCHAR(8000)
DECLARE @sql varchar(max)

if(isnull(@pageNumber,0)<1)
	set @pageNumber = 1


IF len(@site_name) > 0 
BEGIN

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosArmazens'))
		DROP TABLE #dadosArmazens	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt2'))
		DROP TABLE #dadosSt2	

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosImagens'))
		DROP TABLE #dadosImagens


	/* variaveis auxiliares */			
	DECLARE @id_lt varchar(100)	
	DECLARE @site_nr INT
	SELECT @id_lt = id_lt,@site_nr=no from empresa(nolock) where site = @site_name

	declare @stkcaixa as bit
	set @stkcaixa = (select bool from B_Parameters_site(nolock) where stamp='ADM0000000067' and site=(select site from empresa(nolock) where no=@site_nr))

	declare @moeda AS varchar(10)
	SET @moeda = (Select rtrim(ltrim(textValue)) from B_Parameters(nolock) where stamp='ADM0000000260')
	   
	DECLARE @OrderCri VARCHAR(MAX)=''
		
	DECLARE @downloadURL VARCHAR(100)
	SELECT @downloadURL = rtrim(ltrim(textValue)) FROM B_Parameters(nolock) WHERE stamp='ADM0000000313'

	DECLARE @imagemURL varchar(300) 
    set   @imagemURL = @downloadURL + @id_lt




	IF @orderStamp = ''
	BEGIN 
		SET @OrderCri = 'ORDER BY usrdata DESC, usrhora DESC'
	END 
	ELSE 
	BEGIN 
		SELECT @OrderCri='ORDER BY ' + instructionSql  FROM sortingTableOrder (NOLOCK) WHERE stamp = @orderStamp and inativo=0 and nameService='productCampaignSearch'
	END 

	set @orderBy = ' ) X ' + @OrderCri +'  
				 OFFSET ' + convert(varchar(10),@PageSize) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
				 FETCH NEXT  ' + convert(varchar(10),@PageSize) + ' ROWS ONLY '


	/* Armazens disponiveis por empresa */
	select empresa_no
	into #dadosArmazens
	from empresa_arm (nolock)
	inner join 
		empresa on empresa_arm.empresa_no = empresa.no 
	where
		empresa.ncont = (select ncont from empresa where no = @site_nr)





	/* Temp Table para permitir usar If statement */
	create table #dadosSt2 (	
		ref				                 varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
		, nhrn			                 varchar(18) /*cnp*/		
		,design			                 varchar(120) COLLATE SQL_Latin1_General_CP1_CI_AI		
		,stock			                 numeric(13,3)
		,epv1			                 numeric(19,6)
		,validade		                 varchar(10)				
		,generico		                 bit		
		,ststamp		                 char(25)		
		,cnpem			                 varchar(8)
		,marg4			                 numeric(16,3)		
		,u_lab			                 varchar(200)
		,usr1			                 varchar(200)
		,stns			                 bit 
		,inactivo		                 bit
		,dci			                 varchar(254)
		,site			                 varchar(18)
		, site_nr		                 tinyint		
		,obs			                 varchar(254)
		,marcada		                 bit
		,iva			                 numeric(5,2)
		, IVAincl		                 bit
		, moeda			                 varchar(10)		
		, qttembal  	                 numeric(20,2)	
		, qttminvd  	                 numeric(20,2)		
		, CreationDate		             datetime
		, CreationTime		             varchar(8) 
		, LastChangeDate	             datetime
		, Lastchangetime	             varchar(8)
		, lastChangeUser                 VARCHAR(30)
		, usrdata			             datetime
		, usrhora			             varchar(8) 
		, cativado			             numeric(13,3)
		, dicBigMarketHmrId			     int
		, dicBigMarketHmrDescr			 varchar(200)
		, dicMarketHmrId			     int
		, dicMarketHmrDescr			     varchar(200)
		, dicCategoryHmrId			     int
		, dicCategoryHmrDescr			 varchar(200)
		, dicSegmentHmrId			     int
		, dicSegmentHmrDescr			 varchar(200)
		, freeShipping		             bit
		, designOnline                   varchar(100)
		, dispOnline					 bit
		, onlineDescription1             varchar(max)
		, onlineDescription2             varchar(max)
		, onlineNew                      bit
		, onlineDescription3             varchar(max)
		, onlineTimeUnavailability       int  
		, onlineUnavailabilityUnit       varchar(10)
		, campaignId                     int  
		, campaignDescr                  varchar(max)  
		, campaignBuy                    decimal(19,6)
		, campaignOffer                  decimal(19,6)
		, campaignStatus                 bit
		, campaignStartDate              varchar(20)
		, campaignEndDate				 varchar(20)		
		, fornecedor					 varchar(max)												
		, fornec						 int
		, fornestab						 int
		, campaignUnit                   varchar(20)
		, previsto                       numeric(13,3)
			

		) 
	
	/* Result Set Para Pesquisas Feitas Apenas a Produtos com Ficha */

	
	/* Dados do Produto */
	insert into #dadosSt2
	Select 		
		distinct 
		isnull(st.ref, fprod.cnp)
		, isnull(fprod.cnp, st.codCNP)
		, ltrim(rtrim(isnull(st.design,fprod.design)))			
		, stock
		, epv1 
		, isnull(convert(varchar,validade,23), '1900-01-01')				
		, IsNull(fprod.generico,0)			
		, isnull(st.ststamp,'')			
		, convert(varchar(8),ISNULL(fprod.cnpem,''))
		, marg4 	
		, ISNULL(st.u_lab,fprod.titaimdescr)
		, left(ISNULL(st.usr1,fprod.u_marca),20)
		, stns
		, status = case when st.inactivo = 0 then 1 else 0 end
		, dci = ISNULL(dci,'')
		, site
		, st.site_nr			
		, isnull(st.obs,'')
		, st.marcada
		, isnull(taxasiva.taxa, 0)
		, st.ivaincl
		, @moeda			
		, qttembal  
		, qttminvd			
		, isnull(st.ousrdata, '1900-01-01')
		, isnull(st.ousrhora, '00:00:00')
		, isnull(st.usrdata, '1900-01-01')	 
		, isnull(st.usrhora, '00:00:00')   
		, ISNULL(st.usrinis,'')	
		, st.usrdata
		, st.usrhora
		, cativado
		, dicBigMarketHmrId= ISNULL(grande_mercado_hmr.id,'')													
		, dicBigMarketHmrDescr = ISNULL(grande_mercado_hmr.descr,'')														
		, dicMarketHmrId = ISNULL(mercado_hmr.id,'')														
		, dicMarketHmrDescr = ISNULL(mercado_hmr.descr,'')															
		, dicCategoryHmrId = ISNULL(categoria_hmr.id,'')														
		, dicCategoryHmrDescr = ISNULL(categoria_hmr.descr,'')															
		, dicSegmentHmrId = ISNULL(segmento_hmr.id,'')												
		, dicSegmentHmrDescr = ISNULL(segmento_hmr.descr,'')	
		, portesGratis = isnull(st.portesGratis,0)
		, designOnline = ltrim(rtrim(isnull(st.designOnline,'')))
		, dispOnline =  (CASE WHEN st.dispOnline = 1 and  st.inactivo=0 then 0 else 1 end)
		, onlineDescription1 = ltrim(rtrim(isnull(st.caractOnline,'')))
		, onlineDescription2 = ltrim(rtrim(isnull(st.apresentOnline,'')))
		, onlineNew = st.novidadeOnline											 
		, onlineDescription3 = ''
		, onlineTimeUnavailability = st.tempoIndiponibilidade
		, onlineUnavailabilityUnit ='dia'		
		, campaignId = isnull(campanhas_online_st.idTipoCampanha ,0)
		, campaignDescr = REPLACE(REPLACE(ltrim(rtrim(isnull(campanhas_online.descrTipo,''))),'Y',convert(int,isnull(campanhas_online_st.valorCompra,0.00))),'X',convert(int,isnull(campanhas_online_st.valorOferta,0.00)))
		, campaignBuy = round(isnull(campanhas_online_st.valorCompra,0.00),2)
		, campaignOffer =  round(isnull(campanhas_online_st.valorOferta,0.00),2)
		, campaignStatus = isnull(campanhas_online_st.activo,0)
		, campaignStartDate	= convert(varchar, isnull(campanhas_online_st.dataInicio, '1900-01-01'), 23)
		, campaignEndDate   =   convert(varchar, isnull(campanhas_online_st.dataFim, '3000-01-01'), 23)
		, st.fornecedor			
		, st.fornec			
		, st.fornestab	
		, case when isnull(campanhas_online_st.idTipoCampanha ,0) =1 then  @moeda 
				 when isnull(campanhas_online_st.idTipoCampanha ,0) = 2 then  '%' 
				 when isnull(campanhas_online_st.idTipoCampanha ,0) = 3 then  'qtt'		
				else '' end
		,isnull(st.stock,0) + isnull(st.qttfor,0) - isnull(st.qttcli,0) - isnull(cativado,0)
                  
	From 
		st (nolock)
		left join fprod (nolock) on   (st.ref = fprod.cnp or st.codCNP=fprod.cnp)
		left join cptgrp (nolock) on cptgrp.grupo = fprod.grupo
		left join empresa (nolock) on st.site_nr = empresa.no
		left join taxasiva (nolock) on st.tabiva = taxasiva.codigo		
		left join campanhas_online_st(nolock) on campanhas_online_st.ref = st.ref and campanhas_online_st.site_nr =st.site_nr
		left join campanhas_online(nolock) on campanhas_online.idTipo = campanhas_online_st.idTipoCampanha
		left JOIN  grande_mercado_hmr (nolock) on st.u_depstamp = grande_mercado_hmr.id
		left JOIN  mercado_hmr (nolock) on st.u_secstamp= mercado_hmr.id
		left JOIN  categoria_hmr (nolock) on st.u_catstamp = categoria_hmr.id
		left JOIN  segmento_hmr (nolock) on st.u_segstamp= segmento_hmr.id
	
	Where 
		st.site_nr = @site_nr
		AND (
			st.ref like @artigo
			OR exists (Select ref from bc (nolock) where bc.ref = st.ref and bc.site_nr = @site_nr and bc.codigo = @artigo)  
			OR st.design LIKE '%' + @artigo + '%'
			OR fprod.cnpem = @artigo
			OR fprod.cnpem_old = @artigo
			OR st.codCNP like @artigo
			)		
	    AND convert(datetime,convert(varchar(10),st.usrdata,120)+' ' + st.usrhora) >= @lastChangeDateTime
		and isnull(st.u_depstamp,0) =  case when isnull(@id_grande_mercado_hmr,0)=0  then  isnull(st.u_depstamp,0) else @id_grande_mercado_hmr end
		and isnull(st.u_secstamp,0) =  case when isnull(@id_mercado_hmr,0)=0  then  isnull(st.u_secstamp,0) else @id_mercado_hmr end
		and isnull(st.u_catstamp,0) =  case when isnull(@id_categoria_hmr,0)=0  then  isnull(st.u_catstamp,0) else @id_categoria_hmr end
		and isnull(st.u_segstamp,0) =  case when isnull(@id_segmento_mercado_hmr,0)=0  then  isnull(st.u_segstamp,0) else @id_segmento_mercado_hmr end
	 	and isnull(campanhas_online_st.activo,0) = case when @campaignsOnly  = 0 then isnull(campanhas_online_st.activo,0)  else 1 end
		and isnull(campanhas_online.activo,0) = case when @campaignsOnly  = 0 then isnull(campanhas_online.activo,0) else 1 end
		and isnull(campanhas_online_st.idTipoCampanha,0) = case when @campanhaId>0  then  @campanhaId else isnull(campanhas_online_st.idTipoCampanha,0)  end 
		and isnull(campanhas_online_st.site_nr,0) = case when @campaignsOnly  = 0  then isnull(campanhas_online_st.site_nr,0)  else @site_nr end
		and isnull(st.novidadeOnline,0) =  case when isnull(@onlyNew,0)=0 then isnull(st.novidadeOnline,0) else 1 end
		and isnull(st.dispOnline,0) =  case when isnull(@onlyOnline,0)=0 then isnull(st.dispOnline,0) else 1 end
		and isnull(st.inactivo,1)   =  case when isnull(@onlyOnline,0)=0 then isnull(st.inactivo,1) else 0 end
		AND ((@onlyDestak = 1 AND ISNULL(campanhas_online_st.idTipoCampanha, 0) IN (1, 2, 3)) OR (@onlyDestak = 0  AND ISNULL(campanhas_online_st.idTipoCampanha, 0) = ISNULL(campanhas_online_st.idTipoCampanha, 0)))

	/* Tratamento de Dados para Result Set - Eliminar Tabela Temp. parece ter melhor performance do que colocar filtros na query*/

	if @marca != ''
		delete from #dadosSt2 where usr1 != @marca


	
		select @sql = N' SELECT '  + convert(varchar(10),@PageSize) +' as pageSize,  (COUNT(*) OVER() / ' +  convert(varchar(10),@PageSize) +') + 1 as pageTotal, '
				+ convert(varchar(10),@PageNumber) +' as pageNumber, COUNT(*) OVER() as linesTotal		
			,* from (SELECT
		
				st2.design AS designation, 	st2.nhrn, st2.ref, st2.cnpem
			, dbo.unityConverter(st2.stock,''stocksUnityToBox'', ' + convert(varchar(10),@site_nr) + ',st2.ref)	AS stockSite	
			, dbo.unityConverter(x.StockEmpresa,''stocksUnityToBox'', ' + convert(varchar(10),@site_nr) + ',st2.ref)  AS stockCompany
			, dbo.unityConverter(y.StockGrupo,''stocksUnityToBox'', ' + convert(varchar(10),@site_nr) + ',st2.ref)  AS stockGroup
			, dbo.unityConverter(st2.previsto,''stocksUnityToBox'', ' + convert(varchar(10),@site_nr) + ',st2.ref)  AS stockExpected
			, dbo.unityConverter(isnull(st2.epv1,0),''priceUnityToBox'',' + convert(varchar(10),@site_nr) + ' ,st2.ref)    AS retailPrice
			, st2.IVAincl AS vatInc, st2.IVA AS vatPerc 
			, dbo.unityConverter(isnull(st2.marg4,0),''priceUnityToBox'', ' + convert(varchar(10),@site_nr) + ' ,st2.ref)  	 AS grossMarg
			, st2.moeda AS currency, st2.obs, st2.validade AS expirationDate
			, convert(varchar, st2.CreationDate, 23) AS OperationInfo_CreationDate 
			,  CreationTime  AS OperationInfo_CreationTime
			, convert(varchar, st2.LastChangeDate, 23) AS OperationInfo_LastChangeDate 
			,  st2.LastChangeTime AS  OperationInfo_LastChangeTime
			, st2.lastChangeUser  AS  OperationInfo_lastChangeUser
			, st2.inactivo AS status
			, st2.qttminvd AS qttMinSell 
			, st2.qttembal AS qttUnEmb	
			, st2.usrdata
			, st2.usrhora
			, st2.marcada
			, st2.stock
			, dbo.unityConverter(st2.cativado,''stocksUnityToBox'', ' + convert(varchar(10),@site_nr) + ' ,st2.ref)  as stockCaptivated	
			, ISNULL((SELECT TOP 1 ISNULL('''+ @imagemURL +''' + ''/''+ anexosstamp,'''') FROM anexos (nolock) WHERE regstamp COLLATE DATABASE_DEFAULT = st2.ststamp COLLATE DATABASE_DEFAULT order by usrdata desc),'''') AS OnlineImage1
			, dicBigMarketHmrDescr AS dicBigMarketHmrDescr
			, dicBigMarketHmrId AS dicBigMarketHmrId
			, dicCategoryHmrId AS dicCategoryHmrId
			, dicCategoryHmrDescr AS dicCategoryHmrDescr
			, dicMarketHmrId AS dicMarketHmrId
			, dicMarketHmrDescr AS dicMarketHmrDescr
			, dicSegmentHmrId AS dicSegmentHmrId
			, dicSegmentHmrDescr AS dicSegmentHmrDescr
			, usr1 AS ProductInfo_brand
			, onlineDescription1 AS OnlineDescription1
			, onlineDescription2 AS OnlineDescription2
			, onlineDescription3 AS OnlineDescription3
			, designOnline   AS OnlineDesignation	
			, freeShipping   AS OnlineFreeShipping
			, dispOnline     AS OnlineInactive
			, campaignId     AS campaignId
			, campaignDescr  AS campaignDesign
			, campaignBuy    AS campaignBuy
			, campaignOffer  AS campaignOffer		
			, campaignUnit   AS campaignUnit
			, campaignStatus AS campaignStatus	
			, fornecedor     AS providerDesignation
			, fornec         AS providerNumber
			, fornestab      AS providerDep
			, campaignStartDate AS campaignStartDate
			, campaignEndDate	AS campaignEndDate
			, onlineTimeUnavailability AS OnlineTimeUnavailability 
		    , onlineUnavailabilityUnit	AS OnlineUnavailabilityUnit
			, onlineNew AS OnlineNew
		From
			#dadosSt2 st2
			

			left outer join (select ref, StockEmpresa = sum(stock) 
								from st (nolock)
								where 
									site_nr in (select * from #dadosArmazens)
									and stns = 0
								group by 
									ref) x on st2.ref = x.ref
				left outer join (select ref, StockGrupo = sum(stock) 
								from st (nolock)
								where 
									stns = 0
								group by 
									ref) y on st2.ref = y.ref
			'


		
		SELECT @sql = @sql + @orderBy
		
		print @sql
		EXECUTE (@sql)

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosArmazens'))
		DROP TABLE #dadosArmazens	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt2'))
		DROP TABLE #dadosSt2
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosImagens'))
		DROP TABLE #dadosImagens	

END
ELSE
BEGIN
	print('Missing Site_name')
END

GO
GRANT EXECUTE on dbo.up_stocks_PesquisaGeralCampanhasRef TO PUBLIC
GRANT Control on dbo.up_stocks_PesquisaGeralCampanhasRef TO PUBLIC
GO