/* 
SP Pesquisa de Marcas de Productos
2024-10-26, DA

Descri��o:
    Esta stored procedure realiza uma pesquisa de marcas de produtos na tabela `marcas` com base em filtros fornecidos, incluindo:
    marca, nome da loja, data e hora de altera��o, controle de pagina��o, filtro de uso, e c�digo de ordem. 

Par�metros:
    @marca               VARCHAR(200)  - Filtro de marca (palavra-chave) a ser buscada na descri��o do produto.
    @site_name           VARCHAR(18)   - Nome do site ou loja (campo obrigat�rio).
    @lastChangeDate      DATE          - Data m�nima de altera��o dos registros.
    @lastChangeTime      TIME(0)       - Hora m�nima de altera��o dos registros.
    @topMax              INT = 100     - N�mero m�ximo de registros por p�gina (limitado a 20000).
    @pageNumber          INT = 1       - N�mero da p�gina para a pagina��o.
    @usedOnly            BIT = 0       - Filtro opcional para registros usados (0 = todos, 1 = apenas usados).
    @onlineOnly          BIT = 0       - Filtro opcional para registros online (0 = todos, 1 = apenas online).
	@orderStamp          VARCHAR(36) = '' - C�digo opcional de ordem (stamp) para filtragem.

Exemplo de Chamada:
    EXEC up_stocks_PesquisaMarcas 'Isdin', 'Loja 1','1900-01-01','10:00',1000,1,0,0,''

	EXEC up_stocks_PesquisaMarcas '', 'Loja 1','2000-01-01','10:00',20000,1,0,1,''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_stocks_PesquisaMarcas]') IS NOT NULL
    DROP PROCEDURE dbo.up_stocks_PesquisaMarcas
GO

CREATE PROCEDURE dbo.up_stocks_PesquisaMarcas
     @marca               VARCHAR(200),
     @site_name           VARCHAR(18),
     @lastChangeDate	  VARCHAR(10) = '1900-01-01',
	 @lastChangeTime	  VARCHAR(8)  = '00:00:00',
     @topMax              INT = 100,
     @pageNumber          INT = 1,
     @usedOnly            BIT = 0,
	 @onlineOnly          BIT = 0,
     @orderStamp          VARCHAR(36) = ''
AS

-- Limpeza e formata��o dos par�metros
SET @site_name = RTRIM(LTRIM(@site_name))
SET @lastChangeDate = ISNULL(@lastChangeDate, '1900-01-01')
SET @lastChangeTime = ISNULL(@lastChangeTime, '00:00:00')

DECLARE @lastChangeDateTime DATETIME = CAST(@lastChangeDate AS DATETIME) + CAST(@lastChangeTime AS DATETIME)

-- Limite de @topMax para evitar sobrecarga
IF ISNULL(@topMax, 0) > 20000 SET @topMax = 20000
IF ISNULL(@topMax, 0) <= 0 SET @topMax = 20000

DECLARE @PageSize INT = ISNULL(@topMax, 20000)
DECLARE @orderBy VARCHAR(8000) = ''
DECLARE @innerJoin VARCHAR(8000) = ''
DECLARE @sql VARCHAR(MAX) =  ''

IF ISNULL(@pageNumber, 0) < 1 SET @pageNumber = 1

-- Valida��o do nome do site
IF LEN(@site_name) > 0 
BEGIN

	DECLARE @id_lt varchar(100)	
	DECLARE @site_nr INT
	SELECT @id_lt = rtrim(ltrim(id_lt)),@site_nr=no from empresa(nolock) where site = @site_name


	DECLARE @downloadURL VARCHAR(100) = ''
	SELECT  @downloadURL = rtrim(ltrim(textValue)) FROM B_Parameters(nolock) WHERE stamp='ADM0000000313'


	DECLARE @marcaURL varchar(300) 
	set   @marcaURL = @downloadURL + @id_lt + '/'
    -- Crit�rio de Ordena��o
    DECLARE @OrderCri VARCHAR(MAX) = 'ORDER BY DesignInfo_designation ASC'
    SET @orderBy = ' ) X ' + @OrderCri + '  
                     OFFSET ' + CONVERT(VARCHAR(10), @topMax) + ' * (' + CONVERT(VARCHAR(10), @pageNumber) + ' - 1) ROWS
                     FETCH NEXT ' + CONVERT(VARCHAR(10), @topMax) + ' ROWS ONLY '
	
	set @innerJoin = 'inner join st(nolock) on marcas.descr = ISNULL(st.usr1,'''') and st.usr1!='''' and st.site_nr='+rtrim(ltrim(STR(@site_nr))) 
    -- Constru��o da query din�mica
    SET @sql = '
    SELECT ' + CONVERT(VARCHAR(10), @topMax) + ' AS pageSize,
           (COUNT(*) OVER() / ' + CONVERT(VARCHAR(10), @topMax) + ') + 1 AS pageTotal,
           ' + CONVERT(VARCHAR(10), @pageNumber) + ' AS pageNumber,
           COUNT(*) OVER() AS linesTotal,
           * 
    FROM (
        SELECT DISTINCT
               ISNULL(marcas.id, 0) AS IdInfo_ref,
               RTRIM(LTRIM(ISNULL(marcas.descr, ''''))) AS DesignInfo_Designation,
               CONVERT(VARCHAR, marcas.dataalt, 23) AS OperationInfo_CreationDate,
               CONVERT(VARCHAR, marcas.dataalt, 108) AS OperationInfo_CreationTime,
               CONVERT(VARCHAR, marcas.dataalt, 23) AS OperationInfo_LastChangeDate,
               CONVERT(VARCHAR, marcas.dataalt, 108) AS OperationInfo_LastChangeTime,
			   '''+@downloadURL + @id_lt + '/' + '''+anexos.anexosstamp as OnlineInfo_OnlineImage
        FROM marcas (NOLOCK) 
		left join anexos(nolock) on marcas.stamp = anexos.regstamp
		'

		if(@usedOnly=1 or @onlineOnly=1)
			set @sql = @sql + @innerJoin

		
        set @sql = @sql + ' WHERE marcas.dataalt >= ''' + CONVERT(VARCHAR, @lastChangeDateTime, 121) + ''''

    -- Filtro adicional para marca
    IF RTRIM(LTRIM(@marca)) <> ''
        SET @sql = @sql + ' AND marcas.descr LIKE ''%' + RTRIM(LTRIM(@marca)) + '%'''

	if(@onlineOnly=1)
		 SET @sql = @sql + " and st.dispOnline = 1"

    -- Aplica��o do crit�rio de ordena��o
    SET @sql = @sql + @orderBy

    -- Execu��o da query din�mica
    PRINT @sql
    EXECUTE (@sql)
END
ELSE
BEGIN
    PRINT('Missing Site_name')
END

GO

-- Permiss�es
GRANT EXECUTE ON dbo.up_stocks_PesquisaMarcas TO PUBLIC
GRANT CONTROL ON dbo.up_stocks_PesquisaMarcas TO PUBLIC
GO
