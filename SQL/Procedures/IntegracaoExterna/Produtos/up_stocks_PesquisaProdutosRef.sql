/* SP Pesquisa de Produtos por Referencia
2020-04-08, JG

Variaveis de Input (ambas obrigatórias):
--------------------
@ref		
@site_name


Variaveis de Ouput:
--------------------
 ref, nhrn, cnpem, designation, site, site_nr, stockSite, stockCampany , stockGroup 
, stockMax, stockMin, entrusted, booked, lastMoveDate, lastExitDate, lastEntryDate
,retailprice ,grossMargin, currency, vatInc, vatPerc, pcl, pcp, pct, discountPerc, discountValue
, creationDate, creationTime, LastChangeDate, LastChangeTime, expirationDate, service, generic, voucher, weight, weight1
, weightUnit, width, volume, st.obs , lab, brand
, providerDesignation, providerNumber, providerDep
, OnlineDesignation, OnlineInactive, OnlineNew, OnlinePromotion, OnlineDescription1
, OnlineDescription2, OnlineDescription3, OnlineImage
, dicDesignation, dicNhrn, dicDescription, dicCnpem, dicGeneric, dicUnitDose, dicFormPharm
, dicgrpHmgCode,dicgrpHmgDescr,dicTitAimState, dicEmbType, dicComercState, dicComercDescr
, dicVatPerc, dicInn, dicRefPrice,dicRetailPrice, dicRetailPriceMax, dicRetailPriceMaxRe
, dicRetailPriceAgreed, dicBrand, dicLab, dicFamilyCode, dicCurrency




Execução da SP:
----------------
	up_stocks_PesquisaProdutosRef '000340' , 'ATLANTICO'

	 exec up_stocks_PesquisaProdutosRef '5440987' , 'loja 1', ''
	 exec up_stocks_PesquisaProdutosRef '5440987' , 'loja 1','1234567891234'
		
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_PesquisaProdutosRef]') IS NOT NULL
	drop procedure dbo.up_stocks_PesquisaProdutosRef
go

CREATE PROCEDURE dbo.up_stocks_PesquisaProdutosRef
@ref				VARCHAR(28),
@site_name			VARCHAR(18),
@codeAlter		    VARCHAR(18) = ''


AS

SET @ref			=   rtrim(ltrim(@ref))
SET @site_name		=   rtrim(ltrim(@site_name))
SET @codeAlter		=   rtrim(ltrim(isnull(@codeAlter,'')))
 

IF (len(@ref) > 0 or len(@codeAlter)>0) and len(@site_name) > 0
BEGIN
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosArmazens'))
		DROP TABLE #dadosArmazens

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosImagens'))
		DROP TABLE #dadosImagens


	SET NOCOUNT ON		

	-- variaveis auxiliares 		
	DECLARE @siteno AS tinyint
	SET @siteno = (select  no from empresa(nolock) where site = @site_name)	
	
	DECLARE @moeda AS varchar(10)
	SET @moeda = (Select textValue from B_Parameters(nolock) where stamp='ADM0000000260')

	DECLARE @id_lt varchar(100)	
	DECLARE @site_nr INT
	SELECT @id_lt = rtrim(ltrim(id_lt)),@site_nr=no from empresa(nolock) where site = @site_name

	
	DECLARE @downloadURL VARCHAR(100)
	SELECT @downloadURL = rtrim(ltrim(textValue)) FROM B_Parameters(nolock) WHERE stamp='ADM0000000313'

	declare @stkcaixa as bit
	set @stkcaixa = (select bool from B_Parameters_site(nolock) where stamp='ADM0000000067' and site=(select site from empresa(nolock) where no=@site_nr))



	if(len(@ref)=0 and len(@codeAlter)>0)
	begin
		select 
			top 1 @ref = rtrim(ltrim(isnull(ref,'ZZZZZZZZ')))
		from bc(nolock)
		where codigo = @codeAlter and site_nr = @site_nr
	
		if(len(rtrim(ltrim(isnull(@ref,''))))=0)
			set @ref = 'ZZZZZZZZ'
	end




	declare @dicImages bit = 0
	declare @dicText varchar(254) = ''

	select 
		top 1 @dicImages = isnull(bool,0)
	from 
		B_Parameters_site(nolock) 
	where stamp='ADM0000000107' and site = @site_name

	if(isnull(@dicImages,0)=1)
	  set @dicText = 'DIC'
	else
	  set @dicText = 'XXXXXXXXXXXXX'

	SELECT 
	top 5 
		row_number() over(order by anexos.usrdata desc) as rowNumber ,
		@downloadURL + @id_lt + '/' + anexosstamp       as onlineImageURL
	INTO #dadosImagens
	FROM st(nolock)
		Inner join anexos(nolock) ON anexos.regstamp = st.ststamp
	where ref=@ref and site_nr=@site_nr  AND tabela='st' AND (tipo='imagemProd' or tipo=@dicText)


	-- Armazens disponiveis por empresa
	select 	empresa_no
	into #dadosArmazens
	from empresa_arm 
	inner join empresa (nolock)
		on empresa_arm.empresa_no = empresa.no 
	where empresa.ncont = (select ncont from empresa(nolock) where no = @siteno)


	--retorna pais
	declare @pais varchar(350) = ''
	select 
		top 1 @pais = lower(rtrim(ltrim(isnull(textValue,'')))) 
	from 
		B_Parameters_site(nolock) 
	where stamp='ADM0000000050' and site=@site_name


	  
	--alterCode : diversos códigos que podem endereçar a mesma referência
	--select codigo, ref from  bc where ref='5440987' and site_nr = 1
	
	DECLARE @alterCode AS varchar(18)
	SET @alterCode = (SELECT STUFF((select ','+rtrim(ltrim(codigo)) from bc us(nolock)
		where us.ref = ss.ref 		
		FOR XML PATH('')),1,1,'') value		
	from bc (nolock) ss
	where ref=@ref and site_nr = @siteno
	group by ss.ref)



	if(@pais='portugal') 
	begin
		SELECT TOP 1 		
			  isnull(fprod.cnp, st.ref)		                                                            As ref
			, isnull(fprod.cnp, st.codCNP)																AS nhrn
			, convert(varchar(8),ISNULL(fprod.cnpem,''))												AS cnpem
			, isnull(@alterCode,'')																		AS alterCode
			, ltrim(rtrim(isnull(st.design,fprod.design)))												AS designation
			, @site_name																				AS site
			, @siteno																					AS site_nr		
			, isnull(dbo.unityConverter(st.stock,'stocksUnityToBox', @site_nr ,st.ref),0)               AS stockSite
			, isnull(dbo.unityConverter(isnull(st.stock,0) + isnull(st.qttfor,0) - 
					isnull(st.qttcli,0) - isnull(cativado,0),'stocksUnityToBox', @site_nr ,st.ref),0)	AS stockExpected
			, isnull(dbo.unityConverter(st.cativado,'stocksUnityToBox', @site_nr ,st.ref),0)   			AS stockCaptivated
			, isnull((select sum(dbo.unityConverter(st.stock,'stocksUnityToBox', @site_nr ,st.ref) )
			 from st(nolock) where ref = @ref and site_nr in (select * from #dadosArmazens) ),0) 
																										AS stockCompany
			, isnull((select sum(dbo.unityConverter(st.stock,'stocksUnityToBox',
			 @site_nr ,st.ref)) from st(nolock)  where ref = @ref),0)                                   AS stockGroup

			, isnull(dbo.unityConverter(st.stmax,'stocksUnityToBox', @site_nr ,st.ref),0)				AS stockMax
			, isnull(dbo.unityConverter(st.ptoenc,'stocksUnityToBox', @site_nr ,st.ref),0)				AS stockMin
			, isnull(dbo.unityConverter(st.qttfor,'stocksUnityToBox', @site_nr ,st.ref),0)				AS entrusted /*encomendas abertas*/
			, isnull(dbo.unityConverter(st.qttcli,'stocksUnityToBox', @site_nr ,st.ref),0)				AS booked  /*reservas abertas*/
			, case when st.ref is null or  st.site_nr!=@site_nr       
			   then convert(bit,0) else convert(bit,1) end                                              As created
			, convert(varchar, isnull(st.udata, '1900-01-01'), 23)										AS lastMoveDate
			, convert(varchar, isnull(st.usaid, '1900-01-01'), 23)										AS lastExitDate
			, convert(varchar, isnull(st.uintr, '1900-01-01'), 23)										AS lastEntryDate
			, dbo.unityConverter(isnull(isnull(st.epv1,fprod.pvporig),0),'priceUnityToBox'
			, @site_nr ,st.ref) 				                                                        AS retailprice /*pvp*/
			, dbo.unityConverter(isnull(st.marg4,0),'priceUnityToBox', @site_nr ,st.ref) 				AS grossMargin /*margin*/
			, @moeda																					AS currency
			, isnull(st.ivaincl,1)																		AS vatInc
			, isnull(taxasiva.taxa,0)														            AS vatPerc
			, dbo.unityConverter(isnull(st.epcusto,0), 'priceUnityToBox', @site_nr ,st.ref) 			AS pcl		
			, dbo.unityConverter(isnull(st.epcpond,0), 'priceUnityToBox', @site_nr ,st.ref) 			AS pcp
			, dbo.unityConverter(isnull(st.epcult,0),  'priceUnityToBox', @site_nr ,st.ref) 			AS pct
			, isnull(st.mfornec,0)																		AS discountPerc
			, isnull(st.mfornec2,0)																		AS discountValue
			, isnull(st.qttminvd,1)																		AS qttMinSell 
			, isnull(st.qttembal,fprod.num_unidades)													AS qttUnEmb
			, isnull(st.precoSobConsulta,0)																AS priceOnRequest
			, convert(varchar, isnull(st.ousrdata, '1900-01-01'), 23)									AS OperationInfo_CreationDate		
			, isnull(st.ousrhora,'00:00:00')															AS OperationInfo_CreationTime
			, convert(varchar, isnull(st.usrdata, '1900-01-01'), 23)									AS OperationInfo_LastChangeDate		
			, isnull(st.usrhora,'00:00:00')															    AS OperationInfo_LastChangeTime
			, isnull(st.usrinis,'')																		AS OperationInfo_LastChangeUser
			, convert(varchar,isnull((select top 1 isnull(datalc,'1900-01-01') 
										from sl(nolock) 
										where ref=@ref 
										 and origem = 'FT'
										 and armazem = @siteno 
										order by datalc desc
										 ),
										 '1900-01-01'),23)                                              AS OperationInfo_LastFinanceMoveDate
			, convert(varchar, isnull(st.validade, '1900-01-01'), 23)									AS expirationDate
			, isnull(st.stns,0)																			AS service
			, isnull(u_servMarc,0)                                                                      AS isAppointment
			, IsNull(fprod.generico,0)																	AS generic
			, isnull(st.qlook,0)																		AS voucher /*desconto*/
			, isnull(st.pbruto,0)																		AS weight
			, isnull(st.peso,0)																			AS weight1
			, isnull(st.unidade,0)																		AS weightUnit		
			, isnull(st.qttesp,0)																		AS width
			, isnull(st.volume,0)																		AS volume
			, isnull(st.obs,'')                                                                         AS obs   
			, isnull(st.u_lab,fprod.titaimdescr)														AS lab
			, isnull(st.usr1,fprod.u_marca)																AS brand
			, isnull(st.familia, fprod.u_familia)														AS familyCode
			, status = CASE WHEN isnull(st.inactivo,0) = 0 THEN 1 ELSE 0 END
			, isnull(profundidade,0)																	AS depth
			, isnull(altura,0)																			AS height
			,'cm'																						AS measureunit 
																									
			--provider																					
			, isnull(st.fornecedor,'')																	AS providerDesignation
			, isnull(st.fornec,0)																		AS providerNumber
			, isnull(st.fornestab,0)																	AS providerDep

			--online: tudo a vazio
			, isnull(st.designOnline,fprod.designOnline)												AS OnlineDesignation
			, (CASE WHEN st.dispOnline = 1 and  st.inactivo=0 then 0 else 1 end)						AS OnlineInactive
			, isnull(st.novidadeOnline,0)																AS OnlineNew

			, isnull(st.caractOnline, fprod.caractOnline)												AS OnlineDescription1
			, isnull(st.apresentOnline, fprod.apresentOnline)											AS OnlineDescription2
			, ''																						AS OnlineDescription3
			, ISNULL((SELECT ISNULL(onlineImageURL,'') FROM #dadosImagens where rowNumber=1),'')		AS OnlineImage
			, ISNULL((SELECT ISNULL(onlineImageURL,'') FROM #dadosImagens where rowNumber=2),'')		AS OnlineImage1
			, ISNULL((SELECT ISNULL(onlineImageURL,'') FROM #dadosImagens where rowNumber=3),'')		AS OnlineImage2
			, ISNULL((SELECT ISNULL(onlineImageURL,'') FROM #dadosImagens where rowNumber=4),'')		AS OnlineImage3
			, ISNULL((SELECT ISNULL(onlineImageURL,'') FROM #dadosImagens where rowNumber=5),'')		AS OnlineImage4
			, isnull(st.tempoIndiponibilidade,0)														AS OnlineTimeUnavailability 
			,'dia'																						AS OnlineUnavailabilityUnit
																									
			--dicionario																				
			, fprod.design																				AS dicDesignation
			, fprod.cnp																					AS dicNhrn
			, fprod.descricao																			AS dicDescription
			, fprod.cnpem																				AS dicCnpem
			, fprod.generico																			AS dicGeneric
			, fprod.dosuni																				AS dicUnitDose
			, fprod.fformasdescr																		AS dicFormPharm		
			, fprod.grphmgcode																			AS dicgrpHmgCode
			, fprod.grphmgdescr																			AS dicgrpHmgDescr
			, fprod.estaimdescr																			AS dicTitAimState
			, fprod.tipembdescr																			AS dicEmbType
			, fprod.u_nomerc																			AS dicComercState
			, fprod.sitcomdescr																			AS dicComercDescr
			, fprod.u_tabiva																			AS dicVatPerc		
			, fprod.dci																					AS dicInn
			, fprod.pref																				AS dicRefPrice
			, fprod.pvporig																				AS dicRetailPrice
			, fprod.pvpmax																				AS dicRetailPriceMax
			, fprod.pvpmaxre																			AS dicRetailPriceMaxRe
			, fprod.preco_acordo																		AS dicRetailPriceAgreed
			, fprod.u_marca																				AS dicBrand
			, fprod.titaimdescr																			AS dicLab
			, isnull(lab.id,'0')																		AS dicTitAimInfarmedCode
			, isnull(lab.descr,'')																	    AS dicTitAimInfarmedDesign
			, case when ISNULL(fprod.u_familia,'')='' THEN  0 ELSE fprod.u_familia END 					AS dicFamilyCode
			, rtrim(ltrim(isnull(b_atcfp.atccode,'')))													AS dicAtcCode
			, 'EURO'																					AS dicCurrency
			, ISNULL(grande_mercado_hmr.id,'')													        AS dicBigMarketHmrId
			, ISNULL(grande_mercado_hmr.descr,'')														AS dicBigMarketHmrDescr
			, ISNULL(mercado_hmr.id,'')														            AS dicMarketHmrId
			, ISNULL(mercado_hmr.descr,'')																AS dicMarketHmrDescr
			, ISNULL(categoria_hmr.id,'')															    AS dicCategoryHmrId
			, ISNULL(categoria_hmr.descr,'')															AS dicCategoryHmrDescr
			, ISNULL(segmento_hmr.id,'')															    AS dicSegmentHmrId
			, ISNULL(segmento_hmr.descr,'')																AS dicSegmentHmrDescr
			,''																							AS dicLinkPdfRCM
			,''																							AS dicLinkPdfFl
			,rtrim(ltrim(ISNULL(posologia.posologia_orientativa,'')))									AS dicDosage
			,campaignUnit = case when isnull(campanhas_online_st.idTipoCampanha ,0) =1 then  @moeda    
					 when isnull(campanhas_online_st.idTipoCampanha ,0) = 2 then  '%' 
					 when isnull(campanhas_online_st.idTipoCampanha ,0) = 3 then  'qtt'		
					else '' end
			, isnull(campanhas_online_st.idTipoCampanha ,0)                                             AS campaignId
			, REPLACE(REPLACE(ltrim(rtrim(isnull(campanhas_online.descrTipo,''))),'Y',
			  convert(int,isnull(campanhas_online_st.valorCompra,0.00))),'X',
			  convert(int,isnull(campanhas_online_st.valorOferta,0.00)))                                AS campaignDesign
			, round(isnull(campanhas_online_st.valorCompra,0.00),2)                                     AS campaignBuy
			, round(isnull(campanhas_online_st.valorOferta,0.00),2)                                     AS campaignOffer
			, isnull(campanhas_online_st.activo,0)                                                      AS campaignStatus
			, convert(varchar, isnull(campanhas_online_st.dataInicio, '1900-01-01'), 23)                AS campaignStartDate
			, convert(varchar, isnull(campanhas_online_st.dataFim, '3000-01-01'), 23)                   AS campaignEndDate
			, isnull(st.portesGratis,0)                                                                 AS OnlineFreeShipping
			, lower(rtrim(ltrim(isnull(b_famFamilias.design,''))))                                      AS [type]
			, convert(decimal(20,2),round(isnull((1-(st.epcpond/
					CASE WHEN (st.epv1 / ((isnull(taxasiva.taxa,0) / 100) + 1)) = 0
					THEN 1 ELSE
					(st.epv1 / ((isnull(taxasiva.taxa,0) / 100) + 1))
					END
					))*100,0),2))                                                                       AS grossMarginPerc
			,ISNULL(st.fornec,0)                                                                        AS providerNumber
			,ISNULL(st.fornecedor,'')                                                                   AS providerDesignation  
			,ISNULL(st.fornestab,0)                                                                     AS providerDep
			,ISNULL(st.local,'')                                                                        AS localization 
			
			,ISNULL(st.usalote,0)																		AS useBatch
	
			                  
		FROM 
			st (nolock)
			full join fprod (nolock) on st.ref = fprod.cnp --and st.site_nr is null
			left join empresa (nolock) on empresa.no = st.site_nr
			left join taxasiva (nolock) on (st.tabiva = taxasiva.codigo or (fprod.u_tabiva = taxasiva.codigo and site_nr is null))
			left join campanhas_online_st(nolock) on campanhas_online_st.ref = st.ref and campanhas_online_st.site_nr =st.site_nr
			left join campanhas_online(nolock) on campanhas_online.idTipo = campanhas_online_st.idTipoCampanha
			left JOIN  grande_mercado_hmr (nolock) on st.u_depstamp = grande_mercado_hmr.id
			left JOIN  mercado_hmr (nolock) on st.u_secstamp= mercado_hmr.id
			left JOIN  categoria_hmr (nolock) on st.u_catstamp = categoria_hmr.id
			left JOIN  segmento_hmr (nolock) on st.u_segstamp= segmento_hmr.id
			left join  posologia (nolock) on posologia.ref=st.ref 
			left join  b_famFamilias (nolock)  on st.u_famstamp = b_famFamilias.famstamp
			left join  b_atcfp (nolock)  on st.ref = b_atcfp.cnp
			left join  B_labs lab (nolock)  on lab.id = fprod.titaim and lab.source = 'I' 

		where 
			(
				st.ref = @ref
				and st.site_nr = @siteno
			)
			or
			(
				fprod.cnp = @ref
				AND fprod.cnp not in (select ref from st (nolock) where st.site_nr = @siteno) 
			)

	end
	else 
	begin
		SELECT TOP 1 		
			  st.ref		
			, isnull(fprod.cnp, st.codCNP)																AS nhrn
			, convert(varchar(8),ISNULL(fprod.cnpem,''))												AS cnpem
			, isnull(@alterCode,'')																		AS alterCode
			, st.design																					AS designation
			, @site_name																				AS site
			, @siteno																					AS site_nr		
			, dbo.unityConverter(st.stock,'stocksUnityToBox', @site_nr ,st.ref)                         AS stockSite
			, isnull(dbo.unityConverter(isnull(st.stock,0) + isnull(st.qttfor,0) - 
					isnull(st.qttcli,0) - isnull(cativado,0),'stocksUnityToBox', @site_nr ,st.ref),0)	AS stockExpected
			, dbo.unityConverter(st.cativado,'stocksUnityToBox', @site_nr ,st.ref)   			        AS stockCaptivated
			, (select sum(dbo.unityConverter(st.stock,'stocksUnityToBox', @site_nr ,st.ref)  )
			 from st(nolock) where ref = @ref and site_nr in (select * from #dadosArmazens) ) 
																										AS stockCompany
			, (select sum(dbo.unityConverter(st.stock,'stocksUnityToBox',
			 @site_nr ,st.ref)) from st(nolock)  where ref = @ref)                                      AS stockGroup
			, dbo.unityConverter(st.stmax,'stocksUnityToBox', @site_nr ,st.ref)							AS stockMax
			, dbo.unityConverter(st.ptoenc,'stocksUnityToBox', @site_nr ,st.ref)						AS stockMin
			, dbo.unityConverter(st.qttfor,'stocksUnityToBox', @site_nr ,st.ref)						AS entrusted /*encomendas abertas*/
			, dbo.unityConverter(st.qttcli,'stocksUnityToBox', @site_nr ,st.ref)						AS booked  /*reservas abertas*/
			, convert(bit,1)                                                                            AS created  
			, convert(varchar, isnull(st.udata, '1900-01-01'), 23)										AS lastMoveDate
			, convert(varchar, isnull(st.usaid, '1900-01-01'), 23)										AS lastExitDate
			, convert(varchar, isnull(st.uintr, '1900-01-01'), 23)										AS lastEntryDate
			, dbo.unityConverter(isnull(st.epv1,0),'priceUnityToBox', @site_nr ,st.ref) 				AS retailprice /*pvp*/
			, dbo.unityConverter(isnull(st.marg4,0),'priceUnityToBox', @site_nr ,st.ref) 				AS grossMargin /*margin*/
			, @moeda																					AS currency
			, st.ivaincl																				AS vatInc
			, isnull(taxasiva.taxa, 0)																	AS vatPerc
			, dbo.unityConverter(isnull(st.epcusto,0), 'priceUnityToBox', @site_nr ,st.ref) 			AS pcl		
			, dbo.unityConverter(isnull(st.epcpond,0), 'priceUnityToBox', @site_nr ,st.ref) 			AS pcp
			, dbo.unityConverter(isnull(st.epcult,0),  'priceUnityToBox', @site_nr ,st.ref) 			AS pct
			, st.mfornec																				AS discountPerc
			, st.mfornec2																				AS discountValue
			, st.qttminvd																				AS qttMinSell 
			, st.qttembal																				AS qttUnEmb
			, isnull(st.precoSobConsulta,0)																AS priceOnRequest
			, convert(varchar, isnull(st.ousrdata, '1900-01-01'), 23)									AS OperationInfo_CreationDate		
			, st.ousrhora																				AS OperationInfo_CreationTime
			, convert(varchar, isnull(st.usrdata, '1900-01-01'), 23)									AS OperationInfo_LastChangeDate		
			, st.usrhora																				AS OperationInfo_LastChangeTime
			, st.usrinis																				AS OperationInfo_LastChangeUser
			, convert(varchar,isnull((select top 1 isnull(datalc,'1900-01-01') 
										from sl(nolock) 
										where ref=@ref 
										 and origem = 'FT'
										 and armazem = @siteno 
										order by datalc desc
										 ),
										 '1900-01-01'),23)                                              AS OperationInfo_LastFinanceMoveDate
			, convert(varchar, isnull(st.validade, '1900-01-01'), 23)									AS expirationDate
			, isnull(st.stns,0)																			AS service
			, isnull(u_servMarc,0)                                                                      AS isAppointment
			, IsNull(fprod.generico,0)																	AS generic
			, st.qlook																					AS voucher /*desconto*/
			, st.pbruto																					AS weight
			, st.peso																					AS weight1
			, st.unidade																				AS weightUnit		
			, st.qttesp																					AS width
			, st.volume																					AS volume
			, st.obs 
			, st.u_lab																					AS lab
			, st.usr1																					AS brand
			, st.familia																				AS familyCode
			, status = CASE WHEN isnull(st.inactivo,0) = 0 THEN 1 ELSE 0 END
			, profundidade																				AS depth
			, altura																					AS height
			,'cm'																						AS measureunit 
																									
			--provider																					
			, st.fornecedor																				AS providerDesignation
			, st.fornec																					AS providerNumber
			, st.fornestab																				AS providerDep

			--online: tudo a vazio
			, st.designOnline																			AS OnlineDesignation
			, (CASE WHEN st.dispOnline = 1 and  st.inactivo=0 then 0 else 1 end)						AS OnlineInactive
			, st.novidadeOnline																			AS OnlineNew
			--, 0																							AS OnlinePromotion
			, st.caractOnline																			AS OnlineDescription1
			, st.apresentOnline																			AS OnlineDescription2
			, ''																						AS OnlineDescription3
			, ISNULL((SELECT ISNULL(onlineImageURL,'') FROM #dadosImagens where rowNumber=1),'')		AS OnlineImage
			, ISNULL((SELECT ISNULL(onlineImageURL,'') FROM #dadosImagens where rowNumber=2),'')		AS OnlineImage1
			, ISNULL((SELECT ISNULL(onlineImageURL,'') FROM #dadosImagens where rowNumber=3),'')		AS OnlineImage2
			, ISNULL((SELECT ISNULL(onlineImageURL,'') FROM #dadosImagens where rowNumber=4),'')		AS OnlineImage3
			, ISNULL((SELECT ISNULL(onlineImageURL,'') FROM #dadosImagens where rowNumber=5),'')		AS OnlineImage4
			,st.tempoIndiponibilidade																	AS OnlineTimeUnavailability 
			,'dia'																						AS OnlineUnavailabilityUnit
																									
			--dicionario																				
			, fprod.design																				AS dicDesignation
			, fprod.cnp																					AS dicNhrn
			, fprod.descricao																			AS dicDescription
			, fprod.cnpem																				AS dicCnpem
			, fprod.generico																			AS dicGeneric
			, fprod.dosuni																				AS dicUnitDose
			, fprod.fformasdescr																		AS dicFormPharm		
			, fprod.grphmgcode																			AS dicgrpHmgCode
			, fprod.grphmgdescr																			AS dicgrpHmgDescr
			, fprod.estaimdescr																			AS dicTitAimState
			, fprod.tipembdescr																			AS dicEmbType
			, fprod.u_nomerc																			AS dicComercState
			, fprod.sitcomdescr																			AS dicComercDescr
			, fprod.u_tabiva																			AS dicVatPerc		
			, fprod.dci																					AS dicInn
			, fprod.pref																				AS dicRefPrice
			, fprod.pvporig																				AS dicRetailPrice
			, fprod.pvpmax																				AS dicRetailPriceMax
			, fprod.pvpmaxre																			AS dicRetailPriceMaxRe
			, fprod.preco_acordo																		AS dicRetailPriceAgreed
			, fprod.u_marca																				AS dicBrand
			, fprod.titaimdescr																			AS dicLab
			, isnull(lab.id,'0')																	    AS dicTitAimInfarmedCode
			, isnull(lab.descr,'')																		AS dicTitAimInfarmedDesign
			, case when ISNULL(fprod.u_familia,'')='' THEN  0 ELSE fprod.u_familia END 					AS dicFamilyCode
			, rtrim(ltrim(isnull(b_atcfp.atccode,'')))													AS dicAtcCode
			, 'EURO'																					AS dicCurrency
			, ISNULL(grande_mercado_hmr.id,'')													        AS dicBigMarketHmrId
			, ISNULL(grande_mercado_hmr.descr,'')														AS dicBigMarketHmrDescr
			, ISNULL(mercado_hmr.id,'')														            AS dicMarketHmrId
			, ISNULL(mercado_hmr.descr,'')																AS dicMarketHmrDescr
			, ISNULL(categoria_hmr.id,'')															    AS dicCategoryHmrId
			, ISNULL(categoria_hmr.descr,'')															AS dicCategoryHmrDescr
			, ISNULL(segmento_hmr.id,'')															    AS dicSegmentHmrId
			, ISNULL(segmento_hmr.descr,'')																AS dicSegmentHmrDescr
			,''																							AS dicLinkPdfRCM
			,''																							AS dicLinkPdfFl
			,rtrim(ltrim(ISNULL(posologia.posologia_orientativa,'')))									AS dicDosage
			,campaignUnit = case when isnull(campanhas_online_st.idTipoCampanha ,0) =1 then  @moeda    
					 when isnull(campanhas_online_st.idTipoCampanha ,0) = 2 then  '%' 
					 when isnull(campanhas_online_st.idTipoCampanha ,0) = 3 then  'qtt'		
					else '' end
			, isnull(campanhas_online_st.idTipoCampanha ,0)                                             AS campaignId
			, REPLACE(REPLACE(ltrim(rtrim(isnull(campanhas_online.descrTipo,''))),'Y',
			  convert(int,isnull(campanhas_online_st.valorCompra,0.00))),'X',
			  convert(int,isnull(campanhas_online_st.valorOferta,0.00)))                                AS campaignDesign
			, round(isnull(campanhas_online_st.valorCompra,0.00),2)                                     AS campaignBuy
			, round(isnull(campanhas_online_st.valorOferta,0.00),2)                                     AS campaignOffer
			, convert(varchar, isnull(campanhas_online_st.dataInicio, '1900-01-01'), 23)                AS campaignStartDate
			, convert(varchar, isnull(campanhas_online_st.dataFim, '3000-01-01'), 23)                   AS campaignEndDate
			, isnull(st.portesGratis,0)                                                                 AS OnlineFreeShipping
			, convert(decimal(20,2),round(isnull((1-(st.epcpond/
					CASE WHEN (st.epv1 / ((isnull(taxasiva.taxa,0) / 100) + 1)) = 0
					THEN 1 ELSE
					(st.epv1 / ((isnull(taxasiva.taxa,0) / 100) + 1))
					END
					))*100,0),2))                                                                       AS grossMarginPerc
			,ISNULL(st.fornec,0)                                                                        AS providerNumber
			,ISNULL(st.fornecedor,'')                                                                   AS providerDesignation  
			,ISNULL(st.fornestab,0)                                                                     AS providerDep 
			,ISNULL(st.local,'')                                                                        AS localization  
						
			,ISNULL(st.usalote,0)																		AS useBatch

		FROM 
			st (nolock)
			left join fprod (nolock) on (st.ref = fprod.cnp or st.codCNP=fprod.cnp)
			left join empresa (nolock) on empresa.no = st.site_nr
			left join taxasiva (nolock) on st.tabiva = taxasiva.codigo
			left join campanhas_online_st(nolock) on campanhas_online_st.ref = st.ref and campanhas_online_st.site_nr =st.site_nr
			left join campanhas_online(nolock) on campanhas_online.idTipo = campanhas_online_st.idTipoCampanha
			left JOIN  grande_mercado_hmr (nolock) on st.u_depstamp = grande_mercado_hmr.id
			left JOIN  mercado_hmr (nolock) on st.u_secstamp= mercado_hmr.id
			left JOIN  categoria_hmr (nolock) on st.u_catstamp = categoria_hmr.id
			left JOIN  segmento_hmr (nolock) on st.u_segstamp= segmento_hmr.id
			left join  posologia (nolock) on posologia.ref=st.ref
			left join  b_atcfp (nolock)  on st.ref = b_atcfp.cnp  
			left join  B_labs lab (nolock)  on lab.id = fprod.titaim and lab.source = 'I' 
		where 
			st.ref = @ref
			and st.site_nr = @siteno
	end	
		

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosArmazens'))
		DROP TABLE #dadosArmazens

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosImagens'))
		DROP TABLE #dadosImagens

END
ELSE
BEGIN
	print('Missing Ref and/or Site_name')
END


GO
Grant Execute On dbo.up_stocks_PesquisaProdutosRef to Public
Grant Control On dbo.up_stocks_PesquisaProdutosRef to Public
GO



