-- Recursos
-- exec up_stocks_PesquisaProdutosCompostos '5440987','Loja 1'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_PesquisaProdutosCompostos]') IS NOT NULL
	drop procedure up_stocks_PesquisaProdutosCompostos
go

create PROCEDURE up_stocks_PesquisaProdutosCompostos
@ref as varchar(18)
,@site as varchar(60)

/* WITH ENCRYPTION */

AS

	Declare @sitenr int = 0

	Select @sitenr = empresa.no
	From empresa(nolock)
	Where empresa.site = @site

	Select 
		ROUND(st_componentes.qtt,2)												AS compoundInfo_quantity
		,st_componentes.ref														AS compoundInfo_ref
		,st.design																AS compoundInfo_design
		,ROUND(st.qttminvd,2)													AS compoundInfo_quantityMinSell
		,st.unidade																AS compoundInfo_unity
		,ROUND(st.conversao,2)													AS compoundInfo_converting
		,ROUND(st.qttembal,2)													AS compoundInfo_quantityPacking
		
	from 
		st_componentes (nolock) 
		inner join st (nolock) on st_componentes.ref = st.ref
	where 
		RTRIM(LTRIM(st.ref)) = RTRIM(LTRIM(@ref))
		and st.site_nr = @sitenr
		
GO
Grant Execute On up_stocks_PesquisaProdutosCompostos to Public
grant control on up_stocks_PesquisaProdutosCompostos to Public
go	