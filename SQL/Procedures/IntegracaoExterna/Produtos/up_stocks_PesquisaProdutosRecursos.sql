-- Recursos
-- exec up_stocks_PesquisaProdutosRecursos '5440987','Loja 1'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_PesquisaProdutosRecursos]') IS NOT NULL
	drop procedure up_stocks_PesquisaProdutosRecursos
go

create PROCEDURE up_stocks_PesquisaProdutosRecursos
@ref as varchar(18)
,@site as varchar(60) = ''

/* WITH ENCRYPTION */

AS

	Declare @sitenr int = 0
	Select @sitenr = no from empresa(nolock)
		where site = @site

	Select 
		b_cli_stRecursos.tipo												AS resourceInfo_type
		,b_cli_stRecursos.nome												AS resourceInfo_name
		,ROUND(b_cli_stRecursos.pvp,2)										AS resourceInfo_pvp
		,ROUND(b_cli_stRecursos.duracao,2)									AS resourceInfo_duration
		,b_cli_stRecursos.mrsimultaneo										AS resourceInfo_numSimulApointments
		,ROUND(b_stst.duracao,2)											AS resourceInfo_durationMin
		,b_cli_stRecursos.marcacao											AS resourceInfo_onlyAppointment
	from 
		b_cli_stRecursos (nolock)
		inner join b_stst (nolock) on b_stst.ref = b_cli_stRecursos.ref
	where 
		RTRIM(LTRIM(b_cli_stRecursos.ref)) = RTRIM(LTRIM(@ref))
		and b_cli_stRecursos.site_nr = @sitenr

	
GO
Grant Execute On up_stocks_PesquisaProdutosRecursos to Public
grant control on up_stocks_PesquisaProdutosRecursos to Public
go	