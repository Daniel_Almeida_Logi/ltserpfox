
/****** Object:  UserDefinedFunction [dbo].[unityConverter]    Script Date: 18/03/2020 14:54:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[unityConverter]') IS NOT NULL
    DROP FUNCTION [dbo].unityConverter
GO

-- =============================================
-- Author:		<DA>
-- Create date: <16-07-2020>
-- Description:	<Conversão de comprimidos para caixas em quantidades e preços>
-- tipo  stocksUnityToBox - stocks comprimidos para caixa
-- tipo  priceUnityToBox -  preços comprimidos para caixa
-- tipo  stocksBoxToUnity - stocks caixa para comprimidos
-- tipo  priceBoxToUnity -  preços caixa para comprimidos
-- tipo  qttBoxToUnity -    qtt caixa para comprimidos
-- tipo  qttUnityToBox -    qtt comprimidos para caixa
--
-- =============================================
CREATE FUNCTION [dbo].[unityConverter] 
(	
	@value numeric(16,3),
	@type varchar(100),
	@site_nr int,
	@ref varchar(18) = ''

)
RETURNS numeric(16,3)
AS
BEGIN

	set @type=ltrim(rtrim(@type))
	declare @stkComp as bit = 0
	declare @service bit = 0
	declare @qttemb int = 0

		

	set @stkComp = (select bool from B_Parameters_site (nolock) where stamp='ADM0000000067' and site=(select site from empresa(nolock) where no=@site_nr))




	select top 1 @qttemb = isnull(st.qttembal,1), @service=isnull(st.stns,1) from st(nolock) where ref=@ref and site_nr = @site_nr

	/*   tipo  - stocks comprimidos para caixa */
	if(@type = 'stocksUnityToBox') 
	begin
		set @value =  case when @stkComp=0 then (case when @service= 0 then @value else 0 end) else (case when @service = 0 and @qttemb>0 then round(isnull(@value,0)/@qttemb,2) else 0 end) end
		return convert(numeric(12,2),@value)
	end



	/*  tipo  - stocks comprimidos para caixa */
	if(@type = 'priceUnityToBox') 
	begin
		set @value =  case when @stkComp=0 then @value else (case when @service = 0 and @qttemb>0 then round(isnull(@value,0)*@qttemb,2) else @value end) end
		return @value
	end




		/*   tipo  - qtt comprimidos para caixa */
	if(@type = 'qttUnityToBox') 
	begin
		set @value =  case when @stkComp=0 then @value else (case when @qttemb>0 then round(isnull(@value,0)/@qttemb,2) else @value end) end
		return convert(int,@value)
	end




	
	/*   tipo - stocks caixa para comprimidos*/
	if(@type = 'stocksBoxToUnity') 
	begin
		set @value =  case when @stkComp=0 then (case when @service= 0 then @value else 0 end) else (case when @service = 0 and @qttemb>0 then round(isnull(@value,0)*@qttemb,2) else 0 end) end
		return convert(int,@value)
	end



	/*  tipo - preços caixa para comprimidos */
	if(@type = 'priceBoxToUnity') 
	begin
		set @value =  case when @stkComp=0 then @value else (case when @service = 0 and @qttemb>0 then round(isnull(@value,0)/@qttemb,2) else @value end) end
		return @value
	end


	/*   tipo - qtt caixa para comprimidos*/
	if(@type = 'qttBoxToUnity') 
	begin
		set @value =  case when @stkComp=0 then @value else (case when  @qttemb>0 then round(isnull(@value,0)*@qttemb,2) else @value end) end
		return convert(int,@value)
	end







	return @value

END
GO


