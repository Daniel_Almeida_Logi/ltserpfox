/* SP Pesquisa productos alteranativos
2022-02-24, DA

Variaveis de Input (ambas obrigatórias):
--------------------
@ref		
@site_name


Variaveis de Ouput:
--------------------
ref




Execução da SP:
----------------

	 exec up_stocks_PesquisaRefAlternativos '5440987' , 'loja 1'

		
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_PesquisaRefAlternativos]') IS NOT NULL
	drop procedure dbo.up_stocks_PesquisaRefAlternativos
go

CREATE PROCEDURE dbo.up_stocks_PesquisaRefAlternativos
@ref				VARCHAR(28),
@site_name			VARCHAR(18)



AS

SET @ref			=   rtrim(ltrim(@ref))
SET @site_name		=   rtrim(ltrim(@site_name))

 

IF (len(@ref) > 0  and len(@site_name) > 0)
BEGIN


	-- variaveis auxiliares 		
	DECLARE @siteno AS tinyint
	SET @siteno = (select  no from empresa(nolock) where site = @site_name)	
	
	select 
		codigo
	from 
		bc(nolock)
	where ref = @ref and site_nr = @siteno
	order by usrdata desc
	


END
ELSE
BEGIN
	select '' as codigo
END


GO
Grant Execute On dbo.up_stocks_PesquisaRefAlternativos to Public
Grant Control On dbo.up_stocks_PesquisaRefAlternativos to Public
GO



