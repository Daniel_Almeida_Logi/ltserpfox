/* SP Pesquisa de Produtos por Referencia
2020-11-13, DA

Variaveis de Input (ambas obrigatórias):
--------------------
@ref		
@site_name
@topSellers         
@topSellersRange    
@topMax             


Variaveis de Ouput:
--------------------
 ref


Execução da SP:
----------------


	 exec up_stocks_PesquisaProdutosRefRelated '5440987' , 'loja 1', null, null, null
	 exec up_stocks_PesquisaProdutosRefRelated '6932590' , 'loja 1',1,12,20

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_PesquisaProdutosRefRelated]') IS NOT NULL
	drop procedure dbo.up_stocks_PesquisaProdutosRefRelated
go

CREATE PROCEDURE dbo.up_stocks_PesquisaProdutosRefRelated
@ref				VARCHAR(28),
@site_name			VARCHAR(18),
@topSellers         bit =0,
@topSellersRange    int = 6,
@topMax             int = 20


AS

SET @ref			    =   rtrim(ltrim(@ref))
SET @site_name		    =   rtrim(ltrim(@site_name))
SET @topSellers         =	ISNULL(@topSellers,0)
SET @topSellersRange    =	ISNULL(@topSellersRange,6)
SET @topMax             =	ISNULL(@topMax,20)

SET @topSellersRange =  @topSellersRange*-1


DECLARE @sql varchar(max) =''
 

IF len(@ref) > 0 and len(@site_name) > 0
BEGIN
	
	SET NOCOUNT ON		

	-- variaveis auxiliares 		
	DECLARE @siteno AS tinyint
	SET @siteno = (select  no from empresa(nolock) where site = @site_name)	

	set @sql = '
	select top ' +  convert(varchar(10),@topMax)   + ' fii.ref as ref from fi fii (nolock) 	' +
	' where fii.ftstamp in 
	( 
		select 
			distinct fi.ftstamp
		from 
			fi(nolock)
		inner join 
			st(nolock) on fi.ref=st.ref and st.site_nr = ' + convert(varchar(10),@siteno) + ' and ' + ' fi.armazem = ' + convert(varchar(10),@siteno) + ' 
		 where 	
			st.dispOnline = 1 
			and fi.tipodoc = 1
			and fi.rdata>=dateadd(Month,'+convert(varchar(10),@topSellersRange)+',GETDATE())
			and fi.ref = ''' + @ref + '''
		
	) and fii.ref<>''' + @ref + '''
	and fii.rdata>=dateadd(Month,'+convert(varchar(10),@topSellersRange)+',GETDATE()) 
	

	'
	

	--print @sql
	EXECUTE (@sql)




END
ELSE
BEGIN
	select '' as ref
END


GO
Grant Execute On dbo.up_stocks_PesquisaProdutosRefRelated to Public
Grant Control On dbo.up_stocks_PesquisaProdutosRefRelated to Public
GO
