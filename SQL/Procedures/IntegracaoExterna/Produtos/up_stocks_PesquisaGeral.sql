/* SP Pesquisa de Produtos Geral
2020-04-06, JG


Variaveis de Input:
--------------------
@artigo 	permite pesquisa %ref/cnp/cnpem/designacao%		valores possíveis '' ou '%xxx%'
@lab		permite pesquisa @lab							valores possíveis '' ou 'xxx'
@marca		permite pesquisa @marca							valores possíveis '' ou 'xxx'
@dci		permite pesquisa @dci%							valores possíveis '' ou 'xxx%'
@servicos	permite pesquisa por 1 ou 2						valores possíveis '' ou 1 ou 2
@generico	permite pesquisa por 0 ou 1						valores possíveis '' ou 0 ou 1
@site_name  nome da loja
@lastchangedate		mostra dados => desta data
@lastchangetime		mostra dados => desta hora

Variáveis Obrigatórias:
-----------------------
@site_name




Variaveis de Ouput:
--------------------
designation,nhrn, ref, cnpem, stock AS stockSite, stockCompany			
, epv1 AS retailPrice, IVAincl AS vatInc, IVA AS vatPerc, marg4 AS grossMarg
, moeda AS currency, obs, validade AS expirationDate, CreationDate, LastChangeDate, LastChangeTime


Execução da SP:
----------------
exec up_stocks_PesquisaGeral '@artigo','@lab','@marca','@dci',@servicos, @generico,'@site_name', '@LastChangeDate', '@LastChangeTime', '@pageNumber','@class_hmr'

exec up_stocks_PesquisaGeral '7393702',NULL,'','',NULL, NULL,'Loja 1','2022-09-10','18:35:00',NULL,''

exec up_stocks_PesquisaGeral NULL,NULL,'','',NULL, NULL,'Atlantico','2020-07-23','22:35:00',1,''

sele3ct *rom st(m)


exec up_stocks_PesquisaGeral NULL,NULL,NULL,NULL,NULL, NULL,'Loja 1','2021-12-21','16:00:00',215,NULL,NULL,NULL,NULL,NULL,NULL,NULL ,NULL, NULL, NULL, ''

exec up_stocks_PesquisaGeral NULL,NULL,NULL,NULL,NULL, NULL,'Loja 1','2021-12-21','16:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL ,NULL, NULL, NULL, ''

exec up_stocks_PesquisaGeral NULL,NULL,NULL,NULL,NULL, NULL,'Loja 1','1900-01-01','00:00:00',1,null,NULL,null,null,'23615EA9-A668-4D24-AA51-C00FBA36D359'
exec up_stocks_PesquisaGeral 'Ben-U-Ron',NULL,NULL,NULL,NULL, NULL,'Loja 1','1900-01-01','00:00:00',1,NULL,'23615EA9-A668-4D24-AA51-C00FBA36D359'

select stock,epv1,qttembal,ref from st where  site_nr=1 and inactivo = 0 and qttembal>1 and ref='000023'

EXEC up_stocks_PesquisaGeral '',NULL,NULL,NULL,NULL,NULL,N'Loja 1',N'2020-09-18',NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,100,NULL,NULL,1





select bool from B_Parameters_site (nolock) where stamp='ADM0000000067'


*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_stocks_PesquisaGeral]') IS NOT NULL
    DROP procedure dbo.up_stocks_PesquisaGeral
GO

CREATE procedure dbo.up_stocks_PesquisaGeral

	 @artigo						VARCHAR(120)
	,@lab							VARCHAR(150)
	,@marca							VARCHAR(200)
	,@dci							VARCHAR(254)
	,@servicos						int
	,@generico						int 
	,@site_name						VARCHAR(18)
	,@lastChangeDate				datetime
	,@lastChangeTime				varchar(8)
	,@pageNumber					int = 1
	,@id_grande_mercado_hmr         int = null
	,@id_mercado_hmr                int = null
	,@id_categoria_hmr              int = null
	,@id_segmento_mercado_hmr       int = null
	,@orderStamp					VARCHAR(36) = ''
	,@topSellers                    bit =0
	,@topSellersRange               int =6
	,@onlyOnline                    bit = 0
	,@topMax                        int = 100
	,@refs                          VARCHAR(MAX) = ''
	,@tipo                          VARCHAR(254) = ''
	,@onlyActive					bit = 0
	,@onlyAppointment               bit = 0
	,@pvpMin						decimal(19,6) = null
	,@pvpMax						decimal(19,6) = null
	,@onlyNew						bit = 0
	,@onlyDestak                    bit = 0
	,@listBigMarketId				VARCHAR(MAX) = ''

/* WITH ENCRYPTION */
AS

SET @artigo			=   rtrim(ltrim(isnull(@artigo,'')))
SET @lab			=   rtrim(ltrim(isnull(@lab,'')))
SET @marca			=   rtrim(ltrim(isnull(@marca,'')))
SET @dci			=   rtrim(ltrim(isnull(@dci,'')))
SET @servicos		=   rtrim(ltrim(isnull(@servicos,-1)))
SET @generico		=   rtrim(ltrim(isnull(@generico,-1)))
SET @site_name		=   rtrim(ltrim(@site_name))
SET @lastChangeDate =	rtrim(ltrim(isnull(@lastChangeDate,'19000101')))
SET @lastChangeTime =	rtrim(ltrim(isnull(@lastChangeTime,'00:00:00')))
 
DECLARE @lastChangeDateTime VARCHAR(20)
SET @lastChangeDate = ltrim(rtrim(isnull(@lastChangeDate,'19000101')))
SET @lastChangeTime = ltrim(rtrim(isnull(@lastChangeTime,'00:00:00')))
SET @lastChangeDateTime = convert(datetime,@lastChangeDate + @lastChangeTime)
SET @orderStamp         =	rtrim(ltrim(ISNULL(@orderStamp,'')))
SET @topSellers         =	ISNULL(@topSellers,0)
SET @topSellersRange    =	ISNULL(@topSellersRange,6)
SET @onlyOnline         =	ISNULL(@onlyOnline,0)
SET @refs				=	rtrim(ltrim(ISNULL(@refs,'')))
SET @tipo				=	lower(rtrim(ltrim(ISNULL(@tipo,''))))
Set @onlyActive			=	ISNULL(@onlyActive,0)
Set @onlyAppointment	=	ISNULL(@onlyAppointment,0)
Set @pvpMin				=	ISNULL(@pvpMin,-9999999)
Set @pvpMax				=	ISNULL(@pvpMax, 9999999)
Set @onlyNew			=	ISNULL(@onlyNew,0)
Set @onlyDestak			=	ISNULL(@onlyDestak,0)
SET @listBigMarketId	=	rtrim(ltrim(ISNULL(@listBigMarketId,'')))



SET @topSellersRange =  @topSellersRange*-1


if(isnull(@topMax,0)>1000)
set @topMax = 1000

if(isnull(@topMax,0)<=0)
set @topMax = 100

DECLARE @PageSize int = isnull(@topMax,100)
DECLARE @orderBy VARCHAR(8000)
DECLARE @sql varchar(max)
DECLARE @sqlFiColumns varchar(max) = ''
DECLARE @sqlResultFinal varchar(max) = ''


if(isnull(@pageNumber,0)<1)
	set @pageNumber = 1


IF len(@site_name) > 0 
BEGIN

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosArmazens'))
		DROP TABLE #dadosArmazens	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt2'))
		DROP TABLE #dadosSt2	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt3'))
		DROP TABLE #dadosSt3		

	/* variaveis auxiliares */		
	declare @site_nr AS tinyint
	SET @site_nr = (select  no from empresa(nolock) where site = @site_name)

	declare @stkcaixa as bit
	set @stkcaixa = (select bool from B_Parameters_site(nolock) where stamp='ADM0000000067' and site=(select site from empresa(nolock) where no=@site_nr))

	declare @moeda AS varchar(10)
	SET @moeda = (Select textValue from B_Parameters(nolock) where stamp='ADM0000000260')
	   
	DECLARE @OrderCri VARCHAR(MAX)=''


	IF @orderStamp = ''
	BEGIN 
		SET @OrderCri = 'ORDER BY usrdata asc, usrhora asc, ref asc'
	END 
	ELSE 
	BEGIN 
		SELECT @OrderCri='ORDER BY ' + instructionSql  FROM sortingTableOrder (NOLOCK) WHERE stamp = @orderStamp and inativo=0 and nameService='productSearch'
	END 



	/* valida o mais vendidos*/
	if(isnull(@topSellers,0) = 1)
	begin
		set @OrderCri = N' 
			order by qttSold desc '
		set @sqlFiColumns = N' , qttSold = (select isnull(sum(fi.qtt),0) from fi(nolock)
							where 
							fi.rdata>=dateadd(Month,'+convert(varchar(10),@topSellersRange)+',GETDATE()) AND 
							fi.tipodoc=1                          AND
							fi.stns = 0                           AND
							fi.ref not like ''''                  AND
							fi.ref not like ''V%''                AND 
							fi.ref not like ''R%''                AND
							fi.ref not like ''Z%''                AND
							fi.ref = st2.ref					  AND
							fi.armazem=st2.site_nr
							) '
	end


	set @orderBy = ' ) X ' + @OrderCri +'  
				 OFFSET ' + convert(varchar(10),@PageSize) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
				 FETCH NEXT  ' + convert(varchar(10),@PageSize) + ' ROWS ONLY '


	/* Armazens disponiveis por empresa */
	select empresa_no
	into #dadosArmazens
	from empresa_arm (nolock)
	inner join 
		empresa on empresa_arm.empresa_no = empresa.no 
	where
		empresa.ncont = (select ncont from empresa where no = @site_nr)


		

	

	/* Temp Table para permitir usar If statement */
	create table #dadosSt2 (	
		ref						 varchar(18) COLLATE SQL_Latin1_General_CP1_CI_AI
		, nhrn					 varchar(18) /*cnp*/		
		,design					 varchar(120) COLLATE SQL_Latin1_General_CP1_CI_AI		
		,stock					 numeric(13,3)
		,epv1					 numeric(19,6)
		,validade				 varchar(10)				
		,generico				 bit			
		,cnpem					 varchar(8)
		,marg4					 numeric(16,3)		
		,u_lab					 varchar(150)
		,usr1					 varchar(200)
		,stns					 bit 
		,inactivo				 bit
		,dci					 varchar(254)
		,site					 varchar(18)
		, site_nr				 tinyint		
		,obs					 varchar(254)
		,marcada				 bit
		,iva					 numeric(5,2)
		, IVAincl				 bit
		, moeda					 varchar(10)		
		, qttembal  			 numeric(20,2)	
		, qttminvd  			 numeric(20,2)		
		, CreationDate		     datetime
		, CreationTime			 varchar(8) 
		, LastChangeDate		 datetime
		, Lastchangetime		 varchar(8)
		, lastChangeUser		 VARCHAR(30)
		, usrdata				 datetime
		, usrhora			     varchar(8) 
		, cativado			     numeric(13,3)
		, dicBigMarketHmrId      varchar(8)
		, dicBigMarketHmrDescr   varchar(254)
		, codCnp                 varchar(18)
		, tempoIndiponibilidade  numeric(10,2)
		, dicDescription         varchar(254)
		, dicDesignation         varchar(100)
		, dicCnpem               varchar(8)
		, [type]                 varchar(254)
		, u_servmarc             bit
		, previsto               numeric(13,3)

		) 
	
	/* Result Set Para Pesquisas Feitas Apenas a Produtos com Ficha */
	/* Dados do Produto */
	insert into #dadosSt2
	Select 		
		isnull(st.ref, fprod.cnp)
		, isnull(fprod.cnp, st.codCNP)
		, ltrim(rtrim(isnull(st.design,fprod.design)))			
		, stock 
		, epv1 
		, isnull(convert(varchar,validade,23), '1900-01-01')				
		, IsNull(fprod.generico,0)				
		, convert(varchar(8),ISNULL(fprod.cnpem,''))
		, marg4 	
		, ISNULL(st.u_lab,fprod.titaimdescr)
		, left(ISNULL(st.usr1,fprod.u_marca),20)
		, stns
		, status = case when st.inactivo = 0 then 1 else 0 end
		, dci = ISNULL(dci,'')
		, site
		, st.site_nr			
		, isnull(st.obs,'')
		, st.marcada
		, isnull(taxasiva.taxa, 0)
		, st.ivaincl
		, @moeda			
		, qttembal  
		, qttminvd			
		, isnull(st.ousrdata, '1900-01-01')
		, isnull(st.ousrhora, '00:00:00')
		, isnull(st.usrdata, '1900-01-01')	 
		, isnull(st.usrhora, '00:00:00')   
		, ISNULL(st.usrinis,'')	
		, st.usrdata
		, st.usrhora
		, cativado
		, ISNULL(grande_mercado_hmr.id,'')
		, rtrim(ltrim(ISNULL(grande_mercado_hmr.descr,'')))
		, rtrim(ltrim(ISNULL(st.codCNP,'')))
		, isnull(st.tempoIndiponibilidade,0)
		, rtrim(ltrim(ISNULL(fprod.descricao,'')))
		, rtrim(ltrim(ISNULL(fprod.design,'')))
		, rtrim(ltrim(ISNULL(fprod.cnpem,'')))
		, [type] =  lower(rtrim(ltrim(isnull(b_famFamilias.design,''))))
		, isnull(u_servmarc,0)
		, isnull(st.stock,0) + isnull(st.qttfor,0) - isnull(st.qttcli,0) - isnull(cativado,0)
		 
	From 
		st (nolock)
		left join fprod (nolock) on   (st.ref = fprod.cnp or st.codCNP=fprod.cnp)
		left join cptgrp (nolock) on cptgrp.grupo = fprod.grupo
		left join empresa (nolock) on st.site_nr = empresa.no
		left join taxasiva (nolock) on st.tabiva = taxasiva.codigo
		left JOIN  grande_mercado_hmr (nolock) on st.u_depstamp = grande_mercado_hmr.id
		left join b_famFamilias (nolock) on st.u_famstamp = b_famFamilias.famstamp
		left join campanhas_online_st(nolock) on campanhas_online_st.ref = st.ref and campanhas_online_st.site_nr =st.site_nr
		left join campanhas_online(nolock) on campanhas_online.idTipo = campanhas_online_st.idTipoCampanha
	Where 
		st.site_nr = @site_nr
		AND (
			st.ref like @artigo
			OR exists (Select ref from bc (nolock) where bc.ref = st.ref and bc.site_nr = @site_nr and bc.codigo = @artigo)  
			OR st.design LIKE '%' + @artigo + '%'
			OR fprod.cnpem = @artigo
			OR fprod.cnpem_old = @artigo
			OR st.codCNP like @artigo
			)		
		
	    AND convert(datetime,convert(varchar(10),st.usrdata,120)+' ' + st.usrhora) >= @lastChangeDateTime
		and isnull(st.u_depstamp,0)     =  case when isnull(@id_grande_mercado_hmr,0)=0  then  isnull(st.u_depstamp,0) else @id_grande_mercado_hmr end
		and isnull(st.u_catstamp,0)     =  case when isnull(@id_categoria_hmr,0)=0  then  isnull(st.u_catstamp,0) else @id_categoria_hmr end
		and isnull(st.u_segstamp,0)     =  case when isnull(@id_segmento_mercado_hmr,0)=0  then  isnull(st.u_segstamp,0) else @id_segmento_mercado_hmr end
		and isnull(st.dispOnline,0)     =  case when isnull(@onlyOnline,0)=0 then isnull(st.dispOnline,0) else 1 end
		and isnull(st.inactivo,1)       =  case when isnull(@onlyOnline,0)=0 then isnull(st.inactivo,1) else 0 end
		and isnull(st.inactivo,1)       =  case when isnull(@onlyActive,0)=0 then isnull(st.inactivo,1) else 0 end
		and isnull(st.novidadeOnline,0) =  case when isnull(@onlyNew,0)=0 then isnull(st.novidadeOnline,0) else 1 end
		and isnull(st.u_servmarc,1)     =  case when isnull(@onlyAppointment,0)=0 then isnull(st.u_servmarc,1) else 1 end
		and rtrim(ltrim(isnull(b_famFamilias.design,''))) =  case when isnull(@tipo,'')!='' then isnull(@tipo,'') else rtrim(ltrim(isnull(b_famFamilias.design,''))) end
		and st.epv1>=@pvpMin and st.epv1<=@pvpMax
		AND ((@onlyDestak = 1 AND ISNULL(campanhas_online_st.idTipoCampanha, 0) IN (1, 2, 3)) OR (@onlyDestak = 0  AND ISNULL(campanhas_online_st.idTipoCampanha, 0) = ISNULL(campanhas_online_st.idTipoCampanha, 0)))

	/* Tratamento de Dados para Result Set - Eliminar Tabela Temp. parece ter melhor performance do que colocar filtros na query*/

	if @lab != ''
		delete from #dadosSt2 where u_lab != @lab 
	if @marca != ''
		delete from #dadosSt2 where usr1 != @marca
	if @dci != ''
		delete from #dadosSt2 where isnull(dci,'''') not like @dci +'%'
	if @servicos = 1
		delete from #dadosSt2 where stns != 1
	if @servicos = 0
		delete from #dadosSt2 where stns = 1
	if @generico = 1
		delete from #dadosSt2 where generico != 1
	if @generico = 0
		delete from #dadosSt2 where generico != 0
	if(@refs!='')
		delete from #dadosSt2 where ref not in (select value from dbo.splitstring(@refs, ','))
	if(@listBigMarketId!='')
		delete from #dadosSt2 where dicBigMarketHmrId not in (select value from dbo.splitstring(@listBigMarketId, ','))

	



    
	set @sqlResultFinal = '

			SELECT
			    st3.pageSize
				,st3.pageTotal
				,st3.pageNumber
				,st3.linesTotal
				,st2.design AS designation, 	st2.nhrn, st2.ref, st2.cnpem
				, dbo.unityConverter(st2.stock,''stocksUnityToBox'', ' + convert(varchar(10),@site_nr) + ',st2.ref)	AS stockSite			
				, dbo.unityConverter(st3.StockEmpresa,''stocksUnityToBox'', ' + convert(varchar(10),@site_nr) + ',st3.ref)  AS stockCompany
				, dbo.unityConverter(st3.StockGrupo,''stocksUnityToBox'', ' + convert(varchar(10),@site_nr) + ',st3.ref)  AS stockGroup
				, dbo.unityConverter(st2.previsto,''stocksUnityToBox'', ' + convert(varchar(10),@site_nr) + ',st2.ref)  AS stockExpected
				, dbo.unityConverter(isnull(st2.epv1,0),''priceUnityToBox'',' + convert(varchar(10),@site_nr) + ' ,st2.ref)    AS retailPrice
				, st2.IVAincl AS vatInc, st2.IVA AS vatPerc
				, dbo.unityConverter(isnull(st2.marg4,0),''priceUnityToBox'', ' + convert(varchar(10),@site_nr) + ' ,st2.ref)  	 AS grossMarg
				, st2.moeda AS currency, st2.obs, st2.validade AS expirationDate
				, convert(varchar, st2.CreationDate, 23) AS OperationInfo_CreationDate 
				,  CreationTime  AS OperationInfo_CreationTime
				, convert(varchar, st2.LastChangeDate, 23) AS OperationInfo_LastChangeDate 
				,  st2.LastChangeTime AS  OperationInfo_LastChangeTime
				, st2.lastChangeUser  AS  OperationInfo_lastChangeUser
				, st2.inactivo AS status
				, st2.qttminvd AS qttMinSell 
				, st2.qttembal AS qttUnEmb
				, st2.usrdata
				, st2.usrhora	
				, st2.marcada
				, st2.stock
				, dbo.unityConverter(st2.cativado,''stocksUnityToBox'', ' + convert(varchar(10),@site_nr) + ' ,st2.ref)  as stockCaptivated
				, st2.dicBigMarketHmrId as dicBigMarketHmrId
				, st2.dicBigMarketHmrDescr  as dicBigMarketHmrDescr
				, st2.tempoIndiponibilidade		AS OnlineTimeUnavailability 
				,''dia''	AS OnlineUnavailabilityUnit
				, ''1''  as created
				, st2.dicDescription
				, st2.dicDesignation  
				, st2.dicCnpem
				, st2.nhrn as dicNhrn
				, st2.[type] as [type]
				, st2.[u_servMarc] as [isAppointment]
				, ''alter_codes'' = isnull((select  rtrim(ltrim(codigo)) + '';'' from bc(nolock) where bc.ref=st3.ref and bc.site_nr=st3.site_nr FOR XML PATH('''')),'''')			
			From
				#dadosSt3 st3	
			inner join
				#dadosSt2 st2 on st2.ref = 	st3.ref and 	st2.site_nr = st3.site_nr

			
			' + @OrderCri



		select @sql = N' SELECT '  + convert(varchar(10),@PageSize) +' as pageSize,  (COUNT(*) OVER() / ' +  convert(varchar(10),@PageSize) +') + 1 as pageTotal, '
				+ convert(varchar(10),@PageNumber) +' as pageNumber, COUNT(*) OVER() as linesTotal		
			,* 		into
				#dadosSt3 	 
					
			from (SELECT
					st2.ref
					,st2.site_nr
					,st2.usrdata
				    ,st2.usrhora
					,st2.marcada
					,st2.stock
					,y.StockGrupo
					,x.StockEmpresa					
					' + @sqlFiColumns+ '	
					
			From
				#dadosSt2 st2			

			left outer join (select ref, StockEmpresa = sum(stock) 
							from st (nolock)
							where 
								site_nr in (select * from #dadosArmazens)
								and stns = 0
							group by 
								ref) x on st2.ref = x.ref
			left outer join (select ref, StockGrupo = sum(stock) 
							from st (nolock)
							where 
								stns = 0
							group by 
								ref) y on st2.ref = y.ref


		'
		select @sql = @sql  + @orderBy +  @sqlResultFinal

	
PRINT convert(varchar(10), GETDATE(), 108)
		print @sql
		EXECUTE (@sql)
PRINT convert(varchar(10), GETDATE(), 108)
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosArmazens'))
		DROP TABLE #dadosArmazens	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt2'))
		DROP TABLE #dadosSt2	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt3'))
		DROP TABLE #dadosSt3	
END
ELSE
BEGIN
	print('Missing Site_name')
END

GO
GRANT EXECUTE on dbo.up_stocks_PesquisaGeral TO PUBLIC
GRANT Control on dbo.up_stocks_PesquisaGeral TO PUBLIC
GO