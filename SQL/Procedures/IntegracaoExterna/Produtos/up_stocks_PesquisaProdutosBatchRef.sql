/* SP Pesquisa de Lotes de Produtos por Referencia
2020-04-13, JG

Variaveis de Input (ambas obrigatórias):
--------------------
@ref		


Variaveis de Ouput:
--------------------
 ref,
, batchWarehouse, batchDesignation, batchStock, batchExpirationDate
, batchLastChangeDate, batchLastChangeTime, batchCreationDate,batchcreationTime
, batchCurrency, batchRetailPrice

dados do lote:
	 exec up_ga_lotePorRef '5440987'


Execução da SP:
----------------
	up_stocks_PesquisaProdutosBatchRef '@ref' , '@site_name'

	 exec up_stocks_PesquisaProdutosBatchRef '2494185','Loja 1' 
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_stocks_PesquisaProdutosBatchRef]') IS NOT NULL
	drop procedure dbo.up_stocks_PesquisaProdutosBatchRef
go

CREATE PROCEDURE dbo.up_stocks_PesquisaProdutosBatchRef
@ref			VARCHAR(28),
@site			VARCHAR(60) = ''

AS

SET @ref		=   rtrim(ltrim(@ref))

IF len(@ref) > 0 
BEGIN
	SET NOCOUNT ON		
	
	DECLARE @moeda AS varchar(10)
	SET @moeda = (Select textValue from B_Parameters(nolock) where stamp='ADM0000000260')

	-- Lotes (batch)
	select 
		  ref 
		, lote AS batchDesignation
		, convert(varchar,convert(numeric(13,0),stock)) AS batchStock
		, LEFT(convert(date,validade),7)  AS batchExpirationDate
		, convert(varchar,convert(date, st_lotes.usrdata)) AS batchLastChangeDate
		, convert(varchar,convert(date, st_lotes.usrhora)) AS batchLastChangeTime
		, convert(varchar,convert(date, st_lotes.ousrdata)) AS batchCreationDate
		, convert(varchar,convert(date, st_lotes.ousrhora)) AS batchCreationTime
		, st_lotes.usrinis AS batchLastChangeUser
		, isnull(epv1,0) AS batchRetailPrice
		, @moeda AS batchCurrency	
		, armazem AS batchWarehouse
	--into #dadoslote
	from 
		st_lotes (nolock)
		left join st_lotes_precos (nolock) on st_lotes.stamp = st_lotes_precos.stampLote 
	where ref = @ref
		and  st_lotes.armazem in (select armazem from empresa(nolock) inner join empresa_arm(nolock) on empresa_arm.empresa_no = empresa.no where empresa.site = @site ) 
	order by 
		st_lotes.usrdata + st_lotes.usrhora desc
		,st_lotes.lote
		,st_lotes_precos.usrdata + st_lotes_precos.usrhora desc

END
ELSE
BEGIN
	print('Missing Ref')
END


GO
Grant Execute On dbo.up_stocks_PesquisaProdutosBatchRef to Public
Grant Control On dbo.up_stocks_PesquisaProdutosBatchRef to Public
GO
