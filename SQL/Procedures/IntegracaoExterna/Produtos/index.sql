

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[st]') AND name = N'IX_st_all_includes_siteNr')
drop index st.IX_st_all_includes_siteNr


IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[st]') AND name = N'IX_st_all_includes_siteNr')

CREATE NONCLUSTERED INDEX [IX_st_all_includes_siteNr]
ON [dbo].[st] ([site_nr])
INCLUDE ([design],[stock],[epv1],[usr1],[validade],[tabiva],[obs],[ivaincl],[marg4],[stns],[inactivo],[ousrdata],[ousrhora],[usrinis],[usrdata],[usrhora],[marcada],[u_lab],[u_catstamp],[u_depstamp],[u_famstamp],[qttminvd],[qttembal],[codCNP],[tempoIndiponibilidade],[dispOnline],[cativado],[u_segstamp])
GO

