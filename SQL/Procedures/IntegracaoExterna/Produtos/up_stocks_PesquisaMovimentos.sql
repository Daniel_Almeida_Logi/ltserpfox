/* Movimentos de Stocks 
2020-04-07, JG
2020-04-30, revisão para incluir clientnumber/providernumber com consequente alteração para sql dinâmico

A sp "up_stocks_PesquisaMovimentos" permite pesquisa entre duas datas
para determinada referencia e armazem de produto.
O resultado é uma linha com os totais de entradas, saídas e saldo, anteriores, do período, e totais

Os campos @ref e @armazem são obrigatórios, os outros podem vir a NULL ou ''

As variáveis @clientNumber e @providerNumber permitem filtrar para a existência da entidade:
	@clientNumber, @providerNumber : ambas preenchidas, não devolve nada
	@clientNumber, @providerNumber : ambas vazias, devolve todos os registos 
	@clientNumber: só esta preenchida devolve registos para o cliente baseados em origem ‘FT’ 
	@providerNumber : só esta preenchida devolve registos para a entidade baseados em origem ‘BO’ e ‘FO’ 



Input:
---------------------
@ref, @armazem, @dataIni, @dataFim, @lote, @clientNumber, @providerNumber, @pageNumber

Output:
--------
design, nhrn, ref, cnpem, entries, exits, balance, numberDoc, serieDescr,pcl, pcp
, totalPcl, totalPcp, vatInc, currency, batch, warehouse, creationDate, creationTime, 
	

Execução SP:
--------------
exec up_stocks_PesquisaMovimentos_Totais 'ref',armazem, 'dataini', 'datafim','lote','clientNumber'
	,'providerNumber', pageNumber

	 exec up_stocks_PesquisaMovimentos '5440987',1, '20170102', '20181110','','202','',1
	 exec up_stocks_PesquisaMovimentos '5440987',1, '20170102', '20181110','','','',1
	 exec up_stocks_PesquisaMovimentos '5440987',1, '20170102', '20181110','','','',1,'3CBF7099-A353-4CBF-A24F-FFFC6A8E1F50'
	 exec up_stocks_PesquisaMovimentos '5440987',1, '20170102', '20181110','','','',1,'03DC4C81-5D05-4D09-A742-9AAC9DD012FC'
	 exec up_stocks_PesquisaMovimentos '5440987',1, '2019-01-01', '2020-06-02',null,null,null,null,'3CBF7099-A353-4CBF-A24F-FFFC6A8E1F50'
	 exec up_stocks_PesquisaMovimentos '5440987',1, '2019-01-01', '2020-06-02',null,null,null,null,'03DC4C81-5D05-4D09-A742-9AAC9DD012FC'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


IF OBJECT_ID('[dbo].[up_stocks_PesquisaMovimentos]') IS NOT NULL
	drop procedure dbo.up_stocks_PesquisaMovimentos
go

CREATE procedure dbo.up_stocks_PesquisaMovimentos

@ref			varchar (18),
@armazem		numeric(5,0),
@dataIni		datetime,
@dataFim		datetime,
@lote			varchar(30),
@pageNumber		int = 1,
@clientNumber	varchar(14) = null, 
@providerNumber varchar(14) = null,
@orderStamp		VARCHAR(36) = ""


/* WITH ENCRYPTION */
AS


DECLARE @PageSize int = 100

if(isnull(@pageNumber,0)<1)
	set @pageNumber = 1

IF len(@ref) > 0 and len(@armazem) > 0
BEGIN

	--set @ref				= isnull(@ref,'')
	--set @armazem			= isnull(@armazem,'')
	SET @dataIni			= COALESCE(NULLIF(@dataIni,''), '19990101')
	SET @dataFim			= COALESCE(NULLIF(@dataFim,''), '22000101')
	SET @lote				= isnull(@lote,'')
	SET @providerNumber		= isnull(@providerNumber,'')
	SET @clientNumber		= isnull(@clientNumber,'')
	SET @orderStamp         =rtrim(ltrim(ISNULL(@orderStamp,'')))


	--If OBJECT_ID('tempdb.dbo.#SaldoAnterior') IS NOT NULL
		--drop table #SaldoAnterior
	If OBJECT_ID('tempdb.dbo.#TempMovimentos') IS NOT NULL
		drop table #TempMovimentos

	DECLARE @OrderCri VARCHAR(MAX)

	IF @orderStamp = ''
	BEGIN 
		SET @OrderCri = 'ORDER BY datalc DESC, usrhora DESC'
	END 
	ELSE 
	BEGIN 
		SELECT @OrderCri='ORDER BY ' + instructionSql  FROM sortingTableOrder (NOLOCK) WHERE stamp = @orderStamp and inativo=0 and nameService='productMov'
	END 

		
	/*	
	select 
		saldoAnterior = SUM(Case When cm <50 Then qtt Else - qtt End)
		,entradasAnterior = sum(case 
									when cm < 50 then 
										case when qtt < 0 then 0 else round(qtt,0) end
										else case when qtt < 0 and cm > 50 then	qtt * -1 else round(0,0) end
								end
							)
		,saidasAnterior = sum(case 
									when cm > 50 and qtt > 0 
										then round(qtt,0) 
										else case 
												when cm < 50 and qtt < 0
													then ROUND(qtt*-1,0)
													else round(0,0) 
											end
										end
							)
	into 
		#SaldoAnterior	
	from 
		sl (nolock) 
	WHere 
		sl.datalc <  @dataIni 
		and sl.ref = @ref 
		and sl.armazem = @armazem  
		and sl.lote like case when @lote = '' then sl.lote else @lote + '%' end	
		--and sl.cm between 
		--	(case when @movimento like '%Todos%' then 1 else (case when @movimento like '%Entradas%' then 1 else (case when @movimento like '%Saídas%' then 51 else 0 end ) end) end) 
		--	and 
		--	(case when @movimento like '%Todos%' then 99 else (case when @movimento like '%Entradas%' then 50 else (case when @movimento like '%Saídas%' then 99 else 0 end ) end) end)
	*/
	

	--auxiliares para cálculo de nhrn (codcnp ou cnp), cnpem através da ref
	declare @nhrn varchar(18)
	declare @cnpem varchar(18)
	
	select @nhrn= isnull(y.cnp, x.codCNP), @cnpem=y.cnpem from st x
	inner join fprod(nolock) y
	on x.ref = y.cnp
	where x.ref = @ref


	--auxiliar para cálculo do input clientNumber/providerNumber
	-- @campo=-1 é para nao mostrar nada
	-- @campo='' é para mostrar tudo
	
	declare @campo varchar(14)	

	if len(@clientNumber)>0 and len(@providerNumber)>0
		begin
			set @campo = '-1'			
		end
		else if @clientNumber='' and @providerNumber=''
		begin
			set @campo = ''			
		end
			else if @clientNumber <> ''
			begin	
				select distinct @campo = frcl from sl(nolock)  where frcl = @clientNumber and origem='FT' 
				if @@ROWCOUNT=0
						set @campo = '-1'
			end	
				else if @providerNumber <> ''
				begin
					select distinct @campo = frcl from sl(nolock)   where frcl = @providerNumber and origem<>'FT'
					if @@ROWCOUNT=0
						set @campo = '-1'
				end
			--print(@campo)
			
	---------------------------------------------------------------------
	declare @sql varchar(max)	
	declare @sql2 varchar(max)	

	set @sql ='
		SELECT
			sl.cm
			,sl.slstamp
			,ROW_NUMBER() over(order by  sl.datalc asc, sl.ousrhora asc) as numero
			,qtt
		Into
			#TempMovimentos
		FROM
			sl (nolock)
		WHERE
		sl.ref=''' + @ref + '''
		and sl.datalc between '''+  convert(varchar,@dataIni) + ''' and '''+ convert(varchar,@dataFim) + '''
		and sl.armazem = ' + convert(varchar,@armazem) + '
		and sl.lote like case when ''' + @lote + ''' = '''' then sl.lote else ''' + @lote +''' + ''%'' end
		and sl.frcl = COALESCE(NULLIF(''' + @campo + ''',''''), sl.frcl) '

		if (len(@clientNumber)>0 and len(@providerNumber)>0) or (@clientNumber='' and @providerNumber='')
		begin
			set @sql2= ''	
		end
		else if len(@clientNumber)>0 
		begin	
			set @sql2=' and sl.origem = ''FT'' ' 			
		end	
			else if len(@providerNumber)>0 
			begin
				set @sql2=' and sl.origem <> ''FT'' ' 				
			end
		
		set @sql= @sql+ @sql2
		--print(@sql)
		
	set @sql= @sql + ' 	
		create index idx_tempMovimentos_slstamp on #TempMovimentos(slstamp)	
	 
		SELECT 
		--SALDOANTERIOR		= (select SALDOANTERIOR from #SaldoAnterior)
		--,ENTRADASANTERIOR	= (select ENTRADASANTERIOR from #SaldoAnterior)
		--,SAIDASANTERIOR		= (select SAIDASANTERIOR from #SaldoAnterior)
		SALDO				= 	isnull((
										Select sum(case when a.cm < 50 then a.qtt else - a.qtt end) 
										From   (select * from #TempMovimentos)a
												inner join sl (nolock) on sl.slstamp = a.slstamp
												Where a.numero <= b.numero
										   ),0)
								
		,ENTRADAS			= 
							case when b.cm < 50 
								then 
									case when b.qtt < 0
										then
											0
										else
											round(b.qtt,0) 
										end
								else 
									case when b.qtt < 0 and b.cm > 50
										then
											b.qtt * -1
										else
											round(0,0) 
										end
								end

		,SAIDAS				= 
							case when b.cm > 50 and b.qtt > 0
								then 
									round(b.qtt,0) 
								else 
									case when b.cm < 50 and b.qtt < 0
										then
											ROUND(b.qtt*-1,0)
										else
											round(0,0) 
										end
								end			
		, ''' + rtrim(isnull(@nhrn,'')) + ''' AS nhrn
		, ''' + isnull(@cnpem,'') + ''' AS cnpem
		, *
		into #dadossl
		From (	
		select
			ROW_NUMBER() over(order by  sl.datalc asc, sl.ousrhora asc) as numero
			, frcl
			, origem
			,qtt 
			,ref
			,design 
			,slstamp
			,convert(varchar,datalc,23) as datalc			
			,cm
			,cmdesc as cmdesc
			,adoc 
			,nome + (case when bistamp='''' then '''' else ('' - ''+ (select codext from bo2 (nolock) where bo2.bo2stamp=(select bostamp from bi (nolock) where bi.bistamp=sl.bistamp))) end) as nome
			,sl.armazem
			,evu as PCL
			,ett as PCLTOTAL
			,epcpond as PCP
			,(epcpond*qtt) as PCPTOTAL
			,ousrinis
			,sl.ousrhora
			,sl.ousrdata
			,CONVERT(bit,0) as verdoc
			--,origem 
			,fistamp
			,bistamp
			,fnstamp
			,sticstamp
			,(select top 1 username from b_us where sl.ousrinis=b_us.iniciais) as username
			,lote
			,moeda
			,1 As vatInc
			,usrhora			 
		from
			sl (nolock) 
		where
			sl.ref=''' + @ref + '''
			and sl.datalc between '''+  convert(varchar,@dataIni) + ''' and '''+ convert(varchar,@dataFim) + '''
			and sl.armazem = ' + convert(varchar,@armazem) + '
			and sl.lote like case when ''' + @lote + ''' = '''' then sl.lote else ''' + @lote +''' + ''%'' end
			and sl.frcl = COALESCE(NULLIF(''' + @campo + ''',''''), sl.frcl) '
			
			set @sql = @sql + @sql2
			set @sql = @sql + '	

			) b ' + @OrderCri  


		--print(@sql)
		--EXEC @sql

	--select final
	set @sql= @sql + '	
		select 	
		 ' + convert(varchar,@PageSize) + ' as pageSize  
		,(COUNT(*) OVER() / ' + convert(varchar,@PageSize) + ') + 1 as pageTotal
		,convert(varchar(10),' + convert(varchar,@PageNumber) + ')  as pageNumber
		,COUNT(*) OVER() as linesTotal, frcl, origem			
		,design, isnull(nhrn,'''') as nhrn , ref, isnull(cnpem,'''') as cnpem , ENTRADAS AS entries, SAIDAS AS exits
		, SALDO AS balance, adoc AS numberDoc, cmdesc AS serieDescr, PCL AS pcl, PCP AS pcp
		, PCLTOTAL AS totalPcl, PCPTOTAL AS totalPcp, vatInc, moeda AS currency, lote AS batch, armazem AS warehouse
		, convert(varchar,datalc,23) AS creationDate, ousrhora AS creationTime			
		from 
			#dadossl 
		' + @OrderCri +'  
		OFFSET ' + convert(varchar,@PageSize) + ' * (' + convert(varchar,@PageNumber) + ' - 1) ROWS
		FETCH NEXT ' + convert(varchar,@PageSize) + ' ROWS ONLY;'

	--print(@sql)
	EXEC (@sql)

END
ELSE
BEGIN
	print('Missing Ref and/or armazem')
END

GO
Grant Execute on dbo.up_stocks_PesquisaMovimentos to Public
Grant Control on dbo.up_stocks_PesquisaMovimentos to Public
go
