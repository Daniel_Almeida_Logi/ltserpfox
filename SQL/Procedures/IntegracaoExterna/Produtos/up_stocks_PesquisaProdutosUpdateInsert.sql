/*
PESQUISA PRODUTOS UPDATE/INSERT
J.Gomes, 2020-04-09
--------------------------------

Pesquisa do registo na tabela st a partir de 2 campos:
	 ref (@ref), site (@site) 

Campos obrigatórios:
---------------------
@site


A execução passa pelo envio de todas as variáveis que podem assumir 3 estados:
NULL						- significa que não é para fazer nada ao campo (só para o update)
''							- significa que é para colocar o campo sem nada, limpar o campo
'valor' ou valornumerico	- significa que o campo na tabela deve ser alterado para este valor

O processo inicializa pela pesquisa de um conjunto de 2 variáveis, todas preenchidas ou não:
	-  ref (@ref), site (@site) 

Perante estas variáveis, será feita uma procura na tabela st por registos que coincidam
com respectivos valores. 

Podem acontecer 2 situações:
	1) É enviado o @site e a @ref : 
		a) Tem dados com mais que um registo	--> mensagem que são necessários mais parâmetros para pesquisa
		b) Não tem dados						--> pode fazer o INSERT
		b) Tem dados com um só registo			--> pode fazer o UPDATE

	2) Não é enviado a @ref, somente o @site	--> faz o INSERT
		a) ref NULL ou '' : insere novo registo com ref = max(ref)+1, desde que >12 digitos

A opção de 12 digitos foi para não colidir com outras referências que normalmente são de 8 digitos.

Dicionário:
------------

Um dos parâmetros de entrada é o "useDic" com valores de 0/1.
No caso de useDic=1 são usados valores existentes no dicionário para o produto.
Os campos a usar do dicionário são:
	* designation, nhrn, retailPrice, vatPerc, lab, brand e familycode
Estes dados apenas serão usados se os parâmetros respectivos na sp vierem a NULL


No final do processo são retornados valores após o insert ou update:
	* DateMessag, StatMessag, ActionMessage, DescMessag, site , ref

StatMessag: 
	1 - OK
	0 - ERROR

ActionMessage/DescMessag: 
	0 - UPDATE
	1 - INSERT
	2 - NEED MORE FIELDS
	3 - NO DATA
	4 - WRONG TYPE


ex. no INSERT:
DateMessag				StatMessag	ActionMessage	DescMessag	Site	Ref
2020-04-11 18:57:05.840	1			1				INSERT		Loja 3	4663381	

ex. no UPDATE:
DateMessag				StatMessag	ActionMessage	DescMessag	site	ref
2020-04-11 18:52:08.570	1			0				UPDATE		Loja 1	4663381           
			

-----------------------------------------UPDATE -----------------------------------
UPDATE dos seguintes campos:
	, nhrn, design, stmax, stmin
	, epv1, ivaincl, tabiva, epcult
	, mfornec, mfornec2, qttminvd, qttembal
	, validade, stns, qlook
	, pbruto, peso, unidade, qttesp
	, volume, obs, u_lab, usr1, familia
	, usrdata, usrhora
	

Ficam fora do update o "site"  (@site_nr) e a "ref" (@ref) pois são a chave.
O campo usrdata  e usrhora é gravado como sendo o getdate() apenas no update.


-----------------------------------------INSERT -----------------------------------
No processo de insert existem duas possibilidades:
	* não é fornecido a variável "ref" (@ref)	--> insert com "ref" = max(ref)+1 (=12 digitos)
	* é indicado a variável "ref" (@ref)		--> insert ref = @ref 

INSERT dos seguintes campos:
	ststamp 
	, codCNP, design, stmax, stmin
	, epv1, ivaincl, tabiva, epcult
	, mfornec, mfornec2, qttminvd, qttembal
	, validade, stns, qlook
	, pbruto, peso, unidade, qttesp
	, volume, obs, u_lab, usr1, familia
	, usrdata, usrhora, ousrdata, ousrhora
	, site_nr, ref

O campo ststamp é gravado com base na sua criação naquele momento através de:
	@stampid = (select left(newid(),25))

Os campos usrdata, usrhora, ousrdata, ousrhota são gravados como sendo o getdate().

-------------------------------------------------------------------------

PROCEDURE
Criada procedure up_stocks_PesquisaProdutosUpdateInsert com parâmetros 

Parametros: 


EXECUÇÃO DA SP:
---------------
exec up_stocks_PesquisaProdutosUpdateInsert 'ref','nhrn','designation','site_name',useDic (1/0)
, stockmax,stockmin, retailPrice, vatPerc, vatInc,pct, discountPerc, discountValue, qttminsell
, qttUnEmb, 'expirationDate', service (1/0), voucher (1/0), weight
, weight1, 'weightUnit', with, volume, 'obs','lab', 'brand', 'familyCode'
, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

--sem dicionario
exec up_stocks_PesquisaProdutosUpdateInsert '4663381',NULL,null,'Loja 3',0
, 100,50, 5000, 12, 1,5, 100, 2, 2, 7,'2020-12-30', 0 
, 0, 3, 33
, 'kg', 33, 44, 'teste de qq coisa','lab', 'brand', 4
, NULL, NULL, NULL, NULL, NULL, NULL, null, null

--com dicionario
exec up_stocks_PesquisaProdutosUpdateInsert '4663381',null,null,'Loja 3',1
, NULL,NULL, NULL, NULL, null,NULL, NULL, NULL, NULL, NULL, NULL 
, NULL, NULL, NULL
, NULL, NULL, NULL, NULL, NULL,null, null, null
, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL


VERIFICAÇÃO:
------------
select  usrdata,usrhora,codcnp, u_lab, usr1,  familia, tabiva,ivaincl,epv1, design,* 
from st where ref='4663381' and site_nr=3

EXEC up_stocks_PesquisaProdutosUpdateInsert N'',N'',N'Ben-u-ron',N'ATLANTICO',0,10,1,2.79,20,1,1.35,20,20,0,0,N'2019-12-26',0,0,1.1,1.25,N'kg',1.23,123,N'teste123',N'avene',N'5M',99,N'Lasix',0,0,1,N'obs 1',N'obs 2',N'obs 3',N''
*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_stocks_PesquisaProdutosUpdateInsert]') IS NOT NULL
    drop procedure up_stocks_PesquisaProdutosUpdateInsert
go

CREATE PROCEDURE up_stocks_PesquisaProdutosUpdateInsert	
	
		  @ref					VARCHAR(18)
		, @nhrn					VARCHAR(18)
		--, convert(varchar(8),ISNULL(fprod.cnpem,'')) AS cnpem		
		, @designation			VARCHAR(100)
		, @site					VARCHAR(20)		
		, @useDic				BIT
				
		, @stockMax				INT
		, @stockMin				INT
		, @retailprice			DECIMAL
		, @vatPerc				DECIMAL
		, @vatInc				BIT
		, @pct					DECIMAL
		, @discountPerc			DECIMAL
		, @discountValue		DECIMAL
		, @qttMinSell			DECIMAL
		, @qttUnEmb				DECIMAL			   		 	  
		, @expirationDate		DATE
		, @service				BIT		
		, @voucher 				BIT
		, @weight				DECIMAL
		, @weight1				DECIMAL
		, @weightUnit			VARCHAR(10)	
		, @width				DECIMAL
		, @volume				DECIMAL
		, @obs					VARCHAR(4000)	
		, @lab					VARCHAR(150)	
		, @brand				VARCHAR(100)	
		, @familyCode			INT		

		--online: tudo a vazio
		, @OnlineDesignation	VARCHAR(254)	
		, @OnlineInactive		BIT
		, @OnlineNew			BIT
		, @OnlineDescription1	VARCHAR(254)
		, @OnlineDescription2	VARCHAR(254)
		, @OnlineDescription3	VARCHAR(254)
		, @OnlineImage			VARCHAR(4000)

	
AS

		SET @ref					=   rtrim(ltrim(@ref))
		SET @nhrn					=   rtrim(ltrim(@nhrn))	
		SET @designation			=   rtrim(ltrim(@designation))
		SET @site					=   rtrim(ltrim(@site))
		SET @useDic					=   rtrim(ltrim(@useDic))				
		SET @stockMax				=   rtrim(ltrim(@stockMax))
		SET @stockMin				=   rtrim(ltrim(@stockMin))
		SET @retailprice			=   rtrim(ltrim(@retailprice))
		SET @vatPerc				=   rtrim(ltrim(@vatPerc))
		SET @vatInc					=   rtrim(ltrim(@vatInc))
		SET @pct					=   rtrim(ltrim(@pct))
		SET @discountPerc			=   rtrim(ltrim(@discountPerc))
		SET @discountValue			=   rtrim(ltrim(@discountValue))
		SET @qttMinSell				=   rtrim(ltrim(@qttMinSell))
		SET @qttUnEmb				=   rtrim(ltrim(@qttUnEmb))			   		 	  
		SET @expirationDate			=   rtrim(ltrim(@expirationDate))
		SET @service				=   rtrim(ltrim(@service))		
		SET @voucher 				=   rtrim(ltrim(@voucher))
		SET @weight					=   rtrim(ltrim(@weight))
		SET @weight1				=   rtrim(ltrim(@weight1))
		SET @weightUnit				=   rtrim(ltrim(@weightUnit))
		SET @width					=   rtrim(ltrim(@width))
		SET @volume					=   rtrim(ltrim(@volume))
		SET @obs					=   rtrim(ltrim(@obs))	
		SET @lab					=   rtrim(ltrim(@lab))
		SET @brand					=   rtrim(ltrim(@brand))	
		SET @familyCode				=   rtrim(ltrim(@familyCode))		
				
		--online: tudo a vazio
		SET @OnlineDesignation		=   rtrim(ltrim(@OnlineDesignation))
		SET @OnlineInactive			=   rtrim(ltrim(@OnlineInactive))
		SET @OnlineNew				=   rtrim(ltrim(@OnlineNew))
		SET @OnlineDescription1		=   rtrim(ltrim(@OnlineDescription1))
		SET @OnlineDescription2		=   rtrim(ltrim(@OnlineDescription2))
		SET @OnlineDescription3		=   rtrim(ltrim(@OnlineDescription3))
		SET @OnlineImage			=   rtrim(ltrim(@OnlineImage))



DECLARE @sqlCommand			VARCHAR(5000) 
DECLARE @contador			int
DECLARE @lastChangeDate		datetime
DECLARE @lastChangeTime		VARCHAR(8)
DECLARE @CreationDate		datetime
DECLARE @CreationTime		VARCHAR(8)

--fonte
-- I - interna
-- D - dicionario

DECLARE @source	VARCHAR(2) = 'I'
DECLARE @operator		    VARCHAR(8) = 'ONL'


--variaveis para apresentar no final
declare @ref_ VARCHAR(18)


SET @lastChangeDate = convert(varchar,getdate(),23)
SET @lastChangeTime = convert(varchar(8),getdate(),24)
SET @CreationDate = convert(varchar,getdate(),23)
SET @CreationTime = convert(varchar(8),getdate(),24)


IF len(@site) > 0 		
BEGIN		



	DECLARE @siteno AS tinyint
	SET @siteno = (select  no from empresa(nolock) where site = @site)	


		
	select @ref_ = ref from st (nolock) where (ref = @ref AND site_nr = @siteno)	
	select @contador=@@ROWCOUNT	
	
	--se inclui IVA o seu valor tem de existir
	DECLARE @codIVA AS int	
	IF @vatInc = 1 
	--and len(@vatPerc) > 0
	BEGIN		
		select @codIVA = codigo from taxasiva (nolock) where taxa = @vatPerc
		--se nao existir a variavel fica a NULL e segue...
	END

	IF @useDic = 1
	BEGIN
		DECLARE @fproddesign				VARCHAR(100)
		DECLARE @fprodnhrn					VARCHAR(18)		
		DECLARE @fprodretailprice			DECIMAL
		DECLARE @fprodcodIVA				DECIMAL
		DECLARE @fprodlab					VARCHAR(150)	
		DECLARE @fprodbrand					VARCHAR(100)	
		DECLARE @fprodfamilyCode			INT
				
		select 
		  @fproddesign		= design 
		, @fprodnhrn		= cnp
		, @fprodretailprice	= pvporig	
		, @fprodcodIVA		= u_tabiva
		, @fprodlab			= titaimdescr
		, @fprodbrand		= u_marca
		, @fprodfamilyCode	= u_familia
		, @source			= 'I'
		from fprod where ref = @ref
		--pode resultar NULL se a ref nao existir ou se a ref vier a NULL, será para fazer insert
		
		-- a variavel mantem o valor de entrada se tiver dados, mesmo usando o dicionario
		SET @designation	= COALESCE(@designation, @fproddesign)
		SET @nhrn			= COALESCE(@nhrn, @fprodnhrn)
		SET @retailprice	= COALESCE(@retailprice, @fprodretailprice)
		SET @codIVA			= COALESCE(@codIVA, @fprodcodIVA)
		SET @lab			= COALESCE(@lab, @fprodlab)
		SET @brand			= COALESCE(@brand, @fprodbrand)
		SET @familyCode		= COALESCE(@familyCode, @fprodfamilyCode)
		SET @source = isnull(@source,'I')
	END

	/*se tiver mais que um registo devolve mensagem e nao faz o update
	isto não deve acontecer porque ref/site são chave primária...mas...
	*/
	If @contador > 1 
	BEGIN
		--print('Existe mais que um registo na pesquisa, são necessários mais campos de pesquisa')
		select getdate() AS DateMessag, 0 As StatMessag, 2 AS ActionMessage, 'NEED MORE FIELDS' As DescMessag, @ref_ AS Ref, @site as site;
	END
		ELSE IF @contador = 1 /*tem um registo, faz o update*/
		BEGIN	
/*
		BEGIN TRY
*/				
			BEGIN TRANSACTION; 	
				UPDATE st SET 					
					codCNP			= COALESCE(@nhrn, codCNP)  
					,design			= COALESCE(@designation, design) 
					,stmax			= COALESCE(@stockMax, stmax)
					,stmin			= COALESCE(@stockMin, stmin)
					,epv1			= COALESCE(@retailprice, epv1)
					,ivaincl		= COALESCE(@vatInc, ivaincl)
					,tabiva			= COALESCE(@codIVA, tabiva)
					,epcult			= COALESCE(@pct, epcult)
					,mfornec		= COALESCE(@discountPerc, mfornec)
					,mfornec2		= COALESCE(@discountValue, mfornec2)						
					,qttminvd		= COALESCE(@qttMinSell, qttminvd)
					,qttembal		= COALESCE(@qttUnEmb, qttembal)
					,validade		= COALESCE(@expirationDate, validade)
					,stns			= COALESCE(@service, stns)
					,qlook			= COALESCE(@voucher, qlook)
					,pbruto			= COALESCE(@weight, pbruto)
					,peso			= COALESCE(@weight1, peso)						
					,unidade		= COALESCE(@weightUnit, unidade)
					,qttesp			= COALESCE(@width, qttesp)
					,volume			= COALESCE(@volume, volume)
					,obs			= COALESCE(@obs, obs)
					,u_lab			= COALESCE(@lab, u_lab)
					,usr1			= COALESCE(@brand, usr1)
					,familia		= COALESCE(@familyCode, familia)
					,usrdata		= @lastChangeDate
					,usrhora		= @lastChangeTime
					,u_fonte		= @source

					,usrinis        = @operator

					,designOnline	= @OnlineDesignation
					,caractOnline   = @OnlineDescription1
					,apresentOnline = @OnlineDescription2
					,dispOnline		= (case when @OnlineInactive= 1 then 0 else 1 end)	
					,novidadeOnline = @OnlineNew

				/*
				no update não pode mexer nestas variaveis, sao a chave
				, ref 	, site_nr 
				*/
				where (ref = @ref AND site_nr = @siteno)

			COMMIT TRANSACTION;	
			select getdate() AS DateMessag, 1 As StatMessag, 0 AS ActionMessage, 'UPDATE' As DescMessag, @site AS site, @ref_ AS ref;

/*
		END TRY
		BEGIN CATCH
			SELECT   
				ERROR_NUMBER() AS ErrorNumber  
				,ERROR_MESSAGE() AS ErrorMessage;  
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION;		
			END
		END CATCH
*/
		END
			ELSE /*não tem registo, faz o insert*/
			BEGIN			
				BEGIN TRANSACTION;
					DECLARE @stampid CHAR(25)  
					SET @stampid = (select left(newid(),25))						

					--calc valor de ref seguinte ao MAX para inserir caso @ref venha sem dados
					IF @ref IS NULL OR @ref=''
					BEGIN
						DECLARE @maxRef12 numeric(12,0)
						select @maxRef12 = isnull(max(ref),800000000000) from st(nolock) 
							where ISNUMERIC(ref)=1 and len(ref)=12 and site_nr = @siteno

						SET @maxRef12 = @maxRef12 + 1
						--print (@maxRef12)
					END		

					INSERT INTO st (ststamp 
								, codCNP, design, stmax, stmin
								, epv1, ivaincl, tabiva, epcult
								, mfornec, mfornec2, qttminvd, qttembal
								, validade, stns, qlook
								, pbruto, peso, unidade, qttesp
								, volume, obs, u_lab, usr1, familia
								, usrdata, usrhora, ousrdata, ousrhora
								, usrinis, ousrinis
								, site_nr, ref, u_fonte
								,designOnline,caractOnline,apresentOnline,dispOnline,novidadeOnline
							) 
					VALUES (  @stampid
								, isnull(@nhrn,''), isnull(@designation,''), isnull(@stockMax,0), isnull(@stockMin,0)
								, isnull(@retailprice,0), isnull(@vatInc,1), isnull(@codIVA,4), isnull(@pct,0)
								, isnull(@discountPerc,0), isnull(@discountValue,0), isnull(@qttMinSell,0), isnull(@qttUnEmb,0)
								, isnull(@expirationDate,'1900-01-01'), isnull(@service,''), isnull(@voucher,'')
								, isnull(@weight,0), isnull(@weight1,0), isnull(@weightUnit,''), isnull(@width,0)
								, isnull(@volume,0), isnull(@obs,''), isnull(@lab,''), isnull(@brand,''), isnull(@familyCode,99)								
								, @lastChangeDate, @lastChangeTime, @CreationDate, @CreationTime
								, @operator, @operator
								, @siteno, COALESCE(NULLIF(@ref,''), convert(varchar(18),@maxRef12)), @source
								,@OnlineDesignation, @OnlineDescription1,@OnlineDescription2,(case when @OnlineInactive= 1 then 0 else 1 end),@OnlineNew
							);	
				COMMIT TRANSACTION;	


				
				--mensagem final
				select getdate() AS DateMessag, 1 AS StatMessag, 1 AS ActionMessage, 'INSERT' As DescMessag
				, @site AS Site
				, COALESCE(NULLIF(@ref,''), convert(varchar(18),@maxRef12)) AS Ref;
				
			END
	
END
ELSE
BEGIN
	--print('site é obrigatório')
	select getdate() AS DateMessag, 0 As StatMessag, 2 AS ActionMessage, 'NEED MORE FIELDS' As DescMessag, @site AS site;
END

            
GO
Grant Execute On up_stocks_PesquisaProdutosUpdateInsert to Public
Grant Control On up_stocks_PesquisaProdutosUpdateInsert to Public
GO
			
           
			
	