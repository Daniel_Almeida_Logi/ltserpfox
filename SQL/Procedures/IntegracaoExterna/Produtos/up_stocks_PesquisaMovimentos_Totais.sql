/* Movimentos de Stocks Totais
2020-04-07, JG

A sp "up_stocks_PesquisaMovimentos_Totais" permite pesquisa entre duas datas
para determinada referencia e armazem de produto.
O resultado é uma linha com os totais de entradas, saídas e saldo, anteriores, do período, e totais

Os campos @ref e @armazem são obrigatórios, os outros podem vir a NULL ou ''

Input:
---------------------
@ref, @armazem, @dataIni, @dataFim, @lote

Output:
--------
PreviousStockEntries, PreviousStockExits, PreviousStockBalance
, PeriodStockEntries, PeriodStockExits, PeriodStockBalance
, TotalStockEntries, TotalStockExits, TotalStockBalance


Execução SP:
--------------
exec up_stocks_PesquisaMovimentos_Totais 'ref',armazem, 'dataini', 'datafim','lote'

exec up_stocks_PesquisaMovimentos_Totais '6491787',1, '20170102', '20171110',''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


IF OBJECT_ID('[dbo].[up_stocks_PesquisaMovimentos_Totais]') IS NOT NULL
	drop procedure dbo.up_stocks_PesquisaMovimentos_Totais
go

CREATE procedure dbo.up_stocks_PesquisaMovimentos_Totais

@ref varchar (18),
@armazem numeric(5,0),
@dataIni datetime,
@dataFim datetime,
@lote varchar(30)


/* WITH ENCRYPTION */
AS

IF len(@ref) > 0 and len(@armazem) > 0
BEGIN

	--set @ref		= isnull(@ref,'')
	--set @armazem	= isnull(@armazem,'')
	set @dataIni	= COALESCE(NULLIF(@dataIni,''), '19990101')
	set @dataFim	= COALESCE(NULLIF(@dataFim,''), '22000101')
	set @lote		= isnull(@lote,'')


	If OBJECT_ID('tempdb.dbo.#SaldoAnterior') IS NOT NULL
		drop table #SaldoAnterior
	If OBJECT_ID('tempdb.dbo.#SaldoPeriodo') IS NOT NULL
		drop table #SaldoPeriodo

		

	--Anterior
	select 
		saldoAnterior = SUM(Case When cm <50 Then qtt Else - qtt End)
		,entradasAnterior = sum(case 
									when cm < 50 then 
										case when qtt < 0 then 0 else round(qtt,0) end
										else case when qtt < 0 and cm > 50 then	qtt * -1 else round(0,0) end
								end
							)
		,saidasAnterior = sum(case 
									when cm > 50 and qtt > 0 
										then round(qtt,0) 
										else case 
												when cm < 50 and qtt < 0
													then ROUND(qtt*-1,0)
													else round(0,0) 
											end
										end
							)
	into 
		#SaldoAnterior	
	from 
		sl (nolock) 
	WHere 
		sl.datalc <  @dataIni 
		and sl.ref = @ref 
		and sl.armazem = @armazem  
		and sl.lote like case when @lote = '' then sl.lote else @lote + '%' end	
		



		--Periodo
		select 
		saldoPeriodo = SUM(Case When cm <50 Then qtt Else - qtt End)
		,entradasPeriodo = sum(case 
									when cm < 50 then 
										case when qtt < 0 then 0 else round(qtt,0) end
										else case when qtt < 0 and cm > 50 then	qtt * -1 else round(0,0) end
								end
							)
		,saidasPeriodo = sum(case 
									when cm > 50 and qtt > 0 
										then round(qtt,0) 
										else case 
												when cm < 50 and qtt < 0
													then ROUND(qtt*-1,0)
													else round(0,0) 
											end
										end
							)
	into 
		#SaldoPeriodo	
	from 
		sl (nolock) 
	WHere		
		sl.datalc between  @dataIni and @dataFim
		and sl.ref = @ref 
		and sl.armazem = @armazem  
		and sl.lote like case when @lote = '' then sl.lote else @lote + '%' end	
		



	SELECT 
		 PreviousStockEntries	= (select ENTRADASANTERIOR from #SaldoAnterior)
		,PreviousStockExits		= (select SAIDASANTERIOR from #SaldoAnterior)
		,PreviousStockBalance	= (select SALDOANTERIOR from #SaldoAnterior)		
	
		,PeriodStockEntries		= (select ENTRADASPeriodo from #SaldoPeriodo)
		,PeriodStockExits		= (select SAIDASPeriodo from #SaldoPeriodo)
		,PeriodStockBalance		= (select SALDOPeriodo from #SaldoPeriodo)
	into #dadossl
	

	select *	
	, PreviousStockEntries + PeriodStockEntries AS TotalStockEntries
	, PreviousStockExits + PeriodStockExits		AS TotalStockExits	
	, PreviousStockBalance + PeriodStockBalance AS TotalStockBalance 
	from #dadossl
END
ELSE
BEGIN
	print('Missing Ref and/or armazem')
END

GO
Grant Execute on dbo.up_stocks_PesquisaMovimentos_Totais to Public
Grant Control on dbo.up_stocks_PesquisaMovimentos_Totais to Public
go
