/*
PESQUISA LOTES PRODUTOS UPDATE/INSERT
J.Gomes, 2020-04-14
--------------------------------

Pesquisa do registo na tabela st_lotes e st_lotes_precos de forma a fazer o update ou insert. 

Campos obrigatórios:
---------------------
@ref 
@designation
@warehouse 


A execução passa pelo envio de todas as variáveis que podem assumir 3 estados:
NULL						- significa que não é para fazer nada ao campo (só para o update)
''							- significa que é para colocar o campo sem nada, limpar o campo
'valor' ou valornumerico	- significa que o campo na tabela deve ser alterado para este valor

Podem acontecer 2 situações:
	1) A combinação @ref,@designation,@warehouse existe :
		* faz o UPDATE

	2) A combinação @ref,@designation,@warehouse não existe:
		* faz o INSERT

No final do processo são retornados valores após o insert ou update:
	* DateMessag, StatMessag, ActionMessage, DescMessag, @ref,@designation,@warehouse

StatMessag: 
	1 - OK
	0 - ERROR

ActionMessage/DescMessag: 
	0 - UPDATE
	1 - INSERT
	2 - NEED MORE FIELDS
	3 - NO DATA
	4 - WRONG TYPE


ex. no INSERT:
DateMessag				StatMessag	ActionMessage	DescMessag	Ref		Designation		Warehouse
2020-04-14 12:22:41.953	1			1				INSERT		4663381	teste_lote		1  

ex. no UPDATE:
DateMessag				StatMessag	ActionMessage	DescMessag	Ref		Designation		Warehouse
2020-04-14 12:22:41.953	1			0				UPDATE		4663381	teste_lote		1           
			

-----------------------------------------UPDATE -----------------------------------
UPDATE dos seguintes campos:

st_lotes: 					
	* stock	, validade	, usrdata, usrhora, usrinis				
				
st_lotes_precos:
	* epv1, usrdata, usrhora, usrinis		

O campo usrdata  e usrhora é gravado como sendo o getdate() apenas no update.


-----------------------------------------INSERT -----------------------------------
INSERT dos seguintes campos:

st_lotes: 
stamp , ref, lote, stock, validade, armazem
, usrdata, usrhora, ousrdata, ousrhora, usrinis, ousrinis								

st_lotes_precos: 
stamplote, epv1
, usrdata, usrhora, ousrdata, ousrhora, usrinis, ousrinis	

Os campos stamp e stamplote são gravados com base na sua criação naquele momento através de:
	@stampid = (select left(newid(),25))

Os campos usrdata, usrhora, ousrdata, ousrhota são gravados como sendo o getdate().
Os campos usrinis, ousrinis são codificados como 'ADM'

-------------------------------------------------------------------------

PROCEDURE
Criada procedure up_stocks_PesquisaProdutosBatchUpdateInsert com parâmetros 

Parametros: 

		  @ref					VARCHAR(18)			
		, @designation			VARCHAR(100)			
		, @qtt					INT		
		, @retailPrice			DECIMAL					   		 	  
		, @expirationDate		DATE		
		, @warehouse			VARCHAR(10)	

EXECUÇÃO DA SP:
---------------
exec up_stocks_PesquisaProdutosBatchUpdateInsert 'ref','designation',qtt,retailPrice
, 'expirationDate', warehouse


exec up_stocks_PesquisaProdutosBatchUpdateInsert '4663381','teste_lote2',25, 660
, '20201230', 1


VERIFICAÇÃO:
------------
select * from st_lotes A
left join st_lotes_precos B
on A.stamp = B.stamplote
where ref='4663381' and lote='teste_lote2'



*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_stocks_PesquisaProdutosBatchUpdateInsert]') IS NOT NULL
    drop procedure up_stocks_PesquisaProdutosBatchUpdateInsert
go

CREATE PROCEDURE up_stocks_PesquisaProdutosBatchUpdateInsert	
	
		  @ref					VARCHAR(18)			
		, @designation			VARCHAR(100)			
		, @qtt					INT		
		, @retailPrice			DECIMAL					   		 	  
		, @expirationDate		DATE
		, @warehouse			VARCHAR(10)	
		
AS

SET @ref					=   rtrim(ltrim(@ref))
SET @designation			=   rtrim(ltrim(@designation))
SET @qtt					=   rtrim(ltrim(@qtt))
SET @retailprice			=   rtrim(ltrim(@retailprice))
SET @expirationDate			=   rtrim(ltrim(@expirationDate))
SET @warehouse				=   rtrim(ltrim(@warehouse))

DECLARE @contador			int
DECLARE @CreationDate		datetime
DECLARE @CreationTime		VARCHAR(8)
DECLARE @lastChangeDate		datetime
DECLARE @lastChangeTime		VARCHAR(8)
DECLARe @ref_				VARCHAR(18)	

SET @lastChangeDate = convert(varchar,getdate(),23)
SET @lastChangeTime = convert(varchar(8),getdate(),24)
SET @CreationDate = convert(varchar,getdate(),23)
SET @CreationTime = convert(varchar(8),getdate(),24)

DECLARE @operator		    VARCHAR(8) = 'ONL'


IF len(@ref) > 0 AND len(@designation) >0 AND len(@warehouse) >0		
BEGIN		
		
	select @ref_ = ref from st_lotes(nolock) where (ref = @ref AND armazem = @warehouse AND lote = @designation)	
	select @contador=@@ROWCOUNT	

	If @contador > 1 
	BEGIN
		--print('Existe mais que um registo na pesquisa, são necessários mais campos de pesquisa')
		select getdate() AS DateMessag, 0 As StatMessag, 2 AS ActionMessage, 'NEED MORE FIELDS' As DescMessag, @ref AS Ref, @designation as Designation, @warehouse AS Warehouse;
	END
		ELSE IF @contador = 1 /*tem um registo, faz o update*/
		BEGIN	
/*
		BEGIN TRY
*/			
			BEGIN TRANSACTION; 	
				UPDATE st_lotes SET 					
					  stock			= COALESCE(@qtt, stock)  
					, validade		= COALESCE(@expirationDate, validade) 										
					, usrdata		= @lastChangeDate
					, usrhora		= @lastChangeTime
					, usrinis		= @operator				
				where (ref = @ref AND armazem=@warehouse AND lote = @designation)
				
				UPDATE st_lotes_precos SET 					
					  epv1			= COALESCE(@retailPrice, epv1)  								
					, usrdata		= @lastChangeDate
					, usrhora		= @lastChangeTime
					, usrinis		= @operator				
				where stampLote in (Select stamp from st_lotes 
					where ref = @ref AND armazem=@warehouse AND lote = @designation) 
				

			COMMIT TRANSACTION;	
			select getdate() AS DateMessag, 1 As StatMessag, 0 AS ActionMessage, 'UPDATE' As DescMessag, @ref AS Ref, @designation as Designation, @warehouse AS Warehouse;

/*
		END TRY
		BEGIN CATCH
			SELECT   
				ERROR_NUMBER() AS ErrorNumber  
				,ERROR_MESSAGE() AS ErrorMessage;  
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION;		
			END
		END CATCH
*/
		END
			ELSE /*não tem registo, faz o insert*/
			BEGIN			
				BEGIN TRANSACTION;
					DECLARE @stampid CHAR(25)  
					SET @stampid = (select left(newid(),25))	

					INSERT INTO st_lotes (stamp 
								, ref, lote, stock, validade, armazem
								, usrdata, usrhora, ousrdata, ousrhora
								, usrinis, ousrinis								
							) 
					VALUES (	  @stampid
								, @ref, @designation, isnull(@qtt,0)
								, isnull(@expirationDate,'1900-01-01'), @warehouse
								, @lastChangeDate, @lastChangeTime, @CreationDate, @CreationTime
								, @operator, @operator
								);

					INSERT INTO st_lotes_precos (stamplote, epv1
								, usrdata, usrhora, ousrdata, ousrhora
								, usrinis, ousrinis								
							) 
					VALUES (	 @stampid, isnull(@retailPrice,0)								
								, @lastChangeDate, @lastChangeTime, @CreationDate, @CreationTime
								, @operator, @operator
								);

							
				COMMIT TRANSACTION;	
				
				--mensagem final
				select getdate() AS DateMessag, 1 AS StatMessag, 1 AS ActionMessage, 'INSERT' As DescMessag
				, @ref AS Ref, @designation as Designation, @warehouse AS Warehouse;
				
			END
	
END
ELSE
BEGIN
	--print('site é obrigatório')
	select getdate() AS DateMessag, 0 As StatMessag, 2 AS ActionMessage, 'NEED MORE FIELDS' As DescMessag,@ref AS Ref, @designation as designation, @warehouse AS warehouse;
END

            
GO
Grant Execute On up_stocks_PesquisaProdutosBatchUpdateInsert to Public
Grant Control On up_stocks_PesquisaProdutosBatchUpdateInsert to Public
GO
			
           
			
	