/*
Pesquisa Campanhas Lista Produtos Oferta
J.Gomes, 2020-04-20
--------------------------------

Pesquisa nas tabelas: 	campanhas_pofertas_lista por 2 campos:
	@IdCamp
	@ref
	
Campos obrigatórios: @IdCamp

OUTPUT:
---------
	   ProductInfo_OfferRefs
	 


EXECUÇÃO DA SP:
---------------
exec up_PesquisaCampanhasListaProdutosOferta '@IdCamp', @ref


-- exec up_PesquisaCampanhasListaProdutosOferta 2
-- exec up_PesquisaCampanhasListaProdutosOferta 2,NULL
-- exec up_PesquisaCampanhasListaProdutosOferta 2,'000033            '

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_PesquisaCampanhasListaProdutosOferta]') IS NOT NULL
	drop procedure dbo.up_PesquisaCampanhasListaProdutosOferta
go

create procedure dbo.up_PesquisaCampanhasListaProdutosOferta
	@IdCamp			varchar(18),
	@ref			varchar(18) = NULL
AS

SET NOCOUNT ON

SET @IdCamp		=   rtrim(ltrim(isnull(@IdCamp,0)))


IF len(@IdCamp) > 0
BEGIN
	SELECT ref AS ProductInfo_OfferRefs from campanhas_pofertas_lista(nolock)
		 where campanhas like '%' + @IdCamp + '%'
		 AND ref = COALESCE(NULLIF(rtrim(ltrim(@ref)),''), ref) 

END
ELSE
	print('Missing @IdCamp')

Go
Grant Execute on dbo.up_PesquisaCampanhasListaProdutosOferta to Public
Grant Control on dbo.up_PesquisaCampanhasListaProdutosOferta to Public
Go
