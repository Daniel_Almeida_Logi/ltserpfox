/*
Pesquisa Campanhas Lista Produtos 
J.Gomes, 2020-04-20
--------------------------------

Listagem de todas os produtos existentes numa campanha em específico (@idcamp).

Pesquisa nas tabelas: 	campanhas_st_lista por 1 campos:
	@IdCamp
	@ref
	
Campos obrigatórios: @IdCamp

OUTPUT:
---------
	   ProductInfo_Refs
	   

EXECUÇÃO DA SP:
---------------
exec up_PesquisaCampanhasListaProdutos '@IdCamp',@ref


-- exec up_PesquisaCampanhasListaProdutos '',''
-- exec up_PesquisaCampanhasListaProdutos 208, ''
-- exec up_PesquisaCampanhasListaProdutos '4', NULL

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_PesquisaCampanhasListaProdutos]') IS NOT NULL
	drop procedure dbo.up_PesquisaCampanhasListaProdutos
go

create procedure dbo.up_PesquisaCampanhasListaProdutos
	@IdCamp			varchar(18),
	@ref			varchar(18)
AS

SET NOCOUNT ON

SET @IdCamp		=   rtrim(ltrim(isnull(@IdCamp,0)))


IF len(@IdCamp) > 0
BEGIN
	SELECT ref AS ProductInfo_Refs from campanhas_st_lista(nolock)
		 where campanhas like '%' + @IdCamp + '%'
		 AND ref = COALESCE(NULLIF(rtrim(ltrim(@ref)),''), ref) 
END
ELSE
	print('Missing @IdCamp')

Go
Grant Execute on dbo.up_PesquisaCampanhasListaProdutos to Public
Grant Control on dbo.up_PesquisaCampanhasListaProdutos to Public
Go
