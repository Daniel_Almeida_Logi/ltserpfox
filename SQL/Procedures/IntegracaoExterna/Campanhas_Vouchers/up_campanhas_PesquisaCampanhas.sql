/*
Pesquisa Campanhas
J.Gomes, 2020-04-17
--------------------------------

Listagem de todas as campanhas activas e não eliminadas, através de diversos inputs e sempre com 
o parâmetro site como obrigatório. 
A pesquisa pode ser através de uma campanha em específico (@id), pode ser através da referência de 
determinado produto que esteja em campanha (@ref), pode ser através da presença de determinado 
cliente na campanha (@no,  @dep, @email), e ainda permite filtrar a listagem acima de determinada 
data e hora de alteração.


Pesquisa nas tabelas: 	campanhas, b_utentes, campanhas_ut_lista, campanhas_st_lista por 8 campos:
	@Id						,
	@ref					,
	@no						,
	@dep					,
	@site					,
	@email					,
	@lastChangeDate			,
	@lastChangeTime			 
	
Campos obrigatórios: site

OUTPUT:
---------
	DesignInfo_designation	
	,IdInfo_number
	,Id_site	
	,FinanceInfo_discountPerc
	,FinanceInfo_Value	
	,FinanceInfo_discountCheapOnly	
	,FinanceInfo_currency	
	,OperationInfo_creationDate	
	,OperationInfo_creationTime	
	,OperationInfo_lastChangeDate	
	,OperationInfo_lastChangetime	
	,GeneralInfo_startDate	
	,GeneralInfo_endDate	
	,GeneralInfo_status


EXECUÇÃO DA SP:
---------------
exec up_campanhas_PesquisaCampanhas 
	'@Id', '@ref', '@no', '@dep','@site', '@email', '@lastChangeDate','@lastChangeTime'


-- exec up_campanhas_PesquisaCampanhas '', '0008585', '206', '0','Loja 1','','',''
-- exec up_campanhas_PesquisaCampanhas '', '0008585', '206', '0','Loja 1','bruno.carvalho@pontual.pt','',''
-- exec up_campanhas_PesquisaCampanhas '', '', '206', '0','Loja 1','','',''
-- exec up_campanhas_PesquisaCampanhas '', '0008585', '206', '0','Loja 1','bruno.carvalho@pontual.pt','',''
-- exec up_campanhas_PesquisaCampanhas '180', '', '206', '0','Loja 1','','2015-01-21','15:00:00'
-- exec up_campanhas_PesquisaCampanhas '', '', '', '','Atlantico','','',''
-- EXEC up_campanhas_PesquisaCampanhas 1,N'5440987',240,0,N'Loja 1',N'',N'2020-04-09',N''


-- exec up_campanhas_PesquisaCampanhas '', '', '', '','Loja 1','','','',1,'ECFA0711-EB77-49C1-9178-FA2DFACB832F'
-- exec up_campanhas_PesquisaCampanhas '', '', '', '','Loja 1','','','',1,'67FF74E8-92C5-4EEF-8537-BEE109CEC623'

--VERIFICACAO
select * from campanhas_st_lista where ref='0008585'
select * from campanhas_ut_lista where no=206 and estab=0
select no, estab,  email, * from b_utentes where email='bruno.carvalho@pontual.pt'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_campanhas_PesquisaCampanhas]') IS NOT NULL
	drop procedure dbo.up_campanhas_PesquisaCampanhas
go

CREATE PROCEDURE dbo.up_campanhas_PesquisaCampanhas

	@id						varchar(10),
	@ref					varchar(10),
	@no						varchar(10),
	@dep					varchar(3),
	@site					varchar(18),
	@email					varchar(50),
	@lastChangeDate			varchar(10) = '1900-01-01',
	@lastChangeTime			varchar(8)  = '00:00:00',
	@pageNumber             int = 1,
	@orderStamp				VARCHAR(36) = ""
	
AS

SET NOCOUNT ON

SET @id					=   rtrim(ltrim(@id))
SET @ref				=   rtrim(ltrim(@ref))
SET @no					=   rtrim(ltrim(@no))
SET @dep				=   rtrim(ltrim(@dep))
SET @site				=   rtrim(ltrim(@site))
SET @email				=   rtrim(ltrim(@email))
SET @lastChangeDate		=   rtrim(ltrim(@lastChangeDate))
SET @lastChangeTime		=   rtrim(ltrim(@lastChangeTime))
SET @orderStamp         =	rtrim(ltrim(ISNULL(@orderStamp,'')))

DECLARE @orderBy VARCHAR(8000)
DECLARE @PageSize int = 100

if(isnull(@pageNumber,0)<1)
	set @pageNumber = 1

DECLARE @OrderCri VARCHAR(MAX)

IF @orderStamp = ''
BEGIN 
	SET @OrderCri = 'ORDER BY data_alt DESC'
END 
ELSE 
BEGIN 
	SELECT @OrderCri='ORDER BY ' + instructionSql  FROM sortingTableOrder (NOLOCK) WHERE stamp = @orderStamp and inativo=0 and nameService='campaignSearch'
END 


set @orderBy =   @OrderCri +'
				 OFFSET ' + convert(varchar(10),@PageSize) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
				 FETCH NEXT  ' + convert(varchar(10),@PageSize) + ' ROWS ONLY '

			

IF len(@site) > 0
BEGIN
	declare @moeda AS varchar(10)
	SET @moeda = (Select textValue from B_Parameters(nolock) where stamp='ADM0000000260')

	DECLARE @stmt			varchar(8000)
	DECLARE @stmt_no_dep	varchar(500)
	DECLARE @stmt_ref		varchar(500)
	DECLARE @stmt_email		varchar(500)
	DECLARE @sqlcmd			varchar(1500)
	SET @stmt_no_dep	= ''
	SET @stmt_ref		= ''
	SET @stmt_email		= ''

	SET @stmt= 'SELECT 
		 ' + convert(varchar(10),@PageSize) +' as pageSize,  (COUNT(*) OVER() / ' +  convert(varchar(10),@PageSize) +') + 1 as pageTotal, '
			+ convert(varchar(10),@PageNumber) +' as pageNumber, COUNT(*) OVER() as linesTotal
		, descricao AS DesignInfo_designation
		, id AS IdInfo_number
		, site AS Id_site
		, desconto AS FinanceInfo_discountPerc
		, descv AS FinanceInfo_Value
		, maisbarato AS FinanceInfo_discountCheapOnly
		, ''' + @moeda + ''' AS FinanceInfo_currency
		, convert(varchar, data_cri, 23) AS OperationInfo_creationDate
		, convert(varchar, data_cri, 24) AS OperationInfo_creationTime
		, convert(varchar, data_alt, 23) AS OperationInfo_lastChangeDate
		, convert(varchar, data_alt, 24) AS OperationInfo_lastChangetime
		, convert(varchar, dataInicio, 23) AS GeneralInfo_startDate
		, convert(varchar, dataFim, 23) AS GeneralInfo_endDate
		, case when inativo = 0 then 1 else 0 end  AS GeneralInfo_status	
		, B_US.iniciais					AS OperationInfo_lastChangeUser			
	from
		campanhas (nolock)
		INNER JOIN B_US(nolock) ON  campanhas.id_us_alt = userno
	where
			(campanhas.site = '''+ @site + ''')
			and campanhas.inativo = 0
			and campanhas.eliminada = 0
			and data_alt >= COALESCE(''' + @lastChangeDate + ' ' + @lastChangeTime + ''', data_alt)
			and id =  COALESCE(NULLIF(''' + @id + ''',''''), id) '
	
	print @stmt
	IF len(@email)>0 
	BEGIN		
		declare @no_	int
		declare @estab_ int

		IF len(@no)>0 and len(@dep)>0
			select @no_ = no, @estab_ = estab from b_utentes where email = @email and no=@no and estab=@dep
		ELSE
			select top 1 @no_ = no, @estab_ = estab from b_utentes where email = @email
		--print(@no_)
		--print(@estab_)
		SET @stmt_email = (select replace(campanhas,')(',',') from campanhas_ut_lista where no = @no_ and estab = @estab_)
		SET @stmt_email = ' and id in ' + isnull(@stmt_email,'(-1)')	
	END
	
	IF (len(@no)>0 or @no>0) and (len(@dep)>=0 or @dep>=0)
	BEGIN		
		SET @stmt_no_dep = (select replace(campanhas,')(',',') from campanhas_ut_lista where no = @no and estab = @dep)
		--print(@stmt_no_dep)
		if (rtrim(ltrim(@stmt_no_dep))='' or @stmt_no_dep=' '  or @stmt_no_dep=NULL)  
		begin
			set @stmt_no_dep='(-1)'
		end
		SET @stmt_no_dep = ' and id in ' + @stmt_no_dep	
	END

	IF len(@ref)>0
	BEGIN		
		SET @stmt_ref = (select replace(campanhas,')(',',') from campanhas_st_lista where ref = @ref)
		if (rtrim(ltrim(@stmt_ref))='' or @stmt_ref=' '  or @stmt_ref=NULL)  
		begin
			set @stmt_ref='(-1)'
		end

		SET @stmt_ref = ' and id in ' + @stmt_ref		
	END

	SET @sqlcmd = @stmt + @stmt_no_dep + @stmt_ref + @stmt_email + @orderBy
	--print(@sqlcmd)
	EXEC(@sqlcmd)
END
ELSE
BEGIN
	print('Missing site')
END

Go
Grant Execute on dbo.up_campanhas_PesquisaCampanhas to Public
Grant Control on dbo.up_campanhas_PesquisaCampanhas to Public
Go
