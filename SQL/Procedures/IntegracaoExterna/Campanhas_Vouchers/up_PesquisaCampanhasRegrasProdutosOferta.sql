/*
Pesquisa Regras Produtos Oferta
J.Gomes, 2020-04-18
--------------------------------

Listagem de todas as regras existentes para os produtos de oferta para uma campanha 
em específico (@idcamp).

Pesquisa nas tabelas: 	campanhas_pOfertas por 1 campos:
	@IdCamp
	
Campos obrigatórios: @IdCamp

OUTPUT:
---------
	   RulesProductOffer_type
	 , RulesProductOffer_design
	 , RulesProductOffer_family
	 , RulesProductOffer__lab
	 , RulesProductOffer_brand
	 , RulesProductOffer_inn
	 , RulesProductOffer_service
	 , RulesProductOffer_operator
	 , RulesProductOffer_generic
	 , RulesProductOffer_status
	 , RulesProductOffer_parapharmacy
	 , RulesProductOffer_qtt
	 , RulesProductOffer_stock
	 , RulesProductOffer_desrc


EXECUÇÃO DA SP:
---------------
exec up_PesquisaCampanhasRegrasProdutosOferta '@IdCamp'


-- exec up_PesquisaCampanhasRegrasProdutosOferta ''
-- exec up_PesquisaCampanhasRegrasProdutosOferta 176

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_PesquisaCampanhasRegrasProdutosOferta]') IS NOT NULL
	drop procedure dbo.up_PesquisaCampanhasRegrasProdutosOferta
go

create procedure dbo.up_PesquisaCampanhasRegrasProdutosOferta
	@IdCamp			varchar(18)

AS

SET NOCOUNT ON

SET @IdCamp		=   rtrim(ltrim(isnull(@IdCamp,0)))

IF len(@IdCamp) > 0
BEGIN
SELECT 
	 case when excecao=0 then 'Regra' else 'Exceção' end AS RulesProductOffer_type
	 , artigo AS RulesProductOffer_design
	 , familia AS RulesProductOffer_family
	 , lab AS RulesProductOffer__lab
	 , marca AS RulesProductOffer_brand
	 , dci AS RulesProductOffer_inn
	 , servicos AS RulesProductOffer_service
	 , OperadorLogico AS RulesProductOffer_operator
	 , generico AS RulesProductOffer_generic
	 , Inactivo AS RulesProductOffer_status
	 , Parafarmacia AS RulesProductOffer_parapharmacy
	 , qt AS RulesProductOffer_qtt
	 , stock AS RulesProductOffer_stock
	 , regra AS RulesProductOffer_desrc
	  from campanhas_pOfertas(nolock) 
	 where campanhas_id in (@IdCamp)
END
ELSE
	print('Missing @IdCamp')

Go
Grant Execute on dbo.up_PesquisaCampanhasRegrasProdutosOferta to Public
Grant Control on dbo.up_PesquisaCampanhasRegrasProdutosOferta to Public
Go
