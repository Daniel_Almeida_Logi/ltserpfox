/*
Pesquisa Regras Produtos 
J.Gomes, 2020-04-18
--------------------------------

Listagem de todas as regras existentes para os produtos para uma campanha em específico (@idcamp).

Pesquisa nas tabelas: 	campanhas_st por 1 campos:
	@IdCamp
	
Campos obrigatórios: @IdCamp

OUTPUT:
---------
	   RulesProduct_type
	 , RulesProduct_design
	 , RulesProduct_family
	 , RulesProduct__lab
	 , RulesProduct_brand
	 , RulesProduct_inn
	 , RulesProduct_service
	 , RulesProduct_operator
	 , RulesProduct_generic
	 , RulesProduct_status
	 , RulesProduct_parapharmacy
	 , RulesProduct_qtt
	 , RulesProduct_stock
	 , RulesProduct_desrc


EXECUÇÃO DA SP:
---------------
exec up_PesquisaCampanhasRegrasProdutos '@IdCamp'


-- exec up_PesquisaCampanhasRegrasProdutos ''
-- exec up_PesquisaCampanhasRegrasProdutos 206

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_PesquisaCampanhasRegrasProdutos]') IS NOT NULL
	drop procedure dbo.up_PesquisaCampanhasRegrasProdutos
go

create procedure dbo.up_PesquisaCampanhasRegrasProdutos
	@IdCamp			varchar(18)

AS

SET NOCOUNT ON

SET @IdCamp		=   rtrim(ltrim(isnull(@IdCamp,0)))

IF len(@IdCamp) > 0
BEGIN
SELECT 
	 case when excecao=0 then 'Regra' else 'Exceção' end AS RulesProduct_type
	 , artigo AS RulesProduct_design
	 , familia AS RulesProduct_family
	 , lab AS RulesProduct__lab
	 , marca AS RulesProduct_brand
	 , dci AS RulesProduct_inn
	 , servicos AS RulesProduct_service
	 , OperadorLogico AS RulesProduct_operator
	 , generico AS RulesProduct_generic
	 , Inactivo AS RulesProduct_status
	 , Parafarmacia AS RulesProduct_parapharmacy
	 , qt AS RulesProduct_qtt
	 , stock AS RulesProduct_stock
	 , regra AS RulesProduct_desrc
	  from campanhas_st(nolock) 
	 where campanhas_id in (@IdCamp)
END
ELSE
	print('Missing @IdCamp')

Go
Grant Execute on dbo.up_PesquisaCampanhasRegrasProdutos to Public
Grant Control on dbo.up_PesquisaCampanhasRegrasProdutos to Public
Go
