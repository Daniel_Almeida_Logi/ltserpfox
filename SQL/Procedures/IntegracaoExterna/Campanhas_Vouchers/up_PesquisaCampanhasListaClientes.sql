/*
Pesquisa Campanhas Lista Clientes 
J.Gomes, 2020-04-20
--------------------------------

Listagem de todas os clientes existentes numa campanha em específico (@idcamp).

Pesquisa nas tabelas: campanhas_ut_lista por 3 campos:
	@IdCamp, @no, @dep
	
Campos obrigatórios: @IdCamp

OUTPUT:
---------
	   ClientInfo_Refs
	   

EXECUÇÃO DA SP:
---------------
exec up_PesquisaCampanhasListaClientes '@IdCamp', @no, @dep


-- exec up_PesquisaCampanhasListaClientes '','',''
-- exec up_PesquisaCampanhasListaClientes 2,201,0
-- exec up_PesquisaCampanhasListaClientes 208,'1078900',''
-- exec up_PesquisaCampanhasListaClientes '208','1078900','2'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_PesquisaCampanhasListaClientes]') IS NOT NULL
	drop procedure dbo.up_PesquisaCampanhasListaClientes
go

create procedure dbo.up_PesquisaCampanhasListaClientes
	@IdCamp			varchar(18),
	@no				varchar(10),
	@dep			varchar(3)

AS

SET NOCOUNT ON

SET @IdCamp		=   rtrim(ltrim(isnull(@IdCamp,0)))
SET @no			=   rtrim(ltrim(@no))
SET @dep		=   rtrim(ltrim(@dep))

IF len(@IdCamp) > 0
BEGIN
	SELECT no AS ClientInfo_number, estab AS ClientInfo_dep from campanhas_ut_lista(nolock)
		 where campanhas like '%' + @IdCamp + '%'
		AND no = COALESCE(NULLIF(@no,''), no)
		AND estab = COALESCE(NULLIF(@dep,''), estab)
END
ELSE
	print('Missing @IdCamp')

Go
Grant Execute on dbo.up_PesquisaCampanhasListaClientes to Public
Grant Control on dbo.up_PesquisaCampanhasListaClientes to Public
Go
