/*
Pesquisa Regras Clientes 
J.Gomes, 2020-04-18
--------------------------------

Listagem de todas as regras existentes aplicadas aos clientes para uma campanha 
em específico (@idcamp).

Pesquisa nas tabelas: 	campanhas_ut por 1 campos:
	@IdCamp
	
Campos obrigatórios: @IdCamp

OUTPUT:
---------
	 RulesClient_type
	 , RulesClient_desrc
	 , RulesClient_name
	 , RulesClient_number
	 , RulesClient_dep
	 , RulesClient_address
	 , RulesClient_vatNr
	 , RulesClient_cardNr
	 , RulesClient_type
	 , RulesClient_job
	 , RulesClient_sex
	 , RulesClient_operator
	 , RulesClient_age
	 , RulesClient_pathology
	 , RulesClient_phone
	 , RulesClient_medicalNr
	 , RulesClient_SSNr	
	 , RulesClient_status



EXECUÇÃO DA SP:
---------------
exec up_PesquisaCampanhasRegrasClientes '@IdCamp'


-- exec up_PesquisaCampanhasRegrasClientes ''
-- exec up_PesquisaCampanhasRegrasClientes 202

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_PesquisaCampanhasRegrasClientes]') IS NOT NULL
	drop procedure dbo.up_PesquisaCampanhasRegrasClientes
go

create procedure dbo.up_PesquisaCampanhasRegrasClientes
	@IdCamp			varchar(18)

AS

SET NOCOUNT ON

SET @IdCamp		=   rtrim(ltrim(isnull(@IdCamp,0)))

IF len(@IdCamp) > 0
BEGIN
SELECT 
	 case when excecao=0 then 'Regra' else 'Exceção' end AS RulesClient_type
	 , regra AS RulesClient_desrc
	 , nome AS RulesClient_name
	 , no As RulesClient_number
	 , estab AS RulesClient_dep
	 , morada AS RulesClient_address
	 , ncont AS RulesClient_vatNr
	 , ncartao AS RulesClient_cardNr
	 , tipo AS RulesClient_typeCl
	 , profissao AS RulesClient_job
	 , sexo AS RulesClient_sex
	 , operador AS RulesClient_operator
	 , idade AS RulesClient_age
	 , Patologia As RulesClient_pathology
	 , Tlf AS RulesClient_phone
	 , Nbenef AS RulesClient_medicalNr
	 , Nbenef2 AS RulesClient_SSNr	
	 , Inactivo AS RulesClient_status	 
	  from campanhas_ut(nolock)
	 where campanhas_id in (@IdCamp)
END
ELSE
	print('Missing @IdCamp')


	select * from campanhas_ut(nolock)

Go
Grant Execute on dbo.up_PesquisaCampanhasRegrasClientes to Public
Grant Control on dbo.up_PesquisaCampanhasRegrasClientes to Public
Go
