/*
Pesquisa Vales (Vouchers)
J.Gomes, 2020-04-15
--------------------------------

Listagem de Clientes com vouchers (vales), baseada na pesquisa por produto (@IdInfo_ref),
pesqueisa por cliente (@ClientInfo_number, @ClientInfo_dep, @ClientInfo_email), e ainda 
permite filtrar a listagem acima de determinada data e hora de alteração.

Pesquisa nas tabelas: 	B_fidelvale, b_utentes, B_fidelst por 6 campos:
	@IdInfo_ref
	@ClientInfo_number
	@ClientInfo_dep
	@ClientInfo_email
	@OperationInfo_lastChangeDate
	@OperationInfo_lastChangeTime
	
Campos obrigatórios: nenhum

OUTPUT:
---------
IdInfo_ref,	IdInfo_number, ClientInfo_number, ClientInfo_name, ClientInfo_dep
, ClientInfo_card,	FinanceInfo_discountValue, FinanceInfo_currency, OperationInfo_CreationDate
, OperationInfo_Creationtime, OperationInfo_lastChangeDate,	OperationInfo_lastChangetime
, GeneralInfo_expirationDate, GeneralInfo_status, GeneralInfo_type



EXECUÇÃO DA SP:
---------------
exec up_cartao_PesquisarVales '@IdInfo_ref', '@ClientInfo_number', '@ClientInfo_dep'
, '@ClientInfo_email','@OperationInfo_lastChangeDate','@OperationInfo_lastChangeTime'


-- exec up_cartao_PesquisarVales '', '', '', '','','',''
-- exec up_cartao_PesquisarVales NULL,NULL,NULL,NULL,NULL,NULL,NULL
-- exec up_cartao_PesquisarVales '', '201', '','','2019-11-12', '',0
-- exec up_cartao_PesquisarVales '', '201', '','','2019-11-12', '',0,'B1FD8301-4F86-4A67-AC91-C403FC9F461E'
-- exec up_cartao_PesquisarVales '', '', '','','2019-11-12', '',0,'FCAEF05F-33EE-49FB-B639-C1178F97B598',1

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cartao_PesquisarVales]') IS NOT NULL
	drop procedure dbo.up_cartao_PesquisarVales
go

create procedure dbo.up_cartao_PesquisarVales
@IdInfo_ref							varchar(18),
@ClientInfo_number					varchar(10),
@ClientInfo_dep						varchar(3),
@ClientInfo_email					varchar(45),
@OperationInfo_lastChangeDate		datetime = '1900-01-01',
@OperationInfo_lastChangeTime		varchar(8) = '00:00:00',
@pageNumber                         int = 1,
@orderStamp							VARCHAR(36) = '',
@activeOnly						    bit = 0


AS

SET NOCOUNT ON

SET @IdInfo_ref						=   rtrim(ltrim(ISNULL(@IdInfo_ref,'')))
SET @ClientInfo_number				=   rtrim(ltrim(ISNULL(@ClientInfo_number,'')))
SET @ClientInfo_dep					=   rtrim(ltrim(ISNULL(@ClientInfo_dep,'')))
SET @ClientInfo_email				=   rtrim(ltrim(ISNULL(@ClientInfo_email,'')))
SET @OperationInfo_lastChangeDate	=   rtrim(ltrim(ISNULL(@OperationInfo_lastChangeDate,'1900-01-01')))
SET @OperationInfo_lastChangeTime	=   rtrim(ltrim(ISNULL(@OperationInfo_lastChangeTime,'00:00:00')))
SET @orderStamp					    =	rtrim(ltrim(ISNULL(@orderStamp,'')))

declare @moeda AS varchar(10)

SET @moeda = (Select textValue from B_Parameters(nolock) where stamp='ADM0000000260')



DECLARE @orderBy VARCHAR(8000)
DECLARE @PageSize int = 100
DECLARE @sql varchar(max) =''


if(isnull(@pageNumber,0)<1)
	set @pageNumber = 1

DECLARE @OrderCri VARCHAR(MAX)=''
DECLARE @validationDateInc int = 0


IF @orderStamp = ''
BEGIN 
	SET @OrderCri = 'ORDER BY usrdata DESC'

END 
ELSE 
BEGIN 
	SELECT @OrderCri='ORDER BY ' + instructionSql  FROM sortingTableOrder (NOLOCK) WHERE stamp = @orderStamp and inativo=0 and nameService='voucherSearch'
END 


	select @validationDateInc = isnull(numValue,0) from B_Parameters(nolock) where stamp='ADM0000000017'


set @orderBy = ' ) X ' + @OrderCri +'  
				 OFFSET ' + convert(varchar(10),@PageSize) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
				 FETCH NEXT  ' + convert(varchar(10),@PageSize) + ' ROWS ONLY '


	select @sql = N' SELECT '  + convert(varchar(10),@PageSize) +' as pageSize,  (COUNT(*) OVER() / ' +  convert(varchar(10),@PageSize) +') + 1 as pageTotal, '
				 + convert(varchar(10),@PageNumber) +' as pageNumber, COUNT(*) OVER() as linesTotal		
				, * from (SELECT
				  ref AS IdInfo_ref
				, nr AS IdInfo_number
				, b_utentes.no AS ClientInfo_number
				, b_utentes.nome AS ClientInfo_name
				, b_utentes.estab AS ClientInfo_dep
				, b_utentes.nrcartao AS ClientInfo_card
				, valor AS FinanceInfo_discountValue
				, ''' + @moeda + ''' AS FinanceInfo_currency
				, convert(varchar, vale.ousrdata, 23) AS OperationInfo_CreationDate
				, convert(varchar, vale.ousrdata, 24) AS OperationInfo_Creationtime
				, convert(varchar, vale.usrdata, 23) AS OperationInfo_lastChangeDate
				, convert(varchar, vale.usrdata, 24) AS OperationInfo_lastChangetime
				, convert(varchar, DATEADD(day, '+convert(varchar(10),@validationDateInc)+', vale.ousrdata), 23) AS GeneralInfo_expirationDate
				, b_us.iniciais    AS OperationInfo_lastChangeUser
				, case when vale.abatido=0 and vale.anulado = 0 and isnull(B_fidel.inactivo,0) = 0 then 1 else 0 end AS GeneralInfo_status
				, b_utentes.tipo AS GeneralInfo_type
				,vale.usrdata			
			from
					B_fidelvale vale (nolock)
					inner join b_utentes (nolock) on b_utentes.utstamp=vale.clstamp
					left join B_fidel (nolock) on B_fidel.clstamp=b_utentes.utstamp
					INNER JOIN b_us   (nolock) ON vale.usr = b_us.userno
			where
					b_utentes.inactivo=0 
			 AND ref = COALESCE(NULLIF('''+ @IdInfo_ref +''',''''), ref)
			 AND no =  COALESCE(NULLIF('''+@ClientInfo_number +''',''''), no)
			 AND estab = COALESCE(NULLIF('''+@ClientInfo_dep +''',''''), estab)
			 AND b_utentes.email like COALESCE(NULLIF('''+@ClientInfo_email +''',''''), b_utentes.email) + '+ '''%'' ' +
			'AND vale.usrdata >= '''+ CONVERT(VARCHAR(10), @OperationInfo_lastChangeDate, 120) + ' ' + CONVERT(VARCHAR(8), @OperationInfo_lastChangeTime, 114) + '''
			'
			
		IF(@activeOnly=1)
		BEGIN
			SET @sql = @sql  + ' AND vale.abatido =  0   AND vale.anulado =  0  and isnull(B_fidel.inactivo,0) = 0'
		END

		select @sql = @sql + @orderBy
		
		print @sql
		EXECUTE (@sql)

Go
Grant Execute on dbo.up_cartao_PesquisarVales to Public
Grant Control on dbo.up_cartao_PesquisarVales to Public
Go
