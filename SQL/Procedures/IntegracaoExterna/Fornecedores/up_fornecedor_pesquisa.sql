
/*

Daniel Almeida - 20210707

Sp retorna lista de fornecedores


select * from fl(nolock)

EXEC up_fornecedor_pesquisa null,null,null,null,null,null,1000,1



*/






SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO




if OBJECT_ID('[dbo].[up_fornecedor_pesquisa]') IS NOT NULL
	drop procedure dbo.up_fornecedor_pesquisa
go



create procedure dbo.up_fornecedor_pesquisa
	 @name            varchar(80)
	,@vat             varchar(20)
	,@number          int 
	,@dep             int
	,@lastChangeDate  datetime
	,@lastChangeTime  varchar(8)
	,@topMax          int 
	,@pageNumber      int



/* WITH ENCRYPTION */
AS
	


	SET @name			=   rtrim(ltrim(isnull(@name,'')))
	SET @vat			=   rtrim(ltrim(isnull(@vat,'')))
	SET @pageNumber     =	isnull(@pageNumber,1)
	SET @number         =	isnull(@number,-1)
	SET @dep            =	isnull(@dep,-1)
	SET @topMax			=   isnull(@topMax,1000)
	SET @lastChangeDate =	rtrim(ltrim(isnull(@lastChangeDate,'19000101')))
	SET @lastChangeTime =	rtrim(ltrim(isnull(@lastChangeTime,'00:00:00')))
	SET @pageNumber     =	isnull(@pageNumber,1)

	DECLARE @lastChangeDateTime VARCHAR(20)
	DECLARE @siteId				VARCHAR(100) = ''

	SELECT TOP 1 @siteId = id_lt from empresa(nolock) order by no

	set @lastChangeDateTime = @lastChangeDate + @lastChangeTime
 

	if(@topMax>1000)
	 set @topMax = 1000

	if(@topMax<=0)
		set @topMax = 1

	if(@pageNumber<1)
		set @pageNumber = 1


	DECLARE @orderBy VARCHAR(8000)
	DECLARE @OrderCri VARCHAR(MAX) =''

	SET @OrderCri = 'ORDER BY  isnull(OperationInfo_LastChangeDate,''1900-01-01 00:00:00'') ASC'

	


	set @orderBy = ' ) X ' + @OrderCri +'  
				 OFFSET ' + convert(varchar(10),@topMax) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
				 FETCH NEXT  ' + convert(varchar(10),@topMax) + ' ROWS ONLY '


	DECLARE @sql varchar(max) = ''

    select @sql = 

		'
		
		SELECT '  + convert(varchar(10),@topMax) +' as pageSize,  (COUNT(*) OVER() / ' +  convert(varchar(10),@topMax) +') + 1 as pageTotal, '
				+ convert(varchar(10),@PageNumber) +' as pageNumber, COUNT(*) OVER() as linesTotal		
			,* from (
			SELECT 
				distinct	
					 isnull(no,0)                                    AS IdInfo_number
					,isnull(estab,0)                                 AS IdInfo_dep
					,'''+LTRIM(RTRIM(@siteId))+'''					 AS IdInfo_siteId
					,rtrim(ltrim(isnull(fl.flstamp,'''')))           AS IdInfo_regstamp
				    ,rtrim(ltrim(isnull(fl.nome,'''')))              AS EntityInfo_name
					,rtrim(ltrim(isnull(fl.nome2,'''')))             AS EntityInfo_name1
					,rtrim(ltrim(isnull(fl.morada,'''')))            AS AddressInfo_address
					,rtrim(ltrim(isnull(fl.local,'''')))             AS AddressInfo_city
					,rtrim(ltrim(isnull(fl.codpost,'''')))           AS AddressInfo_zipCode
					,rtrim(ltrim(isnull(fl.zona,'''')))              AS AddressInfo_zone
					,rtrim(ltrim(isnull(fl.telefone,'''')))          AS EntityInfo_phone
					,rtrim(ltrim(isnull(fl.tlmvl,'''')))             AS EntityInfo_phone2
					,rtrim(ltrim(isnull(fl.email,'''')))             AS EntityInfo_email
					,rtrim(ltrim(isnull(fl.ncont,'''')))             AS EntityInfo_vat
					,isnull(fl.inactivo,0)                           AS EntityInfo_inactive
			   		,convert(varchar, fl.ousrdata, 23)               AS OperationInfo_CreationDate 
					,fl.ousrhora                                     AS OperationInfo_CreationTime
					,convert(varchar, fl.usrdata, 23)                AS OperationInfo_LastChangeDate 
					,fl.usrhora									     AS OperationInfo_LastChangeTime
					,fl.usrinis									     AS OperationInfo_lastChangeUser
			FROM 
				fl(nolock) 
			WHERE 				
				convert(datetime,convert(varchar(10),fl.usrdata,120) + '' '' + fl.usrhora) >= ''' + @lastChangeDateTime + ''''
		

	if(ltrim(rtrim(@name))!='')
		SET @sql = @sql +  ' and fl.nome like ''%' + ltrim(rtrim(@name)) +'%'''

	if(ltrim(rtrim(@vat))!='')
		SET @sql = @sql +  ' and fl.ncont= ''' + ltrim(rtrim(@vat)) +''''

	if(isnull(@number,-1)>0)
		SET @sql = @sql +  ' and fl.no = '''    + rtrim(ltrim(STR(isnull(@number,-1)))) +''''

	if(isnull(@dep,-1)>=0)
		SET @sql = @sql +  ' and fl.estab = ''' + rtrim(ltrim(STR(isnull(@dep,-1)))) +''''

																		
	select @sql = @sql + @orderBy

		
	print @sql

	EXECUTE (@sql)
								
 
GO
Grant Execute On dbo.up_fornecedor_pesquisa to Public
Grant Control On dbo.up_fornecedor_pesquisa to Public
GO
