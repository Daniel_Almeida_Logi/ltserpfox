/* SP Pesquisa Top Categorias Produtos
J.Gomes, 2020-07-07
--------------------------------

Listagem de produtos por categorias, com top, mais vendidos

Pesquisa nas tabelas: 	
Ft, Fi, fprod, categoria_hmr, mercado_hmr
	
Campos obrigatórios: site


OUTPUT:
---------
@pageSize, @pageTotal, @pageNumber, @linesTotal, @idcategoria, @categoria


EXECUÇÃO DA SP:
---------------
exec Top_CategoriasProdutos @max, '@site', '@@dateInit','@dateEnd' , @pageNumber		

	exec Top_CategoriasProdutos   10,'Loja 1', '2018-06-24', '2020-06-25',1
	exec Top_CategoriasProdutos   10,'Loja 1', '', '',1
	*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[Top_CategoriasProdutos]') IS NOT NULL
	DROP PROCEDURE [dbo].Top_CategoriasProdutos ;
GO

CREATE PROCEDURE [dbo].[Top_CategoriasProdutos]	
	 @max				int = 10
	,@site				VARCHAR(60)	
	,@dateInit			varchar(10) = '1900-01-01'
	,@dateEnd	        varchar(10)  = '3000-01-01'
	,@pageNumber        int = 1
	,@saleOnly          bit = 1
	,@imageOnly         bit = 0

	   
/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

SET @max				=   rtrim(ltrim(isnull(nullif(@max,''),10)))
SET @site				=   rtrim(ltrim(isnull(@site,'')))
SET @dateInit		    =   rtrim(ltrim(isnull(@dateInit,'1900-01-01')))
SET @dateEnd		    =   rtrim(ltrim(isnull(@dateEnd,'3000-01-01')))


if(isnull(@pageNumber,0)<1)
	set @pageNumber = 1	

DECLARE @sql varchar(max)
DECLARE @orderBy VARCHAR(8000)
DECLARE @PageSize int = 1000000

set @orderBy = ' ) X 
				 ORDER BY total DESC
				 OFFSET ' + convert(varchar(10),@PageSize) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
				 FETCH NEXT  ' + convert(varchar(10),@PageSize) + ' ROWS ONLY '

select @sql = N' 				
			SELECT '  + convert(varchar(10),@PageSize) +' as pageSize,  (COUNT(*) OVER() / ' +  convert(varchar(10),@PageSize) +') + 1 as pageTotal, '
				+ convert(varchar(10),@PageNumber) +' as pageNumber, COUNT(*) OVER() as linesTotal		
			, id, designation 
			FROM (
						select top ' + convert(varchar, @max) + ' id, designation, sum(total) total  from
							(
								select fdata, A.ftstamp, B.etiliquido as total, B.ref as bref,K.ref as kref, K.id , K.designation, K.idmercado, K.mercado from ft(nolock) A
								inner join fi(nolock) B on B.ftstamp = A.ftstamp
								inner join 
									(
									select ISNULL(ref,'''') as ref, ISNULL(D.id,0) as id, ISNULL(D.descr,'''') as designation, ISNULL(E.id,0) as idmercado, ISNULL(E.descr,'''') as mercado from fprod(nolock) C 
									left join categoria_hmr(nolock) D on D.id = C.id_categoria_hmr
									left join mercado_hmr(nolock) E on E.id = C.id_mercado_hmr
									
									) K
								on K.ref = B.ref
								left join st(nolock) on st.ref=k.ref
								where A.tipodoc = 1 and st.dispOnline = 1 and K.id>0
					'


if @dateInit != '' and @dateEnd  != ''
	select @sql = @sql + N' AND A.fdata BETWEEN ''' + @dateInit + '''  AND  ''' + @dateEnd + ''''

if @site != ''
	select @sql = @sql + N'	AND A.[site] = '''+@site+''' '


set @sql = @sql + N' ) CC
							GROUP BY id, designation
							ORDER BY total DESC
							'


select @sql = @sql + @orderBy
	   
print @sql
EXECUTE (@sql)



GO
GRANT EXECUTE on dbo.Top_CategoriasProdutos TO PUBLIC
GRANT Control on dbo.Top_CategoriasProdutos TO PUBLIC
GO