/* SP Pesquisa Top Marcas Produtos
J.Gomes, 2020-07-07
--------------------------------

Listagem de produtos por marcas, com top, mais vendidos

Pesquisa nas tabelas: 	
Ft, Fi, st
	
Campos obrigatórios: site



OUTPUT:
---------
@pageSize, @pageTotal, @pageNumber, @linesTotal, @idmarca, @marca

nota: aqui nao existe idmarca mas devolve -1 como sendo o id


EXECUÇÃO DA SP:
---------------
exec Top_MarcasProdutos @max, '@site', '@@dateInit','@dateEnd' , @pageNumber, @saleOnly, @imageOnly		

	exec Top_MarcasProdutos   16,'Loja 1', '1900-06-24', '2040-06-25',1,1,0

	exec Top_MarcasProdutos   1000,'Loja 1', '2018-06-24', '2020-06-25',1, false

	exec Top_MarcasProdutos   1000,'Loja 1', '2018-06-24', '2020-06-25',1, false, true

	exec Top_MarcasProdutos   1000000,'Loja 1', '2018-06-24', '2020-06-25',1, false, false
	exec Top_MarcasProdutos   10,'Loja 1', '', '',1
	*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[Top_MarcasProdutos]') IS NOT NULL
	DROP PROCEDURE [dbo].Top_MarcasProdutos ;
GO

CREATE PROCEDURE [dbo].[Top_MarcasProdutos]	
	 @max				int = 10000000
	,@site				VARCHAR(60)	
	,@dateInit			varchar(10) = '1900-01-01'
	,@dateEnd	        varchar(10)  = '3000-01-01'
	,@pageNumber        int = 1
	,@saleOnly          bit = 1
	,@imageOnly         bit = 0
	   
/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

SET @max				=   rtrim(ltrim(isnull(nullif(@max,''),10000000)))
SET @site				=   rtrim(ltrim(isnull(@site,'')))
SET @dateInit		    =   rtrim(ltrim(isnull(@dateInit,'1900-01-01')))
SET @dateEnd		    =   rtrim(ltrim(isnull(@dateEnd,'3000-01-01')))
SET @saleOnly           =   isnull(@saleOnly,1)
SET @imageOnly          =   isnull(@imageOnly,0)


if(isnull(@pageNumber,0)<1)
	set @pageNumber = 1	

DECLARE @sql varchar(max)
DECLARE @orderBy VARCHAR(8000)
DECLARE @PageSize int = 100

DECLARE @id_lt varchar(100) =''	
DECLARE @site_nr INT

select @id_lt = rtrim(ltrim(id_lt)), @site_nr= no from empresa(nolock) where site = @site


DECLARE @downloadURL VARCHAR(100) = ''
SELECT  @downloadURL = rtrim(ltrim(textValue)) FROM B_Parameters(nolock) WHERE stamp='ADM0000000313'


DECLARE @marcaURL varchar(300) 
set   @marcaURL = @downloadURL + @id_lt + '/'



set @orderBy = ' ) X 
				 ORDER BY total DESC
				 OFFSET ' + convert(varchar(10),@PageSize) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
				 FETCH NEXT  ' + convert(varchar(10),@PageSize) + ' ROWS ONLY '


if(@saleOnly=1) begin
	select @sql = N' 				
				SELECT '  + convert(varchar(10),@PageSize) +' as pageSize,  (COUNT(*) OVER() / ' +  convert(varchar(10),@PageSize) +') + 1 as pageTotal, '
					+ convert(varchar(10),@PageNumber) +' as pageNumber, 
					image = case when rtrim(ltrim(isnull(image,'''')))!='''' then ''' + @marcaURL +''' + image else '''' end
					,total
				
					, COUNT(*) OVER() as linesTotal		
				,  id, designation
				FROM (
							select top ' + convert(varchar, @max) + ' designation,image, sum(total) total, id  from
								(
										select fdata, A.ftstamp, B.etiliquido as total, B.ref as bref,K.ref as kref,
										 ISNULL(K.usr1,'''') as designation,  marcas.id as id
										 ,isnull(anexos.anexosstamp,'''') as image
										from ft(nolock)  A
										inner join fi(nolock)  B on B.ftstamp = A.ftstamp
										inner join st K(nolock) on K.ref = B.ref
										left join marcas(nolock) on marcas.descr = ISNULL(K.usr1,'''') and K.usr1!=''''
										left join anexos(nolock) on marcas.stamp = anexos.regstamp

										where A.tipodoc = 1 and  k.dispOnline = 1
										and ISNULL(K.usr1,'''')!=''''
						'


	if @dateInit != '' and @dateEnd  != ''
		select @sql = @sql + N' AND A.fdata BETWEEN ''' + @dateInit + '''  AND  ''' + @dateEnd + ''''

	if @site != ''
		select @sql = @sql + N'	AND A.[site] = '''+@site+''' '


end
else begin
	select @sql = N' 				
				SELECT '  + convert(varchar(10),@PageSize) +' as pageSize,  (COUNT(*) OVER() / ' +  convert(varchar(10),@PageSize) +') + 1 as pageTotal, '
					+ convert(varchar(10),@PageNumber) +' as pageNumber, 
					image = case when rtrim(ltrim(isnull(image,'''')))!='''' then ''' + @marcaURL +''' + image else '''' end
					,total
				
					, COUNT(*) OVER() as linesTotal		
				,  id, designation
				FROM (
							select top ' + convert(varchar, @max) + ' designation,image, id,1 as total  from
								(
										select K.ref as kref,
										 ISNULL(K.usr1,'''') as designation,  marcas.id as id
										 ,isnull(anexos.anexosstamp,'''') as image
										from marcas (nolock) 
										inner join st K(nolock)  on marcas.descr = ISNULL(K.usr1,'''') and K.usr1!=''''
										left join anexos(nolock) on marcas.stamp = anexos.regstamp

										where  k.site_nr = '+ convert(varchar(10),@site_nr) +'
										and ISNULL(K.usr1,'''')!=''''
										'
end


if isnull(@imageOnly,0)=1
		select @sql = @sql + N'	AND isnull(anexos.anexosstamp,'''')  != '''' '



set @sql = @sql + N' ) CC
							GROUP BY designation, image, id
							ORDER BY total DESC
							'


select @sql = @sql + @orderBy
	   
--print @sql
EXECUTE (@sql)



GO
GRANT EXECUTE on dbo.Top_MarcasProdutos TO PUBLIC
GRANT Control on dbo.Top_MarcasProdutos TO PUBLIC
GO