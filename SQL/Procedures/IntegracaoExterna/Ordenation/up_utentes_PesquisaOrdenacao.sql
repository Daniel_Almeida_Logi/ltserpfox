/*
PESQUISA ORDENACAO 
Jose Simoes, 12-06-2020

Pesquisa detalhe a partir de 2 campos de pesquisa:
	nameService,site 



PROCEDURE
Criada procedure up_utentes_PesquisaOrdenacao com parâmetros 

Parametros: up_utentes_PesquisaOrdenacao 'nameService','site'
up_utentes_PesquisaOrdenacao '','','','',''
up_utentes_PesquisaOrdenacao '','Loja 1','','',''
up_utentes_PesquisaOrdenacao 'productDetail','Loja 1','','',''
up_utentes_PesquisaOrdenacao 'productDetail','Loja 1','','',''
up_utentes_PesquisaOrdenacao '','','','','2'
up_utentes_PesquisaOrdenacao null,null,null,null,null

*/



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_PesquisaOrdenacao]') IS NOT NULL
    drop procedure up_utentes_PesquisaOrdenacao
go

CREATE PROCEDURE up_utentes_PesquisaOrdenacao
	@nameService varchar(200), 
	@site varchar(60),
	@lastChangeDate	varchar(10) = '1900-01-01',
	@lastChangeTime	varchar(8)  = '00:00:00',
	@pageNumber        int = 1

AS

	DECLARE @sqlCommand VARCHAR(5000) 
	DECLARE @PageSize int = 100
	SET @nameService = isnull(@nameService,'')
	SET @lastChangeDate = isnull(@nameService,'1900-01-01')
	SET @lastChangeTime = isnull(@nameService,'00:00:00')

	SET @site = isnull(@site,'')

	IF(isnull(@pageNumber,0)<1)
		SET @pageNumber = 1

	SET @sqlCommand = '
	 SELECT '  
			+ convert(varchar(10),@PageSize) +							  ' AS pageSize,'
			+'(COUNT(*) OVER() / ' +  convert(varchar(10),@PageSize) +')+ 1 AS pageTotal, '
			+ convert(varchar(10),@PageNumber) +						  ' AS pageNumber,' 
			+'	COUNT(*) OVER()												AS linesTotal,		
			stamp															AS stamp ,
			NameService														AS nameService ,
			description														AS description ,
			criteria														AS criteria, 
			site															AS site,
			convert(varchar, STO.usrdata, 23)								AS OperationInfo_CreationDate,
			STO.usrhora														AS OperationInfo_CreationTime,
		    convert(varchar, STO.usrdata, 23)								AS OperationInfo_lastChangeDate,
		    STO.usrinis														AS OperationInfo_lastChangeUser, 
		    STO.usrhora														AS OperationInfo_lastChangeTime
		FROM sortingTableOrder STO
		WHERE inativo=0 ' 
		if @nameService != '' 
			set @sqlCommand = @sqlCommand +'AND nameService ='''+ @nameService +'''' 
		if @site != '' 
			set @sqlCommand = @sqlCommand  +' AND site = '''+ @site +''''
		if @lastChangeDate != ''
			set @sqlCommand = @sqlCommand  select @sqlCommand = @sqlCommand + N' AND STO.usrdata + STO.usrhora >= ''' + @lastChangeDate + ' ' + @lastChangeTime + '''
	ORDER by site asc,nameService asc OFFSET ' 
	+ convert(varchar(10),@PageSize) + ' * ('+ convert(varchar(10),@PageNumber) +' - 1) ROWS
	  FETCH NEXT  ' + convert(varchar(10),@PageSize) + ' ROWS ONLY '

	PRINT @sqlCommand 
	EXEC (@sqlCommand)


/*
ELSE
BEGIN
	print('Tem de preencher pelo menos um campo de pesquisa com valores numéricos')
END
*/


GO
Grant Execute On up_utentes_PesquisaOrdenacao to Public
Grant Control On up_utentes_PesquisaOrdenacao to Public
go
