/*
	 exec up_ga_loteDetalhePrecos '','',3

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_ga_loteDetalhePrecos]') IS NOT NULL
	drop procedure dbo.up_ga_loteDetalhePrecos
go

create PROCEDURE dbo.up_ga_loteDetalhePrecos
@lote varchar(30)
,@ref varchar(18)
,@tipoDoc varchar(18) = ''


/* WITH ENCRYPTION */
AS

SET NOCOUNT ON


	;with cteStockLote as (
		Select 
			ref
			,lote
			,armazem
			,stock = isnull(sum(case when cm < 50 then qtt else -qtt end),0)
			,ousrdata = MIN(ousrdata)
			,ousrhora = MIN(ousrhora)
		from 
			sl (nolock) 
		where 
			lote like @lote + '%'
			and	ref = @ref
		Group by
			ref
			,lote
			,armazem
		Having
			isnull(sum(case when cm < 50 then qtt else -qtt end),0) > 0
	)	

					
	select 
		sel = CONVERT(bit,0)
		,cteStockLote.ref
		,cteStockLote.lote
		,cteStockLote.stock
		,epv1
		,pct = epcult
		,cteStockLote.armazem
		,data = isnull(convert(varchar,convert(date, st.ousrdata)),'190000101')
		,ousrhora = isnull(st.ousrhora,'00:00:00')
		,usrdata = isnull(convert(varchar,convert(date, st.usrdata)),'190000101')
		,usrhora = isnull(st.usrhora,'00:00:00')
		,qttAtribuida = 0
	from 
		cteStockLote 
		inner join st (nolock) on cteStockLote.ref = st.ref

		
GO
Grant Execute On dbo.up_ga_loteDetalhePrecos to Public
Grant Execute On dbo.up_ga_loteDetalhePrecos to Public
go
