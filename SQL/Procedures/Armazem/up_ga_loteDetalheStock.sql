/*
	exec up_ga_loteDetalheStock 'lote98'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_ga_loteDetalheStock]') IS NOT NULL
	drop procedure dbo.up_ga_loteDetalheStock
go

create PROCEDURE dbo.up_ga_loteDetalheStock
@lote varchar(30)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select 
	'stock' = convert(varchar,convert(numeric(13,0), stock))
	,'ultmov' = convert(varchar,convert(date, usrdata))
	,armazem
	,usrhora
from 
	st_lotes (nolock)
where 
	lote = @lote
order by 
	usrdata + usrhora desc

GO
Grant Execute On dbo.up_ga_loteDetalheStock to Public
Grant Execute On dbo.up_ga_loteDetalheStock to Public
go

