/* 

exec up_ga_lotesPesquisa '', '', '', 'Loja 1'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_ga_lotesPesquisa]') IS NOT NULL
	drop procedure dbo.up_ga_lotesPesquisa
go

create PROCEDURE dbo.up_ga_lotesPesquisa
@lote varchar(30),
@ref varchar(18),
@design varchar(60),
@site varchar(60)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select 
	st_lotes.ref
	,'lote' = '[' + CONVERT(varchar,armazem) + ']' + lote
	,'stock' = convert(varchar,convert(numeric(13,0),st_lotes.stock))
	,'validade' = LEFT(convert(date,st_lotes.validade),7)
	,'ultmov' = convert(varchar,convert(date, st_lotes.usrdata))
	,st_lotes.armazem
	,st.design
	,'nomeLote' = lote
from st_lotes (nolock)
inner join st (nolock) on st.ref = st_lotes.ref
where 
	st_lotes.ref like case when @ref = '' then st_lotes.ref else @ref end
	and st_lotes.lote like case when @lote = '' then st_lotes.lote else @lote end
	and st.design like case when @design = '' then st.design else @design end
	and st_lotes.site = case when @site = '' then st_lotes.site else @site end
order by 
	st_lotes.usrdata + st_lotes.usrhora desc

GO
Grant Execute On dbo.up_ga_lotesPesquisa to Public
Grant Execute On dbo.up_ga_lotesPesquisa to Public
go


