
/*
	exec up_ga_loteDetalhe 'loteg'

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_ga_loteDetalhe]') IS NOT NULL
	drop procedure dbo.up_ga_loteDetalhe
go

create PROCEDURE dbo.up_ga_loteDetalhe
@lote varchar(30)
,@site_nr tinyint

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select 
	st_lotes.stamp
	,st_lotes.lote
	,st_lotes.ref
	,st.design
	,'validade' = LEFT(convert(date,st_lotes.validade),7)
from 
	st_lotes (nolock)
	inner join st (nolock) on st.ref = st_lotes.ref
where 
	lote = @lote
	and st.site_nr = @site_nr 

GO
Grant Execute On dbo.up_ga_loteDetalhe to Public
Grant Execute On dbo.up_ga_loteDetalhe to Public
go