/*
	
	exec up_ga_loteUltimoPreco

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_ga_loteUltimoPreco]') IS NOT NULL
	drop procedure dbo.up_ga_loteUltimoPreco
go

create PROCEDURE dbo.up_ga_loteUltimoPreco
@lote varchar(30)



/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select 
	top 1 st_lotes.lote
	,st_lotes_precos.pct
	,st_lotes_precos.epv1
	,st_lotes_precos.usrdata
	,st_lotes_precos.usrhora
from 
	st_lotes_precos (nolock)
	inner join st_lotes (nolock) on st_lotes.stamp = st_lotes_precos.stampLote
where 
	stampLote in (select stamp from st_lotes (nolock) where lote = @lote)
order by 
	st_lotes_precos.ousrdata desc,
	st_lotes_precos.ousrhora desc

GO
Grant Execute On dbo.up_ga_loteUltimoPreco to Public
Grant Execute On dbo.up_ga_loteUltimoPreco to Public
go


