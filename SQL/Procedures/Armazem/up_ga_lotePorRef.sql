/*SP para verificar os lotes de uma ref. especifica 
	
	exec up_ga_lotePorRef '5440987'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_ga_lotePorRef]') IS NOT NULL
	drop procedure dbo.up_ga_lotePorRef
go

create PROCEDURE dbo.up_ga_lotePorRef
@ref varchar(18)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select 
	ref
	,'lote' = '[' + CONVERT(varchar,armazem) + ']' + lote
	,'stock' = convert(varchar,convert(numeric(13,0),stock))
	,'validade' = LEFT(convert(date,validade),7)
	,pct
	,epv1
	,'ultmov' = convert(varchar,convert(date, st_lotes.usrdata))
	,'ultpreco' = convert(varchar,convert(date, st_lotes_precos.usrdata))
	,'id' = ROW_NUMBER() over(partition by lote + convert(varchar,armazem) order by lote)
	,'agrupar' = CONVERT(bit,0)
	,'nomeLote' = st_lotes.lote
from 
	st_lotes (nolock)
	left join st_lotes_precos (nolock) on st_lotes.stamp = st_lotes_precos.stampLote
where 
	ref = @ref
order by 
	st_lotes.usrdata + st_lotes.usrhora desc
	,st_lotes.lote
	,st_lotes_precos.usrdata + st_lotes_precos.usrhora desc

GO
Grant Execute On dbo.up_ga_lotePorRef to Public
Grant Execute On dbo.up_ga_lotePorRef to Public
go


