/*

	exec up_vendas_online_getStatusChange 'storepickup_storepickup', 'Multibanco', 'Magento', 'Pendente', -1
	exec up_vendas_online_getStatusChange 'storepickup_storepickup', 'Multibanco', 'Logitools', 'Pendente', 1

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_vendas_online_getStatusChange]') IS NOT NULL
	DROP PROCEDURE dbo.up_vendas_online_getStatusChange
GO

CREATE PROCEDURE [dbo].[up_vendas_online_getStatusChange]
	@modo_envio VARCHAR(50),
	@pagamento VARCHAR(50),
	@entidade VARCHAR(50),
	@status_ori VARCHAR(50),
	@faturar int = -1
	
AS
	
	SELECT 
		status_Fim 
	FROM
		statusOnlineOrder(nolock)
	WHERE
		modo_envio = @modo_envio
		AND pagamento = @pagamento
		AND entidade = @entidade
		AND status_Ori = @status_ori
		and faturar = (CASE WHEN @faturar = -1 THEN faturar ELSE @faturar END)
	

GO
GRANT EXECUTE ON dbo.up_vendas_online_getStatusChange TO PUBLIC
GRANT CONTROL ON dbo.up_vendas_online_getStatusChange TO PUBLIC
GO