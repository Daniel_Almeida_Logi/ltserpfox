-- Mostrar Todas as Substāncias do Produto (STOUCHPOS)
-- exec up_touch_todasSubst '2134690'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_todasSubst]') IS NOT NULL
	drop procedure dbo.up_touch_todasSubst
go

create procedure dbo.up_touch_todasSubst
	@ref varchar (18)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	Select 
		case when tipo=1 then 'DCI' 
			when tipo=2 or tipo=4 then 'Excipiente'
			when tipo=3 then 'Revestimento'
			else '' end as denominacao,
		case when tipo=1 then '1' 
			when tipo=2 or tipo=4 then '2'
			when tipo=3 then '3'
			else '4' end as ordem,
		descricao, qtt
	from 
		B_moleculasfp (nolock)
		inner join B_moleculas (nolock) on B_moleculasfp.moleculaid=B_moleculas.moleculaid
	where 
		cnp=@ref
	order by 
		ordem

	option (optimize for (@ref unknown))

GO
Grant Execute On dbo.up_touch_todasSubst to Public
Grant Control On dbo.up_touch_todasSubst to Public
GO