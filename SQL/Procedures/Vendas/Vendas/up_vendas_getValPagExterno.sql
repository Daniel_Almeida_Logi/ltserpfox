/* 
	
	exec up_vendas_getValPagExterno 'ADM7FA88F3B-F8E6-45C6'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_vendas_getValPagExterno]') IS NOT NULL
	drop procedure dbo.up_vendas_getValPagExterno
go

create procedure dbo.up_vendas_getValPagExterno
	@ftstamp VARCHAR(25)

AS

	select 
		ISNULL(sum(req.amount), 0) as naoPago
	from 
		b_pagCentral_ext(nolock) as pce
		inner join paymentMethodRequest(nolock) as req on req.token = pce.token
	where 
		nrAtend = (select u_nrAtend from FT(nolock) where ftstamp = @ftstamp)
		and pce.statePaymentID in (-1, 0)

GO
Grant Execute on dbo.up_vendas_getValPagExterno to Public
Grant control on dbo.up_vendas_getValPagExterno to Public
GO