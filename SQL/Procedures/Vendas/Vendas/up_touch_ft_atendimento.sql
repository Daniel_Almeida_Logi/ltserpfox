/* obtem campos da FT

 exec up_touch_ft_atendimento '200461014324P1'
 
 exec up_touch_ft_atendimento ''

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_ft_atendimento]') IS NOT NULL
	drop procedure dbo.up_touch_ft_atendimento
go

create procedure dbo.up_touch_ft_atendimento

@nratend varchar(20)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select 
	ft.ftstamp
	,ft.pais
	,ft.nmdoc
	,ft.fno
	,ft.no
	,(case when b_utentes.removido=0 then ft.nome else '*********' end) as nome
	,(case when b_utentes.removido=0 then ft.morada else '*********' end) as morada
	,(case when b_utentes.removido=0 then ft.local else '*********' end) as local
	,(case when b_utentes.removido=0 then ft.codpost else '*********' end) as codpost
	,(case when b_utentes.removido=0 then ft.ncont else '*********' end) as ncont
	,(case when b_utentes.removido=0 then ft.bino else '*********' end) as bino
	,(case when b_utentes.removido=0 then ft.bidata else '19000101' end) as bidata
	,(case when b_utentes.removido=0 then ft.bilocal else '*********' end) as bilocal
	,(case when b_utentes.removido=0 then ft.telefone else '*********' end) as telefone
	,ft.zona
	,ft.vendedor
	,ft.vendnm
	,ft.fdata
	,ft.ftano
	,ft.pdata
	,ft.carga
	,ft.descar
	,ft.saida
	,ft.ivatx1
	,ft.ivatx2
	,ft.ivatx3
	,ft.fin
	,ft.final
	,ft.ndoc
	,ft.moeda
	,ft.fref
	,ft.ccusto
	,ft.ncusto
	,ft.facturada
	,ft.fnoft
	,ft.nmdocft
	,ft.estab
	,ft.cdata
	,ft.ivatx4
	,ft.segmento
	,ft.totqtt
	,ft.qtt1
	,ft.qtt2
	,ft.tipo
	,ft.cobrado
	,ft.cobranca
	,ft.tipodoc
	,ft.chora
	,ft.ivatx5
	,ft.ivatx6
	,ft.ivatx7
	,ft.ivatx8
	,ft.ivatx9
	,ft.cambiofixo
	,ft.memissao
	,ft.cobrador
	,ft.rota
	,ft.multi
	,ft.cheque
	,ft.clbanco
	,ft.clcheque
	,ft.chtotal
	,ft.echtotal
	,ft.custo
	,ft.eivain1
	,ft.eivain2
	,ft.eivain3
	,ft.eivav1
	,ft.eivav2
	,ft.eivav3
	,ft.ettiliq
	,ft.edescc
	,ft.ettiva
	,ft.etotal
	,ft.eivain4
	,ft.eivav4
	,ft.efinv
	,ft.ecusto
	,ft.eivain5
	,ft.eivav5
	,ft.edebreg
	,ft.eivain6
	,ft.eivav6
	,ft.eivain7
	,ft.eivav7
	,ft.eivain8
	,ft.eivav8
	,ft.eivain9
	,ft.eivav9
	,ft.total
	,ft.totalmoeda
	,ft.ivain1
	,ft.ivain2
	,ft.ivain3
	,ft.ivain4
	,ft.ivain5
	,ft.ivain6
	,ft.ivain7
	,ft.ivain8
	,ft.ivain9
	,ft.ivav1
	,ft.ivav2
	,ft.ivav3
	,ft.ivav4
	,ft.ivav5
	,ft.ivav6
	,ft.ivav7
	,ft.ivav8
	,ft.ivav9
	,ft.ttiliq
	,ft.ttiva
	,ft.descc
	,ft.debreg
	,ft.debregm
	,ft.intid
	,ft.nome2
	,ft.tpstamp
	,ft.tpdesc
	,ft.erdtotal
	,ft.rdtotal
	,ft.rdtotalm
	,ft.cambio
	,ft.site
	,ft.pnome
	,ft.pno
	,ft.cxstamp
	,ft.cxusername
	,ft.ssstamp
	,ft.ssusername
	,ft.anulado
	,ft.virs
	,ft.evirs
	,ft.valorm2
	,ft.ftid
	,ft.ousrinis
	,ft.ousrdata
	,ft.ousrhora
	,ft.usrinis
	,ft.usrdata
	,ft.usrhora
	,ft.u_nratend
	,ft.u_lote2
	,ft.u_lote
	,ft.u_ltstamp2
	,ft.u_nslote2
	,ft.u_nslote
	,ft.u_slote2
	,ft.u_tlote
	,ft.u_tlote2
	,ft.u_ltstamp
	,ft.u_slote
	,ft.u_hclstamp
	,ft.exportado
	,ft.datatransporte
	,ft.localcarga
	,ft.tabIva
	,ft.id_tesouraria_conta
	,ft.pontosVd
	,ft.campanhas
	,td.u_tipodoc
	,td.tiposaft
	,b_utentes.nbenef
	,b_utentes.codigoP
	,b_utentes.pais
	,b_utentes.nascimento
	,localTesouraria = isnull(tesouraria_conta.descr,'')
	,b_utentes.removido
	,b_utentes.no_ext
	,ft.ivatx10
	,ft.ivatx11
	,ft.ivatx12
	,ft.ivatx13
	,ft.eivain10
	,ft.eivain11
	,ft.eivain12
	,ft.eivain13
	,ft.eivav10
	,ft.eivav11
	,ft.eivav12
	,ft.eivav13
from 
	ft (nolock)
	inner join td (nolock) on td.ndoc = ft.ndoc 
	inner join b_utentes (nolock) on b_utentes.no = ft.no and b_utentes.estab = ft.estab
	left join tesouraria_conta (nolock) on tesouraria_conta.id = ft.id_tesouraria_conta
where 
	u_nratend = @nratend and ft.nmdoc!='Inserção de Receita'

GO
Grant Execute On dbo.up_touch_ft_atendimento to Public
Grant Control On dbo.up_touch_ft_atendimento to Public
GO