-- Historico
-- exec up_touch_hist_precos '2849487'

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_hist_precos]') IS NOT NULL
	drop procedure dbo.up_touch_hist_precos
go

create procedure dbo.up_touch_hist_precos
	@ref varchar (18)

/* WITH ENCRYPTION */
AS

	select 
		data
		,preco
	from 
		hist_precos (nolock)
	where 
		ref = @ref
		and id_preco=1
		and ativo=1


GO
Grant Execute On dbo.up_touch_hist_precos to Public
Grant Control On dbo.up_touch_hist_precos to Public
GO

