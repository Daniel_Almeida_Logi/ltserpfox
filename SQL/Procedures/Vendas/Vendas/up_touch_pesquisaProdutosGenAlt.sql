-- Pesquisa de Produtos por Gen�ricos alternativos nas Vendas
-- exec up_touch_pesquisaProdutosGenAlt '8168617', 1

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_pesquisaProdutosGenAlt]') IS NOT NULL
	drop procedure dbo.up_touch_pesquisaProdutosGenAlt
go

create procedure dbo.up_touch_pesquisaProdutosGenAlt
@valor varchar(60),
@armazem numeric(18,0),
@site_nr tinyint

/* WITH ENCRYPTION */
AS

	SET NOCOUNT ON

	;with
		cte1(grphmgcode) as (
			select
				grphmgcode
			from
				fprod (nolock)
				inner join st (nolock) on st.ref=fprod.cnp
			where
				st.ref = @valor
		)

	Select
		 'sel'			=	convert(bit,0)
		,'ref'			=	st.ref
		,'design'		=	ltrim(rtrim(st.design))
		,'stock'		=	st.stock
		,'epv1'		=	st.epv1
		,'validade'	=	convert(varchar,st.validade,102)
		,'faminome'	=	st.faminome
		,'comp'		=	isnull((case 
										when (left(cptgrp.descri��o,3)!='N�o' and cptgrp.u_diploma1='' and cptgrp.u_diploma2='' and cptgrp.u_diploma3='') AND cptgrp.grupo!='ST'
										then 1
										else 0
									end)
								,0)
		,'generico'	=	IsNull(fprod.generico,0)
		,'GRPHMG'	=	case
								when fprod.grphmgcode='GH0000' then fprod.grphmgcode
								else fprod.grphmgcode + ' - ' + fprod.grphmgdescr
							end
		,'psico'		=	isnull(fprod.psico,0)
		,'benzo'		=	isnull(fprod.benzo,0)
		,'ststamp'	=	st.ststamp
		,'ptoenc'		=	st.ptoenc
		,'stmax'		=	st.stmax
		,'marg1'		=	st.marg1
		,'local'		=	st.local
		,'u_local'		=	st.u_local
		,'u_local2'	=	st.u_local2
	from 
		st (nolock)
		left join fprod  (nolock) on st.ref=fprod.cnp
		left join cptgrp (nolock) on cptgrp.grupo=fprod.grupo
		inner join cte1 on fprod.grphmgcode=cte1.grphmgcode
	where
		st.site_nr = @site_nr
		and st.inactivo=0
		and generico=1
		AND fprod.grphmgcode!='GH0000'
		AND fprod.grphmgcode!=''
		AND st.ref!=@valor
	order by
		st.stock desc, st.design


GO
Grant Execute On dbo.up_touch_pesquisaProdutosGenAlt to Public
Grant Control On dbo.up_touch_pesquisaProdutosGenAlt to Public
Go