/*
	Regularizar Creditos
	
	exec up_touch_regularizarCreditos '', -1, '', 30626, 8, '19000101','20210301', '', 1, 'Administrador', 'Loja 3',0,0,0
	exec up_touch_regularizarCreditos '', -1, '', -1, -1, '19000101','20200528', '', 1, 'Administrador', 'Loja 1',0,0,1
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_regularizarCreditos]') IS NOT NULL
	drop procedure dbo.up_touch_regularizarCreditos
go

create procedure dbo.up_touch_regularizarCreditos
	@produto	varchar(60)
	,@venda		numeric(10,0)
	,@cliente	char(55)
	,@no		numeric(10,0)
	,@estab		numeric(3,0)
	,@dataIni	datetime
	,@dataFim	datetime
	,@nrAtend	varchar(20)
	,@user		numeric(6)
	,@group		varchar(50)
	,@site		varchar(20)
	,@sosusp	as bit = 0
	,@sonsusp	as bit = 0
	,@MesmoNif	as bit = 0

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

declare
	 @ProdRef varchar(18) = left(@produto,18)

SET FMTONLY OFF /* temporario ate alterar software -> deixar de usar o set ftmonly on */

	declare @nifempresa varchar(20)
	set @nifempresa = (select ncont from empresa where site= @site)

	if @MesmoNif=0
		begin

		select
			ndoc,nmdoc,u_tipodoc,lancacc
			into
				#temp_td
			from
				td (nolock)
			where
				u_tipodoc != 1
				AND lancacc = 1
				AND dbo.up_PerfilFacturacao(@user,@group,(RTRIM(LTRIM(td.nmdoc)) + ' - Visualizar'),@site)=0
				AND site = @site

			Select distinct
				ft.ftstamp
				,convert(varchar,fdata,102) as fdata
				,ft.ousrhora
				,nmdoc = case when ft.cobrado=1 then ft.nmdoc + '(S)' else ft.nmdoc end
				,ft.fno
				,ft.nome
				,ft.no
				,ft.estab
				,cliente = CONVERT(VARCHAR(100 ),'[' + CONVERT(varchar,ft.no) +'][' + CONVERT(varchar,ft.estab) + '] ' + ft.nome)
				,ft.ndoc
				,ft.u_lote
				,ft.u_nslote
				,ft.u_lote2
				,ft.u_nslote2
				,ft.u_ltstamp
				,ft.u_ltstamp2
				,ft.tipodoc
				,utenteNome = CASE WHEN ft.u_hclstamp !='' THEN (SELECT CONVERT(VARCHAR(100 ),'[' + CONVERT(varchar,b_utentes.no) +'][' + CONVERT(varchar,b_utentes.estab) + '] ' + b_utentes.nome) FROM b_utentes WHERE utstamp=ft.u_hclstamp) 
									ELSE CONVERT(VARCHAR(100 ),'[' + CONVERT(varchar,ft.no) +'][' + CONVERT(varchar,ft.estab) + '] ' + ft.nome) END
			into
				#temp_ft
			from
				ft (nolock)
				inner join fi (nolock) on ft.ftstamp = fi.ftstamp
				inner join #temp_td on #temp_td.ndoc = ft.ndoc
				--left join re on re.restamp = (select top 1 restamp from rl where rl.ccstamp = ft.ftstamp)
				left join re (nolock) on re.restamp in (select restamp from rl where rl.ccstamp = ft.ftstamp)
				left join cc (nolock) on cc.ccstamp=ft.ftstamp
			where
				ft.anulado = 0
				AND isnull(re.u_backoff,0) = 0
				AND fi.epromo = 0
				AND (ft.tipodoc = 1 or #temp_td.u_tipodoc in (4,7) or (ft.tipodoc = 4 and ft.nmdoc like '%import%') ) /* Fatura Acordo, Nota Crédito, facturas/vendas importadas */
				AND (ft.fdata between @dataIni and @dataFim)
				AND (ft.no = case when @no = -1 then ft.no else @no end)
				AND (ft.estab = case when @estab = -1 then ft.estab else @estab end)
				AND (ft.u_nratend = case when @nrAtend = '' then ft.u_nratend else @nrAtend end)
				AND (ft.nome  = case when rtrim(@cliente) = '' then ft.nome else rtrim(@cliente) end)
				AND (ft.fno = case when @venda = -1 then ft.fno else @venda end)
				AND (ft.site = case when @site = '' then ft.site else @site end)
				AND (fi.ref =Ltrim(Rtrim(@prodref))  OR fi.design like @produto + '%')
				AND ft.cobrado between (case when @sosusp=1 then 1 when @sonsusp=1 then 0 else 0 end) and (case when @sosusp=1 then 1 when @sonsusp=1 then 0 else 1 end) 
				and (cc.edeb>cc.edebf or cc.ecred>cc.ecredf)
			--	select * from #temp_ft
				--
				select
					 fi.ofistamp
					,qtt = isnull((select Sum(qtt) as qtt from fi fix where fi.fistamp = fix.ofistamp and fix.ndoc!=75),0)
				into
					#temp_fi_ofistamp
				from
					fi
					inner join #temp_ft on #temp_ft.ftstamp = fi.ftstamp

				Select distinct
					 ftstamp2 = fi.ftstamp
					,fi.fistamp
					,fi.ref
					,fi.design
					,fi.desconto
					,fi.desc2
					,fi.u_descval
					,fi.u_comp
					,fi.etiliquido
					,fi.etiliquido as etiliquido2
					,fi.etiliquido as totalCdesc
					,fi.epv
					,fi.fmarcada
					,fi.u_epvp
					,fi.amostra
					,eaquisicao	= case 
									when fi.eaquisicao = 0
										then 0
									when isnull(#temp_fi_ofistamp.qtt,0) > fi.eaquisicao
										then 0
										else fi.eaquisicao - isnull(#temp_fi_ofistamp.qtt,0)
								  end
					,qtt  = fi.qtt - isnull(#temp_fi_ofistamp.qtt,0)
					,qtt2 = fi.qtt
					,fi.lote
					,ISNULL(fi2.dataValidade,'')  as dataValidade
				into
					#temp_fi
				from
					fi
					left join fi2 (nolock) on fi2.fistamp = fi.fistamp
					inner join #temp_ft on #temp_ft.ftstamp = fi.ftstamp
					left join #temp_fi_ofistamp on fi.fistamp = #temp_fi_ofistamp.ofistamp
				where
					(fi.qtt - isnull(#temp_fi_ofistamp.qtt,0)) > 0

				select
					*
				from (
						select
							 ft.*
							,fi.*
							,0 as dev		
							,CONVERT(bit,0) as docinvert
							,convert(bit,0) as sel
							,CONVERT(bit,0) as copied
							,CONVERT(bit,0) as fact
							,CONVERT(bit,0) as lancacc
							,CONVERT(bit,0) as factPago
						from
							#temp_fi as fi
							inner join #temp_ft as ft on ft.ftstamp = fi.ftstamp2
					 ) as x
				where
					x.qtt > x.eaquisicao
				order by
					fdata desc, ousrhora desc
		end
	else
		begin
			select
			ndoc,nmdoc,u_tipodoc,lancacc
			into
				#temp_td1
			from
				td (nolock)
			where
				u_tipodoc != 1
				AND lancacc = 1
				AND dbo.up_PerfilFacturacao(@user,@group,(RTRIM(LTRIM(td.nmdoc)) + ' - Visualizar'),@site)=0
				AND site in (select site from empresa where ncont=@nifempresa)

			Select distinct
				ft.ftstamp
				,convert(varchar,fdata,102) as fdata
				,ft.ousrhora
				,nmdoc = case when ft.cobrado=1 then ft.nmdoc + '(S)' else ft.nmdoc end
				,ft.fno
				,ft.nome
				,ft.no
				,ft.estab
				,cliente = CONVERT(VARCHAR(100 ),'[' + CONVERT(varchar,ft.no) +'][' + CONVERT(varchar,ft.estab) + '] ' + ft.nome)
				,ft.ndoc
				,ft.u_lote
				,ft.u_nslote
				,ft.u_lote2
				,ft.u_nslote2
				,ft.u_ltstamp
				,ft.u_ltstamp2
				,ft.tipodoc
				,utenteNome = CASE WHEN ft.u_hclstamp !='' THEN (SELECT CONVERT(VARCHAR(100 ),'[' + CONVERT(varchar,b_utentes.no) +'][' + CONVERT(varchar,b_utentes.estab) + '] ' + b_utentes.nome) FROM b_utentes WHERE utstamp=ft.u_hclstamp) 
									ELSE CONVERT(VARCHAR(100 ),'[' + CONVERT(varchar,ft.no) +'][' + CONVERT(varchar,ft.estab) + '] ' + ft.nome) END
			into
				#temp_ft1
			from
				ft (nolock)
				inner join fi (nolock) on ft.ftstamp = fi.ftstamp
				inner join #temp_td1 on #temp_td1.ndoc = ft.ndoc
				--left join re (nolock) on re.restamp = (select top 1 restamp from rl where rl.ccstamp = ft.ftstamp)
				left join re (nolock) on re.restamp in (select restamp from rl where rl.ccstamp = ft.ftstamp)
				inner join [dbo].[empresa] (nolock) on ft.site = empresa.site
				left join cc (nolock) on cc.ccstamp=ft.ftstamp
			where
				ft.anulado = 0
				AND isnull(re.u_backoff,0) = 0
				AND fi.epromo = 0
				AND (ft.tipodoc = 1 or #temp_td1.u_tipodoc in (4,7) or (ft.tipodoc = 4 and ft.nmdoc like '%import%') )
				AND (ft.fdata between @dataIni and @dataFim)
				AND (ft.no = case when @no = -1 then ft.no else @no end)
				AND (ft.estab = case when @estab = -1 then ft.estab else @estab end)
				AND (ft.u_nratend = case when @nrAtend = '' then ft.u_nratend else @nrAtend end)
				AND (ft.nome  = case when rtrim(@cliente) = '' then ft.nome else rtrim(@cliente) end)
				AND (ft.fno = case when @venda = -1 then ft.fno else @venda end)
				--AND (ft.site = case when @site = '' then ft.site else @site end)
				AND [ft].[site] in (select site from empresa where ncont=@nifempresa)
				AND (fi.ref =Ltrim(Rtrim(@prodref))  OR fi.design like @produto + '%')
				AND ft.cobrado between (case when @sosusp=1 then 1 when @sonsusp=1 then 0 else 0 end) and (case when @sosusp=1 then 1 when @sonsusp=1 then 0 else 1 end) 
				and (cc.edeb>cc.edebf or cc.ecred>cc.ecredf)

				--exec up_touch_regularizarCreditos '', -1, '', -1, -1, '19000101','20200528', '', 1, 'Administrador', 'Loja 1',0,0,0
				--exec up_touch_regularizarCreditos '', -1, '', -1, -1, '19000101','20200528', '', 1, 'Administrador', 'Loja 1',0,0,1
				--
				select
					 fi.ofistamp
					,qtt = isnull((select Sum(qtt) as qtt from fi fix where fi.fistamp = fix.ofistamp and fix.ndoc!=75),0)
				into
					#temp_fi_ofistamp1
				from
					fi
					inner join #temp_ft1 on #temp_ft1.ftstamp = fi.ftstamp

				Select distinct
					 ftstamp2 = fi.ftstamp
					,fi.fistamp
					,fi.ref
					,fi.design
					,fi.desconto
					,fi.desc2
					,fi.u_descval
					,fi.u_comp
					,fi.etiliquido
					,fi.etiliquido as etiliquido2
					,fi.etiliquido as totalCdesc
					,fi.epv
					,fi.fmarcada
					,fi.u_epvp
					,fi.amostra
					,eaquisicao	= case 
									when fi.eaquisicao = 0
										then 0
									when isnull(#temp_fi_ofistamp1.qtt,0) > fi.eaquisicao
										then 0
										else fi.eaquisicao - isnull(#temp_fi_ofistamp1.qtt,0)
								  end
					,qtt  = fi.qtt - isnull(#temp_fi_ofistamp1.qtt,0)
					,qtt2 = fi.qtt
					,fi.lote
					,ISNULL(fi2.dataValidade,'') as dataValidade
				into
					#temp_fi1
				from
					fi
					left join fi2 (nolock) on fi2.fistamp = fi.fistamp
					inner join #temp_ft1 on #temp_ft1.ftstamp = fi.ftstamp
					left join #temp_fi_ofistamp1 on fi.fistamp = #temp_fi_ofistamp1.ofistamp
				where
					(fi.qtt - isnull(#temp_fi_ofistamp1.qtt,0)) > 0

				select
					*
				from (
						select
							 ft.*
							,fi.*
							,0 as dev		
							,CONVERT(bit,0) as docinvert
							,convert(bit,0) as sel
							,CONVERT(bit,0) as copied
							,CONVERT(bit,0) as fact
							,CONVERT(bit,0) as lancacc
							,CONVERT(bit,0) as factPago
						from
							#temp_fi1 as fi
							inner join #temp_ft1 as ft on ft.ftstamp = fi.ftstamp2
					 ) as x
				where
					x.qtt > x.eaquisicao
				order by
					fdata desc, ousrhora desc
		end 

	

IF object_id('tempdb.dbo.#temp_td') IS NOT NULL
       DROP TABLE #temp_td;
IF object_id('tempdb.dbo.#temp_ft') IS NOT NULL
       DROP TABLE #temp_ft;
IF object_id('tempdb.dbo.#temp_fi_ofistamp') IS NOT NULL
       DROP TABLE #temp_fi_ofistamp;
IF object_id('tempdb.dbo.#temp_fi') IS NOT NULL
       DROP TABLE #temp_fi;
        

GO
Grant Execute On dbo.up_touch_regularizarCreditos to Public
grant control on dbo.up_touch_regularizarCreditos to Public
GO