/*

	exec up_print_getTotAtend '220611601309ZT'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_print_getTotAtend]') IS NOT NULL
	drop procedure dbo.up_print_getTotAtend
go

create procedure dbo.up_print_getTotAtend
	@nratend VARCHAR(20)

AS
BEGIN

	SELECT 
		@nratend as u_nratend,
		ISNULL(SUM(x.tot),0) as tot
	FROM
	(
		select 
			eTotal as tot 
		from 
			ft(nolock) 
		where 
			u_nratend = @nratend 
			and nmdoc in ('Venda a Dinheiro')

		UNION ALL

		SELECT 
			eTotal as tot
		FROM
			re(nolock)
		WHERE 
			u_nratend = @nratend
			--and nmdoc = 'Normal'
	) as X

END

GO
Grant Execute On dbo.up_print_getTotAtend to Public
Grant Control On dbo.up_print_getTotAtend to Public
GO