-- Ver dados de farm�cia para venda (STOUCHPOS)
-- exec up_touch_dadosFarmaciaProd '8168617', 1
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_dadosFarmaciaProd]') IS NOT NULL
	drop procedure dbo.up_touch_dadosFarmaciaProd
go

create procedure dbo.up_touch_dadosFarmaciaProd
	@ref varchar (18)
	,@site_nr tinyint

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	SELECT
		isnull(fprod.nome,'') as nome, st.design,
		isnull(fprod.dosuni,'') as dosuni,
		isnull(fprod.generico,0) as generico,
		isnull(st.ref,'') as ref,
		isnull(fprod.benzo,0) as benzo,
		isnull(fprod.psico,0) as psico,
		isnull(fprod.grupo,'') as grupo,
		isnull( (CASE WHEN (SELECT count(*) 
							FROM fprod f2 (nolock)
								
							WHERE 
								f2.generico=1 AND f2.grphmgcode=fprod.grphmgcode 
								AND f2.grphmgcode not in ('','GH0000')
								AND st.ref = fprod.CNP) > 0
					THEN Cast(1 as bit) ELSE Cast(0 as Bit) END),0) as genalt,
		st.stock
	from 
		st (nolock)
		left join fprod (nolock) on st.ref = fprod.cnp
	WHERE 
		st.ref=@ref
		and st.site_nr = @site_nr

GO
Grant Execute On dbo.up_touch_dadosFarmaciaProd to Public
Grant Control On dbo.up_touch_dadosFarmaciaProd to Public
Go