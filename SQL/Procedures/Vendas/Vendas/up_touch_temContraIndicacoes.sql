-- ver se existem Contra-Indicações do Produto com determinadas Patologias (STOUCHPOS)
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_temContraIndicacoes]') IS NOT NULL
	drop procedure dbo.up_touch_temContraIndicacoes
go

create procedure dbo.up_touch_temContraIndicacoes
	@ref		varchar (18),
	@clStamp	varchar(25)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

;WITH ctePatologias(ctiefpstamp,descricao,patostamp) AS
	(
		select 
			B_patologias.ctiefpstamp
			,descricao,patostamp
		from
			b_patologias (nolock)
			inner join b_ctidef (nolock) on B_patologias.ctiefpstamp = B_ctidef.ctiefpstamp
		where 
			ostamp = @clStamp
	)		
	
select 
	 num		= count(moleculasfpx.cnp)
	,fistamp	= CONVERT(varchar,'',25)
	,ref		= CONVERT(varchar,'',18)
	,clstamp	= CONVERT(varchar,'',25)
from
	B_ctifp ctifpx (nolock)
	inner join B_moleculasfp moleculasfpx(nolock) on ctifpx.moleculaID = moleculasfpx.moleculaID
where 
	cnp = @ref
	AND ctidefstamp in (SELECT ctiefpstamp FROM ctePatologias) 
	
option (recompile)

GO
Grant Execute On dbo.up_touch_temContraIndicacoes to Public
Grant Control On dbo.up_touch_temContraIndicacoes to Public
GO