-- Ver Dados do Produto Na Venda (FUN��ES DA VENDA)
-- exec up_touch_dadosProduto '8168617',1
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_dadosProduto]') IS NOT NULL
	drop procedure dbo.up_touch_dadosProduto
go

create procedure dbo.up_touch_dadosProduto
@ref varchar (120)
,@site_nr tinyint

/* WITH ENCRYPTION */
AS

/* Acrescentado a 26-01-2012 coloca��o de informa��o sobre as margens regressivas Marcelino Teixeira */
;With cte1 as (
	Select	
		versao,escalao,PVPmin,PVPmax,mrgFarm,FeeFarm
	from	
		b_mrgRegressivas (nolock)
)


SELECT
	local				= local
	,u_local			= isnull(u_local,'')
	,familia			= isnull(familia,'')
	,faminome			= isnull(faminome,'')
	,titaimdescr		= isnull(titaimdescr,'')
	,grphmgcode			= isnull(grphmgcode,'')
	,grphmgdescr		= isnull(grphmgdescr,'')
	,u_nomerc			= isnull(fprod.estaimdescr,'') + ', ' + isnull(sitcomdescr,'')
	,pvpdic				= isnull(fpreco.epreco,0)
	,ranking			= isnull(fprod.ranking,0)
	,escalao			= isnull((Select top 1 escalao from cte1 Where fprod.pvporig between PVPmin and PVPmax order by versao desc),0)
	,mrgFarm			= isnull((Select top 1 mrgFarm from cte1 Where fprod.pvporig between PVPmin and PVPmax order by versao desc),0)
	,feeFarm			= isnull((Select top 1 feeFarm from cte1 Where fprod.pvporig between PVPmin and PVPmax order by versao desc),0)
	,qttfor
	,fornPrefere		= fornecedor + ' [' + CONVERT(varchar, fornec) + ']'
FROM
	st (nolock)
	left join fprod (nolock) on st.ref=fprod.cnp
	left join fpreco (nolock) on st.ref=fpreco.cnp and fpreco.grupo='pvp'
WHERE
	st.ref=@ref
	and st.site_nr = @site_nr

GO
Grant Execute On dbo.up_touch_dadosProduto to Public
Grant Control On dbo.up_touch_dadosProduto to Public
GO