--exec up_touch_DCIp '000240'

-- Mostrar DCI Principal do Produto (STOUCHPOS)
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_DCIp]') IS NOT NULL
	drop procedure dbo.up_touch_DCIp
go

create procedure dbo.up_touch_DCIp
	@ref varchar (18)

/* WITH ENCRYPTION */
AS

	SELECT
		 ref = fprod.ref
		,descricao = dci
	FROM 
		fprod (nolock)
	where 
		cnp = @ref 
		or cnp = (select top 1 codCNP from st where ref=@ref)
	

GO
Grant Execute On dbo.up_touch_DCIp to Public
Grant Control On dbo.up_touch_DCIp to Public
Go