-- Mostrar DCIs em lista do Produto (STOUCHPOS)
-- exec up_touch_DCIs_lista '5324256'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_DCIs_Lista]') IS NOT NULL
	drop procedure dbo.up_touch_DCIs_Lista
go

create procedure dbo.up_touch_DCIs_Lista
	@ref varchar (120)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

DECLARE @str varchar(100)
select @str= (select dci from fprod (nolock) where fprod.cnp=@ref)

;with 
	cte1 as
	(
		select 0 a, 1 b
		union all
		select b, charindex('+', @str, b) + len('+')
		from cte1
		where b > a
	)

select
	descricao	=	ltrim(rtrim(substring(@str,a, case when b > len('+') then b-a-len('+') else len(@str) - a + 1 end)))
from cte1 where a >0


GO
Grant Execute On dbo.up_touch_DCIs_Lista to Public
Grant Control On dbo.up_touch_DCIs_Lista to Public
Go