/* Sp painel reg Vendas
	exec up_touch_regularizarVendas '', -1, '', 215, 0, '20190820', '20190919', '', 1, 'Administrador', 'Loja 1', 100
	exec up_touch_regularizarVendas '', -1, '', 1078963, 0, '20190623', '20190723', '', 2, 'Administrador', 'Loja 1', 100
	exec up_touch_regularizarVendas '000240', -1, '', -1, -1, '20190904', '20191004', '', 1, 'Administrador', 'Loja 1', 1000
	exec up_touch_regularizarVendas '', '-1' , '', -1, -1, '20220501', '20220531', '' , 1, 'Administrador', 'Loja 1', 1000, 0
	exec up_touch_regularizarVendas '', -1, '', -1, -1, '20200705', '20200804', '', 1, 'Administrador', 'Loja 1', 1000, 0
	exec up_touch_regularizarVendas '', -1, '', 203, 0, '20191104', '20191204', '', 4, 'Operadores', 'ATLANTICO', 100, 0

	exec up_touch_regularizarVendas '', '-1', '', -1, -1, '20220713', '20220812', '' , 1, 'Administrador', 'Loja 1', 1000, 0

	exec up_touch_regularizarVendas '', '-1', '', -1, -1, '20220713', '20220812', '' , 1, 'Administrador', 'Loja 1', 1000, 0

	exec up_touch_regularizarVendas '', '-1' , '', -1, -1, '20220723', '20220822', '' , 1, 'Administrador', 'Loja 1', 1000, 0

						exec up_touch_regularizarVendas '', '-1' , '', 234, 0, '20241121', '20241122', '' , 1, 'Administradores', 'Loja 1', 1000, 0
											exec up_touch_regularizarVendas '', '-1' , '', 107902511, -1, '20241121', '20241122', '' , 98, 'Administradores', 'Loja 1', 1000, 0
					exec up_touch_regularizarVendas '', '-1' , '', -1, -1, '20241121', '20241122', '' , 1, 'Administradores', 'Loja 1', 1000, 0

					
	exec up_touch_regularizarVendas '', '-1' , '', -1, -1, '19', '20241216', '' , 1, 'Administrador', 'Loja 1', 1000, 0	


*/	


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_regularizarVendas]') IS NOT NULL
	DROP PROCEDURE [dbo].[up_touch_regularizarVendas] ;
GO


CREATE PROCEDURE [dbo].[up_touch_regularizarVendas]
	@produto	varchar(60)
	,@venda		numeric(10,0)
	,@cliente	varchar(55)
	,@no		numeric(10,0)
	,@estab		numeric(3,0)
	,@dataIni	datetime
	,@dataFim	datetime
	,@nrAtend	varchar(20)
	,@user		numeric(6)
	,@group		varchar(50)
	,@site		varchar(20)
	,@Top		int
	,@MesmoNif	bit = 0

 /*WITH ENCRYPTION */
AS
SET NOCOUNT ON
/**************************
Cria variaveis internas
***************************/
DECLARE 
	 @ProdRef  varchar(18) = LEFT(@produto,18)
	,@nrCartao varchar(30) = LEFT(@cliente,30)
	,@consulta bit
	,@nifemp varchar(15)
	set @consulta = (select bool from b_parameters where stamp = 'ADM0000000270')
	set @nifemp = (select ncont from empresa where site=@site)


	If OBJECT_ID('tempdb.dbo.#dadosSt') IS NOT NULL
			drop table #dadosSt;

	If OBJECT_ID('tempdb.dbo.#temp_td') IS NOT NULL
		drop table #temp_td ;

	If OBJECT_ID('tempdb.dbo.#temp_refIgnora') IS NOT NULL
		drop table #temp_refIgnora ;

	/**********************************************
	Cria tabela temporaria com tipos de documentos
***********************************************/
	SELECT *
	INTO #temp_td
	FROM
		(SELECT
			ndoc,nmdoc,u_tipodoc,lancacc
		FROM
			[dbo].[td] (nolock)
		WHERE
			u_tipodoc NOT IN (1,3,5,13)
			AND (SELECT
					count(B_pf.pfstamp)
				 FROM
					[dbo].[B_pf] (nolock)
					LEFT JOIN [dbo].[b_pfu] (nolock) ON [B_pfu].[pfstamp] = [B_pf].[pfstamp]
					LEFT JOIN [dbo].[b_pfg] (nolock) ON [B_pfg].[pfstamp] = [B_pf].[pfstamp]
				 WHERE 
					B_pf.resumo = ([td].[nmdoc] + ' - Visualizar')
					AND (ISNULL([B_pfu].[userno],9999) = @user or ISNULL([B_pfg].[nome],'') = @group)
				) = 0
			and td.nmdoc not like '%Segurador%'
		) as X   

	-- Ignorar MCDTS e vacinas
	select 
		distinct ref as ref
	into
		#temp_refIgnora
	from
		fprod(nolock)
    where grupo in ('RX','VA','RS')


	create table #dadosSt (ref varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AI)
	/* with ref*/
	declare @sqlSt varchar(max)
	IF(@produto!='')
	begin
		set @sqlSt = 'Insert into  #dadosSt  select distinct ref from   st(nolock) where ref IN (''' + @produto + ''')'	
		exec (@sqlSt)
	end
	declare @armazem varchar(20)
	set @armazem = (select no from empresa(nolock) where site= @site)
	-- só mostra códigos se parametro estiver ativo ou for o admin a fazer a consulta
	IF @consulta = 1 or @user = 1
	begin
		-- só procura documentos na loja onde está
		if @MesmoNif=0
		begin 
			with cte1 as (
			SELECT TOP (@Top)
				ft.ftstamp
				,fi.fistamp
				,convert(varchar,fdata,102) as fdata
				,ft.ousrhora
				,CASE WHEN ft.cobrado=1 then ft.nmdoc + ' (S)' else ft.nmdoc END AS nmdoc
				,ft.fno
				,cast(ft.nome as varchar(80) ) nome
				,ft.no
				,ft.estab
				,ft.ncont
				,fi.ref
				,fi.design
				,fi.desconto
				,fi.desc2
				,fi.u_descval
				,fi.u_comp
				,qtt = (fi.qtt-[Regularizar].[qtt])
				--,qtt = fi.qtt
				,fi.qtt as qtt2
				,fi.etiliquido
				,fi.etiliquido as etiliquido2
				,totlin = fi.etiliquido
				,fi.etiliquido as totalCdesc
				,fi.epv
				,ft.ndoc
				,ft.u_lote
				,ft.u_nslote
				,ft.u_lote2
				,ft.u_nslote2
				,convert(bit,0) as sel
				,CONVERT(bit,0) as copied
				,ft.u_ltstamp
				,ft.u_ltstamp2
				,ft2.u_codigo
				,ft2.u_codigo2
				,fi.u_diploma
				,0 as dev
				,CONVERT(bit,0) as fact
				,CONVERT(bit,0) as factPago
				,fi.fmarcada
				,eaquisicao	= CASE 
								WHEN #temp_td.u_tipodoc in (8) THEN (fi.qtt-[Regularizar].[qtt])
								WHEN fi.eaquisicao=0 THEN 0
								WHEN [Regularizar].[qtt]>fi.eaquisicao THEN 0
								ELSE fi.eaquisicao - [Regularizar].[qtt]
							  END
				,oeaquisicao = CASE 
								WHEN #temp_td.u_tipodoc in (8) THEN (fi.qtt-[Regularizar].[qtt])
								WHEN fi.eaquisicao=0 THEN 0
								WHEN [Regularizar].[qtt]>fi.eaquisicao THEN 0
								ELSE fi.eaquisicao - [Regularizar].[qtt]
							  END
				,fi.u_psicont
				,fi.u_bencont
				,ft2.u_receita
				,ft2.u_nbenef
				,ft2.u_nbenef2
				,ft2.u_nopresc
				,ft2.u_entpresc
				,u_recdata		= 
					case when ISNULL(dadosPsico.u_recdata,'19000101') !='19000101' or isnull(ft2.token,'')='' then ISNULL(dadosPsico.u_recdata,'19000101') 
					else
						isnull((select top 1 isnull(receita_data,'19000101') from Dispensa_Eletronica(nolock) where token = ft2.token),'19000101')
					end
				,u_medico		= ISNULL(dadosPsico.u_medico,'')
				,u_nmutdisp		= ISNULL(dadosPsico.u_nmutdisp,'')
				,u_moutdisp		= ISNULL(dadosPsico.u_moutdisp,'')
				,u_cputdisp		= ISNULL(dadosPsico.u_cputdisp,'')
				,u_nmutavi		= ISNULL(dadosPsico.u_nmutavi,'')
				,u_moutavi		= ISNULL(dadosPsico.u_moutavi,'')
				,u_cputavi		= ISNULL(dadosPsico.u_cputavi,'')
				,u_ndutavi		= ISNULL(dadosPsico.u_ndutavi,'')
				,u_ddutavi		= ISNULL(dadosPsico.u_ddutavi,'19000101')
				,u_idutavi		= ISNULL(dadosPsico.u_idutavi,0)
				,codDocID		= ISNULL(dadosPsico.codigoDocSPMS, '')
				,fi.u_txcomp
				,fi.u_epvp
				,fi.u_ettent1
				,fi.u_ettent2
				,amostra = CONVERT(bit,fi.amostra)
				,docinvert		= CONVERT(bit,0)
				,u_backoff		= ISNULL(re.u_backoff,0)
				,#temp_td.lancacc
				,ft2.u_design
				,ft2.u_abrev
				,ft2.u_abrev2
				,fi.u_epref
				,ft.tipodoc
				,fi.tabiva
				,fi.iva
				,fi.opcao
				,ft2.token
				,ft2.codAcesso
				,ft2.codDirOpcao
				,fi.id_Dispensa_Eletronica_D as id_validacaoDem
				,ft2.token_efectivacao_compl
				,ft2.c2codpost
				,ficompart	= isnull((select top 1 token from fi_compart (nolock) where fi_compart.fistamp=fi.fistamp),'')
				,fi.valcartao
				,'PVPRES'			= 0
				,'PVPACT'			= 0
				,'fecha'			= CONVERT(bit,0)
				,'valorAdiantado'	= case when (select (edeb-ecred) from cc(nolock) where cc.ftstamp=ft.ftstamp)<>0 then 
										(
										cast(case when ft.nmdoc like '%Factura%' 
										then fi.etiliquido*(select round((edebf-ecredf)/(edeb-ecred),2) from cc(nolock) where cc.ftstamp=ft.ftstamp)
										else fi.etiliquido
										end as numeric(15,2)) 
										)
									else
										cast(case when ft.nmdoc='Venda a Dinheiro' or ft.nmdoc='Fatura Recibo'  or carga like '%TSR%'
										then fi.etiliquido
										else 0.00
										end as numeric(15,2)) 
									end 
		--	exec up_touch_regularizarVendas '', -1, '', 215, 0, '20190623', '20190723', '', 1, 'Administrador', 'Loja 1', 100
				,'row'				= 0
				,'fechada'			= CONVERT(bit,0)
				,'qttComp'			= 0
				,'stampAdiantamento' = ''
				,'stock'			= 0
				,'stockArm'			= 0
				,'stockTotal'		= ''
				,'codComp'			= ''
				,'diploma'			= ''
				,descval			= 0
				,'tlmvl '			= b_utentes.tlmvl
				,'qttRec'			=  0
				,'sms'				= convert(bit,0)
				,'clstamp'			= b_utentes.utstamp 
				,'imp'				= convert(bit,0)
				,exepcaoTrocaMed	= ''
				,'qttres'			= 0
				, nrreceita			= ''
				, fi.lote
				, fi2.dataValidade
			FROM
				[dbo].[ft] (nolock)
				INNER JOIN #temp_td ON #temp_td.ndoc=ft.ndoc
				INNER JOIN [dbo].[fi] (nolock) ON ft.ftstamp = fi.ftstamp and fi.rdata BETWEEN @dataIni AND @dataFim --AND fi.ref != 'R000001'
				Left JOIN [dbo].[fi2] (nolock) ON fi.fistamp=fi2.fistamp
				INNER JOIN [dbo].[ft2] (nolock) ON ft.ftstamp=ft2.ft2stamp
				INNER JOIN [dbo].[b_utentes] (nolock) ON b_utentes.no = ft.no AND b_utentes.estab = ft.estab and b_utentes.inactivo=0
				LEFT JOIN  [dbo].[B_dadosPsico] dadosPsico (nolock) ON dadosPsico.ftstamp=ft.ftstamp
				LEFT JOIN  [dbo].[re] (nolock) ON re.restamp=(select top 1 restamp from rl (nolock) where rl.ccstamp=ft.ftstamp)
				/*CROSS APPLY (SELECT ISNULL(SUM(ofi.qtt),0) AS [qtt] FROM [dbo].[fi] as [ofi] where [fi].[fistamp] = [ofi].[ofistamp] AND [ofi].[ndoc] != 75 ) AS [Regularizar]*/
				CROSS APPLY (SELECT ISNULL(SUM(ofi.qtt),0) AS [qtt] FROM [dbo].[fi] as [ofi] (nolock) inner join td (nolock) as otd on ofi.ndoc = otd.ndoc where [fi].[fistamp] = [ofi].[ofistamp] AND [otd].[u_tipodoc] != 3 AND [otd].[codigoDoc] != 32 AND [otd].[nmdoc] not like ('%Segur%') AND [otd].[nmdoc] not like ('%Import%') AND [otd].[nmdoc] not like ('%Manual%') ) AS [Regularizar]
			WHERE
				--[ft].[anulado] = 0
				[ft].[tipodoc] in (1,4)
				AND fi.ref != 'R000001'
				AND fi.epromo = 0
				AND ([fi].[qtt]-[Regularizar].[qtt])>0
				AND [fi].[qtt]>0
				AND ([ft].[fdata] BETWEEN @dataIni AND @dataFim)
				AND ([fi].[ref] LIKE @ProdRef + '%' OR [fi].[design] LIKE @produto +'%')
				AND [ft].[fno] = CASE WHEN @venda = -1 THEN [ft].[fno] ELSE @venda END
				AND [ft].[u_nratend] = CASE WHEN @nrAtend = '' THEN [ft].[u_nratend] ELSE @nrAtend END
				AND ([ft].[nome] LIKE RTRIM(@cliente) +'%' OR [B_utentes].[nrcartao] = RTRIM(@nrCartao) + '%')
				AND [ft].[no] = CASE WHEN @no < 0 THEN [ft].[no] ELSE @no END
				AND [ft].[estab] = CASE WHEN @estab < 0 THEN [ft].[estab] ELSE @estab END
				AND [ft].[site] = @site
				AND fi.ref not in (select ref from #temp_refIgnora)
					GROUP BY 
				ft.ftstamp, fi.fistamp,	ft.ousrhora, ft.nome, ft.no, ft.estab, fi.ref, fi.design, fi.desconto, fi.desc2, fi.qtt, fi.tabiva, fi.iva, b_utentes.tlmvl, b_utentes.utstamp, fi.qtt, fi2.fistamp,
				fi.ousrhora, fi.lote, fi2.dataValidade, ft.fdata, ft.cobrado, ft.nmdoc, ft.ndoc, ft.fno,ft.ncont, fi.u_descval, fi.u_comp, Regularizar.qtt, fi.etiliquido, fi.epv, ft.u_lote, ft.u_nslote,
				ft.u_lote2, ft.u_nslote2, ft.u_ltstamp, ft.u_ltstamp2, ft2.u_codigo, ft2.u_codigo2, fi.u_diploma, fi.fmarcada, #temp_td.u_tipodoc, fi.eaquisicao, fi.u_psicont, fi.u_bencont, ft2.u_receita,
				ft2.u_nbenef,ft2.u_nbenef2, ft2.u_nopresc, ft2.u_entpresc, ft2.token, dadosPsico.u_recdata, dadosPsico.u_medico, dadosPsico.u_nmutdisp, dadosPsico.u_moutdisp, dadosPsico.u_cputdisp,dadosPsico.u_nmutavi,
				dadosPsico.u_moutavi,dadosPsico.u_cputavi,dadosPsico.u_ndutavi, dadosPsico.u_ddutavi, dadosPsico.u_ddutavi, dadosPsico.u_idutavi, dadosPsico.codigoDocSPMS, fi.u_txcomp,fi.u_txcomp, fi.u_epvp,
				fi.u_ettent1,fi.u_ettent2, fi.amostra, re.u_backoff, #temp_td.lancacc, ft2.u_design, ft2.u_abrev, ft2.u_abrev2, fi.u_epref, ft.tipodoc, fi.opcao,ft2.codAcesso, ft2.codDirOpcao,ft2.codDirOpcao
				,fi.id_Dispensa_Eletronica_D,ft2.token_efectivacao_compl,ft2.c2codpost,fi.valcartao, ft.carga

			--exec up_touch_regularizarVendas '', '-1' , '', 234, 0, '20241121', '20241122', '' , 1, 'Administradores', 'Loja 1', 1000, 0
			
			--ORDER BY
			--	fdata DESC, [ft].[ousrhora] DESC
			
			
			union all
			select TOP (@Top)
				bo.bostamp as ftstamp
				,bi.bistamp as fistamp
				,convert(varchar,bo.dataobra,102) as fdata
				,bo.ousrhora
				,bo.nmdos AS nmdoc
				,bo.obrano as fno
				,cast(bo.nome as varchar(80)) as nome
				,bo.no as no
				,bo.estab as estab
				,'' as ncont
				,bi.ref
				,bi.design
				,bi.desconto
				,bi.desc2
				,0 as u_descval
				,convert(bit,0) as u_comp
				,qtt = (bi.qtt-bi.qtt2)
				,qtt as qtt2
				, (select isnull((
								select sum(epv) from fi (nolock) 
								where bistamp = bi.bistamp or fi.fistamp = bi2.fistamp--(select top 1 fistamp from bi2 (nolock) where bi2stamp = bi.bistamp)
								AND [fi].[nmdoc] not like ('%Segur%') AND [fi].[nmdoc] not like ('%Import%') AND [fi].[nmdoc] not like ('%Manual%')),0))
					 as etiliquido
		--					exec up_touch_regularizarVendas '', -1, '', -1, -1, '20200223', '20200324', '', 1, 'Administrador', 'Loja 1', 1000
				,(case when bi.desconto=0 then bi.ettdeb else round(bi.ettdeb*((100-bi.desconto)/100),2) end) as etiliquido2
				,  totlin = case when (select count(fi.ftstamp) from fi (nolock) inner join ft (nolock) on fi.ftstamp=ft.ftstamp where fi.nmdoc='Inserção de Receita' and ref = bi.ref
											and ft.no = bi.no and ft.estab = bi.estab
											and CONVERT(VARCHAR(8),fi.ousrhora,108) between CONVERT(VARCHAR(8),dateadd(MINUTE,-2,bi.ousrhora),108) and CONVERT(VARCHAR(8),dateadd(MINUTE,2,bi.ousrhora),108) ) > 0 
								then
									(select top 1 etiliquido from fi (nolock) inner join ft (nolock) on fi.ftstamp=ft.ftstamp where fi.nmdoc='Inserção de Receita' and ref = bi.ref
									and ft.no = bi.no and ft.estab = bi.estab
									and CONVERT(VARCHAR(8),fi.ousrhora,108) between CONVERT(VARCHAR(8),dateadd(MINUTE,-2,bi.ousrhora),108) and CONVERT(VARCHAR(8),dateadd(MINUTE,2,bi.ousrhora),108) )
								else
									--(case when bi.desconto=0 then bi.ettdeb else round(bi.ettdeb*((100-bi.desconto)/100),2) end)
									round(bi.ettdeb,2)
								end 
				,bi.ettdeb as totalCdesc
				,bi.edebito as epv
				,bo.ndos as ndoc
				,0 as u_lote
				,0 as u_nslote
				,0 as u_lote2
				,0 as u_nslote2
				,convert(bit,0) as sel
				,CONVERT(bit,0) as copied
				,'' as u_ltstamp
				,'' as u_ltstamp2
				,'' as u_codigo
				,'' as u_codigo2
				,'' as u_diploma
				,0 as dev
				,CONVERT(bit,0) as fact
				,CONVERT(bit,0) as factPago
				,CONVERT(bit,0) as fmarcada
				,0 as eaquisicao	
				,0 as oeaquisicao
				,CONVERT(bit,0) as u_psicont
				,CONVERT(bit,0) as u_bencont
				,'' as u_receita
				,'' as u_nbenef
				,'' as u_nbenef2
				,'' as u_nopresc
				,'' as u_entpresc
				,u_recdata		= '19000101'
				,u_medico		= ''
				,u_nmutdisp		= ''
				,u_moutdisp		= ''
				,u_cputdisp		= ''
				,u_nmutavi		= ''
				,u_moutavi		= ''
				,u_cputavi		= ''
				,u_ndutavi		= ''
				,u_ddutavi		= '19000101'
				,u_idutavi		= 0
				,codDocID		= ''
				,0 as u_txcomp
				,0 as u_epvp
				,0 as u_ettent1
				,0 as u_ettent2
				,amostra = CONVERT(bit,0)
				,docinvert		= CONVERT(bit,0)
				,u_backoff		= CONVERT(bit,0)
				,CONVERT(bit,0) as lancacc
				,'' as u_design
				,'' as u_abrev
				,'' as u_abrev2
				,0 as u_epref
				,0 as tipodoc
				,bi.tabiva as tabiva
				,bi.iva as iva
				,'' as opcao
				,'' as token
				,'' as codAcesso
				,'' as codDirOpcao
				,'' as id_validacaoDem
				,'' as token_efectivacao_compl
				,'' as c2codpost
				,'' as ficompart
				,0 as valcartao
				,'PVPRES'			= isnull(bi.edebito,0)
				,'PVPACT'			= isnull(st.epv1,0)
				,'fecha'			= CONVERT(bit,0)
				,'valorAdiantado'	= isnull((select sum(etiliquido) from fi (nolock) where fistamp in (select fistamp from bi2 (nolock) where bi2stamp=bi.bistamp)),0)
				,'row'				= CONVERT(int,ROW_NUMBER() over(order by bo.bostamp))
				,'fechada'			= bi.fechada
				,'qttComp'			= bi.nopat
				,'stampAdiantamento' = isnull((select bi2.fistamp from bi2 (nolock) where bi2.bi2stamp = bi.bistamp),'')
				,'stock'			= st.stock
				,'stockArm'			= sa.stock
				,'stockTotal'		= "t:" + CONVERT(VARCHAR,CEILING((select sum(stock) from st (nolock) where ref = bi.ref ))) + " a:" + CONVERT(VARCHAR, CEILING(sa.stock))
				,'codComp'			= bi.lobs
				,bi.diploma
				,bi.descval
				,'tlmvl '			= b_utentes.tlmvl
				,'qttRec'			=  0
				,'sms'				= convert(bit,(select COUNT(*) from b_res_enviar (nolock) WHERE b_res_enviar.bistamp=bi.bistamp and (tlmvl LIKE '9%' or  tlmvl LIKE '+351%') and LEN(tlmvl)>=9 ))
				,'clstamp'			= b_utentes.utstamp 
				,'imp'				= convert(bit,(select COUNT(*) from doc_hist_impressoes (nolock) WHERE doc_hist_impressoes.doc_stamp=bi.bistamp))
				,bi.exepcaoTrocaMed
				,'qttres'			= bi.qtt
				,nrreceita			= bi2.nrreceita
				, bi.lote
				, bi2.dataValidade
			from
				Bo (nolock)
				inner join bo2 (nolock) on bo.bostamp=bo2.bo2stamp
				inner join bi (nolock) on bi.bostamp = bo.bostamp
				inner join bi2 (nolock) on bi2.bi2stamp=bi.bistamp
				--inner join bo on cte1.bostamp = bo.bostamp
				inner join st (nolock) on st.ref = bi.ref and st.site_nr = @armazem --and st.inactivo=0
				left join sa (nolock) on st.ref = sa.ref and sa.armazem = @armazem
				left join b_utentes (nolock) on b_utentes.no = bo.no and b_utentes.estab=bo.estab-- and b_utentes.inactivo=0
			where
				bo.ndos in (5, 41, 405)
				AND bo.No = case when @no = -1 then bo.No else @no end
				AND bo.Estab = case when @Estab = -1 then bo.Estab  else @Estab end
				and 1 = case 
							when @produto='' then 1 
							else case 
									when st.ref in (select ref from #dadosSt) then 1 
									else 0 
									end
							end
				AND bo.fechada	= 0
				AND bi.fechada  = 0
				and bi.qtt>bi.qtt2
				--AND st.site_nr = @armazem
				AND bo.site = @site
				AND bo.dataobra BETWEEN (case when @no=-1 then @dataIni else '19000101' end) AND @dataFim
				AND ([bi].[ref] LIKE @ProdRef + '%' OR [bi].[design] LIKE @produto +'%')
				AND [bo].[obrano] = CASE WHEN @venda = -1 THEN [bo].[obrano] ELSE @venda END
				AND ([bo].[nome] LIKE RTRIM(@cliente) +'%' OR [B_utentes].[nrcartao] = RTRIM(@nrCartao) + '%')
				AND [bo].[no] = CASE WHEN @no < 0 THEN [bo].[no] ELSE @no END
				AND [bo].[estab] = CASE WHEN @estab < 0 THEN [bo].[estab] ELSE @estab END
				AND [bo2].[u_nratend] = CASE WHEN @nrAtend = '' THEN [bo2].[u_nratend] ELSE @nrAtend END
				AND bi.ref not in (select ref from #temp_refIgnora)
			GROUP BY 
				bo.bostamp, 
				bi.bistamp, 
				bo.dataobra, 
				bo.ousrhora, 
				bo.nmdos, 
				bo.obrano, 
				bo.nome, 
				bo.no, 
				bo.estab, 
				bi.ref, 
				bi.design, 
				bi.desconto, 
				bi.desc2, 
				bi.qtt, 
				bi.qtt2, 
				bi.ettdeb, 
				bi.edebito, 
				bo.ndos, 
				bi.tabiva, 
				bi.iva, 
				b_utentes.tlmvl, 
				b_utentes.utstamp, 
				bi.exepcaoTrocaMed, 
				bi.qtt, 
				bi2.nrreceita, 
				st.stock, 
				sa.stock, 
				bi.lobs, 
				bi.diploma, 
				bi.descval,
				bi2.fistamp,
				bi.no,
				bi.estab,
				bi.ousrhora,
				st.epv1,
				bi.fechada,
				bi.nopat, bi.lote, bi2.dataValidade
			--exec up_touch_regularizarVendas '', '-1' , '', 234, 0, '20241121', '20241122', '' , 1, 'Administradores', 'Loja 1', 1000, 0
			--order by 
			--	bo.obrano, row
			)
			select * from cte1
			ORDER BY
				fdata DESC, ousrhora DESC
		end
		else
		-- procura documentos em todas as lojas que tanham o mesmo nif 
		--exec up_touch_regularizarVendas '', -1, '', -1, -1, '20200520', '20200528', '', 1, 'Administrador', 'Loja 1', 1000,1
		begin
			with cte1 as (
			SELECT TOP (@Top)
				ft.ftstamp
				,fi.fistamp
				,convert(varchar,fdata,102) as fdata
				,ft.ousrhora
				,CASE WHEN ft.cobrado=1 then ft.nmdoc + ' (S)' else ft.nmdoc END AS nmdoc
				,ft.fno
				,cast(ft.nome as varchar(80)) nome
				,ft.no
				,ft.estab
				,ft.ncont
				,fi.ref
				,fi.design
				,fi.desconto
				,fi.desc2
				,fi.u_descval
				,fi.u_comp
				,qtt = (fi.qtt-[Regularizar].[qtt])
				--,qtt = fi.qtt
				,fi.qtt as qtt2
				,fi.etiliquido
				,fi.etiliquido as etiliquido2
				,totlin = fi.etiliquido
				,fi.etiliquido as totalCdesc
				,fi.epv
				,ft.ndoc
				,ft.u_lote
				,ft.u_nslote
				,ft.u_lote2
				,ft.u_nslote2
				,convert(bit,0) as sel
				,CONVERT(bit,0) as copied
				,ft.u_ltstamp
				,ft.u_ltstamp2
				,ft2.u_codigo
				,ft2.u_codigo2
				,fi.u_diploma
				,0 as dev
				,CONVERT(bit,0) as fact
				,CONVERT(bit,0) as factPago
				,fi.fmarcada
				,eaquisicao	= CASE 
								WHEN #temp_td.u_tipodoc in (8) THEN (fi.qtt-[Regularizar].[qtt])
								WHEN fi.eaquisicao=0 THEN 0
								WHEN [Regularizar].[qtt]>fi.eaquisicao THEN 0
								ELSE fi.eaquisicao - [Regularizar].[qtt]
							  END
				,oeaquisicao = CASE 
								WHEN #temp_td.u_tipodoc in (8) THEN (fi.qtt-[Regularizar].[qtt])
								WHEN fi.eaquisicao=0 THEN 0
								WHEN [Regularizar].[qtt]>fi.eaquisicao THEN 0
								ELSE fi.eaquisicao - [Regularizar].[qtt]
							  END
				,fi.u_psicont
				,fi.u_bencont
				,ft2.u_receita
				,ft2.u_nbenef
				,ft2.u_nbenef2
				,ft2.u_nopresc
				,ft2.u_entpresc
				,u_recdata		= case when ISNULL(dadosPsico.u_recdata,'19000101') !='19000101' or isnull(ft2.token,'')='' then ISNULL(dadosPsico.u_recdata,'19000101') 
								  else
									 isnull((select top 1 isnull(receita_data,'19000101') from Dispensa_Eletronica(nolock) where token = ft2.token),'19000101')
								   end
				,u_medico		= ISNULL(dadosPsico.u_medico,'')
				,u_nmutdisp		= ISNULL(dadosPsico.u_nmutdisp,'')
				,u_moutdisp		= ISNULL(dadosPsico.u_moutdisp,'')
				,u_cputdisp		= ISNULL(dadosPsico.u_cputdisp,'')
				,u_nmutavi		= ISNULL(dadosPsico.u_nmutavi,'')
				,u_moutavi		= ISNULL(dadosPsico.u_moutavi,'')
				,u_cputavi		= ISNULL(dadosPsico.u_cputavi,'')
				,u_ndutavi		= ISNULL(dadosPsico.u_ndutavi,'')
				,u_ddutavi		= ISNULL(dadosPsico.u_ddutavi,'19000101')
				,u_idutavi		= ISNULL(dadosPsico.u_idutavi,0)
				,codDocID		= ISNULL(dadosPsico.codigoDocSPMS, '')
				,fi.u_txcomp
				,fi.u_epvp
				,fi.u_ettent1
				,fi.u_ettent2
				,amostra = CONVERT(bit,fi.amostra)
				,docinvert		= CONVERT(bit,0)
				,u_backoff		= ISNULL(re.u_backoff,0)
				,#temp_td.lancacc
				,ft2.u_design
				,ft2.u_abrev
				,ft2.u_abrev2
				,fi.u_epref
				,ft.tipodoc
				,fi.tabiva
				,fi.iva
				,fi.opcao
				,ft2.token
				,ft2.codAcesso
				,ft2.codDirOpcao
				,fi.id_Dispensa_Eletronica_D as id_validacaoDem
				,ft2.token_efectivacao_compl
				,ft2.c2codpost
				,ficompart	= isnull((select top 1 token from fi_compart (nolock) where fi_compart.fistamp=fi.fistamp),'')
				,fi.valcartao
				,'PVPRES'			= 0
				,'PVPACT'			= 0
				,'fecha'			= CONVERT(bit,0)
				,'valorAdiantado'	= case when (select (edeb-ecred) from cc(nolock) where cc.ftstamp=ft.ftstamp)<>0 then 
										(
										cast(case when ft.nmdoc like '%Factura%' 
										then fi.etiliquido*(select round((edebf-ecredf)/(edeb-ecred),2) from cc(nolock) where cc.ftstamp=ft.ftstamp)
										else fi.etiliquido
										end as numeric(15,2)) 
										)
									else
										cast(case when ft.nmdoc='Venda a Dinheiro' or ft.nmdoc='Fatura Recibo' or carga like '%TSR%'
										then fi.etiliquido
										else 0.00
										end as numeric(15,2)) 
									end 
		--	exec up_touch_regularizarVendas '', -1, '', 215, 0, '20190623', '20190723', '', 1, 'Administrador', 'Loja 1', 100
				,'row'				= 0
				,'fechada'			= CONVERT(bit,0)
				,'qttComp'			= 0
				,'stampAdiantamento' = ''
				,'stock'			= 0
				,'stockArm'			= 0
				,'stockTotal'		= ''
				,'codComp'			= ''
				,'diploma'			= ''
				,descval			= 0
				,'tlmvl '			= b_utentes.tlmvl
				,'qttRec'			=  0
				,'sms'				= convert(bit,0)
				,'clstamp'			= b_utentes.utstamp 
				,'imp'				= convert(bit,0)
				,exepcaoTrocaMed	= ''
				,'qttres'			= 0
				,nrreceita			= ''
				,fi.lote
				,fi2.dataValidade
			FROM
				[dbo].[ft] (nolock)
				INNER JOIN #temp_td ON #temp_td.ndoc=ft.ndoc
				INNER JOIN [dbo].[fi] (nolock) ON ft.ftstamp = fi.ftstamp and fi.rdata BETWEEN @dataIni AND @dataFim --AND fi.ref != 'R000001'
				LEFT JOIN [dbo].[fi2] (nolock) ON fi.fistamp=fi2.fistamp
				INNER JOIN [dbo].[ft2] (nolock) ON ft.ftstamp=ft2.ft2stamp
				INNER JOIN [dbo].[b_utentes] (nolock) ON b_utentes.no = ft.no AND b_utentes.estab = ft.estab and b_utentes.inactivo=0
				LEFT JOIN  [dbo].[B_dadosPsico] dadosPsico (nolock) ON dadosPsico.ftstamp=ft.ftstamp
				LEFT JOIN  [dbo].[re] (nolock) ON re.restamp=(select top 1 restamp from rl (nolock) where rl.ccstamp=ft.ftstamp)
				inner join [dbo].[empresa] (nolock) on ft.site = empresa.site
				/*CROSS APPLY (SELECT ISNULL(SUM(ofi.qtt),0) AS [qtt] FROM [dbo].[fi] as [ofi] where [fi].[fistamp] = [ofi].[ofistamp] AND [ofi].[ndoc] != 75 ) AS [Regularizar]*/
				CROSS APPLY (SELECT ISNULL(SUM(ofi.qtt),0) AS [qtt] FROM [dbo].[fi] as [ofi] (nolock) inner join td (nolock) as otd on ofi.ndoc = otd.ndoc where [fi].[fistamp] = [ofi].[ofistamp] AND [otd].[u_tipodoc] != 3 AND [otd].[codigoDoc] != 32  AND [otd].[nmdoc] not like '%Segur%'  AND [otd].[nmdoc] not like ('%Import%') AND [otd].[nmdoc] not like ('%Manual%')) AS [Regularizar]
			WHERE
				--[ft].[anulado] = 0
				[ft].[tipodoc] in (1,4)
				AND fi.ref != 'R000001'
				AND fi.epromo = 0
				AND ([fi].[qtt]-[Regularizar].[qtt])>0
				AND [fi].[qtt]>0
				AND ([ft].[fdata] BETWEEN @dataIni AND @dataFim)
				AND ([fi].[ref] LIKE @ProdRef + '%' OR [fi].[design] LIKE @produto +'%')
				AND [ft].[fno] = CASE WHEN @venda = -1 THEN [ft].[fno] ELSE @venda END
				AND [ft].[u_nratend] = CASE WHEN @nrAtend = '' THEN [ft].[u_nratend] ELSE @nrAtend END
				AND ([ft].[nome] LIKE RTRIM(@cliente) +'%' OR [B_utentes].[nrcartao] = RTRIM(@nrCartao) + '%')
				AND [ft].[no] = CASE WHEN @no < 0 THEN [ft].[no] ELSE @no END
				AND [ft].[estab] = CASE WHEN @estab < 0 THEN [ft].[estab] ELSE @estab END
				AND empresa.ncont=@nifemp
				AND fi.ref not in (select ref from #temp_refIgnora)
					GROUP BY 
				ft.ftstamp, fi.fistamp,	ft.ousrhora, ft.nome, ft.no, ft.estab, fi.ref, fi.design, fi.desconto, fi.desc2, fi.qtt, fi.tabiva, fi.iva, b_utentes.tlmvl, b_utentes.utstamp, fi.qtt, fi2.fistamp,
				fi.ousrhora, fi.lote, fi2.dataValidade, ft.fdata, ft.cobrado, ft.nmdoc, ft.ndoc, ft.fno,ft.ncont, fi.u_descval, fi.u_comp, Regularizar.qtt, fi.etiliquido, fi.epv, ft.u_lote, ft.u_nslote,
				ft.u_lote2, ft.u_nslote2, ft.u_ltstamp, ft.u_ltstamp2, ft2.u_codigo, ft2.u_codigo2, fi.u_diploma, fi.fmarcada, #temp_td.u_tipodoc, fi.eaquisicao, fi.u_psicont, fi.u_bencont, ft2.u_receita,
				ft2.u_nbenef,ft2.u_nbenef2, ft2.u_nopresc, ft2.u_entpresc, ft2.token, dadosPsico.u_recdata, dadosPsico.u_medico, dadosPsico.u_nmutdisp, dadosPsico.u_moutdisp, dadosPsico.u_cputdisp,dadosPsico.u_nmutavi,
				dadosPsico.u_moutavi,dadosPsico.u_cputavi,dadosPsico.u_ndutavi, dadosPsico.u_ddutavi, dadosPsico.u_ddutavi, dadosPsico.u_idutavi, dadosPsico.codigoDocSPMS, fi.u_txcomp,fi.u_txcomp, fi.u_epvp,
				fi.u_ettent1,fi.u_ettent2, fi.amostra, re.u_backoff, #temp_td.lancacc, ft2.u_design, ft2.u_abrev, ft2.u_abrev2, fi.u_epref, ft.tipodoc, fi.opcao,ft2.codAcesso, ft2.codDirOpcao,ft2.codDirOpcao
				,fi.id_Dispensa_Eletronica_D,ft2.token_efectivacao_compl,ft2.c2codpost,fi.valcartao, ft.carga

			--ORDER BY
			--	fdata DESC, [ft].[ousrhora] DESC
			union all
			select TOP (@Top)
				bo.bostamp as ftstamp
				,bi.bistamp as fistamp
				,convert(varchar,bo.dataobra,102) as fdata
				,bo.ousrhora
				,bo.nmdos AS nmdoc
				,bo.obrano as fno
				,cast(bo.nome as varchar(80)) as nome
				,bo.no as no
				,bo.estab as estab
				,'' as ncont
				,bi.ref
				,bi.design
				,bi.desconto
				,bi.desc2
				,0 as u_descval
				,convert(bit,0) as u_comp
				,qtt = (bi.qtt-bi.qtt2)
				,qtt as qtt2
				, (select isnull((
								select sum(epv) from fi(nolock) 
								where bistamp = bi.bistamp or fi.fistamp = bi2.fistamp--(select top 1 fistamp from bi2 (nolock) where bi2stamp = bi.bistamp)
								AND [fi].[nmdoc] not like ('%Segur%') AND [fi].[nmdoc] not like ('%Import%') AND [fi].[nmdoc] not like ('%Manual%')),0))
					 as etiliquido
		--					exec up_touch_regularizarVendas '', -1, '', -1, -1, '20200223', '20200324', '', 1, 'Administrador', 'Loja 1', 1000
				,(case when bi.desconto=0 then bi.ettdeb else round(bi.ettdeb*((100-bi.desconto)/100),2) end) as etiliquido2
				,  totlin = case when (select count(fi.ftstamp) from fi (nolock) inner join ft (nolock) on fi.ftstamp=ft.ftstamp where fi.nmdoc='Inserção de Receita' and ref = bi.ref
											and ft.no = bi.no and ft.estab = bi.estab
											and CONVERT(VARCHAR(8),fi.ousrhora,108) between CONVERT(VARCHAR(8),dateadd(MINUTE,-2,bi.ousrhora),108) and CONVERT(VARCHAR(8),dateadd(MINUTE,2,bi.ousrhora),108) ) > 0 
								then
									(select top 1 etiliquido from fi (nolock) inner join ft (nolock) on fi.ftstamp=ft.ftstamp where fi.nmdoc='Inserção de Receita' and ref = bi.ref
									and ft.no = bi.no and ft.estab = bi.estab
									and CONVERT(VARCHAR(8),fi.ousrhora,108) between CONVERT(VARCHAR(8),dateadd(MINUTE,-2,bi.ousrhora),108) and CONVERT(VARCHAR(8),dateadd(MINUTE,2,bi.ousrhora),108) )
								else
									--(case when bi.desconto=0 then bi.ettdeb else round(bi.ettdeb*((100-bi.desconto)/100),2) end)
									round(bi.ettdeb,2)
								end 
				,bi.ettdeb as totalCdesc
				,bi.edebito as epv
				,bo.ndos as ndoc
				,0 as u_lote
				,0 as u_nslote
				,0 as u_lote2
				,0 as u_nslote2
				,convert(bit,0) as sel
				,CONVERT(bit,0) as copied
				,'' as u_ltstamp
				,'' as u_ltstamp2
				,'' as u_codigo
				,'' as u_codigo2
				,'' as u_diploma
				,0 as dev
				,CONVERT(bit,0) as fact
				,CONVERT(bit,0) as factPago
				,CONVERT(bit,0) as fmarcada
				,0 as eaquisicao	
				,0 as oeaquisicao
				,CONVERT(bit,0) as u_psicont
				,CONVERT(bit,0) as u_bencont
				,'' as u_receita
				,'' as u_nbenef
				,'' as u_nbenef2
				,'' as u_nopresc
				,'' as u_entpresc
				,u_recdata		= '19000101'
				,u_medico		= ''
				,u_nmutdisp		= ''
				,u_moutdisp		= ''
				,u_cputdisp		= ''
				,u_nmutavi		= ''
				,u_moutavi		= ''
				,u_cputavi		= ''
				,u_ndutavi		= ''
				,u_ddutavi		= '19000101'
				,u_idutavi		= 0
				,codDocID		= ''
				,0 as u_txcomp
				,0 as u_epvp
				,0 as u_ettent1
				,0 as u_ettent2
				,amostra = CONVERT(bit,0)
				,docinvert		= CONVERT(bit,0)
				,u_backoff		= CONVERT(bit,0)
				,CONVERT(bit,0) as lancacc
				,'' as u_design
				,'' as u_abrev
				,'' as u_abrev2
				,0 as u_epref
				,0 as tipodoc
				,bi.tabiva as tabiva
				,bi.iva as iva
				,'' as opcao
				,'' as token
				,'' as codAcesso
				,'' as codDirOpcao
				,'' as id_validacaoDem
				,'' as token_efectivacao_compl
				,'' as c2codpost
				,'' as ficompart
				,0 as valcartao
				,'PVPRES'			= isnull(bi.edebito,0)
				,'PVPACT'			= isnull(st.epv1,0)
				,'fecha'			= CONVERT(bit,0)
				,'valorAdiantado'	= isnull((select sum(etiliquido) from fi (nolock) where fistamp in (select fistamp from bi2 (nolock) where bi2stamp=bi.bistamp)),0)
				,'row'				= CONVERT(int,ROW_NUMBER() over(order by bo.bostamp))
				,'fechada'			= bi.fechada
				,'qttComp'			= bi.nopat
				,'stampAdiantamento' = isnull((select bi2.fistamp from bi2 (nolock) where bi2.bi2stamp = bi.bistamp),'')
				,'stock'			= st.stock
				,'stockArm'			= sa.stock
				,'stockTotal'		= "t:" + CONVERT(VARCHAR,CEILING((select sum(stock) from st (nolock) where ref = bi.ref ))) + " a:" + CONVERT(VARCHAR, CEILING(sa.stock))
				,'codComp'			= bi.lobs
				,bi.diploma
				,bi.descval
				,'tlmvl '			= b_utentes.tlmvl
				,'qttRec'			=  0
				,'sms'				= convert(bit,(select COUNT(*) from b_res_enviar (nolock) WHERE b_res_enviar.bistamp=bi.bistamp and (tlmvl LIKE '9%' or  tlmvl LIKE '+351%') and LEN(tlmvl)>=9 ))
				,'clstamp'			= b_utentes.utstamp 
				,'imp'				= convert(bit,(select COUNT(*) from doc_hist_impressoes (nolock) WHERE doc_hist_impressoes.doc_stamp=bi.bistamp))
				,bi.exepcaoTrocaMed
				,'qttres'			= bi.qtt
				,nrreceita			= bi2.nrreceita
				,bi.lote
				,bi2.dataValidade
			from
				Bo (nolock)
				inner join bo2 (nolock) on bo.bostamp=bo2.bo2stamp
				inner join bi (nolock) on bi.bostamp = bo.bostamp
				LEFT join bi2 (nolock) on bi2.bi2stamp=bi.bistamp
				--inner join bo on cte1.bostamp = bo.bostamp
				inner join st (nolock) on st.ref = bi.ref--  and st.site_nr = @armazem
				left join sa (nolock) on st.ref = sa.ref and sa.armazem = @armazem
				left join b_utentes (nolock) on b_utentes.no = bo.no and b_utentes.estab=bo.estab
				inner join [dbo].[empresa] (nolock) on bo.site = empresa.site
			where
				bo.ndos in (5, 41)
				AND bo.No = case when @no = -1 then bo.No else @no end
				AND bo.Estab = case when @Estab = -1 then bo.Estab  else @Estab end
				and 1 = case 
							when @produto='' then 1 
							else case 
									when st.ref in (select ref from #dadosSt) then 1 
									else 0 
									end
							end
				AND bo.fechada	= 0
				AND bi.fechada  = 0
				and bi.qtt>bi.qtt2
				--AND st.site_nr = @armazem
				and st.site_nr in (select distinct no from empresa (nolock) where ncont=@nifemp)
				--AND bo.site = @site
				and empresa.ncont=@nifemp
				AND bo.dataobra BETWEEN (case when @no=-1 then @dataIni else '19000101' end) AND @dataFim
				AND ([bi].[ref] LIKE @ProdRef + '%' OR [bi].[design] LIKE @produto +'%')
				AND [bo].[obrano] = CASE WHEN @venda = -1 THEN [bo].[obrano] ELSE @venda END
				AND ([bo].[nome] LIKE RTRIM(@cliente) +'%' OR [B_utentes].[nrcartao] = RTRIM(@nrCartao) + '%')
				AND [bo].[no] = CASE WHEN @no < 0 THEN [bo].[no] ELSE @no END
				AND [bo].[estab] = CASE WHEN @estab < 0 THEN [bo].[estab] ELSE @estab END
				AND [bo2].[u_nratend] = CASE WHEN @nrAtend = '' THEN [bo2].[u_nratend] ELSE @nrAtend END
			    AND bi.ref not in (select ref from #temp_refIgnora)
					GROUP BY 
				bo.bostamp, 
				bi.bistamp, 
				bo.dataobra, 
				bo.ousrhora, 
				bo.nmdos, 
				bo.obrano, 
				bo.nome, 
				bo.no, 
				bo.estab, 
				bi.ref, 
				bi.design, 
				bi.desconto, 
				bi.desc2, 
				bi.qtt, 
				bi.qtt2, 
				bi.ettdeb, 
				bi.edebito, 
				bo.ndos, 
				bi.tabiva, 
				bi.iva, 
				b_utentes.tlmvl, 
				b_utentes.utstamp, 
				bi.exepcaoTrocaMed, 
				bi.qtt, 
				bi2.nrreceita, 
				st.stock, 
				sa.stock, 
				bi.lobs, 
				bi.diploma, 
				bi.descval,
				bi2.fistamp,
				bi.no,
				bi.estab,
				bi.ousrhora,
				st.epv1,
				bi.fechada,
				bi.nopat, bi.lote, bi2.dataValidade
			--order by 
			--	bo.obrano, row
			)
			select * from cte1
			ORDER BY
				fdata DESC, ousrhora DESC
		end 
	END
	ELSE
	BEGIN
		-- só procura documentos na loja onde está
		if @MesmoNif=0
		begin 
			with cte1 as (
			SELECT TOP (@Top)
				ft.ftstamp
				,fi.fistamp
				,convert(varchar,fdata,102) as fdata
				,ft.ousrhora
				,CASE WHEN ft.cobrado=1 then ft.nmdoc + ' (S)' else ft.nmdoc END AS nmdoc
				,ft.fno
				,cast(ft.nome as varchar(80)) as nome
				,ft.no
				,ft.estab
				,ft.ncont
				,fi.ref
				,fi.design
				,fi.desconto
				,fi.desc2
				,fi.u_descval
				,fi.u_comp
				,qtt = (fi.qtt-[Regularizar].[qtt])
				--,qtt = fi.qtt
				,fi.qtt as qtt2
				,fi.etiliquido
				,fi.etiliquido as etiliquido2
				,totlin = fi.etiliquido
				,fi.etiliquido as totalCdesc
				,fi.epv
				,ft.ndoc
				,ft.u_lote
				,ft.u_nslote
				,ft.u_lote2
				,ft.u_nslote2
				,convert(bit,0) as sel
				,CONVERT(bit,0) as copied
				,ft.u_ltstamp
				,ft.u_ltstamp2
				,ft2.u_codigo
				,ft2.u_codigo2
				,fi.u_diploma
				,0 as dev
				,CONVERT(bit,0) as fact
				,CONVERT(bit,0) as factPago
				,fi.fmarcada
				,eaquisicao	= CASE 
								WHEN #temp_td.u_tipodoc in (8) THEN (fi.qtt-[Regularizar].[qtt])
								WHEN fi.eaquisicao=0 THEN 0
								WHEN [Regularizar].[qtt]>fi.eaquisicao THEN 0
								ELSE fi.eaquisicao - [Regularizar].[qtt]
							  END
				,oeaquisicao = CASE 
								WHEN #temp_td.u_tipodoc in (8) THEN (fi.qtt-[Regularizar].[qtt])
								WHEN fi.eaquisicao=0 THEN 0
								WHEN [Regularizar].[qtt]>fi.eaquisicao THEN 0
								ELSE fi.eaquisicao - [Regularizar].[qtt]
							  END
				,fi.u_psicont
				,fi.u_bencont
				,ft2.u_receita
				,ft2.u_nbenef
				,ft2.u_nbenef2
				,ft2.u_nopresc
				,ft2.u_entpresc
				,u_recdata		= case when ISNULL(dadosPsico.u_recdata,'19000101') !='19000101' or isnull(ft2.token,'')='' then ISNULL(dadosPsico.u_recdata,'19000101') 
								  else
										isnull((select top 1 isnull(receita_data,'19000101') from Dispensa_Eletronica(nolock) where token = ft2.token),'19000101')
								  end
				,u_medico		= ISNULL(dadosPsico.u_medico,'')
				,u_nmutdisp		= ISNULL(dadosPsico.u_nmutdisp,'')
				,u_moutdisp		= ISNULL(dadosPsico.u_moutdisp,'')
				,u_cputdisp		= ISNULL(dadosPsico.u_cputdisp,'')
				,u_nmutavi		= ISNULL(dadosPsico.u_nmutavi,'')
				,u_moutavi		= ISNULL(dadosPsico.u_moutavi,'')
				,u_cputavi		= ISNULL(dadosPsico.u_cputavi,'')
				,u_ndutavi		= ISNULL(dadosPsico.u_ndutavi,'')
				,u_ddutavi		= ISNULL(dadosPsico.u_ddutavi,'19000101')
				,u_idutavi		= ISNULL(dadosPsico.u_idutavi,0)
				,codDocID		= ISNULL(dadosPsico.codigoDocSPMS, '')
				,fi.u_txcomp
				,fi.u_epvp
				,fi.u_ettent1
				,fi.u_ettent2
				,amostra = CONVERT(bit,fi.amostra)
				,docinvert		= CONVERT(bit,0)
				,u_backoff		= ISNULL(re.u_backoff,0)
				,#temp_td.lancacc
				,ft2.u_design
				,ft2.u_abrev
				,ft2.u_abrev2
				,fi.u_epref
				,ft.tipodoc
				,fi.tabiva
				,fi.iva
				,fi.opcao
				,ft2.token
				,'' as codAcesso 
				,'' as codDirOpcao 
				,fi.id_Dispensa_Eletronica_D as id_validacaoDem
				,'' as token_efectivacao_compl
				,ft2.c2codpost
				,ficompart	= isnull((select top 1 token from fi_compart (nolock) where fi_compart.fistamp=fi.fistamp),'')
				,fi.valcartao
				,'PVPRES'			= 0
				,'PVPACT'			= 0
				,'fecha'			= CONVERT(bit,0)
				,'valorAdiantado'	= case when (select (edeb-ecred) from cc(nolock) where cc.ftstamp=ft.ftstamp)<>0 then 
										(
										cast(case when ft.nmdoc like '%Factura%'  
										then fi.etiliquido*(select round((edebf-ecredf)/(edeb-ecred),2) from cc(nolock) where cc.ftstamp=ft.ftstamp)
										else fi.etiliquido
										end as numeric(15,2)) 
										)
									else
										cast(case when ft.nmdoc='Venda a Dinheiro' or ft.nmdoc='Fatura Recibo' or carga like '%TSR%'
										then fi.etiliquido
										else 0.00
										end as numeric(15,2)) 
									end 
				,'row'				= 0
				,'fechada'			= CONVERT(bit,0)
				,'qttComp'			= 0
				,'stampAdiantamento' = ''
				,'stock'			= 0
				,'stockArm'			= 0
				,'stockTotal'		= ''
				,'codComp'			= ''
				,'diploma'			= ''
				,descval			= 0
				,'tlmvl '			= b_utentes.tlmvl
				,'qttRec'			=  0
				,'sms'				= convert(bit,0)
				,'clstamp'			= b_utentes.utstamp 
				,'imp'				= convert(bit,0)
				,exepcaoTrocaMed	= ''
				,'qttres'			= 0
				,nrreceita			= ''
				,fi.lote
				,fi2.dataValidade
			FROM
				[dbo].[ft] (nolock)
				INNER JOIN #temp_td ON #temp_td.ndoc=ft.ndoc
				INNER JOIN [dbo].[fi] (nolock) ON ft.ftstamp = fi.ftstamp and fi.rdata BETWEEN @dataIni AND @dataFim --AND fi.ref != 'R000001'
				LEFT JOIN [dbo].[fi2] (nolock) ON fi.fistamp=fi2.fistamp
				INNER JOIN [dbo].[ft2] (nolock)  ON ft.ftstamp=ft2.ft2stamp
				INNER JOIN [dbo].[b_utentes] (nolock) ON b_utentes.no = ft.no AND b_utentes.estab = ft.estab and b_utentes.inactivo=0
				LEFT JOIN  [dbo].[B_dadosPsico] dadosPsico (nolock) ON dadosPsico.ftstamp=ft.ftstamp
				LEFT JOIN  [dbo].[re] (nolock) ON re.restamp=(select top 1 restamp from rl where rl.ccstamp=ft.ftstamp)		
				--CROSS APPLY (SELECT ISNULL(SUM(ofi.qtt),0) AS [qtt] FROM [dbo].[fi] as [ofi] inner join td as otd on ofi.ndoc = otd.ndoc where [fi].[fistamp] = [ofi].[ofistamp] AND [otd].[u_tipodoc] != 3 ) AS [Regularizar]
				CROSS APPLY (SELECT ISNULL(SUM(ofi.qtt),0) AS [qtt] FROM [dbo].[fi] as [ofi] (nolock) inner join td as otd (nolock) on ofi.ndoc = otd.ndoc where [fi].[fistamp] = [ofi].[ofistamp] AND [otd].[u_tipodoc] != 3 AND [otd].[codigoDoc] != 32  AND [otd].[nmdoc] not like '%Segur%'  AND [otd].[nmdoc] not like ('%Import%') AND [otd].[nmdoc] not like ('%Manual%') ) AS [Regularizar]
			WHERE
				[ft].[tipodoc] in (1,4)
				AND fi.ref != 'R000001'
				AND fi.epromo = 0
				AND ([fi].[qtt]-[Regularizar].[qtt])>0
				AND [fi].[qtt]>0
				AND ([ft].[fdata] BETWEEN @dataIni AND @dataFim)
				AND ([fi].[ref] LIKE @ProdRef + '%' OR [fi].[design] LIKE @produto +'%')
				AND [ft].[fno] = CASE WHEN @venda = -1 THEN [ft].[fno] ELSE @venda END
				AND [ft].[u_nratend] = CASE WHEN @nrAtend = '' THEN [ft].[u_nratend] ELSE @nrAtend END
				AND ([ft].[nome] LIKE RTRIM(@cliente) +'%' OR [B_utentes].[nrcartao] = RTRIM(@nrCartao) + '%')
				AND [ft].[no] = CASE WHEN @no < 0 THEN [ft].[no] ELSE @no END
				AND [ft].[estab] = CASE WHEN @estab < 0 THEN [ft].[estab] ELSE @estab END
				AND [ft].[site] = @site
				AND fi.ref not in (select ref from #temp_refIgnora)
				GROUP BY 
				ft.ftstamp, fi.fistamp,	ft.ousrhora, ft.nome, ft.no, ft.estab, fi.ref, fi.design, fi.desconto, fi.desc2, fi.qtt, fi.tabiva, fi.iva, b_utentes.tlmvl, b_utentes.utstamp, fi.qtt, fi2.fistamp,
				fi.ousrhora, fi.lote, fi2.dataValidade, ft.fdata, ft.cobrado, ft.nmdoc, ft.ndoc, ft.fno,ft.ncont, fi.u_descval, fi.u_comp, Regularizar.qtt, fi.etiliquido, fi.epv, ft.u_lote, ft.u_nslote,
				ft.u_lote2, ft.u_nslote2, ft.u_ltstamp, ft.u_ltstamp2, ft2.u_codigo, ft2.u_codigo2, fi.u_diploma, fi.fmarcada, #temp_td.u_tipodoc, fi.eaquisicao, fi.u_psicont, fi.u_bencont, ft2.u_receita,
				ft2.u_nbenef,ft2.u_nbenef2, ft2.u_nopresc, ft2.u_entpresc, ft2.token, dadosPsico.u_recdata, dadosPsico.u_medico, dadosPsico.u_nmutdisp, dadosPsico.u_moutdisp, dadosPsico.u_cputdisp,dadosPsico.u_nmutavi,
				dadosPsico.u_moutavi,dadosPsico.u_cputavi,dadosPsico.u_ndutavi, dadosPsico.u_ddutavi, dadosPsico.u_ddutavi, dadosPsico.u_idutavi, dadosPsico.codigoDocSPMS, fi.u_txcomp,fi.u_txcomp, fi.u_epvp,
				fi.u_ettent1,fi.u_ettent2, fi.amostra, re.u_backoff, #temp_td.lancacc, ft2.u_design, ft2.u_abrev, ft2.u_abrev2, fi.u_epref, ft.tipodoc, fi.opcao,ft2.codAcesso, ft2.codDirOpcao,ft2.codDirOpcao
				,fi.id_Dispensa_Eletronica_D,ft2.token_efectivacao_compl,ft2.c2codpost,fi.valcartao, ft.carga
			--ORDER BY
			--	fdata DESC, [ft].[ousrhora] DESC
			union all
			select TOP (@Top)
				bo.bostamp as ftstamp
				,bi.bistamp as fistamp
				,convert(varchar,bo.dataobra,102) as fdata
				,bo.ousrhora
				,bo.nmdos AS nmdoc
				,bo.obrano as fno
				,cast(bo.nome as varchar(80)) as nome
				,bo.no as no
				,bo.estab as estab
				,'' as ncont
				,bi.ref
				,bi.design
				,bi.desconto
				,bi.desc2
				,0 as u_descval
				,convert(bit,0) as u_comp
				,qtt = (bi.qtt-bi.qtt2)
				,qtt as qtt2
				, (select isnull((
								select sum(epv) from fi (nolock) 
								where bistamp = bi.bistamp or fi.fistamp = bi2.fistamp--(select top 1 fistamp from bi2 (nolock) where bi2stamp = bi.bistamp)
								AND [fi].[nmdoc] not like ('%Segur%') AND [fi].[nmdoc] not like ('%Import%') AND [fi].[nmdoc] not like ('%Manual%')),0))
					 as etiliquido
				,(case when bi.desconto=0 then bi.ettdeb else round(bi.ettdeb*((100-bi.desconto)/100),2) end) as etiliquido2
				--		exec up_touch_regularizarVendas '', -1, '', -1, -1, '20200225', '20200326', '', 1, 'Administrador', 'Loja 1', 1000
				,  totlin = case when (select count(fi.ftstamp) from fi (nolock) inner join ft (nolock) on fi.ftstamp=ft.ftstamp where fi.nmdoc='Inserção de Receita' and ref = bi.ref
											and ft.no = bi.no and ft.estab = bi.estab
											and CONVERT(VARCHAR(8),fi.ousrhora,108) between CONVERT(VARCHAR(8),dateadd(MINUTE,-2,bi.ousrhora),108) and CONVERT(VARCHAR(8),dateadd(MINUTE,2,bi.ousrhora),108) ) > 0 
								then
									(select top 1 etiliquido from fi (nolock) inner join ft (nolock) on fi.ftstamp=ft.ftstamp where fi.nmdoc='Inserção de Receita' and ref = bi.ref
									and ft.no = bi.no and ft.estab = bi.estab
									and CONVERT(VARCHAR(8),fi.ousrhora,108) between CONVERT(VARCHAR(8),dateadd(MINUTE,-2,bi.ousrhora),108) and CONVERT(VARCHAR(8),dateadd(MINUTE,2,bi.ousrhora),108) )
								else
									--(case when bi.desconto=0 then bi.ettdeb else round(bi.ettdeb*((100-bi.desconto)/100),2) end)
									round(bi.ettdeb,2)
								end 
				,bi.ettdeb as totalCdesc
				,bi.edebito as epv
				,bo.ndos as ndoc
				,0 as u_lote
				,0 as u_nslote
				,0 as u_lote2
				,0 as u_nslote2
				,convert(bit,0) as sel
				,CONVERT(bit,0) as copied
				,'' as u_ltstamp
				,'' as u_ltstamp2
				,'' as u_codigo
				,'' as u_codigo2
				,'' as u_diploma
				,0 as dev
				,CONVERT(bit,0) as fact
				,CONVERT(bit,0) as factPago
				,CONVERT(bit,0) as fmarcada
				,0 as eaquisicao	
				,0 as oeaquisicao
				,CONVERT(bit,0) as u_psicont
				,CONVERT(bit,0) as u_bencont
				,'' as u_receita
				,'' as u_nbenef
				,'' as u_nbenef2
				,'' as u_nopresc
				,'' as u_entpresc
				,u_recdata		= '19000101'
				,u_medico		= ''
				,u_nmutdisp		= ''
				,u_moutdisp		= ''
				,u_cputdisp		= ''
				,u_nmutavi		= ''
				,u_moutavi		= ''
				,u_cputavi		= ''
				,u_ndutavi		= ''
				,u_ddutavi		= '19000101'
				,u_idutavi		= 0
				,codDocID		= ''
				,0 as u_txcomp
				,0 as u_epvp
				,0 as u_ettent1
				,0 as u_ettent2
				,amostra = CONVERT(bit,0)
				,docinvert		= CONVERT(bit,0)
				,u_backoff		= CONVERT(bit,0)
				,CONVERT(bit,0) as lancacc
				,'' as u_design
				,'' as u_abrev
				,'' as u_abrev2
				,0 as u_epref
				,0 as tipodoc
				,bi.tabiva as tabiva
				,bi.iva as iva
				,'' as opcao
				,'' as token
				,'' as codAcesso
				,'' as codDirOpcao
				,'' as id_validacaoDem
				,'' as token_efectivacao_compl
				,'' as c2codpost
				,'' as ficompart
				,0 as valcartao
				,'PVPRES'			= isnull(bi.edebito,0)
				,'PVPACT'			= isnull(st.epv1,0)
				,'fecha'			= CONVERT(bit,0)
				,'valorAdiantado'	= isnull((select sum(etiliquido) from fi (nolock) where fistamp in (select fistamp from bi2 (nolock) where bi2stamp=bi.bistamp)),0)
				,'row'				= CONVERT(int,ROW_NUMBER() over(order by bo.bostamp))
				,'fechada'			= bi.fechada
				,'qttComp'			= bi.nopat
				,'stampAdiantamento' = isnull((select bi2.fistamp from bi2 (nolock) where bi2.bi2stamp = bi.bistamp),'')
				,'stock'			= st.stock
				,'stockArm'			= sa.stock
				,'stockTotal'		= "t:" + CONVERT(VARCHAR,CEILING(st.stock)) + " a:" + CONVERT(VARCHAR, CEILING(sa.stock))
				,'codComp'			= bi.lobs
				,bi.diploma
				,bi.descval
				,'tlmvl '			= b_utentes.tlmvl
				,'qttRec'			=  0
				,'sms'				= convert(bit,(select COUNT(*) from b_res_enviar (nolock) WHERE b_res_enviar.bistamp=bi.bistamp and (tlmvl LIKE '9%' or  tlmvl LIKE '+351%') and LEN(tlmvl)>=9 ))
				,'clstamp'			= b_utentes.utstamp 
				,'imp'				= convert(bit,(select COUNT(*) from doc_hist_impressoes (nolock) WHERE doc_hist_impressoes.doc_stamp=bi.bistamp))
				,bi.exepcaoTrocaMed
				,'qttres'			= bi.qtt
				,nrreceita			= bi2.nrreceita
				,bi.lote
				,bi2.dataValidade
			from
				Bo (nolock)
				inner join bo2 (nolock) on bo.bostamp=bo2.bo2stamp
				inner join bi (nolock) on bi.bostamp = bo.bostamp
				LEFT join bi2 (nolock) on bi2.bi2stamp=bi.bistamp
				--inner join bo on cte1.bostamp = bo.bostamp
				inner join st (nolock) on st.ref = bi.ref  and st.site_nr = @armazem
				left join sa (nolock) on st.ref = sa.ref and sa.armazem = @armazem
				left join b_utentes (nolock) on b_utentes.no = bo.no and b_utentes.estab=bo.estab
			where
				bo.ndos in (5, 41)
				AND bo.No = case when @no = -1 then bo.No else @no end
				AND bo.Estab = case when @Estab = -1 then bo.Estab  else @Estab end
				and 1 = case 
							when @produto='' then 1 
							else case 
									when st.ref in (select ref from #dadosSt) then 1 
									else 0 
									end
							end
				AND bo.fechada	= 0
				AND bi.fechada  = 0
				and bi.qtt>bi.qtt2
				--AND st.site_nr = @armazem
				AND bo.site = @site
				AND bo.dataobra BETWEEN (case when @no=-1 then @dataIni else '19000101' end) AND @dataFim
				AND ([bi].[ref] LIKE @ProdRef + '%' OR [bi].[design] LIKE @produto +'%')
				AND [bo].[obrano] = CASE WHEN @venda = -1 THEN [bo].[obrano] ELSE @venda END
				AND ([bo].[nome] LIKE RTRIM(@cliente) +'%' OR [B_utentes].[nrcartao] = RTRIM(@nrCartao) + '%')
				AND [bo].[no] = CASE WHEN @no < 0 THEN [bo].[no] ELSE @no END
				AND [bo].[estab] = CASE WHEN @estab < 0 THEN [bo].[estab] ELSE @estab END
				AND [bo2].[u_nratend] = CASE WHEN @nrAtend = '' THEN [bo2].[u_nratend] ELSE @nrAtend END
			    AND  bi.ref not in (select ref from #temp_refIgnora)
				GROUP BY 
				bo.bostamp, 
				bi.bistamp, 
				bo.dataobra, 
				bo.ousrhora, 
				bo.nmdos, 
				bo.obrano, 
				bo.nome, 
				bo.no, 
				bo.estab, 
				bi.ref, 
				bi.design, 
				bi.desconto, 
				bi.desc2, 
				bi.qtt, 
				bi.qtt2, 
				bi.ettdeb, 
				bi.edebito, 
				bo.ndos, 
				bi.tabiva, 
				bi.iva, 
				b_utentes.tlmvl, 
				b_utentes.utstamp, 
				bi.exepcaoTrocaMed, 
				bi.qtt, 
				bi2.nrreceita, 
				st.stock, 
				sa.stock, 
				bi.lobs, 
				bi.diploma, 
				bi.descval,
				bi2.fistamp,
				bi.no,
				bi.estab,
				bi.ousrhora,
				st.epv1,
				bi.fechada,
				bi.nopat, bi.lote, bi2.dataValidade
			--order by 
			--	bo.obrano, row
			)
			select * from cte1
			ORDER BY
				fdata DESC, ousrhora DESC
		end
		else
		begin
			-- procura em lojas com o mesmo NIF
			with cte1 as (
			SELECT TOP (@Top)
				ft.ftstamp
				,fi.fistamp
				,convert(varchar,fdata,102) as fdata
				,ft.ousrhora
				,CASE WHEN ft.cobrado=1 then ft.nmdoc + ' (S)' else ft.nmdoc END AS nmdoc
				,ft.fno
				,cast(ft.nome as varchar(80)) as nome
				,ft.no
				,ft.estab
				,ft.ncont
				,fi.ref
				,fi.design
				,fi.desconto
				,fi.desc2
				,fi.u_descval
				,fi.u_comp
				,qtt = (fi.qtt-[Regularizar].[qtt])
				--,qtt = fi.qtt
				,fi.qtt as qtt2
				,fi.etiliquido
				,fi.etiliquido as etiliquido2
				,totlin = fi.etiliquido
				,fi.etiliquido as totalCdesc
				,fi.epv
				,ft.ndoc
				,ft.u_lote
				,ft.u_nslote
				,ft.u_lote2
				,ft.u_nslote2
				,convert(bit,0) as sel
				,CONVERT(bit,0) as copied
				,ft.u_ltstamp
				,ft.u_ltstamp2
				,ft2.u_codigo
				,ft2.u_codigo2
				,fi.u_diploma
				,0 as dev
				,CONVERT(bit,0) as fact
				,CONVERT(bit,0) as factPago
				,fi.fmarcada
				,eaquisicao	= CASE 
								WHEN #temp_td.u_tipodoc in (8) THEN (fi.qtt-[Regularizar].[qtt])
								WHEN fi.eaquisicao=0 THEN 0
								WHEN [Regularizar].[qtt]>fi.eaquisicao THEN 0
								ELSE fi.eaquisicao - [Regularizar].[qtt]
							  END
				,oeaquisicao = CASE 
								WHEN #temp_td.u_tipodoc in (8) THEN (fi.qtt-[Regularizar].[qtt])
								WHEN fi.eaquisicao=0 THEN 0
								WHEN [Regularizar].[qtt]>fi.eaquisicao THEN 0
								ELSE fi.eaquisicao - [Regularizar].[qtt]
							  END
				,fi.u_psicont
				,fi.u_bencont
				,ft2.u_receita
				,ft2.u_nbenef
				,ft2.u_nbenef2
				,ft2.u_nopresc
				,ft2.u_entpresc
				,u_recdata		= case when ISNULL(dadosPsico.u_recdata,'19000101') !='19000101' or isnull(ft2.token,'')='' then ISNULL(dadosPsico.u_recdata,'19000101') 
								  else
										isnull((select top 1 isnull(receita_data,'19000101') from Dispensa_Eletronica(nolock) where token = ft2.token),'19000101')
								  end
				,u_medico		= ISNULL(dadosPsico.u_medico,'')
				,u_nmutdisp		= ISNULL(dadosPsico.u_nmutdisp,'')
				,u_moutdisp		= ISNULL(dadosPsico.u_moutdisp,'')
				,u_cputdisp		= ISNULL(dadosPsico.u_cputdisp,'')
				,u_nmutavi		= ISNULL(dadosPsico.u_nmutavi,'')
				,u_moutavi		= ISNULL(dadosPsico.u_moutavi,'')
				,u_cputavi		= ISNULL(dadosPsico.u_cputavi,'')
				,u_ndutavi		= ISNULL(dadosPsico.u_ndutavi,'')
				,u_ddutavi		= ISNULL(dadosPsico.u_ddutavi,'19000101')
				,u_idutavi		= ISNULL(dadosPsico.u_idutavi,0)
				,codDocID		= ISNULL(dadosPsico.codigoDocSPMS, '')
				,fi.u_txcomp
				,fi.u_epvp
				,fi.u_ettent1
				,fi.u_ettent2
				,amostra = CONVERT(bit,fi.amostra)
				,docinvert		= CONVERT(bit,0)
				,u_backoff		= ISNULL(re.u_backoff,0)
				,#temp_td.lancacc
				,ft2.u_design
				,ft2.u_abrev
				,ft2.u_abrev2
				,fi.u_epref
				,ft.tipodoc
				,fi.tabiva
				,fi.iva
				,fi.opcao
				,ft2.token
				,'' as codAcesso 
				,'' as codDirOpcao 
				,fi.id_Dispensa_Eletronica_D as id_validacaoDem
				,'' as token_efectivacao_compl
				,ft2.c2codpost
				,ficompart	= isnull((select top 1 token from fi_compart (nolock) where fi_compart.fistamp=fi.fistamp),'')
				,fi.valcartao
				,'PVPRES'			= 0
				,'PVPACT'			= 0
				,'fecha'			= CONVERT(bit,0)
				,'valorAdiantado'	= case when (select (edeb-ecred) from cc(nolock) where cc.ftstamp=ft.ftstamp)<>0 then 
										(
										cast(case when ft.nmdoc like '%Factura%' 
										then fi.etiliquido*(select round((edebf-ecredf)/(edeb-ecred),2) from cc(nolock) where cc.ftstamp=ft.ftstamp)
										else fi.etiliquido
										end as numeric(15,2)) 
										)
									else
										cast(case when ft.nmdoc='Venda a Dinheiro' or ft.nmdoc='Fatura Recibo'  or carga like '%TSR%'
										then fi.etiliquido
										else 0.00
										end as numeric(15,2)) 
									end 
				,'row'				= 0
				,'fechada'			= CONVERT(bit,0)
				,'qttComp'			= 0
				,'stampAdiantamento' = ''
				,'stock'			= 0
				,'stockArm'			= 0
				,'stockTotal'		= ''
				,'codComp'			= ''
				,'diploma'			= ''
				,descval			= 0
				,'tlmvl '			= b_utentes.tlmvl
				,'qttRec'			=  0
				,'sms'				= convert(bit,0)
				,'clstamp'			= b_utentes.utstamp 
				,'imp'				= convert(bit,0)
				,exepcaoTrocaMed	= ''
				,'qttres'			= 0
				,nrreceita			= ''
				, fi.lote
				, fi2.dataValidade
			FROM
				[dbo].[ft] (nolock)
				INNER JOIN #temp_td ON #temp_td.ndoc=ft.ndoc
				INNER JOIN [dbo].[fi] (nolock) ON ft.ftstamp = fi.ftstamp and fi.rdata BETWEEN @dataIni AND @dataFim --AND fi.ref != 'R000001'
				LEFT JOIN [dbo].[fi2] (nolock) ON fi.fistamp=fi2.fistamp
				INNER JOIN [dbo].[ft2] (nolock)  ON ft.ftstamp=ft2.ft2stamp
				INNER JOIN [dbo].[b_utentes] (nolock) ON b_utentes.no = ft.no AND b_utentes.estab = ft.estab and b_utentes.inactivo=0
				LEFT JOIN  [dbo].[B_dadosPsico] dadosPsico (nolock) ON dadosPsico.ftstamp=ft.ftstamp
				LEFT JOIN  [dbo].[re] (nolock) ON re.restamp=(select top 1 restamp from rl where rl.ccstamp=ft.ftstamp)	
				inner join [dbo].[empresa] (nolock) on ft.site = empresa.site	
				--CROSS APPLY (SELECT ISNULL(SUM(ofi.qtt),0) AS [qtt] FROM [dbo].[fi] as [ofi] inner join td as otd on ofi.ndoc = otd.ndoc where [fi].[fistamp] = [ofi].[ofistamp] AND [otd].[u_tipodoc] != 3 ) AS [Regularizar]
				CROSS APPLY (SELECT ISNULL(SUM(ofi.qtt),0) AS [qtt] FROM [dbo].[fi] as [ofi] (nolock) inner join td as otd (nolock) on ofi.ndoc = otd.ndoc where [fi].[fistamp] = [ofi].[ofistamp] AND [otd].[u_tipodoc] != 3 AND [otd].[codigoDoc] != 32  AND [otd].[nmdoc] not like '%Segur%'  AND [otd].[nmdoc] not like ('%Import%') AND [otd].[nmdoc] not like ('%Manual%')) AS [Regularizar]
			WHERE
				[ft].[tipodoc] in (1,4)
				AND fi.ref != 'R000001'
				AND fi.epromo = 0
				AND ([fi].[qtt]-[Regularizar].[qtt])>0
				AND [fi].[qtt]>0
				AND ([ft].[fdata] BETWEEN @dataIni AND @dataFim)
				AND ([fi].[ref] LIKE @ProdRef + '%' OR [fi].[design] LIKE @produto +'%')
				AND [ft].[fno] = CASE WHEN @venda = -1 THEN [ft].[fno] ELSE @venda END
				AND [ft].[u_nratend] = CASE WHEN @nrAtend = '' THEN [ft].[u_nratend] ELSE @nrAtend END
				AND ([ft].[nome] LIKE RTRIM(@cliente) +'%' OR [B_utentes].[nrcartao] = RTRIM(@nrCartao) + '%')
				AND [ft].[no] = CASE WHEN @no < 0 THEN [ft].[no] ELSE @no END
				AND [ft].[estab] = CASE WHEN @estab < 0 THEN [ft].[estab] ELSE @estab END
				AND empresa.ncont=@nifemp
				AND fi.ref not in (select ref from #temp_refIgnora)
				GROUP BY 
				ft.ftstamp, fi.fistamp,	ft.ousrhora, ft.nome, ft.no, ft.estab, fi.ref, fi.design, fi.desconto, fi.desc2, fi.qtt, fi.tabiva, fi.iva, b_utentes.tlmvl, b_utentes.utstamp, fi.qtt, fi2.fistamp,
				fi.ousrhora, fi.lote, fi2.dataValidade, ft.fdata, ft.cobrado, ft.nmdoc, ft.ndoc, ft.fno,ft.ncont, fi.u_descval, fi.u_comp, Regularizar.qtt, fi.etiliquido, fi.epv, ft.u_lote, ft.u_nslote,
				ft.u_lote2, ft.u_nslote2, ft.u_ltstamp, ft.u_ltstamp2, ft2.u_codigo, ft2.u_codigo2, fi.u_diploma, fi.fmarcada, #temp_td.u_tipodoc, fi.eaquisicao, fi.u_psicont, fi.u_bencont, ft2.u_receita,
				ft2.u_nbenef,ft2.u_nbenef2, ft2.u_nopresc, ft2.u_entpresc, ft2.token, dadosPsico.u_recdata, dadosPsico.u_medico, dadosPsico.u_nmutdisp, dadosPsico.u_moutdisp, dadosPsico.u_cputdisp,dadosPsico.u_nmutavi,
				dadosPsico.u_moutavi,dadosPsico.u_cputavi,dadosPsico.u_ndutavi, dadosPsico.u_ddutavi, dadosPsico.u_ddutavi, dadosPsico.u_idutavi, dadosPsico.codigoDocSPMS, fi.u_txcomp,fi.u_txcomp, fi.u_epvp,
				fi.u_ettent1,fi.u_ettent2, fi.amostra, re.u_backoff, #temp_td.lancacc, ft2.u_design, ft2.u_abrev, ft2.u_abrev2, fi.u_epref, ft.tipodoc, fi.opcao,ft2.codAcesso, ft2.codDirOpcao,ft2.codDirOpcao
				,fi.id_Dispensa_Eletronica_D,ft2.token_efectivacao_compl,ft2.c2codpost,fi.valcartao, ft.carga
			--ORDER BY
			--	fdata DESC, [ft].[ousrhora] DESC
			union all
			select TOP (@Top)
				bo.bostamp as ftstamp
				,bi.bistamp as fistamp
				,convert(varchar,bo.dataobra,102) as fdata
				,bo.ousrhora
				,bo.nmdos AS nmdoc
				,bo.obrano as fno
				,cast(bo.nome as varchar(80)) as nome
				,bo.no as no
				,bo.estab as estab
				,'' as ncont
				,bi.ref
				,bi.design
				,bi.desconto
				,bi.desc2
				,0 as u_descval
				,convert(bit,0) as u_comp
				,qtt = (bi.qtt-bi.qtt2)
				,qtt as qtt2
				, (select isnull((
								select sum(epv) from fi (nolock) 
								where bistamp = bi.bistamp or fi.fistamp = bi2.fistamp--(select top 1 fistamp from bi2 (nolock) where bi2stamp = bi.bistamp)
								AND [fi].[nmdoc] not like ('%Segur%') AND [fi].[nmdoc] not like ('%Import%') AND [fi].[nmdoc] not like ('%Manual%')),0))
					 as etiliquido
				,(case when bi.desconto=0 then bi.ettdeb else round(bi.ettdeb*((100-bi.desconto)/100),2) end) as etiliquido2
				--		exec up_touch_regularizarVendas '', -1, '', -1, -1, '20200225', '20200326', '', 1, 'Administrador', 'Loja 1', 1000
				,  totlin = case when (select count(fi.ftstamp) from fi (nolock) inner join ft (nolock) on fi.ftstamp=ft.ftstamp where fi.nmdoc='Inserção de Receita' and ref = bi.ref
											and ft.no = bi.no and ft.estab = bi.estab
											and CONVERT(VARCHAR(8),fi.ousrhora,108) between CONVERT(VARCHAR(8),dateadd(MINUTE,-2,bi.ousrhora),108) and CONVERT(VARCHAR(8),dateadd(MINUTE,2,bi.ousrhora),108) ) > 0 
								then
									(select top 1 etiliquido from fi (nolock) inner join ft (nolock) on fi.ftstamp=ft.ftstamp where fi.nmdoc='Inserção de Receita' and ref = bi.ref
									and ft.no = bi.no and ft.estab = bi.estab
									and CONVERT(VARCHAR(8),fi.ousrhora,108) between CONVERT(VARCHAR(8),dateadd(MINUTE,-2,bi.ousrhora),108) and CONVERT(VARCHAR(8),dateadd(MINUTE,2,bi.ousrhora),108) )
								else
									--(case when bi.desconto=0 then bi.ettdeb else round(bi.ettdeb*((100-bi.desconto)/100),2) end)
									round(bi.ettdeb,2)
								end 
				,bi.ettdeb as totalCdesc
				,bi.edebito as epv
				,bo.ndos as ndoc
				,0 as u_lote
				,0 as u_nslote
				,0 as u_lote2
				,0 as u_nslote2
				,convert(bit,0) as sel
				,CONVERT(bit,0) as copied
				,'' as u_ltstamp
				,'' as u_ltstamp2
				,'' as u_codigo
				,'' as u_codigo2
				,'' as u_diploma
				,0 as dev
				,CONVERT(bit,0) as fact
				,CONVERT(bit,0) as factPago
				,CONVERT(bit,0) as fmarcada
				,0 as eaquisicao	
				,0 as oeaquisicao
				,CONVERT(bit,0) as u_psicont
				,CONVERT(bit,0) as u_bencont
				,'' as u_receita
				,'' as u_nbenef
				,'' as u_nbenef2
				,'' as u_nopresc
				,'' as u_entpresc
				,u_recdata		= '19000101'
				,u_medico		= ''
				,u_nmutdisp		= ''
				,u_moutdisp		= ''
				,u_cputdisp		= ''
				,u_nmutavi		= ''
				,u_moutavi		= ''
				,u_cputavi		= ''
				,u_ndutavi		= ''
				,u_ddutavi		= '19000101'
				,u_idutavi		= 0
				,codDocID		= ''
				,0 as u_txcomp
				,0 as u_epvp
				,0 as u_ettent1
				,0 as u_ettent2
				,amostra = CONVERT(bit,0)
				,docinvert		= CONVERT(bit,0)
				,u_backoff		= CONVERT(bit,0)
				,CONVERT(bit,0) as lancacc
				,'' as u_design
				,'' as u_abrev
				,'' as u_abrev2
				,0 as u_epref
				,0 as tipodoc
				,bi.tabiva as tabiva
				,bi.iva as iva
				,'' as opcao
				,'' as token
				,'' as codAcesso
				,'' as codDirOpcao
				,'' as id_validacaoDem
				,'' as token_efectivacao_compl
				,'' as c2codpost
				,'' as ficompart
				,0 as valcartao
				,'PVPRES'			= isnull(bi.edebito,0)
				,'PVPACT'			= isnull(st.epv1,0)
				,'fecha'			= CONVERT(bit,0)
				,'valorAdiantado'	= isnull((select sum(etiliquido) from fi (nolock) where fistamp in (select fistamp from bi2 (nolock) where bi2stamp=bi.bistamp)),0)
				,'row'				= CONVERT(int,ROW_NUMBER() over(order by bo.bostamp))
				,'fechada'			= bi.fechada
				,'qttComp'			= bi.nopat
				,'stampAdiantamento' = isnull((select bi2.fistamp from bi2 (nolock) where bi2.bi2stamp = bi.bistamp),'')
				,'stock'			= st.stock
				,'stockArm'			= sa.stock
				,'stockTotal'		= "t:" + CONVERT(VARCHAR,CEILING(st.stock)) + " a:" + CONVERT(VARCHAR, CEILING(sa.stock))
				,'codComp'			= bi.lobs
				,bi.diploma
				,bi.descval
				,'tlmvl '			= b_utentes.tlmvl
				,'qttRec'			=  0
				,'sms'				= convert(bit,(select COUNT(*) from b_res_enviar (nolock) WHERE b_res_enviar.bistamp=bi.bistamp and (tlmvl LIKE '9%' or  tlmvl LIKE '+351%') and LEN(tlmvl)>=9 ))
				,'clstamp'			= b_utentes.utstamp 
				,'imp'				= convert(bit,(select COUNT(*) from doc_hist_impressoes (nolock) WHERE doc_hist_impressoes.doc_stamp=bi.bistamp))
				,bi.exepcaoTrocaMed
				,'qttres'			= bi.qtt
				,nrreceita			= bi2.nrreceita
				,bi.lote
				,bi2.dataValidade
			from
				Bo (nolock)
				inner join bo2 (nolock) on bo.bostamp=bo2.bo2stamp
				inner join bi (nolock) on bi.bostamp = bo.bostamp
				LEFT join bi2 (nolock) on bi2.bi2stamp=bi.bistamp
				--inner join bo on cte1.bostamp = bo.bostamp
				inner join st (nolock) on st.ref = bi.ref 
				left join sa (nolock) on st.ref = sa.ref and sa.armazem = @armazem
				left join b_utentes (nolock) on b_utentes.no = bo.no and b_utentes.estab=bo.estab
				inner join [dbo].[empresa] (nolock) on bo.site = empresa.site
			where
				bo.ndos in (5, 41)
				AND bo.No = case when @no = -1 then bo.No else @no end
				AND bo.Estab = case when @Estab = -1 then bo.Estab  else @Estab end
				and 1 = case 
							when @produto='' then 1 
							else case 
									when st.ref in (select ref from #dadosSt) then 1 
									else 0 
									end
							end
				AND bo.fechada	= 0
				AND bi.fechada  = 0
				and bi.qtt>bi.qtt2
				--AND st.site_nr = @armazem
				and st.site_nr in (select distinct no from empresa (nolock) where ncont=@nifemp)
				--AND bo.site = @site
				and empresa.ncont=@nifemp
				AND bo.dataobra BETWEEN (case when @no=-1 then @dataIni else '19000101' end) AND @dataFim
				AND ([bi].[ref] LIKE @ProdRef + '%' OR [bi].[design] LIKE @produto +'%')
				AND [bo].[obrano] = CASE WHEN @venda = -1 THEN [bo].[obrano] ELSE @venda END
				AND ([bo].[nome] LIKE RTRIM(@cliente) +'%' OR [B_utentes].[nrcartao] = RTRIM(@nrCartao) + '%')
				AND [bo].[no] = CASE WHEN @no < 0 THEN [bo].[no] ELSE @no END
				AND [bo].[estab] = CASE WHEN @estab < 0 THEN [bo].[estab] ELSE @estab END
				AND [bo2].[u_nratend] = CASE WHEN @nrAtend = '' THEN [bo2].[u_nratend] ELSE @nrAtend END
			    AND bi.ref not in (select ref from #temp_refIgnora)
				GROUP BY 
				bo.bostamp, 
				bi.bistamp, 
				bo.dataobra, 
				bo.ousrhora, 
				bo.nmdos, 
				bo.obrano, 
				bo.nome, 
				bo.no, 
				bo.estab, 
				bi.ref, 
				bi.design, 
				bi.desconto, 
				bi.desc2, 
				bi.qtt, 
				bi.qtt2, 
				bi.ettdeb, 
				bi.edebito, 
				bo.ndos, 
				bi.tabiva, 
				bi.iva, 
				b_utentes.tlmvl, 
				b_utentes.utstamp, 
				bi.exepcaoTrocaMed, 
				bi.qtt, 
				bi2.nrreceita, 
				st.stock, 
				sa.stock, 
				bi.lobs, 
				bi.diploma, 
				bi.descval,
				bi2.fistamp,
				bi.no,
				bi.estab,
				bi.ousrhora,
				st.epv1,
				bi.fechada,
				bi.nopat, bi.lote, bi2.dataValidade
			--order by 
			--	bo.obrano, row
			)
			select * from cte1
			ORDER BY
				fdata DESC, ousrhora DESC
		end 
	END
	
	If OBJECT_ID('tempdb.dbo.#dadosSt') IS NOT NULL
			drop table #dadosSt;

	If OBJECT_ID('tempdb.dbo.#temp_td') IS NOT NULL
		drop table #temp_td ;

	If OBJECT_ID('tempdb.dbo.#temp_refIgnora') IS NOT NULL
		drop table #temp_refIgnora ;

GO
Grant Execute On dbo.up_touch_regularizarVendas to Public
Grant control on dbo.up_touch_regularizarVendas to Public
GO


