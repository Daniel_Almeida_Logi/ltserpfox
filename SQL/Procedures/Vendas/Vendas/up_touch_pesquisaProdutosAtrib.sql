-- Pesquisa de Produtos por Atributos nas Vendas 
-- exec up_touch_pesquisaProdutosAtrib '','','','','','','','',1

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_pesquisaProdutosAtrib]') IS NOT NULL
	drop procedure dbo.up_touch_pesquisaProdutosAtrib
go

create procedure dbo.up_touch_pesquisaProdutosAtrib

@pc1 varchar(60),
@pc2 varchar(60),
@pu1 varchar(60),
@pu2 varchar(60),
@f1 varchar(60),
@f2 varchar(60),
@fa varchar(60),
@marca varchar(200),
@armazem numeric(18,0),
@site_nr tinyint

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

;with
	cte1(ref) as
	(
		select ref
		from B_stAtrib (nolock)
		where descr like @pc1+'%'
			AND ref in (select ref
							from B_stAtrib (nolock)
							where descr like @pc2+'%'
								AND ref in (select ref
												from B_stAtrib (nolock)
												where descr like @pu1+'%'
													AND ref in (select ref
																	from B_stAtrib (nolock) 
																	where descr like @pu2+'%'
																		AND ref in (select ref
																						from B_stAtrib (nolock)
																						where descr like @f1+'%'
																							AND ref in (select ref
																											from B_stAtrib (nolock)
																											where descr like @f2+'%'
																												And ref in (select ref
																																from B_stAtrib (nolock)
																																where descr like @fa+'%'
																																)
																											)
																						)
																	)
												)
							)
		group by ref
	)


Select
	top 500
	 'sel'			=	convert(bit,0)
	,'ref'			=	st.ref
	,'design'		=	ltrim(rtrim(st.design))
	,'stock'		=	st.stock
	,'epv1'		=	st.epv1
	,'validade'	=	convert(varchar,st.validade,102)
	,'faminome'	=	st.faminome
	,'comp'		=	isnull((case when (left(cptgrp.descri��o,3)!='N�o' and cptgrp.u_diploma1='' and cptgrp.u_diploma2='' and cptgrp.u_diploma3='') AND cptgrp.grupo!='ST'
									then 1
									else 0
							end)
						  ,0)
	,'generico'	=	isNull(fprod.generico,0)
	,'GRPHMG'	=	case when fprod.grphmgcode='GH0000' then fprod.grphmgcode else fprod.grphmgcode + ' - ' + fprod.grphmgdescr end
	,'psico'		=	isnull(fprod.psico,0)
	,'benzo'		=	isnull(fprod.benzo,0)
	,'ststamp'	=	st.ststamp
	,'ptoenc'		=	st.ptoenc
	,'stmax'		=	st.stmax
	,'marg1'		=	st.marg1
	,'local'		=	st.local
	,'u_local'		=	st.u_local
	,'u_local2'	=	st.u_local2
from
	st (nolock)
	left join fprod  (nolock) on st.ref=fprod.cnp
	left join cptgrp (nolock) on cptgrp.grupo=fprod.grupo
	inner join cte1 on cte1.ref=st.ref
where
	st.site_nr = @site_nr
	and st.inactivo=0
	AND st.usr1 like @marca+'%'
order by
	st.stock desc, st.design

GO
Grant Execute On dbo.up_touch_pesquisaProdutosAtrib to Public
Grant Control On dbo.up_touch_pesquisaProdutosAtrib to Public
Go