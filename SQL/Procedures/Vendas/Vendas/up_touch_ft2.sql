-- obtem todos os campos da FT2, de forma compativel � previsualiza��o do talao, tamb�m usada pelo aquivo digital
-- exec up_touch_ft2 'ADMA4785E53-C96D-4530-85D'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_ft2]') IS NOT NULL
	drop procedure dbo.up_touch_ft2
go

create procedure dbo.up_touch_ft2

@stamp varchar(30)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON


select 
	 
	ft2.ft2stamp
	,ft2.modop1
	,ft2.epaga1
	,ft2.modop2
	,ft2.epaga2
	,ft2.eacerto
	,ft2.etroco
	,ft2.evdinheiro
	,ft2.subproc
	,ft2.vdollocal
	,ft2.vdlocal
	,ft2.c2no
	,ft2.c2nome
	,ft2.c2estab
	,ft2.c2codpost
	,ft2.c2pais
	,ft2.horaentrega
	,ft2.obsdoc
	,(case when b_utentes.removido=0 then ft2.morada else '*********' end) as morada
	,(case when b_utentes.removido=0 then ft2.local else '*********' end) as local
	,(case when b_utentes.removido=0 then ft2.codpost else '*********' end) as codpost
	,(case when b_utentes.removido=0 then ft2.telefone else '*********' end) as telefone
	,(case when b_utentes.removido=0 then ft2.contacto else '*********' end) as contacto
	,(case when b_utentes.removido=0 then ft2.email else '*********' end) as email
	,ft2.ltstamp
	,ft2.ltstamp2
	,ft2.ousrinis
	,ft2.ousrdata
	,ft2.ousrhora
	,ft2.usrinis
	,ft2.usrdata
	,ft2.usrhora
	,ft2.reexgiva
	,ft2.motiseimp
	,ft2.formapag
	,ft2.assinatura
	,ft2.u_codigo2
	,ft2.u_abrev
	,ft2.u_abrev2
	,ft2.u_design2
	,ft2.u_acertovs
	,ft2.u_vdsusprg
	,ft2.u_entpresc
	,ft2.u_nopresc
	,ft2.u_codigo
	,ft2.u_receita
	,(case when b_utentes.removido=0 then ft2.u_nbenef else '*********' end) as u_nbenef
	,ft2.u_design
	,ft2.u_viatura
	,ft2.u_vddom
	,(case when b_utentes.removido=0 then ft2.u_nbenef2 else '*********' end) as u_nbenef2
	,ft2.u_docorig
	,ft2.exportado
	,ft2.epaga3
	,ft2.epaga4
	,ft2.epaga5
	,ft2.epaga6
	,ft2.codAcesso
	,ft2.codDirOpcao
	,ft2.token
	,'ft2_u_vddom'		=	u_vddom
	,'ft2_contacto'		=	(case when b_utentes.removido=0 then ft2.contacto else '*********' end)
	,'ft2_u_viatura'	=	u_viatura 
	,'ft2_morada'		=	(case when b_utentes.removido=0 then ft2.morada else '*********' end)
	,'ft2_codpost'		=	(case when b_utentes.removido=0 then ft2.codpost else '*********' end)
	,'ft2_local'		=	(case when b_utentes.removido=0 then ft2.local else '*********' end)
	,'ft2_telefone'		=	(case when b_utentes.removido=0 then ft2.telefone else '*********' end)
	,'ft2_email'		=	(case when b_utentes.removido=0 then ft2.email else '*********' end)
	,'ft2_horaentrega'	=	horaentrega
	,ft2.valcartao
	,ft2.valcartaoutilizado
	,ft2.cativa
	,ft2.valcativa
	,ft2.perccativa
	,ft2.impresso
	,ft2.u_hclstamp1
from 
	ft2 (nolock)
	inner join ft (nolock) on ft.ftstamp=ft2.ft2stamp
	inner join b_utentes (nolock) on b_utentes.no = ft.no and b_utentes.estab = ft.estab
where 
	ft2stamp = @stamp

GO
Grant Execute On dbo.up_touch_ft2 to Public
Grant Control On dbo.up_touch_ft2 to Public
GO