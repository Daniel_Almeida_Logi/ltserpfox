/* 

	EXEC up_vendas_geraNrAtend 64, 01, 'ADM'


*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_vendas_geraNrAtend]') IS NOT NULL
    DROP procedure dbo.up_vendas_geraNrAtend
GO

CREATE procedure dbo.up_vendas_geraNrAtend
	@terminal NUMERIC(5),
	@noSite NUMERIC(4),
	@usrInis VARCHAR(3)

/*

AS

	DECLARE @ano NUMERIC(2)
	DECLARE @dia NUMERIC(3)
	DECLARE @segundos VARCHAR(20)
	DECLARE @randomChar VARCHAR(1)
	DECLARE @nrAtend VARCHAR(14)
	DECLARE @newId VARCHAR(100)

	SET @ano = RIGHT(YEAR(GETDATE()),2)
	SET @dia = DATENAME(dayofyear, GETDATE())
	SET @segundos = LTRIM(DATEDIFF(SECOND, '1/1/1900', CONVERT(DATETIME, CAST(CURRENT_TIMESTAMP as TIME(3))))) --+ LTRIM(RIGHT(CAST(CURRENT_TIMESTAMP as TIME(3)),3))
	SET @randomChar = ''

	SET @nrAtend = LTRIM(@ano) + LTRIM(@dia) + LTRIM(@segundos) --+ LTRIM(@terminal)

	
	WHILE LEN(@nrAtend) < 14
	BEGIN

		SET @newId = REPLACE(LEFT(NEWID(),25) + RIGHT(NEWID(),25) + RIGHT(NEWID(),25) + LEFT(NEWID(),25), '-','')

		SET @newId = (CASE WHEN (CAST( FLOOR(RAND()*100) AS INT) % 2) = 0 THEN REVERSE(@newId) ELSE RIGHT(@newId, 50) + LEFT(@newId, 50) END)

		SET @randomChar = ''
		
		SET @randomChar = SUBSTRING(@newID, CAST(FLOOR(LEN(@newID)*RAND()) AS INT), 1)

		SET @nrAtend = @nrAtend + @randomChar

	END
	
	/*
	SET @nrAtend = @nrAtend + RIGHT(NEWID(), 10)

	SET @nrAtend = LEFT(@nrAtend, 14)
	*/

	SELECT @nrAtend as newNrAtend
*/	

AS

	DECLARE @noFarm VARCHAR(2) = RIGHT('00' + LTRIM(@noSite),2)

	PRINT @noFarm

	DECLARE @id numeric(11)
	DECLARE @random_number INT
	DECLARE @result VARCHAR(14) = ''
	DECLARE @count INT = 0
	DECLARE @nrAtend VARCHAR(14)
	DECLARE @nrAtendFinal VARCHAR(14) = ''
	DECLARE @i INT = 1
	DECLARE @curChar VARCHAR(1) = ''

	SET @random_number = CAST(RAND() * 9 AS INT) + 1
    SET @id = CAST(CAST(NEWID() AS VARBINARY) AS INT)
    SET @result = LEFT(@result + CONVERT(VARCHAR(1), @random_number) + CONVERT(VARCHAR(11), @id),12) 

	WHILE LEN(@result) < 12
	BEGIN 

		SET @random_number = CAST(RAND() * 9 AS INT) + 1

		SET @result = LTRIM(@result) + LTRIM(@random_number)

	END

	SET @nrAtend = LTRIM(@result) + LTRIM(@noFarm)

	WHILE @i <= LEN(@nrAtend)
	BEGIN

		SET @curChar = SUBSTRING(@nrAtend, @i, 1)

		IF @curChar = '-'
		BEGIN

			SET @random_number = CAST(RAND() * 9 AS INT) + 1

			SET @nrAtendFinal = @nrAtendFinal + LTRIM(@random_number)

		END
		ELSE
		BEGIN

			SET	@nrAtendFinal = @nrAtendFinal + @curChar

		END
	
	
		SET @i += 1

	END

	SELECT @nrAtendFinal as newNrAtend

GO
GRANT EXECUTE on dbo.up_vendas_geraNrAtend TO PUBLIC
GRANT Control on dbo.up_vendas_geraNrAtend TO PUBLIC
GO