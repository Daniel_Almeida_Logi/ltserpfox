-- Ver 5 mais Baratos (FUN��ES DA VENDA)
-- exec up_touch_5maisBaratos '5294251',1
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

If OBJECT_ID('[dbo].[up_touch_5maisBaratos]') IS NOT NULL
	drop procedure dbo.up_touch_5maisBaratos
Go

create procedure dbo.up_touch_5maisBaratos
	@ref		varchar (18)
	,@armazem	numeric(5,0)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

;with
	cte1(grpmhgcode, epreco) as (
		select
			grphmgcode,
			epreco = case when pvpcalc=0 then pvporig else pvpcalc end
		from 
			fprod (nolock)
		where
			fprod.cnp = @ref
	),
	
	cte2 (ref, design, epreco, stock, bb, t45) as (
		select
			 ref		= fprod.cnp
			,design		= isnull(st.design,fprod.design)
			,epreco		= case when pvpcalc=0 then pvporig else pvpcalc end
			,stock		= ISNULL(sa.stock,0)
			,bb			= ISNULL(marg4,0)
			,t45 = case when fprod.pvporig <= fprod.preco_acordo and fprod.preco_acordo > 0 then "T4"
								when ranking=1 and grphmgcode != 'GH0000' and grphmgcode != '' and fprod.pvporig <= fprod.pvpmaxre then "T5" 
								else ""
							end
		from
			fprod (nolock)
			left join	st (nolock)	on st.ref = fprod.cnp
			inner join	cte1		on fprod.grphmgcode=cte1.grpmhgcode
			left join	sa (nolock)	on sa.ref = st.ref and sa.armazem = @armazem
		where
			ranking=1
			and grphmgcode != 'GH0000' and grphmgcode != ''
			and fprod.cnp!=@ref
	)

select 
	* 
from (		
	select
		sel = convert(bit,0)
		,oref = @ref /* guarda referencia original para usar no cursor */
		,ref
		,design
		,epreco
		,stock
		,barato = dense_rank() over (order by epreco)
		,bb
		,t45
	from
		cte2
) x
where
	barato between 1 and 5
order by
	epreco

Option (recompile)		

GO
Grant Execute On dbo.up_touch_5maisBaratos to Public
Grant Control On dbo.up_touch_5maisBaratos to Public
GO