-- Mostrar DCIs do Produto (STOUCHPOS)
-- exec up_touch_DCIs '5324256'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_DCIs]') IS NOT NULL
	drop procedure dbo.up_touch_DCIs
go

create procedure dbo.up_touch_DCIs
	@ref varchar (18)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	select
		descricao = dci
	from
		fprod (nolock)
	where
		fprod.cnp=@ref

	option (optimize for (@ref unknown))

GO
Grant Execute On dbo.up_touch_DCIs to Public
Grant Control On dbo.up_touch_DCIs to Public
Go