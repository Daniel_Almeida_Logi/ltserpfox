/*
	
	 exec up_print_rodape_reserva 'Loja 1'
	  
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_print_rodape_reserva]') IS NOT NULL
	drop procedure dbo.up_print_rodape_reserva
go

create procedure dbo.up_print_rodape_reserva

	@site varchar(30)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select
(select textValue from B_Parameters_site where stamp='ADM0000000083' and site=@site) as rodapetxt1



GO
Grant Execute On dbo.up_print_rodape_reserva to Public
Grant Control On dbo.up_print_rodape_reserva to Public
GO