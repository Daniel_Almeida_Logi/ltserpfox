-- Ver PVP do Produto na Venda (STOUCHPOS)
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_verPvp]') IS NOT NULL
	drop procedure dbo.up_touch_verPvp
go

create procedure dbo.up_touch_verPvp
	@ref varchar (18)
	,@site_nr tinyint

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

SELECT
	ref
	,design
	,epv1
	--,pv1 
FROM
	st (nolock) 
WHERE
	st.ref=@ref
	and st.site_nr = @site_nr


GO
Grant Execute On dbo.up_touch_verPvp to Public
Grant Control On dbo.up_touch_verPvp to Public
Go