/*

	obtem campos da FT
	
	 exec up_print_qrcode_a4 'ADM09D44A6B-9601-43D9-B64'

	select * from fi
	select * from dplms
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_print_qrcode_a4]') IS NOT NULL
	drop procedure dbo.up_print_qrcode_a4
go

create procedure dbo.up_print_qrcode_a4

	@stamp varchar(30)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

If OBJECT_ID('tempdb.dbo.#iva') IS NOT NULL
		DROP TABLE #iva

declare @IVAISE numeric(20,2) 
		,@IVARED numeric(20,2)
		,@IVAINT numeric(20,2)
		,@IVANOR numeric(20,2)
		,@TIPOSAFT VARCHAR(2)

SET @TIPOSAFT = (SELECT tiposaft FROM td(nolock) WHERE ndoc = (SELECT ndoc FROM ft(nolock) WHERE ftstamp = @stamp))

select (case when (select codigo from regiva where tabiva=1)='ISE' then eivain1 else 0 end)
											+(case when (select codigo from regiva where tabiva=2)='ISE' then eivain2 else 0 end)
											+(case when (select codigo from regiva where tabiva=3)='ISE' then eivain3 else 0 end)
											+(case when (select codigo from regiva where tabiva=4)='ISE' then eivain4 else 0 end)
											+(case when (select codigo from regiva where tabiva=5)='ISE' then eivain5 else 0 end)
											+(case when (select codigo from regiva where tabiva=6)='ISE' then eivain6 else 0 end)
											+(case when (select codigo from regiva where tabiva=7)='ISE' then eivain7 else 0 end)
											+(case when (select codigo from regiva where tabiva=8)='ISE' then eivain8 else 0 end)
											+(case when (select codigo from regiva where tabiva=9)='ISE' then eivain9 else 0 end)
											+(case when (select codigo from regiva where tabiva=10)='ISE' then eivain10 else 0 end)
											+(case when (select codigo from regiva where tabiva=11)='ISE' then eivain11 else 0 end)
											+(case when (select codigo from regiva where tabiva=12)='ISE' then eivain12 else 0 end)
											+(case when (select codigo from regiva where tabiva=13)='ISE' then eivain13 else 0 end) as valorise
	,(case when (select codigo from regiva where tabiva=1)='RED' then eivain1 else 0 end)
											+(case when (select codigo from regiva where tabiva=2)='RED' then eivain2 else 0 end)
											+(case when (select codigo from regiva where tabiva=3)='RED' then eivain3 else 0 end)
											+(case when (select codigo from regiva where tabiva=4)='RED' then eivain4 else 0 end)
											+(case when (select codigo from regiva where tabiva=5)='RED' then eivain5 else 0 end)
											+(case when (select codigo from regiva where tabiva=6)='RED' then eivain6 else 0 end)
											+(case when (select codigo from regiva where tabiva=7)='RED' then eivain7 else 0 end)
											+(case when (select codigo from regiva where tabiva=8)='RED' then eivain8 else 0 end)
											+(case when (select codigo from regiva where tabiva=9)='RED' then eivain9 else 0 end)
											+(case when (select codigo from regiva where tabiva=10)='RED' then eivain10 else 0 end)
											+(case when (select codigo from regiva where tabiva=11)='RED' then eivain11 else 0 end)
											+(case when (select codigo from regiva where tabiva=12)='RED' then eivain12 else 0 end)
											+(case when (select codigo from regiva where tabiva=13)='RED' then eivain13 else 0 end) as valorred
	,(case when (select codigo from regiva where tabiva=1)='INT' then eivain1 else 0 end)
											+(case when (select codigo from regiva where tabiva=2)='INT' then eivain2 else 0 end)
											+(case when (select codigo from regiva where tabiva=3)='INT' then eivain3 else 0 end)
											+(case when (select codigo from regiva where tabiva=4)='INT' then eivain4 else 0 end)
											+(case when (select codigo from regiva where tabiva=5)='INT' then eivain5 else 0 end)
											+(case when (select codigo from regiva where tabiva=6)='INT' then eivain6 else 0 end)
											+(case when (select codigo from regiva where tabiva=7)='INT' then eivain7 else 0 end)
											+(case when (select codigo from regiva where tabiva=8)='INT' then eivain8 else 0 end)
											+(case when (select codigo from regiva where tabiva=9)='INT' then eivain9 else 0 end)
											+(case when (select codigo from regiva where tabiva=10)='INT' then eivain10 else 0 end)
											+(case when (select codigo from regiva where tabiva=11)='INT' then eivain11 else 0 end)
											+(case when (select codigo from regiva where tabiva=12)='INT' then eivain12 else 0 end)
											+(case when (select codigo from regiva where tabiva=13)='INT' then eivain13 else 0 end) as valorint
	,(case when (select codigo from regiva where tabiva=1)='NOR' then eivain1 else 0 end)
											+(case when (select codigo from regiva where tabiva=2)='NOR' then eivain2 else 0 end)
											+(case when (select codigo from regiva where tabiva=3)='NOR' then eivain3 else 0 end)
											+(case when (select codigo from regiva where tabiva=4)='NOR' then eivain4 else 0 end)
											+(case when (select codigo from regiva where tabiva=5)='NOR' then eivain5 else 0 end)
											+(case when (select codigo from regiva where tabiva=6)='NOR' then eivain6 else 0 end)
											+(case when (select codigo from regiva where tabiva=7)='NOR' then eivain7 else 0 end)
											+(case when (select codigo from regiva where tabiva=8)='NOR' then eivain8 else 0 end)
											+(case when (select codigo from regiva where tabiva=9)='NOR' then eivain9 else 0 end)
											+(case when (select codigo from regiva where tabiva=10)='NOR' then eivain10 else 0 end)
											+(case when (select codigo from regiva where tabiva=11)='NOR' then eivain11 else 0 end)
											+(case when (select codigo from regiva where tabiva=12)='NOR' then eivain12 else 0 end)
											+(case when (select codigo from regiva where tabiva=13)='NOR' then eivain13 else 0 end) as valornor
into #iva 
from ft (nolock) where 
ftstamp = @stamp
--or u_nratend= case when @nratend='' then (select u_nratend from ft ftt (nolock) where ftt.ftstamp=@stamp) else @nratend end
select @IVAISE = (select SUM(valorise) from #iva)
	, @IVAINT = (select SUM(valorint) from #iva)
	, @IVANOR = (select SUM(valornor) from #iva)
	, @IVARED = (select SUM(valorred) from #iva)
	
declare @sql varchar(max)
select @sql = N'

	DECLARE @pais	VARCHAR(36)

	SELECT top 1 @pais = pais from empresa

	select
		(''A:''+ltrim(rtrim(empresa.ncont))
		+''*B:''+ltrim(rtrim(ft.ncont))
		+''*C:''+(case when b_utentes.codigop='''' then ''PT'' else ltrim(rtrim(b_utentes.codigop)) end)
		+''*D:''+td.tiposaft
		+''*E:''+(case when ft.anulado = 0 then ''N'' else ''A'' end)
		+''*F:''+convert(varchar, ft.fdata, 112)
		+''*G:''+left(isnull((select invoiceType from b_cert bce where ft.ftstamp=bce.stamp),''FT'') + '' '' + convert(varchar,ft.ndoc) + ''/'' + CONVERT(varchar,ft.fno),60)
		+''*H:''+(CASE WHEN td.ATCUD <> '''' THEN LTRIM(RTRIM(td.atcud)) ELSE ''0'' END) + ''-'' + LTRIM(RTRIM(ft.fno))
		+''*I1:''+(select top 1 descricao from regiva (nolock))'
if @IVAISE <>0 AND @TIPOSAFT NOT IN ('GT')
select @sql = @sql + N'
		+''*I2:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''ISE'' then eivain1 else 0 end)
											+(case when (select codigo from regiva where tabiva=2)=''ISE'' then eivain2 else 0 end)
											+(case when (select codigo from regiva where tabiva=3)=''ISE'' then eivain3 else 0 end)
											+(case when (select codigo from regiva where tabiva=4)=''ISE'' then eivain4 else 0 end)
											+(case when (select codigo from regiva where tabiva=5)=''ISE'' then eivain5 else 0 end)
											+(case when (select codigo from regiva where tabiva=6)=''ISE'' then eivain6 else 0 end)
											+(case when (select codigo from regiva where tabiva=7)=''ISE'' then eivain7 else 0 end)
											+(case when (select codigo from regiva where tabiva=8)=''ISE'' then eivain8 else 0 end)
											+(case when (select codigo from regiva where tabiva=9)=''ISE'' then eivain9 else 0 end)
											+(case when (select codigo from regiva where tabiva=10)=''ISE'' then eivain10 else 0 end)
											+(case when (select codigo from regiva where tabiva=11)=''ISE'' then eivain11 else 0 end)
											+(case when (select codigo from regiva where tabiva=12)=''ISE'' then eivain12 else 0 end)
											+(case when (select codigo from regiva where tabiva=13)=''ISE'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))'
if @IVARED <>0 AND @TIPOSAFT NOT IN ('GT')
select @sql = @sql + N'
		+''*I3:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''RED'' then eivain1 else 0 end)
											+(case when (select codigo from regiva where tabiva=2)=''RED'' then eivain2 else 0 end)
											+(case when (select codigo from regiva where tabiva=3)=''RED'' then eivain3 else 0 end)
											+(case when (select codigo from regiva where tabiva=4)=''RED'' then eivain4 else 0 end)
											+(case when (select codigo from regiva where tabiva=5)=''RED'' then eivain5 else 0 end)
											+(case when (select codigo from regiva where tabiva=6)=''RED'' then eivain6 else 0 end)
											+(case when (select codigo from regiva where tabiva=7)=''RED'' then eivain7 else 0 end)
											+(case when (select codigo from regiva where tabiva=8)=''RED'' then eivain8 else 0 end)
											+(case when (select codigo from regiva where tabiva=9)=''RED'' then eivain9 else 0 end)
											+(case when (select codigo from regiva where tabiva=10)=''RED'' then eivain10 else 0 end)
											+(case when (select codigo from regiva where tabiva=11)=''RED'' then eivain11 else 0 end)
											+(case when (select codigo from regiva where tabiva=12)=''RED'' then eivain12 else 0 end)
											+(case when (select codigo from regiva where tabiva=13)=''RED'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
		+''*I4:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''RED'' then eivav1 else 0 end)
											+(case when (select codigo from regiva where tabiva=2)=''RED'' then eivav2 else 0 end)
											+(case when (select codigo from regiva where tabiva=3)=''RED'' then eivav3 else 0 end)
											+(case when (select codigo from regiva where tabiva=4)=''RED'' then eivav4 else 0 end)
											+(case when (select codigo from regiva where tabiva=5)=''RED'' then eivav5 else 0 end)
											+(case when (select codigo from regiva where tabiva=6)=''RED'' then eivav6 else 0 end)
											+(case when (select codigo from regiva where tabiva=7)=''RED'' then eivav7 else 0 end)
											+(case when (select codigo from regiva where tabiva=8)=''RED'' then eivav8 else 0 end)
											+(case when (select codigo from regiva where tabiva=9)=''RED'' then eivav9 else 0 end)
											+(case when (select codigo from regiva where tabiva=10)=''RED'' then eivav10 else 0 end)
											+(case when (select codigo from regiva where tabiva=11)=''RED'' then eivav11 else 0 end)
											+(case when (select codigo from regiva where tabiva=12)=''RED'' then eivav12 else 0 end)
											+(case when (select codigo from regiva where tabiva=13)=''RED'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))'
if @IVAINT <>0 AND @TIPOSAFT NOT IN ('GT')
select @sql = @sql + N'
		+''*I5:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''INT'' then eivain1 else 0 end)
											+(case when (select codigo from regiva where tabiva=2)=''INT'' then eivain2 else 0 end)
											+(case when (select codigo from regiva where tabiva=3)=''INT'' then eivain3 else 0 end)
											+(case when (select codigo from regiva where tabiva=4)=''INT'' then eivain4 else 0 end)
											+(case when (select codigo from regiva where tabiva=5)=''INT'' then eivain5 else 0 end)
											+(case when (select codigo from regiva where tabiva=6)=''INT'' then eivain6 else 0 end)
											+(case when (select codigo from regiva where tabiva=7)=''INT'' then eivain7 else 0 end)
											+(case when (select codigo from regiva where tabiva=8)=''INT'' then eivain8 else 0 end)
											+(case when (select codigo from regiva where tabiva=9)=''INT'' then eivain9 else 0 end)
											+(case when (select codigo from regiva where tabiva=10)=''INT'' then eivain10 else 0 end)
											+(case when (select codigo from regiva where tabiva=11)=''INT'' then eivain11 else 0 end)
											+(case when (select codigo from regiva where tabiva=12)=''INT'' then eivain12 else 0 end)
											+(case when (select codigo from regiva where tabiva=13)=''INT'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
		+''*I6:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''INT'' then eivav1 else 0 end)
											+(case when (select codigo from regiva where tabiva=2)=''INT'' then eivav2 else 0 end)
											+(case when (select codigo from regiva where tabiva=3)=''INT'' then eivav3 else 0 end)
											+(case when (select codigo from regiva where tabiva=4)=''INT'' then eivav4 else 0 end)
											+(case when (select codigo from regiva where tabiva=5)=''INT'' then eivav5 else 0 end)
											+(case when (select codigo from regiva where tabiva=6)=''INT'' then eivav6 else 0 end)
											+(case when (select codigo from regiva where tabiva=7)=''INT'' then eivav7 else 0 end)
											+(case when (select codigo from regiva where tabiva=8)=''INT'' then eivav8 else 0 end)
											+(case when (select codigo from regiva where tabiva=9)=''INT'' then eivav9 else 0 end)
											+(case when (select codigo from regiva where tabiva=10)=''INT'' then eivav10 else 0 end)
											+(case when (select codigo from regiva where tabiva=11)=''INT'' then eivav11 else 0 end)
											+(case when (select codigo from regiva where tabiva=12)=''INT'' then eivav12 else 0 end)
											+(case when (select codigo from regiva where tabiva=13)=''INT'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))'
if @IVANOR <>0 AND @TIPOSAFT NOT IN ('GT')
select @sql = @sql + N'
		+''*I7:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''NOR'' then eivain1 else 0 end)
											+(case when (select codigo from regiva where tabiva=2)=''NOR'' then eivain2 else 0 end)
											+(case when (select codigo from regiva where tabiva=3)=''NOR'' then eivain3 else 0 end)
											+(case when (select codigo from regiva where tabiva=4)=''NOR'' then eivain4 else 0 end)
											+(case when (select codigo from regiva where tabiva=5)=''NOR'' then eivain5 else 0 end)
											+(case when (select codigo from regiva where tabiva=6)=''NOR'' then eivain6 else 0 end)
											+(case when (select codigo from regiva where tabiva=7)=''NOR'' then eivain7 else 0 end)
											+(case when (select codigo from regiva where tabiva=8)=''NOR'' then eivain8 else 0 end)
											+(case when (select codigo from regiva where tabiva=9)=''NOR'' then eivain9 else 0 end)
											+(case when (select codigo from regiva where tabiva=10)=''NOR'' then eivain10 else 0 end)
											+(case when (select codigo from regiva where tabiva=11)=''NOR'' then eivain11 else 0 end)
											+(case when (select codigo from regiva where tabiva=12)=''NOR'' then eivain12 else 0 end)
											+(case when (select codigo from regiva where tabiva=13)=''NOR'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
		+''*I8:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''NOR'' then eivav1 else 0 end)
											+(case when (select codigo from regiva where tabiva=2)=''NOR'' then eivav2 else 0 end)
											+(case when (select codigo from regiva where tabiva=3)=''NOR'' then eivav3 else 0 end)
											+(case when (select codigo from regiva where tabiva=4)=''NOR'' then eivav4 else 0 end)
											+(case when (select codigo from regiva where tabiva=5)=''NOR'' then eivav5 else 0 end)
											+(case when (select codigo from regiva where tabiva=6)=''NOR'' then eivav6 else 0 end)
											+(case when (select codigo from regiva where tabiva=7)=''NOR'' then eivav7 else 0 end)
											+(case when (select codigo from regiva where tabiva=8)=''NOR'' then eivav8 else 0 end)
											+(case when (select codigo from regiva where tabiva=9)=''NOR'' then eivav9 else 0 end)
											+(case when (select codigo from regiva where tabiva=10)=''NOR'' then eivav10 else 0 end)
											+(case when (select codigo from regiva where tabiva=11)=''NOR'' then eivav11 else 0 end)
											+(case when (select codigo from regiva where tabiva=12)=''NOR'' then eivav12 else 0 end)
											+(case when (select codigo from regiva where tabiva=13)=''NOR'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))'
select @sql = @sql + N'	
		+''*N:''+ cast(cast(abs(ft.ettiva) as numeric(15,2)) as varchar(30))
		+''*O:''+ cast(cast(abs(ft.etotal) as numeric(15,2)) as varchar(30))
		+''*Q:''+ (SUBSTRING (hash,1,1) + SUBSTRING (hash,11,1) + SUBSTRING (hash,21,1) + SUBSTRING (hash,31,1))
		+''*R:''+ (select top 1 left(textValue,charindex(''/'',textValue)-1) from B_Parameters (nolock) where stamp=''ADM0000000078'')) as qrcode
		
	from 
		fi (nolock)
		inner join ft (nolock) on fi.ftstamp=ft.ftstamp
		--inner join ft2 (nolock) on ft2.ft2stamp=ft.ftstamp
		inner join td (nolock) on td.ndoc = ft.ndoc 
		inner join b_cert (nolock) on b_cert.stamp=ft.ftstamp
		left join fprod (nolock) on fprod.cnp = fi.ref
		inner join empresa (nolock) on empresa.site=ft.site
		inner join b_utentes (nolock) on b_utentes.no = ft.no and b_utentes.estab = ft.estab
	where 
		ft.ftstamp = '''+@stamp+''''

--exec up_print_qrcode_a4 'ADM042C976C-40A4-43E6-A0B','21189151904FFX',''

print @sql
execute (@sql)

GO
Grant Execute On dbo.up_print_qrcode_a4 to Public
Grant Control On dbo.up_print_qrcode_a4 to Public
GO
