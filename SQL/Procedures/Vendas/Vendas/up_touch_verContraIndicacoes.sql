-- ver Contra-Indicações do Produto com determinadas Patologias (VendasS)
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_verContraIndicacoes]') IS NOT NULL
	drop procedure dbo.up_touch_verContraIndicacoes
go

create procedure dbo.up_touch_verContraIndicacoes
	@ref		varchar (18),
	@clStamp	varchar(25)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

;WITH ctePatologias(ctiefpstamp,descricao,patostamp) AS
	(
		select 
			B_patologias.ctiefpstamp
			,descricao,patostamp
		from
			b_patologias (nolock)
			inner join b_ctidef (nolock) on B_patologias.ctiefpstamp = B_ctidef.ctiefpstamp
		where 
			ostamp = @clStamp
	)

SELECT 
	 ref = @ref
	,moleculasx.descricao
	,patol	= ctidefx.descricao
	,nvl	= ctinvlx.descricao
	,frase	= ctiexpx.descricao
FROM 
	B_ctifp ctifpx (nolock) 
	INNER JOIN B_ctiexp AS ctiexpx (nolock)ON ctifpx.ctiexpstamp = ctiexpx.ctiexpstamp 
	INNER JOIN B_ctidef AS ctidefx ON ctifpx.ctidefstamp = ctidefx.ctiefpstamp 
	INNER JOIN B_ctinvl AS ctinvlx ON ctifpx.ctinvlstamp = ctinvlx.ctinvlstamp 
	INNER JOIN B_moleculas AS moleculasx ON ctifpx.moleculaID = moleculasx.moleculaID 
	INNER JOIN B_moleculasfp AS moleculasfpx ON ctifpx.moleculaID = moleculasfpx.moleculaID
where 
	cnp = @ref
	AND ctiefpstamp in (SELECT ctiefpstamp FROM ctePatologias)

option (recompile)

GO
Grant Execute On dbo.up_touch_verContraIndicacoes to Public
Grant Control On dbo.up_touch_verContraIndicacoes to Public
Go