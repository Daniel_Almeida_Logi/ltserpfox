
/*	
	

	exec up_vendas_getDadosBonusBAS 'ADM82216E7C-468B-4D93-B5F'

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_vendas_getDadosBonusBAS]') IS NOT NULL
	DROP PROCEDURE dbo.up_vendas_getDadosBonusBAS
GO

CREATE PROCEDURE dbo.up_vendas_getDadosBonusBAS
	@stamp AS VARCHAR(30)

AS

	IF EXISTS (SELECT 1 FROM fi(nolock) WHERE fistamp = @stamp)
	BEGIN

		IF EXISTS (SELECT 1 FROM bi2(NOLOCK) WHERE bi2.fistamp = @stamp)
		BEGIN

			SELECT 
				fi2.bonusTipo,
				fi2.bonusId,
				fi2.bonusDescr
			FROM
				fi2(nolock)
				JOIN fi(nolock) ON fi.fistamp = fi2.fistamp
				JOIN bi2(nolock) ON fi.lrecno = bi2.fistamp_insercao
			WHERE	
				bi2.fistamp = @stamp

		END
		ELSE
		BEGIN

			SELECT 
				bonusTipo,
				bonusId,
				bonusDescr
			FROM
				fi2(nolock)
			WHERE
				fistamp = @stamp

		END

	END
	ELSE
	BEGIN

		SELECT 
			fi2.bonusTipo,
			fi2.bonusId,
			fi2.bonusDescr
		FROM
			fi2(nolock)
			JOIN fi(nolock) ON fi.fistamp = fi2.fistamp
			JOIN bi2(nolock) ON fi.lrecno = bi2.fistamp_insercao
		WHERE	
			bi2.bi2stamp = @stamp
		
	END

GO
Grant Execute on dbo.up_vendas_getDadosBonusBAS to Public
Grant control on dbo.up_vendas_getDadosBonusBAS to Public
Go