/* 
	Produtos Vendidos ao Cliente  (STOUCHPOS)

	 exec up_touch_produtosVendidosCliente 1000083, 0
	 
	 select * from ft inner join td (nolock) on td.ndoc = ft.ndoc where ncont='108943658' AND td.tipodoc != 3 and td.u_tipodoc != 3
	select * from b_utentes where no=30017 and estab=0
	 exec up_touch_produtosVendidosCliente 200 , 0, '20180101', '20191231'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_produtosVendidosCliente]') IS NOT NULL
	drop procedure dbo.up_touch_produtosVendidosCliente
go

create procedure dbo.up_touch_produtosVendidosCliente
	 @nr numeric(10,0)
	,@dep numeric(3,0)
	,@dataIni datetime
	,@dataFim datetime

/* WITH ENCRYPTION */
AS

	SET NOCOUNT ON

	If OBJECT_ID('tempdb.dbo.#dadosFt') IS NOT NULL
		drop table #dadosFt;
	If OBJECT_ID('tempdb.dbo.#dadosReg') IS NOT NULL
		drop table #dadosReg;
	If OBJECT_ID('tempdb.dbo.#dadosFprod') IS NOT NULL
		drop table #dadosFprod;
	
	/* get client stamp */
	Declare @stamp as varchar(25)
	set @stamp = isnull((Select utstamp from b_utentes (nolock) where b_utentes.no = @nr and b_utentes.estab = @dep),'')

	/* get client NIF */
	Declare @ncont as varchar(25)
	set @ncont = isnull((Select ncont from b_utentes (nolock) where b_utentes.no = @nr and b_utentes.estab = @dep),'')
		
	select
		 'data'			=	CONVERT(varchar,fdata,102)
		,'hora'			=	ft.ousrhora
		,'documento'	=	case when ft.cobrado=0 then ft.nmdoc else ft.nmdoc + ' (S)' end
		,'nr'			=	ft.fno
		,'vendedor'		=	ft.vendnm + ' ' + CONVERT(varchar,ft.vendedor) + ''
		,'ref'			=	fi.ref
		,'design'		=	fi.design
		,'epv'			=	epv
		,'u_epvp'		=	u_epvp
		,'u_epref'		=	u_epref
		,'u_ettent1'	=	u_ettent1
		,'u_ettent2'	=	u_ettent2
		,'iva'			=	fi.iva
		,'desconto'		=	fi.desconto
		,'ftstamp'		=	ft.ftstamp
		,'tipodoc'		=	ft.tipodoc
		,'ndoc'			=	ft.ndoc
		,'cobrado'		=	ft.cobrado
		,'pnome'		=	ft.pnome
		,'u_vdsusprg'	=	ft2.u_vdsusprg
		,'u_ltstamp'	=	ft.u_ltstamp
		,'u_ltstamp2'	=	ft.u_ltstamp2
		,'u_codigo'		=	ft2.u_codigo
		,ft.ncont
		,ft.no
		,ft.estab
		,fistamp
		,fi.qtt
		,comp = isnull((case 
							--when (left(cptgrp.descricao,3) = 'Não' and cptgrp.u_diploma1='' and cptgrp.u_diploma2='' and cptgrp.u_diploma3='') OR fprod.grupo = 'ST'
							--then 0
							when left(fprod.cptgrpdescr,3) != 'Não' AND fprod.grupo != 'ST'
						then 1
						else 0
						end
				 ),0)
	into
		#dadosFt
	from
		fi (nolock)
		inner join ft  (nolock) on ft.ftstamp = fi.ftstamp and ft.fdata between @dataIni and @dataFim
		inner join ft2 (nolock) on ft2.ft2stamp = ft.ftstamp
		inner join td (nolock) on td.ndoc = ft.ndoc
		left join fprod (nolock) on fprod.cnp = fi.ref
	where
		fi.ref != ''
		--and (
		--	(ft.no = @nr and ft.estab = @dep and ft.u_hclstamp = '') 
		--	or ft.u_hclstamp = @stamp
		--)
		and (
			((ft.ncont = @ncont or (ft.no = @nr and ft.estab = @dep)) and ft.u_hclstamp = '') 
			or ft.u_hclstamp = @stamp
		)	
		AND td.tipodoc != 3
		and td.u_tipodoc != 3
		and (ft.fdata between @dataIni and @dataFim)
	
	/* Elimina Vendas tendo em conta tipo de cliente*/
	If @ncont = ''
	delete from #dadosFt where cast(#dadosFt.no as varchar(15))+cast(#dadosFt.estab as varchar(3))<>cast(@nr as varchar(15))+cast(@dep as varchar(3))

	--delete from #dadosFt where #dadosFt.no <> @nr and #dadosFt.estab<>@dep
	
	--exec up_touch_produtosVendidosCliente 1000083 , 0, '20180106', '20180705'

	select
		 qtt = isnull(Sum(fi.qtt),0)
		,ofistamp
	into
		#dadosReg	
	from 
		fi (nolock)
		inner join #dadosFt vd on fi.ofistamp = vd.fistamp
	group by
		ofistamp


		
	insert into #dadosFt
	select 
		'data'			=	CONVERT(varchar,hc.data,102)
		,'hora'			=	hc.ousrhora
		,'documento'	=	'Hist. Tratamento'
		,'nr'			=	0
		,'vendedor'		=	hc.ousrinis
		,'ref'			=	hc.ref
		,'design'		=	hc.design
		,'epv'			=	0
		,'u_epvp'		=	0
		,'u_epref'		=	0
		,'u_ettent1'	=	0
		,'u_ettent2'	=	0
		,'iva'			=	0
		,'desconto'		=	0
		,'ftstamp'		=	''
		,'tipodoc'		=	0
		,'ndoc'			=	0
		,'cobrado'		=	0
		,'pnome'		=	''
		,'u_vdsusprg'	=	0
		,'u_ltstamp'	=	''
		,'u_ltstamp2'	=	''
		,'u_codigo'		=	''
		,ncont = ''
		,no = 0
		,estab = 0
		,fistamp = ''
		,hc.qtt
		,comp = 0
	from
		B_histConsumo hc (nolock)
		inner join b_utentes cl (nolock) on hc.clstamp = cl.utstamp
	where
		hc.ref != ''
		--AND cl.no = @nr
		--AND cl.estab = @dep
		and (cl.ncont = @ncont or (cl.no = @nr and cl.estab = @dep))
		AND (hc.data between @dataIni and @dataFim)
		

	select
		vd.data
		,vd.hora
		,vd.documento
		,vd.nr
		,vd.vendedor
		,vd.ref
		,vd.design
		,vd.epv
		,vd.u_epvp
		,vd.u_epref
		,vd.u_ettent1
		,vd.u_ettent2
		,vd.iva
		,vd.desconto
		,pvp = vd.u_epvp * (vd.qtt - isnull(reg.qtt, 0))
		,utente = vd.epv * (vd.qtt - isnull(reg.qtt, 0))
		,vd.ftstamp
		,vd.tipodoc
		,vd.ndoc
		,vd.cobrado
		,vd.pnome
		,vd.u_vdsusprg
		,vd.u_ltstamp
		,vd.u_ltstamp2
		,vd.u_codigo
		,qtt = vd.qtt - ISNULL(reg.qtt,0)
		,ncomp = case when comp = 1 and u_codigo = '' then CONVERT(bit,1) else CONVERT(bit,0) end
		,'sel' = CONVERT(bit,0)
	from
		#dadosFt as vd
		left join #dadosReg reg on vd.fistamp = reg.ofistamp
	where
		vd.qtt - isnull(reg.qtt,0) > 0
	order by
		vd.data desc, vd.hora desc, vd.documento, vd.nr, vd.ref, vd.design


	If OBJECT_ID('tempdb.dbo.#dadosFt') IS NOT NULL
		drop table #dadosFt;
	If OBJECT_ID('tempdb.dbo.#dadosReg') IS NOT NULL
		drop table #dadosReg;
	If OBJECT_ID('tempdb.dbo.#dadosFprod') IS NOT NULL
		drop table #dadosFprod;

GO
Grant Execute On dbo.up_touch_produtosVendidosCliente to Public
Grant Control On dbo.up_touch_produtosVendidosCliente to Public
GO