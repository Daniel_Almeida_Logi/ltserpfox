/* sp painel Somar Atendimentos
	- adicionado parametro para permitir consulta apenas por determinado periodo

 exec up_touch_somarAtendimentos 0,'20230410','20230413','Loja 1'
 
 exec up_touch_somarAtendimentos 0,'','',''
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_somarAtendimentos]') IS NOT NULL
	drop procedure dbo.up_touch_somarAtendimentos
go

create procedure dbo.up_touch_somarAtendimentos
     @op		numeric(5)
	,@dataini	datetime
	,@datafim	datetime
	,@site		varchar(20)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON


	declare @controloTempo as bit 
	set @controloTempo = (select bool from B_Parameters	where stamp = 'ADM0000000269')

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#atendimentos'))
		DROP TABLE #atendimentos

		/*   */
	create table #atendimentos (
			sel				bit
			,data			varchar(50)
			,hora			varchar(50)
			,nome			varchar(254)
			,numero			varchar(254)
			,total			NUMERIC(15,2)
			,operador		varchar(254)
			,terminal		NUMERIC(5,0)
			,terminalNome	varchar(50)
			,nratend		varchar(25)
			,nrVendas		numeric(10,0)
			,no				numeric(10,0)
			,estab			numeric(5,0)
			,ano			numeric(4,0)
	)

	IF @controloTempo = 0
	begin


	


	INSERT INTO #atendimentos (sel,data,hora,nome,numero,total,operador,terminal,terminalNome,nratend,nrVendas,no,estab,ano	)
		select
			sel				= convert(bit,0)
			,data			= convert(varchar,pc.oData,102)
			,hora			= convert(varchar,pc.oData,8)
			,nome			= case when pc.no!=200 then isnull(b_utentes.nome,'')  ELSE '' END 
			,numero			= '[' + CONVERT(varchar,pc.no) + '][' + CONVERT(varchar,pc.estab) + ']'
			,total			= PC.total - pc.epaga3
			,operador		= pc.nome + ' [' + convert(varchar,pc.vendedor) + ']'
			,terminal		= pc.terminal
			,terminalNome	= pc.terminal_nome
			,nratend
			,nrVendas
			,pc.no
			,pc.estab
			,PC.ano
		from
			B_pagCentral pc (nolock)
			left join b_utentes (nolock) on b_utentes.no=pc.no and b_utentes.estab = pc.estab
			/* left join ft (nolock) on ft.u_nratend = pc.nrAtend and  */
		where
			nratend != ''
			and pc.vendedor = case when @op = 0 then pc.vendedor else @op end
			and (CONVERT(varchar,odata,102) between @dataini and @dataFim)
			and pc.site = @site
		order by
			pc.oData desc, hora asc
			
		select 
			sel				
			,data			
			,hora			
			,nome = (CASE WHEN nome COLLATE DATABASE_DEFAULT <> '' THEN nome COLLATE DATABASE_DEFAULT ELSE (SELECT TOP 1 nome COLLATE DATABASE_DEFAULT FROM ft(nolock) WHERE u_nratend  COLLATE DATABASE_DEFAULT = nrAtend  COLLATE DATABASE_DEFAULT and no = #atendimentos.no) END)
			,numero		
			,total			
			,operador		
			,terminal		
			,terminalNome	
			,nratend
			,nrVendas
			,no
			,estab
		 from  #atendimentos
		 order by data  desc, hora desc
	end



	IF @controloTempo = 1
	begin
		declare @valorControlTempo as numeric (10,5)
		set  @valorControlTempo = (select numValue from B_Parameters	where stamp = 'ADM0000000269')

		INSERT INTO #atendimentos (sel,data,hora,nome,numero,total,operador,terminal,terminalNome,nratend,nrVendas,no,estab,ano	)
		select
			sel				= convert(bit,0)
			,data			= convert(varchar,pc.oData,102)
			,hora			= convert(varchar,pc.oData,8)
			,nome			=  case when pc.no!=200 then isnull(b_utentes.nome,'')  ELSE '' END 
			,numero			= '[' + CONVERT(varchar,pc.no) + '][' + CONVERT(varchar,pc.estab) + ']'
			,total			= PC.total - pc.epaga3
			,operador		= pc.nome + ' [' + convert(varchar,pc.vendedor) + ']'
			,terminal		= pc.terminal
			,terminalNome	= pc.terminal_nome
			,nratend
			,nrVendas
			,pc.no
			,pc.estab
			,PC.ano
		from
			B_pagCentral pc (nolock)
			left join b_utentes (nolock) on b_utentes.no=pc.no and b_utentes.estab = pc.estab
			/* left join ft (nolock) on ft.u_nratend = pc.nrAtend and  */
		where
			nratend != ''
			and pc.vendedor = case when @op = 0 then pc.vendedor else @op end
			--and (CONVERT(varchar,odata,102) between @dataini and @dataFim)
			and oData > (SELECT DATEADD(minute, -@valorControlTempo, GETDATE()))
			and pc.site = @site
		order by
			pc.oData desc, hora asc


		select 
			sel				
			,data			
			,hora			
			,nome = (CASE WHEN nome COLLATE DATABASE_DEFAULT <> '' THEN nome COLLATE DATABASE_DEFAULT ELSE (SELECT TOP 1 nome COLLATE DATABASE_DEFAULT FROM ft(nolock) WHERE u_nratend COLLATE DATABASE_DEFAULT = nrAtend  COLLATE DATABASE_DEFAULT and no = #atendimentos.no) END)
			,numero		
			,total			
			,operador		
			,terminal		
			,terminalNome	
			,nratend
			,nrVendas
			,no
			,estab
		 from  #atendimentos
		 order by data  desc, hora desc

	end

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#atendimentos'))
		DROP TABLE #atendimentos
	
GO
Grant Execute On dbo.up_touch_somarAtendimentos to Public
Grant Control On dbo.up_touch_somarAtendimentos to Public
GO