/*
	Regularizar Creditos Por Valor --
	exec up_touch_regularizarCreditosPorValor -1, '', 1073, -1, '19000101','20201212', '', 1, 'Administrador', 'Loja 1',0
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_regularizarCreditosPorValor]') IS NOT NULL
	drop procedure dbo.up_touch_regularizarCreditosPorValor
go

create procedure dbo.up_touch_regularizarCreditosPorValor

@venda		numeric(10,0),
@cliente	varchar(55),
@no			numeric(10,0),
@estab		numeric(3,0),
@dataIni	datetime,
@dataFim	datetime,
@nrAtend	varchar(20),
@user		numeric(6),
@group		varchar(50),
@site		varchar(20),
@MesmoNif	bit = 0

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

declare @nifempresa varchar(20)
set @nifempresa = (select ncont from empresa where site= @site)

if @MesmoNif=0
begin
	Select 
		convert(bit,0) as sel
		,convert(varchar,cc.datalc,102) as datalc
		,cc.ousrhora
		,nmdoc = isnull(case when ft.cobrado=1 then ft.nmdoc + ' (S)' else ft.nmdoc end,'Acerto Conta Corrente') /* qd não existe doc ligado à CC é porque é um acerto de CC */
		,cc.nrdoc
		,cliente = convert(varchar(100),'[' + CONVERT(varchar,cc.no) +'][' + CONVERT(varchar,cc.estab) + '] ' + cc.nome)
		/*,valorNreg = edeb - edebf
		,valorReg = edebf
		,regularizar = edeb - edebf*/
		,valorNreg = (cc.edeb-cc.edebf)-(cc.ecred-cc.ecredf)
		,valorReg = edebf - ecredf
		,regularizar = (cc.edeb-cc.edebf)-(cc.ecred-cc.ecredf)
		,ftstamp = isnull(ft.ftstamp,'')
		,cc.no
		,cc.estab
		,ndoc = isnull(ft.ndoc,0)
		,tipodoc = isnull(ft.tipodoc,0)
		,cc.ccstamp
		,utenteNome = CASE WHEN ft.u_hclstamp !='' THEN (SELECT convert(varchar(100),'[' + CONVERT(varchar,b_utentes.no) +'][' + CONVERT(varchar,b_utentes.estab) + '] ' + b_utentes.nome) FROM b_utentes WHERE utstamp=ft.u_hclstamp) 
									ELSE convert(varchar(100),'[' + CONVERT(varchar,ft.no) +'][' + CONVERT(varchar,ft.estab) + '] ' + ft.nome) END
	from 
		cc (nolock) 
		left join ft (nolock) on ft.ftstamp = cc.ftstamp
	where 
		(cc.nrdoc = case when @venda=-1 then cc.nrdoc else @venda end)
		AND (ft.u_nratend = case when @nrAtend='' then ft.u_nratend else @nrAtend end or ft.u_nratend is null)
		AND (cc.nome like @cliente+'%')
		AND (cc.no = case when @no=-1 then cc.no else @no end)
		AND (cc.estab = case when @estab=-1 then cc.estab else @estab end)
		AND (cc.datalc between @dataIni and @dataFim)
		and (ft.site = case when @site='' then ft.site else @site end or ft.site is null)
		And abs((cc.edeb-cc.edebf)-(cc.ecred-cc.ecredf)) > 0.0
		And origem<>'RD' 
		And origem<>'CH' 
	order by
		cc.datalc desc,cc.ousrhora desc, isnull(case when ft.cobrado=1 then ft.nmdoc + ' (S)' else ft.nmdoc end,''), cc.nrdoc desc
end
else
begin
	Select 
		convert(bit,0) as sel
		,convert(varchar,cc.datalc,102) as datalc
		,cc.ousrhora
		,nmdoc = isnull(case when ft.cobrado=1 then ft.nmdoc + ' (S)' else ft.nmdoc end,'Acerto Conta Corrente') /* qd não existe doc ligado à CC é porque é um acerto de CC */
		,cc.nrdoc
		,cliente = convert(varchar(100),'[' + CONVERT(varchar,cc.no) +'][' + CONVERT(varchar,cc.estab) + '] ' + cc.nome)
		/*,valorNreg = edeb - edebf
		,valorReg = edebf
		,regularizar = edeb - edebf*/
		,valorNreg = (cc.edeb-cc.edebf)-(cc.ecred-cc.ecredf)
		,valorReg = edebf - ecredf
		,regularizar = (cc.edeb-cc.edebf)-(cc.ecred-cc.ecredf)
		,ftstamp = isnull(ft.ftstamp,'')
		,cc.no
		,cc.estab
		,ndoc = isnull(ft.ndoc,0)
		,tipodoc = isnull(ft.tipodoc,0)
		,cc.ccstamp
		,utenteNome = CASE WHEN ft.u_hclstamp !='' THEN (SELECT convert(varchar(100),'[' + CONVERT(varchar,b_utentes.no) +'][' + CONVERT(varchar,b_utentes.estab) + '] ' + b_utentes.nome) FROM b_utentes WHERE utstamp=ft.u_hclstamp) 
									ELSE convert(varchar(100),'[' + CONVERT(varchar,ft.no) +'][' + CONVERT(varchar,ft.estab) + '] ' + ft.nome) END
	from 
		cc (nolock) 
		left join ft (nolock) on ft.ftstamp = cc.ftstamp
		inner join [dbo].[empresa] (nolock) on ft.site = empresa.site
	where 
		(cc.nrdoc = case when @venda=-1 then cc.nrdoc else @venda end)
		AND (ft.u_nratend = case when @nrAtend='' then ft.u_nratend else @nrAtend end or ft.u_nratend is null)
		AND (cc.nome like @cliente+'%')
		AND (cc.no = case when @no=-1 then cc.no else @no end)
		AND (cc.estab = case when @estab=-1 then cc.estab else @estab end)
		AND (cc.datalc between @dataIni and @dataFim)
		and (ft.site in (select site from empresa where ncont=@nifempresa))
		And abs((cc.edeb-cc.edebf)-(cc.ecred-cc.ecredf)) > 0.0
		And origem<>'RD' 
		And origem<>'CH' 
	order by
		cc.datalc desc,cc.ousrhora desc, isnull(case when ft.cobrado=1 then ft.nmdoc + ' (S)' else ft.nmdoc end,''), cc.nrdoc desc
end 


GO
Grant Execute On dbo.up_touch_regularizarCreditosPorValor to Public
grant control on dbo.up_touch_regularizarCreditosPorValor to Public
GO