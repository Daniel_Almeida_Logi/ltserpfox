/*

	obtem campos da FT
	
	 exec up_print_rodape 'Loja 1'
	  

	select * from fi
	select * from dplms
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_print_rodape]') IS NOT NULL
	drop procedure dbo.up_print_rodape
go

create procedure dbo.up_print_rodape

	@site varchar(30)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select
(select textValue from B_Parameters_site where stamp='ADM0000000080' and site=@site) as rodapetxt1
,(select textValue from B_Parameters_site where stamp='ADM0000000081' and site=@site) as rodapetxt2
,(select textValue from B_Parameters_site where stamp='ADM0000000082' and site=@site) as rodapetxt3
,(select textValue from B_Parameters_site where stamp='ADM0000000083' and site=@site) as rodapetxtAdd


GO
Grant Execute On dbo.up_print_rodape to Public
Grant Control On dbo.up_print_rodape to Public
GO
