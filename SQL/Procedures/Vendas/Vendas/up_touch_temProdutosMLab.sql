-- Pesquisa de Produtos que poder�o ser substituidos pelo lab de patrocinio
-- exec up_touch_temProdutosMLab '5440987', '50036432', 1, 'TOLIFE',1
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_temProdutosMLab]') IS NOT NULL
	drop procedure dbo.up_touch_temProdutosMLab
go

create procedure dbo.up_touch_temProdutosMLab
	@ref varchar(18),
	@cnpem varchar (8),
	@armazem numeric(5,0),
	@lab varchar(100),
	@site_nr tinyint

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

Select
	Top 1
	'ref'			= fprod.cnp
	,oref			= @ref /* mapeamento com cursor */
	,lab			= @lab
from
	fprod (nolock)
	left join st  (nolock) on st.ref=fprod.cnp and st.site_nr = @site_nr
	inner join cptgrp (nolock) on cptgrp.grupo=fprod.grupo
	left join sa (nolock) on sa.ref=st.ref AND sa.armazem=@armazem	
where 
	fprod.cnp != @ref
	and fprod.cnpem = @cnpem
	and (fprod.titaimdescr = @lab or st.u_lab = @lab)
order by 
	st.stock desc, st.design
	
OPTION (OPTIMIZE FOR (@ref unknown, @cnpem unknown,@armazem unknown,@lab unknown))	

GO
Grant Execute On dbo.up_touch_temProdutosMLab to Public
Grant Control On dbo.up_touch_temProdutosMLab to Public
Go