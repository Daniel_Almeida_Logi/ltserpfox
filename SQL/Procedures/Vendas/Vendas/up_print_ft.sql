/* obtem campos da FT

 exec up_print_ft 'ADM28EF3C23-9318-46FD-8DB','21188115648ERB ', 0, 1, 0
 
 exec up_print_ft 'ADME8126E43-3448-4400-BBC', '35567155834301', 0, 0, 0

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_print_ft]') IS NOT NULL
	drop procedure dbo.up_print_ft
go

create procedure dbo.up_print_ft

@stamp varchar(30)
,@nratend varchar(20)
,@suspensas as bit = 0
,@reserva as bit = 1
,@nc as bit = 1

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

DECLARE @resNO VARCHAR(30) = ''
DECLARE @reservaStamp VARCHAR(30)

IF @stamp <> '' and @reserva = 1
BEGIN

		SELECT @reservaStamp = bi2.bi2stamp
			FROM
			bi2(NOLOCK)
			INNER JOIN fi(nolock) AS FFI ON ffi.fistamp = bi2.fistamp
		WHERE
		ffi.ftstamp = @stamp

		select @resNO = bi.obrano from bi(nolock) where bistamp=@reservaStamp and bi.ndos = 5





	
END

declare @sql varchar(max)
select @sql = N'
select 
	ft.ftstamp
	,ft.pais
	,ft.nmdoc
	,ft.fno
	,ft.no
	,(case when b_utentes.removido=0 then ft.nome else ''*********'' end) as nome
	,(case when b_utentes.removido=0 then ft.morada else ''*********'' end) as morada
	,(case when b_utentes.removido=0 then ft.local else ''*********'' end) as local
	,(case when b_utentes.removido=0 then ft.codpost else ''*********'' end) as codpost
	,(case when b_utentes.removido=0 then ft.ncont else ''*********'' end) as ncont
	,(case when b_utentes.removido=0 then ft.bino else ''*********'' end) as bino
	,(case when b_utentes.removido=0 then ft.bidata else ''19000101'' end) as bidata
	,(case when b_utentes.removido=0 then ft.bilocal else ''*********'' end) as bilocal
	,(case when b_utentes.removido=0 then ft.telefone else ''*********'' end) as telefone
	,ft.zona
	,ft.vendedor
	,ft.vendnm
	,ft.fdata
	,ft.ftano
	,ft.pdata
	,ft.carga
	,ft.descar
	,ft.saida
	,ft.ivatx1
	,ft.ivatx2
	,ft.ivatx3
	,ft.fin
	,ft.final
	,ft.ndoc
	,ft.moeda
	,ft.fref
	,ft.ccusto
	,ft.ncusto
	,ft.facturada
	,ft.fnoft
	,ft.nmdocft
	,ft.estab
	,ft.cdata
	,ft.ivatx4
	,ft.segmento
	,ft.totqtt
	,ft.qtt1
	,ft.qtt2
	,ft.tipo
	,ft.cobrado
	,ft.cobranca
	,ft.tipodoc
	,ft.chora
	,ft.ivatx5
	,ft.ivatx6
	,ft.ivatx7
	,ft.ivatx8
	,ft.ivatx9
	,ft.cambiofixo
	,ft.memissao
	,ft.cobrador
	,ft.rota
	,ft.multi
	,ft.cheque
	,ft.clbanco
	,ft.clcheque
	,ft.chtotal
	,ft.echtotal
	,ft.custo
	,ft.eivain1
	,ft.eivain2
	,ft.eivain3
	,ft.eivav1
	,ft.eivav2
	,ft.eivav3
	,ft.ettiliq
	,ft.edescc
	,ft.ettiva
	,ft.etotal
	,ft.eivain4
	,ft.eivav4
	,ft.efinv
	,ft.ecusto
	,ft.eivain5
	,ft.eivav5
	,ft.edebreg
	,ft.eivain6
	,ft.eivav6
	,ft.eivain7
	,ft.eivav7
	,ft.eivain8
	,ft.eivav8
	,ft.eivain9
	,ft.eivav9
	,ft.total
	,ft.totalmoeda
	,ft.ivain1
	,ft.ivain2
	,ft.ivain3
	,ft.ivain4
	,ft.ivain5
	,ft.ivain6
	,ft.ivain7
	,ft.ivain8
	,ft.ivain9
	,ft.ivav1
	,ft.ivav2
	,ft.ivav3
	,ft.ivav4
	,ft.ivav5
	,ft.ivav6
	,ft.ivav7
	,ft.ivav8
	,ft.ivav9
	,ft.ttiliq
	,ft.ttiva
	,ft.descc
	,ft.debreg
	,ft.debregm
	,ft.intid
	,ft.nome2
	,ft.tpstamp
	,ft.tpdesc
	,ft.erdtotal
	,ft.rdtotal
	,ft.rdtotalm
	,ft.cambio
	,ft.site
	,ft.pnome
	,ft.pno
	,ft.cxstamp
	,ft.cxusername
	,ft.ssstamp
	,ft.ssusername
	,ft.anulado
	,ft.virs
	,ft.evirs
	,ft.valorm2
	,ft.ftid
	,ft.ousrinis
	,ft.ousrdata
	,ft.ousrhora
	,ft.usrinis
	,ft.usrdata
	,ft.usrhora
	,ft.u_nratend
	,ft.u_lote2
	,ft.u_lote
	,ft.u_ltstamp2
	,ft.u_nslote2
	,ft.u_nslote
	,ft.u_slote2
	,ft.u_tlote
	,ft.u_tlote2
	,ft.u_ltstamp
	,ft.u_slote
	,ft.exportado
	,ft.datatransporte
	,ft.localcarga
	,ft.tabIva
	,ft.id_tesouraria_conta
	,ft.pontosVd
	,ft.campanhas
	,td.u_tipodoc
	,td.tiposaft
	,b_utentes.nbenef as nbenef
	,b_utentes.nbenef2 as nbenef2
	,b_utentes.codigoP
	,b_utentes.pais
	,(case 
		when EXISTS (select 1 from B_dadosPsico(nolock) as b where b.ftstamp = ft.ftstamp and convert(varchar, b.nascimento,112) <> ''19000101'')
			then (select b.nascimento from B_dadosPsico(nolock) as b where b.ftstamp = ft.ftstamp)
		ELSE
			b_utentes.nascimento
	 end) as nascimento
	,localTesouraria = isnull(tesouraria_conta.descr,'''')
	,b_utentes.removido
	,b_utentes.no_ext
	,ft.ivatx10
	,ft.ivatx11
	,ft.ivatx12
	,ft.ivatx13
	,ft.eivain10
	,ft.eivain11
	,ft.eivain12
	,ft.eivain13
	,ft.eivav10
	,ft.eivav11
	,ft.eivav12
	,ft.eivav13
	,''A:''+empresa.ncont
	+''*B:''+ft.ncont
	+''*C:''+(case when b_utentes.codigop='''' then ''PT'' else ltrim(rtrim(b_utentes.codigop)) end)
	+''*D:''+td.tiposaft
	+''*E:''+(case when ft.anulado = 0 then ''N'' else ''A'' end)
	+''*F:''+convert(varchar, ft.fdata, 112)
	+''*G:''+left(isnull((select invoiceType from b_cert bce where ft.ftstamp=bce.stamp),''FT'') + '' '' + convert(varchar,ft.ndoc) + ''/'' + CONVERT(varchar,ft.fno),60)
	+''*H:''+''0''
	+''*I1:''+(select top 1 descricao from regiva (nolock))
	+''*I2:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''ISE'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''ISE'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''ISE'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''ISE'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''ISE'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''ISE'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''ISE'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''ISE'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''ISE'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''ISE'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''ISE'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''ISE'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''ISE'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I3:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''RED'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''RED'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''RED'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''RED'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''RED'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''RED'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''RED'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''RED'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''RED'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''RED'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''RED'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''RED'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''RED'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I4:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''RED'' then eivav1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''RED'' then eivav2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''RED'' then eivav3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''RED'' then eivav4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''RED'' then eivav5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''RED'' then eivav6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''RED'' then eivav7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''RED'' then eivav8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''RED'' then eivav9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''RED'' then eivav10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''RED'' then eivav11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''RED'' then eivav12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''RED'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I5:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''INT'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''INT'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''INT'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''INT'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''INT'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''INT'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''INT'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''INT'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''INT'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''INT'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''INT'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''INT'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''INT'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I6:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''INT'' then eivav1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''INT'' then eivav2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''INT'' then eivav3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''INT'' then eivav4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''INT'' then eivav5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''INT'' then eivav6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''INT'' then eivav7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''INT'' then eivav8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''INT'' then eivav9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''INT'' then eivav10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''INT'' then eivav11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''INT'' then eivav12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''INT'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I7:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''NOR'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''NOR'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''NOR'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''NOR'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''NOR'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''NOR'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''NOR'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''NOR'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''NOR'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''NOR'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''NOR'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''NOR'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''NOR'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I8:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''NOR'' then eivav1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''NOR'' then eivav2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''NOR'' then eivav3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''NOR'' then eivav4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''NOR'' then eivav5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''NOR'' then eivav6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''NOR'' then eivav7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''NOR'' then eivav8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''NOR'' then eivav9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''NOR'' then eivav10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''NOR'' then eivav11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''NOR'' then eivav12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''NOR'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))

	+''*N:''+ cast(cast(abs(ft.ettiva) as numeric(15,2)) as varchar(30))
	+''*O:''+ cast(cast(abs(ft.etotal) as numeric(15,2)) as varchar(30))
	+''*Q:''+ (SUBSTRING (hash,1,1) + SUBSTRING (hash,11,1) + SUBSTRING (hash,21,1) + SUBSTRING (hash,31,1))
	+''*R:''+ (select top 1 left(textValue,4) from B_Parameters (nolock) where stamp=''ADM0000000078'') as codqr
	,(CASE WHEN ' + LTRIM(@reserva) + ' = 1  '
	SELECT @sql = @SQL + (CASE 
							WHEN @nratend <> ''
								THEN ' THEN ISNULL((SELECT TOP 1 LTRIM(bo.obrano) FROM bo(NOLOCK) JOIN bo2(NOLOCK) ON bo.bostamp = bo2.bo2stamp WHERE bo2.u_nratend = ft.u_nratend AND bo.ndos = 5), '''')'
							ELSE
								' THEN '''  + isnull(@resNO,'') + ''''
						 END)

	SELECT @sql = @SQL + '  ELSE '''' END) as numReserva
	,ISNULL(ft2.u_vddom, 0) as ft2vddom
	,ISNULL(ft2.contacto, '''') as ft2contacto
	,ISNULL(ft2.morada, '''') as ft2morada
	,ISNULL(ft2.codpost, '''') as ft2codpost
	,ISNULL(ft2.local, '''') as ft2local
	,ISNULL(LTRIM(ft2.telefone), '''') as ft2telefone
	,ISNULL(ft2.email, '''') as ft2email
	,ISNULL(ft2.u_viatura, '''') as ft2viatura
	,ISNULL(ft2.horaentrega, '''') as ft2horaentrega
	,ISNULL(LTRIM(RTRIM(ft2.nomeUtPag)) + '' '' + (CASE WHEN dep.no = 200 THEN '''' ELSE ''('' + LTRIM(dep.no) + '')('' + LTRIM(dep.estab) + '')'' END) , LTRIM(RTRIM(dep.nome))) as depNome
	,ISNULL(dep.morada, '''') as depMorada
	,ISNULL(dep.codPost, '''') as depCodPost
	,ISNULL(LTRIM(dep.telefone), '''') as depTelefone
	,ISNULL(dep.nbenef, '''') as depNbenef
	,ft.u_hclstamp 
	,b_utentes.utstamp
	,isnull(ft2.id_efr_externa,0) as id_efr_externa 
from 
	ft (nolock)
	inner join ft2 (nolock) on ft2.ft2stamp = ft.ftstamp
	inner join td (nolock) on td.ndoc = ft.ndoc 
	inner join b_utentes (nolock) on b_utentes.no = ft.no and b_utentes.estab = ft.estab
	left join b_utentes(nolock) as dep ON dep.utstamp = ft.u_hclstamp
	left join tesouraria_conta (nolock) on tesouraria_conta.id = ft.id_tesouraria_conta
	inner join empresa (nolock) on empresa.site=ft.site
	inner join B_cert (nolock) on B_cert.stamp=ft.ftstamp
where '

SELECT @sql = @SQL + (CASE 
				WHEN @nratend <> '' 
					then 'ft.u_nratend = '''+@nratend+''' and ft.cobrado = ' + (CASE WHEN @suspensas = 1 THEN '1' ELSE '0' END) + (CASE WHEN @nc = 0 THEN ' and td.tiposaft <> ''NC''' ELSE '' END)
				ELSE
					'ft.ftstamp = '''+@stamp+''' and ft.cobrado = ' + (CASE WHEN @suspensas = 1 THEN '1' ELSE '0' END) + (CASE WHEN @nc = 0 THEN ' and td.tiposaft <> ''NC''' ELSE '' END)
			END)

/*
	cobrado = ' + (CASE WHEN @suspensas = 1 THEN '1' ELSE '0' END) + '	
	and (ftstamp = '''+@stamp+''''
	
	if @nratend != ''
		select @sql = @sql + N'
				or ft.u_nratend = '''+@nratend+''')'
	ELSE
		SELECT @sql = @sql + N')'
*/	



print @sql
execute (@sql)

GO
Grant Execute On dbo.up_print_ft to Public
Grant Control On dbo.up_print_ft to Public
GO