/*

	exec up_vendas_online_checkStatusChange 'storepickup_storepickup', 'Multibanco', 'Magento', 'Pendente', 'Em Processamento','Loja 1'
	exec up_vendas_online_checkStatusChange 'storepickup_storepickup', 'Multibanco', 'Magento', 'Pendente', 'Faturada','Loja 1'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_vendas_online_checkStatusChange]') IS NOT NULL
	DROP PROCEDURE dbo.up_vendas_online_checkStatusChange
GO

CREATE PROCEDURE [dbo].[up_vendas_online_checkStatusChange]
	@modo_envio		VARCHAR(50),
	@pagamento		VARCHAR(50),
	@entidade		VARCHAR(50),
	@status_ori		VARCHAR(50),
	@status_fim		VARCHAR(50),
	@site			VARCHAR(50)

AS
	IF(SELECT bool FROM B_Parameters_site(NOLOCK) WHERE stamp='ADM0000000186' AND site = @site) = 0
	BEGIN
		SELECT CAST(1 AS BIT) AS statChange
		RETURN
	END

	IF EXISTS
		(
		SELECT 
			1
		FROM
			statusOnlineOrder(nolock)
		WHERE
			modo_envio = @modo_envio
			AND pagamento = @pagamento
			AND entidade = @entidade
			AND status_Ori = @status_ori
			AND status_Fim = @status_fim
		)
	BEGIN
		SELECT CAST(1 AS BIT) AS statChange
	END
	ELSE
	BEGIN
		SELECT CAST(0 AS BIT) AS statChange
	END

GO
GRANT EXECUTE ON dbo.up_vendas_online_checkStatusChange TO PUBLIC
GRANT CONTROL ON dbo.up_vendas_online_checkStatusChange TO PUBLIC
GO