/* Guardar dados do produto para actualizar nas vendas (FUN��ES DA VENDA)

	exec up_touch_actref '5440987', 1, 1

	exec up_touch_actref '019429', 1, 1
	
	exec up_touch_actref '026394', 1, 1
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_ActRef]') IS NOT NULL
	drop procedure dbo.up_touch_ActRef
go

create procedure dbo.up_touch_ActRef
	@codigo  varchar (18),
	@armazem numeric(5,0),
	@siteno  tinyint

/* WITH ENCRYPTION */
AS
	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosGerais'))
		DROP TABLE #dadosGerais
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosGH'))
		DROP TABLE #dadosGH
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosMGR'))
		DROP TABLE #dadosMGR
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosArmazens'))
		DROP TABLE #dadosArmazens

	/* Armazens disponiveis por empresa */
	select 
		empresa_no
	into
		#dadosArmazens
	from
		empresa_arm (nolock)
	inner join 
		empresa on empresa_arm.empresa_no = empresa.no 
	where
		empresa.ncont = (select ncont from empresa where no = @siteno)

	---
	select top 1
		 'ststamp'		= st.ststamp
		,'ref'			= st.ref
		,'codigo'		= st.codigo
		,'design'		= st.design
		,'stns'			= st.stns
		,'familia'		= st.familia
		,'faminome'		= st.faminome
		,'epv1'			= st.epv1
		,'pcusto'		= st.pcusto
		,'epcusto'		= st.epcusto
		,'pcpond'		= st.pcpond
		,'epcpond'		= st.epcpond
		,'epcult'		= st.epcult
		,'usr1'			= st.usr1
		,'unidade'		= st.unidade
		,'site'			= isnull(empresa.site,'')
		,'stipo'		= 1
		,'validade'		= st.validade
		,'tabiva'		= st.tabiva
		,'ivaincl'		= st.ivaincl
		,'cpoc'			= st.cpoc
		,'u_nota1'		= st.u_nota1
		,'armazem'		= @armazem
		,'qlook'		= isnull(st.qlook,convert(bit,0))
		--,'stock'		= case when st.stns = 1 then 0 else isnull(sa.stock,0) end
		,'stock'		= case when st.stns = 1 then 0 else isnull(sa.stock,0) end
		,'bccodigo'		= Isnull(bc.codigo,'')
		,'generico'		= Isnull(fprod.generico,CONVERT(bit,0))
		,'benzo'		= Isnull(fprod.benzo,CONVERT(bit,0))
		,'psico'		= Isnull(fprod.psico,CONVERT(bit,0))
		,'grupo'		= Isnull(fprod.grupo,'')
		,grphmgcode		= isnull(grphmgcode,'')
		,titaimdescr	= isnull(titaimdescr,st.u_lab)
		,grphmgdescr	= isnull(grphmgdescr,'')
		,u_nomerc		= isnull(fprod.estaimdescr,'') + ', ' + isnull(sitcomdescr,'')
		,pvpdic			= isnull(fprod.pvporig,0)
		,pref			= isnull(fprod.pref,0)
		,pvpmaxre		= isnull(fprod.pvpmaxre,0)
		,ranking		= isnull(fprod.ranking,0)
		,qttfor
		,fornPrefere	= st.fornecedor + ' [' + CONVERT(varchar, fornec) + ']'
		,pvporig
		,ST.OBS
		,u_posprog		= isnull(posologia_orientativa,'')
		,protocolo = isnull(fprod.protocolo,0)
		,'dias'			= case when st.validade!='19000101'
							then datediff(d,getdate(),st.validade)
							else 0
						  end
		,tipembdescr	= isnull(fprod.tipembdescr,'')
		,st.local
		,st.u_local
		,'stocktot'		= case when st.stns = 1 then 0 else st.stock end
		,'existefprod'	= isnull(fprod.cnp,'')
		,comp			= isnull(
					case when cptgrp.rgeralpct != 0
						then 1
						else 0
					end
				,0)
		,cnpem = ISNULL(fprod.cnpem,'')
		,marg_terap = isnull(fprod.marg_terap,'')
		,usalote
		,st.inactivo
		,st.qttcli
		,u_escoacompart = isnull(fprod.u_escoacompart,0)
		,compSNS = ISNULL(Case when fprod.comp_sns > 0 then convert(bit,1) else convert(bit,0) end ,convert(bit,0))
		,siteno = st.site_nr
		,codUniEmb = isnull(fprod.codUniEmb,0)
		,ViaVerde = ISNULL(case when emb_via_verde.ref IS NULL then convert(bit,0) else convert(bit,1) end, convert(bit,0))
		,pvpTop4 = isnull(fprod.preco_acordo,0)
		,t45 = case when fprod.pvporig <= fprod.preco_acordo and fprod.preco_acordo > 0 then "T4"
					when ranking=1 and grphmgcode != 'GH0000' and grphmgcode != '' and fprod.pvporig <= fprod.pvpmaxre then "T5" 
					else ""
			   end
		, fprod.dispositivo_seguranca
		, st.qttminvd
		, st.qttembal
		,st.codmotiseimp
		,st.motiseimp
		,st.usr2
		,st.usr2desc
		,st.usr3
		,st.usr3desc
		,st.usr4
		,st.usr4desc
		,atcmini = isnull(B_atc.atcmini,'')
		,data_prevista_reposicao = isnull(fprod.data_prevista_reposicao,'1900-01-01')
	into
		#dadosGerais
	from
		st (nolock)
		left join bc	 (nolock) on st.ref=bc.ref and bc.site_nr = @siteno
		left join fprod	 (nolock) on st.ref=fprod.CNP or (st.codCNP = fprod.cnp  and LTRIM(RTRIM(fprod.cnp))!='') and st.codCNP<>'0'
		left join sa	 (nolock) on sa.ref=st.ref AND sa.armazem=@armazem
		left join cptgrp (nolock) on cptgrp.grupo = fprod.grupo
		left join posologia (nolock) on ((posologia.ref = fprod.ref or posologia.ref=st.codCNP) and posologia.ref!='')
		left join empresa (nolock) on st.site_nr = empresa.no
		left join emb_via_verde (nolock) on rtrim(ltrim(st.ref)) = rtrim(ltrim(emb_via_verde.ref))
		left join B_atcfp (nolock) on B_atcfp.cnp = st.ref 
		left join b_atc on b_atc.atccode = b_atcfp.atccode
	where
		(st.ref = @codigo OR st.codigo = @codigo OR bc.codigo = @codigo)
		and st.site_nr = @siteno
	
	---
	select
		top 1 
		fprod.cnp
		,fprod.grphmgcode
	into
		#dadosGH
	FROM
		fprod (nolock)
		inner join st (nolock) on fprod.cnp = st.ref
		inner join #dadosGerais on fprod.grphmgcode = #dadosGerais.grphmgcode
	WHERE
		fprod.cnp != #dadosGerais.ref
		AND fprod.generico = 1
		AND fprod.grphmgcode != '' 
		AND fprod.grphmgcode != 'GH0000'

	----	
	Select	
		versao
		,escalao
		,PVPmin
		,PVPmax
		,mrgFarm
		,FeeFarm
	into
		#dadosMGR
	from
		b_mrgRegressivas (nolock)
	
	---	
	select
		#dadosGerais.ststamp
		,#dadosGerais.ref
		,#dadosGerais.codigo
		,#dadosGerais.design
		,#dadosGerais.stns
		,#dadosGerais.familia
		,#dadosGerais.faminome
		,epv1 = #dadosGerais.epv1
		,#dadosGerais.pcusto
		,epcusto = #dadosGerais.epcusto
		,#dadosGerais.pcpond
		,#dadosGerais.epcpond
		,#dadosGerais.epcult
		,#dadosGerais.usr1
		,#dadosGerais.unidade
		,#dadosGerais.site
		,#dadosGerais.stipo
		,#dadosGerais.validade
		,#dadosGerais.tabiva
		,ivaincl = #dadosGerais.ivaincl
		,#dadosGerais.cpoc
		,#dadosGerais.u_nota1
		,#dadosGerais.armazem
		,#dadosGerais.qlook
		,#dadosGerais.stock
		,#dadosGerais.bccodigo
		,#dadosGerais.generico
		,#dadosGerais.benzo
		,#dadosGerais.psico
		,genalt = case when #dadosGH.cnp is null then convert(bit,0) else convert(bit,1) end
		,#dadosGerais.qttfor
		,#dadosGerais.fornPrefere
		,#dadosGerais.titaimdescr
		,#dadosGerais.grphmgcode
		,#dadosGerais.grphmgdescr
		,#dadosGerais.u_nomerc
		,#dadosGerais.pvpdic
		,#dadosGerais.pref
		,#dadosGerais.ranking
		,escalao	= isnull((Select top 1 escalao from #dadosMGR Where #dadosGerais.pvporig between PVPmin and PVPmax order by versao desc),0)
		,mrgFarm	= isnull((Select top 1 mrgFarm from #dadosMGR Where #dadosGerais.pvporig between PVPmin and PVPmax order by versao desc),0)
		,feeFarm	= isnull((Select top 1 feeFarm from #dadosMGR Where #dadosGerais.pvporig between PVPmin and PVPmax order by versao desc),0)
		,#dadosGerais.u_posprog
		,#dadosGerais.protocolo
		,#dadosGerais.dias
		,#dadosGerais.tipembdescr
		,#dadosGerais.local
		,#dadosGerais.u_local
		,#dadosGerais.stocktot
		,#dadosGerais.existefprod
		,#dadosGerais.comp
		,#dadosGerais.cnpem
		,#dadosGerais.marg_terap
		,#dadosGerais.usalote
		,#dadosGerais.inactivo
		,#dadosGerais.qttcli
		,#dadosGerais.pvpmaxre
		,#dadosGerais.grupo
		,opcao = CASE 
					WHEN #dadosGerais.grphmgcode in ('','GH0000') THEN 'I'
					WHEN #dadosGerais.epv1 > #dadosGerais.pvpmaxre THEN 'S'
					ELSE 'N'
					END
		,#dadosGerais.u_escoacompart
		,#dadosGerais.compSNS
		,#dadosGerais.codUniEmb
		,#dadosGErais.ViaVerde
		,stockempresa = isnull(x.stockempresa,0)
		,StockGrupo  = isnull(y.StockGrupo,0)
		,#dadosGErais.pvpTop4
		,#dadosGErais.t45
		,#dadosGErais.dispositivo_seguranca
		,#dadosGerais.obs
		,#dadosGerais.qttminvd
		,#dadosGerais.qttembal
		,#dadosGerais.codmotiseimp
		,#dadosGerais.motiseimp
		,#dadosGerais.usr2
		,#dadosGerais.usr2desc
		,#dadosGerais.usr3
		,#dadosGerais.usr3desc
		,#dadosGerais.usr4
		,#dadosGerais.usr4desc
		,#dadosGerais.atcmini
		,#dadosGerais.data_prevista_reposicao
	from
		#dadosGerais
		left join #dadosGH on #dadosGerais.grphmgcode = #dadosGH.grphmgcode
		left join empresa on empresa.no = #dadosGerais.siteno

		left outer join (select 
							ref
							,StockEmpresa = sum(stock) 
						from 
							st (nolock)
						where 
							site_nr in (select * from #dadosArmazens)
							and stns = 0
						group by 
							ref) x on #dadosGerais.ref = x.ref

		left outer join (select 
							ref
							,StockGrupo = sum(stock) 
						from 
							st (nolock)
						where 
							stns = 0
						group by 
							ref) y on #dadosGerais.ref = y.ref


	

GO
Grant Execute On dbo.up_touch_ActRef to Public
Grant Control On dbo.up_touch_ActRef to Public
GO