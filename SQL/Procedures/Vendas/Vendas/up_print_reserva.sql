/*

exec up_print_reserva '', 'MLD44BFB29-E678-446E-B8A', 0, 1

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_print_reserva]') IS NOT NULL
	drop procedure dbo.up_print_reserva
go

create procedure dbo.up_print_reserva

	@nratend varchar(20),
	@bostamp varchar(25) = '',
	@adiantamento INT = -1,
	@fechada BIT = 0

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON





declare @sqlCommand varchar(4000) = ''


set @sqlCommand = '
SELECT *,
	(CASE WHEN x.ftstamp <> '''' THEN (SELECT TOP 1 fistamp FROM fi(nolock) WHERE ftstamp = x.ftstamp ORDER BY fistamp DESC) ELSE '''' END) as lastStamp,
	(CASE WHEN x.nrreceita <> '''' THEN (SELECT TOP 1 bi2stamp from bi2(nolock) where bostamp = x.bostamp and nrreceita = x.nrreceita ORDER BY bi2stamp DESC) ELSE '''' END) as lastStampRec
FROM (
	select 
		bo.bostamp
		,bo.obrano
		,nrres=(case when bo.no<7
					then ''R'' + cast(bo.obrano as varchar(15)) + ''C'' + cast(BO.no as varchar(15)) + ''D'' + cast(BO.estab as varchar(15))
					else ''NRES'' + cast(BO.obrano as varchar(15)) + ''E''
					end )
		,bo.nmdos
		,bo.no
		,bo.estab
		,bo.nome
		,b_utentes.morada
		,b_utentes.local
		,b_utentes.codpost
		,b_utentes.telefone
		,b_utentes.tlmvl
		,b_utentes.email
		,b_utentes.ncont
		,bo.vendedor
		,bo.vendnm
		,bo.dataobra
		,bo.ousrhora
		,bo.datafinal
		,ref
		,design
		,cast(qtt as numeric(5,0)) as qtt
		,cast(isnull((select sum(etiliquido) as totad from fi (nolock) inner join bi2(nolock) as bbi2 on fi.fistamp = bbi2.fistamp  where bbi2.bostamp=bo.bostamp),0) as numeric(15,2)) as adiantamento
		,DATEADD(day, ISNULL((select numValue from B_Parameters(nolock) where stamp = ''ADM0000000149''), 0), bo.dataobra) as datValRes
		,bi.bistamp
		,cast(isnull((select sum(fi.etiliquido) as totad from fi (nolock) inner join bi2(nolock) as bbi2 on fi.fistamp = bbi2.fistamp where bbi2.bi2stamp=bi.bistamp),0) as numeric(15,2)) as adiantamentoLin
		,bi2.obsCl
		,bi2.obsInt
		,(CASE WHEN bi2.fistamp <> '''' THEN (SELECT ftstamp FROM fi(nolock) WHERE fistamp = bi2.fistamp) ELSE '''' END) as FTSTAMP
		,bi2.fistamp
		,bi2.nrreceita
	from bi (nolock)
		inner join bo (nolock) on bo.bostamp=bi.bostamp
		inner join bo2 (nolock) on bo2.bo2stamp=bi.bostamp
		inner join b_utentes (nolock) on b_utentes.no=bo.no and b_utentes.estab=bo.estab
		inner join bi2(nolock) on bi2.bi2stamp = bi.bistamp
	where
		 bo.nmdos=''Reserva de Cliente''

' 
	if(@bostamp = '') 
		set @sqlCommand = @sqlCommand+  '	and bo2.u_nratend = '''+ @nratend + ''''
	

	if(@bostamp <> '' AND @nratend = '') 
		set @sqlCommand = @sqlCommand+  ' and bo.bostamp = ''' + @bostamp +''''

	if(@fechada = 1) 
		set @sqlCommand = @sqlCommand+  ' and bi.fechada = 0'



set @sqlCommand = @sqlCommand + '
) as x
WHERE	
	1 = (CASE 
			WHEN '+STR(@adiantamento)+' = -1
				THEN 1
			WHEN '+STR(@adiantamento)+' = 0
				THEN (CASE 
						WHEN x.adiantamento > 0
							THEN 0
						ELSE
							1
					 END)
			WHEN '+STR(@adiantamento)+' = 1
				THEN (CASE 
						WHEN x.adiantamento > 0
							THEN 1
						ELSE
							0
					 END)
			ELSE
				0
		 END)
order by fistamp asc, x.bistamp asc


'

--print @sqlCommand

EXEC (@sqlCommand)

GO
Grant Execute On dbo.up_print_reserva to Public
Grant Control On dbo.up_print_reserva to Public
GO






IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[bo2]') AND name = N'IX_bo2_u_nratend')
CREATE NONCLUSTERED INDEX [IX_bo2_u_nratend] ON [dbo].[bo2]
(
	[u_nratend] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO