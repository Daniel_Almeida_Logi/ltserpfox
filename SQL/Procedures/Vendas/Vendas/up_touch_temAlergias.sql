-- Ver se existem Alergias (STOUCHPOS)
-- exec up_touch_temAlergias '8168617'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_temAlergias]') IS NOT NULL
	drop procedure dbo.up_touch_temAlergias
go

create procedure dbo.up_touch_temAlergias
	@ref varchar (18)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

SELECT 
	 ref = @ref
	,num = count(moleculasfpx.cnp)
FROM 
	B_alrgfp alrgfpx (nolock)
	INNER JOIN B_moleculasfp moleculasfpx (nolock) ON alrgfpx.moleculaID = moleculasfpx.moleculaID
WHERE 
	moleculasfpx.cnp = @ref

option (optimize for (@ref unknown))

GO
Grant Execute On dbo.up_touch_temAlergias to Public
Grant Control On dbo.up_touch_temAlergias to Public
GO