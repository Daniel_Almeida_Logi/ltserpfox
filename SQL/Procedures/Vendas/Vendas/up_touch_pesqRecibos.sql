-- Regularizar Creditos Por Valor --
-- exec up_touch_pesqRecibos -1, '', -1, -1, '19000101','20210527', '', 'Loja 1', '5440987'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_pesqRecibos]') IS NOT NULL
	drop procedure dbo.up_touch_pesqRecibos
go

create procedure dbo.up_touch_pesqRecibos

@venda		numeric(10,0),
@cliente	varchar(55),
@no			numeric(10,0),
@estab		numeric(3,0),
@dataIni	datetime,
@dataFim	datetime,
@nrAtend	varchar(20),
@loja		varchar(20),
@design     varchar(60) = ''

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

Select 
	convert(bit,0) as sel
	,convert(varchar,rdata,102) as rdata
	,re.ousrhora
	,nmdoc
	,rno
	,etotal
	,cliente = CONVERT(VARCHAR(100 ),'[' + CONVERT(varchar,re.no) +'][' + CONVERT(varchar,re.estab) + '] ' + re.nome)
	,restamp
from 
	re (nolock)
where 
	(re.rno = case when @venda=-1 then re.rno else @venda end)
	AND (re.u_nratend = case when @nrAtend='' then re.u_nratend else @nrAtend end)
	AND (re.nome like @cliente+'%')
	AND (re.no = case when @no=-1 then re.no else @no end)
	AND (re.estab = case when @estab=-1 then re.estab else @estab end)
	AND (re.rdata between @dataIni and @dataFim)
	and re.site = case when @loja='' then re.site else @loja end
	AND 1 = (case 
				when @design = '' then 1
				when ISNULL((select COUNT(*) from st(nolock) where ref = @design ), 0) <> 0
					THEN
					(case	
						when exists (
										select 
											1 
										from 
											fi(nolock) 
										where 
											ref = LTRIM(RTRIM(@design)) 
											and ftstamp in (select ftstamp from cc(nolock) where cc.ccstamp in (select ccstamp from rl(nolock) where rl.restamp = re.restamp))
											and fmarcada = 1
									)
							then 1
						else 0
					 end)
				else (case	
						when exists (
										select 
											1 
										from 
											fi(nolock) 
										where 
											design like LTRIM(RTRIM(@design)) + '%' 
											and ftstamp in (select ftstamp from cc(nolock) where cc.ccstamp in (select ccstamp from rl(nolock) where rl.restamp = re.restamp))
											and fmarcada = 1
									)
							then 1
						else 0
					 end)
			end)
order by 
	re.rdata desc
	,re.ousrhora desc


GO
Grant Execute On dbo.up_touch_pesqRecibos to Public
grant control on dbo.up_touch_pesqRecibos to Public
GO