/*

	obtem campos da FT
	
	 exec up_print_pagamentos '67409480089801 '
	  

	select * from fi
	select * from dplms
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_print_pagamentos]') IS NOT NULL
	drop procedure dbo.up_print_pagamentos
go

create procedure dbo.up_print_pagamentos

	@nratend varchar(20),
	@print bit = 1

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

DECLARE @varpaga1	VARCHAR(36)
	, @varpaga2	VARCHAR(36)
	, @varpaga3	VARCHAR(36)
	, @varpaga4	VARCHAR(36)
	, @varpaga5	VARCHAR(36)
	, @varpaga6	VARCHAR(36)
	, @varpaga7	VARCHAR(36)
	, @varpaga8	VARCHAR(36)
	, @varCartao VARCHAR(36)

set @varpaga1 = (select (SUBSTRING(campoFact, CHARINDEX('.', campofact)+1,LEN(campofact)+1-CHARINDEX('.', campofact)))  from B_modoPag where ref='01')
set @varpaga2 = (select (SUBSTRING(campoFact, CHARINDEX('.', campofact)+1,LEN(campofact)+1-CHARINDEX('.', campofact)))  from B_modoPag where ref='02')
set @varpaga3 = (select (SUBSTRING(campoFact, CHARINDEX('.', campofact)+1,LEN(campofact)+1-CHARINDEX('.', campofact)))  from B_modoPag where ref='03')
set @varpaga4 = (select (SUBSTRING(campoFact, CHARINDEX('.', campofact)+1,LEN(campofact)+1-CHARINDEX('.', campofact)))  from B_modoPag where ref='04')
set @varpaga5 = (select (SUBSTRING(campoFact, CHARINDEX('.', campofact)+1,LEN(campofact)+1-CHARINDEX('.', campofact)))  from B_modoPag where ref='05')
set @varpaga6 = (select (SUBSTRING(campoFact, CHARINDEX('.', campofact)+1,LEN(campofact)+1-CHARINDEX('.', campofact)))  from B_modoPag where ref='06')
set @varpaga7 = (select (SUBSTRING(campoFact, CHARINDEX('.', campofact)+1,LEN(campofact)+1-CHARINDEX('.', campofact)))  from B_modoPag where ref='07')
set @varpaga8 = (select (SUBSTRING(campoFact, CHARINDEX('.', campofact)+1,LEN(campofact)+1-CHARINDEX('.', campofact)))  from B_modoPag where ref='08')
set @varCartao = ISNULL((select (SUBSTRING(campoFact, CHARINDEX('.', campofact)+1,LEN(campofact)+1-CHARINDEX('.', campofact)))  from B_modoPag where valCartao = 1), '')

declare @sql varchar(max)
select @sql = N'

	select
	mp1 = (select  top 1 design from B_modoPag (nolock) where ref = ''01'')
	,mp2 = (select top 1 design from B_modoPag (nolock) where ref = ''02'')
	,mp3 = (select top 1 design from B_modoPag (nolock) where ref = ''03'')
	,mp4 = (select top 1 design from B_modoPag (nolock) where ref = ''04'')
	,mp5 = (select top 1 design from B_modoPag (nolock) where ref = ''05'')
	,mp6 = (select top 1 design from B_modoPag (nolock) where ref = ''06'')
	,mp7 = (select top 1 design from B_modoPag (nolock) where ref = ''07'')
	,mp8 = (select top 1 design from B_modoPag (nolock) where ref = ''08'')
	,inativo1 = (select top 1 inativo from B_modoPag (nolock) where ref = ''01'')
	,inativo2 = (select top 1 inativo from B_modoPag (nolock) where ref = ''02'')
	,inativo3 = (select top 1 inativo from B_modoPag (nolock) where ref = ''03'')
	,inativo4 = (select top 1 inativo from B_modoPag (nolock) where ref = ''04'')
	,inativo5 = (select top 1 inativo from B_modoPag (nolock) where ref = ''05'')
	,inativo6 = (select top 1 inativo from B_modoPag (nolock) where ref = ''06'')
	,inativo7 = (select top 1 inativo from B_modoPag (nolock) where ref = ''07'')
	,inativo8 = (select top 1 inativo from B_modoPag (nolock) where ref = ''08'')
	,valor1=isnull((select top 1 cast('+@varpaga1+' as numeric(15,2)) from B_pagCentral where nrAtend='''+@nratend+'''),0)
	,valor2=isnull((select top 1 cast('+@varpaga2+' as numeric(15,2)) from B_pagCentral where nrAtend='''+@nratend+'''),0)
	,valor3=isnull((select top 1 cast('+@varpaga3+' as numeric(15,2)) from B_pagCentral where nrAtend='''+@nratend+'''),0)
	,valor4=isnull((select top 1 cast('+@varpaga4+' as numeric(15,2)) from B_pagCentral where nrAtend='''+@nratend+'''),0)
	,valor5=isnull((select top 1 cast('+@varpaga5+' as numeric(15,2)) from B_pagCentral where nrAtend='''+@nratend+'''),0)
	,valor6=isnull((select top 1 cast('+@varpaga6+' as numeric(15,2)) from B_pagCentral where nrAtend='''+@nratend+'''),0)
	,valor7=isnull((select top 1 cast('+@varpaga7+' as numeric(15,2)) from B_pagCentral where nrAtend='''+@nratend+'''),0)
	,valor8=isnull((select top 1 cast('+@varpaga8+' as numeric(15,2)) from B_pagCentral where nrAtend='''+@nratend+'''),0)
	,credito=isnull((select top 1 cast(ntcredito as numeric(15,2)) from B_pagCentral where nrAtend='''+@nratend+'''),0)
	,etroco=isnull((select  top 1 cast(etroco as numeric(15,2)) from B_pagCentral where nrAtend='''+@nratend+'''),0)
	,CAST(' + (CASE WHEN @print = 1 THEN '1' ELSE '0' END) + ' as bit) as [print]
	,cambUsKz = ISNULL(dbo.uf_cambioEntreMoedas(''AOA'', ''USD'', (SELECT oData FROM b_pagCentral(nolock) WHERE nrAtend = ''' + @nratend + ''')),1)
	,cambEuKz = ISNULL(dbo.uf_cambioEntreMoedas(''AOA'', ''EUR'', (SELECT oData FROM b_pagCentral(nolock) WHERE nrAtend = ''' + @nratend + ''')),1)
	,' +
		(CASE 
		WHEN @varCartao <> '' THEN 'isnull((select top 1 cast('+@varCartao+' as numeric(15,2)) from B_pagCentral where nrAtend='''+@nratend+'''),0)'
		ELSE '0'
	END) +' as pagCartao
	'

print @sql
execute (@sql)

GO
Grant Execute On dbo.up_print_pagamentos to Public
Grant Control On dbo.up_print_pagamentos to Public
GO
