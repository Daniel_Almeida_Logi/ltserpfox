-- Ver as Alergias de um Produto (Vendas)
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_verAlergias]') IS NOT NULL
	drop procedure dbo.up_touch_verAlergias
go

create procedure dbo.up_touch_verAlergias

@ref varchar (120)

/* WITH ENCRYPTION */
AS

	SET NOCOUNT ON

	SELECT 
		moleculasx.descricao
		,alrgx.alergia
	FROM 
		B_alrg alrgx (nolock)
		INNER JOIN B_alrgfp alrgfpx (nolock) ON alrgx.alrgstamp = alrgfpx.alrgstamp
		INNER JOIN B_moleculas moleculasx (nolock) ON alrgfpx.moleculaID = moleculasx.moleculaID
		INNER JOIN B_moleculasfp moleculasfpx (nolock) ON alrgfpx.moleculaID = moleculasfpx.moleculaID
	where 
		cnp=@ref


GO
Grant Execute On dbo.up_touch_verAlergias to Public
Grant Control On dbo.up_touch_verAlergias to Public
GO
