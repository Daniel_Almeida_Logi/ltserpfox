/*

	obtem campos da FT
	exec up_touch_fi_atendimento '2106314511418P      '

	select * from fi
	select * from dplms
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_fi_atendimento]') IS NOT NULL
	drop procedure dbo.up_touch_fi_atendimento
go

create procedure dbo.up_touch_fi_atendimento

	@nratend varchar(20)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	DECLARE @pais	VARCHAR(36)

	SELECT top 1 @pais = pais from empresa
	
	select
		fi.*
		,pvptop4 = isnull(fprod.preco_acordo,0.00)
		--,diploma_id = isnull(dplms.diploma_id,0)
		, motisencao = case when upper(@pais)<>'PORTUGAL' then isnull((select isnull((select campo from b_multidata where b_multidata.multidatastamp=taxasiva.multidataStamp),'') from taxasiva where taxasiva.codigo=fi.tabiva and fi.iva=0),'')
						else isnull((select motiseimp from fi2 (nolock) where fi2.fistamp=fi.fistamp ),'')
						end 
		, LTRIM(RTRIM(ISNULL(fi2.bonusDescr,''))) AS bonusDescr
	from 
		fi (nolock)
		left join fprod (nolock) on fprod.cnp = fi.ref
		inner join ft(nolock) on ft.ftstamp=fi.ftstamp
		left join fi2 (nolock) on fi2.fistamp = fi.fistamp
		--left join dplms (nolock) on fi.u_diploma = dplms.u_design and dplms.u_design != ''
	where 
		u_nratend=@nratend and  ft.nmdoc!='Inserção de Receita'
	order by
		lordem

GO
Grant Execute On dbo.up_touch_fi_atendimento to Public
Grant Control On dbo.up_touch_fi_atendimento to Public
GO
