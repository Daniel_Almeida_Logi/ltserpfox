
/*	
	
	exec up_vendas_getValCartaoDev 10, 'ADMFACD2C0A-4673-40A3-88D'

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_vendas_getValCartaoDev]') IS NOT NULL
	drop procedure dbo.up_vendas_getValCartaoDev
go

create procedure dbo.up_vendas_getValCartaoDev
	@qtt AS NUMERIC(11,3),
	@fistamp AS VARCHAR(25)

AS

	SELECT 
		tipodoc 
		,valcartao = case when @qtt = qtt then valcartao else
				CONVERT(DECIMAL(15,2), ROUND((valcartao/case when qtt=0 then 1 else abs(convert(int,qtt)) end), 2, 1)) 
			end
		,evaldesc = case when @qtt = qtt then ROUND(evaldesc * qtt,2) else
			CONVERT(DECIMAL(15,2), ROUND((evaldesc), 2, 1))
			end
		,ftstamp
		,shouldMultipl = case when @qtt = qtt then convert(bit,0) else convert(bit,1) end
	FROM 
		fi(nolock)
	WHERE 
		fistamp= @fistamp	

GO
Grant Execute on dbo.up_vendas_getValCartaoDev to Public
Grant control on dbo.up_vendas_getValCartaoDev to Public
Go