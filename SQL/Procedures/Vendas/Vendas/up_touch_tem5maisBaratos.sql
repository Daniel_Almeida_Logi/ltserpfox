-- Verificar se existe medicamento dentro dos 5 mais Baratos (FUN��ES DA VENDA)
-- exec up_touch_tem5maisBaratos 'GH0700', '5413745', 1.53
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_tem5maisBaratos]') IS NOT NULL
	drop procedure dbo.up_touch_tem5maisBaratos
go

create procedure dbo.up_touch_tem5maisBaratos
	@grphmg varchar (6),
	@ref varchar(18),
	@pvp numeric(19,4) = 0

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select @pvp = case when @pvp=0 then 999.99 else @pvp end

select top 1
	oref = @ref
	,fprod.cnp
from
	fprod (nolock)
where
	ranking = 1
	and grphmgcode = @grphmg 
	and fprod.cnp != @ref
	and @pvp > (case when pvpcalc=0 then pvporig else pvpcalc end)
	
option (optimize for (@grphmg unknown,@ref unknown,@pvp unknown))	

GO
Grant Execute On dbo.up_touch_tem5maisBaratos to Public
Grant Control On dbo.up_touch_tem5maisBaratos to Public
GO