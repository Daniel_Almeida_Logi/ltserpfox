/*

	obtem campos da FT
 exec up_print_docs_atendimento 'ADMD964550B-5B73-4C80-946','21197113753557      ',''
 exec up_print_docs_atendimento 'ADM28EF3C23-9318-46FD-8DB',' ','', 1
 exec up_print_docs_atendimento 'ADM0483BFFB-3BBA-43EB-BC9', '21194113237E9X', ''
 exec up_print_docs_atendimento 'ADMCCFD37BD-7E17-4694-A9B', '13162818465501', ''
 exec up_print_docs_atendimento 'dos036BDA71-54D5-42FF-B74', '', ''
	

	select campanhas, ftstamp, etiliquido,epv,u_epvp, * from fi where campanhas <>'' order by fi.ftstamp
	select * from dplms
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_print_docs_atendimento]') IS NOT NULL
	drop procedure dbo.up_print_docs_atendimento
go

create procedure dbo.up_print_docs_atendimento

	@stamp varchar(30)
	,@nratend varchar(20)
	,@suspensas varchar(30)
	,@nc as bit = 1

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

If OBJECT_ID('tempdb.dbo.#dados') IS NOT NULL
   drop table #dados 


DECLARE @campanhas VARCHAR(MAX) = ''
DECLARE @campanhasFin VARCHAR(MAX) = ''
DECLARE @tmpCampanha VARCHAR(254)


IF @nratend = ''
BEGIN

	DECLARE uc_campanhas CURSOR FOR 
	SELECT DISTINCT campanhas FROM fi(nolock) WHERE fi.ftstamp = @stamp 

	OPEN uc_campanhas

	FETCH NEXT FROM uc_campanhas INTO @tmpCampanha

	WHILE @@FETCH_STATUS = 0
	BEGIN

		SET @campanhas = @campanhas + (CASE WHEN @campanhas <> '' THEN ',' ELSE '' END) + @tmpCampanha

		FETCH NEXT FROM uc_campanhas INTO @tmpCampanha
	END

	CLOSE uc_campanhas
	DEALLOCATE uc_campanhas

END
ELSE
BEGIN

	DECLARE uc_campanhas CURSOR FOR 
	SELECT DISTINCT campanhas FROM fi(nolock) WHERE fi.ftstamp IN (SELECT ftstamp FROM ft(nolock) WHERE u_nratend = @nratend) 

	OPEN uc_campanhas

	FETCH NEXT FROM uc_campanhas INTO @tmpCampanha

	WHILE @@FETCH_STATUS = 0
	BEGIN

		SET @campanhas = @campanhas + (CASE WHEN @campanhas <> '' THEN ',' ELSE '' END) + @tmpCampanha

		FETCH NEXT FROM uc_campanhas INTO @tmpCampanha
	END

	CLOSE uc_campanhas
	DEALLOCATE uc_campanhas

END

DECLARE uc_campanhas CURSOR FOR
SELECT 
	LTRIM(RTRIM(campanhas.ID)) + '-(' + LTRIM(RTRIM(campanhas.descricao)) + ')' as campanha
FROM
	(select DISTINCT LEFT(items, (CASE WHEN CHARINDEX('-', items) <> 0 then CHARINDEX('-', items)  - 1 ELSE 0 END)) as cod from dbo.up_splitToTable(@campanhas, ',')) as X
	join campanhas(nolock) on RTRIM(LTRIM(campanhas.id)) = RTRIM(LTRIM(X.cod))
	WHERE X.cod <> ''
	and campanhas.pontos = 0
	and campanhas.valor = 0
ORDER BY 
	X.cod asc

OPEN uc_campanhas

FETCH NEXT FROM uc_campanhas INTO @tmpCampanha

WHILE @@FETCH_STATUS = 0
BEGIN

	SET @campanhasFin = @campanhasFin + (CASE WHEN @campanhasFin <> '' THEN ';' ELSE '' END) + LTRIM(RTRIM(@tmpCampanha))

	FETCH NEXT FROM uc_campanhas INTO @tmpCampanha
END

CLOSE uc_campanhas
DEALLOCATE uc_campanhas

SET @campanhasFin = REPLACE(@campanhasFin, '''', '�')

declare @sql nvarchar(max)
declare @sql1 nvarchar(max) = ''

select @sql = N'
;with detalhesdoc as
(select 
ftstamp
,nrdoc = left(isnull(b_cert.invoiceType,''FT'') + '' '' + convert(varchar,ft.ndoc) + ''/'' + CONVERT(varchar,ft.fno),60) 
, valor = cast(etotal as numeric(15,2))
--, totdesc = cast((select SUM(u_descval) from fi (nolock) where fi.ftstamp=ft.ftstamp) as numeric(15,2))
, totdesc = cast((select SUM((epv*qtt) - (CASE WHEN etiliquido >= 0 THEN etiliquido ELSE (etiliquido * -1) END)) from fi (nolock) where fi.ftstamp=ft.ftstamp ) as numeric(15,2)) * (CASE WHEN b_cert.invoiceType = ''NC'' THEN -1 ELSE 1 END)
, valapagar = cast((case 
						when b_cert.invoiceType=''FR'' 
							then ft.etotal
						when b_cert.invoiceType = ''FT''
							then 0
						when b_cert.invoiceType = ''NC''
							then (CASE 
									WHEN NOT EXISTS(select 1 from cc(nolock) where ftstamp = (select top 1 ftstamp from fi(nolock) where fistamp in (select ffi.ofistamp from fi(nolock) as ffi where ffi.ftstamp = FT.ftstamp)))
										--THEN ft.etotal
										THEN (CASE 
												WHEN ft.etotal > 0
													THEN ft.etotal
													ELSE 0
											 END)
									ELSE 0
								 END)
						else 0 
			  end) as numeric(15,2))
, valcred = cast((case 
					when b_cert.invoiceType=''FR''
						then 0
					when b_cert.invoiceType = ''FT''
						then ft.etotal 
					when b_cert.invoiceType = ''NC''
						then (CASE 
								WHEN EXISTS(select 1 from cc(nolock) where ftstamp = (select top 1 ftstamp from fi(nolock) where fistamp in (select ffi.ofistamp from fi(nolock) as ffi where ffi.ftstamp = FT.ftstamp)))
									THEN (CASE WHEN ft.etotal<0 THEN ft.etotal ELSE ft.etotal*-1 END)
								--ELSE 0
								ELSE (CASE 
									    WHEN ft.etotal < 0 
										    THEN ft.etotal 
											ELSE 0 
									    END)
							  END)
					else 0 
				  end) as numeric(15,2))
, campanhas = ''' + @campanhasFin + '''
, valcartao = isnull((select sum(valcartao) from ft2 (nolock) where ft2.ft2stamp=ft.ftstamp),0)
, valcartaoutilizado = isnull((select sum(valcartaoutilizado) from ft2 (nolock) where ft2.ft2stamp=ft.ftstamp),0)
, b_utentes.nrcartao
, valdescontoapl = isnull((select SUM(u_epvp) from fi (nolock) where epromo=1 and fi.ftstamp=ft.ftstamp),0)
, ISNULL(b_fidel.pontosAtrib, 0) AS pontosAtrib
, ISNULL(b_fidel.pontosUsa, 0) AS pontosUsa
, ISNULL((SELECT SUM(pontos) FROM fi(nolock) where fi.ftstamp ' + (CASE 
																	WHEN @nratend = ''
																		THEN ' = ft.ftstamp '
																	ELSE 
																		' in (SELECT fft.ftstamp from ft(nolock) as fft where fft.u_nratend = ''' + @nratend + ''')'
																   END) +
'),0) as pontosGanhosAtend
, ft.ousrdata
, ft.ousrhora
, ft.fno
, valcomp = (select SUM(U_ETTENT1 + U_ETTENT2) from fi(nolock) where fi.ftstamp = ft.ftstamp)
, (CASE 
	WHEN b_cert.invoiceType = ''NC''
		then 1
	WHEN b_cert.invoiceType = ''FT''
		then 3
	WHEN b_cert.invoiceType = ''FR''
		then 4
  END) as ordem
  ,total_compart_estado = case
							when ft2.u_abrev = ''SNS''
							then
								(select sum(fi.u_ettent1) from fi where   fi.ftstamp = ft.ftstamp and ft.cobrado=0 )
							else
								case 
									when ft2.u_abrev2 = ''SNS''
									then
									
										(select sum(fi.u_ettent2) from fi where   fi.ftstamp = ft.ftstamp and ft.cobrado=0 )
									else
									 0
									end
							end
,total_compart_ext = case
							when ft2.u_abrev <> ''SNS'' and ft2.u_abrev2 = ''SNS'' 
							then
							
								(select sum(fi.u_ettent1) from fi where (u_ettent1 <> 0 and  u_ettent2 <> 0 )  and  fi.ftstamp = ft.ftstamp and ft.cobrado=0 )
							else
								case 
									when ft2.u_abrev2 <> ''SNS''
									then
									
										(select sum(fi.u_ettent2) from fi where fi.ftstamp = ft.ftstamp and ft.cobrado=0 )
									else
										0
									end
							end
,total_compart_cli = iif((select sum(epv) from fi where (u_ettent1 <> 0 or u_ettent2 <>0 ) and fi.ftstamp = ft.ftstamp and ft.cobrado=0 and (ft2.u_abrev = ''SNS'' or ft2.u_abrev2 = ''SNS''   ))is  null 
							, 0
							, (select sum(epv) from fi where (u_ettent1 <> 0 or u_ettent2 <>0 ) and fi.ftstamp = ft.ftstamp and ft.cobrado=0 and (ft2.u_abrev = ''SNS'' or ft2.u_abrev2 = ''SNS''   )))
from ft (nolock) 
inner join ft2(nolock) on ft2.ft2stamp = ft.ftstamp
inner join b_cert (nolock) on B_cert.stamp=ft.ftstamp 
inner join td (nolock) on td.ndoc=ft.ndoc
inner join b_utentes (nolock) on b_utentes.no=ft.no and b_utentes.estab=ft.estab
left join b_fidel (nolock) on b_fidel.nrcartao = b_utentes.nrcartao and b_fidel.clno = b_utentes.no and b_fidel.clestab = b_utentes.estab and b_fidel.inactivo = 0
where 
td.nmdoc NOT IN (''Ft. Seguradoras'', ''Nt. Cr�d. Segur.'')

'

SELECT @sql = @SQL + (CASE 
				WHEN @nratend <> '' 
					then N' AND ft.u_nratend = '''+@nratend+''''  /*+ (CASE WHEN @nc = 0 THEN ' and td.tiposaft <> ''NC''' ELSE '' END)*/
				ELSE
					N' AND ft.ftstamp = '''+@stamp+'''' + (CASE WHEN @nc = 0 THEN ' and td.tiposaft <> ''NC''' ELSE '' END)
			END)

/*ft.ftstamp= '''+@stamp+''''

	if @nratend != ''
select @sql = @sql + N'
		or ft.u_nratend = '''+@nratend+''''*/

select @sql1 =  N'
group by ft.ftstamp, b_cert.invoiceType, ft.ndoc, ft.fno, ft.etotal, b_utentes.nrcartao, b_fidel.pontosAtrib, b_fidel.pontosUsa, ft.ousrdata, ft.ousrhora, ft.cobrado, ft2.u_abrev, ft2.u_abrev2

UNION ALL

SELECT 
	restamp as ftstamp,
	nrdoc = ''RC'' + '' '' + LTRIM(ndoc) + ''/'' + LTRIM(rno),
	valor = cast(etotal as numeric(15,2)),
	totdesc = 0,
	valapagar = etotal,
	valcred = 0,
	campanhas = ''' + @campanhasFin + ''',
	valcartao = 0,
	valcartaoutilizado = 0,
	b_utentes.nrcartao,
	valdescontoapl = 0,
	ISNULL(b_fidel.pontosAtrib, 0) AS pontosAtrib,
	ISNULL(b_fidel.pontosUsa, 0) AS pontosUsa,
	ISNULL((SELECT SUM(pontos) FROM fi(nolock) where fi.ftstamp in (SELECT fft.ftstamp from ft(nolock) as fft where fft.u_nratend =  ''' + @nratend + ''' )),0) as pontosGanhosAtend,
	re.ousrdata,
	re.ousrhora,
	re.rno as fno,
	0 as valComp,
	2 as ordem
	,0 as total_compart_estado
	,0 as total_compart_ext
	,0 as total_compart_cli 
FROM
	re(nolock)
	inner join b_utentes (nolock) on b_utentes.no= re.no and b_utentes.estab= re.estab
	left join b_fidel (nolock) on b_fidel.nrcartao = b_utentes.nrcartao and b_fidel.clno = b_utentes.no and b_fidel.clestab = b_utentes.estab
WHERE
	u_nratend = ''' + iif(@nratend = '', '9999999999999999999',@nratend) + '''
	and nmdoc = ''Normal''
)
select ROW_NUMBER() OVER (ORDER BY ordem asc, ousrdata asc, ousrhora asc, fno asc) AS Row,* INTO #dados from detalhesdoc order by ordem asc, ousrdata asc, ousrhora asc, fno asc


SELECT 
	*,
	ISNULL((select top 1 total + creditos from b_pagcentral(nolock) where nratend = ''' + @nratend + ''' order by oData desc ),0) as totalPag,
	(SELECT SUM((CASE WHEN valcred > 0 THEN valcred ELSE 0 END)) FROM #dados) as totalCred,
	--ISNULL((select top 1 ntCredito - devolucoes from b_pagcentral(nolock) where nratend = ''' + @nratend + ''' order by oData desc ),0) as totalCred,
	(SELECT SUM((CASE WHEN totdesc > 0 THEN totdesc ELSE 0 END)) FROM #dados) as totalDesc,
	(SELECT SUM(valComp) FROM #dados) as totalComp,
	(SELECT SUM(total_compart_estado) FROM #dados) as total_compart_estado,
	(SELECT SUM(total_compart_ext) FROM #dados) as total_compart_ext,
	(SELECT SUM(total_compart_cli) FROM #dados) as total_compart_cli'

SET @sql1 = @sql1 + N'

FROM (
	select 
	doc1=(select nrdoc from #dados where Row=1)
	,valor1=(select valor from #dados where Row=1)
	,desc1=(select totdesc from #dados where Row=1)
	,pagar1=(select valapagar from #dados where Row=1)
	,cred1=(select valcred from #dados where Row=1)
	,doc2=isnull((select nrdoc from #dados where Row=2),'''')
	,valor2=isnull((select valor from #dados where Row=2),0)
	,desc2=isnull((select totdesc from #dados where Row=2),0)
	,pagar2=isnull((select valapagar from #dados where Row=2),0)
	,cred2=isnull((select valcred from #dados where Row=2),0)
	,doc3=isnull((select nrdoc from #dados where Row=3),'''')
	,valor3=isnull((select valor from #dados where Row=3),0)
	,desc3=isnull((select totdesc from #dados where Row=3),0)
	,pagar3=isnull((select valapagar from #dados where Row=3),0)
	,cred3=isnull((select valcred from #dados where Row=3),0)
	,doc4=isnull((select nrdoc from #dados where Row=4),'''')
	,valor4=isnull((select valor from #dados where Row=4),0)
	,desc4=isnull((select totdesc from #dados where Row=4),0)
	,pagar4=isnull((select valapagar from #dados where Row=4),0)
	,cred4=isnull((select valcred from #dados where Row=4),0)
	,doc5=isnull((select nrdoc from #dados where Row=5),'''')
	,valor5=isnull((select valor from #dados where Row=5),0)
	,desc5=isnull((select totdesc from #dados where Row=5),0)
	,pagar5=isnull((select valapagar from #dados where Row=5),0)
	,cred5=isnull((select valcred from #dados where Row=5),0)
	,doc6=isnull((select nrdoc from #dados where Row=6),'''')
	,valor6=isnull((select valor from #dados where Row=6),0)
	,desc6=isnull((select totdesc from #dados where Row=6),0)
	,pagar6=isnull((select valapagar from #dados where Row=6),0)
	,cred6=isnull((select valcred from #dados where Row=6),0)
	,doc7=isnull((select nrdoc from #dados where Row=7),'''')
	,valor7=isnull((select valor from #dados where Row=7),0)
	,desc7=isnull((select totdesc from #dados where Row=7),0)
	,pagar7=isnull((select valapagar from #dados where Row=7),0)
	,cred7=isnull((select valcred from #dados where Row=7),0)
	,doc8=isnull((select nrdoc from #dados where Row=8),'''')
	,valor8=isnull((select valor from #dados where Row=8),0)
	,desc8=isnull((select totdesc from #dados where Row=8),0)
	,pagar8=isnull((select valapagar from #dados where Row=8),0)
	,cred8=isnull((select valcred from #dados where Row=8),0)
	,doc9=isnull((select nrdoc from #dados where Row=9),'''')
	,valor9=isnull((select valor from #dados where Row=9),0)
	,desc9=isnull((select totdesc from #dados where Row=9),0)
	,pagar9=isnull((select valapagar from #dados where Row=9),0)
	,cred9=isnull((select valcred from #dados where Row=9),0)
	,doc10=isnull((select nrdoc from #dados where Row=10),'''')
	,valor10=isnull((select valor from #dados where Row=10),0)
	,desc10=isnull((select totdesc from #dados where Row=10),0)
	,pagar10=isnull((select valapagar from #dados where Row=10),0)
	,cred10=isnull((select valcred from #dados where Row=10),0)
	,doc11=isnull((select nrdoc from #dados where Row=11),'''')
	,valor11=isnull((select valor from #dados where Row=11),0)
	,desc11=isnull((select totdesc from #dados where Row=11),0)
	,pagar11=isnull((select valapagar from #dados where Row=11),0)
	,cred11=isnull((select valcred from #dados where Row=11),0)
	,doc12=isnull((select nrdoc from #dados where Row=12),'''')
	,valor12=isnull((select valor from #dados where Row=12),0)
	,desc12=isnull((select totdesc from #dados where Row=12),0)
	,pagar12=isnull((select valapagar from #dados where Row=12),0)
	,cred12=isnull((select valcred from #dados where Row=12),0)
	,doc13=isnull((select nrdoc from #dados where Row=13),'''')
	,valor13=isnull((select valor from #dados where Row=13),0)
	,desc13=isnull((select totdesc from #dados where Row=13),0)
	,pagar13=isnull((select valapagar from #dados where Row=13),0)
	,cred13=isnull((select valcred from #dados where Row=13),0)
	,doc14=isnull((select nrdoc from #dados where Row=14),'''')
	,valor14=isnull((select valor from #dados where Row=14),0)
	,desc14=isnull((select totdesc from #dados where Row=14),0)
	,pagar14=isnull((select valapagar from #dados where Row=14),0)
	,cred14=isnull((select valcred from #dados where Row=14),0)
	,doc15=isnull((select nrdoc from #dados where Row=15),'''')
	,valor15=isnull((select valor from #dados where Row=15),0)
	,desc15=isnull((select totdesc from #dados where Row=15),0)
	,pagar15=isnull((select valapagar from #dados where Row=15),0)
	,cred15=isnull((select valcred from #dados where Row=15),0)
	, campanhas= ''' + @campanhasFin + '''
	, nrcartao = (CASE WHEN (SELECT BOOL FROM B_PARAMETERS(NOLOCK) WHERE STAMP = ''ADM0000000302'') = 1 THEN (select top 1 nrcartao from #dados) ELSE '''' END)
	, valcartao = (select sum( valcartao) from #dados)
	, valcartaoutilizado = (select top 1 valcartaoutilizado from #dados)
	, valcartaocl=(select top 1 valcartao from b_fidel (nolock) where b_fidel.nrcartao= (select top 1 nrcartao from #dados))
	, valcartaohist=(select top 1 valcartaohist from b_fidel (nolock) where b_fidel.nrcartao= (select top 1 nrcartao from #dados))
	, valdescapl = (select top 1 valdescontoapl from #dados)
	, pontosAtrib = (select top 1 pontosAtrib from #dados)
	, pontosUsa = (select top 1 pontosUsa from #dados)
	--, pontosGanhosAtend = (select top 1 pontosGanhosAtend from #dados)
	--, pontosGanhosAtend = ISNULL((select pontosAT from b_pagCentral(nolock) where nrAtend = ''' + @nratend + '''), 0)
	, pontosGanhosAtend = ISNULL((SELECT SUM(fi.pontos) FROM fi(nolock) join ft(nolock) on fi.ftstamp = ft.ftstamp where ft.u_nratend = ''' + @nratend + '''), 0) + ISNULL((select pontosAT from b_pagCentral(nolock) where nrAtend = ''' + @nratend + '''), 0)
	, valComp1 = ISNULL((select valComp from #dados where Row=1),0)
	, valComp2 = ISNULL((select valComp from #dados where Row=2),0)
	, valComp3 = ISNULL((select valComp from #dados where Row=3),0)
	, valComp4 = ISNULL((select valComp from #dados where Row=4),0)
	, valComp5 = ISNULL((select valComp from #dados where Row=5),0)
	, valComp6 = ISNULL((select valComp from #dados where Row=6),0)
	, valComp7 = ISNULL((select valComp from #dados where Row=7),0)
	, valComp8 = ISNULL((select valComp from #dados where Row=8),0)
	, valComp9 = ISNULL((select valComp from #dados where Row=9),0)
	, valComp10 = ISNULL((select valComp from #dados where Row=10),0)
	, valComp11 = ISNULL((select valComp from #dados where Row=11),0)
	, valComp12 = ISNULL((select valComp from #dados where Row=12),0)
	, valComp13 = ISNULL((select valComp from #dados where Row=13),0)
	, valComp14 = ISNULL((select valComp from #dados where Row=14),0)
	, valComp15 = ISNULL((select valComp from #dados where Row=15),0)
	,total_compart_estado  =  ISNULL((select total_compart_estado from #dados where Row=16),0)
	,total_compart_ext  =  ISNULL((select total_compart_ext from #dados where Row=17),0)
	,total_compart_cli =  ISNULL((select total_compart_cli from #dados where Row=18),0)
) as x

'


print(@sql + @sql1)

execute (@sql + @sql1)


GO
Grant Execute On dbo.up_print_docs_atendimento to Public
Grant Control On dbo.up_print_docs_atendimento to Public
GO

