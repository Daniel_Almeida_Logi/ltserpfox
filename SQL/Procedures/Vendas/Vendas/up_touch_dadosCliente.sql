/* Ver dados do Cliente  (STOUCHPOS)

	delete from dispensa_eletronica_cc

	 exec up_touch_dadosCliente 234, 0
	 exec up_touch_dadosCliente 198288020, 0



*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_dadosCliente]') IS NOT NULL
	drop procedure dbo.up_touch_dadosCliente
go

create procedure dbo.up_touch_dadosCliente
	@noCliente		numeric(10,0),
	@estabCliente	numeric(3,0)

/* WITH ENCRYPTION */
AS

	DECLARE @valMes AS DECIMAL(20,5) = (SELECT numValue FROM B_Parameters(NOLOCK) where stamp = 'ADM0000000017')
	declare @utstamp varchar(25), @valorVales as numeric(9,2), @valorValesTotal as numeric(9,2)
	Select @utstamp = utstamp from b_utentes (nolock) where b_utentes.no = @noCliente and b_utentes.estab = @estabCliente and b_utentes.no != 200
	Select @valorVales = SUM(valor) from b_fidelVale (nolock) where b_fidelVale.clstamp = @utstamp and b_fidelVale.anulado = 0 and b_fidelVale.abatido = 0 and CONVERT(varchar, B_fidelvale.ousrdata, 112)<CONVERT(varchar, getdate(), 112) AND CONVERT(VARCHAR,DATEADD(MONTH,@valMes, B_fidelvale.ousrdata),112) >= CONVERT(varchar, GETDATE(),112)
	Select @valorValesTotal = SUM(valor) from b_fidelVale (nolock) where b_fidelVale.clstamp in (select utstamp from b_utentes where no = @noCliente) and b_fidelVale.anulado = 0 and b_fidelVale.abatido = 0 and CONVERT(varchar, B_fidelvale.ousrdata, 112)<CONVERT(varchar, getdate(), 112) AND CONVERT(VARCHAR,DATEADD(MONTH,@valMes, B_fidelvale.ousrdata),112) >= CONVERT(varchar, GETDATE(),112)
	DECLARE @cativacartaoencom AS bit = (select top 1 bool from B_Parameters_site where stamp='ADM0000000127')

	/* Cálculo de MC */
	declare @tipomc bit = (select top 1 bool from B_Parameters_site (nolock) where stamp='ADM0000000131')

	select 
		b_utentes.utstamp
		,b_utentes.nome
		,nome2
		,no
		,estab
		,ncont
		,telefone
		,tlmvl
		,fax
		,b_utentes.morada
		,local
		,codpost
		,zona
		,b_utentes.tipo
		,desconto
		,b_utentes.vencimento
		,eplafond
		,left(convert(varchar(254),obs),254) as obs
		,codigoP = case when b_utentes.codigoP = '' then 'PT' else codigoP end
		,particular
		,b_utentes.nascimento
		,email
		,tp.tpstamp
		,tpdesc
		,bino
		,convert(varchar,ultvenda,102) as u_ultvenda
		,esaldo = (select 
						total = ISNULL(sum(edeb - edebf) - sum(ecred - ecredf),0)
					from cc (nolock) 
					where 
						no = @noCliente
						and estab = @estabCliente)
		,codpla
		,despla
		,nbenef
		,nbenef2
		,entpla
		,valipla
		,valipla2
		,b_utentes.nrcartao
		,modofact		= isnull((select top 1 modofact from b_utentes cl2 (nolock) where cl2.no=b_utentes.no and cl2.estab=0),'')
		,pontos			= isnull(fidel.pontosatrib-fidel.pontosusa,0)
		,pontostotal	= isnull(fidel.pontosatrib,0)
		,valorVales		= case when b_utentes.nrcartao = '' then isnull(@valorValesTotal,0) else isnull(@valorVales,0) end
		,moeda
		,nocredit
		,ccusto
		,'TP_vencimento'	= isnull(tp.vencimento,0)
		,'TP_diames'		= isnull(tp.diames,0)
		,b_utentes.entfact
		,b_utentes.peso
		,b_utentes.altura
		,b_utentes.naoencomenda
		,B_utentes.pais
		,idade = case when year(b_utentes.nascimento)=1900 then 0 else datediff(year,b_utentes.nascimento,GETDATE()) end
		,convert(datetime, isnull(decc.validade_fim, itdatainicio)) as validade_fim
		,isnull(decc.tipodoc,'CC') as tipoDoc
		,cast('' as varchar(80)) as u_nmutdisp
		,cast('' as varchar(55)) as u_moutdisp
		,cast('' as varchar(45)) as u_cputdisp
		-- dados adquirente para psicotrópicos
		,cast('' as varchar(80)) as u_nmutavi 
		,cast('' as varchar(55)) as u_moutavi 
		,cast('' as varchar(45)) as u_cputavi 
		,cast('' as varchar(30)) as u_ndutavi 
		,u_ddutavi = convert(datetime,isnull(decc.validade_fim, '19000101'))
		,u_dcutavi = convert(datetime,'19000101')
		,u_idutavi = 0
		,valcartao = case when @tipomc = 0 then 
							isnull(valcartao-(case when @cativacartaoencom=1 then valcartao_cativado else 0 end),0)
						else
							ISNULL((select SUM(valcartao-(case when @cativacartaoencom=1 then valcartao_cativado else 0 end)) from b_fidel bf (nolock) where bf.nrcartao=b_utentes.nrcartao and convert(varchar,validade,112) >= convert(varchar,GETDATE(),112)),0)
						end 
		,valcartaoOld = case when @tipomc = 0 then 
							isnull(valcartao-(case when @cativacartaoencom=1 then valcartao_cativado else 0 end),0)
						else
							ISNULL((select SUM(valcartao-(case when @cativacartaoencom=1 then valcartao_cativado else 0 end)) from b_fidel bf (nolock) where bf.nrcartao=b_utentes.nrcartao and convert(varchar,validade,112) >= convert(varchar,GETDATE(),112)),0)
						end 
		,b_utentes.cativa
		,b_utentes.perccativa
		,b_utentes.no_ext
		,b_utentes.autorizasite
		,cast('' as varchar(25)) as u_adUtstamp
		,b_utentes.sexo
		,CAST('' as varchar(10)) as codDocID
		,b_utentes.codigoDocSPMS as codDocIDUT
		,CAST('' as varchar(100)) as contactoUT
		,obito
		,obitoRNU
		,CAST('' as varchar(100)) as recmId
	from 
		b_utentes (nolock)
		left join B_fidel fidel (nolock) on b_utentes.utstamp=fidel.clstamp and convert(varchar,fidel.validade,112) >= convert(varchar,GETDATE(),112)
		left join tp (nolock) on tp.descricao = b_utentes.tpdesc and tp.tipo = 1
		left join dispensa_eletronica_cc decc (nolock) ON  b_utentes.utstamp=decc.utstamp
	where 
		b_utentes.no = @noCliente 
		and b_utentes.estab = @estabCliente

GO
Grant Execute On dbo.up_touch_dadosCliente to Public
Grant Control On dbo.up_touch_dadosCliente to Public
Go






