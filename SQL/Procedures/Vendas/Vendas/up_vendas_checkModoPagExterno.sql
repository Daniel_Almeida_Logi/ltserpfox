/*

	exec up_vendas_checkModoPagExterno

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_vendas_checkModoPagExterno]') IS NOT NULL
	drop procedure dbo.up_vendas_checkModoPagExterno
go

CREATE PROCEDURE up_vendas_checkModoPagExterno

AS

	SELECT
		*
	FROM
		B_modoPag(nolock)
	WHERE
		pagamentoExterno = 1



GO
Grant Execute on dbo.up_vendas_checkModoPagExterno to Public
Grant control on dbo.up_vendas_checkModoPagExterno to Public
GO