

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_vendas_requestPagExterno]') IS NOT NULL
	drop procedure dbo.up_vendas_requestPagExterno
go

create procedure [dbo].[up_vendas_requestPagExterno]
	@token varchar(25),
	@username varchar(254),
	@password varchar(254),
	@typeID numeric(4),
	@typePayment numeric(4),
	@typePaymentDesc varchar(254),
	@receiverID varchar(100),
	@receiverName varchar(254),
	@entityType varchar(50),
	@amount numeric(13,2),
	@email varchar(254),
	@phone varchar(50),
	@description varchar(MAX),
	@site varchar(100),
	@test bit,
	@timeLimit numeric(9),
	@ousrinis varchar(30),
	@operationID varchar(254) = '',
	@idExterno   varchar(240) = ''

	
AS	

	DECLARE @typeDesc VARCHAR(254) = (CASE 
										WHEN @typeID = 1 
											THEN 'CREATEPAYMENT' 
										ELSE
											'INFOPAYMENT'
									 END)
	
	
	insert into paymentMethodRequest(token,username,[password],typeID,typeIDdesc,typePayment,typePaymentDesc,receiverid,receivername,entityType,amount
					,[description],email,phone,clientIdNumber,externalReference,[name],categoryId,callBackURL,origin,additionalInfo,
					[address],postCode,city,nic,idUserBackoffice,timeLimitDays,sendEmail,operationId,startDate,endDate,typeGetInfo,[site],test, ousrinis, ousrdata, usrinis, usrdata, countryCode,currency)
	VALUES
				(
					@token,
					@username,
					@password,
					@typeID,
					@typeDesc,
					@typePayment,
					@typePaymentDesc,
					@receiverID,
					@receiverName,
					@entityType,
					@amount,
					@description,
					@email,
					@phone,
					'',
					left(isnull(@idExterno,''),240),
					'',
					0,
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					0,
					@timeLimit,
					0,
					@operationID,
					'',
					'',
					'',
					@site,
					@test,
					@ousrinis,
					GETDATE(),
					@ousrinis,
					GETDATE(),
					'+351',
					'EUR'
				)


GO
Grant Execute on dbo.up_vendas_requestPagExterno to Public
Grant control on dbo.up_vendas_requestPagExterno to Public
GO