-- Ver Saldo do Cliente Principal
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_saldoPorCliente]') IS NOT NULL
	drop procedure dbo.up_touch_saldoPorCliente
go

create procedure dbo.up_touch_saldoPorCliente

@cliente numeric(15,0)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select DATEDIFF(day, dataven, getdate()) as Idade,
	convert(varchar,datalc,102) as datalc, convert(varchar,dataven,102) as dataven,
	edeb, ecred, edebf, ecredf,
	edeb-edebf as dev, 1.11 as saldo,
	cc.moeda, cmdesc, nrdoc, ultdoc, debfm, credfm, cc.intid,
	ftstamp, faccstamp, ccstamp,
	cc.vendnm + ' ' + convert(varchar,cc.vendedor) + '' as vendedor
from cc (nolock)
where cc.no=@cliente and (case when cc.moeda='PTE ou EURO' or cc.moeda=space(11)
							then abs((cc.edeb-cc.edebf)-(cc.ecred-cc.ecredf))
							else abs((cc.debm-cc.debfm)-(cc.credm-cc.credfm)) end)
							>
						(case when cc.moeda='PTE ou EURO' or cc.moeda=space(11)
						then 0.010000
						else 0 end)
order by datalc, cm, nrdoc


GO
Grant Execute On dbo.up_touch_saldoPorCliente to Public
Grant Control On dbo.up_touch_saldoPorCliente to Public
Go