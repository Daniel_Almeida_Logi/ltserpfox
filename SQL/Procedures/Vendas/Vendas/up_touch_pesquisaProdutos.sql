-- Pesquisa de Produtos nas Vendas
-- exec up_touch_pesquisaProdutos 'ben', 1, '', '',1
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_pesquisaProdutos]') IS NOT NULL
	drop procedure dbo.up_touch_pesquisaProdutos
go

create procedure dbo.up_touch_pesquisaProdutos
@valor varchar(60),
@armazem numeric(18,0),
@familia varchar(60),
@dci varchar(60),
@site_nr tinyint

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

Select top 50
	convert(bit,0) as sel
	,st.ref
	,design			= ltrim(rtrim(st.design))
	,st.stock 
	/*stock = isnull((select top 1 stock from sa where ref=st.ref and armazem=@armazem),0)*/
	,st.epv1
	,validade		= convert(varchar,st.validade,102)
	,st.faminome
	,comp			= isnull(
						case when cptgrp.rgeralpct != 0
							then 1
							else 0
						end
					,0)
						/*isnull((case 
								when (left(cptgrp.descri��o,3)!='N�o' and cptgrp.u_diploma1='' and cptgrp.u_diploma2='' and cptgrp.u_diploma3='') AND cptgrp.grupo!='ST'
								then 1 
								else 0 end
							),0) */
	,generico		= IsNull(fprod.generico,0)
	,GRPHMG			= case 
						when fprod.grphmgcode='GH0000'
						then fprod.grphmgcode
						else fprod.grphmgcode + ' - ' + fprod.grphmgdescr
					end
	,psico			= isnull(fprod.psico,0)
	,benzo			= isnull(fprod.benzo,0)
	,st.ststamp
	,st.ptoenc
	,st.stmax
	,st.marg1
	,st.local
	,st.u_local
	,st.u_local2
	,fprod.dci
from
	st (nolock)
	left join fprod (nolock)	on st.ref=fprod.cnp
	left join cptgrp (nolock)	on cptgrp.grupo=fprod.grupo
where
	st.site_nr = @site_nr
	AND ( (st.design like @valor+'%' ) OR (st.ref like @valor+'%')
		OR (st.usr1 like @valor+'%') /*OR (st.usr2 like @valor+'%') OR (st.usr3 like @valor+'%') OR (st.usr4 like @valor+'%')*/
	)
	AND st.inactivo=0
	AND st.familia = (case when @familia='' then st.familia else @familia end)
	and isnull(dci,'') like '%' + @dci + '%'
	 
order by
	st.stock desc/*, st.design*/

GO
Grant Execute On dbo.up_touch_pesquisaProdutos to Public
Grant Control On dbo.up_touch_pesquisaProdutos to Public
Go