/*

	obtem campos da FT
	
	 exec up_print_qrcodes_atendimento '211971817362EP      '
	  exec up_print_qrcodes_atendimento 'ADM28EF3C23-9318-46FD-8DB',' ',''

	select * from fi
	select * from dplms
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_print_docs_atendimento]') IS NOT NULL
	drop procedure dbo.up_print_qrcodes_atendimento
go

create procedure dbo.up_print_qrcodes_atendimento

	@nratend varchar(20)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

If OBJECT_ID('tempdb.dbo.#dados') IS NOT NULL
   drop table #dados 


declare @sql varchar(max), @sql2 varchar(max), @sql3 varchar(max),  @sql4 varchar(max), @sql5 varchar(max),  @sql6 varchar(max), @sql7 varchar(max),  @sql8 varchar(max), @sql9 varchar(max),  @sql10 varchar(max), @sql11 varchar(max),  @sql12 varchar(max)
select @sql = N'
;with detalhesdoc as
(select 
ftstamp
from ft (nolock) 
--inner join b_cert (nolock) on B_cert.stamp=ft.ftstamp 
--inner join td (nolock) on td.ndoc=ft.ndoc
where ft.u_nratend = '''+@nratend+''''

select @sql = @sql + N'
)
select ROW_NUMBER() OVER (ORDER BY ftstamp desc) AS Row,*  into #dados from detalhesdoc order by ftstamp desc

select 
	(select
	''A:''+empresa.ncont
	+''*B:''+ft.ncont
	+''*C:''+(case when b_utentes.codigop='''' then ''PT'' else ltrim(rtrim(b_utentes.codigop)) end)
	+''*D:''+td.tiposaft
	+''*E:''+(case when ft.anulado = 0 then ''N'' else ''A'' end)
	+''*F:''+convert(varchar, ft.fdata, 112)
	+''*G:''+left(isnull((select invoiceType from b_cert bce where ft.ftstamp=bce.stamp),''FT'') + '' '' + convert(varchar,ft.ndoc) + ''/'' + CONVERT(varchar,ft.fno),60)
	+''*H:''+(CASE WHEN td.ATCUD <> '''' THEN LTRIM(RTRIM(td.atcud)) ELSE ''0'' END) + ''-'' + LTRIM(RTRIM(ft.fno))
	+''*I1:''+(select top 1 descricao from regiva (nolock))
	+''*I2:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''ISE'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''ISE'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''ISE'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''ISE'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''ISE'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''ISE'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''ISE'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''ISE'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''ISE'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''ISE'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''ISE'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''ISE'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''ISE'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I3:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''RED'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''RED'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''RED'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''RED'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''RED'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''RED'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''RED'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''RED'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''RED'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''RED'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''RED'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''RED'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''RED'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I4:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''RED'' then eivav1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''RED'' then eivav2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''RED'' then eivav3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''RED'' then eivav4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''RED'' then eivav5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''RED'' then eivav6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''RED'' then eivav7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''RED'' then eivav8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''RED'' then eivav9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''RED'' then eivav10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''RED'' then eivav11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''RED'' then eivav12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''RED'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I5:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''INT'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''INT'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''INT'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''INT'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''INT'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''INT'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''INT'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''INT'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''INT'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''INT'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''INT'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''INT'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''INT'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I6:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''INT'' then eivav1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''INT'' then eivav2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''INT'' then eivav3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''INT'' then eivav4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''INT'' then eivav5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''INT'' then eivav6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''INT'' then eivav7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''INT'' then eivav8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''INT'' then eivav9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''INT'' then eivav10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''INT'' then eivav11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''INT'' then eivav12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''INT'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))'
select @sql2 = N'
	+''*I7:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''NOR'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''NOR'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''NOR'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''NOR'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''NOR'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''NOR'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''NOR'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''NOR'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''NOR'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''NOR'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''NOR'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''NOR'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''NOR'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I8:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''NOR'' then eivav1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''NOR'' then eivav2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''NOR'' then eivav3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''NOR'' then eivav4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''NOR'' then eivav5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''NOR'' then eivav6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''NOR'' then eivav7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''NOR'' then eivav8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''NOR'' then eivav9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''NOR'' then eivav10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''NOR'' then eivav11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''NOR'' then eivav12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''NOR'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))

	+''*N:''+ cast(cast(abs(ft.ettiva) as numeric(15,2)) as varchar(30))
	+''*O:''+ cast(cast(abs(ft.etotal) as numeric(15,2)) as varchar(30))
	+''*Q:''+ (SUBSTRING (hash,1,1) + SUBSTRING (hash,11,1) + SUBSTRING (hash,21,1) + SUBSTRING (hash,31,1))
	+''*R:''+ (select top 1 left(textValue,4) from B_Parameters (nolock) where stamp=''ADM0000000078'')

from 
	ft (nolock)
	inner join ft2 (nolock) on ft2.ft2stamp = ft.ftstamp
	inner join td (nolock) on td.ndoc = ft.ndoc 
	inner join b_utentes (nolock) on b_utentes.no = ft.no and b_utentes.estab = ft.estab
	left join tesouraria_conta (nolock) on tesouraria_conta.id = ft.id_tesouraria_conta
	inner join empresa (nolock) on empresa.site=ft.site
	inner join B_cert (nolock) on B_cert.stamp=ft.ftstamp
where 
	ftstamp = (select ftstamp from #dados where Row=1)) as codlin1
	,(select ft.nmdoc + left(isnull(b_cert.invoiceType,''FT'') + '' '' + convert(varchar,ft.ndoc) + ''/'' + CONVERT(varchar,ft.fno),60) 
		from ft (nolock)
		inner join td (nolock) on td.ndoc = ft.ndoc 
		inner join b_cert (nolock) on b_cert.stamp=ft.ftstamp
		where ftstamp = (select ftstamp from #dados where Row=1)) as docnomelin1
	, (select (SUBSTRING (hash,1,1) + SUBSTRING (hash,11,1) + SUBSTRING (hash,21,1) + SUBSTRING (hash,31,1))
		+ '' '' + Convert(varchar(254),''Processado por programa certificado n.'' + (isnull((select textvalue from B_Parameters (nolock) where stamp=''ADM0000000078''),'''')) + case when isnull(ft.nmdoc,'''') = ''Venda Manual''
																	   then '' - Copia do documento original-'' + Rtrim(versaochave) + ''-'' + Rtrim(invoicetype) + ''M''
																	   else ''''
																	   end)
		from 
			ft (nolock) 
			inner join ft2 (nolock) on ft2.ft2stamp=ft.ftstamp
			inner join td (nolock) on td.ndoc = ft.ndoc 
			inner join b_cert (nolock) on b_cert.stamp=ft.ftstamp
		where ft.ftstamp = (select ftstamp from #dados where Row=1)) as certlin1
'
select @sql3 = N'
	,isnull((select
	''A:''+empresa.ncont
	+''*B:''+ft.ncont
	+''*C:''+(case when b_utentes.codigop='''' then ''PT'' else ltrim(rtrim(b_utentes.codigop)) end)
	+''*D:''+td.tiposaft
	+''*E:''+(case when ft.anulado = 0 then ''N'' else ''A'' end)
	+''*F:''+convert(varchar, ft.fdata, 112)
	+''*G:''+left(isnull((select invoiceType from b_cert bce where ft.ftstamp=bce.stamp),''FT'') + '' '' + convert(varchar,ft.ndoc) + ''/'' + CONVERT(varchar,ft.fno),60)
	+''*H:''+''0''
	+''*I1:''+(select top 1 descricao from regiva (nolock))
	+''*I2:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''ISE'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''ISE'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''ISE'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''ISE'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''ISE'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''ISE'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''ISE'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''ISE'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''ISE'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''ISE'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''ISE'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''ISE'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''ISE'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I3:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''RED'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''RED'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''RED'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''RED'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''RED'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''RED'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''RED'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''RED'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''RED'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''RED'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''RED'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''RED'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''RED'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I4:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''RED'' then eivav1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''RED'' then eivav2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''RED'' then eivav3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''RED'' then eivav4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''RED'' then eivav5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''RED'' then eivav6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''RED'' then eivav7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''RED'' then eivav8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''RED'' then eivav9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''RED'' then eivav10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''RED'' then eivav11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''RED'' then eivav12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''RED'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I5:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''INT'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''INT'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''INT'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''INT'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''INT'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''INT'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''INT'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''INT'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''INT'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''INT'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''INT'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''INT'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''INT'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I6:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''INT'' then eivav1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''INT'' then eivav2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''INT'' then eivav3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''INT'' then eivav4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''INT'' then eivav5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''INT'' then eivav6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''INT'' then eivav7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''INT'' then eivav8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''INT'' then eivav9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''INT'' then eivav10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''INT'' then eivav11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''INT'' then eivav12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''INT'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))'
select @sql4 = N'
	+''*I7:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''NOR'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''NOR'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''NOR'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''NOR'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''NOR'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''NOR'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''NOR'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''NOR'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''NOR'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''NOR'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''NOR'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''NOR'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''NOR'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I8:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''NOR'' then eivav1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''NOR'' then eivav2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''NOR'' then eivav3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''NOR'' then eivav4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''NOR'' then eivav5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''NOR'' then eivav6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''NOR'' then eivav7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''NOR'' then eivav8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''NOR'' then eivav9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''NOR'' then eivav10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''NOR'' then eivav11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''NOR'' then eivav12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''NOR'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))

	+''*N:''+ cast(cast(abs(ft.ettiva) as numeric(15,2)) as varchar(30))
	+''*O:''+ cast(cast(abs(ft.etotal) as numeric(15,2)) as varchar(30))
	+''*Q:''+ (SUBSTRING (hash,1,1) + SUBSTRING (hash,11,1) + SUBSTRING (hash,21,1) + SUBSTRING (hash,31,1))
	+''*R:''+ (select top 1 left(textValue,4) from B_Parameters (nolock) where stamp=''ADM0000000078'')

from 
	ft (nolock)
	inner join ft2 (nolock) on ft2.ft2stamp = ft.ftstamp
	inner join td (nolock) on td.ndoc = ft.ndoc 
	inner join b_utentes (nolock) on b_utentes.no = ft.no and b_utentes.estab = ft.estab
	left join tesouraria_conta (nolock) on tesouraria_conta.id = ft.id_tesouraria_conta
	inner join empresa (nolock) on empresa.site=ft.site
	inner join B_cert (nolock) on B_cert.stamp=ft.ftstamp
where 
	ftstamp = (select ftstamp from #dados where Row=2)),'''') as codlin2
	,isnull((select ft.nmdoc + left(isnull(b_cert.invoiceType,''FT'') + '' '' + convert(varchar,ft.ndoc) + ''/'' + CONVERT(varchar,ft.fno),60) 
		from ft (nolock)
		inner join td (nolock) on td.ndoc = ft.ndoc 
		inner join b_cert (nolock) on b_cert.stamp=ft.ftstamp
		where ftstamp = (select ftstamp from #dados where Row=2)),'''') as docnomelin2
	, isnull((select (SUBSTRING (hash,1,1) + SUBSTRING (hash,11,1) + SUBSTRING (hash,21,1) + SUBSTRING (hash,31,1))
		+ '' '' + Convert(varchar(254),''Processado por programa certificado n.'' + (isnull((select textvalue from B_Parameters (nolock) where stamp=''ADM0000000078''),'''')) + case when isnull(ft.nmdoc,'''') = ''Venda Manual''
																	   then '' - Copia do documento original-'' + Rtrim(versaochave) + ''-'' + Rtrim(invoicetype) + ''M''
																	   else ''''
																	   end)
		from 
			ft (nolock) 
			inner join ft2 (nolock) on ft2.ft2stamp=ft.ftstamp
			inner join td (nolock) on td.ndoc = ft.ndoc 
			inner join b_cert (nolock) on b_cert.stamp=ft.ftstamp
		where ft.ftstamp = (select ftstamp from #dados where Row=2)),'''') as certlin2'

select @sql5 = N'
	,isnull((select
	''A:''+empresa.ncont
	+''*B:''+ft.ncont
	+''*C:''+(case when b_utentes.codigop='''' then ''PT'' else ltrim(rtrim(b_utentes.codigop)) end)
	+''*D:''+td.tiposaft
	+''*E:''+(case when ft.anulado = 0 then ''N'' else ''A'' end)
	+''*F:''+convert(varchar, ft.fdata, 112)
	+''*G:''+left(isnull((select invoiceType from b_cert bce where ft.ftstamp=bce.stamp),''FT'') + '' '' + convert(varchar,ft.ndoc) + ''/'' + CONVERT(varchar,ft.fno),60)
	+''*H:''+''0''
	+''*I1:''+(select top 1 descricao from regiva (nolock))
	+''*I2:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''ISE'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''ISE'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''ISE'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''ISE'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''ISE'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''ISE'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''ISE'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''ISE'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''ISE'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''ISE'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''ISE'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''ISE'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''ISE'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I3:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''RED'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''RED'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''RED'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''RED'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''RED'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''RED'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''RED'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''RED'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''RED'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''RED'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''RED'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''RED'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''RED'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I4:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''RED'' then eivav1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''RED'' then eivav2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''RED'' then eivav3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''RED'' then eivav4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''RED'' then eivav5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''RED'' then eivav6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''RED'' then eivav7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''RED'' then eivav8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''RED'' then eivav9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''RED'' then eivav10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''RED'' then eivav11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''RED'' then eivav12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''RED'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I5:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''INT'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''INT'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''INT'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''INT'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''INT'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''INT'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''INT'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''INT'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''INT'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''INT'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''INT'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''INT'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''INT'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I6:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''INT'' then eivav1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''INT'' then eivav2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''INT'' then eivav3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''INT'' then eivav4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''INT'' then eivav5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''INT'' then eivav6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''INT'' then eivav7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''INT'' then eivav8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''INT'' then eivav9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''INT'' then eivav10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''INT'' then eivav11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''INT'' then eivav12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''INT'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))'
select @sql6 = N'
	+''*I7:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''NOR'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''NOR'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''NOR'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''NOR'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''NOR'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''NOR'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''NOR'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''NOR'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''NOR'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''NOR'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''NOR'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''NOR'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''NOR'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I8:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''NOR'' then eivav1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''NOR'' then eivav2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''NOR'' then eivav3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''NOR'' then eivav4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''NOR'' then eivav5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''NOR'' then eivav6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''NOR'' then eivav7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''NOR'' then eivav8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''NOR'' then eivav9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''NOR'' then eivav10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''NOR'' then eivav11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''NOR'' then eivav12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''NOR'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))

	+''*N:''+ cast(cast(abs(ft.ettiva) as numeric(15,2)) as varchar(30))
	+''*O:''+ cast(cast(abs(ft.etotal) as numeric(15,2)) as varchar(30))
	+''*Q:''+ (SUBSTRING (hash,1,1) + SUBSTRING (hash,11,1) + SUBSTRING (hash,21,1) + SUBSTRING (hash,31,1))
	+''*R:''+ (select top 1 left(textValue,4) from B_Parameters (nolock) where stamp=''ADM0000000078'')

from 
	ft (nolock)
	inner join ft2 (nolock) on ft2.ft2stamp = ft.ftstamp
	inner join td (nolock) on td.ndoc = ft.ndoc 
	inner join b_utentes (nolock) on b_utentes.no = ft.no and b_utentes.estab = ft.estab
	left join tesouraria_conta (nolock) on tesouraria_conta.id = ft.id_tesouraria_conta
	inner join empresa (nolock) on empresa.site=ft.site
	inner join B_cert (nolock) on B_cert.stamp=ft.ftstamp
where 
	ftstamp = (select ftstamp from #dados where Row=3)),'''') as codlin3
	,isnull((select ft.nmdoc + left(isnull(b_cert.invoiceType,''FT'') + '' '' + convert(varchar,ft.ndoc) + ''/'' + CONVERT(varchar,ft.fno),60) 
		from ft (nolock)
		inner join td (nolock) on td.ndoc = ft.ndoc 
		inner join b_cert (nolock) on b_cert.stamp=ft.ftstamp
		where ftstamp = (select ftstamp from #dados where Row=3)),'''') as docnomelin3
	, isnull((select (SUBSTRING (hash,1,1) + SUBSTRING (hash,11,1) + SUBSTRING (hash,21,1) + SUBSTRING (hash,31,1))
		+ '' '' + Convert(varchar(254),''Processado por programa certificado n.'' + (isnull((select textvalue from B_Parameters (nolock) where stamp=''ADM0000000078''),'''')) + case when isnull(ft.nmdoc,'''') = ''Venda Manual''
																	   then '' - Copia do documento original-'' + Rtrim(versaochave) + ''-'' + Rtrim(invoicetype) + ''M''
																	   else ''''
																	   end)
		from 
			ft (nolock) 
			inner join ft2 (nolock) on ft2.ft2stamp=ft.ftstamp
			inner join td (nolock) on td.ndoc = ft.ndoc 
			inner join b_cert (nolock) on b_cert.stamp=ft.ftstamp
		where ft.ftstamp = (select ftstamp from #dados where Row=3)),'''') as certlin3'
select @sql7 = N'
	,isnull((select
	''A:''+empresa.ncont
	+''*B:''+ft.ncont
	+''*C:''+(case when b_utentes.codigop='''' then ''PT'' else ltrim(rtrim(b_utentes.codigop)) end)
	+''*D:''+td.tiposaft
	+''*E:''+(case when ft.anulado = 0 then ''N'' else ''A'' end)
	+''*F:''+convert(varchar, ft.fdata, 112)
	+''*G:''+left(isnull((select invoiceType from b_cert bce where ft.ftstamp=bce.stamp),''FT'') + '' '' + convert(varchar,ft.ndoc) + ''/'' + CONVERT(varchar,ft.fno),60)
	+''*H:''+''0''
	+''*I1:''+(select top 1 descricao from regiva (nolock))
	+''*I2:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''ISE'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''ISE'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''ISE'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''ISE'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''ISE'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''ISE'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''ISE'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''ISE'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''ISE'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''ISE'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''ISE'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''ISE'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''ISE'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I3:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''RED'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''RED'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''RED'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''RED'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''RED'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''RED'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''RED'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''RED'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''RED'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''RED'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''RED'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''RED'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''RED'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I4:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''RED'' then eivav1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''RED'' then eivav2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''RED'' then eivav3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''RED'' then eivav4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''RED'' then eivav5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''RED'' then eivav6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''RED'' then eivav7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''RED'' then eivav8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''RED'' then eivav9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''RED'' then eivav10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''RED'' then eivav11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''RED'' then eivav12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''RED'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I5:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''INT'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''INT'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''INT'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''INT'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''INT'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''INT'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''INT'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''INT'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''INT'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''INT'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''INT'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''INT'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''INT'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I6:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''INT'' then eivav1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''INT'' then eivav2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''INT'' then eivav3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''INT'' then eivav4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''INT'' then eivav5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''INT'' then eivav6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''INT'' then eivav7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''INT'' then eivav8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''INT'' then eivav9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''INT'' then eivav10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''INT'' then eivav11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''INT'' then eivav12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''INT'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))'
select @sql8 = N'
	+''*I7:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''NOR'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''NOR'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''NOR'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''NOR'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''NOR'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''NOR'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''NOR'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''NOR'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''NOR'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''NOR'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''NOR'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''NOR'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''NOR'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I8:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''NOR'' then eivav1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''NOR'' then eivav2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''NOR'' then eivav3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''NOR'' then eivav4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''NOR'' then eivav5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''NOR'' then eivav6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''NOR'' then eivav7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''NOR'' then eivav8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''NOR'' then eivav9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''NOR'' then eivav10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''NOR'' then eivav11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''NOR'' then eivav12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''NOR'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))

	+''*N:''+ cast(cast(abs(ft.ettiva) as numeric(15,2)) as varchar(30))
	+''*O:''+ cast(cast(abs(ft.etotal) as numeric(15,2)) as varchar(30))
	+''*Q:''+ (SUBSTRING (hash,1,1) + SUBSTRING (hash,11,1) + SUBSTRING (hash,21,1) + SUBSTRING (hash,31,1))
	+''*R:''+ (select top 1 left(textValue,4) from B_Parameters (nolock) where stamp=''ADM0000000078'')

from 
	ft (nolock)
	inner join ft2 (nolock) on ft2.ft2stamp = ft.ftstamp
	inner join td (nolock) on td.ndoc = ft.ndoc 
	inner join b_utentes (nolock) on b_utentes.no = ft.no and b_utentes.estab = ft.estab
	left join tesouraria_conta (nolock) on tesouraria_conta.id = ft.id_tesouraria_conta
	inner join empresa (nolock) on empresa.site=ft.site
	inner join B_cert (nolock) on B_cert.stamp=ft.ftstamp
where 
	ftstamp = (select ftstamp from #dados where Row=4)),'''') as codlin4
	,isnull((select ft.nmdoc + left(isnull(b_cert.invoiceType,''FT'') + '' '' + convert(varchar,ft.ndoc) + ''/'' + CONVERT(varchar,ft.fno),60) 
		from ft (nolock)
		inner join td (nolock) on td.ndoc = ft.ndoc 
		inner join b_cert (nolock) on b_cert.stamp=ft.ftstamp
		where ftstamp = (select ftstamp from #dados where Row=4)),'''') as docnomelin4
	, isnull((select (SUBSTRING (hash,1,1) + SUBSTRING (hash,11,1) + SUBSTRING (hash,21,1) + SUBSTRING (hash,31,1))
		+ '' '' + Convert(varchar(254),''Processado por programa certificado n.'' + (isnull((select textvalue from B_Parameters (nolock) where stamp=''ADM0000000078''),'''')) + case when isnull(ft.nmdoc,'''') = ''Venda Manual''
																	   then '' - Copia do documento original-'' + Rtrim(versaochave) + ''-'' + Rtrim(invoicetype) + ''M''
																	   else ''''
																	   end)
		from 
			ft (nolock) 
			inner join ft2 (nolock) on ft2.ft2stamp=ft.ftstamp
			inner join td (nolock) on td.ndoc = ft.ndoc 
			inner join b_cert (nolock) on b_cert.stamp=ft.ftstamp
		where ft.ftstamp = (select ftstamp from #dados where Row=4)),'''') as certlin4'
select @sql9 = N'
	,isnull((select
	''A:''+empresa.ncont
	+''*B:''+ft.ncont
	+''*C:''+(case when b_utentes.codigop='''' then ''PT'' else ltrim(rtrim(b_utentes.codigop)) end)
	+''*D:''+td.tiposaft
	+''*E:''+(case when ft.anulado = 0 then ''N'' else ''A'' end)
	+''*F:''+convert(varchar, ft.fdata, 112)
	+''*G:''+left(isnull((select invoiceType from b_cert bce where ft.ftstamp=bce.stamp),''FT'') + '' '' + convert(varchar,ft.ndoc) + ''/'' + CONVERT(varchar,ft.fno),60)
	+''*H:''+''0''
	+''*I1:''+(select top 1 descricao from regiva (nolock))
	+''*I2:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''ISE'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''ISE'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''ISE'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''ISE'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''ISE'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''ISE'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''ISE'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''ISE'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''ISE'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''ISE'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''ISE'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''ISE'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''ISE'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I3:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''RED'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''RED'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''RED'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''RED'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''RED'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''RED'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''RED'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''RED'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''RED'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''RED'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''RED'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''RED'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''RED'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I4:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''RED'' then eivav1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''RED'' then eivav2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''RED'' then eivav3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''RED'' then eivav4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''RED'' then eivav5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''RED'' then eivav6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''RED'' then eivav7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''RED'' then eivav8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''RED'' then eivav9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''RED'' then eivav10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''RED'' then eivav11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''RED'' then eivav12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''RED'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I5:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''INT'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''INT'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''INT'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''INT'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''INT'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''INT'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''INT'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''INT'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''INT'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''INT'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''INT'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''INT'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''INT'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I6:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''INT'' then eivav1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''INT'' then eivav2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''INT'' then eivav3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''INT'' then eivav4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''INT'' then eivav5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''INT'' then eivav6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''INT'' then eivav7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''INT'' then eivav8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''INT'' then eivav9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''INT'' then eivav10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''INT'' then eivav11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''INT'' then eivav12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''INT'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))'
select @sql10 = N'
	+''*I7:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''NOR'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''NOR'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''NOR'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''NOR'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''NOR'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''NOR'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''NOR'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''NOR'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''NOR'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''NOR'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''NOR'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''NOR'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''NOR'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I8:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''NOR'' then eivav1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''NOR'' then eivav2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''NOR'' then eivav3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''NOR'' then eivav4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''NOR'' then eivav5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''NOR'' then eivav6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''NOR'' then eivav7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''NOR'' then eivav8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''NOR'' then eivav9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''NOR'' then eivav10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''NOR'' then eivav11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''NOR'' then eivav12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''NOR'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))

	+''*N:''+ cast(cast(abs(ft.ettiva) as numeric(15,2)) as varchar(30))
	+''*O:''+ cast(cast(abs(ft.etotal) as numeric(15,2)) as varchar(30))
	+''*Q:''+ (SUBSTRING (hash,1,1) + SUBSTRING (hash,11,1) + SUBSTRING (hash,21,1) + SUBSTRING (hash,31,1))
	+''*R:''+ (select top 1 left(textValue,4) from B_Parameters (nolock) where stamp=''ADM0000000078'')

from 
	ft (nolock)
	inner join ft2 (nolock) on ft2.ft2stamp = ft.ftstamp
	inner join td (nolock) on td.ndoc = ft.ndoc 
	inner join b_utentes (nolock) on b_utentes.no = ft.no and b_utentes.estab = ft.estab
	left join tesouraria_conta (nolock) on tesouraria_conta.id = ft.id_tesouraria_conta
	inner join empresa (nolock) on empresa.site=ft.site
	inner join B_cert (nolock) on B_cert.stamp=ft.ftstamp
where 
	ftstamp = (select ftstamp from #dados where Row=5)),'''') as codlin5
	,isnull((select ft.nmdoc + left(isnull(b_cert.invoiceType,''FT'') + '' '' + convert(varchar,ft.ndoc) + ''/'' + CONVERT(varchar,ft.fno),60) 
		from ft (nolock)
		inner join td (nolock) on td.ndoc = ft.ndoc 
		inner join b_cert (nolock) on b_cert.stamp=ft.ftstamp
		where ftstamp = (select ftstamp from #dados where Row=5)),'''') as docnomelin5
	, isnull((select (SUBSTRING (hash,1,1) + SUBSTRING (hash,11,1) + SUBSTRING (hash,21,1) + SUBSTRING (hash,31,1))
		+ '' '' + Convert(varchar(254),''Processado por programa certificado n.'' + (isnull((select textvalue from B_Parameters (nolock) where stamp=''ADM0000000078''),'''')) + case when isnull(ft.nmdoc,'''') = ''Venda Manual''
																	   then '' - Copia do documento original-'' + Rtrim(versaochave) + ''-'' + Rtrim(invoicetype) + ''M''
																	   else ''''
																	   end)
		from 
			ft (nolock) 
			inner join ft2 (nolock) on ft2.ft2stamp=ft.ftstamp
			inner join td (nolock) on td.ndoc = ft.ndoc 
			inner join b_cert (nolock) on b_cert.stamp=ft.ftstamp
		where ft.ftstamp = (select ftstamp from #dados where Row=5)),'''') as certlin5'
select @sql11 = N'
	,isnull((select
	''A:''+empresa.ncont
	+''*B:''+ft.ncont
	+''*C:''+(case when b_utentes.codigop='''' then ''PT'' else ltrim(rtrim(b_utentes.codigop)) end)
	+''*D:''+td.tiposaft
	+''*E:''+(case when ft.anulado = 0 then ''N'' else ''A'' end)
	+''*F:''+convert(varchar, ft.fdata, 112)
	+''*G:''+left(isnull((select invoiceType from b_cert bce where ft.ftstamp=bce.stamp),''FT'') + '' '' + convert(varchar,ft.ndoc) + ''/'' + CONVERT(varchar,ft.fno),60)
	+''*H:''+''0''
	+''*I1:''+(select top 1 descricao from regiva (nolock))
	+''*I2:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''ISE'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''ISE'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''ISE'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''ISE'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''ISE'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''ISE'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''ISE'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''ISE'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''ISE'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''ISE'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''ISE'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''ISE'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''ISE'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I3:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''RED'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''RED'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''RED'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''RED'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''RED'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''RED'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''RED'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''RED'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''RED'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''RED'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''RED'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''RED'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''RED'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I4:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''RED'' then eivav1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''RED'' then eivav2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''RED'' then eivav3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''RED'' then eivav4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''RED'' then eivav5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''RED'' then eivav6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''RED'' then eivav7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''RED'' then eivav8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''RED'' then eivav9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''RED'' then eivav10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''RED'' then eivav11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''RED'' then eivav12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''RED'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I5:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''INT'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''INT'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''INT'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''INT'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''INT'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''INT'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''INT'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''INT'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''INT'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''INT'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''INT'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''INT'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''INT'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I6:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''INT'' then eivav1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''INT'' then eivav2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''INT'' then eivav3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''INT'' then eivav4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''INT'' then eivav5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''INT'' then eivav6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''INT'' then eivav7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''INT'' then eivav8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''INT'' then eivav9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''INT'' then eivav10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''INT'' then eivav11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''INT'' then eivav12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''INT'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))'
select @sql12 = N'
	+''*I7:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''NOR'' then eivain1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''NOR'' then eivain2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''NOR'' then eivain3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''NOR'' then eivain4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''NOR'' then eivain5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''NOR'' then eivain6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''NOR'' then eivain7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''NOR'' then eivain8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''NOR'' then eivain9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''NOR'' then eivain10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''NOR'' then eivain11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''NOR'' then eivain12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''NOR'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
	+''*I8:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''NOR'' then eivav1 else 0 end)
										+(case when (select codigo from regiva where tabiva=2)=''NOR'' then eivav2 else 0 end)
										+(case when (select codigo from regiva where tabiva=3)=''NOR'' then eivav3 else 0 end)
										+(case when (select codigo from regiva where tabiva=4)=''NOR'' then eivav4 else 0 end)
										+(case when (select codigo from regiva where tabiva=5)=''NOR'' then eivav5 else 0 end)
										+(case when (select codigo from regiva where tabiva=6)=''NOR'' then eivav6 else 0 end)
										+(case when (select codigo from regiva where tabiva=7)=''NOR'' then eivav7 else 0 end)
										+(case when (select codigo from regiva where tabiva=8)=''NOR'' then eivav8 else 0 end)
										+(case when (select codigo from regiva where tabiva=9)=''NOR'' then eivav9 else 0 end)
										+(case when (select codigo from regiva where tabiva=10)=''NOR'' then eivav10 else 0 end)
										+(case when (select codigo from regiva where tabiva=11)=''NOR'' then eivav11 else 0 end)
										+(case when (select codigo from regiva where tabiva=12)=''NOR'' then eivav12 else 0 end)
										+(case when (select codigo from regiva where tabiva=13)=''NOR'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))

	+''*N:''+ cast(cast(abs(ft.ettiva) as numeric(15,2)) as varchar(30))
	+''*O:''+ cast(cast(abs(ft.etotal) as numeric(15,2)) as varchar(30))
	+''*Q:''+ (SUBSTRING (hash,1,1) + SUBSTRING (hash,11,1) + SUBSTRING (hash,21,1) + SUBSTRING (hash,31,1))
	+''*R:''+ (select top 1 left(textValue,4) from B_Parameters (nolock) where stamp=''ADM0000000078'')

from 
	ft (nolock)
	inner join ft2 (nolock) on ft2.ft2stamp = ft.ftstamp
	inner join td (nolock) on td.ndoc = ft.ndoc 
	inner join b_utentes (nolock) on b_utentes.no = ft.no and b_utentes.estab = ft.estab
	left join tesouraria_conta (nolock) on tesouraria_conta.id = ft.id_tesouraria_conta
	inner join empresa (nolock) on empresa.site=ft.site
	inner join B_cert (nolock) on B_cert.stamp=ft.ftstamp
where 
	ftstamp = (select ftstamp from #dados where Row=6)),'''') as codlin6
	,isnull((select ft.nmdoc + left(isnull(b_cert.invoiceType,''FT'') + '' '' + convert(varchar,ft.ndoc) + ''/'' + CONVERT(varchar,ft.fno),60) 
		from ft (nolock)
		inner join td (nolock) on td.ndoc = ft.ndoc 
		inner join b_cert (nolock) on b_cert.stamp=ft.ftstamp
		where ftstamp = (select ftstamp from #dados where Row=6)),'''') as docnomelin6
	, isnull((select (SUBSTRING (hash,1,1) + SUBSTRING (hash,11,1) + SUBSTRING (hash,21,1) + SUBSTRING (hash,31,1))
		+ '' '' + Convert(varchar(254),''Processado por programa certificado n.'' + (isnull((select textvalue from B_Parameters (nolock) where stamp=''ADM0000000078''),'''')) + case when isnull(ft.nmdoc,'''') = ''Venda Manual''
																	   then '' - Copia do documento original-'' + Rtrim(versaochave) + ''-'' + Rtrim(invoicetype) + ''M''
																	   else ''''
																	   end)
		from 
			ft (nolock) 
			inner join ft2 (nolock) on ft2.ft2stamp=ft.ftstamp
			inner join td (nolock) on td.ndoc = ft.ndoc 
			inner join b_cert (nolock) on b_cert.stamp=ft.ftstamp
		where ft.ftstamp = (select ftstamp from #dados where Row=6)),'''') as certlin6'


print @sql
print @sql2
print @sql3
print @sql4
print @sql5
print @sql6
print @sql7
print @sql8
print @sql9
print @sql10
print @sql11
print @sql12
execute (@sql+@sql2+@sql3+@sql4+@sql5+@sql6+@sql7+@sql8+@sql9+@sql10+@sql11+@sql12)

GO
Grant Execute On dbo.up_print_qrcodes_atendimento to Public
Grant Control On dbo.up_print_qrcodes_atendimento to Public
GO