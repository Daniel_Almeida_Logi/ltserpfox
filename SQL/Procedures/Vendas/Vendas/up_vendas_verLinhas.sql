-- ver as linhas de uma venda
-- up_vendas_verLinhas 'AA10010444377.221000003  '
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_vendas_verLinhas]') IS NOT NULL
	drop procedure dbo.up_vendas_verLinhas
go

create procedure dbo.up_vendas_verLinhas

@stamp varchar(30)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select 
	ft2.u_codigo
	,ft2.u_codigo2
	,ft2.u_design
	,ft2.u_design2
	,fi.*
from 
	fi (nolock)
	inner join ft2 (nolock) on ft2.ft2stamp=fi.ftstamp
where 
	(fi.ref!='' or fi.oref!='')
	and fi.ftstamp = @stamp


GO
Grant Execute On dbo.up_vendas_verLinhas to Public
Grant Control On dbo.up_vendas_verLinhas to Public
GO