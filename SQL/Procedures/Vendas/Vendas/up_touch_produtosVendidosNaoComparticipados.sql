-- Produtos Vendidos N�o Comparticipados  (STOUCHPOS)
-- exec up_touch_produtosVendidosNaoComparticipados 209, 0
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_produtosVendidosNaoComparticipados]') IS NOT NULL
	drop procedure dbo.up_touch_produtosVendidosNaoComparticipados
go

create procedure dbo.up_touch_produtosVendidosNaoComparticipados
	@cliente	numeric(10,0),
	@estab		numeric(3,0)
	
/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

Declare @dataIni as datetime

SET @dataIni = getDate() - (select numValue from b_parameters (nolock) where stamp = 'ADM0000000005')

;with
	cte1(data,hora,documento,nr,ref,design,cobrado,ndoc,u_codigo,nmdoc,fistamp,u_epvp,epv,qtt) as
	(
		select
			data			= convert(varchar,ft.fdata,102)
			,hora			= ft.ousrhora
			,documento		= case when ft.cobrado=1 then fi.nmdoc + ' (S)' else fi.nmdoc end
			,Nr				= fi.fno
			,ref			= fi.ref
			,design			= fi.design
			,cobrado		= ft.cobrado
			,ndoc			= fi.ndoc
			,u_codigo		= ft2.u_codigo
			,nmdoc			= ft.nmdoc
			,fistamp		= fi.FISTAMP
			,u_epvp			= fi.u_epvp
			,epv			= fi.epv
			,qtt			= fi.qtt
		from
			fi (nolock)
			inner join ft		(nolock) on ft.ftstamp = fi.ftstamp
			inner join ft2		(nolock) on ft.ftstamp = ft2.ft2stamp
			inner join fprod	(nolock) on fi.ref = fprod.cnp
			inner join cptgrp	(nolock) on cptgrp.grupo = fprod.grupo
		where
			fi.ref != ''
			AND ft.no = @cliente
			AND ft.estab = @estab
			AND	(fdata between @dataIni and getdate())
			AND ft.tipodoc != 3
			and ft.nmdoc != 'Inser��o de Receita'
			and ft.anulado = 0
			/*and cptgrp.rgeralpct!=0*/
			AND NOT (left(cptgrp.descri��o,3) = 'N�o' and cptgrp.u_diploma1 = '' and cptgrp.u_diploma2 = '' and cptgrp.u_diploma3 = '')
			AND cptgrp.grupo != 'ST'
			/*AND (fi.qtt-isnull((select Sum(qtt) as qtt from fi fix where fi.fistamp=fix.ofistamp),0))>0*/
			AND (ft.u_ltstamp='')

	),
	cte2(qtt,ofistamp) as 
	(
		select
			qtt = isnull(Sum(fi.qtt),0),
			ofistamp
		from fi (nolock)
			inner join cte1 on fi.ofistamp=cte1.fistamp
		group by
			ofistamp
	)

select 
	cte1.data, 
	cte1.hora,
	cte1.documento,
	cte1.Nr,
	cte1.ref, 
	cte1.design, 
	PVP		= cte1.u_epvp*(cte1.qtt-isnull(cte2.qtt,0)),
	Utente	= cte1.epv*(cte1.qtt-isnull(cte2.qtt,0)),
	qtt		= cte1.qtt-isnull(cte2.qtt,0),
	cte1.cobrado, 
	cte1.ndoc,
	cte1.u_codigo,
	cte1.nmdoc,
	'sel' = CONVERT(bit,0)
from 
	cte1
	left join cte2 on cte1.fistamp = cte2.ofistamp
where
	cte1.qtt - isnull(cte2.qtt,0)>0
order by 
	cte1.data, cte1.hora, cte1.nr, cte1.nmdoc

Option (recompile)

GO
Grant Execute On dbo.up_touch_produtosVendidosNaoComparticipados to Public
Grant Control On dbo.up_touch_produtosVendidosNaoComparticipados to Public
Go