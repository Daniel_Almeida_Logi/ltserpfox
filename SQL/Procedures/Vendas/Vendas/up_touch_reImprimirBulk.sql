/*  Sp usada para criar cursor Impress�o Resumos Re-Imprimir Bulk 

	 exec up_touch_reImprimirBulk 0, '', 0 , 0,'', '', '', '', '20231212', '20231212',30, 'Loja 1',0,'',0,1

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_touch_reImprimirBulk]') IS NOT NULL
	drop procedure dbo.up_touch_reImprimirBulk
GO

create procedure dbo.up_touch_reImprimirBulk
	@venda			numeric(10,0),
	@cliente		varchar(80),
	@clno			numeric(10,0),
	@estab			numeric(3,0),
	@tipo			varchar(MAX),
	@tipodoc		varchar(MAX),
	@vendedor		varchar(20),
	@terminal		varchar(20),
	@dataIni		datetime,
	@dataFim		datetime,
	@top			int,
	@site			varchar(20),
	@cPlano			bit, 
	@suspensas		varchar(254) = '',
	@excluirZero	bit,
	@creditos		bit

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempSupsOperadorRegParcialProduto'))
		DROP TABLE #TempSupsOperadorRegParcialProduto

	IF @vendedor = ''
		SET @vendedor = 'Todos'
	IF @terminal = ''
		SET @terminal = 'Todos'

	DECLARE @ul_tipo AS TABLE (tipo VARCHAR(20))

	INSERT INTO 
		@ul_tipo (tipo)
	SELECT 
		y.value
	FROM 
		dbo.SplitString(@tipo, ',') x
		cross apply dbo.SplitString(x.value, ',') y

	/* Calcula o valor � linha devido a regulariza��es parciais por produto*/
	select 
		ft.ftstamp
		,fi.etiliquido
	into
		#TempSupsOperadorRegParcialProduto			
	from 
		ft (nolock)
		inner join fi (nolock) on ft.ftstamp = fi.ftstamp
		left join (
			select 
				ofistamp, 
				qtt = sum(fi.qtt)
			from 
				fi (nolock)
				inner join td (nolock) on fi.ndoc = td.ndoc
			WHERE
				td.u_tipodoc != 3
			group by
				ofistamp
		) as FiQTT on fi.fistamp = FiQTT.ofistamp
	where 
		(convert(varchar(9),ft.no) = case when @clno=0 then convert(varchar(9),ft.no) else @clno end)
		and (convert(varchar(9),ft.estab) = case when @estab=-1 then convert(varchar(9),ft.estab) else @estab end)
		and ft.fdata between @dataIni and @dataFim
		and ft.site = case when @site = '' then ft.site else @site end
		and ft.cobrado = 1 /* Suspenso */
		and fi.qtt-isnull(FiQTT.qtt,0) > 0

	/* Result Set Final */
	SELECT 
		top (@top) 
		*, 
		x.myQtt as qtt,
		Round(case when x.ivaincl=0 then x.etiliquido + x.valIva else x.etiliquido end,2) as ettcl 
	FROM (
	select 
		'sel'			= convert(bit,0), 
		'data'			= CONVERT(varchar,fdata,102), 
		ft.ousrhora,
		'nmdoc'			= case when ft.cobrado=0 then ft.nmdoc else ft.nmdoc + ' (S)' end, 
		ft.fno,
		'cliente'		= ltrim(rtrim(ft.nome)) + ' [' + CONVERT(varchar,ft.no) + '][' + CONVERT(varchar,ft.estab) + ']', 
		ft.no, 
		ft.estab,
		ft.tipo,
		'vendedor'		= ft.vendnm + ' [' + CONVERT(varchar,ft.vendedor) + ']',
		fi.ref, 
		fi.design, 
		u_epref, 
		u_ettent1, 
		u_ettent2,
		fi.iva,
		fi.desconto,
		fi.u_descval,
		u_epvp			= ROUND(u_epvp,2),
		epv				= round(epv,2),
		fi.etiliquido,		
		ft.u_tlote, 
		ft.u_tlote2, 
		ft.u_slote,
		ft.u_slote2, 
		ft.u_lote,
		ft.u_lote2,
		ft.u_nslote, 
		ft.u_nslote2,
		ft.ftstamp, 
		ft.tipodoc, 
		ft.ndoc, 
		ft.cobrado, 
		ft.pnome, 
		ft2.u_vdsusprg, 
		ft.u_ltstamp, 
		ft.u_ltstamp2, 
		ft2.u_codigo,
		myqtt				= fi.qtt - ISNULL(FiQTT.qtt,0),
		qttreg				= ISNULL(FiQTT.qtt,0),				
		FT.IVATX1,
		FT.IVATX2,
		FT.IVATX3,
		FT.ivatx4, 
		fi.tabiva, 
		ft.edescc,
		descValor		= ((CASE 
								WHEN fi.desconto = 100 THEN fi.epv * fi.qtt 
								WHEN (fi.desconto BETWEEN 0.01 AND 99.99) then (fi.epv * fi.qtt) - ABS(etiliquido)
								ELSE 0 
								END)
								+
								u_descval
								)
								*
								(CASE WHEN TD.tipodoc = 3 THEN (-1) ELSE 1 END),
		fi.fistamp,
		fi.qtt,
		ft.fdata,
		ft2.u_design,
		eaquisicao	= CASE 
						WHEN td.u_tipodoc in (8) 
							THEN fi.qtt-ISNULL((SELECT SUM(qtt) AS qtt FROM fi (NOLOCK) fix INNER JOIN td
								 ON td.ndoc = fix.ndoc WHERE fi.fistamp=fix.ofistamp and td.u_tipodoc != 3),0)
						WHEN fi.eaquisicao = 0 THEN 0
						WHEN ISNULL((SELECT SUM(qtt) FROM fi (NOLOCK) fix INNER JOIN td on td.ndoc = fix.ndoc
							 WHERE fix.ofistamp = fi.fistamp AND td.u_tipodoc != 3),0) > fi.eaquisicao THEN 0
						ELSE fi.eaquisicao - ISNULL((SELECT SUM(qtt) FROM fi (NOLOCK) fix INNER JOIN td
							 ON td.ndoc = fix.ndoc WHERE fix.ofistamp=fi.fistamp AND td.u_tipodoc != 3),0)
					  END
		,valorReg = 
			CASE 
				WHEN  ISNULL(ft.ndoc,0) = 1 THEN ISNULL(edebf - ecredf,ft.etotal)
				ELSE
					ft.etotal - ISNULL((SELECT SUM(etiliquido) FROM #TempSupsOperadorRegParcialProduto 
						WHERE #TempSupsOperadorRegParcialProduto.ftstamp = ft.ftstamp),0)
			END
		,valorNreg = 
			CASE 
				WHEN  ISNULL(ft.ndoc,0) = 1 THEN ISNULL((cc.edeb-cc.edebf)-(cc.ecred-cc.ecredf),ft.etotal)
				ELSE
					ISNULL((SELECT SUM(etiliquido) FROM #TempSupsOperadorRegParcialProduto 
						WHERE #TempSupsOperadorRegParcialProduto.ftstamp = ft.ftstamp),0)
			END	
		,utenteNome = 
			CASE WHEN ft.u_hclstamp !='' THEN (SELECT  b_utentes.nome + ' [' + CONVERT(varchar,b_utentes.no) +']
				[' + CONVERT(varchar,b_utentes.estab) + ']' FROM b_utentes WHERE utstamp=ft.u_hclstamp) 
			ELSE  ft.nome + ' [' + CONVERT(varchar,ft.no) +'][' + CONVERT(varchar,ft.estab) + ']' END	
		,fi.ivaincl
		,(CASE 
			WHEN fi.ivaincl = 0 
				THEN (fi.etiliquido*fi.iva)/100
			ELSE
				((fi.eslvu*fi.qtt)*fi.iva)/100
		 END) AS valIva
		 ,ft2.u_codigo  AS plano
		,ft2.u_codigo2 AS plano2
		,CASE WHEN (fprod.cnpem = '' or fprod.cnpem = 0) THEN '' ELSE ISNULL(fprod.cnpem,'') END AS cnpem
		,id = ROW_NUMBER() over (partition by ft.fno order by ft.fno desc)
		,b_utentes.nbenef
	FROM 
		ft (NOLOCK)
		INNER JOIN ft2 (NOLOCK) ON ft2.ft2stamp = ft.ftstamp
		INNER JOIN fi (NOLOCK)  ON ft.ftstamp = fi.ftstamp
		INNER JOIN td (NOLOCK)  ON td.ndoc=ft.ndoc
		LEFT JOIN (
			SELECT 
				ofistamp, 
				qtt = SUM(fi.qtt)
			FROM 
				fi (NOLOCK)
				INNER JOIN td (NOLOCK) ON fi.ndoc = td.ndoc
			WHERE 
				td.u_tipodoc != 3
			GROUP BY
				ofistamp
		) AS FiQTT ON fi.fistamp = FiQTT.ofistamp
		LEFT JOIN cc (NOLOCK) ON ft.ftstamp = cc.ftstamp
		INNER JOIN b_utentes (NOLOCK) ON b_utentes.no=ft.no AND b_utentes.estab=ft.estab
		LEFT JOIN fprod (NOLOCK) ON fprod.ref = fi.ref 
	where 
		(ft.tipodoc IN (1,2,3) OR u_tipodoc=4 OR u_tipodoc=9)
		AND td.u_tipodoc != 3 /*inser��o de receita*/
		AND td.codigoDoc != 17
		AND ft.ndoc not in (select distinct serie_regcli from empresa (nolock)) /*regulariza��es*/
		AND ft.ndoc not in (select distinct serie_ent from empresa (nolock)) /*fatura entidades*/
		AND ft.ndoc not in (select distinct serie_entSNS from empresa (nolock)) /*fatura SNS*/
		AND fi.epromo = 0
		AND fdata between @dataIni and @datafim
		AND ft.fno	= case when @venda != 0 then @venda else ft.fno end
		AND ft.nome like case when @cliente <> '' then '%' + @cliente + '%' else ft.nome end
		AND ft.estab = case when @estab != -1 then @estab else ft.estab end
		AND ft.no = case when @clno != 0 then @clno else ft.no end
		AND 1 = (CASE	
					WHEN @tipo = '' 
						then 1
					WHEN @tipo <> ''
						THEN (CASE 
								WHEN (select count(*) from @ul_tipo) = 1
									THEN (CASE WHEN b_utentes.tipo like LTRIM(RTRIM(@tipo)) + '%' THEN 1 ELSE 0 END)
								ELSE (CASE WHEN b_utentes.tipo in (select tipo from @ul_tipo) THEN 1 ELSE 0 END)
							 END)
				END)
		AND ft.vendnm  = CASE WHEN @vendedor != 'Todos'	  THEN @vendedor ELSE ft.vendnm END
		AND ft.pnome   = CASE WHEN @terminal != 'Todos'	  THEN @terminal ELSE ft.pnome  END
		AND ft.cobrado = CASE WHEN @suspensas='Suspensas' THEN 1  
								WHEN  @suspensas='N�o Suspensas' THEN 0  
								ELSE ft.cobrado END  
		AND ft.etotal != CASE WHEN @excluirZero = 1   THEN 0  ELSE -99999 END
		AND (ft2.u_codigo != CASE WHEN @cPlano = 1    THEN '' ELSE '^[[:space:]]*$' END 
			or ft2.u_codigo2 != CASE WHEN @cPlano = 1 THEN '' ELSE '^[[:space:]]*$' END)
		AND ft.site = @site
		AND (ft.nmdoc IN (SELECT items from dbo.up_splitToTable(@tipodoc,',')) OR @tipodoc='') 
		AND NOT EXISTS(SELECT 1 FROM fi(nolock) AS ncfi INNER JOIN td(nolock) ON td.ndoc = ncfi.ndoc
				 WHERE ncfi.ofistamp = fi.fistamp AND td.tiposaft = 'NC')
		AND 1 = CASE WHEN @creditos = 0 THEN 1 ELSE 
					(CASE WHEN fi.eaquisicao >= fi.qtt THEN 0 ELSE 1 END) END
		AND 1 = CASE WHEN @creditos = 0 THEN 1 ELSE CASE WHEN td.tiposaft = 'FT' THEN 1 ELSE 0 END END
		and b_utentes.inactivo = 0
		) AS x
		WHERE 
			1 = 1 
			--AND (x.nmdoc IN (SELECT items from dbo.up_splitToTable(@tipodoc,',')) OR @tipodoc='') 
			and Round(case when x.ivaincl=0 then x.etiliquido + x.valIva else x.etiliquido end,2) != CASE WHEN @excluirZero = 1   THEN 0  ELSE -99999 END
			and x.ref != ''
			and (valorNreg != CASE WHEN @creditos = 1 THEN 0 ELSE -999999999 END 
			or 
			qtt > CASE WHEN @creditos = 1 THEN eaquisicao ELSE -9999999 END)
	ORDER BY 
		x.fdata desc, x.fno desc
			 
	OPTION (RECOMPILE)

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#TempSupsOperadorRegParcialProduto'))
		DROP TABLE #TempSupsOperadorRegParcialProduto
	GO
	Grant Execute On dbo.up_touch_reImprimirBulk to Public
	Grant Control On dbo.up_touch_reImprimirBulk to Public
	GO