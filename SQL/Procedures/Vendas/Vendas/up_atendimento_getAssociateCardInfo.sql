/*

	EXEC up_atendimento_getAssociateCardInfo 'ADM1713CB21-CA63-4AA0-A59', '01', 0

*/

if OBJECT_ID('[dbo].[up_atendimento_getAssociateCardInfo]') IS NOT NULL
	drop procedure dbo.up_atendimento_getAssociateCardInfo
go

create procedure dbo.up_atendimento_getAssociateCardInfo
	@token  varchar (36),
	@planoSns VARCHAR(3),
	@manipulado bit 

AS

	SELECT 
		ext_esb_associatecard.*,
		ISNULL(extPlano.codigo, '') AS recomendedPlan,
		ISNULL(cptpla.design, '') as descPlan
	FROM 
		ext_esb_associatecard(nolock)
		left join ext_esb_cptpla(nolock) AS extPlano ON ext_esb_associatecard.planId = extPlano.planId AND ext_esb_associatecard.entityId = extPlano.entityId and extPlano.planoSns = @planoSns
		left join cptpla(nolock) ON ISNULL(extPlano.codigo, '') = cptpla.codigo 
	WHERE
		ext_esb_associatecard.token = @token	
--		and cptpla.manipulado = @manipulado

GO
Grant Execute On dbo.up_atendimento_getAssociateCardInfo to Public
Grant Control On dbo.up_atendimento_getAssociateCardInfo to Public
Go 