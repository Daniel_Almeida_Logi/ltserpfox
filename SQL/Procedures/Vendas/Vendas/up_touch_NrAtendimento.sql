/*  Gera novo numero de atendimento
	
	exec up_touch_NrAtendimento 256,'Loja 1'

	ft
	b_pagcentral

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


IF OBJECT_ID('[dbo].[up_touch_NrAtendimento]') IS NOT NULL
	drop procedure dbo.up_touch_NrAtendimento
GO 

create procedure dbo.up_touch_NrAtendimento
	 @terminal varchar(128)
	,@site varchar(60)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	declare @nratend as varchar (30)
	
	set @nratend = (select isnull(
								(select stamp from b_ultreg (nolock) where tabela = @terminal AND site = @site and YEAR(getdate()) = YEAR(data)
								),'0000000000'))

	-- Novo n� de atendimento = n� atendimento anterior + 1 + terminal utilizador
	-- 
	set @nratend = CAST(@nratend + 1 as varchar(30)) + CAST(case when LEN(@terminal) < 2 then CAST('0' + @terminal as varchar(30)) else @terminal END as varchar(30))

	set @nratend = SUBSTRING('0000000000',1,10-LEN(@nratend)) + CAST(@nratend as varchar(30))

	select @nratend as nrAtend

GO
Grant Execute On dbo.up_touch_NrAtendimento to Public
Grant Control On dbo.up_touch_NrAtendimento to Public
GO