-- Re-Imprimir --
-- exec up_touch_reImprimir '',55,'',1078961,-1,'20180430','20180430','Factura','','','Loja 1'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_reImprimir]') IS NOT NULL
	drop procedure dbo.up_touch_reImprimir
go

create procedure dbo.up_touch_reImprimir

@produto		varchar(60),
@venda			numeric(10,0),
@cliente		varchar(55),
@no				numeric(10,0),
@estab			numeric(3),
@dataIni		datetime,
@dataFim		datetime,
@tipodoc		varchar(20),
@receita		varchar(20),
@nratend		varchar(20),
@site			varchar(20)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

declare @sql varchar(max)


select @sql = N'

;with
	cte1 as
	(
		select
			ndoc,u_tipodoc
		from td (nolock)
		where
			nmdoc= case when '''+convert(varchar,@tipodoc)+'''='''' then nmdoc else '''+convert(varchar,@tipodoc)+''' end
			and td.u_tipodoc not in (1,5)
	)
select
	 ft.ftstamp
	,CONVERT(varchar,fdata,102) as fdata
	,ft.ousrhora
	,case when ft.cobrado=1 then ft.nmdoc + '' (S)'' else ft.nmdoc end as nmdoc
	,ft.fno
	,ft.ndoc
	,(case when b_utentes.removido=0 then ft.nome else ''*********'' end) as nome
	,ft.no
	,ft.estab
	,ft2.u_receita
	,ft.etotal
	,ft.u_lote
	,ft.u_lote2
	,ft.u_nslote
	,ft.u_nslote2
	,ft2.u_codigo
	,ft.u_nratend
	,cte1.u_tipodoc
	,isnull(''['' + cptpla.codigo + ''] '' + cptpla.design,'''') as plano
	,case when cptpla.compl != '''' then 1 else 0 end as complement
	,ft.vendnm
	,ft.cobrado
from ft (nolock)
inner join b_utentes (nolock) on b_utentes.no=ft.no and b_utentes.estab=ft.estab'

if @produto != ''
select @sql = @sql + N'
	inner join fi  (nolock) on ft.ftstamp = fi.ftstamp'
select @sql = @sql + N'
	inner join ft2 (nolock) on ft.ftstamp = ft2.ft2stamp 
	inner join cte1  on ft.ndoc = cte1.ndoc
	left join cptpla (nolock) on cptpla.codigo = ft2.u_codigo
where 
	ft.anulado = 0
	and ft.ndoc != 86
	and ft.site = '''+@site+''''
if @produto != ''
select @sql = @sql + N'
	AND (fi.ref like '''+convert(varchar,@produto)+'%'' OR fi.design like '''+convert(varchar,@produto)+'%'')'
if @cliente != ''
select @sql = @sql + N'
	AND (ft.nome like '''+convert(varchar,@cliente)+'%'')'
if @receita != ''
select @sql = @sql + N'
	AND  ft2.u_receita like '''+convert(varchar,@receita)+'%'''
if @venda != 0
select @sql = @sql + N'
	AND ft.fno = '+convert(varchar,@venda)
if @no != 0
select @sql = @sql + N'
	AND ft.no = '+convert(varchar,@no)
if @estab != -1
select @sql = @sql + N'
	AND ft.estab = '+convert(varchar,@estab)
if @nratend != '' and @nratend != '0'
select @sql = @sql + N'
	AND ft.u_nratend = '''+ltrim(@nratend)+''''
select @sql = @sql + N'
	AND (ft.fdata between '''+convert(varchar,@dataini)+''' and '''+convert(varchar,@dataFim)+''')
group by ft.ndoc, ft.fno, ft.ftstamp, ft.fdata, ft.ousrhora, ft.cobrado, ft.nmdoc, ft.nome, ft.no,
	ft2.u_receita, ft.etotal, ft.estab, ft.u_lote, ft.u_lote2, ft.u_nslote, ft.u_nslote2, ft2.u_codigo,
	ft.ndoc, ft.u_nratend,cte1.u_tipodoc, cptpla.codigo, cptpla.design, cptpla.compl, ft.vendnm, b_utentes.removido, ft.cobrado

union all 

select
	 ''ftstamp'' = re.restamp
	,CONVERT(varchar,re.rdata,102) as fdata
	,re.ousrhora
	,''nmdoc'' = case when re.nmdoc = ''Normal'' then ''Recibo '' + re.nmdoc else re.nmdoc end
	,''fno'' = re.rno
	,re.ndoc
	,(case when b_utentes.removido=0 then re.nome else ''*********'' end) as nome
	,re.no
	,re.estab
	,''u_receita'' = ''''
	,re.etotal
	,''u_lote'' = 0
	,''u_lote2'' = 0
	,''u_nslote'' = 0
	,''u_nslote2'' = 0
	,''u_codigo'' = ''''
	,re.u_nratend
	,''u_tipodoc'' = 0
	,''plano'' = ''''
	,''complement'' = ''''
	,re.vendnm
	,cast(0 as bit) as cobrado
from re (nolock)
inner join b_utentes (nolock) on b_utentes.no=re.no and b_utentes.estab=re.estab
where 
	ndoc = 1
	AND re.site = '''+@site+''''
if @produto != ''
select @sql = @sql + N'
	AND 1=0'
if @cliente != ''
select @sql = @sql + N'
	AND (re.nome like '''+convert(varchar,@cliente)+'%'')'
if @receita != ''
select @sql = @sql + N'
	AND  1=0'
if @venda != 0
select @sql = @sql + N'
	AND re.rno = '+convert(varchar,@venda)
if @no != 0
select @sql = @sql + N'
	AND re.no = '+convert(varchar,@no)
if @estab != -1
select @sql = @sql + N'
	AND re.estab = '+convert(varchar,@estab)
if @nratend != '' and @nratend != '0'
select @sql = @sql + N'
	AND re.u_nratend = '''+@nratend+''''
if @tipodoc != '' AND @tipodoc != 'Recibo Normal'
select @sql = @sql + N'
	AND 1=0'
select @sql = @sql + N'
	AND (re.rdata between '''+convert(varchar,@dataini)+''' and '''+convert(varchar,@dataFim)+''')

order by fdata desc,ousrhora desc, fno desc'
	
print @sql
execute (@sql)

GO
Grant Execute On dbo.up_touch_reImprimir to Public
grant control On dbo.up_touch_reImprimir to Public
go