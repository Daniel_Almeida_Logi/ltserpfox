/*

	obtem campos da FT
	
	 exec up_print_fi 'ADM28EF3C23-9318-46FD-8DB','21188115648ERB ',''
	  exec up_print_fi 'ADM28EF3C23-9318-46FD-8DB',' ',''

	select * from fi
	select * from dplms
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_print_fi]') IS NOT NULL
	drop procedure dbo.up_print_fi
go

create procedure dbo.up_print_fi

	@stamp varchar(30)
	,@nratend varchar(20)
	,@suspensas varchar(30)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

declare @sql varchar(max)
select @sql = N'

	DECLARE @pais	VARCHAR(36)

	SELECT top 1 @pais = pais from empresa

	select
		fi.*
		,pvptop4 = isnull(fprod.preco_acordo,0.00)
		--,diploma_id = isnull(dplms.diploma_id,0)
		, motisencao = case when upper(@pais)<>''PORTUGAL'' then isnull((select isnull((select campo from b_multidata where b_multidata.multidatastamp=taxasiva.multidataStamp),'''') from taxasiva where taxasiva.codigo=fi.tabiva and fi.iva=0),'''')
						else isnull((select motiseimp from fi2 (nolock) where fi2.fistamp=fi.fistamp ),'''')
						end 
		, nrdocfiscal = left(isnull(b_cert.invoiceType,''FT'') + '' '' + convert(varchar,ft.ndoc) + ''/'' + CONVERT(varchar,ft.fno),60)
		, parte1 = (SUBSTRING (hash,1,1) + SUBSTRING (hash,11,1) + SUBSTRING (hash,21,1) + SUBSTRING (hash,31,1))
		, parte2 = Convert(varchar(254),''Processado por programa certificado n.'' + (isnull((select textvalue from B_Parameters (nolock) where stamp=''ADM0000000078''),'''')) + case when isnull(ft.nmdoc,'''') = ''Venda Manual''
																	   then '' - Copia do documento original-'' + Rtrim(versaochave) + ''-'' + Rtrim(invoicetype) + ''M''
																	   else ''''
																	   end)
		, ft2.u_docorig
		,etotal = cast(ft.etotal as numeric(15,2))
		,laststamp = (select top 1 ftt.ftstamp from fi fii(nolock) inner join ft ftt (nolock) on ftt.ftstamp=fii.ftstamp where ftt.u_nratend=ft.u_nratend order by ftt.ftstamp asc)
		,ivatx1 = cast(ft.ivatx1 as numeric(15,0))
		,ivatx2 = cast(ft.ivatx2 as numeric(15,0))
		,ivatx3 = cast(ft.ivatx3 as numeric(15,0))
		,ivatx4 = cast(ft.ivatx4 as numeric(15,0))
		,ivatx5 = cast(ft.ivatx5 as numeric(15,0))
		,ivatx6 = cast(ft.ivatx6 as numeric(15,0))
		,ivatx7 = cast(ft.ivatx7 as numeric(15,0))
		,ivatx8 = cast(ft.ivatx8 as numeric(15,0))
		,ivatx9 = cast(ft.ivatx9 as numeric(15,0))
		,ivatx10 = cast(ft.ivatx10 as numeric(15,0))
		,ivatx11 = cast(ft.ivatx11 as numeric(15,0))
		,ivatx12 = cast(ft.ivatx12 as numeric(15,0))
		,ivatx13 = cast(ft.ivatx13 as numeric(15,2))
		,eivain1 = cast(ft.eivain1 as numeric(15,2))
		,eivain2 = cast(ft.eivain2 as numeric(15,2))
		,eivain3 = cast(ft.eivain3 as numeric(15,2))
		,eivain4 = cast(ft.eivain4 as numeric(15,2))
		,eivain5 = cast(ft.eivain5 as numeric(15,2))
		,eivain6 = cast(ft.eivain6 as numeric(15,2))
		,eivain7 = cast(ft.eivain7 as numeric(15,2))
		,eivain8 = cast(ft.eivain8 as numeric(15,2))
		,eivain9 = cast(ft.eivain9 as numeric(15,2))
		,eivain10 = cast(ft.eivain10 as numeric(15,2))
		,eivain11 = cast(ft.eivain11 as numeric(15,2))
		,eivain12 = cast(ft.eivain12 as numeric(15,2))
		,eivain13 = cast(ft.eivain13 as numeric(15,2))
		,eivav1 = cast(ft.eivav1 as numeric(15,2))
		,eivav2 = cast(ft.eivav2 as numeric(15,2))
		,eivav3 = cast(ft.eivav3 as numeric(15,2))
		,eivav4 = cast(ft.eivav4 as numeric(15,2))
		,eivav5 = cast(ft.eivav5 as numeric(15,2))
		,eivav6 = cast(ft.eivav6 as numeric(15,2))
		,eivav7 = cast(ft.eivav7 as numeric(15,2))
		,eivav8 = cast(ft.eivav8 as numeric(15,2))
		,eivav9 = cast(ft.eivav9 as numeric(15,2))
		,eivav10 = cast(ft.eivav10 as numeric(15,2))
		,eivav11 = cast(ft.eivav11 as numeric(15,2))
		,eivav12 = cast(ft.eivav12 as numeric(15,2))
		,eivav13 = cast(ft.eivav13 as numeric(15,2))
		,motiseimp = (select motiseimp from fi2 (nolock) where fi2.fistamp=fi.fistamp)

		,(''A:''+empresa.ncont
		+''*B:''+ft.ncont
		+''*C:''+(case when b_utentes.codigop='''' then ''PT'' else ltrim(rtrim(b_utentes.codigop)) end)
		+''*D:''+td.tiposaft
		+''*E:''+(case when ft.anulado = 0 then ''N'' else ''A'' end)
		+''*F:''+convert(varchar, ft.fdata, 112)
		+''*G:''+left(isnull((select invoiceType from b_cert bce where ft.ftstamp=bce.stamp),''FT'') + '' '' + convert(varchar,ft.ndoc) + ''/'' + CONVERT(varchar,ft.fno),60)
		+''*H:''+''0''
		+''*I1:''+(select top 1 descricao from regiva (nolock))
		+''*I2:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''ISE'' then eivain1 else 0 end)
											+(case when (select codigo from regiva where tabiva=2)=''ISE'' then eivain2 else 0 end)
											+(case when (select codigo from regiva where tabiva=3)=''ISE'' then eivain3 else 0 end)
											+(case when (select codigo from regiva where tabiva=4)=''ISE'' then eivain4 else 0 end)
											+(case when (select codigo from regiva where tabiva=5)=''ISE'' then eivain5 else 0 end)
											+(case when (select codigo from regiva where tabiva=6)=''ISE'' then eivain6 else 0 end)
											+(case when (select codigo from regiva where tabiva=7)=''ISE'' then eivain7 else 0 end)
											+(case when (select codigo from regiva where tabiva=8)=''ISE'' then eivain8 else 0 end)
											+(case when (select codigo from regiva where tabiva=9)=''ISE'' then eivain9 else 0 end)
											+(case when (select codigo from regiva where tabiva=10)=''ISE'' then eivain10 else 0 end)
											+(case when (select codigo from regiva where tabiva=11)=''ISE'' then eivain11 else 0 end)
											+(case when (select codigo from regiva where tabiva=12)=''ISE'' then eivain12 else 0 end)
											+(case when (select codigo from regiva where tabiva=13)=''ISE'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
		+''*I3:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''RED'' then eivain1 else 0 end)
											+(case when (select codigo from regiva where tabiva=2)=''RED'' then eivain2 else 0 end)
											+(case when (select codigo from regiva where tabiva=3)=''RED'' then eivain3 else 0 end)
											+(case when (select codigo from regiva where tabiva=4)=''RED'' then eivain4 else 0 end)
											+(case when (select codigo from regiva where tabiva=5)=''RED'' then eivain5 else 0 end)
											+(case when (select codigo from regiva where tabiva=6)=''RED'' then eivain6 else 0 end)
											+(case when (select codigo from regiva where tabiva=7)=''RED'' then eivain7 else 0 end)
											+(case when (select codigo from regiva where tabiva=8)=''RED'' then eivain8 else 0 end)
											+(case when (select codigo from regiva where tabiva=9)=''RED'' then eivain9 else 0 end)
											+(case when (select codigo from regiva where tabiva=10)=''RED'' then eivain10 else 0 end)
											+(case when (select codigo from regiva where tabiva=11)=''RED'' then eivain11 else 0 end)
											+(case when (select codigo from regiva where tabiva=12)=''RED'' then eivain12 else 0 end)
											+(case when (select codigo from regiva where tabiva=13)=''RED'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
		+''*I4:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''RED'' then eivav1 else 0 end)
											+(case when (select codigo from regiva where tabiva=2)=''RED'' then eivav2 else 0 end)
											+(case when (select codigo from regiva where tabiva=3)=''RED'' then eivav3 else 0 end)
											+(case when (select codigo from regiva where tabiva=4)=''RED'' then eivav4 else 0 end)
											+(case when (select codigo from regiva where tabiva=5)=''RED'' then eivav5 else 0 end)
											+(case when (select codigo from regiva where tabiva=6)=''RED'' then eivav6 else 0 end)
											+(case when (select codigo from regiva where tabiva=7)=''RED'' then eivav7 else 0 end)
											+(case when (select codigo from regiva where tabiva=8)=''RED'' then eivav8 else 0 end)
											+(case when (select codigo from regiva where tabiva=9)=''RED'' then eivav9 else 0 end)
											+(case when (select codigo from regiva where tabiva=10)=''RED'' then eivav10 else 0 end)
											+(case when (select codigo from regiva where tabiva=11)=''RED'' then eivav11 else 0 end)
											+(case when (select codigo from regiva where tabiva=12)=''RED'' then eivav12 else 0 end)
											+(case when (select codigo from regiva where tabiva=13)=''RED'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))
		+''*I5:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''INT'' then eivain1 else 0 end)
											+(case when (select codigo from regiva where tabiva=2)=''INT'' then eivain2 else 0 end)
											+(case when (select codigo from regiva where tabiva=3)=''INT'' then eivain3 else 0 end)
											+(case when (select codigo from regiva where tabiva=4)=''INT'' then eivain4 else 0 end)
											+(case when (select codigo from regiva where tabiva=5)=''INT'' then eivain5 else 0 end)
											+(case when (select codigo from regiva where tabiva=6)=''INT'' then eivain6 else 0 end)
											+(case when (select codigo from regiva where tabiva=7)=''INT'' then eivain7 else 0 end)
											+(case when (select codigo from regiva where tabiva=8)=''INT'' then eivain8 else 0 end)
											+(case when (select codigo from regiva where tabiva=9)=''INT'' then eivain9 else 0 end)
											+(case when (select codigo from regiva where tabiva=10)=''INT'' then eivain10 else 0 end)
											+(case when (select codigo from regiva where tabiva=11)=''INT'' then eivain11 else 0 end)
											+(case when (select codigo from regiva where tabiva=12)=''INT'' then eivain12 else 0 end)
											+(case when (select codigo from regiva where tabiva=13)=''INT'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
		+''*I6:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''INT'' then eivav1 else 0 end)
											+(case when (select codigo from regiva where tabiva=2)=''INT'' then eivav2 else 0 end)
											+(case when (select codigo from regiva where tabiva=3)=''INT'' then eivav3 else 0 end)
											+(case when (select codigo from regiva where tabiva=4)=''INT'' then eivav4 else 0 end)
											+(case when (select codigo from regiva where tabiva=5)=''INT'' then eivav5 else 0 end)
											+(case when (select codigo from regiva where tabiva=6)=''INT'' then eivav6 else 0 end)
											+(case when (select codigo from regiva where tabiva=7)=''INT'' then eivav7 else 0 end)
											+(case when (select codigo from regiva where tabiva=8)=''INT'' then eivav8 else 0 end)
											+(case when (select codigo from regiva where tabiva=9)=''INT'' then eivav9 else 0 end)
											+(case when (select codigo from regiva where tabiva=10)=''INT'' then eivav10 else 0 end)
											+(case when (select codigo from regiva where tabiva=11)=''INT'' then eivav11 else 0 end)
											+(case when (select codigo from regiva where tabiva=12)=''INT'' then eivav12 else 0 end)
											+(case when (select codigo from regiva where tabiva=13)=''INT'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))
		+''*I7:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''NOR'' then eivain1 else 0 end)
											+(case when (select codigo from regiva where tabiva=2)=''NOR'' then eivain2 else 0 end)
											+(case when (select codigo from regiva where tabiva=3)=''NOR'' then eivain3 else 0 end)
											+(case when (select codigo from regiva where tabiva=4)=''NOR'' then eivain4 else 0 end)
											+(case when (select codigo from regiva where tabiva=5)=''NOR'' then eivain5 else 0 end)
											+(case when (select codigo from regiva where tabiva=6)=''NOR'' then eivain6 else 0 end)
											+(case when (select codigo from regiva where tabiva=7)=''NOR'' then eivain7 else 0 end)
											+(case when (select codigo from regiva where tabiva=8)=''NOR'' then eivain8 else 0 end)
											+(case when (select codigo from regiva where tabiva=9)=''NOR'' then eivain9 else 0 end)
											+(case when (select codigo from regiva where tabiva=10)=''NOR'' then eivain10 else 0 end)
											+(case when (select codigo from regiva where tabiva=11)=''NOR'' then eivain11 else 0 end)
											+(case when (select codigo from regiva where tabiva=12)=''NOR'' then eivain12 else 0 end)
											+(case when (select codigo from regiva where tabiva=13)=''NOR'' then eivain13 else 0 end)) as numeric(15,2)) as varchar(30))
		+''*I8:''+ cast(cast(abs((case when (select codigo from regiva where tabiva=1)=''NOR'' then eivav1 else 0 end)
											+(case when (select codigo from regiva where tabiva=2)=''NOR'' then eivav2 else 0 end)
											+(case when (select codigo from regiva where tabiva=3)=''NOR'' then eivav3 else 0 end)
											+(case when (select codigo from regiva where tabiva=4)=''NOR'' then eivav4 else 0 end)
											+(case when (select codigo from regiva where tabiva=5)=''NOR'' then eivav5 else 0 end)
											+(case when (select codigo from regiva where tabiva=6)=''NOR'' then eivav6 else 0 end)
											+(case when (select codigo from regiva where tabiva=7)=''NOR'' then eivav7 else 0 end)
											+(case when (select codigo from regiva where tabiva=8)=''NOR'' then eivav8 else 0 end)
											+(case when (select codigo from regiva where tabiva=9)=''NOR'' then eivav9 else 0 end)
											+(case when (select codigo from regiva where tabiva=10)=''NOR'' then eivav10 else 0 end)
											+(case when (select codigo from regiva where tabiva=11)=''NOR'' then eivav11 else 0 end)
											+(case when (select codigo from regiva where tabiva=12)=''NOR'' then eivav12 else 0 end)
											+(case when (select codigo from regiva where tabiva=13)=''NOR'' then eivav13 else 0 end)) as numeric(15,2)) as varchar(30))

		+''*N:''+ cast(cast(abs(ft.ettiva) as numeric(15,2)) as varchar(30))
		+''*O:''+ cast(cast(abs(ft.etotal) as numeric(15,2)) as varchar(30))
		+''*Q:''+ (SUBSTRING (hash,1,1) + SUBSTRING (hash,11,1) + SUBSTRING (hash,21,1) + SUBSTRING (hash,31,1))
		+''*R:''+ (select top 1 left(textValue,4) from B_Parameters (nolock) where stamp=''ADM0000000078'')) as codlin
		
	from 
		fi (nolock)
		inner join ft (nolock) on fi.ftstamp=ft.ftstamp
		inner join ft2 (nolock) on ft2.ft2stamp=ft.ftstamp
		inner join td (nolock) on td.ndoc = ft.ndoc 
		inner join b_cert (nolock) on b_cert.stamp=ft.ftstamp
		left join fprod (nolock) on fprod.cnp = fi.ref
		inner join empresa (nolock) on empresa.site=ft.site
		inner join b_utentes (nolock) on b_utentes.no = ft.no and b_utentes.estab = ft.estab
	where 
		fi.ftstamp = '''+@stamp+''''

	if @nratend != ''
select @sql = @sql + N'
		or ft.u_nratend = '''+@nratend+''''

select @sql = @sql + N'		
	order by
		ftstamp desc, lordem asc'
--exec up_print_fi 'ADM042C976C-40A4-43E6-A0B','21189151904FFX',''
print @sql
execute (@sql)

GO
Grant Execute On dbo.up_print_fi to Public
Grant Control On dbo.up_print_fi to Public
GO
