/* Devolver texto a contatenar Cart�o Medis

 exec up_atendimento_devolveTexto_Medis 'ADMC9DC9195-3B1A-4E39-B70'
*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_atendimento_devolveTexto_Medis]') IS NOT NULL
	drop procedure dbo.up_atendimento_devolveTexto_Medis
go

create procedure dbo.up_atendimento_devolveTexto_Medis
	@token varchar(36)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	SELECT obs as texto FROM ext_esb_associatecard(nolock) WHERE token = @token	

GO
Grant Execute on dbo.up_atendimento_devolveTexto_Medis to Public
Grant Control on dbo.up_atendimento_devolveTexto_Medis to Public
GO