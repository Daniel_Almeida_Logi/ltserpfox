/*

	obtem campos da FT
	
	 exec up_print_qrcode_a4_docs_totais 'ADMF752C683-2E12-42F9-AC0'

	select * from fi
	select * from dplms
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_print_qrcode_a4_docs_totais]') IS NOT NULL
	drop procedure dbo.up_print_qrcode_a4_docs_totais
go

create procedure dbo.up_print_qrcode_a4_docs_totais

	@stamp varchar(30)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

If OBJECT_ID('tempdb.dbo.#iva') IS NOT NULL
		DROP TABLE #iva

declare @TIPOSAFT VARCHAR(2)

SET @TIPOSAFT = (SELECT tiposaft FROM td(nolock) WHERE ndoc = (SELECT ndoc FROM ft(nolock) WHERE ftstamp = @stamp))

declare @sql varchar(max)
select @sql = N'

	DECLARE @pais	VARCHAR(36)

	SELECT top 1 @pais = pais from empresa

	select
		(''A:''+ltrim(rtrim(empresa.ncont))
		+''*B:''+ltrim(rtrim(bo.ncont))
		+''*C:''+''PT''
		+''*D:''+ ''GT''
		+''*E:''+ ''N''
		+''*F:''+convert(varchar, bo.dataobra, 112)
		+''*G:''+left(isnull(b_cert.invoiceType,case when bo.ndos = 7 then ''GA'' else ''GD'' end) + '' '' + convert(varchar,bo.ndos) + right(convert(varchar,bo.boano),2) + ''/'' + CONVERT(varchar,bo.obrano),60)
		+''*H:''+(CASE WHEN ts.ATCUD <> '''' THEN LTRIM(RTRIM(ts.atcud)) ELSE ''0'' END) + ''-'' + LTRIM(RTRIM(bo.obrano))
		+''*I1:''+(select top 1 descricao from regiva (nolock))'

select @sql = @sql + N'	
		+''*N:''+ cast(cast(abs(bo2.ETOTIVA) as numeric(15,2)) as varchar(30))
		+''*O:''+ cast(cast(abs(bo.etotal) as numeric(15,2)) as varchar(30))
		+''*Q:''+ ''XXXX''
		+''*R:''+ (select top 1 left(textValue,charindex(''/'',textValue)-1) from B_Parameters (nolock) where stamp=''ADM0000000078'')) as qrcode
		
	from 
		bo (nolock) 
		inner join bo2 (nolock) on bo.bostamp=bo2.bo2stamp
		left join b_cert (nolock) on b_cert.stamp=bo.bostamp
		inner join empresa (nolock) on empresa.site=bo.site
		inner join ts(nolock) on ts.ndos = bo.ndos 
		--inner join b_utentes (nolock) on b_utentes.no = bo.no and b_utentes.estab = bo.estab
	where 
		bo.bostamp = '''+@stamp+''''

-- exec up_print_qrcode_a4_docs_totais 'ADMF3C3644B-EA73-4B6B-800'
--select * from bo where bostamp='ADMF752C683-2E12-42F9-AC0 '
--select * from bo2 where bo2stamp='ADMF752C683-2E12-42F9-AC0 '

print @sql
execute (@sql)

GO
Grant Execute On dbo.up_print_qrcode_a4_docs_totais to Public
Grant Control On dbo.up_print_qrcode_a4_docs_totais to Public
GO
