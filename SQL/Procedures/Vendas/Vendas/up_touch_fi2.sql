/*

	exec up_touch_fi2 'soD7DF1B51-0080-4498-869', ''
	exec up_touch_fi2 '', 'so4D86AB52-8CCB-4DFB-AF9'
		exec up_touch_fi2 '', ''

	
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_touch_fi2]') IS NOT NULL
	drop procedure dbo.up_touch_fi2
go

create procedure dbo.up_touch_fi2

	@ftstamp char(25)
	,@fistamp char(25)
	

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	Declare @sql varchar(max)


	set @sql = N' 
	select
		*
	FROM
		fi2(nolock)
	WHERE' 

	if(@ftstamp !='')
	begin
		set  @sql +=N' fi2.ftstamp = ''' + @ftstamp +''''
	end
	else
	begin 
		set  @sql +=N' fi2.fistamp = ''' + @fistamp +''''
	end

		print @sql


		execute (@sql)

GO
Grant Execute On dbo.up_touch_fi2 to Public
Grant Control On dbo.up_touch_fi2 to Public
GO
