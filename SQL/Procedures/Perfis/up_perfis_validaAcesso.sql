/* Verifica se um utilizador tem acesso a um perfil --

	exec up_perfis_validaAcesso '', 'Loja 1'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_perfis_validaAcesso]') IS NOT NULL
	drop procedure dbo.up_perfis_validaAcesso
go

create procedure dbo.up_perfis_validaAcesso
	@resumo varchar(60)
	,@site varchar(20)

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON


select
	pf.resumo,
	pfustamp = isnull(pfu.pfustamp,''),
	pfgstamp = isnull(pfg.pfgstamp,''),
	pf.pfstamp,
	username = isnull(pfu.username,''), userno = isnull(pfu.userno,0),
	grupo = ISNULL(pfg.nome,'')
from
	b_pf pf (nolock)
	left join b_pfu pfu (nolock) on pfu.pfstamp=pf.pfstamp
	left join b_pfg pfg (nolock) on pfg.pfstamp=pf.pfstamp
where
	pf.resumo=@resumo
	and (pfu.site = @site or pfg.site = @site )

GO
Grant Execute on dbo.up_perfis_validaAcesso to Public
grant control on dbo.up_perfis_validaAcesso to Public
go
