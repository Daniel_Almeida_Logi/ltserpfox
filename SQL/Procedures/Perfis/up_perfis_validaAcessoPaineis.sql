/*  Verifica se um utilizador/grupo tem acesso a um painel --

	exec up_perfis_validaAcessoPaineis 'Loja 1'
	
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_perfis_validaAcessoPaineis]') IS NOT NULL
	drop procedure dbo.up_perfis_validaAcessoPaineis
go

create procedure dbo.up_perfis_validaAcessoPaineis
	@site   varchar(20)

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

	SELECT
		grupo = ''
		,pfu.userno
		,pf.pfstamp
		,pf.codigo
		,pf.resumo
		,pf.descricao
		,pf.ousrinis
		,pf.ousrdata
		,pf.ousrhora
		,pf.usrinis
		,pf.usrdata
		,pf.usrhora
		,pf.grupo
		,pf.tipo
		,pf.visivel
		,pfu.site
	FROM	
		b_pfu pfu (nolock)
		full join b_pf pf (nolock) on pfu.pfstamp = pf.pfstamp
	WHERE 	
		pfu.site = @site

	union all

	SELECT
		grupo = pfg.nome
		,userno = 0
		,pf.pfstamp
		,pf.codigo
		,pf.resumo
		,pf.descricao
		,pf.ousrinis
		,pf.ousrdata
		,pf.ousrhora
		,pf.usrinis
		,pf.usrdata
		,pf.usrhora
		,pf.grupo
		,pf.tipo
		,pf.visivel
		,pfg.site
		
	FROM	
		b_pfg pfg (nolock)
		full join b_pf pf (nolock) on pfg.pfstamp = pf.pfstamp
	WHERE 	
		pfg.site = @site
	order by 
		resumo
			
GO
Grant Execute on dbo.up_perfis_validaAcessoPaineis to Public
Grant control on dbo.up_perfis_validaAcessoPaineis to Public
GO



