/*
	
	exec up_perfis_Carrega '', '', 0, '52', '', 'Loja 1'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_perfis_Carrega]') IS NOT NULL
	drop procedure dbo.up_perfis_Carrega
GO

create procedure dbo.up_perfis_Carrega
@resumo		varchar(50),
@grupo		varchar(20),
@sel		bit,
@usno		numeric(5),
@gp			varchar(50),
@site		varchar(20)

/* WITH ENCRYPTION */
AS 
begin 

	;with ctePerfis as (
	select
		escolha = 
			Case When (Select COUNT(*) as valor From b_pfu pfu  Where pfu.pfstamp = pf.pfstamp And userno = @usno) != 0
				Or (Select COUNT(*) as valor From b_pfg pfg Where pfg.pfstamp = pf.pfstamp And pfg.nome = @gp) != 0
				 Then convert(bit,1)
				 Else convert(bit,0) 
			End 
		,   
		pf.resumo
		,tipo
		,descricao
		,grupo
		,pf.pfstamp
		
	from 
		b_pf pf (nolock)
	Where 
		resumo like '%'+@resumo+'%'
		And grupo = case when @grupo = '' then grupo else @grupo end
		
	)


	Select 
		* 
	from	
		ctePerfis
	Where 
		escolha = case when @sel = 0 then escolha else 1 end

END
GO

Grant Execute On dbo.up_perfis_Carrega to Public
Grant control On dbo.up_perfis_Carrega to Public
GO