/* Verificar quais documentos o utilizador importa por tido de documento
	
	exec up_perfis_validaAcessoDocs_Importados 1, 'Administrador', 'Visualizar', 'FO,BO', 0, 'Loja 1', 'V/Factura Med.', 'FO'
	exec up_perfis_validaAcessoDocs_Importados '1', 'Administrador', 'Visualizar', 'BO,FT', 0, 'Loja 1', 'Factura', 'FT'
	exec up_perfis_validaAcessoDocs_Importados 1, 'Administrador', 'Visualizar', 'FO,BO,FT', 0, 'Loja 1', 'V/Guia Transp.', 'FO'
	exec up_perfis_validaAcessoDocs_Importados 1, 'Administrador', 'Visualizar', 'FO,BO,FT', 0, 'Loja 1', 'Encomenda a Fornecedor', 'BO'
	exec up_perfis_validaAcessoDocs_Importados 1, 'Administrador', 'Visualizar', 'FO,BO,FT', 0, 'Loja 1', 'Reserva de Cliente', 'BO'

	exec up_perfis_validaAcessoDocs_Importados 1, 'Administrador', 'Visualizar', 'FO,BO,FT', 0, 'Loja 2', 'V/Nt. Cr�dito', 'FO'


		exec up_perfis_validaAcessoDocs_Importados '56', 'Operadores', 'Visualizar', 'BO,FT,FO', 0, 'Loja 1', 'N.D�bito SNS Comp', 'FT'
*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_perfis_validaAcessoDocs_Importados]') IS NOT NULL
	drop procedure dbo.up_perfis_validaAcessoDocs_Importados
go

create procedure dbo.up_perfis_validaAcessoDocs_Importados			
@userno int,
@grupo	varchar(60),
@resumo	varchar(60),
@tipo	varchar(60),
@tabela bit,
@site varchar(20)= '',
@docImp varchar(60),
@docImpTipo varchar(60)

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

DECLARE @sql VARCHAR(MAX) = ''
DECLARE @ini varchar(3) = (select iniciais from b_us(nolock) where userno = @userno)
DECLARE @periodo int = 365
DECLARE @docNum numeric(6) = (
						SELECT 
							(CASE
								WHEN @docImpTipo = 'BO' 
									THEN (select top 1  ndos from ts(nolock) where nmdos = @docImp)
								WHEN @docImpTipo = 'FO'
									THEN (select top 1  cm from cm1(nolock) where cmdesc = @docImp)
								WHEN @docImpTipo = 'FT'
									THEN (select top 1 ndoc from td(nolock) where nmdoc = @docImp and site = @site)
								ELSE
									0
							END)
					  )

IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#upDocsImportados'))
	DROP TABLE #upDocsImportados



IF EXISTS(SELECT 1 FROM ligDocsImp(nolock) where tabela = @docImpTipo AND numDoc = @docNum)
begin
	SELECT
		
		nomedoc
		,ndoc
		,resumo		= RTRIM(LTRIM(NOMEDOC)) +' - ' + @resumo
		,bdempresas
		,tabela
		,u_integra
		,u_copref 
		,site
		
	INTO #upDocsImportados
	FROM
	(
		SELECT 
			nomedoc		= ts.NMDOS
			,ndoc		= ts.ndos
			,bdempresas
			,tabela		= 'BO'
			,u_integra	= convert(bit,0)
			,u_copref 
			,site = ''
		FROM
			ligDocsImp(nolock) as ldi
			join ts(nolock) on ts.nmdos in (select nmdos FROM ts(nolock) as tts where tts.ndos = ldi.numDocCopy)
		WHERE
			ldi.tabela = @docImpTipo
			and ldi.numDoc = @docNum
			and ldi.tabelaCopy = 'BO'
			and 1 = (CASE WHEN ts.site = '' THEN 1 ELSE (CASE WHEN ts.site = @site THEN 1 ELSE 0 END) END)
			and 1 = (CASE WHEN site <> '' THEN 1 WHEN EXISTS (SELECT 1 FROM ts(nolock) as tts WHERE tts.nmdos = ts.nmdos and tts.ndos <> ts.ndos and tts.site = @site) THEN 0 ELSE 1 END)

		UNION ALL

		SELECT 
			nomedoc		= cm1.CMDESC
			,ndoc		= cm1.cm
			,bdempresas = 'FL'
			,tabela		= 'FO'
			,u_integra	= convert(bit,0)
			,u_copref 
			,site = ''
		FROM
			ligDocsImp(nolock) as ldi
			join cm1(nolock) on cm1.cm = ldi.numDocCopy
		WHERE
			ldi.tabela = @docImpTipo
			and ldi.numDoc = @docNum
			and ldi.tabelaCopy = 'FO'

		UNION ALL
		
		SELECT 
			nomedoc		= td.nmdoc
			,ndoc		= td.ndoc
			,bdempresas	= 'CL'
			,tabela		= 'FT'
			,u_integra
			,u_copref
			,site
		FROM
			ligDocsImp(nolock) as ldi
			join td(nolock) on td.ndoc = ldi.numDocCopy
		WHERE
			ldi.tabela = @docImpTipo
			and ldi.numDoc = @docNum
			and ldi.tabelaCopy = 'FT'
	) AS a
	ORDER BY
		NOMEDOC ASC

	SELECT 
		*
	FROM
		#upDocsImportados
	WHERE
		(case when LTRIM(@tabela) = 1 then bdempresas else tabela end) in (select * from dbo.up_splitToTable(LTRIM(@tipo),','))
		and dbo.up_PerfilFacturacao(LTRIM(@userno), @grupo,  RTRIM(LTRIM(NOMEDOC)) + ' - ' + @resumo, @site) = 0
	ORDER BY 
		NOMEDOC ASC


end ELSE
	SET @sql = 
	'

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N''tempdb.dbo.#upDocsImportados''))
		DROP TABLE #upDocsImportados

	SELECT DISTINCT
		nomedoc
		,ndoc
		,resumo		= RTRIM(LTRIM(NOMEDOC)) +'' - '' + ''' + @resumo + '''
		,bdempresas
		,tabela
		,u_integra
		,u_copref 
		,site
	INTO #upDocsImportados
	FROM (
		SELECT
			nomedoc		= ts.NMDOS
			,ndoc		= ts.ndos
			,bdempresas
			,tabela		= ''BO''
			,u_integra	= convert(bit,0)
			,u_copref 
			,site = ''''
		FROM
			TS (nolock)
		where 
			ts.nmdos in (
						select 
							distinct bo.nmdos 
						from 
							bi(nolock) 
							join bo(nolock) on bo.bostamp = bi.bostamp
						where 
						' +	
							case
								when @docImpTipo = 'FO'
									then 'bi.bistamp in (select fn.bistamp from fn(nolock) join fo(nolock) on fo.fostamp = fn.fostamp where fn.docnome = ''' + @docImp + ''' and fn.bistamp <> '''' and fo.docdata>GETDATE() - ' + LTRIM(@periodo) + ')'
								when @docImpTipo = 'BO'
									then 'bi.bistamp in (select bbi.obistamp from bi(nolock) as bbi where bbi.nmdos = ''' + @docImp + ''' and bbi.obistamp <> '''' and bbi.dataobra>GETDATE() - ' + LTRIM(@periodo) + ')'
								when @docImpTipo = 'FT'										
									then 'bi.bistamp in (select fi.bistamp from fi(nolock) join ft(nolock) on ft.ftstamp = fi.ftstamp where ft.nmdoc = ''' + @docImp + ''' and fi.bistamp <> '''' and ft.fdata>GETDATE() - ' + LTRIM(@periodo) + ')'
							end
						+ '
						)

		
		
		UNION ALL
		
		SELECT
			nomedoc		= cm1.CMDESC
			,ndoc		= cm1.cm
			,bdempresas = ''FL''
			,tabela		= ''FO''
			,u_integra	= convert(bit,0)
			,u_copref 
			,site = ''''
		FROM
			CM1 (nolock)
		WHERE
			NAOFO = 0
			and cm1.cmdesc in (
							select 
								distinct fo.docnome 
							from 
								fn(nolock) 
								join fo(nolock) on fo.fostamp = fn.fostamp
							where 
								' +
									case
										when @docImpTipo = 'FO'										
											then 'fn.fnstamp in (select ffn.ofnstamp from fn(nolock) as ffn join fo(nolock) as ffo on ffo.fostamp = ffn.fnstamp where ffn.docnome = ''' + @docImp + ''' and ffn.ofnstamp <> '''' and ffo.docdata>GETDATE() - ' + LTRIM(@periodo) + ')'
										when @docImpTipo = 'BO'										
											then 'fn.fnstamp in (select bi2.fnstamp from bi(nolock) join bi2(nolock) on bi2.bi2stamp = bi.bistamp where bi.nmdos = ''' + @docImp + ''' and bi2.fnstamp <> '''' and bi.dataobra>GETDATE() - ' + LTRIM(@periodo) + ')'
										when @docImpTipo = 'FT'																				
											then 'fn.fnstamp in (select fi2.fnstamp from fi(nolock) join ft(nolock) on ft.ftstamp = fi.ftstamp join fi2(nolock) on fi2.fistamp = fi.fistamp where ft.nmdoc = ''' + @docImp + ''' and fi2.fnstamp <> '''' and ft.fdata>GETDATE() - ' + LTRIM(@periodo) + ')'
									end
								+ '													
						)
		union all

		SELECT
			nomedoc		= td.nmdoc
			,ndoc		= td.ndoc
			,bdempresas	= ''CL''
			,tabela		= ''FT''
			,u_integra
			,u_copref
			,site
		FROM
			TD (nolock)
		where
			site = ''' + @site + '''
			and td.nmdoc in (
							select 
								distinct fi.nmdoc
							from 
								fi(nolock) 
								join ft(nolock) on ft.ftstamp = fi.ftstamp
							where 
								' + 
									case
										when @docImpTipo = 'FO'										
											then 'fi.fistamp in (select fn.fistamp from fn(nolock) join fo(nolock) on fo.fostamp = fn.fostamp where fn.docnome = ''' + @docImp + ''' and fn.fistamp <> '''' and fo.docdata>GETDATE() - ' + LTRIM(@periodo) + ')'
										when @docImpTipo = 'BO'										
											then 'fi.fistamp in (select bi2.fistamp from bi(nolock) join bi2(nolock) on bi2.bi2stamp = bi.bistamp where bi.nmdos = ''' + @docImp + ''' and bi2.fistamp <> '''' and bi.dataobra>GETDATE() - ' + LTRIM(@periodo) + ')'
										when @docImpTipo  = 'FT'
											then 'fi.fistamp in (select ffi.ofistamp from fi(nolock) as ffi join ft(nolock) as fft on fft.ftstamp = ffi.ftstamp where ffi.nmdoc = ''' + @docImp + ''' and ffi.ofistamp <> '''' and fft.fdata>GETDATE() - ' + LTRIM(@periodo) + ')'
									end
								+ '														
							)
	) as a

	ORDER BY
		NOMEDOC ASC


		
		SELECT 
			*
		FROM
			#upDocsImportados
		WHERE
			(case when ' + RTRIM(LTRIM(STR(ISNULL(@tabela,0))))  +' = 1 then bdempresas else tabela end) in (select * from dbo.up_splitToTable('''+RTRIM(LTRIM(@tipo))+''','',''))
			and dbo.up_PerfilFacturacao('+RTRIM(LTRIM(STR(ISNULL(@userno,0)))) + ',''' + @grupo +''',RTRIM(LTRIM(NOMEDOC)) + '' - '' + ''' + RTRIM(LTRIM(ISNULL(@resumo,''))) + ''',''' + @site + ''') = 0 
		ORDER BY 
			NOMEDOC ASC
	'

	--print @sql
IF @sql <> ''
	EXEC (@sql)




IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#upDocsImportados'))
	DROP TABLE #upDocsImportados

GO
Grant Execute on dbo.up_perfis_validaAcessoDocs_Importados to Public
Grant control on dbo.up_perfis_validaAcessoDocs_Importados to Public
GO
