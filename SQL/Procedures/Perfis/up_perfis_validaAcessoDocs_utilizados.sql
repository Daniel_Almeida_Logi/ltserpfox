/* Verifica a que documentos um utilizador/grupo tem acesso --
	
	exec up_perfis_validaAcessoDocs_utilizados 1, 'Administrador', 'Introduzir', 'FO,BO', 0, 'Loja 1'
	exec up_perfis_validaAcessoDocs 1, 'Administrador', 'Introduzir', 'FO,BO', 0, 'Loja 1'

	select * from b_us

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_perfis_validaAcessoDocs_utilizados]') IS NOT NULL
	drop procedure dbo.up_perfis_validaAcessoDocs_utilizados
go

create procedure dbo.up_perfis_validaAcessoDocs_utilizados

@userno int,
@grupo	varchar(60),
@resumo	varchar(60),
@tipo	varchar(60),
@tabela bit = 1,
@site varchar(20) = ''

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

SELECT DISTINCT
	nomedoc
	,ndoc
	,resumo		= RTRIM(LTRIM(NOMEDOC)) +' - ' + @resumo
	,bdempresas
	,tabela
	,u_integra
	,u_copref 
	,site
	,inativo
FROM (
	SELECT
		nomedoc		= ts.NMDOS
		,ndoc		= ts.ndos
		,bdempresas
		,tabela		= 'BO'
		,u_integra	= convert(bit,0)
		,u_copref 
		,site = ''
		, inativo = CONVERT(bit,0)
	FROM
		TS (nolock)
	where 
		ts.nmdos in (select distinct nmdos from bo(nolock) where dataobra>getdate()-365)
		and valor2=0
		and 1 = (CASE WHEN site = '' THEN 1 ELSE (CASE WHEN site = @site THEN 1 ELSE 0 END) END)
		and 1 = (CASE WHEN site <> '' THEN 1 WHEN EXISTS (SELECT 1 FROM ts(nolock) as tts WHERE tts.nmdos = ts.nmdos and tts.ndos <> ts.ndos and tts.site = @site) THEN 0 ELSE 1 END)

	UNION ALL
		
	SELECT
		nomedoc		= cm1.CMDESC
		,ndoc		= cm1.cm
		,bdempresas = 'FL'
		,tabela		= 'FO'
		,u_integra	= convert(bit,0)
		,u_copref 
		,site = ''
		, inativo = CONVERT(bit,0)
	FROM
		CM1 (nolock)
	WHERE
		NAOFO = 0
		and cm1.cmdesc in (select distinct docnome from fo(nolock) where docdata>getdate()-365)
	union all

	SELECT
		nomedoc		= td.nmdoc
		,ndoc		= td.ndoc
		,bdempresas	= 'CL'
		,tabela		= 'FT'
		,u_integra
		,u_copref
		,site
		, inativo
	FROM
		TD (nolock)
	where
		site = @site
		and td.nmdoc in (select distinct nmdoc from ft(nolock) where fdata>getdate()-365)
		and copiad=0
	
) a
where
	(case when @tabela = 1 then bdempresas else tabela end) in (select * from dbo.up_splitToTable(@tipo,','))
	and dbo.up_PerfilFacturacao(@userno, @grupo, RTRIM(LTRIM(NOMEDOC)) + ' - ' + @resumo, @site) = 0
ORDER BY
	NOMEDOC ASC

GO
Grant Execute on dbo.up_perfis_validaAcessoDocs_utilizados to Public
Grant control on dbo.up_perfis_validaAcessoDocs_utilizados to Public
GO


