-- exec	up_reservas_abater 'ADM13041641926.843000003 ',1
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_reservas_abater]') IS NOT NULL
	drop procedure dbo.up_reservas_abater
go

CREATE procedure dbo.up_reservas_abater

@stamp	char(25),
@fecha	bit,
@qtt	numeric(14,4)

/* with encryption */

AS
SET NOCOUNT ON

declare @PorFechar int
declare @bostamp char(25)

select 
	 @bostamp	= (	select top 1 
						bostamp 
					from bi(nolock) 
					where 
						bistamp = @stamp 
						and nmdos='Reserva de Cliente'
					)
	 
select
	 @PorFechar	= case when @fecha = 1
						then
							case when (select bi.qtt-bi.qtt2-bi.nopat from bi (nolock) where bi.bistamp=@stamp) > 0
								then 
									1
								else
									(select top 1 
										isnull(ROW_NUMBER() over(order by bistamp),0) 
									from bi (nolock)
									where
										bi.fechada = 0
										/* and bi.qtt-(bi.nopat + bi.qtt2) > 0 */
										and bi.bostamp = @bostamp
										and bi.bistamp != @stamp
									)
								end
						else
							1
						end

/* Fecha linha da BI */
if @fecha = 1 AND (select bi.qtt-bi.qtt2-bi.nopat from bi (nolock) where bi.bistamp=@stamp) = 0
begin
	update bi
	set fechada = 1
	where bistamp = @stamp
end
print @PorFechar
/* Verifica quantas linhas estao por fechar */
if isnull(@PorFechar,0) = 0
begin
	update bo
	set fechada = 1
	where bo.bostamp = @bostamp
	
	update bi 
	set fechada = 1
	where bi.bostamp = @bostamp
end

Go
Grant Execute on dbo.up_reservas_abater to Public
Grant Control on dbo.up_reservas_abater to Public
Go