/*
	Fechar reservas que j� tenham expirado

	exec up_reservas_rececao '', 'Loja 1'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_reservas_rececao]') IS NOT NULL
	drop procedure dbo.up_reservas_rececao
go

create procedure dbo.up_reservas_rececao

@ref char(18)
,@site varchar(20)

/* with encryption */

AS
SET NOCOUNT ON

	select 
		bi.ref
		,qttreserva = bi.qtt
		,bo.no
		,bo.estab
		,bo.nome
		,stampReserva = bo.bostamp
		,stampReservaBI = bi.bistamp
		,qttdistribuida = 0
	from 
		bo
		inner join bi on bo.bostamp = bi.bostamp 
	where 
		bo.ndos = 5 
		and bo.fechada = 0
		and bi.ref  = @ref
	order by 
		bo.dataobra asc, bo.ousrhora asc

GO
Grant Execute on dbo.up_reservas_rececao to Public
GO
Grant Control on dbo.up_reservas_rececao to Public
GO
