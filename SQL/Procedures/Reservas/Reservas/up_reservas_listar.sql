

  /* SP:	SP que lista as reservas no atendimento

	 exec up_reservas_listar -1, -1, 1, 4, ' ''5148317'' '
	 exec up_reservas_listar 215, 0, 1, 1
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO




IF OBJECT_ID('[dbo].[up_reservas_listar]') IS NOT NULL
	drop procedure dbo.up_reservas_listar
GO

CREATE procedure dbo.up_reservas_listar
	 @No		int = -1
	,@Estab		int = -1
	,@armazem	numeric(5,0)
	,@site_nr	tinyint
	,@ref		VARCHAR(max) = ''

/* with encryption */
AS
	SET NOCOUNT ON

	If OBJECT_ID('tempdb.dbo.#dadosSt') IS NOT NULL
	
			drop table #dadosSt;

	create table #dadosSt (ref varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AI)

	/* with ref*/
	declare @sqlSt varchar(max)
	IF(@ref!='')
	begin
		set @sqlSt = 'Insert into  #dadosSt  select distinct ref from   st(nolock) where ref IN (' + @ref + ')'	
		exec (@sqlSt)
	end
	
	declare @site varchar(20)
	set @site = (select site from empresa where no= @site_nr)

--;with
--	cte1(bostamp,row) as
--	(
--		SELECT
--			'bostamp'	=	bo.bostamp
--			,ROW_NUMBER() over(order by bostamp) as 'row'

--		from
--			Bo (nolock)
--		where
--			bo.ndos = 5
--			AND bo.No = case when @no = -1 then bo.No else @no end
--			AND bo.Estab = case when @Estab = -1 then bo.Estab  else @Estab end
--			AND bo.fechada	= 0
--	)
	
	select
		 'Sel'				= convert(bit,0)
		,'Ref'				= bi.ref
		,'Design'			= bi.design
		,'Qtt'				= bi.qtt - bi.qtt2
		,'PVPRES'			= isnull(bi.edebito,0)
		,'PVPACT'			= isnull(st.epv1,0)
		,'fecha'			= CONVERT(bit,0)
		,'valorAdiantado'	= CONVERT(numeric(19,6),0)
		,'bostamp'			= bo.bostamp
		,'bistamp'			= bi.bistamp
		,'obrano'			= bo.obrano
		,'row'				= CONVERT(int,ROW_NUMBER() over(order by bo.bostamp))
		,'fechada'			= bi.fechada
		,'qttres'			= bi.qtt
		,'qttComp'			= bi.nopat
		,'stampAdiantamento' = isnull((select bi2.fistamp from bi2 (nolock) where bi2.bi2stamp = bi.bistamp),'')
		,'stock'			= st.stock
		,'stockArm'			= sa.stock
		,'stockTotal'		= "t:" + CONVERT(VARCHAR,CEILING(st.stock)) + " a:" + CONVERT(VARCHAR, CEILING(sa.stock))
		,'codComp'			= bi.lobs
		,bi.diploma
		,bi.desconto
		,bi.descval
		,'nomeCliente'		= b_utentes.nome
		,'no'				= b_utentes.no
		,'estab'			= b_utentes.estab
		,'tlmvl '			= b_utentes.tlmvl
		,'qttRec'			=  0
		,'sms'				= convert(bit,(select COUNT(*) from b_res_enviar WHERE b_res_enviar.bistamp=bi.bistamp and (tlmvl LIKE '9%' or  tlmvl LIKE '+351%') and LEN(tlmvl)>=9 ))
		,bi.dataobra
		,'clstamp'			= b_utentes.utstamp 
		,'imp'				= convert(bit,(select COUNT(*) from doc_hist_impressoes WHERE doc_hist_impressoes.doc_stamp=bi.bistamp))
		,bi.exepcaoTrocaMed
		,'stockTotalFiltro' = CEILING(st.stock)
	

	from
		Bo (nolock)
		inner join bi (nolock) on bi.bostamp = bo.bostamp
		--inner join bo on cte1.bostamp = bo.bostamp
		inner join st (nolock) on st.ref = bi.ref 
		left join sa (nolock) on st.ref = sa.ref and sa.armazem = @armazem
		left join b_utentes (nolock) on b_utentes.no = bo.no and b_utentes.estab=bo.estab
		
	where
		bo.ndos = 5
		AND bo.No = case when @no = -1 then bo.No else @no end
		AND bo.Estab = case when @Estab = -1 then bo.Estab  else @Estab end
		and 1 = case 
					when @ref='' then 1 
					else case 
							when st.ref in (select ref from #dadosSt) then 1 
							else 0 
							end
					end
		AND bo.fechada	= 0
		AND bi.fechada  = 0
		AND st.site_nr = @site_nr
		AND bo.site = @site
	order by 
		bo.obrano, row
		
		
	


	If OBJECT_ID('tempdb.dbo.#dadosSt') IS NOT NULL
		drop table #dadosSt;
GO

Grant Execute on dbo.up_reservas_listar to Public
Grant Control on dbo.up_reservas_listar to Public
Go