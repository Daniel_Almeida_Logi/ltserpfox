/*
	Fechar reservas que j� tenham expirado

	exec up_reservas_fechar
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_reservas_fechar]') IS NOT NULL
	drop procedure dbo.up_reservas_fechar
go

create procedure dbo.up_reservas_fechar

/* with encryption */

AS
BEGIN

	declare @usaReservas as bit
	set @usaReservas = (select bool from B_Parameters (nolock) Where stamp = 'ADM0000000142')
	if @usaReservas = 1
	begin 
		/*fechar linhas*/
		update bi
		set fechada = 1
		where bostamp in (
			select bostamp 
			from bo (nolock)
			where 
				ndos=5 
				and (datafinal-(select numValue from B_Parameters (nolock) Where stamp = 'ADM0000000149')) < dataobra 
				and fechada=0
				and (select bool from B_Parameters (nolock) Where stamp = 'ADM0000000149') = 1
		)
		/*fechar cabe�alho*/
		update bo
		set fechada = 1
		where bostamp in (
			select bostamp 
			from bo (nolock)
			where 
				ndos=5 
				and (datafinal-(select numValue from B_Parameters (nolock) Where stamp = 'ADM0000000149')) < dataobra 
				and fechada=0
				and (select bool from B_Parameters (nolock) Where stamp = 'ADM0000000149') = 1
		)
	end

END

GO
Grant Execute on dbo.up_reservas_fechar to Public
GO
Grant Control on dbo.up_reservas_fechar to Public
GO