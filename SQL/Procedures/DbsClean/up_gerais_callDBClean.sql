/* Fazer rebuild a uma base de dados
	exec up_gerais_callDBClean 1 
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_gerais_callDBClean]') IS NOT NULL
	drop procedure up_gerais_callDBClean
go

create PROCEDURE up_gerais_callDBClean
	@month int 
	
/* WITH ENCRYPTION */

AS




		-- se me esquecer de colocar alguma bd , coloca automaticamente 
		Insert into dbInfoSchedule (stamp , dbname,serviceId,serviceIdDesc, ousrdata ,dateTimeNext,active)
		SELECT left(NEWID(),36),name ,1,'DBCLEAN',GETDATE(),DATEADD(Day,7,GETDATE()),1
		FROM MASTER.sys.sysdatabases  
			where 
				(
				 name like 'F%'  or name like 'P%'    
				) -- bds que interessam
				and DATABASEPROPERTYEX('master', 'Status')  = 'ONLINE' and version is not null and  name NOT IN
		  (	SELECT dbName
		FROM [DocsLogitools].dbo.dbInfoSchedule )




		DECLARE @Database NVARCHAR(255)    
		DECLARE DatabaseCursorClean CURSOR READ_ONLY FOR  
		SELECT 
		TOP 10 
		dbName  
		FROM [DocsLogitools].dbo.dbInfoSchedule
		where dateTimeNext<= GETDATE() and active=1
		ORDER BY dateTimeNext  asc
		OPEN DatabaseCursorClean  
		FETCH NEXT FROM DatabaseCursorClean INTO @Database  
		WHILE @@FETCH_STATUS = 0  
		BEGIN  
			IF DB_ID(@Database) IS NOT NULL
			BEGIN
				Exec up_gerais_runDatabaseClean @Database


				IF((select ISNULL(count(*),0) from [DocsLogitools].dbo.dbInfo where dbName = @Database and CONVERT(date , ousrdata) = CONVERT(date , GETDATE()) AND statusID=1)  =0 AND 
				(select ISNULL(count(*),0) from [DocsLogitools].dbo.dbInfo where dbName = @Database and CONVERT(date , ousrdata) = CONVERT(date , GETDATE()) AND statusID=0)  >0)
				BEGIN
					-- caso corra bem
					UPDATE [DocsLogitools].dbo.dbInfoSchedule  set dateTimeNext = DATEADD(MONTH,@month,dateTimeNext) where dbName = @Database

				END 
				ELSE IF((select ISNULL(count(*),0) from [DocsLogitools].dbo.dbInfo where dbName = @Database and CONVERT(date , ousrdata) = CONVERT(date , GETDATE()) AND statusID=1)  =0 AND 
				(select ISNULL(count(*),0) from [DocsLogitools].dbo.dbInfo where dbName = @Database and CONVERT(date , ousrdata) = CONVERT(date , GETDATE()) AND statusID=0)  =0)
				BEGIN
					-- caso nem sequer tenha conseguido correr 

					IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'dbInfo')
					BEGIN 
						Insert into [DocsLogitools].dbo.dbInfo (stamp,dbName,serviceId,serviceIdDesc,statusID,statusIDDesc,ousrdata)
						Values(LEFT(NEWID(),36),@Database,1,'DBCLEAN',1 ,'Erro a chamar o up_gerais_callDBClean , nem sequer fez o shrink ',GETDATE())
					END 
				END
				ELSE IF((select ISNULL(count(*),0) from [DocsLogitools].dbo.dbInfo where dbName = @Database and CONVERT(date , ousrdata) = CONVERT(date , GETDATE()) AND statusID=1) >0 )
				BEGIN

					-- caso tenha partido

					IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'dbInfo')
					BEGIN 
						Insert into [DocsLogitools].dbo.dbInfo (stamp,dbName,serviceId,serviceIdDesc,statusID,statusIDDesc,ousrdata)
						Values(LEFT(NEWID(),36),@Database,1,'DBCLEAN',1 ,'Erro a chamar o up_gerais_callDBClean , correr� novamente amanha ',GETDATE())
					END 
				END 
			END 
			ELSE
			BEGIN
					-- caso a  bd tenha sido apagada 
				IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'dbInfo')
				BEGIN 
					Insert into [DocsLogitools].dbo.dbInfo (stamp,dbName,serviceId,serviceIdDesc,statusID,statusIDDesc,ousrdata)
					Values(LEFT(NEWID(),36),@Database,1,'DBCLEAN',1 ,'Erro a BD j� n�o existe!!!! ',GETDATE())
				END 
			END
			 FETCH NEXT FROM DatabaseCursorClean INTO @Database  
	    END     
		CLOSE DatabaseCursorClean   
		DEALLOCATE DatabaseCursorClean


GO
Grant Execute On up_gerais_callDBClean to Public
Grant Control On up_gerais_callDBClean to Public
GO








