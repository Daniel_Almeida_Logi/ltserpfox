-- Verifica se a procedure j� existe e a remove
IF OBJECT_ID('sp_gerais_shrinkLogFile', 'P') IS NOT NULL
    DROP PROCEDURE sp_gerais_shrinkLogFile
GO

-- COMO USAR:
-- Para encolher o arquivo de log de um banco de dados, execute a procedure da seguinte forma:
-- EXEC sp_gerais_shrinkLogFile 'NomeDoBancoDeDados';
-- Exemplo: 
-- EXEC sp_gerais_shrinkLogFile 'demo_v15';
--
-- O banco de dados passado como par�metro ter� seu modo de recupera��o alterado para SIMPLE,
-- o arquivo de log ser� encolhido para o tamanho m�nimo poss�vel, e o modo de recupera��o ser�
-- restaurado para FULL ap�s o encolhimento.

-- Cria��o da stored procedure
CREATE PROCEDURE sp_gerais_shrinkLogFile
    @dbName VARCHAR(100)  -- Par�metro para o nome do banco de dados
AS
BEGIN
    -- Declara��o de vari�veis
    DECLARE @sql NVARCHAR(MAX) = ''
    DECLARE @logName VARCHAR(100) = ''

    -- Obter o nome do arquivo de log de transa��es para o banco de dados fornecido
    SELECT @logName = mf.name
    FROM sys.master_files AS mf
    WHERE mf.database_id = DB_ID(@dbName) AND mf.type = 1  -- type = 1 refere-se ao arquivo de log

    -- Verifica se o nome do arquivo de log foi encontrado
    IF @logName IS NULL
    BEGIN
        PRINT 'Erro: O arquivo de log de transa��es n�o foi encontrado para o banco de dados ' + @dbName
        RETURN
    END

    -- Construir a string SQL din�mica para alterar o modo de recupera��o e encolher o arquivo de log
    SET @sql = '
    USE ' + QUOTENAME(@dbName) + ';
    ALTER DATABASE ' + QUOTENAME(@dbName) + ' SET RECOVERY SIMPLE;
    DBCC SHRINKFILE (' + QUOTENAME(@logName) + ', 1);
    ALTER DATABASE ' + QUOTENAME(@dbName) + ' SET RECOVERY FULL;'

    -- Imprimir o SQL gerado para depura��o
    PRINT @sql

    -- Executar a string SQL din�mica
    EXEC sp_executesql @sql
END
GO

-- Conceder permiss�o para executar a stored procedure
GRANT EXECUTE ON sp_gerais_shrinkLogFile TO public
GO


