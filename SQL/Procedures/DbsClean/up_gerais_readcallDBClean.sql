/* Fazer rebuild a uma base de dados
	exec up_gerais_readcallDBClean '20231107'
	exec up_gerais_readcallDBClean '20231108'
	exec up_gerais_readcallDBClean '20231113'
*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


if OBJECT_ID('[dbo].[up_gerais_readcallDBClean]') IS NOT NULL
	drop procedure up_gerais_readcallDBClean
go

create PROCEDURE up_gerais_readcallDBClean
		 @date date 
/* WITH ENCRYPTION */

AS

	IF ((SELECT COUNT(*) FROM dbInfo where convert(date ,ousrdata)= @date)>0 )
	BEGIN
		selecT 
			'<u><b>DATA :</b></u>'	+ CONVERT(VARCHAR(10) ,@date)		AS NameD ,
			''															AS Info
		UNION ALL

		SELECT 
			 '<u><b>' + dbName	+':</b></u>'							AS NameD ,
			 --STRING_AGG(CONVERT(NVARCHAR(max), statusIDDesc), ' <br>')	AS Info
			 STUFF((SELECT  statusIDDesc + ' <br>'
              FROM dbInfo sq
              where convert(date ,sq.ousrdata)= @date and sq.dbName = dbInfo.dbName
              ORDER BY ousrdata
              FOR XML PATH(''),TYPE).value('.','varchar(MAX)'),1,3,'') AS Info
		FROM dbInfo
		where convert(date ,ousrdata)= @date
		group by  dbname
	END
	ELSE
	BEGIN
			selecT 
			'<u><b>DATA :</b></u>'	+ CONVERT(VARCHAR(10) ,@date)		AS NameD ,
			'Hoje n�o correram bds neste servidor'						AS Info
	END


GO
Grant Execute On up_gerais_readcallDBClean to Public
Grant Control On up_gerais_readcallDBClean to Public
GO








