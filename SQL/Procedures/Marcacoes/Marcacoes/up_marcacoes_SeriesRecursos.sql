/* exec up_marcacoes_SeriesRecursos 'ADM2F89690A-9719-477F-BEA' */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_SeriesRecursos]') IS NOT NULL
    drop procedure up_marcacoes_SeriesRecursos
go

create PROCEDURE up_marcacoes_SeriesRecursos
@actstamp varchar(25)



/* WITH ENCRYPTION */
AS

	select 
		 b_seriesRecurso.*
	from 
		b_series
		inner join b_seriesRecurso on b_seriesRecurso.seriestamp = b_series.seriestamp
	where
		b_seriesRecurso.seriestamp = @actstamp
	Order
		by nome, no, duracao
	
GO
Grant Execute On up_marcacoes_SeriesRecursos to Public
Grant Control On up_marcacoes_SeriesRecursos to Public
go