/*
  exec up_marcacoes_listaEspecialidades
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_listaEspecialidades]') IS NOT NULL
	drop procedure up_marcacoes_listaEspecialidades
go

create PROCEDURE up_marcacoes_listaEspecialidades

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON
	
	select 
		especialidade, no = espno
	from 
		b_especialidades (nolock) 
	order by 
		especialidade
	
	
GO
Grant Execute On up_marcacoes_listaEspecialidades to Public
Grant Control On up_marcacoes_listaEspecialidades to Public
go