/*
  exec up_marcacoes_listaEspecialistas
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_listaEspecialistas]') IS NOT NULL
	drop procedure up_marcacoes_listaEspecialistas
go

create PROCEDURE up_marcacoes_listaEspecialistas

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON
	
	Select 
		distinct nome, userno
	from 
		b_us (nolock) 
	where
		nome != 'Administrador de Sistema'
		
	
	
GO
Grant Execute On up_marcacoes_listaEspecialistas to Public
Grant Control On up_marcacoes_listaEspecialistas to Public
go