/* exec up_marcacoes_VerificaMarcacoesSerie 1 */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_VerificaMarcacoesSerie]') IS NOT NULL
    drop procedure up_marcacoes_VerificaMarcacoesSerie
go

create PROCEDURE up_marcacoes_VerificaMarcacoesSerie
@serieno as numeric(9,0)
/* WITH ENCRYPTION */
AS

	Select COUNT(serieno) as contador from b_cli_mr where serieno = @serieno
	
GO
Grant Execute On up_marcacoes_VerificaMarcacoesSerie to Public
Grant Control On up_marcacoes_VerificaMarcacoesSerie to Public
go
