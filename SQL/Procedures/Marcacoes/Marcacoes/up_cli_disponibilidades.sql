-- exec up_cli_disponibilidades '20120101','20120131', '','','','','TER�A-FEIRA',''
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cli_disponibilidades]') IS NOT NULL
	drop procedure dbo.up_cli_disponibilidades
go

create procedure dbo.up_cli_disponibilidades
@dataIni as datetime,
@dataFim as datetime,
@drnome as varchar(254),
@dvnome as varchar(254),
@hinicio as varchar(20),
@hfim as varchar(20),
@diasemana as varchar(254),
@servico as char(18)

/* WITH ENCRYPTION */

AS 
BEGIN

	SET LANGUAGE PORTUGUESE

	Declare @dataactual as datetime
	

	IF OBJECT_ID('TEMPDB..#TEMPDISP') IS NOT NULL
	BEGIN
	   DROP TABLE #TEMPDISP
	END
	SET @DATAACTUAL = @DATAINI

	/*Cria Tabela Temporaria*/
	SELECT 0 as ordem,convert(bit,0) as sel,@dataactual as dataactual,* INTO #tempDisp FROM b_cli_drdv (nolock) where 1=0
	/*************************/

	WHILE @dataactual <= @dataFim
	BEGIN
		
		INSERT INTO #TEMPDISP 

		SELECT	0 as ordem,convert(bit,0) as sel, @DATAACTUAL,B_CLI_DRDV.* 
		FROM	B_CLI_DRDV (NOLOCK)
		WHERE	B_CLI_DRDV.DIA = UPPER(DATENAME(DW,@DATAACTUAL))
				AND B_CLI_DRDV.DRNOME	=	CASE WHEN @DRNOME		= '' THEN  B_CLI_DRDV.DRNOME	ELSE @DRNOME	END
				AND B_CLI_DRDV.DVNOME	=	CASE WHEN @DVNOME		= '' THEN  B_CLI_DRDV.DVNOME	ELSE @DVNOME	END
				AND B_CLI_DRDV.HINICIO	>=	CASE WHEN @HINICIO		= '' THEN  B_CLI_DRDV.HINICIO	ELSE @HINICIO	END
				AND B_CLI_DRDV.HFIM		<=	CASE WHEN @HFIM			= '' THEN  B_CLI_DRDV.HFIM		ELSE @HFIM		END
				AND B_CLI_DRDV.DIA		=	CASE WHEN @DIASEMANA	= '' THEN  B_CLI_DRDV.DIA		ELSE @DIASEMANA	END
				AND B_CLI_DRDV.drdvstamp not in (Select distinct drdvstamp from b_cli_mr (nolock) where data = @dataactual)
	
		SET @DATAACTUAL = DATEADD(DD,1,@DATAACTUAL)

	END

	
		
			SELECT	*
			FROM	#TEMPDISP x
			ORDER BY DATAACTUAL, x.HINICIO, x.HFIM
		
		
	/* Elimina Tabela Temporaria */
	IF OBJECT_ID('TEMPDB..#TEMPDISP') IS NOT NULL
	BEGIN
	   DROP TABLE #TEMPDISP
	END


END
GO
Grant Execute on dbo.up_cli_disponibilidades to Public
GO
Grant Control on dbo.up_cli_disponibilidades to Public
GO