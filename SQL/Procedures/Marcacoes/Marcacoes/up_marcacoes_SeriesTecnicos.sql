/* exec up_marcacoes_SeriesTecnicos '' */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_SeriesTecnicos]') IS NOT NULL
    drop procedure up_marcacoes_SeriesTecnicos
go

create PROCEDURE up_marcacoes_SeriesTecnicos
@actstamp varchar(25)

/* WITH ENCRYPTION */
AS

	select 
		 b_us.nome
		 ,b_us.userno
		 ,b_us.usstamp
		 ,b_seriesTecnicos.duracao
		 ,b_series.seriestamp
		 ,b_seriesTecnicos.ref
	from 
		b_series
		inner join b_seriesTecnicos on b_series.seriestamp = b_seriesTecnicos.seriestamp
		inner join b_us on b_us.usstamp = b_seriesTecnicos.usstamp
	where
		b_series.seriestamp = @actstamp
	Order
		by nome, no
	
GO
Grant Execute On up_marcacoes_SeriesTecnicos to Public
Grant Control On up_marcacoes_SeriesTecnicos to Public
go