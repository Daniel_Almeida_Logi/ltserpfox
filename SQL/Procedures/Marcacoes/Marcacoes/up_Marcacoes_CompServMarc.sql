-- Recursos
-- exec up_Marcacoes_CompServMarc 'ADM1C12A517-4D88-422F-97C '
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Marcacoes_CompServMarc]') IS NOT NULL
	drop procedure up_Marcacoes_CompServMarc
go

create PROCEDURE up_Marcacoes_CompServMarc
@mrstamp as char(25)


/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

	select 
		marcacoesServ.mrstamp
		,cpt_marcacoes.servcompmrstamp
		,marcacoesServ.servmrstamp
		,marcacoesServ.ref
		,marcacoesServ.design
		,marcacoesServ.pvp
		,marcacoesServ.total
		,cpt_marcacoes.valor
		,cpt_marcacoes.ordem
		,cpt_marcacoes.entidade
		,cpt_marcacoes.entidadeno
		,cpt_marcacoes.entidadeestab
		,cpt_marcacoes.compart
		,cpt_marcacoes.maximo
		,cpt_marcacoes.tipoCompart
		,cpt_marcacoes.id_cp
		/*,cp. */
	from 
		cpt_marcacoes
		inner join marcacoesServ on marcacoesServ.servmrstamp = cpt_marcacoes.servmrstamp
		left join cpt_val_cli on cpt_val_cli.id = cpt_marcacoes.id_cp
		left join B_utentes on cpt_val_cli.id_entidade = B_utentes.no
	where	
		marcacoesServ.mrstamp = @mrstamp
	order By
		marcacoesServ.ref, ordem


GO
Grant Execute On up_Marcacoes_CompServMarc to Public
grant control on up_Marcacoes_CompServMarc to Public
go	