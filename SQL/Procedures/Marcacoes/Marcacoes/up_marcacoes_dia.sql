/* exec up_marcacos_lista 2014,2,18 */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacos_lista]') IS NOT NULL
    drop procedure up_marcacos_lista
go

create PROCEDURE up_marcacos_lista
@ano numeric(4)
,@mes numeric(2)
,@dia numeric(2)


/* WITH ENCRYPTION */
AS

	select 
		marc_ID
		,data
		,mrno
		,hinicio
		,hfim
		,utstamp
		,nome
		,no
		,estab
		,obs
		,serieno
		,serienome
		,sessao
		,estado
		,SerieDescricao = convert(varchar(254),LEFT(serienome,245) + ' [' + convert(varchar,sessao) + ']')
	from 
		marcacoes
	Where
		year(data) = @ano
		and month(data) = @mes
		and day(data) = @dia
	
	
GO
Grant Execute On up_marcacos_lista to Public
Grant Control On up_marcacos_lista to Public
go