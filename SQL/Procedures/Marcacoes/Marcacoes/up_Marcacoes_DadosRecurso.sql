-- Recursos
-- exec up_Marcacoes_DadosRecurso ''
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Marcacoes_DadosRecurso]') IS NOT NULL
	drop procedure up_Marcacoes_DadosRecurso
go

create PROCEDURE up_Marcacoes_DadosRecurso
@recurso as char(25)


/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

	select * from recursos

GO
Grant Execute On up_Marcacoes_DadosRecurso to Public
grant control on up_Marcacoes_DadosRecurso to Public
go	