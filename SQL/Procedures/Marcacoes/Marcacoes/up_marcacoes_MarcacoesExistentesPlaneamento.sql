
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_marcacoes_MarcacoesExistentesPlaneamento]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[up_marcacoes_MarcacoesExistentesPlaneamento]
GO

create PROCEDURE [dbo].[up_marcacoes_MarcacoesExistentesPlaneamento]

/* WITH ENCRYPTION */

AS

	select 
		* 
	from 
		marcacoes 
	where 
		data > DATEADD(dd,-5,getdate()) 
		and marcacoes.estado != 'CANCELADO'

GO


