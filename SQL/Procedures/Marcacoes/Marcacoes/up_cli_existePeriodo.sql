-- exec up_cli_existePeriodo 'ESPECIALISTA1','ANATOMIA PATOLÓGICA22'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cli_existePeriodo]') IS NOT NULL
	drop procedure dbo.up_cli_existePeriodo
go

create procedure dbo.up_cli_existePeriodo
@drnome as varchar(50),
@dvnome as varchar(50)

/* WITH ENCRYPTION */

AS 
BEGIN
	;with cte1 as (
		Select drnome,dvnome, usrdata, usrhora from b_cli_drdv where drnome = @drnome and dvnome = @dvnome 
		union all
		Select drnome,dvnome, usrdata, usrhora from b_cli_pd where drnome = @drnome and dvnome = @dvnome
	)
	Select drnome,dvnome  from cte1  

END

GO
Grant Execute on dbo.up_cli_existePeriodo to Public
GO
Grant Control on dbo.up_cli_existePeriodo to Public
GO
