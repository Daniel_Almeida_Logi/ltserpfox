-- exec up_cli_detalhemarcacoes ''
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cli_detalhemarcacoes]') IS NOT NULL
	drop procedure dbo.up_cli_detalhemarcacoes
go

create procedure dbo.up_cli_detalhemarcacoes

@mrstamp	as varchar(254)

/* WITH ENCRYPTION */ 

AS 
BEGIN

	SET LANGUAGE PORTUGUESE

	Select	*
	from	marcacoes (nolock)
	Where	mrstamp = @mrstamp
END

GO
Grant Execute on dbo.up_cli_detalhemarcacoes to Public
GO
Grant Control on dbo.up_cli_detalhemarcacoes to Public
GO