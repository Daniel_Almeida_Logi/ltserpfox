----------------------------------------------------------- */ 
-- Varifica j� existe uma marca��o associada ao perido selecionado 
-- exec up_cli_VerificaMarcacaoNoPeriodo
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cli_VerificaMarcacaoNoPeriodo]') IS NOT NULL
	drop procedure dbo.up_cli_VerificaMarcacaoNoPeriodo
go

create procedure dbo.up_cli_VerificaMarcacaoNoPeriodo
@drdvstamp as varchar(50)

/* WITH ENCRYPTION */

AS 
BEGIN

	Select	mrstamp 
	from	b_cli_mr (nolock)
	where	drdvstamp = @drdvstamp
	
END

GO
Grant Execute on dbo.up_cli_VerificaMarcacaoNoPeriodo to Public
GO
Grant Control on dbo.up_cli_VerificaMarcacaoNoPeriodo to Public
GO
