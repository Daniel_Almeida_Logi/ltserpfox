/* exec up_marcacos_STST   '10000037',1 */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacos_STST  ]') IS NOT NULL
    drop procedure up_marcacos_STST  
go

create PROCEDURE up_marcacos_STST  
@ref varchar(18)
,@site_nr as tinyint
/* WITH ENCRYPTION */
AS
	select 
		 b_stst.reforiginal
		 ,st.ref
		 ,st.design
		 ,b_stst.duracao
		 ,b_stst.qtt
	from 
		b_stst (nolock) 
		inner join st (nolock) on b_stst.ref = st.ref
	where
		 b_stst.reforiginal = @ref
		 and b_stst.site_nr = @site_nr
		 and st.site_nr = @site_nr


	
	
GO
Grant Execute On up_marcacos_STST   to Public
Grant Control On up_marcacos_STST   to Public
go