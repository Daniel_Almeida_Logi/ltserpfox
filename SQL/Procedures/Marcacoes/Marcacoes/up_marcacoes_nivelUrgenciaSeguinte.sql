/*
  	exec up_marcacoes_nivelUrgenciaSeguinte 'EMERGENTE','ADM911FEA4E-7E2E-4D69-9D8'

 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_nivelUrgenciaSeguinte]') IS NOT NULL
	drop procedure up_marcacoes_nivelUrgenciaSeguinte
go

create PROCEDURE up_marcacoes_nivelUrgenciaSeguinte
@nivel_actual as  varchar(15)
,@mrstamp as varchar(25)
/* WITH ENCRYPTION */

AS



	update 
		marcacoes
	set
		marcacoes.nivelUrgencia = 
			Case 
				when @nivel_actual = 'EMERGENTE' then 'MTO. URGENTE'
				when @nivel_actual = 'MTO. URGENTE' then 'URGENTE'
				when @nivel_actual = 'URGENTE' then 'POUCO URGENTE'
				when @nivel_actual = 'POUCO URGENTE' then 'N�O URGENTE'
				when @nivel_actual = 'N�O URGENTE' then 'EMERGENTE'
				else ''
			end
	Where
		marcacoes.mrstamp = @mrstamp
		and marcacoes.estado in(
			'EMERGENTE'
			,'MTO. URGENTE'
			,'URGENTE'
			,'POUCO URGENTE'
			,'N�O URGENTE'
		)
	

	
GO
Grant Execute On up_marcacoes_nivelUrgenciaSeguinte to Public
Grant Control On up_marcacoes_nivelUrgenciaSeguinte to Public
go
