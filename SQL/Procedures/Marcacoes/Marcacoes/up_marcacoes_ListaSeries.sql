/* exec up_marcacoes_ListaSeries */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_ListaSeries]') IS NOT NULL
    drop procedure up_marcacoes_ListaSeries
go

create PROCEDURE up_marcacoes_ListaSeries

/* WITH ENCRYPTION */
AS

	Select 
		nome = serienome
		,no = serieno
	from	
		b_series
	Where
		tipo = 'Servi�os'
	Order by
		serienome
	
	
	
GO
Grant Execute On up_marcacoes_ListaSeries to Public
Grant Control On up_marcacoes_ListaSeries to Public
go
