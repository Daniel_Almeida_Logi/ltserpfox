-- exec up_cli_existeMarcacao ''
-- Select * from b_cli_mr
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cli_existeMarcacao]') IS NOT NULL
	drop procedure dbo.up_cli_existeMarcacao
go

create procedure dbo.up_cli_existeMarcacao

@mrstamp as varchar(254)

/* WITH ENCRYPTION */


AS 
BEGIN
	
	Select	count(mrno) as valor from b_cli_mr (nolock) Where mrstamp = @mrstamp and mrstamp!= ''
	
END

GO
Grant Execute on dbo.up_cli_existeMarcacao to Public
GO
Grant Control on dbo.up_cli_existeMarcacao to Public
GO