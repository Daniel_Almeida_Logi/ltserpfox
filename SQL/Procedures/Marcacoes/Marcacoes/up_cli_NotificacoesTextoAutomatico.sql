-- exec up_cli_NotificacoesTextoAutomatico 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cli_NotificacoesTextoAutomatico]') IS NOT NULL
	drop procedure dbo.up_cli_NotificacoesTextoAutomatico
go

create procedure dbo.up_cli_NotificacoesTextoAutomatico

/* WITH ENCRYPTION */

AS 
BEGIN
	Select	[desc] = 'N�mero da Marca��o',
			[cod] = '�mrno�'
	Union ALL
	Select	[desc] = 'Data da Marca��o',
			[cod] = '�data�'
	Union ALL
	Select	[desc] = 'Hora de In�cio',
			[cod] = '�hinicio�'
	Union ALL
	Select	[desc] = 'Hora de Fim',
			[cod] = '�hfim�'
	Union ALL
	Select  [desc] = 'Nome do Utente',
			[cod] = '�nome�'
	Union ALL
	Select	[desc] = 'Especialidade',
			[cod] = '�div�'
	Union ALL
	Select	[desc] = 'Especialista',
			[cod] = '�drnome�'
	Union ALL
	Select	[desc] ='Servi�o',
			[cod] = '�design�'
	
END

GO
Grant Execute on dbo.up_cli_NotificacoesTextoAutomatico to Public
Grant Control on dbo.up_cli_NotificacoesTextoAutomatico to Public
GO