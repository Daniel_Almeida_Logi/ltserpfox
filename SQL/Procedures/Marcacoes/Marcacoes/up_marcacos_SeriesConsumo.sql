/* exec up_marcacos_SeriesConsumo '' */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacos_SeriesConsumo]') IS NOT NULL
    drop procedure up_marcacos_SeriesConsumo
go

create PROCEDURE up_marcacos_SeriesConsumo
@actstamp varchar(25)


/* WITH ENCRYPTION */
AS

	select 
		b_seriesConsumo.ref
		,reforiginal
		,duracao
		,qtt
		,st.design
		,b_seriesConsumo.seriestamp
	from 
		b_seriesConsumo 
		inner join st on b_seriesConsumo.ref = st.ref
	where
		b_seriesConsumo.seriestamp = @actstamp
	
	
GO
Grant Execute On up_marcacos_SeriesConsumo to Public
Grant Control On up_marcacos_SeriesConsumo to Public
go


