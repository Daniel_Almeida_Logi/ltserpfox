/*
  exec up_marcacoes_listaHoras
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_listaHoras]') IS NOT NULL
	drop procedure up_marcacoes_listaHoras
go

create PROCEDURE up_marcacoes_listaHoras

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON
	
	select hora = '00:00' 
	union all
	select hora = '01:00' 
	union all 
	select hora = '02:00' 
	union all
	select hora = '03:00' 
	union all 
	select hora = '04:00' 
	union all
	select hora = '05:00' 
	union all 
	select hora = '06:00' 
	union all
	select hora = '07:00' 
	union all
	select hora = '08:00' 
	union all 
	select hora = '09:00' 
	union all
	select hora = '10:00' 
	union all
	select hora = '11:00' 
	union all
	select hora = '12:00' 
	union all
	select hora = '13:00' 
	union all 
	select hora = '14:00' 
	union all
	select hora = '15:00' 
	union all
	select hora = '16:00' 
	union all 
	select hora = '17:00' 
	union all
	select hora = '18:00' 
	union all
	select hora = '19:00' 
	union all
	select hora = '20:00' 
	union all
	select hora = '21:00' 
	union all
	select hora = '22:00' 
	union all 
	select hora = '23:00' 
GO
Grant Execute On up_marcacoes_listaHoras to Public
Grant Control On up_marcacoes_listaHoras to Public
go