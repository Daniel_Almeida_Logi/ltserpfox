/*
  exec up_marcacoes_seriesUlt
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_seriesUlt]') IS NOT NULL
	drop procedure up_marcacoes_seriesUlt
go

create PROCEDURE up_marcacoes_seriesUlt

/* WITH ENCRYPTION */

AS

	select 
		Top 1 seriestamp 
	from 
		b_series
	Where
		b_series.tipo != 'Utilizadores'
	Order by 
		usrdata desc, usrhora desc
GO
Grant Execute On up_marcacoes_seriesUlt to Public
Grant Control On up_marcacoes_seriesUlt to Public
go