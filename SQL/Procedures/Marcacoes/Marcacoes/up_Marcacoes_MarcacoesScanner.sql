-- Recursos
-- exec up_Marcacoes_MarcacoesScanner '1'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Marcacoes_MarcacoesScanner]') IS NOT NULL
	drop procedure up_Marcacoes_MarcacoesScanner
go

create PROCEDURE up_Marcacoes_MarcacoesScanner
@mrno as varchar(25)


/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

	select 
		data, mrstamp, nome, serienome
	from 
		marcacoes (nolock)
	where
		RTRIM(LTRIM(STR(marcacoes.mrno))) = SUBSTRING(@mrno, PATINDEX('%[^0 ]%', @mrno + ' '), LEN(@mrno))




GO
Grant Execute On up_Marcacoes_MarcacoesScanner to Public
grant control on up_Marcacoes_MarcacoesScanner to Public
go	