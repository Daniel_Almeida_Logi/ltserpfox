-- Recursos
-- exec up_Marcacoes_DuracaoMrSimServico '1000015'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Marcacoes_DuracaoMrSimServico]') IS NOT NULL
	drop procedure up_Marcacoes_DuracaoMrSimServico
go

create PROCEDURE up_Marcacoes_DuracaoMrSimServico
@refs as varchar(max) = ''


/* WITH ENCRYPTION */

AS


select 
	B_cli_stRecursos.ref
	,B_cli_stRecursos.duracao
	,B_cli_stRecursos.mrsimultaneo
	,B_cli_stRecursos.no
	,empresa.site
from 
	B_cli_stRecursos
	inner join empresa on empresa.no = B_cli_stRecursos.site_nr
where 
	B_cli_stRecursos.ref in (select * from up_SplitToTable(@refs,','))
	and B_cli_stRecursos.tipo = 'Utilizador'
	
	
	
	
GO
Grant Execute On up_Marcacoes_DuracaoMrSimServico to Public
grant control on up_Marcacoes_DuracaoMrSimServico to Public
go	