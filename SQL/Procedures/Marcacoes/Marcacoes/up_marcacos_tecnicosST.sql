/* exec up_marcacos_tecnicosST '' */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacos_tecnicosST]') IS NOT NULL
    drop procedure up_marcacos_tecnicosST
go

create PROCEDURE up_marcacos_tecnicosST
@ref varchar(18)


/* WITH ENCRYPTION */
AS

	select 
		 b_us.nome
		 ,b_us.userno
		 ,b_us.usstamp
		 ,b_stus.duracao
		 ,b_stus.ref
	from 
		st (nolock)
		inner join b_stus on b_stus.ref = st.ref
		inner join b_us on b_us.usstamp = b_stus.usstamp
	where
		st.ref = @ref
	
	
GO
Grant Execute On up_marcacos_tecnicosST to Public
Grant Control On up_marcacos_tecnicosST to Public
go