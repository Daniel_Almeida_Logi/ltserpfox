
/* exec up_marcacos_FormasPagamento */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacos_FormasPagamento]') IS NOT NULL
    drop procedure up_marcacos_FormasPagamento
go

create PROCEDURE up_marcacos_FormasPagamento



/* WITH ENCRYPTION */
AS


	Select 'Entidade'
	union all
	Select 'Utente'
	union all
	Select 'Entidade e Utente'
	union all
	Select 'Ninguem Paga'
	
	
GO
Grant Execute On up_marcacos_FormasPagamento to Public
Grant Control On up_marcacos_FormasPagamento to Public
go

