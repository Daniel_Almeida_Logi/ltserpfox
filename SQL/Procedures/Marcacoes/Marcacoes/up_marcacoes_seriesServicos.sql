-- exec up_marcacoes_seriesServicos ''
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_seriesServicos]') IS NOT NULL
	drop procedure up_marcacoes_seriesServicos
go

create PROCEDURE up_marcacoes_seriesServicos
@stamp as varchar(25)


/* WITH ENCRYPTION */

AS

	Select 
		b_seriesServicos.*
	from 
		b_seriesServicos 
	where 
		seriestamp = @stamp
	
		
		

		
GO
Grant Execute On up_marcacoes_seriesServicos to Public
Grant Control On up_marcacoes_seriesServicos to Public
go
