/*
  exec up_marcacoes_ServicosDia 
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_ServicosDia]') IS NOT NULL
	drop procedure up_marcacoes_ServicosDia
go

create PROCEDURE up_marcacoes_ServicosDia
@data as datetime
/* WITH ENCRYPTION */

AS
	
	select 
		marcacoesServ.* 
	from
		marcacoes
		inner join marcacoesServ on marcacoes.mrstamp = marcacoesServ.mrstamp
	Where
		@data between marcacoes.data and marcacoes.dataFim
			
GO
Grant Execute On up_marcacoes_ServicosDia to Public
Grant Control On up_marcacoes_ServicosDia to Public
go
