-- exec up_cli_HistoricoMarcacoes 79
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cli_HistoricoMarcacoes]') IS NOT NULL
	drop procedure dbo.up_cli_HistoricoMarcacoes
go

create procedure dbo.up_cli_HistoricoMarcacoes

@idno as numeric(9,0)

/* WITH ENCRYPTION */

AS 
BEGIN
	
	Select top 30 data,hinicio,hfim,div as dvnome,drnome,estado from b_cli_mr (nolock) Where idno = @idno order by data desc 
	
END

GO
Grant Execute on dbo.up_cli_HistoricoMarcacoes to Public
GO
Grant Control on dbo.up_cli_HistoricoMarcacoes to Public
GO