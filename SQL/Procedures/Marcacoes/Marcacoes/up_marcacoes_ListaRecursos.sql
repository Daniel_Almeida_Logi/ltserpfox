/* exec up_marcacoes_ListaRecursos 'Utilizador', '', 0
	exec up_marcacoes_ListaRecursos '', '',0
	exec up_marcacoes_ListaRecursos '', '', 0
*/ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_ListaRecursos]') IS NOT NULL
    drop procedure up_marcacoes_ListaRecursos
go

create PROCEDURE up_marcacoes_ListaRecursos
@tipo as varchar(30)
,@nome as  varchar(30)
,@no as numeric(5,0)

/* WITH ENCRYPTION */
AS


	Select *
	From (
		Select 
			sel = CONVERT(bit,0)
			,tipo = 'Utilizador'
			,nome = b_us.nome
			,no = b_us.userno
			,stamp = b_us.usstamp
		From 
			b_us
			
		union all

		select 
			sel = CONVERT(bit,0)
			,tipo = 'Especialidade'
			,nome = especialidade
			,CONVERT(int,espno)
			,stamp = especialidadestamp 
			
		from 
			B_especialidades		

		union all

		select 
			sel = CONVERT(bit,0)
			,tipo = 'Recurso'
			,nome
			,no
			,stamp = recursosstamp 
			
		from 
			recursos 		
	) as x
	Where
		x.tipo = case when @tipo = '' then x.tipo else @tipo end
		and x.nome like @nome + '%'
		and x.no = case when @no = 0 then x.no else @no end
	Order 
		by nome, no, Tipo	
		
		
	
GO
Grant Execute On up_marcacoes_ListaRecursos to Public
Grant Control On up_marcacoes_ListaRecursos to Public
go