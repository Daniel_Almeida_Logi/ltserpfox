/*
  exec up_marcacoes_criarPerfilAlertas '123',1
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_criarPerfilAlertas]') IS NOT NULL
	drop procedure up_marcacoes_criarPerfilAlertas
go

create PROCEDURE up_marcacoes_criarPerfilAlertas
@stamp as char(25),
@userno as numeric(9,0)

/* WITH ENCRYPTION */

AS

	IF (Select COUNT(userno) from marcacoes_Alertas where userno = @userno) = 0
	Begin
		insert into marcacoes_Alertas (alertamrstamp, userno)	Values (@stamp,@userno)
	END
	

	
GO
Grant Execute On up_marcacoes_criarPerfilAlertas to Public
Grant Control On up_marcacoes_criarPerfilAlertas to Public
go