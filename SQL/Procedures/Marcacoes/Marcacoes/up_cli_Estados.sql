-- Estado das Marca��es
-- exec up_cli_Estados
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cli_Estados]') IS NOT NULL
	drop procedure dbo.up_cli_Estados
go

create procedure dbo.up_cli_Estados

/* WITH ENCRYPTION */

AS 
BEGIN
	SELECT ''	AS ESTADO
	UNION ALL
	SELECT 'MARCADO'	AS ESTADO
	UNION ALL
	SELECT 'DISPONIVEL' AS ESTADO
	UNION ALL
	SELECT 'PRESENTE'	AS ESTADO
	UNION ALL
	SELECT 'ATRASADO'	AS ESTADO
	UNION ALL
	SELECT 'FALTA'		AS ESTADO
	UNION ALL
	SELECT 'CANCELADO'	AS ESTADO
	UNION ALL
	SELECT 'EFECTUADA'	AS ESTADO
	
END

GO
Grant Execute on dbo.up_cli_Estados to Public
GO
Grant Control on dbo.up_cli_Estados to Public
GO