/*
  exec up_marcacoes_convencoesUtentes 62385,0
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_convencoesUtentes]') IS NOT NULL
	drop procedure up_marcacoes_convencoesUtentes
go

create PROCEDURE up_marcacoes_convencoesUtentes
@no numeric(10,0),
@estab numeric(3,0)



/* WITH ENCRYPTION */

AS


	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosComp'))
		DROP TABLE #dadosComp
			
	select 
		cpt_val_cli.ref
		,cpt_val_cli.pvp
		,cpt_conv.site
		,cpt_conv.descr
		,cpt_conv.id_entidade
	into
		#dadosComp
	From
		cpt_conv	
		inner join cpt_val_cli (nolock) on cpt_val_cli.id_cpt_conv = cpt_conv.id

	select 
		B_entidadesUtentes.eno
		,B_entidadesUtentes.nome
		,convencao = #dadosComp.descr
		,#dadosComp.ref
		,#dadosComp.pvp
		,#dadosComp.site
	from 
		b_utentes (nolock)
		inner join B_entidadesUtentes (nolock) on B_entidadesUtentes.utstamp = b_utentes.utstamp
		inner join #dadosComp on #dadosComp.id_entidade = B_entidadesUtentes.eno
		left join empresa (nolock) on empresa.site = #dadosComp.site
		left join st (nolock) on #dadosComp.ref = st.ref and st.site_nr = empresa.no
	where
		B_utentes.no = @no
		and B_utentes.estab = @estab
	
	--select 
	--	B_entidadesUtentes.eno
	--	,B_entidadesUtentes.nome
	--	,convencao = cpt_conv.descr
	--	,cpt_val_cli.ref
	--	,cpt_val_cli.pvp
	--	,cpt_conv.site
	--from 
	--	b_utentes (nolock)
	--	inner join B_entidadesUtentes (nolock) on B_entidadesUtentes.utstamp = b_utentes.utstamp
	--	inner join cpt_conv (nolock) on cpt_conv.id_entidade = B_entidadesUtentes.eno
	--	inner join cpt_val_cli (nolock) on cpt_val_cli.id_cpt_conv = cpt_conv.id
	--	--left join empresa (nolock) on empresa.site = cpt_conv.site
	--	--left join st (nolock) on cpt_val_cli.ref = st.ref and st.site_nr = empresa.no
	--where
	--	B_utentes.no = @no
	--	and B_utentes.estab = @estab
			
GO
Grant Execute On up_marcacoes_convencoesUtentes to Public
Grant Control On up_marcacoes_convencoesUtentes to Public
go

