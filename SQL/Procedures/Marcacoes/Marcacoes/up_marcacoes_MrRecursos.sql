/* exec up_marcacoes_MrRecursos 1900,1 */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_MrRecursos]') IS NOT NULL
    drop procedure up_marcacoes_MrRecursos
go

create PROCEDURE up_marcacoes_MrRecursos
@ano numeric(4,0),
@mes numeric(2,0)

/* WITH ENCRYPTION */
AS
	
	Select 
		B_cli_mrRecurso.*,
		B_cli_recursos.nome
	From
		B_cli_mrRecurso
		inner join B_cli_recursos on B_cli_recursos.recursosstamp = B_cli_mrRecurso.recursosstamp 
		inner join b_cli_mr on b_cli_mr.mrstamp = B_cli_mrRecurso.mrstamp
	where
		year(b_cli_mr.data) = @ano
		and month(b_cli_mr.data) = @mes
	
GO
Grant Execute On up_marcacoes_MrRecursos to Public
Grant Control On up_marcacoes_MrRecursos to Public
go