-- exec up_cli_UltimoPeriodo  
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cli_UltimoPeriodo]') IS NOT NULL
	drop procedure dbo.up_cli_UltimoPeriodo
go

create procedure dbo.up_cli_UltimoPeriodo


/* WITH ENCRYPTION */

AS 
BEGIN
	;with cte1 as (
		Select drnome,dvnome, usrdata, usrhora from b_cli_drdv 
		union all
		Select drnome,dvnome, usrdata, usrhora from b_cli_pd 
	)
	Select top 1 drnome,dvnome  from cte1  order by usrdata desc, usrhora desc

END

GO
Grant Execute on dbo.up_cli_UltimoPeriodo to Public
GO
Grant Control on dbo.up_cli_UltimoPeriodo to Public
GO	