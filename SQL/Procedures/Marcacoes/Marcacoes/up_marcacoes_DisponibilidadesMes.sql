/*
  exec up_marcacoes_DisponibilidadesMes 2014,02
 */

if OBJECT_ID('[dbo].[up_marcacoes_DisponibilidadesMes]') IS NOT NULL
	drop procedure up_marcacoes_DisponibilidadesMes
go

create PROCEDURE up_marcacoes_DisponibilidadesMes
@ano	as numeric(4),
@mes	as numeric(2)

/* WITH ENCRYPTION */

AS

	SET LANGUAGE PORTUGUESE
	;with 	/* Dados de Marca��es Para o Dia */
		cte1 as ( 
			Select	
				mrstamp
				,drdvstamp
				,mrno
				,nome
				,idno
				,data
				,div
				,coddiv
				,drnome
				,drno
				,hinicio
				,hfim
				,UPPER(datename(dw, data)) as dia
				,estado, obs, speriodo, ref, design,utstamp,serieno,serienome,sessao,
				recursostamp, recursonome, recursono
			FROM	
				b_cli_mr (nolock)
			Where	
				year(data) = @ano
				and MONTH(data) = @mes
	)
	
	Select	
		mrstamp		 = isnull(cte1.mrstamp,''),
		mr_drdvstamp = isnull(cte1.drdvstamp,''),
		drdvstamp	 = isnull(cte1.drdvstamp,left(newid(),25)),
		mrno		 = ISNULL(cte1.mrno,0),
		nome		 = isnull(cte1.nome,''),
		idno		 = isnull(cte1.idno,0),
		data		 = isnull(cte1.data,''),
		drnome		 = isnull(cte1.drnome,''),
		drno		 = isnull(cte1.drno,0),
		dvnome		 = isnull(cte1.div,''),
		dvno		 = isnull(cte1.coddiv,0),
		dia			 = isnull(cte1.dia,''),
		hinicio		 = isnull(cte1.hinicio,'00:00'),
		hfim		 = isnull(cte1.hfim,'00:00'),
		inactivo	 = 0,
		dupli		 = 0,
		capacidade	 = 1,
		estado		 = isnull(cte1.estado,convert(varchar(20),'')),
		speriodo	 = isnull(cte1.speriodo,0),
		utstamp		= isnull(ut.utstamp,''),
		nome		= isnull(ut.nome,''),
		ncont		= isnull(ut.ncont,''),
		nutente		= isnull(ut.nbenef,''),
		nbenef		= isnull(ut.nbenef2,''),
		local		= isnull(ut.local,''),
		dtnasc = isnull(ut.nascimento,''),
		morada = isnull(ut.morada,''),
		telefone = isnull(ut.telefone,''),
		dtopen = isnull(ut.ousrdata,''),
		obs = isnull(ut.obs,'')
		,selecionada = CONVERT(bit,0)
		,ref = isnull(cte1.ref,'')
		,design = isnull(cte1.design,'')
		,serieno
		,serienome
		,sessao
		,recursostamp
		,recursonome = LEFT(recursonome,253)
		,recursono
		,filtroEspecialista = CONVERT(bit,1)
		,filtroRecurso = CONVERT(bit,1)
		,filtroEspecialidade = CONVERT(bit,1)
	from	
		cte1
		/*FULL OUTER JOIN cte2 on cte1.drdvstamp = cte2.drdvstamp*/
		Left join b_utentes as ut on ut.utstamp = cte1.utstamp
	
	ORDER BY 
		hinicio, hfim		
	
GO
Grant Execute On up_marcacoes_DisponibilidadesMes to Public
Grant Control On up_marcacoes_DisponibilidadesMes to Public
go