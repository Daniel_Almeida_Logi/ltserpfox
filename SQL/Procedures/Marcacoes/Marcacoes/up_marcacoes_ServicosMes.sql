/*
  exec up_marcacoes_ServicosMes
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_ServicosMes]') IS NOT NULL
	drop procedure up_marcacoes_ServicosMes
go

create PROCEDURE up_marcacoes_ServicosMes
@ano as numeric(4)
,@mes as numeric(2)
/* WITH ENCRYPTION */

AS
	
	select 
		marcacoesServ.servmrstamp
		,marcacoesServ.mrstamp
		,marcacoesServ.ref
		,marcacoesServ.design
		,marcacoesServ.duracao
		,marcacoesServ.qtt
		,marcacoesServ.dataInicio
		,marcacoesServ.dataFim
		,marcacoesServ.hinicio
		,marcacoesServ.hfim
		,marcacoesServ.ordem
		,marcacoesServ.compart
		,marcacoesServ.pvp
		,marcacoesServ.total
		,marcacoesServ.fu
		,marcacoesServ.fe
		,honorario = case when fn_honorarios.honorario is null then CONVERT(bit,0) else CONVERT(bit,1) end
		,valorHonorario = fn_honorarios.honorario
		,marcacoesServ.ousrinis
		,marcacoesServ.ousrhora
		,marcacoesServ.ousrdata
		,marcacoesServ.usrinis
		,marcacoesServ.usrdata
		,fn_honorarios.HonorarioDeducao
		,fn_honorarios.HonorarioValor
		,fn_honorarios.HonorarioBi
		,fn_honorarios.HonorarioTipo
		,marcacoesServ.usrhora
		,marcacoesServ.ufistamp
		,marcacoesServ.efistamp
		,marcacoesServ.valorAdicional
		,marcacoesServ.EstadohonEsp
		,marcacoesServ.EstadohonOp
	from
		marcacoes (nolock)
		inner join marcacoesServ (nolock) on marcacoes.mrstamp = marcacoesServ.mrstamp
		left join fn_honorarios (nolock) on marcacoesServ.servmrstamp = fn_honorarios.servmrstamp
	Where
		year(marcacoes.data) = @ano
		and month(marcacoes.data) = @mes
			
GO
Grant Execute On up_marcacoes_ServicosMes to Public
Grant Control On up_marcacoes_ServicosMes to Public
go
