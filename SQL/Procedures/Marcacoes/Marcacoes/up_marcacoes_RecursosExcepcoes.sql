/* exec up_marcacoes_RecursosExcepcoes '' */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_RecursosExcepcoes]') IS NOT NULL
    drop procedure up_marcacoes_RecursosExcepcoes
go

create PROCEDURE up_marcacoes_RecursosExcepcoes
@stamp varchar(25)

/* WITH ENCRYPTION */
AS

	
	Select 
		B_seriesExcecoes.*
	from 
		B_seriesExcecoes (nolock)
		inner join b_series (nolock) on B_seriesExcecoes.seriestamp = b_series.seriestamp
	where
		b_series.seriestamp = case when @stamp = '' then b_series.seriestamp else @stamp end

	
	
GO
Grant Execute On up_marcacoes_RecursosExcepcoes to Public
Grant Control On up_marcacoes_RecursosExcepcoes to Public
go
