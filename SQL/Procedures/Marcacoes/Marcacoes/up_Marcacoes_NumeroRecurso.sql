-- exec up_Marcacoes_NumeroRecurso
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Marcacoes_NumeroRecurso]') IS NOT NULL
	drop procedure up_Marcacoes_NumeroRecurso
go

create PROCEDURE up_Marcacoes_NumeroRecurso


/* WITH ENCRYPTION */

AS

	Select	ISNULL(MAX(no),0) +1 as numero
	from	recursos (nolock)

		
GO
Grant Execute On up_Marcacoes_NumeroRecurso to Public
Grant Control On up_Marcacoes_NumeroRecurso to Public
go
