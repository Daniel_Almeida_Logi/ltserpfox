-- exec up_cli_EliminaMarcacao ''
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cli_EliminaMarcacao]') IS NOT NULL
	drop procedure dbo.up_cli_EliminaMarcacao
go

create procedure dbo.up_cli_EliminaMarcacao

@stamp	as varchar(254)

/* WITH ENCRYPTION */

AS 
BEGIN

	Delete from b_cli_mr Where mrstamp = @stamp

END

GO
Grant Execute on dbo.up_cli_EliminaMarcacao to Public
GO
Grant Control on dbo.up_cli_EliminaMarcacao to Public
GO