/*
  exec up_marcacoes_existentes '19000101', '20150101'
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_existentes]') IS NOT NULL
	drop procedure up_marcacoes_existentes
go

create PROCEDURE up_marcacoes_existentes
@dataIni datetime,
@dataFim datetime

/* WITH ENCRYPTION */

AS

	select
		duracao = case when hinicio = '' or hfim = '' then 0 else Datediff(MINUTE,convert(datetime,LEFT(convert(varchar,data,120),11) + hinicio + ':00.000'),convert(datetime,LEFT(convert(varchar,data,120),11) + hfim + ':00.000')) end
		,marcacoes.*
	from
		marcacoes
	where
		marcacoes.data between @dataIni and @dataFim
		and marcacoes.estado != 'CANCELADO'
		and marcacoes.estado != 'PEDIDO'
GO
Grant Execute On up_marcacoes_existentes to Public
Grant Control On up_marcacoes_existentes to Public
go