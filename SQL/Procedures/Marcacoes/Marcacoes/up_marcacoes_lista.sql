/* exec up_marcacoes_lista '20140218' */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_lista]') IS NOT NULL
    drop procedure up_marcacoes_lista
go

create PROCEDURE up_marcacoes_lista
@data as datetime


/* WITH ENCRYPTION */
AS

	select 
		mrstamp
		,data
		,dataFim
		,mrno
		,hinicio
		,hfim
		,utstamp
		,nome
		,no
		,estab
		,obs
		,serieno
		,serienome
		,sessao
		,estado
		,SerieDescricao = convert(varchar(254),LEFT(serienome,245) + ' [' + convert(varchar,sessao) + ']')
		,site
		,combinacao
		,especialistas = isnull(convert(varchar(254),(select 
							marcacoesRecurso.nome + ', '
						from 
							marcacoesRecurso (nolock) 
						Where 
							marcacoesRecurso.mrstamp = marcacoes.mrstamp
						FOR XML PATH(''))),'')
		,especialidade = isnull(convert(varchar(254),(select 
							B_usesp.especialidade + ', '
						from 
							marcacoesRecurso (nolock) 
							left join B_usesp (nolock) on B_usesp.userno = marcacoesRecurso.no
						Where 
							marcacoesRecurso.mrstamp = marcacoes.mrstamp
						FOR XML PATH(''))),'')
		,id_us
		,data_cri
		,id_us_alt
		,data_alt
		,nivelurgencia
		,faturado = case when exists (	Select 
											mrstamp 
										from 
											marcacoesServ (nolock) 
										where 
											marcacoesServ.mrstamp = marcacoes.mrstamp and fu = 1 
										) then CONVERT(bit,1) 
								else CONVERT(bit,0) end
		,motivoCancelamento
		,talaolevantamento
		,LocalEntrega
		,EntregueTLev
		,designEntrega
		,dataEntrega
		,userEntrega
		,dataLevantamento
		,declaracaoPresenca = 'Para os devidos efeitos, declara-se que o Sr(�). ' + RTRIM(LTRIM(marcacoes.nome)) + ' esteve presente nas nossas instala��es no dia ' + left(CONVERT(varchar,marcacoes.data,126),10) 
		+ ' das ' + marcacoes.hinicio + ' �s ' + LEFT(CONVERT(varchar,getdate(),108),5) + ' a fim de: '
	from 
		marcacoes (nolock)
	Where
		@data between data and dataFim
	
	
GO
Grant Execute On up_marcacoes_lista to Public
Grant Control On up_marcacoes_lista to Public
go