/* exec up_marcacoes_MrServicos '' */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_MrServicos]') IS NOT NULL
    drop procedure up_marcacoes_MrServicos
go

create PROCEDURE up_marcacoes_MrServicos
@ano numeric(4,0),
@mes numeric(2,0)


/* WITH ENCRYPTION */
AS

	Select 
		ref
		,b_cli_mrServicos.duracao
		,b_cli_mrServicos.qtt
		,st.design
		,b_cli_mrServicos.consumomrstamp
		,b_cli_mrServicos.mrstamp
	From
		b_cli_mrServicos
		inner join st on st.ref = b_cli_mrServicos.ref
		inner join b_cli_mr on b_cli_mr.mrstamp = b_cli_mrServicos.mrstamp
	where
		year(b_cli_mr.data) = @ano
		and month(b_cli_mr.data) = @mes
	
	
GO
Grant Execute On up_marcacoes_MrServicos to Public
Grant Control On up_marcacoes_MrServicos to Public
go