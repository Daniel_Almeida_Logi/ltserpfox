/*
  exec up_marcacoes_existeMarcacao 'js979E19AC-735C-4AE-BC1'
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_existeMarcacao]') IS NOT NULL
	drop procedure up_marcacoes_existeMarcacao
go

create PROCEDURE up_marcacoes_existeMarcacao
@mrstamp as varchar(25)


/* WITH ENCRYPTION */

AS
	select 
		existe = case when count(mrstamp) = 0 then CONVERT(bit,0) else CONVERT(bit,1) end
	from 
		b_cli_mr (nolock) 
	where
		mrstamp = @mrstamp
GO
Grant Execute On up_marcacoes_existeMarcacao to Public
Grant Control On up_marcacoes_existeMarcacao to Public
go