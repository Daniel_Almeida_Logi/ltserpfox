/* exec up_marcacos_mensal '20130722','','','','','','' */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacos_mensal]') IS NOT NULL
    drop procedure up_marcacos_mensal
go

create PROCEDURE up_marcacos_mensal
@data as datetime,
@ref varchar(18),
@utente varchar(55),
@especialista varchar(50), --drnome
@especialidade varchar(50), --div
@hinicio varchar(5),
@hfim varchar(5)


/* WITH ENCRYPTION */
AS

	set language portuguese

	Select 
		dia = UPPER(datename(dw,data))
		,* 
	from 
		b_cli_mr (nolock)
	Where
		data = @data
		and ref = case when @ref = '' then ref else @ref end
		and nome = case when @utente = '' then nome else @utente end 
		and drnome = case when @especialista = '' then drnome else @especialista end
		and div = case when @especialidade = '' then div else @especialidade end
		and hinicio >= case when @hinicio = '' then  '00:00' else @hinicio end
		and hfim <= case when @hfim = '' then  '23:59' else @hfim end

	
GO
Grant Execute On up_marcacos_mensal to Public
Grant Control On up_marcacos_mensal to Public
go