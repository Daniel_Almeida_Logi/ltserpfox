/*
  exec up_marcacoes_pesqRefsAgrupadas '','','','','','',2
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_pesqRefsAgrupadas]') IS NOT NULL
	drop procedure up_marcacoes_pesqRefsAgrupadas
go

create PROCEDURE up_marcacoes_pesqRefsAgrupadas
@ref as varchar(18)
,@design as varchar(100)
,@especialidade as varchar(254) 
,@especialista as varchar(150) = ''
,@convencao as varchar(80) = ''
,@site as varchar(60) = ''
,@site_nr_PVP as tinyint = 0

/* WITH ENCRYPTION */

AS
	declare @especilidade_us as varchar(254) = ''
	

	/* Obtem especialidade em fun��o do utilizador*/
	if @especialista != ''
	begin
		set @especilidade_us = (select especialidade from b_us (nolock)	inner join b_usesp (nolock) on b_us.userno = b_usesp.userno Where b_us.nome = @especialista )
	end

	select 
		distinct sel = CONVERT(bit,0)
		,st.ref
		,st.design 
		,especialidade = convert(varchar(254),left(isnull(substring((select distinct ', '+nome AS 'data()' from b_cli_stRecursos where b_cli_stRecursos.ref = st.ref for xml path('')),3, 255) ,''),254))
		,u_duracao = (select max(u_duracao) from st as st_1 (nolock) where st_1.ref = st.ref)
		,mrsimultaneo = (select max(mrsimultaneo) from st as st_1 (nolock) where st_1.ref = st.ref)
		,epv1 = isnull((select TOP 1 epv1 from st as st_1 (nolock) where st_1.ref = st.ref and st_1.site_nr = @site_nr_PVP),0)
	from 
		st (nolock)
		inner join empresa (nolock) on st.site_nr = empresa.no
		left join bc (nolock) ON bc.ref = st.ref and st.site_nr = bc.site_nr
	where
		(@ref = '' or st.ref = @ref OR bc.codigo = @ref)
		and st.design like @design+'%' 
		and (@especialidade = '' or st.ref in (select ref from b_cli_stRecursos (nolock) where b_cli_stRecursos.ref = st.ref and tipo = 'Especialidade' and nome = @especialidade))
		and (@especilidade_us = '' or st.ref in (select ref from b_cli_stRecursos (nolock) where b_cli_stRecursos.ref = st.ref and tipo = 'Especialidade' and nome = @especilidade_us))
		and (@convencao = '' or st.ref in (Select cpt_val_cli.ref from cpt_conv (nolock) inner join cpt_val_cli (nolock) on cpt_conv.id = cpt_val_cli.id_cpt_conv and cpt_conv.descr = @convencao))
		and empresa.site = case when @site = '' then empresa.site else @site end
		and st.u_servmarc = 1
	Group by
		st.ref
		,st.design
	
	
	
	
	
		
GO
Grant Execute On up_marcacoes_pesqRefsAgrupadas to Public
Grant Control On up_marcacoes_pesqRefsAgrupadas to Public
go
