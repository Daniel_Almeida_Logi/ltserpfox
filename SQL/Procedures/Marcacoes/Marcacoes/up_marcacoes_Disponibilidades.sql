/*
  exec up_marcacoes_Disponibilidades 2014,02,17,1
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_Disponibilidades]') IS NOT NULL
	drop procedure up_marcacoes_Disponibilidades
go

create PROCEDURE up_marcacoes_Disponibilidades
@ano	as varchar(4),
@mes	as varchar(2),
@dia	as varchar(2),
@colunas as bit = 0

/* WITH ENCRYPTION */

AS

	IF @colunas = 0
	BEGIN 
		SET LANGUAGE PORTUGUESE
		Set @dia = (CASE WHEN LEN(RTRIM(LTRIM(@dia))) = 1 THEN '0'+@dia ELSE @dia END)
		Set @mes = (CASE WHEN LEN(RTRIM(LTRIM(@mes))) = 1 THEN '0'+@mes ELSE @mes END)

		;with 	/* Dados de Marca��es Para o Dia */
			cte1 as ( 
				Select	
					mrstamp
					,drdvstamp = case when drdvstamp is null then newid() else drdvstamp end
					,mrno
					,nome
					,idno
					,data
					,div
					,coddiv
					,drnome
					,drno
					,hinicio
					,hfim
					,dia = UPPER(datename(dw, data))
					,estado
					,obs
					,speriodo
					,ref
					,design
					,utstamp
					,serieno
					,serienome
					,sessao
					,recursostamp
					,recursonome
					,recursono
				FROM	
					b_cli_mr
				Where	
					data = convert(datetime,@ano+@mes+@dia)
		)
		
		Select	
			mrstamp		 = cte1.mrstamp,
			mr_drdvstamp = cte1.drdvstamp,
			drdvstamp	 = isnull(cte1.drdvstamp,newid()),
			mrno		 = ISNULL(cte1.mrno,0),
			nome		 = isnull(cte1.nome,''),
			idno		 = isnull(cte1.idno,0),
			data		 = isnull(cte1.data,''),
			drnome		 = isnull(cte1.drnome,''),
			drno		 = isnull(cte1.drno,0),
			dvnome		 = isnull(cte1.div,''),
			dvno		 = isnull(cte1.coddiv,0),
			dia			 = isnull(cte1.dia,''),
			hinicio		 = isnull(cte1.hinicio,'00:00'),
			hfim		 = isnull(cte1.hfim,'00:00'),
			inactivo	 = 0,
			dupli		 = 0,
			capacidade	 = 1,
			estado		 = isnull(cte1.estado,convert(varchar(20),'')),
			speriodo	 = isnull(cte1.speriodo,0),
			utstamp		= ut.utstamp,
			nome		= isnull(ut.nome,''),
			ncont		= isnull(ut.ncont,''),
			nutente		= isnull(ut.nbenef,''),
			nbenef		= isnull(ut.nbenef2,''),
			local		= isnull(ut.local,''),
			dtnasc = isnull(ut.nascimento,''),
			morada = isnull(ut.morada,''),
			telefone = isnull(ut.telefone,''),
			dtopen = isnull(ut.ousrdata,''),
			obs = isnull(ut.obs,'')
			,selecionada = CONVERT(bit,0)
			,ref = isnull(cte1.ref,'')
			,design = isnull(cte1.design,'')
			,serieno
			,serienome
			,sessao
			,recursostamp
			,recursonome = LEFT(recursonome,253)
			,recursono
			,filtroEspecialista = CONVERT(bit,1)
			,filtroRecurso = CONVERT(bit,1)
			,filtroEspecialidade = CONVERT(bit,1)
		from	
			cte1
			/*FULL OUTER JOIN cte2 on cte1.drdvstamp = cte2.drdvstamp*/
			Left join b_utentes as ut on ut.utstamp = cte1.utstamp
		
		
		ORDER BY 
			hinicio, hfim		
	END
	ELSE
	BEGIN
		
		;with 	/* Dados de Marca��es Para o Dia */
			cte1 as ( 
				Select top 0
					mrstamp
					,drdvstamp = case when drdvstamp is null then newid() else drdvstamp end
					,mrno
					,nome
					,idno
					,data
					,div
					,coddiv
					,drnome
					,drno
					,hinicio
					,hfim
					,dia = UPPER(datename(dw, data))
					,estado
					,obs
					,speriodo
					,ref
					,design
					,utstamp
					,serieno
					,serienome
					,sessao
					,recursostamp
					,recursonome
					,recursono
				FROM	
					b_cli_mr 
		)
		
		Select	
			top 0
			mrstamp		 = cte1.mrstamp,
			mr_drdvstamp = cte1.drdvstamp,
			drdvstamp	 = isnull(cte1.drdvstamp,newid()),
			mrno		 = ISNULL(cte1.mrno,0),
			nome		 = isnull(cte1.nome,''),
			idno		 = isnull(cte1.idno,0),
			data		 = isnull(cte1.data,''),
			drnome		 = isnull(cte1.drnome,''),
			drno		 = isnull(cte1.drno,0),
			dvnome		 = isnull(cte1.div,''),
			dvno		 = isnull(cte1.coddiv,0),
			dia			 = isnull(cte1.dia,''),
			hinicio		 = isnull(cte1.hinicio,'00:00'),
			hfim		 = isnull(cte1.hfim,'00:00'),
			inactivo	 = 0,
			dupli		 = 0,
			capacidade	 = 1,
			estado		 = isnull(cte1.estado,convert(varchar(20),'')),
			speriodo	 = isnull(cte1.speriodo,0),
			utstamp		= isnull(ut.utstamp,''),
			nome		= isnull(ut.nome,''),
			ncont		= isnull(ut.ncont,''),
			nutente		= isnull(ut.nbenef,''),
			nbenef		= isnull(ut.nbenef2,''),
			local		= isnull(ut.local,''),
			dtnasc = isnull(ut.nascimento,''),
			morada = isnull(ut.morada,''),
			telefone = isnull(ut.telefone,''),
			dtopen = isnull(ut.ousrdata,''),
			obs = isnull(ut.obs,'')
			,selecionada = CONVERT(bit,0)
			,ref = isnull(cte1.ref,'')
			,design = isnull(cte1.design,'')
			,serieno
			,serienome
			,sessao
			,recursostamp
			,recursonome = LEFT(recursonome,253)
			,recursono
			,filtroEspecialista = CONVERT(bit,1)
			,filtroRecurso = CONVERT(bit,1)
			,filtroEspecialidade = CONVERT(bit,1)
		from	
			cte1
			Left join b_utentes as ut on ut.utstamp = cte1.utstamp
	
	END
	
GO
Grant Execute On up_marcacoes_Disponibilidades to Public
Grant Control On up_marcacoes_Disponibilidades to Public
go