-- exec up_marcacoes_MarcacoesControloDisp 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_MarcacoesControloDisp]') IS NOT NULL
	drop procedure dbo.up_marcacoes_MarcacoesControloDisp
go

create procedure dbo.up_marcacoes_MarcacoesControloDisp

/* WITH ENCRYPTION */

AS 
BEGIN
	
	Select 
		* 
	from 
		marcacoesServ
	Where
		dataInicio > GETDATE()-1


END

GO
Grant Execute on dbo.up_marcacoes_MarcacoesControloDisp to Public
Grant Control on dbo.up_marcacoes_MarcacoesControloDisp to Public
GO