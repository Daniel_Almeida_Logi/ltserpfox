/*
  exec up_marcacoes_listaAnos 10
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_listaAnos]') IS NOT NULL
	drop procedure up_marcacoes_listaAnos
go

create PROCEDURE up_marcacoes_listaAnos
@anos int



/* WITH ENCRYPTION */

AS

SET NOCOUNT ON
	
	;with Anoslist as 
	(
		select YEAR(GETDATE())-@anos as ano
		union all
		select yl.ano + 1 as ano
		from Anoslist yl
		where yl.ano <= YEAR(GetDate())
	)

	select ano from Anoslist order by ano desc;
			
GO
Grant Execute On up_marcacoes_listaAnos to Public
Grant Control On up_marcacoes_listaAnos to Public
go