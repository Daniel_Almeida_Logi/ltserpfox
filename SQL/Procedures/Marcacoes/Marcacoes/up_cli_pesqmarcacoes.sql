-- exec up_cli_pesqmarcacoes 'Higiene Oral','','T�nia Rita Paiva','C�sar Filipe','11:01','20100919','191'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cli_pesqmarcacoes]') IS NOT NULL
	drop procedure dbo.up_cli_pesqmarcacoes
go

create procedure dbo.up_cli_pesqmarcacoes

@div		as varchar(254), 
@drnome		as varchar(254),
@nome		as varchar(254),
@hinicio	as varchar(254),
@data		as varchar(254),
@mrno		as varchar(254)

/* WITH ENCRYPTION */

AS 
BEGIN

	SET LANGUAGE PORTUGUESE

	Select	convert(bit,0)as sel, mrno, data, hinicio, nome, div, drnome , mrstamp
	from	b_cli_mr (nolock)
	Where	mrno		=		CASE WHEN @mrno		= ''	THEN mrno		ELSE @mrno		END
			AND DATA	=		CASE WHEN @data		= ''	THEN DATA		ELSE @data		END
			AND	hinicio =		CASE WHEN @hinicio	= ''	THEN hinicio	ELSE @hinicio	END
			AND nome	LIKE	CASE WHEN @nome		= ''	THEN nome		ELSE @nome		END+'%'
			AND div		=		CASE WHEN @div		= ''	THEN div		ELSE @div		END
			AND drnome	=		CASE WHEN @drnome	= ''	THEN drnome		ELSE @drnome	END
END

GO
Grant Execute on dbo.up_cli_pesqmarcacoes to Public
GO
Grant Control on dbo.up_cli_pesqmarcacoes to Public
GO