/*
	exec up_marcacoes_indisponibilidades '324'
*/ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_indisponibilidades]') IS NOT NULL
    drop procedure up_marcacoes_indisponibilidades
go

create PROCEDURE up_marcacoes_indisponibilidades
@usernos as varchar(max)

/* WITH ENCRYPTION */
AS


	Select 
		* 
	from 
		b_us_Indisponibilidades
	Where
		b_us_Indisponibilidades.userno in (Select items from up_SplitToTable(@usernos,','))
		
	
GO
Grant Execute On up_marcacoes_indisponibilidades to Public
Grant Control On up_marcacoes_indisponibilidades to Public
go