/*
  exec up_marcacoes_refEntidades 201,''
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_refEntidades]') IS NOT NULL
	drop procedure up_marcacoes_refEntidades
go

create PROCEDURE up_marcacoes_refEntidades
@no as numeric(10,0)
,@ftstamp as varchar(25)

/* WITH ENCRYPTION */

AS

	select 
		ref, refentidade 
	from 
		cpt_val_cli (nolock)
		inner join cpt_conv (nolock) on cpt_val_cli.id_cpt_conv = cpt_conv.id
		inner join B_entidadesUtentes (nolock) on B_entidadesUtentes.eno = cpt_conv.id_entidade
	Where
		B_entidadesUtentes.no = @no
		and cpt_val_cli.ref in (select fi.ref from fi (nolock) where fi.ftstamp = @ftstamp)
		
GO
Grant Execute On up_marcacoes_refEntidades to Public
Grant Control On up_marcacoes_refEntidades to Public
go