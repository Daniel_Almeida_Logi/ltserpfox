/*
  exec up_marcacoes_listaMeses
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_listaMeses]') IS NOT NULL
	drop procedure up_marcacoes_listaMeses
go

create PROCEDURE up_marcacoes_listaMeses

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON
	
	select mes = 1 , descricao = 'Janeiro'
	union all
	select mes = 2 , descricao = 'Fevereiro'
	union all 
	select mes = 3 , descricao = 'Mar�o'
	union all
	select mes = 4 , descricao = 'Abril'
	union all 
	select mes = 5 , descricao = 'Maio'
	union all
	select mes = 6 , descricao = 'Junho'
	union all 
	select mes = 7 , descricao = 'Julho'
	union all
	select mes = 8 , descricao = 'Agosto'
	union all
	select mes = 9 , descricao = 'Setembro'
	union all 
	select mes = 10 , descricao = 'Outubro'
	union all
	select mes = 11 , descricao = 'Novembro'
	union all
	select mes = 12 , descricao = 'Dezembro'
GO
Grant Execute On up_marcacoes_listaMeses to Public
Grant Control On up_marcacoes_listaMeses to Public
go