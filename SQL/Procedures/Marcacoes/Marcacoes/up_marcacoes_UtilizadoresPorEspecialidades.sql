/*
  exec up_marcacoes_UtilizadoresPorEspecialidades '',''
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_UtilizadoresPorEspecialidades]') IS NOT NULL
	drop procedure up_marcacoes_UtilizadoresPorEspecialidades
go

create PROCEDURE up_marcacoes_UtilizadoresPorEspecialidades
@especialidade varchar(50)
,@especialista varchar(150)


/* WITH ENCRYPTION */

AS

SET NOCOUNT ON
	select 
		b_us.nome
		,no = b_us.userno
	from 
		b_us (nolock)
	Where 
		(userno in (
			Select 	
				userno
			From	
				b_usesp (nolock)
			Where
				(b_usesp.especialidade = @especialidade)
				AND nome != 'Administrador de Sistema'
		) or @especialidade = '')
		and b_us.nome = case when @especialista = '' then b_us.nome else @especialista end 
	Order by
		 b_us.userno
	
			
GO
Grant Execute On up_marcacoes_UtilizadoresPorEspecialidades to Public
Grant Control On up_marcacoes_UtilizadoresPorEspecialidades to Public
go
