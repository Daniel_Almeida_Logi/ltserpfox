/* exec up_marcacoes_PeriodosCalculo */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_PeriodosCalculo]') IS NOT NULL
    drop procedure up_marcacoes_PeriodosCalculo
go

create PROCEDURE up_marcacoes_PeriodosCalculo

/* WITH ENCRYPTION */
AS

	Select tipo = 'Minuto'
	union all 
	Select 'Hora'

	
	
GO
Grant Execute On up_marcacoes_PeriodosCalculo to Public
Grant Control On up_marcacoes_PeriodosCalculo to Public
go
