/* exec up_marcacoes_HistoricoSerie 'ADMD8689590-4C5A-4217-97F' */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_HistoricoSerie]') IS NOT NULL
    drop procedure up_marcacoes_HistoricoSerie
go

create PROCEDURE up_marcacoes_HistoricoSerie
@seriestamp as varchar(25)
/* WITH ENCRYPTION */
AS

	Select 
		marcacoes.* 
	from	
		marcacoes 
		inner join b_series ON b_series.serieno = marcacoes.serieno
	where 
		b_series.seriestamp = @seriestamp 
	order by 
		marcacoes.mrno
	desc

GO
Grant Execute On up_marcacoes_HistoricoSerie to Public
Grant Control On up_marcacoes_HistoricoSerie to Public
go