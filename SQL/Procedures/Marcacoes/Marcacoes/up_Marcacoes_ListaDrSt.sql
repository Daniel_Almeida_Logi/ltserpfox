-- Recursos
-- exec up_Marcacoes_ListaDrSt ''
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Marcacoes_ListaDrSt]') IS NOT NULL
	drop procedure up_Marcacoes_ListaDrSt
go

create PROCEDURE up_Marcacoes_ListaDrSt
	@ref as varchar(18)

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

Select 
	nome = UPPER(drnome)
	,duracao
from 
	b_cli_stdr (nolock) where ref = @ref

option (optimize for (@ref UNKNOWN))

GO
Grant Execute On up_Marcacoes_ListaDrSt to Public
grant control on up_Marcacoes_ListaDrSt to Public
go		