/* exec up_marcacoes_ListaTecnicos '' */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_ListaTecnicos]') IS NOT NULL
    drop procedure up_marcacoes_ListaTecnicos
go

create PROCEDURE up_marcacoes_ListaTecnicos
@actstamp varchar(25)

/* WITH ENCRYPTION */
AS

	select 
		 b_us.nome
		 ,userno
		 ,especialidade = isnull((Select Top 1 especialidade from b_usesp (nolock) where b_us.userno = b_usesp.userno order by especialidade),'')
	from 
		b_us (nolock)
	Where
		usstamp not in (
			Select 
				usstamp
			From
				b_seriesTecnicos
			Where
				seriestamp = @actstamp
		)	
	Order by 
		nome, userno
	
GO
Grant Execute On up_marcacoes_ListaTecnicos to Public
Grant Control On up_marcacoes_ListaTecnicos to Public
go