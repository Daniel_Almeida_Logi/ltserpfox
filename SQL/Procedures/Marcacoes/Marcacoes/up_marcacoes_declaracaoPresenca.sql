/* exec up_marcacoes_declaracaoPresenca 'ADMD4DCDEC8-8B64-40C4-A1A' */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_declaracaoPresenca]') IS NOT NULL
    drop procedure up_marcacoes_declaracaoPresenca
go

create PROCEDURE up_marcacoes_declaracaoPresenca
@mrstamp as varchar(25)


/* WITH ENCRYPTION */
AS

	
	set language portuguese
	Select 
		dataDeclaracao = DATENAME(WEEKDAY, GETDATE()) + ', ' + convert(varchar(02),DatePart(day,GETDATE())) + ' ' + UPPER(DateName(Month,GETDATE())) + ', ' + convert(varchar(04),DatePart(YEar,GETDATE()))
		,declaracao =  'Para os devidos efeitos, declara-se que o Sr(�). ' + RTRIM(LTRIM(marcacoes.nome)) + ' esteve presente nas nossas instala��es no dia ' + CONVERT(varchar,marcacoes.data,112) 
		+ ' das ' + marcacoes.hinicio + ' �s ' + LEFT(CONVERT(varchar,getdate(),108),5) + ' a fim de: '
		,data
		,nome
		,hinicio
		,hfim = left(convert(varchar,GETDATE(),108),5) 
	from 
		marcacoes 
	where 
		mrstamp = @mrstamp

	
	
GO
Grant Execute On up_marcacoes_declaracaoPresenca to Public
Grant Control On up_marcacoes_declaracaoPresenca to Public
go