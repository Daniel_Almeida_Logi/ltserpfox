/*
  exec up_marcacoes_listaNiveisUrgencia
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_listaNiveisUrgencia]') IS NOT NULL
	drop procedure up_marcacoes_listaNiveisUrgencia
go

create PROCEDURE up_marcacoes_listaNiveisUrgencia

/* WITH ENCRYPTION */

AS


	Select nivel ='EMERGENTE'
	union all
	Select 'MTO. URGENTE'
	union all
	Select 'URGENTE'
	union all
	Select 'POUCO URGENTE'
	union all
	Select 'N�O URGENTE'
	
	
	
GO
Grant Execute On up_marcacoes_listaNiveisUrgencia to Public
Grant Control On up_marcacoes_listaNiveisUrgencia to Public
go
