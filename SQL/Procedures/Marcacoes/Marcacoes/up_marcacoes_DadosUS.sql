/* exec up_marcacoes_DadosUS 'nome' */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_DadosUS]') IS NOT NULL
    drop procedure up_marcacoes_DadosUS
go

create PROCEDURE up_marcacoes_DadosUS
@nome varchar(254)

/* WITH ENCRYPTION */
AS

	select 
		 nome
		 ,userno
		 ,duracao = 0
		 ,usstamp
		 ,dvnome = isnull(dvnome,'')
	from 
		b_us (nolock)
		left join b_cli_drdv (nolock) on b_us.userno = b_cli_drdv.drno
	Where
		nome = @nome
		
GO
Grant Execute On up_marcacoes_DadosUS to Public
Grant Control On up_marcacoes_DadosUS to Public
go

