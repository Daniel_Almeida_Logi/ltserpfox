/*
  exec up_marcacoes_PvpsSemConvencao '1000001,1000002'
  		exec up_marcacoes_PvpsSemConvencao 'SERV1'
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_PvpsSemConvencao]') IS NOT NULL
	drop procedure up_marcacoes_PvpsSemConvencao
go

create PROCEDURE up_marcacoes_PvpsSemConvencao
@refs as varchar(max) = ''
/* WITH ENCRYPTION */

AS
	
		
	select 
		st.ref
		,empresa.site
		,pvp = st.epv1
	from 
		st (nolock)
		inner join empresa (nolock) on empresa.no = st.site_nr
	where
		st.ref in (select * from up_SplitToTable(@refs,','))
			
GO
Grant Execute On up_marcacoes_PvpsSemConvencao to Public
Grant Control On up_marcacoes_PvpsSemConvencao to Public
go
