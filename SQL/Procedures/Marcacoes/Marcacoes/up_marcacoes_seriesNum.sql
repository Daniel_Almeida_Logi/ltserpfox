/*
  exec up_marcacoes_seriesNum
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_seriesNum]') IS NOT NULL
	drop procedure up_marcacoes_seriesNum
go

create PROCEDURE up_marcacoes_seriesNum


/* WITH ENCRYPTION */

AS

	select numero = isnull(MAX(serieno),0) +1 from b_series
		
GO
Grant Execute On up_marcacoes_seriesNum to Public
Grant Control On up_marcacoes_seriesNum to Public
go
