/* exec up_marcacoes_servicos */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_servicos]') IS NOT NULL
    drop procedure up_marcacoes_servicos
go

create PROCEDURE up_marcacoes_servicos

/* WITH ENCRYPTION */
AS

	

	SELECT	
		ref
		,designacao = design 
	FROM	
		st  (nolock)
	Where	
		u_servmarc = 1 
	ORDER BY 
		design asc	


	
	
GO
Grant Execute On up_marcacoes_servicos to Public
Grant Control On up_marcacoes_servicos to Public
go