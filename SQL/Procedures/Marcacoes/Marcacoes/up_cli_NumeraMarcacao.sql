-- exec up_cli_NumeraMarcacao
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cli_NumeraMarcacao]') IS NOT NULL
	drop procedure dbo.up_cli_NumeraMarcacao
go

create procedure dbo.up_cli_NumeraMarcacao

/* WITH ENCRYPTION */

AS 
BEGIN
	Select ISNULL(MAX(mrno),0)+1 as numero FROM b_cli_mr (nolock)
END

GO
Grant Execute on dbo.up_cli_NumeraMarcacao to Public
GO
Grant Control on dbo.up_cli_NumeraMarcacao to Public
GO