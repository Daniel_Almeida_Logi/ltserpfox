/*
  exec up_marcacoes_ServicosDisponibilidades
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_ServicosDisponibilidades]') IS NOT NULL
	drop procedure up_marcacoes_ServicosDisponibilidades
go

create PROCEDURE up_marcacoes_ServicosDisponibilidades

/* WITH ENCRYPTION */

AS

	Select 
		ref, seriestamp
	from 
		 b_us_disp_resursos (nolock)
	
GO
Grant Execute On up_marcacoes_ServicosDisponibilidades to Public
Grant Control On up_marcacoes_ServicosDisponibilidades to Public
go