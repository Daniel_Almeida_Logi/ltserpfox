-- exec up_cli_ListaEspecialidades 1
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cli_ListaEspecialidades]') IS NOT NULL
	drop procedure dbo.up_cli_ListaEspecialidades
go

create procedure dbo.up_cli_ListaEspecialidades
@tipo as int,
@ref as varchar(50)

/* WITH ENCRYPTION */

AS 
BEGIN
	IF @tipo = 0
	BEGIN
		SELECT '' as nome 
		UNION ALL
		SELECT UPPER(div) as nome FROM dv  (nolock)
		ORDER BY nome asc
	END
	ELSE
	BEGIN
		SELECT	UPPER(div) as nome 
		FROM	dv  (nolock)
		WHERE	UPPER(div) not in (Select UPPER(div) from b_cli_stdv where ref = @ref)
		ORDER BY nome asc
	END
END

GO
Grant Execute on dbo.up_cli_ListaEspecialidades to Public
Grant Control on dbo.up_cli_ListaEspecialidades to Public
GO