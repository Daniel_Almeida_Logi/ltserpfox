-- exec up_cli_ListaServicosMarc  0
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cli_ListaServicosMarc]') IS NOT NULL
	drop procedure dbo.up_cli_ListaServicosMarc
go

create procedure dbo.up_cli_ListaServicosMarc
@tipo as int

/* WITH ENCRYPTION */

AS 
BEGIN
	IF @tipo = 0
	BEGIN
		SELECT '' as ref, '' as design
		UNION ALL
		SELECT	ref,design 
		FROM	st  (nolock)
		Where	u_servmarc = 1 
		ORDER BY design asc
	END
	ELSE
	BEGIN
		SELECT	ref,design 
		FROM	st  (nolock)
		Where	u_servmarc = 1 
		ORDER BY design asc
	END
END

GO
Grant Execute on dbo.up_cli_ListaServicosMarc to Public
GO
Grant Control on dbo.up_cli_ListaServicosMarc to Public
GO