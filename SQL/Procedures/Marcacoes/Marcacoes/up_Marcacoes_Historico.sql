-- exec up_Marcacoes_Historico 0
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Marcacoes_Historico]') IS NOT NULL
	drop procedure up_Marcacoes_Historico
go

create PROCEDURE up_Marcacoes_Historico

@idno as numeric(9,0)

/* WITH ENCRYPTION */

AS 
BEGIN
	
	Select top 30 data,hinicio,hfim,div as dvnome,drnome,estado,ref,design from b_cli_mr (nolock) Where idno = @idno and idno != 0 order by data desc 
	
END

GO
Grant Execute On up_Marcacoes_Historico to Public
GO
Grant Control On up_Marcacoes_Historico to Public
GO