/* exec up_marcacoes_TiposCalculo */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_TiposCalculo]') IS NOT NULL
    drop procedure up_marcacoes_TiposCalculo
go

create PROCEDURE up_marcacoes_TiposCalculo

/* WITH ENCRYPTION */
AS

	Select tipo = 'Servi�os'
	union all 
	Select 'Recursos'

	
	
GO
Grant Execute On up_marcacoes_TiposCalculo to Public
Grant Control On up_marcacoes_TiposCalculo to Public
go
