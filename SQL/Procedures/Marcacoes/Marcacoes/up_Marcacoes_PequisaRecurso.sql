/* exec up_Marcacoes_PequisaRecurso ''
*/ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Marcacoes_PequisaRecurso]') IS NOT NULL
    drop procedure up_Marcacoes_PequisaRecurso
go

create PROCEDURE up_Marcacoes_PequisaRecurso
@stamp as varchar(25)

/* WITH ENCRYPTION */
AS

	Select	
		*
	from	
		recursos (nolock)
	Where	
		recursosstamp = @stamp		
	
GO
Grant Execute On up_Marcacoes_PequisaRecurso to Public
Grant Control On up_Marcacoes_PequisaRecurso to Public
go