/* exec up_marcacoes_listaAgenda 2014,12 */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_listaAgenda]') IS NOT NULL
    drop procedure up_marcacoes_listaAgenda
go

create PROCEDURE up_marcacoes_listaAgenda
@ano as numeric(4)
,@mes as numeric(2)


/* WITH ENCRYPTION */
AS

	IF @ano = 0
	begin
		set @ano = 1900
	End
	IF @mes = 0
	begin
		set @mes = 1
	End


	UPDATE
		marcacoes
	set 
		estado = 'ATRASADO'
	from 
		marcacoes
	Where
		marcacoes.data = convert(varchar,GETDATE(),112)
		and hinicio < convert(varchar,GETDATE(),108)
		and estado = 'MARCADO'
		
	select 
		mrstamp
		,data
		,dataFim
		,mrno
		,hinicio
		,hfim
		,utstamp
		,nome
		,no
		,estab
		,obs
		,serieno
		,serienome
		,sessao
		,estado
		,SerieDescricao = convert(varchar(254),LEFT(serienome,245) + ' [' + convert(varchar,sessao) + ']')
		,site
		,combinacao
		,especialistas = isnull(convert(varchar(254),(select 
							marcacoesRecurso.nome + ', '
						from 
							marcacoesRecurso (nolock) 
						Where 
							marcacoesRecurso.mrstamp = marcacoes.mrstamp
						FOR XML PATH(''))),'')
		,especialidade = isnull(convert(varchar(254),(select 
							B_usesp.especialidade + ', '
						from 
							marcacoesRecurso (nolock) 
							left join B_usesp (nolock) on B_usesp.userno = marcacoesRecurso.no
						Where 
							marcacoesRecurso.mrstamp = marcacoes.mrstamp
						FOR XML PATH(''))),'')
		,id_us
		,data_cri
		,id_us_alt
		,data_alt
		,nivelurgencia
		,faturado = case when exists (	Select 
											mrstamp 
										from 
											marcacoesServ (nolock) 
										where 
											marcacoesServ.mrstamp = marcacoes.mrstamp and fu = 1 
										) then CONVERT(bit,1) 
								else CONVERT(bit,0) end
		,motivoCancelamento
		,talaolevantamento
		,LocalEntrega
		,EntregueTLev
		,designEntrega
		,dataEntrega
		,userEntrega
		,dataLevantamento
		,declaracaoPresenca = 'Para os devidos efeitos, declara-se que o Sr(�). ' + RTRIM(LTRIM(marcacoes.nome)) + ' esteve presente nas nossas instala��es no dia ' + left(CONVERT(varchar,marcacoes.data,126),10) 
		+ ' das ' + marcacoes.hinicio + ' �s ' + LEFT(CONVERT(varchar,getdate(),108),5) + ' a fim de: '
	from 
		marcacoes
	Where
		marcacoes.data >= DateAdd(day, 1 - 1, DateAdd(month, @mes - 1, DateAdd(Year, @ano-1900, 0)))
	order by 
		marcacoes.data
		,marcacoes.hinicio
	
	
	
GO
Grant Execute On up_marcacoes_listaAgenda to Public
Grant Control On up_marcacoes_listaAgenda to Public
go