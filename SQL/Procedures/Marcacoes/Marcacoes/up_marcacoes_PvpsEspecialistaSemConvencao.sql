
if OBJECT_ID('[dbo].[up_marcacoes_PvpsEspecialistaSemConvencao]') IS NOT NULL
    drop procedure up_marcacoes_PvpsEspecialistaSemConvencao
go

create PROCEDURE up_marcacoes_PvpsEspecialistaSemConvencao
@refs as varchar(max) = ''
/* WITH ENCRYPTION */
AS

	

	select 
		B_cli_stRecursos.no
		,st.ref
		,st.design
		,B_cli_stRecursos.pvp
		,empresa.site
	from 
		B_cli_stRecursos
		inner join st on st.ref = B_cli_stRecursos.ref and B_cli_stRecursos.site_nr = st.site_nr
		inner join empresa on empresa.no = st.site_nr
	Where
		B_cli_stRecursos.tipo = 'Utilizador'
		and st.ref in (select * from up_SplitToTable(@refs,','))
	Order by
		st.ref
		
	
GO
Grant Execute On up_marcacoes_PvpsEspecialistaSemConvencao to Public
Grant Control On up_marcacoes_PvpsEspecialistaSemConvencao to Public
go