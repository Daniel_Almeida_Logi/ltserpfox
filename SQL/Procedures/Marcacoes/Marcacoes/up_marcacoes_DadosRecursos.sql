/* exec up_marcacoes_DadosRecursos */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_DadosRecursos]') IS NOT NULL
    drop procedure up_marcacoes_DadosRecursos
go

create PROCEDURE up_marcacoes_DadosRecursos

/* WITH ENCRYPTION */
AS
	select 
		* 
	from 
		b_series (nolock)
	where
		b_series.tipo in ('Recursos','Utilizadores')
	
		
	
GO
Grant Execute On up_marcacoes_DadosRecursos to Public
Grant Control On up_marcacoes_DadosRecursos to Public
go
