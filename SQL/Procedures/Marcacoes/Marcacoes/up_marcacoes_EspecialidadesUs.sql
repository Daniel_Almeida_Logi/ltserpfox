/*
  exec up_marcacoes_EspecialidadesUs 
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_EspecialidadesUs]') IS NOT NULL
	drop procedure up_marcacoes_EspecialidadesUs
go

create PROCEDURE up_marcacoes_EspecialidadesUs
/* WITH ENCRYPTION */

AS

	select 
		distinct
		B_us.userno
		,B_us.nome
		,especialidade = isnull(especialidade,'')
		,local = isnull(B_series.site,'')
	from 
		B_us  
		inner join B_series on  B_us.userno = B_series.no and B_series.tipo = 'Utilizadores'
		left join B_usesp on B_usesp.userno = B_us.userno 
	ORDER BY 
		local
		,especialidade
		,nome

GO
Grant Execute On up_marcacoes_EspecialidadesUs to Public
Grant Control On up_marcacoes_EspecialidadesUs to Public
go