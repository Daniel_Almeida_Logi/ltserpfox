/*
  exec up_marcacoes_dadosTalaoLEvantamento ''
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_dadosTalaoLEvantamento]') IS NOT NULL
	drop procedure up_marcacoes_dadosTalaoLEvantamento
go

create PROCEDURE up_marcacoes_dadosTalaoLEvantamento
@mrstamp as varchar(25)
/* WITH ENCRYPTION */

AS

	select 
		ref
		,design 
		,marcacoes.localEntrega
		,mrno = RTRIM(LTRIM(REPLICATE('0',10-LEN(marcacoes.mrno))))+RTRIM(LTRIM(STR(marcacoes.mrno)))
		,marcacoes.nome
		,marcacoes.no
		,marcacoes.dataLevantamento
	from 
		marcacoes (nolock)
		inner join marcacoesServ (nolock) on marcacoes.mrstamp = marcacoesServ.mrstamp
	Where
		marcacoes.talaolevantamento = 1
		and marcacoesServ.mrstamp = @mrstamp
		
		
		
	
GO
Grant Execute On up_marcacoes_dadosTalaoLEvantamento to Public
Grant Control On up_marcacoes_dadosTalaoLEvantamento to Public
go