/*
  exec up_marcacoes_ValidaAlertasUtilizador 8
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_ValidaAlertasUtilizador]') IS NOT NULL
	drop procedure up_marcacoes_ValidaAlertasUtilizador
go

create PROCEDURE up_marcacoes_ValidaAlertasUtilizador
@userno as numeric(9,0)

/* WITH ENCRYPTION */

AS

	Select * from marcacoes_alertas where userno = @userno
		
GO
Grant Execute On up_marcacoes_ValidaAlertasUtilizador to Public
Grant Control On up_marcacoes_ValidaAlertasUtilizador to Public
go
