-- exec up_cli_ListaEspecialistas 1,''
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cli_ListaEspecialistas]') IS NOT NULL
	drop procedure dbo.up_cli_ListaEspecialistas
go

create procedure dbo.up_cli_ListaEspecialistas
@tipo as int,
@ref as varchar(50)

/* WITH ENCRYPTION */

AS 
BEGIN
	IF @tipo = 0
	BEGIN
		Select '' as nome 
		union all
		Select UPPER(nome) from dr (nolock) 
		Order by nome asc
	END
	ELSE
	BEGIN
		Select UPPER(nome) as nome
		from dr (nolock) where UPPER(nome) not in (Select UPPER(drnome) from b_cli_stdr (nolock) where ref = @ref)
		Order by nome asc
	END
	
END

GO
Grant Execute on dbo.up_cli_ListaEspecialistas to Public
GO
Grant Control on dbo.up_cli_ListaEspecialistas to Public
GO