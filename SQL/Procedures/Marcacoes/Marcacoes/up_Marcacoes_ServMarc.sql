-- Recursos
-- exec up_Marcacoes_ServMarc '1406658IF58J '
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Marcacoes_ServMarc]') IS NOT NULL
	drop procedure up_Marcacoes_ServMarc
go

create PROCEDURE up_Marcacoes_ServMarc
@mrstamp as char(25)


/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

	Select	
		sel = CONVERT(bit,0)
		,marcacoesServ.servmrstamp
		,marcacoesServ.mrstamp
		,marcacoesServ.ref
		,marcacoesServ.design
		,marcacoesServ.duracao
		,marcacoesServ.qtt
		,marcacoesServ.dataInicio
		,marcacoesServ.dataFim
		,marcacoesServ.hinicio
		,marcacoesServ.hfim
		,marcacoesServ.ordem
		,marcacoesServ.compart
		,marcacoesServ.pvp
		,marcacoesServ.total
		,marcacoesServ.fu
		,marcacoesServ.fe
		,honorario = case when fn_honorarios.honorario is null then CONVERT(bit,0) else CONVERT(bit,1) end
		,valorHonorario = fn_honorarios.honorario
		,marcacoesServ.ousrinis
		,marcacoesServ.ousrhora
		,marcacoesServ.ousrdata
		,marcacoesServ.usrinis
		,marcacoesServ.usrdata
		,fn_honorarios.HonorarioDeducao
		,fn_honorarios.HonorarioValor
		,fn_honorarios.HonorarioBi
		,fn_honorarios.HonorarioTipo
		,marcacoesServ.usrhora
		,marcacoesServ.ufistamp
		,marcacoesServ.efistamp
		,marcacoesServ.valorAdicional
		,marcacoesServ.EstadohonEsp
		,marcacoesServ.EstadohonOp
	from	
		marcacoesServ (nolock)
		left join fn_honorarios (nolock) on marcacoesServ.servmrstamp = fn_honorarios.servmrstamp
	where	
		marcacoesServ.mrstamp = @mrstamp
	

GO
Grant Execute On up_Marcacoes_ServMarc to Public
grant control on up_Marcacoes_ServMarc to Public
go	