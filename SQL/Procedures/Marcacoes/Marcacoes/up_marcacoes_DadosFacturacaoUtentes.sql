
-- exec up_marcacoes_DadosFacturacaoUtentes 'ADM603541E8-64E1-403D-9FA'
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_marcacoes_DadosFacturacaoUtentes]') IS NOT NULL
    drop procedure up_marcacoes_DadosFacturacaoUtentes
go

create PROCEDURE up_marcacoes_DadosFacturacaoUtentes
@servmrstamp varchar(25)

/* WITH ENCRYPTION */
AS

select 
	sel = CONVERT(bit,0)
	,Entidade = isnull(cpt_marcacoes.entidade,'')
	,EntidadeNo = isnull(cpt_marcacoes.entidadeno,0)
	,EntidadeEstab = isnull(cpt_marcacoes.entidadeestab,0)
	,marcacoes.mrno
	,dataInicio = marcacoesServ.dataInicio
	,dataFim = marcacoesServ.dataFim
	,marcacoesServ.hinicio
	,marcacoesServ.hfim
	,marcacoes.nome
	,marcacoes.no
	,marcacoes.estab
	,marcacoesServ.ref
	,marcacoesServ.design
	,marcacoesServ.pvp
	,marcacoesServ.qtt
	,total = marcacoesServ.pvp * marcacoesServ.qtt
	,tipoCompart = isnull(cpt_marcacoes.tipoCompart,'')
	,compart = isnull(cpt_marcacoes.compart,0)
	,maximo = isnull(cpt_marcacoes.maximo,0)
	,marcacoes.obs
	,NBENEF = ''
	,faturarUtente = case when marcacoesServ.fu = 0 then marcacoesServ.total else 0 end
	,faturarEntidade =  case when marcacoesServ.fe = 0 then marcacoesServ.compart else 0 end
	,tabiva = isnull(st.tabiva,4)
	,iva = isnull(taxasiva.taxa,0)
	,ivaincl = isnull(st.ivaincl,0)
	,marcacoesServ.servmrstamp
from 
	marcacoesServ
	inner join marcacoes on marcacoes.mrstamp = marcacoesServ.mrstamp
	left join B_utentes on marcacoes.utstamp = B_utentes.utstamp
	left join cpt_marcacoes on marcacoesServ.servmrstamp = cpt_marcacoes.servmrstamp
	left join cpt_val_cli on cpt_val_cli.id = cpt_marcacoes.id_cp
	left join st on st.ref = marcacoesServ.ref
	left join taxasiva on taxasiva.codigo = st.tabiva
Where
	marcacoesServ.servmrstamp = @servmrstamp

		
	
GO
Grant Execute On up_marcacoes_DadosFacturacaoUtentes to Public
Grant Control On up_marcacoes_DadosFacturacaoUtentes to Public
go