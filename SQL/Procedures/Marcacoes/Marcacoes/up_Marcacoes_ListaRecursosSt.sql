-- Recursos
-- exec up_Marcacoes_ListaRecursosSt 'CONSCIRURGIAGERAL',1
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Marcacoes_ListaRecursosSt]') IS NOT NULL
	drop procedure up_Marcacoes_ListaRecursosSt
go

create PROCEDURE up_Marcacoes_ListaRecursosSt
@ref as varchar(18)
,@site_nr as tinyint

/* WITH ENCRYPTION */

AS


	Select 
		sel = CONVERT(bit,0), *
	from 
		b_cli_stRecursos (nolock) 
	where 
		RTRIM(LTRIM(ref)) = RTRIM(LTRIM(@ref))
		and site_nr = @site_nr
	
	
GO
Grant Execute On up_Marcacoes_ListaRecursosSt to Public
grant control on up_Marcacoes_ListaRecursosSt to Public
go	