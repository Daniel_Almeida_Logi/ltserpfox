-- Verifica a que documentos um utilizador/grupo tem acesso --
-- exec up_cli_validaAcesso 1, 'administrador', 'Marca��es - Visualiza apenas as suas Marca��es'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cli_validaAcesso]') IS NOT NULL
	drop procedure dbo.up_cli_validaAcesso
go

create procedure dbo.up_cli_validaAcesso

@userno int,
@grupo	varchar(60),
@resumo	varchar(60)

/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

declare @site as varchar(60)
set @site = isnull((select top 1 loja from b_us where userno = @userno),'')

Select dbo.up_PerfilFacturacao(@userno, @grupo, @resumo, @site) as valor

GO
Grant Execute on dbo.up_cli_validaAcesso to Public
grant control on dbo.up_cli_validaAcesso to Public
go