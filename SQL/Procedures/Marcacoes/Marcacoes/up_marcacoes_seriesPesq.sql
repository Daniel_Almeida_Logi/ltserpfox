/*
  exec up_marcacoes_seriesPesq '',0,'',0,1
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_seriesPesq]') IS NOT NULL
	drop procedure up_marcacoes_seriesPesq
go

create PROCEDURE up_marcacoes_seriesPesq
@serienome as varchar(254)
,@serieno as numeric(9,0)
,@tipo as varchar(30)
,@campos as bit = 0
,@no as numeric(9) = 0

/* WITH ENCRYPTION */

AS

	IF @campos = 0
	begin
	
		select 
			sel = CONVERT(bit,0), * 
		from 
			b_series
		Where 
			serienome = case when @serienome = '' then serienome else @serienome end 
			and serieno = case when @serieno = 0  then serieno else @serieno end
			and tipo = case when @tipo = '' then tipo else @tipo end
			and b_series.no = case when @no = 0 then b_series.no else @no end
			and b_series.tipo != 'Utilizadores' /* a gest�o de disponibnilidades � feita no ecra de utilizadores*/
		Order by 
			serieno desc
	end
	else
	begin
		select
			top 0 
			sel = CONVERT(bit,0), * 
		from 
			b_series
	end
GO
Grant Execute On up_marcacoes_seriesPesq to Public
Grant Control On up_marcacoes_seriesPesq to Public
go
