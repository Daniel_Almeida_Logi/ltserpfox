-- exec up_cli_DiaNome '20120220'

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cli_DiaNome]') IS NOT NULL
	drop procedure dbo.up_cli_DiaNome
go

create procedure dbo.up_cli_DiaNome

@data as datetime

/* WITH ENCRYPTION */

AS 
BEGIN
	
	Set LANGUAGE PORTUGUESE
	select UPPER(datename(dw,@data)) as DiaNome
	
END


GO
Grant Execute on dbo.up_cli_DiaNome to Public
GO
Grant Control on dbo.up_cli_DiaNome to Public
GO