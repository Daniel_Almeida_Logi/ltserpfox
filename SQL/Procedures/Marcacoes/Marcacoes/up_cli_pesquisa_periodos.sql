-- exec up_cli_pesquisa_periodos '',''
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cli_pesquisa_periodos]') IS NOT NULL
	drop procedure dbo.up_cli_pesquisa_periodos
go

create procedure dbo.up_cli_pesquisa_periodos

@drnome as varchar(254),
@dvnome as varchar(254)

/* WITH ENCRYPTION */


AS 
BEGIN
	
	;with cte1 as (
	
		Select	distinct convert(bit,0) as sel,
				b_cli_drdv.drnome, b_cli_drdv.drno,dvnome
		from	b_cli_drdv (nolock)
		Where	b_cli_drdv.drnome		like @drnome	+ '%' 
				AND	b_cli_drdv.dvnome	like @dvnome	+ '%' 
		Group by b_cli_drdv.drnome, b_cli_drdv.drno,dvnome
		
		Union all 
		
		Select	distinct convert(bit,0) as sel,
				b_cli_pd.drnome, b_cli_pd.drno,dvnome
		from	b_cli_pd (nolock)
		Where	b_cli_pd.drnome		like @drnome	+ '%' 
				AND	b_cli_pd.dvnome	like @dvnome	+ '%' 
		Group by b_cli_pd.drnome, b_cli_pd.drno,dvnome
	
	)
	
	
	Select * 
	from cte1
	Group by cte1.sel, cte1.drnome, cte1.drno,dvnome

	
END

GO
Grant Execute on dbo.up_cli_pesquisa_periodos to Public
Grant Control on dbo.up_cli_pesquisa_periodos to Public
GO