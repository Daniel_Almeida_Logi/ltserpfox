/* exec up_marcacoes_mensal 2015, 01 */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_mensal]') IS NOT NULL
    drop procedure up_marcacoes_mensal
go

create PROCEDURE up_marcacoes_mensal
@ano as numeric(4),
@mes as numeric(2)


/* WITH ENCRYPTION */
AS

	 
	select 
		*,
		especialistas = isnull(convert(varchar(254),(select 
							marcacoesRecurso.nome + ', '
						from 
							marcacoesRecurso (nolock) 
						Where 
							marcacoesRecurso.mrstamp = marcacoes.mrstamp
						FOR XML PATH(''))),'')
		,especialidade = isnull(convert(varchar(254),(select 
							B_usesp.especialidade + ', '
						from 
							marcacoesRecurso (nolock) 
							left join B_usesp (nolock) on B_usesp.userno = marcacoesRecurso.no
						Where 
							marcacoesRecurso.mrstamp = marcacoes.mrstamp
						FOR XML PATH(''))),'')
	from 
		marcacoes
	Where
		year(data) = @ano
		and month(data) = @mes
	
	
GO
Grant Execute On up_marcacoes_mensal to Public
Grant Control On up_marcacoes_mensal to Public
go