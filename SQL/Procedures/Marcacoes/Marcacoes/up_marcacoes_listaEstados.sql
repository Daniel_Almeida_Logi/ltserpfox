/*
  exec up_marcacoes_listaEstados
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_listaEstados]') IS NOT NULL
	drop procedure up_marcacoes_listaEstados
go

create PROCEDURE up_marcacoes_listaEstados

/* WITH ENCRYPTION */

AS

	Select estado ='PEDIDO'
	union all
	Select 'MARCADO'
	union all
	Select 'ATRASADO'
	union all
	Select 'CANCELADO'
	union all
	Select 'PRESENTE'
	union all
	Select 'TRATAMENTO'
	union all
	Select 'EFETUADO'
	
	
GO
Grant Execute On up_marcacoes_listaEstados to Public
Grant Control On up_marcacoes_listaEstados to Public
go
