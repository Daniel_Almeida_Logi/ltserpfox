/* exec up_marcacoes_atualizaEstado */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_atualizaEstado]') IS NOT NULL
    drop procedure up_marcacoes_atualizaEstado
go

create PROCEDURE up_marcacoes_atualizaEstado
@servmrstamp as varchar(25)


/* WITH ENCRYPTION */
AS
	
	/*Atualiza estado da Marca��o*/	
	declare @mrstamp as varchar(25)
	set @mrstamp = (select mrstamp from marcacoesServ where servmrstamp = @servmrstamp)
	
	
	UPDATE 
		marcacoesServ
	set
		fu = 1
	Where
		marcacoesServ.servmrstamp = @servmrstamp
		
	
	if not exists (select * from marcacoesServ where mrstamp = @mrstamp and fu = 0)
	Begin
		update marcacoes set estado = 'FATURADO' where mrstamp = @mrstamp
	End	

GO
Grant Execute On up_marcacoes_atualizaEstado to Public
Grant Control On up_marcacoes_atualizaEstado to Public
go