/* exec up_marcacoes_numero '' */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_numero]') IS NOT NULL
    drop procedure up_marcacoes_numero
go

create PROCEDURE up_marcacoes_numero


/* WITH ENCRYPTION */
AS

	select 
		mrno = isnull(MAX(mrno),0) + 1
	from 
		marcacoes
	
	
GO
Grant Execute On up_marcacoes_numero to Public
Grant Control On up_marcacoes_numero to Public
go