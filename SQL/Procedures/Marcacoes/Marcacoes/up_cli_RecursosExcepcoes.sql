-- exec up_cli_RecursosExcepcoes '001'

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cli_RecursosExcepcoes]') IS NOT NULL
	drop procedure dbo.up_cli_RecursosExcepcoes
go

create procedure dbo.up_cli_RecursosExcepcoes

@servico as char(18)

/* WITH ENCRYPTION */

AS 
BEGIN
	
	Select 1
	
END

GO
Grant Execute on dbo.up_cli_RecursosExcepcoes to Public
GO
Grant Control on dbo.up_cli_RecursosExcepcoes to Public
GO