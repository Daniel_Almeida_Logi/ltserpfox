/* exec up_marcacoes_MrTecnicos '' */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_MrTecnicos]') IS NOT NULL
    drop procedure up_marcacoes_MrTecnicos
go

create PROCEDURE up_marcacoes_MrTecnicos
@ano numeric(4,0),
@mes numeric(2,0)

/* WITH ENCRYPTION */
AS
	
	Select 
		* 
	From
		b_cli_mrTecnicos
		inner join B_us on b_cli_mrTecnicos.usstamp = B_us.usstamp
		inner join b_cli_mr on b_cli_mr.mrstamp = b_cli_mrTecnicos.mrstamp
	where
		year(b_cli_mr.data) = @ano
		and month(b_cli_mr.data) = @mes
	
GO
Grant Execute On up_marcacoes_MrTecnicos to Public
Grant Control On up_marcacoes_MrTecnicos to Public
go