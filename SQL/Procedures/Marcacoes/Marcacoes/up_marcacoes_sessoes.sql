/*
  exec up_marcacoes_sessoes  62
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_sessoes]') IS NOT NULL
	drop procedure up_marcacoes_sessoes
go

create PROCEDURE up_marcacoes_sessoes
@serieno as numeric(9,0),
@site as varchar(60) = ''
/* WITH ENCRYPTION */

AS

	Select 
		sessao = ROW_NUMBER() OVER(ORDER BY data asc, marcacoes.hinicio asc)
		,ano = YEAR(marcacoes.data)
		,mes = MONTH(marcacoes.data)
		,dia = DAY(marcacoes.data)
		,marcacoes.hinicio
		,marcacoes.hfim
		,combinacao
		,mrstamp
		,serieno
	from 
		marcacoes (nolock)
	where 
		serieno = @serieno
		and marcacoes.site = case when @site = '' then marcacoes.site else @site end
	Order
		by marcacoes.data
	
	
GO
Grant Execute On up_marcacoes_sessoes to Public
Grant Control On up_marcacoes_sessoes to Public
go
