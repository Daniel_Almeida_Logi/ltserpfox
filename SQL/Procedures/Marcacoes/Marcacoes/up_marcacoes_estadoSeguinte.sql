/*
  	exec up_marcacoes_estadoSeguinte 'ATRASADO','ADM911FEA4E-7E2E-4D69-9D8'
  	select * from marcacoes where mrstamp = 'ADM911FEA4E-7E2E-4D69-9D8'
  			exec up_marcacoes_estadoSeguinte 'RESERVADO','ADM911FEA4E-7E2E-4D69-9D8'
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_estadoSeguinte]') IS NOT NULL
	drop procedure up_marcacoes_estadoSeguinte
go

create PROCEDURE up_marcacoes_estadoSeguinte
@estado_actual as  varchar(15)
,@mrstamp as varchar(25)
/* WITH ENCRYPTION */

AS



	update 
		marcacoes
	set
		marcacoes.estado = 
			Case 
				when @estado_actual = 'PEDIDO' then 'MARCADO'
				when @estado_actual = 'MARCADO' then 'ATRASADO'
				when @estado_actual = 'ATRASADO' then 'CANCELADO'
				when @estado_actual = 'CANCELADO' then 'PRESENTE'
				when @estado_actual = 'PRESENTE' then 'TRATAMENTO'
				when @estado_actual = 'TRATAMENTO' then 'EFETUADO'
				else ''
			end,
		marcacoes.nivelUrgencia = 
			Case 
				when @estado_actual = 'CANCELADO' or @estado_actual = 'PRESENTE' then 'N�O URGENTE'
				else ''
			end	
	Where
		marcacoes.mrstamp = @mrstamp
		and marcacoes.estado in(
			'PEDIDO'
			,'MARCADO'
			,'ATRASADO'
			,'CANCELADO'
			,'PRESENTE'
			,'TRATAMENTO'
		)
	

	
GO
Grant Execute On up_marcacoes_estadoSeguinte to Public
Grant Control On up_marcacoes_estadoSeguinte to Public
go
