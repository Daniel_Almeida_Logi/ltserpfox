-- exec up_cli_marcacoes '','','2012','04','18'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cli_marcacoes]') IS NOT NULL
	drop procedure dbo.up_cli_marcacoes
go

create procedure dbo.up_cli_marcacoes

@drnome as varchar(254),
@dvnome as varchar(254),
@ano	as varchar(4),
@mes	as varchar(2),
@dia	as varchar(2)

/* WITH ENCRYPTION */

AS 
BEGIN

SET LANGUAGE PORTUGUESE
Set @dia = (CASE WHEN LEN(RTRIM(LTRIM(@dia))) = 1 THEN '0'+@dia ELSE @dia END)
Set @mes = (CASE WHEN LEN(RTRIM(LTRIM(@mes))) = 1 THEN '0'+@mes ELSE @mes END)

;with 	
	/* Dados de Marca��es Para o Dia */
	cte1 as ( 
		Select	mrstamp,drdvstamp, mrno, nome, idno, data, div, coddiv,drnome, drno, hinicio, hfim, UPPER(datename(dw, data)) as dia,estado, obs, speriodo, ref, design
		FROM	b_cli_mr (nolock)
		Where	data = convert(datetime,@ano+@mes+@dia)
			
	)
	
	/* Disponibilidades Por Dr e Dv */
	/*cte2 as(
		SELECT	drdvstamp, drnome, drno, dvnome, dvno, dia, hinicio, hfim, inactivo, dupli,capacidade, marcada,convert(varchar(20),'') as estado, 0 as speriodo, '' as ref, '' as design
		FROM	b_cli_drdv (nolock)
		WHERE	dia = UPPER(datename(dw,convert(datetime,@ano+@mes+@dia)))
				AND b_cli_drdv.drnome = CASE WHEN @drnome = '' THEN  b_cli_drdv.drnome ELSE @drnome END
				AND b_cli_drdv.dvnome = CASE WHEN @dvnome = '' THEN  b_cli_drdv.dvnome ELSE @dvnome END
	)*/

	

	Select	'mrstamp'		= isnull(cte1.mrstamp,''),
			'mr_drdvstamp'	= isnull(cte1.drdvstamp,''),
			'drdvstamp'		= isnull(cte1.drdvstamp,''),
			'mrno'			= ISNULL(cte1.mrno,0),
			'nome'			= isnull(cte1.nome,''),
			'idno'			= isnull(cte1.idno,0),
			'data'			= isnull(cte1.data,''),
			'drnome'		= isnull(cte1.drnome,''),
			'drno'			= isnull(cte1.drno,0),
			'dvnome'		= isnull(cte1.div,''),
			'dvno'			= isnull(cte1.coddiv,0),
			'dia'			= isnull(cte1.dia,''),
			'hinicio'		= isnull(cte1.hinicio,'00:00'),
			'hfim'			= isnull(cte1.hfim,'00:00'),
			'inactivo'		= 0,
			'dupli'			= 0,
			'capacidade'	= 1,
			'estado'		= isnull(cte1.estado,convert(varchar(20),'')),
			'speriodo'		= isnull(cte1.speriodo,0),
			isnull(idstamp,'')			as idstamp,
			isnull(apelido,'')			as apelido,
			isnull(ncont,'')			as ncont,
			isnull(nbenef,'')			as nbenef,
			isnull(nutente,'')			as nutente,
			isnull(local,'')			as local,
			isnull(dtnasc,'19000101')	as dtnasc,
			isnull(morada,'')			as morada,
			isnull(telefone,'')			as telefone,
			isnull(dtopen,'19000101')	as dtopen,
			isnull(cte1.obs,'')			as obs,
			isnull(clno,0)				as clno,
			isnull(clestab,0)			as clestab,
			isnull(profissao,'')		as profissao,
			isnull(codpost,'')			as codpost,
			isnull(bino,'')				as bino,
			isnull(bidata,'19000101')	as bidata,
			isnull(bilocal,'')			as bilocal,
			isnull(fax,'')				as fax,
			isnull(telesc,'')			as telesc,
			isnull(clnome,'')			as clnome,
			isnull(ennome,'')			as ennome,
			isnull(enno,0)				as enno,
			isnull(agestab,0)			as agestab,
			isnull(tratamento,'')		as tratamento,
			isnull(mruma,0)				as mruma,
			isnull(ninscs,'')			as ninscs,
			isnull(csaude,'')			as csaude,
			isnull(drfamilia,'')		as drfamilia,
			isnull(tlmvl,'')			as tlmvl,
			isnull(zona,'')				as zona,
			isnull(tipo,'')				as tipo,
			isnull(sexo,0)				as sexo,
			isnull(ecivil,'')			as ecivil,
			isnull(email,'')			as email,
			isnull(nacion,'')			as nacion,
			isnull(mae,'')				as mae,
			isnull(pai,'')				as pai,
			isnull(imagem,'')			as imagem,
			isnull(ecl,0)				as ecl,
			isnull(idnome,'')			as idnome,
			isnull(pacgen,0)			as pacgen,
			isnull(siglaadse,'')		as siglaadse,
			isnull(u_despla2,'')		as u_despla2,
			isnull(u_entpla,'')			as u_entpla,
			isnull(u_entpla2,'')		as u_entpla2,
			isnull(u_nbenef,'')			as u_nbenef,
			isnull(u_codpla2,'')		as u_codpla2,
			isnull(u_descp,'')			as u_descp,
			isnull(u_cerp,0)			as u_cerp,
			isnull(u_cesd,0)			as u_cesd,
			isnull(u_cesdcart,'')		as u_cesdcart,
			isnull(u_cesdcp,'')			as u_cesdcp,
			isnull(u_cesdidi,'')		as u_cesdidi,
			isnull(u_cesdidp,'')		as u_cesdidp,
			isnull(u_cesdpd,'')			as u_cesdpd,
			isnull(u_cesdval,'19000101') as u_cesdval, 
			isnull(u_codigop,'')		as u_codigop,
			isnull(u_codpla,'')			as u_codpla,
			isnull(u_despla,'')			as u_despla,
			CONVERT(text,'')			as obs,
			'ref'						= isnull(cte1.ref,''),
			'design'					= isnull(cte1.design,'')
	from	cte1
	/*FULL OUTER JOIN cte2 on cte1.drdvstamp = cte2.drdvstamp*/
	Left join id (nolock) on id.idno = cte1.idno
	ORDER BY 
		hinicio, hfim		

END

GO
Grant Execute on dbo.up_cli_marcacoes to Public
Grant Control on dbo.up_cli_marcacoes to Public
GO