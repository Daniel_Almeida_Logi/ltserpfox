/* exec up_marcacoes_ListaServicos */ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_ListaServicos]') IS NOT NULL
    drop procedure up_marcacoes_ListaServicos
go

create PROCEDURE up_marcacoes_ListaServicos

/* WITH ENCRYPTION */
AS

	select 
		 st.ref
		 ,st.design
		 ,duracao = u_duracao
	from 
		st (nolock)
	Where
		u_servmarc = 1
	Order by 
		design
	
GO
Grant Execute On up_marcacoes_ListaServicos to Public
Grant Control On up_marcacoes_ListaServicos to Public
go