/*
  exec up_marcacoes_Atividades '20130725'
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_Atividades]') IS NOT NULL
	drop procedure up_marcacoes_Atividades
go

create PROCEDURE up_marcacoes_Atividades
@data as datetime

/* WITH ENCRYPTION */

AS

	SET NOCOUNT ON

	select 
		mrstamp		 = '',
		mr_drdvstamp = '',
		drdvstamp	 = '',
		mrno		 = 0,
		nome		 = '',
		idno		 = 0,
		data		 = '20130725',
		drnome		 = isnull(b_us.nome,''),
		drno		 = isnull(b_us.userno,0),
		dvnome		 = '',
		dvno		 = 0,
		dia			 = '',
		hinicio		 = isnull(b_actividades.horaInicio,'00:00'),
		hfim		 = isnull(b_actividades.horaFim,'23:59'),
		inactivo	 = 0,
		dupli		 = 0,
		capacidade	 = 1,
		estado		 = 'ATIVIDADE',
		speriodo	 = 1,
		utstamp		= '',
		nome		= '',
		ncont		= '',
		nutente		= '',
		nbenef		= '',
		local		= '',
		dtnasc		= '19000101',
		morada		= '',
		telefone	= '',
		dtopen		= '19000101',
		obs			= ''
		,selecionada = CONVERT(bit,0)
		,ref = ''
		,design = isnull(b_actividades.nome,'')
	from 
		b_actividades
		left join b_actividadesUs on b_actividades.actstamp = b_actividadesUs.actstamp
		left join b_us on b_us.usstamp = b_actividadesUs.usstamp
	Where
		@data between b_actividades.dataInicio and b_actividades.dataFim
	
	ORDER BY 
		hinicio, hfim		
	
GO
Grant Execute On up_marcacoes_Atividades to Public
Grant Control On up_marcacoes_Atividades to Public
go