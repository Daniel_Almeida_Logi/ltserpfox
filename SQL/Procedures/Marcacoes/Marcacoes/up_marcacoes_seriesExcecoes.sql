-- exec up_marcacoes_seriesExcecoes ''
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_seriesExcecoes]') IS NOT NULL
	drop procedure up_marcacoes_seriesExcecoes
go

create PROCEDURE up_marcacoes_seriesExcecoes
@stamp as varchar(25)

/* WITH ENCRYPTION */

AS

	Select 
		serieexcstamp
		,seriestamp
		,dataInicio = CONVERT(date, dataInicio)
		,dataFim = CONVERT(date, dataFim)
		,dia
		,hinicio
		,hfim
		,inactivo
		,ousrinis
		,ousrdata
		,ousrhora
		,usrinis
		,usrdata
		,usrhora
		,marcada
	from 
		b_seriesExcecoes 
	where 
		seriestamp = @stamp
		
GO
Grant Execute On up_marcacoes_seriesExcecoes to Public
Grant Control On up_marcacoes_seriesExcecoes to Public
go