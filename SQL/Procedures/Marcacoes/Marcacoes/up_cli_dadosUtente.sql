-- exec up_cli_dadosUtente ''
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cli_dadosUtente]') IS NOT NULL
	drop procedure dbo.up_cli_dadosUtente
go

create procedure dbo.up_cli_dadosUtente

@nome	as varchar(254)

/* WITH ENCRYPTION */

AS 
BEGIN

	SET LANGUAGE PORTUGUESE
	Select	
		idstamp,nome,idno,apelido
	from	
		id (nolock)
	Where	
		nome = @nome
		and nome != ''

END

GO
Grant Execute on dbo.up_cli_dadosUtente to Public
Grant Control on dbo.up_cli_dadosUtente to Public
GO