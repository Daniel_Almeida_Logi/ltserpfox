-- Recursos
-- exec up_Marcacoes_RecursoServMarc 'ADM5736C59B-48BD-4AAC-96B'

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_Marcacoes_RecursoServMarc]') IS NOT NULL
	drop procedure up_Marcacoes_RecursoServMarc
go

create PROCEDURE up_Marcacoes_RecursoServMarc
@mrstamp as char(25)


/* WITH ENCRYPTION */

AS
SET NOCOUNT ON

		
	select 
		marcacoes.mrstamp
		,marcacoes.mrno
		,marcacoesRecurso.*
		,design = isnull((select top 1 design from st where st.ref = marcacoesRecurso.ref),'')
		,especialidade = isnull((Select top 1 especialidade from B_usesp where userno = b_us.userno),'')
	from 
		marcacoes (nolock)
		inner join marcacoesRecurso (nolock) on marcacoesRecurso.mrstamp = marcacoes.mrstamp
		left join B_us on b_us.userno = marcacoesRecurso.no
	where
		marcacoes.mrstamp = @mrstamp
	

GO
Grant Execute On up_Marcacoes_RecursoServMarc to Public
grant control on up_Marcacoes_RecursoServMarc to Public
go	