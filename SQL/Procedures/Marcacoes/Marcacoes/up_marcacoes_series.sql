/*
  exec up_marcacoes_series  ''
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_marcacoes_series]') IS NOT NULL
	drop procedure up_marcacoes_series
go

create PROCEDURE up_marcacoes_series
@stamp as varchar(25)

/* WITH ENCRYPTION */

AS

	Select 
		b_series.*
		,utstamp = isnull(B_utentes.utstamp,'')
	from 
		b_series 
		left join B_utentes on B_utentes.no = b_series.no and B_utentes.estab = b_series.estab
	where 
		seriestamp = @stamp
	
		
GO
Grant Execute On up_marcacoes_series to Public
Grant Control On up_marcacoes_series to Public
go
