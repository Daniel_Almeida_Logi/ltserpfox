/*
	
	FUNCAO:	adiciona/remove comunicações em falta de productos indisponiveis
	SP:		up_painel_excecoes_rel
	Data:	2020-01-22
	Autor:  Daniel Almeida

	exec up_lts_missingProducts_add_remove  0, '1234567891234567891','admx0000001', "ADM", '5440987',1


	Param:

	@add bit - flag que controla se adiciona o remove
	@prescriptionId varchar(100) - numero da receita
	@nrAttend varchar(100) - Numero do atendimento
	@op varchar(100) - Operador que marcou o producto como indisponivel
	@ref varchar(100) - Operador que marcou o producto como indisponivel
	@siteNr int - numero da Loja

	select  * from missingProductsInfo

	select *from fi2

*/




SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_lts_missingProducts_add_remove]') IS NOT NULL
	drop procedure dbo.up_lts_missingProducts_add_remove
go

CREATE procedure dbo.up_lts_missingProducts_add_remove
	@add bit,
	@prescriptionId varchar(100),
	@nrAttend varchar(100),
	@op varchar(100),
	@ref varchar(100),
	@siteNr int

/* with encryption */
AS
	SET NOCOUNT ON

	declare @stamp varchar(100) = ''
	declare @today date = GETDATE();
	declare @yesterday date = DATEADD(DAY, -1, @today)



	select 
	top 1 
		@stamp = isnull(ltrim(rtrim(stamp)),'') 
	from  
		missingProductsInfo mpi(nolock) 
	where 
		mpi.prescriptionId=@prescriptionId and
		mpi.ref = @ref and
		mpi. nrAttend = @nrAttend and
		mpi.date >= @today and 
		mpi.date <=  DATEADD(DAY, 1, @today) and
		export = 0
				  
	SET @stamp = rtrim(ltrim(@stamp))

	if(@add=1)
	begin

		if(@stamp='')
			Insert into missingProductsInfo 
			(stamp,token,prescriptionId,nrAttend,ousrinis,ref,export,date,siteNr,ousrdata)
			VALUES(newid(),newid(),@prescriptionId,@nrAttend,@op,@ref,0,GETDATE(),@siteNr,GETDATE())	
	end

	if(@add=0)
	begin
		if(@stamp!='')
			delete from missingProductsInfo where stamp=@stamp		
	end



	
	

Go
Grant Execute on dbo.up_lts_missingProducts_add_remove to Public
Grant Control on dbo.up_lts_missingProducts_add_remove to Public
Go


