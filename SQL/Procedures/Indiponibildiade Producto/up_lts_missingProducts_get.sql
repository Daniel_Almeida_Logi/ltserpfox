/*

	exec up_lts_missingProducts_get '20230215', '00:00', '23:59',1,1000

	Retorna as comunica��es em falta de productos indisponiveis para um determinado periodo de tempo. ordenado  por dia

	@date varchar(8) - data a comunicar
	@hourInit varchar(8) - hora inicial data a comunicar 
	@hourEnd varchar(8)  - hora final data a comunicar
	@no int, - se j� foi enviado

	select  *  from missingProductsInfo where date between '20200109 00:00' and  '20200109 23:59' and siteNr = 1



*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_lts_missingProducts_get]') IS NOT NULL
	drop procedure dbo.up_lts_missingProducts_get
go

CREATE procedure dbo.up_lts_missingProducts_get
	@date varchar(8),
	@hourInit varchar(8),
	@hourEnd varchar(8),
	@siteNr varchar(20),
	@top int = 1

/* with encryption */
AS
	SET NOCOUNT ON
	select 
		top (@top)
		mpi.stamp, 
		mpi.token, 
		mpi.ref,
		mpi.prescriptionId,
	--	infarmed = RIGHT('00000'+ISNULL(CONVERT(varchar,emp.infarmed),'0'),5),	
		emp.infarmed,
		emp.nomabrv,
		mpi.date,
		emp.ncont
	from 
		missingProductsInfo mpi (nolock)
	inner join empresa(nolock) emp on emp.no = mpi.siteNr
	where 
		mpi.date between @date + " " + @hourInit and  @date  + " " + @hourEnd 
		and mpi.export = 0  
        and mpi.ousrdata > GETDATE() - 30
		and mpi.siteNr = @siteNr
		and emp.no = @siteNr
		and emp.tipoempresa = 'FARMACIA'
		and len(mpi.ref) = 7
	order by date asc






Go
Grant Execute on dbo.up_lts_missingProducts_get to Public
Grant Control on dbo.up_lts_missingProducts_get to Public
Go


