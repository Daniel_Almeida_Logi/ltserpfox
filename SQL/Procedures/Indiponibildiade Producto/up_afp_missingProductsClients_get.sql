/*

	exec up_afp_missingProductsClients_get '20200129', '00:00', '23:59', 0, 1

	Retorna todos os clientes que comunicaram naquele intervalo de tempo.

	@date varchar(8) - data a comunicar
	@hourInit varchar(8) - hora inicial data a comunicar 
	@hourEnd varchar(8)  - hora final data a comunicar
	@sent bit, - se j� foi enviado
	@valid bit - se � valido

	

	select *  from ext_esb_missingProducts where dateInit between '20190101 00:00' and  '20190101 23:59' and valid=1 and sent = 0 

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_afp_missingProductsClients_get]') IS NOT NULL
	drop procedure dbo.up_afp_missingProductsClients_get
go

CREATE procedure dbo.up_afp_missingProductsClients_get
	@date varchar(8),
	@hourInit varchar(8),
	@hourEnd varchar(8),
	@sent bit,
	@valid bit


/* with encryption */
AS
	SET NOCOUNT ON

	If OBJECT_ID('tempdb.dbo.#dadosCodFarm') IS NOT NULL
		DROP TABLE #dadosCodFarm
	
	select 
		distinct assCod = senderId
	into 
		#dadosCodFarm
	from 
		ext_esb_missingProducts eemp (nolock)
	where 
		dateInit between @date + " " + @hourInit and  @date  + " " + @hourEnd  
		and sent = @sent 
		and valid = @valid
		and ousrdata > GETDATE() - 180
	order by senderId

	select 
		--assCod = RIGHT('00000'+ISNULL(CONVERT(varchar,assCod),'0'),5)	 
		assCod
	from
	   #dadosCodFarm

	If OBJECT_ID('tempdb.dbo.#dadosCodFarm') IS NOT NULL
		DROP TABLE #dadosCodFarm



Go
Grant Execute on dbo.up_afp_missingProductsClients_get to Public
Grant Control on dbo.up_afp_missingProductsClients_get to Public
Go


