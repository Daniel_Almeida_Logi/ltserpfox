/* 
	exec up_verifica_indisponibilidade '5017827'
	exec up_verifica_indisponibilidade '5440987'
*/ 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_verifica_indisponibilidade]') IS NOT NULL
    drop procedure up_verifica_indisponibilidade
go

create PROCEDURE up_verifica_indisponibilidade
@ref varchar(18)

/* WITH ENCRYPTION */
AS
	Declare @estadoProduct BIT = 0
	Declare @message	   VARCHAR(254) = 'N�o � possivel marcar como indispon�veis produtos n�o comercializados'

	SELECT 
		top 1
		@estadoProduct = u_nomerc  
	FROM 
		fprod(NOLOCK)
	WHERE� 
		ref = @ref


	IF(@estadoProduct=1)
	BEGIN
		SET @message = ''
	END

	SELECT 
		@estadoProduct AS estado, 
		@message	   AS msg
GO
Grant Execute On up_verifica_indisponibilidade to Public
Grant Control On up_verifica_indisponibilidade to Public
go