/*

	 exec up_afp_missingProducts_get '20200129', '00:00', '23:59','0','1','1481'

	Retorna as comunica��es em falta de productos indisponiveis para um determinado dia. ordenado  por cliente(senderId)

	@date varchar(8) - data a comunicar
	@hourInit varchar(8) - hora inicial data a comunicar 
	@hourEnd varchar(8)  - hora final data a comunicar
	@sent bit, - se j� foi enviado
	@valid bit - se � valido
	@infarmedCod  - codigo infarmed da farmacia


	select *  from ext_esb_missingProducts where dateInit between '20190101 00:00' and  '20190101 23:59' and valid=1 and sent = 0 

	update ext_esb_missingProducts set senderId='1481'

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_afp_missingProducts_get]') IS NOT NULL
	drop procedure dbo.up_afp_missingProducts_get
go

CREATE procedure dbo.up_afp_missingProducts_get
	@date varchar(8),
	@hourInit varchar(8),
	@hourEnd varchar(8),
	@sent bit,
	@valid bit,
	@infarmedCod varchar(20)

/* with encryption */
AS
	SET NOCOUNT ON
	
	select 
	--	assCod =RIGHT('00000'+ISNULL(CONVERT(varchar,senderId),'0'),5),	  
		assCod = senderId,
		dateInit = convert(datetime,dateInit),
		productNhrn,
		prescriptionId,
		stamp
	from 
		ext_esb_missingProducts eemp (nolock)
	where 
		dateInit between @date + " " + @hourInit and  @date  + " " + @hourEnd  
		and sent = @sent 
		and valid = @valid
		and ousrdata > GETDATE() - 180
		and len(productNhrn) = 7
		and senderId = @infarmedCod
	order by senderId



Go
Grant Execute on dbo.up_afp_missingProducts_get to Public
Grant Control on dbo.up_afp_missingProducts_get to Public
Go


