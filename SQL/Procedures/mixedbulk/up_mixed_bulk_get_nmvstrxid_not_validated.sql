
GO
/****** Object:  StoredProcedure [dbo].[up_mixed_bulk_get_nmvstrxid_not_validated]    Script Date: 14/06/2023 15:51:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
	exec up_mixed_bulk_get_nmvstrxid_not_validated '2021-01-06', 'Loja 1',60
	exec up_mixed_bulk_get_nmvstrxid_not_validated '2024-01-25', 'Loja 3', 0
*/
-- =============================================
-- Author:		José Moreira
-- Create date: 2022-11-07
-- Description: Returns all the products needing checking status
-- that have been sent begining at date "@data_pesquisa" 
-- =============================================
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_mixed_bulk_get_nmvstrxid_not_validated]') IS NOT NULL
	drop procedure dbo.up_mixed_bulk_get_nmvstrxid_not_validated
go

CREATE PROCEDURE [dbo].[up_mixed_bulk_get_nmvstrxid_not_validated]
	@data_pesquisa	varchar(30),
	@site			varchar(10),
	@waitTime		NUMERIC(5,0)
AS
BEGIN
	print @data_pesquisa

	select distinct 
		nmvstrxid
	from 
		mixed_bulk_pend (nolock) 
	where 
		--ousrdata >=  @data_pesquisa
		 site = @site
		AND processado = 0
		AND send=1
		AND len(ltrim(rtrim(nmvsTrxId))) > 0
		AND DATEADD(MINUTE, @waitTime, sendDate) < GETDATE()
END

GO
Grant Execute On dbo.up_mixed_bulk_get_nmvstrxid_not_validated to Public
Grant Control On dbo.up_mixed_bulk_get_nmvstrxid_not_validated to Public
GO
