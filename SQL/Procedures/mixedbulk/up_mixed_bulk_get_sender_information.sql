
GO
/****** Object:  StoredProcedure [dbo].[up_mixed_bulk_get_sender_information]    Script Date: 14/06/2023 15:51:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
	exec up_mixed_bulk_get_sender_information 'Loja 1'
*/
-- =============================================
-- Author:		José Moreira
-- Create date: 2022-11-07
-- Description: Gets the sender information for mixed_bulk
-- =============================================
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_mixed_bulk_get_sender_information]') IS NOT NULL
	drop procedure dbo.up_mixed_bulk_get_sender_information
go

CREATE PROCEDURE [dbo].[up_mixed_bulk_get_sender_information] 
	@site varchar(20)
AS
BEGIN
	declare	@userName varchar(30)
	declare @password varchar(30)
	
	select 
		@userName = textValue 
	from 
		B_Parameters_site (nolock)
	where 
		stamp = 'ADM0000000018'
		and site = @site

	select 
		@password = textValue 
	from 
		B_Parameters_site (nolock)
	where 
		stamp = 'ADM0000000019'
		and site = @site
 
	select 
		@userName as username,
		@password as password
END

GO
Grant Execute On dbo.up_mixed_bulk_get_sender_information to Public
Grant Control On dbo.up_mixed_bulk_get_sender_information to Public
GO
