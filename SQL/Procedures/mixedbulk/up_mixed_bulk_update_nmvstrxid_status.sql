
GO
/****** Object:  StoredProcedure [dbo].[up_mixed_bulk_update_nmvstrxid_status]    Script Date: 14/06/2023 15:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
	exec up_mixed_bulk_update_nmvstrxid_status 
*/
-- =============================================
-- Author:		José Moreira
-- Create date: 2022-11-07
-- Description: updates de data that have been validated
--exec up_mixed_bulk_update_nmvstrxid_status '68a91df1-0d1e-470f-a-1', '05600937100221', 'BATCH000000000000004', 'G120', 'SUPPLIED;', 'NMVS_NC_PCK_19', 'M1N2O3P4Q5R600083914', 'O identificador único já se encontra no estado Inativo. Realize uma operação de verificação e confirme se é possível reverter o estado do identificador único para o estado Ativo.', '2023-06-22T13:39:21.059+02:00', 'b9e8c3a427a94eb1a93e91e3b64d8', 'GTIN', 'M1N2O3P4Q5R600083914'
-- =============================================
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_mixed_bulk_update_nmvstrxid_status]') IS NOT NULL
	drop procedure dbo.up_mixed_bulk_update_nmvstrxid_status
go

CREATE PROCEDURE [dbo].[up_mixed_bulk_update_nmvstrxid_status]  
	  @clientTrxId as varchar(30)
	, @productCode as varchar(100)
	, @batchId as varchar(100)
	, @reqType as varchar(10)
	, @reasons as varchar(max)
	, @code as varchar(300)
	, @state as varchar(100)
	, @decription as varchar(max)
	, @timestamp	varchar(30)
	, @nmvsTrxId as varchar(100)
	, @productCodeScheme as varchar(10) 
	, @sn as varchar(25)
	, @nmvsTrxIdConcentrator as varchar(100)
AS
BEGIN

	DECLARE @date datetime 
	DECLARE @stamp VARCHAR(36)

	SELECT top 1 @stamp = mbstamp,  -- top 1 não pode existir mais do que um 
			@date=ousrdata 
	FROM  mixed_bulk_pend 
	WHERE
		clientTrxId = @clientTrxId
		and productCode = @productCode 
		and nmvsTrxId=@nmvsTrxId
		and reqType=@reqType
		and productCodeScheme = @productCodeScheme
		and packSerialNumber =@sn
		and clientTrxId != ''
		and processado = 0

		print @stamp

	update 
		mixed_bulk_pend
	set 
		reqType = @reqType 
		, reasons = @reasons
		, code = @code
		, state = @state
		, description = @decription
		, processado = case when @code='NMVS_FE_TX_08' then 0 else 1 end -- colocar o enviado 
		, nmvsTimeStamp = @timestamp
		, send = case when @code='NMVS_FE_TX_08' then 0 else 1 end -- colocar o enviado 
	where
		mbstamp=@stamp


		if(@reqType='G120' and @code='NMVS_SUCCESS') --colocar o nmvstrxid apenas de sucesso
		BEGIN
			update mixed_bulk_pend SET nmvstrxid = @nmvsTrxIdConcentrator
			WHERE productCode = @productCode 
			and reqType='G121'
			and nmvsTrxId=''
			and productCodeScheme = @productCodeScheme
			and packSerialNumber =@sn
			and processado = 0 and send=0 -- para aqueles que não foram enviados apenas 
			and ousrdata > @date -- colocar apenas naqueles que tem a data superior a data do g120
		END 
		ELSE if(@reqType='G150' and @code='NMVS_SUCCESS')  --colocar o nmvstrxid apenas de sucesso
		BEGIN
			update mixed_bulk_pend SET nmvstrxid = @nmvsTrxIdConcentrator
			WHERE productCode = @productCode 
			and productCodeScheme = @productCodeScheme
			and packSerialNumber =@sn
			and reqType='G151'
			and nmvsTrxId=''
			and processado = 0 and send=0 -- para aqueles que não foram enviados apenas 
			and ousrdata > @date -- colocar apenas naqueles que tem a data superior a data do g150
		END 

END


GO
Grant Execute On dbo.up_mixed_bulk_update_nmvstrxid_status to Public
Grant Control On dbo.up_mixed_bulk_update_nmvstrxid_status to Public
GO
