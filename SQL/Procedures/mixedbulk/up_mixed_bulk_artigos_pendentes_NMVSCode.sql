
GO
/****** Object:  StoredProcedure [dbo].[up_mixed_bulk_artigos_pendentes]    Script Date: 14/06/2023 15:51:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
	exec up_mixed_bulk_artigos_pendentes_NMVSCode '19000101', 'Loja 1','NMVS_FE_TX_08'
	exec up_mixed_bulk_artigos_pendentes_NMVSCode '2023-01-19', 'Loja 1','NMVS_FE_TX_08'
*/
-- =============================================
-- Author:		Jos� Sim�es
-- Create date: 2023-06-21
-- Description: Returns all the products that haven't been processed with error begining at date "@data_pesquisa"
-- =============================================

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_mixed_bulk_artigos_pendentes_NMVSCode]') IS NOT NULL
	drop procedure dbo.up_mixed_bulk_artigos_pendentes_NMVSCode
go
CREATE PROCEDURE [dbo].[up_mixed_bulk_artigos_pendentes_NMVSCode]
	@data_pesquisa	varchar(30),
	@site			varchar(10),
	@NMVSCode		varchar(50)
AS
BEGIN
	

	IF( (SELECT assfarm FROM empresa WHERE site=@site) ='ANF'  )
	BEGIN
		select
			mbstamp
			, token
			, recstamp
			, clientTrxId
			, productCode
			, productCodeScheme
			, batchId
			, right(convert(varchar,batchExpiryDate,112),6) as batchExpiryDate
			, packSerialNumber 
			, nmvstrxid
			, posTerminal
			, tipo 
			, reqType
					, case when (country_productNhrn !=0 and productNhrn!='' ) then '('+convert(varchar(3),country_productNhrn)+')' + productNhrn else productNhrn end  productNhrn
			,ousrdata 
		from
			mixed_bulk_pend (nolock)
		where 
			SEND	= 0  --AND ( reqType !='G121' or (reqType ='G121' and  nmvstrxid!=''))
			and ousrdata > @data_pesquisa
			and site = @site
			and code = case when @NMVSCode='' then code else @NMVSCode end 
			order by ousrdata asc
	END
	ELSE 
	BEGIN 
			select
			mbstamp
			, token
			, recstamp
			, clientTrxId
			, productCode
			, productCodeScheme
			, batchId
			, right(convert(varchar,batchExpiryDate,112),6) as batchExpiryDate
			, packSerialNumber 
			, nmvstrxid
			, posTerminal
			, tipo 
			, reqType
					, case when (country_productNhrn !=0 and productNhrn!='' ) then '('+convert(varchar(3),country_productNhrn)+')' + productNhrn else productNhrn end  productNhrn
			,ousrdata 
		from
			mixed_bulk_pend (nolock)
		where 
			SEND	= 0 
			and ousrdata > @data_pesquisa
			and site = @site
			and code = case when @NMVSCode='' then code else @NMVSCode end 
			order by ousrdata asc
	END 

END

GO
Grant Execute On dbo.up_mixed_bulk_artigos_pendentes_NMVSCode to Public
Grant Control On dbo.up_mixed_bulk_artigos_pendentes_NMVSCode to Public
GO
