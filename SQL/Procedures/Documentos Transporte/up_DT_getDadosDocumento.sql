/* 
	exec up_DT_getDadosDocumento 'ADM2499D998-E996-4893-91D'

select utilizadorDT, passwordDT from empresa (nolock) where site='Loja 1'
exec up_DT_getDadosDocumento 'ADM2B0B6E10-4C93-4F7E-BCD'
exec up_DT_getDadosDocumento 'ADMAF04E25B-6971-4400-AF1'
 
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_DT_getDadosDocumento]') IS NOT NULL
	drop procedure dbo.up_DT_getDadosDocumento
go



create procedure dbo.up_DT_getDadosDocumento
@stamp varchar(30)

/* WITH ENCRYPTION */ 
AS 
BEGIN 
	/* Criar variavel com nome do cliente 200*/
	declare @nomeClienteDuzentos varchar(30)
	set @nomeClienteDuzentos = 'CONSUMIDOR FINAL'

	if(select COUNT(ftstamp) from ft where ftstamp = @stamp) > 0
	begin /*guia de transporte - FT + FI*/
		; with cteEmpresa as(
			select
				taxRegistrationNumber	= left(empresa.ncont,9)
				,CompanyName			= left(ltrim(rtrim(empresa.nomecomp)),100)
				,AddressDetail			= left(ltrim(rtrim(empresa.morada)),100)
				,City					= case when empresa.local = '' then 'Desconhecido' else left(ltrim(rtrim(empresa.local)),50) end						
				,PostalCode				= case when empresa.codpost='' then '0000-000' else LEFT(codpost,8) end
				,Country				= left('PT',2)
				,empresa.site
				,siteExt                = rtrim(ltrim(isnull(empresa.siteExt,'')))                 
			from 
				empresa(nolock)
		)
		select 
			cteEmpresa.*
			--,DocumentNumber		= left(isnull(b.invoiceType,'GT') + right(convert(varchar,ft.ftano),2) + '' + convert(varchar,ft.ndoc) + '/' + CONVERT(varchar,ft.fno),60)
			,DocumentNumber		= isnull(b.invoiceType,'GT') + ' ' + LTRIM(ft.ndoc) + '/' + LTRIM(ft.fno)
			,MovementStatus		= case when ft.anulado = 0 then 'N' else 'A' end
			,MovementDate		= convert(date,ft.fdata)
			,MovementType		= left(isnull(b.invoiceType,'GT'),2)
			,CustomerTaxID		= case when ft.ncont = '' then '999999990' else ft.ncont end
			,MovementStartTime  = dateadd(mi,30,convert(datetime,convert(varchar,convert(date,ft.ousrdata))+' '+CONVERT(varchar,case when ft.ousrhora='' then CONVERT(VARCHAR(8),GETDATE(),108) else ft.ousrhora end)))
			,AddressDetailTo    = rtrim(ltrim(ISNULL(ft2.morada,'')))
			,CountryTo			= left('PT',2)
			,LocalTo			= rtrim(ltrim(ISNULL(ft2.xpdLocalidade,'')))
			,PostalTo			= rtrim(ltrim(ISNULL(ft2.xpdCodPost,'')))
			,Atcud              = case when rtrim(ltrim(isnull(td.ATCUD,'0'))) ='' then '0' else rtrim(ltrim(isnull(td.ATCUD,'0')))  end + '-' + CONVERT(varchar,ft.fno)
			,ATDocCodeID         = rtrim(ltrim(isnull(td.ATCUD,'0'))) 
		from 
			ft (nolock)
			left join ft2(nolock) on ft2.ft2stamp = ft.ftstamp
			left join td(nolock) on td.ndoc = ft.ndoc and td.site = ft.site
			inner join cteEmpresa on cteEmpresa.site = ft.site
			left join B_cert (nolock) as b on ft.ftstamp=b.stamp
		where 
			ft.ftstamp = @stamp
	end
	else
	begin /*dev. a fornecedor - BO + BI*/
		; with cteEmpresa as(
			select 
				taxRegistrationNumber	= left(empresa.ncont,9)
				,CompanyName			= left(ltrim(rtrim(empresa.nomecomp)),100)
				,AddressDetail			= left(ltrim(rtrim(empresa.morada)),100)
				,City					= case when empresa.local = '' then 'Desconhecido' else left(ltrim(rtrim(empresa.local)),50) end						
				,PostalCode				= case when empresa.codpost='' then '0000-000' else LEFT(codpost,8) end
				,Country				= left('PT',2)
				,empresa.site
				,siteExt                = rtrim(ltrim(isnull(empresa.siteExt,'')))
		
			from 
				empresa(nolock)
		)
		select 
			cteEmpresa.*
			--,DocumentNumber     = left(isnull(ts.tiposaft,case when bo.ndos = 7 then 'GA' else 'GD' end) + right(convert(varchar,bo.boano),2) + ' ' + convert(varchar,bo.ndos) + cteEmpresa.siteExt + '/' + CONVERT(varchar,bo.obrano),60)
			,DocumentNumber		= isnull(ts.tiposaft,'') + ' ' + LTRIM(bo.ndos) + '/' + LTRIM(bo.obrano)
			,MovementStatus		= 'N' --case when ft.anulado = 0 then 'N' else 'A' end
			,MovementDate		= convert(date,bo.dataobra)
			,MovementType		= left(isnull(ts.tiposaft,''),2)
			,CustomerTaxID		= case when bo.ncont = '' then '999999990' else bo.ncont end
			,MovementStartTime	= 
				--/* Caso a data n�o esteja preenchida no documento */
				--case 
				--	when xpddata = '19000101' then 
				--		dateadd(mi,30,convert(datetime,convert(varchar,convert(date,bo.ousrdata))+' '+CONVERT(varchar,case when bo.ousrhora='' then CONVERT(VARCHAR(8),GETDATE(),108) else bo.ousrhora end)))
				--	when bo2.xpdhora = '00:00' or  bo2.xpdhora = ''
				--	then 
				--		dateadd(mi,30,
				--			convert(datetime,convert(varchar,convert(date,bo2.xpddata))
				--			+' '
				--			+CONVERT(varchar,case when bo.ousrhora='' then CONVERT(VARCHAR(8),GETDATE(),108) else bo.ousrhora end)
				--		))
				--/* tudo preenchido no documento */
				--else 
				--	convert(datetime,convert(varchar,convert(date,bo2.xpddata))
				--		+' '
				--		/* Caso a hora n�o esteja preenchida no documento */
				--		+
				--		CONVERT(varchar,case when bo2.xpdhora='' then CONVERT(VARCHAR(8),GETDATE(),108) else bo2.xpdhora end)
				--	)
				--end	

				/* Caso a data n�o esteja preenchida no documento */
				case 
					when xpddata = '19000101' 
					then 
						dateadd(mi,30,GETDATE())
					when bo2.xpdhora = '00:00' or  bo2.xpdhora = ''
					then 
						dateadd(mi,30,GETDATE())
				/* tudo preenchido no documento */
				else 
					convert(datetime,convert(varchar,convert(date,bo2.xpddata))
						+' '
						/* Caso a hora n�o esteja preenchida no documento */
						+
						CONVERT(varchar,case when bo2.xpdhora='' then CONVERT(VARCHAR(8),GETDATE(),108) else bo2.xpdhora end)
					)
				end
				,AddressDetailTo		= rtrim(ltrim(isnull(bo2.morada,'')))
				,CountryTo				= left('PT',2)
				,LocalTo			    = rtrim(ltrim(ISNULL(bo2.xpdLocalidade,'')))
				,PostalTo			    = rtrim(ltrim(isnull(bo2.codpost,'')))
				,Atcud                  = case when rtrim(ltrim(isnull(ts.ATCUD,'0'))) ='' then '0' else rtrim(ltrim(isnull(ts.ATCUD,'0')))  end + '-' + CONVERT(varchar,bo.obrano)
				,ATDocCodeID            = rtrim(ltrim(isnull(ts.ATCUD,'0'))) 
		from 
			bo (nolock)
			inner join bo2 (nolock) on bo2stamp = bostamp
			left join  ts  (nolock) on ts.ndos = bo.ndos
			inner join cteEmpresa on cteEmpresa.site = bo.site
			left join B_cert (nolock) as b on bo.bostamp=b.stamp
		where 
			bo.bostamp = @stamp
	end
END

GO
Grant Execute on dbo.up_DT_getDadosDocumento to Public
Grant Control on dbo.up_DT_getDadosDocumento to Public
GO
--------------------------------------------------------------



