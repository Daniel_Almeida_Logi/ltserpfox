/*

exec up_DT_getDadosLinhas 'ADM2834A647-E6FE-4EE1-94D'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_DT_getDadosLinhas]') IS NOT NULL
	drop procedure dbo.up_DT_getDadosLinhas
go

create procedure dbo.up_DT_getDadosLinhas
@stamp varchar(30)

/* WITH ENCRYPTION */ 
AS 
BEGIN 
	if(select COUNT(ftstamp) from ft where ftstamp = @stamp) > 0
	begin /*guia de transporte - FT + FI*/
		select 
			ProductDescription	= left(fi.design,200)
			,Quantity			= convert(int,fi.qtt)
			,UnitOfMeasure		= case
										when fi.unidade='' then 'UN'								
										else left(upper(convert(varchar,fi.unidade)),20)
									end
			,UnitPrice			= case
										when (fi.etiliquido / (fi.iva/100+1)) > 0 then 									
											case 
												when fi.ivaincl=1 then abs((fi.etiliquido / (fi.iva/100+1)) / fi.qtt) 
												else abs(fi.etiliquido/fi.qtt)
											end
										else 0
									end
		from 
			fi (nolock)
			inner join ft (nolock) on fi.ftstamp = ft.ftstamp
		where 
			fi.ftstamp = @stamp
			and fi.epromo=0
			and ( (fi.qtt <> 0 or fi.etiliquido <> 0) OR  ( ft.anulado = 1 ) )
		order by fi.lordem
	end
	else
	begin /*dev. a fornecedor - BO + BI*/
		select 
			ProductDescription	= left(bi.design,200)
			,Quantity			= convert(int,bi.qtt)
			,UnitOfMeasure		= case
										when bi.unidade='' then 'UN'								
										else left(upper(convert(varchar,bi.unidade)),20)
									end
			,UnitPrice			= case
										when (bi.ettdeb / (bi.iva/100+1)) > 0 then 									
											case 
												when bi.ivaincl=1 then abs((bi.ettdeb / (bi.iva/100+1)) / bi.qtt) 
												else abs(bi.ettdeb/bi.qtt)
											end
										else 0
									end
		from 
			bi (nolock)
			inner join bo (nolock) on bi.bostamp = bo.bostamp
		where 
			bi.bostamp = @stamp
			and (
					bi.qtt <> 0 
					or bi.ettdeb <> 0
				)
		order by bi.lordem
	end 
END

GO
Grant Execute on dbo.up_DT_getDadosLinhas to Public
Grant Control on dbo.up_DT_getDadosLinhas to Public
GO
