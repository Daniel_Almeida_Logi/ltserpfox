/*
	         exec up_PEMH_histPEMHCab '93c29ed6-d5f5-4e2b-8dfc-d19f83f1c2f8'
	exec up_pemh_histPemhCab  'D66AF523-AD92-4434-BD68-0A75931D3517'
	exec up_pemh_histPemhCab  ''

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_pemh_histpemhCab]') IS NOT NULL
	drop procedure dbo.up_pemh_histpemhCab
go

create procedure dbo.up_pemh_histpemhCab
	@groupToken VARCHAR(50),
	@nrUtente Varchar(15) = ''

AS
BEGIN

	SELECT

		receita_nr
		,CONVERT(VARCHAR, receita_data, 103)	as receita_data
		,ISNULL(numeroEpisodio, '')				as  numeroEpisodio
		,ISNULL(idProcesso, '')					as  idProcesso
		,ISNULL(utente_descr,'' )				as design
		,isnull(utente_contacto,'')				as contacto
		,ISNULL(beneficiario_nr, '')			as nrBeneficiario
		,ISNULL(prescritor_nome, '')			as prescritor_nome
		,ISNULL(local_presc_descr, '')			as local_presc_descr
		,ISNULL(prescritor_contacto, '')		as prescritor_contacto
		,ISNULL(localExt, '')					as localExt
		,ISNULL(contactoExt, '')				as contactoExt
		,token
	FROM
		Dispensa_Eletronica(nolock)
	WHERE
		groupToken = @groupToken
		and resultado_consulta_cod = '10400100001'
		and beneficiario_nr=  case when @nrUtente <> '' 
				then 
					@nrUtente
				else
					beneficiario_nr
			end
	ORDER BY
		Dispensa_eletronica.receita_data DESC

END

GO
Grant Execute on dbo.up_pemh_histpemhCab to Public
Grant Control on dbo.up_pemh_histpemhCab to Public
GO