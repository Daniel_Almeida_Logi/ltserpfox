/* SP que retorna Questionario

	exec up_questionario_refsQuest '91078C81-4CBD-449B-B823-28DF54053A44'
			exec up_questionario_refsQuest '18374898164203'

*/
if OBJECT_ID('[dbo].[up_questionario_refsQuest]') IS NOT NULL
	drop procedure dbo.up_questionario_refsQuest
go

create procedure dbo.up_questionario_refsQuest
	@NrAtend varchar(36)

/* WITH ENCRYPTION */
AS
	
		SELECT distinct fi.ref as ref,  
				st.questionario, 
				Quest.Descr
		from fi (nolock)
		inner join ft (nolock)		on ft.ftstamp = fi.ftstamp
		inner join td (nolock)		on td.ndoc = ft.ndoc
		inner join st (nolock)		on fi.ref = st.ref 
		inner join Quest (nolock)	on Quest.QuestStamp = st.questionario
		where ft.u_nrAtend = @NrAtend
			and td .cmsl in (75,76)
			and st.questionario <> '' 
			and fi.ofistamp = ''


GO
Grant Execute On dbo.up_questionario_refsQuest to Public
Grant Control On dbo.up_questionario_refsQuest to Public
Go 