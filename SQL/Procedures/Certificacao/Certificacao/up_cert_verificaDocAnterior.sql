/*
	Validar	se o documento anterior est� assinado
	
	exec up_cert_verificaDocAnterior 25,77,'20140604'

	exec up_cert_verificaDocAnterior 2,89,'20170630'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



if OBJECT_ID('[dbo].[up_cert_verificaDocAnterior]') IS NOT NULL
	drop procedure dbo.up_cert_verificaDocAnterior
go

create procedure dbo.up_cert_verificaDocAnterior
	@fno as int,
	@ndoc as int,
	@data as date,
	@tiposaft varchar(3) = ''

/* WITH ENCRYPTION */ 

AS
BEGIN

	IF @fno = 1 or ( /* condi��o adicionada devido a novos documentos entrarem para o saft a meio do ano */
					(@tiposaft in ('GR','GT','GA','GC','GD') or @tiposaft in ('PF','NE','DC','OU') or @tiposaft in ('RC','RG'))
					and @data between '20170701' and '20171231'
					)
	BEGIN 
	
		select 1 as count
	
	END ELSE BEGIN

		declare @invoiceno as varchar(9), @resetNumeracao BIT = (SELECT resetNumeracaoAnual from td(nolock) where ndoc = @ndoc)
		select @invoiceno = convert(varchar(9),@ndoc)+'/'+convert(varchar(9),@fno-1)
		print @invoiceno
	
		Select
			count(stamp)  as count
		from
			B_cert (nolock)
		where
			invoiceNo = @invoiceno
			and 1 = (CASE WHEN @resetNumeracao = 0 THEN 1 ELSE (CASE WHEN YEAR(date)=YEAR(@data) THEN 1 ELSE 0 END) END)
	END
			
END

GO
Grant Execute on dbo.up_cert_verificaDocAnterior to Public
Grant control on dbo.up_cert_verificaDocAnterior to Public
Go