/****************************/
/** Verifica Texto a incluir nos IDUS dos documentos mediante o tipo */
/* exec up_cert_TextoRodape 'CP' */
/************************************/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_cert_TextoRodape]') IS NOT NULL
	drop procedure dbo.up_cert_TextoRodape
go

create procedure dbo.up_cert_TextoRodape

@tipo as varchar(2)

/* WITH ENCRYPTION */ 

AS
BEGIN
	Select case when @tipo	in('CL','FL','RE','CL_FATC_ORG') then 'Este documento n�o serve de factura'
			when @tipo		in('AG')				then 'Documento de Uso Interno' 
			when @tipo		in('FT','NC','ND','VD')	then ''
			when @tipo		in('CP')				then 'C�pia do documento original'
			else ''
			end as rodape
			
END

GO
Grant Execute on dbo.up_cert_TextoRodape to Public
Grant control on dbo.up_cert_TextoRodape to Public
Go