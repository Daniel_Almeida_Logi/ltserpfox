if OBJECT_ID('[dbo].[up_GerarHash]') IS NOT NULL
	drop function dbo.up_GerarHash
go

CREATE FUNCTION [dbo].[up_GerarHash](@texto [nvarchar](4000))
RETURNS [nvarchar](200) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [Encriptacao].[SQLCLREncriptacao].[GerarHash]
GO

grant execute on dbo.[up_GerarHash] to public
grant control on dbo.[up_GerarHash] to public