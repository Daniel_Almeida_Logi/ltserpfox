/* Relatório Detalhe relatorio produtos desafio

	exec up_relatorio_produtos_generis_inout '20170201', '20170228', 'F_Melo'
	
*/

if OBJECT_ID('[dbo].[up_relatorio_produtos_generis_inout]') IS NOT NULL
	drop procedure dbo.up_relatorio_produtos_generis_inout
go

create procedure [dbo].[up_relatorio_produtos_generis_inout]
	@dataIni		datetime
	,@dataFim		datetime
	,@site			varchar(55)

/* with encryption */

AS
SET NOCOUNT ON

	/* Elimina Tabelas */

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
		DROP TABLE #dadosSt	
	
	/* Calc Loja e Armazens */
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)

	declare @codigofarm as numeric(10,0)
	set @codigofarm = ISNULL((select infarmed from empresa where site = @site),0)

	/* Calc Informação Produto */
	Select
		st.ref
	INTO
		#dadosSt
	From
		st (nolock)
	Where
		st.generis=1
		AND site_nr = @site_nr

	/* Preparar Result Set */
	Select
		'"'+cast(@codigofarm as varchar(10))+'"' as COD_FARMACIA
		,'"'+replace(replace(replace(CONVERT(varchar, (DATEADD(month, DATEDIFF(month, 0, @dataIni), 0)), 120),'-',''),':',''),' ','')+'"' as COD_CAMPANHA
		,'"'+CONVERT(varchar, @dataIni, 112)+'"' as INI_PERIODO
		,'"'+CONVERT(varchar, @dataFim, 112)+'"' as FIM_PERIODO
		,'"'+cast(hist_vendas.ano as varchar(4))+right('00'+cast(hist_vendas.mes as varchar(2)),2)+right('00'+cast(hist_vendas.dia as varchar(2)),2)+'000000'+'"' as DT_HR_MOV
		--,'"'+cast(hist_vendas.ano as varchar(4))+right('00'+cast(hist_vendas.ano as varchar(2)),2)+'"' as DT_HR_MOV
		,'"'+cast(st.codidt as varchar(5))+'"' as COD_EMP
		,'"'+hist_vendas.ref+'"' as CODIGO
		,'"'+st.design+'"' as DESIGNACAO
		,'"'+cast(cast(hist_vendas.qt as numeric(10,0)) as varchar(10))+'"' as QT_TOT
		,'"'+cast(cast(hist_vendas.qt  as numeric(10,0)) as varchar(10))+'"' as QT_CAMP
		,'"'+replace(cast(cast(hist_vendas.pvp as numeric(10,2)) * cast(hist_vendas.qt as numeric(10,0)) as varchar(10)),'.','')+'"' as VPVP
	From
		hist_vendas 
	inner join 
		st on st.ref=hist_vendas.ref and st.site_nr=@site_nr
	WHERE
		hist_vendas.ano = year(@dataIni)
		and hist_vendas.mes = month(@dataIni)
		and st.generis=1
		and hist_vendas.site=@site

	--select * from hist_vendas where ref='5589247'
	--select * from hist_vendas where site='F_Melo'and ref='8641902'
	--exec up_relatorio_produtos_generis_inout '20170201', '20170228', 'F_UOurique'
	-- select * from st

	/* Elimina Tabelas */

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosSt'))
		DROP TABLE #dadosSt

GO
Grant Execute on dbo.up_relatorio_produtos_generis_inout to Public
Grant control on dbo.up_relatorio_produtos_generis_inout to Public
GO