-- exec up_Scanner_Logs_AFP '2024-02-27 00:00:00'
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_Scanner_Logs_AFP]') IS NOT NULL
    drop procedure up_Scanner_Logs_AFP
go

CREATE PROCEDURE up_Scanner_Logs_AFP
	@date as datetime

/* WITH ENCRYPTION */
AS

SELECT 
	farmacia_posto  + '   ->   ' + totais_erro as scannlog
	FROM(



	SELECT 'totais_erro  ' AS totais_erro,
		   'farmacia_posto  ' AS farmacia_posto	
	UNION ALL
	SELECT DISTINCT  
		CONVERT(varchar(10),count(company.codigo + ' - ' +  G110_Request.posTerminal)) as totais_erro, 
		company.codigo + ' - ' +  G110_Request.posTerminal AS farmacia_posto 
	FROM [msb-concentrador-direct].dbo.G110_Response(NOLOCK)
	INNER JOIN [msb-concentrador-direct].dbo.G110_Request	ON G110_Response.stampRequest = G110_Request.stamp
	INNER JOIN [msb-concentrador-direct].dbo.company  company		ON company.stamp =  G110_Response.senderStamp
	where [msb-concentrador-direct].dbo.G110_Request.ousrdata>=@date and  [msb-concentrador-direct].dbo.G110_Request.entityID = 'lts' and nmvsCode !='NMVS_SUCCESS'
	GROUP BY company.codigo + ' - ' +  G110_Request.posTerminal
	HAVING COUNT(company.codigo + ' - ' +  G110_Request.posTerminal) > 1
	--ORDER BY   company.codigo + ' - ' +  G110_Request.posTerminal
	) AS X 


GO
GRANT EXECUTE ON up_Scanner_Logs_AFP to Public
GRANT CONTROL ON up_Scanner_Logs_AFP to Public
GO

