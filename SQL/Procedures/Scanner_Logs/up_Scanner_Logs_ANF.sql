-- exec up_Scanner_Logs_ANF '2023-06-23 00:00:00'
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_Scanner_Logs_ANF]') IS NOT NULL
    drop procedure up_Scanner_Logs_ANF
go

CREATE PROCEDURE up_Scanner_Logs_ANF
	@date as datetime

/* WITH ENCRYPTION */
AS

SELECT 
	farmacia_posto  + '   ->   ' + totais_erro as scannlog
	FROM(

	SELECT 'totais_erro  ' AS totais_erro,
		   'farmacia_posto  ' AS farmacia_posto	
	UNION ALL
	SELECT DISTINCT  
		convert(varchar(50),count(G110_Request.senderName + ' - ' + G110_Request.posTerminal)) as totais_erro, 
		G110_Request.senderName + ' - ' + G110_Request.posTerminal as farmacia_posto
	FROM [msb-concentrador-other].dbo.G110_Response(nolock)
		INNER JOIN [msb-concentrador-other].dbo.G110_Request ON G110_Response.stampRequest = G110_Request.stamp
	where G110_Request.ousrdata>=@date  and nmvsCode !='NMVS_SUCCESS'
	GROUP BY G110_Request.senderName + ' - ' + G110_Request.posTerminal
	HAVING   count(G110_Request.senderName + ' - ' + G110_Request.posTerminal)  > 1
	--ORDER BY G110_Request.senderName + ' - ' + G110_Request.posTerminal
	) AS X 

GO
GRANT EXECUTE ON up_Scanner_Logs_ANF to Public
GRANT CONTROL ON up_Scanner_Logs_ANF to Public
GO

