/* Cria Crusor BI 

	exec up_precosValidosCor '5440987',2.92

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_precosValidosCor]') IS NOT NULL
	drop procedure dbo.up_precosValidosCor
go

create procedure dbo.up_precosValidosCor
	@ref		VARCHAR(254),
	@preco		numeric(14,4)


/* WITH ENCRYPTION */
AS

	SELECT 
		dbo.uf_defineCorPvp( @ref,@preco) AS cor
GO
Grant Execute On dbo.up_precosValidosCor to Public
Grant Control On dbo.up_precosValidosCor to Public
Go 
