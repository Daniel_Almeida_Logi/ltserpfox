/*
	IMS DETALHES
	Devolve detalhe de vendas mas apenas de compartiicpação
	
	exec up_imsDetalheComparticipacaoVendas '20131002','1234567'
*/
 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_imsDetalheComparticipacaoVendas]') IS NOT NULL
	drop procedure dbo.up_imsDetalheComparticipacaoVendas
go

create procedure dbo.up_imsDetalheComparticipacaoVendas
	@data as datetime,
	@codigo as varchar(254)

/* WITH ENCRYPTION */ 

AS
BEGIN

	Select
		*
	From (
		Select	
			CODIGO_ENTIDADE			=	'"' + Lojas.codigo + '"'
			,NUMERO_FACTURA			=	'"' + convert(varchar,ft.fno) + '"'
			,NUMERO_LINHA_FACTURA	=	(Select COUNT(fistamp) 
											From	fi (nolock) fia Where fia.ftstamp = fi.ftstamp 
													ANd ref != '' 
													and fia.lordem <= fi.lordem 
											)
			,CODIGO_ENTIDADE_COMP	=	'"' + ft2.u_codigo + '"'
			,NOME_ENTIDADE			=	'"' + (select top 1 RTRIM(LTRIM(design)) from cptpla (nolock) Where cptpla.codigo = ft2.u_codigo) + '"'
			,p6						=	''
			,p7						=	''
			,VALOR_COMPART			=	convert(int,fi.u_ettent1*100)
			,p9						=	''
			,Portaria				=	'"' + RTRIM(LTRIM(fi.u_diploma)) + '"'
		From
			empresa	lojas (nolock)
			inner join ft (nolock)	on ft.site = Lojas.site
			inner join ft2 (nolock)	on ft2.ft2stamp = ft.ftstamp
			inner join fi (nolock)	on fi.ftstamp = ft.ftstamp
			inner join td (nolock)	on td.ndoc = ft.ndoc
		Where
			Lojas.codigo = @codigo
			and ofistamp = ''
			and fdata = @data
			and (fi.u_ettent1 != 0)	
			AND (FT.tipodoc!=4 or u_tipodoc=4) 
			AND U_TIPODOC not in (1, 5)
			and ft.tipodoc!=3
			AND ft.anulado = 0 
				
		UNION ALL

		Select
			CODIGO_ENTIDADE			=	'"' + Lojas.codigo + '"'
			,NUMERO_FACTURA			=	'"' + convert(varchar,ft.fno) + '"'
			,NUMERO_LINHA_FACTURA	=	(Select COUNT(fistamp) 
											From	fi (nolock) fia Where fia.ftstamp = fi.ftstamp 
													ANd ref != '' 
													and fia.lordem <= fi.lordem 
											)
			,CODIGO_ENTIDADE_COMP	=	'"' + ft2.u_codigo2 + '"'
			,NOME_ENTIDADE			=	'"' + (select top 1 design from cptpla (nolock) Where cptpla.codigo = ft2.u_codigo2) + '"'
			,p6						=	''
			,p7						=	''
			,VALOR_COMPART			=	convert(int,fi.u_ettent2*100)
			,p9						=	''
			,Portaria				=	'"' + fi.u_diploma + '"'
		From
			empresa	lojas (nolock)
			inner join ft (nolock)	on ft.site = Lojas.site
			inner join ft2 (nolock)	on ft2.ft2stamp = ft.ftstamp
			inner join fi (nolock)	on fi.ftstamp = ft.ftstamp
			inner join td (nolock)	on td.ndoc = ft.ndoc
		Where
			Lojas.codigo = @codigo
			and fdata = @data
			and ft.tipodoc = 1
			and (fi.u_ettent2 != 0)
			AND (FT.tipodoc!=4 or u_tipodoc=4) 
			AND U_TIPODOC not in (1, 5)
			and ft.tipodoc!=3
			AND ft.anulado = 0	
	) a		
	order by
		NUMERO_FACTURA

END
 
Go
Grant Execute on dbo.up_imsDetalheComparticipacaoVendas to Public
Grant control on dbo.up_imsDetalheComparticipacaoVendas to Public
Go