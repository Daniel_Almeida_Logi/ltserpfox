/*
	IMS DETALHE DE VENDAS
	Devolve informação do detalhe de vendas
	exec up_imsDetalheVendas '20150101','1234567'
*/ 

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_imsDetalheVendas]') IS NOT NULL
	drop procedure dbo.up_imsDetalheVendas
go

create procedure dbo.up_imsDetalheVendas

@data as datetime,
@codigo as varchar(254)

/* WITH ENCRYPTION */

AS
BEGIN

	if Object_id('tempdb.dbo.#temp_detalheVendas') is not null
		drop table #temp_detalheVendas

	select
		ofistamp
	into
		#temp_detalheVendas
	from
		fi (nolock)
		inner join ft (nolock) on ft.ftstamp = fi.ftstamp
		inner join td (nolock) on td.ndoc = ft.ndoc
	Where
		ft.tipodoc != 3
		and ft.no > 200
		AND (FT.tipodoc != 4 or u_tipodoc = 4)
		AND td.U_TIPODOC not in (1, 5)
		AND ft.anulado = 0
		AND fi.ofistamp!=''

	Select
		*
	From (
		Select
			CODIGO_ENTIDADE			=	'"' + Lojas.codigo + '"'
			,NUMERO_FACTURA			=	'"' + convert(varchar,ft.fno) + '"'
			,NUMERO_LINHA_FACTURA	=	(Select COUNT(fistamp)
											From	fi fia (nolock) Where fia.ftstamp = fi.ftstamp
													ANd fia.ref != ''
													and fdata = @data
													and fia.lordem <= fi.lordem
											)
			,NUMERO_RECEITA			=	'"' + RTRIM(LTRIM(ft2.u_receita)) + '"'
			,CODIGO_PRODUTO			=	RTRIM(LTRIM(fi.ref))
			,NOME_PRODUTO			=	'"' + RTRIM(LTRIM(fi.design)) + '"'
			,p7						=	''
			,p8						=	''
			,PVP					=	convert(int,fi.u_epvp*100)
			,VALOR_UTENTE			=	convert(int,fi.etiliquido*100)
			,p11					=	''
			,p12					=	''
			,QUANTIDADE				=	convert(int,case when ft.tipodoc=3 then fi.qtt*(-1) else fi.qtt end)
			,p14					=	''
			,Stock_Final			=	convert(int,
											(ISNULL((
												SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
												FROM	sl (nolock)
												WHERE	ref = fi.REF AND sl.ousrdata <= ft.ousrdata
											),0) - fi.qtt)
										)
		From
			empresa	lojas (nolock)
			inner join ft (nolock)	on ft.site = Lojas.site
			inner join ft2 (nolock)	on ft2.ft2stamp = ft.ftstamp
			inner join fi (nolock)	on fi.ftstamp = ft.ftstamp
			inner join td (nolock)	on td.ndoc = ft.ndoc
		Where
			ofistamp = ''
			and ft.no >= 200
			and	Lojas.codigo = @codigo
			and fdata = @data
			AND (FT.tipodoc != 4 or u_tipodoc = 4)
			AND U_TIPODOC not in (1, 5)
			and td.tipodoc != 3
			AND ft.anulado = 0

		UNION ALL

		Select
			CODIGO_ENTIDADE			=	'"' + Lojas.codigo + '"'
			,NUMERO_FACTURA			=	'"' + convert(varchar,ft.fno) + '"'
			,NUMERO_LINHA_FACTURA	=	(Select COUNT(fistamp)
											From	fi fia (nolock) Where fia.ftstamp = fi.ftstamp
													ANd fia.ref != ''
													and fdata = @data
													and fia.lordem <= fi.lordem
											)
			,NUMERO_RECEITA			=	'"' + RTRIM(LTRIM(ft2.u_receita)) + '"'
			,CODIGO_PRODUTO			=	RTRIM(LTRIM(fi.ref))
			,NOME_PRODUTO			=	'"' + RTRIM(LTRIM(fi.design)) + '"'
			,p7						=	''
			,p8						=	''
			,PVP					=	convert(int,fi.u_epvp*100)
			,VALOR_UTENTE			=	convert(int,fi.etiliquido*100)
			,p11					=	''
			,p12					=	''
			,QUANTIDADE				=	convert(int,case when ft.tipodoc=3 then fi.qtt*(-1) else fi.qtt end)
			,p14					=	''
			,Stock_Final			=	convert(int,
											(ISNULL((
												SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
												FROM	sl (nolock)
												WHERE	ref = fi.REF AND sl.ousrdata <= ft.ousrdata
											),0) - fi.qtt)
										)
		From
			empresa	lojas (nolock)
			inner join ft (nolock)	on ft.site = Lojas.site
			inner join ft2 (nolock)	on ft2.ft2stamp = ft.ftstamp
			inner join fi (nolock)	on fi.ftstamp = ft.ftstamp
			inner join td (nolock)	on td.ndoc = ft.ndoc
		Where
			ft.tipodoc = 3
			and fistamp not in (select ofistamp from #temp_detalheVendas)
			and ft.no >= 200
			and	Lojas.codigo = @codigo
			and fdata = @data
			AND (FT.tipodoc != 4 or u_tipodoc = 4)
			AND U_TIPODOC not in (1, 5)
			AND ft.anulado = 0
	) a
	order by
		a.numero_factura, a.numero_linha_factura

	if Object_id('tempdb.dbo.#temp_detalheVendas') is not null
		drop table #temp_detalheVendas
END

GO
Grant Execute on dbo.up_imsDetalheVendas to Public
Grant control on dbo.up_imsDetalheVendas to Public
Go