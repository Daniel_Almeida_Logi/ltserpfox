/* 
	IMS COMPRAS
	Devolve informação de Commpras
		
	exec up_imsCompras '20130701','1234567'
*/ 

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_imsCompras]') IS NOT NULL
	drop procedure dbo.up_imsCompras
go

create procedure dbo.up_imsCompras

@data as datetime,
@codigo as varchar(254)

/* WITH ENCRYPTION */ 

AS
BEGIN

	Select
		CODIGO_ENTIDADE		= '"' + RTRIM(LTRIM(Lojas.codigo)) + '"'
		,CODIGO_PRODUTO		= RTRIM(LTRIM(fn.ref))
		,NOME_PRODUTO		= '"' + RTRIM(LTRIM(fn.design)) + '"'
		,C4					= ''
		,CODIGO_FORNECEDOR	= LEFT(REPLACE(convert(varchar(254),RAND(fo.no)) + convert(varchar(254),(fo.no * 1000 /4)),'.',''),10)
		,NOME_FORNECEDOR	= '"' + RTRIM(LTRIM(fo.nome)) + '"'
		,NIF_FORNECEDOR		= '"' + RTRIM(LTRIM(fo.ncont)) + '"'
		,c8					= ''
		,C9					= ''
		,QUANTIDADE			= convert(int,fn.qtt)
		,VALOR_TOTAL		= convert(int,fn.etiliquido*100)
		,DATA				= '"' + REPLACE(convert(varchar,fo.docdata,102),'.','-') + '"'
	From
		empresa	lojas (nolock)
		inner join fo (nolock)	on fo.site = Lojas.site
		inner join fn (nolock)	on fn.fostamp = fo.fostamp
		inner join cm1 (nolock)	on cm1.cm = fo.doccode
		/*nner join fl (nolock) on fl.no = fo.no and fl.estab = fo.estab*/
		/*inner join st (nolock) on st.ref = fn.ref*/
	Where
		Lojas.codigo	= @codigo
		and docdata		= @data
		and cm1.FOLANSL = 1

	UNION ALL

	Select
		CODIGO_ENTIDADE		= '"' + RTRIM(LTRIM(Lojas.codigo)) + '"'
		,CODIGO_PRODUTO		= RTRIM(LTRIM(bi.ref))
		,NOME_PRODUTO		= '"' + RTRIM(LTRIM(bi.design)) + '"'
		,c4					= ''
		,CODIGO_FORNECEDOR	= LEFT(REPLACE(convert(varchar(254),RAND(bo.no)) + convert(varchar(254),(bo.no * 1000 /4)),'.',''),10)
		,NOME_FORNECEDOR	= '"' + RTRIM(LTRIM(bo.nome)) + '"'
		,NIF_FORNECEDOR		= '"' + RTRIM(LTRIM(bo.ncont)) + '"'
		,c8					= ''
		,c9					= ''
		,QUANTIDADE			= convert(int,bi.qtt)
		,VALOR_TOTAL		= convert(int,bi.ettdeb*100)
		,DATA				= '"' + REPLACE(convert(varchar,bo.dataobra,102),'.','-') + '"'
	From
		empresa lojas (nolock)
		inner join bo (nolock) on bo.site = Lojas.site
		inner join bi (nolock) on bi.bostamp = bo.bostamp
		inner join ts (nolock) on ts.ndos = bo.ndos
		/*inner join fl (nolock) on fl.no = bo.no and fl.estab = bo.estab
		inner join st (nolock) on st.ref = bi.ref*/
	Where
		Lojas.codigo = @codigo
		and bo.dataobra = @data
		and ts.STOCKS =	1
		and ts.bdempresas = 'FL'
				
END

Go
Grant Execute on dbo.up_imsCompras to Public
Grant control on dbo.up_imsCompras to Public
Go