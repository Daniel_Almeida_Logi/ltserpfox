/*
	IMS CABECALHO DE VENDAS
	Devolve informa?o do cabe?lho de vendas
	exec up_imsCabVendas '20121105','1234567'
*/ 

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_imsCabVendas]') IS NOT NULL
	drop procedure dbo.up_imsCabVendas
go

create procedure dbo.up_imsCabVendas
	@data as datetime,
	@codigo as varchar(254)

/* WITH ENCRYPTION */ 

AS
BEGIN

	with cte1 (ofistamp) as (
		select	
			ofistamp
		from
			fi (nolock)
			inner join ft (nolock) on ft.ftstamp = fi.ftstamp
			inner join td (nolock) on td.ndoc = ft.ndoc
		Where
			ft.tipodoc != 3
			and ft.no > 200
			AND (FT.tipodoc != 4 or u_tipodoc = 4)
			AND td.U_TIPODOC not in (1, 5)
			AND ft.anulado = 0
			and fi.ofistamp!=''
	)

	Select
		CODIGO_ENTIDADE			= '"' + a.codigo + '"'
		,NUMERO_FACTURA			= '"' + convert(varchar,a.fno) + '"'
		,DATA_FACTURA			= '"' + REPLACE(convert(varchar,a.fdata,102)+ ' ' + a.ousrhora,'.','-') + '"'
		,NUMERO_MOVIMENTO		= '"' + convert(varchar,YEAR(fdata)) + convert(varchar,a.ndoc) + convert(varchar,a.fno) + '"'
		,NUMERO_RECEITA			= '"' + rtrim(ltrim(ft2.u_receita)) + '"'
		,c6						= '""'
		,c7						= ''
		,c8						= ''
		,c9						= '""'
		,c10					= ''
		,c11					= ''
		,c12					= '""'
		,c13					= '""'
		,c14					= ''
		,c15					= '""'
		,c16					= '""'
		,c17					= ''
		,c18					= ''
	From (
		Select
			Lojas.codigo,
			ft.fno,
			ft.fdata,
			ft.ousrhora,
			ft.ndoc,
			ft.ftstamp
		From
			empresa	lojas (nolock)
			inner join ft (nolock)	on ft.site = Lojas.site
			inner join fi (nolock)	on fi.ftstamp = ft.ftstamp
			inner join td (nolock)	on td.ndoc = ft.ndoc
		Where
			ofistamp = ''
			and ft.no >= 200
			and	Lojas.codigo = @codigo
			and fdata = @data
			AND (FT.tipodoc != 4 or u_tipodoc = 4) 
			AND U_TIPODOC not in (1, 5)
			and td.tipodoc != 3
			AND ft.anulado = 0 
				
		UNION ALL

		Select
			Lojas.codigo,
			ft.fno,
			ft.fdata,
			ft.ousrhora,
			ft.ndoc,
			ft.ftstamp
		From
			empresa	lojas (nolock)
			inner join ft (nolock)	on ft.site = Lojas.site
			inner join fi (nolock)	on fi.ftstamp = ft.ftstamp
			inner join td (nolock)	on td.ndoc = ft.ndoc
		Where
			ft.tipodoc = 3
			and fistamp not in (select ofistamp from cte1)
			and ft.no >= 200
			and	Lojas.codigo = @codigo
			and fdata = @data
			AND (FT.tipodoc != 4 or u_tipodoc = 4)
			AND U_TIPODOC not in (1, 5)
			AND ft.anulado = 0
	)	a
		inner join ft2 (nolock)		on ft2.ft2stamp = a.ftstamp
	group By
		a.codigo, a.fno, a.fdata, a.ousrhora, a.ndoc, ft2.u_receita, ft2.u_entpresc,
		ft2.u_nopresc
	order by
		a.fdata

END

GO
Grant Execute on dbo.up_imsCabVendas to Public
Grant control on dbo.up_imsCabVendas to Public
Go