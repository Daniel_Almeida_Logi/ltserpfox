/* 
	IMS COMPRAS
	Devolve informação de Commpras
		
	exec up_imsComprasSVendas '20221212','0000000'



*/

SET ANSI_NULLS OFF 
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_imsComprasSVendas]') IS NOT NULL
	drop procedure dbo.up_imsComprasSVendas
go

create procedure dbo.up_imsComprasSVendas

@data as datetime,
@codigo as varchar(254)

/* WITH ENCRYPTION */ 

AS
BEGIN

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#ultimoRefCompras'))
		DROP TABLE #ultimoRefCompras

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#ultimoRefCompraSVenda'))
		DROP TABLE #ultimoRefCompraSVenda

	select 
		* 
	into
		#ultimoRefCompras
	from (
		Select
			CODIGO_ENTIDADE		= '"' + RTRIM(LTRIM(Lojas.codigo)) + '"'
			,CODIGO_PRODUTO		= RTRIM(LTRIM(fn.ref))
			,NOME_PRODUTO		= '"' + RTRIM(LTRIM(fn.design)) + '"'
			,NUMERO_FACTURA		= '"' + convert(varchar,YEAR(FO.data)) + "-" + convert(varchar,fo.doccode) + "-" + convert(varchar,fo.adoc) + '"'
			,QTT				=  fn.qtt
			,[DATA]				= fn.ousrdata + fn.ousrhora
		From
			empresa	lojas (nolock)
			inner join fo (nolock) on fo.site = Lojas.site
			inner join fn (nolock) on fn.fostamp = fo.fostamp
			inner join cm1 (nolock) on cm1.cm = fo.doccode
		Where
			Lojas.codigo	= @codigo
			and fo.data		= @data 
			and cm1.FOLANSL = 1
			and fn.ref!='' 


		UNION ALL

		Select
			CODIGO_ENTIDADE		= '"' + RTRIM(LTRIM(Lojas.codigo)) + '"'
			,CODIGO_PRODUTO		= RTRIM(LTRIM(bi.ref))
			,NOME_PRODUTO		= '"' + RTRIM(LTRIM(bi.design)) + '"'
			,NUMERO_FACTURA		= '"' + convert(varchar,YEAR(bo.dataobra)) + "-" + convert(varchar,bo.ndos) + "-" + convert(varchar,bo.obrano) + '"'
			,QTT				=  bi.qtt
			,[DATA]				= bi.ousrdata + bi.ousrhora
		From
			empresa lojas (nolock)
			inner join bo (nolock) on bo.site = Lojas.site
			inner join bi (nolock) on bi.bostamp = bo.bostamp
			inner join ts (nolock) on ts.ndos = bo.ndos
		Where
			Lojas.codigo = @codigo
			and bo.ousrdata = @data 
			and ts.STOCKS =	1
			and ts.bdempresas = 'FL'
			and bi.ref!=''
	) a


	select	
		 *,
		 ROW_NUMBER() OVER(PARTITION BY CODIGO_PRODUTO ORDER BY data desc) AS rowLine
	into
		#ultimoRefCompraSVenda
	from #ultimoRefCompras




	delete from #ultimoRefCompraSVenda where rowLine!=1

	select 
		CODIGO_ENTIDADE,
		CODIGO_PRODUTO,
		NOME_PRODUTO,
		NUMERO_FACTURA,
		QTT,
		[DATA]
	from  #ultimoRefCompraSVenda


	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#ultimoRefCompras'))
		DROP TABLE #ultimoRefCompras

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#ultimoRefCompraSVenda'))
		DROP TABLE #ultimoRefCompraSVenda

END

Go
Grant Execute on dbo.up_imsComprasSVendas to Public
Grant control on dbo.up_imsComprasSVendas to Public
Go