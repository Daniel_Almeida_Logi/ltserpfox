/*
	IMS DETALHES
	Devolve detalhe de vendas mas apenas de compartiicpação
	
	exec up_imsDetalheComparticipacaoVendas '20170506','0004200'
*/
 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_imsDetalheComparticipacaoVendas]') IS NOT NULL
	drop procedure dbo.up_imsDetalheComparticipacaoVendas
go

create procedure dbo.up_imsDetalheComparticipacaoVendas
	@data as datetime,
	@codigo as varchar(254)

/* WITH ENCRYPTION */ 

AS
BEGIN


	--if Object_id('tempdb.dbo.#vd_reg') is not null
	--	drop table #vd_reg
	--if Object_id('tempdb.dbo.#cred_reg') is not null
	--	drop table #cred_reg

	---- levantar vendas que originarem de outros docs (restrito por data)
	--select
	--	ofistamp
	--into
	--	#vd_reg
	--from
	--	fi (nolock)
	--	inner join ft (nolock) on ft.ftstamp = fi.ftstamp
	--	inner join td (nolock) on td.ndoc = ft.ndoc
	--Where
	--	ft.tipodoc != 3
	--	and ft.no > 200
	--	AND (ft.tipodoc != 4 or u_tipodoc = 4)
	--	AND td.U_TIPODOC not in (1, 5)
	--	and fi.ofistamp != ''
	--	and fdata between @data - 120 and @data + 120


	---- levantar regularizações (notas de crédito que deram origem a novas vendas)
	--select
	--	ofistamp
	--into
	--	#cred_reg
	--from
	--	fi (nolock)
	--	inner join ft (nolock) on ft.ftstamp = fi.ftstamp
	--	inner join td (nolock) on td.ndoc = ft.ndoc
	--Where
	--	ft.tipodoc = 3
	--	and ft.no > 200
	--	AND (FT.tipodoc != 4 or u_tipodoc = 4)
	--	AND td.U_TIPODOC not in (1, 5)
	--	and fi.ofistamp != ''
	--	and fdata between @data-120 and @data+120
	--	and fi.fistamp in (select ofistamp from #vd_reg)


	Select
		*
	From (
		Select
			CODIGO_ENTIDADE			=	'"' + Lojas.codigo + '"'
			,NUMERO_FACTURA			= '"' + convert(varchar,YEAR(ft.fdata)) + "-" + convert(varchar,ft.ndoc) + "-" + convert(varchar,ft.fno) + '"'
			,NUMERO_LINHA_FACTURA	=	(Select COUNT(fistamp)
											From	fi (nolock) fia Where fia.ftstamp = fi.ftstamp
													ANd (fia.ref != '' or fia.oref != '')
													and fia.lordem <= fi.lordem
											)
			,CODIGO_ENTIDADE_COMP	=	'"' + ft2.u_codigo + '"'
			,NOME_ENTIDADE			=	'"' + (select top 1 RTRIM(LTRIM(design)) from cptpla (nolock) Where cptpla.codigo = ft2.u_codigo) + '"'
			,p6						=	''
			,p7						=	''
			,VALOR_COMPART			=	convert(int,fi.u_ettent1*100)
			,p9						=	''
			,Portaria				=	'"' + RTRIM(LTRIM(fi.u_diploma)) + '"'
		From
			empresa	lojas (nolock)
			inner join ft (nolock)	on ft.site = Lojas.site
			inner join ft2 (nolock)	on ft2.ft2stamp = ft.ftstamp
			inner join fi (nolock)	on fi.ftstamp = ft.ftstamp
			inner join td (nolock)	on td.ndoc = ft.ndoc
		Where
			Lojas.codigo = @codigo
			--and ofistamp = ''
			and ft.tipodoc = 1
			and fdata = @data
			and (fi.u_ettent1 != 0)
			AND (FT.tipodoc!=4 or u_tipodoc=4)
			AND U_TIPODOC not in (1, 5)
			and (fi.ref != '' or fi.oref!='')
			--and fi.fistamp not in (select ofistamp from #cred_reg)
			and td.nmdoc not like '%import%'
				
		UNION ALL

		Select
			CODIGO_ENTIDADE			=	'"' + Lojas.codigo + '"'
			,NUMERO_FACTURA			= '"' + convert(varchar,YEAR(ft.fdata)) + "-" + convert(varchar,ft.ndoc) + "-" + convert(varchar,ft.fno) + '"'
			,NUMERO_LINHA_FACTURA	=	(Select COUNT(fistamp) 
											From	fi (nolock) fia Where fia.ftstamp = fi.ftstamp 
													ANd (fia.ref != '' or fia.oref != '')
													and fia.lordem <= fi.lordem 
											)
			,CODIGO_ENTIDADE_COMP	=	'"' + ft2.u_codigo2 + '"'
			,NOME_ENTIDADE			=	'"' + (select top 1 design from cptpla (nolock) Where cptpla.codigo = ft2.u_codigo2) + '"'
			,p6						=	''
			,p7						=	''
			,VALOR_COMPART			=	convert(int,fi.u_ettent2*100)
			,p9						=	''
			,Portaria				=	'"' + fi.u_diploma + '"'
		From
			empresa	lojas (nolock)
			inner join ft (nolock)	on ft.site = Lojas.site
			inner join ft2 (nolock)	on ft2.ft2stamp = ft.ftstamp
			inner join fi (nolock)	on fi.ftstamp = ft.ftstamp
			inner join td (nolock)	on td.ndoc = ft.ndoc
		Where
			Lojas.codigo = @codigo
			and ft.tipodoc = 1
			and fdata = @data
			and (fi.u_ettent2 != 0)
			AND (FT.tipodoc!=4 or u_tipodoc=4)
			AND U_TIPODOC not in (1, 5)
			and (fi.ref != '' or fi.oref!='')
			--and fi.fistamp not in (select ofistamp from #cred_reg)
			and td.nmdoc not like '%import%'
	) a
	order by
		NUMERO_FACTURA


	--if Object_id('tempdb.dbo.#vd_reg') is not null
	--	drop table #vd_reg
	--if Object_id('tempdb.dbo.#cred_reg') is not null
	--	drop table #cred_reg

END
 
Go
Grant Execute on dbo.up_imsDetalheComparticipacaoVendas to Public
Grant control on dbo.up_imsDetalheComparticipacaoVendas to Public
Go