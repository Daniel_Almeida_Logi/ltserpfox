/****** Script for SelectTopNRows command from SSMS  ******/
		

-- Delete the primary key constraint.  




	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dados'))
	DROP TABLE #dados


	-- cria tabela temporaria

	select 
		'stamp' = CONVERT(varchar(255), NEWID() ) , 
		*
	into 
		#dados
	from
		[IMS_MOVIMENTOS_201902] 
	
	where [Tipo mov] = 'V' and len([Código produto])>0

	
	order by
	 cast((convert(int, [Nº doc])) as varchar(20)),  [Nº mov]
	


	Insert into hist_vendas
	select 
	'ano' = YEAR(data),
	'mes' = MONTH(data),
	'dia' = DAY(data),
	'ref' = cast((convert(int,[Código produto])) as varchar(20)), 
	'pvp' = PVP,
	'qt' = UNIDADES,
	'site' = (select site from empresa(nolock) where ncont=LEFT(RTRIM(Ltrim(NIF)), 9)),
	'hora' =  convert(varchar(10), HORA, 108), 
	0,
	'nDoc' =  cast((convert(int, [Nº doc])) as varchar(20)),
	'nrMov'=  [Nº mov]

	from 
	#dados im



	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dados'))
	DROP TABLE #dados










