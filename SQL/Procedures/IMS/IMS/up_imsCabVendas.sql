/*
	IMS CABECALHO DE VENDAS
	Devolve informação do cabeçalho de vendas
	exec up_imsCabVendas '20221219','0021326'

	select * from ft where fno=1 and fdata='20160102'


*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
  
if OBJECT_ID('[dbo].[up_imsCabVendas]') IS NOT NULL
	drop procedure dbo.up_imsCabVendas
go

create procedure dbo.up_imsCabVendas
	@data as datetime,
	@codigo as varchar(254)
		
/* WITH ENCRYPTION */ 

AS
BEGIN
	SET NOCOUNT ON

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#movVendas'))
		DROP TABLE #movVendas
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#movCompras'))
		DROP TABLE #movCompras
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#movComprasSemVendas'))
		DROP TABLE #movComprasSemVendas




	--if Object_id('tempdb.dbo.#vd_reg') is not null
	--	drop table #vd_reg
	--if Object_id('tempdb.dbo.#cred_reg') is not null
	--	drop table #cred_reg

	---- levantar vendas que originarem de outros docs (restrito por data)
	--select
	--	ofistamp
	--into
	--	#vd_reg
	--from
	--	fi (nolock)
	--	inner join ft (nolock) on ft.ftstamp = fi.ftstamp
	--	inner join td (nolock) on td.ndoc = ft.ndoc
	--Where
	--	ft.tipodoc != 3
	--	and ft.no > 200
	--	AND (ft.tipodoc != 4 or u_tipodoc = 4)
	--	AND td.U_TIPODOC not in (1, 5)
	--	and fi.ofistamp != ''
	--	and fdata between @data - 120 and @data + 120


	---- levantar regularizações (notas de crédito que deram origem a novas vendas)
	--select
	--	ofistamp
	--into
	--	#cred_reg
	--from
	--	fi (nolock)
	--	inner join ft (nolock) on ft.ftstamp = fi.ftstamp
	--	inner join td (nolock) on td.ndoc = ft.ndoc
	--Where
	--	ft.tipodoc = 3
	--	and ft.no > 200
	--	AND (FT.tipodoc != 4 or u_tipodoc = 4)
	--	AND td.U_TIPODOC not in (1, 5)
	--	and fi.ofistamp != ''
	--	and fdata between @data-120 and @data+120
	--	and fi.fistamp in (select ofistamp from #vd_reg)

	DECLARE @operator		    VARCHAR(8) = 'ONL'
	
	Select
		CODIGO_ENTIDADE			= '"' + a.codigo + '"'
		,NUMERO_FACTURA			= '"' + convert(varchar,YEAR(fdata)) + "-" + convert(varchar,a.ndoc) + "-" + convert(varchar,a.fno) + '"'
		,DATA_FACTURA			= '"' + REPLACE(convert(varchar,a.fdata,102)+ ' ' + a.ousrhora,'.','-') + '"'
		,NUMERO_MOVIMENTO		= '"' + convert(varchar,YEAR(fdata)) + convert(varchar,a.ndoc) + convert(varchar,a.fno) + '"'
		,NUMERO_RECEITA			= '"' + rtrim(ltrim(ft2.u_receita)) + '"'
		,TIPO_VENDA				= '"' + convert(varchar(2),a.tipo_venda) +'"'
		,TIPO_ENCOMENDA			= '"' + convert(varchar(2),a.tipo_encomenda) +'"'
		,TIPO_ENTREGA			= '"' + convert(varchar(2),a.tipo_entrega) +'"'
		,LOCAL_PRESCRICAO		= '"' + isnull(RTRIM(LTRIM(de.local_presc_cod)),'') + '"'
		,ESPECIALIDADE_MEDICA	= '"' + isnull(RTRIM(LTRIM(de.prescritor_especialidade)),'') + '"'
		,c11					= ''
		,c12					= '""'
		,TIPO_CLIENTE			= ''
		,ANO_NASCIMENTO			= '"' + convert(varchar,year(cl.nascimento)) + '"'
		,GENERO					= '"' + RTRIM(LTRIM(cl.sexo)) + '"'
		,CODIGO_POSTAL			= '"' + RTRIM(LTRIM(cl.codpost)) + '"'
		,c17					= ''
		,c18					= ''

	into #movVendas
	From (
		Select
			Lojas.codigo,
			ft.fno,
			ft.fdata,
			ft.ousrhora,
			ft.ndoc,
			ft.ftstamp,
			ft.no,
			ft.estab,
		    tipo_venda     =  isnull((select 
										top 1 2
										from 
											fi(nolock)  
										inner join 
											bi(nolock) on bi.bistamp=fi.bistamp			
										where
											fi.ftstamp = ft.ftstamp 
											and bi.ousrinis = @operator
								),1),
			tipo_encomenda   =  isnull((select 
										top 1 2
										from 
											fi(nolock)  
										inner join 
											bi(nolock) on bi.bistamp=fi.bistamp			
										where
											fi.ftstamp = ft.ftstamp 
											and bi.ousrinis = @operator
								),1),

			tipo_entrega     =  isnull((select 
										top 1 case 
												when lower(rtrim(ltrim(isnull(bo2.modo_envio,''))))  ='homedelivery' then 2
												else 1
												end
										from 
											fi(nolock)  
										inner join 
											bi(nolock) on bi.bistamp=fi.bistamp	
										inner join
											bo(nolock) on bo.bostamp=bi.bostamp		
										inner join
											bo2(nolock) on bo.bostamp=bo2.bo2stamp				
										where
											fi.ftstamp = ft.ftstamp 
											and bi.ousrinis = @operator
								),1)

		From
			empresa	lojas (nolock)
			inner join ft (nolock)	on ft.site = Lojas.site
			inner join fi (nolock)	on fi.ftstamp = ft.ftstamp
			inner join td (nolock)	on td.ndoc = ft.ndoc
		Where
			ft.no >= 200
			and	Lojas.codigo = @codigo
			and fdata = @data
			AND (FT.tipodoc != 4 or u_tipodoc = 4)
			AND U_TIPODOC not in (1, 5)
			--and td.tipodoc != 3
			and (fi.ref != '' or fi.oref!='')
			--and fi.fistamp not in (select ofistamp from #cred_reg)

	

		--UNION ALL

		--Select
		--	Lojas.codigo,
		--	ft.fno,
		--	ft.fdata,
		--	ft.ousrhora,
		--	ft.ndoc,
		--	ft.ftstamp,
		--	ft.no,
		--	ft.estab
		--From
		--	empresa	lojas (nolock)
		--	inner join ft (nolock)	on ft.site = Lojas.site
		--	inner join fi (nolock)	on fi.ftstamp = ft.ftstamp
		--	inner join td (nolock)	on td.ndoc = ft.ndoc
		--Where
		--	ft.tipodoc = 3
		--	and fistamp not in (select ofistamp from #vd_reg)
		--	and ft.no >= 200
		--	and	Lojas.codigo = @codigo
		--	and fdata = @data
		--	AND (FT.tipodoc != 4 or u_tipodoc = 4)
		--	AND U_TIPODOC not in (1, 5)
		--	and (fi.ref != '' or fi.oref!='')
	)	a
		inner join ft2 (nolock) on ft2.ft2stamp = a.ftstamp
		inner join b_utentes cl (nolock) on cl.no = a.no and cl.estab = a.estab
		left join dispensa_eletronica de on de.token = ft2.token
	group By
		a.codigo, a.fno, a.fdata, a.ousrhora, a.ndoc,
		ft2.u_receita, ft2.u_entpresc, ft2.u_nopresc,
		cl.no, cl.estab, cl.nascimento, cl.sexo, cl.codpost,
		--a.vendedor a.no, a.estab, cl.tipo, cl.nascimento, cl.u_sexo, cl.codpost
		de.local_presc_cod, de.prescritor_especialidade, a.tipo_venda, a.tipo_entrega, a.tipo_encomenda
	order by
		a.fdata

    --Juntar Compras
	create table #movCompras (
		CODIGO_ENTIDADE					VARCHAR(100)
		,CODIGO_PRODUTO					CHAR(18)
		,NOME_PRODUTO					VARCHAR(200)
		,NUMERO_FACTURA					VARCHAR(254)
		,QTT							INT
		,[DATA]							DATETIME
	)
	INSERT #movCompras
	exec up_imsComprasSVendas @data, @codigo



	--A pedido do IMS
	--remover referencias que já existem na parte das vendas
	select 
		distinct  
			#movCompras.NUMERO_FACTURA,
			#movCompras.CODIGO_ENTIDADE

	into
		#movComprasSemVendas
	from 
		#movCompras 
	where 
		#movCompras.CODIGO_PRODUTO not in (
			select 
				fi.ref 
			from 
				fi(nolock)
				inner join ft (nolock)	on fi.ftstamp = ft.ftstamp
				inner join td(nolock) on td.ndoc = ft.ndoc
				inner join empresa(nolock) Lojas on ft.site = Lojas.site
				inner join b_utentes(nolock) cl on cl.no = ft.no and cl.estab = ft.estab
			Where
				ft.no >= 200
				and	Lojas.codigo = @codigo
				and fdata = @data
				AND (FT.tipodoc != 4 or u_tipodoc = 4)
				AND U_TIPODOC not in (1, 5)
				--and td.tipodoc != 3
				and (fi.ref != '' or fi.oref!='')
		)
		

	
	  --devolver result set final
	select 
		* 
	from
	(   
			Select
				CODIGO_ENTIDADE			
				,NUMERO_FACTURA			
				,DATA_FACTURA			
				,NUMERO_MOVIMENTO		
				,NUMERO_RECEITA			
				,TIPO_VENDA				
				,TIPO_ENCOMENDA			
				,TIPO_ENTREGA			
				,LOCAL_PRESCRICAO		
				,ESPECIALIDADE_MEDICA	
				,c11					
				,c12					
				,TIPO_CLIENTE			
				,ANO_NASCIMENTO			
				,GENERO					
				,CODIGO_POSTAL			
				,c17					
				,c18				
			from
				#movVendas
		UNION ALL
			select 
				CODIGO_ENTIDADE			
				,NUMERO_FACTURA		   
				,DATA_FACTURA          = '"'+ convert(varchar,convert(date,@data) ,120) + " 23:59:59"+'"' 		
				,NUMERO_MOVIMENTO	   = '"1"'	
				,NUMERO_RECEITA	       = '""'		
				,TIPO_VENDA			   = '"1"'
				,TIPO_ENCOMENDA		   = '"1"'	
				,TIPO_ENTREGA		   = '"1"'		
				,LOCAL_PRESCRICAO	   = '""'	
				,ESPECIALIDADE_MEDICA  = '""'	
				,c11			       = ''		
				,c12				   = '""'
				,TIPO_CLIENTE		   = ''	
				,ANO_NASCIMENTO		   = '"1900"'	
				,GENERO				   = '""'	
				,CODIGO_POSTAL		   = '""'	
				,c17				   = ''	
				,c18                   = ''
			from
				#movComprasSemVendas
		) a
	



	--if Object_id('tempdb.dbo.#vd_reg') is not null
	--	drop table #vd_reg
	--if Object_id('tempdb.dbo.#cred_reg') is not null
	--	drop table #cred_reg

	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#movVendas'))
		DROP TABLE #movVendas
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#movCompras'))
		DROP TABLE #movCompras
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#movComprasSemVendas'))
		DROP TABLE #movComprasSemVendas


END

GO
Grant Execute on dbo.up_imsCabVendas to Public
Grant control on dbo.up_imsCabVendas to Public
Go



