
		DECLARE @site_nr int;


		DECLARE empresas_cursor CURSOR FOR
		SELECT no FROM empresa;

		OPEN empresas_cursor;
		FETCH NEXT FROM empresas_cursor into  @site_nr;

		WHILE @@FETCH_STATUS = 0
			BEGIN
							
				IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosImpSt'))
				DROP TABLE #dadosImpSt


				Select
					
					ststamp =  left(CONVERT(varchar(255), NEWID() ),25)
					,ref = LTRIM(RTRIM(cast((convert(int,prod.[Código produto])) as varchar(20))))
					,design = LTRIM(RTRIM(prod.[Nome produto]))
					,epv1 = convert(decimal(11,2),pvp.PVP) 
					,epcusto = case 
									when round(convert(decimal(11,2),pvp.PVP),0) = 0 then 0
									else convert(decimal(11,2),convert(decimal(11,2),pvp.PVP)   -  ((convert(decimal(11,2),pvp.PVP)) * prod.IVA)) 
								end
					,epcpond = 0
					,epcult = case 
									when round(convert(decimal(11,2),pvp.PVP),0) = 0 then 0 
									else convert(decimal(11,2), convert(decimal(11,2),pvp.PVP)   -  ((convert(decimal(11,2),pvp.PVP)) * prod.IVA)) 
								end
					,ivaincl = 1
					,iva1incl = 1
					,iva2incl = 1 
					,iva3incl = 1
					,iva4incl = 1
					,iva5incl = 1
					,tabiva = (select top 1 codigo from taxasiva(nolock) where taxa = prod.IVA*100 )
					,stmax = 0
					,ptoenc = 0
					,familia = '99'
					,faminome = 'Outros' 
					,usr1 = ''
					,ousrdata = convert(varchar, getdate(),102)
					,ousrhora = convert(varchar, getdate(),108)
					,ousrinis = 'ADM'
					,usrdata = convert(varchar,getdate(),102)
					,usrhora = convert(varchar,getdate(),108)
					,usrinis = 'ADM'
			        ,u_lab = ''
			        ,site_nr = @site_nr
			        ,u_tipoetiq = 1
				into
					#dadosImpSt
				from
					IMS_PRODUTOS_201902 prod
					left join IMS_PVP_201902 pvp on prod.[Código produto] = pvp.[Código produto] 
					left join st(nolock) on st.ref =  LTRIM(RTRIM(cast((convert(int,prod.[Código produto])) as varchar(20)))) and st.site_nr = @site_nr
				where 
					st.ref is null	
		

				

				DELETE FROM #dadosImpSt WHERE ststamp IN (
					SELECT ststamp FROM (
						SELECT 
							ststamp
							,ROW_NUMBER() OVER (PARTITION BY ref ORDER BY ref) AS [ItemNumber]
						FROM 
							#dadosImpSt
					) a WHERE ItemNumber > 1 -- Keep only the first unique item
				)


		

				INSERT INTO st (
					ststamp,ref,design,epv1,epcusto,epcpond,epcult
					,ivaincl,st.iva1incl,st.iva2incl,st.iva3incl,st.iva4incl,st.iva5incl
					,st.tabiva,stmax,ptoenc,familia,faminome,usr1
					,ousrdata,ousrhora,ousrinis,usrdata,usrhora,usrinis
					,u_lab, site_nr, u_tipoetiq
				)
				Select
					ststamp  
					,LTRIM(RTRIM(ref)) 
					,LTRIM(RTRIM(design))
					,isnull(epv1,0) 
					,isnull(epcusto,0) 
					,isnull(epcpond,0) 
					,isnull(epcult,0)
					,ivaincl 
					,iva1incl 
					,iva2incl
					,iva3incl
					,iva4incl 
					,iva5incl 
					,tabiva
					,stmax
					,ptoenc
					,familia
					,faminome
					,usr1
					,ousrdata
					,ousrhora
					,ousrinis
					,usrdata
					,usrhora
					,usrinis
			        ,u_lab 
			        ,site_nr 
			        ,u_tipoetiq
					from #dadosImpSt
					

		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosImpSt'))
		DROP TABLE #dadosImpSt

		
		FETCH NEXT FROM empresas_cursor into  @site_nr;
	end


	close empresas_cursor
	deallocate  empresas_cursor

					
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosImpSt'))
	DROP TABLE #dadosImpSt


	-- importa marcas e labs

	update 
				st 
			set 
				usr1=case when fprod.u_marca!='''' then fprod.u_marca else usr1 end, 
				u_lab=case when fprod.titaimdescr!='''' then fprod.titaimdescr else st.u_lab end, 
				u_secstamp=fprod.secstamp, 
				u_depstamp=fprod.depstamp 
			from 
				st (nolock) 
				inner join fprod (nolock) on fprod.cnp=st.ref 
			where 
				st.usr1!=fprod.u_marca or st.u_lab!=fprod.titaimdescr 
				or st.u_secstamp!=fprod.secstamp or st.u_depstamp!=fprod.depstamp












	