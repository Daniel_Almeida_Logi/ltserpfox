/*
	IMS DETALHE DE VENDAS
	Devolve informação do detalhe de vendas
	exec up_imsDetalheVendas '20221219','0021326'

*/ 

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_imsDetalheVendas]') IS NOT NULL
	drop procedure dbo.up_imsDetalheVendas
go

create procedure dbo.up_imsDetalheVendas

@data as datetime,
@codigo as varchar(254)

/* WITH ENCRYPTION */

AS
BEGIN
	SET NOCOUNT ON

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#movVendas'))
		DROP TABLE #movVendas
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#movCompras'))
		DROP TABLE #movCompras
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#resultFinal'))
		DROP TABLE #resultFinal
	
	
	Select 
		*
	INTO 
		#movVendas
	From (
		Select
			CODIGO_ENTIDADE			=	'"' + Lojas.codigo + '"'
			,NUMERO_FACTURA			= '"' + convert(varchar,YEAR(ft.fdata)) + "-" + convert(varchar,ft.ndoc) + "-" + convert(varchar,ft.fno) + '"'
			,NUMERO_LINHA_FACTURA	=	(Select COUNT(fistamp)
											From	fi fia (nolock) Where fia.ftstamp = fi.ftstamp
													ANd (fia.ref != '' or fia.oref != '')
													and fdata = @data
													and fia.lordem <= fi.lordem
											)
			,NUMERO_RECEITA			=	'"' + RTRIM(LTRIM(ft2.u_receita)) + '"'
			,CODIGO_PRODUTO			=	RTRIM(LTRIM((case when fi.ref='' then fi.oref else fi.ref end)))
			,NOME_PRODUTO			=	'"' + RTRIM(LTRIM(fi.design)) + '"'
			,CODIGO_PRODUTO_PRESCRITO = '"' + case  when ft.nmdoc = 'Reg. a Cliente' then
														isnull((select top 1 
															case 
																when medicamento_cod!='' then medicamento_cod 
																else medicamento_cnpem 
																end
														from Dispensa_Eletronica_D ded (nolock)
														left join fi fi_aux(nolock) on fi_aux.id_dispensa_eletronica_d=ded.id
														where fi_aux.fistamp = fi.ofistamp
														),'') + '"'
			else
														isnull((select top 1 
															case 
																when medicamento_cod!='' then medicamento_cod 
																else medicamento_cnpem 
																end
														from Dispensa_Eletronica_D ded (nolock)
														where ded.id = fi.id_dispensa_eletronica_d
														),'') + '"'
			end
			,p8						=	''
			,PVP					=	convert(int,fi.u_epvp*100)
			,VALOR_UTENTE			=	convert(int,fi.etiliquido*100)
			,p11					=	''
			,p12					=	''
			,QUANTIDADE				=	convert(int,case when ft.tipodoc=3 then fi.qtt*(-1) else fi.qtt end)
			,p14					=	''
			,Stock_Final			=	convert(int,
											(ISNULL((
												SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
												FROM	sl (nolock)
												WHERE	ref = fi.REF AND sl.ousrdata <= ft.ousrdata
											),0) - fi.qtt)
										)
		From
			empresa	lojas (nolock)
			inner join ft (nolock)	on ft.site = Lojas.site
			inner join ft2 (nolock)	on ft2.ft2stamp = ft.ftstamp
			inner join fi (nolock)	on fi.ftstamp = ft.ftstamp
			inner join td (nolock)	on td.ndoc = ft.ndoc
		Where
			ft.no >= 200
			and	Lojas.codigo = @codigo
			and fdata = @data
			AND (FT.tipodoc != 4 or u_tipodoc = 4)
			AND U_TIPODOC not in (1, 5)
			and (fi.ref != '' or fi.oref!='')
			and td.nmdoc not like '%import%'
	) a
	order by
		a.numero_factura, a.numero_linha_factura


	create table #movCompras (
		CODIGO_ENTIDADE					VARCHAR(100)
		,CODIGO_PRODUTO					CHAR(18)
		,NOME_PRODUTO					VARCHAR(200)
		,NUMERO_FACTURA					VARCHAR(254)
		,QTT							INT
		,[DATA]							DATETIME
	)
	INSERT #movCompras
	exec up_imsComprasSVendas @data, @codigo

	SELECT 
		*
	INTO 
		#resultFinal
	FROM (
				SELECT
					CODIGO_ENTIDADE		
					,NUMERO_FACTURA		
					,NUMERO_LINHA_FACTURA
					,NUMERO_RECEITA			
					,CODIGO_PRODUTO			
					,NOME_PRODUTO			
					,CODIGO_PRODUTO_PRESCRITO
					,p8				
					,PVP			
					,VALOR_UTENTE	
					,p11			
					,p12			
					,QUANTIDADE		
					,p14			
					,Stock_Final	
				FROM 
					#movVendas
			UNION ALL
				SELECT 
					 #movCompras.CODIGO_ENTIDADE		
					,#movCompras.NUMERO_FACTURA			
					,NUMERO_LINHA_FACTURA				= 1
					,NUMERO_RECEITA						= '""'						
					,#movCompras.CODIGO_PRODUTO			
					,#movCompras.NOME_PRODUTO					
					,CODIGO_PRODUTO_PRESCRITO			= '""'
					,p8								    = ''
					,PVP								= 0
					,VALOR_UTENTE					    = 0
					,p11								= ''
					,p12								= ''	
					,QUANTIDADE							= 0
					,p14								= ''
					,Stock_Final					    =  convert(int,(ISNULL((
															SELECT	SUM(CASE WHEN CM < 50 THEN QTT ELSE -QTT END)
															FROM	sl (nolock)
															WHERE	ref = #movCompras.CODIGO_PRODUTO AND sl.ousrdata <= 
															cast(convert(date,#movCompras.DATA) as datetime) + cast('23:59:00' as datetime) --stock fim do dia
															),0)))			
				FROM
					#movCompras
				where
					#movCompras.CODIGO_PRODUTO not in (select CODIGO_PRODUTO from #movVendas)

		) AS x

		SELECT	
			*
		FROM 
			#resultFinal
		order by
			#resultFinal.numero_factura, #resultFinal.numero_linha_factura
		
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#movVendas'))
		DROP TABLE #movVendas
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#movCompras'))
		DROP TABLE #movCompras
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#resultFinal'))
		DROP TABLE #resultFinal
END

GO
Grant Execute on dbo.up_imsDetalheVendas to Public
Grant control on dbo.up_imsDetalheVendas to Public
Go

