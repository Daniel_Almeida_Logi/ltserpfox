/* 
	IMS DETALHE ANULACAO DE VENDAS * DEPRECATED - NÃO EXISTE ANULAÇÃO DE VENDAS NO PHC*
	Devolve informação de detalhe de vendas anulados

	exec up_imsDetalheAnulacaoVendas '20120701','1234567'
	 
	Nota: apenas eliminar esta procedures depois de se verificar com a IMS que já não necessitam do ficheiro correspondente.
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_imsDetalheAnulacaoVendas]') IS NOT NULL
	drop procedure dbo.up_imsDetalheAnulacaoVendas
go

create procedure dbo.up_imsDetalheAnulacaoVendas
	@data as datetime,
	@codigo as varchar(254)

/* WITH ENCRYPTION */ 

AS
BEGIN

	Select
		CODIGO_ENTIDADE			=	Lojas.codigo
		,NUMERO_FACTURA			=	ft.fno
		,NUMERO_LINHA_FACTURA	=	(Select COUNT(fistamp)
										From	fi fia Where fia.ftstamp = fi.ftstamp
												ANd ref != ''
												and fia.lordem <= fi.lordem
										)
		,CODIGO_PRODUTO			=	fi.ref
		,QUANTIDADE				=	fi.qtt
		,p6						=	''
		,DATA_ANULACAO			=	convert(varchar,ft.fdata,102)+ ' ' + ft.ousrhora
	From
		empresa	lojas (nolock)
		inner join ft (nolock)	on ft.site = Lojas.site
		inner join fi (nolock)	on fi.ftstamp = ft.ftstamp
	Where
		1=0
		

END
 
GO
Grant Execute on dbo.up_imsDetalheAnulacaoVendas to Public
Grant control on dbo.up_imsDetalheAnulacaoVendas to Public
Go