SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_print_re]') IS NOT NULL
    DROP procedure dbo.up_print_re
GO

CREATE procedure dbo.up_print_re
	@restamp VARCHAR(25) 

AS
	SELECT 
		*,
		(SELECT (CASE WHEN ISNULL(atcud,'') <> '' THEN atcud ELSE (select (CASE WHEN ISNULL(atcud,'') <> '' THEN ATCUD ELSE '0' END) from cm1(nolock) where cm = re.ndoc) END) FROM tsre(nolock) WHERE ndoc = re.ndoc) as ATCUD,
		(SELECT (CASE WHEN ISNULL(codsaft,'') <> '' THEN codsaft ELSE (select (CASE WHEN ISNULL(tiposaft,'') <> '' THEN tiposaft ELSE '' END) from cm1(nolock) where cm = re.ndoc) END) FROM tsre(nolock) WHERE ndoc = re.ndoc) as codsaft
	FROM
		re(nolock)
	WHERE
		restamp = @restamp

GO
GRANT EXECUTE on dbo.up_print_re TO PUBLIC
GRANT Control on dbo.up_print_re TO PUBLIC
GO