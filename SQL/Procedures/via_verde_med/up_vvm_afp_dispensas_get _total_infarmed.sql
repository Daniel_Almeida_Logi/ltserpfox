/*
	use E01322A
	exec up_vvm_afp_dispensas_get_total_infarmed '20180801', '20190430'

	select * from ext_vvmweb where (nolock)
	 cliente_id not in ('00230','00434','02950','03050','03999','04120','90070','22284','16730','99001','99002')

*/




SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_vvm_afp_dispensas_get_total_infarmed]') IS NOT NULL
	drop procedure dbo.up_vvm_afp_dispensas_get_total_infarmed
go

CREATE procedure dbo.up_vvm_afp_dispensas_get_total_infarmed
	@dataIni varchar(8),
	@dataFim varchar(8),
	@cliente_id varchar(5) = ''


/* with encryption */
AS
	SET NOCOUNT ON

	select
		v2.ref as  'N� Registo'

		,(select isnull(sum(ext_vvmweb_dd.qt_dispensa),0) from ext_vvmweb(nolock)  
		inner join ext_vvmweb_d (nolock)  on ext_vvmweb.token = ext_vvmweb_d.token
		inner join ext_vvmweb_dd (nolock) on ext_vvmweb_d.token = ext_vvmweb_dd.token
		where ext_vvmweb_dd.enviado = 1 and year(ext_vvmweb.data)=2018 and month(ext_vvmweb.data)=8
		and ext_vvmweb.cliente_id = case when @cliente_id='' then ext_vvmweb.cliente_id else @cliente_id end
		and cliente_id not in ('00230','00434','02950','03050','03999','04120','90070','22284','16730','99001','99002')
		and ext_vvmweb_d.ref =  v2.ref
		) as "ago/2018"

		,(select isnull(sum(ext_vvmweb_dd.qt_dispensa),0) from ext_vvmweb(nolock)  
		inner join ext_vvmweb_d (nolock)  on ext_vvmweb.token = ext_vvmweb_d.token
		inner join ext_vvmweb_dd (nolock) on ext_vvmweb_d.token = ext_vvmweb_dd.token
		where ext_vvmweb_dd.enviado = 1 and year(ext_vvmweb.data)=2018 and month(ext_vvmweb.data)=9
		and ext_vvmweb.cliente_id = case when @cliente_id='' then ext_vvmweb.cliente_id else @cliente_id end
		and cliente_id not in ('00230','00434','02950','03050','03999','04120','90070','22284','16730','99001','99002')
		and ext_vvmweb_d.ref =  v2.ref
		) as "set/2018"

		,(select isnull(sum(ext_vvmweb_dd.qt_dispensa),0) from ext_vvmweb(nolock)  
		inner join ext_vvmweb_d (nolock)  on ext_vvmweb.token = ext_vvmweb_d.token
		inner join ext_vvmweb_dd (nolock) on ext_vvmweb_d.token = ext_vvmweb_dd.token
		where ext_vvmweb_dd.enviado = 1 and year(ext_vvmweb.data)=2018 and month(ext_vvmweb.data)=10
		and ext_vvmweb.cliente_id = case when @cliente_id='' then ext_vvmweb.cliente_id else @cliente_id end
		and cliente_id not in ('00230','00434','02950','03050','03999','04120','90070','22284','16730','99001','99002')
		and ext_vvmweb_d.ref =  v2.ref
		) as "out/2018"



		,(select isnull(sum(ext_vvmweb_dd.qt_dispensa),0) from ext_vvmweb(nolock)  
		inner join ext_vvmweb_d (nolock)  on ext_vvmweb.token = ext_vvmweb_d.token
		inner join ext_vvmweb_dd (nolock) on ext_vvmweb_d.token = ext_vvmweb_dd.token
		where ext_vvmweb_dd.enviado = 1 and year(ext_vvmweb.data)=2018 and month(ext_vvmweb.data)=11
		and ext_vvmweb.cliente_id = case when @cliente_id='' then ext_vvmweb.cliente_id else @cliente_id end
		and cliente_id not in ('00230','00434','02950','03050','03999','04120','90070','22284','16730','99001','99002')
		and ext_vvmweb_d.ref =  v2.ref
		) as "nov/2018"



		,(select isnull(sum(ext_vvmweb_dd.qt_dispensa),0) from ext_vvmweb(nolock)  
		inner join ext_vvmweb_d (nolock)  on ext_vvmweb.token = ext_vvmweb_d.token
		inner join ext_vvmweb_dd (nolock) on ext_vvmweb_d.token = ext_vvmweb_dd.token
		where ext_vvmweb_dd.enviado = 1 and year(ext_vvmweb.data)=2018 and month(ext_vvmweb.data)=12
		and ext_vvmweb.cliente_id = case when @cliente_id='' then ext_vvmweb.cliente_id else @cliente_id end
		and cliente_id not in ('00230','00434','02950','03050','03999','04120','90070','22284','16730','99001','99002')
		and ext_vvmweb_d.ref =  v2.ref
		) as "dez/2018"



		,(select isnull(sum(ext_vvmweb_dd.qt_dispensa),0) from ext_vvmweb(nolock)  
		inner join ext_vvmweb_d (nolock)  on ext_vvmweb.token = ext_vvmweb_d.token
		inner join ext_vvmweb_dd (nolock) on ext_vvmweb_d.token = ext_vvmweb_dd.token
		where ext_vvmweb_dd.enviado = 1 and year(ext_vvmweb.data)=2019 and month(ext_vvmweb.data)=1
		and ext_vvmweb.cliente_id = case when @cliente_id='' then ext_vvmweb.cliente_id else @cliente_id end
		and cliente_id not in ('00230','00434','02950','03050','03999','04120','90070','22284','16730','99001','99002')
		and ext_vvmweb_d.ref =  v2.ref
		) as "jan/2019"


		,(select isnull(sum(ext_vvmweb_dd.qt_dispensa),0) from ext_vvmweb(nolock)  
		inner join ext_vvmweb_d (nolock)  on ext_vvmweb.token = ext_vvmweb_d.token
		inner join ext_vvmweb_dd (nolock) on ext_vvmweb_d.token = ext_vvmweb_dd.token
		where ext_vvmweb_dd.enviado = 1 and year(ext_vvmweb.data)=2019 and month(ext_vvmweb.data)=2
		and ext_vvmweb.cliente_id = case when @cliente_id='' then ext_vvmweb.cliente_id else @cliente_id end
		and cliente_id not in ('00230','00434','02950','03050','03999','04120','90070','22284','16730','99001','99002')
		and ext_vvmweb_d.ref =  v2.ref
		) as "fev/2019"


			,(select isnull(sum(ext_vvmweb_dd.qt_dispensa),0) from ext_vvmweb(nolock)  
		inner join ext_vvmweb_d (nolock)  on ext_vvmweb.token = ext_vvmweb_d.token
		inner join ext_vvmweb_dd (nolock) on ext_vvmweb_d.token = ext_vvmweb_dd.token
		where ext_vvmweb_dd.enviado = 1 and year(ext_vvmweb.data)=2019 and month(ext_vvmweb.data)=3
		and ext_vvmweb.cliente_id = case when @cliente_id='' then ext_vvmweb.cliente_id else @cliente_id end
		and cliente_id not in ('00230','00434','02950','03050','03999','04120','90070','22284','16730','99001','99002')
		and ext_vvmweb_d.ref =  v2.ref
		) as "mar/2019"


			,(select isnull(sum(ext_vvmweb_dd.qt_dispensa),0) from ext_vvmweb(nolock)  
		inner join ext_vvmweb_d (nolock)  on ext_vvmweb.token = ext_vvmweb_d.token
		inner join ext_vvmweb_dd (nolock) on ext_vvmweb_d.token = ext_vvmweb_dd.token
		where ext_vvmweb_dd.enviado = 1 and year(ext_vvmweb.data)=2019 and month(ext_vvmweb.data)=4
		and ext_vvmweb.cliente_id = case when @cliente_id='' then ext_vvmweb.cliente_id else @cliente_id end
		and cliente_id not in ('00230','00434','02950','03050','03999','04120','90070','22284','16730','99001','99002')
		and ext_vvmweb_d.ref =  v2.ref
		) as "abril/2019"


		,sum(v3.qt_dispensa) as 'Total'
	from 
		ext_vvmweb v1 (nolock)
		inner join ext_vvmweb_d (nolock) v2 on v1.token = v2.token
		inner join ext_vvmweb_dd (nolock) v3 on v2.token = v3.token
		--inner join emb_via_verde evv (nolock) on v2.ref = evv.ref
	where
		v1.data between @dataIni and @dataFim
		and v3.enviado = 1
		and v1.cliente_id = case when @cliente_id='' then v1.cliente_id else @cliente_id end
		and cliente_id not in ('00230','00434','02950','03050','03999','04120','90070','22284','16730','99001','99002')
	
		
	group by
		v2.ref 

Go
Grant Execute on dbo.up_vvm_afp_dispensas_get_total_infarmed to Public
Grant Control on dbo.up_vvm_afp_dispensas_get_total_infarmed to Public
Go



