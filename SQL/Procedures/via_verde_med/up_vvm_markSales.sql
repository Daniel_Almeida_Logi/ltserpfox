--
-- exec up_vvm_markSales '20160701', '20160731', 'Loja 1'
--
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_vvm_markSales]') IS NOT NULL
	drop procedure dbo.up_vvm_markSales
go

CREATE procedure dbo.up_vvm_markSales
	@dataIni varchar(8),
	@dataFim varchar(8),
	@site varchar(20)

/* with encryption */
AS
	SET NOCOUNT ON

	--
	insert into via_verde_med_s
		(id, data)
	select
		ft.ftstamp
		,ft.fdata
	from
		ft (nolock)
		inner join ft2 (nolock) on ft2.ft2stamp = ft.ftstamp
		inner join via_verde_med vvm (nolock) on vvm.id_receita = ft2.u_receita
		inner join via_verde_med_D vvmd (nolock) on vvm.id = vvmd.id_via_verde_med
		inner join emb_via_verde evv (nolock) on vvmd.ref = evv.ref
		inner join fi (nolock) on ft.ftstamp = fi.ftstamp and evv.ref = fi.ref
		inner join bo (nolock) on vvm.bostamp = bo.bostamp
	where
		ft.site = @site
		and ft.fdata between @dataIni and @dataFim
		and bo.site = @site
		and vvm.aceite = 1
		and vvm.estado != 'Devolucao'
		and ft.ftstamp not in (select id from via_verde_med_s (nolock))
		and fi.fistamp not in (select ofistamp from fi (nolock))
	group by
		ft.ftstamp, ft.fdata
	
Go
Grant Execute on dbo.up_vvm_markSales to Public
Grant Control on dbo.up_vvm_markSales to Public
Go


