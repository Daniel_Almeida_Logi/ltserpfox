/*

	exec up_vvm_afp_dispensas_get '20160501', '20160531', ''
	exec up_vvm_afp_dispensas_get '20160601', '20160630', '03999'
	exec up_vvm_afp_dispensas_get '20160701', '20160731', '03999'
	
	select * from ext_vvmweb where cliente_id='03999'
	select * from ext_vvmweb_dd where token in (select token from ext_vvmweb where cliente_id='00230')
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_vvm_afp_dispensas_get]') IS NOT NULL
	drop procedure dbo.up_vvm_afp_dispensas_get
go

CREATE procedure dbo.up_vvm_afp_dispensas_get
	@dataIni varchar(8),
	@dataFim varchar(8),
	@cliente_id varchar(5)

/* with encryption */
AS
	SET NOCOUNT ON

	select
		token = v3.token
		,infarmed = v1.cliente_id
		,nrReceita = v1.nr_receita
		,NifDistribuidor = v1.distribuidor_nif
		,NoEncomenda = v1.cliente_id
						+ '' + right(convert(varchar,v1.data,112),6) + left(replace(convert(varchar,v1.data,8),':',''),4)
						+ '' + replace(replace(replace(v1.nr,'/',''),'_',''),'-','')
				
		,cnp = v2.ref
		,qttDispensada = v3.qt_dispensa
		,qttDevolvida = v2.qt_devolvida
		,dataDispensa = replace(convert(varchar,v3.data_dispensa,103),'/','-')
		,horaDispensa = left(convert(varchar,v3.data_dispensa,108),5)
	from 
		ext_vvmweb v1 (nolock)
		inner join ext_vvmweb_d (nolock) v2 on v1.token = v2.token
		inner join ext_vvmweb_dd (nolock) v3 on v2.token = v3.token
		--inner join emb_via_verde evv (nolock) on v2.ref = evv.ref
	where
		v3.data_cre between @dataIni and @dataFim
		and v3.enviado = 0
		and v1.cliente_id = case when @cliente_id='' then v1.cliente_id else @cliente_id end
	order by
		v1.cliente_id, v3.data_dispensa asc

Go
Grant Execute on dbo.up_vvm_afp_dispensas_get to Public
Grant Control on dbo.up_vvm_afp_dispensas_get to Public
Go