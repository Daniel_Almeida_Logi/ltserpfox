/*

	
	exec up_vvm_afp_encomendas_get_total_infarmed '20180801', '20190430'

	use E01322A
	
	select distinct cliente_id, entidade_nif from ext_vvmweb where data between '20160601' and '20160630' 
		and cliente_id not in ('00230','00434','02950','03050','03999','04120','90070','22284','16730','99001','99002')
	order by cliente_id

	select * from ext_vvmweb_d where token='16237112054470PF95OxaogKY1zYkn1w8TEX'
	select * from ext_vvmweb_dd where token='16237112054470PF95OxaogKY1zYkn1w8TEX'
	
	select * from ext_vvmweb where cliente_id='35890'
	select * from ext_vvmweb
	select * from ext_vvmweb_d
	select * from ext_vvmweb_dd
	select * from ext_vvmweb_dd
	select * from ext_vvmweb_infarmed
*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_vvm_afp_encomendas_get_total_infarmed]') IS NOT NULL
	drop procedure dbo.up_vvm_afp_encomendas_get_total_infarmed
go

CREATE procedure dbo.up_vvm_afp_encomendas_get_total_infarmed
	@dataIni varchar(8),
	@dataFim varchar(8),
	@cliente_id varchar(5) = ''

/* with encryption */
AS
	SET NOCOUNT ON

    select
        v2.ref as  'N� Registo'
		,(select isnull(sum(qt_encomenda),0) from ext_vvmweb(nolock)  
				inner join ext_vvmweb_d  (nolock) on ext_vvmweb.token = ext_vvmweb_d.token 
				where ext_vvmweb.enviado = 1 and year(ext_vvmweb.data)=2018 and month(ext_vvmweb.data)=8
				and ext_vvmweb.cliente_id = case when @cliente_id='' then ext_vvmweb.cliente_id else @cliente_id end
				and cliente_id not in ('00230','00434','02950','03050','03999','04120','90070','22284','16730','99001','99002')
				and ext_vvmweb_d.ref =  v2.ref
				) as "ago/2018"
		,(select isnull(sum(qt_encomenda),0) from ext_vvmweb(nolock)  
				inner join ext_vvmweb_d  (nolock) on ext_vvmweb.token = ext_vvmweb_d.token 
				where ext_vvmweb.enviado = 1 and year(ext_vvmweb.data)=2018 and month(ext_vvmweb.data)=9
				and ext_vvmweb.cliente_id = case when @cliente_id='' then ext_vvmweb.cliente_id else @cliente_id end
				and cliente_id not in ('00230','00434','02950','03050','03999','04120','90070','22284','16730','99001','99002')
				and ext_vvmweb_d.ref =  v2.ref
				) as "set/2018"
		,(select isnull(sum(qt_encomenda),0) from ext_vvmweb(nolock)  
				inner join ext_vvmweb_d  (nolock) on ext_vvmweb.token = ext_vvmweb_d.token 
				where ext_vvmweb.enviado = 1 and year(ext_vvmweb.data)=2018 and month(ext_vvmweb.data)=10
				and ext_vvmweb.cliente_id = case when @cliente_id='' then ext_vvmweb.cliente_id else @cliente_id end
				and cliente_id not in ('00230','00434','02950','03050','03999','04120','90070','22284','16730','99001','99002')
				and ext_vvmweb_d.ref =  v2.ref
				) as "out/2018"
		 ,(select isnull(sum(qt_encomenda),0) from ext_vvmweb(nolock)  
				inner join ext_vvmweb_d  (nolock) on ext_vvmweb.token = ext_vvmweb_d.token 
				where ext_vvmweb.enviado = 1 and year(ext_vvmweb.data)=2018 and month(ext_vvmweb.data)=11
				and ext_vvmweb.cliente_id = case when @cliente_id='' then ext_vvmweb.cliente_id else @cliente_id end
				and cliente_id not in ('00230','00434','02950','03050','03999','04120','90070','22284','16730','99001','99002')
				and ext_vvmweb_d.ref =  v2.ref
				) as "nov/2018"
		 ,(select isnull(sum(qt_encomenda),0) from ext_vvmweb(nolock)  
				inner join ext_vvmweb_d  (nolock) on ext_vvmweb.token = ext_vvmweb_d.token 
				where ext_vvmweb.enviado = 1 and year(ext_vvmweb.data)=2018 and month(ext_vvmweb.data)=12
				and ext_vvmweb.cliente_id = case when @cliente_id='' then ext_vvmweb.cliente_id else @cliente_id end
				and cliente_id not in ('00230','00434','02950','03050','03999','04120','90070','22284','16730','99001','99002')
				and ext_vvmweb_d.ref =  v2.ref
				) as "dez/2018"
		  ,(select isnull(sum(qt_encomenda),0) from ext_vvmweb(nolock)  
				inner join ext_vvmweb_d  (nolock) on ext_vvmweb.token = ext_vvmweb_d.token 
				where ext_vvmweb.enviado = 1 and year(ext_vvmweb.data)=2019 and month(ext_vvmweb.data)=1
				and ext_vvmweb.cliente_id = case when @cliente_id='' then ext_vvmweb.cliente_id else @cliente_id end
				and cliente_id not in ('00230','00434','02950','03050','03999','04120','90070','22284','16730','99001','99002')
				and ext_vvmweb_d.ref =  v2.ref
				) as "jan/2019"
			,(select isnull(sum(qt_encomenda),0) from ext_vvmweb(nolock)  
				inner join ext_vvmweb_d  (nolock) on ext_vvmweb.token = ext_vvmweb_d.token 
				where ext_vvmweb.enviado = 1 and year(ext_vvmweb.data)=2019 and month(ext_vvmweb.data)=2
				and ext_vvmweb.cliente_id = case when @cliente_id='' then ext_vvmweb.cliente_id else @cliente_id end
				and cliente_id not in ('00230','00434','02950','03050','03999','04120','90070','22284','16730','99001','99002')
				and ext_vvmweb_d.ref =  v2.ref
				) as "fev/2019"
			,(select isnull(sum(qt_encomenda),0) from ext_vvmweb(nolock)  
				inner join ext_vvmweb_d  (nolock) on ext_vvmweb.token = ext_vvmweb_d.token 
				where ext_vvmweb.enviado = 1 and year(ext_vvmweb.data)=2019 and month(ext_vvmweb.data)=3
				and ext_vvmweb.cliente_id = case when @cliente_id='' then ext_vvmweb.cliente_id else @cliente_id end
				and cliente_id not in ('00230','00434','02950','03050','03999','04120','90070','22284','16730','99001','99002')
				and ext_vvmweb_d.ref =  v2.ref
				) as "mar/2019"
			,(select isnull(sum(qt_encomenda),0) from ext_vvmweb(nolock)  
				inner join ext_vvmweb_d  (nolock) on ext_vvmweb.token = ext_vvmweb_d.token 
				where ext_vvmweb.enviado = 1 and year(ext_vvmweb.data)=2019 and month(ext_vvmweb.data)=4
				and ext_vvmweb.cliente_id = case when @cliente_id='' then ext_vvmweb.cliente_id else @cliente_id end
				and cliente_id not in ('00230','00434','02950','03050','03999','04120','90070','22284','16730','99001','99002')
				and ext_vvmweb_d.ref =  v2.ref
				) as "abr/2019"
			 ,sum(v2.qt_encomenda) as "Total"
    from
        ext_vvmweb v1 (nolock)
        inner join ext_vvmweb_d v2 (nolock) on v1.token = v2.token
        --inner join emb_via_verde evv on v2.ref = evv.ref
    where 
		v1.data between @dataIni and @dataFim
		and v1.enviado = 1
		and v1.cliente_id = case when @cliente_id='' then v1.cliente_id else @cliente_id end
		and cliente_id not in ('00230','00434','02950','03050','03999','04120','90070','22284','16730','99001','99002')

	group by
		  v2.ref
Go
Grant Execute on dbo.up_vvm_afp_encomendas_get_total_infarmed to Public
Grant Control on dbo.up_vvm_afp_encomendas_get_total_infarmed to Public
Go


