/*
	
	
	exec up_vvm_afp_encomendas_get '20160601', '20160630', '35963'
	exec up_vvm_afp_encomendas_get '20160701', '20160731', '35963'
	
	select distinct cliente_id, entidade_nif from ext_vvmweb where data between '20160601' and '20160630' 
		and cliente_id not in ('00230','00434','02950','03050','03999','04120')
	order by cliente_id

	select * from ext_vvmweb_d where token='16237112054470PF95OxaogKY1zYkn1w8TEX'
	select * from ext_vvmweb_dd where token='16237112054470PF95OxaogKY1zYkn1w8TEX'
	
	select * from ext_vvmweb where cliente_id='35890'
	select * from ext_vvmweb
	select * from ext_vvmweb_d
	select * from ext_vvmweb_dd
	select * from ext_vvmweb_dd
	select * from ext_vvmweb_infarmed
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_vvm_afp_encomendas_get]') IS NOT NULL
	drop procedure dbo.up_vvm_afp_encomendas_get
go

CREATE procedure dbo.up_vvm_afp_encomendas_get
	@dataIni varchar(8),
	@dataFim varchar(8),
	@cliente_id varchar(5)

/* with encryption */
AS
	SET NOCOUNT ON

    select
		token = v1.token
        ,infarmed = v1.cliente_id
        ,nrReceita = v1.nr_receita
        ,NifDistribuidor = v1.distribuidor_nif
        ,NoEncomenda = v1.cliente_id
                        + '' + right(convert(varchar,v1.data,112),6) + left(replace(convert(varchar,v1.data,8),':',''),4)
                        + '' + replace(replace(replace(v1.nr,'/',''),'_',''),'-','')
        ,cnp = v2.ref
        ,QttEncomenda = v2.qt_encomenda
        ,dataEncomenda = replace(convert(varchar,v1.data,103),'/','-')
        ,horaEncomenda = left(convert(varchar,v1.data,108),5)
    from
        ext_vvmweb v1
        inner join ext_vvmweb_d v2 (nolock) on v1.token = v2.token
        --inner join emb_via_verde evv on v2.ref = evv.ref
    where 
		v1.data_cre between @dataIni and @dataFim
		and v1.enviado = 0
		and v1.cliente_id = case when @cliente_id='' then v1.cliente_id else @cliente_id end
    order by
        v1.cliente_id, v1.data asc


Go
Grant Execute on dbo.up_vvm_afp_encomendas_get to Public
Grant Control on dbo.up_vvm_afp_encomendas_get to Public
Go