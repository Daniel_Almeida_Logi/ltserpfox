/*

	exec up_vvm_get '20161001', '20161004', 'Loja 1', 0

	select * from via_verde_med where id_receita='2011000026309337502'
	select * from bo where bostamp='JLB3CCA18EF-C89B-4F84-915'

	Notas:
		. Npo resultado final, � obrigat�rio que para cada venda, o registo anterior seja a encomenda correspondente
		. As encomendas aparecem sempre vis�veis (o parametro @enviado � apenas para as vendas)
		. O campo enviado valida se a mesma j� foi enviada
		. O campo enviado para as vendas � sempre 0
		. As vendas s� aparecem se n�o tiverem sido enviadas, desde que @enviado=0)
		. Se o parametro @enviado=1 aparece sempre tudo
*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_vvm_get]') IS NOT NULL
	drop procedure dbo.up_vvm_get
go

CREATE procedure dbo.up_vvm_get
	@dataIni varchar(8),
	@dataFim varchar(8),
	@site varchar(20),
	@enviado bit

/* with encryption */
AS
	SET NOCOUNT ON


	If OBJECT_ID('tempdb.dbo.#vvm_sales_t') IS NOT NULL
		drop table #vvm_sales_t;
	If OBJECT_ID('tempdb.dbo.#vvm_orders_t') IS NOT NULL
		drop table #vvm_orders_t;

	-- Client data
	declare @infarmed varchar(5), @nome varchar(100), @nif varchar(20)
	select
		@infarmed  = REPLICATE('0', 5-len(infarmed)) + ltrim(str(infarmed))
		,@nome = nomecomp
		,@nif = ncont
	from
		empresa (nolock)
	where
		site = @site
	
	-- Get Sales
	-- levantar primeiro as vendas, para poder depois mapear com encomendas
	select
		token = vvm.id
		,sale = 1
		,enviado = 0
		-- Client
		,client_id = @infarmed
		,client_name = @nome
		,client_nif = @nif
		-- Order
		,distributor_id = bo.ncont
		,distributor_name = bo.nome
		,order_id = RTRIM(LTRIM(STR(bo.obrano))) + "_"  + RTRIM(LTRIM(STR(bo.boano)))
		,prescription_id = vvm.id_receita
		,sentOn = vvm.data
		,status = vvm.estado
		,dateOfDelivery = vvm.datahora_entrega
		,ref = vvmd.ref
		,qt_ordered = 0
		,qt_supplied = 0
		,qt_returned = 0
		,pvp = vvmd.pvp
		,pvf = vvmd.pvf
		,tax = vvmd.iva
		-- Sale
		,qt_sold = fi.qtt
		,soldOn = replace(convert(varchar,ft.fdata,102),'.','-') + 'T' + ft.ousrhora
	into
		#vvm_sales_t
	from
		ft (nolock)
		inner join ft2 (nolock) on ft2.ft2stamp = ft.ftstamp
		inner join via_verde_med vvm (nolock) on vvm.id_receita = ft2.u_receita
		inner join via_verde_med_D vvmd (nolock) on vvm.id = vvmd.id_via_verde_med
		inner join fi (nolock) on ft.ftstamp = fi.ftstamp 
		inner join emb_via_verde evv (nolock) on fi.ref = evv.ref
		inner join bo (nolock) on vvm.bostamp = bo.bostamp
	where
		ft.site = @site
		and ft.fdata between @dataIni and @dataFim
		and ft.ftstamp not in (select id from via_verde_med_s (nolock) where 1 = case when @enviado=0 then 1 else 0 end)
		and fi.fistamp not in (select ofistamp from fi (nolock))
		and vvm.aceite = 1
		and estado != 'Devolucao'
		and bo.site = @site

	--select * from #vvm_sales_t

	-- Get Orders and Returns
	select
		token = vvm.id
		,sale = 0
		,vvm.enviado
		-- Client
		,client_id = @infarmed
		,client_name = @nome
		,client_nif = @nif
		-- Order
		,distributor_id = bo.ncont
		,distributor_name = bo.nome
		,order_id = RTRIM(LTRIM(STR(bo.obrano))) + "_"  +  RTRIM(LTRIM(STR(bo.boano)))
		,prescription_id = vvm.id_receita
		,sentOn = vvm.data
		,status = vvm.estado
		,dateOfDelivery = vvm.datahora_entrega
		,ref = vvmd.ref
		,qt_ordered = vvmd.qt_pedida
		,qt_supplied = vvmd.qt_entregue
		,qt_returned = isnull((select sum(vvmddev.qt_pedida) from via_verde_med_d vvmddev (nolock) inner join via_verde_med vvmdev (nolock) on vvmdev.id = vvmddev.id where vvmdev.id_receita = vvm.id_receita and vvmdev.estado='Devolucao'),0)
		,pvp = vvmd.pvp
		,pvf = vvmd.pvf
		,tax = vvmd.iva
		-- Sale
		,qt_sold = 0
		,soldOn = ''
	into
		#vvm_orders_t
	from
		via_verde_med vvm (nolock)
		inner join via_verde_med_D vvmd (nolock) on vvm.id = vvmd.id_via_verde_med
		inner join emb_via_verde evv (nolock) on vvmd.ref = evv.ref
		inner join bo (nolock) on vvm.bostamp = bo.bostamp
	where
		vvm.aceite = 1
		and (
			(
				convert(varchar,vvm.data,112) between @dataIni and @dataFim
				and 
				vvm.enviado = case when @enviado=0 then 0 else vvm.enviado end
			)
			OR
			vvm.id in (select token from #vvm_sales_t)
		)
		and estado != 'Devolucao'
		and bo.site = @site

	--
	select
		token
		,sale
		,enviado
		-- Client
		,client_id
		,client_name
		,client_nif
		-- Orders
		,distributor_id
		,distributor_name
		,order_id, prescription_id
		,sentOn =  replace(convert(varchar,sentOn,102),'.','-') + 'T' + CONVERT(varchar,sentOn,108)
		,status
		,dateOfDelivery
		,ref
		,qt_ordered = sum(qt_ordered)
		,qt_supplied = SUM(qt_supplied)
		-- Returns
		,qt_returned = SUM(qt_returned)
		,pvp, pvf, tax
		-- Sales
		,qt_sold = SUM(x.qt_sold)
		,soldOn = x.soldOn
	from (
		select * from #vvm_orders_t

		union all

		select * from #vvm_sales_t
	) x
	group by
		token, sale, enviado
		,client_id, client_name, client_nif
		,distributor_id, distributor_name
		,order_id, prescription_id, sentOn, status, dateOfDelivery
		,ref, pvp, pvf, tax
		,soldOn
	order by
		/* N�o alterar a ordena��o, o VVMWebClient.java conta com esta ordena��o */
		x.prescription_id, x.sentOn


	If OBJECT_ID('tempdb.dbo.#vvm_sales_t') IS NOT NULL
		drop table #vvm_sales_t;
	If OBJECT_ID('tempdb.dbo.#vvm_orders_t') IS NOT NULL
		drop table #vvm_orders_t;

Go
Grant Execute on dbo.up_vvm_get to Public
Grant Control on dbo.up_vvm_get to Public
Go
