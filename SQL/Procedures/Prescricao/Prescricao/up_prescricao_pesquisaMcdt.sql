/*
	meios complementares de diagnóstico e terapeutica MCDT
	exec up_prescricao_pesquisaMcdt 300,'','','ENDOSCOPIA GASTRENTEROLÓGICA','','',''
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_pesquisaMcdt]') IS NOT NULL
	drop procedure up_prescricao_pesquisaMcdt
go

create PROCEDURE up_prescricao_pesquisaMcdt

@top int,
@codsns as varchar(10),
@codconv as varchar(10),
@areadescr as varchar(254),
@grupo as varchar(254),
@subgrupo as varchar(254),
@nome as varchar(254)

/* WITH ENCRYPTION */ 

AS

SET NOCOUNT ON

Select	
	top (@top) 
	CONVERT(bit,0) as sel, 
	tiporeceita = 'MCDT'
	,a.codsns
	,a.codconv
	,a.area
	,areadescr = RTRIM(LTRIM(a.area)) + ' - ' + LEFT(a.areadescr,150)
	,a.grupo
	,a.subgrupo
	,a.nomedescr
	,a.pvp
	,a.taxmod
from	
	b_cli_mcdt a (nolock) 
Where	
	a.codsns		= CASE WHEN @codsns = '' THEN a.codsns ELSE @codsns END
	AND a.codconv	= CASE WHEN @codconv = '' THEN a.codconv ELSE @codconv END
	AND a.areadescr = CASE WHEN @areadescr = '' THEN a.areadescr ELSE @areadescr END
	AND a.grupo		LIKE '%' + @grupo + '%'
	AND a.subgrupo	LIKE '%' + @subgrupo + '%'
	AND a.nomedescr LIKE '%' + @nome + '%'

GO
Grant Execute On up_prescricao_pesquisaMcdt to Public
Grant Control On up_prescricao_pesquisaMcdt to Public
GO