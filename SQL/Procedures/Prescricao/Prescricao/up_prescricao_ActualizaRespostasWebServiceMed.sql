/*
 exec up_prescricao_ActualizaRespostasWebServiceMed ''
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_ActualizaRespostasWebServiceMed]') IS NOT NULL
	drop procedure up_prescricao_ActualizaRespostasWebServiceMed
go

create PROCEDURE up_prescricao_ActualizaRespostasWebServiceMed
@prescstamp		varchar(25)

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

	Select 
		top 1 codigo, b.stamp,b.nreceita, b.datarec, b.pin, b.pinOpcao, b.nreceita2, b.pin2, b.pinOpcao2, b.nreceita3, b.pin3, b.pinOpcao3, a.prescstamp
	from 
		B_cli_presc a
		inner join b_cli_resposta b on a.prescstamp = b.PrescStamp
		inner join b_cli_respostaErros c on b.stamp = c.stamp
	Where
		a.prescstamp = @prescstamp
	order by 
		DataEnvio desc

GO
Grant Execute On up_prescricao_ActualizaRespostasWebServiceMed to Public
Grant Control On up_prescricao_ActualizaRespostasWebServiceMed to Public
GO