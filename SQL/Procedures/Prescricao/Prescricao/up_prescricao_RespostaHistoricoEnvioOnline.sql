/*
	exec up_prescricao_RespostaHistoricoEnvioOnline 'js2F0224CE-8034-43F9-A64'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_RespostaHistoricoEnvioOnline]') IS NOT NULL
	drop procedure up_prescricao_RespostaHistoricoEnvioOnline
go

create PROCEDURE up_prescricao_RespostaHistoricoEnvioOnline
@prescstamp		varchar(25)

/* WITH ENCRYPTION */ 
AS
SET NOCOUNT ON


Select 
	dataEnvio, codigo, descricao, segmento 
from 
	b_cli_Resposta 
	inner join b_cli_RespostaErros on b_cli_Resposta.stamp = b_cli_RespostaErros.stamp
where 
	prescstamp = @prescstamp
Order by 
	DataEnvio desc

GO
Grant Execute On up_prescricao_RespostaHistoricoEnvioOnline to Public
Grant Control On up_prescricao_RespostaHistoricoEnvioOnline to Public
GO