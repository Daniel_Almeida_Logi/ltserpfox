/*
	Obtem Medicamentos Equivalentes COM GRUPO HOM
	exec up_prescricao_MedEquivComGH '7381491', 0,'',0.00,0.0
	
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_MedEquivComGH]') IS NOT NULL
	drop procedure up_prescricao_MedEquivComGH
go

create PROCEDURE up_prescricao_MedEquivComGH
@ref	varchar(50),
@pmu	as numeric(10,5),
@filtro as varchar(254),
@putenteactual as numeric(10,5),
@pSNSactual as numeric(10,5)

/* WITH ENCRYPTION */ 
AS

declare @incluir as int
set @incluir = 0

declare @grphmg varchar(30)
set @grphmg = isnull((select top 1 grphmgcode from fprod (nolock) where fprod.cnp=@ref),'xxx')

declare @num_unidades as int
set @num_unidades = ISNULL((select top 1 num_unidades from fprod (nolock) where cnp = @ref),0)


DECLARE @cte2 TABLE (
	cnp VARCHAR(18)
	,design VARCHAR(100)
	,grphmgcode VARCHAR(6)
	,PMU numeric(9,5)
	,ESTADO VARCHAR(60)
	,TAXA_COMPARTICIPACAO numeric(9,5)	
	,PVP numeric(9,5)	
	,PUT numeric(9,5)	
	,UTENTE numeric(9,5)
	,PSNS numeric(9,5)
	,num_unidades int
	);
insert into @cte2	
select	
	fprod.cnp,
	design,
	fprod.grphmgcode,
	ISNULL(u_precouni,0) as PMU, 
	fprod.estaimdescr as ESTADO,
	'TAXA_COMPARTICIPACAO'	= isnull((select pct from cptval(nolock) where cptplacode='01' and grupo=fprod.grupo),0),
	'PVP'					= isnull(pvporig,0),
	'PUT'					= ISNULL(u_precouni,0),
	'UTENTE'				= ROUND(isnull(pvporig,0) - (isnull(pvporig,0)*(isnull((select pct from cptval(nolock) where cptplacode='01' and grupo=fprod.grupo),0)/100)),2),
	'PSNS'					= ROUND(isnull(pvporig,0)*(isnull((select pct from cptval(nolock) where cptplacode='01' and grupo=fprod.grupo),0)/100),2),
	num_unidades
from	
	fprod (nolock)
	inner join fpreco (nolock) on fprod.cnp=fpreco.cnp
where	
	(left(fprod.cnp,1)		!= '1'
	and fprod.grphmgcode	= @grphmg
	and (fprod.sitcomdescr in ('Comerc. conf. pelo Titular','Sem informação do Titular') or @incluir=1 or fprod.sitcomdescr = '')
	and fprod.cnp != @ref)
	and fprod.grphmgcode	!= 'GH0000'	
	and fpreco.grupo		='pvp'
	and fprod.num_unidades between @num_unidades-10 and @num_unidades+10
	and ISNULL(u_precouni,0) < @pmu
	and (fprod.cnp like '%' + @filtro + '%' or design like  '%' + @filtro + '%')	

	
		
select 
	* 
from (
	select
		refori = @ref,
		'sel' = convert(bit,0), 
		'ref' = cnp,
		design,
		grphmgcode,
		prID = dense_rank() over (partition by grphmgcode order by PMU),
		PMU,
		TAXA_COMPARTICIPACAO,
		pvp as epv,
		PUT,
		UTENTE,
		PSNS,
		u_ettent1	= 0.00,
		u_ettent2	= 0.00,
		u_epvp		= 0.00,
		u_epref		= 0.00,
		percentagem = 0.00,
		qtt			= 0,
		estado,
		num_unidades,
		dif_Utente	=  @putenteactual - UTENTE,
		dif_SNS		=  @pSNSactual - PSNS
	from (
		Select * from @cte2
	) x
	where x.pmu is not null	
) y
where y.prid <=4
order by y.prid


GO
Grant Execute On up_prescricao_MedEquivComGH to Public
Grant Control On up_prescricao_MedEquivComGH to Public
GO