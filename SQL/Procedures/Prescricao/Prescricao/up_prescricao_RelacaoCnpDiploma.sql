/*
 exec up_prescricao_RelacaoCnpDiploma
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_RelacaoCnpDiploma]') IS NOT NULL
	drop procedure up_prescricao_RelacaoCnpDiploma
go

create PROCEDURE up_prescricao_RelacaoCnpDiploma

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

	
	select 
		dplms.diploma
		,pdl.cnp
		,u_design
	from
		dplms
		inner join B_patol_dip_lnk pdl on dplms.diploma_id = pdl.diploma_id
	where
		dplms.inactivo=0
	group by 
		dplms.diploma,pdl.cnp,u_design
	

GO
Grant Execute On up_prescricao_RelacaoCnpDiploma to Public
Grant Control On up_prescricao_RelacaoCnpDiploma to Public
GO