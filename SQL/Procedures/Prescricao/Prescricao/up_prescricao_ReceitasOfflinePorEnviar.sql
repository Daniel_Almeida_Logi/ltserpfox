/*
 exec up_prescricao_ReceitasOfflinePorEnviar 			
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_ReceitasOfflinePorEnviar]') IS NOT NULL
	drop procedure up_prescricao_ReceitasOfflinePorEnviar
go

create PROCEDURE up_prescricao_ReceitasOfflinePorEnviar

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

	select 
		enviado, LEFT(NEWID(),25) stampenvio, prescstamp, tiporeceita, sql_db = DB_NAME()
	from 
		b_cli_presc
	Where
		estado = 3 /* offline */
		and enviado != 1
		and prescstamp not in(
			Select 
				prescstamp
			from 
				b_cli_respostaErros e
				inner join B_cli_Resposta r on e.stamp = r.stamp
				and (codigo != '0000000000000000' or codigo != '')
	)
	Order by
		data


GO
Grant Execute On up_prescricao_ReceitasOfflinePorEnviar to Public
Grant Control On up_prescricao_ReceitasOfflinePorEnviar to Public
GO
