/*
	CARREGA DIPLOMAS EM FUN��O DOS PLANOS
	exec up_prescricao_Diplomas '5344189'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_Diplomas]') IS NOT NULL
	drop procedure up_prescricao_Diplomas
go

create PROCEDURE up_prescricao_Diplomas
@ref		varchar(15)

/* WITH ENCRYPTION */ 
AS
SET NOCOUNT ON

	Select
		distinct diploma_ID = LTRIM(STR(dplms.diploma_ID)), diploma, descricao  = u_design 
	from 
		b_patol
		left join b_patol_dip_lnk on b_patol.id = b_patol_dip_lnk.patol_ID
		inner join dplms on b_patol_dip_lnk.diploma_ID = dplms.diploma_ID
	Where
		cnp = @ref
		and disp != 'Farm�cia  Hospitalar'
	order by
		 LTRIM(STR(dplms.diploma_ID)), diploma

GO
Grant Execute On up_prescricao_Diplomas to Public
Grant Control On up_prescricao_Diplomas to Public
GO