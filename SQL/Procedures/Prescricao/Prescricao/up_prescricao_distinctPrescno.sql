/*
 exec up_prescricao_distinctPrescno
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_distinctPrescno]') IS NOT NULL
	drop procedure up_prescricao_distinctPrescno
go

create PROCEDURE up_prescricao_distinctPrescno


/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

	select 
		distinct prescno 
	from 
		b_cli_presc (nolock)
	Where
		prescno != '' 
		and prescno != '0'
	union all
	select 
		distinct prescno2
	from 
		b_cli_presc (nolock) 
	Where
		prescno2 != ''
		and prescno2 != '0'
	union all
	select 
		distinct prescno3
	from 
		b_cli_presc (nolock) 
	Where
		prescno3 != ''
		and prescno3 != '0'
	Order 
		by prescno

GO
Grant Execute On up_prescricao_distinctPrescno to Public
Grant Control On up_prescricao_distinctPrescno to Public
GO
