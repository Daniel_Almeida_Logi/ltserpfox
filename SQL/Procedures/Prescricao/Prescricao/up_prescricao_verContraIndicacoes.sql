/*
 ver Contra-Indicações do Produto com determinadas Patologias (VendasS)
	exec up_prescricao_verContraIndicacoes '5058375,5058359,5058425,5058409,2841989', 'ADM6E68ED90-F3E4-4446-94D'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_verContraIndicacoes]') IS NOT NULL
	drop procedure up_prescricao_verContraIndicacoes
go

create PROCEDURE up_prescricao_verContraIndicacoes
@refs varchar (max),
@utstamp varchar(30)

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

;with
	cte1 as (
		select 
			B_patologias.patol_ID,descricao, B_patologias.ctiefpstamp
		from 
			b_patologias (nolock) 
			inner join b_ctidef (nolock) on B_patologias.ctiefpstamp = B_ctidef.ctiefpstamp 
		where 
			ostamp = @utstamp
	),
	cte2 as (	
		SELECT 
			cnp, moleculasx.descricao, ctidefx.descricao AS patol, ctinvlx.descricao AS nvl, ctiexpx.descricao AS frase, ctiefpstamp
		FROM 
			b_ctifp ctifpx (nolock) 
			INNER JOIN b_ctiexp AS ctiexpx (nolock)ON ctifpx.ctiexpstamp = ctiexpx.ctiexpstamp 
			INNER JOIN b_ctidef AS ctidefx ON ctifpx.ctidefstamp = ctidefx.ctiefpstamp 
			INNER JOIN b_ctinvl AS ctinvlx ON ctifpx.ctinvlstamp = ctinvlx.ctinvlstamp 
			INNER JOIN b_moleculas AS moleculasx ON ctifpx.moleculaID = moleculasx.moleculaID 
			INNER JOIN b_moleculasfp AS moleculasfpx ON ctifpx.moleculaID = moleculasfpx.moleculaID
		where  
			cnp in (select * from dbo.up_splitToTable(@refs,','))
	),
	cte3 as (		
		Select	 
			cte2.descricao
			,patol
			,nvl = nvl
			,frase
		from	
			cte2 
			inner join cte1 on cte1.ctiefpstamp = cte2.ctiefpstamp
	),
	cte4 as (		
		Select	
			distinct 
			descricao
			,patol
			,nvl = nvl
		from 
			cte3
	)
	
	Select 
		*
		,frase = (select Top 1 frase From cte3 as a where cte4.descricao = a.descricao and cte4.patol = a.patol and cte4.nvl = a.nvl)
	From
		cte4
	
	
option (recompile)

GO
Grant Execute On up_prescricao_verContraIndicacoes to Public
Grant Control On up_prescricao_verContraIndicacoes to Public
Go