-- exec up_prescricao_ultimoRegisto 1, 'Loja 1'
if OBJECT_ID('[dbo].[up_prescricao_ultimoRegisto]') IS NOT NULL
	drop procedure up_prescricao_ultimoRegisto
go

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

Create PROCEDURE [dbo].up_prescricao_ultimoRegisto
@drno as numeric(9,2),
@site as varchar(120)


/* WITH ENCRYPTION */ 

AS
	Select 
		Top 1 nrprescricao 
	from 
		b_cli_presc (nolock)
	where 
		 drno = case when @drno = 0 then drno else @drno end
		 and site = case when @site = '' then site else @site end 
	order by 
		ousrdata desc, ousrhora desc

GO
Grant Execute On up_prescricao_ultimoRegisto to Public
Grant Control On up_prescricao_ultimoRegisto to Public
go