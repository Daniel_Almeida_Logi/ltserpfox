/*
 exec up_prescricao_ValidaEspecialista '' 
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_ValidaEspecialista]') IS NOT NULL
	drop procedure up_prescricao_ValidaEspecialista
go

create PROCEDURE up_prescricao_ValidaEspecialista
@usstamp as varchar(25)

/* WITH ENCRYPTION */

AS

	Select drno,drcedula,drinscri,drordem,drclprofi from b_us where usstamp = @usstamp

GO
Grant Execute On up_prescricao_ValidaEspecialista to Public
Grant Control On up_prescricao_ValidaEspecialista to Public
GO
