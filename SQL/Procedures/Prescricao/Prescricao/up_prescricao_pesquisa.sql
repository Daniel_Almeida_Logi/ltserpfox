/*
 Pesquisa de Prescrições
 exec up_prescricao_pesquisa '19000101','20150101','','','','','','',0,0,0,8,0,'Jacinta Santos', '2021000000001345318'
*/
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_pesquisa]') IS NOT NULL
	drop procedure up_prescricao_pesquisa
go

create PROCEDURE up_prescricao_pesquisa
@dataInicio	as datetime,
@dataFim	as datetime,
@nrprescricao	as varchar(254),
@nome		as varchar(254),
@nrutente	as varchar(254),
@nrbenef	as varchar(254),
@drnome		as varchar(254),
@site		as varchar(60),
@renovavel as bit,
@medicamentos as bit,
@mcdt as bit,
@estado as varchar(1),
@userno	numeric(5,0),
@grupo	varchar(20),
@prescno varchar(38)


/* WITH ENCRYPTION */

AS

SET NOCOUNT ON
	
	Select	
		distinct
		sel = convert(bit,0)
		,data = ousrdata
		,dataTexto =  convert(varchar(10),Replace(convert(date,ousrdata),'-','.'))
		,nrprescricao
		,prescno
		,prescno2
		,prescno3
		,nome
		,no
		,nrutente
		,nrbenef
		,drnome
		,site
	From	
		b_cli_presc (nolock)
	WHere	
		ousrdata between @dataInicio and @dataFim
		and	nrprescricao = case when @nrprescricao = '' Then nrprescricao else @nrprescricao end
		and	(
			prescno = case when @prescno = '' then prescno else @prescno end
			Or
			prescno2 = case when @prescno = '' then prescno2 else @prescno end
			Or
			prescno3 = case when @prescno = '' then prescno3 else @prescno end
		)
		and	nome like @nome + '%'
		and nrbenef = case when @nrbenef = '' Then nrbenef else @nrbenef end
		and nrutente = case when @nrutente = '' Then nrutente else @nrutente end
		and	drnome like @drnome + '%'
		and site = CASE WHEN @site = '' THEN site ELSE @site END
		and renovavel = case when @renovavel = 1 then 1 else renovavel end
		and receitamcdt = case when @medicamentos = 1 then 0 else receitamcdt end
		and receitamcdt = case when @mcdt = 1 then 1 else receitamcdt end
		and estado = case when @estado = '' then estado else @estado end
		and drno = case when dbo.up_PerfilFacturacao(@userno, @grupo, 'Permite visualizar receitas outros especialistas',@site) = 1 then drno else @userno end
	
	order by 
		ousrdata desc, 
		nrprescricao desc
			
GO
Grant Execute On up_prescricao_pesquisa to Public
Grant Control On up_prescricao_pesquisa to Public
go