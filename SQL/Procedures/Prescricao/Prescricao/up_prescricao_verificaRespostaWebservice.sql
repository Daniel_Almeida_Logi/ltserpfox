/*
	exec up_prescricao_verificaRespostaWebservice 'jsC4F1CEC9-9EE9-4384-849'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_verificaRespostaWebservice]') IS NOT NULL
	drop procedure up_prescricao_verificaRespostaWebservice
go

create PROCEDURE up_prescricao_verificaRespostaWebservice

@stamp as varchar(60)

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

select 
	codigo
	,descricao 
	,PrescStamp
from 
	b_cli_respostaErros e
	inner join B_cli_Resposta r on e.stamp = r.stamp
Where 
	codigo = '503'
	And PrescStamp = @stamp

GO
Grant Execute On up_prescricao_verificaRespostaWebservice to Public
Grant Control On up_prescricao_verificaRespostaWebservice to Public
GO