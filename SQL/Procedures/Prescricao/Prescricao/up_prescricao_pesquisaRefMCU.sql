/*
Hist�rio de prescri��es de um utente no painel de pesquisa de medicamentos, com medicamentos cr�nicos
exec up_prescricao_pesquisaRefMCU 1000,'','','','','','',0,'',0,0
*/
if OBJECT_ID('[dbo].[up_prescricao_pesquisaRefMCU]') IS NOT NULL
	drop procedure up_prescricao_pesquisaRefMCU
go

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

Create PROCEDURE [dbo].up_prescricao_pesquisaRefMCU
@top int,
@ref				as varchar(254),
@DCI				as varchar(254),
@NOME_COMERCIAL		as varchar(254),
@GRUPO_TERAPEUTICO	as varchar(254),
@DOSAGEM			as varchar(254),
@FORMA_TERAPEUTICA	as varchar(254),
@incluir			as bit,
@utente				as varchar(25),
@cronico			as bit,
@protocolo			as bit


/* WITH ENCRYPTION */

AS

SET NOCOUNT ON	

;with 
	cte1 (grupoterapeutico, cnp) as (
		select atcmini as grupoterapeutico,cnp from B_atc as a
		inner join B_atcfp as b on a.atccode=b.atccode
	),
	cte3 (nome,ref) as (
		Select	nome,ref 
		From	stfami (nolock)
	),
	cte4 (cnp,numero) as (
		SELECT	cnp, count(tipo) as numero 
		FROM	B_moleculasfp (nolock)
		group by cnp
	),
	cte7 (cnp) as (
		select distinct fprod.cnp 
		from b_cli_prescl prescl
		inner join b_cli_presc presc on presc.prescstamp=prescl.prescstamp
		inner join fprod on prescl.ref=fprod.cnp
		where presc.data>DATEADD(YY,-2,getDATE())
				and presc.nrutente=@utente
				and presc.nrutente != ''
				and fprod.tiptratdescr like case when @cronico=1 then 'Longa Dura��o' else '%%' end
	)
	
	Select	top (@top)
			sel						= convert(bit,0), 
			seldci					= convert(bit,0), 
			'REF'					= fprod.cnp,
			'DESCRICAO'				= fprod.descricao,
			'DCI'					= dci,
			'descr'					= RTRIM(LTRIM(Convert(varchar(254),
										CASE WHEN RTRIM(LTRIM(dci)) = '' THEN
											RTRIM(LTRIM(Convert(varchar(254),nome + ' ' + convert(varchar,dosuni) + ' ' + RTRIM(LTRIM(fformasdescr)) +  ' ' + RTRIM(LTRIM(descricao)))))
										ELSE	
											RTRIM(LTRIM(Convert(varchar(254),RTRIM(LTRIM(dci))+', '+ RTRIM(LTRIM(nome)) + ' ' + convert(varchar,dosuni) + ' ' + RTRIM(LTRIM(fformasdescr)) +  ' ' + RTRIM(LTRIM(descricao)))))
										END
										)))
									,						
										
			'NOME_COMERCIAL'		= convert(varchar(254),nome),
			'DOSAGEM'				= convert(varchar(254),dosuni),
			'FORMA_FARMACEUTICA'	= fformasdescr,
			'GRUPO_TERAPEUTICO'		= cte1.grupoterapeutico,
			'DIMENSAO_EMBALAGEM'	= descricao,
			'TAXA_COMPARTICIPACAO'	= isnull((select pct from cptval (nolock) where cptplacode='01' and grupo=fprod.grupo),0),
			'PVP'					= isnull(pvporig,0),
			'PUT'					= ISNULL(fpreco.u_precouni,0),
			'UTENTE'				= ROUND(isnull(pvporig,0) - (isnull(pvporig,0)*(isnull((select pct from cptval (nolock) where cptplacode='01' and grupo=fprod.grupo),0)/100)),2),
			'PSNS'					= ROUND(isnull(pvporig,0)*(isnull((select pct from cptval (nolock) where cptplacode='01' and grupo=fprod.grupo),0)/100),2),
			'codGrupoH'				= grphmgcode,
			'designGrupoH'			= grphmgdescr,
			'fabricanteno'			= titaim,
			'fabricantenome'		= titaimdescr,
			'familia'				= u_familia, 
			'faminome'				= isnull((Select Top 1 nome from cte3 Where cte3.ref = u_familia),''),
			generico, 
			psico, 
			benzo,
			manipulado,
			protocolo,
			tipembdescr,
			'estadocomercial'		= sitcomdescr,
			'dataEstadoComercial'	= sitcomdata,
			u_posprog  as posologia,
			'ESTADO'				= estaimdescr,
			'grupo'					= fprod.grupo,
			'PMU'					= ISNULL(u_precouni,0), 
			num_unidades,
			fprod.tiptratdescr as tipoduracao,
			dci as ListagemDci
			,lista_dcis		= fprod.dci
			,TipoReceita	= case 
								when fprod.grupo = 'DI' then 'MDT'
								when fprod.protocolo = 1 then 'MDB'
								when fprod.psico = 1 then 'RE'
								when fprod.grupo = 'MN' then 'MM'
								when fprod.medid != 0 then 'RN'
								else 'OUT'
							End
			,medid
			,posologiaSugerida = isnull(posologia.posologia_orientativa,'') 
			,posologiaOriginal = isnull(posologia.posologia_orientativa,'')
			,prescNomeCod = CONVERT(varchar(2),'')
			,prescNomeDescr = CONVERT(varchar(254),'')
			,fprod.cnpem
			,fprod.marg_terap
			,fprod.u_nomerc
	From	
		fprod (nolock)
		left join fpreco on fprod.cnp = fpreco.cnp and fpreco.grupo = 'pvp'
		left join cte1 on cte1.cnp = fprod.cnp
		left join posologia (nolock) on posologia.id_med = fprod.medid
	WHERE  	
		fprod.cnp in (select cnp from cte7)
		and (fprod.u_nomerc = 1 or @incluir=1)
		and protocolo = case when @protocolo = 1 then 1 else protocolo end
	ORDER BY
		case when ISNULL(u_precouni,0)=0 then 9999999 else ISNULL(u_precouni,0) end
		,case when  isnull(pvporig,0)=0 then 9999999 else  isnull(pvporig,0) end
		,nome  asc
			

GO
Grant Execute On up_prescricao_pesquisaRefMCU to Public
Grant Control On up_prescricao_pesquisaRefMCU to Public
go