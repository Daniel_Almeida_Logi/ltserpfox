/*
 Obter dados do Utente Web Service
 exec up_prescricao_dados 'ADM001'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_dados]') IS NOT NULL
	drop procedure up_prescricao_dados
go

create PROCEDURE up_prescricao_dados

@stamp as varchar(60)

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON


	Select	
		stamp
		,dataintro
		,Apelidos = Replace(Apelidos, Char(39),'')
		,DataNascimento
		,Duplicado
		,NomeCompleto = Replace(NomeCompleto, Char(39),'')
		,NomesProprios = Replace(NomesProprios, Char(39),'')
		,NumeroSNS
		,Obito
		,PaisNacionalidade
		,Sexo
		,BenefRECMMotivo
		,BenefRECMDescricao
		,BenefRECMDataInicio
		,BenefRECMDataFim
		,BenefITMMotivo
		,BenefITDescricao
		,BenefITDataInicio
		,BenefITDataFim
		,mensagemCodigo
		,mensagemDescricao
		,xmlEnviado = Replace(xmlEnviado, Char(39),'')
		,xmlResposta = Replace(xmlResposta, Char(39),'')
	from	
		b_cli_dados (nolock)
	Where	
		stamp = @stamp

GO
Grant Execute On up_prescricao_dados to Public
Grant Control On up_prescricao_dados to Public
GO