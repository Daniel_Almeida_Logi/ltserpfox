/*
 exec up_prescricao_CPM_Reposta ''
 			
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_CPM_Reposta]') IS NOT NULL
	drop procedure up_prescricao_CPM_Reposta
go

create PROCEDURE up_prescricao_CPM_Reposta
@pedidostamp as varchar(25)

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

	Select 
		*
	from 
		b_cli_CPM
	Where
		stamp = @pedidostamp
	
GO
Grant Execute On up_prescricao_CPM_Reposta to Public
Grant Control On up_prescricao_CPM_Reposta to Public
GO