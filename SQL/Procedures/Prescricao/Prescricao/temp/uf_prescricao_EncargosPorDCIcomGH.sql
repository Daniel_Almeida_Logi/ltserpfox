/*
	Prescri��o por DCI com grupo homogeneo

	exec uf_prescricao_EncargosPorDCIcomGH 50002392,'GH0728' 

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[uf_prescricao_EncargosPorDCIcomGH]') IS NOT NULL
	drop procedure uf_prescricao_EncargosPorDCIcomGH
go

create PROCEDURE uf_prescricao_EncargosPorDCIcomGH
	@cnpem	varchar(8),
	@grphmgcode varchar(6)

/* WITH ENCRYPTION */ 
AS

SET NOCOUNT ON


/*	
	Calculo encargos - RG e RE

	Prescri��o por DCI com grupo homogeneo

	"Esta prescri��o custa-lhe, no m�ximo, � nn,nn, a n�o ser que opte por um medicamento mais caro. "

	Deve ser identificado o encargo para o utente com base no PVPMAX100%RE - Tipo_preco_ID=301, 
	do CNPEM a que o medicamento pertence. Sendo que:
		. No caso de existirem dois valores distintos do campos PVPMAX100RE dever� ser utilizado
		o valor mais elevado para calculo de encargos.
		. Se todos os PVPs dos medicamentos do CNPEM forem superiores ao valor de PVPMAX100%RE,
		o c�lculo do encargo deve ser efetuado com o valor de PVP mais baixo (Tipo_preco_ID=1) do CNPEM
*/					

	If OBJECT_ID('tempdb.dbo.#dados_fprod_t') IS NOT NULL
		drop table #dados_fprod_t;

	Select
		cnp
		,cnpem
		,grphmgcode
		,grphmgdescr
		,pref = case when pref=0 then pvporig else pref end
		,comp_sns
		,comp_snsRe = case
						when comp_sns > 0 and pvporig <= pvpmaxre then 95
						else case
								when comp_sns = 90 then 95 /* Escalao A */
								when comp_sns = 69 then 84 /* Escalao B */
								when comp_sns = 37 then 52 /* Escalao C */
								when comp_sns = 15 then 30 /* Escalao D */
								else comp_sns
								end
						end
		,pvporig
		,pvpmaxre
		,pvpcalc
	into
		#dados_fprod_t
	From
		fprod (nolock)
	where
		pvpmaxre != 0
		and cnpem = @cnpem
		and grphmgcode = @grphmgcode
		and u_nomerc = 1


	-- select round(pvpmaxre - (pref * comp_snsRe / 100), 2),* from #dados_fprod_t t order by t.pvpmaxre

	/*
		Encontrar a ref e o preco para o calculo de encargos
	*/
	declare @refCalculo varchar(18), @precoCalculo numeric(13,3)
	
	if ((select min(pvporig) from #dados_fprod_t) > (select max(pvpmaxre) from #dados_fprod_t))
	begin
		-- pvp mais baixo
		select top 1
			 @refCalculo = cnp
			,@precoCalculo = pvporig
		from
			#dados_fprod_t
		order by
			pvporig

	end else begin
		--- pvpmaxre mais alto
		select top 1
			 @refCalculo = cnp
			,@precoCalculo = pvpmaxre
		from
			#dados_fprod_t
		where
			pvpmaxre>=pvporig -- se temos produtos com um pvpmaxre, n�o demos considerar produtos com o pvporig > pvpmaxre
		order by
			pvpmaxre desc, pvporig desc
	end


	Select
		cnp
		,cnpem
		,grphmgcode
		,grphmgdescr
		,comp_sns
		,comp_snsRe

		,encSnsRg = case
						when @precoCalculo > (pref * comp_sns / 100)
							then round(pref * comp_sns / 100, 2)
						else @precoCalculo
						end
		,encUtRg = case
						when @precoCalculo > (pref * comp_sns / 100)
							then round(@precoCalculo - (pref * comp_sns / 100), 2)
						else 0
						end

		,encSnsRe = case
						when @precoCalculo > (pref * comp_snsRe / 100)
							then round(pref * comp_snsRe / 100, 2)
						else @precoCalculo
						end
		,encUtRe = case
						when @precoCalculo > (pref * comp_snsRe / 100)
							then round(@precoCalculo - (pref * comp_snsRe / 100), 2)
						else 0
						end

		-- TODO: validar este campo
		,encSNSCnrp = (select min(pvporig) from #dados_fprod_t)
	From
		#dados_fprod_t
	where
		cnp = @refCalculo


	If OBJECT_ID('tempdb.dbo.#dados_fprod_t') IS NOT NULL
		drop table #dados_fprod_t;

GO
Grant Execute On uf_prescricao_EncargosPorDCIcomGH to Public
Grant Control On uf_prescricao_EncargosPorDCIcomGH to Public
GO