/* 
	Prescricao por nome sem justifica��o Tecnica

	exec uf_prescricao_EncargosSJustTecnica 5440987

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[uf_prescricao_EncargosSJustTecnica]') IS NOT NULL
	drop procedure uf_prescricao_EncargosSJustTecnica
go

create PROCEDURE uf_prescricao_EncargosSJustTecnica
	@cnp	varchar(18)

/* WITH ENCRYPTION */ 
AS

SET NOCOUNT ON


/*
	Calculo encargos - RG e RE

	Prescri��o sem justifica��o T�cnica CNP - Outros casos

	"Este medicamento custa-lhe, no m�ximo, � nn,nn."

	 Deve ser identificado o encargo para o utente, com base no pre�o do medicamento.
	 O software deve calcular os encargos para o utente utilizando:
		. se o medicamento for comparticipado - Tipo_preco_ID=501 (Lt. pvpcalc)
		. se o medicamento n�o for comparticipado - Tipo_precoID = 1 (Lt. pvporig)
*/					

	If OBJECT_ID('tempdb.dbo.#dados_fprod_t') IS NOT NULL
		drop table #dados_fprod_t;		

	Select
		cnp
		,cnpem
		,grphmgcode
		,grphmgdescr
		,pref = case when pref=0 then pvporig else pref end
		,comp_sns
		,comp_snsRe = case
						when comp_sns > 0 and pvporig <= pvpmaxre then 95
						else case
								when comp_sns = 90 then 95 /* Escalao A */
								when comp_sns = 69 then 84 /* Escalao B */
								when comp_sns = 37 then 52 /* Escalao C */
								when comp_sns = 15 then 30 /* Escalao D */
								else comp_sns
								end
						end
		,pvporig
		,pvpmaxre
		,pvpcalc
	into
		#dados_fprod_t
	From
		fprod
	where
		pvporig != 0
		and cnp = @cnp


	Select
		cnp
		,cnpem
		,grphmgcode
		,grphmgdescr
		,comp_sns
		,comp_snsRe

		,encSnsRg = case
						when pvporig > (pref * comp_sns / 100)
							then round(pref * comp_sns / 100, 2)
						else pvporig
						end
		,encUtRg = case
						when pvporig > (pref * comp_sns / 100)
							then round(pvporig - (pref * comp_sns / 100), 2)
						else 0
						end

		,encSnsRe = case
						when pvporig > (pref * comp_snsRe / 100)
							then round(pref * comp_snsRe / 100, 2)
						else pvporig
						end
		,encUtRe = case
						when pvporig > (pref * comp_snsRe / 100)
							then round(pvporig - (pref * comp_snsRe / 100), 2)
						else 0
						end

		,pref
		,pvporig
		,pvpmaxre
		,pvpcalc
	From 
		#dados_fprod_t

	If OBJECT_ID('tempdb.dbo.#dados_fprod_t') IS NOT NULL
		drop table #dados_fprod_t;	

GO
Grant Execute On uf_prescricao_EncargosSJustTecnica to Public
Grant Control On uf_prescricao_EncargosSJustTecnica to Public
GO