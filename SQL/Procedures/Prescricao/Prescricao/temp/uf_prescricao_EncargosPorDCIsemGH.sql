/* 
	Prescri��o por DCI sem grupo homogeneo

	exec uf_prescricao_EncargosPorDCIsemGH 50121065

	select pvporig,pref,pvpmaxre,pvpcalc,pvpmax,grphmgcode,grphmgdescr,* from fprod where cnpem='50121065' and u_nomerc=1 and (grphmgcode = '' or grphmgcode = 'GH0000')

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[uf_prescricao_EncargosPorDCIsemGH]') IS NOT NULL
	drop procedure uf_prescricao_EncargosPorDCIsemGH
go

create PROCEDURE uf_prescricao_EncargosPorDCIsemGH
	@cnpem	varchar(8)

/* WITH ENCRYPTION */ 
AS

SET NOCOUNT ON

/*
	Calculo encargos - RG e RE

	Prescri��o por DCI sem grupo homogeneo 

	"Esta prescri��o custa-lhe, no m�ximo, � nn,nn, a n�o ser que opte por um medicamento mais caro. "

	Deve ser identificado o encargo para o utente, com base no pre�o mais baixo do medicamento similar
	O pre�o mais baixo de um medicamento similar deve ser selecionado com base no PVP:
	. se o medicamento for comparticipado - Tipo_preco_ID=501 (Lt. pvpcalc)
	. se o medicamento n�o for comparticipado - Tipo_precoID = 1 (Lt. pvporig)

*/

	If OBJECT_ID('tempdb.dbo.#dados_fprod_t') IS NOT NULL
		drop table #dados_fprod_t;		

	Select
		cnp
		,cnpem
		,grphmgcode
		,grphmgdescr
		,pref = case when pref=0 then pvporig else pref end
		,comp_sns
		,comp_snsRe = case
						when comp_sns > 0 and pvporig <= pvpmaxre then 95
						else case
								when comp_sns = 90 then 95 /* Escalao A */
								when comp_sns = 69 then 84 /* Escalao B */
								when comp_sns = 37 then 52 /* Escalao C */
								when comp_sns = 15 then 30 /* Escalao D */
								else comp_sns
								end
						end
		,pvporig
		,pvpmaxre
		,pvpcalc
	into
		#dados_fprod_t
	From
		fprod
	where
		pvporig != 0
		and (grphmgcode = '' or grphmgcode = 'GH0000')
		and cnpem = @cnpem
		and u_nomerc = 1

	/*
		Encontrar a ref o preco para o calculo de encargos
	*/
	declare @refCalculo varchar(18)
	
	set @refCalculo = (select top 1 cnp from #dados_fprod_t order by pvporig) -- pvp mais baixo

	
	Select
		cnp
		,cnpem
		,grphmgcode
		,grphmgdescr
		,comp_sns
		,comp_snsRe

		,encSnsRg = case
						when pvporig > (pvporig * comp_sns / 100)
							then round(pvporig * comp_sns / 100, 2)
						else pvporig
						end
		,encUtRg = case
						when pvporig > (pvporig * comp_sns / 100)
							then round(pvporig - (pvporig * comp_sns / 100), 2)
						else 0
						end

		,encSnsRe = case
						when pvporig > (pvporig * comp_snsRe / 100)
							then round(pvporig * comp_snsRe / 100, 2)
						else pvporig
						end
		,encUtRe = case
						when pvporig > (pvporig * comp_snsRe / 100)
							then round(pvporig - (pvporig * comp_snsRe / 100), 2)
						else 0
						end

	From
		#dados_fprod_t
	where
		cnp = @refCalculo


	If OBJECT_ID('tempdb.dbo.#dados_fprod_t') IS NOT NULL
		drop table #dados_fprod_t;	

GO
Grant Execute On uf_prescricao_EncargosPorDCIsemGH to Public
Grant Control On uf_prescricao_EncargosPorDCIsemGH to Public
GO