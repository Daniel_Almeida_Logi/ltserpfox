/*
 exec up_prescricao_CPM_Ordem 
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_CPM_Ordem]') IS NOT NULL
	drop procedure up_prescricao_CPM_Ordem
go

create PROCEDURE up_prescricao_CPM_Ordem


/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

	Select cod = '05', descricao = 'Ordem dos M�dicos'
	union all
	Select '06', 'Ordem dos M�dicos Dentistas'
	union all
	Select  '07', 'Odontologistas' 

GO
Grant Execute On up_prescricao_CPM_Ordem to Public
Grant Control On up_prescricao_CPM_Ordem to Public
GO
