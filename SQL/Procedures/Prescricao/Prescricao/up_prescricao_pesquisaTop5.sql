/*
 Pesquisa de Prescrições
 exec up_prescricao_pesquisaTop5 1,''
 */
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_pesquisaTop5]') IS NOT NULL
	drop procedure up_prescricao_pesquisaTop5
go

create PROCEDURE up_prescricao_pesquisaTop5
@userno	numeric(5,0),
@grupo	varchar(20)


/* WITH ENCRYPTION */

AS

SET NOCOUNT ON
	
	declare @site as varchar(20)
	set @site = (Select top 1 loja from b_us where userno = @userno)
	
	Select	
		distinct
		Top 10 
		sel = convert(bit,0)
		,data = ousrdata
		,dataTexto =  convert(varchar(10),Replace(convert(date,ousrdata),'-','.'))
		,nrprescricao
		,prescno
		,prescno2
		,prescno3
		,nome
		,no
		,nrutente
		,nrbenef
		,drnome
		,site
	From	
		b_cli_presc (nolock)
	WHere	
		drno = case when dbo.up_PerfilFacturacao(@userno, @grupo, 'Permite visualizar receitas outros especialistas',@site) = 1 then drno else @userno end
	order by 
		ousrdata desc, 
		nrprescricao desc
			
GO
Grant Execute On up_prescricao_pesquisaTop5 to Public
Grant Control On up_prescricao_pesquisaTop5 to Public
go