/*
	Obtem Medicamentos Equivalentes SEM GRUPO HOM
	exec up_prescricao_MedEquivSemGHTop1 '8168617',  0.1540,'',0.84,0.50
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_MedEquivSemGHTop1]') IS NOT NULL
	drop procedure up_prescricao_MedEquivSemGHTop1
go

create PROCEDURE up_prescricao_MedEquivSemGHTop1
@ref	varchar(50),
@pmu	as numeric(10,5),
@filtro as varchar(254),
@putenteactual as numeric(10,5),
@pSNSactual as numeric(10,5)

/* WITH ENCRYPTION */ 
AS

SET NOCOUNT ON
	
declare @incluir as int
set @incluir = 0
	
declare @grphmg varchar(30)
set @grphmg = isnull((select top 1 grphmgcode from fprod (nolock) where fprod.cnp=@ref),'xxx')

declare @num_unidades as int
set @num_unidades = ISNULL((select top 1 num_unidades from fprod (nolock) where cnp = @ref),0)

declare @dci as varchar(254)
Set @dci = ( SELECT	top 1 DCI from	fprod (nolock) Where cnp = @ref)
	
;with 
	cte1 (dosuni, fformasdescr, vias_admin) as (
		select	
			top 1  CONVERT(varchar(254),dosuni), CONVERT(varchar(254),fformasdescr), vias_admin
		from	
			fprod (nolock)
		Where	
			cnp = @ref
	)	

select 
	top 1 * 
from (
	select
		refori = @ref,
		'sel' = convert(bit,0), 
		'ref' = ref,
		design,
		grphmgcode,
		prID = dense_rank() over (partition by grphmgcode order by PMU),
		PMU,
		TAXA_COMPARTICIPACAO,
		pvp as epv,
		PUT,
		UTENTE,
		PSNS,
		u_ettent1	= 0.00,
		u_ettent2	= 0.00,
		u_epvp		= 0.00,
		u_epref		= 0.00,
		percentagem = 0.00,
		qtt			= 0,
		estado,
		num_unidades,
		dif_Utente	=  @putenteactual - UTENTE,
		dif_SNS		=  @pSNSactual - PSNS
	from (
		select
			ref = fprod.cnp,
			design,
			fprod.grphmgcode,
			ISNULL(u_precouni,0) as PMU, 
			fprod.estaimdescr as ESTADO,
			'TAXA_COMPARTICIPACAO'	= isnull((select pct from cptval(nolock) where cptplacode='01' and grupo=fprod.grupo),0),
			'PVP'					= isnull(pvporig,0),
			'PUT'					= ISNULL(u_precouni,0),
			'UTENTE'				= ROUND(isnull(pvporig,0) - (isnull(pvporig,0)*(isnull((select pct from cptval(nolock) where cptplacode='01' and grupo=fprod.grupo),0)/100)),2),
			'PSNS'					= ROUND(isnull(pvporig,0)*(isnull((select pct from cptval (nolock)where cptplacode='01' and grupo=fprod.grupo),0)/100),2),
			num_unidades
					
		from 
			fprod (nolock)
			inner join fpreco (nolock) on fprod.cnp=fpreco.cnp
		where	
			left(fprod.cnp,1) !=1 /* excluir unidose */
			and (fprod.u_nomerc = 1 or @incluir=1)
			and fprod.cnp != @ref
			and fprod.grphmgcode	= 'GH0000'
			/* Mesmo Conjunto de DCI */
			and dci = @dci
			/* diferenša maxima de 10 unidades */	
			and num_unidades between @num_unidades-10 and @num_unidades+10
			/* Mesma Dosagem */
			and CONVERT(varchar(254),dosuni) = (SELECT top 1 dosuni from cte1)
			/* Mesma Forma farmaceutica */
			and CONVERT(varchar(254),fformasdescr) = (SELECT top 1 fformasdescr from cte1)
			/* Mesma Via Administracao */
			and fprod.vias_admin = (SELECT top 1 vias_admin from cte1)
			/**/
			and ISNULL(u_precouni,99999) < @pmu
	) x
		where x.pmu is not null	
) y
where y.prid <=4


GO
Grant Execute On up_prescricao_MedEquivSemGHTop1 to Public
Grant Control On up_prescricao_MedEquivSemGHTop1 to Public
GO