/*
	meios complementares de diagnůstico e terapeutica MCDT
	exec up_prescricao_mcdtMenos90 203			
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_mcdtMenos90]') IS NOT NULL
	drop procedure up_prescricao_mcdtMenos90
go

create PROCEDURE up_prescricao_mcdtMenos90


@no int

/* WITH ENCRYPTION */ 

AS

SET NOCOUNT ON

Select	
	distinct 
	b_cli_presc.prescno, 
	b_cli_presc.data, 
	LEFT(mcdt_area + '-' + mcdt_areadescr,245) as area, 
	mcdt_grupo as grupo,
	mcdt_subgrupo as subgrupo, 
	mcdt_nomedescr as nomedescr,
	mcdt_sinonimo as sinonimo, 
	DATEDIFF(dd,b_cli_presc.data,getdate()) as idade,
	isnull(diasaviso,0)
from	
	b_cli_presc (nolock)
	inner join b_cli_prescl (nolock) On b_cli_presc.prescstamp = b_cli_prescl.prescstamp
	left join b_cli_mcdt (nolock) on b_cli_mcdt.codconv = b_cli_prescl.mcdt_codconv
Where	
	b_cli_presc.receitamcdt = 1 
	and (b_cli_prescl.mcdt_codconv != '' or b_cli_prescl.mcdt_codsns != '')
	and DATEDIFF(dd,b_cli_presc.data,getdate()) < isnull(diasaviso,0) and no = @no
group by 
	b_cli_presc.prescno, b_cli_presc.data, mcdt_area
	,mcdt_areadescr, mcdt_grupo
	,mcdt_subgrupo, mcdt_nomedescr
	,mcdt_sinonimo, diasaviso
ORDER by
	b_cli_presc.data desc



GO
Grant Execute On up_prescricao_mcdtMenos90 to Public
Grant Control On up_prescricao_mcdtMenos90 to Public
GO