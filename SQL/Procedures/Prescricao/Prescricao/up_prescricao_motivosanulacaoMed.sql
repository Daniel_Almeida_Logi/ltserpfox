/*
	CARREGA MOTIVOS DE ANULACAO
	exec up_prescricao_motivosanulacaoMed
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_motivosanulacaoMed]') IS NOT NULL
	drop procedure up_prescricao_motivosanulacaoMed
go

create PROCEDURE up_prescricao_motivosanulacaoMed

/* WITH ENCRYPTION */ 
AS

SET NOCOUNT ON

	SELECT codigo,descricao, GETDATE() as dataanula FROM b_cli_motivoAnulacao (nolock) order by codigo

GO
Grant Execute On up_prescricao_motivosanulacaoMed to Public
Grant Control On up_prescricao_motivosanulacaoMed to Public
GO
