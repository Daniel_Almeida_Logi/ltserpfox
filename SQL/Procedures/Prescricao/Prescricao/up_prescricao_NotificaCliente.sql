/*
 TAG DE PRODUTO
 EXEC up_prescricao_notificaCliente
 */
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_prescricao_notificaCliente]') IS NOT NULL
	DROP PROCEDURE up_prescricao_notificaCliente
GO

CREATE PROCEDURE up_prescricao_notificaCliente

/* WITH ENCRYPTION */ 
AS

	SET NOCOUNT ON

	
	SELECT	
		codigo
		,descricao 
	FROM	
		b_cli_notificaCliente (nolock)
	ORDER BY 
		codigo
	
	
GO

GRANT EXECUTE ON up_prescricao_notificaCliente TO PUBLIC
GRANT CONTROL ON up_prescricao_notificaCliente TO PUBLIC
GO
