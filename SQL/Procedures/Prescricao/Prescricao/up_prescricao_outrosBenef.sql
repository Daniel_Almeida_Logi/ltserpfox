/*
 Obter dados do Utente Web Service Outros Beneficios
 exec up_prescricao_outrosBenef ''
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_outrosBenef]') IS NOT NULL
	drop procedure up_prescricao_outrosBenef
go

create PROCEDURE up_prescricao_outrosBenef

@stamp as varchar(60)

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

Select	
	*
from	
	b_cli_outrosBenef (nolock)
Where	
	stamp = @stamp


GO
Grant Execute On up_prescricao_outrosBenef to Public
Grant Control On up_prescricao_outrosBenef to Public
GO