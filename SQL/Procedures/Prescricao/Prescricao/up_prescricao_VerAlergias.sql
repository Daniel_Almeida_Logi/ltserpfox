/*
 Ver as Alergias dos produtos da receita
 exec up_prescricao_VerAlergias '8168617,2000784'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_VerAlergias]') IS NOT NULL
	drop procedure up_prescricao_VerAlergias  
go

create PROCEDURE up_prescricao_VerAlergias 
@ref varchar(max)

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

SELECT 
	alrgx.alergia
FROM 
	b_alrg alrgx (nolock)
	INNER JOIN b_alrgfp alrgfpx (nolock) ON alrgx.alrgstamp = alrgfpx.alrgstamp
	INNER JOIN b_moleculas moleculasx (nolock) ON alrgfpx.moleculaID = moleculasx.moleculaID
	INNER JOIN b_moleculasfp moleculasfpx (nolock) ON alrgfpx.moleculaID = moleculasfpx.moleculaID
where 
	moleculasfpx.cnp in (select * from dbo.up_splitToTable(@ref,','))
Order 
	by alrgx.alergia

GO
Grant Execute On up_prescricao_VerAlergias  to Public
Grant Control On up_prescricao_VerAlergias  to Public
GO