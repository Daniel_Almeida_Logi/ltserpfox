/*
 exec up_prescricao_estados
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_estados]') IS NOT NULL
	drop procedure up_prescricao_estados
go

create PROCEDURE up_prescricao_estados

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

Select estado = 0 , estadodescr = 'Receita n�o submetida.'
union all
Select estado = 1 , estadodescr = 'Receita enviada e validada pelo processo Online.'
union all
Select estado = 2 , estadodescr = 'Receita n�o validada pelo processo Online.'
union all
Select estado = 3 , estadodescr = 'Receita Offline.'

GO
Grant Execute On up_prescricao_estados to Public
Grant Control On up_prescricao_estados to Public
GO