/*
 exec up_prescricao_CPM_Diplomas '20130411174045454-32-4'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_CPM_Diplomas]') IS NOT NULL
	drop procedure up_prescricao_CPM_Diplomas
go

create PROCEDURE up_prescricao_CPM_Diplomas
@stampLin as varchar(25)

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

	Select 
		codigo
		, diploma = CONVERT(varchar(254),diploma)
	from 
		b_cli_CPM_diplomas
	Where
		stampLinha = @stampLin
	order by
		codigo, diploma


GO
Grant Execute On up_prescricao_CPM_Diplomas to Public
Grant Control On up_prescricao_CPM_Diplomas to Public
GO
