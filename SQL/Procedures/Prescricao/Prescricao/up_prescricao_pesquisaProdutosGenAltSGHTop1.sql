/*
 ver Gen�ricos
 exec up_prescricao_pesquisaProdutosGenAltSGHTop1 '5389531'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_pesquisaProdutosGenAltSGHTop1]') IS NOT NULL
	drop procedure up_prescricao_pesquisaProdutosGenAltSGHTop1
go

create PROCEDURE up_prescricao_pesquisaProdutosGenAltSGHTop1

@ref varchar(60)

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON
	declare @dci as varchar(254)
	Set @dci = (SELECT	top 1 DCI from fprod (nolock) Where	cnp = @ref)
	declare @incluir as int
	set @incluir = 0
	
	;with 
	cte1 (dosuni, fformasdescr, vias_admin) as (
		select	
			top 1  CONVERT(varchar(254),dosuni), CONVERT(varchar(254),fformasdescr), vias_admin
		from	
			fprod (nolock)
		Where	
			cnp = @ref
	)
	
	select	
		top 1   
		fprod.cnp as ref
	from 
		fprod (nolock)
		left join fpreco (nolock) on fprod.cnp=fpreco.cnp and fpreco.grupo='pvp'
		left join cptgrp (nolock) on cptgrp.grupo=fprod.grupo
	where	
		left(fprod.cnp,1)		!=1 /* excluir unidose */
		and (fprod.sitcomdescr in ('Comerc. conf. pelo Titular','Sem informa��o do Titular') or @incluir=1 or fprod.sitcomdescr = '')
		and fprod.cnp != @ref
		and fprod.grphmgcode	= 'GH0000'
		/* Mesmo Conjunto de DCI */
		and dci = @dci
		/* Mesma Dosagem */
		and CONVERT(varchar(254),dosuni) = (SELECT top 1 dosuni from cte1)
		/* Mesma Forma farmaceutica */
		and CONVERT(varchar(254),fformasdescr) = (SELECT top 1 fformasdescr from cte1)
		/* Mesma Via Administracao */
		and fprod.vias_admin = (SELECT top 1 vias_admin from cte1)
		/* Generico */
		and generico = 1
GO
Grant Execute On up_prescricao_pesquisaProdutosGenAltSGHTop1 to Public
Grant Control On up_prescricao_pesquisaProdutosGenAltSGHTop1 to Public
Go	