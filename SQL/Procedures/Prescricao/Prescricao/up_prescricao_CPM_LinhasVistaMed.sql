/*
 exec up_prescricao_CPM_LinhasVistaMed 'js28A8FB8E-BE2C-4CF2-BB6'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_CPM_LinhasVistaMed]') IS NOT NULL
	drop procedure up_prescricao_CPM_LinhasVistaMed
go

create PROCEDURE up_prescricao_CPM_LinhasVistaMed
@pedidostamp as varchar(25)

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

	Select 
		data = convert(varchar(10),Replace(convert(date,data),'-','.'))
		,descricao = CONVERT(varchar(254), descricao)
		,qtt = quantidade
		,posologia = CONVERT(varchar(254), posologia)
		,excecao
		,cnpem
		,cnp =  numRegisto
		,codigosgreve = ''
		,stampLinha
		,lpDesignacao = left(lpDesignacao,245)
		,anulado = case when anulado = 'S' then 'SIM' ELSE 'N�O' end
		,renovavel = case when renovavel = 'S' then 'SIM' ELSE 'N�O' end
		,numReceita
		,pedidostamp
		,*
	from 
		b_cli_CPM_cabecalho a
		inner join b_cli_CPM_linhaReceita b on a.stamp = b.stampCab
	Where
		pedidostamp = @pedidostamp
	order by 
		a.data desc



GO
Grant Execute On up_prescricao_CPM_LinhasVistaMed to Public
Grant Control On up_prescricao_CPM_LinhasVistaMed to Public
GO
