/*
 exec up_prescricao_CPM_LocaisPresc
 			
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_CPM_LocaisPresc]') IS NOT NULL
	drop procedure up_prescricao_CPM_LocaisPresc
go

create PROCEDURE up_prescricao_CPM_LocaisPresc


/* WITH ENCRYPTION */

AS


	Select 
		cod = LTRIM(STR(COD_LOCAL)),
		descricao = DESIGNACAO
	from 
		b_cli_locais
	Order by
		DESIGNACAO
GO
Grant Execute On up_prescricao_CPM_LocaisPresc to Public
Grant Control On up_prescricao_CPM_LocaisPresc to Public
GO
