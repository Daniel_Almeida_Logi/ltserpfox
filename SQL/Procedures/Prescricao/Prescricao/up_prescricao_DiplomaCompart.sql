/* calc de encargo p utente com base no diploma
	
	 exec up_prescricao_DiplomaCompart 55, 100
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_DiplomaCompart]') IS NOT NULL
	drop procedure up_prescricao_DiplomaCompart
go

create PROCEDURE up_prescricao_DiplomaCompart
@diploma_ID	float,
@encargo as  numeric(9,2)


/* WITH ENCRYPTION */ 
AS
	SET NOCOUNT ON
	
		Select
			Top 1 
			compart,
			encargoUt = @encargo - (@encargo * (compart/100))
		from 
			b_patol
			left join b_patol_dip_lnk on b_patol.id = b_patol_dip_lnk.patol_ID
			inner join dplms on b_patol_dip_lnk.diploma_ID = dplms.diploma_ID
		Where
			dplms.diploma_ID = @diploma_ID
		Order by
			compart desc

GO
Grant Execute On up_prescricao_DiplomaCompart to Public
Grant Control On up_prescricao_DiplomaCompart to Public
GO