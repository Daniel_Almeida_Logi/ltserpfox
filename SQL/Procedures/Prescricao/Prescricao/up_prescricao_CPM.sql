/*
 exec up_prescricao_CPM ''
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_CPM]') IS NOT NULL
	drop procedure up_prescricao_CPM
go

create PROCEDURE up_prescricao_CPM
@pedidostamp as varchar(25)

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

	Select 
		lpDesignacao = left(lpDesignacao,245)
		,anulado = case when anulado = 'S' then 'SIM' ELSE 'N�O' end
		,renovavel = case when renovavel = 'S' then 'SIM' ELSE 'N�O' end
		,data = convert(varchar(10),Replace(convert(date,data),'-','.'))
		,*
	from 
		b_cli_CPM_cabecalho
	Where
		pedidostamp = @pedidostamp
	Order by
		b_cli_CPM_cabecalho.data desc, numReceita desc

GO
Grant Execute On up_prescricao_CPM to Public
Grant Control On up_prescricao_CPM to Public
GO