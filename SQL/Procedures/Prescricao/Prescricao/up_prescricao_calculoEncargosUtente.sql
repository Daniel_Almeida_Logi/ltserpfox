/*
	Calculo de Encargos do Utente

	exec up_prescricao_calculoEncargosUtente  
					@cnp = ''
					,@cnpem = 50002392
					,@grphmgcode = 'GH0728'
					,@compLyrica = 0
					,@diploma_ID = 55
					,@plano = 'CNPRP'

	Notas:

		Calculo encargos - RG e RE
		
		1. Se a sp for chamada com grupo hom�geneo a comparticipa��o � calculada sobre o pvpmaxre

			"Esta prescri��o custa-lhe, no m�ximo, � nn,nn, a n�o ser que opte por um medicamento mais caro."

			Deve ser identificado o encargo para o utente com base no PVPMAX100%RE - Tipo_preco_ID=301, 
			do CNPEM a que o medicamento pertence. Sendo que:
				. No caso de existirem dois valores distintos do campos PVPMAX100RE dever� ser utilizado
				o valor mais elevado para calculo de encargos.
				. Se todos os PVPs dos medicamentos do CNPEM forem superiores ao valor de PVPMAX100%RE,
				o c�lculo do encargo deve ser efetuado com o valor de PVP mais baixo (Tipo_preco_ID=1) do CNPEM

		2. Se a sp for chamada sem grupo homog�neo a comparticipa��o � calculada sobre o pvporig

			"Esta prescri��o custa-lhe, no m�ximo, � nn,nn, a n�o ser que opte por um medicamento mais caro."

			Deve ser identificado o encargo para o utente, com base no pre�o mais baixo do medicamento similar
			O pre�o mais baixo de um medicamento similar deve ser selecionado com base no PVP:
				. se o medicamento for comparticipado - Tipo_preco_ID=501 (Lt. pvpcalc)
				. se o medicamento n�o for comparticipado - Tipo_precoID = 1 (Lt. pvporig)

		3. Com justifica��o t�cnica
			Prescri��o com justifica��o T�cnica CNP

			"Este medicamento custa-lhe, no m�ximo, � nn,nn, podendo optar por um mais barato."

			Deve ser identificado o encargo para o utente, com base no pre�o do medicamento.
			O software deve calcular os encargos para o utente utilizando:
				. se o medicamento for comparticipado - Tipo_preco_ID=501 (Lt. pvpcalc)
				. se o medicamento n�o for comparticipado - Tipo_precoID = 1 (Lt. pvporig)

		4. Em qualquer caso se tiver diploma
			. deve calcular o valor da comparticipa��o com base no diploma

		5. Se o plano for CNPRP
			. a comparticipa��o � a 100%

	TODOs:
		. CNPRPs n�o deviam comparticipar n�o comparticipados????
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_calculoEncargosUtente]') IS NOT NULL
	drop procedure up_prescricao_calculoEncargosUtente
go


create PROCEDURE up_prescricao_calculoEncargosUtente
	@cnp varchar(18) = '' -- codigo produto
	,@cnpem	varchar(8) = 0 -- cnpem do produto
	,@grphmgcode varchar(6) = '' -- grupo homog�neo do produto
	,@compLyrica bit = 0 -- Medicamentos lyrica
	,@diploma_ID float = 0
	,@plano varchar(20) = ''

/* WITH ENCRYPTION */ 
AS

SET NOCOUNT ON
	

	If OBJECT_ID('tempdb.dbo.#dados_fprod_base') IS NOT NULL
		drop table #dados_fprod_base;
	If OBJECT_ID('tempdb.dbo.#dados_fprod_t') IS NOT NULL
		drop table #dados_fprod_t;
	If OBJECT_ID('tempdb.dbo.#dados_diploma_t') IS NOT NULL
		drop table #dados_diploma_t;
	If OBJECT_ID('tempdb.dbo.#resultCompart_t') IS NOT NULL
		drop table #resultCompart_t;


	-- Create temp table
	CREATE TABLE #dados_fprod_base ( 
		cnp varchar(18)
		,cnpem varchar(8)
		,grphmgcode varchar(6)
		,grphmgdescr varchar(100)
		,pref numeric(13,3)
		,comp_sns numeric(5,2)
		,pvporig numeric(13,3)
		,pvpmaxre numeric(13,3)
		,pvpcalc numeric(13,3)
	);


	/*

	*/
	if (@cnp != '')
	begin
		insert into
			#dados_fprod_base
		select
			cnp, cnpem, grphmgcode, grphmgdescr, pref, comp_sns, pvporig, pvpmaxre, pvpcalc
		from 
			fprod (nolock)
		where
			pvporig != 0
			and cnp = @cnp

	end else begin

		insert into
			#dados_fprod_base
		select
			cnp, cnpem, grphmgcode, grphmgdescr, pref, comp_sns, pvporig, pvpmaxre, pvpcalc
		from 
			fprod (nolock)
		where
			cnpem = @cnpem
			and u_nomerc = 1
			and 1 = case
						when @grphmgcode = '' and pvporig != 0 then 1
						when @grphmgcode != '' and pvpmaxre != 0 then 1
						else 0
						end
			and grphmgcode = case
								when @grphmgcode != '' then @grphmgcode
								when grphmgcode = '' or grphmgcode = 'GH0000' then grphmgcode
								else 'dontMatch'
								end
	end


	/*
		
	*/
	Select
		cnp
		,cnpem
		,grphmgcode
		,grphmgdescr
		,pref = case
					when pref=0 then pvporig
					else pref
					end
		-- TODO: Validar se � necess�rio for�ar a comparticipa��o dos medicamentos quando lyrica=1
		,comp_sns = case
						when @compLyrica=1 then 37
						when @plano = 'CNPRP' then 100
						else comp_sns
						end
		,comp_snsRe =  case
						when @compLyrica=1 then 52
						when @plano = 'CNPRP' then 100
						else case
								when comp_sns > 0 and pvporig <= pvpmaxre then 95
								else case
										when comp_sns = 90 then 95 /* Escalao A */
										when comp_sns = 69 then 84 /* Escalao B */
										when comp_sns = 37 then 52 /* Escalao C */
										when comp_sns = 15 then 30 /* Escalao D */
										else comp_sns
										end
								end
						end
		,pvporig
		,pvpmaxre
		,pvpcalc
	into
		#dados_fprod_t
	From
		#dados_fprod_base


	--select round(pvpmaxre - (pref * comp_snsRe / 100), 2),* from #dados_fprod_t t order by t.pvpmaxre

	/*
		Encontrar a ref e o preco para o calculo de encargos
	*/
	declare @refCalculo varchar(18), @precoCalculo numeric(13,3), @precoSNSCalculo numeric(13,3)

	if (@cnp != '')
	begin

		select
			 @refCalculo = cnp
			,@precoCalculo = pvporig
			,@precoSNSCalculo = pref
		from
			#dados_fprod_t

	end else begin

		if (@grphmgcode = '' or (select min(pvporig) from #dados_fprod_t) > (select max(pvpmaxre) from #dados_fprod_t))
		begin

			-- pvp mais baixo
			select top 1
				 @refCalculo = cnp
				,@precoCalculo = pvporig
				,@precoSNSCalculo = pvporig
			from
				#dados_fprod_t
			order by
				pvporig

		end else begin

			-- pvpmaxre mais alto
			select top 1
				 @refCalculo = cnp
				,@precoCalculo = pvpmaxre
				,@precoSNSCalculo = pref
			from
				#dados_fprod_t
			where
				pvpmaxre>=pvporig -- se temos produtos com um pvpmaxre, n�o demos considerar produtos com o pvporig > pvpmaxre
			order by
				pvpmaxre desc, pvporig desc
		end
	end



	/*
		
	*/
	Select
		cnp
		,cnpem
		,grphmgcode
		,grphmgdescr
		,comp_sns
		,comp_snsRe

		,encSnsRg = case
						when @precoCalculo > (@precoSNSCalculo * comp_sns / 100)
							then round(@precoSNSCalculo * comp_sns / 100, 2)
						else @precoCalculo
						end
		,encUtRg = case
						when @precoCalculo > (@precoSNSCalculo * comp_sns / 100)
							then round(@precoCalculo - (@precoSNSCalculo * comp_sns / 100), 2)
						else 0
						end

		,encSnsRe = case
						when @precoCalculo > (@precoSNSCalculo * comp_snsRe / 100)
							then round(@precoSNSCalculo * comp_snsRe / 100, 2)
						else @precoCalculo
						end
		,encUtRe = case
						when @precoCalculo > (@precoSNSCalculo * comp_snsRe / 100)
							then round(@precoCalculo - (@precoSNSCalculo * comp_snsRe / 100), 2)
						else 0
						end

		-- valores com diploma
		,comp_diploma = 0
		,encUtRgDip = 0 
		,encUtReDip = 0 

		-- TODO: validar este campo
		,encSNSCnrp = (select min(pvporig) from #dados_fprod_t)
	into
		#resultCompart_t
	From
		#dados_fprod_t
	where
		cnp = @refCalculo



	/*
		. Se for aplicado diploma & comp_sns > 0
			. obter valores de comparticipa��o com diploma
			. Se o plano for CNPRP manter a 100% de comparticipa��o
	*/
	if (@diploma_ID != 0 and (select comp_sns from #resultCompart_t) > 0)
	begin
		-- Levantar comparticipa��o do diploma
		Select Top 1
			compart_dip = compart
		into
			#dados_diploma_t
		from
			b_patol (nolock)
			left join b_patol_dip_lnk pdl (nolock) on b_patol.id = pdl.patol_ID
			inner join dplms (nolock) on pdl.diploma_ID = dplms.diploma_ID
		Where
			dplms.diploma_ID = @diploma_ID
			and pdl.cnp = @refCalculo -- apenas aplicar diploma se o produto for sujeito ao mesmo
		Order by
			compart desc

		update
			#resultCompart_t
		set
			comp_diploma = d.compart_dip
			,encUtRgDip = case
							when @plano = 'CNPRP' then encUtRg
							else encUtRg - (encUtRg * (d.compart_dip/100))
							end
			,encUtReDip = case
							when @plano = 'CNPRP' then encUtRe
							else encUtRe - (encUtRe * (d.compart_dip/100))
							end
		from
			#dados_diploma_t d
	end


	-- Ouput result
	select * from #resultCompart_t


	-- Clean up
	If OBJECT_ID('tempdb.dbo.#dados_fprod_base') IS NOT NULL
		drop table #dados_fprod_base;
	If OBJECT_ID('tempdb.dbo.#dados_fprod_t') IS NOT NULL
		drop table #dados_fprod_t;
	If OBJECT_ID('tempdb.dbo.#dados_diploma_t') IS NOT NULL
		drop table #dados_diploma_t;
	If OBJECT_ID('tempdb.dbo.#resultCompart_t') IS NOT NULL
		drop table #resultCompart_t;


GO
Grant Execute On up_prescricao_calculoEncargosUtente to Public
Grant Control On up_prescricao_calculoEncargosUtente to Public
GO