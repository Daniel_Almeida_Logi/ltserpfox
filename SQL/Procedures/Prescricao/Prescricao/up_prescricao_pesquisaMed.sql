/* SP pesquisa medicamentos na PE

 	exec up_prescricao_pesquisaMed 50, 'sinva', '', 'sinva', '', '', '', 0, '', 0, 1, 0

	select * from fprod where cnp = '6190926'

	exec up_prescricao_pesquisaMed 500,'7744920','','7744920','','','',0,'',0,50,0
	
*/


if OBJECT_ID('[dbo].[up_prescricao_pesquisaMed]') IS NOT NULL
	drop procedure up_prescricao_pesquisaMed
go

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

Create PROCEDURE [dbo].up_prescricao_pesquisaMed
@top				int,
@ref				as varchar(254),
@DCI				as varchar(254),
@NOME_COMERCIAL		as varchar(254),
@GRUPO_TERAPEUTICO	as varchar(254),
@DOSAGEM			as varchar(254),
@FORMA_TERAPEUTICA	as varchar(254),
@incluir			as bit,
@cnpem				as varchar(8),
@protocolo			as bit,
@userno				as numeric(5,0) = 0,
@PemMaterializada	as bit

/* WITH ENCRYPTION */ 

AS
	if @top = 0
	begin
		Select	
			top 0
			sel	= convert(bit,0)
			,seldci = convert(bit,0)
			,ref = fprod.cnp
			,descricao = fprod.descricao
			,dci
			,descr = ''
			,nome_comercial = convert(varchar(254),nome)
			,dosagem = convert(varchar(254),dosuni)
			,FORMA_FARMACEUTICA	= fformasdescr
			,GRUPO_TERAPEUTICO	= ''
			,DIMENSAO_EMBALAGEM	= descricao
			,TAXA_COMPARTICIPACAO	= 0
			,PVP = isnull(pvporig,0)
			,PUT = ISNULL(fprod.precouni,0)
			,UTENTE = isnull(fprod.pvporig,0)
			,PSNS = 0
			,codGrupoH = grphmgcode
			,designGrupoH = grphmgdescr
			,fabricanteno = titaim
			,fabricantenome = titaimdescr
			,familia = u_familia
			,generico
			,psico
			,benzo
			,manipulado
			,protocolo
			,tipembdescr
			,estadocomercial = fprod.sitcomdescr
			,dataEstadoComercial = fprod.sitcomdata
			,posologia = convert(varchar(254),'')
			,estado	= estaimdescr
			,fprod.grupo
			,pmu = ISNULL(fprod.precouni,0)
			,num_unidades
			,tipoduracao = fprod.tiptratdescr
			,ListagemDci = dci
			,lista_dcis	= fprod.dci
			,TipoReceita = ''
			,medid
			,posologiaSugerida = ''
			,posologiaOriginal = ''
			,prescNomeCod = CONVERT(varchar(2),'')
			,prescNomeDescr = CONVERT(varchar(254),'')
			,fprod.cnpem
			,fprod.marg_terap
			,fprod.u_nomerc
			,DCIPT_ID = case when DCIPT_ID = 0 then 99999 else DCIPT_ID end
			,prescEmbCod = CONVERT(varchar(10),'')
			,prescEmbDescr = CONVERT(varchar(254),'')
			,prescDateCod = CONVERT(varchar(10),'')
			,prescDateDescr = CONVERT(varchar(254),'')
			,descJust = CONVERT(varchar(600),'')
			,embUnit
			,duracaoValor= CONVERT(varchar(10),'')
			,duracaoUnidade= CONVERT(varchar(10),'')
			,frequenciaValor= CONVERT(varchar(10),'')
			,frequenciaUnidade= CONVERT(varchar(10),'')
			,quantidadeValor= CONVERT(varchar(10),'')
			,quantidadeUnidade= CONVERT(varchar(10),'')
		From	
			fprod (nolock)
	
	end
	else
	begin	
		
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosPosologia'))
			DROP TABLE #dadosPosologia
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosUltimaPosologiaProdutoMedico'))
			DROP TABLE #dadosUltimaPosologiaProdutoMedico
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosGrupoTerapeutico'))
			DROP TABLE #dadosGrupoTerapeutico
		
		select 
			atcmini as grupoterapeutico
			,cnp 
		into 
			#dadosGrupoTerapeutico
		from 
			B_atc as a
			inner join B_atcfp as b on a.atccode=b.atccode
		
		
		select 
			ref
			,posologia
			,id =  ROW_NUMBER() over(partition by ref order by B_cli_prescl.ousrdata desc,  B_cli_prescl.ousrhora desc)
			,duracaoValor=isnull(duracaoValor,'')
			,duracaoUnidade=isnull(duracaoUnidade,'')
			,frequenciaValor=isnull(frequenciaValor,'')
			,frequenciaUnidade=isnull(frequenciaUnidade,'')
			,quantidadeValor=isnull(quantidadeValor,'')
			,quantidadeUnidade=isnull(quantidadeUnidade,'')
		into
			#dadosPosologia
		From
			B_cli_prescl (nolock)
			inner join B_cli_presc (nolock) on B_cli_prescl.prescstamp = B_cli_presc.prescstamp
		where
			B_cli_presc.drno = @userno
			and B_cli_prescl.ref != '' and B_cli_prescl.posologiaNova !=0
			and convert(varchar(250),B_cli_prescl.posologia) != ''
		
		
		Select 
			*
		into
			#dadosUltimaPosologiaProdutoMedico
		From
			#dadosPosologia
		Where
			id = 1
		
		
		Select	
			top (@top)
			sel	= convert(bit,0)
			,seldci = convert(bit,0)
			,ref = fprod.cnp
			,descricao = fprod.descricao
			,dci
			,descr = CASE 
				when fprod.grupo = 'MN' THEN 
					RTRIM(LTRIM(Convert(varchar(254), 
					CASE WHEN RTRIM(LTRIM(dci)) = '' THEN RTRIM(LTRIM(Convert(varchar(254),nome + ', ' + case when convert(varchar,dosuni) != '' then convert(varchar,dosuni) else 'N.D.' end + ', ' + RTRIM(LTRIM(fformasdescr)) +  ', ' + RTRIM(LTRIM(descricao))) + " - Fazer segundo a arte"))
					ELSE RTRIM(LTRIM(Convert(varchar(254),RTRIM(LTRIM(dci))+', '+ RTRIM(LTRIM(nome)) + ', ' + convert(varchar,dosuni) + ', ' + RTRIM(LTRIM(fformasdescr)) +  ', ' + RTRIM(LTRIM(descricao))) + " - Fazer segundo a arte")) END)))
				ELSE 				
					RTRIM(LTRIM(Convert(varchar(254), 
					CASE WHEN RTRIM(LTRIM(dci)) = '' THEN RTRIM(LTRIM(Convert(varchar(254),nome + ', ' + case when convert(varchar,dosuni) != '' then convert(varchar,dosuni) else 'N.D.' end + ', ' + RTRIM(LTRIM(fformasdescr)) +  ', ' + RTRIM(LTRIM(descricao)))))
					ELSE RTRIM(LTRIM(Convert(varchar(254),RTRIM(LTRIM(dci))+', '+ RTRIM(LTRIM(nome)) + ', ' + convert(varchar,dosuni) + ', ' + RTRIM(LTRIM(fformasdescr)) +  ', ' + RTRIM(LTRIM(descricao))))) END)))  
				END 
			,nome_comercial = convert(varchar(254),nome)
			,dosagem = convert(varchar(254),dosuni)
			,FORMA_FARMACEUTICA	= fformasdescr
			,GRUPO_TERAPEUTICO	= #dadosGrupoTerapeutico.grupoterapeutico
			,DIMENSAO_EMBALAGEM	= descricao
			,TAXA_COMPARTICIPACAO	= 0
			,PVP = isnull(pvporig,0)
			,PUT = ISNULL(fprod.precouni,0)
			,UTENTE = isnull(fprod.pvporig,0)
			,PSNS = 0
			,codGrupoH = grphmgcode
			,designGrupoH = grphmgdescr
			,fabricanteno = titaim
			,fabricantenome = titaimdescr
			,familia = u_familia
			,generico
			,psico
			,benzo
			,manipulado
			,protocolo
			,tipembdescr
			,estadocomercial = fprod.sitcomdescr
			,dataEstadoComercial = fprod.sitcomdata
			,posologia = isnull(#dadosUltimaPosologiaProdutoMedico.posologia,'')--convert(varchar(254),'') /*--u_posprog*/
			,estado	= estaimdescr
			,fprod.grupo
			,pmu = ISNULL(fprod.precouni,0)
			,num_unidades
			,tipoduracao = fprod.tiptratdescr
			,ListagemDci = dci --case when dci = '' then nome end
			,lista_dcis	= fprod.dci
			,TipoReceita = case 
								when fprod.grupo = 'DI' then case when @PemMaterializada = 1 then 'MDT' else 'LMDT' end
								when fprod.protocolo = 1 then case when @PemMaterializada = 1 then 'MDB' else 'LMDB' end
								when fprod.psico = 1 then case when @PemMaterializada = 1 then 'RE' else 'LE' end
								when fprod.grupo = 'MN' then case when @PemMaterializada = 1 then 'MM' else 'LMM' end
								when fprod.medid != 0 then case when @PemMaterializada = 1 then 'RN' else 
									case when  fprod.grupo = 'SO' then  'LOST' ELSE 'LN' end -- produtos ostomia
								end
								else case when @PemMaterializada = 1 then 'OUT' else 'LOUT' end
							End
			,TipoUe = case 
						when fprod.psico = 1 then 0 else case when @PemMaterializada = 0 then 0 else 1 end
					  end
			,medid
			,posologiaSugerida = isnull(posologia.posologia_orientativa,'')
			,posologiaOriginal = isnull(posologia.posologia_orientativa,'')
			,prescNomeCod = CONVERT(varchar(2),'')
			,prescNomeDescr = CONVERT(varchar(254),'')
			,fprod.cnpem
			,fprod.marg_terap
			,fprod.u_nomerc
			,DCIPT_ID = CASE WHEN DCIPT_ID = 0 THEN ( CASE WHEN fprod.grupo = 'MN' THEN DCIPT_ID  ELSE 99999 END ) ELSE DCIPT_ID END
			,prescEmbCod = CONVERT(varchar(10),'')
			,prescEmbDescr = CONVERT(varchar(254),'')
			,prescDateCod = CONVERT(varchar(10),'')
			,prescDateDescr = CONVERT(varchar(254),'')
			,descJust = CONVERT(varchar(600),'')
			,embUnit
			,duracaoValor=isnull(#dadosUltimaPosologiaProdutoMedico.duracaoValor,'')
			,duracaoUnidade= isnull(#dadosUltimaPosologiaProdutoMedico.duracaoUnidade,'')
			,frequenciaValor=isnull(#dadosUltimaPosologiaProdutoMedico.frequenciaValor,'')
			,frequenciaUnidade=isnull(#dadosUltimaPosologiaProdutoMedico.frequenciaUnidade,'')
			,quantidadeValor=isnull(#dadosUltimaPosologiaProdutoMedico.quantidadeValor,'')
			,quantidadeUnidade=isnull(#dadosUltimaPosologiaProdutoMedico.quantidadeUnidade,'')
		From	
			fprod (nolock)
			left join #dadosGrupoTerapeutico on #dadosGrupoTerapeutico.cnp = fprod.cnp
			left join  posologia (nolock) on posologia.id_med = fprod.medid
			left join #dadosUltimaPosologiaProdutoMedico on #dadosUltimaPosologiaProdutoMedico.ref = fprod.cnp
		WHERE  	
			((fprod.cnp	like @ref + '%' or design like @ref + '%') or (nome = case when @NOME_COMERCIAL = '' THEN nome ELSE @NOME_COMERCIAL END))
			--and fprod.cnpem = case when @cnpem = '' then cnpem else @cnpem end
			and dci = case when @dci = '' then dci else @dci end
			--and nome = case when @NOME_COMERCIAL = '' THEN nome ELSE @NOME_COMERCIAL END	
			and isnull(#dadosGrupoTerapeutico.grupoterapeutico,'') = case when @GRUPO_TERAPEUTICO = '' THEN isnull(#dadosGrupoTerapeutico.grupoterapeutico,'') ELSE @GRUPO_TERAPEUTICO END	
			and fformasdescr = case when @FORMA_TERAPEUTICA = '' THEN fformasdescr ELSE	@FORMA_TERAPEUTICA END		
			and (fprod.u_nomerc = 1 or @incluir=1)
			and protocolo = case when @protocolo = 1 then 1 else protocolo end
		ORDER BY
			case when ISNULL(precouni,0)=0 then 9999999 else ISNULL(precouni,0) end
			,case when  isnull(pvporig,0)=0 then 9999999 else  isnull(pvporig,0) end
			,nome  asc
	end

GO
Grant Execute On up_prescricao_pesquisaMed to Public
Grant Control On up_prescricao_pesquisaMed to Public
go