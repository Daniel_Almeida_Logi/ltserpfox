/*
	Obtem Numera��o de Prescri��o
	19 digitos
	- 1 Regi�o de Saude
			- 1 Norte
			- 2 Centro
			- 3 Regi�o de Lisboa e Vale do Tejo
			- 4 Regi�o do Alentejo
			- 5 Regi�o do algarve
			- 6 Regi�o Aut�noma dos A�ores
			- 7 Regi�o Aut�noma da Madeira
	- 
	
exec up_prescricao_NumeracaoPresc 1,'01','100','Loja 2'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_NumeracaoPresc]') IS NOT NULL
	drop procedure up_prescricao_NumeracaoPresc
go

create PROCEDURE up_prescricao_NumeracaoPresc
@incremeto as numeric(9,0),
@tipoImpresso as varchar(2),
@proveniencia as varchar(3),
@site as varchar(120)

/* WITH ENCRYPTION */ 
AS

SET NOCOUNT ON

	Declare @regiao as varchar(1)
	set @regiao = (Select zonaacss from empresa Where site = @site)
	
	Declare @inicio as varchar(15)
	Declare @numeracao as varchar(15)
	Declare @prescno as varchar(13)

	SET @inicio = @regiao + @tipoImpresso + @proveniencia
	SET @prescno = convert(varchar,(Select  convert(bigint,ISNULL(max(left(right(prescno,13),11)),0))+@incremeto FROM b_cli_presc (nolock) Where site = @site and receitamcdt = 0))
	SET @numeracao = REPLICATE('0',(11-LEN(@prescno))) + @prescno
		
	select @inicio + @numeracao as numeracao
	/* via e checkdigit calculado no Fox */
	
GO
Grant Execute On up_prescricao_NumeracaoPresc to Public
Grant Control On up_prescricao_NumeracaoPresc to Public
GO