/* Gera numero de Prescricao (n� interno software)

	 exec up_prescricao_NrPrescricao 1

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_NrPrescricao]') IS NOT NULL
	drop procedure up_prescricao_NrPrescricao
go

create PROCEDURE up_prescricao_NrPrescricao
@terminal numeric(3,0)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	select 
		nrprescricao = replicate(0,9-len(isnull(LTRIM(STR(MAX(LEFT(nrprescricao,9))+1)),'')))+ isnull(LTRIM(STR(MAX(LEFT(nrprescricao,9))+1)),'') + LTRIM(STR(@@spid))
	FROM
		b_cli_presc (nolock)
	WHERE
		b_cli_presc.terminal != 0
	
GO
Grant Execute On dbo.up_prescricao_NrPrescricao to Public
Grant Control On dbo.up_prescricao_NrPrescricao to Public
GO