/*
	CARREGA MOTIVOS DE ANULACAO MCDT
	exec up_prescricao_motivosanulacaoMCDT
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_motivosanulacaoMCDT]') IS NOT NULL
	drop procedure up_prescricao_motivosanulacaoMCDT
go

create PROCEDURE up_prescricao_motivosanulacaoMCDT

/* WITH ENCRYPTION */ 
AS

SET NOCOUNT ON

	SELECT codigo,descricao, GETDATE() as dataanula FROM b_cli_mcdt_MotivoAnulacao (nolock) order by codigo

GO
Grant Execute On up_prescricao_motivosanulacaoMCDT to Public
Grant Control On up_prescricao_motivosanulacaoMCDT to Public
GO