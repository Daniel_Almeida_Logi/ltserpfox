/*
 Obter dados do Utente Web Service Outros Beneficios
 exec up_prescricao_RecmTipo '4001,3001',1
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_RecmTipo]') IS NOT NULL
	drop procedure up_prescricao_RecmTipo
go

create PROCEDURE up_prescricao_RecmTipo

@CodigosBenf as varchar(250)
,@impressao as bit = 1

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

;with  cteListaRecm as (
	Select	
		distinct tipo 
	from	
		 b_cli_ListadeRecm (nolock)
	Where	
		codigo in (select items from dbo.up_splitToTable(@CodigosBenf,','))
)

	select 
		tipo = 
			Case 
				when 'O' in (select tipo from cteListaRecm) and 'R' in (select tipo from cteListaRecm) then case When @impressao = 1 then 'RO'	else 'R' END /*� Impresso RO, mas enviado apenas R para o Webservice */
				when 'O' in (select tipo from cteListaRecm) and 'R' not in (select tipo from cteListaRecm) then 'O'
				when 'R' in (select tipo from cteListaRecm) and 'O' not in (select tipo from cteListaRecm) then 'R'
				when 'O' not in (select tipo from cteListaRecm) and 'R' not in (select tipo from cteListaRecm) then ''	
				else ''
			end 



GO
Grant Execute On up_prescricao_RecmTipo to Public
Grant Control On up_prescricao_RecmTipo to Public
GO