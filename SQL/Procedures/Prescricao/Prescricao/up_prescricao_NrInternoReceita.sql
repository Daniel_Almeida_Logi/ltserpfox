-- Gera numero de atendimento de Prescricao
-- exec up_prescricao_NrInternoReceita
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_NrInternoReceita]') IS NOT NULL
	drop procedure up_prescricao_NrInternoReceita
go

create PROCEDURE up_prescricao_NrInternoReceita

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

select 
	Case when MAX(isnull(nreclocal,0)) = '' or MAX(isnull(nreclocal,0)) is null then '1' else MAX(isnull(nreclocal,0)) end  as numerointerno
FROM
	b_cli_presc (nolock)

	
GO
Grant Execute On dbo.up_prescricao_NrInternoReceita to Public
Grant Control On dbo.up_prescricao_NrInternoReceita to Public
GO