/* Informação do Utente

	exec up_prescricao_utenteInfo 'jsB3830AB5-D3B4-4323-91C'
	exec up_prescricao_utenteInfo 'js0FCA5585-F4CB-4015-997'
	exec up_prescricao_utenteInfo 'jsCF857E6C-26D6-43DE-B1E'
	exec up_prescricao_utenteInfo 'js8197A924-388A-4C5D-AD3'
	exec up_prescricao_utenteInfo 'js4B2FB031-89A5-4139-B3A'

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_utenteInfo]') IS NOT NULL
	drop procedure up_prescricao_utenteInfo
go

create PROCEDURE up_prescricao_utenteInfo
@utstamp as char(25)

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON
	
;with cteEFRUtente as (

	select 
		b_cli_efrUtente.*
		,b_cli_efr.abrev
	from 
		b_cli_efrUtente 
		left join b_cli_efr on b_cli_efrUtente.codigo = b_cli_efr.cod
	where 
		b_cli_efrUtente.stamp = @utstamp
		and (DataValidade > GETDATE() Or b_cli_efr.abrev = 'SNS' or DataValidade = '19000101')

)
	Select 
		codpla = case when cesd = 1 then cesdidi else isnull((select Top 1 Codigo from cteEFRUtente),'') end
		--,despla = case when cesd = 1 then 'EFREST' else isnull((select Top 1 abrev from cteEFRUtente),'') end
		,despla = isnull((select Top 1 descricao from cteEFRUtente),'')
		,entpla = case when cesd = 1 then 'CESD' else isnull((select Top 1 abrev from cteEFRUtente),'') end
		--,(select Top 1 Codigo from cteEFRUtente)
		--,(select Top 1 Descricao from cteEFRUtente)
		--,(select Top 1 NumeroBenefEntidade from cteEFRUtente)
		--,(select Top 1 DataValidade from cteEFRUtente)
		,utstamp
		,nome
		,no
		,morada
		,local
		,codpost
		,telefone
		,tlmvl
		,email
		,ncont
		,b_utentes.nbenef
		,case when cesd = 1 then cesdidp else b_utentes.nbenef2  end as nbenef2
		,sexo
		,nascimento
		,codigop
		,recmtipo
		,recmdatainicio
		,recmdatafim
		,recmmotivo
		,recmdescricao
		,itdatainicio
		,itdatafim
		,itmotivo
		,itdescricao
		,nprescsns
	From	
		b_utentes (nolock)
	Where	
		 b_utentes.utstamp = @utstamp
		 
GO

Grant Execute On up_prescricao_utenteInfo to Public
Grant Control On up_prescricao_utenteInfo to Public
go



