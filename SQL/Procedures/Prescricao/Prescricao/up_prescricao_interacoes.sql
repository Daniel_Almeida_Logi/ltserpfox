-- exec up_prescricao_interacoes '2046688','','8626507,5058375,9427831'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_interacoes]') IS NOT NULL
	drop procedure up_prescricao_interacoes
go

create PROCEDURE up_prescricao_interacoes
	 @ref	varchar (18)
	,@design varchar(254)
	,@refs	varchar (2000)

/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

	select 
		ref = fprod.cnp
		,design1 = @design
		,cnp2 = fprod_2.cnp
		,Design = fprod_2.design
		,med_interacoes.med_class_b
		,med_interacoes.grau
		,med_interacoes.explicacao
		,med_interacoes.conselho
	from 
		fprod (nolock)
		inner join med_interacoes_lnk (nolock) on fprod.medid = med_interacoes_lnk.id_med_a 
		inner join med_interacoes (nolock) on med_interacoes_lnk.id_med_interacoes = med_interacoes.id
		left join fprod (nolock) as fprod_2 on fprod_2.medid = med_interacoes_lnk.id_med_b
	where
		fprod.cnp=@ref
		and fprod_2.u_nomerc = 1
		and fprod_2.cnp in (select * from dbo.up_splitToTable(@refs,','))
		

--SELECT 
--	 ref		= @ref
--	 ,design1		= @design
--	,fprod.design
--	,molecula1	= moleculas1.descricao
--	,molecula2	= moleculas2.descricao 
--	,cnp2		= moleculasfp2.cnp
--	,tipo		= intmedx.meddef
--	,frase		= intexpx.expdef 
--	,prescricao	= intexpx.predef
--	,dispensa	= intexpx.disdef
--FROM 
--	B_moleculasfp AS moleculasfp1 (nolock) 
--	INNER JOIN B_intexp intexpx (nolock) 
--	INNER JOIN B_intfp intfpx (nolock) ON intexpx.intexpstamp = intfpx.intexpstamp
--	INNER JOIN B_intmed intmedx (nolock) ON intfpx.intmedstamp = intmedx.intmedstamp ON moleculasfp1.moleculaID = intfpx.molecula1
--	INNER JOIN B_moleculas AS moleculas1 (nolock) ON intfpx.molecula1 = moleculas1.moleculaID
--	INNER JOIN B_moleculas AS moleculas2 (nolock) ON intfpx.molecula2 = moleculas2.moleculaID
--	INNER JOIN B_moleculasfp AS moleculasfp2 (nolock) ON intfpx.molecula2 = moleculasfp2.moleculaID
--	INNER JOIN fprod (nolock) ON moleculasfp2.cnp = fprod.cnp
--WHERE
--	moleculasfp1.cnp = @ref
--	and moleculasfp2.cnp in (select * from dbo.up_splitToTable(@refs,','))
	
go
grant execute on up_prescricao_interacoes to public 			
grant execute on up_prescricao_interacoes to public 
go