/*
 exec up_prescricao_ValidaUtilizador 1
 
 
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_ValidaUtilizador]') IS NOT NULL
	drop procedure up_prescricao_ValidaUtilizador
go

create PROCEDURE up_prescricao_ValidaUtilizador
@no			as varchar(10)

/* WITH ENCRYPTION */

AS

	declare @especialidade as varchar(50)
	set @especialidade = isnull((SELECT top 1 especialidade FROM b_usesp where userno = @no order by lordem),'')
	
	
	Select 
		especialidade = @especialidade
		,*
	from 
		b_us
	Where
		userno = @no
		
		

GO
Grant Execute On up_prescricao_ValidaUtilizador to Public
Grant Control On up_prescricao_ValidaUtilizador to Public
GO