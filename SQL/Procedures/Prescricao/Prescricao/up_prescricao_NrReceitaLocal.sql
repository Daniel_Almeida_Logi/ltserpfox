/* Gera numero de Prescricao (n� interno software)

	 exec up_prescricao_NrReceitaLocal 'Loja 3', '01' 
	 
*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



if OBJECT_ID('[dbo].[up_prescricao_NrReceitaLocal]') IS NOT NULL
	drop procedure up_prescricao_NrReceitaLocal
go 

create PROCEDURE up_prescricao_NrReceitaLocal
	@site			as varchar(55)
	,@tipoReceita	as varchar(5)


/* WITH ENCRYPTION */
AS


SET NOCOUNT ON

	declare @zonaacss varchar(2)
	set @zonaacss = ISNULL((select zonaacss from empresa where site = @site),0) 

	-- tratamento nr interno da receita
	declare @nrInterno varchar(20)
	set @nrInterno =  ISNULL((SELECT MAX(SUBSTRING(nreclocal, 11, 7)) from b_cli_presc where ousrdata>'20170127' and site = @site and receitamcdt = 0), '0000001')

	set @nrInterno = convert(varchar,convert(int,@nrInterno) + 1)

	set @nrInterno = replicate('0', 7 - len(@nrInterno)) + convert(varchar,@nrInterno)

	declare @localaccs varchar(5)
	--set @localaccs = varchar,ISNULL((select localacss from empresa where site = @site),0)
	set @localaccs = convert(varchar,ISNULL((select localacss from empresa where site = @site),0))

	-- nr receita Local - O c�digo 236 � fixo atribuido �s entidades prestadoras de software / o c�digo 1234 ainda devemos receber c�digo da SPMS para colocar no lugar do 1234
	SELECT 
		nrReceitaLocal = @zonaacss + @tipoReceita + '236' + @localaccs + @nrInterno 
		--nrReceitaLocal = @zonaacss + @tipoReceita + '236' + '0000' + @nrInterno 
		

GO
Grant Execute On dbo.up_prescricao_NrReceitaLocal to Public
Grant Control On dbo.up_prescricao_NrReceitaLocal to Public
GO