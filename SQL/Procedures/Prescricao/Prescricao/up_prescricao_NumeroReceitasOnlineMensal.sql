/*
Nota Importante: A ordem dos campoos tem que ser exactamente a definida, o codigo java le sequencialmente o valor dos campos
 exec up_prescricao_NumeroReceitasOnlineMensal  2013,5
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_NumeroReceitasOnlineMensal]') IS NOT NULL
	drop procedure up_prescricao_NumeroReceitasOnlineMensal
go

create PROCEDURE up_prescricao_NumeroReceitasOnlineMensal
@ano	numeric(4),
@mes	numeric(2)
/* WITH ENCRYPTION */

AS

SET NOCOUNT ON


;with cteOnline as (
	Select 
		NumeroReceitasOnline = COUNT(prescno)
		,site
	from 
		b_cli_presc
	Where 
		estado = 1
		and YEAR(data) = @ano
		and Month(data) = @mes
	Group by
		site
),cteOffline as (
	Select 
		NumeroReceitasOffline = COUNT(prescno)
		,site
	from 
		b_cli_presc
	Where 
		estado = 3
		and YEAR(data) = @ano
		and Month(data) = @mes
	Group by
		site
)

Select 
	[Identificacao da Entidade] = 'Logitools, Lda.'
	,[Identificacao do Software] = 'Logitools - Prescri��o Electronica v.13'
	,[Local Prescricao] = localnm
	,[C�digo de Local de Prescricao] = localcod
	,[Periodo de Reporte] = STR(@ano,4) + '.' + STR(@mes,2)
	,[Total de Receitas emitidas online] = isnull((select NumeroReceitasOnline from cteOnline where cteOnline.site =empresa.site ),0)
	,[Total de Receitas emitidas offline] = isnull((select NumeroReceitasOffline from cteOffline where cteOffline.site =empresa.site ),0)
	,site
from 
	empresa (nolock)
Where
	localcod != ''


GO
Grant Execute On up_prescricao_NumeroReceitasOnlineMensal to Public
Grant Control On up_prescricao_NumeroReceitasOnlineMensal to Public
GO