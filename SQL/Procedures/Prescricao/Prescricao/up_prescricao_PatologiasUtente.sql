/*
 exec up_prescricao_PatologiasUtente '' 
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_PatologiasUtente]') IS NOT NULL
	drop procedure up_prescricao_PatologiasUtente
go

create PROCEDURE up_prescricao_PatologiasUtente
@utstamp as varchar(254)

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

	select 
		descricao = descr
	from 
		b_patologias (nolock) 
		inner join b_patol (nolock) on B_patologias.patol_ID = convert(varchar(25),b_patol.id)
	where 
		ostamp=@utstamp
	order by 
		descr

GO
Grant Execute On up_prescricao_PatologiasUtente to Public
Grant Control On up_prescricao_PatologiasUtente to Public
GO
