/*
 exec up_prescricao_CPM_Linhas '20130411174045454-32-4'
 			
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_CPM_Linhas]') IS NOT NULL
	drop procedure up_prescricao_CPM_Linhas
go

create PROCEDURE up_prescricao_CPM_Linhas
@stampCab as varchar(25)

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

	Select 
		descricao = CONVERT(varchar(254), descricao)
		,quantidade
		,posologia = CONVERT(varchar(254), posologia)
		,excecao
		,cnpem
		,cnp =  numRegisto
		,stampLinha
		,*
	from 
		b_cli_CPM_linhaReceita
	Where
		stampCab = @stampCab
	order by
		b_cli_CPM_linhaReceita.cnpem, cnp


GO
Grant Execute On up_prescricao_CPM_Linhas to Public
Grant Control On up_prescricao_CPM_Linhas to Public
GO
