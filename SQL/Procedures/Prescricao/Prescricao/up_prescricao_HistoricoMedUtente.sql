/*
 exec up_prescricao_HistoricoMedUtente '' 
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_HistoricoMedUtente]') IS NOT NULL
	drop procedure up_prescricao_HistoricoMedUtente
go

create PROCEDURE up_prescricao_HistoricoMedUtente
@utstamp as varchar(254)

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

	Select 
		top 500 design
		,qtt
		,codigo = case when pordci = 1 then cnpem else ref end
		,pordci = case when pordci = 1 then 'DCI' else 'NOME' end 
		,a.prescno
		,data = case when convert(varchar(10),Replace(convert(date,a.data),'-','.')) = '1900.01.01' then '' else convert(varchar(10),Replace(convert(date,a.data),'-','.')) end
		,a.utstamp
	from 
		B_cli_presc a
		inner join B_cli_prescl b on a.prescstamp = b.prescstamp 
	where 
		a.receitamcdt = 0
		and ref != ''
		and (a.estado = '1' or a.estado = '2')
		and LEFT(design,1) != '.'
		and utstamp = @utstamp
	order by 
		a.ousrdata desc
		,a.nrprescricao desc 
	

GO
Grant Execute On up_prescricao_HistoricoMedUtente to Public
Grant Control On up_prescricao_HistoricoMedUtente to Public
GO
