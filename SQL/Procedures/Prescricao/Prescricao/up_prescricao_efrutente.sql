/*
 exec up_prescricao_efrutente 'jsB3830AB5-D3B4-4323-91C'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_efrutente]') IS NOT NULL
	drop procedure up_prescricao_efrutente
go

create PROCEDURE up_prescricao_efrutente
@utstamp as varchar(25)
/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

	select 
		b_cli_efrutente.stamp
		,b_cli_efrutente.Codigo
		,b_cli_efrutente.Descricao
		,b_cli_efrutente.NumeroBenefEntidade
		,b_cli_efrutente.DataValidade
		,b_cli_efrutente.Pais
		,b_cli_efrutente.Numcartao
		,b_cli_efrutente.TipoCartao
		,stamp = isnull(b_cli_efr.stamp,'')
		,cod = isnull(b_cli_efr.cod,'')
		,abrev = isnull(b_cli_efr.abrev,'')
		,descr = isnull(b_cli_efr.descr,'')
		,internacional = isnull(b_cli_efr.internacional,'')
		,subsistema = isnull(b_cli_efr.subsistema,'')
		,dominio = isnull(b_cli_efr.dominio,'')
		,tipo = isnull(b_cli_efr.tipo,'')
		,marcada = isnull(b_cli_efr.marcada,CONVERT(bit,0))
	from 
		b_cli_efrutente
		left join b_cli_efr on b_cli_efr.cod = b_cli_efrutente.codigo
	where 
		b_cli_efrutente.stamp = @utstamp


GO
Grant Execute On up_prescricao_efrutente to Public
Grant Control On up_prescricao_efrutente to Public
GO
