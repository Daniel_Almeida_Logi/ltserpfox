/*
	exec up_prescricao_RespostaEnvioOnline '2'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_RespostaEnvioOnline]') IS NOT NULL
	drop procedure up_prescricao_RespostaEnvioOnline
go

create PROCEDURE up_prescricao_RespostaEnvioOnline
@stamp		varchar(25)

/* WITH ENCRYPTION */ 
AS
SET NOCOUNT ON

Select * from b_cli_resposta where stamp = @stamp

GO
Grant Execute On up_prescricao_RespostaEnvioOnline to Public
Grant Control On up_prescricao_RespostaEnvioOnline to Public
GO