/*
	exec up_prescricao_RespostaErrosEnvioOnline 'js821AEC1A-6CA2-4342-B10'

	select * from b_cli_resposta

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_RespostaErrosEnvioOnline]') IS NOT NULL
	drop procedure up_prescricao_RespostaErrosEnvioOnline
go

create PROCEDURE up_prescricao_RespostaErrosEnvioOnline
@stamp		varchar(25)

/* WITH ENCRYPTION */ 
AS
SET NOCOUNT ON

	Select 
		b.nreceita
		,b.dataRec
		,b.pin
		,b.pinOpcao
		,b.nreceita2
		,b.pin2
		,b.pinOpcao2
		,b.nreceita3
		,b.pin3
		,b.pinOpcao3
		,b.nreceitaLocal
		,a.* 
	from
		b_cli_respostaErros a 
		inner join b_cli_resposta b on a.stamp = b.stamp 
	Where 
		a.stamp = @stamp

GO
Grant Execute On up_prescricao_RespostaErrosEnvioOnline to Public
Grant Control On up_prescricao_RespostaErrosEnvioOnline to Public
GO