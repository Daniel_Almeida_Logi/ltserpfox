/*
 exec up_prescricao_MedicamentosGenericosComparticipados '2532893'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_MedicamentosGenericosComparticipados]') IS NOT NULL
	drop procedure up_prescricao_MedicamentosGenericosComparticipados
go

create PROCEDURE up_prescricao_MedicamentosGenericosComparticipados
@cnp	varchar(8)

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

declare @cnpem varchar(8)
set @cnpem = isnull((select cnpem from fprod (nolock) where cnp = @cnp),'')

Select 
	cnp,cnpem,generico,comp_sns 
from 
	fprod (nolock)
where 
	generico = 1 
	and comp_sns != 0 
	and cnpem = @cnpem


GO
Grant Execute On up_prescricao_MedicamentosGenericosComparticipados to Public
Grant Control On up_prescricao_MedicamentosGenericosComparticipados to Public
GO