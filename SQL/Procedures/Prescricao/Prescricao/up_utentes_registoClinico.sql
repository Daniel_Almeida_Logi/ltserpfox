

-- exec up_utentes_registoClinico 'ADM39D513F9-E549-4822-A60'
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_utentes_registoClinico]') IS NOT NULL
    drop procedure up_utentes_registoClinico
go

create PROCEDURE up_utentes_registoClinico
@utstamp varchar(25)

/* WITH ENCRYPTION */
AS
	
		with dados (regstamp
		, utstamp
		, data
		, hora
		, ousrinis
		, ousrdata
		, ousrhora
		, usrinis
		, usrdata
		, usrhora
		, s
		, o
		, a
		, p
		, prescno
		,tipo )
	as 
	(
	Select 
		regstamp
		, utstamp
		, data
		, hora
		, ousrinis
		, ousrdata
		, ousrhora
		, usrinis
		, usrdata
		, usrhora
		, s
		, cast (o as varchar(max)) as o
		, cast(a as varchar(max)) as a
		, cast(p as varchar(max)) as p
		, '' as prescno
		,tipo
	from 
		registoClinico 
	Where
		utstamp = @utstamp
	union
	select
		cast(prescstamp as varchar(30)) as regstamp
		, cast(utstamp as varchar(30)) as utstamp
		, ousrdata as data
		, ousrhora as hora
		, ousrinis
		, ousrdata
		, ousrhora
		, usrinis
		, usrdata
		, usrhora
		, 'Prescrição nr. ' + cast(nrprescricao as varchar(30)) as s
		, cast('' as varchar(max)) as o
		, cast('' as varchar(max)) as a
		, cast('' as varchar(max)) as p
		, nrprescricao as prescno
		,'' as tipo
	from 
		B_cli_presc 	
	Where
		utstamp = @utstamp
	union
	select
		cast(anexosstamp as varchar(30)) as regstamp
		, cast(regstamp as varchar(30)) as utstamp
		, ousrdata as data
		, '' as hora
		, ousrinis
		, ousrdata
		, '' as ousrhora
		, usrinis
		, usrdata
		, '' as usrhora
		, filename as s
		, cast('' as varchar(max)) as o
		, cast('' as varchar(max)) as a
		, cast('' as varchar(max)) as p
		, 0 as prescno
		,'Anexo' as tipo
	from 
		anexos 	
	Where
		regstamp = @utstamp
		and tabela='b_utentes_rc'
	)
	select * from dados order by ousrdata, ousrhora
GO
Grant Execute On up_utentes_registoClinico to Public
Grant Control On up_utentes_registoClinico to Public
go

-- select * from anexos