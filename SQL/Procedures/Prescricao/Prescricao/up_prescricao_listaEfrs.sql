/*
 exec up_prescricao_listaEfrs
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_listaEfrs]') IS NOT NULL
	drop procedure up_prescricao_listaEfrs
go

create PROCEDURE up_prescricao_listaEfrs
/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

	Select 
		distinct cod, abrev, descr 
	from 
		b_cli_efr
	Order 
		by cod


GO
Grant Execute On up_prescricao_listaEfrs to Public
Grant Control On up_prescricao_listaEfrs to Public
GO
