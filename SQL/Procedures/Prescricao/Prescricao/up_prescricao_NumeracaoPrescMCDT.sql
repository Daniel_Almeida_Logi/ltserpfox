/*
Obtem Numera��o Local de Prescri��o MCDT

exec up_prescricao_NumeracaoPrescMCDT 1,'Loja 3'



*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_prescricao_NumeracaoPrescMCDT]') IS NOT NULL
	drop procedure up_prescricao_NumeracaoPrescMCDT
go

create PROCEDURE up_prescricao_NumeracaoPrescMCDT
@incremeto as int,
@site as varchar(120)

/* WITH ENCRYPTION */ 
AS


SET NOCOUNT ON

	SELECT	

		REPLICATE('0',9 - LEN(ISNULL(MAX(nreclocal),0)+@incremeto)) +  CONVERT(varchar(19),ISNULL(MAX(nreclocal),0)+@incremeto) as numeracao

	FROM	
		b_cli_PRESC (NOLOCK) 
	WHERE	
		receitamcdt = 1
		and site = @site
		/* data de altera��o do servi�o de MCDTs */
		and ousrdata >= '20181018'
			

GO
Grant Execute On up_prescricao_NumeracaoPrescMCDT to Public
Grant Control On up_prescricao_NumeracaoPrescMCDT to Public
GO

