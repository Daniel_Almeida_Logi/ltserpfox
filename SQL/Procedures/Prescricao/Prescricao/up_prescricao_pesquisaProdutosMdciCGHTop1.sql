/*
 Pesquisa de Produtos por mesmo DCI Com Grupo Homogeneo
 exec up_prescricao_pesquisaProdutosMdciCGHTop1 '2055283'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_pesquisaProdutosMdciCGHTop1]') IS NOT NULL
	drop procedure up_prescricao_pesquisaProdutosMdciCGHTop1
go

create PROCEDURE up_prescricao_pesquisaProdutosMdciCGHTop1

@ref varchar(60)

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON


declare @grphmg varchar(30)
set @grphmg = isnull((select top 1 grphmgcode from fprod (nolock) where fprod.cnp=@ref),'xxx')

declare @num_unidades as int
set @num_unidades = ISNULL((select top 1 num_unidades from fprod (nolock) where cnp = @ref),0)

declare @incluir as int
set @incluir = 0

;with 
	cte4 (cnp,numero) as (
		SELECT	
			cnp, count(tipo) as numero 
		FROM	
			B_moleculasfp (nolock)
		group by 
			cnp
	)
	
	select	
		Top 1 'sel'  = convert(bit,0),  
		fprod.cnp as ref, 
		ltrim(rtrim(fprod.design)) as design, 
		'DCI'	= dci,
		fprod.estaimdescr as Estado,
		pvporig
	from	
		fprod (nolock)
		Left join fpreco (nolock) on fprod.cnp=fpreco.cnp and fpreco.grupo='pvp'
		left join cptgrp (nolock) on cptgrp.grupo=fprod.grupo
	where	
		(left(fprod.cnp,1)		!= '1'
		and fprod.grphmgcode	= @grphmg
		and (fprod.sitcomdescr in ('Comerc. conf. pelo Titular','Sem informação do Titular') or @incluir=1 or fprod.sitcomdescr = '')
		and fprod.cnp != @ref)
		and fprod.grphmgcode	!= 'GH0000'	

			
GO
Grant Execute On up_prescricao_pesquisaProdutosMdciCGHTop1 to Public
Grant Control On up_prescricao_pesquisaProdutosMdciCGHTop1 to Public
Go		