/*
 Pesquisa de Prescrições
 exec up_prescricao_especilistaSimples
*/
SET ANSI_NULLS OFF
GO	
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_especilistaSimples]') IS NOT NULL
	drop procedure up_prescricao_especilistaSimples
go

create PROCEDURE up_prescricao_especilistaSimples
/* WITH ENCRYPTION */

AS

	SET NOCOUNT ON
		
	select 
		nome
		,no = userno
	from 
		b_us
	where 
		drinscri != 0
		and drordem != ''
	Order
		by nome 
		
	
GO
Grant Execute On up_prescricao_especilistaSimples to Public
Grant Control On up_prescricao_especilistaSimples to Public
go