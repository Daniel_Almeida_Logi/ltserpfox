/*
 ver Gen�ricos
 exec up_prescricao_pesquisaProdutosGenAltCGH '107592',100
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_pesquisaProdutosGenAltCGH]') IS NOT NULL
	drop procedure up_prescricao_pesquisaProdutosGenAltCGH
go

create PROCEDURE up_prescricao_pesquisaProdutosGenAltCGH

@ref varchar(60),
@top as int

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON


declare @grphmg varchar(30)
set @grphmg = isnull((select top 1 grphmgcode from fprod (nolock) where fprod.cnp=@ref),'xxx')

declare @num_unidades as int
set @num_unidades = ISNULL((select top 1 num_unidades from fprod (nolock) where cnp = @ref),0)

declare @incluir as int
set @incluir = 0

	select	
		top (@top) 
		refori = @ref,
		'sel'  = convert(bit,0),  
		fprod.cnp as ref, 
		ltrim(rtrim(fprod.design)) as design, 
		epv1 = isnull(pvporig,0), 
		IsNull(fprod.generico,0) as generico,
		'comp' = convert(bit,isnull((case when (left(cptgrp.descricao,3)!='N�o' and cptgrp.u_diploma1='' and cptgrp.u_diploma2='' and cptgrp.u_diploma3='') AND cptgrp.grupo!='ST'
						then 1 else 0 end)	,0)),
		'GRPHMG' = case when fprod.grphmgcode='GH0000' then fprod.grphmgcode
					else fprod.grphmgcode + ' - ' + fprod.grphmgdescr
				end,
		'psico' = isnull(fprod.psico,0), 
		'benzo'	= isnull(fprod.benzo,0),
		fprod.estaimdescr as Estado
	from	
		fprod (nolock)
		Left join fpreco (nolock) on fprod.cnp=fpreco.cnp and fpreco.grupo='pvp'
		left join cptgrp (nolock) on cptgrp.grupo=fprod.grupo
	where	
		(left(fprod.cnp,1)		!= '1'
		and fprod.grphmgcode	= @grphmg
		and (fprod.sitcomdescr in ('Comerc. conf. pelo Titular','Sem informa��o do Titular') or @incluir=1 or fprod.sitcomdescr = '')
		and fprod.cnp != @ref)
		and fprod.grphmgcode	!= 'GH0000'	
		
				
GO
Grant Execute On up_prescricao_pesquisaProdutosGenAltCGH to Public
Grant Control On up_prescricao_pesquisaProdutosGenAltCGH to Public
Go		