/*
exec up_prescricao_PeriodoIndisponibilidade  '20130101'
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_prescricao_PeriodoIndisponibilidade]') IS NOT NULL
	drop procedure up_prescricao_PeriodoIndisponibilidade
go

create PROCEDURE up_prescricao_PeriodoIndisponibilidade
@data	datetime

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON
/*
	Receitas que n�o tenham resposta
*/

;with cteRespostas as (
	select
		PrescStamp
	From
		b_cli_resposta
), cteLojas as (

	Select 
		localnm
		,localcod
		,site
	from 
		empresa (nolock)
)

select
	localcod
	,ousrhora
	,resposta = case when prescstamp in (select top 1 prescStamp from cteRespostas where cteRespostas.prescStamp = b_cli_presc.prescstamp) then 1 else 0 end
	,motivo = 'Falha de Comunica��o'
	--,b_cli_presc.prescstamp
	--,ousrdata
	--,b_cli_presc.site
from 
	b_cli_presc (nolock)
	left join cteLojas on cteLojas.site = b_cli_presc.site
Where 
	ousrdata = @data
	and localcod != ''
Order by
	localcod, ousrdata asc, ousrhora asc
	

GO
Grant Execute On up_prescricao_PeriodoIndisponibilidade to Public
Grant Control On up_prescricao_PeriodoIndisponibilidade to Public
GO