/*
	Get tlmvl do cliente
	
	exec up_sms_getMensagemViaRef '0000001'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_getMensagemViaRef]') IS NOT NULL
	drop procedure dbo.up_sms_getMensagemViaRef
go

create procedure dbo.up_sms_getMensagemViaRef
	@ref	char(7)
	
/* WITH ENCRYPTION */ 
AS

	select
		mensagem 
	from 
		b_sms (nolock) 
	where
		ref=@ref
		
GO
Grant Execute on dbo.up_sms_getMensagemViaRef to Public
Grant Control on dbo.up_sms_getMensagemViaRef to Public
GO