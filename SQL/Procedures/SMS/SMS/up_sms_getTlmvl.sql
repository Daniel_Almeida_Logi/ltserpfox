/*
	Get tlmvl do cliente
	
	exec up_sms_getTlmvl 'ADM12111352686.755125409'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_getTlmvl]') IS NOT NULL
	drop procedure dbo.up_sms_getTlmvl
go

create procedure dbo.up_sms_getTlmvl
	@stamp	char(25)
	
/* WITH ENCRYPTION */ 
AS

	select
		tlmvl, nome 
	from
		b_utentes (nolock) 
	where 
		utstamp in (select clstamp from B_smscl (nolock) where smsstamp=rtrim(@stamp)) and tlmvl != ''

GO
Grant Execute on dbo.up_sms_getTlmvl to Public
Grant Control on dbo.up_sms_getTlmvl to Public
GO