/*
	ver sms enviados/faturados
	
	exec up_sms_getSmsEnviadosFt
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_getSmsEnviadosFt]') IS NOT NULL
	drop procedure dbo.up_sms_getSmsEnviadosFt
go

create procedure dbo.up_sms_getSmsEnviadosFt

/* WITH ENCRYPTION */ 
AS

	select
		'enviados' = isnull(sum(total),0) 
	from
		b_sms_fact (nolock)
	where
		username = (select textvalue from B_Parameters where stamp='ADM0000000157') 
		and status in (0,1,2)

GO
Grant Execute on dbo.up_sms_getSmsEnviadosFt to Public
Grant Control on dbo.up_sms_getSmsEnviadosFt to Public
GO