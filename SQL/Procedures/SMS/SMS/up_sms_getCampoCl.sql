/*
	Get qualquer campo da tabela cl
	
	exec up_sms_getCampoCl 'ADM3468DA78-ED3F-4DB9-9B0', 'ltrim(rtrim(utstamp))'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_getCampoCl]') IS NOT NULL
	drop procedure dbo.up_sms_getCampoCl
go

create procedure dbo.up_sms_getCampoCl
	@utstamp char(25)
	,@campo varchar(150)

/* WITH ENCRYPTION */ 
AS

declare @sql varchar(max)

select @sql = N'
	select
		' + @campo + '
	from
		b_utentes (nolock)
	where
		utstamp = ''' + rtrim(@utstamp) +  ''''

exec (@sql)

GO
Grant Execute on dbo.up_sms_getCampoCl to Public
Grant Control on dbo.up_sms_getCampoCl to Public
GO