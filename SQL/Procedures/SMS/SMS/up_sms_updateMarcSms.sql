/*
	Atualiza marca��es com sms
	
	exec up_sms_updateMarcSms 'ADM12111352686.755125409', 1
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_updateMarcSms]') IS NOT NULL
	drop procedure dbo.up_sms_updateMarcSms
go

create procedure dbo.up_sms_updateMarcSms
	@mrstamp char(25)
	,@smsID int
	
/* WITH ENCRYPTION */ 
AS

	update
		b_cli_mr
	set
		marcada=1
		,smsID = @smsID
	where
		mrstamp = @mrstamp

GO
Grant Execute on dbo.up_sms_updateMarcSms to Public
Grant Control on dbo.up_sms_updateMarcSms to Public
GO