/*
	update envio aniversario
	
	exec up_sms_updateSmsAniversario
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_updateSmsAniversario]') IS NOT NULL
	drop procedure dbo.up_sms_updateSmsAniversario
go

create procedure dbo.up_sms_updateSmsAniversario

/* WITH ENCRYPTION */ 
AS

	update
		b_sms_aniversario
	set
		ultenvio = getdate()

GO
Grant Execute on dbo.up_sms_updateSmsAniversario to Public
Grant Control on dbo.up_sms_updateSmsAniversario to Public
GO