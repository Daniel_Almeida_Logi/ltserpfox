/*
	Atualizar datahora cancelada
	
	exec up_sms_setDatahoraCancelada '123'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_setDatahoraCancelada]') IS NOT NULL
	drop procedure dbo.up_sms_setDatahoraCancelada
go

create procedure dbo.up_sms_setDatahoraCancelada
	@smsstamp char(25)

/* WITH ENCRYPTION */ 
AS

	update
		b_sms
	set
		cancelada = GETDATE()
	where
		smsstamp = @smsstamp

GO
Grant Execute on dbo.up_sms_setDatahoraCancelada to Public
Grant Control on dbo.up_sms_setDatahoraCancelada to Public
GO