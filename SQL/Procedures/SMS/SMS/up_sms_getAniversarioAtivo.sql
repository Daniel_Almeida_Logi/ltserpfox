/*
	Ver aniversário ativo
	
	exec up_sms_getAniversarioAtivo
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_getAniversarioAtivo]') IS NOT NULL
	drop procedure dbo.up_sms_getAniversarioAtivo
go

create procedure dbo.up_sms_getAniversarioAtivo

/* WITH ENCRYPTION */ 
AS

	select top 1
		activo, texto, enviado = CASE 
							WHEN CAST(ultenvio AS DATE) < CAST(GETDATE() AS DATE) 
							THEN 
								case when CAST(horaenvio AS TIME) <= CAST(GETDATE() AS TIME) 
									then
										Convert(bit,0)
									else
										Convert(bit,1)
									end

							ELSE 
								Convert(bit,1)
							END
	from
		b_sms_aniversario (nolock)

GO
Grant Execute on dbo.up_sms_getAniversarioAtivo to Public
Grant Control on dbo.up_sms_getAniversarioAtivo to Public
GO
