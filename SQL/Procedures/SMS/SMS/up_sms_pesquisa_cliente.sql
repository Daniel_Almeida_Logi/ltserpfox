-- Pesquisa de Clientes para Campanha SMS
-- exec up_sms_pesquisa_cliente 200,'',0,-1,'','','',''0,'>',-999999,'',0,'','', '', '19000101', '30000101', '19000101'

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_sms_pesquisa_cliente]') IS NOT NULL
    drop procedure dbo.up_sms_pesquisa_cliente
go

create procedure dbo.up_sms_pesquisa_cliente

@top		int,
@nome		varchar(80),
@no			numeric(9),
@estab		numeric(5),
@ncartao	varchar(30),
@tipo		varchar(20),
@Profissao	varchar(60),
@sexo		varchar(1),
@tlmvl		bit,
@operador	char(1),
@idade		int,
@obs		varchar(240),
@mail		bit,
@entPla		varchar(15),
@patologia  varchar(25),
@ref		varchar(254),
@Entre		datetime,
@E			datetime,
@dtnasc		datetime

/* WITH ENCRYPTION */
AS

declare @ClAux table (sel bit, nome varchar(80), no numeric(9), estab numeric(5), tlmvl varchar(45), ncartao varchar(30), clstamp char(25), idade smallint)
insert into @ClAux (sel, nome, no, estab, tlmvl, ncartao, clstamp, idade)
SELECT
	CONVERT(bit,0) as sel,
	'nome'		= b_utentes.nome,
	'no'		= b_utentes.no,
	'estab'		= b_utentes.estab,
	'tlmvl'		= b_utentes.tlmvl,
	'ncartao'	= b_utentes.nrcartao,
	'clstamp'	= b_utentes.utstamp,
	'idade'		= DateDiff(year, b_utentes.nascimento, getdate())
from 
	b_utentes (nolock)
	left join B_patologias (nolock) on B_patologias.ostamp=(case when @patologia='' then '' else b_utentes.utstamp end)
where 
	b_utentes.nome like @nome+'%'
	  /*n� cliente*/
	  and b_utentes.no = (case when @no > 0 then @no else b_utentes.no end)
	  and b_utentes.no > 200
	  /*estabelecimento*/
	  and b_utentes.estab = (case when @estab >= 0 then @estab else b_utentes.estab end)
	  /*n� cart�o de cliente*/
	  and b_utentes.nrcartao like @ncartao+'%'
	  /*tipo de cliente*/
	  and b_utentes.tipo like @tipo+'%'
	  /*profiss�o*/
	  and b_utentes.profi like @profissao+'%'
	  /*sexo*/
	  and b_utentes.sexo = (case when @sexo <> '' then @sexo else b_utentes.sexo end)
	  /*se tem tlmvl preenchido*/
	  and b_utentes.tlmvl <> (case when @tlmvl = 0 then '***sem tlm***' else '' end)
	  /*observa��es*/
	  and b_utentes.obs like @obs+'%'
	  /*se tem mail preenchido*/
	  and b_utentes.email <> (case when @mail = 0 then '***sem mail***' else '' end)
	  /*EFR*/
	  and (b_utentes.entpla like @entPla+'%')
	  /*Patologia*/
	  and isnull(B_patologias.ctiefpstamp,'') like (case when @patologia='' then '%' else @patologia end)
	  and b_utentes.nascimento = (case when @dtnasc = '19000101' then b_utentes.nascimento else @dtnasc end)

if @ref <> ''
begin
	DECLARE @xml xml
	SET @xml = cast(('<X>'+replace(@ref,'|','</X><X>')+'</X>') as xml)
end

if @operador = '='
begin
	if @ref <> ''
	begin
		select TOP (@top) * from @ClAux
		where clstamp in(
			select distinct(aux.clstamp)
			from @ClAux aux
			inner join ft (nolock) on aux.no=ft.no and aux.estab=ft.estab
			inner join fi (nolock) on fi.ftstamp=ft.ftstamp
			where aux.idade=@idade
				and ft.ndoc=3
				and fi.ref in (SELECT C.value('.', 'varchar(13)') as value FROM @xml.nodes('X') as X(C))
				and ft.fdata between @Entre and @E
		)
	end
	else
	begin
		select TOP (@top) * from @ClAux where idade=@idade
	end 	
end
if @operador = '>'
begin
	if @ref <> ''
	begin
		select TOP (@top) * from @ClAux
		where clstamp in(
			select distinct(aux.clstamp)
			from @ClAux aux
			inner join ft (nolock) on aux.no=ft.no and aux.estab=ft.estab
			inner join fi (nolock) on fi.ftstamp=ft.ftstamp
			where aux.idade>@idade
				and ft.ndoc=3
				and fi.ref in (SELECT C.value('.', 'varchar(13)') as value FROM @xml.nodes('X') as X(C))
				and ft.fdata between @Entre and @E
		)
	end
	else
	begin
		select TOP (@top) * from @ClAux where idade>@idade
	end 
end
if @operador = '<'
begin
	if @ref <> ''
	begin
		select TOP (@top) * from @ClAux
		where clstamp in(
			select distinct(aux.clstamp)
			from @ClAux aux
			inner join ft (nolock) on aux.no=ft.no and aux.estab=ft.estab
			inner join fi (nolock) on fi.ftstamp=ft.ftstamp
			where aux.idade<@idade
				and ft.ndoc=3
				and fi.ref in (SELECT C.value('.', 'varchar(13)') as value FROM @xml.nodes('X') as X(C))
				and ft.fdata between @Entre and @E
		)
	end
	else
	begin
		select TOP (@top) * from @ClAux where idade<@idade
	end 
end


GO
Grant Execute on dbo.up_sms_pesquisa_cliente to Public
Grant Control on dbo.up_sms_pesquisa_cliente to Public
go