-- exec up_sms_AniTextoAutomatico 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_AniTextoAutomatico]') IS NOT NULL
	drop procedure dbo.up_sms_AniTextoAutomatico
go

create procedure dbo.up_sms_AniTextoAutomatico

/* WITH ENCRYPTION */ 

AS 
BEGIN
	Select	[desc] = 'Nome',
			[cod] = '�nome�'
END

GO
Grant Execute on dbo.up_sms_AniTextoAutomatico to Public
GO
Grant Control on dbo.up_sms_AniTextoAutomatico to Public
GO



