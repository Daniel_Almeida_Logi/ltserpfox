/*
	Atualizar termino da sms
	
	exec up_sms_setSMSTerminada '123'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_setSMSTerminada]') IS NOT NULL
	drop procedure dbo.up_sms_setSMSTerminada
go

create procedure dbo.up_sms_setSMSTerminada
	@smsstamp char(25)

/* WITH ENCRYPTION */ 
AS

	update
		b_sms
	set
		terminada = GETDATE()
	where
		smsstamp = @smsstamp

GO
Grant Execute on dbo.up_sms_setSMSTerminada to Public
Grant Control on dbo.up_sms_setSMSTerminada to Public
GO