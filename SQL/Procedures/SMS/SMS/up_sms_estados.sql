-- exec up_sms_estados 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_estados]') IS NOT NULL
	drop procedure dbo.up_sms_estados
go

create procedure dbo.up_sms_estados

/* WITH ENCRYPTION */ 

AS 
BEGIN
	select sel=convert(bit,0), 'Estado' = '0', 'Descri��o' = 'Mensagem planeada. Ainda n�o foi efectuado o envio.' 
	union all 
	select sel=convert(bit,0), 'Estado' = '1', 'Descri��o' = 'Mensagem activa. A mensagem foi enviada e encontra-se � espera de respostas.' 
	union all 
	select sel=convert(bit,0), 'Estado' = '2', 'Descri��o' = 'Mensagem terminada. A mensagem foi enviada e o per�odo para respostas terminou.' 
	union all 
	select sel=convert(bit,0), 'Estado' = '3', 'Descri��o' = 'Mensagem cancelada. A mensagem foi cancelada antes de ter sido enviada.'
END

GO
Grant Execute on dbo.up_sms_estados to Public
Grant Control on dbo.up_sms_estados to Public
GO