/*
	Ver �ltima notifica��o
	
	exec up_sms_getUltimaNotificacao
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_getUltimaNotificacao]') IS NOT NULL
	drop procedure dbo.up_sms_getUltimaNotificacao
go

create procedure dbo.up_sms_getUltimaNotificacao

/* WITH ENCRYPTION */ 
AS

	select top 1
		activo, texto
	from
		b_cli_notificacoes (nolock)

GO
Grant Execute on dbo.up_sms_getUltimaNotificacao to Public
Grant Control on dbo.up_sms_getUltimaNotificacao to Public
GO