/*
	update envio aniversario
	
	exec up_sms_updateSMSList
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_updateSMSList]') IS NOT NULL
	drop procedure dbo.up_sms_updateSMSList
go

create procedure dbo.up_sms_updateSMSList
@stamp as varchar(100)
/* WITH ENCRYPTION */ 
AS

	update
		logs_com
	set
		enviado = 1
	where
		stamp=@stamp

GO
Grant Execute on dbo.up_sms_updateSMSList to Public
Grant Control on dbo.up_sms_updateSMSList to Public
GO