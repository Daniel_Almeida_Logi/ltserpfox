/*
	Get qualquer campo da tabela b_sms via stamp
	
	exec up_sms_getCampoSms 'ref', 'ADM12111352686.755125409'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_getCampoSms]') IS NOT NULL
	drop procedure dbo.up_sms_getCampoSms
go

create procedure dbo.up_sms_getCampoSms
	@campo varchar(50)
	,@stamp	char(25)
	
/* WITH ENCRYPTION */ 
AS

declare @sql varchar(max)

select @sql = N'
	select ' + @campo + ' from b_sms (nolock) where smsstamp=''' + rtrim(@stamp) + ''''

exec (@sql)

GO
Grant Execute on dbo.up_sms_getCampoSms to Public
Grant Control on dbo.up_sms_getCampoSms to Public
GO