/*
	texto resposta Notificações
	
	exec up_sms_getSMSListEnviar 'ADM18/05/08_5720XJGTJ'
*/



SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_getSMSListEnviar]') IS NOT NULL
	drop procedure dbo.up_sms_getSMSListEnviar
go

create procedure dbo.up_sms_getSMSListEnviar
@codEnvio as varchar(100)

/* WITH ENCRYPTION */ 
AS

	select 
		stamp,
		codenvio,
		origem,
		ousrinis,
		texto,
		destino,
		tipo
	from
		logs_com (nolock)
	where
		codenvio=@codEnvio and enviado=0

GO
Grant Execute on dbo.up_sms_getSMSListEnviar to Public
Grant Control on dbo.up_sms_getSMSListEnviar to Public
GO



