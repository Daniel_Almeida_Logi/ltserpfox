/*
	delete smsFt
	
	exec up_sms_deleteSmsFt 1
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_deleteSmsFt]') IS NOT NULL
	drop procedure dbo.up_sms_deleteSmsFt
go

create procedure dbo.up_sms_deleteSmsFt
	@campanhaID int
	
/* WITH ENCRYPTION */ 
AS

	delete from
		b_sms_fact
	where
		campaignId = @campanhaID

GO
Grant Execute on dbo.up_sms_deleteSmsFt to Public
Grant Control on dbo.up_sms_deleteSmsFt to Public
GO