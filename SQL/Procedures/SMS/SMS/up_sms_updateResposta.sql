/*
	Atualizar resposta
	
	exec up_sms_updateResposta '123', '123', '', ''
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_updateResposta]') IS NOT NULL
	drop procedure dbo.up_sms_updateResposta
go

create procedure dbo.up_sms_updateResposta
	@resposta varchar(254)
	,@respostadatahora varchar(50)
	,@smsstamp char(25)
	,@utstamp char(25)

/* WITH ENCRYPTION */ 
AS

	update
		b_smscl
	set
		resposta = @resposta
		,respostadatahora = @respostadatahora
	where
		smsstamp = @smsstamp
		and clstamp = @utstamp

GO
Grant Execute on dbo.up_sms_updateResposta to Public
Grant Control on dbo.up_sms_updateResposta to Public
GO