/*
	update campanha ID
	
	exec up_sms_setCampanhaID 'ADM12051482026.900287543', 'ADM12051482026', 1
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_setCampanhaID]') IS NOT NULL
	drop procedure dbo.up_sms_setCampanhaID
go

create procedure dbo.up_sms_setCampanhaID
	@stamp	char(25)
	,@campanhaID varchar(50)
	,@listID varchar(50)
	,@emitido bit

/* WITH ENCRYPTION */ 
AS

	update
		b_sms
	set
		campanhaID = @campanhaID,
		listID = @listID,
		emitido = @emitido
	where
		smsstamp=@stamp

GO
Grant Execute on dbo.up_sms_setCampanhaID to Public
Grant Control on dbo.up_sms_setCampanhaID to Public
GO