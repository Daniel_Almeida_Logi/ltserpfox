/*
	Insert resposta
	
	exec up_sms_insertResposta '123', '123', 1
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_insertResposta]') IS NOT NULL
	drop procedure dbo.up_sms_insertResposta
go

create procedure dbo.up_sms_insertResposta
	@smsstamp char(25)
	,@tipo varchar(50)
	,@result tinyint

/* WITH ENCRYPTION */ 
AS

	insert into b_sms_resposta
		(stamp, smsstamp, tipo, DataEnvio, Codresp)
	values
		(left(NEWID(),25), @smsstamp, @tipo, GETDATE(), @result)

GO
Grant Execute on dbo.up_sms_insertResposta to Public
Grant Control on dbo.up_sms_insertResposta to Public
GO