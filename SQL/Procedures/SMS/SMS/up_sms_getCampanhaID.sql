
/*
	Get campanha ID via stamp
	
	exec up_sms_getCampanhaID ''
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_getCampanhaID]') IS NOT NULL
	drop procedure dbo.up_sms_getCampanhaID
go

create procedure dbo.up_sms_getCampanhaID
	@stamp	char(25)

/* WITH ENCRYPTION */ 

AS

	select
		campanhaID
	from 
		b_sms (nolock) 
	where 
		smsstamp = rtrim(@stamp)

GO
Grant Execute on dbo.up_sms_getCampanhaID to Public
Grant Control on dbo.up_sms_getCampanhaID to Public
GO