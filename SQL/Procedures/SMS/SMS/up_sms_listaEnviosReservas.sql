/*
	Lista envios reservas
	
	exec up_sms_listaEnviosReservas 'ADM13041853372.622717956'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_listaEnviosReservas]') IS NOT NULL
	drop procedure dbo.up_sms_listaEnviosReservas
go

create procedure dbo.up_sms_listaEnviosReservas
	@stamp char(25)
	
/* WITH ENCRYPTION */ 
AS

	select 
		*
	from
		b_res_enviar (nolock)
	where
		stamp = @stamp

GO
Grant Execute on dbo.up_sms_listaEnviosReservas to Public
Grant Control on dbo.up_sms_listaEnviosReservas to Public
GO