/*
	Get qualquer campo da tabela b_cli_mr via stamp
	
	exec up_sms_getCampoCliMr 'ADM12111352686.755125409', 'qqcoisa'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_getCampoCliMr]') IS NOT NULL
	drop procedure dbo.up_sms_getCampoCliMr
go

create procedure dbo.up_sms_getCampoCliMr
	@mrstamp char(25)
	,@campo varchar(150)
	
/* WITH ENCRYPTION */ 
AS

declare @sql varchar(max)

select @sql = N'
	select 
		' + @campo + '
	from 
		b_cli_mr (nolock)
	where
		mrstamp = ''' + rtrim(@mrstamp) +  ''''

exec (@sql)

GO
Grant Execute on dbo.up_sms_getCampoCliMr to Public
Grant Control on dbo.up_sms_getCampoCliMr to Public
GO