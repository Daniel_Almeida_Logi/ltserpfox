-- Pesquisa Reservas para envio de SMS
-- exec up_sms_pesqReservas

--SET ANSI_NULLS OFF
--GO
--SET QUOTED_IDENTIFIER OFF
--GO

--if OBJECT_ID('[dbo].[up_sms_pesqReservas]') IS NOT NULL
--    drop procedure dbo.up_sms_pesqReservas
--go

--create procedure dbo.up_sms_pesqReservas

--/* WITH ENCRYPTION */
--AS

--select id.tlmvl [idtlm], b_cli_mr.* 
--from b_cli_mr (nolock)
--inner join id (nolock) on id.idno=b_cli_mr.idno
--where 
--	id.u_notif=1
--	and id.tlmvl!=''
--	and convert(date,b_cli_mr.data,101) = convert(date,DATEADD(day,(select top 1 dias from b_cli_notificacoes),GETDATE()),101)
--	and b_cli_mr.marcada=0 /*se j� foi enviada*/
--	and b_cli_mr.estado in ('MARCADO','')

--GO
--Grant Execute on dbo.up_sms_pesqReservas to Public
--Grant Control on dbo.up_sms_pesqReservas to Public
--go