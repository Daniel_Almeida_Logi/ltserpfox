/*
	select * from b_sms_fact WHERE username like 'pharmacare'
	select * from b_sms_fact WHERE status = 0
	delete from b_sms_fact 

	select * from b_sms_aniversario (nolock) where campanhaID like '%1f3099b7-862f-4316-a10b-2ede21cbdade%'
	
exec up_sms_insertSmsFt '467374359', 'Reserva', 557467998, 'LTS_Testes', 'WS_INTERNAL_557430254 - WS_INTERNAL_557466737 - WS_INTERNAL_557471014 - WS_INTERNAL_557467998 - WS_INTERNAL_557471016 - WS_INTERNAL_557425464 - ', 1, 'Tem uma reserva com o numero 220 relativa � data Mar 11 2022 12:00AM', '2022-03-22T12:27:32.0000000Z', 2

exec up_sms_insertSmsFt '1f3099b7-862f-4316-a10b-2ede21cbdade', 'Campanha Teste', 0, 'pharmacare', '+351962921151', 1 , 'asqsewqwqewqewqewqewq', '1733151302.714654', 0

@campanhaID varchar ,@name varchar(250) ,@listID int ,@username varchar(50) ,@listTlm varchar(max) ,@Total int ,@mensagem varchar(459) ,@strDataEnvio  varchar(100) ,@status int
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_insertSmsFt]') IS NOT NULL
	drop procedure dbo.up_sms_insertSmsFt
go

create procedure dbo.up_sms_insertSmsFt
	 @campanhaID varchar(250)
	,@name varchar(250)
	,@listID int
	,@username varchar(50)
	,@listTlm varchar(max)
	,@Total int
	,@mensagem      varchar(459)
	,@strDataEnvio  varchar(100)
	,@status int
	
/* WITH ENCRYPTION */ 
AS

	-- Converter Unix Timestamp para bigint ap�s truncar as casas decimais
    DECLARE @unixTimestamp bigint = CAST(FLOOR(CAST(@strDataEnvio AS float)) AS bigint);

    -- Converter Unix Timestamp para datetime
    DECLARE @dataEnvioDatetime datetime = DATEADD(SECOND, @unixTimestamp, '1970-01-01 00:00:00');

    -- Extrair a data e hora
    DECLARE @dataenvio date = CONVERT(date, @dataEnvioDatetime);
    DECLARE @horaenvio varchar(8) = CONVERT(varchar(8), @dataEnvioDatetime, 108);


	insert into b_sms_fact
		(stamp, campaignId, name, listId, username, listTlm, Total, mensagem, dataenvio, horaenvio, [status])
	values
		(left(newid(),25), @campanhaID, @name, @listID, @username, @listTlm
		,isnull(@Total,0), @mensagem, @dataenvio, @horaenvio, @status)                   

GO
Grant Execute on dbo.up_sms_insertSmsFt to Public
Grant Control on dbo.up_sms_insertSmsFt to Public
GO

