/*
	Atualizar report
	
	exec up_sms_updateReport '123', '123', '', ''
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_updateReport]') IS NOT NULL
	drop procedure dbo.up_sms_updateReport
go

create procedure dbo.up_sms_updateReport
	@relatorio varchar(20)
	,@relatoriodatahora varchar(50)
	,@smsstamp char(25)
	,@utstamp char(25)

/* WITH ENCRYPTION */ 
AS

	update 
		b_smscl 
	set 
		relatorio = @relatorio
		,relatoriodatahora = @relatoriodatahora
	where
		smsstamp = @smsstamp
		and clstamp = @utstamp

GO
Grant Execute on dbo.up_sms_updateReport to Public
Grant Control on dbo.up_sms_updateReport to Public
GO