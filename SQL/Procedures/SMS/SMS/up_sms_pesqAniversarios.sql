-- Pesquisa Aniversários para envio de SMS
-- exec up_sms_pesqAniversarios
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_pesqAniversarios]') IS NOT NULL
    drop procedure dbo.up_sms_pesqAniversarios
go

create procedure dbo.up_sms_pesqAniversarios

/* WITH ENCRYPTION */
AS

select tlmvl,utstamp from (
	select 
		tlmvl
		,utstamp
		,ROW_NUMBER() over(partition by tlmvl order by tlmvl) rep
	from b_utentes (nolock)
	where 
		nascimento != '19000101'
		and day(nascimento) = day(getdate())
		and month(nascimento) = month(getdate())
		and tlmvl != ''
		and (select top 1 ultenvio from b_sms_aniversario (nolock)) != convert(date, getdate())
		--and left(convert(time,getdate()),5) >= (select top 1 horaenvio from b_sms_aniversario (nolock)) 
)x
where x.rep=1

GO
Grant Execute on dbo.up_sms_pesqAniversarios to Public
Grant Control on dbo.up_sms_pesqAniversarios to Public
go