/*
	Get qualquer campo da tabela bi
	
	exec up_sms_getCampoBi 'ADM3468DA78-ED3F-4DB9-9B0', 'ltrim(rtrim(bistamp))'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_getCampoBi]') IS NOT NULL
	drop procedure dbo.up_sms_getCampoBi
go

create procedure dbo.up_sms_getCampoBi
	@bistamp char(25)
	,@campo varchar(150)

/* WITH ENCRYPTION */ 
AS

declare @sql varchar(max)

select @sql = N'
	select
		' + @campo + '
	from
		bi (nolock)
	where
		bistamp = ''' + rtrim(@bistamp) +  ''''

exec (@sql)

GO
Grant Execute on dbo.up_sms_getCampoBi to Public
Grant Control on dbo.up_sms_getCampoBi to Public
GO