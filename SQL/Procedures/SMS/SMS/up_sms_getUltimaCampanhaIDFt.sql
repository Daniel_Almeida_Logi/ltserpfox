/*
	Get �ltima campanha ID facturada
	
	exec up_sms_getUltimaCampanhaIDFt
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_getUltimaCampanhaIDFt]') IS NOT NULL
	drop procedure dbo.up_sms_getUltimaCampanhaIDFt
go

create procedure dbo.up_sms_getUltimaCampanhaIDFt
	
/* WITH ENCRYPTION */ 
AS

	select
		MAX(campaignId) as maxCamp
	from
		b_sms_fact (nolock)

GO
Grant Execute on dbo.up_sms_getUltimaCampanhaIDFt to Public
Grant Control on dbo.up_sms_getUltimaCampanhaIDFt to Public
GO