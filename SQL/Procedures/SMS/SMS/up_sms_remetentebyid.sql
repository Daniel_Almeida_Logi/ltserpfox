/*
	Ver �ltima notifica��o
	
	exec up_sms_remetentebyid '3'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_remetentebyid]') IS NOT NULL
	drop procedure dbo.up_sms_remetentebyid
go

create procedure dbo.up_sms_remetentebyid
@site varchar(30)

/* WITH ENCRYPTION */ 
AS

	select 
		ltrim(rtrim(ISNULL(textValue,''))) as textValue
	from 
		empresa (nolock)
	inner join 
		b_parameters_site (nolock) on b_parameters_site.site = empresa.site
	where 
		no = @site
		and stamp = 'ADM0000000026'

GO
Grant Execute on dbo.up_sms_remetentebyid to Public
Grant Control on dbo.up_sms_remetentebyid to Public
GO