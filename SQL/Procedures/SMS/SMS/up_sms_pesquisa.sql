-- Pesquisa de SMS
-- exec up_sms_pesquisa 100, '', '', '19000101', '30000101', 0, 'Loja 1'

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_pesquisa]') IS NOT NULL
    drop procedure dbo.up_sms_pesquisa
go

create procedure dbo.up_sms_pesquisa

@top		int,
@ref		varchar(7),
@design		varchar(200),
@de			datetime,
@a			datetime,
@emitido	bit,
@site		varchar(20)

/* WITH ENCRYPTION */
AS


SELECT TOP (@top) 
    CONVERT(bit,0) as sel,
    'Ref'		= b_sms.ref,
    'Design'	= b_sms.designacao,
    'ValEntre'	= convert(varchar,b_sms.CampValDe,105) + ' a ' + convert(varchar,b_sms.CampValA,105),
    'Emitido'	= b_sms.emitido
from 
	b_sms (nolock) 
where 
	b_sms.ref like @ref+'%'
      and b_sms.designacao like @design+'%'
      and b_sms.CampValDe >= @de 
      and b_sms.CampValA <= @a
      and b_sms.emitido =(case when @emitido = 1 then @emitido else b_sms.emitido end)
	  and site=@site

GO
Grant Execute on dbo.up_sms_pesquisa to Public
Grant Control on dbo.up_sms_pesquisa to Public
go