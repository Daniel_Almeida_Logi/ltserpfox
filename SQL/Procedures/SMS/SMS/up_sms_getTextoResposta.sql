/*
	texto resposta Notificações
	
	exec up_sms_getTextoResposta
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_getTextoResposta]') IS NOT NULL
	drop procedure dbo.up_sms_getTextoResposta
go

create procedure dbo.up_sms_getTextoResposta

/* WITH ENCRYPTION */ 
AS

	select top 1
		texto
		,activo
	from
		b_res_notificacoes (nolock)

GO
Grant Execute on dbo.up_sms_getTextoResposta to Public
Grant Control on dbo.up_sms_getTextoResposta to Public
GO