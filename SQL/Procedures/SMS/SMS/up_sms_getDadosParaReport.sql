/*
	levantar dados para report
	
	exec up_sms_getDadosParaReport 'ADM12051482026.900287543'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_getDadosParaReport]') IS NOT NULL
	drop procedure dbo.up_sms_getDadosParaReport
go

create procedure dbo.up_sms_getDadosParaReport
	@stamp	char(25)

/* WITH ENCRYPTION */ 
AS

	select
		ltrim(rtrim(b_utentes.tlmvl)) tlmvl
		,B_smscl.*  
	from
		b_smscl					(nolock)
		inner join b_utentes	(nolock) on b_utentes.utstamp=B_smscl.clstamp 
	where
		B_smscl.smsstamp = @stamp

GO
Grant Execute on dbo.up_sms_getDadosParaReport to Public
Grant Control on dbo.up_sms_getDadosParaReport to Public
GO