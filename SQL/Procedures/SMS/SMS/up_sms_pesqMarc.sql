-- Pesquisa Marca��es para envio de SMS
-- exec up_sms_pesqMarc
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_pesqMarc]') IS NOT NULL
    drop procedure dbo.up_sms_pesqMarc
go

create procedure dbo.up_sms_pesqMarc

/* WITH ENCRYPTION */
AS

select 
	b_utentes.tlmvl [idtlm], marcacoes.* 
from 
	marcacoes (nolock)
	inner join b_utentes (nolock) on b_utentes.utstamp = marcacoes.utstamp
where 
	b_utentes.notif=1
	and b_utentes.tlmvl!=''
	and convert(date,marcacoes.data,101) = convert(date,DATEADD(day,(select top 1 dias from b_cli_notificacoes),GETDATE()),101)
	and marcacoes.estado in ('MARCADO','')
	and left(convert(time,getdate()),5) between (select top 1 horaini from b_cli_notificacoes (nolock)) and (select top 1 horafinal from b_cli_notificacoes (nolock))

GO
Grant Execute on dbo.up_sms_pesqMarc to Public
Grant Control on dbo.up_sms_pesqMarc to Public
go