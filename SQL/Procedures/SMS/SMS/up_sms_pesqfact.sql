-- Pesquisa de Campanhas para Factura��o
-- �ltima altera��o : 2012-06-05
-- exec up_sms_pesqfact 'F01064A',-1,-1,'','19000101', '30000101'

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_sms_pesqfact]') IS NOT NULL
    drop procedure dbo.up_sms_pesqfact
go

create procedure dbo.up_sms_pesqfact

@username	varchar(50),
@total		int,
@status		int,
@name		varchar(250),
@dataDe		datetime,
@dataA		datetime

/* WITH ENCRYPTION */
AS

select 
	username
	,total
	,status
	,name as 'nome'
	,[dataenvio] = convert(date,dataenvio,101)
	,horaenvio
from 
	b_sms_fact (nolock)
where
	username like case when @username='' then username else '%'+@username+'%' end
	and total = case when @total>=0 then @total else total end
	and status = case when @status>=0 then @status else status end
	and name like case when @name='' then name else '%'+@name+'%' end
	and dataenvio between @dataDe and @dataA
order by 
	dataenvio desc
	,horaenvio desc

GO
Grant Execute on dbo.up_sms_pesqfact to Public
Grant Control on dbo.up_sms_pesqfact to Public
go