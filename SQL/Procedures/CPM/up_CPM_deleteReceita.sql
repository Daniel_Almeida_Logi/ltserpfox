/* exec up_CPM_deleteReceita '123456789'*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_CPM_deleteReceita]') IS NOT NULL
	drop procedure up_CPM_deleteReceita
go

create PROCEDURE up_CPM_deleteReceita
@stamp varchar(25)

/* WITH ENCRYPTION */ 
AS 
BEGIN 
	delete b_cli_CPM_diplomas
	where stampLinha in (
		select stampLinha from b_cli_CPM_linhaReceita (nolock) 
		where stampCab in (select stamp from b_cli_CPM_cabecalho (nolock) where numReceita=@stamp)
	)

	delete from b_cli_CPM_linhaReceita
	where stampCab in (
		select stamp from b_cli_CPM_cabecalho (nolock) where numReceita=@stamp
	)

	delete from b_cli_CPM_cabecalho where numReceita=@stamp
END

GO
Grant Execute On up_CPM_deleteReceita to Public
Grant Control On up_CPM_deleteReceita to Public
GO
