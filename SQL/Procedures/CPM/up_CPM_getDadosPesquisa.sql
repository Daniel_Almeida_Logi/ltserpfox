/* ---SP's para o programa java CPM--- */ 

/* exec up_CPM_getDadosPesquisa '123456789'*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_CPM_getDadosPesquisa]') IS NOT NULL
	drop procedure up_CPM_getDadosPesquisa
go

create PROCEDURE up_CPM_getDadosPesquisa
@stamp varchar(25)

/* WITH ENCRYPTION */ 
AS 
BEGIN 
	SELECT 
		*
	FROM 
		b_cli_CPM (nolock)
	WHERE 
		stamp = @stamp
END

GO
Grant Execute On up_CPM_getDadosPesquisa to Public
Grant Control On up_CPM_getDadosPesquisa to Public
GO

