/* 
	valida se os motivos enviados para a falta de productos s�o validos

	exec up_associados_validaRazaoFaltaProducto '100', '101'
	select * from  ext_missing_products mp (nolock)

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[up_associados_validaRazaoFaltaProducto]') IS NOT NULL
	drop procedure dbo.up_associados_validaRazaoFaltaProducto
go

create procedure dbo.up_associados_validaRazaoFaltaProducto

@codeDistruidor		int,
@codeTaim		    int


/* WITH ENCRYPTION */
AS
SET NOCOUNT ON
	
	declare @total int = 0

	select
		@total = count(*) 
	from
		ext_missing_products_reason mp (nolock)
	where
		mp.code = @codeDistruidor
	


	select
		@total = @total + count(*) 
	from
		ext_missing_products_reason mp (nolock)
	where
		mp.code = @codeTaim

	
	select @total as total
	
Go
Grant Execute on dbo.up_associados_validaRazaoFaltaProducto to Public
Grant Control on dbo.up_associados_validaRazaoFaltaProducto to Public
Go