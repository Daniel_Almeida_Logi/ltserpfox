/* 
	devolve a quantidade vendida de um determinado mes de um tipo 

	exec up_relatorio_gestao_comunicacao '', '', '',''


*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_relatorio_gestao_comunicacao]') IS NOT NULL
	drop procedure dbo.up_relatorio_gestao_comunicacao
go

create procedure dbo.up_relatorio_gestao_comunicacao

@ano		int,
@mes		int,
@infarmed	varchar(20),
@tipo		varchar(10)


/* WITH ENCRYPTION */
AS
SET NOCOUNT ON
	DECLARE @tipoInt int;
	set @tipoInt = case when @tipo ='' then 0 else convert(int, rtrim(ltrim(@tipo))) END

	select
		b_utentes.id, 
		b_utentes.nome,
		qtt as quantidade,
		comm.month AS mes ,
		comm.year AS ano
	from
		ext_esb_communications comm (nolock)
		INNER JOIN b_utentes (nolock) ON REPLACE(LTRIM(REPLACE(comm.senderId ,'0',' ')),' ','0') = b_utentes.id 
	where
		comm.month = case when @mes = '' then comm.month else @mes end 
		and comm.year = case when @ano = '' then comm.year else @ano end
		and comm.senderId = case when @infarmed = '' then comm.senderId else @infarmed end
		and comm.comunicationType =  case when @tipoInt = 0 then comm.comunicationType else @tipo end 
		and valid = 1 and test = 0 
	order by 
		comm.year desc ,
		comm.month desc
	
	
Go
Grant Execute on dbo.up_relatorio_gestao_comunicacao to Public
Grant Control on dbo.up_relatorio_gestao_comunicacao to Public
Go