/* Resumo Fatura��o Associados

	exec up_resumo_retificado_associados 2015, 5, 1 , 0

	exec up_resumo_retificado_associados 2017, 5, 19, 0
	exec up_resumo_retificado_associados 2017 ,1, 7.00 , 1

						exec up_resumo_retificado_associados 2017 ,2, 7.00 , 1, 1

	select * from cl_fatc_org where nrdocimp = 100768

						exec up_resumo_retificado_associados '2017', '2', '7', 0, 0
	exec up_resumo_retificado_associados '2017', '2', '7', 0, '100768'

	select * from cl_fatc_org where nrdocimp = 100768

	exec up_resumo_retificado_associados '0', '0', '0', 0, '11768 '

	exec up_associados_dadosEntidades 0 , 3, '2017.02.02', '', 1, '11768', 1
	exec up_associados_dadosEntidades '28', 1, '2017.06.26', '', 1, '100741', 0

						exec up_resumo_retificado_associados '0', '0', '0', 0, '11768 '
											exec up_resumo_retificado_associados '0', '0', '7', 0, '11768 '.


																exec up_resumo_retificado_associados '0', '0', '0', 0, '100768'
	exec up_associados_dadosEntidades 0 , 3, '', '', 1, '11768', 0

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_resumo_retificado_associados]') IS NOT NULL
	drop procedure dbo.up_resumo_retificado_associados
go

create procedure dbo.up_resumo_retificado_associados
	@ano			as numeric(4)
	,@mes			as numeric(4)
	,@nr_cl_org		as numeric(10)
	,@dataDados		as bit
	,@nrDocImp  as varchar(10)

/* WITH ENCRYPTION */

AS

	IF @dataDados = 0
		BEGIN

			select
				 descr = 'Ref. Resumo Faturas Ref: ' + upper(left(datename(month, DateAdd(month , mes-1 , 0)),3)) +  + '/' + convert(varchar,ano)
				,referencia = upper(left(datename(month, DateAdd(month , mes-1 , 0)),3)) +  + '/' + convert(varchar,ano)
				,total = sum(valor_ret)
				,cl.nome, cl.nome2, cl.morada, cl.codpost, cl.ncont, cl.telefone, cl.fax
				,tipo = case when sum(valor_ret) < 0 then  'C' ELSE 'D' END
				,nr_org = cl.no
			from
				cl_fatc_org cfo (nolock)
				inner join b_utentes cl (nolock) on cl.no = cfo.nr_cl_org
			where
				cfo.nr_cl_org = case when @nr_cl_org != 0 then @nr_cl_org else cfo.nr_cl_org end
				--cfo.nr_cl_org = @nr_cl_org
				and cfo.ano = case when @nrDocImp = '' then @ano else cfo.ano end
				and cfo.mes = case when @nrDocImp = '' then @mes else cfo.mes end
				and nrDocImp = case when @nrDocImp = '' then 0 else @nrDocImp end
			group by
				ano, mes
				,cl.nome, cl.nome2, cl.morada, cl.codpost, cl.ncont, cl.telefone, cl.fax, cl.no
		END
	ELSE
		BEGIN

			select
				 descr = 'Ref. Resumo Faturas Ref: ' + upper(left(datename(month, DateAdd(month , mes_tratamento-1 , 0)),3)) +  + '/' + convert(varchar,ano_tratamento)
				,referencia = upper(left(datename(month, DateAdd(month , mes_tratamento-1 , 0)),3)) +  + '/' + convert(varchar,ano_tratamento)
				,total = sum(valor_ret)
				,cl.nome, cl.nome2, cl.morada, cl.codpost, cl.ncont, cl.telefone, cl.fax
				--,tipo = 'C'
				,tipo = case when sum(valor_ret) < 0 then  'C' ELSE 'D' END
				,nr_org = cl.no
			from
				cl_fatc_org cfo (nolock)
				inner join b_utentes cl (nolock) on cl.no = cfo.nr_cl_org
			where
				cfo.nr_cl_org = case when @nr_cl_org != 0 then @nr_cl_org else cfo.nr_cl_org end
				--cfo.nr_cl_org = @nr_cl_org
				and cfo.ano_tratamento = case when @nrDocImp = '' then @ano else cfo.ano_tratamento end 
				and cfo.mes_tratamento = case when @nrDocImp = '' then @mes else cfo.mes_tratamento end
				and nrDocImp = case when @nrDocImp = '' then 0 else @nrDocImp end
			group by
				ano_tratamento, mes_tratamento
				,cl.nome, cl.nome2, cl.morada, cl.codpost, cl.ncont, cl.telefone, cl.fax, cl.no
		END 

GO
Grant Execute On dbo.up_resumo_retificado_associados to Public
Grant Control On dbo.up_resumo_retificado_associados to Public
Go