/* 

Listar Movimentos para encontro de Contas de Clientes
exec up_pesqdocassociados_movclientes '', 0, '', 'Farm�cia PenaCova', 0, 0, '', 0, 2014, 11

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_pesqdocassociados_movclientes]') IS NOT NULL
	drop procedure dbo.up_pesqdocassociados_movclientes
go

create procedure dbo.up_pesqdocassociados_movclientes

@nome_org			varchar(55),
@nr_org				numeric(10),
@id_org				varchar(10),
@nome_cl			varchar(55),
@nr_cl				numeric(10),
@dep_cl				numeric(3),
@id_cl				varchar(10),
@nrdoc				varchar(20),
@ano				smallint,
@mes				tinyint 

/* WITH ENCRYPTION */

AS 

SET NOCOUNT ON

	select
		sel						= convert(bit,0) 
		,cfo_id					= cfo.id 
		,fi_id					= ''
		,nome					= 'Factura Entidade ' + Entidades.nome
		,valor_Movimento		= valor_pagar
		,valor_Regularizado		= valor_pago
		,valor_Regularizar		= valor_pagar - valor_pago
		,ano					= cfo.ano
		,mes					= cfo.mes
	from
		cl_fatc_org cfo (nolock)
		left join B_utentes as cl (nolock) on cl.id = cfo.id_cl
		left join B_utentes as Entidades (nolock) on Entidades.no = cfo.nr_cl_org
	where 
		--valor_pago != valor_pagar
		--AND Entidades.nome LIKE '%'+@nome_org+'%'
		Entidades.nome LIKE '%'+@nome_org+'%'
		AND	nr_cl_org = CASE WHEN @nr_org = 0 THEN nr_cl_org ELSE @nr_org END
		AND	Entidades.id = CASE WHEN @id_org = '' THEN Entidades.id ELSE @id_org END
		AND cl.nome LIKE '%'+ @nome_cl+'%'
		AND cl.no = CASE WHEN @nr_cl = 0 THEN cl.no ELSE @nr_cl END
		AND id_cl = CASE WHEN @id_cl = '' THEN id_cl ELSE @id_cl END
		AND nrDoc = CASE WHEN @nrdoc = 0 THEN nrDoc ELSE @nrdoc END
		AND cfo.ano = CASE WHEN @ano = 0 THEN cfo.ano else @ano END
		AND cfo.mes = CASE WHEN @mes = 0 THEN cfo.mes ELSE @mes END
	
UNION ALL
	
	select
		sel						= convert(bit,0) 
		,cfo_id					= '00000000-0000-0000-0000-0000000000000000'
		,fi_id					= ''
		,nome					= fi.design
		,valor_Movimento		= fi.epv
		,valor_Regularizado		= 0
		,valor_Regularizar		= fi.epv - 0
		,ano					= ft.ftano
		,mes					= month(ft.fdata)
	from
		fi (nolock)
		inner join ft (nolock) on fi.ftstamp = ft.ftstamp
		inner join cc (nolock) on cc.ftstamp=ft.ftstamp
	where
		ft.no = CASE WHEN @nr_cl = 0 THEN ft.no ELSE @nr_cl END
		AND ft.estab = CASE WHEN @dep_cl = 0 THEN ft.estab ELSE @dep_cl END
		AND ft.ftano = CASE WHEN @ano = 0 THEN ft.ftano ELSE @ano END
		AND MONTH(ft.fdata) = CASE WHEN @mes = 0 THEN MONTH(ft.fdata) ELSE @mes END 
		AND ft.nome like '%' + @nome_cl + '%'
		AND (abs((cc.ecred-cc.ecredf)-(cc.edeb-cc.edebf)) > 0.010000)
	order by 
		cfo_id,ano, mes

GO
Grant Execute On dbo.up_pesqdocassociados_movclientes to Public
Grant Control On dbo.up_pesqdocassociados_movclientes to Public
Go

		