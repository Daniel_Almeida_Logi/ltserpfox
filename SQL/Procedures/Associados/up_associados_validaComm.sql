/* 
	valida se j� existe uma comunica��o para esse intervalo de tempo (ano,mes) para uma determinada entidade

	exec up_associados_validaComm '2019', '05', '90001', 'pts'

	select * from  ext_esb_communications comm (nolock)

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_associados_validaComm]') IS NOT NULL
	drop procedure dbo.up_associados_validaComm
go

create procedure dbo.up_associados_validaComm

@ano		int,
@mes		int,
@codigo		varchar(20),
@entidade	varchar(20)


/* WITH ENCRYPTION */
AS
SET NOCOUNT ON


	set @codigo =   REPLACE(LTRIM(REPLACE(@codigo,'0',' ')),' ','0') 

	select
		"total" = count(*) 
	from
		ext_esb_communications comm (nolock)
	where
		comm.month = @mes 
		and comm.year = @ano
		and comm.recevierId = @entidade
		and  REPLACE(LTRIM(REPLACE(comm.senderId ,'0',' ')),' ','0')  = @codigo
		and valid = 1 and test = 0
	
	
Go
Grant Execute on dbo.up_associados_validaComm to Public
Grant Control on dbo.up_associados_validaComm to Public
Go