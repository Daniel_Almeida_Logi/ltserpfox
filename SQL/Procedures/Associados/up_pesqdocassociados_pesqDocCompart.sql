/* Pesquisar Documentos Associados


	exec up_pesqdocassociados_pesqDocCompart '2'

	--select totalEFR, vatNr_ext_entity, vatNr_ext_pharmacy, year, month, * from ext_esb_doc
	select *  from cl_fatc_org where ano_tratamento=2018 and mes_tratamento=8
	select *  from cl_fatc_org where ano=2018 and mes=9
	select * from b_utentes where ncont='505372339'
	select * from b_utentes where ncont='513696628'

*/

--select *from ext_esb_doc where tipoDoc = 2 and test = 0 and invalid=0 
--and export=0 order by ousrdata desc


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_pesqdocassociados_pesqDocCompart]') IS NOT NULL
	drop procedure dbo.up_pesqdocassociados_pesqDocCompart
go

create procedure dbo.up_pesqdocassociados_pesqDocCompart
@tipoDoc INT

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

Begin 
	select
		id_cl = cl.id
		,nr_cl_org = entidades.no
		,ano = (CASE 
					WHEN eed.tipoDoc = 2
						THEN YEAR(DATEADD(MONTH, -1, docDate))
					ELSE
						--eed.year
						YEAR(DATEADD(MONTH, -1, getDate()))
				END)
		 
		,mes = (CASE 
					WHEN eed.tipoDoc = 2
						THEN MONTH(DATEADD(MONTH, -1, docDate))
					ELSE
						--eed.month
						MONTH(DATEADD(MONTH, -1, GETDATE()))
				END)
		,ano_tratamento = (CASE 
							WHEN eed.tipoDoc = 2
								THEN ISNULL((select TOP 1 YEAR(ext.startPeriodDate) from ext_esb_doc(nolock) as ext where ext.docNr = eed.oDocNr and ext.docdate = eed.oDocDate and ext.id_ext_pharmacy = eed.id_ext_pharmacy and ext.tipoDoc = 1), 1900)
							ELSE
								year(eed.startPeriodDate) 
						  END)
		,mes_tratamento = (CASE 
							WHEN eed.tipoDoc = 2
								THEN ISNULL((select TOP 1 MONTH(ext.startPeriodDate) from ext_esb_doc(nolock) as ext where ext.docNr = eed.oDocNr and ext.docdate = eed.oDocDate and ext.id_ext_pharmacy = eed.id_ext_pharmacy and ext.tipoDoc = 1), 1)
							ELSE
								month(eed.startPeriodDate)
						  END)
		,valor = (case when eed.tipodoc=1 or eed.tipodoc=3 then eed.totalEFR else 0 end)
		,valor_ret = (case when eed.tipodoc=2 then eed.totalEFR*(-1) else 0 end)
		,valor_pagar = (case when eed.tipodoc=1 or eed.tipodoc=3 then eed.totalEFR else eed.totalEFR*(-1) end)
		,eed.stamp
		,eed.id_ext_efr
		,eed.docNr
		--,valor = sum(case when eed.tipodoc=1 or eed.tipodoc=3 then eed.totalEFR else 0 end)
		--,valor_ret = sum(case when eed.tipodoc=2 then eed.totalEFR*(-1) else 0 end)
		--,valor_pagar = sum(case when eed.tipodoc=1 or eed.tipodoc=3 then eed.totalEFR else eed.totalEFR*(-1) end)
	from
		ext_esb_doc eed (nolock)
	
		left join B_utentes as cl (nolock) on cl.ncont = eed.vatNr_ext_pharmacy and cl.no>200 and inactivo=0
	    left join B_utentes as Entidades (nolock) on Entidades.ncont = 	REPLACE(eed.vatNr_ext_entity, 'PT', '') and Entidades.no<198 and Entidades.inactivo=0
		left join cptorg(nolock) on LTRIM(RTRIM(eed.code_ext_entity)) =  LTRIM(RTRIM(cptorg.id_anf))
	where 
		eed.export = 0
		and eed.invalid = 0 
		and eed.test=0
		and Entidades.no = cptorg.u_no
		and id_ext_pharmacy = ltrim(ltrim(cl.id))
		and eed.tipoDoc = @tipoDoc
		and 1 = (CASE 
					WHEN @tipoDoc <> 1
						THEN (CASE WHEN eed.id_ext_efr in ('018','014','008','014','019','062','068','070','139', '054','011','021') 
						THEN 1 ELSE 0 END)
					ELSE
						1
				END)
		and eed.send = (CASE 
							WHEN cptorg.validExtDoc = 1
								THEN 1
							ELSE
								eed.send
						END)
	--group by
	--	cl.id, Entidades.no, eed.year, eed.month, year(eed.startPeriodDate), month(eed.startPeriodDate)
	order by
		cl.id, Entidades.no, eed.year, eed.month, year(eed.startPeriodDate), month(eed.startPeriodDate)
End

GO
Grant Execute On dbo.up_pesqdocassociados_pesqDocCompart to Public
Grant Control On dbo.up_pesqdocassociados_pesqDocCompart to Public
