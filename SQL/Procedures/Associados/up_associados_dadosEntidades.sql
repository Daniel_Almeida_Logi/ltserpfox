/* Dados Entidade e Numero de documento

	exec up_associados_dadosEntidades '8', 4, '2016.12.10', '20161110', 0, '', 5
	
	Tipo: 1 - Resumo Facturas Entidades; Tipo 2 - Nota Lan�amento Associados; Tipo 3 - Aviso Lan�amento Entidades; Tipo 4 - Recibos

	
	select * from doc_associados where dia = 29  and mes_dados = 5 order by nrdoc 
	select * from cl_fatc_org where nrdoc in (39051, 39052)


	--update cl_fatc_org set nrdocimp = 0 where nrdoc = 39047
	select * from tesouraria_mov where nrdocimp = 100785

							exec up_associados_dadosEntidades 33, 3, '2017.06.29', '20170529', 0, '', 0



	exec up_associados_dadosEntidades 7, 3, '2017.06.29', '20170529', 0, '', 0

							exec up_associados_dadosEntidades 15, 3, '2017.06.29', '20170429', 0, '', 0

	select *  from doc_associados


											exec up_associados_dadosEntidades 0 , 3, '', '', 1, '100798', 0
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_associados_dadosEntidades]') IS NOT NULL
	drop procedure dbo.up_associados_dadosEntidades
go

create procedure dbo.up_associados_dadosEntidades
	 @no		as numeric(9,3)
	,@tipo		as smallint
	,@data		as varchar(10)
	,@dataRef	as varchar(10)
	,@imprimir	as bit
	,@nrDocImp  as varchar(10)
	,@tesourariaMovId as int 

/* WITH ENCRYPTION */

AS
	set language portuguese
	declare @nrDoc as numeric(9,0)
	declare @nDoc  as smallint
	set		@nDoc = @tipo
	declare @nrMovConta as numeric(9,0)
	Set @nrDoc = (select nrdoc from doc_associados where tesouraria_mov_id = @tesourariaMovId and tesouraria_mov_id != 0)
	IF @imprimir = 0
	BEGIN
		-- condicao que validade se vai inserir novo linha de numera��o na tabela de 
		IF not exists (select * from doc_associados where tesouraria_mov_id = @tesourariaMovId and @tesourariaMovId != 0 AND @nrDocImp = case when @nrDocImp = '' then 0 else @nrDocImp end and ndoc = 4)
		BEGIN
			Set @nrDoc = isnull((select MAX(nrdoc) from doc_associados where ndoc = @nDoc),0) + 1
			-- insere o n� do documento na tabela que controla a numeracao dos documentos
			insert into doc_associados (
				ano
				,mes
				,dia
				,ndoc
				,nrdoc
				,id_cl
				,ano_dados
				,mes_dados
				,tesouraria_mov_id
			) values(
				YEAR(@data)
				,MONTH(@data)
				,DAY(@data)
				,@nDoc
				,@nrDoc
				,@no
				,YEAR(@dataRef)
				,MONTH(@dataRef)
				,@tesourariaMovId
			)
			-- atualiza linha do movimento com o nr de recibo criado
			IF @ndoc = 4
			BEGIN
				set @nrMovConta = (select top 1 id from tesouraria_mov order by id desc)
				update tesouraria_mov set nrDocImp = @nrDoc where id = @nrMovConta
			END
		END 
	END
	Select 
		top 1 nome
		,no
		,morada
		,codpost
		,ncont
		,local
		,nrdoc = case when @ndoc = 4 then 
										case when @nrDocImp = '' then @nrDoc else @nrDocImp END
					  when @ndoc = 3 and @imprimir = 0 then @nrDoc
										else isnull(doc_associados.nrdoc,0) end
		,abrev = nome2
		,data = case when @data != '' then @data else (
											select 
												top (1) convert(varchar,doc_associados.ano) + '.' 
												+ right ('00'+ltrim(str(doc_associados.mes)),2 ) + "."
												+ case when doc_associados.dia is not null then right ('00'+ltrim(str(doc_associados.dia)),2 ) else "01" END
											from 
												doc_associados (nolock) 
											where 
												doc_associados.nrdoc = @nrDocImp
												) END
		,ref = convert(varchar,MONTH(@dataRef)) + '/' + Upper(DATENAME(month,@dataRef))
		,ano_dados
		,mes_dados
		,  doc_associados.ndoc
	FROM 
		b_utentes (nolock) 
		left join doc_associados on B_utentes.no = doc_associados.id_cl 
		
	WHERE 
		b_utentes.no				 = case when @no = 0 then b_utentes.no else @no END
		-- caso o documento seja tipo 3 � aviso lan�amento e n�o tendo nrdoc deve ignorar nrdoc devovido do queryset
		and doc_associados.nrdoc	 = case 
											when @nrDocImp = ''  and @tipo = 3 and @imprimir = 1 then 0 
											when @nrDocImp = '' then doc_associados.nrdoc 
											else @nrDocImp 
										END
		and doc_associados.ano		 = case when @data = '' then doc_associados.ano else YEAR(@data) END
		and doc_associados.mes		 = case when @data = '' then doc_associados.mes else MONTH(@data) END
		and doc_associados.ano_dados = case when @dataRef = '' then doc_associados.ano_dados else YEAR(@dataRef) END  
		and doc_associados.mes_dados = case when @dataRef = '' then doc_associados.mes_dados else MONTH(@dataRef) END
		and doc_associados.ndoc		 = @tipo
		

GO
Grant Execute On dbo.up_associados_dadosEntidades to Public
Grant Control On dbo.up_associados_dadosEntidades to Public
Go

