/* 
	exec up_associados_ccAssociados 208, '20150101', '20191010', '1'
	
	select * from tesouraria_mov
	select top 5 * from doc_associados where ndoc = 4
	select top 5 * from cl_fatc_org
	

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_associados_ccAssociados]') IS NOT NULL
	drop procedure dbo.up_associados_ccAssociados
go

create procedure dbo.up_associados_ccAssociados

@no_cl		numeric(10),
@Dataini	datetime,
@Datafim	datetime,
@site		varchar(20) 

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON
	
	/* Prepara Result Set Final*/		
	declare 
		 @SALDOANTERIOR as numeric(10,3)
		 ,@id_cl as varchar(10)
	 
	set	@SALDOANTERIOR = ISNULL((Select SUM(valor_pagar) - SUM(valor_regularizado) from cl_fatc_org where nr_cl_org = @id_cl AND  (select DATEADD(mm, (cl_fatc_org.ano - 1900) * 12 + cl_fatc_org.mes - 1 , 1)) < @Dataini),0)
	set	@id_cl = ISNULL((select id from b_utentes where no = @no_cl),'')

	-- 	
	select
		'SALDOANTERIOR'		=	@SALDOANTERIOR
		,'NO'				=	b_utentes.no
		,'NOME'				=	b_utentes.nome 
		,'CMDESC'			=	'Recibo Pag. Entidade'
		,'ADOC'				=	isnull(doc_associados.nrdoc, 0)
		,'DOCDATA'			=	convert(date,tesouraria_mov.data)
		,'DOCINTRO'			=	convert(date,tesouraria_mov.data)
		,'ECRED'			=	tesouraria_mov.valor
		,'EDEB'				=	0
		,'USERNAME'			=	UPPER((Select top 1 username from b_us where tesouraria_mov.ousrinis = b_us.iniciais))
		,'OUSRINIS'			=	tesouraria_mov.ousrinis
		,'ORIGEM'			=	tesouraria_mov.obs
	from
		tesouraria_mov (nolock)
		inner join b_utentes (nolock) on tesouraria_mov.no = b_utentes.no
		left join doc_associados (nolock) on tesouraria_mov.no = doc_associados.id_cl AND tesouraria_mov.nrDocImp = doc_associados.nrDoc
	where
		tesouraria_mov.no = @no_cl	
		and tesouraria_mov.data between @Dataini and @Datafim
	
union all

	select
		'SALDOANTERIOR'		=	@SALDOANTERIOR
		,'NO'				=	b_utentes.no
		,'NOME'				=	b_utentes.nome 
		,'CMDESC'			=	'Fatura Far. Entidade'
		,'ADOC'				=	cl_fatc_org.nrDoc 
		,'DOCDATA'			=	convert(date,(select DATEADD(mm, (cl_fatc_org.ano - 1900) * 12 + cl_fatc_org.mes - 1 , 1)))
		,'DOCINTRO'			=	convert(date,cl_fatc_org.data)
		,'ECRED'			=	0
		,'EDEB'				=	cl_fatc_org.valor_pagar
		,'USERNAME'			=	UPPER((Select top 1 username from b_us where cl_fatc_org.ousrinis = b_us.iniciais))
		,'OUSRINIS'			=	cl_fatc_org.ousrinis
		,'ORIGEM'			=	case when  convert(varchar,cl_fatc_org.nrDocImp) = 0 then '' ELSE 'Ref. Documento N�: ' + convert(varchar,cl_fatc_org.nrDocImp) END
	from
		cl_fatc_org (nolock)
		inner join b_utentes (nolock) on cl_fatc_org.nr_cl_org = b_utentes.no
	where
		cl_fatc_org.id_cl = @id_cl	
		and (select DATEADD(mm, (cl_fatc_org.ano - 1900) * 12 + cl_fatc_org.mes - 1 , 1)) between @Dataini and @Datafim
	order by DOCDATA asc, ADOC asc
	
Go
Grant Execute on dbo.up_associados_ccAssociados to Public
Grant Control on dbo.up_associados_ccAssociados to Public
Go