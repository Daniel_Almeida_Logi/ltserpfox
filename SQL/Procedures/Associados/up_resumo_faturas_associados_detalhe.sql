/*
	Resumo Fatura��o Associados

	exec up_resumo_faturas_associados_detalhe 2015, 5, 395, 0, '', ''  
*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_resumo_faturas_associados_detalhe]') IS NOT NULL
	drop procedure dbo.up_resumo_faturas_associados_detalhe
go

create procedure dbo.up_resumo_faturas_associados_detalhe
	@ano as numeric(4)
	,@mes as numeric(4)
	,@nr_cl as numeric(10)
	,@dep_cl as numeric(3)
	,@id_cl as varchar(10)
	,@nome_cl varchar(55)

/* WITH ENCRYPTION */

AS

	select
		fi.design
		,credito =  case when fi.etiliquido<0 then fi.etiliquido*(-1) else convert(numeric(13,3),0) end
		,debito =  case when fi.etiliquido>0 then fi.etiliquido else convert(numeric(13,3),0) end
		,fi.fistamp
		,ft.ndoc
		,ft.nmdoc
		,ft.fdata
		,mes = MONTH(ft.fdata)
		,ano = YEAR(ft.fdata)
		,nr_cl = ft.no
		,estab_cl = ft.estab
		,B_utentes.id
		,B_utentes.nome
		,B_utentes.morada
		,B_utentes.codpost
		,B_utentes.local
		,ft.ftstamp
	from
		fi (nolock)
		inner join ft (nolock) on ft.ftstamp = fi.ftstamp
		inner join B_utentes (nolock) on ft.no = B_utentes.no and ft.estab = B_utentes.estab
		inner join cc (nolock) on cc.ftstamp=ft.ftstamp
	where
		ft.no = CASE WHEN @nr_cl = 0 THEN ft.no ELSE @nr_cl END
		AND ft.estab = CASE WHEN @dep_cl = -1 THEN ft.estab ELSE @dep_cl END
		AND ft.ftano = CASE WHEN @ano = 0 THEN ft.ftano ELSE @ano END
		AND MONTH(ft.fdata) = CASE WHEN @mes = 0 THEN MONTH(ft.fdata) ELSE @mes END 
		AND fi.etiliquido != 0
		AND ft.nome like '%' + @nome_cl + '%'
		AND	B_utentes.id = case when @id_cl = '' THEN B_utentes.id ELSE @id_cl end
		AND (abs((cc.ecred-cc.ecredf)-(cc.edeb-cc.edebf)) > 0.010000)
		AND ft.ndoc != 77
		
	order by 
		ft.fdata

GO
Grant Execute On dbo.up_resumo_faturas_associados_detalhe to Public
Grant Control On dbo.up_resumo_faturas_associados_detalhe to Public
Go