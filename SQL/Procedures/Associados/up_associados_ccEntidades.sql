/* 
	exec up_associados_ccEntidades 9, '20100101', '20191010', '1'
	
	select * from cl_fatc_org where  nrdocimp = 0 and ano = 2016

	**	Tipo: 1 - Resumo Facturas Entidades; 2 - Nota Lanšamento Associados; 3 - Aviso Lanšamento 
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_associados_ccEntidades]') IS NOT NULL
	drop procedure dbo.up_associados_ccEntidades
go

create procedure dbo.up_associados_ccEntidades

@nr_cl_org	NUMERIC(10),
@Dataini	datetime,
@Datafim	datetime,
@site		varchar(20) 

/* WITH ENCRYPTION */
AS
SET NOCOUNT ON
	
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosResumoFtAgrupados'))
		DROP TABLE #dadosResumoFtAgrupados
		
	select 
		'ADOC'		= isnull(co.nrDocImp,'')
		,'EDEB'		= sum(co.valor_pagar)
		,'NOENT'	= co.nr_cl_org
		,'USRINIS'  = (select top 1 usrinis from cl_fatc_org (nolock) where nrDocImp=co.nrDocImp) 

	into 
		#dadosResumoFtAgrupados
	from 
		cl_fatc_org co (nolock)
	where
		co.nr_cl_org = @nr_cl_org	
		and (select DATEADD(mm, (co.ano - 1900) * 12 + co.mes - 1 , 1)) between @Dataini and @Datafim
	group by 
		co.nrDocImp, co.nr_cl_org
	order by 
		co.nrDocImp  asc

	/* Prepara Result Set Final*/		
	declare 
		 @SALDOANTERIOR as numeric(10,3), @saldoEdebTotal as numeric(10,3), @saldoEcredTotal as numeric(10,3)
	
	--set @SALDOANTERIOR = ISNULL((Select SUM(valor_pagar) - SUM(valor_regularizado) from cl_fatc_org where nr_cl_org = @nr_cl_org AND  (select DATEADD(mm, (cl_fatc_org.ano - 1900) * 12 + cl_fatc_org.mes - 1 , 1)) < @Dataini),0)
	set @SALDOANTERIOR = ISNULL((Select SUM(valor_pagar) from cl_fatc_org where nr_cl_org = @nr_cl_org AND (select DATEADD(mm, (cl_fatc_org.ano - 1900) * 12 + cl_fatc_org.mes - 1 , 1)) < @Dataini),0)
	set @SALDOANTERIOR = ISNULL(@SALDOANTERIOR - (select sum(valor) from tesouraria_mov where no = @nr_cl_org AND data < @dataini),0)

	set @saldoEdebTotal = (Select SUM(valor_pagar) from cl_fatc_org where nr_cl_org = @nr_cl_org)
	set @saldoEcredTotal = (select sum(valor) from tesouraria_mov where no = @nr_cl_org)

	-- 	
	select
		'SALDOANTERIOR'		=	@SALDOANTERIOR
		,'NO'				=	b_utentes.no
		,'NOME'				=	b_utentes.nome
		,'CMDESC'			=	'Recibo Pag. Entidade'
		,'ADOC'				=	isnull(doc_associados.nrdoc, 0)
		,'DOCDATA'			=	convert(date,tesouraria_mov.data)
		,'DOCINTRO'			=	convert(date,tesouraria_mov.data)
		,'ECRED'			=	tesouraria_mov.valor
		,'EDEB'				=	0
		,'USERNAME'			=	UPPER((Select top 1 username from b_us where tesouraria_mov.ousrinis = b_us.iniciais))
		,'OUSRINIS'			=	tesouraria_mov.ousrinis
		,'ORIGEM'			=	tesouraria_mov.obs
		,'SALDOEDEBTOTAL'	=	@saldoEdebTotal
		,'SALDOECREDTOTAL'	=	@saldoEcredTotal
	from
		tesouraria_mov (nolock)
		inner join b_utentes (nolock) on tesouraria_mov.no = b_utentes.no
		left join doc_associados (nolock) on tesouraria_mov.no = doc_associados.id_cl AND tesouraria_mov.nrDocImp = doc_associados.nrDoc
	where
		tesouraria_mov.no = @nr_cl_org	
		and tesouraria_mov.data between @Dataini and @Datafim
	
union all

	select
		'SALDOANTERIOR'		=	@SALDOANTERIOR
		,'NO'				=	b_utentes.no
		,'NOME'				=	b_utentes.nome				
		,'CMDESC'			=	case 
									when doc_associados.ndoc = 1 
										then 'Resumo Faturas Entidade'  
									when doc_associados.ndoc = 2 
										then 'Nota Lanšamento Associados' 
									when doc_associados.ndoc = 3 
										then 'Aviso Lanšamento ' 
									else 'Fatura Far. Entidade' 
								end
		,'ADOC'				=	#dadosResumoFtAgrupados.ADOC
		,'DOCDATA'			=	convert(date,(select DATEADD(mm, (doc_associados.ano_dados - 1900) * 12 + doc_associados.mes_dados - 1 , 1)))
		,'DOCINTRO'			=	convert(date,(select DATEADD(mm, (doc_associados.ano - 1900) * 12 + doc_associados.mes - 1 , 1)))
		,'ECRED'			=	0
		,'EDEB'				=	#dadosResumoFtAgrupados.edeb
		,'USERNAME'			=	UPPER((Select top 1 username from b_us where #dadosResumoFtAgrupados.usrinis = b_us.iniciais))
		,'OUSRINIS'			=	#dadosResumoFtAgrupados.usrinis
		,'ORIGEM'			=	case when convert(varchar,#dadosResumoFtAgrupados.ADOC) = 0 then '' ELSE 'Ref. Documento N║: ' + convert(varchar,#dadosResumoFtAgrupados.ADOC) END
		,'SALDOEDEBTOTAL'	=	@saldoEdebTotal
		,'SALDOECREDTOTAL'	=	@saldoEcredTotal
	from
		#dadosResumoFtAgrupados (nolock)
		inner join b_utentes (nolock) on #dadosResumoFtAgrupados.noent = b_utentes.no
		left join doc_associados (nolock) on #dadosResumoFtAgrupados.adoc = doc_associados.nrdoc
	order by DOCDATA asc, ADOC asc

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosResumoFtAgrupados'))
		DROP TABLE #dadosResumoFtAgrupados

Go
Grant Execute on dbo.up_associados_ccEntidades to Public
Grant Control on dbo.up_associados_ccEntidades to Public
Go