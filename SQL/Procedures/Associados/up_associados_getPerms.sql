/*

	exec up_associados_getPerms 34070, 9
	exec up_associados_getPerms 34070, 0
	exec up_associados_getPerms 0, 9
	exec up_associados_getPerms 0,0

*/


SET ANSI_NULLS OFF 
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_associados_getPerms]') IS NOT NULL
	drop procedure dbo.up_associados_getPerms
go


CREATE PROCEDURE dbo.up_associados_getPerms
	@idFarm INT,
	@idEnt numeric(10),
	@bloqueadas Bit = 0

AS 
BEGIN

	DECLARE @tabPerms AS TABLE(idEnt numeric(10), idFarm numeric(10), ft bit, nc bit, nd bit, idScreenEnt numeric(10), idScreenFarm numeric(10), nomeScreenEnt varchar(70), nomeScreenFarm varchar(70), stamp varchar(30))

	IF @idFarm <> 0 AND @idEnt = 0
	BEGIN
		INSERT INTO @tabPerms
		SELECT 
			CAST(RIGHT(RTRIM(LTRIM(cptorg.cptorgStamp)),3) as numeric(10)) as IdEnt,
			@idFarm as idFarm,
			CAST(0 as bit) as ft,
			CAST(0 as bit) as nc,
			CAST(0 as bit) as nd,
			CONVERT(NUMERIC(10),u_no) as idScreenEnt,
			CAST(0 as numeric(10)) as idScreenFarm,
			cptorg.nome as nomeScreenEnt,
			CAST('' as varchar(60)) as nomeScreenFarm,
			''
		FROM
			b_utentes(nolock)
			join cptorg(nolock) on cptorg.u_no = b_utentes.no

	END

	IF @idFarm = 0 AND @idEnt <> 0
	BEGIN

		INSERT INTO @tabPerms
		SELECT 
			(SELECT TOP 1 CAST(RIGHT(LTRIM(rtrim(cptorgstamp)),3) as numeric(10)) FROM cptorg(nolock) WHERE u_no = @idEnt) as idEnt,
			id AS idFarm,  
			CAST(0 as bit) as ft,
			CAST(0 as bit) as nc,
			CAST(0 as bit) as nd,
			CAST(0 as numeric(10)) as idScreenEnt,
			CONVERT(numeric(10), id) as idScreenFarm,
			CAST('' as varchar(60)) as nomeScreenEnt,
			nome as nomeScreenFarm,
			''
		FROM 
			b_utentes(nolock) 
		WHERE
			inactivo = 0 
			and no > 200 
			and id <> '' 
			and tipo = 'Farm�cia'
			and estab = 0


	END

	IF @idFarm <> 0 AND @idEnt <> 0
	BEGIN
	
		INSERT INTO @tabPerms
		SELECT 
			(SELECT TOP 1 CAST(RIGHT(LTRIM(rtrim(cptorgstamp)),3) as numeric(10)) FROM cptorg(nolock) WHERE u_no = @idEnt) as idEnt,
			@idFarm AS idFarm,  
			CAST(0 as bit) as ft,
			CAST(0 as bit) as nc,
			CAST(0 as bit) as nd,
			@idEnt as idScreenEnt,
			@idFarm as idScreenFarm,
			(SELECT TOP 1 nome FROM cptorg(nolock) WHERE u_no = @idEnt) as nomeScreenEnt,
			(SELECT TOP 1 nome FROM b_utentes(nolock) WHERE id = LTRIM(@idFarm) ) as nomeScreenFarm,
			''

		

	END

	DECLARE 
		@stampPerm VARCHAR(30),
		@codeInfarmed varchar(10),
		@codeOrg varchar(10),
		@blockInvoice bit,
		@blockCreditNote bit,
		@blockDebitNote bit

	DECLARE uc_perms CURSOR FOR
	SELECT
		stamp,
		codeInfarmed,
		codeOrg,
		blockInvoice,
		blockCreditNote,
		blockDebitNote
	FROM 
		ext_esb_doc_permissions(nolock)

	OPEN uc_perms
	FETCH NEXT FROM uc_perms INTO @stampPerm, @codeInfarmed, @codeOrg, @blockInvoice, @blockCreditNote, @blockDebitNote

	WHILE @@FETCH_STATUS = 0
	BEGIN

		IF EXISTS (SELECT 1 FROM @tabPerms WHERE RIGHT('000' + LTRIM(RTRIM(idEnt)), 3) = @codeOrg AND LTRIM(RTRIM(idFarm)) = @codeInfarmed)
		BEGIN

			UPDATE @tabPerms SET stamp = @stampPerm, ft = @blockInvoice, nc = @blockCreditNote, nd = @blockDebitNote WHERE idEnt = @codeOrg AND idFarm = @codeInfarmed

		END

		FETCH NEXT FROM uc_perms INTO @stampPerm, @codeInfarmed, @codeOrg, @blockInvoice, @blockCreditNote, @blockDebitNote

	END

	CLOSE uc_perms
	DEALLOCATE uc_perms

	IF @bloqueadas = 0
	BEGIN
		SELECT * FROM @tabPerms
	END
	ELSE
	BEGIN
		SELECT * FROM @tabPerms WHERE stamp <> ''
	END

END

Go
Grant Execute on dbo.up_associados_getPerms to Public
Grant control on dbo.up_associados_getPerms to Public
Go