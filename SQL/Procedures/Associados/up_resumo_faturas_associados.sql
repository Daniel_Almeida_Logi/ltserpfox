/* Resumo Fatura��o Associados

	select * from ft where fno in (134,136)
	and no = 416

	exec up_resumo_faturas_associados 2016, 1, 416, -1, '',''
	exec up_resumo_faturas_associados <<YEAR(lcDataFact)>>, <<MONTH(lcDataFact)>>, <<lcNoCliente>>, -1, '', ''  
	exec up_resumo_faturas_associados 1900, 0, 0, 0, '', ''  
*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_resumo_faturas_associados]') IS NOT NULL
	drop procedure dbo.up_resumo_faturas_associados
go

create procedure dbo.up_resumo_faturas_associados
	@ano as numeric(4)
	,@mes as numeric(4)
	,@nr_cl as numeric(10)
	,@dep_cl as numeric(3)
	,@id_cl as varchar(10)
	,@nome_cl varchar(55)

/* WITH ENCRYPTION */

AS

	select
		ft.fno
		,credito =  case when ft.etotal < 0 then ft.etotal * (-1) else convert(numeric(13,3),0) end
		,debito =  case when ft.etotal > 0 then ft.etotal else convert(numeric(13,3),0) end
		,ft.ftstamp
		,ft.ndoc
		,ft.nmdoc
		,ft.fdata
		,mes = MONTH(ft.fdata)
		,ano = YEAR(ft.fdata)
		,nr_cl = ft.no
		,estab_cl = ft.estab
		,B_utentes.id
		,B_utentes.nome
		,B_utentes.morada
		,B_utentes.codpost
		,B_utentes.local
	from
		ft (nolock)
		inner join fi (nolock) on ft.ftstamp = fi.ftstamp
		inner join B_utentes (nolock) on ft.no = B_utentes.no and ft.estab = B_utentes.estab
		inner join cc (nolock) on cc.ftstamp=ft.ftstamp
	where
		ft.no = CASE WHEN @nr_cl = 0 THEN ft.no ELSE @nr_cl END
		AND ft.estab = CASE WHEN @dep_cl = -1 THEN ft.estab ELSE @dep_cl END
		AND ft.ftano = CASE WHEN @ano = 0 THEN ft.ftano ELSE @ano END
		AND MONTH(ft.fdata) = CASE WHEN @mes = 0 THEN MONTH(ft.fdata) ELSE @mes END
		AND ft.nome like '%' + @nome_cl + '%'
		AND	B_utentes.id = case when @id_cl = '' THEN B_utentes.id ELSE @id_cl end
		AND (abs((cc.ecred-cc.ecredf)-(cc.edeb-cc.edebf)) > 0.010000)
		AND ft.ndoc != 77
		AND fi.fistamp not in (select ofistamp from fi (nolock))
	group by 
		ft.fno
		,ft.etotal
		,ft.fdata
		,ft.ftstamp
		,ft.ndoc
		,ft.nmdoc
		,ft.fdata
		,ft.no
		,ft.estab
		,B_utentes.id
		,B_utentes.nome
		,B_utentes.morada
		,B_utentes.codpost
		,B_utentes.local
	order by 
		ft.fdata

GO
Grant Execute On dbo.up_resumo_faturas_associados to Public
Grant Control On dbo.up_resumo_faturas_associados to Public
Go