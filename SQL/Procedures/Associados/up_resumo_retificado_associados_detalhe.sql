/* Resumo Fatura��o Associados

	exec up_resumo_retificado_associados_detalhe 2015, 4, 0 , 0

	exec up_resumo_retificado_associados_detalhe 2015, 4, '18', 0

	select * from cl_fatc_org where valor_ret != 0
	
*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_resumo_retificado_associados_detalhe]') IS NOT NULL
	drop procedure dbo.up_resumo_retificado_associados_detalhe
go

create procedure dbo.up_resumo_retificado_associados_detalhe
	@ano			as numeric(4)
	,@mes			as numeric(4)
	,@nr_cl_org		as numeric(10)
	,@dataDados		as bit

/* WITH ENCRYPTION */

AS
	IF @dataDados = 0
		BEGIN
			select
				cfo.id
				,cfo.id_cl
				,cfo.nr_cl_org
				,cfo.nrDocImp
			from
				cl_fatc_org cfo (nolock)
				inner join b_utentes cl (nolock) on cl.no = cfo.nr_cl_org
			where
				cfo.nr_cl_org	   = case when @nr_cl_org = 0 then cfo.nr_cl_org ELSE @nr_cl_org END
				and cfo.ano = @ano 
				and cfo.mes = @mes
				and valor_ret != 0	-- considerar apenas os retificados
		END
	ELSE
		BEGIN
			select
				cfo.id
				,cfo.id_cl
				,cfo.nr_cl_org
				,cfo.nrDocImp
			from
				cl_fatc_org cfo (nolock)
				inner join b_utentes cl (nolock) on cl.no = cfo.nr_cl_org
			where
				cfo.nr_cl_org	   = case when @nr_cl_org = 0 then cfo.nr_cl_org ELSE @nr_cl_org END
				and cfo.ano_tratamento = @ano 
				and cfo.mes_tratamento = @mes
				and valor_ret != 0	-- considerar apenas os retificados
		END 

GO
Grant Execute On dbo.up_resumo_retificado_associados_detalhe to Public
Grant Control On dbo.up_resumo_retificado_associados_detalhe to Public
Go