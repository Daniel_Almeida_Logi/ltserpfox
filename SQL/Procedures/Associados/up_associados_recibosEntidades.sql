/* Recibos Associados

	exec up_associados_recibosEntidades 2016, 8, 6, 3
	exec up_associados_recibosEntidades 2016, 8, 6, 0

	**aten��o: na reimpress�o a consulta da informa��o � feita pelo nr do documentos para os totais do documento serem corretos

*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_associados_recibosEntidades]') IS NOT NULL
	drop procedure dbo.up_associados_recibosEntidades
go

create procedure dbo.up_associados_recibosEntidades
	@ano			as numeric(4)
	,@mes			as numeric(4)
	,@nr_cl_org		as numeric(10)
	,@nrRecibo		as numeric(10)
	,@descr			as varchar(200)
	,@valor_recibo  as numeric(9,2)
	
/* WITH ENCRYPTION */

AS
	
	set language portuguese 
	
	IF @nrRecibo = 0
		-- entre aqui qd se est� a gerar o recibo	
		select 
			-- descr = case when sum(valor_regularizado) > 0 then 'Ref. Resumo Faturas Ref: '+ 'A/' + convert(varchar,cfo.nrDocImp) + ' - ' + upper(left(datename(month, DateAdd(month , mes-1 , 0)),3)) +  + '/' + convert(varchar,ano) 
						--else 'Ref. Aviso Lan�amento Ref: '+ 'B/' + convert(varchar,cfo.nrDocImp) + ' - ' + upper(left(datename(month, DateAdd(month , mes-1 , 0)),3)) +  + '/' + convert(varchar,ano) end
			 descr = @descr
			,referencia = upper(left(datename(month, DateAdd(month , mes-1 , 0)),3)) +  + '/' + convert(varchar,ano)
			--,total_reg = sum(valor_regularizado)
			,total_reg = @valor_recibo
			,cl.nome, cl.nome2, cl.morada, cl.codpost, cl.local, cl.ncont, cl.telefone, cl.fax
			,tipo = case when sum(valor_regularizado) > 0 Then 'R' else 'C' END
			,nr_org = cl.no
			,DocOrigem = cfo.nrDocImp
		from
			cl_fatc_org cfo (nolock)
			inner join b_utentes cl (nolock) on cl.no = cfo.nr_cl_org
		where
			cfo.nr_cl_org = @nr_cl_org
			and cfo.ano = @ano 
			and cfo.mes = @mes 
		group by
			ano, mes
			,cl.nome, cl.nome2, cl.morada, cl.codpost, cl.local, cl.ncont, cl.telefone, cl.fax, cl.no, cfo.nrDocImp
		having 
			 sum(valor_regularizado) != 0
		
	ELSE
		select 
			 descr = replace(replace(cfo.obs,char(10),'- '),char(13),' ')
			,referencia = replace(replace(cfo.obs,char(10),'- '),char(13),' ')
			,total_reg = valor
			,cl.nome, cl.nome2, cl.morada, cl.codpost, cl.local, cl.ncont, cl.telefone, cl.fax
			,tipo = case when sum(valor) > 0 Then 'R' else 'C' END
			,nr_org = cl.no
			,DocOrigem = cfo.nrDocImp
		from
			tesouraria_mov (nolock) cfo
			inner join b_utentes cl (nolock) on cl.no = cfo.no and cl.estab = cfo.dep
		where
			cfo.nrDocImp = @nrRecibo
		group by
			cfo.obs, cfo.valor
			,cl.nome, cl.nome2, cl.morada, cl.codpost, cl.local, cl.ncont, cl.telefone, cl.fax, cl.no, cfo.nrDocImp

GO
Grant Execute On dbo.up_associados_recibosEntidades to Public
Grant Control On dbo.up_associados_recibosEntidades to Public
Go




		