/* Pesquisa de Clientes

	 exec up_associados_informacaoRelatorio 1000, '', 0, -1, ''
											, '', 'Farm�cia', '', '', ''
											, 0, '', ''

	select * from b_utentes where inactivo = 0

*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_associados_informacaoRelatorio]') IS NOT NULL
    drop procedure dbo.up_associados_informacaoRelatorio
go
create procedure dbo.up_associados_informacaoRelatorio

@top		int
,@nome		varchar(80)
,@no		numeric(9)
,@estab		numeric(5)
,@morada	varchar(55)
,@ncont		varchar(20)
,@tipo		varchar(20)
,@tlmvl		bit
,@obs		varchar(max)
,@mail		bit
,@inactivo	bit
,@tlf		varchar(60)
,@id		varchar(20)

/* WITH ENCRYPTION */
AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosclientes'))
		DROP TABLE #dadosclientes

	SELECT
		 Sel = CONVERT(bit,0)
		,b_utentes.nome
		,b_utentes.no
		,b_utentes.estab
		,b_utentes.tlmvl
		,b_utentes.telefone
		,b_utentes.inactivo
		,b_utentes.utstamp
		,b_utentes.morada
		,b_utentes.local
		,b_utentes.codpost
		,b_utentes.ncont
		,b_utentes.codigop
		,b_utentes.tipo
		,b_utentes.ccusto
		,b_utentes.zona
		,b_utentes.desconto
		,b_utentes.obs
		,b_utentes.email
		,row = ROW_NUMBER() over(partition by b_utentes.utstamp order by b_utentes.utstamp)
		,b_utentes.id
		,b_utentes.direcaoTec
		,b_utentes.proprietario
		,b_utentes.nib
		,b_utentes.moradaAlt
		,b_utentes.noAssociado
		,b_utentes.Alvara
		,b_utentes.software
		,b_utentes.ars
	into 
		#dadosclientes
	from
		b_utentes (nolock)	
	where
		b_utentes.estab = case when @estab >= 0 then @estab else b_utentes.estab end
		and b_utentes.no = case when @no > 0 then @no else b_utentes.no end
		and nome like @nome + '%'
		and morada like @morada + '%'
		and ncont = case when @ncont = '' then ncont else @ncont end
		/*se tem tlmvl preenchido*/
		and tlmvl <> case when @tlmvl = 0 then 's/tlm' else '' end
		and obs like @obs + '%'
		/*se tem mail preenchido*/
		and email <> (case when @mail = 0 then 's/mail' else '' end)
		and inactivo = CASE when @inactivo = 1 THEN inactivo else @inactivo END
		and (
			tlmvl = case when @tlf = '' then tlmvl else @tlf end
			or 
			telefone = case when @tlf = '' then telefone else @tlf end
		)
		and tipo = case when @tipo='' then tipo else @tipo end
		and id = case when @id = '' then id else @id end
	order by 
		b_utentes.no asc

	select
		 top(@top) a.*
	from 
		#dadosclientes a 
	
GO
Grant Execute on dbo.up_associados_informacaoRelatorio to Public
Grant Control on dbo.up_associados_informacaoRelatorio to Public
go