/* usada na faturacao automatica

	exec up_pesqdocassociados_faturacao 2024, 2024 ,4 ,4 ,'' ,0 ,0 ,'' ,'', 1

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_pesqdocassociados_faturacao]') IS NOT NULL
	drop procedure dbo.up_pesqdocassociados_faturacao
go

create procedure dbo.up_pesqdocassociados_faturacao
@anoIni		as numeric(4)
,@anoFim	as numeric(4)
,@mesIni	as numeric(2)
,@mesFim	as numeric(2)
,@nome		varchar(80)
,@nr_cl		as numeric(10)
,@dep_cl	as numeric(10)
,@ref		varchar(18)
,@tipo		varchar(20)
,@site_nr	tinyint

/* WITH ENCRYPTION */

AS
	Declare @dataactual as datetime, @dataFim datetime
	set  @dataactual = DATEADD(mm, (@anoIni - 1900) * 12 + @mesIni - 1 , 1)
	set  @dataFim = DATEADD(mm, (@anoFim - 1900) * 12 + @mesFim - 1 , 1)
	set  @dataFim = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@dataFim)+1,0))
	
	If OBJECT_ID('tempdb.dbo.#dadosAnoMes') IS NOT NULL
		drop table #dadosAnoMes;
	If OBJECT_ID('tempdb.dbo.#dadosFt') IS NOT NULL
		drop table #dadosFt;
	If OBJECT_ID('tempdb.dbo.#dadosAnoMesFt') IS NOT NULL
		drop table #dadosAnoMesFt;

	/*Cria Tabela Temporaria*/
	SELECT 1900 as ano,12 as mes INTO  #dadosAnoMes where 1 =0
			
	WHILE @dataactual <= @dataFim
	BEGIN
		INSERT INTO #dadosAnoMes 
		SELECT year(@DATAACTUAL) as ano, month(@DATAACTUAL) as mes
		
		SET @DATAACTUAL = DATEADD(MM,1,@DATAACTUAL)
	END
	
	
	/**/
	select
		cl.no
		,cl.estab
		,nome = cl.nome + ' ['+ LTRIM(STR(cl.no)) +']['+ LTRIM(STR(cl.estab)) +']'
		,utente = nome
		,nomeFT = case when cl.entfact = 1 then nome else (select nome from b_utentes (nolock) where no = cl.no and estab = 0) end 
		,noFT = case when cl.entfact = 1 then cl.no else (select no from b_utentes (nolock) where no = cl.no and estab = 0) end 
		,estabFT = case when cl.entfact = 1 then cl.estab else 0 end 
		,tipoFT = case when cl.entfact = 1 then cl.tipo else (select tipo from b_utentes (nolock) where no = cl.no and estab = 0) end 
		,cl.tipo
		,cl.autofact
		,st.ref
		,st.design
		,cls.desvio_mes_fat
		,cls.data
		,cls.preco
		,cls.desconto
		,cls.desconto_valor
		,cls.qtt
		,totalLinha = ROUND((cls.preco - (cls.preco * (cls.desconto/100)) - cls.desconto_valor) * cls.qtt ,2) 
		,totalLinhaSemIva = CASE WHEN st.ivaincl = 1 THEN ROUND(((((cls.preco / (taxasiva.taxa/100+1)) * ((100-cls.desconto)/100)) - cls.desconto_valor) * cls.qtt),2) ELSE
														  ROUND((((cls.preco * ((100-cls.desconto)/100))) - cls.desconto_valor) *cls.qtt ,2) END
		,valorCalcTotais = CASE WHEN st.ivaincl = 1 THEN ROUND(((((cls.preco / (taxasiva.taxa/100+1)) * ((100-cls.desconto)/100)) - cls.desconto_valor) * (taxasiva.taxa/100+1)) * cls.qtt,2) ELSE
													     ROUND((((cls.preco * ((100-cls.desconto)/100)) - cls.desconto_valor) * (taxasiva.taxa/100+1)) *cls.qtt ,2)   END
		,totalLinhaIva = CASE WHEN st.ivaincl = 1 then ROUND((((((cls.preco / (taxasiva.taxa/100+1)) * ((100-cls.desconto)/100)) - cls.desconto_valor) * (taxasiva.taxa/100+1)) * cls.qtt) - (((((cls.preco / (taxasiva.taxa/100+1)) * ((100-cls.desconto)/100)) - cls.desconto_valor) * cls.qtt)),2) ELSE
													ROUND(((((cls.preco * ((100-cls.desconto)/100)) - cls.desconto_valor) * (taxasiva.taxa/100+1)) *cls.qtt) - ((((cls.preco * ((100-cls.desconto)/100))) - cls.desconto_valor) *cls.qtt) ,2) END
		,st.epv1
		,st.tabiva
		,iva = taxasiva.taxa
		,st.ivaincl
		,st.epcusto
		,st.obs
		--,valorfaturar = Round(case when st.ivaincl = 1 then cls.preco*cls.qtt else cls.preco*(taxasiva.taxa/100+1)*cls.qtt end,2)
	into 
		#dadosFt
	from
		cl_servicos cls (nolock)
		inner join st (nolock) on st.ref = cls.ref
		inner join b_utentes cl (nolock) on cl.no = cls.nr_cl and cl.estab = cls.dep_cl
		inner join taxasiva (nolock) on taxasiva.codigo = st.tabiva
	where
		cls.fatura = 1
		and st.site_nr = @site_nr
		
	
	/**/
	select 
		*
	into 
		#dadosAnoMesFt 
	from 
		#dadosAnoMes as dAM
		,#dadosFt

	Select 
		distinct
		sel = CONVERT(BIT,0)
		,dAM.*
		,valor = case when fi.etiliquido is null then dAM.totalLinha else isnull(fi.etiliquido,0) end 
		,faturado = case when fi.etiliquido is null then 0 else 1 end
		,fi.fistamp
		,fi.ftstamp
	From
		#dadosAnoMesFt dAM
		left join cl_servicos_fat clf (nolock) on clf.ano = dAM.ano and clf.mes = dAM.mes and clf.nr_cl = dAM.no and clf.dep_cl = dAM.estab and clf.ref = dAM.ref
		left join fi (nolock) on clf.fistamp = fi.fistamp
		left join cl_servicos cls (nolock) on cls.nr_cl = dAM.no and cls.dep_cl = dAM.estab
	Where
		dAM.utente	  = case when @nome = '' then utente else @nome end
		and dAM.ref   = case when @ref = '' then dAM.ref else @ref end
		and dAM.tipo  = case when @tipo = '' then dAM.tipo else @tipo end
		and dAM.no	  = case when @nr_cl = 0 then dAM.no else @nr_cl end
		and dAM.estab = case when @dep_cl = 0 then dAM.estab else @dep_cl end
		and (select DATEADD(mm, (dAM.ano - 1900) * 12 + dAM.mes - 1 , 1) as datacomp) >= dAM.data
	Order by
		ano, mes, nome, no, estab

	If OBJECT_ID('tempdb.dbo.#dadosAnoMes') IS NOT NULL
		drop table #dadosAnoMes;
	If OBJECT_ID('tempdb.dbo.#dadosFt') IS NOT NULL
		drop table #dadosFt;
	If OBJECT_ID('tempdb.dbo.#dadosAnoMesFt') IS NOT NULL
		drop table #dadosAnoMesFt;

GO
Grant Execute On dbo.up_pesqdocassociados_faturacao to Public
Grant Control On dbo.up_pesqdocassociados_faturacao to Public
Go