/* Pesquisar Documentos Associados


	exec up_pesqdocassociados_pesquisarDocumentos '', 0, '', '', 0, '', '', 0, 0, 0, 1, '0'

	exec up_PESQDOCASSOCIADOS_pesquisarDocumentos '', 15.00, '', '', 0, '', '', 2017, 3, 0, 1, ''

	select * from cl_fatc_org
	select * from doc_associados
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_pesqdocassociados_pesquisarDocumentos]') IS NOT NULL
	drop procedure dbo.up_pesqdocassociados_pesquisarDocumentos
go

create procedure dbo.up_pesqdocassociados_pesquisarDocumentos

@nome_org			varchar(55),			
@nr_org				numeric(10),			
@id_org				varchar(10),			
@nome_cl			varchar(55),			
@nr_cl				numeric(10),			
@id_cl				varchar(10),			
@nrDoc				varchar(20),			
@ano				smallint,				
@mes				tinyint,				
@data_tratamento	bit,
@imprimir			varchar(20) = '',
@nrDocImp			varchar(20) = '',
@sofaturas			bit = 0			    

/* WITH ENCRYPTION */

AS

SET NOCOUNT ON

IF @data_tratamento = 0
Begin 
	select
		cfo.id 
		,cfo.ano
		,cfo.mes
		,cfo.ano_tratamento
		,cfo.mes_tratamento
		,cfo.nrDoc
		,cfo.valor
		,cfo.valor_ret
		,cfo.valor_pagar
		,cfo.valor_regularizado
		,cfo.recebido
		,cfo.imprimir
		,cfo.obs
		,cfo.data
		,cfo.data_alt
		,cfo.id_cl, cl.no as nr_cl, cl.estab as dep_cl, cl.nome as nome_cl
		,ncont_cl = cl.ncont, morada_cl = cl.morada, codpost_cl = cl.codpost, local_cl = cl.local
		,Entidades.no as nr_org, Entidades.id as id_org, Entidades.nome as nome_org
		,Entidades.morada as morada_org, Entidades.codpost as codpost_org, Entidades.local as local_org, Entidades.ncont as ncont_org, Entidades.nome2 as abrev_org
		,cfo.fno_cl, cfo.data_fno_cl
		,referencia = upper(left(datename(month, DateAdd(month , mes-1 , 0)),3)) +  + '/' + convert(varchar,ano)
		,cfo.ousrinis
		,cfo.usrinis
		,cfo.nrDocImp
		,cfo.emitido
		,cfo.emitidoe
		,cfo.emitidoale
		,CAST(0 AS bit) as emitir
		,cfo.obs
		,cfo.docNr
	from
		cl_fatc_org cfo (nolock)
		left join B_utentes as cl (nolock) on cl.id = cfo.id_cl
		left join B_utentes as Entidades (nolock) on Entidades.no = cfo.nr_cl_org
	where 
		Entidades.nome LIKE '%'+@nome_org+'%'
		AND	nr_cl_org			 = CASE WHEN @nr_org = 0 THEN nr_cl_org ELSE @nr_org END
		AND	Entidades.id		 = CASE WHEN @id_org = '' THEN Entidades.id ELSE @id_org END
		AND cl.nome LIKE '%'+ @nome_cl+'%'
		AND cl.no				 = CASE WHEN @nr_cl = 0 THEN cl.no ELSE @nr_cl END
		AND id_cl				 = CASE WHEN @id_cl = '' THEN id_cl ELSE @id_cl END
		AND nrDoc				 = CASE WHEN @nrdoc = '' THEN nrDoc ELSE @nrdoc END
		AND cfo.ano				 = CASE WHEN @ano = 0 THEN cfo.ano else @ano END
		AND cfo.mes				 = CASE WHEN @mes = 0 THEN cfo.mes ELSE @mes END
		AND cfo.imprimir		 = CASE WHEN @Imprimir = '' THEN cfo.imprimir ELSE @Imprimir END
		AND cfo.nrDocImp		 = CASE WHEN @nrDocImp = '' then cfo.nrDocImp ELSE @nrDocImp END
		AND cl.inactivo		     = 0
		AND cfo.valor_ret		 = CASE WHEN @sofaturas = 1 THEN 0 ELSE cfo.valor_ret END
		and cl.id<>''
	order by
		cfo.nrdoc, cfo.ano, cfo.mes, cfo.id_cl
End
ELSE
Begin
	select
		cfo.id 
		,cfo.ano
		,cfo.mes
		,cfo.ano_tratamento
		,cfo.mes_tratamento
		,cfo.nrDoc
		,cfo.valor
		,cfo.valor_ret
		,cfo.valor_pagar
		,cfo.valor_regularizado
		,cfo.recebido
		,cfo.imprimir
		,cfo.obs
		,cfo.data
		,cfo.data_alt
		,cfo.id_cl, cl.no as nr_cl, cl.estab as dep_cl, cl.nome as nome_cl
		,ncont_cl = cl.ncont, morada_cl = cl.morada, codpost_cl = cl.codpost, local_cl = cl.local
		,Entidades.no as nr_org, Entidades.id as id_org, Entidades.nome as nome_org
		,Entidades.morada as morada_org, Entidades.codpost as codpost_org, Entidades.local as local_org, Entidades.ncont as ncont_org, Entidades.nome2 as abrev_org
		,cfo.fno_cl, cfo.data_fno_cl
		,referencia = upper(left(datename(month, DateAdd(month , mes-1 , 0)),3)) +  + '/' + convert(varchar,ano)
		,cfo.ousrinis
		,cfo.usrinis
		,cfo.nrDocImp
		,cfo.emitido
		,cfo.emitidoe
		,cfo.emitidoale
		,CAST(0 AS bit) as emitir
		,cfo.obs
		,cfo.docNr
	from
		cl_fatc_org cfo (nolock)
		left join B_utentes as cl (nolock) on cl.id = cfo.id_cl
		left join B_utentes as Entidades (nolock) on Entidades.no = cfo.nr_cl_org
	where 
		Entidades.nome LIKE '%'+@nome_org+'%'
		AND	nr_cl_org						 = CASE WHEN @nr_org = 0 THEN nr_cl_org ELSE @nr_org END
		AND	Entidades.id					 = CASE WHEN @id_org = '' THEN Entidades.id ELSE @id_org END
		AND cl.nome LIKE '%'+ @nome_cl+'%'
		AND cl.no							 = CASE WHEN @nr_cl = 0 THEN cl.no ELSE @nr_cl END
		AND id_cl						 	 = CASE WHEN @id_cl = '' THEN id_cl ELSE @id_cl END
		AND nrDoc							 = CASE WHEN @nrdoc = '' THEN nrDoc ELSE @nrdoc END
		AND cfo.ano_tratamento				 = CASE WHEN @ano = 0 THEN cfo.ano_tratamento else @ano END
		AND cfo.mes_tratamento				 = CASE WHEN @mes = 0 THEN cfo.mes_tratamento ELSE @mes END
		AND cfo.imprimir					 = CASE WHEN @Imprimir = '' THEN cfo.imprimir ELSE @Imprimir END
		AND cfo.nrDocImp					 = CASE WHEN @nrDocImp = '' THEN cfo.nrDocImp ELSE @nrDocImp END
		AND cfo.valor_ret					 = CASE WHEN @sofaturas = 1 THEN 0 ELSE cfo.valor_ret END
		and cl.id<>''
	order by
		cfo.nrdoc, cfo.ano, cfo.mes, cfo.id_cl

End

GO
Grant Execute On dbo.up_pesqdocassociados_pesquisarDocumentos to Public
Grant Control On dbo.up_pesqdocassociados_pesquisarDocumentos to Public
Go