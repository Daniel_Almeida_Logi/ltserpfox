/*
	exec up_valcartao_listar 'Loja 1',1
	exec up_valcartao_listar 'Loja 1',1 

*/
if OBJECT_ID('[dbo].[up_valcartao_listar]') IS NOT NULL
	drop procedure dbo.up_valcartao_listar
go

create procedure dbo.up_valcartao_listar
	@site varchar(60)
	,@atendimento bit = 0

/* WITH ENCRYPTION */
AS


	Select 
		* 
	from 
		campanhas (nolock)
	where 
		(campanhas.site = @site or @site = '')
		and campanhas.inativo = 0
		and campanhas.eliminada = 0
		and valcartao > 0
GO
Grant Execute On dbo.up_valcartao_listar to Public
Grant Control On dbo.up_valcartao_listar to Public
GO

