/*
	exec up_campanhas_listar 'Loja 1',1
	exec up_campanhas_listar 'Loja 1',1 

*/
if OBJECT_ID('[dbo].[up_campanhas_listar]') IS NOT NULL
	drop procedure dbo.up_campanhas_listar
go

create procedure dbo.up_campanhas_listar
	@site varchar(60)
	,@atendimento bit = 0

/* WITH ENCRYPTION */
AS


	Select 
		* 
	from 
		campanhas (nolock)
	where 
		(campanhas.site = @site or @site = '')
		and campanhas.inativo = 0
		and campanhas.eliminada = 0
		and valcartao = 0
		--and convert(date,getdate()) between campanhas.dataInicio and campanhas.dataFim
GO
Grant Execute On dbo.up_campanhas_listar to Public
Grant Control On dbo.up_campanhas_listar to Public
GO

