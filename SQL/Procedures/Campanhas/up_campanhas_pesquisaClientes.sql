/* Pesquisa de Clientes Campanhas

 exec up_campanhas_pesquisaClientes 30, '', 0, -1, '', '', '', '', '', '', 1, '>', 55, '', 0, '', '', '', '20120101', '20121126', '19000101', 0, '', '', '', 1, 0,''

 
 exec up_campanhas_pesquisaClientes 100000, '', 0, -1, '', '','', 'Normal', '','', 0, '>', -9999, '', 0, '','', '', '20151012', '20151027', '19000101', 0,'', '', '', 0, 0, ''


*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_campanhas_pesquisaClientes]') IS NOT NULL
    drop procedure dbo.up_campanhas_pesquisaClientes
go

create procedure dbo.up_campanhas_pesquisaClientes

@top		int
,@nome		varchar(80)
,@no		numeric(9)
,@estab		numeric(5)
,@morada	varchar(55)
,@ncont		varchar(20)
,@ncartao	varchar(30)
,@tipo		varchar(20)
,@Profissao	varchar(60)
,@sexo		varchar(1)
,@tlmvl		bit
,@operador	char(1)
,@idade		int
,@obs		varchar(max)
,@mail		bit
,@entPla	varchar(50)
,@patologia varchar(25)
,@ref		varchar(254)
,@Entre		datetime
,@E			datetime
,@dtnasc	datetime
,@inactivo	bit
,@tlf		varchar(60)
,@nbenef	varchar(18)
,@nbenef2	varchar(18)
,@touch		bit
,@entCompart bit
,@id		varchar(20)

/* WITH ENCRYPTION */
AS

/* guardar valores para idade*/
declare @maiorQue int, @menorQue int
set @maiorQue = case
					when @operador = '>' or @operador = '='
					then @idade
					else -999999
				end
set @menorQue = case
					when @operador = '=' or @operador = '<'
					then @idade
					else 999999
				end
		
if @ref = ''
begin

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosclientes'))
		DROP TABLE #dadosclientes

	SELECT
		 Sel = CONVERT(bit,0)
		,b_utentes.nome
		,b_utentes.no
		,b_utentes.estab
		,b_utentes.tlmvl
		,b_utentes.telefone
		,b_utentes.nrcartao
		,b_utentes.nbenef
		,b_utentes.nbenef2
		,idade = case when year(nascimento) != 1900 then DateDiff(year, nascimento, getdate()) else 0 end
		,b_utentes.inactivo
		,b_utentes.nascimento
		,b_utentes.utstamp
		,b_utentes.morada
		,b_utentes.local
		,b_utentes.codpost
		,b_utentes.ncont
		,b_utentes.codigop
		,b_utentes.tipo
		,b_utentes.ccusto
		,b_utentes.zona
		,b_utentes.desconto
		,b_utentes.nocredit
		,b_utentes.modofact
		,b_utentes.entfact
		,b_utentes.profi
		,b_utentes.sexo
		,b_utentes.obs
		,b_utentes.email
		,b_utentes.entpla
		,b_utentes.tabiva
		,row = ROW_NUMBER() over(partition by b_utentes.utstamp order by b_utentes.utstamp)
		,clinica
		,b_utentes.entCompart
		,b_utentes.id
	into 
		#dadosclientes
	from
		b_utentes (nolock)	
	where
		b_utentes.estab = case when @estab >= 0 then @estab else b_utentes.estab end
		and b_utentes.no = case when @no > 0 then @no else b_utentes.no end
		and (B_utentes.entCompart = @entCompart or @entCompart = 0)
		and b_utentes.inactivo = 0

	select 
		a.no, a.estab
	from 
		#dadosclientes as a
		left join B_patologias (nolock) on B_patologias.ostamp=(case when @patologia='' then '' else utstamp end)
	where
		nome like @nome + '%'
		and morada like @morada + '%'
		and ncont = case when @ncont = '' then ncont else @ncont end
		and nrcartao = case when @ncartao='' then nrcartao else @ncartao end
		and sexo = case when @sexo <> '' then @sexo else sexo end
		/*se tem tlmvl preenchido*/
		and tlmvl <> case when @tlmvl = 0 then 's/tlm' else '' end
		and obs like @obs + '%'
		/*se tem mail preenchido*/
		and email <> (case when @mail = 0 then 's/mail' else '' end)
		and entpla like @entPla + '%'
		and nascimento = (case when @dtnasc = '19000101' then nascimento else @dtnasc end)
		and inactivo = CASE when @inactivo = 1 THEN inactivo else @inactivo END
		and (
			tlmvl = case when @tlf = '' then tlmvl else @tlf end
			or 
			telefone = case when @tlf = '' then telefone else @tlf end
		)
		and nbenef = case when @nbenef = '' then nbenef else @nbenef end
		and nbenef2 = case when @nbenef2 = '' then nbenef2 else @nbenef2 end   
		and DateDiff(year, nascimento, getdate()) between @maiorQue and @menorQue
		and no >= case when @touch = 1 then 200 else 0 end
		and tipo = case when @tipo='' then tipo else @tipo end
		/*profiss�o*/
		and profi like @profissao + '%'
		/*Patologia*/
		and isnull(B_patologias.ctiefpstamp,'') like (case when @patologia='' then '%' else @patologia end)
		and row = 1
		and id = case when @id = '' then id else @id end
	order by no, estab

end
else
begin
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosclientes2'))
		DROP TABLE #dadosclientes2

	
	SELECT
		 Sel = CONVERT(bit,0)
		,b_utentes.nome
		,b_utentes.no
		,b_utentes.estab
		,b_utentes.tlmvl
		,b_utentes.telefone
		,b_utentes.nrcartao
		,b_utentes.nbenef
		,b_utentes.nbenef2
		,idade = DateDiff(year, b_utentes.nascimento, getdate())
		,b_utentes.inactivo
		,b_utentes.nascimento
		,b_utentes.utstamp
		,b_utentes.morada
		,b_utentes.local
		,b_utentes.codpost
		,b_utentes.ncont
		,b_utentes.codigop
		,b_utentes.tipo
		,b_utentes.ccusto
		,b_utentes.zona
		,b_utentes.desconto
		,b_utentes.nocredit
		,b_utentes.modofact
		,b_utentes.entfact
		,b_utentes.profi
		,b_utentes.sexo
		,b_utentes.obs
		,b_utentes.email
		,b_utentes.entpla
		,b_utentes.tabiva
		,row = ROW_NUMBER() over(partition by b_utentes.utstamp order by b_utentes.utstamp)
		,clinica
		,b_utentes.entCompart
		,b_utentes.id
	into 
		#dadosCLIENTEs2
	from
		b_utentes (nolock)	
		inner join ft (nolock) on ft.no=b_utentes.no and ft.estab=b_utentes.estab
		inner join fi (nolock) on ft.ftstamp = fi.ftstamp
	where
		ft.ndoc=3
		and fi.ref in (select * from dbo.up_splitToTable(@ref,'|'))
		and ft.fdata between @Entre and @E
		and ft.estab = case when @estab >= 0 then @estab else ft.estab end
		and ft.no = case when @no > 0 then @no else ft.no end
		and (B_utentes.entCompart = @entCompart or @entCompart = 0)
		and b_utentes.inactivo = 0

	select 
		a.no, a.estab
	from 
		#dadosclientes2 as a
		left join B_patologias (nolock) on B_patologias.ostamp=(case when @patologia='' then '' else utstamp end)
	where
		nome like @nome + '%'
		and morada like @morada + '%'
		and ncont = case when @ncont = '' then ncont else @ncont end
		and nrcartao = case when @ncartao='' then nrcartao else @ncartao end
		and sexo = case when @sexo <> '' then @sexo else sexo end
		/*se tem tlmvl preenchido*/
		and tlmvl <> case when @tlmvl = 0 then 's/tlm' else '' end
		and obs like @obs + '%'
		/*se tem mail preenchido*/
		and email <> (case when @mail = 0 then 's/mail' else '' end)
		and entpla like @entPla + '%'
		and nascimento = (case when @dtnasc = '19000101' then nascimento else @dtnasc end)
		and inactivo = CASE when @inactivo = 1 THEN inactivo else @inactivo END
		and (
			tlmvl = case when @tlf = '' then tlmvl else @tlf end
			or 
			telefone = case when @tlf = '' then telefone else @tlf end
		)
		and nbenef = case when @nbenef = '' then nbenef else @nbenef end
		and nbenef2 = case when @nbenef2 = '' then nbenef2 else @nbenef2 end   
		and DateDiff(year, nascimento, getdate()) between @maiorQue and @menorQue
		and no >= case when @touch = 1 then 200 else 0 end
		and tipo = case when @tipo='' then tipo else @tipo end
		/*profiss�o*/
		and profi like @profissao + '%'
		/*Patologia*/
		and isnull(B_patologias.ctiefpstamp,'') like (case when @patologia='' then '%' else @patologia end)
		and row = 1
		and id = case when @id = '' then id else @id end
	order by no, estab
end

	
GO
Grant Execute on dbo.up_campanhas_pesquisaClientes to Public
Grant Control on dbo.up_campanhas_pesquisaClientes to Public
go