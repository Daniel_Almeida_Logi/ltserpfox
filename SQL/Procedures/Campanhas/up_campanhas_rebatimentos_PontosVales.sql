/*
	exec up_campanhas_rebatimentos_PontosVales 'Loja 1'

*/
if OBJECT_ID('[dbo].[up_campanhas_rebatimentos_PontosVales]') IS NOT NULL
	drop procedure dbo.up_campanhas_rebatimentos_PontosVales
go

create procedure dbo.up_campanhas_rebatimentos_PontosVales
	@site varchar(60)

/* WITH ENCRYPTION */
AS


	Select 
		ID_Regra = campanhas.id
		,campanhas_pNiveis.* 
	from 
		campanhas (nolock)
		left join campanhas_pNiveis (nolock) on campanhas.id = campanhas_pNiveis.campanhas_id
	where 
		campanhas.site = @site
		and campanhas.inativo = 0
		and campanhas.eliminada = 0
		and convert(date,getdate()) between campanhas.dataInicio and campanhas.dataFim
		and campanhas_pNiveis.campanhas_id is not null
	Order by
		campanhas_pNiveis.valor desc,campanhas_id
	

GO
Grant Execute On dbo.up_campanhas_rebatimentos_PontosVales to Public
Grant Control On dbo.up_campanhas_rebatimentos_PontosVales to Public
GO

