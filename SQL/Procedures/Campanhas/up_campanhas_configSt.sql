/*
	exec up_campanhas_configSt 'Loja 1'

*/
if OBJECT_ID('[dbo].[up_campanhas_configSt]') IS NOT NULL
	drop procedure dbo.up_campanhas_configSt
go

create procedure dbo.up_campanhas_configSt
	@site varchar(60)

/* WITH ENCRYPTION */
AS


	Select 
		campanhas_st.* 
	from 
		campanhas (nolock)
		inner join campanhas_st (nolock) on campanhas.id = campanhas_st.campanhas_id
	where 
		(campanhas.site = @site or @site = '')
		and campanhas.inativo = 0
		and campanhas.eliminada = 0
		--and convert(date,getdate()) between campanhas.dataInicio and campanhas.dataFim
	order by 
		campanhas_st.campanhas_id asc

	

GO
Grant Execute On dbo.up_campanhas_configSt to Public
Grant Control On dbo.up_campanhas_configSt to Public
GO

