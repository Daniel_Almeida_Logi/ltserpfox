/*
	exec up_campanhas_ProdutosOferta 'Loja 1'

*/
if OBJECT_ID('[dbo].[up_campanhas_ProdutosOferta]') IS NOT NULL
	drop procedure dbo.up_campanhas_ProdutosOferta
go

create procedure dbo.up_campanhas_ProdutosOferta
	@site varchar(60)
	

/* WITH ENCRYPTION */
AS


	Select 
		campanhas_pOfertas.* 
	from 
		campanhas (nolock)
		inner join campanhas_pOfertas (nolock) on campanhas.id = campanhas_pOfertas.campanhas_id 
	where 
		(campanhas.site = @site or @site = '')
		and campanhas.inativo = 0
		and campanhas.eliminada = 0
		--and convert(date,getdate()) between campanhas.dataInicio and campanhas.dataFim
	

GO
Grant Execute On dbo.up_campanhas_ProdutosOferta to Public
Grant Control On dbo.up_campanhas_ProdutosOferta to Public
GO

