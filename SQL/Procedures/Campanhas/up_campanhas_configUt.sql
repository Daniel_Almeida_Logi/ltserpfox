/*
	exec up_campanhas_configUt 'Loja 1'

*/
if OBJECT_ID('[dbo].[up_campanhas_configUt]') IS NOT NULL
	drop procedure dbo.up_campanhas_configUt
go

create procedure dbo.up_campanhas_configUt
	@site varchar(60)

/* WITH ENCRYPTION */
AS


	Select 
		ID_Regra = isnull(campanhas_ut.id,0)
		,* 
	from 
		campanhas (nolock)
		left join campanhas_ut (nolock) on campanhas.id = campanhas_ut.campanhas_id
	where 
		(campanhas.site = @site or @site = '')
		and campanhas.inativo = 0
		and campanhas.eliminada = 0
		--and convert(date,getdate()) between campanhas.dataInicio and campanhas.dataFim
	

GO
Grant Execute On dbo.up_campanhas_config to Public
Grant Control On dbo.up_campanhas_config to Public
GO

