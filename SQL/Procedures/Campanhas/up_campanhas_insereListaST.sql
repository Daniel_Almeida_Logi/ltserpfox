/*
	exec up_campanhas_insereListaST 1

*/
if OBJECT_ID('[dbo].[up_campanhas_insereListaST]') IS NOT NULL
	drop procedure dbo.up_campanhas_insereListaST
go

create procedure dbo.up_campanhas_insereListaST
	@id int

/* WITH ENCRYPTION */
AS
	
	insert into campanhas_st_lista (ref)
	Select 
		distinct st.ref 
	from 
		st (nolock)
		left join campanhas_st_lista (nolock) on st.ref = campanhas_st_lista.ref
	where 
		campanhas_st_lista.ref is null
		and st.inactivo=0

	update campanhas_st_lista set campanhas = campanhas + '('+ RTRIM(LTRIM(STR(@id)))+')' where campanhas not like '%('+ RTRIM(LTRIM(STR(@id)))+')%'


GO
Grant Execute On dbo.up_campanhas_insereListaST to Public
Grant Control On dbo.up_campanhas_insereListaST to Public
GO

