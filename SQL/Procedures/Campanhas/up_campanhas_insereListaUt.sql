/*
	exec up_campanhas_insereListaUt 1

	select * from campanhas_ut_lista

*/
if OBJECT_ID('[dbo].[up_campanhas_insereListaUt]') IS NOT NULL
	drop procedure dbo.up_campanhas_insereListaUt
go

create procedure dbo.up_campanhas_insereListaUt
	@id int

/* WITH ENCRYPTION */
AS

	insert into campanhas_ut_lista (no,estab)	
	Select 
		b_utentes.no
		,b_utentes.estab
	from 
		b_utentes (nolock) 
		left join campanhas_ut_lista (nolock) on b_utentes.no = campanhas_ut_lista.no and b_utentes.estab = campanhas_ut_lista.estab
	where
		campanhas_ut_lista.no is null
		and b_utentes.inactivo = 0
	ORDER BY 
		b_utentes.no
		,b_utentes.estab
	
	/* limpa linha da campanha*/
	update campanhas_ut_lista set campanhas = REPLACE(campanhas,'('+ RTRIM(LTRIM(STR(@id)))+')','') where campanhas like '%('+ RTRIM(LTRIM(STR(@id)))+')%'
	
	update campanhas_ut_lista set campanhas = campanhas + '('+ RTRIM(LTRIM(STR(@id)))+')' where campanhas not like '%('+ RTRIM(LTRIM(STR(@id)))+')%'

GO
Grant Execute On dbo.up_campanhas_insereListaUt to Public
Grant Control On dbo.up_campanhas_insereListaUt to Public
GO

