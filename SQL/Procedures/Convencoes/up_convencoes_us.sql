-- exec up_convencoes_us 35
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_convencoes_us]') IS NOT NULL
    drop procedure up_convencoes_us
go

create PROCEDURE up_convencoes_us
@id as int

/* WITH ENCRYPTION */
AS

	select 
		no = b_us.userno
		,b_us.nome
	from 
		cpt_conv_us
		inner join cpt_conv on cpt_conv.id = cpt_conv_us.id_cpt_conv
		inner join b_us on b_us.userno = cpt_conv_us.id_us
	where
		cpt_conv.id = @id
	Order by
		no	

GO
Grant Execute On up_convencoes_us to Public
Grant Control On up_convencoes_us to Public
go



 