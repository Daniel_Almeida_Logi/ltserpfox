
-- exec up_convencoes_cab 0
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_convencoes_cab]') IS NOT NULL
    drop procedure up_convencoes_cab
go

create PROCEDURE up_convencoes_cab
@id as int

/* WITH ENCRYPTION */
AS

	select 
		cpt_conv.id
		,cpt_conv.descr
		,entidade = B_utentes.nome
		,entidadeNo = B_utentes.no
		,inativo = cpt_conv.inativo
		,cpt_conv.site
		,cpt_conv.inativo
		,cpt_conv.vDtCartao
	from 
		cpt_conv
		inner join B_utentes on B_utentes.no = cpt_conv.id_entidade 
	Where
		cpt_conv.id = @id
	Order by
		cpt_conv.id
		
	
GO
Grant Execute On up_convencoes_cab to Public
Grant Control On up_convencoes_cab to Public
go

 