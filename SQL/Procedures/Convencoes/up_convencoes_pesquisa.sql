

-- exec up_convencoes_pesquisa 10,'', '', '', '1� CONSULTA DE NEUROLOGIA'
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_convencoes_pesquisa]') IS NOT NULL
    drop procedure up_convencoes_pesquisa
go

create PROCEDURE up_convencoes_pesquisa
@top int
,@site as varchar(20)
,@convencao as  varchar(60)
,@entidade as  varchar(55)
,@id as  int = 0

/* WITH ENCRYPTION */
AS

	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosLinhas'))
		DROP TABLE #dadosLinhas

	select 
		top (@top)
		sel = CONVERT(bit,0)
		,cpt_conv.id
		,cpt_conv.descr
		,entidade = B_utentes.nome
		,entidadeNo = B_utentes.no
		,inativo = cpt_conv.inativo
		,cpt_conv.site
		,cpt_conv.vDtCartao
	from 
		cpt_conv (nolock)
		inner join B_utentes (nolock) on B_utentes.no = cpt_conv.id_entidade and B_utentes.entCompart = 1
	Where
		(cpt_conv.site = @site or @site = '')
		and (cpt_conv.descr = @convencao or @convencao = '')
		and (b_utentes.nome = @entidade or @entidade = '')
		and cpt_conv.id = case when @id = 0 then cpt_conv.id else @id end
		
	IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#dadosLinhas'))
		DROP TABLE #dadosLinhas	
	
GO
Grant Execute On up_convencoes_pesquisa to Public
Grant Control On up_convencoes_pesquisa to Public
go

 