

-- exec up_convencoes_lin 1
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_convencoes_lin]') IS NOT NULL
    drop procedure up_convencoes_lin
go

create PROCEDURE up_convencoes_lin
@id as int


/* WITH ENCRYPTION */
AS

	select 
		sel = CONVERT(bit,0)
		,pesquisa = CONVERT(bit,0)
		,cpt_val_cli.*
		,design = isnull((select top 1 design from st where st.ref = cpt_val_cli.ref),'')
		,especialidade = convert(varchar(254),left(isnull(substring((select distinct ', '+nome AS 'data()' from b_cli_stRecursos where b_cli_stRecursos.ref = cpt_val_cli.ref for xml path('')),3, 255) ,''),254))
		,convencao = cpt_conv.descr
	from 
		cpt_conv (nolock)
		inner join cpt_val_cli (nolock) on cpt_val_cli.id_cpt_conv = cpt_conv.id
	Where
		cpt_conv.id = @id
	Order by
		(select top 1 design from st where st.ref = cpt_val_cli.ref)
		
	
GO
Grant Execute On up_convencoes_lin to Public
Grant Control On up_convencoes_lin to Public
go



 