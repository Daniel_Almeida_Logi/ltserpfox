/*
-- =============================================
-- Author:        João Barbosa 
-- Create date:   2024-08-08
-- Description:   Extração de Informação de Vendas para o Grupo Somos, conforme solicitado pelo cliente:
--
--		Ficheiro de Stocks: CNP, DtValidade, StkAtual, Pcst apos desconto, PVP
--		Periodicidade: Enviar às 03h00; 07h00; 11H00; 15H00; 19h00 e 23h00. 
--		Farmácias Abrangidas: 19763-Frm Claro Russo (Mem Martins), 04200-Frm Camelo (Avanca), 22977-Frm Beatriz (Braga), 11762-Frm Barbosa (Penafiel), 19224-Frm Brito (Amadora)  e  911pharma (Parafarmacia do Somos).
-- 
-- Exemplo de uso:
-- exec up_Export_Stocks_Somos
-- =============================================
*/


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_Export_Stocks_Somos]') IS NOT NULL
    DROP PROCEDURE dbo.up_Export_Stocks_Somos
GO

CREATE PROCEDURE [dbo].up_Export_Stocks_Somos

AS

WITH StockData AS (
    SELECT 
        'CNP'                    AS 'CNP',
        'DtValidade'             AS 'DtValidade',
        'StkAtual'               AS 'StkAtual',
        'Pcst apos desconto'	 AS 'Pcst apos desconto',
        'PVP'                    AS 'PVP'
    UNION ALL
    SELECT 
        LTRIM(RTRIM(CONVERT(VARCHAR, REF)))  AS 'CNP',
        CONVERT(VARCHAR, VALIDADE,103)       AS 'DtValidade',
        CONVERT(VARCHAR, STOCK)              AS 'StkAtual',
        CONVERT(VARCHAR, EPCULT)             AS 'Pcst apos desconto',
        CONVERT(VARCHAR, EPV1)               AS 'PVP'
    FROM 
        ST (NOLOCK)
    WHERE 
        REF NOT LIKE 'R0%' 
        AND REF NOT LIKE 'V%' 
        AND REF != '' 
        AND INACTIVO = 0
)
SELECT 
    *
FROM 
    StockData


GO
GRANT EXECUTE ON dbo.up_Export_Stocks_Somos TO PUBLIC
GO