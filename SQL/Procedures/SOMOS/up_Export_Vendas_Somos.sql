/*
-- =============================================
-- Author:        João Barbosa 
-- Create date:   2024-08-08
-- Description:   Extração de Informação de Vendas para o Grupo Somos, conforme solicitado pelo cliente:
--
--		Ficheiro Vendas: NºVda, NºAtd, CNP, DtHoraMin, Operador, QtdVda, CustoTotalVda, PVPtotal
--		Periodicidade: Enviar às 03h00; 07h00; 11H00; 15H00; 19h00 e 23h00. 
--		Farmácias Abrangidas: 19763-Frm Claro Russo (Mem Martins), 04200-Frm Camelo (Avanca), 22977-Frm Beatriz (Braga), 11762-Frm Barbosa (Penafiel), 19224-Frm Brito (Amadora)  e  911pharma (Parafarmacia do Somos).
-- 
-- Exemplo de uso:
-- exec up_Export_Vendas_Somos '2024-12-11','2024-12-11','11:00:00','15:00:00'
-- =============================================
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

IF OBJECT_ID('[dbo].[up_Export_Vendas_Somos]') IS NOT NULL
	DROP PROCEDURE dbo.up_Export_Vendas_Somos
GO

CREATE PROCEDURE [dbo].up_Export_Vendas_Somos

@dataini datetime,
@datafim datetime,
@horaini datetime,
@horafim datetime


AS
/*Elimina tabelas*/
	If OBJECT_ID('tempdb.dbo.#totais') IS NOT NULL
		DROP TABLE #totais

SELECT	fi.ftstamp
		,CONVERT(varchar,SUM(CONVERT(numeric(15,2),fi.epcp)))		AS 'CustoTotalVda'
		,CONVERT(varchar,SUM(CONVERT(numeric(15,2),fi.u_epvp)))		AS 'PVPtotal'
	INTO #totais
	FROM fi (NOLOCK) 
		where fi.ousrdata+fi.ousrhora between CONCAT(CONVERT(varchar,@dataini,23),' ',CONVERT(varchar,@horaini,108)) and CONCAT(CONVERT(varchar,@datafim,23),' ',CONVERT(varchar,@horafim,108))
	GROUP BY Fi.ftstamp

SELECT 'N_Vda' AS 'N_Vda',
       'N_Atd' AS 'N_Atd',
       'CNP' AS 'CNP',
       'DtHoraMin' AS 'DtHoraMin',
       'Operador' AS 'Operador',
       'QtdVda' AS 'QtdVda',
       'CustoTotalVda' AS 'CustoTotalVda',
       'PVPtotal' AS 'PVPtotal'

UNION ALL

SELECT 
    CONVERT(varchar, ft.fno)										AS 'NºVda',
    CONVERT(varchar, u_nratend)										AS 'NºAtd',
    fi.ref															AS 'CNP',
    CONCAT(CONVERT(varchar, ft.ousrdata, 103), ' ', 
           CONVERT(varchar, ft.ousrhora, 108))			            AS 'DtHoraMin',
    ft.ousrinis												        AS 'Operador',
    CONVERT(varchar, CONVERT(numeric(11,2), fi.qtt))			    AS 'QtdVda',
    ISNULL(CONVERT(varchar, CustoTotalVda), 'N/D')					AS 'CustoTotalVda',
    ISNULL(CONVERT(varchar, PVPtotal), 'N/D')						AS 'PVPtotal'
	FROM ft (NOLOCK) 
		inner join fi (NOLOCK)
			ON fi.ftstamp=ft.ftstamp
		left join #totais
			ON ft.ftstamp = #totais.ftstamp
	WHERE REF not like 'R0%' 
		AND REF not like 'V%'
		AND REF != ''
		AND fi.ousrdata+fi.ousrhora between CONCAT(CONVERT(varchar,@dataini,23),' ',CONVERT(varchar,@horaini,108)) and CONCAT(CONVERT(varchar,@datafim,23),' ',CONVERT(varchar,@horafim,108))
	ORDER BY 
		DtHoraMin DESC

/*Elimina tabelas*/
	If OBJECT_ID('tempdb.dbo.#totais') IS NOT NULL
		DROP TABLE #totais

GO
GRANT EXECUTE ON dbo.up_Export_Vendas_Somos TO PUBLIC
GRANT CONTROL ON dbo.up_Export_Vendas_Somos TO PUBLIC
GO

