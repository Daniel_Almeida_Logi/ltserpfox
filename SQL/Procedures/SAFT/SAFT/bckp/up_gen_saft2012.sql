--exec up_gen_saft2012 '20120101','20121231','LOja 1'
if OBJECT_ID('[dbo].[up_gen_saft2012]') IS NOT NULL
		drop procedure dbo.up_gen_saft2012
	go

	create procedure [dbo].[up_gen_saft2012] 
		@dataini		date, 
		@datafim		date,
		@Loja			varchar(20)
	/* with encryption  */
	as 
	begin

		/*
			Stored Procedure para gerar o ficheiro SAFTPT em formato validado pela DGCI
			Autor:						Ivo Pereira
			Data de Cria��o:			16/02/2010
			Data de Modifica��o:		08/05/2012
			Vers�o:						3.1.1
		
			FALTA ALTERAR: 
			aplicar REPLACE char(2) a todos os campos COmpanyName

			
			Notas:	
			> Aplicado LTRIM no campo NCONT na ficha de empresa, o espa�o nao passava na REGEX de valida��o!
			> Aplicado PostalCode 0000-000 quando � vazio em TODOS os postalcode
			> Aplicar leitura dinamica da versao e nome do software na BENDER!
			> aplicar REPLACEs todos da ST design a FI design
			> adicionado REPLACE para o char(14) que apareceu em Coimbra.. 
			> Adicionado LEFT(20) ao campo TELEPHONE por causa do SOUTO : telefone ; telemovel no mesmo campo (!)
			> Adicionada a serie do documento NDOC da FT na gera��o dos Clientes Virtuais do SAFT-T tanto na tabela Customer como na tabela SalesInvoices												
			> O Campo References tem que ser emitido noutra posi��o caso o documento seja do tipo NC!
			> Adicionada filtragem campo epromo=0 para excluir linhas desconto
			> A clausula FOR XML PATH foi alterada nas subtabelas BillingAddress dentro da tabela Customer de forma a permitir agregar com UNION ALL os dois querys, em que todas as colunas tem que ter nomes ( o BillingAddress era renderizado mas o campo no SELECT nao tinha nome)
			> Quando o doc est� classificado como NC no tiposaft (n�o como 7 no u_tipodoc da TD!!!) � exportado o REFERENCES e nao ORDERREFERENCES
			> Corrigido o USRHORA/DATA mal exportado, eles querem date de CRIA��O e nao altera��o (supostamente esta agora nao pode existir :P )
			> usar NULL com CASE quando nao queremos um campo OU subquery com CASE para subtabelas inteiras a ser exportado no SAFT-T
			> queries de c�lculo dos totais fazem SUM ao mesmo query de calculo do valor das linhas e agora os valores conferem
			> Desconto financeiro ao documento TODO foi adicionado ao GrossTotal
			> Requer que todos os Docs estejam classificados com o Tipo SAFT da PHC e que o documento(s) "Inser��o de Receita" que n�o mexe em stock esteja EXCLUIDO do saft usando o campo da PHC!
			> Requer que exista o artigo '9999999' na ST para exporta��o correcta das Facturas Entidades, dado que s�o geradas automaticamnete com linhas sem refer�ncia
		
			Todo list:

			> Aplicar o NULL nos subqueries da contabilidade detectando uando o PHC est� integrado com a Contabilidade
			> Alterar o texto "Isento" nas linhas com IVA=0 para o texto de disposi��o legal que o Paulo indicar
			> Validar origens de linhas em outros tipos de documentos que n�o Factura��o, ou seja, Dossiers Internos e Compras; de momento apenas temos origens na Factura��o!
			> Resolver o problema do lookup � tabela REGIVA que nao existe no PHC2010 e impede a SP de compilar
			> Tentar parametrizar para BDs com Contabilidade integrada (FFS) caso a PHC nao certifique o software a exporta��o das tabelas GeneralLedger; � tamb�m necess�rio exportar o campo TransactionID caso esta integra��o exista na SourceDocuments!!!
			> Rever a classifica��o 'N' fixa para todos os documentos, como classificar cada um?
			> Considerar a lista de caracteres invalida no XML sem usar os ESCAPECHARS: &,",',<,>, implementar REPLACEs de forma mais dinamica, em vez de ser refer�ncia a refer�ncia talvez no Doc inteiro?
		*/

		begin try
			
			set nocount on																	  
			set xact_abort on
			set language portuguese
			
			/* Criar variavel com nome do cliente 200*/
			declare @nomeClienteDuzentos varchar(30)
			set @nomeClienteDuzentos = 'CONSUMIDOR FINAL'
			declare @nomeClienteDuzentos2 varchar(30)
			set @nomeClienteDuzentos2 = '.'
			
			/* Converter os valores de data enviados pelo PHC para outro formato */
			select
				@dataini=convert(varchar,convert(datetime,@dataini),23),
				@datafim=convert(varchar,convert(datetime,@datafim),23)
				
			/* Parte 1 do SELECT principal, adiciona o string do encoding XML e converte o restante texto de XML para varchar(MAX) */
			select convert(text,replace(convert(varchar(max),(
				select 
					
					/* Tabela Header: dados da Ficha da Empresa e vers�o SAFT/PHC */
					(select top 1 
						AuditFileVersion =																	'1.01_01',
						CompanyID = 																		case 
																												when consreg<>'' then ltrim(rtrim(CONVERT(varchar,consreg))) + ' ' + ltrim(rtrim(CONVERT(varchar,ncont))) 
																												else ltrim(rtrim(CONVERT(varchar,ncont)))
																											end,
						TaxRegistrationNumber =																ncont,																													
						TaxAccountingBasis = 																'F'	,									/* � permitido apenas exportar Factura��o no n/ SAFT e como tal este campo tem que indicar 'F' */																														
						CompanyName =																		nomecomp,
						BusinessName =																		case when nomabrv='' then nomecomp else nomabrv end,
						
						/* Subtabela CompanyAddress*/												
						(select
							AddressDetail =																	case when morada='' then 'Desconhecido' else morada end,
							City =																			case when local='' then 'Desconhecido' else local end	,
							/* 
							PostalCode =																	case when codpost='' then 'Desconhecido' else left(codpost,8) end	,
							*/
							
							PostalCode	=																	case when codpost='' then '0000-000' else LEFT(codpost,8) end,
							
							Region =																		case when freguesia='' then 'Desconhecido' else freguesia end,											
							Country=																		'PT'
						for xml path ('CompanyAddress'), type
						),
						
						FiscalYear =																		convert(varchar,DATEPART(year,@dataini)),
						StartDate =																			@dataini	,				 
						EndDate =																			@datafim,				 
						CurrencyCode =																		'EUR',
						DateCreated =																		convert(date,GETDATE()),
						TaxEntity =																			'Sede',
						ProductCompanyTaxID =																'508935490'	,
						SoftwareCertificateNumber =															0,
						ProductID =																			'Logitools/Logitools',
						ProductVersion =																	case 
																												/* Se nao tivermos versao ou a tabela nao existir, ou algum outro problema, fazer OUTPUT do valor no qual o software foi certificado originalmente */
																												when (select top 1 textValue from b_parameters where stamp='ADM0000000156') = '' or (select top 1 textValue from b_parameters where stamp='ADM0000000156') is null then '11.0.0' 
																												else (select top 1 textValue from b_parameters where stamp='ADM0000000156') 
																											end,
					/*	
						ProductVersion =																	'11.0.0', 
						Campo antigo: Detemrinar se isto � aceite pela DCGI nas novas versoes do software!
					*/
						Telephone =																			left(case when telefone='' then '0000000000' else convert(varchar,telefone) end,20),
						Fax =																				case when fax='' then '0000000000' else fax end,
						Email =																				case when email='' then 'Desconhecido' else email end 
					from empresa
					for xml path ('Header'), type
					),
					
					/* Subtabela MasterFiles */
					(select
						
						/* GeneralLedger: Dados do Plano de Contas, desactivados para j� */
						(select
							case 
								when 0=1 then 	
									(Select 
										AccountID =														conta,
										AccountDescription =											descricao,
										OpeningDebitBalance =											0,
										OpeningCreditBalance =											0
									 from pc (nolock) 
									 where 
										ano=convert(varchar,DATEPART(year,@dataini))
										and (conta not like '0%'  and conta not like '9%' ) 
									order by conta 
									for XML path ('GeneralLedger'), type
									)
								else null
							end
						),
																																														
						/* Tabela TaxTable: Tabela de Regimes de IVA */
						(select
							(select
								TaxType =																'IVA',
								TaxCountryRegion =														'PT', 
								TaxCode =																codigo, 
								Description =															desctaxa, 
								TaxPercentage =															taxa
							from regiva (nolock)
					
							order by 
								descricao
							for XML path ('TaxTableEntry'), type)
						for Xml path ('TaxTable'), type),
					
						/*Tabela Product: tabela de Produtos*/
						(Select 
							ProductType = 
																										case 
																											when st.stns=1 then 'S'
																											when st.stns=1 and st.ref='9999999' then 'O' 
																											else 'P' 
																										end, 
							ProductCode =																	ltrim(rtrim(convert(varchar,st.ref)))	, 
							ProductGroup =																case when st.faminome='' then 'Desconhecido' else st.faminome end,
							ProductDescription = 														case 
																											/* os REPLACES cortam muito espa�o no ficheiro diminuindo a tabela de produtos e removem os CHARs invalidos nas specs do XML*/
																											when st.design like '%'+char(39)+'%' then ltrim(rtrim(replace(st.design,CHAR(39),'')))
																											when st.design like '%'+char(34)+'%' then ltrim(rtrim(replace(st.design,CHAR(34),'')))
																											when st.design like '%'+char(38)+'%' then ltrim(rtrim(replace(st.design,CHAR(38),'')))
																											when st.design like '%'+char(14)+'%' then ltrim(rtrim(replace(st.design,CHAR(14),'')))
																											when st.design like '%'+char(2)+'%' then ltrim(rtrim(replace(st.design,CHAR(2),'')))
																											when st.design like '%<%' then ltrim(rtrim(replace(st.design,'<','')))
																											when st.design like '%>%' then ltrim(rtrim(replace(st.design,'>','')))
																											when st.design is null or st.design='' then 'Desconhecido'
																											else ltrim(rtrim(convert(varchar,st.design))) 
																										end,
							ProductNumberCode =														case when convert(varchar,st.codigo)	='' then ltrim(rtrim(convert(varchar,st.ref))) else convert(varchar,st.codigo)end
						from st (nolock) 
						order by 
							st.ref
						for xml path ('Product'), type
						), 					

						/* Tabela Customer: Tabela de Clientes */
						(select * from 
							(select
								CustomerID =																convert(varchar,no)+'/'+CONVERT(varchar,estab),
								AccountID =																	case when conta='' then 'Desconhecido' else conta end,
								CustomerTaxID =																case when ncont='' then '999999990' else ncont end,
								CompanyName = 																case when nome=@nomeClienteDuzentos then 'Consumidor final' else replace(nome,char(39),'') end,
								Contact =																	'Desconhecido',
								
								/* Subtabela BillingAddress*/ 
								BillingAddress = (
									select
										AddressDetail =														case when morada='' then 'Desconhecida' else replace(morada,char(39),'') end,
										City =																	case when local='' then 'Desconhecida' else replace(local,char(39),'') end,
										PostalCode =															case when codpost='' then '0000-000' else left(convert(varchar,codpost),8) end,
										Country =																'PT' 
									for XML path (''), type
								),
								
								Telephone =																	case 
																														when telefone!='' and telefone!=telefone then left(convert(varchar,telefone),20)
																														when telefone='' then '0000000000' 
																														else left(convert(varchar,telefone),20)
																													end,						
								Fax =																				case when fax='' then '0000000000' else convert(varchar,fax) end, 
								SelfBillingIndicator =															0
							from b_utentes (nolock)
		
							union all 
							
							/* 
								Clientes Virtuais, n�o existem na tabela de Clientes; ID � gerado de forma sequencial APENAS derivado das vendas feitas ao cliente "Venda a Dinheiro" com altera��es de dados
								nao agreguei este query no acima porque o cliente Vendas a Dinheiro aparecia duplicado e o GROUP BY com campos XML n�o e possivel :S 
								pode ser feito INNER JOIN � b_utentes no cliente 200, existe sempre, e este query apenas renderiza os IDs virtuais do 200
							*/
							
							select
								CustomerID =																/*case 
																												when 
																													(ft.ncont = '999999990' or ft.ncont = '') 
																												then 
																													convert(varchar,ft.no) + '-A'  + convert(varchar,ndoc) + convert(varchar,ft.fno) + '/' + convert(varchar,ft.estab)
																												else
																												/*when ft.ncont<>'999999990' then*/ convert(varchar,ft.no) + '-A'  + convert(varchar,ndoc) + convert(varchar,ft.fno) + '/' + convert(varchar,ft.estab) + '/' + CONVERT(varchar,ft.ncont)
																											end,*/
																											case 
																												/* Vendas feitas ao cliente 200 com altera��o de dados (Clientes Virtuais)*/ 
																												when (ft.ncont='999999990' or ft.ncont='') and (ft.nome!=@nomeClienteDuzentos or ft.nome!=@nomeClienteDuzentos2 or ft.morada!='' or ft.telefone!='') then convert(varchar,ft.no) + '-A'  + convert(varchar,ft.ndoc) + convert(varchar,ft.fno) + '/' + convert(varchar,ft.estab)
																												when (ft.ncont!='999999990' or ft.ncont!='') and (ft.nome!=@nomeClienteDuzentos or ft.nome!=@nomeClienteDuzentos2 or ft.morada!='' or ft.telefone!='') then convert(varchar,ft.no) + '-A'  + convert(varchar,ft.ndoc) + convert(varchar,ft.fno) + '/' + convert(varchar,ft.estab) + '/' + CONVERT(varchar,ft.ncont)

																												/* Vendas feitas ao cliente "Venda a Dinheiro" sem altera��o de dados*/
																												when (ft.ncont='999999990' or ft.ncont='')	then convert(varchar,ft.no)+'/'+convert(varchar,ft.estab)
																												when (ft.ncont!='999999990' or ft.ncont!='') then convert(varchar,ft.no)+'/'+convert(varchar,ft.estab) + '/' + CONVERT(varchar,ft.ncont)																												
																											end,
								AccountID =																	case 
																												when b_utentes.conta='' then 'Desconhecido' 
																												else b_utentes.conta 
																											end,
								CustomerTaxID =																case when ft.ncont='' then '999999990' else ft.ncont end,
								CompanyName = 																case when (ft.nome=@nomeClienteDuzentos or ft.nome=@nomeClienteDuzentos2) then 'Consumidor final' else replace(ft.nome,char(39),'') end,
								Contact =																	'Desconhecido',
								
								/* Subtabela BillingAddress*/ 
								BillingAddress = (
									select
										AddressDetail =															case when ft.morada='' then 'Desconhecida' else replace(ft.morada,char(39),'') end,
										City =																		case when ft.local='' then 'Desconhecida' else replace(ft.local,char(39),'') end,
										PostalCode =																case when ft.codpost='' then '0000-000' else left(convert(varchar,ft.codpost),8) end,
										Country =																	'PT' 
									for XML path (''), type
								),
								
								Telephone =																	case 
																														when ft.telefone!='' and ft.telefone!=b_utentes.telefone then left(convert(varchar,ft.telefone),20)
																														when b_utentes.telefone='' then '0000000000' 
																														else left(convert(varchar,b_utentes.telefone),20)
																													end,						
								Fax =																				case when fax='' then '0000000000' else convert(varchar,fax) end, 
								SelfBillingIndicator =															0
							from ft (nolock)																	/* Aqui os dados sao lidos da FT e nao da b_utentes propositamente*/
							inner join b_utentes on ft.no=b_utentes.no														
							where
								ft.ftano = datepart(year,@dataini)
								and ft.no=200
								and (
									ft.nome!=@nomeClienteDuzentos																	/* Campos que, quando alterados, devem gerar os novos IDs virtuais*/
									or ft.nome!=@nomeClienteDuzentos2
									or ft.ncont!='999999990'
									or ft.ncont!=''
									or ft.morada!=''
									or ft.telefone!=''
								)		
							) x 
							
						for xml path ('Customer'), type																																				
						),					
						
						/* Tabela Supplier: Fornecedores */
						(Select 
							SupplierID =																convert(varchar,fl.no)+'/' + convert(varchar,fl.estab),
							AccountID =																	case when fl.conta='' then 'Desconhecido' else fl.conta end, 
							SupplierTaxID =																case when fl.ncont='' then '999999990' else fl.ncont end,						
							CompanyName =																fl.nome,
							Contact =																		case when fl.contacto='' then 'Desconhecido' else convert(varchar,fl.contacto) end,
							
							/* Subtabela BillingAddress*/ 
							(select																																										
								AddressDetail =															case when fl.morada='' then 'Desconhecido' else replace(fl.morada,char(39),'') end,
								City =																		case when fl.local='' then 'Desconhecido' else local end,
								/*
								PostalCode =																case when fl.codpost='' then 'Desconhecido' else left(convert(varchar,codpost),8) end,
								*/
								PostalCode	=																case when fl.codpost='' then '0000-000' else LEFT(convert(varchar,fl.codpost),8) end,
								Country =																	'PT' 
							for xml path ('BillingAddress'), type
							),
							Telephone =																	case when fl.telefone='' then '0000000000' else left(convert(varchar,fl.telefone),20) end,
							Fax =																				case when fl.fax='' then '0000000000' else convert(varchar,fl.fax) end,
							SelfBillingIndicator =															0 
						from fl (nolock) 
						order by 
							fl.no, fl.estab 
						for xml path ('Supplier'), type
						)
				
					/* Final da tabela MasterFiles */
					for XML path ('MasterFiles'), type
					),
				
	/*
					/* Subtabela GeneralLedgerEntries: INACABADA!!!! */
					(select  
						 
						 (select
							dino																					as JournalID,
							dinome																				as Description,
							(select
							
								/* falta o subquery para o detalhe (linhas) de cada transac��o */
								(select
								
								for xml path ('Line'), type)
							
							for xml path ('Transaction'), type)
						from do (nolock)
						where 
							Year(data) = datepart(year,@dataini) 
							and do.mes>0
						group by 
							dino,dinome
						order by 
							dino,dinome
						for xml path ('Journal'), type)
					
					for xml path ('GeneralLedgerEntries'), type
					),
	*/
				
					/* Tabela SourceDocuments */
					(select
						
						/* Tabela SalesInvoices */
						(select
							
							NumberOfEntries = (
								select 
									IsNull(Count(ftstamp),0) 
								from ft (nolock)
								inner join td (nolock) on ft.ndoc=td.ndoc
								where 
									ft.fdata between @dataini and @datafim 
									and anulado=0 
									and len(td.tiposaft) >= 2
									and tiposaft not in ('GT','GR','GA','GC','GD')
									and ft.site = case when @Loja = 'TODAS' then ft.site else @Loja end
							),
		
							TotalDebit = (
								select
									/*	substring(convert(varchar,round(abs(sum(x.TotalLinha)),2)),1,patindex('%.%',convert(varchar,round(abs(sum(x.TotalLinha)),2)))+2)  */
									STR(round(abs(sum(x.TotalLinha)),2),10,2) from (
										select 
											case 
												when (fi.etiliquido / (fi.iva/100+1))<0 then (fi.etiliquido / (fi.iva/100+1)) 
												else 0 
											end as TotalLinha
										from ft (nolock) 
										inner join td on ft.ndoc=td.ndoc
										inner join fi on ft.ftstamp=fi.ftstamp
										where 
											ft.fdata between @dataini and @datafim
											and anulado=0 
											and len(td.tiposaft) >= 2
											and td.tiposaft not in ('GT','GR','GA','GC','GD')
											and  ((fi.qtt <> 0 or fi.etiliquido <> 0) OR  ( ft.anulado = 1 ))
											and ft.site = case when @Loja = 'TODAS' then ft.site else @Loja end
									   ) as x																																 
							),
						
							TotalCredit =(
								select 
									/*	substring(convert(varchar,round(abs(sum(x.TotalLinha)),2)),1,patindex('%.%',convert(varchar,round(abs(sum(x.TotalLinha)),2)))+2) from ( */
									STR(round(abs(sum(x.TotalLinha)),2),10,2) from (	
										select 
											case 
												when (fi.etiliquido / (fi.iva/100+1))>0 then (fi.etiliquido / (fi.iva/100+1))
												else 0 
											end as TotalLinha
										from ft
										inner join td on ft.ndoc=td.ndoc
										inner join fi on ft.ftstamp=fi.ftstamp
										where 			
											ft.fdata between @dataini and @datafim
											and anulado=0 
											and len(td.tiposaft) >= 2
											and td.tiposaft not in ('GT','GR','GA','GC','GD')
											and  ((fi.qtt <> 0 or fi.etiliquido <> 0) OR  ( ft.anulado = 1 ))
											and ft.site = case when @Loja = 'TODAS' then ft.site else @Loja end
										) as x
							),
							
							/* Subtabela Invoice */
							(select
								InvoiceNo =																	case when ft.ndoc=3 then 'VD' else td.tiposaft end + ' ' + convert(varchar,ft.ndoc) + '/' + CONVERT(varchar,ft.fno),
								InvoiceStatus =																case when ft.anulado = 0 then 'N' else 'A' end,																											
								Hash =																		isnull(ltrim(rtrim(left(b.hash,172))),'0'),
								HashControl =																case when b.hash is null or b.hash='' then '0' else '1' end,
								Period =																	DATEPART(month,ft.fdata),	
								InvoiceDate =																convert(date,ft.fdata), 
								InvoiceType =																case when ft.ndoc=3 then 'VD' else td.tiposaft end,
								SelfBillingIndicator =														0,
								SystemEntryDate =															convert(varchar,convert(date,ft.ousrdata))+'T'+CONVERT(varchar,case when ft.ousrhora='' then '00:00:00' else ft.ousrhora end),
							/*	TransactionID =																'', */
								CustomerID =																	case 
																														/* Vendas feitas ao cliente 200 com altera��o de dados (Clientes Virtuais)*/ 
																														when ft.no=200 and (ft.ncont='999999990' or ft.ncont='') and (ft.nome!=@nomeClienteDuzentos or ft.nome!=@nomeClienteDuzentos2 or ft.morada!='' or ft.telefone!='') then convert(varchar,ft.no) + '-A'  + convert(varchar,ft.ndoc) + convert(varchar,ft.fno) + '/' + convert(varchar,ft.estab)
																														when ft.no=200 and (ft.ncont!='999999990' or ft.ncont!='') and (ft.nome!=@nomeClienteDuzentos or ft.nome!=@nomeClienteDuzentos2 or ft.morada!='' or ft.telefone!='') then convert(varchar,ft.no) + '-A'  + convert(varchar,ft.ndoc) + convert(varchar,ft.fno) + '/' + convert(varchar,ft.estab) + '/' + CONVERT(varchar,ft.ncont)

																														/* Vendas feitas ao cliente "Venda a Dinheiro" sem altera��o de dados*/
																														when ft.no=200 and (ft.ncont='999999990' or ft.ncont='')	then convert(varchar,ft.no)+'/'+convert(varchar,ft.estab)
																														when ft.no=200 and (ft.ncont!='999999990' or ft.ncont!='') then convert(varchar,ft.no)+'/'+convert(varchar,ft.estab) + '/' + CONVERT(varchar,ft.ncont)
																														
																														/* Vendas feitas aos restantes clientes*/
																														when ft.no!=200/* and ft.ncont='999999990'*/ then convert(varchar,ft.no)+'/'+convert(varchar,ft.estab)
																														/*when ft.no<>200 and ft.ncont<>'999999990' then convert(varchar,ft.no)+'/'+convert(varchar,ft.estab)*/
																													end,
								/* Subtabela Line */
								(select
									LineNumber =																	ROW_NUMBER() over (order by fi.lordem asc),																									
					
									/* Subtabela OrderReferences: origens de cada linha nas tabelas de Factura��o de todos os documentos excepto Notas de Cr�dito! */  
									(select
										case
											when td.tiposaft!='NC' then 																															
												(select   
													OriginatingON =														n.nmdoc + ' ' + convert(varchar,n.ndoc)+'/'+CONVERT(varchar,n.fno),
													OrderDate =															convert(date,ft.fdata)			
												from fi as n
												where
													n.fistamp=fi.ofistamp
												for xml path ('OrderReferences'), type)
											else
												null 
										end
									),
									 
									ProductCode =																case when fi.ref='' then '9999999' else fi.ref end,             
								/*	ProductDescription =														fi.design, */
									ProductDescription = 														case 
																													/* os REPLACES cortam muito espa�o no ficheiro diminuindo a tabela de produtos e removem os CHARs invalidos nas specs do XML*/
																													when fi.design like '%'+char(39)+'%' then ltrim(rtrim(replace(fi.design,CHAR(39),'')))
																													when fi.design like '%'+char(34)+'%' then ltrim(rtrim(replace(fi.design,CHAR(34),'')))
																													when fi.design like '%'+char(38)+'%' then ltrim(rtrim(replace(fi.design,CHAR(38),'')))
																													when fi.design like '%'+char(14)+'%' then ltrim(rtrim(replace(fi.design,CHAR(14),'')))
																													when fi.design like '%'+char(2)+'%' then ltrim(rtrim(replace(fi.design,CHAR(2),'')))
																													when fi.design like '%<%' then ltrim(rtrim(replace(fi.design,'<','')))
																													when fi.design like '%>%' then ltrim(rtrim(replace(fi.design,'>','')))
																													when fi.design is null or fi.design='' then 'Desconhecido'
																													else ltrim(rtrim(convert(varchar,fi.design))) 
																												end,								
								
									Quantity =																	convert(int,fi.qtt)	,
									UnitOfMeasure =															case
																														when fi.unidade='' then 'UN'								
																														else upper(convert(varchar,fi.unidade))
																													end,
									UnitPrice =																	case
																														when (fi.etiliquido / (fi.iva/100+1)) > 0 then 									
																															case 
																																when fi.ivaincl=1 then abs((fi.etiliquido / (fi.iva/100+1)) / fi.qtt) 
																																else abs(fi.etiliquido/fi.qtt)
																															end
																														else 0
																													end,
									TaxPointDate =															convert(date,getdate()),
									
									/* Campo References, apenas deve aparecer se o documento for do tipo SAFT "NC" e tem que aparecer nesta posi��o (indica��o DGCI); o texto do campo Reason � gen�rico*/
									(select
										case 
											when td.tiposaft='NC' then 
												(select
													(select   
														Reference =											n.nmdoc + ' ' + convert(varchar,n.ndoc)+'/'+CONVERT(varchar,n.fno),
														Reason =												'Regulariza��o / Devolu��o de Produto'
													from fi as n
													where																															
														n.fistamp=fi.ofistamp
													for xml path ('CreditNote'), type
													)	
												for xml path ('References'), type)
											else
												null
										end 
									),								
									
									Description =																fi.design,
									CreditAmount =															case 
																														when (fi.etiliquido / (fi.iva/100+1))>0 then fi.etiliquido / (fi.iva/100+1) 
																														when (fi.etiliquido / (fi.iva/100+1))=0 then 0 
																														else null 
																													end,
									DebitAmount =																case 
																														when (fi.etiliquido / (fi.iva/100+1))<0 then abs(fi.etiliquido / (fi.iva/100+1)) 
																														else null 
																													end,

									/* 
										Subtabela Tax: o subquery com TOP 1 deve-se ao facto de que h� v�rios REGIVAs para o c�digo 0 
									*/
									(select
										TaxType =																'IVA',
										TaxCountryRegion =													'PT',
										TaxCode =																(select top 1 codigo from regiva where tabiva=(case when fi.tabiva=0 then 4 else fi.tabiva end)),
										TaxPercentage =														convert(int,fi.iva) 								
									for xml path ('Tax'), type
									),
		
									TaxExemptionReason =													case when fi.iva=0 then 'Isen��o prevista no n.� 1 do art.� 9.� do CIVA' else null end,
		
									/* 
										Campo SettlementAmount: Resumo de descontos na linha, somados, apenas devolvidos quando existem (indica��o DGCI) 
										Aplicar o mesmo CASE do IVA incluido para calcular o valor (n�o percentagem!) do Desconto Comercial nas linhas, apenas caso este esteja gravado como superior a zero euros
									
									SettlementAmount =														case 
																														when (convert(numeric,desconto))>0 	then 
																															abs(
																																case 
																																	when fi.ivaincl=1 then fi.etiliquido / (fi.iva/100+1) / fi.qtt 
																																	else fi.etiliquido/fi.qtt 
																																end
																															)-
																															(
																																abs(case 
																																	when fi.ivaincl=1 then fi.etiliquido / (fi.iva/100+1) / fi.qtt 
																																	else fi.etiliquido/fi.qtt 
																																end) 
																																* 
																																(100 - abs(convert(numeric,fi.desconto))/100)	
																															)
																														else null 
																													end 
									*/
									
									SettlementAmount =														case 
																														when (convert(numeric,desconto))>0 or (convert(numeric,desc2))>0 or (convert(numeric,u_descval))>0 then 
																															abs(
																																case 
																																	when fi.ivaincl=1 then (fi.epv*fi.qtt) / (fi.iva/100+1)
																																	else fi.epv*fi.qtt
																																end
																															)-
																															abs(
																																case 
																																	when fi.ivaincl=1 then fi.etiliquido / (fi.iva/100+1)
																																	else fi.etiliquido/fi.qtt 
																																end
																															) 
																														else null 
																													end 								
									
									
								/* Final da subtabela Line*/
								from fi as fi
								where
									fi.ftstamp=ft.ftstamp 
									and fi.epromo=0
									and ( (fi.qtt <> 0 or fi.etiliquido <> 0) OR  ( ft.anulado = 1 ))
								order by 
									fi.lordem
								for xml path ('Line'), type
								),
								
								/* Subtabela DocumentTotals */
								(select
									TaxPayable =																str(abs(ft.ettiva),10,2),																															/*substring(convert(varchar,abs(ft.ettiva)),1,patindex('%.%',CONVERT(varchar,ft.ettiva))+2), */
									NetTotal =																	str(abs(ft.eivain1+ft.eivain2+ft.eivain3+ft.eivain4+ft.eivain5+ft.eivain6+ft.eivain7+ft.eivain8+ft.eivain9),10,2),		/*substring(convert(varchar,abs(ft.eivain1+ft.eivain2+ft.eivain3+ft.eivain4+ft.eivain5+ft.eivain6+ft.eivain7+ft.eivain8+ft.eivain9)),1,patindex('%.%',CONVERT(varchar,ft.eivain1+ft.eivain2+ft.eivain3+ft.eivain4+ft.eivain5+ft.eivain6+ft.eivain7+ft.eivain8+ft.eivain9))+2) ,*/
									GrossTotal =																str(abs(ft.etotal+ft.efinv),10,2),																												/*substring(convert(varchar,abs(ft.etotal+ft.efinv)),1,patindex('%.%',CONVERT(varchar,ft.etotal+ft.efinv))+2),*/
									
									/* Subtabela Currrency, apenas exportada quando a moeda de um documento n�o � EURO (indica��o DGCI)*/
									(select 
										case 
											when ft.moeda not in ('PTE ou EURO','EURO','EUR') then 
												(select
													CurrencyCode =											case when ft.moeda='' then 'EUR' else ft.moeda end,
													CurrencyCreditAmount =									str(abs(ft.etotal),10,2)																															/*convert(numeric(10,2),abs(convert(varchar,ft.etotal)))*/
												for xml path ('Currency'), type)  
											else null 
										end
									),	
									
									/* Subtabela Settlement, apenas exportada quando existe Desconto Financeiro no cabe�alho de documento (indica��o DGCI) */
									(select 
										case 
											when ft.efinv<>0 then 				
												(select
													SettlementDiscount =										0,
													SettlementAmount =											ft.efinv,
													SettlementDate =												CONVERT(date,ft.fdata) 
												for xml path ('Settlement'), type
												)
											else null 
										end 
									)
								
								for xml path ('DocumentTotals'),type
								)
								
							from ft  as ft (nolock)
							inner join ft2 (nolock) on ft.ftstamp=ft2.ft2stamp
							inner join td  (nolock) on ft.ndoc=td.ndoc 
							inner join b_utentes  (nolock) ON b_utentes.no=(case when td.lancacli=0 then ft.no else ft2.c2no end)  and b_utentes.estab=(case when td.lancacli=0 then ft.estab else ft2.c2estab end) 
							left join B_cert as b on ft.ftstamp=b.stamp		/* Isto nao deveria ser alterado para INNER? */
							where 
								ft.fdata>=@dataini 
								and ft.fdata<=@datafim
								and len(td.tiposaft) >= 2
								and td.tiposaft not in ('GT','GR','GA','GC','GD')
								and ft.ndoc not in (86,2,6) /*guias de remessa*/
								/* Novo crit�rio que separa documentos por LOJA, para clientes multi-base de dados */
								and ft.site = case when @Loja = 'TODAS' then ft.site else @Loja end
								
							order by
								ft.ndoc,ft.fno 
													
							for xml path ('Invoice'), type)
							
						for xml path ('SalesInvoices'), type)
						
					for xml path ('SourceDocuments'), type)
					
				for xml path (''), root ('AuditFile'), type
				
			)),'<AuditFile>','<?xml version = "1.0" encoding="Windows-1252" standalone="yes"?><AuditFile xmlns="urn:OECD:StandardAuditFile-Tax:PT_1.01_01" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">')) as saft
			
		end try
		
		begin catch
			declare
				@errortext					varchar(256),
				@error_severity			int,
				@error_state				int
			
			select 
				@errortext = isnull(ERROR_PROCEDURE(),'') + ': ' + ERROR_MESSAGE(),
				@error_severity = ERROR_SEVERITY(),
				@error_state = ERROR_state()

			raiserror(@errortext,@error_severity,@error_state)
			return(1)
		end catch
	end

go

grant execute on dbo.up_gen_saft2012 to public
grant control on dbo.up_gen_saft2012 to public

go