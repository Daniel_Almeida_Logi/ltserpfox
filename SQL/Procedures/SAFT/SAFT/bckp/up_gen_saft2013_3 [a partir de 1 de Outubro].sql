/* 
	SP para Criar Ficheiro SAFT v3 

	exec up_gen_saft2013_3 '20170401','20170430','Loja 1'
*/

if OBJECT_ID('[dbo].[up_gen_saft2013_3]') IS NOT NULL
	drop procedure dbo.up_gen_saft2013_3
	go

	create procedure [dbo].[up_gen_saft2013_3] 
		@dataini		date, 
		@datafim		date,
		@Loja			varchar(20)

/* With Encryption */

	as 
	begin

		/*
			Stored Procedure para gerar o ficheiro SAFTPT em formato validado pela DGCI
			Autor: Fernando Carvalho
			Altera��o / Corre��o : Lu�s Leal 31/03/2015
		*/

		begin try
			
			set nocount on																	  
			set xact_abort on
			set language portuguese
			
			/* Criar variavel com nome do cliente 200*/
			declare @nomeClienteDuzentos varchar(30)
			set @nomeClienteDuzentos = 'CONSUMIDOR FINAL'	
			
			/* Converter os valores de data enviados pelo PHC para outro formato */
			select
				@dataini=convert(varchar,convert(datetime,@dataini),23),
				@datafim=convert(varchar,convert(datetime,@datafim),23)
				
			/* Parte 1 do SELECT principal, adiciona o string do encoding XML e converte o restante texto de XML para varchar(MAX) */
			select convert(text,replace(convert(varchar(max),(
				select 
					
					/* Tabela Header: dados da Ficha da Empresa e vers�o SAFT/PHC */
					(select top 1 
						AuditFileVersion =																	left('1.03_01',10),
						CompanyID = 																		case 
																												when consreg<>'' then left(ltrim(rtrim(CONVERT(varchar,consreg))),39) + ' ' + left(replace(ncont,' ',''),10)
																												else left(replace(ncont,' ',''),50)
																											end,
						TaxRegistrationNumber =																left(replace(ncont,' ',''),9),
						TaxAccountingBasis = 																left('F',1)	, /* Fatura��o incluindo os documentos de transporte e os de confer�ncia */
						CompanyName =																		left(nomecomp,100), /* Denomina��o Social / Nome */
						BusinessName =																		case when nomabrv= '' then left(nomecomp,60) else left(nomabrv,60) end, /* Designa��o Social */
						
						/* Subtabela CompanyAddress*/												
						(select
							AddressDetail =																	case 
																												when morada is null or morada='' then 'Desconhecido'
																												else LEFT(
																														replace(replace(replace(replace(replace(replace(replace(replace(morada
																															,CHAR(39),'')
																															,CHAR(34),'')
																															,CHAR(38),'')
																															,CHAR(14),'')
																															,CHAR(2),'')
																															,CHAR(8),'')
																															,'<','')
																															,'>','')
																													,100)
																												end,
							City =																			case when local='' then 'Desconhecido' else left(local,50) end,						
							PostalCode	=																	case when codpost='' then '0000-000' else LEFT(codpost,8) end,
							Region =																		case when distrito='' then null else left(distrito,50) end,											
							Country =																		left('PT',2)
						for xml path ('CompanyAddress'), type
						),
						
						FiscalYear =																		convert(varchar,DATEPART(year,@dataini)),
						StartDate =																			@dataini	,				 
						EndDate =																			@datafim,				 
						CurrencyCode =																		left('EUR',3),
						DateCreated =																		convert(date,GETDATE()),
						TaxEntity =																			(select top 1 left(TaxEntity,20) from empresa (nolock) where empresa.site = case when @Loja = 'TODAS' then empresa.site else @Loja end),
						ProductCompanyTaxID =																left('508935490',20), /* deveria ser par�metro */
						SoftwareCertificateNumber =															(select top 1 left(textValue,4) from B_Parameters (nolock) where stamp='ADM0000000078'),
						ProductID =																			left('Logitools/Logitools',255),
						ProductVersion =																	case 
																												when (select top 1 ltrim(rtrim(textValue)) from b_parameters (nolock) where stamp='ADM0000000156') = '' or (select top 1 ltrim(rtrim(textValue)) from b_parameters (nolock) where stamp='ADM0000000156') is null then left('11.0.0',30)
																												else left((select top 1 ltrim(rtrim(textValue)) from b_parameters (nolock) where stamp='ADM0000000156'),30)
																											end,
						Telephone =																			left(case when telefone='' then null else convert(varchar,telefone) end,20),
						Fax =																				left(case when fax='' then null else fax end,20),
						Email =																				case when email='' then null else left(email,60) end 
					from 
						empresa (nolock)
					where
						empresa.no = (select top 1 no from empresa (nolock) where empresa.site = case when @Loja = 'TODAS' then empresa.site else @Loja END)
					for xml path ('Header'), type
					),
					
					/* Subtabela MasterFiles */
					(select
						
						/* GeneralLedger: Dados do Plano de Contas, desactivados para j�  - validar se � necess�rio incluir isto ou n�o...*/
						--(select
						--	case 
						--		when 0=1 then 	
						--			(Select 
						--				AccountID =														conta,
						--				AccountDescription =											descricao,
						--				OpeningDebitBalance =											0,
						--				OpeningCreditBalance =											0
						--			 from pc (nolock) 
						--			 where 
						--				ano=convert(varchar,DATEPART(year,@dataini))
						--				and (conta not like '0%'  and conta not like '9%' ) 
						--			order by conta 
						--			for XML path ('GeneralLedger'), type
						--			)
						--		else null
						--	end
						--),
					
						/* Tabela Customer: Tabela de Clientes */
						(select * from 
							(select
								CustomerID =																left(convert(varchar,b_utentes.no)+'/'+CONVERT(varchar,b_utentes.estab),30),
								AccountID =																	case when conta='' then 'Desconhecido' else left(conta,30) end,
								CustomerTaxID =																case when ncont in ('', '.') then '999999990' else left(replace(ncont,' ',''),20) end,
								CompanyName = 																case 
																												when nome=@nomeClienteDuzentos then 'Consumidor final' 
																												when nome='' then 'Desconhecido' 
																												else LEFT(
																														replace(replace(replace(replace(replace(replace(replace(replace(nome
																															,CHAR(39),'')
																															,CHAR(34),'')
																															,CHAR(38),'')
																															,CHAR(14),'')
																															,CHAR(2),'')
																															,CHAR(8),'')
																															,'<','')
																															,'>','')
																													,100)
																												end,
								/* Subtabela BillingAddress*/ 
								BillingAddress = (
									select
										AddressDetail =														case 
																												when morada is null or morada='' then 'Desconhecido'
																												else LEFT(
																														replace(replace(replace(replace(replace(replace(replace(replace(morada
																															,CHAR(39),'')
																															,CHAR(34),'')
																															,CHAR(38),'')
																															,CHAR(14),'')
																															,CHAR(2),'')
																															,CHAR(8),'')
																															,'<','')
																															,'>','')
																													,100)
																												end,
										City =																	case when local   = '' then 'Desconhecida' else left(replace(local,char(39),''),50) end,
										PostalCode =															case when codpost = '' then 'Desconhecido' else left(convert(varchar,codpost),20) end,
										Country =																case when codigop = '' then 'Desconhecido' else UPPER(LEFT(RTRIM(LTRIM(codigop)),12)) end
									for XML path (''), type
								),
								
								/* Subtabela ShipToAdrdress*/ 
								ShipToAddress = (
									select
										AddressDetail =														case 
																												when morada is null or morada='' then 'Desconhecido'
																												else LEFT(
																														replace(replace(replace(replace(replace(replace(replace(replace(morada
																															,CHAR(39),'')
																															,CHAR(34),'')
																															,CHAR(38),'')
																															,CHAR(14),'')
																															,CHAR(2),'')
																															,CHAR(8),'')
																															,'<','')
																															,'>','')
																													,100)
																												end,
										City =																	case when local ='' then 'Desconhecida' else left(replace(local,char(39),''),50) end,
										PostalCode =															case when codpost ='' then 'Desconhecido' else left(convert(varchar,codpost),20) end,
										Country =																case when codigop = '' then 'Desconhecido' else UPPER(LEFT(LTRIM(RTRIM(codigop)),12)) end
									for XML path (''), type
								),
								
								Telephone =																	case when telefone='' then null else left(convert(varchar,telefone),20) end,						
								Fax =																		case when fax='' then null else left(convert(varchar,fax),20) end, 
								SelfBillingIndicator =														0
							from
								 b_utentes (nolock)
							inner join (select distinct no, estab from ft (nolock) where fdata between @dataini and @datafim) a on a.no = b_utentes.no and a.estab = b_utentes.estab
							
							union all
							/* Clientes Virtuais, n�o existem na tabela de Clientes; ID � gerado de forma sequencial APENAS derivado das vendas feitas ao cliente "Venda a Dinheiro" com altera��es de dados */
							select
								CustomerID =																case 
																												/* Vendas feitas ao cliente 200 com altera��o de dados (Clientes Virtuais) */ 
																												when ft.nome!=@nomeClienteDuzentos or ft.morada!='' or ft.telefone!='' or ft.ncont!='999999990' 
																													then left(convert(varchar,ft.no) + '-A'  + convert(varchar,ft.ndoc) + '/' + convert(varchar,ft.fno) + '/' + convert(varchar,ft.estab),30)
																												--when ft.ncont='999999990' and (ft.nome!=@nomeClienteDuzentos or ft.morada!='' or ft.telefone!='') then left(convert(varchar,ft.no) + '-A'  + convert(varchar,ft.ndoc) + '/' + convert(varchar,ft.fno) + '/' + convert(varchar,ft.estab),30)
																												--when ft.ncont!='999999990' and (ft.nome!=@nomeClienteDuzentos or ft.morada!='' or ft.telefone!='') then left(convert(varchar,ft.no) + '-A'  + convert(varchar,ft.ndoc) + '/' + convert(varchar,ft.fno) + '/' + convert(varchar,ft.estab),30)

																													/* Vendas feitas ao cliente "Venda a Dinheiro" sem altera��o de dados */
																													else left(convert(varchar,ft.no)+'/'+convert(varchar,ft.estab),30)
																												--when ft.ncont='999999990' 
																												--	then left(convert(varchar,ft.no)+'/'+convert(varchar,ft.estab),30)
																												--when ft.ncont<>'999999990' 
																												--	then left(convert(varchar,ft.no)+'/'+convert(varchar,ft.estab) + '/' + CONVERT(varchar,ft.ncont) + '/' + CONVERT(varchar,ft.fno),30)
																											end
																											,
								AccountID =																	case when b_utentes.conta='' then 'Desconhecido' else left(b_utentes.conta,30) end,
								CustomerTaxID =																case when ft.ncont in ('','.') then '999999990' else left(replace(ft.ncont,' ',''),20) end,
								CompanyName = 																case 
																												when ft.nome=@nomeClienteDuzentos then 'Consumidor final' 
																												when ft.nome='' then 'Desconhecido' 
																												else LEFT(
																														replace(replace(replace(replace(replace(replace(replace(replace(ft.nome
																															,CHAR(39),'')
																															,CHAR(34),'')
																															,CHAR(38),'')
																															,CHAR(14),'')
																															,CHAR(2),'')
																															,CHAR(8),'')
																															,'<','')
																															,'>','')
																													,100)
																												end,
								/* Subtabela BillingAddress*/ 
								BillingAddress = (
									select
										AddressDetail =														/*case when ft.morada='' then 'Desconhecida' else replace(ft.morada,char(39),'') end,*/
																											case 
																												when ft.morada is null or ft.morada='' then 'Desconhecido'
																												else LEFT(
																														replace(replace(replace(replace(replace(replace(replace(replace(ft.morada
																															,CHAR(39),'')
																															,CHAR(34),'')
																															,CHAR(38),'')
																															,CHAR(14),'')
																															,CHAR(2),'')
																															,CHAR(8),'')
																															,'<','')
																															,'>','')
																													,100)
																												end,
										City =																case when ft.local ='' then 'Desconhecida' else replace(ft.local,char(39),'') end,
										PostalCode =														case when ft.codpost ='' then 'Desconhecido' else left(convert(varchar,ft.codpost),20) end,
										Country =															case when b_utentes.codigop = '' then 'Desconhecido' else UPPER(LEFT(RTRIM(LTRIM(codigop)),12)) end
									for XML path (''), type
								),
								
								/* Subtabela ShipToAddress*/ 
								ShipToAddress = (
									select
										AddressDetail =														/*case when ft.morada='' then 'Desconhecida' else replace(ft.morada,char(39),'') end,*/
																											/* os REPLACES cortam muito espa�o no ficheiro e removem os CHARs invalidos nas specs do XML*/
																											case 
																												when ft.morada is null or ft.morada='' then 'Desconhecido'
																												else LEFT(
																														replace(replace(replace(replace(replace(replace(replace(replace(ft.morada
																															,CHAR(39),'')
																															,CHAR(34),'')
																															,CHAR(38),'')
																															,CHAR(14),'')
																															,CHAR(2),'')
																															,CHAR(8),'')
																															,'<','')
																															,'>','')
																													,100)
																												end,
										City =																case when ft.local='' then 'Desconhecida' else replace(ft.local,char(39),'') end,
										PostalCode =														case when ft.codpost='' then 'Desconhecido' else left(convert(varchar,ft.codpost),20) end,
										Country =															case when b_utentes.codigop = '' then 'Desconhecido' else UPPER(LEFT(RTRIM(LTRIM(codigop)),12)) end
									for XML path (''), type
								),
								
								Telephone =																	case 
																												when ft.telefone!='' and ft.telefone!=b_utentes.telefone then left(convert(varchar,ft.telefone),20)
																												when b_utentes.telefone='' then null 
																												else left(convert(varchar,b_utentes.telefone),20)
																											end,						
								Fax =																		case when fax='' then null else left(convert(varchar,fax),20) end, 
								SelfBillingIndicator =														0
							from 
								ft (nolock)		/* Aqui os dados sao lidos da FT e nao da b_utentes propositamente*/
								inner join b_utentes (nolock) on ft.no=b_utentes.no														
							where
								ft.fdata>=@dataini 
								and ft.fdata<=@datafim
								and ft.no=200
								and (
									ft.nome!=@nomeClienteDuzentos		/* Campos que, quando alterados, devem gerar os novos IDs virtuais*/
									or ft.ncont!='999999990'
									or ft.morada!=''
									or ft.telefone!=''
								)		
							) x 
							
						for xml path ('Customer'), type																																				
						),
						
						/* Tabela Supplier: Fornecedores */
						(Select 
							SupplierID =																left(convert(varchar,fl.no)+ '/' + convert(varchar,fl.estab),30),
							AccountID =																	case when fl.conta='' then 'Desconhecido' else left(fl.conta,30) end, 
							SupplierTaxID =																case when fl.ncont='' then '999999990' else left(replace(fl.ncont,' ',''),20) end,						
							CompanyName =																case 
																											when fl.nome='' then 'Desconhecido' 
																											else LEFT(
																													replace(replace(replace(replace(replace(replace(replace(replace(fl.nome
																														,CHAR(39),'')
																														,CHAR(34),'')
																														,CHAR(38),'')
																														,CHAR(14),'')
																														,CHAR(2),'')
																														,CHAR(8),'')
																														,'<','')
																														,'>','')
																												,100)
																											end,
							Contact =																	case when fl.contacto='' then null else left(convert(varchar,fl.contacto),50) end,
							
							/* Subtabela BillingAddress*/ 
							(select																																										
								AddressDetail =															case 
																											when fl.morada is null or fl.morada='' then 'Desconhecido'
																											else LEFT(
																													replace(replace(replace(replace(replace(replace(replace(replace(fl.morada
																														,CHAR(39),'')
																														,CHAR(34),'')
																														,CHAR(38),'')
																														,CHAR(14),'')
																														,CHAR(2),'')
																														,CHAR(8),'')
																														,'<','')
																														,'>','')
																												,100)
																											end,
								City =																	case when fl.local='' then 'Desconhecido' else left(fl.local,50) end,
								PostalCode	=															case when fl.codpost='' then '0000-000' else LEFT(convert(varchar,fl.codpost),20) end,
								Country =																case when fl.pncont = '' then 'PT' else UPPER(LEFT(RTRIM(LTRIM(fl.pncont)),12)) END
							for xml path ('BillingAddress'), type
							),
							
							/* Subtabela ShipFromAddress*/ 
							(select																																										
								AddressDetail =															case 
																											when fl.morada is null or fl.morada='' then 'Desconhecido'
																											else LEFT(
																													replace(replace(replace(replace(replace(replace(replace(replace(fl.morada
																														,CHAR(39),'')
																														,CHAR(34),'')
																														,CHAR(38),'')
																														,CHAR(14),'')
																														,CHAR(2),'')
																														,CHAR(8),'')
																														,'<','')
																														,'>','')
																												,100)
																											end,
								City =																	case when fl.local='' then 'Desconhecido' else left(fl.local,50) end,
								PostalCode	=															case when fl.codpost='' then '0000-000' else LEFT(convert(varchar,fl.codpost),20) end,
								Country =																case when fl.pncont = '' then 'PT' else UPPER(LEFT(LTRIM(RTRIM(fl.pncont)),12)) END
							for xml path ('ShipFromAddress'), type
							),
							
							Telephone =																	case when fl.telefone='' then null else left(convert(varchar,fl.telefone),20) end,
							Fax =																		case when fl.fax='' then null else left(convert(varchar,fl.fax),20) end,
							SelfBillingIndicator =														0 
						from fl (nolock) 
						order by 
							fl.no, fl.estab 
						for xml path ('Supplier'), type
						),
						
						/*Tabela Product: tabela de Produtos*/
						(Select 
							ProductType = 																case 
																											when st.stns=1 then left('S',1)
																											when st.stns=1 and st.ref='9999999' then left('O',1) 
																											else left('P',1) 
																										end, 
							ProductCode =																left(ltrim(rtrim(convert(varchar,st.ref))),30), 
							ProductGroup =																case when st.faminome='' then null else left(st.faminome,50) end,
							ProductDescription = 														case 
																											when st.design is null or st.design='' then 'Desconhecido'
																											else LEFT(
																													replace(replace(replace(replace(replace(replace(replace(replace(st.design
																														,CHAR(39),'')
																														,CHAR(34),'')
																														,CHAR(38),'')
																														,CHAR(14),'')
																														,CHAR(2),'')
																														,CHAR(8),'')
																														,'<','')
																														,'>','')
																												,200)
																											end,
							ProductNumberCode =															case when convert(varchar,st.codigo) = '' then left(ltrim(rtrim(convert(varchar,st.ref))),50) else left(convert(varchar,st.codigo),50) end
						from 
							st (nolock) 
						inner join (select distinct ref from fi (nolock) where ref!='9999999' and rdata between @dataini and @datafim union 
									select distinct oref as ref from fi (nolock) where ref='' and oref!='' and rdata between @dataini and @datafim union
									select ref='9999999') a on a.ref = st.ref
						where
							 st.ref != '' 
							 and st.site_nr = CASE when @Loja = 'TODAS' then st.site_nr ELSE((select top 1 no from empresa (nolock) where empresa.site = @Loja)) END
						order by 
							st.ref
						for xml path ('Product'), type
						),
																																														
						/* Tabela TaxTable: Tabela de Regimes de IVA */
						(select
							(select
								TaxType =																left('IVA',3),
								TaxCountryRegion =														left(descricao,5), 
								TaxCode =																left(codigo,10), 
								Description =															left(desctaxa,255), 
								TaxPercentage =															taxa
							from regiva (nolock)
							order by 
								descricao
							for XML path ('TaxTableEntry'), type)
						for Xml path ('TaxTable'), type)					
									
					/* Final da tabela MasterFiles */
					for XML path ('MasterFiles'), type
					),
				
					/* Tabela SourceDocuments */
					(select
						
						/* Tabela SalesInvoices */
						(select
							
							NumberOfEntries = (
								select 
									IsNull(Count(ftstamp),0) 
								from ft (nolock)
								inner join td (nolock) on ft.ndoc=td.ndoc
								where 
									ft.fdata between @dataini and @datafim  
									--and td.excluisaft=0
									and len(td.tiposaft) >= 2
									and td.tiposaft not in('GT','GR','GA','GC','GD') /*n�o incluir guias de transporte*/
									and ft.site = case when @Loja = 'TODAS' then ft.site else @Loja end
							),
		
							TotalDebit = (
								select
									STR(round(abs(sum(x.TotalLinha)),2),10,2) from (
										select 
											case 
												when (case when fi.ivaincl = 1 then (fi.etiliquido / (fi.iva/100+1)) else fi.etiliquido end) <0 
													then (case when fi.ivaincl = 1 then (fi.etiliquido / (fi.iva/100+1)) else fi.etiliquido end)
												else 
													0 
											end as TotalLinha
										from ft (nolock) 
										inner join td (nolock) on ft.ndoc=td.ndoc
										inner join fi (nolock) on ft.ftstamp=fi.ftstamp
										where 
											ft.fdata between @dataini and @datafim
											and anulado=0 
											--and td.excluisaft=0
											and len(td.tiposaft) >= 2
											and td.tiposaft not in('GT','GR','GA','GC','GD') /*n�o incluir guias de transporte*/
											and ((fi.qtt <> 0 or fi.etiliquido <> 0) OR  ( ft.anulado = 1 ))
											and ft.site = case when @Loja = 'TODAS' then ft.site else @Loja end
									   ) as x																																 
							),
						
							TotalCredit =(
								select 
									STR(round(abs(sum(x.TotalLinha)),2),10,2) from (	
										select 
											case 
												when (case when fi.ivaincl = 1 then (fi.etiliquido / (fi.iva/100+1)) else fi.etiliquido end) >0 
													then (case when fi.ivaincl = 1 then (fi.etiliquido / (fi.iva/100+1)) else fi.etiliquido end)
												else 
													0 
											end as TotalLinha
										from ft (nolock)
										inner join td (nolock) on ft.ndoc=td.ndoc
										inner join fi (nolock) on ft.ftstamp=fi.ftstamp
										where 			
											ft.fdata between @dataini and @datafim
											and anulado=0 
											--and td.excluisaft=0
											and len(td.tiposaft) >= 2
											and td.tiposaft not in('GT','GR','GA','GC','GD') /*n�o incluir guias de transporte*/
											and ((fi.qtt <> 0 or fi.etiliquido <> 0) OR  ( ft.anulado = 1 ))
											and ft.site = case when @Loja = 'TODAS' then ft.site else @Loja end
										) as x
							),
							
							/* Subtabela Invoice */
							(select
								InvoiceNo =																	left(isnull(b.invoiceType,'FT') + ' ' + convert(varchar,ft.ndoc) + '/' + CONVERT(varchar,ft.fno),60),
								/*SubTabela DocumentStatus*/
								(select
									InvoiceStatus =															case when ft.anulado = 0 then 'N' else 'A' end,
									InvoiceStatusDate =														convert(varchar,CONVERT(date,ft.ousrdata))+ 'T' + ft.ousrhora,
									SourceID =																LEFT(ft.vendnm,30),
									SourceBilling =															'P'
								for XML path ('DocumentStatus'), type),																						
								Hash =																		isnull(ltrim(rtrim(left(b.hash,172))),'0'),
								HashControl =																case when b.hash is null or b.hash='' then '0' else '1' end,
								Period =																	DATEPART(month,ft.fdata),	
								InvoiceDate =																convert(date,ft.fdata), 
								InvoiceType =																left(isnull(b.invoiceType,'FT'),2),
								(select
									SelfBillingIndicator =														0,
									CashVATSchemeIndicator =													0,
									ThirdPartiesBillingIndicator =												0
								for XML path ('SpecialRegimes'), type),
								SourceID =																	LEFT(ft.vendnm,30),
								EACCode =																	(select LEFT(RTRIM(LTRIM(cae)),5) from empresa where site = @Loja),
								SystemEntryDate =															convert(varchar,convert(date,ft.ousrdata))+'T'+CONVERT(varchar,case when ft.ousrhora='' then '00:00:00' else ft.ousrhora end),
								CustomerID =																case 
																												when b_utentes.no=200 and (ft.ncont!='999999990' or ft.nome!=@nomeClienteDuzentos or ft.morada!='' or ft.telefone!='') 
																												then left(convert(varchar,ft.no) + '-A' + convert(varchar,ft.ndoc) + '/' + convert(varchar,ft.fno) + '/' + convert(varchar,ft.estab),30)
																												else left(convert(varchar,ft.no) + '/' + convert(varchar,ft.estab),30)
																											end,
								--CustomerID =																case 
								--																				/* Vendas feitas ao cliente 200 com altera��o de dados (Clientes Virtuais)*/ 
								--																				when b_utentes.no=200 and ft.ncont='999999990' and (ft.nome!=@nomeClienteDuzentos or ft.morada!='' or ft.telefone!='') then left(convert(varchar,ft.no) + '-A'  + convert(varchar,ft.ndoc) + convert(varchar,ft.fno) + '/' + convert(varchar,ft.estab),30)
								--																				when b_utentes.no=200 and ft.ncont!='999999990' and (ft.nome!=@nomeClienteDuzentos or ft.morada!='' or ft.telefone!='') then left(convert(varchar,ft.no) + '-A'  + convert(varchar,ft.ndoc) + convert(varchar,ft.fno) + '/' + convert(varchar,ft.estab) + '/' + CONVERT(varchar,ft.ncont) + '/' + CONVERT(varchar,ft.fno),30)
								--																				/* Vendas feitas ao cliente "Venda a Dinheiro" sem altera��o de dados*/
								--																				when ft.no=200 and ft.ncont='999999990'	then left(convert(varchar,ft.no)+'/'+convert(varchar,ft.estab),30)
								--																				when ft.no=200 and ft.ncont<>'999999990' then left(convert(varchar,ft.no)+'/'+convert(varchar,ft.estab) + '/' + CONVERT(varchar,ft.ncont) + '/' + CONVERT(varchar,ft.fno),30)
								--																				/* Vendas feitas aos restantes clientes*/
								--																				when ft.no<>200/* and ft.ncont='999999990'*/ then left(convert(varchar,ft.no)+'/'+convert(varchar,ft.estab),30)
								--																			end,
								/* Subtabela Line */
								(select
									LineNumber =															ROW_NUMBER() over (order by fi.lordem asc),
									/* Subtabela OrderReferences: origens de cada linha nas tabelas de Factura��o de todos os documentos excepto Notas de Cr�dito! */  
									(select
										case
											when td.tiposaft!='NC' then 																															
												(select   
													OriginatingON =											left(n.nmdoc + ' ' + convert(varchar,n.ndoc)+'/'+CONVERT(varchar,n.fno),255),
													OrderDate =												convert(date,ft.fdata)			
												from fi (nolock) as n
												where
													n.fistamp=fi.ofistamp
												for xml path ('OrderReferences'), type)
											else
												null 
										end
									),
									 
									ProductCode =															case 
																												when fi.ref='' then case 
																																		when fi.oref='' then '9999999' 
																																		else ltrim(rtrim(fi.oref)) 
																																		end	
																												else ltrim(rtrim(left(fi.ref,30))) 
																												end, --case when fi.ref='' then '9999999' else ltrim(rtrim(left(fi.ref,30))) end,             
									ProductDescription = 													case 
																												when fi.design is null or fi.design='' then 'Desconhecido'
																												else LEFT(
																														replace(replace(replace(replace(replace(replace(replace(replace(fi.design
																															,CHAR(39),'')
																															,CHAR(34),'')
																															,CHAR(38),'')
																															,CHAR(14),'')
																															,CHAR(2),'')
																															,CHAR(8),'')
																															,'<','')
																															,'>','')
																													,200)
																												end,								
									Quantity =																convert(int,fi.qtt),
									UnitOfMeasure =															case
																												when fi.unidade='' then 'UN'								
																												else left(upper(convert(varchar,fi.unidade)),20)
																											end,
									UnitPrice =																case
																												when (fi.etiliquido / (fi.iva/100+1)) > 0 then 									
																													case 
																														when fi.ivaincl=1 then abs((fi.etiliquido / (fi.iva/100+1)) / fi.qtt) 
																														else abs(fi.etiliquido/fi.qtt)
																													end
																												else 0
																											end,
									TaxPointDate =															convert(date,ft.fdata),
									/* Campo References, apenas deve aparecer se o documento for do tipo SAFT "NC" e tem que aparecer nesta posi��o (indica��o DGCI); o texto do campo Reason � gen�rico*/
									(select
										case 
											when td.tiposaft='NC' then 
												(select 
													Reference =											left(n.nmdoc + ' ' + convert(varchar,n.ndoc)+'/'+CONVERT(varchar,n.fno),60),
													Reason =											left('Regulariza��o / Devolu��o de Produto',50)
												from fi (nolock) as n
												where																															
													n.fistamp=fi.ofistamp	
												for xml path ('References'), type)
											else
												null
										end 
									),								
									
									Description =															case when fi.design = '' then ' ' else left(fi.design,200) end,
									DebitAmount =															case 
																												when (fi.etiliquido / (fi.iva/100+1))<0 then
																																							case 
																																								when fi.ivaincl=1 then abs((fi.etiliquido / (fi.iva/100+1))) 
																																								else abs(fi.etiliquido)
																																							end
																												else null 
																											end,
									CreditAmount =															case 
																												when (fi.etiliquido / (fi.iva/100+1))>0 then
																																							case 
																																								when fi.ivaincl=1 then abs((fi.etiliquido / (fi.iva/100+1))) 
																																								else abs(fi.etiliquido)
																																							end 
																												when (fi.etiliquido / (fi.iva/100+1))=0 then 0 
																												else null 
																											end,
									/* 
										Subtabela Tax: o subquery com TOP 1 deve-se ao facto de que h� v�rios REGIVAs para o c�digo 0 
									*/
									(select
										TaxType =															left('IVA',3),
										TaxCountryRegion =													left('PT',5), /*deveria ser obtido da tabela regIVA*/
										TaxCode =															(select top 1 left(codigo,10) from regiva where tabiva=fi.tabiva),
										TaxPercentage =														convert(int,fi.iva) 								
									for xml path ('Tax'), type
									),
		
									TaxExemptionReason =
										case 
											when fi.iva=0 then 
												case 
													when (Select motiseimp FROM ft2 (nolock) WHERE ft.ftstamp = ft2.ft2stamp) != '' then 
														left(motiseimp,60)
													when (Select pais FROM b_utentes (nolock) WHERE b_utentes.no = ft.no and b_utentes.estab = ft.estab) = 3 then 
														left('Isento Artigo 14.� do CIVA (ou similar)',60)
													when (Select pais FROM b_utentes (nolock) WHERE b_utentes.no = ft.no and b_utentes.estab = ft.estab) = 2 then 
														left('Isento Artigo 14.� do RITI (ou similar).',60)
													else
														left('Isento Artigo 9.� do CIVA (ou similar).',60)
													end	
										else null end,
									
									SettlementAmount =														case 
																												when (convert(numeric,desconto))>0 or (convert(numeric,desc2))>0 or (convert(numeric,u_descval))>0 then 
																													abs(
																														abs(
																															abs(
																																case 
																																	when fi.ivaincl=1 then (fi.epv*fi.qtt) / (fi.iva/100+1)
																																	else fi.epv*fi.qtt
																																end
																															)-
																															abs(
																																case 
																																	when fi.ivaincl=1 then fi.etiliquido / (fi.iva/100+1)
																																	else fi.etiliquido/fi.qtt 
																																end
																															) 
																														)
																													)
																												else null 
																											end 								
									
									
								/* Final da subtabela Line*/
								from fi (nolock) as fi
								where
									fi.ftstamp=ft.ftstamp 
									and fi.epromo=0
									and ( (fi.qtt <> 0 or fi.etiliquido <> 0) OR  ( ft.anulado = 1 ) )
								order by 
									fi.lordem
								for xml path ('Line'), type
								),
								
								/* Subtabela DocumentTotals */
								(select
									TaxPayable =															ltrim(str(abs(ft.ettiva),10,2)),
									NetTotal =																ltrim(str(abs(ft.eivain1+ft.eivain2+ft.eivain3+ft.eivain4+ft.eivain5+ft.eivain6+ft.eivain7+ft.eivain8+ft.eivain9),10,2)),
									GrossTotal =															ltrim(str(abs(ft.etotal+ft.efinv),10,2)),
									
									/* Subtabela Currrency, apenas exportada quando a moeda de um documento n�o � EURO (indica��o DGCI)*/
									(select 
										case 
											when ft.moeda not in ('PTE ou EURO','EURO','EUR', '') then 
												(select
													CurrencyCode =											case when ft.moeda='' then left('EUR',3) else left(ft.moeda,3) end,
													CurrencyAmount =										str(abs(ft.etotal),10,2)
												for xml path ('Currency'), type)  
											else null 
										end
									),	
									
									/* Subtabela Settlement, apenas exportada quando existe Desconto Financeiro no cabe�alho de documento (indica��o DGCI) */
									(select 
										case 
											when ft.efinv<>0 then 				
												(select
													SettlementDiscount =										left('0',30),
													SettlementAmount =											ft.efinv,
													SettlementDate =											CONVERT(date,ft.fdata) 
												for xml path ('Settlement'), type
												)
											else null 
										end 
									)
								
								for xml path ('DocumentTotals'),type
								)
								
							from ft  as ft (nolock)
							inner join ft2 (nolock) on ft.ftstamp=ft2.ft2stamp
							inner join td  (nolock) on ft.ndoc=td.ndoc 
							inner join b_utentes  (nolock) ON b_utentes.no=(case when td.lancacli=0 then ft.no else ft2.c2no end)  and b_utentes.estab=(case when td.lancacli=0 then ft.estab else ft2.c2estab end) 
							left join B_cert (nolock) as b on ft.ftstamp=b.stamp
							where 
								ft.fdata >= @dataini 
								and ft.fdata <= @datafim
								--and td.excluisaft = 0
								and len(td.tiposaft) >= 2
								and td.tiposaft not in ('GT','GR','GA','GC','GD') /*n�o incluir guias de transporte*/
								/* Novo crit�rio que separa documentos por LOJA, para clientes multi-base de dados */
								and ft.site = case when @Loja = 'TODAS' then ft.site else @Loja end
							order by
								ft.ndoc,ft.fno													
							for xml path ('Invoice'), type)
							
						for xml path ('SalesInvoices'), type)
						
						/********************************/
						
					for xml path ('SourceDocuments'), type)
					
				for xml path (''), root ('AuditFile'), type
				
			)),'<AuditFile>','<?xml version = "1.0" encoding="Windows-1252" standalone="yes"?><AuditFile xmlns="urn:OECD:StandardAuditFile-Tax:PT_1.03_01" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">')) as saft
			
		end try
		
		begin catch
			declare
				@errortext				varchar(256),
				@error_severity			int,
				@error_state			int
			
			select 
				@errortext = isnull(ERROR_PROCEDURE(),'') + ': ' + ERROR_MESSAGE(),
				@error_severity = ERROR_SEVERITY(),
				@error_state = ERROR_state()

			raiserror(@errortext,@error_severity,@error_state)
			return(1)
		end catch
	end

go

grant execute on dbo.up_gen_saft2013_3 to public
grant control on dbo.up_gen_saft2013_3 to public

