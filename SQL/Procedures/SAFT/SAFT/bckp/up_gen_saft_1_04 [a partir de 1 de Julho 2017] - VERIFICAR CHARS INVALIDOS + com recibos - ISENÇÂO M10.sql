--exec up_gen_saft_1_04 '20190101', '20190131', 'Loja 1'

/****** Object:  StoredProcedure [dbo].[up_gen_saft_1_04]    Script Date: 21-01-2019 11:23:12 ******/
DROP PROCEDURE [dbo].[up_gen_saft_1_04]
GO

/****** Object:  StoredProcedure [dbo].[up_gen_saft_1_04]    Script Date: 21-01-2019 11:23:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO

create procedure [dbo].[up_gen_saft_1_04]
	@dataini datetime,
	@datafim datetime,
	@Loja varchar(20)
/* With Encryption */
as
begin
	If OBJECT_ID('tempdb.dbo.#t_salesInvoices') IS NOT NULL
		drop table #t_salesInvoices;
	If OBJECT_ID('tempdb.dbo.#t_movOfGoods') IS NOT NULL
		drop table #t_movOfGoods;
	If OBJECT_ID('tempdb.dbo.#t_workingDocs') IS NOT NULL
		drop table #t_workingDocs;
	If OBJECT_ID('tempdb.dbo.#t_workingDocs') IS NOT NULL
		drop table #t_Payments;
	If OBJECT_ID('tempdb.dbo.#t_saft_cl') IS NOT NULL
		drop table #t_saft_cl;
	If OBJECT_ID('tempdb.dbo.#t_saft_st') IS NOT NULL
		drop table #t_saft_st;
	If OBJECT_ID('tempdb.dbo.#modopag') IS NOT NULL
		drop table #modopag;
	begin try
		set nocount on
		set xact_abort on
		set language portuguese
		/* Criar variavel com nome do cliente 200 */
		declare @nomeClienteDuzentos varchar(30)
		set @nomeClienteDuzentos = 'CONSUMIDOR FINAL'
		/*
		select tiposaft, * from ts
			Criar cursores auxiliares
		*/
		/*
			Modos de pagemento
		*/
		select 
			* 
		into 
			#modopag
		from 
			b_modopag
		/*
			SalesInvoices (section 4.1)
		*/
		select
			-- cabeçalho
			docstamp = ft.ftstamp, ndoc=convert(varchar,ft.ndoc),
			ft.no, ft.estab, ft.nome, ft.ncont, ft.morada, ft.telefone, ft.pais, ft.local,
			ft.codpost, fno = convert(varchar,ft.fno), docdata = ft.fdata,
			ft.anulado, ft.vendnm,
			ft.ousrdata, ft.ousrhora,
			b_cert.invoiceType, b_cert.hash,
			entity = 'CL',
			datatransporte = ft.datatransporte, horaentrega=ft2.horaentrega,
			ft2.motiseimp, ft.moeda,
			--ft.ettiva
			0 as ettiva, ft.etotal as ettiliq, ft.etotal, ft.efinv,
			0 as eivain1, 0 as eivain2, 0 as eivain3, 0 as eivain4,
			0 as eivain5, 0 as eivain6, 0 as eivain7, 0 as eivain8, 0 as eivain9,
			--ft.eivain1, ft.eivain2, ft.eivain3, ft.eivain4,
			--ft.eivain5, ft.eivain6, ft.eivain7, ft.eivain8, ft.eivain9,
			td.tiposaft,
			empresa.cae,
			-- linhas
			linstamp = fi.fistamp, fi.lordem, fi.ref, fi.oref, fi.design, fi.qtt, fi.familia,
			desc1 = fi.desconto, fi.desc2, fi.desc3, fi.desc4, descval = fi.u_descval,
			--fi.iva, fi.tabiva
			0 as iva, 4 as tabiva
			, fi.ivaincl
			, fi.armazem, epv = fi.epv, etiliquido = fi.etiliquido,
			fi.epromo, fi.desconto,
			entradaSock = case when fi.qtt > 0 then 0 else 1 end,
			fi.ofistamp, fi.unidade
		into
			#t_salesInvoices
		from
			ft (nolock)
			inner join ft2 (nolock) on ft.ftstamp = ft2.ft2stamp
			inner join fi (nolock) on ft.ftstamp = fi.ftstamp
			inner join td (nolock) on td.ndoc = ft.ndoc
			left join B_cert (nolock) on ft.ftstamp = b_cert.stamp
			inner join empresa (nolock) on empresa.site = ft.site
		where
			len(td.tiposaft) >= 2
			and td.tiposaft not in ('GR','GT','GA','GC','GD') -- MovementsOfGoods
			and td.tiposaft not in ('PF') -- working documents
			and ft.fdata between @dataini and @datafim
			and ft.site = case when @Loja = 'TODAS' then ft.site else @Loja end
			and (fi.ref != '' or fi.oref != '')
		/*
			MovementsOfGoods (secção 4.2)
		*/
		select
			-- cabeçalho
			docstamp = bo.bostamp, ndoc = 'B'+convert(varchar,bo.ndos),
			bo.no, bo.estab, bo.nome, bo.ncont, bo.morada, bo2.telefone, pais=1, bo.local,
			bo.codpost, fno = bo.obrano, docdata = bo.dataobra,
			anulado = 0, bo.vendnm,
			bo.ousrdata, bo.ousrhora,
			b_cert.invoiceType, b_cert.hash,
			entity = ts.bdempresas,
			datatransporte = bo2.xpddata, horaentrega=bo2.xpdhora,
			motiseimp = '', bo.moeda,
			ettiva = bo2.etotiva, ettiliq = bo.etotaldeb, bo.etotal, efinv = 0,
			eivain1 = bo.ebo11_bins, eivain2 = bo.ebo21_bins, eivain3 = bo.ebo31_bins, eivain4 = bo.ebo41_bins,
			eivain5 = bo.ebo51_bins, eivain6 = bo.ebo61_bins, eivain7 = bo2.ebo71_bins, eivain8 = bo2.ebo81_bins, eivain9 = bo2.ebo91_bins,
			ts.tiposaft,
			empresa.cae,
			-- Linhas
			linstamp = bi.bistamp, bi.lordem, bi.ref, oref='', bi.design, bi.qtt, bi.familia,
			desc1 = bi.desconto, bi.desc2, bi.desc3, bi.desc4, bi.descval,
			bi.iva, bi.tabiva, bi.ivaincl, bi.armazem, epv = bi.edebito, etiliquido = bi.ettdeb,
			epromo = 0, bi.desconto,
			entradaSock = case 
							when ts.cmstocks > 50 and bi.qtt > 0 then 0
							when ts.cmstocks < 50 and bi.qtt < 0 then 0 -- Ex: acerto de Stock
							else 1
							end,
			ofistamp='', bi.unidade
		into
			#t_movOfGoods
		from
			bo (nolock)
			inner join bo2 (nolock) on bo.bostamp=bo2.bo2stamp
			inner join bi (nolock) on bo.bostamp = bi.bostamp
			inner join ts (nolock) on bo.ndos = ts.ndos
			left join B_cert (nolock) on bo.bostamp = b_cert.stamp
			inner join empresa (nolock) on empresa.site = bo.site
		where
			ts.tiposaft in ('GR','GT','GA','GC','GD')
			and bo.dataobra between @dataini and @datafim
			and bo.site = case when @Loja = 'TODAS' then bo.site else @Loja end
			and bi.ref != ''
		union all
		select
			-- cabeçalho
			docstamp = ft.ftstamp, ndoc=convert(varchar,ft.ndoc),
			ft.no, ft.estab, ft.nome, ft.ncont, ft.morada, ft.telefone, ft.pais, ft.local,
			ft.codpost, fno = convert(varchar,ft.fno), docdata = ft.fdata,
			ft.anulado, ft.vendnm,
			ft.ousrdata, ft.ousrhora,
			b_cert.invoiceType, b_cert.hash,
			entity = 'CL',
			datatransporte = ft.datatransporte, horaentrega=ft2.horaentrega,
			ft2.motiseimp, ft.moeda,
			ft.ettiva, ft.ettiliq, ft.etotal, ft.efinv,
			ft.eivain1, ft.eivain2, ft.eivain3, ft.eivain4,
			ft.eivain5, ft.eivain6, ft.eivain7, ft.eivain8, ft.eivain9,
			td.tiposaft,
			empresa.cae,
			-- linhas
			linstamp = fi.fistamp, fi.lordem, fi.ref, fi.oref, fi.design, fi.qtt, fi.familia,
			desc1 = fi.desconto, fi.desc2, fi.desc3, fi.desc4, descval = fi.u_descval,
			fi.iva, fi.tabiva, fi.ivaincl, fi.armazem, epv = fi.epv, etiliquido = fi.etiliquido,
			fi.epromo, fi.desconto,
			entradaSock = case when fi.qtt > 0 then 0 else 1 end,
			fi.ofistamp, fi.unidade
		from
			ft (nolock)
			inner join ft2 (nolock) on ft.ftstamp = ft2.ft2stamp
			inner join fi (nolock) on ft.ftstamp = fi.ftstamp
			inner join td (nolock) on td.ndoc = ft.ndoc
			inner join b_utentes cl (nolock) ON cl.no = (case when td.lancacli=0 then ft.no else ft2.c2no end) and cl.estab=(case when td.lancacli=0 then ft.estab else ft2.c2estab end)
			left join B_cert (nolock) on ft.ftstamp = b_cert.stamp
			inner join empresa (nolock) on empresa.site = ft.site
		where
			td.tiposaft in ('GR','GT','GA','GC','GD')
			and td.tiposaft not in ('PF') -- working documents
			and ft.fdata between @dataini and @datafim
			and ft.site = case when @Loja = 'TODAS' then ft.site else @Loja end
			and (fi.ref != '' or fi.oref != '')
		/*
			WorkingDocuments (secção 4.3)
		*/
		select
			-- cabeçalho
			docstamp = bo.bostamp, ndoc = 'B' + convert(varchar,bo.ndos),
			bo.no, bo.estab, bo.nome, bo.ncont, bo.morada, bo2.telefone, pais=1, bo.local,
			bo.codpost, fno = bo.obrano, docdata = bo.dataobra,
			anulado = 0, bo.vendnm,
			bo.ousrdata, bo.ousrhora,
			b_cert.invoiceType, b_cert.hash,
			entity = ts.bdempresas,
			datatransporte = bo2.xpddata, horaentrega=bo2.xpdhora,
			motiseimp = '', bo.moeda,
			ettiva = bo2.etotiva, ettiliq = bo.etotaldeb, bo.etotal, efinv = 0,
			eivain1 = bo.ebo11_bins, eivain2 = bo.ebo21_bins, eivain3 = bo.ebo31_bins, eivain4 = bo.ebo41_bins,
			eivain5 = bo.ebo51_bins, eivain6 = bo.ebo61_bins, eivain7 = bo2.ebo71_bins, eivain8 = bo2.ebo81_bins, eivain9 = bo2.ebo91_bins,
			ts.tiposaft,
			empresa.cae,
			-- Linhas
			linstamp = bi.bistamp, bi.lordem, bi.ref, oref='', bi.design, bi.qtt, bi.familia,
			desc1 = bi.desconto, bi.desc2, bi.desc3, bi.desc4, bi.descval,
			bi.iva, bi.tabiva, bi.ivaincl, bi.armazem, epv = bi.edebito, etiliquido = bi.ettdeb,
			epromo = 0, bi.desconto,
			entradaSock = case 
							when ts.cmstocks > 50 and bi.qtt > 0 then 0
							when ts.cmstocks < 50 and bi.qtt < 0 then 0 -- Ex: acerto de Stock
							else 1
							end,
			ofistamp='', bi.unidade
		into
			#t_workingDocs
		from
			bo (nolock)
			inner join bo2 (nolock) on bo.bostamp=bo2.bo2stamp
			inner join bi (nolock) on bo.bostamp = bi.bostamp
			inner join ts (nolock) on bo.ndos = ts.ndos
			left join B_cert (nolock) on bo.bostamp = b_cert.stamp
			inner join empresa (nolock) on empresa.site = bo.site
		where
			ts.tiposaft in ('NE')
			and bo.dataobra between @dataini and @datafim
			and bo.site = case when @Loja = 'TODAS' then bo.site else @Loja end
			and bi.ref != ''
		/*
			Payments (secção 4.4)
		*/
		select
			re.restamp
			,re.rdata
			, 'R'+convert(varchar, tsre.ndoc) as tsrendoc
			, tsre.nmdoc as tsrenmdoc
			, tsre.codsaft as tsrecodsaft
			, re.nmdoc
			, re.rno
			, re.etotal
			, re.efinv
			, re.ousrinis
			, re.ousrdata
			, re.ousrhora
			, re.no
			, re.estab
			, re.nome
			, re.ncont
			, b_utentes.morada
			, b_utentes.telefone
			, b_utentes.local
			, b_utentes.codpost
			, rl.ndoc
			, InvoiceNo = left(isnull((select invoiceType from b_cert bce where ft.ftstamp=bce.stamp),'FT') + ' ' + convert(varchar,ft.ndoc) + '/' + CONVERT(varchar,ft.fno),60)
			, rl.cdesc
			, rl.nrdoc
			, rl.cm
			, rl.erec
			--, edeb = case when rl.cm < 50 then ft.ettiliq else 0 end
			--, ecred = case when rl.cm > 50 then ft.ettiliq*(-1)  else 0 end
			, edeb = case when rl.cm < 50 then ft.etotal else 0 end
			, ecred = case when rl.cm > 50 then ft.etotal*(-1)  else 0 end
			, edebciva = case when rl.cm < 50 then ft.etotal else 0 end
			, ecredciva = case when rl.cm > 50 then ft.etotal*(-1)  else 0 end
			, rl.lordem
			, rl.datalc
			--, rl.eivav1
			--, rl.eivav2
			--, rl.eivav3
			--, rl.eivav4
			--, rl.eivav5
			--, rl.eivav6
			--, rl.eivav7
			--, rl.eivav8
			--, rl.eivav9
			, 0 as eivav1
			, 0 as eivav2
			, 0 as eivav3
			, 0 as eivav4
			, 0 as eivav5
			, 0 as eivav6
			, 0 as eivav7
			, 0 as eivav8
			, 0 as eivav9
			, 'IVA - Regime de isenção' as motiseimp
			--, ft2.motiseimp
			, 'M10' as code_motive
			--, isnull(b_multidata.code_motive,'') as code_motive
			, numerario = (case when (select campofact from #modopag where design='Dinheiro')= 'ft2.evdinheiro'
									then B_pagCentral.evdinheiro
									else (case when (select campofact from #modopag where design='Dinheiro')= 'ft2.epaga2'
												then B_pagCentral.epaga2
												else (case when (select campofact from #modopag where design='Dinheiro')= 'ft2.epaga1'
														then B_pagCentral.epaga1
														else (case when (select campofact from #modopag where design='Dinheiro')= 'ft2.echtotal'
																then B_pagCentral.echtotal
																else (case when (select campofact from #modopag where design='Dinheiro')= 'ft2.epaga3'
																		then B_pagCentral.epaga3
																		else (case when (select campofact from #modopag where design='Dinheiro')= 'ft2.epaga4'
																				then B_pagCentral.epaga4
																				else (case when (select campofact from #modopag where design='Dinheiro')= 'ft2.epaga5'
																						then B_pagCentral.epaga5
																						else (case when (select campofact from #modopag where design='Dinheiro')= 'ft2.epaga6'
																									then B_pagCentral.epaga6
																									else 0
																									end)
																						end )
																				end)
																		end)
																end)
														end)
												end)
									end)
			, cartaodebito = (case when (select campofact from #modopag where design='Multibanco')= 'ft2.evdinheiro'
									then B_pagCentral.evdinheiro
									else (case when (select campofact from #modopag where design='Multibanco')= 'ft2.epaga2'
												then B_pagCentral.epaga2
												else (case when (select campofact from #modopag where design='Multibanco')= 'ft2.epaga1'
														then B_pagCentral.epaga1
														else (case when (select campofact from #modopag where design='Multibanco')= 'ft2.echtotal'
																then B_pagCentral.echtotal
																else (case when (select campofact from #modopag where design='Multibanco')= 'ft2.epaga3'
																		then B_pagCentral.epaga3
																		else (case when (select campofact from #modopag where design='Multibanco')= 'ft2.epaga4'
																				then B_pagCentral.epaga4
																				else (case when (select campofact from #modopag where design='Multibanco')= 'ft2.epaga5'
																						then B_pagCentral.epaga5
																						else (case when (select campofact from #modopag where design='Multibanco')= 'ft2.epaga6'
																									then B_pagCentral.epaga6
																									else 0
																									end)
																						end )
																				end)
																		end)
																end)
														end)
												end)
									end)
			, cartaocredito = (case when (select campofact from #modopag where design='Visa')= 'ft2.evdinheiro'
									then B_pagCentral.evdinheiro
									else (case when (select campofact from #modopag where design='Visa')= 'ft2.epaga2'
												then B_pagCentral.epaga2
												else (case when (select campofact from #modopag where design='Visa')= 'ft2.epaga1'
														then B_pagCentral.epaga1
														else (case when (select campofact from #modopag where design='Visa')= 'ft2.echtotal'
																then B_pagCentral.echtotal
																else (case when (select campofact from #modopag where design='Visa')= 'ft2.epaga3'
																		then B_pagCentral.epaga3
																		else (case when (select campofact from #modopag where design='Visa')= 'ft2.epaga4'
																				then B_pagCentral.epaga4
																				else (case when (select campofact from #modopag where design='Visa')= 'ft2.epaga5'
																						then B_pagCentral.epaga5
																						else (case when (select campofact from #modopag where design='Visa')= 'ft2.epaga6'
																									then B_pagCentral.epaga6
																									else 0
																									end)
																						end )
																				end)
																		end)
																end)
														end)
												end)
									end)
			, cheque = (case when (select campofact from #modopag where design='Cheques')= 'ft2.evdinheiro'
									then B_pagCentral.evdinheiro
									else (case when (select campofact from #modopag where design='Cheques')= 'ft2.epaga2'
												then B_pagCentral.epaga2
												else (case when (select campofact from #modopag where design='Cheques')= 'ft2.epaga1'
														then B_pagCentral.epaga1
														else (case when (select campofact from #modopag where design='Cheques')= 'ft2.echtotal'
																then B_pagCentral.echtotal
																else (case when (select campofact from #modopag where design='Cheques')= 'ft2.epaga3'
																		then B_pagCentral.epaga3
																		else (case when (select campofact from #modopag where design='Cheques')= 'ft2.epaga4'
																				then B_pagCentral.epaga4
																				else (case when (select campofact from #modopag where design='Cheques')= 'ft2.epaga5'
																						then B_pagCentral.epaga5
																						else (case when (select campofact from #modopag where design='Cheques')= 'ft2.epaga6'
																									then B_pagCentral.epaga6
																									else 0
																									end)
																						end )
																				end)
																		end)
																end)
														end)
												end)
									end)
			, vales = (case when (select campofact from #modopag where design='Vales')= 'ft2.evdinheiro'
									then B_pagCentral.evdinheiro
									else (case when (select campofact from #modopag where design='Vales')= 'ft2.epaga2'
												then B_pagCentral.epaga2
												else (case when (select campofact from #modopag where design='Vales')= 'ft2.epaga1'
														then B_pagCentral.epaga1
														else (case when (select campofact from #modopag where design='Vales')= 'ft2.echtotal'
																then B_pagCentral.echtotal
																else (case when (select campofact from #modopag where design='Vales')= 'ft2.epaga3'
																		then B_pagCentral.epaga3
																		else (case when (select campofact from #modopag where design='Vales')= 'ft2.epaga4'
																				then B_pagCentral.epaga4
																				else (case when (select campofact from #modopag where design='Vales')= 'ft2.epaga5'
																						then B_pagCentral.epaga5
																						else (case when (select campofact from #modopag where design='Vales')= 'ft2.epaga6'
																									then B_pagCentral.epaga6
																									else 0
																									end)
																						end )
																				end)
																		end)
																end)
														end)
												end)
									end)
			, adiantamento = (case when (select top 1 campofact from #modopag where design='ModPag Ad.' order by ref)= 'ft2.evdinheiro'
									then B_pagCentral.evdinheiro
									else (case when (select top 1 campofact from #modopag where design='ModPag Ad.' order by ref)= 'ft2.epaga2'
												then B_pagCentral.epaga2
												else (case when (select top 1 campofact from #modopag where design='ModPag Ad.' order by ref)= 'ft2.epaga1'
														then B_pagCentral.epaga1
														else (case when (select top 1 campofact from #modopag where design='ModPag Ad.' order by ref)= 'ft2.echtotal'
																then B_pagCentral.echtotal
																else (case when (select top 1 campofact from #modopag where design='ModPag Ad.' order by ref)= 'ft2.epaga3'
																		then B_pagCentral.epaga3
																		else (case when (select top 1 campofact from #modopag where design='ModPag Ad.' order by ref)= 'ft2.epaga4'
																				then B_pagCentral.epaga4
																				else (case when (select top 1 campofact from #modopag where design='ModPag Ad.' order by ref)= 'ft2.epaga5'
																						then B_pagCentral.epaga5
																						else (case when (select top 1 campofact from #modopag where design='ModPag Ad.' order by ref)= 'ft2.epaga6'
																									then B_pagCentral.epaga6
																									else 0
																									end)
																						end )
																				end)
																		end)
																end)
														end)
												end)
									end)
			, transfbanc = (case when (select campofact from #modopag where left(design,6)='Transf')= 'ft2.evdinheiro'
									then B_pagCentral.evdinheiro
									else (case when (select campofact from #modopag where left(design,6)='Transf')= 'ft2.epaga2'
												then B_pagCentral.epaga2
												else (case when (select campofact from #modopag where left(design,6)='Transf')= 'ft2.epaga1'
														then B_pagCentral.epaga1
														else (case when (select campofact from #modopag where left(design,6)='Transf')= 'ft2.echtotal'
																then B_pagCentral.echtotal
																else (case when (select campofact from #modopag where left(design,6)='Transf')= 'ft2.epaga3'
																		then B_pagCentral.epaga3
																		else (case when (select campofact from #modopag where left(design,6)='Transf')= 'ft2.epaga4'
																				then B_pagCentral.epaga4
																				else (case when (select campofact from #modopag where left(design,6)='Transf')= 'ft2.epaga5'
																						then B_pagCentral.epaga5
																						else (case when (select campofact from #modopag where left(design,6)='Transf')= 'ft2.epaga6'
																									then B_pagCentral.epaga6
																									else 0
																									end)
																						end )
																				end)
																		end)
																end)
														end)
												end)
									end)
		into 
			#t_Payments
		from
			re (nolock)
			inner join rl (nolock) on re.restamp = rl.restamp
			left join B_cert (nolock) on re.restamp = b_cert.stamp
			inner join cc (nolock) on rl.ccstamp=cc.ccstamp
			inner join ft2 (nolock) on ft2.ft2stamp=cc.ccstamp
			inner join ft (nolock) on ft.ftstamp=cc.ccstamp
			inner join tsre (nolock) on re.nmdoc=tsre.nmdoc
			left join b_multidata (nolock) on ft2.motiseimp=b_multidata.campo and b_multidata.tipo = 'motiseimp' 
			inner join B_pagCentral (nolock) on re.u_nratend=b_pagcentral.nrAtend
			inner join b_utentes (nolock) on b_utentes.no=re.no and b_utentes.estab=re.estab
		where 
			re.rdata between @dataini and @datafim
			and re.site = case when @Loja = 'TODAS' then re.site else @Loja end
			and tsre.codsaft<>''
		order by rdata, nmdoc, rno, lordem
		/*
			Master files Produtos
		*/
		select distinct
			st.ref, st.codigo, st.design,
			st.faminome, st.stns
		into
			#t_saft_st
		from 
			st (nolock)
			inner join (select distinct x.ref from (
							select distinct ref = case when ref='' then oref else ref end from #t_salesInvoices
							union 
							select distinct ref = case when ref='' then oref else ref end from #t_movOfGoods where entity='CL'
							union 
							select distinct ref = case when ref='' then oref else ref end from #t_workingDocs where entity='CL'
							union
							select ref='9999999') x
						) a on a.ref = st.ref
		where
			st.ref != ''
			and st.site_nr = CASE when @Loja = 'TODAS' then st.site_nr ELSE((select top 1 no from empresa (nolock) where empresa.site = @Loja)) END
		/*
			Master files Clientes 
		*/
		select distinct
			cl.no, cl.estab, cl.nome, cl.morada, cl.codpost, cl.conta,
			cl.telefone, cl.codigop, cl.fax, cl.local, cl.ncont,
			doc_nome = docs.nome, doc_ncont = docs.ncont,
			doc_morada = docs.morada, doc_telefone = docs.telefone,
			doc_codpost = docs.codpost, doc_ndoc = docs.ndoc, doc_fno = docs.fno,
			doc_local = docs.local
		into
			#t_saft_cl
		from
			b_utentes cl (nolock)
			inner join (select distinct no, estab, nome, ncont, morada, telefone, local, codpost, ndoc, fno from (
							select distinct no, estab, nome, ncont, morada, telefone, local, codpost, ndoc, fno from #t_salesInvoices
							union 
							select distinct no, estab, nome, ncont, morada, telefone, local, codpost, ndoc, fno from #t_movOfGoods where entity='CL'
							union 
							select distinct no, estab, nome, ncont, morada, telefone, local, codpost, ndoc, fno from #t_workingDocs where entity='CL'
							union
							select distinct no, estab, nome, ncont, morada, telefone, local, codpost, tsrendoc, rno from #t_Payments
							) x
						) docs on docs.no = cl.no and docs.estab = cl.estab
		/* 
			Parte 1 do SELECT principal, 
			adiciona o string do encoding XML e converte o restante texto de XML para varchar(MAX) 
		*/
		select convert(text,replace(convert(varchar(max),(
			select 
				/*
					Tabela Header: dados da Ficha da Empresa e versão SAFT/PHC 
				*/
				(select top 1 
					/**/
					AuditFileVersion = left('1.04_01',10),
					/* Concatenação da conservatória do registo comercial com o nr do registo comercial, 
					separados por espaço. Nos casos em que não existe o registo comercial, deve ser indicado o NIF */
					CompanyID = case 
									when consreg<>'' then left(ltrim(rtrim(CONVERT(varchar,consreg))),39) + ' ' + left(replace(ncont,' ',''),10)
									else left(replace(ncont,' ',''),50)
									end,
					/* NIF PT sem espaços e sem prefixo do país */
					TaxRegistrationNumber =	left(replace(ncont,' ',''),9),
					/* Tipo de Programa (Faturação -> 'F') (outros -> ver doc oficial) */
					TaxAccountingBasis = left('F',1), 
					/* Denominação Social / Nome */
					CompanyName = left(nomecomp,100), 
					/* Designação Social */
					BusinessName = case when nomabrv='' then left(nomecomp,60) else left(nomabrv,60) end,
					/* Subtabela CompanyAddress */
					(select
						/* BuildingNumber (c 10) (opcional) */
						/* StreetName (c 200) (opcional) */
						/**/
						AddressDetail =	case 
											when isnull(morada,'')='' then 'Desconhecido'
											else LEFT(
													replace(replace(replace(replace(replace(replace(replace(replace(morada
														,CHAR(39),''),CHAR(34),''),CHAR(38),''),CHAR(14),''),CHAR(2),'')
														,CHAR(8),''),'<',''),'>','')
												,100)
											end,
						/**/
						City = case when local='' then 'Desconhecido' else left(local,50) end,						
						/**/
						PostalCode = case when codpost='' then '0000-000' else LEFT(codpost,8) end,
						/**/
						Region = case when distrito='' then null else left(distrito,50) end,											
						/**/
						Country = left('PT',2)
					for xml path ('CompanyAddress'), type),
					/* Ano Fiscal. Utilizar as regras do Código do IRC, no caso de períodos contabilísticos não coincidentes com o ano civil. 
					(Exemplo: período de tributação de 01-10-2012 a 30-09-2013 corresponde a 2012). */ 
					FiscalYear = convert(varchar,DATEPART(year,@dataini)),
					/* Data do início do período do ficheiro */
					StartDate = convert(varchar,@dataini,23),
					/* Data do fim do período do ficheiro */
					EndDate = convert(varchar,@datafim,23),
					/* EUR */
					CurrencyCode = left('EUR',3),
					/* Data criação do ficheiro */
					DateCreated = convert(date,GETDATE()),
					/* Identificação do establecimento. Caso fx de faturação, deve ser especificado o establecimento,
					se aplicável. Caso contrário, deve especificar “Global”. Caso fx de contabilidade ou integrado,
					deve expecificar “Sede”. */
					TaxEntity = (select top 1 left(TaxEntity,20) from empresa (nolock) where empresa.site = case when @Loja = 'TODAS' then empresa.site else @Loja end),
					/* NIF da entidade produtora de software */
					ProductCompanyTaxID = left('508935490',20), /* deveria ser parâmetro */
					/* Nr certificação atribuído ao software */
					SoftwareCertificateNumber = (select top 1 left(textValue,4) from B_Parameters (nolock) where stamp='ADM0000000078'),
					/* Nome da aplicação que gera o SAF-T (PT). Formato “Nome da aplicação/Nome da empresa produtora do software” */
					ProductID = left('Logitools/Logitools',255),
					/* Versão do software */
					ProductVersion = case 
										when (select top 1 ltrim(rtrim(textValue)) from b_parameters (nolock) where stamp='ADM0000000156') = '' or (select top 1 ltrim(rtrim(textValue)) from b_parameters (nolock) where stamp='ADM0000000156') is null then left('11.0.0',30)
										else left((select top 1 ltrim(rtrim(textValue)) from b_parameters (nolock) where stamp='ADM0000000156'),30)
										end,
					/* HeaderComment (c 255) (opcional) */
					/* Telefone da empresa (c 20) */
					Telephone = left(case when telefone='' then null else convert(varchar,telefone) end,20),
					/* Fax da empresa (c 20)  */
					Fax = left(case when fax='' then null else fax end,20),
					/* Email da empresa (c 254) */
					Email = case when email='' then null else left(email,60) end
					/* Website (c 60) (opcional) */
				from 
					empresa (nolock)
				where
					empresa.no = (select top 1 no from empresa (nolock) where empresa.site = case when @Loja = 'TODAS' then empresa.site else @Loja END)
				for xml path ('Header'), type),
				/*
					Tabela MasterFiles
				*/
				(select
					/*
						Tabela de código de contas (GeneralLedger)
						(não necessário no saft de faturação)
					*/
					/*
						Tabela de Clientes (Customer)
					*/
					(select * from 
						/* Cliente vendas a dinheiro */
						(select top 1
							/* Identificador único do cliente. Para o consumidor final, deve ser criado um cliente genérico com a designação “Consumidor final” */
							CustomerID = left(convert(varchar,cl.no) + '/' + CONVERT(varchar,cl.estab),30),
							/* Código da conta */
							AccountID = case when cl.conta='' then 'Desconhecido' else left(cl.conta,30) end,
							/* Nif do cliente. O cliente genérico, correspondente ao “Consumidor final”, deve conter o NIF “999999990” */
							CustomerTaxID = '999999990',
							/* Nome da empresa do cliente */
							CompanyName = 'Consumidor final',
							/* Subtabela BillingAddress
							(Corresponde à morada da sede ou do estabelecimento estável em território nacional) */
							BillingAddress = (
								select
									/* BuildingNumber (opcional) */
									/* StreetName (opcional) */
									/* */
									AddressDetail = 'Desconhecido',
									/**/
									City = 'Desconhecida',
									/**/
									PostalCode = 'Desconhecido',
									/* Region (opcional) */
									/* País (ISO 3166–1-alpha-2) 
										Deve ser preenchido com a designação “Desconhecido”, nas seguintes situações:
										. Sistemas   não   integrados,   se   a informação não for conhecida;
										. Operações realizadas com “Consumidor final” */
									Country = 'Desconhecido'
								for XML path (''), type),
							/* Subtabela ShipToAdrdress 
							(Esta estrutura poderá ser gerada tantas vezes quantas as necessárias) */ 
							ShipToAddress = (
								select
									/* BuildingNumber (opcional) */
									/* Streetname (opcional */
									/**/
									AddressDetail = 'Desconhecido',
									/**/
									City = 'Desconhecida',
									/**/
									PostalCode = 'Desconhecido',
									/* Region (opcional) */
									/**/
									Country = case when cl.codigop = '' then 'Desconhecido' else UPPER(LEFT(LTRIM(RTRIM(cl.codigop)),12)) end
								for XML path (''), type),
							/**/
							Telephone = case when cl.telefone='' then null else left(convert(varchar,cl.telefone),20) end,
							/**/
							Fax = case when cl.fax='' then null else left(convert(varchar,cl.fax),20) end,
							/* Email (opcional) */
							/* Website (opcional) */
							/* Indicador de autofaturação */
							SelfBillingIndicator = 0
						from
							#t_saft_cl cl
						where
							cl.no = 200
							/* Campos que, quando alterados, devem gerar os novos IDs virtuais */
							and cl.doc_nome = @nomeClienteDuzentos
							and cl.doc_ncont = '999999990'
							and cl.doc_morada = ''
							and cl.doc_telefone = ''
						/* Clientes Reais (!=200) */
						union all
						select
							/* Identificador único do cliente. Para o consumidor final, deve ser criado um cliente genérico com a designação “Consumidor final” */
							CustomerID = left(convert(varchar,cl.no) + '/' + CONVERT(varchar,cl.estab),30),
							/* Código da conta */
							AccountID = case when cl.conta='' then 'Desconhecido' else left(cl.conta,30) end,
							/* Nif do cliente. O cliente genérico, correspondente ao “Consumidor final”, deve conter o NIF “999999990” */
							CustomerTaxID = case when cl.ncont in ('', '.') then '999999990' else left(replace(REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( 
																												REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( 
																												REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( 
																												REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( 
																												REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( 
																												REPLACE( REPLACE( REPLACE( REPLACE( 
																													cl.ncont
																												,char(0x0000),'') ,char(0x0001),'') ,char(0x0002),'') ,char(0x0003),'') ,char(0x0004),'') 
																												,char(0x0005),'') ,char(0x0006),'') ,char(0x0007),'') ,char(0x0008),'') ,char(0x000B),'') 
																												,char(0x000C),'') ,char(0x000E),'') ,char(0x000F),'') ,char(0x0010),'') ,char(0x0011),'') 
																												,char(0x0012),'') ,char(0x0013),'') ,char(0x0014),'') ,char(0x0015),'') ,char(0x0016),'') 
																												,char(0x0017),'') ,char(0x0018),'') ,char(0x0019),'') ,char(0x001A),'') ,char(0x001B),'') 
																												,char(0x001C),'') ,char(0x001D),'') ,char(0x001E),'') ,char(0x001F),'')
																												,' ',''),20) end,
							/* Nome da empresa do cliente */
							CompanyName = case 
											when nome=@nomeClienteDuzentos then 'Consumidor final'
											when nome='' then 'Desconhecido'
											else LEFT(
													replace(replace(replace(replace(replace(replace(replace(replace(nome
														,CHAR(39),''),CHAR(34),''),CHAR(38),''),CHAR(14),''),CHAR(2),'')
														,CHAR(8),''),'<',''),'>','')
												,100)
											end,
							/* Subtabela BillingAddress
							(Corresponde à morada da sede ou do estabelecimento estável em território nacional) */
							BillingAddress = (
								select
									/* BuildingNumber (opcional) */
									/* StreetName (opcional) */
									/**/
									AddressDetail = case 
														when morada is null or morada='' then 'Desconhecido'
														else LEFT(
																replace(replace(replace(replace(replace(replace(replace(replace(morada
																	,CHAR(39),''),CHAR(34),''),CHAR(38),''),CHAR(14),''),CHAR(2),'')
																	,CHAR(8),''),'<',''),'>','')
															,100)
														end,
									/**/
									City = case when cl.local = '' then 'Desconhecida' else left(replace(cl.local,char(39),''),50) end,
									/**/
									PostalCode = case when cl.codpost = '' then 'Desconhecido' else left(convert(varchar,cl.codpost),20) end,
									/* Region (opcional) */
									/* País (ISO 3166–1-alpha-2) 
										Deve ser preenchido com a designação “Desconhecido”, nas seguintes situações:
										. Sistemas   não   integrados,   se   a informação não for conhecida;
										. Operações realizadas com “Consumidor final” */
									Country = case when cl.codigop = '' then 'Desconhecido' else UPPER(LEFT(RTRIM(LTRIM(cl.codigop)),12)) end
								for XML path (''), type),
							/* Subtabela ShipToAdrdress 
							(Esta estrutura poderá ser gerada tantas vezes quantas as necessárias) */ 
							ShipToAddress = (
								select
									/* BuildingNumber (opcional) */
									/* Streetname (opcional */
									/**/
									AddressDetail = case 
														when morada is null or morada='' then 'Desconhecido'
														else LEFT(
																replace(replace(replace(replace(replace(replace(replace(replace(morada
																	,CHAR(39),''),CHAR(34),''),CHAR(38),''),CHAR(14),''),CHAR(2),'')
																	,CHAR(8),''),'<',''),'>','')
															,100)
														end,
									/**/
									City = case when cl.local ='' then 'Desconhecida' else left(replace(cl.local,char(39),''),50) end,
									/**/
									PostalCode = case when cl.codpost ='' then 'Desconhecido' else left(convert(varchar,cl.codpost),20) end,
									/* Region (opcional) */
									/**/
									Country = case when cl.codigop = '' then 'Desconhecido' else UPPER(LEFT(LTRIM(RTRIM(cl.codigop)),12)) end
								for XML path (''), type),
							/**/
							Telephone = case when cl.telefone='' then null else left(convert(varchar,cl.telefone),20) end,
							/**/
							Fax = case when cl.fax='' then null else left(convert(varchar,cl.fax),20) end,
							/* Email (opcional) */
							/* Website (opcional) */
							/* Indicador de autofaturação */
							SelfBillingIndicator = 0
						from
							#t_saft_cl cl
						where
							cl.no != 200
						group by
							cl.no, cl.estab, cl.nome, cl.morada, cl.codpost, cl.conta,
							cl.telefone, cl.codigop, cl.fax, cl.local, cl.ncont
						/* Clientes Virtuais (não existem na tabela de Clientes)
						ID gerado sequencialmente APENAS derivado das vendas feitas ao cliente "Venda a Dinheiro" com alterações de dados */
						union all
						select 
							/**/
							CustomerID = case 
											/* Vendas feitas ao cliente 200 com alteração de dados (Clientes Virtuais) */ 
											when cl.doc_nome != @nomeClienteDuzentos or cl.doc_morada != '' or cl.doc_telefone != '' or cl.doc_ncont != '999999990' 
												then left(convert(varchar,cl.no) + '-A'  + convert(varchar,cl.doc_ndoc) + '/' + convert(varchar,cl.doc_fno) + '/' + convert(varchar,cl.estab),30)
											/* Vendas feitas ao cliente "Venda a Dinheiro" sem alteração de dados */
											else left(convert(varchar,cl.no)+'/'+convert(varchar,cl.estab),30)
										end,
							/**/
							AccountID =	case when cl.conta='' then 'Desconhecido' else left(cl.conta,30) end,
							/**/
							CustomerTaxID = case when cl.doc_ncont in ('','.') then '999999990' else left(replace(
																											REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( 
																											REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( 
																											REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( 
																											REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( 
																											REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( 
																											REPLACE( REPLACE( REPLACE( REPLACE(		
																												cl.doc_ncont
																											,char(0x0000),'') ,char(0x0001),'') ,char(0x0002),'') ,char(0x0003),'') ,char(0x0004),'') 
																											,char(0x0005),'') ,char(0x0006),'') ,char(0x0007),'') ,char(0x0008),'') ,char(0x000B),'') 
																											,char(0x000C),'') ,char(0x000E),'') ,char(0x000F),'') ,char(0x0010),'') ,char(0x0011),'') 
																											,char(0x0012),'') ,char(0x0013),'') ,char(0x0014),'') ,char(0x0015),'') ,char(0x0016),'') 
																											,char(0x0017),'') ,char(0x0018),'') ,char(0x0019),'') ,char(0x001A),'') ,char(0x001B),'') 
																											,char(0x001C),'') ,char(0x001D),'') ,char(0x001E),'') ,char(0x001F),'')
																											,' ',''),20) end,
							/**/
							CompanyName = case
											when cl.doc_nome = @nomeClienteDuzentos then 'Consumidor final'
											when cl.doc_nome = '' then 'Desconhecido'
											else LEFT(
													replace(replace(replace(replace(replace(replace(replace(replace(cl.doc_nome
														,CHAR(39),''),CHAR(34),''),CHAR(38),''),CHAR(14),''),CHAR(2),'')
														,CHAR(8),''),'<',''),'>','')
												,100)
											end,
							/* Subtabela BillingAddress */ 
							BillingAddress = (
								select
									/**/
									AddressDetail = case 
														when isnull(cl.doc_morada,'') = '' then 'Desconhecido'
														else LEFT(
																replace(replace(replace(replace(replace(replace(replace(replace(cl.doc_morada
																	,CHAR(39),''),CHAR(34),''),CHAR(38),''),CHAR(14),''),CHAR(2),'')
																	,CHAR(8),''),'<',''),'>','')
															,100)
														end,
									/**/
									City = case when cl.doc_local ='' then 'Desconhecida' else replace(cl.doc_local,char(39),'') end,
									/**/
									PostalCode = case when cl.doc_codpost ='' then 'Desconhecido' else left(convert(varchar,cl.doc_codpost),20) end,
									/**/
									Country = case when cl.codigop = '' then 'Desconhecido' else UPPER(LEFT(RTRIM(LTRIM(cl.codigop)),12)) end
								for XML path (''), type),
							/* Subtabela ShipToAddress */ 
							ShipToAddress = (
								select
									/**/
									AddressDetail = case 
														when cl.doc_morada is null or cl.doc_morada='' then 'Desconhecido'
														else LEFT(
																replace(replace(replace(replace(replace(replace(replace(replace(cl.doc_morada
																	,CHAR(39),''),CHAR(34),''),CHAR(38),''),CHAR(14),''),CHAR(2),'')
																	,CHAR(8),''),'<',''),'>','')
															,100)
														end,
									/**/
									City = case when cl.doc_local='' then 'Desconhecida' else replace(cl.doc_local,char(39),'') end,
									/**/
									PostalCode = case when cl.doc_codpost='' then 'Desconhecido' else left(convert(varchar,cl.doc_codpost),20) end,
									/**/
									Country = case when cl.codigop = '' then 'Desconhecido' else UPPER(LEFT(RTRIM(LTRIM(cl.codigop)),12)) end
								for XML path (''), type),
							/**/
							Telephone = case 
											when cl.doc_telefone!='' and cl.doc_telefone != cl.telefone then left(convert(varchar,cl.doc_telefone),20)
											when cl.telefone='' then null 
											else left(convert(varchar,cl.telefone),20)
										end,						
							/**/
							Fax = case when cl.fax='' then null else left(convert(varchar,cl.fax),20) end, 
							/**/
							SelfBillingIndicator = 0
						from
							#t_saft_cl cl
						where
							cl.no = 200
							/* Campos que, quando alterados, devem gerar os novos IDs virtuais */
							and (
								cl.doc_nome != @nomeClienteDuzentos
								or cl.doc_ncont != '999999990'
								or cl.doc_morada != ''
								or cl.doc_telefone != ''
							)
					) x 	
					for xml path ('Customer'), type),
					/* 
						Tabela Supplier: Fornecedores 
					*/
					(Select 
						/* identificador único */
						SupplierID = left(convert(varchar,fl.no)+ '/' + convert(varchar,fl.estab),30),
						/**/
						AccountID = case when fl.conta='' then 'Desconhecido' else left(fl.conta,30) end, 
						/**/
						SupplierTaxID = case when fl.ncont='' then '999999990' else left(replace(fl.ncont,' ',''),20) end,						
						/**/
						CompanyName = case 
										when fl.nome='' then 'Desconhecido' 
										else LEFT(
												replace(replace(replace(replace(replace(replace(replace(replace(fl.nome
													,CHAR(39),''),CHAR(34),''),CHAR(38),''),CHAR(14),''),CHAR(2),'')
													,CHAR(8),''),'<',''),'>','')
											,100)
										end,
						/**/
						Contact = case when fl.contacto='' then null else left(convert(varchar,fl.contacto),50) end,
						/* Subtabela BillingAddress */ 
						(select
							/**/																																							
							AddressDetail = case 
												when fl.morada is null or fl.morada='' then 'Desconhecido'
												else LEFT(
														replace(replace(replace(replace(replace(replace(replace(replace(fl.morada
															,CHAR(39),''),CHAR(34),''),CHAR(38),''),CHAR(14),''),CHAR(2),'')
															,CHAR(8),''),'<',''),'>','')
													,100)
												end,
							/**/
							City =	case when fl.local='' then 'Desconhecido' else left(fl.local,50) end,
							/**/
							PostalCode = case when fl.codpost='' then '0000-000' else LEFT(convert(varchar,fl.codpost),20) end,
							/**/
							Country = case when fl.pncont = '' then 'PT' else UPPER(LEFT(RTRIM(LTRIM(fl.pncont)),12)) END
						for xml path ('BillingAddress'), type),
						/* Subtabela ShipFromAddress */ 
						(select																																										
							/**/
							AddressDetail =	case 
												when fl.morada is null or fl.morada='' then 'Desconhecido'
												else LEFT(
														replace(replace(replace(replace(replace(replace(replace(replace(fl.morada
															,CHAR(39),''),CHAR(34),''),CHAR(38),''),CHAR(14),''),CHAR(2),'')
															,CHAR(8),''),'<',''),'>','')
													,100)
												end,
							/**/
							City = case when fl.local='' then 'Desconhecido' else left(fl.local,50) end,
							/**/
							PostalCode = case when fl.codpost='' then '0000-000' else LEFT(convert(varchar,fl.codpost),20) end,
							/**/
							Country =case when fl.pncont = '' then 'PT' else UPPER(LEFT(LTRIM(RTRIM(fl.pncont)),12)) END
						for xml path ('ShipFromAddress'), type),
						/**/
						Telephone = case when fl.telefone='' then null else left(convert(varchar,fl.telefone),20) end,
						/**/
						Fax = case when fl.fax='' then null else left(convert(varchar,fl.fax),20) end,
						/**/
						SelfBillingIndicator = 0 
					from
						fl (nolock)
					order by 
						fl.no, fl.estab 
					for xml path ('Supplier'), type),
					/* Tabela de Produtos (Product) */
					(Select 
						/* Indicador de produto ou serviço 
						“P” - Produtos;
						“S” – Serviços;
						“O” – Outros (Ex: portes debitados, adiantamentos recebidos ou alienação de ativos);
						“E” – Impostos Especiais de Consumo – (ex.:IABA, ISP, IT);
						“I” – Impostos, taxas e encargos parafiscais, exceto IVA e IS que deverão ser refletidos na tabela 2.5 */
						ProductType = case 
										when st.stns=1 then left('S',1)
										when st.stns=1 and st.ref='9999999' then left('O',1) 
										else left('P',1) 
										end, 
						/* Código produto (c 60) */
						ProductCode = left(ltrim(rtrim(convert(varchar,st.ref))),30),
						/**/
						ProductGroup = case when st.faminome='' then null else left(st.faminome,50) end,
						/**/
						ProductDescription = case 
												when st.design is null or st.design='' then 'Desconhecido'
												else LEFT(
														replace(replace(replace(replace(replace(replace(replace(replace(st.design
															,CHAR(39),''),CHAR(34),''),CHAR(38),''),CHAR(14),''),CHAR(2),'')
															,CHAR(8),''),'<',''),'>','')
													,200)
												end,
						/**/
						ProductNumberCode = rtrim(case 
													when convert(varchar,st.codigo) = '' then left(ltrim(rtrim(convert(varchar,st.ref))),50) 
													else left(convert(varchar,st.codigo),50) 
													end)
						/* Subtabela CustomsDetails (opcional) */
							/* CNCode (opcional) */
							/* UNNumber (opcional) */
					from 
						#t_saft_st st
					order by
						st.ref
					for xml path ('Product'), type),
					/* 
						Tabela TaxTable: Tabela de Regimes de IVA 
					*/
					(select
						/* Subtabela TaxTableEntry: Registo na tabela de impostos */
						(select
							/* Neste campo deve ser indicado o tipo de imposto. Deve ser preenchido com: 
							“IVA” – Imposto sobre o valor acrescentado; 
							“IS” – Imposto do selo;
							“NS” – Não sujeição a IVA ou IS. */
							TaxType = left('IVA',3),
							/* País ou região do imposto (ISO 3166-1-alpha-2)
								No  caso  das  Regiões  Autónomas  da Madeira e Açores deve ser preenchido com:
									“PT-AC” – Espaço  fiscal  da  Região Autónoma dos Açores;
									“PT-MA” – Espaço  fiscal  da  Região Autónoma da Madeira */
							TaxCountryRegion = left(descricao,5), 
							/* Código do imposto
								Caso do campo (TaxType) = IVA, deve ser preenchido com: 
									“RED” – Taxa reduzida;
									“INT” – Taxa intermédia;
									“NOR” – Taxa normal;
									“ISE” – Isenta;
									“OUT” – Outros,  aplicável  para  os regimes especiais de IVA.
								Caso do campo (TaxType) = IS, deve ser preenchido com:
									O código da verba respetiva;
									“ISE” – Isenta.
								Caso  de não sujeição deve ser preenchido com “NS”.
								Nos recibos emitidos sem imposto discriminado deve ser preenchido com “NA” */
							TaxCode = left(codigo,10), --case when convert(int,taxa) = 0 then 'OUT' else (select top 1 left(codigo,10) from regiva where tabiva=taxa) end,
							/**/
							Description = left(desctaxa,255),
							/* TaxExpirationDate (opcional) */
							/**/
							TaxPercentage =	taxa
							/* TaxAmount 
								(O preenchimento é obrigatório, no caso de se tratar de uma verba fixa de imposto do selo) */
						from
							regiva (nolock)
						order by
							descricao
						for XML path ('TaxTableEntry'), type)
					for Xml path ('TaxTable'), type)
				/* Final da tabela MasterFiles */
				for XML path ('MasterFiles'), type),
				/*
					Tabela movimentos contabilisticos (GeneralLedgerEntries)
					Não é necessário para o SAF-T de faturação
				*/
				/*
					4. Tabela documentos comerciais (SourceDocuments)
				*/
				(select
					/*
						4.1. Tabela SalesInvoices
					*/
					case when not exists (select * from #t_salesInvoices) then null else 
					(select
						/* Número de registos de documentos comerciais */
						NumberOfEntries = (select isnull(Count(distinct docstamp),0) from #t_salesInvoices),
						/* Total dos débitos 
							Deve conter a soma de controlo do (DebitAmount), dela excluindo os documentos em que o campo
							(InvoiceStatus) seja do tipo “A” ou “F” */
						TotalDebit = (
							select 
								TotalLinha = STR(round(abs(sum(
												case 
													when (case when sii.ivaincl = 1 then (sii.etiliquido / (sii.iva/100+1)) else sii.etiliquido end) <0 
														then (case when sii.ivaincl = 1 then (sii.etiliquido / (sii.iva/100+1)) else sii.etiliquido end)
													else 0
													end
											)),2),10,2)
							from
								#t_salesInvoices sii
							where
								sii.anulado=0
								and ((sii.qtt <> 0 or sii.etiliquido <> 0) OR  ( sii.anulado = 1 ))
						),
						/* Total dos créditos
							Deve conter a soma de controlo do campo (CreditAmount), dela excluindo os documentos
							em que o campo (InvoiceStatus) seja do tipo “A” ou “F” */
						TotalCredit = (
							select 
								TotalLinha = STR(round(abs(sum(
												case 
													when (case when sii.ivaincl = 1 then (sii.etiliquido / (sii.iva/100+1)) else sii.etiliquido end) >0 
													then (case when sii.ivaincl = 1 then (sii.etiliquido / (sii.iva/100+1)) else sii.etiliquido end)
													else 0 
													end
											)),2),10,2)
							from
								#t_salesInvoices sii
							where 			
								anulado = 0 
								and ((sii.qtt <> 0 or sii.etiliquido <> 0) OR  ( sii.anulado = 1 ))
						),
						/* Subtabela Invoice */
						(select
							/**/
							InvoiceNo = left(isnull(s.invoiceType,'FT') + ' ' + convert(varchar,s.ndoc) + '/' + CONVERT(varchar,s.fno),60),
							/* Código Único do Documento (Preenchido com 0 até à sua regulamentação) */
							ATCUD = '0',
							/* SubTabela DocumentStatus */
							(select
								/* Status do documento
									"N” – Normal;
									“S” – Autofaturação;
									“A” – Documento anulado;
									“R” – Documento de resumo doutros documentos criados noutras aplicações e gerado nesta aplicação;
									“F” – Documento faturado. */
								InvoiceStatus = case when s.anulado = 0 then 'N' else 'A' end,
								/* Data e hora do estado atual do documento */
								InvoiceStatusDate = convert(varchar,CONVERT(date,s.ousrdata))+ 'T' + s.ousrhora,
								/* Reason (opcional) Deve ser indicada a razão que levou à alteração de estado do documento. */
								/* Código do utilizador responsável pelo estado atual */
								SourceID = LEFT(s.vendnm,30),
								/* Origem do documento. Deve ser preenchido com: 
									“P” – Documento produzido na aplicação;
									“I” – Documento integrado e produzido noutra aplicação;
									“M” – Documento proveniente de recuperação ou de emissão manual; */
								SourceBilling = 'P'
							for XML path ('DocumentStatus'), type),	
							/* Chave do documento 
								Assinatura nos termos da Portaria n.º 363/2010, de 23 de junho.
								O campo deve ser preenchido com “0”, caso não haja obrigatoriedade de certificação */
							Hash = isnull(ltrim(rtrim(left(s.hash,172))),'0'),
							/* Chave de controlo 
								O campo deve ser preenchido com “0”, caso o documento seja gerado por um programa não certificado */
							HashControl = case when s.hash is null or s.hash='' then '0' else '1' end,
							/* Período contabilístico (mês) */
							Period = DATEPART(month,s.docdata),
							/**/
							InvoiceDate = convert(date,s.docdata), 
							/* Tipo de documento.
								“FT” – Fatura,  emitida  nos  termos  do artigo 36.º do Código do IVA;
								“FS” – Fatura   simplificada,   emitida nos  termos  do  artigo  40.º  do  Código do IVA;
								“FR” – Fatura-recibo;
								“ND” – Nota de débito;
								“NC” – Nota de crédito;
								“VD” – Venda a dinheiro e factura/recibo; (a)
								“TV” – Talão de venda; (a)
								“TD” – Talão de devolução; (a)
								“AA” – Alienação de ativos; (a)
								“DA” – Devolução de  ativos. (a)
								(a) Para os dados até 2012-12-31 */
							InvoiceType = left(isnull(s.invoiceType,'FT'),2),
							/* SpecialRegimes */
							(select
								/* “1” se respeitar a autofaturação e com “0” no caso contrário */
								SelfBillingIndicator = 0,
								/* Indicador da existência de adesão ao regime de IVA de Caixa. 
									 “1” se houver adesão e “0” no caso contrário */
								CashVATSchemeIndicator = 0,
								/* “1” se respeitar a faturação emitida em nome e por conta de terceiros e com “0” no caso contrário */
								ThirdPartiesBillingIndicator = 0
							for XML path ('SpecialRegimes'), type),
							/* Utilizador que gerou o documento */
							SourceID = LEFT(s.vendnm,30),
							/* código CAE da atividade relacionada com a emissão deste documento */
							EACCode = LEFT(RTRIM(LTRIM(s.cae)),5),
							/* Data da gravação do registo ao segundo, no momento da assinatura */
							SystemEntryDate = convert(varchar,convert(date,s.ousrdata)) + 'T' + CONVERT(varchar,case when s.ousrhora='' then '00:00:00' else s.ousrhora end),
							/* TransactionID
								(obrigatório apenas em sistemas integrados de faturação e contabilidade, ver doc oficial) */
							/* Chave única da tabela (Customer) respeitando a regra aí definida para o campo (CustomerID) */
							CustomerID = case 
											when s.no=200 and (s.ncont!='999999990' or s.nome!=@nomeClienteDuzentos or s.morada!='' or s.telefone!='') 
											then left(convert(varchar,s.no) + '-A' + convert(varchar,s.ndoc) + '/' + convert(varchar,s.fno) + '/' + convert(varchar,s.estab),30)
											else left(convert(varchar,s.no) + '/' + convert(varchar,s.estab),30)
											end,
							/* Subtabela ShipTo (opcional) */
								/* DeliveryID (opcional) */
								/* DeliveryDate (opcional) */
								/* WarehouseID (opcional) */
								/* LocationID (opcional) */
								/* Subtabela Morada (opcional) */
									/* BuildingNumber (opcional) */
									/* StreetName (opcional) */
									/* AddressDetail */
									/* City */
									/* PostalCode */
									/* Region (opcional) */
									/* Country */
							/* Subtabela ShipFrom (opcional) */
								/* DeliveryID (opcional) */
								/* DeliveryDate (opcional) */
								/* WarehouseID (opcional) */
								/* LocationID (opcional) */
								/* Subtabela Morada (opcional) */
									/* BuildingNumber (opcional) */
									/* StreetName (opcional) */
									/* AddressDetail */
									/* City */
									/* PostalCode */
									/* Region (opcional) */
									/* Country */
							/* MovementEndTime (opcional) */
							/* MovementStartTime (obrigatório se o documento servir igualmente de documento de transporte
								de acordo com o disposto no Regime de Bens em Circulação, aprovado pelo 
								Decreto - Lei n.º147/2003, de 11 de julho) */
							/* Subtabela Line */
							(select
								/* Mesma ordem do documento original */
								LineNumber = ROW_NUMBER() over (order by si.lordem asc), 
								/* Subtabela OrderReferences: 
									origem de cada linha nas tabelas de Facturação de todos os documentos excepto Nt/Crédito!
									(pode ser gerada qts vezes quanto necessário) */  
								(select
									case
										when si.tiposaft != 'NC'
											then (select
														/* Número do documento de origem */
														OriginatingON = left(fi.nmdoc + ' ' + convert(varchar,fi.ndoc) + '/' + CONVERT(varchar,fi.fno),255),
														/* Data do documento de origem */
														OrderDate = convert(date,fi.rdata)
													from
														fi (nolock)
													where
														fi.fistamp = si.ofistamp
													for xml path ('OrderReferences'), type)
											else null 
										end),
								/* Identificador do produto ou serviço */
								ProductCode = case 
												when si.ref='' 
													then case 
															when si.oref='' then '9999999' 
															else ltrim(rtrim(si.oref)) 
															end	
												else ltrim(rtrim(left(si.ref,30))) 
												end,
								/* Descrição do produto ou serviço */
								ProductDescription = case 
														when si.design is null or si.design='' then 'Desconhecido'
														else LEFT(
																replace(replace(replace(replace(replace(replace(replace(replace(si.design
																	,CHAR(39),''),CHAR(34),''),CHAR(38),''),CHAR(14),''),CHAR(2),'')
																	,CHAR(8),''),'<',''),'>','')
															,200)
														end,								
								/**/
								Quantity = convert(int,si.qtt),
								/**/
								UnitOfMeasure = case
													when si.unidade='' then 'UN'
													else left(upper(convert(varchar,si.unidade)),20)
													end,
								/**/
								UnitPrice = case
												when (si.etiliquido / (si.iva/100+1)) > 0
													then case 
															when si.ivaincl=1 then abs((si.etiliquido / (si.iva/100+1)) / si.qtt)
															else abs(si.etiliquido/si.qtt)
															end
												--else 0
												else case 
															when si.ivaincl=1 then abs((si.etiliquido / (si.iva/100+1)) / si.qtt)
															else abs(si.etiliquido/si.qtt)
															end
												end,
								/* Valor tributável unitário 
									Valor tributável unitário que não concorre para o Total do documento sem impostos (NetTotal) */
								/* TaxBase */
								/**/
								TaxPointDate = convert(date,si.docdata),
								/* Campo References, apenas deve aparecer se o documento for do tipo SAFT "NC" 
								e tem que aparecer nesta posição (indicação DGCI); o texto do campo Reason é genérico */
								(select
									case 
										when si.tiposaft = 'NC' then 
											(select
												/* Referência à fatura ou fatura simplificada, através de identificação única da mesma, 
												nos sistemas em que exista. Deve ser utilizada a estrutura de numeração do campo de origem */
												Reference =	left(fi.nmdoc + ' ' + convert(varchar,fi.ndoc) + '/' + CONVERT(varchar,fi.fno),60),
												/* Motivo de emissão */
												Reason = left('Regularização / Devolução de Produto',50)
											from
												fi (nolock) as fi
											where																															
												fi.fistamp = si.ofistamp	
											for xml path ('References'), type)
										else null
									end),
								/* Descrição da linha */
								Description = case when si.design = '' then ' ' else left(si.design,200) end,
								/* Valor da linha sem imposto dos documentos a lançar a débito */
								DebitAmount = case 
												when (si.etiliquido / (si.iva/100+1))<0 
													then case 
														when si.ivaincl=1 then abs((si.etiliquido / (si.iva/100+1))) 
														else abs(si.etiliquido)
														end
												else null 
												end,
								/* Valor da linha sem imposto dos documentos a lançar a crédito */
								CreditAmount = case 
												when (si.etiliquido / (si.iva/100+1)) > 0
													then case
															when si.ivaincl=1 then abs((si.etiliquido / (si.iva/100+1)))
															else abs(si.etiliquido)
															end
												when (si.etiliquido / (si.iva/100+1))=0 then 0
												else null
												end,
								/*
									Subtabela Tax: o subquery com TOP 1 deve-se ao facto de que há vários REGIVAs para o código 0
								*/
								(select
									/* Código do tipo de imposto */
									TaxType = left('IVA',3),
									/* País ou região do imposto */
									TaxCountryRegion = left('PT',5), /* deveria ser obtido da tabela regIVA */
									/**/
									TaxCode = case when convert(int,si.iva) = 0 then 'OUT' else (select top 1 left(codigo,10) from regiva where tabiva=si.tabiva) end ,
									/**/
									TaxPercentage = convert(int,si.iva)
									/* TaxAmount  (Obrigatório, no caso de se tratar de uma verba fixa
										unitária de imposto de selo. Este valor, multiplicado pela quantidade
										(Quantity) concorre para o valor de imposto a pagar (TaxPayable) */
								for xml path ('Tax'), type),
								/* Desricao motivo de isenção de imposto (obrigatório quando isento de iva) */
								TaxExemptionReason =
									case
										when si.iva=0
											then --case
													--when si.motiseimp != '' then left(si.motiseimp,60)
													--when si.pais = 3 then left('Isento Artigo 14.º do CIVA (ou similar)',60)
													--when si.pais = 2 then left('Isento Artigo 14.º do RITI (ou similar).',60)
													--else
													 left('IVA - Não confere direito à dedução',60)
													--end
										else null
										end,
								/* TaxExemptionCode 
								TODO: Tem de ser melhorado para suportar os códigos de forma mais dinamica */
								TaxExemptionCode =
									case
										when si.iva=0
											then --case
													--when si.pais = 3 then 'M05'
													--when si.pais = 2 then 'M05'
													--else 
													'M10'
													--else (select top 1 code_motive from b_multidata where b_multidata.tipo = 'motiseimp' and b_multidata.campo=si.motiseimp)
													--end
										else null
										end,
								/* Código motivo de isenção de imposto (obrigatório quando isento de iva) */
								--TaxExemptionCode =
								--case
								--	when si.iva=0
								--		then case
								--				when si.motiseimp != '' then (SELECT top 1 code_motive FROM b_multidata (nolock) WHERE  tipo='motiseimp' and campo=LTRIM(RTRIM(si.motiseimp))) 
								--				when si.pais = 3 then 'M05' 
								--				when si.pais = 2 then 'M05'
								--				else 'M07'  
								--				end
								--	else null
								--	end,
								/* Deve refletir todos os descontos concedidos */
								SettlementAmount = case 
														when (convert(numeric,si.desconto)) > 0 or (convert(numeric,si.desc2)) > 0 or (convert(numeric,si.descval)) > 0 
															then abs(
																	abs(case
																			when si.ivaincl = 1 then (si.epv * si.qtt) / (si.iva / 100 + 1)
																			else si.epv * si.qtt
																			end)
																	-
																	abs(case
																			when si.ivaincl = 1 then si.etiliquido / (si.iva / 100 +1)
																			else si.etiliquido/si.qtt
																	end))
														else null
													end
								/* CustomsInformation - Informação aduaneira (opcional) */
								/* ARCNo - Código de referência administrativo (opcional) (pode ser gerado tantas vezes qt necessárias) */
							/* Final da subtabela Line */
							from
								#t_salesInvoices si
							where
								s.docstamp = si.docstamp
								and si.epromo=0
								and (
									(si.qtt <> 0 or si.etiliquido <> 0) OR  ( si.anulado = 1 )
								)
							order by 
								si.lordem
							for xml path ('Line'), type),
							/* Subtabela DocumentTotals */
							(select
								/* Valor do imposto a pagar */
								TaxPayable = ltrim(str(abs(s.ettiva),10,2)),
								/* Total do documento sem impostos */
								NetTotal = ltrim(str(abs(s.eivain1+s.eivain2+s.eivain3+s.eivain4+s.eivain5+s.eivain6+s.eivain7+s.eivain8+s.eivain9),10,2)),
								/* Total do documento com impostos */
								GrossTotal = ltrim(str(abs(s.etotal+s.efinv),10,2)),
								/* Subtabela Currrency, apenas exportada quando a moeda de um documento não é EURO */
								(select 
									case 
										when s.moeda not in ('PTE ou EURO','EURO','EUR', '') then 
											(select
												/* Código de moeda */
												CurrencyCode = case when s.moeda='' then left('EUR',3) else left(s.moeda,3) end,
												/* Valor total em moeda estrangeira */
												CurrencyAmount = str(abs(s.etotal),10,2)
												/* ExchangeRate - Taxa de câmbio 
												(este campo é obrigatório, mas não existe no software,
												se for emitida alguma fatura em moeda estrangeira é necessário preencher este campo) */
											for xml path ('Currency'), type)
										else null
									end
								),
								/* Subtabela Settlement, apenas exportada quando existe Desconto Financeiro 
									no cabeçalho de documento (pode ser gerada qts vezes qt necessárias) */
								(select 
									case 
										when s.efinv<>0 
											then (select
													/* Acordos de descontos futuros. Deve ser preenchido com os 
														acordos de descontos a aplicar no futuro sobre o valor presente */
													SettlementDiscount = left('0',30),
													/* Montante do desconto. Representa o valor acordado para desconto
														futuro sem afetar o valor presente do documento indicado no campo 
														total do documento com impostos (GrossTotal) */
													SettlementAmount = s.efinv,
													/* Data acordada para o desconto */
													SettlementDate = CONVERT(date,s.docdata)
													/* PaymentTerms - Acordos de pagamento (opcional) */
												for xml path ('Settlement'), type)
										else null
									end)
								/* Payment (opcional) */
									/* PaymentMechanism (opcional) */
									/* PaymentAmount (obrigatório) */
									/* PaymentDate (obrigatório) */
							for xml path ('DocumentTotals'),type)
							/* WithholdingTax - Retenção na fonte (opcional) */
								/* WithholdingTaxType - Código do tipo de imposto retido (opcional) */
								/* WithholdingTaxDescription - Motivo da retenção na fonte (opcional) */
								/* WithholdingTaxAmount - Montante da retenção na fonte (obrigatório) */
						from
							#t_salesInvoices s
						group by
							s.docstamp, s.ndoc,
							s.no, s.estab, s.nome, s.ncont, s.morada, s.telefone, s.pais,
							s.fno, s.docdata,
							s.anulado, s.vendnm, s.ousrdata, s.ousrhora,
							s.invoiceType, s.hash,
							s.entity, s.datatransporte, s.horaentrega,
							s.motiseimp, s.moeda,
							s.ettiva, s.ettiliq, s.etotal, s.efinv,
							s.eivain1,s.eivain2,s.eivain3,s.eivain4,
							s.eivain5,s.eivain6,s.eivain7,s.eivain8,s.eivain9,
							s.tiposaft,	s.cae
						order by
							s.ndoc, s.fno
						for xml path ('Invoice'), type)
					for xml path ('SalesInvoices'), type) end,
					/********************************/
					/*
						4.2. Tabela MovementOfGoods
					*/
					case when not exists (select * from #t_movOfGoods) then null else 
					(select
						/* Nr de registos das linhas de movimentos dos bens */
						NumberOfMovementLines = (select isnull(count(linstamp),0) from #t_movOfGoods),
						/* Soma das quantidades das linhas de movimentos dos bens excluindo os anulados
							(não existe conceito de anulado na bi) */
						TotalQuantityIssued = (select isnull(abs(sum(qtt)),0) from #t_movOfGoods),
						/* Subtabela StockMovement */
						(select
							/* Composto pelos seguintes elementos:
								o código interno do tipo de documento, 
								um espaço, 
								o identificador da série do documento, 
								uma barra (/) 
								e o número sequencial desse documento dentro dessa série. */
							DocumentNumber = left(isnull(mog.invoiceType,'FT') + ' ' + convert(varchar,mog.ndoc) + '/' + CONVERT(varchar,mog.fno),60),
							/* Código Único do Documento (Preenchido com 0 até à sua regulamentação) */
							ATCUD = '0',
							/* SubTabela DocumentStatus */
							(select
								/* Status do documento
									"N” – Normal;
									“T” – Por conta de terceiros;
									“A” – Documento anulado;
									“R” – Documento de resumo doutros documentos criados noutras aplicações e gerado nesta aplicação;
									“F” – Documento faturado. Ainda que parcialmente, quando para este documento também existe na tabela 4.1. SalesInvoices */
								MovementStatus = case when mog.anulado = 0 then 'N' else 'A' end,
								/* Data e hora do estado atual do documento */
								MovementStatusDate = convert(varchar,CONVERT(date,mog.ousrdata)) + 'T' + mog.ousrhora,
								/* Reason (opcional) Deve ser indicada a razão que levou à alteração de estado do documento. */
								/* Código do utilizador responsável pelo estado atual */
								SourceID = LEFT(mog.vendnm,30),
								/* Origem do documento. Deve ser preenchido com: 
									“P” – Documento produzido na aplicação;
									“I” – Documento integrado e produzido noutra aplicação;
									“M” – Documento proveniente de recuperação ou de emissão manual; */
								SourceBilling = 'P'
							for XML path ('DocumentStatus'), type),
							/* Chave do documento 
								Assinatura nos termos da Portaria n.º 363/2010, de 23 de junho.
								O campo deve ser preenchido com “0”, caso não haja obrigatoriedade de certificação */
							Hash = isnull(ltrim(rtrim(left(mog.hash,172))),'0'),
							/* Chave de controlo 
								O campo deve ser preenchido com “0”, caso o documento seja gerado por um programa não certificado */
							HashControl = case when mog.hash is null or mog.hash='' then '0' else '1' end,
							/* Período contabilístico (mês) */
							Period = DATEPART(month,mog.docdata),
							/**/
							MovementDate = convert(date,mog.docdata),
							/* Tipo de documento.
								“FT” – Fatura,  emitida nos termos  do artigo 36.º do Código do IVA;
								“FS” – Fatura   simplificada, emitida nos termos do artigo 40.º do Código do IVA;
								“FR” – Fatura-recibo;
								“ND” – Nota de débito;
								“NC” – Nota de crédito;
								“VD” – Venda a dinheiro e factura/recibo; (a)
								“TV” – Talão de venda; (a)
								“TD” – Talão de devolução; (a)
								“AA” – Alienação de ativos; (a)
								“DA” – Devolução de  ativos. (a)
								(a) Para os dados até 2012-12-31 */
							MovementType = left(isnull(mog.invoiceType,'GT'),2),
							--/* SpecialRegimes */
							--(select
							--	/* “1” se respeitar a autofaturação e com “0” no caso contrário */
							--	SelfBillingIndicator = 0,
							--	/* Indicador da existência de adesão ao regime de IVA de Caixa. 
							--		 “1” se houver adesão e “0” no caso contrário */
							--	CashVATSchemeIndicator = 0,
							--	/* “1” se respeitar a faturação emitida em nome e por conta de terceiros e com “0” no caso contrário */
							--	ThirdPartiesBillingIndicator = 0
							--for XML path ('SpecialRegimes'), type),
							/* Data da gravação do registo ao segundo, no momento da assinatura */
							SystemEntryDate = convert(varchar,convert(date,mog.ousrdata)) + 'T' + CONVERT(varchar,case when mog.ousrhora='' then '00:00:00' else mog.ousrhora end),
							/* TransactionID
								(obrigatório apenas em sistemas integrados de faturação e contabilidade, ver doc oficial) */
							/* Chave única da tabela (Customer) respeitando a regra aí definida para o campo (CustomerID) 
								(No caso de guias em que não se conhece o destinatário, deve ser utilizado o cliente genérico)
								(Este campo também deve ser preenchido no caso de guias que titulam a transferência de bens do próprio remetent) */
							CustomerID = case when mog.entity!= 'CL' then null 
											else case 
													when mog.no=200 and (mog.ncont!='999999990' or mog.nome!=@nomeClienteDuzentos or mog.morada!='' or mog.telefone!='') 
													then left(convert(varchar,mog.no) + '-A' + convert(varchar,mog.ndoc) + '/' + convert(varchar,mog.fno) + '/' + convert(varchar,mog.estab),30)
													else left(convert(varchar,mog.no) + '/' + convert(varchar,mog.estab),30)
													end
											end,
							/* Chave única da tabela (Supplier) respeitando a regra aí definida para o campo (CustomerID) 
								(No caso de guias em que não se conhece o destinatário, deve ser utilizado o cliente genérico)
								(Este campo também deve ser preenchido no caso de guias que titulam a transferência de bens do próprio remetent) */
							SupplierID = case 
											when mog.entity!= 'FL' then null 
											else left(convert(varchar,mog.no) + '/' + convert(varchar,mog.estab),30)
											end,
							/* Utilizador que gerou o documento */
							SourceID = LEFT(mog.vendnm,30),
							/* código CAE da atividade relacionada com a emissão deste documento */
							EACCode = (select LEFT(RTRIM(LTRIM(cae)),5) from empresa where site = @Loja),
							/* MovementComments - razão da emissão do documento (c 60) (opcional) */
							/* Subtabela ShipTo (opcional) */
								/* DeliveryID (opcional) */
								/* DeliveryDate (opcional) */
								/* WarehouseID (opcional) */
								/* LocationID (opcional) */
								/* Subtabela Morada (opcional) */
									/* BuildingNumber (opcional) */
									/* StreetName (opcional) */
									/* AddressDetail */
									/* City */
									/* PostalCode */
									/* Region (opcional) */
									/* Country */
							/* Subtabela ShipFrom (opcional) */
								/* DeliveryID (opcional) */
								/* DeliveryDate (opcional) */
								/* WarehouseID (opcional) */
								/* LocationID (opcional) */
								/* Subtabela Morada (opcional) */
									/* BuildingNumber (opcional) */
									/* StreetName (opcional) */
									/* AddressDetail */
									/* City */
									/* PostalCode */
									/* Region (opcional) */
									/* Country */
							/* MovementEndTime (opcional) */
							/* MovementStartTime (obrigatório se o documento servir igualmente de documento de transporte
								de acordo com o disposto no Regime de Bens em Circulação, aprovado pelo 
								Decreto - Lei n.º147/2003, de 11 de julho) */
							MovementStartTime = convert(varchar,convert(date,mog.datatransporte)) + 'T' + CONVERT(varchar,case when mog.horaentrega='' then '00:00:00' else mog.horaentrega + ':00' end),
							/* ATDocCodeID (opcional) */
							/* 
								Subtabela Line (4.2.3.21)
							*/
							(select
								/* Mesma ordem do documento original */
								LineNumber = ROW_NUMBER() over (order by mogFi.lordem asc),
								/* Subtabela OrderReferences (opcional): 
									origem de cada linha nas tabelas de Facturação de todos os documentos excepto Nt/Crédito!
									(pode ser gerada qts vezes quanto necessário) 
									(visto que no tipo de documentos incluidos nesta secção do SAF-T (guias transporte e encomendas) 
									a probablidade/relevância de estes serem criados a partir de outro documento, ou seja, terem um doc origem,
									ser muito baixa, optamos por não incluir esta subtabela */
										/* OriginatingON (opcional) */
										/* OrderDate (opcional) */
								/* Identificador do produto ou serviço */
								ProductCode = case
												when mogFi.ref='' then case 
																		when mogFi.oref='' then '9999999' 
																		else ltrim(rtrim(mogFi.oref))
																		end	
												else ltrim(rtrim(left(mogFi.ref,30)))
												end,
								/* Descrição do produto ou serviço */
								ProductDescription = case 
														when isnull(mogFi.design,'')='' then 'Desconhecido'
														else LEFT(
																replace(replace(replace(replace(replace(replace(replace(replace(mogFi.design
																	,CHAR(39),''),CHAR(34),''),CHAR(38),''),CHAR(14),''),CHAR(2),'')
																	,CHAR(8),''),'<',''),'>','')
															,200)
														end,
								/**/
								Quantity = convert(int,mogFi.qtt),
								/**/
								UnitOfMeasure = case
													when mogFi.unidade='' then 'UN'								
													else left(upper(convert(varchar,mogFi.unidade)),20)
													end,
								/**/
								UnitPrice = case
												when (mogFi.etiliquido / (mogFi.iva/100+1)) > 0
													then case 
															when mogFi.ivaincl=1 then abs((mogFi.etiliquido / (mogFi.iva/100+1)) / mogFi.qtt) 
															else abs(mogFi.etiliquido/mogFi.qtt)
															end
												--else 0
												else case 
															when mogFi.ivaincl=1 then abs((mogFi.etiliquido / (mogFi.iva/100+1)) / mogFi.qtt)
															else abs(mogFi.etiliquido/mogFi.qtt)
															end
												end,
								/* Description */
								Description = case 
														when isnull(mogFi.design,'')='' then 'Desconhecido'
														else LEFT(
																replace(replace(replace(replace(replace(replace(replace(replace(mogFi.design
																	,CHAR(39),''),CHAR(34),''),CHAR(38),''),CHAR(14),''),CHAR(2),'')
																	,CHAR(8),''),'<',''),'>','')
															,200)
														end,
								/* ProductSerialNumber (opcional) */
									/* SerialNumber */
								/* Valor da linha sem imposto dos documentos a lançar a débito */
								DebitAmount = case
												when entradaSock = 1
													then (case when mogFi.ivaincl = 1 then abs((mogFi.etiliquido / (mogFi.iva/100+1))) else abs(mogFi.etiliquido) end)
												else null
												end,
								/* Valor da linha sem imposto dos documentos a lançar a crédito */
								CreditAmount = case
												when entradaSock = 0
													then (case when mogFi.ivaincl = 1 then abs((mogFi.etiliquido / (mogFi.iva/100+1))) else abs(mogFi.etiliquido) end)
												else null
												end,
								/*
									Subtabela Tax: o subquery com TOP 1 deve-se ao facto de que há vários REGIVAs para o código 0
								*/
								(select
									/* Código do tipo de imposto */
									TaxType = left('IVA',3),
									/* País ou região do imposto */
									TaxCountryRegion = left('PT',5), /* deveria ser obtido da tabela regIVA */
									/**/
									TaxCode = case when convert(int,mogFi.iva) = 0 then 'OUT' else (select top 1 left(codigo,10) from regiva where tabiva=mogFi.tabiva) end,
									--(select top 1 left(codigo,10) from regiva where tabiva=mogFi.tabiva),
									/**/
									TaxPercentage = convert(int,mogFi.iva)
									/* TaxAmount  (Obrigatório, no caso de se tratar de uma verba fixa
										unitária de imposto de selo. Este valor, multiplicado pela quantidade
										(Quantity) concorre para o valor de imposto a pagar (TaxPayable) */
								for xml path ('Tax'), type),
								/* Descrição motivo de isenção de imposto (obrigatório quando isento de iva) */
								TaxExemptionReason =
									case
										when mogFi.iva=0 then 
												--case
												--	when mogFi.motiseimp != '' then left(mogFi.motiseimp,60)
												--	when (Select pais FROM b_utentes (nolock) WHERE b_utentes.no = mogFi.no and b_utentes.estab = mogFi.estab) = 3
												--		then left('Isento Artigo 14.º do CIVA (ou similar)',60)
												--	when (Select pais FROM b_utentes (nolock) WHERE b_utentes.no = mogFi.no and b_utentes.estab = mogFi.estab) = 2
												--		then left('Isento Artigo 14.º do RITI (ou similar).',60)
												--	else 
												left('IVA - Não confere direito à dedução',60)
												--	end
										else null
										end,
								/* TaxExemptionCode 
								 Código motivo de isenção de imposto (obrigatório quando isento de iva) */
								 TaxExemptionCode =
									case
										when mogFi.iva=0 then 
												--case
												--	when (Select pais FROM b_utentes (nolock) WHERE b_utentes.no = mogFi.no and b_utentes.estab = mogFi.estab) = 3
												--		then 'M05'
												--	when (Select pais FROM b_utentes (nolock) WHERE b_utentes.no = mogFi.no and b_utentes.estab = mogFi.estab) = 2
												--		then 'M05'
												--	else 
												'M10'
													--else (select top 1 code_motive from b_multidata where b_multidata.tipo = 'motiseimp' and b_multidata.campo=mogFi.motiseimp)
												--	end
										else null
										end,
								/* Descrição motivo de isenção de imposto (obrigatório quando isento de iva) */
								--TaxExemptionCode =
								--	case
								--		when mogFi.iva=0 then 
								--			case
								--				when mogFi.motiseimp != '' then (SELECT top 1 code_motive FROM b_multidata (nolock) WHERE  tipo='motiseimp' and campo=LTRIM(RTRIM(mogFi.motiseimp))) 
								--				when (Select pais FROM b_utentes (nolock) WHERE b_utentes.no = mogFi.no and b_utentes.estab = mogFi.estab) = 3
								--						then 'M05'
								--				when (Select pais FROM b_utentes (nolock) WHERE b_utentes.no = mogFi.no and b_utentes.estab = mogFi.estab) = 2
								--						then 'M05'
								--				else 'M07'
								--				end
								--		else null
								--end,
								/* Deve refletir todos os descontos concedidos */
								SettlementAmount = case 
														when (convert(numeric,mogFi.desconto)) > 0 or (convert(numeric,mogFi.desc2)) > 0 or (convert(numeric,mogFi.descval)) > 0
															then abs(
																	abs(case
																			when mogFi.ivaincl = 1 then (mogFi.epv * mogFi.qtt) / (mogFi.iva/100+1)
																			else mogFi.epv * mogFi.qtt
																			end)
																	-
																	abs(case
																			when mogFi.ivaincl = 1 then mogFi.etiliquido / (mogFi.iva/100+1)
																			else mogFi.etiliquido / mogFi.qtt
																	end))
														else null
													end
								/* CustomsInformation - Informação aduaneira (opcional) */
									/* ARCNo - Código de referência administrativo (opcional) (pode ser gerado tantas vezes qt necessárias) */
									/* IECAmount - Montante do imposto especial de consumo da linha */
							/* Final da subtabela Line */
							from
								#t_movOfGoods mogFi
							where
								mogFi.docstamp = mog.docstamp
								and mogFi.epromo=0
								and (
									(mogFi.qtt <> 0 or mogFi.etiliquido <> 0) OR (mog.anulado = 1) 
								)
							order by 
								mogFi.lordem
							for xml path ('Line'), type),
							/* Subtabela DocumentTotals */
							(select
								/* Valor do imposto a pagar */
								TaxPayable = ltrim(str(abs(mog.ettiva),10,2)),
								/* Total do documento sem impostos */
								NetTotal = ltrim(str(abs(mog.ettiliq),10,2)),
								/* Total do documento com impostos */
								GrossTotal = ltrim(str(abs(mog.etotal /*+mog.efinv*/),10,2)),
								/* Subtabela Currrency, apenas exportada quando a moeda de um documento não é EURO */
								(select 
									case 
										when mog.moeda not in ('PTE ou EURO','EURO','EUR', '') then 
											(select
												/* Código de moeda */
												CurrencyCode = case when mog.moeda='' then left('EUR',3) else left(mog.moeda,3) end,
												/* Valor total em moeda estrangeira */
												CurrencyAmount = str(abs(mog.etotal),10,2)
												/* ExchangeRate - Taxa de câmbio 
													(este campo é obrigatório, mas não existe no software,
													se for emitida alguma fatura em moeda estrangeira é necessário preencher este campo) */
											for xml path ('Currency'), type)
										else null
									end)
							for xml path ('DocumentTotals'),type )
						from
							#t_movOfGoods mog
						group by
							mog.docstamp, mog.ndoc,
							mog.no, mog.estab, mog.nome, mog.ncont, mog.morada, mog.telefone,
							mog.fno, mog.docdata,
							mog.anulado, mog.vendnm, mog.ousrdata, mog.ousrhora,
							mog.invoiceType, mog.hash,
							mog.entity, mog.datatransporte, mog.horaentrega,
							mog.motiseimp, mog.moeda,
							mog.ettiva, mog.ettiliq, mog.etotal, mog.efinv
						order by
							mog.ndoc, mog.fno
						for xml path ('StockMovement'), type)
					for xml path ('MovementOfGoods'), type) end,
					/********************************/
					/*
						4.3. WorkingDocuments
					*/
					case when not exists (select * from #t_workingDocs) then null else (select
						/* Nr de registos das linhas de movimentos dos bens */
						--NumberOfEntries = (select isnull(count(distinct docstamp),0) from #t_workingDocs),
						NumberOfEntries = (select count(distinct docstamp) from #t_workingDocs),
						/* Total dos débitos 
							Deve conter a soma de controlo do (DebitAmount), dela excluindo os documentos em que o campo
							(WorkStatus) seja do tipo “A” ou “F” */
						TotalDebit = (
							select 
								TotalLinha = STR(round(abs(sum(
												case 
													when (case when wdii.ivaincl = 1 then (wdii.etiliquido / (wdii.iva/100+1)) else wdii.etiliquido end) <0 
														then (case when wdii.ivaincl = 1 then (wdii.etiliquido / (wdii.iva/100+1)) else wdii.etiliquido end)
													else 0
													end
											)),2),10,2)
							from
								#t_workingDocs wdii
							where
								wdii.anulado=0
								and ((wdii.qtt <> 0 or wdii.etiliquido <> 0) OR  ( wdii.anulado = 1 ))
						),
						/* Total dos créditos
							Deve conter a soma de controlo do campo (CreditAmount), dela excluindo os documentos
							em que o campo (WorkStatus) seja do tipo “A” ou “F” */
						TotalCredit = (
							select 
								TotalLinha = STR(round(abs(sum(
												case 
													when (case when wdii.ivaincl = 1 then (wdii.etiliquido / (wdii.iva/100+1)) else wdii.etiliquido end) >0 
													then (case when wdii.ivaincl = 1 then (wdii.etiliquido / (wdii.iva/100+1)) else wdii.etiliquido end)
													else 0 
													end
											)),2),10,2)
							from
								#t_workingDocs wdii
							where 			
								anulado = 0 
								and ((wdii.qtt <> 0 or wdii.etiliquido <> 0) OR  ( wdii.anulado = 1 ))
						),
						/* Subtabela WorkDocument */
						(select
							/* Composto pelos seguintes elementos:
								o código interno do tipo de documento, 
								um espaço, 
								o identificador da série do documento, 
								uma barra (/) 
								e o número sequencial desse documento dentro dessa série. */
							DocumentNumber = left(isnull(wd.invoiceType,'FT') + ' ' + convert(varchar,wd.ndoc) + '/' + CONVERT(varchar,wd.fno),60),
							/* Código Único do Documento (Preenchido com 0 até à sua regulamentação) */
							ATCUD = '0',
							/* SubTabela DocumentStatus */
							(select
								/* Status do documento
									"N” – Normal;
									“A” – Documento anulado;
									“F” – Documento faturado. Ainda que parcialmente, quando para este documento também existe na tabela 4.1. SalesInvoices */
								WorkStatus = case when wd.anulado = 0 then 'N' else 'A' end,
								/* Data e hora do estado atual do documento */
								WorkStatusDate = convert(varchar,CONVERT(date,wd.ousrdata)) + 'T' + wd.ousrhora,
								/* Reason (opcional) Deve ser indicada a razão que levou à alteração de estado do documento. */
								/* Código do utilizador responsável pelo estado atual */
								SourceID = LEFT(wd.vendnm,30),
								/* Origem do documento. Deve ser preenchido com: 
									“P” – Documento produzido na aplicação;
									“I” – Documento integrado e produzido noutra aplicação;
									“M” – Documento proveniente de recuperação ou de emissão manual; */
								SourceBilling = 'P'
							for XML path ('DocumentStatus'), type),
							/* Chave do documento 
								Assinatura nos termos da Portaria n.º 363/2010, de 23 de junho.
								O campo deve ser preenchido com “0”, caso não haja obrigatoriedade de certificação */
							Hash = isnull(ltrim(rtrim(left(wd.hash,172))),'0'),
							/* Chave de controlo 
								O campo deve ser preenchido com “0”, caso o documento seja gerado por um programa não certificado */
							HashControl = case when wd.hash is null or wd.hash='' then '0' else '1' end,
							/* Período contabilístico (mês) */
							Period = DATEPART(month,wd.docdata),
							/**/
							WorkDate = convert(date,wd.docdata),
							/* Tipo de documento.
								(Ver notas no fim deste doc) */
							WorkType = left(isnull(wd.invoiceType,'OU'),2),
							/* Utilizador que gerou o documento */
							SourceID = LEFT(wd.vendnm,30),
							/* código CAE da atividade relacionada com a emissão deste documento */
							EACCode = (select LEFT(RTRIM(LTRIM(cae)),5) from empresa where site = @Loja),
							/* Data da gravação do registo ao segundo, no momento da assinatura */
							SystemEntryDate = convert(varchar,convert(date,wd.ousrdata)) + 'T' + CONVERT(varchar,case when wd.ousrhora='' then '00:00:00' else wd.ousrhora end),
							/* TransactionID
								(obrigatório apenas em sistemas integrados de faturação e contabilidade, ver doc oficial) */
							/* Chave única da tabela (Customer) respeitando a regra aí definida para o campo (CustomerID) 
								(No caso de guias em que não se conhece o destinatário, deve ser utilizado o cliente genérico)
								(Este campo também deve ser preenchido no caso de guias que titulam a transferência de bens do próprio remetent) */
							CustomerID = case when wd.entity!= 'CL' then null 
											else case 
													when wd.no=200 and (wd.ncont!='999999990' or wd.nome!=@nomeClienteDuzentos or wd.morada!='' or wd.telefone!='')
													then left(convert(varchar,wd.no) + '-A' + convert(varchar,wd.ndoc) + '/' + convert(varchar,wd.fno) + '/' + convert(varchar,wd.estab),30)
													else left(convert(varchar,wd.no) + '/' + convert(varchar,wd.estab),30)
													end
											end,
							/* 
								Subtabela Line (4.2.3.21)
							*/
							(select
								/* Mesma ordem do documento original */
								LineNumber = ROW_NUMBER() over (order by wdi.lordem asc),
								/* Subtabela OrderReferences (opcional): 
									origem de cada linha nas tabelas de Facturação de todos os documentos excepto Nt/Crédito!
									(pode ser gerada qts vezes quanto necessário) 
									(visto que no tipo de documentos incluidos nesta secção do SAF-T (guias transporte e encomendas) 
									a probablidade/relevância de estes serem criados a partir de outro documento, ou seja, terem um doc origem,
									ser muito baixa, optamos por não incluir esta subtabela */
										/* OriginatingON (opcional) */
										/* OrderDate (opcional) */
								/* Identificador do produto ou serviço */
								ProductCode = case
												when wdi.ref='' then case 
																		when wdi.oref='' then '9999999' 
																		else ltrim(rtrim(wdi.oref))
																		end	
												else ltrim(rtrim(left(wdi.ref,30)))
												end,
								/* Descrição do produto ou serviço */
								ProductDescription = case 
														when isnull(wdi.design,'')='' then 'Desconhecido'
														else LEFT(
																replace(replace(replace(replace(replace(replace(replace(replace(wdi.design
																	,CHAR(39),''),CHAR(34),''),CHAR(38),''),CHAR(14),''),CHAR(2),'')
																	,CHAR(8),''),'<',''),'>','')
															,200)
														end,
								/**/
								Quantity = convert(int,wdi.qtt),
								/**/
								UnitOfMeasure = case
													when wdi.unidade='' then 'UN'								
													else left(upper(convert(varchar,wdi.unidade)),20)
													end,
								/**/
								UnitPrice = case
												when (wdi.etiliquido / (wdi.iva/100+1)) > 0
													then case 
															when wdi.ivaincl=1 then abs((wdi.etiliquido / (wdi.iva/100+1)) / wdi.qtt) 
															else abs(wdi.etiliquido/wdi.qtt)
															end
												--else 0
												else case 
															when wdi.ivaincl=1 then abs((wdi.etiliquido / (wdi.iva/100+1)) / wdi.qtt)
															else abs(wdi.etiliquido/wdi.qtt)
															end
												end,
								/* valor tributável unitário */
								/* TaxBase */
								/* Data de envio da mercadoria ou prestação do serviço */
								--TaxPointDate = convert(varchar,convert(date,wd.datatransporte)) + 'T' + CONVERT(varchar,case when wd.horaentrega='' then '00:00:00' else wd.horaentrega+':00' end),
								TaxPointDate = convert(date,wd.datatransporte),
								/* Referências a faturas */
								/* References */
									/* Reference */
									/* Reason */
								/* Description */
								[Description] = case 
													when isnull(wdi.design,'')='' then 'Desconhecido'
													else LEFT(
															replace(replace(replace(replace(replace(replace(replace(replace(wdi.design
																,CHAR(39),''),CHAR(34),''),CHAR(38),''),CHAR(14),''),CHAR(2),'')
																,CHAR(8),''),'<',''),'>','')
														,200)
													end,
								/* ProductSerialNumber (opcional) */
									/* SerialNumber */
								/* Valor da linha sem imposto dos documentos a lançar a débito */
								DebitAmount = case
												when wdi.entradaSock = 1
													then (case when wdi.ivaincl = 1 then abs((wdi.etiliquido / (wdi.iva/100+1))) else abs(wdi.etiliquido) end)
												else null
												end,
								/* Valor da linha sem imposto dos documentos a lançar a crédito */
								CreditAmount = case
												when entradaSock = 0
													then (case when wdi.ivaincl = 1 then abs((wdi.etiliquido / (wdi.iva/100+1))) else abs(wdi.etiliquido) end)
												else null
												end,
								/*
									Subtabela Tax: o subquery com TOP 1 deve-se ao facto de que há vários REGIVAs para o código 0
								*/
								(select
									/* Código do tipo de imposto */
									TaxType = left('IVA',3),
									/* País ou região do imposto */
									TaxCountryRegion = left('PT',5), /* deveria ser obtido da tabela regIVA */
									/**/
									TaxCode = case when convert(int,wdi.iva) = 0 then 'OUT' else (select top 1 left(codigo,10) from regiva where tabiva=wdi.tabiva) end,
									--(select top 1 left(codigo,10) from regiva where tabiva=wdi.tabiva),
									/**/
									TaxPercentage = convert(int,wdi.iva)
									/* TaxAmount  (Obrigatório, no caso de se tratar de uma verba fixa
										unitária de imposto de selo. Este valor, multiplicado pela quantidade
										(Quantity) concorre para o valor de imposto a pagar (TaxPayable) */
								for xml path ('Tax'), type),
								/* Código motivo de isenção de imposto (obrigatório quando isento de iva) */
								TaxExemptionReason =
									case
										when wdi.iva=0 then
												--case
												--	when wdi.motiseimp != '' then left(wdi.motiseimp,60)
												--	when (Select pais FROM b_utentes (nolock) WHERE b_utentes.no = wdi.no and b_utentes.estab = wdi.estab) = 3
												--		then left('Isento Artigo 14.º do CIVA (ou similar)',60)
												--	when (Select pais FROM b_utentes (nolock) WHERE b_utentes.no = wdi.no and b_utentes.estab = wdi.estab) = 2
												--		then left('Isento Artigo 14.º do RITI (ou similar).',60)
												--	else 
												left('IVA - Não confere direito à dedução',60)
												--	end
										else null
										end,
								/* TaxExemptionCode 
								TODO: Tem de ser melhorado para suportar os códigos de forma mais dinamica */
								TaxExemptionCode =
									case
										when wdi.iva=0 then
												--case
												--	when (Select pais FROM b_utentes (nolock) WHERE b_utentes.no = wdi.no and b_utentes.estab = wdi.estab) = 3
												--		then 'M05'
												--	when (Select pais FROM b_utentes (nolock) WHERE b_utentes.no = wdi.no and b_utentes.estab = wdi.estab) = 2
												--		then 'M05'
												--	else 
												'M10'
													--else (select top 1 code_motive from b_multidata where b_multidata.tipo = 'motiseimp' and b_multidata.campo=wdi.motiseimp)
												--	end
										else null
										end,
								--TaxExemptionCode =
								--	case
								--		when wdi.iva=0 then
								--				case
								--					when wdi.motiseimp != '' then  (SELECT top 1 code_motive FROM b_multidata (nolock) WHERE  tipo='motiseimp' and campo=LTRIM(RTRIM(wdi.motiseimp))) 
								--					when (Select pais FROM b_utentes (nolock) WHERE b_utentes.no = wdi.no and b_utentes.estab = wdi.estab) = 3
								--						then 'M05'
								--					when (Select pais FROM b_utentes (nolock) WHERE b_utentes.no = wdi.no and b_utentes.estab = wdi.estab) = 2
								--						then 'M05'
								--					else 'M07'
								--					end
								--		else null
								--end,
								/* Deve refletir todos os descontos concedidos */
								SettlementAmount = case 
														when (convert(numeric,wdi.desconto)) > 0 or (convert(numeric,wdi.desc2)) > 0 or (convert(numeric,wdi.descval)) > 0
															then abs(
																	abs(case
																			when wdi.ivaincl = 1 then (wdi.epv * wdi.qtt) / (wdi.iva/100+1)
																			else wdi.epv * wdi.qtt
																			end)
																	-
																	abs(case
																			when wdi.ivaincl = 1 then wdi.etiliquido / (wdi.iva/100+1)
																			else wdi.etiliquido / wdi.qtt
																	end))
														else null
													end
								/* CustomsInformation - Informação aduaneira (opcional) */
									/* ARCNo - Código de referência administrativo (opcional) (pode ser gerado tantas vezes qt necessárias) */
									/* IECAmount - Montante do imposto especial de consumo da linha */
							/* Final da subtabela Line */
							from
								#t_workingDocs wdi
							where
								wdi.docstamp = wd.docstamp
								and (
									(wdi.qtt <> 0 or wdi.etiliquido <> 0) OR (wd.anulado = 1) 
								)
							order by 
								wdi.lordem
							for xml path ('Line'), type),
							/* Subtabela DocumentTotals */
							(select
								/* Valor do imposto a pagar */
								TaxPayable = ltrim(str(abs(wd.ettiva),10,2)),
								/* Total do documento sem impostos */
								NetTotal = ltrim(str(abs(wd.ettiliq),10,2)),
								/* Total do documento com impostos */
								GrossTotal = ltrim(str(abs(wd.etotal /*+mog.efinv*/),10,2)),
								/* Subtabela Currrency, apenas exportada quando a moeda de um documento não é EURO */
								(select 
									case 
										when wd.moeda not in ('PTE ou EURO','EURO','EUR', '') then 
											(select
												/* Código de moeda */
												CurrencyCode = case when wd.moeda='' then left('EUR',3) else left(wd.moeda,3) end,
												/* Valor total em moeda estrangeira */
												CurrencyAmount = str(abs(wd.etotal),10,2)
												/* ExchangeRate - Taxa de câmbio 
													(este campo é obrigatório, mas não existe no software,
													se for emitida alguma fatura em moeda estrangeira é necessário preencher este campo) */
											for xml path ('Currency'), type)
										else null
									end) 
							for xml path ('DocumentTotals'),type )
						from
							#t_workingDocs wd
						group by
							wd.docstamp, wd.ndoc,
							wd.no, wd.estab, wd.nome, wd.ncont, wd.morada, wd.telefone,
							wd.fno, wd.docdata,
							wd.anulado, wd.vendnm, wd.ousrdata, wd.ousrhora,
							wd.invoiceType, wd.hash,
							wd.entity, wd.datatransporte, wd.horaentrega,
							wd.motiseimp, wd.moeda,
							wd.ettiva, wd.ettiliq, wd.etotal, wd.efinv
						order by
							wd.ndoc, wd.fno
						for xml path ('WorkDocument'), type)
					for xml path ('WorkingDocuments'), type) end,
					/********************************/
					/*
						4.4. Tabela Payments
					*/
					case when not exists (select * from #t_Payments) then null else 
					(select
						/* 4.4.1 Número de registos de documentos comerciais */
						NumberOfEntries = (select isnull(Count(distinct restamp),0) from #t_Payments),
						/* 4.4.2 Total dos débitos */
						TotalDebit = (
							select 
								TotalLinha = STR(round(abs(sum(
												tpay.edebciva
											)),2),10,2)
							from
								#t_Payments tpay
							--where
							--	tpay.cm < 50
						),
						/* 4.4.3 Total dos Créditos */
						TotalCredit = (
							select 
								TotalLinha = STR(round(abs(sum(
												tpay.ecredciva
											)),2),10,2)
							from
								#t_Payments tpay
							--where
							--	tpay.cm > 50
						),
						/* 4.4.4 Subtabela Payment */
						(select
							/*  4.4.4.1 Identificação única do recibo (PaymentRefNo) 
								Esta identificação é composta sequencialmente pelosseguintes elementos: o código interno do tipo de reciboatribuído pela aplicação, umespaço, o identificador da sériedo recibo, uma barra (/) e onúmero sequencial desserecibo dentro dessa série. 
								Não podem existir registos coma mesma identificação. 
								Não pode ser utilizado o mesmocódigo interno de tipo dedocumento em diferentes tiposde documentos.  */
							PaymentRefNo = cast(tsrendoc as varchar(2)) + ' ' + tsrecodsaft + '/' + cast(rno as varchar(10))
							/* 4.4.4.2 Documento (ATCUD)
								Código Único do Documento. O campo deve ser preenchido com «0» (zero) até à sua regulamentação.  */
							, ATCUD = '0'
							/* 4.4.4.3 Período contabilístico (Period)
								Deve ser indicado o mês do período de tributação de “1” a “12”, contado desde a data do seu início. */
							, Period = month(rdata)
							/* 4.4.4.4 Identificador da transação (TransactionID) 
							O preenchimento é obrigatório, no caso de se tratar de um sistema integrado de contabilidade e faturação, ainda que o tipo de ficheiro
							(TaxAccountingBasis) não deva conter as tabelas relativas à contabilidade. Deve ser indicada a chave única da tabela 3. Movimentos
							contabilísticos (GeneralLedgerEntries) da transação onde foi lançado este documento, respeitando a regra aí definida para o campo
							3.4.3.1 - Chave única do movimento contabilístico (TransactionID). */ 
							/* 4.4.4.5 Data do recibo (TransactionDate) */ 
							, TransactionDate = convert(date,rdata)
							/* 4.4.4.6 Tipo de recibo (PaymentType) 
								Deve ser preenchido com:
								“RC” – Recibo emitido no âmbito do regime de IVA de Caixa (incluindo os relativos a adiantamentos desse regime); 
								“RG” – Outros recibos emitidos.*/ 
							, PaymentType = 'RG'
							/* 4.4.4.7 Descrição do pagamento (Description) - OPCIONAL	 */ 
							/* 4.4.4.8 Numero gerado pela aplicação (SystemID)  - OPCIONAL
								Número único do recibo gerado internamente pela aplicação. 	 */
							/* 4.4.4.9 Situação do documento (DocumentStatus) */
							,( select 
									/* 4.4.4.9.1. Estado atual do recibo (PaymentStatus) 
									Deve ser preenchido com: 
									“N” – Recibo normal e vigente;
									“A” – Recibo anulado. */ 
									PaymentStatus = 'N'
									/* 4.4.4.9.2. Data e hora do estado atual do recibo (PaymentStatusDate)
									Data da última gravação do estado do recibo ao segundo. Tipo data e hora: “AAAA-MM-DDThh:mm:ss”.  */
									, PaymentStatusDate = convert(varchar,convert(date,ousrdata)) + 'T' + CONVERT(varchar,case when ousrhora='' then '00:00:00' else ousrhora end)
									/* 4.4.4.9.3. Motivo da alteração de estado do recibo (Reason) - OPCIONAL 
									Deve ser indicada a razão que levou à alteração de estado do recibo. */ 
									/* 4.4.4.9.4. Código do utilizador (SourceID)
									Utilizador responsável pelo estado atual do recibo. */
									, SourceID = ousrinis
									/* 4.4.4.9.5. Origem do documento (SourcePayment) 
									Deve ser preenchido com: 
									“P” – Recibo produzido na aplicação;
									“I” – Recibo integrado e produzido noutra aplicação;
									“M” – Recibo proveniente de recuperação ou de emissão manual. */
									,SourcePayment = 'P'
							for xml path ('DocumentStatus'), type)
							/* 4.4.4.10. Forma de Pagamento (PaymentMethod)
							Deve ser indicado o meio de pagamento utilizado. No caso de pagamentos mistos devem ser indicados os montantes por tipo de meio e data de
							pagamento.
							Existindo a necessidade de efetuar mais do que uma referência, esta estrutura poderá ser gerada tantas vezes quantas as necessárias.  */
							/* Meios de pagamento (PaymentMechanism)
							Deve ser preenchido com: 
							“CC” – Cartão crédito; “CD” – Cartão débito; “CH” – Cheque bancário; “CI” – Crédito documentário internacional; “CO” – Cheque ou cartão oferta;
							“CS” – Compensação de saldos em conta corrente; “DE” – Dinheiro eletrónico, por exemplo residente em cartões de fidelidade ou de pontos; 
							“LC” – Letra comercial; “MB” – Referências de pagamento para Multibanco; “NU” – Numerário; “OU” – Outros meios aqui não assinalados; 
							“PR” – Permuta de bens; TB“ – Transferência bancária ou débito direto autorizado; “TR” – títulos de compensação  extrassalarial independentemente do seu
							suporte, por exemplo, títulos de refeição, educação, etc. */
							--,(select case when numerario <> 0
							--	then
							--		(select
							--			/* 4.4.4.10.1. Meios de pagamento (PaymentMechanism) */ 
							--			PaymentMechanism = 'NU'
							--			/* 4.4.4.10.2. Montante do pagamento (PaymentAmount) */
							--			, PaymentAmount = str(round(numerario,2),10,2)
							--			/* 4.4.4.10.3. Data do pagamento (PaymentDate) */
							--			, PaymentDate = convert(date,rdata)
							--		for xml path ('PaymentMethod'), type)
							--	else null 
							--	end)
							--,(select case when cartaodebito <> 0
							--	then
							--		(select
							--			/* 4.4.4.10.1. Meios de pagamento (PaymentMechanism) */ 
							--			PaymentMechanism = 'CD'
							--			/* 4.4.4.10.2. Montante do pagamento (PaymentAmount) */
							--			, PaymentAmount = str(round(cartaodebito,2),10,2)
							--			/* 4.4.4.10.3. Data do pagamento (PaymentDate) */
							--			, PaymentDate = convert(date,rdata)
							--		for xml path ('PaymentMethod'), type)
							--	else null 
							--	end)
							--,(select case when cartaocredito <> 0
							--	then
							--		(select
							--			/* 4.4.4.10.1. Meios de pagamento (PaymentMechanism) */ 
							--			PaymentMechanism = 'CC'
							--			/* 4.4.4.10.2. Montante do pagamento (PaymentAmount) */
							--			, PaymentAmount = str(round(cartaocredito,2),10,2)
							--			/* 4.4.4.10.3. Data do pagamento (PaymentDate) */
							--			, PaymentDate = convert(date,rdata)
							--		for xml path ('PaymentMethod'), type)
							--	else null 
							--	end)
							--,(select case when cheque <> 0
							--	then
							--		(select
							--			/* 4.4.4.10.1. Meios de pagamento (PaymentMechanism) */ 
							--			PaymentMechanism = 'CH'
							--			/* 4.4.4.10.2. Montante do pagamento (PaymentAmount) */
							--			, PaymentAmount = str(round(cheque,2),10,2)
							--			/* 4.4.4.10.3. Data do pagamento (PaymentDate) */
							--			, PaymentDate = convert(date,rdata)
							--		for xml path ('PaymentMethod'), type)
							--	else null 
							--	end)
							--,(select case when vales <> 0
							--	then
							--		(select
							--			/* 4.4.4.10.1. Meios de pagamento (PaymentMechanism) */ 
							--			PaymentMechanism = 'CO'
							--			/* 4.4.4.10.2. Montante do pagamento (PaymentAmount) */
							--			, PaymentAmount = str(round(vales,2),10,2)
							--			/* 4.4.4.10.3. Data do pagamento (PaymentDate) */
							--			, PaymentDate = convert(date,rdata)
							--		for xml path ('PaymentMethod'), type)
							--	else null 
							--	end)
							--,(select case when adiantamento <> 0
							--	then
							--		(select
							--			/* 4.4.4.10.1. Meios de pagamento (PaymentMechanism) */ 
							--			PaymentMechanism = 'CS'
							--			/* 4.4.4.10.2. Montante do pagamento (PaymentAmount) */
							--			, PaymentAmount = str(round(adiantamento,2),10,2)
							--			/* 4.4.4.10.3. Data do pagamento (PaymentDate) */
							--			, PaymentDate = convert(date,rdata)
							--		for xml path ('PaymentMethod'), type)
							--	else null 
							--	end)
							--,(select case when transfbanc <> 0
							--	then
							--		(select
							--			/* 4.4.4.10.1. Meios de pagamento (PaymentMechanism) */ 
							--			PaymentMechanism = 'TB'
							--			/* 4.4.4.10.2. Montante do pagamento (PaymentAmount) */
							--			, PaymentAmount = str(round(transfbanc,2),10,2)
							--			/* 4.4.4.10.3. Data do pagamento (PaymentDate) */
							--			, PaymentDate = convert(date,rdata)
							--		for xml path ('PaymentMethod'), type)
							--	else null 
							--	end)
							--,(select case when numerario + cartaodebito + cartaocredito + cheque + vales + adiantamento + transfbanc  <> etotal
							--	then
							--		(select
							--			/* 4.4.4.10.1. Meios de pagamento (PaymentMechanism) */ 
							--			PaymentMechanism = 'OU'
							--			/* 4.4.4.10.2. Montante do pagamento (PaymentAmount) */
							--			, PaymentAmount = str(round(etotal-(numerario + cartaodebito + cartaocredito + cheque + vales + adiantamento + transfbanc),2),10,2)
							--			/* 4.4.4.10.3. Data do pagamento (PaymentDate) */
							--			, PaymentDate = convert(date,rdata)
							--		for xml path ('PaymentMethod'), type)
							--	else null 
							--	end)
							/* 4.4.4.11. Código do utilizador (SourceID) 
							Utilizador que gerou o documento. */
							, SourceID = ousrinis
							/* 4.4.4.12. Data de gravação do recibo (SystemEntryDate)
							Data da gravação do registo ao segundo, Tipo data e hora: “AAAA-MM-DDThh:mm:ss”. */
							, SystemEntryDate = convert(varchar,convert(date,ousrdata)) + 'T' + CONVERT(varchar,case when ousrhora='' then '00:00:00' else ousrhora end)	
							/* 4.4.4.13. Identificador do cliente (CustomerID)
							Chave única da tabela 2.2. – Tabela de clientes (Customer) respeitando a regra aí definida para o campo 2.2.1. – Identificador único do cliente (CustomerID). */										
							--,CustomerID = left(convert(varchar,no) + '/' + CONVERT(varchar,estab),30)
							,CustomerID = case 
											when no=200 and (ncont!='999999990' or nome!=@nomeClienteDuzentos or morada!='' or telefone!='')
											then left(convert(varchar,no) + '-A' + convert(varchar,tsrendoc) + '/' + convert(varchar,rno) + '/' + convert(varchar,estab),30)
											else left(convert(varchar,no) + '/' + convert(varchar,estab),30)
										end
							--, CustomerID = case 
							--					when no=200 and (ncont!='999999990' or nome!=@nomeClienteDuzentos or morada!='' or telefone!='') 
							--					then left(convert(varchar,no) + '-A' + convert(varchar,tsrendoc) + '/' + convert(varchar,rno) + '/' + convert(varchar,estab),30)
							--					else left(convert(varchar,no) + '/' + convert(varchar,estab),30)
							--					end
							/* 4.4.4.14. Linha (Line) */
							,(select
									/* 4.4.4.14.1. Número de linha (LineNumber) */
									LineNumber = pli.lordem
									/* 4.4.4.14.2. Referência ao documento de origem (SourceDocumentID) 
									Existindo a necessidade de efetuar mais do que uma referência, esta estrutura poderá ser gerada tantas vezes quantas as necessárias. 
									No caso da aplicação ser integrada deve ser utilizada a estrutura de numeração do campo de origem. */
									,(select
										/* 4.4.4.14.2.1. Número do documento de origem (OriginatingON)
										Deve ser indicado o tipo, a série e o número da fatura ou documento retificativo desta a que respeita o pagamento. 
										Se o documento referido estiver contido no SAF-T(PT) deve ser utilizada a estrutura de numeração do campo 4.1.4.1 –Identificação única do 
										documento de venda (InvoiceNo) da Tabela 4.1. – Documentos comerciais a clientes (SalesInvoices).  */
										OriginatingON = pli.InvoiceNo
										/* Data do documento de origem (InvoiceDate)
										Deve ser indicada a data da fatura ou documento retificativo desta a que se refere o pagamento.   */
										, InvoiceDate = convert(date,datalc)
										for xml path ('SourceDocumentID'), type)
									/* 4.4.4.14.4. Valor a débito (DebitAmount) 
									Valor do recibo do documento retificativo, sem impostos e eventuais descontos. */
									--, DebitAmount = case when edeb>0 then str(round(edeb,2),10,2) else null end
									, DebitAmount = case when edeb>0 then str(round(edeb,2),10,2) else (case when ecred=0 then cast(0.00 as numeric(10,2)) else null end) end
									--/* 4.4.4.14.5. Valor a crédito (CreditAmount)
									--Valor do recibo da fatura ou documento retificativo, sem impostos e eventuais descontos. */	
									, CreditAmount = case when ecred>0 then str(round(ecred,2),10,2) else null end
									--/* 4.4.4.14.7. Motivo da isenção de imposto (TaxExemptionReason) 
									--O preenchimento é obrigatório, quando os campos 4.4.4.14.6.4. - Percentagem da taxa de imposto (TaxPercentage) ou 4.4.4.14.6.5. - Montante do
									--imposto (TaxAmount) são iguais a zero. Deve ser referido o preceito legal aplicável. Este campo deve ser igualmente preenchido nos casos de não sujeição aos
									--impostos referidos na tabela 2.5. – Tabela de impostos (TaxTable). */
									--,TaxExemptionReason = case when code_motive='' 
									--						then motiseimp
									--						else null
									--						end
									--/* 4.4.4.14.8. Código do motivo de isenção de imposto (TaxExemptionCode) 
									--Deve ser preenchido com o código do motivo de isenção ou não liquidação, que consta do Manual de Integração de Software – Comunicação das Faturas à AT. 
									--O preenchimento é obrigatório, quando os campos 4.4.4.14.6.4. - Percentagem da taxa de imposto (TaxPercentage) ou 4.4.4.14.6.5. - Montante do imposto (TaxAmount) são iguais
									--a zero. Este campo deve ser igualmente preenchido nos casos de não sujeição aos impostos referidos na tabela 2.5. – Tabela de impostos (TaxTable). */
									--,TaxExemptionCode = case when code_motive='' 
									--						then code_motive
									--						else null
									--						end
								from
									#t_Payments pli
								where
									p.restamp = pli.restamp	
								order by 
									pli.lordem
								for xml path ('Line'), type)
							/* 4.4.4.15. Totais do documento (DocumentTotals) */
							,(select
								/* 4.4.4.15.1. Valor do imposto a pagar (TaxPayable) */
								TaxPayable = case when (select 
													(round(abs(sum(tpay.eivav1+tpay.eivav2+tpay.eivav3+tpay.eivav4+tpay.eivav5+tpay.eivav6+tpay.eivav7+tpay.eivav8+tpay.eivav9)),2))
												from
													#t_Payments tpay
												where
													tpay.restamp = p.restamp ) > 0
											then
												(select 
													STR(round(abs(sum(tpay.eivav1+tpay.eivav2+tpay.eivav3+tpay.eivav4+tpay.eivav5+tpay.eivav6+tpay.eivav7+tpay.eivav8+tpay.eivav9)),2),10,2)
												from
													#t_Payments tpay
												where
													tpay.restamp = p.restamp )
											else
												(select 
													STR(round(abs(sum(tpay.eivav1+tpay.eivav2+tpay.eivav3+tpay.eivav4+tpay.eivav5+tpay.eivav6+tpay.eivav7+tpay.eivav8+tpay.eivav9)),2)*(-1) ,10,2)
												from
													#t_Payments tpay
												where
													tpay.restamp = p.restamp )
											end 
								/* 4.4.4.15.2. Total do documento sem impostos (NetTotal) */
								,NetTotal = case when (round(etotal - (select 
													abs(sum(tpay.eivav1+tpay.eivav2+tpay.eivav3+tpay.eivav4+tpay.eivav5+tpay.eivav6+tpay.eivav7+tpay.eivav8+tpay.eivav9))
												from
													#t_Payments tpay
												where
													tpay.restamp = p.restamp ),2))>0
											then
												STR(round(etotal - (select 
													abs(sum(tpay.eivav1+tpay.eivav2+tpay.eivav3+tpay.eivav4+tpay.eivav5+tpay.eivav6+tpay.eivav7+tpay.eivav8+tpay.eivav9))
												from
													#t_Payments tpay
												where
													tpay.restamp = p.restamp ),2),10,2)
											else
												STR(round(etotal - (select 
													abs(sum(tpay.eivav1+tpay.eivav2+tpay.eivav3+tpay.eivav4+tpay.eivav5+tpay.eivav6+tpay.eivav7+tpay.eivav8+tpay.eivav9))
												from
													#t_Payments tpay
												where
													tpay.restamp = p.restamp ),2)*(-1),10,2)
											end 
								/* 4.4.4.15.3. Total do documento com impostos (GrossTotal)
								Este campo não deve refletir eventuais retenções na fonte constantes na estrutura 4.4.4.16. – Retenção na fonte (WithholdingTax) */
								,GrossTotal = case when round(etotal,2)>0
												then
													STR(round(etotal,2),10,2)
												else
													STR(round(etotal,2)*(-1),10,2)
												end
							for xml path ('DocumentTotals'), type)
							--/* 4.4.4.15.4.  Acordos (Settlement) 
							--Acordos ou formas de pagamento. */
							--,(select
							--	/* 4.4.4.15.4.1. Montante do desconto (SettlementAmount) 
							--	Total dos descontos concedidos aquando deste pagamento. */
							--	SettlementAmount = STR(round(efinv,2),10,2)
							--for xml path ('Settlement'), type)
							/* 4.4.4.16. Retenção na fonte (WithholdingTax) */
							--,(select
							--	/* 4.4.4.16.3.  Montante da retenção na fonte (WithholdingTaxAmount) 
							--	Deve ser indicado o montante retido de imposto.  */
							--	WithholdingTaxAmount = STR(round(0.00,2),10,2)
							--for xml path ('WithholdingTax'), type)
						from
							#t_Payments p
						group by
							p.tsrendoc, p.tsrecodsaft, p.rdata, p.rno, p.ousrdata, p.ousrhora, p.ousrinis, numerario ,cartaodebito ,cartaocredito ,cheque ,vales ,adiantamento ,transfbanc, etotal, no, estab, ncont, nome, morada, telefone, restamp, efinv
						order by
							p.rno
						for xml path ('Payment'), type)
					 for xml path ('Payments'), type) end
					/********************************/
					--exec up_gen_saft_1_04 '20180801','20180830','Loja 1'
				for xml path ('SourceDocuments'), type)
			for xml path (''), root ('AuditFile'), type
		)),'<AuditFile>','<?xml version = "1.0" encoding="Windows-1252" standalone="yes"?>'+ CHAR(13)+CHAR(10) +'<AuditFile xmlns="urn:OECD:StandardAuditFile-Tax:PT_1.04_01" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">')) as saft
	end try
	begin catch
		declare
			@errortext varchar(256),
			@error_severity int,
			@error_state int
		select
			@errortext = isnull(ERROR_PROCEDURE(),'') + ': ' + ERROR_MESSAGE(),
			@error_severity = ERROR_SEVERITY(),
			@error_state = ERROR_state()
		raiserror(@errortext, @error_severity, @error_state)
		return(1)
	end catch
	If OBJECT_ID('tempdb.dbo.#t_salesInvoices') IS NOT NULL
		drop table #t_salesInvoices;
	If OBJECT_ID('tempdb.dbo.#t_movOfGoods') IS NOT NULL
		drop table #t_movOfGoods;
	If OBJECT_ID('tempdb.dbo.#t_workingDocs') IS NOT NULL
		drop table #t_workingDocs;
	If OBJECT_ID('tempdb.dbo.#t_workingDocs') IS NOT NULL
		drop table #t_Payments;
	If OBJECT_ID('tempdb.dbo.#t_saft_cl') IS NOT NULL
		drop table #t_saft_cl;
	If OBJECT_ID('tempdb.dbo.#t_saft_st') IS NOT NULL
		drop table #t_saft_st;
	If OBJECT_ID('tempdb.dbo.#modopag') IS NOT NULL
		drop table #modopag;
end

GO


