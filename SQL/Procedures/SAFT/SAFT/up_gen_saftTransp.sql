-- exec up_gen_saftTransp '20100101','20150101', 'Loja 1'
if OBJECT_ID('[dbo].[up_gen_saftTransp]') IS NOT NULL
		drop procedure dbo.up_gen_saftTransp
	go

	create procedure [dbo].[up_gen_saftTransp] 
		@dataini		date, 
		@datafim		date,
		@Loja			varchar(20)
	/* with encryption */
	as 
	begin

		/*
			Stored Procedure para gerar o ficheiro SAFTPT em formato validado pela DGCI - Documentos de Transporte
			Autor: 
		*/

		begin try
			
			set nocount on																	  
			set xact_abort on
			set language portuguese
			
			/* Criar variavel com nome do cliente 200*/
			declare @nomeClienteDuzentos varchar(30)
			set @nomeClienteDuzentos = 'CONSUMIDOR FINAL'
			
			
			/* Converter os valores de data enviados pelo PHC para outro formato */
			select
				@dataini=convert(varchar,convert(datetime,@dataini),23),
				@datafim=convert(varchar,convert(datetime,@datafim),23)
				
			/* Parte 1 do SELECT principal, adiciona o string do encoding XML e converte o restante texto de XML para varchar(MAX) */
			select convert(text,replace(convert(varchar(max),(
				select 
					
					/* Tabela Header: dados da Ficha da Empresa e vers�o SAFT/PHC */
					(select top 1 
						AuditFileVersion =																	left('1.02_01',10),
						CompanyID = 																		case 
																												when consreg<>'' then left(ltrim(rtrim(CONVERT(varchar,consreg))),39) + ' ' + left(ltrim(rtrim(CONVERT(varchar,ncont))),10)
																												else left(ltrim(rtrim(CONVERT(varchar,ncont))),50)
																											end,
						TaxRegistrationNumber =																left(ncont,9),
						TaxAccountingBasis = 																left('F',1)	, /* Fatura��o incluindo os documentos de transporte e os de confer�ncia */
						CompanyName =																		left(nomecomp,100),
						BusinessName =																		case when nomabrv='' then left(nomecomp,60) else left(nomabrv,60) end,
						
						/* Subtabela CompanyAddress*/												
						(select
							AddressDetail =																	case 
																												/* os REPLACES cortam muito espa�o no ficheiro e removem os CHARs invalidos nas specs do XML*/
																												when morada like '%'+char(39)+'%' then left(ltrim(rtrim(replace(morada,CHAR(39),''))),100)
																												when morada like '%'+char(34)+'%' then left(ltrim(rtrim(replace(morada,CHAR(34),''))),100)
																												when morada like '%'+char(38)+'%' then left(ltrim(rtrim(replace(morada,CHAR(38),''))),100)
																												when morada like '%'+char(14)+'%' then left(ltrim(rtrim(replace(morada,CHAR(14),''))),100)
																												when morada like '%'+char(2)+'%' then left(ltrim(rtrim(replace(morada,CHAR(2),''))),100)
																												when morada like '%'+char(8)+'%' then left(ltrim(rtrim(replace(morada,CHAR(8),''))),100)
																												when morada like '%<%' then left(ltrim(rtrim(replace(morada,'<',''))),100)
																												when morada like '%>%' then left(ltrim(rtrim(replace(morada,'>',''))),100)
																												when morada is null or morada='' then 'Desconhecido'
																												else left(ltrim(rtrim(convert(varchar,morada))),100)
																											end,
							City =																			case when local='' then 'Desconhecido' else left(local,50) end,						
							PostalCode	=																	case when codpost='' then '0000-000' else case when ISNUMERIC(left(codpost,4)) = 1 and ISNUMERIC(right(left(codpost,8),3)) = 1 then left(codpost,8) else '0000-000' end end,
							Region =																		case when distrito='' then null else left(distrito,50) end,											
							Country=																		left('PT',2)
						for xml path ('CompanyAddress'), type
						),
						
						FiscalYear =																		convert(varchar,DATEPART(year,@dataini)),
						StartDate =																			@dataini	,				 
						EndDate =																			@datafim,				 
						CurrencyCode =																		left('EUR',3),
						DateCreated =																		convert(date,GETDATE()),
						TaxEntity =																			(select top 1 left(TaxEntity,20) from empresa (nolock) where empresa.site=@Loja),
						ProductCompanyTaxID =																left('508935490',20), /* deveria ser par�metro */
						SoftwareCertificateNumber =															(select top 1 left(textValue,4) from B_Parameters (nolock) where stamp='ADM0000000078'),
						ProductID =																			left('Logitools/Logitools',255),
						ProductVersion =																	case 
																												when (select top 1 ltrim(rtrim(textValue)) from b_parameters (nolock) where stamp='ADM0000000156') = '' or (select top 1 ltrim(rtrim(textValue)) from b_parameters (nolock) where stamp='ADM0000000156') is null then left('11.0.0',30)
																												else left((select top 1 ltrim(rtrim(textValue)) from b_parameters (nolock) where stamp='ADM0000000156'),30)
																											end,
						Telephone =																			left(case when telefone='' then null else convert(varchar,telefone) end,20),
						Fax =																				left(case when fax='' then null else fax end,20),
						Email =																				case when email='' then null else left(email,60) end 
					from empresa (nolock)
					where empresa.estab=(select top 1 e1 from empresa (nolock) where empresa.site=case when @Loja = 'TODAS' then empresa.site else @Loja end order by site)
					for xml path ('Header'), type
					),
					
					/* Subtabela MasterFiles */
					(select
						
						/* GeneralLedger: Dados do Plano de Contas, desactivados para j�  - validar se � necess�rio incluir isto ou n�o...*/
						(select
							case 
								when 0=1 then 	
									(Select 
										AccountID =														conta,
										AccountDescription =											descricao,
										OpeningDebitBalance =											0,
										OpeningCreditBalance =											0
									 from pc (nolock) 
									 where 
										ano=convert(varchar,DATEPART(year,@dataini))
										and (conta not like '0%'  and conta not like '9%' ) 
									order by conta 
									for XML path ('GeneralLedger'), type
									)
								else null
							end
						),
					
						/* Tabela Customer: Tabela de Clientes */
						(select * from 
							(select
								CustomerID =																left(convert(varchar,b_utentes.no)+'/'+CONVERT(varchar,b_utentes.estab),30),
								AccountID =																	case when conta='' then 'Desconhecido' else left(conta,30) end,
								CustomerTaxID =																case when ncont='' then '999999990' else left(ncont,9) end,
								CompanyName = 																case 
																												when nome=@nomeClienteDuzentos then 'Consumidor final' 
																												when nome='' then 'Desconhecido' 
																												else left(replace(nome,char(39),''),100) 
																											end,
								/* Subtabela BillingAddress*/ 
								BillingAddress = (
									select
										AddressDetail =														case 
																												/* os REPLACES cortam muito espa�o no ficheiro e removem os CHARs invalidos nas specs do XML*/
																												when morada like '%'+char(39)+'%' then left(ltrim(rtrim(replace(morada,CHAR(39),''))),100)
																												when morada like '%'+char(34)+'%' then left(ltrim(rtrim(replace(morada,CHAR(34),''))),100)
																												when morada like '%'+char(38)+'%' then left(ltrim(rtrim(replace(morada,CHAR(38),''))),100)
																												when morada like '%'+char(14)+'%' then left(ltrim(rtrim(replace(morada,CHAR(14),''))),100)
																												when morada like '%'+char(2)+'%' then left(ltrim(rtrim(replace(morada,CHAR(2),''))),100)
																												when morada like '%'+char(8)+'%' then left(ltrim(rtrim(replace(morada,CHAR(8),''))),100)
																												when morada like '%<%' then left(ltrim(rtrim(replace(morada,'<',''))),100)
																												when morada like '%>%' then left(ltrim(rtrim(replace(morada,'>',''))),100)
																												when morada is null or morada='' then 'Desconhecido'
																												else left(ltrim(rtrim(convert(varchar,morada))),100)
																											end,
										City =																	case when local='' then 'Desconhecida' else left(replace(local,char(39),''),50) end,
										PostalCode =															case when codpost='' then '0000-000' else case when ISNUMERIC(left(codpost,4)) = 1 and ISNUMERIC(right(left(codpost,8),3)) = 1 then left(codpost,8) else '0000-000' end end,
										Country =																left('PT',12) /*dever� ser preeenchido com a informa��o da ficha do utente*/
									for XML path (''), type
								),
								
								/* Subtabela ShipToAdrdress*/ 
								ShipToAddress = (
									select
										AddressDetail =														case 
																												/* os REPLACES cortam muito espa�o no ficheiro e removem os CHARs invalidos nas specs do XML*/
																												when morada like '%'+char(39)+'%' then left(ltrim(rtrim(replace(morada,CHAR(39),''))),100)
																												when morada like '%'+char(34)+'%' then left(ltrim(rtrim(replace(morada,CHAR(34),''))),100)
																												when morada like '%'+char(38)+'%' then left(ltrim(rtrim(replace(morada,CHAR(38),''))),100)
																												when morada like '%'+char(14)+'%' then left(ltrim(rtrim(replace(morada,CHAR(14),''))),100)
																												when morada like '%'+char(2)+'%' then left(ltrim(rtrim(replace(morada,CHAR(2),''))),100)
																												when morada like '%'+char(8)+'%' then left(ltrim(rtrim(replace(morada,CHAR(8),''))),100)
																												when morada like '%<%' then left(ltrim(rtrim(replace(morada,'<',''))),100)
																												when morada like '%>%' then left(ltrim(rtrim(replace(morada,'>',''))),100)
																												when morada is null or morada='' then 'Desconhecido'
																												else left(ltrim(rtrim(convert(varchar,morada))),100)
																											end,
										City =																	case when local='' then 'Desconhecida' else left(replace(local,char(39),''),50) end,
										PostalCode =															case when codpost='' then '0000-000' else case when ISNUMERIC(left(codpost,4)) = 1 and ISNUMERIC(right(left(codpost,8),3)) = 1 then left(codpost,8) else '0000-000' end end,
										Country =																left('PT',12) /*dever� ser preeenchido com a informa��o da ficha do utente*/
									for XML path (''), type
								),
								
								Telephone =																	case when telefone='' then null else left(convert(varchar,telefone),20) end,						
								Fax =																		case when fax='' then null else left(convert(varchar,fax),20) end, 
								SelfBillingIndicator =														0
							from b_utentes (nolock)
							inner join (select 
											distinct no, estab 
										from 
											ft (nolock) 
										where 
											fdata between @dataini and @datafim 
											and ndoc in (select ndoc from td (nolock) where tiposaft in ('GT','GR','GA','GC','GD'))
										) a 
										on a.no = b_utentes.no and a.estab = b_utentes.estab
							
							union all
							/*Converter fornecedores em clientes no caso de Dev. a Fornecedor*/
							select
								CustomerID =																left('FL' + convert(varchar,fl.no)+'/'+CONVERT(varchar,fl.estab),30),
								AccountID =																	case when fl.conta='' then 'Desconhecido' else left(fl.conta,30) end,
								CustomerTaxID =																case when fl.ncont='' then '999999990' else left(fl.ncont,9) end,
								CompanyName = 																case 
																												when fl.nome=@nomeClienteDuzentos then 'Consumidor final' 
																												when fl.nome='' then 'Desconhecido' 
																												else left(replace(fl.nome,char(39),''),100) 
																											end,
								/* Subtabela BillingAddress*/ 
								BillingAddress = (
									select
										AddressDetail =														case 
																												/* os REPLACES cortam muito espa�o no ficheiro e removem os CHARs invalidos nas specs do XML*/
																												when fl.morada like '%'+char(39)+'%' then left(ltrim(rtrim(replace(fl.morada,CHAR(39),''))),100)
																												when fl.morada like '%'+char(34)+'%' then left(ltrim(rtrim(replace(fl.morada,CHAR(34),''))),100)
																												when fl.morada like '%'+char(38)+'%' then left(ltrim(rtrim(replace(fl.morada,CHAR(38),''))),100)
																												when fl.morada like '%'+char(14)+'%' then left(ltrim(rtrim(replace(fl.morada,CHAR(14),''))),100)
																												when fl.morada like '%'+char(2)+'%' then left(ltrim(rtrim(replace(fl.morada,CHAR(2),''))),100)
																												when fl.morada like '%'+char(8)+'%' then left(ltrim(rtrim(replace(fl.morada,CHAR(8),''))),100)
																												when fl.morada like '%<%' then left(ltrim(rtrim(replace(fl.morada,'<',''))),100)
																												when fl.morada like '%>%' then left(ltrim(rtrim(replace(fl.morada,'>',''))),100)
																												when fl.morada is null or fl.morada='' then 'Desconhecido'
																												else left(ltrim(rtrim(convert(varchar,fl.morada))),100)
																											end,
										City =																	case when fl.local='' then 'Desconhecida' else left(replace(fl.local,char(39),''),50) end,
										PostalCode =															case when fl.codpost='' then '0000-000' else case when ISNUMERIC(left(codpost,4)) = 1 and ISNUMERIC(right(left(codpost,8),3)) = 1 then left(codpost,8) else '0000-000' end end,
										Country =																left('PT',12) /*dever� ser preeenchido com a informa��o da ficha do fornecedor*/
									for XML path (''), type
								),
								
								/* Subtabela ShipToAdrdress*/ 
								ShipToAddress = (
									select
										AddressDetail =														case 
																												/* os REPLACES cortam muito espa�o no ficheiro e removem os CHARs invalidos nas specs do XML*/
																												when fl.morada like '%'+char(39)+'%' then left(ltrim(rtrim(replace(fl.morada,CHAR(39),''))),100)
																												when fl.morada like '%'+char(34)+'%' then left(ltrim(rtrim(replace(fl.morada,CHAR(34),''))),100)
																												when fl.morada like '%'+char(38)+'%' then left(ltrim(rtrim(replace(fl.morada,CHAR(38),''))),100)
																												when fl.morada like '%'+char(14)+'%' then left(ltrim(rtrim(replace(fl.morada,CHAR(14),''))),100)
																												when fl.morada like '%'+char(2)+'%' then left(ltrim(rtrim(replace(fl.morada,CHAR(2),''))),100)
																												when fl.morada like '%'+char(8)+'%' then left(ltrim(rtrim(replace(fl.morada,CHAR(8),''))),100)
																												when fl.morada like '%<%' then left(ltrim(rtrim(replace(fl.morada,'<',''))),100)
																												when fl.morada like '%>%' then left(ltrim(rtrim(replace(fl.morada,'>',''))),100)
																												when fl.morada is null or fl.morada='' then 'Desconhecido'
																												else left(ltrim(rtrim(convert(varchar,fl.morada))),100)
																											end,
										City =																	case when fl.local='' then 'Desconhecida' else left(replace(fl.local,char(39),''),50) end,
										PostalCode =															case when fl.codpost='' then '0000-000' else case when ISNUMERIC(left(codpost,4)) = 1 and ISNUMERIC(right(left(codpost,8),3)) = 1 then left(codpost,8) else '0000-000' end end,
										Country =																left('PT',12) /*dever� ser preeenchido com a informa��o da ficha do utente*/
									for XML path (''), type
								),
								
								Telephone =																	case when fl.telefone='' then null else left(convert(varchar,fl.telefone),20) end,						
								Fax =																		case when fl.fax='' then null else left(convert(varchar,fl.fax),20) end, 
								SelfBillingIndicator =														0
							from fl (nolock)
							inner join (select 
											distinct no, estab 
										from 
											bo (nolock) 
										where 
											dataobra between @dataini and @datafim 
											and ndos = 17 /*dev. a fornecedor*/
										) a 
										on a.no = fl.no and a.estab = fl.estab
							
							union all
							/* Clientes Virtuais, n�o existem na tabela de Clientes; ID � gerado de forma sequencial APENAS derivado das vendas feitas ao cliente "Venda a Dinheiro" com altera��es de dados */
							select
								CustomerID =																case 
																												/* Vendas feitas ao cliente 200 com altera��o de dados (Clientes Virtuais)*/ 
																												when ft.ncont='999999990' and (ft.nome!=@nomeClienteDuzentos or ft.morada!='' or ft.telefone!='') then left(convert(varchar,ft.no) + '-A'  + convert(varchar,ft.ndoc) + convert(varchar,ft.fno) + '/' + convert(varchar,ft.estab),30)
																												when ft.ncont!='999999990' and (ft.nome!=@nomeClienteDuzentos or ft.morada!='' or ft.telefone!='') then left(convert(varchar,ft.no) + '-A'  + convert(varchar,ft.ndoc) + convert(varchar,ft.fno) + '/' + convert(varchar,ft.estab) + '/' + CONVERT(varchar,ft.ncont) + '/' + CONVERT(varchar,ft.fno),30)

																												/* Vendas feitas ao cliente "Venda a Dinheiro" sem altera��o de dados*/
																												when ft.ncont='999999990'	then left(convert(varchar,ft.no)+'/'+convert(varchar,ft.estab),30)
																												when ft.ncont<>'999999990' then left(convert(varchar,ft.no)+'/'+convert(varchar,ft.estab) + '/' + CONVERT(varchar,ft.ncont) + '/' + CONVERT(varchar,ft.fno),30)
																											end
																											,
								AccountID =																	case when b_utentes.conta='' then 'Desconhecido' else left(b_utentes.conta,30) end,
								CustomerTaxID =																case when ft.ncont='' then '999999990' else left(ft.ncont,9) end,
								CompanyName = 																case 
																												when ft.nome=@nomeClienteDuzentos then 'Consumidor final' 
																												when ft.nome='' then 'Desconhecido'
																												else left(replace(ft.nome,char(39),''),100) 
																											end,
								/* Subtabela BillingAddress*/ 
								BillingAddress = (
									select
										AddressDetail =														/*case when ft.morada='' then 'Desconhecida' else replace(ft.morada,char(39),'') end,*/
																											case 
																												/* os REPLACES cortam muito espa�o no ficheiro e removem os CHARs invalidos nas specs do XML*/
																												when ft.morada like '%'+char(39)+'%' then left(ltrim(rtrim(replace(ft.morada,CHAR(39),''))),100)
																												when ft.morada like '%'+char(34)+'%' then left(ltrim(rtrim(replace(ft.morada,CHAR(34),''))),100)
																												when ft.morada like '%'+char(38)+'%' then left(ltrim(rtrim(replace(ft.morada,CHAR(38),''))),100)
																												when ft.morada like '%'+char(14)+'%' then left(ltrim(rtrim(replace(ft.morada,CHAR(14),''))),100)
																												when ft.morada like '%'+char(2)+'%' then left(ltrim(rtrim(replace(ft.morada,CHAR(2),''))),100)
																												when ft.morada like '%'+char(8)+'%' then left(ltrim(rtrim(replace(ft.morada,CHAR(8),''))),100)
																												when ft.morada like '%<%' then left(ltrim(rtrim(replace(ft.morada,'<',''))),100)
																												when ft.morada like '%>%' then left(ltrim(rtrim(replace(ft.morada,'>',''))),100)
																												when ft.morada is null or ft.morada='' then 'Desconhecido'
																												else left(ltrim(rtrim(convert(varchar,ft.morada))),100)
																											end,
										City =																case when ft.local='' then 'Desconhecida' else replace(ft.local,char(39),'') end,
										PostalCode =														case when ft.codpost='' then '0000-000' else case when ISNUMERIC(left(ft.codpost,4)) = 1 and ISNUMERIC(right(left(ft.codpost,8),3)) = 1 then left(ft.codpost,8) else '0000-000' end end,
										Country =															left('PT',12) /*dever� ser preeenchido com a informa��o da ficha do utente*/
									for XML path (''), type
								),
								
								/* Subtabela ShipToAddress*/ 
								ShipToAddress = (
									select
										AddressDetail =														/*case when ft.morada='' then 'Desconhecida' else replace(ft.morada,char(39),'') end,*/
																											case 
																												/* os REPLACES cortam muito espa�o no ficheiro e removem os CHARs invalidos nas specs do XML*/
																												when ft.morada like '%'+char(39)+'%' then left(ltrim(rtrim(replace(ft.morada,CHAR(39),''))),100)
																												when ft.morada like '%'+char(34)+'%' then left(ltrim(rtrim(replace(ft.morada,CHAR(34),''))),100)
																												when ft.morada like '%'+char(38)+'%' then left(ltrim(rtrim(replace(ft.morada,CHAR(38),''))),100)
																												when ft.morada like '%'+char(14)+'%' then left(ltrim(rtrim(replace(ft.morada,CHAR(14),''))),100)
																												when ft.morada like '%'+char(2)+'%' then left(ltrim(rtrim(replace(ft.morada,CHAR(2),''))),100)
																												when ft.morada like '%'+char(8)+'%' then left(ltrim(rtrim(replace(ft.morada,CHAR(8),''))),100)
																												when ft.morada like '%<%' then left(ltrim(rtrim(replace(ft.morada,'<',''))),100)
																												when ft.morada like '%>%' then left(ltrim(rtrim(replace(ft.morada,'>',''))),100)
																												when ft.morada is null or ft.morada='' then 'Desconhecido'
																												else left(ltrim(rtrim(convert(varchar,ft.morada))),100)
																											end,
										City =																case when ft.local='' then 'Desconhecida' else replace(ft.local,char(39),'') end,
										PostalCode =														case when ft.codpost='' then '0000-000' else case when ISNUMERIC(left(ft.codpost,4)) = 1 and ISNUMERIC(right(left(ft.codpost,8),3)) = 1 then left(ft.codpost,8) else '0000-000' end end,
										Country =															left('PT',12) /*dever� ser preeenchido com a informa��o da ficha do utente*/
									for XML path (''), type
								),
								
								Telephone =																	case 
																												when ft.telefone!='' and ft.telefone!=b_utentes.telefone then left(convert(varchar,ft.telefone),20)
																												when b_utentes.telefone='' then null 
																												else left(convert(varchar,b_utentes.telefone),20)
																											end,						
								Fax =																		case when fax='' then null else left(convert(varchar,fax),20) end, 
								SelfBillingIndicator =														0
							from ft (nolock)		/* Aqui os dados sao lidos da FT e nao da b_utentes propositamente*/
							inner join b_utentes (nolock) on ft.no=b_utentes.no														
							where
								ft.ftano = datepart(year,@dataini)
								and ft.no=200
								and (
									ft.nome!=@nomeClienteDuzentos		/* Campos que, quando alterados, devem gerar os novos IDs virtuais*/
									or ft.ncont!='999999990'
									or ft.morada!=''
									or ft.telefone!=''
								)
								and ft.ndoc in (select ndoc from td (nolock) where td.tiposaft in ('GT','GR','GA','GC','GD'))
							) x 
							
						for xml path ('Customer'), type																																				
						),
						
						/* Tabela Supplier: Fornecedores */
						(Select 
							SupplierID =																left(convert(varchar,fl.no)+ '/' + convert(varchar,fl.estab),30),
							AccountID =																	case when fl.conta='' then 'Desconhecido' else left(fl.conta,30) end, 
							SupplierTaxID =																case when fl.ncont='' then '999999990' else left(fl.ncont,9) end,						
							CompanyName =																case when fl.nome='' then 'Desconhecido' else left(fl.nome,100) end,
							Contact =																	case when fl.contacto='' then null else left(convert(varchar,fl.contacto),50) end,
							
							/* Subtabela BillingAddress*/ 
							(select																																										
								AddressDetail =															case 
																											/* os REPLACES cortam muito espa�o no ficheiro e removem os CHARs invalidos nas specs do XML*/
																											when fl.morada like '%'+char(39)+'%' then left(ltrim(rtrim(replace(fl.morada,CHAR(39),''))),100)
																											when fl.morada like '%'+char(34)+'%' then left(ltrim(rtrim(replace(fl.morada,CHAR(34),''))),100)
																											when fl.morada like '%'+char(38)+'%' then left(ltrim(rtrim(replace(fl.morada,CHAR(38),''))),100)
																											when fl.morada like '%'+char(14)+'%' then left(ltrim(rtrim(replace(fl.morada,CHAR(14),''))),100)
																											when fl.morada like '%'+char(2)+'%' then left(ltrim(rtrim(replace(fl.morada,CHAR(2),''))),100)
																											when fl.morada like '%'+char(8)+'%' then left(ltrim(rtrim(replace(fl.morada,CHAR(8),''))),100)
																											when fl.morada like '%<%' then left(ltrim(rtrim(replace(fl.morada,'<',''))),100)
																											when fl.morada like '%>%' then left(ltrim(rtrim(replace(fl.morada,'>',''))),100)
																											when fl.morada is null or fl.morada='' then 'Desconhecido'
																											else left(ltrim(rtrim(convert(varchar,fl.morada))),100)
																										end,
								City =																	case when fl.local='' then 'Desconhecido' else left(fl.local,50) end,
								PostalCode	=															case when fl.codpost='' then '0000-000' else case when ISNUMERIC(left(codpost,4)) = 1 and ISNUMERIC(right(left(codpost,8),3)) = 1 then left(codpost,8) else '0000-000' end end,
								Country =																left('PT',2) /* deveria ser obtido da tabela FL */
							for xml path ('BillingAddress'), type
							),
							
							/* Subtabela ShipFromAddress*/ 
							(select																																										
								AddressDetail =															case 
																											/* os REPLACES cortam muito espa�o no ficheiro e removem os CHARs invalidos nas specs do XML*/
																											when fl.morada like '%'+char(39)+'%' then left(ltrim(rtrim(replace(fl.morada,CHAR(39),''))),100)
																											when fl.morada like '%'+char(34)+'%' then left(ltrim(rtrim(replace(fl.morada,CHAR(34),''))),100)
																											when fl.morada like '%'+char(38)+'%' then left(ltrim(rtrim(replace(fl.morada,CHAR(38),''))),100)
																											when fl.morada like '%'+char(14)+'%' then left(ltrim(rtrim(replace(fl.morada,CHAR(14),''))),100)
																											when fl.morada like '%'+char(2)+'%' then left(ltrim(rtrim(replace(fl.morada,CHAR(2),''))),100)
																											when fl.morada like '%'+char(8)+'%' then left(ltrim(rtrim(replace(fl.morada,CHAR(8),''))),100)
																											when fl.morada like '%<%' then left(ltrim(rtrim(replace(fl.morada,'<',''))),100)
																											when fl.morada like '%>%' then left(ltrim(rtrim(replace(fl.morada,'>',''))),100)
																											when fl.morada is null or fl.morada='' then 'Desconhecido'
																											else left(ltrim(rtrim(convert(varchar,fl.morada))),100)
																										end,
								City =																	case when fl.local='' then 'Desconhecido' else left(fl.local,50) end,
								PostalCode	=															case when fl.codpost='' then '0000-000' else case when ISNUMERIC(left(codpost,4)) = 1 and ISNUMERIC(right(left(codpost,8),3)) = 1 then left(codpost,8) else '0000-000' end end,
								Country =																left('PT',2) /* deveria ser obtido da tabela FL */
							for xml path ('ShipFromAddress'), type
							),
							
							Telephone =																	case when fl.telefone='' then null else left(convert(varchar,fl.telefone),20) end,
							Fax =																		case when fl.fax='' then null else left(convert(varchar,fl.fax),20) end,
							SelfBillingIndicator =														0 
						from fl (nolock) 
						order by 
							fl.no, fl.estab 
						for xml path ('Supplier'), type
						),
						
						/*Tabela Product: produto para referencias vazias*/
						(Select 
							ProductType = 																'O', 
							ProductCode =																'9999990', 
							ProductGroup =																null,
							ProductDescription = 														'Desconhecido',
							ProductNumberCode =															'9999990'
						for xml path ('Product'), type
						),
						
						/*Tabela Product: tabela de Produtos*/
						(Select 
							ProductType = 																case 
																											when st.stns=1 then left('S',1)
																											when st.stns=1 and st.ref='9999999' then left('O',1) 
																											else left('P',1) 
																										end, 
							ProductCode =																left(ltrim(rtrim(convert(varchar,st.ref))),30), 
							ProductGroup =																case when st.faminome='' then null else left(st.faminome,50) end,
							ProductDescription = 														case 
																											/* os REPLACES cortam muito espa�o no ficheiro e removem os CHARs invalidos nas specs do XML*/
																											when st.design like '%'+char(39)+'%' then left(ltrim(rtrim(replace(st.design,CHAR(39),''))),200)
																											when st.design like '%'+char(34)+'%' then left(ltrim(rtrim(replace(st.design,CHAR(34),''))),200)
																											when st.design like '%'+char(38)+'%' then left(ltrim(rtrim(replace(st.design,CHAR(38),''))),200)
																											when st.design like '%'+char(14)+'%' then left(ltrim(rtrim(replace(st.design,CHAR(14),''))),200)
																											when st.design like '%'+char(2)+'%' then left(ltrim(rtrim(replace(st.design,CHAR(2),''))),200)
																											when st.design like '%'+char(8)+'%' then left(ltrim(rtrim(replace(st.design,CHAR(8),''))),200)
																											when st.design like '%<%' then left(ltrim(rtrim(replace(st.design,'<',''))),200)
																											when st.design like '%>%' then left(ltrim(rtrim(replace(st.design,'>',''))),200)
																											when st.design is null or st.design='' then 'Desconhecido'
																											else left(ltrim(rtrim(convert(varchar,st.design))),200) 
																										end,
							ProductNumberCode =															case when convert(varchar,st.codigo) = '' then left(ltrim(rtrim(convert(varchar,st.ref))),50) else left(convert(varchar,st.codigo),50) end
						from st (nolock) 
						where 
							st.ref != ''
							and (
								ref in (select 
											distinct ref 
										from 
											fi (nolock) 
										where 
											rdata between @dataini and @datafim
											and fi.ndoc in (select ndoc from td (nolock) where td.tiposaft in ('GT','GR','GA','GC','GD'))
										)
								OR
								ref in (select 
											distinct ref 
										from 
											bi (nolock) 
										where 
											dataobra between @dataini and @datafim
											and bi.ndos = 17 /*dev. a fornecedor*/
										)
							)
						order by 
							st.ref
						for xml path ('Product'), type
						),
																																														
						/* Tabela TaxTable: Tabela de Regimes de IVA */
						(select
							(select
								TaxType =																left('IVA',3),
								TaxCountryRegion =														left(descricao,5), 
								TaxCode =																left(codigo,10), 
								Description =															left(desctaxa,255), 
								TaxPercentage =															taxa
							from regiva (nolock)
							order by 
								descricao
							for XML path ('TaxTableEntry'), type)
						for Xml path ('TaxTable'), type)					
									
					/* Final da tabela MasterFiles */
					for XML path ('MasterFiles'), type
					),
				
					/* Tabela SourceDocuments */
					(select
						
						/*incluir aqui a nova informa��o - MovementOfGoods - documentos de trasporte*/
						/* Tabela MovementOfGoods */
						(select
							
							NumberOfMovementLines = (
								(
								select 
									isnull(COUNT(fistamp),0)
								from fi (nolock)
								inner join td (nolock) on fi.ndoc=td.ndoc
								inner join ft (nolock) on ft.ftstamp=fi.ftstamp
								inner join ft2 (nolock) on ft.ftstamp = ft2.ft2stamp
								where
									ft.fdata between @dataini and @datafim  
									and td.tiposaft in ('GT','GR','GA','GC','GD')
									and ft.site = case when @Loja = 'TODAS' then ft.site else @Loja end
									and fi.ref != ''
									and fi.qtt > 0
									and ft2.subproc = ''
								)
								+
								(
								select 
									isnull(COUNT(bistamp),0)
								from bi (nolock)
								inner join bo (nolock) on bo.bostamp=bi.bostamp
								inner join bo2 (nolock) on bo2.bo2stamp = bo.bostamp
								where
									bo.dataobra between @dataini and @datafim  
									and bo.site = case when @Loja = 'TODAS' then bo.site else @Loja end
									and bi.ref != ''
									and bi.qtt > 0
									and bo.ndos = 17 /*dev. fornecedor*/
									and bo2.ATDocCode = ''
								)
							),
		
							TotalQuantityIssued = STR((
								(isnull((
								select
									STR(round(abs(sum(x.qtt)),2),10,2) from (
										select 
											fi.qtt
										from fi (nolock)
										inner join td (nolock) on fi.ndoc=td.ndoc
										inner join ft (nolock) on ft.ftstamp=fi.ftstamp
										inner join ft2 (nolock) on ft.ftstamp = ft2.ft2stamp
										where
											ft.fdata between @dataini and @datafim  
											and ft.site = case when @Loja = 'TODAS' then ft.site else @Loja end
											and ref != ''
											and qtt > 0
											and td.tiposaft in ('GT','GR','GA','GC','GD')
											and ft2.subproc = ''
									) as x	)		
									,0))
									+
									(ISNULL((
									select
										abs(sum(x.qtt)) from (
											select 
												bi.qtt
											from bi (nolock)
											inner join bo (nolock) on bo.bostamp=bi.bostamp
											inner join bo2 (nolock) on bo2.bo2stamp = bo.bostamp
											where
												bo.dataobra between @dataini and @datafim  
												and bo.site = case when @Loja = 'TODAS' then bo.site else @Loja end									
												and ref != ''
												and qtt > 0
												and bo.ndos = 17
												and bo2.ATDocCode = ''
										) as x		)
									,0))																													 
								),10,2),
							
							
							/* Subtabela StockMovement - FT */
							(select
								DocumentNumber =															left(isnull(b.invoiceType,'GT') + ' ' + convert(varchar,ft.ndoc) + '/' + CONVERT(varchar,ft.fno),60),
								/*SubTabela DocumentStatus*/
								 (select
									MovementStatus =														case when ft.anulado = 0 then 'N' else 'A' end,
									MovementStatusDate =													convert(varchar,CONVERT(date,ft.ousrdata))+ 'T' + ft.ousrhora,
									SourceID =																LEFT(ft.vendnm,30),
									SourceBilling =															'P'
								for XML path ('DocumentStatus'), type),																						
								
								Hash =																		isnull(ltrim(rtrim(left(b.hash,172))),'0'),
								HashControl =																case when b.hash is null or b.hash='' then '0' else '1' end,
								Period =																	DATEPART(month,ft.fdata),	
								MovementDate =																convert(date,ft.fdata), 
								MovementType =																left(isnull(b.invoiceType,'GT'),2),
								SystemEntryDate =															convert(varchar,convert(date,ft.ousrdata))+'T'+CONVERT(varchar,case when ft.ousrhora='' then '00:00:00' else ft.ousrhora end),
								CustomerID =																case 
																												/* Vendas feitas ao cliente 200 com altera��o de dados (Clientes Virtuais)*/ 
																												when b_utentes.no=200 and ft.ncont='999999990' and (ft.nome!=@nomeClienteDuzentos or ft.morada!='' or ft.telefone!='') then left(convert(varchar,ft.no) + '-A'  + convert(varchar,ft.ndoc) + convert(varchar,ft.fno) + '/' + convert(varchar,ft.estab),30)
																												when b_utentes.no=200 and ft.ncont!='999999990' and (ft.nome!=@nomeClienteDuzentos or ft.morada!='' or ft.telefone!='') then left(convert(varchar,ft.no) + '-A'  + convert(varchar,ft.ndoc) + convert(varchar,ft.fno) + '/' + convert(varchar,ft.estab) + '/' + CONVERT(varchar,ft.ncont) + '/' + CONVERT(varchar,ft.fno),30)
																												/* Vendas feitas ao cliente "Venda a Dinheiro" sem altera��o de dados*/
																												when ft.no=200 and ft.ncont='999999990'	then left(convert(varchar,ft.no)+'/'+convert(varchar,ft.estab),30)
																												when ft.no=200 and ft.ncont<>'999999990' then left(convert(varchar,ft.no)+'/'+convert(varchar,ft.estab) + '/' + CONVERT(varchar,ft.ncont) + '/' + CONVERT(varchar,ft.fno),30)
																												/* Vendas feitas aos restantes clientes*/
																												when ft.no<>200/* and ft.ncont='999999990'*/ then left(convert(varchar,ft.no)+'/'+convert(varchar,ft.estab),30)
																											end,
								SourceID =																	LEFT(ft.vendnm,30),
								MovementStartTime =															convert(varchar,convert(date,ft.ousrdata))+'T'+CONVERT(varchar,case when ft.ousrhora='' then '00:00:00' else ft.ousrhora end),
								/* Subtabela Line */
								(select
									LineNumber =															ROW_NUMBER() over (order by fi.lordem asc),
									/* Subtabela OrderReferences: origens de cada linha nas tabelas de Factura��o de todos os documentos excepto Notas de Cr�dito! */  
									OrderReferences = (select
										case
											when td.tiposaft!='NC' then 																															
												(select   
													OriginatingON =											left(n.nmdoc + ' ' + convert(varchar,n.ndoc)+'/'+CONVERT(varchar,n.fno),255),
													OrderDate =												convert(date,ft.fdata)			
												from fi (nolock) as n
												where
													n.fistamp=fi.ofistamp
												for xml path (''), type)
											else
												null 
										end
									),
									 
									ProductCode =															case when fi.ref='' then '9999990' else left(fi.ref,30) end,             
									ProductDescription = 													case 
																												/* os REPLACES cortam muito espa�o no ficheiro e removem os CHARs invalidos nas specs do XML*/
																												when fi.design like '%'+char(39)+'%' then left(ltrim(rtrim(replace(fi.design,CHAR(39),''))),200)
																												when fi.design like '%'+char(34)+'%' then left(ltrim(rtrim(replace(fi.design,CHAR(34),''))),200)
																												when fi.design like '%'+char(38)+'%' then left(ltrim(rtrim(replace(fi.design,CHAR(38),''))),200)
																												when fi.design like '%'+char(14)+'%' then left(ltrim(rtrim(replace(fi.design,CHAR(14),''))),200)
																												when fi.design like '%'+char(2)+'%' then left(ltrim(rtrim(replace(fi.design,CHAR(2),''))),200)
																												when fi.design like '%'+char(8)+'%' then left(ltrim(rtrim(replace(fi.design,CHAR(8),''))),200)
																												when fi.design like '%<%' then left(ltrim(rtrim(replace(fi.design,'<',''))),200)
																												when fi.design like '%>%' then left(ltrim(rtrim(replace(fi.design,'>',''))),200)
																												when fi.design is null or fi.design='' then 'Desconhecido'
																												else left(ltrim(rtrim(convert(varchar,fi.design))),200)
																											end,								
									Quantity =																convert(int,fi.qtt),
									UnitOfMeasure =															case
																												when fi.unidade='' then 'UN'								
																												else left(upper(convert(varchar,fi.unidade)),20)
																											end,
									UnitPrice =																case
																												when (fi.etiliquido / (fi.iva/100+1)) > 0 then 									
																													case 
																														when fi.ivaincl=1 then abs((fi.etiliquido / (fi.iva/100+1)) / fi.qtt) 
																														else abs(fi.etiliquido/fi.qtt)
																													end
																												else 0
																											end,
									Description =															case when fi.design = '' then ' ' else left(fi.design,60) end,
									DebitAmount =															case 
																												when (fi.etiliquido / (fi.iva/100+1))<0 then 
																																							case 
																																								when fi.ivaincl=1 then abs((fi.etiliquido / (fi.iva/100+1))) 
																																								else abs(fi.etiliquido)
																																							end
																												else null 
																											end,
									CreditAmount =															case 
																												when (fi.etiliquido / (fi.iva/100+1))>0 then  
																																							case 
																																								when fi.ivaincl=1 then abs((fi.etiliquido / (fi.iva/100+1))) 
																																								else abs(fi.etiliquido)
																																							end
																												when (fi.etiliquido / (fi.iva/100+1))=0 then 0 
																												else null 
																											end,
									/* Subtabela Tax: o subquery com TOP 1 deve-se ao facto de que h� v�rios REGIVAs para o c�digo 0 */
									(select
										TaxType =															left('IVA',3),
										TaxCountryRegion =													left('PT',5), /*deveria ser obtido da tabela regIVA*/
										TaxCode =															(select top 1 left(codigo,10) from regiva where tabiva=fi.tabiva),
										TaxPercentage =														convert(int,fi.iva) 								
									for xml path ('Tax'), type
									),
									TaxExemptionReason =													case when fi.iva=0 then left('Isen��o prevista no n.� 1 do art.� 9.� do CIVA',60) else null end,
									SettlementAmount =														case 
																												when (convert(numeric,desconto))>0 or (convert(numeric,desc2))>0 or (convert(numeric,u_descval))>0 then 
																													abs(
																														case 
																															when fi.ivaincl=1 then (fi.epv*fi.qtt) / (fi.iva/100+1)
																															else fi.epv*fi.qtt
																														end
																													)-
																													abs(
																														case 
																															when fi.ivaincl=1 then fi.etiliquido / (fi.iva/100+1)
																															else fi.etiliquido/fi.qtt 
																														end
																													) 
																												else null 
																											end
								/* Final da subtabela Line*/
								from fi (nolock) as fi
								where
									fi.ftstamp=ft.ftstamp 
									and fi.epromo=0
									and ( (fi.qtt <> 0 or fi.etiliquido <> 0) OR  ( ft.anulado = 1 ) )
								order by 
									fi.lordem
								for xml path ('Line'), type
								),
								
								/* Subtabela DocumentTotals */
								(select
									TaxPayable =															str(abs(ft.ettiva),10,2),
									NetTotal =																str(abs(ft.eivain1+ft.eivain2+ft.eivain3+ft.eivain4+ft.eivain5+ft.eivain6+ft.eivain7+ft.eivain8+ft.eivain9),10,2),
									GrossTotal =															str(abs(ft.etotal+ft.efinv),10,2),
									
									/* Subtabela Currrency, apenas exportada quando a moeda de um documento n�o � EURO (indica��o DGCI)*/
									Currency = (select 
										case 
											when ft.moeda not in ('PTE ou EURO','EURO','EUR') then 
												(select
													CurrencyCode =											case when ft.moeda='' then left('EUR',3) else left(ft.moeda,3) end,
													CurrencyAmount =										str(abs(ft.etotal),10,2)
												for xml path (''), type)  
											else null 
										end
									)
								for xml path ('DocumentTotals'),type
								)
								
							from ft  as ft (nolock)
							inner join ft2 (nolock) on ft.ftstamp=ft2.ft2stamp
							inner join td  (nolock) on ft.ndoc=td.ndoc 
							inner join b_utentes  (nolock) ON b_utentes.no=(case when td.lancacli=0 then ft.no else ft2.c2no end)  and b_utentes.estab=(case when td.lancacli=0 then ft.estab else ft2.c2estab end) 
							left join B_cert (nolock) as b on ft.ftstamp=b.stamp
							where 
								ft.fdata>=@dataini 
								and ft.fdata<=@datafim
								and td.tiposaft in ('GT','GR','GA','GC','GD') /*s� incluir guias de transporte*/
								and ft2.subproc = '' /*guias que ainda n�o foram enviadas*/
								/* Novo crit�rio que separa documentos por LOJA, para clientes multi-base de dados */
								and ft.site = case when @Loja = 'TODAS' then ft.site else @Loja end
							order by
								ft.ndoc,ft.fno
							for xml path ('StockMovement'), type)
							,
							
							/* Subtabela StockMovement - BO*/
							(select
								DocumentNumber =															left(isnull(b.invoiceType,'GD') + ' ' + convert(varchar,bo.ndos) + '/' + CONVERT(varchar,bo.obrano),60),
								/*SubTabela DocumentStatus*/
								(select
									MovementStatus =														'N',--case when bo.anulado = 0 then 'N' else 'A' end,
									MovementStatusDate =													convert(varchar,CONVERT(date,bo.ousrdata))+ 'T' + bo.ousrhora,
									SourceID =																LEFT(bo.vendnm,30),
									SourceBilling =															'P'
								for XML path ('DocumentStatus'), type),																						
								Hash =																		isnull(ltrim(rtrim(left(b.hash,172))),'0'),
								HashControl =																case when b.hash is null or b.hash='' then '0' else '1' end,
								Period =																	DATEPART(month,bo.dataobra),	
								MovementDate =																convert(date,bo.dataobra), 
								MovementType =																'GD',
								SystemEntryDate =															convert(varchar,convert(date,bo.ousrdata))+'T'+CONVERT(varchar,case when bo.ousrhora='' then '00:00:00' else bo.ousrhora end),
								CustomerID =																left('FL' + convert(varchar,bo.no)+'/'+CONVERT(varchar,bo.estab),30),
								SourceID =																	LEFT(bo.vendnm,30),
								MovementStartTime =															convert(varchar,convert(date,bo.ousrdata))+'T'+CONVERT(varchar,case when bo.ousrhora='' then '00:00:00' else bo.ousrhora end),
								/* Subtabela Line */
								(select
									LineNumber =															ROW_NUMBER() over (order by bi.lordem asc),
									ProductCode =															case when bi.ref='' then '9999990' else left(bi.ref,30) end,             
									ProductDescription = 													case 
																												/* os REPLACES cortam muito espa�o no ficheiro e removem os CHARs invalidos nas specs do XML*/
																												when bi.design like '%'+char(39)+'%' then left(ltrim(rtrim(replace(bi.design,CHAR(39),''))),200)
																												when bi.design like '%'+char(34)+'%' then left(ltrim(rtrim(replace(bi.design,CHAR(34),''))),200)
																												when bi.design like '%'+char(38)+'%' then left(ltrim(rtrim(replace(bi.design,CHAR(38),''))),200)
																												when bi.design like '%'+char(14)+'%' then left(ltrim(rtrim(replace(bi.design,CHAR(14),''))),200)
																												when bi.design like '%'+char(2)+'%' then left(ltrim(rtrim(replace(bi.design,CHAR(2),''))),200)
																												when bi.design like '%'+char(8)+'%' then left(ltrim(rtrim(replace(bi.design,CHAR(8),''))),200)
																												when bi.design like '%<%' then left(ltrim(rtrim(replace(bi.design,'<',''))),200)
																												when bi.design like '%>%' then left(ltrim(rtrim(replace(bi.design,'>',''))),200)
																												when bi.design is null or bi.design='' then 'Desconhecido'
																												else left(ltrim(rtrim(convert(varchar,bi.design))),200)
																											end,								
									Quantity =																convert(int,bi.qtt),
									UnitOfMeasure =															case
																												when bi.unidade='' then 'UN'						
																												else left(upper(convert(varchar,bi.unidade)),20)
																											end,
									UnitPrice =																case
																												when (bi.ettdeb / (bi.iva/100+1)) > 0 then 									
																													case 
																														when bi.ivaincl=1 then abs((bi.ettdeb / (bi.iva/100+1)) / bi.qtt) 
																														else abs(bi.ettdeb/bi.qtt)
																													end
																												else 0
																											end,
									Description =															case when bi.design = '' then ' ' else left(bi.design,60) end,
									DebitAmount =															case 
																												when (bi.ettdeb / (bi.iva/100+1))<0 then 
																																						case 
																																							when bi.ivaincl=1 then abs((bi.ettdeb / (bi.iva/100+1))) 
																																							else abs(bi.ettdeb)
																																						end
																												else null 
																											end,
									CreditAmount =															case 
																												when (bi.ettdeb / (bi.iva/100+1)) > 0 then 
																																						case 
																																							when bi.ivaincl=1 then abs((bi.ettdeb / (bi.iva/100+1))) 
																																							else abs(bi.ettdeb)
																																						end
																												when (bi.ettdeb / (bi.iva/100+1)) = 0 then 0 
																												else null 
																											end,
									/* Subtabela Tax: o subquery com TOP 1 deve-se ao facto de que h� v�rios REGIVAs para o c�digo 0 */
									(select
										TaxType =															left('IVA',3),
										TaxCountryRegion =													left('PT',5), /*deveria ser obtido da tabela regIVA*/
										TaxCode =															(select top 1 left(codigo,10) from regiva where tabiva=bi.tabiva),
										TaxPercentage =														convert(int,bi.iva) 								
									for xml path ('Tax'), type
									),
									TaxExemptionReason =													case when bi.iva = 0 then left('Isen��o prevista no n.� 1 do art.� 9.� do CIVA',60) else null end,
									SettlementAmount =														case 
																												when (convert(numeric,desconto))>0 or (convert(numeric,desc2))>0 then 
																													abs(
																														case 
																															when bi.ivaincl=1 then (bi.edebito * bi.qtt) / (bi.iva/100+1)
																															else bi.edebito * bi.qtt
																														end
																													)-
																													abs(
																														case 
																															when bi.ivaincl=1 then bi.ettdeb / (bi.iva/100+1)
																															else bi.ettdeb / bi.qtt 
																														end
																													) 
																												else null 
																											end
								/* Final da subtabela Line*/
								from bi (nolock)
								where
									bi.bostamp = bo.bostamp 
									and (bi.qtt <> 0 or bi.ettdeb <> 0)
								order by 
									bi.lordem
								for xml path ('Line'), type
								),
								
								/* Subtabela DocumentTotals */
								(select
									TaxPayable =															str(abs(bo2.etotiva),10,2),
									NetTotal =																str(abs(bo.etotaldeb),10,2),
									GrossTotal =															str(abs(bo.etotal/*+bo.efinv*/),10,2),
									
									/* Subtabela Currrency, apenas exportada quando a moeda de um documento n�o � EURO (indica��o DGCI)*/
									(select 
										case 
											when bo.moeda not in ('PTE ou EURO','EURO','EUR') then 
												(select
													CurrencyCode =											case when bo.moeda='' then left('EUR',3) else left(bo.moeda,3) end,
													CurrencyAmount =										str(abs(bo.etotal),10,2)
												for xml path ('Currency'), type)  
											else null 
										end
									)
								for xml path ('DocumentTotals'),type
								)
								
							from bo (nolock)
							inner join bo2 (nolock) on bo.bostamp=bo2.bo2stamp
							inner join fl (nolock) ON fl.no = bo.no  and fl.estab = bo.estab
							left join B_cert (nolock) as b on bo.bostamp=b.stamp
							where 
								bo.dataobra >= @dataini 
								and bo.dataobra <= @datafim
								and bo.ndos = 17 /*dev. a fornecedor*/
								and bo.site = case when @Loja = 'TODAS' then bo.site else @Loja end
								and bo2.ATDocCode = ''
							for xml path ('StockMovement'), type)
							
						for xml path ('MovementOfGoods'), type)
						/********************************/
						
					for xml path ('SourceDocuments'), type)
					
				for xml path (''), root ('AuditFile'), type
				
			)),'<AuditFile>','<?xml version = "1.0" encoding="Windows-1252" standalone="yes"?><AuditFile xmlns="urn:OECD:StandardAuditFile-Tax:PT_1.02_01" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">')) as saft
			
		end try
		
		begin catch
			declare
				@errortext				varchar(256),
				@error_severity			int,
				@error_state			int
			
			select 
				@errortext = isnull(ERROR_PROCEDURE(),'') + ': ' + ERROR_MESSAGE(),
				@error_severity = ERROR_SEVERITY(),
				@error_state = ERROR_state()

			raiserror(@errortext,@error_severity,@error_state)
			return(1)
		end catch
	end

go

grant execute on dbo.up_gen_saftTransp to public
grant control on dbo.up_gen_saftTransp to public