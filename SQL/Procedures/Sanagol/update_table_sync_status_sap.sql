
/*

 exec sap_service_getDocument

exec update_table_sync_status_sap '5','falhou o envio ou a mensagem de resposta n�o � conhecida ','ADM18F03081-5F48-4B1A-A80','{"error":{"code":"CX_SXML_PARSE_ERROR/001560AA0E081DEB8CA398D5EBFB9406","message":{"lang":"pt","value":"Erro na an�lise sint�tica de um fluxo XML: BOM / charset detection failed."},"innererror":{"transactionid":"FA04707B6EA40160E0065A4CD01A5462","timestamp":"20240223174245.7553920","Error_Resolution":{"SAP_Transaction":"Run transaction /IWFND/ERROR_LOG on SAP NW Gateway hub system and search for entries with the timestamp above for more details","SAP_Note":"See SAP Note 1797736 for error analysis (https:/service.sap.com/sap/support/notes/1797736)"},"errordetails":[]}}}'
 
*/



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[update_table_sync_status_sap]') IS NOT NULL
	drop procedure dbo.update_table_sync_status_sap
go

create procedure dbo.update_table_sync_status_sap
	@MSG1			VARCHAR(18),
	@MSG2			VARCHAR(254),
	@ftstamp		VARCHAR(36),
	@xmlinput		VARCHAR(MAX)=''


AS
SET NOCOUNT ON

	DECLARE @type			VARCHAR(18)
	DECLARE @name			VARCHAR(100) ='SENDDOCUMENT'
	DECLARE @registerStamp	VARCHAR(50)
	DECLARE @site			VARCHAR(60)


	select @site =site from ft where ftstamp = @ftstamp

	SET @type = case when @MSG1 in('1','2') then 'Sucesso' else 'Falhou' end

	update table_sync_status_sap set 
				sync = (case when @MSG1 in('1','2') then 1 else 0 end ), 
				sync_date=GETDATE() ,  
				archivedType = (case when @MSG1 in('1','2') then 1 else 0 end ) , 
				archivedTypeDesc =   (case when @MSG1 in('1','2') then 'Enviado para a sonagol' else '' end ) 
	
	where regstamp =@ftstamp
			

	EXEC LogsExternal.dbo.usp_insert_ExternalCommunicationSapLogs @type,@name ,@MSG2, 'FT', @ftstamp,@site,@xmlinput



Grant Execute on dbo.update_table_sync_status_sap to Public
Grant control on dbo.update_table_sync_status_sap to Public
GO
