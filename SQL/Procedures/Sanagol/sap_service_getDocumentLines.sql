
/*

 exec sap_service_getDocumentLines 'ADM3D63822E-4768-425C-AC1','NC'
exec sap_service_getDocumentLines 'frEF0F5D2C-06A6-4453-86A ','FR'
*/



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[sap_service_getDocumentLines]') IS NOT NULL
	drop procedure dbo.sap_service_getDocumentLines
go

create procedure dbo.sap_service_getDocumentLines

@ftstamp		VARCHAR(36), 
@tiposaft       varchar(15)

/* with encryption */

AS
SET NOCOUNT ON
	

	
		SELECT 
                fi.design												AS descr,
                fi.lordem												AS item ,
                cast(Round(fi.etiliquido / fi.qtt,2)as numeric(36,2))	AS prcunit,
                cast(fi.qtt	as numeric(36,2))							AS quant,
                cast(fi.etiliquido	as numeric(36,2))					AS val, 
				(case when @tiposaft='NC'  then isnull((select ndoc from uf_nDoc_Mecofarma ((select fi_temp.ftstamp from fi(nolock) fi_temp where fi_temp.fistamp = fi.ofistamp))),'')
				 else '' end ) AS	docref
		FROM fi	(nolock)
		WHERE 
			fi.ftstamp=@ftstamp  and qtt!=0

Grant Execute on dbo.sap_service_getDocumentLines to Public
Grant control on dbo.sap_service_getDocumentLines to Public
GO
