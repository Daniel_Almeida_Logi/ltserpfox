
/*

 exec sap_service_getDocument
*/



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[sap_service_getDocument]') IS NOT NULL
	drop procedure dbo.sap_service_getDocument
go

create procedure dbo.sap_service_getDocument

/* with encryption */

AS
SET NOCOUNT ON
	

	
		SELECT 
			   CONVERT(VARCHAR(10), ft.fdata, 112) 			AS fdata,
				LEFT(ft2.u_nbenef,10)						AS nba, -- isto foi pedido do cliente fazer left , se eles preencherem a mais , vai ser enviado valor diferente
				LEFT(ft2.id_efr_externa,12)					AS nclient, -- isto foi pedido do cliente fazer left , se eles preencherem a mais , vai ser enviado valor diferente
				td.tiposaft+'/' + convert(varchar(10),ft.ndoc)+'/' +  convert(varchar(10),ft.fno	)+'/'+  convert(varchar(10),ft.ftano)		AS nfact,
				ft.nome										AS nameU,
				LEFT(u_receita,15) 							AS nrec, -- isto foi pedido do cliente fazer left , se eles preencherem a mais , vai ser enviado valor diferente
				td.tiposaft									AS tdoc,
				cast(ft.etotal	as numeric(36,2))			AS vFact , 
				td.tiposaft									AS tiposaft,
				ftstamp										AS ftstamp
		FROM table_sync_status_sap  (nolock) 
		inner join ft (nolock) on ft.ftstamp = table_sync_status_sap.regstamp
		left outer join ft2	(nolock) on ft2.ft2stamp = ft.ftstamp
		left outer join td	(nolock) on td.ndoc = ft.ndoc
		WHERE 
			 td.tiposaft !='' and table_sync_status_sap.sync=0
		Order by table_sync_status_sap.ins_date , td.tiposaft asc
			


Grant Execute on dbo.sap_service_getDocument to Public
Grant control on dbo.sap_service_getDocument to Public
GO
