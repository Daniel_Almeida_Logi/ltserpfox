/* 
	exec   up_get_TimeWindowValues
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_TimeWindowValues]') IS NOT NULL
	drop procedure dbo.up_get_TimeWindowValues
go

create procedure [dbo].up_get_TimeWindowValues

AS

	SELECT
		value
		,design
		,obs
	FROM	
		 TimeWindowValues(nolock)

GO
Grant Execute On [dbo].[up_get_TimeWindowValues] to Public
Grant control On [dbo].[up_get_TimeWindowValues]to Public
GO	