/* 
	exec   up_get_InNonDeliveryCaseTypeValues
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_InNonDeliveryCaseTypeValues]') IS NOT NULL
	drop procedure dbo.up_get_InNonDeliveryCaseTypeValues
go

create procedure [dbo].up_get_InNonDeliveryCaseTypeValues

AS

	SELECT
		value
		,design
		,obs
	FROM	
		InNonDeliveryCaseTypeValues(nolock)

GO
Grant Execute On [dbo].[up_get_InNonDeliveryCaseTypeValues] to Public
Grant control On [dbo].[up_get_InNonDeliveryCaseTypeValues]to Public
GO	