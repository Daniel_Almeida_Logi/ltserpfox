/* 
	exec   up_savePostOfficeResponse '','','','','','',''
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_savePostOfficeResponse]') IS NOT NULL
	drop procedure dbo.up_savePostOfficeResponse
go

create procedure [dbo].up_savePostOfficeResponse

@token						varchar(36) 
,@status					varchar(100)
,@deliveryNoteId			varchar(50)
,@shipmentDataToken			varchar(36)
,@errorsToken				varchar(36)
,@documentFilesToken		varchar(36)
,@ObjectsBEToken			varchar(36)
,@statusID					NUMERIC(10,2)
,@deliveryPointsToken		varchar(36)=''

AS


IF (NOT EXISTS(Select * from pfResponse(nolock) where @token = token))
	BEGIN
		INSERT INTO pfResponse (token, status, deliveryNoteId, shipmentDataToken, errorsToken, documentFilesToken, ObjectsBEToken
										,ousrdata ,ousrinis, usrdata, usrinis,statusID,deliveryPointsToken)
		values(@token ,@status, @deliveryNoteId, @shipmentDataToken, @errorsToken, @documentFilesToken, @ObjectsBEToken
				,getdate(), 'ADM', getdate(), 'ADM',@statusID,@deliveryPointsToken)	 
	END
ELSE
	BEGIN
		UPDATE pfResponse set 
		token = @token, 
		status = @status, 
		deliveryNoteId = @deliveryNoteId, 
		shipmentDataToken = @shipmentDataToken,
		errorsToken = @errorsToken, 
		documentFilesToken = @documentFilesToken, 
		ObjectsBEToken = @ObjectsBEToken,
		ousrdata = getdate(), 
		ousrinis = 'ADM', 
		usrdata = getdate(), 
		usrinis = 'ADM',
		statusID = @statusID,
		deliveryPointsToken = @deliveryPointsToken
	END

GO
Grant Execute On [dbo].[up_savePostOfficeResponse] to Public
Grant control On [dbo].[up_savePostOfficeResponse]to Public
GO	