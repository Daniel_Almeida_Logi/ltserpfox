SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON


IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'pfReq'))
BEGIN

	CREATE TABLE [dbo].[pfReq](
		[token]						[varchar](36) NOT NULL,
		[type]						[int],
		[typeDesc]					[varchar](50),
		[typePostOffice]			[varchar](50),
		[authenticationID]			[varchar](36),
		[userID]					[varchar](36),
		[processedSQL]				[bit],
		[test]						[bit],
		[site]						[varchar](50),
		[pfReq_DeliveryStamp]		[varchar](36) NOT NULL,
		[pfEMSToken]				[varchar](36) NOT NULL,
		[pfReq_ShipCloseToken]		[varchar](36) NOT NULL,
		[ousrdata]					[datetime],
		[ousrinis]					[varchar](30),
		[usrdata]					[datetime],
		[usrinis]					[varchar](30)
		
	 CONSTRAINT [PK_pfReq] PRIMARY KEY CLUSTERED 
	(
		[token] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[pfReq] ADD  CONSTRAINT [DF_pfReq_token]  DEFAULT ('') FOR [token]

	ALTER TABLE [dbo].[pfReq] ADD  CONSTRAINT [DF_pfReq_type]  DEFAULT (0) FOR [type]

	ALTER TABLE [dbo].[pfReq] ADD  CONSTRAINT [DF_pfReq_typeDesc]  DEFAULT ('')  FOR [typeDesc]
	
	ALTER TABLE [dbo].[pfReq] ADD  CONSTRAINT [DF_pfReq_typePostOffice]  DEFAULT ('') FOR [typePostOffice]

	ALTER TABLE [dbo].[pfReq] ADD  CONSTRAINT [DF_pfReq_authenticationID]  DEFAULT ('') FOR [authenticationID]

	ALTER TABLE [dbo].[pfReq] ADD  CONSTRAINT [DF_pfReq_userID]  DEFAULT ('') FOR [userID]

	ALTER TABLE [dbo].[pfReq] ADD  CONSTRAINT [DF_pfReq_processedSQL]  DEFAULT (0) FOR [processedSQL]

	ALTER TABLE [dbo].[pfReq] ADD  CONSTRAINT [DF_pfReq_test]  DEFAULT (0) FOR [test]

	ALTER TABLE [dbo].[pfReq] ADD  CONSTRAINT [DF_pfReq_site]  DEFAULT ('') FOR [site]

	ALTER TABLE [dbo].[pfReq] ADD  CONSTRAINT [DF_pfReq_DeliveryStamp]  DEFAULT ('') FOR [pfReq_DeliveryStamp]

	ALTER TABLE [dbo].[pfReq] ADD  CONSTRAINT [DF_pfReq_pfEMSToken]  DEFAULT ('') FOR [pfEMSToken]

	ALTER TABLE [dbo].[pfReq] ADD  CONSTRAINT [DF_pfReq_pfReq_ShipCloseToken]  DEFAULT ('') FOR [pfReq_ShipCloseToken]

	ALTER TABLE [dbo].[pfReq] ADD  CONSTRAINT [DF_pfReq_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[pfReq] ADD  CONSTRAINT [DF_pfReq_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[pfReq] ADD  CONSTRAINT [DF_pfReq_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[pfReq] ADD  CONSTRAINT [DF_pfReq_usrinis]  DEFAULT ('') FOR [usrinis]
END									
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE 
object_id = OBJECT_ID(N'[dbo].[pfReq]') AND name = N'IX_pfReq_pfEMSToken')
CREATE NONCLUSTERED INDEX [IX_pfReq_pfEMSToken] ON [dbo].[pfReq]
(
    [pfEMSToken] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE 
object_id = OBJECT_ID(N'[dbo].[pfReq]') AND name = N'IX_pfReq_DeliveryStamp')
CREATE NONCLUSTERED INDEX [IX_pfReq_DeliveryStamp] ON [dbo].[pfReq]
(
    [pfReq_DeliveryStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE 
object_id = OBJECT_ID(N'[dbo].[pfReq]') AND name = N'IX_pfReq_ShipCloseToken')
CREATE NONCLUSTERED INDEX [IX_pfReq_ShipCloseToken] ON [dbo].[pfReq]
(
    [pfReq_ShipCloseToken] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

------------------------------------------------------------------------------------------------------------------------------------------------------------
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'pfReq_Delivery'))
BEGIN
	
	CREATE TABLE [dbo].[pfReq_Delivery](
		[stamp]						[varchar](36) NOT NULL,
		[clientId]					[varchar](10),
		[contractId]				[varchar](10),
		[distributionChannelId]		[int],
		[extData]					[varchar](max),
		[pfReq_Delivery_shipToken]	[varchar](36) NOT NULL,
		[subProductId]				[varchar](10),
		[ousrdata]					[datetime],
		[ousrinis]					[varchar](30),
		[usrdata]					[datetime],
		[usrinis]					[varchar](30)
		
	 CONSTRAINT [PK_pfReq_Delivery] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[pfReq_Delivery] ADD  CONSTRAINT [DF_pfReq_Delivery_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[pfReq_Delivery] ADD  CONSTRAINT [DF_pfReq_Delivery_clientId]  DEFAULT ('') FOR [clientId]

	ALTER TABLE [dbo].[pfReq_Delivery] ADD  CONSTRAINT [DF_pfReq_Delivery_contractId]  DEFAULT ('') FOR [contractId]

	ALTER TABLE [dbo].[pfReq_Delivery] ADD  CONSTRAINT [DF_pfReq_Delivery_distributionChannelId]  DEFAULT (0) FOR [distributionChannelId]

	ALTER TABLE [dbo].[pfReq_Delivery] ADD  CONSTRAINT [DF_pfReq_Delivery_extData]  DEFAULT ('') FOR [extData]

	ALTER TABLE [dbo].[pfReq_Delivery] ADD  CONSTRAINT [DF_pfReq_Delivery_subProductId]  DEFAULT ('') FOR [subProductId]

	ALTER TABLE [dbo].[pfReq_Delivery] ADD  CONSTRAINT [DF_pfReq_Delivery_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[pfReq_Delivery] ADD  CONSTRAINT [DF_pfReq_Delivery_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[pfReq_Delivery] ADD  CONSTRAINT [DF_pfReq_Delivery_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[pfReq_Delivery] ADD  CONSTRAINT [DF_pfReq_Delivery_usrinis]  DEFAULT ('') FOR [usrinis]
END
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE 
object_id = OBJECT_ID(N'[dbo].[pfReq_Delivery]') AND name = N'IX_pfReq_Delivery_shipToken')
CREATE NONCLUSTERED INDEX [IX_pfReq_Delivery_shipToken] ON [dbo].[pfReq_Delivery]
(
    [pfReq_Delivery_shipToken] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

----------------------------------------------------------------------------------------------------------------------------------------------------



IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'pfReq_Delivery_Ship'))
BEGIN
	
	CREATE TABLE [dbo].[pfReq_Delivery_Ship](
		[stamp]									[varchar](36) NOT NULL,
		[shipmentsToken]						[varchar](36) NOT NULL,
		[extData]								[varchar](max),
		[hasSenderInformation]					[bit],
		[pipAuthToken]							[varchar](36) NOT NULL,
		[pfadressReceiverStamp]					[varchar](36),
		[pfadressSenderStamp]					[varchar](36),
		[pfReq_Delivery_Ship_Info_Stamp]		[varchar](36),
		[pfSpecialServicesToken]				[varchar](36) NOT NULL,
		[aggregationNum]						[varchar](13),
		[isLastShippment]						[bit],
		[isPartialDelivery]						[bit],
		[isSingleCharge]						[bit],
		[refDeliveryAggregation]				[bit],
		[ousrdata]								[datetime],
		[ousrinis]								[varchar](30),
		[usrdata]								[datetime],
		[usrinis]								[varchar](30)
		
	 CONSTRAINT [PK_pfReq_Delivery_Ship] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[pfReq_Delivery_Ship] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_shipmentsToken]  DEFAULT ('') FOR [shipmentsToken]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_extData]  DEFAULT ('') FOR [extData]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_hasSenderInformation]  DEFAULT (0) FOR [hasSenderInformation]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_pipAuthorizationToken]  DEFAULT ('') FOR [pipAuthorizationToken]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_pfadressReceiverStamp]  DEFAULT ('') FOR [pfadressReceiverStamp]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_pfadressSenderStamp]  DEFAULT ('') FOR [pfadressSenderStamp]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_pfReq_Delivery_Ship_InfoStamp]  DEFAULT ('') FOR [pfReq_Delivery_Ship_InfoStamp]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_pfSpecialServicesToken]  DEFAULT ('') FOR [pfSpecialServicesToken]

	ALTER TABLE [dbo].[deliveryAggregationInf] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_aggregationNum]  DEFAULT ('') FOR [aggregationNum]
					  						 
	ALTER TABLE [dbo].[deliveryAggregationInf] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_isLastShippment]  DEFAULT (0) FOR [isLastShippment]
					  						 
	ALTER TABLE [dbo].[deliveryAggregationInf] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_isPartialDelivery]  DEFAULT (0) FOR [isPartialDelivery]
					  						 
	ALTER TABLE [dbo].[deliveryAggregationInf] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_isSingleCharge]  DEFAULT (0) FOR [isSingleCharge]
					  						 
	ALTER TABLE [dbo].[deliveryAggregationInf] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_refDeliveryAggregation]  DEFAULT (0) FOR [refDeliveryAggregation]
	
	ALTER TABLE [dbo].[pfReq_Delivery_Ship] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_usrinis]  DEFAULT ('') FOR [usrinis]
END
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE 
object_id = OBJECT_ID(N'[dbo].[pfReq_Delivery_Ship]') AND name = N'IX_pfReq_Delivery_Ship_shipmentsToken')
CREATE NONCLUSTERED INDEX [IX_pfReq_Delivery_Ship_shipmentsToken] ON [dbo].[pfReq_Delivery_Ship]
(
    [shipmentsToken] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE 
object_id = OBJECT_ID(N'[dbo].[pfReq_Delivery_Ship]') AND name = N'IX_pfReq_Delivery_Ship_pfSpecialServicesToken')
CREATE NONCLUSTERED INDEX [IX_pfReq_Delivery_Ship_pfSpecialServicesToken] ON [dbo].[pfReq_Delivery_Ship]
(
    [pfSpecialServicesToken] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE 
object_id = OBJECT_ID(N'[dbo].[pfReq_Delivery_Ship]') AND name = N'IX_pfReq_Delivery_Ship_pfadressReceiverStamp')
CREATE NONCLUSTERED INDEX [IX_pfReq_Delivery_Ship_pfadressReceiverStamp] ON [dbo].[pfReq_Delivery_Ship]
(
    [pfadressReceiverStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE 
object_id = OBJECT_ID(N'[dbo].[pfReq_Delivery_Ship]') AND name = N'IX_pfReq_Delivery_Ship_pfadressSenderStamp')
CREATE NONCLUSTERED INDEX [IX_pfReq_Delivery_Ship_pfadressSenderStamp] ON [dbo].[pfReq_Delivery_Ship]
(
    [pfadressSenderStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


---------------------------------------------------------------------------------------------------------------------------------------------------------

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'pfReq_Delivery_Ship_Info'))
BEGIN
	
	CREATE TABLE [dbo].[pfReq_Delivery_Ship_Info](
		[stamp]							[varchar](36) NOT NULL,
		[ATCode]						[varchar](254),
		[clientReference]				[varchar](50),
		[codigoAgrupamento]				[varchar](50),
		[pfCustomsDataStamp]			[varchar](36) NOT NULL,
		[declaredValue]					[decimal],
		[exportType]					[varchar](50),
		[isDevolution]					[bit],
		[observations]					[varchar](254),
		[originalObject]				[varchar](35),
		[quantity]						[int],
		[UPUCode]						[varchar](50),
		[validationDate]				[datetime],
		[weight]						[int],
		[chargeDate_partialDelivery]	[bit],
		[chargeDate_schedulingData]		[datetime],
		[chargeDate_schedulingHour]		[datetime],
		[ousrdata]						[datetime],
		[ousrinis]						[varchar](30),
		[usrdata]						[datetime],
		[usrinis]						[varchar](30)
		
	 CONSTRAINT [PK_pfReq_Delivery_Ship_Info] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[pfReq_Delivery_Ship_Info] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_Info_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship_Info] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_Info_ATCode]  DEFAULT ('') FOR [ATCode]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship_Info] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_Info_clientReference]  DEFAULT ('') FOR [clientReference]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship_Info] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_Info_codigoAgrupamento]  DEFAULT ('') FOR [codigoAgrupamento]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship_Info] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_Info_pfCustomsDataStamp]  DEFAULT ('') FOR [pfCustomsDataStamp]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship_Info] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_Info_declaredValue]  DEFAULT ('') FOR [declaredValue]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship_Info] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_Info_exportType]  DEFAULT ('') FOR [exportType]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship_Info] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_Info_isDevolution]  DEFAULT (0) FOR [isDevolution]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship_Info] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_Info_observations]  DEFAULT ('') FOR [observations]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship_Info] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_Info_originalObject]  DEFAULT ('') FOR [originalObject]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship_Info] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_Info_quantity]  DEFAULT (0) FOR [quantity]
	
	ALTER TABLE [dbo].[pfReq_Delivery_Ship_Info] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_Info_UPUCode]  DEFAULT ('') FOR [UPUCode]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship_Info] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_Info_validationDate]  DEFAULT ('') FOR [validationDate]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship_Info] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_Info_weight]  DEFAULT (0) FOR [weight]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship_Info] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_Info_chargeDate_partialDelivery]  DEFAULT ('0') FOR [chargeDate_partialDelivery]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship_Info] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_Info_chargeDate_schedulingData]  DEFAULT ('') FOR [chargeDate_schedulingData]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship_Info] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_Info_chargeDate_schedulingHour]  DEFAULT ('') FOR [chargeDate_schedulingHour]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship_Info] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_Info_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship_Info] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_Info_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship_Info] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_Info_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship_Info] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_Info_usrinis]  DEFAULT ('') FOR [usrinis]

END


/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE 
object_id = OBJECT_ID(N'[dbo].[pfReq_Delivery_Ship_Info]') AND name = N'IX_pfReq_Delivery_Ship_Info_pfCustomsDataStamp')
CREATE NONCLUSTERED INDEX [IX_pfReq_Delivery_Ship_Info_pfCustomsDataStamp] ON [dbo].[pfReq_Delivery_Ship_Info]
(
    [pfCustomsDataStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
------------------------------------------------------------------------------------------------------------------------------------------

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'pfReq_ShipClose'))
BEGIN
	
	CREATE TABLE [dbo].[pfReq_ShipClose](
		[stamp]							[varchar](36) NOT NULL,
		[shipCloseToken]				[varchar](36) NOT NULL,
		[firstObjectCode]				[varchar](35),
		[lastObjectCode]				[varchar](35),
		[subProductCode]				[varchar](10),
		[ousrdata]						[datetime],
		[ousrinis]						[varchar](30),
		[usrdata]						[datetime],
		[usrinis]						[varchar](30)
		
	 CONSTRAINT [PK_pfReq_ShipClose] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[pfReq_ShipClose] ADD  CONSTRAINT [DF_pfReq_ShipClose_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[pfReq_ShipClose] ADD  CONSTRAINT [DF_pfReq_ShipClose_shipCloseToken]  DEFAULT ('') FOR [shipCloseToken]

	ALTER TABLE [dbo].[pfReq_ShipClose] ADD  CONSTRAINT [DF_pfReq_ShipClose_firstObjectCode]  DEFAULT ('') FOR [firstObjectCode]

	ALTER TABLE [dbo].[pfReq_ShipClose] ADD  CONSTRAINT [DF_pfReq_ShipClose_lastObjectCode]  DEFAULT ('') FOR [lastObjectCode]

	ALTER TABLE [dbo].[pfReq_ShipClose] ADD  CONSTRAINT [DF_pfReq_ShipClose_subProductCode]  DEFAULT ('') FOR [subProductCode]

	ALTER TABLE [dbo].[pfReq_ShipClose] ADD  CONSTRAINT [DF_pfReq_ShipClose_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[pfReq_ShipClose] ADD  CONSTRAINT [DF_pfReq_ShipClose_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[pfReq_ShipClose] ADD  CONSTRAINT [DF_pfReq_ShipClose_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[pfReq_ShipClose] ADD  CONSTRAINT [DF_pfReq_ShipClose_usrinis]  DEFAULT ('') FOR [usrinis]

END
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE 
object_id = OBJECT_ID(N'[dbo].[pfReq_ShipClose]') AND name = N'IX_pfReq_ShipClose_shipCloseToken')
CREATE NONCLUSTERED INDEX [IX_pfReq_ShipClose_shipCloseToken] ON [dbo].[pfReq_ShipClose]
(
    [shipCloseToken] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


---------------------------------------------------------------------------

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'pfReq_Delivery_Ship_pipAuth'))
BEGIN
	
	CREATE TABLE [dbo].pfReq_Delivery_Ship_pipAuth(
		[stamp]						[varchar](36) NOT NULL,
		[pipAuthToken]				[varchar](36) NOT NULL,
		[PIPType]					[varchar](100),
		[ousrdata]					[datetime],
		[ousrinis]					[varchar](30),
		[usrdata]					[datetime],
		[usrinis]					[varchar](30)
		
	 CONSTRAINT [PK_pfReq_Delivery_Ship_pipAuth] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[pfReq_Delivery_Ship_pipAuth] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_pipAuth_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship_pipAuth] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_pipAuth_pipAuthToken]  DEFAULT ('') FOR [pipAuthToken]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship_pipAuth] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_pipAuth_PIPType]  DEFAULT ('') FOR [PIPType]
	
	ALTER TABLE [dbo].[pfReq_Delivery_Ship_pipAuth] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_pipAuth_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship_pipAuth] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_pipAuth_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship_pipAuth] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_pipAuth_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[pfReq_Delivery_Ship_pipAuth] ADD  CONSTRAINT [DF_pfReq_Delivery_Ship_pipAuth_usrinis]  DEFAULT ('') FOR [usrinis]
END
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE 
object_id = OBJECT_ID(N'[dbo].[pfReq_Delivery_Ship_pipAuth]') AND name = N'IX_pfReq_Delivery_Ship_pipAuth_pipAuthToken')
CREATE NONCLUSTERED INDEX [IX_pfReq_Delivery_Ship_pipAuth_pipAuthToken] ON [dbo].[pfReq_Delivery_Ship_pipAuth]
(
    [pipAuthToken] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



----------------------------------------------------------------------------------------------------------------------------------------
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'pfSpecialService'))
BEGIN
	
	CREATE TABLE [dbo].pfSpecialService(
		[stamp]								[varchar](36) NOT NULL,
		[pfSpecialServicesToken]			[varchar](36) NOT NULL,
		[certainDate]						[datetime],
		[shipperInstructions]				[varchar](60),
		[pfSpecialService_deliveryPDStamp]	[varchar](50) NOT NULL,
		[pfMultHomeDeliveryStamp]			[varchar](36) NOT NULL,
		[pfReturnAuthStamp]					[varchar](36) NOT NULL,
		[pfSpecialServiceType]				[varchar](100),
		[pfTimeWindowDataStamp]				[varchar](36) NOT NULL,
		[value]								[decimal],
		[ousrdata]							[datetime],
		[ousrinis]							[varchar](30),
		[usrdata]							[datetime],
		[usrinis]							[varchar](30)
		
	 CONSTRAINT [PK_pfSpecialService] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[pfSpecialService] ADD  CONSTRAINT [DF_pfSpecialService_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[pfSpecialService] ADD  CONSTRAINT [DF_pfSpecialService_pfSpecialServicesToken]  DEFAULT ('') FOR [pfSpecialServicesToken]

	ALTER TABLE [dbo].[pfSpecialService] ADD  CONSTRAINT [DF_pfSpecialService_certainDate]  DEFAULT ('') FOR [certainDate]

	ALTER TABLE [dbo].[pfSpecialService] ADD  CONSTRAINT [DF_pfSpecialService_shipperInstructions]  DEFAULT ('') FOR [shipperInstructions]

	ALTER TABLE [dbo].[pfSpecialService] ADD  CONSTRAINT [DF_pfSpecialService_pfSpecialService_deliveryPDStamp]  DEFAULT ('') FOR [pfSpecialService_deliveryPDStamp]

	ALTER TABLE [dbo].[pfSpecialService] ADD  CONSTRAINT [DF_pfSpecialService_pfMultHomeDeliveryStamp]  DEFAULT ('') FOR [pfMultHomeDeliveryStamp]

	ALTER TABLE [dbo].[pfSpecialService] ADD  CONSTRAINT [DF_pfSpecialService_pfReturnAuthStamp]  DEFAULT ('') FOR [pfReturnAuthStamp]

	ALTER TABLE [dbo].[pfSpecialService] ADD  CONSTRAINT [DF_pfSpecialService_pfSpecialServiceType]  DEFAULT ('') FOR [pfSpecialServiceType]

	ALTER TABLE [dbo].[pfSpecialService] ADD  CONSTRAINT [DF_pfSpecialService_pfTimeWindowDataStamp]  DEFAULT ('') FOR [pfTimeWindowDataStamp]

	ALTER TABLE [dbo].[pfSpecialService] ADD  CONSTRAINT [DF_pfSpecialService_value]  DEFAULT (0) FOR [value]
	
	ALTER TABLE [dbo].[pfSpecialService] ADD  CONSTRAINT [DF_pfSpecialService_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[pfSpecialService] ADD  CONSTRAINT [DF_pfSpecialService_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[pfSpecialService] ADD  CONSTRAINT [DF_pfSpecialService_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[pfSpecialService] ADD  CONSTRAINT [DF_pfSpecialService_usrinis]  DEFAULT ('') FOR [usrinis]
END
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE 
object_id = OBJECT_ID(N'[dbo].[pfSpecialService]') AND name = N'IX_pfSpecialService_pfSpecialServicesToken')
CREATE NONCLUSTERED INDEX [IX_pfSpecialService_pfSpecialServicesToken] ON [dbo].[pfSpecialService]
(
    [pfSpecialServicesToken] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE 
object_id = OBJECT_ID(N'[dbo].[pfSpecialService]') AND name = N'IX_pfSpecialService_pfSpecialService_deliveryPDStamp')
CREATE NONCLUSTERED INDEX [IX_pfSpecialService_pfSpecialService_deliveryPDStamp] ON [dbo].[pfSpecialService]
(
    [pfSpecialService_deliveryPDStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE 
object_id = OBJECT_ID(N'[dbo].[pfSpecialService]') AND name = N'IX_pfSpecialService_multipleHomeDeliveryStamp')
CREATE NONCLUSTERED INDEX [IX_pfSpecialService_multipleHomeDeliveryStamp] ON [dbo].[pfSpecialService]
(
    [pfMultHomeDeliveryStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE 
object_id = OBJECT_ID(N'[dbo].[pfSpecialService]') AND name = N'IX_pfSpecialService_pfReturnAuthStamp')
CREATE NONCLUSTERED INDEX [IX_pfSpecialService_pfReturnAuthStamp] ON [dbo].[pfSpecialService]
(
    [pfReturnAuthStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE 
object_id = OBJECT_ID(N'[dbo].[pfSpecialService]') AND name = N'IX_pfSpecialService_pfTimeWindowDataStamp')
CREATE NONCLUSTERED INDEX [IX_pfSpecialService_pfTimeWindowDataStamp] ON [dbo].[pfSpecialService]
(
    [pfTimeWindowDataStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

-----------------------------------------------------------------------------------------------------------------------------------------------------
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'pfSpecialService_deliveryPD'))
BEGIN
	
	CREATE TABLE [dbo].pfSpecialService_deliveryPD(
		[stamp]							[varchar](36) NOT NULL,
		[code]							[varchar](50),
		[name]							[varchar](50),
		[type]							[varchar](100),
		[ousrdata]						[datetime],
		[ousrinis]						[varchar](30),
		[usrdata]						[datetime],
		[usrinis]						[varchar](30)
		
	 CONSTRAINT [PK_pfSpecialService_deliveryPD] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[pfSpecialService_deliveryPD] ADD  CONSTRAINT [DF_pfSpecialService_deliveryPD_stamp]  DEFAULT ('') FOR [stamp]


	ALTER TABLE [dbo].[pfSpecialService_deliveryPD] ADD  CONSTRAINT [DF_pfSpecialService_deliveryPD_code]  DEFAULT ('') FOR [code]

	ALTER TABLE [dbo].[pfSpecialService_deliveryPD] ADD  CONSTRAINT [DF_pfSpecialService_deliveryPD_name]  DEFAULT ('') FOR [name]

	ALTER TABLE [dbo].[pfSpecialService_deliveryPD] ADD  CONSTRAINT [DF_pfSpecialService_deliveryPD_type]  DEFAULT ('') FOR [type]

	ALTER TABLE [dbo].[pfSpecialService_deliveryPD] ADD  CONSTRAINT [DF_pfSpecialService_deliveryPD_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[pfSpecialService_deliveryPD] ADD  CONSTRAINT [DF_pfSpecialService_deliveryPD_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[pfSpecialService_deliveryPD] ADD  CONSTRAINT [DF_pfSpecialService_deliveryPD_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[pfSpecialService_deliveryPD] ADD  CONSTRAINT [DF_pfSpecialService_deliveryPD_usrinis]  DEFAULT ('') FOR [usrinis]
END
------------------------------------------------------------------------------------------------------------------------------------------------------

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'pfMultHomeDelivery'))
BEGIN
	
	CREATE TABLE [dbo].pfMultHomeDelivery(
		[stamp]							[varchar](36) NOT NULL,
		[attemptsNumber]				[int],
		[inNonDeliveryCase]				[varchar](50),
		[secondDeliveryAddress]			[varchar](50),
		[pfAddressStamp]				[varchar](36) NOT NULL,
		[secondDeliveryPoint]			[varchar](50),
		[deliveryPointStamp]			[varchar](36) NOT NULL,
		[ousrdata]						[datetime],
		[ousrinis]						[varchar](30),
		[usrdata]						[datetime],
		[usrinis]						[varchar](30)
		
	 CONSTRAINT [PK_pfMultHomeDelivery] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[pfMultHomeDelivery] ADD  CONSTRAINT [DF_pfMultHomeDelivery_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[pfMultHomeDelivery] ADD  CONSTRAINT [DF_pfMultHomeDelivery_attemptsNumber]  DEFAULT (0) FOR [attemptsNumber]

	ALTER TABLE [dbo].[pfMultHomeDelivery] ADD  CONSTRAINT [DF_pfMultHomeDelivery_inNonDeliveryCase]  DEFAULT ('') FOR [inNonDeliveryCase]

	ALTER TABLE [dbo].[pfMultHomeDelivery] ADD  CONSTRAINT [DF_pfMultHomeDelivery_secondDeliveryAddress]  DEFAULT ('') FOR [secondDeliveryAddress]

	ALTER TABLE [dbo].[pfMultHomeDelivery] ADD  CONSTRAINT [DF_pfMultHomeDelivery_pfAddressStamp]  DEFAULT ('') FOR [pfAddressStamp]

	ALTER TABLE [dbo].[pfMultHomeDelivery] ADD  CONSTRAINT [DF_pfMultHomeDelivery_secondDeliveryPoint]  DEFAULT ('') FOR [secondDeliveryPoint]

	ALTER TABLE [dbo].[pfMultHomeDelivery] ADD  CONSTRAINT [DF_pfMultHomeDelivery_deliveryPointStamp]  DEFAULT ('') FOR [deliveryPointStamp]

	ALTER TABLE [dbo].[pfMultHomeDelivery] ADD  CONSTRAINT [DF_pfMultHomeDelivery_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[pfMultHomeDelivery] ADD  CONSTRAINT [DF_pfMultHomeDelivery_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[pfMultHomeDelivery] ADD  CONSTRAINT [DF_pfMultHomeDelivery_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[pfMultHomeDelivery] ADD  CONSTRAINT [DF_pfMultHomeDelivery_usrinis]  DEFAULT ('') FOR [usrinis]
END
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE 
object_id = OBJECT_ID(N'[dbo].[pfMultHomeDelivery]') AND name = N'IX_pfMultHomeDelivery_pfAddressStamp')
CREATE NONCLUSTERED INDEX [IX_pfMultHomeDelivery_pfAddressStamp] ON [dbo].[pfMultHomeDelivery]
(
    [pfAddressStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE 
object_id = OBJECT_ID(N'[dbo].[pfMultHomeDelivery]') AND name = N'IX_pfMultHomeDelivery_deliveryPointStamp')
CREATE NONCLUSTERED INDEX [IX_pfMultHomeDelivery_deliveryPointStamp] ON [dbo].[pfMultHomeDelivery]
(
    [deliveryPointStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------------------------------------------------------

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'pfReturnAuth'))
BEGIN
	
	CREATE TABLE [dbo].pfReturnAuth(
		[stamp]							[varchar](36) NOT NULL,
		[pfAddressStamp]				[varchar](36) NOT NULL,
		[pfAddress]						[varchar](50),
		[productCode]					[varchar](10),
		[validationDate]				[datetime],
		[ousrdata]						[datetime],
		[ousrinis]						[varchar](30),
		[usrdata]						[datetime],
		[usrinis]						[varchar](30)
		
	 CONSTRAINT [PK_pfReturnAuth] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[pfReturnAuth] ADD  CONSTRAINT [DF_pfReturnAuth_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[pfReturnAuth] ADD  CONSTRAINT [DF_pfReturnAuth_pfAddressStamp]  DEFAULT ('') FOR [pfAddressStamp]

	ALTER TABLE [dbo].[pfReturnAuth] ADD  CONSTRAINT [DF_pfReturnAuth_pfAddress]  DEFAULT ('') FOR [pfAddress]

	ALTER TABLE [dbo].[pfReturnAuth] ADD  CONSTRAINT [DF_pfReturnAuth_productCode]  DEFAULT ('') FOR [productCode]

	ALTER TABLE [dbo].[pfReturnAuth] ADD  CONSTRAINT [DF_pfReturnAuth_validationDate]  DEFAULT ('') FOR [validationDate]
	
	ALTER TABLE [dbo].[pfReturnAuth] ADD  CONSTRAINT [DF_pfReturnAuth_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[pfReturnAuth] ADD  CONSTRAINT [DF_pfReturnAuth_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[pfReturnAuth] ADD  CONSTRAINT [DF_pfReturnAuth_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[pfReturnAuth] ADD  CONSTRAINT [DF_pfReturnAuth_usrinis]  DEFAULT ('') FOR [usrinis]
END
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE 
object_id = OBJECT_ID(N'[dbo].[pfReturnAuth]') AND name = N'IX_pfReturnAuth_pfAddressStamp')
CREATE NONCLUSTERED INDEX [IX_pfReturnAuth_pfAddressStamp] ON [dbo].[pfReturnAuth]
(
    [pfAddressStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


--------------------------------------------------------------------------------------------------------------------------------------------------------
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'pfTimeWindowData'))
BEGIN
	
	CREATE TABLE [dbo].pfTimeWindowData(
		[stamp]							[varchar](36) NOT NULL,
		[deliveryDate]					[datetime],
		[timeWindow]					[varchar](50),
		[ousrdata]						[datetime],
		[ousrinis]						[varchar](30),
		[usrdata]						[datetime],
		[usrinis]						[varchar](30)
		
	 CONSTRAINT [PK_pfTimeWindowData] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[pfTimeWindowData] ADD  CONSTRAINT [DF_pfTimeWindowData_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[pfTimeWindowData] ADD  CONSTRAINT [DF_pfTimeWindowData_deliveryDate]  DEFAULT ('') FOR [deliveryDate]

	ALTER TABLE [dbo].[pfTimeWindowData] ADD  CONSTRAINT [DF_pfTimeWindowData_timeWindow]  DEFAULT ('') FOR [timeWindow]
	
	ALTER TABLE [dbo].[pfTimeWindowData] ADD  CONSTRAINT [DF_pfTimeWindowData_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[pfTimeWindowData] ADD  CONSTRAINT [DF_pfTimeWindowData_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[pfTimeWindowData] ADD  CONSTRAINT [DF_pfTimeWindowData_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[pfTimeWindowData] ADD  CONSTRAINT [DF_pfTimeWindowData_usrinis]  DEFAULT ('') FOR [usrinis]
END

-----------------------------------------------------------------------------------------------------------------------------------------------------

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'pfAddress'))
BEGIN
	
	CREATE TABLE [dbo].[pfAddress](
		[stamp]							[varchar](36) NOT NULL,
		[no]							[numeric](10,0),
		[estab]							[numeric](3,0),
		[address]						[varchar](100),
		[city]							[varchar](50),
		[contactName]					[varchar](60),
		[country]						[varchar](3),
		[door]							[varchar](5),
		[email]							[varchar](200),
		[floor]							[varchar](5),
		[mobilePhone]					[varchar](20),
		[name]							[varchar](60),
		[nonPTZipCode]					[varchar](10),
		[nonPTZipCodeLocation]			[varchar](50),
		[ptZipCode3]					[int],
		[ptZipCode4]					[int],
		[ptZipCodeLocation]				[varchar](50),
		[phone]							[varchar](20),
		[type]							[varchar](36) NOT NULL,							
		[ousrdata]						[datetime],
		[ousrinis]						[varchar](30),
		[usrdata]						[datetime],
		[usrinis]						[varchar](30)
		
	 CONSTRAINT [PK_pfAddress] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[pfAddress] ADD  CONSTRAINT [DF_pfAddress_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[pfAddress] ADD  CONSTRAINT [DF_pfAddress_no]  DEFAULT (0) FOR [no]

	ALTER TABLE [dbo].[pfAddress] ADD  CONSTRAINT [DF_pfAddress_estab]  DEFAULT (0) FOR [estab]

	ALTER TABLE [dbo].[pfAddress] ADD  CONSTRAINT [DF_pfAddress_address]  DEFAULT ('') FOR [address]

	ALTER TABLE [dbo].[pfAddress] ADD  CONSTRAINT [DF_pfAddress_city]  DEFAULT ('') FOR [city]

	ALTER TABLE [dbo].[pfAddress] ADD  CONSTRAINT [DF_pfAddress_contactName]  DEFAULT ('') FOR [contactName]

	ALTER TABLE [dbo].[pfAddress] ADD  CONSTRAINT [DF_pfAddress_country]  DEFAULT ('') FOR [country]

	ALTER TABLE [dbo].[pfAddress] ADD  CONSTRAINT [DF_pfAddress_door]  DEFAULT ('') FOR [door]

	ALTER TABLE [dbo].[pfAddress] ADD  CONSTRAINT [DF_pfAddress_email]  DEFAULT ('') FOR [email]

	ALTER TABLE [dbo].[pfAddress] ADD  CONSTRAINT [DF_pfAddress_floor]  DEFAULT ('') FOR [floor]

	ALTER TABLE [dbo].[pfAddress] ADD  CONSTRAINT [DF_pfAddress_mobilePhone]  DEFAULT ('') FOR [mobilePhone]

	ALTER TABLE [dbo].[pfAddress] ADD  CONSTRAINT [DF_pfAddress_name]  DEFAULT ('') FOR [name]

	ALTER TABLE [dbo].[pfAddress] ADD  CONSTRAINT [DF_pfAddress_nonPTZipCode]  DEFAULT ('') FOR [nonPTZipCode]

	ALTER TABLE [dbo].[pfAddress] ADD  CONSTRAINT [DF_pfAddress_nonPTZipCodeLocation]  DEFAULT ('') FOR [nonPTZipCodeLocation]

	ALTER TABLE [dbo].[pfAddress] ADD  CONSTRAINT [DF_pfAddress_ptZipCode3]  DEFAULT (0) FOR [ptZipCode3]

	ALTER TABLE [dbo].[pfAddress] ADD  CONSTRAINT [DF_pfAddress_ptZipCode4]  DEFAULT (0) FOR [ptZipCode4]

	ALTER TABLE [dbo].[pfAddress] ADD  CONSTRAINT [DF_pfAddress_ptZipCodeLocation]  DEFAULT ('') FOR [ptZipCodeLocation]

	ALTER TABLE [dbo].[pfAddress] ADD  CONSTRAINT [DF_pfAddress_phone]  DEFAULT ('') FOR [phone]

	ALTER TABLE [dbo].[pfAddress] ADD  CONSTRAINT [DF_pfAddress_type]  DEFAULT ('') FOR [type]

	ALTER TABLE [dbo].[pfAddress] ADD  CONSTRAINT [DF_pfAddress_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[pfAddress] ADD  CONSTRAINT [DF_pfAddress_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[pfAddress] ADD  CONSTRAINT [DF_pfAddress_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[pfAddress] ADD  CONSTRAINT [DF_pfAddress_usrinis]  DEFAULT ('') FOR [usrinis]
END

--------------------------------------------------------------------------------------------------------------------------------------------------



--------------------------------------------------------------------------------------------------------------------------------------------------

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'pfCustomsData'))
BEGIN
	
	CREATE TABLE [dbo].[pfCustomsData](
		[stamp]							[varchar](36) NOT NULL,
		[clientCustomsCode]				[varchar](10),
		[comercialInvoice]				[varchar](50),
		[comments]						[varchar](254),
		[pfCustomsItemsInfoToken]		[varchar](36) NOT NULL,
		[customsTotalItems]				[varchar](50),
		[customsTotalValue]				[varchar](50),
		[customsTotalWeight]			[varchar](50),
		[exportLicence]					[varchar](50),
		[height]						[varchar](50),
		[insurancePremium]				[varchar](50),
		[insuranceValue]				[varchar](50),
		[length]						[varchar](50),
		[nonDeliveryCase]				[varchar](50), 
		[originCertificateNumber]		[varchar](50),
		[receiverTIN]					[varchar](50),
		[sachetDocumentation]			[bit],
		[senderEmail]					[varchar](200),
		[serviceValue]					[varchar](50),
		[VATExportDeclaration]			[bit],
		[VATRate]						[bit],
		[width]							[varchar](50),
		[ousrdata]						[datetime],
		[ousrinis]						[varchar](30),
		[usrdata]						[datetime],
		[usrinis]						[varchar](30)
		
	 CONSTRAINT [PK_pfCustomsData] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_clientCustomsCode]  DEFAULT ('') FOR [clientCustomsCode]

	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_comercialInvoice]  DEFAULT ('') FOR [comercialInvoice]

	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_comments]  DEFAULT ('') FOR [comments]

	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_pfCustomsInfoToken]  DEFAULT ('') FOR [pfCustomsInfoToken]

	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_customsTotalItems]  DEFAULT ('') FOR [customsTotalItems]

	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_customsTotalValue]  DEFAULT ('') FOR [customsTotalValue]

	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_customsTotalWeight]  DEFAULT ('') FOR [customsTotalWeight]

	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_exportLicence]  DEFAULT ('') FOR [exportLicence]

	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_height]  DEFAULT ('') FOR [height]

	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_insurancePremium]  DEFAULT ('') FOR [insurancePremium]

	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_insuranceValue]  DEFAULT ('') FOR [insuranceValue]

	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_length]  DEFAULT ('') FOR [length]
		
	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_nonDeliveryCase]  DEFAULT ('') FOR [nonDeliveryCase]

	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_originCertificateNumber]  DEFAULT ('') FOR [originCertificateNumber]

	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_receiverTIN]  DEFAULT ('') FOR [receiverTIN]

	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_sachetDocumentation]  DEFAULT (0) FOR [sachetDocumentation]

	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_senderEmail]  DEFAULT ('') FOR [senderEmail]

	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_serviceValue]  DEFAULT ('') FOR [serviceValue]

	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_VATExportDeclaration]  DEFAULT (0) FOR [VATExportDeclaration]

	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_VATRate]  DEFAULT (0) FOR [VATRate]

	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_width]  DEFAULT ('') FOR [width]

	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[pfCustomsData] ADD  CONSTRAINT [DF_pfCustomsData_usrinis]  DEFAULT ('') FOR [usrinis]

END
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE 
object_id = OBJECT_ID(N'[dbo].[pfCustomsData]') AND name = N'IX_pfCustomsData_pfCustomsItemsInfoToken')
CREATE NONCLUSTERED INDEX [IX_pfCustomsData_pfCustomsItemsInfoToken] ON [dbo].[pfCustomsData]
(
    [pfCustomsItemsInfoToken] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
----------------------------------------------------------------------------------------------------------------------------------------------------

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'pfCustomsInfo'))
BEGIN
	
	CREATE TABLE [dbo].[pfCustomsInfo](
		[stamp]							[varchar](36) NOT NULL,
		[pfCustomsInfoToken]			[varchar](36) NOT NULL,
		[currency]						[varchar](50),
		[detail]						[varchar](50),
		[harmonizedCode]				[varchar](50),
		[itemNumber]					[int],
		[originCountry]					[varchar](50),
		[quantity]						[int],
		[value]							[decimal],
		[weight]						[decimal],
		[ousrdata]						[datetime],
		[ousrinis]						[varchar](30),
		[usrdata]						[datetime],
		[usrinis]						[varchar](30)
		
	 CONSTRAINT [PK_pfCustomsInfo] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[pfCustomsInfo] ADD  CONSTRAINT [DF_pfCustomsInfo_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[pfCustomsInfo] ADD  CONSTRAINT [DF_pfCustomsInfo_pfCustomsInfoToken]  DEFAULT ('') FOR [pfCustomsInfoToken]

	ALTER TABLE [dbo].[pfCustomsInfo] ADD  CONSTRAINT [DF_pfCustomsInfo_currency]  DEFAULT ('') FOR [currency]

	ALTER TABLE [dbo].[pfCustomsInfo] ADD  CONSTRAINT [DF_pfCustomsInfo_detail]  DEFAULT ('') FOR [detail]

	ALTER TABLE [dbo].[pfCustomsInfo] ADD  CONSTRAINT [DF_pfCustomsInfo_harmonizedCode]  DEFAULT ('') FOR [harmonizedCode]

	ALTER TABLE [dbo].[pfCustomsInfo] ADD  CONSTRAINT [DF_pfCustomsInfo_itemNumber]  DEFAULT (0) FOR [itemNumber]

	ALTER TABLE [dbo].[pfCustomsInfo] ADD  CONSTRAINT [DF_pfCustomsInfo_originCountry]  DEFAULT ('') FOR [originCountry]

	ALTER TABLE [dbo].[pfCustomsInfo] ADD  CONSTRAINT [DF_pfCustomsInfo_quantity]  DEFAULT (0) FOR [quantity]

	ALTER TABLE [dbo].[pfCustomsInfo] ADD  CONSTRAINT [DF_pfCustomsInfo_value]  DEFAULT (0) FOR [value]

	ALTER TABLE [dbo].[pfCustomsInfo] ADD  CONSTRAINT [DF_pfCustomsInfo_weight]  DEFAULT (0) FOR [weight]

	ALTER TABLE [dbo].[pfCustomsInfo] ADD  CONSTRAINT [DF_pfCustomsInfo_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[pfCustomsInfo] ADD  CONSTRAINT [DF_pfCustomsInfo_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[pfCustomsInfo] ADD  CONSTRAINT [DF_pfCustomsInfo_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[pfCustomsInfo] ADD  CONSTRAINT [DF_pfCustomsInfo_usrinis]  DEFAULT ('') FOR [usrinis]

END
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE 
object_id = OBJECT_ID(N'[dbo].[pfCustomsInfo]') AND name = N'IX_pfCustomsInfo_pfCustomsInfoToken')
CREATE NONCLUSTERED INDEX [IX_pfCustomsInfo_pfCustomsInfoToken] ON [dbo].[pfCustomsInfo]
(
    [pfCustomsInfoToken] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


-----------------------------------------------------------------------------------------------------------------------------------------------------------

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'pfEMS'))
BEGIN
	
	CREATE TABLE [dbo].pfEMS(
		[stamp]						[varchar](36) NOT NULL,
		[pfEMSToken]				[varchar](36) NOT NULL,
		[object]					[varchar](254),
		[ousrdata]					[datetime],
		[ousrinis]					[varchar](30),
		[usrdata]					[datetime],
		[usrinis]					[varchar](30)
		
	 CONSTRAINT [PK_pfEMS] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[pfEMS] ADD  CONSTRAINT [DF_pfEMS_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[pfEMS] ADD  CONSTRAINT [DF_pfEMS_pfEMSToken]  DEFAULT ('') FOR [pfEMSToken]

	ALTER TABLE [dbo].[pfEMS] ADD  CONSTRAINT [DF_pfEMS_object]  DEFAULT ('') FOR [object]

	ALTER TABLE [dbo].[pfEMS] ADD  CONSTRAINT [DF_pfEMS_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[pfEMS] ADD  CONSTRAINT [DF_pfEMS_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[pfEMS] ADD  CONSTRAINT [DF_pfEMS_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[pfEMS] ADD  CONSTRAINT [DF_pfEMS_usrinis]  DEFAULT ('') FOR [usrinis]
END
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE 
object_id = OBJECT_ID(N'[dbo].[pfEMS]') AND name = N'IX_pfEMS_objectEMSToken')
CREATE NONCLUSTERED INDEX [IX_pfEMS_objectEMSToken] ON [dbo].[pfEMS]
(
    [pfEMSToken] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
-----------------------------------------------------------------------------------------------------------------------------------------------------------

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'addressTypeValue'))
BEGIN
	
	CREATE TABLE [dbo].addressTypeValue(
		[stamp]						[varchar](36) NOT NULL,
		[typeAddresToken]			[varchar](36) NOT NULL,	
		[value]						[int],
		[design]					[varchar](50),
		[obs]						[varchar](254),	
		[ousrdata]					[datetime],
		[ousrinis]					[varchar](30),
		[usrdata]					[datetime],
		[usrinis]					[varchar](30)
		
	 CONSTRAINT [PK_addressTypeValue] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[addressTypeValue] ADD  CONSTRAINT [DF_addressTypeValue_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[addressTypeValue] ADD  CONSTRAINT [DF_addressTypeValue_typeAddresToken]  DEFAULT ('') FOR [typeAddresToken]

	ALTER TABLE [dbo].[addressTypeValue] ADD  CONSTRAINT [DF_addressTypeValue_value]  DEFAULT (0) FOR [value]

	ALTER TABLE [dbo].[addressTypeValue] ADD  CONSTRAINT [DF_addressTypeValue_design]  DEFAULT ('') FOR [design]

	ALTER TABLE [dbo].[addressTypeValue] ADD  CONSTRAINT [DF_addressTypeValue_obs]  DEFAULT ('') FOR [obs]

	ALTER TABLE [dbo].[addressTypeValue] ADD  CONSTRAINT [DF_addressTypeValue_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[addressTypeValue] ADD  CONSTRAINT [DF_addressTypeValue_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[addressTypeValue] ADD  CONSTRAINT [DF_addressTypeValue_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[addressTypeValue] ADD  CONSTRAINT [DF_addressTypeValue_usrinis]  DEFAULT ('') FOR [usrinis]
END
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE 
object_id = OBJECT_ID(N'[dbo].[addressTypeValue]') AND name = N'IX_addressTypeValue_typeAddresToken')
CREATE NONCLUSTERED INDEX [IX_addressTypeValue_typeAddresToken] ON [dbo].[addressTypeValue]
(
    [typeAddresToken] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
-----------------------------------------------------------------------------------------------------------------------------------------------------------

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'exportTypeValues'))
BEGIN
	
	CREATE TABLE [dbo].exportTypeValues(
		[stamp]						[varchar](36) NOT NULL,
		[value]						[int],
		[design]					[varchar](50),
		[obs]						[varchar](254),	
		[ousrdata]					[datetime],
		[ousrinis]					[varchar](30),
		[usrdata]					[datetime],
		[usrinis]					[varchar](30)
		
	 CONSTRAINT [PK_exportTypeValues] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[exportTypeValues] ADD  CONSTRAINT [DF_exportTypeValues_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[exportTypeValues] ADD  CONSTRAINT [DF_exportTypeValues_value]  DEFAULT (0) FOR [value]

	ALTER TABLE [dbo].[exportTypeValues] ADD  CONSTRAINT [DF_exportTypeValues_design]  DEFAULT ('') FOR [design]

	ALTER TABLE [dbo].[exportTypeValues] ADD  CONSTRAINT [DF_exportTypeValues_obs]  DEFAULT ('') FOR [obs]

	ALTER TABLE [dbo].[exportTypeValues] ADD  CONSTRAINT [DF_exportTypeValues_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[exportTypeValues] ADD  CONSTRAINT [DF_exportTypeValues_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[exportTypeValues] ADD  CONSTRAINT [DF_exportTypeValues_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[exportTypeValues] ADD  CONSTRAINT [DF_exportTypeValues_usrinis]  DEFAULT ('') FOR [usrinis]
END

-----------------------------------------------------------------------------------------------------------------------------------------------------------
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'upuCodeValues'))
BEGIN
	
	CREATE TABLE [dbo].upuCodeValues(
		[stamp]						[varchar](36) NOT NULL,
		[value]						[int],
		[design]					[varchar](50),
		[obs]						[varchar](254),	
		[ousrdata]					[datetime],
		[ousrinis]					[varchar](30),
		[usrdata]					[datetime],
		[usrinis]					[varchar](30)
		
	 CONSTRAINT [PK_upuCodeValues] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[upuCodeValues] ADD  CONSTRAINT [DF_upuCodeValues_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[upuCodeValues] ADD  CONSTRAINT [DF_upuCodeValues_value]  DEFAULT (0) FOR [value]

	ALTER TABLE [dbo].[upuCodeValues] ADD  CONSTRAINT [DF_upuCodeValues_design]  DEFAULT ('') FOR [design]

	ALTER TABLE [dbo].[upuCodeValues] ADD  CONSTRAINT [DF_upuCodeValues_obs]  DEFAULT ('') FOR [obs]

	ALTER TABLE [dbo].[upuCodeValues] ADD  CONSTRAINT [DF_upuCodeValues_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[upuCodeValues] ADD  CONSTRAINT [DF_upuCodeValues_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[upuCodeValues] ADD  CONSTRAINT [DF_upuCodeValues_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[upuCodeValues] ADD  CONSTRAINT [DF_upuCodeValues_usrinis]  DEFAULT ('') FOR [usrinis]
END

-----------------------------------------------------------------------------------------------------------------------------------------------------------
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'nonDeliveryCaseValues'))
BEGIN
	
	CREATE TABLE [dbo].nonDeliveryCaseValues(
		[stamp]						[varchar](36) NOT NULL,
		[value]						[int],
		[design]					[varchar](50),
		[obs]						[varchar](254),	
		[ousrdata]					[datetime],
		[ousrinis]					[varchar](30),
		[usrdata]					[datetime],
		[usrinis]					[varchar](30)
		
	 CONSTRAINT [PK_nonDeliveryCaseValues] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[nonDeliveryCaseValues] ADD  CONSTRAINT [DF_nonDeliveryCaseValues_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[nonDeliveryCaseValues] ADD  CONSTRAINT [DF_nonDeliveryCaseValues_value]  DEFAULT (0) FOR [value]

	ALTER TABLE [dbo].[nonDeliveryCaseValues] ADD  CONSTRAINT [DF_nonDeliveryCaseValues_design]  DEFAULT ('') FOR [design]

	ALTER TABLE [dbo].[nonDeliveryCaseValues] ADD  CONSTRAINT [DF_nonDeliveryCaseValues_obs]  DEFAULT ('') FOR [obs]

	ALTER TABLE [dbo].[nonDeliveryCaseValues] ADD  CONSTRAINT [DF_nonDeliveryCaseValues_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[nonDeliveryCaseValues] ADD  CONSTRAINT [DF_nonDeliveryCaseValues_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[nonDeliveryCaseValues] ADD  CONSTRAINT [DF_nonDeliveryCaseValues_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[nonDeliveryCaseValues] ADD  CONSTRAINT [DF_nonDeliveryCaseValues_usrinis]  DEFAULT ('') FOR [usrinis]
END

-----------------------------------------------------------------------------------------------------------------------------------------------------------
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'specialServiceTypeValues'))
BEGIN
	
	CREATE TABLE [dbo].specialServiceTypeValues(
		[stamp]						[varchar](36) NOT NULL,
		[value]						[int],
		[design]					[varchar](50),
		[ousrdata]					[datetime],
		[ousrinis]					[varchar](30),
		[usrdata]					[datetime],
		[usrinis]					[varchar](30)
		
	 CONSTRAINT [PK_specialServiceTypeValues] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[specialServiceTypeValues] ADD  CONSTRAINT [DF_specialServiceTypeValues_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[specialServiceTypeValues] ADD  CONSTRAINT [DF_specialServiceTypeValues_value]  DEFAULT (0) FOR [value]

	ALTER TABLE [dbo].[specialServiceTypeValues] ADD  CONSTRAINT [DF_specialServiceTypeValues_design]  DEFAULT ('') FOR [design]

	ALTER TABLE [dbo].[specialServiceTypeValues] ADD  CONSTRAINT [DF_specialServiceTypeValues_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[specialServiceTypeValues] ADD  CONSTRAINT [DF_specialServiceTypeValues_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[specialServiceTypeValues] ADD  CONSTRAINT [DF_specialServiceTypeValues_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[specialServiceTypeValues] ADD  CONSTRAINT [DF_specialServiceTypeValues_usrinis]  DEFAULT ('') FOR [usrinis]
END

-----------------------------------------------------------------------------------------------------------------------------------------------------------
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'deliveryPointTypeValues'))
BEGIN
	
	CREATE TABLE [dbo].deliveryPointTypeValues(
		[stamp]						[varchar](36) NOT NULL,
		[value]						[int],
		[design]					[varchar](50),
		[obs]						[varchar](254),	
		[ousrdata]					[datetime],
		[ousrinis]					[varchar](30),
		[usrdata]					[datetime],
		[usrinis]					[varchar](30)
		
	 CONSTRAINT [PK_deliveryPointTypeValues] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[deliveryPointTypeValues] ADD  CONSTRAINT [DF_deliveryPointTypeValues_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[deliveryPointTypeValues] ADD  CONSTRAINT [DF_deliveryPointTypeValues_value]  DEFAULT (0) FOR [value]

	ALTER TABLE [dbo].[deliveryPointTypeValues] ADD  CONSTRAINT [DF_deliveryPointTypeValues_design]  DEFAULT ('') FOR [design]

	ALTER TABLE [dbo].[deliveryPointTypeValues] ADD  CONSTRAINT [DF_deliveryPointTypeValues_obs]  DEFAULT ('') FOR [obs]

	ALTER TABLE [dbo].[deliveryPointTypeValues] ADD  CONSTRAINT [DF_deliveryPointTypeValues_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[deliveryPointTypeValues] ADD  CONSTRAINT [DF_deliveryPointTypeValues_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[deliveryPointTypeValues] ADD  CONSTRAINT [DF_deliveryPointTypeValues_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[deliveryPointTypeValues] ADD  CONSTRAINT [DF_deliveryPointTypeValues_usrinis]  DEFAULT ('') FOR [usrinis]
END

-----------------------------------------------------------------------------------------------------------------------------------------------------------
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'inNonDeliveryCaseTypeValues'))
BEGIN
	
	CREATE TABLE [dbo].inNonDeliveryCaseTypeValues(
		[stamp]						[varchar](36) NOT NULL,
		[value]						[int],
		[design]					[varchar](50),
		[obs]						[varchar](254),	
		[ousrdata]					[datetime],
		[ousrinis]					[varchar](30),
		[usrdata]					[datetime],
		[usrinis]					[varchar](30)
		
	 CONSTRAINT [PK_inNonDeliveryCaseTypeValues] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[inNonDeliveryCaseTypeValues] ADD  CONSTRAINT [DF_inNonDeliveryCaseTypeValues_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[inNonDeliveryCaseTypeValues] ADD  CONSTRAINT [DF_inNonDeliveryCaseTypeValues_value]  DEFAULT (0) FOR [value]

	ALTER TABLE [dbo].[inNonDeliveryCaseTypeValues] ADD  CONSTRAINT [DF_inNonDeliveryCaseTypeValues_design]  DEFAULT ('') FOR [design]

	ALTER TABLE [dbo].[inNonDeliveryCaseTypeValues] ADD  CONSTRAINT [DF_inNonDeliveryCaseTypeValues_obs]  DEFAULT ('') FOR [obs]

	ALTER TABLE [dbo].[inNonDeliveryCaseTypeValues] ADD  CONSTRAINT [DF_inNonDeliveryCaseTypeValues_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[inNonDeliveryCaseTypeValues] ADD  CONSTRAINT [DF_inNonDeliveryCaseTypeValues_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[inNonDeliveryCaseTypeValues] ADD  CONSTRAINT [DF_inNonDeliveryCaseTypeValues_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[inNonDeliveryCaseTypeValues] ADD  CONSTRAINT [DF_inNonDeliveryCaseTypeValues_usrinis]  DEFAULT ('') FOR [usrinis]
END

-----------------------------------------------------------------------------------------------------------------------------------------------------------
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'timeWindowValues'))
BEGIN
	
	CREATE TABLE [dbo].timeWindowValues(
		[stamp]						[varchar](36) NOT NULL,
		[value]						[int],
		[design]					[varchar](50),
		[obs]						[varchar](254),	
		[ousrdata]					[datetime],
		[ousrinis]					[varchar](30),
		[usrdata]					[datetime],
		[usrinis]					[varchar](30)
		
	 CONSTRAINT [PK_timeWindowValues] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[timeWindowValues] ADD  CONSTRAINT [DF_timeWindowValues_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[timeWindowValues] ADD  CONSTRAINT [DF_timeWindowValues_value]  DEFAULT (0) FOR [value]

	ALTER TABLE [dbo].[timeWindowValues] ADD  CONSTRAINT [DF_timeWindowValues_design]  DEFAULT ('') FOR [design]

	ALTER TABLE [dbo].[timeWindowValues] ADD  CONSTRAINT [DF_timeWindowValues_obs]  DEFAULT ('') FOR [obs]

	ALTER TABLE [dbo].[timeWindowValues] ADD  CONSTRAINT [DF_timeWindowValues_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[timeWindowValues] ADD  CONSTRAINT [DF_timeWindowValues_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[timeWindowValues] ADD  CONSTRAINT [DF_timeWindowValues_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[timeWindowValues] ADD  CONSTRAINT [DF_timeWindowValues_usrinis]  DEFAULT ('') FOR [usrinis]
END

-----------------------------------------------------------------------------------------------------------------------------------------------------------
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'pipAuthorizationValues'))
BEGIN
	
	CREATE TABLE [dbo].pipAuthorizationValues(
		[stamp]						[varchar](36) NOT NULL,
		[value]						[int],
		[design]					[varchar](50),
		[obs]						[varchar](254),	
		[ousrdata]					[datetime],
		[ousrinis]					[varchar](30),
		[usrdata]					[datetime],
		[usrinis]					[varchar](30)
		
	 CONSTRAINT [PK_pipAuthorizationValues] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[pipAuthorizationValues] ADD  CONSTRAINT [DF_pipAuthorizationValues_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[pipAuthorizationValues] ADD  CONSTRAINT [DF_pipAuthorizationValues_value]  DEFAULT (0) FOR [value]

	ALTER TABLE [dbo].[pipAuthorizationValues] ADD  CONSTRAINT [DF_pipAuthorizationValues_design]  DEFAULT ('') FOR [design]

	ALTER TABLE [dbo].[pipAuthorizationValues] ADD  CONSTRAINT [DF_pipAuthorizationValues_obs]  DEFAULT ('') FOR [obs]

	ALTER TABLE [dbo].[pipAuthorizationValues] ADD  CONSTRAINT [DF_pipAuthorizationValues_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[pipAuthorizationValues] ADD  CONSTRAINT [DF_pipAuthorizationValues_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[pipAuthorizationValues] ADD  CONSTRAINT [DF_pipAuthorizationValues_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[pipAuthorizationValues] ADD  CONSTRAINT [DF_pipAuthorizationValues_usrinis]  DEFAULT ('') FOR [usrinis]
END



-----------------------------------------------------------------------------------------------------------------------------------------------------------
IF (NOT EXISTS (SELECT * FROM addressTypeValue WHERE value = 1 and design='Sender'))
BEGIN
		
INSERT INTO [dbo].[addressTypeValue]
		([stamp]	
		,[value]				
		,[design]			
		,[obs]				
		,[ousrdata]			
		,[ousrinis]			
		,[usrdata]			
		,[usrinis])			
SELECT 
		left(newid(),36)		
		,1				
		,'Sender'			
		,'Endereço do remetente.'				
		,getdate()		
		,''		
		,getdate()			
		,''			
END

IF (NOT EXISTS (SELECT * FROM addressTypeValue WHERE value = 2 and design='Receiver'))
BEGIN
		
INSERT INTO [dbo].[addressTypeValue]
		([stamp]
		,[value]				
		,[design]			
		,[obs]				
		,[ousrdata]			
		,[ousrinis]			
		,[usrdata]			
		,[usrinis])			
SELECT 
		left(newid(),36)			
		,2				
		,'Receiver'			
		,'Endereço do destinatário.'				
		,getdate()		
		,''		
		,getdate()			
		,''			
END

IF (NOT EXISTS (SELECT * FROM addressTypeValue WHERE value = 3 and design='Return'))
BEGIN
		
INSERT INTO [dbo].[addressTypeValue]
		([stamp]
		,[value]				
		,[design]			
		,[obs]				
		,[ousrdata]			
		,[ousrinis]			
		,[usrdata]			
		,[usrinis])			
SELECT 
		left(newid(),36)			
		,3				
		,'Return'			
		,'Endereço de devolução.'				
		,getdate()		
		,''		
		,getdate()			
		,''			
END

IF (NOT EXISTS (SELECT * FROM addressTypeValue WHERE value = 4 and design='SecondReceiver'))
BEGIN
		
INSERT INTO [dbo].[addressTypeValue]
		([stamp]
		,[value]				
		,[design]			
		,[obs]				
		,[ousrdata]			
		,[ousrinis]			
		,[usrdata]			
		,[usrinis])			
SELECT 
		left(newid(),36)			
		,4				
		,'SecondReceiver'			
		,'Endereço de envio caso entrega no .'				
		,getdate()		
		,''		
		,getdate()			
		,''			
END

-----------------------------------------------------------------------------------------------------------------------------------------------------------
IF (NOT EXISTS (SELECT * FROM exportTypeValues WHERE value = 1 and design='Permanet'))
BEGIN
	INSERT INTO [dbo].[exportTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])
	SELECT	
		left(newid(),36)	
		,1		
		,'Permanet'	
		,'Permanente'		
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM exportTypeValues WHERE value = 2 and design='TemporaryPassiveImprovement'))
BEGIN
	INSERT INTO [dbo].[exportTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])
	SELECT	
		left(newid(),36)	
		,2		
		,'TemporaryPassiveImprovement'	
		,'Temporária (Aperfeiçoamento Passivo)'		
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM exportTypeValues WHERE value = 3 and design='TemporaryExhibition'))
BEGIN
	INSERT INTO [dbo].[exportTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])
	SELECT	
		left(newid(),36)		
		,3		
		,'TemporaryExhibition'	
		,'Temporária (Exposição)'		
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END
---------------------------------------------------------------------------------------------------------------------------------------------------------
IF (NOT EXISTS (SELECT * FROM upuCodeValues WHERE value = 32 and design='Samples'))
BEGIN
	INSERT INTO [dbo].[upuCodeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)	
		,32		
		,'Samples'	
		,'Amostras'		
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM upuCodeValues WHERE value = 91 and design='Documents'))
BEGIN
	INSERT INTO [dbo].[upuCodeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)	
		,91		
		,'Documents'	
		,'Documentos'		
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM upuCodeValues WHERE value = 11 and design='Goods'))
BEGIN
	INSERT INTO [dbo].[upuCodeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)	
		,11		
		,'Goods'	
		,'Mercadorias'		
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM upuCodeValues WHERE value = 999 and design='Others'))
BEGIN
	INSERT INTO [dbo].[upuCodeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)	
		,999		
		,'Others'	
		,'Outros'		
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM upuCodeValues WHERE value = 21 and design='Devolution'))
BEGIN
	INSERT INTO [dbo].[upuCodeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)	
		,21		
		,'Devolution'	
		,'Devolução'		
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

-------------------------------------------------------------------------------------------------------------------------------------------------------

IF (NOT EXISTS (SELECT * FROM nonDeliveryCaseValues WHERE value = 1 and design='GiveBack'))
BEGIN
	INSERT INTO [dbo].[nonDeliveryCaseValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])
	SELECT	
		left(newid(),36)
		,1		
		,'GiveBack'	
		,'Devolver'		
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM nonDeliveryCaseValues WHERE value = 2 and design='ToAbandon'))
BEGIN
	INSERT INTO [dbo].[nonDeliveryCaseValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])
	SELECT	
		left(newid(),36)
		,2		
		,'ToAbandon'	
		,'Abandonar'		
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END
------------------------------------------------------------------------------------------------------------------------------------------------------
IF (NOT EXISTS (SELECT * FROM specialServiceTypeValues WHERE value = 1 and design='PostalObject'))
BEGIN
	INSERT INTO [dbo].[specialServiceTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)		
		,1		
		,'PostalObject'	
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM specialServiceTypeValues WHERE value = 2 and design='AgainstReimbursement'))
BEGIN
	INSERT INTO [dbo].[specialServiceTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)		
		,2		
		,'AgainstReimbursement'	
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM specialServiceTypeValues WHERE value = 3 and design='NominativeCheck'))
BEGIN
	INSERT INTO [dbo].[specialServiceTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)		
		,3		
		,'NominativeCheck'	
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM specialServiceTypeValues WHERE value = 4 and design='Saturday'))
BEGIN
	INSERT INTO [dbo].[specialServiceTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)		
		,4		
		,'Saturday'	
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM specialServiceTypeValues WHERE value = 5 and design='ReturnDocumentSigned'))
BEGIN
	INSERT INTO [dbo].[specialServiceTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)		
		,5		
		,'ReturnDocumentSigned'	
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM specialServiceTypeValues WHERE value = 6 and design='SpecialInsurance'))
BEGIN
	INSERT INTO [dbo].[specialServiceTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)		
		,6	
		,'SpecialInsurance'	
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM specialServiceTypeValues WHERE value = 7 and design='Fragil'))
BEGIN
	INSERT INTO [dbo].[specialServiceTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)		
		,7	
		,'Fragil'	
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM specialServiceTypeValues WHERE value = 9 and design='Back'))
BEGIN
	INSERT INTO [dbo].[specialServiceTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)		
		,9	
		,'Back'	
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM specialServiceTypeValues WHERE value = 12 and design='SecondScheduledDelivery'))
BEGIN
	INSERT INTO [dbo].[specialServiceTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)		
		,12	
		,'SecondScheduledDelivery'	
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM specialServiceTypeValues WHERE value = 14 and design='SMS'))
BEGIN
	INSERT INTO [dbo].[specialServiceTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)		
		,14	
		,'SMS'	
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM specialServiceTypeValues WHERE value = 18 and design='DeliveryPoint'))
BEGIN
	INSERT INTO [dbo].[specialServiceTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)		
		,18
		,'DeliveryPoint'	
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM specialServiceTypeValues WHERE value = 20 and design='AuthorizeReturn'))
BEGIN
	INSERT INTO [dbo].[specialServiceTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)		
		,20
		,'AuthorizeReturn'	
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM specialServiceTypeValues WHERE value = 21 and design='MultipleHomeDelivery'))
BEGIN
	INSERT INTO [dbo].[specialServiceTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)		
		,21
		,'MultipleHomeDelivery'	
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM specialServiceTypeValues WHERE value = 22 and design='TimeWindow'))
BEGIN
	INSERT INTO [dbo].[specialServiceTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)		
		,22
		,'TimeWindow'	
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM specialServiceTypeValues WHERE value = 23 and design='CertainDay'))
BEGIN
	INSERT INTO [dbo].[specialServiceTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)		
		,23
		,'CertainDay'	
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM specialServiceTypeValues WHERE value = 24 and design='PhoneContact'))
BEGIN
	INSERT INTO [dbo].[specialServiceTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)		
		,24
		,'PhoneContact'	
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM specialServiceTypeValues WHERE value = 37 and design='LiveTracking'))
BEGIN
	INSERT INTO [dbo].[specialServiceTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)		
		,37
		,'LiveTracking'	
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM specialServiceTypeValues WHERE value = 38 and design='ContactoAgendamento'))
BEGIN
	INSERT INTO [dbo].[specialServiceTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)		
		,38
		,'ContactoAgendamento'	
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM specialServiceTypeValues WHERE value = 39 and design='DeliveryAggregation'))
BEGIN
	INSERT INTO [dbo].[specialServiceTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)		
		,39
		,'DeliveryAggregation'	
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

--------------------------------------------------------------------------------------------------------------------------------------------------
IF (NOT EXISTS (SELECT * FROM deliveryPointTypeValues WHERE value = 0 and design='PostOffice'))
BEGIN
	INSERT INTO [dbo].[deliveryPointTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)	
		,0		
		,'PostOffice'	
		,'Utilizar obrigatoriamente quando Ponto de Entrega localiza-se em Portugal'		
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM deliveryPointTypeValues WHERE value = 1 and design='Shop'))
BEGIN
	INSERT INTO [dbo].[deliveryPointTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)	
		,1		
		,'Shop'	
		,'Utilizar apenas quando Ponto de Entrega é uma loja no estrangeiro'	
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM deliveryPointTypeValues WHERE value = 2 and design='ParcelLocker'))
BEGIN
	INSERT INTO [dbo].[deliveryPointTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)	
		,2		
		,'ParcelLocker'	
		,'Utilizar apenas quando se trata de um cacifo no estrangeiro'
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM deliveryPointTypeValues WHERE value = 3 and design='PostalCollectionFacility'))
BEGIN
	INSERT INTO [dbo].[deliveryPointTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)	
		,3		
		,'PostalCollectionFacility'	
		,'Utilizar apenas quando se trata de um edifício de uma entidade postal no estrangeiro'
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

-----------------------------------------------------------------------------------------------------------------------------------------------
IF (NOT EXISTS (SELECT * FROM inNonDeliveryCaseTypeValues WHERE value = 0 and design='SendToSender'))
BEGIN
	INSERT INTO [dbo].[inNonDeliveryCaseTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)	
		,0		
		,'SendToSender'	
		,'Encomenda é enviada para morada do remetente.'
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM inNonDeliveryCaseTypeValues WHERE value = 1 and design='SendToAddress'))
BEGIN
	INSERT INTO [dbo].[inNonDeliveryCaseTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)	
		,1		
		,'SendToAddress'	
		,'Encomenda é enviada para segunda morada especificada.'
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM inNonDeliveryCaseTypeValues WHERE value = 2 and design='SendToDeliveryPoint'))
BEGIN
	INSERT INTO [dbo].[inNonDeliveryCaseTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)	
		,2		
		,'SendToDeliveryPoint'	
		,'Encomenda é envaida para ponto de entrega especificado.'
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM inNonDeliveryCaseTypeValues WHERE value = 3 and design='PostOfficeNotiffied'))
BEGIN
	INSERT INTO [dbo].[inNonDeliveryCaseTypeValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)	
		,3		
		,'PostOfficeNotiffied'	
		,'Avisado na loja CTT.'
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

--------------------------------------------------------------------------------------------------------------------------------------------------
IF (NOT EXISTS (SELECT * FROM timeWindowValues WHERE value = 2 and design='Delivery_08h00_10h00'))
BEGIN
	INSERT INTO [dbo].[timeWindowValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)	
		,2
		,'Delivery_08h00_10h00'	
		,'08h00-10h00'
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM timeWindowValues WHERE value = 3 and design='Delivery_10h00_13h00'))
BEGIN
	INSERT INTO [dbo].[timeWindowValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)	
		,3
		,'Delivery_10h00_13h00'	
		,'10h00-13h00'
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM timeWindowValues WHERE value = 4 and design='Delivery_13h00_16h00'))
BEGIN
	INSERT INTO [dbo].[timeWindowValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)	
		,4
		,'Delivery_13h00_16h00'	
		,'13h00-16h00'
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM timeWindowValues WHERE value = 5 and design='Delivery_16h00_19h00'))
BEGIN
	INSERT INTO [dbo].[timeWindowValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)	
		,5
		,'Delivery_16h00_19h00'	
		,'16h00-19h00'
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM timeWindowValues WHERE value = 6 and design='Delivery_19h00_22h00'))
BEGIN
	INSERT INTO [dbo].[timeWindowValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)	
		,6
		,'Delivery_19h00_22h00'	
		,'19h00-22h00'
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM timeWindowValues WHERE value = 7 and design='DeliverySaturday_10h00_14h00'))
BEGIN
	INSERT INTO [dbo].[timeWindowValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)	
		,7
		,'DeliverySaturday_10h00_14h00'	
		,'Sábado 10h00-14h00. Apenas válida para envios com entrega ao sábado'
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

---------------------------------------------------------------------------------------------------------------------------------------------------------
IF (NOT EXISTS (SELECT * FROM pipAuthorizationValues WHERE value = 1 and design='AddressChange'))
BEGIN
	INSERT INTO [dbo].[pipAuthorizationValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)	
		,1
		,'AddressChange'	
		,'Alteração de Morada'
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM pipAuthorizationValues WHERE value = 2 and design='DeliveryDateChange'))
BEGIN
	INSERT INTO [dbo].[pipAuthorizationValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)	
		,2
		,'DeliveryDateChange'	
		,'Alteração de Data de Entrega'
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM pipAuthorizationValues WHERE value = 3 and design='DeliveryWindowChange'))
BEGIN
	INSERT INTO [dbo].[pipAuthorizationValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)	
		,3
		,'DeliveryWindowChange'	
		,'Alteração de Janela Horária'
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM pipAuthorizationValues WHERE value = 4 and design='PickUpPeriodChange'))
BEGIN
	INSERT INTO [dbo].[pipAuthorizationValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)	
		,4
		,'PickUpPeriodChange'	
		,'Alteração de Prazo de Levantamento'
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END

IF (NOT EXISTS (SELECT * FROM pipAuthorizationValues WHERE value = 5 and design='AnotherDeliveryTry'))
BEGIN
	INSERT INTO [dbo].[pipAuthorizationValues]
		([stamp]		
		,[value]		
		,[design]	
		,[obs]		
		,[ousrdata]	
		,[ousrinis]	
		,[usrdata]	
		,[usrinis])	
	SELECT	
		left(newid(),36)	
		,5
		,'AnotherDeliveryTry'	
		,'+ 1 Tentativa de Entrega'
		,GETDATE()	
		,''	
		,GETDATE()		
		,''
END
-----------------------------------------------------------------------------------------------------------------------------------------------