/* 
	exec  up_get_UPUCodeValues 
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_UPUCodeValues]') IS NOT NULL
	drop procedure dbo.up_get_UPUCodeValues
go

create procedure [dbo].up_get_UPUCodeValues

AS
	
	SELECT
		value
		,design
		,obs
	FROM	
		upuCodeValues(nolock)

GO
Grant Execute On [dbo].[up_get_UPUCodeValues] to Public
Grant control On [dbo].[up_get_UPUCodeValues]to Public
GO	