/* 
	exec   up_get_NonDeliveryCaseValues 
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_NonDeliveryCaseValues]') IS NOT NULL
	drop procedure dbo.up_get_NonDeliveryCaseValues
go

create procedure [dbo].up_get_NonDeliveryCaseValues

AS

	SELECT
		value
		,design
		,obs
	FROM	
		nonDeliveryCaseValues(nolock)

GO
Grant Execute On [dbo].[up_get_NonDeliveryCaseValues] to Public
Grant control On [dbo].[up_get_NonDeliveryCaseValues]to Public
GO	