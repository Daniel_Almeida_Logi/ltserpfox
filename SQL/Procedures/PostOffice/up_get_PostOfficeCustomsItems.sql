/* 
	exec  up_get_PostOfficeCustomsItems '' 
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_PostOfficeCustomsItems]') IS NOT NULL
	drop procedure dbo.up_get_PostOfficeCustomsItems
go


create procedure [dbo].up_get_PostOfficeCustomsItems

 @token varchar(36)

AS
	
	SELECT
		 isnull(currency,'')			AS currency
		,isnull(detail,'')				AS detail
		,isnull(harmonizedCode,'')		AS harmonizedCode
		,isnull(itemNumber,0)			AS itemNumber
		,isnull(originCountry,'')		AS originCountry
		,isnull(quantity,0)				AS quantity
		,isnull(value,0)				AS value
		,isnull(weight,0)				AS weight
	FROM	
		pfCustomsInfo(nolock)
	WHERE 
		pfCustomsInfoToken = @token 

GO
Grant Execute On [dbo].[up_get_PostOfficeCustomsItems] to Public
Grant control On [dbo].[up_get_PostOfficeCustomsItems]to Public
GO	