/* 
	exec   up_get_ExportTypeValues
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_ExportTypeValues]') IS NOT NULL
	drop procedure dbo.up_get_ExportTypeValues
go

create procedure [dbo].up_get_ExportTypeValues

AS
	
	SELECT
		value
		,design
		,obs
	FROM	
		ExportTypeValues(nolock)

GO
Grant Execute On [dbo].[up_get_ExportTypeValues] to Public
Grant control On [dbo].[up_get_ExportTypeValues]to Public
GO	