/* 
	exec   up_get_SpecialServiceTypeValues
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_SpecialServiceTypeValues]') IS NOT NULL
	drop procedure dbo.up_get_SpecialServiceTypeValues
go

create procedure [dbo].up_get_SpecialServiceTypeValues

AS

	SELECT
		value
		,design
	FROM	
		specialServiceTypeValues(nolock)

GO
Grant Execute On [dbo].[up_get_SpecialServiceTypeValues] to Public
Grant control On [dbo].[up_get_SpecialServiceTypeValues]to Public
GO	