/* 
	exec   up_savePostOfficeLabels '','','',''
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_savePostOfficeLabels]') IS NOT NULL
	drop procedure dbo.up_savePostOfficeLabels
go

create procedure [dbo].up_savePostOfficeLabels

@token					varchar(36)
,@filename				varchar(36)
,@label					varchar(max)
,@bestEncoding			varchar(100)

AS

	INSERT INTO pfResponseLabels(stamp, token, fileName, label, bestEncoding, ousrdata, ousrinis, usrdata, usrinis)
	values(left(newid(),36), @token, @filename, @label, @bestEncoding, getdate(), 'ADM', getdate(), 'ADM')	 

GO
Grant Execute On [dbo].[up_savePostOfficeLabels] to Public
Grant control On [dbo].[up_savePostOfficeLabels]to Public
GO	