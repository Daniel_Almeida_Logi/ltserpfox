/* 
	exec   up_savePostOfficeObjectBE 
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_savePostOfficeObjectBE]') IS NOT NULL
	drop procedure dbo.up_savePostOfficeObjectBE
go

create procedure [dbo].up_savePostOfficeObjectBE

@token					varchar(36)
,@nObject				varchar(254)
,@nRelable				varchar(254)
,@nshipment				varchar(254)
,@creationDate			datetime
,@eventsToken			varchar(36)


AS

	INSERT INTO pfResponseObjectsBE(stamp, token, creationDate, nObject, nRelable, nshipment, eventsToken, ousrdata, ousrinis, usrdata, usrinis)
	values(left(newid(),36), @token, @creationDate, @nObject, @nRelable, @nshipment ,@eventsToken, getdate(), 'ADM',getdate(), 'ADM')	 
	

GO
Grant Execute On [dbo].[up_savePostOfficeObjectBE] to Public
Grant control On [dbo].[up_savePostOfficeObjectBE]to Public
GO	