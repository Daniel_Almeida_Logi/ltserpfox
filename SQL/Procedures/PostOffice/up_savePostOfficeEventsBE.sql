/* 
	exec   up_savePostOfficeEventsBE '','','','','','','','','','','',''
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_savePostOfficeEventsBE]') IS NOT NULL
	drop procedure dbo.up_savePostOfficeEventsBE
go

create procedure [dbo].up_savePostOfficeEventsBE
@token							varchar(36)        
,@codeEvent						varchar(100)
,@codeMotive					varchar(100)
,@codeInEvent					varchar(100)
,@codeSituation					varchar(100)
,@dateEvent						varchar(100)
,@descriptionEvent				varchar(254)
,@descriptionInEvent			varchar(254)
,@descriptionSituation			varchar(254)
,@receivername					varchar(254)
,@order							int
,@valid							varchar(50)

AS

	INSERT INTO pfResponseEventsBE(stamp, token, codeEvent, codeMotive, codeInEvent, codeSituation, dateEvent, descriptionEvent, descriptionInEvent
			,descriptionSituation, receivername, [order], valid, ousrdata, ousrinis, usrdata, usrinis)
	values(left(newid(),36), @token, @codeEvent, @codeMotive, @codeInEvent, @codeSituation, @dateEvent, @descriptionEvent, @descriptionInEvent	
				,@descriptionSituation, @receivername, @order, @valid, getdate(), 'ADM', getdate(),'ADM')	 

GO
Grant Execute On [dbo].[up_savePostOfficeEventsBE] to Public
Grant control On [dbo].[up_savePostOfficeEventsBE]to Public
GO	