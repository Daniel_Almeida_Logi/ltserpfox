/* 
	exec   up_get_PIPAuthorizationValues
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_PIPAuthorizationValues]') IS NOT NULL
	drop procedure dbo.up_get_PIPAuthorizationValues
go

create procedure [dbo].up_get_PIPAuthorizationValues

AS

	SELECT
		value
		,design
		,obs
	FROM	
		  PIPAuthorizationValues(nolock)

GO
Grant Execute On [dbo].[up_get_PIPAuthorizationValues] to Public
Grant control On [dbo].[up_get_PIPAuthorizationValues]to Public
GO	