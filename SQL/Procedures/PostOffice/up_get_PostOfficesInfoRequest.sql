/* 
	exec up_get_PostOfficesInfoRequest ''
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_PostOfficesInfoRequest]') IS NOT NULL
	drop procedure dbo.up_get_PostOfficesInfoRequest
go

create procedure [dbo].up_get_PostOfficesInfoRequest
	@token varchar(36)

AS

	SELECT 
		 pfReq.token												AS token															
		,pfReq.processedSQL											AS processedSQL	
		,pfReq.typePostOffice										AS typePostOffice		
		,pfReq.type													AS type			
		,pfReq.site													AS site			
		,pfReq.test													AS test		
		,empresa.nomabrv											AS name			
	FROM	
		pfReq(nolock)
		INNER JOIN EMPRESA (NOLOCK) ON pfReq.site= EMPRESA.site
	WHERE 
		token=@token 
		

GO
Grant Execute On [dbo].[up_get_PostOfficesInfoRequest] to Public
Grant control On [dbo].[up_get_PostOfficesInfoRequest]to Public
GO	
	 