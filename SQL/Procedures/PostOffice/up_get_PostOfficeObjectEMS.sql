/* 
	exec  up_get_PostOfficeObjectEMS ''
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_PostOfficeObjectEMS]') IS NOT NULL
	drop procedure dbo.up_get_PostOfficeObjectEMS
go

CREATE PROCEDURE [dbo].up_get_PostOfficeObjectEMS
@token varchar(36)
AS
	
	SELECT 
		stamp
		,ISNULL(object,'') as object
	FROM	
		pfEMS(nolock)
	WHERE
		 pfEMSToken = @token
		

GO
Grant Execute On [dbo].[up_get_PostOfficeObjectEMS] to Public
Grant control On [dbo].[up_get_PostOfficeObjectEMS]to Public
GO	