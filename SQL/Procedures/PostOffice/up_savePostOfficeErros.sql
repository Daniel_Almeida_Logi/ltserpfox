/* 
	exec   up_savePostOfficeErros '','','',''
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_savePostOfficeErros]') IS NOT NULL
	drop procedure dbo.up_savePostOfficeErros
go

create procedure [dbo].up_savePostOfficeErros

@token		varchar(36)
,@code		int
,@errorcode	varchar(100)
,@message	varchar(max)

AS

	INSERT INTO pfResponseErrors(stamp, token, code,errorcode, message, ousrdata, ousrinis, usrdata, usrinis)
	values(left(newid(),36), @token, @code,@errorCode, @message, getdate(), 'ADM', getdate(), 'ADM')	 

GO
Grant Execute On [dbo].[up_savePostOfficeErros] to Public
Grant control On [dbo].[up_savePostOfficeErros]to Public
GO	