/* 
	exec  up_get_PostOfficeSpecialService 'AC083FBB-B9B3-47EC-A14C-05325D17440B'
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_PostOfficeSpecialService]') IS NOT NULL
	drop procedure dbo.up_get_PostOfficeSpecialService
go

create procedure [dbo].up_get_PostOfficeSpecialService

@token varchar(36)

AS
	
	SELECT 
		 ISNULL(pfSpecialService.certainDate,'')										AS certainDate 
		,ISNULL(pfSpecialService.shipperInstructions,'')								AS shipperInstructions
		,ISNULL(pfSpecialService.pfSpecialServiceType,'')								AS specialServiceType

		,ISNULL(pfSpecialService_deliveryPD.code,'')									AS DeliveryPointData_code
		,ISNULL(pfSpecialService_deliveryPD.name,'')									AS DeliveryPointData_name
		,ISNULL(pfSpecialService_deliveryPD.type,'')									AS DeliveryPointData_type

		,ISNULL(pfMultHomeDelivery.attemptsNumber,'')									AS MultipleHomeDeliveryData_attemptsNumber		
		,ISNULL(pfMultHomeDelivery.inNonDeliveryCase,'')								AS MultipleHomeDeliveryData_inNonDeliveryCase	
		,ISNULL(pfMultHomeDelivery.secondDeliveryAddress,'')							AS MultipleHomeDeliveryData_secondDeliveryAddress

		,ISNULL(pfSpecialService_deliveryPD.code,'')									AS secondDeliveryPoint_code
		,ISNULL(pfSpecialService_deliveryPD.name,'')									AS secondDeliveryPoint_name
		,ISNULL(pfSpecialService_deliveryPD.type,'')									AS secondDeliveryPoint_type

		,ISNULL(pfReturnAuth.pfAddress	,'')											AS ReturnAuthorizationData_pfAddress
		,ISNULL(pfReturnAuth.productCode,'')											AS ReturnAuthorizationData_productCode
		,ISNULL(pfReturnAuth.validationDate,'')											AS ReturnAuthorizationData_validationDate

		,ISNULL(pfTimeWindowData.deliveryDate,'')										AS timeWindow_deliveryDate	
		,ISNULL(pfTimeWindowData.timeWindow	,'')										AS timeWindow_timeWindow	

		,ISNULL(pfSpecialService.value,-1)												AS value
	FROM	
		pfSpecialService(nolock)
		LEFT join pfSpecialService_deliveryPD(nolock) ON pfSpecialService_deliveryPD.stamp = pfSpecialService.pfSpecialService_deliveryPDStamp
		LEFT join pfMultHomeDelivery(nolock) on pfMultHomeDelivery.stamp = pfSpecialService.pfMultHomeDeliveryStamp
		LEFT Join pfReturnAuth(nolock) on pfReturnAuth.stamp = pfSpecialService.pfReturnAuthStamp
		LEFT join pfTimeWindowData(nolock) on pfTimeWindowData.stamp = pfSpecialService.pfTimeWindowDataStamp
	WHERE
		pfSpecialService.pfSpecialServicesToken = @token
		

GO
Grant Execute On [dbo].[up_get_PostOfficeSpecialService] to Public
Grant control On [dbo].[up_get_PostOfficeSpecialService]to Public
GO	