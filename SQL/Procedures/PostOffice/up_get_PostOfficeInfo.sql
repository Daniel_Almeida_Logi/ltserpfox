/* 
	exec up_get_PostOfficeInfo '49CC0222-4372-491F-8857-31D25320D22B'
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_PostOfficeInfo]') IS NOT NULL
	drop procedure dbo.up_get_PostOfficeInfo
go

create procedure [dbo].up_get_PostOfficeInfo
	@token varchar(36)
AS

	SELECT 

		 ISNULL(pfReq.pfEMSToken,'')												AS tokenObjectEMS
		,ISNULL(pfReq.pfReq_ShipCloseToken,'')										AS tokenShipmentClose
		,ISNULL(pfReq.userID,'')													AS userID
		,ISNULL(pfReq.authenticationID,'')											AS authenticationID
		,ISNULL(pfReq_Delivery.clientId,'')											AS deliveryNote_clientId
		,ISNULL(pfReq_Delivery.contractId,'')										AS deliveryNote_contractId
		,ISNULL(pfReq_Delivery.distributionChannelId,'')							AS deliveryNote_distributionChannelId
		,ISNULL(pfReq_Delivery.extData,'')											AS deliveryNote_extData
		,ISNULL(pfReq_Delivery.subProductId,'')										AS deliveryNote_subProductId
		,ISNULL(pfReq_Delivery.pfReq_Delivery_shipToken,'')							AS tokenShipment
		,ISNULL(pfReq.deliveryPointLocation,'')										AS deliveryPointLocation
		,ISNULL(pfReq.deliveryPointPostalCode,'')									AS deliveryPointPostalCode
		,ISNULL(pfReq.deliveryPointEntity,'')										AS deliveryPointEntity
	FROM	
		pfReq(nolock)
		left join pfReq_Delivery(nolock)						ON pfReq_Delivery.stamp = pfReq.pfReq_DeliveryStamp		

	WHERE 
		pfReq.token=@token

GO
Grant Execute On [dbo].[up_get_PostOfficeInfo] to Public
Grant control On [dbo].[up_get_PostOfficeInfo]to Public
GO	