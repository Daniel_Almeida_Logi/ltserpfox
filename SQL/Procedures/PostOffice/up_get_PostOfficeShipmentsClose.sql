/* 
	exec  up_get_PostOfficeShipmentsClose ''
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_PostOfficeShipmentsClose]') IS NOT NULL
	drop procedure dbo.up_get_PostOfficeShipmentsClose
go

create procedure [dbo].up_get_PostOfficeShipmentsClose

@token varchar(36)

AS
	
	SELECT 
		 ISNULL(firstObjectCode,'')									AS firstObjectCode
		,ISNULL(lastObjectCode,'')									AS lastObjectCode
		,ISNULL(subProductCode,'')								AS subProductCode
	FROM	
		pfReq_ShipClose(NOLOCK)
	WHere
		shipCloseToken = @token 	

GO
Grant Execute On [dbo].[up_get_PostOfficeShipmentsClose] to Public
Grant control On [dbo].[up_get_PostOfficeShipmentsClose]to Public
GO	