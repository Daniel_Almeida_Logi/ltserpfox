SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON


IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'pfResponse'))
BEGIN
	
	CREATE TABLE [dbo].[pfResponse](
		[token]						[varchar](36) NOT NULL
		,[status]					[varchar](100)
		,[deliveryNoteId]			[varchar](50)
		,[shipmentDataToken]		[varchar](36)
		,[errorsToken]				[varchar](36)
		,[documentFilesToken]		[varchar](36)
		,[ObjectsBEToken]			[varchar](36)
		,[ousrdata]					[datetime]
		,[ousrinis]					[varchar](30)
		,[usrdata]					[datetime]
		,[usrinis]					[varchar](30)
		
	 CONSTRAINT [PK_pfResponse] PRIMARY KEY CLUSTERED 
	(
		[token] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[pfResponse] ADD  CONSTRAINT [DF_pfResponse_token]  DEFAULT ('') FOR [token]

	ALTER TABLE [dbo].[pfResponse] ADD  CONSTRAINT [DF_pfResponse_status]  DEFAULT ('') FOR [status]

	ALTER TABLE [dbo].[pfResponse] ADD  CONSTRAINT [DF_pfResponse_deliveryNoteId]  DEFAULT ('') FOR [deliveryNoteId]

	ALTER TABLE [dbo].[pfResponse] ADD  CONSTRAINT [DF_pfResponse_shipmentDataToken]  DEFAULT ('') FOR [shipmentDataToken]

	ALTER TABLE [dbo].[pfResponse] ADD  CONSTRAINT [DF_pfResponse_errorsToken]  DEFAULT ('') FOR [errorsToken]

	ALTER TABLE [dbo].[pfResponse] ADD  CONSTRAINT [DF_pfResponse_documentFilesToken]  DEFAULT ('') FOR [documentFilesToken]

	ALTER TABLE [dbo].[pfResponse] ADD  CONSTRAINT [DF_pfResponse_ObjectsBEToken]  DEFAULT ('') FOR [ObjectsBEToken]

	ALTER TABLE [dbo].[pfResponse] ADD  CONSTRAINT [DF_pfResponse_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[pfResponse] ADD  CONSTRAINT [DF_pfResponse_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[pfResponse] ADD  CONSTRAINT [DF_pfResponse_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[pfResponse] ADD  CONSTRAINT [DF_pfResponse_usrinis]  DEFAULT ('') FOR [usrinis]
END									

/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[pfResponse]') AND name = N'IX_pfResponse_shipmentDataToken')
CREATE NONCLUSTERED INDEX [IX_pfResponse_shipmentDataToken] ON [dbo].[pfResponse]
(
    [shipmentDataToken] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[pfResponse]') AND name = N'IX_pfResponse_documentFilesToken')
CREATE NONCLUSTERED INDEX [IX_pfResponse_documentFilesToken] ON [dbo].[pfResponse]
(
    [documentFilesToken] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[pfResponse]') AND name = N'IX_pfResponse_ObjectsBEToken')
CREATE NONCLUSTERED INDEX [IX_pfResponse_ObjectsBEToken] ON [dbo].[pfResponse]
(
    [ObjectsBEToken] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
------------------------------------------------------------------------------------------------------------------------------------------------------------

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'pfResponseErrors'))
BEGIN
	
	CREATE TABLE [dbo].[pfResponseErrors](
		[stamp]						[varchar](36) NOT NULL
		,[token]					[varchar](36)
		,[code]						[Int]
		,[errorcode]				[varchar](100)
		,[message]					[varchar](max)
		,[ousrdata]					[datetime]
		,[ousrinis]					[varchar](30)
		,[usrdata]					[datetime]
		,[usrinis]					[varchar](30)
		
	 CONSTRAINT [pK_pfResponseErrors] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[pfResponseErrors] ADD  CONSTRAINT [DF_pfResponseErrors_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[pfResponseErrors] ADD  CONSTRAINT [DF_pfResponseErrors_token]  DEFAULT ('') FOR [token]

	ALTER TABLE [dbo].[pfResponseErrors] ADD  CONSTRAINT [DF_pfResponseErrors_code]  DEFAULT (0) FOR [code]

	ALTER TABLE [dbo].[pfResponseErrors] ADD  CONSTRAINT [DF_pfResponseErrors_errorcode]  DEFAULT (0) FOR [errorcode]

	ALTER TABLE [dbo].[pfResponseErrors] ADD  CONSTRAINT [DF_pfResponseErrors_message]  DEFAULT ('') FOR [message]

	ALTER TABLE [dbo].[pfResponseErrors] ADD  CONSTRAINT [DF_pfResponseErrors_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[pfResponseErrors] ADD  CONSTRAINT [DF_pfResponseErrors_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[pfResponseErrors] ADD  CONSTRAINT [DF_pfResponseErrors_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[pfResponseErrors] ADD  CONSTRAINT [DF_pfResponseErrors_usrinis]  DEFAULT ('') FOR [usrinis]
END									
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[pfResponseErrors]') AND name = N'IX_pfResponseErrors_token')
CREATE NONCLUSTERED INDEX [IX_pfResponseErrors_token] ON [dbo].[pfResponseErrors]
(
    [token] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
------------------------------------------------------------------------------------------------------------------------------------------------------------

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'pfResponseDocumentsFiles'))
BEGIN
	
	CREATE TABLE [dbo].[pfResponseDocumentsFiles](
		[stamp]						[varchar](36) NOT NULL
		,[token]					[varchar](36)
		,[fileName]					[varchar](100)
		,[path]						[varchar](max)
		,[ousrdata]					[datetime]
		,[ousrinis]					[varchar](30)
		,[usrdata]					[datetime]
		,[usrinis]					[varchar](30)
		
	 CONSTRAINT [pK_pfResponseDocumentsFiles] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[pfResponseDocumentsFiles] ADD  CONSTRAINT [DF_pfResponseDocumentsFiles_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[pfResponseDocumentsFiles] ADD  CONSTRAINT [DF_pfResponseDocumentsFiles_token]  DEFAULT ('') FOR [token]

	ALTER TABLE [dbo].[pfResponseDocumentsFiles] ADD  CONSTRAINT [DF_pfResponseDocumentsFiles_fileName]  DEFAULT ('') FOR [fileName]

	ALTER TABLE [dbo].[pfResponseDocumentsFiles] ADD  CONSTRAINT [DF_pfResponseDocumentsFiles_path]  DEFAULT ('') FOR [path]

	ALTER TABLE [dbo].[pfResponseDocumentsFiles] ADD  CONSTRAINT [DF_pfResponseDocumentsFiles_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[pfResponseDocumentsFiles] ADD  CONSTRAINT [DF_pfResponseDocumentsFiles_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[pfResponseDocumentsFiles] ADD  CONSTRAINT [DF_pfResponseDocumentsFiles_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[pfResponseDocumentsFiles] ADD  CONSTRAINT [DF_pfResponseDocumentsFiles_usrinis]  DEFAULT ('') FOR [usrinis]
END									
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[pfResponseDocumentsFiles]') AND name = N'IX_pfResponseDocumentsFiles_token')
CREATE NONCLUSTERED INDEX [IX_pfResponseDocumentsFiles_token] ON [dbo].[pfResponseDocumentsFiles]
(
    [token] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
------------------------------------------------------------------------------------------------------------------------------------------------------------

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'pfResponseShipment'))
BEGIN
	
	CREATE TABLE [dbo].[pfResponseShipment](
		[stamp]						[varchar](36) NOT NULL
		,[token]					[varchar](36)
		,[clientReference]			[varchar](50)
		,[firstObject]				[varchar](100)
		,[latObject]				[varchar](100)
		,[shipmentNumber]			[int]
		,[originalObjectID]			[varchar](100)
		,[tokenDocumentFiles]		[varchar](36)
		,[tokenLabel]				[varchar](36)
		,[ousrdata]					[datetime]
		,[ousrinis]					[varchar](30)
		,[usrdata]					[datetime]
		,[usrinis]					[varchar](30)
		
	 CONSTRAINT [pK_pfResponseShipment] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[pfResponseShipment] ADD  CONSTRAINT [DF_pfResponseShipment_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[pfResponseShipment] ADD  CONSTRAINT [DF_pfResponseShipment_token]  DEFAULT ('') FOR [token]
	
	ALTER TABLE [dbo].[pfResponseShipment] ADD  CONSTRAINT [DF_pfResponseShipment_clientReference]  DEFAULT ('') FOR [clientReference]
	
	ALTER TABLE [dbo].[pfResponseShipment] ADD  CONSTRAINT [DF_pfResponseShipment_firstObject]  DEFAULT ('') FOR [firstObject]
	
	ALTER TABLE [dbo].[pfResponseShipment] ADD  CONSTRAINT [DF_pfResponseShipment_latObject]  DEFAULT ('') FOR [latObject]
	
	ALTER TABLE [dbo].[pfResponseShipment] ADD  CONSTRAINT [DF_pfResponseShipment_shipmentNumber]  DEFAULT (0) FOR [shipmentNumber]
	
	ALTER TABLE [dbo].[pfResponseShipment] ADD  CONSTRAINT [DF_pfResponseShipment_originalObjectID]  DEFAULT ('') FOR [originalObjectID]
	
	ALTER TABLE [dbo].[pfResponseShipment] ADD  CONSTRAINT [DF_pfResponseShipment_tokenDocumentFiles]  DEFAULT ('') FOR [tokenDocumentFiles]
	
	ALTER TABLE [dbo].[pfResponseShipment] ADD  CONSTRAINT [DF_pfResponseShipment_tokenLabel]  DEFAULT ('') FOR [tokenLabel]

	ALTER TABLE [dbo].[pfResponseShipment] ADD  CONSTRAINT [DF_pfResponseShipment_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[pfResponseShipment] ADD  CONSTRAINT [DF_pfResponseShipment_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[pfResponseShipment] ADD  CONSTRAINT [DF_pfResponseShipment_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[pfResponseShipment] ADD  CONSTRAINT [DF_pfResponseShipment_usrinis]  DEFAULT ('') FOR [usrinis]
END									
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[pfResponseShipment]') AND name = N'IX_pfResponseShipment_token')
CREATE NONCLUSTERED INDEX [IX_pfResponseShipment_token] ON [dbo].[pfResponseShipment]
(
    [token] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[pfResponseShipment]') AND name = N'IX_pfResponseShipment_tokenDocumentFiles')
CREATE NONCLUSTERED INDEX [IX_pfResponseShipment_tokenDocumentFiles] ON [dbo].[pfResponseShipment]
(
    [tokenDocumentFiles] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[pfResponseShipment]') AND name = N'IX_pfResponseShipment_tokenLabel')
CREATE NONCLUSTERED INDEX [IX_pfResponseShipment_tokenLabel] ON [dbo].[pfResponseShipment]
(
    [tokenLabel] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
------------------------------------------------------------------------------------------------------------------------------------------------------------

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'pfResponseObjectsBE'))
BEGIN
	
	CREATE TABLE [dbo].[pfResponseObjectsBE](
		[stamp]						[varchar](36) NOT NULL
		,[token]					[varchar](36)
		,[creationDate]				[datetime]
		,[nObject]					[varchar](254)
		,[nRelable]					[varchar](254)
		,[nshipment]				[varchar](254)
		,[eventsToken]				[varchar](36)
		,[ousrdata]					[datetime]
		,[ousrinis]					[varchar](30)
		,[usrdata]					[datetime]
		,[usrinis]					[varchar](30)
		
	 CONSTRAINT [pK_pfResponseObjectsBE] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[pfResponseObjectsBE] ADD  CONSTRAINT [DF_pfResponseObjectsBE_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[pfResponseObjectsBE] ADD  CONSTRAINT [DF_pfResponseObjectsBE_token]  DEFAULT ('') FOR [token]
	
	ALTER TABLE [dbo].[pfResponseObjectsBE] ADD  CONSTRAINT [DF_pfResponseObjectsBE_creationDate]  DEFAULT ('') FOR [creationDate]
	
	ALTER TABLE [dbo].[pfResponseObjectsBE] ADD  CONSTRAINT [DF_pfResponseObjectsBE_nObject]  DEFAULT ('') FOR [nObject]
	
	ALTER TABLE [dbo].[pfResponseObjectsBE] ADD  CONSTRAINT [DF_pfResponseObjectsBE_nRelable]  DEFAULT ('') FOR [nRelable]
	
	ALTER TABLE [dbo].[pfResponseObjectsBE] ADD  CONSTRAINT [DF_pfResponseObjectsBE_nshipment]  DEFAULT ('') FOR [nshipment]
	
	ALTER TABLE [dbo].[pfResponseObjectsBE] ADD  CONSTRAINT [DF_pfResponseObjectsBE_eventsToken]  DEFAULT ('') FOR [eventsToken]

	ALTER TABLE [dbo].[pfResponseObjectsBE] ADD  CONSTRAINT [DF_pfResponseObjectsBE_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[pfResponseObjectsBE] ADD  CONSTRAINT [DF_pfResponseObjectsBE_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[pfResponseObjectsBE] ADD  CONSTRAINT [DF_pfResponseObjectsBE_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[pfResponseObjectsBE] ADD  CONSTRAINT [DF_pfResponseObjectsBE_usrinis]  DEFAULT ('') FOR [usrinis]
END									
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[pfResponseObjectsBE]') AND name = N'IX_pfResponseObjectsBE_token')
CREATE NONCLUSTERED INDEX [IX_pfResponseObjectsBE_token] ON [dbo].[pfResponseObjectsBE]
(
    [token] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[pfResponseObjectsBE]') AND name = N'IX_pfResponseObjectsBE_eventsToken')
CREATE NONCLUSTERED INDEX [IX_pfResponseObjectsBE_eventsToken] ON [dbo].[pfResponseObjectsBE]
(
    [eventsToken] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
------------------------------------------------------------------------------------------------------------------------------------------------------------

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'pfResponseEventsBE'))
BEGIN
	
	CREATE TABLE [dbo].[pfResponseEventsBE](
		[stamp]						[varchar](36) NOT NULL
		,[token]					[varchar](36)        
		,[codeEvent]				[varchar](100)
		,[codeMotive]				[varchar](100)
		,[codeInEvent]				[varchar](100)
		,[codeSituation]			[varchar](100)
		,[dateEvent]				[datetime] 
		,[descriptionEvent]			[varchar](254)
		,[descriptionInEvent]		[varchar](254)
		,[descriptionSituation]		[varchar](254)
		,[receivername]				[varchar](254)
		,[order]					[int]
		,[valid]					[varchar](50)
		,[ousrdata]					[datetime]
		,[ousrinis]					[varchar](30)
		,[usrdata]					[datetime]
		,[usrinis]					[varchar](30)
		
	 CONSTRAINT [pK_pfResponseEventsBE] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[pfResponseEventsBE] ADD  CONSTRAINT [DF_pfResponseEventsBE_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[pfResponseEventsBE] ADD  CONSTRAINT [DF_pfResponseEventsBE_codeEvent]  DEFAULT ('') FOR [codeEvent]
	
	ALTER TABLE [dbo].[pfResponseEventsBE] ADD  CONSTRAINT [DF_pfResponseEventsBE_codeMotive]  DEFAULT ('') FOR [codeMotive]
	
	ALTER TABLE [dbo].[pfResponseEventsBE] ADD  CONSTRAINT [DF_pfResponseEventsBE_codeInEvent]  DEFAULT ('') FOR [codeInEvent]
	
	ALTER TABLE [dbo].[pfResponseEventsBE] ADD  CONSTRAINT [DF_pfResponseEventsBE_codeSituation]  DEFAULT ('') FOR [codeSituation]
	
	ALTER TABLE [dbo].[pfResponseEventsBE] ADD  CONSTRAINT [DF_pfResponseEventsBE_dateEvent]  DEFAULT ('') FOR [dateEvent]
	
	ALTER TABLE [dbo].[pfResponseEventsBE] ADD  CONSTRAINT [DF_pfResponseEventsBE_descriptionEvent]  DEFAULT ('') FOR [descriptionEvent]
	
	ALTER TABLE [dbo].[pfResponseEventsBE] ADD  CONSTRAINT [DF_pfResponseEventsBE_descriptionInEvent]  DEFAULT ('') FOR [descriptionInEvent]
	
	ALTER TABLE [dbo].[pfResponseEventsBE] ADD  CONSTRAINT [DF_pfResponseEventsBE_descriptionSituation]  DEFAULT ('') FOR [descriptionSituation]
	
	ALTER TABLE [dbo].[pfResponseEventsBE] ADD  CONSTRAINT [DF_pfResponseEventsBE_receivername]  DEFAULT ('') FOR [receivername]
	
	ALTER TABLE [dbo].[pfResponseEventsBE] ADD  CONSTRAINT [DF_pfResponseEventsBE_order]  DEFAULT (0) FOR [order]
	
	ALTER TABLE [dbo].[pfResponseEventsBE] ADD  CONSTRAINT [DF_pfResponseEventsBE_valid]  DEFAULT ('') FOR [valid]

	ALTER TABLE [dbo].[pfResponseEventsBE] ADD  CONSTRAINT [DF_pfResponseEventsBE_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[pfResponseEventsBE] ADD  CONSTRAINT [DF_pfResponseEventsBE_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[pfResponseEventsBE] ADD  CONSTRAINT [DF_pfResponseEventsBE_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[pfResponseEventsBE] ADD  CONSTRAINT [DF_pfResponseEventsBE_usrinis]  DEFAULT ('') FOR [usrinis]
END									
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[pfResponseEventsBE]') AND name = N'IX_pfResponseEventsBE_token')
CREATE NONCLUSTERED INDEX [IX_pfResponseEventsBE_token] ON [dbo].[pfResponseEventsBE]
(
    [token] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
------------------------------------------------------------------------------------------------------------------------------------------------------------

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'pfResponseLabels'))
BEGIN
	
	CREATE TABLE [dbo].[pfResponseLabels](
		[stamp]						[varchar](36) NOT NULL
		,[token]					[varchar](36) 
		,[fileName]					[varchar](100)
		,[label]					[varchar](max)
		,[bestEncoding]				[varchar](100)  
		,[ousrdata]					[datetime]
		,[ousrinis]					[varchar](30)
		,[usrdata]					[datetime]
		,[usrinis]					[varchar](30)
		
	 CONSTRAINT [pK_pfResponseLabels] PRIMARY KEY CLUSTERED 
	(
		[stamp] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[pfResponseLabels] ADD  CONSTRAINT [DF_pfResponseLabels_stamp]  DEFAULT ('') FOR [stamp]

	ALTER TABLE [dbo].[pfResponseLabels] ADD  CONSTRAINT [DF_pfResponseLabels_token]  DEFAULT ('') FOR [token]
	
	ALTER TABLE [dbo].[pfResponseLabels] ADD  CONSTRAINT [DF_pfResponseLabels_fileName]  DEFAULT ('') FOR [fileName]
	
	ALTER TABLE [dbo].[pfResponseLabels] ADD  CONSTRAINT [DF_pfResponseLabels_label]  DEFAULT ('') FOR [label]
	
	ALTER TABLE [dbo].[pfResponseLabels] ADD  CONSTRAINT [DF_pfResponseLabels_bestEncoding]  DEFAULT ('') FOR [bestEncoding]

	ALTER TABLE [dbo].[pfResponseLabels] ADD  CONSTRAINT [DF_pfResponseLabels_ousrdata]  DEFAULT (getdate()) FOR [ousrdata]

	ALTER TABLE [dbo].[pfResponseLabels] ADD  CONSTRAINT [DF_pfResponseLabels_ousrinis]  DEFAULT ('') FOR [ousrinis]

	ALTER TABLE [dbo].[pfResponseLabels] ADD  CONSTRAINT [DF_pfResponseLabels_usrdata]  DEFAULT (getdate()) FOR [usrdata]

	ALTER TABLE [dbo].[pfResponseLabels] ADD  CONSTRAINT [DF_pfResponseLabels_usrinis]  DEFAULT ('') FOR [usrinis]
END									
/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[pfResponseLabels]') AND name = N'IX_pfResponseLabels_token')
CREATE NONCLUSTERED INDEX [IX_pfResponseLabels_token] ON [dbo].[pfResponseLabels]
(
    [token] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
------------------------------------------------------------------------------------------------------------------------------------------------------------
