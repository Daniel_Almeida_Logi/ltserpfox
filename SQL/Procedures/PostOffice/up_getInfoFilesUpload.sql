/* 

	
exec up_getInfoFilesUpload  'Loja 1'
	

*/
	
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


IF OBJECT_ID('[dbo].[up_getInfoFilesUpload]') IS NOT NULL
		DROP PROCEDURE [dbo].[up_getInfoFilesUpload] ;
GO


CREATE PROCEDURE [dbo].[up_getInfoFilesUpload]	
	@site			varchar(50)
		 		 
	/* WITH ENCRYPTION */
AS

SET NOCOUNT ON

BEGIN	


	DECLARE @pathfile varchar(250)
	DECLARE @possiblefile varchar(250)
	DECLARE @maxsizefile varchar(250)

	select @pathfile = textValue from B_Parameters_site(nolock) where stamp='ADM0000000010' and site = @site
	select @possiblefile = textValue from B_Parameters_site(nolock) where stamp='ADM0000000012' and site = @site
	select @maxsizefile = numValue from B_Parameters_site(nolock) where stamp='ADM0000000014' and site = @site

	select @pathfile as path, @possiblefile as possibleFiles,@maxsizefile as size
	

	

END
GO
GRANT EXECUTE on dbo.up_getInfoFilesUpload TO PUBLIC
GRANT Control on dbo.up_getInfoFilesUpload TO PUBLIC
GO