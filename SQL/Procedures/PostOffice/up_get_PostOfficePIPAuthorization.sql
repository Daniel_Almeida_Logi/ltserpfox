/* 
	exec  up_get_PostOfficePIPAuthorization ''
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_PostOfficePIPAuthorization]') IS NOT NULL
	drop procedure dbo.up_get_PostOfficePIPAuthorization
go

create procedure [dbo].up_get_PostOfficePIPAuthorization

@tokenPIP varchar(36)

AS
	
	SELECT 
		PIPType
	FROM	
		pfReq_Delivery_Ship_pipAuth(nolock)
	WHERE
		pipAuthToken = @tokenPIP	

GO
Grant Execute On [dbo].[up_get_PostOfficePIPAuthorization] to Public
Grant control On [dbo].[up_get_PostOfficePIPAuthorization]to Public
GO	