/* 
	exec up_get_PostOfficeShipments '94E5F545-E132-4B5E-BBCC-C15F9D65E551'
	exec up_get_PostOfficeShipments'3AA57781-6F51-4067-8D0B-7E2C7D7CADC2'
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_PostOfficeShipments]') IS NOT NULL
	drop procedure dbo.up_get_PostOfficeShipments
go

CREATE PROCEDURE [dbo].up_get_PostOfficeShipments
	@token varchar(36)
AS
	
	SELECT 

		 ISNULL(pfReq_Delivery_Ship.extData,'')										AS extData
		,ISNULL(pfReq_Delivery_Ship.hasSenderInformation,0)							AS hasSenderInformation
		,ISNULL(pfReq_Delivery_Ship.pipAuthToken,'')								AS tokenPIPAuthorization
		,ISNULL(pfReq_Delivery_Ship.pfSpecialServicesToken,'')						AS tokenSpecialService

		,ISNULL(receiverAdress.address,'')											AS receiverData_address
		,ISNULL(receiverAdress.city,'')												AS receiverData_city
		,ISNULL(receiverAdress.contactName,'')										AS receiverData_contactName
		,ISNULL(receiverAdress.country,'')											AS receiverData_country
		,ISNULL(receiverAdress.door,'')												AS receiverData_door
		,ISNULL(receiverAdress.email,'')											AS receiverData_email
		,ISNULL(receiverAdress.floor,'')											AS receiverData_floor
		,ISNULL(receiverAdress.mobilePhone,'')										AS receiverData_mobilePhone
		,ISNULL(receiverAdress.name,'')												AS receiverData_name
		,ISNULL(receiverAdress.nonPTZipCode,'')										AS receiverData_nonPTZipCode
		,ISNULL(receiverAdress.nonPTZipCodeLocation,'')								AS receiverData_nonPTZipCodeLocation
		,ISNULL(receiverAdress.ptZipCode3,'')											AS receiverData_ptZipCode3
		,ISNULL(receiverAdress.ptZipCode4,'' )											AS receiverData_ptZipCode4
		,ISNULL(receiverAdress.ptZipCodeLocation,'')								AS receiverData_ptZipCodeLocation
		,ISNULL(receiverAdress.phone,'')											AS receiverData_phone
		,ISNULL(receiverAdress.type,'')												AS receiverData_type
		
		,ISNULL(senderAddress.address,'')											AS senderData_address
		,ISNULL(senderAddress.city,'')												AS senderData_city
		,ISNULL(senderAddress.contactName,'')										AS senderData_contactName
		,ISNULL(senderAddress.country,'')											AS senderData_country
		,ISNULL(senderAddress.door,'')												AS senderData_door
		,ISNULL(senderAddress.email,'')												AS senderData_email
		,ISNULL(senderAddress.floor,'')												AS senderData_floor
		,ISNULL(senderAddress.mobilePhone,'')										AS senderData_mobilePhone
		,ISNULL(senderAddress.name,'')												AS senderData_name
		,ISNULL(senderAddress.nonPTZipCode,'')										AS senderData_nonPTZipCode
		,ISNULL(senderAddress.nonPTZipCodeLocation,'')								AS senderData_nonPTZipCodeLocation
		,ISNULL(senderAddress.ptZipCode3,'' )										AS senderData_ptZipCode3
		,ISNULL(senderAddress.ptZipCode4 ,'' )										AS senderData_ptZipCode4
		,ISNULL(senderAddress.ptZipCodeLocation,'')									AS senderData_ptZipCodeLocation
		,ISNULL(senderAddress.phone,'')												AS senderData_phone
		,ISNULL(senderAddress.type,'')												AS senderData_type
		
		,ISNULL(pfInfo.pfCustomsDataStamp,'')										AS stampCustomsData
		,ISNULL(pfInfo.ATCode,'')													AS shipmentData_ATCode
		,ISNULL(pfInfo.clientReference,'')											AS shipmentData_clientReference
		,ISNULL(pfInfo.codigoAgrupamento,'')										AS shipmentData_codigoAgrupamento
		,ISNULL(pfInfo.declaredValue,0)												AS shipmentData_declaredValue
		,ISNULL(pfInfo.exportType,'')												AS shipmentData_exportType
		,ISNULL(pfInfo.isDevolution,0)												AS shipmentData_isDevolution
		,ISNULL(pfInfo.observations,'')												AS shipmentData_observations
		,ISNULL(pfInfo.originalObject,'')											AS shipmentData_originalObject
		,ISNULL(pfInfo.quantity,0)													AS shipmentData_quantity
		,ISNULL(pfInfo.UPUCode,'')													AS shipmentData_UPUCode
		,ISNULL(pfInfo.validationDate,'')											AS shipmentData_validationDate
		,ISNULL(pfInfo.weight ,0)													AS shipmentData_weight
		,ISNULL(pfInfo.chargeDate_partialDelivery,0)								AS shipmentData_cargoDate_partialDelivery
		,ISNULL(pfInfo.chargeDate_schedulingData,'')								AS shipmentData_cargoDate_schedulingData
		,ISNULL(pfInfo.chargeDate_schedulingHour,'')								AS shipmentData_cargoDate_schedulingHour

	FROM	
		pfReq_Delivery_Ship(nolock)
		LEFT join pfAddress (nolock) receiverAdress		ON receiverAdress.stamp = pfReq_Delivery_Ship.pfadressReceiverStamp 
		LEFT join pfAddress (nolock) senderAddress			ON senderAddress.stamp = pfReq_Delivery_Ship.pfadressSenderStamp
		LEFT join pfReq_Delivery_Ship_Info(nolock)  pfInfo ON pfInfo.stamp  = pfReq_Delivery_Ship.pfReq_Delivery_Ship_Info_Stamp
	WHERE 
		pfReq_Delivery_Ship.shipmentsToken=@token
		

GO
Grant Execute On [dbo].[up_get_PostOfficeShipments] to Public
Grant control On [dbo].[up_get_PostOfficeShipments]to Public
GO	
