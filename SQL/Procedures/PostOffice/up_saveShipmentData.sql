/* 
exec up_saveShipmentData 'e22c700e-471c-4ad8-b16f-5cc8d97a5b7','FarmaciaFerreira','FA273950483PT','FA273950483PT',0,'','239aa8bb-1415-4725-9688-f68267630f2','7e31e320-6fd2-49f0-9b8e-96bd351f7c2'
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_saveShipmentData]') IS NOT NULL
	drop procedure dbo.up_saveShipmentData
go

create procedure [dbo].up_saveShipmentData

 @token					varchar(36)
,@clientReference		varchar(50)
,@firstObject			varchar(100)
,@lastObject				varchar(100)
,@shipmentNumber			int
,@originalObjectID		varchar(100)
,@tokenDocumentFiles		varchar(36)
,@tokenLabel				varchar(36)

AS

	INSERT INTO pfResponseShipment(stamp, token, clientReference, firstObject, latObject, shipmentNumber, originalObjectID, tokenDocumentFiles
												,tokenLabel, ousrdata, ousrinis, usrdata, usrinis)
	values(left(newid(),36) ,@token ,@clientReference, @firstObject, @lastObject, @shipmentNumber, @originalObjectID
				,@tokenDocumentFiles, @tokenLabel, getdate(), 'ADM', getdate(), 'ADM')		

GO
Grant Execute On [dbo].[up_saveShipmentData] to Public
Grant control On [dbo].[up_saveShipmentData]to Public
GO	