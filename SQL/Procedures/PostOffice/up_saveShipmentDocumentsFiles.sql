/* 
	exec   up_saveShipmentDocumentsFiles '','','',''
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_saveShipmentDocumentsFiles]') IS NOT NULL
	drop procedure dbo.up_saveShipmentDocumentsFiles
go

create procedure [dbo].up_saveShipmentDocumentsFiles

@stamp			varchar(36)
,@token			varchar(36)
,@filename		varchar(100)
,@path			varchar(max)


AS

	INSERT INTO pfResponseDocumentsFiles(stamp, token, fileName, path, ousrdata, ousrinis, usrdata, usrinis)
	VALUES(@stamp, @token, @filename, @path, getdate(),'ADM', getdate(), 'ADM')	 

	
GO
Grant Execute On [dbo].[up_saveShipmentDocumentsFiles] to Public
Grant control On [dbo].[up_saveShipmentDocumentsFiles]to Public
GO	