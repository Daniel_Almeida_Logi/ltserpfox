/* 
	exec   up_get_DeliveryPointTypeValues
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_DeliveryPointTypeValues]') IS NOT NULL
	drop procedure dbo.up_get_DeliveryPointTypeValues
go

create procedure [dbo].up_get_DeliveryPointTypeValues

AS

	SELECT
		value
		,design
		,obs
	FROM	
		DeliveryPointTypeValues(nolock)

GO
Grant Execute On [dbo].[up_get_DeliveryPointTypeValues] to Public
Grant control On [dbo].[up_get_DeliveryPointTypeValues]to Public
GO	