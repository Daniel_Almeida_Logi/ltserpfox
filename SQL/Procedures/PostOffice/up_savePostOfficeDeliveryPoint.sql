/* 
	exec   up_savePostOfficeDeliveryPoint '','','','','','',''
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_savePostOfficeDeliveryPoint]') IS NOT NULL
	drop procedure dbo.up_savePostOfficeDeliveryPoint
go

create procedure [dbo].up_savePostOfficeDeliveryPoint
	@token	varchar(36),
	@id varchar(254) ,
	@idType varchar(254) ,
	@countryCode varchar(254) ,
	@language varchar(254) ,
	@displayName varchar(254) ,
	@internalDescription varchar(254) ,
	@latitude varchar(254) ,
	@longitude varchar(254) ,
	@streetName varchar(254) ,
	@streetNumber varchar(254) ,
	@postalcode varchar(254) ,
	@town varchar(254) ,
	@phone varchar(254) ,
	@mondayOpening_1 varchar(254) ,
	@mondayClosing_1 varchar(254) ,
	@mondayOpening2 varchar(254) ,
	@mondayClosing2 varchar(254) ,
	@tuesdayOpening_1 varchar(254) ,
	@tuesdayClosing_1 varchar(254) ,
	@tuesdayOpening_2 varchar(254) ,
	@tuesdayClosing_2 varchar(254) ,
	@wednesdayOpening1 varchar(254) ,
	@wednesdayClosing1 varchar(254) ,
	@wednesdayOpening2 varchar(254) ,
	@wednesdayClosing2 varchar(254) ,
	@thursdayOpening1 varchar(254) ,
	@thursdayClosing1 varchar(254) ,
	@thursdayOpening2 varchar(254) ,
	@thursdayClosing2 varchar(254) ,
	@fridayOpening_1 varchar(254) ,
	@fridayClosing_1 varchar(254) ,
	@fridayOpening_2 varchar(254) ,
	@fridayClosing_2 varchar(254) ,
	@saturdayOpening_1 varchar(254) ,
	@saturdayClosing_1 varchar(254) ,
	@saturdayOpening_2 varchar(254) ,
	@saturdayClosing_2 varchar(254) ,
	@sundayOpening_1 varchar(254) ,
	@sundayClosing_1 varchar(254) ,
	@sundayOpening_2 varchar(254) ,
	@sundayClosing_2 varchar(254) ,
	@firstVacationStart varchar(254) ,
	@firstVacationEnd varchar(254) ,
	@secondVacationStart varchar(254) ,
	@secondVacationEnd varchar(254) ,
	@thirdVacationStart varchar(254) ,
	@thirdVacationEnd varchar(254) ,
	@fourthVacationStart varchar(254) ,
	@fourthVacationEnd varchar(254) ,
	@user			varchar(30) 
AS


IF (NOT EXISTS(Select * from pfResponseDeliveryPoints(nolock) where token  = @token and id=@id))
	BEGIN
		INSERT INTO pfResponseDeliveryPoints (stamp,token,id,idType,countryCode,language,displayName,internalDescription,latitude,longitude,streetName,streetNumber,postalcode,town,phone,mondayOpening_1,mondayClosing_1,mondayOpening2,mondayClosing2,tuesdayOpening_1,tuesdayClosing_1,
				tuesdayOpening_2,tuesdayClosing_2,wednesdayOpening1,wednesdayClosing1,wednesdayOpening2,wednesdayClosing2,thursdayOpening1,thursdayClosing1,thursdayOpening2,thursdayClosing2,fridayOpening_1,fridayClosing_1,fridayOpening_2,fridayClosing_2,saturdayOpening_1,saturdayClosing_1,
				saturdayOpening_2,saturdayClosing_2,sundayOpening_1,sundayClosing_1,sundayOpening_2,sundayClosing_2,firstVacationStart,firstVacationEnd,secondVacationStart,secondVacationEnd,thirdVacationStart,thirdVacationEnd,fourthVacationStart,fourthVacationEnd,ousrdata,ousrinis,usrdata,usrinis)
		values(left(newid(),36),@token,@id,@idType,@countryCode,@language,@displayName,@internalDescription,@latitude,@longitude,@streetName,@streetNumber,@postalcode,@town,@phone,@mondayOpening_1,@mondayClosing_1,@mondayOpening2,@mondayClosing2,@tuesdayOpening_1,@tuesdayClosing_1,
				@tuesdayOpening_2,@tuesdayClosing_2,@wednesdayOpening1,@wednesdayClosing1,@wednesdayOpening2,@wednesdayClosing2,@thursdayOpening1,@thursdayClosing1,@thursdayOpening2,@thursdayClosing2 ,@fridayOpening_1,@fridayClosing_1,@fridayOpening_2,@fridayClosing_2,@saturdayOpening_1,@saturdayClosing_1,
				@saturdayOpening_2,@saturdayClosing_2,@sundayOpening_1,@sundayClosing_1,@sundayOpening_2,@sundayClosing_2,@firstVacationStart,@firstVacationEnd,@secondVacationStart,@secondVacationEnd,@thirdVacationStart,@thirdVacationEnd,@fourthVacationStart,@fourthVacationEnd,GETDATE(),@user,GETDATE(),@user)	 
	END
ELSE
	BEGIN
		UPDATE pfResponseDeliveryPoints 
		set 
			 id=@id,
			 idType=@idType,
			 countryCode=@countryCode,
			 language=@language,
			 displayName=@displayName,
			 internalDescription=@internalDescription,
			 latitude=@latitude,
			 longitude=@longitude,
			 streetName=@streetName,
			 streetNumber=@streetNumber,
			 postalcode=@postalcode,
			 town=@town,
			 phone=@phone,
			 mondayOpening_1=@mondayOpening_1,
			 mondayClosing_1=@mondayClosing_1,
			 mondayOpening2=@mondayOpening2,
			 mondayClosing2=@mondayClosing2,
			 tuesdayOpening_1=@tuesdayOpening_1,
			 tuesdayClosing_1=@tuesdayClosing_1,
			 tuesdayOpening_2=@tuesdayOpening_2,
			 tuesdayClosing_2=@tuesdayClosing_2,
			 wednesdayOpening1=@wednesdayOpening1,
			 wednesdayClosing1=@wednesdayClosing1,
			 wednesdayOpening2=@wednesdayOpening2,
			 wednesdayClosing2=@wednesdayClosing2,
			 thursdayOpening1=@thursdayOpening1,
			 thursdayClosing1=@thursdayClosing1,
			 thursdayOpening2=@thursdayOpening2,
			 thursdayClosing2=@thursdayClosing2,
			 fridayOpening_1=@fridayOpening_1,
			 fridayClosing_1=@fridayClosing_1,
			 fridayOpening_2=@fridayOpening_2,
			 fridayClosing_2=@fridayClosing_2,
			 saturdayOpening_1=@saturdayOpening_1,
			 saturdayClosing_1=@saturdayClosing_1,
			 saturdayOpening_2=@saturdayOpening_2,
			 saturdayClosing_2=@saturdayClosing_2,
			 sundayOpening_1=@sundayOpening_1,
			 sundayClosing_1=@sundayClosing_1,
			 sundayOpening_2=@sundayOpening_2,
			 sundayClosing_2=@sundayClosing_2,
			 firstVacationStart=@firstVacationStart,
			 firstVacationEnd=@firstVacationEnd,
			 secondVacationStart=@secondVacationStart,
			 secondVacationEnd=@secondVacationEnd,
			 thirdVacationStart=@thirdVacationStart,
			 thirdVacationEnd=@thirdVacationEnd,
			 fourthVacationStart=@fourthVacationStart,
			 fourthVacationEnd=@fourthVacationEnd,
			 usrdata=getdate(),
			 usrinis=@user
		where token  = @token and id=@id
	END

GO
Grant Execute On [dbo].[up_savePostOfficeDeliveryPoint] to Public
Grant control On [dbo].[up_savePostOfficeDeliveryPoint]to Public
GO	