/* 
	exec  up_get_PostOfficeCustomsData '' 
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_PostOfficeCustomsData]') IS NOT NULL
	drop procedure dbo.up_get_PostOfficeCustomsData
go


create procedure [dbo].up_get_PostOfficeCustomsData

 @stamp varchar(36)

AS
	
	SELECT
		 ISNULL(clientCustomsCode,'')							AS  clientCustomsCode
		,ISNULL(comercialInvoice,'')							AS comercialInvoice
		,ISNULL(comments,'')									AS comments
		,ISNULL(customsTotalItems,'')							AS customsTotalItems
		,ISNULL(customsTotalValue,'')							AS customsTotalValue
		,ISNULL(customsTotalWeight,'')							AS customsTotalWeight
		,ISNULL(exportLicence,'')								AS exportLicence
		,ISNULL(height,'')										AS height
		,ISNULL(insurancePremium,'')							AS insurancePremium
		,ISNULL(insuranceValue,'')								AS insuranceValue
		,ISNULL(length,'')										AS length
		,ISNULL(nonDeliveryCase,'')								AS nonDeliveryCase
		,ISNULL(originCertificateNumber,'')						AS originCertificateNumber
		,ISNULL(receiverTIN,'')									AS receiverTIN
		,ISNULL(sachetDocumentation,0)							AS sachetDocumentation
		,ISNULL(senderEmail,'')									AS senderEmail
		,ISNULL(serviceValue,'')								AS serviceValue
		,ISNULL(VATExportDeclaration,0)							AS VATExportDeclaration
		,ISNULL(VATRate,'')										AS VATRate
		,ISNULL(width,'')										AS width
		,ISNULL(pfCustomsItemsInfoToken,'')						AS tokencustomsItemsData
	FROM	
		pfCustomsData(nolock)
	WHERE 
		stamp = @stamp 

GO
Grant Execute On [dbo].[up_get_PostOfficeCustomsData] to Public
Grant control On [dbo].[up_get_PostOfficeCustomsData]to Public
GO	