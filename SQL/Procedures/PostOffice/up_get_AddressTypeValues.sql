/* 
	exec  up_get_AddressTypeValues
 */

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_get_AddressTypeValues]') IS NOT NULL
	drop procedure dbo.up_get_AddressTypeValues
go

create procedure [dbo].up_get_AddressTypeValues

AS
	
	SELECT
		value
		,design
		,obs
	FROM	
		addressTypeValue(nolock)
 	

GO
Grant Execute On [dbo].[up_get_AddressTypeValues] to Public
Grant control On [dbo].[up_get_AddressTypeValues]to Public
GO	