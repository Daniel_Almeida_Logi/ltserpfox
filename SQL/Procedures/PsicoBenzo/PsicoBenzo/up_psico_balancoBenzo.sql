/* Balanço de Entradas e Saídas de Benzodiazepinas
	
	exec up_psico_balancoBenzo '20240101', '20241231', 'Loja 1'
	
*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_psico_balancoBenzo]') IS NOT NULL
	drop procedure dbo.up_psico_balancoBenzo
go

create procedure dbo.up_psico_balancoBenzo

@dataIni	DATETIME,
@dataFim	DATETIME,
@site		varchar(20)

/* with encryption */

AS
SET NOCOUNT ON


IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#EmpresaArm'))
	DROP TABLE #EmpresaArm

	/* Calc Loja e Armazens */
	declare @site_nr as tinyint
	set @site_nr = ISNULL((select no from empresa where site = @site),0)

	exec up_stocks_CorrigeSA @site_nr 

	select 
		armazem 
	into
		#EmpresaArm
	from 
		empresa 
		inner join empresa_arm on empresa.no = empresa_arm.empresa_no 
	where 
		empresa.site = @site


	/*Calc Result Set final*/
	select
		dataIni			= convert(varchar,@dataIni,102)
		,dataFim		= convert(varchar,@dataFim,102)
		,ref
		,design
		,stockAnterior	= ISNULL(CONVERT(int,stockAnterior),0)
		--,StockAnterior	= convert(int,(sum(saidas) + sum(quebras)) - (sum(entradas) - stockActual))
		,entradas		= CONVERT(int,entradas)
		,saidas			= CONVERT(int,saidas)
		,quebras		= CONVERT(int,quebras)
		,stockActual	= CONVERT(int,stockActual)
	from (
		select
			ref
			,design
			,stockAnterior	= isnull(
								(select sum(qtt) from (
									select 
										case when cm < 50 then sl.qtt else -sl.qtt end as qtt
									from
										sl (nolock)
									where 
										sl.datalc < @DataIni
										and sl.ref = xx.ref
										and sl.armazem in (select armazem from #EmpresaArm)
									)yy
								),0)
			,entradas		= sum(entradas)
			,saidas			= sum(saidas)
			,quebras		= sum(quebras)
			,stockActual	= (select sum(qtt) from (
									select 
										case when cm < 50 then sl.qtt else -sl.qtt end as qtt
									from
										sl (nolock)
									where 
										sl.datalc <= @DataFim 
										and sl.ref = xx.ref
										and sl.armazem in (select armazem from #EmpresaArm)
									)yy
								)
		from (	
			Select
				sl.ref
				,st.design
				,cm
				,sl.qtt
				,entradas	= case
								when cm<50 and sl.qtt > 0 then sl.qtt
								when cm>50 and sl.qtt < 0 then -sl.qtt
								else 0 end
				,saidas		= case
								when bi.ndos = 18 and bi.qtt < 0 then 0
								when cm>50 and sl.qtt > 0 then sl.qtt
								when cm<50 and sl.qtt < 0 then -sl.qtt
								else 0 end
				,quebras	= case when bi.ndos = 18 and bi.qtt < 0 then (sl.qtt) * -1 else 0 end
				,armazem	= sl.armazem
				/* se o produto sair do ambito destes documentos não existe contador e como tal não devia ser permitido */
				,'ndoc'		= isnull(ft.ndoc,isnull(fo.doccode,isnull(bo.ndos,0)))
				,'nmdoc'	= isnull(fo.docnome,isnull(ft.nmdoc,isnull(bo.nmdos,'')))
				,sl.datalc
			From
				st (nolock)
				Inner Join fprod (nolock)	on st.ref = fprod.cnp
				inner join sl (nolock)		on sl.ref = st.ref and  sl.datalc Between @dataini And @datafim
				left Join fi (nolock)		on sl.fistamp = fi.fistamp
				left join ft (nolock)		on ft.ftstamp = fi.ftstamp
				left join fn (nolock)		on sl.fnstamp = fn.fnstamp
				left join fo (nolock)		on fo.fostamp = fn.fostamp
				left join bi (nolock)		on sl.bistamp = bi.bistamp
				left join bo (nolock)		on bo.bostamp = bi.bostamp
			Where 1 = 1 
				and fprod.benzo = 1
				and sl.armazem in (select armazem from #EmpresaArm)
				and st.site_nr = @site_nr
			
			union all

			Select 
				st.ref
				,st.design
				,0 as cm
				,qtt = 0
				,entradas = 0
				,saidas = 0
				,quebras = 0
				,armazem = sl.armazem
				,ndoc = 0
				,nmdoc = ''
				,'' as datalc
			From
						st (nolock)
						Inner Join fprod (nolock)	on st.ref = fprod.cnp
						left join sl (nolock)		on sl.ref = st.ref and sl.datalc < @dataINI
			where 1 = 1 
				and st.site_nr = 1
				and fprod.benzo = 1
				--and sl.qtt > 0
				--and SUM(sl.qtt) > 0 
			group by st.ref, st.design , sl.armazem
			having 
									SUM(case when cm < 50 then sl.qtt else -sl.qtt end ) > 0
			
		) xx
		group by
			ref, design
		
	) xxx
	Where stockActual is not null
	group by
		ref, design, stockAnterior, stockActual, entradas, saidas, quebras
	Order By
		design


GO
Grant Execute on dbo.up_psico_balancoBenzo to Public
Grant Control on dbo.up_psico_balancoBenzo to Public
GO