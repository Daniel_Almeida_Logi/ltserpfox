SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_psico_consultaAlteraPsico]') IS NOT NULL
	drop procedure dbo.up_psico_consultaAlteraPsico
go

create procedure dbo.up_psico_consultaAlteraPsico

@venda	numeric(10),
@doc	varchar(30),
@ano	numeric(4)

/* with encryption */

AS
SET NOCOUNT ON

Select Top 1
	dp.*, 
	ft2.u_receita, ft.ndoc, ft.fdata, ft.fno, ft.nmdoc
From
	B_dadosPsico dp (nolock)
	inner join ft	(nolock)	on ft.ftstamp=dp.ftstamp
	inner join ft2	(nolock)	on ft2.ft2stamp=ft.ftstamp
where
	ft.fno=@venda and ft.nmdoc=@doc
    and Year(ft.fdata)=@ano


GO
Grant Execute on dbo.up_psico_consultaAlteraPsico to Public
Grant control on dbo.up_psico_consultaAlteraPsico to Public
Go