/* Mapa de Registo de Entradas de Psicotr�picos (Gest�o de Psico / Benzo)

	exec up_psico_entradasPsico '20240301', '20240306', 'Loja 1'

*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_psico_entradasPsico]') IS NOT NULL
	drop procedure dbo.up_psico_entradasPsico
go

create procedure dbo.up_psico_entradasPsico

@dataIni datetime,
@dataFim datetime,
@site varchar (20)

/* with encryption */
AS

SET NOCOUNT ON

;with
	cte1(fostamp,fnstamp,ofnstamp,qtt) as
	(
		select
			fo.fostamp,fn.fnstamp,fn.ofnstamp,fn.qtt
		from
			fo (nolock)
			inner join fn (nolock) on fn.fostamp = fo.fostamp
		where
			fo.data between @DataIni and @DataFim
			and fn.u_psicont > 0
			/*and ofnstamp!=''	*/
	),
	cte2(ofnstamp)as
	(
		select
			ofnstamp
		from
			fn fng2 (nolock)
		where
			fng2.ofnstamp in (select fnf.fnstamp
								from fn fnf (nolock)
								where fnf.fnstamp=fng2.ofnstamp and fnf.ofnstamp in (select fng1.fnstamp
																					 from fn fng1 (nolock)
																					 where fng1.fnstamp=fnf.ofnstamp)
									)
	)

Select
	pesquisa	= CONVERT(bit,0)
	,data		= Convert(varchar, fo.data,102) /*Convert(char(10), fo.docdata,104)*/ /*altera�ao data a utilizar*/
	,fo.docnome
	,fo.adoc
	,u_psicont
	,fo.nome
	,fo.morada
	,qtt		= CONVERT(int,
					case when 
						(select isnull(sum(cte1.qtt),0) from cte1
								where cte1.ofnstamp = isnull((fn.fnstamp),'sshh55###')) > 0
											
						then (select isnull(sum(cte1.qtt),0) from Cte1
							where cte1.ofnstamp = isnull((fn.fnstamp),'sshh55###'))
							
						when (select isnull(sum(cte1.qtt),0) from cte1
							where cte1.ofnstamp = isnull((fn.fnstamp),'sshh55###')) < 0
							
						then fn.qtt - abs((select isnull(sum(cte1.qtt),0) from cte1
							where cte1.ofnstamp = isnull((fn.fnstamp),'sshh55###')))
						
						when (select isnull(sum(cte1.qtt),0) from cte1
							where cte1.ofnstamp = isnull((fn.fnstamp),'sshh55###')) = 0
						then fn.qtt
						else 0 end
					)
	,fn.design
	,fn.ref
	,dataini					= convert(char(10),@DataIni,103)
	,datafim					= convert(char(10),@DataFim,103)
	,fo.final
	,stock		= (
		--,stock		= (select stock from st (nolock) where st.ref=fn.ref)
		select 
			sum(stock)
		from 
			sa 
			inner join empresa_arm on empresa_arm.armazem = sa.armazem
			inner join empresa on empresa_arm.empresa_no = empresa.no
		where 
			sa.ref =  case when fn.ref = '' then fn.oref else fn.ref end
			--sa.ref =  fn.ref
			and empresa.site = @site
	)
	,fnstamp
	,fo.fostamp
	,obs		= case when (select isnull(sum(cte1.qtt),0) from cte1
									where cte1.ofnstamp = isnull((fn.fnstamp),'sshh55###')) < 0
								and
								(fn.qtt - abs((select isnull(sum(cte1.qtt),0) from cte1
								where cte1.ofnstamp = isnull((fn.fnstamp),'sshh55###'))))=0
							then 'Abatido/Corrigido por Guia de Acerto'
							else '' end
	/*,dirtec = (select u_dirtec from e1 (nolock) where estab=0)*/
From
	fn (nolock)
	inner join fo		(nolock) on fo.fostamp = fn.fostamp
	inner join fprod	(nolock) on (case when fn.ref = '' then fn.oref else fn.ref end) = fprod.cnp and fprod.psico=1
	inner join empresa (nolock) on fo.site = empresa.site
Where 
	fo.site = case when @site = '' then fo.site else @site end
	/* Nota: foi alterada a condi��o para cen�rios em que se elimina uma guia associada a uma factura */
	and fnstamp in (select fnstamp from cte1 
							where ofnstamp='' or (ofnstamp not in (select fnstamp from cte1 xx where xx.fnstamp=cte1.ofnstamp)
														and fnstamp not in (select ofnstamp from cte1 yy where yy.ofnstamp=cte1.fnstamp)	
														)
						)

	
--union all

--Select
--	pesquisa		= CONVERT(bit,0)
--	,data			= convert(varchar,fo.data,102) /*Convert(char(10), fo.docdata,104)*/
--	,fo.docnome
--	,fo.adoc
--	,u_psicont
--	,fo.nome
--	,fo.morada
--	,qtt			= 0
--	,fn.design
--	,fn.ref
--	,dataini		= convert(char(10),@DataIni,103)
--	,datafim		= convert(char(10),@DataFim,103)
--	,fo.final
--	,stock		= (
--		select 
--			sum(stock)
--		from 
--			sa 
--			inner join empresa_arm on empresa_arm.armazem = sa.armazem
--			inner join empresa on empresa_arm.empresa_no = empresa.no
--		where 
--			--sa.ref = fn.ref
--			sa.ref =  case when fn.ref = '' then fn.oref else fn.ref end
--			and empresa.site = @site
--	)
	
--	,fnstamp
--	,fo.fostamp
--	,obs			= 'Abatido/Corrigido por Guia de Acerto'
--	/*,dirtec			= (select u_dirtec from e1 (nolock) where estab=0)*/
--From
--	fn					(nolock)
--	inner join fo		(nolock) on fo.fostamp = fn.fostamp
--	inner join cm1		(nolock) on cm1.cm=fo.doccode
--	--inner join fprod	(nolock) on fn.ref = fprod.ref and fprod.psico=1
--	inner join fprod	(nolock) on (case when fn.ref = ' ' then fn.oref else fn.ref end) = fprod.cnp and fprod.psico=1
--	inner join empresa (nolock) on fo.site = empresa.site
--Where
--	fo.site = case when @site = '' then fo.site else @site end
--	and (fo.data between @DataIni and @DataFim)
--	and fn.u_psicont>0 and cm1.folansl=1
--	and ofnstamp!=''
--	and ofnstamp not in (select ofnstamp from cte2)
Order by
	fn.u_psicont


GO
Grant Execute On dbo.up_psico_entradasPsico to Public
Grant Control On dbo.up_psico_entradasPsico to Public
Go
