/*	 Mapa de Registo de Entradas de Benzodiazepinas (Gest�o de Psico / Benzo)

	 exec up_psico_entradasBenzo '20240131', '20240229', 'Loja 1'

	 SELECT * FROM fn WHERE u_bencont = 3230

*/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_psico_entradasBenzo]') IS NOT NULL
	drop procedure dbo.up_psico_entradasBenzo
go

create procedure dbo.up_psico_entradasBenzo

@dataIni	DATETIME,
@dataFim	DATETIME,
@site		VARCHAR (20)

/* with encryption */
AS

SET NOCOUNT ON


;WITH
	cte1(fostamp,fnstamp,ofnstamp,qtt) AS
	(
		SELECT
			fostamp,fnstamp,ofnstamp,qtt
		FROM	
			fn (NOLOCK)
		WHERE 
			data BETWEEN @DataIni AND @DataFim
			AND fn.u_bencont>0 
	),
	cte2(ofnstamp) AS
	(
		SELECT
			ofnstamp 
		FROM
			fn fng2 (NOLOCK) 
		WHERE
			fng2.ofnstamp in (SELECT fnf.fnstamp 
								FROM fn fnf (NOLOCK) 
								WHERE fnf.fnstamp=fng2.ofnstamp AND fnf.ofnstamp IN (SELECT fng1.fnstamp 
																					 FROM fn fng1 (NOLOCK) 
																					 WHERE fng1.fnstamp=fnf.ofnstamp)
									) 
								
	)
	

SELECT 
	pesquisa	= CONVERT(BIT,0)
	,data		= CONVERT(VARCHAR, fo.docdata,102)
	,fo.docnome
	,fo.adoc
	,u_bencont
	,fo.nome
	,fo.morada
	,qtt		= CONVERT(int,
					case when (SELECT isnull(sum(cte1.qtt),0) FROM cte1
								WHERE cte1.ofnstamp = isnull((fn.fnstamp),'sss555###')) > 0
							then (SELECT isnull(sum(cte1.qtt),0) FROM cte1
								WHERE cte1.ofnstamp = isnull((fn.fnstamp),'sss555###'))
							when (SELECT isnull(sum(cte1.qtt),0) FROM cte1
								WHERE cte1.ofnstamp = isnull((fn.fnstamp),'sss555###')) < 0
							then fn.qtt - abs((SELECT isnull(sum(cte1.qtt),0) FROM cte1
								WHERE cte1.ofnstamp = isnull((fn.fnstamp),'sss555###')))
							when (SELECT isnull(sum(cte1.qtt),0) FROM cte1
								WHERE cte1.ofnstamp = isnull((fn.fnstamp),'sss555###')) = 0
							then fn.qtt
							else 0 end
				)
	,fn.design
	,fn.ref
	,dataini	= convert(char(10),@DataIni,103)
	,datafim	= convert(char(10),@DataFim,103)
	,fo.final 
	,stock		= (
	--	SELECT stock FROM st (NOLOCK) WHERE st.ref = fn.ref
		SELECT 
			sum(stock)
		FROM 
			sa 
			inner join empresa_arm on empresa_arm.armazem = sa.armazem
			inner join empresa on empresa_arm.empresa_no = empresa.no
		WHERE 
			sa.ref = fn.ref
			and empresa.site = @site
	)
	,fnstamp
	,fo.fostamp
	,obs		= case when (SELECT isnull(sum(cte1.qtt),0) FROM cte1
										WHERE cte1.ofnstamp = isnull((fn.fnstamp),'sss555###')) < 0
									and 
									(fn.qtt - abs((SELECT isnull(sum(cte1.qtt),0) FROM cte1
									WHERE cte1.ofnstamp = isnull((fn.fnstamp),'sss555###'))))=0
								then 'Abatido/Corrigido por Guia de Acerto'
								else '' end
FROM
	fn (NOLOCK)
	inner join fo (NOLOCK) on fo.fostamp = fn.fostamp
	/*inner join cm1 (NOLOCK) on cm1.cm=fo.doccode*/
	inner join fprod (NOLOCK) on fn.ref = fprod.cnp and fprod.benzo=1
	inner join empresa (NOLOCK) on empresa.site = fo.site
WHERE
	fo.site = case when @site = '' then fo.site else @site end
	/*Nota: foi alterada a condi��o para cen�rios em que se elimina uma guia associada a uma factura*/
	and fnstamp in (SELECT fnstamp FROM cte1 
							WHERE ofnstamp='' or (ofnstamp not in (SELECT fnstamp FROM cte1 xx WHERE xx.fnstamp=cte1.ofnstamp)
														and fnstamp not in (SELECT ofnstamp FROM cte1 yy WHERE yy.ofnstamp=cte1.fnstamp)	
														)
						)
--UNION ALL

--SELECT
--	pesquisa		= CONVERT(BIT,0)
--	,data			= CONVERT(VARCHAR,bi.ousrdata,102)
--	,nmdos = isnull(sl.cmdesc, bo.nmdos)
--	,obrano = CONVERT(VARCHAR,bo.obrano)
--	,u_psicont
--	,bo.nome
--	,bo.morada
--	,qtt = isnull(sl.qtt, bi.qtt)			
--	,bi.design
--	,ref = ISNULL(sl.ref,bi.ref)
--	,dataini		= CONVERT(CHAR(10),@DataIni,103)
--	,datafim		= CONVERT(CHAR(10),@DataFim,103)
--	,final = ''
--	,stock		= (
--		SELECT 
--			SUM(stock)
--		FROM 
--			sa(NOLOCK) 
--			INNER JOIN empresa_arm(NOLOCK)	ON empresa_arm.armazem = sa.armazem
--			INNER JOIN empresa(NOLOCK)		ON empresa_arm.empresa_no = empresa.no
--		WHERE 
--			sa.ref =  bi.ref
--			AND empresa.site = @site
--	)
	
--	,fnstamp = bi.bistamp
--	,fostamp = bo.bostamp
--	,bo.obs			
--FROM
--	bi(NOLOCK)
--	INNER JOIN bo(NOLOCK)		ON bo.bostamp = bi.bostamp
--	INNER JOIN ts(NOLOCK)		ON ts.ndos = bo.ndos
--	INNER JOIN fprod(NOLOCK)	ON bi.ref  = fprod.cnp AND fprod.benzo=1
--	INNER JOIN empresa (NOLOCK) ON bo.site = empresa.site
--	INNER JOIN sl (NOLOCK)		ON sl.bistamp = bi.bistamp
--WHERE
--	(bi.ousrdata BETWEEN @DataIni AND @DataFim)
--	AND bi.u_bencont>0
--	AND bi.qtt >0
--	AND ts.codigoDoc NOT IN(34,35)
--	AND sl.cmdesc NOT LIKE '%Saida p/trf%'
--	AND sl.armazem IN (select armazem from empresa_arm(NOLOCK) INNER JOIN empresa(NOLOCK) 
--								ON empresa.no = empresa_arm.empresa_no WHERE site= @site)
ORDER BY
	fn.u_bencont

GO
Grant Execute On dbo.up_psico_entradasBenzo to Public
Grant Control On dbo.up_psico_entradasBenzo to Public
GO