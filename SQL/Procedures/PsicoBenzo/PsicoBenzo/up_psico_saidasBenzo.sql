-- Mapa de Registo de Sa�das de Benzodiazepinas (Gest�o de Psico / Benzo)
-- exec up_psico_saidasBenzo '20100101', '20110713','Loja 1'
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_psico_saidasBenzo]') IS NOT NULL
    drop procedure dbo.up_psico_saidasBenzo
go

create procedure dbo.up_psico_saidasBenzo

@dataIni datetime,
@dataFim datetime,
@site varchar (20)

/* with encryption */
AS

SET NOCOUNT ON


	
Select 
	fi.ref
	,fi.design
	,fi.u_bencont
	,qtt				= convert(int,
							fi.qtt - isnull(vv.qtt,0)
						)
	,ft.nmdoc
	,ft.fno
	,fdata				= Convert(varchar,ft.fdata,102)
	,ft.nome
	,fi.fistamp
	,ft.ftstamp
	,quebras			= 0
	,stock		= (
		select 
			sum(stock)
		from 
			sa 
			inner join empresa_arm on empresa_arm.armazem = sa.armazem
			inner join empresa on empresa_arm.empresa_no = empresa.no
		where 
			sa.ref = fi.ref
			and empresa.site = @site
	)
	,dataini			= convert(char(10),@DataIni,103)
	,datafim			= convert(char(10),@DataFim,103)
	,obs				= (CASE WHEN ft.anulado=1 THEN 'Documento Anulado' ELSE '' END)
	,obs2				= case when zz.qtt=fi.qtt then 'Venda Anulada por Devolu��o' else '' end
From
	fi (nolock)
	inner join st (nolock)		on fi.ref=st.ref
	inner join empresa (nolock)	on empresa.no = st.site_nr
	Inner Join fprod (nolock)	On fi.ref = fprod.cnp And fprod.benzo=1
	Inner Join ft (nolock)		On ft.ftstamp = fi.ftstamp
	Inner Join ft2 (nolock)		On ft.ftstamp = ft2.ft2stamp
	
	left join (
		select 
			xfi.u_bencont,
			qtt = sum(qtt) 
		from
			fi xfi				(nolock)
			inner join ft xft	(nolock) on xft.ftstamp=xfi.ftstamp
		where 
			xfi.tipodoc=3
			and xfi.fistamp not in (select ofistamp from fi (nolock) where u_bencont=fi.u_bencont)	
			and xft.site = case when @site = '' then xft.site else @site end
		group by
			xfi.u_bencont
	) as vv on fi.u_bencont=vv.u_bencont
	

	left join (
		select 
			u_bencont,
			qtt = sum(qtt) 
		from 
			fi xfi				(nolock)
			inner join ft xft	(nolock) on xft.ftstamp=xfi.ftstamp
		where 
			xfi.tipodoc=3
			and (select count(*) from fi yfi (nolock) where yfi.ofistamp=xfi.fistamp)<=0	
			and xft.site = case when @site = '' then xft.site else @site end
		group by
			xfi.u_bencont
	) as zz on fi.u_bencont=zz.u_bencont
		
Where
	ft.site = case when @site = '' then ft.site else @site end
	and ft.fdata Between @DataIni And @DataFim 
	and ft.ndoc!=76 
	and fi.u_bencont>0 
	and (
		fi.ofistamp='' 
		or (select u_bencont from fi wfi (nolock) where wfi.fistamp=fi.ofistamp)=0
	)
	and empresa.site = ft.site

UNION ALL

select
	ref = ISNULL(sl.ref,bi.ref)
	,bi.design
	,bi.u_psicont
	,qtt					= CONVERT(INT,
								ISNULL(sl.qtt, CASE WHEN cm>50 AND (ts.codigoDoc=32 OR ts.codigoDoc=34 OR ts.codigoDoc=35)
								  THEN bi.qtt ELSE 0 END)
							)
	,nmdoc					= ISNULL(sl.cmdesc,bo.nmdos)
	,fno					= bo.obrano
	,fdata					= CONVERT(VARCHAR,bo.dataobra,102)
	,bo.nome
	,bi.bistamp
	,bo.bostamp
	,quebras				= CONVERT(INT,ISNULL(sl.qtt,CASE WHEN cm>50 and ts.codigoDoc=33 THEN bi.qtt ELSE 0 END))
	,stock		= (
		select 
			sum(stock)
		from 
			sa 
			inner join empresa_arm on empresa_arm.armazem = sa.armazem
			inner join empresa on empresa_arm.empresa_no = empresa.no
		where 
			sa.ref = sl.ref
			and empresa.site = @site
	)
	,dataini					= convert(char(10),@DataIni,103)
	,datafim					= convert(char(10),@DataFim,103)
	,bo.obs
	,obs2						= ''
from
	sl (nolock)
	inner join fprod (nolock) on sl.ref = fprod.cnp and fprod.benzo=1
	inner join bi (nolock) on sl.bistamp = bi.bistamp
	inner join bo (nolock) on bo.bostamp = bi.bostamp
	inner join empresa (nolock)	on empresa.site = bo.site
	left join ts (NOLOCK) on ts.ndos = bi.ndos
Where
	(bo.dataobra Between @DataIni And @DataFim)
	and bi.u_bencont>0
	AND sl.cmdesc NOT LIKE '%Entrada p/trf%'
	AND sl.armazem IN (select armazem from empresa_arm(NOLOCK) INNER JOIN empresa(NOLOCK) 
							ON empresa.no = empresa_arm.empresa_no WHERE site= @site)
Order By 
	fi.u_bencont


GO
Grant Execute On dbo.up_psico_saidasBenzo to Public
Grant Control On dbo.up_psico_saidasBenzo to Public
GO