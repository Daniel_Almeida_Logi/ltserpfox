USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_fc_update]    Script Date: 06/12/2014 10:14:36 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE TRIGGER [dbo].[tr_fc_update] ON [dbo].[fc]  FOR UPDATE  AS SET NOCOUNT ONdeclare @m_banchab char(60), @m_contado floatselect @m_banchab = (select valor from para1 where descricao='m_banchab')SELECT @m_contado = (SELECT TOP 1 noconta FROM bl WHERE bl.banco = SUBSTRING(@m_banchab, 1, 10) AND bl.conta = SUBSTRING(@m_banchab, 12, 20))if @m_contado = NULLselect @m_contado = 1if @m_banchab = NULLselect @m_banchab = ''IF UPDATE (cred) or UPDATE(deb) or UPDATE(ecred) or UPDATE(edeb) or UPDATE(no) UPDATE FL SET 	fl.esaldo=fl.esaldo-(deleted.ecred-deleted.edeb+deleted.edifcambio) from DELETED where 	fl.no=deleted.noIF UPDATE (cred) or UPDATE(deb) or UPDATE(ecred) or UPDATE(edeb) or UPDATE(no) UPDATE FL SET 	fl.esaldo=fl.esaldo+inserted.ecred-inserted.edeb+inserted.edifcambio from INSERTED where 	fl.no=inserted.no	IF UPDATE(no) or UPDATE(cred) or UPDATE(deb) or UPDATE(ecred) or UPDATE(edeb) or UPDATE(cr) or UPDATE(cradoc) UPDATE FC SET 	fc.credf = fc.credf-deleted.deb, fc.debf = fc.debf-deleted.cred	,fc.ecredf = fc.ecredf-deleted.edeb, fc.edebf = fc.edebf-deleted.ecred from deleted where 	fc.cm=deleted.cr 	and fc.adoc=deleted.cradoc 	and fc.no=deleted.no 	and deleted.cr > 0 	and deleted.cradoc <> SPACE(10)	IF UPDATE(no) or UPDATE(cred) or UPDATE(deb) or UPDATE(ecred) or UPDATE(edeb) or UPDATE(cr) or UPDATE(cradoc) UPDATE FC SET 	fc.credf = fc.credf+inserted.deb, fc.debf = fc.debf+inserted.cred	,fc.ecredf = fc.ecredf+inserted.edeb, fc.edebf = fc.edebf+inserted.ecred from INSERTED where 	fc.cm=inserted.cr 	and fc.adoc=inserted.cradoc 	and fc.no=inserted.no 	and inserted.cr > 0 	and inserted.cradoc <> SPACE(10)IF UPDATE(crend) UPDATE PL SET 	pl.crend=inserted.crend from INSERTED where 	pl.fcstamp=inserted.fcstamp
GO

