USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_oc_update]    Script Date: 06/12/2014 10:16:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[tr_oc_update] ON [dbo].[oc] /*WITH ENCRYPTION */FOR UPDATE  AS SET NOCOUNT ONIF UPDATE (olgrupo)BEGINupdate cm1 set cm1.clgrupo = inserted.olgrupo from inserted  where cm1.olcl=inserted.olcodigo update cm1 set cm1.flgrupo = inserted.olgrupo from inserted  where cm1.olfl=inserted.olcodigo ENDIF UPDATE (olsgrupo)BEGINupdate cm1 set cm1.clsgrupo = inserted.olsgrupo from inserted  where cm1.olcl=inserted.olcodigo update cm1 set cm1.flsgrupo = inserted.olsgrupo from inserted  where cm1.olfl=inserted.olcodigo END
GO

