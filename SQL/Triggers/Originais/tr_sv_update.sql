USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_sv_update]    Script Date: 06/12/2014 10:18:51 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE TRIGGER [dbo].[tr_sv_update] ON [dbo].[sv]  FOR UPDATE  AS SET NOCOUNT ON IF update (ref) or update (tt) or update (cm) or update (lote) or update (data)  BEGIN DECLARE @@MROW intSELECT @@MROW = @@ROWCOUNTUPDATE ST SET st.pcpond=case when st.stock>0 then case when deleted.cm<50 then (st.pcpond*st.stock-deleted.tt)/(st.stock) else (st.pcpond*st.stock+deleted.tt)/(st.stock) end else st.pcpond end,st.epcpond=case when st.stock>0 then case when deleted.cm<50 then (st.epcpond*st.stock-deleted.ett)/(st.stock) else (st.epcpond*st.stock+deleted.ett)/(st.stock) end else st.epcpond end from deleted where st.ref=deleted.ref INSERT INTO SA (sastamp,armazem,ref) select convert(char(5),armazem)+ref,armazem,ref from inserted where not EXISTS(select * from sa where sa.ref=inserted.ref and sa.armazem=inserted.armazem)UPDATE ST SET st.pcpond=case when st.stock>0  then case when inserted.cm<50 then (st.pcpond*st.stock+inserted.tt)/(st.stock) else (st.pcpond*st.stock-inserted.tt)/(st.stock) end else st.pcpond end,st.epcpond=case when st.stock>0  then case when inserted.cm<50 then (st.epcpond*st.stock+inserted.ett)/(st.stock) else (st.epcpond*st.stock-inserted.ett)/(st.stock) end else st.epcpond end from INSERTED where st.ref=inserted.ref   END
GO

