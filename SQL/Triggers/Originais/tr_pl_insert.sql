USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_pl_insert]    Script Date: 06/12/2014 10:17:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[tr_pl_insert] ON [dbo].[pl]  FOR INSERT  AS SET NOCOUNT ONUPDATE FC SET 	fc.credf=fc.credf+(select SUM(case when inserted.cambio=0 and  inserted.escrec>0 then inserted.escrec-inserted.arred else case when inserted.cambio<>0 and  inserted.escrec>0 then inserted.escrec else 0 end end) from inserted where inserted.fcstamp=fc.fcstamp),	fc.debf=fc.debf-(select SUM(case when inserted.cambio=0 and  inserted.escrec<0 then inserted.escrec-inserted.arred else case when inserted.cambio<>0 and  inserted.escrec<0 then inserted.escrec else 0 end end) from inserted where inserted.fcstamp=fc.fcstamp),	fc.credfm=fc.credfm+(select SUM(case when inserted.ecambio<>0 and  inserted.rec>0 then inserted.rec else 0 end) from inserted where inserted.fcstamp=fc.fcstamp),	fc.debfm=fc.debfm-(select SUM(case when inserted.ecambio<>0 and  inserted.rec<0 then inserted.rec else 0 end) from inserted where inserted.fcstamp=fc.fcstamp),	fc.ecredf=fc.ecredf+(select SUM(case when inserted.erec>0 then inserted.erec-inserted.earred else 0 end) from inserted where inserted.fcstamp=fc.fcstamp),	fc.edebf=fc.edebf-(select SUM(case when inserted.erec<0 then inserted.erec-inserted.earred else 0 end) from inserted where inserted.fcstamp=fc.fcstamp),	fc.virsreg=fc.virsreg+(select SUM(inserted.virs) from inserted where inserted.fcstamp=fc.fcstamp)	,fc.evirsreg=fc.evirsreg+(select SUM(inserted.evirs) from inserted where inserted.fcstamp=fc.fcstamp) from inserted where 	inserted.fcstamp=fc.fcstamp 	and inserted.process=1	UPDATE FC set 	fc.evalpo=fc.evalpo+(select SUM(inserted.erec) from inserted where inserted.fcstamp=fc.fcstamp)	,fc.valpo=fc.valpo+(select SUM(inserted.escrec) from inserted where inserted.fcstamp=fc.fcstamp)	,fc.mvalpo=fc.mvalpo+(select SUM(inserted.rec) from inserted where inserted.fcstamp=fc.fcstamp),	fc.ultdoc='Doc. nº '+convert(char(10),inserted.rno)+' de '+convert(char(10),inserted.rdata,104)	,fc.recibado=1 from inserted where 	fc.fcstamp=inserted.fcstampUPDATE FC set 	fc.irsdif=fc.irsdif+(select SUM(case when inserted.desconto=100 or po.fin=100 then 0 else (ROUND((inserted.virs/(1-(case when inserted.desconto<>0 then inserted.desconto else po.fin end/100))),0)-inserted.virs) end) from inserted inner join po on po.postamp=inserted.postamp where inserted.fcstamp=fc.fcstamp),	fc.eirsdif=fc.eirsdif+(select SUM(case when inserted.desconto=100 or po.fin=100 then 0 else (ROUND((inserted.evirs/(1-(case when inserted.desconto<>0 then inserted.desconto else po.fin end/100))),2)-inserted.evirs) end) from inserted inner join po on po.postamp=inserted.postamp where inserted.fcstamp=fc.fcstamp) from inserted 
inner join po on po.postamp=inserted.postamp 
where 
	fc.fcstamp=inserted.fcstamp 
	and inserted.process=1 
	and (inserted.desconto<>0 or po.fin<>0)

GO

