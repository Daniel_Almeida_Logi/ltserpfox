USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_fi_delete]    Script Date: 06/12/2014 10:06:14 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE TRIGGER [dbo].[tr_fi_delete] 
ON [dbo].[fi]  
FOR DELETE  
AS 

SET NOCOUNT ON

DELETE FROM SL 
FROM deleted 
where 
	sl.fistamp=deleted.fistamp 
	and deleted.fistamp<>''
	
DELETE FROM SV 
FROM deleted 
where 
	sv.fistamp=deleted.fistamp 
	and deleted.fistamp<>''

UPDATE BI 
SET 
	bi.fno = 0,
	bi.adoc = '',
	bi.nmdoc = '',
	bi.oftstamp = '',
	bi.ndoc = 0, 
	bi.qtt2 = bi.qtt2 - (select 
							SUM(deleted.qtt*(case when deleted.tipodoc=3 then -1 else 1 end)) 
						from deleted 
						where 
							deleted.bistamp <> '' 
							and deleted.bistamp = bi.bistamp
						) 
from bi 
inner join deleted on deleted.bistamp=bi.bistamp 
INNER JOIN ft ON deleted.ftstamp = ft.ftstamp 
where 
	deleted.bistamp<>'' 
	AND ft.anulado=0 
	
UPDATE BI 
SET 
	bi.partes2 = bi.partes2 - ( select 
									SUM(deleted.partes) 
								from deleted 
								where 
									deleted.bistamp <> '' 
									and deleted.bistamp = bi.bistamp
								) 
from bi 
inner join deleted on deleted.bistamp = bi.bistamp 
inner join ts (nolock) on ts.ndos = bi.ndos 
where 
	deleted.bistamp<>'' 
	and (ts.usam1 = 1 or ts.usam2 = 1 or ts.usam3 = 1)

UPDATE FTREG 
SET 
	ftreg.vciva=ftreg.vciva - ( select 
									sum(case when deleted.ivaincl=1 then (deleted.tiliquido)*-1 else -deleted.tiliquido*(1+deleted.iva/100) end) 
								from deleted 
								where 
									deleted.ftregstamp=ftreg.ftregstamp
								), 
	ftreg.vmoeda=ftreg.vmoeda - (select 
									sum(case when deleted.ivaincl=1 then (deleted.tmoeda)*-1 else -deleted.tmoeda*(1+deleted.iva/100) end) 
								from deleted 
								where 
									deleted.ftregstamp=ftreg.ftregstamp
								), 
	ftreg.evciva=ftreg.evciva - (select 
									sum(case when deleted.ivaincl=1 then (deleted.etiliquido)*-1 else -deleted.etiliquido*(1+deleted.iva/100) end) 
								from deleted 
								where 
									deleted.ftregstamp=ftreg.ftregstamp
								), 
	ftreg.ultdoc='' 
from ftreg 
inner join deleted on ftreg.ftregstamp=deleted.ftregstamp 
where 
	deleted.tipodoc=3 
	and deleted.ftregstamp<>space(25)

UPDATE FT 
SET 
	ft.facturada = 0
	,ft.nmdocft = ''
	, ft.fnoft =0 
from deleted 
where 
	deleted.ndocft<>0 
	and deleted.ndocft = ft.ndoc 
	and deleted.fnoft = ft.fno  
	and deleted.ftanoft = ft.ftano 
	and ((select count(*) from fi where fi.fistamp<>deleted.fistamp and fi.ndocft=deleted.ndocft and fi.fnoft=deleted.fnoft and fi.ftanoft=deleted.ftanoft and fi.ftstamp=deleted.ftstamp)+(select count(*) from inserted where inserted.ndocft=deleted.ndocft and inserted.fnoft=deleted.fnoft and inserted.ftanoft=deleted.ftanoft))=0 






UPDATE FT 
SET 
	ft.custo=ft.custo-case when deleted.tipodoc=3 then -deleted.qtt*deleted.custo else deleted.qtt*deleted.custo end
	,ft.ecusto=ft.ecusto-case when deleted.tipodoc=3 then -deleted.qtt*deleted.ecusto else deleted.qtt*deleted.ecusto end 
from deleted 
where 
	deleted.ftstamp=ft.ftstamp 
	and deleted.composto=0
GO

