USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_cc_insert]    Script Date: 06/12/2014 10:06:55 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE TRIGGER [dbo].[tr_cc_insert] ON [dbo].[cc] FOR INSERT AS SET NOCOUNT ON
declare @m_banchab char(60), @m_contado float

select @m_banchab = (select valor from para1 where descricao='m_banchab')

SELECT @m_contado = (SELECT TOP 1 noconta FROM bl WHERE bl.banco = SUBSTRING(@m_banchab, 1, 10) AND bl.conta = SUBSTRING(@m_banchab, 12, 20))

if @m_contado = NULL
select @m_contado = 0

if @m_banchab = NULL
select @m_banchab = ''

UPDATE b_utentes 
SET
	b_utentes.esaldo = b_utentes.esaldo+(
											select 
												Sum(inserted.edeb-inserted.ecred) 
											from inserted 
											where 
												inserted.no = b_utentes.no
												and inserted.estab = b_utentes.estab
										)
from INSERTED 
where 
	b_utentes.no = inserted.no
	and b_utentes.estab = inserted.estab

IF ( select count(*) from inserted where inserted.cr<>0 and inserted.docref<>0 and inserted.origem='CC')>0
UPDATE CC 
SET
	cc.debf = cc.debf+inserted.cred
	,cc.credf = cc.credf+inserted.deb
	,cc.edebf = cc.edebf+inserted.ecred
	,cc.ecredf = cc.ecredf+inserted.edeb
from INSERTED 
where 
	cc.cm=inserted.cr 
	and cc.nrdoc=inserted.docref 
	and cc.no=inserted.no 
	and inserted.origem='CC'
	and cc.ccstamp = inserted.occstamp

if (select COUNT(*) from inserted where inserted.formapag='')>0
UPDATE cc 
SET 
	formapag=1 
from inserted
WHERE 
	cc.ccstamp=inserted.ccstamp 
	AND cc.formapag=''
GO

