USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_fi_update]    Script Date: 06/12/2014 10:06:33 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE TRIGGER [dbo].[tr_fi_update] 
ON [dbo].[fi]
FOR UPDATE 

AS

SET NOCOUNT ON

DELETE FROM SL 
FROM inserted 
where 
	sl.fistamp=inserted.fistamp 
	and inserted.fistamp<>'' 
	and inserted.ref=''
	
DELETE FROM SV FROM inserted where sv.fistamp=inserted.fistamp and inserted.fistamp<>'' and inserted.ref=''DELETE FROM SV FROM inserted where sv.fistamp=inserted.fistamp and inserted.fistamp<>'' and inserted.epcp=inserted.ecusto and inserted.pcp=inserted.custo
		
if	update(usrdata) or update(usrhora) or update(usrinis) or 
	update(fifref) or update(codigo) or update(design) or 
	update(ref) or update(lote) or update(armazem) or update(qtt) or 
	update(iectin) or /*update(vvenda) or*/ update(ficcusto) or update(fincusto) or 
	update(desconto) or update(desc2) or update(desc3) or update(desc4) or update(desc5) or update(desc6) or update(pv) or 
	update(iva) or update(ivaincl) or 
	update(eiectin) or /*update(evvenda) or */
	update(num1) or 
	update(composto) or update(cor) or update(tam) or 
	update(usr1) or update(usr2) or update(usr3) or update(usr4) or update(usr5) or 
	/*update(iectmtot) or update(mvbase ) or */
	update(pvmoeda) or 
	/*update(mvvenda ) or */
	update(custo) or update(pcp) or update(cpoc) or 
	update(vbase ) or 
	update(tiliquido) or 
	update(epv) or update(evbase) or 
	update(etiliquido) or 
	update(ecusto) or update(epcp) or 
	update(ftstamp) or update(ndoc) or update(fistamp) or update(fno) 
BEGIN
	UPDATE SL 
	set 
		sl.usrdata=inserted.usrdata,sl.usrhora=inserted.usrhora,sl.usrinis=inserted.usrinis,
		sl.fref=case when td.lifref=1 then inserted.fifref else ft.fref end,sl.ccusto=case when td.liccusto=1 then inserted.ficcusto else ft.ccusto end,
		sl.ncusto=case when td.lincusto=1 then inserted.fincusto else ft.ncusto end,sl.nome=ft.nome,sl.segmento=ft.segmento,
		sl.codigo=inserted.codigo,sl.moeda=ft.moeda,sl.datalc=ft.fdata,sl.design=inserted.design,
		sl.ref=inserted.ref,sl.lote=inserted.lote,sl.armazem=inserted.armazem,sl.qtt=case when ft.tipodoc=3 then inserted.qtt*-1 else inserted.qtt end,
		sl.tt=inserted.sltt,
		sl.vu=inserted.slvu,
		sl.ett=inserted.esltt,
		sl.evu=inserted.eslvu,
		sl.num1=inserted.num1,
		sl.frcl=ft.no,sl.adoc=convert(char (10), ft.fno),sl.cm=td.cmsl,sl.cmdesc=td.cmsln,sl.composto=inserted.composto,sl.cor=inserted.cor,sl.tam=inserted.tam,
		sl.usr1=inserted.usr1,sl.usr2=inserted.usr2,sl.usr3=inserted.usr3,sl.usr4=inserted.usr4,sl.usr5=inserted.usr5,
		sl.vumoeda=inserted.slvumoeda,
		sl.ttmoeda=inserted.slttmoeda,
		sl.pcpond=inserted.pcp,sl.cpoc=inserted.cpoc,
		sl.valiva=case when inserted.iectin<>0 then inserted.pv-inserted.vbase else case when inserted.ivaincl=1 then inserted.tiliquido-inserted.tiliquido/(1+inserted.iva/100) else inserted.tiliquido*inserted.iva/100 end end,
		sl.evaliva=case when inserted.eiectin<>0 then inserted.epv-inserted.evbase else case when inserted.ivaincl=1 then inserted.etiliquido-inserted.etiliquido/(1+inserted.iva/100) else inserted.etiliquido*inserted.iva/100 end end,
		sl.epcpond=inserted.epcp, 
		sl.stns=inserted.stns 
	from inserted 
	inner join ft on inserted.ftstamp=ft.ftstamp 
	inner join td on td.ndoc=inserted.ndoc 
	where 
		inserted.fistamp = sl.fistamp 
		and td.lancasl = 1 
		and inserted.fistamp IN (select deleted.fistamp from deleted) 
END

if	update(fistamp) or update(usrinis) or update(usrhora) or 
	update(usrdata) or update(ousrinis) or update(ousrhora) or 
	update(ousrdata) or 
	update(fifref) or update(codigo) or update(design) or 
	update(ref) or update(lote) or update(armazem) or 
	update(qtt) or update(iectin) or /*update(vvenda) or */
	update(desconto) or update(desc2) or update(desc3) or update(desc4) or update(desc5) or update(desc6) or update(pv) or 
	update(iva) or update(ivaincl) or 
	update(eiectin) or /*update(evvenda) or */
	update(epv) or 
	update(num1) or 
	update(composto) or update(cor) or update(tam) or 
	update(usr1) or update(usr2) or update(usr3) or update(usr4) or update(usr5) or 
	/*update(iectmtot) or update(mvbase) or */
	update(pvmoeda) or 
	/*update(mvvenda ) or */
	update(custo) or update(pcp) or update(cpoc) or 
	update(vbase ) or 
	update(tiliquido) or 
	update(evbase) or 
	update(etiliquido) or 
	update(ecusto ) or update(epcp) or 
	update(ftstamp) or update(ndoc) or update(fistamp) or update(fno) 
BEGIN
	INSERT INTO SL 
		(
		slstamp,fistamp,stns,usrinis,usrhora,usrdata,ousrinis,ousrhora,ousrdata,origem,
		fref,ccusto,ncusto,nome,segmento,codigo,moeda,datalc,design,
		ref,lote,armazem,qtt,
		tt,
		vu,
		ett,
		evu,
		num1,
		frcl,adoc,cm,cmdesc,composto,cor,tam,usr1,usr2,usr3,usr4,usr5,
		vumoeda,
		ttmoeda,
		pcpond,cpoc,
		valiva,evaliva,epcpond 
		)
	select 
		inserted.fistamp,inserted.fistamp,inserted.stns,inserted.usrinis,inserted.usrhora,inserted.usrdata,inserted.ousrinis,inserted.ousrhora,inserted.ousrdata,'FT',
		case when td.lifref=1 then inserted.fifref else ft.fref end,case when td.liccusto=1 then inserted.ficcusto else ft.ccusto end,
		case when td.lincusto=1 then inserted.fincusto else ft.ncusto end,ft.nome,ft.segmento,inserted.codigo,ft.moeda,ft.fdata,inserted.design,
		inserted.ref,inserted.lote,inserted.armazem,case when ft.tipodoc=3 then inserted.qtt*-1 else inserted.qtt end,
		inserted.sltt,
		inserted.slvu,
		inserted.esltt,
		inserted.eslvu,
		inserted.num1,
		ft.no,convert(char (10), ft.fno),td.cmsl,td.cmsln,inserted.composto,inserted.cor,inserted.tam,inserted.usr1,inserted.usr2,inserted.usr3,inserted.usr4,inserted.usr5,
		inserted.slvumoeda,
		inserted.slttmoeda,
		inserted.pcp,inserted.cpoc,
		case when inserted.iectin<>0 then inserted.pv-inserted.vbase else case when inserted.ivaincl=1 then inserted.tiliquido-inserted.tiliquido/(1+inserted.iva/100) else inserted.tiliquido*inserted.iva/100 end end,
		case when inserted.eiectin<>0 then inserted.epv-inserted.evbase else case when inserted.ivaincl=1 then inserted.etiliquido-inserted.etiliquido/(1+inserted.iva/100) else inserted.etiliquido*inserted.iva/100 end end,
		inserted.epcp 
	from inserted 
	inner join ft on ft.ftstamp=inserted.ftstamp 
	inner join td on inserted.ndoc=td.ndoc 
	where 
		ft.anulado=0 
		and not EXISTS(select slstamp from sl where sl.slstamp=inserted.fistamp) 
		and td.lancasl=1 
		and inserted.ref<>space(18)
END

if (select count(*) from inserted inner join ft on ft.ftstamp=inserted.ftstamp inner join 
td on inserted.ndoc=td.ndoc where 
td.lancasl=1 and td.cmsl>50 and td.cmsv<>0 and td.tipodoc=3 and inserted.qtt<>0 and inserted.ref<>space(18) and (inserted.epcp<>inserted.ecusto or inserted.pcp<>inserted.custo))>0 
if update(usrinis) or update(usrhora) or update(usrdata) 
or update(design) or update(ref) or update(lote) or update(cor) or update(tam) or update(armazem) 
or update(qtt) or update(custo) or update(ecusto) or update(pcp) or update(epcp) 
or update(ftstamp) or update(ndoc) or update(fistamp)  or update(fno)
update SV set 
usrdata=inserted.usrdata,
usrhora=inserted.usrhora,
usrinis=inserted.usrinis,
nome=ft.nome,
data=ft.fdata,
design=inserted.design,
ref=inserted.ref,
lote=inserted.lote,
cor=inserted.cor,
tam=inserted.tam,
armazem=inserted.armazem,
tt=inserted.qtt*(inserted.custo-inserted.pcp),
ett=inserted.qtt*(inserted.ecusto-inserted.epcp),
no=ft.no 
from inserted inner join ft on inserted.ftstamp=ft.ftstamp inner join td on td.ndoc=inserted.ndoc 
where ft.anulado=0 and inserted.fistamp=sv.fistamp and 
td.lancasl=1 and td.cmsl>50 and td.cmsv<>0 and td.tipodoc=3 and inserted.qtt<>0 and inserted.ref<>space(18) and (inserted.epcp<>inserted.ecusto or inserted.pcp<>inserted.custo) 
and inserted.fistamp IN (select deleted.fistamp from deleted) 
if (select count(*) from inserted inner join ft on ft.ftstamp=inserted.ftstamp inner join 
td on inserted.ndoc=td.ndoc where 
not EXISTS(select svstamp from sv where sv.svstamp=inserted.fistamp) and 
td.lancasl=1 and td.cmsl>50 and td.cmsv<>0 and td.tipodoc=3 and inserted.qtt<>0 and inserted.ref<>space(18) and (inserted.epcp<>inserted.ecusto or inserted.pcp<>inserted.custo))>0 
INSERT INTO SV 
(svstamp,fistamp,usrinis,usrhora,usrdata,ousrinis,ousrhora,ousrdata,origem,
nome,moeda,data,design,
ref,lote,cor,tam,armazem,
tt,
ett,
no,adoc,cm,cmdesc)
select inserted.fistamp,inserted.fistamp,inserted.usrinis,inserted.usrhora,inserted.usrdata,inserted.ousrinis,inserted.ousrhora,inserted.ousrdata,'FT',
ft.nome,'EURO',ft.fdata,inserted.design,
inserted.ref,inserted.lote,inserted.cor,inserted.tam,inserted.armazem,
inserted.qtt*(inserted.custo-inserted.pcp),
inserted.qtt*(inserted.ecusto-inserted.epcp),
ft.no,convert(char (10), ft.fno),td.cmsv,td.cmsvn 
from inserted inner join ft on ft.ftstamp=inserted.ftstamp inner join td on ft.ndoc=td.ndoc 
where ft.anulado= 0 and not EXISTS(select svstamp from sv where sv.svstamp=inserted.fistamp) and 
td.lancasl=1 and td.cmsl>50 and td.cmsv<>0 and td.tipodoc=3 and inserted.qtt<>0 and inserted.ref<>space(18) and (inserted.epcp<>inserted.ecusto or inserted.pcp<>inserted.custo) 
	
if update(ivaincl) or update(tiliquido) or update(iva) or 
	update(ftregstamp) or 
	update(tmoeda) or  
	update(etiliquido) or 
	update(tipodoc) 
BEGIN
	UPDATE FTREG 
	SET 
		ftreg.vciva=ftreg.vciva-(select 
									sum(case when deleted.ivaincl=1 then (deleted.tiliquido)*-1 else -deleted.tiliquido*(1+deleted.iva/100) end) 
								from deleted 
								where 
									deleted.ftregstamp=ftreg.ftregstamp), 
		ftreg.vmoeda=ftreg.vmoeda-(select 
									sum(case when deleted.ivaincl=1 then (deleted.tmoeda)*-1 else -deleted.tmoeda*(1+deleted.iva/100) end) 
								from deleted 
								where 
									deleted.ftregstamp=ftreg.ftregstamp), 
		ftreg.evciva=ftreg.evciva-(select 
									sum(case when deleted.ivaincl=1 then (deleted.etiliquido)*-1 else -deleted.etiliquido*(1+deleted.iva/100) end) 
								from deleted 
								where 
									deleted.ftregstamp=ftreg.ftregstamp), 
		ftreg.ultdoc='' 
	from ftreg 
	inner join deleted on ftreg.ftregstamp=deleted.ftregstamp 
	where 
		deleted.tipodoc=3 
		and deleted.ftregstamp<>space(25) 
END

if update(ftregstamp) 
BEGIN
	INSERT INTO FTREG 
		(
		ftregstamp
		)
	select 
		distinct inserted.ftregstamp 

	from inserted 
	where 
		inserted.ftregstamp<>space(25) 
		and not EXISTS(select ftregstamp from ftreg where ftreg.ftregstamp=inserted.ftregstamp) 
END

if	update(ivaincl) or update(tiliquido) or update(iva) or 
	update(ftregstamp) or 
	update(tmoeda) or 
	update(etiliquido) or 
	update(ftstamp) or 
	update(tipodoc) 
BEGIN
	UPDATE FTREG 
	SET 
		ftreg.vciva=ftreg.vciva+(select 
									sum(case when inserted.ivaincl=1 then (inserted.tiliquido)*-1 else -inserted.tiliquido*(1+inserted.iva/100) end) 
								from inserted 
								where 
									inserted.ftregstamp=ftreg.ftregstamp), 
		ftreg.vmoeda=ftreg.vmoeda+(select 
									sum(case when inserted.ivaincl=1 then (inserted.tmoeda)*-1 else -inserted.tmoeda*(1+inserted.iva/100) end) 
								from inserted 
								where 
									inserted.ftregstamp=ftreg.ftregstamp), 
		ftreg.evciva=ftreg.evciva+(select
									sum(case when inserted.ivaincl=1 then (inserted.etiliquido)*-1 else -inserted.etiliquido*(1+inserted.iva/100) end) 
								from inserted 
								where inserted.ftregstamp=ftreg.ftregstamp), 
		ftreg.ultdoc=(select ltrim(rtrim(ft.nmdoc))+' nº '+convert(char(10),ft.fno)+' de '+convert(char(10),ft.fdata,104) from ft where inserted.ftstamp=ft.ftstamp) 
	from ftreg 
	inner join inserted on ftreg.ftregstamp=inserted.ftregstamp 
	where 
		inserted.tipodoc=3 
		and inserted.ftregstamp<>space(25)
END

if update(qtt) or update(ftstamp) or update(bistamp) 
BEGIN
	UPDATE BI 
	SET 
		bi.fno = 0,
		bi.adoc = '',
		bi.nmdoc = '',
		bi.oftstamp = '',
		bi.ndoc = 0, 
		bi.qtt2 = bi.qtt2-(select SUM(deleted.qtt *(case when deleted.tipodoc=3 then -1 else 1 end)) from deleted where deleted.bistamp <> '' and deleted.bistamp = bi.bistamp) 

	from bi 
	inner join deleted on deleted.bistamp=bi.bistamp 
	where 
		deleted.bistamp<>'' 

	UPDATE BI 
	SET 
		bi.partes2 = bi.partes2-(select SUM(deleted.partes) from deleted where deleted.bistamp <> '' and deleted.bistamp = bi.bistamp) 

	from bi 
	inner join deleted on deleted.bistamp = bi.bistamp 
	inner join ts (nolock) on ts.ndos = bi.ndos 
	where 
		deleted.bistamp<>'' 
		and (ts.usam1 = 1 or ts.usam2 = 1 or ts.usam3 = 1)

	UPDATE BI 
	SET 
		bi.fdata = ft.fdata,
		bi.fno = ft.fno,
		bi.adoc = convert(char(10),ft.fno),
		bi.oftstamp = ft.ftstamp,
		bi.nmdoc = ft.nmdoc,
		bi.ndoc = ft.ndoc, 
		bi.qtt2 = bi.qtt2+(select 
								SUM(inserted.qtt*(case when inserted.tipodoc=3 then -1 else 1 end)) 
							from inserted 
							where 
								inserted.bistamp <> '' 
								and inserted.bistamp = bi.bistamp) 
	from bi 
	inner join inserted on inserted.bistamp=bi.bistamp 
	inner join ft (nolock) on inserted.ftstamp=ft.ftstamp 
	where 
		inserted.bistamp<>'' 

	UPDATE BI 
	SET 
		bi.partes2 = bi.partes2+(select SUM(inserted.partes*(case when inserted.tipodoc=3 then -1 else 1 end)) from inserted where inserted.bistamp <> '' and inserted.bistamp = bi.bistamp) 
	from bi 
	inner join inserted on inserted.bistamp = bi.bistamp 
	inner join ts (nolock) on ts.ndos = bi.ndos 
	where 
		inserted.bistamp<>'' 
		and (ts.usam1 = 1 or ts.usam2 = 1 or ts.usam3 = 1)
END

if (select count(*) from inserted where ndocft<>0 and fnoft<>0 ) >0 

UPDATE FT 
SET 
	ft.facturada = 1 ,
	ft.nmdocft = td.nmdoc, 
	ft.fnoft = inserted.fno 
from inserted 
inner join td on td.ndoc=inserted.ndoc 
where 
	inserted.ndocft<>0 
	and inserted.ndocft = ft.ndoc 
	and inserted.fnoft = ft.fno  
	and inserted.ftanoft = ft.ftano 

IF UPDATE(QTT) OR UPDATE(CUSTO) OR UPDATE(ECUSTO)
BEGIN
	UPDATE FT 
	SET 
		ft.custo=ft.custo-case when deleted.tipodoc=3 then -deleted.qtt*deleted.custo else deleted.qtt*deleted.custo end, 
		ft.ecusto=ft.ecusto-case when deleted.tipodoc=3 then -deleted.qtt*deleted.ecusto else deleted.qtt*deleted.ecusto end 
	from deleted 
	where 
		deleted.ftstamp=ft.ftstamp

	UPDATE FT 
	SET 
		ft.custo=ft.custo+case when inserted.tipodoc=3 then -inserted.qtt*inserted.custo else inserted.qtt*inserted.custo end, 
		ft.ecusto=ft.ecusto+case when inserted.tipodoc=3 then -inserted.qtt*inserted.ecusto else inserted.qtt*inserted.ecusto end 
	from inserted 
	where 
		inserted.ftstamp=ft.ftstamp
END
GO

