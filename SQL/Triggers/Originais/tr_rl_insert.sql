USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_rl_insert]    Script Date: 06/12/2014 10:08:36 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE TRIGGER [dbo].[tr_rl_insert] ON [dbo].[rl]  FOR INSERT  AS SET NOCOUNT ONUPDATE CC SET 	cc.debf=cc.debf+case when inserted.cambio=0 and inserted.escrec-inserted.arred>0 then inserted.escrec-inserted.arred else case when inserted.cambio<>0 and inserted.escrec>0 then inserted.escrec else 0 end end	,cc.credf=cc.credf-case when inserted.cambio=0 and inserted.escrec-inserted.arred<0 then inserted.escrec-inserted.arred else case when inserted.cambio<>0 and inserted.escrec<0 then inserted.escrec else 0 end end	,cc.debfm=cc.debfm+case when inserted.ecambio<>0 and inserted.rec>0 then inserted.rec else 0 end	,cc.credfm=cc.credfm-case when inserted.ecambio<>0 and inserted.rec<0 then inserted.rec else 0 end	,cc.edebf=cc.edebf+case when inserted.erec>0 then inserted.erec-inserted.earred else 0 end	,cc.ecredf=cc.ecredf-case when inserted.erec<0 then inserted.erec-inserted.earred else 0 end	,cc.virsreg=cc.virsreg+inserted.virs,cc.evirsreg=cc.evirsreg+inserted.evirs from inserted where 	inserted.ccstamp=cc.ccstamp 	and inserted.process=1	UPDATE CC set 	cc.ultdoc='Recibo nº '+convert(char(10),inserted.rno)+' de '+convert(char(10),inserted.rdata,104)	,cc.recibado=1	,cc.recino=inserted.rno	,cc.recian=datepart(yy,inserted.rdata)	,cc.valre=cc.valre+inserted.escrec	,cc.evalre=cc.evalre+inserted.erec	,cc.mvalre=cc.mvalre+inserted.rec	,cc.valch=cc.valch+case when re.cheque=1 then inserted.escrec else 0 end	,cc.evalch=cc.evalch+case when re.cheque=1 then inserted.erec else 0 end	,cc.cbbno = re.cbbno from inserted inner join re on re.restamp=inserted.restamp where 	cc.ccstamp=inserted.ccstamp	UPDATE CC set 	cc.irsdif=cc.irsdif+case when inserted.desconto=100 or re.fin=100 then 0 else (ROUND((inserted.virs/(1-(case when inserted.desconto<>0 then inserted.desconto else re.fin end/100))),0)-inserted.virs) end	,cc.eirsdif=cc.eirsdif+case when inserted.desconto=100 or re.fin=100 then 0 else (ROUND((inserted.evirs/(1-(case when inserted.desconto<>0 then inserted.desconto else re.fin end/100))),2)-inserted.evirs) end from inserted inner join re on re.restamp=inserted.restamp where 	cc.ccstamp=inserted.ccstamp 	and inserted.process=1 	and (inserted.desconto<>0 or re.fin<>0)
GO

