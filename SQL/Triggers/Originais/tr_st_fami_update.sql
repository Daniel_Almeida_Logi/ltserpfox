USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_stfami_update]    Script Date: 06/12/2014 10:18:19 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE TRIGGER [dbo].[tr_stfami_update] ON [dbo].[stfami]  FOR UPDATE  AS SET NOCOUNT ON IF update (nome) BEGIN update st set st.faminome=inserted.nome from inserted where st.familia=inserted.ref END
GO

