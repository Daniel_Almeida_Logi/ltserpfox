USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_po_delete]    Script Date: 06/12/2014 10:17:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[tr_po_delete] ON [dbo].[po]  FOR DELETE  AS SET NOCOUNT ONDELETE from fc from deleted where fc.postamp=deleted.postamp

GO

