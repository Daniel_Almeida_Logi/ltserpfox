USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_st_update]    Script Date: 06/12/2014 10:08:03 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE TRIGGER [dbo].[tr_st_update] ON [dbo].[st]  FOR UPDATE  AS SET NOCOUNT ONIF (update(ref) or update(design)) BEGINIF (select count(*) from bc inner join inserted on bc.ststamp=inserted.ststamp )>0 update bc set bc.ref=inserted.ref,bc.design=inserted.design from bc inner join inserted on bc.ststamp=inserted.ststampEND

GO

