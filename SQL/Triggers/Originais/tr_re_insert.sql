USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_re_insert]    Script Date: 06/12/2014 10:09:06 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE TRIGGER [dbo].[tr_re_insert] 
ON [dbo].[re]  
FOR INSERT  
AS 

SET NOCOUNT ON

INSERT INTO CC 
	(
	ccstamp,restamp,faccstamp,
	usrdata,usrhora,usrinis,
	ousrdata,ousrhora,ousrinis,
	formapag,situacao,
	datalc,no,nome,cm,cmdesc,origem,cobranca,difcambio,edifcambio,
	cred,ecred,credf,ecredf,credm,credfm,deb,edeb,debf,edebf,debm,debfm,
	moeda,nrdoc,dataven,fref,intid,cobrador,rota,ccusto,ncusto,zona,vendedor,vendnm,segmento,tipo,pais,estab,obs
	) 
select 
	inserted.restamp,inserted.restamp,inserted.faccstamp,
	inserted.usrdata,inserted.usrhora,inserted.usrinis,
	inserted.ousrdata,inserted.ousrhora,inserted.ousrinis,
	'1','2',
	inserted.procdata,inserted.no,inserted.nome,
	(case when exists (select cm from cm1 (nolock) where cm=(select cmcc from tsre (nolock) where ndoc=inserted.ndoc)) then (select cmcc from tsre (nolock) where ndoc=inserted.ndoc) else 70 end),
	(case when exists (select cmdesc from cm1 (nolock) where cm=(select cmcc from tsre (nolock) where ndoc=inserted.ndoc)) then (select cmccn from tsre (nolock) where ndoc=inserted.ndoc) else (select cmdesc from cm1 (nolock) where cm=70) end),
	'RE',inserted.cobranca,inserted.difcambio,inserted.edifcambio,
	case when inserted.total-inserted.arred<0 then 0 else inserted.total-inserted.arred end,case when inserted.etotal-inserted.earred<0 then 0 else inserted.etotal-inserted.earred end,
	case when inserted.total-inserted.arred<0 then 0 else inserted.total-inserted.arred end,case when inserted.etotal-inserted.earred<0 then 0 else inserted.etotal-inserted.earred end,
	case when inserted.totalmoeda<0 then 0 else inserted.totalmoeda end,case when inserted.totalmoeda<0 then 0 else inserted.totalmoeda end,
	case when inserted.total-inserted.arred>0 then 0 else -(inserted.total-inserted.arred) end,case when inserted.etotal-inserted.earred>0 then 0 else -(inserted.etotal-inserted.earred) end,
	case when inserted.total-inserted.arred>0 then 0 else -(inserted.total-inserted.arred) end,case when inserted.etotal-inserted.earred>0 then 0 else -(inserted.etotal-inserted.earred) end,
	case when inserted.totalmoeda>0 then 0 else -inserted.totalmoeda end,case when inserted.totalmoeda>0 then 0 else -inserted.totalmoeda end,
	inserted.moeda,inserted.rno,inserted.procdata,inserted.fref,inserted.intid,inserted.cobrador,inserted.rota,inserted.ccusto,inserted.ncusto,inserted.zona,inserted.vendedor,inserted.vendnm,inserted.segmento,inserted.tipo,inserted.pais,inserted.estab,
	inserted.desc1 
from inserted 
where 
	inserted.process=1 
	
INSERT INTO CC 
	(
	ccstamp,restamp,faccstamp,
	usrdata,usrhora,usrinis,
	ousrdata,ousrhora,ousrinis,
	formapag,situacao,
	datalc,no,nome,cm,cmdesc,origem,cobranca,
	cred,ecred,credf,ecredf,credm,credfm,deb,edeb,debf,edebf,debm,debfm,
	moeda,nrdoc,dataven,fref,intid,cobrador,rota,ccusto,ncusto,zona,vendedor,vendnm,segmento,tipo,pais,estab,obs) 
select 
	substring('F'+inserted.restamp,1,25),inserted.restamp,inserted.faccstamp,
	inserted.usrdata,inserted.usrhora,inserted.usrinis,
	inserted.ousrdata,inserted.ousrhora,inserted.ousrinis,
	'1','2',
	inserted.procdata,inserted.no,inserted.nome,94,(select cmdesc from cm1 (nolock) where cm=94),'RE',inserted.cobranca,
	case when inserted.finv<0 then 0 else inserted.finv end,case when inserted.efinv<0 then 0 else inserted.efinv end,
	case when inserted.finv<0 then 0 else inserted.finv end,case when inserted.efinv<0 then 0 else inserted.efinv end,
	case when inserted.finvmoeda<0 then 0 else inserted.finvmoeda end,case when inserted.finvmoeda<0 then 0 else inserted.finvmoeda end,
	case when inserted.finv>0 then 0 else -inserted.finv end,case when inserted.efinv>0 then 0 else -inserted.efinv end,
	case when inserted.finv>0 then 0 else -inserted.finv end,case when inserted.efinv>0 then 0 else -inserted.efinv end,
	case when inserted.finvmoeda>0 then 0 else -inserted.finvmoeda end,case when inserted.finvmoeda>0 then 0 else -inserted.finvmoeda end,
	inserted.moeda,inserted.rno,inserted.procdata,inserted.fref,inserted.intid,inserted.cobrador,inserted.rota,inserted.ccusto,inserted.ncusto,inserted.zona,inserted.vendedor,inserted.vendnm, inserted.segmento,inserted.tipo,inserted.pais,inserted.estab,
	inserted.desc1 
from inserted 
where 
	inserted.process=1 
	and inserted.finv<>0
	
INSERT INTO CC 
	(
	ccstamp,restamp,faccstamp,
	usrdata,usrhora,usrinis,
	ousrdata,ousrhora,ousrinis,
	formapag,situacao,
	datalc,no,nome,cm,cmdesc,origem,
	cred,ecred,credf,ecredf,deb,edeb,debf,edebf,
	moeda,nrdoc,dataven,fref,ccusto,ncusto,intid,tipo,estab,pais,zona,obs) 
select 
	substring('I'+inserted.restamp,1,25),inserted.restamp,inserted.faccstamp,
	inserted.usrdata,inserted.usrhora,inserted.usrinis,
	inserted.ousrdata,inserted.ousrhora,inserted.ousrinis,
	'1','2',
	inserted.procdata,inserted.no,inserted.nome,
	101,
	(case when exists (select cmdesc from cm1 (nolock) where cm=101) then (select cmdesc from cm1 where cm=101) else 'Indefinido' end),
	'RE',
	case when inserted.virs<0 then 0 else inserted.virs end,case when inserted.evirs<0 then 0 else inserted.evirs end,
	case when inserted.virs<0 then 0 else inserted.virs end,case when inserted.evirs<0 then 0 else inserted.evirs end,
	case when inserted.virs>0 then 0 else -inserted.virs end,case when inserted.evirs>0 then 0 else -inserted.evirs end,
	case when inserted.virs>0 then 0 else -inserted.virs end,case when inserted.evirs>0 then 0 else -inserted.evirs end,
	inserted.moeda,inserted.rno,inserted.procdata,inserted.fref,inserted.ccusto,inserted.ncusto,inserted.intid,inserted.tipo,inserted.estab,inserted.pais,inserted.zona, 
	inserted.desc1 
from inserted 
where 
	inserted.process=1 
	and (inserted.virs<>0 or inserted.evirs<>0)

GO

