USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_cm1_update]    Script Date: 06/12/2014 10:13:32 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE TRIGGER [dbo].[tr_cm1_update] ON [dbo].[cm1]  FOR UPDATE  AS SET NOCOUNT ONIF update (cmdesc) BEGIN 	IF UPDATE (cmdesc)	BEGIN		update cc 		set 			cc.cmdesc=inserted.cmdesc 		from inserted 		where 			cc.cm = inserted.cm				update fc 		set 			fc.cmdesc=inserted.cmdesc 		from inserted 		where 			fc.cm = inserted.cm					update td 		set 			td.cmccn=inserted.cmdesc 		from inserted 		where 			td.cmcc = inserted.cm					update po 		set 			po.cmdesc=inserted.cmdesc 		from inserted 		where 			po.cm = inserted.cm					update fo 		set 			fo.docnome=inserted.cmdesc 		from inserted 		where 			fo.doccode = inserted.cm	ENDEND
GO

