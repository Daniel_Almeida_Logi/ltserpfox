USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_fc_delete]    Script Date: 06/12/2014 10:14:18 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE TRIGGER [dbo].[tr_fc_delete] ON [dbo].[fc]  FOR DELETE  AS SET NOCOUNT ONUPDATE FL SET 	fl.esaldo=fl.esaldo-(select SUM(deleted.ecred-deleted.edeb+deleted.edifcambio) from deleted where deleted.no=fl.no) from deleted where 	fl.no=deleted.noUPDATE FC SET 	fc.credf = fc.credf-deleted.deb	,fc.debf = fc.debf-deleted.cred	,fc.ecredf = fc.ecredf-deleted.edeb	,fc.edebf = fc.edebf-deleted.ecred from deleted where 	(	fc.cm=deleted.cr 	and fc.adoc=deleted.cradoc 	and fc.no=deleted.no 	and deleted.cr > 0 	and deleted.cradoc <> SPACE(10)	) 	OR 	(	fc.fcstamp = substring(deleted.fcstamp,2,25) 	AND substring(deleted.fcstamp,1,1)= 'R'	)
GO

