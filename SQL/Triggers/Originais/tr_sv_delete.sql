USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_sv_delete]    Script Date: 06/12/2014 10:18:33 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE TRIGGER [dbo].[tr_sv_delete] ON [dbo].[sv]  FOR DELETE  AS SET NOCOUNT ONDECLARE @@MROW intSELECT @@MROW = @@ROWCOUNTUPDATE ST SET st.pcpond=case when st.stock>0 then case when deleted.cm<50 then (st.pcpond*st.stock-deleted.tt)/(st.stock) else (st.pcpond*st.stock+deleted.tt)/(st.stock) end else st.pcpond end,st.epcpond=case when st.stock>0 then case when deleted.cm<50 then (st.epcpond*st.stock-deleted.ett)/(st.stock) else (st.epcpond*st.stock+deleted.ett)/(st.stock) end else st.epcpond end from deleted where st.ref=deleted.ref 
GO

