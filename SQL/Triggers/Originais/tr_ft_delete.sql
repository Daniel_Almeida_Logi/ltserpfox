USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_ft_delete]    Script Date: 06/12/2014 10:05:22 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE TRIGGER [dbo].[tr_ft_delete] ON [dbo].[ft]FOR DELETEAS SET NOCOUNT ON

DELETE from CC 
from deleted
where 
	cc.ftstamp=deleted.ftstamp
GO

