USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_re_delete]    Script Date: 06/12/2014 10:08:58 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE TRIGGER [dbo].[tr_re_delete] 
ON [dbo].[re]  
FOR DELETE  
AS 

SET NOCOUNT ON

DELETE FROM CC from deleted where cc.restamp=deleted.restamp

GO

