USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_rl_delete]    Script Date: 06/12/2014 10:08:27 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE TRIGGER [dbo].[tr_rl_delete] ON [dbo].[rl]  FOR DELETE  AS SET NOCOUNT ONUPDATE CC SET 	cc.debf = cc.debf - case when deleted.cambio=0 and deleted.escrec>0 then deleted.escrec-deleted.arred else case when deleted.cambio<>0 and deleted.escrec>0 then deleted.escrec else 0 end end	,cc.credf=cc.credf+case when deleted.cambio=0 and deleted.escrec<0 then deleted.escrec-deleted.arred else case when deleted.cambio<>0 and deleted.escrec<0 then deleted.escrec else 0 end end	,cc.debfm=cc.debfm-case when deleted.ecambio<>0 and deleted.rec>0 then deleted.rec else 0 end	,cc.credfm=cc.credfm+case when deleted.ecambio<>0 and deleted.rec<0 then deleted.rec else 0 end	,cc.edebf=cc.edebf-case when deleted.erec>0 then deleted.erec-deleted.earred else 0 end	,cc.ecredf=cc.ecredf+case when deleted.erec<0 then deleted.erec-deleted.earred else 0 end	,cc.virsreg=cc.virsreg-deleted.virs,cc.evirsreg=cc.evirsreg-deleted.evirs from deleted where 	deleted.ccstamp=cc.ccstamp 	and deleted.process=1UPDATE CC set 	cc.ultdoc=''	,cc.recibado=0	,cc.recino=0	,cc.recian=0	,cc.valre=cc.valre-deleted.escrec	,cc.evalre=cc.evalre-deleted.erec	,cc.mvalre=cc.mvalre-deleted.rec	,cc.valch=cc.valch-case when deleted.cheque=1 then deleted.escrec else 0 end	,cc.evalch=cc.evalch-case when deleted.cheque=1 then deleted.erec else 0 end	,cc.cbbno = 0 from deleted where 	cc.ccstamp=deleted.ccstampUPDATE CC set 	cc.irsdif=cc.irsdif-case when deleted.desconto=100 or re.fin=100 then 0 else (ROUND((deleted.virs/(1-(case when deleted.desconto<>0 then deleted.desconto else re.fin end/100))),0)-deleted.virs) end	,cc.eirsdif=cc.eirsdif-case when deleted.desconto=100 or re.fin=100 then 0 else (ROUND((deleted.evirs/(1-(case when deleted.desconto<>0 then deleted.desconto else re.fin end/100))),2)-deleted.evirs) end from deleted inner join re on re.restamp=deleted.restamp where 	cc.ccstamp=deleted.ccstamp 	and deleted.process=1 	and (deleted.desconto<>0 or re.fin<>0)
GO

