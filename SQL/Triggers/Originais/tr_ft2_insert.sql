USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_ft2_insert]    Script Date: 06/12/2014 10:05:55 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE TRIGGER [dbo].[tr_ft2_insert] ON [dbo].[ft2]  FOR INSERT  AS SET NOCOUNT ON
if (select COUNT(*) from inserted where inserted.formapag='')>0
UPDATE ft2 
SET 
	formapag=1 
from inserted
WHERE 
	ft2.ft2stamp=inserted.ft2stamp 
	AND ft2.formapag=''
GO

