USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_ftreg_delete]    Script Date: 06/12/2014 10:15:47 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE TRIGGER [dbo].[tr_ftreg_delete] ON [dbo].[ftreg]  FOR DELETE  AS SET NOCOUNT ONdeclare @m_debfm numeric(19,6)declare @m_ftregstamp varchar(25)select @m_ftregstamp=(select top 1 deleted.ftregstamp from deleted group by deleted.ftregstamp)select @m_debfm=(select sum(ftreg.vmoeda) from ftreg where ftreg.ftregstamp=@m_ftregstamp)UPDATE CC set 	cc.debf=(case when cc.debf-round(deleted.vciva,0)<0 then 0 else cc.debf-round(deleted.vciva,0) end) , 	cc.edebf=(case when cc.edebf-round(deleted.evciva,2 )<0 then 0 else cc.edebf-round(deleted.evciva,2 ) end), 	cc.debfm=(case when @m_debfm<0 then 0 else @m_debfm end), 	cc.ultdoc='' from deleted where 	cc.ftstamp=deleted.ftregstamp
GO

