USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_pl_delete]    Script Date: 06/12/2014 10:16:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[tr_pl_delete] ON [dbo].[pl]  FOR DELETE  AS SET NOCOUNT ONUPDATE FC SET 	fc.credf = fc.credf-(select SUM(case when deleted.cambio=0 and deleted.escrec>0 then deleted.escrec-deleted.arred else case when deleted.cambio<>0 and  deleted.escrec>0 then deleted.escrec else 0 end end) from deleted where deleted.fcstamp=fc.fcstamp),	fc.debf=fc.debf+(select SUM(case when deleted.cambio=0 and deleted.escrec<0 then deleted.escrec-deleted.arred else case when deleted.cambio<>0 and  deleted.escrec<0 then deleted.escrec else 0 end end) from deleted where deleted.fcstamp=fc.fcstamp),	fc.credfm=fc.credfm-(select SUM(case when deleted.ecambio<>0 and deleted.rec>0 then deleted.rec else 0 end) from deleted where deleted.fcstamp=fc.fcstamp),	fc.debfm=fc.debfm+(select SUM(case when deleted.ecambio<>0 and  deleted.rec<0 then deleted.rec else 0 end) from deleted where deleted.fcstamp=fc.fcstamp),	fc.ecredf=fc.ecredf-(select SUM(case when deleted.erec>0 then deleted.erec-deleted.earred else 0 end) from deleted where deleted.fcstamp=fc.fcstamp),	fc.edebf=fc.edebf+(select SUM(case when deleted.erec<0 then deleted.erec-deleted.earred else 0 end) from deleted where deleted.fcstamp=fc.fcstamp),	fc.virsreg=fc.virsreg-(select SUM(deleted.virs) from deleted where deleted.fcstamp=fc.fcstamp),	fc.evirsreg=fc.evirsreg-(select SUM(deleted.evirs) from deleted where deleted.fcstamp=fc.fcstamp) from deleted where 	deleted.fcstamp=fc.fcstamp 	and deleted.process=1UPDATE FC set 	fc.evalpo=fc.evalpo-(select SUM(deleted.erec) from deleted where deleted.fcstamp=fc.fcstamp)	,fc.valpo=fc.valpo-(select SUM(deleted.escrec) from deleted where deleted.fcstamp=fc.fcstamp)	,fc.mvalpo=fc.mvalpo-(select SUM(deleted.rec) from deleted where deleted.fcstamp=fc.fcstamp),	fc.ultdoc=''	,fc.recibado=0 from deleted where 	fc.fcstamp=deleted.fcstampUPDATE FC set 	fc.irsdif=fc.irsdif-(select SUM(case when deleted.desconto=100 or po.fin=100 then 0 else (ROUND((deleted.virs/(1-(case when deleted.desconto<>0 then deleted.desconto else po.fin end/100))),0)-deleted.virs) end) from deleted inner join po on po.postamp=deleted.postamp where deleted.fcstamp=fc.fcstamp),	fc.eirsdif=fc.eirsdif-(select SUM(case when deleted.desconto=100 or po.fin=100 then 0 else (ROUND((deleted.evirs/(1-(case when deleted.desconto<>0 then deleted.desconto else po.fin end/100))),2)-deleted.evirs) end) from deleted inner join po on po.postamp=deleted.postamp where deleted.fcstamp=fc.fcstamp) from deleted 
inner join po on po.postamp=deleted.postamp 
where 
	fc.fcstamp=deleted.fcstamp 
	and deleted.process=1 
	and (deleted.desconto<>0 or po.fin<>0)

GO

