
/****** Object:  Trigger [dbo].[Tr_Sl_Delete]    Script Date: 09/07/2020 14:24:42 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[Tr_Sl_Delete] 
ON [dbo].[sl] FOR DELETE  
AS 






-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Sl_Delete'
	,@MROW int = @@ROWCOUNT

IF (@@ROWCOUNT = 0)
	RETURN;
	
SET NOCOUNT ON

SET @Msg += '<' + convert(varchar,getdate(),14) + '>'

IF @MROW = 1
BEGIN

	SET @Msg += '<SL (' + convert(varchar,getdate(),8) + ')>'

	-- Atualiza Stocks  - Alterado para ignorar a condição dos Documentos trf. de Armazém LL 25-05-2015
	set Context_Info 0x55556 -- evitar que o trigger de update corra validações desnecessárias

	UPDATE
		ST
	SET
		 st.stock	 = st.stock - case when deleted.cm < 50 then deleted.qtt else -deleted.qtt end
		,st.qttacin  = case when deleted.cm < 50 then -deleted.QTT+st.qttacin else st.qttacin end
		,st.qttacout = case when deleted.cm > 50 then -deleted.QTT+st.qttacout else st.qttacout end
		,st.epcpond  = case
					-- se o stock <= 0 colocar o pcp = 0
					when (st.stock - case when deleted.cm < 50 then deleted.qtt else -deleted.qtt end) <= 0 then 0
					-- se for entrada
					-- Cada entrada afeta o PCP ( total valor / total quantidade = PCP)
					when st.stock - deleted.qtt > 0 and deleted.cm < 50
						THEN (st.epcpond * (CASE WHEN st.stock > 0 THEN st.stock ELSE 0 END) - deleted.evu * deleted.qtt)
							/ ((case when st.stock > 0 then st.stock else 0 end) - deleted.qtt)
					-- se for saída
					else st.epcpond
					end
	    ,st.usrdata = getdate()
		,st.usrhora = CONVERT(CHAR(8),GETDATE(),108)
	FROM
		deleted
		inner join empresa_arm on empresa_arm.armazem = deleted.armazem
		inner join st on st.ref = deleted.ref and st.site_nr = empresa_arm.empresa_no
	
	if @@ROWCOUNT > 0
		SET @Msg += '<Update ST (' + convert(varchar,getdate(),8) + ')>'

	set Context_Info 0x0

	-- Atualiza stocks por armazem
	UPDATE
		SA
	SET
		sa.stock = sa.stock - case when deleted.cm < 50 then deleted.qtt else -deleted.qtt end
		,sa.qttacin = case when deleted.cm < 50 then -deleted.QTT + sa.qttacin else sa.qttacin end
	FROM
		deleted
		inner join sa on sa.ref = deleted.ref and sa.armazem = deleted.armazem

	if @@ROWCOUNT > 0
		SET @Msg += '<Update SA (' + convert(varchar,getdate(),8) + ')>'


	-- Atualiza stocks por lote
	IF exists (select lote from deleted where lote != '') -- se usa lotes
	begin
		update
			st_lotes
		set
			stock = stl.stock - case when deleted.cm < 50 then deleted.qtt else -deleted.qtt end
		from
			deleted
			inner join st_lotes stl on stl.lote=deleted.lote 
			and stl.ref = deleted.ref 
			and stl.armazem = deleted.armazem

		if @@ROWCOUNT > 0
			SET @Msg += '<Update ST_LOTES (' + convert(varchar,getdate(),8) + ')>'
	end

END 
ELSE BEGIN
	
	SET @Msg += '<ML (' + convert(varchar,getdate(),8) + ')>'
	
	-- update multi linha ST - Alterado para ignorar a condição dos Documentos trf. de Armazém LL 25-05-2015
	set Context_Info 0x55556 -- evitar que o trigger de update corra validações desnecessárias

	UPDATE
		ST
	SET
		st.stock = st.stock - isnull((SELECT SUM(case when deleted.cm<50 then deleted.qtt else -deleted.qtt end) FROM deleted WHERE deleted.ref=st.ref and empresa_arm.armazem = deleted.armazem),0)
								--case
								--when (SELECT SUM(case when deleted.cm<50 then deleted.qtt else -deleted.qtt end) FROM deleted WHERE deleted.ref=st.ref and empresa_arm.armazem = deleted.armazem) is null then 0 
								--else (SELECT SUM(case when deleted.cm<50 then deleted.qtt else -deleted.qtt end) FROM deleted WHERE deleted.ref=st.ref and empresa_arm.armazem = deleted.armazem) 
								--end
		,st.qttacout = st.qttacout - isnull((SELECT SUM(case when deleted.cm<50 then 0 else deleted.qtt end) FROM deleted WHERE deleted.ref=st.ref and empresa_arm.armazem = deleted.armazem),0)
									--case
									--	when (SELECT SUM(case when deleted.cm<50 then 0 else deleted.qtt end) FROM deleted WHERE deleted.ref=st.ref and empresa_arm.armazem = deleted.armazem) is null then 0
									--	else (SELECT SUM(case when deleted.cm<50 then 0 else deleted.qtt end) FROM deleted WHERE deleted.ref=st.ref and empresa_arm.armazem = deleted.armazem)
									--	end
		,st.qttacin = st.qttacin - isnull((SELECT SUM(case when deleted.cm<50 then deleted.qtt else 0 end) FROM deleted WHERE deleted.ref=st.ref and empresa_arm.armazem = deleted.armazem),0)
									--case
									--when (SELECT SUM(case when deleted.cm<50 then deleted.qtt else 0 end) FROM deleted WHERE deleted.ref=st.ref and empresa_arm.armazem = deleted.armazem) is null then 0
									--else (SELECT SUM(case when deleted.cm<50 then deleted.qtt else 0 end) FROM deleted WHERE deleted.ref=st.ref and empresa_arm.armazem = deleted.armazem)
									--end
		,st.epcpond = case
						when (SELECT SUM(case when deleted.cm<50 then deleted.qtt else -deleted.qtt end) FROM deleted WHERE deleted.ref=st.ref ) is null then st.epcpond
						-- se o stock <= 0 colocar o pcp = 0
						when (st.stock - (SELECT SUM(case when deleted.cm<50 then deleted.qtt else -deleted.qtt end) FROM deleted WHERE deleted.ref=st.ref)) <= 0 then 0
						-- se exitir entradas
						-- Cada entrada afeta o PCP (total valor / total quantidade = PCP)
						when st.stock - (SELECT SUM(deleted.qtt) FROM deleted WHERE deleted.ref=st.ref and deleted.cm < 50 and empresa_arm.armazem = deleted.armazem) > 0
							then (st.epcpond * (CASE WHEN st.stock > 0 THEN st.stock ELSE 0 END) - (SELECT SUM(deleted.ett) / sum(deleted.qtt) FROM deleted WHERE deleted.ref=st.ref and deleted.cm < 50 and empresa_arm.armazem = deleted.armazem) * (SELECT SUM(deleted.qtt) FROM deleted WHERE deleted.ref=st.ref and deleted.cm < 50 and empresa_arm.armazem = deleted.armazem))
								/ ((case when st.stock > 0 then st.stock else 0 end) - (SELECT SUM(deleted.qtt) FROM deleted WHERE deleted.ref=st.ref and deleted.cm < 50 and empresa_arm.armazem = deleted.armazem ))
						-- se só existir saídas
						else st.epcpond
						end
		,st.usrdata = getdate()
		,st.usrhora = CONVERT(CHAR(8),GETDATE(),108)
	FROM
		deleted
		inner join empresa_arm on empresa_arm.armazem = deleted.armazem
		inner join st on st.ref = deleted.ref and st.site_nr = empresa_arm.empresa_no
	--WHERE
		/*and st.ref IN (SELECT ref FROM deleted group by ref having SUM(qtt)!=0)*/

	if @@ROWCOUNT > 0
		SET @Msg += '<Update ST (' + convert(varchar,getdate(),8) + ')>'

	set Context_Info 0x0

	-- update multi linha SA
	UPDATE
		SA
	SET
		sa.stock = sa.stock - isnull((SELECT SUM(case when deleted.cm<50 then deleted.qtt else -deleted.qtt end) FROM deleted WHERE deleted.ref=sa.ref and deleted.armazem=sa.armazem),0)
							--case
							--	when (SELECT SUM(case when deleted.cm<50 then deleted.qtt else -deleted.qtt end) FROM deleted WHERE deleted.ref=sa.ref and deleted.armazem=sa.armazem) is null then 0
							--	else (SELECT SUM(case when deleted.cm<50 then deleted.qtt else -deleted.qtt end) FROM deleted WHERE deleted.ref=sa.ref and deleted.armazem=sa.armazem )
							--	end
		,sa.qttacin = sa.qttacin - isnull((SELECT SUM(case when deleted.cm<50 then deleted.qtt else 0 end) FROM deleted WHERE deleted.ref=sa.ref and deleted.armazem=sa.armazem ),0)
									--case
									--when (SELECT SUM(case when deleted.cm<50 then deleted.qtt else 0 end) FROM deleted WHERE deleted.ref=sa.ref and deleted.armazem=sa.armazem ) is null then 0
									--else (SELECT SUM(case when deleted.cm<50 then deleted.qtt else 0 end) FROM deleted WHERE deleted.ref=sa.ref and deleted.armazem=sa.armazem )
									--end
	from
		deleted
		inner join sa on deleted.ref=sa.ref and deleted.armazem=sa.armazem

	if @@ROWCOUNT > 0
		SET @Msg += '<Update SA (' + convert(varchar,getdate(),8) + ')>'


	-- Criar na st_lotes as referências/lotes por armazem que não existirem
	IF exists (select lote from deleted where lote != '') -- se usa lotes
	begin
		update
			st_lotes
		set
			stock = stock - isnull((SELECT SUM(case when d.cm<50 then d.qtt else -d.qtt end) FROM deleted d WHERE d.ref=stl.ref and d.armazem=stl.armazem and d.lote=stl.lote),0)
								--case
								--when (SELECT SUM(case when d.cm<50 then d.qtt else -d.qtt end) FROM deleted d WHERE d.ref=stl.ref and d.armazem=stl.armazem and d.lote=stl.lote) is null then 0 
								--else (SELECT SUM(case when d.cm<50 then d.qtt else -d.qtt end) FROM deleted d WHERE d.ref=stl.ref and d.armazem=stl.armazem and d.lote=stl.lote) 
								--end
		from
			deleted
			inner join st_lotes stl on stl.lote=deleted.lote and stl.ref=deleted.ref and stl.armazem=deleted.armazem

		if @@ROWCOUNT > 0
			SET @Msg += '<Update ST_LOTES (' + convert(varchar,getdate(),8) + ')>'
	END
END


-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg);