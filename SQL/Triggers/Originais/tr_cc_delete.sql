USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_cc_delete]    Script Date: 06/12/2014 10:06:47 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE TRIGGER [dbo].[tr_cc_delete] ON [dbo].[cc] FOR DELETE AS SET NOCOUNT ONUPDATE b_utentes SET	b_utentes.esaldo = b_utentes.esaldo - (											select 												sum(deleted.edeb-deleted.ecred) 											from deleted 											where 												deleted.no = b_utentes.no												and deleted.estab = b_utentes.estab											)from deletedwhere	b_utentes.no = deleted.no	and b_utentes.estab = deleted.estabif (select count(*) from deleted where RTRIM(LTRIM(deleted.occstamp)) = '') > 0  UPDATE CC SET 	cc.debf = cc.debf-deleted.cred	,cc.credf = cc.credf-deleted.deb	,cc.edebf = cc.edebf-deleted.ecred	,cc.ecredf = cc.ecredf-deleted.edeb from deleted where 	cc.cm=deleted.cr 	and cc.nrdoc=deleted.docref 	and cc.no=deleted.noif (select count(*) from deleted where RTRIM(LTRIM(deleted.occstamp)) <> '') > 0  UPDATE CC SET 	cc.debf = cc.debf-deleted.cred	,cc.credf = cc.credf-deleted.deb	,cc.edebf = cc.edebf-deleted.ecred	,cc.ecredf = cc.ecredf-deleted.edeb from deleted where 	cc.cm=deleted.cr 	and cc.nrdoc=deleted.docref 	and cc.no=deleted.no 	and cc.ccstamp = deleted.occstamp
GO

