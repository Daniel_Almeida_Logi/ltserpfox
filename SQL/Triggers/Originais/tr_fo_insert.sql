USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_fo_insert]    Script Date: 06/12/2014 10:11:34 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE TRIGGER [dbo].[tr_fo_insert] ON [dbo].[fo]  FOR INSERT  AS SET NOCOUNT ON
INSERT INTO FC 
	(
	tpdesc
	,tpstamp
	,fcstamp
	,fostamp
	,usrdata
	,usrhora
	,usrinis
	,ousrdata
	,ousrhora
	,ousrinis
	,datalc
	,no
	,nome
	,cm
	,cmdesc
	,origem
	,ecred
	,edeb
	,credm
	,debm
	,aprovado
	,nmaprov
	,dtaprov
	,eivav1
	,eivav2
	,eivav3
	,eivav4
	,eivav5
	,eivav6
	,eivav7
	,eivav8
	,eivav9
	,IVATX1
	,IVATX2
	,IVATX3
	,IVATX4
	,IVATX5
	,IVATX6
	,IVATX7
	,IVATX8
	,IVATX9
	,REEXGIVA
	,formapag
	,moeda
	,adoc
	,dataven
	,fref
	,cambiofixo
	,ccusto
	,ncusto
	,intid
	,tipo
	,estab
	,pais
	,zona
	,virs
	,evirs
	,crend
	,obs
	)
select 
	inserted.tpdesc
	,inserted.tpstamp
	,inserted.fostamp
	,inserted.fostamp
	,inserted.usrdata
	,inserted.usrhora
	,inserted.usrinis
	,inserted.ousrdata
	,inserted.ousrhora
	,inserted.ousrinis
	,inserted.data
	,inserted.no
	,inserted.nome
	,inserted.doccode
	,inserted.docnome
	,'FO'
	,case when cm1.debito=1 then 0 else ( case when inserted.pais=2 or fo2.ettinvsuj > 0 then inserted.etotal-(case when inserted.pais=2 then inserted.ettiva else (ISNULL(fo2.ettinvsuj,0)) end) else inserted.etotal end ) end
	,case when cm1.debito=1 then ( case when inserted.pais=2 or fo2.ettinvsuj > 0 then inserted.etotal-(case when inserted.pais=2 then inserted.ettiva else (ISNULL(fo2.ettinvsuj,0)) end) else inserted.etotal end ) else 0 end
	,case when cm1.debito=1 then 0 else ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.totmoeda-inserted.tmiva else inserted.totmoeda end ) end 
	,case when cm1.debito=1 then ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.totmoeda-inserted.tmiva else inserted.totmoeda end ) else 0 end
	,inserted.aprovado
	,inserted.nmaprov
	,inserted.dtaprov
	,inserted.eivav1
	,inserted.eivav2
	,inserted.eivav3
	,inserted.eivav4
	,inserted.eivav5
	,inserted.eivav6
	,inserted.eivav7
	,inserted.eivav8,inserted.eivav9
	,ISNULL(fo2.IVATX1,0)
	,ISNULL(fo2.IVATX2,0)
	,ISNULL(fo2.IVATX3,0)
	,ISNULL(fo2.IVATX4,0)
	,ISNULL(fo2.IVATX5,0)
	,ISNULL(fo2.IVATX6,0)
	,ISNULL(fo2.IVATX7,0)
	,ISNULL(fo2.IVATX8,0)
	,ISNULL(fo2.IVATX9,0)
	,ISNULL(fo2.REEXGIVA,0)
	,'1'
	,inserted.moeda
	,inserted.adoc
	,inserted.pdata
	,inserted.fref
	,inserted.cambiofixo
	,inserted.ccusto
	,inserted.ncusto
	,inserted.intid
	,inserted.tipo
	,inserted.estab
	,inserted.pais
	,inserted.zona
	,case when cm1.debito=1 then inserted.virs*-1 else inserted.virs end
	,case when cm1.debito=1 then inserted.evirs*-1 else inserted.evirs end
	,inserted.crend
	,inserted.final
from inserted 
inner join cm1 on cm1.cm=inserted.doccode 
inner join fo2 on fo2.fo2stamp=inserted.fostamp 
where 
	cm1.folanfc=1 
	and inserted.multi=0
	
UPDATE FC 
set 
	edebf=(case when cm1.debito=1 then ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.etotal-(case when inserted.pais=2 then inserted.ettiva else (ISNULL(fo2.ettinvsuj,0)) end) else inserted.etotal end ) else 0 end )
	,debfm=(case when cm1.debito=1 then ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.totmoeda-inserted.tmiva else inserted.totmoeda end ) else 0 end)
	,ecredf=(case when cm1.debito=1 then 0 else ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.etotal-(case when inserted.pais=2 then inserted.ettiva else (ISNULL(fo2.ettinvsuj,0)) end) else inserted.etotal end ) end)
	,credfm=(case when cm1.debito=1 then 0 else ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.totmoeda-inserted.tmiva else inserted.totmoeda end ) end) 
	,virsreg=(case when cm1.debito=1 then inserted.virs*-1 else inserted.virs end)
	,evirsreg=(case when cm1.debito=1 then inserted.evirs*-1 else inserted.evirs end)
	,obs=inserted.final
from inserted 
inner join cm1 on cm1.cm=inserted.doccode 
inner join fo2 on fo2.fo2stamp=inserted.fostamp  
inner join fc on fc.fostamp=inserted.fostamp 
where 
	cm1.folanfc=1 
	and cm1.fcautoreg=1
	
INSERT INTO FC 
	(
	tpdesc
	,tpstamp
	,fcstamp
	,fostamp
	,usrdata
	,usrhora
	,usrinis
	,ousrdata
	,ousrhora
	,ousrinis
	,datalc
	,no
	,nome
	,cm
	,cmdesc
	,origem
	,ecred
	,edeb
	,credm
	,debm
	,ecredf
	,edebf
	,credfm
	,debfm
	,aprovado
	,nmaprov
	,dtaprov
	,eivav1
	,eivav2
	,eivav3
	,eivav4
	,eivav5
	,eivav6
	,eivav7
	,eivav8
	,eivav9
	,IVATX1
	,IVATX2
	,IVATX3
	,IVATX4
	,IVATX5
	,IVATX6
	,IVATX7
	,IVATX8
	,IVATX9
	,REEXGIVA
	,formapag
	,moeda
	,adoc
	,dataven
	,fref
	,cambiofixo
	,ccusto
	,ncusto
	,intid
	,tipo
	,estab
	,pais
	,zona
	,crend
	,obs
	)
select 
	inserted.tpdesc
	,inserted.tpstamp
	,'R'+inserted.fostamp
	,inserted.fostamp
	,inserted.usrdata
	,inserted.usrhora
	,inserted.usrinis
	,inserted.ousrdata
	,inserted.ousrhora
	,inserted.ousrinis
	,inserted.data
	,inserted.no
	,inserted.nome
	,cm1.cmfcautoreg
	,cm1.cmfcautoregn
	,'FO'
	,case when cm1.debito=1 then ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.etotal-(case when inserted.pais=2 then inserted.ettiva else (ISNULL(fo2.ettinvsuj,0)) end) else inserted.etotal end ) else 0 end
	,case when cm1.debito=1 then 0 else ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.etotal-(case when inserted.pais=2 then inserted.ettiva else (ISNULL(fo2.ettinvsuj,0)) end) else inserted.etotal end ) end
	,case when cm1.debito=1 then ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.totmoeda-inserted.tmiva else inserted.totmoeda end ) else 0 end 
	,case when cm1.debito=1 then 0 else ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.totmoeda-inserted.tmiva else inserted.totmoeda end ) end
	,case when cm1.debito=1 then ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.etotal-(case when inserted.pais=2 then inserted.ettiva else (ISNULL(fo2.ettinvsuj,0)) end) else inserted.etotal end ) else 0 end
	,case when cm1.debito=1 then 0 else ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.etotal-(case when inserted.pais=2 then inserted.ettiva else (ISNULL(fo2.ettinvsuj,0)) end) else inserted.etotal end ) end
	,case when cm1.debito=1 then ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.totmoeda-inserted.tmiva else inserted.totmoeda end ) else 0 end 
	,case when cm1.debito=1 then 0 else ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.totmoeda-inserted.tmiva else inserted.totmoeda end ) end
	,inserted.aprovado
	,inserted.nmaprov
	,inserted.dtaprov
	,inserted.eivav1
	,inserted.eivav2
	,inserted.eivav3
	,inserted.eivav4
	,inserted.eivav5
	,inserted.eivav6
	,inserted.eivav7
	,inserted.eivav8
	,inserted.eivav9
	,ISNULL(fo2.IVATX1,0)
	,ISNULL(fo2.IVATX2,0)
	,ISNULL(fo2.IVATX3,0)
	,ISNULL(fo2.IVATX4,0)
	,ISNULL(fo2.IVATX5,0)
	,ISNULL(fo2.IVATX6,0)
	,ISNULL(fo2.IVATX7,0)
	,ISNULL(fo2.IVATX8,0)
	,ISNULL(fo2.IVATX9,0)
	,ISNULL(fo2.REEXGIVA,0)
	,'1'
	,inserted.moeda
	,inserted.adoc
	,inserted.pdata
	,inserted.fref
	,inserted.cambiofixo
	,inserted.ccusto
	,inserted.ncusto
	,inserted.intid
	,inserted.tipo
	,inserted.estab
	,inserted.pais
	,inserted.zona
	,inserted.crend
	,inserted.final
from inserted 
inner join cm1 on cm1.cm=inserted.doccode 
inner join fo2 on fo2.fo2stamp=inserted.fostamp 
where 
	cm1.folanfc=1 
	and cm1.fcautoreg=1 
	and inserted.multi=0
	
IF (select COUNT(*) from inserted inner join fc on fc.fostamp = inserted.fostamp inner join fo2 on fo2.fo2stamp=inserted.fostamp where fo2.anulado=1)>0	
UPDATE FC 
set 
	deb = 0, edeb = 0, debf = 0, edebf = 0,debfm = 0,
	cred = 0, ecred = 0, credf = 0, ecredf = 0, credfm = 0,
	ivav1 = 0, ivav2 = 0, ivav3 = 0, ivav4 = 0, ivav5 = 0, ivav6 = 0, ivav7 = 0, ivav8 = 0, ivav9 = 0,
	eivav1 = 0, eivav2 = 0, eivav3 = 0, eivav4 = 0, eivav5 = 0, eivav6 = 0, eivav7 = 0, eivav8 = 0, eivav9 = 0,
	IVATX1 = 0, IVATX2 = 0, IVATX3 = 0, IVATX4 = 0, IVATX5 = 0, IVATX6 = 0, IVATX7 = 0, IVATX8 = 0, IVATX9 = 0,
	reexgiva = 0, virs = 0, evirs = 0, 
	virsreg = 0, evirsreg = 0,
	difcambio = 0, edifcambio = 0 
from inserted 
inner join fo2 on fo2.fo2stamp=inserted.fostamp
inner join fc on fc.fostamp = inserted.fostamp 
where 
	fo2.anulado = 1 			
GO

