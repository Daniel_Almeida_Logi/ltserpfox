USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_ftreg_insert]    Script Date: 06/12/2014 10:15:59 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE TRIGGER [dbo].[tr_ftreg_insert] ON [dbo].[ftreg]  FOR INSERT  AS SET NOCOUNT ONUPDATE CC set 	cc.debf=(case when cc.debf+round(inserted.vciva,0)>=cc.deb then cc.deb else cc.debf+round(inserted.vciva,0) end), 	cc.edebf=(case when cc.edebf+round(inserted.evciva,2)>=cc.edeb then cc.edeb else cc.edebf+round(inserted.evciva,2) end), 	cc.debfm=(case when ROUND(cc.debfm+inserted.vmoeda,2)>=cc.debm then cc.debm else cc.debfm+inserted.vmoeda end), 	cc.ultdoc=inserted.ultdoc from inserted where 	cc.ftstamp=inserted.ftregstamp
GO

