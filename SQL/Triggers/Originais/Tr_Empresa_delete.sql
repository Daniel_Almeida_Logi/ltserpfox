SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[Tr_Empresa_DELETE]') IS NOT NULL
	drop trigger dbo.Tr_Empresa_DELETE
go

CREATE TRIGGER [dbo].[Tr_Empresa_DELETE] 
ON [dbo].[Empresa] FOR DELETE 
AS

IF (@@ROWCOUNT = 0) 
	RETURN ;

SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE 
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Empresa_Update'	


set @Msg = '<' + convert(varchar,getdate(),14) + '>'




BEGIN
declare @no as varchar(10) = (select no from deleted)

delete from ag where @no = ag.no


	if @@ROWCOUNT > 0
		SET @Msg += '<Update SL (' + convert(varchar,getdate(),8) + ')>'
END



-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg) ;
