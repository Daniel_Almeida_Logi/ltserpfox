USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_fc_insert]    Script Date: 06/12/2014 10:14:28 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE TRIGGER [dbo].[tr_fc_insert] ON [dbo].[fc]  FOR INSERT  AS SET NOCOUNT ON
declare @m_banchab char(60), @m_contado float
select @m_banchab = (select valor from para1 where descricao='m_banchab')
SELECT @m_contado = (SELECT TOP 1 noconta FROM bl WHERE bl.banco = SUBSTRING(@m_banchab, 1, 10) AND bl.conta = SUBSTRING(@m_banchab, 12, 20))

if @m_contado = NULL
select @m_contado = 1

if @m_banchab = NULL
select @m_banchab = ''

UPDATE FL 
SET
	fl.esaldo=fl.esaldo+inserted.ecred-inserted.edeb+inserted.edifcambio
from INSERTED 
where 
	fl.no=inserted.no

IF UPDATE(no) or UPDATE(cred) or UPDATE(deb) or UPDATE(ecred) or UPDATE(edeb) or UPDATE(cr) or UPDATE(cradoc) 
UPDATE fc 
SET
	fc.credf = fc.credf+inserted.deb
	,fc.debf = fc.debf+inserted.cred
	,fc.ecredf = fc.ecredf+inserted.edeb
	,fc.edebf = fc.edebf+inserted.ecred
from INSERTED 
where 
	fc.cm=inserted.cr 
	and fc.adoc=inserted.cradoc 
	and fc.no=inserted.no
	and inserted.cr > 0 
	and inserted.cradoc <> SPACE(10)
	
if (select COUNT(*) from inserted where inserted.formapag='')>0
UPDATE fc 
SET 
	formapag='1' 
from inserted
WHERE 
	fc.fcstamp=inserted.fcstamp 
	AND fc.formapag=''

if (select COUNT(*) from inserted where inserted.situacao='')>0
UPDATE fc 
SET 
	situacao = case when inserted.cred<>inserted.credf OR inserted.deb<>inserted.debf OR inserted.ecred<>inserted.ecredf OR inserted.edeb<>inserted.edebf
					then '1' 
					ELSE '2' 
				end 
from inserted
WHERE 
	fc.fcstamp=inserted.fcstamp 
	AND fc.situacao=''			
GO

