USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_cm2_update]    Script Date: 06/12/2014 10:13:46 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE TRIGGER [dbo].[tr_cm2_update] ON [dbo].[cm2]  FOR UPDATE  AS SET NOCOUNT ONIF UPDATE (cmdesc)BEGIN	update cm1 	set 		cm1.fosln = inserted.cmdesc 	from inserted  	where 		cm1.fosl=inserted.cm 		update ts 	set 		ts.cmdesc = inserted.cmdesc 	from inserted  	where 		ts.cmstocks=inserted.cm 			update ts 	set 		ts.cm2desc = inserted.cmdesc 	from inserted  	where 		ts.cm2stocks=inserted.cm 		update td 	set 		td.cmsln = inserted.cmdesc 	from inserted  	where 		td.cmsl=inserted.cm 			update sl 	set 		sl.cmdesc = inserted.cmdesc 	from inserted  	where 		sl.cm=inserted.cm END
GO

