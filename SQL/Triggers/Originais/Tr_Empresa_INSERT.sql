SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[Tr_Empresa_INSERT]') IS NOT NULL
	drop trigger dbo.Tr_Empresa_INSERT
go

CREATE TRIGGER [dbo].[Tr_Empresa_INSERT] 
ON [dbo].[Empresa] FOR INSERT 
AS

IF (@@ROWCOUNT = 0) 
	RETURN ;

SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE 
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Empresa_Update'	


set @Msg = '<' + convert(varchar,getdate(),14) + '>'




BEGIN


INSERT INTO [dbo].[ag]
           ([agstamp]
           ,[nome]
           ,[no]
           ,[ncont]
           ,[morada]
           ,[local]
           ,[codpost]
           ,[telefone]
           ,[fax]
           ,[codigo]
           ,[email]
           ,[tlmvl]
           ,[inactivo]
           ,[ousrinis]
           ,[ousrdata]
           ,[ousrhora]
           ,[usrinis]
           ,[usrdata]
           ,[usrhora]
           ,[exportado])
     select
           LEFT(NEWID(),15) + RIGHT(NEWID(),10)
           ,inserted.nomecomp
           ,inserted.no
           ,inserted.ncont
           ,left(inserted.morada,55)
           ,inserted.local
           ,inserted.codpost
           ,inserted.telefone
           ,inserted.fax
           ,inserted.codigo
           ,inserted.email
           ,''
           ,0
           ,inserted.ousrinis
           ,inserted.ousrdata
           ,inserted.ousrhora
           ,inserted.usrinis
           ,inserted.usrdata
           ,inserted.usrhora
           ,0
		   from 
		   inserted


	if @@ROWCOUNT > 0
		SET @Msg += '<Update SL (' + convert(varchar,getdate(),8) + ')>'
END



-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg) ;
