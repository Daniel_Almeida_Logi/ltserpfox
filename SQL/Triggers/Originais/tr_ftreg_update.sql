USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_ftreg_update]    Script Date: 06/12/2014 10:16:08 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE TRIGGER [dbo].[tr_ftreg_update] ON [dbo].[ftreg]  FOR UPDATE  AS SET NOCOUNT ONdeclare @m_debfm numeric(19,6)declare @m_ftregstamp varchar(25)declare @m_vmoeda numeric(19,6)select @m_ftregstamp=(select top 1 deleted.ftregstamp from deleted group by deleted.ftregstamp)select @m_vmoeda=(select top 1 sum(inserted.vmoeda) from inserted group by inserted.ftregstamp)select @m_debfm=(select sum(ftreg.vmoeda) from ftreg where ftreg.ftregstamp=@m_ftregstamp)-@m_vmoedaUPDATE CC set 	cc.debf=(case when cc.debf-round(deleted.vciva,0)<0 then 0 else cc.debf-round(deleted.vciva,0) end) , 	cc.edebf=(case when cc.edebf-round(deleted.evciva,2 )<0 then 0 else cc.edebf-round(deleted.evciva,2 ) end), 	cc.debfm=(case when @m_debfm<0 then 0 else @m_debfm end), 	cc.ultdoc='' from deleted where 	cc.ftstamp=deleted.ftregstampUPDATE CC set 	cc.debf=(case when cc.debf+round(inserted.vciva,0)>=cc.deb then cc.deb else cc.debf+round(inserted.vciva,0) end), 	cc.edebf=(case when cc.edebf+round(inserted.evciva,2)>=cc.edeb then cc.edeb else cc.edebf+round(inserted.evciva,2) end), 	cc.debfm=(case when ROUND(cc.debfm+inserted.vmoeda,2)>=cc.debm then cc.debm else cc.debfm+inserted.vmoeda end), 	cc.ultdoc=inserted.ultdoc from inserted where 	cc.ftstamp=inserted.ftregstamp
GO

