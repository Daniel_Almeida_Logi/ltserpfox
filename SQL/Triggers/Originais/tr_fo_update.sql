USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_fo_update]    Script Date: 06/12/2014 10:11:41 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE TRIGGER [dbo].[tr_fo_update] ON [dbo].[fo]  FOR UPDATE  AS SET NOCOUNT ON	
DELETE FROM FC 
from deleted
where 
	fc.fostamp = deleted.fostamp 
	and deleted.fostamp not in (select inserted.fostamp from inserted)
	
DELETE FROM FC 
from inserted 
inner join cm1 on inserted.doccode=cm1.cm
where 
	fc.fostamp = inserted.fostamp 
	and cm1.folanfc=0
	
DELETE from FC 
from deleted
where 
	fc.fostamp = deleted.fostamp 
	and deleted.multi=0 
	and deleted.fostamp not in (select inserted.fostamp from inserted where inserted.multi=0)

DELETE from FC 
from deleted
where 
	fc.fcstamp = 'R'+deleted.fostamp 
	and deleted.multi=0

UPDATE FC
set 
	fc.usrdata=inserted.usrdata
	,fc.usrhora=inserted.usrhora
	,fc.usrinis=inserted.usrinis
	,fc.tpdesc=inserted.tpdesc
	,fc.tpstamp=inserted.tpstamp
	,fc.datalc=inserted.data
	,fc.no=inserted.no
	,fc.nome=inserted.nome
	,fc.cm=inserted.doccode
	,fc.cmdesc=inserted.docnome
	,fc.moeda=inserted.moeda
	,fc.adoc=inserted.adoc
	,fc.dataven=inserted.pdata
	,fc.fref=inserted.fref
	,fc.cambiofixo=inserted.cambiofixo
	,fc.ccusto=inserted.ccusto
	,fc.ncusto=inserted.ncusto
	,fc.intid=inserted.intid
	,fc.tipo=inserted.tipo
	,fc.estab=inserted.estab
	,fc.pais=inserted.pais
	,fc.zona=inserted.zona
	,fc.aprovado=inserted.aprovado
	,fc.nmaprov=inserted.nmaprov
	,fc.dtaprov=inserted.dtaprov
	,fc.eivav1=inserted.eivav1
	,fc.eivav2=inserted.eivav2
	,fc.eivav3=inserted.eivav3
	,fc.eivav4=inserted.eivav4
	,fc.eivav5=inserted.eivav5
	,fc.eivav6=inserted.eivav6
	,fc.eivav7=inserted.eivav7
	,fc.eivav8=inserted.eivav8
	,fc.eivav9=inserted.eivav9
	,fc.IVATX1=ISNULL(fo2.IVATX1,0)
	,fc.IVATX2=ISNULL(fo2.IVATX2,0)
	,fc.IVATX3=ISNULL(fo2.IVATX3,0)
	,fc.IVATX4=ISNULL(fo2.IVATX4,0)
	,fc.IVATX5=ISNULL(fo2.IVATX5,0)
	,fc.IVATX6=ISNULL(fo2.IVATX6,0)
	,fc.IVATX7=ISNULL(fo2.IVATX7,0)
	,fc.IVATX8=ISNULL(fo2.IVATX8,0)
	,fc.IVATX9=ISNULL(fo2.IVATX9,0)
	,fc.REEXGIVA=ISNULL(fo2.REEXGIVA,0)
	,fc.formapag='1'
	,fc.virs=case when cm1.debito=1 then inserted.virs*-1 else inserted.virs end
	,fc.evirs=case when cm1.debito=1 then inserted.evirs*-1 else inserted.evirs end
	,fc.ecred=case when cm1.debito=1 then 0 else ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.etotal-(case when inserted.pais=2 then inserted.ettiva else (ISNULL(fo2.ettinvsuj,0)) end) else inserted.etotal end ) end	
	,fc.edeb=case when cm1.debito=1 then ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.etotal-(case when inserted.pais=2 then inserted.ettiva else (ISNULL(fo2.ettinvsuj,0)) end) else inserted.etotal end ) else 0 end
	,fc.credm=case when cm1.debito=1 then 0 else ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.totmoeda-inserted.tmiva else inserted.totmoeda end ) end
	,fc.debm=case when cm1.debito=1 then ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.totmoeda-inserted.tmiva else inserted.totmoeda end ) else 0 end
	,fc.crend=inserted.crend
	,fc.obs=inserted.final
from inserted 
inner join cm1 on inserted.doccode=cm1.cm 
inner join fo2 on inserted.fostamp=fo2.fo2stamp 
where 
	inserted.fostamp=fc.fostamp 
	and inserted.multi=0 
	and cm1.folanfc=1
	and inserted.fostamp IN (select deleted.fostamp from deleted where deleted.multi=0)

INSERT INTO FC 
	(
	tpdesc
	,tpstamp
	,fcstamp
	,fostamp
	,usrdata
	,usrhora
	,usrinis
	,ousrdata
	,ousrhora
	,ousrinis
	,datalc
	,no
	,nome
	,cm
	,cmdesc
	,origem
	,ecred
	,edeb
	,credm
	,debm
	,aprovado
	,nmaprov
	,dtaprov
	,eivav1
	,eivav2
	,eivav3
	,eivav4
	,eivav5
	,eivav6
	,eivav7
	,eivav8
	,eivav9
	,IVATX1
	,IVATX2
	,IVATX3
	,IVATX4
	,IVATX5
	,IVATX6
	,IVATX7
	,IVATX8
	,IVATX9
	,REEXGIVA
	,formapag
	,moeda
	,adoc
	,dataven
	,fref
	,cambiofixo
	,ccusto
	,ncusto
	,intid
	,tipo
	,estab
	,pais
	,zona
	,virs
	,evirs
	,crend
	,obs
	)
select 
	inserted.tpdesc
	,inserted.tpstamp
	,inserted.fostamp
	,inserted.fostamp
	,inserted.usrdata
	,inserted.usrhora
	,inserted.usrinis
	,inserted.ousrdata
	,inserted.ousrhora
	,inserted.ousrinis
	,inserted.data
	,inserted.no
	,inserted.nome
	,inserted.doccode
	,inserted.docnome
	,'FO'
	,case when cm1.debito=1 then 0 else ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.etotal-(case when inserted.pais=2 then inserted.ettiva else (ISNULL(fo2.ettinvsuj,0)) end) else inserted.etotal end ) end
	,case when cm1.debito=1 then ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.etotal-(case when inserted.pais=2 then inserted.ettiva else (ISNULL(fo2.ettinvsuj,0)) end) else inserted.etotal end ) else 0 end
	,case when cm1.debito=1 then 0 else ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.totmoeda-inserted.tmiva else inserted.totmoeda end ) end
	,case when cm1.debito=1 then ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.totmoeda-inserted.tmiva else inserted.totmoeda end ) else 0 end 
	,inserted.aprovado
	,inserted.nmaprov
	,inserted.dtaprov
	,inserted.eivav1
	,inserted.eivav2
	,inserted.eivav3
	,inserted.eivav4
	,inserted.eivav5
	,inserted.eivav6
	,inserted.eivav7
	,inserted.eivav8
	,inserted.eivav9
	,ISNULL(fo2.IVATX1,0)
	,ISNULL(fo2.IVATX2,0)
	,ISNULL(fo2.IVATX3,0)
	,ISNULL(fo2.IVATX4,0)
	,ISNULL(fo2.IVATX5,0)
	,ISNULL(fo2.IVATX6,0)
	,ISNULL(fo2.IVATX7,0)
	,ISNULL(fo2.IVATX8,0)
	,ISNULL(fo2.IVATX9,0)
	,ISNULL(fo2.REEXGIVA,0)
	,'1'
	,inserted.moeda
	,inserted.adoc
	,inserted.pdata
	,inserted.fref
	,inserted.cambiofixo
	,inserted.ccusto
	,inserted.ncusto
	,inserted.intid
	,inserted.tipo
	,inserted.estab
	,inserted.pais
	,inserted.zona
	,case when cm1.debito=1 then inserted.virs*-1 else inserted.virs end
	,case when cm1.debito=1 then inserted.evirs*-1 else inserted.evirs end
	,inserted.crend
	,inserted.final
from inserted 
inner join cm1 on inserted.doccode=cm1.cm 
inner join fo2 on fo2.fo2stamp=inserted.fostamp 
where 
	inserted.multi=0 
	and cm1.folanfc=1 
	and not EXISTS(select fostamp from fc where fc.fcstamp=inserted.fostamp)

IF (select COUNT(*) from inserted inner join fo2 on inserted.fostamp=fo2.fo2stamp)>0
begin
	DELETE FROM sl
	from inserted
	inner join fo2 on inserted.fostamp=fo2.fo2stamp
	inner join fn on fn.fostamp=inserted.fostamp
	inner join sl on fn.fnstamp=sl.fnstamp
	where 
		fo2.anulado=1
end
	
UPDATE FC 
set 
	edebf=(case when cm1.debito=1 then ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.etotal-(case when inserted.pais=2 then inserted.ettiva else (ISNULL(fo2.ettinvsuj,0)) end) else inserted.etotal end ) else 0 end )
	,debfm=(case when cm1.debito=1 then ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.totmoeda-inserted.tmiva else inserted.totmoeda end ) else 0 end)
	,ecredf=(case when cm1.debito=1 then 0 else ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.etotal-(case when inserted.pais=2 then inserted.ettiva else (ISNULL(fo2.ettinvsuj,0)) end) else inserted.etotal end ) end)
	,credfm=(case when cm1.debito=1 then 0 else ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.totmoeda-inserted.tmiva else inserted.totmoeda end ) end) 
	,virsreg=(case when cm1.debito=1 then inserted.virs*-1 else inserted.virs end)
	,evirsreg=(case when cm1.debito=1 then inserted.evirs*-1 else inserted.evirs end)
	,obs=inserted.final
from inserted inner join cm1 on cm1.cm=inserted.doccode 
inner join fo2 on fo2.fo2stamp=inserted.fostamp 
inner join fc on fc.fostamp=inserted.fostamp 
where 
	cm1.folanfc=1 
	and cm1.fcautoreg=1
	
INSERT INTO FC 
	(
	tpdesc
	,tpstamp
	,fcstamp
	,fostamp
	,usrdata
	,usrhora
	,usrinis
	,ousrdata
	,ousrhora
	,ousrinis
	,datalc
	,no
	,nome
	,cm
	,cmdesc
	,origem
	,ecred
	,edeb
	,ecredf
	,edebf
	,aprovado
	,nmaprov
	,dtaprov
	,eivav1
	,eivav2
	,eivav3
	,eivav4
	,eivav5
	,eivav6
	,eivav7
	,eivav8
	,eivav9
	,IVATX1
	,IVATX2
	,IVATX3
	,IVATX4
	,IVATX5
	,IVATX6
	,IVATX7
	,IVATX8
	,IVATX9
	,REEXGIVA
	,formapag
	,moeda
	,adoc
	,dataven
	,fref
	,cambiofixo
	,ccusto
	,ncusto
	,intid
	,tipo
	,estab
	,pais
	,zona
	,crend
	,obs
	)
select 
	inserted.tpdesc
	,inserted.tpstamp
	,'R'+inserted.fostamp
	,inserted.fostamp
	,inserted.usrdata
	,inserted.usrhora
	,inserted.usrinis
	,inserted.ousrdata
	,inserted.ousrhora
	,inserted.ousrinis
	,inserted.data
	,inserted.no
	,inserted.nome
	,cm1.cmfcautoreg
	,cm1.cmfcautoregn
	,'FO'
	,case when cm1.debito=1 then ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.etotal-(case when inserted.pais=2 then inserted.ettiva else (ISNULL(fo2.ettinvsuj,0)) end) else inserted.etotal end ) else 0 end
	,case when cm1.debito=1 then 0 else ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.etotal-(case when inserted.pais=2 then inserted.ettiva else (ISNULL(fo2.ettinvsuj,0)) end) else inserted.etotal end ) end
	,case when cm1.debito=1 then ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.etotal-(case when inserted.pais=2 then inserted.ettiva else (ISNULL(fo2.ettinvsuj,0)) end) else inserted.etotal end ) else 0 end
	,case when cm1.debito=1 then 0 else ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.etotal-(case when inserted.pais=2 then inserted.ettiva else (ISNULL(fo2.ettinvsuj,0)) end) else inserted.etotal end ) end
	,inserted.aprovado
	,inserted.nmaprov
	,inserted.dtaprov
	,inserted.eivav1
	,inserted.eivav2
	,inserted.eivav3
	,inserted.eivav4
	,inserted.eivav5
	,inserted.eivav6
	,inserted.eivav7
	,inserted.eivav8
	,inserted.eivav9
	,ISNULL(fo2.IVATX1,0)
	,ISNULL(fo2.IVATX2,0)
	,ISNULL(fo2.IVATX3,0)
	,ISNULL(fo2.IVATX4,0)
	,ISNULL(fo2.IVATX5,0)
	,ISNULL(fo2.IVATX6,0)
	,ISNULL(fo2.IVATX7,0)
	,ISNULL(fo2.IVATX8,0)
	,ISNULL(fo2.IVATX9,0)
	,ISNULL(fo2.REEXGIVA,0)
	,'1'
	,inserted.moeda
	,inserted.adoc
	,inserted.pdata
	,inserted.fref
	,inserted.cambiofixo
	,inserted.ccusto
	,inserted.ncusto
	,inserted.intid
	,inserted.tipo
	,inserted.estab
	,inserted.pais
	,inserted.zona
	,inserted.crend
	,inserted.final
from inserted 
inner join cm1 on cm1.cm=inserted.doccode 
inner join fo2 on fo2.fo2stamp=inserted.fostamp 
where 
	cm1.folanfc=1 
	and cm1.fcautoreg=1 
	and inserted.multi=0 
	
if (update(aprovado) or update(multi))
update fc 
set 
	aprovado=i.aprovado
	,nmaprov=i.nmaprov
	,dtaprov=i.dtaprov
from inserted i
where 
	fc.fostamp=i.fostamp

IF (select COUNT(*) from inserted inner join fc on fc.fostamp = inserted.fostamp inner join fo2 on fo2.fo2stamp=inserted.fostamp where fo2.anulado=1)>0	
UPDATE FC 
set 
	deb = 0, edeb = 0, debf = 0, edebf = 0,debfm = 0,
	cred = 0, ecred = 0, credf = 0, ecredf = 0, credfm = 0,
	ivav1 = 0, ivav2 = 0, ivav3 = 0, ivav4 = 0, ivav5 = 0, ivav6 = 0, ivav7 = 0, ivav8 = 0, ivav9 = 0,
	eivav1 = 0, eivav2 = 0, eivav3 = 0, eivav4 = 0, eivav5 = 0, eivav6 = 0, eivav7 = 0, eivav8 = 0, eivav9 = 0,
	IVATX1 = 0, IVATX2 = 0, IVATX3 = 0, IVATX4 = 0, IVATX5 = 0, IVATX6 = 0, IVATX7 = 0, IVATX8 = 0, IVATX9 = 0,
	reexgiva = 0, virs = 0, evirs = 0, 
	virsreg = 0, evirsreg = 0,
	difcambio = 0, edifcambio = 0
from inserted 
inner join fo2 on fo2.fo2stamp=inserted.fostamp
inner join fc on fc.fostamp = inserted.fostamp 
where 
	fo2.anulado = 1 

GO

