USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_fi_insert]    Script Date: 06/12/2014 10:06:25 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE TRIGGER [dbo].[tr_fi_insert] 
ON [dbo].[fi]  
FOR INSERT  
AS 

SET NOCOUNT ON

if (select count(*) from inserted inner join ft on ft.ftstamp=inserted.ftstamp inner join td on inserted.ndoc=td.ndoc where td.lancasl=1 and inserted.ref<>space(18))>0 
INSERT INTO SL 
	(
	slstamp,fistamp,stns,usrinis,usrhora,usrdata,ousrinis,ousrhora,ousrdata,origem,
	fref,ccusto,ncusto,nome,segmento,codigo,moeda,datalc,design,
	ref,lote,armazem,qtt,
	tt,
	vu,
	ett,
	evu,
	num1,
	frcl,adoc,cm,cmdesc,composto,cor,tam,usr1,usr2,usr3,usr4,usr5,
	vumoeda,
	ttmoeda,
	pcpond,cpoc,
	valiva,evaliva,epcpond 
	)
select 
	inserted.fistamp,inserted.fistamp,inserted.stns,inserted.usrinis,inserted.usrhora,inserted.usrdata,inserted.ousrinis,inserted.ousrhora,inserted.ousrdata,'FT'
	,case when td.lifref=1 then inserted.fifref else ft.fref end,case when td.liccusto=1 then inserted.ficcusto else ft.ccusto end
	,case when td.lincusto=1 then inserted.fincusto else ft.ncusto end,ft.nome,ft.segmento,inserted.codigo,ft.moeda,ft.fdata,inserted.design,
	inserted.ref,inserted.lote,inserted.armazem,case when ft.tipodoc=3 then inserted.qtt*-1 else inserted.qtt end,
	inserted.sltt,
	inserted.slvu,
	inserted.esltt,
	inserted.eslvu,
	inserted.num1,
	ft.no,convert(char (10), ft.fno),td.cmsl,td.cmsln,inserted.composto,inserted.cor,inserted.tam,inserted.usr1,inserted.usr2,inserted.usr3,inserted.usr4,inserted.usr5,
	inserted.slvumoeda,
	inserted.slttmoeda,
	inserted.pcp,inserted.cpoc,
	case when inserted.iectin<>0 then inserted.pv-inserted.vbase else case when inserted.ivaincl=1 then inserted.tiliquido-inserted.tiliquido/(1+inserted.iva/100) else inserted.tiliquido*inserted.iva/100 end end,
	case when inserted.eiectin<>0 then inserted.epv-inserted.evbase else case when inserted.ivaincl=1 then inserted.etiliquido-inserted.etiliquido/(1+inserted.iva/100) else inserted.etiliquido*inserted.iva/100 end end,
	inserted.epcp 
from inserted 
inner join ft on ft.ftstamp=inserted.ftstamp 
inner join td on inserted.ndoc=td.ndoc 
where 
	td.lancasl=1 
	and inserted.ref<>space(18) 
	
if (select count(*) from inserted inner join ft on ft.ftstamp=inserted.ftstamp inner join td on inserted.ndoc=td.ndoc where td.lancasl=1 and td.cmsl>50 and td.cmsv<>0 and td.tipodoc=3 and inserted.qtt<>0 and inserted.ref<>space(18) and (inserted.epcp<>inserted.ecusto or inserted.pcp<>inserted.custo))>0 
INSERT INTO SV 
	(
	svstamp,fistamp,usrinis,usrhora,usrdata,ousrinis,ousrhora,ousrdata,origem,
	nome,moeda,data,design,
	ref,lote,cor,tam,armazem,
	tt,
	ett,
	no,adoc,cm,cmdesc
	)
select 
	inserted.fistamp,inserted.fistamp,inserted.usrinis,inserted.usrhora,inserted.usrdata,inserted.ousrinis,inserted.ousrhora,inserted.ousrdata,'FT',
	ft.nome,'EURO',ft.fdata,inserted.design,
	inserted.ref,inserted.lote,inserted.cor,inserted.tam,inserted.armazem,
	inserted.qtt*(inserted.custo-inserted.pcp),
	inserted.qtt*(inserted.ecusto-inserted.epcp),
	ft.no,convert(char (10), ft.fno),td.cmsv,td.cmsvn 
from inserted 
inner join ft on ft.ftstamp=inserted.ftstamp 
inner join td on ft.ndoc=td.ndoc 
where 
	td.lancasl=1 
	and td.cmsl>50 
	and td.cmsv<>0 
	and td.tipodoc=3 
	and inserted.qtt<>0 
	and inserted.ref<>space(18) 
	and (inserted.epcp<>inserted.ecusto or inserted.pcp<>inserted.custo)

IF (select count(*) from inserted where inserted.tipodoc=3 and inserted.ftregstamp<>space(25)  )>0
INSERT INTO FTREG 
	(
	ftregstamp
	)
select 
	inserted.ftregstamp 
from inserted 
where 
	inserted.ftregstamp<>space(25) 
	and not EXISTS(select ftregstamp from ftreg where ftreg.ftregstamp=inserted.ftregstamp) 
	
IF (select count(*) from inserted where  inserted.tipodoc=3 and inserted.ftregstamp<>space(25) ) >0 
UPDATE FTREG 
SET 
	ftreg.vciva=ftreg.vciva+(select ROUND(sum(case when inserted.ivaincl=1 then (inserted.tiliquido)*-1 else -inserted.tiliquido*(1+inserted.iva/100) end),0) from inserted where inserted.ftregstamp=ftreg.ftregstamp)
	,ftreg.vmoeda=ftreg.vmoeda+(select ROUND(sum(case when inserted.ivaincl=1 then (inserted.tmoeda)*-1 else -inserted.tmoeda*(1+inserted.iva/100) end),2) from inserted where inserted.ftregstamp=ftreg.ftregstamp)
	,ftreg.evciva=ftreg.evciva+(select ROUND(sum(case when inserted.ivaincl=1 then (inserted.etiliquido)*-1 else -inserted.etiliquido*(1+inserted.iva/100) end),2) from inserted where inserted.ftregstamp=ftreg.ftregstamp)
	,ftreg.ultdoc=(select ltrim(rtrim(ft.nmdoc))+' nº '+convert(char(10),ft.fno)+' de '+convert(char(10),ft.fdata,104) from ft where inserted.ftstamp=ft.ftstamp) 
from ftreg 
inner join inserted on ftreg.ftregstamp=inserted.ftregstamp 
where 
	inserted.tipodoc=3 
	and inserted.ftregstamp<>space(25)

IF (select count(*) from inserted where inserted.bistamp<>'' )>0 
BEGIN 
	UPDATE BI 
	SET 
		bi.fdata = ft.fdata,
		bi.fno = ft.fno,
		bi.adoc = convert(char(10),ft.fno),
		bi.nmdoc = ft.nmdoc,
		bi.oftstamp = ft.ftstamp,
		bi.ndoc = ft.ndoc, 
		bi.qtt2 = 
			bi.qtt2
			+
			(select SUM(inserted.qtt*(case when inserted.tipodoc=3 then -1 else 1 end)) from inserted where inserted.bistamp <> '' and inserted.bistamp = bi.bistamp) 
	from bi 
	inner join inserted on inserted.bistamp=bi.bistamp 
	inner join ft (nolock) on inserted.ftstamp=ft.ftstamp 
	where 
		inserted.bistamp<>'' 

	UPDATE BI SET 
		bi.partes2 = bi.partes2+(select SUM(inserted.partes*(case when inserted.tipodoc=3 then -1 else 1 end)) from inserted where inserted.bistamp <> '' and inserted.bistamp = bi.bistamp) 
	from bi inner join inserted on inserted.bistamp = bi.bistamp 
	inner join ts (nolock) on ts.ndos = bi.ndos 
	where 
		inserted.bistamp<>'' 
		and (ts.usam1 = 1 or ts.usam2 = 1 or ts.usam3 = 1)
END 

if (select count(*) from inserted where ndocft<>0 and fnoft<>0 ) >0 
UPDATE FT 
SET 
	ft.facturada = 1 ,
	ft.nmdocft = td.nmdoc, 
	ft.fnoft = inserted.fno 
from inserted 
inner join td on td.ndoc=inserted.ndoc 
where 
	inserted.ndocft<>0 
	and inserted.ndocft = ft.ndoc 
	and inserted.fnoft = ft.fno  
	and inserted.ftanoft = ft.ftano 
	
IF (select count(*) from inserted inner join ft on inserted.ftstamp=ft.ftstamp where ft.anulado = 1) > 0 
UPDATE ft 
SET 
	ft.anulado = (SELECT ft.anulado FROM ft INNER JOIN inserted ON ft.ftstamp = inserted.ftstamp) 
FROM ft 
inner join inserted on inserted.ftstamp=ft.ftstamp

GO

