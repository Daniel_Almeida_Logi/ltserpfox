
/****** Object:  Trigger [dbo].[tr_bi_delete]    Script Date: 16/06/2020 11:04:15 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[tr_bi_delete] ON [dbo].[bi]
FOR DELETE
AS


-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_BI_Delete'
	,@MROW int = @@ROWCOUNT

IF (@@ROWCOUNT = 0)
	RETURN;

SET NOCOUNT ON

SET @Msg += '<' + convert(varchar,getdate(),14) + '>'

declare @site_nr tinyint
set @site_nr = isnull((select top 1 empresa_no from empresa_arm (nolock) inner join deleted d on empresa_arm.armazem = d.armazem where d.armazem != 0),1)
	
	
-- Eliminar movimento de stock
IF EXISTS (SELECT TOP 1 sl.slstamp FROM sl INNER JOIN deleted d ON d.ref <> '' AND sl.bistamp = d.bistamp AND d.bistamp <> '')
begin
	DELETE
		SL
	FROM
		deleted
	WHERE
		deleted.ref != '' 
		AND deleted.bistamp != ''
		and sl.bistamp = deleted.bistamp

	if @@ROWCOUNT > 0
		SET @Msg += '<Delete SL (' + convert(varchar,getdate(),8) + ')>'
end


/*
	eliminar quantidade regularizada de documento de origem
*/
IF exists (SELECT d.bistamp FROM deleted d WHERE d.obistamp != '' AND d.qtt <> 0)
BEGIN
		
	set Context_Info 0x55555 -- evitar que o trigger de update corra validações desnecessárias
	
	UPDATE
		BI
	SET
		bi.fno = 0
		,bi.adoc = ''
		,bi.nmdoc = ''
		,bi.ndoc = 0
		,bi.qtt2 = case when bi.qtt2 - (SELECT SUM(d.qtt) FROM deleted d WHERE d.obistamp = deleted.obistamp) < 0 then 0 else bi.qtt2 - (SELECT SUM(d.qtt) FROM deleted d WHERE d.obistamp = deleted.obistamp) end
		,bi.pbruto = bi.pbruto - (SELECT SUM(d.qtt) FROM deleted d WHERE d.obistamp = deleted.obistamp) -- campo utilizado como qtt2, mas para ser compativel com as compras que não têm qtt2
	FROM
		deleted
		--INNER JOIN ts (nolock) ON deleted.ndos = ts.ndos
	WHERE
		deleted.obistamp != ''
		and deleted.obistamp = bi.bistamp
		--and ts.stocks = 1

	if @@ROWCOUNT > 0
		SET @Msg += '<Update BI (origem) (' + convert(varchar,getdate(),8) + ')>'

	set Context_Info 0x0
END


IF @MROW = 1
BEGIN
		
	SET @Msg += '<SL (' + convert(varchar,getdate(),8) + ')>'

	/*
		Atualizar reservas de cliente
	*/
	IF EXISTS (SELECT d.marcada FROM deleted d WHERE d.ref != '' AND d.rescli = 1 AND d.fechada = 0)
	BEGIN
		
		set Context_Info 0x55556 -- evitar que o trigger de update corra validações desnecessárias

		UPDATE
			ST
		SET 
			st.qttcli = st.qttcli - (CASE 
										WHEN (deleted.qtt - deleted.qtt2) < 0 THEN 0
										ELSE (deleted.qtt - deleted.qtt2)
										END)
		FROM
			deleted
		WHERE
			deleted.rescli = 1 
			AND deleted.fechada = 0
			and st.ref = deleted.ref
			and st.site_nr = @site_nr

		if @@ROWCOUNT > 0
			SET @Msg += '<Update ST (qttcli) (' + convert(varchar,getdate(),8) + ')>'

		set Context_Info 0x0

		UPDATE
			SA
		SET
			sa.rescli = sa.rescli - (CASE 
										WHEN (deleted.qtt - deleted.qtt2) < 0 THEN 0
										ELSE (deleted.qtt - deleted.qtt2)
										END)
		FROM
			deleted
		WHERE
			deleted.rescli = 1 AND deleted.fechada = 0
			and sa.ref = deleted.ref and sa.armazem = deleted.armazem

		if @@ROWCOUNT > 0
			SET @Msg += '<Update SA (rescli) (' + convert(varchar,getdate(),8) + ')>'
	END


	/*
		Atualizar encomendado fornecedor
	*/
	IF EXISTS (SELECT deleted.marcada FROM deleted WHERE deleted.ref <> '' AND deleted.resfor = 1 AND deleted.fechada = 0)
	BEGIN

		set Context_Info 0x55556 -- evitar que o trigger de update corra validações desnecessárias

		UPDATE
			ST
		SET
			st.qttfor = st.qttfor - (CASE 
										WHEN (deleted.qtt - deleted.qtt2) < 0 THEN 0
										ELSE (deleted.qtt - deleted.qtt2)
										END)
		FROM
			deleted
		WHERE
			deleted.resfor = 1 
			AND deleted.fechada = 0
			and st.ref = deleted.ref
			and st.site_nr = @site_nr

		if @@ROWCOUNT > 0
			SET @Msg += '<Update ST (qttfor) (' + convert(varchar,getdate(),8) + ')>'

		set Context_Info 0x0

		UPDATE
			SA
		SET
			sa.resfor = sa.resfor - (CASE
										WHEN (deleted.qtt - deleted.qtt2) < 0 THEN 0
										ELSE (deleted.qtt - deleted.qtt2)
										END)
		FROM
			deleted
		WHERE
			deleted.resfor = 1 
			AND deleted.fechada = 0
			and sa.ref = deleted.ref 
			and sa.armazem = deleted.armazem

		if @@ROWCOUNT > 0
			SET @Msg += '<Update SA (resfor) (' + convert(varchar,getdate(),8) + ')>'
	END

	/*
		Atualiza cativação de stocks
	*/
	IF exists (SELECT i.marcada FROM deleted i INNER JOIN ts (nolock) ON i.ndos = ts.ndos WHERE i.ref != '' and i.qtt != 0 and ts.cativast=1 AND i.fechada = 0)
	begin
		set Context_Info 0x55556 -- evitar que o trigger de update corra validações desnecessárias
			print 'UPD CAT'
			update st set cativado=	isnull((select sum(bi.qtt-bi.qtt2) from bi (nolock)
							INNER JOIN ts (nolock) ON bi.ndos = ts.ndos
							inner join bo (nolock) on bo.bostamp=bi.bostamp
							where ts.cativast=1 and bi.fechada=0 and bo.fechada=0 and bi.armazem=st.site_nr and bi.ref=st.ref),0)
			from st (nolock) where ref in (select ref from deleted) and site_nr=@site_nr
				
			if @@ROWCOUNT > 0
				SET @Msg += '<I Update ST (qttcli) (' + convert(varchar,getdate(),8) + ')>'

			set Context_Info 0x0

			update sa set cativado=	isnull((select sum(bi.qtt-bi.qtt2) from bi (nolock)
							INNER JOIN ts (nolock) ON bi.ndos = ts.ndos
							inner join bo (nolock) on bo.bostamp=bi.bostamp
							where ts.cativast=1 and bi.fechada=0 and bo.fechada=0 and bi.armazem=sa.armazem and bi.ref=sa.ref),0)
			from sa (nolock) where ref in (select ref from inserted) and armazem=@site_nr

			if @@ROWCOUNT > 0
				SET @Msg += '<I Update SA (qttcli) (' + convert(varchar,getdate(),8) + ')>'
	END

END
ELSE 
BEGIN

	SET @Msg += '<ML (' + convert(varchar,getdate(),8) + ')>'

	-- Atualizar reservas de cliente
	IF EXISTS (SELECT d.marcada FROM deleted d WHERE d.ref <> '' AND d.rescli = 1 AND d.fechada = 0)
	BEGIN
		UPDATE
			SA
		SET
			sa.rescli = sa.rescli - isnull((SELECT SUM(CASE 
														WHEN qtt - qtt2 < 0 THEN 0
														ELSE qtt - qtt2
														END)
												FROM deleted
												WHERE deleted.fechada = 0 AND deleted.rescli = 1 and sa.ref=deleted.ref and deleted.armazem = sa.armazem)
										,0)
		WHERE
			sa.ref IN (SELECT deleted.ref FROM deleted
						WHERE deleted.fechada = 0 and deleted.rescli = 1 and sa.armazem = deleted.armazem)


		if @@ROWCOUNT > 0
			SET @Msg += '<Update SA (rescli) (' + convert(varchar,getdate(),8) + ')>'

		set Context_Info 0x55556 -- evitar que o trigger de update corra validações desnecessárias

		UPDATE
			st
		SET
			st.qttcli = st.qttcli - isnull((SELECT SUM(CASE 
															WHEN qtt - qtt2 < 0 THEN 0
															ELSE qtt - qtt2
															END)
												FROM deleted
												WHERE deleted.fechada = 0 AND deleted.rescli = 1 and st.ref=deleted.ref)
										,0)
		WHERE
			st.ref IN (SELECT deleted.ref FROM deleted WHERE deleted.fechada = 0)
			and st.site_nr = @site_nr

		if @@ROWCOUNT > 0
			SET @Msg += '<Update ST (qttcli) (' + convert(varchar,getdate(),8) + ')>'

		set Context_Info 0x0
	END

	/*
		Atualizar encomendado fornecedor
	*/
	IF EXISTS (SELECT d.marcada FROM deleted d WHERE d.ref <> '' AND d.resfor = 1 AND d.fechada = 0)
	BEGIN
		UPDATE
			SA
		SET
			sa.resfor = sa.resfor - isnull((SELECT SUM(CASE 
															WHEN qtt - qtt2 < 0 THEN 0
															ELSE qtt - qtt2
															END)
												FROM deleted
												WHERE deleted.fechada = 0 AND deleted.resfor = 1 and sa.ref=deleted.ref and deleted.armazem = sa.armazem)
										,0)
		WHERE
			sa.ref IN (SELECT deleted.ref FROM deleted WHERE deleted.fechada = 0 AND deleted.resfor = 1)

		if @@ROWCOUNT > 0
			SET @Msg += '<Update SA (resfor) (' + convert(varchar,getdate(),8) + ')>'

		set Context_Info 0x55556 -- evitar que o trigger de update corra validações desnecessárias

		UPDATE
			st
		SET
			st.qttfor = st.qttfor - isnull((SELECT SUM(CASE 
															WHEN qtt - qtt2 < 0 THEN 0
															ELSE qtt - qtt2
															END)
												FROM deleted
												WHERE deleted.fechada = 0 AND deleted.resfor = 1 and st.ref=deleted.ref)
										,0)
		WHERE
			st.ref IN (SELECT deleted.ref FROM deleted WHERE deleted.fechada = 0 AND deleted.resfor = 1)
			and st.site_nr = @site_nr
				
		if @@ROWCOUNT > 0
			SET @Msg += '<Update ST (qttfor) (' + convert(varchar,getdate(),8) + ')>'

		set Context_Info 0x0
	END

	IF exists (SELECT i.marcada FROM deleted i INNER JOIN ts (nolock) ON i.ndos = ts.ndos WHERE i.ref != '' and i.qtt != 0 and ts.cativast=1 AND i.fechada = 0)
	begin
		set Context_Info 0x55556 -- evitar que o trigger de update corra validações desnecessárias
			print 'UPD CAT'
			update st set cativado=	isnull((select sum(bi.qtt-bi.qtt2) from bi (nolock)
							INNER JOIN ts (nolock) ON bi.ndos = ts.ndos
							inner join bo (nolock) on bo.bostamp=bi.bostamp
							where ts.cativast=1 and bi.fechada=0 and bo.fechada=0 and bi.armazem=st.site_nr and bi.ref=st.ref),0)
			from st (nolock) where ref in (select ref from deleted) and site_nr=@site_nr
				
			if @@ROWCOUNT > 0
				SET @Msg += '<I Update ST (qttcli) (' + convert(varchar,getdate(),8) + ')>'

			set Context_Info 0x0

			update sa set cativado=	isnull((select sum(bi.qtt-bi.qtt2) from bi (nolock)
							INNER JOIN ts (nolock) ON bi.ndos = ts.ndos
							inner join bo (nolock) on bo.bostamp=bi.bostamp
							where ts.cativast=1 and bi.fechada=0 and bo.fechada=0 and bi.armazem=sa.armazem and bi.ref=sa.ref),0)
			from sa (nolock) where ref in (select ref from inserted) and armazem=@site_nr

			if @@ROWCOUNT > 0
				SET @Msg += '<I Update SA (qttcli) (' + convert(varchar,getdate(),8) + ')>'
	END
END

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger3') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger3]([nome],[Mensagem]) VALUES(@nome,@Msg) ;


/*
	create table ##Debug_Trigger3 (nome varchar(200), mensagem varchar(500))
	select * from ##Debug_Trigger3
	delete ##Debug_Trigger3


select * from bo where ndos=6
begin transaction asd
delete bi where bostamp='ADM3B28512A-707A-4BE2-91E'
rollback tran asd
delete bi where bistamp in ('ADMBC55AC8B-8F14-4BF6-848','ADM12B668F1-28C0-4431-A46','ADM72E77A50-B3B8-4ABA-900')
select * from bi where bostamp='ADM3B28512A-707A-4BE2-91E'
*/