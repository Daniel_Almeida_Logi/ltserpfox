USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_bl_update]    Script Date: 06/12/2014 10:13:14 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE TRIGGER [dbo].[tr_bl_update] ON [dbo].[bl]
FOR UPDATE
AS
SET NOCOUNT ON

IF UPDATE (conta) OR UPDATE (banco)
BEGIN
	UPDATE re
	SET ollocal = convert(CHAR(10), inserted.banco) + ' ' + convert(CHAR(20), inserted.conta)
	FROM inserted
	WHERE re.contado = inserted.noconta

	UPDATE po
	SET ollocal = convert(CHAR(10), inserted.banco) + ' ' + convert(CHAR(20), inserted.conta)
	FROM inserted
	WHERE po.contado = inserted.noconta

	UPDATE pc
	SET ollocal = convert(CHAR(10), inserted.banco) + ' ' + convert(CHAR(20), inserted.conta)
	FROM inserted
	WHERE pc.contado = inserted.noconta
END
GO

