
/****** Object:  Trigger [dbo].[tr_bi_update]    Script Date: 16/06/2020 10:46:03 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[tr_bi_update] ON [dbo].[bi]
FOR UPDATE
AS

-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(1000) = ''
	,@nome varchar(20) = 'Tr_Bi_Update'
	,@MROW int = @@ROWCOUNT
	
IF (@@ROWCOUNT = 0)
	RETURN;

SET NOCOUNT ON

SET @Msg += '<' + convert(varchar,getdate(),14) + '>'

IF EXISTS (SELECT inserted.bistamp FROM inserted where inserted.ref != '')
BEGIN

	declare @site_nr tinyint
	set @site_nr = (select top 1 empresa_no from empresa_arm (nolock) inner join inserted on empresa_arm.armazem = inserted.armazem where inserted.armazem != 0)
	IF @site_nr is null
	begin
		set @site_nr = isnull((select top 1 empresa_no from empresa_arm (nolock) inner join deleted on empresa_arm.armazem = deleted.armazem where deleted.armazem != 0),1)
	end 


	if (isnull(Context_Info(),0x0) != 0x55555)
	begin
	
		/*
			Cria registo na sa se não existir
		*/
		INSERT INTO SA 
			(sastamp, armazem, ref)
		SELECT distinct 
			convert(CHAR(5), inserted.armazem) + inserted.ref
			,inserted.armazem, inserted.ref
		FROM
			inserted
		WHERE
			NOT EXISTS (SELECT sa.armazem FROM sa WHERE sa.ref = inserted.ref AND sa.armazem = inserted.armazem)
			AND inserted.ref != ''

		if @@ROWCOUNT > 0
			SET @Msg += '<Insert SA (' + convert(varchar,getdate(),8) + ')>'


		/*
			Eliminar da Sl se necessário
		*/
		--IF EXISTS (SELECT sl.slstamp FROM sl (nolock) INNER JOIN deleted d ON d.ref != '' AND sl.bistamp = d.bistamp AND d.bistamp != '')
		--BEGIN
		--	DELETE
		--		SL
		--	FROM
		--		deleted
		--	WHERE
		--		deleted.ref != '' 
		--		AND deleted.bistamp != '' 
		--		and sl.bistamp = deleted.bistamp

		--	if @@ROWCOUNT > 0
		--		SET @Msg += '<Delete SL (' + convert(varchar,getdate(),8) + ')>'
		--END

		--IF EXISTS (SELECT i.bistamp FROM inserted i INNER JOIN ts (nolock) ON i.ndos = ts.ndos WHERE ts.stocks = 1 AND ts.trfa = 1 AND i.ref != '') 
		--BEGIN
			--DELETE
			--	SL
			--FROM
			--	deleted
			--WHERE
			--	deleted.ref != '' 
			--	AND deleted.bistamp != '' 
			--	and 'T'+sl.bistamp = deleted.bistamp

			--if @@ROWCOUNT > 0
			--	SET @Msg += '<Delete SL (trf.arm.) (' + convert(varchar,getdate(),8) + ')>'
		--END


		/*
			Delete and Insert movimentos stock
		*/
		IF EXISTS (SELECT i.marcada FROM inserted i INNER JOIN ts (nolock) ON i.ndos = ts.ndos WHERE ts.stocks = 1 AND i.ref != '') 
		BEGIN
			DELETE
				SL
			FROM
				deleted
			WHERE
				deleted.ref != '' 
				AND deleted.bistamp != '' 
				and sl.bistamp = deleted.bistamp

			if @@ROWCOUNT > 0
				SET @Msg += '<Delete SL (' + convert(varchar,getdate(),8) + ')>'


			INSERT INTO SL (
				slstamp
				,bistamp
				,usrinis
				,usrhora
				,usrdata
				,ousrinis
				,ousrhora
				,ousrdata
				,origem
				,fref
				,ccusto
				,ncusto
				,nome
				,codigo
				,moeda
				,datalc
				,design
				,ref
				,armazem
				,qtt
				,lote
				,tt
				,vu
				,ett
				,evu
				,num1
				,frcl
				,adoc
				,cm
				,cmdesc
				,composto
				,cor
				,tam
				,usr1
				,usr2
				,usr3
				,usr4
				,usr5
				,vumoeda
				,ttmoeda
				,pcpond
				,cpoc
				,valiva
				,trfa
				,evaliva
				,epcpond
				,stns
				)
			SELECT
				inserted.bistamp
				,inserted.bistamp
				,inserted.usrinis
				,inserted.usrhora
				,inserted.usrdata
				,inserted.ousrinis
				,inserted.ousrhora
				,inserted.ousrdata
				,'BO'
				,CASE 
					WHEN ts.lifref = 1 THEN inserted.bifref 
					ELSE bo.fref
					END
				,CASE 
					WHEN ts.tccusto = 1 THEN inserted.ccusto
					ELSE bo.ccusto
					END
				,CASE 
					WHEN ts.tncusto = 1	THEN inserted.ncusto
					ELSE bo.ncusto
					END
				,bo.nome
				,inserted.codigo
				,bo.moeda
				,inserted.rdata
				,inserted.design
				,inserted.ref
				,inserted.armazem
				,inserted.qtt
				,inserted.lote
				,inserted.sltt
				,inserted.slvu
				,inserted.esltt
				,inserted.eslvu
				,inserted.num1
				,bo.no
				,convert(CHAR(10), bo.obrano)
				,CASE 
					WHEN (ts.producao = 1 AND inserted.producao = 1) OR (ts2.trocaequi = 1 AND inserted.trocaequi = 1) THEN ts.cm2stocks
					ELSE ts.cmstocks
					END
				,CASE 
					WHEN (ts.producao = 1 AND inserted.producao = 1) OR (ts2.trocaequi = 1 AND inserted.trocaequi = 1) THEN ts.cm2desc
					ELSE ts.cmdesc
					END
				,inserted.composto
				,inserted.cor
				,inserted.tam
				,inserted.usr1
				,inserted.usr2
				,inserted.usr3
				,inserted.usr4
				,inserted.usr5
				,inserted.slvumoeda
				,inserted.slttmoeda
				,inserted.pcusto
				,inserted.cpoc
				,CASE 
					WHEN inserted.ivaincl = 1 THEN inserted.ttdeb - inserted.ttdeb / (1 + inserted.iva / 100)
					ELSE inserted.ttdeb * inserted.iva / 100
					END
				,ts.trfa
				,CASE 
					WHEN inserted.ivaincl = 1 THEN inserted.ettdeb - inserted.ettdeb / (1 + inserted.iva / 100)
					ELSE inserted.ettdeb * inserted.iva / 100
					END
				,inserted.epcusto
				,inserted.stns
			FROM
				inserted
				--INNER JOIN deleted ON inserted.bistamp = deleted.bistamp
				INNER JOIN bo ON bo.bostamp = inserted.bostamp
				INNER JOIN ts (nolock) ON inserted.ndos = ts.ndos
				INNER JOIN ts2 (nolock) ON ts2.ts2stamp = ts.tsstamp
			WHERE
				ts.stocks = 1
				AND inserted.ref != ''
				and inserted.qtt != 0
				--AND NOT EXISTS (SELECT slstamp FROM sl WHERE sl.slstamp = inserted.bistamp)
			
			if @@ROWCOUNT > 0
				SET @Msg += '<Insert SL (' + convert(varchar,getdate(),8) + ')>'
		END

		/*
			insert movimentos stock entre armazens (mov adicional)
		*/
		IF EXISTS (SELECT i.bistamp FROM inserted i INNER JOIN ts (nolock) ON i.ndos = ts.ndos WHERE ts.stocks = 1 AND ts.trfa = 1	AND i.ref != '') 
		BEGIN
			DELETE
				SL
			FROM
				deleted
			WHERE
				deleted.ref != '' 
				AND deleted.bistamp != '' 
				and sl.bistamp = substring('T' + deleted.bistamp, 1, 25)

			if @@ROWCOUNT > 0
				SET @Msg += '<Delete SL (trf.arm.) (' + convert(varchar,getdate(),8) + ')>'


			INSERT INTO SL (
				slstamp
				,bistamp
				,usrinis
				,usrhora
				,usrdata
				,ousrinis
				,ousrhora
				,ousrdata
				,origem
				,fref
				,ccusto
				,ncusto
				,nome
				,codigo
				,moeda
				,datalc
				,design
				,ref
				,armazem
				,qtt
				,lote
				,tt
				,vu
				,ett
				,evu
				,num1
				,frcl
				,adoc
				,cm
				,cmdesc
				,composto
				,cor
				,tam
				,usr1
				,usr2
				,usr3
				,usr4
				,usr5
				,vumoeda
				,ttmoeda
				,pcpond
				,cpoc
				,valiva
				,trfa
				,evaliva
				,epcpond
				,stns
				)
			SELECT
				substring('T' + inserted.bistamp, 1, 25)
				,inserted.bistamp
				,inserted.usrinis
				,inserted.usrhora
				,inserted.usrdata
				,inserted.ousrinis
				,inserted.ousrhora
				,inserted.ousrdata
				,'BO'
				,CASE
					WHEN ts.lifref = 1 THEN inserted.bifref
					ELSE bo.fref
					END
				,CASE 
					WHEN ts.tccusto = 1 THEN inserted.ccusto
					ELSE bo.ccusto
					END
				,CASE 
					WHEN ts.tncusto = 1	THEN inserted.ncusto
					ELSE bo.ncusto
					END
				,bo.nome
				,inserted.codigo
				,bo.moeda
				,inserted.rdata
				,inserted.design
				,inserted.ref
				,inserted.ar2mazem
				,inserted.qtt
				,inserted.lote
				,inserted.sltt
				,inserted.slvu
				,inserted.esltt
				,inserted.eslvu
				,inserted.num1
				,bo.no
				,convert(CHAR(10), bo.obrano)
				,ts.cm2stocks
				,ts.cm2desc
				,inserted.composto
				,inserted.cor
				,inserted.tam
				,inserted.usr1
				,inserted.usr2
				,inserted.usr3
				,inserted.usr4
				,inserted.usr5
				,inserted.slvumoeda
				,inserted.slttmoeda
				,inserted.pcusto
				,inserted.cpoc
				,CASE 
					WHEN inserted.ivaincl = 1 THEN inserted.ttdeb - inserted.ttdeb / (1 + inserted.iva / 100)
					ELSE inserted.ttdeb * inserted.iva / 100
					END
				,ts.trfa
				,CASE 
					WHEN inserted.ivaincl = 1 THEN inserted.ettdeb - inserted.ettdeb / (1 + inserted.iva / 100)
					ELSE inserted.ettdeb * inserted.iva / 100
					END
				,inserted.epcusto
				,inserted.stns
			FROM
				inserted
				--INNER JOIN deleted ON inserted.bistamp = deleted.bistamp
				INNER JOIN bo ON bo.bostamp = inserted.bostamp
				INNER JOIN ts (nolock) ON inserted.ndos = ts.ndos
			WHERE
				ts.stocks = 1 
				AND ts.trfa = 1
				AND inserted.ref != ''
				and inserted.qtt != 0
				--AND NOT EXISTS (SELECT 'T'+slstamp FROM sl WHERE sl.slstamp = inserted.bistamp)

			if @@ROWCOUNT > 0
				SET @Msg += '<Insert SL (trf.arm.) (' + convert(varchar,getdate(),8) + ')>'
		END


		/*
			Update sl
		*/
		--IF EXISTS (SELECT inserted.marcada FROM inserted INNER JOIN ts (nolock) ON ts.ndos = inserted.ndos WHERE ts.stocks = 1 AND inserted.ref != '')
		--	AND (UPDATE (usrdata) OR UPDATE (usrhora) OR UPDATE (usrinis) OR UPDATE (rdata) OR UPDATE (ref) OR UPDATE (design) OR UPDATE (qtt)
		--			OR UPDATE (lote) OR UPDATE (armazem) OR	UPDATE (num1) OR UPDATE (sltt) OR UPDATE (esltt) OR UPDATE (eslvu)
		--			OR UPDATE (slvumoeda) OR UPDATE (iva) OR UPDATE (ivaincl) OR UPDATE (codigo) OR UPDATE (usr1) OR UPDATE (usr2) OR UPDATE (usr3)
		--			OR UPDATE (usr4) OR UPDATE (usr5) OR UPDATE (slttmoeda) OR UPDATE (ettdeb) OR UPDATE (epcusto) OR UPDATE (cor) OR UPDATE (tam)
		--			OR UPDATE (composto) OR UPDATE (iva) OR	UPDATE (ivaincl) OR UPDATE (stns) OR UPDATE (ccusto) OR UPDATE (ncusto) OR UPDATE (obrano)
		--			OR UPDATE (no) OR UPDATE (nome) OR UPDATE (bifref))
		--begin
		--	UPDATE
		--		SL
		--	SET
		--		sl.usrdata = inserted.usrdata
		--		,sl.usrhora = inserted.usrhora
		--		,sl.usrinis = inserted.usrinis
		--		,sl.fref = CASE
		--					WHEN ts.lifref = 1 THEN inserted.bifref
		--					ELSE bo.fref
		--					END
		--		,sl.ccusto = CASE
		--						WHEN ts.tccusto = 1	THEN inserted.ccusto
		--						ELSE bo.ccusto
		--						END
		--		,sl.ncusto = CASE
		--						WHEN ts.tncusto = 1 THEN inserted.ncusto
		--						ELSE bo.ncusto
		--						END
		--		,sl.nome = bo.nome
		--		,sl.codigo = inserted.codigo
		--		,sl.moeda = bo.moeda
		--		,sl.datalc = inserted.rdata
		--		,sl.design = inserted.design
		--		,sl.ref = inserted.ref
		--		,sl.lote = inserted.lote
		--		,sl.armazem = inserted.armazem
		--		,sl.qtt = inserted.qtt
		--		,sl.num1 = inserted.num1
		--		,sl.tt = inserted.sltt
		--		,sl.vu = inserted.slvu
		--		,sl.ett = inserted.esltt
		--		,sl.evu = inserted.eslvu
		--		,sl.frcl = bo.no
		--		,sl.adoc = convert(CHAR(10), bo.obrano)
		--		,sl.cm = CASE
		--					WHEN (ts.producao = 1 AND inserted.producao = 1) OR (ts2.trocaequi = 1 AND inserted.trocaequi = 1)
		--					THEN ts.cm2stocks
		--					ELSE ts.cmstocks
		--					END
		--		,sl.cmdesc = CASE
		--						WHEN (ts.producao = 1 AND inserted.producao = 1) OR (ts2.trocaequi = 1 AND inserted.trocaequi = 1)
		--						THEN ts.cm2desc
		--						ELSE ts.cmdesc
		--						END
		--		,sl.composto = inserted.composto
		--		,sl.cor = inserted.cor
		--		,sl.tam = inserted.tam
		--		,sl.vumoeda = inserted.slvumoeda
		--		,sl.usr1 = inserted.usr1
		--		,sl.usr2 = inserted.usr2
		--		,sl.usr3 = inserted.usr3
		--		,sl.usr4 = inserted.usr4
		--		,sl.usr5 = inserted.usr5
		--		,sl.ttmoeda = inserted.slttmoeda
		--		,sl.pcpond = inserted.pcusto
		--		,sl.cpoc = inserted.cpoc
		--		,sl.valiva = CASE 
		--						WHEN inserted.ivaincl = 1 THEN inserted.ttdeb - inserted.ttdeb / (1 + inserted.iva / 100)
		--						ELSE inserted.ttdeb * inserted.iva / 100
		--						END
		--		,sl.trfa = ts.trfa
		--		,sl.evaliva = CASE 
		--						WHEN inserted.ivaincl = 1 THEN inserted.ettdeb - inserted.ettdeb / (1 + inserted.iva / 100)
		--						ELSE inserted.ettdeb * inserted.iva / 100
		--						END
		--		,sl.epcpond = inserted.epcusto
		--		,sl.stns = inserted.stns
		--	FROM
		--		inserted
		--		INNER JOIN deleted ON deleted.bistamp = inserted.bistamp
		--		INNER JOIN bo (nolock) ON inserted.bostamp = bo.bostamp
		--		INNER JOIN ts (nolock) ON ts.ndos = inserted.ndos
		--		INNER JOIN ts2 (nolock) ON ts2.ts2stamp = ts.tsstamp
		--	WHERE
		--		inserted.bistamp = sl.slstamp 
		--		AND ts.stocks = 1

		--	if @@ROWCOUNT > 0
		--		SET @Msg += '<Update SL (' + convert(varchar,getdate(),8) + ')>'
		--end

		--/*
		--	Update sl
		--	trf armazem
		--*/
		--IF EXISTS (SELECT i.marcada FROM inserted i INNER JOIN ts (nolock) ON ts.ndos = i.ndos WHERE ts.stocks = 1 AND ts.trfa = 1 AND i.ref != '')
		--begin
		--	UPDATE
		--		SL
		--	SET 
		--		sl.usrdata = inserted.usrdata
		--		,sl.usrhora = inserted.usrhora
		--		,sl.usrinis = inserted.usrinis
		--		,sl.fref = CASE 
		--					WHEN ts.lifref = 1 THEN inserted.bifref
		--					ELSE bo.fref
		--					END
		--		,sl.ccusto = CASE 
		--						WHEN ts.tccusto = 1	THEN inserted.ccusto
		--						ELSE bo.ccusto
		--						END
		--		,sl.ncusto = CASE 
		--						WHEN ts.tncusto = 1	THEN inserted.ncusto
		--						ELSE bo.ncusto
		--						END
		--		,sl.nome = bo.nome
		--		,sl.codigo = inserted.codigo
		--		,sl.moeda = bo.moeda
		--		,sl.datalc = inserted.rdata
		--		,sl.design = inserted.design
		--		,sl.ref = inserted.ref
		--		,sl.lote = inserted.lote
		--		,sl.armazem = inserted.ar2mazem
		--		,sl.qtt = inserted.qtt
		--		,sl.num1 = inserted.num1
		--		,sl.tt = inserted.sltt
		--		,sl.vu = inserted.slvu
		--		,sl.usr1 = inserted.usr1
		--		,sl.usr2 = inserted.usr2
		--		,sl.usr3 = inserted.usr3
		--		,sl.usr4 = inserted.usr4
		--		,sl.usr5 = inserted.usr5
		--		,sl.ett = inserted.esltt
		--		,sl.evu = inserted.eslvu
		--		,sl.frcl = bo.no
		--		,sl.adoc = convert(CHAR(10), bo.obrano)
		--		,sl.cm = ts.cm2stocks
		--		,sl.cmdesc = ts.cm2desc
		--		,sl.composto = inserted.composto
		--		,sl.cor = inserted.cor
		--		,sl.tam = inserted.tam
		--		,sl.vumoeda = inserted.slvumoeda
		--		,sl.ttmoeda = inserted.slttmoeda
		--		,sl.pcpond = inserted.pcusto
		--		,sl.cpoc = inserted.cpoc
		--		,sl.valiva = CASE 
		--						WHEN inserted.ivaincl = 1 THEN inserted.ttdeb - inserted.ttdeb / (1 + inserted.iva / 100)
		--						ELSE inserted.ttdeb * inserted.iva / 100
		--						END
		--		,sl.trfa = ts.trfa
		--		,sl.evaliva = CASE 
		--						WHEN inserted.ivaincl = 1 THEN inserted.ettdeb - inserted.ettdeb / (1 + inserted.iva / 100)
		--						ELSE inserted.ettdeb * inserted.iva / 100
		--						END
		--		,sl.epcpond = inserted.epcusto
		--		,sl.stns = inserted.stns
		--	FROM
		--		inserted
		--		INNER JOIN deleted ON deleted.bistamp = inserted.bistamp
		--		INNER JOIN bo (nolock) ON inserted.bostamp = bo.bostamp
		--		INNER JOIN ts (nolock) ON ts.ndos = inserted.ndos
		--	WHERE
		--		sl.slstamp = substring('T' + inserted.bistamp, 1, 25)
		--		AND ts.stocks = 1 
		--		AND ts.trfa = 1

		--	if @@ROWCOUNT > 0
		--		SET @Msg += '<Update SL (' + convert(varchar,getdate(),8) + ')>'
		--end

	end -- end validação de contexto


	/*
		Atualiza reservado a clientes
		Só documentos abertos!!!
	*/
	IF EXISTS (SELECT inserted.marcada FROM INSERTED WHERE inserted.ref != '' AND inserted.rescli = 1 AND inserted.fechada = 0)
	BEGIN
		set Context_Info 0x55556 -- evitar que o trigger de update corra validações desnecessárias

		UPDATE
			ST
		SET
			st.qttcli = st.qttcli + (CASE 
										WHEN i.valor > 0 THEN i.valor
										ELSE 0
										END)
		FROM (
			SELECT 
				valor = SUM(inserted.qtt - inserted.qtt2)
				,inserted.ref
			FROM
				inserted
				--INNER JOIN deleted ON inserted.bistamp = deleted.bistamp
			WHERE
				inserted.ref != ''
				AND inserted.rescli = 1
				AND inserted.fechada = 0
			GROUP BY
				inserted.ref
			) i
		WHERE
			st.ref = i.ref
			and st.site_nr = @site_nr

		if @@ROWCOUNT > 0
			SET @Msg += '<I Update ST (qttcli) (' + convert(varchar,getdate(),8) + ')>'

		set Context_Info 0x0

		UPDATE
			SA
		SET
			sa.rescli = sa.rescli + (CASE 
										WHEN i.valor > 0 THEN i.valor
										ELSE 0
										END)
		FROM (
			SELECT 
				valor = SUM(inserted.qtt - inserted.qtt2)
				,inserted.ref
				,inserted.armazem
			FROM
				inserted
				--INNER JOIN deleted ON inserted.bistamp = deleted.bistamp
			WHERE
				inserted.ref != '' 
				AND inserted.rescli = 1
				AND inserted.fechada = 0
			GROUP BY
				inserted.ref, inserted.armazem
			) i
		WHERE
			sa.ref = i.ref 
			AND sa.armazem = i.armazem

		if @@ROWCOUNT > 0
			SET @Msg += '<I Update SA (qttcli) (' + convert(varchar,getdate(),8) + ')>'
	END


	IF EXISTS (SELECT deleted.marcada FROM deleted WHERE deleted.ref != '' AND deleted.rescli = 1 AND deleted.fechada = 0)
	BEGIN
		set Context_Info 0x55556 -- evitar que o trigger de update corra validações desnecessárias

		UPDATE
			ST
		SET
			st.qttcli = st.qttcli - (CASE 
										WHEN i.valor > 0 THEN i.valor
										ELSE 0
										END)
		FROM (
			SELECT 
				valor = SUM(deleted.qtt - deleted.qtt2)
				,deleted.ref
			FROM
				deleted
				--INNER JOIN deleted ON inserted.bistamp = deleted.bistamp
			WHERE
				deleted.ref != ''
				AND deleted.rescli = 1
				AND deleted.fechada = 0
			GROUP BY
				deleted.ref
			) i
		WHERE
			st.ref = i.ref
			and st.site_nr = @site_nr

		if @@ROWCOUNT > 0
			SET @Msg += '<d Update ST (qttcli) (' + convert(varchar,getdate(),8) + ')>'

		set Context_Info 0x0

		UPDATE
			SA
		SET
			sa.rescli = sa.rescli - (CASE 
										WHEN i.valor > 0 THEN i.valor
										ELSE 0
										END)
		FROM (
			SELECT 
				valor = SUM(deleted.qtt - deleted.qtt2)
				,deleted.ref
				,deleted.armazem
			FROM
				deleted
				--INNER JOIN deleted ON inserted.bistamp = deleted.bistamp
			WHERE
				deleted.ref != '' 
				AND deleted.rescli = 1
				AND deleted.fechada = 0
			GROUP BY
				deleted.ref, deleted.armazem
			) i
		WHERE
			sa.ref = i.ref 
			AND sa.armazem = i.armazem

		if @@ROWCOUNT > 0
			SET @Msg += '<d Update SA (qttcli) (' + convert(varchar,getdate(),8) + ')>'
	END


	/*
		Atualiza encomendado fornecedores
		Só documentos abertos!!!
	*/
	IF EXISTS (SELECT * FROM INSERTED WHERE inserted.ref != '' AND inserted.resfor = 1 AND inserted.fechada = 0)
	BEGIN
		set Context_Info 0x55556 -- evitar que o trigger de update corra validações desnecessárias

		UPDATE
			ST
		SET
			st.qttfor = st.qttfor + (CASE 
										WHEN i.valor > 0 THEN i.valor
										ELSE 0
										END)
		FROM (
			SELECT
				valor = SUM(inserted.qtt - inserted.qtt2)
				,inserted.ref
			FROM
				inserted
				--INNER JOIN deleted ON inserted.bistamp = deleted.bistamp AND deleted.emconf = 1 AND inserted.emconf = 0
			WHERE
				inserted.ref <> '' 
				AND inserted.resfor = 1
				AND inserted.fechada = 0
			GROUP BY
				inserted.ref
			) i
		WHERE
			st.ref = i.ref
			and st.site_nr = @site_nr
				
		if @@ROWCOUNT > 0
			SET @Msg += '<I Update ST (qttfor) (' + convert(varchar,getdate(),8) + ')>'

		set Context_Info 0x0

		UPDATE
			SA
		SET
			sa.resfor = sa.resfor + (CASE 
										WHEN i.valor > 0 THEN i.valor
										ELSE 0
										END)
		FROM (
			SELECT
				valor = SUM(inserted.qtt - inserted.qtt2)
				,inserted.ref
				,inserted.armazem
			FROM
				inserted
				--INNER JOIN deleted ON inserted.bistamp = deleted.bistamp AND deleted.emconf = 1 AND inserted.emconf = 0
			WHERE
				inserted.ref <> '' 
				AND inserted.resfor = 1
				AND inserted.fechada = 0
			GROUP BY
				inserted.ref, inserted.armazem
			) i
		WHERE
			sa.ref = i.ref 
			AND sa.armazem = i.armazem

		if @@ROWCOUNT > 0
			SET @Msg += '<I Update SA (qttfor) (' + convert(varchar,getdate(),8) + ')>'
	END


	IF EXISTS (SELECT * FROM deleted WHERE deleted.ref != '' AND deleted.resfor = 1 AND deleted.fechada = 0)
	BEGIN
		set Context_Info 0x55556 -- evitar que o trigger de update corra validações desnecessárias

		UPDATE
			ST
		SET
			st.qttfor = st.qttfor - (CASE 
										WHEN i.valor > 0 THEN i.valor
										ELSE 0
										END)
		FROM (
			SELECT
				valor = SUM(deleted.qtt - deleted.qtt2)
				,deleted.ref
			FROM
				deleted
				--INNER JOIN deleted ON inserted.bistamp = deleted.bistamp AND deleted.emconf = 1 AND inserted.emconf = 0
			WHERE
				deleted.ref <> '' 
				AND deleted.resfor = 1
				AND deleted.fechada = 0
			GROUP BY
				deleted.ref
			) i
		WHERE
			st.ref = i.ref
			and st.site_nr = @site_nr

		if @@ROWCOUNT > 0
			SET @Msg += '<D Update ST (qttfor) (' + convert(varchar,getdate(),8) + ')>'

		set Context_Info 0x0

		UPDATE
			SA
		SET
			sa.resfor = sa.resfor - (CASE 
										WHEN i.valor > 0 THEN i.valor
										ELSE 0
										END)
		FROM (
			SELECT
				valor = SUM(deleted.qtt - deleted.qtt2)
				,deleted.ref
				,deleted.armazem
			FROM
				deleted
				--INNER JOIN deleted ON inserted.bistamp = deleted.bistamp AND deleted.emconf = 1 AND inserted.emconf = 0
			WHERE
				deleted.ref <> '' 
				AND deleted.resfor = 1
				AND deleted.fechada = 0
			GROUP BY
				deleted.ref, deleted.armazem
			) i
		WHERE
			sa.ref = i.ref 
			AND sa.armazem = i.armazem

		if @@ROWCOUNT > 0
			SET @Msg += '<D Update SA (qttfor) (' + convert(varchar,getdate(),8) + ')>'
	END



	/*
		Atualizar quantidade regularizada (doc origem)
	*/
	if ((SELECT TRIGGER_NESTLEVEL()) <= 1) -- Fail safe à recursividade, por defeito deve estar 0 = não permite recursividade
	BEGIN

		IF UPDATE (qtt) 
		begin
			if EXISTS (SELECT deleted.marcada FROM deleted WHERE deleted.obistamp != '' AND deleted.qtt != 0)
			BEGIN
				UPDATE 
					BI
				SET
					bi.fno = 0
					,bi.adoc = ' '
					,bi.nmdoc = ' '
					,bi.ndoc = 0
					,bi.qtt2 = bi.qtt2 - (SELECT SUM(d.qtt)
											FROM deleted d
											WHERE d.obistamp = deleted.obistamp)
				FROM
					deleted
					INNER JOIN bo (nolock) ON deleted.bostamp = bo.bostamp
				WHERE
					deleted.obistamp != '' 
					AND deleted.obistamp = bi.bistamp

				if @@ROWCOUNT > 0
					SET @Msg += '<D Update BI (origem) (' + convert(varchar,getdate(),8) + ')>'
			END

			IF EXISTS (SELECT inserted.marcada FROM inserted WHERE inserted.obistamp != '' AND inserted.qtt != 0)
			BEGIN
				UPDATE
					BI
				SET
					bi.fdata = bo.dataobra
					,bi.fno = bo.obrano
					,bi.adoc = convert(CHAR(10), bo.obrano)
					,bi.nmdoc = bo.nmdos
					,bi.ndoc = bo.ndos
					,bi.qtt2 = bi.qtt2 + (SELECT SUM(i.qtt)
											FROM inserted i
											WHERE i.obistamp = inserted.obistamp)
											--GROUP BY i.obistamp)
				FROM
					inserted
					INNER JOIN bo (NOLOCK) ON inserted.bostamp = bo.bostamp
				WHERE
					inserted.obistamp != ''
					AND inserted.obistamp = bi.bistamp
			
				if @@ROWCOUNT > 0
					SET @Msg += '<I Update BI (origem) (' + convert(varchar,getdate(),8) + ')>'
			end
		end
	end


	/*
		Update bo
	*/
	IF UPDATE (fdata)
	BEGIN
		UPDATE
			BO
		SET
			bo.ultfact = inserted.fdata
		FROM
			inserted
		WHERE
			inserted.bostamp = bo.bostamp 
			AND inserted.fdata > bo.ultfact

		if @@ROWCOUNT > 0
			SET @Msg += '<I Update BO (' + convert(varchar,getdate(),8) + ')>'
	END

	if update (qtt)
	begin
		IF EXISTS (SELECT i.marcada FROM INSERTED i INNER JOIN ts (nolock) ON i.ndos = ts.ndos WHERE i.ref != '' and ts.cativast=1)
		BEGIN
			set Context_Info 0x55556 -- evitar que o trigger de update corra validações desnecessárias
			print 'UPD CAT'
			update st set cativado=	isnull((select sum(bi.qtt-bi.qtt2) from bi (nolock)
							INNER JOIN ts (nolock) ON bi.ndos = ts.ndos
							inner join bo (nolock) on bo.bostamp=bi.bostamp
							where ts.cativast=1 and bi.fechada=0 and bo.fechada=0 and bi.armazem=st.site_nr and bi.ref=st.ref),0)
			from st (nolock) where ref in (select ref from inserted) and site_nr=@site_nr
				
			if @@ROWCOUNT > 0
				SET @Msg += '<I Update ST (qttcli) (' + convert(varchar,getdate(),8) + ')>'

			set Context_Info 0x0

			update sa set cativado=	isnull((select sum(bi.qtt-bi.qtt2) from bi (nolock)
							INNER JOIN ts (nolock) ON bi.ndos = ts.ndos
							inner join bo (nolock) on bo.bostamp=bi.bostamp
							where ts.cativast=1 and bi.fechada=0 and bo.fechada=0 and bi.armazem=sa.armazem and bi.ref=sa.ref),0)
			from sa (nolock) where ref in (select ref from inserted) and armazem=@site_nr

			if @@ROWCOUNT > 0
				SET @Msg += '<I Update SA (qttcli) (' + convert(varchar,getdate(),8) + ')>'
		END
	end 
END

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger3') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger3]([nome],[Mensagem]) VALUES(@nome,@Msg) ;

/*
	create table ##Debug_Trigger3 (nome varchar(100), mensagem varchar(1000))
	select * from ##Debug_Trigger3
	delete ##Debug_Trigger3
	drop table ##Debug_Trigger3
	select * from bi where ndos=2

	update bi set bistamp='ADM5CD3F83A-8D51-4F4F-A88' where bistamp='ADM5CD3F83A-8D51-4F4F-A88'
*/