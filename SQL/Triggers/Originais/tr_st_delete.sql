USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_st_delete]    Script Date: 06/12/2014 10:07:53 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE TRIGGER [dbo].[tr_st_delete] ON [dbo].[st]  FOR DELETE  AS SET NOCOUNT ONIF (select count(*) from bc inner join deleted on bc.ststamp=deleted.ststamp )>0 BEGINdelete bc from deleted where bc.ststamp=deleted.ststampENDIF (select count(*) from deleted where deleted.stns=1 and deleted.iecaisref=1)>0 BEGINupdate st set st.iecaref=SPACE(18),st.iecarefnome=SPACE(60) from st inner join deleted on st.iecaref=deleted.ref where st.iecasug=1END

GO

