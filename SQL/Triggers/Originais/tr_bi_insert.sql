
/****** Object:  Trigger [dbo].[tr_bi_insert]    Script Date: 16/06/2020 10:49:19 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER TRIGGER [dbo].[tr_bi_insert] ON [dbo].[bi]
FOR INSERT
AS
IF (@@ROWCOUNT = 0)
	RETURN;
SET NOCOUNT ON
-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Bi_Insert'
SET @Msg += '<' + convert(varchar,getdate(),14) + '>'
IF exists (SELECT inserted.bistamp FROM inserted)
BEGIN
	declare @site_nr tinyint
	set @site_nr = isnull((select top 1 empresa_no from empresa_arm (nolock) inner join inserted on empresa_arm.armazem = inserted.armazem where inserted.armazem != 0),1)
	/*
		Criar registo em sa se não existir
	*/
	INSERT INTO SA 
		(sastamp, armazem, ref)
	SELECT distinct 
		convert(CHAR(5), inserted.armazem) + inserted.ref
		,inserted.armazem
		,inserted.ref
	FROM
		inserted
	WHERE
		NOT EXISTS (SELECT sa.armazem FROM sa WHERE sa.ref = inserted.ref AND sa.armazem = inserted.armazem)
		AND inserted.ref != ''
	if @@ROWCOUNT > 0
		SET @Msg += '<insert sa (' + convert(varchar,getdate(),8) + ')>'
	/*	Insert movimentos stock */
	IF exists (SELECT i.marcada FROM inserted i INNER JOIN ts (nolock) ON i.ndos = ts.ndos WHERE ts.stocks = 1 AND i.ref != '' and i.qtt != 0)
	begin
		INSERT INTO SL (
			slstamp
			,bistamp
			,usrinis
			,usrhora
			,usrdata
			,ousrinis
			,ousrhora
			,ousrdata
			,origem
			,fref
			,ccusto
			,ncusto
			,nome
			,codigo
			,moeda
			,datalc
			,design
			,ref
			,armazem
			,qtt
			,lote
			,tt
			,vu
			,ett
			,evu
			,num1
			,frcl
			,adoc
			,cm
			,cmdesc
			,composto
			,cor
			,tam
			,usr1
			,usr2
			,usr3
			,usr4
			,usr5
			,vumoeda
			,ttmoeda
			,pcpond
			,cpoc
			,valiva
			,trfa
			,evaliva
			,epcpond
			,stns
			)
		SELECT 
			inserted.bistamp
			,inserted.bistamp
			,inserted.usrinis
			,inserted.usrhora
			,inserted.usrdata
			,inserted.ousrinis
			,inserted.ousrhora
			,inserted.ousrdata
			,'BO'
			,CASE 
				WHEN ts.lifref = 1 THEN inserted.bifref
				ELSE bo.fref
				END
			,CASE 
				WHEN ts.tccusto = 1 THEN inserted.ccusto
				ELSE bo.ccusto
				END
			,CASE 
				WHEN ts.tncusto = 1	THEN inserted.ncusto
				ELSE bo.ncusto
				END
			,bo.nome
			,inserted.codigo
			,bo.moeda
			,inserted.rdata
			,inserted.design
			,inserted.ref
			,inserted.armazem
			,inserted.qtt
			,inserted.lote
			,inserted.sltt
			,inserted.slvu
			,inserted.esltt
			,inserted.eslvu
			,inserted.num1
			,bo.no
			,convert(CHAR(10), bo.obrano)
			,CASE 
				WHEN (ts.producao = 1 AND inserted.producao = 1) OR (ts2.trocaequi = 1 AND inserted.trocaequi = 1) THEN ts.cm2stocks
				ELSE ts.cmstocks
				END
			,CASE 
				WHEN (ts.producao = 1 AND inserted.producao = 1) OR (ts2.trocaequi = 1 AND inserted.trocaequi = 1) THEN ts.cm2desc
				ELSE ts.cmdesc
				END
			,inserted.composto
			,inserted.cor
			,inserted.tam
			,inserted.usr1
			,inserted.usr2
			,inserted.usr3
			,inserted.usr4
			,inserted.usr5
			,inserted.slvumoeda
			,inserted.slttmoeda
			,inserted.pcusto
			,inserted.cpoc
			,CASE 
				WHEN inserted.ivaincl = 1 THEN inserted.ttdeb - inserted.ttdeb / (1 + inserted.iva / 100)
				ELSE inserted.ttdeb * inserted.iva / 100
				END
			,ts.trfa
			,CASE 
				WHEN inserted.ivaincl = 1 THEN inserted.ettdeb - inserted.ettdeb / (1 + inserted.iva / 100)
				ELSE inserted.ettdeb * inserted.iva / 100
				END
			,inserted.epcusto
			,inserted.stns
		FROM
			inserted
			INNER JOIN bo ON bo.bostamp = inserted.bostamp
			INNER JOIN ts (nolock) ON inserted.ndos = ts.ndos
			INNER JOIN ts2 (nolock) ON ts2.ts2stamp = ts.tsstamp
		WHERE
			ts.stocks = 1
			AND inserted.ref != ''
			and inserted.qtt != 0
		if @@ROWCOUNT > 0
			SET @Msg += '<insert SL (' + convert(varchar,getdate(),8) + ')>'
	end
	/*
		Insert movimentos stock entre armazens (mov adicional)
	*/
	IF exists (SELECT i.marcada FROM inserted i INNER JOIN ts (nolock) ON i.ndos = ts.ndos WHERE ts.stocks = 1 AND ts.trfa = 1 AND i.ref != '' and i.qtt != 0)
	begin
		INSERT INTO SL (
			slstamp
			,bistamp
			,usrinis
			,usrhora
			,usrdata
			,ousrinis
			,ousrhora
			,ousrdata
			,origem
			,fref
			,ccusto
			,ncusto
			,nome
			,codigo
			,moeda
			,datalc
			,design
			,ref
			,armazem
			,qtt
			,lote
			,tt
			,vu
			,ett
			,evu
			,num1
			,frcl
			,adoc
			,cm
			,cmdesc
			,composto
			,cor
			,tam
			,usr1
			,usr2
			,usr3
			,usr4
			,usr5
			,vumoeda
			,ttmoeda
			,pcpond
			,cpoc
			,valiva
			,trfa
			,evaliva
			,epcpond
			,stns
			)
		SELECT
			substring('T' + inserted.bistamp, 1, 25)
			,inserted.bistamp
			,inserted.usrinis
			,inserted.usrhora
			,inserted.usrdata
			,inserted.ousrinis
			,inserted.ousrhora
			,inserted.ousrdata
			,'BO'
			,CASE 
				WHEN ts.lifref = 1 THEN inserted.bifref
				ELSE bo.fref
				END
			,CASE 
				WHEN ts.tccusto = 1 THEN inserted.ccusto
				ELSE bo.ccusto
				END
			,CASE 
				WHEN ts.tncusto = 1 THEN inserted.ncusto
				ELSE bo.ncusto
				END
			,bo.nome
			,inserted.codigo
			,bo.moeda
			,inserted.rdata
			,inserted.design
			,inserted.ref
			,inserted.ar2mazem
			,inserted.qtt
			,inserted.lote
			,inserted.sltt
			,inserted.slvu
			,inserted.esltt
			,inserted.eslvu
			,inserted.num1
			,bo.no
			,convert(CHAR(10), bo.obrano)
			,ts.cm2stocks
			,ts.cm2desc
			,inserted.composto
			,inserted.cor
			,inserted.tam
			,inserted.usr1
			,inserted.usr2
			,inserted.usr3
			,inserted.usr4
			,inserted.usr5
			,inserted.slvumoeda
			,inserted.slttmoeda
			,inserted.pcusto
			,inserted.cpoc
			,CASE
				WHEN inserted.ivaincl = 1 THEN inserted.ttdeb - inserted.ttdeb / (1 + inserted.iva / 100)
				ELSE inserted.ttdeb * inserted.iva / 100
				END
			,ts.trfa
			,CASE 
				WHEN inserted.ivaincl = 1 THEN inserted.ettdeb - inserted.ettdeb / (1 + inserted.iva / 100)
				ELSE inserted.ettdeb * inserted.iva / 100
				END
			,inserted.epcusto
			,inserted.stns
		FROM
			inserted
			INNER JOIN bo ON bo.bostamp = inserted.bostamp
			INNER JOIN ts (nolock) ON inserted.ndos = ts.ndos
		WHERE
			ts.stocks = 1 
			AND ts.trfa = 1
			AND inserted.ref != ''
			and inserted.qtt != 0
		if @@ROWCOUNT > 0
			SET @Msg += '<insert SL (trf armazens) (' + convert(varchar,getdate(),8) + ')>'
	END
	/*
		Atualiza reservado a clientes
	*/
	IF exists (SELECT i.marcada FROM INSERTED i WHERE i.ref != '' and i.rescli = 1 AND i.fechada = 0)
	begin
		set Context_Info 0x55556 -- evitar que o trigger de update corra validações desnecessárias
		UPDATE
			ST
		SET
			st.qttcli = st.qttcli + (CASE
										WHEN i.valor > 0 THEN i.valor
										ELSE 0
										END)
		FROM (
			SELECT 
				valor = SUM(inserted.qtt - inserted.qtt2)
				,inserted.ref
			FROM
				inserted
			WHERE
				inserted.ref != '' AND inserted.rescli = 1 AND inserted.fechada = 0
			GROUP BY
				inserted.ref
			) i
		WHERE
			st.ref = i.ref
			and st.site_nr = @site_nr
		if @@ROWCOUNT > 0
			SET @Msg += '<Update ST (qttcli) (' + convert(varchar,getdate(),8) + ')>'
		set Context_Info 0x0
		UPDATE 
			SA
		SET
			sa.rescli = sa.rescli + (CASE 
										WHEN i.valor > 0 THEN i.valor
										ELSE 0
										END)
		FROM (
			SELECT 
				valor = SUM(inserted.qtt - inserted.qtt2)
				,inserted.ref
				,inserted.armazem
			FROM
				inserted
			WHERE
				inserted.ref <> '' AND inserted.rescli = 1 AND inserted.fechada = 0
			GROUP BY
				inserted.ref, inserted.armazem
			) i
		WHERE
			sa.ref = i.ref 
			AND sa.armazem = i.armazem
		if @@ROWCOUNT > 0
			SET @Msg += '<Update SA (rescli) (' + convert(varchar,getdate(),8) + ')>'
	END
	/*
		Update encomendado a fornecedores
	*/
	IF exists (SELECT i.marcada FROM INSERTED i WHERE i.ref <> '' AND i.resfor = 1 AND i.fechada = 0)
	begin
		set Context_Info 0x55556 -- evitar que o trigger de update corra validações desnecessárias
		UPDATE 
			ST
		SET
			st.qttfor = st.qttfor + (CASE 
										WHEN i.valor > 0 THEN i.valor
										ELSE 0
										END)
		FROM (
			SELECT 
				valor = SUM(inserted.qtt - inserted.qtt2)
				,inserted.ref
			FROM
				inserted
			WHERE
				inserted.ref <> '' AND inserted.resfor = 1 AND inserted.fechada = 0
			GROUP BY
				inserted.ref
			) i
		WHERE
			st.ref = i.ref
			and st.site_nr = @site_nr
		if @@ROWCOUNT > 0
			SET @Msg += '<Update ST (qttfor) (' + convert(varchar,getdate(),8) + ')>'
		set Context_Info 0x0
		UPDATE
			SA
		SET
			sa.resfor = sa.resfor + (CASE 
										WHEN i.valor > 0 THEN i.valor
										ELSE 0
										END)
		FROM (
			SELECT 
				valor = SUM(inserted.qtt - inserted.qtt2)
				,inserted.ref
				,inserted.armazem
			FROM
				inserted
			WHERE
				inserted.ref <> '' AND inserted.resfor = 1 AND inserted.fechada = 0
			GROUP BY
				inserted.ref, inserted.armazem
			) i
		WHERE
			sa.ref = i.ref 
			AND sa.armazem = i.armazem
		if @@ROWCOUNT > 0
			SET @Msg += '<Update SA (resfor) (' + convert(varchar,getdate(),8) + ')>'
	end
	/*
		Atualizar quantidade regularizada (origem)
	*/
	IF exists (SELECT i.marcada FROM inserted i WHERE i.obistamp <> '' AND i.qtt <> 0)
	BEGIN
		set Context_Info 0x55555 -- evitar que o trigger de update corra validações desnecessárias
		UPDATE
			BI
		SET
			bi.fdata = bo.dataobra
			,bi.fno = bo.obrano
			,bi.adoc = convert(CHAR(10), bo.obrano)
			,bi.nmdoc = bo.nmdos
			,bi.ndoc = bo.ndos
			,bi.qtt2 = bi.qtt2 + (SELECT SUM(i.qtt) FROM inserted i WHERE i.obistamp = inserted.obistamp)
			,bi.pbruto = bi.pbruto + (SELECT SUM(i.qtt) FROM inserted i WHERE i.obistamp = inserted.obistamp) -- campo utilizado como qtt2, mas para ser compativel com as compras que não têm qtt2
		FROM
			inserted
			INNER JOIN bo ON inserted.bostamp = bo.bostamp
		WHERE
			inserted.obistamp != ''
			and inserted.obistamp = bi.bistamp
		if @@ROWCOUNT > 0
			SET @Msg += '<Update BI (origem) (' + convert(varchar,getdate(),8) + ')>'
		set Context_Info 0x0
	END

	/*
		Atualiza cativação de stocks
	*/
	IF exists (SELECT i.marcada FROM inserted i INNER JOIN ts (nolock) ON i.ndos = ts.ndos WHERE i.ref != '' and i.qtt != 0 and ts.cativast=1 AND i.fechada = 0)
	begin
		set Context_Info 0x55556 -- evitar que o trigger de update corra validações desnecessárias
			print 'UPD CAT'
			update st set cativado=	isnull((select sum(bi.qtt-bi.qtt2) from bi (nolock)
							INNER JOIN ts (nolock) ON bi.ndos = ts.ndos
							inner join bo (nolock) on bo.bostamp=bi.bostamp
							where ts.cativast=1 and bi.fechada=0 and bo.fechada=0 and bi.armazem=st.site_nr and bi.ref=st.ref),0)
			from st (nolock) where ref in (select ref from inserted) and site_nr=@site_nr
				
			if @@ROWCOUNT > 0
				SET @Msg += '<I Update ST (qttcli) (' + convert(varchar,getdate(),8) + ')>'

			set Context_Info 0x0

			update sa set cativado=	isnull((select sum(bi.qtt-bi.qtt2) from bi (nolock)
							INNER JOIN ts (nolock) ON bi.ndos = ts.ndos
							inner join bo (nolock) on bo.bostamp=bi.bostamp
							where ts.cativast=1 and bi.fechada=0 and bo.fechada=0 and bi.armazem=sa.armazem and bi.ref=sa.ref),0)
			from sa (nolock) where ref in (select ref from inserted) and armazem=@site_nr

			if @@ROWCOUNT > 0
				SET @Msg += '<I Update SA (qttcli) (' + convert(varchar,getdate(),8) + ')>'
	END
	-- Insert Lotes
	--... em validação
END
-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger3') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger3]([nome],[Mensagem]) VALUES(@nome,@Msg) ;
