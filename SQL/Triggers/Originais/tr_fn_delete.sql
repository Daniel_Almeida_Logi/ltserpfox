USE [farmDev15_temp]
GO

/****** Object:  Trigger [dbo].[tr_fn_delete]    Script Date: 06/12/2014 10:15:08 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE TRIGGER [dbo].[tr_fn_delete] ON [dbo].[fn]  FOR DELETE  AS SET NOCOUNT ONDELETE FROM SL FROM deleted where 	sl.fnstamp=deleted.fnstamp 	and deleted.fnstamp<>''	DELETE FROM SV FROM deleted where 	sv.fnstamp=deleted.fnstamp 	and deleted.fnstamp<>''	If (select top 1 cm1.fonactqtt2 from deleted inner join fo on fo.fostamp=deleted.fostamp inner join cm1 on fo.doccode=cm1.cm)=0Begin	UPDATE BI 	SET 		bi.adoc = '',		bi.nmdoc = '',		bi.ofostamp = '',		bi.ndoc = 0, 		bi.qtt2 = bi.qtt2-(select SUM(deleted.qtt) from deleted where deleted.bistamp <> '' and deleted.bistamp = bi.bistamp) 	from bi 	inner join deleted on deleted.bistamp=bi.bistamp 	where 		deleted.bistamp<>'' End

GO

