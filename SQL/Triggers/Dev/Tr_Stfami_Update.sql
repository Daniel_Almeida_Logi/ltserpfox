
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[tr_stfami_update]
ON [dbo].[stfami]
FOR UPDATE
AS

IF (@@ROWCOUNT = 0)
	RETURN;

SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(100) = ''
	,@nome varchar(20) = 'Tr_Stfami_Update'

IF update (nome)
BEGIN

	SET @Msg += '<' + convert(varchar,getdate(),14) + '>'

	update
		st
	set
		st.faminome = inserted.nome
	from
		inserted
	where
		st.familia = inserted.ref

	if @@ROWCOUNT > 0
		SET @Msg += '<Update ST (' + convert(varchar,getdate(),8) + ')>'
END

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg);