
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TRIGGER [dbo].[tr_po_update] 
ON [dbo].[po]  
FOR UPDATE  
AS 

IF (@@ROWCOUNT = 0)
	RETURN;
	
SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE 
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Po_Update'	

SET @Msg += '<' + convert(varchar,getdate(),14) + '>'

if	update(usrhora) or update(no) or update(nome) or update(total) or update(etotal) or update(procdata) or update(difcambio) or update(edifcambio) or update(arred)
	or update(etotal) or update(earred) or update(moeda) or update(rno) or update(procdata) or update(fref) or update(intid) or update(ccusto) or update(ncusto) or update(zona) 
	or update(tipo) or update(pais) or update(estab) or update(desc1) or update(etotol2) or update(ollocal) or update(contado) or update(ollocal2) or update(contado2)
	or update(telocal) or update(telocal2) or update(olcodigo) or update(finv) or update(process)
BEGIN

	INSERT INTO FC 
		(
		fcstamp
		,postamp
		,usrdata,usrhora,usrinis,ousrdata,ousrhora,ousrinis
		,datalc
		,no
		,nome
		,cm
		,cmdesc
		,origem
		,difcambio
		,edifcambio
		,edeb
		,edebf
		,debm
		,debfm
		,ecred
		,ecredf
		,credm
		,credfm
		,moeda
		,adoc
		,dataven
		,fref
		,ccusto
		,ncusto
		,intid
		,tipo
		,estab
		,pais
		,zona
		,obs
		) 
	select 
		inserted.postamp
		,inserted.postamp
		,inserted.usrdata, inserted.usrhora, inserted.usrinis, inserted.ousrdata, inserted.ousrhora, inserted.ousrinis
		,inserted.procdata
		,inserted.no
		,inserted.nome
		,inserted.cm
		,inserted.cmdesc
		,'PO'
		,inserted.difcambio
		,inserted.edifcambio
		,case when inserted.etotal<0 then 0 else inserted.etotal-inserted.earred end
		,case when inserted.etotal<0 then 0 else inserted.etotal-inserted.earred end
		,case when inserted.totalmoeda<0 then 0 else inserted.totalmoeda end
		,case when inserted.totalmoeda<0 then 0 else inserted.totalmoeda end
		,case when inserted.etotal>0 then 0 else -inserted.etotal-inserted.earred end
		,case when inserted.etotal>0 then 0 else -inserted.etotal-inserted.earred end
		,case when inserted.totalmoeda>0 then 0 else -inserted.totalmoeda end
		,case when inserted.totalmoeda>0 then 0 else -inserted.totalmoeda end
		,inserted.moeda
		,inserted.adoc
		,inserted.procdata
		,inserted.fref
		,inserted.ccusto
		,inserted.ncusto
		,inserted.intid
		,inserted.tipo
		,inserted.estab
		,inserted.pais
		,inserted.zona
		,inserted.desc1 
	from
		inserted 
	where 
		inserted.process=1 
		and inserted.postamp not in (select fc.fcstamp from fc where fc.fcstamp=inserted.postamp) 
	
	if @@ROWCOUNT>0
		SET @Msg += '<I Insert FC (' + convert(varchar,getdate(),8) + ')>'

	UPDATE
		FC 
	SET 
		fc.fcstamp=inserted.postamp
		,fc.postamp=inserted.postamp
		,fc.usrdata=inserted.usrdata
		,fc.usrhora=inserted.usrhora
		,fc.usrinis=inserted.usrinis
		,fc.datalc=inserted.procdata
		,fc.no=inserted.no
		,fc.nome=inserted.nome
		,fc.cm=inserted.cm
		,fc.cmdesc=inserted.cmdesc
		,fc.origem='PO'
		,fc.difcambio=inserted.difcambio
		,fc.edifcambio=inserted.edifcambio
		,fc.edeb=case when inserted.etotal<0 then 0 else inserted.etotal-inserted.earred end
		,fc.edebf=case when inserted.etotal<0 then 0 else inserted.etotal-inserted.earred end
		,fc.debm=case when inserted.totalmoeda<0 then 0 else inserted.totalmoeda end,fc.debfm=case when inserted.totalmoeda<0 then 0 else inserted.totalmoeda end
		,fc.ecred=case when inserted.etotal>0 then 0 else -inserted.etotal-inserted.earred end
		,fc.ecredf=case when inserted.etotal>0 then 0 else -inserted.etotal-inserted.earred end
		,fc.credm=case when inserted.totalmoeda>0 then 0 else -inserted.totalmoeda end,fc.credfm=case when inserted.totalmoeda>0 then 0 else -inserted.totalmoeda end
		,fc.moeda=inserted.moeda
		,fc.adoc=inserted.adoc
		,fc.dataven=inserted.procdata
		,fc.fref=inserted.fref
		,fc.ccusto=inserted.ccusto
		,fc.ncusto=inserted.ncusto
		,fc.intid=inserted.intid
		,fc.tipo=inserted.tipo
		,fc.estab=inserted.estab
		,fc.pais=inserted.pais
		,fc.zona=inserted.zona
		,fc.obs=inserted.desc1
	from
		inserted 
	where 
		inserted.process=1
		and inserted.postamp=fc.fcstamp 
		--and inserted.postamp in (select fc.fcstamp from fc where fc.fcstamp=inserted.postamp)  
		
	
	if @@ROWCOUNT>0
		SET @Msg += '<I Update FC (' + convert(varchar,getdate(),8) + ')>'

	DELETE
		fc 
	FROM
		inserted 
	where 
		inserted.process=0 
		and inserted.postamp=fc.fcstamp
		--and inserted.postamp in (select fc.fcstamp from fc where fc.fcstamp=inserted.postamp) 
		 

	if @@ROWCOUNT > 0
		SET @Msg += '<I Delete FC (' + convert(varchar,getdate(),8) + ')>'	

	INSERT INTO FC 
		(
		fcstamp
		,postamp
		,usrdata,usrhora,usrinis,ousrdata,ousrhora,ousrinis
		,datalc
		,no
		,nome
		,cm
		,cmdesc
		,origem
		,edeb

		,edebf
		,debm
		,debfm

		,ecred

		,ecredf
		,credm
		,credfm
		,moeda
		,adoc
		,dataven
		,fref
		,ccusto
		,ncusto
		,intid
		,tipo
		,estab
		,pais
		,zona
		,obs
		) 
	select 
		substring('F'+inserted.postamp,1,25)
		,inserted.postamp
		,inserted.usrdata, inserted.usrhora, inserted.usrinis,	inserted.ousrdata, inserted.ousrhora, inserted.ousrinis
		,inserted.procdata
		,inserted.no
		,inserted.nome
		,8
		,(select cmdesc from cm1 (nolock) where cm=8)
		,'PO'
		,case when inserted.efinv < 0 then 0 else inserted.efinv end
		,case when inserted.efinv < 0 then 0 else inserted.efinv end
		,case when inserted.finvmoeda < 0 then 0 else inserted.finvmoeda end
		,case when inserted.finvmoeda < 0 then 0 else inserted.finvmoeda end
		,case when inserted.efinv > 0 then 0 else -inserted.efinv end
		,case when inserted.efinv > 0 then 0 else -inserted.efinv end
		,case when inserted.finvmoeda > 0 then 0 else -inserted.finvmoeda end,case when inserted.finvmoeda > 0 then 0 else -inserted.finvmoeda end
		,inserted.moeda
		,inserted.adoc
		,inserted.procdata
		,inserted.fref
		,inserted.ccusto
		,inserted.ncusto
		,inserted.intid
		,inserted.tipo
		,inserted.estab
		,inserted.pais
		,inserted.zona
		,inserted.desc1
	from
		inserted
	where
		inserted.process=1
		and substring('F'+inserted.postamp,1,25) not in (select fc.fcstamp from fc where fc.fcstamp=substring('F'+inserted.postamp,1,25))
		and inserted.finv<>0

	if @@ROWCOUNT>0
		SET @Msg += '<I Insert FC (' + convert(varchar,getdate(),8) + ')>'


	UPDATE
		FC 
	SET 
		fc.fcstamp=substring('F'+inserted.postamp,1,25)
		,fc.postamp=inserted.postamp
		,fc.usrdata=inserted.usrdata
		,fc.usrhora=inserted.usrhora
		,fc.usrinis=inserted.usrinis
		,fc.datalc=inserted.procdata
		,fc.no=inserted.no
		,fc.nome=inserted.nome
		,fc.cm=8
		,fc.cmdesc=(select cmdesc from cm1 (nolock) where cm=8)
		,fc.origem='PO'
		,fc.edeb=case when inserted.efinv<0 then 0 else inserted.efinv end
		,fc.edebf=case when inserted.efinv<0 then 0 else inserted.efinv end
		,fc.debm=case when inserted.finvmoeda<0 then 0 else inserted.finvmoeda end
		,fc.debfm=case when inserted.finvmoeda<0 then 0 else inserted.finvmoeda end
		,fc.ecred=case when inserted.efinv>0 then 0 else -inserted.efinv end
		,fc.ecredf=case when inserted.efinv>0 then 0 else -inserted.efinv end
		,fc.credm=case when inserted.finvmoeda>0 then 0 else -inserted.finvmoeda end
		,fc.credfm=case when inserted.finvmoeda>0 then 0 else -inserted.finvmoeda end
		,fc.moeda=inserted.moeda
		,fc.adoc=inserted.adoc
		,fc.dataven=inserted.procdata
		,fc.fref=inserted.fref
		,fc.ccusto=inserted.ccusto
		,fc.ncusto=inserted.ncusto
		,fc.intid=inserted.intid
		,fc.tipo=inserted.tipo
		,fc.estab=inserted.estab
		,fc.pais=inserted.pais
		,fc.zona=inserted.zona
		,fc.obs=inserted.desc1
	from
		inserted 
	where 
		inserted.process=1 
		--and substring('F'+inserted.postamp,1,25) in (select fc.fcstamp from fc where fc.fcstamp=substring('F'+inserted.postamp,1,25))  
		and substring('F'+inserted.postamp,1,25) = fc.fcstamp 
		and inserted.finv <> 0 
	
	if @@ROWCOUNT>0
		SET @Msg += '<I Update FC (' + convert(varchar,getdate(),8) + ')>'


	INSERT INTO FC 
		(fcstamp
		,postamp
		,usrdata,usrhora,usrinis,ousrdata,ousrhora,ousrinis
		,datalc
		,no
		,nome
		,cm
		,cmdesc
		,origem
		,edeb
		,edebf
		,ecred
		,ecredf
		,moeda
		,adoc
		,dataven
		,fref
		,ccusto
		,ncusto
		,intid
		,tipo
		,estab
		,pais
		,zona
		,obs) 
	select 
		substring('I'+inserted.postamp,1,25)
		,inserted.postamp
		,inserted.usrdata, inserted.usrhora, inserted.usrinis, inserted.ousrdata, inserted.ousrhora, inserted.ousrinis
		,inserted.procdata
		,inserted.no
		,inserted.nome
		,10
		,(select cmdesc from cm1 (nolock) where cm=10)
		,'PO'
		,case when inserted.evirs<0 then 0 else inserted.evirs end
		,case when inserted.evirs<0 then 0 else inserted.evirs end
		,case when inserted.evirs>0 then 0 else -inserted.evirs end
		,case when inserted.evirs>0 then 0 else -inserted.evirs end
		,'EURO'
		,inserted.adoc
		,inserted.procdata
		,inserted.fref
		,inserted.ccusto
		,inserted.ncusto
		,inserted.intid
		,inserted.tipo
		,inserted.estab
		,inserted.pais
		,inserted.zona
		,inserted.desc1
	from
		inserted 
	where 
		inserted.process=1 
		and (substring('I'+inserted.postamp,1,25) not in (select fc.fcstamp from fc where fc.fcstamp=substring('I'+inserted.postamp,1,25))) 
		and (inserted.virs<>0 or inserted.evirs<>0) 
	
	if @@ROWCOUNT>0
		SET @Msg += '<I insert FC (' + convert(varchar,getdate(),8) + ')>'

	UPDATE
		FC
	SET
		fc.fcstamp=substring('I'+inserted.postamp,1,25)
		,fc.postamp=inserted.postamp
		,fc.usrdata=inserted.usrdata
		,fc.usrhora=inserted.usrhora
		,fc.usrinis=inserted.usrinis
		,fc.datalc=inserted.procdata
		,fc.no=inserted.no
		,fc.nome=inserted.nome
		,fc.cm=10
		,fc.cmdesc=(select cmdesc from cm1 (nolock) where cm=10)
		,fc.origem='PO'
		,fc.edeb=case when inserted.evirs<0 then 0 else inserted.evirs end
		,fc.debf=case when inserted.virs<0 then 0 else inserted.virs end
		,fc.edebf=case when inserted.evirs<0 then 0 else inserted.evirs end
		,fc.ecred=case when inserted.evirs>0 then 0 else -inserted.evirs end
		,fc.ecredf=case when inserted.evirs>0 then 0 else -inserted.evirs end
		,fc.moeda='EURO'
		,fc.adoc=inserted.adoc
		,fc.dataven=inserted.procdata
		,fc.fref=inserted.fref
		,fc.ccusto=inserted.ccusto
		,fc.ncusto=inserted.ncusto
		,fc.intid=inserted.intid
		,fc.tipo=inserted.tipo
		,fc.estab=inserted.estab
		,fc.pais=inserted.pais
		,fc.zona=inserted.zona
		,fc.obs=inserted.desc1
	from
		inserted
	where
		inserted.process=1
		--and substring('I'+inserted.postamp,1,25) in (select fc.fcstamp from fc where fc.fcstamp=substring('I'+inserted.postamp,1,25))
		and substring('I'+inserted.postamp,1,25)=fc.fcstamp
		and (inserted.virs<>0 or inserted.evirs<>0)


	if @@ROWCOUNT>0
		SET @Msg += '<I Update FC (' + convert(varchar,getdate(),8) + ')>'

	DELETE
		fc 
	FROM
		inserted 
	where 
		inserted.process = 0 
		and (
			substring('F'+inserted.postamp,1,25) = fc.fcstamp 
				--in (select fc.fcstamp from fc where fc.fcstamp=substring('F'+inserted.postamp,1,25)) and   fc.fcstamp=substring('F'+inserted.postamp,1,25)) 
			or 
			substring('I'+inserted.postamp,1,25) = fc.fcstamp
				--in (select fc.fcstamp from fc where fc.fcstamp=substring('I'+inserted.postamp,1,25)) and substring('I'+inserted.postamp,1,25)=fc.fcstamp)
		) 

	if @@ROWCOUNT>0
		SET @Msg += '<I Delete FC (' + convert(varchar,getdate(),8) + ')>'
END


-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg);