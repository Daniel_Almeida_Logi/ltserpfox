
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[Tr_Ft_Update]
ON [dbo].[ft] FOR UPDATE
AS

IF (@@ROWCOUNT = 0) 
	RETURN ;

SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Ft_Update'

SET @Msg += '<' + convert(varchar,getdate(),14) + '>'

IF UPDATE(ettiliq) or update(ftstamp) or update(no) or update(nome) or update(etotal) or update(clbanco) or update(clcheque) 
	or update(tpstamp) or update(tpdesc) or update(fdata) or update(cobranca) or update(cambiofixo) or update(edebreg) or update(debregm)
	or update(eivav1) or update(eivav2) or update(eivav3) or update(eivav4) or update(eivav5) or update(eivav6) or update(eivav7) or update(eivav8) or update(eivav9) 
	or update(moeda) or update(fno) or update(pdata) or update(fref) or update(intid) or update(cobrador) or update(rota) or update(ccusto) or update(ncusto) or update(zona) 
	or update(segmento) or update(tipo) or update(pais) or update(estab) or update(vENDedor) or update(vENDnm) or update(erdtotal) or update(evirs) or update(echtotal)
BEGIN 		
	
	DELETE
		CC 
	FROM
		deleted 
	WHERE 
		cc.ftstamp = deleted.ftstamp 
		AND deleted.multi=0 
		AND deleted.ftstamp NOT IN (SELECT inserted.ftstamp FROM inserted WHERE inserted.multi=0) 
	
	if @@ROWCOUNT > 0
		SET @Msg += '<Delete CC (' + convert(varchar,getdate(),8) + ')>'

	IF EXISTS (SELECT i.ftstamp FROM inserted i INNER JOIN td (nolock) ON td.ndoc=i.ndoc WHERE td.lancacc=1 AND i.multi=0)
	begin

		INSERT INTO CC 
			(
			tpstamp
			,tpdesc
			,ccstamp
			,ftstamp
			,usrdata
			,usrhora
			,usrinis
			,ousrdata
			,ousrhora
			,ousrinis
			,datalc
			,no
			,nome
			,cm
			,cmdesc
			,origem
			,cobranca
			,cambiofixo
			,edeb
			,ecred
			,debm
			,credm
			,ecredf
			,credfm
			,eivav1,eivav2,eivav3,eivav4,eivav5,eivav6,eivav7,eivav8,eivav9
			,IVATX1,IVATX2,IVATX3,IVATX4,IVATX5,IVATX6,IVATX7,IVATX8,IVATX9
			,REEXGIVA
			,formapag
			,situacao
			,clbanco
			,clcheque
			,moeda
			,nrdoc
			,dataven
			,fref
			,intid
			,cobrador
			,rota
			,ccusto
			,ncusto
			,zona
			,segmento
			,tipo
			,pais
			,estab
			,vENDedor
			,vENDnm
			,evalch
			,evirs
			) 
		SELECT 
			inserted.tpstamp
			,inserted.tpdesc
			,inserted.ftstamp
			,inserted.ftstamp
			,inserted.usrdata
			,inserted.usrhora
			,inserted.usrinis
			,inserted.ousrdata
			,inserted.ousrhora
			,inserted.ousrinis
			,inserted.fdata
			,CASE WHEN td.lancacli=0 THEN inserted.no ELSE ft2.c2no END
			,CASE WHEN td.lancacli=0 THEN inserted.nome ELSE ft2.c2nome END
			,td.cmcc
			,td.cmccn
			,'FT'
			,inserted.cobranca
			,inserted.cambiofixo
			,CASE WHEN td.tipodoc=3 THEN 0 ELSE inserted.etotal END
			,CASE WHEN td.tipodoc=3 THEN inserted.etotal*-1 ELSE 0 END
			,CASE WHEN td.tipodoc=3 THEN 0 ELSE inserted.totalmoeda END
			,CASE WHEN td.tipodoc=3 THEN inserted.totalmoeda*-1 ELSE 0 END
			,CASE WHEN inserted.edebreg>=CASE WHEN td.tipodoc=3 THEN inserted.etotal*-1 ELSE 0 END THEN CASE WHEN td.tipodoc=3 THEN inserted.etotal*-1 ELSE 0 END ELSE inserted.edebreg END
			,CASE WHEN inserted.debregm>=CASE WHEN td.tipodoc=3 THEN inserted.totalmoeda*-1 ELSE 0 END THEN CASE WHEN td.tipodoc=3 THEN inserted.totalmoeda*-1 ELSE 0 END ELSE inserted.debregm END
			,inserted.eivav1,inserted.eivav2,inserted.eivav3,inserted.eivav4,inserted.eivav5,inserted.eivav6,inserted.eivav7,inserted.eivav8,inserted.eivav9
			,inserted.IVATX1,inserted.IVATX2,inserted.IVATX3,inserted.IVATX4,inserted.IVATX5,inserted.IVATX6,inserted.IVATX7,inserted.IVATX8,inserted.IVATX9
			,ft2.REEXGIVA
			,isnull(ft2.formapag,'1')
			,'1'
			,inserted.clbanco
			,inserted.clcheque
			,inserted.moeda
			,inserted.fno
			,inserted.pdata
			,inserted.fref
			,inserted.intid
			,inserted.cobrador
			,inserted.rota
			,inserted.ccusto
			,inserted.ncusto
			,inserted.zona
			,inserted.segmento
			,inserted.tipo
			,CASE WHEN td.lancacli=0 THEN inserted.pais ELSE ft2.c2pais END,CASE WHEN td.lancacli=0 THEN inserted.estab ELSE ft2.c2estab END
			,inserted.vENDedor
			,inserted.vENDnm
			,CASE WHEN inserted.cheque=1 AND td.tipodoc<>3 THEN inserted.etotal ELSE 0 END
			,inserted.evirs
		FROM
			inserted 
			INNER JOIN td (nolock) on inserted.ndoc=td.ndoc 
			INNER JOIN ft2 on ft2.ft2stamp = inserted.ftstamp 
		WHERE 
			td.lancacc=1 
			AND inserted.multi = 0 
			AND inserted.ftstamp not in (SELECT cc.ccstamp FROM cc WHERE cc.ccstamp = inserted.ftstamp)

		if @@ROWCOUNT > 0
			SET @Msg += '<Insert CC (' + convert(varchar,getdate(),8) + ')>'
	end
	

	UPDATE
		CC 
	SET 
		cc.usrdata=inserted.usrdata
		,cc.usrhora=inserted.usrhora
		,cc.usrinis=inserted.usrinis
		,cc.tpstamp=inserted.tpstamp
		,cc.tpdesc=inserted.tpdesc
		,cc.datalc=inserted.fdata
		,cc.no=CASE WHEN td.lancacli=0 THEN inserted.no ELSE ft2.c2no END
		,cc.nome=CASE WHEN td.lancacli=0 THEN inserted.nome ELSE ft2.c2nome END
		,cc.cm=td.cmcc
		,cc.cmdesc=td.cmccn
		,cc.cobranca=inserted.cobranca
		,cc.cambiofixo=inserted.cambiofixo
		,cc.moeda=inserted.moeda
		,cc.nrdoc=inserted.fno
		,cc.dataven=inserted.pdata
		,cc.fref=inserted.fref
		,cc.intid=inserted.intid
		,cc.cobrador=inserted.cobrador
		,cc.rota=inserted.rota
		,cc.ccusto=inserted.ccusto
		,cc.ncusto=inserted.ncusto
		,cc.zona=inserted.zona
		,cc.segmento=inserted.segmento
		,cc.tipo=inserted.tipo
		,cc.pais=CASE WHEN td.lancacli=0 THEN inserted.pais ELSE ft2.c2pais END
		,cc.estab=CASE WHEN td.lancacli=0 THEN inserted.estab ELSE ft2.c2estab END
		,cc.vENDedor=inserted.vENDedor
		,cc.vENDnm=inserted.vENDnm
		,cc.eivav1=inserted.eivav1
		,cc.eivav2=inserted.eivav2
		,cc.eivav3=inserted.eivav3
		,cc.eivav4=inserted.eivav4
		,cc.eivav5=inserted.eivav5
		,cc.eivav6=inserted.eivav6
		,cc.eivav7=inserted.eivav7
		,cc.eivav8=inserted.eivav8
		,cc.eivav9=inserted.eivav9
		,cc.IVATX1=inserted.IVATX1
		,cc.IVATX2=inserted.IVATX2
		,cc.IVATX3=inserted.IVATX3
		,cc.IVATX4=inserted.IVATX4
		,cc.IVATX5=inserted.IVATX5
		,cc.IVATX6=inserted.IVATX6
		,cc.IVATX7=inserted.IVATX7
		,cc.IVATX8=inserted.IVATX8
		,cc.IVATX9=inserted.IVATX9
		,cc.REEXGIVA=td.REEXGIVA
		,cc.formapag=ft2.formapag
		,cc.edeb=CASE WHEN td.tipodoc=3 THEN 0 ELSE inserted.etotal END
		,cc.ecred=CASE WHEN td.tipodoc=3 THEN inserted.etotal*-1 ELSE 0 END
		,cc.debm=CASE WHEN td.tipodoc=3 THEN 0 ELSE inserted.totalmoeda END
		,cc.credm=CASE WHEN td.tipodoc=3 THEN inserted.totalmoeda*-1 ELSE 0 END
		,cc.clbanco=inserted.clbanco
		,cc.clcheque=inserted.clcheque
		,cc.ecredf=(CASE WHEN cc.ecredf+inserted.edebreg>=(CASE WHEN td.tipodoc=3 THEN inserted.etotal*-1 ELSE 0 END) THEN (CASE WHEN td.tipodoc=3 THEN inserted.etotal*-1 ELSE 0 END) ELSE cc.ecredf+inserted.edebreg END)
		,cc.credfm=(CASE WHEN cc.credfm+inserted.debregm>=(CASE WHEN td.tipodoc=3 THEN inserted.totalmoeda*-1 ELSE 0 END) THEN (CASE WHEN td.tipodoc=3 THEN inserted.totalmoeda*-1 ELSE 0 END) ELSE cc.credfm+inserted.debregm END)
		,cc.evalch=cc.evalch+CASE WHEN inserted.cheque=1 AND td.tipodoc<>3 THEN inserted.etotal ELSE 0 END
		,cc.virs=inserted.virs
		,cc.evirs=inserted.evirs 
	FROM
		inserted 
		INNER JOIN [dbo].[Td] (nolock) on inserted.ndoc = td.ndoc 
		INNER JOIN [dbo].[ft2] on ft2.ft2stamp = inserted.ftstamp 
		INNER JOIN deleted on deleted.ftstamp = inserted.ftstamp
	WHERE 
		inserted.ftstamp = cc.ftstamp 
		AND td.lancacc = 1 
		AND inserted.multi = 0 	
		and cc.ccstamp <> substring('R'+inserted.ftstamp,1,25) 

	if @@ROWCOUNT > 0
		SET @Msg += '<Update CC (' + convert(varchar,getdate(),8) + ')>'
END

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg) ;