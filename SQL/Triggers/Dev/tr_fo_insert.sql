
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[tr_fo_insert] 
ON [dbo].[fo]  
FOR INSERT  
AS 

IF (@@ROWCOUNT = 0)
	RETURN;

SET NOCOUNT ON

DECLARE 
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Fo_Insert'	
	
set @Msg = '<' + convert(varchar,getdate(),14) + '>'

INSERT INTO FC 
	(
	tpdesc
	,tpstamp
	,fcstamp
	,fostamp
	,usrdata
	,usrhora
	,usrinis
	,ousrdata
	,ousrhora
	,ousrinis
	,datalc
	,no
	,nome
	,cm
	,cmdesc
	,origem
	,ecred
	,edeb
	,credm
	,debm
	,aprovado
	,nmaprov
	,dtaprov
	,eivav1
	,eivav2
	,eivav3
	,eivav4
	,eivav5
	,eivav6
	,eivav7
	,eivav8
	,eivav9
	,IVATX1
	,IVATX2
	,IVATX3
	,IVATX4
	,IVATX5
	,IVATX6
	,IVATX7
	,IVATX8
	,IVATX9
	,REEXGIVA
	,formapag
	,moeda
	,adoc
	,dataven
	,fref
	,cambiofixo
	,ccusto
	,ncusto
	,intid
	,tipo
	,estab
	,pais
	,zona
	,virs
	,evirs
	,crend
	,obs
	)
select 
	inserted.tpdesc
	,inserted.tpstamp
	,inserted.fostamp
	,inserted.fostamp
	,inserted.usrdata
	,inserted.usrhora
	,inserted.usrinis
	,inserted.ousrdata
	,inserted.ousrhora
	,inserted.ousrinis
	,inserted.data
	,inserted.no
	,inserted.nome
	,inserted.doccode
	,inserted.docnome
	,'FO'
	,case when cm1.debito=1 then 0 else ( case when inserted.pais=2 or fo2.ettinvsuj > 0 then inserted.etotal-(case when inserted.pais=2 then inserted.ettiva else (ISNULL(fo2.ettinvsuj,0)) end) else inserted.etotal end ) end
	,case when cm1.debito=1 then ( case when inserted.pais=2 or fo2.ettinvsuj > 0 then inserted.etotal-(case when inserted.pais=2 then inserted.ettiva else (ISNULL(fo2.ettinvsuj,0)) end) else inserted.etotal end ) else 0 end
	,case when cm1.debito=1 then 0 else ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.totmoeda-inserted.tmiva else inserted.totmoeda end ) end 
	,case when cm1.debito=1 then ( case when inserted.pais=2 or fo2.ettinvsuj>0 then inserted.totmoeda-inserted.tmiva else inserted.totmoeda end ) else 0 end
	,inserted.aprovado
	,inserted.nmaprov
	,inserted.dtaprov
	,inserted.eivav1
	,inserted.eivav2
	,inserted.eivav3
	,inserted.eivav4
	,inserted.eivav5
	,inserted.eivav6
	,inserted.eivav7
	,inserted.eivav8,inserted.eivav9
	,ISNULL(fo2.IVATX1,0)
	,ISNULL(fo2.IVATX2,0)
	,ISNULL(fo2.IVATX3,0)
	,ISNULL(fo2.IVATX4,0)
	,ISNULL(fo2.IVATX5,0)
	,ISNULL(fo2.IVATX6,0)
	,ISNULL(fo2.IVATX7,0)
	,ISNULL(fo2.IVATX8,0)
	,ISNULL(fo2.IVATX9,0)
	,ISNULL(fo2.REEXGIVA,0)
	,'1'
	,inserted.moeda
	,inserted.adoc
	,inserted.pdata
	,inserted.fref
	,inserted.cambiofixo
	,inserted.ccusto
	,inserted.ncusto
	,inserted.intid
	,inserted.tipo
	,inserted.estab
	,inserted.pais
	,inserted.zona
	,case when cm1.debito=1 then inserted.virs*-1 else inserted.virs end
	,case when cm1.debito=1 then inserted.evirs*-1 else inserted.evirs end
	,inserted.crend
	,inserted.final
from
	inserted
	inner join cm1 (nolock) on cm1.cm = inserted.doccode
	inner join fo2 on fo2.fo2stamp = inserted.fostamp
where
	cm1.folanfc = 1
	and inserted.bloqpag = 0
	and inserted.multi = 0

if @@ROWCOUNT > 0
	SET @Msg += '<Insert FC (' + convert(varchar,getdate(),8) + ')>'

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg);
