
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[tr_fc_update]
ON [dbo].[fc]
FOR UPDATE
AS

IF (@@ROWCOUNT = 0)
	RETURN;

SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Fc_Update'


set @Msg = '<' + convert(varchar,getdate(),14) + '>'

--declare @m_banchab char(60), @m_contado float
--select @m_banchab = isnull((select textValue from B_Parameters where stamp = 'ADM0000000252'),'')/*(select valor from para1 where descricao='m_banchab')*/
--SELECT @m_contado = (SELECT TOP 1 noconta FROM bl WHERE bl.banco = SUBSTRING(@m_banchab, 1, 10) AND bl.conta = SUBSTRING(@m_banchab, 12, 20))

--if @m_contado = NULL
--	select @m_contado = 1

--if @m_banchab = NULL
--	select @m_banchab = ''

IF UPDATE(ecred) or UPDATE(edeb) or UPDATE(no)
begin
	UPDATE
		FL 
	SET 
		fl.esaldo=fl.esaldo +
					(select SUM(i.ecred - i.edeb + i.edifcambio) from inserted i where i.no=fl.no)
	where
		fl.no in (select no from inserted)
	--from
	--	INSERTED 
	--where 
	--	fl.no=inserted.no

	if @@ROWCOUNT > 0
		SET @Msg += '<(I) Update FL (esaldo)>'

	UPDATE
		FL 
	SET 
		fl.esaldo=fl.esaldo -
					(select SUM(d.ecred - d.edeb + d.edifcambio) from deleted d where d.no=fl.no)
	where
		fl.no in (select no from deleted)
	--from
	--	DELETED 
	--where 
	--	fl.no=deleted.no

	if @@ROWCOUNT > 0
		SET @Msg += '<(D) Update FL (esaldo) (' + convert(varchar,getdate(),8) + ')>'
end


if ((SELECT TRIGGER_NESTLEVEL()) <= 1) -- Fail safe à recursividade, por defeito deve estar 0 = não permite recursividade
BEGIN

	IF UPDATE(no) or UPDATE(ecred) or UPDATE(edeb) or UPDATE(cr) or UPDATE(cradoc) 
	begin
		UPDATE
			FC 
		SET 
			fc.credf = fc.credf-deleted.deb, fc.debf = fc.debf-deleted.cred
			,fc.ecredf = fc.ecredf-deleted.edeb, fc.edebf = fc.edebf-deleted.ecred 
		from
			deleted 
		where 
			fc.cm=deleted.cr 
			and fc.adoc=deleted.cradoc 
			and fc.no=deleted.no 
			and deleted.cr > 0 
			and deleted.cradoc != ''
	
		if @@ROWCOUNT > 0
			SET @Msg += '<(D) Update FC (' + convert(varchar,getdate(),8) + ')>'


		UPDATE
			FC 
		SET 
			fc.credf = fc.credf+inserted.deb, fc.debf = fc.debf+inserted.cred
			,fc.ecredf = fc.ecredf+inserted.edeb, fc.edebf = fc.edebf+inserted.ecred 
		from
			INSERTED 
		where 
			fc.cm=inserted.cr 
			and fc.adoc=inserted.cradoc 
			and fc.no=inserted.no 
			and inserted.cr > 0 
			and inserted.cradoc != ''

		if @@ROWCOUNT > 0
			SET @Msg += '<(I) Update FC (' + convert(varchar,getdate(),8) + ')>'
	end
end


IF UPDATE(crend)
begin
	UPDATE
		PL
	SET
		pl.crend=inserted.crend
	from
		INSERTED
	where
		pl.fcstamp=inserted.fcstamp

	if @@ROWCOUNT > 0
		SET @Msg += '<Update PL (' + convert(varchar,getdate(),8) + ')>'
end

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg) ;