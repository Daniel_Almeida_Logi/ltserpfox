SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[tr_re_delete]
ON [dbo].[re]
FOR DELETE
AS

IF (@@ROWCOUNT = 0)
	RETURN;
	
SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(100) = ''
	,@nome varchar(20) = 'Tr_Re_Delete'

SET @Msg += '<' + convert(varchar,getdate(),14) + '>'

DELETE
	CC
from
	deleted
where
	cc.restamp=deleted.restamp

if @@ROWCOUNT > 0
	SET @Msg += '<delete CC (' + convert(varchar,getdate(),8) + ')>'

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg);	