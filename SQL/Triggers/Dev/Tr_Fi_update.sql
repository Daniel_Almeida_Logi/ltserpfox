SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[Tr_Fi_UPDATE] 
ON [dbo].[fi] FOR UPDATE 
AS

IF (@@ROWCOUNT = 0) 
	RETURN ;

SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE 
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Fi_Update'	


set @Msg = '<' + convert(varchar,getdate(),14) + '>'

--update ft set numLinhas = (select COUNT(*) FROM fi(nolock) where fi.ftstamp = ft.ftstamp) FROM  ft(nolock) inner join inserted as i ON i.ftstamp = ft.ftstamp

/*
	Delete sl
*/
DELETE
	SL
FROM
	inserted
WHERE
	sl.fistamp = inserted.fistamp
	AND inserted.fistamp != ''
	AND inserted.ref = ''

if @@ROWCOUNT > 0
	SET @Msg += '<Delete SL (' + convert(varchar,getdate(),8) + ')>'

/*
	Update sl
*/
IF	UPDATE(usrdata) or UPDATE(usrhora) or UPDATE(usrinis) or UPDATE(fifref) or UPDATE(codigo) or UPDATE(design) 
	or UPDATE(ref) or UPDATE(lote) or UPDATE(armazem) or UPDATE(qtt) or UPDATE(iectin) or UPDATE(ficcusto) or UPDATE(fincusto) 
	or UPDATE(desconto) or UPDATE(desc2) or UPDATE(desc3) or UPDATE(desc4) or UPDATE(desc5) or UPDATE(desc6) 
	or UPDATE(iva) or UPDATE(ivaincl) or UPDATE(eiectin) or UPDATE(num1) or UPDATE(composto) or UPDATE(cor) or UPDATE(tam) 
	or UPDATE(usr1) or UPDATE(usr2) or UPDATE(usr3) or UPDATE(usr4) or UPDATE(usr5) or UPDATE(pvmoeda) or UPDATE(cpoc) 
	or UPDATE(epv) or UPDATE(evbase) or UPDATE(etiliquido) or UPDATE(ecusto) or UPDATE(epcp) or UPDATE(ftstamp) or UPDATE(ndoc) or UPDATE(fistamp) or UPDATE(fno) 
BEGIN

	UPDATE
		SL
	set
		sl.usrdata=inserted.usrdata,sl.usrhora=inserted.usrhora,sl.usrinis=inserted.usrinis
		,sl.fref=CASE WHEN td.lifref=1 then inserted.fifref else ft.fref end,sl.ccusto=CASE WHEN td.liccusto=1 then inserted.ficcusto else ft.ccusto end
		,sl.ncusto=CASE WHEN td.lincusto=1 then inserted.fincusto else ft.ncusto end,sl.nome=ft.nome,sl.segmento=ft.segmento
		,sl.codigo=inserted.codigo,sl.moeda=ft.moeda,sl.datalc=ft.fdata,sl.design=inserted.design
		,sl.ref=inserted.ref,sl.lote=inserted.lote,sl.armazem=inserted.armazem,sl.qtt=CASE WHEN ft.tipodoc=3 then inserted.qtt*-1 else inserted.qtt end
		,sl.ett=inserted.esltt
		,sl.evu=inserted.eslvu
		,sl.num1=inserted.num1
		,sl.frcl=ft.no,sl.adoc=convert(char (10), ft.fno),sl.cm=td.cmsl,sl.cmdesc=td.cmsln,sl.composto=inserted.composto,sl.cor=inserted.cor,sl.tam=inserted.tam
		,sl.usr1=inserted.usr1,sl.usr2=inserted.usr2,sl.usr3=inserted.usr3,sl.usr4=inserted.usr4,sl.usr5=inserted.usr5
		,sl.vumoeda=inserted.slvumoeda
		,sl.ttmoeda=inserted.slttmoeda
		,sl.valiva=CASE WHEN inserted.iectin<>0 then inserted.pv-inserted.vbase else CASE WHEN inserted.ivaincl=1 then inserted.tiliquido-inserted.tiliquido/(1+inserted.iva/100) else inserted.tiliquido*inserted.iva/100 end end
		,sl.evaliva=CASE WHEN inserted.eiectin<>0 then inserted.epv-inserted.evbase else CASE WHEN inserted.ivaincl=1 then inserted.etiliquido-inserted.etiliquido/(1+inserted.iva/100) else inserted.etiliquido*inserted.iva/100 end end
		,sl.epcpond=inserted.epcp
		,sl.stns=inserted.stns 
	FROM
		inserted 
		INNER JOIN ft (nolock) on inserted.ftstamp=ft.ftstamp 
		INNER JOIN td (nolock) on td.ndoc=inserted.ndoc 
	WHERE 
		inserted.fistamp = sl.fistamp 
		AND td.lancasl = 1 
		AND inserted.fistamp IN (select deleted.fistamp FROM deleted) 

	if @@ROWCOUNT > 0
		SET @Msg += '<Update SL (' + convert(varchar,getdate(),8) + ')>'
END


/*
	Insert sl
*/
IF	UPDATE(fistamp) or UPDATE(usrinis) or UPDATE(usrhora) or UPDATE(usrdata) or UPDATE(ousrinis) or UPDATE(ousrhora) or UPDATE(ousrdata) 
	or UPDATE(fifref) or UPDATE(codigo) or UPDATE(design) or UPDATE(ref) or UPDATE(lote) or UPDATE(armazem) or UPDATE(qtt) or UPDATE(iectin) 
	or UPDATE(desconto) or UPDATE(desc2) or UPDATE(desc3) or UPDATE(desc4) or UPDATE(desc5) or UPDATE(desc6) or UPDATE(iva) or UPDATE(ivaincl) 
	or UPDATE(eiectin) or UPDATE(epv) or UPDATE(num1) or UPDATE(composto) or UPDATE(cor) or UPDATE(tam) 
	or UPDATE(usr1) or UPDATE(usr2) or UPDATE(usr3) or UPDATE(usr4) or UPDATE(usr5) or UPDATE(pvmoeda) 
	or UPDATE(cpoc) or UPDATE(tiliquido) or UPDATE(evbase) or UPDATE(etiliquido) or UPDATE(ecusto) or UPDATE(epcp) or UPDATE(ftstamp) or UPDATE(ndoc) or UPDATE(fistamp) or UPDATE(fno) 
BEGIN
	INSERT INTO SL 
		(
		slstamp,fistamp,stns,usrinis,usrhora,usrdata,ousrinis,ousrhora,ousrdata,origem,
		fref,ccusto,ncusto,nome,segmento,codigo,moeda,datalc,design,
		ref,lote,armazem,qtt,
		ett,
		evu,
		num1,
		frcl,adoc,cm,cmdesc,composto,cor,tam,usr1,usr2,usr3,usr4,usr5,
		vumoeda,
		ttmoeda,
		pcpond,cpoc,
		valiva,evaliva,epcpond 
		)
	SELECT 
		inserted.fistamp,inserted.fistamp,inserted.stns,inserted.usrinis,inserted.usrhora,inserted.usrdata,inserted.ousrinis,inserted.ousrhora,inserted.ousrdata,'FT'
		,CASE WHEN td.lifref=1 then inserted.fifref else ft.fref end,CASE WHEN td.liccusto=1 then inserted.ficcusto else ft.ccusto end
		,CASE WHEN td.lincusto=1 then inserted.fincusto else ft.ncusto end,ft.nome,ft.segmento,inserted.codigo,ft.moeda,ft.fdata,inserted.design
		,inserted.ref,inserted.lote,inserted.armazem,CASE WHEN ft.tipodoc=3 then inserted.qtt*-1 else inserted.qtt end
		,inserted.esltt
		,inserted.eslvu
		,inserted.num1
		,ft.no,convert(char (10), ft.fno),td.cmsl,td.cmsln,inserted.composto,inserted.cor,inserted.tam,inserted.usr1,inserted.usr2,inserted.usr3,inserted.usr4,inserted.usr5
		,inserted.slvumoeda
		,inserted.slttmoeda
		,inserted.pcp,inserted.cpoc
		,CASE WHEN inserted.iectin<>0 then inserted.pv-inserted.vbase else CASE WHEN inserted.ivaincl=1 then inserted.tiliquido-inserted.tiliquido/(1+inserted.iva/100) else inserted.tiliquido*inserted.iva/100 end end
		,CASE WHEN inserted.eiectin<>0 then inserted.epv-inserted.evbase else CASE WHEN inserted.ivaincl=1 then inserted.etiliquido-inserted.etiliquido/(1+inserted.iva/100) else inserted.etiliquido*inserted.iva/100 end end
		,inserted.epcp
	FROM
		inserted
		INNER JOIN ft (nolock) on ft.ftstamp = inserted.ftstamp
		INNER JOIN td (nolock) on inserted.ndoc = td.ndoc
	WHERE
		ft.anulado = 0
		AND NOT EXISTS (select slstamp FROM sl WHERE sl.slstamp=inserted.fistamp)
		AND td.lancasl = 1
		AND LTRIM(RTRIM(inserted.ref)) != ''

	if @@ROWCOUNT > 0
		SET @Msg += '<Insert SL (' + convert(varchar,getdate(),8) + ')>'
END


-- update bi
IF	UPDATE(qtt) or UPDATE(ftstamp) OR UPDATE(bistamp) 
BEGIN
	
	set Context_Info 0x55555 -- evitar que o trigger de update corra validações desnecessárias
	
	UPDATE
		BI 
	SET 
		bi.fno = 0
		,bi.adoc = ''
		,bi.nmdoc = ''
		,bi.oftstamp = ''
		,bi.ndoc = 0
		,bi.qtt2 = bi.qtt2 - (select SUM(d.qtt *(CASE WHEN d.tipodoc=3 then -1 else 1 end)) FROM deleted d WHERE d.bistamp = bi.bistamp)
		,bi.pbruto = bi.pbruto - (select SUM(d.qtt *(CASE WHEN d.tipodoc=3 then -1 else 1 end)) FROM deleted d WHERE d.bistamp = bi.bistamp)
	FROM
		deleted 
	WHERE 
		deleted.bistamp != '' 
		and deleted.bistamp = bi.bistamp

	if @@ROWCOUNT > 0
		SET @Msg += '<D Update BI (origem) (' + convert(varchar,getdate(),8) + ')>'


	UPDATE
		BI
	SET
		bi.fdata = ft.fdata
		,bi.fno = ft.fno
		,bi.adoc = convert(char(10),ft.fno)
		,bi.oftstamp = ft.ftstamp
		,bi.nmdoc = ft.nmdoc
		,bi.ndoc = ft.ndoc
		,bi.qtt2 = bi.qtt2 + (select SUM(i.qtt*(CASE WHEN i.tipodoc=3 then -1 else 1 end)) FROM inserted i WHERE i.bistamp = bi.bistamp)
		,bi.pbruto = bi.pbruto + (select SUM(i.qtt*(CASE WHEN i.tipodoc=3 then -1 else 1 end)) FROM inserted i WHERE i.bistamp = bi.bistamp)
	FROM
		inserted
		INNER JOIN ft (nolock) ON inserted.ftstamp=ft.ftstamp
	WHERE 
		inserted.bistamp <> '' 
		and inserted.bistamp=bi.bistamp

	if @@ROWCOUNT > 0
		SET @Msg += '<I Update BI (origem) (' + convert(varchar,getdate(),8) + ')>'


	set Context_Info 0x0
END


-- update Ft
IF EXISTS(select * FROM inserted WHERE ndocft<>0 AND fnoft<>0)
begin
	UPDATE
		FT
	SET
		ft.facturada = 1
		,ft.nmdocft = td.nmdoc
		,ft.fnoft = inserted.fno 
	FROM
		inserted 
		INNER JOIN td (nolock) on td.ndoc=inserted.ndoc 
	WHERE 
		inserted.ndocft <> 0 
		AND inserted.ndocft = ft.ndoc 
		AND inserted.fnoft = ft.fno  
		AND inserted.ftanoft = ft.ftano

	if @@ROWCOUNT > 0
		SET @Msg += '<Update FT (' + convert(varchar,getdate(),8) + ')>'
end


-- update ft
IF  UPDATE(QTT) OR UPDATE(ECUSTO)
BEGIN

	UPDATE
		FT
	SET
		ft.ecusto=ft.ecusto-CASE WHEN deleted.tipodoc=3 then -deleted.qtt*deleted.ecusto else deleted.qtt*deleted.ecusto end 
	FROM
		deleted 
	WHERE 
		deleted.ftstamp=ft.ftstamp

	if @@ROWCOUNT > 0
		SET @Msg += '<D Update FT (ecusto) (' + convert(varchar,getdate(),8) + ')>'

	UPDATE
		FT 
	SET 
		ft.ecusto=ft.ecusto+CASE WHEN inserted.tipodoc=3 then -inserted.qtt*inserted.ecusto else inserted.qtt*inserted.ecusto end 
	FROM
		inserted 
	WHERE 
		inserted.ftstamp=ft.ftstamp

	if @@ROWCOUNT > 0
		SET @Msg += '<I Update FT (ecusto) (' + convert(varchar,getdate(),8) + ')>'
END

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg) ;
