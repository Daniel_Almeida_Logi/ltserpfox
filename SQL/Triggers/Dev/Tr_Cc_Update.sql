SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[tr_cc_update] 
ON [dbo].[cc] 
FOR UPDATE 
AS 

IF (@@ROWCOUNT = 0 or isnull(Context_Info(),0x0) = 0x55555)
	RETURN;

SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Cc_Update'


set @Msg = '<' + convert(varchar,getdate(),14) + '>'

--declare @m_banchab char(60), @m_contado float
--select @m_banchab = isnull((select textValue from B_Parameters where stamp = 'ADM0000000252'),'')/*(select valor from para1 where descricao='m_banchab')*/
--SELECT @m_contado = (SELECT TOP 1 noconta FROM bl WHERE bl.banco = SUBSTRING(@m_banchab, 1, 10) AND bl.conta = SUBSTRING(@m_banchab, 12, 20))

--if @m_contado = NULL
--	select @m_contado = 0

--if @m_banchab = NULL
--	select @m_banchab = ''

/*
	update utentes
*/
IF UPDATE(edeb) or UPDATE(ecred) or UPDATE(no) or UPDATE(estab)
begin
	UPDATE
		b_utentes 
	SET 
		b_utentes.esaldo=b_utentes.esaldo-(deleted.edeb-deleted.ecred) 
	from
		DELETED 
	where 
		b_utentes.no=deleted.no and b_utentes.estab=deleted.estab

	if @@ROWCOUNT > 0
		SET @Msg += '<(D) Update B_UTENTES (' + convert(varchar,getdate(),8) + ')>'

	UPDATE
		b_utentes 
	SET 
		b_utentes.esaldo=b_utentes.esaldo+inserted.edeb-inserted.ecred 
	from
		INSERTED 
	where 
		b_utentes.no=inserted.no and b_utentes.estab=inserted.estab 

	if @@ROWCOUNT > 0
		SET @Msg += '<(I) Update B_UTENTES (' + convert(varchar,getdate(),8) + ')>'
end


if ((SELECT TRIGGER_NESTLEVEL()) <= 1) -- Fail safe à recursividade, por defeito deve estar 0 = não permite recursividade
BEGIN
	
	set @Msg = '<Trigger NesteLevel = ' + ltrim(str((SELECT TRIGGER_NESTLEVEL()))) + '>'

	if UPDATE(no) or UPDATE(edeb) or UPDATE(ecred) or UPDATE(cr) or UPDATE(docref)
	begin
		
		IF exists (select count(*) from deleted where RTRIM(LTRIM(deleted.occstamp)) = '')
		begin
	
			UPDATE
				CC
			SET
				cc.edebf = cc.edebf-deleted.ecred
				,cc.ecredf = cc.ecredf-deleted.edeb 
			from
				deleted
			where
				cc.cm = deleted.cr
				and cc.nrdoc = deleted.docref
				and cc.no = deleted.no
				and deleted.origem = 'CC'

			if @@ROWCOUNT > 0
				SET @Msg += '<(D) Update CC (' + convert(varchar,getdate(),8) + ')>'

			UPDATE
				CC
			SET
				cc.edebf = cc.edebf-deleted.ecred
				,cc.ecredf = cc.ecredf-deleted.edeb 
			from
				deleted 
			where 
				cc.cm=deleted.cr 
				and cc.nrdoc=deleted.docref 
				and cc.no=deleted.no 
				and deleted.origem='CC' 
				and cc.ccstamp = deleted.occstamp

			if @@ROWCOUNT > 0
				SET @Msg += '<(D) Update CC (' + convert(varchar,getdate(),8) + ')>'
		end


		IF (select count(*) from inserted where RTRIM(LTRIM(inserted.occstamp)) = '') > 0
		begin
			UPDATE
				CC 
			SET 
				cc.edebf = cc.edebf+inserted.ecred
				,cc.ecredf = cc.ecredf+inserted.edeb 
			from
				INSERTED 
			where 
				cc.cm=inserted.cr 
				and cc.nrdoc=inserted.docref 
				and cc.no=inserted.no 
				and inserted.origem='CC' 

			if @@ROWCOUNT > 0
				SET @Msg += '<(I) Update CC (' + convert(varchar,getdate(),8) + ')>'

			UPDATE
				CC
			SET
				cc.edebf = cc.edebf+inserted.ecred
				,cc.ecredf = cc.ecredf+inserted.edeb
			from
				INSERTED
			where
				cc.cm=inserted.cr 
				and cc.nrdoc=inserted.docref 
				and cc.no=inserted.no 
				and inserted.origem='CC' 
				and cc.ccstamp = inserted.occstamp

			if @@ROWCOUNT > 0
				SET @Msg += '<(I) Update CC (' + convert(varchar,getdate(),8) + ')>'
		end
	end
END

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg) ;