SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[tr_fo_delete] 
ON [dbo].[fo]  
FOR DELETE  
AS 

IF (@@ROWCOUNT = 0)
	RETURN;

SET NOCOUNT ON

DECLARE 
	 @Msg varchar(100) = ''
	,@nome varchar(20) = 'tr_fo_delete'	

set @Msg = '<' + convert(varchar,getdate(),14) + '>'
		
DELETE
	FC 
from
	deleted
where 
	fc.fostamp=deleted.fostamp 

if @@ROWCOUNT > 0
	SET @Msg += '<Delete FC (' + convert(varchar,getdate(),8) + ')>'

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg) ;
