
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TRIGGER [dbo].[tr_fl_update] 
ON [dbo].[fl] 
/*WITH ENCRYPTION*/ 
FOR UPDATE 
AS 

IF (@@ROWCOUNT = 0)
	RETURN;
	
SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Fl_Update'

SET @Msg += '<' + convert(varchar,getdate(),14) + '>'

/*
	Update fl - vencimento dependentes
*/
If exists (select i.flstamp from inserted i inner join deleted d on i.flstamp=d.flstamp where i.vencimento<>d.vencimento and i.estab=0 and i.forvd=0)
BEGIN
		
	If exists (select i.flstamp from fl inner join inserted i on fl.no=i.no and fl.estab<>0 and i.estab=0)
	begin

		update
			fl 
		set 
			fl.vencimento=inserted.vencimento 
		from
			inserted 
		where 
			fl.no=inserted.no 
			and fl.estab<>0 
			and inserted.estab=0

		if @@ROWCOUNT > 0
			SET @Msg += '<Update FL (vencimento dependentes) (' + convert(varchar,getdate(),8) + ')>'
	end
END

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg);	
