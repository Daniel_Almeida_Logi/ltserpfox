
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[Tr_Ft_Insert]
ON [dbo].[ft] FOR INSERT
AS

IF (@@ROWCOUNT = 0) 
	RETURN ;

SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Ft_Insert'

SET @Msg += '<' + convert(varchar,getdate(),14) + '>'

-- 	Documentos: N/Nt. Crédito,N/Nt. Débito, N/Factura
IF EXISTS (SELECT i.ftstamp FROM inserted i INNER JOIN td (nolock) on td.ndoc=i.ndoc WHERE td.lancacc = 1 AND i.multi = 0)
BEGIN
	
	INSERT INTO [dbo].[CC]
		(
			tpstamp
			,tpdesc
			,ccstamp
			,ftstamp
			,usrdata
			,usrhora
			,usrinis
			,ousrdata
			,ousrhora
			,ousrinis
			,datalc
			,no
			,nome
			,cm
			,cmdesc
			,origem
			,cobranca
			,cambiofixo
			,edeb
			,ecred
			,debm
			,credm
			,ecredf
			,credfm
			,ivav1
			,ivav2
			,ivav3
			,ivav4
			,ivav5
			,ivav6
			,ivav7
			,ivav8
			,ivav9
			,eivav1
			,eivav2
			,eivav3
			,eivav4
			,eivav5
			,eivav6
			,eivav7
			,eivav8
			,eivav9
			,IVATX1
			,IVATX2
			,IVATX3
			,IVATX4
			,IVATX5
			,IVATX6
			,IVATX7
			,IVATX8
			,IVATX9
			,REEXGIVA
			,formapag
			,situacao
			,clbanco
			,clcheque
			,moeda
			,nrdoc
			,dataven
			,fref
			,intid
			,cobrador
			,rota
			,ccusto
			,ncusto
			,zona
			,segmento
			,tipo
			,pais
			,estab
			,vendedor
			,vendnm
			,evalch
			,evirs
		) 
	SELECT 
		inserted.tpstamp
		,inserted.tpdesc
		,inserted.ftstamp
		,inserted.ftstamp
		,inserted.usrdata
		,inserted.usrhora
		,inserted.usrinis
		,inserted.ousrdata
		,inserted.ousrhora
		,inserted.ousrinis
		,inserted.fdata
		,CASE WHEN td.lancacli=0 THEN inserted.no ELSE ft2.c2no END
		,CASE WHEN td.lancacli=0 THEN inserted.nome ELSE ft2.c2nome END
		,td.cmcc
		,td.cmccn,'FT'
		,inserted.cobranca
		,inserted.cambiofixo
		,CASE WHEN td.tipodoc=3 then 0 else inserted.etotal end
		,CASE WHEN td.tipodoc=3 then inserted.etotal*-1 else 0 end
		,CASE WHEN td.tipodoc=3 then 0 else inserted.totalmoeda end
		,case when td.tipodoc=3 then inserted.totalmoeda*-1 else 0 end
		,CASE WHEN inserted.edebreg>=case when td.tipodoc=3 then inserted.etotal*-1 else 0 end then case when td.tipodoc=3 then inserted.etotal*-1 else 0 end else inserted.edebreg end
		,CASE WHEN inserted.debregm>=case when td.tipodoc=3 then inserted.totalmoeda*-1 else 0 end then case when td.tipodoc=3 then inserted.totalmoeda*-1 else 0 end else inserted.debregm end
		,inserted.ivav1
		,inserted.ivav2
		,inserted.ivav3
		,inserted.ivav4
		,inserted.ivav5
		,inserted.ivav6
		,inserted.ivav7
		,inserted.ivav8
		,inserted.ivav9
		,inserted.eivav1
		,inserted.eivav2
		,inserted.eivav3
		,inserted.eivav4
		,inserted.eivav5
		,inserted.eivav6
		,inserted.eivav7
		,inserted.eivav8
		,inserted.eivav9
		,inserted.IVATX1
		,inserted.IVATX2
		,inserted.IVATX3
		,inserted.IVATX4
		,inserted.IVATX5
		,inserted.IVATX6
		,inserted.IVATX7
		,inserted.IVATX8
		,inserted.IVATX9
		,ft2.REEXGIVA
		,ft2.formapag
		,'1'
		,inserted.clbanco
		,inserted.clcheque
		,inserted.moeda
		,inserted.fno
		,inserted.pdata
		,inserted.fref
		,inserted.intid
		,inserted.cobrador
		,inserted.rota
		,inserted.ccusto
		,inserted.ncusto
		,inserted.zona
		,inserted.segmento
		,inserted.tipo
		,CASE WHEN td.lancacli=0 then inserted.pais else ft2.c2pais end,case when td.lancacli=0 then inserted.estab else ft2.c2estab end
		,inserted.vendedor
		,inserted.vendnm
		,CASE WHEN inserted.cheque = 1 and td.tipodoc <> 3 then inserted.etotal else 0 end
		,inserted.evirs 
	FROM
		inserted 
		INNER JOIN [dbo].[td] (nolock) ON td.ndoc = inserted.ndoc 
		INNER JOIN [dbo].[ft2] ON ft2.ft2stamp = inserted.ftstamp 
	WHERE 
		td.lancacc = 1 
		AND inserted.multi = 0 

	if @@ROWCOUNT > 0
		SET @Msg += 'Insert CC (' + convert(varchar,getdate(),8) + ')>'
END

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg) ;