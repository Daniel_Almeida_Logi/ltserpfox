SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[Tr_Ft_Delete] 
ON [dbo].[ft] FOR DELETE
AS 

IF (@@ROWCOUNT = 0) 
	RETURN ;

SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(100) = ''
	,@nome varchar(20) = 'Tr_Ft_Delete'

SET @Msg += '<' + convert(varchar,getdate(),14) + '>'

DELETE
	CC
from
	deleted
where
	cc.ftstamp = deleted.ftstamp

if @@ROWCOUNT > 0
	SET @Msg += '<Delete CC (' + convert(varchar,getdate(),8) + ')>'

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg) ;