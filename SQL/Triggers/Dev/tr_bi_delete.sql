SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[tr_bi_delete] ON [dbo].[bi]
FOR DELETE
AS


-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_BI_Delete'
	,@MROW int = @@ROWCOUNT

IF (@@ROWCOUNT = 0)
	RETURN;

SET NOCOUNT ON

SET @Msg += '<' + convert(varchar,getdate(),14) + '>'

declare @site_nr tinyint
set @site_nr = isnull((select top 1 empresa_no from empresa_arm (nolock) inner join deleted d on empresa_arm.armazem = d.armazem where d.armazem != 0),1)
	
	
-- Eliminar movimento de stock
IF EXISTS (SELECT TOP 1 sl.slstamp FROM sl INNER JOIN deleted d ON d.ref <> '' AND sl.bistamp = d.bistamp AND d.bistamp <> '')
begin
	DELETE
		SL
	FROM
		deleted
	WHERE
		deleted.ref != '' 
		AND deleted.bistamp != ''
		and sl.bistamp = deleted.bistamp

	if @@ROWCOUNT > 0
		SET @Msg += '<Delete SL (' + convert(varchar,getdate(),8) + ')>'
end


/*
	eliminar quantidade regularizada de documento de origem
*/
IF exists (SELECT d.bistamp FROM deleted d WHERE d.obistamp != '' AND d.qtt <> 0)
BEGIN
		
	set Context_Info 0x55555 -- evitar que o trigger de update corra validações desnecessárias
	
	UPDATE
		BI
	SET
		bi.fno = 0
		,bi.adoc = ''
		,bi.nmdoc = ''
		,bi.ndoc = 0
		,bi.qtt2 = case when bi.qtt2 - (SELECT SUM(d.qtt) FROM deleted d WHERE d.obistamp = deleted.obistamp) < 0 then 0 else bi.qtt2 - (SELECT SUM(d.qtt) FROM deleted d WHERE d.obistamp = deleted.obistamp) end
		,bi.pbruto = bi.pbruto - (SELECT SUM(d.qtt) FROM deleted d WHERE d.obistamp = deleted.obistamp) -- campo utilizado como qtt2, mas para ser compativel com as compras que não têm qtt2
	FROM
		deleted
		--INNER JOIN ts (nolock) ON deleted.ndos = ts.ndos
	WHERE
		deleted.obistamp != ''
		and deleted.obistamp = bi.bistamp
		--and ts.stocks = 1

	if @@ROWCOUNT > 0
		SET @Msg += '<Update BI (origem) (' + convert(varchar,getdate(),8) + ')>'

	set Context_Info 0x0
END


IF @MROW = 1
BEGIN
		
	SET @Msg += '<SL (' + convert(varchar,getdate(),8) + ')>'

	/*
		Atualizar reservas de cliente
	*/
	IF EXISTS (SELECT d.marcada FROM deleted d WHERE d.ref != '' AND d.rescli = 1 AND d.fechada = 0)
	BEGIN
		
		set Context_Info 0x55556 -- evitar que o trigger de update corra validações desnecessárias

		UPDATE
			ST
		SET 
			st.qttcli = st.qttcli - (CASE 
										WHEN (deleted.qtt - deleted.qtt2) < 0 THEN 0
										ELSE (deleted.qtt - deleted.qtt2)
										END)
		FROM
			deleted
		WHERE
			deleted.rescli = 1 
			AND deleted.fechada = 0
			and st.ref = deleted.ref
			and st.site_nr = @site_nr

		if @@ROWCOUNT > 0
			SET @Msg += '<Update ST (qttcli) (' + convert(varchar,getdate(),8) + ')>'

		set Context_Info 0x0

		UPDATE
			SA
		SET
			sa.rescli = sa.rescli - (CASE 
										WHEN (deleted.qtt - deleted.qtt2) < 0 THEN 0
										ELSE (deleted.qtt - deleted.qtt2)
										END)
		FROM
			deleted
		WHERE
			deleted.rescli = 1 AND deleted.fechada = 0
			and sa.ref = deleted.ref and sa.armazem = deleted.armazem

		if @@ROWCOUNT > 0
			SET @Msg += '<Update SA (rescli) (' + convert(varchar,getdate(),8) + ')>'
	END


	/*
		Atualizar encomendado fornecedor
	*/
	IF EXISTS (SELECT deleted.marcada FROM deleted WHERE deleted.ref <> '' AND deleted.resfor = 1 AND deleted.fechada = 0)
	BEGIN

		set Context_Info 0x55556 -- evitar que o trigger de update corra validações desnecessárias

		UPDATE
			ST
		SET
			st.qttfor = st.qttfor - (CASE 
										WHEN (deleted.qtt - deleted.qtt2) < 0 THEN 0
										ELSE (deleted.qtt - deleted.qtt2)
										END)
		FROM
			deleted
		WHERE
			deleted.resfor = 1 
			AND deleted.fechada = 0
			and st.ref = deleted.ref
			and st.site_nr = @site_nr

		if @@ROWCOUNT > 0
			SET @Msg += '<Update ST (qttfor) (' + convert(varchar,getdate(),8) + ')>'

		set Context_Info 0x0

		UPDATE
			SA
		SET
			sa.resfor = sa.resfor - (CASE
										WHEN (deleted.qtt - deleted.qtt2) < 0 THEN 0
										ELSE (deleted.qtt - deleted.qtt2)
										END)
		FROM
			deleted
		WHERE
			deleted.resfor = 1 
			AND deleted.fechada = 0
			and sa.ref = deleted.ref 
			and sa.armazem = deleted.armazem

		if @@ROWCOUNT > 0
			SET @Msg += '<Update SA (resfor) (' + convert(varchar,getdate(),8) + ')>'
	END

END
ELSE BEGIN

	SET @Msg += '<ML (' + convert(varchar,getdate(),8) + ')>'

	-- Atualizar reservas de cliente
	IF EXISTS (SELECT d.marcada FROM deleted d WHERE d.ref <> '' AND d.rescli = 1 AND d.fechada = 0)
	BEGIN
		UPDATE
			SA
		SET
			sa.rescli = sa.rescli - isnull((SELECT SUM(CASE 
														WHEN qtt - qtt2 < 0 THEN 0
														ELSE qtt - qtt2
														END)
												FROM deleted
												WHERE deleted.fechada = 0 AND deleted.rescli = 1 and sa.ref=deleted.ref and deleted.armazem = sa.armazem)
										,0)
		WHERE
			sa.ref IN (SELECT deleted.ref FROM deleted
						WHERE deleted.fechada = 0 and deleted.rescli = 1 and sa.armazem = deleted.armazem)


		if @@ROWCOUNT > 0
			SET @Msg += '<Update SA (rescli) (' + convert(varchar,getdate(),8) + ')>'

		set Context_Info 0x55556 -- evitar que o trigger de update corra validações desnecessárias

		UPDATE
			st
		SET
			st.qttcli = st.qttcli - isnull((SELECT SUM(CASE 
															WHEN qtt - qtt2 < 0 THEN 0
															ELSE qtt - qtt2
															END)
												FROM deleted
												WHERE deleted.fechada = 0 AND deleted.rescli = 1 and st.ref=deleted.ref)
										,0)
		WHERE
			st.ref IN (SELECT deleted.ref FROM deleted WHERE deleted.fechada = 0)
			and st.site_nr = @site_nr

		if @@ROWCOUNT > 0
			SET @Msg += '<Update ST (qttcli) (' + convert(varchar,getdate(),8) + ')>'

		set Context_Info 0x0
	END

	/*
		Atualizar encomendado fornecedor
	*/
	IF EXISTS (SELECT d.marcada FROM deleted d WHERE d.ref <> '' AND d.resfor = 1 AND d.fechada = 0)
	BEGIN
		UPDATE
			SA
		SET
			sa.resfor = sa.resfor - isnull((SELECT SUM(CASE 
															WHEN qtt - qtt2 < 0 THEN 0
															ELSE qtt - qtt2
															END)
												FROM deleted
												WHERE deleted.fechada = 0 AND deleted.resfor = 1 and sa.ref=deleted.ref and deleted.armazem = sa.armazem)
										,0)
		WHERE
			sa.ref IN (SELECT deleted.ref FROM deleted WHERE deleted.fechada = 0 AND deleted.resfor = 1)

		if @@ROWCOUNT > 0
			SET @Msg += '<Update SA (resfor) (' + convert(varchar,getdate(),8) + ')>'

		set Context_Info 0x55556 -- evitar que o trigger de update corra validações desnecessárias

		UPDATE
			st
		SET
			st.qttfor = st.qttfor - isnull((SELECT SUM(CASE 
															WHEN qtt - qtt2 < 0 THEN 0
															ELSE qtt - qtt2
															END)
												FROM deleted
												WHERE deleted.fechada = 0 AND deleted.resfor = 1 and st.ref=deleted.ref)
										,0)
		WHERE
			st.ref IN (SELECT deleted.ref FROM deleted WHERE deleted.fechada = 0 AND deleted.resfor = 1)
			and st.site_nr = @site_nr
				
		if @@ROWCOUNT > 0
			SET @Msg += '<Update ST (qttfor) (' + convert(varchar,getdate(),8) + ')>'

		set Context_Info 0x0
	END
END

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg) ;


/*
	create table ##Debug_Trigger2 (nome varchar(200), mensagem varchar(500))
	select * from ##Debug_Trigger2
	delete ##debug_trigger2


select * from bo where ndos=6
begin transaction asd
delete bi where bostamp='ADM3B28512A-707A-4BE2-91E'
rollback tran asd
delete bi where bistamp in ('ADMBC55AC8B-8F14-4BF6-848','ADM12B668F1-28C0-4431-A46','ADM72E77A50-B3B8-4ABA-900')
select * from bi where bostamp='ADM3B28512A-707A-4BE2-91E'
*/