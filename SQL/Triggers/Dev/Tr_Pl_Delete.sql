
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TRIGGER [dbo].[tr_pl_delete] 
ON [dbo].[pl]  
FOR DELETE  
AS 

IF (@@ROWCOUNT = 0)
	RETURN;
	
SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE 
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Pl_Delete'	

SET @Msg += '<' + convert(varchar,getdate(),14) + '>'


update 
	FC
SET 
	fc.credfm = fc.credfm - x.credfm
	,fc.debfm = fc.debfm + x.debfm
	,fc.ecredf = fc.ecredf - x.ecredf
	,fc.edebf = fc.edebf + x.edebf
	,fc.evirsreg = fc.evirsreg - x.evirsreg
from (
	select
		credfm  = SUM(case when d.ecambio<>0 and d.rec>0 then d.rec else 0 end)
		,debfm = SUM(case when d.ecambio<>0 and  d.rec<0 then d.rec else 0 end)
		,ecredf = SUM(case when d.erec>0 then d.erec-d.earred else 0 end)
		,edebf = SUM(case when d.erec<0 then d.erec-d.earred else 0 end)
		,evirsreg = SUM(d.evirs)
		,fc.fcstamp
	from
		deleted d
		inner join fc on fc.fcstamp=d.fcstamp
	where
		d.process = 1
	group by
		fc.fcstamp
) x
	inner join fc on fc.fcstamp = x.fcstamp

--UPDATE
--	FC 
--SET 
--	fc.credfm=fc.credfm-(select SUM(case when deleted.ecambio<>0 and deleted.rec>0 then deleted.rec else 0 end) from deleted where deleted.fcstamp=fc.fcstamp)
--	,fc.debfm=fc.debfm+(select SUM(case when deleted.ecambio<>0 and  deleted.rec<0 then deleted.rec else 0 end) from deleted where deleted.fcstamp=fc.fcstamp)
--	,fc.ecredf=fc.ecredf-(select SUM(case when deleted.erec>0 then deleted.erec-deleted.earred else 0 end) from deleted where deleted.fcstamp=fc.fcstamp)
--	,fc.edebf=fc.edebf+(select SUM(case when deleted.erec<0 then deleted.erec-deleted.earred else 0 end) from deleted where deleted.fcstamp=fc.fcstamp)
--	,fc.evirsreg=fc.evirsreg-(select SUM(deleted.evirs) from deleted where deleted.fcstamp=fc.fcstamp) 
--from
--	deleted 
--where 
--	deleted.fcstamp=fc.fcstamp 
--	and deleted.process=1

if @@ROWCOUNT > 0
	SET @Msg += '<Update FC (' + convert(varchar,getdate(),8) + ')>'



update 
	FC
SET 
	fc.evalpo = fc.evalpo - x.evalpo
	,fc.mvalpo = fc.mvalpo - x.mvalpo
	,fc.ultdoc = ''
	,fc.recibado = 0 
from (
	select
		evalpo = SUM(deleted.erec)
		,mvalpo = SUM(deleted.rec)
		,fc.fcstamp
	from
		deleted
		inner join fc on fc.fcstamp=deleted.fcstamp
	group by
		fc.fcstamp
) x
	inner join fc on fc.fcstamp = x.fcstamp

--UPDATE
--	FC 
--set 
--	fc.evalpo=fc.evalpo-(select SUM(deleted.erec) from deleted where deleted.fcstamp=fc.fcstamp)
--	,fc.mvalpo=fc.mvalpo-(select SUM(deleted.rec) from deleted where deleted.fcstamp=fc.fcstamp)
--	,fc.ultdoc=''
--	,fc.recibado=0 
--from
--	deleted 
--where 
--	fc.fcstamp=deleted.fcstamp

if @@ROWCOUNT > 0
	SET @Msg += '<Update FC (' + convert(varchar,getdate(),8) + ')>'


UPDATE
	FC
set 
	fc.eirsdif = fc.eirsdif - 
					(select SUM(case 
								when d.desconto=100 or po.fin=100 then 0 
								else (ROUND((d.evirs/(1-(case when d.desconto<>0 then d.desconto else po.fin end/100))),2) - d.evirs) 
								end) 
					from deleted d 
					inner join po on po.postamp=d.postamp 
					where d.fcstamp=fc.fcstamp) 
from
	deleted 
	inner join po on po.postamp=deleted.postamp 
where 
	fc.fcstamp = deleted.fcstamp 
	and deleted.process = 1 
	and (deleted.desconto!=0 or po.fin!=0)

if @@ROWCOUNT > 0
	SET @Msg += '<Update FC (' + convert(varchar,getdate(),8) + ')>'

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg);	