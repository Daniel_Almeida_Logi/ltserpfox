SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[Tr_Fi_Delete] 
ON [dbo].[fi] FOR DELETE  
AS 

IF (@@ROWCOUNT = 0)
	RETURN;

SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE 
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Fi_Delete'	


set @Msg = '<' + convert(varchar,getdate(),14) + '>'

--update ft set numLinhas = (select COUNT(*) FROM fi(nolock) where fi.ftstamp = ft.ftstamp) FROM  ft(nolock) inner join deleted as i ON i.ftstamp = ft.ftstamp

/*
	Delete sl
*/
DELETE
	SL 
FROM
	deleted 
where 
	sl.fistamp=deleted.fistamp 
	AND deleted.fistamp != ''
	and deleted.ref != ''

if @@ROWCOUNT > 0
	SET @Msg += '<Delete SL (' + convert(varchar,getdate(),8) + ')>'


/*
	Update bi
*/
set Context_Info 0x55555 -- evitar que o trigger de update corra validações desnecessárias

UPDATE
	BI 
SET 
	bi.fno = 0
	,bi.adoc = ''
	,bi.nmdoc = ''
	,bi.oftstamp = ''
	,bi.ndoc = 0
	,bi.qtt2 = bi.qtt2 - (select SUM(d.qtt * (case when d.tipodoc=3 then -1 else 1 end)) from deleted d where d.bistamp = bi.bistamp) 
	,bi.pbruto = bi.pbruto - (select SUM(d.qtt * (case when d.tipodoc=3 then -1 else 1 end)) from deleted d where d.bistamp = bi.bistamp) 
from
	bi
	INNER JOIN deleted on deleted.bistamp = bi.bistamp 
where 
	deleted.bistamp != ''

if @@ROWCOUNT > 0
	SET @Msg += '<Update BI (origem) (' + convert(varchar,getdate(),8) + ')>'

set Context_Info 0x0


/*
	Update ft
*/
UPDATE
	FT 
SET 
	ft.facturada = 0
	,ft.nmdocft = ''
	,ft.fnoft = 0
FROM
	deleted 
WHERE 
	deleted.ndocft <> 0 
	AND deleted.ndocft = ft.ndoc 
	AND deleted.fnoft = ft.fno  
	AND deleted.ftanoft = ft.ftano 
	AND (
		NOT EXISTS (SELECT * FROM fi WHERE fi.fistamp<>deleted.fistamp AND fi.ndocft=deleted.ndocft AND fi.fnoft=deleted.fnoft AND fi.ftanoft=deleted.ftanoft AND fi.ftstamp=deleted.ftstamp) 
		OR NOT EXISTS (SELECT * FROM inserted WHERE inserted.ndocft=deleted.ndocft and inserted.fnoft=deleted.fnoft and inserted.ftanoft=deleted.ftanoft)
	)

if @@ROWCOUNT > 0
	SET @Msg += '<Update FT (' + convert(varchar,getdate(),8) + ')>'


UPDATE
	FT 
SET 
	ft.ecusto = ft.ecusto-case when deleted.tipodoc=3 then -deleted.qtt*deleted.ecusto else deleted.qtt*deleted.ecusto end 
FROM
	deleted 
WHERE
	deleted.ftstamp = ft.ftstamp
	AND deleted.composto = 0

if @@ROWCOUNT > 0
	SET @Msg += '<Update FT (ecusto) (' + convert(varchar,getdate(),8) + ')>'

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg);	


	