SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[tr_cm1_update] 
ON [dbo].[cm1]  
FOR UPDATE  
AS 

IF (@@ROWCOUNT = 0)
	RETURN;
	
SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Cm1_Update'


SET @Msg += '<' + convert(varchar,getdate(),14) + '>'

IF update (cmdesc) 
BEGIN 

	update
		cc 
	set 
		cc.cmdesc=inserted.cmdesc 
	from
		inserted 
	where 
		cc.cm = inserted.cm
	
	if @@ROWCOUNT > 0
		SET @Msg += '<Update CC (' + convert(varchar,getdate(),8) + ')>'


	update
		fc 
	set 
		fc.cmdesc=inserted.cmdesc 
	from
		inserted 
	where 
		fc.cm = inserted.cm
	
	if @@ROWCOUNT > 0
		SET @Msg += '<Update FC (' + convert(varchar,getdate(),8) + ')>'

	update
		td 
	set 
		td.cmccn=inserted.cmdesc 
	from
		inserted 
	where 
		td.cmcc = inserted.cm
	
	if @@ROWCOUNT > 0
		SET @Msg += '<Update TD (' + convert(varchar,getdate(),8) + ')>'

	update
		po
	set
		po.cmdesc=inserted.cmdesc
	from
		inserted
	where
		po.cm = inserted.cm

	if @@ROWCOUNT > 0
		SET @Msg += '<Update PO (' + convert(varchar,getdate(),8) + ')>'

	update
		fo
	set
		fo.docnome=inserted.cmdesc
	from
		inserted
	where
		fo.doccode = inserted.cm

	if @@ROWCOUNT > 0
		SET @Msg += '<Update FO (' + convert(varchar,getdate(),8) + ')>'
END

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg) ;