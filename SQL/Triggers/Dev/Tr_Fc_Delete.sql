
/****** Object:  Trigger [dbo].[tr_fc_delete]    Script Date: 18-11-2014 22:43:40 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER TRIGGER [dbo].[tr_fc_delete] 
ON [dbo].[fc]  
FOR DELETE  
AS 

IF (@@ROWCOUNT = 0)
	RETURN;
	
SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Fc_Delete'

set @Msg = '<' + convert(varchar,getdate(),14) + '>'

UPDATE 
	FL 
SET 
	fl.esaldo=fl.esaldo - 
				(select SUM(d.ecred - d.edeb + d.edifcambio) from deleted d where d.no=fl.no) 
where
	fl.no in (select no from deleted)
--from
--	deleted 
--where 
--	fl.no=deleted.no

if @@ROWCOUNT > 0
	SET @Msg += '<Update FL (' + convert(varchar,getdate(),8) + ')>'


IF UPDATE(no) or UPDATE(ecred) or UPDATE(edeb) or UPDATE(cr) or UPDATE(cradoc) or update(fcstamp)
begin

	if ((SELECT TRIGGER_NESTLEVEL()) <= 1) -- Fail safe à recursividade, por defeito deve estar 0 = não permite recursividade
	BEGIN
		UPDATE
			FC
		SET
			fc.ecredf = fc.ecredf-deleted.edeb
			,fc.edebf = fc.edebf-deleted.ecred
		from
			deleted
		where
			(
				fc.cm=deleted.cr 
				and fc.adoc=deleted.cradoc 
				and fc.no=deleted.no 
				and deleted.cr > 0 
				and ltrim(rtrim(deleted.cradoc)) != ''
			) 
			OR 
			(
				fc.fcstamp = substring(deleted.fcstamp,2,25) 
				AND substring(deleted.fcstamp,1,1) = 'R'
			)

		if @@ROWCOUNT > 0
			SET @Msg += '<Update FC (' + convert(varchar,getdate(),8) + ')>'
	end
end


-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg) ;