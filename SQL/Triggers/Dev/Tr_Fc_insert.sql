SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[tr_fc_insert] 
ON [dbo].[fc]  
FOR INSERT  
AS 

IF (@@ROWCOUNT = 0)
	RETURN;

SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Fc_Insert'


set @Msg = '<' + convert(varchar,getdate(),14) + '>'

--declare @m_banchab char(60), @m_contado float
--select @m_banchab = isnull((select textValue from B_Parameters where stamp = 'ADM0000000252'),'')/*(select valor from para1 where descricao='m_banchab')*/
--SELECT @m_contado = (SELECT TOP 1 noconta FROM bl WHERE bl.banco = SUBSTRING(@m_banchab, 1, 10) AND bl.conta = SUBSTRING(@m_banchab, 12, 20))

--if @m_contado = NULL
--	select @m_contado = 1

--if @m_banchab = NULL
--	select @m_banchab = ''

UPDATE
	FL 
SET
	fl.esaldo = fl.esaldo + 
					(select SUM(i.ecred - i.edeb + i.edifcambio) from inserted i where i.no=fl.no) 
where
	fl.no in (select no from inserted)
--from
--	INSERTED 
--where 
--	fl.no = inserted.no

if @@ROWCOUNT > 0
	SET @Msg += '<Update FL (' + convert(varchar,getdate(),8) + ')>'


if ((SELECT TRIGGER_NESTLEVEL()) <= 1) -- Fail safe à recursividade, por defeito deve estar 0 = não permite recursividade
BEGIN

	IF UPDATE(no) or UPDATE(ecred) or UPDATE(edeb) or UPDATE(cr) or UPDATE(cradoc) 
	begin
		UPDATE
			fc
		SET
			fc.ecredf = fc.ecredf+inserted.edeb
			,fc.edebf = fc.edebf+inserted.ecred
		from
			INSERTED
		where
			fc.cm=inserted.cr
			and fc.adoc=inserted.cradoc
			and fc.no=inserted.no
			and inserted.cr > 0
			and inserted.cradoc != ''

		if @@ROWCOUNT > 0
			SET @Msg += '<Update FC (' + convert(varchar,getdate(),8) + ')>'
	end


	if exists (select inserted.fcstamp from inserted where inserted.formapag = '')
	begin
		UPDATE
			fc 
		SET
			formapag='1' 
		from
			inserted
		WHERE 
			fc.fcstamp = inserted.fcstamp 
			AND fc.formapag = ''

		if @@ROWCOUNT > 0
			SET @Msg += '<Update FC (formapag) (' + convert(varchar,getdate(),8) + ')>'
	end


	if exists (select fcstamp from inserted where inserted.situacao = '')
	begin
		UPDATE
			fc
		SET
			situacao = case
						when inserted.cred<>inserted.credf OR inserted.deb<>inserted.debf OR inserted.ecred<>inserted.ecredf OR inserted.edeb<>inserted.edebf then '1'
						ELSE '2' 
						end
		from
			inserted
		WHERE
			fc.fcstamp = inserted.fcstamp AND fc.situacao = ''

		if @@ROWCOUNT > 0
			SET @Msg += '<Update FC (situacao) (' + convert(varchar,getdate(),8) + ')>'
	end

end

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg) ;