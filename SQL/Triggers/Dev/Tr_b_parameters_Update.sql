SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

alter TRIGGER [dbo].[ut_b_parameters_update]
ON  [dbo].[B_Parameters]
AFTER UPDATE
AS 

if @@ROWCOUNT = 0
	return;

SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(100) = ''
	,@nome varchar(20) = 'ut_b_parameters_upd'


SET @Msg += '<' + convert(varchar,getdate(),14) + ')>'

/* 
	Se nao ha linhas a actualizar, nem tentar fazer o INSERT senao os valores sao NULLOS 
*/
declare @old bit, @new bit
select @old = bool from deleted
select @new = bool from inserted

insert into b_ocorrencias
	(stamp,linkstamp,tipo,grau,descr,ovalor,dvalor,usr,date,terminal)
select
	stamp			= 'ADM'+left(replace(newid(),'-',''),23)
	,linkstamp		= i.stamp
	,tipo			= 'Seguranca'
	,Grau			= 1
	,descr			= 'Parâmetro [' + i.name + '] alterado; Utilizador: [' + suser_sname() + ']'
	,oValor			= @old
	,dValor			= @new
	,usr			= 1
	,date			= getdate()
	,terminal		= 1
from
	inserted i

if @@ROWCOUNT > 0
	SET @Msg += '<Insert B_OCORRENCIAS (' + convert(varchar,getdate(),8) + ')>'

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg);	


