
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[Tr_St_Delete] 
ON [dbo].[st] FOR DELETE  
AS

IF (@@ROWCOUNT = 0) 
	RETURN;

SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE 
	 @Msg varchar(100) = ''
	,@nome varchar(30) = 'Tr_St_Delete'	

SET @Msg += '<' + convert(varchar,getdate(),14) + '>'

/*
	delete bc
*/
DELETE
	BC
FROM
	deleted
WHERE
	bc.ststamp=deleted.ststamp

if (@@ROWCOUNT > 0)
	SET @Msg += '<Delete BC (' + convert(varchar,getdate(),8) + ')>'


/*
	delete st_inativos
*/
delete 
	st_inativos
from
	deleted
where
	st_inativos.ref = deleted.ref
	and st_inativos.site_nr = deleted.site_nr

if (@@ROWCOUNT > 0)
	SET @Msg += '<Delete ST_INATIVOS (' + convert(varchar,getdate(),8) + ')>'

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg);