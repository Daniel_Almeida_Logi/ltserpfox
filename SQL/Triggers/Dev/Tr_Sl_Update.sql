/****** Object:  Trigger [dbo].[tr_sl_update]    Script Date: 20/06/2023 11:29:08 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER TRIGGER [dbo].[tr_sl_update]
ON [dbo].[sl]
FOR UPDATE
AS
-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Sl_Update'
	,@usaLotes bit = 0
	,@MROW int = @@ROWCOUNT
IF (@@ROWCOUNT = 0)
	RETURN;
SET NOCOUNT ON
SET @Msg += '<' + convert(varchar,getdate(),14) + '>'
IF update (qtt) or update (ref) or update (evu) or update (cm) or update (lote) or update (cor) or update (tam) or update (datalc) or update (armazem)
BEGIN
	/*
		Criar na SA as referências que não existirem
	*/
	IF exists (select count(sa.ref) from inserted i left join sa on sa.ref=i.ref and sa.armazem=i.armazem where sa.ref is null)
	BEGIN
		INSERT INTO SA
			(sastamp, armazem, ref)
		select distinct 
			convert(char(5),armazem) + ref
			,armazem
			,ref
		FROM
			inserted
		WHERE
			NOT EXISTS (select ref from sa where sa.ref = inserted.ref and sa.armazem = inserted.armazem)
			and inserted.ref != ''
		if @@ROWCOUNT>0
			SET @Msg += '<I Insert SA (' + convert(varchar,getdate(),8) + ')>'
	END
	/*
		Criar na st_lotes as referências/lotes por armazem que não existirem
	*/
	IF exists (select lote from inserted where lote != '')
	begin
		set @usaLotes = 1
		if exists (select count(stl.ref) from inserted i left join st_lotes stl on stl.ref = i.ref and stl.lote = i.lote and stl.armazem = i.armazem where stl.ref is null)
		BEGIN
			insert into st_lotes (
				stamp
				,ref, lote, armazem--, site
				,ousrinis, usrinis
				,ousrdata, ousrhora
				,usrdata, usrhora
			)
			select distinct
				left(convert(varchar,i.armazem) + rtrim(i.ref) + i.lote,25)
				,i.ref, i.lote, i.armazem--, b_lojas.site
				,i.ousrinis, i.usrinis
				,ousrdata = convert(varchar,getdate(),112), ousrhora = convert(varchar,getdate(),108)
				,usrdata = convert(varchar,getdate(),112), usrhora = convert(varchar,getdate(),108)
			from
				inserted i
				--inner join b_lojas on b_lojas.armazem1 = i.armazem
			WHERE
				NOT EXISTS (select stl.ref, stl.lote, stl.armazem from st_lotes stl where stl.ref = i.ref and stl.lote = i.lote and stl.armazem = i.armazem)
			if @@ROWCOUNT > 0
				SET @Msg += '<I Insert ST_LOTES (' + convert(varchar,getdate(),8) + ')>'
		END
	END
	IF @MROW = 1
	BEGIN
		SET @Msg += '<SL (' + convert(varchar,getdate(),8) + ')>'
		/*
			atualiza stocks Alterado para ignorar a condição dos Documentos trf. de Armazém LL 25-05-2015
		*/
		set Context_Info 0x55556 -- evitar que o trigger de update corra validações desnecessárias
		UPDATE
			ST
		SET
			 st.stock	 = st.stock - case when deleted.cm < 50 then deleted.qtt else -deleted.qtt end
			,st.qttacin  = case when deleted.cm < 50 then -deleted.QTT+st.qttacin else st.qttacin end
			,st.qttacout = case when deleted.cm > 50 then -deleted.QTT+st.qttacout else st.qttacout end
			--,st.epcpond  = case
			--			-- se o stock <= 0 colocar o pcp = 0
			--			when (st.stock - case when deleted.cm < 50 then deleted.qtt else -deleted.qtt end) <= 0 then 0
			--			-- se for entrada
			--			-- Cada entrada afeta o PCP ( total valor / total quantidade = PCP)
			--			WHEN st.stock - deleted.qtt > 0 and deleted.cm < 50
			--				THEN (st.epcpond * (CASE WHEN st.stock > 0 THEN st.stock ELSE 0 END) - deleted.ett)
			--					/ ((case when st.stock > 0 then st.stock else 0 end) - deleted.qtt)
			--			-- se for saída
			--			else st.epcpond
			--			end
			 ,st.usrdata = getdate()
		    ,st.usrhora = CONVERT(CHAR(8),GETDATE(),108)
		FROM
			deleted
			inner join empresa_arm (nolock) on empresa_arm.armazem = deleted.armazem
			inner join st on st.ref = deleted.ref and st.site_nr = empresa_arm.empresa_no
		if @@ROWCOUNT > 0
			SET @Msg += '<D Update ST (' + convert(varchar,getdate(),8) + ')>'
		UPDATE
			ST
		SET
			st.stock = st.stock + case when inserted.cm < 50 then inserted.qtt else -inserted.qtt end
			,st.uintr = case when inserted.datalc >= st.uintr and inserted.cm < 50 then inserted.datalc else st.uintr end
			/* Alterado para considerar apenas saidas por documentos de facturação */
			,st.usaid = case when inserted.datalc >= st.usaid and inserted.qtt > 0 and inserted.origem='FT'
                             then inserted.datalc else st.usaid end
			,st.qttacout = case when inserted.cm>50 then inserted.QTT+st.qttacout else st.qttacout end
			,st.qttacin =  case when inserted.cm<50 then inserted.QTT+st.qttacin else st.qttacin end
			,st.udata   =  case when inserted.datalc >= st.udata then inserted.datalc else st.udata end
			,st.epcpond =  case when cm2.mudapcpond = 1 then 
							CASE
							-- se o stock <= 0 colocar o pcp = 0
							WHEN (st.stock + case when inserted.cm < 50 then inserted.qtt else -inserted.qtt end) <= 0 then 0
							-- se for entrada
							-- Cada entrada afeta o PCP ( (total existente + total entrada) / total qtt existente + entrada )
							WHEN inserted.qtt + st.stock > 0 and inserted.cm < 50
								THEN round((st.epcpond * (CASE WHEN st.stock > 0 THEN st.stock ELSE 0 END) + inserted.ett) / ((case when st.stock > 0 then st.stock else 0 end) + inserted.qtt),2)
							WHEN inserted.cm = 77 AND (st.stock + (-inserted.qtt)) > 0 
								THEN inserted.epcpond
							else st.epcpond
							end
						else
							st.epcpond
						end 
			, st.epcusto = case when cm2.mudaepcusto = 1 then 
								--case when inserted.fnstamp<>'' then (select (round(etiliquido/qtt,2)) from fn (nolock) where fn.fnstamp=inserted.fnstamp)
								case when inserted.fnstamp<>'' then (select epv from fn (nolock) where fn.fnstamp=inserted.fnstamp)
								else case when inserted.bistamp<>'' and inserted.cmdesc='Entrada p/trf'  then (select edebito from bi (nolock) where bi.bistamp=inserted.bistamp)
										else
											st.epcpond
										end 
								end
							else
								st.epcusto
							end 
			, st.epcult = case when cm2.mudapcult = 1 then 
								case when inserted.fnstamp<>'' then (select u_upc from fn (nolock) where fn.fnstamp=inserted.fnstamp)
								else case when inserted.bistamp<>'' and inserted.cmdesc='Entrada p/trf'  then (select edebito from bi (nolock) where bi.bistamp=inserted.bistamp)
										else
											st.epcult
										end 
								end
							else
								st.epcult
							end 
			,st.usrdata = getdate()
		    ,st.usrhora = CONVERT(CHAR(8),GETDATE(),108)
		from
			inserted
			inner join empresa_arm (nolock) on empresa_arm.armazem = inserted.armazem
			inner join st on st.ref = inserted.ref and st.site_nr = empresa_arm.empresa_no
			inner join cm2 (nolock) on cm2.cm=inserted.cm
		if @@ROWCOUNT>0
			SET @Msg += '<I Update ST (' + convert(varchar,getdate(),8) + ')>'
		set Context_Info 0x0
		/*
			Atualiza stocks por armazem
		*/
		UPDATE
			SA
		SET
			sa.stock = sa.stock + case when deleted.cm < 50 then -deleted.qtt else deleted.qtt end
			,sa.qttacin = case when deleted.cm < 50 then -deleted.QTT + sa.qttacin else sa.qttacin end
		FROM
			deleted
			inner join sa on sa.ref = deleted.ref
			and sa.armazem=deleted.armazem
		if @@ROWCOUNT>0
			SET @Msg += '<D Update SA (' + convert(varchar,getdate(),8) + ')>'
		UPDATE
			SA
		SET
			sa.stock = sa.stock + case when inserted.cm < 50 then inserted.qtt else -inserted.qtt end
			,sa.qttacin = case when inserted.cm < 50 then inserted.QTT + sa.qttacin else sa.qttacin end
		FROM
			INSERTED
			inner join sa on sa.ref = inserted.ref and sa.armazem = inserted.armazem
		if @@ROWCOUNT>0
			SET @Msg += '<I Update SA (' + convert(varchar,getdate(),8) + ')>'
		/*
			Atualiza stocks por lote
		*/
		IF @usaLotes = 1
		begin
			update
				st_lotes
			set
				stock = stl.stock - case when deleted.cm < 50 then deleted.qtt else -deleted.qtt end
			from
				deleted
				inner join st_lotes stl on stl.lote=deleted.lote and stl.ref=deleted.ref and stl.armazem=deleted.armazem
			if @@ROWCOUNT > 0
				SET @Msg += '<D Update ST_LOTES (' + convert(varchar,getdate(),8) + ')>'
			update
				st_lotes
			set
				stock = stl.stock + case when inserted.cm < 50 then inserted.qtt else -inserted.qtt end
			from
				inserted
				inner join st_lotes stl on stl.lote=inserted.lote and stl.ref=inserted.ref and stl.armazem=inserted.armazem
			if @@ROWCOUNT > 0
				SET @Msg += '<I Update ST_LOTES (' + convert(varchar,getdate(),8) + ')>'
		end
	END
	ELSE BEGIN
		SET @Msg += '<ML (' + convert(varchar,getdate(),8) + ')>'
		-- update campos afetados por multi linha - Alterado para ignorar a condição dos Documentos trf. de Armazém LL 25-05-2015
		set Context_Info 0x55556 -- evitar que o trigger de update corra validações desnecessárias
		UPDATE
			ST
		SET
			st.stock = st.stock - isnull((SELECT SUM(case when deleted.cm<50 then deleted.qtt else -deleted.qtt end) FROM deleted WHERE deleted.ref=st.ref and empresa_arm.armazem = deleted.armazem),0)
									--case
									--when (SELECT SUM(case when deleted.cm<50 then deleted.qtt else -deleted.qtt end) FROM deleted WHERE deleted.ref=st.ref and empresa_arm.armazem = deleted.armazem) is null then 0 
									--else (SELECT SUM(case when deleted.cm<50 then deleted.qtt else -deleted.qtt end) FROM deleted WHERE deleted.ref=st.ref and empresa_arm.armazem = deleted.armazem) 
									--end
			,st.qttacout = st.qttacout - isnull((SELECT SUM(case when deleted.cm<50 then 0 else deleted.qtt end) FROM deleted WHERE deleted.ref=st.ref and empresa_arm.armazem = deleted.armazem),0)
											--case
											--when (SELECT SUM(case when deleted.cm<50 then 0 else deleted.qtt end) FROM deleted WHERE deleted.ref=st.ref and empresa_arm.armazem = deleted.armazem) is null then 0
											--else (SELECT SUM(case when deleted.cm<50 then 0 else deleted.qtt end) FROM deleted WHERE deleted.ref=st.ref and empresa_arm.armazem = deleted.armazem)
											--end
			,st.qttacin = st.qttacin - isnull((SELECT SUM(case when deleted.cm<50 then deleted.qtt else 0 end) FROM deleted WHERE deleted.ref=st.ref and empresa_arm.armazem = deleted.armazem),0)
									--case
									--	when (SELECT SUM(case when deleted.cm<50 then deleted.qtt else 0 end) FROM deleted WHERE deleted.ref=st.ref and empresa_arm.armazem = deleted.armazem) is null then 0
									--	else (SELECT SUM(case when deleted.cm<50 then deleted.qtt else 0 end) FROM deleted WHERE deleted.ref=st.ref and empresa_arm.armazem = deleted.armazem)
									--	end
			--,st.epcpond = case
			--				when (SELECT SUM(case when deleted.cm<50 then deleted.qtt else -deleted.qtt end) FROM deleted WHERE deleted.ref=st.ref ) is null then st.epcpond
			--				-- se o stock <= 0 colocar o pcp = 0
			--				when (st.stock - (SELECT SUM(case when deleted.cm<50 then deleted.qtt else -deleted.qtt end) FROM deleted WHERE deleted.ref=st.ref)) <= 0 then 0
			--				-- se exitir entradas
			--				-- Cada entrada afeta o PCP (total valor / total quantidade = PCP)
			--				when st.stock - (SELECT SUM(deleted.qtt) FROM deleted WHERE deleted.ref=st.ref and deleted.cm < 50 and empresa_arm.armazem = deleted.armazem) > 0
			--					then round((st.epcpond * (CASE WHEN st.stock > 0 THEN st.stock ELSE 0 END) - (SELECT SUM(deleted.ett)  / sum(deleted.qtt) FROM deleted WHERE deleted.ref=st.ref and deleted.cm < 50 and empresa_arm.armazem = deleted.armazem) * (SELECT SUM(deleted.qtt) FROM deleted WHERE deleted.ref=st.ref and deleted.cm < 50 and empresa_arm.armazem = deleted.armazem))
			--						/ ((case when st.stock > 0 then st.stock else 0 end) - (SELECT SUM(deleted.qtt) FROM deleted WHERE deleted.ref=st.ref and deleted.cm < 50 and empresa_arm.armazem = deleted.armazem )),2)
			--				-- se só existir saídas
			--				else st.epcpond
			--				end
			,st.usrdata = getdate()
		    ,st.usrhora = CONVERT(CHAR(8),GETDATE(),108)
		FROM 
			deleted
			inner join empresa_arm (nolock) on empresa_arm.armazem = deleted.armazem
			inner join st on st.ref = deleted.ref and st.site_nr = empresa_arm.empresa_no
		if @@ROWCOUNT>0
			SET @Msg += '<D Update ST (' + convert(varchar,getdate(),8) + ')>'
		UPDATE
			ST
		SET
			st.stock = st.stock + isnull((SELECT SUM(case when inserted.cm<50 then inserted.qtt else -inserted.qtt end) FROM inserted WHERE inserted.ref=st.ref and empresa_arm.armazem = inserted.armazem),0)
									--case
									--when (SELECT SUM(case when inserted.cm<50 then inserted.qtt else -inserted.qtt end) FROM inserted WHERE inserted.ref=st.ref and empresa_arm.armazem = inserted.armazem) is null then 0 
									--else (SELECT SUM(case when inserted.cm<50 then inserted.qtt else -inserted.qtt end) FROM inserted WHERE inserted.ref=st.ref and empresa_arm.armazem = inserted.armazem) 
									--end
			,st.qttacout = st.qttacout + isnull((SELECT SUM(case when inserted.cm<50 then 0 else inserted.qtt end) FROM inserted WHERE inserted.ref=st.ref and empresa_arm.armazem = inserted.armazem),0)
											--case
											--when (SELECT SUM(case when inserted.cm<50 then 0 else inserted.qtt end) FROM inserted WHERE inserted.ref=st.ref and empresa_arm.armazem = inserted.armazem) is null then 0
											--else (SELECT SUM(case when inserted.cm<50 then 0 else inserted.qtt end) FROM inserted WHERE inserted.ref=st.ref and empresa_arm.armazem = inserted.armazem)
											--end
			,st.qttacin = st.qttacin + isnull((SELECT SUM(case when inserted.cm<50 then inserted.qtt else 0 end) FROM inserted WHERE inserted.ref=st.ref and empresa_arm.armazem = inserted.armazem),0)
										--case
										--when (SELECT SUM(case when inserted.cm<50 then inserted.qtt else 0 end) FROM inserted WHERE inserted.ref=st.ref and empresa_arm.armazem = inserted.armazem) is null then 0
										--else (SELECT SUM(case when inserted.cm<50 then inserted.qtt else 0 end) FROM inserted WHERE inserted.ref=st.ref and empresa_arm.armazem = inserted.armazem)
										--end
			,st.epcpond = case when cm2.mudapcpond = 1 then 
							CASE
							-- se o stock <= 0 colocar o pcp = 0
							WHEN (st.stock + case when inserted.cm < 50 then inserted.qtt else -inserted.qtt end) <= 0 then 0
							-- se for entrada
							-- Cada entrada afeta o PCP ( (total existente + total entrada) / total qtt existente + entrada )
							WHEN inserted.qtt + st.stock > 0 and inserted.cm < 50
								THEN round((st.epcpond * (CASE WHEN st.stock > 0 THEN st.stock ELSE 0 END) + inserted.ett) / ((case when st.stock > 0 then st.stock else 0 end) + inserted.qtt),2)
							WHEN inserted.cm = 77 AND (st.stock + (-inserted.qtt)) > 0 
								THEN inserted.epcpond
							else st.epcpond
							end
						else
							st.epcpond
						end 
			, st.epcusto = case when cm2.mudaepcusto = 1 then 
								--case when inserted.fnstamp<>'' then (select (round(etiliquido/qtt,2)) from fn (nolock) where fn.fnstamp=inserted.fnstamp)
								case when inserted.fnstamp<>'' then (select epv from fn (nolock) where fn.fnstamp=inserted.fnstamp)
								else case when inserted.bistamp<>'' then (select edebito from bi (nolock) where bi.bistamp=inserted.bistamp)
										else
											st.epcpond
										end 
								end
							else
								st.epcusto
							end 
			, st.epcult = case when cm2.mudapcult = 1 then 
								case when inserted.fnstamp<>'' then (select u_upc from fn (nolock) where fn.fnstamp=inserted.fnstamp)
								else case when inserted.bistamp<>'' then (select edebito from bi (nolock) where bi.bistamp=inserted.bistamp)
										else
											st.epcult
										end 
								end
							else
								st.epcult
							end 
			,st.usrdata = getdate()
		    ,st.usrhora = CONVERT(CHAR(8),GETDATE(),108)
		FROM 
			inserted
			inner join empresa_arm (nolock) on empresa_arm.armazem = inserted.armazem
			inner join st on st.ref = inserted.ref and st.site_nr = empresa_arm.empresa_no
			inner join cm2 (nolock) on cm2.cm=inserted.cm
		if @@ROWCOUNT>0
			SET @Msg += '<I Update ST (' + convert(varchar,getdate(),8) + ')>'
		-- update campos não afetados por multi linha
		UPDATE
			ST
		SET
			st.uintr = case when inserted.datalc >= st.uintr and inserted.cm < 50 then inserted.datalc else st.uintr end
			/* Alterado para considerar apenas saidas por documentos de facturação */
			,st.usaid = case when inserted.datalc >= st.usaid and inserted.qtt > 0 and inserted.origem='FT'
                             then inserted.datalc else st.usaid end
			,st.udata = case when inserted.datalc >= st.udata then inserted.datalc else st.udata end
		FROM 
			inserted
			inner join empresa_arm (nolock) on empresa_arm.armazem = inserted.armazem
			inner join st on st.ref = inserted.ref and st.site_nr = empresa_arm.empresa_no
		if @@ROWCOUNT > 0
			SET @Msg += '<I Update ST (' + convert(varchar,getdate(),8) + ')>'
		set Context_Info 0x0
		/*
			update campos afetados por multi linha
		*/
		UPDATE
			SA
		SET
			sa.stock = sa.stock - isnull((SELECT SUM(case when d.cm<50 then d.qtt else -d.qtt end) FROM deleted d WHERE d.ref=sa.ref and d.armazem=sa.armazem),0)
									--case
									--when (SELECT SUM(case when d.cm<50 then d.qtt else -d.qtt end) FROM deleted d WHERE d.ref=sa.ref and d.armazem=sa.armazem) is null then 0
									--else (SELECT SUM(case when d.cm<50 then d.qtt else -d.qtt end) FROM deleted d WHERE d.ref=sa.ref and d.armazem=sa.armazem )
									--end
			,sa.qttacin = sa.qttacin - isnull((SELECT SUM(case when d.cm<50 then d.qtt else 0 end) FROM deleted d WHERE d.ref=sa.ref and d.armazem=sa.armazem ),0)
										--case
										--when (SELECT SUM(case when d.cm<50 then d.qtt else 0 end) FROM deleted d WHERE d.ref=sa.ref and d.armazem=sa.armazem ) is null then 0
										--else (SELECT SUM(case when d.cm<50 then d.qtt else 0 end) FROM deleted d WHERE d.ref=sa.ref and d.armazem=sa.armazem )
										--end
		from
			deleted 
			inner join sa on deleted.ref=sa.ref and deleted.armazem=sa.armazem
		if @@ROWCOUNT>0
			SET @Msg += '<D Update SA (' + convert(varchar,getdate(),8) + ')>'
		UPDATE
			SA
		SET
			sa.stock = sa.stock + isnull((SELECT SUM(case when inserted.cm<50 then inserted.qtt else -inserted.qtt end) FROM inserted WHERE inserted.ref=sa.ref and inserted.armazem=sa.armazem ),0)
									--case
									--when (SELECT SUM(case when inserted.cm<50 then inserted.qtt else -inserted.qtt end) FROM inserted WHERE inserted.ref=sa.ref and inserted.armazem=sa.armazem) is null then 0
									--else (SELECT SUM(case when inserted.cm<50 then inserted.qtt else -inserted.qtt end) FROM inserted WHERE inserted.ref=sa.ref and inserted.armazem=sa.armazem )
									--end
			,sa.qttacin = sa.qttacin + isnull((SELECT SUM(case when inserted.cm<50 then inserted.qtt else 0 end) FROM inserted WHERE inserted.ref=sa.ref and inserted.armazem=sa.armazem ),0)
										--case
										--when (SELECT SUM(case when inserted.cm<50 then inserted.qtt else 0 end) FROM inserted WHERE inserted.ref=sa.ref and inserted.armazem=sa.armazem ) is null then 0
										--else (SELECT SUM(case when inserted.cm<50 then inserted.qtt else 0 end) FROM inserted WHERE inserted.ref=sa.ref and inserted.armazem=sa.armazem )
										--end
		from
			inserted
			INNER JOIN sa on inserted.ref = sa.ref 
			AND inserted.armazem = sa.armazem
		if @@ROWCOUNT>0
			SET @Msg += '<(I) Update SA (' + convert(varchar,getdate(),8) + ')>'
		/*
			Atualiza stocks por lote
		*/
		IF exists (select lote from deleted where lote != '') -- se usa lotes
		begin
			update
				st_lotes
			set
				stock = stock - isnull((SELECT SUM(case when d.cm<50 then d.qtt else -d.qtt end) FROM deleted d WHERE d.ref=stl.ref and d.armazem=stl.armazem and d.lote=stl.lote),0)
									--case
									--when (SELECT SUM(case when d.cm<50 then d.qtt else -d.qtt end) FROM deleted d WHERE d.ref=stl.ref and d.armazem=stl.armazem and d.lote=stl.lote) is null then 0 
									--else (SELECT SUM(case when d.cm<50 then d.qtt else -d.qtt end) FROM deleted d WHERE d.ref=stl.ref and d.armazem=stl.armazem and d.lote=stl.lote) 
									--end
			from
				deleted
				inner join st_lotes stl on stl.lote=deleted.lote and stl.ref=deleted.ref and stl.armazem=deleted.armazem
			if @@ROWCOUNT > 0
				SET @Msg += '<(D) Update ST_LOTES (' + convert(varchar,getdate(),8) + ')>'
			update
				st_lotes
			set
				stock = stock + isnull((SELECT SUM(case when i.cm<50 then i.qtt else -i.qtt end) FROM inserted i WHERE i.ref=stl.ref and i.armazem=stl.armazem and i.lote=stl.lote) ,0)
								--case
								--when (SELECT SUM(case when i.cm<50 then i.qtt else -i.qtt end) FROM inserted i WHERE i.ref=stl.ref and i.armazem=stl.armazem and i.lote=stl.lote) is null then 0 
								--else (SELECT SUM(case when i.cm<50 then i.qtt else -i.qtt end) FROM inserted i WHERE i.ref=stl.ref and i.armazem=stl.armazem and i.lote=stl.lote) 
								--end
			from
				inserted
				inner join st_lotes stl on stl.lote=inserted.lote and stl.ref=inserted.ref and stl.armazem=inserted.armazem
			if @@ROWCOUNT > 0
				SET @Msg += '<I Update ST_LOTES (' + convert(varchar,getdate(),8) + ')>'
		end
	END 
END
-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg);
