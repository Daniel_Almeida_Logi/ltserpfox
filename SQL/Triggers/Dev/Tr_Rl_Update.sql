
/****** Object:  Trigger [dbo].[tr_rl_update]    Script Date: 21/01/2025 15:36:49 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[tr_rl_update]
ON [dbo].[rl]
FOR UPDATE
AS

IF (@@ROWCOUNT = 0)
	RETURN;
	
SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Rl_Update'

SET @Msg += '<' + convert(varchar,getdate(),14) + '>'

UPDATE
	CC
SET
	cc.debfm = cc.debfm - case when d.ecambio<>0 and d.rec>0 then d.rec else 0 end
	,cc.credfm = cc.credfm + case when d.ecambio<>0 and d.rec<0 then d.rec else 0 end
	,cc.edebf = cc.edebf - case when d.erec>0 then d.erec-d.earred else 0 end
	,cc.ecredf = cc.ecredf + case when d.erec<0 then d.erec-d.earred else 0 end
	,cc.evirsreg = cc.evirsreg - d.evirs
from
	deleted d
where
	d.ccstamp=cc.ccstamp
	and d.process=1

if @@ROWCOUNT>0
	SET @Msg += '<D Update CC (' + convert(varchar,getdate(),8) + ')>'

UPDATE
	CC 
set 
	cc.ultdoc=''
	,cc.recibado=0
	,cc.recino=0
	,cc.recian=0
	,cc.evalre=cc.evalre-d.erec
	,cc.mvalre=cc.mvalre-d.rec
	,cc.evalch=cc.evalch-case when d.cheque=1 then d.erec else 0 end
	,cc.cbbno = 0 
from
	deleted d
where 
	cc.ccstamp=d.ccstamp

if @@ROWCOUNT > 0
	SET @Msg += '<Update CC (' + convert(varchar,getdate(),8) + ')>'


UPDATE
	CC 
set 
	cc.eirsdif = cc.eirsdif - case 
								when d.desconto=100 or re.fin=100 then 0 
								else (ROUND((d.evirs/(1-(case when d.desconto<>0 then d.desconto else re.fin end/100))),2)-d.evirs) 
								end 
from
	deleted d
	inner join re (nolock) on re.restamp=d.restamp 
where 
	cc.ccstamp=d.ccstamp 
	and d.process=1 
	and (d.desconto<>0 or re.fin<>0)

if @@ROWCOUNT>0
	SET @Msg += '<Update CC (' + convert(varchar,getdate(),8) + ')>'


UPDATE
	CC 
SET 
	cc.debfm=cc.debfm+case when i.ecambio<>0 and i.rec>0 then i.rec else 0 end
	,cc.credfm=cc.credfm-case when i.ecambio<>0 and i.rec<0 then i.rec else 0 end
	,cc.edebf=cc.edebf+case when i.erec>0 then i.erec-i.earred else 0 end
	,cc.ecredf=cc.ecredf-case when i.erec<0 then i.erec-i.earred else 0 end
	,cc.virsreg=cc.virsreg+i.virs,cc.evirsreg=cc.evirsreg+i.evirs 
from
	inserted i
where 
	i.ccstamp=cc.ccstamp 
	and i.process=1
	
if @@ROWCOUNT>0
	SET @Msg += '<I Update CC (' + convert(varchar,getdate(),8) + ')>'


UPDATE
	CC 
set 
	cc.ultdoc='Recibo nº '+convert(char(10),i.rno)+' de '+convert(char(10),i.rdata,104)
	,cc.recibado=1
	,cc.recino=i.rno
	,cc.recian=datepart(yy,i.rdata)
	,cc.evalre=cc.evalre+i.erec
	,cc.mvalre=cc.mvalre+i.rec
	,cc.evalch=cc.evalch+case when re.cheque=1 then i.erec else 0 end
	,cc.cbbno = re.cbbno 
from
	inserted i
	inner join re (nolock) on re.restamp=i.restamp 
where 
	cc.ccstamp=i.ccstamp

if @@ROWCOUNT>0
	SET @Msg += '<I Update CC (' + convert(varchar,getdate(),8) + ')>'

UPDATE
	CC 
set 
	cc.eirsdif = cc.eirsdif + case 
								when i.desconto=100 or re.fin=100 then 0 
								else (ROUND((i.evirs/(1-(case when i.desconto<>0 then i.desconto else re.fin end/100))),2) - i.evirs) 
								end 
from
	inserted i
	inner join re (nolock) on re.restamp=i.restamp 
where 
	cc.ccstamp=i.ccstamp 
	and i.process=1 
	and (i.desconto<>0 or re.fin<>0)

if @@ROWCOUNT>0
	SET @Msg += '<I Update CC (' + convert(varchar,getdate(),8) + ')>'

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg);