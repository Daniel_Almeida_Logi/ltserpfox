SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[tr_fn_update]
ON [dbo].[fn]
FOR UPDATE
AS

IF (@@ROWCOUNT = 0 or isnull(Context_Info(),0x0) = 0x55557)
	RETURN;

SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Fn_Update'


set @Msg = '<' + convert(varchar,getdate(),14) + '>'


DELETE
	SL
FROM
	inserted
	inner join fo on fo.fostamp=inserted.fostamp
	inner join cm1 (nolock) on fo.doccode=cm1.cm
where
	sl.fnstamp = inserted.fnstamp
	and inserted.fnstamp != ''
	and inserted.ref = ''

if @@ROWCOUNT > 0
	SET @Msg += '<Delete SL (' + convert(varchar,getdate(),8) + ')>'



if	update(fnstamp) or update(usrinis) or update(usrhora) or 
	update(usrdata) or update(ousrhora) or update(ousrinis) or 
	update(ousrdata) or update(codigo) or update(design) or 
	update(ref) or update(lote) or update(armazem) or 
	update(qtt) or update(esltt) or update(eslvu) or update(composto) or 
	update(cor) or update(tam) or update(usr1) or update(usr2) or 
	update(usr3) or update(usr4) or update(slvumoeda) or 
	update(cpoc) or update(ivaincl) or update(iva) or update(etiliquido) or update(fostamp) or 
	update(adoc) or update(data) or update(fnfref) or update(fnccusto) or update(fnncusto) or 
	update(fnstamp) or update(fnfref) or update(docnome) or update(stns)
BEGIN
	UPDATE
		SL 
	set 
		sl.usrdata=inserted.usrdata,sl.usrhora=inserted.usrhora,sl.usrinis=inserted.usrinis
		,sl.fref=fo.fref,sl.ccusto=inserted.fnccusto,sl.ncusto=fo.ncusto
		,sl.nome=fo.nome,sl.codigo=inserted.codigo,sl.moeda=fo.moeda,sl.datalc=(case when inserted.svdata > '19000101' then inserted.svdata else fo.data end),sl.design=inserted.design
		,sl.ref=inserted.ref,sl.lote=inserted.lote,sl.armazem=inserted.armazem,sl.qtt=inserted.qtt
		,sl.num1=inserted.num1
		,sl.ett=inserted.esltt
		,sl.evu=inserted.eslvu
		,sl.frcl=fo.no,sl.adoc=fo.adoc,sl.cm=cm1.fosl,sl.cmdesc=cm1.fosln,sl.composto=inserted.composto,sl.cor=inserted.cor,sl.tam=inserted.tam
		,sl.usr1=inserted.usr1,sl.usr2=inserted.usr2,sl.usr3=inserted.usr3,sl.usr4=inserted.usr4
		,sl.vumoeda=inserted.slvumoeda
		,sl.ttmoeda=inserted.slvumoeda*inserted.qtt
		,sl.pcpond=inserted.slvu,sl.cpoc=inserted.cpoc
		,sl.valiva=case when inserted.ivaincl=1 then inserted.tiliquido-inserted.tiliquido/(1+inserted.iva/100) else inserted.tiliquido*inserted.iva/100 end
		,sl.evaliva=case when inserted.ivaincl=1 then inserted.etiliquido-inserted.etiliquido/(1+inserted.iva/100) else inserted.etiliquido*inserted.iva/100 end
		,sl.epcpond=inserted.eslvu,sl.stns=inserted.stns 
	from
		inserted 
		inner join fo on inserted.fostamp=fo.fostamp 
		inner join cm1 (nolock) on fo.doccode=cm1.cm  
	where 
		inserted.fnstamp = sl.fnstamp 
		and cm1.folansl = 1  
		and inserted.qtt != 0
		
	if @@ROWCOUNT > 0
		SET @Msg += '<Update SL (' + convert(varchar,getdate(),8) + ')>'
END



if	update(fnstamp) or update(usrinis) or update(usrhora) or 
	update(usrdata) or update(ousrhora) or update(ousrinis) or 
	update(ousrdata) or update(codigo) or update(design) or 
	update(ref) or update(lote) or update(armazem) or 
	update(qtt) or update(esltt) or update(eslvu) or update(composto) or 
	update(cor) or update(tam) or update(usr1) or update(usr2) or 
	update(usr3) or update(usr4) or update(slvumoeda) or 
	update(cpoc) or update(ivaincl) or update(iva) or update(etiliquido) or update(fostamp) or 
	update(adoc) or update(data) or update(fnfref) or update(fnccusto) or update(fnncusto) or 
	update(fnstamp) or update(fnfref) or update(docnome) or update(stns)
BEGIN

	INSERT INTO SL (
		slstamp,fnstamp,usrinis,usrhora,usrdata,ousrinis,ousrhora,ousrdata,origem
		,fref,ccusto,ncusto,nome,codigo,moeda,datalc,design
		,ref,lote,armazem,qtt
		,ett
		,evu
		,num1
		,frcl,adoc,cm,cmdesc,composto,cor,tam,usr1,usr2,usr3,usr4
		,vumoeda
		,ttmoeda
		,pcpond,cpoc
		,valiva,evaliva,epcpond,stns)
	select 
		inserted.fnstamp,inserted.fnstamp,inserted.usrinis,inserted.usrhora,inserted.usrdata,inserted.ousrinis,inserted.ousrhora,inserted.ousrdata,'FO'
		,fo.fref,inserted.fnccusto,fo.ncusto
		,fo.nome,inserted.codigo,fo.moeda,(case when inserted.svdata > '19000101' then inserted.svdata else fo.data end),inserted.design
		,inserted.ref,inserted.lote,inserted.armazem,inserted.qtt
		,inserted.esltt
		,inserted.eslvu
		,inserted.num1
		,fo.no,fo.adoc,cm1.fosl,cm1.fosln,inserted.composto,inserted.cor,inserted.tam,inserted.usr1,inserted.usr2,inserted.usr3,inserted.usr4
		,inserted.slvumoeda
		,inserted.slvumoeda*inserted.qtt
		,inserted.slvu,inserted.cpoc
		,case when inserted.ivaincl=1 then inserted.tiliquido-inserted.tiliquido/(1+inserted.iva/100) else inserted.tiliquido*inserted.iva/100 end
		,case when inserted.ivaincl=1 then inserted.etiliquido-inserted.etiliquido/(1+inserted.iva/100) else inserted.etiliquido*inserted.iva/100 end
		,inserted.eslvu,inserted.stns
	from
		inserted
		inner join fo on fo.fostamp = inserted.fostamp
		inner join cm1 (nolock) on fo.doccode = cm1.cm
		inner join fo2 on fo.fostamp = fo2.fo2stamp and fo2.anulado = 0
	where
		not EXISTS (select slstamp from sl where sl.fnstamp = inserted.fnstamp)
		and cm1.folansl = 1
		and inserted.ref != ''
		and inserted.qtt != 0

	if @@ROWCOUNT > 0
		SET @Msg += '<Insert SL (' + convert(varchar,getdate(),8) + ')>'

END


if update(fostamp) or update(bistamp)  or update(qtt) or update(data) 
BEGIN	
	
	If exists (select d.bistamp from deleted d where d.bistamp != '' and d.qtt != 0)
	Begin
		set Context_Info 0x55555 -- evitar que o trigger de update corra validações desnecessárias	

		UPDATE
			BI
		SET
			bi.adoc = ''
			,bi.nmdoc = ''
			,bi.ofostamp = ''
			,bi.ndoc = 0
			,bi.qtt2 = bi.qtt2 - (select SUM(d.qtt) from deleted d where d.bistamp = bi.bistamp)
			,bi.pbruto = bi.pbruto - (select SUM(d.qtt) from deleted d where d.bistamp = bi.bistamp) -- campo utilizado como qtt2, mas para ser compativel com as compras que não têm qtt2
		from
			deleted
		where
			deleted.bistamp = bi.bistamp

		if @@ROWCOUNT > 0
			SET @Msg += '<D Update BI (' + convert(varchar,getdate(),8) + ')>'

		set Context_Info 0x0
	END


	If exists (select i.bistamp from inserted i where i.bistamp != '' and i.qtt != 0)
	Begin	

		set Context_Info 0x55555 -- evitar que o trigger de update corra validações desnecessárias	

		UPDATE
			BI
		SET
			bi.fdata = fo.data
			,bi.adoc = fo.adoc
			,bi.nmdoc = fo.docnome
			,bi.ofostamp = fo.fostamp
			,bi.ndoc = fo.doccode
			,bi.qtt2 = bi.qtt2 + (select SUM(i.qtt) from inserted i where i.bistamp = bi.bistamp)
			,bi.pbruto = bi.pbruto + (select SUM(i.qtt) from inserted i where i.bistamp = bi.bistamp) -- campo utilizado como qtt2, mas para ser compativel com as compras que não têm qtt2
		from
			inserted
			inner join fo on inserted.fostamp = fo.fostamp
		where
			inserted.bistamp = bi.bistamp

		if @@ROWCOUNT > 0
			SET @Msg += '<I Update BI (' + convert(varchar,getdate(),8) + ')>'

		set Context_Info 0x0
	END


	If exists (select d.fnstamp from deleted d where d.ofnstamp != '' and d.qtt != 0)
	Begin
		set Context_Info 0x55557 -- evitar que o trigger de update corra validações desnecessárias	

		UPDATE
			FN
		SET
			fn.pbruto = fn.pbruto - (select SUM(d.qtt) from deleted d where d.ofnstamp = fn.fnstamp) -- campo utilizado como qtt2, mas para ser compativel com as compras que não têm qtt2
		from
			deleted
		where
			deleted.ofnstamp != ''
			and deleted.ofnstamp = fn.fnstamp

		if @@ROWCOUNT > 0
			SET @Msg += '<D Update FN (pbruto) (' + convert(varchar,getdate(),8) + ')>'

		set Context_Info 0x0
	END


	If exists (select i.fnstamp from inserted i where i.ofnstamp != '' and i.qtt != 0)
	Begin	

		set Context_Info 0x55557 -- evitar que o trigger de update corra validações desnecessárias	

		UPDATE
			FN
		SET
			fn.pbruto = fn.pbruto + (select SUM(i.qtt) from inserted i where i.ofnstamp = fn.fnstamp) -- campo utilizado como qtt2, mas para ser compativel com as compras que não têm qtt2
		from
			inserted
		where
			inserted.ofnstamp != ''
			and inserted.ofnstamp = fn.fnstamp

		if @@ROWCOUNT > 0
			SET @Msg += '<I Update BI (pbruto) (' + convert(varchar,getdate(),8) + ')>'

		set Context_Info 0x0
	END
END


-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg);	
	
	