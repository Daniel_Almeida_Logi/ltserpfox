
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[tr_cc_insert] 
ON [dbo].[cc] 
FOR INSERT 
AS 

IF (@@ROWCOUNT = 0)
	RETURN;

SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Cc_Insert'


set @Msg = '<' + convert(varchar,getdate(),14) + '>'

--declare @m_banchab char(60), @m_contado float
--select @m_banchab = isnull((select textValue from B_Parameters where stamp = 'ADM0000000252'),'') /*'m_banchab'*/
--SELECT @m_contado = (SELECT TOP 1 noconta FROM bl WHERE bl.banco = SUBSTRING(@m_banchab, 1, 10) AND bl.conta = SUBSTRING(@m_banchab, 12, 20))

--if @m_contado = NULL
--	select @m_contado = 0

--if @m_banchab = NULL
	--select @m_banchab = ''

UPDATE
	b_utentes 
SET
	b_utentes.esaldo = b_utentes.esaldo + (select 
												Sum(inserted.edeb-inserted.ecred) 
											from
												inserted 
											where 
												inserted.no = b_utentes.no and inserted.estab = b_utentes.estab)
from
	INSERTED
where
	b_utentes.no = inserted.no and b_utentes.estab = inserted.estab

if @@ROWCOUNT > 0
	SET @Msg += '<Update B_UTENTES (' + convert(varchar,getdate(),8) + ')>'


IF (select count(*) from inserted where inserted.cr<>0 and inserted.docref<>0 and inserted.origem='CC') > 0
begin

	set Context_Info 0x55555 -- evitar que o trigger de update corra

	UPDATE
		CC
	SET
		cc.edebf = cc.edebf+inserted.ecred
		,cc.ecredf = cc.ecredf+inserted.edeb
	from
		INSERTED
	where
		cc.cm=inserted.cr
		and cc.nrdoc=inserted.docref
		and cc.no=inserted.no
		and inserted.origem='CC'
		and cc.ccstamp = inserted.occstamp

	if @@ROWCOUNT > 0
		SET @Msg += '<Update CC (saldo) (' + convert(varchar,getdate(),8) + ')>'

	set Context_Info 0x0
end

if (select COUNT(*) from inserted where inserted.formapag='') > 0
begin

	set Context_Info 0x55555 -- evitar que o trigger de update corra

	UPDATE
		cc
	SET
		formapag=1
	from
		inserted
	WHERE
		cc.ccstamp=inserted.ccstamp AND cc.formapag=''

	if @@ROWCOUNT > 0
		SET @Msg += '<Update CC (formapag) (' + convert(varchar,getdate(),8) + ')>'
	
	set Context_Info 0x0
end

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg) ;