SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[tr_cm2_update] 
ON [dbo].[cm2]  
FOR UPDATE  
AS 

IF (@@ROWCOUNT = 0)
	RETURN;
	
SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Cm1_Update'


SET @Msg += '<' + convert(varchar,getdate(),14) + '>'

IF UPDATE (cmdesc)
BEGIN

	update
		cm1 
	set 
		cm1.fosln = inserted.cmdesc 
	from
		inserted  
	where 
		cm1.fosl=inserted.cm 
	
	if @@ROWCOUNT > 0
		SET @Msg += '<Update CM1 (' + convert(varchar,getdate(),8) + ')>'

	update
		ts 
	set 
		ts.cmdesc = inserted.cmdesc 
	from
		inserted  
	where 
		ts.cmstocks=inserted.cm 
	
	if @@ROWCOUNT > 0
		SET @Msg += '<Update TS (' + convert(varchar,getdate(),8) + ')>'

	update
		ts 
	set 
		ts.cm2desc = inserted.cmdesc 
	from
		inserted  
	where 
		ts.cm2stocks=inserted.cm 
	
	if @@ROWCOUNT > 0
		SET @Msg += '<Update TS (' + convert(varchar,getdate(),8) + ')>'

	update
		td 
	set 
		td.cmsln = inserted.cmdesc 
	from
		inserted  
	where 
		td.cmsl=inserted.cm

	if @@ROWCOUNT > 0
		SET @Msg += '<Update TD (' + convert(varchar,getdate(),8) + ')>'
		
	update
		sl 
	set 
		sl.cmdesc = inserted.cmdesc 
	from
		inserted  
	where 
		sl.cm=inserted.cm

	if @@ROWCOUNT > 0
		SET @Msg += '<Update SL (' + convert(varchar,getdate(),8) + ')>'
END

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg) ;