
IF  EXISTS (select * from sys.objects where type = 'TR' and name = 'tr_bo_update_encsite')
 DROP TRIGGER [dbo].[tr_bo_update_encsite]

/****** Object:  Trigger [dbo].[tr_bo_update_encsite]    Script Date: 19/01/2021 18:24:56 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
Create TRIGGER [dbo].[tr_bo_update_encsite] ON [dbo].[bo]
FOR update
AS
IF (@@ROWCOUNT = 0)
	RETURN;
SET NOCOUNT ON
-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'tr_bo_update_encsite'
SET @Msg += '<' + convert(varchar,getdate(),14) + '>'
IF exists (SELECT inserted.bostamp FROM inserted)
BEGIN
	declare @site varchar(30)
	set @site = (select top 1 site from inserted)
	declare @notif_enc bit
	set @notif_enc = (select top 1 bool from B_Parameters_site (nolock) where stamp='ADM0000000115' and site=@site)
	IF @notif_enc = 1
	begin
		declare @utilnotif varchar(200)
		set @utilnotif = (select textValue = ltrim(rtrim(isnull(textValue,''))) from B_Parameters_site (nolock) where stamp='ADM0000000113' and site=@site)
		if @utilnotif<>''
		begin
			insert into notificacoes
				(stamp
				,origem
				,usrorigem
				,destino
				,usrdestino
				,grpdestino,ecradestino,stampdestino,mensagem,recebido,tiponotif,data)
			select 
				left(newid(),21)
				,'Online'
				,''
				,''
				,b_us.iniciais
				,'' 
				,'BO'
				,inserted.bostamp
				,'Foi alterada a encomenda com o nº '+ cast(inserted.obrano as varchar(10))
				,0
				,'Alteração Encomenda'
				,getdate()
			from inserted 
			inner join b_us (nolock) on iniciais in (Select items from dbo.up_splitToTable(@utilnotif,','))
			where 
				inserted.nmdos='Encomenda de Cliente' 
				and inserted.usrinis='ONL'
				and (select count(stamp) from notificacoes (nolock) where tiponotif like '%Encomenda%' and recebido=0 and stampdestino=inserted.bostamp)=0
		end
		declare @grpnotif varchar(200)
		set @grpnotif = (select textValue = ltrim(rtrim(isnull(textValue,''))) from B_Parameters_site (nolock) where stamp='ADM0000000114' and site=@site)
		if @grpnotif<>''
		begin
			insert into notificacoes
				(stamp
				,origem
				,usrorigem
				,destino
				,usrdestino
				,grpdestino,ecradestino,stampdestino,mensagem,recebido,tiponotif,data)
			select 
				left(newid(),21)
				,'Online'
				,''
				,''
				,b_us.iniciais
				,'' 
				,'BO'
				,inserted.bostamp
				,'Foi alterada a encomenda com o nº '+ cast(inserted.obrano as varchar(10))
				,0
				,'Alteração Encomenda'
				,getdate()
			from inserted 
			inner join b_us on grupo in (Select items from dbo.up_splitToTable(@grpnotif,','))
			where 
				inserted.nmdos='Encomenda de Cliente' 
				and inserted.usrinis='ONL'
				and (select count(stamp) from notificacoes (nolock) where tiponotif like '%Encomenda%' and recebido=0 and stampdestino=inserted.bostamp)=0
		end
	end
	/*
		Atualiza cativação de valor em cartão
	*/
	IF (select sum(valcartao) from inserted)<>0 and (select top 1 bool from B_Parameters_site (nolock) where stamp='ADM0000000127' and site=(select top 1 site from inserted))=1
	begin
		set Context_Info 0x55556 -- evitar que o trigger de update corra validações desnecessárias
			print 'UPD CAT VALCARTAO'
			update b_fidel set valcartao_cativado= isnull((select sum(valcartao) from bo (nolock) where bo.fechada=0 and bo.nmdos='Encomenda de Cliente' and bo.no=inserted.no and bo.estab=inserted.estab),0)
			from inserted
			inner join b_fidel (nolock) on B_fidel.clno=inserted.no and B_fidel.clestab=inserted.estab where clno=inserted.no and clestab=inserted.estab
	END
END

