
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[tr_pl_update] 
ON [dbo].[pl]  
FOR UPDATE
AS 

IF (@@ROWCOUNT = 0)
	RETURN;
	
SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE 
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Pl_Update'	

SET @Msg += '<' + convert(varchar,getdate(),14) + '>'

update 
	FC
SET 
	fc.credfm = fc.credfm - x.credfm
	,fc.debfm = fc.debfm + x.debfm
	,fc.ecredf = fc.ecredf - x.ecredf
	,fc.edebf = fc.edebf + x.edebf
	,fc.evirsreg = fc.evirsreg - x.evirsreg
from (
	select
		credfm  = SUM(case when d.ecambio<>0 and d.rec>0 then d.rec else 0 end)
		,debfm = SUM(case when d.ecambio<>0 and  d.rec<0 then d.rec else 0 end)
		,ecredf = SUM(case when d.erec>0 then d.erec-d.earred else 0 end)
		,edebf = SUM(case when d.erec<0 then d.erec-d.earred else 0 end)
		,evirsreg = SUM(d.evirs)
		,fc.fcstamp
	from
		deleted d
		inner join fc on fc.fcstamp=d.fcstamp
	where
		d.process = 1
	group by
		fc.fcstamp
) x
	inner join fc on fc.fcstamp = x.fcstamp

--UPDATE
--	FC
--SET
--	fc.credfm = fc.credfm - (select SUM(case when deleted.ecambio<>0 and  deleted.rec>0 then deleted.rec else 0 end) from deleted where deleted.fcstamp=fc.fcstamp)
--	,fc.debfm = fc.debfm + (select SUM(case when deleted.ecambio<>0 and  deleted.rec<0 then deleted.rec else 0 end) from deleted where deleted.fcstamp=fc.fcstamp)
--	,fc.ecredf = fc.ecredf - (select SUM(case when deleted.erec>0 then deleted.erec-deleted.earred else 0 end) from deleted where deleted.fcstamp=fc.fcstamp)
--	,fc.edebf = fc.edebf + (select SUM(case when deleted.erec<0 then deleted.erec-deleted.earred else 0 end) from deleted where deleted.fcstamp=fc.fcstamp)
--	,fc.evirsreg = fc.evirsreg - (select SUM(deleted.evirs) from deleted where deleted.fcstamp=fc.fcstamp) 
--from
--	deleted
--where
--	deleted.fcstamp=fc.fcstamp
--	and deleted.process=1

if @@ROWCOUNT > 0
	SET @Msg += '<D Update FC (' + convert(varchar,getdate(),8) + ')>'

update 
	FC
SET 
	fc.evalpo = fc.evalpo - x.evalpo
	,fc.mvalpo = fc.mvalpo - x.mvalpo
	,fc.ultdoc = ''
	,fc.recibado = 0 
from (
	select
		evalpo = SUM(deleted.erec)
		,mvalpo = SUM(deleted.rec)
		,fc.fcstamp
	from
		deleted
		inner join fc on fc.fcstamp=deleted.fcstamp
	group by
		fc.fcstamp
) x
	inner join fc on fc.fcstamp = x.fcstamp

--UPDATE
--	FC 
--set 
--	fc.evalpo=fc.evalpo-(select SUM(deleted.erec) from deleted where deleted.fcstamp=fc.fcstamp)
--	,fc.mvalpo=fc.mvalpo-(select SUM(deleted.rec) from deleted where deleted.fcstamp=fc.fcstamp)
--	,fc.ultdoc=''
--	,fc.recibado=0 
--from
--	deleted 
--where 
--	fc.fcstamp=deleted.fcstamp

if @@ROWCOUNT > 0
	SET @Msg += '<D Update FC (' + convert(varchar,getdate(),8) + ')>'


UPDATE
	FC
set
	fc.eirsdif = fc.eirsdif - 
					(select SUM(case 
								when d.desconto=100 or po.fin=100 then 0 
								else (ROUND((d.evirs/(1-(case when d.desconto<>0 then d.desconto else po.fin end/100))),2) - d.evirs) 
								end) 
					from deleted d
					inner join po on po.postamp=d.postamp 
					where d.fcstamp=fc.fcstamp)
from
	deleted
	inner join po on po.postamp=deleted.postamp
where
	fc.fcstamp = deleted.fcstamp
	and deleted.process = 1
	and (deleted.desconto<>0 or po.fin<>0)

if @@ROWCOUNT > 0
	SET @Msg += '<D Update FC (' + convert(varchar,getdate(),8) + ')>'


update 
	FC
SET 
	fc.credfm = fc.credfm + x.credfm
	,fc.debfm = fc.debfm - x.debfm
	,fc.ecredf = fc.ecredf + x.ecredf
	,fc.edebf = fc.edebf - x.edebf
	,fc.evirsreg = fc.evirsreg + x.evirsreg
from (
	select
		credfm  = SUM(case when i.ecambio<>0 and i.rec>0 then i.rec else 0 end)
		,debfm = SUM(case when i.ecambio<>0 and  i.rec<0 then i.rec else 0 end)
		,ecredf = SUM(case when i.erec>0 then i.erec-i.earred else 0 end)
		,edebf = SUM(case when i.erec<0 then i.erec-i.earred else 0 end)
		,evirsreg = SUM(i.evirs)
		,fc.fcstamp
	from
		inserted i
		inner join fc on fc.fcstamp=i.fcstamp
	where
		i.process = 1
	group by
		fc.fcstamp
) x
	inner join fc on fc.fcstamp = x.fcstamp


--UPDATE
--	FC 
--SET 
--	fc.credfm=fc.credfm+(select SUM(case when inserted.ecambio<>0 and  inserted.rec>0 then inserted.rec else 0 end) from inserted where inserted.fcstamp=fc.fcstamp)
--	,fc.debfm=fc.debfm-(select SUM(case when inserted.ecambio<>0 and  inserted.rec<0 then inserted.rec else 0 end) from inserted where inserted.fcstamp=fc.fcstamp)
--	,fc.ecredf=fc.ecredf+(select SUM(case when inserted.erec>0 then inserted.erec-inserted.earred else 0 end) from inserted where inserted.fcstamp=fc.fcstamp)
--	,fc.edebf=fc.edebf-(select SUM(case when inserted.erec<0 then inserted.erec-inserted.earred else 0 end) from inserted where inserted.fcstamp=fc.fcstamp)
--	,fc.evirsreg=fc.evirsreg+(select SUM(inserted.evirs) from inserted where inserted.fcstamp=fc.fcstamp) 
--from inserted 
--where 
--	inserted.fcstamp=fc.fcstamp 
--	and inserted.process=1

if @@ROWCOUNT > 0
	SET @Msg += '<I Update FC (' + convert(varchar,getdate(),8) + ')>'


update 
	FC
SET 
	fc.evalpo = fc.evalpo + x.evalpo
	,fc.mvalpo = fc.mvalpo + x.mvalpo
	,fc.ultdoc = 'Doc. nº ' + convert(char(10),x.rno) + ' de ' + convert(char(10),x.rdata,104)
	,fc.recibado = 1 
from (
	select
		evalpo = SUM(inserted.erec)
		,mvalpo = SUM(inserted.rec)
		,fc.fcstamp
		,inserted.rno
		,inserted.rdata
	from
		inserted
		inner join fc on fc.fcstamp=inserted.fcstamp
	group by
		fc.fcstamp, inserted.rno, inserted.rdata
) x
	inner join fc on fc.fcstamp = x.fcstamp

--UPDATE
--	FC 
--set 
--	fc.evalpo=fc.evalpo+(select SUM(inserted.erec) from inserted where inserted.fcstamp=fc.fcstamp)
--	,fc.mvalpo=fc.mvalpo+(select SUM(inserted.rec) from inserted where inserted.fcstamp=fc.fcstamp)
--	,fc.ultdoc='Doc. nº '+convert(char(10),inserted.rno)+' de '+convert(char(10),inserted.rdata,104)
--	,fc.recibado=1 
--from
--	inserted 
--where 
--	fc.fcstamp=inserted.fcstamp

if @@ROWCOUNT > 0
	SET @Msg += '<I Update FC (' + convert(varchar,getdate(),8) + ')>'


UPDATE
	FC 
set
	fc.eirsdif = fc.eirsdif + 
					(select SUM(case 
								when i.desconto=100 or po.fin=100 then 0 
								else (ROUND((i.evirs/(1-(case when i.desconto<>0 then i.desconto else po.fin end/100))),2) - i.evirs) 
								end) 
					from inserted i
					inner join po on po.postamp=i.postamp 
					where i.fcstamp=fc.fcstamp)
from
	inserted
	inner join po on po.postamp=inserted.postamp
where
	fc.fcstamp=inserted.fcstamp
	and inserted.process=1
	and (inserted.desconto<>0 or po.fin<>0)

if @@ROWCOUNT > 0
	SET @Msg += '<I Update FC (' + convert(varchar,getdate(),8) + ')>'

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg);	