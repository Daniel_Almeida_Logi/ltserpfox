SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[tr_fn_delete] 
ON [dbo].[fn]  
FOR DELETE  
AS 

IF (@@ROWCOUNT = 0)
	RETURN;

SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Fn_Delete'


set @Msg = '<' + convert(varchar,getdate(),14) + '>'


DELETE
	SL
FROM
	deleted
where
	sl.fnstamp=deleted.fnstamp 
	and deleted.fnstamp != ''
	and deleted.ref != ''

if @@ROWCOUNT > 0
	SET @Msg += '<Delete SL (' + convert(varchar,getdate(),8) + ')>'

/*
	eliminar quantidade regularizada de documento de origem
*/
If exists (select d.bistamp from deleted d where d.bistamp != '' and d.qtt != 0)
Begin

	set Context_Info 0x55555 -- evitar que o trigger de update corra valida��es desnecess�rias

	UPDATE
		BI
	SET
		bi.adoc = ''
		,bi.nmdoc = ''
		,bi.ofostamp = ''
		,bi.ndoc = 0
		,bi.qtt2 = bi.qtt2 - (select SUM(d.qtt) from deleted d where d.bistamp = bi.bistamp)
		,bi.pbruto = bi.pbruto - (select SUM(d.qtt) from deleted d where d.bistamp = bi.bistamp) -- campo utilizado como qtt2, mas para ser compativel com as compras que n�o t�m qtt2
	from
		deleted
	where
		deleted.bistamp != ''
		and deleted.bistamp = bi.bistamp
	
	if @@ROWCOUNT > 0
		SET @Msg += '<Update BI (' + convert(varchar,getdate(),8) + ')>'

	set Context_Info 0x0
End

If exists (select d.fnstamp from deleted d where d.ofnstamp != '' and d.qtt != 0)
Begin

	set Context_Info 0x55557 -- evitar que o trigger de update corra valida��es desnecess�rias

	UPDATE
		FN
	SET
		fn.pbruto = fn.pbruto - (select SUM(d.qtt) from deleted d where d.ofnstamp = fn.fnstamp) -- campo utilizado como qtt2, mas para ser compativel com as compras que n�o t�m qtt2
	from
		deleted
	where
		deleted.ofnstamp != ''
		and deleted.ofnstamp = fn.fnstamp
	
	if @@ROWCOUNT > 0
		SET @Msg += '<Update FN (pbruto) (' + convert(varchar,getdate(),8) + ')>'

	set Context_Info 0x0
End

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg);	
