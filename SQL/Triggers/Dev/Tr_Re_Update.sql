
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[tr_re_update]
ON [dbo].[re]
FOR UPDATE
AS

IF (@@ROWCOUNT = 0)
	RETURN;
	
SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Re_Update'


SET @Msg += '<' + convert(varchar,getdate(),14) + '>'

if	update(usrhora) or update(chdata) or update(no) or update(nome) or update(clbanco) or update(clcheque) or update(procdata)
	or update(cobranca) or update(edifcambio) or update(arred) or update(etotal) or update(earred) or update(moeda) or update(rno)
	or update(procdata) or update(fref) or update(intid) or update(cobrador) or update(rota) or update(ccusto) or update(ncusto) or update(zona) or update(vendedor) or update(vendnm)
	or update(segmento) or update(tipo) or update(pais) or update(estab) or update(desc1) or update(etotol2) or update(ollocal) or update(contado)
	or update(ollocal2) or update(contado2) or update(telocal) or update(telocal2) or update(olcodigo) or update(finv) or update(process)
BEGIN

	SET @Msg += '(' + convert(varchar,getdate(),8) + ')>'

	if exists (select i.restamp from inserted i where i.process=1 and i.restamp not in (select cc.ccstamp from cc where cc.ccstamp=i.restamp))
	begin

		INSERT INTO CC 
			(
			ccstamp
			,restamp
			,faccstamp
			,usrdata,usrhora,usrinis,ousrdata,ousrhora,ousrinis
			,datalc
			,no
			,nome
			,cm
			,cmdesc
			,origem
			,cobranca
			,difcambio
			,edifcambio
			,ecred
			,ecredf
			,credm
			,credfm
			,edeb
			,edebf
			,debm
			,debfm
			,moeda
			,nrdoc
			,dataven
			,fref
			,intid
			,cobrador
			,rota
			,ccusto
			,ncusto
			,zona
			,vendedor
			,vendnm
			,segmento
			,tipo
			,pais
			,estab
			,obs
			) 
		select 
			inserted.restamp
			,inserted.restamp
			,inserted.faccstamp
			,inserted.usrdata
			,inserted.usrhora
			,inserted.usrinis
			,inserted.ousrdata
			,inserted.ousrhora
			,inserted.ousrinis
			,inserted.procdata
			,inserted.no
			,inserted.nome
			,(case when exists (select cm from cm1 (nolock) where cm=(select cmcc from tsre (nolock) where ndoc=inserted.ndoc)) then (select cmcc from tsre (nolock) where ndoc=inserted.ndoc) else 70 end)
			,(case when exists (select cmdesc from cm1 (nolock) where cm=(select cmcc from tsre (nolock) where ndoc=inserted.ndoc)) then (select cmccn from tsre (nolock) where ndoc=inserted.ndoc) else (select cmdesc from cm1 (nolock) where cm=70) end)
			,'RE'
			,inserted.cobranca
			,inserted.difcambio
			,inserted.edifcambio
			,case when inserted.etotal-inserted.earred<0 then 0 else inserted.etotal-inserted.earred end
			,case when inserted.etotal-inserted.earred<0 then 0 else inserted.etotal-inserted.earred end
			,case when inserted.totalmoeda<0 then 0 else inserted.totalmoeda end,case when inserted.totalmoeda<0 then 0 else inserted.totalmoeda end
			,case when inserted.etotal-inserted.earred>0 then 0 else - (inserted.etotal-inserted.earred) end
			,case when inserted.etotal-inserted.earred>0 then 0 else - (inserted.etotal-inserted.earred) end
			,case when inserted.totalmoeda>0 then 0 else -inserted.totalmoeda end
			,case when inserted.totalmoeda>0 then 0 else -inserted.totalmoeda end
			,inserted.moeda
			,inserted.rno
			,inserted.procdata
			,inserted.fref
			,inserted.intid
			,inserted.cobrador
			,inserted.rota
			,inserted.ccusto
			,inserted.ncusto
			,inserted.zona
			,inserted.vendedor
			,inserted.vendnm
			,inserted.segmento
			,inserted.tipo
			,inserted.pais
			,inserted.estab
			,inserted.desc1 
		from
			inserted 
		where 
			inserted.process=1 
			and inserted.restamp not in (select cc.ccstamp from cc where cc.ccstamp=inserted.restamp) 

		if @@ROWCOUNT>0
			SET @Msg += '<I Insert CC (' + convert(varchar,getdate(),8) + ')>'
	end
	

	--if (select count(*) from deleted where deleted.cheque=1)>0
	--begin
	--	set @Msg += '<Delete RECH>'

	--	DELETE
	--		RECH 
	--	FROM
	--		inserted 
	--	where 
	--		inserted.cheque=0 
	--		and inserted.restamp in (select rech.restamp from rech where rech.restamp=inserted.restamp) 
	--		and inserted.restamp = rech.restamp 
	--end


	if exists (select i.restamp from inserted i where i.process=1 and i.restamp in (select cc.ccstamp from cc where cc.ccstamp=i.restamp))
	begin

		UPDATE
			CC 
		SET 
			cc.ccstamp=inserted.restamp
			,cc.restamp=inserted.restamp
			,cc.faccstamp=inserted.faccstamp
			,cc.usrdata=inserted.usrdata
			,cc.usrhora=inserted.usrhora
			,cc.usrinis=inserted.usrinis
			,cc.datalc=inserted.procdata
			,cc.no=inserted.no
			,cc.nome=inserted.nome
			,cc.cm=(case when exists (select cm from cm1 (nolock) where cm=(select cmcc from tsre (nolock) where ndoc=inserted.ndoc)) then (select cmcc from tsre (nolock) where ndoc=inserted.ndoc) else 70 end)
			,cc.cmdesc=(case when exists (select cmdesc from cm1 (nolock) where cm=(select cmcc from tsre (nolock) where ndoc=inserted.ndoc)) then (select cmccn from tsre (nolock) where ndoc=inserted.ndoc) else (select cmdesc from cm1 (nolock) where cm=70) end)
			,cc.origem='RE'
			,cc.cobranca=inserted.cobranca
			,cc.difcambio=inserted.difcambio
			,cc.edifcambio=inserted.edifcambio
			,cc.ecred=case when inserted.etotal-inserted.earred<0 then cc.ecred else inserted.etotal-inserted.earred end
			,cc.ecredf=case when inserted.etotal-inserted.earred<0 then cc.ecredf else inserted.etotal-inserted.earred end
			,cc.credm=case when inserted.totalmoeda<0 then cc.credm else inserted.totalmoeda end
			,cc.credfm=case when inserted.totalmoeda<0 then cc.credfm else inserted.totalmoeda end
			,cc.edeb=case when inserted.etotal-inserted.earred>0 then cc.edeb else -(inserted.etotal-inserted.earred) end
			,cc.edebf=case when inserted.etotal-inserted.earred>0 then cc.edebf else -(inserted.etotal-inserted.earred) end
			,cc.debm=case when inserted.totalmoeda>0 then cc.debm else -inserted.totalmoeda end
			,cc.debfm=case when inserted.totalmoeda>0 then cc.debfm else -inserted.totalmoeda end
			,cc.moeda=inserted.moeda,cc.nrdoc=inserted.rno,cc.dataven=inserted.procdata
			,cc.fref=inserted.fref
			,cc.intid=inserted.intid
			,cc.cobrador=inserted.cobrador
			,cc.rota=inserted.rota
			,cc.ccusto=inserted.ccusto
			,cc.ncusto=inserted.ncusto
			,cc.zona=inserted.zona
			,cc.vendedor=inserted.vendedor
			,cc.vendnm=inserted.vendnm
			,cc.segmento=inserted.segmento
			,cc.tipo=inserted.tipo
			,cc.pais=inserted.pais
			,cc.estab=inserted.estab,
			cc.obs=inserted.desc1
		from
			inserted 
		where 
			inserted.process=1 
			--and inserted.restamp in (select cc.ccstamp from cc where cc.ccstamp=inserted.restamp) 
			and inserted.restamp=cc.ccstamp 
		
		if @@ROWCOUNT>0
			SET @Msg += '<I Update CC (' + convert(varchar,getdate(),8) + ')>'
	end
	

	DELETE
		CC 
	FROM
		inserted 
	where 
		inserted.process=0 
		--and substring('I'+inserted.restamp,1,25) in (select cc.ccstamp from cc where cc.ccstamp=substring('I'+inserted.restamp,1,25)) 
		and substring('I'+inserted.restamp,1,25) = cc.ccstamp 

	if @@ROWCOUNT>0
		SET @Msg += '<I Delete CC (' + convert(varchar,getdate(),8) + ')>'


	INSERT INTO CC 
		(
		ccstamp
		,restamp
		,faccstamp
		,usrdata,usrhora,usrinis,ousrdata,ousrhora,ousrinis
		,datalc
		,no
		,nome
		,cm
		,cmdesc
		,origem
		,ecred
		,ecredf
		,edeb
		,edebf
		,moeda
		,nrdoc
		,dataven
		,fref
		,ccusto
		,ncusto
		,intid
		,tipo
		,estab
		,pais
		,zona
		,obs
		) 
	select 
		substring('I'+inserted.restamp,1,25)
		,inserted.restamp
		,inserted.faccstamp
		,inserted.usrdata,inserted.usrhora,inserted.usrinis,inserted.ousrdata,inserted.ousrhora,inserted.ousrinis
		,inserted.procdata
		,inserted.no
		,inserted.nome
		,101
		,(case when exists (select cmdesc from cm1 (nolock) where cm=101) then (select cmdesc from cm1 (nolock) where cm=101) else 'Indefinido' end)
		,'RE'
		,case when inserted.evirs<0 then 0 else inserted.evirs end
		,case when inserted.evirs<0 then 0 else inserted.evirs end
		,case when inserted.evirs>0 then 0 else -inserted.evirs end
		,case when inserted.evirs>0 then 0 else -inserted.evirs end
		,inserted.moeda
		,inserted.rno
		,inserted.procdata
		,inserted.fref
		,inserted.ccusto
		,inserted.ncusto
		,inserted.intid
		,inserted.tipo
		,inserted.estab
		,inserted.pais
		,inserted.zona
		,inserted.desc1 
	from
		inserted 
	where 
		inserted.process = 1 
		and (inserted.virs<>0 or inserted.evirs<>0) 
		and (substring('I'+inserted.restamp,1,25) not in (select cc.ccstamp from cc (nolock) where cc.ccstamp=substring('I'+inserted.restamp,1,25))) 
		
	
	if @@ROWCOUNT > 0
		SET @Msg += '<Insert CC (' + convert(varchar,getdate(),8) + ')>'


	UPDATE
		CC 
	SET 
		cc.ccstamp=substring('I'+inserted.restamp,1,25)
		,cc.restamp=inserted.restamp
		,cc.faccstamp=inserted.faccstamp
		,cc.usrdata=inserted.usrdata
		,cc.usrhora=inserted.usrhora
		,cc.usrinis=inserted.usrinis
		,cc.datalc=inserted.procdata
		,cc.no=inserted.no
		,cc.nome=inserted.nome
		,cc.cm=101
		,cc.cmdesc=(case when exists (select cmdesc from cm1 (nolock) where cm=101) then (select cmdesc from cm1 (nolock) where cm=101) else 'Indefinido' end)
		,cc.origem='RE'
		,cc.ecred=case when inserted.evirs<0 then 0 else inserted.evirs end
		,cc.ecredf=case when inserted.evirs<0 then 0 else inserted.evirs end
		,cc.edeb=case when inserted.evirs>0 then 0 else -inserted.evirs end
		,cc.edebf=case when inserted.evirs>0 then 0 else -inserted.evirs end
		,cc.moeda=inserted.moeda
		,cc.nrdoc=inserted.rno
		,cc.dataven=inserted.procdata
		,cc.fref=inserted.fref
		,cc.ccusto=inserted.ccusto
		,cc.ncusto=inserted.ncusto
		,cc.intid=inserted.intid
		,cc.tipo=inserted.tipo
		,cc.estab=inserted.estab
		,cc.pais=inserted.pais
		,cc.zona=inserted.zona
		,cc.obs=inserted.desc1
	from
		inserted 
	where 
		inserted.process=1 
		and (inserted.virs<>0 or inserted.evirs<>0) 
		--and substring('I'+inserted.restamp,1,25) in (select cc.ccstamp from cc where cc.ccstamp=substring('I'+inserted.restamp,1,25)) 
		and substring('I'+inserted.restamp,1,25) = cc.ccstamp 
		
	
	if @@ROWCOUNT>0
		SET @Msg += '<Update CC (' + convert(varchar,getdate(),8) + ')>'

	DELETE
		CC 
	FROM
		inserted 
	where 
		inserted.process=0 
		--and inserted.restamp in (select cc.ccstamp from cc where cc.ccstamp=inserted.restamp) 
		and inserted.restamp=cc.ccstamp 
	
	if @@ROWCOUNT > 0
		SET @Msg += '<Delete CC (' + convert(varchar,getdate(),8) + ')>'


	if exists (select i.restamp from inserted i where i.process=1 and i.finv<>0)
	begin
		INSERT INTO CC 
			(
			ccstamp
			,restamp
			,faccstamp
			,usrdata,usrhora,usrinis,ousrdata,ousrhora,ousrinis
			,datalc
			,no
			,nome
			,cm
			,cmdesc
			,origem
			,cobranca
			,ecred
			,ecredf
			,credm
			,credfm
			,edeb
			,edebf
			,debm
			,debfm
			,moeda
			,nrdoc
			,dataven
			,fref
			,intid
			,cobrador
			,rota
			,ccusto
			,ncusto
			,zona
			,vendedor
			,vendnm
			,obs
			,estab
			) 
		select 
			substring('F'+inserted.restamp,1,25)
			,inserted.restamp
			,inserted.faccstamp
			,inserted.usrdata,inserted.usrhora,inserted.usrinis,inserted.ousrdata,inserted.ousrhora,inserted.ousrinis
			,inserted.procdata
			,inserted.no
			,inserted.nome
			,94
			,(select cmdesc from cm1 (nolock) where cm=94)
			,'RE'
			,inserted.cobranca
			,case when inserted.efinv<0 then 0 else inserted.efinv end
			,case when inserted.efinv<0 then 0 else inserted.efinv end
			,case when inserted.finvmoeda<0 then 0 else inserted.finvmoeda end
			,case when inserted.finvmoeda<0 then 0 else inserted.finvmoeda end
			,case when inserted.efinv>0 then 0 else -inserted.efinv end
			,case when inserted.efinv>0 then 0 else -inserted.efinv end
			,case when inserted.finvmoeda>0 then 0 else -inserted.finvmoeda end,case when inserted.finvmoeda>0 then 0 else -inserted.finvmoeda end
			,inserted.moeda
			,inserted.rno
			,inserted.procdata
			,inserted.fref
			,inserted.intid
			,inserted.cobrador
			,inserted.rota
			,inserted.ccusto
			,inserted.ncusto
			,inserted.zona
			,inserted.vendedor
			,inserted.vendnm
			,inserted.desc1
			,inserted.estab 
		from
			inserted
		where
			inserted.process = 1
			and inserted.finv != 0
			and substring('F'+inserted.restamp,1,25) not in (select cc.ccstamp from cc where cc.ccstamp=substring('F'+inserted.restamp,1,25))

		if @@ROWCOUNT > 0
			SET @Msg += '<Insert CC (' + convert(varchar,getdate(),8) + ')>'
	end


	if exists (select i.restamp from inserted i where i.process=1 and i.finv<>0)
	begin
		UPDATE
			CC 
		SET 
			cc.ccstamp=substring('F'+inserted.restamp,1,25)
			,cc.restamp=inserted.restamp
			,cc.faccstamp=inserted.faccstamp
			,cc.usrdata=inserted.usrdata,cc.usrhora=inserted.usrhora,cc.usrinis=inserted.usrinis
			,cc.datalc=inserted.procdata
			,cc.no=inserted.no
			,cc.nome=inserted.nome
			,cc.cm=94
			,cc.cmdesc=(select cmdesc from cm1 (nolock) where cm=94)
			,cc.origem='RE'
			,cc.cobranca=inserted.cobranca
			,cc.ecred=case when inserted.efinv<0 then cc.ecred else inserted.efinv end
			,cc.ecredf=case when inserted.efinv<0 then cc.ecredf else inserted.efinv end
			,cc.credm=case when inserted.finvmoeda<0 then cc.credm else inserted.finvmoeda end
			,cc.credfm=case when inserted.finvmoeda<0 then cc.credfm else inserted.finvmoeda end
			,cc.edeb=case when inserted.efinv>0 then cc.edeb else -inserted.efinv end
			,cc.edebf=case when inserted.efinv>0 then cc.edebf else -inserted.efinv end
			,cc.debm=case when inserted.finvmoeda>0 then cc.debm else -inserted.finvmoeda end
			,cc.debfm=case when inserted.finvmoeda>0 then cc.debfm else -inserted.finvmoeda end
			,cc.moeda=inserted.moeda
			,cc.nrdoc=inserted.rno
			,cc.dataven=inserted.procdata
			,cc.fref=inserted.fref
			,cc.intid=inserted.intid
			,cc.cobrador=inserted.cobrador
			,cc.rota=inserted.rota
			,cc.ccusto=inserted.ccusto
			,cc.ncusto=inserted.ncusto
			,cc.zona=inserted.zona
			,cc.vendedor=inserted.vendedor
			,cc.vendnm=inserted.vendnm
			,cc.obs=inserted.desc1 
		from
			inserted 
		where 
			inserted.process=1 
			--and substring('F'+inserted.restamp,1,25) in (select cc.ccstamp from cc where cc.ccstamp=substring('F'+inserted.restamp,1,25)) 
			and substring('F'+inserted.restamp,1,25)=cc.ccstamp 
			and inserted.finv != 0 

		if @@ROWCOUNT>0
			SET @Msg += '<I Update CC (' + convert(varchar,getdate(),8) + ')>'
	end

	DELETE
		CC 
	FROM
		inserted 
	where 
		inserted.process=0 
		--and substring('F'+inserted.restamp,1,25) in (select cc.ccstamp from cc where cc.ccstamp=substring('F'+inserted.restamp,1,25)) 
		and substring('F'+inserted.restamp,1,25)=cc.ccstamp 

	if @@ROWCOUNT>0
		SET @Msg += '<I Delete CC (' + convert(varchar,getdate(),8) + ')>'
END 

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg);