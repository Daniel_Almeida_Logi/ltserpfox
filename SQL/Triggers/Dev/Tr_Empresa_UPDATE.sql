SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[Tr_Empresa_UPDATE]') IS NOT NULL
	drop trigger dbo.Tr_Empresa_UPDATE
go

CREATE TRIGGER [dbo].[Tr_Empresa_UPDATE] 
ON [dbo].[Empresa] FOR UPDATE 
AS

IF (@@ROWCOUNT = 0) 
	RETURN ;

SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE 
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Empresa_Update'	


set @Msg = '<' + convert(varchar,getdate(),14) + '>'



/*
	Update sl
*/
IF	UPDATE(nomecomp) or UPDATE(no) or UPDATE(ncont) or UPDATE(morada) or UPDATE(codpost) or UPDATE(local) 
	or UPDATE(ref) or UPDATE(codpost) or UPDATE(telefone) or UPDATE(fax) or UPDATE(codigo) or UPDATE(email)
BEGIN

	UPDATE
		ag
	set
      nome = inserted.nomecomp
      ,no = inserted.no
      ,ncont= inserted.ncont
      ,morada = left(inserted.morada,55)
      ,local = inserted.local
      ,codpost = inserted.codpost
      ,telefone = inserted.telefone
      ,fax = inserted.fax
      ,codigo = inserted.codigo
      ,email = inserted.email
      ,ousrinis = inserted.ousrinis
      ,ousrdata = inserted.ousrdata
      ,ousrhora = inserted.ousrhora
      ,usrinis = inserted.usrinis
      ,usrdata = inserted.usrdata
      ,usrhora = inserted.usrhora
	FROM 
	 inserted
	WHERE 
		inserted.no = ag.no

	if @@ROWCOUNT > 0
		SET @Msg += '<Update SL (' + convert(varchar,getdate(),8) + ')>'
END



-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg) ;
