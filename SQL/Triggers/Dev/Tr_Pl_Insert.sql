
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TRIGGER [dbo].[tr_pl_insert] 
ON [dbo].[pl]  
FOR INSERT  
AS 

IF (@@ROWCOUNT = 0)
	RETURN;
	
SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE 
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Pl_Insert'	

SET @Msg += '<' + convert(varchar,getdate(),14) + '>'

update 
	FC
SET 
	fc.credfm = fc.credfm + x.credfm
	,fc.debfm = fc.debfm - x.debfm
	,fc.ecredf = fc.ecredf + x.ecredf
	,fc.edebf = fc.edebf - x.edebf
	,fc.evirsreg = fc.evirsreg + x.evirsreg
from (
	select
		credfm  = SUM(case when i.ecambio<>0 and i.rec>0 then i.rec else 0 end)
		,debfm = SUM(case when i.ecambio<>0 and  i.rec<0 then i.rec else 0 end)
		,ecredf = SUM(case when i.erec>0 then i.erec-i.earred else 0 end)
		,edebf = SUM(case when i.erec<0 then i.erec-i.earred else 0 end)
		,evirsreg = SUM(i.evirs)
		,fc.fcstamp
	from
		inserted i
		inner join fc on fc.fcstamp=i.fcstamp
	where
		i.process = 1
	group by
		fc.fcstamp
) x
	inner join fc on fc.fcstamp = x.fcstamp

--UPDATE
--	FC 
--SET 
--	fc.credfm = fc.credfm + (select SUM(case when i.ecambio<>0 and i.rec>0 then i.rec else 0 end) from inserted i where i.fcstamp=fc.fcstamp)
--	,fc.debfm = fc.debfm - (select SUM(case when i.ecambio<>0 and  i.rec<0 then i.rec else 0 end) from inserted i where i.fcstamp=fc.fcstamp)
--	,fc.ecredf = fc.ecredf + (select SUM(case when i.erec>0 then i.erec-i.earred else 0 end) from inserted i where i.fcstamp=fc.fcstamp)
--	,fc.edebf = fc.edebf - (select SUM(case when i.erec<0 then i.erec-i.earred else 0 end) from inserted i where i.fcstamp=fc.fcstamp)
--	,fc.evirsreg = fc.evirsreg + (select SUM(i.evirs) from inserted i where i.fcstamp=fc.fcstamp) 
--from
--	inserted 
--where 
--	inserted.fcstamp=fc.fcstamp 
--	and inserted.process=1

if @@ROWCOUNT > 0
	SET @Msg += '<I Update FC (' + convert(varchar,getdate(),8) + ')>'


update 
	FC
SET 
	fc.evalpo = fc.evalpo + x.evalpo
	,fc.mvalpo = fc.mvalpo + x.mvalpo
	,fc.ultdoc = 'Doc. nº ' + convert(char(10),x.rno) + ' de ' + convert(char(10),x.rdata,104)
	,fc.recibado = 1 
from (
	select
		evalpo = SUM(inserted.erec)
		,mvalpo = SUM(inserted.rec)
		,fc.fcstamp
		,inserted.rno
		,inserted.rdata
	from
		inserted
		inner join fc on fc.fcstamp=inserted.fcstamp
	group by
		fc.fcstamp, inserted.rno, inserted.rdata
) x
	inner join fc on fc.fcstamp = x.fcstamp

--UPDATE
--	FC 
--set 
--	fc.evalpo = fc.evalpo+(select SUM(inserted.erec) from inserted where inserted.fcstamp=fc.fcstamp)
--	,fc.mvalpo = fc.mvalpo+(select SUM(inserted.rec) from inserted where inserted.fcstamp=fc.fcstamp)
--	,fc.ultdoc = 'Doc. nº ' + convert(char(10),inserted.rno)+' de '+convert(char(10),inserted.rdata,104)
--	,fc.recibado = 1 
--from
--	inserted 
--where 
--	fc.fcstamp=inserted.fcstamp

if @@ROWCOUNT > 0
	SET @Msg += '<I Update FC (' + convert(varchar,getdate(),8) + ')>'


UPDATE
	FC 
set
	fc.eirsdif= fc.eirsdif + 
					(select SUM(case 
								when i.desconto=100 or po.fin=100 then 0 
								else (ROUND((i.evirs/(1-(case when i.desconto<>0 then i.desconto else po.fin end/100))),2) - i.evirs) 
								end) 
					from inserted i
					inner join po on po.postamp=inserted.postamp 
					where i.fcstamp=fc.fcstamp)
from
	inserted 
	inner join po on po.postamp=inserted.postamp 
where 
	fc.fcstamp = inserted.fcstamp 
	and inserted.process = 1 
	and (inserted.desconto<>0 or po.fin<>0)

if @@ROWCOUNT > 0
	SET @Msg += '<I Update FC (' + convert(varchar,getdate(),8) + ')>'

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg);	