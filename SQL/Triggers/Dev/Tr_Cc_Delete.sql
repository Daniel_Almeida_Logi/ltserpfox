SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[tr_cc_delete] 
ON [dbo].[cc] 
FOR DELETE 
AS 


IF (@@ROWCOUNT = 0)
	RETURN;

SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Cc_Delete'


SET @Msg += '<' + convert(varchar,getdate(),14) + '>'


/*
	Atualiza saldo de cliente na ficha
*/
UPDATE
	b_utentes 
SET
	b_utentes.esaldo = b_utentes.esaldo - (select 
												sum(deleted.edeb-deleted.ecred) 
											from
												deleted 
											where 
												deleted.no = b_utentes.no and deleted.estab = b_utentes.estab)
from
	deleted
where
	b_utentes.no = deleted.no and b_utentes.estab = deleted.estab

if @@ROWCOUNT > 0
	SET @Msg += '<Update B_UTENTES (' + convert(varchar,getdate(),8) + ')>'


/*
	Atualiza conta corrente
*/
if exists (select count(*) from deleted where RTRIM(LTRIM(deleted.occstamp)) = '')
begin

	set Context_Info 0x55555 -- evitar que o trigger de update corra

	UPDATE
		CC 
	SET 
		cc.edebf = cc.edebf-deleted.ecred
		,cc.ecredf = cc.ecredf-deleted.edeb 
	from
		deleted 
	where 
		cc.cm=deleted.cr 
		and cc.nrdoc=deleted.docref 
		and cc.no=deleted.no

	if @@ROWCOUNT > 0
		SET @Msg += '<Update CC (' + convert(varchar,getdate(),8) + ')>'


	UPDATE
		CC 
	SET 
		cc.edebf = cc.edebf-deleted.ecred
		,cc.ecredf = cc.ecredf-deleted.edeb 
	from
		deleted 
	where 
		cc.cm=deleted.cr 
		and cc.nrdoc=deleted.docref 
		and cc.no=deleted.no 
		and cc.ccstamp = deleted.occstamp

	if @@ROWCOUNT > 0
		SET @Msg += '<Update CC (' + convert(varchar,getdate(),8) + ')>'

	set Context_Info 0x0
end

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg) ;