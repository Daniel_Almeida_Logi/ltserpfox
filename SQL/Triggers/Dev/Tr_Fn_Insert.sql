SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[tr_fn_insert] 
ON [dbo].[fn]  
FOR INSERT  
AS 

IF (@@ROWCOUNT = 0)
	RETURN;

SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Fn_Insert'

set @Msg = '<' + convert(varchar,getdate(),14) + '>'

/*
	Insert SL
*/
if exists (select i.fnstamp 
			from INSERTED i
				inner join fo on fo.fostamp = i.fostamp
				inner join fo2 on fo2.fo2stamp = i.fostamp and fo2.anulado = 0
				inner join cm1 (nolock) on fo.doccode = cm1.cm
			where cm1.folansl = 1 and i.ref != '' and i.qtt != 0
		)
BEGIN
	INSERT INTO SL (
		slstamp,fnstamp,usrinis,usrhora,usrdata,ousrinis,ousrhora,ousrdata,origem
		,fref,ccusto,ncusto,nome,codigo,moeda,datalc,design
		,ref,lote,armazem,qtt
		,ett
		,evu
		,num1
		,frcl,adoc,cm,cmdesc,composto,cor,tam,usr1,usr2,usr3,usr4
		,vumoeda
		,ttmoeda
		,pcpond,cpoc
		,valiva,evaliva
		,epcpond
		,stns)
	select
		inserted.fnstamp,inserted.fnstamp,inserted.usrinis,inserted.usrhora,inserted.usrdata,inserted.ousrinis,inserted.ousrhora,inserted.ousrdata,'FO'
		,fo.fref,inserted.fnccusto,fo.ncusto
		,fo.nome,inserted.codigo,fo.moeda,(case when inserted.svdata > '19000101' then inserted.svdata else fo.data end),inserted.design
		,inserted.ref,inserted.lote,inserted.armazem,inserted.qtt
		,inserted.esltt
		,inserted.eslvu
		,inserted.num1
		,fo.no,fo.adoc,cm1.fosl,cm1.fosln,inserted.composto,inserted.cor,inserted.tam,inserted.usr1,inserted.usr2,inserted.usr3,inserted.usr4
		,inserted.slvumoeda
		,inserted.slvumoeda*inserted.qtt
		,inserted.slvu,inserted.cpoc
		,case when inserted.ivaincl=1 then inserted.tiliquido-inserted.tiliquido/(1+inserted.iva/100) else inserted.tiliquido*inserted.iva/100 end
		,case when inserted.ivaincl=1 then inserted.etiliquido-inserted.etiliquido/(1+inserted.iva/100) else inserted.etiliquido*inserted.iva/100 end
		,inserted.eslvu
		,inserted.stns
	from
		inserted
		inner join fo on fo.fostamp = inserted.fostamp
		inner join cm1 (nolock) on fo.doccode = cm1.cm
		inner join fo2 on fo2.fo2stamp = inserted.fostamp and fo2.anulado = 0
	where
		cm1.folansl = 1
		and inserted.ref != ''
		and inserted.qtt != 0
	
	if @@ROWCOUNT > 0
		SET @Msg += '<Insert SL (' + convert(varchar,getdate(),8) + ')>'
END


If exists (select i.bistamp from inserted i where i.bistamp != '' and i.qtt != 0)
Begin

	set Context_Info 0x55555 -- evitar que o trigger de update corra validações desnecessárias

	UPDATE
		BI
	SET
		bi.fdata = fo.data
		,bi.adoc = fo.adoc
		,bi.ofostamp= fo.fostamp
		,bi.nmdoc = fo.docnome
		,bi.ndoc = fo.doccode
		,bi.qtt2 = bi.qtt2 + (select SUM(i.qtt) from inserted i where i.bistamp = bi.bistamp)
		,bi.pbruto = bi.pbruto + (select SUM(i.qtt) from inserted i where i.bistamp = bi.bistamp) -- campo utilizado como qtt2, mas para ser compativel com as compras que não têm qtt2
	 from
		inserted
		inner join fo on inserted.fostamp = fo.fostamp
	 where
		inserted.bistamp=bi.bistamp

	if @@ROWCOUNT > 0
		SET @Msg += '<Update BI (' + convert(varchar,getdate(),8) + ')>'
	
	set Context_Info 0x0
End

If exists (select i.fnstamp from inserted i where i.ofnstamp != '' and i.qtt != 0)
Begin

	set Context_Info 0x55557 -- evitar que o trigger de update corra validações desnecessárias

	UPDATE
		FN
	SET
		fn.pbruto = fn.pbruto + (select SUM(i.qtt) from inserted i where i.ofnstamp = fn.fnstamp) -- campo utilizado como qtt2, mas para ser compativel com as compras que não têm qtt2
	 from
		inserted
	 where
		inserted.ofnstamp != ''
		and inserted.ofnstamp=fn.fnstamp

	if @@ROWCOUNT > 0
		SET @Msg += '<Update FN (pbruto) (' + convert(varchar,getdate(),8) + ')>'
	
	set Context_Info 0x0
End

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg);	