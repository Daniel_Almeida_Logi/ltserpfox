
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TRIGGER [dbo].[tr_po_insert] 
ON [dbo].[po]  
FOR INSERT  
AS 

IF (@@ROWCOUNT = 0)
	RETURN;
	
SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'Tr_Po_Insert'

SET @Msg += '<' + convert(varchar,getdate(),14) + '>'

INSERT INTO FC
	(
	fcstamp
	,postamp
	,usrdata,usrhora,usrinis,ousrdata,ousrhora,ousrinis
	,formapag
	,situacao
	,datalc
	,no
	,nome
	,cm
	,cmdesc
	,origem
	,edifcambio
	,edeb
	,edebf
	,debm
	,debfm
	,ecred
	,ecredf
	,credm
	,credfm
	,moeda
	,adoc
	,dataven
	,fref
	,ccusto
	,ncusto
	,intid
	,tipo
	,estab
	,pais
	,zona
	,obs
	)
select
	inserted.postamp
	,inserted.postamp
	,inserted.usrdata,inserted.usrhora,inserted.usrinis,inserted.ousrdata,inserted.ousrhora,inserted.ousrinis
	,'1'
	,'2'
	,inserted.procdata
	,inserted.no
	,inserted.nome
	,inserted.cm
	,inserted.cmdesc
	,'PO'
	,inserted.edifcambio
	,case when inserted.etotal<0 then 0 else inserted.etotal-inserted.earred end
	,case when inserted.etotal<0 then 0 else inserted.etotal-inserted.earred end
	,case when inserted.totalmoeda<0 then 0 else inserted.totalmoeda end
	,case when inserted.totalmoeda<0 then 0 else inserted.totalmoeda end
	,case when inserted.etotal>0 then 0 else -inserted.etotal-inserted.earred end
	,case when inserted.etotal>0 then 0 else -inserted.etotal-inserted.earred end
	,case when inserted.totalmoeda>0 then 0 else -inserted.totalmoeda end
	,case when inserted.totalmoeda>0 then 0 else -inserted.totalmoeda end
	,inserted.moeda
	,inserted.adoc
	,inserted.procdata
	,inserted.fref
	,inserted.ccusto
	,inserted.ncusto
	,inserted.intid
	,inserted.tipo
	,inserted.estab
	,inserted.pais
	,inserted.zona
	,inserted.desc1
from
	inserted
where
	inserted.process=1

if @@ROWCOUNT > 0
	SET @Msg += '<Insert FC (' + convert(varchar,getdate(),8) + ')>'


INSERT INTO FC
	(
	fcstamp
	,postamp
	,usrdata, usrhora, usrinis, ousrdata, ousrhora, ousrinis
	,formapag
	,situacao
	,datalc
	,no
	,nome
	,cm
	,cmdesc
	,origem
	,edeb
	,edebf
	,debm
	,debfm
	,ecred
	,ecredf
	,credm
	,credfm
	,moeda
	,adoc
	,dataven
	,fref
	,ccusto
	,ncusto
	,intid
	,tipo
	,estab
	,pais
	,zona
	,obs
	) 
select 
	substring('F'+inserted.postamp,1,25)
	,inserted.postamp
	,inserted.usrdata, inserted.usrhora, inserted.usrinis, inserted.ousrdata, inserted.ousrhora, inserted.ousrinis
	,'1'
	,'2'
	,inserted.procdata
	,inserted.no
	,inserted.nome
	,8
	,(select cmdesc from cm1 (nolock) where cm=8)
	,'PO'
	,case when inserted.efinv<0 then 0 else inserted.efinv end
	,case when inserted.efinv<0 then 0 else inserted.efinv end
	,case when inserted.finvmoeda<0 then 0 else inserted.finvmoeda end
	,case when inserted.finvmoeda<0 then 0 else inserted.finvmoeda end
	,case when inserted.efinv>0 then 0 else -inserted.efinv end
	,case when inserted.efinv>0 then 0 else -inserted.efinv end
	,case when inserted.finvmoeda>0 then 0 else -inserted.finvmoeda end,case when inserted.finvmoeda>0 then 0 else -inserted.finvmoeda end
	,inserted.moeda
	,inserted.adoc
	,inserted.procdata
	,inserted.fref
	,inserted.ccusto
	,inserted.ncusto
	,inserted.intid
	,inserted.tipo
	,inserted.estab
	,inserted.pais
	,inserted.zona
	,inserted.desc1
from
	inserted
where
	inserted.process=1 
	and inserted.finv<>0

if @@ROWCOUNT>0
	SET @Msg += '<Insert FC (' + convert(varchar,getdate(),8) + ')>'


INSERT INTO FC 
	(
	fcstamp
	,postamp
	,usrdata,usrhora,usrinis,ousrdata,ousrhora,ousrinis
	,formapag
	,situacao
	,datalc
	,no
	,nome
	,cm
	,cmdesc
	,origem
	,edeb
	,edebf
	,ecred
	,ecredf
	,moeda
	,adoc
	,dataven
	,fref
	,ccusto
	,ncusto
	,intid
	,tipo
	,estab
	,pais
	,zona
	,obs
	)
select
	substring('I'+inserted.postamp,1,25)
	,inserted.postamp
	,inserted.usrdata, inserted.usrhora, inserted.usrinis, inserted.ousrdata, inserted.ousrhora, inserted.ousrinis
	,'1'
	,'2'
	,inserted.procdata
	,inserted.no
	,inserted.nome
	,10
	,(select cmdesc from cm1 (nolock) where cm=10)
	,'PO'
	,case when inserted.evirs<0 then 0 else inserted.evirs end
	,case when inserted.evirs<0 then 0 else inserted.evirs end
	,case when inserted.evirs>0 then 0 else -inserted.evirs end
	,case when inserted.evirs>0 then 0 else -inserted.evirs end
	,'EURO',inserted.adoc,inserted.procdata,inserted.fref,inserted.ccusto,inserted.ncusto,inserted.intid,inserted.tipo,inserted.estab,inserted.pais,inserted.zona
	,inserted.desc1
from
	inserted
where
	inserted.process=1
	and (inserted.virs<>0 or inserted.evirs<>0)

if @@ROWCOUNT>0
	SET @Msg += '<Insert FC (' + convert(varchar,getdate(),8) + ')>'

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg);