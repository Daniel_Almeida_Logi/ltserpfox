SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TRIGGER [dbo].[tr_b_utentes_update]
ON [dbo].[B_utentes]
FOR UPDATE
AS

IF (@@ROWCOUNT = 0) 
	RETURN ;

SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE 
	 @Msg varchar(500) = ''
	,@nome varchar(20) = 'tr_b_utentes_update'	

set @Msg += '<' + convert(varchar,getdate(),14) + '>'

If exists (select inserted.utstamp from inserted where inserted.clivd = 0)
BEGIN

	/*
		Atualizar vencimento nos dependentes
	*/
	If exists (select count(*) from inserted inner join deleted on inserted.utstamp = deleted.utstamp where inserted.vencimento != deleted.vencimento and inserted.estab=0 )
	BEGIN
		If (select count(*) from b_utentes inner join inserted on b_utentes.no=inserted.no and b_utentes.estab<>0 and inserted.estab=0 )>0 
		begin
			update
				b_utentes
			set
				b_utentes.vencimento = inserted.vencimento
			from
				inserted
				inner join b_utentes on b_utentes.no = inserted.no
			where
				b_utentes.estab != 0 and inserted.estab = 0

			if @@ROWCOUNT > 0
				SET @Msg += '<Update B_UTENTES (vencimento) (' + convert(varchar,getdate(),8) + ')>'
		end
	END

	/*
		Atualizar limite de credito nos dependentes
	*/
	If exists (select count(*) from inserted inner join deleted on inserted.utstamp=deleted.utstamp where inserted.alimite<>deleted.alimite and inserted.estab=0)
	BEGIN
		If (select count(*) from b_utentes inner join inserted on b_utentes.no=inserted.no and b_utentes.estab<>0 and inserted.estab=0 )>0 
		begin
			update
				b_utentes 
			set 
				b_utentes.alimite=inserted.alimite 
			from
				inserted 
				inner join b_utentes on b_utentes.no = inserted.no
			where 
				b_utentes.estab != 0 and inserted.estab=0 

			if @@ROWCOUNT > 0
				SET @Msg += '<Update B_UTENTES (alimite) (' + convert(varchar,getdate(),8) + ')>'
		end
	END
END

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg) ;

