
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER TRIGGER [dbo].[Tr_St_Update]
ON [dbo].[st]
FOR UPDATE
AS

IF (@@ROWCOUNT = 0 or isnull(Context_Info(),0x0) = 0x55556)
	RETURN;

SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE
	 @Msg varchar(500) = ''
	,@nome varchar(30) = 'Tr_St_Update'


SET @Msg += '<' + convert(varchar,getdate(),14) + '>'

declare @site_nr tinyint
set @site_nr = (select top 1 inserted.site_nr from inserted)
IF @site_nr is null
begin
	set @site_nr = isnull((select top 1 deleted.site_nr from deleted),1)
end 


IF UPDATE(ref) or UPDATE(design)
BEGIN
	
	SET @Msg += '<ref/design (' + convert(varchar,getdate(),8) + ')>'

	-- Update BC
	UPDATE
		bc
	SET
		bc.ref = x.ref
		,bc.design = x.design
	FROM
		bc
		inner join (select inserted.ststamp, inserted.ref, inserted.design 
					from inserted inner join deleted on deleted.ststamp=inserted.ststamp 
					where inserted.ref != deleted.ref or inserted.design != deleted.design
					) x on x.ststamp = bc.ststamp

	if (@@ROWCOUNT > 0)
		SET @Msg += '<Update BC (' + convert(varchar,getdate(),8) + ')>'

	-- Update ST_INATIVOS
	UPDATE
		st_inativos
	SET
		st_inativos.ref = x.ref
	FROM
		st_inativos
		inner join (select inserted.ref
					from inserted inner join deleted on deleted.ststamp=inserted.ststamp 
					where inserted.ref != deleted.ref
					) x on x.ref = st_inativos.ref
	Where
		st_inativos.site_nr = @site_nr
		
		
	if (@@ROWCOUNT>0)
		SET @Msg += '<Update ST_INATIVOS - ref (' + convert(varchar,getdate(),8) + ')>'
END


IF UPDATE(inactivo)
begin
	SET @Msg += '<inativos (' + convert(varchar,getdate(),8) + ')>'

	declare @data date = convert(date,getdate())

	-- Update ST_INATIVOS
	Update
		st_inativos
	Set
		inativo = x.inactivo
	From
		st_inativos sti
		inner join (select inserted.ref, inserted.inactivo 
					from inserted inner join deleted on deleted.ststamp=inserted.ststamp 
					where inserted.inactivo != deleted.inactivo
					) x on x.ref = sti.ref
	Where
		sti.data = @data
		and sti.site_nr = @site_nr

	if (@@ROWCOUNT > 0)
		SET @Msg += '<Update ST_INATIVOS - inativo (' + convert(varchar,getdate(),8) + ')>'

	Insert Into 
		st_inativos
	Select
		x.ref, x.inactivo, @data, @site_nr
	From
		(select inserted.ref, inserted.inactivo 
		from inserted inner join deleted on deleted.ststamp=inserted.ststamp 
		where inserted.inactivo != deleted.inactivo
		) x
		left join st_inativos on st_inativos.ref = x.ref and st_inativos.data = @data
	Where
		st_inativos.ref is null
		and st_inativos.site_nr = @site_nr

	if (@@ROWCOUNT > 0)
		SET @Msg += '<Insert ST_INATIVOS (' + convert(varchar,getdate(),8) + ')>'
END

IF UPDATE(epv1)
BEGIN
	UPDATE st
	SET
		st.epv1_final = (CASE 
						WHEN st.mfornec <> 0 THEN ROUND(st.epv1 - ((st.epv1*st.mfornec)/100),2)
						WHEN st.mfornec2 <> 0 THEN ROUND((st.epv1 - st.mfornec2), 2)
						ELSE st.epv1
					 END)
	FROM 
		st(nolock)
		inner join inserted ON st.ststamp = inserted.ststamp
END

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg);

