
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TRIGGER [dbo].[tr_po_delete]
ON [dbo].[po]
FOR DELETE
AS

IF (@@ROWCOUNT = 0)
	RETURN;
	
SET NOCOUNT ON

-- Cria variaveis para mensagens
DECLARE 
	 @Msg varchar(100) = ''
	,@nome varchar(20) = 'Tr_Po_Delete'	

SET @Msg += '<' + convert(varchar,getdate(),14) + '>'

DELETE
	fc
from
	deleted
where
	fc.postamp=deleted.postamp

if @@ROWCOUNT > 0
	SET @Msg += '<Update FC (' + convert(varchar,getdate(),8) + ')>'

-- Caso seja a BD de desenvolvimento guarda registo dos triggers executados
IF OBJECT_ID('tempdb.dbo.##Debug_Trigger2') IS NOT NULL
	INSERT INTO [dbo].[##Debug_Trigger2]([nome],[Mensagem]) VALUES(@nome,@Msg);