/* Correr sql em todas as bds que tenham site para ativar e inativar camapnhas

    exec sp_enable_disable_campaigns

*/
Use master

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


if OBJECT_ID('[dbo].[sp_enable_disable_campaigns]') IS NOT NULL
    drop procedure sp_enable_disable_campaigns
go


create PROCEDURE sp_enable_disable_campaigns
     
/* WITH ENCRYPTION */
AS
SET NOCOUNT ON
    
    DECLARE @DB_Name varchar(100) 
    DECLARE @Command nvarchar(max) 
    DECLARE @sql_ nvarchar(max)
	DECLARE @sqlToVerifySite nvarchar(max) 
	DECLARE @hasSite int   -- verify if has site
	DECLARE @ParmDefinition nvarchar(500);
	DECLARE @product_ref varchar(100) -- product ref on for
	DECLARE @product_site varchar(100) -- product site


    DECLARE database_cursor CURSOR FOR 
    SELECT name 
    FROM MASTER.sys.sysdatabases 
    where (name like 'F%' or name like 'ltdev30') and len(name) <= 7 

	/* Cursor to go through all databases in server (or in defined databases) */
    OPEN database_cursor 

    FETCH NEXT FROM database_cursor INTO @DB_Name 

    WHILE @@FETCH_STATUS = 0 
    BEGIN 
		
		 /* The ADM0000000114 is used to notifications on Logitools. Notfications comming from site. So if DB has this value defined has site.  */
		 set @sqlToVerifySite = N'Select @retvalOUT=count(stamp) FROM ' + @DB_Name + '.dbo.B_Parameters_site (nolock) where stamp=''ADM0000000114'' and textValue is not null and ltrim(rtrim(textValue)) <> '''''
		 SET @ParmDefinition = N'@retvalOUT int OUTPUT';
		 EXEC sp_executesql @sqlToVerifySite, @ParmDefinition, @retvalOUT=@hasSite OUTPUT

		/* Verify if has site */
		if (@hasSite > 0)
			BEGIN
				/* Has site */
				print 'Has site'
				print 'DB Name -> ' + @DB_Name
				/* Verify if has campaigns to enable or disable*/
				DECLARE @verIfyHasRefsToActive int
				DECLARE @verIfyHasRefsToInactive int
				Declare @sqlToverifyCampaigns nvarchar(max)

				Declare @sqlToGetRefs nvarchar(max)
				DECLARE @refsToUpdate Table(ref VARCHAR(20), site_nr VARCHAR(20))
				DELETE FROM @refsToUpdate

				set @sqlToGetRefs = N'Select CONVERT(NVARCHAR(max), ref) as ref, site_nr from ' + @DB_Name + '.dbo.campanhas_online_st (nolock) where ref not in (''RECEITADIG'',''SERVPORTES'', ''XXXXXXX'') and activo = 0 and ref is not null and idTipoCampanha in (''1'',''2'',''3'') and CAST(dataInicio AS DATE) <= CAST(getdate() AS DATE)'
				Insert Into @refsToUpdate EXEC sp_executesql @sqlToGetRefs

				if Exists (SELECT TOP (1) NULL FROM @refsToUpdate)
				BEGIN
					/* Has campaigns to enable */
					print 'Has campaings to enable'

					Declare @sqlActiveCampaigns nvarchar(max)
					Declare @sqlUpdateSt nvarchar(max)

					/* Cursor to go through the refs to enable */
					Declare refs_cursor Cursor For Select * from @refsToUpdate

					Open refs_cursor;

					FETCH NEXT FROM refs_cursor INTO @product_ref, @product_site 

					WHILE @@FETCH_STATUS = 0
						BEGIN
						print 'Product ref -> ' + @product_ref
						print 'Product site_nr -> ' + @product_site
						/* Enable campaigns */
						set @sqlActiveCampaigns = N'Update ' + @DB_Name + '.dbo.campanhas_online_st set activo=1, usrdata=getdate(), usrhora=convert(varchar,getdate(),108) where activo = 0 and ref='''+ @product_ref + ''' and site_nr=''' + @product_site + ''' and idTipoCampanha in (''1'',''2'',''3'') and CAST(dataInicio AS DATE) <= CAST(getdate() AS DATE)'
						print @sqlActiveCampaigns
						EXEC sp_executesql @sqlActiveCampaigns

						/* Update Ref to site synchronize */
						set @sqlUpdateSt = N'Update ' + @DB_Name + '.dbo.st set usrdata=getdate(), usrhora=convert(varchar,getdate(),108) where ref='''+ @product_ref + ''' and site_nr=''' + @product_site + ''''
						print @sqlUpdateSt
						EXEC sp_executesql @sqlUpdateSt

						FETCH NEXT FROM refs_cursor INTO @product_ref, @product_site
						END
						CLOSE refs_cursor;
						DEALLOCATE refs_cursor;
					

				END /* End if to verify if has campaigns to enable */

				/* Verify if has campaigns to disable */

				/* Clear temp db */
				DELETE FROM @refsToUpdate

				set @sqlToGetRefs = N'Select CONVERT(NVARCHAR(max), ref) as ref, site_nr from ' + @DB_Name + '.dbo.campanhas_online_st (nolock) where ref not in (''RECEITADIG'',''SERVPORTES'', ''XXXXXXX'') and activo = 1 and ref is not null and idTipoCampanha in (''1'',''2'',''3'') and CAST(dataFim AS DATE) <= CAST(getdate() AS DATE) and CAST(dataFim AS DATE) != ''1900-01-01'' '
				print @sqlToGetRefs
				Insert Into @refsToUpdate EXEC sp_executesql @sqlToGetRefs

				if Exists (SELECT TOP (1) NULL FROM @refsToUpdate)
				Begin
					/* Has campaigns to disable */
					print 'Has campaigns to disable'

					/*Select * from @refsToUpdate*/
					Declare refs_cursor Cursor For Select * from @refsToUpdate

					Open refs_cursor;

					FETCH NEXT FROM refs_cursor INTO @product_ref, @product_site

					WHILE @@FETCH_STATUS = 0
						BEGIN
						print 'Product ref -> ' + @product_ref
						print 'Product site_nr -> ' + @product_site
						/* Disable campaigns */
						set @sqlActiveCampaigns = N'Update ' + @DB_Name + '.dbo.campanhas_online_st set activo=0, idTipoCampanha = 4, valorOferta = 0, valorCompra = 0, usrdata=getdate(), usrhora=convert(varchar,getdate(),108) where activo = 1 and ref='''+ @product_ref + ''' and site_nr=''' + @product_site + ''' and idTipoCampanha in (''1'',''2'',''3'') and CAST(dataFim AS DATE) <= CAST(getdate() AS DATE) and CAST(dataFim AS DATE) != ''1900-01-01'''
						print @sqlActiveCampaigns
						EXEC sp_executesql @sqlActiveCampaigns

						/* Update refs disabled */
						set @sqlUpdateSt = N'Update ' + @DB_Name + '.dbo.st set usrdata=getdate(), usrhora=convert(varchar,getdate(),108) where ref='''+ @product_ref + ''' and site_nr=''' + @product_site + ''''
						print @sqlUpdateSt
						EXEC sp_executesql @sqlUpdateSt

						FETCH NEXT FROM refs_cursor INTO @product_ref, @product_site
						END
						CLOSE refs_cursor;
						DEALLOCATE refs_cursor;

				End /* End refs to disable */

			END /* End if has site */
		ELSE
			BEGIN
			    /* Doesn't has site */
				print 'Doesn t has site'
				print 'DB Name -> ' + @DB_Name
			END

         FETCH NEXT FROM database_cursor INTO @DB_Name 
    END 

    CLOSE database_cursor 
    DEALLOCATE database_cursor 
    
GO
Grant Execute On sp_enable_disable_campaigns to Public
Grant Control On sp_enable_disable_campaigns to Public
GO
