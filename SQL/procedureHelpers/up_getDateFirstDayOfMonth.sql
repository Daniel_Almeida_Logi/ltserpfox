/* SP retorna PRIMEIRO DIA DO MES do dia da semana que querem 
	
	up_getDateFirstDayOfMonth  '2023-03-26 21:30:05.000','MONDAY'
	up_getDateFirstDayOfMonth  '2023-12-26 21:30:05.000','SUNDAY'
	up_getDateFirstDayOfMonth  '2024-01-26 21:30:05.000','SATURDAY'
	up_getDateFirstDayOfMonth  '2024-01-26 21:30:05.000','monday'
	up_getDateFirstDayOfMonth  '2024-01-01','monday'
	up_getDateFirstDayOfMonth  '2024-01-01','monday'
*/

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

if OBJECT_ID('[dbo].[up_getDateFirstDayOfMonth]') IS NOT NULL
	DROP PROCEDURE dbo.up_getDateFirstDayOfMonth
GO

CREATE PROCEDURE dbo.up_getDateFirstDayOfMonth
	 @input_date	datetime
	,@weekday  varchar(30)
AS

SET NOCOUNT ON

	DECLARE @StartDate  date = DATEADD(mm, DATEDIFF(mm,0,@input_date), 0);

	DECLARE @CutoffDate date = DATEADD(DAY, -1, DATEADD(DAY, 7, @StartDate));

	;WITH seq(n) AS 
	(
	  SELECT 0 UNION ALL SELECT n + 1 FROM seq
	  WHERE n < DATEDIFF(DAY, @StartDate, @CutoffDate)
	),
	d(d) AS 
	(
	  SELECT DATEADD(DAY, n, @StartDate) FROM seq
	)
	SELECT d as date FROM d
	WHERE UPPER(Datename(dw,d))=UPPER(@weekday)
	ORDER BY d


GRANT EXECUTE on dbo.up_getDateFirstDayOfMonth TO PUBLIC
GRANT Control on dbo.up_getDateFirstDayOfMonth TO PUBLIC
GO


