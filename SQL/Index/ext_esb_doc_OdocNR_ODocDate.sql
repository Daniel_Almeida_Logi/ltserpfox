IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ext_esb_doc]') AND name = N'IX_ext_esb_doc_OdocNR_ODocDate')
CREATE NONCLUSTERED INDEX [IX_ext_esb_doc_OdocNR_ODocDate] ON [dbo].[ext_esb_doc]
(
    [odocNR] ASC,
	[oDocDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO