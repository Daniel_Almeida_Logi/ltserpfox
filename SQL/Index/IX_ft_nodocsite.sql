/****** Object:  Index [IX_ft_no]    Script Date: 09/05/2017 17:36:11 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ft]') AND name = N'IX_ft_nodocsite')
CREATE NONCLUSTERED INDEX [IX_ft_nodocsite] ON [dbo].[ft]
(
    [ndoc],[site] ASC
)INCLUDE ([ftstamp],[no],[fdata],[estab])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO