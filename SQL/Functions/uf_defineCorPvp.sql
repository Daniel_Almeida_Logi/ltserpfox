/*
	Function que retorna a data da ultima venda do produto 

	SELECT dbo.uf_defineCorPvp( '8113837',5.06)
	SELECT dbo.uf_defineCorPvp( '5440987',2.79)
	SELECT dbo.uf_defineCorPvp( '5440987',1)
*/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT *
           FROM   sys.objects
           WHERE  object_id = OBJECT_ID(N'[dbo].[uf_defineCorPvp]')
		   )
    drop FUNCTION dbo.uf_defineCorPvp

GO

CREATE FUNCTION [dbo].[uf_defineCorPvp] 
(	
	@ref		VARCHAR(254),
	@preco		numeric(14,4)
)
RETURNS varchar(254)
AS
BEGIN
	
	DECLARE @cor varchar(20)
	DECLARE @tabela table(ref varchar(20), preco numeric(14,4), data date, dataFim date)

	insert into @tabela
	select 
		ref
		,preco
		,data
		,data_fim
	from 
		hist_precos (nolock)
	where 
		ref = @ref
		and (id_preco=1 or id_preco=602)
		and ativo = 1
	ORDER BY data DESC

	set @cor = case when NOT exists(SELECT 1 FROM @tabela WHERE preco = @preco)
					THEN '255,0,0'
					ELSE CASE WHEN exists(SELECT 1 FROM @tabela WHERE ROUND(preco,2) = ROUND(@preco,2) 
												AND (dataFim IS NULL OR dataFim = '19000101' OR dataFim = '30000101'
													 OR	convert(date,GETDATE()) < convert(date,dataFim)))
							THEN '255,255,255'
							ELSE '241,196,15'
							END
					END

	RETURN @cor
END
GO

