SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[uf_retorna_valorizacao_produto]') IS NOT NULL
	drop function dbo.uf_retorna_valorizacao_produto
go


CREATE function [dbo].uf_retorna_valorizacao_produto (@epcpond numeric(19,6), @epcult numeric(19,6), @epcusto numeric(19,6) ) 
RETURNS NUMERIC(19,6)
AS
BEGIN		
	Declare @result  NUMERIC(19,6)
	
	IF 	@epcpond > 0
		BEGIN
			set @result = @epcpond
		END	
	ELSE IF @epcult > 0
		BEGIN
			set @result = @epcult
		END		
	ELSE IF @epcusto > 0
		BEGIN
			SET @result = @epcusto
		END	
	ELSE
		BEGIN
			SET	 @result = 0
		END	

	RETURN @result	

END