/*
*/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT *
           FROM   sys.objects
           WHERE  object_id = OBJECT_ID(N'[dbo].[uf_cambioEntreMoedas]')
                  AND type IN ( N'FN', N'IF', N'TF', N'FS', N'FT' ))
    drop FUNCTION dbo.uf_cambioEntreMoedas

GO

CREATE FUNCTION uf_cambioEntreMoedas(@moedaOrigem VARCHAR(3),@moedaDestino Varchar(3),@data DATETIME) RETURNS NUMERIC(19,12)

AS
BEGIN
  
	Declare @cambio Float
	
	set @cambio = 
			(SELECT TOP 1
				cambio
			FROM CB(nolock)
			WHERE CB.moedaOrigem= @moedaOrigem and CB.moeda = @moedaDestino
					AND DATA <= @data
			ORDER BY [data] DESC,
					 [usrdata] DESC,
					 [usrhora] DESC)

   RETURN @cambio
END

