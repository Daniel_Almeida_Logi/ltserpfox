/*
	Function que retorna a data da ultima venda do produto 

	exec uf_stocksUltimaVenda '5440987','Loja 1'

*/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT *
           FROM   sys.objects
           WHERE  object_id = OBJECT_ID(N'[dbo].[uf_stocksUltimaVenda]')
		   )
    drop FUNCTION dbo.uf_stocksUltimaVenda

GO

CREATE FUNCTION [dbo].[uf_stocksUltimaVenda] 
(	
	@ref		VARCHAR(254),
	@site		VARCHAR(254)
)
RETURNS varchar(254)
AS
BEGIN

	DECLARE @dataUtlVenda DATETIME
	
	SET @dataUtlVenda =(
		SELECT top 1 
				sl.datalc
		FROM sl(NOLOCK) 
			INNER JOIN fi (nolock) on fi.fistamp = sl.fistamp
		WHERE 
			sl.ref = @ref 
			AND sl.armazem IN (SELECT  DISTINCT
									empresa_arm.armazem		AS armazem
								FROM empresa
								INNER JOIN empresa_arm on empresa.no = empresa_arm.empresa_no
								WHERE empresa.site = @site )
			AND cm>50 
			AND sl.qtt > 0 
			AND fi.tipodoc = 1
		ORDER BY datalc desc
	)

	SET @dataUtlVenda = ISNULL(@dataUtlVenda,'19000101')

	RETURN @dataUtlVenda
END
GO

