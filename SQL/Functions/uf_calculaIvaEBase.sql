-- =============================================
-- Author:		Daniel Almeida 
-- Create date: 2023-10-12
-- Description: Retorna o valor do Iva e Base de incidencia
-- SELECT ValorBaseIncidencia, ValorIVA
-- FROM CalcularIVA(1234.56, 23.00)
-- 
IF EXISTS (SELECT *
           FROM   sys.objects
           WHERE  object_id = OBJECT_ID(N'[dbo].[uf_calculaIvaEBase]'))
    drop FUNCTION dbo.uf_calculaIvaEBase 
go

CREATE FUNCTION uf_calculaIvaEBase
(
    @valorComIVA DECIMAL(10, 2),
    @taxaIVA DECIMAL(5, 2)
)
RETURNS TABLE
AS
RETURN
(
    SELECT
        @valorComIVA / (1 + (@taxaIVA / 100)) AS ValorBaseIncidencia,
        @valorComIVA - (@valorComIVA / (1 + (@taxaIVA / 100))) AS ValorIVA
)


