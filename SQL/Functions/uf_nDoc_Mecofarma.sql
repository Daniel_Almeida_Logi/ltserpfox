/**
 select * from uf_nDoc_Mecofarma ('ADM5F1212C6-629E-47EA-8D6')
**/
-- SELECT * FROM FT ORDER BY ousrdata + ousrhora DESC



-- SELECT OFISTAMP,* FROM FI WHERE FTSTAMP='ADM3D63822E-4768-425C-AC1'
-- 
-- SELECT  OFISTAMP,* FROM FI WHERE FTSTAMP='ADM90B16687-3EEB-41B4-9A7'


--code to validate if the function exists, if exists, drops function
IF EXISTS (SELECT *
           FROM   sys.objects
           WHERE  object_id = OBJECT_ID(N'[dbo].[uf_nDoc_Mecofarma]'))
    drop FUNCTION dbo.uf_nDoc_Mecofarma 
go
-- =============================================
-- Author:		Pedro Moreira
-- Create date: 2023-12-18
-- Description: Retorna N� DOcumento Mecofarma

/*
nreferencial	= isnull(td.nmdocext+'-'+empresa.siteext+'-'+substring(convert(varchar(4),ft.ftano),3,2)+'-'+convert(varchar(6),format(ft.fno, '000000')),''),
*/
 
CREATE FUNCTION [dbo].[uf_nDoc_Mecofarma]
(	
	@ftstamp varchar(25)
	
)
RETURNS @ndoc TABLE (
	ndoc varchar(100),
	nr_receita varchar(100),
	nr_atendimento varchar(100),
	nr_cli varchar(100),
	nr_cartao varchar(100)
)
AS
begin
	
insert into 
	@ndoc
select 
		ndoc	= 	td.tiposaft+'/' + convert(varchar(10),ft.ndoc)+'/' +  convert(varchar(10),ft.fno	)+'/'+  convert(varchar(10),ft.ftano)	
		,ft2.u_receita
		,ft.u_nratend
		,isnull(ft2.u_nbenef, '''') as nr_cli
		,ft2.u_nbenef2 as nr_cartao
	from 
		ft(nolock)
		INNER JOIN FI		(nolock)		ON FT.ftstamp = FI.ftstamp  AND fi.qtt !=0
		left outer join ft2(nolock)			on ft2.ft2stamp = ft.ftstamp
		left outer join td(nolock)			on td.ndoc = ft.ndoc
		left outer join empresa (nolock)	on ft.site = empresa.site
	where
		ft.ftstamp = @ftstamp
	 group by 
		td.tiposaft+'/' + convert(varchar(10),ft.ndoc) +'/' +  convert(varchar(10),ft.fno	)	, 
		ft2.u_receita , 
		ft.u_nratend , 
		isnull(ft2.u_nbenef, '''') , 
		ft2.u_nbenef2,
		ft.ftano



	RETURN
end