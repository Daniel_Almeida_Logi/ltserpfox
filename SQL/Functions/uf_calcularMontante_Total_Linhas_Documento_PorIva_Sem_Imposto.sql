/*
	 select * from uf_calcularMontante_Total_Linhas_Documento_PorIva_Sem_Imposto ('ADM361FBA33-E2FA-49A6-B39', 'Fi')
*/
--code to validate if the function exists, if exists, drops function
IF EXISTS (SELECT *
           FROM   sys.objects
           WHERE  object_id = OBJECT_ID(N'[dbo].[uf_calcularMontante_Total_Linhas_Documento_PorIva_Sem_Imposto]'))
    drop FUNCTION dbo.uf_calcularMontante_Total_Linhas_Documento_PorIva_Sem_Imposto 
go
-- =============================================
-- Author:		José Moreira
-- Create date: 2022-11-07
-- Description: Returns the total to pay value for the document
Create FUNCTION [dbo].[uf_calcularMontante_Total_Linhas_Documento_PorIva_Sem_Imposto]
(	
	@stamp varchar(25),
	@tabela varchar(2)
)
RETURNS @montanteTotalLinhas TABLE (
	valor numeric(30,2))
AS
begin	
	declare @valor numeric(30, 2)
	
	if @tabela = 'ft'
	begin
		select @valor = STR(round(abs(sum(case when 
											(case when 
												fi.ivaincl = 1 
											then 
												(fi.etiliquido / (fi.iva/100+1)) 
											else 
												fi.etiliquido 
											end) < 0 
										then 
											(case when 
												fi.ivaincl = 1 
											then 
												(fi.etiliquido / (fi.iva/100+1))
											else 
												fi.etiliquido 
											end)
									else
										(fi.etiliquido / (fi.iva/100+1))
									end
						)),2),10,2)
		from 
			fi (nolock)
		where 
			ftstamp = @stamp 		
	end
	if @tabela = 'fi'
	begin
		select @valor = STR(round(abs(sum(case when 
											(case when 
												fi.ivaincl = 1 
											then 
												(fi.etiliquido / (fi.iva/100+1)) 
											else 
												fi.etiliquido 
											end) < 0 
										then 
											0
									else
										(case when 
												fi.ivaincl = 1 
											then 
												(fi.etiliquido / (fi.iva/100+1))
											else 
												fi.etiliquido 
											end)
									end
						)),2),10,2)
		from 
			fi (nolock)
		where 
				ftstamp = (select ftstamp from fi (nolock) where fi.fistamp = @stamp)
				and 	fi.ref in (select fi.ref from fi (nolock) where fi.fistamp = @stamp) 
		group by ref
	end

	insert into @montanteTotalLinhas (valor)
		values ( @valor)

	return 
end
