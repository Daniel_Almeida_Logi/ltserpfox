/**
 select * from uf_calcularDescontosPorStamp ('ADM361FBA33-E2FA-49A6-B39', 'FT')
**/

--code to validate if the function exists, if exists, drops function
IF EXISTS (SELECT *
           FROM   sys.objects
           WHERE  object_id = OBJECT_ID(N'[dbo].[uf_calcularDescontosPorStamp]'))
    drop FUNCTION dbo.uf_calcularDescontosPorStamp 
go
-- =============================================
-- Author:		José Moreira
-- Create date: 2022-11-07
-- Description: Returns the total discount value for the document
CREATE FUNCTION [dbo].[uf_calcularDescontosPorStamp]
(	
	@stamp varchar(25),
	@tabela varchar(2)
)
RETURNS @tdescont TABLE (
	tdescont numeric(30,2))
AS
begin 
	if @tabela = 'FT'
	begin
		insert into @tdescont
		select STR(round(abs(sum(
			case when 
				(convert(numeric, fi.desconto)) > 0 or (convert(numeric, fi.desc2)) > 0 or (convert(numeric, fi.u_descval)) > 0
			then 
				abs(	
						abs(case when  fi.ivaincl = 1  then  (fi.epv * fi.qtt) / (fi.iva/100+1) else  fi.epv * fi.qtt end)
						- abs(case when fi.ivaincl = 1 then fi.etiliquido / (fi.iva/100+1) else fi.etiliquido / fi.qtt end))
			else 
				null
			end)),2),10,2)
		from
			fi (nolock)
		where
			ftstamp = @stamp 
			and ref != ''
	 end 
	 return
end
