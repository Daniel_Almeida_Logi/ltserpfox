-- =============================================
-- Author:		Vitor Teixeira
-- Create date: 2023/09/29
-- Description: Informa se a vacina j� foi faturada ou n�o ou se esta se aplica 
-- Example: 

  -- @cptorg - nome abreviado de quem esta a fazer o pedido 
  -- @codeVacine - codigo da vacina
  -- @nrReceita - nr da receita com que a vacina foi administrada 

-- =============================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


if OBJECT_ID('[dbo].uf_vacinacaoVerificaFaturado') IS NOT NULL
begin
	drop function dbo.uf_vacinacaoVerificaFaturado
end
go


CREATE FUNCTION dbo.uf_vacinacaoVerificaFaturado(@cptorg VARCHAR(15), @codeVacine VARCHAR(60), @nrReceita VARCHAR(254),
				 @tipoReg VARCHAR(50))
	RETURNS VARCHAR(3)
	AS
	BEGIN
	
	DECLARE @faturado VARCHAR(3)
	DECLARE @resQuery VARCHAR(254)

	IF ((@codeVacine = 'GripeSNS' or @codeVacine like 'covid%') AND @tipoReg LIKE '%E-boletim%')
	BEGIN
		SET @resQuery =(SELECT TOP 1
				ctltrct.receita
			FROM 
				B_vacinacao(NOLOCK)
			INNER JOIN 
				ctltrct(NOLOCK) ON ctltrct.receita = B_vacinacao.correlationId
			INNER JOIN
				ft2(NOLOCK) ON ft2.u_receita = ctltrct.receita AND ft2.u_receita != ''
			WHERE 
			tipoReg = @tipoReg
			AND cptorgabrev = @cptorg
			AND codeVacina= @codeVacine
			AND ctltrct.receita = @nrReceita
			AND @nrReceita != ''
			ORDER BY 
				B_vacinacao.ousrdata DESC)
	
		IF (ISNULL(@resQuery,'') = '')
		BEGIN
			SET @faturado = 'N�o'
		END
		ELSE
		BEGIN
			SET @faturado = 'Sim'
		END

			RETURN @faturado
	END
	ELSE
	BEGIN
		SET @faturado = 'N/A'
		RETURN @faturado
	END
	RETURN @faturado
	END
GO