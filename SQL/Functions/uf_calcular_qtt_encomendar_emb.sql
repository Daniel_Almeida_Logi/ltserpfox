/**
 select * from uf_calcular_qtt_encomendar_emb ('11', '1', '5440987','2')
**/

--code to validate if the function exists, if exists, drops function
IF EXISTS (SELECT *
           FROM   sys.objects
           WHERE  object_id = OBJECT_ID(N'[dbo].[uf_calcular_qtt_encomendar_emb]'))
    drop FUNCTION dbo.uf_calcular_qtt_encomendar_emb 
go
-- =============================================
-- Author:		Pedro Moreira
-- Create date: 2023-12-14
-- Description: Retorna a quantidade a encomendar por multiplos da venda 
CREATE FUNCTION [dbo].[uf_calcular_qtt_encomendar_emb]
(	
	@Value numeric(4),
	@Site_nr varchar(60),
	@ref varchar(25),
	@tpArredondamento numeric(1)
	
)
RETURNS @qtt TABLE (
	qtt_encomendar numeric(4)
)
AS
begin
	DECLARE
		@UsaQttEmb bit = 1
		,@QttEmbArredonda numeric(1,0) = 0 
		,@Site varchar(60) = ''
	/*
		@QttEmbArredonda= 1 (Arredonda para baixo)
		@QttEmbArredonda= 2 (Arredonda para Cima)
	*/
	
	select 
		@Site = empresa.site
	from empresa(nolock)
	where empresa.no = @Site_nr

	select 
		@UsaQttEmb = B_Parameters_site.bool,
		@QttEmbArredonda = (case when @tpArredondamento = 0
								then
									B_Parameters_site.numValue
								ELSE
									@tpArredondamento
								END	
									)
	from B_Parameters_site(nolock)
	where 1 = 1
		and  B_Parameters_site.site = @Site
		and B_Parameters_site.stamp = 'ADM0000000218'


insert into 
	@qtt
select 
	(case
						when @UsaQttEmb = 1 and st.qttembenc > 0
						then
						IIF(@Value%st.qttembenc = 0
								, iif(@Value = 0, 1, @Value)
								, iif(@QttEmbArredonda = 1
									,(round((@Value/st.qttembenc),0,1) * st.qttembenc)
										
									,(round((@Value/st.qttembenc),0,1 ) * st.qttembenc)+ st.qttembenc
										 
									)
							)	
						else
							iif(@Value = 0, 1, @Value)
						end  
				)as eoq

	from 
		st(nolock)
	where
		st.ref = @ref
		and st.site_nr = @Site_nr
	
	RETURN
end