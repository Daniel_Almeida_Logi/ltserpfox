
/*

select  dbo.up_check_nif('123456789') AS NIF_Validado		 /*ok*/
select  dbo.up_check_nif('214898466') AS NIF_Validado		/*ok*/
select  dbo.up_check_nif('214898466k') AS NIF_Validado		/*ok*/
select  dbo.up_check_nif('21489846k6') AS NIF_Validado		/*wrong --> ''*/
select  dbo.up_check_nif('414898466') AS NIF_Validado		/*wrong --> ''*/
select  dbo.up_check_nif('21489846') AS NIF_Validado		/*wrong --> ''*/
select  dbo.up_check_nif('214898462') AS NIF_Validado		/*wrong --> ''*/

select ncont as NIF, dbo.up_check_nif(ncont) AS NIF_Validado from b_utentes 
*/

/*
Algoritmo

* NIF tem 9 numeros
		* len = 9
		* must be numeric
* o primeiro digito n�o pode ser (0,4) 

* o 9� numero � usado para controlo

* os 8 primeiros numeros s�o multiplicados desde o primeiro por 9 em ordem drecrescente e somado tudo:
			--------------------------
	NIF:	x1 x2 x3 x4 x5 x6 x7 x8 x9
			--------------------------

	x1..x8 --> result = x1*9 + x2*8 + x3*7 + x4*6 + x5*5 + x6*4 + x7*3 + x8*2
				
				result = result . mod 11  (resto da divis�o do result por 11)

				if result in (0,1) --> digito_controlo = 0
				else			   --> digito_controlo = 11 - result

				if digito_controlo = x9 --> NIF OK
				else					--> NIF Errado
*/


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[up_check_nif]') IS NOT NULL
	drop function dbo.up_check_nif
go


CREATE function [dbo].up_check_nif (@nif_input varchar(9)) 
RETURNS varchar(9) 
AS
BEGIN		

	declare @j int = 9
	declare @i int = 1
	declare @total int = 0
	declare @digit_control int 
	declare @result varchar(9)
	-- nif_pri_digito	= '1,2,3,5,6,7,8,9'

	IF  LEN(@nif_input) = 9 AND @nif_input NOT LIKE '%[^0-9]%' AND LEFT(@nif_input,1) not in (0,4)		
	BEGIN	

		--os primeiros 8 digitos do nif sao multiplicados por [9,8,7,6,5,4,3,2] e por fim tudo somado
		WHILE @i < LEN(@nif_input)	
		BEGIN
   				SET @total = @total + SUBSTRING(@nif_input,@i,1) * @j				
   				SET @j = @j-1 
				SET @i = @i+1
		END				
	
		IF (@total % 11) = 0 OR (@total % 11) = 1 
			SET @digit_control = 0			
		ELSE			
			SET @digit_control = 11 - (@total % 11) 
		
		IF @digit_control = RIGHT(@nif_input,1)									
			SET @result = @nif_input	/* nif v�lido */		
		ELSE					
			SET @result = ''			/* nif inv�lido */	
	END
	ELSE

			SET @result = ''			/* nif inv�lido */	
			
	RETURN @result	

END