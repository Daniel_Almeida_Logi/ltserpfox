-- =============================================
-- Author:		Daniel  Almeida
-- Create date: 2023/09/26
-- Description: Calcula arrendodamento segundo o algoritmo bankersRound
-- Example: 

  --declare @val decimal(10,4);
  --set @val = 1.235;
  --select @val as Value, round(@val, 2) as Round, dbo.fnBankersRound(@val) as BankRound


  --GO

  --declare @val decimal(10,4);
  --set @val = 1.245;
  --select @val as Value, round(@val, 2) as Round, dbo.fnBankersRound(@val) as BankRound

-- =============================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


if OBJECT_ID('[dbo].uf_bankersRound') IS NOT NULL
begin
	drop function dbo.uf_bankersRound
end
go


CREATE FUNCTION dbo.uf_bankersRound(@Num decimal(10,4))
	RETURNS decimal(19,2)
	AS
	BEGIN
	RETURN CASE
			WHEN ROUND(@Num, 2) - @Num = .005 AND ROUND(@Num, 2) % .02 <> 0
			THEN ROUND(@Num, 2) - 0.01
			ELSE ROUND(@Num, 2) 
		END
	END

GO

