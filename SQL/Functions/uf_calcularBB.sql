-- =============================================
-- Author:        Daniel Almeida 
-- Create date:   2024-08-07
-- Description:   Calcula o benef�cio bruto (marg4 da st)
-- 
-- Exemplo de uso:
-- SELECT dbo.uf_calcularBB('5440987', 1) AS BeneficioBruto;
-- =============================================

IF EXISTS (SELECT *
           FROM   sys.objects
           WHERE  object_id = OBJECT_ID(N'[dbo].[uf_calcularBB]') AND type = 'FN')
    DROP FUNCTION dbo.uf_calcularBB;
GO

CREATE FUNCTION dbo.uf_calcularBB
(
    @ref VARCHAR(18),
    @site_nr INT
)
RETURNS DECIMAL(16, 2)
AS
BEGIN
    DECLARE @beneficio_bruto DECIMAL(16, 2);
	DECLARE @calcAttr varchar(100) = ''
	DECLARE @site varchar(100) = ''


	select 
		@calcAttr=rtrim(ltrim(textValue))    
	from 
		B_Parameters (nolock) 
	where stamp = 'ADM0000000278'
	


	set @calcAttr = lower(rtrim(ltrim(isnull(@calcAttr,'pcp'))))

	if(@calcAttr!='pcp' and @calcAttr!='pct' and @calcAttr!='pcl' )
		set @calcAttr = 'pcp'
	

    -- Calcula o benef�cio bruto com base nos dados das tabelas
    SELECT @beneficio_bruto = CASE 
							
                                WHEN st.epv1 > 0 and (@calcAttr='pcp')  THEN 
                                    ROUND((st.epv1 / (taxasiva.taxa / 100 + 1)) - ROUND(st.epcpond, 2), 2)
									
                                WHEN st.epv1 > 0 and (@calcAttr='pct')  THEN 
                                    ROUND((st.epv1 / (taxasiva.taxa / 100 + 1)) - ROUND(st.epcusto, 2), 2)

								 WHEN st.epv1 > 0 and (@calcAttr='pcl')  THEN 
                                    ROUND((st.epv1 / (taxasiva.taxa / 100 + 1)) - ROUND(st.epcult, 2), 2)
                                ELSE 
                                    0 
                              END
    FROM st (NOLOCK)
    INNER JOIN taxasiva (NOLOCK) ON taxasiva.codigo = st.tabiva
    WHERE st.ref = @ref
      AND st.site_nr = @site_nr;

    RETURN @beneficio_bruto;
END;
GO
