IF EXISTS (SELECT *
           FROM   sys.objects
           WHERE  object_id = OBJECT_ID(N'[dbo].[uf_RemoveSpecialChars]'))
    drop FUNCTION dbo.uf_RemoveSpecialChars 
go
-- =============================================
-- Author:		Pedro Oliveira
-- Create date: 2023-06-05
-- Description: Remove todos os caracteres especiais de uma string

CREATE FUNCTION dbo.uf_RemoveSpecialChars(@str nvarchar(max))  
	RETURNS VARCHAR(max) AS
BEGIN
RETURN cast(
    replace((
        replace(@str collate Latin1_General_CS_AS, '�' collate Latin1_General_CS_AS, 'OE' collate Latin1_General_CS_AS) 
    ) collate Latin1_General_CS_AS, '�' collate Latin1_General_CS_AS, 'oe' collate Latin1_General_CS_AS) as varchar(max)
) collate SQL_Latin1_General_Cp1251_CS_AS 
END