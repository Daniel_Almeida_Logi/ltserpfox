/****** 

Object:  UserDefinedFunction [dbo].[up_SplitToTableCId]   
 Script Date: 14/03/2023 16:47:36 
 
 

 declare @teste varchar(100) = '1 ; 2; 3 ;4'
 select * from dbo.up_SplitToTableCId(@teste,';')
 
 ******/

if OBJECT_ID('[dbo].[up_SplitToTableCId]') IS NOT NULL
	drop function dbo.up_SplitToTableCId
go

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE FUNCTION [dbo].[up_SplitToTableCId](@String varchar(8000), @Delimiter char(1))       
returns @temptable TABLE (id int,items varchar(8000))       
as       
begin       
    declare @idx int       
    declare @i int       
    declare @slice varchar(8000)       
	
	select @i = 0 
    select @idx = 1       
        if len(@String)<1 or @String is null  return       

    while @idx!= 0       
    begin       
		
        set @idx = charindex(@Delimiter,@String)       
        if @idx!=0       
            set @slice = left(@String,@idx - 1)       
        else       
            set @slice = @String       
	
     --   if(len(@slice)>0)  
			select @i = @i+1
            insert into @temptable(id, Items) values(@i, @slice)       

        set @String = right(@String,len(@String) - @idx)       
        if len(@String) = 0 break       
    end   
return       
end  
