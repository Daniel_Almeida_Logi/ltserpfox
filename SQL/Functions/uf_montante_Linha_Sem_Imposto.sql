/**
 select * from uf_montante_Linha_Sem_Imposto ('ADM361FBA33-E2FA-49A6-B39', 'FT')
**/

--code to validate if the function exists, if exists, drops function
IF EXISTS (SELECT *
           FROM   sys.objects
           WHERE  object_id = OBJECT_ID(N'[dbo].[uf_montante_Linha_Sem_Imposto]'))
    drop FUNCTION dbo.uf_montante_Linha_Sem_Imposto 
go
-- =============================================
-- Author:		José Moreira
-- Create date: 2022-11-07
-- Description: Returns the total discount value for the document
CREATE FUNCTION [dbo].[uf_montante_Linha_Sem_Imposto]
(	
	@stamp varchar(25),
	@tabela varchar(2)
)
RETURNS @montanteTotalLinhas TABLE (
	valor numeric(30,2))
AS
begin	
	declare @valor numeric(30, 2)
	
	 
	if @tabela = 'fi'
	begin
		select @valor = STR(round(abs(case when 
												fi.ivaincl = 1 
											then 
												(fi.etiliquido / (fi.iva/100+1))
											else 
												fi.etiliquido 
											end
						),2),10,2)
		from 
			fi (nolock)
		where 
				fistamp = @stamp 
	end

	insert into @montanteTotalLinhas (valor)
		values ( @valor)

	return 
end
