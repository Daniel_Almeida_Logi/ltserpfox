/**
 select * from uf_calcular_Percentagem (15, 35)
**/

--code to validate if the function exists, if exists, drops function
IF EXISTS (SELECT *
           FROM   sys.objects
           WHERE  object_id = OBJECT_ID(N'[dbo].[uf_calcular_Percentagem]'))
    drop FUNCTION dbo.uf_calcular_Percentagem 
go
-- =============================================
-- Author:		José Moreira
-- Create date: 2022-11-07
-- Description: Returns the total discount value for the document
CREATE FUNCTION [dbo].[uf_calcular_Percentagem]
(	
	@part numeric(28, 10),
	@whole numeric(28,10)
)
RETURNS @tpercentagem TABLE(
	percentagemPartInt numeric(18,2),
	percentagemPartVarchar varchar(20),
	percentagemRestoInt numeric(18,2),
	percentagemRestoVarchar varchar(20)
) 
AS
begin 
	declare @p numeric(18,2) = abs(round((@part*100)/@whole,2))
	declare @r numeric(18,2) = abs(round(100 - @p,2))

	insert into @tpercentagem (percentagemPartInt, percentagemPartVarchar, percentagemRestoInt, percentagemRestoVarchar)
		values (@p, convert(varchar, @p) + '%', @r, convert(varchar,@r) + '%')
RETURN  
end