-- Exemplo: 

--select dbo.uf_SiteName_PBI ('F07096A')

-- =============================================

--criar tabela com as bds e sites que são para juntar em PBI. Ex: farmacias GAP para juntar tudo numa só BD. 
--útil depois para juntar TODAS as farmácias numa só BD para o "Repositório LTS"
if OBJECT_ID('[dbo].Sites_PBI') IS NOT NULL
begin
	Drop table Sites_PBI
end
go


CREATE TABLE Sites_PBI (
	BD varchar (10),
	site varchar (50)
	)

Insert Sites_PBI 
values
	('F02098A' , 'Uruguai'),
	('F02095A' , 'SMamede'),
	('F16096A' , 'Charneca'),
	('F16097A' , 'PNovo')


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


if OBJECT_ID('[dbo].uf_SiteName_PBI') IS NOT NULL
begin
	drop function dbo.uf_SiteName_PBI
end
go


CREATE FUNCTION dbo.uf_SiteName_PBI (@id_lt varchar (50))
	RETURNS varchar(50)
	AS
	BEGIN 
	RETURN
		CASE 
			WHEN 
			(select 1 from Sites_PBI where bd = @id_lt) = 1 
				THEN (select site from Sites_PBI where BD = @id_lt)
			ELSE 'site'
		END 
	END
GO