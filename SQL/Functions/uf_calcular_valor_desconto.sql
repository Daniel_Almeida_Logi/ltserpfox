/**
 select * from uf_calcular_valor_desconto ('ADM361FBA33-E2FA-49A6-B39', 'Fi')
**/

--code to validate if the function exists, if exists, drops function
IF EXISTS (SELECT *
           FROM   sys.objects
           WHERE  object_id = OBJECT_ID(N'[dbo].[uf_calcular_valor_desconto]'))
    drop FUNCTION dbo.uf_calcular_valor_desconto 
go
-- =============================================
-- Author:		José Moreira
-- Create date: 2022-11-07
-- Description: Returns the total discount value for the document
CREATE FUNCTION [dbo].[uf_calcular_valor_desconto]
(	
	@STAMP varchar(25),
	@tabela varchar(10)
)
RETURNS @desconto TABLE (
	valor_desconto numeric(28,2)
)
AS
begin
	if @tabela = 'FT'
	begin
		insert into 
			@desconto
		select 
			convert(numeric(18,2), round(abs(ft.fin),2)) 
		from 
			ft (nolock) 
		where 
			ftstamp = @stamp
	end	
	if @tabela = 'FI'
	begin
		insert into 
			@desconto
		select 
			case
				when abs((convert(numeric(28,2),convert(numeric(28,2),fi.u_epvp)*fi.qtt) -   convert(numeric(28,2),fi.etiliquido))) < 0
			then 
				0
			else 
				abs(convert(numeric(28,2),convert(numeric(28,2),fi.u_epvp)*fi.qtt) -   convert(numeric(28,2),fi.etiliquido))
			end
		from
			fi	(nolock)
		where 
			fi.fistamp = @STAMP
	end
	RETURN 
end