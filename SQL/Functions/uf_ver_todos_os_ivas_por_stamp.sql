/**
 select * from uf_ver_todos_os_ivas_por_stamp ('ADM361FBA33-E2FA-49A6-B39', 'FT')
**/

--code to validate if the function exists, if exists, drops function
IF EXISTS (SELECT *
           FROM   sys.objects
           WHERE  object_id = OBJECT_ID(N'[dbo].[uf_ver_todos_os_ivas_por_stamp]'))
    drop FUNCTION dbo.uf_ver_todos_os_ivas_por_stamp 
go
-- =============================================
-- Author:		José Moreira
-- Create date: 2022-11-07
-- Description: Returns the total discount value for the document
CREATE FUNCTION [dbo].[uf_ver_todos_os_ivas_por_stamp]
(	
	@stamp varchar(25),
	@tabela varchar(10)
)
RETURNS @ivas_table TABLE (
		dinheiroIva numeric(18,2),
		percentagemIva numeric(18,2),
		dinheiroProduto numeric(18,2)
)
AS
begin
	if @tabela = 'FT'
	begin
		insert into @ivas_table
		select eivav1,ivatx1, eivain1 from ft (nolock) where ftstamp = @stamp
		insert into @ivas_table
		select eivav2,ivatx2, eivain2 from ft (nolock) where ftstamp = @stamp
		insert into @ivas_table
		select eivav3,ivatx3, eivain3 from ft (nolock) where ftstamp = @stamp
		insert into @ivas_table
		select eivav4,ivatx4, eivain4 from ft (nolock) where ftstamp = @stamp
		insert into @ivas_table
		select eivav5,ivatx5, eivain5 from ft (nolock) where ftstamp = @stamp
		insert into @ivas_table
		select eivav6,ivatx6, eivain6 from ft (nolock) where ftstamp = @stamp
		insert into @ivas_table
		select eivav7,ivatx7, eivain7 from ft (nolock) where ftstamp = @stamp
		insert into @ivas_table
		select eivav8,ivatx8, eivain8 from ft (nolock) where ftstamp = @stamp
		insert into @ivas_table
		select eivav9,ivatx9, eivain9 from ft (nolock) where ftstamp = @stamp
		insert into @ivas_table
		select eivav10,ivatx10, eivain10 from ft (nolock) where ftstamp = @stamp
		insert into @ivas_table
		select eivav11,ivatx11, eivain11 from ft (nolock) where ftstamp = @stamp
		insert into @ivas_table
		select eivav12,ivatx12, eivain12 from ft (nolock) where ftstamp = @stamp
		 
		delete from @ivas_table where dinheiroIva = 0 and dinheiroProduto = 0
	end
	RETURN 
end
 