-- =============================================
-- Author:		Daniel Almeida
-- Create date: 2024/03/17
-- Description:	Retorna dados principais de entidade de facturacao
-- Examples: 
--select * from uf_retornaDadosEntidadeFacturacao ('SNS','Loja 1',getdate())
--select * from uf_retornaDadosEntidadeFacturacao ('VacinasSNS','Loja 1',getdate())
--select * from uf_retornaDadosEntidadeFacturacao ('Savida','Loja 1',getdate())

-- =============================================


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[uf_retornaDadosEntidadeFacturacao]') IS NOT NULL
begin
	drop function dbo.uf_retornaDadosEntidadeFacturacao
end
go

CREATE FUNCTION [dbo].uf_retornaDadosEntidadeFacturacao 
(	
	@abrev varchar(100),
	@site varchar(100),
	@date date
)
RETURNS TABLE 
AS
RETURN
(
	select 	
		'no' = case when isnull(cptorg_dep_site.cptorgAbrev,'') = '' then cptorg.u_no else cptorg_dep_site.no end,
		'estab' = case when isnull(cptorg_dep_site.cptorgAbrev,'') = '' then cptorg.u_estab else cptorg_dep_site.estab end
	from
		cptorg(nolock)
	left join  cptorg_dep_site (NOLOCK) ON  cptorg.entPorLoja = 1 and cptorg_dep_site.cptorgstamp = cptorg.cptorgstamp and cptorg_dep_site.site=@site and  convert(date,entPorLojaAposData)<=@date
	where cptorg.abrev = @abrev
	)

GO

