
if OBJECT_ID('[dbo].[uf_gerais_devolve_primeiro_e_ultimo_nome]') IS NOT NULL
begin
	drop function dbo.uf_gerais_devolve_primeiro_e_ultimo_nome
end
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jos� Moreira
-- Create date: 2022/11/15
-- Description:	devolve primeiro + ultimo nome, primeiro nome, ultimo nome
-- Example: select * from uf_gerais_devolve_primeiro_e_ultimo_nome ('Arroz de Pato')
-- =============================================
CREATE FUNCTION uf_gerais_devolve_primeiro_e_ultimo_nome(@nome varchar(4000))
RETURNS TABLE 
AS
RETURN 
(select	ltrim(rtrim(left(rtrim(ltrim(@nome)),charindex(' ',rtrim(ltrim(@nome)) + ' ')))) + ' ' + ltrim(rtrim(RIGHT(rtrim(ltrim(@nome)), (CHARINDEX(' ',REVERSE(rtrim(ltrim(@nome))),0))))) FIRST_AND_LAST_NAME, 
		--LEN(ltrim(rtrim(left(rtrim(ltrim(@nome)),charindex(' ',rtrim(ltrim(@nome)) + ' ')))) + ' ' + ltrim(rtrim(RIGHT(rtrim(ltrim(@nome)), (CHARINDEX(' ',REVERSE(rtrim(ltrim(@nome))),0)))))),
		ltrim(rtrim(left(rtrim(ltrim(@nome)),charindex(' ',rtrim(ltrim(@nome)) + ' ')))) FIRST_NAME, 
		--LEN(ltrim(rtrim(left(rtrim(ltrim(@nome)),charindex(' ',rtrim(ltrim(@nome)) + ' '))))),
		ltrim(rtrim(RIGHT(rtrim(ltrim(@nome)), (CHARINDEX(' ',REVERSE(rtrim(ltrim(@nome))),0))))) as LAST_NAME )
GO
