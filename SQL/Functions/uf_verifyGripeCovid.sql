/*
	Function que retorna se a vacina � covid ou gripe 
	Tipo vacinas - 1 covid, 2 gripe, 3 s�o ambas

	SELECT dbo.uf_verifyGripeCovid( '1524.1',1, 3)
	SELECT dbo.uf_verifyGripeCovid( '3023199',1, 3)	 
	SELECT dbo.uf_verifyGripeCovid( '5686860',1, 2)        
*/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT *
           FROM   sys.objects
           WHERE  object_id = OBJECT_ID(N'[dbo].[uf_verifyGripeCovid]')
		   )
    drop FUNCTION dbo.uf_verifyGripeCovid

GO

CREATE FUNCTION [dbo].[uf_verifyGripeCovid] 
(	
	@vaccineCode		VARCHAR(254),
	@tipoVacina INT
)
RETURNS bit
AS
BEGIN
	
	DECLARE @isGripeCovid bit

	SET @tipoVacina = ISNULL(@tipoVacina,3)

	IF (@tipoVacina = 1)
	BEGIN
		SET @isGripeCovid = CASE WHEN @vaccineCode like '%covid%'
								THEN CAST(1 AS BIT)
								ELSE CAST(0 AS BIT) 
								END
	END
	
	IF (@tipoVacina = 2)
	BEGIN
		SET @isGripeCovid = CASE WHEN @vaccineCode like '%gripeSNS%'
								THEN CAST(1 AS BIT)
								ELSE CAST(0 AS BIT) 
								END
	END

	IF (@tipoVacina = 3)
		BEGIN
			SET @isGripeCovid = CASE WHEN @vaccineCode like '%gripeSNS%' or @vaccineCode like '%covid%'
									THEN CAST(1 AS BIT)
									ELSE CAST(0 AS BIT) 
									END
		END


	RETURN @isGripeCovid

END
GO

