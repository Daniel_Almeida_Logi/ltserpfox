-- =============================================
-- Author:		Daniel Almeida
-- Create date: 2024/07/02
-- Description:	Valida se deve ignorar erro linha dem
-- Examples: 
--select [dbo].uf_dem_valida_execao_erro_linhas ('999998','D083')
--select  [dbo].uf_dem_valida_execao_erro_linhas ('935640','')


-- =============================================


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if OBJECT_ID('[dbo].[uf_dem_valida_execao_erro_linhas]') IS NOT NULL
begin
	drop function dbo.uf_dem_valida_execao_erro_linhas
end
go

CREATE FUNCTION [dbo].uf_dem_valida_execao_erro_linhas 
(	
	@efrCod varchar(100),
	@errCod varchar(100)

)
RETURNS Int 
AS
BEGIN
	 

	declare @value int = 0
	if(rtrim(ltrim(@errCod))) = 'D083' and rtrim(ltrim(@efrCod)) = '999998'
	begin
		set @value = 1
	end
	return @value
END
 
GO

