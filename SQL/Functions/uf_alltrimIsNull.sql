
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[alltrimIsNull] 
(	
	@value varchar(254)
)
RETURNS varchar(254)
AS
BEGIN
	set @value =  ltrim(rtrim(isnull(@value,'')))
	return @value
END
GO

