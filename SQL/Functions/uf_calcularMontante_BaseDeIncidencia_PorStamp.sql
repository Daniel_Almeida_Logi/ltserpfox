/**
 select * from uf_calcularMontante_BaseDeIncidencia_PorStamp ('ADM7E5D4CAD-7A37-43FD-97C', 1, 'FT')
**/
--code to validate if the function exists, if exists, drops function
IF EXISTS (SELECT *
           FROM   sys.objects
           WHERE  object_id = OBJECT_ID(N'[dbo].[uf_calcularMontante_BaseDeIncidencia_PorStamp]'))
    drop FUNCTION dbo.uf_calcularMontante_BaseDeIncidencia_PorStamp 
go
-- =============================================
-- Author:		José Moreira
-- Create date: 2022-11-07
-- Description: Returns the total value for the document
CREATE FUNCTION [dbo].[uf_calcularMontante_BaseDeIncidencia_PorStamp]
(	
	@stamp varchar(25), 
	@tabela varchar(2)
)
RETURNS @baseDeIncidencia TABLE (
	valor numeric(30,2))
AS
begin 
	if @tabela = 'FT'
	begin
	 	insert into @baseDeIncidencia
		select abs(round(ft.eivav1,2)) + abs(round(ft.eivav2,2)) + abs(round(ft.eivav3,2)) + abs(round(ft.eivav4,2)) + abs(round(ft.eivav5,2))
		+ abs(round(ft.eivav6,2)) + abs(round(ft.eivav7,2)) + abs(round(ft.eivav8,2)) + abs(round(ft.eivav9,2))
		from ft (nolock) where ftstamp = @stamp 
	end
	if @tabela = 'FI'
	begin
		insert into @baseDeIncidencia
		select fi.qtt * fi.etiliquido from fi (nolock) where fi.fistamp = @stamp
	end
	return
end


 
