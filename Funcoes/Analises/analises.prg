DO pesqanalises

**
FUNCTION uf_analises_chama
	DO CASE
		CASE !TYPE("PAINELCENTRAL") == "U" 
			IF !uf_gerais_validaPermUser(ch_userno, ch_grupo,'Relat�rios - Painel')
				uf_perguntalt_chama("O SEU PERFIL N�O TEM PERMISS�ES PARA ACEDER A ESTA FUNCIONALIDADE.","OK","",48)
				RETURN .f.
			ENDIF
	ENDCASE
	
	
	IF TYPE("ANALISES")=="U"		

		**Valida Licenciamento	
		IF (uf_gerais_addConnection('ANALISES') == .f.)
			RETURN .F.
		ENDIF
		
		regua(0,6,"A carregar painel...")
		regua(1,1,"A carregar painel...")
		
		regua(1,2,"A carregar painel...")
		
		uf_analises_controlaPermissoes()
		
		IF !USED("ucrsAnalises")
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_analises_Lista
			ENDTEXT
	
			uf_gerais_actGrelha("","ucrsAnalises",lcSQL)
		ENDIF
		
		SELECT ucrsAnalises
			GO TOP
			SCAN
				IF !uf_gerais_validaPermUser(ch_userno, ch_grupo,'Relat�rios - Sub-grupo ' + ALLTRIM(ucrsAnalises.subgrupo))
					SELECT ucrsAnalises
					DELETE
				ENDIF
				IF !uf_gerais_validaPermUser(ch_userno, ch_grupo, ALLTRIM(ucrsAnalises.Descricao))
					SELECT ucrsAnalises
					DELETE
				ENDIF				
			ENDSCAN

		regua(1,3,"A carregar painel...")
		
		IF !USED("ucrsAnalisesGrupo")
			SELECT distinct grupo FROM ucrsAnalises GROUP BY grupo ORDER BY grupo INTO CURSOR ucrsAnalisesGrupo readwrite
		ENDIF
		IF !USED("ucrsAnalisesSubGrupo")
			SELECT subgrupo FROM ucrsAnalises WHERE 1 = 0 INTO CURSOR ucrsAnalisesSubGrupo readwrite		
		ENDIF	
		IF !USED("ucrsAnalisesDescricao")
			SELECT descricao, ordem, report, comandoFox, objectivo FROM ucrsAnalises WHERE 1 = 0 INTO CURSOR ucrsAnalisesDescricao readwrite
		ENDIF

		regua(1,4,"A carregar painel...")
		
		IF !USED("ucrsMeses")
			CREATE CURSOR ucrsMeses (mes c(2), nome c(20))
			SELECT ucrsMeses 
			APPEND BLANK
			Replace ucrsMeses.mes WITH '1'
			Replace ucrsMeses.nome WITH 'Janeiro'

			APPEND BLANK
			Replace ucrsMeses.mes WITH '2'
			Replace ucrsMeses.nome WITH 'Fevereiro'
			
			APPEND BLANK
			Replace ucrsMeses.mes WITH '3'
			Replace ucrsMeses.nome WITH 'Mar�o'
			
			APPEND BLANK
			Replace ucrsMeses.mes WITH '4'
			Replace ucrsMeses.nome WITH 'Abril'
			
			APPEND BLANK
			Replace ucrsMeses.mes WITH '5'
			Replace ucrsMeses.nome WITH 'Maio'
			
			APPEND BLANK
			Replace ucrsMeses.mes WITH '6'
			Replace ucrsMeses.nome WITH 'Junho'
			
			APPEND BLANK
			Replace ucrsMeses.mes WITH '7'
			Replace ucrsMeses.nome WITH 'Julho'
			
			APPEND BLANK
			Replace ucrsMeses.mes WITH '8'
			Replace ucrsMeses.nome WITH 'Agosto'
			
			APPEND BLANK
			Replace ucrsMeses.mes WITH '9'
			Replace ucrsMeses.nome WITH 'Setembro'
			
			APPEND BLANK
			Replace ucrsMeses.mes WITH '10'
			Replace ucrsMeses.nome WITH 'Outubro'
			
			APPEND BLANK
			Replace ucrsMeses.mes WITH '11'
			Replace ucrsMeses.nome WITH 'Novembro'
			
			APPEND BLANK
			Replace ucrsMeses.mes WITH '12'
			Replace ucrsMeses.nome WITH 'Dezembro'
		ENDIF
		
		IF !USED("ucrsAnos")
			
			CREATE CURSOR ucrsAnos(ano c(20))
			lcAno = YEAR(DATE())

			DO WHILE lcAno > YEAR(DATE())-10
				Select ucrsAnos
				Append Blank
				replace ucrsAnos.ano With ALLTRIM(Str(lcAno))
				lcAno = lcAno - 1
			ENDDO		
		ENDIF
		
		IF !USED("uCrsTempTopo")
			CREATE CURSOR uCrsTempTopo (valor c(20))
			SELECT uCrsTempTopo
			APPEND BLANK
			replace valor WITH "15"
			APPEND BLANK
			replace valor WITH "30"
			APPEND BLANK
			replace valor WITH "60"
			APPEND BLANK
			replace valor WITH "100"
			APPEND BLANK
			replace valor WITH "200"
			APPEND BLANK
			replace valor WITH "300"
			APPEND BLANK
			replace valor WITH "500"
			APPEND BLANK
			replace valor WITH "1000"
			APPEND BLANK
			replace valor WITH "2000"
			APPEND BLANK
			replace valor WITH "5000"
			GO TOP
		ENDIF	
				
				
		** Lojas a que o utilizador/grupo tem acesso
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_gerais_trocaEmpresa <<ch_userno>>,'<<ALLTRIM(ch_grupo)>>'
		ENDTEXT
		If !uf_gerais_actGrelha("", "uCrsTempLojas", lcSql)
			uf_perguntalt_chama("N�o foi possivel encontrar regitos.", "", "OK", 16)
			RETURN .f.
		ENDIF
		
		
		**Lojas a que o utilizador/grupo tem acesso -> mas uma entrada com Todas
		
		IF(USED("uCrsTempLojasTodas"))
			fecha("uCrsTempLojasTodas")
		ENDIF

		SELECT * FROM uCrsTempLojas INTO CURSOR uCrsTempLojasTodas  READWRITE
		IF(RECCOUNT("uCrsTempLojasTodas")>=1)
			INSERT INTO uCrsTempLojasTodas  VALUE("TODAS",'TODAS',0,0)
		ENDIF
		

		regua(1,5,"A carregar painel...")		
				
		IF !USED("ucrsComboDocPesq")
			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_perfis_validaAcessoDocs '<<ch_userno>>', '<<ALLTRIM(ch_grupo)>>', 'Visualizar', 'FO,BO', 0
			ENDTEXT
			
			If !uf_gerais_actGrelha("", "ucrsComboDocPesq", lcSql)
				uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR AS DEFINI��ES DE DOCUMENTOS DISPON�VEIS. POR FAVOR CONTACTE O SUPORTE","OK","",16)
				RETURN .f.
			ENDIF
		ENDIF
		
		regua(1,6,"A carregar painel...")
		
		DO FORM ANALISES
		
		ANALISES.autocenter = .t.
		*uf_analises_expandeDescricao()
		regua(2)
					
	ELSE
		ANALISES.show
		ANALISES.autocenter = .t.
	ENDIF		
	
ENDFUNC


**
FUNCTION uf_analises_controlaPermissoes
	IF !USED("ucrsAnalises")
		RETURN .f.
	ENDIF 
	
	** Faz Tratamento de permiss�es
	SELECT uCrsAnalises
	GO TOP 
	Scan
		* No caso do Perfil ser 0 � considerado Sem Controlo de Acessos
		Text To lcSql Noshow textmerge
			exec up_perfis_validaAcesso '<<Alltrim(ucrsAnalises.perfil)>>', '<<mysite>>'
		ENDTEXT
		If !uf_gerais_actGrelha("", "uCrsAnalisesAcesso",lcSql)
			uf_perguntalt_chama("OCORREU UM ERRO A VALIDAR ACESSOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			return .f.	
		Endif	
		
		Select uCrsAnalisesAcesso
		If Reccount()>0
			Select uCrsAnalisesAcesso
			Go top
			Scan
				IF uCrsAnalisesAcesso.userno == ch_userno Or Upper(Alltrim(uCrsAnalisesAcesso.grupo))==Upper(Alltrim(ch_grupo))
					SELECT  ucrsAnalises 
	   				DELETE 
				Endif
			Endscan
		Endif	
	ENDSCAN
		
ENDFUNC


**
FUNCTION uf_analises_alteraGrupo
	
	ANALISES.containerGrupo.gridSubGrupo.recordsource = ""
	
	SELECT ucrsAnalisesGrupo
	SELECT Distinct subgrupo FROM ucrsAnalises WHERE grupo = ucrsAnalisesGrupo.grupo ORDER BY subgrupo INTO CURSOR ucrsAnalisesSubGrupo readwrite
	
	ANALISES.containerGrupo.gridSubGrupo.recordsource = "ucrsAnalisesSubGrupo"
	ANALISES.containerGrupo.gridSubGrupo.refresh
	
	ANALISES.containerCab.grupo.value = ALLTRIM(UPPER(ucrsAnalisesGrupo.grupo))
	ANALISES.containerCab.subgrupo.value = ALLTRIM(UPPER(ucrsAnalisesSubGrupo.subgrupo))
	ANALISES.containerCab.analise.value = ALLTRIM(UPPER(ucrsAnalisesDescricao.descricao))

	**ACtualiza Descricao	
	uf_analises_alteraSubGrupo()

	ANALISES.containerCab.refresh

ENDFUNC


**
FUNCTION uf_analises_alteraSubGrupo	
	ANALISES.containerGrupo.gridDescricao.recordsource = ""
		
	SELECT ucrsAnalisesSubGrupo
	SELECT descricao, ordem, report, formatoExp, comandoFox, objectivo FROM ucrsAnalises WHERE subgrupo = ucrsAnalisesSubGrupo.subgrupo AND grupo = ucrsAnalisesGrupo.grupo INTO CURSOR ucrsAnalisesDescricao readwrite
	
	ANALISES.containerGrupo.gridDescricao.recordsource = "ucrsAnalisesDescricao"
	ANALISES.containerGrupo.gridDescricao.refresh
	
	ANALISES.containerCab.grupo.value = ALLTRIM(UPPER(ucrsAnalisesGrupo.grupo))
	ANALISES.containerCab.subgrupo.value = ALLTRIM(UPPER(ucrsAnalisesSubGrupo.subgrupo))
	ANALISES.containerCab.analise.value = ALLTRIM(UPPER(ucrsAnalisesDescricao.descricao))
	ANALISES.containerCab.refresh
ENDFUNC


**
FUNCTION uf_analises_dynamicPicture
	IF !USED("ucrsAnalisesGrupo") 
		RETURN "Text1"
	ENDIF
	
	SELECT ucrsAnalisesGrupo
	DO CASE
		CASE ALLTRIM(UPPER(ucrsAnalisesGrupo.grupo)) == "CLIENTES"
			RETURN "ImagemClientes"
		CASE ALLTRIM(UPPER(ucrsAnalisesGrupo.grupo)) == "CLINICA"
			RETURN "ImagemClinica"
		CASE ALLTRIM(UPPER(ucrsAnalisesGrupo.grupo)) == "EMPRESA"
			RETURN "ImagemEmpresa"
		CASE ALLTRIM(UPPER(ucrsAnalisesGrupo.grupo)) == "FORNECEDORES"
			RETURN "ImagemFornecedores"
		CASE ALLTRIM(UPPER(ucrsAnalisesGrupo.grupo)) == "STOCKS"
			RETURN "ImagemStocks"
		CASE ALLTRIM(UPPER(ucrsAnalisesGrupo.grupo)) == "FARMACIA" OR ALLTRIM(UPPER(ucrsAnalisesGrupo.grupo)) == "FARM�CIA"
			RETURN "ImagemFarmacia"
		CASE ALLTRIM(UPPER(ucrsAnalisesGrupo.grupo)) == "GEST�O"
			RETURN "ImagemGestao"
		CASE ALLTRIM(UPPER(ucrsAnalisesGrupo.grupo)) == "DCM"
			RETURN "ImagemDCM"
		CASE ALLTRIM(UPPER(ucrsAnalisesGrupo.grupo)) == "OPERADOR"
			RETURN "ImagemClientes"
		OTHERWISE
			RETURN "Text1"
	ENDCASE
	
ENDFUNC


** Selecciona a an�lise para executar
FUNCTION uf_analises_gravar
	LOCAL lcObj, lcValida
	IF TYPE("pv_filtradci_dci") <> "U"
		RELEASE pv_filtradci_dci
	ENDIF

	IF TYPE("pv_filtradci_dciDesc") <> "U"
		RELEASE pv_filtradci_dciDesc
	ENDIF

	IF TYPE("pv_filtradci_gh") <> "U"
		RELEASE pv_filtradci_gh
	ENDIF

	IF TYPE("pv_filtradci_ghDesc") <> "U"
		RELEASE pv_filtradci_ghDesc
	ENDIF

	IF TYPE("pv_filtradci_cnpem") <> "U"
		RELEASE pv_filtradci_cnpem
	ENDIF

	IF TYPE("pv_filtradci_cnpemDesc") <> "U"
		RELEASE pv_filtradci_cnpemDesc
	ENDIF
	uf_relatorio_chama()
	lcObj = "relatorio.pagina2.container1.wdgselreport"
	lcObjTxtAnalise = "relatorio.pagina2.container1.wdgselreport.txtAnalise"
	TRY 
		lcValida = &lcObj..name
	CATCH 
		select uCrsPCentral
		locate for upper(alltrim(uCrsPCentral.nome)) == 'WDGSELREPORT'
		select uCrsPCentral
		uf_painelcentral_alternaPagina(uCrsPCentral.pagina)
	ENDTRY


	&&lcObj = "painelCentral.pagina" + astr(painelCentral.pagina) + ".wdgselreport"
	SELECT ucrsAnalisesDescricao
	&lcObj..lcOrdem 		= ucrsAnalisesDescricao.ordem
	&lcObj..lcComandoFox 	= ucrsAnalisesDescricao.comandoFox
	&lcObj..lcReport 		= ucrsAnalisesDescricao.report
	&lcObjTxtAnalise..value = ALLTRIM(ucrsAnalisesDescricao.descricao)

    lcObj = "relatorio.pagina2.container1.wdgselreport.lcComandoFox"

	IF EMPTY(ALLTRIM(&lcObj))

        RELATORIO.menu1.estado("print, pdf, excel, csv", "SHOW")

    ELSE

        RELATORIO.menu1.estado("print, pdf, excel, csv", "HIDE")

    ENDIF

    IF(ucrsAnalisesDescricao.ordem = 73)
        RELATORIO.menu1.estado("enviarSaft, validarSaft", "SHOW")
    ELSE
        RELATORIO.menu1.estado("enviarSaft, validarSaft", "HIDE")
    ENDIF
	
	IF uf_gerais_getParameter_site('ADM0000000208', 'BOOL', mySite) = .T.
		IF(ucrsAnalisesDescricao.ordem = 130)
        	RELATORIO.menu1.estado("dispensar", "SHOW")
    	ELSE
        	RELATORIO.menu1.estado("dispensar", "HIDE")
    	ENDIF
    ELSE
     	RELATORIO.menu1.estado("dispensar", "HIDE")
	ENDIF
	
	uf_analises_sair()
ENDFUNC 


FUNCTION uf_analises_relatorio_repo_stocks_dispensa_robot
	
	IF !uf_perguntalt_chama("Vai dispensar produtos do robot, pretende continuar?","Sim","N�o")
		regua(2)
		RETURN .F.
	ENDIF	
		
	fecha("uCrsRepoStocksRobot")
	fecha("uCrsDispRobot")

	IF(!uf_robot_servico_terminal_mapeado())
		uf_perguntalt_chama("N�O TEM O LOCAL DE DISPENSA DO ROB� PARAMETRIZADO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		return .f.	
	ENDIF
	
	
	LOCAL lcresp, lcrobotstate 
	STORE '' TO lcresp, lcrobotstate 
	
	uf_gerais_meiaRegua("A validar estado do robot...")
    lcpedstamp = uf_gerais_stamp()
    lcresp = uf_robot_servico_status(ALLTRIM(lcpedstamp))
    
    IF !EMPTY(lcresp) AND VAL(lcresp)=200
       DECLARE Sleep IN Win32API LONG
       sleep(3000.0)
       lcrobotstate = uf_robot_servico_status_response(ALLTRIM(lcpedstamp))    
    ENDIF
    
    regua(2)
    
    
    if(lcrobotstate!='00' and lcrobotstate!='02')
       	uf_perguntalt_chama("POR FAVOR VALIDE O ESTADO DO ROBOT.","OK","",16)
		return .f.	
    ENDIF 
	
	
	LOCAL lcObj,lcDataIni, lcDataFim, lcStock
	lcObj = "relatorio.pagina2.container1.wdgfiltroreport.containerFiltros"

	
	LOCAL lcSql, lcHoraInico, lcHoraFim, lcSite, lcLocal1, lcLocal2, lcFamilia
	STORE '' To lcSql, lcHoraInico, lcHoraFim, lcSite, lcLocal1, lcLocal2, lcFamilia
	
	lcStock = -9999

	lcDataIni = uf_gerais_getdate(&lcObj..dataini.text1.value,"SQL")
	lcDataFim = uf_gerais_getdate(&lcObj..datafim.text1.value,"SQL")	
	lcSite = Alltrim(&lcObj..site.text1.value)
	lcHoraInico = Alltrim(&lcObj..horaini.text1.value)
	lcHoraFim= Alltrim(&lcObj..horafim.text1.value)
	lcLocal1= Alltrim(&lcObj..local1.text1.value)
	lcLocal2= Alltrim(&lcObj..local2.text1.value)
	lcFamilia = Alltrim(&lcObj..familia.text1.value)
	lcStock = &lcObj..stock.text1.value
	
	
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_relatorio_reposicao_stocks '<<lcDataIni>>','<<lcDataFim>>','<<lcHoraInico>>','<<lcHoraFim>>','<<lcLocal1>>','<<lcLocal2>>','<<lcFamilia>>','<<lcSite>>',<<lcStock>>
	ENDTEXT
	
	

	IF !uf_gerais_actGrelha("", "uCrsRepoStocksRobot",lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A VALIDAR STOCKS DE REPOSI��O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		fecha("uCrsRepoStocksRobot")
		return .f.			
	ENDIF	

	
	SELECT distinct ref ,SUM(vendas) as qtt FROM uCrsRepoStocksRobot GROUP BY ref ORDER BY ref INTO CURSOR uCrsDispRobot READWRITE
	
	IF(USED("uCrsDispRobot"))
		DELETE FROM uCrsDispRobot WHERE qtt < 1
	
		SELECT uCrsDispRobot 
		GO TOP
		
		uf_robot_servico_retrieve_line()
		
	ENDIF
					
	fecha("uCrsRepoStocksRobot")
	fecha("uCrsDispRobot")
	
	
ENDFUNC






** Executa a analise
FUNCTION uf_analises_executar
	LPARAMETERS lcBrowser
	
	LOCAL lcFiltros, lcfiltrosCursor, lcReport
	lcReport = ""

	** valida��es **
	lcTxtRel = IIF(TYPE("relatorio") == "U","painelCentral.pagina" + astr(painelCentral.pagina),"relatorio.pagina2") + ".container1.wdgselreport.txtAnalise"
	IF EMPTY(ALLTRIM(&lcTxtRel..value))
		uf_perguntalt_chama("DEVE SELECIONAR UMA AN�LISE PARA EXECUTAR. POR FAVOR VERIFIQUE.","OK","",64)
		RETURN .f.
	ENDIF
	lcObj = IIF(TYPE("relatorio") == "U","painelCentral.pagina" + astr(painelCentral.pagina),"relatorio.pagina2") + ".container1.wdgfiltroreport.containerFiltros."
	**********************
	
	IF uf_analises_validaCamposObrigatorios(lcObj) == .f.
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER, POR FAVOR VERIFIQUE.","OK","",64)
		RETURN .f.
	ENDIF
	
	lcFiltros = ''
	**Cursor com o nome dos filtros, para contruir link
	SELECT ucrsAnalisesDef
	GO TOP
	SCAN
		lcObj = IIF(TYPE("relatorio") == "U","painelCentral.pagina" + astr(painelCentral.pagina),"relatorio.pagina2") + ".container1.wdgfiltroreport.containerFiltros."
		Do CASE
			Case UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) == "S"
				lcObj = lcObj + ALLTRIM(ucrsAnalisesDef.nome) + ".Spinner1"
				lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "=" + ASTR(&lcObj..value)

			Case UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) == "D" Or UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) = "H"
				lcObj = lcObj + ALLTRIM(ucrsAnalisesDef.nome) + ".Text1"
				lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(&lcObj..value))
		
			Case UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) == "CB" && combos
				lcObj = lcObj + ALLTRIM(ucrsAnalisesDef.nome) + ".combo1"
				lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(&lcObj..value))
					
			Case UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) == "C" && check box ultiliza a propriedade Tag; devolve true ou false
				lcObj = lcObj + ALLTRIM(ucrsAnalisesDef.nome)
				lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(&lcObj..tag))
		
			Case UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) == "T" && text box
				lcObj = lcObj + ALLTRIM(ucrsAnalisesDef.nome) + ".Text1"
				TRY
					lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(STR(&lcObj..value)))
				CATCH
					lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(&lcObj..value))
				ENDTRY
			Case UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) == "L" && check box
				lcObj = lcObj + ALLTRIM(ucrsAnalisesDef.nome) && + ".Check1"
				lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(&lcObj..tag))
		
			Case UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) == "SB" && select box
				**cursor com base no nome do obj
				lcCrsSB = "ucrsSb" + ALLTRIM(ucrsAnalisesDef.nome)
				lcfiltrosCursor = ""
				IF USED(lcCrsSB)
					SELECT &lcCrsSB
					GO Top
					SCAN
						IF &lcCrsSB..sel == .t.
							lcfiltrosCursor = lcfiltrosCursor + ALLTRIM(&lcCrsSB..opcao) + ","
						ENDIF
					ENDSCAN
				ENDIF
				lcfiltrosCursor = LEFT(ALLTRIM(lcfiltrosCursor),LEN(ALLTRIM(lcfiltrosCursor))-1)
				lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(lcfiltrosCursor)) 
			
			Case UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) == "VC"
				IF ALLTRIM(ucrsAnalisesDef.nome) != "relatorio"
					lcObj = lcObj + ALLTRIM(ucrsAnalisesDef.nome) + ".Text1"
					lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(&lcObj..value))
				ELSE
					lcObj = lcObj + ALLTRIM(ucrsAnalisesDef.nome) + ".Text1"
					lcReport = uf_gerais_trataCarateresReservadosURL(ALLTRIM(&lcObj..value))	
				ENDIF	
			Case UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) == "FM"
				lcObj = lcObj + ALLTRIM(ucrsAnalisesDef.nome) + ".Text1"
				lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "=" +  uf_gerais_trataCarateresReservadosURL(ALLTRIM(&lcObj..value))
				
			Case UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) == "OP"
				
				lcObjTag = lcObj + ALLTRIM(ucrsAnalisesDef.nome) && + ".Check1"
				lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "Tag=" + &lcObjTag..tag
				
				lcObj2 = lcObj + ALLTRIM(ucrsAnalisesDef.nome) + ".Text1"
				lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "=" + ALLTRIM(&lcObj2..value)
				
				lcObjOperador = lcObj + ALLTRIM(ucrsAnalisesDef.nome) + ".operadorLogico"
				lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "Op=" + ALLTRIM(&lcObjOperador..value)
				
				
			Case UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) == "DCI"
				
				IF VARTYPE(pv_filtradci_dci)=="U"			
					lcFiltros = lcFiltros + "&dci="
					lcFiltros = lcFiltros + "&dciDesc="
				ELSE
					lcFiltros = lcFiltros + "&dci=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(pv_filtradci_dci))
					lcFiltros = lcFiltros + "&dciDesc=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(pv_filtradci_dciDesc))
				ENDIF 
				
				IF VARTYPE(pv_filtradci_gh)=="U"			
					lcFiltros = lcFiltros + "&grphmgcode="
					lcFiltros = lcFiltros + "&grphmgcodeDesc="
				ELSE
					lcFiltros = lcFiltros + "&grphmgcode=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(pv_filtradci_gh))
					lcFiltros = lcFiltros + "&grphmgcodeDesc=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(pv_filtradci_ghDesc))
				ENDIF 
				
				IF VARTYPE(pv_filtradci_cnpem)=="U"			
					lcFiltros = lcFiltros + "&cnpem="
					lcFiltros = lcFiltros + "&cnpemDesc="
				ELSE
					lcFiltros = lcFiltros + "&cnpem=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(pv_filtradci_cnpem))
					lcFiltros = lcFiltros + "&cnpemDesc=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(pv_filtradci_cnpemDesc))
				ENDIF 
			
			Case UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) == "VCF"
				IF ALLTRIM(ucrsAnalisesDef.nome) != "relatorio"
					lcObj = lcObj + ALLTRIM(ucrsAnalisesDef.nome)
					lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(&lcObj..tag))
				ELSE
					lcObj = lcObj + ALLTRIM(ucrsAnalisesDef.nome) 
					lcReport = uf_gerais_trataCarateresReservadosURL(ALLTRIM(&lcObj..tag))	
				ENDIF	
							
			Otherwise
				**
		Endcase
	ENDSCAN
	
	lcObj = IIF(TYPE("relatorio") == "U","painelCentral.pagina" + astr(painelCentral.pagina),"relatorio.pagina2") + ".container1.wdgselreport.lcComandoFox"
	IF EMPTY(ALLTRIM(&lcObj))
		** chamar report
		lcObj = IIF(TYPE("relatorio") == "U","painelCentral.pagina" + astr(painelCentral.pagina),"relatorio.pagina2") + ".container1.wdgselreport.lcReport"
		
		** Nome Servidor
		LOCAL nomeServidor, lcReport, lcVersaoSoft 
		nomeServidor = uf_gerais_getParameter('ADM0000000045', 'text')
		lcVersaoSoft = "Versao_" + STRTRAN(ALLTRIM(uf_gerais_getParameter('ADM0000000156', 'text')),".","_")

		IF EMPTY(nomeServidor)
			uf_perguntalt_chama("N�O FOI POSSIVEL ENCONTRAR O NOME DO SERVIDOR. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
		
		IF EMPTY(lcVersaoSoft)
			uf_perguntalt_chama("N�O FOI POSSIVEL DETERMINAR A VERS�O DO SOFTWARE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
		
		IF USED("ucrsDadosServidorReport")
			fecha("ucrsDadosServidorReport")
		ENDIF 
		
		lcSQL = ""
		**
		IF uf_gerais_getParameter_site('ADM0000000027', 'BOOL', mySite) = .f.
			TEXT TO lcSQL NOSHOW TEXTMERGE
				SELECT ServerName = 
						convert(varchar,ServerProperty('machinename')) 
						+ case when (select ServerProperty('InstanceName')) is null then '' else '\' end
						+ convert(varchar,isnull((select ServerProperty('InstanceName')),''))
						+ case when convert(varchar,(SELECT local_tcp_port FROM   sys.dm_exec_connections WHERE  session_id = @@SPID)) is null then '' else ',' end
						+ isnull(convert(varchar,(SELECT local_tcp_port FROM   sys.dm_exec_connections WHERE  session_id = @@SPID)),'')
						, dbName = DB_NAME() 
			ENDTEXT 
		ELSE
			TEXT TO lcSQL NOSHOW TEXTMERGE
				SELECT ServerName = (select textvalue from b_parameters_site where stamp='ADM0000000028' and site='<<ALLTRIM(mySite)>>')
					, dbName = (select textvalue from b_parameters_site where stamp='ADM0000000029' and site='<<ALLTRIM(mySite)>>')
			ENDTEXT 
		ENDIF 
		IF !uf_gerais_actGrelha("",[ucrsDadosServidorReport],lcSQL )
			uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR A LOCALIZA��O DA BD. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .F.
		ENDIF		
		
		**Se exsitir um parametros chamado "relatorio" considera que esse filtro � o nome do relat�rio
		IF EMPTY(lcReport)
			lcReport = &lcObj
		ELSE
			SELECT ucrsAnalisesDef
			GO TOp
			lcOrdem = ucrsAnalisesDef.ordem

			TEXT TO lcSQL NOSHOW TEXTMERGE 
				exec up_relatorio_MultiRelatorio <<lcOrdem>>, '<<ALLTRIM(lcReport)>>'
			ENDTEXT

			IF !uf_gerais_actGrelha("","ucrsAnalisesMultiReport",lcSQL)
				uf_perguntalt_chama("Defini��es de relat�rio incorretas.","OK","",48)
				RETURN .f.
			ENDIF 
			IF RECCOUNT("ucrsAnalisesMultiReport")>0
				Select ucrsAnalisesMultiReport
				lcReport = ALLTRIM(ucrsAnalisesMultiReport.relatorio)
			ELSE
				lcReport = &lcObj
			ENDIF 
		ENDIF

		&& consulta a informa��o dos SMS enviados		
		IF ALLTRIM(lcReport) == "listagem_sms_enviados"
			LOCAL uv_obj , uv_data, lcSite, lcObjTemp, lcSiteNo
			lcObjTemp = IIF(TYPE("relatorio") == "U","painelCentral.pagina" + astr(painelCentral.pagina),"relatorio.pagina2") + ".container1.wdgfiltroreport.containerFiltros."		
						
			uv_data = ALLTRIM(&lcObjTemp.dataA.text1.value)			
			lcSite = Alltrim(&lcObjTemp.site.text1.value)
			
			lcSiteNo= uf_gerais_getUmValor("empresa", "no", "site = '" + alltrim(lcSite) + "'")		
			
			uf_ListaSMS_chama(uv_Data, lcSiteNo)
		ENDIF
	
		IF ALLTRIM(lcReport) == "relatorio_stocks_repoRobot" 

			if(myUsaServicosRobot)
				LOCAL lcPedStampStRobot
				lcPedStampStRobot= uf_gerais_stamp()
				if(!uf_robot_servicos_processInventario(lcPedStampStRobot))
					RETURN .f.
				ENDIF			
			ELSE
				uf_perguntalt_chama("O Logitools n�o tem integra��o com o robot activa.","OK","",16)
				RETURN .f.
			ENDIF
			
		
		ENDIF

		Select ucrsDadosServidorReport
		TEXT TO lcURL TEXTMERGE NOSHOW
			http://<<Alltrim(nomeServidor)>>/ReportServer/Pages/ReportViewer.aspx?%2f<<Alltrim(lcVersaoSoft)>>%2f<<ALLTRIM(lcReport)>>&rs:Command=Render&rs:ParameterLanguage=pt-PT&rc:Parameters=false&ServidorSQL=<<ALLTRIM(ucrsDadosServidorReport.ServerName)>>&BaseDados=<<ALLTRIM(ucrsDadosServidorReport.dbName)>><<ALLTRIM(lcFiltros)>>
		ENDTEXT

		LOCAL lcNomeAnalise
		
		lcNomeAnalise = IIF(TYPE("relatorio") == "U","painelCentral.pagina" + astr(painelCentral.pagina),"relatorio.pagina2") + ".container1.wdgselreport.txtAnalise"
		lcNomeAnalise = ALLTRIM(&lcNomeAnalise..value)
		

		uf_wdgbrowser_abreURL(lcURL,lcBrowser,lcNomeAnalise)
		

	ELSE

		** executa c�digo fox
		lcComandoFox = &lcObj 
		&lcComandoFox 
		
		&&fechar browser
		lcBrowser = lcBrowser + ".Btnimg1"
		TRY 
			&lcBrowser..click()
		CATCH
		ENDTRY

        **uv_pasta = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "' and site = '" + ALLTRIM(mySite) + "'"))
        uv_pasta = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'"))

        DECLARE INTEGER ShellExecute IN shell32.dll ; 
            INTEGER hndWin, ; 
            STRING cAction, ; 
            STRING cFileName, ; 
            STRING cParams, ;  
            STRING cDir, ; 
            INTEGER nShowWin

        ShellExecute(0,'explore',uv_pasta,'','',1)

	ENDIF


ENDFUNC





**
FUNCTION uf_analises_validaCamposObrigatorios
	LPARAMETERS lcObject
	
	IF !USED("ucrsAnalisesDef")
		RETURN .t.
	ENDIF
	
	LOCAL lcObj
	lcObj = lcObject
		
	**Cursor com o nome dos filtros
	SELECT ucrsAnalisesDef
	GO TOP
	SCAN
		lcObject = lcObj
		DO CASE
			Case ucrsAnalisesDef.tipo = "d" Or ucrsAnalisesDef.tipo = "h" OR ucrsAnalisesDef.tipo = "vc" OR ucrsAnalisesDef.tipo = "fm"
				lcObject = lcObject + ALLTRIM(ucrsAnalisesDef.nome) + ".Text1"
				IF ucrsAnalisesDef.obrigatorio == .t. AND EMPTY(ALLTRIM(&lcObject..value))
					&lcObject..backcolor = RGB(230,242,255)
					&lcObject..setfocus
					RETURN .f.
				ENDIF
			Case ucrsAnalisesDef.tipo = "cb" && combos
				lcObject = lcObject + ALLTRIM(ucrsAnalisesDef.nome) + ".combo1"
				IF ucrsAnalisesDef.obrigatorio == .t. AND EMPTY(ALLTRIM(&lcObject..value))
					&lcObject..backcolor = RGB(230,242,255)
					&lcObject..setfocus
					RETURN .f.
				ENDIF
			Otherwise 
				**
		Endcase
	ENDSCAN
		
	RETURN .t.

ENDFUNC


**
FUNCTION uf_analises_expandeDescricao
		
	If EMPTY(ANALISES.TAG)
		ANALISES.containerObs.Visible = .f.
		ANALISES.TAG = "1"
		ANALISES.check1.config(myPath + "\imagens\icons\pagedown_B.png", "")
		ANALISES.check1.top = ANALISES.Height-30
		ANALISES.lbl1.top = ANALISES.check1.top
		ANALISES.lbl1.visible = .f.
		
		ANALISES.containerGrupo.Height = ANALISES.Height - 50 + 174
	ELSE
		ANALISES.containerObs.Visible = .t.
			
		ANALISES.check1.config(myPath + "\imagens\icons\pageup_b.png", "")
		ANALISES.TAG = ""
		ANALISES.check1.top = ANALISES.Height-184
		ANALISES.lbl1.top = ANALISES.check1.top
		ANALISES.lbl1.visible = .t.
		
		ANALISES.containerGrupo.Height = ANALISES.Height - 50
	Endif
ENDFUNC


**
FUNCTION uf_analises_sair
	**Fecha Cursores
	regua(2)
	SET POINT TO 
	
	IF USED("ucrsAnalises")
		fecha("ucrsAnalises")
	ENDIF
	IF USED("ucrsAnalisesGrupo")
		fecha("ucrsAnalisesGrupo")
	ENDIF
	IF USED("ucrsAnalisesSubGrupo")
		fecha("ucrsAnalisesSubGrupo")
	ENDIF	
	IF USED("ucrsAnalisesDescricao")
		fecha("ucrsAnalisesDescricao")
	ENDIF	
	
	IF USED("uCrsAnalisesAcesso")
		fecha("uCrsAnalisesAcesso")
	ENDIF
	

	ANALISES.hide
	ANALISES.release
	RELEASE ANALISES
ENDFUNC


**
Function uf_analises_selDirSaft
	LPARAMETERS lcObj
	
	Local mdir
	oShell = CREATEOBJECT("Shell.Application")
	oFolder = oShell.Application.BrowseForFolder(_screen.HWnd,"Select Folder", 1)
	if isnull(oFolder)
		RETURN .f.
	ENDIF
	
	mdir = oFolder.self.path

	If !Empty(mdir)
		&lcObj..value = Alltrim(mdir)
	Endif
ENDFUNC


** Emitir ficheiro SAF(T) 
Function uf_analises_gerarSaftPT
	LPARAMETERS lcTipo, lcFileVal
	LOCAL lcValidaFile, lcResult 
	
	lcResult = .f.
	
	IF TYPE("lcTipo") != "C" OR EMPTY(ALLTRIM(lcTipo))
		uf_perguntalt_chama("Par�metro inv�lido. Por favor contacte o suporte.","OK","",48)
		RETURN .f.
	ENDIF 
	
	Local lcObj
	lcObj = "relatorio.pagina2.container1.wdgfiltroreport.containerFiltros"
	
	Local lcSql, lcFIle
	Store '' To lcSql, lcFile
	
	Local lcDataIni, lcDataFim, lcSite, lcDirectoria, lcDiretoriaLocal  
	lcDataIni = uf_gerais_getdate(&lcObj..dataini.text1.value,"SQL")
	lcDataFim = uf_gerais_getdate(&lcObj..datafim.text1.value,"SQL")
	lcSite = Alltrim(&lcObj..site.text1.value)
	**lcDirectoria = Alltrim(&lcObj..directoria.text1.value)
    **lcDirectoria = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "' and site = '" + ALLTRIM(mySite) + "'"))
    lcDirectoria = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'"))
	lcDiretoriaLocal = GETENV('TEMP')
	
	************** Valida��es **************************
	If Ctod(&lcObj..dataini.text1.value) > Ctod(&lcObj..datafim.text1.value)
		uf_perguntalt_chama("N�O PODE TER A DATA FINAL INFERIOR � DATA INICIAL.","OK","",48)
		RETURN .f.
	ENDIF
	**If Empty(Alltrim(&lcObj..directoria.text1.value))
    IF EMPTY(lcDirectoria)
		uf_perguntalt_chama("Deve preencher o par�metro 'Caminho Relat�rio' para poder gerar o ficheiro.","OK","",48)
		RETURN .f.
	Endif

    IF !uf_gerais_createDir(lcDirectoria)
    	regua(2)
        RETURN .F.
    ENDIF

	If Empty(astr(&lcObj..anofiscal.text1.value))
		regua(2)
		uf_perguntalt_chama("DEVE PRIMEIRO SELECCIONAR O ANO FISCAL.","OK","",48)
		RETURN .f.
	Endif
	If right(uf_gerais_getDate(&lcObj..dataini.text1.value,"DATA"),4) != Alltrim(&lcObj..anofiscal.text1.value) Or right(uf_gerais_getDate(&lcObj..datafim.text1.value,"DATA"),4) != Alltrim(&lcObj..anofiscal.text1.value)
		regua(2)
		uf_perguntalt_chama("A DATA INICIAL E FINAL DEVE CORRESPONDER AO ANO FISCAL.","OK","",48)
		RETURN .f.
	Endif
	If Empty(&lcObj..site.text1.value)
		regua(2)
		uf_perguntalt_chama("POR FAVOR SELECCIONE UMA LOJA A EMITIR FICHEIRO SAFT-PT.","OK","",48)
		RETURN .f.
	Endif	
	
	Select uCrsE1
	GO TOP
	
	&& validar numero de contribuinte 
	IF !Type(Alltrim(uCrsE1.ncont))=='N'
		uf_perguntalt_chama("O N�MERO DE CONTRIBUINTE N�O EST� CORRECTAMENTE PREENCHIDO. POR FAVOR VALIDE.","OK","",48)
		RETURN .f.
	ENDIF
	
	&&Validar Cae
	IF EMPTY(ALLTRIM(uCrsE1.Cae))
		uf_perguntalt_chama("O Cae n�o est� corretamente preenchido na ficha completa da empresa. Por favor verifique.","OK","",48)
		RETURN .f.
	ENDIF
	
	
	&& tabela de regimes de IVA
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select 
			tabiva
			,taxa 
		from 
			regiva (nolock)
	ENDTEXT 
	uf_gerais_actGrelha("","uCrsRegIva",lcSQL)
	
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select 
			distinct 
				tabiva
				,iva 
		from 
			fi (nolock) 
		where 
			fi.rdata between '<<lcDataIni>>' and '<<lcDataFim>>' 
			and fi.ref != ''
			and fi.ndoc != 75
			and (SELECT anulado FROM ft(nolock) WHERE ft.ftstamp = fi.ftstamp) = 0
	ENDTEXT
	uf_gerais_actGrelha("","uCrsDistIva",lcSQL)
	SELECT uCrsDistIva
	SCAN
		SELECT uCrsRegIva
		LOCATE FOR uCrsRegIva.tabiva == uCrsDistIva.tabiva AND uCrsRegIva.taxa == uCrsDistIva.iva
		IF !FOUND()
			uf_perguntalt_chama("A TABELA DE REGIMES DE IVA N�O SE ENCONTRA CORRECTAMENTE PREENCHIDA." + CHR(13) + "POR FAVOR CONTACTE O SUPORTE.","OK","",48)
			RETURN .f.
		ENDIF
	ENDSCAN
	fecha("uCrsDistIva")
	fecha("uCrsRegIva")
	
	*** VERIFICAR SE HA DOCUMENTOS PARA GERAR SAFT. SE NAO HOUVER, NAO PERMITIR GERA��O! FICHEIRO SAI � MESMA MAS INVALIDO
	
	** Determinar numero da ULTIMA factura para este ANO, MES e ENTIDADE a ser enviada via Webservice
	DO CASE 
		CASE UPPER(ALLTRIM(lcTipo)) == "DF"	
			Text To lcSql Noshow textmerge
				select 
					NumDocs = Count(ftstamp)
				from ft (nolock)
				where 
					fdata between '<<lcDataIni>>' and '<<lcDataFim>>' and site = <<IIF(lcSite=="TODAS","ft.site","'"+lcSite+"'")>>
			Endtext

			If uf_gerais_actGrelha("","uCrsNumDocsSAFT",lcSql)
				select uCrsNumDocsSAFT
				If Reccount("uCrsNumDocsSAFT")>0
					If uCrsNumDocsSAFT.NumDocs == 0
						uf_perguntalt_chama("N�o existem documentos de fatura��o a exportar. Por favor escolha outro periodo.","OK","",48)
						Fecha("uCrsNumDocsSAFT")
						RETURN .f.
					ENDIF
				ENDIF
				Fecha("uCrsNumDocsSAFT")
			Else
				uf_perguntalt_chama("N�o foi poss�vel determinar o total de documentos pass�veis de exporta��o. Por favor contacte o suporte.","OK","",16)
				RETURN .f. 
			ENDIF
		CASE UPPER(ALLTRIM(lcTipo)) == "DT"	
			Text To lcSql Noshow textmerge
				select SUM(NumDocs) as NumDocs
				from (
					select 
						NumDocs = Count(ft.ftstamp)
					from ft (nolock)
					inner join ft2 (nolock) on ft2.ft2stamp = ft.ftstamp
					where 
						fdata between '<<lcDataIni>>' and '<<lcDataFim>>' 
						and ft.site = <<IIF(lcSite=="TODAS","ft.site","'"+lcSite+"'")>>
						and ft.ndoc in (select ndoc from td (nolock) where td.tiposaft in ('GT','GR','GA','GC','GD'))
						and ft2.subproc = ''
					
					union all

					select 
						NumDocs = Count(bo.bostamp)
					from bo (nolock)
					--inner join ft2 (nolock) on ft2.ft2stamp = ft.ftstamp
					where 
						dataobra between '<<lcDataIni>>' and '<<lcDataFim>>' 
						--and ft.site = <<IIF(lcSite=="TODAS","ft.site","'"+lcSite+"'")>>
						and bo.ndos = 17
						--and ft2.subproc = ''
			)x
			Endtext

			If uf_gerais_actGrelha("","uCrsNumDocsSAFT",lcSql)
				select uCrsNumDocsSAFT
				If Reccount("uCrsNumDocsSAFT")>0
					If uCrsNumDocsSAFT.NumDocs == 0
						uf_perguntalt_chama("N�o existem documentos de fatura��o a exportar. Por favor escolha outro periodo.","OK","",48)
						Fecha("uCrsNumDocsSAFT")
						RETURN .f.
					ENDIF
				ENDIF
				Fecha("uCrsNumDocsSAFT")
			Else
				uf_perguntalt_chama("N�o foi poss�vel determinar o total de documentos pass�veis de exporta��o. Por favor contacte o suporte.","OK","",16)
				RETURN .f. 
			ENDIF
		OTHERWISE
			uf_perguntalt_chama("Par�metro inv�lido. Por favor contacte o suporte.","OK","",48)
			RETURN .f.
	ENDCASE
	
	
	** abrir regua de progresso
	regua(0,3,"A obter dados para ficheiro SAFT...")	
	
	LOCAL lcStamp
	
	** Gerar e correr comando para gerar o SAFT 
	DO CASE 
		CASE UPPER(ALLTRIM(lcTipo)) == "DF"	
			IF left(lcDataIni,4) <= "2012"
				Text To lcSql Noshow Textmerge
					exec up_gen_saft2012 '<<lcDataIni>>', '<<lcDataFim>>', '<<lcSite>>'
				ENDTEXT
			ELSE
				IF VAL(left(lcDataIni,4)) == 2013
					IF VAL(right(left(lcDataIni,6),2)) < 7 && Inferior a Julho dever� correr a SP antiga
						Text To lcSql Noshow Textmerge
							exec up_gen_saft2013_1 '<<lcDataIni>>', '<<lcDataFim>>', '<<lcSite>>'
						ENDTEXT
					ELSE && Superior a Julho correr a nova vers�o
						IF VAL(right(left(lcDataIni,6),2)) < 10 && Inferior a Outubro
							Text To lcSql Noshow Textmerge
								exec up_gen_saft2013_2 '<<lcDataIni>>', '<<lcDataFim>>', '<<lcSite>>'
							ENDTEXT
						ELSE
							Text To lcSql Noshow Textmerge
								exec up_gen_saft2013_3 '<<lcDataIni>>', '<<lcDataFim>>', '<<lcSite>>'
							ENDTEXT
						ENDIF 
					ENDIF 
				ELSE
					
					IF VAL(left(lcDataIni,4)) <= 2017
						&&Apartir de Julho de 2017 (incluisive)
						IF VAL(left(lcDataIni,4)) == 2017 AND VAL(right(left(lcDataIni,6),2))>=7 
							
							Text To lcSql Noshow Textmerge
								exec up_gen_saft_1_04 '<<lcDataIni>>', '<<lcDataFim>>', '<<lcSite>>'
							ENDTEXT				
						&&antes de Julho de 2017
						ELSE
							
							Text To lcSql Noshow Textmerge
								exec up_gen_saft2013_3 '<<lcDataIni>>', '<<lcDataFim>>', '<<lcSite>>'
							ENDTEXT
						
						ENDIF
						&&Apartir  2018 (incluisive)
					ELSE
						&& Verificar a SP para extrair o SAF-T dependente do pa�s
						IF UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'
							Text To lcSql Noshow Textmerge
								exec up_gen_saft_1_04 '<<lcDataIni>>', '<<lcDataFim>>', '<<lcSite>>'
							ENDTEXT			
						ELSE
							LOCAL lcSPSaft
							lcSPSaft = uf_gerais_getparameter_site('ADM0000000049', 'TEXT', mySite)
							Text To lcSql Noshow Textmerge
								exec <<ALLTRIM(lcSPSaft)>> '<<lcDataIni>>', '<<lcDataFim>>', '<<lcSite>>'
							ENDTEXT
						ENDIF 
					ENDIF
				
					
				ENDIF
			ENDIF
		CASE UPPER(ALLTRIM(lcTipo)) == "DT"
			Text To lcSql Noshow Textmerge
				exec up_gen_saftTransp '<<lcDataIni>>', '<<lcDataFim>>', '<<lcSite>>'
			ENDTEXT
		OTHERWISE
			uf_perguntalt_chama("Par�metro inv�lido. Por favor contacte o suporte.","OK","",48)
			RETURN .f.
	ENDCASE
	
	If !uf_gerais_actGrelha("","uCrsSaftFile",lcSql)
		REGUA(2)
		uf_perguntalt_chama("N�o foi poss�vel gerar o SAF-T. Por favor contacte o suporte.","OK","",16)
		
		lcStamp = uf_gerais_stamp()
		
		&& gravar nas ocorr�ncias
		TEXT TO lcSql Noshow Textmerge
			INSERT into B_ocorrencias
				(stamp, linkstamp, tipo, grau, descr,
				ovalor, dvalor,
				usr, date, site, terminal)
			values
				('<<lcStamp>>', '', 'SAFT', 1, 'Erro a correr SP de gera��o de SAFT',
				'Data Inicial: <<lcDataIni>> Data Final: <<lcDataFim>>', 'Site: <<lcSite>> Directoria: <<lcDirectoria>>',
				<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()), '<<mySite>>', '<<myTermNo>>')
		ENDTEXT
		uf_gerais_actGrelha("","",lcSql)
	Else

		REGUA(1,3,"A gerar ficheiro SAFT-T...")	
		
		IF  Reccount("uCrsSaftFile")>0
			SELECT uCrsE1
			lcFile = lcDiretoriaLocal + "\" + Alltrim(uCrsE1.ncont) + right(uf_gerais_getDate(&lcObj..dataini.text1.value,"DATA"),4) + '.xml'
			
			TRY 
				Set Safety off
				COPY MEMO uCrsSaftFile.saft TO (lcFile)
				COPY FILE (lcFile) TO (lcDirectoria)
				
                IF(!EMPTY(lcFileVal))
                    COPY FILE (lcFile) TO (lcFileVal)
                 ENDIF   

                
				Set Safety on		
				lcValidaFile = .t.
			CATCH
			
				lcValidaFile = .f.			
				lcStamp = uf_gerais_stamp()

				&& gravar nas ocorr�ncias
				TEXT TO lcSql Noshow Textmerge
					INSERT into B_ocorrencias
						(stamp, linkstamp, tipo, grau, descr,
						ovalor, dvalor,
						usr, date, site, terminal)
					values
						('<<lcStamp>>', '', 'SAFT', 2, 'N�o foi poss�vel gerar o ficheiro - possivelmente n�o tem permiss�es para escrever na directoria seleccionada.',
						'Data Inicial: <<lcDataIni>> Data Final: <<lcDataFim>>', 'Site: <<lcSite>> Directoria: <<lcDirectoria>>',
						<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()), '<<mySite>>', '<<myTermNo>>')
				ENDTEXT
				uf_gerais_actGrelha("","",lcSql)
				
			ENDTRY 
			
			REGUA(2)		
			IF lcValidaFile == .f.
				uf_perguntalt_chama("N�o foi poss�vel gerar o SAF-T. Por favor contacte o suporte. cod: Gravar Ficheiro","OK","",16)
				Fecha("uCrsSaftFile")
			ELSE
                IF(EMPTY(lcFile))
				    uf_perguntalt_chama("FICHEIRO GERADO COM SUCESSO.","OK","",64)
                ENDIF  
		
			ENDIF
			
			lcStamp = uf_gerais_stamp()
			&& gravar nas ocorr�ncias
			TEXT TO lcSql Noshow Textmerge
				INSERT into B_ocorrencias
					(stamp, linkstamp, tipo, grau, descr,
					ovalor, dvalor,
					usr, date, site, terminal)
				values
					('<<lcStamp>>', '', 'SAFT', 3, 'SAFT gerado com sucesso.',
					'Data Inicial: <<lcDataIni>> Data Final: <<lcDataFim>>', 'Site: <<lcSite>> Directoria: <<lcDirectoria>>',
					<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()), '<<mySite>>', '<<myTermNo>>')
			ENDTEXT
			uf_gerais_actGrelha("","",lcSql)
			lcResult = .t.
		ELSE 
			REGUA(2)
			uf_perguntalt_chama("N�O FORAM ENCONTRADOS MOVIMENTOS PARA GERAR O FICHEIRO.","OK","",48)
		ENDIF 
		
		Fecha("uCrsSaftFile")
	Endif
	RETURN lcResult 
ENDFUNC


** Emitir ficheiro com vendas de MNSRM - Parafarm�cia envia este ficheiro mensalmente ao Infarmed
Function uf_analises_listInfarmedMNSRM
	LOCAL lcObj
	lcObj = "relatorio.pagina2.container1.wdgfiltroreport.containerFiltros"

    LOCAL lcDataIni, lcDataFim, lcSite, lcDirectoria, lcDiretoriaLocal 
	lcDataIni = uf_gerais_getdate(&lcObj..dataini.text1.value,"SQL")
	lcDataFim = uf_gerais_getdate(&lcObj..datafim.text1.value,"SQL")
	lcSite = Alltrim(&lcObj..site.text1.value)
	**lcDirectoria = Alltrim(&lcObj..directoria.text1.value)
    **lcDirectoria = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "' and site = '" + ALLTRIM(mySite) + "'"))
    lcDirectoria = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'"))
	lcDiretoriaLocal = GETENV('TEMP')
	
	** Valida��es 
	If Ctod(&lcObj..dataini.text1.value) > Ctod(&lcObj..datafim.text1.value)
		uf_perguntalt_chama("A data final n�o pode ser inferior � data inicial.","Ok","",48)
		RETURN .f.
	ENDIF
	If Empty(lcDirectoria)
		uf_perguntalt_chama("Deve preencher o par�metro 'Caminho Relat�rio' para gerar o ficheiro.","Ok","",48)
		RETURN .f.
	Endif

    IF !uf_gerais_createDir(lcDirectoria)
    	regua(2)
        RETURN .F.
    ENDIF

	If Empty(&lcObj..site.text1.value)
		uf_perguntalt_chama("Por favor seleccione a Loja para a qual pretende emitir o ficheiro.","Ok","",48)
		RETURN .f.
	ENDIF
	**
	


	
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_relatorio_infarmed_mnsrm '<<lcDataIni>>','<<lcDataFim>>','<<lcSite>>'
	ENDTEXT
	uf_gerais_actGrelha("","uCrsMNSRM",lcSQL)  

	select uCrsMNSRM
	campos = "ano,mes,ref,qtt,pvp"
		
	lcFileName = lcDiretoriaLocal + "\listInfarmedMNSRM.csv"

	Set Safety off
	COPY TO (lcFileName) FIELDS &campos DELIMITED

	COPY FILE (lcFileName) TO (lcDirectoria)
	Set Safety on

	lcText = strtran(FILETOSTR(lcFileName),'"','')

	strtofile(left(lcText,len(lcText)-2),lcFileName)
	
	fecha("uCrsMNSRM")
	
	uf_perguntalt_chama("Ficheiro gerado com sucesso.","Ok","",64)
ENDFUNC


** Emitir ficheiro Infarmed Consumos Farm�cia Vendas
Function uf_analises_ExportIntraComunitarioDispensas
	LOCAL lcObj, lcArmazem
	lcObj = "relatorio.pagina2.container1.wdgfiltroreport.containerFiltros"
	
	** Valida��es
	**If Empty(ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "' and site = '" + ALLTRIM(mySite) + "'")))
	If Empty(ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'")))
		uf_perguntalt_chama("Deve preencher o par�metro 'Caminho Relat�rio' para poder gerar o ficheiro.","OK","",48)
		RETURN .f.
	Endif
	If Empty(&lcObj..site.text1.value)
		uf_perguntalt_chama("Por favor seleccione uma loja.","OK","",48)
		RETURN .f.
	ENDIF
	
	TEXT TO lcSql Noshow Textmerge
		select top 1 tipoempresa from empresa
	ENDTEXT
	uf_gerais_actGrelha("","curtpempr",lcSql)
	IF curtpempr.tipoempresa=='ARMAZEM'
		lcArmazem=.t.
	ELSE
		lcArmazem=.f.
	ENDIF 
**	lcArmazem = uf_gerais_getParameter('ADM0000000211','BOOL')
	
	LOCAL lcDataIni, lcDataFim, lcSite, lcDirectoria, lcDiretoriaLocal 
	lcAno = &lcObj..ano.text1.value
	lcMes = &lcObj..mes.text1.value
	lcSite = Alltrim(&lcObj..site.text1.value)
	**lcDirectoria = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "' and site = '" + ALLTRIM(mySite) + "'"))
	lcDirectoria = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'"))
	lcDiretoriaLocal = GETENV('TEMP')

    IF !uf_gerais_createDir(lcDirectoria)
    	regua(2)
        RETURN .F.
    ENDIF
	
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_relatorio_infarmed_ExportIntraComunitarioDispensas <<ALLTRIM(lcAno)>>,<<ALLTRIM(lcMes)>>,'<<ALLTRIM(lcSite)>>',<<IIF(uf_gerais_getParameter('ADM0000000211','BOOL'),1,0)>>
	ENDTEXT
	
	uf_gerais_actGrelha("","uCrsExportIntraComunitarioDispensas",lcSQL)  
	select uCrsExportIntraComunitarioDispensas

	IF  lcArmazem == .F.
		campos = "ANO,MES,REF,QTTDISPENSADA"
	ELSE
		campos = "ANO,MES,REF,QTTDISPENSADA,NIF,ENTIDADE,PAIS"
	ENDIF
		
	lcFileName = lcDiretoriaLocal + "\ExportIntraComunitarioDispensas.csv"
	
	COPY TO (lcFileName) FIELDS &campos DELIMITED WITH ""
	
	COPY FILE (lcFileName) TO (lcDirectoria)
	
**	lcText = strtran(FILETOSTR(lcFileName),'"','')
	lcText = strtran(FILETOSTR(lcFileName),'','')

	strtofile(left(lcText,len(lcText)-2),lcFileName)
	
	IF RECCOUNT("uCrsExportIntraComunitarioDispensas") == 0
		uf_perguntalt_chama("Ficheiro gerado com sucesso. N�o cont�m dados.","Ok","",64)
	ELSE
		uf_perguntalt_chama("Ficheiro gerado com sucesso.","OK","",64)
	ENDIF

	fecha("uCrsExportIntraComunitarioDispensas")

ENDFUNC


** Emitir ficheiro Infarmed Consumos Farm�cia Devolu��es
Function uf_analises_ExportIntraComunitarioRecepcoesDevolucoes
	LOCAL lcObj, lcArmazem 
	lcObj = "relatorio.pagina2.container1.wdgfiltroreport.containerFiltros"

    **uv_pasta = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "' and site = '" + ALLTRIM(mySite) + "'"))
    uv_pasta = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'"))
	
	** Valida��es 
	**If Empty(Alltrim(&lcObj..directoria.text1.value))
    IF EMPTY(uv_pasta)
    	regua(2)
		uf_perguntalt_chama("Deve preencher o par�metro 'Caminho Relat�rio' para gerar o ficheiro.","Ok","",48)
		RETURN .f.
	Endif

    IF !uf_gerais_createDir(uv_pasta)
    	regua(2)
        RETURN .F.
    ENDIF

	If Empty(&lcObj..site.text1.value)
		regua(2)
		uf_perguntalt_chama("Por favor seleccione uma loja.","OK","",48)
		RETURN .f.
	ENDIF
	
	
	TEXT TO lcSql Noshow Textmerge
		select top 1 tipoempresa from empresa
	ENDTEXT
	uf_gerais_actGrelha("","curtpempr",lcSql)
	IF curtpempr.tipoempresa=='ARMAZEM'
		lcArmazem=.t.
	ELSE
		lcArmazem=.f.
	ENDIF 
	
	**lcArmazem = uf_gerais_getParameter('ADM0000000211','BOOL')
	
	LOCAL lcDataIni, lcDataFim, lcSite, lcDirectoria, lcDiretoriaLocal 
	lcAno = &lcObj..ano.text1.value
	lcMes = &lcObj..mes.text1.value
	lcSite = Alltrim(&lcObj..site.text1.value)
	lcDirectoria = uv_pasta
	lcDiretoriaLocal = GETENV('TEMP')

	
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_relatorio_infarmed_ExportIntraComunitarioRecepcoesDevolucoes <<ALLTRIM(lcAno)>>,<<ALLTRIM(lcMes)>>,'<<lcSite>>'
	ENDTEXT
	
	uf_gerais_actGrelha("","ExportIntraComunitarioRecepcoesDevolucoes",lcSQL)  
	
	select ExportIntraComunitarioRecepcoesDevolucoes
	
	IF lcArmazem == .t.
		campos = "ANO,MES,REF,QTTRECEBIDA,NIFFORNECEDOR,FORNECEDOR,PAIS"
	ELSE
		campos = "ANO,MES,REF,QTTENCOMENDADA,QTTRECEBIDA,NIFFORNECEDOR,FORNECEDOR,PAIS"
	ENDIF
	
	lcFileName = lcDiretoriaLocal + "\ExportIntraComunitarioRecepcoesDevolucoes.csv"

	COPY TO (lcFileName) FIELDS &campos DELIMITED WITH ""
	
	COPY FILE (lcFileName) TO (lcDirectoria)

	lcText = strtran(FILETOSTR(lcFileName),'"','')

	strtofile(left(lcText,len(lcText)-2),lcFileName)
	
	IF RECCOUNT("ExportIntraComunitarioRecepcoesDevolucoes") == 0
		uf_perguntalt_chama("Ficheiro gerado com sucesso. N�o cont�m dados.","Ok","",64)
	ELSE
		uf_perguntalt_chama("Ficheiro gerado com sucesso. ","Ok","",64)
	ENDIF
	fecha("ExportIntraComunitarioRecepcoesDevolucoes")
ENDFUNC


** Emitir ficheiro com produtos para integra��o no site (Farm�cia Termal)
Function uf_analises_exportST
	LOCAL lcObj
	lcObj = "relatorio.pagina2.container1.wdgfiltroreport.containerFiltros"

    **uv_pasta = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "' and site = '" + ALLTRIM(mySite) + "'"))
    uv_pasta = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'"))
	
	&& Valida��es 
	**IF EMPTY(Alltrim(&lcObj..directoria.text1.value))
    IF EMPTY(uv_pasta)
    	regua(2)
		uf_perguntalt_chama("Deve preencher o par�metro 'Caminho Relat�rio' para gerar o ficheiro.","Ok","",48)
		RETURN .f.
	ENDIF

    IF !uf_gerais_createDir(uv_pasta)
    	regua(2)
        RETURN .F.
    ENDIF
	
	LOCAL lcDirectoria, lcDesign, lcDiretoriaLocal
	lcDirectoria = uv_pasta
	lcDesign = ALLTRIM(&lcObj..design.text1.value)
	lcDiretoriaLocal = GETENV('TEMP')
	
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_relatorio_exportTermal '<<lcDesign>>'
	ENDTEXT
	uf_gerais_actGrelha("","uCrsExportST",lcSQL)  
	select uCrsExportST
	lcFileName = lcDiretoriaLocal + "\listExportST.txt"

	COPY TO (lcFileName) DELIMITED WITH TAB
	
	COPY FILE (lcFileName) TO (lcDirectoria) 

	lcText = strtran(FILETOSTR(lcFileName),'"','')

	strtofile(left(lcText,len(lcText)-2),lcFileName)
	
	fecha("uCrsExportST")
	
	uf_perguntalt_chama("Ficheiro gerado com sucesso.","Ok","",64)
ENDFUNC

** Gerar Ficheiro de Invent�rio AT
FUNCTION uf_analises_gerarInventarioAT
	LOCAL lcValidaFile 
	
	LOCAL lcObj
	lcObj = "relatorio.pagina2.container1.wdgfiltroreport.containerFiltros"
	
	Local lcSql, lcFIle
	Store '' To lcSql, lcFile
	
	LOCAL lcData, lcDirectoria, lcTipo 
	lcData = uf_gerais_getdate(&lcObj..data.text1.value,"SQL")
	**lcDirectoria = Alltrim(&lcObj..directoria.text1.value)
    **lcDirectoria = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "' and site = '" + ALLTRIM(mySite) + "'"))
    lcDirectoria = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'"))
	lcTipo = Alltrim(&lcObj..tipo.text1.value)
	lcValorizado = ALLTRIM(&lcObj..valorizado.tag)
	************** Valida��es **************************
	**If Empty(Alltrim(&lcObj..directoria.text1.value))
    IF EMPTY(lcDirectoria)
    	regua(2)
		uf_perguntalt_chama("Deve preencher o par�metro 'Caminho Relat�rio' para gerar o ficheiro.","OK","",48)
		RETURN .f.
	ENDIF

    IF !uf_gerais_createDir(lcDirectoria)
    	regua(2)
        RETURN .F.
    ENDIF

	If Empty(Alltrim(&lcObj..tipo.text1.value))
		uf_perguntalt_chama("DEVE SELECIONAR O TIPO DO FICHEIRO.","OK","",48)
		RETURN .f.
	ENDIF

	TEXT TO lcSQL NOSHOW TEXTMERGE 
		select 
			ServerName = 
				convert(varchar,ServerProperty('machinename')) 
				+ case when convert(varchar,(SELECT local_tcp_port FROM   sys.dm_exec_connections WHERE  session_id = @@SPID)) is null then '' else ':' end
				+ isnull(convert(varchar,(SELECT local_tcp_port FROM   sys.dm_exec_connections WHERE  session_id = @@SPID)),'')
			,InstanceName = 
				case when (select ServerProperty('InstanceName')) is null then '' else '\' end
				+ convert(varchar,isnull((select ServerProperty('InstanceName')),''))
			,dbName = DB_NAME()
			,ncont = ISNULL((select top 1 ncont from empresa (nolock) where no = <<mysite_nr>>),'')
	ENDTEXT 

	IF !uf_gerais_actGrelha("","ucrsConfigDBConnection",lcSQL)  
		MESSAGEBOX("Verifique a configura��o dos Parametros Logitools. Obrigado.")
		RETURN .f.
	ENDIF 

	IF DIRECTORY(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsATInventario')

		IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsATInventario\ltsATInventario.jar')
			uf_perguntalt_chama("Ficheiro configura��o n�o encontrado. Contacte o Suporte","OK","",64)
			RETURN .f.
		ENDIF 
		
		lcFileName = ALLTRIM(lcDirectoria)+ "\" + ALLTRIM(ucrsConfigDBConnection.ncont)+ "_" + lcData + "_inventario.csv"

		lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsATInventario\ltsATInventario.jar ';
		+ ' "' + ALLTRIM(lcTipo) + '"';
		+ ' "' + ALLTRIM(ucrsConfigDBConnection.ncont) + '"';
		+ ' "' + lcData + '"';
		+ ' "' + ALLTRIM(STR(mysite_nr)) + '"' ;
		+ ' "' + ALLTRIM(lcFileName) + '"';
		+ ' "' + ALLTRIM(ucrsConfigDBConnection.dbName) + '"';
		+ ' "' + ALLTRIM(ucrsConfigDBConnection.ServerName) + '"';
		+ ' "' + ALLTRIM(ucrsConfigDBConnection.InstanceName) + '"';
		+ ' "' + ALLTRIM(lcValorizado) + '"'		
		
		
		lcWsPath = "javaw -jar " + lcWsPath
		
		_cliptext = lcWsPath 
		
		oWSShell = CREATEOBJECT("WScript.Shell")
		oWSShell.Run(lcWsPath,1,.T.)

		IF USED("ucrsConfigDBConnection")
			fecha("ucrsConfigDBConnection")
		ENDIF 

		uf_perguntalt_chama("A��o terminada. Verifique o ficheiro na directoria especificada.","Ok","",64)

	ENDIF
	
ENDFUNC 


**
FUNCTION uf_analises_gerarInfoPrex
	LOCAL lcObj
	lcObj = "relatorio.pagina2.container1.wdgfiltroreport.containerFiltros"
	
	SET POINT TO ","
	
	LOCAL lcSql, lcFIle
	STORE '' To lcSql, lcFile
	
	LOCAL lcDataIni, lcDataFim, lcSite, lcDirectoria, lcDiretoriaLocal  
	lcDataIni = uf_gerais_getdate(&lcObj..dataini.text1.value,"SQL")
	lcDataFim = uf_gerais_getdate(&lcObj..datafim.text1.value,"SQL")
	lcSite = Alltrim(&lcObj..site.text1.value)
	**lcDirectoria = Alltrim(&lcObj..directoria.text1.value)
    **lcDirectoria = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "' and site = '" + ALLTRIM(mySite) + "'"))
    lcDirectoria = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'"))
	lcDiretoriaLocal = GETENV('TEMP')

	** Valida��es
	IF Ctod(&lcObj..dataini.text1.value) > Ctod(&lcObj..datafim.text1.value)
		uf_perguntalt_chama("N�O PODE TER A DATA FINAL INFERIOR � DATA INICIAL.","OK","",48)
		SET POINT TO 
		RETURN .f.
	ENDIF
	**IF Empty(Alltrim(&lcObj..directoria.text1.value))
    IF EMPTY(lcDirectoria)  
    	regua(2)
		uf_perguntalt_chama("Deve preencher o par�metro 'Caminho Relat�rio' para gerar o ficheiro.","OK","",48)
		SET POINT TO 
		RETURN .f.
	ENDIF 

    IF !uf_gerais_createDir(lcDirectoria)
    	regua(2)
    	SET POINT TO 
        RETURN .F.
    ENDIF
	
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_relatorio_exportInfoPrex '<<lcDataIni>>','<<lcDataFim>>', '<<lcSite>>', 0
	ENDTEXT 
	uf_gerais_actGrelha("","uCrsInfoPrex",lcSQL)  

	select * FROM uCrsInfoPrex INTO CURSOR uCrsInfoPrex READWRITE 
	select uCrsInfoPrex
	GO top
	SCAN
		replace uCrsInfoPrex.pvp WITH STRTRAN(uCrsInfoPrex.pvp, '.', ',')
		replace uCrsInfoPrex.pcu WITH STRTRAN(uCrsInfoPrex.pcu, '.', ',')
		replace uCrsInfoPrex.pvp5 WITH STRTRAN(uCrsInfoPrex.pvp5, '.', ',')
	ENDSCAN 
	select uCrsInfoPrex
		
	lcFileName = ALLTRIM(lcDiretoriaLocal) + "\Infoprex_" + uf_gerais_getdate(date(),"SQL") + SYS(2) + ".txt"

**	COPY TO (lcFileName) DELIMITED WITH TAB
**	COPY FILE (lcFileName) TO (lcDirectoria) DELIMITED WITH TAB
	COPY TO (lcFileName) TYPE DELIMITED WITH "" WITH CHARACTER TAB
	COPY FILE (lcFileName) TO (lcDirectoria) 

	lcText = strtran(FILETOSTR(lcFileName),'"','')

	strtofile(left(lcText,len(lcText)-2),lcFileName)
	
	IF USED("uCrsInfoPrex")
		fecha("uCrsInfoPrex")
	ENDIF 
	
	SET POINT TO 
	
	uf_perguntalt_chama("Ficheiro gerado com sucesso.","Ok","",64)

ENDFUNC 




FUNCTION uf_analises_gerarTiMediPim
LOCAL lcObj
	lcObj = "relatorio.pagina2.container1.wdgfiltroreport.containerFiltros"
	
	SET POINT TO ","
	
	LOCAL lcSql, lcFIle , lcNameFile, lcIncExport, lcDocTipo 
	STORE '' To lcSql, lcFile,lcNameFile
	
	LOCAL lcDataIni, lcDataFim, lcSite, lcDirectoria, lcDiretoriaLocal, lcTipoCliente, lcClientePim, lcProdutoPim
	
	lcDataIni = uf_gerais_getdate(&lcObj..dataini.text1.value,"SQL")
	lcDataFim = uf_gerais_getdate(&lcObj..datafim.text1.value,"SQL")
	lcSite = Alltrim(&lcObj..site.text1.value)
	**lcDirectoria = Alltrim(&lcObj..directoria.text1.value)
    **lcDirectoria = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "' and site = '" + ALLTRIM(mySite) + "'"))
    lcDirectoria = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'"))
	lcTipoCliente = Alltrim(&lcObj..tipo.text1.value)
	lcDiretoriaLocal = GETENV('TEMP')
	lcNameFile  = Alltrim(&lcObj..nome.text1.value)
	lcDocTipo   = Alltrim(&lcObj..tipoDoc.text1.value)
	lcIncExport = IIF(&lcObj..inclExportados.tag  == 'true', 1, 0)
	lcClientePim = IIF(&lcObj..clientePim.tag  == 'true', 1, 0)
	lcProdutoPim = IIF(&lcObj..produtoPim.tag  == 'true', 1, 0)

	** Valida��es
	IF Ctod(&lcObj..dataini.text1.value) > Ctod(&lcObj..datafim.text1.value)
		uf_perguntalt_chama("N�O PODE TER A DATA FINAL INFERIOR � DATA INICIAL.","OK","",48)
		SET POINT TO 
		RETURN .f.
	ENDIF
			
	**IF Empty(Alltrim(&lcObj..directoria.text1.value))
    IF EMPTY(lcDirectoria)
    	regua(2)
		uf_perguntalt_chama("Deve preencher o par�metro 'Caminho Relat�rio' para gerar o ficheiro.","OK","",48)
		SET POINT TO 
		RETURN .f.
	ENDIF

    IF !uf_gerais_createDir(lcDirectoria)
    	regua(2)
    	SET POINT TO 
        RETURN .F.
    ENDIF
	
	oRE = CreateObject("VBScript.RegExp")
	oRE.Pattern ='^[a-zA-Z0-9_.-]*$'
	
	IF (NOT EMPTY(lcNameFile))
		IF (not oRE.test(lcNameFile))
			uf_perguntalt_chama("O NOME APENAS PODE CONTER LETRAS E N�MEROS.","OK","",48)
			SET POINT TO 
			RETURN .f.
		ENDIF 
	ENDIF 
	
	fecha("uCrsvendasPim")
	

	LOCAL lcSQLTemp 
	lcSQLTemp  = ''
	TEXT TO lcSQLTemp  NOSHOW TEXTMERGE 
		exec up_relatorio_vendas_tiMedi '<<lcDataIni>>','<<lcDataFim>>', '<<lcSite>>', '<<lcTipoCliente>>',<<lcIncExport>>,<<lcClientePim>>,<<lcProdutoPim>>
	ENDTEXT 
	

	
	uf_gerais_actGrelha("","uCrsvendasPim",lcSQLTemp  )  

	select * FROM uCrsvendasPim INTO CURSOR uCrsvendasPim READWRITE 	

	select uCrsvendasPim

	IF Empty(Alltrim(lcNameFile))	
		lcFileName = ALLTRIM(lcDiretoriaLocal) + "\pim_" + uf_gerais_getdate(date(),"SQL") + SYS(2) + lcDocTipo
	
	ELSE
		lcFileName = ALLTRIM(lcDiretoriaLocal) + "\"+ lcNameFile + lcDocTipo
	
	ENDIF
	 

**	COPY TO (lcFileName) DELIMITED WITH TAB
**	COPY FILE (lcFileName) TO (lcDirectoria) DELIMITED WITH TAB
	COPY TO (lcFileName) TYPE DELIMITED WITH "" WITH CHARACTER ";"
	COPY FILE (lcFileName) TO (lcDirectoria) 

	lcText = strtran(FILETOSTR(lcFileName),'"','')

	strtofile(left(lcText,len(lcText)-2),lcFileName)
	
	IF USED("uCrsvendasPim")
		fecha("uCrsvendasPim")
	ENDIF 
	
	SET POINT TO 
	
	uf_perguntalt_chama("Ficheiro gerado com sucesso.","Ok","",64)
	
	&& marcar como exportado	
	LOCAL lcSQLTemp1 
	lcSQLTemp1  = ''
	TEXT TO lcSQLTemp1  NOSHOW TEXTMERGE 
		exec up_marca_vendas_exportadas_vendas_tiMedi '<<lcDataIni>>','<<lcDataFim>>', '<<lcSite>>', '<<lcTipoCliente>>',<<lcIncExport>>,'<<m_chinis>>'
	ENDTEXT 
	
	if(!uf_gerais_actGrelha("","",lcSQLTemp1)) 
		uf_perguntalt_chama("N�o foi possivel marcar a vendas PIM como exportadas. Por favor contacte o suporte.","OK","",48)
	ENDIF
	

ENDFUNC 



**
FUNCTION uf_analises_gerarListagemVendasSuspensas
	LOCAL lcObj
	lcObj = "relatorio.pagina2.container1.wdgfiltroreport.containerFiltros"
	
	SET POINT TO ","
	
	LOCAL lcSql, lcFIle , lcNameFile
	STORE '' To lcSql, lcFile,lcNameFile
	
	LOCAL lcDataIni, lcDataFim, lcSite, lcDirectoria, lcDiretoriaLocal, lcTipoCliente
	lcDataIni = uf_gerais_getdate(&lcObj..dataini.text1.value,"SQL")
	lcDataFim = uf_gerais_getdate(&lcObj..datafim.text1.value,"SQL")
	lcSite = Alltrim(&lcObj..site.text1.value)
	**lcDirectoria = Alltrim(&lcObj..directoria.text1.value)
    **lcDirectoria = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "' and site = '" + ALLTRIM(mySite) + "'"))
    lcDirectoria = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'"))
	lcTipoCliente = Alltrim(&lcObj..tipo.text1.value)
	lcDiretoriaLocal = GETENV('TEMP')
	lcNameFile=Alltrim(&lcObj..nome.text1.value)

	** Valida��es
	IF Ctod(&lcObj..dataini.text1.value) > Ctod(&lcObj..datafim.text1.value)
		uf_perguntalt_chama("N�O PODE TER A DATA FINAL INFERIOR � DATA INICIAL.","OK","",48)
		SET POINT TO 
		RETURN .f.
	ENDIF
			
	**IF Empty(Alltrim(&lcObj..directoria.text1.value))
    IF EMPTY(lcDirectoria)
    	regua(2)
		uf_perguntalt_chama("Deve preencher o par�metro 'Caminho Relat�rio' para gerar o ficheiro.","OK","",48)
		SET POINT TO 
		RETURN .f.
	ENDIF

    IF !uf_gerais_createDir(lcDirectoria)
    	regua(2)
    	SET POINT TO 
        RETURN .F.
    ENDIF
	
	oRE = CreateObject("VBScript.RegExp")
	oRE.Pattern ='^[a-zA-Z0-9_.-]*$'
	
	IF (NOT EMPTY(lcNameFile))
		IF (not oRE.test(lcNameFile))
			uf_perguntalt_chama("O NOME APENAS PODE CONTER LETRAS E N�MEROS.","OK","",48)
			SET POINT TO 
			RETURN .f.
		ENDIF 
	ENDIF 
	
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_listagem_vendasSuspensas '<<lcDataIni>>','<<lcDataFim>>', '<<lcSite>>', '<<lcTipoCliente>>'
	ENDTEXT 
	uf_gerais_actGrelha("","uCrsvendasSuspensas",lcSQL)  

	select * FROM uCrsvendasSuspensas INTO CURSOR uCrsvendasSuspensas READWRITE 

	select uCrsvendasSuspensas
	
	IF Empty(Alltrim(lcNameFile))	
		lcFileName = ALLTRIM(lcDiretoriaLocal) + "\listagem_vendasSuspensas_" + uf_gerais_getdate(date(),"SQL") + SYS(2) + ".csv"
	
	ELSE
		lcFileName = ALLTRIM(lcDiretoriaLocal) + "\"+ lcNameFile + ".csv"
	
	ENDIF
	 

**	COPY TO (lcFileName) DELIMITED WITH TAB
**	COPY FILE (lcFileName) TO (lcDirectoria) DELIMITED WITH TAB
	COPY TO (lcFileName) TYPE DELIMITED WITH "" WITH CHARACTER ";"
	COPY FILE (lcFileName) TO (lcDirectoria) 

	lcText = strtran(FILETOSTR(lcFileName),'"','')

	strtofile(left(lcText,len(lcText)-2),lcFileName)
	
	IF USED("uCrsvendasSuspensas")
		fecha("uCrsvendasSuspensas")
	ENDIF 
	
	SET POINT TO 
	
	uf_perguntalt_chama("Ficheiro gerado com sucesso.","Ok","",64)

ENDFUNC 


**
FUNCTION uf_analises_gerarStocks_MovimentosTotais
	LOCAL lcObj
	lcObj = "relatorio.pagina2.container1.wdgfiltroreport.containerFiltros"
	
	SET POINT TO ","
	
	LOCAL lcSql, lcFIle
	STORE '' To lcSql, lcFile
	
	LOCAL lcDataIni, lcDataFim, lcLote, lcDirectoria, lcDiretoriaLocal, lcModo ,lcArmazem, lcMovimento
	
	** o relatorio passa sempre o modo sempre a 1 
	lcModo=1 
	lcDataIni = uf_gerais_getdate(&lcObj..dataIni.text1.value,"SQL")
	lcDataFim = uf_gerais_getdate(&lcObj..datafim.text1.value,"SQL")
	lcLote = Alltrim(&lcObj..lote.text1.value)
	**lcDirectoria = Alltrim(&lcObj..directoria.text1.value)
    **lcDirectoria = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "' and site = '" + ALLTRIM(mySite) + "'"))
    lcDirectoria = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'"))
	lcMovimento= Alltrim(&lcObj..movimento.text1.value)
	lcArmazem= Alltrim(&lcObj..armazem.text1.value)
	lcDiretoriaLocal = GETENV('TEMP')

	** Valida��es
	IF Ctod(&lcObj..dataini.text1.value) > Ctod(&lcObj..datafim.text1.value)
		uf_perguntalt_chama("N�O PODE TER A DATA FINAL INFERIOR � DATA INICIAL.","OK","",48)
		SET POINT TO 
		RETURN .f.
	ENDIF
	**IF Empty(Alltrim(&lcObj..directoria.text1.value))
    IF EMPTY(lcDirectoria)
    	regua(2)
		uf_perguntalt_chama("Deve preencher o par�metro 'Caminho Relat�rio' para gerar o ficheiro.","OK","",48)
		SET POINT TO 
		RETURN .f.
	ENDIF 

    IF !uf_gerais_createDir(lcDirectoria)
    	regua(2)
    	SET POINT TO 
        RETURN .F.
    ENDIF
	
	IF !uf_perguntalt_chama("Este mapa considera todos os movimentos dos artigos no per�odo de tempo selecionado.Aconselhamos a gerar o relat�rio com a loja encerrada, uma vez que poder� afetar o desempenho da aplica��o. TEM A CERTEZA QUE QUER CONTINUAR?","Sim","N�o")
		regua(2)
		SET POINT TO 
		RETURN .F.
	ENDIF	
	
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_stocks_MovimentosTotais '<<lcArmazem>>' ,'<<lcDataIni>>','<<lcDataFim>>', <<lcModo>> ,'<<lcLote>>', '<<lcMovimento>>'
	ENDTEXT 
	uf_gerais_actGrelha("","ucrsmovimentosstockstodos",lcSQL)  
	
	select * FROM ucrsmovimentosstockstodos INTO CURSOR ucrsmovimentosstockstodos READWRITE 

	select ucrsmovimentosstockstodos
		
	lcFileName = ALLTRIM(lcDiretoriaLocal) + "\ListagemExtratoMovimentosStocks_" + uf_gerais_getdate(date(),"SQL") + SYS(2) + ".csv"

**	COPY TO (lcFileName) DELIMITED WITH TAB
**	COPY FILE (lcFileName) TO (lcDirectoria) DELIMITED WITH TAB

	COPY TO (lcFileName) TYPE DELIMITED WITH "" WITH CHARACTER TAB
	COPY FILE (lcFileName) TO (lcDirectoria) 

	lcText = strtran(FILETOSTR(lcFileName),'"','')

	strtofile(left(lcText,len(lcText)-2),lcFileName)
	
	IF USED("uCrsMovimentosStocksTodos")
		fecha("uCrsMovimentosStocksTodos")
	ENDIF 
	
	SET POINT TO 
	
	uf_perguntalt_chama("Ficheiro gerado com sucesso.","Ok","",64)

ENDFUNC 

FUNCTION uf_analises_downloadReport
	LPARAMETERS uv_format, uv_print, uv_showFolder
	
	LOCAL lcFiltros, lcfiltrosCursor, lcReport, lcNomeAnalise
    
	lcReport = ""
    lcNomeAnalise = ''
    lcURL = ''
    


    regua(0,6,"A guardar Ficheiro...")
	regua(1,1,"A guardar Ficheiro...")
		
	regua(1,2,"A guardar Ficheiro...")

    **uv_pasta = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "' and site = '" + ALLTRIM(mySite) + "'"))
    uv_pasta = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'"))
    

	IF EMPTY(uv_pasta) AND !uv_print
	   	regua(2)
		uf_perguntalt_chama("Tem de preencher o par�metro 'Caminho Relat�rio' pra poder efectuar o download do ficheiro.","OK","",16)    
		RETURN .F.
	ENDIF

    IF !uf_gerais_createDir(uv_pasta)
    	regua(2)
        RETURN .F.
    ENDIF

	** valida��es **
	lcTxtRel = IIF(TYPE("relatorio") == "U","painelCentral.pagina" + astr(painelCentral.pagina),"relatorio.pagina2") + ".container1.wdgselreport.txtAnalise"
	IF EMPTY(ALLTRIM(&lcTxtRel..value))
		uf_perguntalt_chama("DEVE SELECIONAR UMA AN�LISE PARA EXECUTAR. POR FAVOR VERIFIQUE.","OK","",64)
        regua(2)
		RETURN .f.
	ENDIF
	lcObj = IIF(TYPE("relatorio") == "U","painelCentral.pagina" + astr(painelCentral.pagina),"relatorio.pagina2") + ".container1.wdgfiltroreport.containerFiltros."
	**********************
	
	IF uf_analises_validaCamposObrigatorios(lcObj) == .f.
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER, POR FAVOR VERIFIQUE.","OK","",64)
        regua(2)
		RETURN .f.
	ENDIF

    regua(1,3,"A guardar Ficheiro...")
	
	lcFiltros = ''
	**Cursor com o nome dos filtros, para contruir link
	SELECT ucrsAnalisesDef
	GO TOP
	SCAN
		lcObj = IIF(TYPE("relatorio") == "U","painelCentral.pagina" + astr(painelCentral.pagina),"relatorio.pagina2") + ".container1.wdgfiltroreport.containerFiltros."
		Do CASE
			Case UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) == "S"
				lcObj = lcObj + ALLTRIM(ucrsAnalisesDef.nome) + ".Spinner1"
				lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "=" + ASTR(&lcObj..value)

			Case UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) == "D" Or UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) = "H"
				lcObj = lcObj + ALLTRIM(ucrsAnalisesDef.nome) + ".Text1"
				lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(&lcObj..value))
		
			Case UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) == "CB" && combos
				lcObj = lcObj + ALLTRIM(ucrsAnalisesDef.nome) + ".combo1"
				lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(&lcObj..value))
					
			Case UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) == "C" && check box ultiliza a propriedade Tag; devolve true ou false
				lcObj = lcObj + ALLTRIM(ucrsAnalisesDef.nome)
				lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(&lcObj..tag))
		
			Case UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) == "T" && text box
				lcObj = lcObj + ALLTRIM(ucrsAnalisesDef.nome) + ".Text1"
				TRY
					lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(STR(&lcObj..value)))
				CATCH
					lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(&lcObj..value))
				ENDTRY
			Case UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) == "L" && check box
				lcObj = lcObj + ALLTRIM(ucrsAnalisesDef.nome) && + ".Check1"
				lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(&lcObj..tag))
		
			Case UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) == "SB" && select box
				**cursor com base no nome do obj
				lcCrsSB = "ucrsSb" + ALLTRIM(ucrsAnalisesDef.nome)
				lcfiltrosCursor = ""
				IF USED(lcCrsSB)
					SELECT &lcCrsSB
					GO Top
					SCAN
						IF &lcCrsSB..sel == .t.
							lcfiltrosCursor = lcfiltrosCursor + ALLTRIM(&lcCrsSB..opcao) + ","
						ENDIF
					ENDSCAN
				ENDIF
				lcfiltrosCursor = LEFT(ALLTRIM(lcfiltrosCursor),LEN(ALLTRIM(lcfiltrosCursor))-1)
				lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(lcfiltrosCursor)) 
			
			Case UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) == "VC"
				IF ALLTRIM(ucrsAnalisesDef.nome) != "relatorio"
					lcObj = lcObj + ALLTRIM(ucrsAnalisesDef.nome) + ".Text1"
					lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(&lcObj..value))
				ELSE
					lcObj = lcObj + ALLTRIM(ucrsAnalisesDef.nome) + ".Text1"
					lcReport = uf_gerais_trataCarateresReservadosURL(ALLTRIM(&lcObj..value))	
				ENDIF	
			Case UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) == "FM"
				lcObj = lcObj + ALLTRIM(ucrsAnalisesDef.nome) + ".Text1"
				lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "=" +  uf_gerais_trataCarateresReservadosURL(ALLTRIM(&lcObj..value))
				
			Case UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) == "OP"
				
				lcObjTag = lcObj + ALLTRIM(ucrsAnalisesDef.nome) && + ".Check1"
				lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "Tag=" + &lcObjTag..tag
				
				lcObj2 = lcObj + ALLTRIM(ucrsAnalisesDef.nome) + ".Text1"
				lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "=" + ALLTRIM(&lcObj2..value)
				
				lcObjOperador = lcObj + ALLTRIM(ucrsAnalisesDef.nome) + ".operadorLogico"
				lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "Op=" + ALLTRIM(&lcObjOperador..value)
				
			Case UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) == "DCI"
				
				IF VARTYPE(pv_filtradci_dci)=="U"			
					lcFiltros = lcFiltros + "&dci="
					lcFiltros = lcFiltros + "&dciDesc="
				ELSE
					lcFiltros = lcFiltros + "&dci=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(pv_filtradci_dci))
					lcFiltros = lcFiltros + "&dciDesc=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(pv_filtradci_dciDesc))
				ENDIF 
				
				IF VARTYPE(pv_filtradci_gh)=="U"			
					lcFiltros = lcFiltros + "&grphmgcode="
					lcFiltros = lcFiltros + "&grphmgcodeDesc="
				ELSE
					lcFiltros = lcFiltros + "&grphmgcode=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(pv_filtradci_gh))
					lcFiltros = lcFiltros + "&grphmgcodeDesc=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(pv_filtradci_ghDesc))
				ENDIF 
				
				IF VARTYPE(pv_filtradci_cnpem)=="U"			
					lcFiltros = lcFiltros + "&cnpem="
					lcFiltros = lcFiltros + "&cnpemDesc="
				ELSE
					lcFiltros = lcFiltros + "&cnpem=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(pv_filtradci_cnpem))
					lcFiltros = lcFiltros + "&cnpemDesc=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(pv_filtradci_cnpemDesc))
				ENDIF 
				
			Case UPPER(ALLTRIM(ucrsAnalisesDef.tipo)) == "VCF"
				IF ALLTRIM(ucrsAnalisesDef.nome) != "relatorio"
					lcObj = lcObj + ALLTRIM(ucrsAnalisesDef.nome)
					lcFiltros = lcFiltros + "&"+ ALLTRIM(ucrsAnalisesDef.nome) + "=" + uf_gerais_trataCarateresReservadosURL(ALLTRIM(&lcObj..tag))
				ELSE
					lcObj = lcObj + ALLTRIM(ucrsAnalisesDef.nome) 
					lcReport = uf_gerais_trataCarateresReservadosURL(ALLTRIM(&lcObj..tag))	
				ENDIF	
							
				
			Otherwise
				**
		Endcase
	ENDSCAN
	
    regua(1,4,"A guardar Ficheiro...")

	lcObj = IIF(TYPE("relatorio") == "U","painelCentral.pagina" + astr(painelCentral.pagina),"relatorio.pagina2") + ".container1.wdgselreport.lcComandoFox"
	IF EMPTY(ALLTRIM(&lcObj))
		** chamar report
		lcObj = IIF(TYPE("relatorio") == "U","painelCentral.pagina" + astr(painelCentral.pagina),"relatorio.pagina2") + ".container1.wdgselreport.lcReport"
		
		** Nome Servidor
		LOCAL nomeServidor, lcReport, lcVersaoSoft 
		nomeServidor = uf_gerais_getParameter('ADM0000000045', 'text')
		lcVersaoSoft = "Versao_" + STRTRAN(ALLTRIM(uf_gerais_getParameter('ADM0000000156', 'text')),".","_")

		IF EMPTY(nomeServidor)
			uf_perguntalt_chama("N�O FOI POSSIVEL ENCONTRAR O NOME DO SERVIDOR. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
            regua(2)
			RETURN .f.
		ENDIF
		
		IF EMPTY(lcVersaoSoft)
			uf_perguntalt_chama("N�O FOI POSSIVEL DETERMINAR A VERS�O DO SOFTWARE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
            regua(2)
			RETURN .f.
		ENDIF
		
		IF USED("ucrsDadosServidorReport")
			fecha("ucrsDadosServidorReport")
		ENDIF 

        regua(1,5,"A guardar Ficheiro...")
		
		lcSQL = ""
		**
		IF uf_gerais_getParameter_site('ADM0000000027', 'BOOL', mySite) = .f.
			TEXT TO lcSQL NOSHOW TEXTMERGE
				SELECT ServerName = 
						convert(varchar,ServerProperty('machinename')) 
						+ case when (select ServerProperty('InstanceName')) is null then '' else '\' end
						+ convert(varchar,isnull((select ServerProperty('InstanceName')),''))
						+ case when convert(varchar,(SELECT local_tcp_port FROM   sys.dm_exec_connections WHERE  session_id = @@SPID)) is null then '' else ',' end
						+ isnull(convert(varchar,(SELECT local_tcp_port FROM   sys.dm_exec_connections WHERE  session_id = @@SPID)),'')
						, dbName = DB_NAME() 
			ENDTEXT 
		ELSE
			TEXT TO lcSQL NOSHOW TEXTMERGE
				SELECT ServerName = (select textvalue from b_parameters_site where stamp='ADM0000000028' and site='<<ALLTRIM(mySite)>>')
					, dbName = (select textvalue from b_parameters_site where stamp='ADM0000000029' and site='<<ALLTRIM(mySite)>>')
			ENDTEXT 
		ENDIF 
		IF !uf_gerais_actGrelha("",[ucrsDadosServidorReport],lcSQL )
			uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR A LOCALIZA��O DA BD. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
            regua(2)
			RETURN .F.
		ENDIF		
		
	
		
		**Se exsitir um parametros chamado "relatorio" considera que esse filtro � o nome do relat�rio
		IF EMPTY(lcReport)
			lcReport = &lcObj
		ELSE
			SELECT ucrsAnalisesDef
			GO TOp
			lcOrdem = ucrsAnalisesDef.ordem

			TEXT TO lcSQL NOSHOW TEXTMERGE 
				exec up_relatorio_MultiRelatorio <<lcOrdem>>, '<<ALLTRIM(lcReport)>>'
			ENDTEXT

			IF !uf_gerais_actGrelha("","ucrsAnalisesMultiReport",lcSQL)
				uf_perguntalt_chama("Defini��es de relat�rio incorretas.","OK","",48)
                regua(2)
				RETURN .f.
			ENDIF 
			IF RECCOUNT("ucrsAnalisesMultiReport")>0
				Select ucrsAnalisesMultiReport
				lcReport = ALLTRIM(ucrsAnalisesMultiReport.relatorio)
			ELSE
				lcReport = &lcObj
			ENDIF 
		ENDIF

        regua(1,6,"A guardar Ficheiro...")
        
   
	
		Select ucrsDadosServidorReport
		TEXT TO lcURL TEXTMERGE NOSHOW
http://<<Alltrim(nomeServidor)>>/ReportServer/Pages/ReportViewer.aspx?%2f<<Alltrim(lcVersaoSoft)>>%2f<<ALLTRIM(lcReport)>>&rs:Command=Render&rs:ParameterLanguage=pt-PT&rc:Parameters=false&ServidorSQL=<<ALLTRIM(ucrsDadosServidorReport.ServerName)>>&BaseDados=<<ALLTRIM(ucrsDadosServidorReport.dbName)>><<ALLTRIM(lcFiltros)>>&rs:format=<<IIF(!uv_print, ALLTRIM(uv_format), 'PDF')>>
		ENDTEXT

		
		
		lcNomeAnalise = IIF(TYPE("relatorio") == "U","painelCentral.pagina" + astr(painelCentral.pagina),"relatorio.pagina2") + ".container1.wdgselreport.txtAnalise"
		lcNomeAnalise = ALLTRIM(&lcNomeAnalise..value)
				
	ENDIF

    regua(1,7,"A guardar Ficheiro...")
    
   

    IF !EMPTY(lcURL)
    

	IF uv_print		
			select uCrsE1
	        uv_output = ALLTRIM(uf_gerais_getParameter('ADM0000000015','TEXT')) + "\Cache\PDF\" + ALLTRIM(lcNomeAnalise) + "_" + ALLTRIM(uCrsE1.id_lt) + ".pdf" 
	ELSE

        uv_day = RIGHT('00' + ASTR(DAY(DATE())), 2)

        uv_month = RIGHT('00' + ASTR(MONTH(DATE())), 2)

        uv_year = ASTR(YEAR(DATE()))

        uv_time = STRTRAN(TIME(), ":", "")

        uv_date = uv_day + uv_month + uv_year + "_" + uv_time
		select uCrsE1
	    uv_output = ALLTRIM(uv_pasta) + "\" + ALLTRIM(lcNomeAnalise) + "_" + uv_date + "_" + ALLTRIM(uCrsE1.id_lt) + "." + IIF(ALLTRIM(UPPER(uv_format)) == 'EXCEL', 'xls', ALLTRIM(uv_format))

	ENDIF

	

		

        TEXT TO uv_request TEXTMERGE NOSHOW
$source = '<<ALLTRIM(lcURL)>>'
$destination = '<<ALLTRIM(uv_output)>>'
Invoke-WebRequest -Uri $source -OutFile $destination -UseDefaultCredential
        ENDTEXT
        
		uf_gerais_getRequest(uv_request, uv_output)
		IF !FILE(uv_output )
            uf_perguntalt_chama("Erro ao gerar ficheiro! Por favor contacte o Suporte.","OK","",16)
            regua(2)
            RETURN .F.
        ELSE
        	IF !uv_print AND uv_showFolder
        		

				DECLARE INTEGER ShellExecute IN shell32.dll ; 
				  INTEGER hndWin, ; 
				  STRING cAction, ; 
				  STRING cFileName, ; 
				  STRING cParams, ;  
				  STRING cDir, ; 
				  INTEGER nShowWin

				ShellExecute(0,'explore',uv_pasta,'','',1)
        	ENDIF
        ENDIF

        regua(1,8,"A guardar Ficheiro...")

        IF uv_print

			TEXT TO uv_request TEXTMERGE NOSHOW
Start-Process �FilePath '<<ALLTRIM(uv_output)>>' -Verb Print | out-printer 'TSPrint Printer' | kill
			ENDTEXT

			uf_gerais_getRequest(uv_request, "")

        ENDIF

    ENDIF

    regua(2)

    IF !uv_print AND !uv_showFolder

        uf_perguntalt_chama("Ficheiro gerado com sucesso.","Ok","",64)

    ENDIF

ENDFUNC


**
FUNCTION uf_analises_gerarInfoCovid
	LOCAL lcObj
	lcObj = "relatorio.pagina2.container1.wdgfiltroreport.containerFiltros"
	
	SET POINT TO ","
	
	LOCAL lcSql, lcFIle
	STORE '' To lcSql, lcFile
	
	LOCAL lcDataIni, lcDataFim, lcSite, lcDirectoria, lcDiretoriaLocal  
	lcDataIni = uf_gerais_getdate(&lcObj..dataini.text1.value,"SQL")
	lcDataFim = uf_gerais_getdate(&lcObj..datafim.text1.value,"SQL")
	lcSite = Alltrim(&lcObj..site.text1.value)
	**lcDirectoria = Alltrim(&lcObj..directoria.text1.value)
    **lcDirectoria = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "' and site = '" + ALLTRIM(mySite) + "'"))
    lcDirectoria = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'"))
	lcDiretoriaLocal = GETENV('TEMP')

	** Valida��es
	IF Ctod(&lcObj..dataini.text1.value) > Ctod(&lcObj..datafim.text1.value)
		uf_perguntalt_chama("N�O PODE TER A DATA FINAL INFERIOR � DATA INICIAL.","OK","",48)
		SET POINT TO 
		RETURN .f.
	ENDIF
	**IF Empty(Alltrim(&lcObj..directoria.text1.value))
    IF EMPTY(lcDirectoria)  
    	regua(2)
		uf_perguntalt_chama("Deve preencher o par�metro 'Caminho Relat�rio' para gerar o ficheiro.","OK","",48)
		SET POINT TO 
		RETURN .f.
	ENDIF 

    IF !uf_gerais_createDir(lcDirectoria)
    	regua(2)
    	SET POINT TO 
        RETURN .F.
    ENDIF
	
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_relatorio_Covid '<<lcDataIni>>','<<lcDataFim>>', '<<lcSite>>'
	ENDTEXT 
	uf_gerais_actGrelha("","uCrsInfoCovid",lcSQL)  

	select * FROM uCrsInfoCovid INTO CURSOR uCrsInfoCovid READWRITE 
	select uCrsInfoCovid
		
	lcFileName = ALLTRIM(lcDiretoriaLocal) + "\autoTestes" + "_" + lcSite +"_" + uf_gerais_getdate(date(),"SQL") + SYS(2) + ".csv"

**	COPY TO (lcFileName) DELIMITED WITH TAB
  **COPY FILE (lcFileName) TO (lcDirectoria) DELIMITED WITH ','
    COPY TO (lcFileName) TYPE DELIMITED WITH  "" WITH CHARACTER ','
	**COPY TO(lcFileName) TYPE CSV
	COPY FILE (lcFileName) TO (lcDirectoria) 

	lcText = strtran(FILETOSTR(lcFileName),'"','')

	strtofile(left(lcText,len(lcText)-2),lcFileName)
	
	IF USED("uCrsInfoCovid")
		fecha("uCrsInfoCovid")
	ENDIF 
	
	SET POINT TO 
	
	uf_perguntalt_chama("Ficheiro gerado com sucesso.","Ok","",64)

ENDFUNC 

FUNCTION uf_analises_gerarRelFaturacaoDet

        uv_nomeJar = 'FaturacaoEletronicaMedicamentos.jar'
                
        IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsFaturacaoEletronica\' + ALLTRIM(uv_nomeJar))
            uf_perguntalt_chama("FICHEIRO DE CONFIGURA��O N�O ENCONTRADO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
            regua(2)
            RETURN .f.
        ENDIF 

		uv_obj = "relatorio.pagina2.container1.wdgfiltroreport.containerFiltros"

        regua(1,2,"A recolher informa��o para envio...")

        uv_StampLigacao = uf_gerais_stamp()
        uv_Ano = ALLTRIM(&uv_obj..ano.text1.value)
        uv_Mes = ALLTRIM(&uv_obj..mes.text1.value)
        uv_StampFaturaSNS = "AFP"
        uv_ResultadoFatura = "0"
		uv_loja = ALLTRIM(&uv_obj..site.text1.value)
		uv_ResultadoFatura = "0"

		IF INLIST(ALLTRIM(UPPER(sql_db)), 'LTDEV30', 'LTSTAG', 'E01322A_TESTES')
        	uv_IDCliente = "E01322A"
		ELSE
			uv_IDCliente = ALLTRIM(UPPER(sql_db))
		ENDIF

        uv_outFolder = ALLTRIM(uf_gerais_getParameter('ADM0000000015','TEXT')) + "\Cache\" + uf_gerais_stamp()

		uv_EntId = ""

		IF uf_gerais_actGrelha("", "uc_entsTmp", "exec up_relatorio_getEntidades")
			IF RECCOUNT("uc_entsTmp") <> 0
				SELECT uc_entsTmp
				LOCATE FOR ALLTRIM(uc_entsTmp.nome) == ALLTRIM(&uv_obj..ENTIDADE.text1.value)
				IF FOUND()
					uv_EntId = ALLTRIM(uc_entsTmp.num)
				ENDIF
			ENDIF
		ENDIF

		IF EMPTY(uv_EntId)
            uf_perguntalt_chama("Erro com a Entidade selecionada! Por favor entre em contacto com o suporte." ,"OK","",16)
            regua(2)
            RETURN  .f.
		ENDIF

        regua(1,3,"A enviar informa��o...")

        lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsFaturacaoEletronica\' + ALLTRIM(uv_NomeJar);
                + " " + ["] + ALLTRIM(uv_StampLigacao) + ["];	
                + " " + ["] + uv_StampFaturaSNS + ["];
                + " " + ["] + uv_IDCliente + ["];
                + " " + uv_Ano;
                + " " + uv_Mes;
                + " " + ["] + ALLTRIM(uv_loja) + ["];
                + " " + uv_ResultadoFatura;
                + " 0";
                + " 0"; 
                + " " + ["] + alltrim(uv_EntId) + ["];
            	+ " " + ["] + ALLTRIM(uv_outFolder) + ["];
                + " 1"

        lcWsPath = "javaw -jar " + lcWsPath 

        **_cliptext = lcWsPath

        oWSShell = CREATEOBJECT("WScript.Shell")
        oWSShell.Run(lcWsPath, 1, .t.)

        regua(1,4,"A verificar resposta dos Servi�os Partilhados...")

        uv_fileCount = 0

        oFSO = CreateObject("Scripting.FileSystemObject") 

        FOR i = 1 TO ADIR(uv_files,ALLTRIM(uv_outFolder) + "\*.zip")

            uv_outPutFile = ALLTRIM(uv_outFolder) + "\" + ALLTRIM(uv_files(i, 1))
            uv_userFileFolder = ALLTRIM(uf_gerais_getUmValor("B_Terminal", "LTRIM(RTRIM(pathExport))", "terminal = '" + alltrim(myTerm) + "'")) + "\" + ALLTRIM(uv_files(i, 1))
                        
            oFile = oFSO.GetFile(uv_outPutFile) 
            oFile.Copy(uv_userFileFolder)    
            
            uv_fileCount = uv_fileCount + 1
        NEXT

        IF uv_fileCount = 0
            uf_perguntalt_chama("N�o foi exportado nenhum documento! Verifique os filtros da an�lise." ,"OK","",16)
            regua(2)
            RETURN  .f.
        ELSE
            uf_perguntalt_chama("Documento exportado com sucesso." ,"OK","",64)
            regua(2)
        ENDIF

ENDFUNC




** Pepara dados do saft de factura��o para enviar
** uf_analises_enviarSaftEl("VALIDAR")
** uf_analises_enviarSaftEl("ENVIAR")

 FUNCTION uf_analises_enviarSaftEl
    LPARAMETERS lcOpSaft

    local lcAno, lcMes, lcObj, uv_pass, lcDataIni, lcEmpresaNif, pathToFile, lcNomeFicheiro, pathToFileResponse
	
    IF(!USED("ucrse1") and !USED(lcObj))
         RETURN .F.
    ENDIF

    IF(EMPTY(ucrse1.ncont))
        RETURN .F.
    ENDIF
 		
    SELECT ucrse1 
    GO TOP
    lcEmpresaNif = ALLTRIM(ucrse1.ncont)

    lcObj = "relatorio.pagina2.container1.wdgfiltroreport.containerFiltros"
    
    lcDataIni = uf_gerais_getdate(&lcObj..dataini.text1.value,"SQL")

	IF(EMPTY(lcDataIni))
		RETURN .F.
	ENDIF
	 
    IF(EMPTY(lcDataIni) OR VAL(LEFT(lcDataIni,4)) < 2000 OR VAL(LEFT(lcDataIni,4)) > 3000 ) 
        uf_perguntalt_chama("Data para retirada do saft invalida","OK","",64)	
        RETURN .F.
    ENDIF

    lcAno = LEFT(lcDataIni,4)
    lcMes = SUBSTR(lcDataIni,5,2)

    lcNomeFicheiro = lcEmpresaNif
    

 
    pathToFile =  ALLTRIM(uf_gerais_getParameter('ADM0000000015','TEXT')) + "\Cache\XML\" + ALLTRIM(lcNomeFicheiro) + ".xml"
    pathToFileResponse=  ALLTRIM(uf_gerais_getParameter('ADM0000000015','TEXT')) + "\Cache\XML\" + ALLTRIM(lcNomeFicheiro)  + "_resp" + ".xml"  


    IF(EMPTY(uf_analises_gerarSaftPT("DF", pathToFile)))
    	RETURN .f.
    ENDIF
   
	IF(!FILE(pathToFile))
	    uf_perguntalt_chama("Ocorreu um erro ao aceder a localizacao do ficheiro","OK","",64)	
    	RETURN .f.
    ENDIF

 
    public myPassAT   

    uf_tecladoalpha_Chama('myPassAT', 'INTRODUZA PASSWORD DAS FINANCAS', .t., .f., 0)

    IF EMPTY(myPassAT)
        RETURN .F.
    ENDIF
    
    uv_pass= ALLTRIM(myPassAT)
    RELEASE myPassAT   


    IF(EMPTY(lcOpSaft) OR !ALLTRIM(LOWER(lcOpSaft)) == "validar" AND !ALLTRIM(LOWER(lcOpSaft)) == "enviar" ) 
        uf_perguntalt_chama("Ocorreu um erro ao escolher a opera��o(Enviar, Validar)","OK","",64)	
        RETURN .F.
    ENDIF
    
    LOCAL lcResultSaftEl
    
    lcResultSaftEl=uf_gerais_enviaSaftEl(lcEmpresaNif, lcAno, lcMes, uv_pass, pathToFile, pathToFileResponse, lcOpSaft)
    
    DO CASE
	CASE lcResultSaftEl= -1
	   uf_perguntalt_chama("Existem erros de Saft, Por favor Contacte o Suporte","OK","",64)
	CASE lcResultSaftEl =1
	   uf_perguntalt_chama("Opera��o realizada com sucesso.","OK","",64)
	 CASE lcResultSaftEl = 0
	    uf_perguntalt_chama("N�o foi obtida qualquer resposta por parte da AT","OK","",64)
	OTHERWISE
	ENDCASE
	
 	**nao comentar esta linha, elemina os ficheiros de reposta do saft
 	uf_gerais_deleteFiles(pathToFile, pathToFileResponse)	
 	
 	RETURN .t.
    
ENDFUNC

FUNCTION uf_analises_gerarRelatorioCentralFechosCaixas
	
	LOCAL lcObj
	lcObj = "relatorio.pagina2.container1.wdgfiltroreport.containerFiltros"
	
	SET POINT TO ","
	
	LOCAL lcSql, lcFIle , lcNameFile, lcIncExport, lcDocTipo 
	STORE '' To lcSql, lcFile,lcNameFile
	
	LOCAL lcDataIni, lcDataFim, lcSite, lcDirectoria, lcDiretoriaLocal, lcTipoCliente
	
	lcDataIni = uf_gerais_getdate(&lcObj..dataini.text1.value,"SQL")
	lcDataFim = uf_gerais_getdate(&lcObj..datafim.text1.value,"SQL")
	lcSite = Alltrim(&lcObj..site.text1.value)
	lcDirectoria = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'"))
	lcDiretoriaLocal = GETENV('TEMP')
	lcNameFile  = Alltrim(&lcObj..nome.text1.value)
	lcDocTipo   = Alltrim(&lcObj..tipoDoc.text1.value)

	
	** Valida��es
	IF Ctod(&lcObj..dataini.text1.value) > Ctod(&lcObj..datafim.text1.value)
		uf_perguntalt_chama("N�O PODE TER A DATA FINAL INFERIOR � DATA INICIAL.","OK","",48)
		SET POINT TO 
		RETURN .f.
	ENDIF
			
	**IF Empty(Alltrim(&lcObj..directoria.text1.value))
    IF EMPTY(lcDirectoria)
    	regua(2)
		uf_perguntalt_chama("Deve preencher o par�metro 'Caminho Relat�rio' para gerar o ficheiro.","OK","",48)
		SET POINT TO 
		RETURN .f.
	ENDIF

    IF !uf_gerais_createDir(lcDirectoria)
    	regua(2)
    	SET POINT TO 
        RETURN .F.
    ENDIF
	
	oRE = CreateObject("VBScript.RegExp")
	oRE.Pattern ='^[a-zA-Z0-9_.-]*$'
	
	IF (NOT EMPTY(lcNameFile))
		IF (not oRE.test(lcNameFile))
			uf_perguntalt_chama("O NOME APENAS PODE CONTER LETRAS E N�MEROS.","OK","",48)
			SET POINT TO 
			RETURN .f.
		ENDIF 
	ENDIF 
	
	fecha("uCrsvendasPim")
	

	LOCAL lcSQLFechoCaixa 
	STORE '' TO lcSQLFechoCaixa 
	
	TEXT TO lcSQLFechoCaixa NOSHOW TEXTMERGE 
		exec up_relatorio_centralFechosCaixas '<<lcDataIni>>','<<lcDataFim>>', '<<lcSite>>'
	ENDTEXT 	
	uf_gerais_actGrelha("","ucrsCentralFechoCaixa",lcSQLFechoCaixa)  

	select * FROM ucrsCentralFechoCaixa INTO CURSOR ucrsCentralFechoCaixa READWRITE 	

	select ucrsCentralFechoCaixa 
	IF Empty(Alltrim(lcNameFile))	
		lcFileName = ALLTRIM(lcDiretoriaLocal) + "\centralFechoCaixa_" + uf_gerais_getdate(date(),"SQL") + SYS(2) + lcDocTipo
	
	ELSE
		lcFileName = ALLTRIM(lcDiretoriaLocal) + "\"+ lcNameFile + lcDocTipo
	
	ENDIF

	COPY TO (lcFileName) TYPE DELIMITED WITH "" WITH CHARACTER ";"
	COPY FILE (lcFileName) TO (lcDirectoria) 

	lcText = strtran(FILETOSTR(lcFileName),'"','')

	strtofile(left(lcText,len(lcText)-2),lcFileName)
	
	IF USED("ucrsCentralFechoCaixa")
		fecha("ucrsCentralFechoCaixa")
	ENDIF 
	
	SET POINT TO 
	
	uf_perguntalt_chama("Ficheiro gerado com sucesso.","Ok","",64)
	
ENDFUNC

FUNCTION uf_analises_iqvia_exportion

LOCAL lcObj
	lcObj = "relatorio.pagina2.container1.wdgfiltroreport.containerFiltros"
	
	SET POINT TO ","
	
	LOCAL lcSql, lcFIle , lcNameFile, lcIncExport, lcDocTipo 
	STORE '' To lcSql, lcFile,lcNameFile
	
	LOCAL lcDataIni, lcDataFim, lcSite, lcDirectoria, lcDiretoriaLocal, lcFormulario  
	
	lcDataIni = uf_gerais_getdate(&lcObj..dataini.text1.value,"SQL")
	lcDataFim = uf_gerais_getdate(&lcObj..datafim.text1.value,"SQL")
	lcSite = Alltrim(&lcObj..site.text1.value)
	lcDirectoria = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'"))
	lcDiretoriaLocal = GETENV('TEMP')
	lcFormulario  = Alltrim(&lcObj..questionario.text1.value)
	lcDocTipo   = '.csv'

	** Valida��es
	IF Ctod(&lcObj..dataini.text1.value) > Ctod(&lcObj..datafim.text1.value)
		uf_perguntalt_chama("N�O PODE TER A DATA FINAL INFERIOR � DATA INICIAL.","OK","",48)
		SET POINT TO 
		RETURN .f.
	ENDIF

    IF EMPTY(lcDirectoria)
    	regua(2)
		uf_perguntalt_chama("Deve preencher o par�metro 'Caminho Relat�rio' para gerar o ficheiro.","OK","",48)
		SET POINT TO 
		RETURN .f.
	ENDIF

    IF !uf_gerais_createDir(lcDirectoria)
    	regua(2)
    	SET POINT TO 
        RETURN .F.
    ENDIF
	
	oRE = CreateObject("VBScript.RegExp")
	oRE.Pattern ='^[a-zA-Z0-9_.-]*$'

	LOCAL lcSQLIqvia
	STORE '' TO lcSQLIqvia
	
	TEXT TO lcSQLIqvia NOSHOW TEXTMERGE 
		exec up_relatorio_iqvia '<<lcSite>>', '<<lcDataIni>>','<<lcDataFim>>', '<<lcFormulario>>' 
	ENDTEXT

	IF !uf_gerais_actGrelha("","ucrsIqvia",lcSQLIqvia)  
		uf_perguntalt_chama("Erro ao Obter dados.","Ok","",64)
		SET POINT TO
		RETURN .F.
	ELSE 
		IF !USed("ucrsIqvia")
			uf_perguntalt_chama("N�o existem dados para os par�metros de pesquisa escolhidos.","Ok","",64)
			SET POINT TO
			RETURN .F.
	 	ENDIF 
	ENDIF		


	select * FROM ucrsIqvia INTO CURSOR ucrsIqvia READWRITE 	

	select ucrsIqvia
	IF Empty(Alltrim(lcNameFile))	
		lcFileName = ALLTRIM(lcDiretoriaLocal) + "\iqvia_" + uf_gerais_getdate(date(),"SQL") + SYS(2) + lcDocTipo	
	ENDIF

	COPY TO (lcFileName) TYPE DELIMITED WITH "" WITH CHARACTER ";"
	COPY FILE (lcFileName) TO (lcDirectoria) 

	lcText = strtran(FILETOSTR(lcFileName),'"','')

	strtofile(left(lcText,len(lcText)-2),lcFileName)
	
	IF USED("ucrsIqvia")
		fecha("ucrsIqvia")
	ENDIF 
	
	SET POINT TO 
	
	uf_perguntalt_chama("Ficheiro gerado com sucesso.","Ok","",64)

ENDFUNC






FUNCTION uf_analises_sitCli_exportion

LOCAL lcObj
	lcObj = "relatorio.pagina2.container1.wdgfiltroreport.containerFiltros"
	
	LOCAL lcSql, lcFIle , lcNameFile, lcIncExport, lcDocTipo 
	STORE '' To lcSql, lcFile,lcNameFile
	
	LOCAL lcDataIni, lcDataFim, lcSite, lcDirectoria, lcDiretoriaLocal
	
	lcDataIni = uf_gerais_getdate(&lcObj..dataini.text1.value,"SQL")
	lcDataFim = uf_gerais_getdate(&lcObj..datafim.text1.value,"SQL")
	lcSite = Alltrim(&lcObj..site.text1.value)
	lcDirectoria = ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'"))
	lcDiretoriaLocal = GETENV('TEMP')
	lcDocTipo   = '.csv'

	** Valida��es
	IF Ctod(&lcObj..dataini.text1.value) > Ctod(&lcObj..datafim.text1.value)
		uf_perguntalt_chama("N�O PODE TER A DATA FINAL INFERIOR � DATA INICIAL.","OK","",48)
		SET POINT TO 
		RETURN .f.
	ENDIF

    IF EMPTY(lcDirectoria)
    	regua(2)
		uf_perguntalt_chama("Deve preencher o par�metro 'Caminho Relat�rio' para gerar o ficheiro.","OK","",48)
		SET POINT TO 
		RETURN .f.
	ENDIF

    IF !uf_gerais_createDir(lcDirectoria)
    	regua(2)
    	SET POINT TO 
        RETURN .F.
    ENDIF
	
	oRE = CreateObject("VBScript.RegExp")
	oRE.Pattern ='^[a-zA-Z0-9_.-]*$'

	LOCAL lcSQLSitCli
	STORE '' TO lcSQLSitCli 
	
	TEXT TO lcSQLSitCli NOSHOW TEXTMERGE 
		exec up_relatorio_sITcLI '<<lcSite>>', '<<lcDataIni>>','<<lcDataFim>>', 'Situacoes Clinicas'
	ENDTEXT 	
	
	IF !uf_gerais_actGrelha("","ucrsSitCli",lcSQLSitCli )  
		uf_perguntalt_chama("Erro ao Obter dados.","Ok","",64)
		SET POINT TO
		RETURN .F.
	ELSE 
		
		IF !USED("ucrsSitCli")
			uf_perguntalt_chama("N�o existem dados para os par�metros de pesquisa escolhidos.","Ok","",64)
			SET POINT TO
			RETURN .F.
	 	ENDIF 
	ENDIF
	
	SELECT ucrsSitCli
	GOTO TOP 
	
	select * FROM ucrsSitCli INTO CURSOR ucrsSitCli READWRITE
	SELECT ucrsSitCli
	IF Empty(Alltrim(lcNameFile))	
		lcFileName = ALLTRIM(lcDiretoriaLocal) + "\sitCli_" + uf_gerais_getdate(date(),"SQL") + SYS(2) + lcDocTipo	
	ENDIF

	COPY TO (lcFileName) TYPE DELIMITED WITH "" WITH CHARACTER ";"
	COPY FILE (lcFileName) TO (lcDirectoria) 

	lcText = strtran(FILETOSTR(lcFileName),'"','')

	strtofile(left(lcText,len(lcText)-2),lcFileName)
	
	IF USED("ucrsSitCli")
		fecha("ucrsSitCli")
	ENDIF 
	
	SET POINT TO 
	
	uf_perguntalt_chama("Ficheiro gerado com sucesso.","Ok","",64)

ENDFUNC