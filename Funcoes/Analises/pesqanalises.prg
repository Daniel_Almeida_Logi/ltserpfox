**
FUNCTION uf_pesqanalises_chama
	IF TYPE("PESQANALISES")=="U"
		regua(0,2,"A carregar painel...")
		regua(1,1,"A carregar painel...")
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_analises_pesquisa ''
		ENDTEXT
		uf_gerais_actGrelha("","UCRSANALISESPESQ",lcSQL)
		regua(1,2,"A carregar painel...")
		*uf_pesqanalises_controlaPermissoes()

		DO FORM PESQANALISES
		
		regua(2)
	ELSE
		PESQANALISES.show
	ENDIF
ENDFUNC


**
FUNCTION uf_pesqanalises_controlaPermissoes
	IF !USED("UCRSANALISESPESQ")
		RETURN .f.
	ENDIF 
	
	** Faz Tratamento de permiss�es
	Select UCRSANALISESPESQ
	Scan
		* No caso do Perfil ser 0 � considerado Sem Controlo de Acessos
		TEXT To lcSql Noshow textmerge
			exec up_perfis_validaAcesso '<<Alltrim(UCRSANALISESPESQ.perfil)>>'
		ENDTEXT
		If !uf_logitools_sqlExec(lcSql, "uCrsAnalisesAcessoPesq")
			uf_perguntalt_chama("OCORREU UM ERRO A VALIDAR ACESSOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			return .f.	
		Endif	
		
		Select uCrsAnalisesAcessoPesq
		If Reccount()>0
			Go top
			Scan
				If uCrsAnalisesAcessoPesq.userno==ch_userno Or Upper(Alltrim(uCrsAnalisesAcessoPesq.grupo))==Upper(Alltrim(ch_grupo))
					Select UCRSANALISESPESQ
	   				delete
				Endif
			Endscan
		Endif	
	ENDSCAN
	Select UCRSANALISESPESQ
	GO top
ENDFUNC


FUNCTION uf_pesqanalises_actualizar
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_analises_pesquisa '<<PESQANALISES.analise.value>>'
	ENDTEXT
	uf_gerais_actGrelha("PESQANALISES.gridPesq","UCRSANALISESPESQ",lcSQL)
ENDFUNC


FUNCTION uf_pesqanalises_sel
	SELECT ucrsAnalisesPesq
	SELECT uCrsAnalisesGrupo
	LOCATE FOR ALLTRIM(UPPER(uCrsAnalisesGrupo.grupo)) == ALLTRIM(UPPER(ucrsAnalisesPesq.grupo))
    Analises.containerGrupo.gridGrupo.setfocus
    SELECT uCrsAnalisesSubGrupo
    LOCATE FOR ALLTRIM(UPPER(uCrsAnalisesSubGrupo.SubGrupo)) == ALLTRIM(UPPER(ucrsAnalisesPesq.subgrupo))
    Analises.containerGrupo.gridSubGrupo.setfocus
	SELECT ucrsAnalisesDescricao
	LOCATE FOR ALLTRIM(UPPER(ucrsAnalisesDescricao.descricao)) == ALLTRIM(UPPER(ucrsAnalisesPesq.descricao))
    Analises.containerGrupo.gridDescricao.setfocus

	uf_pesqanalises_sair()
ENDFUNC


**
FUNCTION uf_pesqanalises_sair
	fecha("ucrsAnalisesPesq")
	
	PESQANALISES.hide
	PESQANALISES.release
ENDFUNC