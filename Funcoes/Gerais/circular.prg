FUNCTION uf_circular_chama

	DO FORM CIRCULAR

ENDFUNC

FUNCTION uf_circular_sair

   IF !WEXIST("CIRCULAR")
      RETURN ''
   ENDIF

   uf_circular_limpar()

	CIRCULAR.hide
	CIRCULAR.release

ENDFUNC

FUNCTION uf_circular_escolhePDF

	PUBLIC upv_filenameCircular

	TEXT TO uc_sql TEXTMERGE NOSHOW
		select filename, path from anexos where  tipo = 'Circular' order by usrdata desc
	ENDTEXT
	
	uf_gerais_actGrelha("", "uc_circulares", uc_sql)

	uf_valorescombo_chama("upv_filenameCircular", 0, "uc_circulares", 2, "filename", "filename", .F., .F., .T.)
	
	SELECT uc_circulares
	GO TOP
	
	LOCATE FOR uf_gerais_compStr(uc_circulares.filename, upv_filenameCircular)
	
	IF FOUND()

	    uf_circular_limpar()

		uv_bckFile = "\\st01\ArquivoDigital\Circulares\backup\" + ALLTRIM(uf_gerais_stamp()) + ".pdf"
		
		COPY FILE (ALLTRIM(uc_circulares.path)) TO (uv_bckFile)
	
		CIRCULAR.filename = ALLTRIM(uv_bckFile)
		CIRCULAR.showPDF()
	
	ENDIF

ENDFUNC

FUNCTION uf_circular_limpar

   LOCAL uv_delFile

   CIRCULAR.olecontrol1.obJECT.Navigate2("About:Blank")
   CIRCULAR.showPDF()

	IF !EMPTY(CIRCULAR.fileName)
      uv_delFile = CIRCULAR.filename
      CIRCULAR.filename = ""
      CIRCULAR.olecontrol1.obJECT.Navigate2("About:Blank")
      CIRCULAR.showPDF()
		DELETE FILE (uv_delFile)
	ENDIF

ENDFUNC