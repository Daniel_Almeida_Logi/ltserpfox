**
FUNCTION uf_PESQANEXARDOC_chama
	
	IF USED("uCrsAnexosRegpAux")
		fecha("uCrsAnexosRegpAux")
	ENDIF 
	lcSQL =""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		select 
			CAST(0 AS bit) as sel
			, filename AS nomeficheiro
			, descr as descricao
			, CONVERT(varchar, ousrdata, 102) as dateins
			, * 
		from 
			anexos 
		where 
			regstamp='<<ALLTRIM(mylcRegStamp)>>' 
			AND tabela='<<ALLTRIM(mylcTable)>>'
			and ousrinis = (case when protected=1 then '<<m_chinis>>' else ousrinis end)
			and 0 = 1
	ENDTEXT 
	
	IF !uf_gerais_actgrelha("", "uCrsAnexosRegpAux", lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DOS ANEXOS.","OK","",16)
		RETURN .f.
	ENDIF
	
	CREATE CURSOR uCrsAnexosRegp(sel l, regstamp c(30), tabela c(30), filename c(254), descr c(254), keyword c(254))
	SELECT uCrsAnexosRegp
	APPEND BLANK 
		replace uCrsAnexosRegp.nomeficheiro WITH ''
		replace uCrsAnexosRegp.descricao WITH ''
		replace uCrsAnexosRegp.descr WITH ''
		replace uCrsAnexosRegp.anexosstamp WITH  ''
		replace uCrsAnexosRegp.dateins WITH ALLTRIM(STR(YEAR(DATE())))+'.'+RIGHT('0'+ALLTRIM(STR(MONTH(DATE()))),2)+'.'+RIGHT('0'+ALLTRIM(STR(DAY(DATE()))),2)
		replace uCrsAnexosRegp.protected WITH .f.
		replace uCrsAnexosRegp.ousrinis WITH ''

		
	&& Controla Abertura do Painel
	IF type("PESQANEXARDOC")=="U"
		DO FORM PESQANEXARDOC
	ELSE
		PESQANEXARDOC.show
	ENDIF

	PESQANEXARDOC.menu1.adicionaOpcao("visualizar", "Visualizar", myPath + "\imagens\icons\visualizar_w.png", "uf_PESQANEXARDOC_Visualizar","")
	PESQANEXARDOC.menu1.adicionaOpcao("download", "Transferir", myPath + "\imagens\icons\transferir_w.png", "uf_PESQANEXARDOC_download","")
	PESQANEXARDOC.menu1.adicionaOpcao("email", "Email Fx.", myPath + "\imagens\icons\email_w.png", "uf_PESQANEXARDOC_email","")

	PESQANEXARDOC.menu1.estado("", "", "Gravar", .f., "Sair", .t.)
	
	PESQANEXARDOC.refresh
ENDFUNC 

**

FUNCTION uf_PESQANEXARDOC_pesquisar

	lcSQL =""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		select 
			CAST(0 AS bit) as sel
			, filename AS nomeficheiro
			, descr as descricao
			, CONVERT(varchar, ousrdata, 102) as dateins
			, * 
		from 
			anexos 
		where 
			ousrinis = (case when protected=1 then '<<m_chinis>>' else ousrinis end)
			and keyword like '%<<ALLTRIM(uCrsanexos.keyword)>>%'
	ENDTEXT 
	
	IF !uf_gerais_actgrelha("", "uCrsAnexosRegpAux", lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DOS ANEXOS.","OK","",16)
		RETURN .f.
	ENDIF
	
	SELECT uCrsAnexosRegpAux
	GO TOP 
	SCAN 
		SELECT uCrsAnexosRegp
		DELETE ALL 
		APPEND BLANK 
		replace uCrsAnexosReg.nomeficheiro WITH alltrim(uCrsAnexosRegpAux.filename)
		replace uCrsAnexosReg.descricao WITH ALLTRIM(uCrsAnexosRegpAux.descr)
		replace uCrsAnexosReg.descr WITH ALLTRIM(uCrsAnexosRegpAux.descr)
		replace uCrsAnexosReg.anexosstamp WITH ALLTRIM(uCrsAnexosRegpAux.anexosstamp )
		replace uCrsAnexosReg.dateins WITH uCrsAnexosRegpAux.dateins 
		replace uCrsAnexosReg.protected WITH uCrsAnexosRegpAux.protected 
		replace uCrsAnexosReg.ousrinis WITH uCrsAnexosRegpAux.ousrinis 
		SELECT uCrsAnexosRegpAux
	ENDSCAN 
	
	uf_PESQANEXARDOC_refresh()

ENDFUNC 


**
FUNCTION uf_PESQANEXARDOC_sair
	
	&& fecha cursores
	Fecha("uCrsAnexos")
	Fecha("uCrsAnexosRegp")
	Fecha("uCrsTipoFx")
					
	PESQANEXARDOC.hide
	PESQANEXARDOC.Release

	RELEASE PESQANEXARDOC
ENDFUNC


FUNCTION uf_PESQANEXARDOC_refresh
	PESQANEXARDOC.GridPesq.refresh
	PESQANEXARDOC.refresh
ENDFUNC 


FUNCTION uf_PESQANEXARDOC_unsel
	
	SELECT uCrsAnexosRegp
	GO TOP 
	SCAN 
		replace uCrsAnexosRegp.sel WITH .f. 
	ENDSCAN 
	
	SELECT uCrsAnexosRegp
	GO TOP 
	
	PESQANEXARDOC.GridPesq.refresh
	PESQANEXARDOC.refresh
ENDFUNC 


FUNCTION uf_PESQANEXARDOC_valsel
	
	LOCAL lcValSel
	STORE .f. TO lcValSel
	SELECT uCrsAnexosRegp
	GO TOP 
	SCAN 
		if uCrsAnexosRegp.sel == .t. 
			lcValSel = .t.
		ENDIF 
	ENDSCAN
	SELECT uCrsAnexosRegp
	GO TOP 
	RETURN lcValSel 
		
ENDFUNC 


FUNCTION uf_PESQANEXARDOC_Visualizar
	
	IF !uf_PESQANEXARDOC_valsel()
		uf_perguntalt_chama("N�O SELECIONOU NENHUM REGISTO!","OK","",64)
	ELSE
	
		LOCAL lcFile, lcDestDir
		lcDestDir=""+alltrim(uf_gerais_getParameter_site('ADM0000000010', 'text'))+"\"+ALLTRIM(uCrsAnexos.tabela)+"\"
		lcFile = lcDestDir + ALLTRIM(uCrsAnexosRegp.nomeficheiro)
			
		SELECT uCrsAnexosRegp
		GO TOP
		SCAN
			IF uCrsAnexosRegp.sel=.t.
				IF uCrsAnexosRegp.protected=.f. 
					DECLARE INTEGER ShellExecute IN shell32.dll ; 
				  	INTEGER hndWin, ; 
				  	STRING cAction, ; 
				  	STRING cFileName, ; 
				  	STRING cParams, ;  
				  	STRING cDir, ; 
					INTEGER nShowWin
					ShellExecute(0,"open",lcFile ,"","",1)
				ELSE
					IF ALLTRIM(uCrsAnexosRegp.ousrinis) == ALLTRIM(m_chinis)
						DECLARE INTEGER ShellExecute IN shell32.dll ; 
					  	INTEGER hndWin, ; 
					  	STRING cAction, ; 
					  	STRING cFileName, ; 
					  	STRING cParams, ;  
					  	STRING cDir, ; 
						INTEGER nShowWin

						ShellExecute(0,"open",lcFile ,"","",1)
					ELSE 
						uf_perguntalt_chama("O FICHEIRO EST� PROTEGIDO E POR ESSE MOTIVO S� PODE SER CONSULTADO PELO UTILIZADOR QUE O INSERIU!","OK","",64)
					ENDIF 
				ENDIF 
			ENDIF 
		ENDSCAN 
		
		SELECT uCrsAnexosRegp
		GO TOP
		PESQANEXARDOC.GridPesq.refresh
		PESQANEXARDOC.refresh
	ENDIF 
ENDFUNC 

FUNCTION uf_PESQANEXARDOC_email

	IF !uf_PESQANEXARDOC_valsel()
		uf_perguntalt_chama("N�O SELECIONOU NENHUM REGISTO!","OK","",64)
	ELSE

		LOCAL lccount
		lccount=0
		IF ucrse1.usacoms=.t. 
			SELECT uCrsAnexosRegp
			GO TOP
			SCAN
				IF uCrsAnexosRegp.sel=.t.
					lccount=lccount + 1
				ENDIF 
			ENDSCAN
		
			IF lccount=1 then
				SELECT uCrsAnexosRegp
				GO TOP
				SCAN
					IF uCrsAnexosRegp.sel=.t.
						LOCAL mDir, lcTo, lcSubject, lcBody, lcAtatchment, lcDestDir
						PUBLIC lcToEmail
						lcDestDir=""+alltrim(uf_gerais_getParameter_site('ADM0000000010', 'text'))+"\"+ALLTRIM(uCrsAnexos.tabela)+"\"
						lcTo = ""
						lcToEmail = ""
						uf_tecladoalpha_chama("lcToEmail", "Introduza o email para o envio", .f., .f., 0)
						lcSubject = "Envio de ficheiro"
						lcBody = "Envio de anexo do software Logitools"
						lcAtatchment = lcDestDir + ALLTRIM(uCrsAnexosRegp.nomeficheiro)
						regua(0,6,"A enviar email ...")
						uf_startup_sendmail_emp(lcTo, lcToEmail, lcSubject, lcBody , lcAtatchment, .t.)
						regua(2)
					ENDIF 
				ENDSCAN 
			ELSE
				uf_perguntalt_chama("S� PODE SER EFETUADO O ENVIO DE 1 DOCUMENTO DE CADA VEZ!","OK","",64)
			ENDIF 		
			
		ELSE
			uf_perguntalt_chama("PARA UTILIZAR ESTA FUNCIONALIDADE DEVER� ADQUIRIR O M�DULO DE COMUNICA��ES." + CHR(13) + "PARA MAIS INFORMA��ES POR FAVOR CONTACTE A LOGITOOLS (OP��O - COMERCIAL).", "OK","",16)
		ENDIF
		
		uf_PESQANEXARDOC_unsel()
		
		SELECT uCrsAnexosRegp
		GO TOP
		PESQANEXARDOC.GridPesq.refresh
		PESQANEXARDOC.refresh
	ENDIF 
ENDFUNC

FUNCTION uf_PESQANEXARDOC_download

	LOCAL lcCurDir, lcDestDir, lcDestFolder, lcFile , lcDestfile 
	STORE '' TO lcCurDir, lcDestDir, lcDestFolder, lcFile , lcDestfile 
	
	IF !uf_PESQANEXARDOC_valsel()
		uf_perguntalt_chama("N�O SELECIONOU NENHUM REGISTO!","OK","",64)
	ELSE

		lcCurDir = FULLPATH(CurDir())
		lcDestDir= myGDFolder
		
		IF EMPTY(myPrevFolder)	
			CD(myDefFolder)
		ELSE
			CD(myPrevFolder)
		ENDIF 
		
		lcDestFolder=GETDIR(myDefFolder, "Selecione a diretoria de destino:", "Pasta 'AnexosLogitools' do Ambiente de trabalho", "", .t.)
		
		IF !EMPTY(lcDestFolder)
			LOCAL lcFile, lcDestDir, lcDestfile, lcCont, lcCont2
			calculate count() for uCrsAnexosRegp.sel==.t. to lcCont
			lcCont2 = 1
			regua(0,lcCont,"A transferir ficheiro(s) ...")
			SELECT uCrsAnexosRegp
			GO TOP
			SCAN
				IF uCrsAnexosRegp.sel=.t.
					regua(1,lcCont2,"A transferir ficheiro(s) ...")
					IF uCrsAnexosRegp.protected=.f.
						IF ALLTRIM(uCrsAnexosRegp.ousrinis) == ALLTRIM(m_chinis)
							lcDestDir=""+alltrim(uf_gerais_getParameter_site('ADM0000000010', 'text'))+"\"+ALLTRIM(uCrsAnexos.tabela)+"\"
							lcFile = "'"+lcDestDir + ALLTRIM(uCrsAnexosRegp.nomeficheiro)+"'"
							lcDestfile = "'"+lcDestFolder + ALLTRIM(uCrsAnexosRegp.nomeficheiro)+"'"
		
							COPY FILE &lcFile TO &lcDestfile 
						ELSE
							uf_perguntalt_chama("O FICHEIRO " + ALLTRIM(uCrsAnexosRegp.nomeficheiro) + " EST� PROTEGIDO E POR ESSE MOTIVO S� PODE SER TRANSFERIDO PELO UTILIZADOR QUE O INSERIU!","OK","",64)
						ENDIF 
					ELSE 				
						lcDestDir=""+alltrim(uf_gerais_getParameter_site('ADM0000000010', 'text'))+"\"+ALLTRIM(uCrsAnexos.tabela)+"\"
							lcFile = "'"+lcDestDir + ALLTRIM(uCrsAnexosRegp.nomeficheiro)+"'"
							lcDestfile = "'"+lcDestFolder + ALLTRIM(uCrsAnexosRegp.nomeficheiro)+"'"
					
						COPY FILE &lcFile TO &lcDestfile 
					ENDIF 
					lcCont2 =lcCont2 + 1
				ENDIF 
			ENDSCAN 
			uf_perguntalt_chama("TRANSFER�NCIA CONCLU�DA!","OK","",64)
			regua(2)
			
			uf_PESQANEXARDOC_unsel()

		ELSE
			uf_perguntalt_chama("N�O FOI SELECIONADA NENHUMA DIRETORIA!","OK","",64)
		ENDIF 
		
		CD(myDefDir)
	ENDIF 
ENDFUNC 