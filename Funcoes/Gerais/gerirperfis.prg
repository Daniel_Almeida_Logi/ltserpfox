**
FUNCTION uf_gerirperfis_chama
	LPARAMETERS lcCodigo
	PUBLIC 	myPfIntroducao , myPfAlteracao
	
	IF TYPE("lcCodigo") != "N"
		lcCodigo = 0
	ENDIF
	
	&& Cursor de grupo
	fecha("uCrsPerfis")
	IF !(uf_gerais_actGrelha("","uCrsPerfis","select * from b_pf (nolock) where codigo = "+ASTR(lcCodigo)))
		uf_perguntalt_chama("Ocorreu uma anomalia a verificar a lista de perfis. Por favor contacte o Suporte.","OK","",16)
		RETURN .f.
	ENDIF
	SELECT uCrsPerfis
	
	&& Controla Abertura do Painel
	IF type("GERIRPERFIS")=="U"
		DO FORM GERIRPERFIS
	ELSE
		GERIRPERFIS.show
	ENDIF
	
	&&menu
	IF TYPE("GERIRPERFIS.menu1.pesquisar") == "U"
		WITH GERIRPERFIS.menu1
			.adicionaopcao("pesquisar","Pesquisar",myPath + "\imagens\icons\lupa_w.png","uf_GERIRPERFIS_Pesquisar","P")
			.adicionaOpcao("novo","Novo",myPath + "\imagens\icons\mais_w.png","uf_GERIRPERFIS_novo","N")
			.adicionaOpcao("editar","Editar",myPath + "\imagens\icons\lapis_w.png","uf_GERIRPERFIS_Editar","E")
			.adicionaOpcao("eliminar","Eliminar",myPath + "\imagens\icons\cruz_w.png","uf_GERIRPERFIS_Elimina","X")
			.adicionaOpcao("anterior","Anterior",myPath + "\imagens\icons\ponta_seta_left_w.png","uf_GERIRPERFIS_registoanterior","19")
			.adicionaOpcao("seguinte","Seguinte",myPath + "\imagens\icons\ponta_seta_rigth_w.png","uf_GERIRPERFIS_registoseguinte","04")
		ENDWITH
	ENDIF
	
	uf_GERIRPERFIS_controlaObj()
ENDFUNC 


**
FUNCTION uf_GERIRPERFIS_registoanterior
	SELECT uCrsPerfis
	uf_GERIRPERFIS_chama(uCrsPerfis.codigo-1)
ENDFUNC


**
FUNCTION uf_GERIRPERFIS_registoseguinte
	SELECT uCrsPerfis
	uf_GERIRPERFIS_chama(uCrsPerfis.codigo+1)
ENDFUNC


**
FUNCTION uf_GERIRPERFIS_gravar
	&&valida��es
	IF EMPTY(ALLTRIM(uCrsPerfis.resumo))
		uf_perguntalt_chama("O campo resumo � de preenchimento obrigat�rio.","OK","",64)
		RETURN .f.
	ENDIF
	IF EMPTY(ALLTRIM(uCrsPerfis.grupo))
		uf_perguntalt_chama("O campo grupo � de preenchimento obrigat�rio.","OK","",64)
		RETURN .f.
	ENDIF  
	IF EMPTY(ALLTRIM(uCrsPerfis.tipo))
		uf_perguntalt_chama("O campo tipo � de preenchimento obrigat�rio.","OK","",64)
		RETURN .f.
	ENDIF  
	
	LOCAL lcSQL, lcStamp
	IF myPfIntroducao
		lcStamp = uf_gerais_stamp()
		lcSQL=''
		TEXT TO lcSQL TEXTMERGE NOSHOW
			INSERT INTO b_pf (
				pfstamp,codigo,resumo,
				descricao,grupo,tipo,
				ousrinis,ousrdata,ousrhora,
				usrinis,usrdata,usrhora
			)
			values(
				'<<ALLTRIM(lcStamp)>>',<<uCrsPerfis.codigo>>,'<<ALLTRIM(uCrsPerfis.resumo)>>',
				'<<ALLTRIM(uCrsPerfis.descricao)>>','<<ALLTRIM(uCrsPerfis.grupo)>>','<<ALLTRIM(uCrsPerfis.tipo)>>',
				'<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108), 
				'<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
			)
		ENDTEXT 

		IF !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("Ocorreu uma anomalia a introduzir o Perfil. Por favor contacte o suporte.","OK","",16)
		ELSE
			uf_perguntalt_chama("Perfil inserido com sucesso.","OK","",64)
		ENDIF 
	ELSE
		lcSQL = ''
		TEXT TO lcSQL TEXTMERGE NOSHOW
			update 
				b_pf
			set
				resumo = '<<ALLTRIM(uCrsPerfis.resumo)>>',
				descricao = '<<ALLTRIM(uCrsPerfis.descricao)>>',
				grupo = '<<ALLTRIM(uCrsPerfis.grupo)>>',
				tipo = '<<ALLTRIM(uCrsPerfis.tipo)>>',
				usrinis = '<<m_chinis>>',
				usrdata = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
				usrhora = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
			where 
				pfstamp = '<<ALLTRIM(uCrsPerfis.pfstamp)>>'
		ENDTEXT 
		
		IF !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("Ocorreu uma anomalia a actulizar o Perfil. Por favor contacte o suporte.","OK","",16)
		ELSE
			uf_perguntalt_chama("Perfil actulizado com sucesso.","OK","",64)
		ENDIF 
	ENDIF
	
	myPfIntroducao = .f.
	myPfAlteracao = .f.
	uf_GERIRPERFIS_chama(uCrsPerfis.codigo)
ENDFUNC


**
FUNCTION uf_GERIRPERFIS_sair
	
	* Verifica se o Documento esta em Modo de Edicao ou Introducao
	If myPfIntroducao OR myPfAlteracao
		IF uf_perguntalt_chama("Quer cancelar as altera��es efectuadas?", "SIM", "N�O", 64)
			STORE .f. TO myPfIntroducao, myPfAlteracao
			
			SELECT uCrsPerfis
			uf_GERIRPERFIS_chama(uCrsPerfis.codigo)
		ENDIF
	ELSE
		&& fecha cursores
		Fecha("uCrsPerfis")
					
		GERIRPERFIS.hide
		GERIRPERFIS.Release

		RELEASE GERIRPERFIS
	ENDIF
ENDFUNC


**
FUNCTION uf_gerirperfis_Pesquisar
	IF !(uf_gerais_actGrelha("","uCrsPesqPerfil",[Select codigo,resumo from b_pf (nolock)]))
		uf_perguntalt_chama("Ocorreu uma anomalia a verificar os perfis. Por favor contacte o suporte.","OK","",16)
		RETURN .f.
	ELSE
		PUBLIC pValorPesqPf
		pValorPesqPf = 0
		uf_valorescombo_chama("pValorPesqPf", 0, "uCrsPesqPerfil", 2, "codigo", "codigo,resumo")	
		fecha("uCrsPesqPerfil")
		IF pValorPesqPf != 0
			uf_gerirperfis_chama(pValorPesqPf)
		ENDIF
	ENDIF
ENDFUNC 


**
FUNCTION uf_gerirperfis_Elimina
	IF uf_perguntalt_chama("Tem a certeza que pretende eliminar o Perfil?","Sim","N�o")
		LOCAL lcSQL
		lcSQL=''
		TEXT TO lcSQL TEXTMERGE noshow
			DELETE FROM b_pf WHERE pfstamp='<<ALLTRIM(uCrsPerfis.pfstamp)>>'
			DELETE FROM b_pfu WHERE pfstamp='<<ALLTRIM(uCrsPerfis.pfstamp)>>'
			DELETE FROM b_pfg WHERE pfstamp='<<ALLTRIM(uCrsPerfis.pfstamp)>>'
		ENDTEXT 
		IF uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("Perfil eliminado com sucesso.", "OK", "", 64)
			uf_gerais_actGrelha("","uCrsTempPf","select top 1 codigo from b_pf order by codigo")
			uf_GERIRPERFIS_chama(uCrsTempPf.codigo)
			fecha("uCrsTempGr")
		ENDIF 
	ENDIF
ENDFUNC 


**
FUNCTION uf_gerirperfis_Editar
	IF myPfIntroducao == .t. OR myPfAlteracao == .t.
		uf_perguntalt_chama("O Ecr� de Gest�o de Perfis est� em modo de Introdu��o/Edi��o. Deve cancelar ou gravar para proceder com esta ac��o.", "OK", "", 64)
		RETURN .f.
	ENDIF
	
	IF empty(ALLTRIM(uCrsPerfis.pfstamp))
		RETURN .f.
	Endif
	
	myPfAlteracao = .t.
	gerirperfis.txtResumo.setfocus
	
	uf_gerirperfis_controlaObj()
	gerirperfis.refresh
ENDFUNC


**
FUNCTION uf_gerirperfis_novo
	IF myPfIntroducao == .t. OR myPfAlteracao == .t.
		uf_perguntalt_chama("O Ecr� de Gest�o de Perfis est� em modo de Introdu��o/Edi��o. Deve cancelar ou gravar para proceder com esta ac��o.", "OK", "", 64)
		RETURN .f.
	ENDIF

	myPfIntroducao = .t.
	
	gerirperfis.txtResumo.setfocus
	
	**Limpa Cursores
	IF USED("uCrsPerfis")
		SELECT uCrsPerfis
		DELETE ALL
		SELECT uCrsPerfis
		APPEND BLANK
	ENDIF 

	&& calcular c�digo do perfil
	uf_gerais_actGrelha("","uCrsTempCodigo","select ISNULL(MAX(codigo),0)+1 as codigo from b_pf (nolock)")
	SELECT uCrsTempCodigo
	SELECT uCrsPerfis
	replace uCrsPerfis.codigo WITH uCrsTempCodigo.codigo
	fecha("uCrsTempCodigo")

	uf_gerirperfis_controlaObj()
	gerirperfis.refresh
ENDFUNC


**
FUNCTION uf_gerirperfis_controlaObj
	
	IF !myPfIntroducao AND !myPfAlteracao && ecra em modo de consulta
		**
		WITH gerirperfis
			.caption = uf_gerais_aplicaLanguageMsg("Perfis de Acesso")
		ENDWITH

		gerirperfis.txtCodigo.readonly = .t.
		gerirperfis.txtResumo.readonly = .t.
		gerirperfis.txtGrupo.readonly = .t.
		gerirperfis.txtTipo.readonly = .t.
		gerirperfis.edtDescr.readonly = .t.
		
		gerirperfis.menu1.estado("pesquisar, novo, editar, eliminar, anterior, seguinte", "SHOW", "Gravar", .f., "Sair", .t.)
	ELSE
		**
		WITH gerirperfis
			IF myPfIntroducao
				.caption = uf_gerais_aplicaLanguageMsg("Perfis de Acesso - Introdu��o")
			ELSE
				.caption = uf_gerais_aplicaLanguageMsg("Perfis de Acesso - Altera��o")
			ENDIF
		ENDWITH

		gerirperfis.txtResumo.readonly = .f.
		gerirperfis.txtGrupo.readonly = .f.
		gerirperfis.edtDescr.readonly = .f.
		
		gerirperfis.menu1.estado("pesquisar, novo, editar, eliminar, anterior, seguinte", "HIDE", "Gravar", .t., "Sair", .t.)
	ENDIF

ENDFUNC