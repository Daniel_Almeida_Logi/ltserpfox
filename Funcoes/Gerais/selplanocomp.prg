**
FUNCTION uf_selplanocomp_chama
	LPARAMETERS objCrs, campos, campos2, lcBackOffice, lcDem, lcCabOrdem
	
	PUBLIC pCrs, pCampos, pCampos2
	pCrs = objCrs
	pCampos = campos
	pCampos2 = campos2
	
	SELECT uCrsE1
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_receituario_getPlanos '<<Alltrim(uCrsE1.u_assfarm)>>','','','',0
	ENDTEXT
	
	IF !uf_gerais_ActGrelha("", "uCrsPesqPlanos", lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia a verificar a lista de planos dispon�vels.","OK","",16)
		RETURN .F.
	ENDIF

	**
	IF !EMPTY(lcDem)
		IF USED("ucrsPlanosDemAutomaticos")

			SELECT *,.f. as naoapagar FROM uCrsPesqPlanos INTO CURSOR uCrsPesqPlanosAux READWRITE 
			
			SELECT uCrsPesqPlanosAux 
			GO Top
			SCAN 
				SELECT ucrsPlanosDemAutomaticos 
				GO Top
				LOCATE FOR ALLTRIM(uCrsPesqPlanosAux.codigo) == ALLTRIM(ucrsPlanosDemAutomaticos.plano)
				IF FOUND() 
					SELECT uCrsPesqPlanosAux 
					Replace uCrsPesqPlanosAux.naoapagar WITH .t.
				ENDIF 
			ENDSCAN
			
			DELETE FROM uCrsPesqPlanosAux WHERE EMPTY(uCrsPesqPlanosAux.naoapagar)
			SELECT uCrsPesqPlanos 
			GO TOP
			SCAN
				DELETE 
			ENDSCAN 

			SELECT * FROM  uCrsPesqPlanosAux INTO CURSOR uCrsPesqPlanos READWRITE  
			SELECT uCrsPesqPlanos 
			GO Top
			
			IF USED("uCrsPesqPlanosAux")
				fecha("uCrsPesqPlanosAux")
			ENDIF 
		ENDIF 
	ENDIF 

	&& Controla Abertura do Painel
	if type("SELPLANOCOMP")=="U"
		DO FORM SELPLANOCOMP WITH lcBackOffice, lcDem, lcCabOrdem
	ELSE
		SELPLANOCOMP.show
		uf_SELPLANOCOMP_FiltraDados()
		SELPLANOCOMP.gridpesq.Refresh
		SELPLANOCOMP.lblRegistos.caption = ALLTRIM(STR(x)) + " Resultados"
	ENDIF
ENDFUNC


**
FUNCTION uf_selplanocomp_carregamenu
	selplanocomp.menu1.adicionaOpcao("actualizar", "Actualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_selplanocomp_filtraDados","A")
	selplanocomp.menu1.adicionaOpcao("tecladoVirtual", "Teclado", myPath + "\imagens\icons\teclado_w.png", "uf_selplanocomp_tecladoVirtual","T")
	selplanocomp.menu1.adicionaOpcao("limpaFiltros", "Limpar", myPath + "\imagens\icons\limpar_w.png", "uf_selplanocomp_limpaFiltros","L")
	IF TYPE("ATENDIMENTO")=="U"
		selplanocomp.menu1.adicionaOpcao("vazio", "Vazio", myPath + "\imagens\icons\cruz_w.png", "uf_selplanocomp_planoVazio","V")
	ENDIF
ENDFUNC


**
FUNCTION uf_selplanocomp_planoVazio
	SELECT uCrsPesqPlanos
	APPEND BLANK
	uf_SELPLANOCOMP_seleccionarPlano()
ENDFUNC 


**
FUNCTION uf_SELPLANOCOMP_FiltraDados
	LOCAL lcSQL, x, lcPlanoComp
	x=0
	STORE '' TO lcPlanoComp

	
	IF SELPLANOCOMP.chkfiltracompl.tag = "true"
	
		IF(VARTYPE(FACTURACAO)!='U')
			SELECT FT2
			GO TOP
			lcPlanoComp  = ALLTRIM(FT2.U_CODIGO)

		ENDIF
	
		if(VARTYPE(ATENDIMENTO)!='U')
			lcPlanoComp = ALLTRIM(Atendimento.pgfDados.page3.txtCod.value)
		ENDIF	
				
	ENDIF 
	
	SELECT uCrsE1
	lcSQL = ""

	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_receituario_getPlanos '<<Alltrim(uCrsE1.u_assfarm)>>','<<Alltrim(SELPLANOCOMP.txtEntidade.value)>>','<<Alltrim(SELPLANOCOMP.txtEntCod.value)>>','<<Alltrim(SELPLANOCOMP.txtPlano.value)>>',<<IIF(SELPLANOCOMP.chkInativos.tag=="true",1,0)>>, '<<ALLTRIM(lcPlanoComp)>>',1
	ENDTEXT
	
	IF !uf_gerais_ActGrelha("SELPLANOCOMP.gridpesq", "uCrsPesqPlanos", lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia a verificar a lista de planos dispon�vels.","OK","",16)
		RETURN .F.
	ENDIF
	
	SELPLANOCOMP.gridpesq.Refresh
	SELPLANOCOMP.lblRegistos.caption = ALLTRIM(STR(x)) + " Resultados"

ENDFUNC


**
FUNCTION uf_SELPLANOCOMP_seleccionarPlano

	IF !EMPTY(SELPLANOCOMP.dem) AND !EMPTY(SELPLANOCOMP.cabordem)
		SELECT Fi 
		LOCATE FOR lordem == SELPLANOCOMP.cabordem
		IF FOUND()
			**
		ENDIF 
	ENDIF  

	IF USED(pCrs) AND !(LEFT(pCrs,3)=="uf_")
		
		SELECT &pCrs
		lnCount = ALINES(Words, pCampos, .T., "|")
		lnCount2 = ALINES(Words2, pCampos2, .T., "|")
		
		FOR i=1 TO lnCount
			replace &pCrs..&Words[i] WITH uCrsPesqPlanos.&Words2[i]
		ENDFOR
	ELSE
		IF LEFT(pCrs,3) == "uf_"
			DO &pCrs WITH uCrsPesqPlanos.&pCampos, pCampos2
		ELSE
			uf_perguntalt_chama("O CURSOR DO PAR�METRO N�O EST� A SER UTILIZADO.","OK","", 16)
		ENDIF
	ENDIF
	
	uf_selplanocomp_sair()
ENDFUNC 


**
FUNCTION uf_selplanocomp_limpaFiltros
	selplanocomp.txtEntCod.Value = ''
	selplanocomp.txtPlano.Value = ''

	uf_SELPLANOCOMP_FiltraDados()
ENDFUNC


**
FUNCTION uf_selplanocomp_tecladoVirtual
	selplanocomp.tecladoVirtual1.show("selplanocomp.gridPesq", 161, "selplanocomp.shpGrid", 161)
ENDFUNC


**
FUNCTION uf_SELPLANOCOMP_sair
	SELPLANOCOMP.hide
	SELPLANOCOMP.release
ENDFUNC
