
**
FUNCTION uf_sistemaconfigemp_chama
	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Configura��o do sistema')
		**
	ELSE
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE CONSULTAR AS CONFIGURA��ES DE SISTEMA.","OK","",48)
		RETURN .f.
	ENDIF
	
	
	IF !USED("uCrsParameterssiteSite")
		IF !uf_gerais_actgrelha("", "uCrsParameterssite", [exec up_parameters_site ']+ALLTRIM(mySite)+['])
			uf_perguntalt_chama("OCORREU UM ERRO A LER OS PAR�METROS DO SITE.","OK","",16)
		ENDIF
	ENDIF
	
	SELECT uCrsUser
	GO TOP
	IF(ALLTRIM(UPPER(uCrsUser.iniciais)) != "ADM")
		SELECT uCrsParameterssite 
		SET FILTER TO uCrsParameterssite.visivel==.t.
		GO top
	ENDIF
	
			
	IF TYPE("sistemaconfigemp") == "U"
		**Valida Licenciamento	
		IF (uf_gerais_addConnection('SISTEMA') == .f.)
			RETURN .F.
		ENDIF
		DO FORM sistemaconfigemp
		
	ELSE
		sistemaconfigemp.show
	ENDIF
	**uf_sistemaconfigemp_menu()
	
ENDFUNC

** adiciona op��es ao menu lateral
FUNCTION uf_sistemaconfigemp_menu

	&& Configura menu principal
	sistemaconfigemp.menu1.adicionaOpcao("opcoes","Op��es",myPath + "\imagens\icons\opcoes_w.png","","O")
	sistemaconfigemp.menu1.adicionaOpcao("limpar","Limpar",myPath + "\imagens\icons\limpar_w.png","uf_sistemaconfigemp_LimpaFiltros", "L")
	sistemaconfigemp.menu1.adicionaOpcao("actualizar","Actualizar",myPath + "\imagens\icons\actualizar_w.png","uf_sistemaconfigemp_aplicaFiltros", "A")
	**sistemaconfigemp.menu1.adicionaOpcao("cfgRobot","Config. Robot",myPath + "\imagens\icons\sistema_w.png","uf_configRobot_chama")

	sistemaconfigemp.menu1.estado("", "SHOW", "GRAVAR", .t., "CANCELAR", .t.)
	**sistemaconfigemp.menu_opcoes.adicionaOpcao("Cambios","Gest�o C�mbios","","uf_cambios_chama")
	
	sistemaconfigemp.Menu1.gravar.img1.Picture = myPath + "\imagens\icons\guardar_w.png"
	
	sistemaconfigemp.Icon = ALLTRIM(mypath) + "\imagens\icons\logitools.ico"
ENDFUNC


**
FUNCTION uf_sistemaconfigemp_aplicaFiltros
	
	LOCAL LcFilter
	STORE '' TO LcFilter
	
	IF(ALLTRIM(UPPER(uCrsUser.iniciais)) != "ADM")
		LcFilter =	"uCrsParameterssite.visivel==.t."
	ENDIF
	
	DO CASE 
		CASE !EMPTY(ALLTRIM(UPPER(sistemaconfigemp.grupo.value))) AND !EMPTY(ALLTRIM(UPPER(sistemaconfigemp.descricao.value)))
			SELECT uCrsParameterssite
			SET FILTER TO  ALLTRIM(UPPER(uCrsParameterssite.type)) =  ALLTRIM(UPPER(sistemaconfigemp.grupo.value)) AND LIKE ('*'+ALLTRIM(UPPER(sistemaconfigemp.descricao.value))+'*',ALLTRIM(UPPER(uCrsParameterssite.name)))
			
		CASE !EMPTY(ALLTRIM(UPPER(sistemaconfigemp.grupo.value))) AND EMPTY(ALLTRIM(UPPER(sistemaconfigemp.descricao.value)))
			SELECT uCrsParameterssite
			SET FILTER TO  ALLTRIM(UPPER(uCrsParameterssite.type)) =  ALLTRIM(UPPER(sistemaconfigemp.grupo.value)) 
		
		CASE EMPTY(ALLTRIM(UPPER(sistemaconfigemp.grupo.value))) AND !EMPTY(ALLTRIM(UPPER(sistemaconfigemp.descricao.value)))
			SELECT uCrsParameterssite
			SET FILTER TO LIKE ('*'+ALLTRIM(UPPER(sistemaconfigemp.descricao.value))+'*',ALLTRIM(UPPER(uCrsParameterssite.name)))

		OTHERWISE
			SELECT uCrsParameterssite 
			SET FILTER TO 
	ENDCASE

	SELECT uCrsParameterssite 
	LcFilter = IIF(EMPTY(FILTER()),"uCrsParameterssite.visivel==.t. ", FILTER() ) + IIF(!uf_gerais_compstr(uCrsUser.iniciais,"ADM")," and uCrsParameterssite.visivel==.t. ", "")
	
	SELECT uCrsParameterssite
	SET FILTER TO &LcFilter. 

	SELECT uCrsParameterssite 
	GO Top
	sistemaconfigemp.PageFrame1.Page1.gridPesq.refresh
ENDFUNC


**
FUNCTION uf_sistemaconfigemp_gravar
	LOCAL lcErro 
	STORE '' TO lcErro
	
	regua(0,100,"A PROCESSAR A GRAVA��O DOS PARAMETROS...",.f.)

    uf_gerais_actGrelha("","ucrsParametersAux","SELECT * FROM B_PARAMETERS_site(NOLOCK) where site = '" + ALLTRIM(mySite) + "'")

	SELECT uCrsParameterssite 
	GO Top
	SCAN

        uv_descAlt = ""
        uv_logStamp = uf_gerais_Stamp()

        SELECT ucrsParametersAux
        LOCATE FOR ucrsParametersAux.stamp = uCrsParameterssite.stamp

        IF FOUND()

            IF !(ALLTRIM(uCrsParameterssite.textValue) == ALLTRIM(ucrsParametersAux.textValue))
                uv_descAlt = "textValue: " + IIF(EMPTY(ALLTRIM(ucrsParametersAux.textValue)), "(Vazio)", ALLTRIM(ucrsParametersAux.textValue)) + " <---> " + IIF(EMPTY(uCrsParameterssite.textValue), "(Vazio)", ALLTRIM(uCrsParameterssite.textValue))
            ENDIF

            IF uCrsParameterssite.numValue <> ucrsParametersAux.numValue
                uv_descAlt = uv_descAlt + IIF(!EMPTY(uv_descAlt), " ;", "") + "numValue: " + ASTR(ucrsParametersAux.numValue,8,4) + " <---> " + ASTR(uCrsParameterssite.numValue,8,4)
            ENDIF

            IF uCrsParameterssite.bool <> ucrsParametersAux.bool
                uv_descAlt = uv_descAlt + IIF(!EMPTY(uv_descAlt), " ;", "") + "bool: " + IIF(ucrsParametersAux.bool, "Ativo", "Inativo") + " <---> " + IIF(uCrsParameterssite.bool, "Ativo", "Inativo")
            ENDIF

        ENDIF
		
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			UPDATE 
				b_parameters_site
			SET
				textValue 	= '<<uCrsParameterssite.textValue>>'
				,numValue 	= <<uCrsParameterssite.numValue>>
				,bool		= <<IIF(uCrsParameterssite.bool,1,0)>>
				,obs	 	= '<<ALLTRIM(ucrsParameterssite.obs)>>'
			WHERE
				stamp 		= '<<uCrsParameterssite.stamp>>'
				and site	= '<<mysite>>'


            IF '<<ALLTRIM(uv_descAlt)>>' <> ''
            BEGIN

                INSERT INTO logParameters
                VALUES
                (
                    '<<uv_logStamp>>',
                    '<<ALLTRIM(uCrsParameterssite.stamp)>>',
                    '<<ALLTRIM(uv_descAlt)>>',
                    '<<ALLTRIM(m_chinis)>>',
                    CONVERT(VARCHAR, GETDATE(), 112),
                    LEFT(CONVERT(TIME, GETDATE()),8),
                    '<<ALLTRIM(mySite)>>',
                    1
                )

            END

		ENDTEXT 
		
		IF !uf_gerais_actGrelha("", "", lcSQL)
			lcErro = lcErro + " " +  ALLTRIM(STR(ucrsParameters.id))
		ENDIF
	ENDSCAN

    FECHA("ucrsParametersAux")
	
	REGUA(2)



	IF !EMPTY(ALLTRIM(lcErro))
		uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR O PAR�METRO " + lcErro ,"OK","", 16)
	ELSE

		uf_perguntalt_chama("Para aplicar as altera��es o Software ir� ser reiniciado." ,"OK","", 64)
		uf_logitools_sair()

		**LOCAL lcSql1
		**lcsql1 = ''
	    **TEXT TO lcsql1 TEXTMERGE NOSHOW
		**		exec up_parametersVisiveissite '<<ALLTRIM(mysite)>>', '<<ALLTRIM(m_chinis)>>'
	    **ENDTEXT
		****Carrega nova configura��o dos parametros
		**IF !uf_gerais_actgrelha("sistemaconfigemp.PageFrame1.Page1.gridPesq", "uCrsParameterssite", lcsql1 )
		**	uf_perguntalt_chama("OCORREU UM ERRO A LER OS PAR�METROS.","OK","",16)
		**ENDIF
**
		**uf_perguntalt_chama("ACTUALIZA��O CONCLUIDA COM SUCESSO!" ,"OK","", 64)
	ENDIF
ENDFUNC


**
FUNCTION uf_sistemaconfigemp_LimpaFiltros
	sistemaconfigemp.grupo.value = ""
	sistemaconfigemp.descricao.value = ""
	sistemaconfigemp.refresh
ENDFUNC


**
FUNCTION uf_sistemaconfigemp_sair
	SELECT uCrsParameterssite
	SET FILTER TO 
	**
	sistemaconfigemp.hide
	sistemaconfigemp.release
ENDFUNC