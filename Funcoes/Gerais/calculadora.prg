FUNCTION uf_calculadora_chama
	&& Controla Abertura do Painel
	if type("CALCULADORA")=="U"
		DO FORM CALCULADORA
	ELSE
		CALCULADORA.show
	ENDIF
	
	CALCULADORA.txtvalor.setfocus
ENDFUNC


**
FUNCTION uf_calculadora_carregamenu
	calculadora.menu1.estado("", "", "Gravar", .f., "Sair", .t.)
ENDFUNC


**
FUNCTION uf_calculadora_KeypressPainel
	LPARAMETERS nKeyCode
	
	If nKeyCode == 7
		uf_calculadora_botaoPress("del")
		Return
	Endif

	If !(CALCULADORA.ActiveControl.name == 'txtValor') And  !(CALCULADORA.ActiveControl.name == 'txtValor2')
		Return .f.
	Endif
		
	Do Case
		Case nKeyCode == 13
			uf_calculadora_PermiteVariasParcelas()
		Case nKeyCode == 45
			uf_calculadora_PermiteVariasParcelas()
			uf_calculadora_botaoPress("menos")
		Case nKeyCode == 42
			uf_calculadora_PermiteVariasParcelas()	
			uf_calculadora_botaoPress("vezes")
		Case nKeyCode == 43
			uf_calculadora_PermiteVariasParcelas()
			uf_calculadora_botaoPress("mais")
		Case nKeyCode == 46
			CALCULADORA.tecladovirtual1.pageframe1.page3.ponto.click()
		Case nKeyCode == 47
			uf_calculadora_PermiteVariasParcelas()
			uf_calculadora_botaoPress("dividir")
		OTHERWISE
			***
	ENDCASE
	
	CALCULADORA.txtvalor3.click()
ENDFUNC 


**
Function uf_calculadora_botaoPress
	LParameters lcTexto
		
	Local lcLen, lcCaixa, lcObj
	Store "" to lcCaixa, lcObj
	lcLen=0
	
	DO CASE
		Case CALCULADORA.txtvalor.backcolor = RGB(230, 242, 255)
			lcCaixa='txtvalor'
			
			If (OCCURS(".", CALCULADORA.txtvalor.value) > 0) AND (lcTexto = ".")
				Return
			Endif

		CASE CALCULADORA.txtvalor2.backcolor = RGB(230,242,255)
			lcCaixa='txtvalor2'
			IF (OCCURS(".", CALCULADORA.txtvalor2.value)>0) AND (lcTexto=".")
				Return
			ENDIF

		OTHERWISE
			IF lcTexto == "del"
				CALCULADORA.txtvalor.value = ''
				CALCULADORA.txtvalor2.value = ''
				CALCULADORA.txtvalor3.value = ''
				CALCULADORA.txtvalor.setfocus
			ENDIF
			
			RETURN
	ENDCASE
	
	lcObj = 'CALCULADORA.' + lcCaixa
	
	IF lcTexto == "backdel"
		lcLen = Len(ALLTRIM(&lcObj..value))
		&lcObj..value = Left(&lcObj..value,lcLen-1)
	ELSE
		DO CASE
			CASE lcTexto == "del"
				CALCULADORA.txtvalor.value = ''
				CALCULADORA.txtvalor2.value = ''
				CALCULADORA.txtvalor3.value = ''
				CALCULADORA.txtvalor.setfocus
				
			CASE lcTexto == "clear"
				&lcObj..value = ""
			
			CASE lcTexto == "enter"
				uf_calculadora_PermiteVariasParcelas()
			
			CASE lcTexto == "mais"
				uf_calculadora_PermiteVariasParcelas()
				CALCULADORA.lblOperador.caption = "+"
				CALCULADORA.lblOperador.fontsize = 35
				CALCULADORA.txtvalor2.setfocus
						
			CASE lcTexto == "menos"
				uf_calculadora_PermiteVariasParcelas()
				CALCULADORA.lblOperador.caption = "-"
				CALCULADORA.lblOperador.fontsize = 35
				CALCULADORA.txtvalor2.setfocus
		
			CASE lcTexto == "dividir"
				uf_calculadora_PermiteVariasParcelas()
				CALCULADORA.lblOperador.caption = "/"
				CALCULADORA.lblOperador.fontsize = 30
				CALCULADORA.txtvalor2.setfocus
	
			CASE lcTexto == "vezes"
				uf_calculadora_PermiteVariasParcelas()
				CALCULADORA.lblOperador.caption = "x"
				CALCULADORA.lblOperador.fontsize = 35
				CALCULADORA.txtvalor2.setfocus
			
			CASE lcTexto == "maismenos"
				IF !EMPTY(ALLTRIM(&lcObj..value))
					&lcObj..value = astr(Val(&lcObj..value) * (-1), 8, 2)
					&lcObj..setfocus
					&lcObj..selstart = Len(astr(&lcObj..value,8,2)) + 1
					&lcObj..sellength = 0
				ENDIF
				
			OTHERWISE
				If Len(ALLTRIM(&lcObj..value))>=10
					RETURN
				ENDIF
		
				&lcObj..value = ALLTRIM(&lcObj..value) + lcTexto
				&lcObj..setfocus
				&lcObj..selstart = Len(astr(&lcObj..value,8,2)) + 1
				&lcObj..sellength = 0
		ENDCASE
	ENDIF
ENDFUNC


**
Function uf_calculadora_PermiteVariasParcelas
	If !(Empty(astr(CALCULADORA.txtvalor.value))) And !(Empty(astr(CALCULADORA.txtvalor2.value)))
		
		uf_calculadora_calculaTotal()
		
		CALCULADORA.txtvalor.value = CALCULADORA.txtvalor3.value
		CALCULADORA.txtvalor2.value = ""
		CALCULADORA.txtvalor2.setfocus
	Endif	
Endfunc


**
Function uf_calculadora_calculaTotal
	If Empty(CALCULADORA.txtvalor.value) or Empty(CALCULADORA.txtvalor2.value)
		RETURN
	ENDIF
	
	SET Point TO "."
	
	DO CASE
		CASE Alltrim(CALCULADORA.lblOperador.caption) == "+"
			CALCULADORA.txtvalor3.value = astr(Val(CALCULADORA.txtvalor.value) + Val(CALCULADORA.txtvalor2.value),8,2)
				
		CASE Alltrim(CALCULADORA.lblOperador.caption) == "-"
			CALCULADORA.txtvalor3.value = astr(Val(CALCULADORA.txtvalor.value) - Val(CALCULADORA.txtvalor2.value),8,2)
			
		CASE Alltrim(CALCULADORA.lblOperador.caption) == "/"
			If  val(CALCULADORA.txtvalor2.value)==0
				uf_perguntalt_chama("N�O PODE DIVIDIR POR ZERO.","OK","", 48)
			Else
				CALCULADORA.txtvalor3.value = astr(Val(CALCULADORA.txtvalor.value) / Val(CALCULADORA.txtvalor2.value),8,2)
			Endif
			
		CASE Alltrim(CALCULADORA.lblOperador.caption) == "x"
			CALCULADORA.txtvalor3.value = astr(Val(CALCULADORA.txtvalor.value) * Val(CALCULADORA.txtvalor2.value),8,2)
			
		OTHERWISE
			RETURN
	ENDCASE
	
	*CALCULADORA.txtvalor3.value = astr(Round(val(CALCULADORA.txtvalor3.value),2))

ENDFUNC


**
FUNCTION uf_calculadora_sair
	CALCULADORA.hide
	CALCULADORA.release
ENDFUNC


**
FUNCTION uf_calculadora_gravar
ENDFUNC