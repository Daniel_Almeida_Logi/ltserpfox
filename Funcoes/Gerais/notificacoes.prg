**
FUNCTION uf_notificacoes_chama

	lcSQL = ''
	TEXT TO lcSql NOSHOW TEXTMERGE
		select 
			stamp
			, origem
			, usrorigem
			, destino
			, usrdestino
			, grpdestino
			, ecradestino
			, stampdestino
			, cast(mensagem as varchar(8000)) as mensagem 
			, tiponotif
			, recebido
			,data
		from notificacoes (NOLOCK)
		WHERE (usrdestino='<<ALLTRIM(m_chinis)>>' OR grpdestino='<<ALLTRIM(ch_grupo)>>')
			and data between '19000101' and getdate()
			and recebido=0
	ENDTEXT
				
	IF !uf_gerais_actGrelha("","uCrsnotificacoes",lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL OBTER O REGISTO DE MENSAGENS.","OK","",16)
		RETURN .f.
	ENDIF 
	
	PUBLIC lcTipo, lcServico, lcTabela, lcDatade, LcDataa, lcSite
	STORE '' TO lcTipo, lcServico, lcTabela, lcSite
	STORE DATE() TO lcDatade, LcDataa
	IF TYPE("NOTIFICACOES") == "U"
		DO FORM NOTIFICACOES
	ELSE
		NOTIFICACOES.show
	ENDIF

ENDFUNC

FUNCTION uf_notificacoes_gravar

ENDFUNC 

** adiciona op��es ao menu lateral
FUNCTION uf_notificacoes_menu

	&& Configura menu principal
	notificacoes.menu1.adicionaOpcao("actualizar","Actualizar",myPath + "\imagens\icons\actualizar_w.png","uf_notificacoes_aplicaFiltros", "A")
	notificacoes.menu1.adicionaOpcao("limpar","Limpar",myPath + "\imagens\icons\limpar_w.png","uf_notificacoes_LimpaFiltros", "L")
	notificacoes.menu1.adicionaOpcao("verreg","Aceder",myPath + "\imagens\icons\detalhe.png","uf_notificacoes_verreg", "D")
	notificacoes.menu1.adicionaOpcao("responder","Responder",myPath + "\imagens\icons\devolver_w.png","uf_notificacoes_responder", "X")
	notificacoes.menu1.adicionaOpcao("lida","Marcar Lida",myPath + "\imagens\icons\checked_w.png","uf_notificacoes_marcarlida", "D")

	notificacoes.menu1.estado("", "SHOW", "", .t., "Sair", .t.)
	notificacoes.menu1.estado("verreg, responder", "HIDE")
		
	notificacoes.Icon = ALLTRIM(mypath) + "\imagens\icons\logitools.ico"
ENDFUNC


**
FUNCTION uf_notificacoes_aplicaFiltros
		
	lcSQL = ''
	TEXT TO lcSql NOSHOW TEXTMERGE
		select 
			stamp
			, origem
			, usrorigem
			, destino
			, usrdestino
			, grpdestino
			, ecradestino
			, stampdestino
			, cast(mensagem as varchar(8000)) as mensagem 
			, tiponotif
			, recebido
			,data
		from notificacoes (NOLOCK)
		WHERE (usrdestino='<<ALLTRIM(m_chinis)>>' OR grpdestino='<<ALLTRIM(ch_grupo)>>')
			and data between '<<uf_gerais_getDate(notificacoes.txtde.value,"SQL")>>' and '<<uf_gerais_getDate(notificacoes.txta.value,"SQL")>>'
	ENDTEXT
	IF !empty(notificacoes.txttype.value)
		lcSQL1 = ''
		TEXT TO lcSql1 NOSHOW TEXTMERGE
			and tiponotif = '<<ALLTRIM(notificacoes.txttype.value)>>'
		ENDTEXT
		lcSql = lcSql + CHR(13) + lcSql1 
	ENDIF 
	IF notificacoes.sel.tag="true"
		lcSQL2 = ''
		TEXT TO lcSQL2 NOSHOW TEXTMERGE
			and recebido=0
		ENDTEXT
		lcSql = lcSql + CHR(13) + lcSql2 
	ELSE 
		lcSQL2 = ''
		TEXT TO lcSQL2 NOSHOW TEXTMERGE
			and recebido=1
		ENDTEXT
		lcSql = lcSql + CHR(13) + lcSql2 
	ENDIF 
	**_cliptext=lcSql 
				
	IF !uf_gerais_actGrelha("notificacoes.grdpesq","uCrsnotificacoes",lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL OBTER O REGISTO DE MENSAGENS.","OK","",16)
		RETURN .f.
	ENDIF 

	notificacoes.grdpesq.refresh

ENDFUNC

**
FUNCTION uf_notificacoes_LimpaFiltros
	NOTIFICACOES.txttype.value=''
	NOTIFICACOES.txtde.Value = uf_gerais_getdate('19000101')
	NOTIFICACOES.txta.Value = uf_gerais_getdate(DATE())
ENDFUNC


FUNCTION uf_notificacoes_sair	
	fecha("uCrsnotificacoes")
	&& fecha painel
	NOTIFICACOES.hide
	NOTIFICACOES.release 
ENDFUNC

FUNCTION uf_notificacoes_verreg
	LOCAL lcTipoAviso, lcStampreg
	SELECT uCrsnotificacoes
	lcTipoAviso = ALLTRIM(uCrsnotificacoes.ecradestino)
	lcStampreg = ALLTRIM(uCrsnotificacoes.stampdestino)
	DO CASE 
		CASE ALLTRIM(UPPER(lcTipoAviso)) == "BO"
			uf_documentos_Chama(lcStampreg)
		CASE ALLTRIM(UPPER(lcTipoAviso)) == "FT"
			uf_facturacao_chama(lcStampreg)
		OTHERWISE 
	ENDCASE 
	
ENDFUNC 

FUNCTION uf_notificacoes_marcarlida
	LOCAL lcstampact
	lcstampact = ALLTRIM(uCrsnotificacoes.stamp)
	lcSQL = ''
	TEXT TO lcSql NOSHOW TEXTMERGE
		UPDATE notificacoes 
		SET recebido = 1
		WHERE stamp = '<<ALLTRIM(lcstampact)>>'
	ENDTEXT
				
	IF !uf_gerais_actGrelha("","",lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL ATUALIZAR A MENSAGEM.","OK","",16)
		RETURN .f.
	ENDIF 
	
	uf_notificacoes_aplicaFiltros()
ENDFUNC 

FUNCTION uf_notificacoes_responder
	notificacoes.mensagem.height=277
	notificacoes.Editresposta.visible=.t.
	notificacoes.lblresposta.visible=.t.
	notificacoes.btnresponder.visible=.t.
	notificacoes.txtbtnresponder.visible=.t.
ENDFUNC 

FUNCTION uf_notificacoes_sellin
	notificacoes.mensagem.Value = uCrsnotificacoes.mensagem
	notificacoes.mensagem.refresh
	IF !EMPTY(uCrsnotificacoes.stampdestino)
		notificacoes.menu1.estado("verreg", "SHOW")
	ELSE
		notificacoes.menu1.estado("verreg", "HIDE")
	ENDIF 
	IF !EMPTY(uCrsnotificacoes.usrorigem)
		notificacoes.menu1.estado("responder", "SHOW")
	ELSE
		notificacoes.menu1.estado("responder", "HIDE")
	ENDIF 
ENDFUNC 