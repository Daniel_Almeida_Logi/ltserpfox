**
FUNCTION uf_insereTexto_chama
	LPARAMETERS tcTextoPergunta, lcLabelGravar, lcLabelSair, lcTipo, lcLabelCancelar, lcImgCancelar, lcImgGravar, lcImgSair
	PUBLIC myRetornoPergunta, myRetornoCancelar
	myRetornoPergunta = .f.
	myRetornoCancelar = .f.
	
	**
	IF TYPE("INSERETEXTO") == 'U'
		DO FORM INSERETEXTO WITH tcTextoPergunta, lcLabelGravar, lcLabelSair, lcTipo, lcLabelCancelar, lcImgCancelar, lcImgGravar, lcImgSair
	ELSE
		INSERETEXTO.show
	ENDIF
	

	RETURN myRetornoPergunta

ENDFUNC

**
FUNCTION uf_inseretexto_gravar
	
	TRY 
		myRetornoPergunta = Inseretexto.txttexto.value
		INSERETEXTO.hide
		INSERETEXTO.release
	ENDTRY
	
ENDFUNC



