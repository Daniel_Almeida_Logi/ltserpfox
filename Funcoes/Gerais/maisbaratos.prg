**
FUNCTION uf_MaisBaratos_chama
	LPARAMETERS lcOpcao,lcRef,lcDesign	
	LOCAL lcSQL
	
	&& Valida Parametros
	IF EMPTY("lcOpcao") OR !(TYPE("lcOpcao") == "N")
		uf_perguntalt_chama("O PAR�METRO: OP��O, N�O � V�LIDO.","OK","",48)
		RETURN .F.
	ENDIF
	
	IF EMPTY("lcRef") OR !(TYPE("lcRef") == "C")
		uf_perguntalt_chama("O PAR�METRO: REF, N�O � V�LIDO.","OK","",48)
		RETURN .F.
	ENDIF
	
	IF EMPTY("lcDesign") OR !(TYPE("lcDesign") == "C")
		uf_perguntalt_chama("O PAR�METRO: DESIGNA��O, N�O � V�LIDO.","OK","",48)
		RETURN .F.
	ENDIF
	
	DO CASE
		CASE lcOpcao = 1 && chamado da Prescri��o Electr�nica				
			Text to lcSql noshow textmerge
				exec up_Prescricoes_5maisBaratos '<<Alltrim(lcRef)>>'
			ENDTEXT
		
		CASE lcOpcao = 2 && Chamado do Atendimento
			Text to lcSql noshow textmerge
				exec up_touch_5maisBaratos '<<Alltrim(lcRef)>>',<<myArmazem>>
			ENDTEXT
		
		CASE lcOpcao = 3 && Chamado das Encomendas Autom�ticas			
			SELECT crsenc 
			Text to lcSql noshow textmerge
				exec up_enc_5maisBB '<<Alltrim(lcRef)>>',<<myArmazem>>, <<crsenc.fornec>>,<<crsenc.fornestab>>, <<mysite_nr>>
			ENDTEXT	
		OTHERWISE
			***
	ENDCASE			

	PUBLIC myOrdermaisbaratos 	
	myOrdermaisbaratos = .t.

	&& Cria o cursor
	uf_gerais_ActGrelha("","uCrs5MaisBaratos",lcSQL)
	
	&& Controla Abertura do Painel
	IF TYPE("MaisBaratos") == "U"
		DO FORM MaisBaratos WITH lcRef,lcDesign,lcOpcao&& painel � modal - config � feita no init
	ELSE
		MaisBaratos.show()
	ENDIF	

ENDFUNC


**************************************************
**	Selecciona uma refere�ncia para substituir  **
**************************************************
FUNCTION uf_MaisBaratos_SelProd	

	SELECT uCrs5MaisBaratos
	lcSQL=''
	TEXT TO lcSQL TEXTMERGE noshow
		select 
			top 1 ref 
		from 
			st (nolock) 
		where 
			ref = '<<ALLTRIM(uCrs5MaisBaratos.ref)>>'
	ENDTEXT 
	uf_gerais_actgrelha("","uCrsTmpVerifRef",lcSQL)
	IF RECCOUNT("uCrsTmpVerifRef")==0		
*!*			replace ALL sel WITH .F. IN uCrs5MaisBaratos
*!*			GO top
		maisbaratos.gridpesq.columns(4).check1.value=0
		maisbaratos.gridpesq.refresh()
		
		uf_perguntalt_chama("O PRODUTO SELECCIONADO N�O TEM FICHA CRIADA. PODER� CRIAR A FICHA ATRAV�S DO PAINEL DE DICION�RIO.","OK","",64)
		RETURN .f.
	ENDIF 

	DO case
		CASE MaisBaratos.opcao = 1
			
		CASE MaisBaratos.opcao = 2	&& Atendimento	
			IF uf_perguntalt_chama("VAI SUBSTITUIR O PRODUTO ACTUAL PELO SELECCIONADO. PRETENDE CONTINUAR?" + CHR(13) + IIF(uCrs5MaisBaratos.stock < 1,"O PRODUTO SELECCIONADO N�O TEM STOCK DISPON�VEL!",""),"Sim","N�o")							
				LOCAL lcPosFi
				
				SELECT uCrs5MaisBaratos
				 
				SELECT fi
				REPLACE fi.ref WITH ALLTRIM(uCrs5MaisBaratos.ref)
				
				SELECT uCrsAtendST
				LOCATE FOR uCrsAtendST.ststamp = fi.fistamp
				DELETE
				
				SELECT fi
				lcPosFi = RECNO("fi")
				uf_atendimento_actRef()
				SELECT fi
				TRY 
					GO lcPosFi
				CATCH
					GO bottom
				ENDTRY
				uf_atendimento_eventoFi()
				SELECT fi
				TRY 
					GO lcPosFi
				CATCH
					GO bottom
				ENDTRY
				&& actualiza o ecra
				uf_atendimento_dadosST()			
				SELECT fi
				TRY 
					GO lcPosFi
				CATCH
					GO bottom
				ENDTRY
				&& fecha o painel
				uf_MaisBaratos_sair()
				
				&& coloca o foco na grelha
				atendimento.grdAtend.setfocus()
			ELSE
				replace ALL sel WITH .F. IN uCrs5MaisBaratos
				GO top
				
				&& actualiza o painel
				maisbaratos.refresh()
			ENDIF	
		CASE MaisBaratos.opcao = 3 && Encomendas Autom�ticas
			
			IF Used("crsenc")
				
				** procura produto **
				SELECT uCrs5MaisBaratos
				lcSQL = ""
				TEXT TO lcSQL NOSHOW TEXTMERGE 
					exec up_enc_autoref <<myArmazem>>,'<<Alltrim(uCrs5MaisBaratos.ref)>>', '', <<mysite_nr>>
				ENDTEXT 
				IF uf_gerais_actgrelha("", "uCrsAddProdPea", lcSQL)
					IF reccount("uCrsAddProdPea") == 0
						uf_perguntalt_chama("N�O � POSSIVEL ADICIONAR ESTE PRODUTO.","OK","",64)
						RETURN .f.
					ELSE
						** verifica se produto j� existe **
					
						Local lcCrsencPos, lcValidaAddPea
						SELECT crsenc
						lcCrsencPos = RECNO()

						SELECT * from crsenc WITH (BUFFERING = .T.) INTO CURSOR uc_tmpCrsenc
						
						SELECT uc_tmpCrsenc
						GO TOP
						SCAN
							IF Alltrim(uc_tmpCrsenc.ref) == Alltrim(uCrsAddProdPea.ref)
								uf_perguntalt_chama("O PRODUTO QUE ESCOLHEU J� EXISTE NESTA ENCOMENDA AUTOM�TICA.","OK","",64)

								**Select crsenc
								**Try
								**	Go lcCrsencPos
								**Catch
								**	***
								**Endtry

								uf_MaisBaratos_sair()
								return .f.
							ENDIF
						ENDSCAN
						***************************	

						** adiciona produto **
						Select crsenc
						**LOCATE FOR ALLTRIM(uCrs5MaisBaratos.oref) == ALLTRIM(crsenc.ref)
						
						replace crsenc.ststamp		With 	Alltrim(uCrsAddProdPea.ststamp)
						replace crsenc.u_fonte		With 	Alltrim(uCrsAddProdPea.u_fonte)
						replace crsenc.ref			With 	Alltrim(uCrsAddProdPea.ref)
						replace crsenc.refb			With 	Alltrim(uCrsAddProdPea.refb)
						replace crsenc.codigo		With 	Alltrim(uCrsAddProdPea.codigo)
						replace crsenc.design		With 	Alltrim(uCrsAddProdPea.design)
						replace crsenc.stock		With 	uCrsAddProdPea.stock
						replace crsenc.stmin		With 	uCrsAddProdPea.stmin
						replace crsenc.stmax		With 	uCrsAddProdPea.stmax
						replace crsenc.ptoenc		With 	uCrsAddProdPea.ptoenc
						replace crsenc.eoq			With 	uCrsAddProdPea.eoq
						replace crsenc.qtbonus		With 	uCrsAddProdPea.qtbonus
						replace crsenc.qttadic		With 	uCrsAddProdPea.qttadic
						replace crsenc.qttfor		With 	uCrsAddProdPea.qttfor
						replace crsenc.qttacin		With 	uCrsAddProdPea.qttacin
						replace crsenc.qttcli		With 	uCrsAddProdPea.qttcli
						replace crsenc.fornecedor	With 	Alltrim(uCrsAddProdPea.fornecedor)
						replace crsenc.fornec		With 	uCrsAddProdPea.fornec
						replace crsenc.fornestab	With 	uCrsAddProdPea.fornestab
						replace crsenc.epcusto		with 	uCrsAddProdPea.epcusto
						replace crsenc.epcpond		With 	uCrsAddProdPea.epcpond
						replace crsenc.epcult		With 	uCrsAddProdPea.epcult
						replace crsenc.tabiva		With 	uCrsAddProdPea.tabiva
						replace crsenc.iva			With 	uCrsAddProdPea.iva
						replace crsenc.cpoc			With 	uCrsAddProdPea.cpoc
						replace crsenc.familia		With 	uCrsAddProdPea.familia
						replace crsenc.faminome		With 	alltrim(uCrsAddProdPea.faminome)
						replace crsenc.u_lab		With 	Alltrim(uCrsAddProdPea.u_lab)
						replace crsenc.conversao	With 	uCrsAddProdPea.conversao
						replace crsenc.stockgh		With 	uCrsAddProdPea.stockgh
						replace crsenc.bonusfornec	With 	Alltrim(uCrsAddProdPea.bonusfornec)
						Replace crsenc.pclfornec	WITH 	uCrsAddProdPea.pclfornec
						replace crsenc.esgotado		With 	Alltrim(uCrsAddProdPea.esgotado)
						replace crsenc.marg4		With 	uCrsAddProdPea.marg4
						**Alertas
						replace crsenc.alertaStockGH With	uCrsAddProdPea.alertaStockGH
						replace crsenc.alertaPCL 	With	uCrsAddProdPea.alertaPCL
						replace crsenc.alertaBonus 	With	uCrsAddProdPea.alertaBonus

						replace crsenc.enc			With 	.f.
						replace crsenc.sel			with	.f.
					ENDIF
					
					Fecha("uCrsAddProdPea")
		
					encautomaticas.gridPesq.refresh
					uf_perguntalt_chama("PRODUTO SUBSTITUIDO COM SUCESSO!","OK","",64)
					uf_MaisBaratos_sair()
					
				ENDIF
			ENDIF	
		
		OTHERWISE
			**
	ENDCASE

ENDFUNC



**Fechar Ecra de Pesquisa de 5MAISBARATOS
FUNCTION uf_MaisBaratos_sair
		
	&& Fecha painel
	MAISBARATOS.release
	
	&& Liberta variaveis publicas
	RELEASE MAISBARATOS	
	
	&& Fecha Cursores
	IF USED("uCrs5MaisBaratos")
		fecha("uCrs5MaisBaratos")
	ENDIF	

ENDFUNC
