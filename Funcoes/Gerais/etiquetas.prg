**
FUNCTION uf_etiquetas_chama
	LPARAMETERS lcPainel

	PUBLIC pPainelEtiq
	pPainelEtiq = lcPainel
	
	&& Controla Abertura do Painel			
	IF type("ETIQUETAS")=="U"
		DO FORM ETIQUETAS
		ETIQUETAS.show
	ELSE
		ETIQUETAS.show
	ENDIF
ENDFUNC


**
FUNCTION uf_etiquetas_impEtiqSel
	LPARAMETERS tcNum
	
	LOCAL lcReportNormalCPVP, lcReportNormalSPVP, lcReportAlarmeCPVP, lcReportAlarmeSPVP, lcReportPrateleiraPVP, lcReportPrateleiraPVPA4, lcReportBrotherPVP
	
	LOCAL lcCount, lcPrinter,lcOldPrinter
	STORE 0 To lcCount
	lcOldPrinter= set("printer",2)
	
	IF EMPTY(pPainelEtiq)
		pPainelEtiq = ''
	ENDIF
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select documento,nomeficheiro from B_impressoes (nolock) where tabela = 'ETIQUETA'
	ENDTEXT
	If !uf_gerais_actGrelha("", "ucrsEtiquetasFicheiro", lcSql)
		uf_perguntalt_chama("N�o foi possivel verificar a configura��o de etiquetas. Por favor contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	
	Select ucrsEtiquetasFicheiro
	LOCATE FOR ALLTRIM(UPPER(documento)) == "NORMAL_C_PVP"
	IF FOUND()
		lcReportNormalCPVP = ALLTRIM(ucrsEtiquetasFicheiro.nomeficheiro)
	ENDIF 
	Select ucrsEtiquetasFicheiro
	LOCATE FOR ALLTRIM(UPPER(documento)) == "NORMAL_S_PVP"
	IF FOUND()
		lcReportNormalSPVP = ALLTRIM(ucrsEtiquetasFicheiro.nomeficheiro)
	ENDIF 
	Select ucrsEtiquetasFicheiro
	LOCATE FOR ALLTRIM(UPPER(documento)) == "ALARME_C_PVP"
	IF FOUND()
		lcReportAlarmeCPVP = ALLTRIM(ucrsEtiquetasFicheiro.nomeficheiro)
	ENDIF 
	Select ucrsEtiquetasFicheiro
	LOCATE FOR ALLTRIM(UPPER(documento)) == "ALARME_S_PVP"
	IF FOUND()
		lcReportAlarmeSPVP = ALLTRIM(ucrsEtiquetasFicheiro.nomeficheiro)
	ENDIF 

	Select ucrsEtiquetasFicheiro
	LOCATE FOR ALLTRIM(UPPER(documento)) == "PRATELEIRA_C_PVP"
	IF FOUND()
		lcReportPrateleiraPVP = ALLTRIM(ucrsEtiquetasFicheiro.nomeficheiro)
	ENDIF 
	
	Select ucrsEtiquetasFicheiro
	LOCATE FOR ALLTRIM(UPPER(documento)) == "PRATELEIRA_C_PVP_FOLHA_A4"
	IF FOUND()
		lcReportPrateleiraPVPA4 = ALLTRIM(ucrsEtiquetasFicheiro.nomeficheiro)		

		&& s� permite esta funcionalidade a partir da ficha do produto
		IF pPainelEtiq != "ST" AND tcNum == 6
			uf_perguntalt_chama("Funcionalidade n�o dispon�vel neste painel. Em caso de d�vida contacte o Suporte.", "", "OK", 32)
			RETURN .f.
		ENDIF
	
		&& informa que as etiquetas ser�o impressas na impressora pre definida 
		IF tcNum == 6
			IF !uf_perguntalt_chama("ATEN��O: As etiquetas ser�o impressas numa folha A4 na impressora predefinida no sistema operativo. Pretende Continuar?","Sim","N�o",48)
				RETURN .f.
			ENDIF
		ENDIF
	ENDIF

	Select ucrsEtiquetasFicheiro
	LOCATE FOR ALLTRIM(UPPER(documento)) == "BROTHER_C_PVP"
	IF FOUND()
		lcReportBrotherPVP = ALLTRIM(ucrsEtiquetasFicheiro.nomeficheiro)
	ENDIF 

	IF EMPTY(lcReportNormalCPVP) OR EMPTY(lcReportNormalSPVP) OR EMPTY(lcReportAlarmeCPVP) OR EMPTY(lcReportAlarmeSPVP) OR EMPTY(lcReportPrateleiraPVP) OR EMPTY(lcReportPrateleiraPVPA4) OR EMPTY(lcReportBrotherPVP)
		uf_perguntalt_chama("N�o foi possivel verificar a configura��o de etiquetas. Por favor contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	
	&& Verifica se os reports de etiquetas existem
	If !File(Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportNormalCPVP));
		OR !File(Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportNormalSPVP));
		OR !File(Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportAlarmeCPVP));
		OR !File(Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportAlarmeSPVP));
		OR !FILE(ALLTRIM(mypath)+'\analises\' + ALLTRIM(lcReportPrateleiraPVP));
		OR !FILE(ALLTRIM(mypath)+'\analises\' + ALLTRIM(lcReportPrateleiraPVPA4));
		OR !FILE(ALLTRIM(mypath)+'\analises\' + ALLTRIM(lcReportBrotherPVP))

		uf_perguntalt_chama("Os IDU's das Etiquetas n�o est�o a ser detectados. Por favor contacte o Suporte.","OK","",48)
		RETURN .f.
	ENDIF
	
	&& Calcula Nome da impressora
	IF tcNum == 5
		lcPrinter = uf_etiquetas_calculaLabelPrinter("PRATELEIRA")
	ELSE
		IF tcNum == 6
			lcPrinter =  "Impressora por defeito do Windows"
		ELSE
			lcPrinter = uf_etiquetas_calculaLabelPrinter("PRODUTO")
		ENDIF
	ENDIF
				
	&& Verificar se a impressora existe
	IF EMPTY(ALLTRIM(lcPrinter))
		uf_perguntalt_chama("A impressora n�o est� a ser detectada. Se a dificuldade persistir contacte o Suporte.","OK","",48)
		RETURN .f.
	ELSE
		IF tcNum != 6
			DIMENSION lcPrinters[1,1]
			APRINTERS(lcPrinters)
			LOCAL lcExistePrinter
			FOR lnIndex = 1 TO ALEN(lcPrinters,1)
				IF upper(alltrim(lcPrinters(lnIndex,1))) == upper(alltrim(lcPrinter))
					lcExistePrinter = .t.
				ENDIF
			ENDFOR
			IF !lcExistePrinter && se existir
				uf_perguntalt_chama("N�o � poss�vel configurar a impressora. Se a dificuldade persistir contacte o Suporte.","OK","",48)
				RETURN
			ENDIF
		ENDIF
	ENDIF 
		
	&& Define a Impressora	
	IF tcNum != 6
		Set Printer To Name ''+lcPrinter+''
	ENDIF
	Set Device To print
	Set Console off
	
	&& Cria Cursor de impressao e inicializa variaveis
	create cursor uCrsProdEtiq (ref c(60), design c(60), pvp n(9,2), iva n(6,2), forref c(20), oriref c(60), unidade c(40), conversao n(8,2))
	lcCount = 0
	
	IF Type("myTipoDoc") == "U" OR Type("myTipoDoc") != "C"
		myTipoDoc=""
	ENDIF 
	
	DO CASE
		&& Chamado em documentos do tipo Dossier Interno
		CASE pPainelEtiq == "DOC" AND myTipoDoc == "BO" 
			IF USED("bi")	
				SELECT bi
				GO TOP 
				SCAN FOR bi.qtt > 0
					TEXT TO lcSQL NOSHOW TEXTMERGE 
						SELECT  
							top 1 u_tipoetiq, epv1, tabiva, u_impetiq, forref, unidade, conversao 
						from  
							st (nolock) 
						where  
							st.ref = '<<Alltrim(bi.ref)>>'
							and st.site_nr = <<mysite_nr>>
					ENDTEXT 
					
				
					IF uf_gerais_actGrelha("","uCrsCheckRef",lcSQL)
						IF Reccount("uCrsCheckRef") > 0
							SELECT uCrsCheckRef
							
							IF tcNum < 5
								IF !(uCrsCheckRef.u_impetiq)
											
									DO CASE 
										CASE tcNum = 1 Or tcNum = 2			
											IF uCrsCheckRef.u_tipoetiq = 1
										
												FOR i=1 TO bi.qtt
													INSERT INTO uCrsProdEtiq (ref, design, pvp, iva, forref, oriref, unidade, conversao) Values ('*'+alltrim(bi.ref)+'*', alltrim(bi.design), uCrsCheckRef.epv1, uf_gerais_getTabelaIva(uCrsCheckRef.tabiva,"TAXA"), uCrsCheckRef.forref, ALLTRIM(bi.ref), ALLTRIM(uCrsCheckRef.unidade), uCrsCheckRef.conversao)
													lcCount = lcCount + 1
												ENDFOR 
											ENDIF 
										CASE  tcNum = 3 Or tcNum = 4
											
											IF uCrsCheckRef.u_tipoetiq = 2
												FOR i=1 TO bi.qtt
													INSERT INTO uCrsProdEtiq (ref, design, pvp, iva, forref, oriref, unidade, conversao) Values ('*'+alltrim(bi.ref)+'*', alltrim(bi.design), uCrsCheckRef.epv1, uf_gerais_getTabelaIva(uCrsCheckRef.tabiva,"TAXA"), uCrsCheckRef.forref, ALLTRIM(bi.ref), ALLTRIM(uCrsCheckRef.unidade), uCrsCheckRef.conversao)
													lcCount = lcCount + 1
												ENDFOR 
											ENDIF 
										OTHERWISE 
											****	
									ENDCASE
								ENDIF
							ELSE
								
								INSERT INTO uCrsProdEtiq (ref, design, pvp, iva, forref, oriref, unidade, conversao) Values ('*'+alltrim(bi.ref)+'*', alltrim(bi.design), uCrsCheckRef.epv1, uf_gerais_getTabelaIva(uCrsCheckRef.tabiva,"TAXA"), uCrsCheckRef.forref, ALLTRIM(bi.ref), ALLTRIM(uCrsCheckRef.unidade), uCrsCheckRef.conversao)
								lcCount = lcCount + 1
							ENDIF
						ENDIF
						Fecha("uCrsCheckRef")
					ENDIF
					SELECT bi
				ENDSCAN
				
	
				&& Fix para corrigir bug em que n�o eram impressas as etiquetas com 13 digitos em documentos com ref de 7 e 13 digitos
				SELECT LEN(ALLTRIM(oriref)) as ordemImp, * FROM uCrsProdEtiq INTO CURSOR uCrsProdEtiqAux READWRITE 
				
				IF Used("uCrsProdEtiq")
					Fecha("uCrsProdEtiq")
				ENDIF 
				
				SELECT * FROM uCrsProdEtiqAux ORDER BY ordemImp DESC INTO CURSOR uCrsProdEtiq READWRITE 

				&& Impress�o
				SELECT uCrsProdEtiq

				IF lcCount >= 1
					IF tcNum < 5
						IF tcNum == 1 Or tcNum == 3
							IF LEN(ALLTRIM(uCrsProdEtiq.oriref))-2 <= 8
								Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportNormalCPVP) To PRINTER 
							ELSE
								Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportAlarmeCPVP) To Printer
							ENDIF 
						ELSE
							IF LEN(ALLTRIM(uCrsProdEtiq.oriref))-2 <= 8
								Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportNormalSPVP) To printer
							ELSE
								Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportAlarmeSPVP) To printer
							ENDIF 
						ENDIF
					ELSE 
						IF tcNum == 7  && etiquetas brother prateleira
							Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportBrotherPVP) To Printer
						ELSE
							Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportPrateleiraPVP) To Printer
						ENDIF
					ENDIF
				ELSE
					uf_perguntalt_chama("N�O EXISTEM PRODUTOS PARA IMPRIMIR COM ESTE TIPO DE ETIQUETA OU OS PRODUTOS EST�O CONFIGURADOS PARA N�O IMPRIMIREM ETIQUETAS.","OK","",48)
				ENDIF
				
				IF USED("uCrsProdEtiq")
					Fecha("uCrsProdEtiq")
				ENDIF
				
				lcCount=0
			ENDIF	
		&& Chamado em documentos de Compras
		CASE pPainelEtiq == "DOC" AND myTipoDoc == "FO" 
			IF Used("FN")	
				SELECT fn
				GO TOP 
				SCAN FOR fn.qtt > 0
					TEXT TO lcSQL NOSHOW TEXTMERGE 
						select top 1 
							u_tipoetiq
							,epv1
							,tabiva
							,u_impetiq
							,forref
							,unidade
							,conversao
						from 
							st (nolock)
						where 
							st.ref = '<<Alltrim(fn.ref)>>'
							and st.site_nr=<<mysite_nr>>
					ENDTEXT 
					IF uf_gerais_actGrelha("","uCrsCheckRef",lcSQL)
						IF Reccount("uCrsCheckRef") > 0
							SELECT uCrsCheckRef
							IF tcNum < 5
								IF !(uCrsCheckRef.u_impetiq)
									DO CASE 
										CASE tcNum = 1 Or tcNum = 2
											IF uCrsCheckRef.u_tipoetiq = 1
												FOR i = 1 TO fn.qtt
													INSERT INTO uCrsProdEtiq (ref, design, pvp, iva, forref, oriref, unidade, conversao) Values ('*'+alltrim(fn.ref)+'*', alltrim(fn.design), uCrsCheckRef.epv1, uf_gerais_getTabelaIva(uCrsCheckRef.tabiva,"TAXA"), uCrsCheckRef.forref, ALLTRIM(fn.ref), ALLTRIM(uCrsCheckRef.unidade), uCrsCheckRef.conversao)
													lcCount = lcCount + 1
												ENDFOR 
											ENDIF 
										CASE tcNum = 3 Or tcNum = 4
											IF uCrsCheckRef.u_tipoetiq = 2
												FOR i = 1 TO fn.qtt
													INSERT INTO uCrsProdEtiq (ref, design, pvp, iva, forref, oriref, unidade, conversao) Values ('*'+alltrim(fn.ref)+'*', alltrim(fn.design), uCrsCheckRef.epv1, uf_gerais_getTabelaIva(uCrsCheckRef.tabiva,"TAXA"), uCrsCheckRef.forref, ALLTRIM(fn.ref), ALLTRIM(uCrsCheckRef.unidade), uCrsCheckRef.conversao)
													lcCount = lcCount + 1
												ENDFOR 
											ENDIF 
									ENDCASE 
								ENDIF
							ELSE 
								INSERT INTO uCrsProdEtiq (ref, design, pvp, iva, forref, oriref, unidade, conversao) Values ('*'+alltrim(fn.ref)+'*', alltrim(fn.design), uCrsCheckRef.epv1, uf_gerais_getTabelaIva(uCrsCheckRef.tabiva,"TAXA"), uCrsCheckRef.forref, ALLTRIM(fn.ref), ALLTRIM(uCrsCheckRef.unidade), uCrsCheckRef.conversao)
								lcCount = 1
							ENDIF
						ENDIF 
						Fecha("uCrsCheckRef")
					ENDIF 
					SELECT fn
				ENDSCAN 

				&& Fix para corrigir bug em que n�o eram impressas as etiquetas com 13 digitos em documentos com ref de 7 e 13 digitos
				SELECT  LEN(ALLTRIM(oriref)) as ordemImp, * FROM uCrsProdEtiq INTO CURSOR uCrsProdEtiqAux READWRITE 
				
				IF Used("uCrsProdEtiq")
					Fecha("uCrsProdEtiq")
				ENDIF 
				
				SELECT * FROM uCrsProdEtiqAux ORDER BY ordemImp DESC INTO CURSOR uCrsProdEtiq READWRITE 
				
				&& Impress�o
				SELECT uCrsProdEtiq
				
				IF lcCount >= 1
					IF tcNum < 5
						IF tcNum == 1 Or tcNum == 3 	
							IF LEN(ALLTRIM(uCrsProdEtiq.oriref))-2 <= 8
							
								REPORT FORM ALLTRIM(mypath)+'\analises\' + ALLTRIM(lcReportNormalCPVP) TO PRINTER 
							ELSE
								REPORT FORM ALLTRIM(mypath)+'\analises\' + ALLTRIM(lcReportAlarmeCPVP) TO PRINTER 
							ENDIF 									
						ELSE
							IF LEN(ALLTRIM(uCrsProdEtiq.oriref))-2 <= 8
								Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportNormalSPVP) To printer
							ELSE
								Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportAlarmeSPVP) To printer
							ENDIF 
						ENDIF
					ELSE
						IF tcNum = 7 && etiquetas brother prateleira
						Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportPrateleiraPVP) To Printer
						ELSE
							Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportPrateleiraPVP) To Printer
						ENDIF
					ENDIF
				ELSE 
					uf_perguntalt_chama("N�O EXISTEM PRODUTOS PARA IMPRIMIR COM ESTE TIPO DE ETIQUETA OU OS PRODUTOS EST�O CONFIGURADOS PARA N�O IMPRIMIREM ETIQUETAS.","OK","",48)
				ENDIF 
				
				IF Used("uCrsProdEtiq")
					Fecha("uCrsProdEtiq")
				ENDIF 
				
				IF Used("uCrsProdEtiqAux")
					Fecha("uCrsProdEtiqAux")
				ENDIF 
									
				lcCount=0
			ENDIF		
		
		&& Chamado da ST	
		CASE pPainelEtiq == "ST"
			IF Used("st")
				Select st
				FOR i=1 TO ETIQUETAS.spnNo.value
					Insert Into uCrsProdEtiq (ref, design, pvp, iva, forref, oriref, unidade, conversao) Values ('*'+alltrim(st.ref)+'*', alltrim(st.design), st.epv1, uf_gerais_getTabelaIva(st.tabiva,"TAXA"), alltrim(st.forref), ALLTRIM(st.ref), ALLTRIM(st.unidade), st.conversao)	
				ENDFOR 
				Select uCrsProdEtiq
				GO TOP
				IF tcNum < 5			
					If tcNum == 1 Or tcNum == 3
						IF LEN(ALLTRIM(uCrsProdEtiq.ref))-2 <= 8
							Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportNormalCPVP) To Printer
						ELSE
							Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportAlarmeCPVP) To Printer
						ENDIF 
					ELSE
						IF LEN(ALLTRIM(uCrsProdEtiq.ref))-2 <= 8
							Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportNormalSPVP) To printer
						ELSE
							Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportAlarmeSPVP) To printer
						ENDIF 
					ENDIF
				ELSE
					IF tcNum == 6 && impress�o etiquetas prateleira em A4		
						Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportPrateleiraPVPA4) To Printer
					ELSE
						IF tcNum = 7 && etiquetas brother prateleira
							Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportBrotherPVP) To Printer
						ELSE
							Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportPrateleiraPVP) To Printer
						ENDIF
					ENDIF
				ENDIF
				
				IF USED("uCrsProdEtiq")
					Fecha("uCrsProdEtiq")
				ENDIF
			ENDIF
			
		&& Chamado do Painel de Conf. PVP's - aplicado na v15.13.xx - Lu�s Leal
		CASE pPainelEtiq == "CONFPVP"
			IF USED("uCrsConfPvpAuxEti")
					SELECT uCrsConfPvpAuxEti
					GO TOP 
					SCAN For uCrsConfPvpAuxEti.stock > 0
						TEXT TO lcSQL TEXTMERGE NOSHOW   
							SELECT TOP 1 u_tipoetiq, epv1, tabiva, u_impetiq, forref, stock, unidade, conversao FROM st (nolock) WHERE st.ref = '<<ALLTRIM(uCrsConfPvpAuxEti.ref)>>' AND site_nr = <<mysite_nr>>
						ENDTEXT 			
						IF uf_gerais_actGrelha("","uCrsCheckRef",lcSQL)
							SELECT uCrsCheckRef
							IF Reccount("uCrsCheckRef") > 0
								IF tcNum < 5
									Select uCrsCheckRef
									IF !(uCrsCheckRef.u_impetiq)
										DO  CASE 
											CASE tcNum = 1 Or tcNum = 2
												IF uCrsCheckRef.u_tipoetiq = 1
													FOR i=1 To uCrsConfPvpAuxEti.stock
														Insert Into uCrsProdEtiq (ref, design, pvp, iva, forref, oriref, unidade, conversao) Values ('*'+alltrim(uCrsConfPvpAuxEti.ref)+'*', alltrim(uCrsConfPvpAuxEti.design), uCrsCheckRef.epv1, uf_gerais_getTabelaIva(uCrsCheckRef.tabiva,"TAXA"), uCrsCheckRef.forref, ALLTRIM(uCrsConfPvpAuxEti.ref), ALLTRIM(uCrsCheckRef.unidade), uCrsCheckRef.conversao)
														lcCount = lcCount + 1
													ENDFOR 
												ENDIF 
											CASE  tcNum=3 Or tcNum=4					
												IF  uCrsCheckRef.u_tipoetiq=2
													FOR  i=1 TO  uCrsConfPvpAuxEti.stock
														INSERT INTO uCrsProdEtiq (ref, design, pvp, iva, forref, oriref, unidade, conversao) Values ('*'+alltrim(uCrsConfPvpAuxEti.ref)+'*', alltrim(uCrsConfPvpAuxEti.design), uCrsCheckRef.epv1, uf_gerais_getTabelaIva(uCrsCheckRef.tabiva,"TAXA"), uCrsCheckRef.forref, ALLTRIM(uCrsConfPvpAuxEti.ref), ALLTRIM(uCrsCheckRef.unidade), uCrsCheckRef.conversao)
														lcCount = lcCount + 1
													ENDFOR 
												ENDIF 
										ENDCASE 
									ENDIF
								ELSE
									INSERT  Into uCrsProdEtiq (ref, design, pvp, iva, forref, oriref, unidade, conversao) Values ('*'+alltrim(uCrsConfPvpAuxEti.ref)+'*', alltrim(uCrsConfPvpAuxEti.design), uCrsCheckRef.epv1, uf_gerais_getTabelaIva(uCrsCheckRef.tabiva,"TAXA"), uCrsCheckRef.forref, ALLTRIM(uCrsConfPvpAuxEti.ref), ALLTRIM(uCrsCheckRef.unidade), uCrsCheckRef.conversao)
									lcCount = 1
								ENDIF
							ENDIF 
							Fecha("uCrsCheckRef")
						ENDIF 
						SELECT  uCrsConfPvpAuxEti
					ENDSCAN 
							
					SELECT uCrsProdEtiq
					If lcCount >= 1
						IF tcNum < 5	
							IF tcNum == 1 Or tcNum == 3
								IF LEN(ALLTRIM(uCrsProdEtiq.ref))-2 <= 8
									Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportNormalCPVP) To Printer
								ELSE
									Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportAlarmeCPVP) To Printer
								ENDIF 
							ELSE
								IF LEN(ALLTRIM(uCrsProdEtiq.ref))-2 <= 8
									Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportNormalSPVP) To printer
								ELSE
									Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportAlarmeSPVP) To printer
								ENDIF 
							ENDIF 
						ELSE
							IF tcNum = 7 && etiquetas brother prateleira
							Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportBrotherPVP) To Printer
						ELSE
							Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportPrateleiraPVP) To Printer
						ENDIF
						
						ENDIF
					ELSE 
						uf_perguntalt_chama("N�O EXISTEM PRODUTOS PARA IMPRIMIR COM ESTE TIPO DE ETIQUETA OU OS PRODUTOS EST�O CONFIGURADOS PARA N�O IMPRIMIREM ETIQUETAS.","OK","",48)
					ENDIF 
					
					IF Used("uCrsProdEtiq")
						Fecha("uCrsProdEtiq")
					ENDIF 
					lcCount=0
			ENDIF
			
		&& Chamado do Painel Central para os cen�rios em que existe altera�ao de Pre�o quando corre a altera��o de pre�o autom�tica - Aplicado na v15.13.xx - Lu�s Leal
		CASE pPainelEtiq == "PCENTRAL"
			IF USED("uCrsImpEtiquetas")
					SELECT uCrsImpEtiquetas
					GO TOP 
					SCAN For uCrsImpEtiquetas.stock > 0
						TEXT TO lcSQL TEXTMERGE NOSHOW   
							SELECT TOP 1 u_tipoetiq, epv1, tabiva, u_impetiq, forref, stock, unidade, conversao FROM st (nolock) WHERE st.ref = '<<ALLTRIM(uCrsImpEtiquetas.ref)>>' AND site_nr = <<mysite_nr>>
						ENDTEXT 			
						IF uf_gerais_actGrelha("","uCrsCheckRef",lcSQL)
							IF Reccount("uCrsCheckRef") > 0
								IF tcNum < 5
									Select uCrsCheckRef
									IF !(uCrsCheckRef.u_impetiq)
										DO CASE 
											CASE tcNum = 1 Or tcNum = 2
												IF uCrsCheckRef.u_tipoetiq = 1
													FOR i=1 TO uCrsImpEtiquetas.stock
														INSERT INTO uCrsProdEtiq (ref, design, pvp, iva, forref, oriref, unidade, conversao) Values ('*'+alltrim(uCrsImpEtiquetas.ref)+'*', alltrim(uCrsImpEtiquetas.design), uCrsCheckRef.epv1, uf_gerais_getTabelaIva(uCrsCheckRef.tabiva,"TAXA"), uCrsCheckRef.forref, ALLTRIM(uCrsImpEtiquetas.ref), ALLTRIM(uCrsCheckRef.unidade), uCrsCheckRef.conversao)
														lcCount = lcCount + 1
													ENDFOR 
												ENDIF 
											CASE tcNum=3 Or tcNum=4
												IF uCrsCheckRef.u_tipoetiq=2
													FOR i=1 TO uCrsImpEtiquetas.stock
														INSERT INTO uCrsProdEtiq (ref, design, pvp, iva, forref, oriref, unidade, conversao) Values ('*'+alltrim(uCrsImpEtiquetas.ref)+'*', alltrim(uCrsImpEtiquetas.design), uCrsCheckRef.epv1, uf_gerais_getTabelaIva(uCrsCheckRef.tabiva,"TAXA"), uCrsCheckRef.forref, ALLTRIM(uCrsImpEtiquetas.ref), ALLTRIM(uCrsCheckRef.unidade), uCrsCheckRef.conversao)
														lcCount = lcCount + 1
													ENDFOR 
												ENDIF 
										ENDCASE 
									ENDIF
								ELSE 
									INSERT INTO uCrsProdEtiq (ref, design, pvp, iva, forref, oriref, unidade, conversao) Values ('*'+alltrim(uCrsImpEtiquetas.ref)+'*', alltrim(uCrsImpEtiquetas.design), uCrsCheckRef.epv1, uf_gerais_getTabelaIva(uCrsCheckRef.tabiva,"TAXA"), uCrsCheckRef.forref, ALLTRIM(uCrsImpEtiquetas.ref), ALLTRIM(uCrsCheckRef.unidade), uCrsCheckRef.conversao)
									lcCount = 1
								ENDIF
							ENDIF 
							Fecha("uCrsCheckRef")
						ENDIF 
						SELECT uCrsImpEtiquetas
					ENDSCAN 
							
					SELECT uCrsProdEtiq
					IF lcCount >= 1
						IF tcNum < 5							
							IF tcNum == 1 Or tcNum == 3
								IF LEN(ALLTRIM(uCrsProdEtiq.ref))-2 <= 8
									Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportNormalCPVP) To Printer
								ELSE
									Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportAlarmeCPVP) To Printer
								ENDIF 
							ELSE
								IF LEN(ALLTRIM(uCrsProdEtiq.ref))-2 <= 8
									Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportNormalSPVP) To printer
								ELSE
									Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportAlarmeSPVP) To printer
								ENDIF 
							ENDIF 
						ELSE 
							IF tcNum = 7 && etiquetas brother prateleira
								Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportBrotherPVP) To Printer
							ELSE
								Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportPrateleiraPVP) To Printer
							ENDIF
						ENDIF 
					ELSE 
						uf_perguntalt_chama("N�O EXISTEM PRODUTOS PARA IMPRIMIR COM ESTE TIPO DE ETIQUETA OU OS PRODUTOS EST�O CONFIGURADOS PARA N�O IMPRIMIREM ETIQUETAS.","OK","",48)
					ENDIF  
					
					IF Used ("uCrsProdEtiq")
						Fecha("uCrsProdEtiq")
					ENDIF 
					lcCount=0
			ENDIF
		OTHERWISE
			***		
	ENDCASE
	
	&& Define a impressora no default
	Set Device To screen
	Set printer to Name ''+lcOldPrinter+''
	Set Console On
ENDFUNC 


**
FUNCTION uf_etiquetas_Posologia

	Local lcCount, lcPrinter,lcOldPrinter, lcnome
	Store 0 To lcCount
	lcOldPrinter= set("printer",2)
	
	lcnome=ALLTRIM(ft.nome)

	&& Verifica se os reports de etiquetas existem
	If !File(Alltrim(mypath)+'\analises\label_posologia.frx') 
		uf_perguntalt_chama("OS REPORTS DE ETIQUETAS N�O EST�O A SER DETECTADOS! POR FAVOR CONTACTE O SUPORTE.","OK","",48)
		RETURN .f.
	ENDIF
	
	&& Calcula Nome da impressora
	lcPrinter = uf_etiquetas_calculaLabelPrinter("POSOLOGIA")

	&& Verificar se a impressora existe
	IF empty(lcPrinter)
		uf_perguntalt_chama("A IMPRESSORA N�O EST� A SER DETECTADA.","OK","",48)
		RETURN .f.
	ELSE
		DIMENSION lcPrinters[1,1]
		APRINTERS(lcPrinters)
		LOCAL lcExistePrinter
		FOR lnIndex = 1 TO ALEN(lcPrinters,1)
			IF upper(alltrim(lcPrinters(lnIndex,1))) == upper(alltrim(lcPrinter))
				lcExistePrinter = .t.
			ENDIF
		ENDFOR
		IF !lcExistePrinter && se existir
			uf_perguntalt_chama("A IMPRESSORA N�O EST� A SER DETECTADA.","OK","",48)
			RETURN
		ENDIF
	ENDIF 
	
	Set Device To print
	Set Console off
	
	&& Cria Cursor de impressao e inicializa variaveis
		IF USED("uCrsProdEtiq")
		fecha("uCrsProdEtiq")
	ENDIF
	
    WAIT WINDOW '' TIMEOUT 0.1 
	create cursor uCrsProdEtiq (texto c(254), design c(60), nome c(55))

	SELECT LEFT(uCrsAtendST.u_posprog,254) as posologia, fi.design from fi   WITH (BUFFERING = .T.) INNER JOIN uCrsAtendST   WITH (BUFFERING = .T.) ON fi.fistamp = uCrsAtendST.ststamp  into cursor uCrsfiatemp readwrite 

	SELECT uCrsfiatemp
	GO TOP

	SCAN
		IF !EMPTY(uCrsfiatemp.posologia) 
			Select uCrsProdEtiq
			APPEND BLANK
			Replace uCrsProdEtiq.texto WITH "-" + LEFT(ALLTRIM(uCrsfiatemp.posologia),254)
			Replace uCrsProdEtiq.design WITH ALLTRIM(uCrsfiatemp.design)
			Replace uCrsProdEtiq.nome WITH ALLTRIM(lcnome)
		ENDIF 
		SELECT uCrsfiatemp
	ENDSCAN 
    
    Fecha("uCrsfiatemp")

    SET REPORTBEHAVIOR 90

	Select uCrsProdEtiq
	GO TOP 
	SCAN 
		Report Form Alltrim(mypath)+'\analises\label_posologia.frx' To Printer
		Select uCrsProdEtiq
	ENDSCAN 

    SET REPORTBEHAVIOR 80

	IF Used("uCrsProdEtiq")
		Fecha("uCrsProdEtiq")
	ENDIF 
		
	&& Define a impressora no default
	Set Device To screen
	Set printer to Name ''+lcOldPrinter+''
	Set Console On
ENDFUNC 


FUNCTION uf_etiquetas_Posologia_L

	Local lcCount, lcPrinter,lcOldPrinter, lcnome
	Store 0 To lcCount
	lcOldPrinter= set("printer",2)
	
	lcnome=ALLTRIM(ft.nome)

	&& Verifica se os reports de etiquetas existem
	If !File(Alltrim(mypath)+'\analises\label_posologia_L.frx') 
		uf_perguntalt_chama("OS REPORTS DE ETIQUETAS N�O EST�O A SER DETECTADOS! POR FAVOR CONTACTE O SUPORTE.","OK","",48)
		RETURN .f.
	ENDIF
	
	&& Calcula Nome da impressora
	lcPrinter = uf_etiquetas_calculaLabelPrinter("POSOLOGIA")

	&& Verificar se a impressora existe
	IF empty(lcPrinter)
		uf_perguntalt_chama("A IMPRESSORA N�O EST� A SER DETECTADA.","OK","",48)
		RETURN .f.
	ELSE
		DIMENSION lcPrinters[1,1]
		APRINTERS(lcPrinters)
		LOCAL lcExistePrinter
		FOR lnIndex = 1 TO ALEN(lcPrinters,1)
			IF upper(alltrim(lcPrinters(lnIndex,1))) == upper(alltrim(lcPrinter))
				lcExistePrinter = .t.
			ENDIF
		ENDFOR
		IF !lcExistePrinter && se existir
			uf_perguntalt_chama("A IMPRESSORA N�O EST� A SER DETECTADA.","OK","",48)
			RETURN
		ENDIF
	ENDIF 
	
	Set Device To print
	Set Console off
	
	&& Cria Cursor de impressao e inicializa variaveis
	IF USED("uCrsProdEtiq")
		fecha("uCrsProdEtiq")
	ENDIF
	&& uc_printPos
    WAIT WINDOW '' TIMEOUT 0.1 
	create cursor uCrsProdEtiq (texto c(254), design c(60), nome c(55))

	SELECT LEFT(uCrsAtendST.u_posprog,254) as posologia, fi.design from fi   WITH (BUFFERING = .T.) INNER JOIN uCrsAtendST   WITH (BUFFERING = .T.) ON fi.fistamp = uCrsAtendST.ststamp  into cursor uCrsfiatemp readwrite 

	SELECT uCrsfiatemp
	GO TOP

	SCAN
		IF !EMPTY(uCrsfiatemp.posologia) 
			Select uCrsProdEtiq
			APPEND BLANK
			Replace uCrsProdEtiq.texto WITH "-" + LEFT(ALLTRIM(uCrsfiatemp.posologia),254)
			Replace uCrsProdEtiq.design WITH ALLTRIM(uCrsfiatemp.design)
			Replace uCrsProdEtiq.nome WITH ALLTRIM(lcnome)
		ENDIF 
		SELECT uCrsfiatemp
	ENDSCAN 
    
    Fecha("uCrsfiatemp")

    SET REPORTBEHAVIOR 90

	Select uCrsProdEtiq
	GO TOP 
	SCAN 
		Report Form Alltrim(mypath)+'\analises\label_posologia_L.frx' To Printer
		Select uCrsProdEtiq
	ENDSCAN 

    SET REPORTBEHAVIOR 80

	IF Used("uCrsProdEtiq")
		Fecha("uCrsProdEtiq")
	ENDIF 
		
	&& Define a impressora no default
	Set Device To screen
	Set printer to Name ''+lcOldPrinter+''
	Set Console On
ENDFUNC 

FUNCTION uf_etiquetas_Posologia_Linha

	Local lcCount, lcPrinter,lcOldPrinter, lcnome
	Store 0 To lcCount
	lcOldPrinter= set("printer",2)
	
	lcnome=ALLTRIM(ft.nome)

	&& Verifica se os reports de etiquetas existem
	If !File(Alltrim(mypath)+'\analises\label_posologia_L.frx') 
		uf_perguntalt_chama("OS REPORTS DE ETIQUETAS N�O EST�O A SER DETECTADOS! POR FAVOR CONTACTE O SUPORTE.","OK","",48)
		RETURN .f.
	ENDIF

    IF !USED("uc_printPos")
		uf_perguntalt_chama("OCORREU UM ERRO AO IMPRIMIR OS DADOS! POR FAVOR CONTACTE O SUPORTE.","OK","",48)
		RETURN .f.
    ENDIF
	
	&& Calcula Nome da impressora
	lcPrinter = uf_etiquetas_calculaLabelPrinter("POSOLOGIA")

	&& Verificar se a impressora existe
	IF empty(lcPrinter)
		uf_perguntalt_chama("A IMPRESSORA N�O EST� A SER DETECTADA.","OK","",48)
		RETURN .f.
	ELSE
		DIMENSION lcPrinters[1,1]
		APRINTERS(lcPrinters)
		LOCAL lcExistePrinter
		FOR lnIndex = 1 TO ALEN(lcPrinters,1)
			IF upper(alltrim(lcPrinters(lnIndex,1))) == upper(alltrim(lcPrinter))
				lcExistePrinter = .t.
			ENDIF
		ENDFOR
		IF !lcExistePrinter && se existir
			uf_perguntalt_chama("A IMPRESSORA N�O EST� A SER DETECTADA.","OK","",48)
			RETURN
		ENDIF
	ENDIF 
	
	Set Device To print
	Set Console off
		

	SELECT ucrse1
	SELECT ft

	Select uc_printPos
	GO TOP 

    SELECT * FROM uc_printPos INTO CURSOR uCrsProdEtiq    

	Report Form Alltrim(mypath)+'\analises\label_posologia_L.frx' To Printer NEXT 1

    IF Used("uCrsProdEtiq")
		Fecha("uCrsProdEtiq")
	ENDIF 
			
	&& Define a impressora no default
	Set Device To screen
	Set printer to Name ''+lcOldPrinter+''
	Set Console On

    Fecha("uc_printPos")

ENDFUNC 


**uf_etiquetas_calculaLabelPrinter('POSOLOGIA')
FUNCTION uf_etiquetas_calculaLabelPrinter
	LPARAMETERS lcTipoImpressora
		
	LOCAL lclabelPrinter
	
	DO CASE
		CASE lcTipoImpressora == 'PRODUTO'
			Store 'Etiquetas' To lcLabelPrinter
		CASE lcTipoImpressora == 'POSOLOGIA'
			Store 'EtiquetasPosologia' To lcLabelPrinter
		CASE lcTipoImpressora == 'PRATELEIRA'
			Store 'EtiquetasPrateleira' To lcLabelPrinter
		OTHERWISE
			****
	ENDCASE
	
	If uf_gerais_getparameter('ADM0000000135', 'BOOL')
		** Verificar se a Impressora � TS ou se tem impressora predifinida **
		If uf_gerais_actGrelha("","uCrsTermTs",[select top 1 ts, labelprinter from B_terminal (nolock) where no=] + ALLTRIM(str(myTermNo)) + [ order by udata desc])
			IF RECCOUNT() > 0
				IF !EMPTY(ALLTRIM(uCrsTermTs.labelprinter)) AND LEN(ALLTRIM(uCrsTermTs.labelprinter))>2
					lcLabelPrinter = ALLTRIM(uCrsTermTs.labelprinter)	
				ELSE
					IF uCrsTermTs.ts==.t.
						** verificar vers�o do windows **
						LOCAL lcWindowsVersion, lcWindowsLocal
						STORE 'EN' TO lcWindowsLocal
						lcWindowsVersion = GETENV("homepath")
						
						lcWindowsLocal = ALLTRIM(uf_gerais_getparameter('ADM0000000085', 'TEXT'))
						***********************************
						
						** verificar session id **
						LOCAL b
						STORE '' TO b
						b = uf_gerais_getRdpIdFromStr()
						
						LOCAL idRemote
						idRemote = VAL(ALLTRIM(b))
						
						
						LOCAL uv_curPrinter

						uv_curPrinter = ALLTRIM(lcLabelPrinter)

						IF !(lcWindowsLocal=='PT')
							IF UPPER(LEFT(ALLTRIM(lcWindowsVersion),2))=="\U" OR ( UPPER(LEFT(ALLTRIM(lcWindowsVersion),1))=="\" AND !(UPPER(LEFT(ALLTRIM(lcWindowsVersion),2))=="\D") )
						
								lcLabelPrinter = ALLTRIM(uv_curPrinter) + " #" + ALLTRIM(b)

								IF !uf_gerais_showActivePrinters(lcLabelPrinter)
									b=ALLTRIM(STR(idRemote))
									lcLabelPrinter = ALLTRIM(uv_curPrinter) + ' (Redirected ' + ALLTRIM(b) + ')'
								ENDIF
							ELSE
							
								lcLabelPrinter = ALLTRIM(uv_curPrinter) + " #" + ALLTRIM(b)

								IF !uf_gerais_showActivePrinters(lcLabelPrinter)
									b=ALLTRIM(STR(idRemote))
									lcLabelPrinter = lcLabelPrinter + ' (from ' + UPPER(ALLTRIM(getenv("clientname"))) + ') in session ' + ALLTRIM(b)
								ENDIF
							ENDIF
						ELSE
							IF UPPER(LEFT(ALLTRIM(lcWindowsVersion),2))=="\U" OR ( UPPER(LEFT(ALLTRIM(lcWindowsVersion),1))=="\" AND !(UPPER(LEFT(ALLTRIM(lcWindowsVersion),2))=="\D") )
							
								lcLabelPrinter = ALLTRIM(uv_curPrinter) + " #" + ALLTRIM(b)

								IF !uf_gerais_showActivePrinters(lcLabelPrinter)
									b=ALLTRIM(STR(idRemote))
									lcLabelPrinter = lcLabelPrinter + ' (redireccionado ' + ALLTRIM(b) + ')'
								ENDIF
							ELSE
								
								lcLabelPrinter = ALLTRIM(uv_curPrinter) + " #" + ALLTRIM(b)

								IF !uf_gerais_showActivePrinters(lcLabelPrinter)
									b=ALLTRIM(STR(idRemote))
									lcLabelPrinter = lcLabelPrinter + ' (de ' + UPPER(ALLTRIM(getenv("clientname"))) + ') in session ' + ALLTRIM(b)
								ENDIF
							ENDIF
						Endif
						*******
					ENDIF			
				ENDIF
			ENDIF
			fecha("uCrsTermTs")
		ENDIF
	ENDIF
	
	RETURN lcLabelPrinter
ENDFUNC 


**
FUNCTION uf_etiquetas_sair
	RELEASE pPainelEtiq
	
	ETIQUETAS.hide
	ETIQUETAS.release
ENDFUNC


FUNCTION uf_etiquetas_posologia_predef
	local lcRedirected, lcCmd
	store '' to lcRedirected, lcCmd
	If Aprinters(gaprinters) > 0
		For i=1 To Aprinters(gaprinters)
			if upper(left(gaprinters(i,1),18))=='ETIQUETASPOSOLOGIA'
				lcRedirected = alltrim(substr(gaprinters(i,1), 20,20))
			endif

		Endfor
	Else
		Messagebox( 'No printers found.',16)
	Endif

	lcCmd = 'RUNDLL32 PRINTUI.DLL,PrintUIEntry /y /n "etiquetasposologia '+alltrim(lcRedirected)+'"'

	STRTOFILE(lcCmd , "C:\Logitools\temp\bat\YourBATfile"+alltrim(ucrse1.id_lt)+".BAT")
	
	DECLARE INTEGER ShellExecute IN shell32.dll ; 
				  	INTEGER hndWin, ; 
				  	STRING cAction, ; 
				  	STRING cFileName, ; 
				  	STRING cParams, ;  
				  	STRING cDir, ; 
					INTEGER nShowWin
	ShellExecute(0,"open","C:\Logitools\temp\bat\YourBATfile"+alltrim(ucrse1.id_lt)+".BAT" ,"","",1)
	ShellExecute(0,"open","C:\Logitools\delbat.BAT" ,"","",1)
ENDFUNC 