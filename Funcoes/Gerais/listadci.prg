FUNCTION uf_ListaDCI_chama
	LPARAMETERS lcRef
	LOCAL lcSQL
	
	&& Valida Parametro
	IF EMPTY("lcRef") OR !(TYPE("lcRef")=="C")
		uf_perguntalt_chama("PAR�METRO REFER�NCIA INV�LIDO.","OK","",48)
		RETURN .F.
	ENDIF
	
	&& Cria Cursor
	TEXT TO lcSQL TEXTMERGE noshow
		exec up_touch_todasSubst '<<Alltrim(lcRef)>>'
	ENDTEXT	
	uf_gerais_actGrelha("","uCrsListaDCI",lcSQL)
		
	&& Controla abertura do painel
	IF TYPE("ListaDci") == "U"	
		DO FORM ListaDci
	ELSE
		ListaDci.show()
	ENDIF

ENDFUNC

***********************
**  Fecho do painel  **
***********************
FUNCTION uf_ListaDCI_sair
	&& fecha Painel
	ListaDCI.release
	
	&& Liberta variaveis publicas
	RELEASE ListaDCI
	
	&& Fecha Cursores
	fecha("uCrsListaDCI")
ENDFUNC
