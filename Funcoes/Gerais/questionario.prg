DEFINE CLASS myTextBoxText as TextBox
	PROCEDURE LostFocus
			SELECT uCrsQuestionario
			Replace uCrsQuestionario.resposta 	WITH Questionario.quest.myTxtBox.Value
			Replace uCrsQuestionario.sel 		WITH .T.
			
			SELECT uCrsQuestionarioTemp
			Replace uCrsQuestionarioTemp.resposta 	WITH Questionario.quest.myTxtBox.Value
			Replace uCrsQuestionarioTemp.sel 		WITH .T.
ENDDEFINE 


DEFINE CLASS myTxtBoxClass AS TextBox
	PROCEDURE Click 
   		uf_getdate_chama(.f., "lcFData", 0, .f., .f., .t.)
   		Questionario.quest.mytxtBox.Value = lcFData
ENDDEFINE




FUNCTION uf_questionario_chama
	LPARAMETERS lcQuestStamp, lcRef, lcNrAtendimento
	
	PUBLIC lcRefQuest, lcPerguntaNr, lcGrpRespostas, lcSelResp, myRetornoPergunta, myRetornoCancelar, lcNrAtend
	
	myRetornoPergunta = .f.
	myRetornoCancelar = .f.
	lcRefQuest = lcRef
	lcNrAtend = lcNrAtendimento
	lcPerguntaNr = 1
	lcGrpRespostas = uf_gerais_stamp()
	lcSelResp  = ''
	
	uf_questionario_carregaQuest(lcQuestStamp)
	
	&& Controla Abertura do Painel
	if type("QUESTIONARIO")=="U"
		DO FORM QUESTIONARIO WITH lcQuestStamp, lcRef
	ELSE
		QUESTIONARIO.show
	ENDIF
	
	RETURN myRetornoPergunta
ENDFUNC


FUNCTION uf_questionario_carregaQuest
	LPARAMETERS lcQuestSatmp
	TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_questionario_getQuest '<<lcQuestSatmp>>'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "uCrsQuestionario", lcsql)
       uf_perguntalt_chama("Ocorreu uma anomalia ao criar o question�rio. Por Favor contacte o Suporte.", "OK", "", 16)
       RETURN .F.
    ENDIF

	SELECT uCrsQuestionario
	GOTO TOP 
	SELECT  * FROM uCrsQuestionario INTO CURSOR uCrsQuestionarioTemp READWRITE
	
	SELECT uCrsQuestionarioTemp
	GOTO TOP 
	SET FILTER TO uCrsQuestionarioTemp.ordem = lcPerguntaNr

ENDFUNC 




FUNCTION uf_questionario_controlaEstadoMenus 
	
	LOCAL lcMaxPergunta, lcMinPergunta
	
	SELECT MAX(ordem) as maxPerg, MIN(ordem) as minPerg FROM uCrsQuestionario INTO CURSOR uCrsTemp READWRITE 
	
	DO CASE
	CASE maxPerg = lcPerguntaNr 
		questionario.menu1.anterior.visible = .T.
		questionario.menu1.proximo.visible 	= .F.
		questionario.menu1.gravar.Visible 	= .T.
		
	CASE minPerg = lcPerguntaNr 
		questionario.menu1.anterior.visible = .F.
		questionario.menu1.proximo.visible 	= .T.
		questionario.menu1.gravar.Visible 	= .F.
		
	OTHERWISE
		questionario.menu1.anterior.visible = .T.
		questionario.menu1.proximo.visible 	= .T.
		questionario.menu1.gravar.Visible 	= .F.
		
	ENDCASE
	
	Questionario.refresh()
	
	fecha("uCrsTemp")
ENDFUNC 


FUNCTION uf_questionario_limpaForm
		IF type("Questionario.quest.opgOptionGroup1") == "O"

			Questionario.quest.removeobject("opgOptionGroup1")
		ENDIF
		
		IF type("Questionario.quest.opgOptionGroup2") == "O"
			Questionario.quest.removeobject("opgOptionGroup2")
		ENDIF
		
		IF type("Questionario.quest.mytxtbox") == "O"
			Questionario.quest.removeobject("mytxtbox")
		ENDIF
ENDFUNC 




FUNCTION uf_questionario_geraEcra
	
	uf_questionario_limpaForm()

	SELECT uCrsQuestionarioTemp
	GOTO TOP 
	SET FILTER TO 1=1
	SELECT uCrsQuestionarioTemp
	GOTO TOP 
	SET FILTER TO uCrsQuestionarioTemp.ordem = lcPerguntaNr
	*SELECT  * FROM uCrsQuestionario WHERE uCrsQuestionario.ordem = lcPerguntaNr INTO CURSOR uCrsQuestionarioTemp READWRITE

	
	SELECT uCrsQuestionarioTemp
	GOTO TOP
	SCAN FOR uCrsQuestionarioTemp.sel = .T.
		lcSelResp = uCrsQuestionarioTemp.Quest_pergunta_respStamp
	ENDSCAN 
	SELECT uCrsQuestionarioTemp
	GOTO TOP
	
	Questionario.quest.txtPergunta.Caption = uCrsQuestionarioTemp.Pergunta
	
	
	IF uCrsQuestionarioTemp.id = 1
	
		Questionario.Quest.GRIDPESQMULTI.Visible = .T.
		Questionario.Quest.valor.visible = .T.
		IF uCrsQuestionarioTemp.permitemulti
			Questionario.multiselecao = .T.
		ELSE 
			Questionario.multiselecao = .F.
		ENDIF
	ELSE
		
		Questionario.Quest.GRIDPESQMULTI.Visible = .F.
		Questionario.Quest.valor.visible = .F.
		
		WITH Questionario.quest
			
			IF 	uCrsQuestionarioTemp.ID = 2			
				lcSelResp = uCrsQuestionarioTemp.Quest_pergunta_respStamp
				
				.AddObject('myTxtBox','myTextBoxText')
				.myTxtBox.top = 100
				.myTxtBox.left = 20
				.myTxtBox.width = .Width - 50
				.myTxtBox.Visible =  .T.
				.myTxtBox.MAxLength = 100
				.myTxtBox.Enabled = .T.
				.myTxtBox.Tag =  uCrsQuestionarioTemp.Quest_pergunta_respStamp
				.myTxtBox.Value =  uCrsQuestionarioTemp.resposta
				SELECT uCrsQuestionario
				GOTO TOP 
				LOCATE FOR uCrsQuestionario.quest_perguntastamp = uCrsQuestionarioTemp.quest_perguntaStamp
			ENDIF
			
			
			IF 	uCrsQuestionarioTemp.ID = 3
				
				lcSelResp = uCrsQuestionarioTemp.Quest_pergunta_respStamp

				.AddObject('myTxtBox','TextBox')
				.myTxtBox.top = 100
				.myTxtBox.left = 20
				.myTxtBox.width = .width -50
				.myTxtBox.Visible =  .T.
				.myTxtBox.Enabled = .T.
				.myTxtBox.InputMask = '999999'
				.myTxtBox.Tag =  uCrsQuestionarioTemp.Quest_pergunta_respStamp
				.myTxtBox.Value =  uCrsQuestionarioTemp.resposta
				
			ENDIF
			
			
			
			
			IF 	uCrsQuestionarioTemp.ID = 4
				
				lcSelResp = uCrsQuestionarioTemp.Quest_pergunta_respStamp	

				.AddObject('myTxtBox','myTxtBoxClass')
				.myTxtBox.top = 100
				.myTxtBox.left = 20
				.myTxtBox.width = .width - 50 
				.myTxtBox.Visible =  .T.
				.myTxtBox.ReadOnly  = .T.
				.myTxtBox.Tag =  uCrsQuestionarioTemp.Quest_pergunta_respStamp
				.myTxtBox.Value =  uCrsQuestionarioTemp.resposta

			ENDIF

		ENDWITH
		
	ENDIF
ENDFUNC






FUNCTION uf_questionario_Proximo
	LOCAL lcMultipla
	
	lcMultipla = .F.
	questionario.quest.valor.value = ''
	
	SELECT uCrsQuestionarioTemp
	GOTO TOP 
	lcMultipla = RECcount("uCrsQuestionarioTemp") > 1
	SELECT uCrsQuestionarioTemp
	
	SELECT uCrsQuestionario
	GOTO TOP 
	LOCATE FOR uCrsQuestionario.Quest_pergunta_respStamp = lcSelResp
	DO CASE
		CASE !uCrsQuestionarioTemp.sel  and  uCrsQuestionarioTemp.Obrigatorio AND lcMultipla
			uf_perguntalt_chama("Necessita Responder.", "OK", "", 16)
			RETURN .F.
		
		CASE !uCrsQuestionarioTemp.sel  and  !uCrsQuestionarioTemp.Obrigatorio AND lcMultipla
			lcPerguntaNr = uCrsQuestionario.Prox_Ordem 
	   		lcSelResp = ''
	   		
		CASE uCrsQuestionario.Prox_ordem > 0 AND !lcMultipla
			IF EMPTY(Questionario.quest.mytxtbox.Value)
				uf_perguntalt_chama("Necessita Responder.", "OK", "", 16)
				RETURN .F.		
			ELSE
				SELECT 	uCRsQuestionario
				Replace uCrsQuestionario.resposta	WITH ALLTRIM(Questionario.quest.mytxtbox.Value)
				Replace uCrsQuestionario.sel		WITH .T.
			ENDIF
			lcPerguntaNr = uCrsQuestionario.Prox_Ordem 
	   		lcSelResp = ''

		OTHERWISE		
			IF EMPTY(lcSelResp)
				uf_perguntalt_chama(" Necessita selecionar uma resposta.", "OK", "", 16)
		       	RETURN .F.
			ENDIF 
			SELECT uCrsQuestionario
			IF uCrsQuestionario.ID <> 1
			
					lcRespostaString = alines(uc_RespostaString,STRTRAN( uCrsQuestionarioTemp.resposta,"**",chr(13)))
					
					IF lcRespostaString = 1 AND uf_gerais_contemStr(uCrsQuestionario.resposta, "Outro")
						uf_inseretexto_chama("Outro",  "Gravar")
		
						SELECT uCrsQuestionario
						Replace uCrsQuestionario.resposta  	WITH  "Outro: " + ALLTRIM(myRetornoPergunta)
						REplace uCrsQuestionario.sel  		WITH .T.  

						
						SELECT uCrsQuestionarioTemp
						LOCATE FOR uCrsQuestionarioTemp.Quest_pergunta_respStamp = lcSelResp
						Replace uCrsQuestionarioTemp.resposta  	WITH  "Outro: " + ALLTRIM(myRetornoPergunta)
										
						myRetornoPergunta = .T.
					ELSE
		
	*					SELECT uCrsQuestionario
	*					Replace uCrsQuestionario.resposta  	WITH  ALLTRIM(uc_RespostaString[1])+"**"+Questionario.quest.mytxtbox.Value
	*					REplace uCrsQuestionario.sel  		WITH .T.  

					ENDIF
					
			ELSE
					Replace uCrsQuestionario.sel  		WITH .T.  
			ENDIF
			
			lcPerguntaNr_old = lcPerguntaNr 
			SELECT uCrsQuestionarioTemp
			GOTO TOP  
			SELECT MIN(uCrsQuestionarioTemp.Prox_Ordem) as prox_ordem FROM uCrsQuestionarioTemp  WHERE uCrsQuestionarioTemp.quest_perguntastamp = uCrsQuestionario.quest_perguntastamp AND  uCrsQuestionarioTemp.sel = .T. INTO cursor ucProxOrdem readwrite    
			lcPerguntaNr = ucProxOrdem.Prox_Ordem
			
			uf_questionario_limpaSel(lcPerguntaNr_old)
			fecha("ucProxOrdem ")
			lcSelResp = ''
		
	ENDCASE

   	
   	uf_questionario_limpaForm()
   	
   
	uf_questionario_geraEcra()
   	
   	uf_questionario_controlaEstadoMenus()
   	   
ENDFUNC


FUNCTION uf_questionario_limpaSel
	LPARAMETERS lcPerguntaNr_old

	SELECT uCrsQuestionarioTemp
	lcPermiteMulti = uCrsQuestionarioTemp.permiteMulti
	SELECT COUNT(*) as countSel FROM uCrsQuestionarioTemp WHERE uCrsQuestionarioTemp.sel  = .T. INTO CURSOR uCrsQuestionarioTempCount READWRITE 
			
			

	SELECT uCrsQuestionario
	GOTO TOP 
	SELECT * FROM uCrsQuestionario WHERE uCrsQuestionario.ordem = lcPerguntaNr_old INTO CURSOR uCrsQuestionarioLimpaSel READWRITE
	
	SELECT uCrsQuestionarioLimpaSel
	GOTO TOP 
	SCAN For uCrsQuestionarioLimpaSel.quest_pergunta_respStamp != lcSelResp 
		SELECT uCrsQuestionario
		GOTO TOP 
		LOCATE FOR  uCrsQuestionario.ordem = lcPerguntaNr and uCrsQuestionarioLimpaSel.quest_pergunta_respStamp = uCrsQuestionario.quest_pergunta_respStamp 
		SELECT uCrsQuestionario
		Replace uCrsQuestionario.sel WITH .F.
	
	ENDSCAN
	
	
	SELECT uCrsQuestionario
	GOTO TOP
	
	fecha("uCrsQuestionarioLimpaSel")

ENDFUNC 




FUNCTION uf_questionario_Anterior
	
	lcSelResp = '' 
	
	questionario.quest.valor.value = ''
	
	questionario.menu1.proximo.visible = .T.
	
	SELECT uCrsQuestionario.quest_perguntaStamp FROM uCrsQuestionario WHERE  Prox_ordem = lcPerguntaNr  INTO CURSOR uCrsAntTemp ReadWrite
	
	SELECT uCrsAntTemp 
	lcPerguntaNr = uf_gerais_getUmValor("quest_pergunta", "ordem", "quest_pergunta.quest_perguntaStamp = '"+uCrsAntTemp .quest_perguntaStamp +"'")
	
	uf_questionario_controlaEstadoMenus()
	
	uf_questionario_limpaform()
	
		
	uf_questionario_geraEcra()
	uf_questionario_controlaEstadoMenus()
	
	fecha("uCrsAntTemp")
	
ENDFUNC





FUNCTION uf_questionario_sair
	IF !uf_perguntalt_chama("Deseja cancelar o preenchimento do question�rio?" , "Sim", "N�o")
		RETURN .F.
	ENDIF   
			
	
	fecha("uCrsQuestionario")
	TRY 
		QUESTIONARIO.hide
		QUESTIONARIO.release
		
		myRetornoPergunta = .F.		
	ENDTRY 
	
	
	
	RELEASE lcRefQuest, lcPerguntaNr , lcGrpRespostas, lcSelResp, myCursPos  
	

ENDFUNC
**
FUNCTION uf_questionario_gravar 
	LOCAL lcMultipla,lcSexo, lcFaixaEtaria
	lcMultipla = .F.

	
	IF YEAR(UcrsAtendCl.nascimento) == 1900
	
		create Cursor uCrsSel(sel c(200))
		Select uCrsSel
		APPEND  Blank
		IF !USED("uCrsTempSitCliFe")
			
			LOCAL lcSQL
			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				select code, campo from b_multidata where tipo = 'a_faixaetaria' order by ordem 
			ENDTEXT
			If !uf_gerais_actGrelha("", "uCrsTempSitCliFe", lcSql)
				uf_perguntalt_chama("N�o foi possivel encontrar regitos.", "", "OK", 16)
				RETURN .f.
			ENDIF
		ENDIF	
			
			uf_valorescombo_chama("uCrsSel.sel", 2, "uCrsTempSitCliFe", 2, "campo", "campo",.f. ,.f.,.F.,.T.,.f.,.f.,.f.,.f.,.f.,.t.)
			lcFaixaEtaria = uCrsSel.sel
			fecha("uCrsSel")
		
		
	ELSE
		 lcFaixaEtaria = uf_gerais_getidade(UcrsAtendCl.nascimento,.T.)
	ENDIF
	
	IF !EMPTY(UcrsAtendCl.sexo)
		lcSexo = UcrsAtendCl.sexo
	ELSE 
		create Cursor uCrsSel(sel c(200))
		Select uCrsSel
		APPEND  Blank
		
		IF !USED("uCrsTempSitCliSexo")
			LOCAL lcSQL
			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				select code, campo from b_multidata where tipo = 'a_sexo' order by ordem 
			ENDTEXT
			If !uf_gerais_actGrelha("", "uCrsTempSitCliSexo", lcSql)
				uf_perguntalt_chama("N�o foi possivel encontrar regitos.", "", "OK", 16)
				RETURN .f.
			ENDIF
			
		ENDIF
		
		uf_valorescombo_chama("uCrsSel.sel", 2, "uCrsTempSitCliSexo", 2, "code", "code,Campo",.f. ,.f.,.f.,.T.,.f.,.f.,.f.,.f.,.f.,.t.)
		SELECT uCrsSel
		lcSexo = ALLTRIM(uCrsSel.sel)
		fecha("uCrsSel")
		
	ENDIF
	
	
	
	
	SELECT uCrsQuestionarioTemp
	GOTO TOP 
	lcMultipla = RECcount("uCrsQuestionarioTemp") > 1
	SELECT uCrsQuestionarioTemp
	GOTO TOP 
	
	SELECT uCrsQuestionario
	GOTO TOP 
	LOCATE FOR uCrsQuestionario.Quest_pergunta_respStamp = lcSelResp

	IF uCrsQuestionario.Prox_ordem > 0 AND !lcMultipla &&AND uCrsQuestionario.id <> 1   		
			
		IF EMPTY(Questionario.quest.mytxtbox.Value)
			uf_perguntalt_chama("Necessita Responder.", "OK", "", 16)
			RETURN .F.		
		ENDIF 
	ELSE 
	
		IF EMPTY(lcSelResp)
			uf_perguntalt_chama(" Necessita selecionar uma resposta.", "OK", "", 16)
	       	RETURN .F.
		ENDIF 
   	ENDIF 

	SELECT uCrsQuestionario
	GOTO TOP
	
		* Criar cursor tempor�rio para armazenar os resultados
	CREATE CURSOR cursor_resultados (quest_perguntastamp C(30), questStamp c(30), respostas_concatenadas c(254))

	* Selecionar os registros distintos de quest_perguntastamp
	SELECT DISTINCT quest_perguntastamp, queststamp FROM uCrsQuestionario INTO CURSOR cursor_perguntas

	* Percorrer as perguntas e concatenar as respostas
	SCAN
	    * Inicializar a vari�vel para concatenar as respostas
	    m_respostas = ""
	    
	    * Selecionar as respostas onde sel � verdadeiro e quest_perguntastamp � igual
	    SELECT ALLTRIM(resposta) as resposta FROM uCrsQuestionario ;
	    WHERE quest_perguntastamp = cursor_perguntas.quest_perguntastamp AND sel = .T. ;
	    INTO CURSOR cursor_respostas
	    
	    * Percorrer as respostas e concatenar
	    SCAN
	        IF EMPTY(m_respostas)
	            m_respostas = ALLTRIM(cursor_respostas.resposta)
	        ELSE
	            m_respostas = m_respostas + ", " + ALLTRIM(cursor_respostas.resposta)
	        ENDIF
	    ENDSCAN
	    
	    * Inserir no cursor final
	    INSERT INTO cursor_resultados (quest_perguntastamp, queststamp,respostas_concatenadas) ;
	        VALUES (cursor_perguntas.quest_perguntastamp,cursor_perguntas.queststamp ,m_respostas)
	ENDSCAN

	* Exibir o resultado final
	SELECT cursor_resultados
	*SELECT uCrsQuestionario
	GOTO TOP 
	SCAN 
		lcStampTemp = uf_gerais_Stamp()
		lcRespostaString = alines(uc_RespostaString,STRTRAN( uCrsQuestionario.resposta,"**",chr(13)))
		lcDate = uf_gerais_getdate(Date(),"SQL") 
		lcHora = TIME()
	
	
		TEXT TO lcsql TEXTMERGE NOSHOW
	
			INSERT INTO [dbo].[quest_respostas]
	           ([quest_respostasStamp]
	           ,[questStamp]
	           ,[quest_respostas_grpStamp]
	           ,[quest_perguntaStamp]
	           ,[fistamp]
	           ,[resposta]
	           ,[ref]
	           ,[no]
	           ,[sexo]
	           ,[faixaEtaria]
	           ,[operador]
	           ,[site]
	           ,[ousrdata]
	           ,[ousrhora]
	           ,[ousrinis]
	           ,[usrdata]
	           ,[usrinis])
	     	VALUES
	           ('<<lcStampTemp>>'
	           ,'<<cursor_resultados.questStamp>>'
	           ,'<<lcGrpRespostas>>'
	           ,'<<cursor_resultados.quest_perguntaStamp>>'
	           ,'<<lcNrAtend>>'
	           ,'<<ALLTRIM(IIF( lcRespostaString > 1 ,uc_RespostaString[2],cursor_resultados.respostas_concatenadas))>>'
	           ,'<<lcRefQuest>>'
	           ,'<<UcrsAtendCl.no>>'
	           ,'<<lcSexo>>'
	           ,'<<lcFaixaEtaria>>'
	           ,'<<ch_userno>>'
	           ,'<<mysite>>'
	           ,'<<lcDate>>'
	           ,'<<lcHora>>'
	           ,''
	           ,'<<lcDate>>'
	           ,'')
	           
	           
	    ENDTEXT
	    IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
	       uf_perguntalt_chama("Ocorreu uma anomalia ao gravar as respostas. Por Favor contacte o Suporte.", "OK", "", 16)
	       RETURN .F.
	    ENDIF
	
	ENDSCAN
	
	uf_perguntalt_chama("Question�rio Gravado com Sucesso.", "OK", "", 16)
	
	
	fecha("uCrsQuestionario")
	
	TRY 
		QUESTIONARIO.hide
		QUESTIONARIO.release
		
		myRetornoPergunta = .t.
	ENDTRY 
	
	
	
	RELEASE lcRefQuest, lcPerguntaNr , lcGrpRespostas, lcSelResp, myCursPos, lcNrAtend
	

	
ENDFUNC




FUNCTION uf_questionario_FiltraCursor
	

	lcFiltro = [LIKE('*'+UPPER(ALLTRIM(Questionario.quest.valor.value))+'*',UPPER(uCrsQuestionarioTemp.] + "Resposta"+[)) ]


	** manter sempre os j� seleccionados no filtro
	IF !EMPTY(lcFiltro)
		lcFiltro = "("+lcFiltro + " " + "OR uCrsQuestionarioTemp.sel = .t.)"&&  +  " and uCrsQuestionarioTemp.ordem = '" + lcPerguntaNr + "'"
 	ENDIF
 	

	SELECT uCrsQuestionarioTemp
	GO TOP
	IF !EMPTY(lcFiltro)
		SET FILTER TO &lcFiltro AND uCrsQuestionarioTemp.ordem = lcPerguntaNr 
	ELSE
		SET FILTER TO
	ENDIF

	QUESTIONARIO.Quest.GRIDPESQMULTI.refresh
ENDFUNC




FUNCTION uf_Questionario_Teclado
	*Questionario.tecladoVirtual1.show("Questionario.quest.gridPesqMulti", 161)
ENDFUNC

FUNCTION uf_Questionario_escolhe
	** Caso esteja definido multiselecao, nao faz nada
	IF Questionario.MultiSelecao == .t.
		
		
		SELECT uCrsQuestionarioTemp
		lcSelResp = uCrsQuestionarioTemp.quest_pergunta_respStamp
	
		SELECT uCrsQuestionario
		GOTO TOP 
		LOCATE FOR uCrsQuestionario.Quest_pergunta_respStamp = lcSelResp
		Replace uCrsQuestionario.resposta  		WITH ALLTRIM(uCrsQuestionarioTemp.resposta)
		Replace uCrsQuestionario.sel  			WITH uCrsQuestionarioTemp.sel
		
		
		
		RETURN .f.
	ELSE
		SELECT uCrsQuestionarioTemp
		lcSelResp = uCrsQuestionarioTemp.quest_pergunta_respStamp
		SELECT uCrsQuestionarioTemp
		GOTO TOP 
		SCAN 
			IF !uf_gerais_compstr(uCrsQuestionarioTemp.quest_pergunta_respStamp, lcSelResp)
				Replace uCrsQuestionarioTemp.sel WITH .F.
			ENDIF 
		ENDSCAN
		SELECT uCrsQuestionarioTemp
		GOTO TOP 
		LOCATE FOR uCrsQuestionarioTemp.quest_pergunta_respStamp = lcSelResp
		
		
		SELECT uCrsQuestionario
		GOTO TOP 
		LOCATE FOR uCrsQuestionario.Quest_pergunta_respStamp = lcSelResp
		Replace uCrsQuestionario.resposta  		WITH ALLTRIM(uCrsQuestionarioTemp.resposta)
		Replace uCrsQuestionario.sel  			WITH uCrsQuestionarioTemp.sel
		
		
	ENDIF
	
ENDFUNC
