*********************************
*		Criar Cursores			*
*********************************
FUNCTION uf_selcontas_chama
	LPARAMETERS nObj, lcConta
	
	PUBLIC myOrigemContaPPCC, myOrigemObjPPCC
	
	myOrigemContaPPCC 	= lcConta
	myOrigemObjPPCC		= nObj
	
	TEXT TO lcSQL TEXTMERGE noshow
		SET fmtonly on
		
		SELECT TOP 1
			sel	= CONVERT(bit,0)
			,conta
			,descricao
			,ncont
			,contaiva
		FROM 
			pc
			(nolock)
			
		SET fmtonly off
	ENDTEXT
	IF !uf_gerais_actgrelha("", "uCrsSelContas", lcSql)
		uf_perguntalt_chama("OCORREU UM PROBLEMA AO OBTER DADOS DA CONTA. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
	ENDIF
	
	
	** abre painel
	IF type("selcontas")=="U"
		DO FORM selcontas
	ELSE
		selcontas.show
	ENDIF
ENDFUNC


**
FUNCTION uf_selcontas_chamamenu
	selcontas.menu1.adicionaopcao("atualizar", "Atualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_selcontas_carregaDados","A")
ENDFUNC


**
FUNCTION uf_selcontas_carregaDados
		
	LOCAL lcSQL
	STORE '' TO lcSQL
	
	TEXT TO lcSQL TEXTMERGE noshow
		SELECT
			sel	= CONVERT(bit,0)
			,conta
			,descricao
			,ncont
			,contaiva
		FROM 
			pc
			(nolock)
		WHERE
			conta like '<<ALLTRIM(selcontas.txtConta.value)>>%'
			and descricao like '%<<ALLTRIM(selcontas.txtDesc.value)>>%'
			and ncont like '<<ALLTRIM(selcontas.txtContribuinte.value)>>%'
			and contaiva like '<<ALLTRIM(selcontas.txtIVA.value)>>%'
	ENDTEXT

	IF !uf_gerais_actgrelha("selcontas.grdSelContas", "uCrsSelContas", lcSql)
		uf_perguntalt_chama("OCORREU UM PROBLEMA AO OBTER DADOS DA CONTA. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
	ENDIF
ENDFUNC


**
FUNCTION uf_selcontas_sair
	IF USED("uCrsSelContas")
		fecha("uCrsSelContas")
	ENDIF
	
	RELEASE myLocalSelContas
	RELEASE myOrigemContaPPCC
	RELEASE myOrigemObjPPCC
	
	selcontas.hide
	selcontas.release
ENDFUNC
