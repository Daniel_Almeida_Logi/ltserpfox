PROCEDURE uf_browlist_chama
	LPARAMETERS uv_cur, uv_formName, ua_fieldSpec
	
	IF EMPTY(uv_cur)
		RETURN .F.
	ENDIF

	IF !WEXIST("BROWLIST")
		DO FORM BROWLIST
	ELSE
		BROWLIST.SHOW
	ENDIF
	
	IF !EMPTY(uv_formName)
		BROWLIST.caption = ALLTRIM(uv_formName)
	ENDIF

	BROWLIST.grid1.recordSource = ""
	BROWLIST.grid1.columnCount = 0

	SELECT * FROM &uv_cur. WITH (BUFFERING = .T.) INTO CURSOR uc_browTmp

	SELECT uc_browTmp
	GO TOP

	BROWLIST.grid1.recordSource = "uc_browTmp"

	i = AFIELDS(ua_fields)

	DO WHILE i > 0

		BROWLIST.grid1.addColumn(1)
		
		WITH BROWLIST.grid1.columns(BROWLIST.grid1.columnCount).header1
			IF TYPE("ua_fieldSpec[i,1]") <> "U"
				.caption = ua_fieldSpec(i,1)
			ELSE
				.caption = ua_fields(i,1)
			ENDIF
			
			.alignment = 2
			.fontName = 'Verdana'
			.fontSize = 9
		ENDWITH

		WITH BROWLIST.grid1.columns(BROWLIST.grid1.columnCount)
			.name = ua_fields(i,1)
			.controlSource =  "uc_browTmp." + ALLTRIM(ua_fields(i,1))
			.readOnly = .T.
			
			IF TYPE("ua_fieldSpec[i,2]") <> "U"
				.inputMask = ua_fieldSpec[i,2]
				.format = ua_fieldSpec[i,2]
			ENDIF

			IF ua_fields(i, 2) = "L"
				.removeObject('TEXT1')
				.addobject('chk1', 'CHECKBOX')
				.sparse = .F.
				WITH .chk1
					.centered  = .T.
					.picturePosition = 14
					.specialEffect = 1
					.style = 1
					.DownPicture = myPath + "\imagens\icons\checked_b_24.bmp"
					.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
				ENDWITH
			ENDIF
			
			IF ua_fields(i, 2) = "M"
				.removeObject('TEXT1')
				.addobject('edtb1', 'EDITBOX')
				.sparse = .F.
			ENDIF

		ENDWITH
		
		i= i-1

	ENDDO
	
	BROWLIST.grid1.autoFit()

ENDPROC

PROCEDURE uf_browlist_sair

	IF WEXIST("BROWLIST")
		BROWLIST.HIDE
		BROWLIST.RELEASE
	ENDIF

ENDPROC