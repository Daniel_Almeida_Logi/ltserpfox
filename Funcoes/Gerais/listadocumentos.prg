**
FUNCTION uf_LISTADOCUMENTOS_chama
	LPARAMETERS lcNo, lcEstab, lcNome, lcTipo, lcDoc
	
	&&valida parameters
	IF EMPTY(lcNO)
		lcNo = 0
	ENDIF
	
	IF EMPTY(lcEstab) OR !(Type("lcEstab")=="N")
		lcEstab = 0
	ENDIF
	
	IF EMPTY(lcNome)
		lcNome = ""
	ENDIF
	
	IF EMPTY(lcTipo)
		uf_perguntalt_chama("O PAR�METRO TIPO TEM QUE SER FORNECIDO!","OK","",16)
		RETURN .F.
	ENDIF
	
	PUBLIC myOrigemTipoPLD, myOrigemDocPLD
	myOrigemTipoPLD = lcTipo
	myOrigemDocPLD	= lcDoc
	*******************************
	
	** Cria cursores vazios
	
	** cabe�alho do documento
	IF !USED("uCrsPesqDocsLD")
		lcSQl=""
		Text To lcSQL Textmerge NOSHOW
			set fmtonly on
			
			exec up_gerais_listaDocumentosRef '8168617', 'Venda a Dinheiro', '20000101', '30000101', 'FT'
				
			set fmtonly off
		Endtext			
		uf_gerais_actgrelha("", "uCrsPesqDocsLD", lcSql)
	ENDIF
	
	
	** linhas do documento
	IF !USED("uCrsPesqLDocsLD")
		lcSQl=""
		Text To lcSQL Textmerge NOSHOW
			set fmtonly on
			
			exec up_gerais_listaLinhasDoc 'xxx', 'FT'
			
			set fmtonly off
		Endtext
		uf_gerais_actgrelha("", "uCrsPesqLDocsLD", lcSql)
	ENDIF
	
	
	** lista de documentos
	IF !USED("uCrsListaDocsLD")
		DO CASE 
			CASE UPPER(ALLTRIM(lcTipo)) = "ST"
				* Carrega lista de documentos com bases nas permissoes
				lcSQLDocs = ""
				TEXT TO lcSQLDocs TEXTMERGE NOSHOW
					exec up_gerais_listaDocumentos '<<m.ch_userno>>', '<<m.ch_grupo>>'
				ENDTEXT
			
			CASE UPPER(ALLTRIM(lcTipo)) = "FL"
				* Carrega lista de documentos com bases nas permissoes
				lcSQLDocs = ""
				TEXT TO lcSQLDocs TEXTMERGE NOSHOW
					exec up_perfis_validaAcessoDocs  '<<m.ch_userno>>', '<<m.ch_grupo>>', 'visualizar', 'FL'
				ENDTEXT
			
			CASE UPPER(ALLTRIM(lcTipo)) = "CL"
				* Carrega lista de documentos com bases nas permissoes
				lcSQLDocs = ""
				TEXT TO lcSQLDocs TEXTMERGE NOSHOW
					exec up_perfis_validaAcessoDocs '<<m.ch_userno>>', '<<m.ch_grupo>>', 'visualizar', 'CL', 1, '<<mysite>>'
				ENDTEXT
			
			CASE UPPER(ALLTRIM(lcTipo)) = "ENCAUTOMATICAS"
				* Carrega lista de documentos com bases nas permissoes
				lcSQLDocs = ""
				TEXT TO lcSQLDocs TEXTMERGE NOSHOW
					exec up_perfis_validaAcessoDocs '<<m.ch_userno>>', '<<m.ch_grupo>>', 'visualizar', 'CL', 1, '<<mysite>>'
				ENDTEXT
				
			OTHERWISE 
				uf_perguntalt_chama("O PAR�METRO TIPO � INV�LIDO!","OK","", 16)
				RETURN .F.
		ENDCASE
	
		IF !uf_gerais_actgrelha("", "uCrsListaDocsLD", lcSQLDocs)
			uf_perguntalt_chama("Ocorreu uma anomalia a obter a lista de Documentos. Por favor contacte o suporte.","OK","",16)
			RETURN .F.
		ENDIF 
	ENDIF
	
	
	&& Controla Abertura do Painel			
	IF type("LISTADOCUMENTOS")=="U"
		DO FORM LISTADOCUMENTOS
	ELSE
		LISTADOCUMENTOS.show
	ENDIF
	
	
	** Altera caption do painel respectivamente
	DO CASE 
		CASE UPPER(ALLTRIM(lcTipo)) = "ST"
			LISTADOCUMENTOS.caption = "DOCUMENTOS DO PRODUTO"
			
		CASE UPPER(ALLTRIM(lcTipo)) = "FL"
			LISTADOCUMENTOS.caption = "DOCUMENTOS DO FORNECEDOR"
			
		CASE UPPER(ALLTRIM(lcTipo)) = "CL"
			LISTADOCUMENTOS.caption = "DOCUMENTOS DO CLIENTE"
	ENDCASE 
	
	
	LISTADOCUMENTOS.txtNome.value = ALLTRIM(lcNome)
	LISTADOCUMENTOS.txtNo.value = lcNo
	LISTADOCUMENTOS.txtEstab.value = lcEstab
	
	IF UPPER(ALLTRIM(lcTipo)) == "ST"
		LISTADOCUMENTOS.lblNome.caption = "Designa��o"
		LISTADOCUMENTOS.lblNo.caption = "Refer�ncia"
		LISTADOCUMENTOS.txtNo.width = 103
		LISTADOCUMENTOS.lblEstab.visible = .f.
		LISTADOCUMENTOS.txtEstab.visible = .f.
	ELSE
		LISTADOCUMENTOS.lblNome.caption = "Nome"
		LISTADOCUMENTOS.lblNo.caption = "N�"
		LISTADOCUMENTOS.txtNo.width = 62
		LISTADOCUMENTOS.lblEstab.visible = .t.
		LISTADOCUMENTOS.txtEstab.visible = .t.
	ENDIF
	
	IF !EMPTY(myOrigemDocPLD)
		listaDocumentos.lstTipoDoc.displayvalue = myOrigemDocPLD
		listaDocumentos.lstTipoDoc.Click()
		listadocumentos.GridPesqD.setfocus()
	ENDIF
	
	LISTADOCUMENTOS.lblRegistosDoc.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqDocsLD"))) + " Resultados"
	LISTADOCUMENTOS.lblRegistosLinhas.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqLDocsLD"))) + " Resultados"
ENDFUNC


**
FUNCTION uf_LISTADOCUMENTOS_carregaMenu
	LISTADOCUMENTOS.menu1.adicionaopcao("actualiza", "Atualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_LISTADOCUMENTOS_PesquisaDocs with .f.","A")
	LISTADOCUMENTOS.menu1.adicionaopcao("docsAbertos", "Doc. Abertos", myPath + "\imagens\icons\unchecked_w.png", "uf_LISTADOCUMENTOS_docsAbertos","D")
	LISTADOCUMENTOS.menu1.adicionaopcao("linhasAbertas", "Lin. Abertas", myPath + "\imagens\icons\unchecked_w.png", "uf_LISTADOCUMENTOS_linhasAbertas","L")
	LISTADOCUMENTOS.menu1.adicionaopcao("chamaDoc", "Navega Doc.", myPath + "\imagens\icons\doc_seta_w.png", "uf_LISTADOCUMENTOS_navega","N")
ENDFUNC


**
FUNCTION uf_LISTADOCUMENTOS_PesquisaDocs
	LPARAMETERS tcBool
	
	listadocumentos.txtNome.setfocus
	
	IF !tcBool && Actualiza Tudo
		Local lcSQL, lcRefNo, lcDataIni, lcDataFim, lcEstab
		Store "" To lcSQL

		* validar data
		lcDataIni 	= uf_gerais_getDate(LISTADOCUMENTOS.txtDe.value, "SQL")
		lcDataFim 	= uf_gerais_getDate(LISTADOCUMENTOS.txtA.value, "SQL")
		lcRefNo		= LISTADOCUMENTOS.txtNo.value
		lcEstab		= LISTADOCUMENTOS.txtEstab.value
		lcNmdoc		= ALLTRIM(LISTADOCUMENTOS.lstTipoDoc.value)
		
		&& LIMPA LINHAS DO DOCUMENTO
		Select uCrsPesqLDocsLD
		Delete All
		
		If Used("uCrsPesqDocsLD")
			Select uCrsPesqDocsLD
			Delete all
		ENDIF
		
		*******************************
		* CALCULA ORIGEM DO DOCUMENTO *
		*******************************
		If Used("uCrsTabelaPesq")
			Fecha("uCrsTabelaPesq")
		Endif
		
		TEXT TO lcSQL Textmerge noshow
			select top 1 nmdoc, 'FT' as tabela from td (nolock) where nmdoc='<<Alltrim(LISTADOCUMENTOS.lstTipoDoc.value)>>'
			union all 
			select top 1 nmdos, 'BO' as tabela from ts (nolock) where nmdos='<<Alltrim(LISTADOCUMENTOS.lstTipoDoc.value)>>'
			union all 
			select top 1 cmdesc, 'FO' as tabela from cm1 (nolock)where cmdesc='<<Alltrim(LISTADOCUMENTOS.lstTipoDoc.value)>>'
		ENDTEXT
		
		If uf_gerais_actgrelha("", "uCrsTabelaPesq", lcSql)
			* Selecciona dados do documento
			lcSQL=""
			
			LOCAL lcTabela
			lcTabela = Upper(Alltrim(uCrsTabelaPesq.tabela))
			
			DO CASE
			 	CASE myOrigemTipoPLD == "ST"
					Text To lcSQL Textmerge NOSHOW
						exec up_gerais_listaDocumentosRef '<<lcRefNo>>', '<<lcNmdoc>>', '<<lcDataIni>>', '<<lcDataFim>>', '<<lcTabela>>'
					ENDTEXT
				CASE myOrigemTipoPLD == "ENCAUTOMATICAS"
					Text To lcSQL Textmerge NOSHOW
						exec up_gerais_listaDocumentosRef '', '<<lcNmdoc>>', '<<lcDataIni>>', '<<lcDataFim>>', '<<lcTabela>>', 0, 0, 0
					ENDTEXT
				OTHERWISE
					Text To lcSQL Textmerge NOSHOW
						exec up_gerais_listaDocumentosRef '', '<<lcNmdoc>>', '<<lcDataIni>>', '<<lcDataFim>>', '<<lcTabela>>', 0, <<lcRefNo>>, <<lcEstab>>
					ENDTEXT
			ENDCASE
			uf_gerais_actGrelha("LISTADOCUMENTOS.GridPesqD", "uCrsPesqDocsLD", lcSQL)
	   ENDIF
	ENDIF


	** aplica restric��es
	IF (LISTADOCUMENTOS.menu1.docsAbertos.tag == "true")

		Select uCrsPesqDocsLD
		If Reccount("uCrsPesqDocsLD")>0
			SET Filter To uCrsPesqDocsLD.status == 'A'
			GO TOP
		ENDIF
		
		SELECT uCrsPesqDocsLD
		COUNT FOR !(UPPER(ALLTRIM(uCrsPesqDocsLD.status))=='A') TO docAbertos
		LISTADOCUMENTOS.lblRegistosDoc.caption = ALLTRIM(STR(docAbertos)) + " Resultados"
		LISTADOCUMENTOS.lblRegistosLinhas.caption = ALLTRIM(STR(docAbertos)) + " Resultados"
	ELSE
		SELECT uCrsPesqDocsLD
		If Reccount("uCrsPesqDocsLD")>0
			Set Filter To 
			GO TOP
		ENDIF
		
		LISTADOCUMENTOS.lblRegistosDoc.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqDocsLD"))) + " Resultados"
		LISTADOCUMENTOS.lblRegistosLinhas.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqLDocsLD"))) + " Resultados"
	ENDIF	
	
	SELECT uCrsPesqDocsLD
	GO TOP
	
   	LISTADOCUMENTOS.refresh
ENDFUNC


**
FUNCTION uf_LISTADOCUMENTOS_PesquisaLinhas
	
	If Used("uCrsPesqDocsLD")
		Select uCrsPesqDocsLD
		
		If !Empty(uCrsPesqDocsLD.stamp)
			Local lcSelStamp, lcSQL,lcValida, lcPos
			Store "" To lcSQL
			Store 0 To lcValida
			
			lcSelStamp = uCrsPesqDocsLD.stamp
							
			If Used("uCrsPesqLDocsLD")
				Select uCrsPesqLDocsLD
				Delete all
			Endif
			
			lcSQl = ""
			Select uCrsPesqDocsLD
			
			Text To lcSQL Textmerge noshow
				exec up_gerais_listaLinhasDoc '<<lcSelStamp>>', '<<Upper(Alltrim(uCrsTabelaPesq.tabela))>>'
			Endtext
			
			uf_gerais_actGrelha("LISTADOCUMENTOS.GridPesqL", "uCrsPesqLDocsLD", lcSQL)
		
			LISTADOCUMENTOS.GridPesqL.refresh
			LISTADOCUMENTOS.GridPesqD.setfocus
		ENDIF
	ENDIF
	
	SELECT uCrsPesqLDocsLD
	GO TOP
	
	LISTADOCUMENTOS.lblRegistosDoc.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqDocsLD"))) + " Resultados"
   	LISTADOCUMENTOS.lblRegistosLinhas.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqLDocsLD"))) + " Resultados"
ENDFUNC



**
FUNCTION uf_LISTADOCUMENTOS_docsAbertos
	IF LISTADOCUMENTOS.menu1.docsAbertos.tag == "true"
		LISTADOCUMENTOS.menu1.docsAbertos.tag = "false"
		LISTADOCUMENTOS.menu1.docsAbertos.config("Doc. Abertos", myPath + "\imagens\icons\unchecked_w.png", "uf_LISTADOCUMENTOS_docsAbertos","D")
	ELSE
		LISTADOCUMENTOS.menu1.docsAbertos.tag = "true"
		LISTADOCUMENTOS.menu1.docsAbertos.config("Doc. Abertos", myPath + "\imagens\icons\checked_w.png", "uf_LISTADOCUMENTOS_docsAbertos","D")
	ENDIF
	
	uf_LISTADOCUMENTOS_PesquisaDocs(.t.)
ENDFUNC



**
FUNCTION uf_LISTADOCUMENTOS_linhasAbertas
	IF LISTADOCUMENTOS.menu1.linhasAbertas.tag == "true"
		LISTADOCUMENTOS.menu1.linhasAbertas.tag = "false"
		LISTADOCUMENTOS.menu1.linhasAbertas.config("Lin. Abertas", myPath + "\imagens\icons\unchecked_w.png", "uf_LISTADOCUMENTOS_linhasAbertas","L")
		
		
		IF USED("uCrsPesqLDocsLD")
		SELECT uCrsPesqLDocsLD
		IF RECCOUNT("uCrsPesqLDocsLD") > 0
			
			SET Filter To uCrsPesqLDocsLD.qttmov < uCrsPesqLDocsLD.qtt
		ENDIF
	ENDIF
	
	ELSE
		LISTADOCUMENTOS.menu1.linhasAbertas.tag = "true"
		LISTADOCUMENTOS.menu1.linhasAbertas.config("Lin. Abertas", myPath + "\imagens\icons\checked_w.png", "uf_LISTADOCUMENTOS_linhasAbertas","L")
		
		IF USED("uCrsPesqLDocsLD")
			SELECT uCrsPesqLDocsLD
			SET Filter To
		ENDIF
	ENDIF
	
	LISTADOCUMENTOS.GridPesqD.refresh
	LISTADOCUMENTOS.GridPesqL.refresh
			
	LISTADOCUMENTOS.lblRegistosDoc.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqDocsLD"))) + " Resultados"
	LISTADOCUMENTOS.lblRegistosLinhas.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqLDocsLD"))) + " Resultados"
ENDFUNC



**
FUNCTION uf_LISTADOCUMENTOS_navega
	IF USED("uCrsPesqDocsLD")
		SELECT uCrsPesqDocsLD
		IF RECCOUNT("uCrsPesqDocsLD")>0
			IF !EMPTY(uCrsPesqDocsLD.stamp)
				
				SELECT uCrsListaDocsLD
				DO CASE
					CASE uCrsListaDocsLD.tabela == "FT"

						If myFtIntroducao == .t. OR myFtAlteracao == .t.
							uf_perguntalt_chama("O ECR� DE FACTURA��O ENCONTRA-SE EM MODO DE INTRODU��O OU EDI��O.","OK","",48)
						ELSE
							IF TYPE('ATENDIMENTO') == "U"
								uf_facturacao_chama(ALLTRIM(uCrsPesqDocsLD.stamp))
							ELSE
								uf_perguntalt_chama("N�O PODE ABRIR O ECR� DE GEST�O DE FACTURA��O COM O ECR� DE ATENDIMENTO ABERTO.","OK","",48)
							Endif 
						ENDIF
						
					OTHERWISE
					
						If mydocalteracao == .t. or mydocintroducao == .t.
							uf_perguntalt_chama("O ECR� DE DOCUMENTOS ENCONTRA-SE EM MODO DE INTRODU��O OU EDI��O.","OK","",48)
						ELSE
							uf_documentos_chama(ALLTRIM(uCrsPesqDocsLD.stamp))
						ENDIF
				ENDCASE
			ENDIF
		ENDIF
	ENDIF
ENDFUNC



**
FUNCTION uf_LISTADOCUMENTOS_sair
	**Fecha Cursores
	IF USED("uCrsPesqLDocsLD")
		fecha("uCrsPesqLDocsLD")
	ENDIF
	IF USED("uCrsPesqDocsLD")
		fecha("uCrsPesqDocsLD")
	ENDIF 
	IF USED("uCrsListaDocsLD")
		fecha("uCrsListaDocsLD")
	ENDIF 
	IF USED("uCrsTabelaPesq")
		fecha("uCrsTabelaPesq")
	ENDIF 
	
	RELEASE myOrigemTipoPLD
	
	LISTADOCUMENTOS.hide
	LISTADOCUMENTOS.release
ENDFUNC