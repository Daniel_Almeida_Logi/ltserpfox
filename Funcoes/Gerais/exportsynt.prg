FUNCTION uf_exportsynt_chama
**	LPARAMETERS lcLote, lcRef, lcEcra, lcTipoDoc
	
	IF TYPE("EXPORTSYNT") != "U"
		EXPORTSYNT.show
	ELSE
		
		DO FORM EXPORTSYNT
	ENDIF 
	
**	uf_exportsynt_CarregaDados()
ENDFUNC 


** 
FUNCTION uf_exportsynt_gravar

	LOCAL lcValidou, lcLinSel, lcLinSelOrdem
	STORE .t. TO lcValidou, lcLinSelOrdem
	STORE .f. TO lcLinSel

	IF EMPTY(EXPORTSYNT.nome.value)
		uf_perguntalt_chama("TEM DE PREENCHER UM NOME PARA A CONFIGURA��O.","OK","",48)
		RETURN .f.
	ENDIF 
	IF EMPTY(EXPORTSYNT.charsep.value)
		uf_perguntalt_chama("TEM DE PREENCHER O CARACTERE DE SEPARA��O.","OK","",48)
		RETURN .f.
	ENDIF 
	
	SELECT ucrsEXPORTSYNT 
	GO TOP
	SCAN
		IF ucrsEXPORTSYNT.sel = .t.
			lcLinSel = .t.
		ENDIF
		IF ucrsEXPORTSYNT.sel = .t. AND ucrsEXPORTSYNT.ordem=0
			lcLinSelOrdem= .f.
		ENDIF 
	ENDSCAN 
	
	IF lcLinSel = .f.
		uf_perguntalt_chama("TEM DE SELECIONAR PELO MENOS UMA LINHA PARA A CONFIGURA��O.","OK","",48)
		RETURN .f.	
	ENDIF 
	IF lcLinSelOrdem = .f.
		uf_perguntalt_chama("TODAS AS LINHAS SELECIONADAS T�M DE TER A ORDEM PREENCHIDA.","OK","",48)
		RETURN .f.	
	ENDIF 


	LOCAL lcStampCab, lcTable, lcCharSep
	STORE '' TO lcTable
	lcCharSep = ALLTRIM(EXPORTSYNT.charsep.value)

	lcStampCab = uf_gerais_stamp()
	IF TYPE("pesqfact")!='U'
		lcTable="pesqfact"
	ENDIF 
	
	TEXT TO lcSQL TEXTMERGE noshow
		insert into exportsynt (exportsyntstamp, [table], nome, sepchar, ousrinis, ousrdata, site_no)
		values ('<<ALLTRIM(lcStampCab )>>', '<<ALLTRIM(lcTable)>>', '<<ALLTRIM(EXPORTSYNT.nome.value)>>', '<<ALLTRIM(lcCharSep)>>', '<<m_chinis>>', dateadd(HOUR, <<difhoraria>>, getdate()), <<uCrsE1.Siteno>>)
	ENDTEXT
	uf_gerais_actGrelha("","",lcSQL)
	
	SELECT * FROM ucrsEXPORTSYNT WHERE sel=.t. ORDER BY ordem INTO CURSOR ucrsEXPORTSYNTAux readwrite
	SELECT ucrsEXPORTSYNTAux 
	GO TOP 
	SCAN
		TEXT TO lcSQL TEXTMERGE noshow
			insert into exportsynt_linhas (exportsynt_linhasstamp, exportsyntstamp, design, campo, ordem)
			values (LEFT(newid(),21), '<<ALLTRIM(lcStampCab)>>', '<<ALLTRIM(ucrsEXPORTSYNTAux.design)>>', '<<ALLTRIM(ucrsEXPORTSYNTAux.campo)>>', <<ucrsEXPORTSYNTAux.ordem>>)
		ENDTEXT
		uf_gerais_actGrelha("","",lcSQL)	
	ENDSCAN 
	fecha("ucrsEXPORTSYNT")
	
	SELECT * FROM ucrsEXPORTSYNTAux INTO CURSOR ucrsEXPORTSYNT_linhas readwrite
	
	TEXT TO lcSQL TEXTMERGE noshow
		SELECT * from exportsynt WHERE exportsyntstamp='<<ALLTRIM(lcStampCab)>>'
	ENDTEXT
	uf_gerais_actGrelha("","ucrsEXPORTSYNT",lcSQL)
	
	fecha("ucrsEXPORTSYNTAux")
	
	EXPORTSYNT.hide
	EXPORTSYNT.release
ENDFUNC

**
FUNCTION uf_exportsynt_sair
	IF USED("ucrsexportsynt")
		fecha("ucrsexportsynt")
	ENDIF

	EXPORTSYNT.hide
	EXPORTSYNT.release
	RETURN .f.
ENDFUNC

