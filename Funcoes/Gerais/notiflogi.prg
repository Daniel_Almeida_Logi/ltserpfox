FUNCTION uf_notifLogi_chama
	LPARAMETERS uv_texto, uv_tempo
	oForm = _screen.ActiveForm
	
	PUBLIC pv_notifLogi_time 
	
	pv_notifLogi_time = IIF(EMPTY(uv_tempo), 1500, uv_tempo)
	
	DO FORM NOFIFLOGI WITH uv_texto
	
	NOFIFLOGI.show
	
	oForm.show
	
	NOTIFLOGI.ADDOBJECT("notifLogiTimer", "logiTimer")
	
ENDFUNC

DEFINE CLASS logiTimer as Timer
	interval = pv_notifLogi_time

	FUNCTION Timer
	
		this.Enabled = .F.

		NOTIFLOGI.release()

	ENDFUNC
	
ENDDEFINE