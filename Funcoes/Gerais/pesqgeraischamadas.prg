**
FUNCTION uf_pesqgeraischamadas_Chama
	LPARAMETERS lcnumeroTel,lcDataInicio,lcDataFim,lctipo,lcNumber,lcestab,lcmotivo
	
	
	
	
	********* Controle de Permiss�es	(pode-se eliminar o codigo desta primeira validacao 'Gest�o de Documentos'?)
	IF !uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Gest�o de Documentos')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL GEST�O DE DOCUMENTOS.","OK","",48)
		RETURN .f.
	ENDIF
	
	
	IF !uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Pesquisa de Chamadas')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL PESQUISA DE CHAMADAS.","OK","",48)
		RETURN .f.
	ENDIF
	*********
	
	
	
	PUBLIC myOrderPesqChamadas	
	myOrderPesqChamadas = .t.
	
		
	
	IF !USED("ucrsPesqChamadas")
		uf_pesqgeraischamadas_actPesquisa(lcnumeroTel,lcDataInicio,lcDataFim,lctipo,lcNumber,lcestab,lcmotivo, .f.)
	ELSE
		SELECT ucrsPesqChamadas
		GO TOP
	ENDIF

	IF TYPE("PESQGERAISCHAMADAS")=="U"
		DO FORM PESQGERAISCHAMADAS 
	ELSE 
		PESQGERAISCHAMADAS.show
	ENDIF
	
	
	
		uf_pesqgeraischamadas_preencheValores(lcnumeroTel,lcDataInicio,lcDataFim,lctipo,lcNumber,lcestab,lcmotivo)
ENDFUNC 



FUNCTION uf_pesqgeraischamadas_actPesquisa
	LPARAMETERS lcnumeroTel,lcDataInicio,lcDataFim,lctipo,lcNumber,lcestab,lcmotivo , lcFa
	LOCAL lcTop 
	
	LOCAL lcSQL
	STORE "" TO lcSql
	  	
	  	
	  	****** corrigir a chamada a sp apos as caixas do no/estab ficarem vazias  - 20201007
	  	
	  	** mesmo so para garantir...
	  	IF(vartype(lcestab)=='N')
			lcNumber = ALLTRIM(STR(lcNumber ))
		ENDIF
		
		IF(vartype(lcestab)=='N')
			lcestab = ALLTRIM(STR(lcestab))
		ENDIF
		
	  	
  	
  		IF  empty( ALLTRIM(lcestab))  	  			
  			lcestab = -1
  		ENDIF
  		
  		IF  empty(ALLTRIM(lcNumber)) 
  			lcNumber = -1
  			lcestab = -1
  		ENDIF
	  		
	  	*******************	
	
  	
		lcSQL = ""
		TEXT TO lcSql NOSHOW TEXTMERGE
			exec usp_get_ChamadasTelefonicas '<<lcnumeroTel>>','<<lcDataInicio>>','<<lcDataFim>>','<<lctipo>>',<<lcNumber>>,<<lcestab>>,'<<lcmotivo>>'
		ENDTEXT
		
				
		set hours to 24
		set seconds off

		
		IF lcFa
			IF !uf_gerais_actGrelha("PESQGERAISCHAMADAS.GridPesq",[ucrsPesqChamadas],lcSql)
				uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR A LISTAGEM DE CHAMADAS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF
			
			PESQGERAISCHAMADAS.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("ucrsPesqChamadas"))) + " Resultados"
		
		ELSE
			IF !uf_gerais_actGrelha("",[ucrsPesqChamadas],lcSql)
				uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR A LISTAGEM DE CHAMADAS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF
		
		ENDIF 
		
		

ENDFUNC


**
FUNCTION uf_pesqgeraischamadas_sair
	IF type("PESQGERAISCHAMADAS")!="U"
		PESQGERAISCHAMADAS.hide
		PESQGERAISCHAMADAS.release
		
		RELEASE myOrderPesqChamadas
		
		fecha("ucrsPesqChamadas")
	ENDIF 
ENDFUNC


**
FUNCTION uf_pesqgeraischamadas_CarregaMenu

	** Configura Menu barra Lateral
	WITH PESQGERAISCHAMADAS.MENU1
		.adicionaopcao("aplicacoes", "Aplica��es" , mypath + "\imagens\icons\aplicacoes_w.png", "uf_gerais_menuAplicacoes With 'pesqgeraischamadas'","O" )
		.adicionaOpcao("opcoes", "Op��es", myPath + "\imagens\icons\opcoes_w.png", "uf_gerais_menuOpcoes With 'pesqdocumentos'","O")
		.adicionaOpcao("actualizar", "Pesquisar", myPath + "\imagens\icons\lupa_w.png", "uf_pesqgeraischamadas_pesquisa","A")
		.adicionaOpcao("subir", "Subir", myPath + "\imagens\icons\pg_up_b.png", "uf_gerais_MovePage with .t.,'pesqgeraischamadas.gridpesq','ucrsPesqChamadas'","05")
		.adicionaOpcao("descer", "Descer", myPath + "\imagens\icons\pg_down_b.png", "uf_gerais_MovePage with .f.,'pesqgeraischamadas.gridpesq','ucrsPesqChamadas'","24")			
		&&.adicionaOpcao("editar", "Editar Extens�es", myPath + "\imagens\icons\suporta.png", "uf_pesqgeraischamadas_editaextensoes","E")
				
	ENDWITH 
	
		

		** Configura Menu Aplica��es
	**	WITH PESQGERAISCHAMADAS.menu_aplicacoes
*!*			.adicionaOpcao("prepararEnc","Preparar Encomenda","","uf_prepararEncomenda_chama")	
*!*			.adicionaOpcao("conferenciaFact","Confer�ncia Facturas","","uf_conffact_chama with 'DOC'")
*!*			.adicionaOpcao("pagamentos", "Pagamentos", "", "uf_forn_pagamentos")
**			PESQGERAISCHAMADAS.menu_aplicacoes.adicionaOpcao("arquivodigital", "Arquivo Digital", "", "uf_arquivodigital_chama")
	**	ENDWITH 
	
	** Configura Menu op��es
	**WITH PESQGERAISCHAMADAS.menu_opcoes
	**	.adicionaOpcao("Imp", "Imprimir Dados", myPath + "\imagens\icons\imprimir_w.png", "uf_pesqgeraischamadas_imprimir")
	**	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Documentos - Permite fechar de seguida') == .t.
	**		.adicionaOpcao("Fech", "Fechar documentos", myPath + "\imagens\icons\imprimir_w.png", "uf_pesqgeraischamadas_fechar")
	**	ENDIF 
	&&  .adicionaOpcao("tecladoVirtual", "Teclado", myPath + "\imagens\icons\teclado_w.png", "uf_uf_PesqDocumentos_tecladoVirtual","T")
	
	**	IF uf_gerais_getParameter("ADM0000000213","BOOL") == .t. AND uf_gerais_getParameter("ADM0000000214","BOOL") == .f.
	**			.adicionaOpcao("evovendas","Importar Evovendas","","uf_evovendas_chama")
	**	ENDIF
	**ENDWITH 
	
	**pesqgeraischamadas.menu1.estado("processar", "HIDE")
	
	**IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Documentos - Permite fechar de seguida') == .t.
	**	PESQGERAISCHAMADAS.menu1.estado("seltodos", "HIDE")
	**ENDIF 
	
	**IF UPPER(myPaisConfSoftw) == 'ANGOLA'
		**PESQGERAISCHAMADAS.GridPesq.Envio.visible=.t.
		**PESQGERAISCHAMADAS.GridPesq.Info.visible=.t.
		**PESQGERAISCHAMADAS.GridPesq.codext.visible=.t.
		**PESQGERAISCHAMADAS.GridPesq.Status.visible=.t.
		**PESQGERAISCHAMADAS.GridPesq.Pagamento.visible=.t.
	**ELSE
	**	PESQGERAISCHAMADAS.GridPesq.Envio.visible=.f.
	**	PESQGERAISCHAMADAS.GridPesq.Info.visible=.f.
	**	PESQGERAISCHAMADAS.GridPesq.codext.visible=.f.
	**	PESQGERAISCHAMADAS.GridPesq.Status.visible=.f.
	**	PESQGERAISCHAMADAS.GridPesq.Pagamento.visible=.f.
	**ENDIF 
	
	PESQGERAISCHAMADAS.refresh
ENDFUNC


**
FUNCTION uf_pesqgeraischamadas_Escolhe


ENDFUNC


** 
FUNCTION uf_pesqgeraischamadas_gravar
	DO CASE 
		CASE PESQGERAISCHAMADAS.origem = "VFATURARESUMO"
			uf_pagForn_importaMovimentosResumo()
		OTHERWISE 
			**
	ENDCASE 
ENDFUNC 


*********************************
*	PESQUISAR PESSOA		*
*********************************
FUNCTION uf_PESQGERAISCHAMADAS_pesquisaPessoa
**
	lcSQL = ""
	public  myNumberLine
	TEXT TO lcSQL TEXTMERGE NOSHOW
		SELECT 
				ROW_NUMBER() OVER(ORDER BY (SELECT NULL))  AS Row,
				nome,
				no,
				estab,
				tipoTabela
			FROM (
					select 
						nome			AS nome,
						no				AS no,
						estab			AS estab,
						'utentes'		AS tipoTabela
						from 
						b_utentes

					union all 

					select 
						nome			 AS nome,
						NO				 AS no,
						estab			 AS estab,
						'fornecedores'	 AS tipoTabela
					from 
						FL

					union all 

					select 
						name			 AS nome,
						CONVERT(NUMERIC(10,0),extension) AS no,
						0				 AS estab,
						'operadores'	 AS tipoTabela
					from 
						extensionPhone
				) X
	ENDTEXT
	If !uf_gerais_actGrelha("", "uCrsTempB_us", lcSql)
		uf_perguntalt_chama("N�o foi possivel encontrar.", "", "OK", 16)
		RETURN .f.
	ENDIF

	uf_valorescombo_chama("myNumberLine", 0, "uCrsTempB_us", 2, "Row", "nome,no,estab",.f.)
	
	if(!EMPTY(myNumberLine))
		SELECT uCrsTempB_us
		GO Top
		LOCATE FOR uCrsTempB_us.Row == myNumberLine
		IF FOUND()
			PESQGERAISCHAMADAS.numUser.value = uCrsTempB_us.no 
			PESQGERAISCHAMADAS.estab.value =uCrsTempB_us.estab
			PESQGERAISCHAMADAS.tipo.value = uCrsTempB_us.tipoTabela
		ENDIF 
	ELSE 
			PESQGERAISCHAMADAS.numUser.value = ""
			PESQGERAISCHAMADAS.estab.value = ""
			PESQGERAISCHAMADAS.tipo.value = ""
	ENDIF
  	fecha("uCrsTempB_us")
  	fecha("myNumberLine")

	
ENDFUNC

*********************************
*	PESQUISAR TIPO		*
*********************************
FUNCTION uf_PESQGERAISCHAMADAS_pesquisaTipo()
**
	lcSQL = ""
	public  myType
	TEXT TO lcSQL TEXTMERGE NOSHOW
		
		select 'utentes'		 AS tipo 
		union 
		select 'fornecedores'	 AS tipo 
		union
		select 'operadores'	 	 AS tipo 
	ENDTEXT
	If !uf_gerais_actGrelha("", "uCrsTempB_us", lcSql)
		uf_perguntalt_chama("N�o foi possivel encontrar.", "", "OK", 16)
		RETURN .f.
	ENDIF

	uf_valorescombo_chama("myType", 0, "uCrsTempB_us", 2, "tipo", "tipo ",.f.)
	

	PESQGERAISCHAMADAS.tipo.value = myType

	
ENDFUNC

*********************************
*	PESQUISAR MOTIVO		*
*********************************
FUNCTION uf_PESQGERAISCHAMADAS_pesquisaMotivo()
	lcSQL = ""
	public  myMotive
	TEXT TO lcSQL TEXTMERGE NOSHOW
	
		SELECT 
			DISTINCT reasonTerminated AS Motivo
		FROM 
			logsPhoneCalls
	ENDTEXT
	If !uf_gerais_actGrelha("", "uCrsTempB_us", lcSql)
		uf_perguntalt_chama("N�o foi possivel encontrar.", "", "OK", 16)
		RETURN .f.
	ENDIF

	uf_valorescombo_chama("myMotive", 0, "uCrsTempB_us", 2, "Motivo", "Motivo",.f.)
	

	PESQGERAISCHAMADAS.motivo.value = myMotive
ENDFUNC

FUNCTION uf_pesqgeraischamadas_fechar
  	fecha("ucrsPesqChamadas")
ENDFUNC 



FUNCTION uf_pesqgeraischamadas_preencheValores
	LPARAMETERS lcnumeroTel,lcDataInicio,lcDataFim,lctipo,lcNumber,lcestab,lcmotivo
	
	PESQGERAISCHAMADAS.numeroTele.value = lcnumeroTel
	PESQGERAISCHAMADAS.dataIni.value = lcDataInicio
	PESQGERAISCHAMADAS.dataFim.value = lcDataFim
	
*!*		IF lcNumber = -1
*!*			PESQGERAISCHAMADAS.numUser.value = ""
*!*		else
*!*			PESQGERAISCHAMADAS.numUser.value = lcNumber
*!*		ENDIF
*!*		
*!*		IF lcestab = -1
*!*			PESQGERAISCHAMADAS.estab.value = ""
*!*		else
*!*			PESQGERAISCHAMADAS.estab.value = lcestab
*!*		ENDIF
	
	PESQGERAISCHAMADAS.numUser.value = lcNumber
	PESQGERAISCHAMADAS.estab.value = lcestab
	
	
	PESQGERAISCHAMADAS.tipo.value = lctipo
	PESQGERAISCHAMADAS.motivo.value = lcmotivo
	
ENDFUNC 


FUNCTION uf_pesqgeraischamadas_pesquisa
	LOCAL lcnumeroTel,lcDataInicio,lcDataFim,lctipo,lcNumber,lcestab,lcmotivo

	lcnumeroTel= Alltrim(PESQGERAISCHAMADAS.numeroTele.value) 
	lcDataInicio= PESQGERAISCHAMADAS.dataIni.value
	lcDataFim= PESQGERAISCHAMADAS.dataFim.value
	
	IF EMPTY(PESQGERAISCHAMADAS.numUser.value)
		**lcNumber = -1 
		lcNumber = '' 
	ELSE 
		lcNumber =  PESQGERAISCHAMADAS.numUser.value
	ENDIF 
	IF EMPTY(PESQGERAISCHAMADAS.numUser.value)
		**lcestab= -1 
		lcestab= ''
	ELSE 
		lcestab=  PESQGERAISCHAMADAS.estab.value
	ENDIF 	
	
	lctipo=Alltrim(PESQGERAISCHAMADAS.tipo.value)
	lcmotivo=Alltrim(PESQGERAISCHAMADAS.motivo.value)

	uf_pesqgeraischamadas_actPesquisa(lcnumeroTel,lcDataInicio,lcDataFim,lctipo,lcNumber,lcestab,lcmotivo,.t.)
	
	
ENDFUNC 


FUNCTION uf_pesqgeraischamada_download
	LPARAMETERS nameFile , tabela

	LOCAL lcFile, lcDestDir, lcDestfile

	IF !EMPTY(nameFile)
		IF !EMPTY(myDefFolder)
			lcDestDir=""+alltrim(uf_gerais_getParameter_site('ADM0000000010', 'text'))+"\"+ALLTRIM(tabela)+"\"
			lcFile = "'"+lcDestDir + ALLTRIM(nameFile)+"'"
			lcDestfile = "'"+myDefFolder+ "\" + ALLTRIM(nameFile)+"'"
								
			COPY FILE &lcFile TO &lcDestfile 
			uf_perguntalt_chama("TRANSFER�NCIA DO FICHEIRO " + ALLTRIM(nameFile) + " CONCLU�DA PARA A PASTA 'AnexosLogitools' DO SEU AMBIENTE DE TRABALHO!","OK","",64)
		ELSE
			uf_perguntalt_chama("N�O FOI SELECIONADA NENHUMA DIRETORIA!","OK","",64)
		ENDIF  
	ELSE 	
		uf_perguntalt_chama("N�O FOI SELECIONADA NENHUM FICHEIRO!","OK","",64)
	ENDIF  

ENDFUNC 



*!*	FUNCTION uf_pesqgeraischamadas_editaextensoes	

*!*			
*!*		&&uf_pesqgeraischamadasextensoes_chama("mysite", 2, "ucrsPesqChamadasExt", 2, "name", "name,extension")
*!*		uf_pesqgeraischamadasext_Chama()
*!*		
*!*		
*!*	ENDFUNC

