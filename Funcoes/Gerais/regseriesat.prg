FUNCTION uf_regSeriesAT_chama

	IF !uf_regSeriesAT_criarCursores()
        RETURN .F.
    ENDIF

    IF TYPE("REGSERIESAT")=="U"
		DO FORM REGSERIESAT
	ELSE
		REGSERIESAT.show
	ENDIF

ENDFUNC

FUNCTION uf_regSeriesAT_criarCursores

    IF WEXIST("REGSERIESAT")

        loGrid = regSeriesAT.grdTd
        lcCursor = "uc_regTD"

	    lnColumns = loGrid.ColumnCount
	
	    IF lnColumns > 0

		    DIMENSION laControlSource[lnColumns]
		    FOR lnColumn = 1 TO lnColumns
			    laControlSource[lnColumn] = loGrid.Columns[lnColumn].ControlSource
		    ENDFOR
	    ENDIF

        loGrid.RecordSource = ""

    ENDIF

    TEXT TO lcSql TEXTMERGE NOSHOW
        EXEC getRegTD '<<ALLTRIM(mySite)>>'
    ENDTEXT

    IF !uf_gerais_actGrelha("", "uc_regTD", lcSql)
        uf_perguntalt_chama("N�O FOI POSS�VEL CARREGAR OS DADOS DAS S�RIES DE FATURA��O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
        RETURN .F.
    ENDIF

    IF WEXIST("regSeriesAT")
        loGrid.RecordSource = lcCursor
        
        FOR lnColumn = 1 TO lnColumns
            loGrid.Columns[lnColumn].ControlSource = laControlSource[lnColumn]
        ENDFOR
    ENDIF

    RETURN .T.

ENDFUNC

FUNCTION uf_regSeriesAT_sair

    IF USED("uc_regTD")
        FECHA("uc_regTD")
    ENDIF

    IF WEXIST("REGSERIESAT")
        REGSERIESAT.release
    ENDIF

ENDFUNC

FUNCTION uf_regSeriesAT_comunicar


    regua(1,1,"A registar s�ries...")

    SELECT * FROM uc_regTD WITH (BUFFERING = .T.) WHERE uc_regTD.sel  INTO CURSOR uc_regSel READWRITE
    
    
    SELECT uc_regSel
    GO TOP
    SCAN
    	if(!EMPTY(ALLTRIM(uc_regSel.ATCUD)))
    		uf_perguntalt_chama("Est� a comunicar series j� comunicadas, por favor valide.","OK","",16)
	        uf_regSeriesAT_criarCursores()
	        regua(2)
	        RETURN .F.
    	ENDIF
    	SELECT uc_regSel   	
    ENDSCAN
    

    SELECT uc_regSel
    GO TOP


    uv_token = uf_gerais_stamp()
    uv_insert = ""
	uv_erroReg = .F.

    SELECT uc_regSel
    GO TOP    

    SCAN

        DO CASE
            CASE UPPER(ALLTRIM(uc_regSel.origem)) == 'TD'
                uv_numseq = uf_gerais_getUmValor("ft", "ISNULL(MAX(fno),1)", "ndoc = " + ASTR(uc_regSel.ndoc) + " AND ftano = YEAR(GETDATE())")
                
            CASE UPPER(ALLTRIM(uc_regSel.origem)) == 'TS'
                uv_numseq = uf_gerais_getUmValor("bo", "ISNULL(MAX(obrano),1)", "ndos = " + ASTR(uc_regSel.ndoc) + " AND boano = YEAR(GETDATE())")

            OTHERWISE
                uv_numseq = uf_gerais_getUmValor("re", "ISNULL(MAX(rno),1)", "ndoc = " + ASTR(uc_regSel.ndoc) + " AND reano = YEAR(GETDATE())")

        ENDCASE

        
        uv_newStamp = uf_gerais_stamp()
        uv_numCertif = STRTRAN(uf_gerais_getParameter("ADM0000000078", "TEXT"), "/AT", "")

        TEXT TO msel TEXTMERGE NOSHOW
            (
                '<<ALLTRIM(uv_newStamp)>>','<<ALLTRIM(uv_token)>>', '<<ASTR(uc_regSel.ndoc)>>', 'N', '<<ALLTRIM(uc_regSel.classe)>>', '<<ALLTRIM(uc_regSel.tipoSaft)>>', <<ASTR(uv_numSeq)>>, CONVERT(date, GETDATE()),
                <<ALLTRIM(uv_numCertif)>>, 'PI', GETDATE(), '<<ALLTRIM(m_chinis)>>', GETDATE(), '<<ALLTRIM(m_chinis)>>', '<<ALLTRIM(uc_regSel.origem)>>'
            )
        ENDTEXT

        uv_insert = uv_insert + IIF(!EMPTY(uv_insert), ',', '') + ALLTRIM(msel)

    ENDSCAN

    regua(1,2,"A registar s�ries...")

    IF !EMPTY(uv_insert)

        TEXT TO lcSql TEXTMERGE NOSHOW
            INSERT INTO req_regTD
            VALUES
            <<ALLTRIM(uv_insert)>>
        ENDTEXT


        IF !uf_gerais_actGrelha("", "", lcSql)
            uf_perguntalt_chama("ERRO A GERAR O PEDIDO DE REGISTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
            uf_regSeriesAT_criarCursores()
            regua(2)
            RETURN .F.
        ENDIF

        regua(1,3,"A registar s�ries...")

        IF DIRECTORY(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\DT')

            lcWsPath = "javaw -jar "
            lcWsPath = lcWsPath + ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\DT\ATDocTransporte.jar "' + ALLTRIM(uv_token) + '" "' + ALLTRIM(sql_db) + '" "' + ALLTRIM(uv_token) + '" "' + ALLTRIM(mySite) + '" "wsseries"'

            oWSShell = CREATEOBJECT("WScript.Shell")
            oWSShell.Run(lcWsPath,1,.T.)
            
        ELSE
            uf_perguntalt_chama("N�O FOI POSS�VEL REGISTAR AS S�RIES. POR FAVOR CONTACTE O SUPORTE","OK","",16)
            uf_regSeriesAT_criarCursores()
            regua(2)
            RETURN .F.
        ENDIF

        regua(1,4,"A registar s�ries...")

        TEXT TO msel TEXTMERGE NOSHOW
            SELECT * FROM resp_regTD(nolock) WHERE token = '<<ALLTRIM(uv_token)>>'
        ENDTEXT

        IF !uf_gerais_actGrelha("", "uc_resp", msel)
            uf_perguntalt_chama("ERRO A OBTER RESPOSTA AO PEDIDO DE REGISTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
            uf_regSeriesAT_criarCursores()
            regua(2)
            RETURN .F.
        ENDIF

        IF RECCOUNT("uc_resp") = 0
            uf_perguntalt_chama("N�O FOI DEVOLVIDA NENHUMA RESPOSTA.","OK","",16)
            uf_regSeriesAT_criarCursores()
            regua(2)
            RETURN .F.
        ENDIF



        regua(1,5,"A registar s�ries...")

        SELECT uc_resp
        GO TOP

        SCAN

            IF uc_resp.codigo <> 2001
                uf_perguntalt_chama("ERRO " + ASTR(uc_resp.codigo) + ":" + CHR(13) + ALLTRIM(uc_resp.resp),"OK","",16)
                uv_erroReg = .T.
            ENDIF
        
        ENDSCAN

        uf_regSeriesAT_criarCursores()
        regua(2)

		IF !uv_erroReg
	        uf_perguntalt_chama("S�RIES REGISTADOS COM SUCESSO.","OK","",64)
	    ENDIF

    ENDIF

    RETURN .T.

ENDFUNC