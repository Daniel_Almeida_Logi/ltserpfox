

FUNCTION uf_gesttpa_chama
	IF !uf_gesttpa_criarCursores()
        RETURN .F.
    ENDIF

    IF TYPE("GESTTPA")=="U"
		DO FORM GESTTPA
	ELSE
		GESTTPA.show
	ENDIF
ENDFUNC


FUNCTION uf_gesttpa_criarCursores

    IF WEXIST("GESTTPA")

        loGrid = gesttpa.grdTpa
        lcCursor = "uc_tpaGest"

	    lnColumns = loGrid.ColumnCount
	
	    IF lnColumns > 0

		    DIMENSION laControlSource[lnColumns]
		    FOR lnColumn = 1 TO lnColumns
			    laControlSource[lnColumn] = loGrid.Columns[lnColumn].ControlSource
		    ENDFOR
	    ENDIF

        loGrid.RecordSource = ""

    ENDIF

    TEXT TO lcSql TEXTMERGE NOSHOW
        EXEC up_gerais_buscaTpa '<<ALLTRIM(mySite)>>'
    ENDTEXT

    IF !uf_gerais_actGrelha("", "uc_tpaGest", lcSql)
        uf_perguntalt_chama("N�O FOI POSS�VEL CARREGAR OS TPA's. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
        RETURN .F.
    ENDIF

    IF WEXIST("gesttpa")
        loGrid.RecordSource = lcCursor
        
        FOR lnColumn = 1 TO lnColumns
            loGrid.Columns[lnColumn].ControlSource = laControlSource[lnColumn]
        ENDFOR
    ENDIF
    
    SELECT uc_tpaGest
    GO TOP 
    
	RETURN .T.
ENDFUNC 

FUNCTION uf_gestTpa_testUni
LPARAMETERS lcEstado

	TEXT TO lcSql TEXTMERGE NOSHOW
        EXEC up_gerais_buscaTpa '<<ALLTRIM(mySite)>>'
    ENDTEXT

    IF !uf_gerais_actGrelha("", "uc_tpaGest", lcSql)
        uf_perguntalt_chama("N�O FOI POSS�VEL CARREGAR OS TPA's. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
        RETURN .F.
    ENDIF
    
    SELECT uc_tpaGest
    GO TOP 
    SCAN
    
    	replace uc_tpaGest.sel WITH .T.
    	 
    ENDSCAN
    
	
	IF uf_gerais_compStr(lcEstado, "ABRIR")
		uf_gesttpa_abrirTpa()
	ELSE
		uf_gesttpa_fecharTpa()
	ENDIF
	
ENDFUNC



FUNCTION uf_gesttpa_tpaOperation
	LPARAMETERS lcResponseArray, lcWaitingMessage, lcMessage, lcMessageType, lcAmount, lcTermNo, lcTpaModel, lcTpaVersion, lcTpaSpecVersion, lcTpaIp, lcTpaPort, lcTpaStamp
	

	LOCAL lcWsPath,lcWsParams,lcWsCmd,lcToken , lcTPAParam
	STORE '' TO lcWsPath, lcWsParams, lcToken, lcWsCmd
	STORE '&' TO lcAdd
	&& validate if terminal has tpa
	IF EMPTY(ALLTRIM(lcTpaStamp)) OR  !uf_gerais_isIp(lcTpaIp)         
		RETURN .f.	
	ENDIF
	

	&&maximo a pagar
	
	IF(!EMPTY(lcAmount))
		IF(lcAmount>50000)
			uf_perguntalt_chama("Valor m�ximo ultrapassado.","OK","",16)
			RETURN .f.	
		ENDIF
	ENDIF
	

	IF (ALLTRIM(LcMessageType)=="app.msg.purchase.operation")
		IF(EMPTY(lcAmount))
			uf_perguntalt_chama("Tem de definir um valor a pagar","OK","",16)
			RETURN .f.	
		ENDIF	
	ENDIF

	regua(0,4,lcWaitingMessage)
	lcToken = uf_gerais_stamp()
	lcTPAParam = uf_gerais_getParameter_site('ADM0000000047', 'BOOL', mySite)
	IF (lcTPAParam == .f.)		
		lcNomeJar = 'TPACli.jar'	
		lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsTpa\' + ALLTRIM(lcNomeJar)
		lcWsDir	= ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsTpa'
		&& validate path to file
		IF !FILE(lcWsPath)
			uf_perguntalt_chama("FICHEIRO DE CONFIGURA��O N�O ENCONTRADO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			regua(2)
			RETURN .f.
		ENDIF 
		

		&&construct param to java
		
		
		regua(1,3,lcWaitingMessage)
			
		
		lcWsParams = ' "--TOKEN=' 						+  ALLTRIM(lcToken)  	  	 	 + ["] +;
					 ' "--MSGTYPE=' 					+  ALLTRIM(lcMessageType) 	 	 + ["] +; 
					 ' "--MSG=' 						+  ALLTRIM(lcMessage) 	  	 	 + ["] +;
					 ' "--TERMINAL=' 					+  ALLTRIM(STR(lcTermNo))	 	 + ["] +;
					 ' "--IDCL=' 						+  ALLTRIM(uCrsE1.id_lt)   	 	 + ["] +; 
					 ' "--AMOUNT=' 						+  ALLTRIM(PADL(lcAmount,20))	 + ["] +; 
					 ' "--TPAMODEL=' 					+  ALLTRIM(lcTpaModel) 	  	 	 + ["] +; 
					 ' "--TPAVERSION=' 					+  ALLTRIM(lcTpaVersion)  	 	 + ["] +; 
					 ' "--TPASPECIFICATION_VERSION=' 	+  ALLTRIM(lcTpaSpecVersion) 	 + ["] +; 
					 ' "--TPAIP=' 						+  ALLTRIM(lcTpaIp) 		 	 + ["] +; 
					 ' "--TPAPORT=' 					+  ALLTRIM(STR(lcTpaPort))   	 + ["] +;
					 ' "--WAINTING_MESSAGE=' 			+  ALLTRIM(lcWaitingMessage)   	 + ["] 
					 
		
		regua(2)
		
		
		
		
		&&execute
		&&cmd /c cd C:\Logitools\Versoes\15_13_58_02\ltsTpa\ & javaw.exe -jar C:\Logitools\Versoes\15_13_58_02\ltsTpa\TPACli.jar
		 
		&&lcWsCmd = " javaw -jar " + lcWsPath + " " + lcWsParams
	    lcWsCmd = "cmd /c cd /d " + lcWsDir+ " "+  lcAdd + lcAdd + " javaw -Dfile.encoding=UTF-8  -jar " + lcWsPath + " " + lcWsParams 
		
		oWSShell = CREATEOBJECT("WScript.Shell")
		oWSShell.Run(lcWsCmd, 0, .t.)
	
	ELSE 
		uf_tpa_sendTPAOperation (ALLTRIM(lcToken),ALLTRIM(lcMessageType),ALLTRIM(lcMessage),ALLTRIM(PADL(lcAmount,20)),ALLTRIM(STR(lcTermNo)),LOWER(ALLTRIM(uCrsE1.id_lt)),ALLTRIM(lcTpaIp),ALLTRIM(STR(lcTpaPort)),ALLTRIM(lcTpaVersion),ALLTRIM(lcTpaModel),ALLTRIM(lcTpaSpecVersion),ALLTRIM(lcWaitingMessage))
		regua(2)
	ENDIF 
	&&regua(1,4,lcWaitingMessage)
	
	
	&& resultado to pedido
	TEXT TO lcSQL NOSHOW TEXTMERGE
		EXEC [dbo].[up_tpa_resultadoOp]  '<<ALLTRIM(lcToken)>>'
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "uCrsResOp", lcSQL)
		uf_perguntalt_chama("N�o foi poss�vel comunicar com a base de dados. Por favor contacte o suporte." ,"OK","",32)
		regua(2)
		fecha("uCrsResOp")
		RETURN .f.
	ELSE
		IF(RECCOUNT("uCrsResOp")==0)	
			uf_perguntalt_chama("N�o foi poss�vel obter os dados. Por favor contacte o suporte." ,"OK","",32)
			regua(2)
			RETURN .f.
		ENDIF		
	ENDIF
	
	&&regua(1,4,lcWaitingMessage)

	
	uf_tpa_processResponse(lcMessageType,@lcResponseArray)


	fecha("uCrsResOp")

	&&regua(2)
	
	RETURN lcResponseArray
	

ENDFUNC





FUNCTION uf_gesttpa_estadoTpa
LPARAMETERS lcEstado , lcTermNo, lcTpaModel, lcTpaVersion, lcTpaSpecVersion,lcTpaIp, lcTpaPort, lcTpaStamp
	RELEASE lcresponsearray
	DIMENSION lcresponsearray(2)
	
	uf_gesttpa_tpaOperation(@lcresponsearray, "A validar estado TPA...", "", "app.msg.status.tpa", 0, lcTermNo, lcTpaModel, lcTpaVersion, lcTpaSpecVersion,lcTpaIp, lcTpaPort, lcTpaStamp)
	IF ( .NOT. EMPTY(lcresponsearray(2))) AND INLIST(ALLTRIM(UPPER(lcresponsearray(2))), "ABERTO", "FECHADO")
		IF uf_gerais_compstr(lcresponsearray(2), lcEstado)
		
			RETURN 1
		ELSE
			RETURN 0
		ENDIF 
	ELSE
		uf_perguntalt_chama("N�o foi obtida resposta do TPA. Por favor contacte o suporte.","OK","",16)
		RETURN -1
	ENDIF
	RELEASE lcresponsearray
ENDFUNC




FUNCTION uf_gesttpa_fecharTpa 
	
	LOCAL lcTermNo, lcTpaModel, lcTpaVersion, lcTpaSpecVersion, lcTpaIp, lcTpaPort, lcTpaStamp, lcTpaEstado 
	
	IF !USED("uc_tpaGest")
		RETURN .F.
	ENDIF 
	
	SELECT uc_tpaGest
	GO TOP 
	SCAN FOR uc_tpaGest.sel 
		lcTermNo 			= uc_tpaGest.termNo 
		lcTpaModel 			= uc_tpaGest.model
		lcTpaVersion 		= uc_tpaGest.version
		lcTpaSpecVersion	= uc_tpaGest.termSpecVersion
		lcTpaIp				= uc_tpaGest.tpa
		lcTpaPort			= uc_tpaGest.port
		lcTpaStamp			= uc_tpaGest.tpaStamp
		
		lcTpaEstado = uf_gesttpa_estadoTpa("aberto", lcTermNo, lcTpaModel, lcTpaVersion, lcTpaSpecVersion,lcTpaIp, lcTpaPort, lcTpaStamp)
		
		IF lcTpaEstado == 1
			RELEASE lcresponsearray
			DIMENSION lcresponsearray(2)
		 	LOCAL msgtype, msgwait
		 	msgtype = "app.msg.accounting.period.close"
		    msgwait = "A fechar sess�o do Tpa..."
		 
			uf_gesttpa_tpaOperation(@lcresponsearray, msgwait, "", msgtype, 0, lcTermNo, lcTpaModel, lcTpaVersion, lcTpaSpecVersion,lcTpaIp, lcTpaPort, lcTpaStamp)
			IF (lcresponsearray(1)!=0)
			    uf_perguntalt_chama(uf_tpa_messagemresposta(ALLTRIM(lcresponsearray(2))), "OK", "", 16)
			ELSE 
			 	uf_perguntalt_chama( uc_tpaGest.terminal + "fechado com sucesso.", "OK", "", 16)
			ENDIF
		ENDIF 
	ENDSCAN
ENDFUNC 


FUNCTION uf_gesttpa_abrirTpa
	
	
	LOCAL lcTermNo, lcTpaModel, lcTpaVersion, lcTpaSpecVersion, lcTpaIp, lcTpaPort, lcTpaStamp, lcTpaEstado
	
	IF !USED("uc_tpaGest")
		RETURN .F.
	ENDIF 
	
	
	SELECT uc_tpaGest
	GO TOP 
	SCAN FOR uc_tpaGest.sel 
		lcTermNo 			= uc_tpaGest.termNo 
		lcTpaModel 			= uc_tpaGest.model
		lcTpaVersion 		= uc_tpaGest.version
		lcTpaSpecVersion	= uc_tpaGest.termSpecVersion
		lcTpaIp				= uc_tpaGest.tpa
		lcTpaPort			= uc_tpaGest.port
		lcTpaStamp			= uc_tpaGest.tpaStamp
		
		lcTpaEstado = uf_gesttpa_estadoTpa("aberto", lcTermNo, lcTpaModel, lcTpaVersion, lcTpaSpecVersion,lcTpaIp, lcTpaPort, lcTpaStamp)
	
		IF lcTpaEstado == 0
			RELEASE lcresponsearray
			DIMENSION lcresponsearray(2)
		 	LOCAL msgtype, msgwait
		 	msgtype = "app.msg.accounting.period.open"
		    msgwait = "A abrir sess�o do Tpa..."
		 
			uf_gesttpa_tpaOperation(@lcresponsearray, msgwait, "", msgtype, 0, lcTermNo, lcTpaModel, lcTpaVersion, lcTpaSpecVersion,lcTpaIp, lcTpaPort, lcTpaStamp)
			IF (lcresponsearray(1)!=0)
			    uf_perguntalt_chama(uf_tpa_messagemresposta(ALLTRIM(lcresponsearray(2)))+ uc_tpaGest.terminal, "OK", "", 16)
			 ELSE 
			 	uf_perguntalt_chama( uc_tpaGest.terminal + "aberto com sucesso.", "OK", "", 16)
			 ENDIF
		ENDIF 
	ENDSCAN
	
ENDFUNC 



FUNCTION uf_gesttpa_sair

	IF TYPE("GESTTPA")=="U"
		RETURN .F.
	ENDIF
	
	fecha("uc_tpaGest")
	
	GESTTPA.hide
	GESTTPA.release
ENDFUNC  
