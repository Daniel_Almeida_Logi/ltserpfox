FUNCTION uf_ESCALOESPV_chama
	LPARAMETERS lcStamp
		
	** cria cursor vazio
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select * from Escaloespv WHERE 0=1 
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "uCrsEscaloespv", lcSql)
		uf_perguntalt_chama("ERRO AO LER A TABELA DE PV'S!","OK","",16)
		RETURN .F.
	ENDIF 
	
	&& Controla Abertura do Painel			
	if type("ESCALOESPV")=="U"
		DO FORM ESCALOESPV
	ELSE
		ESCALOESPV.show
	ENDIF
	
	** carrega dados
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select * from Escaloespv ORDER BY valinf
	ENDTEXT 	
	IF !uf_gerais_actgrelha("ESCALOESPV.GridPesq", "uCrsEscaloespv", lcSql)
		uf_perguntalt_chama("ERRO AO LER A TABELA DE PV'S!","OK","",16)
		RETURN .F.
	ENDIF 
	myESCALOESPVIntroducao=.F.
	myESCALOESPVAlteracao=.F.
	uf_ESCALOESPV_alternaMenu()
ENDFUNC


**
FUNCTION uf_ESCALOESPV_carregamenu

	ESCALOESPV.menu1.adicionaopcao("atualizar", "Atualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_ESCALOESPV_Pesquisa", "F2")
	ESCALOESPV.menu1.adicionaOpcao("editar", "Editar", myPath + "\imagens\icons\lapis_w.png", "uf_ESCALOESPV_editar","F5")
	ESCALOESPV.menu1.adicionaopcao("novaLinha","Nova Linha",myPath + "\imagens\icons\mais_w.png","uf_ESCALOESPV_addLinha","")
	ESCALOESPV.menu1.adicionaopcao("eliminarLinha","Eliminar Linha",myPath + "\imagens\icons\cruz_w.png","uf_ESCALOESPV_remLinha","")
	
ENDFUNC


**
FUNCTION uf_ESCALOESPV_Pesquisa
	LOCAL lcSQL
	
	regua(0,0,"A atualizar...")
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select * from Escaloespv ORDER BY valinf
	ENDTEXT

	uf_gerais_actGrelha("ESCALOESPV.GridPesq", "uCrsEscaloespv", lcSQL)

	regua(2)
	
	ESCALOESPV.Refresh
	
ENDFUNC

**
FUNCTION uf_ESCALOESPV_sair
	IF myESCALOESPVIntroducao OR myESCALOESPVAlteracao
		myESCALOESPVAlteracao=.f.
		uf_ESCALOESPV_alternaMenu()
	ELSE
		**Fecha Cursores
		IF USED("uCrsEscaloespv")
			fecha("uCrsEscaloespv")
		ENDIF 
	
		ESCALOESPV.hide
		ESCALOESPV.release
	ENDIF 
ENDFUNC

FUNCTION uf_ESCALOESPV_editar
	myESCALOESPVAlteracao=.t.
	uf_ESCALOESPV_alternaMenu()
ENDFUNC 

FUNCTION uf_ESCALOESPV_alternaMenu
	IF myESCALOESPVIntroducao OR myESCALOESPVAlteracao
		ESCALOESPV.menu1.estado("", "", "Gravar", .t., "Cancelar", .t.)
		ESCALOESPV.menu1.estado("atualizar, editar", "HIDE")
		ESCALOESPV.menu1.estado("novaLinha, eliminarLinha", "SHOW")
		ESCALOESPV.GridPesq.readonly=.f.
	ELSE
		ESCALOESPV.menu1.estado("", "", "Gravar", .f., "Sair", .t.)
		ESCALOESPV.menu1.estado("atualizar, editar", "SHOW")
		ESCALOESPV.menu1.estado("novaLinha, eliminarLinha", "HIDE")
		ESCALOESPV.GridPesq.readonly=.t.
	ENDIF 
ENDFUNC

FUNCTION uf_ESCALOESPV_addLinha
	SELECT uCrsEscaloespv
	APPEND BLANK 
	ESCALOESPV.GridPesq.refresh
ENDFUNC 

FUNCTION uf_ESCALOESPV_remLinha
	ESCALOESPV.GridPesq.SetFocus
	SELECT uCrsEscaloespv
	DELETE  
	ESCALOESPV.GridPesq.refresh
ENDFUNC 

FUNCTION uf_ESCALOESPV_gravar

	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		delete from Escaloespv  
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("ERRO AO ELIMINAR OS DADOS DA TABELA DE TABELAS DE PV'S !","OK","",16)
		RETURN .F.
	ENDIF 
	SELECT uCrsEscaloespv
	GO TOP
	SCAN
		lcSQL = ""
		IF EMPTY(uCrsEscaloespv.escaloespvstamp)
			TEXT TO lcSQL TEXTMERGE NOSHOW
				insert into escaloespv (escaloespvstamp, valinf, valsup, mga, mgf, fee, pvpmax, ousrinis, ousrdata, usrinis, usrdata)
				select left(newid(),21), <<uCrsEscaloespv.valinf>>, <<uCrsEscaloespv.valsup>>, <<uCrsEscaloespv.mga>>, <<uCrsEscaloespv.mgf>>, <<uCrsEscaloespv.fee>>, <<uCrsEscaloespv.pvpmax>>, '<<m_chinis>>', dateadd(HOUR, <<difhoraria>>, getdate()), '<<m_chinis>>', dateadd(HOUR, <<difhoraria>>, getdate())
			ENDTEXT 
		ELSE
			TEXT TO lcSQL TEXTMERGE NOSHOW
				insert into escaloespv (escaloespvstamp, valinf, valsup, mga, mgf, fee, pvpmax, ousrinis, ousrdata, usrinis, usrdata)
				select '<<uCrsEscaloespv.escaloespvstamp>>', <<uCrsEscaloespv.valinf>>, <<uCrsEscaloespv.valsup>>, <<uCrsEscaloespv.mga>>, <<uCrsEscaloespv.mgf>>, <<uCrsEscaloespv.fee>>, <<uCrsEscaloespv.pvpmax>>, '<<uCrsEscaloespv.ousrinis>>', '<<dtosql(uCrsEscaloespv.ousrdata)>>', '<<m_chinis>>', dateadd(HOUR, <<difhoraria>>, getdate())
			ENDTEXT 
		ENDIF 
		
		IF !uf_gerais_actgrelha("", "", lcSql)
			uf_perguntalt_chama("ERRO AO INSERIR OS DADOS DA TABELA DE TABELAS DE PV'S !","OK","",16)
			RETURN .F.
		ENDIF 
	ENDSCAN 
ENDFUNC 