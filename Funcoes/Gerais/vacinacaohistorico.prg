FUNCTION uf_vacinacaoHistorico_chama
	LPARAMETERS uv_nrUtente
		
	IF !uf_vacinacaoHistorico_getHist(uv_nrUtente)
		RETURN .F.
	ENDIF
	
	
	if(!USED("UCRSREGISTRATIONS"))
		uf_perguntalt_chama("N�o foram devolvidos dados de hist�rico do utente.","OK","",16)
		RETURN .F.
	ENDIF
	
	IF WEXIST("VacinacaoHistorico")
		uf_vacinacaoHistorico_sair()
	ENDIF

	DO FORM VacinacaoHistorico
	
ENDFUNC

FUNCTION uf_vacinacaoHistorico_sair
	
	uf_vacinacaoHistorico_fechaCursores()
		
	VacinacaoHistorico.hide
	VacinacaoHistorico.release
	RELEASE VacinacaoHistorico
ENDFUNC

FUNCTION uf_vacinacaoHistorico_getNomeNumUtente
	
	LOCAL lcSQLUtente
	STORE '' TO lcSQLUtente
	
	IF (!USED("uCrsVacinacao") OR EMPTY(uCrsVacinacao.nrSns))
		uf_perguntalt_chama("Preencha o n�mero do SNS no painel da vacina��o.","OK","",16)
		RETURN .F.
	ENDIF
	
	TEXT TO lcSQLUtente TEXTMERGE NOSHOW
		SELECT TOP 1 nome, no, estab FROM b_utentes(nolock) WHERE nbenef = '<<uCrsVacinacao.nrSns>>' AND inactivo = 0
	ENDTEXT
	
	IF !(uf_gerais_actGrelha("","ucrsUtenteVacina",lcSQLUtente))
		uf_perguntalt_chama("Ocorreu um erro a obter os dados do utente. Por favor contacte o suporte.","OK","",16)
		RETURN .F.
	ENDIF
	
	RETURN .T.
ENDFUNC

FUNCTION uf_vacinacaoHistorico_fechaCursores

	IF(USED("UCRSREGISTRATIONS"))
		fecha("UCRSREGISTRATIONS")
	ENDIF

ENDFUNC

FUNCTION uf_vacinacaoHistorico_getHist
	LPARAMETERS uv_nrUtente

	IF EMPTY(uv_nrUtente)
		uf_perguntalt_chama("Tem de preencher o N� de Utente.", "OK", "", 48)
		RETURN .F.
	ENDIF

	LOCAL lcSQLInsert, lcToken, lcSearch, lcProfId, lcProfName, lcCode, lcSenderAssoc, lcSenderAssocId, lcSenderId, lcSenderName, lcTeste, lcSite, lcSigla
	STORE '' TO lcSQLInsert, lcToken, lcSearch, lcProfId, lcProfName, lcCode, lcSenderAssoc, lcSenderAssocId, lcSenderId, lcSenderName, lcSite, lcSigla
	
	IF uf_gerais_getparameter("ADM0000000227", "BOOL")
		lcteste = 1
	ELSE
		lcteste = 0
	ENDIF
	
	lcSearch = uv_nrUtente
	
	&& n�mero da cedula
	IF uf_gerais_checkCedula()
		lcProfId  = uf_gerais_removeAllLeterFromString(ucrsuser.drcedula)
    ENDIF
    
	SELECT ucrsuser

    && professional nome
	lcProfName = ucrsuser.nome &&Vacinacao.txtAdministrante.value
	IF(EMPTY(lcProfName))
		IF EMPTY(ucrsuser.nome)   
      		uf_perguntalt_chama("Tem de preencher o campo 'Nome' na ficha do utilizador para utilizar esta funcionalidade.", "OK", "", 48)
      		RETURN .F.
      	ELSE
      		lcProfName = ALLTRIM(ucrsuser.nome)
    	ENDIF
	ENDIF
	
	&& iniciais do utilizador	
	IF(EMPTY(ucrsuser.iniciais))
		lcSigla = ""
    ELSE
    	lcSigla = ALLTRIM(ucrsuser.iniciais)
	ENDIF
	
	SELECT ucrse1
	GO TOP
	&& associacao
	IF(EMPTY(ucrse1.u_assfarm))
	    uf_perguntalt_chama("Tem de preencher o campo 'Associa��o' na ficha da empresa para utilizar esta funcionalidade.", "OK", "", 48)
    	RETURN .F.
	ELSE
		lcSenderAssoc = ucrse1.u_assfarm
	ENDIF
	
	&& codigo da associacao
	IF(EMPTY(ucrse1.u_codfarm))
	    uf_perguntalt_chama("Tem de preencher o campo 'N� da Associa��o' na ficha da empresa para utilizar esta funcionalidade.", "OK", "", 48)
    	RETURN .F.
	ELSE
		lcSenderAssocId = ucrse1.u_codfarm
	ENDIF
	
	&& id da base dados
	IF(EMPTY(ucrse1.id_lt))
	    uf_perguntalt_chama("Erro ao obter informa��es da base de dados. Por favor Contacte o suporte.", "OK", "", 48)
    	RETURN .F.
	ELSE
		lcSenderId = ucrse1.id_lt
	ENDIF
	
	&& nome da empresa
	IF(EMPTY(ucrse1.nomecomp))
	    uf_perguntalt_chama("Tem de preencher o campo 'Nome da empresa' na ficha da empresa para utilizar esta funcionalidade.", "OK", "", 48)
    	RETURN .F.
	ELSE
		lcSenderName = ucrse1.nomecomp
	ENDIF
	
	&& loja
	IF(EMPTY(mySite))
		uf_perguntalt_chama("Erro ao obter loja. Por favor Contacte o suporte.", "OK", "", 48)
    	RETURN .F.
	ELSE
		lcSite = mySite
	ENDIF

	lcToken = uf_gerais_gerarIdentifiers(36)

	lcBreakStamp = ""
	lcSaveStamp = ""
		
	TEXT TO lcSQLInsert NOSHOW TEXTMERGE 
		exec up_save_sendVaccine '<<ALLTRIM(lcToken)>>', '<<ALLTRIM(lcSearch)>>', '<<ALLTRIM(lcProfId)>>', '<<ALLTRIM(lcProfName)>>', '<<UPPER(ALLTRIM(lcCode))>>', '<<ALLTRIM(lcSenderAssoc)>>', '<<ALLTRIM(lcSenderAssocId)>>', '<<ALLTRIM(lcSenderId)>>', '<<ALLTRIM(lcSenderName)>>', 1, 'SPMS', 2, 'vaccinesCalendar', <<lcTeste>>, '<<ALLTRIM(lcSite)>>', '<<ALLTRIM(lcSaveStamp)>>', '<<ALLTRIM(lcBreakStamp)>>', '<<ALLTRIM(lcSigla)>>',
		'', 0, 0, 0, 0
	ENDTEXT
	
	IF !uf_gerais_actGrelha("", "", lcSQLInsert)
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF


	LOCAL lcWsPath,lcWsParams,lcWsCmd
	STORE '' TO lcWsPath, lcWsParams, lcWsCmd
	STORE '&' TO lcAdd

	lcWaitingMessage = "A comunicar com servi�o..."
	
	regua(0,7,lcWaitingMessage)
	
	lcNomeJar = 'ltsVaccineCli.jar'	
	lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsVaccineCli\' + ALLTRIM(lcNomeJar)
	lcWsDir	= ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsVaccineCli'
	
	&& validate path to file
	IF !FILE(lcWsPath)
		uf_perguntalt_chama("FICHEIRO DE CONFIGURA��O N�O ENCONTRADO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		regua(2)
		RETURN .f.
	ENDIF 
				
	regua(1,3,lcWaitingMessage)
	
	lcWsParams = ' "--idCl=' 	+  ALLTRIM(lcSenderId) 	+ ["] +; 
				 ' "--TOKEN=' 		+  ALLTRIM(lcToken) + ["] 
				 				 
	regua(1,5,lcWaitingMessage)

	&&execute
    lcWsCmd = "cmd /c cd /d " + lcWsDir+ " "+  lcAdd + lcAdd + " javaw -Dfile.encoding=UTF-8  -jar " + lcWsPath + " " + lcWsParams 
	
	IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.T.
	   	uf_perguntalt_chama("Webservice de teste...", "OK", "", 64)
	    _CLIPTEXT = lcWsCmd
	ENDIF

	oWSShell = CREATEOBJECT("WScript.Shell")
	oWSShell.Run(lcWsCmd, 0, .t.)
	
	regua(1,7,lcWaitingMessage)

	LOCAL lcSQLRegistration
	STORE "" TO lcSQLRegistration
	
	IF(USED("ucrsRegistrations"))
		fecha("ucrsRegistrations")
	ENDIF
	
	LOCAL lcRespToken
	
	**lcRespToken = uf_gerais_getUmValor("sendVaccineResp", "ISNULL(registrationsToken, '')", "token = '" + lcToken + "'")

	uf_vacinacao_createCursor_sendVaccineResp(lcToken)

	IF USED("ucrsSendVaccineResp") AND RECCOUNT("ucrsSendVaccineResp")> 0
		SELECT ucrsSendVaccineResp
		IF(ucrsSendVaccineResp.statusCode <> 200)
			SELECT ucrsSendVaccineResp
			uf_perguntalt_chama(ALLTRIM(ucrsSendVaccineResp.statusCodeDesc), "OK", "", 48)
			regua(2)
			RETURN .F.
		ENDIF
	ELSE

		uf_perguntalt_chama("N�o foi obtida resposta do servi�o." + chr(13) + "Por favor contacte o suporte.", "OK", "", 48)
		RETURN .F.

	ENDIF
	
	SELECT ucrsSendVaccineResp
	TEXT TO lcSQLRegistration TEXTMERGE NOSHOW
		exec up_get_registrations '<<ucrsSendVaccineResp.registrationsToken>>'
	ENDTEXT

	IF !(uf_gerais_actGrelha("","ucrsRegistrations",lcSQLRegistration))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF

	SELECT ucrsSendVaccineResp
    TEXT TO lcSQLRegistration TEXTMERGE NOSHOW
        EXEC up_get_adverseReactions '<<ucrsSendVaccineResp.registrationsToken>>'
    ENDTEXT

	IF !(uf_gerais_actGrelha("","ucrsAdverseReactions",lcSQLRegistration))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE REAC��ES ADVERSAS! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF

    SELECT ucrsRegistrations
    GO TOP

    uf_vacinacaoHistorico_filtraAdverseReactions()
	
	regua(2)

	RETURN .T.

ENDFUNC

PROCEDURE uf_vacinacaoHistorico_filtraAdverseReactions

    SELECT ucrsRegistrations

    SELECT ucrsAdverseReactions

    SET FILTER TO uf_gerais_compStr(ucrsAdverseReactions.token, ucrsRegistrations.adverseReactionsToken)

    SELECT ucrsAdverseReactions
    GO TOP

    IF WEXIST("VacinacaoHistorico")
        VacinacaoHistorico.REFRESH()
    ENDIF

ENDPROC