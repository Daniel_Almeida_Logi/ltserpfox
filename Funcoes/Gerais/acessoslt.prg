

*** Classes Linhas ***
** Define class para os eventos **
DEFINE CLASS pacessoslt As Custom
	
	PROCEDURE up_acessoslt_clickPf
		
		SELECT uCrsGrupUt
		DO CASE
			CASE UPPER(ALLTRIM(uCrsGrupUt.tipo)) == "UTILIZADOR" 

				LOCAL lcSite, lcUserno,lcPerfil,lcValor,lcUserno, lcStamp
				
				lcSite = ALLTRIM(ACESSOSLT.gridPesq.columns(ACESSOSLT.gridPesq.ActiveColumn).header1.caption)
				lcStamp = ALLTRIM(uCrsGrupUt.stamp)
				lcUsername = ALLTRIM(uCrsGrupUt.descricao)
				lcUserno = uCrsGrupUt.Userno
				lcPfstamp = ALLTRIM(ucrsPerfLt.pfstamp)
				lcPerfil = ALLTRIM(ucrsPerfLt.resumo)
				lcValor = ACESSOSLT.gridPesq.columns(ACESSOSLT.gridPesq.ActiveColumn).Check1.value
	
				IF !EMPTY(lcValor)

					lcSQL = ""
					TEXT To lcSQL TEXTMERGE NOSHOW
						Insert into b_pfu (pfustamp,pfstamp,usstamp,username,userno,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,site)
						values  (
								'ADM' + left(NEWID(),20)
								,'<<lcpfstamp>>'
								,'<<lcStamp>>'
								,'<<lcUsername>>'
								,'<<lcUserno>>'
								,'ADM'
								,dateadd(HOUR, <<difhoraria>>, getdate())
								,CONVERT(TIME,dateadd(HOUR, <<difhoraria>>, getdate()))
								,'ADM'
								,dateadd(HOUR, <<difhoraria>>, getdate())
								,CONVERT(TIME,dateadd(HOUR, <<difhoraria>>, getdate()))
								,'<<lcSite>>'
								)
					ENDTEXT 
				ELSE
					
					TEXT To lcSQL TEXTMERGE NOSHOW
						DELETE 
						from  	b_pfu 
						Where   pfstamp = '<<lcpfstamp>>'
								and b_pfu.username = '<<lcUsername>>'
								and site = '<<lcSite>>'
					ENDTEXT 
				ENDIF 	

				IF !uf_gerais_actgrelha("", "", lcSQL)
					uf_perguntalt_chama("N�O FOI POSS�VEL ATUALIZAR O PERFIL. POR FAVOR CONTACTE O SUPORTE.","OK","",64)
					RETURN .f.
				ENDIF

			CASE UPPER(ALLTRIM(uCrsGrupUt.tipo)) == "GRUPO" 
				
				LOCAL lcSite, lcUserno,lcPerfil,lcValor,lcUserno, lcStamp
				
				lcSite = ALLTRIM(ACESSOSLT.gridPesq.columns(ACESSOSLT.gridPesq.ActiveColumn).header1.caption)
				lcStamp = ALLTRIM(uCrsGrupUt.stamp)
				lcUsername = ALLTRIM(uCrsGrupUt.descricao)
				lcUserno = uCrsGrupUt.Userno
				lcPfstamp = ALLTRIM(ucrsPerfLt.pfstamp)
				lcPerfil = ALLTRIM(ucrsPerfLt.resumo)
				lcValor = ACESSOSLT.gridPesq.columns(ACESSOSLT.gridPesq.ActiveColumn).Check1.value
	
				IF !EMPTY(lcValor)

					lcSQL = ""
					TEXT To lcSQL TEXTMERGE NOSHOW
						Insert into b_pfg (pfgstamp,pfstamp,ugstamp,nome,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,site)
						values  (
								'ADM' + left(NEWID(),20)
								,'<<lcpfstamp>>'
								,'<<lcStamp>>'
								,'<<lcUsername>>'
								,'ADM'
								,dateadd(HOUR, <<difhoraria>>, getdate())
								,CONVERT(TIME,dateadd(HOUR, <<difhoraria>>, getdate()))
								,'ADM'
								,dateadd(HOUR, <<difhoraria>>, getdate())
								,CONVERT(TIME,dateadd(HOUR, <<difhoraria>>, getdate()))
								,'<<lcSite>>'
								)
					ENDTEXT 
				ELSE
					
					TEXT To lcSQL TEXTMERGE NOSHOW
						DELETE 
						from 
							b_pfg 
						Where   
							pfstamp = '<<lcpfstamp>>'
							And b_pfg.nome = '<<lcUsername>>'
							and site = '<<lcSite>>'
					ENDTEXT 
				ENDIF 	
				IF !uf_gerais_actgrelha("", "", lcSQL)
					uf_perguntalt_chama("N�O FOI POSS�VEL ATUALIZAR O PERFIL. POR FAVOR CONTACTE O SUPORTE.","OK","",64)
					RETURN .f.
				ENDIF
		ENDCASE
	ENDPROC
	
ENDDEFINE 


**
FUNCTION uf_acessoslt_chama
	Local lcSQL, lcUser, lcGrupo, lcFiltroPerf, lcGpPerf 
	
	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Acessos')
		**
	ELSE
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL GEST�O ACESSOS.","OK","",48)
		RETURN .f.
	ENDIF
	
	**cria cursores
	IF !USED("uc_perfLt")
		CREATE CURSOR uc_perfLt (escolha l, resumo c(50), pfstamp c(25), descricao c(200), u_grupo c(55), u_tipo c(55))
	ENDIF

	** List utilizadores
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		Select 
			tipo = 'Grupo'
			,nome = ''
			,grupo = nome
			,descricao = nome
			,userno = 0
			,stamp = b_ug.ugstamp
		from 
			b_ug (nolock)
			
		union all

		Select 
			tipo = 'Utilizador'
			,nome
			,grupo 
			,descricao = nome
			,userno
			,stamp = b_us.ugstamp
		from 
			b_us (nolock)
		where
			b_us.inactivo = 0
	ENDTEXT 
	
	IF ALLTRIM(m_chinis)!='ADM'
		lcSQL = lcSQL + " and grupo !='Administrador'"
	ENDIF 
	
	lcSQL = lcSQL + " order by grupo, nome"


	IF !uf_gerais_actgrelha("", "uCrsGrupUt", lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DE GRUPOS ASSOCIADOS AOS PERFIS.","OK","",16)
		RETURN .f.
	ENDIF
	
	uf_ACESSOSLT_CarregaDados(.t.)
	
	IF TYPE("ACESSOSLT")=="U"
	
		**Valida Licenciamento	
		IF (uf_gerais_addConnection('ACESSOS') == .f.)
			RETURN .F.
		ENDIF

		DO FORM ACESSOSLT
		
		*** Cria classe para permitir os eventos dinamicos **
		If Type("ACESSOSLT.oCust") # "O"
			ACESSOSLT.AddObject("oCust","pacessoslt")
		ENDIF
		*******************************
		
		TEXT To lcSQL TEXTMERGE NOSHOW
			Select site, site_nr = 'p_' +RTRIM(LTRIM(STR(no))) FROM empresa ORDER BY no
		ENDTEXT 
		
		uf_gerais_actGrelha("","ucrsListSite",lcSQL)
		
		WITH ACESSOSLT.gridPesq
		
			i = .columnCount
			SELECT ucrsListSite
			GO top
			SCAN 
			
				i = i+1
				lcNomeCampo = ALLTRIM(ucrsListSite.site_nr)
				lcControlSource = "ucrsPerfLt." + ALLTRIM(lcNomeCampo)

				.AddColumn 
				.RowHeight = uf_gerais_getRowHeight()
				.columns(i).ControlSource = lcControlSource 
				.columns(i).text1.ControlSource = lcControlSource 
				.columns(i).header1.FontName = "Verdana"
				.columns(i).header1.Fontsize = 9
				.columns(i).header1.Alignment = 2
				.columns(i).FontName = "Verdana"
				.columns(i).Fontsize = 9
				.columns(i).ReadOnly = .t.
				.columns(i).text1.ReadOnly = .t.
				.Columns(i).columnorder = i
									
				.Columns(i).width = 50
				
				.Columns(i).InputMask=""
				.Columns(i).text1.InputMask=""
				.columns(i).header1.caption = ucrsListSite.site
				.columns(i).header1.Alignment = 2
				
				WITH .columns(i)
					.AddObject("Check1","CheckBox")
					.Sparse = .F.
					.CurrentControl = "Check1"
					WITH .Check1
						.Alignment = 2
						.Caption = ""
						.Name = "Check1"
						.DownPicture = myPath + "\imagens\icons\checked_b_16.bmp"
						.Picture = myPath + "\imagens\icons\unchecked_b_16.bmp"
						.themes = .f.
						.style = 1
						.SpecialEffect = 1
						.Visible = .T.
						.readonly = .f.
						
						
					ENDWITH
					.readonly = .f.
					&&
					BindEvent(.check1,"Click",ACESSOSLT.oCust,"up_acessoslt_clickPf")

					
					.RemoveObject("text1")
				ENDWITH 
			ENDSCAN
		ENDWITH
		
		**********************************
		
	ELSE
		ACESSOSLT.show
	ENDIF 
	
	
	
ENDFUNC


** 
FUNCTION uf_ACESSOSLT_CarregaDados
	LPARAMETERS tcBool 
	Local lcSQL, lcUser, lcGrupo, lcFiltroPerf, lcGpPerf 
	Store '' To lcUser, lcGrupo, lcFiltroPerf, lcGpPerf 

	SELECT uCrsGrupUt
	lcUser = uCrsGrupUt.userno
	lcGrupo = uCrsGrupUt.grupo
	
	
	TEXT To lcSQL TEXTMERGE NOSHOW
		Select site, site_nr = 'p_' +RTRIM(LTRIM(STR(no))) FROM empresa ORDER BY no
	ENDTEXT 
	
	uf_gerais_actGrelha("","ucrsListSite",lcSQL)


	lcSQL = ""
	&&			exec up_perfis_Carrega '<<Alltrim(ACESSOSLT.perfil.value)>>','<<Alltrim(ACESSOSLT.grupo.value)>>',<<ACESSOSLT.selecionados.value>>,'<<lcUser>>','<<lcGrupo>>','<<Alltrim(ACESSOSLT.site.value)>>'
	TEXT To lcSQL1 TEXTMERGE NOSHOW
		Select 
			escolha = convert(bit,0)
			,pf.resumo
			,tipo
			,descricao
			,grupo
			,pf.pfstamp
			,left(pf.resumo,100) as resumoIndex
	ENDTEXT 


			
	lcSQL = lcSQL1
	SELECT ucrsListSite
	GO Top
	SCAN FOR !EMPTY(ALLTRIM(ucrsListSite.site))
		
		SELECT uCrsGrupUt
		DO CASE
			CASE UPPER(ALLTRIM(uCrsGrupUt.tipo)) == "UTILIZADOR" 
				TEXT To lcSQL2 TEXTMERGE NOSHOW
					,'<<ALLTRIM(ucrsListSite.site_nr)>>' = Case when exists(select pfstamp  from b_pfu(nolock) pfu where pfu.site = '<<ALLTRIM(ucrsListSite.site)>>' and pfu.username = '<<ALLTRIM(uCrsGrupUt.descricao)>>' and pfu.pfstamp = pf.pfstamp) then CONVERT(bit,1) else CONVERT(bit,0) end
				ENDTEXT 
			CASE UPPER(ALLTRIM(uCrsGrupUt.tipo)) == "GRUPO" 
				TEXT To lcSQL2 TEXTMERGE NOSHOW
					,'<<ALLTRIM(ucrsListSite.site_nr)>>' = Case when exists(select pfstamp from b_pfg(nolock) pfg where pfg.site = '<<ALLTRIM(ucrsListSite.site)>>' and pfg.nome = '<<ALLTRIM(uCrsGrupUt.descricao)>>' and pfg.pfstamp = pf.pfstamp) then CONVERT(bit,1) else CONVERT(bit,0) end
				ENDTEXT 
		ENDCASE			
		lcSQL = lcSQL + chr(13) + lcSQL2 
	ENDSCAN 
	
	IF tcBool == .f.
	
		TEXT To lcSQL3 TEXTMERGE NOSHOW
			From
				b_pf pf
			Where
				resumo like '%<<Alltrim(ACESSOSLT.perfil.value)>>%'
				And grupo = case when '<<Alltrim(ACESSOSLT.grupo.value)>>' = '' then grupo else '<<Alltrim(ACESSOSLT.grupo.value)>>' end
		ENDTEXT 
		lcSQL = lcSQL + chr(13) + lcSQL3
		IF !uf_gerais_actgrelha("ACESSOSLT.gridPesq", "ucrsPerfLt", lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA A CARREGAR DEFINI��ES DE PERFIS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		ENDIF
		
		uf_acessoslt_aplicaFiltros()
	ELSE
	
		TEXT To lcSQL3 TEXTMERGE NOSHOW
			From
				b_pf pf
		ENDTEXT 
		lcSQL = lcSQL + lcSQL3

		IF !uf_gerais_actgrelha("", "ucrsPerfLt", lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA A CARREGAR DEFINI��ES DE PERFIS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		ENDIF
	ENDIF
ENDFUNC 


** Aplica Filtros
FUNCTION uf_acessoslt_aplicaFiltros
	lcFiltro = ""
	lcCursor = "ucrsPerfLt"
	SELECT ucrsPerfLt
	SET FILTER TO 
	
	
	
	IF acessoslt.sel.tag == "true"
		IF !USED("ucrsListSite")
			TEXT To lcSQL TEXTMERGE NOSHOW
				Select site, site_nr = 'p_' +RTRIM(LTRIM(STR(no))) FROM empresa ORDER BY no
			ENDTEXT 
			
			uf_gerais_actGrelha("","ucrsListSite",lcSQL)
		ENDIF 
		
		SELECT ucrsListSite
		GO TOP
		SCAN FOR !EMPTY(site_nr)
			IF EMPTY(lcFiltro)
				lcFiltro = ' ucrsPerfLt.' +  ALLTRIM(ucrsListSite.site_nr) + '== .t.'		
			ELSE
				lcFiltro = lcFiltro + ' OR ucrsPerfLt.' +  ALLTRIM(ucrsListSite.site_nr) + '== .t.'		
			ENDIF 
		ENDSCAN 
	ENDIF

	** aplicar filtro
	SELECT &lcCursor
	GO top
	IF !EMPTY(ALLTRIM(lcFiltro))
		SELECT &lcCursor
		SET FILTER TO &lcFiltro
	ELSE
		SELECT &lcCursor
		SET FILTER TO 
	ENDIF

	SELECT &lcCursor
	GO top

	ACESSOSLT.gridPesq.refresh
ENDFUNC 



**
FUNCTION uf_ACESSOSLT_sair
	
	IF USED("ucrsPerfLt")
		Fecha("ucrsPerfLt")
	ENDIF
	
	IF USED("ucrsUtilizadoresPerf")
		fecha("ucrsUtilizadoresPerf")
	ENDIF
	
	IF USED("ucrsGruposPerf")
		fecha("ucrsGruposPerf")
	ENDIF
	
	
	ACESSOSLT.hide
	ACESSOSLT.Release
		
	RELEASE ACESSOSLT
ENDFUNC 