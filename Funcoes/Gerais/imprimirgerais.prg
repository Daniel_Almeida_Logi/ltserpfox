*--------------------------------------------------------------------------------------
* FoxBarcodeQR.prg
*--------------------------------------------------------------------------------------
* FoxBarcodeQR is a application free software and offers a Barcode tool for the Visual
* FoxPro Community. This is a supplement of FoxBarcode class only for QR Code barcodes.
* This software is provided "as is" without express or implied warranty.
* Use it at your own risk
*--------------------------------------------------------------------------------------
* Version: 2.10
* Date   : 2021.02.27
* Author : VFPEncoding
* Email  : vfpencoding@gmail.com
*
* Note   : VFPEncoding are
*          Guillermo Carrero (QEPD) (Barcelona, Spain) and
*          Luis Maria Guayan (Tucuman, Argentina)
*--------------------------------------------------------------------------------------
* Note   : This application use the libraries
*          BarCodeLibrary.dll (by: Dario Alvarez Aranda, Mexico)
*          QRCodeLib.dll (visit: www.validacfd.com)
*          Google API - https://developers.google.com/chart/infographics/docs/qr_codes
*--------------------------------------------------------------------------------------

*--------------------------------------------------------------------------------------
* FoxBarcodeQR Class Definition
*--------------------------------------------------------------------------------------
Define Class FoxBarcodeQR As Custom

	m.cTempPath = "" && Windows Temp folder + SYS(2015)
	m.cAppPath = "" && App folder
	m.lDeleteTempFiles = .T. && Delete the temporary folder and image files

	m.nVersion = 2.10 && Version
	m.lAutoConfigurate = .T.
	m.lAutoFit = .F.
	m.nBackColor = Rgb(255, 255, 255) && White
	m.nBarColor = Rgb(0, 0, 0) && Black
	m.nCorrectionLevel = 1 && 0-[L]ow 7%, 1-[M]edium 15%, 2-[Q]uartile 25%, 3-[H]igh 30%]
	m.nEncoding = 4 && Automatic encoding algorithm
	m.nMarginPixels = 0 && pixels
	m.nModuleWidth = 2 && pixels
	m.nHeight = 132 && pixels
	m.nWidth = 132 && pixels
	m.nSize = 132 && pixels

	*---------------------------------------------------------
	* PROCEDURE QRBarcodeImage()
	*---------------------------------------------------------
	* Generated QR Barcode image with BarCodeLibrary.dll
	*  Parameters:
	*   tcText: Text to encode
	*   tcFile: Imagen File Name (optional)
	*   tnSize: Imagen Size [2..12] (default = 4)
	*     (Width in pixels is tnSize x 33 pixels)
	*     (tnSize = 4 -> 132 x 132 pixels)
	*   tnType: Imagen Type [BMP, JPG or PNG] (default = 0)
	*     0 = BMP
	*     1 = JPG
	*     2 = PNG
	*---------------------------------------------------------
	Procedure QRBarcodeImage(tcText, tcFile, tnSize, tnType)
		Local lcType, lcFolder
		Public upv_qrCodePath

		If Vartype(m.tnSize) <> "N"
			m.tnSize = Int(This.nSize / 33) && default size:  132 x 132 pixels
		Endif

		If Vartype(m.tnType) <> "N"
			m.tnType = 0  && defaul type: BMP
		Endif

		m.tnSize = Min(Max(m.tnSize, 2), 12)
		m.tnType = Min(Max(m.tnType, 0), 2)
		m.lcType = Iif(m.tnType = 1, "JPG", Iif(m.tnType = 2, "PNG", "BMP"))

		If Empty(m.tcFile)
			m.lcFolder = This.cTempPath
			If Not Directory(m.lcFolder)
				Md (m.lcFolder)
			Endif
			m.tcFile = Forceext(m.lcFolder + Sys(2015), m.lcType)
		Else
			m.lcFolder = Justpath(m.tcFile)
			If Not Directory(m.lcFolder) And Not Empty(m.lcFolder)
				Md (m.lcFolder)
			Endif
			m.tcFile = Forceext(m.tcFile, m.lcType)
		Endif

		*-- Declare the functions of BarCodeLibrary.dll
		Declare Integer GenerateFile In "BarCodeLibrary.dll" ;
			STRING cData, String cFileName

		Declare Integer SetConfiguration In "BarCodeLibrary.dll" ;
			INTEGER nSize, Integer nImageType

		SetConfiguration(m.tnSize, m.tnType)
		GenerateFile(m.tcText, m.tcFile)

		Clear Dlls "SetConfiguration", "GenerateFile"

		upv_qrCodePath = m.tcFile

		Return m.tcFile
	Endproc

	*---------------------------------------------------------
	* PROCEDURE FullQRCodeImage()
	*---------------------------------------------------------
	* Generated QR Barcode image with QRCodeLib.dll (visit: www.validacfd.com)
	*  Parameters:
	*   tcText: Text to encode
	*   tcFile: Imagen File Name (optional)
	*   tnSize: Imagen Size in pixels
	*   tnType: Imagen Type [Only 0=BMP]
	*---------------------------------------------------------
	Procedure FullQRCodeImage(tcText, tcFile, tnSize, tnType)
		Local lcType, lcFolder
		Public upv_qrCodePath

		If Vartype(m.tnSize) <> "N"
			m.tnSize = This.nSize
		Endif
		m.tnSize = Min(Max(m.tnSize, 64), 1280)
		Store m.tnSize To This.nHeight, This.nWidth

		*-- Only BMP type
		m.tnType = 0
		m.lcType = "BMP"

		If Empty(m.tcFile)
			m.lcFolder = This.cTempPath
			If Not Directory(m.lcFolder)
				Md (m.lcFolder)
			Endif
			m.tcFile = Forceext(m.lcFolder + Sys(2015), m.lcType)
		Else
			m.lcFolder = Justpath(m.tcFile)
			If Not Directory(m.lcFolder) And Not Empty(m.lcFolder)
				Md (m.lcFolder)
			Endif
			m.tcFile = Forceext(m.tcFile, m.lcType)
		Endif
		Clear Resources m.tcFile

		*-- Declare the functions of QRCodeLib.dll (visit: www.validacfd.com)
		Declare FullQRCode In "QRCodeLib.dll" ;
			INTEGER lAutoConfigurate, ;
			INTEGER lAutoFit, ;
			LONG nBackColor, ;
			LONG nBarColor, ;
			STRING cText, ;
			INTEGER nCorrectionLevel, ;
			INTEGER nEncoding, ;
			INTEGER nMarginPixels, ;
			INTEGER nModuleWidth, ;
			INTEGER nHeight, ;
			INTEGER nWidth, ;
			STRING cFileName

		FullQRCode(This.lAutoConfigurate, This.lAutoFit, This.nBackColor, This.nBarColor, ;
			M.tcText, This.nCorrectionLevel, This.nEncoding, This.nMarginPixels, ;
			THIS.nModuleWidth, This.nHeight, This.nWidth, m.tcFile)

		Clear Dlls "FullQRCode"

		upv_qrCodePath = m.tcFile

		Return m.tcFile
	Endproc

	*---------------------------------------------------------
	* PROCEDURE FastQRCodeImage()
	*---------------------------------------------------------
	* Generated QR Barcode image with QRCodeLib.dll (visit: www.validacfd.com)
	*  Parameters:
	*   tcText: Text to encode
	*   tcFile: Imagen File Name (optional)
	*---------------------------------------------------------
	Procedure FastQRCodeImage(tcText, tcFile)
		Local lcFolder, lcType

		*-- Only BMP type
		m.lcType = "BMP"

		If Empty(m.tcFile)
			m.lcFolder = This.cTempPath
			If Not Directory(m.lcFolder)
				Md (m.lcFolder)
			Endif
			m.tcFile = Forceext(m.lcFolder + Sys(2015), m.lcType)
		Else
			m.lcFolder = Justpath(m.tcFile)
			If Not Directory(m.lcFolder) And Not Empty(m.lcFolder)
				Md (m.lcFolder)
			Endif
			m.tcFile = Forceext(m.tcFile, m.lcType)
		Endif
		Clear Resources m.tcFile

		*-- Declare the functions of QRCodeLib.dll (visit: www.validacfd.com)
		Declare FastQRCode In "QRCodeLib.dll" ;
			STRING cText, ;
			STRING cFileName

		FastQRCode(m.tcText, m.tcFile)

		Clear Dlls "FastQRCode"

		Return m.tcFile
	Endproc

	*---------------------------------------------------------
	* PROCEDURE GooQRCodeImage()
	*---------------------------------------------------------
	* Generated QR Barcode image with Google API (requires internet connection)
	* https://developers.google.com/chart/infographics/docs/qr_codes
	*  Parametes:
	*   tcText: Text to encode
	*   tcFile: Imagen File Name (optional)
	*   tnSize: Imagen Size in pixels
	*   tnType: Imagen Type [Only 2=PNG]
	*---------------------------------------------------------
	Procedure GooQRCodeImage(tcText, tcFile, tnSize, tnType)
		Local lcType, lcFolder, lcUrl, lcCorrection, lnMargin

		If Vartype(m.tnSize) <> "N"
			m.tnSize = This.nSize && Defaul size = 132 x 132 pixels
		Endif

		m.tnSize = Min(Max(m.tnSize, 72), 540)
		m.lnMargin = Min(Max(This.nMarginPixels, 0), 10)

		*-- Only PNG type
		m.tnType = 2
		m.lcType = "PNG"

		If Empty(m.tcFile)
			m.lcFolder = This.cTempPath
			If Not Directory(m.lcFolder)
				Md (m.lcFolder)
			Endif
			m.tcFile = Forceext(m.lcFolder + Sys(2015), m.lcType)
		Else
			m.lcFolder = Justpath(m.tcFile)
			If Not Directory(m.lcFolder) And Not Empty(m.lcFolder)
				Md (m.lcFolder)
			Endif
			m.tcFile = Forceext(m.tcFile, m.lcType)
		Endif
		Clear Resources m.tcFile

		m.lcCorrection = Substr([LMQH], This.nCorrectionLevel + 1, 1)
		If Empty(m.lcCorrection)
			m.lcCorrection = [M] && Default
		Endif

		m.lcUrl = [https://chart.googleapis.com/chart?cht=qr] + ;
			[&chs=] + Transform(m.tnSize) + [x] + Transform(m.tnSize) + ;
			[&chld=] + m.lcCorrection + [|] + Transform(m.lnMargin) + ;
			[&chl=] + m.tcText

		*-- Declare URLDownloadToFile function
		Declare Long URLDownloadToFile In URLMON.Dll ;
			LONG, String, String, Long, Long

		Erase (m.tcFile)
		If 0 = URLDownloadToFile(0, m.lcUrl, m.tcFile, 0, 0)
			Return m.tcFile
		Else
			Return ""
		Endif
	Endproc

	*------------------------------------------------------
	* PROCEDURE QRCodeVersion()
	*------------------------------------------------------
	* Returns the version of the QRCodeLib.dll library
	*------------------------------------------------------
	Procedure QRCodeVersion()
		Local lcVersion
		Declare String QRCodeLibVer In "QRCodeLib.dll"
		m.lcVersion = QRCodeLibVer()
		Clear Dlls "QRCodeLibVer"
		Return m.lcVersion
	Endproc

	*------------------------------------------------------
	* PROCEDURE QRBarcodeVersion()
	*------------------------------------------------------
	* Returns the version of the BarCodeLibrary.dll library
	*------------------------------------------------------
	Procedure QRBarcodeVersion()
		Local lcVersion
		Declare Integer LibraryVersion In "BarCodeLibrary.dll"
		m.lcVersion = LibraryVersion()
		Clear Dlls "LibraryVersion"
		Return m.lcVersion
	Endproc

	*------------------------------------------------------
	* PROCEDURE Init()
	*------------------------------------------------------
	Procedure Init()
		This.cTempPath = Addbs(This.TempPath() + Sys(2015))
		This.cAppPath = Fullpath("")
		Store This.nSize To This.nHeight, This.nWidth
	Endproc

	*------------------------------------------------------
	* PROCEDURE Destroy()
	*------------------------------------------------------
	Procedure Destroy()
		If This.lDeleteTempFiles
			This.EmptyFolder(This.cTempPath)
			If Directory(This.cTempPath)
				Rd (This.cTempPath)
			Endif
		Endif
	Endproc

	*------------------------------------------------------
	* PROCEDURE EmptyFolder(tcFolder)
	*------------------------------------------------------
	* Empty temporary image folder
	*------------------------------------------------------
	Procedure EmptyFolder(tcFolder)
		Local loFso As Object
		Local lcMask
		Do Case
			Case Empty(m.tcFolder)
				Return .F.
			Case Not Directory(m.tcFolder)
				Return .F.
		Endcase
		m.lcMask = Addbs(m.tcFolder) + "*.*"
		#If .T. && Use FSO
			m.loFso  = Createobject("Scripting.FileSystemObject")
			m.loFso.DeleteFile(m.lcMask, .T.)
		#Else && Not Use FSO
			Erase (m.lcMask)
		#Endif
		Return  .T.
	Endproc

	*---------------------------------------------------------
	* PROCEDURE TempPath()
	*---------------------------------------------------------
	* Returns the path for temporary files
	*---------------------------------------------------------
	Procedure TempPath()
		Local lcPath, lnRet
		Local lnSize
		m.lcPath = Space(255)
		m.lnSize = 255
		Declare Integer GetTempPath In WIN32API ;
			INTEGER nBufSize, ;
			STRING @cPathName
		m.lnRet = GetTempPath(m.lnSize, @m.lcPath)
		If m.lnRet <= 0
			m.lcPath = Addbs(Fullpath("TEMP"))
		Else
			m.lcPath = Addbs(Substr(m.lcPath, 1, m.lnRet))
		Endif
		Return m.lcPath
	Endproc

	*---------------------------------------------------------
	* PROCEDURE Error
	*---------------------------------------------------------
	* Error procedure
	*---------------------------------------------------------
	Procedure Error
		Lparameters nError, cMethod, nLine
		Local lcErrMsg
		Local la[1]
		Aerror(la)
		m.lcErrMsg =  "Error number: " + Transform(m.la(1, 1)) + Chr(13) + ;
			"Error message: " + m.la(1, 2) + Chr(13) + Chr(13) + ;
			"Method: " + m.cMethod + Chr(13) + ;
			"Line: " + Transform(m.nLine)
		Messagebox(m.lcErrMsg, 0 + 16, "FoxBarcodeQR error")
	Endproc

Enddefine && FoxBarcodeQR

*--------------------------------------------------------------------------------------
* END DEFINE FoxBarcodeQR Class
*--------------------------------------------------------------------------------------

*--------------------------------------------------------------------------------------
* End FoxBarcodeQR.prg
*--------------------------------------------------------------------------------------


**
Function uf_imprimirgerais_Chama
	Lparameters lcBool, lcSemPerguntas, lcModal, lcDocumentoDefault, lcNrAtend
	Public MYIMPRESSAOORIGEM, myNrAtend
	Local lcSql, lcDocumento, lcEmailEnvio
	lcDocumento = ''
	lcEmailEnvio = ''

	myNrAtend = lcNrAtend
	MYIMPRESSAOORIGEM = lcBool

	Do Case
		Case uf_gerais_compstr(MYIMPRESSAOORIGEM , "Atendimento")
			lcDocumento = "Venda a Dinheiro"
			Select ft
			lcDocumento = Alltrim(ft.Nmdoc)

			TEXT TO lcSQL TEXTMERGE NOSHOW
			 	select email from b_utentes(nolock) WHERE no=<<ft.no>> and estab=<<ft.estab>>
			ENDTEXT

			If !uf_gerais_actGrelha("","ucrsEmailEnv",lcSql)
				uf_perguntalt_chama("N�O FOI POSSIVEL ENCONTRAR A CONFIGURA��O DE ENVIO DE EMAILS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Else
				If Reccount("ucrsEmailEnv")>0 Then
					lcEmailEnvio = Alltrim(ucrsEmailEnv.email)
				Endif
			Endif





		Case lcBool == "FACTURACAO"
			Select ft
			lcDocumento = Alltrim(ft.Nmdoc)

			** fix para nao imprimir  na  Pharma Sun - Carla Isabel Duarte Bernardo
			If(Used("ucrse1"))
				If(Alltrim(ucrse1.id_lt) == "P18117A")
					Return .F.
				Endif
			Endif



			TEXT TO lcSQL TEXTMERGE NOSHOW
			 	select email from b_utentes(nolock) WHERE no=<<ft.no>> and estab=<<ft.estab>>
			ENDTEXT


			If !uf_gerais_actGrelha("","ucrsEmailEnv",lcSql)
				uf_perguntalt_chama("N�O FOI POSSIVEL ENCONTRAR A CONFIGURA��O DE ENVIO DE EMAILS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Else
				If Reccount("ucrsEmailEnv")>0 Then
					lcEmailEnvio = Alltrim(ucrsEmailEnv.email)
				Endif
			Endif


		Case lcBool == "DOCUMENTOS"
			Select CabDoc
			lcDocumento = Alltrim(CabDoc.Doc)

			Select ucrsConfigDoc
			If Alltrim(ucrsConfigDoc.bdempresas) = "FL"
				Select CabDoc
				TEXT TO lcSQL TEXTMERGE NOSHOW
				 	select email from fl(nolock) WHERE no=<<cabDoc.no>> and estab=<<cabDoc.estab>>
				ENDTEXT
				If !uf_gerais_actGrelha("","ucrsEmailEnv",lcSql)
					uf_perguntalt_chama("N�O FOI POSSIVEL ENCONTRAR A CONFIGURA��O DE ENVIO DE EMAILS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					Return .F.
				Else
					If Reccount("ucrsEmailEnv")>0 Then
						lcEmailEnvio = Alltrim(ucrsEmailEnv.email)
					Endif
				Endif
			Endif

			If Alltrim(ucrsConfigDoc.bdempresas) = "CL"
				Select CabDoc
				TEXT TO lcSQL TEXTMERGE NOSHOW
				 	select email from b_utentes WHERE no=<<cabDoc.no>> and estab=<<cabDoc.estab>>
				ENDTEXT
				If !uf_gerais_actGrelha("","ucrsEmailEnv",lcSql)
					uf_perguntalt_chama("N�O FOI POSSIVEL ENCONTRAR A CONFIGURA��O DE ENVIO DE EMAILS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					Return .F.
				Else
					If Reccount("ucrsEmailEnv")>0 Then
						lcEmailEnvio = Alltrim(ucrsEmailEnv.email)
					Endif
				Endif
			Endif


		Case lcBool == "FORNECEDORES"
			lcDocumento = "Ficha do Fornecedor"
			TEXT TO lcSQL TEXTMERGE NOSHOW
			 	select email from fl WHERE no=<<fl.no>> and estab=<<fl.estab>>
			ENDTEXT
			If !uf_gerais_actGrelha("","ucrsEmailEnv",lcSql)
				uf_perguntalt_chama("N�O FOI POSSIVEL ENCONTRAR A CONFIGURA��O DE ENVIO DE EMAILS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Else
				If Reccount("ucrsEmailEnv")>0 Then
					lcEmailEnvio = Alltrim(ucrsEmailEnv.email)
				Endif
			Endif

		Case lcBool == "RECIBO"
			lcDocumento = "RECIBO"

			uv_gestRec = .F.

			If Wexist("REGVENDAS")
				If REGVENDAS.pgfReg.ActivePage = 4 And uf_gerais_getParameter_site('ADM0000000001', 'BOOL', mySite)
					uv_gestRec = .T.
				Endif
			Endif

			If uv_gestRec

				If Used("UCRSRECIBOS")

					Select UCRSRECIBOS

					Locate For UCRSRECIBOS.sel = .T.

					If Found()

						TEXT TO msel TEXTMERGE NOSHOW

							select
								email,
								autorizado,
								autoriza_emails,
								tokenaprovacao
							from
								b_utentes(nolock)
							where
								no = (select no from re(nolock) where restamp = '<<ALLTRIM(UCRSRECIBOS.restamp)>>')
								and estab = (select estab from re(nolock) where restamp = '<<ALLTRIM(UCRSRECIBOS.restamp)>>')

						ENDTEXT

						If !uf_gerais_actGrelha("", "uc_email", msel)
							uf_perguntalt_chama("N�O FOI POSSIVEL ENCONTRAR A CONFIGURA��O DE ENVIO DE EMAILS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
							Return .F.
						Else
							If Reccount("uc_email") <> 0

								lcEmailEnvio = Iif(uf_gerais_getParameter_site('ADM0000000001', 'BOOL', mySite), Iif(uc_email.autorizado And uc_email.autoriza_emails, Alltrim(uc_email.email), '') , Alltrim(uc_email.email))

							Endif
						Endif

					Endif

				Endif

			Else
				TEXT TO lcSQL TEXTMERGE NOSHOW
                    select email, autorizado, autoriza_emails from b_utentes(nolock) WHERE no=<<uCrsReCons.no>> and estab=<<uCrsReCons.estab>>
				ENDTEXT
				If !uf_gerais_actGrelha("","ucrsEmailEnv",lcSql)
					uf_perguntalt_chama("N�O FOI POSSIVEL ENCONTRAR A CONFIGURA��O DE ENVIO DE EMAILS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					Return .F.
				Else
					If Reccount("ucrsEmailEnv")>0 Then
						**lcEmailEnvio = ALLTRIM(ucrsEmailEnv.email)
						lcEmailEnvio = Iif(uf_gerais_getParameter_site('ADM0000000001', 'BOOL', mySite), Iif(ucrsEmailEnv.autorizado And ucrsEmailEnv.autoriza_emails, Alltrim(ucrsEmailEnv.email), '') , Alltrim(ucrsEmailEnv.email))
					Endif
				Endif
			Endif

		Case lcBool == "PAGAMENTO"
			lcDocumento = "PAGAMENTO"
			TEXT TO lcSQL TEXTMERGE NOSHOW
			 	select email from fl WHERE no=<<po.no>> and estab=<<po.estab>>
			ENDTEXT
			If !uf_gerais_actGrelha("","ucrsEmailEnv",lcSql)
				uf_perguntalt_chama("N�O FOI POSSIVEL ENCONTRAR A CONFIGURA��O DE ENVIO DE EMAILS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Else
				If Reccount("ucrsEmailEnv")>0 Then
					lcEmailEnvio = Alltrim(ucrsEmailEnv.email)
				Endif
			Endif

		Case lcBool == "MCDT"
			lcDocumento = "MCDT"
			If Empty(lcSemPerguntas)
				Prescricao.Enabled = .F.
			Endif

		Case lcBool == "PRESCRICAOMED"
			lcDocumento = "Receita"
			If Empty(lcSemPerguntas)
				Prescricao.Enabled = .F.
			Endif
			TEXT TO lcSQL TEXTMERGE NOSHOW
			 	select email from b_utentes WHERE no=<<ucrsCabPresc.NO>> and estab=<<ucrsCabPresc.estab>>
			ENDTEXT
			If !uf_gerais_actGrelha("","ucrsEmailEnv",lcSql)
				uf_perguntalt_chama("N�O FOI POSSIVEL ENCONTRAR A CONFIGURA��O DE ENVIO DE EMAILS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Else
				If Reccount("ucrsEmailEnv")>0 Then
					lcEmailEnvio = Alltrim(ucrsEmailEnv.email)
				Endif
			Endif


		Case lcBool == "MARCACOES"
			lcDocumento = "MARCACOES"

		Case lcBool == "TRATAMENTO"
			lcDocumento = "TRATAMENTO"

		Case lcBool == "RESUMOFTENTIDADES"
			lcDocumento = "Resumo Faturas - Entidades"

		Case lcBool == "RESUMOFTASSOCIADOS"
			lcDocumento = "Nota de Lan�amento - Associados"

		Case lcBool == "RESUMORTASSOCIADOS"
			lcDocumento = "Aviso de Lan�amento - Entidades"

		Case lcBool == "RECIBOENTASSOCIADOS"
			lcDocumento = "Recibo - Entidades"

		Case lcBool == "RECIBOENTASSOCIADOSREIMP"
			lcDocumento = "Recibo - Entidades"

		Case lcBool == "CARTAO_H"
			lcDocumento = "CARTAO_H"

		Case lcBool == "TALAO_LAVANTAMENTO"
			lcDocumento = "TALAO_LAVANTAMENTO"

		Case lcBool == "ETIQUETAS_STOCKS"
			lcDocumento = "ETIQUETAS_STOCKS"
		Case lcBool == "DOCS_ETIQUETAS"
			lcDocumento = "ETIQUETAS_STOCKS"
		Case lcBool == "ERROSFATURA"
			lcDocumento = "ERROSFATURA"
		Case lcBool == "ERROSFATURAENTMES"
			lcDocumento = "ERROSFATURAENTMES"
		Case lcBool == "ERROSFATURAMES"
			lcDocumento = "ERROSFATURAMES"
		Case lcBool == "ERROSFACTELEC"
			lcDocumento = "ERROSFACTELEC"
		Case lcBool == "SINAVE"
			Select ucrsatendcl

			TEXT TO lcSQL TEXTMERGE NOSHOW
			 	select email from b_utentes WHERE no=<<ucrsatendcl.no>> and estab=<<ucrsatendcl.estab>>
			ENDTEXT
			If !uf_gerais_actGrelha("","ucrsEmailEnv",lcSql)
				uf_perguntalt_chama("N�O FOI POSSIVEL ENCONTRAR A CONFIGURA��O DE ENVIO DE EMAILS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Else
				If Reccount("ucrsEmailEnv")>0 Then
					lcEmailEnvio = Alltrim(ucrsEmailEnv.email)
				Endif
			Endif
			lcDocumento = "SINAVE"
		Case lcBool == "RegistoClinico"
			lcDocumento = "Registo Clinico"

	Endcase
	If Used("ucrsEmailEnv")
		fecha("ucrsEmailEnv")
	Endif

	If Used("ucrsImpIdus")
		fecha("ucrsImpIdus")
	Endif

	lcSql = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		 Select nomeimpressao From b_impressoes (nolock) where documento =  '<<ALLTRIM(lcDocumento)>>' ORDER BY idupordefeito Desc
	ENDTEXT
	If !uf_gerais_actGrelha("","ucrsImpIdus",lcSql)
		uf_perguntalt_chama("N�O FOI POSSIVEL ENCONTRAR A CONFIGURA��O DE IMPRESS�ES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif

	If Used("ucrsImpImpressoras")
		fecha("ucrsImpImpressoras")
	Endif

	Create Cursor ucrsImpImpressoras(printerName c(200))

	Select ucrsImpImpressoras
	Append Blank
	Replace ucrsImpImpressoras.printerName With "Impressora por defeito do Windows"

	** Preenche Combo Impressoras **
	Dimension laPrinters[1,1]
	=Aprinters(laPrinters)

	If(Vartype(laPrinters)=='C')
		For lnIndex = 1 To Alen(laPrinters,1)
			lcPrinterName=laPrinters(lnIndex,1)
			lcPort=Alltrim(laPrinters(lnIndex,2))
			Append Blank
			Replace ucrsImpImpressoras.printerName With lcPrinterName
		Endfor
	Endif


	If Used("ucrsImpIdusDefault")
		fecha("ucrsImpIdusDefault")
	Endif

	lcSql = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select
			nomeimpressao,descricao
		From
			b_impressoes (nolock)
		Where
			nomeimpressao != ''
			AND documento = '<<Alltrim(lcDocumento)>>'
			AND (tipocliente = '<<ALLTRIM(uCrsE1.tipoempresa)>>' or tipocliente = '')
            AND tipocliente <> 'INTERNO'
		ORDER BY
			idupordefeito DESC
	ENDTEXT

	If !uf_gerais_actGrelha("","ucrsImpIdusDefault",lcSql)
		uf_perguntalt_chama("N�O FOI POSSIVEL ENCONTRAR A CONFIGURA��O DE IMPRESS�ES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif

	**
	If Used("uCrsImprimirDocs")
		fecha("uCrsImprimirDocs")
	Endif
	lcSql = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_gerais_getImpressoes '<<Alltrim(lcDocumento)>>', '<<ALLTRIM(uCrsE1.tipoempresa)>>'
	ENDTEXT

	If !uf_gerais_actGrelha("","uCrsImprimirDocs",lcSql)
		uf_perguntalt_chama("N�O FOI POSSIVEL ENCONTRAR A CONFIGURA��O DE IMPRESS�ES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif

	**

	If Empty(lcSemPerguntas)
		If Type("IMPRIMIRGERAIS") == "U"
			lcIniciou = 1
			Do Form IMPRIMIRGERAIS With lcModal, lcDocumento, lcDocumentoDefault
			IMPRIMIRGERAIS.Show
		Else
			IMPRIMIRGERAIS.Show
			IMPRIMIRGERAIS.Refresh
		Endif
	Else

		Do Case
			Case MYIMPRESSAOORIGEM == "MCDT" Or MYIMPRESSAOORIGEM == "PRESCRICAOMED"
				uf_imprimirGerais_imprimir_Pe("print", lcSemPerguntas)

			Case uf_gerais_compstr(MYIMPRESSAOORIGEM, "Atendimento")
				If IMPRIMIRGERAIS.pageframe1.page1.chkPorEmail.Value=1
					If ucrse1.usacoms=.T.
						If Empty(IMPRIMIRGERAIS.pageframe1.page1.emaildestino.Value)
							uf_perguntalt_chama("N�o existe um email para envio preenchido." + Chr(13) + "Insira um ou atualize a ficha respetiva.","OK","",64)
							Return .F.
						Endif
					Else
						uf_perguntalt_chama("PARA UTILIZAR ESTA FUNCIONALIDADE DEVER� ADQUIRIR O M�DULO DE COMUNICA��ES." + Chr(13) + "PARA MAIS INFORMA��ES POR FAVOR CONTACTE A LOGITOOLS (OP��O - COMERCIAL).", "OK","",16)
					Endif
				Endif
				uf_imprimirgerais_imprimirAtendimentoInit()
				Select ftTempImp
				Go Top
				Scan
					lcStampFtTempImp = ftTempImp.ftstamp

					fecha ("ft")
					fecha ("ft2")
					fecha ("fi")

					lcSql = ''
					TEXT TO lcsql TEXTMERGE NOSHOW
							exec up_touch_ft '<<ALLTRIM(lcStampFtTempImp)>>'
					ENDTEXT
					uf_gerais_actGrelha("", "FT", lcSql)
					lcSql = ''
					TEXT TO lcsql TEXTMERGE NOSHOW
							SELECT respostaDt = isnull((select top 1 DescrResposta from b_dt_resposta WHERE stampDoc = '<<lcStampFtTempImp>>' order by DataEnvio),''),* FROM FT2 (nolock) WHERE ft2stamp='<<ALLTRIM(lcStampFtTempImp)>>'
					ENDTEXT
					uf_gerais_actGrelha("", "FT2", lcSql)
					lcSql = ''
					TEXT TO lcsql TEXTMERGE NOSHOW
							exec up_facturacao_linhas 0, <<mysite_nr>>, '<<lcStampFtTempImp>>'
					ENDTEXT
					uf_gerais_actGrelha("", "FI", lcSql)

					TEXT TO lcSQL TEXTMERGE NOSHOW
			        	exec up_print_qrcode_a4 '<<lcStampFtTempImp>>'
					ENDTEXT
					uf_gerais_actGrelha("", "FTQRCODE", lcSql)

					Select ftTempImp
					lcDocumento = Alltrim(ftTempImp.Nmdoc)

					uf_imprimirGerais_imprimir("print", lcSemPerguntas)

				Endscan
				If IMPRIMIRGERAIS.pageframe1.page1.chkPorEmail.Value=1
					uf_gerais_sendmail_bulk("uc_CursorAttachment", .T.)
					fecha("uc_CursorAttachment")
				Endif
				uf_imprimirgerais_imprimirAtendimentoFim()

			Otherwise
				uf_imprimirGerais_imprimir("print", lcSemPerguntas)
		Endcase


	Endif

	**	uf_imprimirgerais_Chama('DOCUMENTOS',.f.,.t.,"Pedido Regulariza��o")
	**	LPARAMETERS lcBool, lcSemPerguntas, lcModal, lcDocumentoDefault
	If Vartype(IMPRIMIRGERAIS)!="U"
		If Vartype(lcDocumentoDefault)=="L"
			IMPRIMIRGERAIS.pageframe1.page1.emaildestino.Value = lcEmailEnvio
			IMPRIMIRGERAIS.pageframe1.page1.emailsubject.Value = "Envio de documento "
			IMPRIMIRGERAIS.pageframe1.page1.emailbody.Value = "Exmo(a). Sr(a). " + Chr(13) + Chr(13) + "Vimos por este meio enviar-lhe o documento em anexo." + Chr(13) + Alltrim(ucrse1.mailsign)
		Endif
	Endif


Endfunc


**
Function uf_imprimirGerais_imprimir
	Lparameters lctipo, lcSemPerguntas, lcNomePDF
	**

	If uf_gerais_compstr(MYIMPRESSAOORIGEM, "Atendimento")

		If !Used("TD")
			lcSQLTd  = ''
			Select ft
			Go Top
			TEXT TO lcSQLTd TEXTMERGE NOSHOW
			 	select * from td(nolock) WHERE ndoc=<<ft.ndoc>>
			ENDTEXT

			If !uf_gerais_actGrelha("","TD",lcSQLTd)
				uf_perguntalt_chama("N�O FOI POSSIVEL ENCONTRAR A CONFIGURA��O DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif
		Endif


	Endif


	If Empty(lcSemPerguntas)
		If Empty(Alltrim(IMPRIMIRGERAIS.pageframe1.page1.destino.Value)) Or Empty(Alltrim(IMPRIMIRGERAIS.pageframe1.page1.impressao.Value))
			uf_perguntalt_chama("Para imprimir � necess�rio selecionar a impress�o e destino.", "", "OK", 64)
			Return .F.
		Endif
	Else
		**
	Endif
	If MYIMPRESSAOORIGEM == "CARTAO_H"
		uf_imprimirGerais_imprimir_CARTAO_H(lctipo)
		Return .F.
	Endif

	If MYIMPRESSAOORIGEM == "RESUMOFTENTIDADES"
		uf_imprimirGerais_imprimir_ResumoFtEntidades(lctipo)
		Return .F.
	Endif

	If MYIMPRESSAOORIGEM == "RESUMOFTASSOCIADOS"
		uf_imprimirGerais_imprimir_ResumoFtAssociados(lctipo)
		Return .F.
	Endif

	If MYIMPRESSAOORIGEM == "RESUMORTASSOCIADOS"
		uf_imprimirGerais_imprimir_ResumoRtAssociados(lctipo)
		Return .F.
	Endif

	If MYIMPRESSAOORIGEM == "RECIBOENTASSOCIADOS"
		uf_imprimirGerais_imprimir_ReciboEntAssociados(lctipo)
		Return .F.
	Endif

	If MYIMPRESSAOORIGEM == "RECIBOENTASSOCIADOSREIMP"
		uf_imprimirGerais_imprimir_ReciboEntAssociados(lctipo, .T.)
		Return .F.
	Endif

	If MYIMPRESSAOORIGEM == "MCDT" Or MYIMPRESSAOORIGEM == "PRESCRICAOMED"
		uf_imprimirGerais_imprimir_Pe(lctipo)
		Return .F.
	Endif

	If Upper(MYIMPRESSAOORIGEM) == "MARCACOES"
		uf_imprimirGerais_imprimir_Marcacoes(lctipo)
		Return .F.
	Endif

	If Upper(MYIMPRESSAOORIGEM) == "TRATAMENTO"

		If Used("uCrsHisTratPrint")
			fecha("uCrsHisTratPrint")
		Endif
		If Used("ucrsCabecalhoTratPrint")
			fecha("ucrsCabecalhoTratPrint")
		Endif
		Create Cursor ucrsCabecalhoTratPrint(dtini d, dtfim d)

		Select * From uCrsHisTrat Into Cursor uCrsHisTratPrint Readwrite
		Select uCrsHisTrat

		Select Min(dtini) As dtini, Max(dtp) As dtp From uCrsHisTrat Into Cursor ucrsCabecalhoTratPrint Readwrite

		uf_imprimirGerais_imprimir_geral(lctipo,"uCrsHisTratPrint")
		Return .F.
	Endif

	If Upper(MYIMPRESSAOORIGEM) == "REGISTOCLINICO"
		uf_imprimirGerais_imprimir_geral(lctipo,"uCrsRegistoClinico")
		Return .F.
	Endif

	**
	Local lcimpressao,lcDefaultPrinter,lcSql, lcNomeFicheiroPDF, lcNo, lcEstab, lcPodeEnviarEmail, lcTabCab

	Public myIsentoIva, myPaisNacional, myMulticopia
	Store .F. To myIsentoIva, myPaisNacional, myMulticopia
	Store .T. To lcPodeEnviarEmail
	Store '' To lcNomeFicheiroPDF, lcTabCab
	Store 0 To lcNo, lcEstab

	** Guarda a impressora por defeito do Windows***
	lcDefaultPrinter=Set("printer",2)

	Local lcDefaultPrinter
	Local lcValErroPrinter
	lcValErroPrinter = .F.

	Try
		lcDefaultPrinter=Set("printer",2)
	Catch
		uf_perguntalt_chama("Foi detectado algum problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
		lcValErroPrinter = .T.
	Endtry

	If lcValErroPrinter
		Return .F.
	Endif


	Do Case
		Case Alltrim(Upper(MYIMPRESSAOORIGEM)) =  "RECIBO"

			If uf_gerais_getParameter_site('ADM0000000001', 'BOOL', mySite)

				uv_gestRec = .F.

				If Wexist("REGVENDAS")
					If REGVENDAS.pgfReg.ActivePage = 4
						uv_gestRec = .T.
					Endif
				Endif

				If uv_gestRec

					If Used("UCRSRECIBOS")

						Select UCRSRECIBOS

						Locate For UCRSRECIBOS.sel = .T.

						If Found()

							TEXT TO msel TEXTMERGE NOSHOW
								select autorizado, autoriza_emails, ncont from b_utentes(nolock) where no = (select no from re(nolock) where restamp = '<<ALLTRIM(UCRSRECIBOS.restamp)>>')	and estab = (select estab from re(nolock) where restamp = '<<ALLTRIM(UCRSRECIBOS.restamp)>>')
							ENDTEXT

							If uf_gerais_actGrelha("", "uc_email", msel)
								If Reccount("uc_email") <> 0

									If (uc_email.autoriza_emails = .F. And (Left(Alltrim(uc_email.ncont),1) <> '5' And Left(Alltrim(uc_email.ncont),1) <> '9' And Left(Alltrim(uc_email.ncont),1) <> '6') )
										lcPodeEnviarEmail = .F.
									Else
										lcPodeEnviarEmail = .T.
									Endif
								Endif
								fecha("uc_email")
							Endif

						Endif

					Endif

				Else
					TEXT TO lcSQL TEXTMERGE NOSHOW
						select email, autorizado, autoriza_emails from b_utentes(nolock) WHERE no=<<uCrsReCons.no>> and estab=<<uCrsReCons.estab>>
					ENDTEXT
					If uf_gerais_actGrelha("","ucrsEmailEnv",lcSql)
						If Reccount("ucrsEmailEnv")>0 Then
							If !ucrsEmailEnv.autorizado Or !ucrsEmailEnv.autoriza_emails
								lcPodeEnviarEmail = .F.
							Endif
							fecha("ucrsEmailEnv")
						Endif
					Endif
				Endif

			Else
				lcPodeEnviarEmail = .F.
			Endif

	Endcase



	**
	If Empty(lcSemPerguntas)
		lcimpressao = Alltrim(IMPRIMIRGERAIS.pageframe1.page1.impressao.Value) &&Alltrim(ucrsImpIdusDefault.nomeimpressao)
	Else
		Select uCrsImprimirDocs
		Go Top
		lcimpressao = uCrsImprimirDocs.nomeimpressao
	Endif

	** Obtem Nome do Ficheiro Tabela de Impressoes **
	lcSql = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select Top 1 nomeficheiro, tabela, temduplicados, numduplicados, multicopia
		From b_impressoes (nolock)
		Where nomeimpressao = '<<ALLTRIM(lcimpressao)>>'
	ENDTEXT

	If !uf_gerais_actGrelha("","templcSQL",lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR A CONFIGURA��O DE IMPRESS�ES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return
	Endif


	If Reccount("templcSQL") > 0
		lcnomeFicheiro = Alltrim(templcSQL.nomeficheiro)
		myMulticopia = templcSQL.multicopia
		If Empty(lcSemPerguntas)
			lcprinter = IMPRIMIRGERAIS.pageframe1.page1.destino.Value
		Else
			lcprinter = "Impressora por defeito do Windows"
		Endif
		lctabela = templcSQL.tabela
		lctemduplicados = templcSQL.temduplicados
		lcnumduplicados = templcSQL.numduplicados
	Else
		Return .F.
	Endif
	If !(uf_gerais_actGrelha("","uCrsImpressao",lcSql))
		&&erro
	Endif

	** PREPARAR CURSORES PARA IMPRESS�ES **

	** Obtem dados Gerais da Empresa / Loja **
	If !Used("uCrsE1")
		uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR OS DADOS GERAIS DA EMPRESA. CONTACTE O SUPORTE.", "OK", "", 16)
		Return .F.
	Endif
	**

	** Guardar report a imprimir - ficheiro **
	lcreport = Alltrim(myPath)+"\analises\" + Alltrim(lcnomeFicheiro)


	Local lcStampDoc && guarda o stamp do documento a imprimir
	Local lcExistemPsicoBenzo && Valida se existem psicos ou benzos nas linhas
	lcExistemPsicoBenzo = .F.

	Do Case
		Case Alltrim(lcnomeFicheiro) == "pedidoregularizacao.frx"

			Select CabDoc
			Go Top
			lcStampDoc = Alltrim(CabDoc.cabstamp)

			lcSql = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_documentos_pedidoRegularizacao '<<lcStampDoc>>'
			ENDTEXT
			uf_gerais_actGrelha("","ucrsPedidoRegularizacao",lcSql)

			If Reccount("ucrsPedidoRegularizacao") == 0
				uf_perguntalt_chama("N�o existem diferen�as de confer�ncia. N�o � possivel gerar o Pedido de Regulariza��o.","OK","",16)
				Return .F.
			Endif

			lcSql = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				Select * From FO (nolock) WHERE Fostamp = '<<lcStampDoc>>'
			ENDTEXT
			uf_gerais_actGrelha("","FO",lcSql)

			lcSql = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				Select * From FO2 (nolock) WHERE Fo2stamp = '<<lcStampDoc>>'
			ENDTEXT
			uf_gerais_actGrelha("","FO2",lcSql)

			Select FN
			Go Top
			Scan For psico==.T. Or benzo==.T.
				lcExistemPsicoBenzo = .T.
			Endscan

			Select FO
			Go Top

			Select ucrsPedidoRegularizacao
			Go Top


			** criar cursores para dossiers internos
		Case Alltrim(lctabela) == "BO"
			If !Used("cabDoc")
				Return .F.
			Endif

			Select CabDoc
			Go Top
			lcStampDoc = Alltrim(CabDoc.cabstamp)

			lcSql = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				Select * From BO (nolock) Where Bostamp = '<<lcStampDoc>>'
			ENDTEXT
			uf_gerais_actGrelha("","BO",lcSql)

			lcNomeFicheiroPDF= Alltrim(bo.nmdos) + " " + Alltrim(Str(bo.obrano))+".PDF"
			lcTabCab = "FI"

			lcSql = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				Select Sum(QTT) as qtttotal From Bi (nolock) Where Bostamp = '<<lcStampDoc>>'
			ENDTEXT
			uf_gerais_actGrelha("","uCrsQTTtotal",lcSql)

			lcSql = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				Select * From BO2 (nolock) Where bo2stamp = '<<lcStampDoc>>'
			ENDTEXT
			uf_gerais_actGrelha("","BO2",lcSql)

			lcSql = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_print_qrcode_a4_docs '<<lcStampDoc>>'
			ENDTEXT
			uf_gerais_actGrelha("","ucrsqrcode",lcSql)

			lcSql = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_print_qrcode_a4_docs_totais '<<lcStampDoc>>'
			ENDTEXT
			uf_gerais_actGrelha("","ucrsqrcodetots",lcSql)


			Select BI
			Go Top

			** Calcula Informa��o necess�ra para impress�o - Laboratorio
			If bo.ndos == 41 Or bo.ndos == 43

				If lcnomeFicheiro == "entregaclientespiking.frx"
					lcCursor = "ucrsTempBi"
					Select * From BI Where (BI.QTREC - BI.QTT2) > 0 Into Cursor ucrsTempBi Readwrite

					Select ucrsTempBi
					Go Top
					Scan For !Empty(Alltrim(ucrsTempBi .ref))

						If Used("uCrsLabTemp")
							fecha("uCrsLabTemp")
						Endif

						lcSql = ""
						TEXT TO lcSQL TEXTMERGE NOSHOW
							Select u_lab from st (nolock) Where ref = '<<Alltrim(ucrsTempBi.ref)>>'
						ENDTEXT
						uf_gerais_actGrelha("","uCrsLabTemp",lcSql)

						Select uCrsLabTemp
						Replace ucrsTempBi.lab With uCrsLabTemp.u_lab

						If Used("uCrsLabTemp")
							fecha("uCrsLabTemp")
						Endif

					Endscan

					**Lab
					Select ucrsTempBi
					Go Top
					**INDEX ON lab + STR(LORDEM) TAG CONCAT

					** Ordena Factura Ordem Alfabetica
					If !Empty(IMPRIMIRGERAIS.pageframe1.page1.chkOrdenacaoAlfabetica.Value)

						Select ucrsTempBi
						Go Top
						Index On Design Tag Concat
					Else
						Select ucrsTempBi
						Go Top
						Index On lordem Tag Concat
					Endif
				Else

					Select BI
					Go Top
					Scan For !Empty(Alltrim(BI.ref))

						If Used("uCrsLabTemp")
							fecha("uCrsLabTemp")
						Endif

						lcSql = ""
						TEXT TO lcSQL TEXTMERGE NOSHOW
							Select u_lab from st (nolock) Where ref = '<<Alltrim(BI.ref)>>'
						ENDTEXT
						uf_gerais_actGrelha("","uCrsLabTemp",lcSql)

						Select uCrsLabTemp
						Replace BI.lab With uCrsLabTemp.u_lab

						If Used("uCrsLabTemp")
							fecha("uCrsLabTemp")
						Endif

					Endscan

					** Ordena Factura Ordem Alfabetica
					If !Empty(IMPRIMIRGERAIS.pageframe1.page1.chkOrdenacaoAlfabetica.Value)
						Select BI
						Go Top
						Index On Design Tag Concat
					Else
						Select BI
						Go Top
						Index On lordem Tag Concat
					Endif
				Endif
			Endif

			Select BI
			Go Top
			Scan For psico==.T. Or benzo==.T.
				lcExistemPsicoBenzo = .T.
			Endscan

			** criar cursores para compras
		Case Alltrim(lctabela) == "FO"
			If !Used("cabDoc")
				Return .F.
			Endif

			Select CabDoc
			Go Top
			lcStampDoc = Alltrim(CabDoc.cabstamp)

			lcSql = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				Select * From FO (nolock) Where Fostamp = '<<lcStampDoc>>'
			ENDTEXT
			uf_gerais_actGrelha("","FO",lcSql)

			lcNomeFicheiroPDF= Alltrim(FO.docnome) + " " + Alltrim(FO.Adoc)+".PDF"
			lcTabCab = "FN"

			lcSql = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				Select * From FO2 (nolock) Where Fo2stamp = '<<lcStampDoc>>'
			ENDTEXT
			uf_gerais_actGrelha("","FO2",lcSql)

			** Trata BASES INC. IVA
			lcSql = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				SELECT	SUM(BASEINC1) as BASEINC1,
				SUM(BASEINC2) as BASEINC2,
				SUM(BASEINC3) as BASEINC3,
				SUM(BASEINC4) as BASEINC4,
				SUM(BASEINC5) as BASEINC5,
				SUM(BASEINC6) as BASEINC6,
				SUM(BASEINC7) as BASEINC7,
				SUM(BASEINC8) as BASEINC8,
				SUM(BASEINC9) as BASEINC9
				FROM(
					SELECT	ISNULL(CASE WHEN TABIVA = 1 THEN ISNULL(fn.etiliquido,0) END,0) as BASEINC1,
						ISNULL(CASE WHEN TABIVA = 2 THEN ISNULL(fn.etiliquido,0) END,0) as BASEINC2,
						ISNULL(CASE WHEN TABIVA = 3 THEN ISNULL(fn.etiliquido,0) END,0) as BASEINC3,
						ISNULL(CASE WHEN TABIVA = 4 THEN ISNULL(fn.etiliquido,0) END,0) as BASEINC4,
						ISNULL(CASE WHEN TABIVA = 5 THEN ISNULL(fn.etiliquido,0) END,0) as BASEINC5,
						ISNULL(CASE WHEN TABIVA = 6 THEN ISNULL(fn.etiliquido,0) END,0) as BASEINC6,
						ISNULL(CASE WHEN TABIVA = 7 THEN ISNULL(fn.etiliquido,0) END,0) as BASEINC7,
						ISNULL(CASE WHEN TABIVA = 8 THEN ISNULL(fn.etiliquido,0) END,0) as BASEINC8,
						ISNULL(CASE WHEN TABIVA = 9 THEN ISNULL(fn.etiliquido,0) END,0) as BASEINC9
						FROM FN (nolock)
						WHERE	FOSTAMP = '<<lcStampDoc>>'
						and qtt > 0
				)a
			ENDTEXT
			If !uf_gerais_actGrelha("","FObaseIncIva",lcSql)
				uf_perguntalt_chama("OCORREU UM ERRO A CALCULAR AS BASES DE INCIDENCIA. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

			Select FN
			Go Top
			Scan For psico==.T. Or benzo==.T.
				lcExistemPsicoBenzo = .T.
			Endscan

			** criar cursores para factura��o
		Case Alltrim(lctabela) == "FT"
			Select ft
			Go Top
			lcStampDoc = Alltrim(ft.ftstamp)
			lcNomeFicheiroPDF= Alltrim(ft.Nmdoc) + " " + Alltrim(Str(ft.fno))+".PDF"
			lcTabCab = "FI"
			lcNo = ft.no
			lcEstab = ft.estab


			** AFP - ir buscar o valor da fatura para colocar na impress�o da nota de lan�amento
			If Alltrim(ft.Nmdoc)='Nt. Lan�am. Assoc.'
				Local lcContaLin
				Public myDocLig, myTotDeb, myDocLig1, myTotDeb1, myDocLig2, myTotDeb2
				Store 0 To myDocLig, myTotDeb, myDocLig1, myTotDeb1, myDocLig2, myTotDeb2
				Store 0 To lcContaLin

				TEXT TO lcSQL TEXTMERGE NOSHOW
					exec up_facturacao_getDocsNtLancamento <<ft.no>>, <<ft.estab>>, '<<uf_gerais_getDate(ft.fdata, "SQL")>>'
				ENDTEXT
				If !uf_gerais_actGrelha("","uCrsFatMes",lcSql)
					uf_perguntalt_chama("Ocorreu um erro a consultar a fatura do m�s da farm�cia. Por favor contacte o suporte.", "OK", "", 16)
					Return .F.
				Endif
				lcContaLin=1

				If Reccount("uCrsFatMes")>0
					Select uCrsFatMes
					Go Top
					Scan


						If lcContaLin=1

							myDocLig = uCrsFatMes.fno
							myTotDeb = uCrsFatMes.etotal
						Endif
						If lcContaLin=2
							myDocLig1 = uCrsFatMes.fno
							myTotDeb1 = uCrsFatMes.etotal
						Endif
						If lcContaLin=3
							myDocLig2 = uCrsFatMes.fno
							myTotDeb2 = uCrsFatMes.etotal
						Endif
						lcContaLin=lcContaLin+1
					Endscan
				Endif
				If Used("uCrsFatMes")
					fecha("uCrsFatMes")
				Endif

			Endif

			lcSql = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				SELECT autoriza_emails, ncont FROM B_UTENTES WHERE no=<<ft.no>> and estab=<<ft.estab>>
			ENDTEXT
			If !uf_gerais_actGrelha("","ucrsAutorizaEmails",lcSql)
				uf_perguntalt_chama("Ocorreu uma anomalia a obter a informa��o para impress�o. Por favor contacte o suporte.","OK","",16)
				Return .F.
			Endif
			If (ucrsAutorizaEmails.autoriza_emails = .F. And (Left(Alltrim(ucrsAutorizaEmails.ncont),1) <> '5' And Left(Alltrim(ucrsAutorizaEmails.ncont),1) <> '9' And Left(Alltrim(ucrsAutorizaEmails.ncont),1) <> '6') )
				lcPodeEnviarEmail = .F.
			Endif

			** Trata BASES INC. IVA
			lcSql = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_gerais_iduCert '<<lcStampDoc>>'
			ENDTEXT
			If !uf_gerais_actGrelha("","ucrsHash",lcSql)
				uf_perguntalt_chama("Ocorreu uma anomalia a obter a informa��o para impress�o. Por favor contacte o suporte.","OK","",16)
				Return .F.
			Endif
			If !Reccount("ucrsHash")>0
				Select ucrsHash
				Append Blank
				Replace ucrsHash.temcert With .F.
				Replace ucrsHash.parte1	With ''
				Replace ucrsHash.parte1	With ''
			Endif

			** Actualiza Referencia Entidade (Clinica), verifica as conven��es para o Utente em Causa, o campo h� vem pre-preenchido da ficha
			Select ft
			lcSql = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_marcacoes_refEntidades <<ft.no>>,'<<ALLTRIM(ft.ftstamp)>>'
			ENDTEXT
			If !uf_gerais_actGrelha("","ucrsRefsEntidades",lcSql)
				uf_perguntalt_chama("Ocorreu uma anomalia a obter a informa��o das referencias das entidades. Por favor contacte o suporte.","OK","",16)
				Return .F.
			Endif

			If(Used("ucrsRefsEntidades"))
				If(Reccount("ucrsRefsEntidades")>0)
					Select ucrsRefsEntidades
					Go Top
					Select Fi
					Go Top

					Update Fi Set Fi.refentidade = ucrsRefsEntidades.refentidade From Fi inner Join ucrsRefsEntidades On Fi.ref =  ucrsRefsEntidades.ref
				Endif
			Endif



			If Used("ucrsRefsEntidades")
				fecha("ucrsRefsEntidades")
			Endif
			Select Fi
			Go Top

			**
			Select ft
			Select ft2
			Select Fi

			***** Se for uma factura a entidades ***
			Select ucrse1
			If Upper(Alltrim(ucrse1.tipoempresa)) != "CLINICA"
				Select td
				If (td.u_tipodoc)==1 Or Left(Alltrim(td.Nmdoc),15) = 'N.Cr�dito SNS' Or Left(Alltrim(td.Nmdoc),14) = 'N.D�bito SNS'



					Public lnTot, mes
					Public pvp1,pvp2,pvp3,pvp4,pvp5,pvp6,pvp7,pvp8,pvp9
					Public compUtente1,compUtente2,compUtente3,compUtente4,compUtente5,compUtente6,compUtente7,compUtente8,compUtente9
					Public myCompOrg1,myCompOrg2,myCompOrg3,myCompOrg4,myCompOrg5,myCompOrg6,myCompOrg7,myCompOrg8,myCompOrg9
					Public fee1,fee2,fee3,fee4,fee5,fee6,fee7,fee8,fee9, feetotal, feetotalbi, feetotaliva
					Public base1,base2,base3,base4,base5,base6,base7,base8,base9

					Select Fi
					Calculate Sum(etiliquido) To lnTot
					inTot = uf_gerais_extenso(lnTot)

					******* Calcula Totais ************
					Store 0 To pvp1,pvp2,pvp3,pvp4,pvp5,pvp6,pvp7,pvp8,pvp9
					Store 0 To compUtente1,compUtente1,compUtente2,compUtente3,compUtente4,compUtente5,compUtente6,compUtente7,compUtente8,compUtente9
					Store 0 To myCompOrg1,myCompOrg2,myCompOrg3,myCompOrg4,myCompOrg5,myCompOrg6,myCompOrg7,myCompOrg8,myCompOrg9
					Store 0 To fee1,fee2,fee3,fee4,fee5,fee6,fee7,fee8,fee9, feetotal, feetotalbi, feetotaliva
					Store 0 To base1,base2,base3,base4,base5,base6,base7,base8,base9



					Select Fi
					Go Top
					Scan
						Do Case
							Case Fi.tabiva == 1
								pvp1 			= pvp1 + Fi.altura
								compUtente1 	= compUtente1 + Fi.largura
								myCompOrg1 		= myCompOrg1 +  Fi.etiliquido
								fee1 			= fee1 +  Fi.pvp4_fee

							Case Fi.tabiva == 2
								pvp2 			= pvp2 + Fi.altura
								compUtente2 	= compUtente2 + Fi.largura
								myCompOrg2 		= myCompOrg2 +  Fi.etiliquido
								fee2			= fee2 +  Fi.pvp4_fee

							Case Fi.tabiva == 3
								pvp3			= pvp3 + Fi.altura
								compUtente3 	= compUtente3 + Fi.largura
								myCompOrg3 		= myCompOrg3 +  Fi.etiliquido
								fee3 			= fee3 +  Fi.pvp4_fee

							Case Fi.tabiva == 4
								pvp4 			= pvp4 + Fi.altura
								compUtente4 	= compUtente4 + Fi.largura
								myCompOrg4 		= myCompOrg4 +  Fi.etiliquido
								fee4 			= fee4 +  Fi.pvp4_fee

							Case Fi.tabiva == 5
								pvp5 			= pvp5 + Fi.altura
								compUtente5 	= compUtente5 + Fi.largura
								myCompOrg5 		= myCompOrg5 +  Fi.etiliquido
								fee5 			= fee5 +  Fi.pvp4_fee

							Case Fi.tabiva == 6
								pvp6 			= pvp6 + Fi.altura
								compUtente6 	= compUtente6 + Fi.largura
								myCompOrg6 		= myCompOrg6 +  Fi.etiliquido
								fee6 			= fee6 +  Fi.pvp4_fee

							Case Fi.tabiva == 7
								pvp7 			= pvp5 + Fi.altura
								compUtente7 	= compUtente7 + Fi.largura
								myCompOrg7 		= myCompOrg7 +  Fi.etiliquido
								fee7 			= fee7 +  Fi.pvp4_fee

							Case Fi.tabiva == 8
								pvp8 			= pvp8 + Fi.altura
								compUtente8 	= compUtente8 + Fi.largura
								myCompOrg8 		= myCompOrg8 +  Fi.etiliquido
								fee8 			= fee8 +  Fi.pvp4_fee

							Case Fi.tabiva == 9
								pvp9 			= pvp5 + Fi.altura
								compUtente9 	= compUtente9 + Fi.largura
								myCompOrg9 		= myCompOrg9 +  Fi.etiliquido
								fee9 			= fee9 +  Fi.pvp4_fee
						Endcase

						If Alltrim(Fi.Design)=='Remunera��o Espec�fica'
							**MESSAGEBOX(fi.etiliquido)
							feetotal = Fi.etiliquido
							feetotalbi = Round(Fi.etiliquido / 1.06, 2)
							feetotaliva = Fi.etiliquido - Round(Fi.etiliquido / 1.06, 2)
						Endif

						If Fi.iva == 0 And Fi.epromo == .F.
							myIsentoIva = .T.
						Endif

						Select Fi
					Endscan




					*************************
					**** Calcula o Mes **********
					Select ft
					myMesDocumento = uf_Gerais_CalculaMes(Month(ft.fdata))


					Select Fi

					lcSql=""
					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_facturacao_ImprimefacturacaoEntidades '<<lcStampDoc>>'
					ENDTEXT

					If !uf_gerais_actGrelha("","tempFi",lcSql)
						uf_perguntalt_chama("Ocorreu uma anomalia a obter a informa��o para impress�o. Por favor contacte o suporte.","OK","",16)
						Return
					Endif

					Select ft
					Select tempFi
					Go Top

				Endif



				*************************
				**** Selecciona id_anf  **********
				If Used("uCrsCptorg")
					fecha("uCrsCptorg")
				Endif
				Create Cursor uCrsCptorg(id_anf c(10),abrev c(15), e_datamatrix bit)

				lcSql = ""
				TEXT TO lcSQL TEXTMERGE NOSHOW

				SELECT TOP 1
					id_anf,
					abrev ,
					e_datamatrix
				FROM cptorg (nolock)
				where u_no=<<lcNo>> and inactivo =0
				ORDER BY cptorgstamp DESC
				ENDTEXT
				If !uf_gerais_actGrelha("","uCrsCptorg", lcSql)
					uf_perguntalt_chama("Ocorreu uma anomalia a verificar o codigo da organiza��o. Por favor contacte o suporte.", "OK", "", 16)
					Return .F.
				Endif




				** Fatura SNS, Nota de Credito Entidades -
				Select td
				**IF (td.u_tipodoc == 1 OR td.u_tipodoc == 7) AND ft.no < 199 AND (lcnomeFicheiro == ""
				If lcnomeFicheiro == "factura_sns.frx" Or lcnomeFicheiro = "notacredito_sns.frx" Or lcnomeFicheiro = "notadebito_sns.frx"
					uf_imprimirgerais_dataMatrixCriaImagem(td.ndoc, td.u_tipodoc)
				Endif
				Select uCrsCptorg
				** apenas para entidades como o  SBSI
				If (Alltrim(lcnomeFicheiro) == "Factura_entidades.frx" And !Empty(uCrsCptorg.e_datamatrix))
					lcnomeFicheiro = "factura_entidades_datamatrix.frx"
					** Guardar report a imprimir - ficheiro **
					lcreport = Alltrim(myPath)+"\analises\" + Alltrim(lcnomeFicheiro)
					uf_imprimirgerais_dataMatrixCriaImagem_Entidades(td.ndoc, td.u_tipodoc,id_anf)
				Endif
			Endif

			** Ordena Factura Ordem Alfabetica
			If Empty(lcSemPerguntas)
				If !Empty(IMPRIMIRGERAIS.pageframe1.page1.chkOrdenacaoAlfabetica.Value)
					Select Fi
					Go Top
					Index On Design Tag Concat
				Else
					Select Fi
					Go Top
					Index On lordem Tag Concat
				Endif
			Else
			Endif



			Select Fi
			Select td
			Select ucrse1
			If (td.u_tipodoc)==1 And Upper(Alltrim(ucrse1.tipoempresa)) != "CLINICA"
				Select tempFi
				Go Top
			Else
				Select Fi
				Go Top
				Scan For Fi.iva == 0 And Fi.epromo == .F.
					myIsentoIva = .T.
				Endscan

				Select Fi
				Go Top
			Endif


			** Texto de Isen��o no IDU (calculado com base na nacionalidade do utente)
			lcSql = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				Select codigoP, noConvencao, designConvencao FROM b_utentes (nolock) WHERE b_utentes.no = <<ft.no>> and b_utentes.estab = <<ft.estab>>
			ENDTEXT
			If !uf_gerais_actGrelha("","uCrsDadosNacionalidadeUtente",lcSql)
				uf_perguntalt_chama("Ocorreu uma anomalia a verificar Nacionalidade do Utente. Por favor contacte o suporte.","OK","",16)
				Return .F.
			Endif
			Select uCrsDadosNacionalidadeUtente
			If Upper(Alltrim(uCrsDadosNacionalidadeUtente.codigoP)) != 'PT'
				myPaisNacional = .F.
			Else
				myPaisNacional = .T.
			Endif

			Select Fi
			Go Top
			Scan For u_psico==.T. Or u_benzo==.T.
				lcExistemPsicoBenzo = .T.
			Endscan

			** Motivo de Isencao de IVA Lu�s Leal 20150930
			Local lcMotIsencaoIva
			Public myMotIsencaoIva
			Store .F. To lcMotIsencaoIva
			Store '' To myMotIsencaoIva

			&& Verifica se deve imprimir motivo de isencao de Iva
			lcMotIsencaoIva = uf_gerais_getParameter("ADM0000000261","Bool")



			If lcMotIsencaoIva == .T.
				Do Case
					Case myIsentoIva == .F.
						myMotIsencaoIva = ''
					Case !Empty(Alltrim(ft2.motiseimp))
						myMotIsencaoIva = ft2.motiseimp
					Case ft.pais == 3
						myMotIsencaoIva = 'Isen��o prevista no n.� 3 do art.� 14.� do CIVA'
					Case ft.pais==2
						myMotIsencaoIva = 'Isen��o ao abrigo da al�nea a) do art.� 14.� do RITI'
					Otherwise
						**myMotIsencaoIva = ALLTRIM(ucrse1.motivo_isencao_iva)
						If !Empty(ucrse1.motivo_isencao_iva)
							myMotIsencaoIva = ucrse1.motivo_isencao_iva
						Else
							myMotIsencaoIva = 'Isen��o prevista no n.� 1 do art.� 9.� do CIVA'
						Endif
				Endcase
			Else
				myMotIsencaoIva = ''
			Endif


			** Calcula TxCambio para Impressoes a cliente estrangeiros
			Public myTxCambio
			myTxCambio = 0

			Select ft
			If (Upper(Alltrim(ft.moeda)) != 'EURO' And Upper(Alltrim(ft.moeda)) != 'PTE OU EURO' And Upper(Alltrim(ft.moeda)) != 'AOA')
				lcSql = ""
				TEXT TO lcSQL TEXTMERGE NOSHOW
					select top 1 cambio as taxa from cb where moeda = '<<ALLTRIM(ft.moeda)>>' order by data + ousrhora desc
				ENDTEXT
				If !uf_gerais_actGrelha("","uCrsTxCambio",lcSql)
					uf_perguntalt_chama("Ocorreu uma anomalia a verificar Taxa de Cambio. Por favor contacte o suporte.","OK","",16)
					Return .F.
				Endif
				Select uCrsTxCambio
				Go Top
				myTxCambio = uCrsTxCambio.taxa
			Else
				myTxCambio = 1
			Endif



			** criar cursores para recibo
		Case Alltrim(lctabela) == "RE"
			If Used("uCrsRecibos")
				Select UCRSRECIBOS
				Locate For UCRSRECIBOS.sel == .T.
				If Found()
					lcStampDoc = Alltrim(UCRSRECIBOS.restamp)
					uf_gerais_actGrelha("","uCrsRePrint","exec up_print_re '"+lcStampDoc+"'")
					uf_gerais_actGrelha("","uCrsRlPrint","select * from rl (nolock) where restamp = '"+lcStampDoc+"'  order by datalc, cm, nrdoc ")
				Else
					If Used("uCrsReCons")
						If !Empty(Alltrim(uCrsReCons.restamp))
							lcStampDoc = Alltrim(uCrsReCons.restamp)
							uf_gerais_actGrelha("","uCrsRePrint","exec up_print_re '"+lcStampDoc+"'")
							uf_gerais_actGrelha("","uCrsRlPrint","select * from rl (nolock) where restamp = '"+lcStampDoc+"'  order by datalc, cm, nrdoc")
						Else
							Return .F.
						Endif
					Else
						Return .F.
					Endif
				Endif
				lcNomeFicheiroPDF= Alltrim(uCrsRePrint.Nmdoc) + " " + Alltrim(Str(uCrsRePrint.rno))+".PDF"
				lcTabCab = "RL"



				Local lcNrReCons
				lcNrReCons = 0
				If(Used("uCrsReCons"))
					Select uCrsReCons
					lcNrReCons =Reccount("uCrsReCons")
				Endif

				Local lcNrRePrint
				lcNrRePrint= 0
				If(Used("uCrsRePrint"))
					Select uCrsRePrint
					lcNrRePrint=Reccount("uCrsRePrint")
				Endif

				If(lcNrReCons>0)
					Select uCrsReCons
					lcNo= uCrsReCons.no
					lcEstab = uCrsReCons.estab
				Else
					If(lcNrRePrint>0)
						Select uCrsRePrint
						lcNo= uCrsRePrint.no
						lcEstab = uCrsRePrint.estab
					Endif
				Endif

			Else
				Return .F.
			Endif

			** criar cursores para listagens associados
		Case Alltrim(lctabela) == "CL_FATC_ORG"
			If lcnomeFicheiro == 'Associados_Resumo_Faturas_Associados.frx'
				Select uCrsDocAssociados
				Go Top

				lcSql = ""
				TEXT TO lcSQL TEXTMERGE NOSHOW
					select
						id_cl = b_utentes.id
						,fi.design, fi.etiliquido, fi.fistamp
						,ft.ndoc, ft.nmdoc
					from
						fi (nolock)
						inner join ft (nolock) on ft.ftstamp = fi.ftstamp
						inner join b_utentes (nolock) on b_utentes.no = ft.no and b_utentes.estab = ft.estab
					where
						ft.no = <<uCrsDocAssociados.nr_cl>> and ft.estab = <<uCrsDocAssociados.estab_cl>>
						and ft.ftano = <<uCrsDocAssociados.ano>> and month(ft.fdata) = <<uCrsDocAssociados.mes>>
				ENDTEXT
				If !uf_gerais_actGrelha("","uCrsDocAssociadosFaturado", lcSql)
					uf_perguntalt_chama("Ocorreu uma anomalia a verificar a fatura��o do associado. Por favor contacte o suporte.", "OK", "", 16)
					Return .F.
				Else
					Select uCrsDocAssociados.*, uCrsDocAssociadosFaturado.Design From uCrsDocAssociadosFaturado inner Join uCrsDocAssociados On uCrsDocAssociados.id_cl = uCrsDocAssociadosFaturado.id_cl Into Cursor uCrsDocAssociadosDetalhe
					Select uCrsDocAssociadosDetalhe
					&&brow
				Endif
			Endif
		Case Alltrim(lctabela) == "ext_sinave"
			lcTabCab = "uCrsComSinave"
			If !Empty(uCrsComSinave.rnu)
				lcNomeFicheiroPDF= Left(lcnomeFicheiro,At('.',lcnomeFicheiro)-1)+ "_" + Alltrim(Str(uCrsComSinave.rnu))+ "_" + uf_gerais_getdate(Date(),"SQL") +".PDF"
			Else
				lcNomeFicheiroPDF= Left(lcnomeFicheiro,At('.',lcnomeFicheiro)-1)+ "_" + Alltrim(uCrsComSinave.healthUserId)+ "_" + uf_gerais_getdate(Date(),"SQL") +".PDF"
			Endif
			**
	Endcase


	** se o documento for requisi��o de psicotr�picos e existirem psicos ou benzos incrementa contador
	If lcnomeFicheiro == "requisicao_cliente_psicobenzo.frx" Or lcnomeFicheiro == "requisicao_fornecedor_psicobenzo.frx"
		If (!lcExistemPsicoBenzo) && se n�o existirem psicos ou benzos n�o imprime nada
			uf_perguntalt_chama("N�o existem psicotr�picos nem benzodiazepinas nas linhas", "OK", "", 48)
			Return .F.
		Endif

		If lcnomeFicheiro = "requisicao_cliente_psicobenzo.frx"
			Select * From Fi Where (u_psico = .T. Or u_benzo = .T.) Into Cursor uCrsAuxFi Readwrite
		Endif

		If lcnomeFicheiro = "requisicao_fornecedor_psicobenzo.frx"
			Select * From BI Where (psico = .T. Or benzo = .T.) Into Cursor uCrsAuxBi  Readwrite
		Endif

		lcEntradaPsicoBenzo = Iif(lcnomeFicheiro = "requisicao_cliente_psicobenzo.frx", 0, 1)

		lcSql = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			Select contador, ano
			from doc_psicobenzo_contreq (nolock)
			WHERE stamp = '<<lcStampDoc>>'
		ENDTEXT
		If !uf_gerais_actGrelha("","uCrsContaReqPsicoBenzo",lcSql)
			uf_perguntalt_chama("Ocorreu uma anomalia a verificar contador de requisi��es de psicotr�picos e benzodiazepinas. Por favor contacte o suporte.","OK","",16)
			Return .F.
		Endif
		Select uCrsContaReqPsicoBenzo
		If Reccount("uCrsContaReqPsicoBenzo") == 0 && contador ainda n�o existe (nova impress�o)
			TEXT TO lcSQL TEXTMERGE NOSHOW
				INSERT INTO doc_psicobenzo_contreq
					(stamp, entrada, ano, contador)
				select
					'<<lcStampDoc>>', <<lcEntradaPsicoBenzo>>, year(dateadd(HOUR, <<difhoraria>>, getdate()))
					,contador = (select isnull(MAX(contador)+1,1)
								from doc_psicobenzo_contreq (nolock)
								where ano = year(dateadd(HOUR, <<difhoraria>>, getdate())))

				Select contador, ano
				from doc_psicobenzo_contreq (nolock)
				WHERE stamp = '<<lcStampDoc>>'
			ENDTEXT
			If !uf_gerais_actGrelha("","uCrsContaReqPsicoBenzo",lcSql)
				uf_perguntalt_chama("Ocorreu um erro a inserir registo de contador de requisi��es de psicotr�picos e benzodiazepinas. Por favor contacte o suporte.", "OK", "", 16)
				Return .F.
			Endif
		Endif
		Select uCrsContaReqPsicoBenzo
		Go Top
	Endif
	**


	Do Case
		Case uf_gerais_compstr(MYIMPRESSAOORIGEM, "Atendimento")
			uf_gerais_TextoRodape(td.tiposaft)
		Case MYIMPRESSAOORIGEM == "FACTURACAO"
			uf_gerais_TextoRodape(td.tiposaft)

		Case MYIMPRESSAOORIGEM == "DOCUMENTOS"
			uf_gerais_TextoRodape(ucrsConfigDoc.bdempresas)

		Case MYIMPRESSAOORIGEM == "RECIBO"
			uf_gerais_TextoRodape('RE')

		Case MYIMPRESSAOORIGEM == "PAGAMENTO"
			uf_gerais_TextoRodape('PO')

		Case MYIMPRESSAOORIGEM == "DOCASSOCIADOS";
				OR MYIMPRESSAOORIGEM == "RESUMOFTENTIDADES";
				OR MYIMPRESSAOORIGEM == "RESUMOFTASSOCIADOS";
				OR MYIMPRESSAOORIGEM == "RESUMORTASSOCIADOS"

			uf_gerais_TextoRodape("CL_FATC_ORG")
	Endcase

	If Alltrim(myVersaoDoc) = 'ORIGINAL'
		myVersaoDoc = uf_gerais_versaodocumento(1)
	Endif



	Set Procedure To Locfile("FoxBarcodeQR.prg") Additive
	*--- Create FoxBarcodeQR private object
	Private poFbc
	m.poFbc = Createobject("FoxBarcodeQR")


	*-- New properties
	m.poFbc.nCorrectionLevel = 3 && Level_H
	m.poFbc.nBarColor = Rgb(0, 0, 0) && BLACK

	Create Cursor TempQR (TempQR I)
	Insert Into TempQR Values (0)


	** Colocar cursores linhas no inicio **
	Select ucrse1
	Go Top

	Do Case
		Case Alltrim(lcnomeFicheiro) == "pedidoregularizacao.frx"
			Select ucrsPedidoRegularizacao
			Go Top

		Case Alltrim(lctabela) == "FT"
			If (td.u_tipodoc)==1 And Upper(Alltrim(ucrse1.tipoempresa)) != "CLINICA"
				Do Case
					Case (lcnomeFicheiro) == "talaoatend.frx"
						Select FTprint
						Go Top
						Select FIprint
						Go Top
					Case (lcnomeFicheiro) == "talaoreserva.frx"
						Select Reservaprint
						Go Top
					Otherwise
						Select tempFi
				Endcase

			Else
				If (lcnomeFicheiro) == "requisicao_cliente_psicobenzo.frx"
					Select uCrsAuxFi
					Go Top
				Else
					If (lcnomeFicheiro) == "talaoatend.frx"
						Select FTprint
						Go Top
						Select FIprint
						Go Top
					Else
						Select Fi
						Go Top
					Endif
				Endif
			Endif

		Case Alltrim(lctabela) == "FO"
			Select FN
			Go Top

		Case Alltrim(lctabela) == "BO"
			If lcnomeFicheiro == "entregaclientespiking.frx"
				Select ucrsTempBi
				Go Top
			Else
				If Alltrim(lcnomeFicheiro) == "requisicao_fornecedor_psicobenzo.frx"
					Select uCrsAuxBi
					Go Top
				Else
					Select BI
					Go Top
				Endif
			Endif

		Case Alltrim(lctabela) == "RE"
			Select uCrsRePrint
			Go Top
			Select uCrsRlPrint
			Go Top

		Case Alltrim(lctabela) == "PO"
			Select PO
			Go Top
			Select PL
			Go Top

		Case Alltrim(lctabela) == "CL_FATC_ORG"
			If Used([uCrsDocAssociadosFaturado])
				Select uCrsDocAssociadosFaturado
				Go Top
				Select uCrsDocAssociadosDetalhe
				Go Top
			Endif
			Select uCrsDocAssociados
			Go Top

		Case Upper(Alltrim(MYIMPRESSAOORIGEM)) = "ETIQUETAS_STOCKS"

			Create Cursor uCrsProdEtiq (ref c(60), Design c(60), pvp N(9,2), iva N(6,2), forref c(20), oriref c(60), unidade c(40), conversao N(8,2))

			Select st
			For I=1 To IMPRIMIRGERAIS.pageframe1.page1.spnNO.Value
				Insert Into uCrsProdEtiq (ref, Design, pvp, iva, forref, oriref, unidade, conversao) Values ('*'+Alltrim(st.ref)+'*', Alltrim(st.Design), st.epv1, uf_gerais_getTabelaIva(st.tabiva,"TAXA"), Alltrim(st.forref), Alltrim(st.ref), Alltrim(st.unidade), st.conversao)
			Endfor

			Select uCrsProdEtiq
			Go Top

		Case Upper(Alltrim(MYIMPRESSAOORIGEM)) = "DOCS_ETIQUETAS"

			Create Cursor uCrsProdEtiq (ref c(60), Design c(60), pvp N(9,2), iva N(6,2), forref c(20), oriref c(60), unidade c(40), conversao N(8,2))

			Select BI
			Go Top
			Scan
				For I=1 To BI.qtt
					**SELECT '*'+alltrim(bi.ref)+'*' as ref, design, edebito as pvp, iva, forref, '' as oriref, unidade, 0.00 as conversao FROM bi INTO CURSOR uCrsProdEtiq READWRITE
					**Insert Into uCrsProdEtiq (ref, design, pvp, iva, forref, oriref, unidade, conversao) Values ('*'+alltrim(st.ref)+'*', alltrim(st.design), st.epv1, uf_gerais_getTabelaIva(st.tabiva,"TAXA"), alltrim(st.forref), ALLTRIM(st.ref), ALLTRIM(st.unidade), st.conversao)
					Insert Into uCrsProdEtiq (ref, Design, pvp, iva, forref, oriref, unidade, conversao) Values ('*'+Alltrim(BI.ref)+'*' , BI.Design, BI.edebito, uf_gerais_getTabelaIva(BI.tabiva,"TAXA"), Alltrim(BI.forref), Alltrim(BI.ref), Alltrim(BI.unidade), 0.00)
				Endfor
			Endscan

			Select uCrsProdEtiq
			Go Top



		Case Upper(Alltrim(MYIMPRESSAOORIGEM)) = "ERROSFATURA"

			Select uc_errosCab
			Go Top

			Select uc_errosLin
			Go Top

		Case Upper(Alltrim(MYIMPRESSAOORIGEM)) = "ERROSFATURAENTMES"

			Select uc_errosCab
			Go Top

			Select uc_errosLin
			Go Top

		Case Upper(Alltrim(MYIMPRESSAOORIGEM)) = "ERROSFATURAMES"

			Select uc_errosCab
			Go Top

			Select uc_errosLin
			Go Top

		Case Upper(Alltrim(MYIMPRESSAOORIGEM)) = "ERROSFACTELEC"

			Select uc_errosXMLDesc
			Go Top

	Endcase


	**
	** preparar impressora **
	If  !uf_gerais_compstr(lcprinter,"Impressora por defeito do Windows")

		Local lcValErroPrinter
		Try
			Set Printer To Name (Alltrim(lcprinter))
		Catch
			uf_perguntalt_chama("Foi detectado algum problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
			lcValErroPrinter = .T.
		Endtry
		If lcValErroPrinter == .T.
			Return .F.
		Endif
	Endif

	Set Device To Print
	Set Console Off
	**
	** IMPRIMIR
	If Like(lctipo,"print")
		** ENVIA PARA PDF
		If Type("IMPRIMIRGERAIS")!="U"
			If IMPRIMIRGERAIS.pageframe1.page1.chkParaPDF.Value=1
				If IMPRIMIRGERAIS.pageframe1.page1.chkPorEmail.Value=0
					Local lcExpFolder
					Store '' To lcExpFolder
					If Alltrim(myVersaoDoc) = 'ORIGINAL'
						myVersaoDoc = uf_gerais_versaodocumento(1)
					Endif
					If Type("FTSeguradora")!='U'
						If Alltrim(FTSeguradora) = 'GUARDA'
							Select ft
							lcNomeFicheiroPDF = Alltrim(Str(ft.fno))+'_'+Alltrim(Str(ft.ndoc))+'_'+Alltrim(Str(Year(ft.fdata)))+Alltrim(Str(Month(ft.fdata)))+Alltrim(Str(Day(ft.fdata)))+'_fatura.pdf'

						Endif
						If Alltrim(FTSeguradora) == 'HIST'
							Select ft
							lcNomeFicheiroPDF = Alltrim(Str(ft.fno))+'_'+Alltrim(Str(ft.ndoc))+'_'+Alltrim(Str(Year(ft.fdata)))+Alltrim(Str(Month(ft.fdata)))+Alltrim(Str(Day(ft.fdata)))+'.pdf'
							lcExpFolder = uf_gerais_getParameter_site('ADM0000000100', 'TEXT', mySite)

						Endif
						If Alltrim(FTSeguradora) == 'DOC'

							Local lcFtStampDoc, lcNomeFicheirolt, lcExpFolder
							Select ft
							lcFtStampDoc = Alltrim(astr(ft.ftstamp))

							lcNomeFicheiroPDF = Alltrim(ucrse1.id_lt) +"_"+ lcFtStampDoc +'.pdf'
							lcExpFolder = Alltrim(uf_gerais_getParameter("ADM0000000015","TEXT") + "\Cache\")

						Endif
					Endif

					Local lcGuardaCache
					lcGuardaCache = .T.
					If IMPRIMIRGERAIS.pageframe1.page1.chkCertificar.Value=1 And Upper(Alltrim(MYIMPRESSAOORIGEM)) = "FACTURA"
						lcNomeFicheiroPDF = uf_gerais_removeCharEspecial(Alltrim(Strtran(Strtran(Alltrim(ft.Nmdoc), ' ', '_'), '/',''))+'_'+Alltrim(Str(ft.fno)) + '_' + Alltrim(ft.ftstamp)+'.pdf')
						lcExpFolder = Alltrim(uf_gerais_getParameter("ADM0000000015","TEXT") + "\Cache\PDF\")
					Else
						lcGuardaCache = .F.
					Endif

					If !Empty(lcNomePDF)
						lcNomeFicheiroPDF = Alltrim(Justfname(lcNomePDF))

						If !Empty(Justpath(lcNomePDF))
							lcExpFolder = Justpath(lcNomePDF) + "\"
							lcGuardaCache = .T.
						Endif
					Else
						If  Upper(Alltrim(MYIMPRESSAOORIGEM)) = "PAGAMENTO"

							If(Used("PL"))
								Select PL
								Go Top
								lcTabCab = 'PL'
							Endif

							Select PO
							Go Top
							lcNomeFicheiroPDF = uf_gerais_removeCharEspecial(Alltrim(Strtran(Strtran(Alltrim(PO.cmdesc), ' ', '_'), '/',''))+'_'+Alltrim(Str(PO.rno)) + '_' + Alltrim(PO.postamp)+'.pdf')
						Endif
					Endif

					uf_guarda_pdf(Alltrim(lcreport), Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ',''), lcGuardaCache , lcTabCab, Alltrim(lcExpFolder) )

					If IMPRIMIRGERAIS.pageframe1.page1.chkCertificar.Value=1 And Upper(Alltrim(MYIMPRESSAOORIGEM)) = "FACTURA"
						If !uf_facturacao_certficarDocumento(Alltrim(uf_gerais_getUmValor("B_Terminal", "LTRIM(RTRIM(pathExport))", "terminal = '" + Alltrim(myTerm) + "'")) + "\"+Alltrim(Juststem(lcNomeFicheiroPDF))+"_signed.pdf", Alltrim(lcExpFolder) + Alltrim(lcNomeFicheiroPDF), Alltrim(Juststem(lcNomeFicheiroPDF))+"_signed.pdf")
							uf_perguntalt_chama("Ocorreu um erro na certifica��o de Documento(S)","OK","",16)
							Return .F.
						Endif
					Endif
				Else
					If lcPodeEnviarEmail = .T.

						If ucrse1.usacoms=.T.
							If !Empty(IMPRIMIRGERAIS.pageframe1.page1.emaildestino.Value)
								Local mDir, lcTo, lcToEmail, lcSubject, lcBody, lcAtatchment
								mDir = Alltrim(uf_gerais_getParameter('ADM0000000015','TEXT')) + "\Cache\PDF"
								lcTo = ""
								lcToEmail = Alltrim(IMPRIMIRGERAIS.pageframe1.page1.emaildestino.Value)
								lcSubject = Alltrim(IMPRIMIRGERAIS.pageframe1.page1.emailsubject.Value)
								lcBody = Alltrim(IMPRIMIRGERAIS.pageframe1.page1.emailbody.Value)
								lcAtatchment = mDir + "\"+Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ','')
								If Alltrim(myVersaoDoc) = 'ORIGINAL'
									myVersaoDoc = uf_gerais_versaodocumento(1)
								Endif

								If IMPRIMIRGERAIS.pageframe1.page1.chkCertificar.Value=1 And Upper(Alltrim(MYIMPRESSAOORIGEM)) = "FACTURA"
									lcNomeFicheiroPDF = uf_gerais_removeCharEspecial(Alltrim(Strtran(Strtran(Alltrim(ft.Nmdoc), ' ', '_'), '/',''))+'_'+Alltrim(Str(ft.fno)) + '_' + Alltrim(ft.ftstamp)+'.pdf')
								Endif

								If  Upper(Alltrim(MYIMPRESSAOORIGEM)) = "PAGAMENTO"
									Select PL
									Go Top
									lcNomeFicheiroPDF = uf_gerais_removeCharEspecial(Alltrim(Strtran(Strtran(Alltrim(PO.cmdesc), ' ', '_'), '/',''))+'_'+Alltrim(Str(PO.rno)) + '_' + Alltrim(PO.postamp)+'.pdf')
									lcAtatchment = Alltrim(mDir) + "\" + Alltrim(Juststem(lcNomeFicheiroPDF))+'.pdf'
								Endif

								uf_guarda_pdf(Alltrim(lcreport), Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ',''), .T. , lcTabCab)

								If IMPRIMIRGERAIS.pageframe1.page1.chkCertificar.Value=1 And Upper(Alltrim(MYIMPRESSAOORIGEM)) = "FACTURA"
									If !uf_facturacao_certficarDocumento('', Alltrim(mDir) + "\" + Alltrim(lcNomeFicheiroPDF), Alltrim(Juststem(lcNomeFicheiroPDF))+"_signed.pdf")
										uf_perguntalt_chama("Ocorreu um erro na certifica��o de Documento(S)","OK","",16)
										Return .F.
									Endif

									lcAtatchment = Alltrim(mDir) + "\" + Alltrim(Juststem(lcNomeFicheiroPDF))+"_signed.pdf"
								Endif

								If(!uf_gerais_validaEmailRgpd(lcToEmail,lcNo,lcEstab, lctabela ))
									Return .F.
								Endif
								regua(0,6,"A enviar email ...")
								**uf_imprimirgerais_imprimir
								If uf_gerais_compstr(MYIMPRESSAOORIGEM, "Atendimento")
									uf_imprimirgerais_guardaCursorAnexos(lcTo, lcToEmail, lcSubject, lcBody , lcAtatchment)
								Else
									uf_startup_sendmail_emp(lcTo, lcToEmail, lcSubject, lcBody , lcAtatchment, .T.)
								Endif


								If At("EncomendaaFornecedor", Alltrim(lcAtatchment)) <> 0
									If Used("cabdoc")
										TEXT TO lcSQL TEXTMERGE NOSHOW
											UPDATE bo SET logi1=1 WHERE bostamp='<<ALLTRIM(cabdoc.cabstamp)>>'
										ENDTEXT
										If !uf_gerais_actGrelha("", "", lcSql)
											uf_perguntalt_chama("ERRO AO ATUALIZAR O ENVIO DA ENCOMENDA!","OK","",16)
											Return .F.
										Endif
									Endif
								Endif
								regua(2)
								If lcNo=0
									TEXT TO lcSQL TEXTMERGE NOSHOW
										SELECT utstamp FROM b_utentes where no=<<lcNo>> and estab=<<lcEstab>>
									ENDTEXT
									If !uf_gerais_actGrelha("", "ucrsUtStamp", lcSql)
										uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!","OK","",16)
										Return .F.
									Endif
									uf_user_log_ins('CL', Alltrim(ucrsUtStamp.utstamp), 'EMAIL', 'Envio de documento por email - ' + Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ',''))
								Endif
								shellexecute(0,"open","del "+Alltrim(lcAtatchment)+" /q ","","",1)
							Else
								uf_perguntalt_chama("N�o existe um email para envio preenchido." + Chr(13) + "Insira um ou atualize a ficha respetiva.","OK","",64)
								Return .F.
							Endif
						Else
							uf_perguntalt_chama("PARA UTILIZAR ESTA FUNCIONALIDADE DEVER� ADQUIRIR O M�DULO DE COMUNICA��ES." + Chr(13) + "PARA MAIS INFORMA��ES POR FAVOR CONTACTE A LOGITOOLS (OP��O - COMERCIAL).", "OK","",16)
						Endif
					Else
						uf_perguntalt_chama("LAMENTAMOS MAS N�O VAI SER POSS�VEL EFETUAR O ENVIO." + Chr(13) + "O UTENTE N�O AUTORIZA O ENVIO DE EMAILS.", "OK","",16)
					Endif
				Endif
			Else
				** Trata Duplicados
				If lctemduplicados == .T. And lcnumduplicados > 0

					Try
						For I=0 To lcnumduplicados
							Do Case
								Case I == 0
									myVersaoDoc = uf_gerais_versaodocumento(1)
									Report Form Alltrim(lcreport) To Printer Nopageeject
								Case I < lcnumduplicados
									myVersaoDoc = uf_gerais_versaodocumento(I+1)
									**REPORT FORM ALLTRIM(lcreport) TO PRINTER NORESET NOPAGEEJECT
									Report Form Alltrim(lcreport) To Printer Nopageeject
								Case I == lcnumduplicados
									myVersaoDoc = uf_gerais_versaodocumento(lcnumduplicados+1)
									**REPORT FORM ALLTRIM(lcreport) TO PRINTER NORESET
									Report Form Alltrim(lcreport) To Printer
							Endcase
						Endfor
					Catch

						uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
					Endtry
				Else
					Try
						If !Used("ltBO2")
							*!*								SET PROCEDURE TO LOCFILE("FoxBarcodeQR.prg") ADDITIVE
							*!*								*--- Create FoxBarcodeQR private object
							*!*								PRIVATE poFbc
							*!*								m.poFbc = CREATEOBJECT("FoxBarcodeQR")

							*!*								*-- New properties
							*!*								m.poFbc.nCorrectionLevel = 3 && Level_H
							*!*								m.poFbc.nBarColor = RGB(0, 128, 0) && Green

							*!*								CREATE CURSOR TempQR (TempQR I)
							*!*								INSERT INTO TempQR VALUES (0)

							Report Form Alltrim(lcreport) To Printer

							*!*								USE IN TempQR

							*!*								m.poFbc = NULL

						Else
							If myMulticopia
								Public totimp, nrimp
								nrimp = 1
								totimp = ltBO2.nrviasimp
								If ltBO2.nrviasimp<=1
									Report Form Alltrim(lcreport) To Printer
								Else
									For I = 1 To totimp
										Report Form Alltrim(lcreport) To Printer
										nrimp = nrimp + 1
									Endfor
								Endif
							Else

								Report Form Alltrim(lcreport) To Printer
							Endif
						Endif
					Catch
						uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
					Endtry
				Endif
			Endif
		Else
			** Trata Duplicados
			If lctemduplicados == .T. And lcnumduplicados > 0

				Try
					For I=0 To lcnumduplicados
						Do Case
							Case I == 0
								myVersaoDoc = uf_gerais_versaodocumento(1)
								If Empty(myFileNameSeq)
									Report Form Alltrim(lcreport) To Printer  Nopageeject
								Else
									lcNomeFicheiroPDF= Alltrim(myFileNameSeq) + ".PDF"

									uf_guarda_pdf_seldir(Alltrim(lcreport), Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ',''), Alltrim(myFileTableSeq) , Alltrim(myFilePathSeq), lcTabCab)
								Endif

							Case I < lcnumduplicados
								myVersaoDoc = uf_gerais_versaodocumento(I+1)
								If Empty(myFileNameSeq)
									Report Form Alltrim(lcreport) To Printer  Nopageeject
								Else
									lcNomeFicheiroPDF= Alltrim(myFileNameSeq) + "(" + Alltrim(myVersaoDoc) + ").PDF"

									uf_guarda_pdf_seldir(Alltrim(lcreport), Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ',''), Alltrim(myFileTableSeq) , Alltrim(myFilePathSeq), lcTabCab)
								Endif
							Case I == lcnumduplicados
								myVersaoDoc = uf_gerais_versaodocumento(lcnumduplicados+1)
								If Empty(myFileNameSeq)
									Report Form Alltrim(lcreport) To Printer  Nopageeject
								Else
									lcNomeFicheiroPDF= Alltrim(myFileNameSeq) + "(" + Alltrim(myVersaoDoc) + ").PDF"


									uf_guarda_pdf_seldir(Alltrim(lcreport), Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ',''), Alltrim(myFileTableSeq) , Alltrim(myFilePathSeq), lcTabCab)
								Endif
						Endcase
					Endfor
				Catch
					uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
				Endtry
			Else
				If Vartype(myFileNameSeq)=='U'
					Try
						Report Form Alltrim(lcreport) To Printer
					Catch
						uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
					Endtry
				Else
					If Empty(myFileNameSeq)
						Try
							Report Form Alltrim(lcreport) To Printer
						Catch
							uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
						Endtry
					Else
						If Alltrim(myVersaoDoc) = 'ORIGINAL'
							myVersaoDoc = uf_gerais_versaodocumento(1)
						Endif
						**lcNomeFicheiroPDF= ALLTRIM(myFileNameSeq) + " " + ALLTRIM(STR(ft.fno))+".PDF"
						lcNomeFicheiroPDF= Alltrim(myFileNameSeq) + ".PDF"

						If Alltrim(Upper(myFileTableSeq)) = "ERROSFATURA"
							Select uc_errosLin
							Go Top
						Endif

						uf_guarda_pdf_seldir(Alltrim(lcreport), Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ',''), Alltrim(myFileTableSeq) , Alltrim(myFilePathSeq), lcTabCab)
					Endif
				Endif
			Endif
		Endif

	Endif

	** PREVIS�O
	If Like(lctipo,"prev")
		Set REPORTBEHAVIOR 90
		Try
			If Alltrim(lcreport) == Alltrim(myPath)+"\analises\levantamento_exames.frx"
				lcFiStamps = ""
				Select Fi
				Go Top
				Scan
					lcFiStamps = lcFiStamps + Iif(Empty(lcFiStamps),"",",") + [']+ Alltrim(Fi.fistamp) +[']
				Endscan

				TEXT TO lcSQL NOSHOW TEXTMERGE
					select ref, design from marcacoesServ where levantamento = 1 AND ufistamp in (<<lcFiStamps>>)
				ENDTEXT

				If !uf_gerais_actGrelha("","uCrsRefsMarcacoes",lcSql)
					uf_perguntalt_chama("Ocorreu um erro a imprimir tal�o de levantamento.", "OK", "", 16)
					Return .F.
				Endif

			Endif
			Report Form Alltrim(lcreport) To Printer Prompt Nodialog Preview
		Catch

			uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
		Endtry
		Set REPORTBEHAVIOR 80
	Endif

	Use In TempQR
	m.poFbc = Null

	** por impressora no default **
	Set Device To Screen
	Set Printer To Name ''+lcDefaultPrinter+''
	Set Console On
	**

	If Type("FACTURACAO") != 'U'
		Select ft2
		Replace ft2.impresso With .T.
		lcSql = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			UPDATE ft2 SET impresso=1 WHERE ft2.ft2stamp='<<ALLTRIM(ft2.ft2stamp)>>'
		ENDTEXT
		uf_gerais_actGrelha("","",lcSql)
	Endif

	If Used("doc_psicobenzo_contreq")
		fecha("doc_psicobenzo_contreq")
	Endif

	Release myMotIsencaoIva

Endfunc

Function uf_imprimirGerais_imprimir_gerais_release_variables


	Release lnTot, mes
	Release pvp1,pvp2,pvp3,pvp4,pvp5,pvp6,pvp7,pvp8,pvp9
	Release compUtente1,compUtente2,compUtente3,compUtente4,compUtente5,compUtente6,compUtente7,compUtente8,compUtente9
	Release myCompOrg1,myCompOrg2,myCompOrg3,myCompOrg4,myCompOrg5,myCompOrg6,myCompOrg7,myCompOrg8,myCompOrg9
	Release fee1,fee2,fee3,fee4,fee5,fee6,fee7,fee8,fee9, feetotal, feetotalbi, feetotaliva
	Release base1,base2,base3,base4,base5,base6,base7,base8,base9

Endfunc





** funcao p imprimir receitas m�dulo prescricao eletr�nica
Function uf_imprimirGerais_imprimir_Pe
	Lparameters lctipo, lcSemPerguntas

	Public myFolha, myLogoPathReceita, myImgVersaoSoftware, myLogoPathReceitaSNS, myLtsPrescricao, myempvertical
	Local lcTipoReceita
	lcTipoReceita = 0

	** Calcula Caminho Imagens
	myImgVersaoSoftware = myPath + "\imagens\Pe\peversaosoftware13.png"
	myImgMinSaude = myPath + "\imagens\Pe\GovernoMinSaude_PeB.png"
	myImgRepublica = myPath + "\imagens\Pe\Republica_Portuguesa.png"

	lcSubRegiao = uf_gerais_getParameter('ADM0000000238', 'TEXT')

	Do Case
		Case Empty(lcSubRegiao) Or Upper(Alltrim(lcSubRegiao)) == "CONTINENTE"
			myLogoPathReceita = myPath + "\imagens\Pe\governominsaude_peb.png"
		Case Upper(Alltrim(lcSubRegiao)) == "A�ORES"
			myLogoPathReceita = myPath + "\imagens\Pe\SRSAcores.PNG"
		Case Upper(Alltrim(lcSubRegiao)) == "MADEIRA"
			myLogoPathReceita = myPath + "\imagens\Pe\SRSMadeira.png"
	Endcase

	myLtsPrescricao = myPath + 	"\imagens\Pe\lts_prescricaoeletronica.png"
	myLogoPathReceitaSNS = myPath + "\imagens\Pe\logo_sns.png"
	myempvertical= myPath + "\imagens\Pe\nempvertical.bmp"

	**

	Public myRecImprimeTodas

	If myRecImprimeTodas == .F.
		Select ucrsLinPresc

		myFolha = 1

		If ucrsLinPresc.receitamcdt == .F.
			uf_imprimirGerais_imprimirMedIndividual(lctipo,1, lcSemPerguntas)
		Else
			uf_imprimirGerais_imprimirMCDTIndividual(lctipo,1, lcSemPerguntas)
		Endif
	Else
		If Upper(Alltrim(lctipo)) == "PREV"

			Select ucrsLinPresc
			Go Top
			Scan For Left(Alltrim(ucrsLinPresc.Design),1) == "."
				If ucrsLinPresc.receitamcdt == .T.
					uf_prescricao_imprimirReceitaMcdt(.T.) &&Refresca informa��o da receita e prepara cursores
					lcTipoReceita = 2
				Else
					If Upper(Left(Alltrim(ucrsLinPresc.tiporeceita),1)) != 'L'
						uf_prescricao_imprimirReceitaMed(.T.) &&Refresca informa��o da receita e prepara cursores para receitas materializadas
					Else
						uf_prescricao_imprimirReceitaMedRSP() &&Refresca informa��o da receita e prepara cursores para receitas desmaterializadas
					Endif
					lcTipoReceita = 1
				Endif

				Exit
			Endscan
			If lcTipoReceita = 2
				uf_imprimirGerais_imprimirMCDTIndividual("PREV")
			Endif
			If lcTipoReceita = 1
				uf_imprimirGerais_imprimirMedIndividual("PREV")
			Endif
		Else

			Public myFolha
			Local lcNumeroVias, lcTotalFolhas, lcNumeroViasMCDT
			Store 0 To lcNumeroVias, lcTotalFolhas, myFolha, lcNumeroViasMCDT

			Select ucrsLinPresc
			Go Top
			Calculate Sum(vias) For Left(Alltrim(ucrsLinPresc.Design),1) == "." And Empty(ucrsLinPresc.receitamcdt) And ((ucrsLinPresc.estado == "1") Or (ucrsLinPresc.estado == "3")) To lcNumeroVias

			Select ucrsLinPresc
			Go Top
			Calculate Sum(vias) For Left(Alltrim(ucrsLinPresc.Design),1) == "." And !Empty(ucrsLinPresc.receitamcdt) And ((ucrsLinPresc.estado == "1") Or (ucrsLinPresc.estado == "3")) To lcNumeroViasMCDT

			lcTotalFolhas = lcNumeroVias
			myFolha = 1

			Select ucrsLinPresc
			Go Top
			Scan For Left(Alltrim(ucrsLinPresc.Design),1) == "." And Empty(ucrsLinPresc.receitamcdt) And ((ucrsLinPresc.estado == "1") Or (ucrsLinPresc.estado == "3"))

				If Upper(Left(Alltrim(ucrsLinPresc.tiporeceita),1)) != 'L'
					uf_prescricao_imprimirReceitaMed(.T.) &&Refresca informa��o da receita e prepara cursores para receitas materializadas
				Else
					uf_prescricao_imprimirReceitaMedRSP() &&Refresca informa��o da receita e prepara cursores para receitas desmaterializadas
				Endif
				uf_imprimirGerais_imprimirMedIndividual("PRINT", lcTotalFolhas, lcSemPerguntas) && imprime receitas
			Endscan

			Select ucrsLinPresc
			Go Top
			Scan For Left(Alltrim(ucrsLinPresc.Design),1) == "." And !Empty(ucrsLinPresc.receitamcdt) And (ucrsLinPresc.estado == "1")
				uf_prescricao_imprimirReceitaMcdt(.T.) &&Refresca informa��o da receita e prepara cursores
				uf_imprimirGerais_imprimirMCDTIndividual("PRINT", lcNumeroViasMCDT, lcSemPerguntas)
			Endscan
		Endif
	Endif

Endfunc


**
Function uf_imprimirGerais_imprimirMCDTIndividual
	Lparameters lctipo, lcTotalFolhas, lcSemPerguntas
	Public myRecViaDesc, myRecImprimeTodas, myIdadeUt, myDataPresc
	Local lcimpressao,lcDefaultPrinter,lcSql, lcprinter, lcreport

	** Guardar report a imprimir - ficheiro **
	lcreport = Alltrim(myPath)+"\analises\receitamcdt.frx"


	Do Case
		Case Upper(Alltrim(lctipo)) == "PREV"

			Report Form Alltrim(lcreport) To Printer Prompt Nodialog Preview

		Case Upper(Alltrim(lctipo)) == "PRINT"

			If IMPRIMIRGERAIS.pageframe1.page1.chkParaPDF.Value=1

				Local lcNomeFicheiroPDF, lcNo, lcEstab, lcPodeEnviarEmail
				Store .T. To lcPodeEnviarEmail
				Store '' To lcNomeFicheiroPDF
				Store 0 To lcNo, lcEstab

				lcNomeFicheiroPDF= "ReceitaMCDT Prescri��o: " + lcnrPrescricao + ".PDF"
				lcNo = ucrsCabPresc.no
				lcEstab = ucrsCabPresc.estab

				lcSql = ""
				TEXT TO lcSQL TEXTMERGE NOSHOW
					SELECT autoriza_emails, ncont FROM B_UTENTES WHERE no=<<ucrsCabPresc.NO>> and estab=<<ucrsCabPresc.estab>>
				ENDTEXT
				If !uf_gerais_actGrelha("","ucrsAutorizaEmails",lcSql)
					uf_perguntalt_chama("Ocorreu uma anomalia a obter a informa��o para impress�o. Por favor contacte o suporte.","OK","",16)
					Return .F.
				Endif
				If ucrsAutorizaEmails.autoriza_emails = .F. And (Left(Alltrim(ucrsAutorizaEmails.ncont),1) <> '5' And Left(Alltrim(ucrsAutorizaEmails.ncont),1) <> '9' And Left(Alltrim(ucrsAutorizaEmails.ncont),1) <> '6')
					lcPodeEnviarEmail = .F.
				Endif

				If IMPRIMIRGERAIS.pageframe1.page1.chkPorEmail.Value=0
					If Alltrim(myVersaoDoc) = 'ORIGINAL'
						myVersaoDoc = uf_gerais_versaodocumento(1)
					Endif
					uf_guarda_pdf(Alltrim(lcreport), Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ',''), .F., lcTabCab)
				Else

					If lcPodeEnviarEmail = .T.
						If ucrse1.usacoms=.T.
							If !Empty(IMPRIMIRGERAIS.pageframe1.page1.emaildestino.Value)
								Local mDir, lcTo, lcToEmail, lcSubject, lcBody, lcAtatchment
								mDir = Alltrim(uf_gerais_getParameter('ADM0000000015','TEXT')) + "\Cache\PDF"
								lcTo = ""
								lcToEmail = Alltrim(IMPRIMIRGERAIS.pageframe1.page1.emaildestino.Value)
								lcSubject = Alltrim(IMPRIMIRGERAIS.pageframe1.page1.emailsubject.Value)
								lcBody = Alltrim(IMPRIMIRGERAIS.pageframe1.page1.emailbody.Value)
								lcAtatchment = mDir + "\"+Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ','')
								If Alltrim(myVersaoDoc) = 'ORIGINAL'
									myVersaoDoc = uf_gerais_versaodocumento(1)
								Endif

								uf_guarda_pdf(Alltrim(lcreport), Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ',''), .T., lcTabCab)
								If(!uf_gerais_validaEmailRgpd(lcToEmail,lcNo,lcEstab, lctabela))
									Return .F.
								Endif
								regua(0,6,"A enviar email ...")
								uf_startup_sendmail_emp(lcTo, lcToEmail, lcSubject, lcBody , lcAtatchment, .T.)
								regua(2)
								If lcNo=0
									TEXT TO lcSQL TEXTMERGE NOSHOW
										SELECT utstamp FROM b_utentes where no=<<lcNo>> and estab=<<lcEstab>>
									ENDTEXT
									If !uf_gerais_actGrelha("", "ucrsUtStamp", lcSql)
										uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!","OK","",16)
										Return .F.
									Endif
									uf_user_log_ins('CL', Alltrim(ucrsUtStamp.utstamp), 'EMAIL', 'Envio de documento por email - ' + Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ',''))
								Endif
								shellexecute(0,"open","del "+Alltrim(lcAtatchment)+" /q ","","",1)
							Else
								uf_perguntalt_chama("N�o existe um email para envio preenchido." + Chr(13) + "Insira um ou atualize a ficha respetiva.","OK","",64)
								Return .F.
							Endif
						Else
							uf_perguntalt_chama("PARA UTILIZAR ESTA FUNCIONALIDADE DEVER� ADQUIRIR O M�DULO DE COMUNICA��ES." + Chr(13) + "PARA MAIS INFORMA��ES POR FAVOR CONTACTE A LOGITOOLS (OP��O - COMERCIAL).", "OK","",16)
						Endif
					Else
						uf_perguntalt_chama("LAMENTAMOS MAS N�O VAI SER POSS�VEL EFETUAR O ENVIO." + Chr(13) + "O UTENTE N�O AUTORIZA O ENVIO DE EMAILS.", "OK","",16)
					Endif

				Endif
			Else

				** Guarda a impressora por defeito do Windows***
				lcDefaultPrinter=Set("printer",2)
				*****************************************

				If Empty(lcSemPerguntas)
					lcprinter = Alltrim(IMPRIMIRGERAIS.pageframe1.page1.destino.Value)
				Else
					lcprinter = "Impressora por defeito do Windows"
				Endif

				** preparar impressora **
				If !(Alltrim(lcprinter) == "Impressora por defeito do Windows")
					Set Printer To Name ''+Alltrim(lcprinter)+''
				Endif

				Set Device To Print
				Set Console Off
				**

				Select ucrsLinPrescPrint

				If myFolha == lcTotalFolhas
					**
					Report Form Alltrim(lcreport) To Printer Noreset
					myFolha = myFolha + 1

				Else
					Report Form Alltrim(lcreport) To Printer Noreset Nopageeject
					myFolha = myFolha + 1

				Endif

				** por impressora no default **
				Set Device To Screen
				Set Printer To Name ''+lcDefaultPrinter+''
				Set Console On
				**
			Endif
		Otherwise
	Endcase
Endfunc


** funcao para imprimir receitas
Function uf_imprimirGerais_imprimirMedIndividual
	Lparameters lctipo, lcTotalFolhas, lcSemPerguntas

	Public myRecViaDesc, myRecImprimeTodas, myPrescno, myPin, myPinOpcao, myPrescno2, myPin2, myPinOpcao2, myPrescno3, myPin3, myPinOpcao3, myPrintPrevisao
	Local lcimpressao,lcDefaultPrinter,lcSql, lcprinter, lcreport
	Store '' To myPrescno, myPin, myPinOpcao
	Store .F. To myPrintPrevisao

	&& altera o layout da receita mediante o tipo de receita a imprmir
	If Upper(Left(Alltrim(ucrsLinPrescPrint.tiporeceita),1)) != 'L'
		lcreport = Alltrim(myPath)+"\analises\receita_rcp.frx"
	Else
		lcreport = Alltrim(myPath)+"\analises\receita_rsp.frx"
	Endif

	Do Case
		Case Upper(Alltrim(lctipo)) == "PREV"

			&& receitas materiliazidas
			If Upper(Left(Alltrim(ucrsLinPrescPrint.tiporeceita),1)) != 'L'
				Select ucrsLinPrescPrint
				Go Top
			Else
				Select ucrsLinPrescPrint
				Go Top
				Select ucrsLinPrescPrintLinhas
				Go Top
			Endif

			myRecViaDesc = ""
			myPrescno = Alltrim(ucrsLinPrescPrint.prescno)
			myPrintPrevisao = .T.

			&& prever a receita em full screen automaticamente

			Local lcPreview, lcListener

			Do (_ReportPreview) With lcPreview

			With lcPreview
				.Caption = 'Pre visualiza��o Receita'
				.Toolbarisvisible = .T.
				.Topform = .T.
				.ZoomLevel = 6
			Endwith

			lcListener = Newobject('reportlistener')
			lcListener.ListenerType = 1
			lcListener.PreviewContainer = lcPreview

			Report Form Alltrim(lcreport) Object lcListener
			lcPreview.oForm.WindowState = 2	&& Maximize Preview Window
			**REPORT FORM ALLTRIM(lcreport) TO PRINTER PROMPT NODIALOG PREVIEW

		Case Upper(Alltrim(lctipo)) == "PRINT"

			** Guarda a impressora por defeito do Windows***
			lcDefaultPrinter=Set("printer",2)
			**

			If Empty(lcSemPerguntas)
				lcprinter = Alltrim(IMPRIMIRGERAIS.pageframe1.page1.destino.Value)
			Else
				lcprinter = "Impressora por defeito do Windows"
			Endif

			** preparar impressora **
			If !(Alltrim(lcprinter) == "Impressora por defeito do Windows")
				Set Printer To Name ''+Alltrim(lcprinter)+''
			Endif

			Set Device To Print
			Set Console Off
			**

			** Trata Renovavel apenas para RCP
			If Upper(Left(Alltrim(ucrsLinPrescPrint.tiporeceita),1)) != 'L'


				** verifica se � para imprimir ou enviar por email
				If IMPRIMIRGERAIS.pageframe1.page1.chkParaPDF.Value=1 && Enviar por email

					Local lcNomeFicheiroPDF, lcNo, lcEstab, lcPodeEnviarEmail
					Store .T. To lcPodeEnviarEmail
					Store '' To lcNomeFicheiroPDF
					Store 0 To lcNo, lcEstab

					lcNomeFicheiroPDF= "ReceitaRCP"+ Strtran(Alltrim(Prescricao.lblPrescricao.Caption),':','') + ".PDF"
					lcNo = ucrsCabPresc.no
					lcEstab = ucrsCabPresc.estab

					lcSql = ""
					TEXT TO lcSQL TEXTMERGE NOSHOW
						SELECT autoriza_emails, ncont FROM B_UTENTES WHERE no=<<ucrsCabPresc.NO>> and estab=<<ucrsCabPresc.estab>>
					ENDTEXT
					If !uf_gerais_actGrelha("","ucrsAutorizaEmails",lcSql)
						uf_perguntalt_chama("Ocorreu uma anomalia a obter a informa��o para impress�o. Por favor contacte o suporte.","OK","",16)
						Return .F.
					Endif
					If ucrsAutorizaEmails.autoriza_emails = .F. And (Left(Alltrim(ucrsAutorizaEmails.ncont),1) <> '5' And Left(Alltrim(ucrsAutorizaEmails.ncont),1) <> '9' And Left(Alltrim(ucrsAutorizaEmails.ncont),1) <> '6')
						lcPodeEnviarEmail = .F.
					Endif

					If IMPRIMIRGERAIS.pageframe1.page1.chkPorEmail.Value=0
						If Alltrim(myVersaoDoc) = 'ORIGINAL'
							myVersaoDoc = uf_gerais_versaodocumento(1)
						Endif
						uf_guarda_pdf(Alltrim(lcreport), Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ',''), .F., lcTabCab)
					Else
						If lcPodeEnviarEmail = .T.
							If ucrse1.usacoms=.T.
								If !Empty(IMPRIMIRGERAIS.pageframe1.page1.emaildestino.Value)
									Local mDir, lcTo, lcToEmail, lcSubject, lcBody, lcAtatchment
									mDir = Alltrim(uf_gerais_getParameter('ADM0000000015','TEXT')) + "\Cache\PDF"
									lcTo = ""
									lcToEmail = Alltrim(IMPRIMIRGERAIS.pageframe1.page1.emaildestino.Value)
									lcSubject = Alltrim(IMPRIMIRGERAIS.pageframe1.page1.emailsubject.Value)
									lcBody = Alltrim(IMPRIMIRGERAIS.pageframe1.page1.emailbody.Value)
									lcAtatchment = mDir + "\"+Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ','')
									If Alltrim(myVersaoDoc) = 'ORIGINAL'
										myVersaoDoc = uf_gerais_versaodocumento(1)
									Endif
									uf_guarda_pdf(Alltrim(lcreport), Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ',''), .T., lcTabCab)
									If(!uf_gerais_validaEmailRgpd(lcToEmail,lcNo,lcEstab, lctabela))
										Return .F.
									Endif
									regua(0,6,"A enviar email ...")
									uf_startup_sendmail_emp(lcTo, lcToEmail, lcSubject, lcBody , lcAtatchment, .T.)
									regua(2)
									If lcNo=0
										TEXT TO lcSQL TEXTMERGE NOSHOW
											SELECT utstamp FROM b_utentes where no=<<lcNo>> and estab=<<lcEstab>>
										ENDTEXT
										If !uf_gerais_actGrelha("", "ucrsUtStamp", lcSql)
											uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!","OK","",16)
											Return .F.
										Endif
										uf_user_log_ins('CL', Alltrim(ucrsUtStamp.utstamp), 'EMAIL', 'Envio de documento por email - ' + Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ',''))
									Endif
									shellexecute(0,"open","del "+Alltrim(lcAtatchment)+" /q ","","",1)
								Else
									uf_perguntalt_chama("N�o existe um email para envio preenchido." + Chr(13) + "Insira um ou atualize a ficha respetiva.","OK","",64)
									Return .F.
								Endif
							Else
								uf_perguntalt_chama("PARA UTILIZAR ESTA FUNCIONALIDADE DEVER� ADQUIRIR O M�DULO DE COMUNICA��ES." + Chr(13) + "PARA MAIS INFORMA��ES POR FAVOR CONTACTE A LOGITOOLS (OP��O - COMERCIAL).", "OK","",16)
							Endif
						Else
							uf_perguntalt_chama("LAMENTAMOS MAS N�O VAI SER POSS�VEL EFETUAR O ENVIO." + Chr(13) + "O UTENTE N�O AUTORIZA O ENVIO DE EMAILS.", "OK","",16)
						Endif

					Endif


				Else && Imprimir
					Select ucrsLinPrescPrint

					If ucrsLinPrescPrint.renovavel == .T.

						If ucrsLinPrescPrint.vias == 1

							If myFolha == lcTotalFolhas
								myRecViaDesc = "1.� VIA"
								Select ucrsLinPrescPrint

								Report Form Alltrim(lcreport) To Printer
								myFolha = myFolha + 1

							Else
								myRecViaDesc = "1.� VIA"
								Select ucrsLinPrescPrint

								Report Form Alltrim(lcreport) To Printer Nopageeject
								myFolha = myFolha + 1

							Endif
						Endif

						If ucrsLinPrescPrint.vias == 2
							myRecViaDesc = "1.� VIA"
							Select ucrsLinPrescPrint

							Report Form Alltrim(lcreport) To Printer Nopageeject
							myFolha = myFolha + 1

							If myFolha == lcTotalFolhas
								Select ucrsLinPrescPrint
								myRecViaDesc = "2.� VIA"
								Report Form Alltrim(lcreport) To Printer Noreset
								myFolha = myFolha + 1

							Else
								Select ucrsLinPrescPrint
								myRecViaDesc = "2.� VIA"

								Report Form Alltrim(lcreport) To Printer Noreset Nopageeject
								myFolha = myFolha + 1

							Endif
						Endif

						If ucrsLinPrescPrint.vias == 3
							myRecViaDesc = "1.� VIA"
							Select ucrsLinPrescPrint

							Report Form Alltrim(lcreport) To Printer Nopageeject
							myFolha = myFolha + 1

							Select ucrsLinPrescPrint
							myRecViaDesc = "2.� VIA"

							Report Form Alltrim(lcreport) To Printer Noreset Nopageeject
							myFolha = myFolha + 1

							If myFolha == lcTotalFolhas
								Select ucrsLinPrescPrint
								myRecViaDesc = "3.� VIA"

								Report Form Alltrim(lcreport) To Printer Noreset
								myFolha = myFolha + 1

							Else
								Select ucrsLinPrescPrint
								myRecViaDesc = "3.� VIA"

								Report Form Alltrim(lcreport) To Printer Nopageeject
								myFolha = myFolha + 1

							Endif
						Endif

					Else
						Select ucrsLinPrescPrint

						myRecViaDesc = ""
						myPrintPrevisao = .F.

						If myFolha == lcTotalFolhas
							**

							Report Form Alltrim(lcreport) To Printer Noreset
							myFolha = myFolha + 1

						Else

							Report Form Alltrim(lcreport) To Printer Noreset Nopageeject
							myFolha = myFolha + 1

						Endif

					Endif
				Endif
			Else && controla impress�o de RSP

				If Type("IMPRIMIRGERAIS") != "U"
					** verifica se � para imprimir ou enviar por email
					If IMPRIMIRGERAIS.pageframe1.page1.chkParaPDF.Value=1 && Enviar por email
						Local lcNomeFicheiroPDF, lcNo, lcEstab, lcPodeEnviarEmail
						Store .T. To lcPodeEnviarEmail
						Store '' To lcNomeFicheiroPDF
						Store 0 To lcNo, lcEstab

						lcNomeFicheiroPDF= "ReceitaRSP"+ Strtran(Alltrim(Prescricao.lblPrescricao.Caption),':','') + ".PDF"
						lcNo = ucrsCabPresc.no
						lcEstab = ucrsCabPresc.estab

						lcSql = ""
						TEXT TO lcSQL TEXTMERGE NOSHOW
							SELECT autoriza_emails, ncont FROM B_UTENTES WHERE no=<<ucrsCabPresc.NO>> and estab=<<ucrsCabPresc.estab>>
						ENDTEXT
						If !uf_gerais_actGrelha("","ucrsAutorizaEmails",lcSql)
							uf_perguntalt_chama("Ocorreu uma anomalia a obter a informa��o para impress�o. Por favor contacte o suporte.","OK","",16)
							Return .F.
						Endif
						If ucrsAutorizaEmails.autoriza_emails = .F. And (Left(Alltrim(ucrsAutorizaEmails.ncont),1) <> '5' And Left(Alltrim(ucrsAutorizaEmails.ncont),1) <> '9' And Left(Alltrim(ucrsAutorizaEmails.ncont),1) <> '6')
							lcPodeEnviarEmail = .F.
						Endif

						If Upper(Left(Alltrim(ucrsLinPrescPrint.tiporeceita),1)) != 'L'
							Select ucrsLinPrescPrint
							Go Top
						Else
							Select ucrsLinPrescPrint
							Go Top
							Select ucrsLinPrescPrintLinhas
							Go Top
						Endif

						myRecViaDesc = ""
						myPrescno = Alltrim(ucrsLinPrescPrint.prescno)

						If IMPRIMIRGERAIS.pageframe1.page1.chkPorEmail.Value=0
							**myVersaoDoc = uf_gerais_versaodocumento(1)
							uf_guarda_pdf(Alltrim(lcreport), Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ',''), .F., lcTabCab)
						Else
							If lcPodeEnviarEmail = .T.
								If ucrse1.usacoms=.T.
									If !Empty(IMPRIMIRGERAIS.pageframe1.page1.emaildestino.Value)
										Local mDir, lcTo, lcToEmail, lcSubject, lcBody, lcAtatchment
										mDir = Alltrim(uf_gerais_getParameter('ADM0000000015','TEXT')) + "\Cache\PDF"
										lcTo = ""
										lcToEmail = Alltrim(IMPRIMIRGERAIS.pageframe1.page1.emaildestino.Value)
										lcSubject = Alltrim(IMPRIMIRGERAIS.pageframe1.page1.emailsubject.Value)
										lcBody = Alltrim(IMPRIMIRGERAIS.pageframe1.page1.emailbody.Value)
										lcAtatchment = mDir + "\"+Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ','')
										If Alltrim(myVersaoDoc) = 'ORIGINAL'
											myVersaoDoc = uf_gerais_versaodocumento(1)
										Endif

										uf_guarda_pdf(Alltrim(lcreport), Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ',''), .T., lcTabCab)
										If(!uf_gerais_validaEmailRgpd(lcToEmail,lcNo,lcEstab, lctabela))
											Return .F.
										Endif
										regua(0,6,"A enviar email ...")
										uf_startup_sendmail_emp(lcTo, lcToEmail, lcSubject, lcBody , lcAtatchment, .T.)
										regua(2)
										If lcNo=0
											TEXT TO lcSQL TEXTMERGE NOSHOW
												SELECT utstamp FROM b_utentes where no=<<lcNo>> and estab=<<lcEstab>>
											ENDTEXT
											If !uf_gerais_actGrelha("", "ucrsUtStamp", lcSql)
												uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!","OK","",16)
												Return .F.
											Endif
											uf_user_log_ins('CL', Alltrim(ucrsUtStamp.utstamp), 'EMAIL', 'Envio de documento por email - ' + Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ',''))
										Endif
										shellexecute(0,"open","del "+Alltrim(lcAtatchment)+" /q ","","",1)
									Else
										uf_perguntalt_chama("N�o existe um email para envio preenchido." + Chr(13) + "Insira um ou atualize a ficha respetiva.","OK","",64)
										Return .F.
									Endif
								Else
									uf_perguntalt_chama("PARA UTILIZAR ESTA FUNCIONALIDADE DEVER� ADQUIRIR O M�DULO DE COMUNICA��ES." + Chr(13) + "PARA MAIS INFORMA��ES POR FAVOR CONTACTE A LOGITOOLS (OP��O - COMERCIAL).", "OK","",16)
								Endif
							Else
								uf_perguntalt_chama("LAMENTAMOS MAS N�O VAI SER POSS�VEL EFETUAR O ENVIO." + Chr(13) + "O UTENTE N�O AUTORIZA O ENVIO DE EMAILS.", "OK","",16)
							Endif

						Endif
					Else && Imprimir

						Select ucrsLinPrescPrint
						Go Top
						Select ucrsLinPrescPrintLinhas
						Go Top

						myRecViaDesc = ""
						myPrintPrevisao = .F.

						&& hard code para print de dados para certifica��o software - Lu�s Leal
						If Reccount("ucrsLinPrescPrintLinhas") > 24
							lcreport = Alltrim(myPath)+"\analises\receita_rsp_qrcode1.frx"
							lcreport1 = Alltrim(myPath)+"\analises\receita_rsp_qrcode.frx"

							Report Form Alltrim(lcreport) To Printer Noreset Nopageeject
							Report Form Alltrim(lcreport1) To Printer Noreset
						Else
							**REPORT FORM ALLTRIM(lcreport) TO PRINTER NORESET
							** editado aqui para n�o ignorar o reset da numeracao de paginas
							Report Form Alltrim(lcreport) To Printer
						Endif
					Endif
				Else
					If IMPRIMIRGERAIS.pageframe1.page1.chkParaPDF.Value=1 && Enviar por email
						Local lcNomeFicheiroPDF, lcNo, lcEstab, lcPodeEnviarEmail
						Store .T. To lcPodeEnviarEmail
						Store '' To lcNomeFicheiroPDF
						Store 0 To lcNo, lcEstab

						lcNomeFicheiroPDF= "ReceitaRSP"+ Strtran(Alltrim(Prescricao.lblPrescricao.Caption),':','') + ".PDF"
						lcNo = ucrsCabPresc.no
						lcEstab = ucrsCabPresc.estab

						lcSql = ""
						TEXT TO lcSQL TEXTMERGE NOSHOW
							SELECT autoriza_emails,ncont FROM B_UTENTES WHERE no=<<ucrsCabPresc.NO>> and estab=<<ucrsCabPresc.estab>>
						ENDTEXT
						If !uf_gerais_actGrelha("","ucrsAutorizaEmails",lcSql)
							uf_perguntalt_chama("Ocorreu uma anomalia a obter a informa��o para impress�o. Por favor contacte o suporte.","OK","",16)
							Return .F.
						Endif
						If ucrsAutorizaEmails.autoriza_emails = .F. And (Left(Alltrim(ucrsAutorizaEmails.ncont),1) <> '5' And Left(Alltrim(ucrsAutorizaEmails.ncont),1) <> '9' And Left(Alltrim(ucrsAutorizaEmails.ncont),1) <> '6')
							lcPodeEnviarEmail = .F.
						Endif

						Select ucrsLinPrescPrint
						Go Top
						Select ucrsLinPrescPrintLinhas
						Go Top

						myRecViaDesc = ""
						myPrintPrevisao = .F.

						If IMPRIMIRGERAIS.pageframe1.page1.chkPorEmail.Value=0

							If Reccount("ucrsLinPrescPrintLinhas") > 24
								lcreport = Alltrim(myPath)+"\analises\receita_rsp_qrcode1.frx"
								lcreport1 = Alltrim(myPath)+"\analises\receita_rsp_qrcode.frx"
								myVersaoDoc = uf_gerais_versaodocumento(1)
								uf_guarda_pdf(Alltrim(lcreport), Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ',''), .F., lcTabCab)
								uf_guarda_pdf(Alltrim(lcreport1), Strtran(Strtran(Alltrim('0'+lcNomeFicheiroPDF),'/',''),' ',''), .F., lcTabCab)
							Else
								myVersaoDoc = uf_gerais_versaodocumento(1)
								uf_guarda_pdf(Alltrim(lcreport), Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ',''), .F., lcTabCab)
							Endif
						Else
							If lcPodeEnviarEmail = .T.
								If ucrse1.usacoms=.T.
									If !Empty(IMPRIMIRGERAIS.pageframe1.page1.emaildestino.Value)
										Local mDir, lcTo, lcToEmail, lcSubject, lcBody, lcAtatchment
										mDir = Alltrim(uf_gerais_getParameter('ADM0000000015','TEXT')) + "\Cache\PDF"
										lcTo = ""
										lcToEmail = Alltrim(IMPRIMIRGERAIS.pageframe1.page1.emaildestino.Value)
										lcSubject = Alltrim(IMPRIMIRGERAIS.pageframe1.page1.emailsubject.Value)
										lcBody = Alltrim(IMPRIMIRGERAIS.pageframe1.page1.emailbody.Value)
										lcAtatchment = mDir + "\"+Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ','')
										myVersaoDoc = uf_gerais_versaodocumento(1)
										uf_guarda_pdf(Alltrim(lcreport), Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ',''), .T., lcTabCab)
										If(!uf_gerais_validaEmailRgpd(lcToEmail,lcNo,lcEstab, lctabela))
											Return .F.
										Endif
										regua(0,6,"A enviar email ...")
										uf_startup_sendmail_emp(lcTo, lcToEmail, lcSubject, lcBody , lcAtatchment, .T.)
										regua(2)
										If lcNo=0
											TEXT TO lcSQL TEXTMERGE NOSHOW
													SELECT utstamp FROM b_utentes where no=<<lcNo>> and estab=<<lcEstab>>
											ENDTEXT
											If !uf_gerais_actGrelha("", "ucrsUtStamp", lcSql)
												uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!","OK","",16)
												Return .F.
											Endif
											uf_user_log_ins('CL', Alltrim(ucrsUtStamp.utstamp), 'EMAIL', 'Envio de documento por email - ' + Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ',''))
										Endif
										shellexecute(0,"open","del "+Alltrim(lcAtatchment)+" /q ","","",1)
									Else
										uf_perguntalt_chama("N�o existe um email para envio preenchido." + Chr(13) + "Insira um ou atualize a ficha respetiva.","OK","",64)
										Return .F.
									Endif
								Else
									uf_perguntalt_chama("PARA UTILIZAR ESTA FUNCIONALIDADE DEVER� ADQUIRIR O M�DULO DE COMUNICA��ES." + Chr(13) + "PARA MAIS INFORMA��ES POR FAVOR CONTACTE A LOGITOOLS (OP��O - COMERCIAL).", "OK","",16)
								Endif
							Else
								uf_perguntalt_chama("LAMENTAMOS MAS N�O VAI SER POSS�VEL EFETUAR O ENVIO." + Chr(13) + "O UTENTE N�O AUTORIZA O ENVIO DE EMAILS.", "OK","",16)
							Endif


						Endif


					Else
						Select ucrsLinPrescPrint
						Go Top
						Select ucrsLinPrescPrintLinhas
						Go Top

						myRecViaDesc = ""
						myPrintPrevisao = .F.

						&& hard code para print de dados para certifica��o software - Lu�s Leal
						If Reccount("ucrsLinPrescPrintLinhas") > 24
							lcreport = Alltrim(myPath)+"\analises\receita_rsp_qrcode1.frx"
							lcreport1 = Alltrim(myPath)+"\analises\receita_rsp_qrcode.frx"
							Report Form Alltrim(lcreport) To Printer Noreset Nopageeject
							Report Form Alltrim(lcreport1) To Printer Noreset
						Else
							**REPORT FORM ALLTRIM(lcreport) TO PRINTER NORESET
							** editado aqui para n�o ignorar o reset da numeracao de paginas
							Report Form Alltrim(lcreport) To Printer
						Endif
					Endif
				Endif
			Endif

			** por impressora no default **
			Set Device To Screen
			Set Printer To Name ''+lcDefaultPrinter+''
			Set Console On
			**

		Otherwise
	Endcase
Endfunc

**
Function uf_imprimirGerais_gravar
	Lparameters uv_nomePDF
	Local lcSQLImpressoes
	Store '' To lcSQLImpressoes
	If pgfocus=1 Or pgfocus=3

		Local uv_curLineFilter, uv_filter
		Store '' To uv_curLineFilter

		Select uCrsImprimirDocs

		TEXT TO lcSQLImpressoes TEXTMERGE NOSHOW
			exec up_gerais_getImpressoes '<<ALLTRIM(uCrsImprimirDocs.documento)>>', '','<<ALLTRIM(uCrsImprimirDocs.nomeimpressao)>>'
		ENDTEXT

		If !uf_gerais_actGrelha("","ucrsConfigImpressoes",lcSQLImpressoes)
			uf_perguntalt_chama("Erro a validar configura��o Impress�o.Por favor contacte o suporte.","OK","",16)
			Return .F.
		Endif

		If !Empty(ucrsConfigImpressoes.lineCursor) And !Empty(ucrsConfigImpressoes.lineFilter)

			uv_filter = Alltrim(ucrsConfigImpressoes.lineFilter)

			Select (Alltrim(ucrsConfigImpressoes.lineCursor))

			uv_curLineFilter = Filter()

			Set Filter To &uv_filter.

			Select (Alltrim(ucrsConfigImpressoes.lineCursor))
			Count To lcCount
			If lcCount = 0
				uf_perguntalt_chama("N�o tem elementos a imprimir.","OK","",16)

				Select (Alltrim(ucrsConfigImpressoes.lineCursor))
				Set Filter To &uv_curLineFilter.

				Return .F.
			Endif

		Endif

		If  uf_gerais_compstr(MYIMPRESSAOORIGEM, "Atendimento")
			If IMPRIMIRGERAIS.pageframe1.page1.chkPorEmail.Value=1
				If ucrse1.usacoms=.T.
					If Empty(IMPRIMIRGERAIS.pageframe1.page1.emaildestino.Value)
						uf_perguntalt_chama("N�o existe um email para envio preenchido." + Chr(13) + "Insira um ou atualize a ficha respetiva.","OK","",64)
						Return .F.
					Endif
				Else
					uf_perguntalt_chama("PARA UTILIZAR ESTA FUNCIONALIDADE DEVER� ADQUIRIR O M�DULO DE COMUNICA��ES." + Chr(13) + "PARA MAIS INFORMA��ES POR FAVOR CONTACTE A LOGITOOLS (OP��O - COMERCIAL).", "OK","",16)
				Endif
			Endif
			uf_imprimirgerais_imprimirAtendimentoInit()
			Select ftTempImp
			Go Top
			Scan
				lcStampFtTempImp = ftTempImp.ftstamp

				fecha ("ft")
				fecha ("ft2")
				fecha ("fi")
				lcSql = ''
				TEXT TO lcsql TEXTMERGE NOSHOW
							exec up_touch_ft '<<ALLTRIM(lcStampFtTempImp)>>'
				ENDTEXT
				uf_gerais_actGrelha("", "FT", lcSql)
				lcSql = ''
				TEXT TO lcsql TEXTMERGE NOSHOW
							SELECT respostaDt = isnull((select top 1 DescrResposta from b_dt_resposta WHERE stampDoc = '<<lcStampFtTempImp>>' order by DataEnvio),''),* FROM FT2 (nolock) WHERE ft2stamp='<<ALLTRIM(lcStampFtTempImp)>>'
				ENDTEXT
				uf_gerais_actGrelha("", "FT2", lcSql)
				lcSql = ''
				TEXT TO lcsql TEXTMERGE NOSHOW
							exec up_facturacao_linhas 0, <<mysite_nr>>, '<<lcStampFtTempImp>>'
				ENDTEXT
				uf_gerais_actGrelha("", "FI", lcSql)

				TEXT TO lcSQL TEXTMERGE NOSHOW
			        	exec up_print_qrcode_a4 '<<lcStampFtTempImp>>'
				ENDTEXT
				uf_gerais_actGrelha("", "FTQRCODE", lcSql)
				Select ftTempImp
				lcDocumento = Alltrim(ftTempImp.Nmdoc)

				uf_imprimirGerais_imprimir("print", .F., uv_nomePDF)

			Endscan
			If IMPRIMIRGERAIS.pageframe1.page1.chkPorEmail.Value=1
				If Used("uc_CursorAttachment")
					Select uc_CursorAttachment
					Go Top
					uf_gerais_sendmail_bulk("uc_CursorAttachment", .T.)
					fecha("uc_CursorAttachment")
				Endif
			Endif
			uf_imprimirgerais_imprimirAtendimentoFim()
		Else
			uf_imprimirGerais_imprimir("print", .F., uv_nomePDF)
		Endif


		If !Empty(ucrsConfigImpressoes.lineCursor) And !Empty(ucrsConfigImpressoes.lineFilter)

			Select (Alltrim(ucrsConfigImpressoes.lineCursor))

			If Empty(uv_curLineFilter)
				Set Filter To
			Else
				Set Filter To &uv_curLineFilter.
			Endif

		Endif

	Else
		Local lcSql
		Store '' To lcSql

		Select uCrsImprimirDocs
		If (uCrsImprimirDocs.temduplicados) And (uCrsImprimirDocs.numduplicados=0)
			uf_perguntalt_chama("O N�MERO DE DUPLICADOS N�O � V�LIDO!","OK","",48)
			Return .F.
		Endif

		Select uCrsImprimirDocs
		If Empty(astr(uCrsImprimirDocs.Id))
			Return .F.
		Endif

		TEXT TO lcSQL TEXTMERGE Noshow
			UPDATE b_impressoes
			SET
				idupordefeito = 0
			From
				b_impressoes
			Where
				nomeimpressao!= ''
				AND documento = '<<Alltrim(IMPRIMIRGERAIS.Documento)>>'


			UPDATE b_impressoes
			SET
				idupordefeito = <<IIF(uCrsImprimirDocs.idupordefeito,1,0)>>,
				temDuplicados = <<IIF(uCrsImprimirDocs.TemDuplicados,1,0)>>,
				NumDuplicados = <<uCrsImprimirDocs.NumDuplicados>>
			where
				documento = '<<Alltrim(IMPRIMIRGERAIS.Documento)>>'
				and  nomeimpressao  ='<<Alltrim(IMPRIMIRGERAIS.Pageframe1.Page2.impressao.DisplayValue)>>'

		ENDTEXT

		If uf_gerais_actGrelha("","",lcSql)
			uf_perguntalt_chama("REGISTO ALTERADO COM SUCESSO!","OK","",64)
		Else
			uf_perguntalt_chama("OCORREU UM ERRO A ALTERAR O REGISTO! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return .F.
		Endif
	Endif
Endfunc


**
Function uf_imprimirGerais_imprimir_Marcacoes
	Lparameters lctipo

	** Guarda a impressora por defeito do Windows***
	lcDefaultPrinter=Set("printer",2)
	*****************************************

	lcimpressao = Alltrim(IMPRIMIRGERAIS.pageframe1.page1.impressao.Value) &&Alltrim(ucrsImpIdusDefault.nomeimpressao)
	** Obtem Nome do Ficheiro Tabela de Impressoes **
	lcSql = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select Top 1 nomeficheiro, tabela, temduplicados, numduplicados
		From b_impressoes (nolock)
		Where nomeimpressao = '<<lcimpressao>>'
	ENDTEXT

	If !uf_gerais_actGrelha("","templcSQL",lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR A CONFIGURA��O DE IMPRESS�ES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return
	Endif

	If Reccount("templcSQL")>0
		lcnomeFicheiro = Alltrim(templcSQL.nomeficheiro)
		lcprinter = IMPRIMIRGERAIS.pageframe1.page1.destino.Value
		lctabela = templcSQL.tabela
		lctemduplicados = templcSQL.temduplicados
		lcnumduplicados = templcSQL.numduplicados
	Else
		Return .F.
	Endif

	If !(uf_gerais_actGrelha("","uCrsImpressao",lcSql))
		&&erro
	Endif

	****************************************
	** PREPARAR CURSORES PARA IMPRESS�ES **
	****************************************

	** Obtem dados Gerais da Emprea / Loja **
	If !Used("uCrsE1")
		uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR OS DADOS GERAIS DA EMPRESA. CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif
	********************************

	** Guardar report a imprimir - ficheiro **
	lcreport = Alltrim(myPath)+"\analises\" + Alltrim(lcnomeFicheiro)

	** preparar impressora **
	If !(Alltrim(lcprinter) == "Impressora por defeito do Windows")
		Local lcValErroPrinter
		Try
			Set Printer To Name ''+Alltrim(lcprinter)+''
		Catch

			uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
			lcValErroPrinter = .T.
		Endtry
		If lcValErroPrinter == .T.
			Return .F.
		Endif
	Endif

	Set Device To Print
	Set Console Off
	***********************************


	For I=0 To lcnumduplicados
		myVersaoDoc = uf_gerais_versaodocumento(I+1)

		** IMPRIMIR
		If Like(lctipo,"print")
			Try
				Report Form Alltrim(lcreport) To Printer
			Catch

				uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
			Endtry
		Endif
	Endfor

	** PREVIS�O
	If Like(lctipo,"prev")
		Set REPORTBEHAVIOR 90
		Try
			Report Form Alltrim(lcreport) To Printer Prompt Nodialog Preview
		Catch
			uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
		Endtry
		Set REPORTBEHAVIOR 80
	Endif

	** por impressora no default **
	Set Device To Screen
	Set Printer To Name ''+lcDefaultPrinter+''
	Set Console On
	**************************

Endfunc


**
Function uf_imprimirGerais_imprimir_CARTAO_H
	Lparameters lctipo

	** Guarda a impressora por defeito do Windows***
	lcDefaultPrinter=Set("printer",2)
	*****************************************

	lcimpressao = Alltrim(IMPRIMIRGERAIS.pageframe1.page1.impressao.Value) &&Alltrim(ucrsImpIdusDefault.nomeimpressao)
	** Obtem Nome do Ficheiro Tabela de Impressoes **
	lcSql = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select Top 1 nomeficheiro, tabela, temduplicados, numduplicados
		From b_impressoes (nolock)
		Where nomeimpressao = '<<lcimpressao>>'
	ENDTEXT

	If !uf_gerais_actGrelha("","templcSQL",lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR A CONFIGURA��O DE IMPRESS�ES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return
	Endif

	If Reccount("templcSQL")>0
		lcnomeFicheiro = Alltrim(templcSQL.nomeficheiro)
		lcprinter = IMPRIMIRGERAIS.pageframe1.page1.destino.Value
		lctabela = templcSQL.tabela
		lctemduplicados = templcSQL.temduplicados
		lcnumduplicados = templcSQL.numduplicados
	Else
		Return .F.
	Endif

	If !(uf_gerais_actGrelha("","uCrsImpressao",lcSql))
		&&erro
	Endif

	****************************************
	** PREPARAR CURSORES PARA IMPRESS�ES **
	****************************************

	** Obtem dados Gerais da Emprea / Loja **
	If !Used("uCrsE1")
		uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR OS DADOS GERAIS DA EMPRESA. CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif
	********************************

	** Guardar report a imprimir - ficheiro **
	lcreport = Alltrim(myPath)+"\analises\" + Alltrim(lcnomeFicheiro)

	** preparar impressora **
	If !(Alltrim(lcprinter) == "Impressora por defeito do Windows")
		Local lcValErroPrinter
		Try
			Set Printer To Name ''+Alltrim(lcprinter)+''
		Catch

			uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
			lcValErroPrinter = .T.
		Endtry
		If lcValErroPrinter == .T.
			Return .F.
		Endif
	Endif

	Set Device To Print
	Set Console Off
	***********************************


	For I=0 To lcnumduplicados
		myVersaoDoc = uf_gerais_versaodocumento(I+1)

		** IMPRIMIR
		If Like(lctipo,"print")
			Try
				Report Form Alltrim(lcreport) To Printer
			Catch

				uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
			Endtry
		Endif
	Endfor


	** PREVIS�O
	If Like(lctipo,"prev")
		Set REPORTBEHAVIOR 90
		Try
			Report Form Alltrim(lcreport) To Printer Prompt Nodialog Preview
		Catch

			uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
		Endtry
		Set REPORTBEHAVIOR 80
	Endif

	** por impressora no default **
	Set Device To Screen
	Set Printer To Name ''+lcDefaultPrinter+''
	Set Console On
	**************************

Endfunc


**
Function uf_imprimirGerais_imprimir_geral
	Lparameters lctipo, lcCursor
	** Guarda a impressora por defeito do Windows***
	lcDefaultPrinter=Set("printer",2)
	*****************************************
	lcimpressao = Alltrim(IMPRIMIRGERAIS.pageframe1.page1.impressao.Value) &&Alltrim(ucrsImpIdusDefault.nomeimpressao)
	** Obtem Nome do Ficheiro Tabela de Impressoes **
	lcSql = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select Top 1 nomeficheiro, tabela, temduplicados, numduplicados
		From b_impressoes (nolock)
		Where nomeimpressao = '<<lcimpressao>>'
	ENDTEXT

	If !uf_gerais_actGrelha("","templcSQL",lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR A CONFIGURA��O DE IMPRESS�ES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return
	Endif

	If Reccount("templcSQL")>0
		lcnomeFicheiro  = Alltrim(templcSQL.nomeficheiro)
		lcprinter 		= IMPRIMIRGERAIS.pageframe1.page1.destino.Value
		lctabela 		= templcSQL.tabela
		lctemduplicados = templcSQL.temduplicados
		lcnumduplicados = templcSQL.numduplicados
	Else
		Return .F.
	Endif

	If !(uf_gerais_actGrelha("","uCrsImpressao",lcSql))
		&&erro
	Endif

	** PREPARAR CURSORES PARA IMPRESS�ES
	** Obtem dados Gerais da Emprea / Loja **
	If !Used("uCrsE1")
		uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR OS DADOS GERAIS DA EMPRESA. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif
	********************************

	** Guardar report a imprimir - ficheiro **
	lcreport = Alltrim(myPath)+"\analises\" + Alltrim(lcnomeFicheiro)

	** preparar impressora **
	If !(Alltrim(lcprinter) == "Impressora por defeito do Windows")
		Local lcValErroPrinter
		Try
			Set Printer To Name ''+Alltrim(lcprinter)+''
		Catch
			uf_perguntalt_chama("Foi detectada uma anomalia com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
			lcValErroPrinter = .T.
		Endtry
		If lcValErroPrinter == .T.
			Return .F.
		Endif
	Endif

	If !Empty(lcCursor)
		Select &lcCursor
		Go Top
	Endif

	Set Device To Print
	Set Console Off
	***********************************

	** IMPRIMIR
	If Like(lctipo,"print")
		** Trata Duplicados
		If lctemduplicados == .T. And lcnumduplicados > 0
			Try
				For I=0 To lcnumduplicados
					Do Case
						Case I == 0
							myVersaoDoc = uf_gerais_versaodocumento(1)
							Report Form Alltrim(lcreport) To Printer Nopageeject
						Case I < lcnumduplicados
							myVersaoDoc = uf_gerais_versaodocumento(I+1)
							Report Form Alltrim(lcreport) To Printer Noreset Nopageeject
						Case I == lcnumduplicados
							myVersaoDoc = uf_gerais_versaodocumento(lcnumduplicados+1)
							Report Form Alltrim(lcreport) To Printer Noreset
					Endcase
				Endfor
			Catch

				uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
			Endtry
		Else
			Try
				Report Form Alltrim(lcreport) To Printer
			Catch

				uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
			Endtry
		Endif
	Endif


	If IMPRIMIRGERAIS.pageframe1.page1.chkParaPDF.Value=1
		If IMPRIMIRGERAIS.pageframe1.page1.chkPorEmail.Value=0
			Local lcExpFolder, lcNomePDF, lcTabCab
			Store '' To lcExpFolder
			lcNomePDF = 'RegistoClinico_' + Alltrim(Str(Cl.no))+'.pdf'
			lcreport = 'registoClinico.frx'
			lcTabCab = 'RC'
			If Alltrim(myVersaoDoc) = 'ORIGINAL'
				myVersaoDoc = uf_gerais_versaodocumento(1)
			Endif

			Local lcGuardaCache
			lcGuardaCache = .T.


			lcNomeFicheiroPDF = Alltrim(Justfname(lcNomePDF))

			If !Empty(Justpath(lcNomePDF))
				lcExpFolder = Justpath(lcNomePDF) + "\"
				lcGuardaCache = .T.
			Endif
			uf_guarda_pdf(Alltrim(myPath)+'\analises\'+Alltrim(lcreport), lcNomePDF, lcGuardaCache , lcTabCab, Alltrim(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + Alltrim(myTerm) + "'")))
		Endif
	Endif

	** PREVIS�O
	If Like(lctipo,"prev")

		Set REPORTBEHAVIOR 90
		Try
			Report Form Alltrim(lcreport) To Printer Prompt Nodialog Preview
		Catch

			uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
		Endtry
		Set REPORTBEHAVIOR 80
	Endif

	** por impressora no default **
	Set Device To Screen
	Set Printer To Name ''+lcDefaultPrinter+''
	Set Console On

Endfunc


**
Function uf_imprimirgerais_dataMatrixCriaImagem

	Lparameters lcNdoc, lnU_tipodoc, lcVerso
	Public myFilePathDataMatrix

	Local lcU_tipodoc

	If Empty(lnU_tipodoc)
		lcU_tipodoc = 0
	Else
		lcU_tipodoc = lnU_tipodoc
	Endif

	&&LOCAL lcCodigoANF
	Local lcString, lcFilePath, lcFtstamp
	Store '' To lcString, lcFilePath, lcFtstamp
	Local lcFtSNS
	Store .F. To lcFtSNS
	&& vari�veis para totais fatura
	Local lcTotalLotes, lcTotalReceitas, totalPVP, totalUtente, totalEntidade, lcTotalEmbalagens, lcValorIvaLinha, lctotalIncentivo
	Store 0 To lcTotalLotes, lcTotalReceitas, lcTotalPVP, lcTotalUtente, lcTotalEntidade, lcTotalEmbalagens, lcValorIvaLinha, lctotalIncentivo
	&& vari�veis para totais por tipo de lote
	Local lcTotalLotes10, lcTotalReceitas10, lcTotalPVP10, lctotalEntidade10, lcTotalEmbalagens10, lcTotalPVP10, lctotalUtente10, lctotalIncentivo10
	Local lcTotalLotes11, lcTotalReceitas11, lcTotalPVP11, lctotalEntidade11, lcTotalEmbalagens11, lcTotalPVP11, lctotalUtente11, lctotalIncentivo11
	Local lcTotalLotes12, lcTotalReceitas12, lcTotalPVP12, lctotalEntidade12, lcTotalEmbalagens12, lcTotalPVP12, lctotalUtente12, lctotalIncentivo12
	Local lcTotalLotes13, lcTotalReceitas13, lcTotalPVP13, lctotalEntidade13, lcTotalEmbalagens13, lcTotalPVP13, lctotalUtente13, lctotalIncentivo13
	Local lcTotalLotes15, lcTotalReceitas15, lcTotalPVP15, lctotalEntidade15, lcTotalEmbalagens15, lcTotalPVP15, lctotalUtente15, lctotalIncentivo15
	Local lcTotalLotes16, lcTotalReceitas16, lcTotalPVP16, lctotalEntidade16, lcTotalEmbalagens16, lcTotalPVP16, lctotalUtente16, lctotalIncentivo16
	Local lcTotalLotes17, lcTotalReceitas17, lcTotalPVP17, lctotalEntidade17, lcTotalEmbalagens17, lcTotalPVP17, lctotalUtente17, lctotalIncentivo17
	Local lcTotalLotes18, lcTotalReceitas18, lcTotalPVP18, lctotalEntidade18, lcTotalEmbalagens18, lcTotalPVP18, lctotalUtente18, lctotalIncentivo18
	Local lcTotalLotes19, lcTotalReceitas19, lcTotalPVP19, lctotalEntidade19, lcTotalEmbalagens19, lcTotalPVP19, lctotalUtente19, lctotalIncentivo19
	Local lcTotalLotes23, lcTotalReceitas23, lcTotalPVP23, lctotalEntidade23, lcTotalEmbalagens23, lcTotalPVP23, lctotalUtente23, lctotalIncentivo23
	Local lcTotalLotes30, lcTotalReceitas30, lcTotalPVP30, lctotalEntidade30, lcTotalEmbalagens30, lcTotalPVP30, lctotalUtente30, lctotalIncentivo30
	Local lcTotalLotes95, lcTotalReceitas95, lcTotalPVP95, lctotalEntidade95, lcTotalEmbalagens95, lcTotalPVP95, lctotalUtente95, lctotalIncentivo95
	Local lcTotalLotes96, lcTotalReceitas96, lcTotalPVP96, lctotalEntidade96, lcTotalEmbalagens96, lcTotalPVP96, lctotalUtente96, lctotalIncentivo96
	Local lcTotalLotes97, lcTotalReceitas97, lcTotalPVP97, lctotalEntidade97, lcTotalEmbalagens97, lcTotalPVP97, lctotalUtente97, lctotalIncentivo97
	Local lcTotalLotes98, lcTotalReceitas98, lcTotalPVP98, lctotalEntidade98, lcTotalEmbalagens98, lcTotalPVP98, lctotalUtente98, lctotalIncentivo98
	Local lcTotalLotes99, lcTotalReceitas99, lcTotalPVP99, lctotalEntidade99, lcTotalEmbalagens99, lcTotalPVP99, lctotalUtente99, lctotalIncentivo99
	Store 0 To lcTotalLotes23, lcTotalReceitas23, lcTotalPVP23, lctotalEntidade23, lcTotalEmbalagens23, lcTotalPVP23, lctotalUtente23, lctotalIncentivo23
	Store 0 To lcTotalLotes10, lcTotalReceitas10, lcTotalPVP10, lctotalEntidade10, lcTotalEmbalagens10, lcTotalPVP10, lctotalUtente10, lctotalIncentivo10
	Store 0 To lcTotalLotes11, lcTotalReceitas11, lcTotalPVP11, lctotalEntidade11, lcTotalEmbalagens11, lcTotalPVP11, lctotalUtente11, lctotalIncentivo11
	Store 0 To lcTotalLotes12, lcTotalReceitas12, lcTotalPVP12, lctotalEntidade12, lcTotalEmbalagens12, lcTotalPVP12, lctotalUtente12, lctotalIncentivo12
	Store 0 To lcTotalLotes18, lcTotalReceitas18, lcTotalPVP18, lctotalEntidade18, lcTotalEmbalagens18, lcTotalPVP18, lctotalUtente18, lctotalIncentivo18
	Store 0 To lcTotalLotes19, lcTotalReceitas19, lcTotalPVP19, lctotalEntidade19, lcTotalEmbalagens19, lcTotalPVP19, lctotalUtente19, lctotalIncentivo19
	Store 0 To lcTotalLotes17, lcTotalReceitas17, lcTotalPVP17, lctotalEntidade17, lcTotalEmbalagens17, lcTotalPVP17, lctotalUtente17, lctotalIncentivo17
	Store 0 To lcTotalLotes15, lcTotalReceitas15, lcTotalPVP15, lctotalEntidade15, lcTotalEmbalagens15, lcTotalPVP15, lctotalUtente15, lctotalIncentivo15
	Store 0 To lcTotalLotes16, lcTotalReceitas16, lcTotalPVP16, lctotalEntidade16, lcTotalEmbalagens16, lcTotalPVP16, lctotalUtente16, lctotalIncentivo16
	Store 0 To lcTotalLotes13, lcTotalReceitas13, lcTotalPVP13, lctotalEntidade13, lcTotalEmbalagens13, lcTotalPVP13, lctotalUtente13, lctotalIncentivo13
	Store 0 To lcTotalLotes30, lcTotalReceitas30, lcTotalPVP30, lctotalEntidade30, lcTotalEmbalagens30, lcTotalPVP30, lctotalUtente30, lctotalIncentivo30
	Store 0 To lcTotalLotes95, lcTotalReceitas95, lcTotalPVP95, lctotalEntidade95, lcTotalEmbalagens95, lcTotalPVP95, lctotalUtente95, lctotalIncentivo95
	Store 0 To lcTotalLotes96, lcTotalReceitas96, lcTotalPVP96, lctotalEntidade96, lcTotalEmbalagens96, lcTotalPVP96, lctotalUtente96, lctotalIncentivo96
	Store 0 To lcTotalLotes97, lcTotalReceitas97, lcTotalPVP97, lctotalEntidade97, lcTotalEmbalagens97, lcTotalPVP97, lctotalUtente97, lctotalIncentivo97
	Store 0 To lcTotalLotes98, lcTotalReceitas98, lcTotalPVP98, lctotalEntidade98, lcTotalEmbalagens98, lcTotalPVP98, lctotalUtente98, lctotalIncentivo98
	Store 0 To lcTotalLotes99, lcTotalReceitas99, lcTotalPVP99, lctotalEntidade99, lcTotalEmbalagens99, lcTotalPVP99, lctotalUtente99, lctotalIncentivo99

	Select ucrsLojas
	Goto Top
	Locate For ucrsLojas.serie_entSNS == lcNdoc
	If Found()
		lcFtSNS = .T.
	Endif




	Do Case
		Case lcNdoc == 0 And !Empty(lcVerso)

			If !Used("uCrsFt")
				Return .F.
			Else
				Select uCrsFt
				lcFtstamp = uCrsFt.ftstamp
			Endif

			If Used("uCrsDMl")
				fecha("uCrsDMl")
			Endif
			Create Cursor uCrsDMl(cnp c(7), portaria c(3), pvp c(6), Pref c(6), comparticipacao c(6), valorutente c(6), direitoopcao c(1))

			lcString = ""

			Local lcMsg
			lcMsg = ""

			** cursores e variaveis para o data-matrix (cabe�alho e linhas)
			Local dm_codigofarmacia, dm_codigoentidade, dm_data, dm_operador, dm_serie, dm_lote, dm_posicaolote, dm_nrvenda, dm_nrreceita, dm_campo2, dm_campo3, dm_campo4, dm_totalutenteliquido
			Store "" To dm_codigofarmacia, dm_codigoentidade, dm_data, dm_operador, dm_serie, dm_lote, dm_posicaolote, dm_nrvenda, dm_nrreceita, dm_campo2, dm_campo3, dm_campo4, dm_totalutenteliquido
			Create Cursor uCrsDMl(cnp c(7), portaria c(3), pvp c(6), Pref c(6), comparticipacao c(6), valorutente c(6), direitoopcao c(1))

			If uCrsFt.u_tipodoc==3 && se for uma inser��o de receita imprime cdata
				If uf_gerais_getdate(uCrsFt.cData,"DATA") == '01.01.1900'
					dm_data = "00000000" && datamatrix data
				Else
					dm_data = Replicate('0',8-Len(uf_gerais_getdate(uCrsFt.cData,"SQL"))) + uf_gerais_getdate(uCrsFt.cData,"SQL") && datamatrix data
				Endif
			Else
				If uf_gerais_getdate(uCrsFt.cData,"DATA") == '01.01.1900'
					dm_data = Replicate('0',8-Len(uf_gerais_getdate(uCrsFt.fdata,"SQL"))) + uf_gerais_getdate(uCrsFt.fdata,"SQL") && datamatrix data
				Else
					dm_data = Replicate('0',8-Len(uf_gerais_getdate(uCrsFt.cData,"SQL"))) + uf_gerais_getdate(uCrsFt.cData,"SQL") && data
				Endif
			Endif

			** Tipo e Nr Documento
			dm_operador = Replicate('0',10-Len(Alltrim(Left(uCrsFt.vendnm,10)))) + Alltrim(Left(uCrsFt.vendnm,10)) && datamatrix vendedor
			dm_nrvenda = Replicate('0',7-Len(astr(uCrsFt.fno))) + astr(uCrsFt.fno) && datamatrix n�mero venda
			dm_nrreceita = Replicate('0',20-Len(Alltrim(uCrsFt2.u_receita))) + Alltrim(uCrsFt2.u_receita) && datamatrix receita


			Do Case
				Case lcVerso = 1
					dm_codigoentidade = Replicate('0',3-Len(Alltrim(uCrsFt2.U_CODIGO))) + Alltrim(uCrsFt2.U_CODIGO) && C�digo entidade (??? c�digo plano)
				Case lcVerso = 2
					dm_codigoentidade = Replicate('0',3-Len(Alltrim(uCrsFt2.U_CODIGO2))) + Alltrim(uCrsFt2.U_CODIGO2) && C�digo entidade 2 (??? c�digo plano)
			Endcase

			dm_lote = Replicate('0',4-Len(astr(L.LOTE))) + astr(L.LOTE) && datamatrix lote
			dm_posicaolote = Replicate('0',3-Len(astr(R.NRECEITA))) + astr(R.NRECEITA) && damatrix posicao lote
			dm_serie = Replicate('0',3-Len(astr(S.SLOTE))) + astr(S.SLOTE) && datamatrix serie

			** guardar tabela diplomas para mapear com id para o c�digo matrix
			Local lcDiploma_id
			If !Used("uCrsDplms")
				uf_gerais_actGrelha("","uCrsDplms","select u_design, diploma_id from dplms (nolock) where u_design!=''")
			Endif

			****** IMPRESS�O DAS LINHAS ******
			Local lcContadorLinhas
			lcContadorLinhas = 1
			Select uCrsFi
			Do Case
				Case lcVerso = 1

					Scan For uCrsFi.U_ETTENT1 > 0 And Alltrim(uCrsFi.ftstamp) == Alltrim(uCrsFt.ftstamp)
						For I=1 To uCrsFi.qtt
							** datamatrix
							Select uCrsDMl
							Append Blank
							Replace uCrsDMl.cnp With Replicate('0', 7 - Len(Alltrim(Left(uCrsFi.ref,7)))) + Alltrim(Left(uCrsFi.ref,7)) && datamatrix cnp

							** datamatrix portaria
							lcDiploma_id = 0
							Select uCrsDplms
							Go Top
							Scan
								If Alltrim(uCrsDplms.u_design) == Alltrim(uCrsFi.u_diploma)
									lcDiploma_id = uCrsDplms.diploma_id
								Endif
							Endscan

							Select uCrsDMl
							Replace uCrsDMl.portaria With Replicate('0', 3 - Len(astr(lcDiploma_id))) + astr(lcDiploma_id) && datamatrix portaria
							Replace uCrsDMl.pvp With Replicate('0', 6 - Len(Left(astr(Round(uCrsFi.u_epvp * 100, 0)),6))) + Left(astr(Round(uCrsFi.u_epvp * 100,0)),6) && datamatrix pvp
							Replace uCrsDMl.Pref With Replicate('0', 6 - Len(Left(astr(Round(uCrsFi.u_epref * 100,0)),6))) + Left(astr(Round(uCrsFi.u_epref * 100,0)),6) && datamatrix pref
							Replace uCrsDMl.comparticipacao With Replicate('0', 6 - Len(Left(astr(Round(Round(uCrsFi.U_ETTENT1/uCrsFi.qtt,2) * 100,0)),6))) + Left(astr(Round(Round(uCrsFi.U_ETTENT1/uCrsFi.qtt,2) * 100,0)),6) && datamatrix comparticipacao
							Replace uCrsDMl.valorutente With Replicate('0', 6 - Len(Left(astr(Round(Round(uCrsFi.u_epvp-uCrsFi.U_ETTENT1/uCrsFi.qtt,2) * 100,0)),6))) + Left(astr(Round(Round(uCrsFi.u_epvp-uCrsFi.U_ETTENT1/uCrsFi.qtt,2) * 100,0)),6) && datamatrix valorutente

							If Empty(Alltrim(uCrsFi.lobs3)) && se n�o for utilizada excep��o
								Replace uCrsDMl.direitoopcao With uCrsFi.opcao && datamatrix direito opcao
							Else
								Replace uCrsDMl.direitoopcao With "N" && datamatrix direito opcao
							Endif
						Endfor

						Select uCrsFi
					Endscan

				Case lcVerso = 2
					Scan For uCrsFi.U_ETTENT2 > 0 And Alltrim(uCrsFi.ftstamp) == Alltrim(uCrsFt.ftstamp)
						For I=1 To uCrsFi.qtt

							** datamatrix
							Select uCrsDMl
							Append Blank
							Replace uCrsDMl.cnp With Replicate('0',7-Len(Alltrim(uCrsFi.ref))) + Alltrim(uCrsFi.ref) && datamatrix cnp

							** datamatrix portaria
							lcDiploma_id = 0
							Select uCrsDplms
							Go Top
							Scan
								If Alltrim(uCrsDplms.u_design) == Alltrim(uCrsFi.u_diploma)
									lcDiploma_id = uCrsDplms.diploma_id
								Endif
							Endscan

							Select uCrsDMl
							Replace uCrsDMl.portaria With Replicate('0', 3 - Len(astr(lcDiploma_id))) + astr(lcDiploma_id) && datamatrix portaria
							Replace uCrsDMl.pvp With Replicate('0', 6 - Len(Left(astr(Round(uCrsFi.u_epvp * 100, 0)),6))) + Left(astr(Round(uCrsFi.u_epvp * 100,0)),6) && datamatrix pvp
							Replace uCrsDMl.Pref With Replicate('0', 6 - Len(Left(astr(Round(uCrsFi.u_epref * 100,0)),6))) + Left(astr(Round(uCrsFi.u_epref * 100,0)),6) && datamatrix pref
							Replace uCrsDMl.comparticipacao With Replicate('0', 6 - Len(Left(astr(Round(Round(uCrsFi.U_ETTENT2/uCrsFi.qtt,2) * 100,0)),6))) + Left(astr(Round(Round(uCrsFi.U_ETTENT2/uCrsFi.qtt,2) * 100,0)),6) && datamatrix comparticipacao
							Replace uCrsDMl.valorutente With Replicate('0', 6 - Len(Left(astr(Round(Round(uCrsFi.u_epvp-uCrsFi.U_ETTENT2/uCrsFi.qtt,2) * 100,0)),6))) + Left(astr(Round(Round(uCrsFi.u_epvp-uCrsFi.U_ETTENT2/uCrsFi.qtt,2) * 100,0)),6) && datamatrix valorutente

							If Empty(Alltrim(uCrsFi.lobs3)) && se n�o for utilizada excep��o
								Replace uCrsDMl.direitoopcao With uCrsFi.opcao && datamatrix direito opcao
							Else
								Replace uCrsDMl.direitoopcao With "N" && datamatrix direito opcao
							Endif
						Endfor

						Select uCrsFi
					Endscan
			Endcase

			&& Imprime linhas que n�o est�o preenchidas
			Select uCrsDMl
			Count To lcPosicoesDML
			If lcPosicoesDML < 4
				For I=1 To 4-lcPosicoesDML

					** datamatrix
					Select uCrsDMl
					Append Blank
					Replace cnp With Replicate('0',7) && datamatrix cnp
					Replace portaria With Replicate('0',3) && datamatrix portaria
					Replace pvp With Replicate('0', 6) && datamatrix pvp
					Replace Pref With Replicate('0', 6) && datamatrix pref
					Replace comparticipacao With Replicate('0', 6) && datamatrix comparticipacao
					Replace valorutente With Replicate('0', 6) && datamatrix valorutente
					Replace direitoopcao With 'I'
				Endfor
			Endif

			Select uCrsFi

			** PREPARAR OS TOTAIS
			Do Case
				Case lcVerso = 1
					Calculate Sum(uCrsFi.u_epvp*uCrsFi.qtt) For uCrsFi.U_ETTENT1>0 And Alltrim(uCrsFi.ftstamp) = Alltrim(uCrsFt.ftstamp) To totalPVP
					Calculate Sum(uCrsFi.qtt) For uCrsFi.U_ETTENT1>0 And Alltrim(uCrsFi.ftstamp) = Alltrim(uCrsFt.ftstamp) To totalQtt
					Calculate Sum(uCrsFi.U_ETTENT1) For uCrsFi.U_ETTENT1>0 And Alltrim(uCrsFi.ftstamp) = Alltrim(uCrsFt.ftstamp) To totalComp
					Calculate Sum(uCrsFi.u_epvp*uCrsFi.qtt-uCrsFi.U_ETTENT1) For uCrsFi.U_ETTENT1>0 And Alltrim(uCrsFi.ftstamp) = Alltrim(uCrsFt.ftstamp) To totalUtente
				Case lcVerso = 2
					Calculate Sum(uCrsFi.u_epvp*uCrsFi.qtt) For uCrsFi.U_ETTENT2>0 And Alltrim(uCrsFi.ftstamp) = Alltrim(uCrsFt.ftstamp) To totalPVP
					Calculate Sum(uCrsFi.qtt) For uCrsFi.U_ETTENT2>0 And Alltrim(uCrsFi.ftstamp) = Alltrim(uCrsFt.ftstamp) To totalQtt
					Calculate Sum(uCrsFi.U_ETTENT2) For uCrsFi.U_ETTENT2>0 And Alltrim(uCrsFi.ftstamp) = Alltrim(uCrsFt.ftstamp) To totalComp
					Calculate Sum(uCrsFi.u_epvp*uCrsFi.qtt-uCrsFi.U_ETTENT2) For uCrsFi.U_ETTENT2>0 And Alltrim(uCrsFi.ftstamp) = Alltrim(uCrsFt.ftstamp) To totalUtente
					**calculate sum(uCrsFi.pvp4_fee*uCrsFi.qtt-uCrsFi.u_ettent2) FOR uCrsFi.u_ettent2>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalIncentivo
			Endcase

			dm_totalutenteliquido = Replicate('0',6-Len(astr(Round(totalUtente,2) * 100))) + astr(Round(totalUtente,2) * 100) && datamatrix total utente iliquido
			**dm_totalutenteliquido = REPLICATE('0',6-LEN(astr(round(totalUtente,2) * 100))) + astr(round(totalUtente,2) * 100) && datamatrix total incentivo iliquido


			** DataMatrix **
			Local lcDM
			lcDM = ""

			Select ucrse1
			Go Top

			** vers�o
			lcDM = lcDM	+ '103'

			** c�digo farm�cia
			lcDM = lcDM + Replicate('0',6-Len(astr(ucrse1.u_infarmed))) + astr(ucrse1.u_infarmed)

			** codigo entidade + data + operador + serie + lote + posicao lote + numero venda + numero receita + campo2 + campo3 + campo4
			lcDM = lcDM + dm_codigoentidade + dm_data + dm_operador + dm_serie + dm_lote + dm_posicaolote + dm_nrvenda + dm_nrreceita

			** campo2 + campo3 + campo4
			lcDM = lcDM	+ "000000000000" + "000000000000" + "00000000000000000000"

			** linhas
			Select uCrsDMl
			Go Top
			Scan
				** cnp + portaria + pvp + pref + comparticipacao + valor utente + direitoopcao
				lcDM = lcDM + uCrsDMl.cnp + uCrsDMl.portaria + uCrsDMl.pvp + uCrsDMl.Pref + uCrsDMl.comparticipacao + uCrsDMl.valorutente + uCrsDMl.direitoopcao
			Endscan
			fecha("uCrsDMl")

			** total utente liquido
			lcString = lcDM + dm_totalutenteliquido
			****************

		Case lcFtSNS == .T.

			Local lnFtNo
			lnFtNo = 1
			If !Used("FT")
				Return .F.
			Else
				Select ft
				lcFtstamp = ft.ftstamp
				lnFtNo  = ft.no
			Endif

			Local lcAbrev
			lcAbrev = "SNS"

			**Prestacao Vacinas
			If(lnFtNo==142)
				lcAbrev= "VacinasSNS"
			Else
				lcAbrev = Alltrim(uf_gerais_getUmValor("ft2", "id_efr_externa", "ft2stamp = '" + ft.ftstamp + "'"))
			Endif


			&& Aten��o ques esta SP � partilhada com o webservice que envia a fatura ao SNS eletronicamente.
			Select ft
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_receituario_fe_detalheLote <<YEAR(Ft.cdata)>>, <<MONTH(Ft.cdata)>>, '<<lcAbrev>>' , '', '', 0, 0, '<<mysite>>'
			ENDTEXT



			If !uf_gerais_actGrelha("","ucrsDetalheLoteTemp",lcSql)
				uf_perguntalt_chama("N�o foi poss�vel definir a configura��o de impress�es. Por favor contacte o Suporte.","OK","",16)
				Return .F.
			Endif

			Select ucrsDetalheLoteTemp
			Select Max(LOTE) As loteMax, Max(NRECEITA) As receitaMax From ucrsDetalheLoteTemp Group By tlote Into Cursor ucrsDetalheLoteTemp2 Readwrite && Necess�rio para calcular o sum distinct
			Select Sum(loteMax) As totalLotes, Sum(receitaMax) As totalReceitas From ucrsDetalheLoteTemp2 Into Cursor ucrsDetalheLoteTemp3 Readwrite
			Select tlote,LOTE,Max(NRECEITA) As receitaMax From ucrsDetalheLoteTemp Group By tlote,LOTE Into Cursor ucrsDetalheLoteTemp4 Readwrite
			Select Sum(receitaMax) As totalReceitas From ucrsDetalheLoteTemp4  Into Cursor ucrsDetalheLoteTemp5 Readwrite

			Select ucrsDetalheLoteTemp3
			lcTotalLotes = ucrsDetalheLoteTemp3.totalLotes
			Select ucrsDetalheLoteTemp5
			lcTotalReceitas = ucrsDetalheLoteTemp5.totalReceitas

			Select ucrsDetalheLoteTemp
			Calculate Sum(ucrsDetalheLoteTemp.pvp) To lcTotalPVP
			Calculate Sum(qtt) To lcTotalEmbalagens
			Calculate Sum(Comp) To lcTotalEntidade
			Calculate Sum(utente) To lcTotalUtente
			Calculate Sum(pvp4_fee) To lctotalIncentivo

			lctotalIncentivo = uf_gerais_calcFee(lctotalIncentivo)

			&& Calcular nr de linhas da fatura -> tlote distinct
			Select tlote From ucrsDetalheLoteTemp Group By tlote Into Cursor ucrsDetalheLoteTempAux Readwrite
			Select ucrsDetalheLoteTempAux
			lcNumeroLinhas = Reccount("ucrsDetalheLoteTempAux")


			** Tipo Lote 10
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "10" To lcTotalLotes10

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "10" To lcTotalReceitas10
			Select ucrsDetalheLoteTemp

			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "10" To lcTotalPVP10
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "10" To lctotalEntidade10
			Calculate Sum(qtt) For tlote = "10" To lcTotalEmbalagens10
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "10" To lcTotalPVP10
			Calculate Sum(utente) For tlote = "10" To lctotalUtente10
			Calculate Sum(pvp4_fee) For tlote = "10" To lctotalIncentivo10

			lctotalIncentivo10 = uf_gerais_calcFee(lctotalIncentivo10)


			** Tipo Lote 11
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "11" To lcTotalLotes11

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "11" To lcTotalReceitas11
			Select ucrsDetalheLoteTemp

			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "11" To lcTotalPVP11
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "11" To lctotalEntidade11
			Calculate Sum(qtt) For tlote = "11" To lcTotalEmbalagens11
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "11" To lcTotalPVP11
			Calculate Sum(utente) For tlote = "11" To lctotalUtente11
			Calculate Sum(pvp4_fee) For tlote = "11" To lctotalIncentivo11

			lctotalIncentivo11= uf_gerais_calcFee(lctotalIncentivo11)


			** Tipo Lote 12
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "12" To lcTotalLotes12

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "12" To lcTotalReceitas12
			Select ucrsDetalheLoteTemp

			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "12" To lcTotalPVP12
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "12" To lctotalEntidade12
			Calculate Sum(qtt) For tlote = "12" To lcTotalEmbalagens12
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "12" To lcTotalPVP12
			Calculate Sum(utente) For tlote = "12" To lctotalUtente12
			Calculate Sum(pvp4_fee) For tlote = "12" To lctotalIncentivo12

			lctotalIncentivo12= uf_gerais_calcFee(lctotalIncentivo12)


			** Tipo Lote 13
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "13" To lcTotalLotes13

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "13" To lcTotalReceitas13
			Select ucrsDetalheLoteTemp

			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "13" To lcTotalPVP13
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "13" To lctotalEntidade13
			Calculate Sum(qtt) For tlote = "13" To lcTotalEmbalagens13
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "13" To lcTotalPVP13
			Calculate Sum(utente) For tlote = "13" To lctotalUtente13
			Calculate Sum(pvp4_fee) For tlote = "13" To lctotalIncentivo13

			lctotalIncentivo13= uf_gerais_calcFee(lctotalIncentivo13)


			** Tipo Lote 15
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "15" To lcTotalLotes15

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "15" To lcTotalReceitas15

			Select ucrsDetalheLoteTemp
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "15" To lcTotalPVP15
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "15" To lctotalEntidade15
			Calculate Sum(qtt) For tlote = "15" To lcTotalEmbalagens15
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "15" To lcTotalPVP15
			Calculate Sum(utente) For tlote = "15" To lctotalUtente15
			Calculate Sum(pvp4_fee) For tlote = "15" To lctotalIncentivo15

			lctotalIncentivo15= uf_gerais_calcFee(lctotalIncentivo15)


			** Tipo Lote 16
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "16" To lcTotalLotes16



			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "16" To lcTotalReceitas16
			Select ucrsDetalheLoteTemp
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "16" To lcTotalPVP16
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "16" To lctotalEntidade16
			Calculate Sum(qtt) For tlote = "16" To lcTotalEmbalagens16
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "16" To lcTotalPVP16
			Calculate Sum(utente) For tlote = "16" To lctotalUtente16
			Calculate Sum(pvp4_fee) For tlote = "16" To lctotalIncentivo16

			lctotalIncentivo16= uf_gerais_calcFee(lctotalIncentivo16)


			** Tipo Lote 17
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "17" To lcTotalLotes17

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "17" To lcTotalReceitas17
			Select ucrsDetalheLoteTemp

			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "17" To lcTotalPVP17
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "17" To lctotalEntidade17
			Calculate Sum(qtt) For tlote = "17" To lcTotalEmbalagens17
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "17" To lcTotalPVP17
			Calculate Sum(utente) For tlote = "17" To lctotalUtente17
			Calculate Sum(pvp4_fee) For tlote = "17" To lctotalIncentivo17

			lctotalIncentivo17 = uf_gerais_calcFee(lctotalIncentivo17)


			** Tipo Lote 18
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "18" To lcTotalLotes18

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "18" To lcTotalReceitas18
			Select ucrsDetalheLoteTemp

			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "18" To lcTotalPVP18
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "18" To lctotalEntidade18
			Calculate Sum(qtt) For tlote = "18" To lcTotalEmbalagens18
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "18" To lcTotalPVP18
			Calculate Sum(utente) For tlote = "18" To lctotalUtente18
			Calculate Sum(pvp4_fee) For tlote = "18" To lctotalIncentivo18

			lctotalIncentivo18= uf_gerais_calcFee(lctotalIncentivo18)


			** Tipo Lote 19
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "19" To lcTotalLotes19

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "19" To lcTotalReceitas19
			Select ucrsDetalheLoteTemp

			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "19" To lcTotalPVP19
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "19" To lctotalEntidade19
			Calculate Sum(qtt) For tlote = "19" To lcTotalEmbalagens19
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "19" To lcTotalPVP19
			Calculate Sum(utente) For tlote = "19" To lctotalUtente19
			Calculate Sum(pvp4_fee) For tlote = "19" To lctotalIncentivo19

			lctotalIncentivo19= uf_gerais_calcFee(lctotalIncentivo19)


			** Tipo Lote 23
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "23" To lcTotalLotes23

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "23" To lcTotalReceitas23

			Select ucrsDetalheLoteTemp
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "23" To lcTotalPVP23
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "23" To lctotalEntidade23
			Calculate Sum(ucrsDetalheLoteTemp.qtt) For tlote = "23" To lcTotalEmbalagens23
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "23" To lcTotalPVP23
			Calculate Sum(utente) For tlote = "23" To lctotalUtente23
			Calculate Sum(pvp4_fee) For tlote = "23" To lctotalIncentivo23

			lctotalIncentivo23= uf_gerais_calcFee(lctotalIncentivo23)


			** Tipo Lote 30
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "30" To lcTotalLotes30

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "30" To lcTotalReceitas30

			Select ucrsDetalheLoteTemp
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "30" To lcTotalPVP30
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "30" To lctotalEntidade30
			Calculate Sum(ucrsDetalheLoteTemp.qtt) For tlote = "30" To lcTotalEmbalagens30
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "30" To lcTotalPVP30
			Calculate Sum(utente) For tlote = "30" To lctotalUtente30
			Calculate Sum(pvp4_fee) For tlote = "30" To lctotalIncentivo30

			lctotalIncentivo30= uf_gerais_calcFee(lctotalIncentivo30)

			** Tipo Lote 95
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "95" To lcTotalLotes95

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "95" To lcTotalReceitas95

			Select ucrsDetalheLoteTemp
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "95" To lcTotalPVP95
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "95" To lctotalEntidade95
			Calculate Sum(ucrsDetalheLoteTemp.qtt) For tlote = "95" To lcTotalEmbalagens95
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "95" To lcTotalPVP95
			Calculate Sum(utente) For tlote = "95" To lctotalUtente95
			Calculate Sum(pvp4_fee) For tlote = "95" To lctotalIncentivo95

			lctotalIncentivo95 = uf_gerais_calcFee(lctotalIncentivo95)


			** Tipo Lote 96
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "96" To lcTotalLotes96

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "96" To lcTotalReceitas96

			Select ucrsDetalheLoteTemp
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "96" To lcTotalPVP96
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "96" To lctotalEntidade96
			Calculate Sum(ucrsDetalheLoteTemp.qtt) For tlote = "96" To lcTotalEmbalagens96
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "96" To lcTotalPVP96
			Calculate Sum(utente) For tlote = "96" To lctotalUtente96
			Calculate Sum(pvp4_fee) For tlote = "96" To lctotalIncentivo96

			lctotalIncentivo96 = uf_gerais_calcFee(lctotalIncentivo96)


			** Tipo Lote 97
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "97" To lcTotalLotes97

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "97" To lcTotalReceitas97

			Select ucrsDetalheLoteTemp
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "97" To lcTotalPVP97
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "97" To lctotalEntidade97
			Calculate Sum(ucrsDetalheLoteTemp.qtt) For tlote = "97" To lcTotalEmbalagens97
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "97" To lcTotalPVP97
			Calculate Sum(utente) For tlote = "97" To lctotalUtente97
			Calculate Sum(pvp4_fee) For tlote = "97" To lctotalIncentivo97


			lctotalIncentivo97= uf_gerais_calcFee(lctotalIncentivo97)


			** Tipo Lote 98
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "98" To lcTotalLotes98

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "98" To lcTotalReceitas98

			Select ucrsDetalheLoteTemp
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "98" To lcTotalPVP98
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "98" To lctotalEntidade98
			Calculate Sum(ucrsDetalheLoteTemp.qtt) For tlote = "98" To lcTotalEmbalagens98
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "98" To lcTotalPVP98
			Calculate Sum(utente) For tlote = "98" To lctotalUtente98
			Calculate Sum(pvp4_fee) For tlote = "98" To lctotalIncentivo98


			lctotalIncentivo98 = uf_gerais_calcFee(lctotalIncentivo98)


			** Tipo Lote 99
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "99" To lcTotalLotes99

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "99" To lcTotalReceitas99

			Select ucrsDetalheLoteTemp
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "99" To lcTotalPVP99
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "99" To lctotalEntidade99
			Calculate Sum(ucrsDetalheLoteTemp.qtt) For tlote = "99" To lcTotalEmbalagens99
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "99" To lcTotalPVP99
			Calculate Sum(utente) For tlote = "99" To lctotalUtente99
			Calculate Sum(pvp4_fee) For tlote = "99" To lctotalIncentivo99

			lctotalIncentivo99= uf_gerais_calcFee(lctotalIncentivo99)


			lcString = ""
			Select ucrsLojas
			Locate For Alltrim(ucrsLojas.site) == Alltrim(mySite)
			If Found()

				lcString = "103" && Vers�o

				lcString = lcString + Replicate('0',6-Len(astr(ucrsLojas.infarmed))) + astr(ucrsLojas.infarmed) && C�digo Farm�cia
				lcString = lcString + '001' && C�digo Entidade
				lcString = lcString + 'A' && S�rie Fatura
				lcString = lcString + Replicate('0',4-Len(astr(ft.fno))) + astr(ft.fno) && N�mero Fatura
				lcString = lcString + Alltrim(uf_gerais_getdate(ft.fdata,"SQL")) && data Fatura
				lcString = lcString + astr(Year(ft.fdata)) && ano Fatur
				lcString = lcString + Replicate('0',2-Len(astr(Month(ft.fdata)))) + astr(Month(ft.fdata)) && m�s Fatura
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalLotes))) + astr(lcTotalLotes) && Total Lotes
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalReceitas))) + astr(lcTotalReceitas) && Total Receitas
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalEmbalagens))) + astr(lcTotalEmbalagens) && Total Embalagens - Validar Totais
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalPVP,9,2),'.',''))) + Strtran(astr(lcTotalPVP,9,2),'.','') && Total PVP
				lcString = lcString + "1" && Total Linhas detalhe IVA
				lcString = lcString + "006" && TAXAS de IVA fixo (TODO: VALIDAR IVA FIXO)
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalEntidade,9,2),'.',''))) + Strtran(astr(lcTotalEntidade,9,2),'.','') && Total Entidade
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalIncentivo,9,2),'.',''))) + Strtran(astr(lctotalIncentivo,9,2),'.','') && Total Incentivos
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalUtente,9,2),'.',''))) + Strtran(astr(lcTotalUtente,9,2),'.','') && Total Utente
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalEntidade,9,2),'.',''))) + Strtran(astr(lcTotalEntidade,9,2),'.','') && Total Comparticipacao = lctotalEntidade
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalEntidade23,9,2),'.',''))) + Strtran(astr(lctotalEntidade23,9,2),'.','') &&Total de 3� Protocolo 10, Designacao Tipo 23
				lcString = lcString + Replicate('0',2-Len(astr(lcNumeroLinhas))) + astr(lcNumeroLinhas) && N� de Linhas (N� Linhas da Fatura -> tlote distinct)

				** Tipo 10
				lcString = lcString + "010" &&001 - Tipo 10 SNS
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalLotes10))) + astr(lcTotalLotes10) &&Numero de Lotes
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalReceitas10))) + astr(lcTotalReceitas10) &&Numero de Receitas
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalEmbalagens10))) + astr(lcTotalEmbalagens10) &&Numero de Embalagens
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalPVP10,9,2),'.',''))) + Strtran(astr(lcTotalPVP10,9,2),'.','') &&Total PVP
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalUtente10,9,2),'.',''))) + Strtran(astr(lctotalUtente10,9,2),'.','') &&Total Utentes
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalEntidade10,9,2),'.',''))) + Strtran(astr(lctotalEntidade10,9,2),'.','') &&Total ENTIDADES
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalIncentivo10,9,2),'.',''))) + Strtran(astr(lctotalIncentivo10,9,2),'.','') &&Total INCENTIVO

				** Tipo 11
				lcString = lcString + "011" &&041 - Tipo 11 SNS - Den�as Profissionais
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalLotes11))) + astr(lcTotalLotes11) &&Numero de Lotes
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalReceitas11))) + astr(lcTotalReceitas11) &&Numero de Receitas
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalEmbalagens11))) + astr(lcTotalEmbalagens11) &&Numero de Embalagens
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalPVP11,9,2),'.',''))) + Strtran(astr(lcTotalPVP11,9,2),'.','') &&Total PVP
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalUtente11,9,2),'.',''))) + Strtran(astr(lctotalUtente11,9,2),'.','') &&Total Utentes
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalEntidade11,9,2),'.',''))) + Strtran(astr(lctotalEntidade11,9,2),'.','') &&Total ENTIDADES
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalIncentivo11,9,2),'.',''))) + Strtran(astr(lctotalIncentivo11,9,2),'.','') &&Total INCENTIVO new

				** Tipo 12
				lcString = lcString + "012" &&042 - Tipo 12 SNS - Diplomas - Paramiloidose
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalLotes12))) + astr(lcTotalLotes12) &&Numero de Lotes
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalReceitas12))) + astr(lcTotalReceitas12) &&Numero de Receitas
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalEmbalagens12))) + astr(lcTotalEmbalagens12) &&Numero de Embalagens
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalPVP12,9,2),'.',''))) + Strtran(astr(lcTotalPVP12,9,2),'.','') &&Total PVP
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalUtente12,9,2),'.',''))) + Strtran(astr(lctotalUtente12,9,2),'.','') &&Total Utentes
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalEntidade12,9,2),'.',''))) + Strtran(astr(lctotalEntidade12,9,2),'.','') &&Total ENTIDADES
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalIncentivo12,9,2),'.',''))) + Strtran(astr(lctotalIncentivo12,9,2),'.','') &&Total INCENTIVO new

				**
				lcString = lcString + "013" &&067 - Tipo 13	SNS - Diplomas - Lupus, Hem, Talas, Depra
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalLotes13))) + astr(lcTotalLotes13) &&Numero de Lotes
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalReceitas13))) + astr(lcTotalReceitas13) &&Numero de Receitas
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalEmbalagens13))) + astr(lcTotalEmbalagens13) &&Numero de Embalagens
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalPVP13,9,2),'.',''))) + Strtran(astr(lcTotalPVP13,9,2),'.','') &&Total PVP
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalUtente13,9,2),'.',''))) + Strtran(astr(lctotalUtente13,9,2),'.','') &&Total Utentes
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalEntidade13,9,2),'.',''))) + Strtran(astr(lctotalEntidade13,9,2),'.','') &&Total ENTIDADES
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalIncentivo13,9,2),'.',''))) + Strtran(astr(lctotalEntidade13,9,2),'.','') &&Total INCENTIVO new

				**
				lcString = lcString + "015" &&048 - Tipo 15	SNS - Regime Especial
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalLotes15))) + astr(lcTotalLotes15) &&Numero de Lotes
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalReceitas15))) + astr(lcTotalReceitas15) &&Numero de Receitas
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalEmbalagens15))) + astr(lcTotalEmbalagens15) &&Numero de Embalagens
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalPVP15,9,2),'.',''))) + Strtran(astr(lcTotalPVP15,9,2),'.','') &&Total PVP
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalUtente15,9,2),'.',''))) + Strtran(astr(lctotalUtente15,9,2),'.','') &&Total Utentes
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalEntidade15,9,2),'.',''))) + Strtran(astr(lctotalEntidade15,9,2),'.','') &&Total ENTIDADES
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalIncentivo15,9,2),'.',''))) + Strtran(astr(lctotalIncentivo15,9,2),'.','') &&Total INCENTIVO new

				**
				lcString = lcString + "016" &&049 - Tipo 16	SNS - Regime Especial - Diplomas
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalLotes16))) + astr(lcTotalLotes16) &&Numero de Lotes
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalReceitas16))) + astr(lcTotalReceitas16) &&Numero de Receitas
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalEmbalagens16))) + astr(lcTotalEmbalagens16)&&Numero de Embalagens
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalPVP16,9,2),'.',''))) + Strtran(astr(lcTotalPVP16,9,2),'.','') &&Total PVP
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalUtente16,9,2),'.',''))) + Strtran(astr(lctotalUtente16,9,2),'.','') &&Total Utentes
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalEntidade16,9,2),'.',''))) + Strtran(astr(lctotalEntidade16,9,2),'.','') &&Total ENTIDADES
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalIncentivo16,9,2),'.',''))) + Strtran(astr(lctotalIncentivo16,9,2),'.','') &&Total INCENTIVO new

				**
				lcString = lcString + "017" &&046 - Tipo 17 SNS - Trabalhadores Migrantes
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalLotes17))) + astr(lcTotalLotes17) &&Numero de Lotes
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalReceitas17))) + astr(lcTotalReceitas17) &&Numero de Receitas
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalEmbalagens17))) + astr(lcTotalEmbalagens17) &&Numero de Embalagens
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalPVP17,9,2),'.',''))) + Strtran(astr(lcTotalPVP17,9,2),'.','') &&Total PVP
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalUtente17,9,2),'.',''))) + Strtran(astr(lctotalUtente17,9,2),'.','') &&Total Utentes
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalEntidade17,9,2),'.',''))) + Strtran(astr(lctotalEntidade17,9,2),'.','') &&Total ENTIDADES
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalIncentivo17,9,2),'.',''))) + Strtran(astr(lctotalIncentivo17,9,2),'.','') &&Total INCENTIVO new

				**
				lcString = lcString + "018" &&045 - Tipo 18 SNS - Trabalhadores Migrantes - Diplomas
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalLotes18))) + astr(lcTotalLotes18) &&Numero de Lotes
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalReceitas18))) + astr(lcTotalReceitas18) &&Numero de Receitas
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalEmbalagens18))) + astr(lcTotalEmbalagens18) &&Numero de Embalagens
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalPVP18,9,2),'.',''))) + Strtran(astr(lcTotalPVP18,9,2),'.','') &&Total PVP
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalUtente18,9,2),'.',''))) + Strtran(astr(lctotalUtente18,9,2),'.','') &&Total Utentes
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalEntidade18,9,2),'.',''))) + Strtran(astr(lctotalEntidade18,9,2),'.','') &&Total ENTIDADES
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalIncentivo18,9,2),'.',''))) + Strtran(astr(lctotalIncentivo18,9,2),'.','') &&Total INCENTIVO new

				**
				lcString = lcString + "019" &&047 - Tipo 19	SNS - Manipulados - Diabeticos
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalLotes19)))  + astr(lcTotalLotes19) &&Numero de Lotes
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalReceitas19)))  + astr(lcTotalReceitas19) &&Numero de Receitas
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalEmbalagens19)))  + astr(lcTotalEmbalagens19) &&Numero de Embalagens
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalPVP19,9,2),'.','')))  + Strtran(astr(lcTotalPVP19,9,2),'.','') &&Total PVP
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalUtente19,9,2),'.','')))  + Strtran(astr(lctotalUtente19,9,2),'.','') &&Total Utentes
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalEntidade19,9,2),'.','')))  + Strtran(astr(lctotalEntidade19,9,2),'.','') &&Total ENTIDADES
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalIncentivo19,9,2),'.','')))  + Strtran(astr(lctotalIncentivo19,9,2),'.','') &&Total INCENTIVO new

				** Tipo 23
				lcString = lcString + "023"  &&0DS - Tipo 23 Diabetes
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalLotes23))) + astr(lcTotalLotes23) && 0DS - Numero de Lotes
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalReceitas23))) + astr(lcTotalReceitas23) && 0DS - Numero de Receitas
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalEmbalagens23))) + astr(lcTotalEmbalagens23) && 0DS - Numero de Embalagens
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalPVP23,9,2),'.',''))) + Strtran(astr(lcTotalPVP23,9,2),'.','') && 0DS - Total PVP
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalUtente23,9,2),'.',''))) + Strtran(astr(lctotalUtente23,9,2),'.','') && 0DS - Total Utentes
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalEntidade23,9,2),'.',''))) + Strtran(astr(lctotalEntidade23,9,2),'.','') && 0DS - Total ENTIDADES
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalIncentivo23,9,2),'.',''))) + Strtran(astr(lctotalIncentivo23,9,2),'.','') && 0DS - Total INCENTIVOS

				** Tipo 30
				lcString = lcString + "030"  &&0DS - Tipo 23 Diabetes
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalLotes30))) + astr(lcTotalLotes30) && Numero de Lotes
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalReceitas30))) + astr(lcTotalReceitas30) && Numero de Receitas
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalEmbalagens30))) + astr(lcTotalEmbalagens30) && Numero de Embalagens
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalPVP30,9,2),'.',''))) + Strtran(astr(lcTotalPVP30,9,2),'.','') && Total PVP
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalUtente30,9,2),'.',''))) + Strtran(astr(lctotalUtente30,9,2),'.','') && Total Utentes
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalEntidade30,9,2),'.',''))) + Strtran(astr(lctotalEntidade30,9,2),'.','') && Total ENTIDADES
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalIncentivo30,9,2),'.',''))) + Strtran(astr(lctotalIncentivo30,9,2),'.','') && Total INCENTIVOS

				** Tipo 95 - Vacinas Gripe e Covid
				lcString = lcString + "095"
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalLotes95))) + astr(lcTotalLotes95) && Numero de Lotes
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalReceitas95))) + astr(lcTotalReceitas95) && Numero de Receitas
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalEmbalagens95))) + astr(lcTotalEmbalagens95) && Numero de Embalagens
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalPVP95,9,2),'.',''))) + Strtran(astr(lcTotalPVP95,9,2),'.','') && Total PVP
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalUtente95,9,2),'.',''))) + Strtran(astr(lctotalUtente95,9,2),'.','') && Total Utentes
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalEntidade95,9,2),'.',''))) + Strtran(astr(lctotalEntidade95,9,2),'.','') && Total ENTIDADES
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalIncentivo95,9,2),'.',''))) + Strtran(astr(lctotalIncentivo95,9,2),'.','') && Total INCENTIVOS

				** Tipo 96
				lcString = lcString + "096"
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalLotes96))) + astr(lcTotalLotes96) && Numero de Lotes
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalReceitas96))) + astr(lcTotalReceitas96) && Numero de Receitas
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalEmbalagens96))) + astr(lcTotalEmbalagens96) && Numero de Embalagens
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalPVP96,9,2),'.',''))) + Strtran(astr(lcTotalPVP96,9,2),'.','') && Total PVP
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalUtente96,9,2),'.',''))) + Strtran(astr(lctotalUtente96,9,2),'.','') && Total Utentes
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalEntidade96,9,2),'.',''))) + Strtran(astr(lctotalEntidade96,9,2),'.','') && Total ENTIDADES
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalIncentivo96,9,2),'.',''))) + Strtran(astr(lctotalIncentivo96,9,2),'.','') && Total INCENTIVOS

				** Tipo 97
				lcString = lcString + "097"  &&0DS - Tipo 23 Diabetes
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalLotes97))) + astr(lcTotalLotes97) && Numero de Lotes
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalReceitas97))) + astr(lcTotalReceitas97) && Numero de Receitas
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalEmbalagens97))) + astr(lcTotalEmbalagens97) && Numero de Embalagens
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalPVP97,9,2),'.',''))) + Strtran(astr(lcTotalPVP97,9,2),'.','') && Total PVP
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalUtente97,9,2),'.',''))) + Strtran(astr(lctotalUtente97,9,2),'.','') && Total Utentes
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalEntidade97,9,2),'.',''))) + Strtran(astr(lctotalEntidade97,9,2),'.','') && Total ENTIDADES
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalIncentivo97,9,2),'.',''))) + Strtran(astr(lctotalIncentivo97,9,2),'.','') && Total INCENTIVOS

				** Tipo 98
				lcString = lcString + "098"  &&0DS - Tipo 23 Diabetes
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalLotes98))) + astr(lcTotalLotes98) && Numero de Lotes
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalReceitas98))) + astr(lcTotalReceitas98) && Numero de Receitas
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalEmbalagens98))) + astr(lcTotalEmbalagens98) && Numero de Embalagens
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalPVP98,9,2),'.',''))) + Strtran(astr(lcTotalPVP98,9,2),'.','') && Total PVP
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalUtente98,9,2),'.',''))) + Strtran(astr(lctotalUtente98,9,2),'.','') && Total Utentes
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalEntidade98,9,2),'.',''))) + Strtran(astr(lctotalEntidade98,9,2),'.','') && Total ENTIDADES
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalIncentivo98,9,2),'.',''))) + Strtran(astr(lctotalIncentivo98,9,2),'.','') && Total INCENTIVOS

				** Tipo 99
				lcString = lcString + "099"  &&0DS - Tipo 23 Diabetes
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalLotes99))) + astr(lcTotalLotes99) && Numero de Lotes
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalReceitas99))) + astr(lcTotalReceitas99) && Numero de Receitas
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalEmbalagens99))) + astr(lcTotalEmbalagens99) && Numero de Embalagens
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalPVP99,9,2),'.',''))) + Strtran(astr(lcTotalPVP99,9,2),'.','') && Total PVP
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalUtente99,9,2),'.',''))) + Strtran(astr(lctotalUtente99,9,2),'.','') && Total Utentes
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalEntidade99,9,2),'.',''))) + Strtran(astr(lctotalEntidade99,9,2),'.','') && Total ENTIDADES
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lctotalIncentivo99,9,2),'.',''))) + Strtran(astr(lctotalIncentivo99,9,2),'.','') && Total INCENTIVOS

				&&
				&& TODO: validar datamatrix - acrescentar Hash + nr programa certificado + total protocolo 3 !!!!
				&&
			Endif

			&& nota de credito e nota de debito
		Case lcU_tipodoc == 7 Or lcU_tipodoc == 6

			If !Used("FT")
				Return .F.
			Else
				Select ft
				lcFtstamp = ft.ftstamp
			Endif

			**
			lcString = ""
			Store 0 To lcNumLinhasTaxa1, lcNumLinhasTaxa2, lcNumLinhasTaxa3, lcNumLinhasTaxa4, lcNumLinhasTaxa5, lcNumLinhasTaxa6, lcNumLinhasTaxa7, lcNumLinhasTaxa8, lcNumLinhasTaxa9

			Select ucrsLojas
			Locate For Alltrim(ucrsLojas.site) == Alltrim(mySite)
			If Found()
				lcString = "103" && Vers�o
				lcString = lcString + Replicate('0',6-Len(astr(ucrsLojas.infarmed))) + astr(ucrsLojas.infarmed) && C�digo Farm�cia
				If lcU_tipodoc == 7
					lcString = lcString + 'C' && Tipo Documento
				Else
					lcString = lcString + 'D' && Tipo Documento
				Endif
				lcString = lcString + '001' && C�digo Entidade
				lcString = lcString + 'F' && S�rie Documento Nota Credito
				lcString = lcString + Replicate('0',4-Len(astr(ft.fno))) + astr(ft.fno) && N�mero Documento
				lcString = lcString + Alltrim(uf_gerais_getdate(ft.fdata,"SQL")) && data Documento
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(Abs(ft.etotal),9,2),'.',''))) + astr(Strtran(astr(Abs(ft.etotal),9,2),'.','')) && Total PVP

				lcString = lcString +  "1" && Total Linha Iva
				lcString = lcString +  Replicate('0',3-Len(astr(ft.ivatx1))) + astr(ft.ivatx1)  && TaxaIva
				lcString = lcString +  Replicate('0',10-Len(Strtran(astr(Abs(ft.eivav1),9,2),'.',''))) + astr(Strtran(astr(Abs(ft.eivav1),9,2),'.',''))  && Total Iva

				**
				lcString = lcString + "1" && Total Linhas Detalhe de IVA
				Select Fi
				Go Top
				Scan For !Empty(Fi.ofistamp)
					lcValorIvaLinha = 0
					lcSql = ""
					TEXT TO lcSQL NOSHOW TEXTMERGE
						select
							ft.fno
							,ft.fdata
							,ft.etotal
							,eivav1
						from
							ft (nolock)
							inner join fi (nolock) on ft.ftstamp = fi.ftstamp
						where
							fi.fistamp = '<<fi.ofistamp>>'
					ENDTEXT
					If !uf_gerais_actGrelha("","ucrsDadosFactEntidade",lcSql)
						uf_perguntalt_chama("N�o foi possivel encontrar informa��o da fatura de origem.","OK","",16)
						Return .F.
					Endif
					Select ucrsDadosFactEntidade
					Go Top
					lcString = lcString + "E" && Serie
					lcString = lcString + Replicate('0',4-Len(astr(ucrsDadosFactEntidade.fno))) + astr(ucrsDadosFactEntidade.fno) && Numero
					lcString = lcString + Alltrim(uf_gerais_getdate(ucrsDadosFactEntidade.fdata,"SQL")) && Data
					lcString = lcString + Replicate('0',10-Len(Strtran(astr(Abs(Fi.etiliquido),9,2),'.',''))) + astr(Strtran(astr(Abs(Fi.etiliquido),9,2),'.','')) && Total
					lcString = lcString + "1" && Linhas Iva
					lcString = lcString + "006" && Iva
					lcValorIvaLinha = Abs(Fi.etiliquido - (Fi.etiliquido/ (Fi.iva/100+1)))
					lcString = lcString + Replicate('0',10-Len(Strtran(astr(Abs(lcValorIvaLinha),9,2),'.',''))) + astr(Strtran(astr(Abs(lcValorIvaLinha),9,2),'.','')) && Total Iva

				Endscan
			Endif

		Otherwise
			**
	Endcase



	If Empty(lcString) &&
		Return .F.
	Endif


	If Directory(Alltrim(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsPrint')
		lcFilePath = Alltrim(Alltrim(uf_gerais_getParameter("ADM0000000015","TEXT")) + "\Cache\" + Alltrim(lcFtstamp) + ".png")

		myFilePathDataMatrix = lcFilePath && variavel publica usada no IDU



		If !File(Alltrim(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsPrint\ltsPrint.jar')
			uf_perguntalt_chama("Ficheiro configura��o de Datamatrix n�o encontrado. Contacte o Suporte","OK","",64)
			Return .F.
		Endif

		lcWsPath = Alltrim(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsPrint\ltsPrint.jar "datamatrix" "' + Alltrim(lcFilePath) + '" "' + Alltrim(lcString) + '"'
		lcWsPath = "javaw -jar " + lcWsPath


		oWSShell = Createobject("WScript.Shell")
		oWSShell.Run(lcWsPath, 0, .F.)
	Endif

	Wait Window 'A criar codigo datamatrix...' Timeout 2 && Tempo para permitir a gera��o da imagem na cache

Endfunc


**  funcionalidade passou para uf_imprimirgerais_imprimir - Lu�s Leal 20150930
*!*	FUNCTION uf_imprimirgerais_textoIsencao
*!*		LOCAL lcMotIsencaoIva
*!*		PUBLIC myMotIsencaoIva
*!*		STORE .f. TO lcMotIsencaoIva
*!*		STORE '' TO myMotIsencaoIva

*!*		&& Verifica se deve imprimir motivo de isencao de Iva
*!*		lcMotIsencaoIva = uf_gerais_getParameter("ADM0000000261","Bool")
*!*
*!*		IF lcMotIsencaoIva == .t.
*!*			DO CASE
*!*				CASE myIsentoIva == .f.
*!*					myMotIsencaoIva = ''
*!*				CASE !empty(alltrim(ft2.motiseimp))
*!*					myMotIsencaoIva = ft2.motiseimp
*!*				CASE ft.pais == 3
*!*					myMotIsencaoIva = 'Isen��o prevista no n.� 3 do art.� 14.� do CIVA'
*!*				CASE ft.pais==2
*!*					myMotIsencaoIva = 'Isen��o ao abrigo da al�nea a) do art.� 14.� do RITI'
*!*				OTHERWISE
*!*					IF !EMPTY(uCrse1.motivo_isencao_iva)
*!*						myMotIsencaoIva = uCrse1.motivo_isencao_iva
*!*					ELSE
*!*						myMotIsencaoIva = 'Isen��o prevista no n.� 1 do art.� 9.� do CIVA'
*!*					ENDIF
*!*			ENDCASE
*!*		ELSE
*!*			myMotIsencaoIva = ''
*!*		ENDIF


*!*	ENDFUNC


**
Function uf_imprimirgerais_dataMatrixImagem
	Lparameters lcDocumento
	Public myFilePathDataMatrix

	If Empty(myFilePathDataMatrix)
		Return ""
	Else
		Return myFilePathDataMatrix
	Endif
Endfunc


**
Function uf_imprimirGerais_sair

	If Empty(myStampNCSeg)
		** RELEASE VAR PUB
		If MYIMPRESSAOORIGEM == "MCDT" Or MYIMPRESSAOORIGEM  == "PRESCRICAOMED"
			Prescricao.Enabled = .T.
		Endif

		If MYIMPRESSAOORIGEM  == "RECIBOENTASSOCIADOS"
			uf_MOVBANCARIOS_actualizar()
			&&uf_movBancarios_actDados()
		Endif



		Release MYIMPRESSAOORIGEM
		Release myNrAtend
		&& fecha painel
		If Type("IMPRIMIRGERAIS") != 'U'
			IMPRIMIRGERAIS.Hide
			IMPRIMIRGERAIS.Release
		Endif

		If Used("uCrsAuxMovimento")
			fecha("uCrsAuxMovimento")
		Endif
	Endif

	If Type("FTSeguradora") != 'U'

		If  Alltrim(FTSeguradora) = 'IMPRIME'
			If Empty(myStampNCSeg)
				uf_facturacao_sair()
				If Wexist("ATENDIMENTO")
					uf_atendimento_sair()
				Endif
			Endif
			FTSeguradora = ''
			If !Empty(myStampNCSeg)
				uf_facturacao_chama(Alltrim(myStampNCSeg))
				uf_imprimirgerais_Chama('FACTURACAO')
				myStampNCSeg = ''
			Endif
		Endif
		If  Alltrim(FTSeguradora) = 'NCSEGIMPRIME'
			FTSeguradora = ''
			myStampNCSeg = ''
			IMPRIMIRGERAIS.Hide
			IMPRIMIRGERAIS.Release
			uf_facturacao_sair()
			If Wexist("ATENDIMENTO")
				uf_atendimento_sair()
			Endif
		Endif
		FTSeguradora = ''


	Endif

Endfunc


** Carrega Cursores para Impress�o de Dados - M�dulo Associados
Function uf_imprimirGerais_imprimir_ResumoFtAssociadosPreparaCursores
	Lparameters I,lcdatadados,lcdataimpressao,lcdataimpressaotxt, lcNoCliente
	Local lcDataFact

	lcDataFact = Gomonth(lcdatadados,2)

	** Fatura��o a Entidades
	** Consulta Faturas Emitidas a Entidades
	TEXT TO lcSQL TEXTMERGE NOSHOW
    	exec up_PESQDOCASSOCIADOS_pesquisarDocumentos '', 0, '', '', <<lcNoCliente>>, '', '', <<YEAR(lcdatadados)>>, <<MONTH(lcdatadados)>>, 0, 1, ''
	ENDTEXT

	If !uf_gerais_actGrelha("","uCrsAssociadosFtEntidadePrint",lcSql)
		uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR A LISTAGEM DE DOCUMENTOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif


	** Faturado �s Farmacias
	** Consulta dos Documentos/Servi�os Faturados �s Farm�cias
	TEXT TO lcSQL TEXTMERGE NOSHOW
    	exec up_resumo_faturas_associados <<YEAR(lcDataFact)>>, <<MONTH(lcDataFact)>>, <<lcNoCliente>>, -1, '', ''
	ENDTEXT

	If !uf_gerais_actGrelha("","uCrsAssociadosFtClientePrint",lcSql)
		uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR A LISTAGEM DE DOCUMENTOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif

	** Prepara Cursores para impressao
	If Used("ucrsResumoFtAssociados")
		fecha("ucrsResumoFtAssociados")
	Endif

	Create Cursor ucrsResumoFtAssociados(tipo c(1), entidade c(55), ano N(4), mes N(2), ano_tratamento N(4), mes_tratamento N(4), valor N(13,3), valor_ret N(13,3), valor_pagar N(13,3), fno N(5), detalhe c(60), credito N(13,3), debito N(13,3))

	Select uCrsAssociadosFtEntidadePrint
	Go Top
	Scan

		Select ucrsResumoFtAssociados
		Append Blank
		Replace ucrsResumoFtAssociados.tipo		 				With "1"
		Replace ucrsResumoFtAssociados.entidade	 				With uCrsAssociadosFtEntidadePrint.nome_org
		Replace ucrsResumoFtAssociados.ano						With uCrsAssociadosFtEntidadePrint.ano
		Replace ucrsResumoFtAssociados.mes 						With uCrsAssociadosFtEntidadePrint.mes
		Replace ucrsResumoFtAssociados.ano_tratamento			With uCrsAssociadosFtEntidadePrint.ano_tratamento
		Replace ucrsResumoFtAssociados.mes_tratamento 			With uCrsAssociadosFtEntidadePrint.mes_tratamento
		Replace ucrsResumoFtAssociados.valor					With uCrsAssociadosFtEntidadePrint.valor
		Replace ucrsResumoFtAssociados.valor_ret 				With uCrsAssociadosFtEntidadePrint.valor_ret
		Replace ucrsResumoFtAssociados.valor_pagar				With uCrsAssociadosFtEntidadePrint.valor_pagar

	Endscan

	Select uCrsAssociadosFtClientePrint
	Go Top
	Scan

		Select ucrsResumoFtAssociados
		Append Blank
		Replace ucrsResumoFtAssociados.tipo 			With "2"
		Replace ucrsResumoFtAssociados.fno 				With uCrsAssociadosFtClientePrint.fno
		Replace ucrsResumoFtAssociados.ano 				With uCrsAssociadosFtClientePrint.ano
		Replace ucrsResumoFtAssociados.mes 				With uCrsAssociadosFtClientePrint.mes
		Replace ucrsResumoFtAssociados.ano_tratamento	With uCrsAssociadosFtClientePrint.ano
		Replace ucrsResumoFtAssociados.mes_tratamento	With uCrsAssociadosFtClientePrint.mes
		Replace ucrsResumoFtAssociados.detalhe 			With ""
		Replace ucrsResumoFtAssociados.credito 			With uCrsAssociadosFtClientePrint.credito
		Replace ucrsResumoFtAssociados.debito 			With uCrsAssociadosFtClientePrint.debito

	Endscan

	Select Distinct Nome, morada, codpost,Local From uCrsAssociadosFtClientePrint Into Cursor ucrsResumoFtAssociadosDadosFarmacia Readwrite
	Select Distinct nome_cl, morada_cl, codpost_cl,local_cl From uCrsAssociadosFtEntidadePrint Into Cursor ucrsDadosEntidadePrint Readwrite

	** Calculo de totais
	Select;
		SUM(Iif(ucrsResumoFtAssociados.tipo = "1",ucrsResumoFtAssociados.valor_pagar,0.000)) As totalEntidades;
		,Sum(Iif(ucrsResumoFtAssociados.tipo = "2",ucrsResumoFtAssociados.debito,0.000)) As totalFt;
		,Sum(Iif(ucrsResumoFtAssociados.tipo = "1",ucrsResumoFtAssociados.valor_pagar,0.000)) - Sum(Iif(ucrsResumoFtAssociados.tipo = "2",ucrsResumoFtAssociados.debito,0)) As Total;
		FROM ucrsResumoFtAssociados Into Cursor ucrsResumoFtAssociadosTotais Readwrite

	Go Top

Endfunc


** Impress�o Resumos Faturas Entidades - Modulo Associados (Painel Gest�o de Entidades)
Function uf_imprimirGerais_imprimir_ResumoFtEntidades
	Lparameters lctipo
	Local lcValidaDadosImpressao
	Store 0 To lcValidaDadosImpressao

	** Guarda a impressora por defeito do Windows***
	lcDefaultPrinter=Set("printer",2)
	**

	lcimpressao = Alltrim(IMPRIMIRGERAIS.pageframe1.page1.impressao.Value) && Alltrim(ucrsImpIdusDefault.nomeimpressao)

	** Obtem Nome do Ficheiro Tabela de Impressoes **
	lcSql = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select Top 1 nomeficheiro, tabela, temduplicados, numduplicados
		From b_impressoes (nolock)
		Where nomeimpressao = '<<lcimpressao>>'
	ENDTEXT

	If !uf_gerais_actGrelha("","templcSQL",lcSql)
		uf_perguntalt_chama("N�o foi poss�vel definir a configura��o de impress�es. Por favor contacte o Suporte.","OK","",16)
		Return
	Endif

	If Reccount("templcSQL")>0
		lcnomeFicheiro 	= Alltrim(templcSQL.nomeficheiro)
		lcprinter 		= IMPRIMIRGERAIS.pageframe1.page1.destino.Value
		lctabela		= templcSQL.tabela
		lctemduplicados = templcSQL.temduplicados
		lcnumduplicados = templcSQL.numduplicados
	Else
		Return .F.
	Endif

	If !(uf_gerais_actGrelha("","uCrsImpressao",lcSql))
		&&erro
	Endif

	****************************************
	** PREPARAR CURSORES PARA IMPRESS�ES **
	****************************************

	** Obtem dados Gerais da Empresa / Loja **
	If !Used("uCrsE1")
		uf_perguntalt_chama("N�o foi poss�vel verificar os dados da Empresa. Por favor contacte o suporte.","OK","",16)
		Return .F.
	Endif
	**

	** Guardar report a imprimir - ficheiro **
	lcreport = Alltrim(myPath)+"\analises\" + Alltrim(lcnomeFicheiro)

	** preparar impressora **
	If !(Alltrim(lcprinter) == "Impressora por defeito do Windows")
		Local lcValErroPrinter
		Try
			Set Printer To Name ''+Alltrim(lcprinter)+''
		Catch
			uf_perguntalt_chama("Foi detectado uma anomalia com a impressora." + Chr(13) + "Por favor contacte o Suporte.","OK","",64)
			lcValErroPrinter = .T.
		Endtry
		If lcValErroPrinter == .T.
			Return .F.
		Endif
	Endif

	Set Device To Print
	Set Console Off
	**

	uf_gerais_TextoRodape("CL_FATC_ORG")
	lcdataimpressaotxt = IMPRIMIRGERAIS.pageframe1.page1.Data.Value
	lcdataimpressao	   = Ctot(IMPRIMIRGERAIS.pageframe1.page1.Data.Value)
	lcdatadados		   = Ctot(IMPRIMIRGERAIS.pageframe1.page1.datadados.Value)

	lcDo = Iif(Empty(IMPRIMIRGERAIS.pageframe1.page1.txtDO.Value),0,Val(astr(IMPRIMIRGERAIS.pageframe1.page1.txtDO.Value)))
	lcAo = Iif(Empty(IMPRIMIRGERAIS.pageframe1.page1.txtAO.Value),0,Val(astr(IMPRIMIRGERAIS.pageframe1.page1.txtAO.Value)))

	lcNrDocImp = IMPRIMIRGERAIS.pageframe1.page1.nrDocImp.Value

	**
	If Empty(lcDo) And Empty(lcNrDocImp) Or Empty(lcAo) And Empty(lcNrDocImp)
		uf_perguntalt_chama("Intervalo de Entidades n�o est� definido. Por favor verfique.","OK","",16)
		Return .F.
	Endif

	** Criar Cursor para permitir imprimir por ordem alfab�tica das Entidades
	TEXT TO lcSQL NOSHOW TEXTMERGE
		SELECT * FROM b_utentes WHERE no between <<lcdo>> and <<lcAo>> and tipo = 'Entidade' order BY nome
	ENDTEXT
	If !(uf_gerais_actGrelha("","uCrsEntidadesOrdenadas",lcSql))
	Endif

	Do Case
		Case Like(lctipo,"print")
			**
			** Impress�o Por Periodo Dados e N� Entidade
			If Empty(lcNrDocImp)
				**
				I=0
				Select uCrsEntidadesOrdenadas
				Go Top
				Scan
					I = I+1

					If Used("ucrsResumoFtAssociados")
						fecha("ucrsResumoFtAssociados")
					Endif

					If Used("ucrsResumoFtAssociados2")
						fecha("ucrsResumoFtAssociados2")
					Endif

					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_PESQDOCASSOCIADOS_pesquisarDocumentos '' , <<uCrsEntidadesOrdenadas.no>>, '' ,'', 0	,'' , '', <<YEAR(lcdatadados)>>, <<MONTH(lcdatadados)>>, 0, 1, '', 1
					ENDTEXT

					If !uf_gerais_actGrelha("","ucrsResumoFtEntidades2",lcSql)
						uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
						Return .F.
					Endif

					Select * From ucrsResumoFtEntidades2 Order By nome_cl Into Cursor ucrsResumoFtEntidades Readwrite

					If Reccount("ucrsResumoFtEntidades") > 0 && Caso n�o tenha resultados n�o imprime

						lcValidaDadosImpressao = lcValidaDadosImpressao + 1

						**	Tipo: 1 - Resumo Facturas Entidades; 2 - Nota Lan�amento Associados; 3 - Aviso Lan�amento
						TEXT TO lcSQL TEXTMERGE NOSHOW
							exec up_associados_dadosEntidades <<uCrsEntidadesOrdenadas.no>>, 1, '<<lcdataimpressaotxt>>', '<<uf_gerais_getdate(lcdatadados,"SQL")>>', 1, '', 0
						ENDTEXT

						If !uf_gerais_actGrelha("","ucrsDadosEntidadePrint",lcSql)
							uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
							Return .F.
						Endif

						If Used("ucrsDadosEntidadePrint")
							Select ucrsDadosEntidadePrint
							Go Top
						Endif

						Select ucrsResumoFtEntidades
						Go Top

						*********************
						If lctemduplicados == .F.
							lcnumduplicados = 0
						Endif
						For j=0 To lcnumduplicados
							myVersaoDoc = uf_gerais_versaodocumento(j+1)

							** IMPRIMIR
							If Like(lctipo,"print")
								Try
									Report Form Alltrim(lcreport) To Printer
								Catch

									uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
								Endtry
							Endif
						Endfor
					Endif
				Endscan
			Else
				**
				** Impress�o por n� Documento
				TEXT TO lcSQL TEXTMERGE NOSHOW
					exec up_PESQDOCASSOCIADOS_pesquisarDocumentos '' , 0, '' ,'', 0, '' , '', 0, 0, 0, 1, '<<lcNrDocImp>>'
				ENDTEXT

				If !uf_gerais_actGrelha("","ucrsResumoFtEntidades2",lcSql)
					uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
					Return .F.
				Endif

				Select ucrsResumoFtEntidades2
				Go Top

				** Usado aqui para n�o alterar l�gica de c�digo (IDU utiliza o cursor ucrsResumoFtEntidades)
				Select * From ucrsResumoFtEntidades2 Order By nome_cl Into Cursor ucrsResumoFtEntidades Readwrite

				If Reccount("ucrsResumoFtEntidades") > 0 && Caso n�o tenha resultados n�o imprime

					lcValidaDadosImpressao = lcValidaDadosImpressao + 1

					**	Tipo: 1 - Resumo Facturas Entidades; 2 - Nota Lan�amento Associados; 3 - Aviso Lan�amento
					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_associados_dadosEntidades '<<uCrsResumoFtEntidades.nr_org>>', 1, '', '', 1, '<<lcNrDocImp>>', 0
					ENDTEXT

					If !uf_gerais_actGrelha("","uCrsDadosEntidadePrint",lcSql)
						uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
						Return .F.
					Endif

					Select ucrsDadosEntidadePrint
					Go Top

					Select ucrsResumoFtEntidades
					Go Top

					****************************
					If lctemduplicados == .F.
						lcnumduplicados = 0
					Endif
					For j=0 To lcnumduplicados
						myVersaoDoc = uf_gerais_versaodocumento(j+1)

						** IMPRIMIR
						If Like(lctipo,"print")
							Try
								Report Form Alltrim(lcreport) To Printer
							Catch

								uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
							Endtry
						Endif
					Endfor
				Endif
			Endif

			If lcValidaDadosImpressao = 0
				uf_perguntalt_chama("N�o existem dados para imprimir no periodo seleccionado. Por favor verifique. Obrigado.","OK","",64)
				Return .F.
			Endif

		Case Like(lctipo,"prev")
			**
			** Previs�o Por Per�odo Dados e N� Entidade
			If Empty(lcNrDocImp)

				** Prepara Cursores para impressao
				**	Tipo: 1 - Resumo Facturas Entidades; 2 - Nota Lan�amento Associados; 3 - Aviso Lan�amento

				**
				If lcDo != lcAo
					uf_perguntalt_chama("Apenas pode prever informa��o para uma Entidade. Por favor verfique.","OK","",16)
					Return .F.
				Endif

				If Used("ucrsResumoFtAssociados")
					fecha("ucrsResumoFtAssociados")
				Endif

				If Used("ucrsResumoFtAssociados2")
					fecha("ucrsResumoFtAssociados2")
				Endif

				TEXT TO lcSQL TEXTMERGE NOSHOW
					exec up_PESQDOCASSOCIADOS_pesquisarDocumentos '', <<lcdo>>, '', '', 0, '', '', <<YEAR(lcdatadados)>>, <<MONTH(lcdatadados)>>, 0, 1, '', 1
				ENDTEXT

				If !uf_gerais_actGrelha("","ucrsResumoFtEntidades2",lcSql)
					uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
					Return .F.
				Endif

				Select * From ucrsResumoFtEntidades2 Order By nome_cl Into Cursor ucrsResumoFtEntidades Readwrite

				If Reccount("ucrsResumoFtEntidades") > 0

					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_associados_dadosEntidades <<lcDo>>, 1, '<<lcdataimpressaotxt>>', '<<uf_gerais_getdate(lcdatadados,"SQL")>>', 1, '', 0
					ENDTEXT

					If !uf_gerais_actGrelha("","ucrsDadosEntidadePrint",lcSql)
						uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
						Return .F.
					Endif

					myVersaoDoc = uf_gerais_versaodocumento(1)

					Select ucrsDadosEntidadePrint
					Go Top
					Select ucrsResumoFtEntidades
					Go Top

					Set REPORTBEHAVIOR 90
					Try
						Report Form Alltrim(lcreport) To Printer Prompt Nodialog Preview
					Catch

						uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
					Endtry
					Set REPORTBEHAVIOR 80
				Else
					uf_perguntalt_chama("N�o existem dados para imprimir no periodo seleccionado. Por favor verifique. Obrigado.","OK","",64)
					Return .F.
				Endif
			Else
				**
				** Impress�o por n� Documento
				TEXT TO lcSQL TEXTMERGE NOSHOW
					exec up_PESQDOCASSOCIADOS_pesquisarDocumentos '' , 0, '' ,'', 0, '' , '', 0, 0, 0, 1, '<<lcNrDocImp>>'
				ENDTEXT

				If !uf_gerais_actGrelha("","ucrsResumoFtEntidades2",lcSql)
					uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
					Return .F.
				Endif

				Select ucrsResumoFtEntidades2
				Go Top

				** Usado aqui para n�o alterar l�gica de c�digo (IDU utiliza o cursor ucrsResumoFtEntidades)
				Select * From ucrsResumoFtEntidades2 Order By nome_cl Into Cursor ucrsResumoFtEntidades Readwrite

				If Reccount("ucrsResumoFtEntidades") > 0 && Caso n�o tenha resultados n�o imprime

					lcValidaDadosImpressao = lcValidaDadosImpressao + 1

					**	Tipo: 1 - Resumo Facturas Entidades; 2 - Nota Lan�amento Associados; 3 - Aviso Lan�amento
					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_associados_dadosEntidades '<<uCrsResumoFtEntidades.nr_org>>', 1, '', '', 1, '<<lcNrDocImp>>', 0
					ENDTEXT

					If !uf_gerais_actGrelha("","uCrsDadosEntidadePrint",lcSql)
						uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
						Return .F.
					Endif

					myVersaoDoc = uf_gerais_versaodocumento(1)

					Select ucrsDadosEntidadePrint
					Go Top

					Select ucrsResumoFtEntidades
					Go Top

					Set REPORTBEHAVIOR 90
					Try
						Report Form Alltrim(lcreport) To Printer Prompt Nodialog Preview
					Catch
						uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
					Endtry
					Set REPORTBEHAVIOR 80
				Else
					uf_perguntalt_chama("N�o existem dados para imprimir no periodo seleccionado. Por favor verifique. Obrigado.","OK","",64)
					Return .F.
				Endif
			Endif

	Endcase

	** por impressora no default **
	Set Device To Screen
	Set Printer To Name ''+lcDefaultPrinter+''
	Set Console On
	**
Endfunc


** Impress�o Nota de Lan�amento Associados (Resumo Faturas Associados) - Modulo Associados - Painel Gest�o Associados
Function uf_imprimirGerais_imprimir_ResumoFtAssociados
	Lparameters lctipo
	Local lcValidaDadosImpressao

	Store 0 To lcValidaDadosImpressao

	** Guarda a impressora por defeito do Windows***
	lcDefaultPrinter=Set("printer",2)
	*****************************************

	lcimpressao = Alltrim(IMPRIMIRGERAIS.pageframe1.page1.impressao.Value) &&Alltrim(ucrsImpIdusDefault.nomeimpressao)

	** Obtem Nome do Ficheiro Tabela de Impressoes configura impressao**
	lcSql = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select Top 1 nomeficheiro, tabela, temduplicados, numduplicados	From b_impressoes (nolock)	Where nomeimpressao = '<<lcimpressao>>'
	ENDTEXT

	If !uf_gerais_actGrelha("","templcSQL",lcSql)
		uf_perguntalt_chama("N�o foi poss�vel definir a configura��o de impress�es. Por favor contacte o Suporte.","OK","",16)
		Return
	Endif

	If Reccount("templcSQL")>0
		lcnomeFicheiro 	= Alltrim(templcSQL.nomeficheiro)
		lcprinter 		= IMPRIMIRGERAIS.pageframe1.page1.destino.Value
		lctabela		= templcSQL.tabela
		lctemduplicados = templcSQL.temduplicados
		lcnumduplicados = templcSQL.numduplicados
	Else
		Return .F.
	Endif

	If !(uf_gerais_actGrelha("","uCrsImpressao",lcSql))
	Endif

	****************************************
	** PREPARAR CURSORES PARA IMPRESS�ES **
	****************************************

	** Obtem dados Gerais da Empresa / Loja **
	If !Used("uCrsE1")
		uf_perguntalt_chama("N�o foi poss�vel verificar os dados da Empresa. Por favor contacte o suporte.","OK","",16)
		Return .F.
	Endif
	********************************

	** Guardar report a imprimir - ficheiro **
	lcreport = Alltrim(myPath)+"\analises\" + Alltrim(lcnomeFicheiro)


	** preparar impressora **
	If !(Alltrim(lcprinter) == "Impressora por defeito do Windows")
		Local lcValErroPrinter
		Try
			Set Printer To Name ''+Alltrim(lcprinter)+''
		Catch
			uf_perguntalt_chama("Foi detectada uma dificuldade com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
			lcValErroPrinter = .T.
		Endtry
		If lcValErroPrinter == .T.
			Return .F.
		Endif
	Endif

	Set Device To Print
	Set Console Off
	***********************************

	uf_gerais_TextoRodape("CL_FATC_ORG")

	** Valores para definir periodo de Impress�o e periodo de pesquisa de Dados para impress�o
	lcdataimpressaotxt   = IMPRIMIRGERAIS.pageframe1.page1.Data.Value
	lcdataimpressao      = Ctot(IMPRIMIRGERAIS.pageframe1.page1.Data.Value)
	lcdatadados			 = Ctot(IMPRIMIRGERAIS.pageframe1.page1.datadados.Value)

	lcDo = Iif(Empty(IMPRIMIRGERAIS.pageframe1.page1.txtDO.Value),0,Val(astr(IMPRIMIRGERAIS.pageframe1.page1.txtDO.Value)))
	lcAo = Iif(Empty(IMPRIMIRGERAIS.pageframe1.page1.txtAO.Value),0,Val(astr(IMPRIMIRGERAIS.pageframe1.page1.txtAO.Value)))

	**
	If Empty(lcDo) Or Empty(lcAo)
		uf_perguntalt_chama("Intervalo de Clientes n�o configurado.","OK","",16)
		Return .F.
	Endif

	** Cursor com Clientes Ordenados por odem alfab�tica para permitir criar impressoes por ordem alfabetica
	TEXT TO lcSQL NOSHOW TEXTMERGE
		SELECT * FROM b_utentes WHERE no between <<lcdo>> and <<lcAo>>  and no > '200' order BY nome
	ENDTEXT
	If !(uf_gerais_actGrelha("","uCrsClientesOrdenados",lcSql))
		&& Numero nao existe
		Return .F.
	Endif

	** Tratamento Impress�o e previsualiza��o
	Do Case
		Case Like(lctipo,"print")
			If IMPRIMIRGERAIS.pageframe1.page1.chksend.Value = 0
				** IMPRIMIR OS DOCUMENTOS
				I=0
				Select uCrsClientesOrdenados
				Go Top
				Scan
					I = I+1

					lcNoCliente = uCrsClientesOrdenados.no
					uf_imprimirGerais_imprimir_ResumoFtAssociadosPreparaCursores(I,lcdatadados,	lcdataimpressao,lcdataimpressaotxt, lcNoCliente)

					Select ucrsResumoFtAssociadosDadosFarmacia
					Go Top
					Select ucrsResumoFtAssociados
					Go Top
					*********************

					Select ucrsResumoFtAssociados
					If Reccount("ucrsResumoFtAssociados") > 0  && Caso n�o tenha resultados n�o imprime

						lcValidaDadosImpressao = lcValidaDadosImpressao + 1

						**	Tipo: 1 - Resumo Facturas Entidades; 2 - Nota Lan�amento Associados; 3 - Aviso Lan�amento Entidades
						TEXT TO lcSQL TEXTMERGE NOSHOW
							exec up_associados_dadosEntidades  <<lcNoCliente>>, 2, '<<lcdataimpressaotxt>>', '<<uf_gerais_getdate(lcdatadados,"SQL")>>', 1, '', 0
						ENDTEXT
						If !uf_gerais_actGrelha("","ucrsDadosEntidadePrint",lcSql)
							uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
							Return .F.
						Endif


						*********************
						If lctemduplicados == .F.
							lcnumduplicados = 0
						Endif
						For j=0 To lcnumduplicados
							myVersaoDoc = uf_gerais_versaodocumento(j+1)
							Select ucrsResumoFtAssociadosTotais
							Go Top
							Select ucrsResumoFtAssociadosDadosFarmacia
							Go Top
							Select ucrsDadosEntidadePrint
							Go Top
							Select ucrsResumoFtAssociados
							Go Top
							** IMPRIMIR
							If Like(lctipo,"print")
								Try
									Report Form Alltrim(lcreport) To Printer
								Catch
									uf_perguntalt_chama("Foi detectada uma dificuldade com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
								Endtry
							Endif
						Endfor
					Endif
				Endscan

			Else

				** ENVIAR OS DOCUMENTOS PARA A AFP
				I=0
				Select uCrsClientesOrdenados
				Go Top
				Scan
					I = I+1

					lcNoCliente = uCrsClientesOrdenados.no
					uf_imprimirGerais_imprimir_ResumoFtAssociadosPreparaCursores(I,lcdatadados,	lcdataimpressao,lcdataimpressaotxt, lcNoCliente)

					Select ucrsResumoFtAssociadosDadosFarmacia
					Go Top
					Select ucrsResumoFtAssociados
					Go Top
					*********************
					lcCont2 = 1
					regua(0,Reccount("ucrsResumoFtAssociados"),"A Exportar Documentos...")
					Select ucrsResumoFtAssociados
					If Reccount("ucrsResumoFtAssociados") > 0  && Caso n�o tenha resultados n�o imprime
						regua(1,lcCont2,"A Exportar Documento: " + astr(lcCont2))
						lcValidaDadosImpressao = lcValidaDadosImpressao + 1

						**	Tipo: 1 - Resumo Facturas Entidades; 2 - Nota Lan�amento Associados; 3 - Aviso Lan�amento Entidades
						TEXT TO lcSQL TEXTMERGE NOSHOW
							exec up_associados_dadosEntidades  <<lcNoCliente>>, 2, '<<lcdataimpressaotxt>>', '<<uf_gerais_getdate(lcdatadados,"SQL")>>', 1, '', 0
						ENDTEXT
						If !uf_gerais_actGrelha("","ucrsDadosEntidadePrint",lcSql)
							uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
							Return .F.
						Endif


						*********************
						If lctemduplicados == .F.
							lcnumduplicados = 0
						Endif
						For j=0 To lcnumduplicados
							myVersaoDoc = uf_gerais_versaodocumento(j+1)
							Select ucrsResumoFtAssociadosTotais
							Go Top
							Select ucrsResumoFtAssociadosDadosFarmacia
							Go Top
							Select ucrsDadosEntidadePrint
							Go Top
							Select ucrsResumoFtAssociados
							Go Top

							lcNomeFicheiroPDF= Left(Ttoc(ucrsDadosEntidadePrint.Data),8) + "_NOTADEBITO_" + Alltrim(Str(ucrsDadosEntidadePrint.nrdoc)) + ".PDF"
							uf_guarda_pdf_seldir(Alltrim(lcreport), Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ',''), "FtAssociados" , Alltrim(uf_gerais_getParameter('ADM0000000015','TEXT')) + "\Cache\PDF\")

							regua(1,lcCont2,"A enviar documento para FTP: " + astr(lcCont2))
							*uf_send_file_ftp("ftp.afp.com.pt", "afpcompt", "p0f181lOjL", "public_html/assets/media/docs/original/", ALLTRIM(uf_gerais_getParameter('ADM0000000015','TEXT')) + "\Cache\PDF\"+STRTRAN(STRTRAN(ALLTRIM(lcNomeFicheiroPDF),'/',''),' ',''), STRTRAN(STRTRAN(ALLTRIM(lcNomeFicheiroPDF),'/',''),' ',''))
							lcSql = ""
							TEXT TO lcsql TEXTMERGE NOSHOW
									exec up_pathFtp '','<<ucrsDadosEntidadePrint.ano_dados>>' ,'<<ucrsDadosEntidadePrint.NRDOC>>','<<ucrsDadosEntidadePrint.NDOC>>'
							ENDTEXT
							If  .Not. uf_gerais_actGrelha("", "ucrsPath", lcSql)
								uf_perguntalt_chama("N�O FOI POSS�VEL GRAVAR O DOCUMENTO. POR FAVOR CONTACTE O SUPORTE", "OK", "", 16)
								Return .F.
							Endif

							uf_send_file_ftp("ftp.ua878528.serversignin.com", "logitools_cloud@ua878528.serversignin.com", "^bB2e$AHQt-J", Alltrim(ucrsPath.Path), myFilePathSeq+Strtran(Strtran(Alltrim(myFileNameSeq), '/', ''), ' ', '')+".PDF", Alltrim(ucrsPath.Nome)+".PDF")
							fecha("ucrsPath")
							regua(1, lcCont2, "A registar o documento na BD: "+astr(lcCont2))


							regua(1,lcCont2,"A registar o documento na BD: " + astr(lcCont2))
							&& registar ficheiro na BD MySQL
							Public Server,port,db,uid,pw, stringsend
							Server="cps6.webserver.pt"
							port="3306" && port mysql is open on
							db="afpcompt_mad2017" && db name
							uid="afpcompt_mad" && user anme
							pw="9Dq6Cm6Jf7Kl" && password

							mysql = Sqlstringconnect('Driver={MySQL ODBC 5.3 Unicode Driver};Server=' + Server + ';Port=' + port + ';Database=' + db + ';uid=' + uid + ';Pwd=' + pw + ';',.T.)
							If Vartype(mysql) == 'L' Or m.mysql < 0
								Messagebox('MySQL Connection Failed. Logging will be disabled for this session.',16,'MySQL Error',3500)
							Endif

							TEXT TO lcSQL TEXTMERGE NOSHOW
								call  afpcompt_mad2017.importInvoices(<<ALLTRIM(STR(ucrsDadosEntidadePrint.no))>>,'Nota de D�bito nr <<ALLTRIM(STR(ucrsDadosEntidadePrint.nrdoc))>> de <<ALLTRIM(STR(MONTH(ucrsDadosEntidadePrint.data))))>>-<<ALLTRIM(STR(YEAR(ucrsDadosEntidadePrint.data)))>>','<<STRTRAN(STRTRAN(ALLTRIM(lcNomeFicheiroPDF),'/',''),' ','')>>',@out);
							ENDTEXT
							If Vartype(mysql) != 'L' And m.mysql >= 0
								SQLExec(mysql ,lcSql, "")
							Endif

							TEXT TO lcSQL TEXTMERGE NOSHOW
								select * from afpcompt_mad2017.afp_docs where name='Nota de D�bito nr <<ALLTRIM(STR(ucrsDadosEntidadePrint.nrdoc))>> de <<ALLTRIM(STR(MONTH(ucrsDadosEntidadePrint.data))))>>-<<ALLTRIM(STR(YEAR(ucrsDadosEntidadePrint.data)))>>'
							ENDTEXT

							If Vartype(mysql) != 'L' And m.mysql >= 0
								SQLExec(mysql ,lcSql, "UCRSRETURNVAL")
							Endif
							If !Used("UCRSRETURNVAL")
								uf_perguntalt_chama("O SEGUINTE FICHEIRO N�O FICOU REGISTADO NA BD MYSQL - " + Strtran(Strtran(Alltrim(lcNomeFicheiroPDF),'/',''),' ','') ,"OK","",48)
							Endif
							fecha("UCRSRETURNVAL")

							**sqldisconnect(mysql)

						Endfor
						lcCont2 = lcCont2 + 1
					Endif
				Endscan


			Endif

			If lcValidaDadosImpressao = 0
				uf_perguntalt_chama("N�o existem dados para imprimir no periodo seleccionado. Por favor verifique. Obrigado.","OK","",64)
				Return .F.
			Endif

		Case Like(lctipo,"prev")

			If lcDo != lcAo
				uf_perguntalt_chama("Apenas pode prever informa��o para um Cliente. Por favor verfique.","OK","",16)
				Return .F.
			Endif

			** Prepara Cursores para impressao
			uf_imprimirGerais_imprimir_ResumoFtAssociadosPreparaCursores(lcDo, lcdatadados, lcdataimpressao, lcdataimpressaotxt, lcDo)

			Select ucrsResumoFtAssociadosDadosFarmacia
			Go Top
			Select ucrsResumoFtAssociados
			Go Top
			*********************

			Select ucrsResumoFtAssociados
			If Reccount("ucrsResumoFtAssociados") > 0

				TEXT TO lcSQL TEXTMERGE NOSHOW
					exec up_associados_dadosEntidades  <<lcdo>>, 2, '<<lcdataimpressaotxt>>', '<<uf_gerais_getdate(lcdatadados,"SQL")>>', 1, '', 0
				ENDTEXT

				If !uf_gerais_actGrelha("","ucrsDadosEntidadePrint",lcSql)
					uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
					Return .F.
				Endif

			Else
				uf_perguntalt_chama("N�o existem dados para imprimir no periodo seleccionado. Por favor verifique. Obrigado.","OK","",64)
				Return .F.
			Endif

			myVersaoDoc = uf_gerais_versaodocumento(1)

			Select ucrsResumoFtAssociadosTotais
			Go Top
			Select ucrsResumoFtAssociadosDadosFarmacia
			Go Top
			Select ucrsDadosEntidadePrint
			Go Top
			Select ucrsResumoFtAssociados
			Go Top


			Set REPORTBEHAVIOR 90
			Try
				Report Form Alltrim(lcreport) To Printer Prompt Nodialog Preview
			Catch
				uf_perguntalt_chama("Foi detectada uma anomalia com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
			Endtry
			Set REPORTBEHAVIOR 80
	Endcase

	** por impressora no default **
	Set Device To Screen
	Set Printer To Name ''+lcDefaultPrinter+''
	Set Console On
	**************************
Endfunc


** Impress�o Aviso de Lan�amento Entidades (associados_aviso_lancamento_entidades [cliente refere-se a isto como Nota de Cr�dito a Entidades]) - Modulo Associados
Function uf_imprimirGerais_imprimir_ResumoRtAssociados
	Lparameters lctipo
	Local lcValidaDadosImpressao
	Store 0 To lcValidaDadosImpressao

	** Guarda a impressora por defeito do Windows***
	lcDefaultPrinter=Set("printer",2)
	*****************************************

	lcimpressao = Alltrim(IMPRIMIRGERAIS.pageframe1.page1.impressao.Value) &&Alltrim(ucrsImpIdusDefault.nomeimpressao)

	** Obtem Nome do Ficheiro Tabela de Impressoes **
	lcSql = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select Top 1 nomeficheiro, tabela, temduplicados, numduplicados
		From b_impressoes (nolock)
		Where nomeimpressao = '<<lcimpressao>>'
	ENDTEXT

	If !uf_gerais_actGrelha("","templcSQL",lcSql)
		uf_perguntalt_chama("N�o foi poss�vel definir a configura��o de impress�es. Por favor contacte o Suporte.","OK","",16)

		Return
	Endif

	If Reccount("templcSQL")>0
		lcnomeFicheiro 	= Alltrim(templcSQL.nomeficheiro)
		lcprinter 		= IMPRIMIRGERAIS.pageframe1.page1.destino.Value
		lctabela		= templcSQL.tabela
		lctemduplicados = templcSQL.temduplicados
		lcnumduplicados = templcSQL.numduplicados
	Else
		Return .F.
	Endif

	If !(uf_gerais_actGrelha("","uCrsImpressao",lcSql))
		&&erro
	Endif

	****************************************
	** PREPARAR CURSORES PARA IMPRESS�ES **
	****************************************

	** Obtem dados Gerais da Emprea / Loja **
	If !Used("uCrsE1")
		uf_perguntalt_chama("N�o foi poss�vel verificar os dados da Empresa. Por favor contacte o suporte.","OK","",16)
		Return .F.
	Endif
	********************************

	** Guardar report a imprimir - ficheiro **
	lcreport = Alltrim(myPath)+"\analises\" + Alltrim(lcnomeFicheiro)

	** preparar impressora **
	If !(Alltrim(lcprinter) == "Impressora por defeito do Windows")
		Local lcValErroPrinter
		Try
			Set Printer To Name ''+Alltrim(lcprinter)+''
		Catch

			uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
			lcValErroPrinter = .T.
		Endtry
		If lcValErroPrinter == .T.
			Return .F.
		Endif
	Endif

	Set Device To Print
	Set Console Off
	***********************************

	**
	uf_gerais_TextoRodape("CL_FATC_ORG")

	** Vari�veis para definir datas de Impress�o
	Local lcdataimpressaotxt, lcdatadados, lcDataTratamento, lcDo, lcAo
	Store '' To lcdataimpressaotxt, lcdataimpressaotxt
	Store .F. To lcDataTratamento

	lcdataimpressaotxt = IMPRIMIRGERAIS.pageframe1.page1.Data.Value
	lcdatadados		   = Ctot(IMPRIMIRGERAIS.pageframe1.page1.datadados.Value)
	lcDataTratamento   = IMPRIMIRGERAIS.pageframe1.page1.chkTratamento.Value

	lcDo = Iif(Empty(IMPRIMIRGERAIS.pageframe1.page1.txtDO.Value),0,Val(astr(IMPRIMIRGERAIS.pageframe1.page1.txtDO.Value)))
	lcAo = Iif(Empty(IMPRIMIRGERAIS.pageframe1.page1.txtAO.Value),0,Val(astr(IMPRIMIRGERAIS.pageframe1.page1.txtAO.Value)))

	lcNrDocImp = IMPRIMIRGERAIS.pageframe1.page1.nrDocImp.Value

	** Calculo de Dados e Impress�o
	If Empty(lcDo) And Empty(lcNrDocImp) Or Empty(lcAo) And Empty(lcNrDocImp)
		uf_perguntalt_chama("Intervalo de Entidades n�o configurado.","OK","",16)
		Return .F.
	Endif

	Do Case
			**
		Case Like(lctipo,"print")
			**
			** Impress�o de Documentos Por data dados ou numero cliente
			If Empty(lcNrDocImp)
				For I = lcDo To lcAo

					** Prepara Cursores para impressao
					If Used("ucrsResumoRtEntidades")
						fecha("ucrsResumoRtEntidades")
					Endif

					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_resumo_retificado_associados <<YEAR(lcDataDados)>>, <<MONTH(lcDataDados)>>, <<i>>, <<lcDataTratamento>>, ''
					ENDTEXT

					If !uf_gerais_actGrelha("","ucrsResumoRtEntidades",lcSql)
						uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
						Return .F.
					Endif

					If uCrsResumoRtEntidades.Total != 0 && Caso n�o tenha valores n�o imprime

						lcValidaDadosImpressao = lcValidaDadosImpressao + 1

						** Prepara Cursores para impressao
						**	Tipo: 1 - Resumo Facturas Entidades; 2 - Nota Lan�amento Associados; 3 - Aviso Lan�amento Entidades
						TEXT TO lcSQL TEXTMERGE NOSHOW
							exec up_associados_dadosEntidades  <<i>>, 3, '<<lcDataImpressaoTxt>>', '<<uf_gerais_getdate(lcDataDados,"SQL")>>' , 1, '', 0
						ENDTEXT
						If !uf_gerais_actGrelha("","ucrsDadosEntidadePrint",lcSql)
							uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
							Return .F.
						Endif

						Select ucrsDadosEntidadePrint
						Go Top
						Select uCrsResumoRtEntidades
						Go Top

						*********************
						If lctemduplicados == .F.
							lcnumduplicados = 0
						Endif
						For j = 0 To lcnumduplicados
							myVersaoDoc = uf_gerais_versaodocumento(j+1)

							** IMPRIMIR
							If Like(lctipo,"print")
								Try
									Report Form Alltrim(lcreport) To Printer
								Catch
									uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
								Endtry
							Endif
						Endfor
					Endif

				Endfor
			Else
				**
				** Impress�o por N� Documento

				** Prepara Cursores para impressao
				**	Tipo: 1 - Resumo Facturas Entidades; 2 - Nota Lan�amento Associados; 3 - Aviso Lan�amento Entidades
				TEXT TO lcSQL TEXTMERGE NOSHOW
					exec up_associados_dadosEntidades 0 , 3, '', '', 1, '<<lcNrDocImp>>', 0
				ENDTEXT
				If !uf_gerais_actGrelha("","uCrsDadosEntidadePrint",lcSql)
					uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
					Return .F.
				Endif

				TEXT TO lcSQL TEXTMERGE NOSHOW
					exec up_resumo_retificado_associados '<<uCrsDadosEntidadePrint.ano_dados>>', '<<uCrsDadosEntidadePrint.mes_dados)>>', '<<uCrsDadosEntidadePrint.no>>', <<lcDataTratamento>>, '<<lcNrDocImp>>'
				ENDTEXT

				If !uf_gerais_actGrelha("","ucrsResumoRtEntidades",lcSql)
					uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
					Return .F.
				Endif

				If uCrsResumoRtEntidades.Total != 0

					lcValidaDadosImpressao = lcValidaDadosImpressao + 1

					Select ucrsDadosEntidadePrint
					Go Top
					Select uCrsResumoRtEntidades
					Go Top

					*********************
					If lctemduplicados == .F.
						lcnumduplicados = 0
					Endif
					For j = 0 To lcnumduplicados
						myVersaoDoc = uf_gerais_versaodocumento(j+1)

						** IMPRIMIR
						If Like(lctipo,"print")
							Try
								Report Form Alltrim(lcreport) To Printer
							Catch
								uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
							Endtry
						Endif
					Endfor
				Else
					uf_perguntalt_chama("N�o existem dados para imprimir. Por favor verifique. Obrigado.","OK","",64)
				Endif

			Endif

			If lcValidaDadosImpressao = 0
				uf_perguntalt_chama("N�o existem dados para imprimir no periodo seleccionado. Por favor verifique. Obrigado.","OK","",64)
				Return .F.
			Endif

			** Previs�o de Documentos
		Case Like(lctipo,"prev")
			**
			** Impress�o baseado nas datas e numero de entidade
			If Empty(lcNrDocImp)
				If lcDo != lcAo
					uf_perguntalt_chama("Apenas pode prever informa��o para uma Entidade. Por favor verfique.","OK","",16)
					Return .F.
				Endif

				** Prepara Cursores para impressao
				If Used("ucrsResumoRtEntidades")
					fecha("ucrsResumoRtEntidades")
				Endif

				TEXT TO lcSQL TEXTMERGE NOSHOW
					exec up_resumo_retificado_associados <<YEAR(lcDataDados)>> ,<<MONTH(lcDataDados)>>, <<lcdo>> , <<lcDataTratamento>>, ''
				ENDTEXT

				If !uf_gerais_actGrelha("","ucrsResumoRtEntidades",lcSql)
					uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
					Return .F.
				Endif

				Select uCrsResumoRtEntidades
				Go Top

				If uCrsResumoRtEntidades.Total != 0 && Caso n�o tenha valores n�o imprime

					lcValidaDadosImpressao = lcValidaDadosImpressao + 1

					**	Tipo: 1 - Resumo Facturas Entidades; 2 - Nota Lan�amento Associados; 3 - Aviso Lan�amento
					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_associados_dadosEntidades  <<lcDo>> , 3, '<<lcDataImpressaoTxt>>' , '<<uf_gerais_getdate(lcDataDados,"SQL")>>', 1, '', 0
					ENDTEXT

					If !uf_gerais_actGrelha("","ucrsDadosEntidadePrint",lcSql)
						uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
						Return .F.
					Endif

					Select ucrsDadosEntidadePrint
					Go Top
					Select uCrsResumoRtEntidades
					Go Top

					myVersaoDoc = uf_gerais_versaodocumento(1)

					Set REPORTBEHAVIOR 90
					Try
						Report Form Alltrim(lcreport) To Printer Prompt Nodialog Preview
					Catch
						uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
					Endtry
					Set REPORTBEHAVIOR 80
				Else
					uf_perguntalt_chama("N�o existem dados para imprimir no periodo seleccionado. Por favor verifique. Obrigado.","OK","",16)
					Return .F.
				Endif
			Else
				**
				** Impress�o baseada no numero do documento
				**	Tipo: 1 - Resumo Facturas Entidades; 2 - Nota Lan�amento Associados; 3 - Aviso Lan�amento Entidades
				TEXT TO lcSQL TEXTMERGE NOSHOW
					exec up_associados_dadosEntidades 0 , 3, '', '', 1, '<<lcNrDocImp>>', 0
				ENDTEXT
				If !uf_gerais_actGrelha("","uCrsDadosEntidadePrint",lcSql)
					uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
					Return .F.
				Endif

				TEXT TO lcSQL TEXTMERGE NOSHOW
					exec up_resumo_retificado_associados '<<uCrsDadosEntidadePrint.ano_dados>>', '<<uCrsDadosEntidadePrint.mes_dados)>>', '<<uCrsDadosEntidadePrint.no>>', <<lcDataTratamento>>, '<<lcNrDocImp>>'
				ENDTEXT

				If !uf_gerais_actGrelha("","ucrsResumoRtEntidades",lcSql)
					uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
					Return .F.
				Endif

				If uCrsResumoRtEntidades.Total != 0

					lcValidaDadosImpressao = lcValidaDadosImpressao + 1

					Select ucrsDadosEntidadePrint
					Go Top
					Select uCrsResumoRtEntidades
					Go Top

					myVersaoDoc = uf_gerais_versaodocumento(1)

					Set REPORTBEHAVIOR 90
					Try
						Report Form Alltrim(lcreport) To Printer Prompt Nodialog Preview
					Catch
						uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
					Endtry
					Set REPORTBEHAVIOR 80
				Else
					uf_perguntalt_chama("N�o existem dados para imprimir. Por favor verifique. Obrigado.","OK","",16)
					Return .F.
				Endif

			Endif

			If lcValidaDadosImpressao = 0
				uf_perguntalt_chama("N�o existem dados para imprimir. Por favor verifique. Obrigado.","OK","",64)
				Return .F.
			Endif

	Endcase

	** por impressora no default **
	Set Device To Screen
	Set Printer To Name ''+lcDefaultPrinter+''
	Set Console On
	**************************
Endfunc


** Funcao para impress�o de recibos - Esta funcionalidade tem de ser revista - este c�digo foi feito para uma implementa��o r�pida na AFP
Function uf_imprimirGerais_imprimir_ReciboEntAssociados
	Lparameters lctipo, lcReImp
	Local lcIdMovimento
	Store 0 To lcIdMovimento

	&& significa que impress�o est� a acontecer ap�s a grava��o de um movimento
	If Used("uCrsAuxMovimento")
		lcIdMovimento = uCrsAuxMovimento.Id
	Endif

	&& Obtem Nome do Ficheiro Tabela de Impressoes
	lcimpressao = Alltrim(IMPRIMIRGERAIS.pageframe1.page1.impressao.Value)

	lcSql = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select Top 1 nomeficheiro, tabela, temduplicados, numduplicados
		From b_impressoes (nolock)
		Where nomeimpressao = '<<lcimpressao>>'
	ENDTEXT

	If !uf_gerais_actGrelha("","templcSQL",lcSql)
		uf_perguntalt_chama("N�o foi poss�vel definir a configura��o de impress�es. Por favor contacte o Suporte.","OK","",16)
		Return .F.
	Endif

	If Reccount("templcSQL")>0
		lcnomeFicheiro 	= Alltrim(templcSQL.nomeficheiro)
		lcprinter 		= IMPRIMIRGERAIS.pageframe1.page1.destino.Value
		lctabela		= templcSQL.tabela
		lctemduplicados = templcSQL.temduplicados
		lcnumduplicados = templcSQL.numduplicados
	Else
		Return .F.
	Endif

	&& Guardar report a imprimir - ficheiro
	lcreport = Alltrim(myPath)+"\analises\" + Alltrim(lcnomeFicheiro)

	&& preparar impressora
	&& Guarda a impressora por defeito do Windows
	lcDefaultPrinter=Set("printer",2)

	If !(Alltrim(lcprinter) == "Impressora por defeito do Windows")
		Local lcValErroPrinter
		Try
			Set Printer To Name ''+Alltrim(lcprinter)+''
		Catch
			uf_perguntalt_chama("Foi detectado uma anomalia com a impressora." + Chr(13) + "Por favor contacte o Suporte.","OK","",64)
			lcValErroPrinter = .T.
		Endtry
		If lcValErroPrinter == .T.
			Return .F.
		Endif
	Endif

	Set Device To Print
	Set Console Off

	uf_gerais_TextoRodape("TESOURARIA_MOV")

	&& Trata informa��o para imprimir
	Local lcData, lcTotaltrf, lcDataRef, lcdatadados, lcDo, lcNrDoc
	Store 0 To lcDataRef, lcNrDoc

	&& Se for Re Impress�o
	If lcReImp
		lcdatadados		= Ctot(IMPRIMIRGERAIS.pageframe1.page1.datadados.Value)
		lcData 			= IMPRIMIRGERAIS.pageframe1.page1.Data.Value
		lcDo			= Iif(Empty(IMPRIMIRGERAIS.pageframe1.page1.txtDO.Value),0,Val(astr(IMPRIMIRGERAIS.pageframe1.page1.txtDO.Value)))
		lcNrDocImp		= IMPRIMIRGERAIS.pageframe1.page1.nrDocImp.Value
	Else
		**
		Select uCrsMovBancDocEntidades
		Go Top

		lcData 			= Alltrim(movbancarios.Data.Value)
		lcTotaltrf  	= (movbancarios.pageframe1.page2.soma_regularizar.Value)
		lcDataRef		= Date(uCrsMovBancDocEntidades.ano,uCrsMovBancDocEntidades.mes,10)
	Endif

	&& no caso de ser reimpress�o tem de colocar o n� do documento de impress�o
	If !Empty(lcReImp) And Empty(lcNrDocImp)
		uf_perguntalt_chama("Por favor coloque o n� do recibo que pretende reimprimir.","OK","",16)
		Return .F.
	Endif

	&&
	If Used('uCrsMovBancDocEntidades')
		Select uCrsMovBancDocEntidades
		Go Top
	Endif

	Do Case
		Case Like(lctipo,"print") && impress�o do movimento
			** Se for reimpress�o do movimento
			If lcReImp

				&& caso tenha nr de documento n�o precisa de datas
				If Empty(lcNrDocImp)
					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_associados_recibosEntidades <<YEAR(lcDataDados)>>, <<MONTH(lcDataDados)>>, <<	lcDo>>, <<lcNrDocImp>>, '', 0
					ENDTEXT
				Else
					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_associados_recibosEntidades 0, 0, <<lcDo>>, <<lcNrDocImp>>, '', 0
					ENDTEXT
				Endif

				If !uf_gerais_actGrelha("","uCrsReciboEntidade",lcSql)
					uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
					Return .F.
				Endif


				&& caso tenha nr de documento n�o precisa de datas
				If Empty(lcNrDocImp)
					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_associados_dadosEntidades <<lcDo>>, 4, '', '<<uf_gerais_getdate(lcDataDados, 	"SQL")>>', 1, '<<lcNrDocImp>>', <<lcIdMovimento>>
					ENDTEXT
				Else
					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_associados_dadosEntidades <<lcDo>>, 4, '', '', 1, '<<lcNrDocImp>>', <<lcIdMovimento>>
					ENDTEXT
					If !uf_gerais_actGrelha("","uCrsDadosEntidadePrint",lcSql)
						uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
						Return .F.
					Endif
				Endif

				Calculate Sum(uCrsReciboEntidade.total_reg) To lcTotaltrf

				&& valida dados para impress�o
				If Reccount("uCrsReciboEntidade") == 0 Or Reccount("uCrsDadosEntidadePrint") == 0
					uf_perguntalt_chama("N�o existem dados para impress�o. Por favor verifique.","OK","",16)
					Return .F.
				Endif
			Else
				** fluxo normal da primeira impress�o
				lcSql = ''
				TEXT TO lcSQL TEXTMERGE NOSHOW
					 exec up_associados_recibosEntidades '<<uCrsMovBancDocEntidades.ano>>','<<uCrsMovBancDocEntidades.mes>>','<<uCrsMovBancDocEntidades.nr_org>>', 0, '<<ALLTRIM(UcrsMovBANc.obs)>>', <<uCrsMovBANc.valor>>
				ENDTEXT
				If !uf_gerais_actGrelha("","uCrsReciboEntidade",lcSql)
					uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
					Return .F.
				Endif

				lcSql = ''
				TEXT TO lcSQL TEXTMERGE NOSHOW
					exec up_associados_dadosEntidades '<<uCrsMovBancDocEntidades.nr_org>>', 4, '<<lcData>>', '<<uf_gerais_getdate(lcDataRef,"SQL")>>' , 0, '', <<lcIdMovimento>>
				ENDTEXT
				If !uf_gerais_actGrelha("","uCrsDadosEntidadePrint",lcSql)
					uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
					Return .F.
				Endif

				&& valida dados para impress�o
				If Reccount("uCrsReciboEntidade") == 0 Or Reccount("uCrsDadosEntidadePrint") == 0
					uf_perguntalt_chama("N�o existem dados para impress�o. Por favor verifique.","OK","",16)
					Return .F.
				Endif
			Endif

			Select ucrsDadosEntidadePrint
			Go Top

			Select uCrsReciboEntidade
			Go Top
			**

			If lctemduplicados == .F.
				lcnumduplicados = 0
			Endif
			For j=0 To lcnumduplicados
				myVersaoDoc = uf_gerais_versaodocumento(j+1)

				** IMPRIMIR
				If Like(lctipo,"print")
					Try
						Report Form Alltrim(lcreport) To Printer
					Catch
						uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
					Endtry
				Endif
			Endfor


		Case Like(lctipo,"prev") && previsao
			** Se for reimpress�o do recibo
			If lcReImp
				&& caso tenha nr de documento n�o precisa de datas
				If Empty(lcNrDocImp)
					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_associados_recibosEntidades <<YEAR(lcDataDados)>>, <<MONTH(lcDataDados)>>, <<	lcDo>>, <<lcNrDocImp>>, '', 0
					ENDTEXT
				Else
					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_associados_recibosEntidades 0, 0, <<lcDo>>, <<lcNrDocImp>>, '', 0
					ENDTEXT
				Endif

				If !uf_gerais_actGrelha("","uCrsReciboEntidade",lcSql)
					uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
					Return .F.
				Endif


				&& caso tenha nr de documento n�o precisa de datas
				If Empty(lcNrDocImp)
					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_associados_dadosEntidades <<lcDo>>, 4, '', '<<uf_gerais_getdate(lcDataDados, 	"SQL")>>', 1, '<<lcNrDocImp>>', <<lcIdMovimento>>
					ENDTEXT
				Else
					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_associados_dadosEntidades <<lcDo>>, 4, '', '', 1, '<<lcNrDocImp>>', <<lcIdMovimento>>
					ENDTEXT
				Endif

				If !uf_gerais_actGrelha("","uCrsDadosEntidadePrint",lcSql)
					uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
					Return .F.
				Endif

				Calculate Sum(uCrsReciboEntidade.total_reg) To lcTotaltrf

				&& valida dados para impress�o
				If Reccount("uCrsReciboEntidade") == 0 Or Reccount("uCrsDadosEntidadePrint") == 0
					uf_perguntalt_chama("N�o existem dados para impress�o. Por favor verifique.","OK","",16)
					Return .F.
				Endif

			Else
				** fluxo normal da primeira previsao
				lcSql = ''
				TEXT TO lcSQL TEXTMERGE NOSHOW
					 exec up_associados_recibosEntidades '<<uCrsMovBancDocEntidades.ano>>','<<uCrsMovBancDocEntidades.mes>>','<<uCrsMovBancDocEntidades.nr_org>>', 0, '<<ALLTRIM(UcrsMovBANc.obs)>>', <<uCrsMovBANc.valor>>
				ENDTEXT
				If !uf_gerais_actGrelha("","uCrsReciboEntidade",lcSql)
					uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
					Return .F.
				Endif

				lcSql = ''
				TEXT TO lcSQL TEXTMERGE NOSHOW
					exec up_associados_dadosEntidades '<<uCrsMovBancDocEntidades.nr_org>>', 4, '<<lcData>>', '<<uf_gerais_getdate(lcDataRef, "SQL")>>', 0, '', <<lcIdMovimento>>
				ENDTEXT
				If !uf_gerais_actGrelha("","uCrsDadosEntidadePrint",lcSql)
					uf_perguntalt_chama("N�o foi poss�vel obter dados para impress�o. Por favor contacte o Suporte. Obrigado.","OK","",16)
					Return .F.
				Endif

				&& valida dados para impress�o
				If Reccount("uCrsReciboEntidade") == 0 Or Reccount("uCrsDadosEntidadePrint") == 0
					uf_perguntalt_chama("N�o existem dados para impress�o. Por favor verifique.","OK","",16)
					Return .F.
				Endif
			Endif

			Select ucrsDadosEntidadePrint
			Go Top

			Select uCrsReciboEntidade
			Go Top

			**
			myVersaoDoc = uf_gerais_versaodocumento(1)

			Set REPORTBEHAVIOR 90
			Try
				Report Form Alltrim(lcreport) To Printer Prompt Nodialog Preview
			Catch
				uf_perguntalt_chama("Foi detectado um problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
			Endtry
			Set REPORTBEHAVIOR 80
	Endcase

	** por impressora no default **
	Set Device To Screen
	Set Printer To Name ''+lcDefaultPrinter+''
	Set Console On
	**

Endfunc


** Funcao Para calculo da numera��o de documentos no m�dulo de Associados - Lu�s Leal 2015-06-09
Function uf_imprimirGerais_emitirDoc
	Local lcDo, lcAo, lcValidaDadosImpressao, lcdatadados, lcdataimpressaotxt
	Store 0 To lcDo, lcAo, lcValidaDadosImpressao, lcdatadados, lcdataimpressaotxt

	Do Case
		Case MYIMPRESSAOORIGEM == "RESUMOFTENTIDADES"

			** Atribui Entidades para as quais ser�o emitidos os "Documentos" e data para impress�o dados
			lcDo = Iif(Empty(IMPRIMIRGERAIS.pageframe1.page1.txtDO.Value),0,Val(astr(IMPRIMIRGERAIS.pageframe1.page1.txtDO.Value)))
			lcAo = Iif(Empty(IMPRIMIRGERAIS.pageframe1.page1.txtAO.Value),0,Val(astr(IMPRIMIRGERAIS.pageframe1.page1.txtAO.Value)))

			lcdatadados 		= Ctot(IMPRIMIRGERAIS.pageframe1.page1.datadados.Value)
			lcdataimpressaotxt  = IMPRIMIRGERAIS.pageframe1.page1.Data.Value

			If Empty(lcDo) Or Empty(lcAo)
				uf_perguntalt_chama("Intervalo de Entidades n�o est� definido. Por favor verfique.","OK","",16)
				Return .F.
			Endif

			** Criar Cursor para permitir criar "Documentos" por ordem alfab�tica das Entidades
			TEXT TO lcSQL NOSHOW TEXTMERGE
				SELECT * FROM b_utentes WHERE no between <<lcdo>> and <<lcAo>> and tipo = 'Entidade' order BY nome
			ENDTEXT

			If !(uf_gerais_actGrelha("","uCrsEntidadesOrdenadas",lcSql))
			Endif

			&&i = 0
			Select uCrsEntidadesOrdenadas
			Go Top
			Scan
				&&i = i + 1

				If Used("ucrsResumoFtAssociados")
					fecha("ucrsResumoFtAssociados")
				Endif

				If Used("ucrsResumoFtAssociados2")
					fecha("ucrsResumoFtAssociados2")
				Endif

				TEXT TO lcSQL TEXTMERGE NOSHOW
					exec up_PESQDOCASSOCIADOS_pesquisarDocumentos '', <<uCrsEntidadesOrdenadas.no>>, '', '' , 0, '', '', <<YEAR(lcdatadados)>>, <<MONTH(lcdatadados)>>, 0, 1, '', 1
				ENDTEXT

				If !uf_gerais_actGrelha("","ucrsResumoFtEntidades2",lcSql)
					uf_perguntalt_chama("N�o foi poss�vel obter dados para Emiss�o de Documentos. Por favor contacte o Suporte. Obrigado.","OK","",16)
					Return .F.
				Endif

				Select * From ucrsResumoFtEntidades2 Order By nome_cl Into Cursor ucrsResumoFtEntidades Readwrite

				If Reccount("ucrsResumoFtEntidades") > 0 && Caso n�o tenha resultados emite "Documentos"

					Local temDocEmitido, temNovoDoc
					Store 1 To temDocEmitido
					Store .F. To temNovoDoc

					** valida se existe alguma linha sem doc impress�o para n�o permitir criar documentos duplicados.
					Select ucrsResumoFtEntidades
					Go Top
					Locate For ucrsResumoFtEntidades.nrDocImp != 0 And ucrsResumoFtEntidades.valor_ret = 0
					If Not Found()
						temDocEmitido = 0
					Endif

					Select ucrsResumoFtEntidades
					Go Top
					Locate For ucrsResumoFtEntidades.nrDocImp == 0 And ucrsResumoFtEntidades.valor_ret = 0
					If Found()
						temNovoDoc = .T.
					Endif

					lcValidaDadosImpressao = lcValidaDadosImpressao + 1

					**	Tipo: 1 - Resumo Facturas Entidades; 2 - Nota Lan�amento Associados; 3 - Aviso Lan�amento
					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_associados_dadosEntidades <<uCrsEntidadesOrdenadas.no>>, 1, '<<lcdataimpressaotxt>>', '<<uf_gerais_getdate(lcdatadados,"SQL")>>', <<temDocEmitido>>, '', 0
					ENDTEXT

					If !uf_gerais_actGrelha("","ucrsDadosEntidadePrint",lcSql)
						uf_perguntalt_chama("N�o foi poss�vel obter dados para Emiss�o de Documentos. Por favor contacte o Suporte. Obrigado.","OK","",16)
						Return .F.
					Endif

					If Used("ucrsDadosEntidadePrint")
						Select ucrsDadosEntidadePrint
						Go Top
					Endif

					** Colocar n� de documento na tabela de registo de faturas
					Select ucrsResumoFtEntidades
					Go Top
					Scan
						TEXT TO lcSQL TEXTMERGE NOSHOW
							UPDATE cl_fatc_org SET nrDocImp = <<ucrsDadosEntidadePrint.nrDoc>> WHERE id = '<<ucrsResumoFtEntidades.id>>' AND nrDocImp = 0
						ENDTEXT
						If !uf_gerais_actGrelha("","",lcSql)
							uf_perguntalt_chama("Ocorreu uma anomalia emitir a numera��o dos documentos. Por favor contacte o Suporte.","OK","",16)
						Endif
					Endscan

					Select ucrsResumoFtEntidades
					Go Top
				Endif
			Endscan

			If lcValidaDadosImpressao = 0
				uf_perguntalt_chama("N�o existem dados para Emiss�o de Documentos no periodo seleccionado. Por favor verifique. Obrigado.","OK","",64)
				Return .F.
			Else
				If temDocEmitido = 1
					If temNovoDoc
						uf_perguntalt_chama("Documentos j� foram emitidos anteriormente. Vai ser efetuada uma rectifica��o para novo(s) documento(s).","OK","",64)
					Else
						uf_perguntalt_chama("Documentos j� foram emitidos anteriormente.","OK","",64)
					Endif
				Else
					uf_perguntalt_chama("Documentos emitidos com sucesso para as entidades seleccionadas. Obrigado.","OK","",64)
				Endif
			Endif

		Case MYIMPRESSAOORIGEM == "RESUMOFTASSOCIADOS"

			** Valores para definir periodo de Impress�o e periodo de pesquisa de Dados para impress�o
			lcdataimpressaotxt   = IMPRIMIRGERAIS.pageframe1.page1.Data.Value
			lcdataimpressao      = Ctot(IMPRIMIRGERAIS.pageframe1.page1.Data.Value)
			lcdatadados			 = Ctot(IMPRIMIRGERAIS.pageframe1.page1.datadados.Value)

			lcDo = Iif(Empty(IMPRIMIRGERAIS.pageframe1.page1.txtDO.Value),0,Val(astr(IMPRIMIRGERAIS.pageframe1.page1.txtDO.Value)))
			lcAo = Iif(Empty(IMPRIMIRGERAIS.pageframe1.page1.txtAO.Value),0,Val(astr(IMPRIMIRGERAIS.pageframe1.page1.txtAO.Value)))

			**
			If Empty(lcDo) Or Empty(lcAo)
				uf_perguntalt_chama("Intervalo de Clientes n�o configurado.","OK","",16)
				Return .F.
			Endif

			** Cursor com Clientes Ordenados por odem alfab�tica para permitir criar impressoes por ordem alfabetica
			TEXT TO lcSQL NOSHOW TEXTMERGE
				SELECT * FROM b_utentes WHERE no between <<lcdo>> and <<lcAo>> and tipo != 'Entidade' order BY nome
			ENDTEXT
			If !(uf_gerais_actGrelha("","uCrsClientesOrdenados",lcSql))
				&& Numero nao existe
				Return .F.
			Endif

			** Emiss�o
			I=0
			Select uCrsClientesOrdenados
			Go Top
			Scan
				I = I+1

				lcNoCliente = uCrsClientesOrdenados.no

				uf_imprimirGerais_imprimir_ResumoFtAssociadosPreparaCursores(I, lcdatadados, lcdataimpressao, lcdataimpressaotxt, lcNoCliente)

				Select ucrsResumoFtAssociadosDadosFarmacia
				Go Top
				Select ucrsResumoFtAssociados
				Go Top
				*********************

				Select ucrsResumoFtAssociados
				If Reccount("ucrsResumoFtAssociados") > 0  && Caso n? tenha resultados emite nrDocumento

					lcValidaDadosImpressao = lcValidaDadosImpressao + 1

					**	Tipo: 1 - Resumo Facturas Entidades; 2 - Nota Lan�amento Associados; 3 - Aviso Lan�amento Entidades
					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_associados_dadosEntidades  <<lcNoCliente>>, 2, '<<lcdataimpressaotxt>>', '<<uf_gerais_getdate(lcdatadados,"SQL")>>', 0, '', 0
					ENDTEXT

					If !uf_gerais_actGrelha("","ucrsDadosEntidadePrint",lcSql)
						uf_perguntalt_chama("N�o foi poss�vel obter dados para Emiss�o de Documentos. Por favor contacte o Suporte. Obrigado.","OK","",16)
						Return .F.
					Endif
				Endif
			Endscan

			If lcValidaDadosImpressao = 0
				uf_perguntalt_chama("N�o existem dados para Emiss�o de Documentos no periodo seleccionado. Por favor verifique. Obrigado.","OK","",64)
				Return .F.
			Else
				If lcValidaDadosImpressao = -1
					uf_perguntalt_chama("Documentos j?foram emitidos anteriormente. Obrigado.","OK","",64)
				Else
					uf_perguntalt_chama("Documentos emitidos com sucesso para as entidades seleccionadas. Obrigado.","OK","",64)
				Endif
				&&uf_perguntalt_chama("Documentos emitidos com sucesso para os clientes seleccionados. Obrigado.","OK","",64)
			Endif

		Case MYIMPRESSAOORIGEM == "RESUMORTASSOCIADOS"

			** Vari�veis para definir datas de Impress�o
			Local lcdataimpressaotxt, lcdatadados, lcDataTratamento, lcDo, lcAo
			Store '' To lcdataimpressaotxt, lcdataimpressaotxt
			Store .F. To lcDataTratamento

			lcdataimpressaotxt = IMPRIMIRGERAIS.pageframe1.page1.Data.Value
			lcdatadados		   = Ctot(IMPRIMIRGERAIS.pageframe1.page1.datadados.Value)
			lcDataTratamento   = IMPRIMIRGERAIS.pageframe1.page1.chkTratamento.Value

			lcDo = Iif(Empty(IMPRIMIRGERAIS.pageframe1.page1.txtDO.Value),0,Val(astr(IMPRIMIRGERAIS.pageframe1.page1.txtDO.Value)))
			lcAo = Iif(Empty(IMPRIMIRGERAIS.pageframe1.page1.txtAO.Value),0,Val(astr(IMPRIMIRGERAIS.pageframe1.page1.txtAO.Value)))

			** Calculo de Dados e Impress�o
			If Empty(lcDo) Or Empty(lcAo)
				uf_perguntalt_chama("Intervalo de Entidades n�o configurado.","OK","",16)
				Return .F.
			Endif

			** Criar Cursor para permitir criar "Documentos" por ordem alfab�tica das Entidades
			TEXT TO lcSQL NOSHOW TEXTMERGE
				SELECT * FROM b_utentes WHERE no between <<lcdo>> and <<lcAo>> and tipo = 'Entidade' order BY nome
			ENDTEXT
			If !(uf_gerais_actGrelha("","uCrsEntidadesOrdenadas",lcSql))
			Endif

			** Calculo NrDocumento
			I = 0
			Select uCrsEntidadesOrdenadas
			Go Top
			Scan
				I = I+1

				** Prepara Cursores para impressao
				If Used("ucrsResumoRtEntidades")
					fecha("ucrsResumoRtEntidades")
				Endif

				TEXT TO lcSQL TEXTMERGE NOSHOW
					exec up_resumo_retificado_associados <<YEAR(lcDataDados)>>, <<MONTH(lcDataDados)>>, <<uCrsEntidadesOrdenadas.no>>, <<lcDataTratamento>>, ''
				ENDTEXT

				If !uf_gerais_actGrelha("","uCrsResumoRtEntidades",lcSql)
					uf_perguntalt_chama("N�o foi poss�vel obter dados para Emiss�o de Documentos. Por favor contacte o Suporte. Obrigado.","OK","",16)
					Return .F.
				Endif

				TEXT TO lcSQL TEXTMERGE NOSHOW
					exec up_resumo_retificado_associados_detalhe <<YEAR(lcDataDados)>>, <<MONTH(lcDataDados)>>, <<uCrsEntidadesOrdenadas.no>>, <<lcDataTratamento>>
				ENDTEXT

				If !uf_gerais_actGrelha("","uCrsResumoRtEntidadesDetalhe",lcSql)
					uf_perguntalt_chama("N�o foi poss�vel obter dados para Emiss�o de Documentos. Por favor contacte o Suporte. Obrigado.","OK","",16)
					Return .F.
				Endif

				Select uCrsResumoRtEntidades
				Go Top

				Select uCrsResumoRtEntidadesDetalhe
				Go Top

				If uCrsResumoRtEntidades.Total != 0 &&

					Local temDocEmitido
					Store 1 To temDocEmitido

					** valida se existe alguma linha sem doc impress�o para n�o permitir criar documentos duplicados.
					Select uCrsResumoRtEntidadesDetalhe
					Go Top
					Locate For uCrsResumoRtEntidadesDetalhe.nrDocImp = 0
					If Found()
						temDocEmitido = 0
					Endif

					lcValidaDadosImpressao = lcValidaDadosImpressao + 1

					** Prepara Cursores para impressao
					**	Tipo: 1 - Resumo Facturas Entidades; 2 - Nota Lan�amento Associados; 3 - Aviso Lan�amento Entidades
					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_associados_dadosEntidades <<uCrsEntidadesOrdenadas.no>>, 3, '<<lcDataImpressaoTxt>>', '<<uf_gerais_getdate(lcDataDados,"SQL")>>', <<temDocEmitido>>, '', 0
					ENDTEXT

					If !uf_gerais_actGrelha("","ucrsDadosEntidadePrint",lcSql)
						uf_perguntalt_chama("N�o foi poss�vel obter dados para Emiss�o de Documentos. Por favor contacte o Suporte. Obrigado.","OK","",16)
						Return .F.
					Endif

					Select uCrsResumoRtEntidadesDetalhe
					Go Top
					Select ucrsDadosEntidadePrint
					Go Top
					Select uCrsResumoRtEntidades
					Go Top

					** Colocar n� de documento na tabela de registo de faturas
					Select uCrsResumoRtEntidadesDetalhe
					Go Top
					Scan
						TEXT TO lcSQL TEXTMERGE NOSHOW
							UPDATE cl_fatc_org SET nrDocImp = <<uCrsDadosEntidadePrint.nrDoc>> WHERE id = '<<uCrsResumoRtEntidadesDetalhe.id>>' AND nrDocImp = 0
						ENDTEXT

						If !uf_gerais_actGrelha("","",lcSql)
							uf_perguntalt_chama("Ocorreu uma anomalia emitir a numera��o dos documentos. Por favor contacte o Suporte.","OK","",16)
							uf_registaErroLog('Erro a emitir numeracao dos documentos','Emitir Doc Associados')
						Endif
					Endscan
				Endif
			Endscan

			If lcValidaDadosImpressao = 0
				uf_perguntalt_chama("N�o existem dados para para emiss�o de Documentos no periodo seleccionado. Por favor verifique. Obrigado.","OK","",64)
			Else
				If temDocEmitido = 1
					uf_perguntalt_chama("Documentos j� foram emitidos anteriormente. Obrigado.","OK","",64)
				Else
					uf_perguntalt_chama("Documentos emitidos com sucesso para as entidades seleccionadas. Obrigado.","OK","",64)
				Endif

				Return .F.
			Endif

		Otherwise
			**
	Endcase

Endfunc


** funcao para chmar report de forma a ser poss�vel exportar para excel no m�dulo da AFP associados
Function uf_imprimirGerais_ExportarDoc

	** definic�o de valores para consulta
	uf_gerais_TextoRodape("CL_FATC_ORG")
	lcdataimpressaotxt = IMPRIMIRGERAIS.pageframe1.page1.Data.Value
	lcdataimpressao	   = Ctot(IMPRIMIRGERAIS.pageframe1.page1.Data.Value)
	lcdatadados		   = Ctot(IMPRIMIRGERAIS.pageframe1.page1.datadados.Value)

	lcDo = Iif(Empty(IMPRIMIRGERAIS.pageframe1.page1.txtDO.Value),0,Val(astr(IMPRIMIRGERAIS.pageframe1.page1.txtDO.Value)))
	lcAo = Iif(Empty(IMPRIMIRGERAIS.pageframe1.page1.txtAO.Value),0,Val(astr(IMPRIMIRGERAIS.pageframe1.page1.txtAO.Value)))

	lcNrDocImp = IMPRIMIRGERAIS.pageframe1.page1.nrDocImp.Value

	** valida��es
	If lcDo != lcAo
		uf_perguntalt_chama("N�o � poss�vel exportar a informa��o para mais do que uma Entidade em simult�neo. Por favor verfique.","OK","",16)
		Return .F.
	Endif

	lcreport = ''
	TEXT TO lcReport TEXTMERGE NOSHOW
		&nome_org=<<>>&nr_org=<<INT(lcDo)>>&id_org=<<>>&nome_cl=<<>>&nr_cl=0&id_cl=<<>>&ano=<<YEAR(lcdatadados)>>&mes=<<MONTH(lcdatadados)>>&data_tratamento=0&imprimir=<<>>&nrdoc=<<>>
	ENDTEXT

	uf_gerais_chamaReport("relatorio_Resumo_Faturas_Entidades", Alltrim(lcreport))
Endfunc


** Impress�o tal�o Novo Nordisk
Function uf_talao_novonordisk
	Local lcCodBar, lcNremb, lcSoma, lcCodBarP, lcString, lcRef
	Store '' To lcNremb, lcCodBar, lcCodBarP, lcString, lcRef
	Store 0 To lcSoma

	lcConta=Reccount("fi_nnordisk")
	Select fi_nnordisk
	Go Top
	Scan
		If !Empty(fi_nnordisk.codbar)
			lcCodBar=Alltrim(fi_nnordisk.codbar)
		Endif
	Endscan
	If !Empty(lcCodBar)
		lcSoma=Val(Substr(lcCodBar,Len(Alltrim(lcCodBar))-2,2))
	Else
		lcSoma=0
	Endif
	** Alterado em 2020.02.13 porque o limite de embalagens passou para 24
	**IF lcsoma=11
	Local lcnaoimprime
	lcnaoimprime = .F.

	Local lcMaxEmb
	lcMaxEmb = uf_gerais_getParameter('ADM0000000362', 'NUM')

	If !uf_gerais_getParameter('ADM0000000349', 'BOOL')
		If lcSoma>=23
			lcSoma=0
			lcnaoimprime = .T.
		Endif
	Else
		If lcSoma>=lcMaxEmb
			lcSoma=0
			lcnaoimprime = .T.
		Endif
	Endif



	Select fi_nnordisk
	Select * From fi_nnordisk Where 1 = 0 Into Cursor fi_nnordiskTMP Readwrite

	Select fi_nnordisk
	Go Top
	Scan
		lcSoma=lcSoma+1

		lcString=Right('00000'+Alltrim(ucrse1.u_codfarm),5)+Right('000000'+Alltrim(fi_nnordisk.nrembal),6)+Right('0'+Alltrim(Str(lcSoma)),2)

		lcCodBarP=Alltrim(lcString)+Alltrim(Str(uf_novonordisk_chkdigit(Alltrim(lcString),'Emite')))

		**	_cliptext=lcCodBarP
		**	MESSAGEBOX(lcCodBarP)
		**	MESSAGEBOX(lcnaoimprime)


		**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
		If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")
			If lcnaoimprime = .F.
				Public upv_codBarP

				upv_codBarP = lcCodBarP

				uv_idImp = uf_gerais_getUmValor("B_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Controlo Saxenda'")

				Select fi_nnordiskTMP
				Delete All

				Select fi_nnordisk
				Scatter Memvar

				Select fi_nnordiskTMP
				Append Blank
				Gather Memvar

				uf_imprimirgerais_sendprinter_talao(uv_idImp , .F., .F.)
			Endif

		Else
			If lcnaoimprime = .F.

				&& PREPARAR DEFINI��ES GERAIS DA IMPRESSORA
				lcValidaImp = uf_gerais_setImpressoraPOS(.T.)

				If lcValidaImp
					*** INICIALIZAR A IMPRESSORA ***
					???	Chr(027)+Chr(064)
					*** ESCOLHER A IMPRESSORA ***
					???	Chr(27)+Chr(99)+Chr(48)+Chr(1)
					*** DEFINIR C�DIGO DE CARACTERES PORTUGU�S ***
					???	Chr(027)+Chr(116)+Chr(003)
					*** ESCOLHER MODOS DE IMPRESS�O ***
					???	Chr(27)+Chr(33)+Chr(1)


					****** IMPRESS�O DO CABE�ALHO ******
					*** NOME DA EMPRESA ***
					??	" "
					?	Left(Alltrim(ucrse1.NOMECOMP),uf_gerais_getParameter("ADM0000000194","NUM"))

					*** OUTROS DADOS DA EMPRESA ***
					??? Chr(27)+Chr(33)+Chr(1)	&& print mode B
					?	Substr(ucrse1.Local,1,10)
					??	" Tel:"
					??	Transform(ucrse1.TELEFONE,"#########")
					??	" NIF:"
					??	Transform(ucrse1.ncont,"999999999")
					?   Alltrim(uf_gerais_getMacrosReports(2))
					?	"Dir. Tec. "
					??	Left(Alltrim(ucrse1.U_DIRTEC),uf_gerais_getParameter("ADM0000000194","NUM"))
					??
					uf_gerais_separadorPOS()
					?	Padc("TALAO DE CONTROLO",uf_gerais_getParameter("ADM0000000194","NUM")," ")
					uf_gerais_separadorPOS()
					?	"Data / Hora:  "
					??	Datetime()+(difhoraria*3600)
					uf_gerais_separadorPOS()

					&& Impressao codigo barras com identifica��o reservas
					?
					??	Chr(27)+Chr(97)+Chr(1)
					?'Saxenda 6mg/ml 5x3ml Sol Inj Can PC'
					?
					??? Chr(29) + Chr(104) +Chr(50) + Chr(29)+ Chr(72) + Chr(2) + Chr(29)+ Chr(119)+Chr(1) + Chr(29)+Chr(107)+Chr(4) + Upper(Alltrim(lcCodBarP)) + Chr(0)

					**   ??? chr(29) + chr(104) +chr(50) + chr(29)+ chr(72) + chr(2) + chr(29)+ chr(119)+chr(2) + chr(29)+chr(107)+chr(52) +chr(4) + UPPER(ALLTRIM(lcCodBarP)) + chr(0)

					?	"CODIGO DE VALIDACAO:   "
					??	 Upper(Alltrim(lcCodBarP))

					??	Chr(27)+Chr(97)+Chr(1)
					? ""
					??  ""

					****** IMPRESS�O DO RODAP� ******
					?	"Data de Emissao:  "
					??	Datetime()+(difhoraria*3600)

					uf_gerais_feedCutPOS()
				Endif
				uf_gerais_setImpressoraPOS(.F.)

				Set Printer To Default
			Endif
		Endif
		lcnaoimprime = .F.
		Select fi_nnordisk
	Endscan

Endfunc


**
Function uf_imprimirgerais_dataMatrixCriaImagem_Entidades
	Lparameters lcNdoc, lnU_tipodoc,id_anf,lcVerso

	Public myFilePathDataMatrix
	Local lcU_tipodoc
	If Empty(lnU_tipodoc)
		lcU_tipodoc = 0
	Else
		lcU_tipodoc = lnU_tipodoc
	Endif

	&&LOCAL lcCodigoANF
	Local lcString, lcFilePath, lcFtstamp
	Store '' To lcString, lcFilePath, lcFtstamp
	&& vari�veis para totais fatura
	Local lcTotalLotes, lcTotalReceitas, totalPVP, totalUtente, totalEntidade, lcTotalEmbalagens, lcValorIvaLinha, lctotalIncentivo, lcTotalIva , lcValorProtocolo, lcValor3Protocolo
	Store 0 To lcTotalLotes, lcTotalReceitas, lcTotalPVP, lcTotalUtente, lcTotalEntidade, lcTotalEmbalagens, lcValorIvaLinha, lctotalIncentivo,lcTotalIva,lcValorProtocolo, lcValor3Protocolo
	&& vari�veis para totais por tipo de lote
	Local lcTotalLotes10, lcTotalReceitas10, lcTotalPVP10, lctotalEntidade10, lcTotalEmbalagens10, lcTotalPVP10, lctotalUtente10, lctotalIncentivo10
	Local lcTotalLotes11, lcTotalReceitas11, lcTotalPVP11, lctotalEntidade11, lcTotalEmbalagens11, lcTotalPVP11, lctotalUtente11, lctotalIncentivo11
	Local lcTotalLotes12, lcTotalReceitas12, lcTotalPVP12, lctotalEntidade12, lcTotalEmbalagens12, lcTotalPVP12, lctotalUtente12, lctotalIncentivo12
	Local lcTotalLotes13, lcTotalReceitas13, lcTotalPVP13, lctotalEntidade13, lcTotalEmbalagens13, lcTotalPVP13, lctotalUtente13, lctotalIncentivo13
	Local lcTotalLotes15, lcTotalReceitas15, lcTotalPVP15, lctotalEntidade15, lcTotalEmbalagens15, lcTotalPVP15, lctotalUtente15, lctotalIncentivo15
	Local lcTotalLotes16, lcTotalReceitas16, lcTotalPVP16, lctotalEntidade16, lcTotalEmbalagens16, lcTotalPVP16, lctotalUtente16, lctotalIncentivo16
	Local lcTotalLotes17, lcTotalReceitas17, lcTotalPVP17, lctotalEntidade17, lcTotalEmbalagens17, lcTotalPVP17, lctotalUtente17, lctotalIncentivo17
	Local lcTotalLotes18, lcTotalReceitas18, lcTotalPVP18, lctotalEntidade18, lcTotalEmbalagens18, lcTotalPVP18, lctotalUtente18, lctotalIncentivo18
	Local lcTotalLotes19, lcTotalReceitas19, lcTotalPVP19, lctotalEntidade19, lcTotalEmbalagens19, lcTotalPVP19, lctotalUtente19, lctotalIncentivo19
	Local lcTotalLotes23, lcTotalReceitas23, lcTotalPVP23, lctotalEntidade23, lcTotalEmbalagens23, lcTotalPVP23, lctotalUtente23, lctotalIncentivo23
	Local lcTotalLotes30, lcTotalReceitas30, lcTotalPVP30, lctotalEntidade30, lcTotalEmbalagens30, lcTotalPVP30, lctotalUtente30, lctotalIncentivo30
	Local lcTotalLotes96, lcTotalReceitas96, lcTotalPVP96, lctotalEntidade96, lcTotalEmbalagens96, lcTotalPVP96, lctotalUtente96, lctotalIncentivo96
	Local lcTotalLotes97, lcTotalReceitas97, lcTotalPVP97, lctotalEntidade97, lcTotalEmbalagens97, lcTotalPVP97, lctotalUtente97, lctotalIncentivo97
	Local lcTotalLotes98, lcTotalReceitas98, lcTotalPVP98, lctotalEntidade98, lcTotalEmbalagens98, lcTotalPVP98, lctotalUtente98, lctotalIncentivo98
	Local lcTotalLotes99, lcTotalReceitas99, lcTotalPVP99, lctotalEntidade99, lcTotalEmbalagens99, lcTotalPVP99, lctotalUtente99, lctotalIncentivo99
	Store 0 To lcTotalLotes23, lcTotalReceitas23, lcTotalPVP23, lctotalEntidade23, lcTotalEmbalagens23, lcTotalPVP23, lctotalUtente23, lctotalIncentivo23
	Store 0 To lcTotalLotes10, lcTotalReceitas10, lcTotalPVP10, lctotalEntidade10, lcTotalEmbalagens10, lcTotalPVP10, lctotalUtente10, lctotalIncentivo10
	Store 0 To lcTotalLotes11, lcTotalReceitas11, lcTotalPVP11, lctotalEntidade11, lcTotalEmbalagens11, lcTotalPVP11, lctotalUtente11, lctotalIncentivo11
	Store 0 To lcTotalLotes12, lcTotalReceitas12, lcTotalPVP12, lctotalEntidade12, lcTotalEmbalagens12, lcTotalPVP12, lctotalUtente12, lctotalIncentivo12
	Store 0 To lcTotalLotes18, lcTotalReceitas18, lcTotalPVP18, lctotalEntidade18, lcTotalEmbalagens18, lcTotalPVP18, lctotalUtente18, lctotalIncentivo18
	Store 0 To lcTotalLotes19, lcTotalReceitas19, lcTotalPVP19, lctotalEntidade19, lcTotalEmbalagens19, lcTotalPVP19, lctotalUtente19, lctotalIncentivo19
	Store 0 To lcTotalLotes17, lcTotalReceitas17, lcTotalPVP17, lctotalEntidade17, lcTotalEmbalagens17, lcTotalPVP17, lctotalUtente17, lctotalIncentivo17
	Store 0 To lcTotalLotes15, lcTotalReceitas15, lcTotalPVP15, lctotalEntidade15, lcTotalEmbalagens15, lcTotalPVP15, lctotalUtente15, lctotalIncentivo15
	Store 0 To lcTotalLotes16, lcTotalReceitas16, lcTotalPVP16, lctotalEntidade16, lcTotalEmbalagens16, lcTotalPVP16, lctotalUtente16, lctotalIncentivo16
	Store 0 To lcTotalLotes13, lcTotalReceitas13, lcTotalPVP13, lctotalEntidade13, lcTotalEmbalagens13, lcTotalPVP13, lctotalUtente13, lctotalIncentivo13
	Store 0 To lcTotalLotes30, lcTotalReceitas30, lcTotalPVP30, lctotalEntidade30, lcTotalEmbalagens30, lcTotalPVP30, lctotalUtente30, lctotalIncentivo30
	Store 0 To lcTotalLotes96, lcTotalReceitas96, lcTotalPVP96, lctotalEntidade96, lcTotalEmbalagens96, lcTotalPVP96, lctotalUtente96, lctotalIncentivo96
	Store 0 To lcTotalLotes97, lcTotalReceitas97, lcTotalPVP97, lctotalEntidade97, lcTotalEmbalagens97, lcTotalPVP97, lctotalUtente97, lctotalIncentivo97
	Store 0 To lcTotalLotes98, lcTotalReceitas98, lcTotalPVP98, lctotalEntidade98, lcTotalEmbalagens98, lcTotalPVP98, lctotalUtente98, lctotalIncentivo98
	Store 0 To lcTotalLotes99, lcTotalReceitas99, lcTotalPVP99, lctotalEntidade99, lcTotalEmbalagens99, lcTotalPVP99, lctotalUtente99, lctotalIncentivo99


	Do Case
		Case lcNdoc == 0 And !Empty(lcVerso)

			If !Used("uCrsFt")
				Return .F.
			Else
				Select uCrsFt
				lcFtstamp = uCrsFt.ftstamp
			Endif

			If Used("uCrsDMl")
				fecha("uCrsDMl")
			Endif
			Create Cursor uCrsDMl(cnp c(7), portaria c(3), pvp c(6), Pref c(6), comparticipacao c(6), valorutente c(6), direitoopcao c(1))

			lcString = ""

			Local lcMsg
			lcMsg = ""

			** cursores e variaveis para o data-matrix (cabe�alho e linhas)
			Local dm_codigofarmacia, dm_codigoentidade, dm_data, dm_operador, dm_serie, dm_lote, dm_posicaolote, dm_nrvenda, dm_nrreceita, dm_campo2, dm_campo3, dm_campo4, dm_totalutenteliquido
			Store "" To dm_codigofarmacia, dm_codigoentidade, dm_data, dm_operador, dm_serie, dm_lote, dm_posicaolote, dm_nrvenda, dm_nrreceita, dm_campo2, dm_campo3, dm_campo4, dm_totalutenteliquido
			Create Cursor uCrsDMl(cnp c(7), portaria c(3), pvp c(6), Pref c(6), comparticipacao c(6), valorutente c(6), direitoopcao c(1))

			If uCrsFt.u_tipodoc==3 && se for uma inser��o de receita imprime cdata
				If uf_gerais_getdate(uCrsFt.cData,"DATA") == '01.01.1900'
					dm_data = "00000000" && datamatrix data
				Else
					dm_data = Replicate('0',8-Len(uf_gerais_getdate(uCrsFt.cData,"SQL"))) + uf_gerais_getdate(uCrsFt.cData,"SQL") && datamatrix data
				Endif
			Else
				If uf_gerais_getdate(uCrsFt.cData,"DATA") == '01.01.1900'
					dm_data = Replicate('0',8-Len(uf_gerais_getdate(uCrsFt.fdata,"SQL"))) + uf_gerais_getdate(uCrsFt.fdata,"SQL") && datamatrix data
				Else
					dm_data = Replicate('0',8-Len(uf_gerais_getdate(uCrsFt.cData,"SQL"))) + uf_gerais_getdate(uCrsFt.cData,"SQL") && data
				Endif
			Endif

			** Tipo e Nr Documento
			dm_operador = Replicate('0',10-Len(Alltrim(Left(uCrsFt.vendnm,10)))) + Alltrim(Left(uCrsFt.vendnm,10)) && datamatrix vendedor
			dm_nrvenda = Replicate('0',7-Len(astr(uCrsFt.fno))) + astr(uCrsFt.fno) && datamatrix n�mero venda
			dm_nrreceita = Replicate('0',20-Len(Alltrim(uCrsFt2.u_receita))) + Alltrim(uCrsFt2.u_receita) && datamatrix receita


			Do Case
				Case lcVerso = 1
					dm_codigoentidade = Replicate('0',3-Len(Alltrim(uCrsFt2.U_CODIGO))) + Alltrim(uCrsFt2.U_CODIGO) && C�digo entidade (??? c�digo plano)
				Case lcVerso = 2
					dm_codigoentidade = Replicate('0',3-Len(Alltrim(uCrsFt2.U_CODIGO2))) + Alltrim(uCrsFt2.U_CODIGO2) && C�digo entidade 2 (??? c�digo plano)
			Endcase

			dm_lote = Replicate('0',4-Len(astr(L.LOTE))) + astr(L.LOTE) && datamatrix lote
			dm_posicaolote = Replicate('0',3-Len(astr(R.NRECEITA))) + astr(R.NRECEITA) && damatrix posicao lote
			dm_serie = Replicate('0',3-Len(astr(S.SLOTE))) + astr(S.SLOTE) && datamatrix serie

			** guardar tabela diplomas para mapear com id para o c�digo matrix
			Local lcDiploma_id
			If !Used("uCrsDplms")
				uf_gerais_actGrelha("","uCrsDplms","select u_design, diploma_id from dplms (nolock) where u_design!=''")
			Endif

			****** IMPRESS�O DAS LINHAS ******
			Local lcContadorLinhas
			lcContadorLinhas = 1
			Select uCrsFi
			Do Case
				Case lcVerso = 1

					Scan For uCrsFi.U_ETTENT1 > 0 And Alltrim(uCrsFi.ftstamp) == Alltrim(uCrsFt.ftstamp)
						For I=1 To uCrsFi.qtt
							** datamatrix
							Select uCrsDMl
							Append Blank
							Replace uCrsDMl.cnp With Replicate('0', 7 - Len(Alltrim(Left(uCrsFi.ref,7)))) + Alltrim(Left(uCrsFi.ref,7)) && datamatrix cnp

							** datamatrix portaria
							lcDiploma_id = 0
							Select uCrsDplms
							Go Top
							Scan
								If Alltrim(uCrsDplms.u_design) == Alltrim(uCrsFi.u_diploma)
									lcDiploma_id = uCrsDplms.diploma_id
								Endif
							Endscan

							Select uCrsDMl
							Replace uCrsDMl.portaria With Replicate('0', 3 - Len(astr(lcDiploma_id))) + astr(lcDiploma_id) && datamatrix portaria
							Replace uCrsDMl.pvp With Replicate('0', 6 - Len(Left(astr(Round(uCrsFi.u_epvp * 100, 0)),6))) + Left(astr(Round(uCrsFi.u_epvp * 100,0)),6) && datamatrix pvp
							Replace uCrsDMl.Pref With Replicate('0', 6 - Len(Left(astr(Round(uCrsFi.u_epref * 100,0)),6))) + Left(astr(Round(uCrsFi.u_epref * 100,0)),6) && datamatrix pref
							Replace uCrsDMl.comparticipacao With Replicate('0', 6 - Len(Left(astr(Round(Round(uCrsFi.U_ETTENT1/uCrsFi.qtt,2) * 100,0)),6))) + Left(astr(Round(Round(uCrsFi.U_ETTENT1/uCrsFi.qtt,2) * 100,0)),6) && datamatrix comparticipacao
							Replace uCrsDMl.valorutente With Replicate('0', 6 - Len(Left(astr(Round(Round(uCrsFi.u_epvp-uCrsFi.U_ETTENT1/uCrsFi.qtt,2) * 100,0)),6))) + Left(astr(Round(Round(uCrsFi.u_epvp-uCrsFi.U_ETTENT1/uCrsFi.qtt,2) * 100,0)),6) && datamatrix valorutente

							If Empty(Alltrim(uCrsFi.lobs3)) && se n�o for utilizada excep��o
								Replace uCrsDMl.direitoopcao With uCrsFi.opcao && datamatrix direito opcao
							Else
								Replace uCrsDMl.direitoopcao With "N" && datamatrix direito opcao
							Endif
						Endfor

						Select uCrsFi
					Endscan

				Case lcVerso = 2
					Scan For uCrsFi.U_ETTENT2 > 0 And Alltrim(uCrsFi.ftstamp) == Alltrim(uCrsFt.ftstamp)
						For I=1 To uCrsFi.qtt

							** datamatrix
							Select uCrsDMl
							Append Blank
							Replace uCrsDMl.cnp With Replicate('0',7-Len(Alltrim(uCrsFi.ref))) + Alltrim(uCrsFi.ref) && datamatrix cnp

							** datamatrix portaria
							lcDiploma_id = 0
							Select uCrsDplms
							Go Top
							Scan
								If Alltrim(uCrsDplms.u_design) == Alltrim(uCrsFi.u_diploma)
									lcDiploma_id = uCrsDplms.diploma_id
								Endif
							Endscan

							Select uCrsDMl
							Replace uCrsDMl.portaria With Replicate('0', 3 - Len(astr(lcDiploma_id))) + astr(lcDiploma_id) && datamatrix portaria
							Replace uCrsDMl.pvp With Replicate('0', 6 - Len(Left(astr(Round(uCrsFi.u_epvp * 100, 0)),6))) + Left(astr(Round(uCrsFi.u_epvp * 100,0)),6) && datamatrix pvp
							Replace uCrsDMl.Pref With Replicate('0', 6 - Len(Left(astr(Round(uCrsFi.u_epref * 100,0)),6))) + Left(astr(Round(uCrsFi.u_epref * 100,0)),6) && datamatrix pref
							Replace uCrsDMl.comparticipacao With Replicate('0', 6 - Len(Left(astr(Round(Round(uCrsFi.U_ETTENT2/uCrsFi.qtt,2) * 100,0)),6))) + Left(astr(Round(Round(uCrsFi.U_ETTENT2/uCrsFi.qtt,2) * 100,0)),6) && datamatrix comparticipacao
							Replace uCrsDMl.valorutente With Replicate('0', 6 - Len(Left(astr(Round(Round(uCrsFi.u_epvp-uCrsFi.U_ETTENT2/uCrsFi.qtt,2) * 100,0)),6))) + Left(astr(Round(Round(uCrsFi.u_epvp-uCrsFi.U_ETTENT2/uCrsFi.qtt,2) * 100,0)),6) && datamatrix valorutente

							If Empty(Alltrim(uCrsFi.lobs3)) && se n�o for utilizada excep��o
								Replace uCrsDMl.direitoopcao With uCrsFi.opcao && datamatrix direito opcao
							Else
								Replace uCrsDMl.direitoopcao With "N" && datamatrix direito opcao
							Endif
						Endfor

						Select uCrsFi
					Endscan
			Endcase

			&& Imprime linhas que n�o est�o preenchidas
			Select uCrsDMl
			Count To lcPosicoesDML
			If lcPosicoesDML < 4
				For I=1 To 4-lcPosicoesDML

					** datamatrix
					Select uCrsDMl
					Append Blank
					Replace cnp With Replicate('0',7) && datamatrix cnp
					Replace portaria With Replicate('0',3) && datamatrix portaria
					Replace pvp With Replicate('0', 6) && datamatrix pvp
					Replace Pref With Replicate('0', 6) && datamatrix pref
					Replace comparticipacao With Replicate('0', 6) && datamatrix comparticipacao
					Replace valorutente With Replicate('0', 6) && datamatrix valorutente
					Replace direitoopcao With 'I'
				Endfor
			Endif

			Select uCrsFi

			** PREPARAR OS TOTAIS
			Do Case
				Case lcVerso = 1
					Calculate Sum(uCrsFi.u_epvp*uCrsFi.qtt) For uCrsFi.U_ETTENT1>0 And Alltrim(uCrsFi.ftstamp) = Alltrim(uCrsFt.ftstamp) To totalPVP
					Calculate Sum(uCrsFi.qtt) For uCrsFi.U_ETTENT1>0 And Alltrim(uCrsFi.ftstamp) = Alltrim(uCrsFt.ftstamp) To totalQtt
					Calculate Sum(uCrsFi.U_ETTENT1) For uCrsFi.U_ETTENT1>0 And Alltrim(uCrsFi.ftstamp) = Alltrim(uCrsFt.ftstamp) To totalComp
					Calculate Sum(uCrsFi.u_epvp*uCrsFi.qtt-uCrsFi.U_ETTENT1) For uCrsFi.U_ETTENT1>0 And Alltrim(uCrsFi.ftstamp) = Alltrim(uCrsFt.ftstamp) To totalUtente
				Case lcVerso = 2
					Calculate Sum(uCrsFi.u_epvp*uCrsFi.qtt) For uCrsFi.U_ETTENT2>0 And Alltrim(uCrsFi.ftstamp) = Alltrim(uCrsFt.ftstamp) To totalPVP
					Calculate Sum(uCrsFi.qtt) For uCrsFi.U_ETTENT2>0 And Alltrim(uCrsFi.ftstamp) = Alltrim(uCrsFt.ftstamp) To totalQtt
					Calculate Sum(uCrsFi.U_ETTENT2) For uCrsFi.U_ETTENT2>0 And Alltrim(uCrsFi.ftstamp) = Alltrim(uCrsFt.ftstamp) To totalComp
					Calculate Sum(uCrsFi.u_epvp*uCrsFi.qtt-uCrsFi.U_ETTENT2) For uCrsFi.U_ETTENT2>0 And Alltrim(uCrsFi.ftstamp) = Alltrim(uCrsFt.ftstamp) To totalUtente
					**calculate sum(uCrsFi.pvp4_fee*uCrsFi.qtt-uCrsFi.u_ettent2) FOR uCrsFi.u_ettent2>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalIncentivo
			Endcase

			dm_totalutenteliquido = Replicate('0',6-Len(astr(Round(totalUtente,2) * 100))) + astr(Round(totalUtente,2) * 100) && datamatrix total utente iliquido
			**dm_totalutenteliquido = REPLICATE('0',6-LEN(astr(round(totalUtente,2) * 100))) + astr(round(totalUtente,2) * 100) && datamatrix total incentivo iliquido

			**guarda Valor Protocolo enquanto nao obtemos resposta deles do que � colocamos a 0
			lcValorProtocolo = '0'

			**guarda Valor do 3� Protocolo enquanto nao obtemos resposta deles do que � colocamos a 0
			lcValor3Protocolo = '0'

			** DataMatrix **
			Local lcDM
			lcDM = ""

			Select ucrse1
			Go Top

			** vers�o
			lcDM = lcDM	+ '103'

			** c�digo farm�cia
			lcDM = lcDM + Replicate('0',6-Len(astr(ucrse1.u_infarmed))) + astr(ucrse1.u_infarmed)

			** codigo entidade + data + operador + serie + lote + posicao lote + numero venda + numero receita + campo2 + campo3 + campo4
			lcDM = lcDM + dm_codigoentidade + dm_data + dm_operador + dm_serie + dm_lote + dm_posicaolote + dm_nrvenda + dm_nrreceita

			** campo2 + campo3 + campo4
			lcDM = lcDM	+ "000000000000" + "000000000000" + "00000000000000000000"

			** linhas
			Select uCrsDMl
			Go Top
			Scan
				** cnp + portaria + pvp + pref + comparticipacao + valor utente + direitoopcao
				lcDM = lcDM + uCrsDMl.cnp + uCrsDMl.portaria + uCrsDMl.pvp + uCrsDMl.Pref + uCrsDMl.comparticipacao + uCrsDMl.valorutente + uCrsDMl.direitoopcao
			Endscan
			fecha("uCrsDMl")

			** total utente liquido
			lcString = lcDM + dm_totalutenteliquido
			****************

		Case !Empty(id_anf)

			If !Used("FT")
				Return .F.
			Else
				Select ft
				lcFtstamp = ft.ftstamp
				lcTotalIva= Round(ft.eivav1,2) && guarda valor do iva
			Endif

			&& Aten��o ques esta SP � partilhada com o webservice que envia a fatura ao SNS eletronicamente.
			Select ft
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_receituario_fe_detalheLote <<YEAR(Ft.cdata)>>, <<MONTH(Ft.cdata)>>, '<<uCrsCptorg.abrev>>', '', '', 0, 0, '<<mysite>>'
			ENDTEXT

			If !uf_gerais_actGrelha("","ucrsDetalheLoteTemp",lcSql)
				uf_perguntalt_chama("N�o foi poss�vel definir a configura��o de impress�es. Por favor contacte o Suporte.","OK","",16)
				Return .F.
			Endif

			Select ucrsDetalheLoteTemp
			Select Max(LOTE) As loteMax, Max(NRECEITA) As receitaMax From ucrsDetalheLoteTemp Group By tlote Into Cursor ucrsDetalheLoteTemp2 Readwrite && Necess�rio para calcular o sum distinct
			Select Sum(loteMax) As totalLotes, Sum(receitaMax) As totalReceitas From ucrsDetalheLoteTemp2 Into Cursor ucrsDetalheLoteTemp3 Readwrite
			Select tlote,LOTE,Max(NRECEITA) As receitaMax From ucrsDetalheLoteTemp Group By tlote,LOTE Into Cursor ucrsDetalheLoteTemp4 Readwrite
			Select Sum(receitaMax) As totalReceitas From ucrsDetalheLoteTemp4  Into Cursor ucrsDetalheLoteTemp5 Readwrite

			Select ucrsDetalheLoteTemp3
			lcTotalLotes = ucrsDetalheLoteTemp3.totalLotes
			Select ucrsDetalheLoteTemp5
			lcTotalReceitas = ucrsDetalheLoteTemp5.totalReceitas

			Select ucrsDetalheLoteTemp
			Calculate Sum(ucrsDetalheLoteTemp.pvp) To lcTotalPVP
			Calculate Sum(qtt) To lcTotalEmbalagens
			Calculate Sum(Comp) To lcTotalEntidade
			Calculate Sum(utente) To lcTotalUtente
			Calculate Sum(pvp4_fee) To lctotalIncentivo
			&& Calcular nr de linhas da fatura -> tlote distinct
			Select tlote From ucrsDetalheLoteTemp Group By tlote Into Cursor ucrsDetalheLoteTempAux Readwrite
			Select ucrsDetalheLoteTempAux
			lcNumeroLinhas = Reccount("ucrsDetalheLoteTempAux")


			** Tipo Lote 10
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "10" To lcTotalLotes10

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "10" To lcTotalReceitas10
			Select ucrsDetalheLoteTemp

			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "10" To lcTotalPVP10
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "10" To lctotalEntidade10
			Calculate Sum(qtt) For tlote = "10" To lcTotalEmbalagens10
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "10" To lcTotalPVP10
			Calculate Sum(utente) For tlote = "10" To lctotalUtente10
			Calculate Sum(pvp4_fee) For tlote = "10" To lctotalIncentivo10


			** Tipo Lote 11
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "11" To lcTotalLotes11

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "11" To lcTotalReceitas11
			Select ucrsDetalheLoteTemp

			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "11" To lcTotalPVP11
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "11" To lctotalEntidade11
			Calculate Sum(qtt) For tlote = "11" To lcTotalEmbalagens11
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "11" To lcTotalPVP11
			Calculate Sum(utente) For tlote = "11" To lctotalUtente11
			Calculate Sum(pvp4_fee) For tlote = "11" To lctotalIncentivo11


			** Tipo Lote 12
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "12" To lcTotalLotes12

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "12" To lcTotalReceitas12
			Select ucrsDetalheLoteTemp

			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "12" To lcTotalPVP12
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "12" To lctotalEntidade12
			Calculate Sum(qtt) For tlote = "12" To lcTotalEmbalagens12
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "12" To lcTotalPVP12
			Calculate Sum(utente) For tlote = "12" To lctotalUtente12
			Calculate Sum(pvp4_fee) For tlote = "12" To lctotalIncentivo12


			** Tipo Lote 13
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "13" To lcTotalLotes13

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "13" To lcTotalReceitas13
			Select ucrsDetalheLoteTemp

			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "13" To lcTotalPVP13
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "13" To lctotalEntidade13
			Calculate Sum(qtt) For tlote = "13" To lcTotalEmbalagens13
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "13" To lcTotalPVP13
			Calculate Sum(utente) For tlote = "13" To lctotalUtente13
			Calculate Sum(pvp4_fee) For tlote = "13" To lctotalIncentivo13


			** Tipo Lote 15
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "15" To lcTotalLotes15

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "15" To lcTotalReceitas15

			Select ucrsDetalheLoteTemp
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "15" To lcTotalPVP15
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "15" To lctotalEntidade15
			Calculate Sum(qtt) For tlote = "15" To lcTotalEmbalagens15
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "15" To lcTotalPVP15
			Calculate Sum(utente) For tlote = "15" To lctotalUtente15
			Calculate Sum(pvp4_fee) For tlote = "15" To lctotalIncentivo15


			** Tipo Lote 16
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "16" To lcTotalLotes16

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "16" To lcTotalReceitas16
			Select ucrsDetalheLoteTemp
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "16" To lcTotalPVP16
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "16" To lctotalEntidade16
			Calculate Sum(qtt) For tlote = "16" To lcTotalEmbalagens16
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "16" To lcTotalPVP16
			Calculate Sum(utente) For tlote = "16" To lctotalUtente16
			Calculate Sum(pvp4_fee) For tlote = "16" To lctotalIncentivo16


			** Tipo Lote 17
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "17" To lcTotalLotes17

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "17" To lcTotalReceitas17
			Select ucrsDetalheLoteTemp

			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "17" To lcTotalPVP17
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "17" To lctotalEntidade17
			Calculate Sum(qtt) For tlote = "17" To lcTotalEmbalagens17
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "17" To lcTotalPVP17
			Calculate Sum(utente) For tlote = "17" To lctotalUtente17
			Calculate Sum(pvp4_fee) For tlote = "17" To lctotalIncentivo17


			** Tipo Lote 18
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "18" To lcTotalLotes18

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "18" To lcTotalReceitas18
			Select ucrsDetalheLoteTemp

			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "18" To lcTotalPVP18
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "18" To lctotalEntidade18
			Calculate Sum(qtt) For tlote = "18" To lcTotalEmbalagens18
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "18" To lcTotalPVP18
			Calculate Sum(utente) For tlote = "18" To lctotalUtente18
			Calculate Sum(pvp4_fee) For tlote = "18" To lctotalIncentivo18


			** Tipo Lote 19
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "19" To lcTotalLotes19

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "19" To lcTotalReceitas19
			Select ucrsDetalheLoteTemp

			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "19" To lcTotalPVP19
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "19" To lctotalEntidade19
			Calculate Sum(qtt) For tlote = "19" To lcTotalEmbalagens19
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "19" To lcTotalPVP19
			Calculate Sum(utente) For tlote = "19" To lctotalUtente19
			Calculate Sum(pvp4_fee) For tlote = "19" To lctotalIncentivo19


			** Tipo Lote 23
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "23" To lcTotalLotes23

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "23" To lcTotalReceitas23

			Select ucrsDetalheLoteTemp
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "23" To lcTotalPVP23
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "23" To lctotalEntidade23
			Calculate Sum(ucrsDetalheLoteTemp.qtt) For tlote = "23" To lcTotalEmbalagens23
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "23" To lcTotalPVP23
			Calculate Sum(utente) For tlote = "23" To lctotalUtente23
			Calculate Sum(pvp4_fee) For tlote = "23" To lctotalIncentivo23


			** Tipo Lote 30
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "30" To lcTotalLotes30

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "30" To lcTotalReceitas30

			Select ucrsDetalheLoteTemp
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "30" To lcTotalPVP30
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "30" To lctotalEntidade30
			Calculate Sum(ucrsDetalheLoteTemp.qtt) For tlote = "30" To lcTotalEmbalagens30
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "30" To lcTotalPVP30
			Calculate Sum(utente) For tlote = "30" To lctotalUtente30
			Calculate Sum(pvp4_fee) For tlote = "30" To lctotalIncentivo30


			** Tipo Lote 96
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "96" To lcTotalLotes96

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "96" To lcTotalReceitas96

			Select ucrsDetalheLoteTemp
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "96" To lcTotalPVP96
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "96" To lctotalEntidade96
			Calculate Sum(ucrsDetalheLoteTemp.qtt) For tlote = "96" To lcTotalEmbalagens96
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "96" To lcTotalPVP96
			Calculate Sum(utente) For tlote = "96" To lctotalUtente96
			Calculate Sum(pvp4_fee) For tlote = "96" To lctotalIncentivo96


			** Tipo Lote 97
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "97" To lcTotalLotes97

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "97" To lcTotalReceitas97

			Select ucrsDetalheLoteTemp
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "97" To lcTotalPVP97
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "97" To lctotalEntidade97
			Calculate Sum(ucrsDetalheLoteTemp.qtt) For tlote = "97" To lcTotalEmbalagens97
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "97" To lcTotalPVP97
			Calculate Sum(utente) For tlote = "97" To lctotalUtente97
			Calculate Sum(pvp4_fee) For tlote = "97" To lctotalIncentivo97


			** Tipo Lote 98
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "98" To lcTotalLotes98

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "98" To lcTotalReceitas98

			Select ucrsDetalheLoteTemp
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "98" To lcTotalPVP98
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "98" To lctotalEntidade98
			Calculate Sum(ucrsDetalheLoteTemp.qtt) For tlote = "98" To lcTotalEmbalagens98
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "98" To lcTotalPVP98
			Calculate Sum(utente) For tlote = "98" To lctotalUtente98
			Calculate Sum(pvp4_fee) For tlote = "98" To lctotalIncentivo98


			** Tipo Lote 99
			Select ucrsDetalheLoteTemp
			Calculate Max(ucrsDetalheLoteTemp.LOTE) For tlote = "99" To lcTotalLotes99

			Select ucrsDetalheLoteTemp4
			Calculate Sum(ucrsDetalheLoteTemp4.receitaMax) For tlote = "99" To lcTotalReceitas99

			Select ucrsDetalheLoteTemp
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "99" To lcTotalPVP99
			Calculate Sum(ucrsDetalheLoteTemp.Comp) For tlote = "99" To lctotalEntidade99
			Calculate Sum(ucrsDetalheLoteTemp.qtt) For tlote = "99" To lcTotalEmbalagens99
			Calculate Sum(ucrsDetalheLoteTemp.pvp) For tlote = "99" To lcTotalPVP99
			Calculate Sum(utente) For tlote = "99" To lctotalUtente99
			Calculate Sum(pvp4_fee) For tlote = "99" To lctotalIncentivo99

			** c�digo entidade
			id_anf = Replicate('0',3-Len(Alltrim(id_anf))) + Alltrim(id_anf)
			lcString = ""
			Select ucrsLojas
			Locate For Alltrim(ucrsLojas.site) == Alltrim(mySite)
			If Found()
				lcString = "103" && Vers�o
				lcString = lcString + Replicate('0',6-Len(astr(ucrsLojas.infarmed))) + astr(ucrsLojas.infarmed) && C�digo Farm�cia
				lcString = lcString + Alltrim(id_anf)  && C�digo Entidade
				lcString = lcString + 'E' && S�rie Fatura
				lcString = lcString + Replicate('0',4-Len(astr(ft.fno))) + astr(ft.fno) && N�mero Fatura
				lcString = lcString + Alltrim(uf_gerais_getdate(ft.fdata,"SQL")) && data Fatura
				lcString = lcString + astr(Year(ft.cData)) && ano Fatura��o
				lcString = lcString + Replicate('0',2-Len(astr(Month(ft.cData)))) + astr(Month(ft.cData)) && m�s Fatura��o
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalLotes))) + astr(lcTotalLotes) && Total Lotes
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalReceitas))) + astr(lcTotalReceitas) && Total Receitas
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalEmbalagens))) + astr(lcTotalEmbalagens) && Total Embalagens
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalPVP,9,2),'.',''))) + Strtran(astr(lcTotalPVP,9,2),'.','') && Total PVP
				lcString = lcString + "1" && Total Linhas detalhe IVA (PARA JA APENAS TEM PRODUTOS A 6%)
				lcString = lcString + "006" && TAXAS de IVA fixo (PARA JA APENAS TEM PRODUTOS A 6%)
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalIva,9,2),'.',''))) + Strtran(astr(lcTotalIva,9,2),'.','') && Total dos ivas
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalUtente,9,2),'.',''))) + Strtran(astr(lcTotalUtente,9,2),'.','') && Total Utente
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalEntidade,9,2),'.',''))) + Strtran(astr(lcTotalEntidade,9,2),'.','') && Total Entidade
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcValorProtocolo,9,2),'.',''))) + Strtran(astr(lcValorProtocolo,9,2),'.','') && valores Protocolo / o que �
				lcString = lcString + "01" && numero de sub-sistema

				lcString = lcString + Alltrim(id_anf)  && sub-sistema
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalLotes))) + astr(lcTotalLotes) && n� Lotes (como temos apenas um subsistema � o mesmo do total)
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalReceitas))) + astr(lcTotalReceitas) && n� Receitas (como temos apenas um subsistema � o mesmo do total)
				lcString = lcString + Replicate('0',6-Len(astr(lcTotalEmbalagens))) + astr(lcTotalEmbalagens) && n� Embalagens	(como temos apenas um subsistema � o mesmo do total)
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalPVP,9,2),'.',''))) + Strtran(astr(lcTotalPVP,9,2),'.','') && Total PVP
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalUtente,9,2),'.',''))) + Strtran(astr(lcTotalUtente,9,2),'.','') && Total Utente
				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcTotalEntidade,9,2),'.',''))) + Strtran(astr(lcTotalEntidade,9,2),'.','') && Total Entidade

				lcString = lcString + Replicate('0',10-Len(Strtran(astr(lcValor3Protocolo,9,2),'.',''))) + Strtran(astr(lcValor3Protocolo,9,2),'.','') && valores do 3� protocolo
			Endif
	Endcase

	If Empty(lcString) &&
		Return .F.
	Endif


	If Directory(Alltrim(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsPrint')
		lcFilePath = Alltrim(Alltrim(uf_gerais_getParameter("ADM0000000015","TEXT")) + "\Cache\" + Alltrim(lcFtstamp) + ".png")

		myFilePathDataMatrix = lcFilePath && variavel publica usada no IDU



		If !File(Alltrim(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsPrint\ltsPrint.jar')

			uf_perguntalt_chama("Ficheiro configura��o de Datamatrix n�o encontrado. Contacte o Suporte","OK","",64)
			Return .F.
		Endif

		lcWsPath = Alltrim(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsPrint\ltsPrint.jar "datamatrix" "' + Alltrim(lcFilePath) + '" "' + Alltrim(lcString) + '"'
		lcWsPath = "javaw -jar " + lcWsPath

		oWSShell = Createobject("WScript.Shell")
		oWSShell.Run(lcWsPath, 0, .F.)
	Endif

	Wait Window 'A criar codigo datamatrix...' Timeout 2 && Tempo para permitir a gera��o da imagem na cache

Endfunc

Function uf_gerais_criaCursoresDataMatrix_Entidades
	Lparameters lcCptorgstamp , lcU_fistamp , lcNdoc


	*************************
	**** cria cursor cptorg **********
	If Used("uCrsCptorg")
		fecha("uCrsCptorg")
	Endif
	Create Cursor uCrsCptorg(id_anf c(10),abrev c(15) ,e_datamatrix bit)

	lcSql = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		SELECT TOP 1
				id_anf,
				abrev,
				e_datamatrix
		FROM cptorg (nolock)
		where cptorgstamp='<<LcCptorgstamp>>' and inactivo =0
		ORDER BY cptorgstamp DESC
	ENDTEXT
	If !uf_gerais_actGrelha("","uCrsCptorg", lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia a verificar o codigo da organiza��o. Por favor contacte o suporte.", "OK", "", 16)
		Return .F.
	Endif

	*************************
	**** cria cursor FT e uCrsFt **********
	lcSql = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		SELECT
			FT.* , ft2.id_efr_externa
		FROM ctltrct CT (nolock)
		INNER JOIN FI 	(nolock) ON CT.u_fistamp = FI.bistamp
		INNER JOIN FT 	(nolock) ON FI.ftstamp = FT.ftstamp
		LEFT  JOIN FT2	(nolock) ON ft.ftstamp = ft2.ft2stamp
		WHERE CT.u_fistamp = '<<lcU_fistamp >>'
	ENDTEXT
	If !uf_gerais_actGrelha("","FT", lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia a preencher a FT. Por favor contacte o suporte.", "OK", "", 16)
		Return .F.
	Endif
	If !uf_gerais_actGrelha("","uCrsFt", lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia a preencher a uCrsFt. Por favor contacte o suporte.", "OK", "", 16)
		Return .F.
	Endif


	*************************
	**** cria cursor FI  **********
	lcSql = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		SELECT *
		FROM FI WHERE ftstamp ='<<uCrsFt.ftstamp>>'
	ENDTEXT
	If !uf_gerais_actGrelha("","uCrsFi", lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia a preencher a uCrsFi. Por favor contacte o suporte.", "OK", "", 16)
		Return .F.
	Endif

	*************************
	**** cria cursor ucrsTd  **********
	lcSql = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		SELECT u_tipodoc
		FROM TD WHERE ndoc=<<lcNdoc>>
	ENDTEXT
	If !uf_gerais_actGrelha("","td", lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia a preencher a uCrsFi. Por favor contacte o suporte.", "OK", "", 16)
		Return .F.
	Endif

	uf_imprimirgerais_dataMatrixCriaImagem_Entidades(lcNdoc, td.u_tipodoc,uCrsCptorg.id_anf)
Endfunc

Function uf_imprimirgerais_sendprinter_talao
	Lparameters uv_id, uv_reImp, uv_prev, uv_slip, uv_noCheck



	If uf_gerais_getUmValor("B_terminal", "forcaPrevImp", "terminal = '" + Alltrim(myTerm) + "'")
		uv_prev = .T.
	Endif

	If Empty(uv_id)
		uf_perguntalt_chama("N�O FOI POSSIVEL ENCONTRAR A IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif

	If Used("ucrsImpIdusDefault")
		fecha("ucrsImpIdusDefault")
	Endif

	uv_mostraReimp = uf_gerais_getParameter("ADM0000000209", "BOOL")

	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select TOP 1
            tabela, nomeImpressao, nomeficheiro,
			temduplicados, numduplicados, impressoradefeito
		From
			b_impressoes (nolock)
		where
			id = <<IIF(TYPE("uv_id") = 'N', ASTR(uv_id), ALLTRIM(uv_id))>>
		ORDER BY
			idupordefeito DESC
	ENDTEXT

	_Cliptext = lcSql
	If !uf_gerais_actGrelha("","ucrsImpIdusDefault",lcSql)
		uf_perguntalt_chama("N�O FOI POSSIVEL ENCONTRAR A CONFIGURA��O DE IMPRESS�ES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif

	lcLayout = Alltrim(ucrsImpIdusDefault.nomeficheiro)
	lctabela = Alltrim(ucrsImpIdusDefault.tabela)
	lcNomeImp = Alltrim(ucrsImpIdusDefault.nomeimpressao)
	lcNomeImpDefeito = Alltrim(ucrsImpIdusDefault.impressoradefeito)

	Local lcBool, lcSemPerguntas, lcModal, lcDocumentoDefault
	Public MYIMPRESSAOORIGEM

	Local  lcDocumento, lcEmailEnvio
	lcDocumento = ''
	lcEmailEnvio = ''

	MYIMPRESSAOORIGEM = lcBool

	If Used("ucrsImpIdus")
		fecha("ucrsImpIdus")
	Endif

	** Preenche Combo Impressoras **
	Dimension laPrinters[1,1]
	=Aprinters(laPrinters)

	lcprinter = ''

	If !uf_gerais_getUmValor("B_terminal", "nUsaTSPrint", "terminal = '" + Alltrim(myTerm) + "'")
		uv_sessID = Right("000" + Right(Alltrim(Sys(2023)),Len(Sys(2023)) - Rat('\',Sys(2023))), 3)
	Else
		uv_sessID = Right(Alltrim(Sys(2023)),Len(Sys(2023)) - Rat('\',Sys(2023)))
	Endif

	If Alltrim(Upper(lcNomeImp)) == "TAL�O VERSO RECEITA"

		If Type("uv_slip") = "C" And uv_slip = "1"
			If !uf_gerais_getUmValor("B_terminal", "nUsaTSPrint", "terminal = '" + Alltrim(myTerm) + "'")
				uv_reportPrint = "TALOES #" + Alltrim(uv_sessID)
			Else

				uv_reportPrint = "TALOES #" + Alltrim(uv_sessID)

				If !uf_gerais_showActivePrinters(uv_reportPrint)
					uv_reportPrint = "TALOES (redirected " + Alltrim(uv_sessID) + ")"
				Endif
			Endif
		Else
			If !uf_gerais_getUmValor("B_terminal", "nUsaTSPrint", "terminal = '" + Alltrim(myTerm) + "'")
				uv_reportPrint = Alltrim(lcNomeImpDefeito) + " #" + Alltrim(uv_sessID)
			Else

				uv_reportPrint = Alltrim(lcNomeImpDefeito) + " #" + Alltrim(uv_sessID)

				If !uf_gerais_showActivePrinters(uv_reportPrint)
					uv_reportPrint = Alltrim(lcNomeImpDefeito) + " (redirected " + Alltrim(uv_sessID) + ")"
				Endif

			Endif
		Endif

	Else

		If !uf_gerais_getUmValor("B_terminal", "nUsaTSPrint", "terminal = '" + Alltrim(myTerm) + "'")
			uv_reportPrint = Alltrim(lcNomeImpDefeito) + " #" + Alltrim(uv_sessID)
		Else

			uv_reportPrint = Alltrim(lcNomeImpDefeito) + " #" + Alltrim(uv_sessID)

			If !uf_gerais_showActivePrinters(uv_reportPrint)
				uv_reportPrint = Alltrim(lcNomeImpDefeito) + " (redirected " + Alltrim(uv_sessID) + ")"
			Endif

		Endif
	Endif


	For lnIndex = 1 To Alen(laPrinters,1)
		If  !Empty(laPrinters(lnIndex,1)) And At(Upper(Alltrim(uv_reportPrint)), Upper(Alltrim(laPrinters(lnIndex,1))))>0
			lcprinter = Upper(Alltrim(laPrinters(lnIndex,1)))
			lcPort=Alltrim(laPrinters(lnIndex,2))
		Endif
	Endfor

	If Empty(lcprinter)
		If !uf_gerais_getUmValor("B_terminal", "nUsaTSPrint", "terminal = '" + Alltrim(myTerm) + "'")
			lcprinter = "TALOES #" + Alltrim(uv_sessID)
		Else

			lcprinter = "TALOES #" + Alltrim(uv_sessID)

			If !uf_gerais_showActivePrinters(lcprinter)
				lcprinter = "TALOES (redirected " + Alltrim(uv_sessID) + ")"
			Endif

		Endif
	Endif



	Local lcDefaultPrinter
	Local lcValErroPrinter

	Try
		lcDefaultPrinter=Set("printer",3)
	Catch
		uf_perguntalt_chama("Foi detectado algum problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
		lcValErroPrinter = .T.
	Endtry

	If lcValErroPrinter
		Return .F.
	Endif

	If !Used("uCrsE1")
		uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR OS DADOS GERAIS DA EMPRESA. CONTACTE O SUPORTE.", "OK", "", 16)
		Return .F.
	Endif
	**

	** Guardar report a imprimir - ficheiro **
	lcreport = Alltrim(myPath)+"\analises\" + Alltrim(lcLayout)

	If Alltrim(myVersaoDoc) = 'ORIGINAL'
		myVersaoDoc = uf_gerais_versaodocumento(1)
	Endif

	Set Procedure To Locfile("FoxBarcodeQR.prg") Additive
	*--- Create FoxBarcodeQR private object
	Private poFbc
	m.poFbc = Createobject("FoxBarcodeQR")

	*-- New properties
	m.poFbc.nCorrectionLevel = 3 && Level_H
	m.poFbc.nBarColor = Rgb(0, 0, 0) && black

	Create Cursor TempQR (TempQR I)
	Insert Into TempQR Values (0)

	**
	** preparar impressora **
	If !uv_prev
		Try
			Set Printer To Name (lcprinter)
		Catch
			If Inlist(Lower(Alltrim(sql_db)), 'ltstag', 'ltdev30', 'ltdevint')
				**uv_prev = .T.
				lcValErroPrinter = .T.
			Else
				uf_perguntalt_chama("Foi detectado algum problema com a impressora." + Chr(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
				lcValErroPrinter = .T.
			Endif
		Endtry
		If lcValErroPrinter == .T.
			Return .F.
		Endif
	Endif

	Public upv_printNomeSoftware
	upv_printNomeSoftware = uf_gerais_getParameter("ADM0000000113","BOOL")


	Public upv_printCapSocial
	upv_printCapSocial = uf_gerais_getParameter("ADM0000000124","BOOL")

	If Type("upv_moeda") = "U"
		Public upv_moeda
	Endif

	upv_moeda = Alltrim(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))

	Public upv_impDep, upv_hasNC

	upv_impDep = uf_gerais_getParameter_site('ADM0000000123', 'BOOL', mySite)
	upv_hasNC = .F.


	Set Device To Print
	Set Console Off

	** Colocar cursores linhas no inicio **
	Select ucrse1
	Go Top

	Do Case
		Case Alltrim(Upper(lcNomeImp)) == "TAL�O ATENDIMENTO" Or uf_gerais_compstr(lcNomeImp, "Talao Oferta")

			Public upv_prazVdSusp, upv_printRes, upv_printSusp, upv_printCC, upv_Res, upv_Susp, upv_CC
			upv_prazVdSusp = ""
			upv_Res = ""
			upv_Susp = ""
			upv_CC = ""
			upv_printRes = .F.
			upv_printSusp = .F.
			upv_printCC = .F.

			If uf_gerais_getParameter("ADM0000000014","BOOL")
				upv_prazVdSusp = astr(uf_gerais_getParameter("ADM0000000014","NUM"))
			Endif

			If Used("FTprint") And Used("FIprint")
				Select FTprint
				Go Top

				If uf_gerais_getParameter_site("ADM0000000156", "BOOL", mySite) And FTprint.no <> 200
					upv_printRes = .T.
					upv_Res = uf_gerais_getUmValor("bo", "count(*)", "ndos = 5 and fechada = 0 and no = " + astr(FTprint.no) + " and estab = " + astr(FTprint.estab))
				Endif

				If uf_gerais_getParameter_site("ADM0000000157", "BOOL", mySite) And FTprint.no <> 200
					upv_printSusp = .T.
					upv_Susp = uf_gerais_getUmValor("ft", "count(*)", "cobrado = 1 and no = " + astr(FTprint.no) + " and estab = " + astr(FTprint.estab) + " and not exists (select 1 from fi(nolock) where ofistamp in (select fistamp from fi(nolock) as ofi where ofi.ftstamp = ft.ftstamp))")
				Endif

				If uf_gerais_getParameter_site("ADM0000000158", "BOOL", mySite) And FTprint.no <> 200
					upv_printCC = .T.
					uf_gerais_actGrelha("","uc_cc", "exec up_clientes_cc_nreg " + astr(FTprint.no) + ", " + astr(FTprint.estab) + ", 1, 3, '" + mySite + "'")

					Select uc_cc
					Calculate Sum(uc_cc.edeb) To lcTotEdeb
					Calculate Sum(uc_cc.ecred) To lcTotEcred
					Calculate Sum(uc_cc.edebf) To lcTotEdebf
					Calculate Sum(uc_cc.ecredf) To lcTotEcredf

					upv_CC = Round((lcTotEdeb-lcTotEdebf) - (lcTotEcred-lcTotEcredf),2)

				Endif

				Select FTprint
				Go Top

				Select FIprint
				Go Top
				Locate For Alltrim(FIprint.T) == 'NC'
				If Found()
					upv_hasNC = .T.
				Endif

				Select FIprint
				Go Top

			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

		Case Alltrim(Upper(lcNomeImp)) == "TAL�O ATENDIMENTO RESERVA"
			If Used("Reservaprint")
				Select Reservaprint
				Go Top

			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

		Case Alltrim(Upper(lcNomeImp)) == "TAL�O RESUMOS"

			If Used("ucrsselvendaimpbulk")
				Select ucrsselvendaimpbulk
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

		Case Alltrim(Upper(lcNomeImp)) == "TAL�O FECHO CAIXA"

			Public upv_printTotCred, upv_printTotCompart

			If uf_gerais_getParameter_site("ADM0000000159", "BOOL", mySite)
				upv_printTotCompart = .T.
			Endif

			If uf_gerais_getParameter_site("ADM0000000160", "BOOL", mySite)
				upv_printTotCred = .T.
			Endif

			If Used("ucrsdtfcx")
				Select ucrsdtfcx
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

		Case Alltrim(Upper(lcNomeImp)) == "TAL�O MOVIMENTO CAIXA"

			If Used("uc_PrintMovCX")
				Select uc_PrintMovCX

			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

		Case Alltrim(Upper(lcNomeImp)) == "TAL�O VALE CLIENTE"

			If Used("uCrsFidelValesPendentes_Tmp") And Type("upv_moeda") <> "U" And Type("upv_validade") <> "U" And Type("upv_intrans") <> "U"
				Select uCrsFidelValesPendentes_Tmp
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

		Case Alltrim(Upper(lcNomeImp)) == "TAL�O RECIBO CLIENTE"

			If Used("uCrsRe")
				Select uCrsRe
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

			If Used("ucrsRodapeCert")
				Select ucrsRodapeCert
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

			If Used("uCrsRl")
				Select uCrsRl
				Go Top

			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

		Case Alltrim(Upper(lcNomeImp)) == "TAL�O RECIBO CLIENTE PRODUTOS"

			If Used("uCrsRe")
				Select uCrsRe
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

			If Used("uCrsFi2LregTMP")
				Select uCrsFi2LregTMP
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

		Case Alltrim(Upper(lcNomeImp)) == "TAL�O HIST�RICO UTENTE"

			If Used("ucrsprodvendidos")
				Select ucrsprodvendidos
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

		Case Alltrim(Upper(lcNomeImp)) == "TAL�O REULARIZA��ES"

			If Used("ucrsregvendas")
				Select ucrsregvendas
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

			If Used("crsprintres")
				Select crsprintres
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

		Case Alltrim(Upper(lcNomeImp)) == "TAL�O REGISTO CR�DITOS"

			If Used("ucrsregvendas")
				Select ucrsregvendas
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

		Case Alltrim(Upper(lcNomeImp)) == "TAL�O SOMAR ATENDIMENTOS"

			If Used("ucrssomaratendimentosprint")
				Select ucrssomaratendimentosprint
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

		Case Alltrim(Upper(lcNomeImp)) == "TAL�O GUIA DE AVIAMENTO"

			If Used("ucrsft")
				Select uCrsFt
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

			If Used("ucrsft2")
				Select uCrsFt2
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

			If Used("ucrsfi")
				Select uCrsFi
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

		Case Alltrim(Upper(lcNomeImp)) == "TAL�O PSICOTR�PICOS"

			If Used("uCrsReImpPsico")
				Select uCrsReImpPsico
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

		Case Alltrim(Upper(lcNomeImp)) == "TAL�O GUIA DEM"

			If Used("ucrsverificarespostadem_tot")
				Select ucrsverificarespostadem_tot
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

		Case Alltrim(Upper(lcNomeImp)) == "TAL�O CONTROLO SAXENDA"

			If Used("fi_nnordiskTMP")
				Select fi_nnordiskTMP
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

		Case Alltrim(Upper(lcNomeImp)) == "TAL�O MEDICAMENTOS"

			If Used("ucrsreceitas")
				Select ucrsreceitas
				Go Top

			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

			If Used("ucrsdemndisp")
				Select ucrsdemndisp
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

		Case Alltrim(Upper(lcNomeImp)) == "TAL�O VERSO RECEITA"

			If Used("T")
				Select T
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

			If Used("T2")
				Select T2
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

			If Used("L")
				Select L
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

			If Used("R")
				Select R
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

			If Used("S")
				Select S
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

			If Used("uCrsl1")
				Select uCrsl1
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

		Case Alltrim(Upper(lcNomeImp)) == "TAL�O VALE CLIENTE ATENDIMENTO"

		Case Alltrim(Upper(lcNomeImp)) == "TAL�O N�MERO ATENDIMENTO"

			If Used("uc_totAtendPrint")
				Select uc_totAtendPrint
				Go Top
			Else
				uf_perguntalt_chama("ERRO A CARREGAR DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

		Otherwise
			If !uv_noCheck
				uf_perguntalt_chama("N�O FOI POSSIVEL ENCONTRAR A CONFIGURA��O DE IMPRESS�ES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

	Endcase

	Set REPORTBEHAVIOR 90

	If ucrsImpIdusDefault.temduplicados And !uv_reImp

		For I = 1 To ucrsImpIdusDefault.numduplicados

			myVersaoDoc = uf_gerais_versaodocumento(I)

			If !uv_prev

				Report Form Alltrim(lcreport) To Printer Noconsole Nodialog Nowait

			Else

				Report Form Alltrim(lcreport) Preview Noconsole

			Endif

		Endfor

	Else

		If !uv_mostraReimp And Alltrim(Upper(lcNomeImp)) == "TAL�O ATENDIMENTO"
			myVersaoDoc = Iif(uv_reImp, '', 'ORIGINAL')
		Else
			myVersaoDoc = Iif(uv_reImp, '2� VIA', 'ORIGINAL')
		Endif

		If !uv_prev

			Report Form Alltrim(lcreport) To Printer Noconsole

		Else
			Report Form Alltrim(lcreport) Preview Noconsole

		Endif

	Endif

	Set REPORTBEHAVIOR 80

	Use In TempQR
	m.poFbc = Null

	Set Device To Screen
	Set Printer To Name ''+lcDefaultPrinter+''

Endfunc

Function uf_imprimirgerais_createCursTalao
	Lparameters uv_id, uv_stampCab, uv_stampLin, uv_numAtend, uv_ecra, uv_printType, uv_vdSusp
	If Empty(uv_id)
		Return .F.
	Endif

	uv_impressao = uf_gerais_getUmValor("B_IMPRESSOES", "nomeimpressao", "id = '" + Iif(Type("uv_id") = 'N', astr(uv_id), Alltrim(uv_id)) + "'")
	Do Case
		Case uf_gerais_compstr(uv_impressao, "TALAO OFERTA")
			If !uf_gerais_actGrelha("","FTprint","exec up_print_ft '"+Alltrim(uv_stampCab)+"', '', " + Iif(uv_vdSusp, "1", "0") + ", 1")
				Return .F.
			Endif
			If !uf_gerais_actGrelha("","FIprint","exec up_print_fi '"+Alltrim(uv_stampCab)+"', '', " + Iif(uv_vdSusp, "1", "0") + ", 1")
				Return .F.
			Endif
			If !uf_gerais_actGrelha("","DocListprint","exec up_print_docs_atendimento '"+Alltrim(uv_stampCab)+"', '', ''")
				Return .F.
			Endif
			If !uf_gerais_actGrelha("","DocPagprint","exec up_print_pagamentos 'XXX', 0")
				Return .F.
			Endif
			If !uf_gerais_actGrelha("","DOCPRINTRODAPE","exec up_print_rodape '" + mySite + "' ")
				Return .F.
			Endif

		Case Alltrim(Upper(uv_impressao)) == "TAL�O ATENDIMENTO" And Alltrim(Upper(uv_ecra)) = "RESUMOS"
			If Upper(mypaisconfsoftw)=='PORTUGAL'
				If !uf_gerais_actGrelha("","FTprint","exec up_print_ft '"+Alltrim(uv_stampCab)+"', '', " + Iif(uv_vdSusp, "1", "0") + ", 1")
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","FIprint","exec up_print_fi '"+Alltrim(uv_stampCab)+"', '', " + Iif(uv_vdSusp, "1", "0") + ", 1")
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","DocListprint","exec up_print_docs_atendimento '"+Alltrim(uv_stampCab)+"', '', ''")
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","DocPagprint","exec up_print_pagamentos 'XXX', 0")
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","DOCPRINTRODAPE","exec up_print_rodape '" + mySite + "' ")
					Return .F.
				Endif

			Else

				If !uf_gerais_actGrelha("","FTprint","exec up_print_ft '"+Alltrim(uv_stampCab)+"', '', " + Iif(uv_vdSusp, "1", "0") + ", 1")
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","FIprint","exec up_print_fi '"+Alltrim(uv_stampCab)+"', '', " + Iif(uv_vdSusp, "1", "0") + ", 1")
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","DocListprint","exec up_print_docs_atendimento '"+Alltrim(uv_stampCab)+"', '" + Alltrim(uv_numAtend) + "', ''")
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","DocPagprint","exec up_print_pagamentos 'XXX', 0")
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","DOCPRINTRODAPE","exec up_print_rodape '" + mySite + "' ")
					Return .F.
				Endif

			Endif

		Case Alltrim(Upper(uv_impressao)) == "TAL�O ATENDIMENTO" And Alltrim(Upper(uv_ecra)) == "REIMPPOS"

			uv_nc = 0

			If Round(uf_gerais_getParameter("ADM0000000016","NUM"),0) <> 0
				uv_nc = 1
			Endif

			If Upper(mypaisconfsoftw)=='PORTUGAL'

				If !uf_gerais_actGrelha("","FTprint","exec up_print_ft '"+Alltrim(uv_stampCab)+"', '" + Alltrim(uv_numAtend) + "', " + Iif(uv_vdSusp, "1", "0") + ", 0, " + astr(uv_nc))
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","FIprint","exec up_print_fi '"+Alltrim(uv_stampCab)+"', '" + Alltrim(uv_numAtend) + "', " + Iif(uv_vdSusp, "1", "0")+ ", 0, " + astr(uv_nc))

					Return .F.
				Endif
				If !uf_gerais_actGrelha("","DocListprint","exec up_print_docs_atendimento '"+Alltrim(uv_stampCab)+"', '" + Alltrim(uv_numAtend) + "', '', " + astr(uv_nc))
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","DocPagprint","exec up_print_pagamentos '" + Alltrim(uv_numAtend) + "', " + astr(uv_printType))
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","DOCPRINTRODAPE","exec up_print_rodape '" + mySite + "' ")
					Return .F.
				Endif

			Else

				If !uf_gerais_actGrelha("","FTprint","exec up_print_ft '"+Alltrim(uv_stampCab)+"', '" + Alltrim(uv_numAtend) + "', " + Iif(uv_vdSusp, "1", "0") + ", 0, " + astr(uv_nc))
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","FIprint","exec up_print_fi '"+Alltrim(uv_stampCab)+"', '" + Alltrim(uv_numAtend) + "', " + Iif(uv_vdSusp, "1", "0") + ", 0, " + astr(uv_nc))
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","DocListprint","exec up_print_docs_atendimento '"+Alltrim(uv_stampCab)+"', '" + Alltrim(uv_numAtend) + "', '', " + astr(uv_nc))
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","DocPagprint","exec up_print_pagamentos '" + Alltrim(uv_numAtend) + "', " + astr(uv_printType))
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","DOCPRINTRODAPE","exec up_print_rodape '" + mySite + "' ")
					Return .F.
				Endif

			Endif

		Case Alltrim(Upper(uv_impressao)) == "TAL�O ATENDIMENTO" And Alltrim(Upper(uv_ecra)) == "REIMPPOSRES"

			uv_nc = 0

			If Round(uf_gerais_getParameter("ADM0000000016","NUM"),0) <> 0
				uv_nc = 1
			Endif

			If Upper(mypaisconfsoftw)=='PORTUGAL'

				If !uf_gerais_actGrelha("","FTprint","exec up_print_ft '"+Alltrim(uv_stampCab)+"', '', " + Iif(uv_vdSusp, "1", "0") + ', 1, ' + astr(uv_nc))
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","FIprint","exec up_print_fi '"+Alltrim(uv_stampCab)+"', '', " + Iif(uv_vdSusp, "1", "0") + ', 1, ' + astr(uv_nc))
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","DocListprint","exec up_print_docs_atendimento '"+Alltrim(uv_stampCab)+"', '" + Alltrim(uv_numAtend) + "', '', " + astr(uv_nc))
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","DocPagprint","exec up_print_pagamentos '" + Alltrim(uv_numAtend) + "', " + astr(uv_printType))
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","DOCPRINTRODAPE","exec up_print_rodape '" + mySite + "' ")
					Return .F.
				Endif

			Else

				If !uf_gerais_actGrelha("","FTprint","exec up_print_ft '"+Alltrim(uv_stampCab)+"', '', " + Iif(uv_vdSusp, "1", "0") + ', 1, ' + astr(uv_nc))
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","FIprint","exec up_print_fi '"+Alltrim(uv_stampCab)+"', '', " + Iif(uv_vdSusp, "1", "0") + ', 1, ' + astr(uv_nc))
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","DocListprint","exec up_print_docs_atendimento '"+Alltrim(uv_stampCab)+"', '" + Alltrim(uv_numAtend) + "', '', " + astr(uv_nc))
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","DocPagprint","exec up_print_pagamentos '" + Alltrim(uv_numAtend) + "', " + astr(uv_printType))
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","DOCPRINTRODAPE","exec up_print_rodape '" + mySite + "' ")
					Return .F.
				Endif

			Endif

		Case Alltrim(Upper(uv_impressao)) == "TAL�O ATENDIMENTO" And Alltrim(Upper(uv_ecra)) == "REIMPPOSSUSP"

			If Upper(mypaisconfsoftw)=='PORTUGAL'

				If !uf_gerais_actGrelha("","FTprint","exec up_print_ft '"+Alltrim(uv_stampCab)+"', '', " + Iif(uv_vdSusp, "1", "0"))
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","FIprint","exec up_print_fi '"+Alltrim(uv_stampCab)+"', '', " + Iif(uv_vdSusp, "1", "0"))
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","DocListprint","exec up_print_docs_atendimento '"+Alltrim(uv_stampCab)+"', '" + Alltrim(uv_numAtend) + "', ''")
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","DocPagprint","exec up_print_pagamentos '" + Alltrim(uv_numAtend) + "'")
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","DOCPRINTRODAPE","exec up_print_rodape '" + mySite + "' ")
					Return .F.
				Endif

			Else

				If !uf_gerais_actGrelha("","FTprint","exec up_print_ft '"+Alltrim(uv_stampCab)+"', '', " + Iif(uv_vdSusp, "1", "0"))
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","FIprint","exec up_print_fi '"+Alltrim(uv_stampCab)+"', '', " + Iif(uv_vdSusp, "1", "0"))
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","DocListprint","exec up_print_docs_atendimento '"+Alltrim(uv_stampCab)+"', '" + Alltrim(uv_numAtend) + "', ''")
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","DocPagprint","exec up_print_pagamentos '" + Alltrim(uv_numAtend) + "'")
					Return .F.
				Endif
				If !uf_gerais_actGrelha("","DOCPRINTRODAPE","exec up_print_rodape '" + mySite + "' ")
					Return .F.
				Endif

			Endif

		Case Alltrim(Upper(uv_impressao)) == "TAL�O RECIBO CLIENTE" And Alltrim(Upper(uv_ecra)) = "REGVENDAS"


			If !uf_gerais_actGrelha("","uCrsRe","select * from re (nolock) where restamp='" + Alltrim(uv_stampCab) + "'",)
				Return .F.
			Endif

			If !uf_gerais_actGrelha("","uCrsRl","select CDESC, NRDOC, datalc, EVAL, EREC, LORDEM, RESTAMP, RLSTAMP from rl (nolock) where restamp='" + Alltrim(uv_stampCab) + "' order by datalc, cm, nrdoc")
				Return .F.
			Endif

		Case Alltrim(Upper(uv_impressao)) == "TAL�O RECIBO CLIENTE PRODUTOS" And Alltrim(Upper(uv_ecra)) = "REGVENDAS"

			If !uf_gerais_actGrelha("","uCrsRe","select * from re (nolock) where restamp='" + Alltrim(uv_stampCab) + "'",)
				Return .F.
			Endif

			If !uf_gerais_actGrelha("","uCrsRlTMP","select CDESC, NRDOC, datalc, EVAL, EREC, LORDEM, RESTAMP, RLSTAMP from rl (nolock) where restamp='" + Alltrim(uv_stampCab) + "' order by datalc, cm, nrdoc")
				Return .F.
			Endif

			Create Cursor uCrsFi2LregTMP (Design c(60), qtt N(5), rlstamp c(25), cdesc c(20), nrdoc N(10), datalc d, Eval N(19,6), erec N(19,6), restamp c(25))

			If Type("uv_PrintType") = "N" And Used("uCrsFi2Lreg")
				If Inlist(uv_printType,901,904)
					Select uCrsRlTMP
					Go Top
					Scan
						Select uCrsFi2Lreg
						Go Top
						Scan For uCrsFi2Lreg.fno = uCrsRlTMP.nrdoc And Alltrim(Upper(uCrsFi2Lreg.Nmdoc)) = Alltrim(Upper(uCrsRlTMP.cdesc))

							Select uCrsFi2LregTMP
							Append Blank

							Replace uCrsFi2LregTMP.Design With uCrsFi2Lreg.Design
							Replace uCrsFi2LregTMP.qtt With uCrsFi2Lreg.qtt
							Replace uCrsFi2LregTMP.rlstamp With uCrsRlTMP.rlstamp
							Replace uCrsFi2LregTMP.cdesc With uCrsRlTMP.cdesc
							Replace uCrsFi2LregTMP.nrdoc With uCrsRlTMP.nrdoc
							Replace uCrsFi2LregTMP.datalc With uCrsRlTMP.datalc
							Replace uCrsFi2LregTMP.Eval With uCrsRlTMP.Eval
							Replace uCrsFi2LregTMP.erec With uCrsRlTMP.erec
							Replace uCrsFi2LregTMP.restamp With uCrsRlTMP.restamp

							Select uCrsFi2Lreg
						Endscan

						Select uCrsRlTMP
					Endscan
				Endif
			Endif


		Case Alltrim(Upper(uv_impressao)) == "TAL�O PSICOTR�PICOS"

			If !uf_gerais_actGrelha("",[uCrsReImpPsico],[exec up_gerais_psico_talao ']+Alltrim(uv_stampCab)+['])
				Return .F.
			Endif

		Otherwise
			Return .F.
	Endcase




	Return .T.
Endfunc

Function uf_imprimirgerais_gerarQRCode
	Lparameters uv_mensagem

	**Calcular o comprimento da STRING para enviar como parametro para a impressora
	uv_len = astr(Len(Alltrim(uv_mensagem)) + 3)

	uv_qr = ''

	**Select the QR Code model
	uv_qr = uv_qr + Chr(29) + Chr(40) + Chr(107) + Chr(4) + Chr(0) + Chr(49) + Chr(65) + Chr(50) + Chr(0)
	**Set the QR Code size of module.
	uv_qr = uv_qr + Chr(29) + Chr(40) + Chr(107) + Chr(3) + Chr(0) + Chr(49) + Chr(67) + Chr(6) && Tamanho � o ultimo CHR desta linha
	**Select the QR Code error correction level.
	uv_qr = uv_qr + Chr(29) + Chr(40) + Chr(107) + Chr(3) + Chr(0) + Chr(49) + Chr(69) + Chr(48)
	**Store the QR Code data in the symbol storage area.
	uv_qr = uv_qr + Chr(29) + Chr(40) + Chr(107) + Chr(&uv_len.) + Chr(0) + Chr(49) + Chr(80) + Chr(48)
	**Dados a incluir no QRCode
	uv_qr = uv_qr + uv_mensagem
	**Print the QR Code symbol data in the symbol storage area
	uv_qr = uv_qr + Chr(29) + Chr(40) + Chr(107) + Chr(3) + Chr(0) + Chr(49) + Chr(81) + Chr(48)

	Return uv_qr

Endfunc


** Impress�o tal�o Novo Nordisk
Function uf_talao_parque

	If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")

		Public lcQRCodestring, lcDataHoraparque

		lcQRCodestring = uf_gerais_getParameter_site('ADM0000000155', 'TEXT', mySite) + Right('0'+astr(Month(Date())),2) + Right('0'+astr(Day(Date())),2) + Strtran(Time(),':','')
		lcDataHoraparque = Transform(Date()) + ' ' + Time()

		uv_idImp = uf_gerais_getUmValor("B_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Ticket Parque'")

		uf_imprimirgerais_sendprinter_talao(uv_idImp , .F., .F., "", .T.)

	Endif

Endfunc


Function uf_imprimirGerais_MostraChckCertificar
	Lparameters lcObject

	If Alltrim(Upper(MYIMPRESSAOORIGEM)) <> 'FACTURACAO' Or !uf_gerais_getParameter_site('ADM0000000169', 'bool', mySite)

		lcObject.Visible = .F.

	Endif


	If Alltrim(Upper(MYIMPRESSAOORIGEM)) == 'FACTURACAO' Or uf_gerais_getParameter_site('ADM0000000169', 'bool', mySite)
		Local lcTipoSaft
		lcTipoSaft = ""
		If(Used("td"))
			Select td
			Go Top
			lcTipoSaft = Alltrim(td.tiposaft)
		Endif

		If(Empty(lcTipoSaft ))
			lcObject.Visible = .F.
		Endif
	Endif


	If uf_gerais_compstr(MYIMPRESSAOORIGEM,'Atendimento') And uf_gerais_getParameter_site('ADM0000000169', 'bool', mySite)
		Local lcTipoSaft
		lcTipoSaft = ""
		If(Used("td"))
			Select td
			Go Top
			lcTipoSaft = Alltrim(td.tiposaft)
		Endif

		If !(Empty(lcTipoSaft ))
			IMPRIMIRGERAIS.pageframe1.page1.chkParaPDF.Value = 1
		Endif
	Endif
Endfunc

Function uf_imprimirgerais_prever

	Local uv_curLineFilter, uv_filter, lcCount, lcSQLImpressoes
	Store '' To uv_curLineFilter, lcSQLImpressoes

	Select uCrsImprimirDocs

	TEXT TO lcSQLImpressoes TEXTMERGE NOSHOW
		exec up_gerais_getImpressoes '<<ALLTRIM(uCrsImprimirDocs.documento)>>', '','<<ALLTRIM(uCrsImprimirDocs.nomeimpressao)>>'
	ENDTEXT

	If !uf_gerais_actGrelha("","ucrsConfigImpressoes",lcSQLImpressoes)
		uf_perguntalt_chama("Erro a validar configura��o Impress�o.Por favor contacte o suporte.","OK","",16)
		Return .F.
	Endif

	If !Empty(ucrsConfigImpressoes.lineCursor) And !Empty(ucrsConfigImpressoes.lineFilter)

		uv_filter = Alltrim(ucrsConfigImpressoes.lineFilter)

		Select (Alltrim(ucrsConfigImpressoes.lineCursor))

		uv_curLineFilter = Filter()

		Set Filter To &uv_filter.

		Select (Alltrim(ucrsConfigImpressoes.lineCursor))
		Count To lcCount
		If lcCount = 0
			uf_perguntalt_chama("N�o tem elementos a imprimir.","OK","",16)

			Select (Alltrim(ucrsConfigImpressoes.lineCursor))
			Set Filter To &uv_curLineFilter.

			Return .F.
		Endif

	Endif

	If uf_gerais_compstr(MYIMPRESSAOORIGEM, "Atendimento")
		uf_imprimirgerais_imprimirAtendimentoInit()
		Select ftTempImp
		Go Top
		Scan
			lcStampFtTempImp = ftTempImp.ftstamp

			fecha ("ft")
			fecha ("ft2")
			fecha ("fi")

			lcSql = ''
			TEXT TO lcsql TEXTMERGE NOSHOW
							exec up_touch_ft '<<ALLTRIM(lcStampFtTempImp)>>'
			ENDTEXT
			uf_gerais_actGrelha("", "FT", lcSql)
			lcSql = ''
			TEXT TO lcsql TEXTMERGE NOSHOW
							SELECT respostaDt = isnull((select top 1 DescrResposta from b_dt_resposta WHERE stampDoc = '<<lcStampFtTempImp>>' order by DataEnvio),''),* FROM FT2 (nolock) WHERE ft2stamp='<<ALLTRIM(lcStampFtTempImp)>>'
			ENDTEXT
			uf_gerais_actGrelha("", "FT2", lcSql)
			lcSql = ''
			TEXT TO lcsql TEXTMERGE NOSHOW
							exec up_facturacao_linhas 0, <<mysite_nr>>, '<<lcStampFtTempImp>>'
			ENDTEXT
			uf_gerais_actGrelha("", "FI", lcSql)

			TEXT TO lcSQL TEXTMERGE NOSHOW
			        	exec up_print_qrcode_a4 '<<lcStampFtTempImp>>'
			ENDTEXT
			uf_gerais_actGrelha("", "FTQRCODE", lcSql)

			Select ftTempImp
			lcDocumento = Alltrim(ftTempImp.Nmdoc)
			uf_imprimirGerais_imprimir('prev')

		Endscan
		uf_imprimirgerais_imprimirAtendimentoFim()
	Else
		uf_imprimirGerais_imprimir('prev')
	Endif



	If !Empty(ucrsConfigImpressoes.lineCursor) And !Empty(ucrsConfigImpressoes.lineFilter)

		Select (Alltrim(ucrsConfigImpressoes.lineCursor))

		If Empty(uv_curLineFilter)
			Set Filter To
		Else
			Set Filter To &uv_curLineFilter.
		Endif

	Endif

Endfunc


Function uf_imprimirgerais_imprimirAtendimentoInit
	Public aControlSources
	Dimension aControlSources[atendimento.grdatend.ColumnCount]
	For I = 1 To atendimento.grdatend.ColumnCount
		aControlSources[i] = atendimento.grdatend.Columns[i].ControlSource
	Endfor
	atendimento.grdatend.RecordSource = ""
	Select * From Fi 	Into Cursor fiPrintTemp 	Readwrite
	Select * From ft 	Into Cursor ftPrintTemp 	Readwrite
	Select * From ft2 	Into Cursor ft2PrintTemp 	Readwrite
	fecha("fi")
	fecha("ft")
	fecha("ft2")


	lcSqlFt = ''
	TEXT TO lcSqlFt TEXTMERGE NOSHOW
	 	select * from ft(nolock)
	 	WHERE ft.u_nratend='<<myNrAtend>>'
	ENDTEXT
	_Cliptext= lcSqlFt
	If !uf_gerais_actGrelha("","ftTempImp",lcSqlFt )
		uf_perguntalt_chama("N�O FOI POSSIVEL ENCONTRAR OS DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif



	Local lcValores, lcSeparador
	lcValores = ""
	lcSeparador = ""

	Select ftstamp From ftTempImp Into Cursor tempCursor NOFILTER

	Scan
		lcValores = lcValores + lcSeparador + "'" + Alltrim(ftstamp) + "'"
		lcSeparador = ", "
	Endscan

	lcSqlFt2 = ''
	TEXT TO lcSqlFt2 TEXTMERGE NOSHOW
	 	select * from ft2(nolock)
	 	WHERE ft2.ft2stamp in(<<lcValores>>)
	ENDTEXT

	If !uf_gerais_actGrelha("","ft2TempImp",lcSqlFt2)
		uf_perguntalt_chama("N�O FOI POSSIVEL ENCONTRAR OS DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif



	lcSqlFi = ''
	TEXT TO lcSqlFi TEXTMERGE NOSHOW
		select * from fi(nolock)
		LEFT join fi2(nolock) on fi2.fistamp = fi.fistamp
		WHERE fi.ftstamp in(<<lcValores>>)
	ENDTEXT

	If !uf_gerais_actGrelha("","fiTempImp",lcSqlFi)
		uf_perguntalt_chama("N�O FOI POSSIVEL ENCONTRAR OS DADOS DE IMPRESS�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif
Endfunc


Function uf_imprimirgerais_imprimirAtendimentoFim


	fecha("fiTempImp")
	fecha("ftTempImp")
	fecha("ft2TempImp")
	fecha("fi")
	fecha("ft")
	fecha("ft2")


	Select * From fiPrintTemp Into Cursor Fi Readwrite
	Select * From ftPrintTemp Into Cursor ft Readwrite
	Select * From ft2PrintTemp Into Cursor ft2 Readwrite

	fecha("fiPrintTemp")
	fecha("ftPrintTemp")
	fecha("ft2PrintTemp")

	atendimento.grdatend.RecordSource = "fi"

	For I = 1 To atendimento.grdatend.ColumnCount
		atendimento.grdatend.Columns[i].ControlSource = aControlSources[i]
	Endfor

	uf_atendimento_valoresdefeito()

	Release aControlSources
Endfunc


**
Function uf_imprimirgerais_guardaCursorAnexos
	Lparameters lcTo, lcToEmail, lcSubject, lcBody , lcAtatchment

	If !Used("uc_CursorAttachment")
		Create Cursor uc_CursorAttachment(;
			to c(100), ;
			email c(100), ;
			subject c(253), ;
			body M, ;
			attachment c(253) ;
			)

	Endif

	Select uc_CursorAttachment
	Go Bottom
	Append Blank
	Replace uc_CursorAttachment.To 			With lcTo
	Replace uc_CursorAttachment.email 		With lcToEmail
	Replace uc_CursorAttachment.subject 	With lcSubject
	Replace uc_CursorAttachment.body 		With lcBody
	Replace uc_CursorAttachment.attachment 	With lcAtatchment

Endfunc
