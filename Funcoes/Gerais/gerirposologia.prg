FUNCTION uf_gerirposologia_chama
	LPARAMETERS lcObj, lcTipo, lcPosologia
	
	**
	IF TYPE("GERIRPOSOLOGIA") == "U"
		DO FORM GERIRPOSOLOGIA WITH lcObj, lcTipo, lcPosologia 
		
		IF !EMPTY(GERIRPOSOLOGIA.posologiaOriginal)
			GERIRPOSOLOGIA.Posologia.value = ALLTRIM(GERIRPOSOLOGIA.posologiaOriginal)
		ENDIF
	ELSE
		GERIRPOSOLOGIA.show
		GERIRPOSOLOGIA.refresh
	ENDIF
	
	
	
	GERIRPOSOLOGIA.menu1.estado("", "SHOW", "Aplicar", .t., "Sair", .t.)
ENDFUNC


** Contru��o de Posologia **
FUNCTION uf_gerirposologia_constroiPosologia
	LOCAL lcValue, lcValue2
	STORE '' TO lcValue, lcValue2
	
	WITH GERIRPOSOLOGIA
	
		DO CASE 
			CASE GERIRPOSOLOGIA.ptomar == .t.
				lcValue = "Tomar "
			CASE GERIRPOSOLOGIA.paplicar == .t.
				lcValue = "Aplicar "		
			OTHERWISE
				**
		ENDCASE
	
		
		IF GERIRPOSOLOGIA.pxhoras == .t.
			
			IF !EMPTY(.ContainerAuxVezesDia.horas.value)
				IF .ContainerAuxVezesDia.horas.value > 0 AND .ContainerAuxVezesDia.horas.value < 25
					lcValue = lcValue + 'de ' + ALLTRIM(str(.ContainerAuxVezesDia.horas.value)) + ' em ' + ALLTRIM(str(.ContainerAuxVezesDia.horas.value)) + ' Horas'
					lcvalue = lcValue + ' '
				ENDIF
			ENDIF
			
		ENDIF
		
		IF GERIRPOSOLOGIA.pxaodia == .t.
			
			IF !EMPTY(.ContainerAuxVezesDia.horas.value)
				IF .ContainerAuxVezesDia.horas.value > 0 AND .ContainerAuxVezesDia.horas.value < 25
					lcValue = lcValue + ALLTRIM(str(.ContainerAuxVezesDia.horas.value)) + ' vez(es) ao dia'
					lcvalue = lcValue + ' '
				ENDIF
			ENDIF
			
		ENDIF
		
		IF !EMPTY(.containerauxTomar.durantedias.value) AND .containerauxTomar.visible == .t.
			IF .containerauxTomar.durantedias.value > 0 AND .containerauxTomar.durantedias.value < 100
				lcValue = lcValue + ', durante ' + ALLTRIM(str(.containerauxTomar.durantedias.value)) + ' dias'
				lcvalue = lcValue + ' '
			ENDIF
		ENDIF

		
		IF GERIRPOSOLOGIA.pjejum == .t.
			lcValue = lcValue + ', em jejum'
		ENDIF

		
		DO CASE
			CASE GERIRPOSOLOGIA.pdurante == .t.
				lcValue2 = ', durante o '
			CASE GERIRPOSOLOGIA.pantes == .t.
				lcValue2 = ', antes do '
			CASE GERIRPOSOLOGIA.pdepois == .t. 
				lcValue2 = ', depois do '
			OTHERWISE
				**
		ENDCASE
		
		IF GERIRPOSOLOGIA.ppalmoco == .t. AND (GERIRPOSOLOGIA.pdurante != .f. OR GERIRPOSOLOGIA.pantes != .f. OR GERIRPOSOLOGIA.pdepois != .f.)
			lcValue = lcValue + lcValue2 + 'pequeno almo�o'
		ENDIF
		
		IF GERIRPOSOLOGIA.palmoco == .t. AND (GERIRPOSOLOGIA.pdurante != .f. OR GERIRPOSOLOGIA.pantes != .f. OR GERIRPOSOLOGIA.pdepois != .f.)
			lcValue = lcValue + lcValue2 + 'almo�o'
		ENDIF
		
		IF  GERIRPOSOLOGIA.planche == .t. AND (GERIRPOSOLOGIA.pdurante != .f. OR GERIRPOSOLOGIA.pantes != .f. OR GERIRPOSOLOGIA.pdepois != .f.)
			lcValue = lcValue + lcValue2 + 'lanche'
		ENDIF
	
		IF  GERIRPOSOLOGIA.pjantar == .t. AND (GERIRPOSOLOGIA.pdurante != .f. OR GERIRPOSOLOGIA.pantes != .f. OR GERIRPOSOLOGIA.pdepois != .f.)
			lcValue = lcValue + lcValue2 + 'jantar'
		ENDIF
		
		IF  GERIRPOSOLOGIA.ppalmoco == .t. AND EMPTY(ALLTRIM(lcValue2))
			lcValue = lcValue + ', ao pequeno almo�o'
		ENDIF
		
		IF  GERIRPOSOLOGIA.palmoco == .t.  AND EMPTY(ALLTRIM(lcValue2))
			lcValue = lcValue + ', ao almo�o'
		ENDIF
		
		IF  GERIRPOSOLOGIA.planche == .t.  AND EMPTY(ALLTRIM(lcValue2))
			lcValue = lcValue + ', ao lanche'
		ENDIF
		
		IF  GERIRPOSOLOGIA.pjantar == .t. AND EMPTY(ALLTRIM(lcValue2))
			lcValue = lcValue + ', ao jantar'
		ENDIF
		
			IF  GERIRPOSOLOGIA.pdeitar == .t. 
			lcValue = lcValue + ', ao deitar'
		ENDIF
		
		
		IF GERIRPOSOLOGIA.pfimEmbalagem == .t.
			lcValue = lcValue + ', at� ao fim da embalagem'
		ENDIF
		
		.posologia.value = lcValue
		.posologia.refresh
	ENDWITH


ENDFUNC



FUNCTION uf_gerirposologia_gereSeleccao
	LPARAMETERS Lcnome 

	LOCAL lcObj 
	
	WITH GERIRPOSOLOGIA
		DO CASE 
			CASE lcNome == "Tomar"
				
				lcObj = "GERIRPOSOLOGIA." + Lcnome 
				
				IF GERIRPOSOLOGIA.ptomar == .f.
					&lcObj..BackColor = RGB(42,143,154)
					&lcObj..LABEL1.ForeColor = RGB(255,255,255)
					&lcObj..parent.containerauxTomar.visible = .t.
					GERIRPOSOLOGIA.ptomar = .t.
				ELSE
					&lcObj..BackColor = RGB(230,230,230)
					&lcObj..LABEL1.ForeColor = RGB(0,0,0)
					&lcObj..parent.containerauxTomar.visible = .f.
					GERIRPOSOLOGIA.ptomar = .f.
				ENDIF
				
				IF GERIRPOSOLOGIA.paplicar == .t.
					lcObj2 = "GERIRPOSOLOGIA.Aplicar"
					&lcObj2..BackColor = RGB(230,230,230)
					&lcObj2..LABEL1.ForeColor = RGB(0,0,0)
					GERIRPOSOLOGIA.paplicar = .f.
				ENDIF
				
				&lcObj..LABEL1.refresh
			
			CASE lcNome == "Aplicar"
				
				lcObj = "GERIRPOSOLOGIA." + Lcnome 
				
				IF GERIRPOSOLOGIA.paplicar == .f.
					&lcObj..BackColor = RGB(42,143,154)
					&lcObj..LABEL1.ForeColor = RGB(255,255,255)
					&lcObj..parent.containerauxTomar.visible = .t.
					GERIRPOSOLOGIA.paplicar = .t.
				ELSE
					&lcObj..BackColor = RGB(230,230,230)
					&lcObj..LABEL1.ForeColor = RGB(0,0,0)
					&lcObj..parent.containerauxTomar.visible = .f.
					GERIRPOSOLOGIA.paplicar = .f.
				ENDIF
				
				IF GERIRPOSOLOGIA.ptomar == .t.
					lcObj2 = "GERIRPOSOLOGIA.Tomar"
					&lcObj2..BackColor = RGB(230,230,230)
					&lcObj2..LABEL1.ForeColor = RGB(0,0,0)
					GERIRPOSOLOGIA.ptomar = .f.
				ENDIF
				
				&lcObj..LABEL1.refresh

			CASE lcNome == "FimEmbalagem"
				lcObj = "GERIRPOSOLOGIA." + Lcnome 
				
				IF GERIRPOSOLOGIA.pFimEmbalagem == .f.
					&lcObj..BackColor = RGB(42,143,154)
					&lcObj..LABEL1.ForeColor = RGB(255,255,255)
					GERIRPOSOLOGIA.pFimEmbalagem = .t.
				ELSE
					&lcObj..BackColor = RGB(230,230,230)
					&lcObj..LABEL1.ForeColor = RGB(0,0,0)
					GERIRPOSOLOGIA.pFimEmbalagem = .f.
				ENDIF

				&lcObj..LABEL1.refresh
			
			CASE lcNome == "xemxhoras"
				lcObj = "GERIRPOSOLOGIA." + Lcnome 
				
				IF GERIRPOSOLOGIA.pxhoras == .f.
					&lcObj..BackColor = RGB(42,143,154)
					&lcObj..LABEL1.ForeColor = RGB(255,255,255)
					GERIRPOSOLOGIA.pxhoras = .t.
					.ContainerAuxVezesDia.visible = .t.
					.ContainerAuxVezesDia.label1.caption = "em"
					.ContainerAuxVezesDia.label2.caption = "Horas"
				ELSE
					&lcObj..BackColor = RGB(230,230,230)
					&lcObj..LABEL1.ForeColor = RGB(0,0,0)
					GERIRPOSOLOGIA.pxhoras = .f.
					.ContainerAuxVezesDia.visible = .f.
				ENDIF

				IF GERIRPOSOLOGIA.pxaodia == .t.
					lcObj2 = "GERIRPOSOLOGIA.xaodia"
					&lcObj2..BackColor = RGB(230,230,230)
					&lcObj2..LABEL1.ForeColor = RGB(0,0,0)
					GERIRPOSOLOGIA.pxaodia = .f.
				ENDIF
				
				&lcObj..LABEL1.refresh
			
			CASE lcNome == "xaodia"
				lcObj = "GERIRPOSOLOGIA." + Lcnome 
				
				IF GERIRPOSOLOGIA.pxaodia == .f.
					&lcObj..BackColor = RGB(42,143,154)
					&lcObj..LABEL1.ForeColor = RGB(255,255,255)
					GERIRPOSOLOGIA.pxaodia = .t.
					.ContainerAuxVezesDia.visible = .t.
					.ContainerAuxVezesDia.label1.caption = ""
					.ContainerAuxVezesDia.label2.caption = "ao Dia"
				ELSE
					&lcObj..BackColor = RGB(230,230,230)
					&lcObj..LABEL1.ForeColor = RGB(0,0,0)
					GERIRPOSOLOGIA.pxaodia = .f.
					.ContainerAuxVezesDia.visible = .f.
				ENDIF

				IF GERIRPOSOLOGIA.pxhoras == .t.
					lcObj2 = "GERIRPOSOLOGIA.xemxhoras"
					&lcObj2..BackColor = RGB(230,230,230)
					&lcObj2..LABEL1.ForeColor = RGB(0,0,0)
					GERIRPOSOLOGIA.pxhoras = .f.
				ENDIF

				&lcObj..LABEL1.refresh	
			
			CASE lcNome == "Antes"
				lcObj = "GERIRPOSOLOGIA." + Lcnome 
				
				IF GERIRPOSOLOGIA.pAntes == .f.
					&lcObj..BackColor = RGB(42,143,154)
					&lcObj..LABEL1.ForeColor = RGB(255,255,255)
					GERIRPOSOLOGIA.pAntes = .t.
				ELSE
					&lcObj..BackColor = RGB(230,230,230)
					&lcObj..LABEL1.ForeColor = RGB(0,0,0)
					GERIRPOSOLOGIA.pAntes = .f.
				ENDIF
				
				IF GERIRPOSOLOGIA.pDurante == .t.
					lcObj2 = "GERIRPOSOLOGIA.Durante"
					&lcObj2..BackColor = RGB(230,230,230)
					&lcObj2..LABEL1.ForeColor = RGB(0,0,0)
					GERIRPOSOLOGIA.pDurante = .f.
				ENDIF
				
				IF GERIRPOSOLOGIA.pDepois == .t.
					lcObj2 = "GERIRPOSOLOGIA.Depois"
					&lcObj2..BackColor = RGB(230,230,230)
					&lcObj2..LABEL1.ForeColor = RGB(0,0,0)
					GERIRPOSOLOGIA.pDepois = .f.
				ENDIF

				&lcObj..LABEL1.refresh
				
			CASE lcNome == "Durante"
				lcObj = "GERIRPOSOLOGIA." + Lcnome 
				
				IF GERIRPOSOLOGIA.pDurante == .f.
					&lcObj..BackColor = RGB(42,143,154)
					&lcObj..LABEL1.ForeColor = RGB(255,255,255)
					GERIRPOSOLOGIA.pDurante = .t.
				ELSE
					&lcObj..BackColor = RGB(230,230,230)
					&lcObj..LABEL1.ForeColor = RGB(0,0,0)
					GERIRPOSOLOGIA.pDurante = .f.
				ENDIF

				IF GERIRPOSOLOGIA.pAntes == .t.
					lcObj2 = "GERIRPOSOLOGIA.Antes"
					&lcObj2..BackColor = RGB(230,230,230)
					&lcObj2..LABEL1.ForeColor = RGB(0,0,0)
					GERIRPOSOLOGIA.pAntes = .f.
				ENDIF
				
				IF GERIRPOSOLOGIA.pDepois == .t.
					lcObj2 = "GERIRPOSOLOGIA.Depois"
					&lcObj2..BackColor = RGB(230,230,230)
					&lcObj2..LABEL1.ForeColor = RGB(0,0,0)
					GERIRPOSOLOGIA.pDepois = .f.
				ENDIF


				&lcObj..LABEL1.refresh

			CASE lcNome == "Depois"
				lcObj = "GERIRPOSOLOGIA." + Lcnome 
				
				IF GERIRPOSOLOGIA.pDepois == .f.
					&lcObj..BackColor = RGB(42,143,154)
					&lcObj..LABEL1.ForeColor = RGB(255,255,255)
					GERIRPOSOLOGIA.pDepois = .t.
				ELSE
					&lcObj..BackColor = RGB(230,230,230)
					&lcObj..LABEL1.ForeColor = RGB(0,0,0)
					GERIRPOSOLOGIA.pDepois = .f.
				ENDIF

				IF GERIRPOSOLOGIA.pAntes == .t.
					lcObj2 = "GERIRPOSOLOGIA.Antes"
					&lcObj2..BackColor = RGB(230,230,230)
					&lcObj2..LABEL1.ForeColor = RGB(0,0,0)
					GERIRPOSOLOGIA.pAntes = .f.
				ENDIF
				
				IF GERIRPOSOLOGIA.pDurante == .t.
					lcObj2 = "GERIRPOSOLOGIA.Durante"
					&lcObj2..BackColor = RGB(230,230,230)
					&lcObj2..LABEL1.ForeColor = RGB(0,0,0)
					GERIRPOSOLOGIA.pDurante = .f.
				ENDIF

				&lcObj..LABEL1.refresh	
				
			CASE lcNome == "Jejum"
				lcObj = "GERIRPOSOLOGIA." + Lcnome 
				
				IF GERIRPOSOLOGIA.pJejum == .f.
					&lcObj..BackColor = RGB(42,143,154)
					&lcObj..LABEL1.ForeColor = RGB(255,255,255)
					GERIRPOSOLOGIA.pJejum = .t.
				ELSE
					&lcObj..BackColor = RGB(230,230,230)
					&lcObj..LABEL1.ForeColor = RGB(0,0,0)
					GERIRPOSOLOGIA.pJejum = .f.
				ENDIF

				&lcObj..LABEL1.refresh		
			
			CASE lcNome == "btnpalmoco"
				lcObj = "GERIRPOSOLOGIA." + Lcnome 
	
				IF GERIRPOSOLOGIA.ppalmoco == .f.
					&lcObj..BackColor = RGB(42,143,154)
					&lcObj..LABEL1.ForeColor = RGB(255,255,255)
					GERIRPOSOLOGIA.ppalmoco = .t.
				ELSE
					&lcObj..BackColor = RGB(230,230,230)
					&lcObj..LABEL1.ForeColor = RGB(0,0,0)
					GERIRPOSOLOGIA.ppalmoco = .f.
				ENDIF

				&lcObj..LABEL1.refresh	
			
			CASE lcNome == "lanche"
				lcObj = "GERIRPOSOLOGIA." + Lcnome 
				
				IF GERIRPOSOLOGIA.planche == .f.
					&lcObj..BackColor = RGB(42,143,154)
					&lcObj..LABEL1.ForeColor = RGB(255,255,255)
					GERIRPOSOLOGIA.planche = .t.
				ELSE
					&lcObj..BackColor = RGB(230,230,230)
					&lcObj..LABEL1.ForeColor = RGB(0,0,0)
					GERIRPOSOLOGIA.planche = .f.
				ENDIF

				&lcObj..LABEL1.refresh
						
			CASE lcNome == "almoco"
				lcObj = "GERIRPOSOLOGIA." + Lcnome 
				
				IF GERIRPOSOLOGIA.palmoco == .f.
					&lcObj..BackColor = RGB(42,143,154)
					&lcObj..LABEL1.ForeColor = RGB(255,255,255)
					GERIRPOSOLOGIA.palmoco = .t.
				ELSE
					&lcObj..BackColor = RGB(230,230,230)
					&lcObj..LABEL1.ForeColor = RGB(0,0,0)
					GERIRPOSOLOGIA.palmoco = .f.
				ENDIF

				&lcObj..LABEL1.refresh		
						
			CASE lcNome == "jantar"
				lcObj = "GERIRPOSOLOGIA." + Lcnome 
				
				IF GERIRPOSOLOGIA.pjantar == .f.
					&lcObj..BackColor = RGB(42,143,154)
					&lcObj..LABEL1.ForeColor = RGB(255,255,255)
					GERIRPOSOLOGIA.pjantar = .t.
				ELSE
					&lcObj..BackColor = RGB(230,230,230)
					&lcObj..LABEL1.ForeColor = RGB(0,0,0)
					GERIRPOSOLOGIA.pjantar = .f.
				ENDIF

				&lcObj..LABEL1.refresh	
			
			CASE lcNome == "deitar"
				lcObj = "GERIRPOSOLOGIA." + Lcnome 
				
				IF GERIRPOSOLOGIA.pdeitar == .f.
					&lcObj..BackColor = RGB(42,143,154)
					&lcObj..LABEL1.ForeColor = RGB(255,255,255)
					GERIRPOSOLOGIA.pdeitar = .t.
				ELSE
					&lcObj..BackColor = RGB(230,230,230)
					&lcObj..LABEL1.ForeColor = RGB(0,0,0)
					GERIRPOSOLOGIA.pdeitar = .f.
				ENDIF

				&lcObj..LABEL1.refresh	
						
			OTHERWISE
				**
		ENDCASE
	ENDWITH

	uf_gerirposologia_constroiPosologia()
ENDFUNC


**
FUNCTION uf_gerirposologia_gravar

	IF EMPTY(GERIRPOSOLOGIA.Target)
		RETURN .f.
	ENDIF

	IF UPPER(ALLTRIM(GERIRPOSOLOGIA.Target)) == UPPER(ALLTRIM("utentes.Pageframe1.Page6.ContainerDetalhes.posologia"))
		Select uCrsHisTrat
		Replace uCrsHisTrat.palmoco WITH GERIRPOSOLOGIA.ppalmoco
		Replace uCrsHisTrat.almoco WITH GERIRPOSOLOGIA.palmoco
		Replace uCrsHisTrat.lanche WITH GERIRPOSOLOGIA.planche
		Replace uCrsHisTrat.jantar WITH GERIRPOSOLOGIA.pjantar
		Replace uCrsHisTrat.noite WITH GERIRPOSOLOGIA.pdeitar
	ENDIF
	
	DO CASE 
		CASE GERIRPOSOLOGIA.tipoTarget == 1 && Objecto 
			
			lcCursorCampo = GERIRPOSOLOGIA.Target
			&lcCursorCampo..Value = ALLTRIM(GERIRPOSOLOGIA.posologia.value)
		
		CASE GERIRPOSOLOGIA.tipoTarget == 2 && Cursor, o campo deve chamar-se sempre posologia
			
			lcCursor = GERIRPOSOLOGIA.Target
			
			Select &lcCursor
			Replace &lcCursor..posologia WITH  ALLTRIM(GERIRPOSOLOGIA.posologia.value)
			
		CASE GERIRPOSOLOGIA.tipoTarget == 3 && Cursor,
			uf_gerirposologia_ucrsPosologia()
			lcCursorCampo = GERIRPOSOLOGIA.Target
			&lcCursorCampo..Value = ALLTRIM(GERIRPOSOLOGIA.posologia.value)&& tem de ser a ultima coisa a fazer para actualizar na pagina os valores do cursor !!
			
		OTHERWISE	
			**
	ENDCASE 
		
	uf_gerirposologia_sair()
ENDFUNC


**
FUNCTION uf_gerirposologia_ucrsPosologia
	
	
	LOCAL lcmyFreq, lcmyDur, lcmyCal
	STORE 0 TO 	lcmyDur,lcmyCal
	STORE 1 TO lcmyFreq
	 		
	IF GERIRPOSOLOGIA.pxaodia == .t. AND !EMPTY(GERIRPOSOLOGIA.ContainerAuxVezesDia.horas.value)
		replace ucrsPosologia.frequenciaValor 	 WITH STR(GERIRPOSOLOGIA.ContainerAuxVezesDia.horas.value)
		replace ucrsPosologia.frequenciaUnidade  WITH "DIA"
		lcmyFreq =  GERIRPOSOLOGIA.ContainerAuxVezesDia.horas.value
	ENDIF 
	
	
	IF GERIRPOSOLOGIA.pxhoras == .t. AND !EMPTY(GERIRPOSOLOGIA.ContainerAuxVezesDia.horas.value)
		replace ucrsPosologia.frequenciaValor 	WITH STR(GERIRPOSOLOGIA.ContainerAuxVezesDia.horas.value)
		replace ucrsPosologia.frequenciaUnidade WITH "HORAS"
		lcmyFreq =  GERIRPOSOLOGIA.ContainerAuxVezesDia.horas.value
	ENDIF 
	
	IF (GERIRPOSOLOGIA.ptomar == .t. OR  GERIRPOSOLOGIA.paplicar == .t.) AND !EMPTY(GERIRPOSOLOGIA.containerauxTomar.durantedias.value)
		replace ucrsPosologia.duracaoValor   	WITH STR(GERIRPOSOLOGIA.containerauxTomar.durantedias.value)
		replace ucrsPosologia.duracaoUnidade 	WITH "DIAS"
		lcmyDur = GERIRPOSOLOGIA.containerauxTomar.durantedias.value
	ENDIF 
	
	
	IF GERIRPOSOLOGIA.pfimEmbalagem == .t.
		IF ucrsPosologia.numEmbalagem !=0
			replace ucrsPosologia.quantidadeValor  	WITH STR(ucrsPosologia.numEmbalagem) 
			replace ucrsPosologia.quantidadeUnidade	WITH ''
		ELSE 
			IF GERIRPOSOLOGIA.pxhoras == .t. AND (GERIRPOSOLOGIA.ptomar == .t. OR  GERIRPOSOLOGIA.paplicar == .t. )
				lcmyCal = ROUND((lcmyDur*24) /lcmyFreq,0)
				replace ucrsPosologia.quantidadeValor  	WITH STR(lcmyCal)
				replace ucrsPosologia.quantidadeUnidade	WITH ''	
			ELSE 
				replace ucrsPosologia.quantidadeValor  	WITH STR (lcmyFreq * lcmyDur)
				replace ucrsPosologia.quantidadeUnidade	WITH ''	
			
			ENDIF 
		ENDIF 
	ELSE
		IF GERIRPOSOLOGIA.pxhoras == .t. AND (GERIRPOSOLOGIA.ptomar == .t. OR  GERIRPOSOLOGIA.paplicar == .t. )
			lcmyCal = ROUND((lcmyDur*24) /lcmyFreq,0)
			replace ucrsPosologia.quantidadeValor  	WITH STR(lcmyCal)
			replace ucrsPosologia.quantidadeUnidade	WITH ''	
		ELSE 
			replace ucrsPosologia.quantidadeValor  	WITH STR (lcmyFreq * lcmyDur)
			replace ucrsPosologia.quantidadeUnidade	WITH ''	
		
		ENDIF 
	ENDIF 
	
	replace ucrsPosologia.mudar WITH  'S'
			
	Replace ucrsPosologia.posologia WITH  ALLTRIM(GERIRPOSOLOGIA.posologia.value)
				
ENDFUNC


FUNCTION uf_gerirposologia_sair
	
	&& fecha painel
	GERIRPOSOLOGIA.hide
	GERIRPOSOLOGIA.release
	RELEASE GERIRPOSOLOGIA

ENDFUNC
