
**
FUNCTION uf_criarQuest_chama 
   PUBLIC  myCriarQuestStamp, myCriarQuestIntroducao, myCriarQuestAlteracao 
  	
  	
  	uf_criarQuest_Cursores()
  	
	IF TYPE("criarQuest")=="U"
		DO FORM criarQuest	
	ELSE
		criarQuest.show
	ENDIF 
	
		
	
ENDFUNC
**
FUNCTION uf_criarQuest_pesquisa
	
	STORE "" TO lcQuestStamp 
	
	IF !USED("uCrsQuestPesq")
   		TEXT TO lcsql TEXTMERGE NOSHOW
			SELECT * FROM QUEST (nolock) WHERE 1=1		
		ENDTEXT 
		IF !uf_gerais_actgrelha("", "uCrsQuestPesq", lcsql)
			uf_perguntalt_chama("N�O FOI POSS�VEL ABRIR O MENU. POR FAVOR CONTACTE O SUPORTE.","OK","",64)  
			RETURN .F. 
		ENDIF
   	ENDIF  
	
	
	uf_valorescombo_chama("myCriarQuestStamp", 0, "uCrsQuestPesq", 2, "queststamp", "queststamp,descr", .F., .F., .F., .T., .F., .F., .F., .F., .T.)
	
	
	uf_criarQuest_Cursores(.F., myCriarQuestStamp)
	
	fecha("uCrsQuestPesq")
ENDFUNC 
**
FUNCTION uf_criarQuest_novo
	
ENDFUNC 
**
FUNCTION uf_criarQuest_editar
	
ENDFUNC 
**
FUNCTION uf_criarQuest_Cab
	LPARAMETERS lcInit, lcQuestStamp
	
	LOCAL lcWhere 
	STORE "" TO lcWhere
	
	
	
	IF lcInit
		lcWhere = " and 1 = 0"
	ELSE 
		lcWhere = " and Queststamp = '" +ALLTRIM(lcQuestStamp)+ "'"
 	ENDIF
 	
 	
 	

   	TEXT TO lcsql TEXTMERGE NOSHOW
		SELECT * FROM QUEST (nolock) WHERE 1=1	<<lcWhere>>			
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "uCrsQuest", lcsql)
		uf_perguntalt_chama("N�O FOI POSS�VEL ABRIR O MENU. POR FAVOR CONTACTE O SUPORTE.","OK","",64)  
		RETURN .F. 
	ENDIF
	
	IF !lcInit
 	CRIARQUEST.gridPesq.init
 	ENDIF 
 	
 	 
ENDFUNC 




FUNCTION uf_criarQuest_Cursores
	LPARAMETERS lcInit, lcQuestStamp

	    TEXT TO lcsql TEXTMERGE NOSHOW
	        SELECT *
	        FROM QUEST (nolock)
	        WHERE QuestStamp = '<<lcQuestStamp>>'
	    ENDTEXT

	    IF !uf_gerais_actgrelha("", "uCrsQuest", lcsql)
	        uf_perguntalt_chama("N�O FOI POSS�VEL CARREGAR OS QUESTION�RIOS. CONTACTE O SUPORTE.", "OK", "", 64)
	        RETURN .F.
	    ENDIF

	* Verificar e criar o cursor para Perguntas do Question�rio selecionado
	    TEXT TO lcsql TEXTMERGE NOSHOW
	        SELECT *
	        FROM Quest_Pergunta (nolock)
	        WHERE QuestStamp = '<<lcQuestStamp>>'
	    ENDTEXT

	    IF !uf_gerais_actgrelha("", "uCrsPerguntas", lcsql)
	        uf_perguntalt_chama("N�O FOI POSS�VEL CARREGAR AS PERGUNTAS. CONTACTE O SUPORTE.", "OK", "", 64)
	        RETURN .F.
	    ENDIF

	* Verificar e criar o cursor para Respostas das Perguntas selecionadas
	    TEXT TO lcsql TEXTMERGE NOSHOW
	        SELECT *
	        FROM Quest_Pergunta_Resp (nolock)
	        WHERE Quest_PerguntaStamp IN (
	            SELECT Quest_PerguntaStamp
	            FROM Quest_Pergunta (nolock)
	            WHERE QuestStamp = '<<lcQuestStamp>>'
	        )
	    ENDTEXT

	    IF !uf_gerais_actgrelha("", "uCrsRespostas", lcsql)
	        uf_perguntalt_chama("N�O FOI POSS�VEL CARREGAR AS RESPOSTAS. CONTACTE O SUPORTE.", "OK", "", 64)
	        RETURN .F.
	    ENDIF

	* Verificar e criar o cursor para Tipos de Respostas
	    TEXT TO lcsql TEXTMERGE NOSHOW
	        SELECT *
	        FROM Quest_Pergunta_Resp_Tp (nolock)
	    ENDTEXT

	    IF !uf_gerais_actgrelha("", "uCrsTiposRespostas", lcsql)
	        uf_perguntalt_chama("N�O FOI POSS�VEL CARREGAR OS TIPOS DE RESPOSTAS. CONTACTE O SUPORTE.", "OK", "", 64)
	        RETURN .F.
	    ENDIF
	    
	* Verificar e criar o cursor para Regras
	    TEXT TO lcsql TEXTMERGE NOSHOW
	        SELECT *
	        FROM Quest_Regras (nolock)
	        WHERE QuestStamp = '<<lcQuestStamp>>'
	    ENDTEXT

	    IF !uf_gerais_actgrelha("", "uCrsRegras", lcsql)
	        uf_perguntalt_chama("N�O FOI POSS�VEL CARREGAR AS REGRAS. CONTACTE O SUPORTE.", "OK", "", 64)
	        RETURN .F.
	    ENDIF
	

 	 
ENDFUNC 


**
FUNCTION uf_criarQuest_gravar
	
	LOCAL lcCabScript, lcPerguntaScript, lcRespostaScript, lcRegrasScript
	STORE ""  TO lcCabScript, lcPerguntaScript, lcRespostaScript, lcRegrasScript
	
	**Gravar Cab
	SELECT uCrsPergunta
	GO TOP 
	SCAN 
		TEXT TO lcsql TEXTMERGE NOSHOW
			INSERT INTO [dbo].[Quest]
		           ([QuestStamp]
		           ,[ID]
		           ,[Descr]
		           ,[DadosCli]
		           ,[ousrdata]
		           ,[ousrinis]
		           ,[usrdata]
		           ,[usrinis]
		           ,[Inativo])
		     VALUES
		           (<<uCrsQuest.QuestStamp>>
		           ,<<uCrsQuest.ID>>
		           ,<<uCrsQuest.Descr>>
		           ,<<uCrsQuest.DadosCli>>
		           ,<<uCrsQuest.ousrdata>>
		           ,<<uCrsQuest.ousrinis>>
		           ,<<uCrsQuest.usrdata>>
		           ,<<uCrsQuest.usrinis>>
		           ,<<uCrsQuest.Inativo>>)
		ENDTEXT
		IF myExportQuest
			lcCabScript = lcCabScript +lcSql  
		ELSE   
			IF !uf_gerais_actgrelha("", "", lcsql)
				uf_perguntalt_chama("N�O FOI POSS�VEL ABRIR O MENU. POR FAVOR CONTACTE O SUPORTE.","OK","",64)  
				RETURN .F. 
			ENDIF
		ENDIF  
		

	ENDSCAN 
	
	
	
	**Gravar Pergunta
	SELECT uCrsPergunta
	GO TOP 
	SCAN 		
		TEXT TO lcsql TEXTMERGE NOSHOW
			INSERT INTO [dbo].[quest_pergunta]
		           ([quest_perguntastamp]
		           ,[queststamp]
		           ,[pergunta]
		           ,[ordem]
		           ,[ousrdata]
		           ,[ousrinis]
		           ,[usrdata]
		           ,[usrinis]
		           ,[permiteMulti])
		     VALUES
		           (<<uCrsPergunta.quest_perguntastamp>>
		           ,<<uCrsPergunta.queststamp>>
		           ,<<uCrsPergunta.pergunta>>
		           ,<<uCrsPergunta.ordem>>
		           ,<<uCrsPergunta.ousrdata>>
		           ,<<uCrsPergunta.ousrinis>>
		           ,<<uCrsPergunta.usrdata>>
		           ,<<uCrsPergunta.usrinis>>
		           ,<<uCrsPergunta.permiteMulti>>)
		ENDTEXT
		IF myExportQuest
			lcPerguntaScript= lcPerguntaScript +lcSql  
		ELSE   
			IF !uf_gerais_actgrelha("", "", lcsql)
				uf_perguntalt_chama("N�O FOI POSS�VEL ABRIR O MENU. POR FAVOR CONTACTE O SUPORTE.","OK","",64)  
				RETURN .F. 
			ENDIF
		ENDIF  

	ENDSCAN 
	
	
	**Gravar Resp
	SELECT uCrsRespostas
	GO TOP 
	SCAN
		TEXT TO lcsql TEXTMERGE NOSHOW
			INSERT INTO [dbo].[quest_pergunta_resp]
		           ([Quest_pergunta_respStamp]
		           ,[Quest_perguntaStamp]
		           ,[resposta]
		           ,[ordem]
		           ,[quest_pergunta_resp_tpStamp]
		           ,[ousrdata]
		           ,[ousrinis]
		           ,[usrdata]
		           ,[usrinis])
		     VALUES
		           ('<<uCrsRespostas.Quest_pergunta_respStamp>>'
		           ,'<<uCrsRespostas.Quest_perguntaStamp>>'
		           ,'<<uCrsRespostas.resposta>>'
		           ,'<<uCrsRespostas.ordem>>'
		           ,'<<uCrsRespostas.quest_pergunta_resp_tpStamp>>'
		           ,'<<uCrsRespostas.ousrdata>>'
		           ,'<<uCrsRespostas.ousrinis>>'
		           ,'<<uCrsRespostas.usrdata>>'
		           ,'<<uCrsRespostas.usrinis>>')				
		ENDTEXT
		IF myExportQuest
			lcRespostaScript= lcRespostaScript +lcSql  
		ELSE   
			IF !uf_gerais_actgrelha("", "", lcsql)
				uf_perguntalt_chama("N�O FOI POSS�VEL ABRIR O MENU. POR FAVOR CONTACTE O SUPORTE.","OK","",64)  
				RETURN .F. 
			ENDIF
		ENDIF  
	ENDSCAN 	
	**Gravar Regras
	
	SELECT uCrsPergunta
	GO TOP 
	SELECT uCrsRespostas
	GO TOP 
	SELECT ;
		uf_gerais_stamp() as quest_regrasStamp,;
		uCrsPergunta.queststamp as questStamp,;
		uCrsPergunta.quest_perguntastamp as quest_perguntaStamp,;
		uCrsRespostas.quest_pergunta_respStamp as quest_pergunta_respStamp,;
		"" as quest_pergunta_respStamp_dependente,;
		uCrsRespostas.Prox_Ordem as Prox_Ordem,;
		uCrsRespostas.ousrdata as ousrdata,;
		uCrsRespostas.ousrdata as ousrinis,;
		uCrsRespostas.ousrdata as usrdata,;
		uCrsRespostas.ousrdata as usrinis;
	From uCrsRespostas ;
	INNER JOIN  uCrsPergunta ON  uCrsPergunta.quest_perguntastamp = uCrsRespostas.quest_perguntastamp;
	into cursor  uCrsRegras READWRITE 
	
	
	
	
	SELECT uCrsRegras
	GO top
	SCAN 
		TEXT TO lcsql TEXTMERGE NOSHOW
			INSERT INTO [dbo].[quest_regras]
		           ([quest_regrasStamp]
		           ,[questStamp]
		           ,[quest_perguntaStamp]
		           ,[quest_pergunta_respStamp]
		           ,[quest_pergunta_respStamp_dependente]
		           ,[Prox_Ordem]
		           ,[ousrdata]
		           ,[ousrinis]
		           ,[usrdata]
		           ,[usrinis])
		     VALUES
		           ('<<uCrsRegras.quest_regrasStamp>>'
		           ,'<<uCrsRegras.questStamp>>'
		           ,'<<uCrsRegras.quest_perguntaStamp>>'
		           ,'<<uCrsRegras.quest_pergunta_respStamp>>'
		           ,'<<uCrsRegras.quest_pergunta_respStamp_dependente>>'
		           ,'<<uCrsRegras.Prox_Ordem>>'
		           ,'<<uCrsRegras.ousrdata>>'
		           ,'<<uCrsRegras.ousrinis>>'
		           ,'<<uCrsRegras.usrdata>>'
		           ,'<<uCrsRegras.usrinis>>')			
			ENDTEXT
		IF myExportQuest
			lcRegrasScript= lcRegrasScript +lcSql  
		ELSE   
			IF !uf_gerais_actgrelha("", "", lcsql)
				uf_perguntalt_chama("N�O FOI POSS�VEL ABRIR O MENU. POR FAVOR CONTACTE O SUPORTE.","OK","",64)  
				RETURN .F. 
			ENDIF
		ENDIF  
		
	ENDSCAN 
	
	
	IF myExportQuest
		***Exportar as variaveis todas para txt lcCabScript, lcPerguntaScript, lcRespostaScript, lcRegrasScript
		myExportQuest = .F.
	ENDIF  
ENDFUNC 



**
FUNCTION uf_criarQuest_sair
	fecha("uCrsQuest")
	fecha("uCrsPergunta")
	fecha("uCrsRespostas")
	fecha("uCrsRegras")
	
	Release myCriarQuestStamp, myCriarQuestIntroducao, myCriarQuestAlteracao
	
	criarQuest.hide
	criarQuest.Release
		
	RELEASE criarQuest
ENDFUNC 