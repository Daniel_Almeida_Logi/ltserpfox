**
FUNCTION uf_pesqmultidata_chama
	LPARAMETERS lcOrigem, lcObjecto, lcTipoRetorno, ucrsCampos, lcMantemPainelEscolher
	
	PUBLIC myOrigemPPMD, myObjDestPPMD, myTipoRetorno
	myOrigemPPMD = UPPER(ALLTRIM(lcOrigem))
	myObjDestPPMD = ALLTRIM(lcObjecto)
	
	IF(EMPTY(lcTipoRetorno))
		myTipoRetorno = 0
	ELSE
		myTipoRetorno = lcTipoRetorno
	ENDIF
	
	IF(EMPTY(lcMantemPainelEscolher))
		myMantemPainelEscolher = 0
	ELSE
		myMantemPainelEscolher = lcMantemPainelEscolher
	ENDIF
	
	IF USED("uCrsPesqMulti")
		fecha("uCrsPesqMulti")
	ENDIF
	CREATE CURSOR uCrsPesqMulti(sel bit, valor nvarchar)

	
	IF type("PESQUISAMULTIDATA")=="U"
		DO FORM PESQUISAMULTIDATA
	ELSE
		PESQUISAMULTIDATA.show
	ENDIF
ENDFUNC


**
FUNCTION uf_PESQUISAMULTIDATA_FiltraCursor
	SELECT uCrsPesqMulti
	GO TOP
	SET FILTER TO LIKE('*'+UPPER(ALLTRIM(PESQUISAMULTIDATA.valor.value))+'*',UPPER(valor))
	
	PESQUISAMULTIDATA.GRIDPESQMULTI.refresh
ENDFUNC


** Configura grelha de Pesquisa
FUNCTION uf_pesquisamultidata_confgrid
	
	FOR i=1 TO PESQUISAMULTIDATA.GRIDPESQMULTI.columncount
	 	
	 	DO CASE 	 
	 		CASE UPPER(PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).controlsource) == "UCRSPESQMULTI.SEL"
	 			PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).columnorder 	 	= 1
	 			PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).WIDTH 			= 50
	 			PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).sparse 		 	= .f.
	 			PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).header1.Caption	= "Sel."
	 			PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).visible 		 	= .t.
	 		
	 		CASE UPPER(PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).controlsource) == "UCRSPESQMULTI.VALOR"
	 			PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).columnorder 	 	= 2
	 			PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).WIDTH 			= 685
	 			
	 			DO CASE
					&& NOME COMERCIAL
						CASE myOrigemPPMD=="NOME"
							PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).header1.Caption	=	"Nome Comercial"
					
					&& FORMA FARMACEUTICA
						CASE myOrigemPPMD=="FORMA"
							PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).header1.Caption	=	"Forma Farmac�utica"
					
					&& PRINCIPIO ACTIVO
						CASE myOrigemPPMD=="DCI"
							PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).header1.Caption	=	"Princ�pio Activo"
					
					&& EMBALAGEM
						CASE myOrigemPPMD=="EMBALAGEM"
							PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).header1.Caption	=	"Embalagem"
					
					&& CNPEM
						CASE myOrigemPPMD=="CNPEM"
							PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).header1.Caption	=	"CNPEM"
					
					&& DOSAGEM
						CASE myOrigemPPMD=="CNPEM"
							PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).header1.Caption	=	"DOSAGEM"
					
					&& VENDEDORES
						CASE myOrigemPPMD=="VENDEDORES"
							PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).header1.Caption	=	"Vendedores"
							
							
					&& VIA ADMINISTRACAO
						CASE myOrigemPPMD=="VIAADMIN"
							PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).header1.Caption	=	"Via Administra��o"
							
					&& MARCA
						CASE myOrigemPPMD=="MARCA"
							PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).header1.Caption	=	"Marca"
					
					&& LABORATORIO
						CASE myOrigemPPMD=="LABORATORIO"
							PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).header1.Caption	=	"Laborat�rio"
							
					&& EFR_SMS
						CASE myOrigemPPMD=="EFR_SMS"
							PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).header1.Caption	=	"Entidade Financeira Respons�vel"	
					
					&& Patologias_SMS
						CASE myOrigemPPMD=="PATOLOGIAS_SMS"
							PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).header1.Caption	=	"Patologia"	
					
					&& MotivoCancelamento Marca��es
						CASE myOrigemPPMD=="MARCACOES_MOTIVOCANCELAMENTO"
							PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).header1.Caption	=	"Motivo Cancelamento"

					&& Nivel 3 familia comercial
						CASE myOrigemPPMD=="FAMCATEGORIAS"
							PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).header1.Caption	=	"Class. Comercial"							
							
					&& Class farm�cia - tipo PRoduto
						CASE myOrigemPPMD=="FAMFAMILIA"
							PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).header1.Caption	=	"Tipo Produto"	
							
					&& LOCAL_2
						CASE myOrigemPPMD=="LOCAL_2"
							PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).header1.Caption	=	"Local"	
					
					&& Patologias Ficha Utente
						CASE myOrigemPPMD=="PATOLOGIAS"
							PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).header1.Caption	=	"PATOLOGIAS"																
					
					OTHERWISE
						***
				ENDCASE
				
	 			PESQUISAMULTIDATA.GRIDPESQMULTI.columns(i).visible 		 = .t.			
		ENDCASE
	ENDFOR
ENDFUNC


**
FUNCTION uf_pesqmultidata_vazio
	SELECT uCrsPesqMulti
	&myObjDestPPMD..value = ""
		
	uf_PESQUISAMULTIDATA_sair()
ENDFUNC

**Evento Click na selec��o
FUNCTION uf_pesquisamultidata_escolhe

	LOCAL lcObj, lcRetorno
	lcObj = uCrsPesqMulti.valor
		
	SELECT uCrsPesqMulti
	DO CASE
*!*			CASE myTipoRetorno == 1 && vari�vel
*!*				&myObjDestPPMD = uCrsPesqMulti.&lcRetorno 
*!*				
*!*			CASE myTipoRetorno == 2 && objecto
*!*				&myObjDestPPMD..value = uCrsPesqMulti.&lcRetorno 
			
		CASE myTipoRetorno == 1 && cursor

				lcCursor = myObjDestPPMD			
				
				SELECT (lcCursor) 
				APPEND BLANK				
				SELECT ucrsCampos
				GO TOP
				SCAN
					LOCAL lcAttr, lcAttrDestino 
					lcAttrOrigem = ucrsCampos.atrOrigem 
					lcAttrDestino = ucrsCampos.atrDestino 
					IF  TYPE("uCrsPesqMulti." + lcAttrOrigem)!='U'  AND !EMPTY(uCrsPesqMulti.&lcAttrOrigem.) 
						SELECT (lcCursor)
						REPLACE &lcCursor..&lcAttrDestino. WITH uCrsPesqMulti.&lcAttrOrigem.
					ENDIF
				SELECT ucrsCampos	
				ENDSCAN			
			
		OTHERWISE
			&myObjDestPPMD..value = uCrsPesqMulti.valor
	ENDCASE
	
	IF(EMPTY(myMantemPainelEscolher))
		uf_PESQUISAMULTIDATA_sair()
	ENDIF
	
	
ENDFUNC
**
FUNCTION uf_PESQUISAMULTIDATA_sair
	**Fecha Cursores
	IF USED("uCrsPesqMulti")
		fecha("uCrsPesqMulti")
	ENDIF
	
	**
	RELEASE myObjDestPPMD
	RELEASE myOrigemPPMD
	RELEASE  myTipoRetorno 
	RELEASE  myMantemPainelEscolher
	
	PESQUISAMULTIDATA.hide
	PESQUISAMULTIDATA.release
ENDFUNC


**
FUNCTION uf_pesquisamultidata_gravar
	
ENDFUNC


**
FUNCTION uf_pesqmultidata_Teclado
	PESQUISAMULTIDATA.tecladoVirtual1.show("PESQUISAMULTIDATA.gridPesqMulti", 161, "PESQUISAMULTIDATA.shpGrid", 161)
ENDFUNC


** ALTERAR MARCA 
FUNCTION uf_pesqmultidata_alterarMarca
	LOCAL lcCampo,lcStamp,lcSql, lcRetorno 
	STORE "" To lcSQL,lcStamp
	
	SELECT uCrsPesqMulti
	
	IF UPPER(ALLTRIM(uCrsPesqMulti.source)) == "D"
		uf_perguntalt_chama("N�o pode editar uma marca introduzida via actualiza��o de dicion�rio.","OK","",48)
		RETURN .f.
	ENDIF 
	
	lcStamp = uCrsPesqMulti.id
	
	IF EMPTY(lcStamp)
		uf_perguntalt_chama("POR FAVOR SELECIONE UM REGISTO.","OK","",48)
		RETURN .f.
	ENDIF
	
	lcRetorno = uf_dytable_getnome()
	
	If Empty(lcRetorno)
		uf_perguntalt_chama("A DESCRI��O N�O PODE SER VAZIA!","OK","",48)
		RETURN .f.
	ENDIF
	
	lcCampo = LEFT(STRTRAN(ALLTRIM(lcRetorno),CHR(39),""),60)
	
	IF uf_perguntalt_chama("Vai alterar a marca para " + Alltrim(lcCampo) + ". Pretende continuar?","Sim","N�o",)
		TEXT TO lcSQL TEXTMERGE NOSHOW 
			Update marcas
			Set 
				descr	= '<<ALLTRIM(lcCampo)>>',
				dataalt = dateadd(HOUR, <<difhoraria>>, getdate())
			Where 
				id = <<lcStamp>>
		Endtext
		
		If uf_gerais_actGrelha("","",lcSql)
			Select uCrsPesqMulti
			lcDescrMarca = uCrsPesqMulti.valor
			replace uCrsPesqMulti.valor With alltrim(lcCampo)
			
			IF uf_perguntalt_chama("Pretende alterar esta marca em todos os artigos?","Sim","N�o")
				lcSQL = ''
				TEXT TO lcSQL TEXTMERGE NOSHOW 
					UPDATE st
					SET usr1 = '<<alltrim(lcCampo)>>'
					where usr1 = '<<ALLTRIM(lcDescrMarca)>>'
				ENDTEXT 
				uf_gerais_actGrelha("","",lcSql)
			ENDIF 
			
			uf_perguntalt_chama("ALTERA��O EFECTUADA COM SUCESSO.","OK","",64)
		Else
			uf_perguntalt_chama("ERRO AO REALIZAR A GRAVA��O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
	ENDIF 
ENDFUNC 


** INTRODUZIR MARCA
FUNCTION uf_pesqmultidata_introduzirMarca
	LOCAL lcCampo,lcSql,lcStamp, lcRetorno
	STORE "" TO lcSQL,lcStamp
	
	lcRetorno = uf_dytable_getnome()
	If Empty(lcRetorno)
		uf_perguntalt_chama("A DESCRIC�O N�O PODE SER VAZIA!","OK","",48)
		RETURN .f.
	ENDIF
	
	lcCampo = LEFT(STRTRAN(ALLTRIM(lcRetorno),CHR(39),""),60)
	
	If uf_perguntalt_chama("Vai inserir a marca " + Alltrim(lcCampo) + ". Pretende continuar?","Sim","N�o")
		Text To lcSQL Textmerge Noshow
			INSERT into  marcas(id, descr, source, dataalt)
			Values  
				(
					isnull((select MAX(id) + 1 from marcas),1),
					'<<lcCampo>>',
					'U',
					dateadd(HOUR, <<difhoraria>>, getdate())
				)
			SELECT MAX(id) as id FROM marcas
		ENDTEXT
		
		If uf_gerais_actGrelha("","uCrsMaxIDMArca",lcSql)
			SELECT uCrsMaxIDMArca
			Select uCrsPesqMulti
			APPEND BLANK 
			replace uCrsPesqMulti.id WITH uCrsMaxIDMArca.id
			replace uCrsPesqMulti.sel With .f.
			replace uCrsPesqMulti.valor With ALLTRIM(lcCampo)
			replace uCrsPesqMulti.source With 'U'
			uf_perguntalt_chama("MARCA INSERIDA COM SUCESSO.","OK","",64)
		Else
			uf_perguntalt_chama("ERRO AO REALIZAR A GRAVA��O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
	ENDIF 
ENDFUNC 


** ELIMINAR MARCA
FUNCTION uf_pesqmultidata_eliminarMarca
	Local lcStamp,lcSql
	Store "" To lcSQL,lcStamp
	
	SELECT uCrsPesqMulti
	
	IF UPPER(ALLTRIM(uCrsPesqMulti.source)) == "D"
		uf_perguntalt_chama("N�o pode eliminar uma marca introduzida via actualiza��o de dicion�rio.","OK","",48)
		RETURN .f.
	ENDIF 
	
	lcStamp = uCrsPesqMulti.id
	
	IF EMPTY(lcStamp)
		uf_perguntalt_chama("POR FAVOR SELECCIONE UM REGISTO.","OK","",48)
		RETURN .f.
	ENDIF
	
	If uf_perguntalt_chama("Tem a certeza que deseja eliminar a marca?","Sim","N�o")	
		lcSQL=''
		Text To lcSQL Textmerge Noshow
			DELETE from marcas
			Where id=<<lcStamp>>
		Endtext
		
		If uf_gerais_actGrelha("","",lcSql)
			Select uCrsPesqMulti
			lcDescrMarca = uCrsPesqMulti.valor
			DELETE
			
			TRY 
				SKIP 1
			CATCH 
				SKIP -1
			FINALLY 
				GO TOP 
			ENDTRY
			
			IF uf_perguntalt_chama("Pretende remover esta marca de todos os artigos?","Sim","N�o")
				lcSQL = ''
				TEXT TO lcSQL TEXTMERGE NOSHOW 
					UPDATE st
					SET usr1 = ''
					where usr1 = '<<ALLTRIM(lcDescrMarca)>>'
				ENDTEXT 
				uf_gerais_actGrelha("","",lcSql)
			ENDIF 
			
			uf_perguntalt_chama("REGISTO ELIMINADO COM SUCESSO.","OK","",64)
		
			pesquisamultidata.refresh
		Else
			uf_perguntalt_chama("ERRO AO ELIMINAR O REGISTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		endif
	Endif
ENDFUNC 


***************************************
*			ALTERAR Lab			  *
***************************************
FUNCTION uf_pesqmultidata_alterarLab
	Local lcCampo, lcStamp,lcSql, lcRetorno, lcSource
	Store "" To lcSQL,lcStamp
	
	SELECT uCrsPesqMulti
	
	IF UPPER(ALLTRIM(uCrsPesqMulti.source)) != "U"
		uf_perguntalt_chama("N�o pode editar um Laborat�rio introduzido via actualiza��o de dicion�rio.","OK","",48)
		RETURN .f.
	ENDIF 
	
	lcStamp = uCrsPesqMulti.id
	lcSource = uCrsPesqMulti.source
	
	IF EMPTY(lcStamp)
		uf_perguntalt_chama("POR FAVOR SELECIONE UM REGISTO.","OK","",48)
		RETURN .f.
	ENDIF
	
	lcRetorno = uf_dytable_getnome()
	
	If Empty(lcRetorno)
		uf_perguntalt_chama("A DESCRI��O N�O PODE SER VAZIA!","OK","",48)
		RETURN .f.
	ENDIF
	
	lcCampo = LEFT(STRTRAN(ALLTRIM(lcRetorno),CHR(39),""),60)

	If uf_perguntalt_chama("Vai alterar o Laborat�rio para " + Alltrim(lcCampo) + ". Pretende continuar?","Sim","N�o",)
		Text To lcSQL Textmerge Noshow
			Update 
				b_labs
			Set 
				abrev	= '<<ALLTRIM(lcCampo)>>',
				descr	= '<<ALLTRIM(lcCampo)>>',
				dataalt = dateadd(HOUR, <<difhoraria>>, getdate())
			Where 
				id = <<lcStamp>>
				and source = <<lcSource>>
		Endtext
		
		If uf_gerais_actGrelha("","",lcSql)
			Select uCrsPesqMulti
			lcDescrLab = uCrsPesqMulti.valor
			replace uCrsPesqMulti.valor With alltrim(lcCampo)
			
			IF uf_perguntalt_chama("Pretende alterar este Laborat�rio em todos os artigos?","Sim","N�o")
				lcSQL = ''
				TEXT TO lcSQL TEXTMERGE NOSHOW 
					UPDATE 
						st
					SET 
						u_lab = '<<alltrim(lcCampo)>>'
					where  
						u_lab = '<<ALLTRIM(lcDescrLab)>>'
				ENDTEXT 
				uf_gerais_actGrelha("","",lcSql)
			ENDIF 
			
			uf_perguntalt_chama("ALTERA��O EFECTUADA COM SUCESSO.","OK","",64)
		Else
			uf_perguntalt_chama("ERRO AO REALIZAR A GRAVA��O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
	ENDIF 
ENDFUNC 


*******************************************
*			INTRODUZIR LAB			  *
*******************************************
FUNCTION uf_pesqmultidata_introduzirLab
	Local lcCampo,lcSql,lcStamp, lcRetorno
	Store "" To lcSQL,lcStamp
	
	lcRetorno = uf_dytable_getnome()
	If Empty(lcRetorno)
		uf_perguntalt_chama("A DESCRIC�O N�O PODE SER VAZIA!","OK","",48)
		RETURN .f.
	ENDIF
	
	lcCampo = LEFT(STRTRAN(ALLTRIM(lcRetorno),CHR(39),""),60)
	
	If uf_perguntalt_chama("Vai inserir o Laborat�rio: " + Alltrim(lcCampo) + ". Pretende continuar?","Sim","N�o")
		Text To lcSQL Textmerge Noshow
			INSERT into	b_labs(
					id
					,abrev
					,descr
					,source
					,dataalt
			)Values	(
					isnull((select MAX(id) + 1 from b_labs),1),
					'<<lcCampo>>',
					'<<lcCampo>>',
					'U',
					dateadd(HOUR, <<difhoraria>>, getdate())
			)
			SELECT MAX(id) as id FROM b_labs
		ENDTEXT
		
		If uf_gerais_actGrelha("","uCrsMaxIDLab",lcSql)
			SELECT uCrsMaxIDLab
			Select uCrsPesqMulti
			APPEND BLANK 
			replace uCrsPesqMulti.id WITH uCrsMaxIDLab.id
			replace uCrsPesqMulti.sel With .f.
			replace uCrsPesqMulti.valor With ALLTRIM(lcCampo)
			replace uCrsPesqMulti.source With 'U'
			
			uf_perguntalt_chama("LABORAT�RIO INSERIDO COM SUCESSO.","OK","",64)
		Else
			uf_perguntalt_chama("ERRO AO REALIZAR A GRAVA��O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
	Endif
ENDFUNC 



** 20200917 JG
*****************************
** ALTERAR LOCAL_2 
*****************************
FUNCTION uf_pesqmultidata_alterarLocal
	LOCAL lcCampo,lcStamp,lcSql, lcRetorno 
	STORE "" To lcSQL,lcStamp
	
	SELECT uCrsPesqMulti
	
	IF UPPER(ALLTRIM(uCrsPesqMulti.source)) == "D"
		uf_perguntalt_chama("N�o pode editar um local introduzido via actualiza��o de dicion�rio.","OK","",48)
		RETURN .f.
	ENDIF 
	
	lcStamp = uCrsPesqMulti.id
	
	IF EMPTY(lcStamp)
		uf_perguntalt_chama("POR FAVOR SELECIONE UM REGISTO.","OK","",48)
		RETURN .f.
	ENDIF
	
	lcRetorno = uf_dytable_getnome()
	
	If Empty(lcRetorno)
		uf_perguntalt_chama("A DESCRI��O N�O PODE SER VAZIA!","OK","",48)
		RETURN .f.
	ENDIF
	
	lcCampo = STRTRAN(ALLTRIM(lcRetorno),CHR(39),"")

	IF LEN(lcCampo) >20
		uf_perguntalt_chama("O campo da localiza��o � limitado a 20 caracteres, por favor retifique","OK","",16)
  		RETURN .f.
	ENDIF

	IF uf_perguntalt_chama("Vai alterar o local para " + Alltrim(lcCampo) + ". Pretende continuar?","Sim","N�o",)
		TEXT TO lcSQL TEXTMERGE NOSHOW 
			Update locais
			Set 
				descr	= '<<ALLTRIM(lcCampo)>>',
				dataalt = dateadd(HOUR, <<difhoraria>>, getdate())
			Where 
				id = <<lcStamp>>
		Endtext
		
		If uf_gerais_actGrelha("","",lcSql)
			Select uCrsPesqMulti
			lcDescrLocal = uCrsPesqMulti.valor
			replace uCrsPesqMulti.valor With alltrim(lcCampo)
			
			IF uf_perguntalt_chama("Pretende alterar este local em todos os artigos?","Sim","N�o")
				lcSQL = ''
				TEXT TO lcSQL TEXTMERGE NOSHOW 
					UPDATE st
					SET local = '<<alltrim(lcCampo)>>'
					where local = '<<ALLTRIM(lcDescrLocal)>>'
										
					UPDATE st
					SET u_local = '<<alltrim(lcCampo)>>'
					where u_local = '<<ALLTRIM(lcDescrLocal)>>'
					
					UPDATE st
					SET u_local2 = '<<alltrim(lcCampo)>>'
					where u_local2 = '<<ALLTRIM(lcDescrLocal)>>'			
										
				ENDTEXT 
				uf_gerais_actGrelha("","",lcSql)
			ENDIF 
			
			uf_perguntalt_chama("ALTERA��O EFECTUADA COM SUCESSO.","OK","",64)
		Else
			uf_perguntalt_chama("ERRO AO REALIZAR A GRAVA��O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
	ENDIF 
ENDFUNC 


*****************************
** INTRODUZIR LOCAL_2
*****************************
FUNCTION uf_pesqmultidata_introduzirLocal
	LOCAL lcCampo,lcSql,lcStamp, lcRetorno
	STORE "" TO lcSQL,lcStamp
	
	lcRetorno = uf_dytable_getnome()
	If Empty(lcRetorno)
		uf_perguntalt_chama("A DESCRIC�O N�O PODE SER VAZIA!","OK","",48)
		RETURN .f.
	ENDIF
	
	lcCampo = STRTRAN(ALLTRIM(lcRetorno),CHR(39),"")

	IF LEN(lcCampo) >20
		uf_perguntalt_chama("O campo da localiza��o � limitado a 20 caracteres, por favor retifique","OK","",16)
  		RETURN .f.
	ENDIF
	
	If uf_perguntalt_chama("Vai inserir o local " + Alltrim(lcCampo) + ". Pretende continuar?","Sim","N�o")
		Text To lcSQL Textmerge Noshow
			INSERT into  locais ( descr, source, dataalt)
			Values  
				(					
					'<<lcCampo>>',
					'U',
					dateadd(HOUR, <<difhoraria>>, getdate())
				)
			SELECT MAX(id) as id FROM locais 
		ENDTEXT
		
		If uf_gerais_actGrelha("","uCrsMaxIDLocal",lcSql)
			SELECT uCrsMaxIDLocal
			Select uCrsPesqMulti
			APPEND BLANK 
			replace uCrsPesqMulti.id WITH uCrsMaxIDLocal.id
			replace uCrsPesqMulti.sel With .f.
			replace uCrsPesqMulti.valor With ALLTRIM(lcCampo)
			replace uCrsPesqMulti.source With 'U'
			uf_perguntalt_chama("LOCAL INSERIDO COM SUCESSO.","OK","",64)
		Else
			uf_perguntalt_chama("ERRO AO REALIZAR A GRAVA��O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
	ENDIF 
ENDFUNC 

*****************************
** ELIMINAR LOCAL_2
*****************************
FUNCTION uf_pesqmultidata_eliminarLocal
	Local lcStamp,lcSql
	Store "" To lcSQL,lcStamp
	
	SELECT uCrsPesqMulti
	
	IF UPPER(ALLTRIM(uCrsPesqMulti.source)) == "D"
		uf_perguntalt_chama("N�o pode eliminar um local introduzido via actualiza��o de dicion�rio.","OK","",48)
		RETURN .f.
	ENDIF 
	
	lcStamp = uCrsPesqMulti.id
	
	IF EMPTY(lcStamp)
		uf_perguntalt_chama("POR FAVOR SELECCIONE UM REGISTO.","OK","",48)
		RETURN .f.
	ENDIF
	
	If uf_perguntalt_chama("Tem a certeza que deseja eliminar o local?","Sim","N�o")	
		lcSQL=''
		Text To lcSQL Textmerge Noshow
			DELETE from locais
			Where id=<<lcStamp>>
		Endtext
		
		If uf_gerais_actGrelha("","",lcSql)
			Select uCrsPesqMulti
			lcDescrLocal = uCrsPesqMulti.valor
			DELETE
			
			TRY 
				SKIP 1
			CATCH 
				SKIP -1
			FINALLY 
				GO TOP 
			ENDTRY
			
			IF uf_perguntalt_chama("Pretende remover este local de todos os artigos?","Sim","N�o")
				lcSQL = ''
				TEXT TO lcSQL TEXTMERGE NOSHOW
					UPDATE st
					SET local = ''
					where local = '<<ALLTRIM(lcDescrLocal)>>'
										
					UPDATE st
					SET u_local = ''
					where u_local = '<<ALLTRIM(lcDescrLocal)>>'
					
					UPDATE st
					SET u_local2 = ''
					where u_local2 = '<<ALLTRIM(lcDescrLocal)>>'		
					
				ENDTEXT 
				uf_gerais_actGrelha("","",lcSql)
			ENDIF 
			
			uf_perguntalt_chama("REGISTO ELIMINADO COM SUCESSO.","OK","",64)
		
			pesquisamultidata.refresh
		Else
			uf_perguntalt_chama("ERRO AO ELIMINAR O REGISTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		endif
	Endif
ENDFUNC 





*******************************************
*				ELIMINAR LAB			  *
*******************************************
FUNCTION uf_pesqmultidata_eliminarLab
	Local lcStamp,lcSql
	Store "" To lcSQL,lcStamp
	
	SELECT uCrsPesqMulti
	
	IF UPPER(ALLTRIM(uCrsPesqMulti.source)) != "U"
		uf_perguntalt_chama("N�o pode eliminar um Laborat�rio introduzido via actualiza��o de dicion�rio.","OK","",48)
		RETURN .f.
	ENDIF 
	
	lcStamp = uCrsPesqMulti.id
	
	IF EMPTY(lcStamp)
		uf_perguntalt_chama("POR FAVOR SELECCIONE UM REGISTO.","OK","",48)
		RETURN .f.
	ENDIF
	
	If uf_perguntalt_chama("Tem a certeza que deseja eliminar o Laborat�rio?","Sim","N�o")	
		lcSQL=''
		Text To lcSQL Textmerge Noshow
			DELETE from b_labs
			Where id = <<lcStamp>>
		Endtext
		
		If uf_gerais_actGrelha("","",lcSql)
			Select uCrsPesqMulti
			lcDescrLab = uCrsPesqMulti.valor
			DELETE
			
			TRY 
				SKIP 1
			CATCH 
				SKIP -1
			FINALLY 
				GO TOP 
			ENDTRY
			
			IF uf_perguntalt_chama("Pretende remover este Laborat�rio de todos os artigos?","Sim","N�o")
				lcSQL = ''
				TEXT TO lcSQL TEXTMERGE NOSHOW 
					UPDATE 
						st
					SET 
						u_lab = ''
					where 
						u_lab = '<<ALLTRIM(lcDescrLab)>>'
				ENDTEXT 
				uf_gerais_actGrelha("","",lcSql)
			ENDIF 
			
			uf_perguntalt_chama("REGISTO ELIMINADO COM SUCESSO.","OK","",64)
		
			pesquisamultidata.refresh
		Else
			uf_perguntalt_chama("ERRO AO ELIMINAR O REGISTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF 
	ENDIF 
ENDFUNC 


** introduz familia comercial n�vel 3
FUNCTION uf_pesqmultidata_introduzirImsNivel3
	LOCAL lcCampo, lcSql, lcStamp, lcRetorno, lcAux
	STORE "" TO lcSQL, lcStamp
	STORE '0' TO lcAux
	
	
	lcRetorno = uf_dytable_getnome()
	IF EMPTY (lcRetorno)
		uf_perguntalt_chama("A DESCRIC�O N�O PODE SER VAZIA!","OK","",48)
		RETURN .f.
	ENDIF
	
	&& valida o secstamp (n�vel 2) porque vai introduzir um n�vel que vai ficar ligado ao n�vel que est� seleccionado.
	
	IF USED("st")
		IF EMPTY(stocks.pageframe1.page2.seccao.value)
			uf_perguntalt_chama("O n�vel 2 de classifica��o n�o pode estar vazio. Por favor verifique.","OK","",48)
			RETURN .f.	
		ELSE
			lcAux = ALLTRIM(st.u_secstamp)
		ENDI
	ENDIF
	
	lcCampo = LEFT(STRTRAN(ALLTRIM(lcRetorno),CHR(39),""),60)
	
	IF uf_perguntalt_chama("Vai inserir a class. comercial n�vel 3 " + Alltrim(lcCampo) + ". Pretende continuar?","Sim","N�o")
		TEXT TO lcSQL TEXTMERGE NOSHOW 
			INSERT INTO b_famCategorias (catstamp, secstamp, design, inactivo)
			VALUES  
				(
					isnull((select MAX(CAST(catstamp as int)) + 1 from b_famCategorias),1),
					'<<lcAux>>',
					'<<lcCampo>>',
					0
				)
		ENDTEXT
		IF uf_gerais_actGrelha("","",lcSql)
			
			SELECT uCrsPesqMulti
			APPEND BLANK 
			
			replace uCrsPesqMulti.sel 	 WITH .f.
			replace uCrsPesqMulti.valor  With ALLTRIM(lcCampo)

			uf_perguntalt_chama("CLASSIFICA��O INTRODUZIDA COM SUCESSO.","OK","",64)
			
			pesquisamultidata.refresh
		
		ELSE 
			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO EFETUAR A OPERA��O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
	ENDIF 
ENDFUNC 


** elimina familia IMS n�vel 3
FUNCTION uf_pesqmultidata_eliminarImsNivel3
	LOCAL lcStamp,lcSql
	STORE "" TO lcSQL,lcStamp
	
	SELECT uCrsPesqMulti
	
	lcStamp = ALLTRIM(uCrsPesqMulti.id)
	
	IF EMPTY(lcStamp)
		uf_perguntalt_chama("POR FAVOR SELECCIONE UM REGISTO.","OK","",48)
		RETURN .f.
	ENDIF
	
	IF uf_perguntalt_chama("Tem a certeza que deseja eliminar a class. comercial n�vel 3?","Sim","N�o")	
		
		TEXT TO lcSQL TEXTMERGE NOSHOW 
			DELETE from b_famCategorias
			Where catstamp = '<<lcStamp>>'
		ENDTEXT 
		
		IF uf_gerais_actGrelha("","",lcSql)
			SELECT uCrsPesqMulti

			DELETE		
			TRY 
				SKIP 1
			CATCH 
				SKIP -1
			FINALLY 
				GO TOP 
			ENDTRY
			
			&& Atualiza produtos que t�m class IMS nivel 3 configurada
			IF uf_perguntalt_chama("Pretende remover esta class. comercial n�vel 3 de todos os artigos?","Sim","N�o")
				lcSQL = ''
				TEXT TO lcSQL TEXTMERGE NOSHOW 
					UPDATE st
					SET u_catstamp = ''
					where u_catstamp = '<<lcStamp>>'
				ENDTEXT 
				uf_gerais_actGrelha("","",lcSql)
			ENDIF 
			
			uf_perguntalt_chama("REGISTO ELIMINADO COM SUCESSO.","OK","",64)
		
			pesquisamultidata.refresh
		ELSE 
			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO ELIMINAR O REGISTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF 
	ENDIF 
ENDFUNC 


** introduz classifica��o definida pela farm�cia 
FUNCTION uf_pesqmultidata_introduzirFarmTipoProduto
	LOCAL lcCampo, lcSql, lcRetorno
	STORE "" TO lcSQL

	lcRetorno = uf_dytable_getnome()
	
	IF EMPTY (lcRetorno)
		uf_perguntalt_chama("A DESCRIC�O N�O PODE SER VAZIA!","OK","",48)
		RETURN .f.
	ENDIF
		
	lcCampo = LEFT(STRTRAN(ALLTRIM(lcRetorno),CHR(39),""),60)
	
	IF uf_perguntalt_chama("Vai inserir a classifica��o Farm " + Alltrim(lcCampo) + ". Pretende continuar?","Sim","N�o")
		TEXT TO lcSQL TEXTMERGE NOSHOW 
			INSERT INTO b_famFamilias (famstamp, design, inactivo)
			VALUES  
				(
					isnull((select MAX(CAST(famstamp as int)) + 1 from b_famFamilias),1),
					'<<lcCampo>>',
					0
				)
		ENDTEXT
		IF uf_gerais_actGrelha("","",lcSql)

			SELECT uCrsPesqMulti
			APPEND BLANK 
			
			replace uCrsPesqMulti.sel 	 WITH .f.
			replace uCrsPesqMulti.valor  With ALLTRIM(lcCampo)

			uf_perguntalt_chama("CLASSIFICA��O INTRODUZIDA COM SUCESSO.","OK","",64)
			
			pesquisamultidata.refresh
			
		ELSE 
			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO EFETUAR A OPERA��O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
	ENDIF 
ENDFUNC 


** elimina classifica��o da lista de classifica��es criada pela farm�cia
FUNCTION uf_pesqmultidata_eliminarFarmTipoProduto
	LOCAL lcStamp,lcSql
	STORE "" TO lcSQL,lcStamp
	
	SELECT uCrsPesqMulti
	
	lcStamp = ALLTRIM(uCrsPesqMulti.id)
	
	IF EMPTY(lcStamp)
		uf_perguntalt_chama("POR FAVOR SELECCIONE UM REGISTO.","OK","",48)
		RETURN .f.
	ENDIF
	
	IF uf_perguntalt_chama("Tem a certeza que deseja eliminar a Class. da Farm�cia?","Sim","N�o")	
		
		TEXT TO lcSQL TEXTMERGE NOSHOW 
			DELETE from b_famFamilias
			Where famstamp = '<<lcStamp>>'
		ENDTEXT 
		
		IF uf_gerais_actGrelha("","",lcSql)
			SELECT uCrsPesqMulti

			DELETE		
			TRY 
				SKIP 1
			CATCH 
				SKIP -1
			FINALLY 
				GO TOP 
			ENDTRY
			
			&& Atualiza produtos que t�m class IMS nivel 3 configurada
			IF uf_perguntalt_chama("Pretende remover esta Class. Farm�cia de todos os artigos?","Sim","N�o")
				lcSQL = ''
				TEXT TO lcSQL TEXTMERGE NOSHOW 
					UPDATE st
					SET u_famstamp = ''
					where u_famstamp = '<<lcStamp>>'
				ENDTEXT 
				uf_gerais_actGrelha("","",lcSql)
			ENDIF 
			
			uf_perguntalt_chama("REGISTO ELIMINADO COM SUCESSO.","OK","",64)
		
			pesquisamultidata.refresh
		ELSE 
			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO ELIMINAR O REGISTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF 
	ENDIF 
ENDFUNC

FUNCTION uf_pesqmultidata_novaPatologia
	
	LOCAL lcCampo, lcSql, lcRetorno, lcMarcada
	STORE "" TO lcSQL

	lcRetorno = uf_dytable_getnome()
	
	IF EMPTY (lcRetorno)
		uf_perguntalt_chama("A PATOLOGIA  N�O PODE SER VAZIA!","OK","",48)
		RETURN .f.
	ENDIF
		
	lcCampo = LEFT(STRTRAN(ALLTRIM(lcRetorno),CHR(39),""),100)
	lcMarcada = 0

	IF uf_perguntalt_chama("Vai inserir a Patologia " + Alltrim(lcCampo) + ". Pretende continuar?","Sim","N�o")
		TEXT TO lcSQL TEXTMERGE NOSHOW 
			EXEC up_utentes_insertNovaPatolologia '<<lcCampo>>', '<<m_chinis>>', <<lcMarcada>> 
		ENDTEXT

		IF !uf_gerais_actGrelha("","uCrsNovaPatol",lcSQL)
			uf_perguntalt_chama("Ocorreu umaa anomalia a criar uma nova patologia. Por favor contacte o suporte","OK","",48)
			RETURN .f.
		ENDIF 

			IF 	uCrsNovaPatol.correu = .t.
				
	            SELECT uCrsPesqMulti
	            APPEND BLANK 
	            
	            replace uCrsPesqMulti.sel      WITH .f.
	            replace uCrsPesqMulti.valor  With ALLTRIM(lcCampo)
								
				uf_perguntalt_chama("Patologia inserido com sucesso.","OK","",64)
				
				pesquisamultidata.refresh
				
			ELSE 
				uf_perguntalt_chama("Patologia j� existente!","OK","",16)
				RETURN .f.
			ENDIF
	ENDIF 
ENDFUNC

FUNCTION uf_pesqmultidata_eliminarPatologia
	
	LOCAL lcDescricao,lcSql
	STORE "" TO lcDescricao,lcSql
	
	SELECT uCrsPesqMulti

	lcDescricao = ALLTRIM(uCrsPesqMulti.valor)
	
	IF EMPTY(lcDescricao)
		uf_perguntalt_chama("Por favor selecione uma patologia.","OK","",48)
		RETURN .f.
	ENDIF
	
	IF uf_perguntalt_chama("Tem a certeza que deseja eliminar a Patologia?","Sim","N�o")	
		
		TEXT TO lcSQL TEXTMERGE NOSHOW 
			DELETE from B_ctidef
			Where descricao = '<<lcDescricao>>'
		ENDTEXT 
		
		IF uf_gerais_actGrelha("","",lcSql)
			SELECT uCrsPesqMulti

			DELETE		
			TRY 
				SKIP 1
			CATCH 
				SKIP -1
			FINALLY 
				GO TOP 
			ENDTRY
			
			uf_perguntalt_chama("Patologia eliminada com sucesso.","OK","",64)
		
			pesquisamultidata.refresh
		ELSE 
			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO ELIMINAR A PATOLOGIA. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF 
	ENDIF 
ENDFUNC