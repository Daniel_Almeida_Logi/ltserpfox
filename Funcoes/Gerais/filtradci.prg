
***
FUNCTION uf_filtraDCI_chama
		
	uf_filtraDCI_criarCursor()
				
	IF TYPE("FILTRADCI")=="U"
		DO FORM FILTRADCI
	ELSE
		FILTRADCI.show
	ENDIF
ENDFUNC 

**
PROCEDURE uf_filtraDCI_criarCursor

	TEXT TO msel TEXTMERGE NOSHOW
		Select distinct CAST(0 as bit) as Sel, dcipt_id, dci as Designacao from fprod (nolock) where dci <> '' order by dci
	ENDTEXT

	uf_gerais_actgrelha("","uc_dci",msel)

	CREATE CURSOR uc_linSelDCI (dcipt_id c(10))

	TEXT TO msel TEXTMERGE NOSHOW
		Select distinct CAST(0 as bit) as Sel, grphmgcode as codigo, grphmgdescr as descricao from fprod (nolock) where 1=0 order by grphmgcode
	ENDTEXT

	uf_gerais_actgrelha("","uc_gh",msel)

	CREATE CURSOR uc_linSelGH (codigo c(6))

	TEXT TO msel TEXTMERGE NOSHOW
		Select distinct CAST(0 as bit) as Sel, cnpem from fprod(nolock) where 1=0 order by cnpem
	ENDTEXT

	uf_gerais_actgrelha("","uc_cnpem",msel)

	CREATE CURSOR uc_linselCNPEM (cnpem c(8))

ENDPROC

**
FUNCTION uf_filtraDCI_gravar

	uv_found = .T.

	SELECT uc_dci
	LOCATE FOR uc_dci.sel

	IF !FOUND()
		uv_found = .F.
	ENDIF

*!*		SELECT uc_gh
*!*		LOCATE FOR uc_gh.sel

*!*		IF !FOUND()
*!*			uv_found = .F.
*!*		ENDIF

*!*		SELECT uc_cnpem
*!*		LOCATE FOR uc_cnpem.sel

*!*		IF !FOUND()
*!*			uv_found = .F.
*!*		ENDIF

	IF !uv_found
		uf_perguntalt_chama("N�o selecionou nenhum registo!","OK","",16)
		RETURN .F.
	ENDIF

	IF TYPE("pv_filtradci_dci") <> "U"
		RELEASE pv_filtradci_dci
	ENDIF

	IF TYPE("pv_filtradci_dciDesc") <> "U"
		RELEASE pv_filtradci_dciDesc
	ENDIF

	IF TYPE("pv_filtradci_gh") <> "U"
		RELEASE pv_filtradci_gh
	ENDIF

	IF TYPE("pv_filtradci_ghDesc") <> "U"
		RELEASE pv_filtradci_ghDesc
	ENDIF

	IF TYPE("pv_filtradci_cnpem") <> "U"
		RELEASE pv_filtradci_cnpem
	ENDIF

	IF TYPE("pv_filtradci_cnpemDesc") <> "U"
		RELEASE pv_filtradci_cnpemDesc
	ENDIF


	PUBLIC pv_filtradci_dci, pv_filtradci_gh, pv_filtradci_cnpem, pv_filtradci_dciDesc, pv_filtradci_ghDesc, pv_filtradci_cnpemDesc
	STORE '' TO pv_filtradci_dci, pv_filtradci_gh, pv_filtradci_cnpem, pv_filtradci_dciDesc, pv_filtradci_ghDesc, pv_filtradci_cnpemDesc


	SELECT uc_dci
	GO TOP

	SCAN FOR uc_dci.sel

		pv_filtradci_dci = pv_filtradci_dci + IIF(!EMPTY(pv_filtradci_dci), ";", "") + ALLTRIM(uc_dci.dcipt_id)
        pv_filtradci_dciDesc = pv_filtradci_dciDesc + IIF(!EMPTY(pv_filtradci_dciDesc), ";", "") + ALLTRIM(uc_dci.Designacao)

		SELECT uc_dci
	ENDSCAN

	SELECT uc_gh
	GO TOP

	SCAN FOR uc_gh.sel

		pv_filtradci_gh = pv_filtradci_gh + IIF(!EMPTY(pv_filtradci_gh), ";", "") + ALLTRIM(uc_gh.codigo)
        pv_filtradci_ghDesc = pv_filtradci_ghDesc + IIF(!EMPTY(pv_filtradci_ghDesc), ";", "") + ALLTRIM(uc_gh.descricao)

		SELECT uc_gh
	ENDSCAN

	SELECT uc_cnpem
	GO TOP

	SCAN FOR uc_cnpem.sel

		pv_filtradci_cnpem = pv_filtradci_cnpem + IIF(!EMPTY(pv_filtradci_cnpem), ";", "") + ALLTRIM(uc_cnpem.cnpem)
        pv_filtradci_cnpemDesc = pv_filtradci_cnpemDesc + IIF(!EMPTY(pv_filtradci_cnpemDesc), ";", "")

		SELECT uc_cnpem
	ENDSCAN

	IF TYPE("RELATORIO.pagina2.wdgfiltroreport.containerFiltros.dci") <> "U"
		RELATORIO.pagina2.wdgfiltroreport.containerFiltros.dci.refresh()
	endif

	uf_filtraDCI_sair()

ENDFUNC

**
FUNCTION uf_filtraDCI_vazio

ENDFUNC

**
FUNCTION uf_filtraDCI_sair

	FECHA("uc_dci")
	FECHA("uc_linSelDCI")
	FECHA("uc_gh")
	FECHA("uc_linSelGH")
	FECHA("uc_cnpem")
	FECHA("uc_linSelCNPEM")

	FILTRADCI.hide
	FILTRADCI.release
ENDFUNC

**
PROCEDURE uf_filtraDCI_resize

ENDPROC

**
PROCEDURE uf_filtraDCI_recalcGH

	SELECT * FROM uc_dci WITH (BUFFERING = .T.) WHERE uc_dci.sel = .T. INTO CURSOR uc_dciTmp

	uv_dciFiltro = ""

	SELECT uc_dciTmp
	GO TOP

	SCAN

		uv_dciFiltro = uv_dciFiltro + IIF(!EMPTY(uv_dciFiltro), ",", "") + "'" + ALLTRIM(uc_dciTmp.dcipt_id) + "'"

		SELECT uc_dciTmp
	ENDSCAN

	FECHA("uc_dciTmp")

	IF EMPTY(uv_dciFiltro)

		TEXT TO msel TEXTMERGE NOSHOW
			Select distinct CAST(0 as bit) as Sel, grphmgcode as codigo, grphmgdescr as descricao from fprod (nolock) where 1=0 order by grphmgcode
		ENDTEXT

	ELSE

		TEXT TO msel TEXTMERGE NOSHOW
			Select distinct CAST(0 as bit) as Sel, grphmgcode as codigo, (CASE WHEN grphmgcode ='GH0000' THEN 'Sem Grupo Homogenio' ELSE grphmgcode END)  as descricao 
            from fprod (nolock) where grphmgcode <> '' and grphmgdescr <> '' and dcipt_id in (<<ALLTRIM(uv_dciFiltro)>>) order by grphmgcode
		ENDTEXT

	ENDIF

	loGrid = FILTRADCI.grdGH
	lcCursor = "uc_gh"

	lnColumns = loGrid.ColumnCount
	
	IF lnColumns > 0

		DIMENSION laControlSource[lnColumns]
		FOR lnColumn = 1 TO lnColumns
			laControlSource[lnColumn] = loGrid.Columns[lnColumn].ControlSource
		ENDFOR

	ENDIF
	
	loGrid.RecordSource = ""

	uf_gerais_actgrelha("","uc_gh",msel)

	loGrid.RecordSource = lcCursor
	
	FOR lnColumn = 1 TO lnColumns
		loGrid.Columns[lnColumn].ControlSource = laControlSource[lnColumn]
	ENDFOR

	SELECT uc_linSelGH
	GO TOP

	SCAN

		uv_codigo = uc_linSelGH.codigo

		SELECT uc_gh
		LOCATE FOR ALLTRIM(UPPER(uc_gh.codigo)) == ALLTRIM(UPPER(uv_codigo))

		IF FOUND()

			SELECT uc_gh
			REPLACE uc_gh.sel with .T.
		
		ELSE

			SELECT uc_linSelGH
			DELETE

		ENDIF

		SELECT uc_linSelGH
	ENDSCAN

	FILTRADCI.grdGH.refresh

	uf_filtraDCI_recalcCNPEM()

ENDPROC

**
PROCEDURE uf_filtraDCI_recalcCNPEM

	SELECT * FROM uc_dci WITH (BUFFERING = .T.) WHERE uc_dci.sel = .T. INTO CURSOR uc_dciTmp

	uv_dciFiltro = ""

	SELECT uc_dciTmp
	GO TOP

	SCAN

		uv_dciFiltro = uv_dciFiltro + IIF(!EMPTY(uv_dciFiltro), ",", "") + "'" + ALLTRIM(uc_dciTmp.dcipt_id) + "'"

		SELECT uc_dciTmp
	ENDSCAN

	FECHA("uc_dciTmp")

	SELECT * FROM uc_gh WITH (BUFFERING = .T.) WHERE uc_gh.sel = .T. INTO CURSOR uc_ghTmp

	uv_ghFiltro  = ""

	SELECT uc_ghTmp
	GO TOP

	SCAN

		uv_ghFiltro = uv_ghFiltro + IIF(!EMPTY(uv_ghFiltro), ",", "") + "'" + ALLTRIM(uc_ghTmp.codigo) + "'"

		SELECT uc_ghTmp
	ENDSCAN

	FECHA("uc_ghTmp")

	IF EMPTY(uv_ghFiltro)

		TEXT TO msel TEXTMERGE NOSHOW
			Select distinct CAST(0 as bit) as Sel, cnpem from fprod(nolock) where 1=0 order by cnpem
		ENDTEXT

	ELSE

		TEXT TO msel TEXTMERGE NOSHOW
			Select distinct CAST(0 as bit) as Sel, cnpem from fprod(nolock) where cnpem <> '' and dcipt_id in (<<ALLTRIM(uv_dciFiltro)>>)and grphmgcode in (<<ALLTRIM(uv_ghFiltro)>>) order by cnpem
		ENDTEXT

	ENDIF

	
	loGrid = FILTRADCI.grdCNPEM
	lcCursor = "uc_cnpem"

	lnColumns = loGrid.ColumnCount
	
	IF lnColumns > 0

		DIMENSION laControlSource[lnColumns]
		FOR lnColumn = 1 TO lnColumns
			laControlSource[lnColumn] = loGrid.Columns[lnColumn].ControlSource
		ENDFOR

	ENDIF
	
	loGrid.RecordSource = ""

	uf_gerais_actgrelha("","uc_cnpem",msel)

	loGrid.RecordSource = lcCursor
	
	FOR lnColumn = 1 TO lnColumns
		loGrid.Columns[lnColumn].ControlSource = laControlSource[lnColumn]
	ENDFOR

	SELECT uc_linSelCNPEM
	GO TOP

	SCAN

		uv_cnpem = uc_linSelCNPEM.cnpem

		SELECT uc_cnpem
		LOCATE FOR ALLTRIM(UPPER(uc_cnpem.cnpem)) == ALLTRIM(UPPER(uv_cnpem))

		IF FOUND()

			SELECT uc_cnpem
			REPLACE uc_cnpem.sel with .T.
		
		ELSE

			SELECT uc_linSelCNPEM
			DELETE

		ENDIF

		SELECT uc_linSelCNPEM
	ENDSCAN

	FILTRADCI.grdCNPEM.refresh
ENDPROC

**
PROCEDURE uf_filtraDCI_filterDCI

	SELECT uc_dci
	

	IF !EMPTY(FILTRADCI.txtDCI.Value)

		SET FILTER TO LIKE( UPPER(ALLTRIM(FILTRADCI.txtDCI.Value)) + "*", UPPER(ALLTRIM(uc_dci.Designacao)))

	ELSE

		SET FILTER TO

	ENDIF

	SELECT uc_dci
	GO TOP

	FILTRADCI.grdDCI.refresh()

ENDPROC

**
PROCEDURE uf_filtraDCI_filterGH

	SELECT uc_gh
	GO TOP

	IF !EMPTY(FILTRADCI.txtGH.Value)

		SET FILTER TO LIKE( UPPER(ALLTRIM(FILTRADCI.txtGH.Value)) + "*", UPPER(ALLTRIM(uc_gh.descricao)))

	ELSE

		SET FILTER TO

	ENDIF

	SELECT uc_gh
	GO TOP

	FILTRADCI.grdGH.refresh()

ENDPROC

**
PROCEDURE uf_filtraDCI_filterCNPEM

	SELECT uc_cnpem
	GO TOP

	IF !EMPTY(FILTRADCI.txtCNPEM.Value)

		SET FILTER TO LIKE( UPPER(ALLTRIM(FILTRADCI.txtCNPEM.Value)) + "*", UPPER(ALLTRIM(uc_cnpem.cnpem)))

	ELSE

		SET FILTER TO

	ENDIF

	SELECT uc_cnpem
	GO TOP

	FILTRADCI.grdCNPEM.refresh()

ENDPROC

PROCEDURE uf_filtraDCI_selDCI
	LPARAMETERS uv_progClick

	IF FILTRADCI.selDCI
		FILTRADCI.selDCI = .F.
		IF !uv_progClick
		
			DELETE FROM uc_linSelDCI

			SELECT uc_dci
			GO TOP

			REPLACE uc_dci.sel with .F. FOR 1=1

			SELECT uc_dci
			GO TOP

		ENDIF
	ELSE
		FILTRADCI.selDCI = .T.
		IF !uv_progClick

			FILTRADCI.grdDCI.setFocus()

			SELECT uc_dci
			
			REPLACE uc_dci.sel WITH .T. FOR 1=1

			INSERT INTO uc_linSelDCI SELECT dcipt_id FROM uc_dci WITH (BUFFERING = .T.) WHERE uc_dci.sel AND !DELETED()

			SELECT uc_dci
			GO TOP

		ENDIF
	ENDIF

	FILTRADCI.grdDCI.refresh()

	uf_filtraDCI_recalcGH()

ENDPROC

PROCEDURE uf_filtraDCI_selLinDCI

	SELECT uc_dci

	IF uc_dci.sel

		SELECT uc_linSelDCI
		APPEND BLANK
		REPLACE uc_linSelDCI.dcipt_id WITH uc_dci.dcipt_id
		
		IF !FILTRADCI.selDCI
		
			SELECT * FROM uc_dci WITH (BUFFERING = .T.) INTO CURSOR uc_selDCI

			SELECT uc_selDCI
			COUNT TO uv_count FOR !DELETED() AND uc_selDCI.sel

			FECHA("uc_selDCI")
			
			IF uv_count = 0

				FILTRADCI.chkDCI.click(.T.)

			ENDIF

		ENDIF
		
	ELSE

		SELECT uc_linSelDCI
		DELETE FOR ALLTRIM(uc_linSelDCI.dcipt_id) == ALLTRIM(uc_dci.dcipt_id)
		
		IF FILTRADCI.selDCI
		
			FILTRADCI.chkDCI.click(.T.)
		
		ENDIF
		
	ENDIF

	uf_filtraDCI_recalcGH()

ENDPROC

PROCEDURE uf_filtraDCI_selGH
	LPARAMETERS uv_progClick

	IF FILTRADCI.selGH
		FILTRADCI.selGH = .F.
		IF !uv_progClick
		
			DELETE FROM uc_linSelGH

			SELECT uc_GH
			GO TOP

			REPLACE uc_GH.sel with .F. FOR 1=1

			SELECT uc_GH
			GO TOP

		ENDIF
	ELSE
		FILTRADCI.selGH = .T.
		IF !uv_progClick

			FILTRADCI.grdGH.setFocus()

			SELECT uc_GH
			
			REPLACE uc_GH.sel WITH .T. FOR 1=1

			INSERT INTO uc_linSelGH SELECT codigo FROM uc_gh WITH (BUFFERING = .T.) WHERE uc_gh.sel AND !DELETED()

			SELECT uc_gh
			GO TOP

		ENDIF
	ENDIF

	FILTRADCI.grdGH.refresh()

	uf_filtraDCI_recalcCNPEM()

ENDPROC

PROCEDURE uf_filtraDCI_selLinGH

	SELECT uc_GH

	IF uc_GH.sel

		SELECT uc_linSelGH
		APPEND BLANK
		REPLACE uc_linSelGH.codigo WITH uc_gh.codigo
		
		IF !FILTRADCI.selGH
		
			SELECT * FROM uc_gh WITH (BUFFERING = .T.) INTO CURSOR uc_selGH

			SELECT uc_selGH
			COUNT TO uv_count FOR !DELETED() AND uc_selGH.sel

			FECHA("uc_selGH")
			
			IF uv_count = 0

				FILTRADCI.chkGH.click(.T.)

			ENDIF

		ENDIF
		
	ELSE

		SELECT uc_linSelGH
		DELETE FOR ALLTRIM(uc_linSelGH.codigo) == ALLTRIM(uc_gh.codigo)
		
		IF FILTRADCI.selGH
		
			FILTRADCI.chkGH.click(.T.)
		
		ENDIF
		
	ENDIF

	uf_filtraDCI_recalcCNPEM()

ENDPROC

PROCEDURE uf_filtraDCI_selCNPEM
	LPARAMETERS uv_progClick

	IF FILTRADCI.selCNPEM
		FILTRADCI.selCNPEM = .F.
		IF !uv_progClick
		
			DELETE FROM uc_linSelCNPEM

			SELECT uc_CNPEM
			GO TOP

			REPLACE uc_CNPEM.sel with .F. FOR 1=1

			SELECT uc_CNPEM
			GO TOP

		ENDIF
	ELSE
		FILTRADCI.selCNPEM = .T.
		IF !uv_progClick

			FILTRADCI.grdCNPEM.setFocus()

			SELECT uc_CNPEM
			
			REPLACE uc_CNPEM.sel WITH .T. FOR 1=1

			INSERT INTO uc_linSelCNPEM SELECT CNPEM FROM uc_CNPEM WITH (BUFFERING = .T.) WHERE uc_CNPEM.sel AND !DELETED()

			SELECT uc_CNPEM
			GO TOP

		ENDIF
	ENDIF

	FILTRADCI.grdCNPEM.refresh()

ENDPROC

PROCEDURE uf_filtraDCI_selLinCNPEM

	SELECT uc_CNPEM

	IF uc_CNPEM.sel

		SELECT uc_linSelCNPEM
		APPEND BLANK
		REPLACE uc_linSelCNPEM.CNPEM WITH uc_CNPEM.CNPEM
		
		IF !FILTRADCI.selCNPEM
		
			SELECT * FROM uc_CNPEM WITH (BUFFERING = .T.) INTO CURSOR uc_selCNPEM

			SELECT uc_selCNPEM
			COUNT TO uv_count FOR !DELETED() AND uc_selCNPEM.sel

			FECHA("uc_selCNPEM")
			
			IF uv_count = 0

				FILTRADCI.chkCNPEM.click(.T.)

			ENDIF

		ENDIF
		
	ELSE

		SELECT uc_linSelCNPEM
		DELETE FOR ALLTRIM(uc_linSelCNPEM.CNPEM) == ALLTRIM(uc_CNPEM.CNPEM)
		
		IF FILTRADCI.selCNPEM
		
			FILTRADCI.chkCNPEM.click(.T.)
		
		ENDIF
		
	ENDIF

ENDPROC

