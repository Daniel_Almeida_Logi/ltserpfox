**
FUNCTION uf_perguntalt_chama
	LPARAMETERS tcTextoPergunta, lcLabelGravar, lcLabelSair, lcTipo, lcTempoFecho, lcLabelCancelar, lcImgCancelar, lcImgGravar, lcImgSair
	PUBLIC myRetornoPergunta, myRetornoCancelar
	myRetornoPergunta = .f.
	myRetornoCancelar = .f.
	
	**
	IF TYPE("PERGUNTALT") == 'U'
		DO FORM PERGUNTALT WITH tcTextoPergunta, lcLabelGravar, lcLabelSair, lcTipo, lcTempoFecho, lcLabelCancelar, lcImgCancelar, lcImgGravar, lcImgSair
	ELSE
		PERGUNTALT.show
	ENDIF
	
	IF !EMPTY(lcTempoFecho)
		PERGUNTALT.tempo = lcTempoFecho && Tempo em segundos
		PERGUNTALT.labelTempo.visible =  .t.
		PERGUNTALT.timerFecho.enabled = .t.
	ENDIF
	
	RETURN myRetornoPergunta

ENDFUNC

*!*	**
*!*	FUNCTION uf_perguntalt_timerFecho
*!*		
*!*		IF EMPTY(PERGUNTALT.tempo) &&= 0
*!*			uf_perguntalt_sair()
*!*			RETURN .f.
*!*		ENDIF 

*!*		PERGUNTALT.tempo = PERGUNTALT.tempo - 1
*!*		PERGUNTALT.labelTempo.Caption = "(" + ASTR(PERGUNTALT.tempo) + ")"
*!*	ENDFUNC

**
FUNCTION uf_perguntalt_gravar
	
	TRY 
		PERGUNTALT.hide
		PERGUNTALT.release

		myRetornoPergunta = .t.
	ENDTRY
	
ENDFUNC

**
FUNCTION uf_perguntalt_sair
	TRY 
		PERGUNTALT.hide
		PERGUNTALT.release
		
		myRetornoPergunta = .f.
	ENDTRY
ENDFUNC

**
FUNCTION uf_perguntalt_cancelar
	TRY 
		PERGUNTALT.hide
		PERGUNTALT.release

		myRetornoCancelar = .t.
	ENDTRY 
ENDFUNC

FUNCTION uf_perguntalt_responsivo_chama
	LPARAMETERS tcTextoPergunta, lcLabelGravar, lcLabelSair, lcTipo, lcTempoFecho, lcLabelCancelar, lcImgCancelar, lcImgGravar, lcImgSair
	PUBLIC myRetornoPergunta, myRetornoCancelar
	myRetornoPergunta = .f.
	myRetornoCancelar = .f.
	
	**
	IF TYPE("PERGUNTALT") == 'U'
		DO FORM PERGUNTALT WITH tcTextoPergunta, lcLabelGravar, lcLabelSair, lcTipo, lcTempoFecho, lcLabelCancelar, lcImgCancelar, lcImgGravar, lcImgSair, .T.
	ELSE
		PERGUNTALT.show
	ENDIF
	
	IF !EMPTY(lcTempoFecho)
		PERGUNTALT.tempo = lcTempoFecho && Tempo em segundos
		PERGUNTALT.labelTempo.visible =  .t.
		PERGUNTALT.timerFecho.enabled = .t.
	ENDIF
	
	RETURN myRetornoPergunta

ENDFUNC