**
Function uf_imprimirgeraisbulk_Chama
	
	If Type("IMPRIMIRGERAISBULK") == "U"
			**
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_email_emailTextoAutomatico
		ENDTEXT
		IF !uf_gerais_actGrelha("","ucrsEmailTextoAutomatico",lcSQL)
			uf_perguntalt_chama("N�O FOI POSSIVEL DETERIMNAR OS DADOS EXISTENTES PARA AS NOTIFICA��ES. POR FAVOR CONTACTE O SUPORTE","OK","",16)
			RETURN .f.
		ENDIF
		Do Form IMPRIMIRGERAISBULK
	Else
		IMPRIMIRGERAISBULK.Show
		IMPRIMIRGERAISBULK.Refresh
	Endif
	

Endfunc
**************************************************************************************************************************************
Function uf_imprimirgeraisbulk_gravar
	
	CREATE CURSOR uCrsEmailData (subject C(250), body M)
	
	
	select uCrsEmailData 
	GO TOP
	APPEND BLANK 	
	REPLACE uCrsEmailData.subject WITH ALLTRIM(IMPRIMIRGERAISBULK.pageframe1.page1.emailsubject.Value)
	REPLACE uCrsEmailData.body WITH ALLTRIM(IMPRIMIRGERAISBULK.pageframe1.page1.emailbody.Value)

	Select ucrsEmailTextoAutomatico
	GO TOP 
	SCAN
		select uCrsEmailData
		Replace uCrsEmailData.body WITH STRTRAN(uCrsEmailData.body, ALLTRIM(ucrsEmailTextoAutomatico.cod), "<<"+ALLTRIM(ucrsEmailTextoAutomatico.cursor)+"."+ALLTRIM(ucrsEmailTextoAutomatico.campo)+">>")
	ENDSCAN 

	
	
	
	uf_imprimirgeraisbulk_sair()
ENDFUNC
**************************************************************************************************************************************
Function uf_imprimirgeraisbulk_sair
	RELEASE myImprimirGeraisBulkCrs
	
	
	IMPRIMIRGERAISBULK.hide
	IMPRIMIRGERAISBULK.Release
	
	
ENDFUNC 

**************************************************************************************************************************************

FUNCTION uf_iprimirgeraisBulk_insereTexto
	
	SELECT ucrsEmailTextoAutomatico
	
	IF !EMPTY(ALLTRIM(ImprimirGeraisBulk.tautomatico.Value))
		
		select ucrsEmailTextoAutomatico
		LOCATE for ALLTRIM(ucrsEmailTextoAutomatico.desc) == ALLTRIM(ImprimirGeraisBulk.tautomatico.Value)
		
		IMPRIMIRGERAISBULK.pageframe1.page1.emailbody.Value = ALLTRIM(IMPRIMIRGERAISBULK.pageframe1.page1.emailbody.Value) +  ucrsEmailTextoAutomatico.cod

		IMPRIMIRGERAISBULK.pageframe1.page1.emailbody.refresh
	ENDIF
		
ENDFUNC 
**************************************************************************************************************************************