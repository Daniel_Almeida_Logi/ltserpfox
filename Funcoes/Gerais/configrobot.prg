FUNCTION uf_configRobot_chama
	uf_configRobot_carregaDados()
	DO FORM configRobot
	
ENDFUNC

FUNCTION uf_configRobot_carregaDados
	IF used("ucrsDadosRobot")
		fecha("ucrsDadosRobot")
	ENDIF
	IF used("ucrsDadosRobotAux")
		fecha("ucrsDadosRobotAux")
	ENDIF
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select 
			term.site
			,term.terminal
			,term.drop_location
			,term.tstamp 
			,ISNULL(term.xopvision_terminal_id, 0) as xopvision_terminal_id
			,ISNULL(tpa.ip, '') as TPA
			,term.pathAnexos
			,term.posprinter
			,term.labelprinter
            ,term.pathExport
            ,term.pathReport
			,term.usaDriverWin
            ,term.nUsaTSPrint
			,ISNULL(term.xopvision_pos_terminal_id, 0) as xopvision_pos_terminal_id
			,ISNULL(term.cash_machine, '') AS cash_machine
			,ISNULL(tpa.port, 0) as tpaPort
			,term.gaveta
		from 
			B_Terminal (nolock) AS term
			left join tpa_map(nolock) as tpa on tpa.termstamp = term.tstamp
		order by 
			term.site
			,term.no
	ENDTEXT
	IF !uf_gerais_actGrelha("","ucrsDadosRobot",lcSQL)
		uf_perguntalt_chama("N�o foi poss�vel obter dados do robot. Por favor contacte o Suporte. Obrigado.","OK","",16)
		RETURN .f.
	ELSE
		SELECT * FROM ucrsDadosRobot INTO CURSOR ucrsDadosRobotAux READWRITE
	ENDIF
ENDFUNC

FUNCTION uf_configRobot_sair

	IF used("ucrsDadosRobot")
		fecha("ucrsDadosRobot")
	ENDIF
	IF used("ucrsDadosRobotAux")
		fecha("ucrsDadosRobotAux")
	ENDIF
	
	configRobot.hide
	configRobot.Release
		
	RELEASE configRobot
ENDFUNC

** grava altera��s feitas �s drop location definidas para cada posto
FUNCTION uf_configrobot_gravar
	
	LOCAL lcSql2, lcSql
	STORE '' to lcSql2
	STORE '' to lcSql
	
	SELECT ucrsDadosRobot
	GO TOP
	SCAN

        uv_createAnexos = .F.
        uv_createExport = .F.
        uv_createReport = .F.

		SELECT ucrsDadosRobotAux
		GO TOP
		SCAN FOR ALLTRIM(ucrsDadosRobot.tstamp) == ALLTRIM(ucrsDadosRobotAux.tstamp)



			SET EXACT ON

            uv_createAnexos = IIF(ALLTRIM(ucrsDadosRobot.pathAnexos) <> ALLTRIM(ucrsDadosRobotAux.pathAnexos) AND !EMPTY(ucrsDadosRobot.pathAnexos), .T., .F.)
            uv_createExport = IIF(ALLTRIM(ucrsDadosRobot.pathExport) <> ALLTRIM(ucrsDadosRobotAux.pathExport) AND !EMPTY(ucrsDadosRobot.pathExport), .T., .F.)
            uv_createReport = IIF(ALLTRIM(ucrsDadosRobot.pathReport) <> ALLTRIM(ucrsDadosRobotAux.pathReport) AND !EMPTY(ucrsDadosRobot.pathReport), .T., .F.)

			uv_updateTerm = ""
			uv_updateTerm = IIF(ALLTRIM(ucrsDadosRobot.drop_location) <> ALLTRIM(ucrsDadosRobotAux.drop_location), "B_Terminal.drop_location = '" + alltrim(ucrsDadosRobot.drop_location) + "'", "")
			uv_updateTerm = uv_updateTerm + IIF(ucrsDadosRobot.xopvision_terminal_id <> ucrsDadosRobotAux.xopvision_terminal_id, IIF(!EMPTY(uv_updateTerm), ",", "") + "B_Terminal.xopvision_terminal_id = " + ASTR(ucrsDadosRobot.xopvision_terminal_id), "")
			uv_updateTerm = uv_updateTerm + IIF(ucrsDadosRobot.xopvision_pos_terminal_id <> ucrsDadosRobotAux.xopvision_pos_terminal_id, IIF(!EMPTY(uv_updateTerm), ",", "") + "B_Terminal.xopvision_pos_terminal_id = " + ASTR(ucrsDadosRobot.xopvision_pos_terminal_id), "")
			uv_updateTerm = uv_updateTerm + IIF(ALLTRIM(ucrsDadosRobot.pathAnexos) <> ALLTRIM(ucrsDadosRobotAux.pathAnexos), IIF(!EMPTY(uv_updateTerm), ",", "") + "B_Terminal.pathAnexos = '" + ALLTRIM(ucrsDadosRobot.pathAnexos) + "'", "")
            uv_updateTerm = uv_updateTerm + IIF(ALLTRIM(ucrsDadosRobot.pathExport) <> ALLTRIM(ucrsDadosRobotAux.pathExport), IIF(!EMPTY(uv_updateTerm), ",", "") + "B_Terminal.pathExport = '" + ALLTRIM(ucrsDadosRobot.pathExport) + "'", "")
            uv_updateTerm = uv_updateTerm + IIF(ALLTRIM(ucrsDadosRobot.pathReport) <> ALLTRIM(ucrsDadosRobotAux.pathReport), IIF(!EMPTY(uv_updateTerm), ",", "") + "B_Terminal.pathReport = '" + ALLTRIM(ucrsDadosRobot.pathReport) + "'", "")
			uv_updateTerm = uv_updateTerm + IIF(ALLTRIM(ucrsDadosRobot.posprinter) <> ALLTRIM(ucrsDadosRobotAux.posprinter), IIF(!EMPTY(uv_updateTerm), ",", "") + "B_Terminal.posprinter = '" + ALLTRIM(ucrsDadosRobot.posprinter) + "'","")
			uv_updateTerm = uv_updateTerm + IIF(ALLTRIM(ucrsDadosRobot.labelprinter) <> ALLTRIM(ucrsDadosRobotAux.labelprinter), IIF(!EMPTY(uv_updateTerm), ",", "") + "B_Terminal.labelprinter = '" + ALLTRIM(ucrsDadosRobot.labelprinter) + "'","")
			uv_updateTerm = uv_updateTerm + IIF(ucrsDadosRobot.usaDriverWin <> ucrsDadosRobotAux.usaDriverWin, IIF(!EMPTY(uv_updateTerm), ",", "") + "B_Terminal.usaDriverWin = " + IIF(ucrsDadosRobot.usaDriverWin, '1', '0'),"")
            uv_updateTerm = uv_updateTerm + IIF(ucrsDadosRobot.nUsaTSPrint <> ucrsDadosRobotAux.nUsaTSPrint, IIF(!EMPTY(uv_updateTerm), ",", "") + "B_Terminal.nUsaTSPrint = " + IIF(ucrsDadosRobot.nUsaTSPrint, '1', '0'),"")
			uv_updateTerm = uv_updateTerm + IIF(ALLTRIM(ucrsDadosRobot.cash_machine) <> ALLTRIM(ucrsDadosRobotAux.cash_machine), IIF(!EMPTY(uv_updateTerm), ",", "") + "B_Terminal.cash_machine= '" + ALLTRIM(ucrsDadosRobot.cash_machine) + "'","")
            uv_updateTerm = uv_updateTerm + IIF(ucrsDadosRobot.gaveta <> ucrsDadosRobotAux.gaveta , IIF(!EMPTY(uv_updateTerm), ",", "") + "B_Terminal.gaveta = " + IIF(ucrsDadosRobot.gaveta, '1', '0'),"")

			uv_updateTPA = ""
			uv_updateTPA = IIF(ALLTRIM(ucrsDadosRobot.tpa) <> ALLTRIM(ucrsDadosRobotAux.tpa), "tpa_map.ip = '" + ALLTRIM(ucrsDadosRobot.tpa) + "'", "")
			uv_updateTPA = uv_updateTPA + IIF(ucrsDadosRobot.tpaPort <> ucrsDadosRobotAux.tpaPort, IIF(!EMPTY(ALLTRIM(uv_updateTPA)), ",", "") + "tpa_map.port = " + ASTR(ucrsDadosRobot.tpaPort), "")

			SET EXACT OFF
			
			IF !EMPTY(uv_updateTerm)

				TEXT TO lcSql NOSHOW TEXTMERGE
					UPDATE B_Terminal SET <<ALLTRIM(uv_updateTerm)>> WHERE B_Terminal.tstamp = '<<ALLTRIM(ucrsDadosRobot.tstamp)>>'
				ENDTEXT

				lcSql2 = lcSql2 + lcSql + chr(10)+chr(13)

			ENDIF

			IF !EMPTY(uv_updateTPA)

				IF uf_gerais_getUmValor("tpa_map", "count(*)", "tpa_map.termstamp = '" + ALLTRIM(ucrsDadosRobot.tstamp) + "'") = 1

					IF !EMPTY(ALLTRIM(ucrsDadosRobot.tpa)) OR !EMPTY(ucrsDadosRobot.tpaPort)

						TEXT TO lcSql NOSHOW TEXTMERGE
							UPDATE tpa_map SET <<ALLTRIM(uv_updateTPA)>> WHERE tpa_map.termstamp = '<<ALLTRIM(ucrsDadosRobot.tstamp)>>'
						ENDTEXT

					ELSE

						TEXT TO lcSql NOSHOW TEXTMERGE
							DELETE FROM tpa_map WHERE tpa_map.termstamp = '<<ALLTRIM(ucrsDadosRobot.tstamp)>>'
						ENDTEXT 


					ENDIF

				ELSE

					TEXT TO lcSQL NOSHOW TEXTMERGE

						INSERT INTO [dbo].[tpa_map]
								([stamp]
								,[termstamp]
								,[ip]
								,[port]
								,[version]
								,[model]
								,[specification_version]
								,[oursdata]
								,[ousrhora]
								,[ousrinis]
								,[usrdata]
								,[usrinis]
								,[usrhora])
							VALUES
								(LEFT(NEWID(), 15) + RIGHT(NEWID(), 10)
								,'<<ALLTRIM(ucrsDadosRobot.tstamp)>>'
								,'<<ALLTRIM(ucrsDadosRobot.tpa)>>'
								,<<astr(ucrsDadosRobot.tpaPort)>>
								,'05'
								,'IH'
								,'0200'
								,CONVERT(VARCHAR, GETDATE(), 112)
								,LEFT(CONVERT(TIME,GETDATE()),8)
								,'<<m_chinis>>'
								,CONVERT(VARCHAR, GETDATE(), 112)
								,'<<m_chinis>>'
								,LEFT(CONVERT(TIME,GETDATE()),6))

					ENDTEXT

				ENDIF

				lcSql2 = lcSql2 + lcSql + chr(10)+chr(13)

			ENDIF

			SELECT ucrsDadosRobotAux
		ENDSCAN

        IF uv_createAnexos

            IF !DIRECTORY(ucrsDadosRobot.pathAnexos)

                uv_path = ALLTRIM(ucrsDadosRobot.pathAnexos)

                TRY

                    MKDIR &uv_path.

                CATCH TO oError WHEN .T.

                    uf_perguntalt_chama("Ocurreu um erro ao gerar a pasta de Anexos no " + ALLTRIM(ucrsDadosRobot.terminal) + ". Por favor contacte o Suporte. Obrigado.","OK","",16)

                ENDTRY

            ENDIF

        ENDIF

        IF uv_createExport

            IF !DIRECTORY(ucrsDadosRobot.pathExport)

                uv_path = ALLTRIM(ucrsDadosRobot.pathExport)

                TRY

                    MKDIR &uv_path.

                CATCH TO oError WHEN .T.

                    uf_perguntalt_chama("Ocurreu um erro ao gerar a pasta de Documentos Exportados no " + ALLTRIM(ucrsDadosRobot.terminal) + ". Por favor contacte o Suporte. Obrigado.","OK","",16)

                ENDTRY
                
            ENDIF

        ENDIF

        IF uv_createReport

            IF !DIRECTORY(ucrsDadosRobot.pathReport)

                uv_path = ALLTRIM(ucrsDadosRobot.pathReport)

                TRY

                    MKDIR &uv_path.

                CATCH TO oError WHEN .T.

                    uf_perguntalt_chama("Ocurreu um erro ao gerar a pasta de Relat�rios no " + ALLTRIM(ucrsDadosRobot.terminal) + ". Por favor contacte o Suporte. Obrigado.","OK","",16)

                ENDTRY
                
            ENDIF

        ENDIF
		
		SELECT ucrsDadosRobot
	ENDSCAN
	
	IF !EMPTY(lcSql2)
		
		IF !uf_gerais_actGrelha("","ucrsDadosRobot",lcSQL2)
			uf_perguntalt_chama("Ocorreu um erro a gravar altera��es. Por favor contacte o Suporte. Obrigado.","OK","",16)
			RETURN .f.
		ELSE
			** success message
			uf_perguntalt_chama("Atualiza��o concluida com sucesso!","OK","",64)
		ENDIF
	ENDIF		
	
	uf_configRobot_sair()
	
ENDFUNC

