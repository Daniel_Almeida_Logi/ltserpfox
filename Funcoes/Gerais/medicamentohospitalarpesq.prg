FUNCTION uf_medicamentoHospitalarPesq_Chama

	lcSQL=''
	TEXT TO lcSQL TEXTMERGE noshow
		SET FMTONLY ON
		exec up_medicamentoHospitalar_pesquisa '','','',''
		SET FMTONLY OFF
	ENDTEXT	

	uf_gerais_actGrelha("","uCrsMedicamentoHospitalarPesq",lcSQL)

	IF TYPE("MedicamentoHospitalarPesq")=="U"
		DO FORM MedicamentoHospitalarPesq
	ELSE
		MedicamentoHospitalarPesq.show
	ENDI
ENDFUNC

*****************************************
*	 Fun��o que Pesquisa Vacinacaoes 	*
*****************************************
FUNCTION uf_MedicamentoHospitalarPesq_Pesquisar
	lcSQL=''
	TEXT TO lcSQL TEXTMERGE noshow
		up_medicamentoHospitalar_pesquisa '<<uf_gerais_getDate(MedicamentoHospitalarPesq.txtDataini.value,"SQL")>>','<<uf_gerais_getDate(MedicamentoHospitalarPesq.txtDataFim.value,"SQL")>>','<<ALLTRIM(MedicamentoHospitalarPesq.txtCliente.value)>>','<<ALLTRIM(MedicamentoHospitalarPesq.txtAdministrante.value)>>'
	ENDTEXT	

	uf_gerais_actGrelha("MedicamentoHospitalarPesq.grdPesq","uCrsMedicamentoHospitalarPesq",lcSQL)
ENDFUNC 

*****************************************************
*		Fechar Ecra de Pesquisa de Vacinca��es		*
*****************************************************
FUNCTION uf_MedicamentoHospitalarPesq_sair
	IF USED("uCrsMedicamentoHospitalarPesq")
		fecha("uCrsMedicamentoHospitalarPesq")
	ENDIF 
	
	MedicamentoHospitalarPesq.hide
	MedicamentoHospitalarPesq.release
ENDFUNC

FUNCTION uf_MedicamentoHospitalarPesq_topo
	IF USED("uCrsMedicamentoHospitalarPesq")
		IF RECCOUNT("uCrsMedicamentoHospitalarPesq")>0
			SELECT uCrsMedicamentoHospitalarPesq
			GO top
		ENDIF
		MedicamentoHospitalarPesq.grdPesq.refresh
	ENDIF
ENDFUNC

FUNCTION uf_MedicamentoHospitalarPesq_fundo
	IF USED("uCrsMedicamentoHospitalarPesq")
		IF RECCOUNT("uCrsMedicamentoHospitalarPesq")>0
			SELECT uCrsMedicamentoHospitalarPesq
			GO bottom
		ENDIF
		MedicamentoHospitalarPesq.grdPesq.refresh
	ENDIF
ENDFUNC 

FUNCTION uf_MedicamentoHospitalarPesq_teclado
	MedicamentoHospitalarPesq.tecladoVirtual1.show("MedicamentoHospitalarPesq.grdPesq", 161, "MedicamentoHospitalarPesq.shape1", 161)
ENDFUNC 