**
FUNCTION uf_criarclatend_chama
 lcsql = ""
 TEXT TO lcsql TEXTMERGE NOSHOW
		SET fmtonly on select * from b_utentes (nolock) where 1=0 SET fmtonly off
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsInCLAtend", lcsql)
    uf_perguntalt_chama("N�O FOI POSS�VEL CRIAR CURSOR DE CLIENTES.", "OK", "", 16)
    RETURN .F.
 ENDIF
 SELECT ucrsinclatend
 APPEND BLANK
 lcsql = ""
 TEXT TO lcsql TEXTMERGE NOSHOW
		select descricao,ctiefpstamp from b_ctidef (NOLOCK) order by descricao
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsCmbPat1", lcsql)
    uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR PATOLOGIAS.", "OK", "", 16)
 ELSE
    SELECT * FROM uCrsCmbPat1 INTO CURSOR uCrsCmbPat2 READWRITE
 ENDIF
 lcsql = ""
 TEXT TO lcsql TEXTMERGE NOSHOW
		select ccusto from cct (nolock)
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsCmbCCusto", lcsql)
    uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR CENTROS DE CUSTO.", "OK", "", 16)
 ENDIF
 IF TYPE("CRIARCLATEND")=="U"
    DO FORM CRIARCLATEND
 ELSE
    criarclatend.show
 ENDIF
 IF TYPE("CRIARCLATEND.menu1.actCartao")=="U"
    criarclatend.menu1.adicionaopcao("OPCOES", "Op��es", mypath+"\imagens\icons\opcoes_w.png", "", "F1")
    criarclatend.menu1.adicionaopcao("actCartao", "Act. Cart�o", mypath+"\imagens\icons\actualizar_w.png", "uf_criarclatend_actNrCartao", "F2")
    criarclatend.menu1.adicionaopcao("dependente", "Dependente", mypath+"\imagens\icons\unchecked_w.png", "uf_criarclatend_dependente with .t.", "F3")
 ENDIF
 criarclatend.menu1.estado("", "", "GRAVAR", .T.)
 IF uf_gerais_getparameter("ADM0000000022", "BOOL")
    criarclatend.txttlf.backcolor = RGB(191, 223, 223)
    criarclatend.txttlm.backcolor = RGB(191, 223, 223)
 ENDIF
 IF uf_gerais_getparameter("ADM0000000023", "BOOL")
    criarclatend.txtdatanasc.backcolor = RGB(191, 223, 223)
 ENDIF
 IF uf_gerais_getparameter("ADM0000000046", "BOOL")
    criarclatend.txtncont.backcolor = RGB(191, 223, 223)
 ENDIF
 IF uf_gerais_getparameter("ADM0000000112", "BOOL")
    criarclatend.txtmorada.backcolor = RGB(191, 223, 223)
 ENDIF
 IF uf_gerais_getparameter("ADM0000000145", "BOOL")
    criarclatend.txtncartao.readonly = .F.
 ENDIF
 IF uf_gerais_getparameter("ADM0000000305", "BOOL")
    criarclatend.txtbi.backcolor = RGB(191, 223, 223)
 ENDIF
 criarclatend.txtdatanasc.value = uf_gerais_getdate('01.01.1900')
 criarclatend.txtvalidade.value = uf_gerais_getdate('01.01.1900')
 uf_utentes_valoresdefeito("uCrsInCLAtend", "CRIARCLATEND")
 criarclatend.txtnome.setfocus
ENDFUNC
**
FUNCTION uf_criarclatend_gravar
 IF criarclatend.editar==1
    uf_utentes_update("uCrsInCLAtend")
    uf_atendimento_selcliente(ucrsinclatend.no, ucrsinclatend.estab, "ATENDIMENTO")
 ELSE
    LOCAL lcstamp
    IF USED("uCrsPatClAtend")
       SELECT ucrspatclatend
       DELETE ALL
    ELSE
       CREATE CURSOR uCrsPatClAtend (patostamp VARCHAR(25), ctiefpstamp VARCHAR(25))
    ENDIF
    IF  .NOT. EMPTY(criarclatend.cmbpat1.value)
       lcstamp = uf_gerais_stamp()
       SELECT ucrspatclatend
       APPEND BLANK
       REPLACE patostamp WITH lcstamp
       REPLACE ctiefpstamp WITH criarclatend.cmbpat1.value
    ENDIF
    IF  .NOT. EMPTY(criarclatend.cmbpat2.value)
       lcstamp = uf_gerais_stamp()
       SELECT ucrspatclatend
       APPEND BLANK
       REPLACE patostamp WITH lcstamp
       REPLACE ctiefpstamp WITH criarclatend.cmbpat2.value
    ENDIF
    uf_utentes_contasnc(.T., "CRIARCLATEND")
    LOCAL lcvalida
    STORE .F. TO lcvalida
    lcvalida = uf_utente_validacoes("uCrsInCLAtend")
    IF lcvalida==.T.
       IF  .NOT. uf_utentes_gravarbd("uCrsInCLAtend", "CRIARCLATEND", "uCrsPatCLAtend")
          RETURN .F.
       ENDIF
    ELSE
       RETURN .F.
    ENDIF
    IF  .NOT. (TYPE("ATENDIMENTO")=="U")
       IF TYPE("PAGAMENTO")=="U"
          uf_atendimento_selcliente(ucrsinclatend.no, ucrsinclatend.estab, "ATENDIMENTO")
       ELSE
          uf_pagamento_pesqentfact(ucrsinclatend.no, ucrsinclatend.estab)
       ENDIF
    ENDIF
 ENDIF
 uf_criarclatend_sair()
ENDFUNC
**
PROCEDURE uf_criarclatend_actnrcartao
 LOCAL lcnrcartao
 STORE '' TO lcnrcartao
 lcnrcartao = uf_cartaocliente_geranumero(.T., uCrsInCLAtend.estab)
 SELECT ucrsinclatend
 GOTO TOP
 REPLACE ucrsinclatend.nrcartao WITH lcnrcartao
 criarclatend.refresh
ENDPROC
**
PROCEDURE uf_criarclatend_sair
 criarclatend.hide
 criarclatend.release
 RELEASE criarclatend
ENDPROC
**
PROCEDURE uf_criarclatend_dependente
 LPARAMETERS lcbool
 IF lcbool
    criarclatend.menu1.dependente.config("Dependente", mypath+"\imagens\icons\checked_w.png", "uf_criarclatend_dependente with .f.", "D")
    uf_pesqutentes_chama("CRIARCLATEND")
 ELSE
    SELECT ucrsinclatend
    DELETE ALL
    APPEND BLANK
    criarclatend.txtdatanasc.value = uf_gerais_getdate('01.01.1900')
    criarclatend.txtvalidade.value = uf_gerais_getdate('01.01.1900')
    uf_utentes_valoresdefeito("uCrsInCLAtend", "CRIARCLATEND")
    criarclatend.txtnome.setfocus
    criarclatend.menu1.dependente.config("Dependente", mypath+"\imagens\icons\unchecked_w.png", "uf_criarclatend_dependente with .t.", "D")
 ENDIF
ENDPROC
**
PROCEDURE uf_criarclatend_pesqsede
 LPARAMETERS lcno
 TEXT TO lcsql TEXTMERGE NOSHOW
		Select TOP 1 *
		From b_utentes (nolock)
		where no = <<lcNo>> and estab = 0
 ENDTEXT
 uf_gerais_actgrelha("", "uCrsTempCL", lcsql)
 SELECT ucrsinclatend
 DELETE ALL
 SELECT ucrstempcl
 GOTO TOP
 SELECT * FROM uCrsTempCL INTO CURSOR uCrsInCLAtend READWRITE
 IF USED("uCrsTempCL")
    fecha("uCrsTempCL")
 ENDIF
 TEXT TO lcsql TEXTMERGE NOSHOW
		Select case when Max(estab) is null then 1 else max(estab)+1 end as estab
		From b_utentes (nolock)
		where no=<<lcNo>>
 ENDTEXT
 uf_gerais_actgrelha("", "uCrsTempEstab", lcsql)
 LOCAL lcstamp
 lcstamp = uf_gerais_stamp()
 SELECT ucrsinclatend
 REPLACE ucrsinclatend.estab WITH ucrstempestab.estab
 REPLACE ucrsinclatend.utstamp WITH lcstamp
 REPLACE ucrsinclatend.nrcartao WITH ''
 IF USED("uCrsTempEstab")
    fecha("uCrsTempEstab")
 ENDIF
 criarclatend.refresh()
ENDPROC
**
PROCEDURE uf_criarclatend_editarcl
 LPARAMETERS lcno, lcestab
 criarclatend.caption = "EDITAR FICHA DE CLIENTE"
 criarclatend.editar = 1
 criarclatend.menu1.estado("dependente,actCartao", "HIDE")
 criarclatend.shape6.visible = .F.
 criarclatend.label4.visible = .F.
 criarclatend.label23.visible = .F.
 criarclatend.cmbpat1.visible = .F.
 criarclatend.cmbpat2.visible = .F.
 TEXT TO lcsql TEXTMERGE NOSHOW
		Select TOP 1 *
		From b_utentes (nolock)
		where no = <<lcNo>> and estab = <<lcEstab>>
 ENDTEXT
 uf_gerais_actgrelha("", "uCrsTempCL", lcsql)
 SELECT ucrsinclatend
 DELETE ALL
 SELECT ucrstempcl
 GOTO TOP
 SELECT * FROM uCrsTempCL INTO CURSOR uCrsInCLAtend READWRITE
 criarclatend.txtdatanasc.value = uf_gerais_getdate(ucrstempcl.nascimento)
 criarclatend.txtvalidade.value = uf_gerais_getdate(ucrstempcl.valipla)
 IF USED("uCrsTempCL")
    fecha("uCrsTempCL")
 ENDIF
 criarclatend.refresh()
ENDPROC
**
