**
FUNCTION uf_valespendentes_chama
 LPARAMETERS lcno, lcestab
 lcsql = ""
 TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_cartao_aplicarValesPendentes <<lcNo>>,<<lcEstab>>
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsTemValPen", lcsql)
    uf_perguntalt_chama("OCORREU UM ERRO A PROCURAR OS VALES DO CLIENTE.", "OK", "", 16)
    RETURN .F.
 ENDIF
 SELECT u_refvale FROM fi WHERE  NOT EMPTY(u_refvale) INTO CURSOR ucrsFIRefsValesTemp READWRITE
 SELECT ucrsfirefsvalestemp
 GOTO TOP
 SCAN
    SELECT ucrstemvalpen
    GOTO TOP
    SCAN FOR UPPER(ALLTRIM(ucrstemvalpen.ref))==UPPER(ALLTRIM(ucrsfirefsvalestemp.u_refvale))
       SELECT ucrstemvalpen
       DELETE
    ENDSCAN
 ENDSCAN
 IF USED("ucrsFIRefsValesTemp")
    fecha("ucrsFIRefsValesTemp")
 ENDIF
 LOCAL lcidadeval
 lcidadeval = uf_gerais_getparameter("ADM0000000017", "NUM")
 IF uf_gerais_getparameter_site("ADM0000000017", "BOOL")
    SELECT ucrstemvalpen
    GOTO TOP
    SCAN
       IF ucrstemvalpen.idade_meses>lcidadeval
          SELECT ucrstemvalpen
          DELETE
       ENDIF
       SELECT ucrstemvalpen
    ENDSCAN
 ENDIF
 SELECT ucrstemvalpen
 GOTO TOP
 IF TYPE("valespendentes")=="U"
    DO FORM valespendentes
 ELSE
    valespendentes.show()
 ENDIF
ENDFUNC
**
PROCEDURE uf_valespendentes_aplicar
 SELECT ucrstemvalpen
 atendimento.txtscanner.value = ALLTRIM(ucrstemvalpen.ref)
 uf_valespendentes_sair()
 atendimento.txtscanner.lostfocus
ENDPROC
**
PROCEDURE uf_valespendentes_sair
 IF USED("uCrsTemValPen")
    fecha("uCrsTemValPen")
 ENDIF
 valespendentes.hide
 valespendentes.release
ENDPROC
**
