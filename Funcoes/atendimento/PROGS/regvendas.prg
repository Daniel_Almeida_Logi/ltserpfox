	**
FUNCTION uf_regvendas_chama
 LPARAMETERS lcregcreditos, lcnatend, lcnvenda
 regua(0, 8, "A carregar painel...")
 regua(1, 1, "A carregar painel...")


 IF TYPE("REGVENDAS") = "U"
   IF USED("uc_dadosPsico")
      FECHA("uc_dadosPsico")
   ENDIF
 ENDIF

 IF TYPE("PESQUTENTES")<>"U"
    uf_pesqutentes_sair()
 ENDIF
 PUBLIC sores, soenc, pesqmmnif, talaomaqdinheiro, verificaregreceita
 sores = 0
 soenc = 0
 pesqmmnif = 0
 talaomaqdinheiro = .F.
 verificaregreceita = .t.
 lcsql = ''
 
 IF USED("ucrsfiltraprodreceita")
 	SELECT ucrsfiltraprodreceita
 	DELETE ALL 
 ELSE 
 	CREATE CURSOR ucrsfiltraprodreceita(ftstamp C(25), fistamp C(25), u_receita C(25), fno n(10,0), no n(10,0), estab n(10,0))
 ENDIF 


 IF ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))='�'
    TEXT TO lcsql TEXTMERGE NOSHOW
				select * from B_modoPag (nolock)
    ENDTEXT
 ELSE
    TEXT TO lcsql TEXTMERGE NOSHOW
				select * from B_modoPag_moeda(nolock)
    ENDTEXT
 ENDIF
 IF  .NOT. uf_gerais_actgrelha("", "UcrsConfigModPag", lcsql)
    uf_perguntalt_chama("N�o foi poss�vel verificar as configura��es de Modo de Pagamento. Contacte o suporte.", "", "OK", 32)
    RETURN .F.
 ENDIF
 IF TYPE("REGVENDAS")=="U"
    LOCAL lcclienteno, lcclienteestab, lcvalidatot, lcnatend, lcvenda
    STORE .F. TO lcvalidatot
    STORE -1 TO lcclienteno, lcclienteestab
    IF USED("uCrsAtendCl")
       SELECT ucrsatendcl
       Go top 
       IF ucrsatendcl.no<>200
          lcclienteno = ucrsatendcl.no
          lcclienteestab = ucrsatendcl.estab
       ENDIF
    ELSE
       IF USED("CL")
       		Select CL 
       		Browse
          	lcclienteno = cl.no
          	lcclienteestab = cl.estab
       ENDIF
    ENDIF
                
    **remove o estab, se for sede
    IF lcclienteestab == 0
    	lcclienteestab = -1
    endif
      
    
    regua(1, 2, "A carregar painel...")
    IF  .NOT. EMPTY(lcnatend)
       lcnatend = astr(lcnatend)
    ELSE
       lcnatend = ''
    ENDIF
    IF  .NOT. EMPTY(lcnvenda)
       lcnvenda = ALLTRIM(STRTRAN(lcnvenda, ' ', ''))
    ELSE
       lcnvenda = -1
    ENDIF

    LOCAL lcdiasproc
    lcdiasproc = uf_gerais_getparameter('ADM0000000300', 'num')
    IF YEAR(DATE()-lcdiasproc)>=1900
       dtini = uf_gerais_getdate(DATETIME()+((difhoraria-(24*lcdiasproc))*3600))
    ELSE
       dtini = uf_gerais_getdate('19000101')
    ENDIF
    IF  .NOT. lcregcreditos
       TEXT TO lcsql TEXTMERGE NOSHOW
					exec up_touch_regularizarVendas '', '<<lcNVenda>>' , '', <<lcClienteNo>>, <<lcClienteEstab>>, '<<uf_gerais_getdate(dtini , "SQL")>>', '<<uf_gerais_getdate(date(), "SQL")>>', '<<lcNAtend>>' , <<ch_userno>>, '<<Alltrim(ch_grupo)>>', '<<mySite>>', 1000, <<pesqmmnif>>
       ENDTEXT
    ELSE
       TEXT TO lcsql TEXTMERGE NOSHOW
					exec up_touch_regularizarVendas '', '<<lcNVenda>>', '', <<lcClienteNo>>, <<lcClienteEstab>>, '<<uf_gerais_getdate(dtini , "SQL")>>', '<<uf_gerais_getdate(date(), "SQL")>>', '<<lcNAtend>>' , <<ch_userno>>, '<<Alltrim(ch_grupo)>>', '<<mySite>>', 1000, <<pesqmmnif>>
       ENDTEXT
    ENDIF

	
    regua(1, 3, "A carregar painel...")
    IF  .NOT. uf_gerais_actgrelha("", "uCrsRegVendas", lcsql)
       uf_perguntalt_chama("Ocorreu uma anomalia a verificar as vendas. Por favor contacte o Suporte. Obrigado.", "OK", "", 16)
       RETURN .F.
    ENDIF


    SELECT ucrsregvendas
    GOTO TOP
    SCAN
       IF ucrsregvendas.qtt<>ucrsregvendas.qtt2
          REPLACE ucrsregvendas.etiliquido WITH ucrsregvendas.qtt*ucrsregvendas.etiliquido2/ucrsregvendas.qtt2
       ENDIF
    ENDSCAN
    SELECT ucrsregvendas
    GOTO TOP
    regua(1, 4, "A carregar painel...") 
    
    IF USED("uCrsVale")

       IF  .NOT. USED("uCrsVale2")
       		
          SELECT * FROM uCrsVale INTO CURSOR uCrsVale2 READWRITE
          SELECT ucrsvale2
          GOTO TOP
          SCAN FOR ucrsvale2.copied=.T.
             REPLACE ucrsvale2.sel WITH .F.
          ENDSCAN
          SELECT ucrsvale2
          GOTO TOP
          LOCAL lctotal, lcqtttotal
          STORE 0 TO lctotal, lcqtttotal
          CALCULATE SUM(ucrsvale2.etiliquido), SUM(ucrsvale2.qtt) TO lctotal, lcqtttotal 
          lcvalidatot = .T.
          SELECT ucrsvale2
          GOTO TOP  
       ENDIF
    ELSE
       IF  .NOT. USED("uCrsVale2")
          SELECT * FROM uCrsRegVendas WHERE 1=0 INTO CURSOR uCrsVale2 READWRITE
       ENDIF
    ENDIF
    regua(1, 5, "A carregar painel...")
    TEXT TO lcsql TEXTMERGE NOSHOW
			SET fmtonly on
			exec up_touch_regularizarCreditos '', -1, '', -1, -1, '<<uf_gerais_getdate(dtini , "SQL")>>', '<<uf_gerais_getdate(date(), "SQL")>>', '', <<ch_userno>>, '<<Alltrim(ch_grupo)>>', '<<mysite>>',0,0,0
			SET fmtonly off
    ENDTEXT

    IF  .NOT. uf_gerais_actgrelha("", "uCrsRegCreditos", lcsql)
       RETURN .F.
    ENDIF
    TEXT TO lcsql TEXTMERGE NOSHOW
			SET fmtonly on
			exec up_touch_regularizarCreditosPorValor -1, '', -1, -1, '<<uf_gerais_getdate(dtini , "SQL")>>', '<<uf_gerais_getdate(date(), "SQL")>>', '', <<ch_userno>>, '<<Alltrim(ch_grupo)>>', '<<mysite>>', 0
			SET fmtonly off
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "uCrsRegCreditosValor", lcsql)
       RETURN .F.
    ENDIF
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			SET fmtonly on
			exec up_touch_pesqRecibos -1, '', -1, -1, '', '', '', '', ''
			SET fmtonly off
    ENDTEXT
    uf_gerais_actgrelha("", "uCrsRecibos", lcsql)



    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			SET fmtonly on
			select 
				re.nmdoc
				,re.rno
				,re.nome
				,no
				,estab
				,convert(varchar,rdata,102) rdata
				,moeda
				,fin
				,efinv
				,virs
				,etotal
				,olcodigo
				,ollocal
				,desc1
				,re.site
				,pnome
				,pno
				,vendnm
				,vendedor
				,evdinheiro
				,epaga2
				,epaga1
				,echtotal 
				,restamp
				,ISNULL(tsre.u_tipoDoc,0) as u_tipodoc
			from 
				re (nolock) 
			left join tsre (nolock) on  re.ndoc = 	tsre.ndoc
			SET fmtonly off
    ENDTEXT
    uf_gerais_actgrelha("", "uCrsReCons", lcsql)
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			SET fmtonly on
			select 
				rno
				,cdesc
				,eval
				,erec
				,desconto 
				,nrdoc
			from 
				rl (nolock) 
			SET fmtonly off
    ENDTEXT
    uf_gerais_actgrelha("", "uCrsRlCons", lcsql)
    regua(1, 6, "A carregar painel...")
    uf_pagcentral_carregacursores()
    LOCAL lcfno, lcfno2, lcdocinvert
    STORE 0 TO lcfno, lcfno2
    SELECT ucrsregvendas
    GOTO TOP
    lcfno = ucrsregvendas.fno
    SCAN
       lcfno2 = ucrsregvendas.fno
       IF lcfno<>lcfno2
          IF lcdocinvert
             lcdocinvert = .F.
          ELSE
             lcdocinvert = .T.
          ENDIF
       ENDIF
       REPLACE ucrsregvendas.docinvert WITH lcdocinvert
       lcfno = ucrsregvendas.fno
    ENDSCAN
    SELECT ucrsregvendas
    GOTO TOP
    regua(1, 7, "A carregar painel...")
    DO FORM REGVENDAS

    IF lcvalidatot
       regvendas.pgfreg.page1.txttotalreg.value = ROUND(lctotal, 2)
       regvendas.pgfreg.page1.txttotalqttreg.value = ROUND(lcqtttotal, 2)
    ENDIF
    IF TYPE("REGVENDAS.menu1.regCreditos")=="U"
       regvendas.menu1.adicionaopcao("opcoes", "Op��es", mypath+"\imagens\icons\opcoes_w.png", "", "F1")
       regvendas.menu1.adicionaopcao("actualizar", "Actualizar", mypath+"\imagens\icons\actualizar_w.png", "uf_regvendas_pesquisarRegVendas", "F2")
       regvendas.menu1.adicionaopcao("regCreditos", "Reg. Cr�ditos", mypath+"\imagens\icons\doc_euro_w.png", "uf_regvendas_VerRegCreditos", "F7")
       regvendas.menu1.adicionaopcao("pagCentral", "Pag. Central", mypath+"\imagens\icons\pag_central.png", "uf_regvendas_VerPagCentral", "F8")
       regvendas.menu1.adicionaopcao("fechar", "Anular Res.", mypath+"\imagens\icons\fecharDia.png", "uf_regvendas_fecha", "F9")
       regvendas.menu1.adicionaopcao("multiemp", "Outras Lojas", mypath+"\imagens\icons\unchecked_w.png", "uf_regvendas_outraslojas", "F10")
       regvendas.menu1.adicionaopcao("adicionar", "Adicionar", mypath+"\imagens\icons\mais_w.png", "uf_pagCentral_adicionaPagPendente", "F2")
       regvendas.menu1.adicionaopcao("pagPendentes", "Pag. Penden.", mypath+"\imagens\icons\euro_interrugacao_w.png", "uf_pagCentral_pagPendentes", "F3")
       regvendas.menu1.adicionaopcao("abaterPag", "Abater", mypath+"\imagens\icons\cruz_w.png", "uf_pagCentral_abaterAtend", "F4")
       regvendas.menu1.adicionaopcao("gaveta", "Gaveta", mypath+"\imagens\icons\gaveta_w.png", "uf_PAGAMENTO_abreGaveta", "F5")
       regvendas.menu1.adicionaopcao("limparPag", "Limpar Pag.", mypath+"\imagens\icons\limpar_w.png", "uf_pagCentral_limparValores", "F6")
       regvendas.menu1.adicionaopcao("selTodos", "Sel. Todos", mypath+"\imagens\icons\unchecked_w.png", "uf_regvendas_seltodosRvReg with .t.", "F4")
       regvendas.menu1.adicionaopcao("nTaloes", "N� Tal�es", mypath+"\imagens\icons\1_w.png", "uf_regvendas_nrTaloesImpRvCreditos", "F5")
       regvendas.menu1.adicionaopcao("voltar", "Voltar", mypath+"\imagens\icons\voltar_w.png", "uf_regvendas_VerRegVendas", "F10")
    ENDIF
    regvendas.menu_opcoes.adicionaopcao("meiosPag", "Meios Pag.", "", "uf_regvendas_meiosPagamento")
    regvendas.menu_opcoes.adicionaopcao("meiosPagRec", "Meios Pag.", "", "uf_regvendas_meiosPagamento_Recibos")
    regvendas.menu_opcoes.adicionaopcao("imprimir", "Imprimir", "", "uf_regVendas_imprimirCreditos")
    regvendas.menu_opcoes.adicionaopcao("editSerie", "Editar S�ries", "", "uf_editSeries_chama WITH 'TSRE'")
    &&regvendas.menu_opcoes.adicionaopcao("imprimirt", "Tal�o", "", "uf_imprimir_nreg")
    regvendas.menu_opcoes.adicionaopcao("impReg", "Imprimir", "", "uf_imprimir_regularizacoes")
    regvendas.menu1.gravar.img1.picture = mypath+"\imagens\icons\guardar_w.png"
    regvendas.menu1.estado("opcoes, actualizar", "SHOW")
    regvendas.menu_opcoes.estado("meiosPagRec", "HIDE")
    IF uf_gerais_getparameter_site('ADM0000000097', 'BOOL', mysite)
       SELECT ucrse1
       LOCAL lcmynif
       lcmynif = ALLTRIM(ucrse1.ncont)
       TEXT TO lcsql TEXTMERGE NOSHOW
				select count(no) as nremp from empresa (NOLOCK) where ncont = '<<ALLTRIM(lcMyNif)>>'
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "ucrsempmmnif", lcsql)
          uf_perguntalt_chama("OCORREU UM PROBLEMA A VERIFICAR AS EMPRESAS COM O MESMO NIF.", "OK", "", 16)
          RETURN .F.
       ENDIF
       IF ucrsempmmnif.nremp>1
          regvendas.menu1.estado("multiemp", "SHOW")
       ELSE
          regvendas.menu1.estado("multiemp", "HIDE")
       ENDIF
    ELSE
       regvendas.menu1.estado("multiemp", "HIDE")
    ENDIF
    regvendas.menu1.estado("adicionar, pagPendentes, abaterPag, gaveta, limparPag, selTodos, nTaloes, voltar, fechar,regCreditos, pagCentral", "HIDE")
    regvendas.menu_opcoes.estado("imprimir ", "HIDE")
    regvendas.menu1.estado("", "", "Regularizar", .T.)
    IF  .NOT. uf_gerais_getparameter("ADM0000000138", "BOOL")
       regvendas.menu1.estado("regCreditos", "HIDE")
    ENDIF
    IF  .NOT. uf_gerais_getparameter("ADM0000000079", "BOOL")
       regvendas.menu1.estado("pagCentral", "HIDE")
    ENDIF
    regua(1, 8, "A carregar painel...")
    uf_regvendas_configgrdregvendas()
    LOCAL lcdiasproc
    lcdiasproc = uf_gerais_getparameter('ADM0000000300', 'num')
    IF YEAR(DATE()-lcdiasproc)>=1900
       regvendas.txtdataini.value = uf_gerais_getdate(DATETIME()+((difhoraria-(24*lcdiasproc))*3600))
    ELSE
       regvendas.txtdataini.value = uf_gerais_getdate('19000101')
    ENDIF
    regvendas.txtdatafim.value = uf_gerais_getdate(DATETIME()+(difhoraria*3600))
    regvendas.ctnpagamento.txtvalatend.value = 0
    regvendas.ctnpagamento.txttroco.value = 0

    IF lcclienteno<>-1
       regvendas.txtno.value = lcclienteno
       regvendas.txtestab.value = astr(lcclienteestab)
    ENDIF    
 
    regvendas.pgfreg.page1.grdvendas.setfocus
    regvendas.txtnatend.setfocus
    regvendas.ctnpagamento.tecladovirtual1.setestado(3)
    regvendas.ctnpagamento.tecladovirtual1.setsize(85, 45)
    regvendas.ctnpagamento.tecladovirtual1.funcaoteclas = "uf_regvendas_botaoPressRvCred"
    regvendas.ctnpagamento.tecladovirtual1.allowdot(.T.)
    regvendas.ctnpagamento.tecladovirtual1.allownegativevalue(.F.)
    regvendas.ctnpagamento.tecladovirtual1.nrcasasdecimais = 2
 ELSE
    regua(1, 8, "A carregar painel...")
    regvendas.show
 ENDIF



 uf_regvendas_marcarSel()
 
 regvendas.menu1.estado("fechar", "HIDE")


 regua(2)
ENDFUNC
**




PROCEDURE uf_regvendas_meiospagamento
 SELECT ucrsregvendas
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		select u_nratend, ftano
		from ft (nolock) 
		where 
			ftstamp = '<<uCrsRegVendas.ftstamp>>'
 ENDTEXT
 uf_gerais_actgrelha("", "ucrsNrAtendRegVenda", lcsql)
 SELECT ucrsnratendregvenda
 IF RECCOUNT("ucrsNrAtendRegVenda")>0
    SELECT ucrsnratendregvenda
    uf_alterameiospag_chama(ucrsnratendregvenda.u_nratend, ucrsnratendregvenda.ftano)
 ENDIF
ENDPROC
**
PROCEDURE uf_regvendas_meiospagamento_Recibos
   SELECT uCrsReCons

   lcsql = ''
   TEXT TO lcsql TEXTMERGE NOSHOW
      select 
         reano,
         u_nratend
      from 
         re(nolock)
      where
         restamp = '<<uCrsReCons.restamp>>'
   ENDTEXT
   uf_gerais_actgrelha("", "ucrsNrAtendRegVenda", lcsql)
   SELECT ucrsnratendregvenda
   IF RECCOUNT("ucrsNrAtendRegVenda")>0
      SELECT ucrsnratendregvenda
      uf_alterameiospag_chama(ucrsnratendregvenda.u_nratend, ucrsnratendregvenda.reano, '')
   ENDIF
ENDPROC

**
PROCEDURE uf_regvendas_nrtaloesimprvcreditos
 DO CASE
    CASE RIGHT(UPPER(ALLTRIM(regvendas.menu1.ntaloes.img.picture)), 7)==UPPER(ALLTRIM("1_w.png"))
       regvendas.menu1.ntaloes.img.picture = mypath+"\imagens\icons\2_w.png"
    CASE RIGHT(UPPER(ALLTRIM(regvendas.menu1.ntaloes.img.picture)), 7)=UPPER(ALLTRIM("2_w.png"))
       regvendas.menu1.ntaloes.img.picture = mypath+"\imagens\icons\3_w.png"
    CASE RIGHT(UPPER(ALLTRIM(regvendas.menu1.ntaloes.img.picture)), 7)=UPPER(ALLTRIM("3_w.png"))
       regvendas.menu1.ntaloes.img.picture = mypath+"\imagens\icons\0_w.png"
    CASE RIGHT(UPPER(ALLTRIM(regvendas.menu1.ntaloes.img.picture)), 7)=UPPER(ALLTRIM("0_w.png"))
       regvendas.menu1.ntaloes.img.picture = mypath+"\imagens\icons\1_w.png"
    OTHERWISE
       regvendas.menu1.ntaloes.img.picture = mypath+"\imagens\icons\1_w.png"
 ENDCASE
ENDPROC
**
PROCEDURE uf_regvendas_verregcreditos
 regvendas.pgfreg.activepage = 2
 regvendas.menu1.estado(" pagCentral, multiemp, regCreditos", "HIDE")
 regvendas.menu1.estado("selTodos, nTaloes, voltar", "SHOW")
 regvendas.menu_opcoes.estado("imprimir", "SHOW")
 regvendas.menu_opcoes.estado("meiosPag,meiosPagRec, impReg", "HIDE")
 regvendas.menu1.estado("", "", "Emitir Rec.", .T.)
 regvendas.menu1.actualizar.config("Actualizar", mypath+"\imagens\icons\actualizar_w.png", "uf_regvendas_pesquisarRegCreditos", "F2")
 regvendas.caption = 'REGULARIZAR CR�DITOS'
 regvendas.ctnpagamento.visible = .T.
 regvendas.txtbtnsuspensas.visible = .F.
 regvendas.txtbtncreditos.visible = .F.
 regvendas.txtbtnreservas.visible = .F.
 regvendas.txtbtnencomendas.visible = .F.
 regvendas.btnsuspensas.visible = .F.
 regvendas.btncreditos.visible = .F.
 regvendas.btnreservas.visible = .F.
 regvendas.btnencomendas.visible = .F.
 IF  .NOT. EMPTY(regvendas.txtno.value)
    regvendas.menu1.actualizar.click()
 ELSE
    SELECT ucrsregcreditos
    IF RECCOUNT("uCrsRegCreditos")>0
       REPLACE sel WITH .F. ALL
    ENDIF
    SELECT ucrsregcreditosvalor
    IF RECCOUNT("uCrsRegCreditosValor")>0
       REPLACE sel WITH .F. ALL
    ENDIF
 ENDIF
 uf_regvendas_configpesq()
ENDPROC
**
PROCEDURE uf_regvendas_verregvendas
 
 regvendas.pgfreg.activepage = 1
 regvendas.menu1.estado("actualizar", "SHOW")
 IF  .NOT. uf_gerais_getparameter("ADM0000000138", "BOOL")
    regvendas.menu1.estado("regCreditos", "HIDE")
 ELSE
 ENDIF
 IF  .NOT. uf_gerais_getparameter("ADM0000000079", "BOOL")
    regvendas.menu1.estado("pagCentral", "HIDE")
 ELSE
 ENDIF
 regvendas.menu_opcoes.estado("imprimir,meiosPagRec", "HIDE")
 regvendas.menu_opcoes.estado("meiosPag, impReg", "SHOW")
 regvendas.menu1.estado("selTodos, nTaloes, voltar, gaveta, limparPag, pagPendentes, abaterPag,adicionar", "HIDE")
 regvendas.menu1.estado("", "", "Regularizar", .T.)
 regvendas.menu1.actualizar.config("Actualizar", mypath+"\imagens\icons\actualizar_w.png", "uf_regvendas_pesquisarRegVendas", "F2")
 regvendas.ctnpagamento.visible = .F.
 regvendas.caption = 'REGULARIZAR VENDAS'
 uf_regvendas_configpesq()
 uf_regvendas_resetvalorespagamento()
ENDPROC
**
FUNCTION uf_regvendas_verpagcentral
 IF uf_gerais_validapermuser(ch_userno, ch_grupo, 'Pagamentos Centralizados')
    IF (uf_gerais_addconnection('PAGCENTRAL')==.F.)
       uf_perguntalt_chama("N�O EXISTEM LICEN�AS DISPON�VEIS PARA ESTE M�DULO.", "OK", "", 48)
       RETURN .F.
    ENDIF
 ELSE
    uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL PAGAMENTOS CENTRALIZADOS.", "OK", "", 48)
    RETURN .F.
 ENDIF
 regvendas.pgfreg.activepage = 3
 regvendas.pgfreg.page3.pgfpagcentral.activepage = 1
 regvendas.menu1.estado("actualizar, pagCentral, multiemp, regCreditos", "HIDE")
 regvendas.menu1.estado("adicionar,limparPag,pagPendentes,gaveta, abaterPag, voltar", "SHOW")
 regvendas.menu_opcoes.estado("imprimir,meiosPag,meiosPagRec, impreg", "HIDE")
 regvendas.menu1.actualizar.config("Adicionar", mypath+"\imagens\icons\mais_w.png", "uf_pagCentral_adicionaPagPendente", "F2")
 regvendas.menu1.actualizar.config("Pag. Penden.", mypath+"\imagens\icons\euro_interrugacao_w.png", "uf_pagCentral_pagPendentes", "F3")
 regvendas.menu1.actualizar.config("Abater", mypath+"\imagens\icons\cruz_w.png", "uf_pagCentral_abaterAtend", "F4")
 
 IF uf_gerais_getUmValor("B_terminal", "gaveta", "terminal = '" + ALLTRIM(myTerm) + "'")
 	regvendas.menu1.actualizar.config("Gaveta", mypath+"\imagens\icons\gaveta_w.png", "uf_PAGAMENTO_abreGaveta", "F5")
 ENDIF	
 
 regvendas.menu1.actualizar.config("Limpar Pag.", mypath+"\imagens\icons\limpar_w.png", "uf_pagCentral_limparValores", "F6")
 regvendas.menu1.actualizar.config("Voltar", mypath+"\imagens\icons\voltar_w.png", "uf_regvendas_VerRegVendas", "F10")
 IF TYPE("ATENDIMENTO")=="U"
    regvendas.menu1.estado("voltar", "HIDE")
 ELSE
    regvendas.menu1.estado("voltar", "SHOW")
 ENDIF
 regvendas.menu1.estado("", "", "Registar", .T.)
 regvendas.ctnpagamento.visible = .T.
 regvendas.caption = 'PAGAMENTOS CENTRALIZADOS'
 uf_regvendas_configpesq()
 uf_pagcentral_totalporpagar()
 SELECT ucrspca
 GOTO TOP
 regvendas.pgfreg.page3.txtpendente.setfocus
 regvendas.pgfreg.page3.pgfpagcentral.page1.grdpagcentral.setfocus
ENDFUNC
**
PROCEDURE uf_regvendas_resetvalorespagamento
 regvendas.ctnpagamento.txtvalatend.value = 0
 regvendas.ctnpagamento.txtdinheiro.value = ''
 regvendas.ctnpagamento.txtmb.value = ''
 regvendas.ctnpagamento.txtvisa.value = ''
 regvendas.ctnpagamento.txtcheque.value = ''
 regvendas.ctnpagamento.txtmodpag3.value = ''
 regvendas.ctnpagamento.txtmodpag4.value = ''
 regvendas.ctnpagamento.txtmodpag5.value = ''
 regvendas.ctnpagamento.txtmodpag6.value = ''
 regvendas.ctnpagamento.txtvalrec.value = ''
 regvendas.ctnpagamento.txttroco.value = 0
ENDPROC
**
PROCEDURE uf_regvendas_configpesq
 IF regvendas.pgfreg.activepage==3
    regvendas.txtnvenda.visible = .F.
    regvendas.txtcliente.visible = .F.
    regvendas.txtno.visible = .F.
    regvendas.txtestab.visible = .F.
    regvendas.txtproduto.visible = .F.
    regvendas.txtdataini.visible = .F.
    regvendas.txtdatafim.visible = .F.
    regvendas.lblnvenda.visible = .F.
    regvendas.lblcliente.visible = .F.
    regvendas.lblproduto.visible = .F.
    regvendas.lbldata.visible = .F.
    regvendas.lbla.visible = .F.
    regvendas.txtnatend.width = 160
 ELSE
    regvendas.txtnvenda.visible = .T.
    regvendas.txtcliente.visible = .T.
    regvendas.txtno.visible = .T.
    regvendas.txtestab.visible = .T.
    regvendas.txtproduto.visible = .T.
    regvendas.txtdataini.visible = .T.
    regvendas.txtdatafim.visible = .T.
    regvendas.lblnvenda.visible = .T.
    regvendas.lblcliente.visible = .T.
    regvendas.lblproduto.visible = .T.
    regvendas.lbldata.visible = .T.
    regvendas.lbla.visible = .T.
    **regvendas.txtnatend.width = 160
 ENDIF
ENDPROC

** Valida se permite escolher multiplas regularizacoes
** uf_gerais_getparameter('ADM0000000004', 'bool') -> permite multivenda
FUNCTION uf_regvendas_validaPermiteMultiRecibos
	LPARAMETERS lcCursor
	
	LOCAL  lcPosTemp, lcResult, lcPosTemp1
	
	
	if(USED("ucrsregcreditos"))
		SELECT ucrsregcreditos
		lcPosTemp = RECNO("ucrsregcreditos")
	ENDIF
	
	
	if(USED("uCrsRegCreditosValor"))
		SELECT uCrsRegCreditosValor
		lcPosTemp1= RECNO("uCrsRegCreditosValor")
	ENDIF
	
	
	lcResult= .t.
	
	if(uf_gerais_getparameter('ADM0000000004', 'bool'))
		RETURN .t.
	ENDIF
	

	fecha("ucrsTemp")
	if(USED("ucrsregcreditos"))		
		SELECT COUNT(distinct ftstamp) as conta FROM ucrsregcreditos WHERE ucrsregcreditos.sel = .t. INTO CURSOR  ucrsTemp  READWRITE
		
		SELECT ucrsTemp 
		GO TOP
		if(ucrsTemp.conta>1)
			lcResult = .f.
		ENDIF
	ENDIF
	
		

	fecha("ucrsTemp")
	
	
	if(USED("uCrsRegCreditosValor"))
		SELECT COUNT(distinct ftstamp) as conta FROM uCrsRegCreditosValor WHERE uCrsRegCreditosValor.sel = .t.  INTO CURSOR  ucrsTemp  READWRITE
		
		SELECT ucrsTemp 
		GO TOP
		if(ucrsTemp.conta>1)
			lcResult = .f.
		ENDIF
		

		fecha("ucrsTemp")
		
	ENDIF	
	
	if(USED("ucrsregcreditos"))	
	    SELECT ucrsregcreditos
	    TRY
	       GOTO lcPosTemp 
	    CATCH
	    ENDTRY    
    ENDIF
    
	
	if(USED("uCrsRegCreditosValor"))
		SELECT uCrsRegCreditosValor
	    TRY
	       GOTO lcPosTemp1 
	    CATCH
	    ENDTRY
    ENDIF

	
	RETURN lcResult 
	
ENDFUNC


**
FUNCTION uf_regvendas_addlinhareg
 LOCAL lctotal, lcqtttotal, lcvalidainsert, lcvalidainsert2, lcpos, lcnrdoc, lcndoc, lcvalidadev, lcstamp
 STORE 0 TO lctotal, lcqtttotal, lcpos, lcnrdoc, lcndoc
 STORE .T. TO lcvalidainsert, lcvalidainsert2
 STORE .F. TO lcvalidadev
 STORE "" TO lcstamp
 SELECT ucrsregvendas
 

 
 IF ucrsregvendas.sel=.T.
 	IF ALLTRIM(ucrsregvendas.nmdoc)='Encomenda de Cliente'
	 	LOCAL lcstampftstampsel
	    SELECT ucrsregvendas
	    lcstampftstampsel = ALLTRIM(ucrsregvendas.ftstamp)
	    SELECT ucrsregvendas
	    LOCAL lcposlnrec
	    lcposlnrec = RECNO("uCrsRegVendas")
	    SELECT ucrsregvendas
	    GOTO TOP
	    SCAN
	       IF ALLTRIM(ucrsregvendas.ftstamp)=lcstampftstampsel .AND. ucrsregvendas.sel=.F.
	          REPLACE ucrsregvendas.sel WITH .T.
	       ENDIF
	    ENDSCAN
	    SELECT ucrsregvendas
	    GOTO TOP
	    SELECT ucrsregvendas
	    TRY
	       GOTO lcposlnrec
	    CATCH
	    ENDTRY
 	ENDIF
 	
 	SELECT ucrsregvendas
	LOCAL lcposlnrec3
	lcposlnrec3 = RECNO("uCrsRegVendas")

	    
 	** verificar se est� a selecionar linha filtrada com artigo e com receita (para depois selecionar os outros artigos) 
 	IF verificaregreceita = .t.
 		SELECT ucrsfiltraprodreceita
 		GO TOP 
	 	IF LEN(ALLTRIM((ucrsfiltraprodreceita.u_receita)))<>0
 			uf_perguntalt_chama("N�O PODE SELECIONAR 2 LINHAS QUANDO FILTRA POR ARTIGO E J� ESCOLHEU UMA COM RECEITA ELETR�NICA", "OK", "", 64)
 			SELECT ucrsregvendas
 			REPLACE ucrsregvendas.sel WITH .F.
          	RETURN .F.
 		ENDIF 
 		SELECT ucrsregvendas
	 	IF  .NOT. EMPTY(ALLTRIM(regvendas.txtproduto.value)) AND LEN(ALLTRIM((ucrsregvendas.u_receita)))<>0
*!*		 		IF !empty(ucrsfiltraprodreceita.u_receita)
*!*		 			uf_perguntalt_chama("N�O PODE SELECIONAR 2 LINHAS COM RECEITA QUANDO FILTRA POR ARTIGO", "OK", "", 64)
*!*		 			REPLACE ucrsregvendas.sel WITH .F.
*!*		          	RETURN .F.
*!*		 		ENDIF 
	 		LOCAL lcContsel
	 		SELECT ucrsregvendas
	 		GO TOP 
	 		calculate count() for ucrsregvendas.sel to lcContsel
	 		IF lcContsel > 1
	 			uf_perguntalt_chama("N�O PODE SELECIONAR 1 LINHA COM RECEITA EM CONJUNTO COM OUTRAS QUANDO FILTRA POR ARTIGO", "OK", "", 64)
				REPLACE ucrsregvendas.sel WITH .F.
	    	   	RETURN .F.
			ENDIF 
	 	ENDIF 
	 	
	 	SELECT ucrsregvendas
		GOTO TOP
		SELECT ucrsregvendas
		TRY
		   GOTO lcposlnrec3
		CATCH
		ENDTRY
	 	
	 	IF  .NOT. EMPTY(ALLTRIM(regvendas.txtproduto.value)) AND !EMPTY(ucrsregvendas.u_ltstamp)
	 		SELECT ucrsfiltraprodreceita
	 		APPEND BLANK 
	 		replace ucrsfiltraprodreceita.ftstamp WITH ucrsregvendas.ftstamp 
	 		replace ucrsfiltraprodreceita.fistamp WITH ucrsregvendas.fistamp 
	 		replace ucrsfiltraprodreceita.u_receita  WITH ucrsregvendas.u_receita  
	 		replace ucrsfiltraprodreceita.fno  WITH ucrsregvendas.fno  
	 		replace ucrsfiltraprodreceita.no  WITH ucrsregvendas.no  
	 		replace ucrsfiltraprodreceita.estab  WITH ucrsregvendas.estab  
	 		
	 		select ucrsregvendas
	 	ENDIF 
	 ENDIF 
 	 
    SELECT ucrsvale2
    GOTO TOP
    SCAN
       IF ucrsvale2.fistamp=ucrsregvendas.fistamp
          lcvalidainsert = .F.
       ENDIF
    ENDSCAN
    IF lcvalidainsert
       SELECT ucrsregvendas
       IF ucrsregvendas.u_backoff
          uf_perguntalt_chama("ESTA VENDA J� FOI REGULARIZADA NO BACK-OFFICE POR VALOR."+CHR(10)+"N�O PODE REGULARIZAR UMA VENDA A CR�DITO POR PRODUTO SE J� A TIVER REGULARIZADO NO BACK-OFFICE POR VALOR.", "OK", "", 64)
          RETURN .F.
       ENDIF
       IF EMPTY(ucrsregvendas.ref) .AND.  .NOT. EMPTY(ucrsregvendas.fistamp)
          uf_perguntalt_chama("N�O PODE REGULARIZAR UM PRODUTO SEM REFER�NCIA NO DOCUMENTO DE VENDA. DEVE VALIDAR O ESTADO DESTE DOCUMENTO NO ECR� DE FACTURA��O EM BACK-OFFICE.", "OK", "", 64)
          RETURN .F.
       ENDIF
       IF ucrsregvendas.fmarcada=.T. .AND. ucrsregvendas.eaquisicao<>0
          IF ucrsregvendas.eaquisicao==ucrsregvendas.qtt
             lcvalidadev = .T.
          ELSE
             IF uf_perguntalt_chama("ATEN��O: PARTE DAS QUANTIDADES DESTE PRODUTO J� FORAM LIQUIDADAS."+CHR(13)+CHR(10)+"PRETENDE LIQUIDAR OU REGULARIZAR O PRODUTO?"+CHR(13)+CHR(13)+'[Sim] - Regularizar [N�o] - Liquidar', "Sim", "N�o")
                lcvalidadev = .T.
             ENDIF
          ENDIF
       ENDIF
       SELECT ucrsregvendas
       IF  .NOT. EMPTY(ucrsregvendas.u_ltstamp) .OR.  .NOT. EMPTY(ucrsregvendas.u_ltstamp2)
          SELECT ucrsregvendas
          lcstamp = ucrsregvendas.ftstamp
          lcpos = RECNO()
          GOTO TOP
          SCAN FOR UPPER(ALLTRIM(ucrsregvendas.ftstamp))==UPPER(ALLTRIM(lcstamp))
             IF  .NOT. (lcvalidadev==.F. .AND. (ucrsregvendas.qtt-ROUND(ucrsregvendas.eaquisicao, 0))<=0 .AND. ucrsregvendas.fmarcada==.T.)
                SELECT ucrsvale2
                GOTO TOP
                SCAN FOR ucrsvale2.fistamp=ucrsregvendas.fistamp
                   lcvalidainsert2 = .F.
                ENDSCAN
                IF lcvalidainsert2
                   SELECT ucrsvale2
                   APPEND BLANK
                   REPLACE ucrsvale2.docinvert WITH ucrsregvendas.docinvert
                   REPLACE ucrsvale2.ftstamp WITH ucrsregvendas.ftstamp
                   REPLACE ucrsvale2.fistamp WITH ucrsregvendas.fistamp
                   REPLACE ucrsvale2.fdata WITH ucrsregvendas.fdata
                   REPLACE ucrsvale2.ousrhora WITH ucrsregvendas.ousrhora
                   REPLACE ucrsvale2.nmdoc WITH ucrsregvendas.nmdoc
                   REPLACE ucrsvale2.fno WITH ucrsregvendas.fno
                   REPLACE ucrsvale2.nome WITH ucrsregvendas.nome
                   REPLACE ucrsvale2.no WITH ucrsregvendas.no
                   REPLACE ucrsvale2.estab WITH ucrsregvendas.estab
                   REPLACE ucrsvale2.ncont WITH ucrsregvendas.ncont
                   REPLACE ucrsvale2.design WITH ucrsregvendas.design
                   REPLACE ucrsvale2.ref WITH ucrsregvendas.ref
                   REPLACE ucrsvale2.eaquisicao WITH ucrsregvendas.eaquisicao
                   REPLACE ucrsvale2.oeaquisicao WITH ucrsregvendas.oeaquisicao
                   REPLACE ucrsvale2.ndoc WITH ucrsregvendas.ndoc
                   REPLACE ucrsvale2.sel WITH .T.
                   REPLACE ucrsvale2.u_ltstamp WITH ucrsregvendas.u_ltstamp
                   REPLACE ucrsvale2.u_ltstamp2 WITH ucrsregvendas.u_ltstamp2
                   REPLACE ucrsvale2.u_codigo WITH ucrsregvendas.u_codigo
                   REPLACE ucrsvale2.u_codigo2 WITH ucrsregvendas.u_codigo2
                   REPLACE ucrsvale2.u_diploma WITH ucrsregvendas.u_diploma
                   REPLACE ucrsvale2.qtt2 WITH ucrsregvendas.qtt
                   REPLACE ucrsvale2.epv WITH ucrsregvendas.epv
                   REPLACE ucrsvale2.etiliquido2 WITH ucrsregvendas.etiliquido
                   REPLACE ucrsvale2.desconto WITH ucrsregvendas.desconto
                   REPLACE ucrsvale2.desc2 WITH ucrsregvendas.desc2
                   REPLACE ucrsvale2.u_descval WITH ucrsregvendas.u_descval
                   REPLACE ucrsvale2.u_comp WITH ucrsregvendas.u_comp
                   REPLACE ucrsvale2.u_psicont WITH ucrsregvendas.u_psicont
                   REPLACE ucrsvale2.u_bencont WITH ucrsregvendas.u_bencont
                   REPLACE ucrsvale2.u_recdata WITH ucrsregvendas.u_recdata
                   REPLACE ucrsvale2.u_medico WITH ucrsregvendas.u_medico
                   REPLACE ucrsvale2.u_nmutdisp WITH ucrsregvendas.u_nmutdisp
                   REPLACE ucrsvale2.u_moutdisp WITH ucrsregvendas.u_moutdisp
                   REPLACE ucrsvale2.u_cputdisp WITH ucrsregvendas.u_cputdisp
                   REPLACE ucrsvale2.u_nmutavi WITH ucrsregvendas.u_nmutavi
                   REPLACE ucrsvale2.u_moutavi WITH ucrsregvendas.u_moutavi
                   REPLACE ucrsvale2.u_cputavi WITH ucrsregvendas.u_cputavi
                   REPLACE ucrsvale2.u_ndutavi WITH ucrsregvendas.u_ndutavi
                   REPLACE ucrsvale2.u_ddutavi WITH ucrsregvendas.u_ddutavi
                   REPLACE ucrsvale2.u_idutavi WITH ucrsregvendas.u_idutavi
                   REPLACE ucrsvale2.u_receita WITH ucrsregvendas.u_receita
					REPLACE ucrsvale2.lote WITH  IIF(empty(ucrsregvendas.lote), '', ucrsregvendas.lote) 
					REPLACE ucrsvale2.dataValidade WITH IIF(EMPTY(ucrsregvendas.dataValidade), '', ucrsregvendas.dataValidade)
                   IF EMPTY(ucrsregvendas.ficompart)
                      REPLACE ucrsvale2.u_nbenef WITH ucrsregvendas.u_nbenef
                      REPLACE ucrsvale2.u_nbenef2 WITH ucrsregvendas.u_nbenef2
                   ELSE
                      IF EMPTY(ucrsregvendas.u_nbenef) .AND.  .NOT. EMPTY(ucrsregvendas.u_nbenef2)
                         REPLACE ucrsvale2.u_nbenef WITH ucrsregvendas.u_nbenef2
                         REPLACE ucrsvale2.u_nbenef2 WITH ucrsregvendas.u_nbenef2
                      ENDIF
                      IF  .NOT. EMPTY(ucrsregvendas.u_nbenef) .AND. EMPTY(ucrsregvendas.u_nbenef2)
                         REPLACE ucrsvale2.u_nbenef WITH ucrsregvendas.u_nbenef
                         REPLACE ucrsvale2.u_nbenef2 WITH ucrsregvendas.u_nbenef
                      ENDIF
                   ENDIF
                   REPLACE ucrsvale2.u_entpresc WITH ucrsregvendas.u_entpresc
                   REPLACE ucrsvale2.u_nopresc WITH ucrsregvendas.u_nopresc
                   REPLACE ucrsvale2.u_txcomp WITH ucrsregvendas.u_txcomp
                   REPLACE ucrsvale2.u_epvp WITH ucrsregvendas.u_epvp
                   REPLACE ucrsvale2.u_ettent1 WITH ucrsregvendas.u_ettent1
                   REPLACE ucrsvale2.u_ettent2 WITH ucrsregvendas.u_ettent2
                   REPLACE ucrsvale2.amostra WITH ucrsregvendas.amostra
                   REPLACE ucrsvale2.u_abrev WITH ucrsregvendas.u_abrev
                   REPLACE ucrsvale2.u_abrev2 WITH ucrsregvendas.u_abrev2
                   REPLACE ucrsvale2.u_epref WITH ucrsregvendas.u_epref
                   REPLACE ucrsvale2.opcao WITH ucrsregvendas.opcao
                   REPLACE ucrsvale2.codacesso WITH ucrsregvendas.codacesso
                   REPLACE ucrsvale2.coddiropcao WITH ucrsregvendas.coddiropcao
                   REPLACE ucrsvale2.token WITH ucrsregvendas.token
                   REPLACE ucrsvale2.id_validacaodem WITH ucrsregvendas.id_validacaodem
                   REPLACE ucrsvale2.c2codpost WITH ucrsregvendas.c2codpost
					REPLACE ucrsvale2.lote WITH  IIF(empty(ucrsregvendas.lote), '', ucrsregvendas.lote) 
					REPLACE ucrsvale2.dataValidade WITH IIF(EMPTY(ucrsregvendas.dataValidade), '', ucrsregvendas.dataValidade)
                   IF ucrsregvendas.fmarcada
                      IF lcvalidadev=.T.
                         REPLACE ucrsvale2.qtt WITH ROUND(ucrsregvendas.eaquisicao, 0)
                         REPLACE ucrsvale2.etiliquido WITH ROUND((ROUND(ucrsregvendas.eaquisicao, 0)*ucrsregvendas.etiliquido/ucrsregvendas.qtt), 2)
                      ELSE
                         REPLACE ucrsvale2.qtt WITH ucrsregvendas.qtt-ROUND(ucrsregvendas.eaquisicao, 0)
                         REPLACE ucrsvale2.etiliquido WITH ROUND(((ucrsregvendas.qtt-ROUND(ucrsregvendas.eaquisicao, 0))*ucrsregvendas.etiliquido/ucrsregvendas.qtt), 2)
                      ENDIF
                   ELSE
                      REPLACE ucrsvale2.qtt WITH ucrsregvendas.qtt
                      REPLACE ucrsvale2.etiliquido WITH ucrsregvendas.etiliquido
                   ENDIF
                   IF lcvalidadev
                      REPLACE ucrsvale2.fmarcada WITH ucrsregvendas.fmarcada
                   ENDIF
                   IF ucrsregvendas.lancacc=.T.
                      REPLACE ucrsvale2.lancacc WITH .T.
                   ENDIF

                   uf_regVendas_getDadosUtente(ucrsregvendas.ftstamp, ucrsregvendas.fistamp)

                   SELECT ucrsregvendas
                   REPLACE sel WITH .T.
                ENDIF
                lcvalidainsert2 = .T.
             ENDIF
             SELECT ucrsregvendas
          ENDSCAN
          SELECT ucrsregvendas
          TRY
             GOTO lcpos
          CATCH
          ENDTRY
       ELSE
          SELECT ucrsvale2
          APPEND BLANK
          REPLACE ucrsvale2.docinvert WITH ucrsregvendas.docinvert
          REPLACE ucrsvale2.ftstamp WITH ucrsregvendas.ftstamp
          REPLACE ucrsvale2.fistamp WITH ucrsregvendas.fistamp
          REPLACE ucrsvale2.fdata WITH ucrsregvendas.fdata
          REPLACE ucrsvale2.ousrhora WITH ucrsregvendas.ousrhora
          REPLACE ucrsvale2.nmdoc WITH ucrsregvendas.nmdoc
          REPLACE ucrsvale2.fno WITH ucrsregvendas.fno
          REPLACE ucrsvale2.nome WITH ucrsregvendas.nome
          REPLACE ucrsvale2.no WITH ucrsregvendas.no
          REPLACE ucrsvale2.estab WITH ucrsregvendas.estab
          REPLACE ucrsvale2.ncont WITH ucrsregvendas.ncont
          REPLACE ucrsvale2.design WITH ucrsregvendas.design
          REPLACE ucrsvale2.ref WITH ucrsregvendas.ref
          REPLACE ucrsvale2.eaquisicao WITH ucrsregvendas.eaquisicao
          REPLACE ucrsvale2.oeaquisicao WITH ucrsregvendas.oeaquisicao
          REPLACE ucrsvale2.ndoc WITH ucrsregvendas.ndoc
          REPLACE ucrsvale2.sel WITH .T.
          REPLACE ucrsvale2.u_ltstamp WITH ucrsregvendas.u_ltstamp
          REPLACE ucrsvale2.u_ltstamp2 WITH ucrsregvendas.u_ltstamp2
          REPLACE ucrsvale2.u_codigo WITH ucrsregvendas.u_codigo
          REPLACE ucrsvale2.u_codigo2 WITH ucrsregvendas.u_codigo2
          REPLACE ucrsvale2.u_diploma WITH ucrsregvendas.u_diploma
          REPLACE ucrsvale2.qtt2 WITH ucrsregvendas.qtt
          REPLACE ucrsvale2.epv WITH ucrsregvendas.epv
          REPLACE ucrsvale2.etiliquido2 WITH ucrsregvendas.etiliquido
          REPLACE ucrsvale2.desconto WITH ucrsregvendas.desconto
          REPLACE ucrsvale2.desc2 WITH ucrsregvendas.desc2
          REPLACE ucrsvale2.u_descval WITH ucrsregvendas.u_descval
          REPLACE ucrsvale2.u_comp WITH ucrsregvendas.u_comp
          REPLACE ucrsvale2.u_psicont WITH ucrsregvendas.u_psicont
          REPLACE ucrsvale2.u_bencont WITH ucrsregvendas.u_bencont
          REPLACE ucrsvale2.u_recdata WITH ucrsregvendas.u_recdata
          REPLACE ucrsvale2.u_medico WITH ucrsregvendas.u_medico
          REPLACE ucrsvale2.u_nmutdisp WITH ucrsregvendas.u_nmutdisp
          REPLACE ucrsvale2.u_moutdisp WITH ucrsregvendas.u_moutdisp
          REPLACE ucrsvale2.u_cputdisp WITH ucrsregvendas.u_cputdisp
          REPLACE ucrsvale2.u_nmutavi WITH ucrsregvendas.u_nmutavi
          REPLACE ucrsvale2.u_moutavi WITH ucrsregvendas.u_moutavi
          REPLACE ucrsvale2.u_cputavi WITH ucrsregvendas.u_cputavi
          REPLACE ucrsvale2.u_ndutavi WITH ucrsregvendas.u_ndutavi
          REPLACE ucrsvale2.u_ddutavi WITH ucrsregvendas.u_ddutavi
          REPLACE ucrsvale2.u_idutavi WITH ucrsregvendas.u_idutavi
          REPLACE ucrsvale2.u_receita WITH ucrsregvendas.u_receita 
		  REPLACE ucrsvale2.lote WITH  IIF(empty(ucrsregvendas.lote), '', ucrsregvendas.lote) 
		  REPLACE ucrsvale2.dataValidade WITH IIF(EMPTY(ucrsregvendas.dataValidade), '', ucrsregvendas.dataValidade)
          IF EMPTY(ucrsregvendas.ficompart)
             REPLACE ucrsvale2.u_nbenef WITH ucrsregvendas.u_nbenef
             REPLACE ucrsvale2.u_nbenef2 WITH ucrsregvendas.u_nbenef2
          ELSE
             IF EMPTY(ucrsregvendas.u_nbenef) .AND.  .NOT. EMPTY(ucrsregvendas.u_nbenef2)
                REPLACE ucrsvale2.u_nbenef WITH ucrsregvendas.u_nbenef2
                REPLACE ucrsvale2.u_nbenef2 WITH ucrsregvendas.u_nbenef2
             ENDIF
             IF  .NOT. EMPTY(ucrsregvendas.u_nbenef) .AND. EMPTY(ucrsregvendas.u_nbenef2)
                REPLACE ucrsvale2.u_nbenef WITH ucrsregvendas.u_nbenef
                REPLACE ucrsvale2.u_nbenef2 WITH ucrsregvendas.u_nbenef
             ENDIF
          ENDIF
          REPLACE ucrsvale2.u_entpresc WITH ucrsregvendas.u_entpresc
          REPLACE ucrsvale2.u_nopresc WITH ucrsregvendas.u_nopresc
          REPLACE ucrsvale2.u_txcomp WITH ucrsregvendas.u_txcomp
          REPLACE ucrsvale2.u_epvp WITH ucrsregvendas.u_epvp
          REPLACE ucrsvale2.u_ettent1 WITH ucrsregvendas.u_ettent1
          REPLACE ucrsvale2.u_ettent2 WITH ucrsregvendas.u_ettent2
          REPLACE ucrsvale2.u_abrev WITH ucrsregvendas.u_abrev
          REPLACE ucrsvale2.u_abrev2 WITH ucrsregvendas.u_abrev2
          REPLACE ucrsvale2.u_epref WITH ucrsregvendas.u_epref
          REPLACE ucrsvale2.opcao WITH ucrsregvendas.opcao
          REPLACE ucrsvale2.codacesso WITH ucrsregvendas.codacesso
          REPLACE ucrsvale2.coddiropcao WITH ucrsregvendas.coddiropcao
          REPLACE ucrsvale2.token WITH ucrsregvendas.token
          REPLACE ucrsvale2.id_validacaodem WITH ucrsregvendas.id_validacaodem
          REPLACE ucrsvale2.c2codpost WITH ucrsregvendas.c2codpost
          IF ucrsregvendas.fmarcada
             IF lcvalidadev=.T.
                REPLACE ucrsvale2.qtt WITH ROUND(ucrsregvendas.eaquisicao, 0)
                REPLACE ucrsvale2.etiliquido WITH ROUND((ROUND(ucrsregvendas.eaquisicao, 0)*ucrsregvendas.etiliquido/ucrsregvendas.qtt), 2)
             ELSE
                REPLACE ucrsvale2.qtt WITH ucrsregvendas.qtt-ROUND(ucrsregvendas.eaquisicao, 0)
                REPLACE ucrsvale2.etiliquido WITH ROUND(((ucrsregvendas.qtt-ROUND(ucrsregvendas.eaquisicao, 0))*ucrsregvendas.etiliquido/ucrsregvendas.qtt), 2)
             ENDIF
          ELSE
             REPLACE ucrsvale2.qtt WITH ucrsregvendas.qtt
             REPLACE ucrsvale2.etiliquido WITH ucrsregvendas.etiliquido
          ENDIF
          IF lcvalidadev
             REPLACE ucrsvale2.fmarcada WITH ucrsregvendas.fmarcada
          ENDIF
          IF ucrsregvendas.lancacc=.T.
             REPLACE ucrsvale2.lancacc WITH .T.
          ENDIF

          uf_regVendas_getDadosUtente(ucrsregvendas.ftstamp, ucrsregvendas.fistamp)

       ENDIF
       SELECT ucrsvale2
       CALCULATE SUM(ucrsvale2.etiliquido), SUM(ucrsvale2.qtt) TO lctotal, lcqtttotal 
       regvendas.pgfreg.page1.txttotalreg.value = ROUND(lctotal, 2)
       regvendas.pgfreg.page1.txttotalqttreg.value = ROUND(lcqtttotal, 2)
    ELSE
    	IF verificaregreceita = .t.
	       uf_perguntalt_chama("ESSE PRODUTO J� FOI SELECCIONADO PARA REGULARIZA��O.", "OK", "", 64)
		ENDIF 
    ENDIF
 ELSE
    LOCAL lcstampftstampdel
    SELECT ucrsregvendas
    
    IF  .NOT. EMPTY(ALLTRIM(regvendas.txtproduto.value)) AND !EMPTY(ucrsregvendas.u_ltstamp)
    	SELECT ucrsfiltraprodreceita 
    	GO TOP 
 		DELETE FROM ucrsfiltraprodreceita WHERE ALLTRIM(ucrsfiltraprodreceita.u_receita) = ALLTRIM(ucrsregvendas.u_receita)
 		select ucrsregvendas
 	ENDIF 
 	
    lcstampftstampdel = ALLTRIM(ucrsregvendas.ftstamp)
    DELETE FROM uCrsVale2 WHERE ALLTRIM(ucrsvale2.ftstamp)=lcstampftstampdel
    SELECT ucrsregvendas
    LOCAL lcposlnrec
    lcposlnrec = RECNO("uCrsRegVendas")
    SELECT ucrsregvendas
    GOTO TOP
    SCAN
       IF ALLTRIM(ucrsregvendas.ftstamp)=lcstampftstampdel .AND. ucrsregvendas.sel=.T.
          REPLACE ucrsregvendas.sel WITH .F.
       ENDIF
    ENDSCAN
    SELECT ucrsregvendas
    GOTO TOP
    SELECT ucrsregvendas
    TRY
       GOTO lcposlnrec
    CATCH
    ENDTRY
 ENDIF
ENDFUNC
**
PROCEDURE uf_regvendas_removelinhareg
 LOCAL lctotal, lcqtttotal, lcpos, lcnrdoc, lcndoc, lcvalida, lcvalapaga
 STORE 0 TO lctotal, lcqtttotal, lcpos, lcnrdoc, lcndoc
 STORE .F. TO lcvalida
 SELECT ucrsvale2
 IF ucrsvale2.sel=.F.
    CREATE CURSOR uCrsApagaStamps (lcstamp C(25), lcltstamp C(25))
    SELECT ucrsvale2
    SELECT ucrsregvendas
    LOCATE FOR ALLTRIM(ucrsregvendas.fistamp)==ALLTRIM(ucrsvale2.fistamp)
    IF FOUND()
       SELECT ucrsregvendas
       REPLACE ucrsregvendas.sel WITH .F.
    ENDIF
    SELECT ucrsvale2
    IF  .NOT. EMPTY(ucrsvale2.u_ltstamp)
       SELECT ucrsvale2
       lcnrdoc = ucrsvale2.fno
       lcndoc = ucrsvale2.ndoc
       lcvalida = .T.
       GOTO TOP
       SCAN FOR ucrsvale2.fno=lcnrdoc .AND. ucrsvale2.ndoc=lcndoc
          INSERT INTO uCrsApagaStamps (lcstamp, lcltstamp) VALUES (ucrsvale2.fistamp, ucrsvale2.u_ltstamp)
          DELETE
       ENDSCAN
    ELSE
       SELECT ucrsvale2
       lcnrdoc = ucrsvale2.fno
       lcndoc = ucrsvale2.ndoc
       lcvalida = .T.
       INSERT INTO uCrsApagaStamps (lcstamp, lcltstamp) VALUES (ucrsvale2.fistamp, ucrsvale2.u_ltstamp)
       DELETE
       SELECT ucrsvale2
       GOTO TOP
       SCAN FOR ucrsvale2.fno=lcnrdoc .AND. ucrsvale2.ndoc=lcndoc
          lcvalida = .F.
       ENDSCAN
    ENDIF
    SELECT ucrsapagastamps
    GOTO TOP
    SCAN
       lcvalapaga = .T.
       DO WHILE lcvalapaga==.T.
          SELECT ucrsapagastamps
          SELECT fi
          LOCATE FOR (ALLTRIM(fi.ofistamp)==ALLTRIM(ucrsapagastamps.lcstamp)) .AND. (fi.lordem<>100000)
          IF FOUND("FI")
             uf_atendimento_apagalinha(.T.)
          ELSE
             lcvalapaga = .F.
          ENDIF
       ENDDO
       SELECT ucrsapagastamps
    ENDSCAN
    IF lcvalida
       SELECT ucrsapagastamps
       GOTO TOP
       SCAN
          SELECT fi
          GOTO TOP
          SCAN
             IF ( .NOT. EMPTY(fi.tkhposlstamp)) .AND. (ALLTRIM(fi.tkhposlstamp)==ALLTRIM(ucrsapagastamps.lcltstamp))
                IF fi.lordem==100000
                   REPLACE fi.tkhposlstamp WITH ''
                   REPLACE fi.design WITH ".SEM RECEITA"
                ELSE
                   uf_atendimento_apagalinha(.T.)
                ENDIF
             ENDIF
          ENDSCAN
          SELECT ucrsapagastamps
       ENDSCAN
    ENDIF
    IF USED("uCrsApagaStamps")
       fecha("uCrsApagaStamps")
    ENDIF
    uf_regvendas_calculatotaisreg(.T.)
    uf_atendimento_limpacab()
    regvendas.show
 ENDIF
ENDPROC
**
PROCEDURE uf_regvendas_calculatotaisreg
 LPARAMETERS tcbool
 LOCAL lcpos
 SELECT ucrsvale2
 lcpos = RECNO()
 REPLACE ucrsvale2.etiliquido WITH ucrsvale2.qtt*ucrsvale2.etiliquido2/ucrsvale2.qtt2
 CALCULATE SUM(ucrsvale2.etiliquido), SUM(ucrsvale2.qtt) TO lctotal, lcqtttotal 
 regvendas.pgfreg.page1.txttotalreg.value = ROUND(lctotal, 2)
 regvendas.pgfreg.page1.txttotalqttreg.value = ROUND(lcqtttotal, 2)
 IF tcbool=.T.
    SELECT ucrsvale2
    GOTO TOP
 ENDIF
ENDPROC
**
FUNCTION uf_regvendas_actdadoscabvenda
 IF USED("uCrsRegVendas")
    SELECT ucrsregvendas
    regvendas.pgfreg.page1.txtplano.value = "["+ucrsregvendas.u_codigo+"]"+" "+ucrsregvendas.u_design
    regvendas.pgfreg.page1.txtl.value = ucrsregvendas.u_lote
    regvendas.pgfreg.page1.txtnr.value = ucrsregvendas.u_nslote
    regvendas.pgfreg.page1.txtl2.value = ucrsregvendas.u_lote2
    regvendas.pgfreg.page1.txtnr2.value = ucrsregvendas.u_nslote2
    regvendas.pgfreg.page1.txtcliente.value = ucrsregvendas.nome
    regvendas.pgfreg.page1.txtno.value = ucrsregvendas.no
    regvendas.pgfreg.page1.txtestab.value = ucrsregvendas.estab
    regvendas.pgfreg.page1.txtvaldivida.value = ROUND(ucrsregvendas.totlin-ucrsregvendas.valoradiantado, 2)
    regvendas.pgfreg.page1.txtvalpago.value = ROUND(ucrsregvendas.valoradiantado, 2)
    IF ALLTRIM(ucrsregvendas.nmdoc)=='Reserva de Cliente'
       regvendas.pgfreg.page1.txtstk.visible = .T.
       regvendas.pgfreg.page1.label6.visible = .T.
       regvendas.pgfreg.page1.txtstk.value = ALLTRIM(ucrsregvendas.stocktotal)
    ELSE
       regvendas.pgfreg.page1.txtstk.visible = .F.
       regvendas.pgfreg.page1.label6.visible = .F.
    ENDIF
    IF ALLTRIM(ucrsregvendas.nmdoc)=='Reserva de Cliente'
       regvendas.menu1.estado("fechar", "SHOW")
    ELSE
       regvendas.menu1.estado("fechar", "HIDE")
    ENDIF
    RETURN .T.
 ENDIF
ENDFUNC
**
PROCEDURE uf_regvendas_configgrdregvendas
 LOCAL ti, i
 STORE 0 TO ti, i
 ti = regvendas.pgfreg.page1.grdvendas.columncount
 FOR i = 1 TO ti
    IF UPPER(ALLTRIM((regvendas.pgfreg.page1.grdvendas.columns(i).name)))=="ETILIQUIDO"
       regvendas.pgfreg.page1.grdvendas.columns(i).dynamicbackcolor = "IIF(uCrsRegVendas.valoradiantado=0, IIF( uCrsRegVendas.totlin =0 ,rgb[255,255,255],rgb[255,0,0]) , IIF( uCrsRegVendas.totlin = uCrsRegVendas.valoradiantado,rgb[255,255,255],rgb[255,163,70]))"
    ENDIF
    IF UPPER(ALLTRIM((regvendas.pgfreg.page1.grdvendas.columns(i).name)))=="QTT"
       regvendas.pgfreg.page1.grdvendas.columns(i).dynamicbackcolor = "IIF(uCrsRegVendas.qtt<=uCrsRegVendas.stock and ALLTRIM(uCrsRegVendas.nmdoc)='Reserva de Cliente', rgb[39,174,97],rgb[255,255,255])"
    ENDIF
 ENDFOR
 regvendas.pgfreg.page1.grdvendas.refresh
 regvendas.pgfreg.page1.grdvreg.refresh
ENDPROC
**
PROCEDURE uf_regvendas_pesquisarregvendas
 LOCAL lcproduto, lcnvenda, lccliente, lcno, lcestab, lcnatend, lctop
 STORE '' TO lcproduto, lccliente, lcnatend
 STORE -1 TO lcnvenda, lcno, lcestab
 

 regua(0,0,"A carregar dados...")

 IF  .NOT. EMPTY(ALLTRIM(regvendas.txtproduto.value))
    lcproduto = ALLTRIM(regvendas.txtproduto.value)
 ELSE
    lcproduto = ''
 ENDIF
 IF  .NOT. EMPTY(astr(regvendas.txtnvenda.value))
    lcnvenda = ALLTRIM(STRTRAN(regvendas.txtnvenda.value, ' ', ''))
 ELSE
    lcnvenda = -1
 ENDIF
 IF regvendas.pgfreg.page2.chkporutente.tag="true"
    lccliente = ''
 ELSE
    IF  .NOT. EMPTY(ALLTRIM(regvendas.txtcliente.value))
       lccliente = ALLTRIM(regvendas.txtcliente.value)
    ELSE
       lccliente = ''
    ENDIF
 ENDIF
 IF regvendas.pgfreg.page2.chkporutente.tag="true"
    lcno = -1
 ELSE
    IF  .NOT. EMPTY(astr(regvendas.txtno.value))
       lcno = astr(regvendas.txtno.value)
    ELSE
       lcno = -1
    ENDIF
 ENDIF
 IF regvendas.pgfreg.page2.chkporutente.tag="true"
    lcestab = -1
 ELSE
    IF  .NOT. EMPTY(astr(regvendas.txtestab.value))
       lcestab = astr(regvendas.txtestab.value)
    ELSE
       lcestab = -1
    ENDIF
 ENDIF
 IF  .NOT. EMPTY(astr(regvendas.txtnatend.value))
    lcnatend = astr(regvendas.txtnatend.value)
 ELSE
    lcnatend = ''
 ENDIF
 lctop = 100000
 TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_touch_regularizarVendas '<<lcProduto>>', <<lcNVenda>>, '<<lcCliente>>', <<lcNo>>, <<lcEstab>>, '<<uf_gerais_getdate(REGVENDAS.txtDataIni.value, "SQL")>>', '<<uf_gerais_getdate(REGVENDAS.txtDataFim.value, "SQL")>>', '<<lcNAtend>>', <<ch_userno>>, '<<Alltrim(ch_grupo)>>', '<<mySite>>', <<lcTop>>, <<pesqmmnif>>
 ENDTEXT
 uf_gerais_actgrelha("REGVENDAS.pgfReg.page1.grdVendas", "uCrsRegVendas", lcsql)
 


 SELECT ucrsregvendas
 GOTO TOP
 
 SCAN
    IF ucrsregvendas.qtt<>ucrsregvendas.qtt2
       REPLACE ucrsregvendas.etiliquido WITH ucrsregvendas.qtt*ucrsregvendas.etiliquido2/ucrsregvendas.qtt2
    ENDIF
 ENDSCAN
 SELECT ucrsregvendas
 GOTO TOP
 IF USED("uCrsVale")
    IF  .NOT. USED("uCrsVale2")
       SELECT * FROM uCrsVale INTO CURSOR uCrsVale2 READWRITE
       SELECT ucrsvale2
       GOTO TOP
       SCAN FOR ucrsvale2.copied=.T.
          REPLACE ucrsvale2.sel WITH .F.
       ENDSCAN
       SELECT ucrsvale2
       GOTO TOP
       LOCAL lctotal, lcqtttotal
       STORE 0 TO lctotal, lcqtttotal
       CALCULATE SUM(ucrsvale2.etiliquido), SUM(ucrsvale2.qtt) TO lctotal, lcqtttotal 
       lcvalidatot = .T.
       SELECT ucrsvale2
       GOTO TOP
    ENDIF
 ELSE
    IF  .NOT. USED("uCrsVale2")
       SELECT * FROM uCrsRegVendas WHERE 1=0 INTO CURSOR uCrsVale2 READWRITE
    ENDIF
 ENDIF
 **DO CASE
 **   CASE sores=1
 **      SELECT ucrsregvendas
 **      SET FILTER TO ALLTRIM(ucrsregvendas.nmdoc)="Reserva de Cliente"
 **   CASE regvendas.sosusp=1
 **      SELECT ucrsregvendas
 **      IF regvendas.socred==1
 **         SET FILTER TO UPPER(RIGHT(ALLTRIM(ucrsregvendas.nmdoc), 3))="(S)" .AND. ucrsregvendas.qtt>ucrsregvendas.eaquisicao .AND. ucrsregvendas.u_backoff=.F.
 **      ELSE
 **         SET FILTER TO UPPER(RIGHT(ALLTRIM(ucrsregvendas.nmdoc), 3))="(S)"
 **      ENDIF
 **   CASE regvendas.socred=1
 **      SELECT ucrsregvendas
 **      IF regvendas.sosusp==1
 **         SET FILTER TO UPPER(RIGHT(ALLTRIM(ucrsregvendas.nmdoc), 3))="(S)" .AND. ucrsregvendas.qtt>ucrsregvendas.eaquisicao .AND. ucrsregvendas.u_backoff=.F. .AND. ALLTRIM(ucrsregvendas.nmdoc)<>"Reserva de Cliente"
 **      ELSE
 **         SET FILTER TO ucrsregvendas.qtt>ucrsregvendas.eaquisicao .AND. ucrsregvendas.u_backoff=.F. .AND. ALLTRIM(ucrsregvendas.nmdoc)<>"Reserva de Cliente"
 **      ENDIF
 **ENDCASE
 LOCAL lcfno, lcfno2, lcdocinvert
 STORE 0 TO lcfno, lcfno2
 SELECT ucrsregvendas
 GOTO TOP
 lcfno = ucrsregvendas.fno
 SCAN
    lcfno2 = ucrsregvendas.fno
    IF lcfno<>lcfno2
       IF lcdocinvert
          lcdocinvert = .F.
       ELSE
          lcdocinvert = .T.
       ENDIF
    ENDIF
    REPLACE ucrsregvendas.docinvert WITH lcdocinvert
    lcfno = ucrsregvendas.fno
 ENDSCAN
 SELECT ucrsregvendas
 GOTO TOP

 regua(2)
 
 uf_regvendas_configgrdregvendas()

 uf_regvendas_filtrar_ecra(regvendas.txtbtnsuspensas.fontbold, regvendas.txtbtncreditos.fontbold, regvendas.txtbtnreservas.fontbold, regvendas.txtbtnencomendas.fontbold)

 uf_regvendas_marcarSel()

ENDPROC
**
PROCEDURE uf_regvendas_filtrarsusp
 LPARAMETERS lcsusp
 LOCAL lccursor
 DO CASE
    CASE regvendas.pgfreg.activepage==1
       lccursor = "uCrsRegVendas"
    CASE regvendas.pgfreg.activepage==2
       IF regvendas.pgfreg.page2.chkporproduto.tag=="true"
          lccursor = "uCrsRegCreditos"
       ELSE
          lccursor = "uCrsRegCreditosValor"
       ENDIF
 ENDCASE
 IF lcsusp
    SELECT &lccursor
    IF regvendas.pgfreg.activepage==1
       IF regvendas.socred==1
          SET FILTER TO UPPER(RIGHT(ALLTRIM(&lccursor..nmdoc),3)) = "(S)" AND &lccursor..qtt > &lccursor..eaquisicao AND &lccursor..u_backoff = .f.
       ELSE
          SET FILTER TO UPPER(RIGHT(ALLTRIM(&lccursor..nmdoc),3)) = "(S)"
       ENDIF
       regvendas.sosusp = 1
    ELSE
       SET FILTER TO UPPER(RIGHT(ALLTRIM(&lccursor..nmdoc),3)) = "(S)"
    ENDIF
    GOTO TOP
 ELSE
    SELECT &lccursor
    IF regvendas.pgfreg.activepage==1
       IF regvendas.socred==1
          SET FILTER TO &lccursor..qtt > &lccursor..eaquisicao AND &lccursor..u_backoff = .f.
       ELSE
          SET FILTER TO
       ENDIF
       regvendas.sosusp = 0
    ELSE
       SET FILTER TO
    ENDIF
    GOTO TOP
 ENDIF
 DO CASE
    CASE regvendas.pgfreg.activepage==1
       uf_regvendas_configgrdregvendas()
    CASE regvendas.pgfreg.activepage==2
       IF regvendas.pgfreg.page2.chkporproduto.tag=="true"
          regvendas.pgfreg.page2.grdregcredito.refresh()
       ELSE
          regvendas.pgfreg.page2.grdregcreditovalor.refresh()
       ENDIF
 ENDCASE
ENDPROC
**
PROCEDURE uf_regvendas_filtrarres
 LPARAMETERS lcres


 LOCAL lccursor
 lccursor = "uCrsRegVendas"
 IF lcres
    SELECT &lccursor
    SET FILTER TO ALLTRIM(&lccursor..nmdoc) = "Reserva de Cliente" 
    sores = 1
    GOTO TOP

    IF WEXIST("regvendas")
        regvendas.txtbtnreservas.fontbold = .T.
    ENDIF

 ELSE
    SELECT &lccursor
    SET FILTER TO
    sores = 0
    GOTO TOP

    IF WEXIST("regvendas")
        regvendas.txtbtnreservas.fontbold = .F.
    ENDIF
    
 ENDIF
 uf_regvendas_configgrdregvendas()
ENDPROC
**
PROCEDURE uf_regvendas_filtrarenc
 LPARAMETERS lcres
 LOCAL lccursor
 lccursor = "uCrsRegVendas"
 IF lcres
    SELECT &lccursor
    SET FILTER TO ALLTRIM(&lccursor..nmdoc) = "Encomenda de Cliente" 
    soenc = 1
    GOTO TOP
 ELSE
    SELECT &lccursor
    SET FILTER TO
    soenc = 0
    GOTO TOP
 ENDIF
 uf_regvendas_configgrdregvendas()
ENDPROC
**
PROCEDURE uf_regvendas_filtrarvdsusp
 LOCAL lccursor
 DO CASE
    CASE regvendas.pgfreg.activepage==1
       lccursor = "uCrsRegVendas"
    CASE regvendas.pgfreg.activepage==2
       IF regvendas.pgfreg.page2.chkporproduto.tag=="true"
          lccursor = "uCrsRegCreditos"
       ELSE
          lccursor = "uCrsRegCreditosValor"
       ENDIF
 ENDCASE
 IF regvendas.pgfreg.page2.chksosusp.tag="true"
    SELECT &lccursor
    SET FILTER TO UPPER(RIGHT(ALLTRIM(&lccursor..nmdoc),3)) = "(S)"	
 ELSE
 ENDIF
 DO CASE
    CASE regvendas.pgfreg.activepage==1
       uf_regvendas_configgrdregvendas()
    CASE regvendas.pgfreg.activepage==2
       IF regvendas.pgfreg.page2.chkporproduto.tag=="true"
          regvendas.pgfreg.page2.grdregcredito.refresh()
       ELSE
          regvendas.pgfreg.page2.grdregcreditovalor.refresh()
       ENDIF
 ENDCASE
 regvendas.menu1.actualizar.click
ENDPROC
**
PROCEDURE uf_regvendas_filtrarvdnaosusp
 LOCAL lccursor
 DO CASE
    CASE regvendas.pgfreg.activepage==1
       lccursor = "uCrsRegVendas"
    CASE regvendas.pgfreg.activepage==2
       IF regvendas.pgfreg.page2.chkporproduto.tag=="true"
          lccursor = "uCrsRegCreditos"
       ELSE
          lccursor = "uCrsRegCreditosValor"
       ENDIF
 ENDCASE
 IF regvendas.pgfreg.page2.chksonaosusp.tag="true"
    SELECT &lccursor
    SET FILTER TO UPPER(RIGHT(ALLTRIM(&lccursor..nmdoc),3)) != "(S)"	
 ELSE
 ENDIF
 DO CASE
    CASE regvendas.pgfreg.activepage==1
       uf_regvendas_configgrdregvendas()
    CASE regvendas.pgfreg.activepage==2
       IF regvendas.pgfreg.page2.chkporproduto.tag=="true"
          regvendas.pgfreg.page2.grdregcredito.refresh()
       ELSE
          regvendas.pgfreg.page2.grdregcreditovalor.refresh()
       ENDIF
 ENDCASE
 regvendas.menu1.actualizar.click
ENDPROC


**guadar o cliente original
FUNCTION uf_regvendas_clienteInicialComFacturacaoExterna
  LPARAMETERS lcFiStamp, lcFtstamp
  	LOCAL lcPosFt2Temp, lcClientOrigStamp, lcsql  

      
	if(EMPTY(lcFiStamp))
		RETURN .f.
	ENDIF
	
	if(EMPTY(lcFtstamp))
		RETURN .f.
	ENDIF
	
	if(!USED("ft"))
		RETURN .f.
	ENDIF
	
	if(!USED("ft2"))
		RETURN .f.
	ENDIF

	
	SELECT ft2
    lcPosFt2Temp= RECNO("ft2")
    
    
    fecha("ucrsFtClientOriginal")
    
	lcsql = ''
	TEXT TO lcsql TEXTMERGE NOSHOW
			SELECT  TOP 1 RTRIM(LTRIM(ISNULL(u_hclstamp,''))) AS u_hclstamp
			from ft (nolock) 
			inner join fi(nolock) on fi.ftstamp = ft.ftstamp
			where 
				fi.fistamp = '<<lcFiStamp>>'
	ENDTEXT
	uf_gerais_actgrelha("", "ucrsFtClientOriginal", lcsql)
	

	

	lcClientOrigStamp  = ''
	 
	SELECT ucrsFtClientOriginal
	GO TOP
	lcClientOrigStamp   = ALLTRIM(ucrsFtClientOriginal.u_hclstamp)
	

    UPDATE ft2 SET u_hclstamp1 = ALLTRIM(lcClientOrigStamp)   WHERE ft2stamp  = ALLTRIM(lcFtstamp)

    fecha("ucrsFtClientOriginal")

    SELECT ft2
    TRY
       GOTO lcPosFt2Temp
    CATCH
       GOTO TOP
    ENDTRY
	
	RETURN .t.
		
ENDFUNC






**
PROCEDURE uf_regvendas_filtrarcred
 LPARAMETERS lccred
 IF lccred
    SELECT ucrsregvendas
    IF regvendas.sosusp==1
       SET FILTER TO UPPER(RIGHT(ALLTRIM(ucrsregvendas.nmdoc), 3))="(S)" .AND. ucrsregvendas.qtt>ucrsregvendas.eaquisicao .AND. ucrsregvendas.u_backoff=.F. .AND. ALLTRIM(ucrsregvendas.nmdoc)<>"Reserva de Cliente"
    ELSE
       SET FILTER TO ucrsregvendas.qtt>ucrsregvendas.eaquisicao .AND. ucrsregvendas.u_backoff=.F. .AND. ALLTRIM(ucrsregvendas.nmdoc)<>"Reserva de Cliente"
    ENDIF
    GOTO TOP
    regvendas.socred = 1
 ELSE
    SELECT ucrsregvendas
    IF regvendas.sosusp==1
       SET FILTER TO UPPER(RIGHT(ALLTRIM(ucrsregvendas.nmdoc), 3))="(S)"
    ELSE
       SET FILTER TO
    ENDIF
    GOTO TOP
    regvendas.socred = 0
 ENDIF
 uf_regvendas_configgrdregvendas()
ENDPROC
**
FUNCTION uf_regvendas_regvendassel
 LPARAMETERS lbadreserva, lbnovavenda
 IF  .NOT. USED("uCrsVale2")
    RETURN .F.
 ENDIF
 
 LOCAL lcnrvendas, lccountprod, lcposfi
 lcnrvendas = atendimento.txttotalvendas.value
 lccountprod = 0
 SELECT fi
 GOTO TOP
 SCAN
    IF  .NOT. EMPTY(fi.ref)
       lccountprod = lccountprod+1
    ENDIF
 ENDSCAN
 IF lccountprod=0 .AND. lcnrvendas=1
    GOTO TOP
 ELSE
    GOTO BOTTOM
 ENDIF
 IF EMPTY(uf_regvendas_verificarefsinativas())
    RETURN .F.
 ENDIF

 SELECT * FROM ucrsvale2 WHERE ALLTRIM(ucrsvale2.nmdoc)<>'Reserva de Cliente' AND ALLTRIM(ucrsvale2.nmdoc)<>'Encomenda de Cliente' ORDER BY ftstamp INTO CURSOR uCrsvale READWRITE
 
 LOCAL lcstamp1, lcstamp2, lccriounv, lcnocl, lcestabcl, lcvalalterapvp, lcnomecl, lcncont
 STORE 0 TO lcnocl, lcestabcl
 STORE .F. TO lcvalalterapvp
 STORE '' TO lcnomecl, lcncont
 lcstamp2 = "hgdsxwyz"
 SELECT ucrsvale
 GOTO TOP
 
 
 SCAN FOR ucrsvale.copied==.F.
    lcstamp1 = ucrsvale.ftstamp
    IF  .NOT. (UPPER(ALLTRIM(lcstamp1))==UPPER(ALLTRIM(lcstamp2))) .AND.  .NOT. lbnovavenda

       IF lccountprod<>0 .OR. lcnrvendas<>1
          lccriounv = uf_atendimento_novavenda(.F.)
          SELECT fi
          GOTO BOTTOM
       ELSE
          lccountprod = 1
       ENDIF
      
       LOCAL lcvalefistamp
       SELECT ucrsvale
	   
       lcvalefistamp = ALLTRIM(ucrsvale.fistamp)
       IF  .NOT. EMPTY(ucrsvale.u_codigo)
          IF EMPTY(ucrsvale.u_codigo2)
             SELECT fi
             REPLACE fi.design WITH ".["+ALLTRIM(ucrsvale.u_codigo)+"]["+ALLTRIM(ucrsvale.u_abrev)+"] Receita:"+ALLTRIM(ucrsvale.u_receita)+" -"
          ELSE
             REPLACE fi.design WITH ".["+ALLTRIM(ucrsvale.u_codigo)+"]["+ALLTRIM(ucrsvale.u_abrev)+"]["+ALLTRIM(ucrsvale.u_codigo2)+"]["+ALLTRIM(ucrsvale.u_abrev2)+"] Receita:"+ALLTRIM(ucrsvale.u_receita)+" -"
          ENDIF
          IF  .NOT. uf_gerais_actgrelha("", 'uCrsTempComp', [exec up_receituario_dadosDoPlano ']+ALLTRIM(ucrsvale.u_codigo)+['])
             uf_perguntalt_chama("OCORREU UM ERRO A APLICAR O PLANO.", "OK", "", 16)
             RETURN .F.
          ELSE

             IF RECCOUNT("uCrsTempComp")>0
                SELECT fi
                DELETE FROM uCrsAtendComp WHERE UPPER(ALLTRIM(fistamp))==UPPER(ALLTRIM(fi.fistamp))
                SELECT * FROM uCrsAtendComp UNION ALL SELECT * FROM uCrsTempComp INTO CURSOR uCrsAtendComp READWRITE
                TEXT TO lcsql TEXTMERGE NOSHOW
							select u_receita, u_nbenef, codAcesso, codDirOpcao, u_design, u_codigo, u_nbenef2, u_nbenef, token, c2codpost
							from ft2 (nolock) where ft2stamp in (select ftstamp from ft (nolock) where u_nratend in( select u_nratend from ft (nolock) where u_nratend in( select u_nratend from ft (nolock) where ftstamp in (select ftstamp from fi (nolock) where fistamp='<<ALLTRIM(lcvalefistamp)>>')))
							and nmdoc='Inser��o de Receita') 
                ENDTEXT
                IF  .NOT. uf_gerais_actgrelha("", "ucrsDadosRec1", lcsql)
                   uf_perguntalt_chama("N�o foi poss�vel Construir o cursor os dados da receita da reserva. Por favor contacte o suporte.", "OK", "", 32)
                   regua(2)
                   RETURN .F.
                ENDIF
                
                

                
                IF RECCOUNT("ucrsDadosRec1")=0
                   SELECT ucrsatendcomp
                   GOTO BOTTOM
                   REPLACE ucrsatendcomp.fistamp WITH fi.fistamp
                   REPLACE ucrsatendcomp.codacesso WITH ucrsvale.codacesso
                   REPLACE ucrsatendcomp.coddiropcao WITH ucrsvale.coddiropcao
                   REPLACE ucrsatendcomp.token WITH ucrsvale.token
                   REPLACE ucrsatendcomp.id_validacaodem WITH ucrsvale.id_validacaodem
                   REPLACE ucrsatendcomp.datareceita WITH ucrsvale.u_recdata
                   
                   if(!EMPTY(ALLTRIM(ucrsvale.c2codpost)))
                   		REPLACE ucrsatendcomp.ncartao WITH ucrsvale.c2codpost
                   	ELSE
                   		REPLACE ucrsatendcomp.ncartao WITH ucrsvale.u_nbenef2	
                    ENDIF
                  	
                   		
                   IF  .NOT. EMPTY(ucrsvale.token)
                      SELECT fi
                      REPLACE fi.dem WITH .T.
                      REPLACE fi.receita WITH ALLTRIM(ucrsvale.u_receita)
                      REPLACE fi.token WITH ucrsvale.token
                      REPLACE fi.id_validacaodem WITH ucrsvale.id_validacaodem
                      lcfilordemcab = LEFT(astr(fi.lordem), 2)+REPLICATE('0', LEN(astr(fi.lordem))-2)
                      UPDATE Fi SET tipor = "DEM", dem = .T., token = ucrsvale.token, receita = ucrsvale.u_receita WHERE astr(fi.lordem)=lcfilordemcab
                   ELSE
                      lcfilordemcab = LEFT(astr(fi.lordem), 2)+REPLICATE('0', LEN(astr(fi.lordem))-2)
                      UPDATE Fi SET tipor = "RM" WHERE astr(fi.lordem)=lcfilordemcab
                   ENDIF
                ELSE
                   SELECT ucrsatendcomp
                   GOTO BOTTOM
                   REPLACE ucrsatendcomp.fistamp WITH fi.fistamp
                   REPLACE ucrsatendcomp.codacesso WITH ucrsdadosrec1.codacesso
                   REPLACE ucrsatendcomp.coddiropcao WITH ucrsdadosrec1.coddiropcao
                   REPLACE ucrsatendcomp.token WITH ucrsdadosrec1.token
                   REPLACE ucrsatendcomp.id_validacaodem WITH ucrsvale.id_validacaodem
                   REPLACE ucrsatendcomp.datareceita WITH ucrsvale.u_recdata                  
                   REPLACE ucrsatendcomp.ncartao WITH ucrsdadosrec1.u_nbenef
                   IF  .NOT. EMPTY(ucrsdadosrec1.token)
                      SELECT fi
                      REPLACE fi.dem WITH .T.
                      REPLACE fi.receita WITH ALLTRIM(ucrsdadosrec1.u_receita)
                      REPLACE fi.token WITH ucrsdadosrec1.token
                      REPLACE fi.id_validacaodem WITH ucrsvale.id_validacaodem
                      lcfilordemcab = LEFT(astr(fi.lordem), 2)+REPLICATE('0', LEN(astr(fi.lordem))-2)
                      UPDATE Fi SET tipor = "DEM", dem = .T., token = ucrsdadosrec1.token, receita = ucrsdadosrec1.u_receita WHERE astr(fi.lordem)=lcfilordemcab
                   ELSE
                      lcfilordemcab = LEFT(astr(fi.lordem), 2)+REPLICATE('0', LEN(astr(fi.lordem))-2)
                      UPDATE Fi SET tipor = "RM" WHERE astr(fi.lordem)=lcfilordemcab
                   ENDIF
                ENDIF
                fecha("ucrsDadosRec1")
             ELSE
                uf_perguntalt_chama("N�O EXISTE NENHUM PLANO COM ESSE C�DIGO.", "OK", "", 48)
             ENDIF
          ENDIF
          SELECT ucrsvale
          IF ( .NOT. EMPTY(ucrsvale.u_ltstamp) .OR.  .NOT. EMPTY(ucrsvale.u_ltstamp2)) .AND.  .NOT. (ALLTRIM(ucrsvale.ref)=="R000001")
             SELECT fi
             IF  .NOT. EMPTY(ucrsvale.u_ltstamp)
                REPLACE fi.tkhposlstamp WITH ucrsvale.u_ltstamp
             ELSE
                REPLACE fi.tkhposlstamp WITH ucrsvale.u_ltstamp2
             ENDIF
             REPLACE fi.nccod WITH ALLTRIM(ucrsvale.u_codigo)
             REPLACE fi.ncinteg WITH ALLTRIM(ucrsvale.u_codigo2)
             IF ucrsvale.lancacc==.T.
                REPLACE fi.marcada WITH .T.
             ENDIF
          ENDIF
       ENDIF
       SELECT fi
       REPLACE fi.u_nbenef WITH ucrsvale.u_nbenef
       REPLACE fi.u_nbenef2 WITH ucrsvale.u_nbenef2
       **IF !EMPTY(ucrsvale.u_nopresc)
       **     REPLACE fi.szzstamp WITH ucrsvale.u_nopresc
       ** ELSE
**
       **     uv_med = uf_gerais_getUmValor("B_dadosPsico", "u_medico", "ftstamp = '" + ALLTRIM(ucrsvale.ftstamp) + "'")
**
       **     IF !EMPTY(uv_med)
**
       **         REPLACE fi.szzstamp WITH uv_med
**
       **     ELSE
**
                **uv_med = uf_gerais_getUmValor("B_dadosPsico", "u_medico", "ftstamp = (	select 	ft2stamp from ft2(nolock) where ft2stamp <> '" + ALLTRIM(ucrsvale.ftstamp) + "' and stampreserva = ISNULL((select stampreserva from ft2(nolock) as fft2 where fft2.ft2stamp = '" + ALLTRIM(ucrsvale.ftstamp) + "' and stampreserva <> ''), 'XXX'))")
**
       **         IF !EMPTY(uv_med)
**
       **             REPLACE fi.szzstamp WITH uv_med
**
       **         ENDIF
**
**
**
       **     ENDIF
**
       **ENDIF
       REPLACE fi.rvpstamp WITH ucrsvale.u_entpresc
    ENDIF

    **uf_regVendas_getDadosUtente(ucrsvale.ftstamp)

    lcnocl = ucrsvale.no
    lcnoestab = ucrsvale.estab
    lcnomecl = ucrsvale.nome
    lcncont = ucrsvale.ncont
    uf_atendimento_adicionalinhafi(.F., .F.)
    SELECT fi
    lcposfi = RECNO("fi")
    
    IF  .NOT. EMPTY(ucrsvale.token)
      SELECT fi
      REPLACE fi.dem WITH .T.
      REPLACE fi.receita WITH ALLTRIM(ucrsvale.u_receita)
      REPLACE fi.token WITH ucrsvale.token
      REPLACE fi.id_validacaodem WITH ucrsvale.id_validacaodem
      lcfilordemcab = LEFT(astr(fi.lordem), 2)+REPLICATE('0', LEN(astr(fi.lordem))-2)
      UPDATE Fi SET tipor = "DEM", dem = .T., token = ucrsvale.token, receita = ucrsvale.u_receita WHERE astr(fi.lordem)=lcfilordemcab
    ENDIF
    SELECT ucrsvale
    SELECT fi
    REPLACE fi.ref WITH ALLTRIM(ucrsvale.ref)
    REPLACE fi.ofistamp WITH ALLTRIM(ucrsvale.fistamp)
    SELECT ucrsvale


	TEXT TO uv_sel TEXTMERGE NOSHOW

		SELECT
			bonusTipo,
			bonusId,
			bonusDescr,
			idProcesso,
			idEpisodio,
			dataValidade
		FROM
			fi2(nolock)
		WHERE
			fi2.fistamp = '<<ALLTRIM(ucrsvale.fistamp)>>'

	ENDTEXT

	IF !uf_gerais_actGrelha("", "uc_oriFi2", uv_sel)
		RETURN .F.
	ENDIF

	SELECT fi
	uv_curFistamp = fi.fistamp

	SELECT fi2
	LOCATE FOR fi2.fistamp = uv_curFistamp

	IF FOUND()
		SELECT fi2
		REPLACE fi2.bonusTipo WITH uc_oriFi2.bonusTipo,;
				fi2.bonusId WITH uc_oriFi2.bonusId,;
				fi2.bonusDescr WITH uc_oriFi2.bonusDescr;
				fi2.idProcesso WITH ALLTRIM(uc_oriFi2.idProcesso);
				fi2.idEpisodio WITH ALLTRIM(uc_oriFi2.idEpisodio);
				fi2.dataValidade WITH uc_oriFi2.dataValidade
	ENDIF

	SELECT FI

    uf_regvendas_clienteInicialComFacturacaoExterna(ALLTRIM(ucrsvale.fistamp),ALLTRIM(fi.ftstamp))
      
    uf_atendimento_actref()

    SELECT fi
    lcref = ALLTRIM(fi.ref)
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_stocks_procuraRef '<<Alltrim(lcRef)>>', <<mysite_nr>>
    ENDTEXT
    uf_gerais_actgrelha("", "uCrsFindRef", lcsql)
    lcarvato = ucrsfindref.dispositivo_seguranca
    IF lcarvato

		uf_insert_fi_trans_info_seminfo()
		atendimento.pgfdados.page2.txtref.disabledbackcolor = RGB(255, 0, 0)
		atendimento.pgfdados.page2.txtref.forecolor = RGB(255, 255, 255)

    ENDIF
    fecha("uCrsFindRef")
    SELECT fi
    TRY
       GOTO lcposfi
    CATCH
       GOTO BOTTOM
    ENDTRY
    SELECT fi

    IF UPPER(ALLTRIM(fi.ref))=="R000001"
       REPLACE fi.tabiva WITH ucrsvale.tabiva
       REPLACE fi.iva WITH ucrsvale.iva
    ENDIF
    IF  .NOT. lbadreserva
       SELECT fi
       SELECT ucrsvale
       IF fi.u_epvp<>ucrsvale.u_epvp 
          &&nao pergunta se for dem
		  if(EMPTY(ucrsvale.id_validacaodem))
		  	  PUBLIC forcaalterapreco
		  	  forcaalterapreco = .f.
	          IF  .NOT. uf_perguntalt_chama("O PVP do produto: ("+ALLTRIM(fi.ref)+") "+UPPER(ALLTRIM(fi.design))+" � diferente da venda original."+CHR(13)+"PVP Atual: "+astr(fi.u_epvp, 8, 2)+CHR(13)+"PVP Venda Original: "+astr(ucrsvale.u_epvp, 8, 2)+CHR(13)+"Prentende utilizar qual PVP?", "Atual", "Vd. Origem")
	             lcvalalterapvp = .T.
	          ELSE
				forcaalterapreco = .t.
	          ENDIF
	       ELSE
	       		&&usa sempre a de origem se for dem
	       		lcvalalterapvp = .T.
	       ENDIF 
	       
       ENDIF
    ENDIF

    
    SELECT fi
    REPLACE fi.qtt WITH ucrsvale.qtt
    IF uf_gerais_getParameter_site('ADM0000000125', 'BOOL', mySite)
    	REPLACE fi.qtdem WITH 0
    ELSE
    	REPLACE fi.qtdem WITH ucrsvale.qtt
    ENDIF 
    
    REPLACE fi.ndoc WITH ucrsvale.ndoc
    IF  .NOT. (ALLTRIM(ucrsvale.ref)=="R000001")
       IF  .NOT. EMPTY(ucrsvale.u_ltstamp)
          REPLACE fi.tkhposlstamp WITH ALLTRIM(ucrsvale.u_ltstamp)
       ELSE
          REPLACE fi.tkhposlstamp WITH ALLTRIM(ucrsvale.u_ltstamp2)
       ENDIF
       REPLACE fi.nccod WITH ALLTRIM(ucrsvale.u_codigo)
       REPLACE fi.ncinteg WITH ALLTRIM(ucrsvale.u_codigo2)
    ENDIF
    REPLACE fi.fmarcada WITH ucrsvale.fmarcada
    REPLACE fi.desconto WITH ucrsvale.desconto
    REPLACE fi.desc2 WITH ucrsvale.desc2
    REPLACE fi.u_descval WITH ucrsvale.u_descval
    REPLACE fi.u_comp WITH ucrsvale.u_comp
    REPLACE fi.u_psicont WITH ucrsvale.u_psicont
    REPLACE fi.u_bencont WITH ucrsvale.u_bencont
    REPLACE fi.u_diploma WITH ucrsvale.u_diploma
    REPLACE fi.epv WITH ucrsvale.epv
    REPLACE fi.pv WITH ucrsvale.epv*200.482 
    REPLACE fi.u_txcomp WITH ucrsvale.u_txcomp
    REPLACE fi.u_epvp WITH ucrsvale.u_epvp
    REPLACE fi.u_ettent1 WITH ucrsvale.u_ettent1
    REPLACE fi.u_ettent2 WITH ucrsvale.u_ettent2
    REPLACE fi.etiliquido WITH ucrsvale.etiliquido
    REPLACE fi.tiliquido WITH ucrsvale.etiliquido*200.482 
    REPLACE fi.lote WITH ucrsvale.lote 
    REPLACE fi.amostra WITH lcvalalterapvp
    IF ucrsvale.dev=1
       REPLACE fi.partes WITH 1
    ELSE
       IF ucrsvale.dev=2
          REPLACE fi.partes WITH 2
       ENDIF
    ENDIF
    IF ucrsvale.lancacc=.T.
       REPLACE fi.marcada WITH .T.
    ENDIF

    TEXT TO lcsql TEXTMERGE NOSHOW
			select *,cabVendasOrdem = 0, u_dcutavi = getdate()-(365*u_idutavi), CAST('' as varchar(200)) as contactoUt from B_dadosPsico(nolock) where ftstamp='<<ALLTRIM(uCrsVale.ftstamp)>>'
    ENDTEXT

    IF  .NOT. uf_gerais_actgrelha("", "dadosPsico2", lcsql)
       uf_perguntalt_chama("N�o foi possivel verificar os dados dos psicotr�picos para esta regulariza��o.", "", "OK", 16)
       RETURN .F.
    ENDIF

    
    IF RECCOUNT("dadosPsico2")>0
       SELECT * FROM dadosPsico2 INTO CURSOR dadosPsico READWRITE
       REPLACE ucrsatendcl.u_nmutavi WITH ALLTRIM(dadospsico.u_nmutavi)
       REPLACE ucrsatendcl.u_moutavi WITH ALLTRIM(dadospsico.u_moutavi)
       REPLACE ucrsatendcl.u_cputavi WITH ALLTRIM(dadospsico.u_cputavi)
       REPLACE ucrsatendcl.u_dcutavi WITH dadospsico.u_dcutavi
       REPLACE ucrsatendcl.u_ndutavi WITH ALLTRIM(dadospsico.u_ndutavi)
       REPLACE ucrsatendcl.u_ddutavi WITH dadospsico.u_ddutavi
       REPLACE ucrsatendcl.u_idutavi WITH dadospsico.u_idutavi
       REPLACE ucrsatendcl.codDocID  WITH dadospsico.codigoDocSPMS
       REPLACE ft.morada WITH ALLTRIM(dadospsico.u_moutdisp)
       REPLACE ft.codpost WITH ALLTRIM(dadospsico.u_cputdisp)
       
       atendimento.pgfdados.page1.refresh
    ENDIF
    
    **IF USED("uc_dadosPsico")
**
    **  messageb(uv_curStamp)
**
**
    **  SCAN FOR UPPER(ALLTRIM(uc_dadosPsico.ftstamp)) = UPPER(ALLTRIM(uv_curStamp))
**
    **     REPLACE ucrsatendcl.u_nmutavi WITH ALLTRIM(uc_dadosPsico.u_nmutavi)
    **     REPLACE ucrsatendcl.u_moutavi WITH ALLTRIM(uc_dadosPsico.u_moutavi)
    **     REPLACE ucrsatendcl.u_cputavi WITH ALLTRIM(uc_dadosPsico.u_cputavi)
    **     REPLACE ucrsatendcl.u_dcutavi WITH uc_dadosPsico.u_dcutavi
    **     REPLACE ucrsatendcl.u_ndutavi WITH ALLTRIM(uc_dadosPsico.u_ndutavi)
    **     REPLACE ucrsatendcl.u_ddutavi WITH uc_dadosPsico.u_ddutavi
    **     REPLACE ucrsatendcl.u_idutavi WITH uc_dadosPsico.u_idutavi   
    **     REPLACE ucrsatendcl.codDocID  WITH uc_dadosPsico.codigoDocSPMS
**
    **  ENDSCAN
**
    **ENDIF

    SELECT ucrsvale



    SELECT ft2
    REPLACE ft2.u_receita WITH ucrsvale.u_receita
    REPLACE ft2.codacesso WITH ucrsvale.codacesso
    REPLACE ft2.coddiropcao WITH ucrsvale.coddiropcao
    SELECT ucrsvale
    REPLACE ucrsvale.copied WITH .T.
    lcstamp2 = ucrsvale.ftstamp
    lcvalalterapvp = .F.
 ENDSCAN
 GOTO TOP
 LOCAL lcfipos
 lcfipos = 0
 SELECT fi
 lcfipos = RECNO()
 SELECT ucrsvale
 GOTO TOP
 SCAN FOR ucrsvale.copied=.T.
    SELECT fi
    GOTO TOP
    LOCATE FOR ALLTRIM(fi.ofistamp)==ALLTRIM(ucrsvale.fistamp)
    IF FOUND()
       IF fi.qtt<>ucrsvale.qtt
          REPLACE fi.qtt WITH ucrsvale.qtt
          IF uf_gerais_getParameter_site('ADM0000000125', 'BOOL', mySite)
	          REPLACE fi.qtdem WITH 0
	      ELSE
	      		REPLACE fi.qtdem WITH ucrsvale.qtt
	      ENDIF 
       ENDIF
       uf_atendimento_eventofi(.T.)
    ENDIF
    SELECT ucrsvale
 ENDSCAN
 GOTO TOP
 SELECT fi
 TRY
    GOTO lcfipos
 CATCH
 ENDTRY
 IF UPPER(ALLTRIM(ucrse1.pais))<>'PORTUGAL'
    SELECT ucrsatendcomp
    IF ucrsatendcomp.e_compart=.T.
       atendimento.pgfdados.page3.btn12.label1.caption = 'VALID. COMPART.'
    ELSE
       atendimento.pgfdados.page3.btn12.label1.caption = 'COMPART. MANUAL'
       atendimento.pgfdados.page3.btn12.label1.left = 8
       SELECT fi
       GOTO TOP
       SCAN
          IF  .NOT. EMPTY(fi.ref)
             uf_atendimento_adicionalinhafimancompart()
          ENDIF
       ENDSCAN
       SELECT fi
       TRY
          GOTO lcfipos
       CATCH
       ENDTRY
    ENDIF
 ENDIF
 IF lcnocl<>0
    SELECT ucrsatendcl
    IF ucrsatendcl.no<>lcnocl .OR. ucrsatendcl.estab<>lcestabcl
       uf_atendimento_selcliente(lcnocl, lcnoestab, "ATENDIMENTO")
    ENDIF
    IF  .NOT. EMPTY(lcnomecl) .AND. lcnocl==200 .AND.  .NOT. (UPPER(ALLTRIM(lcnomecl))==UPPER(ALLTRIM(ucrsatendcl.nome)))
       SELECT ucrsatendcl
       REPLACE ucrsatendcl.nome WITH ALLTRIM(lcnomecl)
       REPLACE ucrsatendcl.ncont WITH ALLTRIM(lcncont)
       SELECT ft
       REPLACE ft.nome WITH ALLTRIM(lcnomecl)
       REPLACE ft.ncont WITH ALLTRIM(lcncont)
       atendimento.refresh()
    ENDIF
    IF ALLTRIM(lcncont)<>'999999990' .AND. lcnocl==200
       SELECT ucrsatendcl
       REPLACE ucrsatendcl.ncont WITH ALLTRIM(lcncont)
       SELECT ft
       REPLACE ft.ncont WITH ALLTRIM(lcncont)
       atendimento.refresh()
    ENDIF
    SELECT ucrsatendcl
    REPLACE ucrsatendcl.u_nmutdisp WITH ucrsvale.u_nmutdisp
    REPLACE ucrsatendcl.u_moutdisp WITH ucrsvale.u_moutdisp
    REPLACE ucrsatendcl.u_cputdisp WITH ucrsvale.u_cputdisp
 ENDIF

 IF  .NOT. lbadreserva
    uf_regvendas_sair()
 ENDIF
 
 IF USED("uCrsVale")
	 SELECT uCrsVale 
	 GO TOP
	 
	 SELECT * FROM uCrsVale INTO CURSOR uCrsValeAuxMR READWRITE
 ENDIF 

 IF VARTYPE(receitaencomenda)<>'U'
    IF receitaencomenda=.T. .AND.  .NOT. EMPTY(nrreceitaenc) .AND.  .NOT. EMPTY(codreceitaenc)
    
       LOCAL lcposcabrece
       SELECT fi
       GOTO TOP
       SCAN
          IF ALLTRIM(fi.encreceita)==ALLTRIM(nrreceitaenc) .AND. EMPTY(fi.ref)
             lcposcabrece = RECNO("fi")
          ENDIF
       ENDSCAN
       SELECT ucrscabvendas
       TRY
          GOTO lcposcabrece
       CATCH
       ENDTRY
       IF UPPER(ALLTRIM(ucrse1.pais))=='PORTUGAL'
          uf_atendimento_dadosdemchama()
       ENDIF
    ENDIF
 ENDIF


 IF USED("uCrsAtendComp") AND USED("uc_dadosPsico")

   CREATE CURSOR uc_cabs(fistamp c(25), bostamp c(25), receita c(100))

   select fi
   go top

   scan for !empty(fi.tipoR)

	   select uc_cabs
	   append blank

	   replace uc_cabs.fistamp with fi.fistamp
       replace uc_cabs.receita with fi.receita

	   select fi
	   SKIP

	   IF EMPTY(fi.tipoR)
		   select uc_cabs
		   replace uc_cabs.bostamp with fi.bostamp
	   ENDIF

	   SELECT FI
	   SKIP - 1

   endscan

   select fi
   go top

   SELECT uc_cabs
   go top

   SCAN

      uv_curCab = uc_cabs.fistamp
      uv_curRec = uc_cabs.receita

      SELECT uc_dadosPsico
      GO TOP
      SCAN FOR UPPER(ALLTRIM(uc_dadosPsico.ftstamp)) = UPPER(ALLTRIM(uc_cabs.bostamp)) AND UPPER(ALLTRIM(uc_dadosPsico.u_receita)) = UPPER(ALLTRIM(uc_cabs.receita))

         SELECT UCRSATENDCOMP
         UPDATE UCRSATENDCOMP SET uCrsAtendComp.datareceita = uc_dadosPsico.u_recdata WHERE UPPER(ALLTRIM(uCrsAtendComp.fistamp)) = UPPER(ALLTRIM(uv_curCab))

         SELECT FI
         UPDATE FI SET fi.szzstamp = uc_dadosPsico.u_medico WHERE UPPER(ALLTRIM(fi.fistamp)) = UPPER(ALLTRIM(uv_curCab))

      ENDSCAN

      select uc_cabs
   ENDSCAN


	ATENDIMENTO.pgfDados.refresh()

 ENDIF

ENDFUNC
**
FUNCTION uf_regvendas_verificarefsinativas
 LOCAL lcrefs
 IF  .NOT. USED("ucrsvale2")
    RETURN .T.
 ENDIF
 IF USED("ucrsProdutosInactivosDoc")
    fecha("ucrsProdutosInactivosDoc")
 ENDIF
 TEXT TO lcsql TEXTMERGE NOSHOW
		select ref as refinativa from st (nolock) where inactivo = 1 AND site_nr = <<mysite_nr>>
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "ucrsRefsInativas", lcsql)
    uf_perguntalt_chama("N�O FOI POSSIVEL VERIFICAR A LISTA DE REFER�NCIAS INATIVAS. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
    RETURN .F.
 ENDIF
 SELECT DISTINCT refinativa FROM ucrsvale2 LEFT JOIN ucrsRefsInativas ON ucrsvale2.ref=ucrsrefsinativas.refinativa WHERE refinativa IS NOT NULL  INTO CURSOR ucrsProdutosInactivosDoc READWRITE
 IF RECCOUNT("ucrsProdutosInactivosDoc")<>0
    lcrefs = ""
    SELECT ucrsprodutosinactivosdoc
    GOTO TOP
    SCAN
       IF EMPTY(lcrefs)
          lcrefs = ALLTRIM(ucrsprodutosinactivosdoc.refinativa)
       ELSE
          lcrefs = lcrefs+", "+ALLTRIM(ucrsprodutosinactivosdoc.refinativa)
       ENDIF
    ENDSCAN
    uf_perguntalt_chama("Existem produtos inativos selecionados: Ex. "+lcrefs+CHR(13)+CHR(13)+"N�o � possivel continuar.", "OK", "", 16)
    RETURN .F.
 ENDIF
 RETURN .T.
ENDFUNC
**
PROCEDURE uf_regvendas_sair
 IF USED("uCrsVale")
	
	
 
    LOCAL lccountref
    STORE 0 TO lccountref
    SELECT ucrsvaleres
    IF VARTYPE(ucrsvale.amostra)=='N'
       SELECT ftstamp, fistamp, fdata, ousrhora, nmdoc, fno, nome, no, estab, ncont, ref, design, desconto, desc2, u_descval, u_comp, qtt, qtt2, etiliquido, etiliquido2, totalcdesc, epv, ndoc, u_lote, u_nslote, u_lote2, u_nslote2, sel, copied, u_ltstamp, u_ltstamp2, u_codigo, u_codigo2, u_diploma, dev, fact, factpago, fmarcada, eaquisicao, oeaquisicao, u_psicont, u_bencont, u_receita, u_nbenef, u_nbenef2, u_nopresc, u_entpresc, u_recdata, u_medico, u_nmutdisp, u_moutdisp, u_cputdisp, u_nmutavi, u_moutavi, u_cputavi, u_ndutavi, u_ddutavi, u_idutavi, u_txcomp, u_epvp, u_ettent1, u_ettent2, docinvert, u_backoff, lancacc, u_design, u_abrev, u_abrev2, u_epref, tipodoc, tabiva, iva, opcao, token, codacesso, coddiropcao, id_validacaodem, amostra, c2codpost, lote, datavalidade FROM uCrsValeRes UNION ALL SELECT ftstamp, fistamp, fdata, ousrhora, nmdoc, fno, nome, no, estab, ncont, ref, design, desconto, desc2, u_descval, u_comp, qtt, qtt2, etiliquido, etiliquido2, totalcdesc, epv, ndoc, u_lote, u_nslote, u_lote2, u_nslote2, sel, copied, u_ltstamp, u_ltstamp2, u_codigo, u_codigo2, u_diploma, dev, fact, factpago, fmarcada, eaquisicao, oeaquisicao, u_psicont, u_bencont, u_receita, u_nbenef, u_nbenef2, u_nopresc, u_entpresc, u_recdata, u_medico, u_nmutdisp, u_moutdisp, u_cputdisp, u_nmutavi, u_moutavi, u_cputavi, u_ndutavi, u_ddutavi, u_idutavi, u_txcomp, u_epvp, u_ettent1, u_ettent2, docinvert, u_backoff, lancacc, u_design, u_abrev, u_abrev2, u_epref, tipodoc, tabiva, iva, opcao, token, codacesso, coddiropcao, id_validacaodem, IIF(ucrsvale.amostra=0, .F., .T.) AS amostra, c2codpost, lote, datavalidade FROM uCrsVale INTO CURSOR ucrsValeTemp READWRITE
    ELSE
       SELECT ftstamp, fistamp, fdata, ousrhora, nmdoc, fno, nome, no, estab, ncont, ref, design, desconto, desc2, u_descval, u_comp, qtt, qtt2, etiliquido, etiliquido2, totalcdesc, epv, ndoc, u_lote, u_nslote, u_lote2, u_nslote2, sel, copied, u_ltstamp, u_ltstamp2, u_codigo, u_codigo2, u_diploma, dev, fact, factpago, fmarcada, eaquisicao, oeaquisicao, u_psicont, u_bencont, u_receita, u_nbenef, u_nbenef2, u_nopresc, u_entpresc, u_recdata, u_medico, u_nmutdisp, u_moutdisp, u_cputdisp, u_nmutavi, u_moutavi, u_cputavi, u_ndutavi, u_ddutavi, u_idutavi, u_txcomp, u_epvp, u_ettent1, u_ettent2, docinvert, u_backoff, lancacc, u_design, u_abrev, u_abrev2, u_epref, tipodoc, tabiva, iva, opcao, token, codacesso, coddiropcao, id_validacaodem, amostra, c2codpost, lote,datavalidade FROM uCrsValeRes UNION ALL SELECT ftstamp, fistamp, fdata, ousrhora, nmdoc, fno, nome, no, estab, ncont, ref, design, desconto, desc2, u_descval, u_comp, qtt, qtt2, etiliquido, etiliquido2, totalcdesc, epv, ndoc, u_lote, u_nslote, u_lote2, u_nslote2, sel, copied, u_ltstamp, u_ltstamp2, u_codigo, u_codigo2, u_diploma, dev, fact, factpago, fmarcada, eaquisicao, oeaquisicao, u_psicont, u_bencont, u_receita, u_nbenef, u_nbenef2, u_nopresc, u_entpresc, u_recdata, u_medico, u_nmutdisp, u_moutdisp, u_cputdisp, u_nmutavi, u_moutavi, u_cputavi, u_ndutavi, u_ddutavi, u_idutavi, u_txcomp, u_epvp, u_ettent1, u_ettent2, docinvert, u_backoff, lancacc, u_design, u_abrev, u_abrev2, u_epref, tipodoc, tabiva, iva, opcao, token, codacesso, coddiropcao, id_validacaodem, amostra, c2codpost, lote, datavalidade FROM uCrsVale INTO CURSOR ucrsValeTemp READWRITE
    ENDIF
    SELECT ucrsvaleres
    DELETE ALL
    fecha("uCrsVale")
    IF USED("ucrsvalecurini")
    
       SELECT * FROM ucrsValeTemp INTO CURSOR ucrsVale1 READWRITE
       SELECT * FROM ucrsvalecurini UNION ALL SELECT * FROM ucrsVale1  WHERE ucrsVale1.fistamp NOT in (SELECT fistamp FROM ucrsvalecurini) INTO CURSOR ucrsVale READWRITE
       
       SELECT ucrsVale 
    ELSE

       SELECT * FROM ucrsValeTemp INTO CURSOR ucrsVale READWRITE
    ENDIF
    SELECT ucrsvale
    CALCULATE SUM(ucrsvale.qtt) TO lccountref 
    IF lccountref==0
       fecha("uCrsVale")
    ELSE
       SELECT ucrsvale
       GOTO TOP
    ENDIF
    

 ENDIF
 IF USED("uCrsRegVendas")
    fecha("uCrsRegVendas")
 ENDIF
 IF USED("uCrsVale2")
    fecha("uCrsVale2")
 ENDIF
 IF USED("uCrsLancaCc")
    fecha("uCrsLancaCc")
 ENDIF
 IF USED("ucrsVale1 ")
    fecha("ucrsVale1 ")
 ENDIF
 IF TYPE('FACTURACAO')=="U"
    IF USED("uCrsPagCentral")
       fecha("uCrsPagCentral")
    ENDIF
 ENDIF
 IF USED("uCrsPca")
    fecha("uCrsPca")
 ENDIF
 IF USED("uCrsAtendPendPag")
    fecha("uCrsAtendPendPag")
 ENDIF
 IF USED("uCrsAtendPendPag2")
    fecha("uCrsAtendPendPag2")
 ENDIF



 IF USED("dadosPsico2") .AND. RECCOUNT("dadosPsico2")>0
 
    SELECT * FROM dadosPsico2 INTO CURSOR dadosPsico READWRITE
    REPLACE ucrsatendcl.u_nmutavi WITH ALLTRIM(dadospsico.u_nmutavi)
    REPLACE ucrsatendcl.u_moutavi WITH ALLTRIM(dadospsico.u_moutavi)
    REPLACE ucrsatendcl.u_cputavi WITH ALLTRIM(dadospsico.u_cputavi)
    REPLACE ucrsatendcl.u_dcutavi WITH dadospsico.u_dcutavi
    REPLACE ucrsatendcl.u_ndutavi WITH ALLTRIM(dadospsico.u_ndutavi)
    REPLACE ucrsatendcl.u_ddutavi WITH dadospsico.u_ddutavi
    REPLACE ucrsatendcl.u_idutavi WITH dadospsico.u_idutavi
    REPLACE ucrsatendcl.codDocID  WITH dadospsico.codigoDocSPMS
    REPLACE ft.morada WITH ALLTRIM(dadospsico.u_moutdisp)
    REPLACE ft.codpost WITH ALLTRIM(dadospsico.u_cputdisp)
    SELECT dadospsico
    REPLACE dadospsico.cabvendasordem WITH 100000
    atendimento.pgfdados.page1.refresh
 ENDIF
 fecha("dadosPsico2")
 IF USED("fi") .AND. TYPE("ATENDIMENTO")<>"U"
    SELECT fi
    GOTO TOP
    uf_eventoactvalores()
 ENDIF
 regvendas.hide
 regvendas.release
 RELEASE regvendas
ENDPROC
**
FUNCTION uf_regvendas_validasemostraalertaquerydemorada
 LPARAMETERS tcdatainit, tcdatafim, tcdiasdiff, tcnratend, tcnrvenda, tcnrcli
 IF ( .NOT. EMPTY(tcnratend) .OR. tcnrvenda<>"-1" .OR. tcnrcli<>"-1")
    RETURN .F.
 ELSE
    IF uf_gerais_datediff(tcdatainit, tcdatafim)>tcdiasdiff
       RETURN .T.
    ENDIF
 ENDIF
 RETURN .F.
ENDFUNC
**
PROCEDURE uf_regvendas_pesquisarregcreditos
 LOCAL lcproduto, lcnvenda, lccliente, lcno, lcestab, lcnatend
 STORE '' TO lcproduto, lccliente, lcnatend
 STORE -1 TO lcnvenda, lcno, lcestab
  
 IF  .NOT. EMPTY(ALLTRIM(regvendas.txtproduto.value))
    lcproduto = ALLTRIM(regvendas.txtproduto.value)
 ELSE
    lcproduto = ''
 ENDIF
 IF  .NOT. EMPTY(astr(regvendas.txtnvenda.value))
    lcnvenda = ALLTRIM(STRTRAN(regvendas.txtnvenda.value, ' ', ''))
 ELSE
    lcnvenda = '-1'
 ENDIF
 IF regvendas.pgfreg.page2.chkporutente.tag="true"
    lccliente = ''
 ELSE
    IF  .NOT. EMPTY(ALLTRIM(regvendas.txtcliente.value))
       lccliente = ALLTRIM(regvendas.txtcliente.value)
    ELSE
       lccliente = ''
    ENDIF
 ENDIF
 IF regvendas.pgfreg.page2.chkporutente.tag="true"
    lcno = '-1'
 ELSE
    IF  .NOT. EMPTY(astr(regvendas.txtno.value))
       lcno = astr(regvendas.txtno.value)
    ELSE
       lcno = '-1'
    ENDIF
 ENDIF
 

 IF regvendas.pgfreg.page2.chkporutente.tag="true"
    lcestab = '-1'
 ELSE
    IF  .NOT. EMPTY(astr(regvendas.txtestab.value))
       lcestab = astr(regvendas.txtestab.value)
    ELSE
       lcestab = '-1'
    ENDIF
 ENDIF
 IF  .NOT. EMPTY(astr(regvendas.txtnatend.value))
    lcnatend = astr(regvendas.txtnatend.value)
 ELSE
    lcnatend = ''
 ENDIF
 regua(0, 1, "A pesquisar...", .T.)
 LOCAL lcoutrasemp
 IF regvendas.pgfreg.page2.chkoutraslojas.tag="false"
    lcoutrasemp = 0
 ELSE
    lcoutrasemp = 1
 ENDIF
 
 
 
 IF regvendas.pgfreg.page2.chkporproduto.tag=="true"
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_touch_regularizarCreditos '<<lcProduto>>', <<lcNVenda>>, '<<lcCliente>>', <<lcNo>>, <<lcEstab>>, '<<uf_gerais_getdate(REGVENDAS.txtDataIni.value, "SQL")>>', '<<uf_gerais_getdate(REGVENDAS.txtDataFim.value, "SQL")>>', '<<lcNAtend>>', <<ch_userno>>, '<<Alltrim(ch_grupo)>>', '<<mySite>>',0,0,<<lcOutrasEmp>>
    ENDTEXT
    
    uf_gerais_actgrelha("REGVENDAS.pgfReg.page2.grdRegCredito", "uCrsRegCreditos", lcsql)
    SELECT ucrsregcreditos
    GOTO TOP
    SCAN
       IF  .NOT. (ucrsregcreditos.qtt==ucrsregcreditos.qtt2)
          REPLACE ucrsregcreditos.etiliquido WITH ucrsregcreditos.qtt*ucrsregcreditos.etiliquido2/ucrsregcreditos.qtt2
       ENDIF
       REPLACE ucrsregcreditos.etiliquido WITH ROUND(((ucrsregcreditos.qtt-ROUND(ucrsregcreditos.eaquisicao, 0))*ucrsregcreditos.etiliquido/ucrsregcreditos.qtt), 2)
       SELECT ucrsregcreditos
    ENDSCAN
    LOCAL lcfno3, lcfno4, lcdocinvert2
    STORE 0 TO lcfno3, lcfno4
    SELECT ucrsregcreditos
    GOTO TOP
    lcfno3 = ucrsregcreditos.fno
    SCAN
       lcfno4 = ucrsregcreditos.fno
       IF lcfno3<>lcfno4
          IF lcdocinvert2
             lcdocinvert2 = .F.
          ELSE
             lcdocinvert2 = .T.
          ENDIF
       ENDIF
       REPLACE ucrsregcreditos.docinvert WITH lcdocinvert2
       lcfno3 = ucrsregcreditos.fno
    ENDSCAN
    SELECT ucrsregcreditos
    GOTO TOP
 ELSE
    LOCAL lcoutrasemp
    IF regvendas.pgfreg.page2.chkoutraslojas.tag="false"
       lcoutrasemp = 0
    ELSE
       lcoutrasemp = 1
    ENDIF
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_touch_regularizarCreditosPorValor <<lcNVenda>>, '<<lcCliente>>', <<lcNo>>, <<lcEstab>>, '<<uf_gerais_getdate(REGVENDAS.txtDataIni.value, "SQL")>>', '<<uf_gerais_getdate(REGVENDAS.txtDataFim.value, "SQL")>>', '<<lcNAtend>>', <<ch_userno>>, '<<Alltrim(ch_grupo)>>', '<<mySite>>', <<lcOutrasEmp>>
    ENDTEXT
    
    uf_gerais_actgrelha("REGVENDAS.pgfReg.page2.grdRegCreditoValor", "uCrsRegCreditosValor", lcsql)
 ENDIF
 
 uf_regvendas_resetvaloresrvcred(.T.)
 regvendas.menu1.seltodos.config("Sel. Todos", mypath+"\imagens\icons\unchecked_w.png", "uf_regvendas_seltodosRvReg with .t.", "T")
 

 uf_regvendas_filtrarvdsusp()
 uf_regvendas_filtrarvdnaosusp()
 uf_regvendas_configgrdregcreditos()

 regua(2)
ENDPROC
**
FUNCTION uf_regvendas_seltodosrvreg
 LPARAMETERS lcsel
 
 
 IF  .NOT. USED("uCrsRegCreditos") .OR.  .NOT. USED("uCrsRegCreditosValor")
    RETURN .F.
 ENDIF
 LOCAL lctotal, lcvalreg
 STORE 0 TO lctotal
 STORE .F. TO lcvalreg
 IF lcsel
    IF regvendas.pgfreg.page2.chkporproduto.tag=="true"
       regvendas.pgfreg.page2.grdregcredito.columns(2).setfocus
       SELECT ucrsregcreditos
       GOTO TOP
       SCAN
          IF USED("uCrsVale")
             SELECT ucrsvale
             GOTO TOP
             SCAN FOR ALLTRIM(ucrsvale.fistamp)==ALLTRIM(ucrsregcreditos.fistamp)
                lcvalreg = .T.
             ENDSCAN
          ENDIF
          IF  .NOT. lcvalreg
             SELECT ucrsregcreditos
             REPLACE ucrsregcreditos.sel WITH .T.
             lctotal = lctotal+ucrsregcreditos.etiliquido
          ENDIF
          lcvalreg = .F.
          SELECT ucrsregcreditos
       ENDSCAN
       GOTO TOP
    ELSE
       regvendas.pgfreg.page2.grdregcreditovalor.columns(2).setfocus
       SELECT ucrsregcreditosvalor
       GOTO TOP
       SCAN
          IF USED("uCrsVale")
             SELECT ucrsvale
             GOTO TOP
             LOCATE FOR ALLTRIM(ucrsvale.ftstamp)==ALLTRIM(ucrsregcreditosvalor.ftstamp)
             IF FOUND()
                lcvalreg = .T.
             ENDIF
          ENDIF
          IF  .NOT. lcvalreg
             SELECT ucrsregcreditosvalor
             REPLACE ucrsregcreditosvalor.sel WITH .T.
             lctotal = lctotal+ucrsregcreditosvalor.regularizar
          ENDIF
          lcvalreg = .F.
          SELECT ucrsregcreditosvalor
       ENDSCAN
       GOTO TOP
    ENDIF
    regvendas.menu1.seltodos.config("Des. Todos", mypath+"\imagens\icons\checked_w.png", "uf_regvendas_seltodosRvReg with .f.", "T")
    regvendas.ctnpagamento.txtvalatend.value = lctotal
    regvendas.ctnpagamento.txtdinheiro.value = ''
    regvendas.ctnpagamento.txtmb.value = ''
    regvendas.ctnpagamento.txtvisa.value = ''
    regvendas.ctnpagamento.txtcheque.value = ''
    regvendas.ctnpagamento.txtmodpag3.value = ''
    regvendas.ctnpagamento.txtmodpag4.value = ''
    regvendas.ctnpagamento.txtmodpag5.value = ''
    regvendas.ctnpagamento.txtmodpag6.value = ''
    regvendas.ctnpagamento.txtvalrec.value = ''
    regvendas.pgfreg.page2.txtdescperc.value = ''
    regvendas.pgfreg.page2.txtdesceuro.value = ''
    regvendas.ctnpagamento.txttroco.value = 0
 ELSE
    IF regvendas.pgfreg.page2.chkporproduto.tag=="true"
       SELECT ucrsregcreditos
       GOTO TOP
       SCAN
          REPLACE ucrsregcreditos.sel WITH .F.
       ENDSCAN
       GOTO TOP
    ELSE
       SELECT ucrsregcreditosvalor
       GOTO TOP
       SCAN
          REPLACE ucrsregcreditosvalor.sel WITH .F.
       ENDSCAN
       GOTO TOP
    ENDIF
    regvendas.menu1.seltodos.config("Sel. Todos", mypath+"\imagens\icons\unchecked_w.png", "uf_regvendas_seltodosRvReg with .t.", "T")
    regvendas.ctnpagamento.txtvalatend.value = 0
    regvendas.ctnpagamento.txtdinheiro.value = ''
    regvendas.ctnpagamento.txtmb.value = ''
    regvendas.ctnpagamento.txtvisa.value = ''
    regvendas.ctnpagamento.txtcheque.value = ''
    regvendas.ctnpagamento.txtmodpag3.value = ''
    regvendas.ctnpagamento.txtmodpag4.value = ''
    regvendas.ctnpagamento.txtmodpag5.value = ''
    regvendas.ctnpagamento.txtmodpag6.value = ''
    regvendas.ctnpagamento.txtvalrec.value = ''
    regvendas.pgfreg.page2.txtdescperc.value = ''
    regvendas.pgfreg.page2.txtdesceuro.value = ''
    regvendas.ctnpagamento.txttroco.value = 0
 ENDIF
 
  uf_regvendas_controlaobjpagamento()
 
ENDFUNC
**
PROCEDURE uf_regvendas_configgrdregcreditos
 LOCAL ti, i
 STORE 0 TO ti, i
 ti = regvendas.pgfreg.page2.grdregcredito.columncount
 FOR i = 1 TO ti
    regvendas.pgfreg.page2.grdregcredito.columns(i).dynamicbackcolor = "IIF(uCrsRegCreditos.docinvert, rgb[255,255,255], rgb[240,240,240])"
 ENDFOR
 regvendas.pgfreg.page2.grdregcredito.refresh
ENDPROC
**
PROCEDURE uf_regvendas_actvaloresrvcred
 IF OCCURS(".", regvendas.ctnpagamento.txtvalrec.value)>0
    LOCAL lcdec
    lcdec = STREXTRACT(regvendas.ctnpagamento.txtvalrec.value, ".")
    IF  .NOT. EMPTY(lcdec)
       IF LEN(lcdec)=1
          regvendas.ctnpagamento.txtvalrec.value = astr((VAL(regvendas.ctnpagamento.txtdinheiro.value)+VAL(regvendas.ctnpagamento.txtmb.value)+VAL(regvendas.ctnpagamento.txtvisa.value)+VAL(regvendas.ctnpagamento.txtcheque.value)+VAL(regvendas.ctnpagamento.txtmodpag3.value)+VAL(regvendas.ctnpagamento.txtmodpag4.value)+VAL(regvendas.ctnpagamento.txtmodpag5.value)+VAL(regvendas.ctnpagamento.txtmodpag6.value)), 15, 2)
       ELSE
          IF LEN(lcdec)=2
             regvendas.ctnpagamento.txtvalrec.value = astr((VAL(regvendas.ctnpagamento.txtdinheiro.value)+VAL(regvendas.ctnpagamento.txtmb.value)+VAL(regvendas.ctnpagamento.txtvisa.value)+VAL(regvendas.ctnpagamento.txtcheque.value)+VAL(regvendas.ctnpagamento.txtmodpag3.value)+VAL(regvendas.ctnpagamento.txtmodpag4.value)+VAL(regvendas.ctnpagamento.txtmodpag5.value)+VAL(regvendas.ctnpagamento.txtmodpag6.value)), 15, 2)
          ELSE
             regvendas.ctnpagamento.txtvalrec.value = astr((VAL(regvendas.ctnpagamento.txtdinheiro.value)+VAL(regvendas.ctnpagamento.txtmb.value)+VAL(regvendas.ctnpagamento.txtvisa.value)+VAL(regvendas.ctnpagamento.txtcheque.value)+VAL(regvendas.ctnpagamento.txtmodpag3.value)+VAL(regvendas.ctnpagamento.txtmodpag4.value)+VAL(regvendas.ctnpagamento.txtmodpag5.value)+VAL(regvendas.ctnpagamento.txtmodpag6.value)), 15, 2)
          ENDIF
       ENDIF
    ENDIF
 ELSE
    regvendas.ctnpagamento.txtvalrec.value = astr((VAL(regvendas.ctnpagamento.txtdinheiro.value)+VAL(regvendas.ctnpagamento.txtmb.value)+VAL(regvendas.ctnpagamento.txtvisa.value)+VAL(regvendas.ctnpagamento.txtcheque.value)+VAL(regvendas.ctnpagamento.txtmodpag3.value)+VAL(regvendas.ctnpagamento.txtmodpag4.value)+VAL(regvendas.ctnpagamento.txtmodpag5.value)+VAL(regvendas.ctnpagamento.txtmodpag6.value)), 15, 2)
 ENDIF
 IF VAL(regvendas.ctnpagamento.txtvalrec.value)=0
    regvendas.ctnpagamento.txtvalrec.value = ""
 ENDIF
 uf_regvendas_corrigircasasdecrvcred()
 uf_regvendas_calculartrocorvcred()
ENDPROC
**
PROCEDURE uf_regvendas_corrigircasasdecrvcred
 LOCAL lcdec
 lcdec = STREXTRACT(regvendas.ctnpagamento.txtvalrec.value, ".")
 IF LEN(lcdec)>2
    regvendas.ctnpagamento.txtvalrec.value = astr(VAL(regvendas.ctnpagamento.txtvalrec.value), 15, 2)
 ENDIF
 lcdec = STREXTRACT(regvendas.ctnpagamento.txtdinheiro.value, ".")
 IF LEN(lcdec)>2
    regvendas.ctnpagamento.txtdinheiro.value = astr(VAL(regvendas.ctnpagamento.txtdinheiro.value), 15, 2)
 ENDIF
 lcdec = STREXTRACT(regvendas.ctnpagamento.txtmb.value, ".")
 IF LEN(lcdec)>2
    regvendas.ctnpagamento.txtmb.value = astr(VAL(regvendas.ctnpagamento.txtmb.value), 15, 2)
 ENDIF
 lcdec = STREXTRACT(regvendas.ctnpagamento.txtvisa.value, ".")
 IF LEN(lcdec)>2
    regvendas.ctnpagamento.txtvisa.value = astr(VAL(regvendas.ctnpagamento.txtvisa.value), 15, 2)
 ENDIF
 lcdec = STREXTRACT(regvendas.ctnpagamento.txtcheque.value, ".")
 IF LEN(lcdec)>2
    regvendas.ctnpagamento.txtcheque.value = astr(VAL(regvendas.ctnpagamento.txtcheque.value), 15, 2)
 ENDIF
 lcdec = STREXTRACT(regvendas.ctnpagamento.txtmodpag3.value, ".")
 IF LEN(lcdec)>2
    regvendas.ctnpagamento.txtmodpag3.value = astr(VAL(regvendas.ctnpagamento.txtmodpag3.value), 15, 2)
 ENDIF
 lcdec = STREXTRACT(regvendas.ctnpagamento.txtmodpag4.value, ".")
 IF LEN(lcdec)>2
    regvendas.ctnpagamento.txtmodpag4.value = astr(VAL(regvendas.ctnpagamento.txtmodpag4.value), 15, 2)
 ENDIF
 lcdec = STREXTRACT(regvendas.ctnpagamento.txtmodpag5.value, ".")
 IF LEN(lcdec)>2
    regvendas.ctnpagamento.txtmodpag5.value = astr(VAL(regvendas.ctnpagamento.txtmodpag5.value), 15, 2)
 ENDIF
 lcdec = STREXTRACT(regvendas.ctnpagamento.txtmodpag6.value, ".")
 IF LEN(lcdec)>2
    regvendas.ctnpagamento.txtmodpag6.value = astr(VAL(regvendas.ctnpagamento.txtmodpag6.value), 15, 2)
 ENDIF
 lcdec = STREXTRACT(regvendas.pgfreg.page2.txtdescperc.value, ".")
 IF LEN(lcdec)>2
    regvendas.pgfreg.page2.txtdescperc.value = astr(VAL(regvendas.pgfreg.page2.txtdescperc.value), 15, 2)
 ENDIF
 lcdec = STREXTRACT(regvendas.pgfreg.page2.txtdesceuro.value, ".")
 IF LEN(lcdec)>2
    regvendas.pgfreg.page2.txtdesceuro.value = astr(VAL(regvendas.pgfreg.page2.txtdesceuro.value), 15, 2)
 ENDIF
ENDPROC
**
PROCEDURE uf_regvendas_actvaloresdescvrvcred
 regvendas.runpctxtdesceuro = 0
 regvendas.runpctxtdescperc = 0
 uf_regvendas_corrigircasasdecrvcred()
 uf_regvendas_calctotalrecrvcred()
 IF EMPTY(regvendas.pgfreg.page2.txtdesceuro.value)
    regvendas.pgfreg.page2.txtdescperc.value = ''
 ELSE
    regvendas.pgfreg.page2.txtdescperc.value = astr(ROUND(VAL(regvendas.pgfreg.page2.txtdesceuro.value)*100/regvendas.ctnpagamento.txtvalatend.value, 2), 6, 2)
    IF regvendas.ctnpagamento.txtvalatend.value<VAL(regvendas.pgfreg.page2.txtdesceuro.value)
       regvendas.pgfreg.page2.txtdesceuro.value = ''
       regvendas.pgfreg.page2.txtdescperc.value = ''
    ELSE
       regvendas.ctnpagamento.txtvalatend.value = regvendas.ctnpagamento.txtvalatend.value-VAL(regvendas.pgfreg.page2.txtdesceuro.value)
    ENDIF
 ENDIF
 uf_regvendas_resetvaloresrvcred(.F., .T.)
 regvendas.runpctxtdesceuro = 1
 regvendas.runpctxtdescperc = 1
ENDPROC
**
PROCEDURE uf_regvendas_actvaloresdescprvcred
 regvendas.runpctxtdescperc = 0
 regvendas.runpctxtdesceuro = 0
 uf_regvendas_corrigircasasdecrvcred()
 uf_regvendas_calctotalrecrvcred()
 IF EMPTY(regvendas.pgfreg.page2.txtdescperc.value)
    regvendas.pgfreg.page2.txtdesceuro.value = ''
 ELSE
    LOCAL lcvalor
    lcvalor = VAL(regvendas.pgfreg.page2.txtdescperc.value)/100
    lcvalor = ROUND(regvendas.ctnpagamento.txtvalatend.value*lcvalor, 2)
    regvendas.pgfreg.page2.txtdesceuro.value = astr(lcvalor, 8, 2)
    IF regvendas.ctnpagamento.txtvalatend.value<VAL(regvendas.pgfreg.page2.txtdesceuro.value)
       regvendas.pgfreg.page2.txtdesceuro.value = ''
       regvendas.pgfreg.page2.txtdescperc.value = ''
    ELSE
       regvendas.ctnpagamento.txtvalatend.value = regvendas.ctnpagamento.txtvalatend.value-VAL(regvendas.pgfreg.page2.txtdesceuro.value)
    ENDIF
 ENDIF
 uf_regvendas_resetvaloresrvcred(.F., .T.)
 regvendas.runpctxtdescperc = 1
 regvendas.runpctxtdesceuro = 1
ENDPROC
**
PROCEDURE uf_regvendas_resetvaloresrvcred
 LPARAMETERS tcbool, tcbool2
 regvendas.ctnpagamento.txtdinheiro.value = ''
 regvendas.ctnpagamento.txtmb.value = ''
 regvendas.ctnpagamento.txtvisa.value = ''
 regvendas.ctnpagamento.txtcheque.value = ''
 regvendas.ctnpagamento.txtmodpag3.value = ''
 regvendas.ctnpagamento.txtmodpag4.value = ''
 regvendas.ctnpagamento.txtmodpag5.value = ''
 regvendas.ctnpagamento.txtmodpag6.value = ''
 regvendas.ctnpagamento.txtvalrec.value = ''
 IF  .NOT. tcbool2
    regvendas.pgfreg.page2.txtdescperc.value = ''
    regvendas.pgfreg.page2.txtdesceuro.value = ''
 ENDIF
 IF tcbool
    regvendas.ctnpagamento.txtvalatend.value = 0
 ENDIF
 regvendas.ctnpagamento.txttroco.value = 0
 regvendas.ctnpagamento.txtvalatend.value = regvendas.ctnpagamento.txtvalatend.value
ENDPROC
**
FUNCTION uf_regvendas_calctotalrecrvcred
 IF  .NOT. USED("uCrsRegCreditos") .OR.  .NOT. USED("uCrsRegCreditosValor")
    RETURN .F.
 ENDIF
 LOCAL lcpos, lctotal
 STORE 0 TO lcpos, lctotal
 IF regvendas.pgfreg.page2.chkporproduto.tag=="true"
    SELECT ucrsregcreditos
    lcpos = RECNO("uCrsRegCreditos")
    CALCULATE SUM(ucrsregcreditos.etiliquido) TO lctotal FOR ucrsregcreditos.sel==.T.
    SELECT ucrsregcreditos
    TRY
       GOTO lcpos
    CATCH
       GOTO TOP
    ENDTRY
 ELSE
    IF TYPE("UtilPlafond")<>"U"
       IF utilplafond=.T.
          IF ucrsregcreditosvalor.valornreg-ucrsregcreditosvalor.regularizar>plafonddisput
             uf_perguntalt_chama("O VALOR EM D�VIDA ULTRAPASSA O PLAFOND DISPON�VEL. POR FAVOR RETIFIQUE.", "OK", "", 16)
             REPLACE ucrsregcreditosvalor.regularizar WITH ucrsregcreditosvalor.valornreg-plafonddisput
             regvendas.ctnpagamento.txtvalatend.value = ucrsregcreditosvalor.valornreg-plafonddisput
             REPLACE ucrsregcreditosvalor.sel WITH .T.
             RETURN .F.
          ENDIF
       ENDIF
    ENDIF
    SELECT ucrsregcreditosvalor
    lcpos = RECNO("uCrsRegCreditosValor")
    CALCULATE SUM(ucrsregcreditosvalor.regularizar) TO lctotal FOR ucrsregcreditosvalor.sel==.T.
    SELECT ucrsregcreditosvalor
    TRY
       GOTO lcpos
    CATCH
       GOTO TOP
    ENDTRY
 ENDIF
 regvendas.ctnpagamento.txtvalatend.value = ROUND(lctotal, 2)
ENDFUNC
**
FUNCTION uf_regvendas_selgrdregcredito


 IF  .NOT. USED("uCrsRegCreditos")
    RETURN .F.
 ENDIF
 IF uf_gerais_getparameter_site('ADM0000000088', 'BOOL', mysite)
    lcsql = ""
    TEXT TO lcsql TEXTMERGE NOSHOW
			select count(bistamp) as contaenc from bi (nolock) 
			where bistamp in(select bistamp from fi (nolock) where ftstamp='<<ALLTRIM(uCrsRegCreditos.ftstamp)>>') 
				and nmdos='Encomenda de Cliente'
    ENDTEXT
    uf_gerais_actgrelha("", "ucrsOrigemEnc", lcsql)
    IF ucrsorigemenc.contaenc=0
       SELECT ucrsregcreditosvalor
       lcsql = ""
       TEXT TO lcsql TEXTMERGE NOSHOW
				select Processed from Rep_Control (nolock) where Identifier='<<ALLTRIM(uCrsRegCreditos.ftstamp)>>' and Operation='I'
       ENDTEXT
       uf_gerais_actgrelha("", "ucrsProcessed", lcsql)
       SELECT ucrsprocessed
       IF ucrsprocessed.processed=.T.
          uf_perguntalt_chama("N�o fazer um recebimento de um documento que j� foi enviado para a BD central.", "", "OK", 16)
          SELECT ucrsregcreditos
          REPLACE ucrsregcreditos.sel WITH .F.
          RETURN .F.
       ENDIF
       fecha("ucrsProcessed")
    ENDIF
    fecha("ucrsOrigemEnc")
 ENDIF
 SELECT ucrsregcreditos
 IF USED("uCrsVale")
    SELECT ucrsvale
    GOTO TOP
    SCAN FOR ALLTRIM(ucrsvale.fistamp)==ALLTRIM(ucrsregcreditos.fistamp)
       regvendas.pgfreg.page2.grdregcredito.sel.check1.value = .F.
       uf_perguntalt_chama("ESTE PRODUTO J� SE ENCONTRA PARA REGULARIZA��O NO ECR� DE ATENDIMENTO. N�O PODE REGULARIZAR O MESMO PRODUTO NESTE PAINEL. POR FAVOR VERIFIQUE.", "OK", "", 32)
       RETURN .F.
    ENDSCAN
 ENDIF
 SELECT ucrsregcreditos
 IF  .NOT. (ucrsregcreditos.etiliquido==0)
    IF ucrsregcreditos.sel==.T.
       regvendas.ctnpagamento.txtvalatend.value = regvendas.ctnpagamento.txtvalatend.value+ucrsregcreditos.etiliquido
    ELSE
       regvendas.ctnpagamento.txtvalatend.value = regvendas.ctnpagamento.txtvalatend.value-ucrsregcreditos.etiliquido
    ENDIF
    uf_regvendas_resetvaloresrvcred()
 ENDIF

 uf_regvendas_controlaobjpagamento()
ENDFUNC
**
FUNCTION uf_regvendas_selgrdregcreditovalor
 IF  .NOT. USED("uCrsRegCreditosValor")
    RETURN .F.
 ENDIF
 IF uf_gerais_getparameter_site('ADM0000000088', 'BOOL', mysite)
    lcsql = ""
    TEXT TO lcsql TEXTMERGE NOSHOW
			select count(bistamp) as contaenc from bi (nolock) 
			where bistamp in(select bistamp from fi (nolock) where ftstamp='<<ALLTRIM(uCrsRegCreditosValor.ftstamp)>>') 
				and nmdos='Encomenda de Cliente'
    ENDTEXT
    uf_gerais_actgrelha("", "ucrsOrigemEnc", lcsql)
    IF ucrsorigemenc.contaenc=0
       SELECT ucrsregcreditosvalor
       lcsql = ""
       TEXT TO lcsql TEXTMERGE NOSHOW
				select Processed from Rep_Control (nolock) where Identifier='<<ALLTRIM(uCrsRegCreditosValor.ftstamp)>>' and Operation='I'
       ENDTEXT
       uf_gerais_actgrelha("", "ucrsProcessed", lcsql)
       SELECT ucrsprocessed
       IF ucrsprocessed.processed=.T.
          uf_perguntalt_chama("N�o pode fazer um recebimento de um documento que j� foi enviado para a BD central.", "", "OK", 16)
          SELECT ucrsregcreditosvalor
          REPLACE ucrsregcreditosvalor.sel WITH .F.
          RETURN .F.
       ENDIF
       fecha("ucrsProcessed")
    ENDIF
    fecha("ucrsOrigemEnc")
    
    TEXT TO lcsql TEXTMERGE NOSHOW
			select sum(ettdeb) as valcartao from bi (nolock) 
			where bistamp in(select bistamp from fi (nolock) where ftstamp='<<ALLTRIM(uCrsRegCreditosValor.ftstamp)>>') 
				and nmdos='Encomenda de Cliente'
				and ref='V000001'
    ENDTEXT
    uf_gerais_actgrelha("", "ucrsValcartao", lcsql)
	sele ucrsValcartao
	IF ucrsValcartao.valcartao<0
		regvendas.ctnpagamento.txtvalatend.value = uCrsRegCreditosValor.regularizar
		regvendas.ctnpagamento.txtcheque.value = astr(ROUND(ucrsValcartao.valcartao*(-1),2),15,2)
		uf_regvendas_actValoresRvCred()
	ENDIF 

 ENDIF
 SELECT ucrsregcreditosvalor
 IF USED("uCrsVale")
    SELECT ucrsvale
    LOCATE FOR ALLTRIM(ucrsvale.ftstamp)==ALLTRIM(ucrsregcreditosvalor.ftstamp)
    IF FOUND()
       regvendas.pgfreg.page2.grdregcreditovalor.sel.check1.value = .F.
       uf_perguntalt_chama("ESTE DOCUMENTO J� SE ENCONTRA PARA REGULARIZA��O NO ECR� DE ATENDIMENTO. N�O PODE REGULARIZAR O MESMO NESTE PAINEL. POR FAVOR VERIFIQUE.", "OK", "", 32)
       RETURN .F.
    ENDIF
 ENDIF
 SELECT ucrsregcreditosvalor
 IF  .NOT. (ucrsregcreditosvalor.regularizar==0) AND len(ALLTRIM(regvendas.ctnpagamento.txtcheque.value))=0
    IF ucrsregcreditosvalor.sel==.T.
       regvendas.ctnpagamento.txtvalatend.value = regvendas.ctnpagamento.txtvalatend.value+ucrsregcreditosvalor.regularizar
    ELSE
       regvendas.ctnpagamento.txtvalatend.value = regvendas.ctnpagamento.txtvalatend.value-ucrsregcreditosvalor.regularizar
    ENDIF
    uf_regvendas_resetvaloresrvcred()
 ENDIF
 uf_regvendas_controlaobjpagamento()
 IF ALLTRIM(ucrsregcreditosvalor.nmdoc)='Aviso Lanc. Ent.' .OR. ALLTRIM(ucrsregcreditosvalor.nmdoc)='Resumo Entid.'
    regvendas.pgfreg.page2.chkrecent.tag = "true"
 ELSE
    regvendas.pgfreg.page2.chkrecent.tag = "false"
 ENDIF

 SELECT ucrsregcreditosvalor
 IF ucrsregcreditosvalor.tipoDoc = 0 AND UPPER(ALLTRIM(ucrsregcreditosvalor.nmDoc)) == "ACERTO CONTA CORRENTE"
	IF ucrsregcreditosvalor.regularizar > 0
		uf_perguntalt_chama("O VALOR DE UM ACERTO DE CONTA CORRENTE N�O PODE SER POSITIVO.", "OK", "", 32)
		SELECT ucrsregcreditosvalor
		REPLACE ucrsregcreditosvalor.regularizar WITH ucrsregcreditosvalor.valornreg
	ENDIF
	IF ucrsregcreditosvalor.regularizar < ucrsregcreditosvalor.valornreg
		uf_perguntalt_chama("O VALOR A REGULARIZAR N�O PODE SER SUPERIOR AO VALOR N�O REGULARIZADO.", "OK", "", 32)
		SELECT ucrsregcreditosvalor
		REPLACE ucrsregcreditosvalor.regularizar WITH ucrsregcreditosvalor.valornreg
	ENDIF
 ENDIF

 IF ucrsregcreditosvalor.tipoDoc = 1
	IF ucrsregcreditosvalor.regularizar < 0
		uf_perguntalt_chama("O VALOR A REGULARIZAR DE UMA FATURA N�O PODE SER NEGATIVO.", "OK", "", 32)
		SELECT ucrsregcreditosvalor
		REPLACE ucrsregcreditosvalor.regularizar WITH ucrsregcreditosvalor.valornreg
	ENDIF
 ENDIF

 regvendas.pgfreg.page2.chkrecent.refresh
ENDFUNC
**
FUNCTION uf_regvendas_controlaobjpagamento
 IF (TYPE("REGVENDAS")=="U")
    RETURN .T.
 ENDIF
 IF (VARTYPE("REGVENDAS.ctnPagamento.txtValAtend")<>"U")
    IF regvendas.ctnpagamento.txtvalatend.value<0
       regvendas.ctnpagamento.txtdinheiro.value = astr(regvendas.ctnpagamento.txtvalatend.value, 9, 2)
       regvendas.ctnpagamento.txtdinheiro.refresh
       uf_regvendas_inactivaobjectospagamento()
       IF mypagcentral .AND.  .NOT. uf_gerais_validapagcentraldinheiro()
          uf_regvendas_limpaobjectospagamento()
       ENDIF
    ELSE
       IF ( .NOT. mypagcentral .OR. regvendas.pgfreg.page2.chkmovcaixa.tag=="false") .OR. uf_gerais_validapagcentraldinheiro()
          uf_regvendas_activaobjectospagamento()
       ELSE
          uf_regvendas_inactivaobjectospagamento()
          uf_regvendas_limpaobjectospagamento()
       ENDIF
    ENDIF
 ENDIF
ENDFUNC
**
PROCEDURE uf_regvendas_calculartrocorvcred
 IF regvendas.ctnpagamento.txtvalatend.value>0
    IF  .NOT. EMPTY(regvendas.ctnpagamento.txtvalrec.value)
       lctroco = ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2)-ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)
       IF lctroco>0
          regvendas.ctnpagamento.txttroco.value = ROUND(lctroco, 2)
       ELSE
          regvendas.ctnpagamento.txttroco.value = 0
       ENDIF
    ELSE
       regvendas.ctnpagamento.txttroco.value = 0
    ENDIF
 ELSE
    regvendas.ctnpagamento.txttroco.value = 0
    RETURN
 ENDIF
ENDPROC
**
PROCEDURE uf_regvendas_setposvirtualkeyrvcred
 DO CASE
    CASE regvendas.activecontrol.name=='dinheiro'
       regvendas.ctnpagamento.txtdinheiro.selstart = LEN(ALLTRIM(regvendas.ctnpagamento.txtdinheiro.value))
    CASE regvendas.activecontrol.name=='mb'
       regvendas.ctnpagamento.txtmb.selstart = LEN(ALLTRIM(regvendas.ctnpagamento.txtmb.value))
    CASE regvendas.activecontrol.name=='visa'
       regvendas.ctnpagamento.txtvisa.selstart = LEN(ALLTRIM(regvendas.ctnpagamento.txtvisa.value))
    CASE regvendas.activecontrol.name=='cheque'
       regvendas.ctnpagamento.txtcheque.selstart = LEN(ALLTRIM(regvendas.ctnpagamento.txtcheque.value))
    CASE regvendas.activecontrol.name=='modpag3'
       regvendas.ctnpagamento.txtmodpag3.selstart = LEN(ALLTRIM(regvendas.ctnpagamento.txtmodpag3.value))
    CASE regvendas.activecontrol.name=='modpag4'
       regvendas.ctnpagamento.txtmodpag4.selstart = LEN(ALLTRIM(regvendas.ctnpagamento.txtmodpag4.value))
    CASE regvendas.activecontrol.name=='modpag5'
       regvendas.ctnpagamento.txtmodpag5.selstart = LEN(ALLTRIM(regvendas.ctnpagamento.txtmodpag5.value))
    CASE regvendas.activecontrol.name=='modpag6'
       regvendas.ctnpagamento.txtmodpag6.selstart = LEN(ALLTRIM(regvendas.ctnpagamento.txtmodpag6.value))
    OTHERWISE
 ENDCASE
ENDPROC
**
FUNCTION up_regvendas_virtualkeyboardrvcred
 LPARAMETERS nkeycode, nshiftaltctrl
 IF regvendas.pgfreg.activepage==2
    IF  .NOT. (regvendas.activecontrol.name=='dinheiro') .AND.  .NOT. (regvendas.activecontrol.name=='mb') .AND.  .NOT. (regvendas.activecontrol.name=='visa') .AND.  .NOT. (regvendas.activecontrol.name=='cheque') .AND.  .NOT. (regvendas.activecontrol.name=='dfp') .AND.  .NOT. (regvendas.activecontrol.name=='dfv') .AND.  .NOT. (regvendas.activecontrol.name=='modpag3') .AND.  .NOT. (regvendas.activecontrol.name=='modpag4') .AND.  .NOT. (regvendas.activecontrol.name=='modpag5') .AND.  .NOT. (regvendas.activecontrol.name=='modpag6')
       RETURN .F.
    ENDIF
    DO CASE
       CASE nkeycode==13
          RETURN .F.
       CASE nkeycode==46
          regvendas.ctnpagamento.tecladovirtual1.pageframe1.page3.ponto.click()
          uf_regvendas_setposvirtualkeyrvcred()
       CASE nkeycode==48
          regvendas.ctnpagamento.tecladovirtual1.pageframe1.page3.zero.click()
          uf_regvendas_setposvirtualkeyrvcred()
       CASE nkeycode==49
          regvendas.ctnpagamento.tecladovirtual1.pageframe1.page3.um.click()
          uf_regvendas_setposvirtualkeyrvcred()
       CASE nkeycode==50
          regvendas.ctnpagamento.tecladovirtual1.pageframe1.page3.dois.click()
          uf_regvendas_setposvirtualkeyrvcred()
       CASE nkeycode==51
          regvendas.ctnpagamento.tecladovirtual1.pageframe1.page3.tres.click()
          uf_regvendas_setposvirtualkeyrvcred()
       CASE nkeycode==52
          regvendas.ctnpagamento.tecladovirtual1.pageframe1.page3.quatro.click()
          uf_regvendas_setposvirtualkeyrvcred()
       CASE nkeycode==53
          regvendas.ctnpagamento.tecladovirtual1.pageframe1.page3.cinco.click()
          uf_regvendas_setposvirtualkeyrvcred()
       CASE nkeycode==54
          regvendas.ctnpagamento.tecladovirtual1.pageframe1.page3.seis.click()
          uf_regvendas_setposvirtualkeyrvcred()
       CASE nkeycode==55
          regvendas.ctnpagamento.tecladovirtual1.pageframe1.page3.sete.click()
          uf_regvendas_setposvirtualkeyrvcred()
       CASE nkeycode==56
          regvendas.ctnpagamento.tecladovirtual1.pageframe1.page3.oito.click()
          uf_regvendas_setposvirtualkeyrvcred()
       CASE nkeycode==57
          regvendas.ctnpagamento.tecladovirtual1.pageframe1.page3.nove.click()
          uf_regvendas_setposvirtualkeyrvcred()
       CASE nkeycode==127
          regvendas.ctnpagamento.tecladovirtual1.pageframe1.page3.backdel.click()
          uf_regvendas_setposvirtualkeyrvcred()
       CASE nkeycode==7
          regvendas.ctnpagamento.tecladovirtual1.pageframe1.page3.del.click()
          uf_regvendas_setposvirtualkeyrvcred()
       OTHERWISE
    ENDCASE
 ENDIF
ENDFUNC
**
PROCEDURE uf_regvendas_controlaqtt
 LOCAL lcpos
 lcpos = 0
 SELECT ucrsregvendas
 lcpos = RECNO()
 GOTO TOP
 SCAN FOR ALLTRIM(ucrsregvendas.fistamp)==ALLTRIM(ucrsvale2.fistamp)
    IF ucrsregvendas.fmarcada
       IF ucrsvale2.fmarcada=.F.
          IF ucrsvale2.qtt2-ucrsvale2.eaquisicao<regvendas.pgfreg.page1.grdvreg.columns(7).text1.value
             uf_perguntalt_chama("AS QUANTIDADES 'A REGULARIZAR' N�O PODEM SER SUPERIORES �S QUANTIDADE 'POR REGULARIZAR'.", "OK", "", 64)
             REPLACE ucrsvale2.qtt WITH ucrsvale2.qtt2-ucrsvale2.eaquisicao
             regvendas.pgfreg.page1.grdvreg.columns(7).text1.value = ucrsvale2.qtt2-ucrsvale2.eaquisicao
             SELECT ucrsregvendas
             TRY
                GOTO lcpos
             CATCH
             ENDTRY
             RETURN
          ELSE
             IF regvendas.pgfreg.page1.grdvreg.columns(7).text1.value<=0
                uf_perguntalt_chama("N�O PODE ALTERAR AS QUANTIDADES PARA '0' OU MENOR QUE '0'.", "OK", "", 64)
                REPLACE ucrsvale2.qtt WITH ucrsvale2.qtt2-ucrsvale2.eaquisicao
                regvendas.pgfreg.page1.grdvreg.columns(7).text1.value = ucrsvale2.qtt2-ucrsvale2.eaquisicao
                SELECT ucrsregvendas
                TRY
                   GOTO lcpos
                CATCH
                ENDTRY
                RETURN
             ENDIF
          ENDIF
       ENDIF
    ENDIF
 ENDSCAN
 SELECT ucrsregvendas
 TRY
    GOTO lcpos
 CATCH
 ENDTRY
 SELECT ucrsvale2
 IF ucrsvale2.qtt2<regvendas.pgfreg.page1.grdvreg.columns(7).text1.value
    uf_perguntalt_chama("AS QUANTIDADES 'A REGULARIZAR' N�O PODEM SER SUPERIORES �S QUANTIDADE 'POR REGULARIZAR'.", "OK", "", 64)
    REPLACE ucrsvale2.qtt WITH ucrsvale2.qtt2
    regvendas.pgfreg.page1.grdvreg.columns(7).text1.value = ucrsvale2.qtt2
 ELSE
    IF regvendas.pgfreg.page1.grdvreg.columns(7).text1.value<=0
       uf_perguntalt_chama("N�O PODE ALTERAR AS QUANTIDADES PARA '0' OU MENOR QUE '0'.", "OK", "", 64)
       REPLACE ucrsvale2.qtt WITH ucrsvale2.qtt2
       regvendas.pgfreg.page1.grdvreg.columns(7).text1.value = ucrsvale2.qtt2
    ENDIF
 ENDIF
ENDPROC
**
FUNCTION uf_regvendas_botaopressrvcred
 LPARAMETERS lctexto
 IF regvendas.ctnpagamento.txtvalatend.value==0 .AND. regvendas.focugrdregvalor==0
    uf_regvendas_resetvaloresrvcred()
    RETURN .F.
 ELSE
    IF regvendas.ctnpagamento.txtvalatend.value<0 .AND. regvendas.focugrdregvalor==0
       RETURN .F.
    ENDIF
 ENDIF
 LOCAL lclen, lccaixa, lcobj
 STORE "" TO lccaixa, lcobj
 lclen = 0
 DO CASE
    CASE regvendas.ctnpagamento.txtdinheiro.backcolor==RGB(191, 223, 223)
       lccaixa = 'txtDinheiro'
       IF (OCCURS(".", regvendas.ctnpagamento.txtdinheiro.value)>0) .AND. (lctexto==".")
          RETURN .F.
       ENDIF
    CASE regvendas.ctnpagamento.txtmb.backcolor==RGB(191, 223, 223)
       lccaixa = 'txtMB'
       IF (OCCURS(".", regvendas.ctnpagamento.txtmb.value)>0) .AND. (lctexto==".")
          RETURN .F.
       ENDIF
    CASE regvendas.ctnpagamento.txtvisa.backcolor==RGB(191, 223, 223)
       lccaixa = 'txtVisa'
       IF (OCCURS(".", regvendas.ctnpagamento.txtvisa.value)>0) .AND. (lctexto==".")
          RETURN .F.
       ENDIF
    CASE regvendas.ctnpagamento.txtcheque.backcolor==RGB(191, 223, 223)
       lccaixa = 'txtCheque'
       IF (OCCURS(".", regvendas.ctnpagamento.txtcheque.value)>0) .AND. (lctexto==".")
          RETURN .F.
       ENDIF
    CASE regvendas.ctnpagamento.txtmodpag3.backcolor==RGB(191, 223, 223)
       lccaixa = 'txtModPag3'
       IF (OCCURS(".", regvendas.ctnpagamento.txtmodpag3.value)>0) .AND. (lctexto==".")
          RETURN .F.
       ENDIF
    CASE regvendas.ctnpagamento.txtmodpag4.backcolor==RGB(191, 223, 223)
       lccaixa = 'txtModPag4'
       IF (OCCURS(".", regvendas.ctnpagamento.txtmodpag4.value)>0) .AND. (lctexto==".")
          RETURN .F.
       ENDIF
    CASE regvendas.ctnpagamento.txtmodpag5.backcolor==RGB(191, 223, 223)
       lccaixa = 'txtModPag5'
       IF (OCCURS(".", regvendas.ctnpagamento.txtmodpag5.value)>0) .AND. (lctexto==".")
          RETURN .F.
       ENDIF
    CASE regvendas.ctnpagamento.txtmodpag6.backcolor==RGB(191, 223, 223)
       lccaixa = 'txtModPag6'
       IF (OCCURS(".", regvendas.ctnpagamento.txtmodpag6.value)>0) .AND. (lctexto==".")
          RETURN .F.
       ENDIF
    CASE regvendas.pgfreg.page2.txtdescperc.backcolor==RGB(191, 223, 223)
       lccaixa = 'txtDescPerc'
       IF (OCCURS(".", regvendas.pgfreg.page2.txtdescperc.value)>0) .AND. (lctexto==".")
          RETURN .F.
       ENDIF
    CASE regvendas.pgfreg.page2.txtdesceuro.backcolor==RGB(191, 223, 223)
       lccaixa = 'txtDescEuro'
       IF (OCCURS(".", regvendas.pgfreg.page2.txtdesceuro.value)>0) .AND. (lctexto==".")
          RETURN .F.
       ENDIF
    CASE regvendas.focugrdregvalor==1
       lccaixa = 'text1'
       IF (OCCURS(".", astr(regvendas.pgfreg.page2.grdregcreditovalor.regularizar.text1.value))>0) .AND. (lctexto==".")
          RETURN .F.
       ENDIF
    OTHERWISE
       RETURN .F.
 ENDCASE
 DO CASE
    CASE lccaixa=="txtDescPerc" .OR. lccaixa=="txtDescEuro"
       lcobj = 'REGVENDAS.pgfReg.page2.'+lccaixa
    CASE regvendas.focugrdregvalor==1
       lcobj = 'REGVENDAS.pgfReg.page2.grdRegCreditoValor.regularizar.'+lccaixa
    OTHERWISE
       lcobj = 'REGVENDAS.ctnPagamento.'+lccaixa
 ENDCASE
 IF lctexto=="backdel"
    lclen = LEN(ALLTRIM(&lcobj..VALUE))
    &lcobj..VALUE = LEFT(ALLTRIM(&lcobj..VALUE),lclen-1)
 ELSE
    IF lctexto=="del"
       &lcobj..VALUE = ""
    ELSE
       IF LEN(astr(&lcobj..VALUE))>=10
          RETURN .F.
       ENDIF
       &lcobj..VALUE = astr(&lcobj..VALUE) + lctexto
    ENDIF
 ENDIF
ENDFUNC
**
FUNCTION uf_regvendas_modopagamentorvcred
 LPARAMETERS lcmodop
 IF ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2)>=ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)
    RETURN .F.
 ENDIF
 IF EMPTY(regvendas.ctnpagamento.txtvalrec.value) .AND.  .NOT. EMPTY(regvendas.ctnpagamento.txtvalatend.value)
    DO CASE
       CASE lcmodop=="dinheiro"
          regvendas.ctnpagamento.txtdinheiro.value = astr(regvendas.ctnpagamento.txtvalatend.value, 15, 2)
          regvendas.ctnpagamento.txtdinheiro.gotfocus
       CASE lcmodop=="mb"
          regvendas.ctnpagamento.txtmb.value = astr(regvendas.ctnpagamento.txtvalatend.value, 15, 2)
          regvendas.ctnpagamento.txtmb.gotfocus
       CASE lcmodop=="visa"
          regvendas.ctnpagamento.txtvisa.value = astr(regvendas.ctnpagamento.txtvalatend.value, 15, 2)
          regvendas.ctnpagamento.txtvisa.gotfocus
       CASE lcmodop=="cheque"
          regvendas.ctnpagamento.txtcheque.value = astr(regvendas.ctnpagamento.txtvalatend.value, 15, 2)
          regvendas.ctnpagamento.txtcheque.gotfocus
       CASE lcmodop=="modpag3"

		  IF uf_gerais_getUmValor("b_modoPag", "pagamentoExterno", "ref = '05'")
		  	IF !uf_regVendas_modoPagExterno("05")
				RETURN .F.
			ENDIF
		  ENDIF

          regvendas.ctnpagamento.txtmodpag3.value = astr(regvendas.ctnpagamento.txtvalatend.value, 15, 2)
          regvendas.ctnpagamento.txtmodpag3.gotfocus
       CASE lcmodop=="modpag4"

		  IF uf_gerais_getUmValor("b_modoPag", "pagamentoExterno", "ref = '06'")
		  	IF !uf_regVendas_modoPagExterno("06")
				RETURN .F.
			ENDIF
		  ENDIF

          regvendas.ctnpagamento.txtmodpag4.value = astr(regvendas.ctnpagamento.txtvalatend.value, 15, 2)
          regvendas.ctnpagamento.txtmodpag4.gotfocus
       CASE lcmodop=="modpag5"

		  IF uf_gerais_getUmValor("b_modoPag", "pagamentoExterno", "ref = '07'")
		  	IF !uf_regVendas_modoPagExterno("07")
				RETURN .F.
			ENDIF
		  ENDIF

          regvendas.ctnpagamento.txtmodpag5.value = astr(regvendas.ctnpagamento.txtvalatend.value, 15, 2)
          regvendas.ctnpagamento.txtmodpag5.gotfocus
       CASE lcmodop=="modpag6"

		  IF uf_gerais_getUmValor("b_modoPag", "pagamentoExterno", "ref = '08'")
		  	IF !uf_regVendas_modoPagExterno("08")
				RETURN .F.
			ENDIF
		  ENDIF

          regvendas.ctnpagamento.txtmodpag6.value = astr(regvendas.ctnpagamento.txtvalatend.value, 15, 2)
          regvendas.ctnpagamento.txtmodpag6.gotfocus
       OTHERWISE
    ENDCASE
 ENDIF
 DO CASE
    CASE lcmodop=="dinheiro"
       IF  .NOT. EMPTY(regvendas.ctnpagamento.txtvalrec.value) .AND. (ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2)<ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)) .AND. EMPTY(regvendas.ctnpagamento.txtdinheiro.value)
          regvendas.ctnpagamento.txtdinheiro.value = astr(ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)-ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2), 15, 2)
          regvendas.ctnpagamento.txtvalrec.value = astr(regvendas.ctnpagamento.txtvalatend.value, 15, 2)
          regvendas.ctnpagamento.txttroco.value = ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2)-ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)
          regvendas.ctnpagamento.txtdinheiro.gotfocus
       ENDIF
    CASE lcmodop=="mb"
       IF  .NOT. EMPTY(regvendas.ctnpagamento.txtvalrec.value) .AND. (ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2)<ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)) .AND. EMPTY(regvendas.ctnpagamento.txtmb.value)
          regvendas.ctnpagamento.txtmb.value = astr(ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)-ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2), 15, 2)
          regvendas.ctnpagamento.txtvalrec.value = astr(regvendas.ctnpagamento.txtvalatend.value, 15, 2)
          regvendas.ctnpagamento.txttroco.value = ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2)-ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)
          regvendas.ctnpagamento.txtmb.gotfocus
       ENDIF
    CASE lcmodop=="visa"
       IF  .NOT. EMPTY(regvendas.ctnpagamento.txtvalrec.value) .AND. (ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2)<ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)) .AND. EMPTY(regvendas.ctnpagamento.txtvisa.value)
          regvendas.ctnpagamento.txtvisa.value = astr(ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)-ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2), 15, 2)
          regvendas.ctnpagamento.txtvalrec.value = astr(regvendas.ctnpagamento.txtvalatend.value, 15, 2)
          regvendas.ctnpagamento.txttroco.value = ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2)-ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)
          regvendas.ctnpagamento.txtvisa.gotfocus
       ENDIF
    CASE lcmodop=="cheque"
       IF  .NOT. EMPTY(regvendas.ctnpagamento.txtvalrec.value) .AND. (ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2)<ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)) .AND. EMPTY(regvendas.ctnpagamento.txtcheque.value)
          regvendas.ctnpagamento.txtcheque.value = astr(ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)-ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2), 15, 2)
          regvendas.ctnpagamento.txtvalrec.value = astr(regvendas.ctnpagamento.txtvalatend.value, 15, 2)
          regvendas.ctnpagamento.txttroco.value = ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2)-ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)
          regvendas.ctnpagamento.txtcheque.gotfocus
       ENDIF
    CASE lcmodop=="modpag3"
       IF  .NOT. EMPTY(regvendas.ctnpagamento.txtvalrec.value) .AND. (ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2)<ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)) .AND. EMPTY(regvendas.ctnpagamento.txtmodpag3.value)

		  IF uf_gerais_getUmValor("b_modoPag", "pagamentoExterno", "ref = '05'")
		  	IF !uf_regVendas_modoPagExterno("05")
				RETURN .F.
			ENDIF
		  ENDIF

          regvendas.ctnpagamento.txtmodpag3.value = astr(ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)-ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2), 15, 2)
          regvendas.ctnpagamento.txtvalrec.value = astr(regvendas.ctnpagamento.txtvalatend.value, 15, 2)
          regvendas.ctnpagamento.txttroco.value = ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2)-ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)
          regvendas.ctnpagamento.txtmodpag3.gotfocus
       ENDIF
    CASE lcmodop=="modpag4"
       IF  .NOT. EMPTY(regvendas.ctnpagamento.txtvalrec.value) .AND. (ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2)<ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)) .AND. EMPTY(regvendas.ctnpagamento.txtmodpag4.value)

		  IF uf_gerais_getUmValor("b_modoPag", "pagamentoExterno", "ref = '06'")
		  	IF !uf_regVendas_modoPagExterno("06")
				RETURN .F.
			ENDIF
		  ENDIF

          regvendas.ctnpagamento.txtmodpag4.value = astr(ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)-ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2), 15, 2)
          regvendas.ctnpagamento.txtvalrec.value = astr(regvendas.ctnpagamento.txtvalatend.value, 15, 2)
          regvendas.ctnpagamento.txttroco.value = ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2)-ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)
          regvendas.ctnpagamento.txtmodpag4.gotfocus
       ENDIF
    CASE lcmodop=="modpag5"
       IF  .NOT. EMPTY(regvendas.ctnpagamento.txtvalrec.value) .AND. (ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2)<ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)) .AND. EMPTY(regvendas.ctnpagamento.txtmodpag5.value)

		  IF uf_gerais_getUmValor("b_modoPag", "pagamentoExterno", "ref = '07'")
		  	IF !uf_regVendas_modoPagExterno("07")
				RETURN .F.
			ENDIF
		  ENDIF

          regvendas.ctnpagamento.txtmodpag5.value = astr(ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)-ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2), 15, 2)
          regvendas.ctnpagamento.txtvalrec.value = astr(regvendas.ctnpagamento.txtvalatend.value, 15, 2)
          regvendas.ctnpagamento.txttroco.value = ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2)-ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)
          regvendas.ctnpagamento.txtmodpag5.gotfocus
       ENDIF
    CASE lcmodop=="modpag6"
       IF  .NOT. EMPTY(regvendas.ctnpagamento.txtvalrec.value) .AND. (ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2)<ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)) .AND. EMPTY(regvendas.ctnpagamento.txtmodpag6.value)

		  IF uf_gerais_getUmValor("b_modoPag", "pagamentoExterno", "ref = '08'")
		  	IF !uf_regVendas_modoPagExterno("08")
				RETURN .F.
			ENDIF
		  ENDIF

          regvendas.ctnpagamento.txtmodpag5.value = astr(ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)-ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2), 15, 2)
          regvendas.ctnpagamento.txtvalrec.value = astr(regvendas.ctnpagamento.txtvalatend.value, 15, 2)
          regvendas.ctnpagamento.txttroco.value = ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2)-ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)
          regvendas.ctnpagamento.txtmodpag6.gotfocus
       ENDIF
    OTHERWISE
 ENDCASE
ENDFUNC
**
FUNCTION uf_regvendas_gravar
 PUBLIC receitaencomenda, nrreceitaenc, codreceitaenc, cdoreceitaenc
 STORE .F. TO receitaencomenda
 STORE '' TO nrreceitaenc, codreceitaenc, cdoreceitaenc
 IF uf_gerais_getparameter_site('ADM0000000060', 'BOOL', mysite) .AND. regvendas.pgfreg.activepage<>2
    IF ALLTRIM(ch_grupo)<>'Administrador' .AND. ALLTRIM(ch_grupo)<>'Supervisores'
       IF  .NOT. uf_gerais_valida_password_admin('REGULARIZA��ES', 'PUXAR REGULARIZA��O')
          RETURN .F.
       ENDIF
    ENDIF
 ENDIF
 DO CASE
    CASE regvendas.pgfreg.activepage==1
       LOCAL lccontaencslin, lccontaoutrosdocs
       STORE 0 TO lccontaencslin, lccontaoutrosdocs
       
       
       SELECT ucrsfiltraprodreceita
       GO TOP 
       IF LEN(ALLTRIM(ucrsfiltraprodreceita.u_receita))>0
			uf_regvendas_filtraprod_receita()
       ENDIF 
       
       SELECT ucrsregvendas
       GOTO TOP
       SELECT * FROM ucrsregvendas WHERE ALLTRIM(nmdoc)=='Encomenda de Cliente' AND EMPTY(fistamp) AND sel=.T. INTO CURSOR ucrsencclslin
       lccontaencslin = RECCOUNT("ucrsencclslin")
       SELECT ucrsregvendas
       GOTO TOP
       SELECT * FROM ucrsregvendas WHERE ALLTRIM(nmdoc)<>'Encomenda de Cliente' AND sel=.T. INTO CURSOR ucrsencclslinoutros
       lccontaoutrosdocs = RECCOUNT("ucrsencclslinoutros")
       IF lccontaencslin>0 .AND. lccontaoutrosdocs>0
          uf_perguntalt_chama("N�O PODE REGULARIZAR ENCOMENDAS COM RECEITAS E OUTROS DOCUMENTOS AO MESMO TEMPO.", "OK", "", 16)
          RETURN .F.
       ENDIF
       IF lccontaencslin=1
          uf_regvendas_encomendasslin_sel()
          SELECT ucrsregvendas
          DELETE FROM ucrsregvendas WHERE ALLTRIM(nmdoc)=='Encomenda de Cliente' .AND. sel=.T. .AND. EMPTY(fistamp)
       ELSE
          IF lccontaencslin>1
             uf_perguntalt_chama("S� PODE REGULARIZAR 1 ENCOMENDA COM RECEITA DE CADA VEZ.", "OK", "", 16)
             RETURN .F.
          ENDIF
       ENDIF

	   IF !uf_regvendas_checkInactiveRefs()
	   		RETURN .F.
	   ENDIF

      SELECT UCRSREGVENDAS 
      GO TOP

      CALCULATE COUNT() FOR UCRSREGVENDAS.sel AND UPPER(ALLTRIM(UCRSREGVENDAS.nmdoc)) = "RESERVA DE CLIENTE" AND !EMPTY(UCRSREGVENDAS.nrreceita) TO uv_count

      IF uv_count > 0

         IF !uf_atendimento_chkReservaDEM("REGULARIZAR")
            RETURN .F.
         ENDIF

      ENDIF

       IF USED("ucrsencclslin")
          fecha("ucrsencclslin")
       ENDIF
       IF USED("ucrsencclslinoutros")
          fecha("ucrsencclslinoutros")
       ENDIF
       SELECT ucrsregvendas
       GOTO TOP
       SELECT * FROM ucrsregvendas WHERE ALLTRIM(nmdoc)='Encomenda de Cliente' AND  NOT EMPTY(fistamp) AND sel=.T. INTO CURSOR ucrsenccl
       SELECT ucrsenccl
       IF RECCOUNT("ucrsenccl")>0
          uf_regvendas_encomendas_sel()
          SELECT ucrsregvendas
          DELETE FROM ucrsregvendas WHERE ALLTRIM(nmdoc)=='Encomenda de Cliente' .AND. sel=.T.
       ENDIF
       IF USED("ucrsenccl")
          fecha("ucrsenccl")
       ENDIF
       
       IF USED("ucrsvale")
          SELECT * FROM ucrsvale INTO CURSOR ucrsvalecurini READWRITE
       ENDIF
       SELECT * FROM ucrsvale2 INTO CURSOR uCrsvaleTemp READWRITE
       uf_regvendas_reservas_sel()
       LOCAL lclordemt, lccontal
       STORE '' TO clordemt
       STORE 0 TO lccontal
       SELECT 1E0 AS conta, * FROM fi WHERE  NOT EMPTY(fi.tipor) INTO CURSOR fitemp READWRITE
       SELECT fitemp
       GOTO TOP
       SCAN
          lccontal = 0
          lclordemt = LEFT((ALLTRIM(STR(fitemp.lordem))), 1)
          SELECT fi
          GOTO TOP
          CALCULATE CNT() TO lccontal FOR ALLTRIM(lclordemt)==LEFT((ALLTRIM(STR(fi.lordem))), 1)
          SELECT fitemp
          IF lccontal<>0
             REPLACE fitemp.conta WITH lccontal
          ENDIF
       ENDSCAN
       SELECT fitemp
       GOTO TOP
       SCAN
          IF conta<=1
             DELETE FROM fi WHERE fi.fistamp=fitemp.fistamp .AND. lordem<>100000
          ENDIF
       ENDSCAN
       SELECT * FROM uCrsvaleTemp INTO CURSOR ucrsvale2 READWRITE
       fecha("uCrsvaleTemp")
       SELECT * FROM fi INTO CURSOR ucrsfidelete READWRITE
       SELECT ucrsfidelete
       GOTO TOP
       SCAN
          DELETE FROM ucrsvale2 WHERE ALLTRIM(ucrsvale2.fistamp)==ALLTRIM(ucrsfidelete.ofistamp)
          SELECT ucrsfidelete
       ENDSCAN

       fecha("ucrsfidelete")
       uf_regvendas_regvendassel()
    CASE regvendas.pgfreg.activepage==2
       uf_regvendas_gravarregcredito()
    CASE regvendas.pgfreg.activepage==3
       uf_pagcentral_registarvalores()
 ENDCASE
ENDFUNC
**
FUNCTION uf_regvendas_gravarregcredito

 LOCAL lcvalidapasswut
 STORE .T. TO lcvalidapasswut
 IF TYPE("UtilPlafond")<>"U"
    IF utilplafond=.T.
       lcvalidapasswut = .F.
    ENDIF
 ENDIF
 IF ALLTRIM(ucrse1.tipoempresa)=="PARAFARMACIA" .OR. ALLTRIM(ucrse1.tipoempresa)=="FARMACIA" .AND. lcvalidapasswut=.T.
    LOCAL lcvalidapw
    STORE 0 TO lcvalidapw
    DO WHILE lcvalidapw=0
       lcvalidapw = uf_painelcentral_login()
       IF lcvalidapw==2
          EXIT
       ENDIF
    ENDDO
    IF lcvalidapw==2
       RETURN .F.
    ENDIF
    IF (VARTYPE(regvendas)<>"U")
       IF (VARTYPE(regvendas.ctnpagamento)<>"U")
          IF (VARTYPE(regvendas.ctnpagamento.txtvendnm)<>"U")
             regvendas.ctnpagamento.txtvendnm.value = ch_vendnm
          ENDIF
       ENDIF
    ENDIF
 ENDIF

 IF regvendas.pgfreg.page2.chkporproduto.tag=="true"
    IF  .NOT. USED("uCrsRegCreditos")
       RETURN .F.
    ENDIF
 ELSE
    IF  .NOT. USED("uCrsRegCreditosValor")
       RETURN .F.
    ENDIF
 ENDIF
 IF TYPE("atendimento")=="U"
    PUBLIC nratendimento
    uf_atendimento_geranratendimento()
 ENDIF
 IF regvendas.pgfreg.page2.chkmovcaixa.tag=="true"
 	uf_atendimento_configoperador()
    uf_gestaocaixas_verificacaixa()
    IF EMPTY(ALLTRIM(mycxstamp)) .OR. EMPTY(ALLTRIM(myssstamp))
       IF uf_perguntalt_chama("A op��o de Movimentar Caixa est� activa mas n�o tem nenhuma caixa aberta."+CHR(13)+CHR(13)+"Pretende abrir uma nova caixa?", "Sim", "N�o")
          uf_caixas_gerircaixa(.F.)
       ELSE
          RETURN .F.
       ENDIF
    ENDIF

 ELSE
    mycxstamp = ""
    mycxuser = ""
    myssstamp = ""
 ENDIF
 IF  .NOT. USED("FT")
    IF  .NOT. uf_gerais_actgrelha("", 'FT', [set fmtonly on exec up_touch_ft '' set fmtonly off])
       uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR [FT].", "OK", "", 16)
       RETURN .F.
    ELSE
       SELECT ft
       APPEND BLANK
       REPLACE ft.moeda WITH 'EURO'
       REPLACE ft.pais WITH 1
       REPLACE ft.vendedor WITH ch_vendedor
       REPLACE ft.vendnm WITH ch_vendnm
    ENDIF
 ENDIF
 IF  .NOT. USED("FT2")
    IF  .NOT. uf_gerais_actgrelha("", 'FT2', 'set fmtonly on select * from FT2 (nolock) set fmtonly off')
       uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR [FT2].", "OK", "", 16)
       RETURN .F.
    ELSE
       SELECT ft2
       APPEND BLANK
    ENDIF
 ENDIF
** inicialmente aqui 


 PUBLIC lccontinuamaqpag
 lccontinuamaqpag = .F.
 LOCAL lcdinheiropc
 lcdinheiropc = VAL(regvendas.ctnpagamento.txtdinheiro.value)
 IF lcdinheiropc<>0 .AND. uf_gerais_getparameter_site('ADM0000000041', 'BOOL', mysite)=.T.
    uf_maqdinheiro_chama()
    IF lccontinuamaqpag=.F.
       RETURN (.F.)
    ENDIF
    lcstampatmaq = nratendimento
 ENDIF
 IF VAL(regvendas.pgfreg.page2.txtdescperc.value)<0 .OR. VAL(regvendas.pgfreg.page2.txtdescperc.value)>100
    uf_perguntalt_chama("O DESCONTO FINANCEIRO EM PERCENTAGEM � INV�LIDO. POR FAVOR RECTIFIQUE.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF VAL(regvendas.pgfreg.page2.txtdesceuro.value)<0
    uf_perguntalt_chama("O DESCONTO FINANCEIRO EM VALOR � INV�LIDO. POR FAVOR RECTIFIQUE.", "OK", "", 48)
    RETURN .F.
 ENDIF
 LOCAL lccountsel
 STORE 0 TO lccountsel
 

 
 IF regvendas.pgfreg.page2.chkporproduto.tag=="true"
    SELECT ucrsregcreditos
    CALCULATE SUM(ucrsregcreditos.qtt) TO lccountsel FOR ucrsregcreditos.sel
    IF EMPTY(lccountsel)
       uf_perguntalt_chama("N�O SELECCIONOU NENHUM CR�DITO PARA REGULARIZAR.", "OK", "", 48)
       RETURN .F.
    ENDIF
 ELSE
    SELECT ucrsregcreditosvalor
    COUNT FOR ucrsregcreditosvalor.sel==.T. TO lccountsel
    IF lccountsel==0
       uf_perguntalt_chama("N�O SELECCIONOU NENHUM CR�DITO PARA REGULARIZAR.", "OK", "", 48)
       RETURN .F.
    ENDIF
 ENDIF
 IF regvendas.pgfreg.page2.chkporproduto.tag=="false"
    SELECT ucrsregcreditosvalor
    SCAN FOR ucrsregcreditosvalor.sel==.T.
       IF UPPER(ALLTRIM(ucrse1.id_lt))<>'LTDEV30' .AND. UPPER(ALLTRIM(ucrse1.id_lt))<>'E01322A'
          IF ucrsregcreditosvalor.tipodoc==3 .AND. ucrsregcreditosvalor.regularizar>0
             uf_perguntalt_chama("N�o � poss�vel colocar um valor positivo para Documentos de Cr�dito."+CHR(13)+CHR(13)+"Por favor verifique.", "OK", "", 48)
             RETURN .F.
          ENDIF
       ENDIF
       IF ABS(ucrsregcreditosvalor.regularizar)>ABS(ucrsregcreditosvalor.valornreg)
          uf_perguntalt_chama("Seleccionou documentos em que o valor a regularizar � superior ao valor n�o regularizado."+CHR(13)+CHR(13)+"Por favor verifique.", "OK", "", 48)
          RETURN .F.
       ENDIF
    ENDSCAN
 ENDIF
 IF uf_gerais_getparameter("ADM0000000037", "BOOL")
    IF VAL(regvendas.ctnpagamento.txtmb.value)+VAL(regvendas.ctnpagamento.txtvisa.value)+VAL(regvendas.ctnpagamento.txtcheque.value)+VAL(regvendas.ctnpagamento.txtmodpag3.value)+VAL(regvendas.ctnpagamento.txtmodpag4.value)+VAL(regvendas.ctnpagamento.txtmodpag5.value)+VAL(regvendas.ctnpagamento.txtmodpag6.value)>=ROUND(regvendas.ctnpagamento.txtvalatend.value, 2) .AND. (VAL(regvendas.ctnpagamento.txtmb.value)<>0 .OR. VAL(regvendas.ctnpagamento.txtvisa.value)<>0 .OR. VAL(regvendas.ctnpagamento.txtcheque.value)<>0 .OR. VAL(regvendas.ctnpagamento.txtmodpag3.value)<>0 .OR. VAL(regvendas.ctnpagamento.txtmodpag4.value)<>0 .OR. VAL(regvendas.ctnpagamento.txtmodpag5.value)<>0 .OR. VAL(regvendas.ctnpagamento.txtmodpag6.value)<>0) .AND. VAL(regvendas.ctnpagamento.txtdinheiro.value)<>0
       uf_perguntalt_chama("APENAS PODER� INCLUIR TROCO NO MEIO DE PAGAMENTO A DINHEIRO. DEVE CORRIGIR A SITUA��O ANTES DE PODER CONTINUAR.", "OK", "", 48)
       RETURN .F.
    ENDIF
    IF VAL(regvendas.ctnpagamento.txtmb.value)+VAL(regvendas.ctnpagamento.txtvisa.value)+VAL(regvendas.ctnpagamento.txtcheque.value)>ROUND(regvendas.ctnpagamento.txtvalatend.value, 2) .AND. (VAL(regvendas.ctnpagamento.txtmb.value)<>0 .OR. VAL(regvendas.ctnpagamento.txtvisa.value)<>0 .OR. VAL(regvendas.ctnpagamento.txtcheque.value)<>0) .AND. VAL(regvendas.ctnpagamento.txtdinheiro.value)=0
       uf_perguntalt_chama("APENAS PODER� INCLUIR TROCO NO MEIO DE PAGAMENTO A DINHEIRO. DEVE CORRIGIR A SITUA��O ANTES DE PODER CONTINUAR.", "OK", "", 48)
       RETURN .F.
    ENDIF
 ENDIF
 IF ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)>0 .AND. (ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2)<ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)) .AND. ( .NOT. mypagcentral .OR. uf_gerais_validapagcentraldinheiro())
    uf_perguntalt_chama("ATEN��O: N�O PODE FINALIZAR COM O VALOR RECEBIDO INFERIOR AO VALOR TOTAL.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)<0 .AND. (ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2)>ROUND(regvendas.ctnpagamento.txtvalatend.value, 2)) .AND. ( .NOT. mypagcentral .OR. uf_gerais_validapagcentraldinheiro())
    uf_perguntalt_chama("ATEN��O: N�O PODE FINALIZAR COM O VALOR RECEBIDO SUPERIOR AO VALOR TOTAL.", "OK", "", 48)
    RETURN .F.
 ENDIF
 
  
 if(!uf_regvendas_validaPermiteMultiRecibos())
 	  uf_perguntalt_chama("N�O � PERMITIDO EMITIR O MESMO RECIBO PARA DOCUMENTOS DISTINTOS.", "OK", "", 48)
 	   RETURN .F.
 ENDIF
 

 

 
 LOCAL lcno, lcestab
 STORE 0 TO lcno, lcestab
 IF regvendas.pgfreg.page2.chkporproduto.tag=="true"
    SELECT DISTINCT no FROM uCrsRegCreditos WHERE sel==.T. INTO CURSOR uCrsRegCrDistNo READWRITE
 ELSE
    SELECT DISTINCT no FROM uCrsRegCreditosValor WHERE sel==.T. INTO CURSOR uCrsRegCrDistNo READWRITE
 ENDIF
 IF RECCOUNT("uCrsRegCrDistNo")>1 .AND. regvendas.pgfreg.page2.chkporutente.tag="false"
    uf_perguntalt_chama("APENAS PODER� EMITIR O RECIBO PARA UM CLIENTE DE CADA VEZ.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF regvendas.pgfreg.page2.chkporutente.tag="false"
    SELECT ucrsregcrdistno
    GOTO TOP
    lcno = ucrsregcrdistno.no
 ELSE
    lcno = astr(regvendas.txtno.value)
 ENDIF
 fecha("uCrsRegCrDistNo")
 IF regvendas.pgfreg.page2.chkporproduto.tag=="true"
    SELECT DISTINCT estab FROM uCrsRegCreditos WHERE sel==.T. INTO CURSOR uCrsRegCrDistEstab READWRITE
 ELSE
    SELECT DISTINCT estab FROM uCrsRegCreditosValor WHERE sel==.T. INTO CURSOR uCrsRegCrDistEstab READWRITE
 ENDIF
 
 LOCAL lcTpaMb 
 
 IF EMPTY(regvendas.ctnpagamento.txtmb.value)
    lcTpaMb = 0
 ELSE
    lcTpaMb = VAL(regvendas.ctnpagamento.txtmb.value)
 ENDIF
 
 
 IF (VARTYPE(mytpastamp)<>"U" .AND. lcTpaMb >0)
    IF ( .NOT. uf_pagamento_pagatpa(lcTpaMb))
       RETURN .F.
    ENDIF
 ENDIF
 
 IF regvendas.pgfreg.page2.chkporutente.tag="false"
    IF RECCOUNT("uCrsRegCrDistEstab")>1
       IF uf_perguntalt_chama("FORAM DETECTADOS V�RIOS DEPENDENTES."+CHR(13)+"Por favor escolha:"+CHR(13)+CHR(13)+"[Sede] - Recibo geral em nome da sede."+CHR(13)+CHR(13)+"[Separado] - Recibos individuais por cada dependente.", "Sede", "Separado")
          IF uf_gerais_actgrelha("", 'uCrsClReg', 'exec up_touch_dadosCliente '+astr(lcno)+', 0')
             SELECT ucrsclreg
             lcclno = ucrsclreg.no
             lcclestab = ucrsclreg.estab

			 IF !uf_regvendas_geraPagExternos(lcClNo, lcClEstab, nrAtendimento)
				RETURN .F.
			 ENDIF

             IF !uf_regvendas_emitirrecibos(lcclno, lcclestab, .F.)
               RETURN .F.
             ENDIF
          ENDIF
       ELSE
          SELECT ucrsregcrdistestab
          GOTO TOP
          SCAN
             IF uf_gerais_actgrelha("", 'uCrsClReg', 'exec up_touch_dadosCliente '+astr(lcno)+', '+astr(ucrsregcrdistestab.estab))
                SELECT ucrsclreg
                lcclno = ucrsclreg.no
                lcclestab = ucrsclreg.estab

				IF !uf_regvendas_geraPagExternos(lcClNo, lcClEstab, nrAtendimento)
					RETURN .F.
				ENDIF

               IF !uf_regvendas_emitirrecibos(lcclno, lcclestab, .T.)
                  RETURN .F.
               ENDIF
             ENDIF
          ENDSCAN
       ENDIF
    ELSE
       SELECT ucrsregcrdistestab
       GOTO TOP
       lcestab = ucrsregcrdistestab.estab
       

       
       IF lcestab<>0
          IF uf_perguntalt_chama("PRETENDE EMITIR O RECIBO EM NOME DO DEPENDENTE?"+CHR(13)+CHR(13)+"[Dependente] - Recibo em nome do dependente."+CHR(13)+CHR(13)+"[Sede] - Recibo em nome da sede.", "Dependente", "Sede")
             IF uf_gerais_actgrelha("", 'uCrsClReg', 'exec up_touch_dadosCliente '+astr(lcno)+', '+astr(lcestab))
                SELECT ucrsclreg
                lcclno = ucrsclreg.no
                lcclestab = ucrsclreg.estab

				IF !uf_regvendas_geraPagExternos(lcClNo, lcClEstab, nrAtendimento)
					RETURN .F.
				ENDIF
 
               IF !uf_regvendas_emitirrecibos(lcclno, lcclestab, .F.)
                  RETURN .F.
               ENDIF
             ENDIF
          ELSE
             IF uf_gerais_actgrelha("", 'uCrsClReg', 'exec up_touch_dadosCliente '+astr(lcno)+', 0')
                SELECT ucrsclreg
                lcclno = ucrsclreg.no
                lcclestab = ucrsclreg.estab

				IF !uf_regvendas_geraPagExternos(lcClNo, lcClEstab, nrAtendimento)
					RETURN .F.
				ENDIF

               IF !uf_regvendas_emitirrecibos(lcclno, lcclestab, .F.)
                  RETURN .F.
               ENDIF
             ENDIF
          ENDIF
       ELSE
          IF uf_gerais_actgrelha("", 'uCrsClReg', 'exec up_touch_dadosCliente '+astr(lcno)+', 0')
             SELECT ucrsclreg
             lcclno = ucrsclreg.no
             lcclestab = ucrsclreg.estab

			 IF !uf_regvendas_geraPagExternos(lcClNo, lcClEstab, nrAtendimento)
				RETURN .F.
			 ENDIF

            IF !uf_regvendas_emitirrecibos(lcclno, lcclestab, .F.)
               RETURN .F.
            ENDIF
          ENDIF
       ENDIF
    ENDIF
 ELSE
    lcclno = lcno
    IF  .NOT. EMPTY(regvendas.txtestab.value)
       lcclestab = astr(regvendas.txtestab.value)
    ELSE
       lcclestab = 0
    ENDIF
    
    
    IF uf_gerais_actgrelha("", 'uCrsClReg', 'exec up_touch_dadosCliente '+astr(lcclno)+', '+astr(lcclestab)+'')

       IF !uf_regvendas_geraPagExternos(lcno, lcclestab, nrAtendimento)
			RETURN .F.
	   ENDIF

      IF !uf_regvendas_emitirrecibos(lcno, lcclestab, .F.)
         RETURN .F.
      ENDIF
    ELSE
       uf_perguntalt_chama("Ocorreu um erro ao tentar obter os dados do utente. Por favor contacte o suporte.", "OK", "", 48)
    ENDIF
 ENDIF
 fecha("uCrsRegCrDistEstab")
 LOCAL lctrocopc, lcdinheiropc, lcmbpc, lcvisapc, lcchequepc, lcmodpag3, lcmodpag4, lcmodpag5, lcmodpag6
 STORE 0 TO lctrocopc, lcdinheiropc, lcmbpc, lcvisapc, lcchequepc, lcmodpag3, lcmodpag4, lcmodpag5, lcmodpag6
 IF EMPTY(regvendas.ctnpagamento.txttroco.value)
    lctrocopc = 0
 ELSE
    lctrocopc = regvendas.ctnpagamento.txttroco.value
 ENDIF
 IF EMPTY(regvendas.ctnpagamento.txtdinheiro.value)
    lcdinheiropc = 0
 ELSE
    lcdinheiropc = VAL(regvendas.ctnpagamento.txtdinheiro.value)
 ENDIF
 IF EMPTY(regvendas.ctnpagamento.txtmb.value)
    lcmbpc = 0
 ELSE
    lcmbpc = VAL(regvendas.ctnpagamento.txtmb.value)
 ENDIF
 IF EMPTY(regvendas.ctnpagamento.txtvisa.value)
    lcvisapc = 0
 ELSE
    lcvisapc = VAL(regvendas.ctnpagamento.txtvisa.value)
 ENDIF
 IF EMPTY(regvendas.ctnpagamento.txtcheque.value)
    lcchequepc = 0
 ELSE
    lcchequepc = VAL(regvendas.ctnpagamento.txtcheque.value)
 ENDIF
 IF EMPTY(regvendas.ctnpagamento.txtmodpag3.value)
    lcmodpag3 = 0
 ELSE
    lcmodpag3 = VAL(regvendas.ctnpagamento.txtmodpag3.value)
 ENDIF
 IF EMPTY(regvendas.ctnpagamento.txtmodpag4.value)
    lcmodpag4 = 0
 ELSE
    lcmodpag4 = VAL(regvendas.ctnpagamento.txtmodpag4.value)
 ENDIF
 IF EMPTY(regvendas.ctnpagamento.txtmodpag5.value)
    lcmodpag5 = 0
 ELSE
    lcmodpag5 = VAL(regvendas.ctnpagamento.txtmodpag5.value)
 ENDIF
 IF EMPTY(regvendas.ctnpagamento.txtmodpag6.value)
    lcmodpag6 = 0
 ELSE
    lcmodpag6 = VAL(regvendas.ctnpagamento.txtmodpag6.value)
 ENDIF

 LOCAL lcdevrealpc, lcdevrealpc1, lcdevrealpc2, lcntcreditopc, lccreditopc, lctotalbruto
 STORE 0 TO lcdevrealpc, lcdevrealpc1, lcdevrealpc2, lcntcreditopc, lccreditopc, lctotalbruto
 IF regvendas.pgfreg.page2.chkmovcaixa.tag=="true"
    LOCAL lcdevefechar, lcfechastamp
    lcdevefechar = .F.
    lcfechastamp = ''
    IF uf_gerais_validapagcentraldinheiro()
       IF (ROUND((lcdinheiropc-lctrocopc), 2))!=0
          lcdevefechar = .F.
       ELSE
          lcdevefechar = .T.
          lcfechastamp = uf_gerais_stamp()
       ENDIF
    ENDIF
    
    uf_gerais_regista_logs_passos("B_PAGCENTRAL",nrAtendimento,"b_pag1","insert antes uf_regvendas_gravarregcredito")
    
    TEXT TO lcsql TEXTMERGE NOSHOW
			Insert Into B_pagCentral
				(stamp,
				nrAtend, nrVendas, total,
				ano, no, estab,
				vendedor, nome,
				terminal, terminal_nome,
				evdinheiro, epaga1, epaga2, echtotal,
				evdinheiro_semTroco, etroco,
				total_bruto, devolucoes, ntCredito, creditos,
				cxstamp, ssstamp, site
				,fechado
				,epaga3
				,epaga4
				,epaga5
				,epaga6
				,odata
				,udata
				,fechastamp
			)values(
				'<<astr(YEAR(DATE())) + nrAtendimento>>',
				'<<nrAtendimento>>', 0, <<Round(REGVENDAS.ctnPagamento.txtValAtend.value,2)>>,
				<<YEAR(DATE())>>, <<lcClNo>>, <<lcClEstab>>,
				<<ch_vendedor>>, '<<ALLTRIM(ch_vendnm)>>',
				<<myTermNo>>, '<<ALLTRIM(myTerm)>>',
				<<lcDinheiroPc-lcTrocoPc>>, <<lcVisaPc>>, <<lcMbPc>>, <<lcChequePc>>,
				<<lcDinheiroPc>>, <<lcTrocoPc>>,
				<<lcTotalBruto>>, <<lcDevRealPc>>, <<lcNtCreditoPc>>, <<lcCreditoPc>>,
				'<<ALLTRIM(myCxStamp)>>', '<<ALLTRIM(mySsStamp)>>', '<<ALLTRIM(mySite)>>'
				,<<IIF(Round(REGVENDAS.ctnPagamento.txtValAtend.value,2) == 0 OR !EMPTY(lcDeveFechar), "1", "0")>>
				,<<lcModPag3>>
				,<<lcModPag4>>
				,<<lcModPag5>>
				,<<lcModPag6>>
				, dateadd(HOUR, <<difhoraria>>, getdate())
				, dateadd(HOUR, <<difhoraria>>, getdate())
				, '<<ALLTRIM(lcFechaStamp)>>'
			)
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
       uf_perguntalt_chama("OCORREU UM PROBLEMA A REGISTAR VALORES PARA PAGAMENTO CENTRAL. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
       uf_registaerrolog('Erro a Registar Valores Pagamento Central', 'uf_regVendas_GravarRegCredito')
    ENDIF
    LOCAL lcdinheiropc
    lcdinheiropc = VAL(regvendas.ctnpagamento.txtdinheiro.value)
    
    uf_gerais_regista_logs_passos("B_PAGCENTRAL",nrAtendimento,"b_pag1","update antes uf_regvendas_gravarregcredito")
    
    IF uf_gerais_getparameter_site('ADM0000000041', 'BOOL', mysite)=.T. .AND. lcdinheiropc>0
       lcsqlpagcentralfechado = ''
       TEXT TO lcsqlpagcentralfechado TEXTMERGE NOSHOW
				update b_pagcentral 
				set fechado=(select ISNULL(fechado,0) from B_pagCentral_maquinas (nolock) where B_pagCentral_maquinas.nratend='<<ALLTRIM(lcStampATMaq)>>') 
				where nrAtend='<<ALLTRIM(nrAtendimento)>>'
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "", lcsqlpagcentralfechado)
          uf_perguntalt_chama("OCORREU UM PROBLEMA A FECHAR OS VALORES NO PAGAMENTO CENTRAL. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
          uf_registaerrolog('Erro a Registar Valores Pagamento Central', 'uf_regVendas_GravarRegCredito')
       ENDIF
    ENDIF
    
    uf_gerais_regista_logs_passos("B_PAGCENTRAL",nrAtendimento,"b_pag1","update depois uf_regvendas_gravarregcredito")
    
 ELSE
    lcstamp = uf_gerais_stamp()
    TEXT TO lcsql TEXTMERGE NOSHOW
			INSERT into B_ocorrencias
				(stamp, linkstamp, tipo, grau, descr,
				ovalor, dvalor,
				usr, date, site, terminal)
			values
				('<<alltrim(lcStamp)>>', '<<nrAtendimento>>', 'Recibo - BackOffice', 3, 'Recibo efetuado em BackOffice sem movimentar caixa',
				'Recibo N� - ' + (select top 1 convert(varchar,rno) from re (nolock) where u_nratend = '<<ALLTRIM(nrAtendimento)>>' order by rdata desc, ousrhora desc), '<<astr((lcDinheiroPc + lcMbPc + lcVisaPc + lcChequePc),8,2)>>',
				<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()), '<<mySite>>', '<<myTermNo>>')
    ENDTEXT
    uf_gerais_actgrelha("", "", lcsql)
 ENDIF
 
 uf_gerais_regista_logs_passos("B_PAGCENTRAL",nrAtendimento,"b_pag1","insert depois uf_regvendas_gravarregcredito")
 
 talaomaqdinheiro = .F.
 nratendimento = ''
 uf_atendimento_geranratendimento()
 IF TYPE("UtilPlafond")<>"U"
    utilplafond = .F.
 ENDIF
 IF TYPE("atendimento")<>"U"
    uf_regvendas_sair()
    uf_pagamento_limparvendasfimvd()
 ELSE
    regvendas.pgfreg.page2.txtdatarec.value = uf_gerais_getdate(DATE())
    regvendas.menu1.actualizar.click
 ENDIF
ENDFUNC
**
FUNCTION uf_regvendas_emitirrecibos
 LPARAMETERS lcclno, lcclestab, lcvalidaestab
 
 IF USED("uCrsFi2Reg")
   FECHA("uCrsFi2Reg")
 ENDIF

 PUBLIC myinvoicedata, lcnrrecibo
 STORE 0 TO lcnrrecibo
 LOCAL lchora, lcrdata, lcbackoff
 STORE .F. TO lcbackoff
 SET HOURS TO 24
 atime = TTOC(DATETIME()+(difhoraria*3600), 2)
 astr = LEFT(atime, 8)
 lchora = astr
 &&::TODO
 myinvoicedata = uf_gerais_getdate(regvendas.pgfreg.page2.txtdatarec.value, "SQL")
 LOCAL lcincluilinha
 STORE .T. TO lcincluilinha
 IF regvendas.pgfreg.page2.chkporproduto.tag=="true"
    SELECT ucrsregcreditos
    GOTO TOP
    SCAN FOR ucrsregcreditos.sel==.T.
       IF lcvalidaestab
          IF ucrsregcreditos.no==lcclno .AND. ucrsregcreditos.estab==lcclestab
             lcincluilinha = .T.
          ELSE
             lcincluilinha = .F.
          ENDIF
       ENDIF
       IF lcincluilinha
          uf_gerais_actgrelha("", 'uCrsFi', [select *, 0 as qttrobot from fi (nolock) where fistamp=']+ALLTRIM(ucrsregcreditos.fistamp)+['])
          SELECT ucrsfi
          REPLACE ucrsfi.qtt WITH ucrsregcreditos.qtt-ucrsregcreditos.eaquisicao
          REPLACE ucrsfi.etiliquido WITH ucrsregcreditos.etiliquido
          REPLACE ucrsfi.tiliquido WITH ucrsregcreditos.etiliquido*200.482 
          REPLACE ucrsfi.esltt WITH ucrsregcreditos.etiliquido
          REPLACE ucrsfi.sltt WITH ucrsregcreditos.etiliquido*200.482 
          REPLACE ucrsfi.tliquido WITH ucrsregcreditos.etiliquido
          REPLACE ucrsfi.fmarcada WITH ucrsregcreditos.fmarcada
          SELECT ucrsfi
          GOTO TOP
          IF  .NOT. USED("uCrsFi2Reg")
             SELECT *, .F. as isAcerto FROM uCrsFi WHERE 1=0 INTO CURSOR uCrsFi2Reg READWRITE
          ENDIF
          SELECT ucrsfi2reg
          APPEND FROM DBF("uCrsFi")
          fecha("uCrsFi")
       ENDIF
       SELECT ucrsregcreditos
    ENDSCAN
    lcbackoff = .F.
 ELSE
    SELECT ucrsregcreditosvalor
    GOTO TOP
    SCAN FOR ucrsregcreditosvalor.sel==.T.
       IF lcvalidaestab
          IF ucrsregcreditosvalor.no==lcclno .AND. ucrsregcreditosvalor.estab==lcclestab
             lcincluilinha = .T.
          ELSE
             lcincluilinha = .F.
          ENDIF
       ENDIF
       IF lcincluilinha
          IF  .NOT. USED("uCrsFi2Reg")
             CREATE CURSOR uCrsFi2Reg (etiliquido N(19, 6), fno N(10), ftstamp C(25), rdata D, ndoc N(15), valornreg N(19, 6), ccstamp C(25), qttrobot N(5, 0), isAcerto L)
          ENDIF

          uv_newStamp = uf_gerais_stamp()

          SELECT ucrsfi2reg
          APPEND BLANK
          REPLACE ucrsfi2reg.etiliquido WITH ucrsregcreditosvalor.regularizar
          REPLACE ucrsfi2reg.fno WITH ucrsregcreditosvalor.nrdoc
          REPLACE ucrsfi2reg.ftstamp WITH IIF(!EMPTY(ucrsregcreditosvalor.ftstamp), ALLTRIM(ucrsregcreditosvalor.ftstamp), IIF( ALLTRIM(UPPER(ucrsregcreditosvalor.nmdoc)) == "ACERTO CONTA CORRENTE", uv_newStamp,''))
          REPLACE ucrsfi2reg.ccstamp WITH ucrsregcreditosvalor.ccstamp
          REPLACE ucrsfi2reg.rdata WITH CTOD(ucrsregcreditosvalor.datalc)
          REPLACE ucrsfi2reg.ndoc WITH ucrsregcreditosvalor.ndoc
          REPLACE ucrsfi2reg.valornreg WITH ucrsregcreditosvalor.valornreg
          REPLACE ucrsfi2reg.isAcerto WITH IIF(ALLTRIM(UPPER(ucrsregcreditosvalor.nmdoc)) == "ACERTO CONTA CORRENTE", .T., .F.)
       ENDIF
    ENDSCAN
    lcbackoff = .T.
 ENDIF
 IF USED("uCrsFi2Reg")
    IF  .NOT. RECCOUNT("uCrsFi2Reg")>0
       RETURN .F.
    ENDIF
 ELSE
    RETURN .F.
 ENDIF
 lcrestamp = uf_gerais_stamp()
 IF  .NOT. USED("uCrsImpTalaoPos")
    CREATE CURSOR uCrsImpTalaoPos (stamp C(26), tipo I(4), lordem I(8), susp L, adireserva L, nrreceita C(26), rm L, planocomp L, impcomp1 L, impcomp2 L, lote C(5), impresso L , plano c(5))
 ELSE
    SELECT ucrsimptalaopos
    DELETE ALL
 ENDIF
 SELECT ucrsimptalaopos
 APPEND BLANK
 REPLACE ucrsimptalaopos.stamp WITH ALLTRIM(lcrestamp)
 IF myoffline
    REPLACE ucrsimptalaopos.tipo WITH 904
 ELSE
    REPLACE ucrsimptalaopos.tipo WITH 901
 ENDIF
 REPLACE ucrsimptalaopos.lordem WITH 99999
 REPLACE ucrsimptalaopos.susp WITH .F.
 SELECT ucrse1
 LOCAL lcrdoc, lcrmdoc, lcfno
 STORE 0 TO lcrdoc, lcfno
 STORE '' TO lcrmdoc
 IF  .NOT. uf_gerais_getparameter("ADM0000000308", "BOOL")
    IF myoffline
       lcrdoc = 4
       lcrmdoc = "Normal Offline"
    ELSE
       lcrdoc = 1
       lcrmdoc = "Normal"
    ENDIF
    IF regvendas.pgfreg.page2.chkrecent.tag=="true"
       lcrdoc = 6
       lcrmdoc = "Entidades"
    ENDIF
 ELSE
    TEXT TO lcsql TEXTMERGE NOSHOW
			select nmdoc, ndoc from tsre (nolock) where site='<<ALLTRIM(mySite)>>' and left(nmdoc,4)='Rec.'
    ENDTEXT
    uf_gerais_actgrelha("", "uCrsConfRec", lcsql)
    SELECT ucrsconfrec
    lcrdoc = ucrsconfrec.ndoc
    lcrmdoc = ALLTRIM(ucrsconfrec.nmdoc)
    fecha("uCrsConfRec")
 ENDIF
 IF lcrdoc = 0
    uf_perguntalt_chama("N�o foi poss�vel encontrar a configura��o do Recibo. Por favor contacte o Suporte.", "OK", "", 16)
    RETURN .F.
 ENDIF

 IF uf_gerais_getDate(regvendas.pgfreg.page2.txtDataRec.value, "SQL") > uf_gerais_getDate(DATE(), "SQL")
	uf_perguntalt_chama("ATEN��O: n�o pode criar um recibo com data superior � atual.", "OK", "", 64)
   RETURN .F.
 ENDIF

 IF uf_gerais_getUmValor("tsre", "ISNULL(valDataUltRec, 1)", "ndoc = " +  ASTR(lcrdoc))

   IF uf_gerais_getUmValor("re", "count(*)", "site = '" + ALLTRIM(mySite) + "' and ndoc = " + ASTR(lcrdoc) + " and rdata > '" + uf_gerais_getDate(regvendas.pgfreg.page2.txtDataRec.value, "SQL") + "'") > 0
      uf_perguntalt_chama("ATEN��O: existe um recibo com data superior � data selecionada.", "OK", "", 64)
      RETURN .F.
   ENDIF

 ENDIF

 LOCAL lcetotal, lctotcdesc, lctotcdescperc, lctotcdescvalor
 LOCAL lceivav1, lceivav2, lceivav3, lceivav4, lceivav5, lceivav6, lceivav7, lceivav8, lceivav9, lceivav10, lceivav11, lceivav12, lceivav13
 STORE 0 TO lcetotal, lctotcdesc, lctotcdescperc, lctotcdescvalor
 STORE 0 TO lceivav1, lceivav2, lceivav3, lceivav4, lceivav5, lceivav6, lceivav7, lceivav8, lceivav9, lceivav10, lceivav11, lceivav12, lceivav13
 SELECT ucrsfi2reg
 GOTO TOP
 SCAN
    lcetotal = lcetotal+ucrsfi2reg.etiliquido
    lctotcdesc = lctotcdesc+ucrsfi2reg.etiliquido-(ucrsfi2reg.etiliquido*(VAL(regvendas.pgfreg.page2.txtdescperc.value)/100))
    SELECT ucrsfi2reg
 ENDSCAN
 IF  .NOT. EMPTY(regvendas.pgfreg.page2.txtdesceuro.value)
    lctotcdescvalor = lcetotal-lctotcdesc
    lctotcdescperc = VAL(regvendas.pgfreg.page2.txtdescperc.value)
 ENDIF
 LOCAL lctroco, lcdinheiro, lcmb, lcvisa, lccheque
 STORE 0 TO lctroco, lcdinheiro, lcmb, lcvisa, lccheque
 IF  .NOT. EMPTY(regvendas.ctnpagamento.txttroco.value)
    lctroco = regvendas.ctnpagamento.txttroco.value
    SELECT ft2
    REPLACE ft2.etroco WITH lctroco
 ENDIF
 IF  .NOT. EMPTY(regvendas.ctnpagamento.txtdinheiro.value)
    lcdinheiro = VAL(regvendas.ctnpagamento.txtdinheiro.value)
    IF lctroco<>0
       lcdinheiro = lcdinheiro-lctroco
    ENDIF
    SELECT ft2
    REPLACE ft2.evdinheiro WITH lcdinheiro
 ENDIF
 IF  .NOT. EMPTY(regvendas.ctnpagamento.txtmb.value)
    lcmb = VAL(regvendas.ctnpagamento.txtmb.value)
    SELECT ft2
    REPLACE ft2.epaga2 WITH lcmb
 ENDIF
 IF  .NOT. EMPTY(regvendas.ctnpagamento.txtvisa.value)
    lcvisa = VAL(regvendas.ctnpagamento.txtvisa.value)
    SELECT ft2
    REPLACE ft2.epaga1 WITH lcvisa
 ENDIF
 IF  .NOT. EMPTY(regvendas.ctnpagamento.txtcheque.value)
    lccheque = VAL(regvendas.ctnpagamento.txtcheque.value)
    SELECT ft
    REPLACE ft.echtotal WITH lccheque
    REPLACE ft.chtotal WITH lccheque*200.482 
 ENDIF
 

*!*	  IF (VARTYPE(mytpastamp)<>"U" .AND. lcmb>0)
*!*	    IF ( .NOT. uf_pagamento_pagatpa(lcmb))
*!*	       RETURN .F.
*!*	    ENDIF
*!*	 ENDIF


 LOCAL lcolcodigo, lcollocal, lccontado
 lcolcodigo = ALLTRIM(regvendas.pgfreg.page2.txtolcodigo.value)
 lcollocal = ALLTRIM(regvendas.pgfreg.page2.txtollocal.value)
 IF EMPTY(regvendas.contado)
    lccontado = 99
 ELSE
    lccontado = regvendas.contado
 ENDIF
 lcexecutesqlre = ""
 IF regvendas.pgfreg.page2.chkrecent.tag=="false"
    IF myoffline
       lcsqlre = uf_pagamento_gravarregre(4, lcrestamp, lcrmdoc, lcrdoc, lcfno, ROUND(lctotcdesc, 2), lchora, ROUND(lctotcdescperc, 2), ROUND(lctotcdescvalor, 2), .F., lcbackoff, lcolcodigo, lcollocal, lccontado)
       lcsqlre = uf_gerais_trataplicassql(lcsqlre)
       lcexecutesqlre = lcexecutesqlre+CHR(13)+lcsqlre
    ELSE
       lcsqlre = uf_pagamento_gravarregre(1, lcrestamp, lcrmdoc, lcrdoc, lcfno, ROUND(lctotcdesc, 2), lchora, ROUND(lctotcdescperc, 2), ROUND(lctotcdescvalor, 2), .F., lcbackoff, lcolcodigo, lcollocal, lccontado)
       lcsqlre = uf_gerais_trataplicassql(lcsqlre)
       lcexecutesqlre = lcexecutesqlre+CHR(13)+lcsqlre
    ENDIF
 ELSE
    lcsqlre = uf_pagamento_gravarregre(6, lcrestamp, 'Entidades', 6, lcfno, ROUND(lctotcdesc, 2), lchora, ROUND(lctotcdescperc, 2), ROUND(lctotcdescvalor, 2), .F., lcbackoff, lcolcodigo, lcollocal, lccontado)
    lcsqlre = uf_gerais_trataplicassql(lcsqlre)
    lcexecutesqlre = lcexecutesqlre+CHR(13)+lcsqlre
 ENDIF
 IF USED("uCrsActAcerto")
    fecha("uCrsActAcerto")
 ENDIF
 uf_gerais_gravaultregisto(STR(mytermno), LEFT(nratendimento, 8))
 STORE 0 TO lcetotal
 STORE 0 TO lceivav1, lceivav2, lceivav3, lceivav4, lceivav5, lceivav6, lceivav7, lceivav8, lceivav9
 LOCAL lcvalstamp1, lcperc, lceval
 STORE '' TO lcvalstamp1
 STORE 0 TO lcperc, lceval
 SELECT ucrsfi2reg
 SELECT DISTINCT ftstamp FROM uCrsFi2Reg INTO CURSOR uCrsFi2RegStamps READWRITE
 SELECT ucrsfi2regstamps
 GOTO TOP
 SCAN
    SELECT ucrsfi2reg
    GOTO TOP
    SCAN FOR ALLTRIM(ucrsfi2reg.ftstamp)==ALLTRIM(ucrsfi2regstamps.ftstamp)
       lcetotal = lcetotal+ucrsfi2reg.etiliquido
       lceval = lcetotal
       IF regvendas.pgfreg.page2.chkporproduto.tag=="true"
       		 TEXT TO lcsql TEXTMERGE NOSHOW
					select eivav1, eivav2, eivav3, eivav4, eivav5, eivav6, eivav7, eivav8, eivav9, eivav10, eivav11, eivav12, eivav13  from ft(nolock) where ftstamp='<<alltrim(uCrsFi2Reg.ftstamp)>>'
	          ENDTEXT
	          IF  .NOT. uf_gerais_actgrelha("", "ucrsivafi2reg", lcsql)
	             uf_perguntalt_chama("Ocorreu uma anomalia recolher os valores do IVA do documento de fatura��o. Por favor contacte o Suporte.", "OK", "", 16)
	          ENDIF
	          SELECT ucrsivafi2reg
              lceivav1 = ucrsivafi2reg.eivav1
              lceivav2 = ucrsivafi2reg.eivav2
              lceivav3 = ucrsivafi2reg.eivav3
              lceivav4 = ucrsivafi2reg.eivav4
              lceivav5 = ucrsivafi2reg.eivav5
              lceivav6 = ucrsivafi2reg.eivav6
              lceivav7 = ucrsivafi2reg.eivav7
              lceivav8 = ucrsivafi2reg.eivav8
              lceivav9 = ucrsivafi2reg.eivav9
              lceivav10 = ucrsivafi2reg.eivav10
              lceivav11 = ucrsivafi2reg.eivav11
              lceivav12 = ucrsivafi2reg.eivav12
              lceivav13 = ucrsivafi2reg.eivav13
              fecha("ucrsivafi2reg")
              
              SELECT uCrsFi2Reg
          	  TEXT TO lcsql TEXTMERGE NOSHOW
					update fi set fmarcada=1, eaquisicao=eaquisicao+<<uCrsFi2Reg.qtt>>
					where fistamp='<<alltrim(uCrsFi2Reg.fistamp)>>'
	          ENDTEXT
    	      IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
        	     uf_perguntalt_chama("Ocorreu uma anomalia a marcar os produtos do documento como recibado. Por favor contacte o Suporte.", "OK", "", 16)
            	 uf_registaerrolog('Erro a marcar o Produto como Recibado', 'Regularizar Facturas')
	          ENDIF

*!*	          DO CASE
*!*	             CASE ucrsfi2reg.tabiva==1
*!*	                lceivav1 = lceivav1+ucrsfi2reg.etiliquido-(ucrsfi2reg.etiliquido/(ucrsfi2reg.iva/100+1))
*!*	             CASE ucrsfi2reg.tabiva==2
*!*	                lceivav2 = lceivav2+ucrsfi2reg.etiliquido-(ucrsfi2reg.etiliquido/(ucrsfi2reg.iva/100+1))
*!*	             CASE ucrsfi2reg.tabiva==3
*!*	                lceivav3 = lceivav3+ucrsfi2reg.etiliquido-(ucrsfi2reg.etiliquido/(ucrsfi2reg.iva/100+1))
*!*	             CASE ucrsfi2reg.tabiva==4
*!*	                lceivav4 = lceivav4+ucrsfi2reg.etiliquido-(ucrsfi2reg.etiliquido/(ucrsfi2reg.iva/100+1))
*!*	             CASE ucrsfi2reg.tabiva==5
*!*	                lceivav5 = lceivav5+ucrsfi2reg.etiliquido-(ucrsfi2reg.etiliquido/(ucrsfi2reg.iva/100+1))
*!*	             CASE ucrsfi2reg.tabiva==6
*!*	                lceivav6 = lceivav6+ucrsfi2reg.etiliquido-(ucrsfi2reg.etiliquido/(ucrsfi2reg.iva/100+1))
*!*	             CASE ucrsfi2reg.tabiva==7
*!*	                lceivav7 = lceivav7+ucrsfi2reg.etiliquido-(ucrsfi2reg.etiliquido/(ucrsfi2reg.iva/100+1))
*!*	             CASE ucrsfi2reg.tabiva==8
*!*	                lceivav8 = lceivav8+ucrsfi2reg.etiliquido-(ucrsfi2reg.etiliquido/(ucrsfi2reg.iva/100+1))
*!*	             CASE ucrsfi2reg.tabiva==9
*!*	                lceivav9 = lceivav9+ucrsfi2reg.etiliquido-(ucrsfi2reg.etiliquido/(ucrsfi2reg.iva/100+1))
*!*	          ENDCASE
*!*	          TEXT TO lcsql TEXTMERGE NOSHOW
*!*						update fi set fmarcada=1, eaquisicao=eaquisicao+<<uCrsFi2Reg.qtt>>
*!*						where fistamp='<<alltrim(uCrsFi2Reg.fistamp)>>'
*!*	          ENDTEXT
*!*	          IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
*!*	             uf_perguntalt_chama("Ocorreu uma anomalia a marcar o produto como recibado. Por favor contacte o Suporte.", "OK", "", 16)
*!*	             uf_registaerrolog('Erro a marcar o Produto como Recibado', 'Regularizar Facturas')
*!*	          ENDIF
       ELSE
          IF  .NOT. EMPTY(ucrsfi2reg.ftstamp) AND !ucrsfi2reg.isAcerto
             uf_gerais_actgrelha("", "uCrsCalcIva", "select eivav1,eivav2,eivav3,eivav4,eivav5,eivav6,eivav7,eivav8,eivav9,eivav10,eivav11,eivav12,eivav13,etotal from ft (nolock) where ftstamp = '"+ALLTRIM(ucrsfi2reg.ftstamp)+"'")
             SELECT ucrscalciva
             lcperc = (ucrsfi2reg.etiliquido*100)/ucrscalciva.etotal
             lceivav1 = ucrscalciva.eivav1*(lcperc/100)
             lceivav2 = ucrscalciva.eivav2*(lcperc/100)
             lceivav3 = ucrscalciva.eivav3*(lcperc/100)
             lceivav4 = ucrscalciva.eivav4*(lcperc/100)
             lceivav5 = ucrscalciva.eivav5*(lcperc/100)
             lceivav6 = ucrscalciva.eivav6*(lcperc/100)
             lceivav7 = ucrscalciva.eivav7*(lcperc/100)
             lceivav8 = ucrscalciva.eivav8*(lcperc/100)
             lceivav9 = ucrscalciva.eivav9*(lcperc/100)
             lceivav10 = ucrscalciva.eivav10*(lcperc/100)
             lceivav11 = ucrscalciva.eivav11*(lcperc/100)
             lceivav12 = ucrscalciva.eivav12*(lcperc/100)
             lceivav13 = ucrscalciva.eivav13*(lcperc/100)
             lceval = ucrsfi2reg.valornreg
             fecha("uCrsCalcIva")
             
             TEXT TO lcsql TEXTMERGE NOSHOW
						update 
							fi 
						set 
							fmarcada=1
							,eaquisicao=qtt
						where 
							ftstamp='<<alltrim(uCrsFi2Reg.ftstamp)>>'
             ENDTEXT
             IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
                uf_perguntalt_chama("Ocorreu uma anomalia a marcar a produto como recibado. Por favor contacte o Suporte.", "OK", "", 16)
                uf_registaerrolog('Erro a marcar o Produto como Recibado', 'Regularizar Facturas')
             ENDIF
          ELSE
             uf_gerais_actgrelha("", "uCrsCalcIva", "select eivav1,eivav2,eivav3,eivav4,eivav5,eivav6,eivav7,eivav8,eivav9,eivav10,eivav11,eivav12,eivav13  from cc (nolock) where ccstamp = '"+ALLTRIM(ucrsfi2reg.ccstamp)+"'")
             SELECT ucrscalciva
             lceivav1 = ucrscalciva.eivav1*(lcperc/100)
             lceivav2 = ucrscalciva.eivav2*(lcperc/100)
             lceivav3 = ucrscalciva.eivav3*(lcperc/100)
             lceivav4 = ucrscalciva.eivav4*(lcperc/100)
             lceivav5 = ucrscalciva.eivav5*(lcperc/100)
             lceivav6 = ucrscalciva.eivav6*(lcperc/100)
             lceivav7 = ucrscalciva.eivav7*(lcperc/100)
             lceivav8 = ucrscalciva.eivav8*(lcperc/100)
             lceivav9 = ucrscalciva.eivav9*(lcperc/100)
             lceivav10 = ucrscalciva.eivav10*(lcperc/100)
             lceivav11 = ucrscalciva.eivav11*(lcperc/100)
             lceivav12 = ucrscalciva.eivav12*(lcperc/100)
             lceivav13 = ucrscalciva.eivav13*(lcperc/100)
             fecha("uCrsCalcIva")
          ENDIF
       ENDIF
       IF  .NOT. EMPTY(ucrsfi2reg.ftstamp) AND !ucrsfi2reg.isAcerto
          lcvalstamp1 = ucrsfi2reg.ftstamp
       ELSE
          lcvalstamp1 = ucrsfi2reg.ccstamp
       ENDIF
       lcfno = ucrsfi2reg.fno
       lcrdata = uf_gerais_getdate(ucrsfi2reg.rdata, "SQL")
       SELECT ucrsfi2reg
    ENDSCAN
    SELECT ucrsfi2reg
    LOCATE FOR ALLTRIM(ucrsfi2reg.ftstamp)==ALLTRIM(ucrsfi2regstamps.ftstamp)
    IF  .NOT. EMPTY(lcvalstamp1)
       IF regvendas.pgfreg.page2.chkrecent.tag=="false"
          IF myoffline
             SELECT ucrsfi2reg
             SELECT ucrsgeraltd
             LOCATE FOR ucrsgeraltd.ndoc==ucrsfi2reg.ndoc
             IF FOUND()
                IF ucrsgeraltd.u_tipodoc==7
 
                   lcsqlrl = uf_pagamento_gravarregrl(5, lcrestamp, lcvalstamp1, lcrdoc, lcnrrecibo, lcfno, ROUND(lcetotal, 2), ROUND(lceivav1, 2), ROUND(lceivav2, 2), ROUND(lceivav3, 2), ROUND(lceivav4, 2), ROUND(lceivav5, 2), ROUND(lceivav6, 2), ROUND(lceivav7, 2), ROUND(lceivav8, 2), ROUND(lceivav9, 2), lchora, lcrdata, ROUND(lceval, 2),ROUND(lceivav10, 2), ROUND(lceivav11, 2),ROUND(lceivav12, 2),ROUND(lceivav13, 2))
                   lcsqlrl = uf_gerais_trataplicassql(lcsqlrl)
                   lcexecutesqlre = lcexecutesqlre+CHR(13)+lcsqlrl
                ELSE
                  
                   lcsqlrl = uf_pagamento_gravarregrl(4, lcrestamp, lcvalstamp1, lcrdoc, lcnrrecibo, lcfno, ROUND(lcetotal, 2), ROUND(lceivav1, 2), ROUND(lceivav2, 2), ROUND(lceivav3, 2), ROUND(lceivav4, 2), ROUND(lceivav5, 2), ROUND(lceivav6, 2), ROUND(lceivav7, 2), ROUND(lceivav8, 2), ROUND(lceivav9, 2), lchora, lcrdata, ROUND(lceval, 2),ROUND(lceivav10, 2), ROUND(lceivav11, 2), ROUND(lceivav12, 2),ROUND(lceivav13, 2))
                   lcsqlrl = uf_gerais_trataplicassql(lcsqlrl)
                   lcexecutesqlre = lcexecutesqlre+CHR(13)+lcsqlrl
                ENDIF
             ELSE
           
                lcsqlrl = uf_pagamento_gravarregrl(4, lcrestamp, lcvalstamp1, lcrdoc, lcnrrecibo, lcfno, ROUND(lcetotal, 2), ROUND(lceivav1, 2), ROUND(lceivav2, 2), ROUND(lceivav3, 2), ROUND(lceivav4, 2), ROUND(lceivav5, 2), ROUND(lceivav6, 2), ROUND(lceivav7, 2), ROUND(lceivav8, 2), ROUND(lceivav9, 2), lchora, lcrdata, ROUND(lceval, 2),ROUND(lceivav10, 2),ROUND(lceivav11, 2),ROUND(lceivav12, 2),ROUND(lceivav13, 2))
                lcsqlrl = uf_gerais_trataplicassql(lcsqlrl)
                lcexecutesqlre = lcexecutesqlre+CHR(13)+lcsqlrl
             ENDIF
          ELSE
             SELECT ucrsfi2reg
             SELECT ucrsgeraltd
             LOCATE FOR ucrsgeraltd.ndoc==ucrsfi2reg.ndoc
             IF FOUND()
                IF ucrsgeraltd.u_tipodoc==7
               
                   lcsqlrl = uf_pagamento_gravarregrl(3, lcrestamp, lcvalstamp1, lcrdoc, lcnrrecibo, lcfno, ROUND(lcetotal, 2), ROUND(lceivav1, 2), ROUND(lceivav2, 2), ROUND(lceivav3, 2), ROUND(lceivav4, 2), ROUND(lceivav5, 2), ROUND(lceivav6, 2), ROUND(lceivav7, 2), ROUND(lceivav8, 2), ROUND(lceivav9, 2), lchora, lcrdata, ROUND(lceval, 2), ROUND(lceivav10, 2), ROUND(lceivav11, 2),  ROUND(lceivav12, 2), ROUND(lceivav13, 2))
                   lcsqlrl = uf_gerais_trataplicassql(lcsqlrl)
                   lcexecutesqlre = lcexecutesqlre+CHR(13)+lcsqlrl
                ELSE
                  
                   lcsqlrl = uf_pagamento_gravarregrl(1, lcrestamp, lcvalstamp1, lcrdoc, lcnrrecibo, lcfno, ROUND(lcetotal, 2), ROUND(lceivav1, 2), ROUND(lceivav2, 2), ROUND(lceivav3, 2), ROUND(lceivav4, 2), ROUND(lceivav5, 2), ROUND(lceivav6, 2), ROUND(lceivav7, 2), ROUND(lceivav8, 2), ROUND(lceivav9, 2), lchora, lcrdata, ROUND(lceval, 2),ROUND(lceivav10, 2), ROUND(lceivav11, 2), ROUND(lceivav12, 2), ROUND(lceivav13, 2))
                   lcsqlrl = uf_gerais_trataplicassql(lcsqlrl)
                   lcexecutesqlre = lcexecutesqlre+CHR(13)+lcsqlrl
                ENDIF
             ELSE
          
                lcsqlrl = uf_pagamento_gravarregrl(1, lcrestamp, lcvalstamp1, lcrdoc, lcnrrecibo, lcfno, ROUND(lcetotal, 2), ROUND(lceivav1, 2), ROUND(lceivav2, 2), ROUND(lceivav3, 2), ROUND(lceivav4, 2), ROUND(lceivav5, 2), ROUND(lceivav6, 2), ROUND(lceivav7, 2), ROUND(lceivav8, 2), ROUND(lceivav9, 2), lchora, lcrdata, ROUND(lceval, 2),ROUND(lceivav10, 2), ROUND(lceivav11, 2), ROUND(lceivav12, 2),ROUND(lceivav13, 2))
                lcsqlrl = uf_gerais_trataplicassql(lcsqlrl)
                lcexecutesqlre = lcexecutesqlre+CHR(13)+lcsqlrl
             ENDIF
          ENDIF
       ELSE
          SELECT ucrsfi2reg
          SELECT ucrsgeraltd
          LOCATE FOR ucrsgeraltd.ndoc==ucrsfi2reg.ndoc
          IF FOUND()
       
             lcsqlrl = uf_pagamento_gravarregrl(6, lcrestamp, lcvalstamp1, 6, lcnrrecibo, lcfno, ROUND(lcetotal, 2), ROUND(lceivav1, 2), ROUND(lceivav2, 2), ROUND(lceivav3, 2), ROUND(lceivav4, 2), ROUND(lceivav5, 2), ROUND(lceivav6, 2), ROUND(lceivav7, 2), ROUND(lceivav8, 2), ROUND(lceivav9, 2), lchora, lcrdata, ROUND(lceval, 2), ROUND(lceivav10, 2), ROUND(lceivav11, 2), ROUND(lceivav12, 2),ROUND(lceivav13, 2))
             lcsqlrl = uf_gerais_trataplicassql(lcsqlrl)
             lcexecutesqlre = lcexecutesqlre+CHR(13)+lcsqlrl
          ELSE
          	
             lcsqlrl = uf_pagamento_gravarregrl(6, lcrestamp, lcvalstamp1, 6, lcnrrecibo, lcfno, ROUND(lcetotal, 2), ROUND(lceivav1, 2), ROUND(lceivav2, 2), ROUND(lceivav3, 2), ROUND(lceivav4, 2), ROUND(lceivav5, 2), ROUND(lceivav6, 2), ROUND(lceivav7, 2), ROUND(lceivav8, 2), ROUND(lceivav9, 2), lchora, lcrdata, ROUND(lceval, 2), ROUND(lceivav10, 2), ROUND(lceivav11, 2), ROUND(lceivav12, 2),ROUND(lceivav13, 2))
             lcsqlrl = uf_gerais_trataplicassql(lcsqlrl)
             lcexecutesqlre = lcexecutesqlre+CHR(13)+lcsqlrl
          ENDIF
       ENDIF
       STORE 0 TO lcetotal, lceivav1, lceivav2, lceivav3, lceivav4, lceivav5, lceivav6, lceivav7, lceivav8, lceivav9, lceivav10, lceivav11, lceivav12, lceivav13, lcfno
       STORE "" TO lcvalstamp1
    ENDIF
    SELECT ucrsfi2regstamps
 ENDSCAN
 IF  .NOT. EMPTY(ALLTRIM(lcexecutesqlre))
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_gerais_execSql 'Atendimento - Recibos', 1,'<<lcExecuteSQLRe>>', '', '', '' , '', ''
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
       uf_perguntalt_chama("OCORREU UMA ANOMALIA AO GRAVAR RECIBOS ASSOCIADOS AO ATENDIMENTO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
       RETURN .F.
    ENDIF

   IF regvendas.pgfreg.page2.chkrecent.tag=="false"
      uf_gravarCert(lcrestamp, uf_gerais_getUmvalor("re", "rno", "restamp = '" + lcrestamp + "'"), lcrdoc, lcRdata, lcRdata, lcHora, uf_gerais_getUmvalor("re", "etotal", "restamp = '" + lcrestamp + "'"), 'RE')
   ENDIF 

 ENDIF
 SELECT ucrsfi2regstamps
 GOTO TOP
 SCAN
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_enc_getBostamp '<<ALLTRIM(uCrsFi2RegStamps.ftstamp)>>' 
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "ucrsInfoEnc", lcsql)
       uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR O STAMP DA ENCOMENDA ASSOCIADA � FATURA. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
       RETURN .F.
    ENDIF
    SELECT ucrsinfoenc
    IF RECCOUNT("ucrsInfoEnc")>0
       IF  .NOT. EMPTY(ucrsinfoenc.bostamp)
          LOCAL lcstampboencomenda
          lcstampboencomenda = ALLTRIM(ucrsinfoenc.bostamp)

          TEXT TO lcsql TEXTMERGE NOSHOW
					Update bo2 set status='Faturada_Recibada' where bo2stamp='<<ALLTRIM(lcStampBOEncomenda)>>'
					
					Update BO SET usrdata = dateadd(HOUR, <<difhoraria>>, getdate()),usrhora = convert(varchar,getdate(),8),usrinis =	'<<m_chinis>>' where bostamp='<<ALLTRIM(lcStampBOEncomenda)>>'
             
          ENDTEXT
          IF  .NOT. uf_gerais_actgrelha("", "ucrsInfoEnc", lcsql)
             uf_perguntalt_chama("OCORREU UMA ANOMALIA AO ATUALIZAR O STATUS DA ENCOMENDA ASSOCIADA � FATURA. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
             RETURN .F.
          ENDIF
       ENDIF
    ENDIF
    fecha("ucrsInfoEnc")
    SELECT ucrsfi2regstamps
 ENDSCAN
 IF USED("uCrsFi2RegStamps")
    fecha("uCrsFi2RegStamps")
 ENDIF
 IF  .NOT. uf_gerais_actgrelha("", "", [update re set restamp=restamp where restamp=']+ALLTRIM(lcrestamp)+['])
    uf_perguntalt_chama("Ocorreu uma anomalia a atualizar o recibo. Por favor contacte o suporte.", "OK", "", 16)
    uf_registaerrolog('Erro a Atualizar Recibo', 'uf_emitirRecibosRvReg')
 ENDIF
 IF regvendas.pgfreg.page2.chkporproduto.tag=="true"
    IF  .NOT. USED("uCrsFi2Lreg")
       SELECT * FROM uCrsFi2Reg INTO CURSOR uCrsFi2Lreg READWRITE
    ELSE
       SELECT ucrsfi2lreg
       APPEND FROM DBF("uCrsFi2Reg")
    ENDIF
 ELSE
    fecha("uCrsFi2Lreg")
 ENDIF
 LOCAL lcnrtaloes
 DO CASE
    CASE RIGHT(UPPER(ALLTRIM(regvendas.menu1.ntaloes.img.picture)), 7)==UPPER(ALLTRIM("0_w.png"))
       lcnrtaloes = 0
    CASE RIGHT(UPPER(ALLTRIM(regvendas.menu1.ntaloes.img.picture)), 7)==UPPER(ALLTRIM("1_w.png"))
       lcnrtaloes = 1
    CASE RIGHT(UPPER(ALLTRIM(regvendas.menu1.ntaloes.img.picture)), 7)==UPPER(ALLTRIM("2_w.png"))
       lcnrtaloes = 2
    CASE RIGHT(UPPER(ALLTRIM(regvendas.menu1.ntaloes.img.picture)), 7)==UPPER(ALLTRIM("3_w.png"))
       lcnrtaloes = 3
    OTHERWISE
       lcnrtaloes = 1
 ENDCASE
 FOR i = 1 TO lcnrtaloes
    IF myoffline
       IF  .NOT. uf_gerais_getparameter("ADM0000000126", "BOOL")
          uf_pagamento_imptalaorecpos(904)
       ENDIF
    ELSE
       IF  .NOT. uf_gerais_getparameter("ADM0000000126", "BOOL")
          uf_pagamento_imptalaorecpos(901)
       ELSE
          uf_pagamento_imptalaorecposa6(901)
       ENDIF
    ENDIF
 ENDFOR
 fecha("uCrsClReg")
 fecha("uCrsActAcerto")
 fecha("uCrsFi2Reg")
 fecha("uCrsFi2Lreg")
 fecha("uCrsImpTalaoPos")
 fecha("uCrsFt")
 fecha("uCrsFi")
 

ENDFUNC
**
PROCEDURE uf_regvendas_removervdreg
 LOCAL lctotal, lcqtttotal
 STORE 0 TO lctotal, lcqtttotal
 IF uf_perguntalt_chama("ATEN��O: VAI ELIMINAR TODOS OS PRODUTOS MARCADOS PARA REGULARIZA��O, TEM A CERTEZA QUE PRETENDE CONTINUAR?", "Sim", "N�o")
    CREATE CURSOR uCrsApagaStamps (lcstamp C(25), lcltstamp C(25))
    SELECT ucrsvale2
    GOTO TOP
    SCAN
       INSERT INTO uCrsApagaStamps (lcstamp, lcltstamp) VALUES (ucrsvale2.fistamp, ucrsvale2.u_ltstamp)
       DELETE
       SELECT ucrsvale2
    ENDSCAN
    SELECT ucrsapagastamps
    GOTO TOP
    SCAN
       SELECT fi
       GOTO TOP
       SCAN FOR (ALLTRIM(fi.ofistamp)==ALLTRIM(ucrsapagastamps.lcstamp)) .AND.  .NOT. (fi.lordem==10000)
          uf_atendimento_apagalinha(.T.)
       ENDSCAN
       SELECT ucrsapagastamps
    ENDSCAN
    SELECT ucrsapagastamps
    GOTO TOP
    SCAN
       SELECT fi
       GOTO TOP
       SCAN FOR (ALLTRIM(fi.ofistamp)==ALLTRIM(ucrsapagastamps.lcstamp)) .AND.  .NOT. (fi.lordem==10000)
          uf_atendimento_apagalinha(.T.)
       ENDSCAN
       SELECT ucrsapagastamps
    ENDSCAN
    SELECT ucrsapagastamps
    GOTO TOP
    SCAN
       SELECT fi
       GOTO TOP
       SCAN FOR ( .NOT. EMPTY(fi.tkhposlstamp)) .AND. (ALLTRIM(fi.tkhposlstamp)==ALLTRIM(ucrsapagastamps.lcltstamp))
          IF fi.lordem==10000
             REPLACE fi.tkhposlstamp WITH ''
             REPLACE fi.design WITH ".SEM RECEITA"
          ELSE
             uf_atendimento_apagalinha(.T.)
          ENDIF
       ENDSCAN
       SELECT ucrsapagastamps
    ENDSCAN
    IF USED("uCrsApagaStamps")
       fecha("uCrsApagaStamps")
    ENDIF
    SELECT ucrsvale2
    CALCULATE SUM(ucrsvale2.etiliquido), SUM(ucrsvale2.qtt) TO lctotal, lcqtttotal 
    regvendas.pgfreg.page1.txttotalreg.value = ROUND(lctotal, 2)
    regvendas.pgfreg.page1.txttotalqttreg.value = ROUND(lcqtttotal, 2)
    IF USED("dadospsico")
       fecha("dadospsico")
    ENDIF
    uf_atendimento_limpacab()
    uf_eventoactvalores()
    SELECT ucrsregvendas
    REPLACE ucrsregvendas.sel WITH .F. ALL
    GOTO TOP
 ENDIF
ENDPROC
**
PROCEDURE uf_regvendas_limparpesquisa
 regvendas.txtnvenda.value = ''
 regvendas.txtnatend.value = ''
 regvendas.txtcliente.value = ''
 regvendas.txtno.value = ''
 regvendas.txtestab.value = ''
 regvendas.txtproduto.value = ''
 IF YEAR(DATE()-(uf_gerais_getparameter('ADM0000000300', 'num')))>=1900
    regvendas.txtdataini.value = uf_gerais_getdate(DATETIME()+((difhoraria-(24*(uf_gerais_getparameter('ADM0000000300', 'num'))))*3600))
 ELSE
    regvendas.txtdataini.value = uf_gerais_getdate('19000101')
 ENDIF
 regvendas.txtdatafim.value = uf_gerais_getdate(DATETIME()+(difhoraria*3600))
ENDPROC
**
PROCEDURE uf_regvendas_imprimircreditos
 IF RECCOUNT("uCrsRegCreditos")>0
    LOCAL lcproduto, lcnvenda, lccliente, lcno, lcestab, lcnatend
    STORE '' TO lcproduto, lccliente, lcnatend
    STORE -1 TO lcnvenda, lcno, lcestab
    IF  .NOT. EMPTY(ALLTRIM(regvendas.txtproduto.value))
       lcproduto = ALLTRIM(regvendas.txtproduto.value)
    ELSE
       lcproduto = ''
    ENDIF
    IF  .NOT. EMPTY(astr(regvendas.txtnvenda.value))
       lcnvenda = ALLTRIM(STRTRAN(regvendas.txtnvenda.value, ' ', ''))
    ELSE
       lcnvenda = -1
    ENDIF
    IF  .NOT. EMPTY(ALLTRIM(regvendas.txtcliente.value))
       lccliente = ALLTRIM(regvendas.txtcliente.value)
    ELSE
       lccliente = ''
    ENDIF
    IF  .NOT. EMPTY(astr(regvendas.txtno.value))
       lcno = astr(regvendas.txtno.value)
    ELSE
       lcno = -1
    ENDIF
    IF  .NOT. EMPTY(astr(regvendas.txtestab.value))
       lcestab = astr(regvendas.txtestab.value)
    ELSE
       lcestab = -1
    ENDIF
    IF  .NOT. EMPTY(astr(regvendas.txtnatend.value))
       lcnatend = astr(regvendas.txtnatend.value)
    ELSE
       lcnatend = ''
    ENDIF
    IF regvendas.pgfreg.page2.chksosusp.tag="true"
       lcsosusp = 1
    ELSE
       lcsosusp = 0
    ENDIF
    IF TYPE("ATENDIMENTO")<>"U" .AND. regvendas.pgfreg.activepage==1
       IF regvendas.pgfreg.page2.chksores.tag="true"
          sores = 1
       ELSE
          sores = 0
       ENDIF
       sores = 0
    ENDIF
    IF regvendas.pgfreg.page2.chksonaosusp.tag="true"
       lcsonsusp = 1
    ELSE
       lcsonsusp = 0
    ENDIF
    LOCAL lcoutrasemp
    IF regvendas.pgfreg.page2.chkoutraslojas.tag="false"
       lcoutrasemp = 0
    ELSE
       lcoutrasemp = 1
    ENDIF
    parametros = ''
    TEXT TO parametros TEXTMERGE NOSHOW
			&produto=<<lcProduto>>&venda=<<lcNVenda>>&cliente=<<lcCliente>>&no=<<lcNo>>&estab=<<lcEstab>>&dataIni=<<uf_gerais_getdate(REGVENDAS.txtDataIni.value, "DATA")>>&dataFim=<<uf_gerais_getdate(REGVENDAS.txtDataFim.value, "DATA")>>&nrAtend=<<lcNAtend>>&user=<<ch_userno>>&group=<<Alltrim(ch_grupo)>>&site=<<ALLTRIM(mysite)>>&sosusp=<<lcSosusp>>&sonsusp=<<lcSonsusp>>&loja=<<mysite>>&ordem=0&MesmoNif=<<lcOutrasEmp>>
    ENDTEXT
    uf_gerais_chamareport("relatorio_vendas_regularizarcreditos", parametros)
 ENDIF
ENDPROC
**
PROCEDURE uf_regvendas_verificadatarecibo
 LPARAMETERS lcdata
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		select COUNT(rno) cont
		from re (nolock)
		where rdata > '<<lcData>>'
 ENDTEXT
 uf_gerais_actgrelha("", "uCrsContRE", lcsql)
 SELECT ucrscontre
 IF ucrscontre.cont>0
    **uf_perguntalt_chama("ATEN��O: existe um recibo com data superior � data seleccionada.", "OK", "", 64)
    RETURN .F.
 ENDIF
 fecha("uCrsContRE")
ENDPROC
**
PROCEDURE uf_regvendas_porvalor
 regvendas.pgfreg.page2.chkporproduto.tag = "false"
 regvendas.pgfreg.page2.chkporproduto.refresh()
 regvendas.pgfreg.page2.grdregcredito.visible = .F.
 regvendas.pgfreg.page2.grdregcreditovalor.visible = .T.
 regvendas.menu1.actualizar.click
ENDPROC
**
PROCEDURE uf_regvendas_porproduto
 regvendas.pgfreg.page2.chkporvalor.tag = "false"
 regvendas.pgfreg.page2.chkporvalor.refresh()
 regvendas.pgfreg.page2.grdregcredito.visible = .T.
 regvendas.pgfreg.page2.grdregcreditovalor.visible = .F.
 regvendas.menu1.actualizar.click
ENDPROC
**
PROCEDURE uf_regvendas_gerirrecibos
 LPARAMETERS lcbool
 regvendas.pgfreg.activepage = 4
 regvendas.menu1.estado("selTodos,nTaloes, recibos", "HIDE")
 regvendas.menu_opcoes.estado("meiosPagRec, impreg", "HIDE")
 regvendas.menu1.estado("fechar", "HIDE")
 regvendas.txtbtnsuspensas.visible = .F.
 regvendas.txtbtncreditos.visible = .F.
 regvendas.txtbtnreservas.visible = .F.
 regvendas.txtbtnencomendas.visible = .F.
 regvendas.btnsuspensas.visible = .F.
 regvendas.btncreditos.visible = .F.
 regvendas.btnreservas.visible = .F.
 regvendas.btnencomendas.visible = .F.
 IF TYPE("REGVENDAS.menu1.impTalRec")=="U"
    regvendas.menu1.adicionaopcao("impTalRec", "Imp. Tal�o", mypath+"\imagens\icons\imprimir_w.png", "uf_regVendas_impTalaoRec WITH .F.", "F3")
    regvendas.menu1.adicionaopcao("impA4Rec", "Imp. A4", mypath+"\imagens\icons\imprimir_w.png", "uf_regVendas_impA4Rec", "F4")
    regvendas.menu1.adicionaopcao("consultaRec", "Consultar", mypath+"\imagens\icons\doc_seta_w.png", "uf_regvendas_consultarRecibos with ''", "F5")
 ENDIF
 regvendas.menu1.estado("voltar,impTalRec,ImpA4Rec,consultaRec", "SHOW")
 regvendas.menu1.estado("", "", "Emitir Rec.", .F.)
 regvendas.menu1.actualizar.config("Actualizar", mypath+"\imagens\icons\actualizar_w.png", "uf_regvendas_pesquisarRecibos", "F2")
 regvendas.menu1.voltar.config("Voltar", mypath+"\imagens\icons\voltar_w.png", "uf_regVendas_voltarGerirRecibos", "F6")
 regvendas.caption = 'GEST�O DE RECIBOS'
 regvendas.ctnpagamento.visible = .F.
 IF  .NOT. lcbool
    regvendas.menu1.actualizar.click
 ENDIF
ENDPROC
**
FUNCTION uf_regvendas_imptalaorec
 LPARAMETERS pboolorigem
 IF  .NOT. USED("uCrsImpTalaoPos")
    CREATE CURSOR uCrsImpTalaoPos (stamp C(26), tipo I(4), lordem I(8), susp L, adireserva L, nrreceita C(26), rm L, planocomp L, impcomp1 L, impcomp2 L, lote C(5), impresso L, plano c(5))
 ELSE
    SELECT ucrsimptalaopos
    DELETE ALL
 ENDIF
 IF  .NOT. pboolorigem
    IF regvendas.pgfreg.page4.grdrecibos.visible==.T.
       SELECT ucrsrecibos
       LOCATE FOR ucrsrecibos.sel==.T.
       IF FOUND()
          SELECT ucrsimptalaopos
          APPEND BLANK
          REPLACE ucrsimptalaopos.stamp WITH ALLTRIM(ucrsrecibos.restamp)
       ELSE
          RETURN .F.
       ENDIF
    ELSE
       SELECT ucrsrecons
       IF  .NOT. EMPTY(ALLTRIM(ucrsrecons.restamp))
          SELECT ucrsimptalaopos
          APPEND BLANK
          REPLACE ucrsimptalaopos.stamp WITH ALLTRIM(ucrsrecons.restamp)
       ELSE
          RETURN .F.
       ENDIF
    ENDIF
 ELSE
    SELECT ucrspesqvendas
    SELECT ucrsimptalaopos
    APPEND BLANK
    REPLACE ucrsimptalaopos.stamp WITH ALLTRIM(ucrspesqvendas.ftstamp)
 ENDIF
 SELECT ucrsimptalaopos
 IF myoffline
    REPLACE ucrsimptalaopos.tipo WITH 904
 ELSE
    REPLACE ucrsimptalaopos.tipo WITH 901
 ENDIF
 REPLACE ucrsimptalaopos.lordem WITH 99999
 REPLACE ucrsimptalaopos.susp WITH .F.
 IF myoffline
    IF  .NOT. uf_gerais_getparameter("ADM0000000126", "BOOL")
       uf_pagamento_imptalaorecpos(904)
    ENDIF
 ELSE
    IF  .NOT. uf_gerais_getparameter("ADM0000000126", "BOOL")
       uf_pagamento_imptalaorecpos(901)
    ELSE
       uf_pagamento_imptalaorecposa6(901)
    ENDIF
 ENDIF
ENDFUNC
**
FUNCTION uf_regvendas_impa4rec
 IF regvendas.pgfreg.page4.grdrecibos.visible==.T.
    SELECT ucrsrecibos
    LOCATE FOR ucrsrecibos.sel==.T.
    IF FOUND()
       uf_imprimirgerais_chama("RECIBO")
    ELSE
       RETURN .F.
    ENDIF
 ELSE
    SELECT ucrsrecons
    IF  .NOT. EMPTY(ALLTRIM(ucrsrecons.restamp))
       uf_imprimirgerais_chama("RECIBO")
    ELSE
       RETURN .F.
    ENDIF
 ENDIF
ENDFUNC
**
PROCEDURE uf_regvendas_pesquisarrecibos
 regvendas.pgfreg.page4.ctnconsulta.visible = .F.
 regvendas.pgfreg.page4.grdrecibos.visible = .T.
  
 regua(0, 1, "A pesquisar...", .T.)
 LOCAL lcnvenda, lccliente, lcno, lcestab, lcnatend
 STORE '' TO lccliente, lcnatend
 STORE -1 TO lcnvenda, lcno, lcestab
 IF  .NOT. EMPTY(astr(regvendas.txtnvenda.value))
    lcnvenda = ALLTRIM(STRTRAN(regvendas.txtnvenda.value, ' ', ''))
 ELSE
    lcnvenda = -1
 ENDIF
 IF  .NOT. EMPTY(ALLTRIM(regvendas.txtcliente.value))
    lccliente = ALLTRIM(regvendas.txtcliente.value)
 ELSE
    lccliente = ''
 ENDIF
 IF  .NOT. EMPTY(astr(regvendas.txtno.value))
    lcno = astr(regvendas.txtno.value)
 ELSE
    lcno = -1
 ENDIF
 IF  .NOT. EMPTY(astr(regvendas.txtestab.value))
    lcestab = astr(regvendas.txtestab.value)
 ELSE
    lcestab = -1
 ENDIF
 IF  .NOT. EMPTY(astr(regvendas.txtnatend.value))
    lcnatend = astr(regvendas.txtnatend.value)
 ELSE
    lcnatend = ''
 ENDIF
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_touch_pesqRecibos <<lcNVenda>>, '<<lcCliente>>', <<lcNo>>, <<lcEstab>>, '<<uf_gerais_getdate(REGVENDAS.txtDataIni.value, "SQL")>>', '<<uf_gerais_getdate(REGVENDAS.txtDataFim.value, "SQL")>>', '<<lcNAtend>>', '<<mySite>>', '<<ALLTRIM(REGVENDAS.txtProduto.value)>>'
 ENDTEXT

 
  
 uf_gerais_actgrelha("REGVENDAS.pgfReg.page4.grdRecibos", "uCrsRecibos", lcsql)
 regua(2)
ENDPROC
**
PROCEDURE uf_regvendas_voltargerirrecibos
 IF TYPE("regVendas.pgfReg.Page4.ctnConsulta") <> "U"
	IF regVendas.pgfReg.Page4.ctnConsulta.visible

        uv_selectRno = 0

        IF USED("uCrsRlCons")
            SELECT uCrsRlCons
            uv_selectRno = uCrsRlCons.rno
        ENDIF

		uf_regvendas_gerirrecibos()

        IF uv_selectRno <> 0
            IF USED("uCrsRecibos")

                SELECT uCrsRecibos
                LOCATE FOR uCrsRecibos.rno == uv_selectRno

                IF FOUND()

                    SELECT uCrsRecibos
                    REPLACE uCrsRecibos.sel WITH .T.

                ENDIF

            ENDIF
        ENDIF

        return .t.
	ENDIF
ENDIF
 
 uf_regvendas_verregcreditos()
 regvendas.menu1.estado("voltar,impTalRec,ImpA4Rec,consultaRec", "HIDE")
 regvendas.menu1.estado("recibos", "SHOW")
ENDPROC
**
PROCEDURE uf_regvendas_selgrdrecibos
 LOCAL lcpos
 SELECT ucrsrecibos
 lcpos = RECNO("uCrsRecibos")
 SELECT ucrsrecibos
 REPLACE sel WITH .F. ALL
 SELECT ucrsrecibos
 TRY
    GOTO lcpos
    REPLACE ucrsrecibos.sel WITH .T.
 CATCH
 ENDTRY
ENDPROC
**
FUNCTION uf_regvendas_consultarrecibos
 LPARAMETERS lcrestamp

 IF TYPE("regVendas.pgfReg.Page4.ctnConsulta") <> "U"
	IF regVendas.pgfReg.Page4.ctnConsulta.visible
        IF USED("uCrsRlCons")
            IF EMPTY(uCrsRlCons.FTSTAMP)
                uf_perguntalt_chama("Esta linha de recibo n�o est� associada a nenhuma fatura!", "OK", "", 16)
                RETURN .T.
            ENDIF

            select uCrsRlCons

            uf_facturacao_chama(uCrsRlCons.ftstamp)

            regvendas.AlwaysOnTop = .f.

            facturacao.AlwaysOnTop  = .t.

            facturacao.AlwaysOnTop  = .f.

            RETURN .T.

        ENDIF
    ENDIF
 ENDIF


 IF TYPE("regvendas")=="U"
    uf_regvendas_chama(.T.)
    regvendas.menu1.estado("regcreditos, multiemp", "HIDE")
    IF TYPE("regVendas.menu1.recibos")=="U"
       regvendas.menu1.adicionaopcao("recibos", "Recibos", mypath+"\imagens\icons\doc_re_w.png", "uf_regvendas_gerirRecibos", "G")
       regvendas.menu1.estado("fechar", "HIDE")
    ENDIF
    uf_regvendas_gerirrecibos(.T.)
    regvendas.menu1.refresh()
    regvendas.windowstate = 0
    uf_gerais_maximizarJanela("REGVENDAS")
   ENDIF
 IF EMPTY(ALLTRIM(lcrestamp))
    IF USED("uCrsRecibos")
       SELECT ucrsrecibos
       LOCATE FOR ucrsrecibos.sel==.T.
       IF FOUND()
          lcrestamp = ALLTRIM(ucrsrecibos.restamp)
       ELSE
          RETURN .F.
       ENDIF
    ELSE
       RETURN .F.
    ENDIF
 ENDIF
 regvendas.pgfreg.activepage = 4
 regvendas.menu_opcoes.estado("meiosPagRec", "SHOW")
 regvendas.ctnpagamento.visible = .F.
 regvendas.pgfreg.page4.ctnconsulta.visible = .T.
 regvendas.pgfreg.page4.grdrecibos.visible = .F.
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		select 
			re.nmdoc
			,re.ndoc
			,rno
			,nome
			,no
			,estab
			,convert(varchar,rdata,102) rdata
			,moeda
			,fin
			,efinv
			,virs
			,etotal
			,olcodigo
			,ollocal
			,desc1
			,re.site
			,pnome
			,pno
			,vendnm
			,vendedor
			,evdinheiro
			,epaga2
			,epaga1
			,echtotal 
			,restamp
			,ISNULL(tsre.u_tipodoc,0) as u_tipoDoc
			
		from 
			re (nolock)
		left join tsre (nolock) on  re.ndoc = 	tsre.ndoc		 
		where
			restamp = '<<ALLTRIM(lcReStamp)>>'
 ENDTEXT
 uf_gerais_actgrelha("", "uCrsReCons", lcsql)
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		select 
			rno
			,cdesc
			,eval
			,erec
			,desconto 
			,nrdoc
            ,ISNULL((select ftstamp from cc(nolock) where cc.ccstamp = rl.ccstamp),'') as ftstamp
		from 
			rl (nolock) 
		where
			restamp = '<<ALLTRIM(lcReStamp)>>' order by datalc, cm, nrdoc
 ENDTEXT
 uf_gerais_actgrelha("regvendas.pgfReg.page4.ctnConsulta.grdLinhasRecibo", "uCrsRlCons", lcsql)
 LOCAL lcndocrec
 STORE 0 TO lcndocrec
 SELECT ucrsrecons
 lcndocrec = ucrsrecons.ndoc
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		select nmdoc, ndoc,codsaft from tsre (nolock) where ndoc = <<lcNdocRec>>
 ENDTEXT
 uf_gerais_actgrelha("", "uCrsTsReCons", lcsql)
 regvendas.pgfreg.page4.refresh
ENDFUNC
**
PROCEDURE uf_regvendas_limpaobjectospagamento
 regvendas.ctnpagamento.txtdinheiro.value = ''
 regvendas.ctnpagamento.txtmb.value = ''
 regvendas.ctnpagamento.txtvisa.value = ''
 regvendas.ctnpagamento.txtcheque.value = ''
 regvendas.ctnpagamento.txtmodpag3.value = ''
 regvendas.ctnpagamento.txtmodpag4.value = ''
 regvendas.ctnpagamento.txtmodpag5.value = ''
 regvendas.ctnpagamento.txtmodpag6.value = ''
ENDPROC
**
PROCEDURE uf_regvendas_inactivaobjectospagamento
 regvendas.ctnpagamento.txtdinheiro.enabled = .F.
 regvendas.ctnpagamento.txtmb.enabled = .F.
 regvendas.ctnpagamento.txtvisa.enabled = .F.
 regvendas.ctnpagamento.txtcheque.enabled = .F.
 regvendas.ctnpagamento.txtmodpag3.enabled = .F.
 regvendas.ctnpagamento.txtmodpag4.enabled = .F.
 regvendas.ctnpagamento.txtmodpag5.enabled = .F.
 regvendas.ctnpagamento.txtmodpag6.enabled = .F.
 regvendas.ctnpagamento.btndinheiro.enable(.F.)
 regvendas.ctnpagamento.btnmb.enable(.F.)
 regvendas.ctnpagamento.btnvisa.enable(.F.)
 regvendas.ctnpagamento.btncheque.enable(.F.)
 regvendas.ctnpagamento.btnmodpag3.enable(.F.)
 regvendas.ctnpagamento.btnmodpag4.enable(.F.)
 regvendas.ctnpagamento.btnmodpag5.enable(.F.)
 regvendas.ctnpagamento.btnmodpag6.enable(.F.)
ENDPROC
**
PROCEDURE uf_regvendas_activaobjectospagamento
 regvendas.ctnpagamento.txtdinheiro.enabled = .T.
 regvendas.ctnpagamento.txtmb.enabled = .T.
 regvendas.ctnpagamento.txtvisa.enabled = .T.
 regvendas.ctnpagamento.txtcheque.enabled = .T.
 regvendas.ctnpagamento.txtmodpag3.enabled = .T.
 regvendas.ctnpagamento.txtmodpag4.enabled = .T.
 regvendas.ctnpagamento.txtmodpag5.enabled = .T.
 regvendas.ctnpagamento.txtmodpag6.enabled = .T.
 regvendas.ctnpagamento.btndinheiro.enable(.T.)
 regvendas.ctnpagamento.btnmb.enable(.T.)
 regvendas.ctnpagamento.btnvisa.enable(.T.)
 regvendas.ctnpagamento.btncheque.enable(.T.)
 regvendas.ctnpagamento.btnmodpag3.enable(.T.)
 regvendas.ctnpagamento.btnmodpag4.enable(.T.)
 regvendas.ctnpagamento.btnmodpag5.enable(.T.)
 regvendas.ctnpagamento.btnmodpag6.enable(.T.)
ENDPROC
**
PROCEDURE uf_imprimir_regularizacoes
 LPARAMETERS lcstamp, lcpainel, lcexporta
 LOCAL totval, totsaldo, lcespaco, docnumber, contador, uv_print
 PUBLIC mypaisnacional
 STORE 0 TO totval
 STORE 0 TO totsaldo
 STORE "" TO lcespaco
 STORE 0 TO docnumber, contador
 
 uv_print = .f.

 DO CASE

    CASE regvendas.sosusp = 1 AND regvendas.socred = 1 AND soenc = 0 AND sores = 0
        uv_print = .t.

    CASE regvendas.sosusp = 1 AND regvendas.socred = 0 AND soenc = 0 AND sores = 0
        uv_print = .t.

    CASE regvendas.sosusp = 0 AND regvendas.socred = 1 AND soenc = 0 AND sores = 0
        uv_print = .t.

    CASE regvendas.sosusp = 0 AND regvendas.socred = 0 AND soenc = 1 AND sores = 0
        uv_print = .t.

    CASE regvendas.sosusp = 0 AND regvendas.socred = 0 AND soenc = 0 AND sores = 1
        uv_print = .t.

    OTHERWISE
        uv_print = .f.

 ENDCASE

 IF EMPTY(regvendas.txtno.value) .OR. !uv_print
    uf_perguntalt_chama("Para utilizar esta op��o tem de filtrar um utente e seleccionar pelo menos uma op��o:" + chr(13) + ".Suspensas" + chr(13) + ".Cr�ditos" + chr(13) + ".Suspensas+Cr�ditos" + chr(13) + ".Reservas" + chr(13) + ".Encomendas", "OK", "", 16)
 ELSE

    IF regvendas.sosusp==1

		select ucrsregvendas 

		uv_filtro = FILTER()

        SELECT 999 as stamp,ref, space(60) as des, SUM(qtt) as qtt from ucrsregvendas WHERE &uv_filtro. GROUP BY ref, des INTO CURSOR crsprintres READWRITE

        SELECT crsprintres
        GO TOP
        SCAN

            replace crsprintres.des WITH uf_gerais_getUmValor("st","design","ref = '" + crsprintres.ref + "'")

        ENDSCAN
		
		SELECT * FROM crsprintres  ORDER BY des  INTO CURSOR crsprintres  
		
    ENDIF
    
    **IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
	IF uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + ALLTRIM(myTerm) + "'")
    
    	PUBLIC Cabprintline
		store '' to Cabprintline
		
		uv_idImp = ''
	
		DO CASE
            CASE regvendas.sosusp==1 AND regvendas.soCred == 1
				Cabprintline = PADC("VENDAS SUSPENSAS/CREDITO", uf_gerais_getparameter("ADM0000000194", "NUM"), " ")
				
				uv_idImp = uf_gerais_getUmValor("B_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Reulariza��es'")				
				
			CASE regvendas.sosusp==1
			
				Cabprintline = PADC("VENDAS SUSPENSAS", uf_gerais_getparameter("ADM0000000194", "NUM"), " ")
				
				uv_idImp = uf_gerais_getUmValor("B_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Reulariza��es'")				
				
	        CASE regvendas.socred==1
	        	Cabprintline = PADC("SALDO CONTA CORRENTE", uf_gerais_getparameter("ADM0000000194", "NUM"), " ")
	        	
				uv_idImp = uf_gerais_getUmValor("B_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Registo Cr�ditos'")				
	        	
          	CASE sores==1
	            Cabprintline = PADC("RESERVAS PENDENTES", uf_gerais_getparameter("ADM0000000194", "NUM"), " ")
	        	
				uv_idImp = uf_gerais_getUmValor("B_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Registo Cr�ditos'")				
				
		ENDCASE
    

		IF !EMPTY(uv_idImp)
			uf_imprimirgerais_sendprinter_talao(uv_idImp , .f., .F.)
		ENDIF
    
	ELSE
	    lcvalidaimp = uf_gerais_setimpressorapos(.T.)
	    IF lcvalidaimp
	       ??? CHR(27E0)+CHR(64E0)
	       ??? CHR(27)+CHR(99)+CHR(48)+CHR(1)
	       ??? CHR(27E0)+CHR(116)+CHR(003)
	       ??? CHR(27)+CHR(33)+CHR(1)
	       ?? " "
	       ? LEFT(ALLTRIM(ucrse1.nomecomp), uf_gerais_getparameter("ADM0000000194", "NUM"))
	       ??? CHR(27)+CHR(33)+CHR(1)
	       ? "Dir. Tec. "
	       ?? LEFT(ALLTRIM(ucrse1.u_dirtec), uf_gerais_getparameter("ADM0000000194", "NUM"))
	       ? ALLTRIM(ucrse1.morada)
	       ? SUBSTR(ucrse1.codpost, 1, 30)
	       ?? " NIF:"
	       ?? TRANSFORM(ucrse1.ncont, "999999999")
	       ? "Tel:"
	       ?? TRANSFORM(ucrse1.telefone, "#########")
	       ?? "    Email:"
	       ?? ALLTRIM(ucrse1.email)
	       ??
          ?  ALLTRIM(uf_gerais_getMacrosReports(2))
          ??
	       uf_gerais_separadorpos()
	       LOCAL lcutenteinfo, lcdatainicio, lcdatafim, lccodbarrasreserva
	       STORE '' TO lcutenteinfo, lcdatainicio, lcdatafim, lccodbarrasreserva
	       SELECT ucrsregvendas
	       GOTO TOP
	       lcutenteinfo = ALLTRIM("Utente: "+ALLTRIM(ucrsregvendas.nome)+" ["+ALLTRIM(STR(ucrsregvendas.no))+"]"+" ["+ALLTRIM(STR(ucrsregvendas.estab))+"]")
	       lcdatainicio = "Data Inicio: "+ALLTRIM(regvendas.txtdataini.value)
	       lcdatafim = "   Data Fim: "+ALLTRIM(regvendas.txtdatafim.value)
	       DO CASE
	            CASE regvendas.sosusp==1 AND regvendas.soCred == 1
	          	*SELECT ref, space(60) as des, SUM(qtt) as qtt from ucrsregvendas WHERE AT('(S)',ucrsregvendas.nmdoc)>0 GROUP BY ref, des INTO CURSOR crsprintres READWRITE

	             ? PADC("VENDAS SUSPENSAS/CREDITO", uf_gerais_getparameter("ADM0000000194", "NUM"), " ")
	             uf_gerais_separadorpos()
	             ? lcutenteinfo
	             ? lcdatainicio
	             ?? lcdatafim
	             uf_gerais_separadorpos()
	             ? "PRODUTO                                     QTD"
	             uf_gerais_separadorpos()
	             SELECT crsprintres
	             GOTO TOP
	             SCAN
	                ? LEFT(crsprintres.des, 45)+"  "+ALLTRIM(TRANSFORM(crsprintres.qtt, "9999"))
	             ENDSCAN
	             fecha("crsprintres")

	          CASE regvendas.sosusp==1
	          	*SELECT ref, space(60) as des, SUM(qtt) as qtt from ucrsregvendas WHERE AT('(S)',ucrsregvendas.nmdoc)>0 GROUP BY ref, des INTO CURSOR crsprintres READWRITE

	             ? PADC("VENDAS SUSPENSAS", uf_gerais_getparameter("ADM0000000194", "NUM"), " ")
	             uf_gerais_separadorpos()
	             ? lcutenteinfo
	             ? lcdatainicio
	             ?? lcdatafim
	             uf_gerais_separadorpos()
	             ? "PRODUTO                                     QTD"
	             uf_gerais_separadorpos()
	             SELECT crsprintres
	             GOTO TOP
	             SCAN
	                ? LEFT(crsprintres.des, 45)+"  "+ALLTRIM(TRANSFORM(crsprintres.qtt, "9999"))
	             ENDSCAN
	             fecha("crsprintres")

	          CASE regvendas.socred==1
	             ? PADC("SALDO CONTA CORRENTE", uf_gerais_getparameter("ADM0000000194", "NUM"), " ")
	             uf_gerais_separadorpos()
	             ? lcutenteinfo
	             ? lcdatainicio
	             ?? lcdatafim
	             uf_gerais_separadorpos()
	             ? "DATA        DOCUMENTO"
	             ? "QTD PRODUTO	                         PVP	 SALDO"
	             uf_gerais_separadorpos()
	             SELECT ucrsregvendas
	             GOTO TOP
	             SCAN
	                ? ALLTRIM(ucrsregvendas.fdata)+"  "+ALLTRIM(ucrsregvendas.nmdoc)+" "+astr(ucrsregvendas.fno)
	                ? ALLTRIM(TRANSFORM(ucrsregvendas.qtt, "9999"))+" "+LEFT(ucrsregvendas.design, 37)+" "+TRANSFORM(ucrsregvendas.etiliquido, "999.99")+" "+TRANSFORM(ucrsregvendas.etiliquido-ucrsregvendas.valoradiantado, "999.99")
	                ? " "
	                totval = totval+ucrsregvendas.etiliquido
	                totsaldo = totsaldo+ucrsregvendas.etiliquido-ucrsregvendas.valoradiantado
	             ENDSCAN
	             uf_gerais_separadorpos()
	             ? "TOTAL: "+ALLTRIM(TRANSFORM(totval, "999999.99"))
	             ?? "                         SALDO: "+TRANSFORM(totsaldo, "99999.99")
	             uf_gerais_separadorpos()
	             ? "                  IVA Incluido"
	          CASE sores==1
	             ? PADC("RESERVAS PENDENTES", uf_gerais_getparameter("ADM0000000194", "NUM"), " ")
	             uf_gerais_separadorpos()
	             ? lcutenteinfo
	             ? lcdatainicio
	             ?? lcdatafim
	             uf_gerais_separadorpos()
	             ? "DATA        DOCUMENTO"
	             ? "QTD  PRODUTO                               PVP   ADT"
	             uf_gerais_separadorpos()
	             SELECT ucrsregvendas
	             GOTO TOP
	             SCAN
	                IF ucrsregvendas.fno<>docnumber
	                   ? " "
	                   ? ALLTRIM(ucrsregvendas.fdata)+"  "+ALLTRIM(ucrsregvendas.nmdoc)+" "+astr(ucrsregvendas.fno)
	                ENDIF
	                ? TRANSFORM(ucrsregvendas.qtt, "9999")+" "+LEFT(ucrsregvendas.design, 34)+"  "+ALLTRIM(TRANSFORM(ucrsregvendas.etiliquido, "9999.99"))+"  "+ALLTRIM(TRANSFORM(ucrsregvendas.valoradiantado, "9999.99"))
	                docnumber = (ucrsregvendas.fno)
	             ENDSCAN
	             uf_gerais_separadorpos()
	             ? "                  IVA Incluido"
	          OTHERWISE
	             ? PADC("HISTORICO CLIENTE", uf_gerais_getparameter("ADM0000000194", "NUM"), " ")
	             uf_gerais_separadorpos()
	             ? lcutenteinfo
	             ? lcdatainicio
	             ?? lcdatafim
	             uf_gerais_separadorpos()
	             ? "DATA        PRODUTO                                QTD"
	             uf_gerais_separadorpos()
	             SELECT ucrsregvendas
	             GOTO TOP
	             SCAN
	                ? ALLTRIM(ucrsregvendas.fdata)+"  "+LEFT(ucrsregvendas.design, 40)+"  "+ALLTRIM(TRANSFORM(ucrsregvendas.qtt, "9999"))
	             ENDSCAN
	       ENDCASE
	       uf_gerais_separadorpos()
	       ? DATETIME()+(difhoraria*3600)
	       ?? "        Op: "+ALLTRIM(m_chinis)
	       ? "              Processado por computador"
	       IF sores==1
	          SELECT ucrsregvendas
	          GOTO TOP
	          uf_gerais_separadorpos()
	          lccodbarrasreserva = 'R'+ALLTRIM(STR(ucrsregvendas.fno))+'C'+ALLTRIM(STR(ucrsregvendas.no))+'D'+ALLTRIM(STR(ucrsregvendas.estab))
	          ? " "
	          ?
	          ??? CHR(29)+CHR(104)+CHR(50)+CHR(29)+CHR(72)+CHR(2)+CHR(29)+CHR(119)+CHR(2)+CHR(29)+CHR(107)+CHR(4)+UPPER(ALLTRIM(lccodbarrasreserva))+CHR(0)
	          ? " "
	       ENDIF
	       uf_gerais_feedcutpos()
	    ENDIF
	    uf_gerais_setimpressorapos(.F.)
	    SET PRINTER TO DEFAULT
	ENDIF
 ENDIF
ENDPROC
**
PROCEDURE uf_imprimir_nreg
 LPARAMETERS lcstamp, lcpainel, lcexporta
 LOCAL totval, lcespaco
 PUBLIC mypaisnacional
 STORE 0 TO totval
 STORE "" TO lcespaco
 IF EMPTY(regvendas.txtno.value)
    uf_perguntalt_chama("Para utilizar esta op��o tem de filtrar um utente", "OK", "", 16)
 ELSE
    lcvalidaimp = uf_gerais_setimpressorapos(.T.)
    IF lcvalidaimp
       ??? CHR(27E0)+CHR(64E0)
       ??? CHR(27)+CHR(99)+CHR(48)+CHR(1)
       ??? CHR(27E0)+CHR(116)+CHR(003)
       ??? CHR(27)+CHR(33)+CHR(1)
       ?? " "
       ? LEFT(ALLTRIM(ucrse1.nomecomp), uf_gerais_getparameter("ADM0000000194", "NUM"))
       ??? CHR(27)+CHR(33)+CHR(1)
       ? SUBSTR(ucrse1.local, 1, 10)
       ?? " Tel:"
       ?? TRANSFORM(ucrse1.telefone, "#########")
       ?? " NIF:"
       ?? TRANSFORM(ucrse1.ncont, "999999999")
       ? ALLTRIM(uf_gerais_getMacrosReports(2))
       ? "Dir. Tec. "
       ?? LEFT(ALLTRIM(ucrse1.u_dirtec), uf_gerais_getparameter("ADM0000000194", "NUM"))
       ??
       uf_gerais_separadorpos()
       ? PADC("MOVIMENTOS NAO REGULARIZADOS", uf_gerais_getparameter("ADM0000000194", "NUM"), " ")
       ? "Data Inicio.: "+ALLTRIM(regvendas.txtdataini.value)
       ? "Data Inicio.: "+ALLTRIM(regvendas.txtdatafim.value)
       uf_gerais_separadorpos()
       ? DATETIME()+(difhoraria*3600)
       uf_gerais_separadorpos()
       IF regvendas.pgfreg.page2.chkporvalor.tag="true"
          ? "Utente: "+ALLTRIM(ucrsregcreditosvalor.cliente)
          uf_gerais_separadorpos()
          ? "Documento         Valor Pendente"
          uf_gerais_separadorpos()
          SELECT ucrsregcreditosvalor
          GOTO TOP
          SCAN
             DO CASE
                   lcespaco = " "
                CASE LEN(ALLTRIM(TRANSFORM(ucrsregcreditosvalor.valornreg, "999999.99")))=6
                   lcespaco = "  "
                CASE LEN(ALLTRIM(TRANSFORM(ucrsregcreditosvalor.valornreg, "999999.99")))=5
                   lcespaco = "   "
                CASE LEN(ALLTRIM(TRANSFORM(ucrsregcreditosvalor.valornreg, "999999.99")))=4
                   lcespaco = "    "
                CASE LEN(ALLTRIM(TRANSFORM(ucrsregcreditosvalor.valornreg, "999999.99")))=3
                   lcespaco = "     "
                OTHERWISE
                   lcespaco = ""
             ENDCASE
             ? LEFT(ALLTRIM(ucrsregcreditosvalor.nmdoc)+" "+astr(ucrsregcreditosvalor.nrdoc)+"   "+ALLTRIM(ucrsregcreditosvalor.datalc)+" ............................................................", 42)+" "+lcespaco+ALLTRIM(TRANSFORM(ucrsregcreditosvalor.valornreg, "9999.99"))
             totval = totval+ucrsregcreditosvalor.valornreg
          ENDSCAN
       ENDIF
       IF regvendas.pgfreg.page2.chkporproduto.tag="true"
          ? "Utente: "+ALLTRIM(ucrsregcreditos.cliente)
          uf_gerais_separadorpos()
          ? "Documento          Artigo    "
          ? "Data         Qtd         Valor Pendente"
          uf_gerais_separadorpos()
          SELECT ucrsregcreditos
          GOTO TOP
          SCAN
             ? ALLTRIM(ucrsregcreditos.nmdoc)+" "+astr(ucrsregcreditos.fno)+" ..... "+ALLTRIM(LEFT(ucrsregcreditos.design, 35))
             ? ALLTRIM(ucrsregcreditos.fdata)+" ..... "+ALLTRIM(TRANSFORM(ucrsregcreditos.qtt, "9999"))+" ....... "+ALLTRIM(TRANSFORM(ucrsregcreditos.etiliquido, "9999.99"))
             totval = totval+ucrsregcreditos.etiliquido
          ENDSCAN
       ENDIF
       uf_gerais_separadorpos()
       ? "Total: "+ALLTRIM(TRANSFORM(totval, "999999.99"))
       uf_gerais_feedcutpos()
    ENDIF
    uf_gerais_setimpressorapos(.F.)
    SET PRINTER TO DEFAULT
 ENDIF
ENDPROC
**
FUNCTION uf_regvendas_fechalinha
 LOCAL lccursor, lccont, lcposcab
 SELECT ucrsregvendas
 lcposcab = RECNO("uCrsRegVendas")
 CALCULATE CNT() TO lccont FOR ucrsregvendas.sel=.T.
 IF lccont=0
    uf_perguntalt_chama("N�o selecionou nenhuma linha para anular! Por favor retifique.", "OK", "", 48)
    SELECT ucrsregvendas
    GOTO TOP
    TRY
       GOTO lcposcab
    CATCH
    ENDTRY
    RETURN .F.
 ENDIF
 IF lccont>1
    uf_perguntalt_chama("S� pode anular uma linha de cada vez! Por favor retifique.", "OK", "", 48)
    RETURN .F.
 ENDIF
 lccursor = 'uCrsRegVendas'
 SELECT ucrsregvendas
 GOTO TOP
 SCAN
    IF ucrsregvendas.sel=.T.
       IF uf_perguntalt_chama("Vai anular a linha na reserva de forma permanente."+CHR(13)+CHR(13)+"Tem a certeza que pretende continuar?", "Sim", "N�o")
          LOCAL lcval
          lcval = .F.
          IF uf_gerais_getparameter("ADM0000000066", "BOOL")==.T.
             IF  .NOT. EMPTY(uf_gerais_getparameter("ADM0000000066", "TEXT"))
                lcpass = uf_gerais_getparameter("ADM0000000066", "TEXT")
             ENDIF
             PUBLIC passfechar
             STORE '' TO passfechar
             uf_tecladoalpha_chama("passFechar", "INTRODUZA PASSWORD DE SUPERVISOR DE DOCUMENTOS:", .T., .F., 0)
             IF  .NOT. EMPTY(passfechar)
                cval = ALLTRIM(passfechar)
             ELSE
                cval = ""
             ENDIF
             RELEASE passfechar
             DO CASE
                CASE EMPTY(cval) .OR. EMPTY(lcpass)
                   uf_perguntalt_chama("DEVE INTRODUZIR A PASSWORD DE SUPERVISOR PARA REALIZAR ESTA OPERA��O.", "OK", "", 48)
                CASE  .NOT. (UPPER(ALLTRIM(lcpass))==UPPER(ALLTRIM(cval)))
                   uf_perguntalt_chama("A PASSWORD N�O EST� CORRECTA. POR FAVOR VERIFIQUE SE EST� A INTRODUZIR CORRECTAMENTE.", "OK", "", 16)
                OTHERWISE
                   lcval = .T.
             ENDCASE
          ELSE
             lcval = .T.
          ENDIF
          IF lcval==.T.
             LOCAL lcsql, lcbistamp, lcbostamp
             SELECT &lccursor
             IF !EMPTY(&lccursor..stampadiantamento)
                SELECT fi
                GOTO BOTTOM
                SELECT &lccursor
                lcfistampadiant = &lccursor..stampadiantamento
                lcsql = ''
                TEXT TO lcsql TEXTMERGE NOSHOW
							exec up_reservas_regularizarAdiantamento '<<lcFiStampAdiant>>'
                ENDTEXT
                uf_gerais_actgrelha("", "uCrsValeReserva", lcsql)
                IF USED("ucrsVale")
                   SELECT * FROM ucrsVale UNION ALL SELECT * FROM uCrsValeReserva INTO CURSOR ucrsVale2 READWRITE
                ELSE
                   SELECT * FROM uCrsValeReserva INTO CURSOR ucrsVale2 READWRITE
                ENDIF
                SELECT ucrsvale2
                uf_regvendas_regvendassel(.T.)
                SELECT ucrsvalereserva
                SELECT fi
                LOCATE FOR ALLTRIM(fi.ofistamp)==ALLTRIM(lcfistampadiant)
                IF AT("Ad. Reserva", ALLTRIM(ucrsvalereserva.design))=0
                   REPLACE fi.design WITH "Ad. Reserva - "+ucrsvalereserva.design IN "FI"
                ELSE
                   REPLACE fi.design WITH " "+ucrsvalereserva.design IN "FI"
                ENDIF
                REPLACE fi.bistamp WITH &lccursor..fistamp IN "FI"
                REPLACE fi.bostamp WITH &lccursor..ftstamp IN "FI"

                lndesc = fi.u_descval
                uf_atendimento_devolver()
                fecha("uCrsValeReserva")
                fecha("uCrsVale2")
             ENDIF
             SELECT &lccursor
             lcbistamp = &lccursor..fistamp
             lcbostamp = &lccursor..ftstamp
             lcsql = ''
             TEXT TO lcsql TEXTMERGE NOSHOW
						UPDATE bi
						SET fechada = 1,
						usrdata = dateadd(HOUR, <<difhoraria>>, getdate()),
						usrhora = convert(varchar,getdate(),8)
						where bistamp = '<<lcBIstamp>>'
						
						DECLARE @nRowsF numeric(5,0)
						
						SELECT @nRowsF = COUNT(bistamp) FROM bi (nolock) WHERE fechada = 0 AND bostamp = '<<lcBOstamp>>'
						
						IF @nRowsF = 0
						BEGIN 
							UPDATE bo
							SET fechada = 1,
							DATAFECHO = dateadd(HOUR, <<difhoraria>>, getdate()),
							usrdata = dateadd(HOUR, <<difhoraria>>, getdate()),
							usrhora = convert(varchar,getdate(),8)
							where bostamp = '<<lcBOstamp>>'
						END
             ENDTEXT
             uf_gerais_actgrelha("", "", lcsql)
             SELECT &lccursor
             DELETE
          ELSE
             RETURN .F.
          ENDIF
       ELSE
          RETURN .F.
       ENDIF
    ENDIF
    
 ENDSCAN
 uf_regvendas_sair()
ENDFUNC
**
FUNCTION uf_regvendas_fecha
 LOCAL lnnumreserva, lcposlin
 STORE 0 TO lnnumreserva
 lcposlin = RECNO("uCrsRegVendas")
 SELECT ucrsregvendas
 GOTO TOP
 SCAN
    IF ucrsregvendas.sel=.T.
       lnnumreserva = ucrsregvendas.fno
       SELECT * FROM fi WHERE ALLTRIM(fi.bistamp)==ALLTRIM(ucrsregvendas.fistamp) OR ALLTRIM(fi.bostamp)==ALLTRIM(ucrsregvendas.ftstamp) INTO CURSOR ucrsfifecha READWRITE
       SELECT ucrsfifecha
       IF RECCOUNT("ucrsfifecha")>0
          fecha("ucrsfifecha")
          uf_perguntalt_chama("N�o pode anular uma reserva com linha(s) seleccionada(s) inclu�da(s) no atendimento."+CHR(13)+"Por favor remova a mesma do atendimento.", "OK", "", 48)
          RETURN .F.
       ENDIF
       fecha("ucrsfifecha ")
    ENDIF
 ENDSCAN
 IF lnnumreserva=0
    uf_perguntalt_chama("N�o selecionou registo para anular! Por favor retifique.", "OK", "", 48)
    SELECT ucrsregvendas
    GOTO TOP
    TRY
       GOTO lcposlin
    CATCH
    ENDTRY
    RETURN .F.
 ENDIF
 
 LOCAL lbPerguntaReserva
 lbPerguntaReserva = .t.
 
 
 **valida se tem dems seleccionadas
 ** ::TODO vers�o 24_03
*!*	 SELECT ucrsregvendas
*!*	 GOTO TOP
*!*	 SCAN
*!*	 IF ucrsregvendas.sel=.T. AND !empty(ucrsregvendas.nrreceita) AND LEN(ucrsregvendas.nrreceita)>18
*!*	 	**lbPerguntaReserva = .f.
*!*	 ENDIF
*!*	 ENDSCAN
*!*	 

 
 PUBLIC myretornocancelar
 if(lbPerguntaReserva)
 	lcretorno = uf_perguntalt_chama("ESCOLHA O QUE PRETENDE ANULAR", "RESERVA", "LINHA", .F., .F., "Cancelar")
 ELSE
 	lcretorno  = .t. &&sempre toda a reserva
 ENDIF
 
 IF myretornocancelar==.T.
    SELECT ucrsregvendas
    GOTO TOP
    TRY
       GOTO lcposlin
    CATCH
    ENDTRY
    RETURN .F.
 ENDIF
 IF lcretorno==.T.
    LOCAL lnnumreserva, lcposlin
    STORE 0 TO lnnumreserva
    lcposlin = RECNO("uCrsRegVendas")
    SELECT ucrsregvendas
    GOTO TOP
    SELECT DISTINCT ftstamp FROM uCrsRegVendas WHERE ucrsregvendas.sel=.T. INTO CURSOR contastamps READWRITE
    SELECT contastamps
    IF RECCOUNT("contastamps")>1
       uf_perguntalt_chama("S� pode anular um documento de cada vez! Por favor retifique.", "OK", "", 48)
       SELECT ucrsregvendas
       GOTO TOP
       TRY
          GOTO lcposlin
       CATCH
       ENDTRY
       RETURN .F.
    ENDIF
    fecha("contastamps")
    SELECT ucrsregvendas
    GOTO TOP
    SCAN
       IF ucrsregvendas.sel=.T.
          lnnumreserva = ucrsregvendas.fno
       ENDIF
    ENDSCAN
    IF lnnumreserva=0
       uf_perguntalt_chama("N�o selecionou nenhuma linha para anular! Por favor retifique.", "OK", "", 48)
       SELECT ucrsregvendas
       GOTO TOP
       TRY
          GOTO lcposlin
       CATCH
       ENDTRY
       RETURN .F.
    ENDIF
    SELECT ucrsregvendas
    LOCATE FOR ucrsregvendas.fno==lnnumreserva
    LOCAL lccursor
    lccursor = 'uCrsRegVendas'
    IF uf_perguntalt_chama("Quer mesmo anular a Reserva n� "+astr(ucrsregvendas.fno)+"? "+CHR(13)+"Deixar� de estar dispon�vel para utilizar no atendimento.", "Sim", "N�o")
       LOCAL lcval
       lcval = .F.
       IF uf_gerais_getparameter("ADM0000000066", "BOOL")==.T.
          IF  .NOT. EMPTY(uf_gerais_getparameter("ADM0000000066", "TEXT"))
             lcpass = uf_gerais_getparameter("ADM0000000066", "TEXT")
          ENDIF
          PUBLIC passfechar
          STORE '' TO passfechar
          uf_tecladoalpha_chama("passFechar", "INTRODUZA PASSWORD DE SUPERVISOR DE DOCUMENTOS:", .T., .F., 0)
          IF  .NOT. EMPTY(passfechar)
             cval = ALLTRIM(passfechar)
          ELSE
             cval = ""
          ENDIF
          RELEASE passfechar
          DO CASE
             CASE EMPTY(cval) .OR. EMPTY(lcpass)
                uf_perguntalt_chama("DEVE INTRODUZIR A PASSWORD DE SUPERVISOR PARA REALIZAR ESTA OPERA��O.", "OK", "", 48)
             CASE  .NOT. (UPPER(ALLTRIM(lcpass))==UPPER(ALLTRIM(cval)))
                uf_perguntalt_chama("A PASSWORD N�O EST� CORRECTA. POR FAVOR VERIFIQUE SE EST� A INTRODUZIR CORRECTAMENTE.", "OK", "", 16)
             OTHERWISE
                lcval = .T.
          ENDCASE
       ELSE
          lcval = .T.
       ENDIF
       IF lcval==.T.
          IF !uf_gerais_actgrelha("","","UPDATE BO SET Fechada = 1,usrdata=getdate(), usrhora=convert(varchar,getdate(),8) WHERE Bostamp = '" + ALLTRIM(&lccursor..ftstamp) + "'") OR !uf_gerais_actgrelha("","","UPDATE BI SET Fechada = 1,usrdata=getdate(),usrhora = convert(varchar,getdate(),8) WHERE Bostamp = '" + ALLTRIM(&lccursor..ftstamp) + "'")
             uf_perguntalt_chama("N�O FOI POSS�VEL ALTERAR O ESTADO DO DOCUMENTO! POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
             RETURN .F.
          ELSE
             uf_regvendas_fecha_liberta_receitas( ALLTRIM(&lccursor..ftstamp),  ALLTRIM(&lccursor..fistamp))
             LOCAL lnobrano, lordem2
             SELECT &lccursor
             lnobrano = &lccursor..fno
       
             LOCAL lcCabCriada, lcDeveCriarCab 
             lcCabCriada = .f.
             lcDeveCriarCab  = .t.
             
             
             
             SELECT &lccursor
             GOTO TOP
			 
             SCAN FOR &lccursor..fno == lnobrano
                IF !EMPTY(&lccursor..stampadiantamento)
                   SELECT fi
                   GOTO BOTTOM
                   SELECT &lccursor
                   lcfistampadiant = &lccursor..stampadiantamento
                   lcsql = ''
                   TEXT TO lcsql TEXTMERGE NOSHOW
						exec up_reservas_regularizarAdiantamento '<<lcFiStampAdiant>>'
                   ENDTEXT
                   uf_gerais_actgrelha("", "uCrsValeReserva", lcsql)

					                   
                   IF USED("ucrsVale")
                      SELECT FTSTAMP,FISTAMP,FDATA,OUSRHORA,NMDOC,FNO,NOME,NO,ESTAB,NCONT,REF,DESIGN,DESCONTO,DESC2,U_DESCVAL,U_COMP,QTT,QTT2,ETILIQUIDO,ETILIQUIDO2,ETILIQUIDO as TOTLIN,TOTALCDESC;
                            ,EPV,NDOC,U_LOTE,U_NSLOTE,U_LOTE2,U_NSLOTE2,SEL,COPIED,U_LTSTAMP,U_LTSTAMP2,U_CODIGO,U_CODIGO2,U_DIPLOMA,DEV,FACT,FACTPAGO,FMARCADA,EAQUISICAO,OEAQUISICAO,U_PSICONT;
                            ,U_BENCONT,U_RECEITA,U_NBENEF,U_NBENEF2,U_NOPRESC,U_ENTPRESC,U_RECDATA,U_MEDICO,U_NMUTDISP,U_MOUTDISP,U_CPUTDISP,U_NMUTAVI,U_MOUTAVI,U_CPUTAVI,U_NDUTAVI,U_DDUTAVI,U_IDUTAVI;
                            ,U_TXCOMP,U_EPVP,U_ETTENT1,U_ETTENT2,DOCINVERT,U_BACKOFF,LANCACC,U_DESIGN,U_ABREV,U_ABREV2,U_EPREF,TIPODOC,TABIVA,IVA,OPCAO,TOKEN,CODACESSO,CODDIROPCAO,ID_VALIDACAODEM,AMOSTRA,c2codpost, lote, datavalidade; 
                                FROM ucrsVale UNION ALL SELECT * FROM uCrsValeReserva INTO CURSOR ucrsVale2 READWRITE
                   ELSE
                      SELECT * FROM uCrsValeReserva INTO CURSOR ucrsVale2 READWRITE
                   ENDIF
                   SELECT ucrsvale2
                   
                   if(!lcCabCriada and lcDeveCriarCab )	 
             
			            SELECT fi
			            GOTO BOTTOM

                        IF !uf_regVendas_checkCabs()
			                uf_atendimento_novavenda(.F.)     
                        ENDIF

                        lcCabCriada  = .t.              
   
                   ENDIF

                   uf_regvendas_regvendassel(.T., .T.)
                   
         
                   SELECT ucrsvalereserva
                   SELECT fi
                   
                   LOCATE FOR ALLTRIM(fi.ofistamp)==ALLTRIM(lcfistampadiant)
                   IF AT("Ad. Reserva", ALLTRIM(&lccursor..DESIGN)) = 0
                      REPLACE fi.DESIGN WITH "Ad. Reserva - " + &lccursor..DESIGN IN "FI"
                   ELSE
                      REPLACE fi.DESIGN WITH " " + &lccursor..DESIGN IN "FI"
                   ENDIF
                   REPLACE fi.bistamp WITH &lccursor..fistamp IN "FI"
                   REPLACE fi.bostamp WITH &lccursor..ftstamp IN "FI"
                   lndesc = fi.u_descval
                   uf_atendimento_devolver()
                   lordem2 = fi.lordem
                   fecha("uCrsValeReserva")
                   fecha("uCrsVale2")
                ENDIF
             ENDSCAN
             IF VARTYPE(lordem2)=='N'
                UPDATE fi SET id_validacaodem = '', receita = '', token = '' WHERE LEFT(ALLTRIM(STR(lordem2)), 1)==LEFT(ALLTRIM(STR(fi.lordem)), 1)
             ENDIF
             UPDATE fi SET tipor = 'SR' WHERE LEFT(ALLTRIM(STR(100001)), 1)==LEFT(ALLTRIM(STR(fi.lordem)), 1) .AND. RIGHT(ALLTRIM(STR(fi.lordem)), 3)=='000'
             IF USED("uCrsVale")
                SELECT ucrsvale
                UPDATE uCrsVale FROM uCrsVale INNER JOIN fi ON ALLTRIM(ucrsvale.fistamp)==ALLTRIM(fi.ofistamp) SET etiliquido = fi.etiliquido, etiliquido2 = fi.etiliquido, epv = fi.epv
             ENDIF
             IF VARTYPE(fechares)<>"U"
                fechares = .T.
             ENDIF
             uf_regvendas_sair()
          ENDIF
       ENDIF
    ENDIF
 ELSE
    uf_regvendas_fechalinha()
 ENDIF
ENDFUNC


FUNCTION uf_atendimento_fixPrimeiroCabecalhoReserva
LPARAMETERS lcReceita, lordemcCab
	LOCAL lcPosFi
	
	SELECT fi
    lcPosFi = RECNO("fi")
	
	IF uf_gerais_compStr(lordemcCab ,"100000") AND !EMPTY(lcReceita)
		UPDATE fi SET receita = lcReceita WHERE uf_gerais_compStr(ALLTRIM(STR(fi.lordem)),lordemcCab)
	ENDIF
	
	SELECT fi
    TRY
       GOTO lcPosFi 
    CATCH
       GOTO BOTTOM
    ENDTRY
	

ENDFUNC



**
FUNCTION uf_regvendas_reservas_sel
 LOCAL lccursor
 set exact on

 
 
 SELECT ucrsregvendas
 GOTO TOP
 SELECT * FROM uCrsRegVendas INTO CURSOR uCrsRegVendasTemp READWRITE
 fecha("uCrsRegVendas")
 SELECT ucrsregvendastemp
 GOTO TOP
 SELECT * FROM uCrsRegVendasTemp WHERE ALLTRIM(ucrsregvendastemp.nmdoc)=='Reserva de Cliente' AND uCrsRegVendasTemp.sel ORDER BY fno, row INTO CURSOR uCrsRegVendas READWRITE
 **SELECT * FROM uCrsRegVendasTemp WHERE ALLTRIM(ucrsregvendastemp.nmdoc)=='Reserva de Cliente' ORDER BY fno, row INTO CURSOR uCrsRegVendas READWRITE

 lccursor = 'uCrsRegVendas'
 atendimento.lockscreen = .T.
 atendimento.grdatend.setfocus()
 LOCAL lcpos, lnobrano, lccodcomp, lcrecanterior
 lnobrano = 0
 lccodcomp = ""
 lcrecanterior = ''
 IF EMPTY(lccursor)
    uf_perguntalt_chama("O PAR�METRO: LCCURSOR, N�O PODE SER VAZIO! POR FAVOR CONTACTE O SUPORTE.", "OK", "", 48)
    atendimento.lockscreen = .F.
    RETURN .F.
 ENDIF
 PUBLIC RESreceitacab 
 STORE '' TO RESreceitacab 
 SELECT &lccursor
 GOTO TOP
 SCAN
    SELECT &lccursor
    IF &lccursor..sel AND (&lccursor..fechada OR &lccursor..qttcomp > 0) AND ALLTRIM(&lccursor..nmdoc) == 'Reserva de Cliente'
       uf_perguntalt_chama("Este produto j� foi inclu�do numa receita ou j� se encontra regularizado. N�o pode ser inclu�do neste atendimento."+CHR(13)+CHR(13)+"Nota: se todos os produtos j� estiverem regularizados dever� fechar a Reserva.", "OK", "", 48)
       REPLACE &lccursor..sel WITH .f.
       atendimento.lockscreen = .F.
       RETURN .F.
    ENDIF
    LOCAL lcvalidapreco, lcvalidaaplicacomp
    STORE .T. TO lcvalidapreco
    LOCAL lcposfi, lndesc
    STORE 0 TO lcposfi
    lndesc = 0
    SELECT &lccursor
    IF &lccursor..sel AND ALLTRIM(&lccursor..nmdoc) == 'Reserva de Cliente'
    	LOCAL lcNrReceitaResCL
    	STORE '' TO lcNrReceitaResCL
    	
    	IF ALLTRIM(&lccursor..nrreceita)!=''
    		lcNrReceitaResCL = ALLTRIM(&lccursor..nrreceita)
    	ENDIF 
    	
   
    	
       SELECT &lccursor
       lcstampbi = &lccursor..fistamp
       SELECT fi
       SELECT fi
       LOCATE FOR ALLTRIM(fi.bistamp)==ALLTRIM(lcstampbi)
       IF FOUND("FI")
          lcvalidaexiste = .T.
       ELSE
          lcvalidaexiste = .F.
       ENDIF
       IF  .NOT. lcvalidaexiste
          SELECT fi
          GOTO BOTTOM
          
          LOCAL lcnrreceitalinres, lnAnoReserva
	      STORE '' TO lcnrreceitalinres
	      lnAnoReserva = 2022
	      
		** 

	      
	      SELECT &lccursor
		  if(!EMPTY(&lccursor..fdata))
	    		lnAnoReserva  = VAL(alltrim(LEFT(uf_gerais_getdate(uCrsRegVendas.fdata, "SQL"),4)))
	      ELSE
	      		lnAnoReserva = 2022
	      ENDIF
	      
	   
	    
	      **apenas procura receita do modo anterior se a reserva for de 2022 para tras    
          IF EMPTY(lcNrReceitaResCL) AND lnAnoReserva <= 2022
	          lcsql = ''
	          TEXT TO lcsql TEXTMERGE NOSHOW
					select u_receita from ft (nolock) 
					inner join fi (nolock) on ft.ftstamp=fi.ftstamp
					inner join ft2 (nolock) on ft2.ft2stamp=ft.ftstamp
					where (u_nratend in( select u_nratend from ft (nolock) where ftstamp in (select ftstamp from fi (nolock) where fistamp=(select fistamp from bi2 (nolock) where bi2stamp='<<ALLTRIM(lcStampBi)>>')))
							and ft.nmdoc='Inser��o de Receita'
							and fi.ref in (select bi.ref from bi (nolock) join bi2(nolock) ON bi2.bi2stamp = bi.bistamp where bi.bistamp='<<ALLTRIM(lcStampBi)>>' and bi2.nrreceita = ft2.u_receita)
							or u_nratend in (select u_nratend from bo2 (nolock) where bo2stamp = (select bostamp from bi (nolock) where bistamp='<<ALLTRIM(lcStampBi)>>')))
							and fi.ref in (select bi.ref from bi (nolock) join bi2(nolock) ON bi2.bi2stamp = bi.bistamp where bi.bistamp='<<ALLTRIM(lcStampBi)>>' and bi2.nrreceita = ft2.u_receita)
	          ENDTEXT

	          uf_gerais_actgrelha("", "uCrsTemReceita", lcsql)

	          SELECT uCrsTemReceita 
	          lcnrreceitalinres = ALLTRIM(uCrsTemReceita.u_receita)
	          RESreceitacab = ALLTRIM(uCrsTemReceita.u_receita)
	      ELSE

	          lcnrreceitalinres = ALLTRIM(lcNrReceitaResCL)
	          RESreceitacab = ALLTRIM(lcNrReceitaResCL)
	      ENDIF 
         fecha("uCrsTemReceita")
          SELECT &lccursor
          IF (!(ALLTRIM(&lccursor..codcomp)==ALLTRIM(lccodcomp)) AND !EMPTY(lccodcomp) ) OR (ALLTRIM(lcnrreceitalinres)<>ALLTRIM(lcrecanterior) AND RECCOUNT("FI")>1)            
             SELECT fi
             GOTO BOTTOM
             uf_atendimento_novavenda(.F.)
             replace fi.receita WITH ALLTRIM(lcnrreceitalinres)
	             
            ** MESSAGEBOX(ALLTRIM(lcnrreceitalinres))
          ENDIF
          

          
          
         ** MESSAGEBOX(ALLTRIM(lcnrreceitalinres))
          lcrecanterior=ALLTRIM(lcnrreceitalinres)
          RESreceitacab =''
*!*	          lcsql = ''
*!*	          TEXT TO lcsql TEXTMERGE NOSHOW
*!*						select count(ftstamp) from ft (nolock) where u_nratend in( select u_nratend from ft (nolock) where ftstamp in (select ftstamp from fi (nolock) where fistamp=(select fistamp from bi2 (nolock) where bi2stamp='<<ALLTRIM(lcStampBi)>>')))
*!*						and nmdoc='Inser��o de Receita'
*!*	          ENDTEXT
*!*	          uf_gerais_actgrelha("", "uCrsTemReceita", lcsql)
*!*	          SELECT ucrstemreceita
*!*	          IF RECCOUNT("uCrsTemReceita")>0
*!*	             TEXT TO lcsql TEXTMERGE NOSHOW
*!*							select u_receita, u_nbenef, codAcesso, codDirOpcao, u_design, u_codigo 
*!*							from ft2 (nolock) where ft2stamp=(select top 1 ft.ftstamp 
*!*														from ft (nolock)
*!*														inner join fi fii (nolock) on fii.ftstamp=ft.ftstamp
*!*														where u_nratend in( select u_nratend 
*!*																				from ft (nolock)
*!*																				where ftstamp = (select ftstamp 
*!*																									from fi (nolock)
*!*																									where fistamp=(select fistamp 
*!*																														from bi2 (nolock)
*!*																														where bi2stamp='<<ALLTRIM(lcStampBi)>>')))
*!*							and fii.nmdoc='Inser��o de Receita'
*!*							and fii.ref in (select ref from bi (nolock)	where bistamp='<<ALLTRIM(lcStampBi)>>'))
*!*	             ENDTEXT
*!*	             uf_gerais_actgrelha("", "uCrsdadosreceita", lcsql)
*!*	             fecha("uCrsdadosreceita")
*!*	          ENDIF
          
          fecha("uCrsTemReceita")
          lccodcomp = ALLTRIM(&lccursor..codcomp)			
          SELECT &lccursor
          IF !EMPTY(&lccursor..stampadiantamento)
             lcfistampadiant = &lccursor..stampadiantamento
             lcsql = ''
             TEXT TO lcsql TEXTMERGE NOSHOW
						exec up_reservas_regularizarAdiantamento '<<lcFiStampAdiant>>'
             ENDTEXT
             uf_gerais_actgrelha("", "uCrsValeReserva", lcsql)
             
             SELECT ucrsvalereserva
             IF VARTYPE(ucrsvaleres.amostra)=='N'
                SELECT ftstamp, fistamp, fdata, ousrhora, nmdoc, fno, nome, no, estab, ncont, ref, design, desconto, desc2, u_descval, u_comp, qtt, qtt2, etiliquido, etiliquido2, totalcdesc, epv, ndoc, u_lote, u_nslote, u_lote2, u_nslote2, sel, copied, u_ltstamp, u_ltstamp2, u_codigo, u_codigo2, u_diploma, dev, fact, factpago, fmarcada, eaquisicao, oeaquisicao, u_psicont, u_bencont, u_receita, u_nbenef, u_nbenef2, u_nopresc, u_entpresc, u_recdata, u_medico, u_nmutdisp, u_moutdisp, u_cputdisp, u_nmutavi, u_moutavi, u_cputavi, u_ndutavi, u_ddutavi, u_idutavi, u_txcomp, u_epvp, u_ettent1, u_ettent2, docinvert, u_backoff, lancacc, u_design, u_abrev, u_abrev2, u_epref, tipodoc, tabiva, iva, opcao, token, codacesso, coddiropcao, id_validacaodem, amostra, c2codpost, lote, datavalidade  FROM uCrsValeReserva UNION ALL SELECT ftstamp, fistamp, fdata, ousrhora, nmdoc, fno, nome, no, estab, ncont, ref, design, desconto, desc2, u_descval, u_comp, qtt, qtt2, etiliquido, etiliquido2, totalcdesc, epv, ndoc, u_lote, u_nslote, u_lote2, u_nslote2, sel, copied, u_ltstamp, u_ltstamp2, u_codigo, u_codigo2, u_diploma, dev, fact, factpago, fmarcada, eaquisicao, oeaquisicao, u_psicont, u_bencont, u_receita, u_nbenef, u_nbenef2, u_nopresc, u_entpresc, u_recdata, u_medico, u_nmutdisp, u_moutdisp, u_cputdisp, u_nmutavi, u_moutavi, u_cputavi, u_ndutavi, u_ddutavi, u_idutavi, u_txcomp, u_epvp, u_ettent1, u_ettent2, docinvert, u_backoff, lancacc, u_design, u_abrev, u_abrev2, u_epref, tipodoc, tabiva, iva, opcao, token, codacesso, coddiropcao, id_validacaodem, IIF(ucrsvaleres.amostra=0, .F., .T.) AS amostra, c2codpost, lote, datavalidade FROM uCrsValeRes INTO CURSOR ucrsValeRes1 READWRITE
             ELSE
                SELECT ftstamp, fistamp, fdata, ousrhora, nmdoc, fno, nome, no, estab, ncont, ref, design, desconto, desc2, u_descval, u_comp, qtt, qtt2, etiliquido, etiliquido2, totalcdesc, epv, ndoc, u_lote, u_nslote, u_lote2, u_nslote2, sel, copied, u_ltstamp, u_ltstamp2, u_codigo, u_codigo2, u_diploma, dev, fact, factpago, fmarcada, eaquisicao, oeaquisicao, u_psicont, u_bencont, u_receita, u_nbenef, u_nbenef2, u_nopresc, u_entpresc, u_recdata, u_medico, u_nmutdisp, u_moutdisp, u_cputdisp, u_nmutavi, u_moutavi, u_cputavi, u_ndutavi, u_ddutavi, u_idutavi, u_txcomp, u_epvp, u_ettent1, u_ettent2, docinvert, u_backoff, lancacc, u_design, u_abrev, u_abrev2, u_epref, tipodoc, tabiva, iva, opcao, token, codacesso, coddiropcao, id_validacaodem, amostra, c2codpost, lote, dataValidade FROM uCrsValeReserva UNION ALL SELECT ftstamp, fistamp, fdata, ousrhora, nmdoc, fno, nome, no, estab, ncont, ref, design, desconto, desc2, u_descval, u_comp, qtt, qtt2, etiliquido, etiliquido2, totalcdesc, epv, ndoc, u_lote, u_nslote, u_lote2, u_nslote2, sel, copied, u_ltstamp, u_ltstamp2, u_codigo, u_codigo2, u_diploma, dev, fact, factpago, fmarcada, eaquisicao, oeaquisicao, u_psicont, u_bencont, u_receita, u_nbenef, u_nbenef2, u_nopresc, u_entpresc, u_recdata, u_medico, u_nmutdisp, u_moutdisp, u_cputdisp, u_nmutavi, u_moutavi, u_cputavi, u_ndutavi, u_ddutavi, u_idutavi, u_txcomp, u_epvp, u_ettent1, u_ettent2, docinvert, u_backoff, lancacc, u_design, u_abrev, u_abrev2, u_epref, tipodoc, tabiva, iva, opcao, token, codacesso, coddiropcao, id_validacaodem, amostra, c2codpost, lote, dataValidade FROM uCrsValeRes INTO CURSOR ucrsValeRes1 READWRITE
             ENDIF
             fecha("ucrsValeRes")
             SELECT * FROM ucrsValeRes1 INTO CURSOR ucrsValeRes READWRITE
             fecha("ucrsValeRes1")
             SELECT * FROM uCrsValeReserva INTO CURSOR ucrsVale2 READWRITE
             SELECT * FROM uCrsValeReserva INTO CURSOR ucrsVale READWRITE
             SELECT ucrsvale2
             uf_regvendas_regvendassel(.t., IIF(&lccursor..fno==lnobrano,.t.,.f.))
             lnobrano = &lccursor..fno
             SELECT ucrsvalereserva
             SELECT fi
             LOCATE FOR ALLTRIM(fi.ofistamp)==ALLTRIM(lcfistampadiant)
             IF FOUND()
                REPLACE fi.design WITH "  "+ucrsvalereserva.design IN "FI"
                REPLACE fi.bistamp WITH &lccursor..fistamp IN "FI"
                REPLACE fi.bostamp WITH &lccursor..ftstamp IN "FI"
                lndesc = fi.u_descval
             ENDIF
             

             uf_atendimento_devolver()
             fecha("uCrsValeReserva")
             fecha("uCrsVale2")
          ENDIF
          
          

          
       
          
          IF !EMPTY(ALLTRIM(&lccursor..codcomp))
             lcvalidaaplicacomp = .T.
          ELSE
             lcvalidaaplicacomp = .F.
          ENDIF
          SELECT fi
          GOTO BOTTOM
          SELECT fi
          uf_atendimento_adicionalinhafi(.F., .F.)
          SELECT fi
          lcposfi = RECNO("fi")
          SELECT &lccursor
          SELECT fi
          REPLACE fi.ref WITH &lccursor..ref
          SELECT fi
          uf_atendimento_actref()
          SELECT fi
          lcref = ALLTRIM(fi.ref)
          lcsql = ''
          TEXT TO lcsql TEXTMERGE NOSHOW
		            exec up_stocks_procuraRef '<<Alltrim(lcRef)>>', <<mysite_nr>>
          ENDTEXT
          uf_gerais_actgrelha("", "uCrsFindRef", lcsql)
          lcarvato = ucrsfindref.dispositivo_seguranca
          IF lcarvato
             uf_insert_fi_trans_info_seminfo()
             atendimento.pgfdados.page2.txtref.disabledbackcolor = RGB(255, 0, 0)
             atendimento.pgfdados.page2.txtref.forecolor = RGB(255, 255, 255)
          ENDIF
          fecha("uCrsFindRef")
          SELECT fi
          TRY
             GOTO lcposfi
          CATCH
             GOTO BOTTOM
          ENDTRY
          IF LEN(ALLTRIM(lcnrreceitalinres))>0
          	replace fi.receita WITH ALLTRIM(lcnrreceitalinres)
          	**colocar o cabecalho com o numero de receita
          	**LOCAL lcLordemCab
          	**lcLordemCab = LEFT(astr(fi.lordem), 2)+REPLICATE('0', LEN(astr(fi.lordem))-2)
          	**uf_atendimento_fixPrimeiroCabecalhoReserva(ALLTRIM(lcnrreceitalinres), lcLordemCab )
          ENDIF 
          IF lcvalidapreco
             REPLACE fi.amostra WITH .F. IN "FI"
          ELSE
             SELECT &lccursor
             SELECT fi
             REPLACE fi.u_epvp	WITH	ROUND(&lccursor..pvpres,2) IN "FI"
             REPLACE fi.amostra WITH .T. IN "FI"
          ENDIF
          SELECT &lccursor
          SELECT fi
          REPLACE fi.bistamp	WITH &lccursor..fistamp IN "FI"
          REPLACE fi.bostamp	WITH &lccursor..ftstamp IN "FI"
          REPLACE fi.qtt		WITH &lccursor..qtt IN "FI"
          REPLACE fi.u_descval WITH &lccursor..descval + lndesc IN "FI"
          REPLACE fi.desconto WITH &lccursor..desconto IN "FI"
          REPLACE fi.u_diploma	WITH	ALLTRIM(&lccursor..diploma) IN "FI"
          REPLACE fi.lobs3		WITH	ALLTRIM(&lccursor..exepcaotrocamed) IN "FI"
          
   


		  uv_oriStamp = &lccursor..fistamp

		  IF !EMPTY(uv_oriStamp)

			TEXT TO msel TEXTMERGE NOSHOW
				EXEC up_vendas_getDadosBonusBAS '<<ALLTRIM(uv_oriStamp)>>'
			ENDTEXT

			IF uf_gerais_actGrelha("", "uc_tmpOriFI2", msel)

				SELECT FI

				uv_curStamp = fi.fistamp

				SELECT fi2
				LOCATE FOR fi2.fistamp = uv_curStamp

				IF FOUND()

					SELECT uc_tmpOriFI2

					REPLACE fi2.bonusTipo WITH uc_tmpOriFI2.bonusTipo,;
							fi2.bonusId WITH uc_tmpOriFI2.bonusId,;
							fi2.bonusDescr WITH uc_tmpOriFI2.bonusDescr
							

				ENDIF

			ENDIF

		  ENDIF

          uv_posologia = ''

          IF !EMPTY(fi.bostamp) AND uf_gerais_getUmValor("bo", "ndos", "bostamp = '" + fi.bostamp + "'") = 5

            uv_posologia = uf_gerais_getUmvalor("fi", "posologia", "ref = '" + fi.ref + "' and ftstamp in (select ft2stamp from ft2(nolock) where stampreserva = '" + fi.bostamp + "')")

            IF !EMPTY(uv_posologia)

                REPLACE fi.posologia WITH uv_posologia

                SELECT UCRSATENDST
                LOCATE FOR uCrsAtendST.ststamp = fi.fistamp
                
                IF FOUND()
                    REPLACE UCRSATENDST.u_posprog with uv_posologia
                ENDIF
            ENDIF
            
          ENDIF
		  
	
          uf_atendimento_fiiliq()
          SELECT fi
          TRY
             GOTO lcposfi
          CATCH
             GOTO BOTTOM
          ENDTRY
          uf_atendimento_eventofi()
          SELECT fi
          TRY
             GOTO lcposfi
          CATCH
             GOTO BOTTOM
          ENDTRY
          uf_atendimento_dadosst()
          SELECT fi
          TRY
             GOTO lcposfi
          CATCH
             GOTO BOTTOM
          ENDTRY
          uf_atendimento_infototais()
          SELECT fi
          TRY
             GOTO lcposfi
          CATCH
             GOTO BOTTOM
          ENDTRY
		
		
	
          
          IF !EMPTY(&lccursor..stampadiantamento)	
             IF  .NOT. EMPTY(ALLTRIM(uf_atendimento_verificacompart()))
                DO WHILE  .NOT. BOF("FI")
                   IF LEFT(fi.design, 1)=="."
                      IF LEFT(fi.design, 2)<>".S"
                         IF RIGHT(ALLTRIM(fi.design), 10)=="*SUSPENSA*" .OR. RIGHT(ALLTRIM(fi.design), 18)=="*SUSPENSA RESERVA*"
                         ELSE
                            lcstampfiversusp = &lccursor..stampadiantamento
                            lcsql = ''
                            TEXT TO lcsql TEXTMERGE NOSHOW
											DECLARE @stampft char(25)
											
											SELECT @stampft = ftstamp FROM fi (nolock) WHERE fistamp = '<<ALLTRIM(lcStampFiVerSusp)>>'
											SELECT cobrado FROM ft WHERE ftstamp = @stampft
                            ENDTEXT
                            uf_gerais_actgrelha("", "crsVerSuspTmp", lcsql)
                            SELECT crsversusptmp
                            IF crsversusptmp.cobrado==.T.
                            ELSE
                               REPLACE fi.design WITH ALLTRIM(fi.design)+" *SUSPENSA RESERVA*" IN "FI"
                            ENDIF
                         ENDIF
                         EXIT
                      ENDIF
                      EXIT
                   ENDIF
                   TRY
                      SKIP -1
                   CATCH
                   ENDTRY
                ENDDO
             ENDIF
          ENDIF
          SELECT fi
          TRY
             GOTO lcposfi
          CATCH
             GOTO BOTTOM
          ENDTRY
          
		 	
          IF lcvalidaaplicacomp
             SELECT fi
             DO WHILE  .NOT. BOF("FI")
                SELECT fi
                IF LEFT(fi.design, 1)=="."
                  
                  
                   IF uf_atendimento_verificacompart()==""  
                             	 
                      uf_atendimento_aplicacompart(ALLTRIM(&lccursor..codcomp),.T.,.t.) 
                                      
                   ENDIF
              		
                   IF uf_gerais_getparameter("ADM0000000222", "BOOL") .AND.  .NOT. uf_atendimento_verificasuspensa()
        				uf_gerais_meiaRegua("A suspender a venda...")	
                     	uf_atendimento_suspendevenda(.T.)
                     	regua(2)
                   ENDIF
                   
                   
                   EXIT
                ENDIF
                TRY
                   SKIP -1
                CATCH
                ENDTRY
             ENDDO
          ENDIF
          SELECT fi
          TRY
             GOTO lcposfi
          CATCH
             GOTO BOTTOM
          ENDTRY
       ENDIF
		

       
       TEXT TO lcsql TEXTMERGE NOSHOW
				select TOP 1 *,cabVendasOrdem = 0, u_dcutavi = getdate()-(365*u_idutavi), CAST('' as varchar(200)) as contactoUt from B_dadosPsico(nolock) 
				where ftstamp in (select ft.ftstamp from ft (nolock) 
									inner join fi(nolock) on fi.fistamp=ft.ftstamp
									where ft.nmdoc='Inser��o de Receita' and fi.ref='<<ALLTRIM(ucrsregvendas.ref)>>' and u_nratend= (select u_nratend from bo2 (nolock) where bo2stamp='<<ALLTRIM(ucrsregvendas.ftstamp)>>'))
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "dadosPsico2", lcsql)
          uf_perguntalt_chama("N�o foi possivel verificar os dados dos psicotr�picos para esta regulariza��o.", "", "OK", 16)
          RETURN .F.
       ENDIF
       
    
       
       IF RECCOUNT("dadosPsico2")>0
       
          SELECT * FROM dadosPsico2 INTO CURSOR dadosPsico READWRITE
          REPLACE ucrsatendcl.u_nmutavi WITH ALLTRIM(dadospsico.u_nmutavi)
          REPLACE ucrsatendcl.u_moutavi WITH ALLTRIM(dadospsico.u_moutavi)
          REPLACE ucrsatendcl.u_cputavi WITH ALLTRIM(dadospsico.u_cputavi)
          REPLACE ucrsatendcl.u_dcutavi WITH dadospsico.u_dcutavi
          REPLACE ucrsatendcl.u_ndutavi WITH ALLTRIM(dadospsico.u_ndutavi)
          REPLACE ucrsatendcl.u_ddutavi WITH dadospsico.u_ddutavi
          REPLACE ucrsatendcl.u_idutavi WITH dadospsico.u_idutavi
          REPLACE ft.morada WITH ALLTRIM(dadospsico.u_moutdisp)
          REPLACE ft.codpost WITH ALLTRIM(dadospsico.u_cputdisp)
          uf_regvendas_adiciona_medico_psico()
          

          atendimento.pgfdados.page1.refresh
       ENDIF
       LOCAL lcnocl, lcestabcl, lcnomecl, lcncont
       lcnocl = &lccursor..no
       lcestabcl = &lccursor..estab
       lcnomecl = ALLTRIM(&lccursor..NOME)
       lcncont = ALLTRIM(&lccursor..ncont)
       IF lcnocl<>0
          SELECT ucrsatendcl
          IF ucrsatendcl.no<>lcnocl .OR. ucrsatendcl.estab<>lcestabcl
             uf_atendimento_selcliente(lcnocl, lcestabcl, "ATENDIMENTO")
          ENDIF
          IF  .NOT. EMPTY(lcnomecl) .AND. lcnocl==200 .AND.  .NOT. (UPPER(ALLTRIM(lcnomecl))==UPPER(ALLTRIM(ucrsatendcl.nome)))
             SELECT ucrsatendcl
             REPLACE ucrsatendcl.nome WITH ALLTRIM(lcnomecl)
             REPLACE ucrsatendcl.ncont WITH ALLTRIM(lcncont)
             SELECT ft
             REPLACE ft.nome WITH ALLTRIM(lcnomecl)
             REPLACE ft.ncont WITH ALLTRIM(lcncont)
             atendimento.refresh()
          ENDIF
       ENDIF
    ELSE
    ENDIF
    SELECT &lccursor
 ENDSCAN
 
 set exact off
 
 atendimento.lockscreen = .F.
 atendimento.refresh()

 fecha("uCrsRegVendas")
 SELECT ucrsregvendastemp
 SELECT * FROM uCrsRegVendasTemp INTO CURSOR uCrsRegVendas READWRITE
 fecha("uCrsRegVendasTemp")
 
 
ENDFUNC


FUNCTION uf_regvendas_adiciona_medico_psico

      
  if(USED("ft") AND USED("dadospsico"))
  		
  	  LOCAL lcPosFiAux
  	  lcPosFiAux= RECNO("fi")	
  		
  	  LOCAL lcfFtstampPsico, lcMedicoIdPsico
  	  STORE '' TO lcfFtstampPsico, lcMedicoIdPsico
  	  SELECT ft
      lcfFtstampPsico = ALLTRIM(ft.ftstamp)

      SELECT dadospsico
      lcMedicoIdPsico =ALLTRIM(dadospsico.u_medico)


      **coloca o medico na fi
 		  UPDATE fi SET fi.szzstamp = lcMedicoIdPsico  where ftstamp=ALLTRIM(ftstamp) AND LEFT(ALLTRIM(fi.design), 1)=='.'
      
      SELECT fi
      TRY
         GOTO lcPosFiAux
      CATCH
         GOTO BOTTOM
      ENDTRY
  ENDIF

ENDFUNC


**
FUNCTION uf_regvendas_reservas_imprimir
 SELECT ucrsregvendas
 LOCATE FOR ucrsregvendas.sel==.T. .AND. ALLTRIM(ucrsregvendas.nmdoc)="Reserva de Cliente"
 IF  .NOT. FOUND()
    uf_perguntalt_chama("Por favor seleccione uma reserva para imprimir.", "OK", "", 16)
    RETURN .F.
 ENDIF
 SELECT ucrsregvendas
 GOTO TOP
 SELECT DISTINCT ftstamp FROM uCrsRegVendas WHERE ucrsregvendas.sel=.T. AND ALLTRIM(ucrsregvendas.nmdoc)="Reserva de Cliente" INTO CURSOR uCrsRegVendasImpRes READWRITE
 SELECT ucrsregvendasimpres
 GOTO TOP
 SCAN
    uf_reservas_imprimetalaopos(.T., ucrsregvendasimpres.ftstamp)
 ENDSCAN
 fecha("uCrsRegVendasImpRes")
 SELECT ucrsregvendas
 GOTO TOP
ENDFUNC
**
PROCEDURE uf_regvendas_outraslojas
 LOCAL lccursor
 lccursor = "uCrsRegVendas"
 IF pesqmmnif=0
    SELECT &lccursor
    pesqmmnif = 1
    GOTO TOP
    regvendas.menu1.multiemp.config("Outras Lojas", mypath+"\imagens\icons\checked_w.png", "uf_regvendas_outraslojas", "F10")
 ELSE
    SELECT &lccursor
    pesqmmnif = 0
    GOTO TOP
    regvendas.menu1.multiemp.config("Outras Lojas", mypath+"\imagens\icons\unchecked_w.png", "uf_regvendas_outraslojas", "F10")
 ENDIF
 uf_regvendas_configgrdregvendas()
ENDPROC
**
PROCEDURE uf_regvendas_fecha_liberta_receitas
 LPARAMETERS lcresstampcab, lcresstamplin
 LOCAL nrnrat, nrreceita, lcftstampanul
 STORE '' TO nrnrat, nrreceita, lcftstampanul
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		select u_nratend from bo2 where bo2stamp='<<ALLTRIM(lcResStampCab)>>'
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsNoNraAT", lcsql)
    uf_perguntalt_chama("O software n�o conseguiu verificar o n� do atendimento da reserva. Por favor contacte o suporte.", "OK", "", 16)
    lcvalidadoc = .T.
 ELSE
    IF RECCOUNT("uCrsNoNraAT")>0 .AND.  .NOT. EMPTY(ucrsnonraat.u_nratend)
       SELECT ucrsnonraat
       nrnrat = ALLTRIM(ucrsnonraat.u_nratend)
       TEXT TO lcsql TEXTMERGE NOSHOW
				select ftstamp,fdata,u_nratend, u_tlote, u_tlote2, u_receita, u_nbenef, codAcesso, codDirOpcao, u_design, u_codigo, u_abrev, u_abrev2  from ft (nolock) 
				inner join ft2 (nolock) on ft.ftstamp=ft2.ft2stamp 
				where nmdoc ='Inser��o de Receita' and u_nratend='<<ALLTRIM(nrNrAt)>>'
       ENDTEXT
       uf_gerais_actgrelha("", "uCrsDadosRecRes", lcsql)
       IF ALLTRIM(ucrsdadosrecres.u_tlote)=='96' .OR. ALLTRIM(ucrsdadosrecres.u_tlote)=='97' .OR. ALLTRIM(ucrsdadosrecres.u_tlote2)=='96' .OR. ALLTRIM(ucrsdadosrecres.u_tlote2)=='97' .OR. ALLTRIM(ucrsdadosrecres.u_tlote)=='98' .OR. ALLTRIM(ucrsdadosrecres.u_tlote)=='99' .OR. ALLTRIM(ucrsdadosrecres.u_tlote2)=='98' .OR. ALLTRIM(ucrsdadosrecres.u_tlote2)=='99'
          nrreceita = ALLTRIM(ucrsdadosrecres.u_receita)
          lcftstampanul = ALLTRIM(ucrsdadosrecres.ftstamp)
       ENDIF
    ELSE
       lcsql = ''
       TEXT TO lcsql TEXTMERGE NOSHOW
				select count(ftstamp) 
				from ft (nolock) 
				where u_nratend in( select u_nratend from ft (nolock) where ftstamp in (select ftstamp from fi (nolock) where fistamp=(select fistamp from bi2 (nolock) where bi2stamp='<<ALLTRIM(lcResStampLin)>>')))
				and nmdoc='Inser��o de Receita'
       ENDTEXT
       uf_gerais_actgrelha("", "uCrsTemReceita", lcsql)
       SELECT ucrstemreceita
       IF RECCOUNT("uCrsTemReceita")>0
          TEXT TO lcsql TEXTMERGE NOSHOW
					select ftstamp,fdata,u_nratend, u_tlote, u_tlote2, u_receita, u_nbenef, codAcesso, codDirOpcao, u_design, u_codigo, u_abrev, u_abrev2
						from ft2 (nolock) 
						inner join ft ftt on ftt.ftstamp=ft2.ft2stamp where ft2stamp=(select ftstamp 
												from ft (nolock)
												where u_nratend in( select u_nratend 
																		from ft (nolock)
																		where ftstamp = (select ftstamp 
																							from fi 
																							where fistamp=(select fistamp 
																												from bi2 (nolock)
																												where bi2stamp='<<ALLTRIM(lcResStampLin)>>')))
					and nmdoc='Inser��o de Receita') 
          ENDTEXT
          uf_gerais_actgrelha("", "uCrsdadosreceita", lcsql)
          IF ALLTRIM(ucrsdadosreceita.u_tlote)=='96' .OR. ALLTRIM(ucrsdadosreceita.u_tlote)=='97' .OR. ALLTRIM(ucrsdadosreceita.u_tlote2)=='96' .OR. ALLTRIM(ucrsdadosreceita.u_tlote2)=='97' .OR. ALLTRIM(ucrsdadosreceita.u_tlote)=='98' .OR. ALLTRIM(ucrsdadosreceita.u_tlote)=='99' .OR. ALLTRIM(ucrsdadosreceita.u_tlote2)=='98' .OR. ALLTRIM(ucrsdadosreceita.u_tlote2)=='99'
             nrreceita = ALLTRIM(ucrsdadosreceita.u_receita)
             lcftstampanul = ALLTRIM(ucrsdadosreceita.ftstamp)
          ENDIF
          fecha("uCrsdadosreceita")
       ENDIF
       fecha("uCrsTemReceita")
    ENDIF
 ENDIF
 IF USED("uCrsNoNraAT")
    fecha("uCrsNoNraAT")
 ENDIF
 IF USED("uCrsDadosRecRes")
    fecha("uCrsDadosRecRes")
 ENDIF
 IF  .NOT. EMPTY(nrreceita)
    LOCAL lcnrreceitabusca, lnnratendbusca
    STORE '' TO lcnrreceitabusca, lnnratendbusca
    TEXT TO lcsql TEXTMERGE NOSHOW
			select ft2.u_receita, ft2.token, ft.u_lote, ft.u_lote2 , u_nratend
			from ft (nolock)
			inner join ft2 (nolock) on ft.ftstamp=ft2.ft2stamp
			where ftstamp='<<ALLTRIM(LcFtstampAnul)>>'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "uCrsRecSLote", lcsql)
       uf_perguntalt_chama("Ocorreu ao verificar a informa��o da receita a eliminar. Por favor contacte o Suporte.", "OK", "", 16)
    ELSE
       IF  .NOT. EMPTY(ucrsrecslote.u_receita) .AND.  .NOT. EMPTY(ucrsrecslote.token)
          lcnrreceitabusca = ALLTRIM(ucrsrecslote.u_receita)
          lnnratendbusca = ALLTRIM(ucrsrecslote.u_nratend)
       ENDIF
    ENDIF
    fecha("uCrsRecSLote")
    IF  .NOT. EMPTY(lcnrreceitabusca)
       TEXT TO lcsql TEXTMERGE NOSHOW
				select fi.fistamp from ft2 
				inner join ft on ft2.ft2stamp=ft.ftstamp
				inner join fi on fi.ftstamp=ft.ftstamp
				 where u_receita='<<ALLTRIM(lcNrReceitaBusca)>>'	and 
				 u_nratend='<<ALLTRIM(lnNrAtendBusca)>>' and ft.nmdoc='Venda a Dinheiro'
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "uCrsFiReceita", lcsql)
          uf_perguntalt_chama("Ocorreu ao verificar a informa��o dos produtos faturados da receita. Por favor contacte o Suporte.", "OK", "", 16)
       ELSE
          IF RECCOUNT("uCrsFiReceita")>0
             SELECT ucrsfireceita
             GOTO TOP
             SCAN
                LOCAL lcstampfianular
                lcstampfianular = ALLTRIM(ucrsfireceita.fistamp)
                lcsql = ''
                TEXT TO lcsql TEXTMERGE NOSHOW
							exec up_reservas_regularizarAdiantamento '<<LcStampfianular>>'
                ENDTEXT
                uf_gerais_actgrelha("", "uCrsValeReserva", lcsql)
                IF USED("ucrsVale")
                   SELECT * FROM ucrsVale UNION ALL SELECT * FROM uCrsValeReserva INTO CURSOR ucrsVale2 READWRITE
                ELSE
                   SELECT * FROM uCrsValeReserva INTO CURSOR ucrsVale2 READWRITE
                ENDIF
                SELECT ucrsvale2
                uf_regvendas_regvendassel(.T., .T.)
                SELECT ucrsvalereserva
                SELECT fi
                LOCAL lordem1
                lordem1 = fi.lordem
                lndesc = fi.u_descval
                SELECT fi
                uf_atendimento_devolver()
                UPDATE fi SET partes = 1 WHERE LEFT(ALLTRIM(STR(lordem1)), 1)==LEFT(ALLTRIM(STR(fi.lordem)), 1)
                fecha("uCrsValeReserva")
             ENDSCAN
             fecha("uCrsVale2")
          ENDIF
       ENDIF
       fecha("uCrsFiReceita")
    ENDIF
 ENDIF
ENDPROC
**
FUNCTION uf_regvendas_encomendas_sel
 LOCAL lcstampant, lcstampact
 STORE '' TO lcstampant, lcstampact
 lccursor = "ucrsenccl"
 SELECT &lccursor
 GOTO TOP
 SCAN
    lcstampbi = &lccursor..fistamp
    SELECT fi
    SELECT fi
    LOCATE FOR ALLTRIM(fi.bistamp)==ALLTRIM(lcstampbi)
    IF FOUND("FI")
       lcvalidaexiste = .T.
    ELSE
       lcvalidaexiste = .F.
    ENDIF
    IF  .NOT. lcvalidaexiste
       lcstampact =  ALLTRIM(&lccursor..ftstamp)
       LOCAL lnencnrreceita, lnencpinreceita, lnencpinopreceita, lnencpagam, lnencmompagam
       STORE '' TO lnencnrreceita, lnencpinreceita, lnencpinopreceita, lnencpagam, lnencmompagam
       TEXT TO lcsql TEXTMERGE NOSHOW
				select nrReceita, pinAcesso, pinOpcao, pagamento, momento_pagamento from bo2 (nolock) where bo2stamp='<<ALLTRIM(lcstampact)>>'
       ENDTEXT
       
       IF  .NOT. uf_gerais_actgrelha("", "ucrsDadosRecEnc", lcsql)
          MESSAGEBOX("OCORREU UMA ANOMALIA A VERIFICAR OS DADOS DA ENCOMENDA.", 16, "LOGITOOLS SOFTWARE")
          RETURN .F.
       ENDIF
       
       SELECT ucrsdadosrecenc
       IF  .NOT. EMPTY(ucrsdadosrecenc.nrreceita)
          lnencnrreceita = ALLTRIM(ucrsdadosrecenc.nrreceita)
          lnencpinreceita = ALLTRIM(ucrsdadosrecenc.pinacesso)
          lnencpinopreceita = ALLTRIM(ucrsdadosrecenc.pinopcao)
          lnencpagam = ALLTRIM(ucrsdadosrecenc.pagamento)
          lnencmompagam = ALLTRIM(ucrsdadosrecenc.momento_pagamento)
          receitaencomenda = .T.
          nrreceitaenc = ALLTRIM(lnencnrreceita)
          codreceitaenc = ALLTRIM(lnencpinreceita)
          cdoreceitaenc = ALLTRIM(lnencpinopreceita)
       ENDIF
       SELECT fi
       GOTO BOTTOM
       IF LEFT(ALLTRIM(fi.design), 1)<>'.' .AND. (ALLTRIM(lcstampact)<>ALLTRIM(lcstampant) .OR. EMPTY(lcstampant))
          SELECT fi
          GOTO BOTTOM
          uf_atendimento_novavenda(.F.)
          IF  .NOT. EMPTY(lnencnrreceita)
             REPLACE fi.encreceita WITH ALLTRIM(lnencnrreceita)
             REPLACE fi.encpinreceita WITH ALLTRIM(lnencpinreceita)
             REPLACE fi.enccodopreceita WITH ALLTRIM(lnencpinopreceita)
             REPLACE fi.encpagam WITH ALLTRIM(lnencpagam)
             REPLACE fi.encmompagam WITH ALLTRIM(lnencmompagam)
             receitaencomenda = .T.
             nrreceitaenc = ALLTRIM(lnencnrreceita)
             codreceitaenc = ALLTRIM(lnencpinreceita)
             cdoreceitaenc = ALLTRIM(lnencpinopreceita)
          ENDIF
          REPLACE fi.bostamp WITH ALLTRIM(lcstampact)
          REPLACE fi.nmdos WITH 'Encomenda de Cliente'
       ENDIF
       SELECT fi
       lcposfi = RECNO("fi")
       IF  .NOT. EMPTY(lnencnrreceita) .AND. RECCOUNT("fi")=1
          UPDATE fi SET fi.bostamp = ALLTRIM(lcstampact), fi.encreceita = ALLTRIM(lnencnrreceita), fi.encpinreceita = ALLTRIM(lnencpinreceita), fi.enccodopreceita = ALLTRIM(lnencpinopreceita), fi.nmdos = 'Encomenda de Cliente' WHERE lordem=100000
          receitaencomenda = .T.
          nrreceitaenc = ALLTRIM(lnencnrreceita)
          codreceitaenc = ALLTRIM(lnencpinreceita)
          cdoreceitaenc = ALLTRIM(lnencpinopreceita)
       ENDIF
       IF EMPTY(lnencnrreceita) .AND. RECCOUNT("fi")=1
          UPDATE fi SET fi.bostamp = ALLTRIM(lcstampact) WHERE lordem=100000
       ENDIF
       SELECT fi
       TRY
          GOTO lcposfi
       CATCH
          GOTO BOTTOM
       ENDTRY
       SELECT &lccursor
       lccodcomp = ALLTRIM(&lccursor..codcomp)			
       SELECT fi
       GOTO BOTTOM
       uf_atendimento_adicionalinhafi(.F., .F.)
       SELECT fi
       lcposfi = RECNO("fi")
       SELECT &lccursor
       SELECT fi
       REPLACE fi.ref WITH &lccursor..ref
       REPLACE fi.encomenda WITH .T.
       IF  .NOT. EMPTY(lnencnrreceita)
          REPLACE fi.encreceita WITH ALLTRIM(lnencnrreceita)
          REPLACE fi.encpinreceita WITH ALLTRIM(lnencpinreceita)
          REPLACE fi.enccodopreceita WITH ALLTRIM(lnencpinopreceita)
       ENDIF
       REPLACE fi.nmdos WITH 'Encomenda de Cliente'
       SELECT fi
       uf_atendimento_actref()
       SELECT fi
       lcref = ALLTRIM(fi.ref)
       lcsql = ''
       TEXT TO lcsql TEXTMERGE NOSHOW
	            exec up_stocks_procuraRef '<<Alltrim(lcRef)>>', <<mysite_nr>>
       ENDTEXT
       uf_gerais_actgrelha("", "uCrsFindRef", lcsql)
       lcarvato = ucrsfindref.dispositivo_seguranca
       IF lcarvato
          uf_insert_fi_trans_info_seminfo()
          atendimento.pgfdados.page2.txtref.disabledbackcolor = RGB(255, 0, 0)
          atendimento.pgfdados.page2.txtref.forecolor = RGB(255, 255, 255)
       ENDIF
       fecha("uCrsFindRef")
       SELECT fi
       TRY
          GOTO lcposfi
       CATCH
          GOTO BOTTOM
       ENDTRY
       SELECT &lccursor
       SELECT fi
       REPLACE fi.u_epvp	WITH	ROUND(&lccursor..pvpres,2) IN "FI"
       REPLACE fi.amostra WITH .T. IN "FI"
       SELECT &lccursor
       SELECT fi
       REPLACE fi.bistamp	WITH &lccursor..fistamp IN "FI"
       REPLACE fi.bostamp	WITH &lccursor..ftstamp IN "FI"
       REPLACE fi.qtt		WITH &lccursor..qtt IN "FI"
       REPLACE fi.u_descval WITH &lccursor..descval IN "FI"
       REPLACE fi.desconto WITH &lccursor..desconto IN "FI"
       REPLACE fi.u_diploma	WITH	ALLTRIM(&lccursor..diploma) IN "FI"
       REPLACE fi.lobs3		WITH	ALLTRIM(&lccursor..exepcaotrocamed) IN "FI"
       uf_atendimento_fiiliq()
       SELECT fi
       TRY
          GOTO lcposfi
       CATCH
          GOTO BOTTOM
       ENDTRY
       uf_atendimento_eventofi()
       SELECT fi
       TRY
          GOTO lcposfi
       CATCH
          GOTO BOTTOM
       ENDTRY
       uf_atendimento_dadosst()
       SELECT fi
       TRY
          GOTO lcposfi
       CATCH
          GOTO BOTTOM
       ENDTRY
       uf_atendimento_infototais()
       SELECT fi
       TRY
          GOTO lcposfi
       CATCH
          GOTO BOTTOM
       ENDTRY
       SELECT fi
       TRY
          GOTO lcposfi
       CATCH
          GOTO BOTTOM
       ENDTRY
       SELECT fi
       TRY
          GOTO lcposfi
       CATCH
          GOTO BOTTOM
       ENDTRY
    ENDIF
    TEXT TO lcsql TEXTMERGE NOSHOW
			select *,cabVendasOrdem = 0, u_dcutavi = getdate()-(365*u_idutavi), CAST('' as varchar(200)) as contactoUt from B_dadosPsico where ftstamp in (select ftstamp from ft (nolock) where nmdoc='Inser��o de Receita' and  u_nratend= (select u_nratend from bo2 (nolock) where bo2stamp='<<ALLTRIM(ucrsregvendas.ftstamp)>>'))
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "dadosPsico2", lcsql)
       uf_perguntalt_chama("N�o foi possivel verificar os dados dos psicotr�picos para esta regulariza��o.", "", "OK", 16)
       RETURN .F.
    ENDIF
    IF RECCOUNT("dadosPsico2")>0
  
       SELECT * FROM dadosPsico2 INTO CURSOR dadosPsico READWRITE
       REPLACE ucrsatendcl.u_nmutavi WITH ALLTRIM(dadospsico.u_nmutavi)
       REPLACE ucrsatendcl.u_moutavi WITH ALLTRIM(dadospsico.u_moutavi)
       REPLACE ucrsatendcl.u_cputavi WITH ALLTRIM(dadospsico.u_cputavi)
       REPLACE ucrsatendcl.u_dcutavi WITH dadospsico.u_dcutavi
       REPLACE ucrsatendcl.u_ndutavi WITH ALLTRIM(dadospsico.u_ndutavi)
       REPLACE ucrsatendcl.u_ddutavi WITH dadospsico.u_ddutavi
       REPLACE ucrsatendcl.u_idutavi WITH dadospsico.u_idutavi
       REPLACE ft.morada WITH ALLTRIM(dadospsico.u_moutdisp)
       REPLACE ft.codpost WITH ALLTRIM(dadospsico.u_cputdisp)
       atendimento.pgfdados.page1.refresh
    ENDIF
    LOCAL lcnocl, lcestabcl, lcnomecl, lcncont
    lcnocl = &lccursor..no
    lcestabcl = &lccursor..estab
    lcnomecl = ALLTRIM(&lccursor..NOME)
    lcncont = ALLTRIM(&lccursor..ncont)
    IF lcnocl<>0
       SELECT ucrsatendcl
       IF ucrsatendcl.no<>lcnocl .OR. ucrsatendcl.estab<>lcestabcl
          uf_atendimento_selcliente(lcnocl, lcestabcl, "ATENDIMENTO")
       ENDIF
       IF  .NOT. EMPTY(lcnomecl) .AND. lcnocl==200 .AND.  .NOT. (UPPER(ALLTRIM(lcnomecl))==UPPER(ALLTRIM(ucrsatendcl.nome)))
          SELECT ucrsatendcl
          REPLACE ucrsatendcl.nome WITH ALLTRIM(lcnomecl)
          REPLACE ucrsatendcl.ncont WITH ALLTRIM(lcncont)
          SELECT ft
          REPLACE ft.nome WITH ALLTRIM(lcnomecl)
          REPLACE ft.ncont WITH ALLTRIM(lcncont)
          atendimento.refresh()
       ENDIF
    ENDIF
    SELECT &lccursor
    lcstampant =  ALLTRIM(&lccursor..ftstamp)
 ENDSCAN
ENDFUNC
**
FUNCTION uf_regvendas_encomendasslin_sel
 LOCAL lcstampant, lcstampact
 STORE '' TO lcstampant, lcstampact
 lccursor = "ucrsencclslin"
 SELECT &lccursor
 GOTO TOP
 SCAN
    lcstampact =  ALLTRIM(&lccursor..ftstamp)
    LOCAL lnencnrreceita, lnencpinreceita, lnencpinopreceita, lnencpagam, lnencmompagam
    STORE '' TO lnencnrreceita, lnencpinreceita, lnencpinopreceita, lnencpagam, lnencmompagam
    TEXT TO lcsql TEXTMERGE NOSHOW
			select nrReceita, pinAcesso, pinOpcao, pagamento, momento_pagamento from bo2 (nolock) where bo2stamp='<<ALLTRIM(lcstampact)>>'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "ucrsDadosRecEnc", lcsql)
       MESSAGEBOX("OCORREU UMA ANOMALIA A VERIFICAR OS DADOS DA ENCOMENDA.", 16, "LOGITOOLS SOFTWARE")
       RETURN .F.
    ENDIF
    SELECT ucrsdadosrecenc
    IF  .NOT. EMPTY(ucrsdadosrecenc.nrreceita)
       lnencnrreceita = ALLTRIM(ucrsdadosrecenc.nrreceita)
       lnencpinreceita = ALLTRIM(ucrsdadosrecenc.pinacesso)
       lnencpinopreceita = ALLTRIM(ucrsdadosrecenc.pinopcao)
       lnencpagam = ALLTRIM(ucrsdadosrecenc.pagamento)
       lnencmompagam = ALLTRIM(ucrsdadosrecenc.momento_pagamento)
    ENDIF
    SELECT fi
    GOTO BOTTOM
    IF LEFT(ALLTRIM(fi.design), 1)<>'.' .AND. (ALLTRIM(lcstampact)<>ALLTRIM(lcstampant) .OR. EMPTY(lcstampant))
       SELECT fi
       GOTO BOTTOM
       uf_atendimento_novavenda(.F.)
       IF  .NOT. EMPTY(lnencnrreceita)
          REPLACE fi.encreceita WITH ALLTRIM(lnencnrreceita)
          REPLACE fi.encpinreceita WITH ALLTRIM(lnencpinreceita)
          REPLACE fi.enccodopreceita WITH ALLTRIM(lnencpinopreceita)
          REPLACE fi.encpagam WITH ALLTRIM(lnencpagam)
          REPLACE fi.encmompagam WITH ALLTRIM(lnencmompagam)
          REPLACE fi.bostamp WITH ALLTRIM(lcstampact)
          REPLACE fi.nmdos WITH 'Encomenda de Cliente'
          receitaencomenda = .T.
          nrreceitaenc = ALLTRIM(lnencnrreceita)
          codreceitaenc = ALLTRIM(lnencpinreceita)
          cdoreceitaenc = ALLTRIM(lnencpinopreceita)
       ENDIF
       REPLACE fi.bostamp WITH ALLTRIM(lcstampact)
    ENDIF
    SELECT fi
    lcposfi = RECNO("fi")
    IF  .NOT. EMPTY(lnencnrreceita) .AND. RECCOUNT("fi")=1
       UPDATE fi SET fi.bostamp = ALLTRIM(lcstampact), fi.encreceita = ALLTRIM(lnencnrreceita), fi.encpinreceita = ALLTRIM(lnencpinreceita), fi.enccodopreceita = ALLTRIM(lnencpinopreceita), fi.nmdos = 'Encomenda de Cliente' WHERE lordem=100000
       receitaencomenda = .T.
       nrreceitaenc = ALLTRIM(lnencnrreceita)
       codreceitaenc = ALLTRIM(lnencpinreceita)
       cdoreceitaenc = ALLTRIM(lnencpinopreceita)
    ENDIF
    IF EMPTY(lnencnrreceita) .AND. RECCOUNT("fi")=1
       UPDATE fi SET fi.bostamp = ALLTRIM(lcstampact) WHERE lordem=100000
    ENDIF
    SELECT fi
    TRY
       GOTO lcposfi
    CATCH
       GOTO BOTTOM
    ENDTRY
    SELECT &lccursor
    LOCAL lcnocl, lcestabcl, lcnomecl, lcncont
    lcnocl = &lccursor..no
    lcestabcl = &lccursor..estab
    lcnomecl = ALLTRIM(&lccursor..NOME)
    lcncont = ALLTRIM(&lccursor..ncont)
    IF lcnocl<>0
       SELECT ucrsatendcl
       IF ucrsatendcl.no<>lcnocl .OR. ucrsatendcl.estab<>lcestabcl
          uf_atendimento_selcliente(lcnocl, lcestabcl, "ATENDIMENTO")
       ENDIF
       IF  .NOT. EMPTY(lcnomecl) .AND. lcnocl==200 .AND.  .NOT. (UPPER(ALLTRIM(lcnomecl))==UPPER(ALLTRIM(ucrsatendcl.nome)))
          SELECT ucrsatendcl
          REPLACE ucrsatendcl.nome WITH ALLTRIM(lcnomecl)
          REPLACE ucrsatendcl.ncont WITH ALLTRIM(lcncont)
          SELECT ft
          REPLACE ft.nome WITH ALLTRIM(lcnomecl)
          REPLACE ft.ncont WITH ALLTRIM(lcncont)
          atendimento.refresh()
       ENDIF
    ENDIF
    SELECT &lccursor
    lcstampant =  ALLTRIM(&lccursor..ftstamp)
 ENDSCAN
ENDFUNC
**

**
FUNCTION uf_regvendas_filtrar_ecra
   LPARAMETERS uv_filSus, uv_filCre, uv_filRes, uv_filEnc

   uv_filtro = ''
   uv_cursor = ''

   regvendas.sosusp = 0
   regvendas.socred = 0
   sores = 0
   soenc = 0

   DO CASE
      CASE regvendas.pgfreg.activepage==1
         uv_cursor = "uCrsRegVendas"
      CASE regvendas.pgfreg.activepage==2
         IF regvendas.pgfreg.page2.chkporproduto.tag=="true"
            uv_cursor = "uCrsRegCreditos"
         ELSE
            uv_cursor = "uCrsRegCreditosValor"
         ENDIF
   ENDCASE

   IF uv_filSus
      
      uv_filtro = 'UPPER(RIGHT(ALLTRIM(' + ALLTRIM(uv_cursor) + '.nmdoc),3)) = "(S)"'
      regvendas.sosusp = 1
      
   ENDIF

   IF uv_filCre

      IF uv_cursor == 'uCrsRegVendas'
         *** Filtro Original          uv_filtro = uv_filtro + IIF(!empty(uv_filtro), ' AND ', ' ') + 'ROUND(ucrsregvendas.totlin-ucrsregvendas.valoradiantado, 2)> 0 .AND. ALLTRIM(ucrsregvendas.nmdoc)<>"Reserva de Cliente"'
        	uv_filtro = uv_filtro + IIF(!empty(uv_filtro), ' AND ', ' ') + 'ucrsregvendas.qtt>ucrsregvendas.eaquisicao  AND ucrsregvendas.lancacc' 
			regvendas.socred = 1
      ENDIF

   ENDIF

   IF uv_filRes 

      IF uv_cursor == 'uCrsRegVendas'
         uv_filtro = 'ALLTRIM(uCrsRegVendas.nmdoc) = "Reserva de Cliente"'
         sores = 1
      ENDIF

   ENDIF

   IF uv_filEnc

      IF uv_cursor == 'uCrsRegVendas'
         uv_filtro = 'ALLTRIM(uCrsRegVendas.nmdoc) = "Encomenda de Cliente"'
         soEnc = 1
      ENDIF

   ENDIF
   
   SELECT &uv_cursor
   
   IF !EMPTY(uv_filtro)
	   SET FILTER TO &uv_filtro
   ELSE
       SET FILTER TO 
   ENDIF
   
   GO TOP
   
*!*	   TEXT TO uv_msgDev TEXTMERGE NOSHOW
*!*	   	TABELA = <<uv_cursor>>
*!*	   	FILTRO = <<uv_filtro>>
*!*	   	
*!*	   	SUS = <<IIF(uv_filSus, 1, 0)>>
*!*	   	Cred = <<IIF(uv_filCre, 1, 0)>>
*!*	   	Res = <<IIF(uv_filRes, 1, 0)>>
*!*	   	Enc = <<IIF(uv_filEnc, 1, 0)>>
*!*	   	
*!*	   ENDTEXT
*!*	   
*!*	   _clipText = uv_msgDev

   DO CASE
      CASE regvendas.pgfreg.activepage==1
         uf_regvendas_configgrdregvendas()
      CASE regvendas.pgfreg.activepage==2
         IF regvendas.pgfreg.page2.chkporproduto.tag=="true"
            regvendas.pgfreg.page2.grdregcredito.refresh()
         ELSE
            regvendas.pgfreg.page2.grdregcreditovalor.refresh()
         ENDIF
   ENDCASE

ENDFUNC
**

**
PROCEDURE uf_regvendas_marcarSel


	IF USED("FI")

	    select FI
    	GO TOP

	    SCAN FOR !EMPTY(FI.ofistamp)

    	    select uCrsRegVendas 

        	DELETE for uCrsRegVendas.fistamp = fi.Ofistamp

	    ENDSCAN
	    
	    SELECT uCrsRegVendas
		GO TOP
		
		regVendas.pgfReg.Page1.grdVendas.refresh

	ENDIF	    

ENDPROC
**

FUNCTION uf_regvendas_filtraprod_receita
	SELECT ucrsfiltraprodreceita
	LOCAL lcFistampDel 
	lcFistampDel = ALLTRIM(ucrsfiltraprodreceita.fistamp)
	TEXT TO lcsql TEXTMERGE NOSHOW
		select COUNT(fistamp) as conta from fi where ftstamp='<<ALLTRIM(ucrsfiltraprodreceita.ftstamp)>>'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "UcrsDadosContaFIR", lcsql)
       uf_perguntalt_chama("N�o foi possivel verificar os dados dos psicotr�picos para esta regulariza��o.", "", "OK", 16)
       RETURN .F.
    ENDIF
    verificaregreceita = .f.
    IF UcrsDadosContaFIR.conta>1
    	TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_touch_regularizarVendas '', <<ucrsfiltraprodreceita.fno>>, '', <<ucrsfiltraprodreceita.no>>, <<ucrsfiltraprodreceita.estab>>, '<<uf_gerais_getdate(REGVENDAS.txtDataIni.value, "SQL")>>', '<<uf_gerais_getdate(REGVENDAS.txtDataFim.value, "SQL")>>', '', <<ch_userno>>, '<<Alltrim(ch_grupo)>>', '<<mySite>>', 100, 0
		ENDTEXT

		uf_gerais_actgrelha("", "uCrsRegVendas", lcsql)
		LOCAL lcposlnrec1
	    SELECT uCrsRegVendas
	    GO TOP 
	    SCAN 
		    lcposlnrec1 = RECNO("uCrsRegVendas")
	    	replace uCrsRegVendas.sel WITH .t.
	    	IF ALLTRIM(uCrsRegVendas.fistamp) <> ALLTRIM(lcFistampDel)
	    		uf_regvendas_addLinhaReg()	
	    	ENDIF 
	    	SELECT ucrsregvendas
		    TRY
		       GOTO lcposlnrec1
		    CATCH
		    ENDTRY
	    ENDSCAN 
    ENDIF 
    fecha("UcrsDadosContaFIR")
ENDFUNC 

FUNCTION uf_regVendas_getDadosUtente
	LPARAMETERS uv_ftstamp, uv_linstamp

	IF EMPTY(uv_ftstamp) OR !USED("ucrsvale2") OR !WEXIST("ATENDIMENTO")
		RETURN .F.
	ENDIF

	uv_stampreserva = ""

	uv_stampreserva = uf_gerais_getUmValor("ft2", "stampreserva", "ft2stamp = '" + ALLTRIM(uv_ftstamp) + "'")
    uv_nrReceita = uf_gerais_getUmValor("bi2", "nrreceita", "bi2stamp = '" + ALLTRIM(uv_linstamp) + "'")

	IF EMPTY(uv_stampreserva)
		uv_stampreserva = uv_ftstamp
	ENDIF

	TEXT TO msel TEXTMERGE NOSHOW

      SELECT *
      FROM(
         select
            u_nmutavi,
            u_moutavi,
            u_cputavi,
            u_dcutavi = getdate()-(365*u_idutavi),
            u_ndutavi,
            u_ddutavi,
            u_idutavi,
            u_medico,
            codigoDocSPMS,
            u_recdata,
            (CASE WHEN CONVERT(varchar, u_recdata, 112) = '19000101' THEN 0 ELSE 1 END) as hasData,
            ft2.u_receita
         from 
            B_dadosPsico(nolock) 
            join ft2(nolock) on ft2.ft2stamp = B_dadosPsico.ftstamp
         where 
            ftstamp in (select ft2stamp from ft2(nolock) where stampreserva = '<<ALLTRIM(uv_stampReserva)>>')
      ) as x
      WHERE
         x.u_receita = (CASE WHEN '<<ALLTRIM(uv_nrReceita)>>' = '' THEN x.u_receita ELSE '<<ALLTRIM(uv_nrReceita)>>' END)

	ENDTEXT

   **_cliptext = msel

	IF !uf_gerais_actGrelha("", "uc_dadosReservaTMP", msel)
		RETURN .F.
	ENDIF

	IF RECCOUNT("uc_dadosReservaTMP") = 0
		RETURN .F.
	ENDIF


   IF !USED("uc_dadosPsico")
      CREATE CURSOR uc_dadosPsico(ftstamp c(25), u_nmutavi c(55), u_moutavi c(55), u_cputavi c(45), u_dcutavi d, u_ndutavi c(20), u_ddutavi d, u_idutavi n(3), u_medico c(40), codigoDocSPMS c(10), u_recdata d, u_receita c(100))
   ENDIF

	SELECT * FROM uc_dadosReservaTMP WITH (BUFFERING = .T.) WHERE 1 = 0 INTO CURSOR uc_dadosReserva READWRITE

	SELECT uc_dadosReserva
	DELETE 

	APPEND BLANK

	SELECT uc_dadosReservaTMP
	GO TOP
	SCAN

		SELECT uc_dadosReserva
		GO TOP

		IF EMPTY(uc_dadosReserva.u_nmutavi)
			REPLACE uc_dadosReserva.u_nmutavi WITH uc_dadosReservaTMP.u_nmutavi
		ENDIF

		IF EMPTY(uc_dadosReserva.u_moutavi)
			REPLACE uc_dadosReserva.u_moutavi WITH uc_dadosReservaTMP.u_moutavi
		ENDIF

		IF EMPTY(uc_dadosReserva.u_cputavi)
			REPLACE uc_dadosReserva.u_cputavi WITH uc_dadosReservaTMP.u_cputavi
		ENDIF

		IF EMPTY(uc_dadosReserva.u_dcutavi)
			**REPLACE uc_dadosReserva.u_dcutavi WITH uc_dadosReservaTMP.u_dcutavi
		ENDIF

		IF EMPTY(uc_dadosReserva.u_ndutavi)
			REPLACE uc_dadosReserva.u_ndutavi WITH uc_dadosReservaTMP.u_ndutavi
		ENDIF

		IF EMPTY(uc_dadosReserva.u_ddutavi)
			REPLACE uc_dadosReserva.u_ddutavi WITH uc_dadosReservaTMP.u_ddutavi
		ENDIF

		IF EMPTY(uc_dadosReserva.u_idutavi)
			REPLACE uc_dadosReserva.u_idutavi WITH uc_dadosReservaTMP.u_idutavi
		ENDIF

		IF EMPTY(uc_dadosReserva.u_medico)
			REPLACE uc_dadosReserva.u_medico WITH uc_dadosReservaTMP.u_medico
		ENDIF

		IF EMPTY(uc_dadosReserva.codigoDocSPMS)
			REPLACE uc_dadosReserva.codigoDocSPMS WITH uc_dadosReservaTMP.codigoDocSPMS
		ENDIF

		IF EMPTY(uc_dadosReserva.u_recdata) AND uc_dadosReservaTMP.hasData = 1
			REPLACE uc_dadosReserva.u_recdata WITH uc_dadosReservaTMP.u_recdata
		ENDIF

		SELECT uc_dadosReservaTMP
	ENDSCAN

	SELECT uc_dadosPsico
   APPEND BLANK

   REPLACE uc_dadosPsico.ftstamp with uv_ftstamp
   REPLACE uc_dadosPsico.u_receita with uv_nrReceita

	IF !EMPTY(uc_dadosReserva.u_nmutavi)
		REPLACE uc_dadosPsico.u_nmutavi WITH ALLTRIM(uc_dadosReserva.u_nmutavi)
	ENDIF
	IF !EMPTY(uc_dadosReserva.u_moutavi)
		REPLACE uc_dadosPsico.u_moutavi WITH ALLTRIM(uc_dadosReserva.u_moutavi)
	ENDIF
	IF !EMPTY(uc_dadosReserva.u_cputavi)
		REPLACE uc_dadosPsico.u_cputavi WITH ALLTRIM(uc_dadosReserva.u_cputavi)
	ENDIF
	IF !EMPTY(uc_dadosReserva.u_dcutavi)
		REPLACE uc_dadosPsico.u_dcutavi WITH uc_dadosReserva.u_dcutavi
	ENDIF
	IF !EMPTY(uc_dadosReserva.u_ndutavi)
		REPLACE uc_dadosPsico.u_ndutavi WITH ALLTRIM(uc_dadosReserva.u_ndutavi)
	ENDIF
	IF !EMPTY(uc_dadosReserva.u_ddutavi)
		REPLACE uc_dadosPsico.u_ddutavi WITH uc_dadosReserva.u_ddutavi
	ENDIF
	IF !EMPTY(uc_dadosReserva.u_idutavi)
		REPLACE uc_dadosPsico.u_idutavi WITH uc_dadosReserva.u_idutavi
	ENDIF
	IF !EMPTY(uc_dadosReserva.codigoDocSPMS)
		REPLACE uc_dadosPsico.codigoDocSPMS WITH uc_dadosReserva.codigoDocSPMS
	ENDIF
	
	IF !EMPTY(uc_dadosReserva.u_medico)
		REPLACE uc_dadosPsico.u_medico WITH uc_dadosReserva.u_medico
	ENDIF

	IF !EMPTY(uc_dadosReserva.u_recdata)
		REPLACE uc_dadosPsico.u_recdata WITH uc_dadosReserva.u_recdata
	ENDIF


	FECHA("uc_dadosReserva")
	FECHA("uc_dadosReservaTMP")

	

	RETURN .T.

ENDFUNC

FUNCTION uf_regVendas_checkCabs

    SELECT FI
    uv_lordem = fi.lordem

    SELECT * FROM fi WITH (BUFFERING = .T.) INTO CURSOR uc_tmpFI
    SELECT * FROM fi WITH (BUFFERING = .T.) WHERE EMPTY(fi.ref) AND LEFT(fi.design,1) = '.' INTO CURSOR uc_tmpFIcab


    SELECT uc_tmpFI
    GO TOP

    IF RECCOUNT("uc_tmpFI") = 1 

        IF uc_tmpFIcab.tipor = 'SR'
            RETURN .T.
        ELSE
            RETURN .F.
        ENDIF

    ELSE

        SELECT uc_tmpFIcab
        LOCATE FOR uc_tmpFIcab.lordem = uv_lordem

        IF FOUND()

            IF uc_tmpFIcab.tipor = 'SR'
                SELECT uc_tmpFI
                LOCATE FOR uc_tmpFI.lordem > uv_lordem
                IF !FOUND()
                    RETURN .T.
                ENDIF
            ENDIF

        ENDIF

        SELECT uc_tmpFIcab
        GO BOTTOM

        uv_lordem = uc_tmpFIcab.lordem

        IF uc_tmpFIcab.tipor = 'SR'
            SELECT uc_tmpFI
            LOCATE FOR uc_tmpFI.lordem > uv_lordem
            IF !FOUND()
                RETURN .T.
            ELSE
                RETURN .F.
            ENDIF        
        ELSE
            RETURN .F.
        ENDIF

    ENDIF

ENDFUNC

FUNCTION uf_regvendas_checkInactiveRefs()

	SELECT * FROM ucrsregvendas WITH (BUFFERING = .T.) WHERE sel INTO CURSOR uc_tmpReg

	select uc_tmpReg
	GO TOP

	SCAN FOR !EMPTY(uc_tmpReg.ref)

		IF uf_gerais_getUmValor("st", "inactivo", "ref = '" + ALLTRIM(uc_tmpReg.ref) + "' and site_nr = " + ASTR(mySite_nr))
			uf_perguntalt_chama("PRODUTO MARCADO COMO INATIVO. POR FAVOR VERIFIQUE A FICHA DO PRODUTO:" + CHR(13) + CHR(13) + ALLTRIM(uc_tmpReg.ref) + " - " + ALLTRIM(uc_tmpReg.design), "OK", "", 16)

			uf_curStamp = uc_tmpReg.fistamp

			SELECT ucrsregvendas 
			GO TOP
			LOCATE FOR ucrsregvendas.fistamp = uf_curStamp
			RETURN .F.

		ENDIF

	ENDSCAN

	RETURN .T.

ENDFUNC

FUNCTION uf_regVendas_modoPagExterno
	LPARAMETERS uv_modoPag

	IF EMPTY(uv_modoPag)
		RETURN .F.
	ENDIF

	LOCAL uv_no, uv_estab

	IF regvendas.pgfreg.page2.chkporproduto.tag=="true"
		SELECT DISTINCT no FROM uCrsRegCreditos WHERE sel==.T. INTO CURSOR uCrsRegCrDistNoExt READWRITE
	ELSE
		SELECT DISTINCT no FROM uCrsRegCreditosValor WHERE sel==.T. INTO CURSOR uCrsRegCrDistNoExt READWRITE
	ENDIF

	IF  .NOT. EMPTY(regvendas.txtestab.value)
       uv_estab = astr(regvendas.txtestab.value)
    ELSE
       uv_estab = 0
    ENDIF

	IF regvendas.pgfreg.page2.chkporutente.tag="false"
		SELECT uCrsRegCrDistNoExt
		GOTO TOP
		uv_no = uCrsRegCrDistNoExt.no
	ELSE
		uv_no = astr(regvendas.txtno.value)
	ENDIF

	IF regvendas.ctnpagamento.txtvalatend.value < 0
		uf_perguntalt_chama("N�o pode utilizar este modo de pagamento para devolu��es.","OK","",16)
		RETURN .F.
	ENDIF

	IF !uf_pagExterno_chama(uv_modoPag, uv_no, uv_estab)
		RETURN .F.
	ENDIF

	IF !USED("uc_dadosPagExterno" + ALLTRIM(uv_modoPag))
		RETURN .F.
	ENDIF

	RETURN .T.

ENDFUNC

FUNCTION uf_regvendas_geraPagExternos
	LPARAMETERS uv_no, uv_estab, uv_nrAtend

	IF EMPTY(uv_no) OR TYPE("uv_estab") <> "N" OR EMPTY(uv_nrAtend)
		RETURN .F.
	ENDIF

	IF !uf_gerais_actGrelha("","uc_modosPagExterno", "exec up_vendas_checkModoPagExterno")

		uf_perguntalt_chama("Erro a verificar modos de pagamento externo. Por favor contacte o suporte.","OK","",16)
		RETURN .F.

	ENDIF

	IF RECCOUNT("uc_modosPagExterno") > 0

		CREATE CURSOR uc_pagsExterno(modoPag C(10), tlmvl C(50), email C(254), duracao C(50), descricao M, valor N(14,2), tipoPagExt N(3), tipoPagExtDesc C(100), receiverID C(100), receiverName C(100), no N(9), estab N(5), nrAtend C(20))
		LOCAL uv_valor, uv_curCursor

		SELECT uc_modosPagExterno
		GO TOP

		SCAN

			uv_valor = 0

			DO CASE
				CASE ALLTRIM(uc_modosPagExterno.ref) = "05"

					uv_valor = VAL(regvendas.ctnpagamento.txtModPag3.value)

					IF uv_valor <> 0 AND !USED("uc_dadosPagExterno05") 
						uf_perguntalt_chama("N�o preencheu os dados para Pagamento Externo:" + CHR(13) + ALLTRIM(regvendas.ctnpagamento.btnModPag3.Label7.caption),"OK","",16)
						RETURN .F.
					ENDIF
					
				CASE ALLTRIM(uc_modosPagExterno.ref) = "06"

					uv_valor = VAL(regvendas.ctnpagamento.txtModPag4.value)

					IF uv_valor <> 0 AND !USED("uc_dadosPagExterno06") 
						uf_perguntalt_chama("N�o preencheu os dados para Pagamento Externo:" + CHR(13) + ALLTRIM(regvendas.ctnpagamento.btnModPag4.Label7.caption),"OK","",16)
						RETURN .F.
					ENDIF

				CASE ALLTRIM(uc_modosPagExterno.ref) = "07"

					uv_valor = VAL(regvendas.ctnpagamento.txtModPag5.value)

					IF uv_valor <> 0 AND !USED("uc_dadosPagExterno07") 
						uf_perguntalt_chama("N�o preencheu os dados para Pagamento Externo:" + CHR(13) + ALLTRIM(regvendas.ctnpagamento.btnModPag5.Label7.caption),"OK","",16)
						RETURN .F.
					ENDIF

				CASE ALLTRIM(uc_modosPagExterno.ref) = "08"

					uv_valor = VAL(regvendas.ctnpagamento.txtModPag6.value)

					IF uv_valor <> 0 AND !USED("uc_dadosPagExterno08") 
						uf_perguntalt_chama("N�o preencheu os dados para Pagamento Externo:" + CHR(13) + ALLTRIM(regvendas.ctnpagamento.btnModPag6.Label7.caption),"OK","",16)
						RETURN .F.
					ENDIF

				OTHERWISE
					LOOP
			ENDCASE

			uv_curCursor = "uc_dadosPagExterno" + ALLTRIM(uc_modosPagExterno.ref)

			IF uv_valor <> 0 AND USED(uv_curCursor)

					SELECT uc_pagsExterno
					APPEND BLANK

					REPLACE uc_pagsExterno.modoPag WITH ALLTRIM(uc_modosPagExterno.ref)
					REPLACE uc_pagsExterno.tlmvl WITH ALLTRIM(&uv_curCursor..tlmvl)
					REPLACE uc_pagsExterno.email WITH ALLTRIM(&uv_curCursor..email)
					REPLACE uc_pagsExterno.duracao WITH ALLTRIM(&uv_curCursor..duracao)
					REPLACE uc_pagsExterno.descricao WITH ALLTRIM(&uv_curCursor..descricao)
					REPLACE uc_pagsExterno.valor WITH uv_valor
					REPLACE uc_pagsExterno.tipoPagExt WITH uc_modosPagExterno.tipoPagExt
					REPLACE uc_pagsExterno.tipoPagExtDesc WITH uc_modosPagExterno.tipoPagExtDesc
					REPLACE uc_pagsExterno.receiverID WITH uc_modosPagExterno.receiverID
					REPLACE uc_pagsExterno.receiverName WITH uc_modosPagExterno.receiverName
					REPLACE uc_pagsExterno.no WITH uv_no
					REPLACE uc_pagsExterno.estab WITH uv_estab
					REPLACE uc_pagsExterno.nrAtend WITH ALLTRIM(uv_nrAtend)

			ENDIF


			SELECT uc_modosPagExterno
		ENDSCAN

		SELECT uc_pagsExterno
      
		IF RECCOUNT("uc_pagsExterno") > 0
			IF regvendas.pgfreg.page2.chkmovcaixa.tag = 'false'
				uf_perguntalt_chama("Para utilizar Pagamentos Externos tem de movimentar caixa.","OK","",16)
				RETURN .F.
			ENDIF

			IF !uf_pagamento_reqPagExterno()
				uf_perguntalt_chama("N�o foi poss�vel criar os pagamentos externos. Por favor contacte o suporte.","OK","",16)
				RETURN .F.
			ENDIF
		ENDIF	

	ENDIF

	RETURN .T.

ENDFUNC




function uf_regVendas_pesquisarCliente
	uf_pesqUTENTES_Chama("REGVENDAS")
	Pesqutentes.alwaysontop = 1
endfunc 