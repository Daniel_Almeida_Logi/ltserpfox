**
FUNCTION uf_prodvendidos_chama
 LPARAMETERS lcmodo, lcnome, lcno, lcestab
 LOCAL lcsql
 PUBLIC myorderpesquisarh
 IF EMPTY("lcModo") .OR.  .NOT. (TYPE("lcModo")=="L")
    uf_perguntalt_chama("O PAR�METRO: MODO, N�O � V�LIDO.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF EMPTY("lcNome") .OR.  .NOT. (TYPE("lcNome")=="C")
    uf_perguntalt_chama("O PAR�METRO: NOME, N�O � V�LIDO.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF EMPTY("lcNo") .OR.  .NOT. (TYPE("lcNo")=="N")
    uf_perguntalt_chama("O PAR�METRO: NO, N�O � V�LIDO.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF EMPTY("lcEstab") .OR.  .NOT. (TYPE("lcEstab")=="N")
    uf_perguntalt_chama("O PAR�METRO: ESTAB, N�O � V�LIDO.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF lcno=200
    uf_perguntalt_chama("N�O PODE CONSULTAR OS PRODUTOS VENDIDOS PARA O CLIENTE N� 200.", "OK", "", 64)
    RETURN .F.
 ENDIF
 IF TYPE("ProdVendidos")=="U"
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_touch_produtosVendidosCliente <<lcNo>>,<<lcEstab>>,'19000101','19000101'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", 'uCrsProdVendidos', lcsql)
       uf_perguntalt_chama("OCORREU UM ERRO A PESQUISAR OS PRODUTOS.", "OK", "", 16)
       RETURN .F.
    ENDIF
    DO FORM ProdVendidos
 ELSE
    prodvendidos.show
 ENDIF
 prodvendidos.modo = lcmodo
 prodvendidos.txtnome.value = ALLTRIM(lcnome)
 prodvendidos.txtno.value = lcno
 prodvendidos.txtestab.value = lcestab
 IF TYPE("ProdVendidos.menu1.actualizar")=="U"
    prodvendidos.menu1.adicionaopcao("actualizar", "Actualizar", mypath+"\imagens\icons\actualizar_w.png", "uf_ProdVendidos_pesquisar", "F1")
    prodvendidos.menu1.adicionaopcao("imprimir", "Imprimir", mypath+"\imagens\icons\imprimir_w.png", "uf_ProdVendidos_Imprimir", "F2")
 ENDIF
 uf_prodvendidos_pesquisar()
 uf_prodvendidos_filtrar()
ENDFUNC
**
FUNCTION uf_prodvendidos_imprimir


 **IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
 IF uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + ALLTRIM(myTerm) + "'")

	uv_idImp = uf_gerais_getUmValor("B_impressoes", "id", "tabela = 'CL' and nomeImpressao = 'Tal�o Hist�rico Utente'")

	uf_imprimirgerais_sendprinter_talao(uv_idImp , .f., .F.)

 ELSE


	LOCAL nrdoc, nrdoc2
	STORE 0 TO nrdoc, nrdoc2
	IF EMPTY(myposprinter)
		uf_perguntalt_chama("A IMPRESSORA N�O EST� A SER DETECTADA. VERIFIQUE SE A IMPRESSORA EST� BEM LIGADA.", "OK", "", 48)
		RETURN .F.
	ENDIF
	uf_gerais_setimpressorapos(.T.)
	??? CHR(27E0)+CHR(64E0)
	??? CHR(27)+CHR(99)+CHR(48)+CHR(1)
	??? CHR(27E0)+CHR(116)+CHR(003)
	??? CHR(27)+CHR(33)+CHR(1)
	SELECT ucrse1
	?? SUBSTR(ucrse1.nomecomp, 1, 53)
	??? CHR(27)+CHR(33)+CHR(1)
	? "Dir. Tec. "
	?? SUBSTR(ucrse1.u_dirtec, 1, 43)
	? SUBSTR(ucrse1.morada, 1, 42)
	? TRANSFORM(ucrse1.codpost, "#######################")
	?? "Contrib Nr.: "
	?? TRANSFORM(ucrse1.ncont, "99999999999999")
	? "Telef:"
	?? TRANSFORM(ucrse1.telefone, "##############")
	?? "Fax:"
	?? TRANSFORM(ucrse1.fax, "#############")
   ? ALLTRIM(uf_gerais_getMacrosReports(2))
	? "----------------------------------------------------"
	? "Op: "
	?? SUBSTR(m_chnome, 1, 30)+" ("+ALLTRIM(STR(ch_vendedor))+")"
	? "Data: "
	?? DATE()
	? "----------------------------------------------------"
	? "Cliente: "
	?? SUBSTR(prodvendidos.txtnome.value, 1, 40)
	? "Nr.: "+SUBSTR(STR(prodvendidos.txtno.value), 1, 10)
	? "----------------------------------------------------"
	? "HISTORICO DE PRODUTOS DO UTENTE "
	SELECT ucrsprodvendidos
	GOTO TOP
	SCAN
		nrdoc2 = ucrsprodvendidos.nr
		IF nrdoc<>nrdoc2
		? "----------------------------------------------------"
		? SUBSTR(ucrsprodvendidos.documento, 1, 30)
		?? TRANSFORM(ucrsprodvendidos.nr, "999999999")
		ENDIF
		nrdoc = ucrsprodvendidos.nr
		? " "+SUBSTR(ucrsprodvendidos.design, 1, 44)
		?? TRANSFORM(ucrsprodvendidos.qtt, "999")
	ENDSCAN
	? "----------------------------------------------------"
	? "	  Processado por Computador - IVA INCLUIDO"
	??? CHR(27)+CHR(100)+CHR(7)
	??? CHR(29)+CHR(86)+CHR(48)
	uf_gerais_setimpressorapos(.F.)
 ENDIF
 SELECT ucrsprodvendidos
 GOTO TOP
ENDFUNC
**
PROCEDURE uf_prodvendidos_pesquisar
 LOCAL lcsql
 TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_touch_produtosVendidosCliente <<ProdVendidos.txtNo.value>>, <<ProdVendidos.txtEstab.value>>, '<<uf_gerais_getdate(ProdVendidos.dataInicio.value,"SQL")>>', '<<uf_gerais_getdate(ProdVendidos.dataFim.value,"SQL")>>'
 ENDTEXT
 uf_gerais_actgrelha("ProdVendidos.grdPesq", "uCrsProdVendidos", lcsql)
 uf_prodvendidos_filtrar()
ENDPROC
**
PROCEDURE uf_prodvendidos_filtrar
 LOCAL lcfiltro
 STORE '' TO lcfiltro
 SELECT ucrsprodvendidos
 SET FILTER TO
 SELECT ucrsprodvendidos
 GOTO TOP
 prodvendidos.grdpesq.refresh
 IF prodvendidos.chkexcluiplanos.tag=="true"
    IF  .NOT. EMPTY(ALLTRIM(lcfiltro))
       lcfiltro = lcfiltro+' AND Empty(ALLTRIM(uCrsProdVendidos.u_codigo))'
    ELSE
       lcfiltro = ' Empty(ALLTRIM(uCrsProdVendidos.u_codigo))'
    ENDIF
 ENDIF
 IF prodvendidos.chkfiltrarsusp.tag=="true"
    IF  .NOT. EMPTY(ALLTRIM(lcfiltro))
       lcfiltro = lcfiltro+' AND RIGHT(ALLTRIM(uCrsProdVendidos.Documento),3) = "(S)"'
    ELSE
       lcfiltro = ' RIGHT(ALLTRIM(uCrsProdVendidos.Documento),3) = "(S)"'
    ENDIF
 ENDIF
 IF prodvendidos.chknaocomp.tag=="true"
    IF  .NOT. EMPTY(ALLTRIM(lcfiltro))
       lcfiltro = lcfiltro+' AND !EMPTY(uCRsProdVendidos.ncomp) AND EMPTY(ALLTRIM(uCRsProdVendidos.u_ltstamp)) AND EMPTY(ALLTRIM(uCRsProdVendidos.u_ltstamp2))'
    ELSE
       lcfiltro = ' !EMPTY(uCRsProdVendidos.ncomp) AND EMPTY(ALLTRIM(uCRsProdVendidos.u_ltstamp)) AND EMPTY(ALLTRIM(uCRsProdVendidos.u_ltstamp2))'
    ENDIF
 ENDIF
 IF prodvendidos.chktratamento.tag=="true"
 ELSE
    IF  .NOT. EMPTY(ALLTRIM(lcfiltro))
       lcfiltro = lcfiltro+' AND ALLTRIM(uCRsProdVendidos.documento) != "Hist. Tratamento"'
    ELSE
       lcfiltro = ' ALLTRIM(uCRsProdVendidos.documento) != "Hist. Tratamento"'
    ENDIF
 ENDIF
 SELECT ucrsprodvendidos
 GOTO TOP
 IF  .NOT. EMPTY(ALLTRIM(lcfiltro))
    SELECT ucrsprodvendidos
    SET FILTER TO &lcfiltro
 ELSE
    SELECT ucrsprodvendidos
    SET FILTER TO
 ENDIF
 SELECT ucrsprodvendidos
 GOTO TOP
 prodvendidos.grdpesq.refresh
ENDPROC
**
PROCEDURE uf_prodvendidos_sair
 prodvendidos.release
 RELEASE prodvendidos
 IF USED("uCrsProdVendidos")
    fecha("uCrsProdVendidos")
 ENDIF
 
 if(VARTYPE(atendimento)!='U')
 	if(VARTYPE(atendimento.grdatend)!='U')
 	   atendimento.grdatend.setfocus()
 	ENDIF 	
 ENDIF

ENDPROC
**
FUNCTION uf_prodvendidos_selprod
 SELECT ucrsprodvendidos
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		select top 1 ref 
		from st (nolock) 
		where ref='<<ALLTRIM(uCrsProdVendidos.ref)>>'
 ENDTEXT
 uf_gerais_actgrelha("", "uCrsTmpVerifRef", lcsql)
 IF RECCOUNT("uCrsTmpVerifRef")==0
    prodvendidos.grdpesq.columns(11).check1.value = 0
    prodvendidos.grdpesq.refresh()
    uf_perguntalt_chama("O produto seleccionado n�o tem ficha criada. Poder� criar a ficha atrav�s do painel de dicion�rio.", "OK", "", 64)
    RETURN .F.
 ENDIF
 IF uf_perguntalt_chama("Vai adicionar o produto seleccionado ao atendimento."+CHR(13)+"Pretende continuar?", "Sim", "N�o")
    LOCAL lcposfi
    SELECT ucrsprodvendidos
    uf_atendimento_adicionalinhafi(.F., .F.)
    SELECT fi
    REPLACE fi.ref WITH ALLTRIM(ucrsprodvendidos.ref)
    lcposfi = RECNO("fi")
    uf_atendimento_actref()
    SELECT fi
    TRY
       GOTO lcposfi
    CATCH
       GOTO BOTTOM
    ENDTRY
    uf_atendimento_eventofi()
    SELECT fi
    TRY
       GOTO lcposfi
    CATCH
       GOTO BOTTOM
    ENDTRY
    uf_atendimento_dadosst()
    SELECT fi
    TRY
       GOTO lcposfi
    CATCH
       GOTO BOTTOM
    ENDTRY
 ELSE
    prodvendidos.grdpesq.columns(11).check1.value = 0
    prodvendidos.grdpesq.refresh()
 ENDIF
ENDFUNC
**
