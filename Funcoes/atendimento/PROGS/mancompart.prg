**
PROCEDURE uf_mancompart_chama
 SELECT fimancompart
 GOTO TOP
 
 
  uf_mancompart_validaCursor()
 
 IF TYPE("MANCOMPART")=="U"
    DO FORM MANCOMPART
 ELSE
    mancompart.show()
 ENDIF
 

 
ENDPROC




PROCEDURE uf_mancompart_validaCursor
	
	if(!USED("fimancompart") OR !USED("fi") )
		RETURN .f.
	endif	

	 if(USED("fimancompart")) 
	     UPDATE fimancompart SET fimancompart.lordem = fi.lordem, fimancompart.valortot =  fi.qtt*(fi.epvori-(ROUND(fi.epvori*(fi.desconto/100), 2))) from  fimancompart  inner join fi on fi.fistamp = fimancompart.fistamp 
	 endif  

	fecha("ucrsRemoveFinCompart")
	CREATE CURSOR ucrsRemoveFinCompart(lordem numeric(10,0))	
	
	
	SELECT fimancompart
	GO TOP
	LOCAL lcLordem
	
	SCAN
		lcLordem = fimancompart.lordem
		if(!uf_atendimento_retornaPlanoCabecalho(lcLordem))
			INSERT INTO ucrsRemoveFinCompart(lordem )  VALUES (lcLordem)
		endif	
	ENDSCAN
	
	if(USED("ucrsRemoveFinCompart"))
		SELECT ucrsRemoveFinCompart
		GO TOP
		LOCAL lcLordem
		SCAN
			lcLordem = ucrsRemoveFinCompart.lordem
			DELETE FROM fimancompart where lordem = lcLordem
		ENDSCAN
		
	ENDIF
	
	
	
	
	
	
	fecha("ucrsRemoveFinCompart")	
	
	SELECT fimancompart
	GO TOP
	


ENDPROC


**
PROCEDURE uf_mancompart_gravar
 LOCAL lcvalcomp, lcperccomp, lcfistamp, lcfietiliquido
 STORE 0.00  TO lcvalcomp, lcperccomp, lcfietiliquido
 STORE '' TO lcfistamp
 LOCAL lcpos
 SELECT fi
 lcpos = RECNO()
 SELECT fimancompart
 GOTO TOP
 SCAN
    lcvalcomp = fimancompart.valor
    lcperccomp = fimancompart.perc
    lcfistamp = fimancompart.fistamp
    DO CASE
       **CASE lcvalcomp>0
       **   SELECT fi
       **   GOTO TOP
       **   SCAN
       **      lcfietiliquido = fi.qtt*(fi.epvori-(ROUND(fi.epvori*(fi.desconto/100), 2)))
       **      IF ALLTRIM(fi.fistamp)=ALLTRIM(lcfistamp) .AND. lcfietiliquido>0
       **         REPLACE fi.etiliquido WITH lcfietiliquido-lcvalcomp
       **         REPLACE fi.pv WITH ROUND((lcfietiliquido-lcvalcomp)/fi.qtt, 2)
       **         REPLACE fi.pvmoeda WITH ROUND((lcfietiliquido-lcvalcomp)/fi.qtt, 2)
       **         REPLACE fi.epv WITH ROUND((lcfietiliquido-lcvalcomp)/fi.qtt, 2)
       **         REPLACE fi.u_ettent1 WITH lcvalcomp
       **         REPLACE fi.u_comp WITH .T.
       **         REPLACE fi.u_txcomp WITH 100-ROUND(((lcvalcomp*100)/lcfietiliquido), 2)
       **         REPLACE fi.comp WITH ROUND(((lcvalcomp*100)/lcfietiliquido), 2)
       **         REPLACE fi.comp_diploma WITH ROUND(((lcvalcomp*100)/lcfietiliquido), 2)
       **      ENDIF
       **   ENDSCAN
       CASE lcperccomp>0
          SELECT fi
          GOTO TOP
          SCAN
             lcfietiliquido = fi.qtt*(fi.epvori-(ROUND(fi.epvori*(fi.desconto/100), 2)))
             lcfietiliquido = fi.qtt*fi.epvori
             IF ALLTRIM(fi.fistamp)=ALLTRIM(lcfistamp) .AND. lcfietiliquido>0
                **lcvalcomp = ROUND(lcfietiliquido*(lcperccomp/100), 2)
                lcvalcomp = fi.epvori*(lcperccomp/100)
                **REPLACE fi.pv WITH ROUND((lcfietiliquido-lcvalcomp)/fi.qtt, 2)
                REPLACE fi.pv WITH ROUND(fi.epvori - lcvalcomp,2)
                REPLACE fi.pvmoeda WITH ROUND(fi.epvori - lcvalcomp,2)
                REPLACE fi.epv WITH ROUND(fi.epvori - lcvalcomp,2)
                REPLACE fi.etiliquido WITH fi.qtt*(fi.epv-(ROUND(fi.epv*(fi.desconto/100), 2)))
                REPLACE fi.u_ettent1 WITH lcvalcomp*fi.qtt
                REPLACE fi.u_comp WITH .T.
                REPLACE fi.u_txcomp WITH ROUND(100-lcperccomp,2)
                REPLACE fi.comp WITH lcperccomp
                REPLACE fi.comp_diploma WITH lcperccomp

             ENDIF
          ENDSCAN
       OTHERWISE
          SELECT fi
          GOTO TOP
          SCAN
             lcfietiliquido = fi.qtt*(fi.epvori-(ROUND(fi.epvori*(fi.desconto/100), 2)))
             IF ALLTRIM(fi.fistamp)=ALLTRIM(lcfistamp) .AND. lcfietiliquido>0
                REPLACE fi.etiliquido WITH lcfietiliquido
                REPLACE fi.pv WITH fi.epvori
                REPLACE fi.pvmoeda WITH fi.epvori
                REPLACE fi.epv WITH fi.epvori
                REPLACE fi.u_ettent1 WITH 0
                REPLACE fi.u_comp WITH .F.
                REPLACE fi.u_txcomp WITH 100
                REPLACE fi.comp WITH 0
                REPLACE fi.comp_diploma WITH 0
             ENDIF
          ENDSCAN
    ENDCASE
    SELECT fimancompart
 ENDSCAN
 SELECT fi
 GOTO TOP
 atendimento.grdatend.refresh
 uf_atendimento_infototaiscab()
 uf_atendimento_infototais()
 SELECT fi
 TRY
    GOTO lcpos
 CATCH
 ENDTRY
 uf_mancompart_sair()
ENDPROC
**
PROCEDURE uf_mancompart_sair
 mancompart.hide
 mancompart.release
ENDPROC
**
PROCEDURE uf_mancompart_aplicar_perc_todos
 LOCAL lccontalin, lcpercapl
 STORE 1 TO lccontalin
 STORE 0 TO lcpercapl
 SELECT fimancompart
 GOTO TOP
 SCAN
    IF lccontalin=1
       lcpercapl = fimancompart.perc
    ELSE
       REPLACE fimancompart.perc WITH lcpercapl

      uv_val = (fimancompart.perc/100) * fimancompart.totLinDescSeg
      REPLACE fimancompart.valor WITH uv_val
    ENDIF
    lccontalin = lccontalin+1
 ENDSCAN
 SELECT fimancompart
 GOTO TOP
 mancompart.grdmancompart.refresh
ENDPROC

FUNCTION uf_mancompart_removeComparts

	SELECT fimancompart 
	GO TOP
	UPDATE fimancompart SET valor = 0, perc = 0
	
ENDFUNC

FUNCTION uf_mancompart_totalCompartVenda()
	LOCAL lctotft
	lctotft = 0.00
	SELECT fimancompart 
	GO TOP
	SCAN
		lctotft  = lctotft  + fimancompart.valortot
	ENDSCAN
	RETURN lctotft  
ENDFUNC



**
FUNCTION uf_mancompart_instot
 PUBLIC lcvaltotcompart
 lcvaltotcompart = 0
 LOCAL lctotft, lctotapl, lcdifcomp
 lctotft = uf_mancompart_totalCompartVenda() 
 STORE 0.00  TO lctotapl, lcdifcomp
 uf_tecladonumerico_chama("lcValtotcompart", "Qual val. tot. da comparticipação?", 0, .T., .F., 15, 2)
 IF lcvaltotcompart>lctotft
    uf_perguntalt_chama("O valor da comparticipação não pode ser superior ao valor da venda comparticipada.", "OK", "", 48)
    RETURN .F.
 ENDIF
 uf_mancompart_removeComparts()
 SELECT fimancompart
 GOTO TOP
 SCAN
    REPLACE fimancompart.valor WITH ROUND(lcvaltotcompart*(((fimancompart.valortot*100)/lctotft)/100), 2)
    lctotapl = lctotapl+ROUND(lcvaltotcompart*(((fimancompart.valortot*100)/lctotft)/100), 2)

    uv_valPerc = (fimancompart.valor * 100)/ IIF(EMPTY(fimancompart.totLinDescSeg), 1, fimancompart.totLinDescSeg)
	REPLACE fimancompart.perc WITH uv_valPerc

 ENDSCAN
 IF lcvaltotcompart<>lctotapl
    lcdifcomp = lcvaltotcompart-lctotapl
    SELECT fimancompart
    GOTO BOTTOM
    REPLACE fimancompart.valor WITH fimancompart.valor+lcdifcomp

    uv_valPerc = (fimancompart.valor * 100)/ IIF(EMPTY(fimancompart.totLinDescSeg), 1, fimancompart.totLinDescSeg)
    REPLACE fimancompart.perc WITH uv_valPerc

 ENDIF
 SELECT fimancompart
 GOTO TOP
 mancompart.grdmancompart.refresh
ENDFUNC
**
PROCEDURE uf_mancompart_compfixa
 LOCAL lcpcompfixa
 STORE 0.00  TO lcpcompfixa
 SELECT ucrsatendcomp
 IF ucrsatendcomp.compfixa<>0
    lcpcompfixa = ucrsatendcomp.compfixa
    SELECT fimancompart
    GOTO TOP
    SCAN
       REPLACE fimancompart.perc WITH lcpcompfixa
    ENDSCAN
    mancompart.grdmancompart.readonly = .T.
    mancompart.menu1.estado("TODOS,TOTAL", "HIDE")
 ENDIF
 SELECT fimancompart
 GOTO TOP
 mancompart.grdmancompart.refresh
ENDPROC
**
