**
FUNCTION uf_pagcentral_chama
 IF mypagcentral
    IF uf_gerais_validapermuser(ch_userno, ch_grupo, 'Pagamentos Centralizados')
       IF (uf_gerais_addconnection('PAGCENTRAL')==.F.)
          uf_perguntalt_chama("N�O EXISTEM LICEN�AS DISPON�VEIS PARA ESTE M�DULO.", "OK", "", 48)
          RETURN .F.
       ENDIF
       IF  .NOT. (TYPE('ATENDIMENTO')=="U")
          uf_perguntalt_chama("N�O PODE ABRIR O ECR� DE PAGAMENTOS CENTRALIZADOS ENQUANTO O ECR� DE ATENDIMENTO ESTIVER ABERTO.", "OK", "", 48)
          RETURN .F.
       ENDIF
       IF  .NOT. (TYPE('FACTURACAO')=="U")
          uf_perguntalt_chama("N�O PODE ABRIR O ECR� DE PAGAMENTOS CENTRALIZADOS ENQUANTO O ECR� DE FACTURA��O ESTIVER ABERTO.", "OK", "", 48)
          RETURN .F.
       ENDIF
       IF  .NOT. (TYPE('IDENTIFICACAO')=="U")
          uf_perguntalt_chama("N�O PODE ABRIR O ECR� DE PAGAMENTOS CENTRALIZADOS ENQUANTO O ECR� DE IDENTIFICA��O PARA O ATENDIMENTO ESTIVER ABERTO.", "OK", "", 48)
          RETURN .F.
       ENDIF
       IF  .NOT. (TYPE('PSICOBENZO')=="U")
          uf_perguntalt_chama("N�O PODE ABRIR O ECR� DE PAGAMENTOS CENTRALIZADOS ENQUANTO O ECR� DE GEST�O DE PSICOTR�PICOS E BENZODIAZEPINAS ESTIVER ABERTO.", "OK", "", 48)
          RETURN .F.
       ENDIF
       IF  .NOT. (TYPE('RECEITUARIO')=="U")
          uf_perguntalt_chama("N�O PODE ABRIR O ECR� DE PAGAMENTOS CENTRALIZADOS ENQUANTO O ECR� DE GEST�O DE LOTES ESTIVER ABERTO.", "OK", "", 48)
          RETURN .F.
       ENDIF
       uf_regvendas_chama()
       uf_regvendas_verpagcentral()
       regvendas.menu1.estado("voltar", "HIDE")
    ELSE
       uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL PAGAMENTOS CENTRALIZADOS.", "OK", "", 48)
       RETURN .F.
    ENDIF
 ELSE
    uf_perguntalt_chama("PARA ADQUIRIR ESTE M�DULO POR FAVOR ENTRE EM CONTACTO COM SUPORTE! PE�A J� A SUA DEMONSTRA��O.", "OK", "", 64)
    RETURN .F.
 ENDIF
ENDFUNC
**
PROCEDURE uf_pagcentral_pagpendentes
 regvendas.pgfreg.page3.pgfpagcentral.activepage = 2
 regvendas.menu1.estado("adicionar,limparPag,pagPendentes", "HIDE")
 regvendas.menu1.estado("abaterPag, pagCentral", "SHOW")
 regvendas.menu1.estado("", "", "Registar", .F.)
 regvendas.ctnpagamento.visible = .F.
 uf_pagcentral_veratendpend()
 uf_pagcentral_aplicarfiltros()
ENDPROC
**
PROCEDURE uf_pagcentral_carregacursores
 IF  .NOT. USED("uCrsPca")
    uf_gerais_actgrelha("", "uCrsPca", 'SET FMTONLY ON Select top 1 * From b_pagCentral SET FMTONLY OFF')
 ELSE
    SELECT ucrspca
    DELETE ALL
 ENDIF
 IF  .NOT. USED("uCrsAtendPendPag")
    uf_gerais_actgrelha("", "uCrsAtendPendPag", [SET FMTONLY ON exec up_caixa_atendPendPag '', 0, 0, '' SET FMTONLY OFF])
 ENDIF
ENDPROC
**
PROCEDURE uf_pagcentral_totalporpagar
 IF uf_gerais_actgrelha("", "uCrsTppPca", "exec up_caixa_totalPorPagar '"+mysite+"'")
    IF RECCOUNT()>0
       regvendas.pgfreg.page3.txtpendente.value = ROUND(ucrstpppca.total, 2)
       regvendas.pgfreg.page3.txtnratend.value = ucrstpppca.nratend
    ELSE
       regvendas.pgfreg.page3.txtpendente.value = 0
       regvendas.pgfreg.page3.txtnratend.value = 0
    ENDIF
 ELSE
    regvendas.pgfreg.page3.txtpendente.value = 0
    regvendas.pgfreg.page3.txtnratend.value = 0
 ENDIF
ENDPROC
**
PROCEDURE uf_pagcentral_veratendpend
 IF uf_gerais_actgrelha("regVendas.pgfReg.page3.pgfPagCentral.page2.grdPagPendentes", "uCrsAtendPendPag", "exec up_caixa_atendPendPag '', 0, 0, '"+mysite+"'")
    IF RECCOUNT("uCrsAtendPendPag")>0
       SELECT ucrspca
       IF RECCOUNT("uCrsPca")>0
          SELECT ucrspca
          GOTO TOP
          SCAN
             SELECT ucrsatendpendpag
             LOCATE FOR ucrsatendpendpag.nratend==ucrspca.nratend
             IF FOUND()
                SELECT ucrsatendpendpag
                REPLACE ucrsatendpendpag.sel WITH .T.
             ENDIF
          ENDSCAN
          SELECT ucrsatendpendpag
          GOTO TOP
          regvendas.pgfreg.page3.pgfpagcentral.page2.grdpagpendentes.refresh()
       ENDIF
    ELSE
       uf_perguntalt_chama("N�O EXISTEM ATENDIMENTOS PENDENTES DE PAGAMENTO.", "OK", "", 64)
       uf_regvendas_verpagcentral()
    ENDIF
 ENDIF
ENDPROC
**
PROCEDURE uf_pagcentral_aplicarfiltros
 LOCAL lcvendedor, lctermf
 lcvendedor = ALLTRIM(regvendas.pgfreg.page3.pgfpagcentral.page2.txtvendedor.value)
 lctermf = regvendas.pgfreg.page3.pgfpagcentral.page2.txtterminal.value
 SELECT ucrsatendpendpag
 DO CASE
    CASE EMPTY(lcvendedor) .AND. EMPTY(lctermf)
       SET FILTER TO
    CASE EMPTY(lctermf) .AND.  .NOT. EMPTY(lcvendedor)
       SET FILTER TO UPPER(ALLTRIM(ucrsatendpendpag.vendedornome))==UPPER(ALLTRIM(regvendas.pgfreg.page3.pgfpagcentral.page2.txtvendedor.value))
    CASE EMPTY(lcvendedor) .AND.  .NOT. EMPTY(lctermf)
       SET FILTER TO ucrsatendpendpag.terminal==regvendas.pgfreg.page3.pgfpagcentral.page2.txtterminal.value
    CASE  .NOT. EMPTY(lcvendedor) .AND.  .NOT. EMPTY(lctermf)
       SET FILTER TO ucrsatendpendpag.terminal==regvendas.pgfreg.page3.pgfpagcentral.page2.txtterminal.value .AND. UPPER(ALLTRIM(ucrsatendpendpag.vendedornome))==UPPER(ALLTRIM(regvendas.pgfreg.page3.pgfpagcentral.page2.txtvendedor.value))
 ENDCASE
 GOTO TOP
 regvendas.pgfreg.page3.pgfpagcentral.page2.grdpagpendentes.refresh
ENDPROC
**
PROCEDURE uf_pagcentral_selpagpendente
 SELECT ucrsatendpendpag
 regvendas.txtnatend.value = ucrsatendpendpag.nratend
 uf_pagcentral_adicionapagpendente()
ENDPROC
**
FUNCTION uf_pagcentral_adicionapagpendente
 LPARAMETERS lcbool
 IF EMPTY(regvendas.txtnatend.value)
    RETURN .F.
 ENDIF
 LOCAL lcpcarows
 STORE 0 TO lcpcarows
 TEXT TO lcsql TEXTMERGE NOSHOW
		Select *
		From B_pagCentral (nolock)
		where nrAtend = '<<regVendas.txtNAtend.value>>'
		order by ano desc
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsPagCentral", lcsql)
    regvendas.pgfreg.page3.pgfpagcentral.page2.grdpagpendentes.sel.check1.click
    uf_perguntalt_chama("OCORREU UM ERRO A PROCURAR O ATENDIMENTO.", "OK", "", 16)
    RETURN .F.
 ELSE
    IF  .NOT. RECCOUNT("uCrsPagCentral")>0
       regvendas.pgfreg.page3.pgfpagcentral.page2.grdpagpendentes.sel.check1.value =  .NOT. (regvendas.pgfreg.page3.pgfpagcentral.page2.grdpagpendentes.sel.check1.value)
       uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR O ATENDIMENTO.", "OK", "", 64)
    ELSE
       SELECT ucrspagcentral
       GOTO TOP
       SELECT ucrspagcentral
       IF ucrspagcentral.abatido=.T.
          regvendas.pgfreg.page3.pgfpagcentral.page2.grdpagpendentes.sel.check1.value =  .NOT. (regvendas.pgfreg.page3.pgfpagcentral.page2.grdpagpendentes.sel.check1.value)
          uf_perguntalt_chama("ESSE ATENDIMENTO FOI ABATIDO.", "OK", "", 64)
          RETURN .F.
       ENDIF
       IF ucrspagcentral.fechado=.T.
          regvendas.pgfreg.page3.pgfpagcentral.page2.grdpagpendentes.sel.check1.value =  .NOT. (regvendas.pgfreg.page3.pgfpagcentral.page2.grdpagpendentes.sel.check1.value)
          uf_perguntalt_chama("ESSE ATENDIMENTO J� FOI PAGO.", "OK", "", 64)
          RETURN .F.
       ENDIF
       SELECT ucrspca
       COUNT TO lcpcarows
       IF lcpcarows>0
          SELECT ucrspca
          GOTO TOP
          LOCATE FOR ucrspca.vendedor==ucrspagcentral.vendedor .AND. ucrspca.terminal==ucrspagcentral.terminal
          IF  .NOT. FOUND()
             regvendas.pgfreg.page3.pgfpagcentral.page2.grdpagpendentes.sel.check1.value =  .NOT. (regvendas.pgfreg.page3.pgfpagcentral.page2.grdpagpendentes.sel.check1.value)
             uf_perguntalt_chama("S� PODE SOMAR ATENDIMENTO SE PERTENCEREM AO MESMO VENDEDOR E TERMINAL.", "OK", "", 64)
             RETURN .F.
          ENDIF
       ENDIF
       SELECT ucrspca
       LOCATE FOR ucrspca.nratend==ucrspagcentral.nratend
       IF FOUND()
          SELECT ucrspca
          DELETE
          GOTO BOTTOM
          uf_pagcentral_calctotalpag()
          regvendas.pgfreg.page3.pgfpagcentral.page1.grdpagcentral.refresh
          RETURN .T.
       ENDIF
       SELECT ucrspca
       APPEND BLANK
       REPLACE ucrspca.stamp WITH ucrspagcentral.stamp
       REPLACE ucrspca.nratend WITH ucrspagcentral.nratend
       REPLACE ucrspca.nrvendas WITH ucrspagcentral.nrvendas
       REPLACE ucrspca.total WITH ucrspagcentral.total
       REPLACE ucrspca.ano WITH ucrspagcentral.ano
       REPLACE ucrspca.no WITH ucrspagcentral.no
       REPLACE ucrspca.estab WITH ucrspagcentral.estab
       REPLACE ucrspca.vendedor WITH ucrspagcentral.vendedor
       REPLACE ucrspca.terminal WITH ucrspagcentral.terminal
       REPLACE ucrspca.dinheiro WITH ucrspagcentral.dinheiro
       REPLACE ucrspca.mb WITH ucrspagcentral.mb
       REPLACE ucrspca.visa WITH ucrspagcentral.visa
       REPLACE ucrspca.cheque WITH ucrspagcentral.cheque
       REPLACE ucrspca.troco WITH ucrspagcentral.troco
       uf_pagcentral_calctotalpag()
       regvendas.pgfreg.page3.pgfpagcentral.page1.grdpagcentral.refresh
       IF lcbool
          IF regvendas.pgfreg.page3.pgfpagcentral.activepage==2
             LOCAL lcpos
             lcpos = 0
             SELECT ucrsatendpendpag
             lcpos = RECNO("uCrsAtendPendPag")
             GOTO TOP
             SCAN FOR ALLTRIM(ucrsatendpendpag.nratend)==ALLTRIM(regvendas.txtnatend.value)
                REPLACE ucrsatendpendpag.sel WITH .T.
             ENDSCAN
             SELECT ucrsatendpendpag
             GOTO lcpos
             regvendas.pgfreg.page3.pgfpagcentral.page1.grdpagcentral.refresh
          ENDIF
       ENDIF
    ENDIF
 ENDIF
ENDFUNC
**
PROCEDURE uf_pagcentral_calctotalpag
 SELECT ucrspca
 CALCULATE SUM(ucrspca.total) TO regvendas.ctnpagamento.txtvalatend.value 
 IF regvendas.ctnpagamento.txtvalatend.value<0
    regvendas.ctnpagamento.txtdinheiro.value = astr(ROUND(regvendas.ctnpagamento.txtvalatend.value, 2), 8, 2)
 ELSE
    regvendas.ctnpagamento.txtdinheiro.value = ""
 ENDIF
ENDPROC
**
PROCEDURE uf_pagcentral_limparvalores
 IF USED("uCrsPagCentral")
    fecha("uCrsPagCentral")
 ENDIF
 IF USED("uCrsAtendPendPag2")
    fecha("uCrsAtendPendPag2")
 ENDIF
 IF USED("uCrsAtendPendPag")
    SELECT ucrsatendpendpag
    DELETE ALL
 ENDIF
 IF USED("uCrsPca")
    SELECT ucrspca
    DELETE ALL
 ENDIF
 regvendas.txtnatend.value = ''
 regvendas.txtnatend.setfocus
 regvendas.pgfreg.page3.pgfpagcentral.page1.grdpagcentral.refresh
 uf_regvendas_resetvalorespagamento()
 uf_pagcentral_totalporpagar()
ENDPROC
**
FUNCTION uf_pagcentral_abateratend
 SELECT ucrsatendpendpag
 IF  .NOT. EMPTY(ucrsatendpendpag.nratend)
    IF uf_perguntalt_chama("Aten��o: Vai abater o atendimento ["+ALLTRIM(ucrsatendpendpag.nratend)+"] dos pendentes."+CHR(13)+"Tem a certeza que pretende continuar?", "Sim", "N�o")
       LOCAL lcvalida
       STORE .F. TO lcvalida
       SELECT ucrspca
       GOTO TOP
       LOCATE FOR ALLTRIM(ucrspca.nratend)==ALLTRIM(ucrsatendpendpag.nratend)
       IF FOUND()
          lcvalida = .T.
       ENDIF
       IF  .NOT. lcvalida
          LOCAL lcstamp
          lcstamp = uf_gerais_stamp()
          TEXT TO lcsql TEXTMERGE NOSHOW
					UPDATE B_pagCentral SET
					 	fechado		= 1,
						abatido		= 1,
						fechaStamp	= '<<Alltrim(lcStamp)>>',
						fechaUser	= <<ch_userno>>,
						udata 		= dateadd(HOUR, <<difhoraria>>, getdate())
					where 
						stamp	= '<<Alltrim(uCrsAtendPendPag.stamp)>>'
          ENDTEXT
          IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
             uf_perguntalt_chama("OCORREU UM ERRO A ABATER O ATENDIMENTO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
             RETURN .F.
          ELSE
             uf_perguntalt_chama("ATENDIMENTO ABATIDO COM SUCESSO.", "OK", "", 64)
             TEXT TO lcsql TEXTMERGE NOSHOW
						INSERT INTO B_ocorrencias (stamp, linkstamp, tipo, grau, descr,ovalor, dvalor,usr, date)
						values
							(LEFT(newid(),25), '<<alltrim(uCrsAtendPendPag.stamp)>>', 'Caixa', 2, 'Abate do Atendimento nos Pagamentos Centralizados',
							'nrAtend: ' + '<<alltrim(uCrsAtendPendPag.nrAtend)>>' + ' Total: ' + '<<round(uCrsAtendPendPag.total,2)>>', '',
							<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()))
             ENDTEXT
             IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
                uf_perguntalt_chama("ATEN��O: OCORREU UM PROBLEMA A GUARDAR REGISTO DE ABATE DO ATENDIMENTO.", "OK", "", 16)
             ENDIF
             LOCAL lcpos
             STORE 0 TO lcpos
             SELECT ucrsatendpendpag
             lcpos = RECNO("uCrsAtendPendPag")-1
             SELECT ucrsatendpendpag
             DELETE
             SELECT ucrsatendpendpag
             TRY
                GOTO lcpos
             CATCH
                GOTO TOP
             ENDTRY
             regvendas.pgfreg.page3.pgfpagcentral.page2.grdpagpendentes.refresh
             uf_pagcentral_totalporpagar()
          ENDIF
       ELSE
          uf_perguntalt_chama("ESSE ATENDIMENTO ENCONTRA-SE ACTUALMENTE ADICIONADO AO PAGAMENTO. REMOVA O ATENDIMENTO DA LISTA DE PAGAMENTO PARA O PODER ABATER.", "OK", "", 64)
       ENDIF
    ENDIF
 ENDIF
ENDFUNC
**
FUNCTION uf_pagcentral_registarvalores
 IF  .NOT. USED("uCrsPca")
    uf_perguntalt_chama("N�O EXISTE NADA A REGISTAR.", "OK", "", 64)
    RETURN .F.
 ELSE
    LOCAL lcnrrowspca
    STORE 0 TO lcnrrowspca
    SELECT ucrspca
    COUNT TO lcnrrowspca
    IF lcnrrowspca==0
       uf_perguntalt_chama("N�O EXISTE NADA A REGISTAR.", "OK", "", 64)
       RETURN .F.
    ELSE
       SELECT ucrspca
       GOTO TOP
    ENDIF
 ENDIF
 IF uf_gerais_getparameter("ADM0000000037", "BOOL")
    IF VAL(regvendas.ctnpagamento.txtmb.value)+VAL(regvendas.ctnpagamento.txtvisa.value)+VAL(regvendas.ctnpagamento.txtcheque.value)>=ROUND(regvendas.ctnpagamento.txtvalatend.value, 2) .AND. (VAL(regvendas.ctnpagamento.txtmb.value)<>0 .OR. VAL(regvendas.ctnpagamento.txtvisa.value)<>0 .OR. VAL(regvendas.ctnpagamento.txtcheque.value)<>0) .AND. VAL(regvendas.ctnpagamento.txtdinheiro.value)<>0
       uf_perguntalt_chama("APENAS PODER� INCLUIR TROCO NO MEIO DE PAGAMENTO A DINHEIRO. DEVE CORRIGIR A SITUA��O ANTES DE PODER CONTINUAR.", "OK", "", 48)
       RETURN .F.
    ENDIF
    IF VAL(regvendas.ctnpagamento.txtmb.value)+VAL(regvendas.ctnpagamento.txtvisa.value)+VAL(regvendas.ctnpagamento.txtcheque.value)>ROUND(regvendas.ctnpagamento.txtvalatend.value, 2) .AND. (VAL(regvendas.ctnpagamento.txtmb.value)<>0 .OR. VAL(regvendas.ctnpagamento.txtvisa.value)<>0 .OR. VAL(regvendas.ctnpagamento.txtcheque.value)<>0) .AND. VAL(regvendas.ctnpagamento.txtdinheiro.value)=0
       uf_perguntalt_chama("APENAS PODER� INCLUIR TROCO NO MEIO DE PAGAMENTO A DINHEIRO. DEVE CORRIGIR A SITUA��O ANTES DE PODER CONTINUAR.", "OK", "", 48)
       RETURN .F.
    ENDIF
 ENDIF
 IF (ROUND(VAL(regvendas.ctnpagamento.txtvalrec.value), 2)<ROUND(regvendas.ctnpagamento.txtvalatend.value, 2))
    uf_perguntalt_chama("ATEN��O: N�O PODE FINALIZAR COM O VALOR RECEBIDO INFERIOR AO VALOR TOTAL.", "OK", "", 48)
    RETURN .F.
 ENDIF
 LOCAL lccntmodpag
 STORE 0 TO lccntmodpag
 IF  .NOT. EMPTY(regvendas.ctnpagamento.txtdinheiro.value)
    lccntmodpag = lccntmodpag+1
 ENDIF
 IF  .NOT. EMPTY(regvendas.ctnpagamento.txtmb.value)
    lccntmodpag = lccntmodpag+1
 ENDIF
 IF  .NOT. EMPTY(regvendas.ctnpagamento.txtvisa.value)
    lccntmodpag = lccntmodpag+1
 ENDIF
 IF  .NOT. EMPTY(regvendas.ctnpagamento.txtcheque.value)
    lccntmodpag = lccntmodpag+1
 ENDIF
 LOCAL lcnrvendas
 STORE 0 TO lcnrvendas
 SELECT ucrspca
 CALCULATE CNT(nrvendas) TO lcnrvendas 
 LOCAL lctroco, lcdinheiro, lcmb, lcvisa, lccheque
 STORE 0 TO lctroco, lcdinheiro, lcmb, lcvisa, lccheque
 IF lcnrvendas>0 .AND. lccntmodpag>0
    IF  .NOT. EMPTY(regvendas.ctnpagamento.txttroco.value)
       lctroco = (regvendas.ctnpagamento.txttroco.value/lcnrvendas)
    ENDIF
    IF  .NOT. EMPTY(regvendas.ctnpagamento.txtdinheiro.value)
       lcdinheiro = VAL(regvendas.ctnpagamento.txtdinheiro.value)/lcnrvendas
       IF lctroco<>0
          lcdinheiro = lcdinheiro-lctroco
       ENDIF
    ENDIF
    IF  .NOT. EMPTY(regvendas.ctnpagamento.txtmb.value)
       lcmb = VAL(regvendas.ctnpagamento.txtmb.value)/lcnrvendas
    ENDIF
    IF  .NOT. EMPTY(regvendas.ctnpagamento.txtvisa.value)
       lcvisa = VAL(regvendas.ctnpagamento.txtvisa.value)/lcnrvendas
    ENDIF
    IF  .NOT. EMPTY(regvendas.ctnpagamento.txtcheque.value)
       lccheque = VAL(regvendas.ctnpagamento.txtcheque.value)/lcnrvendas
    ENDIF
 ENDIF
 LOCAL lcpagstamp
 lcpagstamp = uf_gerais_stamp()
 SELECT ucrspca
 GOTO TOP
 SCAN
    TEXT TO lcsql TEXTMERGE NOSHOW
			UPDATE ft2
			SET evdinheiro 	= <<lcDinheiro>>,
				epaga1		= <<lcVisa>>,
				epaga2		= <<lcMb>>,
				etroco		= <<lcTroco>>
			where ft2stamp in (select ftstamp from ft where u_nratend='<<Alltrim(uCrsPca.nrAtend)>>' and ndoc!=1 and ftano=<<uCrsPca.ano>>)
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
       uf_perguntalt_chama("OCORREU UM ERRO A REGISTAR VALORES DE PAGAMENTO. POR FAVOR CONTACTE O SUPORTE."+CHR(13)+"Cod.[FT2]", "OK", "", 16)
       RETURN .F.
    ENDIF
    TEXT TO lcsql TEXTMERGE NOSHOW
			UPDATE  ft
			SET echtotal = <<lcCheque>>
			where u_nratend = '<<Alltrim(uCrsPca.nrAtend)>>' and ndoc!=1 and ftano=<<uCrsPca.ano>>
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
       uf_perguntalt_chama("OCORREU UM ERRO A REGISTAR VALORES DE PAGAMENTO. POR FAVOR CONTACTE O SUPORTE."+CHR(13)+"Cod.[FT]", "OK", "", 16)
       RETURN .F.
    ENDIF
    TEXT TO lcsql TEXTMERGE NOSHOW
			UPDATE re
			SET evdinheiro 	= <<lcDinheiro>>,
				epaga1		= <<lcVisa>>,
				epaga2		= <<lcMb>>,
				echtotal	= <<lcCheque>>
			where u_nratend='<<Alltrim(uCrsPca.nrAtend)>>' and olcodigo!='R00003' and reano=<<uCrsPca.ano>>
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
       uf_perguntalt_chama("OCORREU UM ERRO A REGISTAR VALORES DE PAGAMENTO. POR FAVOR CONTACTE O SUPORTE."+CHR(13)+"Cod.[RE]", "OK", "", 16)
       RETURN .F.
    ENDIF
    TEXT TO lcsql TEXTMERGE NOSHOW
			UPDATE B_pagCentral
			SET	evdinheiro 				= <<lcDinheiro>>,
				evdinheiro_semTroco		= <<lcDinheiro>> + <<lcTroco>>,
				epaga1					= <<lcVisa>>,
				epaga2					= <<lcMb>>,
				echtotal				= <<lcCheque>>,
				etroco					= <<lcTroco>>,
				dinheiro 				= <<Val(REGVENDAS.ctnPagamento.txtdinheiro.value)>>,
				visa					= <<Val(REGVENDAS.ctnPagamento.txtvisa.value)>>,
				mb						= <<Val(REGVENDAS.ctnPagamento.txtmb.value)>>,
				cheque					= <<Val(REGVENDAS.ctnPagamento.txtcheque.value)>>,
				troco					= <<REGVENDAS.ctnPagamento.txtTroco.value>>,
				fechado					= 1,
				fechaStamp				= '<<Alltrim(lcPagStamp)>>',
				fechaUser				= <<ch_userno>>,
				udata					= dateadd(HOUR, <<difhoraria>>, getdate())
			where stamp	= '<<Alltrim(uCrsPca.stamp)>>'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
       uf_perguntalt_chama("OCORREU UM ERRO A REGISTAR VALORES DE PAGAMENTO. POR FAVOR CONTACTE O SUPORTE."+CHR(13)+"Cod.[pagCentral]", "OK", "", 16)
       RETURN .F.
    ENDIF
    SELECT ucrspca
 ENDSCAN
 uf_perguntalt_chama("OPERA��O CONCLU�DA COM SUCESSO.", "OK", "", 64)
 uf_pagcentral_limparvalores()
ENDFUNC
**
