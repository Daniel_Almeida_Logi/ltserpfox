**
FUNCTION uf_gestaocaixas_chama
 LPARAMETERS lcmarcacoes, lcpcentral
 PUBLIC myidentificacaomarcacoes
 myidentificacaomarcacoes = lcmarcacoes
 PUBLIC mysacodinheiro, mycxuser, myaltercaixa
 STORE '' TO mycxstamp, mycxuser
 STORE .F. TO mysacodinheiro, myaltercaixa
 SELECT ucrse1
 GOTO TOP
 IF TYPE("FACTURACAO")<>'U' AND UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'
    IF myftintroducao=.T. .AND. facturacao.pageframe1.page2.movcaixa.tag=="false"
       uf_perguntalt_chama("ATEN��O: N�O PODE ABRIR O ECR� DE ATENDIMENTO COM O ECR� DE FATURA��O ABERTO. POR FAVOR FECHE O MESMO.", "OK", "", 48)
       RETURN .F.
    ENDIF
    IF myftintroducao=.F.
       uf_perguntalt_chama("ATEN��O: N�O PODE ABRIR O ECR� DE ATENDIMENTO COM O ECR� DE FATURA��O ABERTO. POR FAVOR FECHE O MESMO.", "OK", "", 48)
       RETURN .F.
    ENDIF
 ENDIF
 IF TYPE("PESQFACT")<>'U'
    uf_perguntalt_chama("ATEN��O: N�O PODE ABRIR O ECR� DE ATENDIMENTO COM O ECR� DE PESQUISA DE FATURA��O ABERTO. POR FAVOR FECHE O MESMO.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF TYPE("DOCUMENTOS")<>'U'
    IF TYPE("FACTURACAO")<>'U' .AND. myftintroducao=.T. .AND. facturacao.pageframe1.page2.movcaixa.tag=="false"
       uf_perguntalt_chama("ATEN��O: N�O PODE ABRIR O ECR� DE ATENDIMENTO COM O ECR� DE DOCUMENTOS ABERTO. POR FAVOR FECHE O MESMO.", "OK", "", 48)
       RETURN .F.
    ENDIF
 ENDIF
 IF TYPE("PESQDOCUMENTOS")<>'U'
    uf_perguntalt_chama("ATEN��O: N�O PODE ABRIR O ECR� DE ATENDIMENTO COM O ECR� DE PESQUISA DE DOCUMENTOS ABERTO. POR FAVOR FECHE O MESMO.", "OK", "", 48)
    RETURN .F.
 ENDIF
 mysacodinheiro = uf_gerais_getparameter('ADM0000000040', 'bool')
 IF ( .NOT. EMPTY(lcpcentral) .AND. mypagcentral==.F.) .OR. lcpcentral==.T.
 
	 IF  .NOT. uf_gerais_validapermuser(ch_userno, ch_grupo, 'Atendimento - Abrir Atendimento')
	    uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL DE ATENDIMENTO.", "OK", "", 48)
	    RETURN .F.
	 ENDIF
	 
    uf_gestaocaixas_verificacaixa()
    IF  .NOT. EMPTY(mycxstamp)
       IF uf_gestaocaixas_verificacaixasabertas()==.T.

          IF uf_gerais_getparameter('ADM0000000338', 'bool')
            uf_perguntalt_chama("ATEN��O: TEM UMA CAIXA ABERTA DO DIA ANTERIOR. PARA FECHAR E ABRIR UMA NOVA CAIXA PARA O DIA ATUAL: CAIXAS > FECHAR CX > ABRIR CX", "OK", "", 48)
            RETURN .F.
          ENDIF
          uf_perguntalt_chama("ATEN��O: EXISTEM SESS�ES DE CAIXA ABERTAS DE DIAS ANTERIORES. POR FAVOR VERIFIQUE.", "OK", "", 48)
          uf_gestaocaixas_gravar()
       ELSE
          uf_gestaocaixas_gravar()
       ENDIF
    ELSE
       IF (VARTYPE(mytpastamp)<>"U")
          uf_caixas_validaabrefechatpa(.T.)
       ENDIF
       IF uf_gerais_getparameter('ADM0000000026', 'bool')
          uf_caixas_criarsessao(uf_gerais_getparameter('ADM0000000026', 'num'))
       ELSE
          PUBLIC valfundocx
          valfundocx = 0
          uf_tecladonumerico_chama("ValFundoCX", "Qual o Fundo de Caixa:", 0, .T., .F., 6, 2)
          uf_caixas_criarsessao(valfundocx)
       ENDIF
       uf_gestaocaixas_gravar()
    ENDIF
    RETURN .T.
 ENDIF
 IF TYPE("GestaoCaixas")=="U"
    uf_gestaocaixas_cursoresdefault()
    DO FORM GestaoCaixas
    IF TYPE("GestaoCaixas.menu1.actualizar")=="U"
       gestaocaixas.menu1.adicionaopcao("opcoes", "Op��es", mypath+"\imagens\icons\opcoes_w.png", "", "F1")
       gestaocaixas.menu1.adicionaopcao("actualizar", "Actualizar", mypath+"\imagens\icons\actualizar_w.png", "uf_GestaoCaixas_pesquisar", "F2")
       gestaocaixas.menu1.adicionaopcao("diaAnt", "Dia Ant.", mypath+"\imagens\icons\ponta_seta_left_w.png", "uf_GestaoCaixas_diaAnt", "F3")
       gestaocaixas.menu1.adicionaopcao("diaSeg", "Dia Seg.", mypath+"\imagens\icons\ponta_seta_rigth_w.png", "uf_GestaoCaixas_diaSeg", "F4")
       gestaocaixas.menu1.adicionaopcao("movimentaCaixa", "Mov. Caixa", mypath+"\imagens\icons\gestao_caixa.png", "uf_GestaoCaixas_movimentaCaixa", "F5")
       IF mypagcentral==.T.
          gestaocaixas.menu1.adicionaopcao("pagcentral", "Pag. Centraliz.", mypath+"\imagens\icons\pag_central.png", "uf_GestaoCaixas_pagamentosCentral", "F6")
       ENDIF
       gestaocaixas.menu1.adicionaopcao("caixa", "Fechar Cx", mypath+"\imagens\icons\fecharCaixa_w.png", "uf_GestaoCaixas_FecharCaixa with .t.", "F8")
       uf_gestaocaixas_menu_manipulaestadotpa()
       gestaocaixas.menu1.adicionaopcao("fecharCxsOp", "Fechar Cxs", mypath+"\imagens\icons\fecharCaixa_w.png", "uf_GestaoCaixas_fecharCaixasPorOperador", "F9")
       IF uf_gerais_getparameter_site('ADM0000000076', 'BOOL', mysite) .AND.  .NOT. uf_gerais_validapermuser(ch_userno, ch_grupo, 'Fecho de Caixa - Alterar')=.T.
          gestaocaixas.menu1.adicionaopcao("alterarCxsOp", "Alterar Cx", mypath+"\imagens\icons\actualizar_w.png", "uf_GestaoCaixas_alterarCx", "F10")
       ENDIF
		
		IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gerir Varios TPA') = .T.		
			gestaocaixas.menu_opcoes.adicionaopcao("gerirTpas", "Abrir/Fechar TPA's", "", "uf_GestaoCaixas_gerirTpas")			
       	ENDIF
       
       gestaocaixas.menu_opcoes.adicionaopcao("abrirDia", "Abrir Dia", "", "uf_GestaoCaixas_reabrirDia")
       gestaocaixas.menu_opcoes.adicionaopcao("fecharDia", "Fechar Dia", "", "uf_GestaoCaixas_FechoDia")
       gestaocaixas.menu_opcoes.adicionaopcao("valTPA", "Valores TPA", "", "uf_GestaoCaixas_ValoresTPAs")
       IF (VARTYPE(mytpastamp)<>"U")
          gestaocaixas.menu_opcoes.adicionaopcao("ultimoReciboTpa", "Reimp. Tal�o TPA", "", "uf_gestaocaixas_mensagemMenu with 1")
       ENDIF
       gestaocaixas.menu_opcoes.adicionaopcao("ReimpTalao", "Reimp. Tal�o Caixa", "", "uf_GestaoCaixas_ReimpTalaoCaixa")
       IF ucrse1.gestao_cx_operador=.F.
          gestaocaixas.menu_opcoes.adicionaopcao("fecharCaixas", "Fechar Todas Cxs", "", "uf_caixas_fecharTodas")
          gestaocaixas.menu_opcoes.adicionaopcao("abrirCaixas", "Abrir Todas Cxs", "", "uf_caixas_abrirTodas")
       ENDIF
       IF  .NOT. EMPTY(uf_gerais_validapermuser(ch_userno, ch_grupo, 'Bot�o an�lise relatorio caixa'))
          gestaocaixas.menu_opcoes.adicionaopcao("relatorioCaixa", "Relat�rio Caixa", "", "uf_GestaoCaixas_relatorioCaixa")
       ENDIF
       IF uf_gerais_getUmValor("b_modoPag", "count(*)", "pagamentoExterno = 1") > 0
	       gestaocaixas.menu_opcoes.adicionaopcao("pagExterno", "Pag. Externo", "", "uf_faturacaoPagExterno_chama")
	   ENDIF
       gestaocaixas.menu1.gravar.img1.picture = mypath+"\imagens\icons\guardar_w.png"
       gestaocaixas.menu1.estado('fecharCxsOp', "HIDE")
       IF (TYPE("atendimento")=="U")
          gestaocaixas.menu1.estado("", "", "Atendimento", .T.)
       ENDIF

       
       if(USED("ucrse1"))
       	   SELECT ucrse1
       	   GO TOP
	 	   IF ucrse1.has_ticket_integration AND TYPE("gestaocaixas.menu_opcoes.resetSenhas") <> "O"
	 	   		gestaocaixas.menu_opcoes.adicionaopcao("resetSenhas", "Reset Senhas", "", "uf_chilkat_send_request_resetCounters_ticket")
	 	   endif
       endif
       
       
    ENDIF
    IF uf_gerais_getparameter("ADM0000000140", "BOOL")
       gestaocaixas.menu_opcoes.estado("abrirDia", "SHOW")
    ELSE
       gestaocaixas.menu_opcoes.estado("abrirDia", "HIDE")
    ENDIF
    IF ucrse1.gestao_cx_operador=.F.
       IF uf_gerais_getparameter("ADM0000000040", "BOOL")
          gestaocaixas.menu_opcoes.estado("fecharCaixas, abrirCaixas", "HIDE")
       ELSE
          gestaocaixas.menu_opcoes.estado("fecharCaixas, abrirCaixas", "SHOW")
       ENDIF
    ENDIF
 ELSE
    
    IF WEXIST("GESTAOCAIXAS")
        **gestaocaixas.show
    ENDIF
    
 ENDIF
 WITH gestaocaixas.pageframe1
    IF ucrse1.gestao_cx_operador=.T.
       .activepage = 2
    ELSE
       .activepage = 1
    ENDIF
 ENDWITH
 uf_gestaocaixas_pesquisar()
 WITH gestaocaixas
    .txtdata.value = uf_gerais_getdate(DATETIME()+(difhoraria*3600))
    .txtloja.value = ALLTRIM(mysite)
    .txtarmazem.value = myarmazem
    .txtrobo.value = ''
    .txtterminal.value = ALLTRIM(myterm)
    .txtusername.value = ALLTRIM(ch_vendnm)
 ENDWITH
 uf_gestaocaixas_verificacaixa()
 uf_gestaocaixas_verificarobot()
 uf_gestaocaixas_verificacashdro()
 uf_gestaocaixas_actestado()
 IF ucrse1.gestao_cx_operador=.T.
    uf_gestaocaixas_calculototais()
 ENDIF
 
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
			select cast(campo as numeric(9,2)) as ID, 0 as contagem from b_multidata where tipo = 'NUMERARIO-MOEDAS' and upper(pais) = '<<UPPER(ALLTRIM(myPaisConfSoftw))>>' order by id
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "ucrsContMoedas", lcsql)
    uf_perguntalt_chama("N�o foi poss�vel Construir o cursor para contagem de numer�rio - Moedas. Por favor contacte o suporte.", "OK", "", 32)
    regua(2)
    RETURN .F.
 ENDIF
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
			select cast(campo as numeric(9,0)) as ID, 0 as contagem from b_multidata where tipo = 'NUMERARIO-NOTAS' and upper(pais) = '<<UPPER(ALLTRIM(myPaisConfSoftw))>>' order by id
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "ucrsContNotas", lcsql)
    uf_perguntalt_chama("N�o foi poss�vel Construir o cursor para contagem de numer�rio - Notas. Por favor contacte o suporte.", "OK", "", 32)
    regua(2)
    RETURN .F.
 ENDIF
 uf_gestaocaixas_config()
ENDFUNC
**
PROCEDURE uf_gestaocaixas_abrefechatpa
 LPARAMETERS lcsatus
 LOCAL msgtype, msgwait
 RELEASE lcresponsearray
 DIMENSION lcresponsearray(2)
 IF  .NOT. EMPTY(lcsatus)
    msgtype = "app.msg.accounting.period.open"
    msgwait = "A abrir sess�o do Tpa..."
 ELSE
    msgtype = "app.msg.accounting.period.close"
    msgwait = "A fechar sess�o do Tpa..."
 ENDIF
 uf_tpa_operation(@lcresponsearray, msgwait, "", msgtype, 0)
 IF (lcresponsearray(1)==0)
    IF ( .NOT. EMPTY(lcresponsearray(2)))
       IF EMPTY(lcsatus)
          IF (VARTYPE(gestaocaixas.menu1.estadotpa)<>"U")
             **gestaocaixas.menu1.estadotpa.config("Abrir Tpa", mypath+"\imagens\icons\abrirCaixa_w.png", "uf_gestaocaixas_abreFechaTpa with .t.", "F7")
          ENDIF
          gestaocaixas.txttpa.value = "Fechado"
       ELSE
          IF (VARTYPE(gestaocaixas.menu1.estadotpa)<>"U")
             **gestaocaixas.menu1.estadotpa.config("Fechar Tpa", mypath+"\imagens\icons\fecharCaixa_w.png", "uf_gestaocaixas_abreFechaTpa with .f.", "F7")
          ENDIF
          gestaocaixas.txttpa.value = "Aberto"
       ENDIF
    ELSE
       gestaocaixas.txttpa.value = "N/D"
    ENDIF
 ELSE
    gestaocaixas.txttpa.value = "N/D"
    uf_perguntalt_chama(uf_tpa_messagemresposta(ALLTRIM(lcresponsearray(2))), "OK", "", 16)
 ENDIF
 RELEASE lcresponsearray
ENDPROC
**
PROCEDURE uf_GestaoCaixas_gerirTpas
	uf_gesttpa_chama()
ENDPROC
** 
PROCEDURE uf_gestaocaixas_mensagemmenu
 LPARAMETERS lcmensagemtipo
 LOCAL msgtype, msgwait
 RELEASE lcresponsearray
 DIMENSION lcresponsearray(2)
 DO CASE
    CASE lcmensagemtipo=1
       msgtype = 'app.msg.menu.repeat_last_receipt'
       msgwait = "A imprimir �ltimo tal�o.."
    CASE lcmensagemtipo=2
       msgtype = 'app.msg.menu.supervisor'
       msgwait = 'A abrir menu de supervisor...'
 ENDCASE
 uf_tpa_operation(@lcresponsearray, msgwait, "", msgtype, 0)
 IF (VARTYPE(lcresponsearray(1))<>"U")
    IF (lcresponsearray(1)<>0)
       IF (VARTYPE(lcresponsearray(2))<>"U")
          uf_perguntalt_chama(uf_tpa_messagemresposta(ALLTRIM(lcresponsearray(2))), "OK", "", 16)
       ENDIF
    ENDIF
 ENDIF
 RELEASE lcresponsearray
ENDPROC
**
PROCEDURE uf_gestaocaixas_menu_manipulaestadotpa
 LOCAL lcstate
 IF (VARTYPE(mytpastamp)<>"U")
    RELEASE lcresponsearray
    DIMENSION lcresponsearray(2)
    uf_tpa_operation(@lcresponsearray, "A validar estado TPA...", "", "app.msg.status.tpa", 0)
    IF ( .NOT. EMPTY(lcresponsearray(2)))
       DO CASE
          CASE (ALLTRIM(lcresponsearray(2))=="Aberto")
             IF (VARTYPE(gestaocaixas.menu1.estadotpa)=="U")
                gestaocaixas.menu1.adicionaopcao("estadoTpa", "Fechar Tpa", mypath+"\imagens\icons\fecharCaixa_w.png", "uf_gestaocaixas_abreFechaTpa with .f.", "F7")
             ENDIF
          CASE (ALLTRIM(lcresponsearray(2))=="Fechado")
             IF (VARTYPE(gestaocaixas.menu1.estadotpa)=="U")
                gestaocaixas.menu1.adicionaopcao("estadoTpa", "Abrir Tpa", mypath+"\imagens\icons\abrirCaixa_w.png", "uf_gestaocaixas_abreFechaTpa with .t.", "F7")
             ENDIF
          OTHERWISE
             uf_perguntalt_chama(uf_tpa_messagemresposta(ALLTRIM(lcresponsearray(2))), "OK", "", 16)
       ENDCASE
    ENDIF
    IF ( .NOT. EMPTY(lcresponsearray(2)))
       gestaocaixas.txttpa.value = ALLTRIM(lcresponsearray(2))
    ELSE
       gestaocaixas.txttpa.value = "N/D"
    ENDIF
    RELEASE lcresponsearray
 ENDIF
ENDPROC
**
FUNCTION uf_gestaocaixas_cursoresdefault
 IF USED("uCrsRegitosValoreCaixa")
    fecha("uCrsRegitosValoreCaixa")
 ENDIF
 CREATE CURSOR uCrsRegitosValoreCaixa (valor N(9, 2))
 SELECT ucrsregitosvalorecaixa
 APPEND BLANK
 REPLACE ucrsregitosvalorecaixa.valor WITH 0
 IF ucrse1.gestao_cx_operador=.T.
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_caixa_FechoDia '', '', 1
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "uCrsFechoDia", lcsql)
       uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es de Caixa", "OK", "", 16)
       RETURN .F.
    ENDIF
 ELSE
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_caixa_FechoDia '', '', 0
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "uCrsFechoDia", lcsql)
       uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es de Caixa", "OK", "", 16)
       RETURN .F.
    ENDIF
 ENDIF
ENDFUNC
**
PROCEDURE uf_gestaocaixas_diaant
 LOCAL lcdia
 lcdia = CTOD(uf_gerais_getdate(gestaocaixas.txtdata.value))-1
 gestaocaixas.txtdata.value = uf_gerais_getdate(lcdia)
 gestaocaixas.menu1.actualizar.click()
ENDPROC
**
PROCEDURE uf_gestaocaixas_diaseg
 LOCAL lcdia
 lcdia = CTOD(uf_gerais_getdate(gestaocaixas.txtdata.value))+1
 gestaocaixas.txtdata.value = uf_gerais_getdate(lcdia)
 gestaocaixas.menu1.actualizar.click()
ENDPROC
**
FUNCTION uf_gestaocaixas_actestado

 uv_thisForm = uf_gerais_getFormName("GESTAOCAIXAS")

 IF  .NOT. USED("uCrsFechoDia")
    RETURN .F.
 ENDIF
 SELECT ucrsfechodia
 IF uf_gerais_actgrelha("", "uCrsValFd", [Select fechado From b_fechoDia (nolock) Where fdstamp= ']+ALLTRIM(ucrsfechodia.fdstamp)+['])
    IF RECCOUNT()>0
       IF ucrsvalfd.fechado==.T.
          uv_thisForm.txtestado.value = "Dia Fechado"
       ELSE
          uv_thisForm.txtestado.value = "Dia Aberto"
       ENDIF
    ELSE
       uv_thisForm.txtestado.value = "Sem Sess�o"
    ENDIF
    fecha("uCrsValFd")
 ENDIF
ENDFUNC
**
FUNCTION uf_gestaocaixas_pesquisar
 LOCAL lcsql, lcgestao

 uv_thisForm = uf_gerais_getFormName("GESTAOCAIXAS")

 WITH uv_thisForm.pageframe1
    IF .activepage=1
       lcsql = ''
       TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_caixa_FechoDia '<<uf_gerais_getDate(uv_thisForm.txtData.value,"SQL")>>', '<<Alltrim(uv_thisForm.txtLoja.value)>>', 0
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("uv_thisForm.Pageframe1.Page1.grdPesq", "uCrsFechoDia", lcsql)
          uf_perguntalt_chama("N�o foi poss�vel verificar as sess�es de caixa abertas por Terminal.", "OK", "", 16)
          RETURN .F.
       ENDIF
       uv_thisForm.pageframe1.page1.grdpesq.setfocus
    ELSE
       lcsql = ''
       TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_caixa_FechoDia '<<uf_gerais_getDate(uv_thisForm.txtData.value,"SQL")>>', '<<Alltrim(uv_thisForm.txtLoja.value)>>', 1
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("uv_thisForm.Pageframe1.Page2.grdPesqOp", "uCrsFechoDia", lcsql)
          uf_perguntalt_chama("N�o foi poss�vel verificar as sess�es de caixa abertas por Operador.", "OK", "", 16)
          RETURN .F.
       ENDIF
       IF  .NOT. uf_gerais_validapermuser(ch_userno, ch_grupo, 'Consulta Caixa Operadores')==.F.
          DELETE FROM uCrsFechoDia WHERE ucrsfechodia.auserno<>ch_vendedor
          SELECT ucrsfechodia
          GOTO TOP
       ENDIF
       uv_thisForm.pageframe1.page2.grdpesqop.setfocus
    ENDIF
 ENDWITH
 uf_gestaocaixas_actestado()
 IF ucrse1.gestao_cx_operador=.T.
    uf_gestaocaixas_calculototais()
 ENDIF
ENDFUNC
**
FUNCTION uf_gestaocaixas_valorestpas
 IF  .NOT. (uf_gerais_validapermuser(ch_userno, ch_grupo, 'Fecho do dia - Alterar'))
    uf_perguntalt_chama("O SEU PERFIL N�O PERMITE O REGISTO DE VALORES.", "OK", "", 48)
    RETURN .F.
 ENDIF
 uf_valorestpa_chama(gestaocaixas.txtdata.value, gestaocaixas.txtloja.value)
ENDFUNC
**
FUNCTION uf_gestaocaixas_reabrirdia
 IF  .NOT. (uf_gerais_validapermuser(ch_userno, ch_grupo, 'Fecho do Dia - Re-Abrir Dia'))
    uf_perguntalt_chama("O SEU PERFIL N�O PERMITE REABRIR O DIA.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF  .NOT. USED("uCrsFechoDia")
    RETURN .F.
 ENDIF
 IF  .NOT. ALLTRIM(gestaocaixas.txtestado.value)=="Dia Fechado"
    uf_perguntalt_chama("N�O EXISTE 'SESS�O DE DIA' FECHADA NESTE DIA.", "OK", "", 48)
    RETURN .F.
 ENDIF
 SELECT ucrsfechodia
 IF uf_perguntalt_chama("ATEN��O: VAI REABRIR O DIA. TEM A CERTEZA QUE PRETENDE CONTINUAR?"+CHR(13)+CHR(13)+"ESTA OPERA��O SER� REGISTADA NAS OCORR�NCIAS.", "Sim", "N�o")
    SELECT ucrsfechodia
    IF  .NOT. uf_gerais_actgrelha("", "", [Update b_fechodia set fechado=0 where fdstamp=']+ALLTRIM(ucrsfechodia.fdstamp)+['])
       uf_perguntalt_chama("OCORREU UM PROBLEMA A REABRIR O DIA.", "OK", "", 48)
       RETURN .F.
    ELSE
       uf_gerais_registaocorrencia('Caixa', 'Re-Abertura da Sess�o do Dia', 3, '', '', ALLTRIM(ucrsfechodia.fdstamp))
       gestaocaixas.txtestado.value = "Dia Aberto"
    ENDIF
 ENDIF
ENDFUNC
**
FUNCTION uf_gestaocaixas_fechodia
 LOCAL lcsql
 IF  .NOT. (uf_gerais_validapermuser(ch_userno, ch_grupo, 'Fecho do dia - Alterar'))
    uf_perguntalt_chama("O SEU PERFIL N�O PERMITE O REGISTO DE VALORES.", "OK", "", 48)
    RETURN .F.
 ELSE
    IF  .NOT. uf_perguntalt_chama("VAI PROCEDER AO FECHO DO DIA, TEM A CERTEZA QUE PRETENDE CONTINUAR?", "Sim", "N�o")
       RETURN .F.
    ENDIF
 ENDIF
 IF  .NOT. gestaocaixas.txtestado.value=="Dia Aberto"
    uf_perguntalt_chama("N�O EXISTE 'SESS�O DE DIA' ABERTA NESTE DIA.", "OK", "", 48)
    RETURN .F.
 ENDIF
 SELECT ucrsfechodia
 GOTO TOP
 SCAN FOR  .NOT. ucrsfechodia.fechada
    uf_perguntalt_chama("N�O PODE FECHAR O DIA ENQUANTO HOUVER CAIXAS ABERTAS.", "OK", "", 64)
    RETURN .F.
 ENDSCAN
 SELECT ucrsfechodia
 GOTO TOP
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		update 
			CX
		set
			cx.diaFechado = 1
		where
			cx.fdstamp ='<<ALLTRIM(uCrsFechoDia.fdstamp)>>'
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
    uf_perguntalt_chama("OCORREU UM ERRO A FECHAR O DIA.", "OK", "", 16)
    RETURN .F.
 ENDIF
 SELECT ucrsfechodia
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		Update b_fechoDia Set 
			fechado=1, 
			dataFecho=dateadd(HOUR, <<difhoraria>>, getdate()), 
			userFecho='<<m_chinis>>', 
			usr=<<ch_userno>>, 
			usrdata=dateadd(HOUR, <<difhoraria>>, getdate())
		where
			fdStamp='<<Alltrim(uCrsFechoDia.fdstamp)>>'
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
    uf_perguntalt_chama("OCORREU UM ERRO A FECHAR O DIA.", "OK", "", 16)
    RETURN .F.
 ENDIF
 uf_perguntalt_chama("DIA FECHADO COM SUCESSO.", "OK", "", 64)
 gestaocaixas.txtestado.value = "Dia Fechado"
ENDFUNC
**
PROCEDURE uf_gestaocaixas_sair
 
 gestaocaixas.release
 RELEASE gestaocaixas
 IF USED("uCrsFechoDia")
    fecha("uCrsFechoDia")
 ENDIF
ENDPROC
**
PROCEDURE uf_gestaocaixas_sombra
 LPARAMETERS tcbool
 IF tcbool
    gestaocaixas.txtestado.setfocus
    gestaocaixas.sombra.width = gestaocaixas.width
    gestaocaixas.sombra.height = gestaocaixas.height
    gestaocaixas.sombra.left = 0
    gestaocaixas.sombra.top = 0
    gestaocaixas.sombra.visible = .T.
 ELSE
    gestaocaixas.sombra.visible = .F.
 ENDIF
ENDPROC
**
FUNCTION uf_gestaocaixas_movimentacaixa
 IF uf_gerais_getparameter_site('ADM0000000060', 'BOOL', mysite)
    IF ALLTRIM(ch_grupo)<>'Administrador' .AND. ALLTRIM(ch_grupo)<>'Supervisores'
       IF  .NOT. uf_gerais_valida_password_admin('CAIXA', 'MOVIMENTO CAIXA')
          RETURN .F.
       ENDIF
    ENDIF
 ENDIF

 LOCAL lcvalidapw
 STORE 0 TO lcvalidapw
 lcvalidapw = uf_painelcentral_login()
 IF lcvalidapw==2
   RETURN .F.
 ENDIF

 IF TYPE("GestaoCaixas")=="U"
 	uf_gestaocaixas_chama()
 ENDIF 

  
 uf_gestaocaixas_sombra(.T.)
 gestaocaixas.ctnmovimentocaixa.visible = .T.
 IF UPPER(mypaisconfsoftw)=='ANGOLA'
    gestaocaixas.ctnmovimentocaixa.labelmotivo.visible = .T.
    gestaocaixas.ctnmovimentocaixa.txtmotivoid.visible = .T.
 ENDIF
ENDFUNC
**
PROCEDURE uf_gestaocaixas_movimentacaixafechar
 gestaocaixas.ctnmovimentocaixa.visible = .F.
 uf_gestaocaixas_sombra(.F.)
ENDPROC
**
FUNCTION uf_gestaocaixas_movimentacaixagravar
 IF  .NOT. uf_perguntalt_chama("Vai dar "+IIF(gestaocaixas.ctnmovimentocaixa.chkentrada.tag=="true", "entrada", "sa�da")+" de "+astr(gestaocaixas.ctnmovimentocaixa.txtvalor.value, 9, 2)+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))+" na caixa do Terminal "+astr(mytermno)+CHR(13)+CHR(13)+"Tem a certeza que prentende continuar?", "SIM", "N�O")
    RETURN .F.
 ENDIF
 LOCAL lctipo, lcvalor, lcmotivo, lchora, lcsql, lcpositivonegativo, lcmotivoid
 lctipo = IIF(gestaocaixas.ctnmovimentocaixa.chkentrada.tag=="true", "ENTRADA DE CAIXA", "SAIDA DE CAIXA")
 lcvalor = gestaocaixas.ctnmovimentocaixa.txtvalor.value
 lcmotivo = ALLTRIM(gestaocaixas.ctnmovimentocaixa.txtmotivo.value)
 lcmotivoid = ""
 SET HOURS TO 24
 atime = TTOC(DATETIME()+(difhoraria*3600), 2)
 astr = LEFT(atime, 8)
 lchora = LEFT(astr, 5)
 IF EMPTY(ALLTRIM(mycxstamp))
    uf_perguntalt_chama("N�o pode efectuar movimentos de caixa com a caixa fechada."+CHR(13)+CHR(13)+"Por favor verifique.", "OK", "", 64)
    RETURN .F.
 ENDIF
 IF gestaocaixas.ctnmovimentocaixa.txtvalor.value==0
    uf_perguntalt_chama("N�o introduziu nenhum valor para movimentar caixa.", "OK", "", 64)
    RETURN .F.
 ENDIF
 IF (EMPTY(ALLTRIM(gestaocaixas.ctnmovimentocaixa.txtmotivo.value)) .AND. (UPPER(mypaisconfsoftw)<>'ANGOLA'))
    uf_perguntalt_chama("O motivo � de preenchimento obrigat�rio.", "OK", "", 64)
    RETURN .F.
 ENDIF
 IF UPPER(mypaisconfsoftw)=='ANGOLA' .AND. EMPTY(ALLTRIM(gestaocaixas.ctnmovimentocaixa.txtmotivoid.value))
    uf_perguntalt_chama("O ID motivo � de preenchimento obrigat�rio.", "OK", "", 64)
    RETURN .F.
 ELSE
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			SELECT TOP 1 ID FROM MotivesCashMovements (NOLOCK) WHERE descr = '<<ALLTRIM(GestaoCaixas.ctnMovimentoCaixa.txtMotivoID.value)>>'
    ENDTEXT
    IF uf_gerais_actgrelha("", "uCrsMovimento", lcsql)
       IF RECCOUNT("uCrsMovimento")>0
          SELECT ucrsmovimento
          lcmotivoid = ALLTRIM(ucrsmovimento.id)
          fecha("uCrsMovimento")
       ENDIF
    ENDIF
 ENDIF
 

 
 IF uf_gerais_actgrelha("", "", "BEGIN TRANSACTION")
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			INSERT into B_ocorrencias
				(stamp
				,linkstamp
				,tipo
				,grau
				,descr
				,ovalor
				,dvalor
				,usr
				,date
				,site
			)values(
				LEFT(newid(),25)
				,''
				,'Movimento de Caixa'
				,2
				,'<<lcTipo + " - " + lcMotivo>>'
				,<<lcValor>>
				,''
				,<<ch_userno>>
				,dateadd(HOUR, <<difhoraria>>, getdate())
				,'<<mySite>>'
			)
    ENDTEXT
    nrmovimento = uf_gestaocaixas_geranrmovimento()
    LOCAL lcfechaatendimento
    lcfechaatendimento = 1
    IF (EMPTY(uf_gerais_getparameter_site('ADM0000000040', 'BOOL', mysite)))
       lcfechaatendimento = 0
    ENDIF
    IF uf_gerais_actgrelha("", "", lcsql)
       lcpositivonegativo = IIF(gestaocaixas.ctnmovimentocaixa.chkentrada.tag=="true", 1, -1)
       lcsql = ''
       TEXT TO lcsql TEXTMERGE NOSHOW
				INSERT INTO B_pagCentral(
					stamp
					,nrAtend
					,nrVendas
					,total
					,ano
					,no
					,estab
					,vendedor
					,nome
					,terminal
					,terminal_nome
					,evdinheiro
					,epaga1
					,epaga2
					,echtotal
					,evdinheiro_semTroco
					,etroco
					,total_bruto
					,devolucoes
					,ntCredito
					,creditos
					,cxstamp
					,ssstamp
					,site
					,fechado
					,obs
					,odata
					,udata
					,nrMotivoMov
                    ,ignoraCaixa
				)values(
					'<<astr(YEAR(DATE())) + "Cx" + RIGHT("00" + ASTR(mySite_nr), 2) + nrMovimento>>'
					,'<<"Cx" + RIGHT("00" + ASTR(mySite_nr), 2) + nrMovimento>>', 1, <<Round(lcValor * lcPositivoNegativo,2)>>
					,<<YEAR(DATE())>>
					,200
					,0
					,<<ch_vendedor>>
					,'<<ALLTRIM(ch_vendnm)>>'
					,<<myTermNo>>
					,'<<ALLTRIM(myTerm)>>'
					,<<Round(lcValor * lcPositivoNegativo,2)>>
					,0
					,0
					,0
					,<<Round(lcValor * lcPositivoNegativo,2)>>
					,0
					,<<IIF(lcPositivoNegativo > 0, Round(lcValor * lcPositivoNegativo,2),0)>>
					,0
					,0
					,0
					,'<<ALLTRIM(myCxStamp)>>'
					,'<<ALLTRIM(mySsStamp)>>'
					,'<<ALLTRIM(mySite)>>'
					,<<lcFechaAtendimento>>
					,'<<lcTipo + " - " + lcMotivo>>'
					, dateadd(HOUR, <<difhoraria>>, getdate())
					, dateadd(HOUR, <<difhoraria>>, getdate())
					,'<<ALLTRIM(lcMotivoID)>>' 
                    ,<<IIF(uf_gerais_getparameter('ADM0000000327', 'BOOL') = .T., '0', '1')>>
				)
       ENDTEXT
       IF uf_gerais_actgrelha("", "", lcsql)
          IF uf_gerais_actgrelha("", "", "COMMIT TRANSACTION")
             uf_gerais_gravaultregisto("MovCx", astr(VAL(nrmovimento)))
             uf_imprimirpos_imptalaomovimentocaixa(lctipo, astr(lcvalor, 8, 2), lcmotivo, DATETIME()+(difhoraria*3600), lchora, nrmovimento)
             uf_perguntalt_chama("MOVIMENTO DE CAIXA INTRODUZIDO COM SUCESSO.", "OK", "", 64)
             uf_gestaocaixas_movimentacaixafechar()
          ELSE
             uf_perguntalt_chama("OCORREU UM ERRO A INSERIR O MOVIMENTO DE CAIXA. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
             RETURN .F.
          ENDIF
       ELSE
          uf_perguntalt_chama("OCORREU UM ERRO A INSERIR NA TABELA DE PAGAMENTOS CENTRAIS. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
          uf_gerais_actgrelha("", "", "ROLLBACK")
          RETURN .F.
       ENDIF
    ELSE
       uf_perguntalt_chama("OCORREU UM ERRO A INSERIR NA TABELA DE OCORR�NCIAS. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
       uf_gerais_actgrelha("", "", "ROLLBACK")
       RETURN .F.
    ENDIF
 ENDIF
 
 
 
ENDFUNC
**
FUNCTION uf_gestaocaixas_geranrmovimento
 LOCAL lcmov, lcmovimentonovo
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		select stamp from b_ultReg (nolock) where tabela = 'MovCx' and site = '<<ALLTRIM(Mysite)>>'
 ENDTEXT
 IF uf_gerais_actgrelha("", "uCrsNrMovimento", lcsql)
    IF RECCOUNT("uCrsNrMovimento")>0
       lcmov = astr(ucrsnrmovimento.stamp)
       IF lcmov='99999999'
          lcmov = '0'
       ENDIF
       lcmov = astr(VAL(lcmov)+1)
       lcmov = REPLICATE("0", 8-LEN(lcmov))+lcmov
       lcmovimentonovo = lcmov
    ELSE
       lcmovimentonovo = '00000001'
    ENDIF
    fecha("uCrsNrMovimento")
 ELSE
    uf_perguntalt_chama("OCORREU UMA ANOMALIA AO GERAR O NR DE MOVIMENTO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
    RETURN .F.
 ENDIF
 
 RETURN lcmovimentonovo
ENDFUNC
**
PROCEDURE uf_gestaocaixas_verificacashdro
 IF (VARTYPE(mycashdrostamp)<>"U")
    LOCAL msgtype, msgwait
    RELEASE lcresponsearray
    DIMENSION lcresponsearray(6)
    msgtype = "app.msg.menu.check_status_request"
    msgwait = "A validar estado CashDro..."
    uf_cashdro_operation(@lcresponsearray, msgwait, msgtype, 0, "", "0")
    LOCAL lcmessage
    IF VARTYPE("lcResponseArray(2)")<>'U'
       lcmessage = ALLTRIM(lcresponsearray(2))
    ELSE
       lcmessage = ''
    ENDIF
    IF (ALLTRIM(lcresponsearray(1))=="1")
       gestaocaixas.txtcashdro.value = "OK"
    ELSE
       gestaocaixas.txtcashdro.value = "N/D"
    ENDIF
    RELEASE lcresponsearray
 ELSE
    gestaocaixas.txtcashdro.visible = .F.
    gestaocaixas.lblcashdro.visible = .F.
 ENDIF
ENDPROC
**
PROCEDURE uf_gestaocaixas_verificarobot
 LOCAL lcvalidarobot, lcrobotstate
 myrobotstate = .F.
 IF myusaservicosrobot=.F.
    DO CASE
       CASE myusarobot .OR. myusarobotapostore
          lcrobotstate = uf_robot_validatecnilabrobotstate()
          DO CASE
             CASE lcrobotstate='00'
                gestaocaixas.txtrobo.value = 'OK'
                myrobotstate = .T.
             CASE lcrobotstate='02'
                gestaocaixas.txtrobo.value = 'Parcialmente OK'
                myrobotstate = .T.
             OTHERWISE
                gestaocaixas.txtrobo.value = 'N�o OK'
          ENDCASE
       CASE myusafarmax
          gestaocaixas.txtrobo.value = 'ACTIVO'
       CASE myusafarmax
          gestaocaixas.txtrobo.value = 'ACTIVO'
       CASE myusarobotconsis
          gestaocaixas.txtrobo.value = 'ACTIVO'
          myrobotstate = .T.
       OTHERWISE
          gestaocaixas.txtrobo.value = 'N/D'
    ENDCASE
 ELSE
    LOCAL lcresp
    lcpedstamp = uf_gerais_stamp()
    lcresp = uf_robot_servico_status(ALLTRIM(lcpedstamp))
    IF  .NOT. EMPTY(lcresp) .AND. VAL(lcresp)=200
       DECLARE Sleep IN Win32API LONG
       sleep(1000.0 )
       lcrobotstate = uf_robot_servico_status_response(ALLTRIM(lcpedstamp))
       DO CASE
          CASE lcrobotstate='00'
             gestaocaixas.txtrobo.value = 'OK'
          CASE lcrobotstate='02'
             gestaocaixas.txtrobo.value = 'Parcialmente OK'
          OTHERWISE
             gestaocaixas.txtrobo.value = 'N/D'
       ENDCASE
       gestaocaixas.timer1.interval = 1000
       gestaocaixas.timer1.reset()
    ELSE
       gestaocaixas.txtrobo.value = 'N/D'
    ENDIF
 ENDIF
ENDPROC
**
FUNCTION uf_gestaocaixas_verificacaixasabertas
 LOCAL lcsql, lccxaberta
 STORE .F. TO lccxaberta
 IF ucrse1.gestao_cx_operador=.T.
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			select top 1 ssstamp from ss (nolock) where dfechar = '19000101' and dabrir != convert(date,dateadd(HOUR, <<difhoraria>>, getdate())) and site = '<<mysite>>' AND ausername = '<<ALLTRIM(ch_vendnm)>>'
    ENDTEXT
    IF uf_gerais_actgrelha("", "uCrsCxAbertas", lcsql)
       SELECT ucrscxabertas
       IF RECCOUNT("uCrsCxAbertas")>0
          lccxaberta = .T.
       ELSE
          lccxaberta = .F.
       ENDIF
       IF USED("uCrsCxAbertas")
          fecha("uCrsCxAbertas")
       ENDIF
    ENDIF
 ELSE
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			select top 1 cxstamp from cx (nolock) where dfechar = '19000101' and dabrir != convert(date,dateadd(HOUR, <<difhoraria>>, getdate())) and site = '<<mysite>>'
    ENDTEXT
    IF uf_gerais_actgrelha("", "uCrsCxAbertas", lcsql)
       SELECT ucrscxabertas
       IF RECCOUNT("uCrsCxAbertas")>0
          lccxaberta = .T.
       ELSE
          lccxaberta = .F.
       ENDIF
       IF USED("uCrsCxAbertas")
          fecha("uCrsCxAbertas")
       ENDIF
    ENDIF
 ENDIF
 RETURN lccxaberta
ENDFUNC
**
PROCEDURE uf_gestaocaixas_pagamentoscentral
 uf_gestaocaixas_sair()
 uf_pagcentral_chama()
ENDPROC
**
PROCEDURE uf_gestaocaixas_fecharcaixa
 LPARAMETERS lcbool

 LOCAL lcvalidapw
 STORE 0 TO lcvalidapw
 lcvalidapw = uf_painelcentral_login()
 IF lcvalidapw==2
   RETURN .F.
 ENDIF

 IF TYPE("GestaoCaixas")=="U"
 	uf_gestaocaixas_chama()
 ENDIF 

 TEXT TO lcsql TEXTMERGE NOSHOW
	exec up_caixa_verificaCaixaAberta '<<ALLTRIM(mysite)>>', '<<ALLTRIM(ch_vendnm)>>', 1
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsCx", lcsql)
	uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es de Caixa", "OK", "", 16)
	RETURN .F.
 ENDIF

 IF RECCOUNT("uCrsCx") > 0
	lcbool = .T.
 ELSE
	lcbool = .F.
 ENDIF

 **IF !uf_gerais_autenticaCurUser()
 **  RETURN .F.
 **ENDIF

 IF lcbool
    uf_gestaocaixas_validaabrefechatpa(.T.)
    uf_caixas_gerircaixa(.T.)
 ELSE
    uf_gestaocaixas_validaabrefechatpa(.F.)
    uf_caixas_gerircaixa(.F.)
 ENDIF
 uf_gestaocaixas_verificacaixa()
ENDPROC
**
PROCEDURE uf_gestaocaixas_validaabrefechatpa
 LPARAMETERS lcstatus
 IF (VARTYPE(mytpastamp)<>"U")
    uf_gestaocaixas_menu_manipulaestadotpa()
    IF (UPPER(ALLTRIM(gestaocaixas.txttpa.value))=="ABERTO" .AND.  .NOT. EMPTY(lcstatus))
       uf_gestaocaixas_abrefechatpa(.F.)
    ELSE
       IF (UPPER(ALLTRIM(gestaocaixas.txttpa.value))=="FECHADO" .AND. EMPTY(lcstatus))
          uf_gestaocaixas_abrefechatpa(.T.)
       ENDIF
    ENDIF
 ENDIF
ENDPROC
**
FUNCTION uf_gestaocaixas_verificacaixa
 LOCAL lcsql
 IF USED("uCrsCx")
    fecha("uCrsCx")
 ENDIF
 IF ucrse1.gestao_cx_operador=.T.
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_caixa_verificaCaixaAberta '<<ALLTRIM(mysite)>>', '<<ALLTRIM(ch_vendnm)>>', 1
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "uCrsCx", lcsql)
       uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es de Caixa", "OK", "", 16)
       RETURN .F.
    ELSE
       IF RECCOUNT("uCrsCx")>0
            IF WEXIST("GESTAOCAIXAS")
                uv_thisForm = uf_gerais_getFormName("GESTAOCAIXAS")

                uv_thisForm.menu1.caixa.config("Fechar Cx", mypath+"\imagens\icons\fecharCaixa_w.png", "uf_gestaocaixas_FecharCaixa with .t.", "")
            ENDIF

            PUBLIC mycxuser
            mycxstamp = ucrscx.ssstamp
            mycxuser = ucrscx.ausername
            myssstamp = ucrscx.ssstamp
       ELSE
          IF TYPE("GestaoCaixas")<>"U"
             gestaocaixas.menu1.caixa.config("Abrir Cx", mypath+"\imagens\icons\abrirCaixa_w.png", "uf_gestaocaixas_FecharCaixa with .f.", "")
          ENDIF
          PUBLIC mycxuser
          mycxstamp = ""
          mycxuser = ""
          myssstamp = ""
       ENDIF
       fecha("uCrsCx")
    ENDIF
 ELSE
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_caixa_verificaCaixaAberta '<<ALLTRIM(mysite)>>', '<<ALLTRIM(myTerm)>>', 0
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "uCrsCx", lcsql)
       uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es de Caixa", "OK", "", 16)
       RETURN .F.
    ELSE
       IF RECCOUNT("uCrsCx")>0
          IF TYPE("GestaoCaixas")<>"U"
             gestaocaixas.menu1.caixa.config("Fechar Cx", mypath+"\imagens\icons\fecharCaixa_w.png", "uf_gestaocaixas_FecharCaixa with .t.", "")
          ENDIF
          PUBLIC mycxuser
          mycxstamp = ucrscx.cxstamp
          mycxuser = ucrscx.ausername
          myssstamp = ucrscx.ssstamp
       ELSE
          IF TYPE("GestaoCaixas")<>"U"
             gestaocaixas.menu1.caixa.config("Abrir Cx", mypath+"\imagens\icons\abrirCaixa_w.png", "uf_gestaocaixas_FecharCaixa with .f.", "")
          ENDIF
          PUBLIC mycxuser
          mycxstamp = ""
          mycxuser = ""
          myssstamp = ""
       ENDIF
       fecha("uCrsCx")
    ENDIF
 ENDIF
 IF TYPE("GestaoCaixas")<>"U"
    uf_gestaocaixas_pesquisar()
 ENDIF
ENDFUNC
**
FUNCTION uf_gestaocaixas_gravar
 IF EMPTY(mysite)
    uf_perguntalt_chama("A LOJA N�O EST� CONFIGURADA. POR FAVOR REINICIE O SOFTWARE. OBRIGADO.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF EMPTY(mytermno) .OR. EMPTY(myarmazem)
    uf_perguntalt_chama("O TERMINAL E ARMAZ�M DESTE POSTO N�O EST�O CONFIGURADOS. OBRIGADO.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF EMPTY(mycxstamp) .OR. EMPTY(ch_vendnm) .OR. EMPTY(myssstamp)
    uf_perguntalt_chama("POR FAVOR ABRA UMA SESS�O DE CAIXA. OBRIGADO.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF TYPE("GestaoCaixas")<>"U"
    uf_gestaocaixas_sair()
 ENDIF
 IF EMPTY(myidentificacaomarcacoes)
    uf_atendimento_chama()
    IF uf_gerais_getparameter("ADM0000000189", "BOOL")
       _SCREEN.width = _SCREEN.width-1
       _SCREEN.width = _SCREEN.width+1
    ENDIF
 ENDIF
ENDFUNC
**
FUNCTION uf_gestaocaixas_fecharcaixasporoperador
 IF USED("uCrsCxAbertas")
    fecha("uCrsCxAbertas")
 ENDIF
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_caixa_CaixasAbertas '<<mySite>>', '<<ALLTRIM(ch_vendnm)>>', 1
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsCxAbertas", lcsql)
    uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR AS CAIXAS ABERTAS NO SISTEMA.", "OK", "", 16)
    RETURN .F.
 ELSE
    IF RECCOUNT("uCrsCxAbertas")>0
       uf_caixas_gerircaixa(.T.)
    ELSE
       uf_perguntalt_chama("N�O EXISTEM CAIXAS ABERTAS PARA FECHO.", "OK", "", 16)
    ENDIF
 ENDIF
 IF USED("uCrsCxAbertas")
    fecha("uCrsCxAbertas")
 ENDIF
 uf_gestaocaixas_verificacaixasabertas()
ENDFUNC
**
PROCEDURE uf_gestaocaixas_calculototais

 uv_thisForm = uf_gerais_getFormName("GESTAOCAIXAS")

 LOCAL lcnratendtotal, lcvendastotal, lccreditostotal, lcdevtotal, lccaixastotal
 STORE 0 TO lcnratendtotal, lcvendastotal, lccreditostotal, lcdevtotal, lccaixastotal
 IF  .NOT. uf_gerais_validapermuser(ch_userno, ch_grupo, 'Consulta Caixa Operadores')==.T.
    SELECT ucrsfechodia
    GOTO TOP
    CALCULATE SUM(ucrsfechodia.nratendimento) TO lcnratendtotal 
    CALCULATE SUM(ucrsfechodia.valorvendas) TO lcvendastotal 
    CALCULATE SUM(ucrsfechodia.valorcred) TO lccreditostotal 
    CALCULATE SUM(ucrsfechodia.valorreg) TO lcdevtotal 
    CALCULATE SUM(ucrsfechodia.valorcaixa) TO lccaixastotal 
    CALCULATE SUM(ucrsfechodia.totCompart) TO lctotCompart
    uv_thisForm.pageframe1.page2.txttotalatend.value = lcnratendtotal
    uv_thisForm.pageframe1.page2.txttotalvendas.value = lcvendastotal
    uv_thisForm.pageframe1.page2.txttotalcreditos.value = lccreditostotal
    uv_thisForm.pageframe1.page2.txttotaldevolucoes.value = lcdevtotal
    uv_thisForm.pageframe1.page2.txttotalcaixa.value = lccaixastotal
    uv_thisForm.pageframe1.page2.txttotCompart.value = lctotCompart
 ELSE
    SELECT ucrsfechodia
    GOTO TOP
    CALCULATE SUM(ucrsfechodia.nratendimento) TO lcnratendtotal FOR ucrsfechodia.auserno=ch_vendedor
    CALCULATE SUM(ucrsfechodia.valorvendas) TO lcvendastotal FOR ucrsfechodia.auserno=ch_vendedor
    CALCULATE SUM(ucrsfechodia.valorcred) TO lccreditostotal FOR ucrsfechodia.auserno=ch_vendedor
    CALCULATE SUM(ucrsfechodia.valorreg) TO lcdevtotal FOR ucrsfechodia.auserno=ch_vendedor
    CALCULATE SUM(ucrsfechodia.valorcaixa) TO lccaixastotal FOR ucrsfechodia.auserno=ch_vendedor
	CALCULATE SUM(ucrsfechodia.totCompart) TO lctotCompart FOR ucrsfechodia.auserno=ch_vendedor    
    uv_thisForm.pageframe1.page2.txttotalatend.value = lcnratendtotal
    uv_thisForm.pageframe1.page2.txttotalvendas.value = lcvendastotal
    uv_thisForm.pageframe1.page2.txttotalcreditos.value = lccreditostotal
    uv_thisForm.pageframe1.page2.txttotaldevolucoes.value = lcdevtotal
    uv_thisForm.pageframe1.page2.txttotalcaixa.value = lccaixastotal
    uv_thisForm.pageframe1.page2.txttotCompart.value = lctotCompart 
 ENDIF
 uf_gestaocaixas_config()
 uv_thisForm.refresh
ENDPROC
**
PROCEDURE uf_gestaocaixas_relatoriocaixa
 LOCAL lcsql, lcopini, lcopfim, lcdataini, lcdatafim, lccollapseorexpand
 STORE "" TO lcsql
 STORE 0 TO lcopini, lcopfim, lccollapseorexpand
 IF ucrse1.gestao_cx_operador=.T.
    IF  .NOT. uf_gerais_validapermuser(ch_userno, ch_grupo, 'Consulta Caixa Operadores')==.T.
       lcopini = 0
       lcopfim = 99
       lcdataini = gestaocaixas.txtdata.value
       lcdatafim = gestaocaixas.txtdata.value
       lccollapseorexpand = 0
    ELSE
       lcopini = ch_vendedor
       lcopfim = ch_vendedor
       lcdataini = gestaocaixas.txtdata.value
       lcdatafim = gestaocaixas.txtdata.value
       lccollapseorexpand = 0
    ENDIF
    lcsql = ""
    TEXT TO lcsql TEXTMERGE NOSHOW
			&dataIni=<<lcDataIni>>&dataFim=<<lcDataFim>>&opIni=<<lcOpIni>>&opFim=<<lcOpFim>>&CollapseorExpand=<<IIF(lcCollapseOrExpand==1,"true","false")>>&site=<<mysite>>
    ENDTEXT
    uf_gerais_chamareport("relatorio_gestao_caixa_operador", lcsql)
 ELSE
    lcdataini = gestaocaixas.txtdata.value
    lcdatafim = gestaocaixas.txtdata.value
    lccollapseorexpand = 0
    lcsql = ""
    TEXT TO lcsql TEXTMERGE NOSHOW
			&dataIni=<<lcDataIni>>&dataFim=<<lcDataFim>>&CollapseorExpand=<<IIF(lcCollapseOrExpand==1,"true","false")>>&site=<<mysite>>
    ENDTEXT
    uf_gerais_chamareport("relatorio_gestao_caixa_terminal_TPA", lcsql)
 ENDIF
ENDPROC
**
FUNCTION uf_gestaocaixas_reimptalaocaixa
 LOCAL lcdabrir
 lcdabrir = gestaocaixas.txtdata.value
 IF ucrse1.gestao_cx_operador=.T.
    lcsql = ""
    TEXT TO lcsql TEXTMERGE NOSHOW
    	 select ssstamp from (select distinct ssstamp , ROW_NUMBER() OVER( PARTITION BY cxstamp order by usrdata desc) as rowNumber  FROM ss WHERE dabrir = '<<uf_gerais_getdate(lcDabrir,"SQL")>>' AND auserno = <<ch_vendedor>> ) as x  where rowNumber=1
	ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "uCrsReimpAux", lcsql)
       uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es de Caixa", "OK", "", 16)
       RETURN .F.
    ELSE
       SELECT ucrsreimpaux
       GOTO TOP
       SCAN
          uf_caixas_talao(ucrsreimpaux.ssstamp)
       ENDSCAN
    ENDIF
 ELSE
    lcsql = ""
    TEXT TO lcsql TEXTMERGE NOSHOW
		select cxstamp FROM cx WHERE dabrir = '<<uf_gerais_getdate(lcDabrir,"SQL")>>' AND pno = <<myTermNo>> AND auserno = <<ch_vendedor>> order by usrdata desc
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "uCrsReimpAux", lcsql)
       uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es de Caixa", "OK", "", 16)
       RETURN .F.
    ELSE
       SELECT ucrsreimpaux
       GOTO TOP
       SCAN
          uf_caixas_talao(ucrsreimpaux.cxstamp)
       ENDSCAN
    ENDIF
 ENDIF
 IF USED("uCrsReimpAux")
    fecha("uCrsReimpAux")
 ENDIF
ENDFUNC
**
PROCEDURE uf_gestaocaixas_config
 uv_thisForm = uf_gerais_getFormName("GESTAOCAIXAS")
 IF ucrse1.gestao_cx_operador=.T.
    IF  .NOT. uf_gerais_validapermuser(ch_userno, ch_grupo, 'Consulta Detalhe Caixa Operadores')==.F.
       WITH uv_thisForm.pageframe1.page2
          .grdpesqop.column9.visible = .F.
          .grdpesqop.column10.visible = .F.
          .grdpesqop.column11.visible = .F.
          .grdpesqop.column12.visible = .F.
          .grdpesqop.column13.visible = .F.
		  .grdpesqop.column2.visible = .F.
          .txttotalatend.value = 0
          .txttotalvendas.value = 0
          .txttotalcreditos.value = 0
          .txttotaldevolucoes.value = 0
          .txttotalcaixa.value = 0
          .txttotCompart.value = 0
       ENDWITH
    ENDIF
 ENDIF
ENDPROC
**
FUNCTION uf_gestaocaixas_alterarcx
 IF !ucrse1.gestao_cx_operador
   uf_perguntalt_chama("A altera��o de caixas n�o � permitida quando o software est� configurado para gest�o de caixas por terminal.", "OK", "", 16)
   RETURN .F.
 ENDIF
 SELECT ucrsfechodia
 IF ucrsfechodia.fechada=.F.
    uf_perguntalt_chama("N�o pode alterar uma caixa ainda em aberto!", "OK", "", 16)
 ELSE
    LOCAL lcssstamp
    lcssstamp = ALLTRIM(ucrsfechodia.ssstamp)
    myssstamp = ALLTRIM(ucrsfechodia.ssstamp)
    TEXT TO lcsql TEXTMERGE NOSHOW
				select * FROM ss (nolock) WHERE ssstamp='<<lcSsStamp>>'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "uCrsTmpSS", lcsql)
       uf_perguntalt_chama("N�o foi poss�vel ir buscar a sess�o de Caixa", "OK", "", 16)
       RETURN .F.
    ELSE
       TEXT TO lcsql TEXTMERGE NOSHOW
				select id, contagem, ssstamp from contnumerario(nolock) where tipo = 'MOEDAS' and ssstamp='<<lcSsStamp>>' order by id
       ENDTEXT
       uf_gerais_actgrelha("", "ucrsContMoedas", lcsql)
       TEXT TO lcsql TEXTMERGE NOSHOW
				select id, contagem, ssstamp from contnumerario(nolock) where tipo = 'NOTAS' and ssstamp='<<lcSsStamp>>' order by id
       ENDTEXT
       uf_gerais_actgrelha("", "ucrsContNotas", lcsql)
       
       SELECT ucrsContMoedas
       If Reccount("ucrsContMoedas")<2
			lcsql = ''
			 TEXT TO lcsql TEXTMERGE NOSHOW
				select cast(campo as numeric(9,2)) as ID, 0 as contagem, '<<lcSsStamp>>' as ssstamp from b_multidata where tipo = 'NUMERARIO-MOEDAS' and upper(pais) = '<<UPPER(ALLTRIM(myPaisConfSoftw))>>' order by id
			 ENDTEXT
			 IF  .NOT. uf_gerais_actgrelha("", "ucrsContMoedas", lcsql)
			    uf_perguntalt_chama("N�o foi poss�vel Construir o cursor para contagem de numer�rio - Moedas. Por favor contacte o suporte.", "OK", "", 32)
			    RETURN .F.
			 ENDIF
			 lcsql = ''
			 TEXT TO lcsql TEXTMERGE NOSHOW
				select cast(campo as numeric(9,0)) as ID, 0 as contagem, '<<lcSsStamp>>' as ssstamp from b_multidata where tipo = 'NUMERARIO-NOTAS' and upper(pais) = '<<UPPER(ALLTRIM(myPaisConfSoftw))>>' order by id
			 ENDTEXT
			 IF  .NOT. uf_gerais_actgrelha("", "ucrsContNotas", lcsql)
			    uf_perguntalt_chama("N�o foi poss�vel Construir o cursor para contagem de numer�rio - Notas. Por favor contacte o suporte.", "OK", "", 32)
			    RETURN .F.
			 ENDIF
       ENDIF 
       
       myaltercaixa = .T.
       uf_gestaosessaocaixa_chama()
    ENDIF
 ENDIF
ENDFUNC
**
PROCEDURE uf_gestaocaixas_setmotivoid
 LPARAMETERS lctypemotive
 PUBLIC mytabmotiveid
 LOCAL lctabmotivedescr
 mytabmotiveid = ""
 IF USED("uCrsTempMotives")
    fecha("uCrsTempMotives")
 ENDIF
 CREATE CURSOR uCrsTempMotives (id VARCHAR(254), descr VARCHAR(254))
 IF uf_gerais_actgrelha("", "uCrsTempMotives", [select id,descr from MotivesCashMovements(nolock) WHERE TYPE=']+ALLTRIM(lctypemotive)+[' ORDER BY id ASC])
    uf_valorescombo_chama("myTabMotiveID ", 0, "uCrsTempMotives", 2, "id", "id, descr ", .F., .F., .T., .F.)
 ENDIF
 IF (EMPTY(mytabmotiveid))
    mytabmotiveid = "0"
 ENDIF
 SELECT ucrstempmotives
 GOTO TOP
 LOCATE FOR id==mytabmotiveid
 IF FOUND()
    lctabmotivedescr = ALLTRIM(descr)
 ELSE
    lctabmotivedescr = ""
 ENDIF
 IF USED("uCrsTempMotives")
    fecha("uCrsTempMotives")
 ENDIF
 gestaocaixas.ctnmovimentocaixa.txtmotivoid.value = lctabmotivedescr
 RELEASE mytabmotiveid
ENDPROC
**
