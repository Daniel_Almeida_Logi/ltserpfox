**
FUNCTION uf_valorestpa_chama
 LPARAMETERS lcdata, lcloja
 IF EMPTY(ALLTRIM(lcdata)) .OR.  .NOT. TYPE("lcData")="C"
    uf_perguntalt_chama("O PAR�METRO: DATA, N�O � V�LIDO.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF EMPTY(ALLTRIM(lcloja)) .OR.  .NOT. TYPE("lcLoja")="C"
    uf_perguntalt_chama("O PAR�METRO: LOJA, N�O � V�LIDO.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF  .NOT. USED("uCrsFechodia")
    RETURN .F.
 ELSE
    LOCAL lcvalor
    lcvalor = 0.00 
    SELECT DISTINCT 'TPA'+astr(pno) AS tpa, lcvalor AS valor, pno, 0.00  AS comissaotpa FROM uCrsFechodia INTO CURSOR uCrsTPAs READWRITE
 ENDIF
 IF TYPE("ValoresTPA")=="U"
    DO FORM ValoresTPA
 ELSE
    valorestpa.show
 ENDIF
 valorestpa.txtdata.value = lcdata
 valorestpa.txtloja.value = lcloja
 valorestpa.menu1.estado("", "", "Gravar", .T.)
ENDFUNC
**
FUNCTION uf_valorestpa_gravar
 SET POINT TO "."
 LOCAL lcsql
 STORE '' TO lcsql
 IF RECCOUNT("uCrsTPAs")==0
    uf_perguntalt_chama("N�O EXISTEM REGISTOS PARA ALTERAR.", "OK", "", 64)
    RETURN .F.
 ENDIF
 IF uf_gerais_actgrelha("", "uCrsVerifyRegistoTpa", [Select pno From cx (nolock) where dabrir = ']+uf_gerais_getdate(valorestpa.txtdata.value, "SQL")+[' and valorTpa != 0])
    IF RECCOUNT()>0 .AND.  .NOT. uf_perguntalt_chama("J� EXISTEM VALORES REGISTADOS PARA ESTE DIA. PRETENDE SUBSTITUIR OS VALORES EXISTENTES PELOS VALORES ACTUAIS?", "Sim", "N�o")
       fecha("uCrsVerifyRegistoTpa")
       RETURN .F.
    ELSE
       SELECT ucrstpas
       GOTO TOP
       SCAN
          lcsql = ''
          TEXT TO lcsql TEXTMERGE NOSHOW
					Update cx
					Set 
						valorTpa = <<uCrsTPAs.valor>>
						,comissaoTPA = <<uCrsTPAs.comissaoTPA>>
					where dabrir = '<<uf_gerais_getDate(ValoresTPA.txtData.value,"SQL")>>' and pno=<<uCrsTPAs.pno>>
          ENDTEXT
          IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
             uf_perguntalt_chama("CORREU UM ERRO A REGISTAR VALORES DOS TPAs.", "OK", "", 16)
             RETURN .F.
          ENDIF
       ENDSCAN
       uf_perguntalt_chama("VALORES REGISTADOS COM SUCESSO.", "OK", "", 64)
    ENDIF
 ELSE
    uf_perguntalt_chama("OCORREU UM ERRO A VALIDAR OS VALORES TPA.", "OK", "", 16)
    RETURN .F.
 ENDIF
 fecha("uCrsVerifyRegistoTpa")
ENDFUNC
**
PROCEDURE uf_valorestpa_sair
 valorestpa.release
 RELEASE valorestpa
 IF USED("uCrsTPAs")
    fecha("uCrsTPAs")
 ENDIF
ENDPROC
**
