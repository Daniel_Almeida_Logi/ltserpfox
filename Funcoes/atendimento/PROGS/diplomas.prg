**
FUNCTION uf_diplomas_chama
 LPARAMETERS ltodaslinhas
 PUBLIC mydiplomatodaslinhas
 STORE .F. TO mydiplomatodaslinhas
 
 
 IF (USED("fi"))
    SELECT fi
    uf_atendimento_devolvecursorcabecalhofi(fi.lordem)
    LOCAL lctiporeceita
    lctiporeceita = ""
    lctiporeceita = ALLTRIM(ucrstempficabecalho.tipor)

    IF (USED("ucrsTempFiCabecalho"))
       fecha("ucrsTempFiCabecalho")
    ENDIF
    
    &&backoffice
    IF TYPE("ATENDIMENTO")=="U" AND !EMPTY(lctiporeceita)
    	RETURN .F.
    ENDIF
    
    
    &&atendimento   
    IF (TYPE("ATENDIMENTO")!='U' AND lctiporeceita<>'RM')
       RETURN .F.
    ENDIF
    
 ENDIF
 IF  .NOT. mydiplomaauto
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			SELECT 
				sel=CAST(0 as Bit)
				,u_design
				,dplmsstamp
			from 
				dplms (nolock)
			where 
				inactivo=0
				and u_design != ''
			ORDER BY 
				u_coluna
    ENDTEXT
    uf_gerais_actgrelha("", "uCrsDip", lcsql)
    IF TYPE("ATENDIMENTO")<>"U"
       lccod = uf_atendimento_verificacompart()
       SELECT fi
       lcref = fi.ref
    ELSE
       lccod = ft2.u_codigo
       lcref = fi.ref
    ENDIF
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_receituario_dadosDoPlano '<<Alltrim(lcCod)>>'
    ENDTEXT
    uf_gerais_actgrelha("", "uCrsCodPla", lcsql)
    IF RECCOUNT('uCrsCodPla')=0
       RETURN .F.
    ENDIF
    SELECT ucrsdip
    SELECT ucrscodpla
    IF ucrscodpla.dplmsstamp="01"
       SELECT ucrsdip
       SET FILTER TO ucrsdip.dplmsstamp="01"
       GOTO TOP
    ENDIF
    IF ucrscodpla.dplmsstamp="02"
       SELECT ucrsdip
       GOTO TOP
    ENDIF
    IF ucrscodpla.dplmsstamp<>"01" .AND. ucrscodpla.dplmsstamp<>"02"
       SELECT ucrsdip
       SET FILTER TO ucrsdip.dplmsstamp<>"01" .AND. ucrsdip.dplmsstamp<>"02"
       GOTO TOP
    ENDIF
 ENDIF
 IF TYPE("DIPLOMAS")=="U"
    DO FORM DIPLOMAS WITH ltodaslinhas
 ELSE
    diplomas.show()
 ENDIF
ENDFUNC
**
PROCEDURE uf_diplomas_todaslinhas
 LPARAMETERS lcbool
 IF lcbool
    mydiplomatodaslinhas = .T.
    diplomas.menu1.todaslinhas.config("Aplic. Todas", mypath+"\imagens\icons\checked_w.png", "uf_diplomas_todasLinhas with .f.", "F1")
 ELSE
    mydiplomatodaslinhas = .F.
    diplomas.menu1.todaslinhas.config("Aplic. Todas", mypath+"\imagens\icons\unchecked_w.png", "uf_diplomas_todasLinhas with .t.", "F1")
 ENDIF
ENDPROC
**
PROCEDURE uf_diplomas_aplicadiploma
 LOCAL lnsel, lcposcab, lcposact, lcpos, lcposfi, lcfistamp, lcaxudip, lcauxlordem
 STORE 0 TO lnsel, lcposcab, lcposact, lcpos, lcposfi
 STORE '' TO lcfistamp, lcaxudip, lcauxlordem
 SELECT fi
 lcposfi = RECNO("FI")
 IF USED("uCrsDip")
    diplomas.dci.setfocus
    SELECT ucrsdip
    GOTO TOP
    SCAN
       IF ucrsdip.sel==.T.
          lcaxudip = ALLTRIM(ucrsdip.u_design)
       ENDIF
       SELECT ucrsdip
    ENDSCAN
    IF mydiplomatodaslinhas==.F.
       SELECT fi
       IF fi.u_comp==.T.
          lcauxlordem = astr(fi.lordem)
          SELECT fi
          TRY
             GOTO lcposfi
          CATCH
             GOTO TOP
          ENDTRY
          SELECT fi
          REPLACE fi.u_diploma WITH lcaxudip IN "FI"
          IF TYPE("ATENDIMENTO")<>"U"
             IF  .NOT. mydiplomaauto
                uf_atendimento_fiiliq()
                TRY
                   GOTO lcposfi
                CATCH
                ENDTRY
                uf_atendimento_eventofi(.T.)
                TRY
                   GOTO lcposfi
                CATCH
                ENDTRY
                uf_atendimento_infototais()
                TRY
                   GOTO lcposfi
                CATCH
                ENDTRY
             ENDIF
          ELSE
             IF  .NOT. mydiplomaauto
                uf_atendimento_actvaloresfi()
                TRY
                   GOTO lcposfi
                CATCH
                ENDTRY
             ENDIF
          ENDIF
       ENDIF
    ELSE
       IF TYPE("ATENDIMENTO")<>"U"
          SELECT fi
          lcposact = RECNO()
          lcposcab = uf_diplomas_getposcab()
          GOTO lcposcab
          SKIP
          DO WHILE fi.lordem<>0
             IF LEFT(fi.design, 1)="."
                EXIT
             ENDIF
             lcpos = RECNO("fi")
             IF  .NOT. (ALLTRIM(fi.ref)=='R000001')
                SELECT fi
                REPLACE fi.u_diploma WITH lcaxudip IN "FI"
             ENDIF
             IF  .NOT. mydiplomaauto
                uf_atendimento_fiiliq()
                uf_atendimento_eventofi()
                uf_atendimento_infototais()
             ENDIF
             GOTO lcpos
             SELECT fi
             TRY
                SKIP
             CATCH
             ENDTRY
          ENDDO
          GOTO lcposact
       ELSE
          SELECT fi
          GOTO TOP
          SCAN
             IF fi.u_comp=.T.
                REPLACE fi.u_diploma WITH lcaxudip
                IF  .NOT. mydiplomaauto
                   uf_atendimento_actvaloresfi()
                   TRY
                      GOTO lcposfi
                   CATCH
                   ENDTRY
                ENDIF
             ENDIF
          ENDSCAN
          IF  .NOT. mydiplomaauto
             uf_atendimento_acttotaisft()
             TRY
                GOTO lcposfi
             CATCH
             ENDTRY
          ENDIF
       ENDIF
    ENDIF
 ENDIF
 SELECT fi
 TRY
    GOTO lcposfi
 CATCH
 ENDTRY
 IF TYPE("ATENDIMENTO")<>"U"
    atendimento.grdatend.setfocus
 ENDIF
 uf_diplomas_sair()
ENDPROC
**
FUNCTION uf_diplomas_getposcab
 LOCAL lcposcab
 lcposcab = 0
 SELECT fi
 DO WHILE  .NOT. BOF()
    IF LEFT(fi.design, 1)="."
       lcposcab = RECNO()
       EXIT
    ENDIF
    SKIP -1
 ENDDO
 RETURN lcposcab
ENDFUNC
**
PROCEDURE uf_diplomas_sair
 IF USED("uCrsDip")
    fecha("uCrsDip")
 ENDIF
 IF USED("uCrsCodPla")
    fecha("uCrsCodPla")
 ENDIF
 diplomas.hide
 diplomas.release
ENDPROC
**
