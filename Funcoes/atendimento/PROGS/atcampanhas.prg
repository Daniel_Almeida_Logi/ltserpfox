**
FUNCTION uf_atcampanhas_chama
 LPARAMETERS lccampanhasnaoaplicadas
 LOCAL lcsql
 STORE '' TO lcsql
 TEXT TO lcsql TEXTMERGE NOSHOW
		Select 
			sel = CONVERT(bit,0)
			,aplicada = CONVERT(bit,0)
			,aplicavel = CONVERT(bit,0)
			,apagar = CONVERT(bit,0)
			,* 
		from 
			campanhas (nolock)
		where 
			convert(date,getdate()) between dataInicio and dataFim
			and inativo = 0
			and eliminada = 0
			and site='<<ALLTRIM(mysite)>>'
 ENDTEXT
 IF TYPE("ATCAMPANHAS")=="U"
    IF  .NOT. uf_gerais_actgrelha("", "uCrsPesqCampanhasAt", lcsql)
       uf_perguntalt_chama("Ocorreu uma anomalia a carregar as campanhas. Contacte o suporte.", "OK", "", 16)
       RETURN .F.
    ENDIF
    DO FORM ATCAMPANHAS WITH lccampanhasnaoaplicadas
 ELSE
    IF  .NOT. uf_gerais_actgrelha("ATCAMPANHAS.gridPesq", "ucrsPesqCampanhasAt", lcsql)
       uf_perguntalt_chama("Ocorreu uma anomalia a carregar as campanhas. Contacte o suporte.", "OK", "", 16)
       RETURN .F.
    ENDIF
    atcampanhas.show
 ENDIF
 uf_atcampanhas_campanhasdoutente()
 uf_atcampanhas_excluicampanhassopontos()
 uf_atcampanhas_actualizaprodutosnovos()
 uf_atcampanhas_marcaaplicadas()
 uf_atcampanhas_marcaaplicaveis()
 uf_atcampanhas_ordernacursor()
 SELECT ucrspesqcampanhasat
 GOTO TOP
 atcampanhas.gridpesq.refresh
 atcampanhas.gridpesq.setfocus
ENDFUNC
**
PROCEDURE uf_atcampanhas_campanhasdoutente
 LOCAL lcno, lcestab, lcencontroucampanha, lccampanhas, lcrows
 lcno = ft.no
 lcestab = ft.estab
 uf_atendimento_carregacampanhasut()
 SELECT ucrspesqcampanhasat
 GOTO TOP
 SCAN
    lccamplin = astr(ucrspesqcampanhasat.id)
    SELECT ucrscampanhaslist
    LOCATE FOR astr(ucrscampanhaslist.id)=lccamplin
    IF FOUND()
       SELECT ucrspesqcampanhasat
       lcid = '('+astr(ucrspesqcampanhasat.id)+')'
       SELECT ucrscampanhaslistaut
       GOTO TOP
       LOCATE FOR ucrscampanhaslistaut.no=lcno .AND. ucrscampanhaslistaut.estab=lcestab
       IF FOUND()
          lccampanhas = ucrscampanhaslistaut.campanhas
          lcencontroucampanha = .F.
          lcrows = ALINES(ucrscamptemp, lccampanhas, ")")
          FOR i = 1 TO lcrows
             IF ucrscamptemp(i)+")"==lcid
                lcencontroucampanha = .T.
                EXIT
             ENDIF
          ENDFOR
          IF lcencontroucampanha==.F.
             SELECT ucrspesqcampanhasat
             REPLACE ucrspesqcampanhasat.apagar WITH .T.
          ENDIF
       ENDIF
    ENDIF
    SELECT ucrspesqcampanhasat
 ENDSCAN
 DELETE FROM ucrsPesqCampanhasAt WHERE  .NOT. EMPTY(ucrspesqcampanhasat.apagar)
 SELECT ucrspesqcampanhasat
 GOTO TOP
ENDPROC
**
FUNCTION uf_atcampanhas_excluicampanhassopontos
 LOCAL lcsql
 STORE '' TO lcsql
 TEXT TO lcsql TEXTMERGE NOSHOW
		Select distinct campanhas_id from campanhas_pOfertas
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "ucrsCampanhasDefProdOferta", lcsql)
    uf_perguntalt_chama("Ocorreu uma anomalia a verificar produtos associados �s campanhas.", "OK", "", 16)
    RETURN .F.
 ENDIF
 SELECT ucrspesqcampanhasat
 GOTO TOP
 SCAN FOR ucrspesqcampanhasat.descv==0 .AND. ucrspesqcampanhasat.desconto==0 .AND. valorvaledireto==0
    SELECT ucrscampanhasdefprodoferta
    LOCATE FOR ucrscampanhasdefprodoferta.campanhas_id==ucrspesqcampanhasat.id
    IF  .NOT. FOUND()
       SELECT ucrspesqcampanhasat
       REPLACE ucrspesqcampanhasat.apagar WITH .T.
    ENDIF
 ENDSCAN
 DELETE FROM ucrsPesqCampanhasAt WHERE  .NOT. EMPTY(ucrspesqcampanhasat.apagar)
 SELECT ucrspesqcampanhasat
 GOTO TOP
ENDFUNC
**
PROCEDURE uf_atcampanhas_carregamenu
 LPARAMETERS lccampanhasnaoaplicadas
 WITH atcampanhas.menu1
    .adicionaopcao("detalhe", "Detalhe", mypath+"\imagens\icons\detalhe_micro.png", "uf_ATCAMPANHAS_detalhe", "A")
    .adicionaopcao("removerCampanhas", "Remover", mypath+"\imagens\icons\cruz_w.png", "uf_ATCAMPANHAS_removerCampanhas", "R")
 ENDWITH
ENDPROC
**
PROCEDURE uf_atcampanhas_filtra
 IF  .NOT. EMPTY(ALLTRIM(atcampanhas.nome.value))
    lcfiltro = [LIKE('*'+UPPER(ALLTRIM(ATCAMPANHAS.nome.value))+'*',UPPER(uCrsPesqCampanhasAt.descricao))]
    SELECT ucrspesqcampanhasat
    SET FILTER TO &lcfiltro 
 ELSE
    SELECT ucrspesqcampanhasat
    SET FILTER TO
 ENDIF
 atcampanhas.gridpesq.refresh
ENDPROC
**
PROCEDURE uf_atcampanhas_detalhe
 SELECT ucrspesqcampanhasat
 uf_campanhas_chama(ucrspesqcampanhasat.id)
 uf_atcampanhas_sair()
ENDPROC
**
FUNCTION uf_atcampanhas_escolhe
 LOCAL lccriterioprodutos, lcauxlordem, lcauxpos, lcaplicoudesconto, lcordemvenda
 STORE .F. TO lcaplicoudesconto
 TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_campanhas_configST '<<ALLTRIM(mysite)>>'
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "ucrsCampanhasConfigSt", lcsql)
    uf_perguntalt_chama("Ocorreu uma anomalia ao verificar as definic�es da Campanha. Por favor contacte o Suporte.", "OK", "", 16)
    RETURN .F.
 ENDIF
 atendimento.campanhas = .F.
 SELECT ucrspesqcampanhasat
 IF EMPTY(ucrspesqcampanhasat.aplicavel)
    uf_perguntalt_chama("N�o � possivel aplicar a campanha selecionada.", "OK", "", 32)
    SELECT ucrspesqcampanhasat
    REPLACE ucrspesqcampanhasat.sel WITH .F.
    RETURN .F.
 ENDIF
 IF  .NOT. EMPTY(ucrspesqcampanhasat.aplicada)
    uf_perguntalt_chama("Campanha j� aplicada ao atendimento. Elimine as linhas associadas � campanha.", "OK", "", 32)
    SELECT ucrspesqcampanhasat
    REPLACE ucrspesqcampanhasat.sel WITH .F.
    RETURN .F.
 ENDIF
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		Select ISNULL(MIN(qt),0) as qt from campanhas_st (nolock) where campanhas_id = <<uCrsPesqCampanhasAt.id>>
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "ucrsCampanhasQtAplicavel", lcsql)
    uf_perguntalt_chama("Ocorreu uma anomalia ao verificar produtos associados � campanha. Por favor contacte o Suporte.", "OK", "", 16)
    RETURN .F.
 ENDIF
 IF ucrscampanhasqtaplicavel.qt>0
    SELECT ucrscampanhasqtaplicavel
    lcqttaplicavel = ucrscampanhasqtaplicavel.qt
 ELSE
    lcqttaplicavel = 1
 ENDIF
 IF USED("ucrsCampanhasQtAplicavel")
    fecha("ucrsCampanhasQtAplicavel")
 ENDIF
 uf_atcampanhas_verificalinhascampanha(ucrspesqcampanhasat.id, ucrscampanhaslist.vallimite)
 SELECT ucrsfipontos
 CALCULATE SUM(ucrsfipontos.qtt) TO lcqtprodvendas 
 IF lcqtprodvendas>=lcqttaplicavel
    SELECT ucrsfipontos
    GOTO TOP
    SCAN
       SELECT fi
       GOTO TOP
       LOCATE FOR ALLTRIM(ucrsfipontos.fistamp)==ALLTRIM(fi.fistamp)
       IF FOUND()
          SELECT ucrspesqcampanhasat
          IF ucrspesqcampanhasat.desconto>0
             IF ucrspesqcampanhasat.maisbarato==.F.
                IF ucrspesqcampanhasat.acumula=.T.
                   uf_atendimento_alteradesc(.T., fi.desconto+ucrspesqcampanhasat.desconto)
                ELSE
                   uf_atendimento_alteradesc(.T., ucrspesqcampanhasat.desconto)
                ENDIF
                uf_atcampanhas_resgistacampanhafi(ucrspesqcampanhasat.id, ucrspesqcampanhasat.descricao)
             ELSE
 
                SELECT fi
                lcauxpos = RECNO("fi")
                lcauxlordem = LEFT(astr(fi.lordem), 2)
                SELECT TOP 1 fi.fistamp FROM fi INNER JOIN uCrsFiPontos ON ALLTRIM(fi.ref)=ALLTRIM(ucrsfipontos.ref) WHERE LEFT(astr(fi.lordem), 2)==lcauxlordem AND fi.u_epvp>0 ORDER BY fi.u_epvp INTO CURSOR uCrsAuxFiMaisBarato READWRITE
                SELECT fi
                LOCATE FOR ALLTRIM(fi.fistamp)==ALLTRIM(ucrsauxfimaisbarato.fistamp)
                IF FOUND()
                   IF lcaplicoudesconto==.F. .OR. lcordemvenda<>lcauxlordem
                    
                      LOCAL lcDesconto, lcQtt
                      lcDesconto=0
                      lcQtt = 1

                      
                      SELECT fi
                      lcQtt = fi.qtt
                      
                      if(lcQtt==0)
                      	lcQtt = 1
                      ENDIF
                      
                      
                                            
                      if(USED("ucrspesqcampanhasat"))
                      	SELECT ucrspesqcampanhasat
                      	lcDesconto = ROUND(ucrspesqcampanhasat.desconto/lcQtt,2)
                      ENDIF
					  
					  SELECT fi	
                      IF ucrspesqcampanhasat.acumula=.T.
                         uf_atendimento_alteradesc(.T., fi.desconto+lcDesconto)
                      ELSE
                         uf_atendimento_alteradesc(.T., lcDesconto)
                      ENDIF
                      **uf_atcampanhas_resgistacampanhafi(ucrspesqcampanhasat.id, ucrspesqcampanhasat.descricao)
					  UPDATE fi SET fi.campanhas = ALLTRIM(fi.campanhas)+IIF( .NOT. EMPTY(ALLTRIM(fi.campanhas)), ",", "")+astr(ucrspesqcampanhasat.id)+"-("+STRTRAN(ALLTRIM(ucrspesqcampanhasat.descricao), ',', '')+")" FROM fi INNER JOIN uCrsFiPontos ON ALLTRIM(fi.ref)=ALLTRIM(ucrsfipontos.ref) WHERE LEFT(astr(fi.lordem), 2)==lcauxlordem AND !EMPTY(fi.ref)
                      lcaplicoudesconto = .T.
                      lcordemvenda = lcauxlordem
                   ENDIF
                ENDIF
                SELECT fi
                GOTO lcauxpos
                IF USED("uCrsAuxFiMaisBarato ")
                   fecha("uCrsAuxFiMaisBarato ")
                ENDIF
             ENDIF
          ENDIF
          IF ucrspesqcampanhasat.descv>0
             SELECT fi
             lcnovovalordesc = fi.u_descval+ucrspesqcampanhasat.descv
             uf_atendimento_alteradescval(.T., 0)
             uf_atendimento_alteradescval(.T., lcnovovalordesc)
             uf_atcampanhas_resgistacampanhafi(ucrspesqcampanhasat.id, ucrspesqcampanhasat.descricao)
          ENDIF
       ENDIF
       SELECT ucrsfipontos
    ENDSCAN
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			Select ref from campanhas_st_lista where campanhas like '%(' + '<<uCrsPesqCampanhasAt.id>>' + ')%' 
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "uCrsCampanhasProdutosVenda", lcsql)
       uf_perguntalt_chama("Ocorreu uma anomalia a verificar produtos dispon�veis para oferta.", "OK", "", 16)
       RETURN .F.
    ENDIF
    SELECT ucrspesqcampanhasat
    SELECT ucrscampanhasconfigst
    GOTO TOP
    SCAN FOR ucrspesqcampanhasat.id==ucrscampanhasconfigst.campanhas_id .AND. ucrscampanhasconfigst.excecao==.F.
       SELECT fi.* FROM fi INNER JOIN ucrsCampanhasProdutosVenda ON ALLTRIM(fi.ref)=ALLTRIM(ucrscampanhasprodutosvenda.ref) WHERE  NOT EMPTY(ALLTRIM(fi.ref)) INTO CURSOR ucrsProdVendasCamp READWRITE
       SELECT ucrsprodvendascamp
       CALCULATE SUM(ucrsprodvendascamp.qtt) TO lcqtprodvendas 
       IF lcqtprodvendas>=ucrscampanhasconfigst.qt
          uf_atcampanhas_produtosoferta(ucrspesqcampanhasat.id, ucrspesqcampanhasat.descricao, ucrsprodvendascamp.ref, lcqtprodvendas/ucrscampanhasconfigst.qt, ucrscampanhasconfigst.qt)
       ENDIF
    ENDSCAN
    SELECT ucrsfipontos
    GOTO TOP
    SELECT ucrspesqcampanhasat
    REPLACE ucrspesqcampanhasat.sel WITH .F.
    REPLACE ucrspesqcampanhasat.aplicada WITH .T.
    atendimento.campanhas = .T.
 ENDIF
 usacampanha = 1
 atcampanhas.gridpesq.setfocus
 atcampanhas.gridpesq.refresh
 IF USED("uCrsCampanhasProdParaOferta")
    fecha("uCrsCampanhasProdParaOferta")
 ENDIF
ENDFUNC
**
PROCEDURE uf_atcampanhas_marcaaplicadas
 LOCAL lcid
 SELECT ucrspesqcampanhasat
 lcid = ucrspesqcampanhasat.id
 IF USED("ucrsCampanhasAplicadas")
    fecha("ucrsCampanhasAplicadas")
 ENDIF
 CREATE CURSOR ucrsCampanhasAplicadas (campanha C(60))
 SELECT campanhas FROM fi WHERE LEFT(ALLTRIM(fi.design), 1)<>"." INTO CURSOR ucrsFiAuxCampAplic READWRITE
 SELECT ucrsfiauxcampaplic
 GOTO TOP
 SCAN
    lclistacampanhas = fi.campanhas
    ALINES(ucrscampanhas, lclistacampanhas, .F., ",")
    lccampanharegistada = .F.
    FOR i = 1 TO ALEN(ucrscampanhas)
       lccampanha = ALLTRIM(GETWORDNUM(ucrscampanhas(i), 1, "-"))
       IF  .NOT. EMPTY(ALLTRIM(lccampanha))
          SELECT ucrscampanhasaplicadas
          APPEND BLANK
          REPLACE ucrscampanhasaplicadas.campanha WITH lccampanha
       ENDIF
    ENDFOR
 ENDSCAN
 UPDATE uCrsPesqCampanhasAt FROM uCrsPesqCampanhasAt INNER JOIN ucrsCampanhasAplicadas ON ALLTRIM(ucrscampanhasaplicadas.campanha)==astr(ucrspesqcampanhasat.id) SET ucrspesqcampanhasat.aplicada = .T.
 IF USED("ucrsCampanhasAplicadas")
    fecha("ucrsCampanhasAplicadas")
 ENDIF
 SELECT ucrspesqcampanhasat
 LOCATE FOR ucrspesqcampanhasat.id==lcid
ENDPROC
**
FUNCTION uf_atcampanhas_produtosoferta
 LPARAMETERS lcidcampanha, lccampanhanome, lcreforig, lcqtorig, lcqtorig2
 LOCAL lclinhaspromo, lcqtdistribuida, lcqtoferta, lcvalordistribuido, lcvalorarremfalta, lcvaloroferta, lcqtoferta
 STORE 0 TO lclinhaspromo, lcqtdistribuida, lcqtoferta
 STORE 0.00  TO lcvalordistribuido, lcvalorarremfalta, lcvaloroferta
 IF USED("ucrsLinhasCampanha")
    fecha("ucrsLinhasCampanha")
 ENDIF
 CREATE CURSOR ucrsLinhasCampanha (stamp C(25), fistamp C(25), u_epvp N(9, 3), etiliquido N(9, 3), ref C(25), design C(100), qtt N(9, 0), oferta L)
 SELECT * FROM fi INTO CURSOR ucrsFiAux READWRITE
 SELECT fi.* FROM fi WHERE 1=0 INTO CURSOR uCrsProdOfertasCamp READWRITE
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		Select ref from campanhas_pOfertas_lista where campanhas like '%(' + '<<ASTR(lcIdCampanha)>>' + ')%'
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "ucrsCampanhasProdutosOferta", lcsql)
    uf_perguntalt_chama("Ocorreu uma anomalia a verificar produtos associados �s campanhas.", "OK", "", 16)
    RETURN .F.
 ENDIF
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		Select *from campanhas_pOfertas where campanhas_id = '<<ASTR(lcIdCampanha)>>' AND excecao = 0
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsCampanhasQttProdutosOferta", lcsql)
    uf_perguntalt_chama("Ocorreu uma anomalia a verificar produtos associados �s campanhas.", "OK", "", 16)
    RETURN .F.
 ENDIF
 SELECT fi.* FROM fi INNER JOIN ucrsCampanhasProdutosOferta ON ALLTRIM(fi.ref)=ALLTRIM(ucrscampanhasprodutosoferta.ref) WHERE  NOT EMPTY(ALLTRIM(fi.ref)) ORDER BY fi.u_epvp DESC INTO CURSOR ucrsProdOfertasCamp READWRITE
 IF lcqtorig>1
    FOR i = 1 TO INT(lcqtorig)
       SELECT ucrsprodofertascamp
       GOTO TOP
       LOCATE FOR ucrsprodofertascamp.qtt>0
       IF FOUND()
          SELECT ucrsprodofertascamp
          REPLACE ucrsprodofertascamp.qtt WITH ucrsprodofertascamp.qtt-1
       ENDIF
    ENDFOR
 ENDIF
 SELECT ucrsprodofertascamp
 COUNT TO lcregistosoferta
 IF lcregistosoferta==0
    RETURN .F.
 ENDIF
 SELECT ucrscampanhasqttprodutosoferta
 lcqtoferta = ucrscampanhasqttprodutosoferta.qt
 WAIT WINDOW "QT. Oferta: "+astr(INT(lcqtoferta)) TIMEOUT 1
 lcqtmaxoferta = INT(lcqtoferta)
 SELECT * FROM ucrsProdOfertasCamp ORDER BY ucrsprodofertascamp.u_epvp INTO CURSOR ucrsProdOfertasCampAux READWRITE
 IF USED("ucrsProdOfertasCamp")
    fecha("ucrsProdOfertasCamp")
 ENDIF
 SELECT * FROM ucrsProdOfertasCampAux INTO CURSOR uCrsProdOfertasCamp READWRITE
 IF USED("ucrsProdOfertasCampAux")
    fecha("ucrsProdOfertasCampAux")
 ENDIF
 lcqt = 0
 SELECT ucrsprodofertascamp
 GOTO TOP
 SCAN
    FOR i = 1 TO ucrsprodofertascamp.qtt
       IF lcqt<lcqtmaxoferta
          SELECT ucrslinhascampanha
          GOTO TOP
          LOCATE FOR ALLTRIM(ucrslinhascampanha.fistamp)==ALLTRIM(ucrsprodofertascamp.fistamp)
          IF  .NOT. FOUND()
             SELECT ucrslinhascampanha
             APPEND BLANK
             REPLACE ucrslinhascampanha.fistamp WITH ucrsprodofertascamp.fistamp
             REPLACE ucrslinhascampanha.etiliquido WITH ucrsprodofertascamp.etiliquido
             REPLACE ucrslinhascampanha.ref WITH ucrsprodofertascamp.ref
             REPLACE ucrslinhascampanha.design WITH ucrsprodofertascamp.design
             REPLACE ucrslinhascampanha.qtt WITH 1
             REPLACE ucrslinhascampanha.oferta WITH .T.
             REPLACE ucrslinhascampanha.u_epvp WITH ucrsprodofertascamp.u_epvp
             lcqt = lcqt+1
          ELSE
             SELECT ucrslinhascampanha
             REPLACE ucrslinhascampanha.qtt WITH ucrslinhascampanha.qtt+1
             lcqt = lcqt+1
          ENDIF
       ENDIF
    ENDFOR
 ENDSCAN
 SELECT ucrslinhascampanha
 CALCULATE SUM(ucrslinhascampanha.u_epvp*ucrslinhascampanha.qtt) TO lcvaloroferta 
 WAIT WINDOW "Valor de Oferta: "+astr(lcvaloroferta, 9, 2) TIMEOUT 1
 SELECT ucrslinhascampanha
 GOTO TOP
 SCAN
    SELECT ucrsprodvendascamp
    GOTO TOP
    SCAN FOR ALLTRIM(ucrsprodvendascamp.fistamp)==ALLTRIM(ucrslinhascampanha.fistamp)
       SELECT ucrsprodvendascamp
       REPLACE ucrsprodvendascamp.qtt WITH ucrsprodvendascamp.qtt-ucrslinhascampanha.qtt
    ENDSCAN
 ENDSCAN
 lcqtj = 0
 SELECT ucrsprodvendascamp
 GOTO TOP
 SCAN
    FOR i = 1 TO ucrsprodvendascamp.qtt
       IF lcqtj<=INT(lcqt*lcqtorig)
          SELECT ucrslinhascampanha
          GOTO TOP
          LOCATE FOR ALLTRIM(ucrslinhascampanha.fistamp)==ALLTRIM(ucrsprodvendascamp.fistamp)
          IF  .NOT. FOUND()
             SELECT ucrslinhascampanha
             APPEND BLANK
             REPLACE ucrslinhascampanha.fistamp WITH ucrsprodvendascamp.fistamp
             REPLACE ucrslinhascampanha.etiliquido WITH ucrsprodvendascamp.etiliquido
             REPLACE ucrslinhascampanha.ref WITH ucrsprodvendascamp.ref
             REPLACE ucrslinhascampanha.design WITH ucrsprodvendascamp.design
             REPLACE ucrslinhascampanha.qtt WITH 1
             REPLACE ucrslinhascampanha.oferta WITH .F.
             REPLACE ucrslinhascampanha.u_epvp WITH ucrsprodvendascamp.u_epvp
             lcqtj = lcqtj+1
          ELSE
             SELECT ucrslinhascampanha
             REPLACE ucrslinhascampanha.qtt WITH ucrslinhascampanha.qtt+1
             lcqtj = lcqtj+1
          ENDIF
       ENDIF
    ENDFOR
 ENDSCAN
 SELECT ucrslinhascampanha
 CALCULATE SUM(ucrslinhascampanha.qtt) TO lctotalqtt 
 lcvalordistribuido = 0
 SELECT ucrslinhascampanha
 GOTO TOP
 SCAN
    SELECT fi
    GOTO TOP
    LOCATE FOR ALLTRIM(fi.fistamp)==ALLTRIM(ucrslinhascampanha.fistamp)
    IF FOUND()
       IF ((lcvaloroferta/lctotalqtt)*ucrslinhascampanha.qtt)>fi.etiliquido
          uf_perguntalt_chama("Valor de vendas insuficiente para aplicar a campanha.", "OK", "", 64)
          SELECT fi
          GOTO TOP
          SCAN FOR  .NOT. (LEFT(fi.design, 1)==".")
             uf_atendimento_alteradescval(.T., 0)
          ENDSCAN
       ELSE
          uf_atcampanhas_resgistacampanhafi(lcidcampanha, lccampanhanome)
          SELECT fi
          lcnovovalordesc = fi.u_descval+((lcvaloroferta/lctotalqtt)*ucrslinhascampanha.qtt)
          uf_atendimento_alteradescval(.T., 0)
          uf_atendimento_alteradescval(.T., lcnovovalordesc)
          lcvalordistribuido = lcvalordistribuido+fi.u_descval
       ENDIF
    ENDIF
 ENDSCAN
 IF lcvalordistribuido<>lcvaloroferta
    lcvalorarremfalta = lcvalordistribuido-lcvaloroferta
    IF lcvalordistribuido>lcvaloroferta
       lcvalorarremfalta = (lcvalordistribuido-lcvaloroferta)*-1
    ELSE
       lcvalorarremfalta = lcvalordistribuido-lcvaloroferta
    ENDIF
    SELECT ucrslinhascampanha
    GOTO TOP
    LOCATE FOR ALLTRIM(fi.fistamp)==ALLTRIM(ucrslinhascampanha.fistamp)
    IF FOUND()
       uf_atendimento_alteradescval(.T., fi.u_descval+lcvalorarremfalta)
    ENDIF
 ENDIF
 IF USED("uCrsCampanhasQttProdutosOferta")
    fecha("uCrsCampanhasQttProdutosOferta")
 ENDIF
ENDFUNC
**
PROCEDURE uf_atcampanhas_resgistacampanhafi
 LPARAMETERS lccampanhaid, lccampanhadescr
 SELECT fi
 ALINES(ucrscampanhas, fi.campanhas, .F., ",")
 lccampanharegistada = .F.
 FOR i = 1 TO ALEN(ucrscampanhas)
    IF LEFT(ALLTRIM(ucrscampanhas(i)), 1)==astr(lccampanhaid)
       lccampanharegistada = .T.
       EXIT
    ENDIF
 ENDFOR
 IF lccampanharegistada==.F.
    SELECT fi
    REPLACE fi.campanhas WITH ALLTRIM(fi.campanhas)+IIF( .NOT. EMPTY(ALLTRIM(fi.campanhas)), ",", "")+astr(lccampanhaid)+"-("+STRTRAN(ALLTRIM(lccampanhadescr), ',', '')+")"
 ENDIF
ENDPROC
**
FUNCTION uf_atcampanhas_marcaaplicaveis
 LOCAL lccampanhas, lcid, lcqttaplicavel, lccampanhavalida, lcencontroucampanha, lcrows
 SELECT * FROM uCrsPesqCampanhasAT ORDER BY ucrspesqcampanhasat.id INTO CURSOR uCrsPesqCampanhasAtAux READWRITE
 SELECT ucrspesqcampanhasataux
 GOTO TOP
 SCAN
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			Select ISNULL(MIN(qt),0) as qt from campanhas_st (nolock) where campanhas_id = <<uCrsPesqCampanhasAtAux.id>> and excecao = 0 
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "uCrsCampanhasQtAplicavel", lcsql)
       uf_perguntalt_chama("Ocorreu uma anomalia a verificar produtos associados �s campanhas.", "OK", "", 16)
       RETURN .F.
    ENDIF
    IF ucrscampanhasqtaplicavel.qt>0
       SELECT ucrscampanhasqtaplicavel
       lcqttaplicavel = ucrscampanhasqtaplicavel.qt
    ELSE
       lcqttaplicavel = 1
    ENDIF
    IF USED("ucrsCampanhasQtAplicavel")
       fecha("ucrsCampanhasQtAplicavel")
    ENDIF
    uf_atcampanhas_verificalinhascampanha(ucrspesqcampanhasataux.id, ucrscampanhaslist.vallimite)
    SELECT ucrsfipontos
    CALCULATE SUM(ucrsfipontos.qtt) TO lcqtprodvendas 
    IF lcqtprodvendas>=lcqttaplicavel .AND. lcqtprodvendas<>0
       lccampanhavalida = .T.
    ELSE
       SELECT ucrspesqcampanhasataux
       REPLACE ucrspesqcampanhasataux.apagar WITH .T.
    ENDIF
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			Select ref from campanhas_pOfertas_lista where campanhas like '%(' + '<<uCrsPesqCampanhasAtAux.id>>' + ')%'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "ucrsCampanhasProdutosOferta", lcsql)
       uf_perguntalt_chama("Ocorreu uma anomalia a verificar produtos associados �s campanhas.", "OK", "", 16)
       RETURN .F.
    ENDIF
    IF RECCOUNT("ucrsCampanhasProdutosOferta")>0
       lcsql = ''
       TEXT TO lcsql TEXTMERGE NOSHOW
				SELECT qtOferta = ISNULL(SUM(qt),0) FROM campanhas_st WHERE campanhas_id = <<uCrsPesqCampanhasAtAux.id>> and excecao = 0
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "uCrsQtOferta", lcsql)
          uf_perguntalt_chama("Ocorreu uma anomalia a verificar produtos associados �s campanhas.", "OK", "", 16)
          RETURN .F.
       ENDIF
       SELECT ucrsqtoferta
       lcqtoferta = ucrsqtoferta.qtoferta
       IF lcqtoferta>0
          SELECT fi.* FROM Fi INNER JOIN uCrsCampanhasProdutosOferta ON ALLTRIM(fi.ref)=ALLTRIM(ucrscampanhasprodutosoferta.ref) WHERE  NOT EMPTY(ALLTRIM(fi.ref)) INTO CURSOR uCrsProdVendasCamp READWRITE
          SELECT ucrsprodvendascamp
          CALCULATE SUM(ucrsprodvendascamp.qtt) TO lcqtprodvendas 
          IF lcqtprodvendas<lcqtoferta
             SELECT ucrspesqcampanhasataux
             REPLACE ucrspesqcampanhasataux.apagar WITH .T.
          ENDIF
       ENDIF
    ENDIF
    IF USED("ucrsCampanhasProdutosOferta")
       fecha("ucrsCampanhasProdutosOferta")
    ENDIF
    IF USED("ucrsQtOferta")
       fecha("ucrsQtOferta")
    ENDIF
 ENDSCAN
 DELETE FROM uCrsPesqCampanhasAtAux WHERE ucrspesqcampanhasataux.apagar==.T.
 SELECT ucrspesqcampanhasataux
 GOTO TOP
 SCAN
    SELECT ucrspesqcampanhasat
    GOTO TOP
    LOCATE FOR ucrspesqcampanhasat.id==ucrspesqcampanhasataux.id
    IF FOUND()
       SELECT ucrspesqcampanhasat
       REPLACE ucrspesqcampanhasat.aplicavel WITH .T.
    ELSE
       SELECT ucrspesqcampanhasat
       REPLACE ucrspesqcampanhasat.aplicavel WITH .F.
    ENDIF
 ENDSCAN
 IF USED("uCrsPesqCampanhasAtAux")
    fecha("uCrsPesqCampanhasAtAux")
 ENDIF
 IF USED("ucrsFiPontosAplicaveis")
    fecha("ucrsFiPontosAplicaveis")
 ENDIF
 SELECT ucrspesqcampanhasat
 GOTO TOP
ENDFUNC
**

FUNCTION uf_atcampanhas_verificalinhascampanha
 LPARAMETERS lnid, lcvallimite, lcimporthist
 LOCAL lcsql
 STORE '' TO lcsql
 IF EMPTY(lcimporthist)
    IF lcvallimite=0
       SELECT * FROM fi WHERE  NOT EMPTY(fi.ref) INTO CURSOR uCrsFiPontos READWRITE
    ELSE
       SELECT * FROM fi WHERE  NOT EMPTY(fi.ref) AND fi.u_epvp<lcvallimite INTO CURSOR uCrsFiPontos READWRITE
    ENDIF
 ENDIF
 IF USED("uCrsCampanhasListaSt")
    SELECT * FROM uCrsCampanhasListaSt WHERE AT("("+ALLTRIM(STR(lnid))+")", ucrscampanhaslistast.campanhas)>0 INTO CURSOR uCrsCampanhasListaStAux READWRITE
 ELSE
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			Select ref from campanhas_st_lista (nolock) where campanhas like '%(<<lnID>>)%'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "uCrsCampanhasListaStAux", lcsql)
       uf_perguntalt_chama("Ocorreu uma anomalia a verificar produtos associados �s campanhas.", "OK", "", 16)
       RETURN .F.
    ENDIF
 ENDIF
 
*!*	 ** verificar se acumula pontos apenas com receita
*!*	 IF uf_gerais_getParameter_site('ADM0000000135','BOOL')

*!*		SELECT uCrsCabVendas 
*!*		GO TOP 
*!*		SELECT * FROM uCrsCabVendas INTO CURSOR uCrsCabVendascamp readwrite
*!*		
*!*		SELECT ucrsFiPontos 
*!*		GO TOP 	
*!*		SCAN 
*!*			LOCAL lcauxposdelfiponto 
*!*			lcauxposdelfiponto = RECNO("ucrsFiPontos")
*!*			LOCAL lcFistampfaminome, lcFamiNomeSel, lctipolincab, lcReceitaTipo
*!*			lcFamiNomeSel = ''
*!*			lctipolincab = ''
*!*			lcReceitaTipo = ''
*!*			lcFistampfaminome = ALLTRIM(ucrsFiPontos.ref)
*!*			SELECT ucrsatendst
*!*			LOCATE FOR ALLTRIM(ucrsatendst.ref) == ALLTRIM(lcFistampfaminome)
*!*			IF FOUND()
*!*				lcFamiNomeSel  = ALLTRIM(ucrsatendst.faminome)					
*!*			ENDIF 
*!*			SELECT fi 		
*!*			IF ALLTRIM(lcFamiNomeSel) == 'MSRM' &&AND uf_gerais_getParameter_site('ADM0000000135','BOOL')
*!*				LOCAL lcLordemFiS, lcNrReceitaCamp
*!*				lcNrReceitaCamp=''
*!*				SELECT ucrsFiPontos
*!*				lcLordemFiS = ucrsFiPontos.lordem
*!*				SELECT uCrsCabVendascamp
*!*				LOCATE FOR  LEFT(ALLTRIM(STR(uCrsCabVendascamp.lordem)),2)==LEFT(ALLTRIM(STR(lcLordemFiS)),2)
*!*				IF FOUND()
*!*					lcReceitaTipo = ALLTRIM(uCrsCabVendascamp.receita_tipo)	
*!*					lcNrReceitaCamp = ALLTRIM(STREXTRACT(uCrsCabVendascamp.design, ':', ' -', 1, 0))				
*!*				ENDIF
*!*			ENDIF 
*!*			
*!*			**IF !uf_gerais_getParameter_site('ADM0000000135','BOOL') OR (uf_gerais_getParameter_site('ADM0000000135','BOOL') AND lcReceitaTipo <> 'SR' AND ALLTRIM(lcFamiNomeSel) == 'MSRM' AND len(alltrim(lcNrReceitaCamp))<>0)
*!*			IF lcReceitaTipo <> 'SR' AND ALLTRIM(lcFamiNomeSel) == 'MSRM' AND len(alltrim(lcNrReceitaCamp))<>0
*!*			
*!*			ELSE
*!*				SELECT ucrsFiPontos
*!*				TRY
*!*	                GOTO lcauxposdelfiponto 
*!*	                DELETE 
*!*	            CATCH
*!*	            ENDTRY
*!*			ENDIF 
*!*		ENDSCAN
*!*		IF USED("uCrsCabVendascamp") 
*!*			fecha("uCrsCabVendascamp") 
*!*		ENDIF 
*!*	ENDIF 
 
 SELECT ucrsfipontos
 GOTO TOP
 SCAN
    SELECT ucrscampanhaslistastaux
    GOTO TOP
    LOCATE FOR ALLTRIM(ucrsfipontos.ref)==ALLTRIM(ucrscampanhaslistastaux.ref)
    IF  .NOT. FOUND()
       SELECT ucrsfipontos
       DELETE
    ENDIF
 ENDSCAN
 IF USED("uCrsCampanhasListaStAux")
    fecha("uCrsCampanhasListaStAux")
 ENDIF
ENDFUNC
**
PROCEDURE uf_atcampanhas_removercampanhas
 IF uf_perguntalt_chama("Vai eliminar todos os descontos e campanhas aplicadas ao Atendimento. Pretende continuar?", "Sim", "N�o")
    atendimento.campanhas = .F.
    SELECT fi
    GOTO TOP
    SCAN FOR  .NOT. (LEFT(fi.design, 1)==".")
       REPLACE fi.campanhas WITH ""
       uf_atendimento_alteradesc(.T., 0)
       uf_atendimento_alteradescval(.T., 0)
    ENDSCAN
    uf_atcampanhas_sair()
 ENDIF
ENDPROC
**
PROCEDURE uf_atcampanhas_ordernacursor
 SELECT * FROM uCrsPesqCampanhasAt ORDER BY aplicada DESC, aplicavel DESC, id INTO CURSOR uCrsPesqCampanhasAtAux READWRITE
 SELECT ucrspesqcampanhasat
 GOTO TOP
 SCAN
    DELETE
 ENDSCAN
 SELECT ucrspesqcampanhasat
 APPEND FROM DBF("uCrsPesqCampanhasAtAux")
 SELECT ucrspesqcampanhasat
 GOTO TOP
ENDPROC
**
FUNCTION uf_atcampanhas_actualizaprodutosnovos
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		Select ref from campanhas_st_lista (nolock)
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsCampanhasListaStAux", lcsql)
    uf_perguntalt_chama("Ocorreu uma anomalia a verificar produtos associados �s campanhas.", "OK", "", 16)
    RETURN .F.
 ENDIF
 SELECT .F. AS apagar, fi.ref FROM fi WHERE  NOT EMPTY(ALLTRIM(fi.ref)) INTO CURSOR ucrsFiTemp READWRITE
 SELECT ucrsfitemp
 GOTO TOP
 SCAN
    SELECT ucrscampanhaslistastaux
    GOTO TOP
    LOCATE FOR ALLTRIM(ucrscampanhaslistastaux.ref)==ALLTRIM(ucrsfitemp.ref)
    IF FOUND()
       SELECT ucrsfitemp
       REPLACE ucrsfitemp.apagar WITH .T.
    ENDIF
 ENDSCAN
 DELETE FROM ucrsFiTemp WHERE  .NOT. EMPTY(ucrsfitemp.apagar)
 IF USED("uCrsCampanhasListaStAux")
    fecha("uCrsCampanhasListaStAux")
 ENDIF
 SELECT ucrsfitemp
 GOTO TOP
 SCAN
    uf_campanhas_actualizacampanhasproduto(ucrsfitemp.ref)
 ENDSCAN
 IF USED("ucrsFiTemp")
    fecha("ucrsFiTemp")
 ENDIF
ENDFUNC
**
PROCEDURE uf_atcampanhas_sair
 IF USED("ucrsProdVendasCamp")
    fecha("ucrsProdVendasCamp")
 ENDIF
 IF USED("uCrsPesqCampanhasAtAux")
    fecha("uCrsPesqCampanhasAtAux")
 ENDIF
 atcampanhas.hide
 atcampanhas.release
ENDPROC
**
