**
Procedure uf_infoembal_chama
	Lparameters lcBackOffice, lcDocumentos, lcErros

	Select fi_trans_info
	Goto Top
	If Type("INFOEMBAL")=="U"
		Do Form INFOEMBAL With lcBackOffice, lcDocumentos, lcErros
	Else
		INFOEMBAL.Show()
	Endif
Endproc
**
Procedure uf_infoembal_sair

	If Type("upv_cancelaInfoEmbal") <> "U"
		upv_cancelaInfoEmbal = .T.
	Endif

	INFOEMBAL.Hide()
	INFOEMBAL.Release
	Release INFOEMBAL
Endproc
**
Procedure uf_infoembal_gravar

	If Type("upv_cancelaInfoEmbal") <> "U"
		upv_cancelaInfoEmbal = .F.
	Endif

	INFOEMBAL.Hide()
	INFOEMBAL.Release
	Release INFOEMBAL
Endproc
**
Function uf_infoembal_scanner
	Lparameters lcBackOffice, lcDocumentos

	INFOEMBAL.txtscanner.Value = Strtran(INFOEMBAL.txtscanner.Value, Chr(39), '')
	If Empty(Alltrim(INFOEMBAL.txtscanner.Value))
		Return .F.
	Endif
	Local lceanid, lcdtvalid, lcloteid, lcidid, lcrefid, lcrefpaisid, lcreftempid, lcposdelimiterid, uv_newToken
	Store '' To lceanid, lcdtvalid, lcloteid, lcidid, lcrefid, lcrefpaisid
	Store 0 To lcposdelimiterid

	uv_newToken = uf_gerais_stamp()

	lcreftempid = Upper(Alltrim(INFOEMBAL.txtscanner.Value))
	uv_dMatrix = Upper(Alltrim(INFOEMBAL.txtscanner.Value))

	Local lcMax
	lcMax = 100

	Local lcEan
	lcEan = 14

	lcreftempid = Strtran(lcreftempid , "'", "-")
	lcreftempid = uf_gerais_remove_last_caracter_by_char(lcreftempid,'#')

	If !uf_gerais_retorna_campos_datamatrix_sql(lcreftempid)
		Return .F.
	Endif

	Select ucrDataMatrixTemp
	If !ucrDataMatrixTemp.valido
		uf_perguntalt_chama("N�o foi possivel validar o datamatrix", "", "OK", 16)
		Return .F.
	Endif

	lcdtvalid = uf_gerais_retornaApenasAlphaNumericos(ucrDataMatrixTemp.Data)
	lcrefid  = uf_gerais_retornaApenasAlphaNumericos(ucrDataMatrixTemp.ref)
	lcrefpaisid = ucrDataMatrixTemp.countryCode
	lceanid = ucrDataMatrixTemp.pc
	lcloteid = ucrDataMatrixTemp.lote
	lcidid = ucrDataMatrixTemp.sn


	If Empty(lcrefid)

		uf_gerais_notif("Medicamento conferido." + Chr(13) + "Datamatrix n�o desativado.", 3000)
		uf_gerais_registaerrolog("Erro ao ler o datamatrix uf_infoembal_scanner: " +  uv_dMatrix, "datamatrix")


		lcrefid = uf_gerais_getFromString(uv_dMatrix, '714', 7, .T.)
		If Len(lcrefid) <> 7
			uf_gerais_registaerrolog("Artigo n�o encontrado: " +  uv_dMatrix, "datamatrixCnp")
			uf_perguntalt_chama("ERRO NA LEITURA OU C�DIGO DE BARRAS INV�LIDO.", "OK", "", 64)
			Return .F.
		Endif

	Endif

	Local lcjalida
	lcjalida = .F.
	Select fi_trans_info
	Goto Top
	Scan
		If Alltrim(fi_trans_info.productcode)==Alltrim(lceanid) .And. Alltrim(fi_trans_info.packserialnumber)==Alltrim(lcidid)
			uf_Atendimento_SinglePackVerify(fi_trans_info.token)
			lcjalida = .T.
		Endif
	Endscan
	Local lcatribuiu
	lcatribuiu = .F.
	Select fi_trans_info
	Goto Top
	Scan
		If lcjalida=.F.

			If Alltrim(fi_trans_info.productnhrn)==Alltrim(lcrefid) .And. Empty(fi_trans_info.packserialnumber) .And. lcatribuiu=.F.
				Replace fi_trans_info.token With Iif(lcBackOffice, Alltrim(uv_newToken), Alltrim(nratendimento))
				Replace fi_trans_info.productcode With lceanid
				Replace fi_trans_info.productcodescheme With 'GTIN'
				Replace fi_trans_info.batchid With lcloteid
				Set Century On
				If Alltrim(Substr(lcdtvalid, 5, 2))<>'00' And !Empty(Alltrim(Substr(lcdtvalid, 5, 2)))
					Replace fi_trans_info.batchexpirydate With Date(2000+Val(Substr(lcdtvalid, 1, 2)), Val(Substr(lcdtvalid, 3, 2)), Val(Substr(lcdtvalid, 5, 2)))
				Else
					Replace fi_trans_info.batchexpirydate With Gomonth(Date(2000+Val(Substr(lcdtvalid, 1, 2)), Val(Substr(lcdtvalid, 3, 2)), 1), 1)-1
				Endif
				Replace fi_trans_info.packserialnumber With lcidid
				Replace fi_trans_info.posterminal With Alltrim(myterm)
				Replace fi_trans_info.country_productnhrn With Val(uf_gerais_retornaApenasAlphaNumericos(lcrefpaisid))
				Replace fi_trans_info.ousrinis With Alltrim(ch_vendnm)
				Replace fi_trans_info.ousrdata With Datetime()
				lcatribuiu = .T.


				Select fi_trans_info
				uf_Atendimento_SinglePackVerify(fi_trans_info.token)

				If lcBackOffice

					If !lcDocumentos
						uf_faturacao_tipoEstadoInfoEmbal()
					Else
						uf_documento_tipoEstadoInfoEmbal()
					Endif
				Endif

			Endif
		Endif
	Endscan



	If (lcatribuiu=.F. .And. lcjalida=.F.)
		Local uv_fistamp, lcTipoOperacao
		Store "" To uv_fistamp, lcTipoOperacao
		lcTipoOperacao = 'S'


		If lcDocumentos = .F.

			Select FI2
			Go Top
			Locate For Alltrim(FI2.productcodeEmbal) == Alltrim(lceanid) And !Empty(FI2.productcodeEmbal)
			If Found()

				Select FI2
				uv_fistamp = FI2.fistamp

			Endif

		Endif

		If Empty(uv_fistamp)

			If lcDocumentos

				If uf_gerais_compStr(myTipoDoc, "BO")
					Select bistamp As recStamp, * From bi With (Buffering = .F.) Where uf_gerais_compStr(bi.ref, lcrefid) Into Cursor uc_tmpFI
				Else
					Select fnstamp As recStamp, * From fn With (Buffering = .F.) Where uf_gerais_compStr(fn.ref, lcrefid) Into Cursor uc_tmpFI
				Endif

			Else

				Select fistamp As recStamp, * From fi With (Buffering = .T.) Where Alltrim(fi.ref) == Alltrim(lcrefid) Into Cursor uc_tmpFI

			Endif

			Select uc_tmpFI
			Go Top
			Scan

				Select fi_trans_info
				Go Top

				Locate For Alltrim(fi_trans_info.recStamp) == Alltrim(uc_tmpFI.recStamp) And Alltrim(fi_trans_info.productnhrn) == Alltrim(uc_tmpFI.ref)

				If !Found()
					uv_fistamp = uc_tmpFI.recStamp
					If !lcDocumentos
						If uc_tmpFI.partes > 0
							lcTipoOperacao = "E"
						Endif
					Endif

					Exit
				Endif

			Endscan

		Endif

		If !Empty(uv_fistamp)
			Select fi_trans_info
			Append Blank

			Replace fi_trans_info.productnhrn With Alltrim(lcrefid)
			Replace fi_trans_info.token With Iif(lcBackOffice, Alltrim(uv_newToken), Alltrim(nratendimento))
			Replace fi_trans_info.productcode With lceanid
			Replace fi_trans_info.productcodescheme With 'GTIN'
			Replace fi_trans_info.batchid With lcloteid
			Set Century On
			If Alltrim(Substr(lcdtvalid, 5, 2))<>'00' And !Empty(Alltrim(Substr(lcdtvalid, 5, 2)))
				Replace fi_trans_info.batchexpirydate With Date(2000+Val(Substr(lcdtvalid, 1, 2)), Val(Substr(lcdtvalid, 3, 2)), Val(Substr(lcdtvalid, 5, 2)))
			Else
				Replace fi_trans_info.batchexpirydate With Gomonth(Date(2000+Val(Substr(lcdtvalid, 1, 2)), Val(Substr(lcdtvalid, 3, 2)), 1), 1)-1
			Endif
			Replace fi_trans_info.packserialnumber With lcidid
			Replace fi_trans_info.posterminal With Alltrim(myterm)
			Replace fi_trans_info.country_productnhrn With Val(uf_gerais_retornaApenasAlphaNumericos(lcrefpaisid))
			Replace fi_trans_info.ousrinis With Alltrim(ch_vendnm)
			Replace fi_trans_info.ousrdata With Datetime()
			Replace fi_trans_info.tipo With lcTipoOperacao
			Replace fi_trans_info.recStamp With Alltrim(uv_fistamp)
			Replace fi_trans_info.recTable With Iif(!lcDocumentos, "FT", Alltrim(myTipoDoc))

			Local uv_newClientTrxId
			uv_newClientTrxId = uf_gerais_stamp()

			Select fi_trans_info

			Replace fi_trans_info.clienttrxid With Alltrim(uv_newClientTrxId)

			If !lcDocumentos

				Select FI2
				Locate For Alltrim(FI2.fistamp) == Alltrim(uv_fistamp)
				If Found()
					If Empty(FI2.productcodeEmbal)
						Replace FI2.productcodeEmbal With Alltrim(lceanid)
					Endif
				Endif

			Endif


			Select fi_trans_info
			uf_Atendimento_SinglePackVerify(fi_trans_info.token)

			If lcBackOffice

				If !lcDocumentos
					uf_faturacao_tipoEstadoInfoEmbal()
				Else
					uf_documento_tipoEstadoInfoEmbal()
				Endif
			Endif

		Else
			uf_gerais_registaerrolog("Artigo n�o encontrado: " +  uv_dMatrix, "datamatrixCnp")
			uf_perguntalt_chama("Artigo n�o encontrado", "OK", "", 64)

		Endif

	Endif

	If(Used("fi_trans_info"))
		Select fi_trans_info
		Goto Top
	Endif

	INFOEMBAL.pgfinfo.page1.grdinfo.Refresh
Endfunc
**
Function uf_infoembal_cor_linhas
	Lparameters lcDocumentos

	If(Empty(uf_gerais_getParameter_site("ADM0000000205", "BOOL", mySite)))
		Return .T.
	Endif

	If Type("INFOEMBAL")!="U"
		Local i
		i = 0
		For i = 1 To INFOEMBAL.pgfinfo.page1.grdinfo.ColumnCount

			If !INFOEMBAL.erros

				If Upper(Alltrim(INFOEMBAL.pgfinfo.page1.grdinfo.Columns(i).ControlSource))==Upper("fi_trans_info.productnhrn") Or;
						UPPER(Alltrim(INFOEMBAL.pgfinfo.page1.grdinfo.Columns(i).ControlSource))==Upper("fi_trans_info.state")
					INFOEMBAL.pgfinfo.page1.grdinfo.Columns(i).DynamicBackColor = "uf_infoembal_getcolor(" + Iif(lcDocumentos, ".T.", ".F.") + ")"
				Endif

			Endif

		Endfor

		If INFOEMBAL.erros

			INFOEMBAL.pgfinfo.page1.grdinfo.msg.ControlSource = "ALLTRIM(fi_trans_info.code) + '|' +ALLTRIM(fi_trans_info.description)"
			INFOEMBAL.pgfinfo.page1.grdinfo.msg.header1.Caption = "Mensagem"
			INFOEMBAL.pgfinfo.page1.grdinfo.msg.DynamicBackColor = "uf_infoembal_getcolor(" + Iif(lcDocumentos, ".T.", ".F.") + ")"

		Endif

		INFOEMBAL.pgfinfo.page1.grdinfo.Refresh()
	Endif

	Return .T.
Endproc
**

Function uf_infoembal_getcolor
	Lparameters lcDocumentos

	If INFOEMBAL.erros
		Return Iif(uf_gerais_compStr(fi_trans_info.Code, 'NMVS_SUCCESS'), Rgb(42, 143, 154),Rgb(255,0,0))
	Endif

	If lcDocumentos

		Return  Iif(uf_gerais_compStr(fi_trans_info.state, 'ACTIVE'),Rgb(42, 143, 154), Rgb(255,0,0))

	Else
		Select fi_trans_info
		Local lcEstadoValidoReverterEstado
		lcEstadoValidoReverterEstado = uf_infocaixas_validaSePossivelReverterEstado(fi_trans_info.state,fi_trans_info.undoPossibleReturn,fi_trans_info.userMatchesReturn,fi_trans_info.reqType)

		Return  Iif(((Upper(Alltrim(fi_trans_info.tipo))=='S' And  Upper(Alltrim(fi_trans_info.state))=='ACTIVE') Or;
			(Upper(Alltrim(fi_trans_info.tipo))=='E' And  Upper(Alltrim(fi_trans_info.state))=='INACTIVE' And lcEstadoValidoReverterEstado!=-1 )),Rgb(42, 143, 154),Rgb(255,0,0))
	Endif
Endfunc


Function uf_infoembal_convertPedidoManual
	Lparameters uv_token


	If !Used("fi_trans_info")
		Return .F.
	Endif

	If Empty(uv_token)
		Select fi_trans_info
		Replace fi_trans_info.reqManual With .T.

	Else
		Wait Timeout 1
		Select fi_trans_info
		Locate For uf_gerais_compStr(uv_token, fi_trans_info.token)

		If Found()
			Replace fi_trans_info.reqManual With .T.
		Endif

	Endif

Endfunc

Function uf_infoembal_checkRef
	Lparameters uv_ref, uv_sendMessage, lcDocumentos

	If Empty(uv_ref)
		Return .F.
	Endif

	If lcDocumentos
		If uf_gerais_compStr(myTipoDoc, "BO")
			Select bi
			Calculate Count() To uv_COUNT For Alltrim(bi.ref) = Alltrim(uv_ref)
		Else
			Select fn
			Calculate Count() To uv_COUNT For Alltrim(fn.ref) = Alltrim(uv_ref)
		Endif
	Else
		Select fi
		Calculate Count() To uv_COUNT For Alltrim(fi.ref) = Alltrim(uv_ref)
	Endif

	If uv_COUNT = 0
		If uv_sendMessage
			uf_perguntalt_chama("Artigo n�o encontrado no antendimento.", "OK", "", 64)
		Endif
		Return .F.
	Endif

	If lcDocumentos
		If uf_gerais_compStr(myTipoDoc, "BO")
			Select bistamp As recStamp, * From bi With (Buffering = .T.) Where Alltrim(bi.ref) == Alltrim(uv_ref) Into Cursor uc_tmpFI
		Else
			Select fnstamp As recStamp, * From fn With (Buffering = .T.) Where Alltrim(fn.ref) == Alltrim(uv_ref) Into Cursor uc_tmpFI
		Endif
	Else
		Select fistamp As recStamp, * From fi With (Buffering = .T.) Where Alltrim(fi.ref) == Alltrim(uv_ref) Into Cursor uc_tmpFI
	Endif

	Select * From fi_trans_info With (Buffering = .T.) Where Alltrim(productnhrn) == Alltrim(uv_ref) Into Cursor uc_tmpFiTrans

	uv_hasLine = .F.

	Select uc_tmpFI
	Go Top
	Scan

		Select uc_tmpFiTrans
		Go Top
		Locate For Alltrim(uc_tmpFiTrans.recStamp) == Alltrim(uc_tmpFI.recStamp) And Alltrim(uc_tmpFiTrans.productnhrn) == Alltrim(uc_tmpFI.ref)
		If !Found()
			uv_hasLine = .T.
			Exit
		Endif

	Endscan

	If !uv_hasLine
		If uv_sendMessage
			uf_perguntalt_chama("Artigo n�o encontrado no antendimento.", "OK", "", 64)
		Endif
		Return .F.
	Endif

	If !uf_gerais_getUmValor("fprod", "dispositivo_seguranca", "cnp = '" + Alltrim(uv_ref) + "'")
		If uv_sendMessage
			uf_perguntalt_chama("Este artigo n�o utiliza dispositivo de seguran�a.", "OK", "", 64)
		Endif
		Return .F.
	Endif

	Return .T.

Endfunc

Function uf_infoembal_addLinha
	Lparameters uv_ref, lcBackOffice, lcDocumentos, uv_qtt

	Local uv_fistamp, lcTipoOperacao, lcToken, uv_qttLin
	uv_fistamp = ''
	lcTipoOperacao = 'S'

	uv_qttLin = 0

	If !Empty(uv_qtt)
		uv_qttLin = uv_qtt
	Endif

	lcToken = uf_gerais_stamp()

	If !Empty(uv_ref)

		If !uf_infoembal_checkRef(uv_ref, .F.,lcDocumentos)
			Return .F.
		Endif

		If lcDocumentos

			If uf_gerais_compStr(myTipoDoc, "BO")
				Select bistamp As recStamp, * From bi With (Buffering = .T.) Where Alltrim(bi.ref) == Alltrim(uv_ref) Into Cursor uc_tmpFI
			Else
				Select fnstamp As recStamp, * From fn With (Buffering = .T.) Where Alltrim(fn.ref) == Alltrim(uv_ref) Into Cursor uc_tmpFI
			Endif

		Else
			Select fistamp As recStamp, * From fi With (Buffering = .T.) Where Alltrim(fi.ref) == Alltrim(uv_ref) Into Cursor uc_tmpFI
		Endif

		Select * From fi_trans_info With (Buffering = .T.) Where Alltrim(productnhrn) == Alltrim(uv_ref) Into Cursor uc_tmpFiTrans

		Select uc_tmpFI
		Go Top
		Scan

			Select uc_tmpFiTrans
			Go Top
			Locate For Alltrim(uc_tmpFiTrans.recStamp) == Alltrim(uc_tmpFI.recStamp) And Alltrim(uc_tmpFiTrans.productnhrn) == Alltrim(uc_tmpFI.ref)
			If !Found()

				uv_fistamp = Alltrim(uc_tmpFI.recStamp)
				If !lcDocumentos
					If(uc_tmpFI.partes > 0)
						lcTipoOperacao  = 'E'
					Endif
				Endif

				Exit
			Endif

		Endscan

	Endif

	For i=1 To uv_qttLin

		Select fi_trans_info
		Append Blank
		Go Bottom

		Replace fi_trans_info.token With Iif(lcBackOffice, Alltrim(lcToken), Alltrim(nratendimento))
		Replace fi_trans_info.productcodescheme With 'GTIN'
		Replace fi_trans_info.batchexpirydate With Date(1900, 1E0, 1E0)
		Replace fi_trans_info.posterminal With Alltrim(myterm)
		If !Empty(uv_ref)
			Replace fi_trans_info.productnhrn With Alltrim(uv_ref)
		Endif
		Replace fi_trans_info.ousrinis With Alltrim(ch_vendnm)
		Replace fi_trans_info.ousrdata With Datetime()
		Replace fi_trans_info.tipo With lcTipoOperacao
		If !Empty(uv_ref)
			Replace fi_trans_info.recStamp With Alltrim(uv_fistamp)
			Replace fi_trans_info.recTable With Iif(!lcDocumentos, "FT", Alltrim(myTipoDoc))

			Local uv_newClientTrxId
			uv_newClientTrxId = uf_gerais_stamp()

			Select fi_trans_info

			Replace fi_trans_info.clienttrxid With Alltrim(uv_newClientTrxId)
		Else

			uv_newStamp = uf_gerais_stamp()

			Select fi_trans_info

			Replace fi_trans_info.recStamp With Alltrim(uv_newStamp)
			Replace fi_trans_info.recTable With Iif(!lcDocumentos, "FT", Alltrim(myTipoDoc))

			Local uv_newClientTrxId
			uv_newClientTrxId = uf_gerais_stamp()

			Select fi_trans_info

			Replace fi_trans_info.clienttrxid With Alltrim(uv_newClientTrxId)
		Endif
		Replace fi_trans_info.country_productnhrn With Iif(!Empty(uv_ref) And Len(Alltrim(uv_ref))=7, 714, 0)

	Next

	If Wexist("INFOEMBAL")
		INFOEMBAL.pgfinfo.page1.grdinfo.referencia.text1.SetFocus()
	Endif

	If lcBackOffice
		If !lcDocumentos
			uf_faturacao_tipoEstadoInfoEmbal()
		Else
			uf_documento_tipoEstadoInfoEmbal()
		Endif
	Endif


Endfunc

Procedure uf_infoembal_pullRefs
	Lparameters lcBackOffice, lcDocumentos

	If lcDocumentos
		Local uv_tab, uv_stamp, uv_select, uv_qtt

		uv_tab = Iif(uf_gerais_compStr(myTipoDoc, "BO"), 'bi', 'fn')
		uv_stamp = uv_tab + "stamp"


		TEXT TO uv_select TEXTMERGE NOSHOW
			SELECT * FROM <<ALLTRIM(uv_tab)>> WITH (BUFFERING = .T.) WHERE !EMPTY(<<ALLTRIM(uv_tab)>>.ref)  INTO CURSOR uc_refsFI READWRITE
		ENDTEXT

		&uv_select.

		If mydocalteracao

			Select uc_refsFI
			Go Top
			Scan

				If uf_gerais_getUmValor("fi_trans_info", "count(*)", "recStamp = '" + uc_refsFI.&uv_stamp. + "' and code = 'NMVS_SUCCESS' and recTable = '" + myTipoDoc + "'") > 0
					Select uc_refsFI
					Delete
				Endif

				Select uc_refsFI
			Endscan

		Endif

	Else
		Select * From fi With (Buffering = .T.) Where !Empty(fi.ref) Into Cursor uc_refsFI
	Endif

	Select uc_refsFI
	Go Top
	Scan
		If !uf_gerais_compStr(Type("DOCUMENTOS"),"U")
			Select Count(*) As qtt From fi_trans_info Where fi_trans_info.productnhrn = uc_refsFI.ref Into Cursor tmpQttFiTransInfo Readwrite
			uv_qtt = uc_refsFI.qtt- tmpQttFiTransInfo.qtt
		Else
			uv_qtt = 1
		Endif

		uf_infoembal_addLinha(Alltrim(uc_refsFI.ref), lcBackOffice, lcDocumentos,uv_qtt)
		fecha("tmpQttFiTransInfo")
		Select uc_refsFI
	Endscan


	If !lcBackOffice

		uf_infoembal_removeRefsToNotPick()

	Endif

Endproc


Function uf_infoembal_removeRefsToNotPick

	If(Used("fi_trans_info") And Used("fi"))
		Select fi_trans_info
		Go Top
		Delete From fi_trans_info Where fi_trans_info.recStamp In(Select fistamp From fi Where (fi.partes == 0 And !Empty(Alltrim(fi.ofistamp))) Or (Upper(Alltrim(fi.tipor)) =='RES'))

	Else
		Return .F.
	Endif

	Return .T.
Endfunc

Function uf_infoembal_mostraEstado

	uf_perguntalt_responsivo_chama(uf_infocaixas_respServico(), "", "OK", 64)

Endfunc


Function uf_infoembal_verificar
	LOCAL nrserie , codprod, dtval, lote, ref ,pais
	
	
	Select fi_trans_info
	Go Top
	Scan
		
		LOCAL uv_pedido, uv_url 
		
		STORE "" TO nrserie , codprod, dtval, lote, ref
		STORE "0" TO pais 
		
		nrserie = fi_trans_info.packSerialNumber
		codprod = fi_trans_info.productCode
		dtval	= fi_trans_info.batchexpirydate
		lote	= fi_trans_info.batchid
		ref 	= fi_trans_info.productnhrn
		
		uv_pedido = 'VERIFY'
		uv_url = 'verifySinglePack'
		
		
		If Empty(uv_pedido) Or Empty(uv_url)
			Return .F.
		Endif



		resposta = ""
		If uf_infoembal_dadospreenchidos()

			TEXT TO lcSQL NOSHOW TEXTMERGE
			select
				token
			from
				service_credentials (nolock)
			where
				service='Logitools'
				and username='<<ALLTRIM(uCrsE1.id_lt)>>@logitools.pt'
			ENDTEXT
			uf_gerais_actgrelha("", "curToken", lcSQL)


			LcTokenReq = Alltrim(curToken.token)
			fecha("curToken")
			lcTokenPed = Alltrim(uf_gerais_stamp())

			Local lcCont2
			lcCont2 = 1
			regua(0,0,"A comunicar com o servi�o...")
			Local lcSendMsg, lcSentBatchid, lcSentStamp
			lcSendMsg = ''

			lcSendMsg = '{'
			lcSendMsg = lcSendMsg + '"token": "'+Alltrim(lcTokenPed)+'",'
			lcSendMsg = lcSendMsg + '"entity": {'
			If !Empty(myServicosEntApp )
				lcSendMsg = lcSendMsg + '"app": "lts",'
			Endif
			If !Empty(myServicosEntId)
				lcSendMsg = lcSendMsg + '"id": "lts",'
			Endif
			If !Empty(myServicosEntName)
				lcSendMsg = lcSendMsg + '"name": "lts"'
			Endif
			lcSendMsg = lcSendMsg + '},'
			lcSendMsg = lcSendMsg + '"transaction": {'
			If !Empty(codprod)
				lcSendMsg = lcSendMsg + '"productCode": "'+Alltrim(codprod)+'"'
			Endif
			lcSendMsg = lcSendMsg + ',"productCodeScheme": "GTIN"'
			lcSendMsg = lcSendMsg + ',"batchId": "'+Alltrim(lote)+'"'
			lcSentBatchid = Alltrim(lcTokenPed)
			lcSendMsg = lcSendMsg + ',"batchExpiryDate": "'+Right(Alltrim(Str(Year(dtval))),2)+Right('0'+Alltrim(Str(Month(dtval))),2)+Right('0'+Alltrim(Str(Day(dtval))),2)+'"'
			lcSendMsg = lcSendMsg + ',"packSerialNumber": "'+Alltrim(nrserie)+'"'
			lcSendMsg = lcSendMsg + ',"clientTrxId": "'+Alltrim(lcTokenPed)+'"'
			lcSendMsg = lcSendMsg + ',"posTerminal" : "'+Iif(Empty(lcUtilPortal) And Type("myTerm") <> "U", Alltrim(myterm), Alltrim(lcUtilPortal)) +'"'
			If !Empty(fi_trans_info.nmvsTrxId)

				lcSendMsg = lcSendMsg + ',"nmvsTrxId" : "'+Alltrim(fi_trans_info.nmvsTrxId)+'"'
			Endif
			If !Empty(ref) And  !Empty(Alltrim(pais)) And  Alltrim(pais)!='0'
				lcSendMsg = lcSendMsg + ',"productNhrn" : "('+Alltrim(pais)+')'+Alltrim(ref)+'"'
			Endif
			lcSendMsg = lcSendMsg + '},'
			lcSendMsg = lcSendMsg + '"sender": {'
			lcSendMsg = lcSendMsg + '"user": "'+Alltrim(myServicosUser)+'",'

			lcSendMsg = lcSendMsg + '"pw": "'+Alltrim(myServicosPassw)+'"'
			lcSendMsg = lcSendMsg + '}'
			lcSendMsg = lcSendMsg + '}'
			lcSentStamp = Alltrim(uf_gerais_stamp())

			**uf_perguntalt_responsivo_chama(lcSendMsg, "OK", "", 64)

			Local uv_resp, uv_sucesso

			Select fi_trans_info
			Replace fi_trans_info.token With Alltrim(lcTokenPed)

			uv_sucesso = uf_single_pack_requests(uv_pedido, myServicosSite + myServicosSiteSuf +'/' + Alltrim(uv_url), Alltrim(lcSendMsg), Alltrim(lcSentBatchid), Alltrim(lcSentStamp), Alltrim(LcTokenReq) )

			If uv_sucesso = .T.

				uv_resp = uf_infocaixas_respServico()

			Else

				uv_resp = "Erro a comunicar com o servi�o." + Chr(13) + "Por favor contacte o suporte."

			Endif


			lcCont2 = lcCont2 + 1
			Select fi_trans_info

			resposta = Alltrim(uv_resp)
			**resposta = 'Processado - Transa��o registada'
			
			regua(2)
			
			uf_infoembal_cor_linhas()
		Endif

	Endscan
	Select fi_trans_info
	Go Top

Endfunc


FUNCTION uf_infoembal_dadospreenchidos
	LOCAL nrserieVerify , codprodVerify, dtvalVerify, loteVerify
	
	Select fi_trans_info
	nrserieVerify 	= fi_trans_info.packSerialNumber
	codprodVerify 	= fi_trans_info.productCode
	dtvalVerify		= fi_trans_info.batchexpirydate
	loteVerify		= fi_trans_info.batchid
	
	IF EMPTY(nrserieVerify) OR EMPTY(codprodVerify) OR year(dtvalVerify)=1900 OR EMPTY(loteVerify) &&OR EMPTY(ref)
		uf_perguntalt_chama("TEM DE PREENCHER TODOS OS DADOS PARA ENVIAR O PEDIDO!","OK","",64)	
		RETURN .F.
	ELSE
		RETURN .T.
	ENDIF 
ENDFUNC 

