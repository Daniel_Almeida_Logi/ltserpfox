**
FUNCTION uf_maqdinheiropag_chama
 PUBLIC lcstampatmaq
 STORE '' TO lcstampatmaq
 LOCAL lcclno, lcclestab, lcdinheiropc
 IF regvendas.pgfreg.page2.chkporproduto.tag=="true"
    SELECT DISTINCT no FROM uCrsRegCreditos WHERE sel==.T. INTO CURSOR uCrsRegCrDistNo READWRITE
 ELSE
    SELECT DISTINCT no FROM uCrsRegCreditosValor WHERE sel==.T. INTO CURSOR uCrsRegCrDistNo READWRITE
 ENDIF
 IF RECCOUNT("uCrsRegCrDistNo")>1 .AND. regvendas.pgfreg.page2.chkporutente.tag="false"
    uf_perguntalt_chama("APENAS PODER� EMITIR O RECIBO PARA UM CLIENTE DE CADA VEZ.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF regvendas.pgfreg.page2.chkporutente.tag="false"
    SELECT ucrsregcrdistno
    GOTO TOP
    lcclno = ucrsregcrdistno.no
 ELSE
    lcclno = astr(regvendas.txtno.value)
 ENDIF
 IF  .NOT. EMPTY(regvendas.txtestab.value)
    lcclestab = astr(regvendas.txtestab.value)
 ELSE
    lcclestab = 0
 ENDIF
 IF TYPE("atendimento")=="U"
    PUBLIC nratendimento
    uf_atendimento_geranratendimento()
 ENDIF
 IF regvendas.pgfreg.page2.chkmovcaixa.tag=="true"
    uf_gestaocaixas_verificacaixa()
    IF EMPTY(ALLTRIM(mycxstamp)) .OR. EMPTY(ALLTRIM(myssstamp))
       IF uf_perguntalt_chama("A op��o de Movimentar Caixa est� activa mas n�o tem nenhuma caixa aberta."+CHR(13)+CHR(13)+"Pretende abrir uma nova caixa?", "Sim", "N�o")
          uf_caixas_gerircaixa(.F.)
       ELSE
          RETURN .F.
       ENDIF
    ENDIF
    uf_atendimento_configoperador()
 ELSE
    mycxstamp = ""
    mycxuser = ""
    myssstamp = ""
 ENDIF
 fecha("uCrsRegCrDistNo")
 lcdinheiropc = VAL(regvendas.ctnpagamento.txtdinheiro.value)
 lcstampatmaq = ALLTRIM(nratendimento)
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		INSERT INTO B_pagCentral_maquinas
			(stamp, nrAtend, nrVendas, total, ano, no, estab, vendedor
			,nome ,terminal, terminal_nome,	evdinheiro, epaga1, epaga2, echtotal
			,evdinheiro_semTroco, etroco, total_bruto, devolucoes, ntCredito, creditos, cxstamp, ssstamp, site
			,fechado, epaga3, epaga4, epaga5, epaga6
			,odata, udata)
		VALUES
			('<<astr(YEAR(DATE())) + nrAtendimento>>',
				'<<nrAtendimento>>', 0, <<Round(REGVENDAS.ctnPagamento.txtValAtend.value,2)>>,
				<<YEAR(DATE())>>, <<lcClNo>>, <<lcClEstab>>,
				<<ch_vendedor>>, '<<ALLTRIM(ch_vendnm)>>',
				<<myTermNo>>, '<<ALLTRIM(myTerm)>>',
				0, 0, 0, 0,
				0, 0,
				0, 0, 0, 0,
				'<<ALLTRIM(myCxStamp)>>', '<<ALLTRIM(mySsStamp)>>', '<<ALLTRIM(mySite)>>'
				,<<IIF(Round(REGVENDAS.ctnPagamento.txtValAtend.value,2) == 0, "1", "0")>>
				,<<0>>
				,<<0>>
				,<<0>>
				,<<0>>
				, dateadd(HOUR, <<difhoraria>>, getdate())
				, dateadd(HOUR, <<difhoraria>>, getdate()))
 ENDTEXT
 uf_gerais_actgrelha("", "", lcsql)
 uf_talao_atendimento()
 IF TYPE("maqdinheiropag")=="U"
    DO FORM maqdinheiropag
 ELSE
    maqdinheiropag.show
 ENDIF
ENDFUNC
**
PROCEDURE uf_talao_atendimento
 lcvalidaimp = uf_gerais_setimpressorapos(.T.)
 IF lcvalidaimp
    ??? CHR(27E0)+CHR(64E0)
    ??? CHR(27)+CHR(99)+CHR(48)+CHR(1)
    ??? CHR(27E0)+CHR(116)+CHR(003)
    ??? CHR(27)+CHR(33)+CHR(1)
    ?? " "
    ? LEFT(ALLTRIM(ucrse1.nomecomp), uf_gerais_getparameter("ADM0000000194", "NUM"))
    ??? CHR(27)+CHR(33)+CHR(1)
    ? SUBSTR(ucrse1.local, 1, 10)
    ?? " Tel:"
    ?? TRANSFORM(ucrse1.telefone, "#########")
    ?? " NIF:"
    ?? TRANSFORM(ucrse1.ncont, "999999999")
    ? ALLTRIM(uf_gerais_getMacrosReports(2))
    ? "Dir. Tec. "
    ?? LEFT(ALLTRIM(ucrse1.u_dirtec), uf_gerais_getparameter("ADM0000000194", "NUM"))
    ??
    uf_gerais_separadorpos()
    ? PADC("TALAO PARA PAGAMENTO", uf_gerais_getparameter("ADM0000000194", "NUM"), " ")
    uf_gerais_separadorpos()
    ? "Data / Hora:  "
    ?? DATETIME()+(difhoraria*3600)
    uf_gerais_separadorpos()
    ?
    ?? CHR(27)+CHR(97)+CHR(1)
    ? 'Atendimento:'
    ?
    ??? CHR(29)+CHR(104)+CHR(50)+CHR(29)+CHR(72)+CHR(2)+CHR(29)+CHR(119)+CHR(2)+CHR(29)+CHR(107)+CHR(4)+UPPER(ALLTRIM(nratendimento))+CHR(0)
    uf_gerais_textoljpos()
    ? "Data de Emissao:  "
    ?? DATETIME()+(difhoraria*3600)
    uf_gerais_feedcutpos()
 ENDIF
 uf_gerais_setimpressorapos(.F.)
 SET PRINTER TO DEFAULT
ENDPROC
**
