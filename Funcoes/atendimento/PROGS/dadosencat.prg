**
PROCEDURE uf_dadosencat_chama
 LPARAMETERS lnencviaverde
 IF  .NOT. EMPTY(lnencviaverde)
    IF TYPE("DADOSENCAT")=="U"
       DO FORM DADOSENCAT WITH .T.
    ELSE
       dadosencat.show()
    ENDIF
 ELSE
    IF TYPE("DADOSENCAT")=="U"
       DO FORM DADOSENCAT
    ELSE
       dadosencat.show()
    ENDIF
 ENDIF
ENDPROC
**
FUNCTION uf_dadosencat_gravar
 LOCAL lcsql
 STORE '' TO lcsql
 IF EMPTY(ALLTRIM(dadosencat.txtnome.value))
    uf_perguntalt_chama("Existem campos obrigat�rios por preencher.", "OK", "", 32)
    RETURN .F.
 ENDIF
 TEXT TO lcsql TEXTMERGE NOSHOW
		SELECT 
			fl.nome
			,fl.no
			,fl.estab
			,fl.tipo
			,fl.local
			,fl.codpost
			,fl.ncont
			,fl.morada
			,fl.moeda
			,fl.ccusto
			,fl_site.via_verde_user
		from 
			fl (nolock)
			inner join fl_site on fl.no = fl_site.nr_fl and fl.estab = fl_site.dep_fl
		where 
			fl.no = <<DADOSENCAT.txtNo.value>> 
			and fl.estab = <<DADOSENCAT.txtEstab.value>>
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsDadosForn", lcsql)
    uf_perguntalt_chama("OCORREU UM ERRO A LER DADOS DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
    RETURN .F.
 ENDIF
 IF dadosencat.chkencviaverde.tag=="true"
    SELECT ucrsatendst
    IF EMPTY(ucrsatendst.viaverde)
       uf_perguntalt_chama("O produto seleccionado n�o abrangido � por este protocolo. Por favor verifique.", "OK", "", 48)
       RETURN .F.
    ENDIF
    LOCAL lcfilordem, lcnrreceita
    STORE 0 TO lcfilordem
    STORE '' TO lcnrreceita
    lcfilordem = LEFT(astr(fi.lordem), 2)
    SELECT * FROM fi INTO CURSOR uCrsTempFiValidaNrReceita READWRITE
    SELECT ucrstempfivalidanrreceita
    GOTO TOP
    LOCATE FOR LEFT(astr(ucrstempfivalidanrreceita.lordem), 2)=lcfilordem
    IF FOUND()
       IF (LEN(STREXTRACT(ucrstempfivalidanrreceita.design, ':', ' -', 1))<>13 .AND. LEN(STREXTRACT(ucrstempfivalidanrreceita.design, ':', ' -', 1))<>19) .OR. LEFT(ucrstempfivalidanrreceita.design, 4)=='.SEM'
          uf_perguntalt_chama("Deve preencher o n�mero de receita para a venda em que est� inserida esta refer�ncia.", "OK", "", 48)
          RETURN .F.
       ELSE
          lcnrreceita = STREXTRACT(ucrstempfivalidanrreceita.design, ':', ' -', 1)
       ENDIF
    ENDIF
    IF USED("ucrsTempFiValidaNrReceita")
       fecha("ucrsTempFiValidaNrReceita")
    ENDIF
    SELECT fi
 ENDIF
 LOCAL lcstampenc
 lcstampenc = uf_gerais_stamp()
 IF dadosencat.chkencviaverde.tag=="true"
    IF uf_gerais_actgrelha("", "uCrsNrMax", 'select ISNULL((MAX(convert(numeric,obrano))),0) as nr from bo (nolock) where bo.ndos = 3 and YEAR(dataobra) = YEAR(GETDATE())')
       IF RECCOUNT("uCrsNrMax")>0
          lcnrdoc = ucrsnrmax.nr+1
       ENDIF
       fecha("uCrsNrMax")
    ELSE
       uf_perguntalt_chama("O DOCUMENTO [ENCOMENDAS A FORNECEDOR] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.", "OK", "", 48)
       RETURN .F.
    ENDIF
 ELSE
    IF uf_gerais_actgrelha("", "uCrsNrMax", 'select ISNULL((MAX(convert(numeric,obrano))),0) as nr from bo (nolock) where bo.ndos = 2 and YEAR(dataobra) = YEAR(GETDATE())')
       IF RECCOUNT("uCrsNrMax")>0
          lcnrdoc = ucrsnrmax.nr+1
       ENDIF
       fecha("uCrsNrMax")
    ELSE
       uf_perguntalt_chama("O DOCUMENTO [ENCOMENDAS A FORNECEDOR] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.", "OK", "", 48)
       RETURN .F.
    ENDIF
 ENDIF
 IF dadosencat.chkencviaverde.tag=="true"
    lcsqlinsercaoenc = uf_dadosencat_inserecab(lcstampenc, lcnrdoc, .T.)
    lcsqlinsercaoenc = uf_gerais_trataplicassql(lcsqlinsercaoenc)
 ELSE
    lcsqlinsercaoenc = uf_dadosencat_inserecab(lcstampenc, lcnrdoc)
    lcsqlinsercaoenc = uf_gerais_trataplicassql(lcsqlinsercaoenc)
 ENDIF
 IF  .NOT. EMPTY(lcsqlinsercaoenc)
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_gerais_execSql 'Encomendas At', 1, '<<lcSqlInsercaoEnc>>', '', '', '', '', ''
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
       uf_perguntalt_chama("OCORREU UM ERRO A GRAVAR O CABE�ALHO DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: Enc. At.", "OK", "", 16)
       RETURN .F.
    ENDIF
    IF dadosencat.chkenviaencomenda.tag=="true"
       IF dadosencat.chkencviaverde.tag=="true"
          IF EMPTY(ucrsdadosforn.via_verde_user)
             uf_perguntalt_chama("O FORNECEDOR SELECCIONADO N�O EST� CONFIGURADO PARA O ENVIO DE ENCOMENDAS DO TIPO VIA VERDE.", "OK", "", 32)
             RETURN .F.
          ENDIF
          regua(0, 3, "A enviar Encomenda, por favor aguarde.")
          LOCAL viaverdepath, lcwspath, lcidcliente, lcnomejar
          STORE '' TO viaverdepath, lcwspath, lcidcliente, lcnomejar
          SELECT ucrse1
          IF EMPTY(ALLTRIM(ucrse1.id_lt))
             uf_perguntalt_chama("O C�DIGO DE CLIENTE LOGITOOLS N�O EST� CORRETAMENTE PREENCHIDO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
             RETURN .F.
          ENDIF
          lcidcliente = ALLTRIM(ucrse1.id_lt)
          lcnomejar = 'ViaVerdeMed.jar'
          IF  .NOT. FILE(ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\ViaVerdeMed\'+ALLTRIM(lcnomejar))
             uf_perguntalt_chama("O SOFTWARE DE ENVIO N�O EST� CONFIGURADO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
             regua(2)
             RETURN .F.
          ENDIF
          LOCAL lcdefinewstest
          STORE 0 TO lcdefinewstest
          IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.F.
             lcdefinewstest = 0
          ELSE
             lcdefinewstest = 1
          ENDIF
          regua(1, 2, "A enviar Encomenda, por favor aguarde.")
          lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\ViaVerdeMed\'+ALLTRIM(lcnomejar)+' ENVIO '+' "--orderStamp='+ALLTRIM(lcstampenc)+'"'+' "--orderNr='+ALLTRIM(STR(lcnrdoc))+'"'+' "--clID='+lcidcliente+'"'+' "--site='+UPPER(ALLTRIM(mysite))+'"'+' --supplierNr='+ALLTRIM(STR(dadosencat.txtno.value))+' --supplierDep='+ALLTRIM(STR(dadosencat.txtestab.value))+' "--IDReceita='+ALLTRIM(lcnrreceita)+'"'+' --test='+ALLTRIM(STR(lcdefinewstest))
          lcwspath = "javaw -jar "+lcwspath
          owsshell = CREATEOBJECT("WScript.Shell")
          owsshell.run(lcwspath, 1, .T.)
          regua(1, 3, "A verificar resposta do Fornecedor.")
          lcsql = ''
          TEXT TO lcsql TEXTMERGE NOSHOW
					select TOP 1 * FROM via_verde_med WHERE bostamp = '<<ALLTRIM(lcStampEnc)>>' ORDER BY data desc
          ENDTEXT
          IF  .NOT. uf_gerais_actgrelha("", "uCrsAuxResposta", lcsql)
             uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
             regua(2)
             RETURN .F.
          ELSE
             IF RECCOUNT("uCrsAuxResposta")>0
                IF ucrsauxresposta.aceite==.T.
                   lcsql = ''
                   TEXT TO lcsql TEXTMERGE NOSHOW
								UPDATE bo SET logi1 = 1 WHERE bostamp = '<<ALLTRIM(uCrsAuxResposta.bostamp)>>'
                   ENDTEXT
                   IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
                      uf_perguntalt_chama("OCORREU UMA ANOMALIA A ATUALIZAR A ENCOMENDA. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: Enc. At.", "OK", "", 16)
                      RETURN .F.
                   ENDIF
                   uf_perguntalt_chama("Encomenda enviada com sucesso!", "OK", "", 64)
                   
                   
				   SELECT uCrsAuxResposta
	               If(uf_documentos_fecha_encomendas_semProducto(ALLTRIM(uCrsAuxResposta.estado),ALLTRIM(uCrsAuxResposta.bostamp)))
	               		regua(2)
	               		RETURN .F.
	               ENDIF

                   
                   lcsql = ''
                   TEXT TO lcsql TEXTMERGE NOSHOW
								SELECT vv.ref, bi.design,  vv.qt_pedida, vv.qt_entregue FROM via_verde_med_d vv (nolock)
								INNER JOIN bi (nolock) ON vv.ref = bi.ref 
								WHERE id_via_verde_med = '<<ALLTRIM(uCrsAuxResposta.id)>>' 
								AND bi.bostamp = '<<ALLTRIM(uCrsAuxResposta.bostamp)>>'
                   ENDTEXT
                   IF  .NOT. uf_gerais_actgrelha("", "uCrsAuxRespostaDetalhe", lcsql)
                      uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
                      regua(2)
                      RETURN .F.
                   ELSE
                      LOCAL lcinfforn
                      STORE '' TO lcinfforn
                      SELECT ucrsauxrespostadetalhe
                      GOTO TOP
                      SCAN
                         lcinfforn = lcinfforn+CHR(13)+ALLTRIM(ucrsauxrespostadetalhe.ref)+' '+ALLTRIM(ucrsauxrespostadetalhe.design)+' Qtt Pedida '+ALLTRIM(STR(ucrsauxrespostadetalhe.qt_pedida))+' Qtt Entregue '+ALLTRIM(STR(ucrsauxrespostadetalhe.qt_entregue))
                      ENDSCAN
                      uf_perguntalt_chama(LEFT(lcinfforn, 200), "OK", "", 16)
                      regua(2)
                   ENDIF
                ELSE
                   lcsql = ''
                   TEXT TO lcsql TEXTMERGE NOSHOW
								select TOP 2 * FROM via_verde_med_d WHERE id_via_verde_med = '<<ALLTRIM(uCrsAuxResposta.id)>>' 
                   ENDTEXT
                   IF  .NOT. uf_gerais_actgrelha("", "uCrsAuxRespostaDetalhe", lcsql)
                      uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
                      regua(2)
                      RETURN .F.
                   ELSE
                      LOCAL lcinfforn
                      STORE '' TO lcinfforn
                      SELECT ucrsauxrespostadetalhe
                      GOTO TOP
                      SCAN
                         lcinfforn = lcinfforn+CHR(13)+ALLTRIM(ucrsauxrespostadetalhe.response_descr)
                      ENDSCAN
                      uf_perguntalt_chama(LEFT(lcinfforn, 100), "OK", "", 16)
                      regua(2)
                   ENDIF
                ENDIF
             ELSE
                uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
                regua(2)
                RETURN .F.
             ENDIF
             IF USED("uCrsAuxResposta")
                fecha("uCrsAuxResposta")
             ENDIF
             IF USED("uCrsAuxRespostaDetalhe")
                fecha("uCrsAuxRespostaDetalhe")
             ENDIF
          ENDIF
          regua(2)
       ELSE
          LOCAL postalpath
          postalpath = uf_gerais_getparameter('ADM0000000185', 'TEXT')
          IF  .NOT. (RIGHT(postalpath, 1)=="\")
             postalpath = ALLTRIM(postalpath)+'\'
          ENDIF
          postalpath = postalpath+"Postal\Postal.jar"
          IF !FILE('&postalPath')
             uf_perguntalt_chama("O POSTAL N�O EST� A SER ENCONTRADO. ASSIM N�O � POSS�VEL ENVIAR A ENCOMENDA. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 48)
             RETURN .F.
          ENDIF
          postalpath = "javaw -jar "+postalpath+" "+ALLTRIM(STR(dadosencat.txtno.value))+' '+ALLTRIM(STR(dadosencat.txtestab.value))+' '+ALLTRIM(STR(lcnrdoc))+' "'+ALLTRIM(lcstampenc)+'" '+ALLTRIM(STR(ch_userno))+' "'+ALLTRIM(mysite)+'" "'+ALLTRIM(sql_db)+'"'
          owsshell = CREATEOBJECT("WScript.Shell")
          owsshell.run(postalpath, 1, .T.)
          uf_perguntalt_chama("Encomenda criada com sucesso!", "OK", "", 64)
       ENDIF
    ENDIF
    uf_dadosencat_sair()
 ENDIF
ENDFUNC
**
FUNCTION uf_dadosencat_inserecab
 LPARAMETERS lcstampenc, lcnrdoc, lcencviaverde
 LOCAL lcexecutesql
 lcexecutesql = ""
 LOCAL valoriva1, valoriva2, valoriva3, valoriva4, valoriva5, lctotaliva, totalcomiva, totalsemiva, totalnservico, lctotalfinal
 LOCAL totalsemiva1, totalsemiva2, totalsemiva3, totalsemiva4, totalsemiva5, lcqtsum
 LOCAL lcsql, lcdoccont
 LOCAL lcbistamp, lccounts, lcordem, lcqtaltern, lcref, lcqt, lctotalliq, lcvalinsertlin, lcepv
 STORE 0 TO valoriva1, valoriva2, valoriva3, valoriva4, valoriva5, lctotaliva, totalcomiva, totalsemiva, totalnservico, lctotalfinal
 STORE 0 TO totalsemiva1, totalsemiva2, totalsemiva3, totalsemiva4, totalsemiva5, lcqtsum
 STORE 0 TO lcdoccont
 STORE '' TO lcsql
 LOCAL lcdoccode, lcdocnome
 STORE 0 TO lcdoccode
 IF lcencviaverde==.T.
    IF uf_gerais_actgrelha("", "uCrsTsCode", 'select nmdos from ts (nolock) where ndos = 3')
       IF RECCOUNT("uCrsTsCode")>0
          lcdocnome = ucrstscode.nmdos
          lcdoccode = 3
       ELSE
          uf_perguntalt_chama("O DOCUMENTO [ENCOMENDA A FORNECEDOR] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.", "OK", "", 48)
          regua(2)
          RETURN .F.
       ENDIF
       fecha("uCrsTsCode")
    ELSE
       uf_perguntalt_chama("O DOCUMENTO [ENCOMENDA A FORNECEDOR] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.", "OK", "", 48)
       regua(2)
       RETURN .F.
    ENDIF
 ELSE
    IF uf_gerais_actgrelha("", "uCrsTsCode", 'select nmdos from ts (nolock) where ndos = 2')
       IF RECCOUNT("uCrsTsCode")>0
          lcdocnome = ucrstscode.nmdos
          lcdoccode = 2
       ELSE
          uf_perguntalt_chama("O DOCUMENTO [ENCOMENDA A FORNECEDOR] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.", "OK", "", 48)
          regua(2)
          RETURN .F.
       ENDIF
       fecha("uCrsTsCode")
    ELSE
       uf_perguntalt_chama("O DOCUMENTO [ENCOMENDA A FORNECEDOR] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.", "OK", "", 48)
       regua(2)
       RETURN .F.
    ENDIF
 ENDIF
 SELECT fi
 lcqtsum = fi.qtt
 lcref = ALLTRIM(fi.ref)
 lctabiva = fi.tabiva
 lciva = fi.iva
 lctotalfinal = fi.ecusto*fi.qtt
 lcepv = fi.ecusto
 lccodigo = fi.codigo
 lcdesign = fi.design
 lcqtaltern = 0
 lcstock = fi.u_stock
 lccpoc = fi.cpoc
 lcfamilia = fi.familia
 DO CASE
    CASE lctabiva==1
       valoriva1 = valoriva1+(lctotalfinal*(lciva/100))
       totalsemiva1 = totalsemiva1+lctotalfinal
    CASE lctabiva==2
       valoriva2 = valoriva2+(lctotalfinal*(lciva/100))
       totalsemiva2 = totalsemiva2+lctotalfinal
    CASE lctabiva==3
       valoriva3 = valoriva3+(lctotalfinal*(lciva/100))
       totalsemiva3 = totalsemiva3+lctotalfinal
    CASE lctabiva==4
       valoriva4 = valoriva4+(lctotalfinal*(lciva/100))
       totalsemiva4 = totalsemiva4+lctotalfinal
    CASE lctabiva==5
       valoriva5 = valoriva5+(lctotalfinal*(lciva/100))
       totalsemiva5 = totalsemiva5+lctotalfinal
 ENDCASE
 totalcomiva = totalcomiva+(lctotalfinal*(lciva/100+1))
 totalsemiva = totalsemiva+(lctotalfinal)
 totalnservico = totalnservico+lctotalfinal
 lctotaliva = valoriva1+valoriva2+valoriva3+valoriva4+valoriva5
 SELECT ucrsdadosforn
 TEXT TO lcsql TEXTMERGE NOSHOW
			INSERT INTO bo
				(
				bostamp
				,ndos
				,nmdos
				,obrano
				,dataobra
				,U_DATAENTR
				,nome
				,nome2
				,[no]
				,estab
				,morada
				,[local]
				,codpost
				,tipo
				,ncont
				,etotaldeb
				,dataopen
				,boano
				,ecusto
				,etotal
				,moeda
				,memissao
				,origem
				,site
				,vendedor
				,vendnm
				,obs
				,ccusto
				,sqtt14
				,ebo_1tvall
				,ebo_2tvall
				,ebo_totp1
				,ebo_totp2
				,ebo11_bins
				,ebo12_bins
				,ebo21_bins
				,ebo22_bins
				,ebo31_bins
				,ebo32_bins
				,ebo41_bins
				,ebo42_bins
				,ebo51_bins
				,ebo52_bins
				,ebo11_iva
				,ebo12_iva
				,ebo21_iva
				,ebo22_iva
				,ebo31_iva
				,ebo32_iva
				,ebo41_iva
				,ebo42_iva
				,ebo51_iva
				,ebo52_iva
				,ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
				)
			Values  
				(
				'<<ALLTRIM(lcStampEnc)>>', <<lcDocCode>>, '<<lcDocNome>>', <<lcNrDoc>>, convert(varchar,GETDATE(),102), convert(varchar,GETDATE(),102),
				'<<ALLTRIM(uCrsDadosForn.nome)>>', '', <<uCrsDadosForn.no>>, <<uCrsDadosForn.estab>>, '<<ALLTRIM(uCrsDadosForn.morada)>>',
				'<<ALLTRIM(uCrsDadosForn.local)>>', '<<ALLTRIM(uCrsDadosForn.codpost)>>', '<<ALLTRIM(uCrsDadosForn.Tipo)>>', '<<ALLTRIM(uCrsDadosForn.Ncont)>>',
				<<ROUND(totalSemIva,2)>>, convert(varchar,GETDATE(),102), year(getdate()), <<ROUND(totalSemIva,2)>>, <<ROUND(totalComIva,2)>>, '<<ALLTRIM(uCrsDadosForn.moeda)>>', 'EURO', 'BO', '<<ALLTRIM(mySite)>>',
				<<ch_vendedor>>, '<<ALLTRIM(ch_vendnm)>>', 'Documento gerado via Enc. Forn. Atendimento', '<<uCrsDadosForn.ccusto>>',
				<<lcQtSum>>, <<ROUND(totalSemIva,2)>>, <<ROUND(totalSemIva,2)>>, <<ROUND(totalSemIva,2)>>, <<ROUND(totalSemIva,2)>>,
				<<totalSemIva1>>, <<totalSemIva1>>, <<totalSemIva2>>, <<totalSemIva2>>, <<totalSemIva3>>, <<totalSemIva3>>, <<totalSemIva4>>, <<totalSemIva4>>, <<totalSemIva5>>, <<totalSemIva5>>,
				<<valorIva1>>, <<valorIva1>>, <<valorIva2>>, <<valorIva2>>, <<valorIva3>>, <<valorIva3>>, <<valorIva4>>, <<valorIva4>>, <<valorIva5>>, <<valorIva5>>,
				'<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), '<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8) 
				)
 ENDTEXT
 lcexecutesql = lcexecutesql+CHR(13)+CHR(13)+lcsql
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
			Insert into bo2
				(
				bo2stamp, autotipo, pdtipo, etotalciva, ETOTIVA, u_class, u_doccont, armazem,
				ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
				)
			Values
				(
				'<<ALLTRIM(lcStampEnc)>>', 1, 1, <<ROUND(totalComIva,2)>>, <<ROUND(lcTotalIva,2)>>, 'Documento gerado via Enc. Forn. Atendimento', <<lcDocCont>>, <<myArmazem>>,
				'<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), '<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8) 
				)
 ENDTEXT
 lcexecutesql = lcexecutesql+CHR(13)+CHR(13)+lcsql
 lcexecutesqlin = uf_dadosencat_inserelin(lcstampenc, lcdocnome, lcnrdoc, lcref, lcdoccode, lcepv, lcqtsum, totalcomiva, lccodigo, lcdesign, lcqtaltern, lcstock, lciva, lctabiva, lccpoc, lcfamilia)
 RETURN lcexecutesql+lcexecutesqlin
ENDFUNC
**
FUNCTION uf_dadosencat_inserelin
 LPARAMETERS lcstampenc, lcdocnome, lcnrdoc, lcref, lcdoccode, lcepv, lcqt, lctotalliq, lccodigo, lcdesign, lcqtaltern, lcstock, lciva, lctabiva, lccpoc, lcfamilia
 LOCAL lcexecutesql
 lcexecutesql = ""
 lcbistamp = uf_gerais_stamp()
 lcordem = 10000
 TEXT TO lcsql TEXTMERGE NOSHOW
		Insert into bi
			(
			bistamp, bostamp, nmdos, obrano, ref, codigo,
			design, qtt, qtt2, uni2qtt, u_bonus,
			u_stockact, iva, tabiva, armazem, stipo, ndos, cpoc, familia,
			no, nome, local, morada, codpost, ccusto,
			epu, edebito, eprorc, epcusto, ettdeb, ecustoind, edebitoori, u_upc,
			rdata, dataobra, dataopen, resfor, lordem,
			lobs,
			ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
			)
		Values
			(
			'<<ALLTRIM(lcBiStamp)>>', '<<ALLTRIM(lcStampEnc)>>', '<<ALLTRIM(lcDocNome)>>', <<lcNrDoc>>, '<<ALLTRIM(lcRef)>>', '<<ALLTRIM(lcCodigo)>>',
			'<<ALLTRIM(lcDesign)>>', <<lcQt>>, 0, <<lcQtAltern>>, 0,
			<<lcStock>>, <<lciva>>, <<lctabiva>>, <<myArmazem>>, 4, <<lcDocCode>>, <<lccpoc>>, '<<lcfamilia>>',
			<<uCrsDadosForn.No>>, '<<ALLTRIM(uCrsDadosForn.Nome)>>', '<<ALLTRIM(uCrsDadosForn.Local)>>', '<<ALLTRIM(uCrsDadosForn.Morada)>>', '<<ALLTRIM(uCrsDadosForn.CodPost)>>', '<<uCrsDadosForn.ccusto>>',
			<<lcEpv>>, <<lcEpv>>, <<lcEpv>>, <<lcEpv>>, <<lcTotalLiq>>, <<lcEpv>>, <<lcEpv>>, <<lcEpv>>,
			convert(varchar,getdate(),102), convert(varchar,getdate(),102), convert(varchar,getdate(),102), 1, <<lcOrdem>>,
			'',
			'<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), '<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8) 
			)
 ENDTEXT
 lcexecutesql = lcexecutesql+CHR(13)+CHR(13)+lcsql
 TEXT TO lcsql TEXTMERGE NOSHOW
		Insert into bi2
			(
			bi2stamp, bostamp, morada, local, codpost
			)
		Values
			(
			'<<ALLTRIM(lcBiStamp)>>', '<<ALLTRIM(lcStampEnc)>>', '<<ALLTRIM(uCrsDadosForn.Morada)>>', '<<ALLTRIM(uCrsDadosForn.Local)>>', '<<ALLTRIM(uCrsDadosForn.CodPost)>>'
			)
 ENDTEXT
 lcexecutesql = lcexecutesql+CHR(13)+CHR(13)+lcsql
 RETURN lcexecutesql
ENDFUNC
**
PROCEDURE uf_dadosencat_sair
 dadosencat.hide
 dadosencat.release
ENDPROC
**
