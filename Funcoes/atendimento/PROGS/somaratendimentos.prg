**
PROCEDURE uf_somaratendimentos_chama
 IF TYPE("somaratendimentos")=="U"
    uf_somaratendimentos_criacursores()
    DO FORM somarAtendimentos
    somaratendimentos.menu1.adicionaopcao("opcoes", "Op��es", mypath+"\imagens\icons\opcoes_w.png", "", "F1")
    somaratendimentos.menu1.adicionaopcao("pesquisar", "Actualizar", mypath+"\imagens\icons\actualizar_w.png", "uf_somarAtendimentos_pesquisar", "F2")
    somaratendimentos.menu1.adicionaopcao("imprimir", "Imprimir", mypath+"\imagens\icons\imprimir_w.png", "uf_somarAtendimentos_imprimir", "F3")
    somaratendimentos.menu1.adicionaopcao("cima", "Cima", mypath+"\imagens\icons\ponta_seta_up.png", "uf_somaratendimentos_moverLinha WITH .F.", "F4")
    somaratendimentos.menu1.adicionaopcao("baixo", "Baixo", mypath+"\imagens\icons\ponta_seta_down.png", "uf_somaratendimentos_moverLinha WITH .T.", "F5")
    TEXT TO lcsql TEXTMERGE NOSHOW
			SELECT TOP 1 * FROM tpa_map(nolock) where termstamp=(select TOP 1 tstamp from B_Terminal(nolock) where no=<<myTermNo>>)
    ENDTEXT
    uf_gerais_actgrelha("", "uCrsTermTPA", lcsql)
    IF RECCOUNT("uCrsTermTPA")>0 .AND. ucrse1.usatpa=.T.
       somaratendimentos.menu1.adicionaopcao("tpa", "Pagam. TPA", mypath+"\imagens\icons\euro_w.png", "uf_somaratendimentos_pagaTPA", "F6")
    ENDIF
    IF uf_gerais_getUmValor("b_modoPag", "count(*)", "pagamentoExterno = 1") > 0
	    somaratendimentos.menu1.adicionaopcao("pagExterno", "Pag. Externo", mypath+"\imagens\icons\euro_w.png", "uf_faturacaoPagExterno_chama", "F7")
	ENDIF
    somaratendimentos.txtdataini.value = uf_gerais_getdate(DATETIME()+(difhoraria*3600))
    somaratendimentos.txtdatafim.value = uf_gerais_getdate(DATETIME()+(difhoraria*3600))


	somarAtendimentos.menu_opcoes.adicionaopcao("meioPagamento", "Meios de Pagamento","","uf_somarAtendimentos_meiospagamento")
    
    IF uf_gerais_actGrelha("","uc_meiosPagExt", "exec up_vendas_checkModoPagExterno")
      
      SELECT uc_meiosPagExt
      GO TOP

      SCAN

         somarAtendimentos.menu_opcoes.adicionaopcao("pagExt" + ALLTRIM(uc_meiosPagExt.ref), "Pagam." + ALLTRIM(uc_meiosPagExt.design), "", "uf_somarAtendimentos_pagExterno WITH '" + ALLTRIM(uc_meiosPagExt.ref) + "'")

      ENDSCAN

    ENDIF
    
  	somaratendimentos.refresh()
  	
    uf_somaratendimentos_pesquisar()
 ELSE
    somaratendimentos.show
    somaratendimentos.refresh()
 ENDIF
ENDPROC
**
FUNCTION uf_somaratendimentos_criacursores
 IF  .NOT. uf_gerais_actgrelha("", 'uCrsSomarOps', 'exec up_gerais_vendedores')
    uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR [UCRSSOMAROP].", "OK", "", 16)
    RETURN .F.
 ENDIF
 
 SELECT ft
 SELECT ucrssomarops
 LOCATE FOR ucrssomarops.cm==ft.vendedor
 IF  .NOT. uf_gerais_actgrelha("", 'uCrsSomarAtendimentos', [exec up_touch_somarAtendimentos 0,'','',''])
    uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR [UCRSSOMARATENDIMENTOS].", "OK", "", 16)
    RETURN .F.
 ENDIF
ENDFUNC
**
FUNCTION uf_somaratendimentos_pesquisar
 LOCAL lcsql
 STORE '' TO lcsql
 lcdataini = CTOD(uf_gerais_getdate(somaratendimentos.txtdataini.value, "DATA"))
 lcdatafim = CTOD(uf_gerais_getdate(somaratendimentos.txtdatafim.value, "DATA"))
 IF (lcdataini>DATE()) .OR. (lcdatafim>DATE()) .OR. (lcdataini>lcdatafim)
    uf_perguntalt_chama("O INTERVALO DE DATAS N�O � V�LIDO.", "OK", "", 64)
    RETURN .F.
 ENDIF
 IF (lcdatafim-lcdataini>3)
    IF  .NOT. uf_perguntalt_chama("SELECCIONOU UM INTERVALO DE DATAS SUPERIORES A 3 DIAS. TEM A CERTEZA QUE QUER CONTINUAR?", "Sim", "N�o")
       RETURN .F.
    ENDIF
 ENDIF
 mntotal = 2
 regua(0, mntotal, "A PESQUISAR...", .F.)
 regua(0, 1, "A PESQUISAR...", .F.)
 TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_touch_somarAtendimentos <<somaratendimentos.cmbOperador.value>>,'<<uf_gerais_getDate(somaratendimentos.txtDataini.value,"SQL")>>','<<uf_gerais_getDate(somaratendimentos.txtDataFim.value,"SQL")>>', '<<mySite>>'
 ENDTEXT
 uf_gerais_actgrelha("somarAtendimentos.grdSomar", "uCrsSomarAtendimentos", lcsql)
 somaratendimentos.lblresultados.caption = ALLTRIM(STR(RECCOUNT("uCrsSomarAtendimentos")))+" Resultados"
 regua(0, 2, "A PESQUISAR...", .F.)
 somaratendimentos.txttotal.value = 0
 regua(2)
ENDFUNC
**
PROCEDURE uf_somaratendimentos_sair
 somaratendimentos.hide
 somaratendimentos.release
 RELEASE somaratendimentos
 RELEASE myordersomaratendimentos
 IF USED("uCrsSomarAtendimentos")
    fecha("uCrsSomarAtendimentos")
 ENDIF
 IF USED("UCRSSOMAROP")
    fecha("UCRSSOMAROP")
 ENDIF
ENDPROC
**
PROCEDURE uf_somaratendimentos_sel
 LOCAL lctotal, lcpos
 STORE 0 TO lctotal
 SELECT ucrssomaratendimentos
 lcpos = RECNO()
 CALCULATE SUM(ucrssomaratendimentos.total) TO lctotal FOR ucrssomaratendimentos.sel=.T.
 somaratendimentos.txttotal.value = ROUND(lctotal, 2)
 somaratendimentos.txttotal.refresh
 SELECT ucrssomaratendimentos
 TRY
    GOTO lcpos
 CATCH
 ENDTRY
 somaratendimentos.grdsomar.setfocus()
ENDPROC
**
PROCEDURE uf_somaratendimentos_moverlinha
 LPARAMETERS lcpos
 IF lcpos
    somaratendimentos.grdsomar.setfocus()
    KEYBOARD '{PGDN}'
 ELSE
    somaratendimentos.grdsomar.setfocus()
    KEYBOARD '{PGUP}'
 ENDIF
ENDPROC
**
FUNCTION uf_somaratendimentos_imprimir
 LOCAL lccountsel, lcpos, lcno
 STORE 0 TO lccountsel, lcpos
 STORE '' TO lcno
 lcpos = RECNO("uCrsSomarAtendimentos")
 IF uf_gerais_getparameter("ADM0000000126", "BOOL")
    uf_perguntalt_chama("FUNCIONALIDADE N�O � SUPORTADA PARA IMPRESSORAS A6.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF EMPTY(myposprinter)
    uf_perguntalt_chama("A IMPRESSORA N�O EST� A SER DETECTADA.", "OK", "", 16)
    RETURN .F.
 ENDIF
 SELECT ucrssomaratendimentos
 CALCULATE CNT() TO lccountsel FOR ucrssomaratendimentos.sel=.T.
 IF (lccountsel=0)
    uf_perguntalt_chama("N�O EXISTE NENHUM ATENDIMENTO SELECCIONADO PARA IMPRIMIR.", "OK", "", 48)
    SELECT ucrssomaratendimentos
    TRY
       GOTO lcpos
    CATCH
    ENDTRY
    RETURN .F.
 ENDIF
 SELECT ucrssomaratendimentos
 GOTO TOP
 LOCATE FOR ucrssomaratendimentos.sel=.T.
 lcno = ucrssomaratendimentos.numero
 SCAN FOR (ucrssomaratendimentos.sel==.T.) .AND. (ucrssomaratendimentos.numero<>lcno)
    uf_perguntalt_chama("S� PODE IMPRIMIR O TAL�O RESUMO PARA O MESMO CLIENTE.", "OK", "", 48)
    SELECT ucrssomaratendimentos
    TRY
       GOTO lcpos
    CATCH
    ENDTRY
    RETURN .F.
 ENDSCAN
 
 
 **IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
 IF uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + ALLTRIM(myTerm) + "'")

	uv_idImp = uf_gerais_getUmValor("B_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Somar Atendimentos'")
	
	select * from ucrssomaratendimentos where ucrssomaratendimentos.sel=.t. into cursor ucrssomaratendimentosprint readwrite

	uf_imprimirgerais_sendprinter_talao(uv_idImp , .f., .F.)
	
	
 ELSE
 
 
	 IF  .NOT. (uf_gerais_setimpressorapos(.T.))
	    SELECT ucrssomaratendimentos
	    TRY
	       GOTO lcpos
	    CATCH
	    ENDTRY
	    RETURN .F.
	 ENDIF
	 uf_gerais_criarcabecalhopos()
	 uf_gerais_textocjpos()
	 ? PADC("DOCUMENTO SEM VALIDADE FISCAL", uf_gerais_getparameter("ADM0000000194", "NUM"), " ")
	 uf_gerais_textoljpos()
	 uf_gerais_separadorpos()
	 SELECT ucrssomaratendimentos
	 GOTO TOP
	 LOCATE FOR ucrssomaratendimentos.sel=.T.
	 ? "Cliente: "
	 ?? ALLTRIM(ucrssomaratendimentos.nome)+ALLTRIM(ucrssomaratendimentos.numero)
	 uf_gerais_separadorpos()
	 SELECT ucrssomaratendimentos
	 GOTO TOP
	 SCAN FOR ucrssomaratendimentos.sel=.T.
	    uf_gerais_textovermpos()
	    ? "Ref. ao Atendimento: "
	    ?? ALLTRIM(ucrssomaratendimentos.nratend)
	    ? "Valor               : "
	    ?? TRANSFORM(ucrssomaratendimentos.total, "9999999.99")+CHR(27)+CHR(116)+CHR(19)+CHR(213)
	    uf_gerais_resetcorpos()
	    ? "Op: "
	    ?? ALLTRIM(ucrssomaratendimentos.operador)
	    ? "Data: "
	    ?? ALLTRIM(ucrssomaratendimentos.data)
	    ?? "      Hora: "
	    ?? ALLTRIM(ucrssomaratendimentos.hora)
	    uf_gerais_separadorpos()
	 ENDSCAN
	 uf_gerais_textovermpos()
	 ?
	 ? "Total               : "
	 ?? TRANSFORM(somaratendimentos.txttotal.value, "9999999.99")+CHR(27)+CHR(116)+CHR(19)+CHR(213)
	 uf_gerais_resetcorpos()
	 uf_gerais_separadorpos()
	 uf_gerais_textocjpos()
	 ? "Processado por Computador - IVA INCLUIDO"
	 uf_gerais_feedcutpos()
	 uf_gerais_setimpressorapos(.F.)
 ENDIF
 SELECT ucrssomaratendimentos
 TRY
    GOTO lcpos
 CATCH
 ENDTRY
ENDFUNC
**
FUNCTION uf_somaratendimentos_pagatpa
 LOCAL lcnratend, lctot
 STORE '' TO lcnratend
 STORE 0 TO lctot, lctot1
 lctot1 = somaratendimentos.txttotal.value
 TEXT TO lcsql TEXTMERGE NOSHOW
		SELECT TOP 1 * FROM tpa_map(nolock) where termstamp=(select TOP 1 tstamp from B_Terminal(nolock) where no=<<myTermNo>>)
 ENDTEXT
 uf_gerais_actgrelha("", "uCrsTermTPA", lcsql)
 IF RECCOUNT("uCrsTermTPA")>0 .AND. ucrse1.usatpa=.T.
    IF uf_pagamento_pagatpa_somaat(ROUND(lctot1, 2))
       SELECT ucrssomaratendimentos
       GOTO TOP
       SCAN
          IF ucrssomaratendimentos.sel=.T.
             lcnratend = ucrssomaratendimentos.nratend
             lctot = ucrssomaratendimentos.total
             lcsql = ''
            
              
   			 IF  !uf_gerais_validaPagCentralDinheiro()
	             TEXT TO lcsql TEXTMERGE NOSHOW
							update B_pagCentral SET dinheiro=0, mb=0, visa=0, cheque=0, troco=0, evdinheiro=0, epaga1=0, epaga2=<<ROUND(lcTot,2)>>, echtotal=0, evdinheiro_semTroco=0, epaga3=0, epaga4=0, epaga5=0, epaga6=0, udata= dateadd(HOUR, <<difhoraria>>, getdate()) where nrAtend='<<ALLTRIM(lcNratend)>>'
	             ENDTEXT
             ELSE
             	   TEXT TO lcsql TEXTMERGE NOSHOW
						update B_pagCentral SET dinheiro=0, fechado=1, mb=0, visa=0, cheque=0, troco=0, evdinheiro=0, epaga1=0, epaga2=<<ROUND(lcTot,2)>>, echtotal=0, evdinheiro_semTroco=0, epaga3=0, epaga4=0, epaga5=0, epaga6=0, udata= dateadd(HOUR, <<difhoraria>>, getdate()) where nrAtend='<<ALLTRIM(lcNratend)>>'
	             ENDTEXT
             ENDIF
             IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
                uf_perguntalt_chama("N�o foi poss�vel verificar atualizar a forma de pagamento. Por favor contacte o suporte.", "", "OK", 48)
                RETURN .F.
             ELSE
             ENDIF
          ENDIF
          SELECT ucrssomaratendimentos
       ENDSCAN
       uf_perguntalt_chama("A FORMA DE PAGAMENTO DOS DOCUMENTOS SELECIONADOS FOI ATUALIZADA PARA MULTIBANCO", "", "OK", 64)
    ELSE
       IF uf_perguntalt_chama("OCORREU UM ERRO NO PROCESSAMENTO."+CHR(13)+"DESEJA MUDAR NA MESMA A FORMA DE PAGAMENTO DOS DOCUMENTOS SELECIONADOS PARA MULTIBANCO ?", "Sim", "N�o")
          SELECT ucrssomaratendimentos
          GOTO TOP
          SCAN
             IF ucrssomaratendimentos.sel=.T.
                lcnratend = ucrssomaratendimentos.nratend
                lctot = ucrssomaratendimentos.total
                lcsql = ''
                IF  !uf_gerais_validaPagCentralDinheiro()
	             TEXT TO lcsql TEXTMERGE NOSHOW
							update B_pagCentral SET dinheiro=0, mb=0, visa=0, cheque=0, troco=0, evdinheiro=0, epaga1=0, epaga2=<<ROUND(lcTot,2)>>, echtotal=0, evdinheiro_semTroco=0, epaga3=0, epaga4=0, epaga5=0, epaga6=0, udata= dateadd(HOUR, <<difhoraria>>, getdate()) where nrAtend='<<ALLTRIM(lcNratend)>>'
	             ENDTEXT
	             ELSE
	             	   TEXT TO lcsql TEXTMERGE NOSHOW
							update B_pagCentral SET dinheiro=0, fechado=1, mb=0, visa=0, cheque=0, troco=0, evdinheiro=0, epaga1=0, epaga2=<<ROUND(lcTot,2)>>, echtotal=0, evdinheiro_semTroco=0, epaga3=0, epaga4=0, epaga5=0, epaga6=0, udata= dateadd(HOUR, <<difhoraria>>, getdate()) where nrAtend='<<ALLTRIM(lcNratend)>>'
		             ENDTEXT
	             ENDIF
                IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
                   uf_perguntalt_chama("N�o foi poss�vel verificar atualizar a forma de pagamento. Por favor contacte o suporte.", "", "OK", 48)
                   RETURN .F.
                ELSE
                ENDIF
             ENDIF
             SELECT ucrssomaratendimentos
          ENDSCAN
          uf_perguntalt_chama("A FORMA DE PAGAMENTO DOS DOCUMENTOS SELECIONADOS FOI ATUALIZADA PARA MULTIBANCO", "", "OK", 64)
       ELSE
          uf_perguntalt_chama("PROCESSAMENTO CANCELADO", "", "OK", 48)
       ENDIF
    ENDIF
 ELSE
 
 	
 	
    SELECT ucrssomaratendimentos
    GOTO TOP
    SCAN
       IF ucrssomaratendimentos.sel=.T.
          lcnratend = ucrssomaratendimentos.nratend
          lctot = ucrssomaratendimentos.total
          lcsql = ''
          TEXT TO lcsql TEXTMERGE NOSHOW
					update B_pagCentral set dinheiro=0, mb=0, visa=0, cheque=0, troco=0, evdinheiro=0, epaga1=0, epaga2=<<ROUND(lcTot,2)>>, echtotal=0, evdinheiro_semTroco=0, epaga3=0, epaga4=0, epaga5=0, epaga6=0, udata= dateadd(HOUR, <<difhoraria>>, getdate()) where nrAtend='<<ALLTRIM(lcNratend)>>'
          ENDTEXT
          IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
             uf_perguntalt_chama("N�o foi poss�vel verificar atualizar a forma de pagamento. Por favor contacte o suporte.", "", "OK", 48)
             RETURN .F.
          ELSE
             uf_perguntalt_chama("A FORMA DE PAGAMENTO DOS DOCUMENTOS SELECIONADOS FOI ATUALIZADA PARA MULTIBANCO", "", "OK", 48)
          ENDIF
       ENDIF
    ENDSCAN
 ENDIF
 
 
 
ENDFUNC
**

FUNCTION  uf_somarAtendimentos_meiospagamento
 
 
 
   SELECT * FROM uCrsSomarAtendimentos WITH (BUFFERING = .T.) WHERE uCrsSomarAtendimentos.sel INTO CURSOR uc_selAtend 

   SELECT uc_selAtend
   GO TOP

   IF RECCOUNT("uc_selAtend") = 0
      REGUA(2)
      RETURN .F.
   ENDIF

   SELECT DISTINCT no FROM uc_selAtend ORDER BY no INTO CURSOR uc_countNoSelAtend

   IF RECCOUNT("uc_countNoSelAtend") > 1
      uf_perguntalt_chama("N�o pode somar atendimentos para clientes diferentes.", "", "OK", 48)
      REGUA(2)
      RETURN .F.
   ENDIF

   FECHA("uc_countNoSelAtend")

   SELECT uc_selAtend
   GO TOP

   SCAN

      IF uf_gerais_getUmValor("b_pagCentral_ext", "count(*)", "nrAtend = '" + ALLTRIM(uc_selAtend.nrAtend) + "'") > 0
         uf_perguntalt_chama("N�o pode somar o Atendimento nr� " + ALLTRIM(uc_selAtend.nrAtend) + " pois este j� tem um pagamento externo associado.", "", "OK", 48)
         REGUA(2)
         RETURN .F.
      ENDIF

   ENDSCAN

 
 
 
	SELECT uc_selAtend
	GO TOP
   	uf_alterameiospag_chama('','', 'uc_selAtend')

ENDFUNC


FUNCTION uf_somarAtendimentos_pagExterno
   LPARAMETERS uv_modoPag

   IF EMPTY(uv_modoPag)
      REGUA(2)
      RETURN .F.
   ENDIF

   IF USED("uc_dadosPagExterno" + ALLTRIM(uv_modoPag))
      FECHA("uc_dadosPagExterno" + ALLTRIM(uv_modoPag))
   ENDIF

   SELECT * FROM uCrsSomarAtendimentos WITH (BUFFERING = .T.) WHERE uCrsSomarAtendimentos.sel INTO CURSOR uc_selAtend 

   SELECT uc_selAtend
   GO TOP

   IF RECCOUNT("uc_selAtend") = 0
      REGUA(2)
      RETURN .F.
   ENDIF

   SELECT DISTINCT no FROM uc_selAtend ORDER BY no INTO CURSOR uc_countNoSelAtend

   IF RECCOUNT("uc_countNoSelAtend") > 1
      uf_perguntalt_chama("N�o pode somar atendimentos para clientes diferentes.", "", "OK", 48)
      REGUA(2)
      RETURN .F.
   ENDIF

   FECHA("uc_countNoSelAtend")

   SELECT uc_selAtend
   GO TOP

   SCAN

      IF uf_gerais_getUmValor("b_pagCentral_ext", "count(*)", "nrAtend = '" + ALLTRIM(uc_selAtend.nrAtend) + "'") > 0
         uf_perguntalt_chama("N�o pode somar o Atendimento nr� " + ALLTRIM(uc_selAtend.nrAtend) + " pois este j� tem um pagamento externo associado.", "", "OK", 48)
         REGUA(2)
         RETURN .F.
      ENDIF

   ENDSCAN

   LOCAL uv_userName, uv_userPass, uv_entityType, uv_teste, uv_token, uv_sql, uv_curPagExterno, uv_totSoma, uv_operationID, uv_email, uv_tlmvl, uv_desc, uv_duracao, uv_modoPagCampo

   uv_modoPagCampo = uf_gerais_getUmValor("b_modoPag", "REPLACE((REPLACE(campoFact, 'ft2.', '')), 'ft.', '')", "ref = '" + ALLTRIM(uv_modoPag) + "'")

   REGUA(0, RECCOUNT("uc_selAtend"), "A gerar pagamento...")

   IF EMPTY(uv_modoPagCampo)
      REGUA(2)
      RETURN .F.
   ENDIF

   SELECT uc_selAtend
   GO TOP

   SUM ROUND(uc_selAtend.total, 2) TO uv_totSoma

   SELECT uc_selAtend
   GO TOP

   uf_pagExterno_chama(ALLTRIM(uv_modoPag), uc_selAtend.no, uc_selAtend.estab)

   IF !USED("uc_dadosPagExterno" + ALLTRIM(uv_modoPag))
      REGUA(2)
      RETURN .F.
   ENDIF

   uv_curPagExterno = "uc_dadosPagExterno" + ALLTRIM(uv_modoPag)

   uv_email = &uv_curPagExterno..email
   uv_tlmvl = &uv_curPagExterno..tlmvl
   uv_desc = &uv_curPagExterno..descricao
   uv_duracao = &uv_curPagExterno..duracao

   uv_userName = uf_gerais_getParameter_site("ADM0000000187", "TEXT", mySite)
   uv_userPass = uf_gerais_getParameter_site("ADM0000000188", "TEXT", mySite)
   uv_entityType = uf_gerais_getParameter_site("ADM0000000189", "TEXT", mySite)

   IF EMPTY(uv_userName) OR EMPTY(uv_userPass) OR EMPTY(uv_entityType)
      REGUA(2)
      RETURN .F.
   ENDIF

   IF !uf_gerais_getparameter("ADM0000000227", "BOOL")
    	uv_teste = 0
 	ELSE
    	uv_teste = 1
 	ENDIF   
 	
   local lcAtendAsStr
   lcAtendAsStr = ''
   select uc_selAtend
   GO TOP
   SCAN
   		lcAtendAsStr = lcAtendAsStr  +  ALLTRIM(uc_selAtend.nrAtend) + ";" 
   ENDSCAN
   
   if(!EMPTY(lcAtendAsStr))
   		lcAtendAsStr = SUBSTR(lcAtendAsStr, 1, 240)
   ENDIF
   		

   uv_token = uf_gerais_stamp()

   uf_gerais_actGrelha("", "uc_configPagExterno","SELECT * FROM b_modoPag(nolock) WHERE ref = '" + ALLTRIM(uv_modoPag) + "'")
	
   SELECT uc_configPagExterno

   TEXT TO uv_sql TEXTMERGE NOSHOW

      EXEC up_vendas_requestPagExterno
         '<<ALLTRIM(uv_token)>>',
         '<<ALLTRIM(uv_userName)>>',
         '<<ALLTRIM(uv_userPass)>>',
         1,
         <<ASTR(uc_configPagExterno.tipoPagExt)>>,
         '<<ALLTRIM(uc_configPagExterno.tipoPagExtDesc)>>',
         '<<ALLTRIM(uc_configPagExterno.receiverID)>>',
         '<<ALLTRIM(uc_configPagExterno.receiverName)>>',
         '<<ALLTRIM(uv_entityType)>>',
         <<ASTR(uv_totSoma,14,2)>>,
         '<<ALLTRIM(uv_email)>>',
         '<<ALLTRIM(uv_tlmvl)>>',
         '<<ALLTRIM(uv_desc)>>',
         '<<ALLTRIM(mySite)>>',
         <<ASTR(uv_teste)>>,
         <<ASTR(VAL(uv_duracao))>>,
         '<<ALLTRIM(m_chinis)>>',
         '',
         '<<ALLTRIM(lcAtendAsStr)>>'
         
   ENDTEXT

   IF !uf_gerais_actGrelha("","",uv_sql)
      REGUA(2)
      RETURN .F.
   ENDIF

   LOCAL lcNomeJar, LcWsPath, lcWsDir, lcAdd  

   lcNomeJar = "ltsPaymentMethods.jar"
   LcWsPath  = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsPaymentMethods\'+ALLTRIM(lcNomeJar)+' ' +' --TOKEN='+ALLTRIM(uv_token) +' --IDCL='+ALLTRIM(uCrsE1.id_lt)
   lcWsDir	  =  ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsPaymentMethods\'
   lcAdd  	  = "&"
               
   LcWsPath  = "cmd /c cd /d "+lcWsDir+" "+lcAdd+lcAdd+" javaw -jar " +LcWsPath 
   owsshell  = CREATEOBJECT("WScript.Shell")
   owsshell.run(lcwspath, 0, .T.) 
   
   IF uv_teste = 1
      _CLIPTEXT = lcwspath
   ENDIF

   IF !uf_gerais_actGrelha("","uc_pagExternoResp", "SELECT * FROM paymentMethodResponse(nolock) where token = '" + ALLTRIM(uv_token) + "'") 
      REGUA(2)
      RETURN .F.	
   ENDIF

   select uc_pagExternoResp
   
   IF uc_pagExternoResp.statusID <> 200 OR uc_pagExternoResp.statusID <> 201

      uf_perguntalt_chama(ALLTRIM(LEFT(uc_pagExternoResp.statusDesc,254)), "OK", "", 16)
      REGUA(2)
      RETURN .F.

   ENDIF

   IF EMPTY(uc_pagExternoResp.operationID)
      REGUA(2)
      RETURN .F.
   ENDIF

   uv_operationID = uc_pagExternoResp.operationID
   

   SELECT uc_selAtend
   GO TOP
	
	
	
   SCAN

      REGUA(1, RECNO(), "A gerar pagamento...")

      uv_token = uf_gerais_stamp()

      SELECT uc_configPagExterno

      TEXT TO uv_sql TEXTMERGE NOSHOW

         INSERT INTO b_pagCentral_ext (token,nrAtend,operationId,typePayment,typePaymentDesc,status,statusDesc,
                           statePaymentID,statePaymentIDDesc,receiverid,receivername,site,ousrinis,ousrdata,usrinis,usrdata, limitDate)
                  VALUES (
                           '<<ALLTRIM(uv_token)>>',
                           '<<ALLTRIM(uc_selAtend.nrAtend)>>',
                           '<<ALLTRIM(uc_pagExternoResp.operationId)>>',
                           <<ASTR(uc_configPagExterno.tipoPagExt)>>,
                           '<<ALLTRIM(uc_configPagExterno.tipoPagExtDesc)>>',
                           '',
                           '',
                           0,
                           '',
                           '<<ALLTRIM(uc_configPagExterno.receiverID)>>',
                           '<<ALLTRIM(uc_configPagExterno.receiverName)>>',
                           '<<ALLTRIM(mySite)>>',
                           '<<ALLTRIM(m_chinis)>>',
                           GETDATE(),
                           '<<ALLTRIM(m_chinis)>>',
                           GETDATE(),
                           '<<IIF(VAL(uv_duracao) = 0, "19000101", uf_gerais_getDate((DATE() + VAL(uv_duracao)+ 1),"SQL")))>>'
                     )

      ENDTEXT

      IF !uf_gerais_actGrelha("","",uv_sql)
         REGUA(2)
         RETURN .F.
      ENDIF

      TEXT TO uv_sql TEXTMERGE NOSHOW
            UPDATE 
               b_pagcentral 
            SET 
               evdinheiro = 0
               ,epaga2 = <<IIF(UPPER(ALLTRIM(uv_modoPagCampo)) = 'EPAGA2', 'b_pagCentral.total', '0')>>
               ,epaga1 = <<IIF(UPPER(ALLTRIM(uv_modoPagCampo)) = 'EPAGA1', 'b_pagCentral.total', '0')>>
               ,echtotal = 0
               ,evdinheiro_semTroco = 0
               ,etroco = 0
               ,mb = 0
               ,visa = 0
               ,cheque = 0
               ,epaga3 =  <<IIF(UPPER(ALLTRIM(uv_modoPagCampo)) = 'EPAGA3', 'b_pagCentral.total', '0')>>
               ,epaga4 =  <<IIF(UPPER(ALLTRIM(uv_modoPagCampo)) = 'EPAGA4', 'b_pagCentral.total', '0')>>
               ,epaga5 =  <<IIF(UPPER(ALLTRIM(uv_modoPagCampo)) = 'EPAGA5', 'b_pagCentral.total', '0')>>
               ,epaga6 =  <<IIF(UPPER(ALLTRIM(uv_modoPagCampo)) = 'EPAGA6', 'b_pagCentral.total', '0')>>
            where 
               nrAtend = '<<uc_selAtend.nrAtend>>'
      ENDTEXT

      IF !uf_gerais_actgrelha("", "", uv_sql)
         uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR OS MEIOS DE PAGAMENTO."+CHR(13)+"POR FAVOR CONTACTE O SUPORTE.", "OK", "", 48)
         REGUA(2)
         RETURN .F.
      ENDIF

      SELECT uc_selAtend
   ENDSCAN
	

	
   REGUA(2)

   uf_perguntalt_chama("Pagamento gerado com sucesso.", "OK", "", 64)

   RETURN .T.

ENDFUNC


