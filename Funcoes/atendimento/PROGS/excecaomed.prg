**
FUNCTION uf_excecaomed_chama
 IF TYPE("EXCECAOMED")<>"U"
    uf_excecaomed_sair()
    uf_atendimento_painel_excecao()
 ENDIF
 IF  .NOT. USED("Fi")
    RETURN .F.
 ENDIF
 IF  .NOT. USED("Ft")
    RETURN .F.
 ENDIF
 IF TYPE("EXCECAOMED")=="U"
    DO FORM EXCECAOMED
 ELSE
    excecaomed.show()
 ENDIF
ENDFUNC
**
FUNCTION uf_excecaomed_menu
 IF TYPE("EXCECAOMED")=="U"
    RETURN .F.
 ENDIF
 excecaomed.menu1.estado("", "", "Aplicar", .T.)
 excecaomed.menu1.adicionaopcao("subir", "Subir", mypath+"\imagens\icons\pg_up_b.png", "uf_gerais_MovePage with .t.,'excecaomed.pageframe1.Page1.grid1','uCrsExcecaoPanel'", "05")
 excecaomed.menu1.adicionaopcao("descer", "Descer", mypath+"\imagens\icons\pg_down_b.png", "uf_gerais_MovePage with .f.,'excecaomed.pageframe1.Page1.grid1','uCrsExcecaoPanel'", "24")
 excecaomed.menu1.adicionaopcao("vazio", "Vazio", mypath+"\imagens\icons\unchecked_w.png", "uf_excecaoMed_Vazio", "V")
 IF (uf_excecaomed_validajusttodas())
    excecaomed.menu1.adicionaopcao("justTodas", "Just.Todas", mypath+"\imagens\icons\unchecked_w.png", "uf_excecaoMed_JustTodasMarca", "L")
 ENDIF
ENDFUNC
**
PROCEDURE uf_excecaomed_justtodasmarca
 IF excecaomed.menu1.justtodas.tag=="true"
    excecaomed.menu1.justtodas.tag = "false"
    excecaomed.menu1.justtodas.img.picture = mypath+"\imagens\icons\unchecked_w.png"
 ELSE
    excecaomed.menu1.justtodas.tag = "true"
    excecaomed.menu1.justtodas.img.picture = mypath+"\imagens\icons\checked_w.png"
 ENDIF
ENDPROC
**
FUNCTION uf_excecaomed_validajusttodas
 IF ( .NOT. USED("fi2") .OR.  .NOT. USED("fi"))
    RETURN .F.
 ENDIF
 LOCAL lcposfi
 SELECT fi
 lcposfi = RECNO("fi")
 LOCAL lcerroexiste
 lcerroexiste = .F.
 SELECT fi
 GOTO TOP
 SCAN
    LOCAL lccode
    lccode = ''
    IF TYPE("fi.codigoDemResposta")<>'U'
       lccode = ALLTRIM(fi.codigodemresposta)
       IF (uf_atendimento_devolvejusterrosvalid(lccode) .AND. EMPTY(lcerroexiste))
          lcerroexiste = .T.
       ENDIF
    ENDIF
    SELECT fi
 ENDSCAN
 SELECT fi
 TRY
    GOTO lcposfi
 CATCH
    GOTO TOP
 ENDTRY
 RETURN lcerroexiste
ENDFUNC
**
PROCEDURE uf_excecaomed_gravar
 IF (USED("uCrsExcecaoPanel") .AND. USED("fi"))
    LOCAL lcposfi
    SELECT fi
    lcposfi = RECNO("fi")
    LOCAL lcfistamp
    SELECT fi
    lcfistamp = ALLTRIM(fi.fistamp)
    LOCAL lcmultijust, lcmultiexcecao
    lcmultijust = .F.
    lcmultiexcecao = .F.
    

    
    
    SELECT ucrsexcecaopanel
    GOTO TOP
    SCAN &&FOR !EMPTY(ucrsexcecaopanel.sel)
       LOCAL lcselexcecao, lccodexcecao, lctipoexcecao, lcdescrexcecao, lbMultSel
       lcselexcecao   = ucrsexcecaopanel.sel
       lccodexcecao   = ALLTRIM(ucrsexcecaopanel.code)
       lctipoexcecao  = ALLTRIM(ucrsexcecaopanel.type)
       lcdescrexcecao =  ALLTRIM(ucrsexcecaopanel.descr)
       lbMultSel      = ucrsexcecaopanel.multiSel
   
       DO CASE
          CASE lctipoexcecao=="COMPART"
             uf_excecaomed_compartexececao(lcselexcecao)
          CASE lctipoexcecao=="INDISPONIVEL" 
             uf_excecaomed_indisponivel(lcselexcecao)
          CASE lctipoexcecao=="EXCECAO"
             IF (EMPTY(lcmultiexcecao))
                uf_excecaomed_painel_set_excecao(lcselexcecao, lccodexcecao)
             ENDIF
             IF ( .NOT. EMPTY(lcselexcecao))
                lcmultiexcecao = .T.
             ENDIF
                    
       ENDCASE
    ENDSCAN
    
    
    uf_excecaomed_justexececao(lcfistamp)   
    
    
    
    SELECT fi
    TRY
       GOTO lcposfi
    CATCH
       GOTO TOP
    ENDTRY
 ENDIF
 uf_excecaomed_preenchecamposlivres()
 uf_excecaomed_sair()
ENDPROC
**
PROCEDURE uf_excecaomed_vazio
 IF (USED("uCrsExcecaoPanel"))
    UPDATE uCrsExcecaoPanel SET sel = .F.
 ENDIF
 SELECT ucrsexcecaopanel
 GOTO TOP
 excecaomed.pageframe1.page1.grid1.refresh
ENDPROC
**
PROCEDURE uf_excecaomed_validmultisel
 LPARAMETERS lcselexcecao, lctipoexcecao, lcmultiview, lcstamp, lcMultiSel
 IF  !EMPTY(lcmultiview)
    IF  !EMPTY(lcselexcecao) AND EMPTY(lcMultiSel) 
       UPDATE uCrsExcecaoPanel SET sel = .F. WHERE type=lctipoexcecao .AND. stamp<>lcstamp
    ENDIF
 ENDIF
ENDPROC
**
PROCEDURE uf_excecaomed_orderdesign
 uf_gerais_ordenagrelha("excecaoMed.pageframe1.page1.grid1", "uCrsExcecaoPanel", "descr", myorderexcecaomed)
 IF myorderexcecaomed
    myorderexcecaomed = .F.
 ELSE
    myorderexcecaomed = .T.
 ENDIF
ENDPROC
**
PROCEDURE uf_excecaomed_sair
 fecha("uCrsExcecaoPanel")
 CLEAR CLASS dropdowncontainer
 excecaomed.hide
 excecaomed.release
 RELEASE myorderexcecaomed
 IF (uf_atendimento_painelatendimento())
    atendimento.grdatend.setfocus()
 ENDIF
ENDPROC
**
PROCEDURE uf_excecaomed_compartexececao
 LPARAMETERS lcsel
 LOCAL lccod
 lccod = ""
 LOCAL lcposfi
 SELECT fi
 lcposfi = RECNO("fi")
 SELECT fi
 IF lcsel==.F.
    REPLACE fi.u_comp WITH .F.
    REPLACE fi.u_diploma WITH ''
    REPLACE fi.nccod WITH ''
    REPLACE fi.ndoc WITH 0
    REPLACE fi.marcada WITH .F.
    REPLACE fi.comp_tipo WITH 0
    REPLACE fi.comp WITH 0
    REPLACE fi.comp_diploma WITH 0
    myremovecompart = .T.
    uf_eventoactvalores()
    IF UPPER(mypaisconfsoftw)=='PORTUGAL'
       WITH atendimento.pgfdados.page2
          .btndiploma.label1.caption = 'APLICAR DIPLOMA'
          .btndiploma.backcolor = RGB(42, 143, 154)
       ENDWITH
    ENDIF
 ELSE
    lccod = uf_atendimento_verificacompart( .NOT. uf_atendimento_painelatendimento())
    IF  .NOT. EMPTY(lccod)
       REPLACE fi.u_comp WITH .T.
       uf_eventoactvalores()
    ENDIF
 ENDIF
 SELECT fi
 TRY
    GOTO lcposfi
 CATCH
    GOTO TOP
 ENDTRY
ENDPROC
**
PROCEDURE uf_excecaomed_justexececao
 LPARAMETERS lcfistamp
 LOCAL lcposfitemp, lcJustCodeStr, lcJustDescrStr 
 STORE '' TO lcJustCodeStr, lcJustDescrStr 
 
 
 
 SELECT fi
 lcposfitemp = RECNO("fi")
 

 
 lcDelimiter  = ' | '
 

 
 
 FECHA("ucsTempFi2Just")
 FECHA("ucrsJustCodeStr")
 FECHA("ucrsJustDescrStr")

*!*	 
*!*	 SELECT justificacao_tecnica_cod, justificacao_tecnica_descr   FROM fi2  WHERE fistamp=lcfistamp INTO CURSOR ucsTempFi2Just READWRITE
*!*	 
*!*	 if(USED("ucsTempFi2Just"))
*!*	 	 SELECT ucsTempFi2Just 
*!*	 	 GO TOP
*!*		 lcJustCodeStr = ALLTRIM(justificacao_tecnica_cod)
*!*		 lcJustDescrStr = ALLTRIM(justificacao_tecnica_descr)
*!*		 uf_gerais_stringToCursor(lcJustCodeStr, lcDelimiter   , "ucrsJustCodeStr")
*!*		 uf_gerais_stringToCursor(lcJustDescrStr , lcDelimiter   , "ucrsJustDescrStr")

*!*	 ENDIF
*!*	 
 
  **limpar erros se alguma selecionada
 SELECT ucrsexcecaopanel
 GO TOP
 SCAN FOR  uf_gerais_compStr(ALLTRIM(ucrsexcecaopanel.type), "JUSTIFICAODEM")
	 if(ucrsexcecaopanel.sel)
	 	 UPDATE fi SET codigodemresposta = '', tipoerro = 0 WHERE fistamp=lcfistamp	 	
	 ENDIF 	 	
 ENDSCAN
 
 ** volta a construir
 UPDATE fi2 SET justificacao_tecnica_cod = '', justificacao_tecnica_descr = '' WHERE fistamp=lcfistamp 
 SELECT ucrsexcecaopanel
 GO TOP
 SCAN FOR  uf_gerais_compStr(ALLTRIM(ucrsexcecaopanel.type), "JUSTIFICAODEM") OR  uf_gerais_compStr(ALLTRIM(ucrsexcecaopanel.type), "JUSTIFICAOPEMH") 
	 if(ucrsexcecaopanel.sel)
	 	lcJustCodeStr = lcJustCodeStr + Alltrim(ucrsexcecaopanel.code) + lcDelimiter
	 	lcJustDescrStr  = lcJustDescrStr  + Alltrim(ucrsexcecaopanel.descr) + lcDelimiter 
	 ENDIF 	 	
 ENDSCAN
 
 
 

  	
 if(LEN(lcJustCodeStr)>0)
 	lcJustCodeStr = uf_gerais_removeLastSubstring(lcJustCodeStr,lcDelimiter)
 ENDIF
 if(LEN(lcJustDescrStr  )>0)
 	lcJustDescrStr  = uf_gerais_removeLastSubstring(lcJustDescrStr ,lcDelimiter)
 ENDIF
 
 UPDATE fi2 SET justificacao_tecnica_cod = ALLTRIM(lcJustCodeStr), justificacao_tecnica_descr = ALLTRIM(lcJustDescrStr) WHERE fistamp=lcfistamp

 
 IF (VARTYPE(excecaomed.menu1.justtodas)<>'U')
   IF (excecaomed.menu1.justtodas.tag="true")
	  SELECT fi
	  GOTO TOP
	  SELECT fi2
	  GOTO TOP
	  UPDATE fi2 FROM fi2 INNER JOIN fi ON ALLTRIM(fi.fistamp)=ALLTRIM(fi2.fistamp) SET justificacao_tecnica_cod = ALLTRIM(lcJustCodeStr), justificacao_tecnica_descr = ALLTRIM(lcJustDescrStr) WHERE fi.codigodemresposta IN ('C022', 'C021', 'C023', 'C009','D146')
	  UPDATE fi SET codigodemresposta = '', tipoerro = 0 WHERE codigodemresposta IN ('C022', 'C021', 'C023', 'C009','D146')
    ENDIF
 ENDIF



 FECHA("ucsTempFi2Just")
 FECHA("ucrsJustCodeStr")
 FECHA("ucrsJustDescrStr")
 
 SELECT fi
 TRY
    GOTO lcposfitemp
 CATCH
    GOTO TOP
 ENDTRY
 

 
 
ENDPROC
**
PROCEDURE uf_excecaomed_justaplicatodaslinhas
 LPARAMETERS lcjustcode, lcjustdescr
 LOCAL lcjustall
 lcjustall = "true"
 LOCAL lcposfi
 IF (USED("fi"))
    SELECT fi
    lcposfi = RECNO("fi")
    IF ( .NOT. EMPTY(lcjustall) .AND. lcjustall=="true")
       SELECT fi
       GOTO TOP
       SCAN FOR  .NOT. EMPTY(fi.dem) .AND.  .NOT. EMPTY(ALLTRIM(fi.id_dispensa_eletronica_d))
          LOCAL lcfistamp
          lcfistamp = ALLTRIM(fi.fistamp)
          SELECT fi2
          GOTO TOP
          SCAN FOR ALLTRIM(fi2.fistamp)==ALLTRIM(lcfistamp)
             REPLACE fi2.justificacao_tecnica_cod WITH ALLTRIM(lcjustcode)
             REPLACE fi2.justificacao_tecnica_descr WITH ALLTRIM(lcjustdescr)
             SELECT fi2
          ENDSCAN
          SELECT fi
       ENDSCAN
    ENDIF
    SELECT fi
    TRY
       GOTO lcposfi
    CATCH
       GOTO TOP
    ENDTRY
 ENDIF
ENDPROC
**
FUNCTION uf_excecaomed_painel_set_excecao
 LPARAMETERS lcsel, lccodesel
 LOCAL lcexcecao, lccod, lcbackoffice
 LOCAL lcposfi
 SELECT fi
 lcposfi = RECNO("fi")
 IF UPPER(mypaisconfsoftw)=='PORTUGAL' .AND. USED("fi")
    lccod = uf_atendimento_verificacompart( .NOT. uf_atendimento_painelatendimento())
    IF EMPTY(ALLTRIM(lccod))
       RETURN .F.
    ENDIF
    DO CASE
       CASE ALLTRIM(lccodesel)='EA'
          lcexcecao = 'A'
       CASE ALLTRIM(lccodesel)='EB'
          lcexcecao = 'B'
       CASE ALLTRIM(lccodesel)='EC1'
          lcexcecao = 'C1'
       CASE ALLTRIM(lccodesel)='EC2'
          lcexcecao = 'C2'
       OTHERWISE
          lcexcecao = ''
    ENDCASE
    IF (EMPTY(lcsel))
       lcexcecao = ''
    ENDIF
    SELECT fi
    REPLACE fi.lobs3 WITH ALLTRIM(lcexcecao)
    IF (uf_atendimento_painelatendimento() .AND. lcexcecao=='C2')
       SELECT fi
       uf_principioactivo_chama(ALLTRIM(fi.ref), ALLTRIM(fi.design), ALLTRIM(atendimento.pgfdados.page2.txtcnpem.value), "CNPEM")
    ENDIF
    SELECT fi
    IF EMPTY(ALLTRIM(fi.lobs3))
       SELECT fi
       SELECT ucrsatendst
       DO CASE
          CASE ucrsatendst.grphmgcode=='' .OR. ucrsatendst.grphmgcode=='GH0000'
             REPLACE fi.opcao WITH 'I'
          CASE fi.u_epvp>fi.pvpmaxre
             REPLACE fi.opcao WITH 'S'
          OTHERWISE
             REPLACE fi.opcao WITH 'N'
       ENDCASE
    ENDIF
    SELECT fi
    TRY
       GOTO lcposfi
    CATCH
       GOTO TOP
    ENDTRY
 ENDIF
ENDFUNC
**
FUNCTION uf_excecaomed_indisponivel
 LPARAMETERS lcsel
 LOCAL lcreceita, lcposfi, lcfilordemcab, lcreceita, lcref, lcfistamp, lcaddremove, lcadd
 IF (USED("fi"))
    LOCAL lcposfi
    SELECT fi
    lcposfi = RECNO("fi")
    SELECT fi
    lcref = ALLTRIM(fi.ref)
    lcfistamp = ALLTRIM(fi.fistamp)
    
    
    lcSqlIndisponibilidade = ""
    TEXT TO lcSqlIndisponibilidade TEXTMERGE NOSHOW
			exec up_verifica_indisponibilidade '<<ALLTRIM(lcRef)>>'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "ucrVerfIndiponibilidade", lcSqlIndisponibilidade )
       uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [ucrVerfIndiponibilidade]. Por favor reinicie o Software.", "OK", "", 16)
       RETURN .F.
    ENDIF
   
    IF (ucrVerfIndiponibilidade.estado == .F. AND !EMPTY(lcsel))
   		uf_perguntalt_chama(ucrVerfIndiponibilidade.msg,"OK","",16)
    	RETURN .F.
    ENDIF
    fecha("ucrVerfIndiponibilidade")
    
    IF fi.lordem<>0
       lcfilordemcab = LEFT(astr(fi.lordem), 2)+REPLICATE('0', LEN(astr(fi.lordem))-2)
    ENDIF
    SELECT fi
    GOTO TOP
    LOCATE FOR lordem==VAL(lcfilordemcab)
    IF FOUND()
       lcreceita = ALLTRIM(STREXTRACT(fi.design, ':', ' -', 1, 0))
    ELSE
       lcreceita = ''
    ENDIF
    IF (LEN(lcreceita)<>19)
       lcreceita = ''
    ENDIF
    SELECT fi
    TRY
       GOTO lcposfi
    CATCH
       GOTO TOP
    ENDTRY
    IF ( .NOT. EMPTY(lcsel))
       lcadd = 1
    ELSE
       lcadd = 0
    ENDIF
    lcsql = ""
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_lts_missingProducts_add_remove  <<lcAdd>>, '<<ALLTRIM(lcReceita)>>','<<ALLTRIM(nrAtendimento)>>', '<<ALLTRIM(m_chinis)>>', '<<ALLTRIM(lcRef)>>',<<mySite_Nr>>
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "ucrTempIndiponivel", lcsql)
       uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [uCrsPrecosValidos]. Por favor reinicie o Software.", "OK", "", 16)
       RETURN .F.
    ENDIF
    IF (lcsel==.T.)
       UPDATE fi2 SET indisponivel = .T. WHERE fistamp=lcfistamp
    ELSE
       UPDATE fi2 SET indisponivel = .F. WHERE fistamp=lcfistamp
    ENDIF
    fecha("ucrTempIndiponivel")
 ENDIF
ENDFUNC
**
FUNCTION uf_excecaomed_selexececao
 LOCAL lcpospanel, lccod, lccodexcecao, lctipoexcecao, lcselexcecao, lcmultiview, lccompart, lcpospanel, lcfaminfarmed, lcref, lcmultisel 
 lccompart = .F.
 lcfaminfarmed = 0
 IF (USED("uCrsExcecaoPanel"))
    SELECT ucrsexcecaopanel
    lcpospanel = RECNO("uCrsExcecaoPanel")
    SELECT ucrsexcecaopanel
    lcselexcecao = ucrsexcecaopanel.sel
    lccodexcecao = ALLTRIM(ucrsexcecaopanel.code)
    lctipoexcecao = ALLTRIM(ucrsexcecaopanel.type)
    lcmultiview = ucrsexcecaopanel.multivisible
    lcstampexcecao = ALLTRIM(ucrsexcecaopanel.stamp)
    lcmultisel = ucrsexcecaopanel.multisel
    
    
    IF (USED("uCrsAtendSt"))
       SELECT ucrsatendst
       lcfaminfarmed = ucrsatendst.familia
       lcref = ALLTRIM(ucrsatendst.ref)
    ENDIF
    lccod = uf_atendimento_verificacompart( .NOT. uf_atendimento_painelatendimento())
    IF  .NOT. EMPTY(lccod)
       lccompart = .T.
    ENDIF
    IF (EMPTY(lccompart))
       IF (lctipoexcecao=='EXCECAO' .OR. lctipoexcecao=='JUSTIFICAODEM' .OR. lctipoexcecao=='COMPART')
          uf_perguntalt_chama("Exce��o inv�lida para linha n�o comparticipada!", "OK", "", 64)
          UPDATE uCrsExcecaoPanel SET sel = .F. WHERE stamp=lcstampexcecao
          SELECT ucrsexcecaopanel
          TRY
             GOTO lcpospanel
          CATCH
             GOTO TOP
          ENDTRY
          RETURN .F.
       ENDIF
    ENDIF
    IF (lctipoexcecao=='INDISPONIVEL' .AND.  .NOT. EMPTY(lcselexcecao))
       IF lcfaminfarmed<>'1' .AND. lcfaminfarmed<>'2' .AND. lcfaminfarmed<>'58'
          uf_perguntalt_chama("Exce��o inv�lida para esta fam�lia de producto!", "OK", "", 64)
          UPDATE uCrsExcecaoPanel SET sel = .F. WHERE stamp=lcstampexcecao
          RETURN .F.
       ENDIF
       IF ( .NOT. LEN(lcref)==7)
          uf_perguntalt_chama("Exce��o inv�lida para este producto!", "OK", "", 64)
          UPDATE uCrsExcecaoPanel SET sel = .F. WHERE stamp=lcstampexcecao
          RETURN .F.
       ENDIF
    ENDIF
    IF (lctipoexcecao=='EXCECAO' .AND.  .NOT. EMPTY(lcselexcecao))
       IF (ALLTRIM(lccodexcecao)=='EA')
          SELECT ucrsatendst
          IF UPPER(ALLTRIM(ucrsatendst.marg_terap))=="N"
             IF  .NOT. uf_perguntalt_chama("ATEN��O: de acordo com as regras de prescri��o electr�nica n�o � poss�vel aplicar EXCE��O A a este produto."+CHR(13)+CHR(13)+"Tem a certeza que pretende aplicar?", "Sim", "N�o")
                UPDATE uCrsExcecaoPanel SET sel = .F. WHERE stamp=lcstampexcecao
                RETURN .F.
             ENDIF
          ENDIF
       ENDIF
    ENDIF
    uf_excecaomed_validmultisel(lcselexcecao, ALLTRIM(lctipoexcecao), lcmultiview, lcstampexcecao, lcmultisel)
    SELECT ucrsexcecaopanel
    TRY
       GOTO lcpospanel
    CATCH
       GOTO TOP
    ENDTRY
 ENDIF
ENDFUNC
**
FUNCTION uf_excecaomed_preenchecamposlivres
 IF ( .NOT. USED("fi"))
    RETURN .F.
 ENDIF
 IF ( .NOT. USED("fi2"))
    RETURN .F.
 ENDIF
 LOCAL lcposfi
 SELECT fi
 lcposfi = RECNO("fi")
 LOCAL lctext, lcexcecao, lccompart, lcindisponivel, lcjustcode, lcfistamp, lccar, lccoderrorresp, lcBonusDescr
 lctext = ''
 lcindisponivel = .F.
 lccar = ' | '
 lctemexcecao = .F.
 lccoderrorresp = ''
 lcBonusDescr = ''
 SELECT fi
 lcexcecao = ALLTRIM(fi.lobs3)
 lcfistamp = ALLTRIM(fi.fistamp)

 lccompart = fi.u_comp
 IF TYPE("fi.codigoDemResposta")<>'U'
    lccoderrorresp = ALLTRIM(fi.codigodemresposta)
 ENDIF
 SELECT fi2
 GOTO TOP
 SCAN FOR ALLTRIM(fi2.fistamp)=lcfistamp
    IF  .NOT. EMPTY(fi2.indisponivel)
       lcindisponivel = .T.
    ELSE
       lcindisponivel = .F.
    ENDIF
    lcjustcode = ALLTRIM(fi2.justificacao_tecnica_cod)
    lcBonusDescr = ALLTRIM(fi2.bonusDescr)
 ENDSCAN
 IF ( .NOT. EMPTY(lccompart))
    lctext = lctext+"Comparticipada "+lccar
 ENDIF
 IF ( .NOT. EMPTY(lcexcecao))
    lctemexcecao = .T.
    lctext = lctext+"Exce��o "+ALLTRIM(lcexcecao)+lccar
 ENDIF
 IF ( .NOT. EMPTY(lcindisponivel))
    lctemexcecao = .T.
    lctext = lctext+" Indispon�bilidade Comunicada "+lccar
 ENDIF
 IF ( .NOT. EMPTY(lcjustcode))
    lctemexcecao = .T.
    lctext = lctext+lcjustcode+lccar
 ENDIF
 IF ( .NOT. EMPTY(lcBonusDescr))
    lctext = lctext+lcBonusDescr + " "+lccar
 ENDIF
 IF (LEN(ALLTRIM(lctext))>4)
    lctext = SUBSTR(ALLTRIM(lctext), 1, LEN(ALLTRIM(lctext))-LEN(ALLTRIM(lccar)))
 ENDIF
 IF (TYPE("Atendimento.pgfDados.page2.txtExcecaoLinha")<>'U')
    atendimento.pgfdados.page2.txtexcecaolinha.value = LEFT(ALLTRIM(lctext), 254)
    IF  .NOT. EMPTY(ALLTRIM(lccoderrorresp)) .AND. EMPTY(lcjustcode)
       atendimento.pgfdados.page2.txtexcecaolinha.disabledbackcolor = RGB(255, 0, 0)
       atendimento.pgfdados.page2.txtexcecaolinha.forecolor = RGB(255, 255, 255)
    ELSE
       atendimento.pgfdados.page2.txtexcecaolinha.disabledbackcolor = RGB(255, 255, 255)
       atendimento.pgfdados.page2.txtexcecaolinha.forecolor = RGB(0, 0, 0)
    ENDIF
 ENDIF
 IF (TYPE("facturacao.pageframe1.page2.txtExcecaoLinha")<>'U')
    facturacao.pageframe1.page2.txtexcecaolinha.value = LEFT(ALLTRIM(lctext), 254)
 ENDIF
 SELECT fi
 TRY
    GOTO lcposfi
 CATCH
    GOTO BOTTOM
 ENDTRY
ENDFUNC
**
