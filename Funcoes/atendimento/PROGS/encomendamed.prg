**
FUNCTION uf_encomendamed_chama
 IF TYPE("ENCOMENDAMED")<>"U"
    uf_encomendamed_sair()
 ENDIF
 IF  .NOT. USED("Fi")
    RETURN .F.
 ENDIF
 IF  .NOT. USED("Ft")
    RETURN .F.
 ENDIF
 uf_encomendamed_syncatendimento()
 IF TYPE("ENCOMENDAMED")=="U"
    DO FORM ENCOMENDAMED
 ELSE
    encomendamed.show()
 ENDIF
 uf_encomendamed_menu()
 uf_encomendamed_calculartotais()
 uf_encomendamed_vistoautomaticoencomenda()
ENDFUNC
**
PROCEDURE uf_encomendamed_menu
 encomendamed.menu1.estado("", "", "Gravar", .T.)
 encomendamed.menu1.adicionaopcao("InfoFornec", "Consulta Forn.", mypath+"\imagens\icons\detalhe_micro.png", "uf_stocks_consultarProd with 'EncomendasAtendimento'", "")
ENDPROC
**
FUNCTION uf_encomendamed_syncatendimento
 LOCAL lcsql
 STORE '' TO lcsql
 CREATE CURSOR ucrsEncomenda (id C(25), nreceita C(25), lordem N(10), ref C(18), design C(100), sel L, encviaverde L, fornecedor C(200), estab N(5), encfornecedor L, stock N(9), qtt N(6), no N(8), ecusto N(11, 3), canuseviaverde L, iva N(5, 2), tabiva N(5, 1), u_stock N(11, 3), cpoc N(6), familia C(18))
 CREATE CURSOR ucrsEncomendaAEnviar (stamp C(50), nrdoc N(9), nreceita C(25), ref C(18), encviaverde L, estab N(5), no N(8), estadoenviado L, resperro C(200))
 INSERT INTO ucrsEncomenda (qtt, ref, design, ecusto, lordem, iva, tabiva, u_stock, cpoc, familia) SELECT fi.qtt, ALLTRIM(fi.ref), (fi.design), fi.ecusto, fi.lordem, fi.iva, fi.tabiva, fi.u_stock, fi.cpoc, fi.familia FROM fi WHERE LEFT(ALLTRIM(fi.design), 1)<>"."
 lcsql = ""
 TEXT TO lcsql TEXTMERGE NOSHOW
		Select distinct nome, no, estab from fl (nolock) where inactivo=0 order by nome
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsFornEncomenda", lcsql)
    uf_perguntalt_chama("OCORREU UMA ANOMALIA A VERIFICAR LISTA DE FORNECEDORES! POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
    RETURN .F.
 ENDIF
 SELECT ucrsencomenda
 GOTO TOP
 SCAN
    IF  .NOT. EMPTY(ALLTRIM(ucrsencomenda.ref))
       lcsql = ""
       TEXT TO lcsql TEXTMERGE NOSHOW
				Select fornec,fornecedor,fornestab, stock, qttcli from st (nolock) WHERE Ref = '<<ALLTRIM(ucrsEncomenda.ref)>>' AND site_nr = <<mysite_nr>>
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "uCrsEncomendaFornec", lcsql)
          uf_perguntalt_chama("OCORREU A VERIFICAR O FORNECEDOR ASSOCIADO � REFERENCIA! POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
          RETURN .F.
       ENDIF
       REPLACE ucrsencomenda.fornecedor WITH ucrsencomendafornec.fornecedor
       REPLACE ucrsencomenda.no WITH ucrsencomendafornec.fornec
       REPLACE ucrsencomenda.estab WITH ucrsencomendafornec.fornestab
       REPLACE ucrsencomenda.stock WITH ucrsencomendafornec.stock
       fecha("uCrsEncomendaFornec")
       lcsql = ""
       TEXT TO lcsql TEXTMERGE NOSHOW
				SELECT COUNT(ref) as viaVerde FROM emb_via_verde (nolock) WHERE  Ref = '<<ALLTRIM(ucrsEncomenda.ref)>>' and  tipo='VVM' and (data_fim='19000101' or  data_fim<=GETDATE()) 
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "uCrsEncomendaViaVerde", lcsql)
          uf_perguntalt_chama("OCORREU A VERIFICAR O ESTADO VIA VERDE DO MEDICAMENTO! POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
          RETURN .F.
       ENDIF
       IF (viaverde>0)
          REPLACE ucrsencomenda.canuseviaverde WITH .T.
          REPLACE ucrsencomenda.encviaverde WITH .T.
       ELSE
          REPLACE ucrsencomenda.canuseviaverde WITH .F.
          REPLACE ucrsencomenda.encviaverde WITH .F.
       ENDIF
       fecha("uCrsEncomendaViaVerde")
    ENDIF
 ENDSCAN
 SELECT ucrsencomenda
 GOTO TOP
ENDFUNC
**
FUNCTION uf_encomendamed_validaencomenda
 LOCAL lcsql
 STORE '' TO lcsql
 IF ucrsencomenda.no==0
    uf_perguntalt_chama("Por favor, preencha o campo fornecedor", "OK", "", 32)
    RETURN .F.
 ENDIF
 IF  .NOT. uf_gerais_actgrelha("", "uCrsEncomendaForn", lcsql)
    uf_perguntalt_chama("OCORREU UM ERRO A LER DADOS DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
    RETURN .F.
 ENDIF
 SELECT fi
 GOTO TOP
 IF ucrsencomenda.canuseviaverde==.T. .AND. ucrsencomenda.encviaverde==.T.
    LOCAL lcmednome
    lcmednome = ALLTRIM(ucrsencomenda.design)
    IF (ucrsencomenda.qtt>2)
       uf_perguntalt_chama("Pode encomendar no m�ximo 2 embalagens de "+lcmednome, "OK", "", 48)
       RETURN .F.
    ENDIF
    LOCAL lcfilordem, lcnrreceita
    STORE 0 TO lcfilordem
    STORE '' TO lcnrreceita
    lcfilordem = LEFT(astr(ucrsencomenda.lordem), 2)
    SELECT * FROM fi WHERE LEFT(astr(fi.lordem), 2)=lcfilordem AND LEFT(astr(fi.design), 1)="." INTO CURSOR uCrsTempFiValidaNrReceita READWRITE
    SELECT ucrstempfivalidanrreceita
    GOTO TOP
    LOCATE FOR LEFT(astr(ucrstempfivalidanrreceita.lordem), 2)=lcfilordem
    IF FOUND()
       IF (LEN(STREXTRACT(ucrstempfivalidanrreceita.design, ':', ' -', 1))<>13 .AND. LEN(STREXTRACT(ucrstempfivalidanrreceita.design, ':', ' -', 1))<>19) .OR. LEFT(ucrstempfivalidanrreceita.design, 4)=='.SEM'
          uf_perguntalt_chama("Deve preencher o n�mero de receita para a venda em que est� inserida esta refer�ncia.", "OK", "", 48)
          RETURN .F.
       ELSE
          lcnrreceita = STREXTRACT(ucrstempfivalidanrreceita.design, ':', ' -', 1)
          REPLACE ucrsencomenda.nreceita WITH lcnrreceita
       ENDIF
    ENDIF
    IF USED("ucrsTempFiValidaNrReceita")
       fecha("ucrsTempFiValidaNrReceita")
    ENDIF
    SELECT fi
 ENDIF
ENDFUNC
**
FUNCTION uf_encomendamed_inserelin
 LPARAMETERS lcstampenc, lcdocnome, lcnrdoc, lcref, lcdoccode, lcepv, lcqt, lctotalliq, lccodigo, lcdesign, lcqtaltern, lcstock, lciva, lctabiva, lccpoc, lcfamilia, lcdate, lctime
 LOCAL lcexecutesql
 lcexecutesql = ""
 lcbistamp = uf_gerais_stamp()
 lcordem = 10000
 TEXT TO lcsql TEXTMERGE NOSHOW
		Insert into bi
			(
			bistamp, bostamp, nmdos, obrano, ref, codigo,
			design, qtt, qtt2, uni2qtt, u_bonus,
			u_stockact, iva, tabiva, armazem, stipo, ndos, cpoc, familia,
			no, nome, local, morada, codpost, ccusto,
			epu, edebito, eprorc, epcusto, ettdeb, ecustoind, edebitoori, u_upc,
			rdata, dataobra, dataopen, resfor, lordem,lobs,
			ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
			)
		Values
			(
			'<<ALLTRIM(lcBiStamp)>>', '<<ALLTRIM(lcStampEnc)>>', '<<ALLTRIM(lcDocNome)>>', <<lcNrDoc>>, '<<ALLTRIM(lcRef)>>', '<<ALLTRIM(lcCodigo)>>',
			'<<ALLTRIM(lcDesign)>>', <<lcQt>>, 0, <<lcQtAltern>>, 0,
			<<lcStock>>, <<lciva>>, <<lctabiva>>, <<myArmazem>>, 4, <<lcDocCode>>, <<lccpoc>>, '<<ALLTRIM(lcfamilia)>>',
			<<uCrsDadosForn.No>>, '<<ALLTRIM(uCrsDadosForn.Nome)>>', '<<ALLTRIM(uCrsDadosForn.Local)>>', '<<ALLTRIM(uCrsDadosForn.Morada)>>', '<<ALLTRIM(uCrsDadosForn.CodPost)>>', '<<uCrsDadosForn.ccusto>>',
			<<ROUND(lcEpv,2)>>, <<ROUND(lcEpv,2)>>, <<ROUND(lcEpv,2)>>, <<ROUND(lcEpv,2)>>, <<ROUND(lcTotalLiq,2)>>, <<ROUND(lcEpv,2)>>, <<ROUND(lcEpv,2)>>, <<ROUND(lcEpv,2)>>,
			'<<lcDate>>', '<<lcDate>>', '<<lcDate>>', 1, <<lcOrdem>>,'',
			'<<m_chinis>>', '<<lcDate>>', '<<ALLTRIM(lcTime)>>' , '<<m_chinis>>', '<<lcDate>>', '<<ALLTRIM(lcTime)>>' 
			)
 ENDTEXT
 lcexecutesql = lcexecutesql+CHR(13)+CHR(13)+lcsql
 TEXT TO lcsql TEXTMERGE NOSHOW
		Insert into bi2
			(
			bi2stamp, bostamp, morada, local, codpost
			)
		Values
			(
			'<<ALLTRIM(lcBiStamp)>>', '<<ALLTRIM(lcStampEnc)>>', '<<ALLTRIM(uCrsDadosForn.Morada)>>', '<<ALLTRIM(uCrsDadosForn.Local)>>', '<<ALLTRIM(uCrsDadosForn.CodPost)>>'
			)
 ENDTEXT
 lcexecutesql = lcexecutesql+CHR(13)+CHR(13)+lcsql
 RETURN lcexecutesql
ENDFUNC
**
FUNCTION uf_encomendamed_dadosfornecedores
 LPARAMETERS lcno, lcestab, lcnome, lcescondermensagem
 LOCAL lcnometexto
 IF USED("uCrsDadosForn")
    fecha("uCrsDadosForn")
 ENDIF
 TEXT TO lcsql TEXTMERGE NOSHOW
			SELECT 
				fl.nome
				,fl.no
				,fl.estab
				,fl.tipo
				,fl.local
				,fl.codpost
				,fl.ncont
				,fl.morada
				,fl.moeda
				,fl.ccusto
				,fl_site.via_verde_user
				,fl_site.idclfl
				,fl_site.clpass
				
			
			from 
				fl (nolock)
				left join fl_site on fl.no = fl_site.nr_fl and fl.estab = fl_site.dep_fl
			where 
				fl.no = <<lcNo>> 
				and site_nr  = <<mysite_nr>>
				and fl.estab = <<lcEstab>>
				
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsDadosForn", lcsql)
    uf_perguntalt_chama("OCORREU UM ERRO A LER DADOS DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
    RETURN .F.
 ENDIF
 IF (RECCOUNT("uCrsDadosForn")==0)
    IF (EMPTY(ALLTRIM(lcnome)))
       lcnometexto = ""
    ELSE
       lcnometexto = " "+ALLTRIM(lcnome)+" "
    ENDIF
    IF (EMPTY(lcescondermensagem))
       uf_perguntalt_chama("O FORNECEDOR N�O TEM O ENVIO DE ENCOMENDAS CONFIGURADO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
    ENDIF
    RETURN .F.
 ENDIF
 RETURN .T.
ENDFUNC
**
FUNCTION uf_encomendamed_validafornecedoresenvia
 LPARAMETERS lcescondermensagem
 LOCAL lcvalidaforn
 STORE .F. TO lcvalidaforn
 SELECT ucrsencomenda
 IF ucrsencomenda.encviaverde==.T.
    lcvalidaforn = uf_encomendamed_dadosfornecedores(ucrsencomenda.no, ucrsencomenda.estab, "", lcescondermensagem)
    IF (lcvalidaforn==.F.)
       RETURN .F.
    ENDIF
    IF ((EMPTY(ucrsdadosforn.via_verde_user) .OR. ISNULL(ucrsdadosforn.via_verde_user)) .AND. EMPTY(lcescondermensagem))
       uf_perguntalt_chama("O FORNECEDOR N�O EST� CONFIGURADO PARA O ENVIO DE ENCOMENDAS DO TIPO VIA VERDE.", "OK", "", 32)
       RETURN .F.
    ENDIF
    IF ((EMPTY(ucrsdadosforn.idclfl) .OR. EMPTY(ucrsdadosforn.clpass) .OR. ISNULL(ucrsdadosforn.idclfl) .OR. ISNULL(ucrsdadosforn.clpass)) .AND.  .NOT. EMPTY(lcescondermensagem))
       RETURN .F.
    ENDIF
 ELSE
    lcvalidaforn = uf_encomendamed_dadosfornecedores(ucrsencomenda.no, ucrsencomenda.estab, "", lcescondermensagem)
    IF (lcvalidaforn==.F.)
       RETURN .F.
    ENDIF
    IF ((EMPTY(ucrsdadosforn.idclfl) .OR. EMPTY(ucrsdadosforn.clpass) .OR. ISNULL(ucrsdadosforn.idclfl) .OR. ISNULL(ucrsdadosforn.clpass)) .AND. EMPTY(lcescondermensagem))
       uf_perguntalt_chama("O FORNECEDOR N�O EST� CONFIGURADO PARA O ENVIO DE ENCOMENDAS.", "OK", "", 32)
       RETURN .F.
    ENDIF
    IF ((EMPTY(ucrsdadosforn.idclfl) .OR. EMPTY(ucrsdadosforn.clpass) .OR. ISNULL(ucrsdadosforn.idclfl) .OR. ISNULL(ucrsdadosforn.clpass)) .AND.  .NOT. EMPTY(lcescondermensagem))
       RETURN .F.
    ENDIF
 ENDIF
 RETURN .T.
ENDFUNC
**
FUNCTION uf_encomendamed_enviadadosfornecedor
 LOCAL lcvalidaforn
 STORE .F. TO lcvalidaforn, lcbreak
 IF USED("uCrsTipoFl")
    fecha("uCrsTipoFl")
 ENDIF
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
        select tipo, usa_esb, id_esb from fl (nolock) LEFT JOIN fl_site ON fl.no = fl_site.nr_fl AND fl.estab = fl_site.dep_fl where no = <<ucrsEncomendaAEnviar.no>> and estab = <<ucrsEncomendaAEnviar.estab>> and site_nr = <<mysite_nr>>
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsTipoFl", lcsql)
    uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR TIPO DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
    RETURN .F.
 ENDIF
 SELECT ucrsencomendaaenviar
 GOTO TOP
 SCAN
    IF (ucrsencomendaaenviar.estadoenviado==.F.)
       IF ucrsencomendaaenviar.encviaverde==.T.
          lcvalidaforn = uf_encomendamed_dadosfornecedores(ucrsencomendaaenviar.no, ucrsencomendaaenviar.estab, "")
          IF (lcvalidaforn==.F.)
             lcbreak = .T.
          ENDIF
          regua(0, 3, "A enviar Encomenda, por favor aguarde.")
          LOCAL viaverdepath, lcwspath, lcidcliente, lcnomejar, lcstampenc, lcnrdoc, lcfornno, lcfornestab, lcnrreceita, lcestadodeenvio, lcresperro
          STORE '' TO viaverdepath, lcwspath, lcidcliente, lcnomejar, lcstampenc, lcnrdoc, lcfornno, lcfornestab, lcnrreceita, lcresperro
          STORE .F. TO lcestadodeenvio
          SELECT ucrse1
          IF EMPTY(ALLTRIM(ucrse1.id_lt))
             uf_perguntalt_chama("O C�DIGO DE CLIENTE LOGITOOLS N�O EST� CORRETAMENTE PREENCHIDO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
             RETURN .F.
          ENDIF
          lcidcliente = ALLTRIM(ucrse1.id_lt)
          lcnomejar = 'ViaVerdeMed.jar'
          IF  .NOT. FILE(ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\ViaVerdeMed\'+ALLTRIM(lcnomejar))
             uf_perguntalt_chama("O SOFTWARE DE ENVIO N�O EST� CONFIGURADO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
             regua(2)
             RETURN .F.
          ENDIF
          LOCAL lcdefinewstest
          STORE 0 TO lcdefinewstest
          IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.F.
             lcdefinewstest = 0
          ELSE
             lcdefinewstest = 1
          ENDIF
          lcstampenc = ucrsencomendaaenviar.stamp
          lcnrdoc = ucrsencomendaaenviar.nrdoc
          lcfornno = ucrsencomendaaenviar.no
          lcfornestab = ucrsencomendaaenviar.estab
          lcnrreceita = ucrsencomendaaenviar.nreceita
          regua(1, 2, "A enviar Encomenda, por favor aguarde.")
          lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\ViaVerdeMed\'+ALLTRIM(lcnomejar)+' ENVIO '+' "--orderStamp='+ALLTRIM(lcstampenc)+'"'+' "--orderNr='+ALLTRIM(STR(lcnrdoc))+'"'+' "--clID='+lcidcliente+'"'+' "--site='+UPPER(ALLTRIM(mysite))+'"'+' --supplierNr='+ALLTRIM(STR(lcfornno))+' --supplierDep='+ALLTRIM(STR(lcfornestab))+' "--IDReceita='+ALLTRIM(lcnrreceita)+'"'+' --test='+ALLTRIM(STR(lcdefinewstest))
          lcwspath = "javaw -jar "+lcwspath
          owsshell = CREATEOBJECT("WScript.Shell")
          owsshell.run(lcwspath, 1, .T.)
          lcsql = ''
          TEXT TO lcsql TEXTMERGE NOSHOW
						select TOP 1 * FROM via_verde_med (nolock) WHERE bostamp = '<<ALLTRIM(lcStampEnc)>>' ORDER BY data desc
          ENDTEXT
          IF  .NOT. uf_gerais_actgrelha("", "uCrsAuxResposta", lcsql)
             lcestadodeenvio = .F.
             lcresperro = "OCORREU UMA ANOMALIA A ATUALIZAR A ENCOMENDA. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: Enc. At."
          ELSE
             IF RECCOUNT("uCrsAuxResposta")>0
                IF ucrsauxresposta.aceite==.T.
                   lcsql = ''
                   TEXT TO lcsql TEXTMERGE NOSHOW
									UPDATE bo SET logi1 = 1 WHERE bostamp = '<<ALLTRIM(uCrsAuxResposta.bostamp)>>'
                   ENDTEXT
                   IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
                      lcestadodeenvio = .F.
                      lcresperro = "OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE."
                   ENDIF
                   lcsql = ''
                   TEXT TO lcsql TEXTMERGE NOSHOW
									SELECT vv.ref, bi.design,  vv.qt_pedida, vv.qt_entregue FROM via_verde_med_d vv (nolock)
									INNER JOIN bi (nolock) ON vv.ref = bi.ref 
									WHERE id_via_verde_med = '<<ALLTRIM(uCrsAuxResposta.id)>>' 
									AND bi.bostamp = '<<ALLTRIM(uCrsAuxResposta.bostamp)>>'
                   ENDTEXT
                   IF  .NOT. uf_gerais_actgrelha("", "uCrsAuxRespostaDetalhe", lcsql)
                      lcestadodeenvio = .F.
                      lcresperro = "OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE."
                   ELSE
                      LOCAL lcinfforn
                      STORE '' TO lcinfforn
                      SELECT ucrsauxrespostadetalhe
                      GOTO TOP
                      SCAN
                         lcinfforn = lcinfforn+CHR(13)+ALLTRIM(ucrsauxrespostadetalhe.ref)+' '+ALLTRIM(ucrsauxrespostadetalhe.design)+' Qtt Pedida '+ALLTRIM(STR(ucrsauxrespostadetalhe.qt_pedida))+' Qtt Entregue '+ALLTRIM(STR(ucrsauxrespostadetalhe.qt_entregue))
                      ENDSCAN
                      lcestadodeenvio = .T.
                      uf_perguntalt_chama(LEFT(lcinfforn, 200), "OK", "", 16)
                      regua(2)
                   ENDIF
                ELSE
                   lcsql = ''
                   TEXT TO lcsql TEXTMERGE NOSHOW
									select TOP 2 * FROM via_verde_med_d(nolock) WHERE id_via_verde_med = '<<ALLTRIM(uCrsAuxResposta.id)>>' 
                   ENDTEXT
                   IF  .NOT. uf_gerais_actgrelha("", "uCrsAuxRespostaDetalhe", lcsql)
                      lcestadodeenvio = .F.
                      lcresperro = "OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE."
                   ELSE
                      LOCAL lcinfforn
                      STORE '' TO lcinfforn
                      SELECT ucrsauxrespostadetalhe
                      GOTO TOP
                      SCAN
                         lcinfforn = lcinfforn+CHR(13)+ALLTRIM(ucrsauxrespostadetalhe.response_descr)
                      ENDSCAN
                      lcestadodeenvio = .F.
                      uf_perguntalt_chama(LEFT(lcinfforn, 100), "OK", "", 16)
                      regua(2)
                   ENDIF
                ENDIF
             ELSE
                lcestadodeenvio = .F.
                lcresperro = "OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE."
                regua(2)
             ENDIF
             IF USED("uCrsAuxResposta")
                fecha("uCrsAuxResposta")
             ENDIF
             IF USED("uCrsAuxRespostaDetalhe")
                fecha("uCrsAuxRespostaDetalhe")
             ENDIF
          ENDIF
          regua(2)
       ELSE
          IF  .NOT. EMPTY(ucrstipofl.usa_esb)
             LOCAL lcresperro, lcestadodeenvio
             STORE '' TO lcresperro
             STORE .F. TO lcestadodeenvio
             IF EMPTY(ucrstipofl.id_esb)
                lcestadodeenvio = .F.
                lcresperro = "Falta preencher o c�digo do fornecedor no ESB. Por favor contacte o suporte."
             ENDIF
             LOCAL lcwspath, lcidcliente, lcnomejar, lctoken
             STORE '' TO lcwspath, lcidcliente, lcnomejar, lctoken
             lctoken = uf_gerais_stamp()
             SELECT ucrse1
             IF EMPTY(ALLTRIM(ucrse1.id_lt))
                lcestadodeenvio = .F.
                lcresperro = "O C�DIGO DE CLIENTE LOGITOOLS N�O EST� CORRETAMENTE PREENCHIDO. POR FAVOR CONTACTE O SUPORTE."
             ENDIF
             lcidcliente = ALLTRIM(ucrse1.id_lt)
             lcnomejar = 'LTSESBOrdersClient.jar'
             IF  .NOT. FILE(ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\LTSESBOrdersClient\'+ALLTRIM(lcnomejar))
                lcestadodeenvio = .F.
                lcresperro = "O SOFTWARE DE ENVIO LTS ESB N�O EST� CONFIGURADO. POR FAVOR CONTACTE O SUPORTE."
             ENDIF
             regua(1, 2, "A enviar Encomenda, por favor aguarde.")
             LOCAL lcdefinewstest
             STORE 0 TO lcdefinewstest
             IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.F.
                lcdefinewstest = 0
             ELSE
                lcdefinewstest = 1
             ENDIF
             lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\LTSESBOrdersClient\'+ALLTRIM(lcnomejar)+' --requestType=POST_ORDERS'+' "--idOrder='+ALLTRIM(ucrsencomendaaenviar.stamp)+'"'+' "--entity='+ALLTRIM(ucrstipofl.id_esb)+'"'+' "--idCl='+lcidcliente+'"'+' "--site='+UPPER(ALLTRIM(mysite))+'"'+' --siteNr='+ALLTRIM(STR(mysite_nr))+' --test='+ALLTRIM(STR(lcdefinewstest))+' "--Token='+ALLTRIM(lctoken)+'"'
             lcwspath = "javaw -jar "+lcwspath
             _CLIPTEXT = lcwspath
             owsshell = CREATEOBJECT("WScript.Shell")
             owsshell.run(lcwspath, 1, .T.)
             regua(2)
             lcsql = ''
             TEXT TO lcsql TEXTMERGE NOSHOW
							SELECT TOP 1 * FROM ext_esb_msgStatus (nolock) WHERE token = '<<lcToken>>'
             ENDTEXT
             IF  .NOT. uf_gerais_actgrelha("", "uCrsAuxRespostaDetalheESB", lcsql)
                lcestadodeenvio = .F.
                lcresperro = "OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE."
             ELSE
                LOCAL lcinfforn
                STORE '' TO lcinfforn
                IF ALLTRIM(ucrsauxrespostadetalheesb.id)=='200.1'
                   lcestadodeenvio = .T.
                ELSE
                   lcinfforn = ALLTRIM(ucrsauxrespostadetalheesb.descr)
                   lcestadodeenvio = .F.
                   lcresperro = LEFT(lcinfforn, 200)
                ENDIF
             ENDIF
             IF USED("uCrsAuxRespostaDetalheESB")
                fecha("uCrsAuxRespostaDetalheESB")
             ENDIF
          ELSE
             LOCAL postalpath, lcfornno, lcfornestab, lcestadodeenvio, lcresperro, lcnrdoc, lcstampenc, lcestadodeenvio
             STORE '' TO postalpath, lcfornno, lcfornestab, lcestadodeenvio, lcresperro, lcnrdoc, lcstampenc
             STORE .F. TO lcestadodeenvio
             postalpath = uf_gerais_getparameter('ADM0000000185', 'TEXT')
             IF  .NOT. (RIGHT(postalpath, 1)=="\")
                postalpath = ALLTRIM(postalpath)+'\'
             ENDIF
             postalpath = postalpath+"Postal\Postal.jar"
             lcstampenc = ucrsencomendaaenviar.stamp
             lcnrdoc = ucrsencomendaaenviar.nrdoc
             lcfornno = ucrsencomendaaenviar.no
             lcfornestab = ucrsencomendaaenviar.estab
             IF !FILE('&postalPath')
                lcresperro = "O POSTAL N�O EST� A SER ENCONTRADO. ASSIM N�O � POSS�VEL ENVIAR A ENCOMENDA. POR FAVOR CONTACTE O SUPORTE."
                lcestadodeenvio = .F.
             ENDIF
             lcsql = ''
             TEXT TO lcsql TEXTMERGE NOSHOW
							UPDATE bo SET logi1 = 1 WHERE bostamp = '<<ALLTRIM(lcStampEnc)>>'
             ENDTEXT
             IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
                lcestadodeenvio = .F.
                lcresperro = "OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE."
             ENDIF
             postalpath = "javaw -jar "+postalpath+" "+ALLTRIM(STR(lcfornno))+' '+ALLTRIM(STR(lcfornestab))+' '+ALLTRIM(STR(lcnrdoc))+' "'+ALLTRIM(lcstampenc)+'" '+ALLTRIM(STR(ch_userno))+' "'+ALLTRIM(mysite)+'" "'+ALLTRIM(sql_db)+'"'
             owsshell = CREATEOBJECT("WScript.Shell")
             owsshell.run(postalpath, 1, .T.)
             lcsql = ''
             TEXT TO lcsql TEXTMERGE NOSHOW
							select TOP 1 logi1 FROM bo(nolock) WHERE logi1 = 1 and bostamp = '<<ALLTRIM(lcStampEnc)>>' ORDER BY ousrdata desc
             ENDTEXT
             IF  .NOT. uf_gerais_actgrelha("", "uCrsAuxResposta", lcsql)
                lcestadodeenvio = .F.
                lcresperro = "OCORREU UMA ANOMALIA A ATUALIZAR A ENCOMENDA. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: Enc. At."
             ELSE
                IF RECCOUNT("uCrsAuxResposta")>0
                   IF (ucrsauxresposta.logi1==.T.)
                      lcestadodeenvio = .T.
                   ELSE
                      lcestadodeenvio = .F.
                   ENDIF
                ELSE
                   lcestadodeenvio = .F.
                   lcestadodeenvio = "OCORREU UMA ANOMALIA A ATUALIZAR A ENCOMENDA. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: Enc. At."
                ENDIF
             ENDIF
             fecha("uCrsAuxResposta")
             regua(2)
          ENDIF
       ENDIF
       SELECT ucrsencomendaaenviar
       REPLACE estadoenviado WITH lcestadodeenvio
       REPLACE resperro WITH lcresperro
    ENDIF
 ENDSCAN
 IF USED("uCrsTipoFl")
    fecha("uCrsTipoFl")
 ENDIF
ENDFUNC
**
PROCEDURE uf_encomendamed_alertacliente
 LOCAL lcresposta, lcnrdoccomerro, lccontaerros, lcsize
 STORE '' TO lcresposta, lcnrdoccomerro
 STORE 0 TO lccontaerros, lcsize
 SELECT ucrsencomendaaenviar
 GOTO TOP
 SELECT ucrsencomendaaenviar
 SCAN
    IF (ucrsencomendaaenviar.estadoenviado==.F.)
       lccontaerros = lccontaerros+1
       lcnrdoccomerro = ALLTRIM(lcnrdoccomerro)+ALLTRIM(STR(ucrsencomendaaenviar.nrdoc))+","
    ENDIF
 ENDSCAN
 lcsize = LEN(ALLTRIM(lcnrdoccomerro))
 lcnrdoccomerro = SUBSTR(lcnrdoccomerro, 1, lcsize-1)
 IF (lccontaerros==0)
    lcresposta = "ENCOMENDA(S) GUARDADA(S) E ENVIADA(S) COM SUCESSO."
 ELSE
    IF (lccontaerros==1)
       lcresposta = "O SEGUINTE DOCUMENTO FOI GUARDADO MAS N�O ENVIADO COM SUCESSO: "+lcnrdoccomerro
    ELSE
       lcresposta = "OS SEGUINTES DOCUMENTOS FORAM GUARDADOS MAS N�O ENVIADOS COM SUCESSO: "+lcnrdoccomerro
    ENDIF
 ENDIF
 IF RECCOUNT("ucrsEncomendaAEnviar")==0
    lcresposta = "ENCOMENDA(S) GUARDADA(S) COM SUCESSO."
 ENDIF
 uf_perguntalt_chama(lcresposta, "OK", "", 64)
 uf_encomendamed_sair()
ENDPROC
**
FUNCTION uf_encomendamed_gravar
 LOCAL lcvalida, lcvalidasel, lcenvia
 LOCAL lcsql, lccurnormal, lccurviaverde, lcvalidaforn
 STORE '' TO lcsql
 STORE .F. TO lcvalidasel, lcvalidaforn
 STORE .F. TO lcenvia
 DIMENSION lcsqlinsercaoenc(2)
 DELETE FROM ucrsEncomendaAEnviar
 SELECT ucrsencomenda
 GOTO TOP
 SCAN
    IF ucrsencomenda.sel==.T.
       lcvalidasel = .T.
       lcvalida = uf_encomendamed_validaencomenda()
       IF (lcvalida==.F.)
          RETURN .F.
       ENDIF
    ENDIF
 ENDSCAN
 IF (lcvalidasel==.F.)
    uf_perguntalt_chama("Por favor seleccione um medicamento.", "OK", "", 16)
    RETURN .F.
 ENDIF
 SELECT id, nreceita, ref, design, fornecedor, estab, no, SUM(qtt) AS qtt, ecusto, stock, encfornecedor, encviaverde, u_stock, tabiva, iva, cpoc, familia FROM ucrsEncomenda WHERE sel=.T. AND encviaverde=.F. GROUP BY nreceita, ref, design, fornecedor, estab, no, ecusto, stock, encfornecedor, encviaverde, u_stock, tabiva, iva, cpoc, familia, id INTO CURSOR ucrsEncomendaNormal READWRITE
 SELECT uf_gerais_stamp() AS id, nreceita, ref, design, fornecedor, estab, no, ecusto, stock, qtt, encfornecedor, encviaverde, u_stock, tabiva, iva, cpoc, familia FROM ucrsEncomenda WHERE sel=.T. AND encviaverde=.T. INTO CURSOR ucrsEncomendaViaVerde READWRITE
 SELECT DISTINCT no, estab, fornecedor FROM ucrsEncomendaNormal INTO CURSOR ucrEncFornecedorDistinct READWRITE
 LOCAL totalsql
 STORE '' TO totalsql
 DIMENSION lcsqlinsercaoenc(3)
 SELECT ucrencfornecedordistinct
 GOTO TOP
 SCAN
    lcvalidaforn = uf_encomendamed_dadosfornecedores(ucrencfornecedordistinct.no, ucrencfornecedordistinct.estab, ucrencfornecedordistinct.fornecedor)
    IF (lcvalidaforn==.F.)
       RETURN .F.
    ENDIF
    LOCAL lcposcab
    lcposcab = RECNO("ucrsEncomendaNormal")
    LOCAL lcstampenc
    lcstampenc = uf_gerais_stamp()
    IF uf_gerais_actgrelha("", "uCrsNrMax", 'select ISNULL((MAX(convert(numeric,obrano))),0) as nr from bo (nolock) where bo.ndos = 2 and YEAR(dataobra) = YEAR(GETDATE())')
       IF RECCOUNT("uCrsNrMax")>0
          lcnrdoc = ucrsnrmax.nr+1
       ENDIF
       fecha("uCrsNrMax")
    ELSE
       uf_perguntalt_chama("O DOCUMENTO [ENCOMENDAS A FORNECEDOR] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.", "OK", "", 48)
       RETURN .F.
    ENDIF
    uf_encomendamed_inserecab(lcstampenc, lcnrdoc, ucrencfornecedordistinct.no, ucrencfornecedordistinct.estab, "", .F., "ucrsEncomendaNormal", @lcsqlinsercaoenc)
    lcsqlinsercaoenc[1] = uf_gerais_trataplicassql(lcsqlinsercaoenc(1))
    lcsqlinsercaoenc[2] = uf_gerais_trataplicassql(lcsqlinsercaoenc(2))
    lcsqlinsercaoenc[3] = uf_gerais_trataplicassql(lcsqlinsercaoenc(3))
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_gerais_execSql 'Encomendas At', 1, '<<lcSqlInsercaoEnc[1]>>', '<<lcSqlInsercaoEnc[2]>>', '', '', '', ''
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
       uf_perguntalt_chama("OCORREU UM ERRO A GRAVAR O CABE�ALHO DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: Enc. At.", "OK", "", 16)
       RETURN .F.
    ENDIF
    SELECT ucrsencomendanormal
    TRY
       GOTO lcposcab
    CATCH
    ENDTRY
 ENDSCAN
 RELEASE lcsqlinsercaoenc
 DIMENSION lcsqlinsercaoenc(3)
 SELECT ucrsencomendaviaverde
 GOTO TOP
 SCAN
    lcvalidaforn = uf_encomendamed_dadosfornecedores(ucrsencomendaviaverde.no, ucrsencomendaviaverde.estab, ucrsencomendaviaverde.fornecedor)
    IF (lcvalidaforn==.F.)
       RETURN .F.
    ENDIF
    LOCAL lcposcabviaverde
    lcposcabviaverde = RECNO("ucrsEncomendaViaVerde")
    LOCAL lcstampenc
    lcstampenc = uf_gerais_stamp()
    IF uf_gerais_actgrelha("", "uCrsNrMax", 'select ISNULL((MAX(convert(numeric,obrano))),0) as nr from bo (nolock) where bo.ndos = 3 and YEAR(dataobra) = YEAR(GETDATE())')
       IF RECCOUNT("uCrsNrMax")>0
          lcnrdoc = ucrsnrmax.nr+1
       ENDIF
       fecha("uCrsNrMax")
    ELSE
       uf_perguntalt_chama("O DOCUMENTO [ENCOMENDAS A FORNECEDOR] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.", "OK", "", 48)
       RETURN .F.
    ENDIF
    uf_encomendamed_inserecab(lcstampenc, lcnrdoc, ucrsencomendaviaverde.no, ucrsencomendaviaverde.estab, ucrsencomendaviaverde.id, .T., "ucrsEncomendaViaVerde", @lcsqlinsercaoenc)
    lcsqlinsercaoenc[1] = uf_gerais_trataplicassql(lcsqlinsercaoenc(1))
    lcsqlinsercaoenc[2] = uf_gerais_trataplicassql(lcsqlinsercaoenc(2))
    lcsqlinsercaoenc[3] = uf_gerais_trataplicassql(lcsqlinsercaoenc(3))
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_gerais_execSql 'Encomendas At', 1, '<<lcSqlInsercaoEnc[1]>>', '<<lcSqlInsercaoEnc[2]>>', '', '', '', ''
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
       uf_perguntalt_chama("OCORREU UM ERRO A GRAVAR O CABE�ALHO DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: Enc. At.", "OK", "", 16)
       RETURN .F.
    ENDIF
    SELECT ucrsencomendaviaverde
    TRY
       GOTO lcposcabviaverde
    CATCH
    ENDTRY
 ENDSCAN
 uf_encomendamed_enviadadosfornecedor()
 uf_encomendamed_alertacliente()
ENDFUNC
**
FUNCTION uf_encomendamed_inserecab
 LPARAMETERS lcstampenc, lcnrdoc, lcno, lcestab, lcid, lcencviaverde, lcenc, lcsqlarray
 LOCAL lcenvia, lctime, lcdate
 LOCAL lcexecutesql, lcsqlcert
 STORE "" TO lcexecutesql, lcsqlcert
 LOCAL valoriva1, valoriva2, valoriva3, valoriva4, valoriva5, valoriva6, valoriva7, valoriva8, valoriva9, lctotaliva, totalcomiva, totalsemiva, totalnservico, lctotalfinal
 LOCAL totalsemiva1, totalsemiva2, totalsemiva3, totalsemiva4, totalsemiva5, totalsemiva6, totalsemiva7, totalsemiva8, totalsemiva9, lcqtsum
 LOCAL lcsql, lcdoccont, lcnraux, lcnrreceita
 LOCAL lcbistamp, lccounts, lcordem, lcqtaltern, lcref, lcqt, lctotalliq, lcvalinsertlin, lcepv, lcexecutesqlin
 STORE 0 TO valoriva1, valoriva2, valoriva3, valoriva4, valoriva5, valoriva6, valoriva7, valoriva8, valoriva9, lctotaliva, totalcomiva, totalsemiva, totalnservico, lctotalfinal
 STORE 0 TO totalsemiva1, totalsemiva2, totalsemiva3, totalsemiva4, totalsemiva5, totalsemiva6, totalsemiva7, totalsemiva8, totalsemiva9, lcqtsum
 STORE 0 TO lcdoccont
 STORE '' TO lcsql, lcexecutesqlin, lcnrreceita
 STORE 0 TO lcnraux
 STORE .F. TO lcenvia
 LOCAL lcdoccode, lcdocnome
 STORE 0 TO lcdoccode
 IF (EMPTY(ALLTRIM(lcid)))
    lcnraux = 1
 ENDIF
 lctime = TIME()
 lcdate = uf_gerais_getdate(DATE(), "SQL")
 SELECT &lcenc 
 GOTO TOP
 SCAN FOR (estab=lcestab .AND. no=lcno .AND. (id=lcid .OR. lcnraux=1))
    LOCAL totalcomivalinha
    STORE 0 TO totalcomivalinha
    IF lcencviaverde==.T.
       IF uf_gerais_actgrelha("", "uCrsTsCode", 'select nmdos from ts (nolock) where ndos = 3')
          IF RECCOUNT("uCrsTsCode")>0
             lcdocnome = ucrstscode.nmdos
             lcdoccode = 3
          ELSE
             uf_perguntalt_chama("O DOCUMENTO [ENCOMENDA A FORNECEDOR] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.", "OK", "", 48)
             regua(2)
             RETURN .F.
          ENDIF
          fecha("uCrsTsCode")
       ELSE
          uf_perguntalt_chama("O DOCUMENTO [ENCOMENDA A FORNECEDOR] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.", "OK", "", 48)
          regua(2)
          RETURN .F.
       ENDIF
    ELSE
       IF uf_gerais_actgrelha("", "uCrsTsCode", 'select nmdos from ts (nolock) where ndos = 2')
          IF RECCOUNT("uCrsTsCode")>0
             lcdocnome = ucrstscode.nmdos
             lcdoccode = 2
          ELSE
             uf_perguntalt_chama("O DOCUMENTO [ENCOMENDA A FORNECEDOR] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.", "OK", "", 48)
             regua(2)
             RETURN .F.
          ENDIF
          fecha("uCrsTsCode")
       ELSE
          uf_perguntalt_chama("O DOCUMENTO [ENCOMENDA A FORNECEDOR] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.", "OK", "", 48)
          regua(2)
          RETURN .F.
       ENDIF
    ENDIF
    lcenvia=&lcenc..encfornecedor
    lcqtsum	= lcqtsum+&lcenc..qtt
    lcref = ALLTRIM(&lcenc..ref)
    lctabiva = &lcenc..tabiva
    lciva = &lcenc..iva
    lctotalfinal = &lcenc..ecusto*&lcenc..qtt
    totalcomivalinha = &lcenc..ecusto*&lcenc..qtt
    lcepv = &lcenc..ecusto
    lccodigo = ALLTRIM(&lcenc..ref)
    lcdesign = ALLTRIM(&lcenc..DESIGN)
    lcqtaltern = 0
    lcstock = &lcenc..u_stock
    lccpoc = &lcenc..cpoc 
    lcfamilia = &lcenc..familia
    lcnrreceita = &lcenc..nreceita
    DO CASE
       CASE lctabiva==1
          valoriva1 = valoriva1+(lctotalfinal*(lciva/100))
          totalsemiva1 = totalsemiva1+lctotalfinal
       CASE lctabiva==2
          valoriva2 = valoriva2+(lctotalfinal*(lciva/100))
          totalsemiva2 = totalsemiva2+lctotalfinal
       CASE lctabiva==3
          valoriva3 = valoriva3+(lctotalfinal*(lciva/100))
          totalsemiva3 = totalsemiva3+lctotalfinal
       CASE lctabiva==4
          valoriva4 = valoriva4+(lctotalfinal*(lciva/100))
          totalsemiva4 = totalsemiva4+lctotalfinal
       CASE lctabiva==5
          valoriva5 = valoriva5+(lctotalfinal*(lciva/100))
          totalsemiva5 = totalsemiva5+lctotalfinal
       CASE lctabiva==6
          valoriva6 = valoriva6+(lctotalfinal*(lciva/100))
          totalsemiva6 = totalsemiva6+lctotalfinal
       CASE lctabiva==7
          valoriva7 = valoriva7+(lctotalfinal*(lciva/100))
          totalsemiva7 = totalsemiva7+lctotalfinal
       CASE lctabiva==8
          valoriva8 = valoriva8+(lctotalfinal*(lciva/100))
          totalsemiva8 = totalsemiva8+lctotalfinal
       CASE lctabiva==9
          valoriva9 = valoriva9+(lctotalfinal*(lciva/100))
          totalsemiva9 = totalsemiva9+lctotalfinal
    ENDCASE
    totalcomiva = totalcomiva+(lctotalfinal*(lciva/100+1))
    totalsemiva = totalsemiva+(lctotalfinal)
    totalnservico = totalnservico+lctotalfinal
    lctotaliva = valoriva1+valoriva2+valoriva3+valoriva4+valoriva5+valoriva6+valoriva7+valoriva8+valoriva9
    lcexecutesqlin =lcexecutesqlin   + CHR(10) + CHR(13) + uf_encomendamed_inserelin(lcstampenc, lcdocnome, lcnrdoc, lcref, lcdoccode, lcepv, &lcenc..qtt, totalcomivalinha , lccodigo, lcdesign, lcqtaltern, lcstock, lciva, lctabiva, lccpoc, lcfamilia, lcdate, lctime)
 ENDSCAN
 IF (lcenvia==.T.)
    INSERT INTO ucrsEncomendaAEnviar (stamp, nrdoc, nreceita, ref, encviaverde, no, estab) VALUES (lcstampenc, lcnrdoc, lcnrreceita, lcref, lcencviaverde, lcno, lcestab)
 ENDIF
 SELECT ucrsdadosforn
 TEXT TO lcsql TEXTMERGE NOSHOW
				INSERT INTO bo
					(
					bostamp,ndos,nmdos,obrano,dataobra,U_DATAENTR
					,nome,nome2,[no],estab,morada
					,[local],codpost,tipo,ncont
					,etotaldeb,dataopen,boano,ecusto,etotal,moeda,memissao
					,origem,site
					,vendedor,vendnm,obs,ccusto,sqtt14
					,ebo_1tvall,ebo_2tvall,ebo_totp1,ebo_totp2
					,ebo11_bins,ebo12_bins,ebo21_bins,ebo22_bins,ebo31_bins,ebo32_bins,
					ebo41_bins,ebo42_bins,ebo51_bins,ebo52_bins,ebo61_bins,ebo62_bins					
					,ebo11_iva,ebo12_iva,ebo21_iva,ebo22_iva,ebo31_iva,ebo32_iva
					,ebo41_iva,ebo42_iva,ebo51_iva,ebo52_iva,ebo61_iva,ebo62_iva
					,ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
					)
				Values  
					(
					'<<ALLTRIM(lcStampEnc)>>', <<lcDocCode>>, '<<ALLTRIM(lcDocNome)>>', <<lcNrDoc>>, '<<lcDate>>', '<<lcDate>>',
					'<<ALLTRIM(uCrsDadosForn.nome)>>', '', <<uCrsDadosForn.no>>, <<uCrsDadosForn.estab>>, '<<ALLTRIM(uCrsDadosForn.morada)>>',
					'<<ALLTRIM(uCrsDadosForn.local)>>', '<<ALLTRIM(uCrsDadosForn.codpost)>>', '<<ALLTRIM(uCrsDadosForn.Tipo)>>', '<<ALLTRIM(uCrsDadosForn.Ncont)>>',
					<<ROUND(totalSemIva,2)>>, '<<lcDate>>',year(dateadd(HOUR, <<difhoraria>>, getdate())), <<ROUND(totalSemIva,2)>>, <<ROUND(totalComIva,2)>>, '<<ALLTRIM(uCrsDadosForn.moeda)>>', 'EURO', 
					'BO', '<<ALLTRIM(mySite)>>',
					<<ch_vendedor>>, '<<ALLTRIM(ch_vendnm)>>', 'Documento gerado via Enc. Forn. Atendimento', '<<ALLTRIM(uCrsDadosForn.ccusto)>>',<<lcQtSum>>, 
					<<ROUND(totalSemIva,2)>>, <<ROUND(totalSemIva,2)>>, <<ROUND(totalSemIva,2)>>, <<ROUND(totalSemIva,2)>>,
					<<ROUND(totalSemIva1,2)>>, <<ROUND(totalSemIva1,2)>>, <<ROUND(totalSemIva2,2)>>, <<ROUND(totalSemIva2,2)>>, <<ROUND(totalSemIva3,2)>>, <<ROUND(totalSemIva3,2)>>, 
					<<ROUND(totalSemIva4,2)>>, <<ROUND(totalSemIva4,2)>>, <<ROUND(totalSemIva5,2)>>, <<ROUND(totalSemIva5,2)>>,<<ROUND(totalSemIva6,2)>>, <<ROUND(totalSemIva6,2)>>,
					<<ROUND(valorIva1,2)>>, <<ROUND(valorIva1,2)>>, <<ROUND(valorIva2,2)>>, <<ROUND(valorIva2,2)>>, <<ROUND(valorIva3,2)>>, <<ROUND(valorIva3,2)>>, 
					<<ROUND(valorIva4,2)>>, <<ROUND(valorIva4,2)>>, <<ROUND(valorIva5,2)>>, <<ROUND(valorIva5,2)>>,<<ROUND(valorIva6,2)>>, <<ROUND(valorIva6,2)>>,
					'<<m_chinis>>', '<<lcDate>>', '<<ALLTRIM(lcTime)>>', '<<m_chinis>>', '<<ALLTRIM(lcDate)>>', '<<ALLTRIM(lcTime)>>'
					)
 ENDTEXT
 lcexecutesql = lcexecutesql+CHR(13)+CHR(13)+lcsql
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
				Insert into bo2
					(
					bo2stamp, autotipo, pdtipo, etotalciva, ETOTIVA, u_class, u_doccont, armazem,
					ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora,
					ebo71_bins,ebo72_bins,ebo81_bins,ebo82_bins,ebo91_bins,ebo92_bins,
					ebo71_iva,ebo72_iva,ebo81_iva,ebo82_iva,ebo91_iva,ebo92_iva, nrReceita
					)
				Values
					(
					'<<ALLTRIM(lcStampEnc)>>', 1, 1, <<ROUND(totalComIva,2)>>, <<ROUND(lcTotalIva,2)>>, 'Documento gerado via Enc. Forn. Atendimento', <<lcDocCont>>, <<myArmazem>>,
					'<<m_chinis>>', '<<lcDate>>', '<<ALLTRIM(lcTime)>>', '<<m_chinis>>', '<<lcDate>>', '<<ALLTRIM(lcTime)>>',
					<<ROUND(totalSemIva7,2)>>, <<ROUND(totalSemIva7,2)>>,<<ROUND(totalSemIva8,2)>>, <<ROUND(totalSemIva8,2)>>,<<ROUND(totalSemIva9,2)>>, <<ROUND(totalSemIva9,2)>>,
					<<ROUND(valorIva7,2)>>, <<ROUND(valorIva7,2)>>,<<ROUND(valorIva8,2)>>, <<ROUND(valorIva8,2)>>,<<ROUND(valorIva9,2)>>, <<ROUND(valorIva9,2)>> , '<<ALLTRIM(lcNrReceita)>>'
					) 
					
 ENDTEXT
 lcexecutesql = lcexecutesql+CHR(10)+CHR(13)+lcsql
 lcsqlcert = lcsqlcert+uf_pagamento_gravarcert(ALLTRIM(lcstampenc), lcdate, lctime, lcdoccode, ROUND(totalcomiva, 2), 'BO', 0)
 STORE lcexecutesql TO lcsqlarray(1)
 STORE lcexecutesqlin TO lcsqlarray(2)
 STORE lcsqlcert TO lcsqlarray(3)
ENDFUNC
**
PROCEDURE uf_encomendamed_actualizainformacao
 LOCAL lcfornecedorenviar
 STORE .F. TO lcfornecedorenviar
 SELECT ucrsencomenda
 SELECT ucrsfornencomenda
 LOCATE FOR ALLTRIM(ucrsfornencomenda.nome)==ALLTRIM(ucrsencomenda.fornecedor)
 REPLACE ucrsencomenda.no WITH ucrsfornencomenda.no
 REPLACE ucrsencomenda.estab WITH ucrsfornencomenda.estab
 IF (ucrsencomenda.encfornecedor==.T.)
    lcfornecedorenviar = uf_encomendamed_validafornecedoresenvia()
    REPLACE ucrsencomenda.encfornecedor WITH lcfornecedorenviar
 ENDIF
 IF (uf_encomendamed_validafornecedoresenvia(.T.))
    REPLACE ucrsencomenda.encfornecedor WITH .T.
 ENDIF
ENDPROC
**
FUNCTION uf_encomendamed_tcqtt
 LOCAL lcqtt1
 lcqtt1 = ucrsencomenda.qtt
 uf_tecladonumerico_chama("ucrsEncomenda.qtt", "Quantidade:", 2, .F., .F., 6, 0)
 IF uf_gerais_getparameter_site('ADM0000000060', 'BOOL', mysite)
    LOCAL lcintbit, lcdecbit, lcconversao, lcqttminvd
    SELECT ucrsatendst
    GOTO TOP
    SCAN
       IF ALLTRIM(ucrsatendst.ref)=ALLTRIM(ucrsencomenda.ref)
          lcqttminvd = ucrsatendst.qttminvd
       ENDIF
    ENDSCAN
    lcconversao = ucrsencomenda.qtt/lcqttminvd
    intbit = INT(lcconversao)
    decbit = (lcconversao-intbit)*10
    IF decbit<>0 .AND. ALLTRIM(ch_grupo)<>'Administrador' .AND. ALLTRIM(ch_grupo)<>'Supervisores'
       IF  .NOT. uf_gerais_valida_password_admin('ATENDIMENTO', 'ALTERA��O QTT')
          uf_perguntalt_chama("A quantidade inserida n�o � v�lida. Por favor verifique.", "OK", "", 64)
          SELECT ucrsencomenda
          REPLACE ucrsencomenda.qtt WITH lcqtt1
          RETURN .F.
       ENDIF
    ENDIF
 ENDIF
 uf_encomendamed_calculartotais()
ENDFUNC
**
PROCEDURE uf_encomendamed_calculartotais
 LOCAL lctotalencomenda, lctotalcusto, lcpos
 STORE 0 TO lctotalencomenda, lctotalcusto, lcpos
 lcpos = RECNO("ucrsEncomenda")
 SELECT ucrsencomenda
 CALCULATE SUM(qtt) TO lctotalencomenda FOR ucrsencomenda.sel==.T.
 CALCULATE SUM(ecusto*qtt) TO lctotalcusto FOR ucrsencomenda.sel==.T.
 IF (ISNULL(lctotalencomenda))
    REPLACE lctotalencomenda WITH 0
 ENDIF
 IF (ISNULL(lctotalcusto))
    REPLACE lctotalcusto WITH 0
 ENDIF
 SELECT ucrsencomenda
 GOTO lcpos
 encomendamed.pageframe1.page1.txttotalencomenda.value = lctotalencomenda
 encomendamed.pageframe1.page1.txttotal.value = lctotalcusto
ENDPROC
**
PROCEDURE uf_encomendamed_orderref
 uf_gerais_ordenagrelha("encomendaMed.pageframe1.page1.grid1", "uCrsEncomenda", "ref", myorderencomendamed)
 IF myorderencomendamed
    myorderencomendamed = .F.
 ELSE
    myorderencomendamed = .T.
 ENDIF
ENDPROC
**
PROCEDURE uf_encomendamed_orderdesign
 uf_gerais_ordenagrelha("encomendaMed.pageframe1.page1.grid1", "uCrsEncomenda", "design", myorderencomendamed)
 IF myorderencomendamed
    myorderencomendamed = .F.
 ELSE
    myorderencomendamed = .T.
 ENDIF
ENDPROC
**
PROCEDURE uf_encomendamed_orderqtt
 uf_gerais_ordenagrelha("encomendaMed.pageframe1.page1.grid1", "uCrsEncomenda", "qtt", myorderencomendamed)
 IF myorderencomendamed
    myorderencomendamed = .F.
 ELSE
    myorderencomendamed = .T.
 ENDIF
ENDPROC
**
PROCEDURE uf_encomendamed_orderstock
 uf_gerais_ordenagrelha("encomendaMed.pageframe1.page1.grid1", "uCrsEncomenda", "stock", myorderencomendamed)
 IF myorderencomendamed
    myorderencomendamed = .F.
 ELSE
    myorderencomendamed = .T.
 ENDIF
ENDPROC
**
PROCEDURE uf_encomendamed_seleciona
 SELECT ucrsencomenda
 GOTO TOP
 SCAN
    IF ucrsencomenda.sel==.T.
       REPLACE ucrsencomenda.sel WITH .F.
    ELSE
       REPLACE ucrsencomenda.sel WITH .T.
    ENDIF
 ENDSCAN
 SELECT ucrsencomenda
 GOTO TOP
ENDPROC
**
FUNCTION uf_encomendamed_selproduto
 RETURN .T.
ENDFUNC
**
PROCEDURE uf_encomendamed_selecionaencforn
 SELECT ucrsencomenda
 GOTO TOP
 SCAN
    IF ucrsencomenda.encfornecedor==.T.
       REPLACE ucrsencomenda.encfornecedor WITH .F.
    ELSE
       REPLACE ucrsencomenda.encfornecedor WITH .T.
    ENDIF
 ENDSCAN
 SELECT ucrsencomenda
 GOTO TOP
ENDPROC
**
FUNCTION uf_encomendamed_selfornecedor
 SELECT ucrsencomenda
 IF (ucrsencomenda.encfornecedor==.F.)
    RETURN .T.
 ENDIF
 LOCAL lcfornecedorenviar
 STORE .F. TO lcfornecedorenviar
 lcfornecedorenviar = uf_encomendamed_validafornecedoresenvia()
 RETURN lcfornecedorenviar
ENDFUNC
**
PROCEDURE uf_encomendamed_selecionaencviaverde
 SELECT ucrsencomenda
 GOTO TOP
 SCAN
    IF ucrsencomenda.encviaverde==.T.
       REPLACE ucrsencomenda.encviaverde WITH .F.
    ELSE
       IF ucrsencomenda.canuseviaverde==.T.
          REPLACE ucrsencomenda.encviaverde WITH .T.
       ENDIF
    ENDIF
 ENDSCAN
 SELECT ucrsencomenda
 GOTO TOP
ENDPROC
**
FUNCTION uf_encomendamed_selviaverde
 SELECT ucrsencomenda
 IF (ucrsencomenda.canuseviaverde==.T.)
    RETURN .T.
 ELSE
    uf_perguntalt_chama("ESTE MEDICAMENTO N�O PODE SER ENCOMENDADO POR VIA VERDE.", "OK", "", 16)
    RETURN .F.
 ENDIF
ENDFUNC
**
PROCEDURE uf_encomendamed_sair
 fecha("ucrsEncomenda")
 fecha("ucrsEncomendaNormal")
 fecha("uCrsReservaBi")
 fecha("uCrsCliente")
 fecha("uCrsFornEncomenda")
 fecha("uc_PRECOCUSTO")
 fecha("uCrsTempRef")
 fecha("uCrsTempFL")
 fecha("uCrsListaReservas")
 fecha("ucrsEncomendaNormal")
 fecha("ucrsEncomendaViaVerde")
 fecha("ucrsEcomendaAEnviar")
 CLEAR CLASS dropdowncontainer
 encomendamed.hide
 encomendamed.release
 RELEASE myorderencomendasmed
 IF TYPE("ATENDIMENTO")<>"U"
    SELECT fi
    GOTO BOTTOM
    IF LEFT(fi.design, 1)=="."
       uf_atendimento_removecompart(.T.)
    ENDIF
    uf_atendimento_limpacab(.T.)
    uf_atendimento_verificalordem()
    SELECT fi
    GOTO TOP
    atendimento.grdatend.setfocus()
    IF atendimento.pgfdados.activepage<>1
       atendimento.pgfdados.activepage = 1
    ENDIF
 ENDIF
ENDPROC
**
PROCEDURE uf_encomendamed_vistoautomaticoencomenda
 SELECT ucrsencomenda
 GOTO TOP
 SCAN
    IF (uf_encomendamed_validafornecedoresenvia(.T.))
       REPLACE ucrsencomenda.encfornecedor WITH .T.
    ENDIF
 ENDSCAN
ENDPROC
**
