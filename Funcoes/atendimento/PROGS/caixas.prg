**
FUNCTION uf_caixas_gerircaixa
 LPARAMETERS lcmodo
 IF lcmodo
    IF  .NOT. (uf_gerais_validapermuser(ch_userno, ch_grupo, 'Fecho de Caixa'))
       uf_perguntalt_chama("O SEU PERFIL N�O LHE PERMITE FECHAR A CAIXA.", "OK", "", 48)
       RETURN .F.
    ENDIF
    IF uf_gerais_getparameter('ADM0000000024', 'BOOL')
       uf_caixas_fecharcaixa(.F.)
       RETURN .F.
    ENDIF
 ELSE
    IF uf_gerais_getparameter('ADM0000000026', 'bool')
       uf_caixas_criarsessao(uf_gerais_getparameter('ADM0000000026', 'num'))
       RETURN .F.
    ELSE
       PUBLIC valfundocx
       valfundocx = 0
       uf_tecladonumerico_chama("ValFundoCX", "Qual o Fundo de Caixa:", 0, .T., .F., 6, 2)
       uf_caixas_criarsessao(valfundocx)
       RETURN .F.
    ENDIF
 ENDIF
 uf_gestaosessaocaixa_chama()
ENDFUNC
**
FUNCTION uf_caixas_fecharcaixa
 LPARAMETERS tcbool
 LOCAL lcstamp, lcfdstamp
 STORE '' TO lcfdstamp
 lcstamp = uf_gerais_stamp()
  
 IF ucrse1.gestao_cx_operador=.T.
    IF EMPTY(gestaosessaocaixa.txtdinheiro.value) .AND. EMPTY(gestaosessaocaixa.txtmultibanco.value) .AND. EMPTY(gestaosessaocaixa.txtcheques.value) .AND. EMPTY(gestaosessaocaixa.txtvisa.value) .AND. EMPTY(gestaosessaocaixa.txtmodpag3.value) .AND. EMPTY(gestaosessaocaixa.txtmodpag4.value) .AND. EMPTY(gestaosessaocaixa.txtmodpag5.value) .AND. EMPTY(gestaosessaocaixa.txtmodpag6.value)
       IF  .NOT. uf_perguntalt_chama("N�O REGISTOU NENHUM VALOR DE FECHO DE CAIXA, PRETENDE CONTINUAR?", "Sim", "N�o")
          RETURN .F.
       ENDIF
    ENDIF
    lcsql = ''
    IF myaltercaixa=.F.
       TEXT TO lcsql TEXTMERGE NOSHOW
				UPDATE 
					ss
				SET 
					fusername='<<ch_vendnm>>', fuserno=<<ch_vendedor>>, dfechar=CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), hfechar=CONVERT(char(8),dateadd(HOUR, <<difhoraria>>, getdate()),8), fechada=1
					,evdinheiro	= <<Round(Val(GestaoSessaoCaixa.txtDinheiro.value),2)>>
					,epaga1		= <<Round(Val(GestaoSessaoCaixa.txtMultibanco.value),2)>>
					,epaga2		= <<Round(Val(GestaoSessaoCaixa.txtVisa.value),2)>>
					,echtotal	= <<Round(Val(GestaoSessaoCaixa.txtCheques.value),2)>>
					,fecho_epaga3	= <<Round(Val(GestaoSessaoCaixa.txtModPag3.value),2)>>
					,fecho_epaga4	= <<Round(Val(GestaoSessaoCaixa.txtModPag4.value),2)>>
					,fecho_epaga5	= <<Round(Val(GestaoSessaoCaixa.txtModPag5.value),2)>>
					,fecho_epaga6	= <<Round(Val(GestaoSessaoCaixa.txtModPag6.value),2)>>
					,comissaoTPA	= <<Round(Val(GestaoSessaoCaixa.txtComissaoTPA.value),2)>>
					,causa	= '<<alltrim(GestaoSessaoCaixa.txtObs.value)>>', sacoDinheiro='<<alltrim(GestaoSessaoCaixa.txtSaco.value)>>'
					,fundocaixa = <<IIF(EMPTY(Round(Val(GestaoSessaoCaixa.txtFundo.value),2)),0,Round(Val(GestaoSessaoCaixa.txtFundo.value),2))>>
					
				WHERE 
					ssstamp='<<mySsStamp>>'
       ENDTEXT
    ELSE
        IF uf_gerais_getParameter("ADM0000000339", "BOOL")
			LOCAL uv_newStamp 
            uv_newStamp = uf_gerais_stamp()

            TEXT TO lcSQL TEXTMERGE NOSHOW

                INSERT INTO SS
                        (ssstamp
                        ,site
                        ,pnome
                        ,pno
                        ,cxstamp
                        ,cxusername
                        ,ausername
                        ,auserno
                        ,dabrir
                        ,habrir
                        ,fusername
                        ,fuserno
                        ,dfechar
                        ,hfechar
                        ,fechada
                        ,ousrinis
                        ,ousrdata
                        ,ousrhora
                        ,usrinis
                        ,usrdata
                        ,usrhora
                        ,marcada
                        ,nr_atend
                        ,nr_vendas
                        ,evdinheiro_bruto
                        ,devolucoes
                        ,devolucoes_reais
                        ,creditos
                        ,evdinheiro
                        ,epaga1
                        ,epaga2
                        ,echtotal
                        ,fecho_epaga3
                        ,fecho_epaga4
                        ,fecho_epaga5
                        ,fecho_epaga6
                        ,totalcaixa
                        ,pendente
                        ,exportado
                        ,diaFechado
                        ,fdstamp
                        ,causa
                        ,valorTPA
                        ,comissaoTPA
                        ,sacoDinheiro
                        ,fundoCaixa)
                SELECT '<<ALLTRIM(uv_newStamp)>>'
                    ,site
                    ,pnome
                    ,pno
                    ,cxstamp
                    ,cxusername
                    ,ausername
                    ,auserno
                    ,dabrir
                    ,habrir
                    ,fusername
                    ,fuserno
                    ,dfechar
                    ,hfechar
                    ,fechada
                    ,'<<ALLTRIM(m_chinis)>>'
                    ,getdate()
                    ,LEFT(CONVERT(time, getDate()) ,8)
                    ,'<<ALLTRIM(m_chinis)>>'
                    ,getdate()
                    ,LEFT(CONVERT(time, getDate()) ,8)
                    ,marcada
                    ,nr_atend
                    ,nr_vendas
                    ,evdinheiro_bruto
                    ,devolucoes
                    ,devolucoes_reais
                    ,creditos
                    ,<<Round(Val(GestaoSessaoCaixa.txtDinheiro.value),2)>>
                    ,<<Round(Val(GestaoSessaoCaixa.txtMultibanco.value),2)>>
                    ,<<Round(Val(GestaoSessaoCaixa.txtVisa.value),2)>>
                    ,<<Round(Val(GestaoSessaoCaixa.txtCheques.value),2)>>
                    ,<<Round(Val(GestaoSessaoCaixa.txtModPag3.value),2)>>
                    ,<<Round(Val(GestaoSessaoCaixa.txtModPag4.value),2)>>
                    ,<<Round(Val(GestaoSessaoCaixa.txtModPag5.value),2)>>
                    ,<<Round(Val(GestaoSessaoCaixa.txtModPag6.value),2)>>
                    ,totalcaixa
                    ,pendente
                    ,exportado
                    ,diaFechado
                    ,fdstamp
                    ,'<<alltrim(GestaoSessaoCaixa.txtObs.value)>>'
                    ,valorTPA
                    ,<<Round(Val(GestaoSessaoCaixa.txtComissaoTPA.value),2)>>
                    ,'<<alltrim(GestaoSessaoCaixa.txtSaco.value)>>'
                    ,<<IIF(EMPTY(Round(Val(GestaoSessaoCaixa.txtFundo.value),2)),0,Round(Val(GestaoSessaoCaixa.txtFundo.value),2))>>
                FROM ss(nolock)
                WHERE
                    ssstamp = '<<mySsStamp>>'

            ENDTEXT
			
        ELSE

            TEXT TO lcsql TEXTMERGE NOSHOW
                    UPDATE 
                        ss
                    SET 
                        evdinheiro	= <<Round(Val(GestaoSessaoCaixa.txtDinheiro.value),2)>>
                        ,epaga1		= <<Round(Val(GestaoSessaoCaixa.txtMultibanco.value),2)>>
                        ,epaga2		= <<Round(Val(GestaoSessaoCaixa.txtVisa.value),2)>>
                        ,echtotal	= <<Round(Val(GestaoSessaoCaixa.txtCheques.value),2)>>
                        ,fecho_epaga3	= <<Round(Val(GestaoSessaoCaixa.txtModPag3.value),2)>>
                        ,fecho_epaga4	= <<Round(Val(GestaoSessaoCaixa.txtModPag4.value),2)>>
                        ,fecho_epaga5	= <<Round(Val(GestaoSessaoCaixa.txtModPag5.value),2)>>
                        ,fecho_epaga6	= <<Round(Val(GestaoSessaoCaixa.txtModPag6.value),2)>>
                        ,comissaoTPA	= <<Round(Val(GestaoSessaoCaixa.txtComissaoTPA.value),2)>>
                        ,causa	= '<<alltrim(GestaoSessaoCaixa.txtObs.value)>>', sacoDinheiro='<<alltrim(GestaoSessaoCaixa.txtSaco.value)>>'
                        ,fundocaixa = <<IIF(EMPTY(Round(Val(GestaoSessaoCaixa.txtFundo.value),2)),0,Round(Val(GestaoSessaoCaixa.txtFundo.value),2))>>
                        , usrinis = '<<ALLTRIM(m_chinis)>>'
                        , usrdata = getdate()
                    WHERE 
                        ssstamp='<<mySsStamp>>'
            ENDTEXT

        ENDIF
    ENDIF

    IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
       uf_perguntalt_chama("OCORREU UMA ANOMALIA A FECHAR AS SESS�ES DE CAIXA.", "OK", "", 16)
       RETURN .F.
    ENDIF
 ELSE
    IF tcbool
       IF EMPTY(gestaosessaocaixa.txtdinheiro.value) .AND. EMPTY(gestaosessaocaixa.txtmultibanco.value) .AND. EMPTY(gestaosessaocaixa.txtcheques.value) .AND. EMPTY(gestaosessaocaixa.txtvisa.value) .AND. EMPTY(gestaosessaocaixa.txtmodpag3.value) .AND. EMPTY(gestaosessaocaixa.txtmodpag4.value) .AND. EMPTY(gestaosessaocaixa.txtmodpag5.value) .AND. EMPTY(gestaosessaocaixa.txtmodpag6.value)
          IF  .NOT. uf_perguntalt_chama("N�O REGISTOU NENHUM VALOR DE FECHO DE CAIXA, PRETENDE CONTINUAR?", "Sim", "N�o")
             RETURN .F.
          ENDIF
       ENDIF
       lcsql = ''
       TEXT TO lcsql TEXTMERGE NOSHOW
				UPDATE  
					cx
				SET  
					fusername 	= '<<alltrim(ch_vendnm)>>'
					,fuserno	= <<ch_vendedor>>
					,dfechar	= CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,hfechar	= CONVERT(char(8),dateadd(HOUR, <<difhoraria>>, getdate()),8)
					,fechada	= 1
					,causa		= '<<alltrim(GestaoSessaoCaixa.txtObs.value)>>', sacoDinheiro='<<alltrim(GestaoSessaoCaixa.txtSaco.value)>>'
					,fecho_dinheiro	= <<Round(Val(GestaoSessaoCaixa.txtDinheiro.value),2)>>
					,fecho_mb		= <<Round(Val(GestaoSessaoCaixa.txtMultibanco.value),2)>>
					,fecho_visa		= <<Round(Val(GestaoSessaoCaixa.txtVisa.value),2)>>
					,fecho_cheques	= <<Round(Val(GestaoSessaoCaixa.txtCheques.value),2)>>
					,fecho_epaga3	= <<Round(Val(GestaoSessaoCaixa.txtModPag3.value),2)>>
					,fecho_epaga4	= <<Round(Val(GestaoSessaoCaixa.txtModPag4.value),2)>>
					,fecho_epaga5	= <<Round(Val(GestaoSessaoCaixa.txtModPag5.value),2)>>
					,fecho_epaga6	= <<Round(Val(GestaoSessaoCaixa.txtModPag6.value),2)>>
					,comissaoTPA	= <<Round(Val(GestaoSessaoCaixa.txtComissaoTPA.value),2)>>
					,usrdata	= CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,usrhora	= CONVERT(char(8),dateadd(HOUR, <<difhoraria>>, getdate()),8)
					,usrinis	= '<<m_chinis>>'
					,fundocaixa = <<IIF(EMPTY(Round(Val(GestaoSessaoCaixa.txtFundo.value),2)),0,Round(Val(GestaoSessaoCaixa.txtFundo.value),2))>>
				where   
					cxstamp='<<alltrim(myCxStamp)>>'
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
          uf_perguntalt_chama("Ocorreu uma anomalia a efetuar o fecho de Caixa. Por favor contacte o Suporte.", "OK", "", 16)
          RETURN .F.
       ENDIF
    ELSE
       LOCAL lcpermitezero
       lcpermitezero = uf_gerais_getparameter('ADM0000000028', 'bool')
       IF uf_gerais_getparameter('ADM0000000025', 'bool')
          DO WHILE LEN(astr(identificacao.codsaco.value))<>uf_gerais_getparameter('ADM0000000027', 'num') .AND.  .NOT. (identificacao.codsaco.value==0)
             uf_tecladonumerico_chama("IDENTIFICACAO.codSaco", "C�d. Saco do Dinheiro:", 1, .F., .F., 8, 0)
             IF LEN(astr(identificacao.codsaco.value))<>uf_gerais_getparameter('ADM0000000027', 'num') .AND.  .NOT. (identificacao.codsaco.value==0)
                uf_perguntalt_chama("N�O PODE FECHAR A CAIXA SEM PRIMEIRO INTRODUZIR CORRECTAMENTE O C�DIGO DO SACO DO DINHEIRO.", "OK", "", 64)
                identificacao.codsaco.value = 1
             ENDIF
          ENDDO
       ELSE
          uf_tecladonumerico_chama("IDENTIFICACAO.codSaco", "C�d. Saco do Dinheiro:", 1, .F., .F., 8, 0)
       ENDIF
       IF  .NOT. EMPTY(STR(identificacao.codsaco.value))
          IF identificacao.codsaco.value==0
             IF  .NOT. uf_perguntalt_chama("ATEN��O: VAI GUARDAR O C�DIGO DO SACO A 'ZERO'. TEM A CERTEZA QUE PRETENDE CONTINUAR?", "Sim", "N�o")
                identificacao.codsaco.value = 1
                RETURN .F.
             ENDIF
          ENDIF
          IF uf_gerais_actgrelha("", "uCrsValSdr", [select sacoDinheiro from cx (nolock) where sacoDinheiro!='0' and sacoDinheiro!='' and sacoDinheiro=']+STR(identificacao.codsaco.value)+['])
             IF RECCOUNT()>0
                uf_perguntalt_chama("J� EXISTE UM SACO REGISTADO O MESMO C�DIGO ["+STR(identificacao.codsaco.value)+"]. ASSIM N�O PODE CONTINUAR.", "OK", "", 48)
                identificacao.codsaco.value = 1
                RETURN .F.
             ENDIF
             fecha("uCrsValSdr")
          ENDIF
       ENDIF
       lcsql = ''
       TEXT TO lcsql TEXTMERGE NOSHOW
				UPDATE   
					cx
				SET  
					fusername = '<<alltrim(ch_vendnm)>>'
					,fuserno  = <<ch_vendedor>>
					,dfechar  = CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,hfechar  = CONVERT(char(8),dateadd(HOUR, <<difhoraria>>, getdate()),8)
					,fechada  = 1
					,causa = ''
					,sacoDinheiro = '<<IDENTIFICACAO.codsaco.value>>'
					,usrdata = CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,usrhora = CONVERT(char(8),dateadd(HOUR, <<difhoraria>>, getdate()),8)
					,usrinis = '<<m_chinis>>'
				where 
					cxstamp='<<alltrim(myCxStamp)>>'
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
          uf_perguntalt_chama("OCORREU UM ERRO A FECHAR A CAIXA", "OK", "", 16)
          RETURN .F.
       ENDIF
    ENDIF
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			Update ss
			Set fusername='<<ch_vendnm>>', fuserno=<<ch_vendedor>>, dfechar=CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), hfechar=CONVERT(char(8),dateadd(HOUR, <<difhoraria>>, getdate()),8), fechada=1
			where cxstamp='<<myCxStamp>>'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
       uf_perguntalt_chama("OCORREU UMA ANOMALIA A FECHAR AS SESS�ES DE CAIXA.", "OK", "", 16)
       RETURN .F.
    ENDIF
 ENDIF
 
 IF uf_gerais_getparameter_site('ADM0000000062', 'BOOL', mysite)
    TEXT TO lcsql TEXTMERGE NOSHOW
			DELETE from contnumerario WHERE ssstamp = '<<mySsStamp>>'
    ENDTEXT
    uf_gerais_actgrelha("", "", lcsql)
    SELECT ucrscontmoedas
    GOTO TOP
    SCAN
       TEXT TO lcsql TEXTMERGE NOSHOW
					insert into contnumerario (stamp, ssstamp, id, contagem, ousrinis, ousrdata, tipo)
					values (LEFT(newid(),21), '<<mySsStamp>>', <<ucrsContMoedas.id>>, <<ucrsContMoedas.contagem>>, '<<m_chinis>>', getdate(), 'MOEDAS')
       ENDTEXT

      
       IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
          uf_perguntalt_chama("OCORREU UMA ANOMALIA A REGISTAR A CONTAGEM DE NUMER�RIO - MOEDAS.", "OK", "", 16)
          RETURN .F.
       ENDIF
       SELECT ucrscontmoedas
    ENDSCAN
    
    
    SELECT ucrscontnotas
    GOTO TOP
    SCAN
       TEXT TO lcsql TEXTMERGE NOSHOW
					insert into contnumerario (stamp, ssstamp, id, contagem, ousrinis, ousrdata, tipo)
					values (LEFT(newid(),21), '<<mySsStamp>>', <<ucrsContNotas.id>>, <<ucrsContNotas.contagem>>, '<<m_chinis>>', getdate(), 'NOTAS')
       ENDTEXT
       
       IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
          uf_perguntalt_chama("OCORREU UMA ANOMALIA A REGISTAR A CONTAGEM DE NUMER�RIO - NOTAS.", "OK", "", 16)
          RETURN .F.
       ENDIF
       SELECT ucrscontnotas
    ENDSCAN
 ENDIF
 IF uf_gerais_getparameter('ADM0000000036', 'bool')
    IF ucrse1.gestao_cx_operador=.T.
       uf_caixas_talao(IIF(TYPE("uv_newStamp")= "C" , ALLTRIM(uv_newStamp), ALLTRIM(myssstamp)))
    ELSE
       uf_caixas_talao(ALLTRIM(mycxstamp))
    ENDIF
 ENDIF
 uf_gestaocaixas_chama()
 gestaocaixas.menu1.caixa.config("Abrir Cx", mypath+"\imagens\icons\abrirCaixa_w.png", "uf_GestaoCaixas_FecharCaixa with .f.", "F7")
 myssstamp = ''
 mycxstamp = ''
ENDFUNC
**
FUNCTION uf_caixas_criarsessao
 LPARAMETERS tcfundocaixa
 LOCAL lcstamp1, lccx, lcvalidaabredia, lcvalidadiafechado, lcfdstamp
 STORE 0 TO lccx
 STORE '' TO lcfdstamp
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_caixa_registosDia '<<uf_gerais_getDate(DATE(),"SQL")>>', '<<mysite>>'
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsAbreDia", lcsql)
    uf_perguntalt_chama("OCORREU UMA ANOMALIA A PROCURAR REGISTOS DO DIA.", "OK", "", 16)
    RETURN .F.
 ELSE
    IF RECCOUNT()>0
       SELECT ucrsabredia
       GOTO TOP
       SCAN
          IF ucrsabredia.fechado=.T.
             lcvalidadiafechado = .T.
          ELSE
             lcfdstamp = ucrsabredia.fdstamp
          ENDIF
       ENDSCAN
    ELSE
       lcvalidaabredia = .T.
       lcfdstamp = uf_gerais_stamp()
    ENDIF
 ENDIF
 IF lcvalidadiafechado
    uf_perguntalt_chama("O DIA J� FOI FECHADO. N�O � POSS�VEL ABRIR NOVA SESS�O DE CAIXA DEPOIS DO FECHO DO DIA.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF lcvalidaabredia
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			INSERT INTO B_fechoDia
				(fdStamp, site, userAbriu, ousr, usr, usrdata)
			values 
				('<<ALLTRIM(lcFdStamp)>>', '<<ALLTRIM(mySite)>>', '<<m_chinis>>', <<ch_userno>>, <<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()))
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
       uf_perguntalt_chama("OCORREU UMA ANOMALIA A ABRIR O DIA.", "OK", "", 16)
       RETURN .F.
    ENDIF
 ENDIF
 IF ucrse1.gestao_cx_operador=.T.
    myssstamp = uf_gerais_stamp()
    mycxstamp = uf_gerais_stamp()
    mycxuser = ALLTRIM(ch_vendnm)
    mycxuserno = ch_vendedor
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			INSERT INTO ss (
				ssstamp, site, pnome, pno, cxstamp
				,cxusername, ausername, auserno, dabrir, habrir
				,ousrdata, ousrhora, ousrinis, usrdata, usrhora
				,usrinis, fundoCaixa, fdstamp
			)Values(
				'<<ALLTRIM(mySsStamp)>>', '<<ALLTRIM(mySite)>>', '<<ALLTRIM(myTerm)>>', <<myTermNo>>, '<<ALLTRIM(myCxStamp)>>'
				,'<<ALLTRIM(ch_vendnm)>>', '<<ALLTRIM(ch_vendnm)>>', <<ch_vendedor>>, CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), CONVERT(char(8),dateadd(HOUR, <<difhoraria>>, getdate()),8)
				,CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), CONVERT(char(8),dateadd(HOUR, <<difhoraria>>, getdate()),8), '<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), CONVERT(char(8),dateadd(HOUR, <<difhoraria>>, getdate()),8)
				,'<<m_chinis>>', <<Round(tcFundoCaixa,2)>>, '<<ALLTRIM(lcFdStamp)>>'
			)
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
       uf_perguntalt_chama("OCORREU UMA ANOMALIA A CRIAR A SESS�O DE CAIXA.", "OK", "", 16)
       RETURN .F.
    ENDIF
 ELSE
    mycxstamp = uf_gerais_stamp()
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			select isnull((Max(cxno)),0) as cxno from cx (nolock)
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "uCrsCxNo", lcsql)
       uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR VALORES DE CAIXA.", "OK", "", 16)
       RETURN .F.
    ELSE
       SELECT ucrscxno
       IF  .NOT. EMPTY(ucrscxno.cxno)
          lccx = ucrscxno.cxno+1
       ELSE
          lccx = 1
       ENDIF
    ENDIF
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			INSERT INTO cx (
				cxstamp, [site], pnome, pno, cxno
				,cxano, ausername, auserno, dabrir, habrir
				,fdStamp, ousrdata, ousrhora, ousrinis, usrdata
				,usrhora, usrinis, fundocaixa
			)values(
				'<<alltrim(myCxStamp)>>', '<<alltrim(mySite)>>', '<<alltrim(myTerm)>>', <<myTermNo>>, <<lcCx>>
				,Year(dateadd(HOUR, <<difhoraria>>, getdate())), '<<alltrim(ch_vendnm)>>', <<ch_vendedor>>, CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), CONVERT(char(8),dateadd(HOUR, <<difhoraria>>, getdate()),8)
				,'<<ALLTRIM(lcFdStamp)>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), CONVERT(char(8),dateadd(HOUR, <<difhoraria>>, getdate()),8), '<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
				, CONVERT(char(8),dateadd(HOUR, <<difhoraria>>, getdate()),8), '<<m_chinis>>', <<Round(tcFundoCaixa,2)>>
			)
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
       uf_perguntalt_chama("OCORREU UMA ANOMALIA AO ABRIR A CAIXA.", "OK", "", 64)
       RETURN .F.
    ENDIF
    mycxuser = ALLTRIM(ch_vendnm)
    mycxuserno = ch_vendedor
    myssstamp = uf_gerais_stamp()
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			INSERT INTO ss (
				ssstamp, site, pnome, pno, cxstamp
				,cxusername, ausername, auserno, dabrir, habrir
				,ousrdata, ousrhora, ousrinis, usrdata, usrhora
				,usrinis
			)Values(
				'<<alltrim(mySsStamp)>>', '<<alltrim(mySite)>>', '<<alltrim(myTerm)>>', <<myTermNo>>, '<<alltrim(myCxStamp)>>'
				,'<<alltrim(myCxUser)>>', '<<alltrim(ch_vendnm)>>', <<ch_vendedor>>, CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), CONVERT(char(8),dateadd(HOUR, <<difhoraria>>, getdate()),8)
				,CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), CONVERT(char(8),dateadd(HOUR, <<difhoraria>>, getdate()),8), '<<m_chinis>>', CONVERT(varchar,getdate(),102), CONVERT(char(8),dateadd(HOUR, <<difhoraria>>, getdate()),8)
				,'<<m_chinis>>'
			)
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
       uf_perguntalt_chama("OCORREU UMA ANOMALIA A CRIAR A SESS�O DE CAIXA.", "OK", "", 16)
       RETURN .F.
    ENDIF
 ENDIF
 IF TYPE("GestaoCaixas")<>"U"
    gestaocaixas.menu1.caixa.config("Fechar Cx", mypath+"\imagens\icons\fecharCaixa_w.png", "uf_GestaoCaixas_FecharCaixa with .t.", "")
 ENDIF
 IF uf_gerais_getparameter('ADM0000000041', 'bool')
    uf_perguntalt_chama("SESS�O DE CAIXA FOI ABERTA PARA O OPERADOR *"+UPPER(ALLTRIM(ch_vendnm))+"* COM UM FUNDO DE CAIXA DE: "+astr(ROUND(tcfundocaixa, 2), 8, 2)+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite)), "OK", "", 64)
 ENDIF
ENDFUNC
**
PROCEDURE uf_caixas_validaabrefechatpa
 LPARAMETERS lcsatus
 LOCAL msgwait
 STORE '' TO msgwait
 RELEASE lcresponsearray
 DIMENSION lcresponsearray(2)
 uf_tpa_operation(@lcresponsearray, "A validar estado TPA...", "", "app.msg.status.tpa", 0)
 IF  .NOT. EMPTY(lcsatus)
    msgtype = "app.msg.accounting.period.open"
    msgwait = "A abrir sess�o do Tpa..."
 ELSE
    msgtype = "app.msg.accounting.period.close"
    msgwait = "A fechar sess�o do Tpa..."
 ENDIF
 IF ( .NOT. EMPTY(lcresponsearray(2)))
    DO CASE
       CASE (ALLTRIM(lcresponsearray(2))=="Aberto")
       CASE (ALLTRIM(lcresponsearray(2))=="Fechado")
          RELEASE lcresponsearray
          DIMENSION lcresponsearray(2)
          uf_tpa_operation(@lcresponsearray, msgwait, "", msgtype, 0)
          IF (lcresponsearray(1)==0)
             IF ( .NOT. EMPTY(lcresponsearray(2)))
             ELSE
                uf_perguntalt_chama(uf_tpa_messagemresposta(ALLTRIM(lcresponsearray(2))), "OK", "", 16)
             ENDIF
          ELSE
             uf_perguntalt_chama(uf_tpa_messagemresposta(ALLTRIM(lcresponsearray(2))), "OK", "", 16)
          ENDIF
       OTHERWISE
          uf_perguntalt_chama(uf_tpa_messagemresposta(ALLTRIM(lcresponsearray(2))), "OK", "", 16)
    ENDCASE
 ENDIF
ENDPROC
**
FUNCTION uf_caixas_fechartodas

 LOCAL lcvalidapw
 STORE 0 TO lcvalidapw
 lcvalidapw = uf_painelcentral_login()
 IF lcvalidapw==2
   RETURN .F.
 ENDIF

 IF  .NOT. (uf_gerais_validapermuser(ch_userno, ch_grupo, 'Fecho do Dia - Alterar'))
    uf_perguntalt_chama("O SEU PERFIL N�O PERMITE FECHAR TODAS AS CAIXAS.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF  .NOT. USED("uCrsFechoDia")
    RETURN .F.
 ENDIF
 
 LOCAL lcstamp, lcfdstamp
 lcstamp = uf_gerais_stamp()
 STORE '' TO lcfdstamp
 IF uf_gerais_getparameter('ADM0000000040', 'BOOL')
    uf_perguntalt_chama("O FECHO CENTRALIZADO DAS CAIXAS S� � PERMITIDO EM SISTEMAS QUE N�O USEM O SACO DO DINHEIRO.", "OK", "", 64)
    RETURN .F.
 ENDIF
 LOCAL lcexistecxabertas
 STORE .F. TO lcexistecxabertas
 SELECT ucrsfechodia
 GOTO TOP
 SCAN FOR ucrsfechodia.fechada==.F.
    lcexistecxabertas = .T.
 ENDSCAN
 IF  .NOT. lcexistecxabertas
    uf_perguntalt_chama("N�O EXISTEM CAIXAS ABERTAS PARA FECHAR.", "OK", "", 64)
    RETURN .F.
 ELSE
    IF  .NOT. uf_perguntalt_chama("ATEN��O: O SOFTWARE DEVE SER DESLIGADO EM TODOS OS PONTOS DE VENDA ANTES DE CONTINUAR."+CHR(10)+CHR(10)+"TEM A CERTEZA QUE PRETENDE FECHAR TODAS AS CAIXAS?", "Sim", "N�o")
       RETURN .F.
    ENDIF
 ENDIF
 SELECT ucrsfechodia
 GOTO TOP
 SCAN FOR ucrsfechodia.fechada==.F.
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			UPDATE   
				cx
			SET  
				fusername = '<<alltrim(ch_vendnm)>>', fuserno=<<ch_vendedor>>,
				dfechar=CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), hfechar=CONVERT(char(8),dateadd(HOUR, <<difhoraria>>, getdate()),8), fechada=1,
				causa='', sacoDinheiro='<<alltrim(uCrsFechoDia.sacodinheiro)>>',
				fecho_dinheiro	= 0,
				fecho_mb		= 0,
				fecho_visa		= 0,
				fecho_cheques	= 0,
				fecho_epaga3	= 0,
				fecho_epaga4	= 0,
				fecho_epaga5	= 0,
				fecho_epaga6	= 0,
				comissaoTPA		= 0
				,usrdata=CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
				,usrhora=CONVERT(char(8),dateadd(HOUR, <<difhoraria>>, getdate()),8)
				,usrinis='<<m_chinis>>'
				,fundocaixa = <<uCrsFechoDia.efundocx>>
			where  
				cxstamp='<<alltrim(uCrsFechoDia.cxstamp)>>'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
       uf_perguntalt_chama("OCORREU UMA ANOMALIA AO FECHAR A CAIXA.", "OK", "", 16)
       RETURN .T.
    ENDIF
    IF uf_gerais_getparameter('ADM0000000036', 'bool')
       uf_caixas_talao(ALLTRIM(ucrsfechodia.cxstamp))
    ENDIF
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			Update ss
			Set fusername='<<ch_vendnm>>', fuserno=<<ch_vendedor>>, dfechar=CONVERT(varchar,getdate(),102), hfechar=CONVERT(char(8),getdate(),8), fechada=1
			where cxstamp='<<alltrim(uCrsFechoDia.cxstamp)>>'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
       uf_perguntalt_chama("OCORREU UMA ANOMALIA A FECHAR AS SESS�ES DE CAIXA.", "OK", "", 16)
       RETURN .F.
    ENDIF
    mycxstamp = ''
    SELECT ucrsfechodia
 ENDSCAN
 uf_perguntalt_chama("OPERA��O EFECTUADA COM SUCESSO.", "OK", "", 64)
 uf_gestaocaixas_pesquisar()
ENDFUNC
**
FUNCTION uf_caixas_abrirtodas
 IF CTOD(uf_gerais_getdate(gestaocaixas.txtdata.value))<>DATE()
    uf_perguntalt_chama("N�O PODE ABRIR CAIXAS NUM DIA DIFERENTE DO DIA CORRENTE", "OK", "", 48)
    RETURN .F.
 ENDIF
 LOCAL lcstamp
 lcstamp = uf_gerais_stamp()
 IF  .NOT. (uf_gerais_validapermuser(ch_userno, ch_grupo, 'Fecho do Dia - Abrir todas as Caixas'))
    uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ABRIR TODAS AS CAIXAS.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF  .NOT. USED("uCrsFechoDia")
    RETURN .F.
 ENDIF
 IF uf_gerais_getparameter('ADM0000000040', 'BOOL')
    uf_perguntalt_chama("A ABERTURA CENTRALIZADA DAS CAIXAS S� � PERMITIDA EM SISTEMAS QUE N�O USEM O SACO DO DINHEIRO.", "OK", "", 64)
    RETURN .F.
 ENDIF
 LOCAL lcexistecxfechadas
 STORE .F. TO lcexistecxfechadas
 SELECT ucrsfechodia
 GOTO TOP
 SCAN FOR ucrsfechodia.fechada==.T.
    TEXT TO lcsql TEXTMERGE NOSHOW
			SELECT TOP 1 atendimento
			FROM b_terminal (nolock)
			WHERE no=<<myTermNo>> and atendimento=1
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "uCrsCxPermAtend", lcsql)
       uf_perguntalt_chama("OCORREU UM PROBLEMA A VALIDAR AS CAIXAS.", "OK", "", 16)
       RETURN .F.
    ELSE
       SELECT ucrscxpermatend
       IF RECCOUNT("uCrsCxPermAtend")>0
          lcexistecxfechadas = .T.
       ENDIF
       fecha("uCrsCxPermAtend")
    ENDIF
    SELECT ucrsfechodia
 ENDSCAN
 IF  .NOT. lcexistecxfechadas
    uf_perguntalt_chama("N�O EXISTEM CAIXAS FECHADAS NO DIA EM QUEST�O QUE POSSAM SER NOVAMENTE ABERTAS.", "OK", "", 64)
    RETURN .F.
 ELSE
    IF  .NOT. uf_perguntalt_chama("TEM A CERTEZA QUE PRETENDE ABRIR TODAS AS CAIXAS?", "Sim", "N�o")
       RETURN .F.
    ENDIF
 ENDIF
 LOCAL lcvalidadiafechado, lcvalidaabredia, lcfdstamp
 STORE '' TO lcfdstamp
 TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_caixa_registosDia '<<uf_gerais_getDate(DATE(),"SQL")>>', '<<mySite>>'
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsAbreDia", lcsql)
    uf_perguntalt_chama("OCORREU UM ERRO A PROCURAR REGISTOS DO DIA.", "OK", "", 16)
    RETURN
 ELSE
    IF RECCOUNT()>0
       SELECT ucrsabredia
       GOTO TOP
       SCAN
          IF ucrsabredia.fechado=.T.
             lcvalidadiafechado = .T.
          ELSE
             lcfdstamp = ucrsabredia.fdstamp
          ENDIF
       ENDSCAN
    ELSE
       lcvalidaabredia = .T.
       lcfdstamp = uf_gerais_stamp(43)
    ENDIF
    fecha("uCrsAbreDia")
 ENDIF
 IF lcvalidadiafechado
    uf_perguntalt_chama("O DIA J� FOI FECHADO. N�O � POSS�VEL ABRIR NOVA SESS�O DE CAIXA DEPOIS DO FECHO DO DIA.", "OK", "", 64)
    RETURN .F.
 ENDIF
 LOCAL lcfundocx, lcnaopedefundocx
 STORE 0 TO lcfundocx
 STORE .F. TO lcnaopedefundocx
 IF uf_gerais_getparameter('ADM0000000026', 'bool')
    lcfundocx = ROUND(uf_gerais_getparameter('ADM0000000026', 'num'), 2)
    lcnaopedefundocx = .T.
 ENDIF
 IF  .NOT. lcnaopedefundocx
    uf_tecladonumerico_chama("GestaoCaixas.fundoCaixa", "Qual o Fundo de Caixa:", 0, .T., .F., 6, 2)
    lcfundocx = gestaocaixas.fundocaixa
 ENDIF
 IF lcvalidaabredia
    TEXT TO lcsql TEXTMERGE NOSHOW
			INSERT INTO B_fechoDia
				(fdStamp, site, userAbriu, ousr, usr, usrdata)
			values
				('<<ALLTRIM(lcFdStamp)>>', '<<ALLTRIM(mySite)>>', '<<m_chinis>>', <<ch_userno>>, <<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()))
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
       uf_perguntalt_chama("OCORREU UM ERRO A ABRIR O DIA.", "OK", "", 64)
       RETURN .F.
    ENDIF
 ENDIF
 LOCAL lcstampcx, lcstampfcx, lcstampss
 STORE '' TO lcstampcx, lcstampfcx, lcstampss
 LOCAL lccx, lcnaoabrecx, lcnrterm, lcvalterm, lcnrcaixasabertas, lcabruitermact
 STORE 0 TO lccx, lcnrterm, lcnrcaixasabertas
 STORE .F. TO lcnaoabrecx, lcvalterm, lcabriutermact
 SELECT ucrsfechodia
 GOTO TOP
 SCAN FOR fechada==.T.
    IF lcnrterm==0
       lcnrterm = ucrsfechodia.pno
       lcvalterm = .T.
    ELSE
       IF lcnrterm==ucrsfechodia.pno
          lcvalterm = .F.
       ELSE
          lcnrterm = ucrsfechodia.pno
          lcvalterm = .T.
       ENDIF
    ENDIF
    IF lcvalterm=.T.
       IF  .NOT. uf_gerais_actgrelha("", "uCrsCxEstaAberta", [exec up_caixa_verificaCaixaAberta ']+ALLTRIM(mysite)+[', ']+ALLTRIM(ucrsfechodia.pnome)+[', 0])
          uf_perguntalt_chama("OCORREU UM PROBLEMA A VERIFICAR ESTADO DA CAIXA.", "OK", "", 16)
          RETURN .F.
       ELSE
          SELECT ucrscxestaaberta
          IF RECCOUNT("uCrsCxEstaAberta")>0
             lcnaoabrecx = .T.
          ENDIF
          fecha("uCrsCxEstaAberta")
       ENDIF
       SELECT ucrsfechodia
       IF  .NOT. lcnaoabrecx
          lcstampcx = uf_gerais_stamp()
          IF uf_gerais_actgrelha("", "uCrsCxNo", 'select isnull((Max(cxno)),0) as cxno from cx (nolock)')
             SELECT ucrscxno
             IF  .NOT. EMPTY(ucrscxno.cxno)
                lccx = ucrscxno.cxno+1
             ELSE
                lccx = 1
             ENDIF
          ENDIF
          TEXT TO lcsql TEXTMERGE NOSHOW
					insert into cx (
						cxstamp
						,[site]
						,pnome
						,pno
						,cxno
						,cxano
						,ausername
						,auserno
						,dabrir
						,habrir
						,fdStamp
						,ousrdata
						,ousrhora
						,ousrinis
						,usrdata
						,usrhora
						,usrinis
						,fundocaixa
					)values(
						'<<alltrim(lcStampCx)>>'
						,'<<alltrim(mySite)>>'
						,'<<alltrim(uCrsFechoDia.pnome)>>'
						,<<uCrsFechoDia.pno>>
						,<<lcCx>>
						,Year(getdate())
						,'<<alltrim(ch_vendnm)>>'
						,<<ch_vendedor>>
						,CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
						,CONVERT(char(8),dateadd(HOUR, <<difhoraria>>, getdate()),8)
						,'<<ALLTRIM(lcFdStamp)>>'
						,CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
						,CONVERT(char(8),dateadd(HOUR, <<difhoraria>>, getdate()),8)
						,'<<m_chinis>>'
						,CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
						,CONVERT(char(8),dateadd(HOUR, <<difhoraria>>, getdate()),8)
						,'<<m_chinis>>'
						,<<lcFundoCx>>
					)
          ENDTEXT
          IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
             uf_perguntalt_chama("OCORREU UM ERRO A ABRIR A CAIXA.", "OK", "", 16)
             RETURN .F.
          ENDIF
          IF mytermno==ucrsfechodia.pno
             mycxuser = ALLTRIM(ch_vendnm)
             mycxuserno = ch_vendedor
             mycxstamp = ALLTRIM(lcstampcx)
             lcabriutermact = .T.
          ENDIF
          lcnrcaixasabertas = lcnrcaixasabertas+1
          lcstampss = uf_gerais_stamp()
          TEXT TO lcsql TEXTMERGE NOSHOW
					Insert into ss (ssstamp, site, pnome, pno, cxstamp, cxusername, ausername, auserno,
							dabrir, habrir,
							ousrdata, ousrhora, ousrinis, usrdata, usrhora, usrinis)
					Values ('<<alltrim(lcStampSs)>>', '<<alltrim(mySite)>>', '<<alltrim(uCrsFechoDia.pnome)>>', <<uCrsFechoDia.pno>>, '<<alltrim(myCxStamp)>>', '<<alltrim(myCxUser)>>', '<<alltrim(ch_vendnm)>>', <<ch_vendedor>>,
							CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), CONVERT(char(8),dateadd(HOUR, <<difhoraria>>, getdate()),8),
							CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), CONVERT(char(8),dateadd(HOUR, <<difhoraria>>, getdate()),8), '<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),  CONVERT(char(8),dateadd(HOUR, <<difhoraria>>, getdate()),8), '<<m_chinis>>')
          ENDTEXT
          IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
             uf_perguntalt_chama("OCORREU UM ERRO A CRIAR A SESS�O DE CAIXA.", "OK", "", 16)
             RETURN .F.
          ENDIF
          IF mytermno==ucrsfechodia.pno
             myssstamp = ALLTRIM(lcstampss)
          ENDIF
       ENDIF
    ENDIF
    lcvalterm = .F.
    SELECT ucrsfechodia
 ENDSCAN
 IF (lcnrcaixasabertas>0)
    IF uf_gerais_getparameter('ADM0000000041', 'bool')
       uf_perguntalt_chama("SESS�ES DE CAIXA FORAM ABERTAS PARA O OPERADOR *"+UPPER(ALLTRIM(ch_vendnm))+"* COM UM FUNDO DE CAIXA DE: "+astr(ROUND(lcfundocx, 2), 8, 2)+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite)), "OK", "", 64)
    ENDIF
 ENDIF
 IF lcabriutermact
    IF  .NOT. TYPE("identificacao")=="U"
       identificacao.menu1.caixa.config("Fechar Cx", mypath+"\imagens\icons\fecharCaixa_w.png", "uf_identificacao_FecharCaixa with .t.", "C")
    ENDIF
 ENDIF
 uf_gestaocaixas_pesquisar()
 uf_perguntalt_chama("OPERA��O EFECTUADA COM SUCESSO."+CHR(10)+CHR(10)+"N� DE CAIXAS ABERTAS: "+astr(lcnrcaixasabertas), "OK", "", 64)
ENDFUNC
**
FUNCTION uf_caixas_validafechomeianoite
 LOCAL tcallowopfmn
 STORE .T. TO tcallowopfmn
 IF uf_gerais_actgrelha("", "uCrsFcDiff", 'select datediff(MINUTE, convert(datetime,convert(varchar,GETDATE(),102)),getdate()) as fcDiff')
    IF (ucrsfcdiff.fcdiff>ROUND(uf_gerais_getparameter('ADM0000000035', 'num'), 0))
       IF ucrse1.gestao_cx_operador=.T.
          lcsql = ''
          TEXT TO lcsql TEXTMERGE NOSHOW
					exec up_caixa_FechoDia '<<uf_gerais_getDate(DATE()-1,"SQL")>>', '<<ALLTRIM(mysite)>>', 1
          ENDTEXT
          IF  .NOT. uf_gerais_actgrelha("", "uCrsValDiaAnt", lcsql)
             uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es de Caixa", "OK", "", 16)
             RETURN .F.
          ELSE
             IF RECCOUNT()>0
                SELECT ucrsvaldiaant
                GOTO TOP
                SCAN
                   IF  .NOT. ucrsvaldiaant.fechada
                        IF uf_gerais_getParameter("ADM0000000338", "BOOL")
                            IF ucrsvaldiaant.auserno = ch_userno
                                tcallowopfmn = .F.
                            ENDIF
                        ELSE
                            tcallowopfmn = .F.
                        ENDIF
                   ENDIF
                ENDSCAN
             ENDIF
             fecha("uCrsValDiaAnt")
          ENDIF
       ELSE
          lcsql = ''
          TEXT TO lcsql TEXTMERGE NOSHOW
					exec up_caixa_FechoDia '<<uf_gerais_getDate(DATE()-1,"SQL")>>', '<<ALLTRIM(mysite)>>', 0
          ENDTEXT
          IF  .NOT. uf_gerais_actgrelha("", "uCrsValDiaAnt", lcsql)
             uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es de Caixa", "OK", "", 16)
             RETURN .F.
          ELSE
             IF RECCOUNT()>0
                SELECT ucrsvaldiaant
                GOTO TOP
                SCAN
                   IF  .NOT. ucrsvaldiaant.fechada .AND. ucrsvaldiaant.pno=mytermno
                        IF uf_gerais_getParameter("ADM0000000338", "BOOL")
                            IF ucrsvaldiaant.auserno = ch_userno
                                tcallowopfmn = .F.
                            ENDIF
                        ELSE
                            tcallowopfmn = .F.
                        ENDIF
                   ENDIF
                ENDSCAN
             ENDIF
             fecha("uCrsValDiaAnt")
          ENDIF
       ENDIF
    ENDIF
    fecha("uCrsFcDiff")
 ENDIF
 IF tcallowopfmn
    RETURN .T.
 ELSE
    RETURN .F.
 ENDIF
ENDFUNC
**
FUNCTION uf_caixas_talao
 LPARAMETERS tccxstamp
 IF EMPTY(myposprinter)
    uf_perguntalt_chama("A IMPRESSORA N�O EST� A SER DETECTADA.", "OK", "", 16)
    RETURN .F.
 ENDIF
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_caixa_dadosParaTaloesFecho '<<ALLTRIM(tcCxStamp)>>', '<<mySite>>'
 ENDTEXT
 

 IF  .NOT. uf_gerais_actgrelha("", "uCrsDTFCx", lcsql)
    uf_perguntalt_chama("Ocorreu uma anomalia a verificar os dados para a impress�o do Tal�o de Caixa. Por favor contacte o Suporte.", "OK", "", 16)
    RETURN .F.
 ELSE
    
    LOCAL lcDeveValidarContagem
    lcDeveValidarContagem = .f.
    
    IF uf_gerais_getparameter_site('ADM0000000062', 'BOOL', mysite)
    	lcDeveValidarContagem  = .t.
    ENDIF
    
    IF ucrsdtfcx.totalcaixa == 0   
 		uf_perguntalt_chama("N�O EXISTEM DADOS PARA IMPRIMIR RELATIVOS AO OPERADOR ["+ALLTRIM(ucrsdtfcx.ausername)+"].OBRIGADO.", "OK", "", 16)
     	RETURN .F.
    ENDIF
    
    IF ucrsdtfcx.totalcontagem == 0 AND lcDeveValidarContagem
    	uf_perguntalt_chama("N�O EXISTE CONTAGEM DE NUMER�RIO PREENCHIDA RELATIVO AO OPERADOR ["+ALLTRIM(ucrsdtfcx.ausername)+"].OBRIGADO.", "OK", "", 16)
    	RETURN .F.
    ENDIF
    
       **IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
	   IF uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + ALLTRIM(myTerm) + "'")

            PUBLIC upv_moeda

            upv_moeda =  ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))

            uv_idImp = uf_gerais_getUmValor("B_impressoes", "id", "tabela = 'CX' and nomeImpressao = 'Tal�o Fecho Caixa'")

            uf_imprimirgerais_sendprinter_talao(uv_idImp , .f., .F.)

       ELSE

            SELECT ucrsdtfcx
            SELECT ucrse1
            IF  .NOT. (uf_gerais_setimpressorapos(.T.))
                RETURN .F.
            ENDIF
            uf_gerais_criarcabecalhopos()
            ? PADC("INFORMACAO FECHO CAIXA", uf_gerais_getparameter("ADM0000000194", "NUM"), " ")
            uf_gerais_separadorpos()
            ? "Terminal: "
            ?? ALLTRIM(TRANSFORM(ucrsdtfcx.pno, "9999"))
            ? "              DATA	 HORA	    OPERADOR"
            ? "Abertura:  "
            ?? uf_gerais_getdate(ucrsdtfcx.dabrir, "DATA")+"  "
            ?? SUBSTR(ucrsdtfcx.habrir, 1, 8)+"  "
            ?? ALLTRIM(SUBSTR(ucrsdtfcx.ausername, 1, 20))
            ? "Fecho:	   "
            ?? uf_gerais_getdate(ucrsdtfcx.dfechar, "DATA")+"  "
            ?? SUBSTR(ucrsdtfcx.hfechar, 1, 8)+"  "
            ?? ALLTRIM(SUBSTR(ucrsdtfcx.fusername, 1, 20))
            uf_gerais_separadorpos()
            ? "		 	CONTAGEM	SISTEMA"
            IF EMPTY(uf_gerais_getparameter("ADM0000000260", "text")) .OR. UPPER(ALLTRIM(uf_gerais_getparameter("ADM0000000260", "text")))=="EURO"
                ? "Fundo de Caixa:		"
                ?? ALLTRIM(TRANSFORM(ucrsdtfcx.efundocx, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)
                IF  .NOT. EMPTY(ucrsdtfcx.dinheiro) .OR.  .NOT. EMPTY(ucrsdtfcx.sistemadinheiro)
                    ? ALLTRIM(SUBSTR(ucrsdtfcx.modopag1, 1, 20))+":		"
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.dinheiro, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)+"	   "
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.sistemadinheiro, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)
                ENDIF
                IF  .NOT. EMPTY(ucrsdtfcx.multibanco) .OR.  .NOT. EMPTY(ucrsdtfcx.sistemamb)
                    ? ALLTRIM(SUBSTR(ucrsdtfcx.modopag2, 1, 20))+":		"
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.multibanco, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)+"	   "
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.sistemamb, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)
                ENDIF
                IF  .NOT. EMPTY(ucrsdtfcx.visa) .OR.  .NOT. EMPTY(ucrsdtfcx.sistemavisa)
                    ? ALLTRIM(SUBSTR(ucrsdtfcx.modopag3, 1, 20))+":			"
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.visa, "99999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)+"	   "
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.sistemavisa, "99999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)
                ENDIF
                IF  .NOT. EMPTY(ucrsdtfcx.cheques) .OR.  .NOT. EMPTY(ucrsdtfcx.sistemacheques)
                    ? ALLTRIM(SUBSTR(ucrsdtfcx.modopag4, 1, 20))+":		"
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.cheques, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)+"	   "
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.sistemacheques, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)
                ENDIF
                IF  .NOT. EMPTY(ucrsdtfcx.epaga3) .OR.  .NOT. EMPTY(ucrsdtfcx.sistemaepaga3)
                    ? ALLTRIM(SUBSTR(ucrsdtfcx.modopag5, 1, 20))+": 		  "
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.epaga3, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)+"		    "
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.sistemaepaga3, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)
                ENDIF
                IF  .NOT. EMPTY(ucrsdtfcx.epaga4) .OR.  .NOT. EMPTY(ucrsdtfcx.sistemaepaga4)
                    ? ALLTRIM(SUBSTR(ucrsdtfcx.modopag6, 1, 20))+":		  "
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.epaga4, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)+"		    "
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.sistemaepaga4, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)
                ENDIF
                IF  .NOT. EMPTY(ucrsdtfcx.epaga5) .OR.  .NOT. EMPTY(ucrsdtfcx.sistemaepaga5)
                    ? ALLTRIM(SUBSTR(ucrsdtfcx.modopag7, 1, 20))+":		  "
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.epaga5, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)+"		    "
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.sistemaepaga5, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)
                ENDIF
                IF  .NOT. EMPTY(ucrsdtfcx.epaga6) .OR.  .NOT. EMPTY(ucrsdtfcx.sistemaepaga6)
                    ? ALLTRIM(SUBSTR(ucrsdtfcx.modopag8, 1, 20))+": 		  "
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.epaga6, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)+"		    "
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.sistemaepaga6, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)
                ENDIF
                ? "Comissao TPA:		"
                ?? ALLTRIM(TRANSFORM(ucrsdtfcx.comissaotpa, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)
                uf_gerais_separadorpos()
                SELECT ucrsdtfcx
                GOTO TOP
                ? "Total:			"
                ?? ALLTRIM(TRANSFORM(ucrsdtfcx.totalcontagem, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)+"	   "
                ?? ALLTRIM(TRANSFORM(ucrsdtfcx.totalcaixa, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)
                ? "Observacoes: "
                ?? ALLTRIM(SUBSTR(ucrsdtfcx.obs, 1, 65))
                uf_gerais_separadorpos()
                ? PADC("RESUMO OPERACOES (Terminal)", uf_gerais_getparameter("ADM0000000194", "NUM"), " ")
                uf_gerais_separadorpos()
                SELECT ucrsdtfcx
                GOTO TOP
                ? "Num. Clientes:			"
                ?? ALLTRIM(TRANSFORM(ucrsdtfcx.nr_clientes, "999999"))
                ? "Num. Vendas:			"
                ?? ALLTRIM(TRANSFORM(ucrsdtfcx.nrvendas, "999999"))
                ? "Num. Atendimentos:		"
                ?? ALLTRIM(TRANSFORM(ucrsdtfcx.nratendimentos, "999999"))
                ? "Valor Medio Venda:		"
                ?? ALLTRIM(TRANSFORM(ucrsdtfcx.vmv, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)
                ? "Devolucoes:			"
                ?? ALLTRIM(TRANSFORM(ucrsdtfcx.devolucoes, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)
                ? "Descontos:			"
                ?? ALLTRIM(TRANSFORM(ucrsdtfcx.totaldesconto, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)
            ELSE
                IF uf_gerais_getparameter("ADM0000000260", "text")='USD'
                    ? "Fundo de Caixa:		"
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.efundocx, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(36)
                    IF  .NOT. EMPTY(ucrsdtfcx.dinheiro) .OR.  .NOT. EMPTY(ucrsdtfcx.sistemadinheiro)
                        ? ALLTRIM(SUBSTR(ucrsdtfcx.modopag1, 1, 20))+":		"
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.dinheiro, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(36)+"	   "
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.sistemadinheiro, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(36)
                    ENDIF
                    IF  .NOT. EMPTY(ucrsdtfcx.multibanco) .OR.  .NOT. EMPTY(ucrsdtfcx.sistemamb)
                        ? ALLTRIM(SUBSTR(ucrsdtfcx.modopag2, 1, 20))+":		"
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.multibanco, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(36)+"	   "
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.sistemamb, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(36)
                    ENDIF
                    IF  .NOT. EMPTY(ucrsdtfcx.visa) .OR.  .NOT. EMPTY(ucrsdtfcx.sistemavisa)
                        ? ALLTRIM(SUBSTR(ucrsdtfcx.modopag3, 1, 20))+":			"
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.visa, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)+"	   "
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.sistemavisa, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(36)
                    ENDIF
                    IF  .NOT. EMPTY(ucrsdtfcx.cheques) .OR.  .NOT. EMPTY(ucrsdtfcx.sistemacheques)
                        ? ALLTRIM(SUBSTR(ucrsdtfcx.modopag4, 1, 20))+":		"
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.cheques, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)+"	   "
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.sistemacheques, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(36)
                    ENDIF
                    IF  .NOT. EMPTY(ucrsdtfcx.epaga3) .OR.  .NOT. EMPTY(ucrsdtfcx.sistemaepaga3)
                        ? ALLTRIM(SUBSTR(ucrsdtfcx.modopag5, 1, 20))+": 		  "
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.epaga3, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)+"		    "
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.sistemaepaga3, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(36)
                    ENDIF
                    IF  .NOT. EMPTY(ucrsdtfcx.epaga4) .OR.  .NOT. EMPTY(ucrsdtfcx.sistemaepaga4)
                        ? ALLTRIM(SUBSTR(ucrsdtfcx.modopag6, 1, 20))+":		  "
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.epaga4, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)+"		    "
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.sistemaepaga4, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(36)
                    ENDIF
                    IF  .NOT. EMPTY(ucrsdtfcx.epaga5) .OR.  .NOT. EMPTY(ucrsdtfcx.sistemaepaga5)
                        ? ALLTRIM(SUBSTR(ucrsdtfcx.modopag7, 1, 20))+":		  "
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.epaga5, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)+"		    "
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.sistemaepaga5, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)
                    ENDIF
                    IF  .NOT. EMPTY(ucrsdtfcx.epaga6) .OR.  .NOT. EMPTY(ucrsdtfcx.sistemaepaga6)
                        ? ALLTRIM(SUBSTR(ucrsdtfcx.modopag8, 1, 20))+": 		  "
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.epaga6, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(213)+"		    "
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.sistemaepaga6, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(36)
                    ENDIF
                    ? "Comissao TPA:		"
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.comissaotpa, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(36)
                    uf_gerais_separadorpos()
                    SELECT ucrsdtfcx
                    GOTO TOP
                    ? "Total:			"
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.totalcontagem, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(36)+"	   "
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.totalcaixa, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(36)
                    ? "Observacoes: "
                    ?? ALLTRIM(SUBSTR(ucrsdtfcx.obs, 1, 65))
                    uf_gerais_separadorpos()
                    ? PADC("RESUMO OPERACOES (Terminal)", uf_gerais_getparameter("ADM0000000194", "NUM"), " ")
                    uf_gerais_separadorpos()
                    SELECT ucrsdtfcx
                    GOTO TOP
                    ? "Num. Clientes:			"
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.nr_clientes, "999999"))
                    ? "Num. Vendas:			"
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.nrvendas, "999999"))
                    ? "Num. Atendimentos:		"
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.nratendimentos, "999999"))
                    ? "Valor Medio Venda:		"
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.vmv, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(36)
                    ? "Devolucoes:			"
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.devolucoes, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(36)
                    ? "Descontos:			"
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.totaldesconto, "9999999.99"))+CHR(27)+CHR(116)+CHR(19)+CHR(36)
                ELSE
                    ? "Fundo de Caixa:		"
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.efundocx, "9999999.99"))+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
                    IF  .NOT. EMPTY(ucrsdtfcx.dinheiro) .OR.  .NOT. EMPTY(ucrsdtfcx.sistemadinheiro)
                        ? ALLTRIM(SUBSTR(ucrsdtfcx.modopag1, 1, 20))+":		"
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.dinheiro, "9999999.99"))+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))+"	   "
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.sistemadinheiro, "9999999.99"))+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
                    ENDIF
                    IF  .NOT. EMPTY(ucrsdtfcx.multibanco) .OR.  .NOT. EMPTY(ucrsdtfcx.sistemamb)
                        ? ALLTRIM(SUBSTR(ucrsdtfcx.modopag2, 1, 20))+":		"
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.multibanco, "9999999.99"))+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))+"	   "
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.sistemamb, "9999999.99"))+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
                    ENDIF
                    IF  .NOT. EMPTY(ucrsdtfcx.visa) .OR.  .NOT. EMPTY(ucrsdtfcx.sistemavisa)
                        ? ALLTRIM(SUBSTR(ucrsdtfcx.modopag3, 1, 20))+":			"
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.visa, "9999999.99"))+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))+"	   "
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.sistemavisa, "9999999.99"))+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
                    ENDIF
                    IF  .NOT. EMPTY(ucrsdtfcx.cheques) .OR.  .NOT. EMPTY(ucrsdtfcx.sistemacheques)
                        ? ALLTRIM(SUBSTR(ucrsdtfcx.modopag4, 1, 20))+":		"
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.cheques, "9999999.99"))+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))+"	   "
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.sistemacheques, "9999999.99"))+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
                    ENDIF
                    IF  .NOT. EMPTY(ucrsdtfcx.epaga3) .OR.  .NOT. EMPTY(ucrsdtfcx.sistemaepaga3)
                        ? ALLTRIM(SUBSTR(ucrsdtfcx.modopag5, 1, 20))+": 		  "
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.epaga3, "9999999.99"))+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))+"		    "
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.sistemaepaga3, "9999999.99"))+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
                    ENDIF
                    IF  .NOT. EMPTY(ucrsdtfcx.epaga4) .OR.  .NOT. EMPTY(ucrsdtfcx.sistemaepaga4)
                        ? ALLTRIM(SUBSTR(ucrsdtfcx.modopag6, 1, 20))+":		  "
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.epaga4, "9999999.99"))+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))+"		    "
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.sistemaepaga4, "9999999.99"))+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
                    ENDIF
                    IF  .NOT. EMPTY(ucrsdtfcx.epaga5) .OR.  .NOT. EMPTY(ucrsdtfcx.sistemaepaga5)
                        ? ALLTRIM(SUBSTR(ucrsdtfcx.modopag7, 1, 20))+":		  "
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.epaga5, "9999999.99"))+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))+"		    "
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.sistemaepaga5, "9999999.99"))+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
                    ENDIF
                    IF  .NOT. EMPTY(ucrsdtfcx.epaga6) .OR.  .NOT. EMPTY(ucrsdtfcx.sistemaepaga6)
                        ? ALLTRIM(SUBSTR(ucrsdtfcx.modopag8, 1, 20))+": 		  "
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.epaga6, "9999999.99"))+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))+"		    "
                        ?? ALLTRIM(TRANSFORM(ucrsdtfcx.sistemaepaga6, "9999999.99"))+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
                    ENDIF
                    ? "Comissao TPA:		"
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.comissaotpa, "9999999.99"))+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
                    uf_gerais_separadorpos()
                    SELECT ucrsdtfcx
                    GOTO TOP
                    ? "Total:			"
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.totalcontagem, "9999999.99"))+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))+"	   "
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.totalcaixa, "9999999.99"))+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
                    ? "Observacoes: "
                    ?? ALLTRIM(SUBSTR(ucrsdtfcx.obs, 1, 65))
                    uf_gerais_separadorpos()
                    ? PADC("RESUMO OPERACOES (Terminal)", uf_gerais_getparameter("ADM0000000194", "NUM"), " ")
                    uf_gerais_separadorpos()
                    SELECT ucrsdtfcx
                    GOTO TOP
                    ? "Num. Clientes:			"
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.nr_clientes, "999999"))
                    ? "Num. Vendas:			"
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.nrvendas, "999999"))
                    ? "Num. Atendimentos:		"
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.nratendimentos, "999999"))
                    ? "Valor Medio Venda:		"
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.vmv, "9999999.99"))+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
                    ? "Devolucoes:			"
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.devolucoes, "9999999.99"))+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
                    ? "Descontos:			"
                    ?? ALLTRIM(TRANSFORM(ucrsdtfcx.totaldesconto, "9999999.99"))+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
                ENDIF
            ENDIF
            ??? CHR(27)+CHR(33)+CHR(1)
            ??? CHR(27)+CHR(51)+CHR(20)
            ? ""
            uf_gerais_separadorpos()
            ? PADC("Processado por Computador - Logitools Software", uf_gerais_getparameter("ADM0000000194", "NUM"), " ")
            ? PADC((DTOC(DATE())+" "+TIME()), uf_gerais_getparameter("ADM0000000194", "NUM"), " ")
            ??? CHR(27)+CHR(33)+CHR(0)
            ??? CHR(27)+CHR(51)+CHR(30)
            ??? CHR(27)+CHR(100)+CHR(13)
            ??? CHR(29)+CHR(86)+CHR(48)
            uf_gerais_setimpressorapos(.F.)
            LOCAL lcvalidaimpsaco
            STORE .F. TO lcvalidaimpsaco
            IF  .NOT. (uf_gerais_getparameter('ADM0000000040', 'bool'))
                RETURN
            ENDIF
            IF  .NOT. (uf_gerais_setimpressorapos(.T.))
                RETURN .F.
            ENDIF
            uf_gerais_criarcabecalhopos()
            ? ""
            ? "           SANGRIA OUTSOURCING"
            ?
            SELECT ucrsdtfcx
            ? "Safebag: "
            ?? SUBSTR(ucrsdtfcx.sacodinheiro, 1, 13)
            ?
            ? "----------------------------------------"
            ? "Terminal: "
            ?? TRANSFORM(ucrsdtfcx.pno, "9999")
            ?? " Sup: "
            ?? TRANSFORM(ucrsdtfcx.supervisor_no, "9999")
            ?? "  Op: "
            ?? TRANSFORM(ucrsdtfcx.fuserno, "9999")
            ? "Data: "
            ?? uf_gerais_getdate(ucrsdtfcx.dabrir, "DATA")
            ?? "  Hora: "
            ?? SUBSTR(ucrsdtfcx.habrir, 1, 8)
            ? ""
            ? "Operador  : "
            ?? SUBSTR(ucrsdtfcx.fusername, 1, 25)
            SELECT ucrsdtfcx
            GOTO TOP
            ? "----------------------------------------"
            ? "DISCRIMINACAO:      |QTDE|         VALOR"
            ? "----------------------------------------"
            ? "Reforco             |"
            ?? "    "
            ?? "|"
            IF ucrsdtfcx.dinheiro<0
                ?? TRANSFORM(ucrsdtfcx.efundocx+ucrsdtfcx.dinheiro, "99999999999.99")
            ELSE
                ?? TRANSFORM(ucrsdtfcx.efundocx, "99999999999.99")
            ENDIF
            IF ucrsdtfcx.dinheiro>0
                ? "Numerario           |"
                ?? "    "
                ?? "|"
                ?? TRANSFORM(ucrsdtfcx.dinheiro, "99999999999.99")
            ENDIF
            IF ucrsdtfcx.cheques>0
                ? "Cheques             |"
                ?? "    "
                ?? "|"
                ?? TRANSFORM(ucrsdtfcx.cheques, "99999999999.99")
            ENDIF
            ? "----------------------------------------"
            ? "                    | TOT:"
            ?? TRANSFORM(ucrsdtfcx.dinheiro+ucrsdtfcx.cheques+efundocx, "99999999999.99")
            ??? CHR(27)+CHR(33)+CHR(1)
            ??? CHR(27)+CHR(51)+CHR(20)
            ? ""
            ? "-----------------------------------------------------"
            ? "-----------------------------------------------------"
            ? "   Processado por Computador - "+DTOC(DATE())+" "+TIME()
            ??? CHR(27)+CHR(33)+CHR(0)
            ??? CHR(27)+CHR(100)+CHR(15)
            ??? CHR(29)+CHR(86)+CHR(48)
            uf_gerais_setimpressorapos(.F.)
        ENDIF
    ENDIF
 
ENDFUNC
**
