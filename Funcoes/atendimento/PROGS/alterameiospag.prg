**
FUNCTION uf_alterameiospag_chama
 LPARAMETERS lcnratend, lnano, lcCursorAtendimento
 IF  .NOT. USED("UcrsConfigModPag")
    lcsql = ''
    IF ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))='�'
       TEXT TO lcsql TEXTMERGE NOSHOW
				select * from B_modoPag (nolock)
       ENDTEXT
    ELSE
       TEXT TO lcsql TEXTMERGE NOSHOW
				select * from B_modoPag_moeda(nolock)
       ENDTEXT
    ENDIF
    IF  .NOT. uf_gerais_actgrelha("", "UcrsConfigModPag", lcsql)
       uf_perguntalt_chama("N�o foi poss�vel verificar as configura��es de Modo de Pagamento. Contacte o suporte.", "", "OK", 32)
       RETURN .F.
    ENDIF
 ENDIF
 IF TYPE("ALTERAMEIOSPAG")=="U"
    DO FORM ALTERAMEIOSPAG WITH lcnratend, lnano, lcCursorAtendimento
 ELSE
    alterameiospag.show()
 ENDIF
ENDFUNC
**
PROCEDURE uf_alterameiospag_sair
 fecha("ucrsB_pagCentral")
 alterameiospag.release()
ENDPROC
**
FUNCTION uf_alterameiospag_gravar
	LOCAL lcsql, lntotalsel
	
	
	SELECT ucrsb_pagcentral
	GO TOP 
	SCAN
		
		IF  .NOT. EMPTY(ucrse1.gestao_cx_operador)
			lcsql = ''
			TEXT TO lcsql TEXTMERGE NOSHOW
				SELECT 
					fechada 
				FROM ss (nolock)
				where 
					ssstamp = '<<uCrsB_pagCentral.ssstamp>>'
			ENDTEXT
			uf_gerais_actgrelha("", "ucrsVerCxAberta", lcsql) 
			SELECT ucrsvercxaberta
			IF RECCOUNT("ucrsVerCxAberta")==0 .OR. ucrsvercxaberta.fechada
				uf_perguntalt_chama("A CAIXA DO ATENDIMENTO EST� FECHADA. S� � POSS�VEL ALTERAR M�TODOS DE PAGAMENTO EM ATENDIMENTOS ASSOCIADOS A CAIXAS QUE AINDA ESTEJAM ABERTAS.OBRIGADO.", "OK", "", 48)
				RETURN .F.
			ENDIF
		ELSE
			lcsql = ''
			TEXT TO lcsql TEXTMERGE NOSHOW
				SELECT fechada 
				FROM cx (nolock)
				where cxstamp = '<<ucrsB_pagCentral.cxstamp>>'
			ENDTEXT
			uf_gerais_actgrelha("", "ucrsVerCxAberta", lcsql)
			
			SELECT ucrsvercxaberta
			IF RECCOUNT("ucrsVerCxAberta")==0 .OR. ucrsvercxaberta.fechada
				uf_perguntalt_chama("A CAIXA DO ATENDIMENTO EST� FECHADA. S� � POSS�VEL ALTERAR M�TODOS DE PAGAMENTO EM ATENDIMENTOS ASSOCIADOS A CAIXAS QUE AINDA ESTEJAM ABERTAS.OBRIGADO.", "OK", "", 48)
				RETURN .F.
			ENDIF
		ENDIF
	
	ENDSCAN 
	
	lntotalsel = alterameiospag.txtdinheiro.value+alterameiospag.txtmb.value+alterameiospag.txtvisa.value+alterameiospag.txtcheque.value+alterameiospag.txtmodpag3.value+alterameiospag.txtmodpag4.value+alterameiospag.txtmodpag5.value+alterameiospag.txtmodpag6.value

	IF lntotalsel<>alterameiospag.txtvalatend.value
		uf_perguntalt_chama("O TOTAL DOS MEIOS DE PAGAMENTO INTRODUZIDOS � DIFERENTE DO VALOR DO ATENDIMENTO."+CHR(13)+"POR FAVOR VERIFIQUE.", "OK", "", 48)
		RETURN .F.
	ENDIF

	LOCAL lcabrepag, lcdinheiro
	lcabrepag = 0
	
	IF RECCOUNT("ucrsb_pagcentral") > 0
	
		SELECT ucrsb_pagcentral
		GO TOP 
		SCAN
			SELECT ucrsb_pagcentral
			lcdinheiro = ucrsb_pagcentral.evdinheiro
			lcabrepag = uf_alterameiospag_validaestadofinalpagamento(lcdinheiro)
		
			SELECT ucrsb_pagcentral
			LOCAL lcfechastamp, lcfechauser, lcabatido, lcfechado
			lcfechastamp = ALLTRIM(ucrsb_pagcentral.fechastamp)
			lcfechauser = ucrsb_pagcentral.fechauser
			lcabatido = IIF( .NOT. EMPTY(ucrsb_pagcentral.abatido), 1, 0)
			lcfechado = IIF( .NOT. EMPTY(ucrsb_pagcentral.fechado), 1, 0)

			DO CASE
				CASE lcabrepag==1
					lcfechastamp = ''
					lcfechauser = 0
					lcabatido = 0
					lcfechado = 0
				CASE lcabrepag==2
				IF (lcfechado==0)
					IF (EMPTY(lcfechastamp))
						lcfechastamp = uf_gerais_stamp()
					ENDIF
					lcfechado = 1
				ENDIF
			OTHERWISE
			ENDCASE

			TEXT TO uv_sql TEXTMERGE NOSHOW
				up_faturacao_getPagExternoAtendimento '<<ALLTRIM(ucrsb_pagcentral.nrAtend)>>', 0
			ENDTEXT
			IF !uf_gerais_actGrelha("", "uc_oriPagExterno", uv_sql)
				uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR OS MEIOS DE PAGAMENTO."+CHR(13)+"POR FAVOR CONTACTE O SUPORTE.", "OK", "", 48)
				RETURN .F.
			ENDIF
			
			
			SELECT uc_oriPagExterno	
			
			IF RECCOUNT("uc_oriPagExterno") > 0
				IF uf_gerais_getUmValor("b_pagCentral_ext", "count(*)", "operationID = '" + ALLTRIM(uc_oriPagExterno.operationID) + "' and CONVERT(VARCHAR, ousrdata, 112) <= DATEADD( MONTH, 2, '" + uf_gerais_getDate(uc_oriPagExterno.ousrdata, "SQL") + "') ") > 1
					uf_perguntalt_chama("N�o pode alterar o meio de pagamento em documentos de 'Somar Atendimento'.", "OK", "", 48)
					RETURN .F.
				ENDIF
				
				SELECT uc_oriPagExterno
				GO TOP
				LOCATE FOR uc_oriPagExterno.statePaymentID = 0 AND EMPTY(uc_oriPagExterno.statePaymentIDDesc)
				IF FOUND()
					uf_perguntalt_chama("Existem m�todos de Pagamento Externo por consultar." + CHR(13) + "Por favor verifique os mesmos.", "OK", "", 48)
					RETURN .F.
				ENDIF
				
				SELECT uc_oriPagExterno
				GO TOP
				SCAN FOR uc_oriPagExterno.statePaymentID = 0
					TEXT TO uv_sql TEXTMERGE NOSHOW
		 
						UPDATE 
							b_PagCentral_ext 
						SET 
							statePaymentID = -4, 
							statePaymentIdDesc = 'Cancelada pelo Operador', 
							usrdata = GETDATE(), 
							usrinis = '<<ALLTRIM(m_chinis)>>'
						WHERE
							token = '<<uc_oriPagExterno.token>>'

					ENDTEXT

					IF !uf_gerais_actGrelha("", "", uv_sql)
						uf_perguntalt_chama("ERRO A ATUALIZAR MEIOS DE PAGAMENTO EXTERNO."+CHR(13)+"POR FAVOR CONTACTE O SUPORTE.", "OK", "", 48)
						RETURN .F.
					ENDIF

					SELECT uc_oriPagExterno
				ENDSCAN

			ENDIF

			IF !uf_alterameiospag_geraPagExterno(ucrsb_pagcentral.no, ucrsb_pagcentral.estab, ucrsb_pagcentral.nrAtend)
				uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR OS MEIOS DE PAGAMENTO."+CHR(13)+"POR FAVOR CONTACTE O SUPORTE.", "OK", "", 48)
				RETURN .F.
			ENDIF
			
			
			LOCAL uv_dinheiro, uv_mb, uv_visa, uv_cheque, uv_modPag3, uv_modPag4, uv_modPag5 , uv_modPag6 
		
			lcsql = ''
			
			uv_dinheiro = ALTERAMEIOSPAG.txtDinheiro.value
			uv_mb 		= ALTERAMEIOSPAG.txtMB.value
			uv_visa 	= ALTERAMEIOSPAG.txtVisa.value 
			uv_cheque 	= ALTERAMEIOSPAG.txtCheque.value
			uv_modPag3	= ALTERAMEIOSPAG.txtModPag3.value
			uv_modPag4	= ALTERAMEIOSPAG.txtModPag4.value
			uv_modPag5 	= ALTERAMEIOSPAG.txtModPag5.value
			uv_modPag6 	= ALTERAMEIOSPAG.txtModPag6.value
			
			**apenas � possivel escolher um metodo de pagamento no somar atendimentos
		 	IF TYPE("SOMARATENDIMENTOS")<>"U"
		 				
				uv_dinheiro = '0'
				uv_mb 		= '0'
				uv_visa 	= '0'
				uv_cheque 	= '0'
				uv_modPag3	= '0'
				uv_modPag4	= '0'
				uv_modPag5 	= '0'
				uv_modPag6 	= '0'
				
				DO CASE
					CASE ALTERAMEIOSPAG.txtDinheiro.value <> 0
						uv_dinheiro = ucrsb_pagcentral.total

					CASE ALTERAMEIOSPAG.txtMB.value <> 0
						uv_mb 		= ucrsb_pagcentral.total

					CASE ALTERAMEIOSPAG.txtVisa.value <> 0
						uv_visa 	= ucrsb_pagcentral.total	

					CASE ALTERAMEIOSPAG.txtCheque.value <> 0
						uv_cheque 	= ucrsb_pagcentral.total

					CASE ALTERAMEIOSPAG.txtModPag3.value <> 0
						uv_modPag3	= ucrsb_pagcentral.total				

					CASE ALTERAMEIOSPAG.txtModPag4.value <> 0
						uv_modPag4	= ucrsb_pagcentral.total

					CASE ALTERAMEIOSPAG.txtModPag5.value <> 0
						uv_modPag5	= ucrsb_pagcentral.total

					CASE ALTERAMEIOSPAG.txtModPag6.value <> 0
						uv_modPag6	= ucrsb_pagcentral.total

					OTHERWISE
						
				ENDCASE
				
			ENDIF
			
			TEXT TO lcsql TEXTMERGE NOSHOW
				UPDATE 
					b_pagcentral 
				SET 
					evdinheiro = <<uv_dinheiro>>
					,epaga2 = <<uv_mb>>
					,epaga1 = <<uv_visa>>
					,echtotal = <<uv_cheque>>
					,evdinheiro_semTroco = 0
					,etroco = 0
					,epaga3 =  <<uv_modPag3>>
					,epaga4 =  <<uv_modPag4>>
					,epaga5 =  <<uv_modPag5>>
					,epaga6 =  <<uv_modPag6>>
					,fechaStamp = '<<ALLTRIM(lcFechaStamp))>>'
					,fechaUser  = <<lcFechaUser>>
					,abatido    = <<lcAbatido>>
					,fechado    = <<lcFechado>>
				where 
					stamp = '<<ucrsB_pagCentral.stamp>>'
			ENDTEXT
			IF uf_gerais_actgrelha("", "", lcsql)
				SELECT ucrsb_pagcentral
				lcsql = ''
				TEXT TO lcsql TEXTMERGE NOSHOW
					INSERT into B_ocorrencias
						(stamp, linkstamp, tipo, grau, descr,
						ovalor,
						dvalor,
						usr, date, site, terminal)
					values
						(LEFT(newid(),25), '', 'Vendas', 2, 'Alterados meios de pagamento do atendimento: <<ucrsB_pagCentral.nrAtend>>',
						'D-M-V-C: <<ucrsB_pagCentral.evdinheiro>> - <<ucrsB_pagCentral.epaga2>> - <<ucrsB_pagCentral.epaga1>> - <<ucrsB_pagCentral.echtotal>>'
						,'D-M-V-C: <<ALTERAMEIOSPAG.txtDinheiro.value>> - <<ALTERAMEIOSPAG.txtMB.value>> - <<ALTERAMEIOSPAG.txtVisa.value>> - <<ALTERAMEIOSPAG.txtCheque.value>>'
						,<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()), '<<mySite>>', <<myTermNo>>)
				ENDTEXT

				uf_gerais_actgrelha("", "", lcsql)
			ELSE
				uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR OS MEIOS DE PAGAMENTO."+CHR(13)+"POR FAVOR CONTACTE O SUPORTE.", "OK", "", 48)
				RETURN .F.
			ENDIF			
		ENDSCAN 
		

	
	
	ELSE
		SELECT ucrsb_pagcentral
		lcdinheiro = ucrsb_pagcentral.evdinheiro
		lcabrepag = uf_alterameiospag_validaestadofinalpagamento(lcdinheiro)
	
		SELECT ucrsb_pagcentral
		LOCAL lcfechastamp, lcfechauser, lcabatido, lcfechado
		lcfechastamp = ALLTRIM(ucrsb_pagcentral.fechastamp)
		lcfechauser = ucrsb_pagcentral.fechauser
		lcabatido = IIF( .NOT. EMPTY(ucrsb_pagcentral.abatido), 1, 0)
		lcfechado = IIF( .NOT. EMPTY(ucrsb_pagcentral.fechado), 1, 0)

		DO CASE
			CASE lcabrepag==1
				lcfechastamp = ''
				lcfechauser = 0
				lcabatido = 0
				lcfechado = 0
			CASE lcabrepag==2
			IF (lcfechado==0)
				IF (EMPTY(lcfechastamp))
					lcfechastamp = uf_gerais_stamp()
				ENDIF
				lcfechado = 1
			ENDIF
		OTHERWISE
		ENDCASE

		TEXT TO uv_sql TEXTMERGE NOSHOW
			up_faturacao_getPagExternoAtendimento '<<ALLTRIM(ucrsb_pagcentral.nrAtend)>>', 0
		ENDTEXT
		IF !uf_gerais_actGrelha("", "uc_oriPagExterno", uv_sql)
			uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR OS MEIOS DE PAGAMENTO."+CHR(13)+"POR FAVOR CONTACTE O SUPORTE.", "OK", "", 48)
			RETURN .F.
		ENDIF
		
		
		SELECT uc_oriPagExterno	
		
		IF RECCOUNT("uc_oriPagExterno") > 0
			IF uf_gerais_getUmValor("b_pagCentral_ext", "count(*)", "operationID = '" + ALLTRIM(uc_oriPagExterno.operationID) + "' and CONVERT(VARCHAR, ousrdata, 112) <= DATEADD( MONTH, 2, '" + uf_gerais_getDate(uc_oriPagExterno.ousrdata, "SQL") + "') ") > 1
				uf_perguntalt_chama("N�o pode alterar o meio de pagamento em documentos de 'Somar Atendimento'.", "OK", "", 48)
				RETURN .F.
			ENDIF
			
			SELECT uc_oriPagExterno
			GO TOP
			LOCATE FOR uc_oriPagExterno.statePaymentID = 0 AND EMPTY(uc_oriPagExterno.statePaymentIDDesc)
			IF FOUND()
				uf_perguntalt_chama("Existem m�todos de Pagamento Externo por consultar." + CHR(13) + "Por favor verifique os mesmos.", "OK", "", 48)
				RETURN .F.
			ENDIF
			
			SELECT uc_oriPagExterno
			GO TOP
			SCAN FOR uc_oriPagExterno.statePaymentID = 0
				TEXT TO uv_sql TEXTMERGE NOSHOW
	 
					UPDATE 
						b_PagCentral_ext 
					SET 
						statePaymentID = -4, 
						statePaymentIdDesc = 'Cancelada pelo Operador', 
						usrdata = GETDATE(), 
						usrinis = '<<ALLTRIM(m_chinis)>>'
					WHERE
						token = '<<uc_oriPagExterno.token>>'

				ENDTEXT

				IF !uf_gerais_actGrelha("", "", uv_sql)
					uf_perguntalt_chama("ERRO A ATUALIZAR MEIOS DE PAGAMENTO EXTERNO."+CHR(13)+"POR FAVOR CONTACTE O SUPORTE.", "OK", "", 48)
					RETURN .F.
				ENDIF

				SELECT uc_oriPagExterno
			ENDSCAN

		ENDIF

		IF !uf_alterameiospag_geraPagExterno(ucrsb_pagcentral.no, ucrsb_pagcentral.estab, ucrsb_pagcentral.nrAtend)
			uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR OS MEIOS DE PAGAMENTO."+CHR(13)+"POR FAVOR CONTACTE O SUPORTE.", "OK", "", 48)
			RETURN .F.
		ENDIF

		lcsql = ''
		TEXT TO lcsql TEXTMERGE NOSHOW
			UPDATE 
				b_pagcentral 
			SET 
				evdinheiro = <<ALTERAMEIOSPAG.txtDinheiro.value>>
				,epaga2 = <<ALTERAMEIOSPAG.txtMB.value>>
				,epaga1 = <<ALTERAMEIOSPAG.txtVisa.value>>
				,echtotal = <<ALTERAMEIOSPAG.txtCheque.value>>
				,evdinheiro_semTroco = 0
				,etroco = 0
				,epaga3 =  <<ALTERAMEIOSPAG.txtModPag3.value>>
				,epaga4 =  <<ALTERAMEIOSPAG.txtModPag4.value>>
				,epaga5 =  <<ALTERAMEIOSPAG.txtModPag5.value>>
				,epaga6 =  <<ALTERAMEIOSPAG.txtModPag6.value>>
				,fechaStamp = '<<ALLTRIM(lcFechaStamp))>>'
				,fechaUser  = <<lcFechaUser>>
				,abatido    = <<lcAbatido>>
				,fechado    = <<lcFechado>>
			where 
				stamp = '<<ucrsB_pagCentral.stamp>>'
		ENDTEXT
		IF uf_gerais_actgrelha("", "", lcsql)
			SELECT ucrsb_pagcentral
			lcsql = ''
			TEXT TO lcsql TEXTMERGE NOSHOW
				INSERT into B_ocorrencias
					(stamp, linkstamp, tipo, grau, descr,
					ovalor,
					dvalor,
					usr, date, site, terminal)
				values
					(LEFT(newid(),25), '', 'Vendas', 2, 'Alterados meios de pagamento do atendimento: <<ucrsB_pagCentral.nrAtend>>',
					'D-M-V-C: <<ucrsB_pagCentral.evdinheiro>> - <<ucrsB_pagCentral.epaga2>> - <<ucrsB_pagCentral.epaga1>> - <<ucrsB_pagCentral.echtotal>>'
					,'D-M-V-C: <<ALTERAMEIOSPAG.txtDinheiro.value>> - <<ALTERAMEIOSPAG.txtMB.value>> - <<ALTERAMEIOSPAG.txtVisa.value>> - <<ALTERAMEIOSPAG.txtCheque.value>>'
					,<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()), '<<mySite>>', <<myTermNo>>)
			ENDTEXT

			uf_gerais_actgrelha("", "", lcsql)
		ELSE
			uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR OS MEIOS DE PAGAMENTO."+CHR(13)+"POR FAVOR CONTACTE O SUPORTE.", "OK", "", 48)
			RETURN .F.
		ENDIF
	
	
	
	ENDIF
	
*!*		SELECT ucrsb_pagcentral
*!*		lcdinheiro = ucrsb_pagcentral.evdinheiro
*!*		lcabrepag = uf_alterameiospag_validaestadofinalpagamento(lcdinheiro)

*!*		SELECT ucrsb_pagcentral
*!*		LOCAL lcfechastamp, lcfechauser, lcabatido, lcfechado
*!*		lcfechastamp = ALLTRIM(ucrsb_pagcentral.fechastamp)
*!*		lcfechauser = ucrsb_pagcentral.fechauser
*!*		lcabatido = IIF( .NOT. EMPTY(ucrsb_pagcentral.abatido), 1, 0)
*!*		lcfechado = IIF( .NOT. EMPTY(ucrsb_pagcentral.fechado), 1, 0)

*!*		DO CASE
*!*			CASE lcabrepag==1
*!*				lcfechastamp = ''
*!*				lcfechauser = 0
*!*				lcabatido = 0
*!*				lcfechado = 0
*!*			CASE lcabrepag==2
*!*			IF (lcfechado==0)
*!*				IF (EMPTY(lcfechastamp))
*!*					lcfechastamp = uf_gerais_stamp()
*!*				ENDIF
*!*				lcfechado = 1
*!*			ENDIF
*!*		OTHERWISE
*!*		ENDCASE

*!*		TEXT TO uv_sql TEXTMERGE NOSHOW
*!*			up_faturacao_getPagExternoAtendimento '<<ALLTRIM(ucrsb_pagcentral.nrAtend)>>', 0
*!*		ENDTEXT
*!*		IF !uf_gerais_actGrelha("", "uc_oriPagExterno", uv_sql)
*!*			uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR OS MEIOS DE PAGAMENTO."+CHR(13)+"POR FAVOR CONTACTE O SUPORTE.", "OK", "", 48)
*!*			RETURN .F.
*!*		
*!*		
*!*		ENDIF
*!*		
*!*		
*!*		SELECT uc_oriPagExterno
*!*		
*!*		
*!*		
*!*		
*!*		
*!*		
*!*		
*!*		
*!*		
*!*		IF RECCOUNT("uc_oriPagExterno") > 0
*!*			IF uf_gerais_getUmValor("b_pagCentral_ext", "count(*)", "operationID = '" + ALLTRIM(uc_oriPagExterno.operationID) + "' and CONVERT(VARCHAR, ousrdata, 112) <= DATEADD( MONTH, 2, '" + uf_gerais_getDate(uc_oriPagExterno.ousrdata, "SQL") + "') ") > 1
*!*				uf_perguntalt_chama("N�o pode alterar o meio de pagamento em documentos de 'Somar Atendimento'.", "OK", "", 48)
*!*				RETURN .F.
*!*			ENDIF
*!*			
*!*			SELECT uc_oriPagExterno
*!*			GO TOP
*!*			LOCATE FOR uc_oriPagExterno.statePaymentID = 0 AND EMPTY(uc_oriPagExterno.statePaymentIDDesc)
*!*			IF FOUND()
*!*				uf_perguntalt_chama("Existem m�todos de Pagamento Externo por consultar." + CHR(13) + "Por favor verifique os mesmos.", "OK", "", 48)
*!*				RETURN .F.
*!*			ENDIF
*!*			
*!*			SELECT uc_oriPagExterno
*!*			GO TOP
*!*			SCAN FOR uc_oriPagExterno.statePaymentID = 0
*!*				TEXT TO uv_sql TEXTMERGE NOSHOW
*!*	 
*!*					UPDATE 
*!*						b_PagCentral_ext 
*!*					SET 
*!*						statePaymentID = -4, 
*!*						statePaymentIdDesc = 'Cancelada pelo Operador', 
*!*						usrdata = GETDATE(), 
*!*						usrinis = '<<ALLTRIM(m_chinis)>>'
*!*					WHERE
*!*						token = '<<uc_oriPagExterno.token>>'

*!*				ENDTEXT

*!*				IF !uf_gerais_actGrelha("", "", uv_sql)
*!*					uf_perguntalt_chama("ERRO A ATUALIZAR MEIOS DE PAGAMENTO EXTERNO."+CHR(13)+"POR FAVOR CONTACTE O SUPORTE.", "OK", "", 48)
*!*					RETURN .F.
*!*				ENDIF

*!*				SELECT uc_oriPagExterno
*!*			ENDSCAN

*!*		ENDIF

*!*		IF !uf_alterameiospag_geraPagExterno(ucrsb_pagcentral.no, ucrsb_pagcentral.estab, ucrsb_pagcentral.nrAtend)
*!*			uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR OS MEIOS DE PAGAMENTO."+CHR(13)+"POR FAVOR CONTACTE O SUPORTE.", "OK", "", 48)
*!*			RETURN .F.
*!*		ENDIF

*!*		lcsql = ''
*!*		TEXT TO lcsql TEXTMERGE NOSHOW
*!*			UPDATE 
*!*				b_pagcentral 
*!*			SET 
*!*				evdinheiro = <<ALTERAMEIOSPAG.txtDinheiro.value>>
*!*				,epaga2 = <<ALTERAMEIOSPAG.txtMB.value>>
*!*				,epaga1 = <<ALTERAMEIOSPAG.txtVisa.value>>
*!*				,echtotal = <<ALTERAMEIOSPAG.txtCheque.value>>
*!*				,evdinheiro_semTroco = 0
*!*				,etroco = 0
*!*				,epaga3 =  <<ALTERAMEIOSPAG.txtModPag3.value>>
*!*				,epaga4 =  <<ALTERAMEIOSPAG.txtModPag4.value>>
*!*				,epaga5 =  <<ALTERAMEIOSPAG.txtModPag5.value>>
*!*				,epaga6 =  <<ALTERAMEIOSPAG.txtModPag6.value>>
*!*				,fechaStamp = '<<ALLTRIM(lcFechaStamp))>>'
*!*				,fechaUser  = <<lcFechaUser>>
*!*				,abatido    = <<lcAbatido>>
*!*				,fechado    = <<lcFechado>>
*!*			where 
*!*				stamp = '<<ucrsB_pagCentral.stamp>>'
*!*		ENDTEXT
*!*		IF uf_gerais_actgrelha("", "", lcsql)
*!*			SELECT ucrsb_pagcentral
*!*			lcsql = ''
*!*			TEXT TO lcsql TEXTMERGE NOSHOW
*!*				INSERT into B_ocorrencias
*!*					(stamp, linkstamp, tipo, grau, descr,
*!*					ovalor,
*!*					dvalor,
*!*					usr, date, site, terminal)
*!*				values
*!*					(LEFT(newid(),25), '', 'Vendas', 2, 'Alterados meios de pagamento do atendimento: <<ucrsB_pagCentral.nrAtend>>',
*!*					'D-M-V-C: <<ucrsB_pagCentral.evdinheiro>> - <<ucrsB_pagCentral.epaga2>> - <<ucrsB_pagCentral.epaga1>> - <<ucrsB_pagCentral.echtotal>>'
*!*					,'D-M-V-C: <<ALTERAMEIOSPAG.txtDinheiro.value>> - <<ALTERAMEIOSPAG.txtMB.value>> - <<ALTERAMEIOSPAG.txtVisa.value>> - <<ALTERAMEIOSPAG.txtCheque.value>>'
*!*					,<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()), '<<mySite>>', <<myTermNo>>)
*!*			ENDTEXT

*!*			uf_gerais_actgrelha("", "", lcsql)
*!*		ELSE
*!*			uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR OS MEIOS DE PAGAMENTO."+CHR(13)+"POR FAVOR CONTACTE O SUPORTE.", "OK", "", 48)
*!*			RETURN .F.
*!*		ENDIF
	uf_perguntalt_chama("MEIOS DE PAGAMENTO ALTERADOS COM SUCESSO.", "OK", "", 64)
	uf_alterameiospag_sair()
ENDFUNC
**
FUNCTION uf_alterameiospag_validaestadofinalpagamento
 LPARAMETERS lcdinheiro
 IF uf_gerais_validapagcentraldinheiro()
    IF (lcdinheiro==0) .AND. alterameiospag.txtdinheiro.value>0
       RETURN 1
    ENDIF
    IF (lcdinheiro<>0) .AND. alterameiospag.txtdinheiro.value==0
       RETURN 2
    ENDIF
 ENDIF
 RETURN 0
ENDFUNC
**
FUNCTION uf_alterameiospag_modopagamento
 LPARAMETERS lcmodop, lcCursorAtendimento
 lntotalsel = alterameiospag.txtdinheiro.value+alterameiospag.txtmb.value+alterameiospag.txtvisa.value+alterameiospag.txtcheque.value+alterameiospag.txtmodpag3.value+alterameiospag.txtmodpag4.value+alterameiospag.txtmodpag5.value+alterameiospag.txtmodpag6.value
 IF lntotalsel==0 .AND.  .NOT. EMPTY(alterameiospag.txtvalatend.value)
    DO CASE
       CASE lcmodop=="dinheiro"
          alterameiospag.txtdinheiro.value = alterameiospag.txtvalatend.value
       CASE lcmodop=="mb"
          alterameiospag.txtmb.value = alterameiospag.txtvalatend.value
       CASE lcmodop=="visa"
          alterameiospag.txtvisa.value = alterameiospag.txtvalatend.value
       CASE lcmodop=="cheque"
          alterameiospag.txtcheque.value = alterameiospag.txtvalatend.value
       CASE lcmodop=="modpag3"

          IF uf_gerais_getUmValor("b_modoPag", "pagamentoExterno", "ref = '05'")
            IF !uf_alterameiospag_modoPagExterno("05")
               RETURN .F.
            ENDIF
          ENDIF

          alterameiospag.txtmodpag3.value = alterameiospag.txtvalatend.value
       CASE lcmodop=="modpag4"

          IF uf_gerais_getUmValor("b_modoPag", "pagamentoExterno", "ref = '06'")
            IF !uf_alterameiospag_modoPagExterno("06")
               RETURN .F.
            ENDIF
          ENDIF

          alterameiospag.txtmodpag4.value = alterameiospag.txtvalatend.value
       CASE lcmodop=="modpag5"

          IF uf_gerais_getUmValor("b_modoPag", "pagamentoExterno", "ref = '07'")
            IF !uf_alterameiospag_modoPagExterno("07")
               RETURN .F.
            ENDIF
          ENDIF

          alterameiospag.txtmodpag5.value = alterameiospag.txtvalatend.value
       CASE lcmodop=="modpag6"

          IF uf_gerais_getUmValor("b_modoPag", "pagamentoExterno", "ref = '08'")
            IF !uf_alterameiospag_modoPagExterno("08")
               RETURN .F.
            ENDIF
          ENDIF

          alterameiospag.txtmodpag6.value = alterameiospag.txtvalatend.value
       OTHERWISE
          uf_perguntalt_chama("PAR�METRO DE ALTERAMEIOSPAG INV�LIDO", "OK", "", 64)
          RETURN .F.
    ENDCASE
 ENDIF
 DO CASE
    CASE lcmodop=="dinheiro"
       IF EMPTY(lcCursorAtendimento)
       		IF ROUND(VAL(astr(lntotalsel, 8, 2)), 2)<ROUND(alterameiospag.txtvalatend.value, 2) .AND. EMPTY(alterameiospag.txtdinheiro.value)
		          alterameiospag.txtdinheiro.value = ROUND(alterameiospag.txtvalatend.value, 2)-ROUND(VAL(astr(lntotalsel, 8, 2)), 2)
	       	ENDIF
       
       ELSE
       		alterameiospag.txtdinheiro.value = alterameiospag.txtvalatend.value
			 WITH alterameiospag
				.txtmb.value = 0
			    .txtvisa.value = 0
			    .txtcheque.value = 0
			    .txtmodpag3.value = 0
			    .txtmodpag4.value = 0
			    .txtmodpag5.value = 0
			    .txtmodpag6.value = 0       		
			ENDWITH 
       ENDIF 
       alterameiospag.txtdinheiro.setfocus
    CASE lcmodop=="mb"
       IF EMPTY(lcCursorAtendimento)
	       IF ROUND(VAL(astr(lntotalsel, 8, 2)), 2)<ROUND(alterameiospag.txtvalatend.value, 2) .AND. EMPTY(alterameiospag.txtmb.value)
	          alterameiospag.txtmb.value = ROUND(alterameiospag.txtvalatend.value, 2)-ROUND(VAL(astr(lntotalsel, 8, 2)), 2)
	       ENDIF
       ELSE
       		alterameiospag.txtmb.value = alterameiospag.txtvalatend.value
			 WITH alterameiospag
				.txtdinheiro.value = 0
			    .txtvisa.value = 0
			    .txtcheque.value = 0
			    .txtmodpag3.value = 0
			    .txtmodpag4.value = 0
			    .txtmodpag5.value = 0
			    .txtmodpag6.value = 0       		
       		ENDWITH 
       ENDIF
       alterameiospag.txtmb.setfocus
    CASE lcmodop=="visa"
       IF EMPTY(lcCursorAtendimento)
	       IF ROUND(VAL(astr(lntotalsel, 8, 2)), 2)<ROUND(alterameiospag.txtvalatend.value, 2) .AND. EMPTY(alterameiospag.txtvisa.value)
	          alterameiospag.txtvisa.value = ROUND(alterameiospag.txtvalatend.value, 2)-ROUND(VAL(astr(lntotalsel, 8, 2)), 2)
	       ENDIF    
       ELSE
       		alterameiospag.txtvisa.value = alterameiospag.txtvalatend.value
			 WITH alterameiospag
				.txtdinheiro.value = 0
			    .txtmb.value = 0
			    .txtcheque.value = 0
			    .txtmodpag3.value = 0
			    .txtmodpag4.value = 0
			    .txtmodpag5.value = 0
			    .txtmodpag6.value = 0       		
			ENDWITH 
       ENDIF
       alterameiospag.txtvisa.setfocus
    CASE lcmodop=="cheque"
       IF EMPTY(lcCursorAtendimento)    
	       IF ROUND(VAL(astr(lntotalsel, 8, 2)), 2)<ROUND(alterameiospag.txtvalatend.value, 2) .AND. EMPTY(alterameiospag.txtcheque.value)
	          alterameiospag.txtcheque.value = ROUND(alterameiospag.txtvalatend.value, 2)-ROUND(VAL(astr(lntotalsel, 8, 2)), 2)
	       ENDIF
       ELSE
       		alterameiospag.txtcheque.value = alterameiospag.txtvalatend.value
			 WITH alterameiospag
				.txtdinheiro.value = 0
			    .txtmb.value = 0
			    .txtvisa.value = 0
			    .txtmodpag3.value = 0
			    .txtmodpag4.value = 0
			    .txtmodpag5.value = 0
			    .txtmodpag6.value = 0       		
			ENDWITH 
       ENDIF
       alterameiospag.txtcheque.setfocus
    CASE lcmodop=="modpag3"
       IF EMPTY(lcCursorAtendimento)
	       IF ROUND(VAL(astr(lntotalsel, 8, 2)), 2)<ROUND(alterameiospag.txtvalatend.value, 2) .AND. EMPTY(alterameiospag.txtmodpag3.value)

	          IF uf_gerais_getUmValor("b_modoPag", "pagamentoExterno", "ref = '05'")
	            IF !uf_alterameiospag_modoPagExterno("05")
	               RETURN .F.
	            ENDIF
	          ENDIF

	          alterameiospag.txtmodpag3.value = ROUND(alterameiospag.txtvalatend.value, 2)-ROUND(VAL(astr(lntotalsel, 8, 2)), 2)
	       ENDIF
	       
       ELSE
       		alterameiospag.txtmodpag3.value = alterameiospag.txtvalatend.value
			WITH alterameiospag
				.txtdinheiro.value = 0
			    .txtmb.value = 0
			    .txtvisa.value = 0
			    .txtcheque.value = 0
			    .txtmodpag4.value = 0
			    .txtmodpag5.value = 0
			    .txtmodpag6.value = 0       		
       		ENDWITH 
       ENDIF
       alterameiospag.txtmodpag3.setfocus
    CASE lcmodop=="modpag4"
       IF EMPTY(lcCursorAtendimento)
	       IF ROUND(VAL(astr(lntotalsel, 8, 2)), 2)<ROUND(alterameiospag.txtvalatend.value, 2) .AND. EMPTY(alterameiospag.txtmodpag4.value)

	          IF uf_gerais_getUmValor("b_modoPag", "pagamentoExterno", "ref = '06'")
	            IF !uf_alterameiospag_modoPagExterno("06")
	               RETURN .F.
	            ENDIF
	          ENDIF

	          alterameiospag.txtmodpag4.value = ROUND(alterameiospag.txtvalatend.value, 2)-ROUND(VAL(astr(lntotalsel, 8, 2)), 2)
	       ENDIF
	       
       ELSE
       		alterameiospag.txtmodpag4.value = alterameiospag.txtvalatend.value
			 WITH alterameiospag
				.txtdinheiro.value = 0
			    .txtmb.value = 0
			    .txtvisa.value = 0
			    .txtcheque.value = 0
			    .txtmodpag3.value = 0
			    .txtmodpag5.value = 0
			    .txtmodpag6.value = 0       		
			ENDWITH 
       ENDIF
       alterameiospag.txtmodpag4.setfocus
    CASE lcmodop=="modpag5"
       IF EMPTY(lcCursorAtendimento)
	       IF ROUND(VAL(astr(lntotalsel, 8, 2)), 2)<ROUND(alterameiospag.txtvalatend.value, 2) .AND. EMPTY(alterameiospag.txtmodpag5.value)

	          IF uf_gerais_getUmValor("b_modoPag", "pagamentoExterno", "ref = '07'")
	            IF !uf_alterameiospag_modoPagExterno("07")
	               RETURN .F.
	            ENDIF
	          ENDIF
	          alterameiospag.txtmodpag5.value = ROUND(alterameiospag.txtvalatend.value, 2)-ROUND(VAL(astr(lntotalsel, 8, 2)), 2)
	       ENDIF

       ELSE
       		alterameiospag.txtmodpag5.value = alterameiospag.txtvalatend.value
			 WITH alterameiospag
				.txtdinheiro.value = 0
			    .txtmb.value = 0
			    .txtvisa.value = 0
			    .txtcheque.value = 0
			    .txtmodpag3.value = 0
			    .txtmodpag4.value = 0
			    .txtmodpag6.value = 0       		
			ENDWITH 
       ENDIF
       
       alterameiospag.txtmodpag5.setfocus
    CASE lcmodop=="modpag6"
       IF EMPTY(lcCursorAtendimento)
	       IF ROUND(VAL(astr(lntotalsel, 8, 2)), 2)<ROUND(alterameiospag.txtvalatend.value, 2) .AND. EMPTY(alterameiospag.txtmodpag6.value)

	          IF uf_gerais_getUmValor("b_modoPag", "pagamentoExterno", "ref = '08'")
	            IF !uf_alterameiospag_modoPagExterno("08")
	               RETURN .F.
	            ENDIF
	          ENDIF

	          alterameiospag.txtmodpag6.value = ROUND(alterameiospag.txtvalatend.value, 2)-ROUND(VAL(astr(lntotalsel, 8, 2)), 2)
	       ENDIF
       ELSE
       		alterameiospag.txtmodpag6.value = alterameiospag.txtvalatend.value
			 WITH alterameiospag
				.txtdinheiro.value = 0
			    .txtmb.value = 0
			    .txtvisa.value = 0
			    .txtcheque.value = 0
			    .txtmodpag3.value = 0
			    .txtmodpag4.value = 0
			    .txtmodpag5.value = 0
			ENDWITH 
       ENDIF
       alterameiospag.txtmodpag6.setfocus
    OTHERWISE
       uf_perguntalt_chama("PAR�METRO DE ALTERAMEIOSPAG INV�LIDO", "OK", "", 64)
       RETURN .F.
 ENDCASE
ENDFUNC
**
PROCEDURE uf_alterameiospag_resetvaloresmodopag
 WITH alterameiospag
    .txtdinheiro.value = 0
    .txtmb.value = 0
    .txtvisa.value = 0
    .txtcheque.value = 0
    .txtmodpag3.value = 0
    .txtmodpag4.value = 0
    .txtmodpag5.value = 0
    .txtmodpag6.value = 0
 ENDWITH
ENDPROC
**

FUNCTION uf_alterameiospag_modoPagExterno
	LPARAMETERS uv_modoPag

	IF EMPTY(uv_modoPag) OR !USED("ucrsB_pagCentral")
		RETURN .F.
	ENDIF

   SELECT ucrsB_pagCentral

	IF ALTERAMEIOSPAG.txtvalatend.value < 0
		uf_perguntalt_chama("N�o pode utilizar este modo de pagamento para devolu��es.","OK","",16)
		RETURN .F.
	ENDIF

	IF !uf_pagExterno_chama(uv_modoPag, ucrsB_pagCentral.no, ucrsB_pagCentral.estab)
		RETURN .F.
	ENDIF

	IF !USED("uc_dadosPagExterno" + ALLTRIM(uv_modoPag))
		RETURN .F.
	ENDIF

	RETURN .T.

ENDFUNC

FUNCTION uf_alterameiospag_geraPagExterno
	LPARAMETERS uv_no, uv_estab, uv_nrAtend

	IF EMPTY(uv_no) OR TYPE("uv_estab") <> "N" OR EMPTY(uv_nrAtend)
		RETURN .F.
	ENDIF

	IF !uf_gerais_actGrelha("","uc_modosPagExterno", "exec up_vendas_checkModoPagExterno")

		uf_perguntalt_chama("Erro a verificar modos de pagamento externo. Por favor contacte o suporte.","OK","",16)
		RETURN .F.

	ENDIF

	IF RECCOUNT("uc_modosPagExterno") > 0

		CREATE CURSOR uc_pagsExterno(modoPag C(10), tlmvl C(50), email C(254), duracao C(50), descricao M, valor N(14,2), tipoPagExt N(3), tipoPagExtDesc C(100), receiverID C(100), receiverName C(100), no N(9), estab N(5), nrAtend C(20))
		LOCAL uv_valor, uv_curCursor

		SELECT uc_modosPagExterno
		GO TOP

		SCAN

			uv_valor = 0

			DO CASE
				CASE ALLTRIM(uc_modosPagExterno.ref) = "05"

					uv_valor = alterameiospag.txtModPag3.value

					IF uv_valor <> 0 AND !USED("uc_dadosPagExterno05") 
						uf_perguntalt_chama("N�o preencheu os dados para Pagamento Externo:" + CHR(13) + ALLTRIM(alterameiospag.btnModPag3.Label7.caption),"OK","",16)
						RETURN .F.
					ENDIF
					
				CASE ALLTRIM(uc_modosPagExterno.ref) = "06"

					uv_valor = alterameiospag.txtModPag4.value

					IF uv_valor <> 0 AND !USED("uc_dadosPagExterno06") 
						uf_perguntalt_chama("N�o preencheu os dados para Pagamento Externo:" + CHR(13) + ALLTRIM(alterameiospag.btnModPag4.Label7.caption),"OK","",16)
						RETURN .F.
					ENDIF

				CASE ALLTRIM(uc_modosPagExterno.ref) = "07"

					uv_valor = alterameiospag.txtModPag5.value

					IF uv_valor <> 0 AND !USED("uc_dadosPagExterno07") 
						uf_perguntalt_chama("N�o preencheu os dados para Pagamento Externo:" + CHR(13) + ALLTRIM(alterameiospag.btnModPag5.Label7.caption),"OK","",16)
						RETURN .F.
					ENDIF

				CASE ALLTRIM(uc_modosPagExterno.ref) = "08"

					uv_valor = alterameiospag.txtModPag6.value

					IF uv_valor <> 0 AND !USED("uc_dadosPagExterno08") 
						uf_perguntalt_chama("N�o preencheu os dados para Pagamento Externo:" + CHR(13) + ALLTRIM(alterameiospag.btnModPag6.Label7.caption),"OK","",16)
						RETURN .F.
					ENDIF

				OTHERWISE
					LOOP
			ENDCASE

			uv_curCursor = "uc_dadosPagExterno" + ALLTRIM(uc_modosPagExterno.ref)

			IF uv_valor <> 0 AND USED(uv_curCursor)

					SELECT uc_pagsExterno
					APPEND BLANK

					REPLACE uc_pagsExterno.modoPag WITH ALLTRIM(uc_modosPagExterno.ref)
					REPLACE uc_pagsExterno.tlmvl WITH ALLTRIM(&uv_curCursor..tlmvl)
					REPLACE uc_pagsExterno.email WITH ALLTRIM(&uv_curCursor..email)
					REPLACE uc_pagsExterno.duracao WITH ALLTRIM(&uv_curCursor..duracao)
					REPLACE uc_pagsExterno.descricao WITH ALLTRIM(&uv_curCursor..descricao)
					REPLACE uc_pagsExterno.valor WITH uv_valor
					REPLACE uc_pagsExterno.tipoPagExt WITH uc_modosPagExterno.tipoPagExt
					REPLACE uc_pagsExterno.tipoPagExtDesc WITH uc_modosPagExterno.tipoPagExtDesc
					REPLACE uc_pagsExterno.receiverID WITH uc_modosPagExterno.receiverID
					REPLACE uc_pagsExterno.receiverName WITH uc_modosPagExterno.receiverName
					REPLACE uc_pagsExterno.no WITH uv_no
					REPLACE uc_pagsExterno.estab WITH uv_estab
					REPLACE uc_pagsExterno.nrAtend WITH ALLTRIM(uv_nrAtend)

			ENDIF


			SELECT uc_modosPagExterno
		ENDSCAN

		SELECT uc_pagsExterno
      
		IF RECCOUNT("uc_pagsExterno") > 0
			IF !uf_pagamento_reqPagExterno()
				uf_perguntalt_chama("N�o foi poss�vel criar os pagamentos externos. Por favor contacte o suporte.","OK","",16)
				RETURN .F.
			ENDIF
		ENDIF	

	ENDIF

	RETURN .T.

ENDFUNC