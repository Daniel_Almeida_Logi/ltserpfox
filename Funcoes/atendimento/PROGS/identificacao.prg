**
FUNCTION uf_identificacao_chama
 LPARAMETERS lcmarcacoes
 PUBLIC myidentificacaomarcacoes
 myidentificacaomarcacoes = lcmarcacoes
 IF EMPTY(lcmarcacoes)
    IF uf_gerais_validapermuser(ch_userno, ch_grupo, 'Atendimento - Abrir Atendimento')
       IF  .NOT. (TYPE("ATENDIMENTO")=="U")
          uf_perguntalt_chama("N�O PODE ABRIR O ECR� DE IDENTIFICA��O PARA O ATENDIMENTO ENQUANTO O ECR� DE ATENDIMENTO ESTIVER ABERTO.", "OK", "", 48)
          RETURN .F.
       ENDIF
       IF  .NOT. (TYPE("FACTURACAO")=="U")
          uf_perguntalt_chama("N�O PODE ABRIR O ECR� DE IDENTIFICA��O PARA O ATENDIMENTO ENQUANTO O ECR� DE FACTURA��O ESTIVER ABERTO.", "OK", "", 48)
          RETURN .F.
       ENDIF
       IF  .NOT. (TYPE("DOCUMENTOS")=="U")
          uf_perguntalt_chama("N�O PODE ABRIR O ECR� DE IDENTIFICA��O PARA O ATENDIMENTO ENQUANTO O ECR� DE DOCUMENTOS ESTIVER ABERTO.", "OK", "", 48)
          RETURN .F.
       ENDIF
       IF  .NOT. (TYPE("ARQUIVO_DIGITAL")=="U")
          uf_perguntalt_chama("N�O PODE ABRIR O ECR� DE IDENTIFICA��O PARA O ATENDIMENTO ENQUANTO O ECR� DE ARQUIVO DIGITAL DE TAL�ES ESTIVER ABERTO.", "OK", "", 48)
          RETURN .F.
       ENDIF
    ELSE
       uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL DE ATENDIMENTO.", "OK", "", 48)
       RETURN .F.
    ENDIF
 ENDIF
 PUBLIC mycxstamp, mysacodinheiro, mycxuser
 STORE '' TO mycxstamp, mycxuser
 STORE .F. TO mysacodinheiro
 mysacodinheiro = uf_gerais_getparameter('ADM0000000040', 'bool')
 IF  .NOT. TYPE("atendimento")=="U"
    atendimento.show()
 ELSE
    IF TYPE("identificacao")=="U"
       DO FORM identificacao
    ELSE
       identificacao.show
    ENDIF
 ENDIF
 WITH identificacao
    .txtloja.value = ALLTRIM(mysite)
    .txtterminal.value = ALLTRIM(myterm)
    .txtarmazem.value = myarmazem
    .txtrobo.value = ''
 ENDWITH
 IF myoffline
    identificacao.lbloffline.visible = .T.
 ENDIF
 IF TYPE("identificacao.menu1.caixa")=="U"
    identificacao.menu1.sair.image1.picture = mypath+"\imagens\icons\sair_w.png"
    identificacao.menu1.gravar.img1.picture = mypath+"\imagens\icons\entrar_w.png"
    identificacao.menu1.adicionaopcao("caixa", "Fechar Cx", mypath+"\imagens\icons\fecharCaixa_w.png", "uf_identificacao_FecharCaixa with .t.", "")
    identificacao.menu1.adicionaopcao("fechoDia", "Gest�o Cx", mypath+"\imagens\icons\gestao_caixa.png", "uf_identificacao_fechodia", "")
    IF mypagcentral==.T.
       identificacao.menu1.adicionaopcao("pagcentral", "Pag. Centraliz.", mypath+"\imagens\icons\pag_central.png", "uf_identificacao_pagamentosCentral", "")
    ENDIF
 ENDIF
 identificacao.menu1.estado("", "", "Entrar", .T.)
 uf_identificacao_verificacaixa()
 uf_identificacao_verificarobot()
 uf_identificacao_verificacaixasabertas()
ENDFUNC
**
PROCEDURE uf_identificacao_verificacaixasabertas
 IF (uf_gerais_actgrelha("", "uCrsCxAbertas", "select top 1 cxstamp from cx (nolock) where dfechar = '19000101' and dabrir != convert(date,GETDATE()) and site = '"+mysite+"'"))
    SELECT ucrscxabertas
    IF RECCOUNT("uCrsCxAbertas")>0
       identificacao.lblcxaberta.visible = .T.
    ELSE
       identificacao.lblcxaberta.visible = .F.
    ENDIF
    IF USED("uCrsCxAbertas")
       fecha("uCrsCxAbertas")
    ENDIF
 ENDIF
ENDPROC
**
PROCEDURE uf_identificacao_fecharcaixa
 LPARAMETERS lcbool
 IF lcbool
    uf_caixas_gerircaixa(.T.)
 ELSE
    uf_caixas_gerircaixa(.F.)
 ENDIF
 uf_identificacao_verificacaixa()
ENDPROC
**
FUNCTION uf_identificacao_verificacaixa
 IF ucrse1.gestao_cx_operador=.T.
    IF uf_gerais_actgrelha("", "uCrsCx", [exec up_caixa_verificaCaixaAberta ']+ALLTRIM(mysite)+[', ']+ALLTRIM(myterm)+[', 1])
       IF RECCOUNT("uCrsCx")>0
          IF TYPE("identificacao")<>"U"
             identificacao.menu1.caixa.config("Fechar Cx", mypath+"\imagens\icons\fecharCaixa_w.png", "uf_identificacao_FecharCaixa with .t.", "")
             identificacao.txtdabrir.value = ucrscx.dabrir
             identificacao.txtausername.value = ucrscx.ausername
          ENDIF
          PUBLIC mycxstamp, mycxuser, myssstamp
          mycxstamp = ucrscx.cxstamp
          mycxuser = ucrscx.ausername
          myssstamp = ucrscx.ssstamp
       ELSE
          IF TYPE("identificacao")<>"U"
             identificacao.menu1.caixa.config("Abrir Cx", mypath+"\imagens\icons\abrirCaixa_w.png", "uf_identificacao_FecharCaixa with .f.", "")
             identificacao.txtdabrir.value = ''
             identificacao.txtausername.value = ''
          ENDIF
          PUBLIC mycxstamp, mycxuser, myssstamp
          mycxstamp = ""
          mycxuser = ""
          myssstamp = ""
       ENDIF
       fecha("uCrsCx")
    ELSE
       uf_perguntalt_chama("OCORREU UM ERRO A CRIAR SESS�O DE CAIXA.", "OK", "", 16)
       RETURN .F.
    ENDIF
 ELSE
    IF uf_gerais_actgrelha("", "uCrsCx", [exec up_caixa_verificaCaixaAberta ']+ALLTRIM(mysite)+[', ']+ALLTRIM(myterm)+[', 0])
       IF RECCOUNT("uCrsCx")>0
          IF TYPE("identificacao")<>"U"
             identificacao.menu1.caixa.config("Fechar Cx", mypath+"\imagens\icons\fecharCaixa_w.png", "uf_identificacao_FecharCaixa with .t.", "")
             identificacao.txtdabrir.value = ucrscx.dabrir
             identificacao.txtausername.value = ucrscx.ausername
          ENDIF
          PUBLIC mycxstamp, mycxuser, myssstamp
          mycxstamp = ucrscx.cxstamp
          mycxuser = ucrscx.ausername
          myssstamp = ucrscx.ssstamp
       ELSE
          IF TYPE("identificacao")<>"U"
             identificacao.menu1.caixa.config("Abrir Cx", mypath+"\imagens\icons\abrirCaixa_w.png", "uf_identificacao_FecharCaixa with .f.", "")
             identificacao.txtdabrir.value = ''
             identificacao.txtausername.value = ''
          ENDIF
          PUBLIC mycxstamp, mycxuser, myssstamp
          mycxstamp = ""
          mycxuser = ""
          myssstamp = ""
       ENDIF
       fecha("uCrsCx")
    ELSE
       uf_perguntalt_chama("OCORREU UM ERRO A CRIAR SESS�O DE CAIXA.", "OK", "", 16)
       RETURN .F.
    ENDIF
 ENDIF
ENDFUNC
**
PROCEDURE uf_identificacao_verificarobot
 LOCAL lcvalidarobot, lcrobotstate
 myrobotstate = .F.
 DO CASE
    CASE myusarobot .OR. myusarobotapostore
       lcrobotstate = uf_robot_validatecnilabrobotstate()
       DO CASE
          CASE lcrobotstate='00'
             identificacao.txtrobo.value = 'OK'
             myrobotstate = .T.
          CASE lcrobotstate='02'
             identificacao.txtrobo.value = 'Parcialmente OK'
             myrobotstate = .T.
          OTHERWISE
             identificacao.txtrobo.value = 'N�o OK'
       ENDCASE
    CASE myusafarmax
       identificacao.txtrobo.value = 'ACTIVO'
    CASE myusafarmax
       identificacao.txtrobo.value = 'ACTIVO'
    CASE myusarobotconsis
       identificacao.txtrobo.value = 'ACTIVO'
       myrobotstate = .T.
    OTHERWISE
       identificacao.txtrobo.value = 'N/D'
 ENDCASE
ENDPROC
**
PROCEDURE uf_identificacao_fechodia
 IF (uf_gerais_validapermuser(ch_userno, ch_grupo, 'Fecho do Dia - Visualizar'))
    uf_gestaocaixas_chama()
 ELSE
    uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL FECHO DO DIA.", "OK", "", 48)
 ENDIF
ENDPROC
**
FUNCTION uf_identificacao_gravar
 IF EMPTY(mysite)
    uf_perguntalt_chama("PARA ABRIR O ECR� DE ATENDIMENTO TEM DE TER UMA SESS�O DE CAIXA ABERTA.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF EMPTY(mytermno) .OR. EMPTY(myarmazem)
    uf_perguntalt_chama("O TERMINAL E ARMAZ�M DESTE POSTO N�O EST�O CONFIGURADOS.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF EMPTY(mycxstamp) .OR. EMPTY(ch_vendnm)
    uf_perguntalt_chama("POR FAVOR ABRA UMA SESS�O DE CAIXA.", "OK", "", 48)
    RETURN .F.
 ENDIF
 uf_identificacao_sair(.T.)
 IF EMPTY(myidentificacaomarcacoes)
    uf_atendimento_chama()
    IF uf_gerais_getparameter("ADM0000000189", "BOOL")
       _SCREEN.width = _SCREEN.width-1
       _SCREEN.width = _SCREEN.width+1
    ENDIF
 ENDIF
ENDFUNC
**
PROCEDURE uf_identificacao_sair
 LPARAMETERS lcgravou
 identificacao.release
 RELEASE identificacao
 RELEASE mysacodinheiro
 IF  .NOT. EMPTY(myidentificacaomarcacoes) .AND. EMPTY(lcgravou)
    mycxstamp = ""
 ENDIF
ENDPROC
**
PROCEDURE uf_identificacao_pagamentoscentral
 uf_identificacao_sair()
 uf_pagcentral_chama()
ENDPROC
**
