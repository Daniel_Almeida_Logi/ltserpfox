**
PROCEDURE uf_contagemnumerario_chama

 IF TYPE("contagemnumerario")=="U"
    DO FORM contagemnumerario
 ELSE
    contagemnumerario.show
 ENDIF

ENDPROC
**
PROCEDURE uf_contagemnumerario_initCursores
  
 LOCAL lcSstamp
lcSstamp = ''
IF(USED("uCrsFechoDia"))
	SELECT uCrsFechoDia
	lcSstamp = ALLTRIM(uCrsFechoDia.Ssstamp) 
ENDIF   
  
IF((uf_gerais_getUmValor("contnumerario", "count(*)", "ssstamp = '" + ALLTRIM(lcSstamp ) + "'") >0))

	FECHA("ucrsContNotas")
 	FECHA("ucrsContMoedas")
	
    TEXT TO lcSql TEXTMERGE NOSHOW
   		 exec up_caixa_contagemnumerario 'NOTAS','<<lcSstamp >>'
 	ENDTEXT
	
    IF (!uf_gerais_actGrelha("","ucrsContNotas",lcSql))
        uf_perguntalt_chama("N�o foi poss�vel Construir o cursor para contagem de numer�rio - Notas. Por favor contacte o suporte.", "OK", "", 32)
    ENDIF

    TEXT TO lcSql TEXTMERGE NOSHOW
        exec up_caixa_contagemnumerario 'MOEDAS','<<lcSstamp >>'
 	ENDTEXT
 		
    IF (!uf_gerais_actGrelha("","ucrsContMoedas",lcSql))
        uf_perguntalt_chama("N�o foi poss�vel Construir o cursor para contagem de numer�rio - Moedas. Por favor contacte o suporte.", "OK", "", 32)
    ENDIF
    
ENDIF

ENDPROC
**
PROCEDURE uf_contagemnumerario_init
 LOCAL lcsql
 uf_contagemnumerario_somatotais()
 contagemnumerario.menu1.adicionaopcao("actualizar", "Actualizar", mypath+"\imagens\icons\actualizar_w.png", "uf_contagemnumerario_somatotais_top", "F1")
 contagemnumerario.menu1.estado("", "", "Gravar", .T.)
ENDPROC
**
FUNCTION uf_contagemnumerario_sair
 LPARAMETERS lcopcao
 IF contagemnumerario.txttotcont.value<>0 .AND. lcopcao=.F.
    IF  .NOT. uf_perguntalt_chama("Pretende ignorar os valores j� lan�ados?", "Sim", "N�o")
       RETURN .F.
    ENDIF
 ENDIF
 IF lcopcao=.F.
    SELECT ucrscontmoedas
    REPLACE ucrscontmoedas.contagem WITH 0 ALL
    SELECT ucrscontnotas
    REPLACE ucrscontnotas.contagem WITH 0 ALL
    
    uf_contagemnumerario_initCursores()
    
 ENDIF
 
 contagemnumerario.hide
 contagemnumerario.release
ENDFUNC
**
PROCEDURE uf_contagemnumerario_gravar
 uf_contagemnumerario_somatotais_top()
 gestaosessaocaixa.txtdinheiro.value = ASTR(contagemnumerario.txttotcont.value,16,2)
 uf_contagemnumerario_sair(.T.)
ENDPROC
**
PROCEDURE uf_contagemnumerario_somatotais
 LOCAL lcsomatt
 STORE 0 TO lcsomatt
 SELECT * FROM ucrsContMoedas INTO CURSOR ucrsContMoedasTT READWRITE
 SELECT * FROM ucrsContNotas INTO CURSOR ucrsContNotasTT READWRITE
 
 SELECT ucrscontmoedastt
 GOTO TOP
 SCAN
    lcsomatt = lcsomatt+(ucrscontmoedastt.contagem*ucrscontmoedastt.id)
 ENDSCAN
 SELECT ucrscontnotastt
 GOTO TOP
 SCAN
    lcsomatt = lcsomatt+(ucrscontnotastt.contagem*ucrscontnotastt.id)
 ENDSCAN
 contagemnumerario.txttotcont.value = lcsomatt
 fecha("ucrsContMoedasTT")
 fecha("ucrsContNotasTT")
ENDPROC
**
PROCEDURE uf_contagemnumerario_somatotais_top
 LOCAL lcsomatt
 STORE 0 TO lcsomatt
 SELECT ucrscontmoedas
 GOTO TOP
 SELECT ucrscontnotas
 GOTO TOP
 SELECT * FROM ucrsContMoedas INTO CURSOR ucrsContMoedasTT READWRITE
 SELECT * FROM ucrsContNotas INTO CURSOR ucrsContNotasTT READWRITE
 SELECT ucrscontmoedastt
 GOTO TOP
 SCAN
    lcsomatt = lcsomatt+(ucrscontmoedastt.contagem*ucrscontmoedastt.id)
 ENDSCAN
 SELECT ucrscontnotastt
 GOTO TOP
 SCAN
    lcsomatt = lcsomatt+(ucrscontnotastt.contagem*ucrscontnotastt.id)
 ENDSCAN
 contagemnumerario.txttotcont.value = lcsomatt
 fecha("ucrsContMoedasTT")
 fecha("ucrsContNotasTT")
 SELECT ucrscontmoedas
 GOTO TOP
 SELECT ucrscontnotas
 GOTO TOP
ENDPROC
**
