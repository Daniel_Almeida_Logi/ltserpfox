**
FUNCTION uf_dadosdem_chama
 LPARAMETERS lcbackoffice, lcconsultaanterior
 LOCAL lcreceitanr, lccodacesso, lccoddiropcao, lcnrsns, lcfistamp
 PUBLIC mydemsuspensa, mydemconsultada
 STORE .F. TO mydemsuspensa, lcdemconsultada, lcreservasuspensa
 mydemconsultada = uf_dadosdem_carregadados(lcconsultaanterior, lcbackoffice)
 
 regua(2)

 
 IF USED("ucrsPlanosDemAutomaticos")
    fecha("ucrsPlanosDemAutomaticos")
 ENDIF
 IF EMPTY(lcbackoffice)
    SELECT fi
    lclordem = fi.lordem
    lcfistamp = fi.fistamp
    LOCAL lclordemaux
    lclordemaux = LEFT(astr(lclordem), 2)
    SELECT * FROM fi WHERE LEFT(astr(fi.lordem), 2)=lclordemaux AND  (NOT EMPTY(fi.ofistamp) OR NOT EMPTY(fi.bistamp)) INTO CURSOR uCrsFiTempAux READWRITE
    SELECT uCrsFiTempAux 
    GO TOP 
    SCAN 
    	IF !EMPTY(uCrsFiTempAux.bistamp)
    		LOCAL lcBistampFi
    		lcBistampFi = ALLTRIM(uCrsFiTempAux.bistamp)
    		TEXT TO lcsql TEXTMERGE NOSHOW
				select nmdos from bi (nolock) where bistamp='<<ALLTRIM(lcBistampFi)>>'
		    ENDTEXT
		    IF  .NOT. uf_gerais_actgrelha("", "uCrsNmdos", lcsql)
		       uf_perguntalt_chama("Ocorreu uma anomalia ao validar o nome do documento origem. Por favor reinicie o Software.", "OK", "", 16)
		       RETURN .F.
		    ENDIF
		    SELECT uCrsNmdos
		    IF ALLTRIM(uCrsNmdos.nmdos)<>'Reserva de Cliente'
		    	SELECT uCrsFiTempAux
		    	DELETE 
		    ENDIF
		    fecha("uCrsNmdos") 
		    SELECT uCrsFiTempAux
    	ENDIF 
    ENDSCAN 
    
    IF RECCOUNT("uCrsFiTempAux")>0 .AND. usoureserv=.F. 
       IF  .NOT. uf_perguntalt_chama("Vai regularizar uma venda atrav�s da funcionalidade DEM."+CHR(13)+CHR(13)+"Pretende continuar ?", "Sim", "N�o")
          RETURN .F.
       ENDIF
       mydemsuspensa = .T.
    ENDIF
    SELECT * FROM fi WHERE LEFT(astr(fi.lordem), 2)=lclordemaux AND EMPTY(fi.ofistamp) AND EMPTY(fi.bistamp) AND fi.dem=.F. AND LEFT(ALLTRIM(fi.design), 1)<>"." AND EMPTY(fi.epromo) INTO CURSOR uCrsFiTempAux2 READWRITE
    IF RECCOUNT("uCrsFiTempAux2")>0
       uf_perguntalt_chama("N�o pode utilizar esta funcionalidade se a venda j� tiver produtos adicionados manualmente que n�o sejam provenientes de regulariza��es. Por favor verifique. Obrigado.", "OK", "", 16)
       RETURN .F.
    ENDIF
    IF (uf_atendimento_verificareservasuspensa())
       uf_perguntalt_chama("N�o pode utilizar esta funcionalidade, a receita j� foi inserida", "OK", "", 16)
       RETURN .F.
    ENDIF
    IF uf_gerais_getparameter("ADM0000000267", "BOOL")
       uf_atendimento_conferir_limpar()
    ELSE
       uf_atendimento_salvar_limpar()
    ENDIF
    

    
    IF VARTYPE(receitaencomenda)<>'U'
       IF receitaencomenda=.T.
          IF UPPER(ALLTRIM(ucrse1.pais))=='PORTUGAL'
             lcreceitanr = ALLTRIM(nrreceitaenc)
             lccodacesso = ALLTRIM(codreceitaenc)
             lccoddiropcao = ALLTRIM(cdoreceitaenc)
          ENDIF
       ELSE
          IF  .NOT. EMPTY(ALLTRIM(atendimento.pgfdados.page3.btnreceita.label1.caption))
             lcreceitanr = ALLTRIM(atendimento.pgfdados.page3.btnreceita.label1.caption)
          ENDIF
          IF  .NOT. EMPTY(ALLTRIM(atendimento.pgfdados.page3.txtcodigoacesso.value)) .AND.  .NOT. EMPTY(ALLTRIM(atendimento.pgfdados.page3.btnreceita.label1.caption))
             lccodacesso = atendimento.pgfdados.page3.txtcodigoacesso.value
          ENDIF
          IF  .NOT. EMPTY(ALLTRIM(atendimento.pgfdados.page3.txtcodigodireitoopcao.value)) .AND.  .NOT. EMPTY(ALLTRIM(atendimento.pgfdados.page3.btnreceita.label1.caption))
             lccoddiropcao = atendimento.pgfdados.page3.txtcodigodireitoopcao.value
          ENDIF
          lcnrsns = uf_dadosdem_getnrsns(lcnrsns)
       ENDIF
    ELSE
       IF  .NOT. EMPTY(ALLTRIM(atendimento.pgfdados.page3.btnreceita.label1.caption))
          lcreceitanr = ALLTRIM(atendimento.pgfdados.page3.btnreceita.label1.caption)
       ENDIF
       IF  .NOT. EMPTY(ALLTRIM(atendimento.pgfdados.page3.txtcodigoacesso.value)) .AND.  .NOT. EMPTY(ALLTRIM(atendimento.pgfdados.page3.btnreceita.label1.caption))
          lccodacesso = atendimento.pgfdados.page3.txtcodigoacesso.value
       ENDIF
       IF  .NOT. EMPTY(ALLTRIM(atendimento.pgfdados.page3.txtcodigodireitoopcao.value)) .AND.  .NOT. EMPTY(ALLTRIM(atendimento.pgfdados.page3.btnreceita.label1.caption))
          lccoddiropcao = atendimento.pgfdados.page3.txtcodigodireitoopcao.value
       ENDIF
       SELECT fi
       lcnrsns = ALLTRIM(fi.u_nbenef)
       lcnrsns = uf_dadosdem_getnrsns(lcnrsns)
    ENDIF
 ELSE

    lcreceitanr = ft2.u_receita
    lccodacesso = ft2.codacesso
    lccoddiropcao = ft2.coddiropcao
    lcnrsns = ft2.u_nbenef
 ENDIF
 IF TYPE("DADOSDEM")=="U"
	
    DO FORM DADOSDEM WITH lcreceitanr, lccodacesso, lccoddiropcao, lcnrsns, lcfistamp, lcbackoffice
 ELSE
 
    dadosdem.show()
    IF  .NOT. EMPTY(lcreceitanr)
       dadosdem.txtreceita.value = ALLTRIM(lcreceitanr)
    ENDIF
    IF  .NOT. EMPTY(lccodacesso)
       dadosdem.txtcodacesso.value = ALLTRIM(lccodacesso)
    ENDIF
    IF  .NOT. EMPTY(lccoddiropcao)
       dadosdem.txtcoddiropcao.value = ALLTRIM(lccoddiropcao)
    ENDIF
    IF  .NOT. EMPTY(lcnrsns)
       dadosdem.txtnrsns.value = ALLTRIM(lcnrsns)
    ENDIF
    dadosdem.fistamp = lcfistamp
    

 ENDIF
 
 
ENDFUNC
**
FUNCTION uf_dadosdem_colocaCursorGrelha
 &&coloca o cursor na grelha se estiver preenchida, chamado no show do form
 IF TYPE("DADOSDEM")!="U"
     IF TYPE("dadosdem.grdpesq")=="O"     
        IF(USED("uCrsVerificaRespostaDEM"))      
        	if(RECCOUNT("uCrsVerificaRespostaDEM")>0) 
        		IF TYPE("dadosdem.grdpesq.sel")=="O"      	
        			dadosdem.grdpesq.sel.setfocus()
        		ENDIF        			  		
        	ENDIF       	
        ENDIF
       	 
	 ENDIF	 	
  ENDIF
ENDFUNC	 



FUNCTION uf_dadosdem_getnrsns
 LPARAMETERS lcnrsnstemp
 IF (EMPTY(lcnrsnstemp))
    IF (USED("ft2"))
       SELECT ft2
       lcnrsnstemp = ALLTRIM(ft2.u_nbenef)
    ENDIF
 ENDIF
 IF (EMPTY(lcnrsnstemp))
    IF (USED("uCrsAtendCL"))
       SELECT ucrsatendcl
       GOTO TOP
       lcnrsnstemp = ALLTRIM(ucrsatendcl.nbenef)
    ENDIF
 ENDIF
 RETURN lcnrsnstemp
ENDFUNC
**
PROCEDURE uf_dadosdem_actmenu
 LPARAMETERS lcdemconsultada
 IF (mydemconsultada==.T. .AND. TYPE("DADOSDEM")<>"U")
    dadosdem.menu1.estado("limparDados", "HIDE")
    dadosdem.menu1.estado("", "", "Dispensar", .T., "Sair", .T.)
 ENDIF
ENDPROC
**
FUNCTION uf_dadosdem_validaseconsultada
 LOCAL lcpos, lcnumreceita, lcvalida
 STORE "null" TO lcnumreceita
 STORE .F. TO lcvalida
 lcpos = RECNO("fi")
 SELECT fi
 IF (fi.dem==.T.)
    lcnumreceita = ALLTRIM(fi.receita)
 ENDIF
 IF (fi.epromo)
    uf_atendimento_devolvecursorcabecalhofi(fi.lordem)
    lcnumreceita = ALLTRIM(STREXTRACT(ucrstempficabecalho.design, ':', ' -', 1, 0))
    IF (USED("ucrsTempFiCabecalho"))
       fecha("ucrsTempFiCabecalho")
    ENDIF
 ENDIF
 IF (USED("uCrsVerificaRespostaDEMTotal"))
    IF (USED("uCrsVerificaRespostaDEM"))
       fecha("uCrsVerificaRespostaDEM")
    ENDIF
    
    SELECT uCrsVerificaRespostaDEMTotal 
    GO TOP
    
    SELECT * FROM uCrsVerificaRespostaDEMTotal WHERE receita_nr=lcnumreceita AND sel=.F. INTO CURSOR uCrsVerificaRespostaDEM READWRITE
    
       
    SELECT uCrsVerificaRespostaDEMTotal 
    GO TOP
    
    SELECT * FROM uCrsVerificaRespostaDEMTotal WHERE receita_nr=lcnumreceita AND sel=.T. INTO CURSOR uCrsVerificaRespostaDEMAuxiliar READWRITE
    IF (RECCOUNT("uCrsVerificaRespostaDEM")>0 .OR. (RECCOUNT("uCrsVerificaRespostaDEMAuxiliar")>0 .AND. RECCOUNT("uCrsVerificaRespostaDEM")==0))
       IF (USED("uCrsVerificaRespostaDEMAuxliar"))
          fecha("uCrsVerificaRespostaDEMAuxliar")
       ENDIF
       lcvalida = .T.
    ENDIF
 ENDIF
 SELECT fi
 TRY
    GOTO lcpos
 CATCH
 ENDTRY
 RETURN lcvalida
ENDFUNC
**
FUNCTION uf_dadosdem_validareceitarepetida
 LPARAMETERS lcnumreceita, lcnrsns, lcnracesso
 LOCAL lcpos, lcvalida
 IF ( .NOT. EMPTY(lcnumreceita))
    SELECT ucrsverificarespostademtotal
    GOTO TOP
    LOCATE FOR ALLTRIM(ucrsverificarespostademtotal.receita_nr)==ALLTRIM(lcnumreceita)
    IF FOUND()
       lcvalida = .T.
    ENDIF
 ELSE
    IF (USED("ucrsAtendComp"))
       lcvalida = .F.
       lcpos = RECNO("ucrsAtendComp")
       SELECT ucrsatendcomp
       GOTO TOP
       LOCATE FOR ALLTRIM(ucrsatendcomp.codacesso)==ALLTRIM(lcnracesso)
       IF FOUND()
          lcvalida = .T.
       ENDIF
       SELECT ucrsatendcomp
       TRY
          GOTO lcpos
       CATCH
       ENDTRY
    ENDIF
 ENDIF
 RETURN lcvalida
ENDFUNC
**
FUNCTION uf_dadosdem_gravar
 IF RIGHT(ALLTRIM(dadosdem.menu1.gravar.img1.picture), 28)='\imagens\icons\checked_w.png'
    uf_dadosdem_seltodos()
 ENDIF
 LOCAL lcbackoffice
 lcbackoffice = dadosdem.backoffice
 IF  .NOT. USED("uCrsVerificaRespostaDEM")
    RETURN .F.
 ELSE
 	
 
    SELECT medicamento_descr, validade_linha, COUNT(medicamento_descr) AS tot, receita_nr, utente_nome, medicamento_cod, medicamento_cnpem, posologia FROM uCrsVerificaRespostaDEM WITH (BUFFERING=.T.) WHERE sel=.F. AND ALLTRIM(receita_tipo)=="RSP" GROUP BY medicamento_descr, validade_linha, receita_nr, utente_nome, medicamento_cod, medicamento_cnpem, posologia INTO CURSOR uCrsVerificaRespostaDEM_nd READWRITE
    IF RECCOUNT("uCrsDEMndisp")=0
       SELECT ucrsverificarespostadem_nd
       GOTO TOP
       SCAN
          SELECT ucrsdemndisp
          GOTO BOTTOM
          APPEND BLANK
          **REPLACE ucrsdemndisp.receita WITH ALLTRIM(dadosdem.txtreceita.value)
          REPLACE ucrsdemndisp.receita WITH ALLTRIM(ucrsverificarespostadem_nd.receita_nr)
          REPLACE ucrsdemndisp.codacesso WITH ALLTRIM(dadosdem.txtcodacesso.value)
          REPLACE ucrsdemndisp.coddiropcao WITH ALLTRIM(dadosdem.txtcoddiropcao.value)
          REPLACE ucrsdemndisp.nrsns WITH ALLTRIM(dadosdem.txtnrsns.value)
          REPLACE ucrsdemndisp.medicamento_descr WITH ALLTRIM(ucrsverificarespostadem_nd.medicamento_descr)
          REPLACE ucrsdemndisp.validade_linha WITH ucrsverificarespostadem_nd.validade_linha
          REPLACE ucrsdemndisp.tot WITH ucrsverificarespostadem_nd.tot
          REPLACE ucrsdemndisp.nomeUtente WITH ucrsverificarespostadem_nd.utente_nome
		  REPLACE ucrsdemndisp.medicamento_cod WITH ucrsverificarespostadem_nd.medicamento_cod
		  REPLACE ucrsdemndisp.medicamento_cnpem WITH ucrsverificarespostadem_nd.medicamento_cnpem
		  REPLACE ucrsdemndisp.posologia WITH ucrsverificarespostadem_nd.posologia
		  
          SELECT ucrsverificarespostadem_nd
       ENDSCAN
    ELSE
       SELECT ucrsdemndisp
       GOTO TOP
       SCAN
          IF ALLTRIM(ucrsdemndisp.receita)==ALLTRIM(dadosdem.txtreceita.value)
             DELETE
          ENDIF
       ENDSCAN
       SELECT ucrsverificarespostadem_nd
       GOTO TOP
       SCAN
          SELECT ucrsdemndisp
          GOTO BOTTOM
          APPEND BLANK
          **REPLACE ucrsdemndisp.receita WITH ALLTRIM(dadosdem.txtreceita.value)
          REPLACE ucrsdemndisp.receita WITH ALLTRIM(ucrsverificarespostadem_nd.receita_nr)
          REPLACE ucrsdemndisp.codacesso WITH ALLTRIM(dadosdem.txtcodacesso.value)
          REPLACE ucrsdemndisp.coddiropcao WITH ALLTRIM(dadosdem.txtcoddiropcao.value)
          REPLACE ucrsdemndisp.nrsns WITH ALLTRIM(dadosdem.txtnrsns.value)
          REPLACE ucrsdemndisp.medicamento_descr WITH ALLTRIM(ucrsverificarespostadem_nd.medicamento_descr)
          REPLACE ucrsdemndisp.validade_linha WITH ucrsverificarespostadem_nd.validade_linha
          REPLACE ucrsdemndisp.tot WITH ucrsverificarespostadem_nd.tot
          REPLACE ucrsdemndisp.nomeUtente WITH ucrsverificarespostadem_nd.utente_nome
		  REPLACE ucrsdemndisp.medicamento_cod WITH ucrsverificarespostadem_nd.medicamento_cod
		  REPLACE ucrsdemndisp.medicamento_cnpem WITH ucrsverificarespostadem_nd.medicamento_cnpem
		  REPLACE ucrsdemndisp.posologia WITH ucrsverificarespostadem_nd.posologia
		  
          SELECT ucrsverificarespostadem_nd
       ENDSCAN
    ENDIF
    IF USED("uCrsVerificaRespostaDEM_nd")
       fecha("uCrsVerificaRespostaDEM_nd")
    ENDIF
    IF  .NOT. uf_dadosdemvalidaselecionados()
       RETURN .F.
    ENDIF
    uf_dadosdem_sair()
    
    
    SELECT ucrsverificarespostadem
    GOTO TOP
    SELECT * FROM uCrsVerificaRespostaDEM WHERE  NOT EMPTY(ucrsverificarespostadem.sel) INTO CURSOR uCrsVerificaRespostaDEMAux READWRITE
    uf_dadosdem_guardacopia()
    IF USED("uCrsVerificaRespostaDEM")
       fecha("uCrsVerificaRespostaDEM")
    ENDIF
    SELECT * FROM uCrsVerificaRespostaDEMAux INTO CURSOR uCrsVerificaRespostaDEM READWRITE
    
       
  
    uf_atendimento_tmrconsultadem(lcbackoffice, .F.)
    uf_atendimento_infototais()
   
 ENDIF
ENDFUNC
**
FUNCTION uf_dadosdem_consultar
 LOCAL lcbackoffice, lcreceitanr, lccodacesso, lccoddiropcao, lcnrsns
 STORE '' TO lcreceitanr
 STORE 0 TO lccodacesso, lccoddiropcao, lcnrsns
 lcbackoffice = dadosdem.backoffice
 

 IF EMPTY(lcbackoffice)
    SELECT COUNT(fistamp) AS rownumber FROM fi WHERE  NOT EMPTY(fi.fistamp) INTO CURSOR uCrsFiAux READWRITE
    IF ALLTRIM(fi.design)<>'.SEM RECEITA' .AND. ucrsfiaux.rownumber=1
       SELECT fi
       GOTO TOP
       uf_atendimento_removecompart(.T.)
    ENDIF
    IF USED("uCrsFiAux")
       fecha("uCrsFiAux")
    ENDIF
 ENDIF
 IF (LEN(ALLTRIM(dadosdem.txtreceita.value))<>19 .OR. (LEN(ALLTRIM(dadosdem.txtreceita.value))=0 .AND. LEN(ALLTRIM(dadosdem.txtnrsns.value))<>6)) .AND. EMPTY(ALLTRIM(dadosdem.txtnrsns.value))
    RETURN .F.
 ENDIF
 IF LEN(ALLTRIM(dadosdem.txtcoddiropcao.value))<>0 .AND. ((ALLTRIM(DADOSDEM.label3.caption) = "C. Direito Op��o" AND LEN(ALLTRIM(dadosdem.txtcoddiropcao.value))<>4) OR (ALLTRIM(DADOSDEM.label3.caption) = "Presta��o" AND LEN(ALLTRIM(dadosdem.txtcoddiropcao.value))<>6) )
    RETURN .F.
 ENDIF
 IF LEN(ALLTRIM(dadosdem.txtnrsns.value))<>0 .AND. LEN(ALLTRIM(dadosdem.txtnrsns.value))<>9
    RETURN .F.
 ENDIF
 IF LEN(ALLTRIM(dadosdem.txtcodacesso.value))<>6
    dadosdem.txtcodacesso.setfocus()
    RETURN .F.
 ENDIF
 IF LEN(ALLTRIM(dadosdem.txtcodacesso.value))=6 .AND. EMPTY(ALLTRIM(dadosdem.txtnrsns.value)) .AND. EMPTY(ALLTRIM(dadosdem.txtreceita.value))
    RETURN .F.
 ENDIF
 

 
 lcreceitanr = ALLTRIM(dadosdem.txtreceita.value)
 lccodacesso = ALLTRIM(dadosdem.txtcodacesso.value)
 lccoddiropcao = ALLTRIM(dadosdem.txtcoddiropcao.value)
 lcnrsns = ALLTRIM(dadosdem.txtnrsns.value)
 
 

  
 LOCAL lcTipo
 lcTipo= uf_gerais_devolveDescrDispensaEletronica(uf_gerais_retornaTipoDispensaEletronica(lcreceitanr))
 
 

 
 if(UPPER(ALLTRIM(lcTipo)) = "MCDT") AND !uf_gerais_getparameter("ADM0000000355", "bool")
   	uf_perguntalt_chama("N�o � permitida a consulta eletr�nica de mcdts", "OK", "", 16)	
   	RETURN .F.
 ENDIF
 


 
 IF  .NOT. EMPTY(lcbackoffice)

    SELECT fi
    DELETE ALL
    GOTO TOP
    SCAN
       DELETE
    ENDSCAN
    SELECT fi
    GOTO TOP

    uf_atendimento_adicionalinhafi(.T., .T.)
    SELECT ft2
    REPLACE ft2.u_receita WITH lcreceitanr
    REPLACE ft2.codacesso WITH lccodacesso
    REPLACE ft2.coddiropcao WITH lccoddiropcao
  
    IF  .NOT. EMPTY(lcnrsns)
       REPLACE ft2.u_nbenef WITH lcnrsns
    ENDIF
 
 ELSE

    IF (uf_dadosdem_validareceitarepetida(lcreceitanr, lcnrsns, lccodacesso))
       uf_perguntalt_chama("Esta receita j� foi consultada. Por favor valide no atendimento", "OK", "", 16)
       RETURN .F.
    ENDIF

    uf_atendimento_aplicacompart("01", .F., .F., .F., .T.)
 
    SELECT ucrsatendcomp
    GOTO TOP
    LOCATE FOR ALLTRIM(ucrsatendcomp.fistamp)==ALLTRIM(dadosdem.fistamp)
    IF FOUND()
       REPLACE ucrsatendcomp.codacesso WITH lccodacesso
       REPLACE ucrsatendcomp.coddiropcao WITH lccoddiropcao
    ENDIF

    IF  .NOT. EMPTY(ALLTRIM(dadosdem.txtnrsns.value))
       SELECT fi
       GOTO TOP
       LOCATE FOR ALLTRIM(fi.fistamp)==ALLTRIM(dadosdem.fistamp)
       IF FOUND()
          REPLACE fi.u_nbenef WITH lcnrsns
       ENDIF
    ENDIF

    SELECT ft2
    REPLACE ft2.u_receita WITH lcreceitanr
    REPLACE ft2.u_nbenef WITH lcnrsns

    atendimento.pgfdados.refresh

 ENDIF
 

 uf_atendimento_dem(.F., lcreceitanr, lcnrsns, lccodacesso, lccoddiropcao, lcbackoffice, .T.)

 
 dadosdem.grdpesq.sel.setfocus()
ENDFUNC
**
FUNCTION uf_dadosdem_preenchedadosadicionais
 IF (USED("uCrsVerificaRespostaDEM") .AND. VARTYPE(dadosdem)<>"U")
    LOCAL lcposdemresp
    lcposdemresp = RECNO("uCrsVerificaRespostaDEM")
    SELECT ucrsverificarespostadem
    dadosdem.txtcontactoutente.value = ucrsverificarespostadem.utente_contacto
    dadosdem.txtnomeutente.value = ALLTRIM(ucrsverificarespostadem.utente_nome)
    dadosdem.txtnomemedico.value = ALLTRIM(ucrsverificarespostadem.prescritor_nome)
    dadosdem.txtcontactomedico.value = ucrsverificarespostadem.prescritor_contacto
    dadosdem.txtespecialidademedico.value = ucrsverificarespostadem.especialidade
    dadosdem.txtlocal.value = ALLTRIM(ucrsverificarespostadem.local_presc)
    dadosdem.txtnrcartaosns.value = ALLTRIM(ucrsverificarespostadem.nbenef)
    dadosdem.txtnrcartao.value = ALLTRIM(ucrsverificarespostadem.nrcartao)
    dadosdem.txtEfr.value = ALLTRIM(ucrsverificarespostadem.efr_descr)
   
   

    IF  .NOT. USED("UCRSATENDCOMP")
       IF  .NOT. uf_gerais_actgrelha("", "UCRSATENDCOMP", [set fmtonly on exec up_receituario_dadosDoPlano '' set fmtonly off])
          uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [UCRSATENDCOMP]. Por favor reinicie o Software.", "OK", "", 16)
          RETURN .F.
       ENDIF
    ENDIF
    SELECT ucrsatendcomp
    REPLACE ucrsatendcomp.nomeutente WITH ALLTRIM(dadosdem.txtnomeutente.value)
    REPLACE ucrsatendcomp.contactoutente WITH ALLTRIM(dadosdem.txtcontactoutente.value)
    REPLACE ucrsatendcomp.nomemedico WITH ALLTRIM(dadosdem.txtnomemedico.value)
    REPLACE ucrsatendcomp.contactomedico WITH ALLTRIM(dadosdem.txtcontactomedico.value)
    REPLACE ucrsatendcomp.especialidademedico WITH ALLTRIM(dadosdem.txtespecialidademedico.value)
    REPLACE ucrsatendcomp.localprescricao WITH ALLTRIM(ucrsverificarespostadem.local_presc)
    REPLACE ucrsatendcomp.nrcartaosns WITH ALLTRIM(ucrsverificarespostadem.nbenef)
    REPLACE ucrsatendcomp.nrcartao WITH ALLTRIM(ucrsverificarespostadem.nrcartao)
    REPLACE ucrsatendcomp.efr_descr WITH ALLTRIM(ucrsverificarespostadem.efr_descr)
    
    
    
    SELECT ucrsverificarespostadem
    TRY
       GOTO lcposdemresp
    CATCH
    ENDTRY
 ENDIF
ENDFUNC
**
FUNCTION uf_dadosdem_scanner
 IF EMPTY(ALLTRIM(dadosdem.txtscanner.value))
    RETURN .F.
 ENDIF
 LOCAL lcref
 STORE "" TO lcref
 lcref = UPPER(ALLTRIM(dadosdem.txtscanner.value))
 DO CASE
    CASE LEN(lcref)>=13
       dadosdem.txtreceita.value = ALLTRIM(dadosdem.txtscanner.value)
       dadosdem.txtscanner.value = ""
       dadosdem.txtcodacesso.setfocus()
    CASE LEN(lcref)==6
       dadosdem.txtcodacesso.value = ALLTRIM(dadosdem.txtscanner.value)
       dadosdem.txtscanner.value = ""
       dadosdem.txtcoddiropcao.setfocus()
    CASE LEN(lcref)==4
       dadosdem.txtcoddiropcao.value = ALLTRIM(dadosdem.txtscanner.value)
       dadosdem.txtscanner.value = ""
    CASE LEN(lcref)==9
       dadosdem.txtnrsns.value = ALLTRIM(dadosdem.txtscanner.value)
       dadosdem.txtscanner.value = ""
       dadosdem.txtreceita.setfocus()
       uf_dadosdem_aplicautente()
 ENDCASE
ENDFUNC
**
FUNCTION uf_dadosdem_aplicautente
 LOCAL lcbackoffice
 lcbackoffice = dadosdem.backoffice
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		Select no, estab from b_utentes (nolock) where nbenef = '<<ALLTRIM(DADOSDEM.txtNrSNS.Value)>>' and inactivo = 0
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "ucrsTempCl", lcsql)
    RETURN .F.
 ENDIF
 SELECT ucrstempcl
 IF  .NOT. EMPTY(ucrstempcl.no)
    IF EMPTY(lcbackoffice)
       SELECT fi
       LOCATE FOR ALLTRIM(fi.fistamp)=ALLTRIM(dadosdem.fistamp)
       IF FOUND()
       ENDIF
       uf_atendimento_selcliente(ucrstempcl.no, ucrstempcl.estab, "ATENDIMENTO")
       SELECT fi
       LOCATE FOR ALLTRIM(fi.fistamp)=ALLTRIM(dadosdem.fistamp)
       IF FOUND()
       ENDIF
    ELSE
       uf_atendimento_selcliente(ucrstempcl.no, ucrstempcl.estab, "FACTURACAO")
    ENDIF
 ENDIF
ENDFUNC
**
FUNCTION uf_dadosdem_validaestadoconsulta
 LOCAL lctimer, lcbackoffice
 STORE .F. TO lctimer
 
 IF(VARTYPE(dadosdem)=="U")
 	RETURN .f.
 ENDIF
 
 lcbackoffice = dadosdem.backoffice
 IF EMPTY(lcbackoffice)
 	IF(VARTYPE(ATENDIMENTO)=="U")
 		RETURN .f.
 	ENDIF
    lctimer = atendimento.tmrconsultadem.enabled
 ELSE
 	IF(VARTYPE(facturacao)=="U")
 		RETURN .f.
 	ENDIF
    lctimer = facturacao.tmrconsultadem.enabled
 ENDIF
 RETURN lctimer
ENDFUNC
**
FUNCTION uf_dadosdem_sair
 IF (uf_dadosdem_validaestadoconsulta())
    RETURN .F.
 ENDIF
 IF(VARTYPE(dadosdem)<>"U")
	 dadosdem.hide
	 dadosdem.release
 ENDIF
ENDFUNC
**
FUNCTION uf_dadosdem_seltodos
 LOCAL lcbackoffice, lcvalidadetodos
 lcvalidadetodos = 0
 lcbackoffice = dadosdem.backoffice
 IF  .NOT. USED("uCrsVerificaRespostaDEM")
    RETURN .F.
 ELSE

   SELECT ucrsverificarespostadem
   GOTO TOP

   CALCULATE COUNT() TO uv_recCount
   CALCULATE COUNT() FOR isEfetivado TO uv_countEfetivado
   CALCULATE COUNT() FOR !EMPTY(anulacaoMotivo) TO uv_countAnulado
   CALCULATE COUNT() FOR uf_gerais_getdate(ucrsverificarespostadem.validade_linha, "SQL")<uf_gerais_getdate(DATE(), "SQL") TO uv_countVal

   DO CASE
      CASE uv_recCount = uv_countEfetivado
         uf_perguntalt_chama("Todas as linhas encontram-se efetivadas! N�o � poss�vel avan�ar.", "OK", "", 16)
         RETURN .F.
      CASE uv_recCount = uv_countAnulado
         uf_perguntalt_chama("Todas as linhas encontram-se anuladas! N�o � poss�vel avan�ar.", "OK", "", 16)
         RETURN .F.
   ENDCASE

   UPDATE uCrsVerificaRespostaDEM SET sel = .T. WHERE uf_gerais_getdate(ucrsverificarespostadem.validade_linha, "SQL")>=uf_gerais_getdate(DATE(), "SQL") AND !ucrsverificarespostadem.isEfetivado AND EMPTY(ucrsverificarespostadem.anulacaoMotivo)
   dadosdem.menu1.estado("unselTodos", "SHOW")
   dadosdem.menu1.estado("selTodos", "HIDE")
   uf_dadosdem_tiposaida() 
 ENDIF
ENDFUNC
**
FUNCTION uf_dadosdem_unseltodos
 LOCAL lcbackoffice, lcvalidadetodos
 lcvalidadetodos = 0
 lcbackoffice = dadosdem.backoffice
 IF  .NOT. USED("uCrsVerificaRespostaDEM")
    RETURN .F.
 ELSE
    UPDATE uCrsVerificaRespostaDEM SET sel = .F.
    dadosdem.menu1.estado("unselTodos", "HIDE")
    dadosdem.menu1.estado("selTodos", "SHOW")
    uf_dadosdem_tiposaida()
 ENDIF
ENDFUNC
**
FUNCTION uf_dadosdem_dispensartodos
 LOCAL lcbackoffice, lcvalidadetodos
 lcvalidadetodos = 0
 lcbackoffice = dadosdem.backoffice
 IF  .NOT. USED("uCrsVerificaRespostaDEM")
    RETURN .F.
 ELSE
    SELECT ucrsverificarespostadem
    GOTO TOP
    SCAN
       IF uf_gerais_getdate(ucrsverificarespostadem.validade_linha, "SQL")>=uf_gerais_getdate(DATE(), "SQL") .AND.  .NOT. uf_dadosdem_validaexecpcaoseringas()
          lcvalidadetodos = 1
       ENDIF
    ENDSCAN

    UPDATE uCrsVerificaRespostaDEM SET sel = .T.
    uf_dadosdem_guardacopia()
    DELETE FROM uCrsVerificaRespostaDEM WHERE uf_gerais_getdate(ucrsverificarespostadem.validade_linha, "SQL")<uf_gerais_getdate(DATE()+1, "SQL") .AND. ucrsverificarespostadem.medicamento_cod NOT IN ('2454684', '8650309', '8567404', '5726229') .AND. ucrsverificarespostadem.medicamento_cnpem<>'50139215' .AND. ucrsverificarespostadem.medicamento_cnpem<>'50180444'
    uf_dadosdem_sair()
    uf_atendimento_tmrconsultadem(lcbackoffice, .F.)
 ENDIF
ENDFUNC
**
PROCEDURE uf_dadosdem_guardacopia
 IF (USED("uCrsVerificaRespostaDEMTotal"))
    SELECT ucrsverificarespostadem
    SCAN
       SELECT ucrsverificarespostademtotal
       LOCATE FOR ucrsverificarespostadem.id=ucrsverificarespostademtotal.id
       IF FOUND()
          UPDATE uCrsVerificaRespostaDEMTotal SET ucrsverificarespostademtotal.sel = .F. WHERE ucrsverificarespostadem.id=ucrsverificarespostademtotal.id
       ELSE
          INSERT INTO uCrsVerificaRespostaDEMTotal SELECT * FROM uCrsVerificaRespostaDEM
          UPDATE uCrsVerificaRespostaDEMTotal SET ucrsverificarespostademtotal.sel = .F. WHERE ucrsverificarespostadem.id=ucrsverificarespostademtotal.id
       ENDIF
    ENDSCAN
 ELSE
    SELECT * FROM uCrsVerificaRespostaDEM INTO CURSOR uCrsVerificaRespostaDEMTotal READWRITE
    UPDATE uCrsVerificaRespostaDEMTotal SET ucrsverificarespostademtotal.sel = .F.
 ENDIF
ENDPROC
**
FUNCTION uf_dadosdemvalidaselecionados
 SELECT ucrsverificarespostadem
 GOTO TOP
 LOCATE FOR ucrsverificarespostadem.sel=.T.
 IF  .NOT. FOUND()
    uf_perguntalt_chama("Por favor seleccione um medicamento.", "OK", "", 16)
    RETURN .F.
 ENDIF
 RETURN .T.
ENDFUNC
**
FUNCTION uf_dadosdem_dispensarseleccionados
 LOCAL lcbackoffice
 lcbackoffice = dadosdem.backoffice
 IF  .NOT. USED("uCrsVerificaRespostaDEM")
    RETURN .F.
 ELSE
    SELECT medicamento_descr, validade_linha, COUNT(medicamento_descr) AS tot, receita_nr, utente_nome, medicamento_cod, medicamento_cnpem, posologia FROM uCrsVerificaRespostaDEM WITH (BUFFERING=.T.) WHERE sel=.F. AND ALLTRIM(receita_tipo)=="RSP" GROUP BY medicamento_descr, validade_linha, receita_nr, utente_nome, medicamento_cod, medicamento_cnpem, posologia INTO CURSOR uCrsVerificaRespostaDEM_nd READWRITE
    IF RECCOUNT("uCrsDEMndisp")=0
       SELECT ucrsverificarespostadem_nd
       GOTO TOP
       SCAN
          SELECT ucrsdemndisp
          GOTO BOTTOM
          APPEND BLANK
          **REPLACE ucrsdemndisp.receita WITH ALLTRIM(dadosdem.txtreceita.value)
          REPLACE ucrsdemndisp.receita WITH ALLTRIM(uCrsVerificaRespostaDEM_nd.receita_nr)
          REPLACE ucrsdemndisp.codacesso WITH ALLTRIM(dadosdem.txtcodacesso.value)
          REPLACE ucrsdemndisp.coddiropcao WITH ALLTRIM(dadosdem.txtcoddiropcao.value)
          REPLACE ucrsdemndisp.nrsns WITH ALLTRIM(dadosdem.txtnrsns.value)
          REPLACE ucrsdemndisp.medicamento_descr WITH ALLTRIM(ucrsverificarespostadem_nd.medicamento_descr)
          REPLACE ucrsdemndisp.validade_linha WITH ucrsverificarespostadem_nd.validade_linha
          REPLACE ucrsdemndisp.tot WITH ucrsverificarespostadem_nd.tot
          REPLACE ucrsdemndisp.nomeUtente WITH ucrsverificarespostadem_nd.utente_nome
		  REPLACE ucrsdemndisp.medicamento_cod WITH ucrsverificarespostadem_nd.medicamento_cod
		  REPLACE ucrsdemndisp.medicamento_cnpem WITH ucrsverificarespostadem_nd.medicamento_cnpem
		  REPLACE ucrsdemndisp.posologia WITH ucrsverificarespostadem_nd.posologia
          SELECT ucrsverificarespostadem_nd
       ENDSCAN
    ELSE
       SELECT ucrsdemndisp
       GOTO TOP
       SCAN
          IF ALLTRIM(ucrsdemndisp.receita)==ALLTRIM(dadosdem.txtreceita.value)
             DELETE
          ENDIF
       ENDSCAN
       SELECT ucrsverificarespostadem_nd
       GOTO TOP
       SCAN
          SELECT ucrsdemndisp
          GOTO BOTTOM
          APPEND BLANK
          **REPLACE ucrsdemndisp.receita WITH ALLTRIM(dadosdem.txtreceita.value)
          REPLACE ucrsdemndisp.receita WITH ALLTRIM(uCrsVerificaRespostaDEM_nd.receita_nr)
          REPLACE ucrsdemndisp.codacesso WITH ALLTRIM(dadosdem.txtcodacesso.value)
          REPLACE ucrsdemndisp.coddiropcao WITH ALLTRIM(dadosdem.txtcoddiropcao.value)
          REPLACE ucrsdemndisp.nrsns WITH ALLTRIM(dadosdem.txtnrsns.value)
          REPLACE ucrsdemndisp.medicamento_descr WITH ALLTRIM(ucrsverificarespostadem_nd.medicamento_descr)
          REPLACE ucrsdemndisp.validade_linha WITH ucrsverificarespostadem_nd.validade_linha
          REPLACE ucrsdemndisp.tot WITH ucrsverificarespostadem_nd.tot
          REPLACE ucrsdemndisp.nomeUtente WITH ucrsverificarespostadem_nd.utente_nome
		  REPLACE ucrsdemndisp.medicamento_cod WITH ucrsverificarespostadem_nd.medicamento_cod
		  REPLACE ucrsdemndisp.medicamento_cnpem WITH ucrsverificarespostadem_nd.medicamento_cnpem
          SELECT ucrsverificarespostadem_nd
       ENDSCAN
    ENDIF
    IF USED("uCrsVerificaRespostaDEM_nd")
       fecha("uCrsVerificaRespostaDEM_nd")
    ENDIF
    IF  .NOT. uf_dadosdemvalidaselecionados()
       RETURN .F.
    ENDIF
    uf_dadosdem_sair()
    SELECT ucrsverificarespostadem
    GOTO TOP
    SELECT * FROM uCrsVerificaRespostaDEM WHERE  NOT EMPTY(ucrsverificarespostadem.sel) INTO CURSOR uCrsVerificaRespostaDEMAux READWRITE
    uf_dadosdem_guardacopia()
    IF USED("uCrsVerificaRespostaDEM")
       fecha("uCrsVerificaRespostaDEM")
    ENDIF
    SELECT * FROM uCrsVerificaRespostaDEMAux INTO CURSOR uCrsVerificaRespostaDEM READWRITE
    uf_atendimento_tmrconsultadem(lcbackoffice, .F.)
    uf_atendimento_infototais()
 ENDIF
ENDFUNC
**
FUNCTION uf_dadosdem_carregadados
 LPARAMETERS lcconsultaanterior, lcbackoffice, lcdemanterior
 STORE .F. TO lcdemanterior
 IF EMPTY(lcconsultaanterior)
    IF EMPTY(lcbackoffice)
       lcdemanterior = uf_dadosdem_validaseconsultada()
    ENDIF
    IF (lcdemanterior==.F.)
       lcsql = ''
       TEXT TO lcsql TEXTMERGE NOSHOW
	            exec up_dem_dadosReceita ''
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "uCrsVerificaRespostaDEM", lcsql)
          uf_perguntalt_chama("OCORREU UMA ANOMALIA AO PREPARAR A CONSULTA. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
       ENDIF
       IF ( .NOT. USED("uCrsVerificaRespostaDEMTotal"))
          SELECT * FROM uCrsVerificaRespostaDEM INTO CURSOR uCrsVerificaRespostaDEMTotal READWRITE
       ENDIF
    ENDIF
 ENDIF
 RETURN lcdemanterior
ENDFUNC
**
FUNCTION uf_dadosdem_validaexecpcaoseringas
 SELECT ucrsverificarespostadem
 IF ALLTRIM(ucrsverificarespostadem.medicamento_cod)=='5686860' .OR. ALLTRIM(ucrsverificarespostadem.medicamento_cnpem)=='50164015' .OR. ALLTRIM(ucrsverificarespostadem.medicamento_cod)=='5726229' .OR. ALLTRIM(ucrsverificarespostadem.medicamento_cnpem)=='50180444'
    RETURN .T.
 ENDIF
 RETURN .F.
ENDFUNC
**
PROCEDURE uf_dadosdem_sel
 IF (USED("uCrsVerificaRespostaDEM"))

	SELECT uCrsVerificaRespostaDEM

	IF ucrsverificarespostadem.sel


      IF ucrsverificarespostadem.isEfetivado
			uf_perguntalt_chama("Esta linha da receita encontra-se efetivada.", "OK", "", 16)
			REPLACE ucrsverificarespostadem.sel WITH .F.
			RETURN .F.
      ENDIF

      IF !EMPTY(ucrsverificarespostadem.anulacaoMotivo)
			uf_perguntalt_chama("MCDT anulado a " + ALLTRIM(ucrsverificarespostadem.anulacaoData) + "." + CHR(13) + "Motivo:" + CHR(13) + ALLTRIM(ucrsverificarespostadem.anulacaoMotivo), "OK", "", 16)
			REPLACE ucrsverificarespostadem.sel WITH .F.
			RETURN .F.
      ENDIF

	ENDIF

   uf_dadosdem_tiposaida()
 ENDIF
ENDPROC
**
PROCEDURE uf_dadosdem_tiposaida
 LOCAL lccont
 lccont = 0 
 
 SELECT ucrsverificarespostadem
 GOTO TOP
 SCAN 
    IF ucrsverificarespostadem.sel=.T.
       lccont = lccont+1
    ENDIF
 ENDSCAN

 IF(VARTYPE(dadosdem)<>"U")
	 IF lccont=0
		dadosdem.menu1.estado("", .F., "aaa", .F.)
		dadosdem.menu1.estado("", "", "", .T., "Sair", .T.)
	 ELSE	 
		dadosdem.menu1.gravar.img1.picture = mypath+"\imagens\icons\guardar_w.png"
		dadosdem.menu1.estado("", "", "Dispensar", .T., "Sair", .T.)
	 ENDIF
 ENDIF

 SELECT ucrsverificarespostadem
 LOCAL lcposlnrec
 lcposlnrec = RECNO("uCrsVerificaRespostaDEM")

 SELECT ucrsverificarespostadem
 TRY
    GOTO lcposlnrec
 CATCH
 ENDTRY
 
ENDPROC
**
PROCEDURE uf_dadosdem_botao
 LOCAL lccont, lcPos
 lccont = 0 

 	SELECT ucrsverificarespostadem
	lcPos = RECNO("ucrsverificarespostadem")
	
	SELECT ucrsverificarespostadem
	LOCATE FOR ucrsverificarespostadem.sel=.T.
	IF FOUND()
		lccont = lccont+1
	ENDIF
	
	SELECT ucrsverificarespostadem
	TRY
		GO lcPos 	
	CATCH
	ENDTRY

 IF(VARTYPE(dadosdem)<>"U")
	 IF lccont=0
		dadosdem.menu1.estado("", .F., "aaa", .F.)
		dadosdem.menu1.estado("", "", "", .T., "Sair", .T.)
	 ELSE	 
		dadosdem.menu1.gravar.img1.picture = mypath+"\imagens\icons\guardar_w.png"
		dadosdem.menu1.estado("", "", "Dispensar", .T., "Sair", .T.)
	 ENDIF
 ENDIF

ENDPROC

PROCEDURE uf_dadosdem_limpardados
 dadosdem.txtreceita.value = ""
 dadosdem.txtcodacesso.value = ""
 dadosdem.txtcoddiropcao.value = ""
 dadosdem.txtnrsns.value = ""
 dadosdem.txtNomeUtente.value = ""
 dadosdem.txtContactoUtente.value = ""
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_dem_dadosReceita ''
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("DadosDem.GrdPesq", "uCrsVerificaRespostaDEM", lcsql)
    uf_perguntalt_chama("OCORREU UMA ANOMALIA AO PREPARAR A CONSULTA. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
 ENDIF
 dadosdem.lblregistos.caption = ALLTRIM(STR(RECCOUNT("uCrsVerificaRespostaDEM")))+" registos."
ENDPROC
**
FUNCTION uf_imprimir_guia
 LPARAMETERS lcstamp, lcpainel, lcexporta
 PUBLIC mypaisnacional
 IF USED("uCrsBenef")
    fecha('uCrsBenef')
 ENDIF
 IF USED("uCrsFt")
    fecha("uCrsFt")
 ENDIF
 IF USED("uCrsFt2")
    fecha("uCrsFt2")
 ENDIF
 IF USED("uCrsFi")
    fecha("uCrsFi")
 ENDIF
 SELECT * FROM ft WITH (BUFFERING=.T.) INTO CURSOR uCrsFt READWRITE
 SELECT * FROM ft2 WITH (BUFFERING=.T.) INTO CURSOR uCrsFt2 READWRITE
 SELECT * FROM fi WITH (BUFFERING=.T.) INTO CURSOR uCrsFi READWRITE
 SELECT ucrsft
 IF  .NOT. USED("uCrsFt")
    RETURN .F.
 ELSE
    SELECT ucrsft
    IF  .NOT. RECCOUNT()>0
       RETURN .F.
    ENDIF
 ENDIF
 SELECT ucrsft
 SELECT ucrsft
 GOTO TOP
 SELECT ucrsft2
 GOTO TOP
 SELECT ucrsfi
 GOTO TOP
 LOCAL lcnrreceita
 SELECT ucrsverificarespostadem
 lcnrreceita = ALLTRIM(ucrsverificarespostadem.receita_nr)
 SELECT token,medicamento_descr, validade_linha, COUNT(medicamento_descr) AS tot, medicamento_cod, medicamento_cnpem, posologia FROM uCrsVerificaRespostaDEM WITH (BUFFERING=.T.) GROUP BY TOKEN,medicamento_descr, validade_linha, medicamento_cod, medicamento_cnpem, posologia INTO CURSOR uCrsVerificaRespostaDEM_tot READWRITE
 **IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
 IF uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + ALLTRIM(myTerm) + "'")

      IF RECCOUNT("ucrsverificarespostadem_tot") = 0
         RETURN .F.
      ENDIF

      PUBLIC upv_nrReceita, upv_nmCl

      upv_nmCl = IIF(dadosdem.txtnomeutente.value='', ALLTRIM(uCrsAtendComp.nomeutente), ALLTRIM(dadosdem.txtnomeutente.value))

      upv_nrReceita = lcnrreceita

      uv_idImp = uf_gerais_getUmValor("B_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Guia DEM'")

      uf_imprimirgerais_sendprinter_talao(uv_idImp , .f., .F.)    

 ELSE
    lcvalidaimp = uf_gerais_setimpressorapos(.T.)
    IF lcvalidaimp
        ??? CHR(27E0)+CHR(64E0)
        ??? CHR(27)+CHR(99)+CHR(48)+CHR(1)
        ??? CHR(27E0)+CHR(116)+CHR(003)
        ??? CHR(27)+CHR(33)+CHR(1)
        ?? " "
        ? LEFT(ALLTRIM(ucrse1.nomecomp), uf_gerais_getparameter("ADM0000000194", "NUM"))
        ??? CHR(27)+CHR(33)+CHR(1)
        ? SUBSTR(ucrse1.local, 1, 10)
        ?? " Tel:"
        ?? TRANSFORM(ucrse1.telefone, "#########")
        ?? " NIF:"
        ?? TRANSFORM(ucrse1.ncont, "999999999")
        ? ALLTRIM(uf_gerais_getMacrosReports(2))
        ? "Dir. Tec. "
        ?? LEFT(ALLTRIM(ucrse1.u_dirtec), uf_gerais_getparameter("ADM0000000194", "NUM"))
        ??
        uf_gerais_separadorpos()
        ? PADC("MEDICAMENTOS POR DISPENSAR", uf_gerais_getparameter("ADM0000000194", "NUM"), " ")
        uf_gerais_separadorpos()
        SELECT ucrsft
        ? DATETIME()
        SELECT ucrsft
        ? "Op: "
        ?? SUBSTR(ucrsft.vendnm, 1, 40)+" ("+ALLTRIM(STR(ucrsft.vendedor))+")"
        ? " "
        IF dadosdem.txtnomeutente.value=''
            ? "Cliente: "+ALLTRIM(uCrsAtendComp.nomeutente)
        ELSE
            ? "Cliente: "+ALLTRIM(dadosdem.txtnomeutente.value)
        ENDIF 
        ? " "
        ? "Receita Nr.: "+ALLTRIM(lcnrreceita)
        ? " "
        ? "Codigo de acesso: _______________ "
        ? " "
        ? "Codigo de op��o : _______________ "
        ? " "
        ? "PRODUTO(S)"
        uf_gerais_separadorpos()
        SELECT ucrsverificarespostadem_tot
        SCAN FOR  .NOT. EMPTY(ucrsverificarespostadem_tot.medicamento_descr)
        ? ALLTRIM(ucrsverificarespostadem_tot.medicamento_descr)
        ? "Validade Prescricao: "+DTOC(ucrsverificarespostadem_tot.validade_linha)+"        Quant: "+ALLTRIM(TRANSFORM(ucrsverificarespostadem_tot.tot, "999"))
        uf_gerais_separadorpos()
        ENDSCAN
        uf_gerais_feedcutpos()
    ENDIF
    uf_gerais_setimpressorapos(.F.)
    SET PRINTER TO DEFAULT
 ENDIF
 IF USED("uCrsBenef")
    fecha('uCrsBenef')
 ENDIF
 IF USED("uCrsFt")
    fecha("uCrsFt")
 ENDIF
 IF USED("uCrsFt2")
    fecha("uCrsFt2")
 ENDIF
 IF USED("uCrsFi")
    fecha("uCrsFi")
 ENDIF
 IF USED("uCrsVerificaRespostaDEM_tot")
    fecha("uCrsVerificaRespostaDEM_tot")
 ENDIF
ENDFUNC
**

PROCEDURE uf_dadosdem_setLbl
   LPARAMETERS uv_nrReceita

   IF EMPTY(uv_nrReceita)

      WITH DADOSDEM

         .Label1.caption = "C. de Acesso"
         .Label3.caption = "C. Direito Op��o"
         .grdpesq.isEfetivado.visible = .F.
         

      ENDWITH

   ENDIF

   uv_tipo = uf_gerais_devolveDescrDispensaEletronica(uf_gerais_retornaTipoDispensaEletronica(ALLTRIM(uv_nrReceita)))
   


   DO CASE
      CASE UPPER(ALLTRIM(uv_tipo)) = "MCDT"

         WITH DADOSDEM

            .Label1.caption = "Agendamento"
            .Label2.caption = "N�mero de Requisi��o"
            .Label3.caption = "Presta��o"
            .grdpesq.isEfetivado.visible = .T.
            .grdpesq.isEfetivado.width = 76

         ENDWITH


      OTHERWISE

         WITH DADOSDEM

            .Label1.caption = "C. de Acesso"
            .Label2.caption = "N�mero de Receita"
            .Label3.caption = "C. Direito Op��o"
            .grdpesq.isEfetivado.visible = .F.

         ENDWITH

   ENDCASE

   DADOSDEM.refresh

ENDPROC

FUNCTION uf_dadosdem_consultarHist
	
	LOCAL lcreceitanr, lccodacesso, lccoddiropcao, lcnrsns 
	STORE "" TO lcreceitanr, lccodacesso, lccoddiropcao, lcnrsns 
	
	if(USED("uCrsVerificaRespostaDEM"))
		if(RECCOUNT("uCrsVerificaRespostaDEM")>0)
			 uf_perguntalt_chama("N�o pode consultar hist�rico com nenhuma receita neste painel.", "OK", "", 16)
			 RETURN .f.
		ENDIF			
	ENDIF
   
    
   IF TYPE("DADOSDEM")<>"U"		
	   	lcreceitanr = ALLTRIM(dadosdem.txtreceita.value)
	   	lccodacesso = ALLTRIM(dadosdem.txtcodacesso.value)
	   	lccoddiropcao = ALLTRIM(dadosdem.txtcoddiropcao.value)
	   	lcnrsns = ALLTRIM(dadosdem.txtnrsns.value)
   ENDIF

		
   IF (uf_dadosdem_validareceitarepetida(lcreceitanr, lcnrsns, lccodacesso))
      uf_perguntalt_chama("N�o pode consultar hist�rico com nenhuma receita consultada neste painel.", "OK", "", 16)
       RETURN .F.
    ENDIF
		
   IF !uf_gerais_checkCedula()
	 RETURN .F.
   ENDIF

   if(VARTYPE(dadosDem) != "U")
   		dadosDem.alwaysOnTop = .f.
   endif	

   uf_histDem_chama(ALLTRIM(DADOSDEM.txtNrSns.value))

   IF WEXIST("HISTDEM")
      HISTDEM.show
      HISTDEM.txtNrUtente.setFocus()
   ENDIF

ENDFUNC

FUNCTION uf_dadosdem_consultarHistPEMH
	
	LOCAL lcreceitanr, lccodacesso, lccoddiropcao, lcnrsns 
	STORE "" TO lcreceitanr, lccodacesso, lccoddiropcao, lcnrsns 
		
   IF !uf_gerais_checkCedula()
	 RETURN .F.
   ENDIF

   if(VARTYPE(dadosDem) != "U")
   		dadosDem.alwaysOnTop = .f.
   endif	

   uf_histPEMH_chama(ALLTRIM(DADOSDEM.txtNrSns.value))

   IF WEXIST("HISTPEMH")
      HISTPEMH.show
   ENDIF

ENDFUNC

FUNCTION uf_dadosdem_EnvnotaTerapeutic

   IF !USED("uCrsVerificaRespostaDEM")
   	  uf_perguntalt_chama("N�o selecionou nenhuma linha de receita!", "OK", "", 48)
      RETURN .F.
   ENDIF

   IF RECCOUNT("uCrsVerificaRespostaDEM") = 0
   	  uf_perguntalt_chama("N�o selecionou nenhuma linha de receita!", "OK", "", 48)
      RETURN .F.
   ENDIF

   LOCAL uv_nrReceita, uv_nrLinha, uv_design, uv_operador, uv_utente
	
	SELECT uCrsVerificaRespostaDEM
	
	uv_nrReceita = ALLTRIM(uCrsVerificaRespostaDEM.receita_nr)	
	
	
	uv_nrLinha = ALLTRIM(uCrsVerificaRespostaDEM.id)
	uv_design = ALLTRIM(uCrsVerificaRespostaDEM.medicamento_descr)
	uv_utente = ALLTRIM(uCrsVerificaRespostaDEM.utente_nome)
	
	SELECT ucrsUser
	
	uv_operador = ALLTRIM(ucrsUser.nome)
	
	uf_envNotasDem_chama(uv_nrReceita, uv_nrLinha, uv_design, uv_operador, uv_utente)

	IF WEXIST("ENVNOTASDEM")

	   ENVNOTASDEM.txtNrReceita.setFocus()
	   
	ENDIF

ENDFUNC

FUNCTION uf_dadosdem_consultNotasDem

   LOCAL uv_nrReceita, uv_nrLinha, uv_operador, uv_operadorInis, uv_utente, uv_design
   STORE '' TO uv_nrReceita, uv_nrLinha, uv_operador, uv_operadorInis, uv_utente, uv_design

	IF USED("uCrsVerificaRespostaDEM")
		
	   IF RECCOUNT("uCrsVerificaRespostaDEM") <> 0
         
         SELECT uCrsVerificaRespostaDEM
         
         uv_nrReceita = ALLTRIM(uCrsVerificaRespostaDEM.receita_nr)	
         uv_utente = ALLTRIM(uCrsVerificaRespostaDEM.utente_nome)
                  
         uv_nrLinha = ALLTRIM(uCrsVerificaRespostaDEM.id)
         uv_design = ALLTRIM(uCrsVerificaRespostaDEM.medicamento_descr)

      ENDIF

   ENDIF
	
	SELECT ucrsUser
	
	uv_operador = ALLTRIM(ucrsUser.nome)
	uv_operadorInis = ALLTRIM(m_chInis)
	
	uf_histnotasdem_chama(uv_nrReceita, uv_nrLinha, uv_operador, uv_operadorInis, uv_utente, uv_design)

	IF WEXIST("HISTNOTASDEM")

	   HISTNOTASDEM.txtNrReceita.setFocus()
	  
	ENDIF

ENDFUNC