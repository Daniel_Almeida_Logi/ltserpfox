**
FUNCTION uf_envios_chama
 LPARAMETERS lcorigem
 PUBLIC lcrefminuta, lcsource
 STORE '' TO lcrefminuta
 lcsource = lcorigem
 lcsql = ""
 TEXT TO lcsql TEXTMERGE NOSHOW
		select ref as ident ,designacao 
		from b_sms 
		where tipoenvio='MINUTA' and codminuta='ENVIOS-UTENTES'
		order by ref
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "curMinutas", lcsql)
    uf_perguntalt_chama("OCORREU UM ERRO A LER AS MINUTAS.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF TYPE("ENVIOS")=="U"
    DO FORM ENVIOS
 ELSE
    envios.show
 ENDIF
ENDFUNC
**
PROCEDURE uf_envios_gravar
 SET POINT TO "."
 LOCAL lcsql
 STORE '' TO lcsql
ENDPROC
**
PROCEDURE uf_envios_sair
 envios.release
 RELEASE envios
ENDPROC
**
FUNCTION uf_sms_minuta
 LOCAL lcmsg, lccodenvio, lcdestino, lcwspath, lcutstamp
 STORE LEFT(ALLTRIM(m_chinis)+DTOC(DATE())+SYS(2015), 25) TO lccodenvio
 TEXT TO lcsql TEXTMERGE NOSHOW
		select utstamp, email, autoriza_emails, tlmvl, autoriza_sms from b_utentes where no=<<ft.No>> and estab=<<ft.Estab>>
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "ucrsDadosCL", lcsql)
    uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!", "OK", "", 16)
    RETURN .F.
 ENDIF
 IF  .NOT. EMPTY(ucrsdadoscl.tlmvl)
    IF ucrsdadoscl.autoriza_sms=.F.
       uf_perguntalt_chama("O UTENTE N�O AUTORIZOU O ENVIO DE SMS'S!", "OK", "", 16)
       RETURN .F.
    ELSE
       lcdestino = ALLTRIM(ucrsdadoscl.tlmvl)
       lcutstamp = ALLTRIM(ucrsdadoscl.utstamp)
    ENDIF
 ELSE
    uf_perguntalt_chama("O UTENTE N�O TEM O N�MERO DE TELEM�VEL PREENCHIDO NA SUA FICHA!", "OK", "", 16)
    RETURN .F.
 ENDIF
 fecha("ucrsDadosCL")
 lcsql = ""
 TEXT TO lcsql TEXTMERGE NOSHOW
		select email_body from b_sms where ref='<<ALLTRIM(lcRefMinuta)>>'
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "ucrsDadosMinuta", lcsql)
    uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE MINUTAS!", "OK", "", 16)
    RETURN .F.
 ENDIF
 SELECT ucrsdadosminuta
 lcmsg = ALLTRIM(ucrsdadosminuta.email_body)
 fecha("ucrsDadosMinuta")
 uf_send_sms(lccodenvio, 'ENVIOS', 'Atendimento', ALLTRIM(lcmsg), lcdestino, 'CL', lcutstamp, 'Envio de comunica��o por SMS para o nr '+ALLTRIM(lcdestino))
ENDFUNC
**
FUNCTION uf_email_minuta
 LOCAL lcto, lctoemail, lcsubject, lcbody, lcatatchment, lctoken, lctexto
 STORE '' TO lctexto
 TEXT TO lcsql TEXTMERGE NOSHOW
		select utstamp, email, autoriza_emails, tlmvl, autoriza_sms from b_utentes where no=<<ft.No>> and estab=<<ft.Estab>>
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "ucrsDadosCL", lcsql)
    uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!", "OK", "", 16)
    RETURN .F.
 ENDIF
 IF  .NOT. EMPTY(ucrsdadoscl.email)
    IF ucrsdadoscl.autoriza_emails=.F.
       uf_perguntalt_chama("O UTENTE N�O AUTORIZOU O ENVIO DE EMAIL'S!", "OK", "", 16)
       RETURN .F.
    ELSE
       lctoemail = ALLTRIM(ucrsdadoscl.email)
       lcutstamp = ALLTRIM(ucrsdadoscl.utstamp)
    ENDIF
 ELSE
    uf_perguntalt_chama("O UTENTE N�O TEM O ENDERE�O DE EMAIL PREENCHIDO NA SUA FICHA!", "OK", "", 16)
    RETURN .F.
 ENDIF
 fecha("ucrsDadosCL")
 SELECT ft
 lctexto = 'Exmo(a). Sr(a). '+ALLTRIM(ft.nome)+' '
 lcsql = ""
 TEXT TO lcsql TEXTMERGE NOSHOW
		select email_body from b_sms where ref='<<ALLTRIM(lcRefMinuta)>>'
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "ucrsDadosMinuta", lcsql)
    uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE MINUTAS!", "OK", "", 16)
    RETURN .F.
 ENDIF
 SELECT ucrsdadosminuta
 lctexto = lctexto+CHR(13)+ALLTRIM(ucrsdadosminuta.email_body)+CHR(13) + CHR(10)
 lctexto = lctexto+CHR(13)
 fecha("ucrsDadosMinuta")
 lcto = ""
 lctoemail = ALLTRIM(lctoemail)
 lcsubject = 'Envio de Informa��o'
 lcbody = lctexto+CHR(13)+ALLTRIM(ucrse1.mailsign)
 lcatatchment = ""
 regua(0, 6, "A enviar email ...")
 uf_startup_sendmail_emp(lcto, lctoemail, lcsubject, lcbody, lcatatchment, .T.)
 regua(2)
 uf_user_log_ins('CL', ALLTRIM(lcutstamp), 'ENVIOS', 'Envio de Minuta por Email para '+ALLTRIM(lctoemail))
ENDFUNC
**
