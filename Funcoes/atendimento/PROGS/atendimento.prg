*** 
*** ReFox XII  #PT125680  Jose Costa  Lts, Lda. [VFP90]
***
 DO caixas
 DO gestaocaixas
 DO gestaosessaocaixa
 DO infovenda
 DO principioactivo
 DO prodvendidos
 DO resumos
 DO somaratendimentos
 DO valorestpa
 DO criarclatend
 DO regvendas
 DO diplomas
 DO valespendentes
 DO pagcentral
 DO alterameiospag
 DO atcampanhas
 DO dadosdem
 DO dadosencat
 DO encomendamed
 DO excecaomed
 DO filaespera
 DO envios
 DO infoembal
 DO listafaltasrobot
 DO maqdinheiropag
 DO contagemnumerario
 DO mancompart
 DO identificacao
 DO histdem
 DO histPEMH
 DO histnotasdem
 DO envnotasdem

**
FUNCTION uf_atendimento_chama

 IF  .NOT. uf_gerais_validapermuser(ch_userno, ch_grupo, 'Atendimento - Abrir Atendimento')
    uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL DE ATENDIMENTO.", "OK", "", 48)
    RETURN .F.
 ENDIF
 uf_chk_nova_versao()
 
 
 PUBLIC myRefCovid, myChamaSinave, myNrRecSinave
 myRefCovid = '1888889'
 myChamaSinave = 'N�o chama'
 myNrRecSinave = ''
 
 regua(0, 7, "A carregar painel...")
 regua(1, 1, "A verificar Campanhas dispon�veis....")
 uf_atendimento_carregacampanhasut()
 uf_atendimento_carregacampanhasst()
 regua(1, 2, "A configurar Atendimento...")
 uf_atendimento_configuracao()
 uf_atendimento_criacursores()
 uf_atendimento_verificarobot()
 regua(1, 3, "A carregar painel...")
 IF (TYPE("atendimento")=="U")
    IF uf_gerais_addconnection("ATEND")==.F.
       regua(2)
       RETURN .F.
    ENDIF
    regua(1, 4, "A carregar painel...")
    DO FORM Atendimento
    atendimento.txtscanner.left = atendimento.txtscanner.left+4000
    atendimento.menu1.adicionaopcao("OPCOES", "Op��es", mypath+"\imagens\icons\opcoes_w.png", "", "F1")
    atendimento.menu1.adicionaopcao("pesqUtente", "Utente", mypath+"\imagens\icons\ut_w_at.png", "uf_atendimento_ProcuraCliente", "F2")
    atendimento.menu1.adicionaopcao("nova_venda", "Venda", mypath+"\imagens\icons\venda_w_at.png", "uf_atendimento_novaVenda WITH .t.", "F3")
    atendimento.menu1.adicionaopcao("pesquisar", "Produto", mypath+"\imagens\icons\st_w_at.png", "uf_atendimento_PesquisarST", "F4")
    atendimento.menu1.adicionaopcao("regularizar", "Regularizar", mypath+"\imagens\icons\historico_w.png", "uf_REGVENDAS_Chama", "F5")
    atendimento.menu1.adicionaopcao("conferir", "Conferir", mypath+"\imagens\icons\unchecked_w.png", "uf_atendimento_conferir", "F6")
    atendimento.menu1.adicionaopcao("dem", "DEM", mypath+"\imagens\icons\rec_linhas_w.png", "uf_atendimento_DadosDemChama", "F7")
    IF ((myusarobot .OR. myusarobotapostore .OR. myusarobotconsis .OR. myusaservicosrobot) .AND. myrobotstate) .OR. myusafarmax
       IF (EMPTY(myusaservicosrobot))
          atendimento.menu1.adicionaopcao("dispensar", "Dispensar", mypath+"\imagens\icons\robot_w.png", "uf_robot_dispensaFrontBulk with .F.", "F9")
       ELSE
          IF (uf_robot_servico_terminal_mapeado())
             atendimento.menu1.adicionaopcao("dispensar", "Dispensar", mypath+"\imagens\icons\robot_w.png", "uf_robot_servico_retrieve", "F9")
          ENDIF
       ENDIF
    ENDIF
        
    IF uf_gerais_getUmValor("B_terminal", "gaveta", "terminal = '" + ALLTRIM(myTerm) + "'")
    	 atendimento.menu_opcoes.adicionaopcao("gaveta", "Abrir Gaveta (F10)", "", "uf_pagamento_abreGaveta with .t.")
    ENDIF

   
    atendimento.menu_opcoes.adicionaopcao("reimprimir", "Reimprimir POS", "", "uf_reimppos_chama with ''")
    atendimento.menu_opcoes.adicionaopcao("somar", "Somar Atendimentos", "", "uf_SomarAtendimentos_chama")
	IF  uf_gerais_validaPermUser(ch_userno, ch_grupo,'Registo de Vacina��o')
		atendimento.menu_opcoes.adicionaopcao("vacinacao", "Reg.Vacina��o/Injet�vel", "", "uf_atendimento_OP_vacinacao")
	ENDIF
    atendimento.menu_opcoes.adicionaopcao("medicamentoHospitalar", "Registo Med. Hospitalar", "", "uf_atendimento_OP_medicamentoHospitalar")
    atendimento.menu_opcoes.adicionaopcao("sinave", "Registo Sinave", "", "uf_atendimento_chamaSinave")
    atendimento.menu_opcoes.adicionaopcao("GuiaAviamento", "Guia Aviamento", "", "uf_imprimir_guiaaviamento")

    IF uf_gerais_getparameter_site('ADM0000000061', 'BOOL', mysite)
       atendimento.menu_opcoes.adicionaopcao("sitecompart", "Site compart.", "", "uf_atendimento_site_compart")
    ENDIF
    
    atendimento.menu_opcoes.adicionaopcao("GestaoCaixas", "Gest�o Caixas", "", "uf_GestaoCaixas_chama with .f., .f.")
    
    
    IF !EMPTY(uf_gerais_getParameter_site('ADM0000000225', 'BOOL', mySite))
    	 atendimento.menu_opcoes.adicionaopcao("InteracaoMed", "Intera�ao Med.", "", "uf_atendimento_interacao_medicamentos")
    ENDIF

    
    IF uf_gerais_getparameter("ADM0000000189", "BOOL") .AND. (((myusarobot .OR. myusarobotapostore) .AND. myrobotstate) .OR. myusafarmax .OR. (myusaservicosrobot .AND. myrobotstate))
       atendimento.menu_opcoes.adicionaopcao("limpar", "Limpar", "", "uf_atendimento_limpar")
    ENDIF
    
&&    IF uf_gerais_getParameter_site('ADM0000000214', 'BOOL', mySite) = .T.
&&       atendimento.menu_opcoes.adicionaopcao("precoSpms", "Pre�os SPMS", "", "uf_atendimento_precosSpms")
&&    ENDIF

    atendimento.menu1.estado("conferir, dem", "HIDE")
    atendimento.refresh
    uf_atendimento_initsenhas()
    atendimento.refresh
    regua(1, 5, "A carregar painel...")
    uf_atendimento_valoresdefeito()
    regua(1, 6, "A carregar painel...")
    uf_atendimento_configgrid()
    atendimento.tmroperador.interval = (mytmpinactividade*1000)
    IF  .NOT. uf_gerais_getparameter("ADM0000000003", "BOOL")==.F. .AND. mytmpinactividade>0
       atendimento.tmroperador.enabled = .T.
    ELSE
       atendimento.tmroperador.enabled = .F.
    ENDIF
    IF ((myusarobot .OR. myusarobotapostore) .AND. myrobotstate) .OR. myusafarmax
       atendimento.tmrrobot.interval = IIF(myusafarmax=.T., 60000, 2500)
    ENDIF

	IF uf_gerais_getParameter_site("ADM0000000172", "BOOL", mySite)

		IF !EMPTY(uf_gerais_getParameter_site("ADM0000000174", "NUM", mySite))
			ATENDIMENTO.timerXop.interval = uf_gerais_getParameter_site("ADM0000000174", "NUM", mySite)
		ENDIF
		ATENDIMENTO.timerXop.enabled = .T.

	ENDIF

 ELSE
    regua(1, 6, "A carregar painel...")
    atendimento.show
 ENDIF
 IF UPPER(ALLTRIM(ucrse1.pais))=='PORTUGAL'
    atendimento.pgfdados.page3.txtelegibilidade.visible = .F.
    atendimento.pgfdados.page3.lblelegibilidade.visible = .F.
 ELSE
    atendimento.pgfdados.page3.lblncont.visible = .F.
    atendimento.pgfdados.page3.label1.visible = .F.
    atendimento.pgfdados.page3.label3.visible = .F.
    atendimento.pgfdados.page3.label4.visible = .F.
    atendimento.pgfdados.page3.txtcod.visible = .F.
    atendimento.pgfdados.page3.txtabrev.visible = .F.
    atendimento.pgfdados.page3.txtcod2.visible = .F.
    atendimento.pgfdados.page3.txtabrev2.visible = .F.
 ENDIF
 IF uf_gerais_getparameter_site('ADM0000000060', 'BOOL', mysite) .AND. ALLTRIM(ch_grupo)<>'Administrador' .AND. ALLTRIM(ch_grupo)<>'Supervisores'
    atendimento.pgfdados.page2.txtu_stock.visible = .F.
    atendimento.pgfdados.page2.txtstocktot.visible = .F.
    atendimento.pgfdados.page2.txtnum1.visible = .F.
    atendimento.pgfdados.page2.txtlocal1.visible = .F.
    atendimento.pgfdados.page2.txtlocal2.visible = .F.
    atendimento.pgfdados.page2.text5.visible = .F.
    atendimento.pgfdados.page2.text6.visible = .F.
 ENDIF
 IF  .NOT. uf_gerais_getparameter_site('ADM0000000067', 'BOOL', mysite)
    atendimento.pgfdados.page2.txtqtmv.visible = .F.
    atendimento.pgfdados.page2.label10.visible = .F.
 ENDIF
 
 
 IF  (!mymultivenda)
    atendimento.menu1.estado("nova_venda", "HIDE")
 ENDIF

 
 ** Utiliza medicamentos controlados
 IF uf_gerais_getParameter_site('ADM0000000152', 'BOOL', mySite)
 
 	IF empty(ucrsatendcomp.fistamp)
		SELECT ucrsatendcomp
		APPEND BLANK
		replace ucrsatendcomp.fistamp WITH ALLTRIM(fi.fistamp)
	ENDIF 

 	atendimento.pgfDados.Page3.txtCodigoAcesso.visible=.f.
	atendimento.pgfDados.Page3.Label18.visible=.f.
	atendimento.pgfDados.Page3.Label17.visible=.f.
	atendimento.pgfDados.Page3.txtCodigoDireitoOpcao.visible=.f.
	atendimento.pgfDados.Page3.Label8.visible=.f.
	atendimento.pgfDados.Page3.txtU_nbenef.visible=.f.
	atendimento.pgfDados.Page3.Label9.visible=.f.
	atendimento.pgfDados.Page3.txtU_nbenef2.visible=.f.
	atendimento.pgfDados.Page3.txtSzzstamp.visible=.f.
	atendimento.pgfDados.Page3.txtRvpstamp.visible=.f.
	atendimento.pgfDados.Page3.txtDesign.readonly=.f.
	atendimento.pgfDados.Page3.txtnomemedico.readonly=.f.
	atendimento.pgfDados.Page3.txtcontactomedico.readonly=.f.
	atendimento.pgfDados.Page3.txtespecialidademedico.readonly=.f.
	atendimento.pgfDados.Page3.txtlocalprescricao.readonly=.f.
	atendimento.pgfDados.Page3.txtnomeutente.readonly=.f.
	atendimento.pgfDados.Page3.txtcontactoutente.readonly=.f.
 ENDIF 
 
 regua(1, 7, "A carregar painel...")
 uf_atendimento_configoperador()
 atendimento.grdatend.setfocus
 atendimento.refresh
 _SCREEN.width = _SCREEN.width-1
 _SCREEN.width = _SCREEN.width+1
 regua(2)
 SELECT fi
 IF RECCOUNT("fi")<=1
    atendimento.menu1.sair.image1.picture = mypath+"\imagens\icons\sair_w.png"
    atendimento.menu1.estado("", "", "aa", .F., "Sair", .T.)
 ELSE
    uf_atendimento_conferir_cancelar()
 ENDIF
 tpsair = 1
 
 uf_servicos_pricesDem_closeCursors()
ENDFUNC
**
PROCEDURE uf_atendimento_gestaoDeCaixas
		uf_gestaocaixas_chama()
ENDPROC



PROCEDURE uf_atendimento_limpar
 atendimento.campanhas = .F.
 IF TYPE("UtilPlafond")<>"U"
 	utilplafond = .F.
 ENDIF
 uf_atendimento_valoresdefeito()
 
 uf_servicos_pricesDem_closeCursors()
 atendimento.pgfdados.page2.btnIntercacoes.Shape1.backcolor = RGB(0,  167, 231)
 fecha("ucrsMergeSearchFilter")
ENDPROC
**
FUNCTION uf_atendimento_validapaineisaberto
 IF (TYPE("documentos")=="O" .OR. TYPE("faturacao")=="O" .OR. TYPE("receituario")=="O")
    RETURN .F.
 ENDIF
ENDFUNC
**
PROCEDURE uf_atendimento_tmrcallnextticket
 IF (ucrse1.senhamotivosematendimento==.T.)
    WAIT WINDOW "A pr�xima senha ser� chamada em 5 segundos..." TIMEOUT 5
    uf_ticket_call_next()
 ENDIF
ENDPROC
**
PROCEDURE uf_atendimento_gravar

 uf_atendimento_precosSpms_atualizaLinhas()
   
 IF !uf_atendimento_chkReservaDEM("TERMINAR")
	RETURN .F.
 ENDIF
 uf_chk_nova_versao()
  
 IF RIGHT(ALLTRIM(atendimento.menu1.gravar.img1.picture), 25)=="\imagens\icons\save_w.png"
 
    uf_atendimento_finalizarvenda()
    
    IF (TYPE("atendimento")<>"U")
	    IF atendimento.menu1.conferir.tag=="true"
	          atendimento.menu1.conferir.tag = "false"
	    ENDIF
    ENDIF
    
    SELECT fi
    IF RECCOUNT("fi")<=1 .AND. TYPE("ATENDIMENTO")<>'U'
       atendimento.menu1.sair.image1.picture = mypath+"\imagens\icons\sair_w.png"
       atendimento.menu1.estado("", "", "aaa", .F., "Sair", .T.)
    ENDIF
 ELSE
    IF uf_gerais_getparameter("ADM0000000267", "BOOL") .AND. uf_gerais_getparameter_site("ADM0000000002", "BOOL", mysite)
       IF atendimento.menu1.conferir.tag=="true"
          atendimento.menu1.conferir.tag = "false"
          atendimento.menu1.gravar.img1.picture = mypath+"\imagens\icons\unchecked_w.png"
       ELSE
          IF uf_atendimento_todos_conferidos()
             atendimento.menu1.conferir.tag = "true"
             atendimento.menu1.gravar.img1.picture = mypath+"\imagens\icons\save_w.png"
             atendimento.menu1.sair.image1.picture = mypath+"\imagens\icons\cancelar_w.png"
             atendimento.menu1.estado("", "", "Terminar", .T., "Cancelar", .T.)
          ELSE
             atendimento.menu1.conferir.tag = "true"
             atendimento.menu1.gravar.img1.picture = mypath+"\imagens\icons\checked_w.png"
          ENDIF
       ENDIF
    ELSE

       IF uf_gerais_getparameter("ADM0000000267", "BOOL")
          IF ucrse1.obrigaconf=.T.
             IF atendimento.menu1.conferir.tag=="false"
                atendimento.menu1.conferir.tag = "true"
                IF uf_atendimento_todos_dem_conferidos()
                   uf_atendimento_salvar_cancelar()
                ELSE
                   uf_atendimento_conferindo_limpar()
                ENDIF
             ELSE
                atendimento.menu1.conferir.tag = "false"
                uf_atendimento_conferir_limpar()
             ENDIF
          ELSE
             atendimento.menu1.conferir.tag = "true"
             uf_atendimento_salvar_cancelar()
          ENDIF
       ENDIF
    ENDIF
 ENDIF
 
 

ENDPROC
**
PROCEDURE uf_atendimento_op_vacinacao
 atendimento.tmroperador.reset()
 uf_vacinacao_chama()
ENDPROC
**
PROCEDURE uf_atendimento_op_medicamentohospitalar
 atendimento.tmroperador.reset()
 IF  .NOT. (TYPE("MEDICAMENTOHOSPITALAR")=="U")
    IF myvacintroducao .OR. myvacalteracao
       uf_perguntalt_chama("O ECR� DE REGISTO DE MEDICAMENTO HOSPITALAR ENCONTRA-SE EM MODO DE ALTERA��O / INTRODU��O! TERMINE ESTA OPERA��O E TENTE NOVAMENTE.", "OK", "", 64)
       medicamentohospitalar.show
    ELSE
       uf_medicamentohospitalar_chama()
    ENDIF
 ELSE
    uf_medicamentohospitalar_chama()
 ENDIF
ENDPROC
**
PROCEDURE uf_atendimento_op_fornecedores
 atendimento.tmroperador.reset()
 IF  .NOT. (TYPE("FORN")=="U")
    IF myflintroducao .OR. myflalteracao
       uf_perguntalt_chama("O ECR� DE FORNECEDORES ENCONTRA-SE EM MODO DE ALTERA��O / INTRODU��O! TERMINE ESTA OPERA��O E TENTE NOVAMENTE.", "OK", "", 64)
       RETURN
    ENDIF
 ENDIF
 uf_forn_chama()
ENDPROC
**
PROCEDURE uf_atendimento_op_facturacao
 atendimento.tmroperador.reset()
 IF uf_perguntalt_chama("PARA ABRIR O ECR� DE FACTURA��O NECESSITA DE SAIR DO ECR� DE ATENDIMENTO, TEM A CERTEZA QUE PRETENDE CONTINUAR?"+CHR(13)+CHR(13)+"SE 'SIM' IR� PERDER TODOS OS DADOS.", "Sim", "N�o")
    uf_atendimento_sair(.T.)
    uf_facturacao_chama('')
 ENDIF
ENDPROC
**
PROCEDURE uf_atendimento_op_imprimirposologia
 atendimento.tmroperador.reset()
 IF EMPTY(ALLTRIM(atendimento.pgfdados.page2.txtu_posprog.value))
    atendimento.pgfdados.page2.txtu_posprog.value = "Posologia n�o definida."
 ENDIF
 uf_etiquetas_posologia_predef()
 WAIT WINDOW "" TIMEOUT 1
 uf_etiquetas_posologia()
ENDPROC
**
PROCEDURE uf_atendimento_op_imprimirposologia_l
 atendimento.tmroperador.reset()
 IF EMPTY(ALLTRIM(atendimento.pgfdados.page2.txtu_posprog.value))
    atendimento.pgfdados.page2.txtu_posprog.value = "Posologia n�o definida."
 ENDIF
 uf_etiquetas_posologia_predef()
 WAIT WINDOW "" TIMEOUT 1
 uf_etiquetas_posologia_l()
ENDPROC
**
FUNCTION uf_atendimento_op_enviarposologia
 LOCAL lctemposologia
 STORE .F. TO lctemposologia
 atendimento.tmroperador.reset()
 IF ucrse1.usacoms=.T.
    SELECT ucrsatendst
    GOTO TOP
    SCAN
       IF  .NOT. EMPTY(ALLTRIM(LEFT(u_posprog, 254)))
          lctemposologia = .T.
       ENDIF
    ENDSCAN
    IF lctemposologia=.F.
       uf_perguntalt_chama("POSOLOGIA N�O DEFINIDA."+CHR(13)+"POR FAVOR PREENCHA A MESMA.", "OK", "", 48)
       RETURN .F.
    ENDIF
    IF uf_perguntalt_chama("ESCOLHA A FORMA DE ENVIO", "SMS", "EMAIL")
       uf_sms_posologia()
    ELSE
       uf_email_posologia()
    ENDIF
 ELSE
    uf_perguntalt_chama("PARA UTILIZAR ESTA FUNCIONALIDADE DEVER� ADQUIRIR O M�DULO DE COMUNICA��ES."+CHR(13)+"PARA MAIS INFORMA��ES POR FAVOR CONTACTE A LOGITOOLS (OP��O - COMERCIAL).", "OK", "", 16)
 ENDIF
ENDFUNC
**
PROCEDURE uf_atendimento_op_documentos
 atendimento.tmroperador.reset()
 IF uf_perguntalt_chama("PARA ABRIR O ECR� DE DOCUMENTOS NECESSITA DE SAIR DO ECR� DE ATENDIMENTO, TEM A CERTEZA QUE PRETENDE CONTINUAR?"+CHR(13)+CHR(13)+"SE 'SIM' IR� PERDER TODOS OS DADOS.", "Sim", "N�o")
    uf_atendimento_sair(.T.)
    IF TYPE("documentos")=="U"
       uf_documentos_chama('')
    ELSE
       documentos.show()
    ENDIF
 ENDIF
ENDPROC
**
PROCEDURE uf_atendimento_seloperador
 uf_painelcentral_login()
ENDPROC
**
FUNCTION uf_atendimento_sair
 LPARAMETERS lcbool
 regua(2)
 PUBLIC myretornocancelar
 DO CASE
    CASE (TYPE("ATENDIMENTO")<>'U') .AND. RIGHT(ALLTRIM(atendimento.menu1.gravar.img1.picture), 25)=="\imagens\icons\save_w.png" .AND. RIGHT(ALLTRIM(atendimento.menu1.sair.image1.picture), 29)=="\imagens\icons\cancelar_w.png"
       atendimento.menu1.gravar.img1.picture = mypath+"\imagens\icons\unchecked_w.png"
       atendimento.menu1.sair.image1.picture = mypath+"\imagens\icons\limpar_w.png"
       atendimento.menu1.conferir.tag = "false"
       atendimento.menu1.estado("", "", "Conferir", .T., "Limpar", .T.)
    CASE (TYPE("ATENDIMENTO")<>'U') .AND. RIGHT(ALLTRIM(atendimento.menu1.gravar.img1.picture), 28)=="\imagens\icons\checked_w.png" .AND. RIGHT(ALLTRIM(atendimento.menu1.sair.image1.picture), 29)=="\imagens\icons\cancelar_w.png"
       atendimento.menu1.gravar.img1.picture = mypath+"\imagens\icons\unchecked_w.png"
       atendimento.menu1.sair.image1.picture = mypath+"\imagens\icons\limpar_w.png"
       atendimento.menu1.conferir.tag = "false"
       atendimento.menu1.estado("", "", "Conferir", .T., "Limpar", .T.)
    CASE (TYPE("ATENDIMENTO")<>'U') .AND. RIGHT(ALLTRIM(atendimento.menu1.gravar.img1.picture), 30)=="\imagens\icons\unchecked_w.png" .AND. RIGHT(ALLTRIM(atendimento.menu1.sair.image1.picture), 27)=="\imagens\icons\limpar_w.png"
       SELECT fi
       IF RECCOUNT("fi")>1
          IF uf_gerais_getparameter_site('ADM0000000060', 'BOOL', mysite)
             IF ALLTRIM(ch_grupo)<>'Administrador' .AND. ALLTRIM(ch_grupo)<>'Supervisores'
                IF  .NOT. uf_gerais_valida_password_admin('ATENDIMENTO', 'LIMPAR ATENDIMENTO')
                   RETURN .F.
                ENDIF
             ENDIF
          ENDIF
       ENDIF
	   IF uf_gerais_getParameter("ADM0000000342","BOOL")
			IF !uf_perguntalt_chama("Pretende Limpar o Atendimento?", "Sim", "N�o")
				RETURN .F.
			ENDIF
	   ENDIF
       uf_atendimento_limpar()
       SELECT fi
       IF RECCOUNT("fi")<=1
          atendimento.menu1.sair.image1.picture = mypath+"\imagens\icons\sair_w.png"
          atendimento.menu1.estado("", "", "aa", .F., "Sair", .T.)
       ENDIF
    CASE (TYPE("ATENDIMENTO")<>'U') .AND. RIGHT(ALLTRIM(atendimento.menu1.gravar.img1.picture), 25)=="\imagens\icons\save_w.png" .AND. RIGHT(ALLTRIM(atendimento.menu1.sair.image1.picture), 27)=="\imagens\icons\limpar_w.png"
       SELECT fi
       IF RECCOUNT("fi")>1
          IF uf_gerais_getparameter_site('ADM0000000060', 'BOOL', mysite)
             IF ALLTRIM(ch_grupo)<>'Administrador' .AND. ALLTRIM(ch_grupo)<>'Supervisores'
                IF  .NOT. uf_gerais_valida_password_admin('ATENDIMENTO', 'LIMPAR ATENDIMENTO')
                   RETURN .F.
                ENDIF
             ENDIF
          ENDIF
       ENDIF
	   IF uf_gerais_getParameter("ADM0000000342","BOOL")
			IF !uf_perguntalt_chama("Pretende Limpar o Atendimento?", "Sim", "N�o")
				RETURN .F.
			ENDIF
	   ENDIF
       uf_atendimento_limpar()
       atendimento.menu1.sair.image1.picture = mypath+"\imagens\icons\sair_w.png"
       atendimento.menu1.estado("", "", "aa", .F., "Sair", .T.)
    OTHERWISE
       IF uf_gerais_getparameter("ADM0000000142", "BOOL") .AND. USED("FI")
          SELECT fi
          LOCATE FOR ALLTRIM(fi.ref)=="R000001" .AND. EMPTY(ALLTRIM(fi.ofistamp))
          IF FOUND()
             uf_perguntalt_chama("Existe um Adiantamento de Reserva no atendimento."+CHR(13)+"N�o pode fechar o atendimento.", "OK", "", 48)
             RETURN .F.
          ENDIF
       ENDIF
       IF TYPE("REGVENDAS")<>"U"
          uf_regvendas_sair()
       ENDIF
       IF TYPE("PESQUTENTES")<>"U"
          uf_pesqutentes_sair()
       ENDIF
       IF TYPE("RESERVAS")<>"U"
          uf_reservas_sair()
       ENDIF
       IF TYPE("PESQSTOCKS")<>"U"
          uf_pesqstocks_sair()
       ENDIF
       IF TYPE("filaEspera")<>"U"
          uf_filaespera_sair()
       ENDIF
       IF  .NOT. (TYPE("excecaoMed")=="U")
          uf_excecaomed_sair()
       ENDIF

       IF TYPE("atendimento")<>"U"

          ATENDIMENTO.timer1.Enabled = .F.
          ATENDIMENTO.timer2.Enabled = .F.
          ATENDIMENTO.timerXop.Enabled = .F.
          ATENDIMENTO.tmrCallNext.Enabled = .F.
          ATENDIMENTO.tmrConsultaDEM.Enabled = .F.
          ATENDIMENTO.tmrOperador.Enabled = .F.
          ATENDIMENTO.tmrRobot.Enabled = .F.
          ATENDIMENTO.tmrValidacaoDEM.Enabled = .F.

          atendimento.hide()
          atendimento.release
       ENDIF
       fecha("ft")
       fecha("fi")
       fecha("ft2")
       fecha("dadospsico")
       fecha("UCRSATENDST")
       fecha("uCrsAtendCL")
       fecha("UCRSATENDDCI")
       fecha("UCRSATENDMAISBARATOS")
       fecha("UCRSATENDLAB")
       fecha("uCrsCabVendas")
       fecha("fi2")
       fecha("uCrsProdInteraccoes")
	   fecha("uCrsInteraccoes")
       fecha("ucrsCovidAtend")
       fecha("uc_rnuRecms")
       IF USED("fimancompart")
          fecha("fimancompart")
       ENDIF
       IF (USED("uCrsPrecosValidos"))
          fecha("uCrsPrecosValidos")
       ENDIF
       IF (USED("uCrsVerificaRespostaDEM"))
          fecha("uCrsVerificaRespostaDEM")
       ENDIF
       IF (USED("uCrsVerificaRespostaDEMTotal"))
          fecha("uCrsVerificaRespostaDEMTotal")
       ENDIF
       IF (USED("ucrsDemEfetivada"))
          fecha("ucrsDemEfetivada")
       ENDIF
       IF USED("ucrsvaliva")
          fecha("ucrsvaliva")
       ENDIF
       IF USED("fi_trans_info")
          fecha("fi_trans_info")
       ENDIF
       IF USED("mixed_bulk_pend")
          fecha("mixed_bulk_pend")
       ENDIF
       IF USED("nrserie_com_results")
          fecha("nrserie_com_results")
       ENDIF
       IF USED("uCrsPlanosDemAutomaticos")
          fecha("uCrsPlanosDemAutomaticos")
       ENDIF
       IF USED("crsAtendStCopia")
          fecha("crsAtendStCopia")
       ENDIF
       IF USED("crsFICopia")
          fecha("crsFICopia")
       ENDIF
       IF USED("crsFTCopia")
          fecha("crsFTCopia")
       ENDIF
       IF USED("crsFT2Copia")
          fecha("crsFT2Copia")
       ENDIF
       IF USED("ucrsCampanhasList")
          fecha("ucrsCampanhasList")
       ENDIF
       IF USED("ucrsValcartaoList")
          fecha("ucrsValcartaoList")
       ENDIF
       IF USED("uCrsExcecaoPanel")
          fecha("uCrsExcecaoPanel")
       ENDIF
       IF USED("uCrsExcecaoPanelRel")
          fecha("uCrsExcecaoPanelRel")
       ENDIF
       IF USED("uCrsFiReservas")
          fecha("uCrsFiReservas")
       ENDIF
       if(USED("ucrsSNSExternoAtend"))
	  	 fecha("ucrsSNSExternoAtend")
	   ENDIF
	

       
       RELEASE atendimento
       RELEASE mymultivenda
       RELEASE mycopyplano
       RELEASE mypermitealtnome
       RELEASE mypedelocalpresc
       RELEASE mypedecodpresc
       RELEASE myvendeclgenerico
       RELEASE myimptalaoconjunto
       RELEASE mypvpnegavativo
       RELEASE myvendascredito
       RELEASE mytmpinactividade
       RELEASE mytimer
       RELEASE mydevolucoescredito
       RELEASE myvalidatroco
       RELEASE mydevolucoesdinheiro
       RELEASE mydemconsultada
       RELEASE plafonddisput
       RELEASE utilplafond
       RELEASE nratendimentorec
       IF TYPE("myDemDispensada")<>"U"
          RELEASE mydemdispensada
       ENDIF
       IF TYPE("stampcompart")<>"U"
       	stampcompart  = ''
       ENDIF
  

       IF uf_gerais_getparameter_site('ADM0000000084', 'BOOL', mysite)
          IF ALLTRIM(ch_grupo)='Operadores'
             TEXT TO lcsql TEXTMERGE NOSHOW
						exec up_manutencao_ActualizaPainelCentralPelosTerminais_soatendimento 0, '<<mySite>>'
             ENDTEXT
          ELSE
             TEXT TO lcsql1 TEXTMERGE NOSHOW
						delete from b_pcentral WHERE terminal = <<IIF(TYPE("myTermNo") == "U", 0, myTermNo)>>
             ENDTEXT
             uf_gerais_actgrelha("", "", lcsql1)
             TEXT TO lcsql TEXTMERGE NOSHOW
						exec up_manutencao_ActualizaPainelCentralPelosTerminais 0, '<<mySite>>', <<IIF(TYPE("myTermNo") == "U", 0, myTermNo)>>
             ENDTEXT
          ENDIF
          IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
             MESSAGEBOX("OCORREU UMA ANOMALIA A CONFIGURAR O PAINEL CENTRAL.", 16, "LOGITOOLS SOFTWARE")
             RETURN .F.
          ENDIF
          FOR EACH mf IN painelcentral.objects
             IF LIKE("*PAGINA*", UPPER(ALLTRIM(mf.name)))==.T.
                painelcentral.removeobject(mf.name)
             ENDIF
          ENDFOR

          IF(TYPE("painelcentral.menu1")!="U")
          	painelcentral.menu1.visible = .F.
          ENDIF
          	
          CREATE CURSOR uCrsPaineis (nome C(100))
          FOR i = 1 TO _SCREEN.formcount
             SELECT ucrspaineis
             APPEND BLANK
             REPLACE ucrspaineis.nome WITH _SCREEN.forms(i).name
          ENDFOR
          LOCAL lcpainel
          SELECT DISTINCT nome FROM uCrsPaineis INTO CURSOR uCrsPaineisFechar READWRITE
          SELECT ucrspaineisfechar
          SCAN
             IF  .NOT. (UPPER(ALLTRIM(ucrspaineisfechar.nome))=="PAINELCENTRAL")
                lcpainel = ucrspaineisfechar.nome
                TRY
                   &lcpainel..RELEASE()
                CATCH
                ENDTRY
             ENDIF
          ENDSCAN
          uf_painelcentral_carregamenu(1)
          uf_painelcentral_alternapagina(1)
       ENDIF
 ENDCASE
 uf_painelcentral_alternapagina(1)
 uf_painelcentral_alternapagina(2)
ENDFUNC
**
FUNCTION uf_atendimento_alteranomecl
 atendimento.tmroperador.reset()
 SELECT ft
 IF ft.no==200
    IF mypermitealtnome
       uf_perguntalt_chama("O SISTEMA N�O PERMITE ALTERAR O NOME DO CLIENTE GEN�RICO.", "OK", "", 48)
       RETURN .F.
    ELSE
       uf_tecladoalpha_chama("Atendimento.txtNome", "INTRODUZA O NOME DO CLIENTE:", .F., .F., 1)
       SELECT ft
       SELECT ucrsatendcl
       IF  .NOT. EMPTY(ALLTRIM(ft.nome))
          REPLACE ucrsatendcl.nome WITH ft.nome
       ELSE
          REPLACE ucrsatendcl.nome WITH "CONSUMIDOR FINAL"
          REPLACE ft.nome WITH "CONSUMIDOR FINAL"
       ENDIF
    ENDIF
 ENDIF
ENDFUNC
**
FUNCTION uf_atendimento_criacursores
 IF ( .NOT. USED("ucrsDemEfetivada"))
    CREATE CURSOR ucrsDemEfetivada (token C(50))
 ENDIF
 IF ( .NOT. USED("uCrsPrecosValidos"))
    lcsql = ""
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_touch_hist_precos ''
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "uCrsPrecosValidos", lcsql)
       uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [uCrsPrecosValidos]. Por favor reinicie o Software.", "OK", "", 16)
       RETURN .F.
    ENDIF
 ENDIF
 IF  .NOT. USED("FT")
    IF  .NOT. uf_gerais_actgrelha("", 'FT', [set fmtonly on exec up_touch_ft '' set fmtonly off])
       uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [FT]. Por favor reinicie o Software.", "OK", "", 16)
       RETURN .F.
    ENDIF
 ELSE
    DELETE FROM ft
 ENDIF
 IF TYPE("ATENDIMENTO")=="U"
    TEXT TO lcsql TEXTMERGE NOSHOW
			select 
				convert(bit,0) as manipulado
				,convert(varchar(3),'') as tipoR
				,fi.id_Dispensa_Eletronica_D as id_validacaoDem
				,convert(varchar(20),'') as receita
				,convert(varchar(40),'') as token
				,convert(bit,0) as dem
				,0 as qtDem
				,convert(varchar(250),design) as design
				,convert(bit,0) as usalote
				,convert(bit,0) as alertalote
				,convert(bit,0) as compSNS
				,convert(varchar(8),'') as CNPEM
				,0.000 as PvpTop4
				,0 as qttrobot
				,convert(varchar(200),'') as obs
				,convert(varchar(20),'') as tpres
				,convert(bit,0) as encomenda 
				,convert(varchar(40),'') as EncReceita
				,convert(varchar(40),'') as EncPinReceita
				,convert(varchar(40),'') as EncCodOpReceita
				,convert(varchar(40),'') as EncPagam
				,convert(varchar(40),'') as EncMomPagam
				,convert(varchar(40),'') as bostamp
				,convert(varchar(40),'') as nmdos
				,0  as tipoErro
				,convert(varchar(20),'') as codmotiseimp
				,convert(varchar(150),'') as motiseimp
				,* 
            ,CAST(0 as bit) as forceCalcCompart
			from 
				FI (nolock) 
				left join fi_comp (nolock) on fi.fistamp = fi_comp.id_fi
			WHERE 
				0=1
    ENDTEXT
    uf_gerais_actgrelha("", "FI", lcsql)
 ELSE
    TEXT TO lcsql TEXTMERGE NOSHOW
			select 
				convert(bit,0) as manipulado
				,convert(varchar(3),'') as tipoR
				,fi.id_Dispensa_Eletronica_D as id_validacaoDem
				,convert(varchar(20),'') as receita
				,convert(varchar(40),'') as token
				,convert(bit,0) as dem
				,0 as qtDem
				,convert(varchar(250),design) as design
				,convert(bit,0) as usalote
				,convert(bit,0) as alertalote
				,convert(bit,0) as compSNS 
				,convert(varchar(8),'') as CNPEM
				,0.000 as PvpTop4
				,0 as qttrobot
				,convert(varchar(200),'') as obs
				,convert(varchar(20),'') as tpres
				,convert(bit,0) as encomenda 
				,convert(varchar(40),'') as EncReceita
				,convert(varchar(40),'') as EncPinReceita
				,convert(varchar(40),'') as EncCodOpReceita
				,convert(varchar(40),'') as EncPagam
				,convert(varchar(40),'') as EncMomPagam
				,convert(varchar(40),'') as bostamp
				,convert(varchar(40),'') as nmdos
				,convert(varchar(40),'') as codigoDemResposta
				,0  as tipoErro
				,convert(varchar(20),'') as codmotiseimp
				,convert(varchar(150),'') as motiseimp
                ,convert(varchar(200),'') as obsCl
                ,convert(varchar(200),'') as obsInt
				,* 
            ,CAST(0 as bit) as forceCalcCompart
			from 
				FI (nolock) 
				left join fi_comp (nolock) on fi.fistamp = fi_comp.id_fi
			WHERE 
				0=1
    ENDTEXT
    uf_gerais_actgrelha("Atendimento.grdAtend", "FI", lcsql)
 ENDIF
 IF USED("fi2")
    fecha("fi2")
 ENDIF
 IF USED("ucrsCovidAtend")
 	fecha("ucrsCovidAtend")
 ENDIF
 
 TEXT TO lcsql TEXTMERGE NOSHOW
		select 
			*
		from 
			FI2 (nolock) 
		WHERE 
			0=1
 ENDTEXT
 uf_gerais_actgrelha("", "FI2", lcsql)
 IF uf_gerais_getparameter_site('ADM0000000073', 'BOOL', mysite)
    IF USED("fimancompart")
       fecha("fimancompart")
    ENDIF

    CREATE CURSOR fimancompart (fistamp C(25), ref C(18), design C(100), valor N(10,2), perc N(9,5), valortot N(10,2), lordem N(10), descSeg N(10,2), totLin N(10,2), totLinDescSeg N(10,2))

 ENDIF
 IF  .NOT. USED("FT2")
    IF  .NOT. uf_gerais_actgrelha("", 'FT2', 'set fmtonly on select * from FT2 (nolock) set fmtonly off')
       uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [FT2]. Por favor reinicie o Software.", "OK", "", 16)
       RETURN .F.
    ENDIF
 ELSE
    DELETE FROM FT2
 ENDIF
 FECHA("fi_trans_info")
 IF  .NOT. USED("fi_trans_info")
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			set fmtonly on 
			select 
				*
				,CAST('' as varchar(30)) as verify_i
				,CAST('' as varchar(100)) as verify_s
				, CAST('' as varchar(100)) as tcode 
				, CAST('' as varchar(254)) as verifymsg 
				, CAST('' as varchar(100)) as verif_reason
				,CAST('' as varchar(30)) as dispense_i
				, CAST('' as varchar(100)) as dispense_s 
				, CAST('' as varchar(254)) as dispensemsg 
				, CAST('' as varchar(100)) as disp_reason 
				,CAST('' as varchar(30)) as undo_i
				, CAST('' as varchar(100)) as undo_s 
				, CAST('' as varchar(254)) as undodispensemsg 
				, CAST('' as varchar(100)) as undo_reason 
				, CAST('' as varchar(10)) as tipo
				, CAST(0 as bit) as conferido
				, CAST(0 as bit) as deleted
			from 
				fi_trans_info (nolock) 
			set fmtonly off
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "fi_trans_info", lcsql)
       uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [fi_trans_info]. Por favor reinicie o Software.", "OK", "", 16)
       RETURN .F.
    ENDIF
 ELSE
    DELETE FROM fi_trans_info
 ENDIF
 IF  .NOT. USED("mixed_bulk_pend")
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			set fmtonly on 
			select 
				*
			from 
				mixed_bulk_pend (nolock) 
			set fmtonly off
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "mixed_bulk_pend", lcsql)
       uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [mixed_bulk_pend]. Por favor reinicie o Software.", "OK", "", 16)
       RETURN .F.
    ENDIF
 ELSE
    DELETE FROM mixed_bulk_pend
 ENDIF
 IF  .NOT. USED("nrserie_com_results")
    IF  .NOT. uf_gerais_actgrelha("", 'nrserie_com_results', 'set fmtonly on select * from nrserie_com_results (nolock) set fmtonly off')
       uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [nrserie_com_results]. Por favor reinicie o Software.", "OK", "", 16)
       RETURN .F.
    ENDIF
 ELSE
    DELETE FROM nrserie_com_results
 ENDIF
 IF  .NOT. USED("fi_nnordisk")
    TEXT TO lcsql TEXTMERGE NOSHOW
			select 
				cast('' as varchar(15)) as ref ,* 
			from 
				fi_nnordisk(nolock) 
			WHERE 
				0=1
    ENDTEXT
    uf_gerais_actgrelha("", "fi_nnordisk", lcsql)
 ELSE
    DELETE FROM fi_nnordisk
 ENDIF
 IF  .NOT. USED("ft_compart")
    IF  .NOT. uf_gerais_actgrelha("", 'ft_compart', 'select * from ft_compart (nolock) where 0=1')
       uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [ft_compart]. Por favor reinicie o Software.", "OK", "", 16)
       RETURN .F.
    ENDIF
 ELSE
    DELETE FROM ft_compart
 ENDIF
 IF  .NOT. USED("ft_compart_result")
    IF  .NOT. uf_gerais_actgrelha("", 'ft_compart_result', 'select * from ft_compart_result (nolock) where 0=1')
       uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [ft_compart_result]. Por favor reinicie o Software.", "OK", "", 16)
       RETURN .F.
    ENDIF
 ELSE
    DELETE FROM ft_compart_result
 ENDIF
 IF  .NOT. USED("fi_compart")
    IF  .NOT. uf_gerais_actgrelha("", 'fi_compart', 'select * from fi_compart (nolock) where 0=1')
       uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [fi_compart]. Por favor reinicie o Software.", "OK", "", 16)
       RETURN .F.
    ENDIF
 ELSE
    DELETE FROM fi_compart
 ENDIF
 fecha("DADOSPSICO")
 IF  .NOT. uf_gerais_actgrelha("", 'DADOSPSICO', "set fmtonly on select *,cabVendasOrdem = 0 from B_dadosPsico(nolock) set fmtonly off")
    uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [DADOSPSICO]. Por favor reinicie o Software.", "OK", "", 16)
    RETURN .F.
 ENDIF
 IF  .NOT. USED("uCrsAtendCL")
    IF  .NOT. uf_gerais_actgrelha("", 'uCrsAtendCL', 'set fmtonly on Exec up_touch_dadosCliente 200,0 set fmtonly off')
       uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [UCRSATENDCL]. Por favor reinicie o Software.", "OK", "", 16)
       RETURN .F.
    ENDIF
 ELSE
    DELETE FROM uCrsAtendCL
 ENDIF
 IF  .NOT. USED("UCRSATENDCLPATO")
    IF  .NOT. (uf_gerais_actgrelha("", 'UCRSATENDCLPATO', [set fmtonly on exec up_clientes_patologias '' set fmtonly off]))
       uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [UCRSATENDCLPATO]. Por favor reinicie o Software.", "OK", "", 16)
       RETURN .F.
    ENDIF
 ELSE
    DELETE FROM UCRSATENDCLPATO
 ENDIF
 IF  .NOT. (uf_gerais_actgrelha("", 'UCRSATENDST', [exec up_touch_ActRef 'xxxxxxxxxxx', 0, 0]))
    uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [UCRSATENDST]. Por favor reinicie o Software.", "OK", "", 16)
    RETURN .F.
 ENDIF
 IF  .NOT. USED("UCRSATENDDCI")
    IF  .NOT. uf_gerais_actgrelha("", 'UCRSATENDDCI', [set fmtonly on exec up_touch_DCIp '' set fmtonly off])
       uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [UCRSATENDDCI]. Por favor reinicie o Software.", "OK", "", 16)
       RETURN .F.
    ENDIF
 ELSE
    DELETE FROM UCRSATENDDCI
 ENDIF
 IF  .NOT. USED("UCRSATENDCOMP")
    IF  .NOT. uf_gerais_actgrelha("", "UCRSATENDCOMP", [set fmtonly on exec up_receituario_dadosDoPlano '' set fmtonly off])
       uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [UCRSATENDCOMP]. Por favor reinicie o Software.", "OK", "", 16)
       RETURN .F.
    ENDIF
 ELSE
    DELETE FROM UCRSATENDCOMP
 ENDIF
 IF  .NOT. USED("UCRSATENDMAISBARATOS")
    IF  .NOT. uf_gerais_actgrelha("", "UCRSATENDMAISBARATOS", [set fmtonly on EXEC up_touch_tem5maisBaratos '', '', 0 set fmtonly off])
       uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [UCRSATENDMAISBARATOS]. Por favor reinicie o Software.", "OK", "", 16)
       RETURN .F.
    ENDIF
 ELSE
    DELETE FROM UCRSATENDMAISBARATOS
 ENDIF
 IF uf_gerais_getparameter("ADM0000000204", "BOOL")
    IF  .NOT. USED("UCRSATENDLAB")
       lcsql = ""
       TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_touch_temProdutosMLab '', '', 0, '', <<mysite_nr>>
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "UCRSATENDLAB", lcsql)
          uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [UCRSATENDLAB]. Por favor reinicie o Software.", "OK", "", 16)
          RETURN .F.
       ENDIF
    ELSE
       DELETE FROM UCRSATENDLAB
    ENDIF
 ENDIF
 IF USED("uCrsPlanosCombinados")
    fecha("uCrsPlanosCombinados")
 ENDIF
 IF  .NOT. (uf_gerais_actgrelha("", 'UCRSATENDST', [exec up_touch_ActRef 'xxxxxxxxxxx', 0, 0]))
    uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [UCRSATENDST]. Por favor reinicie o Software.", "OK", "", 16)
    RETURN .F.
 ENDIF
 TEXT TO lcsql TEXTMERGE NOSHOW
		select  codigo from  cptpla(nolock) WHERE combinado  = 1
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsPlanosCombinados", lcsql)
    uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [uCrsPlanosCombinados]. Por favor reinicie o Software.", "OK", "", 16)
 ENDIF
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_reservas_regularizarAdiantamento '000'
 ENDTEXT
 uf_gerais_actgrelha("", "uCrsValeRes", lcsql)
 IF  .NOT. USED("docs_reserva")
    CREATE CURSOR docs_reserva (stampres C(30), stampdoc C(30), tabledoc C(10), tipodoc C(50))
 ELSE
    DELETE FROM docs_reserva
 ENDIF
 IF USED("uCrsVale")
    fecha("uCrsVale")
 ENDIF
 IF USED("uCrsVale2")
    fecha("uCrsVale2")
 ENDIF
 IF USED("uCrsValeAuxMR")
    fecha("uCrsValeAuxMR")
 ENDIF
 IF USED("uCrsNrReserva")
    fecha("uCrsNrReserva")
 ENDIF
 IF USED("uCrsDEMndisp")
    SELECT ucrsdemndisp
    DELETE ALL
 ENDIF
 IF TYPE("atendimento")<>"U"
    uf_atendimento_infototais()
 ENDIF
 IF  .NOT. USED("uCrsFiReservas")
    TEXT TO lcsql TEXTMERGE NOSHOW
			select 
				convert(bit,0) as manipulado
				,convert(varchar(3),'') as tipoR
				,fi.id_Dispensa_Eletronica_D as id_validacaoDem
				,convert(varchar(20),'') as receita
				,convert(varchar(40),'') as token
				,convert(bit,0) as dem
				,0 as qtDem
				,convert(varchar(250),design) as design
				,convert(bit,0) as usalote
				,convert(bit,0) as alertalote
				,convert(bit,0) as compSNS
				,convert(varchar(8),'') as CNPEM
				,0.000 as PvpTop4
				,0 as qttrobot
				,convert(varchar(200),'') as obs
				,convert(varchar(20),'') as tpres
				, fistamp, ref, design, qtt, etiliquido, iva, ivaincl, tabiva, armazem, lordem, epv, ecusto, epvori, u_epvp, u_ettent1, u_ettent2, u_txcomp 
				, comp_tipo, comp, comp_diploma, comp_2, comp_diploma_2, comp_tipo_2, CONVERT(varchar(200), '') as obsCl, CONVERT(varchar(200), '') as obsInt
            ,CAST(0 as bit) as forceCalcCompart
			from 
				FI (nolock) 
				left join fi_comp (nolock) on fi.fistamp = fi_comp.id_fi
			WHERE 
				0=1
    ENDTEXT
    uf_gerais_actgrelha("", "uCrsFiReservas", lcsql)
 ELSE
    SELECT ucrsfireservas
    DELETE ALL
 ENDIF
ENDFUNC
**
PROCEDURE uf_atendimento_infototais
 LOCAL lctotalqtt, lctotalutente, lctotaldoc, lctotalutente, lnsaldo
 STORE 0 TO lctotalqtt, lctotalutente, lctotaldoc, lctotalutente, lnsaldo
 SELECT fi
 CALCULATE SUM(fi.u_epvp*qtt), SUM(fi.qtt) TO lctotaldoc, lctotalqtt FOR LEFT(fi.design, 1)<>"." .AND.  .NOT. DELETED()
 SELECT fi
 CALCULATE SUM(fi.etiliquido) TO lctotalutente FOR (fi.partes<>1 .AND. fi.marcada<>.T.) .OR. (fi.partes=0 .AND. fi.marcada=.T.)
 IF USED("uCrsVale")
    LOCAL lcvalepos
    STORE 0 TO lcvalepos
    IF VARTYPE(fechares)<>"U"
       IF fechares=.T.
          SELECT ucrsvale
          UPDATE uCrsVale FROM uCrsVale INNER JOIN fi ON ALLTRIM(ucrsvale.fistamp)==ALLTRIM(fi.ofistamp) SET etiliquido = fi.etiliquido, etiliquido2 = fi.etiliquido, epv = fi.epv
       ENDIF
    ENDIF
    SELECT ucrsvale
    lcvalepos = RECNO()
    GOTO TOP
    SCAN
       IF (ucrsvale.lancacc<>.T.)
          lnsaldo = lnsaldo+ucrsvale.etiliquido
       ELSE
          IF (ucrsvale.lancacc==.T.) .AND. (ucrsvale.fmarcada)
             IF ucrsvale.etiliquido=ucrsvale.eaquisicao .OR. (ucrsvale.qtt<=ucrsvale.eaquisicao)
                lnsaldo = lnsaldo+ucrsvale.etiliquido
             ELSE
                lnsaldo = lnsaldo+(ucrsvale.eaquisicao*ucrsvale.etiliquido/ucrsvale.qtt)
             ENDIF
          ENDIF
       ENDIF
    ENDSCAN
    SELECT ucrsvale
    TRY
       GOTO lcvalepos
    CATCH
    ENDTRY
 ENDIF

 IF TYPE("ATENDIMENTO")<>"U"
    WITH atendimento
       .txttotalqtt.value = lctotalqtt
       .txttotalutente.value = ROUND(lctotalutente-lnsaldo, 2)
       .txttotaldoc.value = lctotaldoc
    ENDWITH
   
 ENDIF
ENDPROC


**
FUNCTION uf_atendimento_infototaiscab
 LOCAL lcpos, lctotalqtt, lctotalutente, lctotalcomp, lcvalpos
 STORE 0 TO lcpos, lctotalqtt, lctotalutente, lctotalcomp
 STORE .F. TO lcvalpos
 
 myChamaSinave = 'N�o chama'
 myNrRecSinave = ''
 
 SELECT fi
 IF LEFT(fi.design, 1)<>"."
    RETURN .F.
 ENDIF
 WITH atendimento.pgfdados.page3
    .txttotalqtt.value = 0
    .txttotalutente.value = 0
    .txttotalcomp.value = 0
 ENDWITH
 SELECT fi
 lcpos = RECNO("fi")
 SCAN
    IF  .NOT. lcvalpos
       TRY
          GOTO lcpos+1
       CATCH
          GOTO BOTTOM
       ENDTRY
       lcvalpos = .T.
    ENDIF
    IF LEFT(fi.design, 1)=="."
       EXIT
    ENDIF
    

    
    WITH atendimento.pgfdados.page3
       .txttotalqtt.value = .txttotalqtt.value+fi.qtt
       .txttotalutente.value = .txttotalutente.value + fi.etiliquido
       .txttotalcomp.value = .txttotalcomp.value+fi.u_ettent1+fi.u_ettent2
    ENDWITH
    
    **Atendimento.refresh()
    

 ENDSCAN
 SELECT fi
 TRY
    GOTO lcpos
 CATCH
 ENDTRY
ENDFUNC
**
FUNCTION uf_atendimento_valoresdefeito
 IF TYPE("Atendimento")=="U"
    RETURN .F.
 ENDIF
 IF (USED("ucrsDemEfetivada"))
    fecha("ucrsDemEfetivada")
 ENDIF
 IF USED("ucrsvalecurini")
    fecha("ucrsvalecurini")
 ENDIF
 IF (USED("uCrsPrecosValidos"))
    fecha("uCrsPrecosValidos")
 ENDIF
 IF USED("uCrsPlanosDemAutomaticos")
    fecha("uCrsPlanosDemAutomaticos")
 ENDIF
 fecha("uc_rnuRecms")
 fecha("uCrsInteraccoes")
 fecha("uCrsProdInteraccoes")
 IF USED("docs_reserva")
    IF RECCOUNT("docs_reserva")>0
       LOCAL lcstampres
       lcstampres = ''
       SELECT docs_reserva
       GOTO TOP
       SCAN
          IF ALLTRIM(docs_reserva.tipodoc)='Reserva'
             lcstampres = ALLTRIM(docs_reserva.stampdoc)
          ENDIF
       ENDSCAN
       SELECT docs_reserva
       GOTO TOP
       SCAN
          IF ALLTRIM(docs_reserva.tabledoc)='FT'
             TEXT TO lcsql TEXTMERGE NOSHOW
						UPDATE ft2 SET stampreserva = '<<ALLTRIM(lcStampRes)>>' WHERE ft2stamp='<<ALLTRIM(docs_reserva.stampdoc)>>'
             ENDTEXT
             IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
                uf_perguntalt_chama("OCORREU UM ERRO A ATUALIZAR O STAMP DA RESERVA NOS DOCUMENTOS! POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
             ENDIF
          ENDIF
          SELECT docs_reserva
       ENDSCAN
    ENDIF
 ENDIF
 atendimento.pgfdados.page3.txtEfr_descr.value = ""
 atendimento.txtscanner.value = ""
 atendimento.txttotalvendas.value = 0
 atendimento.pgfdados.page1.txtiddadquirente.value = 0
 atendimento.pgfdados.page3.txtncartaocompart.disabledbackcolor = RGB(255, 255, 255)
 atendimento.lbldev.caption = "Devolu��o"
 LOCAL lcstamp
 lcstamp = uf_gerais_stamp()
 IF uf_gerais_getparameter("ADM0000000142", "BOOL") .AND. TYPE("PAGAMENTO")=="U"
    SELECT fi
    LOCATE FOR ALLTRIM(fi.ref)=="R000001" .AND. ALLTRIM(fi.ofistamp)==""
    IF FOUND()
       uf_perguntalt_chama("Existe um Adiantamento de Reserva no Atendimento."+CHR(13)+"O Atendimento n�o pode ser limpo.", "OK", "", 48)
       RETURN .F.
    ENDIF
 ENDIF
 uf_atendimento_criacursores()
 PUBLIC mycxuser
 PUBLIC naoimprimetalao, mypublicintvar, myvirtualvar, myvirtualtext, mydiplomaauto, myecra, myvaledescval, myremovecompart
 myvaledescval = .F.
 usoureserv = .F.
 stamplindem = ''
 myremovecompart = .F.
 fechares = .F.
 SELECT ucrsatendst
 APPEND BLANK
 SELECT ft
 APPEND BLANK
 REPLACE ft.ftstamp WITH astr(lcstamp), ft.pais WITH 1, ft.moeda WITH 'EURO', ft.tipo WITH 'Dinheiro', ft.memissao WITH uf_gerais_getparameter("ADM0000000260", "text"), ft.cambio WITH 1, ft.descar WITH '� definido nos par�metros', ft.carga WITH '� definido nos par�metros', ft.ndoc WITH IIF(TYPE("myVd")=="N", myvd, VAL(myvd)), ft.tipodoc WITH 1, ft.nmdoc WITH astr(myvdnm), ft.cobrado WITH .F., ft.vendedor WITH IIF(TYPE("ch_vendedor")=="N", ch_vendedor, VAL(ch_vendedor)), ft.vendnm WITH ALLTRIM(ch_vendnm)
 REPLACE ft.site WITH mysite, ft.pnome WITH myterm, ft.pno WITH mytermno, ft.cxstamp WITH mycxstamp, ft.cxusername WITH mycxuser, ft.ssstamp WITH myssstamp, ft.ssusername WITH ch_vendnm
 REPLACE ft.ivatx1 WITH uf_gerais_gettabelaiva(1, "TAXA"), ft.ivatx2 WITH uf_gerais_gettabelaiva(2, "TAXA"), ft.ivatx3 WITH uf_gerais_gettabelaiva(3, "TAXA"), ft.ivatx4 WITH uf_gerais_gettabelaiva(4, "TAXA"), ft.ivatx5 WITH uf_gerais_gettabelaiva(5, "TAXA"), ft.ivatx6 WITH uf_gerais_gettabelaiva(6, "TAXA"), ft.ivatx7 WITH uf_gerais_gettabelaiva(7, "TAXA"), ft.ivatx8 WITH uf_gerais_gettabelaiva(8, "TAXA"), ft.ivatx9 WITH uf_gerais_gettabelaiva(9, "TAXA"), ft.ivatx10 WITH uf_gerais_gettabelaiva(10, "TAXA"), ft.ivatx11 WITH uf_gerais_gettabelaiva(11, "TAXA"), ft.ivatx12 WITH uf_gerais_gettabelaiva(12, "TAXA"), ft.ivatx13 WITH uf_gerais_gettabelaiva(13, "TAXA"), ft.id_tesouraria_conta WITH 99
 SELECT ft2
 APPEND BLANK
 REPLACE ft2.ft2stamp WITH ft.ftstamp
 uf_atendimento_selcliente(200, 0, "ATENDIMENTO")
 STORE '' TO nratendimento
 STORE '' TO tokencompart
 STORE .F. TO compsemsimul
 uf_atendimento_geranratendimento()
 uf_atendimento_novavenda(.T.)
 SELECT fi
 INDEX ON fi.lordem TAG lordem
 atendimento.txttotalvendas.value = 1
 atendimento.pgfdados.page3.btnvia.label1.caption = "Normal"
 IF atendimento.menu1.conferir.tag=="true"
    atendimento.menu1.conferir.tag = "false"
    atendimento.menu1.conferir.img.picture = mypath+"\imagens\icons\unchecked_w.png"
 ENDIF
 fecha("TD")
 fecha("FIinsercaoReceita")
 IF (USED("uCrsVerificaRespostaDEM"))
    fecha("uCrsVerificaRespostaDEM")
 ENDIF
 IF (USED("uCrsVerificaRespostaDEMTotal"))
    fecha("uCrsVerificaRespostaDEMTotal")
 ENDIF
 
 IF (USED("ucrsSNSExternoAtend"))
    fecha("ucrsSNSExternoAtend")
 ENDIF 
 
 IF TYPE("myDemDispensada")<>"U"
    RELEASE mydemdispensada
 ENDIF
 DELETE FROM fi_nnordisk
 IF USED("uCrsCabVendas")
    fecha("uCrsCabVendas")
 ENDIF
 SELECT design, .F. AS sel, .F. AS sel2, .F. AS receita, .F. AS modopag, etiliquido, etiliquido AS etreg, etiliquido AS valorpagar, u_nbenef, u_nbenef2, lordem, .F. AS saved, fistamp AS stamp, ofistamp, tkhposlstamp AS u_ltstamp, partes, .F. AS fact, .F. AS factdev, .F. AS factpago, rvpstamp AS entpresc, szzstamp AS nopresc, rdata AS datareceita, ref AS codacesso, ref AS coddiropcao, CAST('' AS C(40) ) AS token, CAST('' AS C(35) ) AS cartao, .F. AS temadiantamento, 0.00  AS eacerto, .F. AS isencaoiva, CAST('' AS C(4) ) AS receita_tipo, .F. AS planocomplem, .F. AS mantemreceita, rdata AS datadispensa, bostamp, .F. AS nifgenerico, SPACE(200) as obsCl, SPACE(200) as obsInt, .F. as oferta FROM fi WHERE 1=0 INTO CURSOR uCrsCabVendas READWRITE
 linpsico = .F.
 uf_psico_campos()
 IF uf_gerais_getparameter("ADM0000000267", "BOOL")
    atendimento.menu1.estado("", "", "Conferir", .T., "Sair", .T.)
    atendimento.menu1.gravar.img1.picture = mypath+"\imagens\icons\unchecked_w.png"
    atendimento.menu1.sair.image1.picture = mypath+"\imagens\icons\sair_w.png"
 ELSE
    atendimento.menu1.sair.image1.picture = mypath+"\imagens\icons\sair_w.png"
    atendimento.menu1.estado("", "", "Terminar", .T., "Sair", .T.)
 ENDIF
 tpsair = 1
 IF  .NOT. (TYPE("PESQUTENTES")=="U")
    uf_pesqutentes_sair()
 ENDIF
 IF  .NOT. (TYPE("excecaoMed")=="U")
    uf_excecaomed_sair()
 ENDIF
 **IF TYPE("UtilPlafond")<>"U"
 	**utilplafond = .F.
 **ENDIF
 stampcompart = ''

 IF uf_gerais_getParameter_site("ADM0000000167", "BOOL", mySite)

	uv_plano = uf_gerais_getParameter_site("ADM0000000167", "TEXT", mySite)

	IF !EMPTY(uv_plano)
		uf_atendimento_aplicacompart(uv_plano, .F.)
	ENDIF

 ENDIF

 atendimento.refresh
 
 **forca producto
 **uf_atendimento_forcaProductoAtend('1888889')
 
ENDFUNC

	

PROCEDURE uf_atendimento_forcaProductoAtend
	LPARAMETERS lcRef
	atendimento.txtscanner.value = ALLTRIM(lcRef)
	uf_atendimento_scanner()
ENDPROC



**
PROCEDURE uf_atendimento_geranratendimento
**   LOCAL lcdataaux, lctimeaux, lcrandaux

**   STORE '' TO lcdataaux, lctimeaux, lcrandaux
**   nratendimento = ''
**
**   lcanoaux = LEFT(DTOC(DATETIME()), 2)
**   lcdataaux = DATE()
**   lcdataaux = ALLTRIM(STR(lcdataaux-DATE(YEAR(lcdataaux)-1, 12, 31)))
**
**   IF LEN(lcdataaux)=2
**      lcdataaux = '0'+lcdataaux
**   ELSE
**      IF LEN(lcdataaux)=1
**         lcdataaux = '00'+lcdataaux
**      ENDIF
**   ENDIF
**
**   lctimeaux = STRTRAN(ASTR(SECONDS(),5),'.','')
**
**   uv_return = ALLTRIM(lcanoaux) + ALLTRIM(lcdataaux) + ALLTRIM(lctimeaux)
**
**   lcrandaux = uf_gerais_randomString(1, (14 - LEN(ALLTRIM(uv_return))))

**   nratendimento = ALLTRIM(uv_return) + ALLTRIM(lcrandaux)


   TEXT TO uv_sql TEXTMERGE NOSHOW
      exec up_vendas_geraNrAtend <<myTermNo>>, <<mySite_nr>>, '<<ALLTRIM(m_chinis)>>'
   ENDTEXT

   uf_gerais_actGrelha("", "uc_newNrAtend", uv_sql)

   SELECT uc_newNrAtend

   nratendimento = ALLTRIM(uc_newNrAtend.newNrAtend)

   FECHA("uc_newNrAtend")

ENDPROC

FUNCTION uf_atendimento_geranratendimento_return
**   LOCAL lcdataaux, lctimeaux, lcrandaux
**
**   STORE '' TO lcdataaux, lctimeaux, lcrandaux
**
**   lcanoaux = LEFT(DTOC(DATETIME()), 2)
**   lcdataaux = DATE()
**   lcdataaux = ALLTRIM(STR(lcdataaux-DATE(YEAR(lcdataaux)-1, 12, 31)))
**
**   IF LEN(lcdataaux)=2
**      lcdataaux = '0'+lcdataaux
**   ELSE
**      IF LEN(lcdataaux)=1
**         lcdataaux = '00'+lcdataaux
**      ENDIF
**   ENDIF
**
**   lctimeaux = STRTRAN(ASTR(SECONDS(),5),'.','')
**
**   uv_return = ALLTRIM(lcanoaux) + ALLTRIM(lcdataaux) + ALLTRIM(lctimeaux)
**
**   lcrandaux = uf_gerais_randomString(1, (14 - LEN(ALLTRIM(uv_return))))

   LOCAL uv_newNrAtend

   TEXT TO uv_sql TEXTMERGE NOSHOW
      exec up_vendas_geraNrAtend <<myTermNo>>, <<mySite_nr>>, '<<ALLTRIM(m_chinis)>>'
   ENDTEXT

   uf_gerais_actGrelha("", "uc_newNrAtend", uv_sql)

   SELECT uc_newNrAtend

   uv_newNrAtend = ALLTRIM(uc_newNrAtend.newNrAtend)

   FECHA("uc_newNrAtend")

   RETURN uv_newNrAtend
ENDFUNC
**
PROCEDURE uf_atendimento_initsenhas
 SELECT ucrse1
 IF ucrse1.has_ticket_integration==.F. .OR. VARTYPE(my_xopvision_term)=="U"
    atendimento.txtnome.width = atendimento.shape3.width-10
    atendimento.btncallnext.enabled = .F.
    atendimento.txtpending.enabled = .F.
    atendimento.image1.enabled = .F.
    atendimento.btncallnext.visible = .F.
    atendimento.image1.visible = .F.
    atendimento.txtpending.visible = .F.
    atendimento.txtpending.value = "-"
    atendimento.btncallnext.width = 0
    atendimento.txtpending.width = 0
 ENDIF
ENDPROC
**
FUNCTION uf_ticket_call_next
 LOCAL lcmethod, lctoken, lclocal, lcclid, lcinfoerro, lcvalida, lcsiteloja
 STORE '' TO lcinfoerro
 IF (ucrse1.senhamotivosematendimento==.T.)
    IF  .NOT. uf_ticket_check_last_service()
       RETURN .F.
    ENDIF
 ENDIF
 SELECT ucrse1
 lcclid = ALLTRIM(ucrse1.id_lt)
 sitenr = ucrse1.siteno
 lcsiteloja = ucrse1.siteloja
 atendimento.txtpending.enabled = .F.
 atendimento.btncallnext.enabled = .F.
 atendimento.image1.enabled = .F.
 regua(1, 2, "A chamar pr�ximo utente. Por favor aguarde.")
 atendimento.txtpending.value = ""
 atendimento.txtpending.comment = ""
 uf_chilkat_send_request_callnext_ticket(astr(my_xopvision_term), m_chinis, lcsiteloja, astr(ch_userno), astr(sitenr))
 SELECT ucrscallnext
 GOTO TOP
 IF (RECCOUNT("uCrsCallNext")>0 .AND. ucrscallnext.status==200)
    IF (ucrscallnext.total_counter)>0
       lcinforemaining = "Existem "+ALLTRIM(STR(ucrscallnext.total_counter))+" senhas em espera."
       lcinfomessage = "Foi chamada a senha "+ALLTRIM(ucrscallnext.serie)+" "+ALLTRIM(STR(ucrscallnext.numerator_counter))+"."
    ELSE
       lcinforemaining = "Atualmente n�o existem mais senhas em espera."
       lcinfomessage = "N�o existem mais senhas em espera."
    ENDIF
    IF  .NOT. EMPTY(tokenpausa)
       lcsql = ''
       TEXT TO lcsql TEXTMERGE NOSHOW
					UPDATE ext_xopvision_breakmotive set duration = datediff(mi, start_date, getdate()) WHERE token='<<ALLTRIM(TokenPausa)>>'
       ENDTEXT
       uf_gerais_actgrelha("", "", lcsql)
       tokenpausa = ""
    ENDIF
    atendimento.txtpending.value = ALLTRIM(ucrscallnext.serie)+""+ALLTRIM(STR(ucrscallnext.numerator_counter))
    atendimento.txtpending.comment = ALLTRIM(ucrscallnext.token)
    atendimento.txtpending.enabled = .T.
    atendimento.refresh
    uf_filaespera_chama()
    regua(2)
 ELSE
    IF (RECCOUNT("uCrsCallNext")==0)
       uf_perguntalt_chama("N�o foi possivel validar motivo do erro. Por favor contacte o suporte.", "OK", "", 32)
       regua(2)
       RETURN .F.
    ELSE
       SELECT ucrscallnext
       GOTO TOP
       IF  .NOT. EMPTY(ucrscallnext.message)
          lcinfoerro = ALLTRIM(ucrscallnext.message)
       ELSE
          lcinfoerro = 'O servi�o de senhas n�o retornou informa��o. Por favor contacte o Suporte. Obrigado.'
       ENDIF
       uf_perguntalt_chama(LEFT(lcinfoerro, 200), "OK", "", 48)
       regua(2)
    ENDIF
 ENDIF
 atendimento.btncallnext.enabled = .T.
 atendimento.image1.enabled = .T.
 IF USED("uCrsInfoAux")
    fecha("uCrsInfoAux")
 ENDIF
 IF USED("uCrsCallNext")
    fecha("uCrsCallNext")
 ENDIF
 IF USED("uCrsInfoAuxErro")
    fecha("uCrsInfoAuxErro")
 ENDIF
ENDFUNC
**
FUNCTION uf_ticket_check_last_service
 LOCAL lcsql, lcsqlaux
 STORE '' TO lcsql
 IF  .NOT. EMPTY(atendimento.txtpending.comment)
    TEXT TO lcsql TEXTMERGE NOSHOW
			SELECT 
				serie, 
				numerator_counter, 
				nrAtend
			FROM 
				ext_xopvision_queue_management (nolock) 
			where 
				token = '<<Atendimento.txtPending.comment>>'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "uCrsInfo", lcsql)
       uf_perguntalt_chama("N�o foi poss�vel concluir a opera��o. Por favor contacte o suporte.", "OK", "", 32)
       regua(2)
       RETURN .F.
    ELSE
       IF ISNULL(ucrsinfo.nratend)
          lcsqlaux = ''
          TEXT TO lcsqlaux TEXTMERGE NOSHOW
					SELECT 
						id, 
						type, 
						description
					FROM 
						ext_ticket_occurency (nolock) 
					where 
						type = 'salta senha'
          ENDTEXT
          IF  .NOT. uf_gerais_actgrelha("", "uCrsMotives", lcsqlaux)
             uf_perguntalt_chama("N�o foi poss�vel concluir a opera��o. Por favor contacte o suporte.", "OK", "", 32)
             regua(2)
             RETURN .F.
          ELSE
             IF (RECCOUNT("uCrsMotives")==0)
                RETURN .F.
             ENDIF
             IF  .NOT. ISNULL("uCrsMotives")
                uf_perguntalt_chama("N�o concluiu o atendimento associado � senha "+ALLTRIM(ucrsinfo.serie)+""+ALLTRIM(STR(ucrsinfo.numerator_counter))+" ."+CHR(13)+"Selecione um motivo.", "OK", "", 48)
                lcmotivoid = ''
                uf_valorescombo_chama("lcMotivoID", 0, "uCrsMotives", 2, "id", "description", .F., .F., .T., .T., .F., .F.)
                IF EMPTY(lcmotivoid)
                   RETURN .F.
                ENDIF
                atendimento.txtpending.comment = ''
                IF  .NOT. (uf_ticket_set_motive(lcmotivoid))
                   RETURN .F.
                ENDIF
                uf_ticket_call_next()
             ENDIF
          ENDIF
       ELSE
          RETURN .T.
       ENDIF
    ENDIF
    IF USED("uCrsInfo")
       fecha("uCrsInfo")
    ENDIF
    IF USED("uCrsMotives")
       fecha("uCrsMotives")
    ENDIF
 ELSE
    RETURN
 ENDIF
 RETURN .F.
ENDFUNC
**
FUNCTION uf_ticket_set_motive
 LPARAMETERS motive_id
 LOCAL lcsql, lctoken
 STORE '' TO lcsql
 SELECT ucrse1
 sitenr = ucrse1.siteno
 lctoken = uf_gerais_stamp()
 TEXT TO lcsql TEXTMERGE NOSHOW
		INSERT into ext_xopvision_queue_management
			(token, start_date, site_nr, ext_ticket_occurency_id, userno, tstamp )  
		values
			('<<ALLTRIM(lcToken)>>', dateadd(HOUR, <<difhoraria>>, getdate()), <<astr(siteNr)>>, <<motive_id>>, <<ch_userno>>, (select tstamp from b_terminal (nolock) where no= <<myTermNo>>))

 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
    uf_perguntalt_chama("OCORREU UM PROBLEMA A GUARDAR REGISTO.", "OK", "", 16)
    RETURN .F.
 ENDIF
 RETURN .T.
ENDFUNC
**
FUNCTION uf_atendimento_ativar_pausa
 LOCAL lcmethod, lctoken, lcclid
 IF USED("uCrsMotives")
    fecha("uCrsMotives")
 ENDIF
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		SELECT id, type, description as Motivo, motive_id FROM ext_ticket_occurency (nolock) WHERE type = 'pausa'
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsMotives", lcsql)
    uf_perguntalt_chama("N�o foi poss�vel obter os dados. Por favor contacte o suporte.", "OK", "", 32)
    regua(2)
    RETURN .F.
 ELSE
    IF  .NOT. ISNULL("uCrsMotives")
       lcmotivoid = ''
       uf_valorescombo_chama("lcMotivoID", 0, "uCrsMotives", 2, "id", "Motivo", .F., .F., .T., .T., .F., .F.)
       IF EMPTY(lcmotivoid)
          RETURN .F.
       ENDIF
       IF  .NOT. (uf_ticket_set_motive(lcmotivoid))
          RETURN .F.
       ELSE
          uf_update_pause_motive_in_ext_service(lcmotivoid)
       ENDIF
    ENDIF
    regua(2)
 ENDIF
 IF USED("uCrsMotives")
    fecha("uCrsMotives")
 ENDIF
ENDFUNC
**
PROCEDURE uf_update_pause_motive_in_ext_service
 LPARAMETERS lcmotiveid
 LOCAL lcmethod, lctoken, lcclid, lcsiteloja
 IF USED("uCrsMotives")
    fecha("uCrsMotives")
 ENDIF
 SELECT ucrse1
 sitenr = ucrse1.siteno
 lcsiteloja = ucrse1.siteloja
 lcclid = ALLTRIM(ucrse1.id_lt)
 lctoken = uf_gerais_stamp()
 regua(1, 2, "A carregar dados. Por favor aguarde.")
 uf_chilkat_send_request_setmotive_ticket(astr(my_xopvision_term), astr(lcmotiveid), m_chinis, lcsiteloja)
 regua(2)
 SELECT ucrssetmotive
 GOTO TOP
 IF EMPTY(ucrssetmotive.message)
    uf_perguntalt_chama("N�o foi poss�vel obter os dados. Por favor contacte o suporte.", "OK", "", 32)
 ELSE
    uf_perguntalt_chama(ucrssetmotive.message, "OK", "", 64)
    TEXT TO lcsql TEXTMERGE NOSHOW
			insert into ext_xopvision_breakmotive (token, start_date, ousrinis, sitenr, userno, terminal, motiveid)
			values ('<<ALLTRIM(uCrsSetmotive.token)>>', dateadd(HOUR, <<difhoraria>>, getdate()) , '<<ALLTRIM(m_chinis)>>', <<siteNr>>, <<ch_userno>>, <<my_xopvision_term>>, <<lcMotiveID>>)
    ENDTEXT
    uf_gerais_actgrelha("", "", lcsql)
    tokenpausa = ALLTRIM(ucrssetmotive.token)
    &&_CLIPTEXT = ucrssetmotive.token
 ENDIF
 IF USED("uCrsSetmotive")
    fecha("uCrsSetmotive")
 ENDIF
ENDPROC
**
FUNCTION uf_atendimento_limpademtotal
 LOCAL lcposfi, lcnrreceita
 IF ( .NOT. USED("fi"))
    RETURN .F.
 ENDIF
 IF ( .NOT. USED("uCrsVerificaRespostaDEMTotal"))
    RETURN .F.
 ENDIF
 lcposfi = RECNO("fi")
 SELECT DISTINCT receita_nr FROM uCrsVerificaRespostaDEMTotal INTO CURSOR uCrsVerificaRespostaDEMTotalAux READWRITE
 SELECT ucrsverificarespostademtotalaux
 GOTO TOP
 SCAN
    SELECT fi
    GOTO TOP
    LOCATE FOR STREXTRACT(fi.design, ':', ' -', 1, 0)==ALLTRIM(ucrsverificarespostademtotalaux.receita_nr) .AND. LEFT(fi.design, 1)=="."
    IF  .NOT. FOUND()
       DELETE FROM uCrsVerificaRespostaDEMTotal WHERE ucrsverificarespostademtotal.receita_nr=ALLTRIM(ucrsverificarespostademtotalaux.receita_nr)
    ENDIF
    SELECT ucrsverificarespostademtotalaux
 ENDSCAN
 IF (USED("uCrsVerificaRespostaDEMTotalAux"))
    fecha("uCrsVerificaRespostaDEMTotalAux ")
 ENDIF
 SELECT fi
 TRY
    GOTO lcposfi
 CATCH
    GOTO TOP
 ENDTRY
ENDFUNC
**
PROCEDURE uf_atendimento_marcademtotal
 LPARAMETERS lcdem, lcdesign, lcnumreceita, lcdemvalidacao, lcdemestadofinal, lcapagadem
 
 IF (lcdem==.T. .AND.  .NOT. (LEFT(lcdesign, 1)==".") .AND. USED("uCrsVerificaRespostaDEMTotal"))
    IF (lcapagadem=.F.)
       UPDATE uCrsVerificaRespostaDEMTotal SET sel = lcdemestadofinal WHERE  uf_gerais_compStr(receita_nr,lcnumreceita) .AND.  uf_gerais_compStr(id,lcdemvalidacao)
    ELSE
       DELETE FROM uCrsVerificaRespostaDEMTotal WHERE   uf_gerais_compStr(receita_nr,lcnumreceita)
    ENDIF
 ENDIF

ENDPROC
**
PROCEDURE uf_ticket_recall
 LOCAL lcmethod, lctoken, lclocal, lcclid, lcinfoerro, lcvalida, lcsiteloja
 STORE '' TO lcinfoerro
 SELECT ucrse1
 lcclid = ALLTRIM(ucrse1.id_lt)
 lcsiteloja = ucrse1.siteloja
 sitenr = ucrse1.siteno
 regua(1, 2, "A rechamar senha. Por favor aguarde.")
 uf_chilkat_send_request_recall_ticket(astr(my_xopvision_term), m_chinis, lcsiteloja)
 regua(2)
ENDPROC
**
PROCEDURE uf_ticket_details
 LOCAL lctoken, lcinfomessage, lcrecall
 STORE '' TO lcinfomessage
 uf_filaespera_chama()
ENDPROC
**
PROCEDURE uf_ticket_update_nratend
 LPARAMETERS nratendimento
 
 IF TYPE("ATENDIMENTO") <> "O"
 	RETURN .F.
 ENDIF
 
 LOCAL lctoken, lcsql 
 lctoken = atendimento.txtpending.comment
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		UPDATE ext_xopvision_queue_management SET nrAtend = '<<nrAtendimento>>', end_date = getdate() WHERE token = '<<ALLTRIM(lcToken)>>'
 ENDTEXT
 IF  .NOT. (uf_gerais_actgrelha("", "", lcsql))
    uf_perguntalt_chama("Ocorreu uma anomalia a mapear o ticket com o atendimento. Por favor contacte o Suporte.", "OK", "", 16)
 ENDIF
 
ENDPROC
**
FUNCTION uf_atendimento_pesquisarst
 IF uf_atendimento_validacampanhas()==.F.
    RETURN .F.
 ENDIF
 uf_chk_nova_versao()
 TRY
    KEYBOARD '{TAB}'
 CATCH
 ENDTRY
 atendimento.tmroperador.reset()
 uf_pesqstocks_chama("ATENDIMENTO")
ENDFUNC
**
FUNCTION uf_atendimento_novavenda
 LPARAMETERS lcbool
 IF uf_atendimento_validacampanhas()==.F.
    RETURN .F.
 ENDIF
 atendimento.tmroperador.reset()

 LOCAL lcnrvendas, lccod, lccodacesso
 STORE 0 TO lcnrvendas
 STORE "" TO lccod, lccodacesso
 uf_atendimento_adicionalinhafi(.F., .T.)
 SELECT fi
 GOTO BOTTOM
 REPLACE fi.design WITH '.SEM RECEITA'
 REPLACE fi.tipor WITH 'SR'
 IF  .NOT. EMPTY(atendimento.txttotalvendas.value)
    atendimento.txttotalvendas.value = atendimento.txttotalvendas.value+1
 ELSE
    SELECT fi
    GOTO TOP
    SCAN FOR LEFT(fi.design, 1)=="."
       lcnrvendas = lcnrvendas+1
    ENDSCAN
    atendimento.txttotalvendas.value = lcnrvendas
 ENDIF
 IF lcbool
    IF mycopyplano
       SELECT fi
       GOTO BOTTOM
       TRY
          SKIP -1
       CATCH
       ENDTRY
       lccompart = uf_atendimento_verificacompart()
       SELECT fi
       GOTO BOTTOM
       uf_atendimento_aplicacompart(lccompart, .F.)
       IF TYPE("pesqStocks")<>"U"
          pesqstocks.show
       ENDIF
    ENDIF
    uf_atendimento_dadoscab()
    IF USED("uCrsAtendComp")
       SELECT ucrsatendcomp
       IF  .NOT. EMPTY(ucrsatendcomp.codacesso)
          lccodacesso = ALLTRIM(ucrsatendcomp.codacesso)
       ENDIF
    ENDIF
 ENDIF
 atendimento.grdatend.refresh()
 IF TYPE("pesqstocks")=="U"
    SELECT fi
    TRY
       SKIP -1
    CATCH
    ENDTRY
    atendimento.grdatend.setfocus()
    TRY
       SKIP
    CATCH
       GOTO TOP
    ENDTRY
    atendimento.grdatend.setfocus()
 ENDIF
 uf_atendimento_verificalordem()
 IF uf_gerais_getparameter("ADM0000000267", "BOOL")
    uf_atendimento_conferir_limpar()
 ELSE
    uf_atendimento_salvar_limpar()
 ENDIF
 SELECT FI
 GO BOTTOM
ENDFUNC
**
FUNCTION uf_atendimento_validasevalidacompartexterna
 IF ( .NOT. USED("ucrsAtendComp"))
    RETURN .T.
 ENDIF
 IF ( .NOT. USED("ucrsCabVendas"))
    RETURN .T.
 ENDIF
 
 
 
 SELECT ucrsatendcomp
 GOTO TOP
 SCAN
    IF (ucrsatendcomp.e_compart) AND !uf_gerais_savida_afp(ALLTRIM(ucrsatendcomp.cptorgabrev))
       SELECT fi
       GOTO TOP
       SCAN FOR fistamp=ALLTRIM(ucrsatendcomp.fistamp)
          SELECT ucrscabvendas
          GOTO TOP
          SCAN FOR ucrscabvendas.lordem=fi.lordem
             IF (EMPTY(ALLTRIM(ucrscabvendas.ofistamp)))
                RETURN .T.
             ELSE 
             	lcsql = ""
				 TEXT TO lcsql TEXTMERGE NOSHOW
						select u_comp from fi (nolock) where fistamp='<<ALLTRIM(ucrscabvendas.ofistamp)>>'
				 ENDTEXT
				 IF uf_gerais_actgrelha("", 'uCrsVerifCompLin', lcsql)
	                SELECT uCrsVerifCompLin
	                IF uCrsVerifCompLin.u_comp = .f.
	                	RETURN .T.
	                ENDIF 
	             ELSE
	                uf_perguntalt_chama("Ocorreu uma anomalia a verificar a comparticipa��o da linha.", "OK", "", 16)
	             ENDIF 
             ENDIF
             SELECT ucrscabvendas
          ENDSCAN
       ENDSCAN
       SELECT fi
    ENDIF
    SELECT ucrsatendcomp
 ENDSCAN
 RETURN .F.
ENDFUNC
**
PROCEDURE uf_atendimento_limpalinhafi
 IF (USED("fi"))
    LOCAL lcfistampaux
    lcfistampaux = ''
    SELECT fi
    lcfistampaux = ALLTRIM(fi.fistamp)
    DELETE FROM fi WHERE ALLTRIM(fi.fistamp)=lcfistampaux
 ENDIF
ENDPROC
**
FUNCTION uf_atendimento_actref
 LPARAMETERS tcmostrainfo, tcactqtt 
 IF TYPE("ATENDIMENTO")<>"U"
    atendimento.tmroperador.reset()
 ENDIF
 LOCAL lccodigo, lcsql, lcpos
 STORE "" TO lccodigo
 STORE 0 TO lcpos
 LOCAL lcpercadicseg
 lcpercadicseg = 0
 IF USED("uCrsAtendComp")
    SELECT ucrsatendcomp
    GOTO TOP
    IF ucrsatendcomp.percadic>0
       lcpercadicseg = ucrsatendcomp.percadic
    ENDIF
 ENDIF
 
 SELECT fi
 lccodigo = fi.ref
 lcsql = ""
 TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_touch_ActRef '<<ALLTRIM(lcCodigo)>>', <<myArmazem>>, <<mysite_nr>>
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", 'uCrsActRef', lcsql)
    uf_perguntalt_chama("Ocorreu uma anomalia a atualizar a refer�ncia.", "OK", "", 64)
    REPLACE fi.ref WITH '' IN fi
    RETURN .F.
 ELSE
    IF RECCOUNT("uCrsActRef")==0
       SELECT fi
       REPLACE fi.ref WITH ""
       uf_perguntalt_chama("A REFER�NCIA INTRODUZIDA N�O TEM FICHA DE PRODUTO CRIADA.", "OK", "", 64)
       IF(TYPE("ATENDIMENTO")<> "U")
           uf_atendimento_apagalinha(.T.)
       ENDIF

    ELSE
       SELECT ucrsactref
       IF ucrsactref.inactivo==.T.
     
          SELECT fi
          REPLACE fi.ref WITH '' IN fi
		  uf_perguntalt_chama("PRODUTO MARCADO COMO INATIVO. POR FAVOR VERIFIQUE A FICHA DO PRODUTO.", "OK", "", 64)
          IF(TYPE("ATENDIMENTO")<> "U")
             uf_atendimento_apagalinha(.T.)
          ENDIF

          RETURN .F.
       ENDIF
       IF uf_gerais_getparameter_site('ADM0000000060', 'BOOL', mysite)
          IF ucrsactref.qttminvd>ucrsactref.qttembal .OR. ucrsactref.qttminvd=0            
             SELECT fi
             REPLACE fi.ref WITH '' IN fi
			 uf_perguntalt_chama("O PRODUTO N�O PODE SER UTILIZADO NA VENDA. INFORMA��O INV�LIDA.", "OK", "", 64)
			 IF(TYPE("ATENDIMENTO")<> "U")
             	uf_atendimento_apagalinha(.T.)
          	 ENDIF

             RETURN .F.
          ENDIF
       ENDIF
       INSERT INTO UCRSATENDST SELECT * FROM uCrsActRef
       SELECT fi
       SELECT ucrsatendst
       GOTO BOTTOM
       REPLACE ucrsatendst.ststamp WITH fi.fistamp
       SELECT fi
       REPLACE fi.design WITH ucrsactref.design
       REPLACE fi.posologia WITH ucrsactref.u_posprog
       IF TYPE("fi.posologia_c") <> "U"
         IF USED("ucrsverificarespostadem")
            REPLACE fi.posologia_c WITH ucrsverificarespostadem.posologia
            REPLACE fi.posologia WITH ucrsverificarespostadem.posologia
         ELSE
            REPLACE fi.posologia_c WITH ucrsactref.u_posprog
            REPLACE fi.posologia WITH ucrsactref.u_posprog
         ENDIF
       ENDIF
       IF uf_gerais_getparameter_site('ADM0000000060', 'BOOL', mysite)
          IF fi.qtt=0
             REPLACE fi.qtt WITH ucrsactref.qttembal
          ENDIF
       ELSE
          REPLACE fi.qtt WITH 1
       ENDIF
       IF  .NOT. ucrsactref.qlook
          IF lcpercadicseg=0
             REPLACE fi.epv WITH ucrsactref.epv1, fi.u_epvp WITH ucrsactref.epv1, fi.epvori WITH ucrsactref.epv1, fi.etiliquido WITH ucrsactref.epv1
          ELSE
             REPLACE fi.epv WITH ROUND(ucrsactref.epv1*(1+(lcpercadicseg)), 2), fi.u_epvp WITH ROUND(ucrsactref.epv1*(1+(lcpercadicseg)), 2), fi.epvori WITH ROUND(ucrsactref.epv1*(1+(lcpercadicseg)), 2), fi.etiliquido WITH ROUND(ucrsactref.epv1*(1+(lcpercadicseg)), 2)
          ENDIF
       ELSE
          REPLACE fi.epromo WITH .T.
          REPLACE fi.u_epvp WITH ucrsactref.epv1
       ENDIF
       SELECT fi
       REPLACE tabiva WITH ucrsactref.tabiva, iva WITH 6.00 , ivaincl WITH ucrsactref.ivaincl, codigo WITH ucrsactref.codigo,; 
       cpoc WITH ucrsactref.cpoc, usr1 WITH ucrsactref.usr1, stipo WITH ucrsactref.stipo, familia WITH ucrsactref.familia, stns WITH ucrsactref.stns,;
       custo WITH ucrsactref.pcusto, ecusto WITH ucrsactref.epcult, pcp WITH ucrsactref.pcpond, epcp WITH ucrsactref.epcpond, unidade WITH ucrsactref.unidade, u_stock WITH ucrsactref.stock,;
       u_generico WITH ucrsactref.generico, u_psico WITH ucrsactref.psico, u_benzo WITH ucrsactref.benzo, u_genalt WITH ucrsactref.genalt, pvmoeda WITH 0, ftanoft WITH YEAR(DATE()),;
       lrecno WITH fi.fistamp, fi.usalote WITH ucrsactref.usalote, fi.amostra WITH .F., fi.u_epref WITH ucrsactref.pref,;
       fi.pic WITH ucrsactref.pvpdic, fi.pvpmaxre WITH ucrsactref.pvpmaxre, fi.opcao WITH ucrsactref.opcao,;
       fi.compsns WITH ucrsactref.compsns, fi.cnpem WITH ucrsactref.cnpem, fi.pvptop4 WITH ucrsactref.pvptop4
       IF TYPE("FACTURACAO")=="U"
          REPLACE fi.obs WITH ucrsactref.obs
       ENDIF
       IF TYPE("FACTURACAO")<>"U"
       ELSE
	   	  LOCAL uv_corPvp
          SELECT fi
          REPLACE fi.armazem WITH ucrsactref.armazem
          uv_corPvp = uf_atendimento_alteracorcelulapvp()
		  SELECT fi
          REPLACE fi.cor WITH uv_corPvp
       ENDIF
       IF TYPE("FACTURACAO")=="U"
          IF fi.u_psico .AND. linpsico=.F.
             linpsico = .T.
             uf_psico_campos()
          ENDIF
       ENDIF 
       
       SELECT FI
       
      replace fi.codmotiseimp WITH ALLTRIM(ucrsactref.codmotiseimp)
	   replace fi.motiseimp WITH ALLTRIM(ucrsactref.motiseimp)
	         
       SELECT fi
       DO CASE
          CASE fi.tabiva<99
             REPLACE fi.iva WITH uf_gerais_gettabelaiva(fi.tabiva, "TAXA")
          OTHERWISE
             REPLACE fi.iva WITH 0
       ENDCASE
       IF TYPE("ATENDIMENTO")<>"U"
          LOCAL lcexiste
          STORE .F. TO lcexiste
          STORE .F. TO lcexiste
          SELECT ucrsatenddci
          SCAN FOR UPPER(ALLTRIM(ucrsatenddci.ref))==UPPER(ALLTRIM(fi.ref))
             lcexiste = .T.
          ENDSCAN
          IF lcexiste=.F.
             IF uf_gerais_actgrelha("", 'uCrsDCITEMP', [exec up_touch_DCIp ']+fi.ref+['])
                IF RECCOUNT("uCrsDCITEMP")>0
                   SELECT * FROM uCrsAtendDCI UNION ALL SELECT * FROM uCrsDCITEMP INTO CURSOR uCrsAtendDCI READWRITE
                ENDIF
             ELSE
                uf_perguntalt_chama("Ocorreu uma anomalia a verificar o DCI.", "OK", "", 16)
             ENDIF
             IF USED("uCrsDCITEMP")
                fecha("uCrsDCITEMP")
             ENDIF
          ENDIF
          STORE .F. TO lcexiste
          SELECT ucrsatendmaisbaratos
          SCAN FOR UPPER(ALLTRIM(ucrsatendmaisbaratos.oref))==UPPER(ALLTRIM(fi.ref))
             lcexiste = .T.
          ENDSCAN
          IF lcexiste=.F.
             lcsql = ''
             TEXT TO lcsql TEXTMERGE NOSHOW
						exec up_touch_tem5maisBaratos '<<ALLTRIM(uCrsAtendST.grphmgcode)>>', '<<ALLTRIM(fi.ref)>>', <<uCrsActRef.pvpdic>>
             ENDTEXT
             IF uf_gerais_actgrelha("", 'uCrsBARATOSTEMP', lcsql)
                IF RECCOUNT("uCrsBARATOSTEMP")>0
                   SELECT * FROM uCrsAtendMaisBaratos UNION ALL SELECT * FROM uCrsBARATOSTEMP INTO CURSOR uCrsAtendMaisBaratos READWRITE
                   SELECT ucrsatendmaisbaratos
                   GOTO BOTTOM
                   REPLACE ucrsatendmaisbaratos.oref WITH fi.ref IN ucrsatendmaisbaratos
                ENDIF
             ELSE
                uf_perguntalt_chama("Ocorreu uma anomalia ao verificar a informa��o dos 5 mais baratos.", "OK", "", 16)
             ENDIF
             IF USED("uCrsBARATOSTEMP")
                fecha("uCrsBARATOSTEMP")
             ENDIF
          ENDIF
          IF uf_gerais_getparameter("ADM0000000204", "BOOL") AND USED("ucrsatendlab")
             ALINES(labs, uf_gerais_getparameter("ADM0000000204", "TEXT"), ";")
             FOR i = 1 TO ALEN(labs)
                SELECT ucrsatendst
                IF  .NOT. (UPPER(ALLTRIM(ucrsatendst.titaimdescr))==UPPER(ALLTRIM(labs(i))))
                   STORE .F. TO lcexiste
                   SELECT ucrsatendlab
                   SCAN FOR UPPER(ALLTRIM(ucrsatendlab.oref))==UPPER(ALLTRIM(fi.ref))
                      lcexiste = .T.
                   ENDSCAN
                   IF lcexiste=.F.
                      lcsql = ''
                      TEXT TO lcsql TEXTMERGE NOSHOW
									exec up_touch_temProdutosMLab '<<ALLTRIM(uCrsAtendST.ref)>>', '<<ALLTRIM(uCrsAtendST.cnpem)>>', <<myArmazem>>, '<<ALLTRIM(labs[i])>>', <<mysite_nr>>
                      ENDTEXT
                      IF uf_gerais_actgrelha("", 'uCrsLABTEMP', lcsql)
                         IF RECCOUNT("uCrsLABTEMP")>0
                            SELECT * FROM uCrsAtendLAB UNION ALL SELECT * FROM uCrsLABTEMP INTO CURSOR uCrsAtendLAB READWRITE
                            SELECT ucrsatendlab
                            GOTO BOTTOM
                            REPLACE ucrsatendlab.oref WITH fi.ref IN ucrsatendlab
                         ENDIF
                      ELSE
                         uf_perguntalt_chama("Ocorreu uma anomalia a verificar os laborat�rios.", "OK", "", 16)
                      ENDIF
                      IF USED("uCrsLABTEMP")
                         fecha("uCrsLABTEMP")
                      ENDIF
                   ENDIF
                ENDIF
             ENDFOR
          ENDIF
          IF myusaservicosrobot=.F.
             IF (myusarobot .OR. myusarobotapostore) .AND. myrobotstate
                SELECT fi
                IF EMPTY(fi.ofistamp) .AND.  .NOT. fi.epromo
                   uf_robot_tecnilabmsgb(lccodigo)
                   IF  .NOT. uf_gerais_actgrelha("", "uCrsVerificaDispStock", [exec up_robot_respostaB ']+astr(mytermno)+[', ]+STR(IIF(myusarobotapostore, 1, 0))+'')
                      uf_perguntalt_chama("Ocorreu uma anomalia ao verificar o stock dispon�vel no robot.", "OK", "", 16)
                   ELSE
                      IF RECCOUNT("uCrsVerificaDispStock")>0
                         SELECT ucrsverificadispstock
                         GOTO TOP
                         SELECT fi
                         REPLACE fi.num1 WITH ucrsverificadispstock.stock
                         uf_gerais_actgrelha("", "", [exec up_robot_delTable ']+astr(mytermno)+[', 'b'])
                      ENDIF
                      fecha("uCrsVerificaDispStock")
                   ENDIF
                ENDIF
             ENDIF
             IF myusarobotconsis .AND. myrobotstate
                IF  .NOT. EMPTY(fi.ref)
                   TEXT TO lcsql TEXTMERGE NOSHOW
								exec up_robot_consis_getMap '<<ALLTRIM(fi.ref)>>', 0, 0, 0, <<mysite_nr>>
                   ENDTEXT
                   IF  .NOT. uf_gerais_actgrelha("", "uCrsVerificaDispStock", lcsql)
                      uf_perguntalt_chama("O Software n�o obteve resposta do Robot.", "OK", "", 16)
                   ELSE
                      IF RECCOUNT("uCrsVerificaDispStock")>0
                         SELECT ucrsverificadispstock
                         GOTO TOP
                         SELECT fi
                         REPLACE fi.num1 WITH ucrsverificadispstock.stock
                      ENDIF
                      fecha("uCrsVerificaDispStock")
                   ENDIF
                ENDIF
             ENDIF
          ELSE
             IF myrobotstate=.T.

                LOCAL lcpedstampstrobot
                LOCAL lcrefrobot, lcrespped, lcstockrobot
                lcpedstampstrobot = uf_gerais_stamp()
                SELECT fi
                lcrefrobot = ALLTRIM(fi.ref)

                LOCAL lcrespstkrobot, contador, lctimeout
                lcrespstkrobot = .F.
                lctimeout = uf_gerais_getparameter_site('ADM0000000035', 'NUM', mysite)
                contador = lctimeout/1000
                lcstockrobot = uf_robot_servico_getprondinfo(ALLTRIM(lcpedstampstrobot), ALLTRIM(lcrefrobot))
                IF (EMPTY(lcstockrobot))
                   lcstockrobot = "-99999"
                ENDIF
                IF (VAL(lcstockrobot)>-99999)
                   SELECT fi
                   REPLACE fi.num1 WITH VAL(lcstockrobot)
                   atendimento.pgfdados.page2.txtnum1.refresh
                ELSE
                   MESSAGEBOX("Tempo de resposta do pedido ao Robot de "+ASTR(lctimeout)+" segundos ultrapassado."+chr(13)+chr(13)+"Volte a tentar","Logitools")
                   SELECT fi
                   REPLACE fi.num1 WITH 0
                   atendimento.pgfdados.page2.txtnum1.refresh
                 
                ENDIF
             ELSE
                SELECT fi
                REPLACE fi.num1 WITH 0
             ENDIF
          ENDIF
       ENDIF
       LOCAL lccod
       STORE '' TO lccod
       LOCAL lcposfitemp
       lcposfitemp = RECNO("fi")


       
       IF TYPE("ATENDIMENTO")<>"U"
          lccod = uf_atendimento_verificacompart()
          myecra = 'Atendimento'
       ELSE
          SELECT ft2
          IF  .NOT. EMPTY(ft2.u_codigo)
             lccod = ft2.u_codigo
             myecra = 'FACTURACAO'
          ENDIF
          SELECT fi
       ENDIF
       


       SELECT fi
       TRY
          GOTO lcposfitemp
       CATCH
          GOTO BOTTOM
       ENDTRY
       
       IF  .NOT. EMPTY(lccod)

		  if !uf_gerais_getparameter("ADM0000000355", "bool")
		  	SELECT fi   
		  	    	
       	  	uf_atendimento_validaAlteraDemMcdtLinhas(lccod, fi.lordem, .f.)
       	  ENDIF

          SELECT fi
          REPLACE fi.u_comp WITH .T.
          IF uf_gerais_actgrelha("", 'uCrsDip', [exec up_facturacao_Diploma ']+ALLTRIM(lccodigo)+[', ']+ALLTRIM(lccod)+['])
             IF RECCOUNT("uCrsDip")>0
                mydiplomaauto = .T.
                IF (TYPE("REGVENDAS")=="U")
                   SELECT fi
                   IF (TYPE("RESERVAS")=="U")
                      uf_diplomas_chama()
                   ELSE
                   ENDIF
                ENDIF
             ELSE
                fecha("uCrsDip")
             ENDIF
          ENDIF
       ENDIF
       SELECT ucrsactref
       IF ucrsactref.qlook
          IF TYPE("FACTURACAO")<>"U"
             SELECT td
             IF  .NOT. (td.u_tipodoc==7 .OR. td.u_tipodoc==6 .OR. td.u_tipodoc==2)
                IF  .NOT. (ABS(fi.u_epvp)>ft.etotal)
                   LOCAL lcpostr, lcnrvales, lcvalordesc, lcfistamp
                   STORE 0 TO lcpostr, lcnrvales, lcvalordesc
                   SELECT fi
                   lcfistamp = fi.fistamp
                   lcpostr = RECNO("fi")
                   CALCULATE SUM(fi.u_epvp) TO lcnrvales FOR fi.epromo .AND.  .NOT. (ALLTRIM(fi.fistamp)==ALLTRIM(lcfistamp))
                   CALCULATE SUM(fi.u_descval) TO lcvalordesc FOR  .NOT. fi.epromo
                   SELECT fi
                   TRY
                      GOTO lcpostr
                   CATCH
                      GOTO BOTTOM
                   ENDTRY
                   IF lcvalordesc<>0 .AND. lcnrvales=0
                      IF  .NOT. uf_perguntalt_chama("ATEN��O, AO APLICAR O VALE VAI ELIMINAR OS DESCONTOS DE VALOR APLICADOS ANTERIORMENTE. PRETENDE CONTINUAR?", "Sim", "N�o")
                         REPLACE fi.ref WITH '', fi.design WITH '', fi.u_epvp WITH 0, fi.qtt WITH 0, fi.u_stock WITH 0, fi.epromo WITH .F.
                      ELSE
                         uf_atendimento_transformavaledc(.T.)
                      ENDIF
                   ELSE
                      uf_atendimento_transformavaledc(.T.)
                   ENDIF
                ELSE
                   uf_perguntalt_chama("O VALOR DO VALE � SUPERIOR AO TOTAL DO DOCUMENTO, O DESCONTO N�O SER� APLICADO.", "OK", "", 64)
                   SELECT fi
                   REPLACE fi.ref WITH '', fi.design WITH '', fi.u_epvp WITH 0, fi.qtt WITH 0, fi.u_stock WITH 0, fi.epromo WITH .F.
                ENDIF
             ELSE
                uf_perguntalt_chama("N�O PODE APLICAR VALES DE DESCONTO A ESTE TIPO DE DOCUMENTO.", "OK", "", 64)
                SELECT fi
                REPLACE fi.ref WITH '', fi.design WITH '', fi.u_epvp WITH 0, fi.qtt WITH 0, fi.u_stock WITH 0, fi.epromo WITH .F.
             ENDIF
          ELSE
             LOCAL lcfipos, lctot, lctotsdesc
             STORE 1 TO lcfipos
             STORE 0 TO lctot, lctotsdesc
             lctot = atendimento.txttotalutente.value
             SELECT fi
             IF  .NOT. (ABS(fi.u_epvp)>lctot)
                uf_atendimento_transformavaledc(.F.)
             ELSE
                uf_perguntalt_chama("O VALOR DO VALE � SUPERIOR AO TOTAL DO ATENDIMENTO, O DESCONTO N�O SER� APLICADO.", "OK", "", 64)
                uf_atendimento_apagalinha(.T.)
                SELECT fi
                GOTO TOP
                atendimento.grdatend.setfocus
                fecha("uCrsActRef")
                RETURN
             ENDIF
          ENDIF
       ENDIF

 	
       
       IF TYPE("FACTURACAO")<>"U"
          IF EMPTY(ALLTRIM(fi.ofistamp)) .AND.  .NOT. (fi.epromo)
             uf_atendimento_aplicapromo(.F., 0, .T.)
          ENDIF
       ELSE
          IF EMPTY(ALLTRIM(fi.ofistamp)) .AND.  .NOT. (fi.epromo)
             uf_atendimento_aplicapromo(.F., 0, .F.)
          ENDIF
       ENDIF
       
       
       LOCAL lcEstadoComerc, lcEstadoRuturaStock, lcDataReposicao, lcDataPrevisaoStr, lcDataReposicaoFinal
	   Store "" to lcEstadoComerc
	   lcEstadoRuturaStock = "Temporariamente indispon�vel"
	   lcDataPrevisaoStr = "Informa��o recolhida do dicion�rio disponibilizado pelo Infarmed  prevista reposi��o: "
	   SELECT ucrsactref
	   lcEstadoComerc  = ALLTRIM(ucrsactref.u_nomerc) 
       lcDataReposicao= ucrsactref.data_prevista_reposicao
       
       LOCAL lcposfi
       lcposfi = RECNO("fi")
       SELECT fi
       IF  (!EMPTY(ucrsactref.u_nota1) OR AT(lcEstadoRuturaStock ,lcEstadoComerc)>0) .AND. EMPTY(fi.ofistamp) .AND.  .NOT. tcmostrainfo
          IF TYPE("Reservas")=="U"
          	
	 
             IF LEN(ALLTRIM(ucrsactref.u_nota1))>0 OR  AT(lcEstadoRuturaStock ,lcEstadoComerc)>0
             	
                LOCAL lcnovotexto
                lnmemowith = SET("Memowidth")
                SET MEMOWIDTH TO 72
                lcnovotexto = "INFORMA��O ADICIONAL DO PRODUTO ("+ALLTRIM(ucrsactref.ref)+"): "+CHR(13)+CHR(13)
                FOR i = 1 TO MEMLINES(ucrsactref.u_nota1)
                   lcnovotexto = lcnovotexto+UPPER(ALLTRIM(MLINE(ucrsactref.u_nota1, i)))+CHR(13)
                ENDFOR
                lcnovotexto = LEFT(lcnovotexto, LEN(lcnovotexto)-1)
                SET MEMOWIDTH TO (lnmemowith)�

                if(AT(lcEstadoRuturaStock ,lcEstadoComerc) >0)
                
                	if(LEN(lcnovotexto )>0)
                		lcnovotexto = lcnovotexto  + CHR(13) +  CHR(13) +  UPPER("Medicamento ") + UPPER(lcEstadoRuturaStock) 
                	ELSE
                		lcnovotexto = lcnovotexto   + UPPER("Medicamento ") + UPPER(lcEstadoRuturaStock) 	
                	ENDIF
                	
                	IF  !EMPTY(lcDataReposicao)  AND VARTYPE(lcDataReposicao) == "C" AND LEN(lcDataReposicao)>=8
                		lcDataReposicaoFinal = CTOD(lcDataReposicao)
                	ELSE
                		lcDataReposicaoFinal = lcDataReposicao
                	ENDIF
                	
                	
         
                	IF(!EMPTY(lcDataReposicaoFinal  )  AND  VARTYPE(lcDataReposicaoFinal  ) == "D"  AND  YEAR(lcDataReposicaoFinal  )>2000)
                		lcnovotexto = lcnovotexto   + CHR(13) + CHR(13) + lcDataPrevisaoStr + uf_gerais_getDate(lcDataReposicaoFinal  , 'PRESCRSP')
                	ENDIF
    	
                ENDIF
				
                uf_perguntalt_chama(LEFT(lcnovotexto, 254), "OK", "", 64)
             ENDIF
          ENDIF
       ENDIF
    
       fecha("uCrsActRef")
       SELECT fi
       TRY
          GOTO lcposfi
       CATCH
          GOTO BOTTOM
       ENDTRY
    ENDIF
 ENDIF
 LOCAL lcposfidem
 SELECT fi
 lcposfidem = RECNO("fi")
 IF TYPE("ATENDIMENTO")<>"U"
    uf_atendimento_dadosst()
 ENDIF
 SELECT fi
 TRY
    GOTO lcposfidem
 CATCH
    GOTO BOTTOM
 ENDTRY
 IF TYPE("ATENDIMENTO")<>"U"



**********QUESTIONARIO IQVIA***********************************************************************************************

 
    IF ALLTRIM(lccodigo)=='5681903' .OR. ALLTRIM(lccodigo)=='5771050'
       uf_novonordisk_leitura(ALLTRIM(lccodigo))
    ENDIF
    IF insnnordisk=.T.
    	uf_atendimento_compart_nnordisk()
    	
       lcpos = RECNO("fi")
       lclordem = fi.lordem
       lclordem = VAL(LEFT(ALLTRIM(STR(lclordem)), 2)+'0000')
       SELECT fi
       GOTO TOP
       SCAN
       	 &&fix NOVONORDISK
          IF fi.lordem=lclordem

			IF uf_atendimento_aplicaPlanoND(fi.fistamp)

				SELECT ucrsatendcomp

				REPLACE fi.design WITH '.[' + ALLTRIM(ucrsatendcomp.codigo) + '][' + ALLTRIM(ucrsatendcomp.cptorgabrev) + '] Receita: -'
				REPLACE fi.tipor WITH 'RM'

			ENDIF

          ENDIF
       ENDSCAN
       SELECT fi
       TRY
          GOTO lcpos
       CATCH
       ENDTRY

       uf_atendimento_actvalorescab()
       uf_atendimento_infototaiscab()
       uf_atendimento_infototais()
    ENDIF
    IF TYPE("PESQSTOCKS")=="U"
       insnnordisk = .F.
    ENDIF
 ENDIF

ENDFUNC

FUNCTION uf_atendimento_planoMcdt
	LPARAM	lcPlano
	
	
	
	LOCAL lcResult
	
	lcResult = .f.
	
	fecha("uCrsTempCompAux")
	IF !uf_gerais_actgrelha("", 'uCrsTempCompAux', [exec up_receituario_dadosDoPlano ']+lcPlano+['])
    	uf_perguntalt_chama("OCORREU UMA ANOMALIA A VALIDAR O PLANO MCDT.", "OK", "", 16)
 	ELSE
		SELECT 	uCrsTempCompAux
		GO TOP
		SCAN
			lcResult  = uCrsTempCompAux.mcdt	 
		ENDSCAN	
	ENDIF
	
	fecha("uCrsTempCompAux")
	

	RETURN lcResult 
	

ENDFUNC




FUNCTION uf_atendimento_validaAlteraDemMcdtLinhas
	LPARAMETERS lcPlano, lcLordem, lcForceDem, lcForceIdValidacaoDem
	

	
	local lcposfi, lcIsDem, lcIDValidacaoDem 
	
	lcIDValidacaoDem  = ''
	
	IF(!USED("fi"))
		RETURN .f.
	ENDIF
	
	if(!uf_atendimento_planoMcdt(lcPlano))
		RETURN .f.
	ENDIF
	

	
 	SELECT fi
 	lcposfi = RECNO("FI")
 	lcIsDem = fi.dem
 	
 	
 	

 	 
 	uf_atendimento_devolvecursorcabecalhofi(lcLordem)

	IF(USED("ucrsTempFiCabecalho"))
		 SELECT ucrsTempFiCabecalho
		 GO TOP 		 
		 lcIsDem = ucrsTempFiCabecalho.dem
		 lcIDValidacaoDem = ucrsTempFiCabecalho.id_validacaoDem
		 fecha("ucrsTempFiCabecalho")	 
	 ENDIF
 	
 	SELECT FI
 	GO TOP
 	
 	&&from backoffice
 	if(!EMPTY(lcForceDem))
 		lcIsDem  = .t.
 		lcIDValidacaoDem  = lcForceIdValidacaoDem
 	ENDIF
 	
 	UPDATE FI set FI.dem = lcIsDem, fi.id_validacaoDem = lcIDValidacaoDem   WHERE LEFT(ALLTRIM(str(fi.lordem)),2) = LEFT(ALLTRIM(str(lcLordem)),2) 	
 	
 	
 	lcposfi = RECNO("fi")
	SELECT fi
	TRY
	    GOTO lcposfi
	CATCH
	    GOTO BOTTOM
	ENDTRY
	
	RETURN .t.


ENDFUNC




**
FUNCTION uf_atendimento_validadadospsicoutente

 LOCAL lcposfi, lcreceitaspsico, lcvalido, lctextopsi, lctextadquirente, lctextutente, lctextreceita
 STORE '' TO lctextopsi, lctextadquirente, lctextutente, lctextreceita
 STORE .T. TO lcvalido
 SELECT fi
 lcposfi = RECNO("FI")

 

 SELECT LEFT(astr(lordem), 2) AS lordem FROM fi WHERE fi.u_psico=.T. INTO CURSOR ucrsFILordem READWRITE

 IF (RECCOUNT("ucrsFILordem")>0)

    DO CASE 

        CASE UPPER(ALLTRIM(mypaisconfsoftw)) = 'ANGOLA'

            

            SELECT ucrsatendcl
            GOTO TOP            

            IF EMPTY(ALLTRIM(ucrsatendcl.u_nmutavi))
                lcvalido = .F.
                lctextadquirente = lctextadquirente+"Nome,"
            ENDIF
            IF EMPTY(ALLTRIM(ucrsatendcl.u_ndutavi))
                lcvalido = .F.
                lctextadquirente = lctextadquirente+"Doc ID,"
            ENDIF
            IF uf_gerais_getdate(ucrsatendcl.u_ddutavi, "SQL")=="19000101"
                lcvalido = .F.
                lctextadquirente = lctextadquirente+"Validade Doc ID,"
            ENDIF
            IF (ucrsatendcl.u_idutavi>130 .OR. ucrsatendcl.u_idutavi<1)
                lcvalido = .F.
                lctextadquirente = lctextadquirente+"Data de Nascimento,"
            ENDIF

            SELECT ucrsatendcomp 
            GO TOP

            SCAN

                IF EMPTY(ALLTRIM(ucrsatendcomp.contactoutente))
                    lcvalido = .F.
                    lctextadquirente = lctextadquirente+"Contacto,"
                ENDIF


            ENDSCAN

            SELECT ft
            GOTO TOP



        OTHERWISE

         SELECT ucrsatendcl
         GOTO TOP
         IF EMPTY(ALLTRIM(ucrsatendcl.u_nmutavi))
               lcvalido = .F.
               lctextadquirente = lctextadquirente+"Nome,"
         ENDIF
         IF EMPTY(ALLTRIM(ucrsatendcl.u_moutavi))
               lcvalido = .F.
               lctextadquirente = lctextadquirente+"Morada,"
         ENDIF
         IF EMPTY(ALLTRIM(ucrsatendcl.u_cputavi))
               lcvalido = .F.
               lctextadquirente = lctextadquirente+"C�digo Postal,"
         ENDIF
         IF EMPTY(ALLTRIM(ucrsatendcl.u_ndutavi))
               lcvalido = .F.
               lctextadquirente = lctextadquirente+"Doc ID,"
         ENDIF
         IF uf_gerais_getdate(ucrsatendcl.u_ddutavi, "SQL")=="19000101"
               lcvalido = .F.
               lctextadquirente = lctextadquirente+"Validade Doc ID,"
         ENDIF
         IF (ucrsatendcl.u_idutavi>130 .OR. ucrsatendcl.u_idutavi<1)
               lcvalido = .F.
               lctextadquirente = lctextadquirente+"Data de Nascimento,"
         ENDIF

         SELECT ft
         GOTO TOP

         IF EMPTY(ALLTRIM(ft.nome))
               lcvalido = .F.
               lctextutente = lctextutente+"Nome,"
         ENDIF
         IF EMPTY(ALLTRIM(ft.morada))
               lcvalido = .F.
               lctextutente = lctextutente+"Morada,"
         ENDIF
         IF EMPTY(ALLTRIM(ft.codpost))
               lcvalido = .F.
               lctextutente = lctextutente+"C�digo Postal,"
         ENDIF
         SELECT ucrsfilordem
         GOTO TOP
         SCAN
         SELECT fi
         GOTO TOP
         SCAN FOR LEFT(astr(fi.lordem), 2)=ucrsfilordem.lordem .AND. LEFT(fi.design, 1)=="."
               SELECT ucrsatendcomp
               LOCATE FOR ALLTRIM(ucrsatendcomp.fistamp)==ALLTRIM(fi.fistamp)
               LOCAL lcmedico, lcdatareceita, lcreceitanr
               lcmedico = fi.szzstamp
               lcdatareceita = ucrsatendcomp.datareceita
               lcreceitanr = STREXTRACT(fi.design, ':', ' -', 1, 0)
               IF (fi.dem==.T.)
                  IF EMPTY(ALLTRIM(lcmedico))
                     lcvalido = .F.
                     lctextreceita = lctextreceita+"C�digo M�dico,"
                  ENDIF
                  IF EMPTY(lcdatareceita) .OR. uf_gerais_getdate(lcdatareceita, "SQL")=='19000101'
                     lcvalido = .F.
                     lctextreceita = lctextreceita+"Data,"
                  ENDIF
               ENDIF
               IF LEN(ALLTRIM(lcreceitanr))<>13 .AND. LEN(ALLTRIM(lcreceitanr))<>19
                  lcvalido = .F.
                  lctextreceita = lctextreceita+"N�mero,"
               ENDIF
               SELECT fi
            ENDSCAN
            SELECT ucrsfilordem
         ENDSCAN

    ENDCASE


    IF ( .NOT. EMPTY(ALLTRIM(lctextadquirente)))
       lctextadquirente = SUBSTR(lctextadquirente, 1, LEN(lctextadquirente)-1)
       lctextopsi = lctextopsi+CHR(13)+"Dados Adquirente: "+lctextadquirente
    ENDIF
    IF ( .NOT. EMPTY(ALLTRIM(lctextutente)))
       lctextutente = SUBSTR(lctextutente, 1, LEN(lctextutente)-1)
       lctextopsi = lctextopsi+CHR(13)+"Dados Utente: "+lctextutente
    ENDIF
    IF ( .NOT. EMPTY(ALLTRIM(lctextreceita)))
       lctextreceita = SUBSTR(lctextreceita, 1, LEN(lctextreceita)-1)
       lctextopsi = lctextopsi+CHR(13)+"Dados Receita: "+lctextreceita
    ENDIF
    IF  .NOT. lcvalido
       uf_perguntalt_chama(LEFT("Para abrir o painel de reservas, proceda ao preenchimento dos dados psicotr�picos."+CHR(13)+lctextopsi, 255), "OK", "", 64)
    ENDIF

 ENDIF
 TRY
    GOTO lcposfi
 CATCH
 ENDTRY
 RETURN lcvalido
ENDFUNC
**
PROCEDURE uf_atendimento_reservas
 atendimento.tmroperador.reset()
 LOCAL lctemcompartr
 STORE .F. TO lctemcompartr
 SELECT ucrsatendcomp
 GOTO TOP
 SCAN
 	&&covid comentario inicio
    IF ucrsatendcomp.e_compart=.T. OR ucrsatendcomp.snsExterno=.T.
    &&IF ucrsatendcomp.e_compart=.T.
       lctemcompartr = .T.
    ENDIF
    &&covid comentario fim
 ENDSCAN
 IF lctemcompartr=.T.
    uf_perguntalt_chama("ESTA FUNCIONALIDADE N�O SE ENCONTRA DISPON�VEL PARA COMPARTICIPA��ES EXTERNAS", "OK", "", 64)
 ELSE
    IF myreservascl
       IF (uf_atendimento_validadadospsicoutente())
          SELECT ft
          uf_reservas_cria('CRIA', .T.)
       ENDIF
    ELSE
       uf_perguntalt_chama("PARA ADQUIRIR ESTE M�DULO POR FAVOR ENTRE EM CONTACTO COM SUPORTE. PE�A J� A SUA DEMONSTRA��O.", "OK", "", 64)
    ENDIF
 ENDIF
ENDPROC
**
FUNCTION uf_atendimento_transformavaledc
 LPARAMETERS tcbool, tcapaga, tcdelvales
 IF  .NOT. tcbool
    atendimento.tmroperador.reset()
 ENDIF
 LOCAL lcvalidadescval, lctotalvales
 PUBLIC myvaledescval
 STORE 0 TO lctotalvales
 IF  .NOT. tcapaga
    SELECT fi
    IF fi.u_epvp==0
       RETURN .F.
    ENDIF
 ENDIF
 IF tcbool
    LOCAL lcfp, lcvalor, lctotal, lctotalcdesc, lcpvp, lcpvp1, lcpvp2, lcposicfi
    STORE 0 TO lcfp, lcvalor, lctotal, lctotalcdesc, lcpvp, lcpvp1, lcpvp2
    SELECT fi
    lcfp = RECNO()
    SELECT fi
    GOTO TOP
    SCAN
       IF fi.epromo
          lctotalvales = lctotalvales+fi.u_epvp
       ENDIF
       IF !WEXIST("FACTURACAO")
         REPLACE fi.u_descval WITH 0
       ENDIF
       uf_atendimento_actvaloresfi()
       SELECT fi
    ENDSCAN
    uf_atendimento_acttotaisft()
    SELECT ft
    lctotal = ft.etotal
    IF  .NOT. uf_atendimento_verifres(.T.)
       IF tcdelvales
          IF ABS(lctotalvales)>lctotal .AND. ABS(lctotalvales)>0
             LOCAL lctotval, lccountval
             STORE 0 TO lctotval, lccountval
             DO WHILE ABS(lctotalvales)>lctotal
                SELECT fi
                GOTO TOP
                SCAN
                   IF fi.epromo
                      lccountval = lccountval+1
                      IF lccountval==1
                         uf_perguntalt_chama("ATEN��O: O VALE ["+ALLTRIM(fi.design)+"] VAI SER ELIMINADO POIS O TOTAL DOS VALES EXCEDE O TOTAL DO DOCUMENTO.", "OK", "", 48)
                         SELECT fi
                         DELETE
                      ELSE
                         lctotval = lctotval+fi.u_epvp
                      ENDIF
                   ENDIF
                   SELECT fi
                ENDSCAN
                lccountval = 0
                lctotalvales = lctotval
                lctotval = 0
             ENDDO
          ENDIF
       ENDIF
    ENDIF
    IF ft.etotal>0
       SELECT fi
       CALCULATE SUM(fi.u_epvp) TO lcpvp1 FOR fi.epromo .AND. fi.u_epvp<0
       CALCULATE SUM(fi.u_epvp) TO lcpvp2 FOR fi.epromo .AND. fi.u_epvp>0
       lcpvp = (lcpvp1*-1)+(lcpvp2)
    ELSE
       RETURN
    ENDIF
    SELECT fi
    GOTO TOP
    SCAN FOR (fi.epromo=.F.)
       IF (fi.qtt>0) .AND. (fi.etiliquido>0)
          lcvalor = (fi.etiliquido*100)/lctotal
          lcvalor = (lcpvp*lcvalor)/100
          REPLACE fi.u_descval WITH ROUND(lcvalor, 2)
          uf_atendimento_actvaloresfi()
       ENDIF
       SELECT fi
    ENDSCAN
    uf_atendimento_acttotaisft()
    SELECT ft
    lctotalcdesc = ft.etotal
    IF lctotalcdesc<>(lctotal-lcpvp)
       lctotalcdesc = (lctotal-lcpvp)-lctotalcdesc
       SELECT fi
       GOTO TOP
       SCAN FOR  .NOT. fi.epromo     
          IF (lctotalcdesc<0)
             IF (fi.etiliquido>0)
                IF fi.etiliquido<=(lctotalcdesc*-1)
                   lctotalcdesc = lctotalcdesc+fi.etiliquido
                   REPLACE fi.u_descval WITH fi.u_descval+fi.etiliquido
                ELSE
                   REPLACE fi.u_descval WITH fi.u_descval+(lctotalcdesc*-1)
                   lctotalcdesc = 0
                ENDIF
             ENDIF
          ENDIF
          IF lctotalcdesc>0
             IF fi.u_descval>0
                IF fi.u_descval<=lctotalcdesc
                   lctotalcdesc = lctotalcdesc-fi.u_descval
                   REPLACE fi.u_descval WITH 0
                ELSE
                   REPLACE fi.u_descval WITH fi.u_descval-lctotalcdesc
                   lctotalcdesc = 0
                ENDIF
             ENDIF
          ENDIF
          uf_atendimento_actvaloresfi()
          IF lctotalcdesc=0
             EXIT
          ENDIF
          SELECT fi
       ENDSCAN
       uf_atendimento_acttotaisft()
    ENDIF
    lcvalidadescval = .F.
    SELECT fi
    SCAN FOR fi.epromo==.T.
       lcvalidadescval = .T.
    ENDSCAN
    IF lcvalidadescval
       myvaledescval = .T.
    ELSE
       myvaledescval = .F.
    ENDIF
    SELECT fi
    IF lcfp>0
       TRY
          GOTO lcfp
       CATCH
          GOTO BOTTOM
       ENDTRY
    ENDIF
 ENDIF
 IF  .NOT. tcbool
    LOCAL lcfp, lcvalor, lctotal, lctotalcdesc, lcpvp, lcpvp1, lcpvp2, lcvalida, lctotalvales
    STORE 0 TO lcfp, lcvalor, lctotal, lctotalcdesc, lcpvp, lcpvp1, lcpvp2, lctotalvales
    SELECT fi
    lcfp = RECNO("fi")
    SELECT fi
    GOTO TOP
    SCAN
       IF fi.u_descval>0
          REPLACE fi.u_descval WITH 0
          lcvalida = .T.
       ENDIF
    ENDSCAN
    IF lcvalida
       uf_eventoactvalores()
    ENDIF
    lcvalida = .F.
    SELECT fi
    GOTO TOP
    SCAN FOR  .NOT. (LEFT(fi.design, 1)==".") .AND. partes==0 .AND. EMPTY(ALLTRIM(fi.bistamp))
       IF fi.epromo
          lctotalvales = lctotalvales+fi.u_epvp
       ENDIF
       IF fi.u_descval>0
          REPLACE fi.u_descval WITH 0
          lcvalida = .T.
       ENDIF
       SELECT fi
    ENDSCAN
    IF lcvalida
       uf_eventoactvalores()
    ENDIF
    lctotal = atendimento.txttotalutente.value
    IF  .NOT. uf_atendimento_verifres(.T.)
       IF tcdelvales
          IF ABS(lctotalvales)>lctotal .AND. ABS(lctotalvales)>0
             LOCAL lctotval, lccountval
             STORE 0 TO lctotval, lccountval
             DO WHILE ABS(lctotalvales)>lctotal
                SELECT fi
                GOTO TOP
                SCAN FOR  .NOT. (LEFT(fi.design, 1)==".") .AND. partes==0
                   IF fi.epromo
                      lccountval = lccountval+1
                      IF lccountval==1
                         uf_perguntalt_chama("O VALE ["+ALLTRIM(fi.design)+"] VAI SER ELIMINADO POIS O TOTAL DOS VALES EXCEDE O TOTAL DO DOCUMENTO.", "OK", "", 48)
                         SELECT fi
                         DELETE
                      ELSE
                         lctotval = lctotval+fi.u_epvp
                      ENDIF
                   ENDIF
                   SELECT fi
                ENDSCAN
                lccountval = 0
                lctotalvales = lctotval
                lctotval = 0
             ENDDO
          ENDIF
       ENDIF
    ENDIF
    IF lctotal>0
       SELECT fi
       CALCULATE SUM(fi.u_epvp) TO lcpvp1 FOR fi.epromo .AND. fi.u_epvp<0
       CALCULATE SUM(fi.u_epvp) TO lcpvp2 FOR fi.epromo .AND. fi.u_epvp>0
       lcpvp = (lcpvp1*-1)+(lcpvp2)
    ELSE
       RETURN .F.
    ENDIF
    IF lcpvp=0
       myvaledescval = .F.
       RETURN .F.
    ENDIF
    SELECT fi
    GOTO TOP
    SCAN FOR fi.epromo==.F. .AND.  .NOT. (LEFT(fi.design, 1)==".") .AND. partes==0
       IF fi.qtt>0 .AND. fi.etiliquido>0
          lcvalor = (fi.etiliquido*100)/lctotal
          lcvalor = (lcpvp*lcvalor)/100
          REPLACE fi.u_descval WITH ROUND(lcvalor, 2)
       ENDIF
       lcposicfi = RECNO("FI")
       uf_atendimento_fiiliq(.F.)
       SELECT fi
       TRY
          GOTO lcposicfi
       CATCH
       ENDTRY
       SELECT fi
    ENDSCAN
    uf_eventoactvalores()
    lctotalcdesc = atendimento.txttotalutente.value
    IF lctotalcdesc<>(lctotal-lcpvp)
       SELECT ref, fistamp, lordem FROM fi WHERE  NOT EMPTY(fi.ref) AND fi.partes==0 AND  NOT fi.epromo ORDER BY fi.ofistamp DESC INTO CURSOR uCrsFiDev READWRITE
       lctotalcdesc = (lctotal-lcpvp)-lctotalcdesc
       SELECT ucrsfidev
       GOTO TOP
       SCAN
          SELECT fi
          GOTO TOP
          SCAN FOR ALLTRIM(fi.fistamp)==ALLTRIM(ucrsfidev.fistamp) .AND. fi.lordem==ucrsfidev.lordem .AND. ALLTRIM(fi.ref)==ALLTRIM(ucrsfidev.ref)
             IF lctotalcdesc<0
                IF fi.etiliquido>0
                   IF fi.etiliquido<=(lctotalcdesc*-1)
                      lctotalcdesc = lctotalcdesc+fi.etiliquido
                      SELECT fi
                      REPLACE fi.u_descval WITH fi.u_descval+fi.etiliquido
                   ELSE
                      SELECT fi
                      REPLACE fi.u_descval WITH fi.u_descval + (lctotalcdesc*-1)
                      lctotalcdesc = 0
                   ENDIF
                ENDIF
             ENDIF
             IF lctotalcdesc>0
                IF fi.u_descval>0
                   IF fi.u_descval<=lctotalcdesc
                   	lctotalcdesc = lctotalcdesc-fi.u_descval
                      SELECT fi
                      REPLACE fi.u_descval WITH 0
                   ELSE
                      SELECT fi
                      REPLACE fi.u_descval WITH fi.u_descval-lctotalcdesc
                      lctotalcdesc = 0
                   ENDIF
                ENDIF
             ENDIF
             IF lctotalcdesc=0
                EXIT
             ENDIF
             SELECT fi
          ENDSCAN
          IF lctotalcdesc=0
             EXIT
          ENDIF
          SELECT ucrsfidev
       ENDSCAN
       uf_eventoactvalores()
    ENDIF
    IF USED("uCrsFiDev")
       fecha("uCrsFiDev")
    ENDIF
    myvaledescval = .T.
    SELECT fi
    IF lcfp>0
       TRY
          GOTO lcfp
       CATCH
          GOTO BOTTOM
       ENDTRY
    ENDIF
 ENDIF
ENDFUNC
**
PROCEDURE uf_atendimento_fiiliq
 LPARAMETERS tcbool
 LOCAL lctabiva, lctaxaiva
 STORE 0 TO lctaxaiva, lctabiva
 SELECT ft
 IF ( .NOT. EMPTY(ft.tabiva) .AND. ALLTRIM(fi.ref)=="R000001")
    REPLACE fi.tabiva WITH ft.tabiva
    lctaxaiva = uf_gerais_gettabelaiva(ft.tabiva, "TAXA")
    REPLACE fi.iva WITH lctaxaiva
 ENDIF
 SELECT fi
 IF fi.qtt<>0
    SELECT fi
    REPLACE fi.tiliquido WITH ROUND(fi.qtt*fi.pv, 2)
    REPLACE fi.etiliquido WITH ROUND(fi.qtt*fi.epv, 2)
     
    IF (fi.qtt>0)
       IF (fi.desconto>0) .OR. (fi.desc2>0) .OR. (fi.u_descval>0)
          IF (fi.desconto>0)
             REPLACE fi.tiliquido WITH ROUND(fi.pv*fi.qtt-(fi.pv*fi.qtt*(fi.desconto/100)), 2), fi.etiliquido WITH ROUND(fi.epv*fi.qtt-(fi.epv*fi.qtt*(fi.desconto/100)), 2)
          ENDIF
          IF (fi.desc2>0)
             IF (fi.desconto=0)
                REPLACE fi.tiliquido WITH ROUND(fi.pv*fi.qtt-(fi.pv*fi.qtt*(fi.desc2/100)), 2), fi.etiliquido WITH ROUND(fi.epv*fi.qtt-(fi.epv*fi.qtt*(fi.desc2/100)), 2)
             ELSE
                REPLACE fi.tiliquido WITH fi.tiliquido-(fi.tiliquido*(fi.desc2/100)), fi.etiliquido WITH fi.etiliquido-(fi.etiliquido*(fi.desc2/100))
             ENDIF
          ENDIF
          IF (fi.u_descval>0)
             IF fi.desconto==0 .AND. fi.desc2==0
                REPLACE fi.tiliquido WITH ROUND(fi.pv*fi.qtt-(fi.u_descval*200.482 ), 2), fi.etiliquido WITH ROUND(fi.epv*fi.qtt-(fi.u_descval), 2)
             ELSE
                REPLACE fi.tiliquido WITH fi.tiliquido-(fi.u_descval*200.482 ), fi.etiliquido WITH fi.etiliquido-(fi.u_descval)
             ENDIF
          ENDIF
       ENDIF
       uf_atendimento_calcvalorreserva()
       
    ENDIF
    IF tcbool
       IF USED("TD")
          SELECT td
          IF (td.tipodoc=3)
             SELECT fi
             REPLACE fi.tiliquido WITH fi.tiliquido*-1, fi.etiliquido WITH fi.etiliquido*-1
          ENDIF
       ENDIF
    ENDIF
 ELSE
    REPLACE fi.epv WITH 0, fi.pv WITH 0, fi.tiliquido WITH 0, fi.etiliquido WITH 0, fi.desconto WITH 0, fi.desc2 WITH 0, fi.desc3 WITH 0, fi.desc4 WITH 0, fi.u_descval WITH 0
 ENDIF
 IF usamancompart
    LOCAL lcfistamplin, lctotlin
    lcfistamplin = ALLTRIM(fi.fistamp)
    lctotlin = fi.etiliquido
    SELECT fimancompart
    LOCATE FOR ALLTRIM(fimancompart.fistamp)=ALLTRIM(lcfistamplin)
    IF FOUND()
       REPLACE fimancompart.valortot WITH lctotlin
    ENDIF
    SELECT fi
 ENDIF
ENDPROC
**
PROCEDURE uf_atendimento_adicionalinhafi
 LPARAMETERS tcbool, tcbool2
 LOCAL lcordem, lcpos, lczeros, lcstamp
 SELECT fi
 lcposfi = RECNO("fi")
 SELECT fi
 TRY
    GOTO lcposfi
 CATCH
    GOTO BOTTOM
 ENDTRY
 SELECT fi
 lcordem = fi.lordem
 lcfistamp = fi.fistamp
 IF  .NOT. EMPTY(ALLTRIM(fi.ref)) .OR. LEFT(ALLTRIM(fi.design), 1)=="." .OR. (tcbool2==.T.)
    IF tcbool .OR. tcbool2
       CALCULATE MAX(fi.lordem) TO lcordem 
    ENDIF
    IF tcbool2
       IF TYPE("ATENDIMENTO")<>"U"
          IF atendimento.txttotalvendas.value>9
             IF atendimento.txttotalvendas.value>99
                lczeros = STR(atendimento.txttotalvendas.value+1)+REPLICATE("0", 8-LEN(astr(atendimento.txttotalvendas.value)))
             ELSE
                lczeros = STR(atendimento.txttotalvendas.value+1)+REPLICATE("0", 7-LEN(astr(atendimento.txttotalvendas.value)))
             ENDIF
          ELSE
             lczeros = STR(atendimento.txttotalvendas.value+1)+REPLICATE("0", 6-LEN(astr(atendimento.txttotalvendas.value)))
          ENDIF
          lcordem = VAL(lczeros)
       ELSE
          lcordem = lcordem+100000
       ENDIF
    ELSE
       CALCULATE MAX(fi.lordem) TO lcvalorminimo FOR LEFT(ALLTRIM(fi.design), 1)=="." .AND. fi.lordem<=lcordem
       CALCULATE MIN(fi.lordem) TO lcvalormaximo FOR LEFT(ALLTRIM(fi.design), 1)=="." .AND. fi.lordem>lcordem
       IF EMPTY(lcvalormaximo)
          lcvalormaximo = 10000000
       ENDIF
       CALCULATE MAX(fi.lordem) TO lcordem FOR fi.lordem>=lcvalorminimo .AND. fi.lordem<lcvalormaximo
       IF TYPE("fi.bostamp")<>"U"
          CALCULATE MAX(fi.bostamp) TO lcbostampcab FOR fi.lordem=lcvalorminimo
          CALCULATE MAX(fi.nmdos) TO lcnmdos FOR fi.lordem=lcvalorminimo
       ENDIF
       lcordem = lcordem+100
    ENDIF
    lcstamp = uf_gerais_stamp()
    SELECT fi
    DELETE TAG all
    SELECT fi
    APPEND BLANK
    SELECT fi
    stamplindem = lcstamp
    REPLACE fi.fistamp WITH lcstamp
    REPLACE fi.lordem WITH lcordem
    REPLACE fi.ftstamp WITH ft.ftstamp
    REPLACE fi.partes WITH 0
    REPLACE fi.fmarcada WITH .F.
    REPLACE fi.marcada WITH .F.
    IF TYPE("ATENDIMENTO")<>"U" .AND. VARTYPE(lcbostampcab)<>'U' .AND.  .NOT. EMPTY(lcbostampcab)
       REPLACE fi.bostamp WITH ALLTRIM(lcbostampcab)
       REPLACE fi.nmdos WITH ALLTRIM(lcnmdos)
    ENDIF
    IF TYPE("FACTURACAO")<>"U"
       LOCAL lcmoedadefault
       STORE 'EURO' TO lcmoedadefault
       lcmoedadefault = uf_gerais_getparameter("ADM0000000260", "text")
       IF EMPTY(lcmoedadefault)
          SELECT fi
          REPLACE fi.tabiva WITH 1
          REPLACE fi.iva WITH uf_gerais_gettabelaiva(1, "TAXA")
       ELSE
          REPLACE fi.tabiva WITH 4
          REPLACE fi.iva WITH 0
       ENDIF
       IF USED("TD")
          SELECT td
          SELECT fi
          REPLACE fi.nmdoc WITH ALLTRIM(td.nmdoc)
          REPLACE fi.ndoc WITH td.ndoc
          REPLACE fi.fno WITH ft.fno
       ELSE
          SELECT fi
          REPLACE fi.nmdoc WITH ALLTRIM(ft.nmdoc)
          REPLACE fi.ndoc WITH ft.ndoc
          REPLACE fi.fno WITH ft.fno
       ENDIF
       SELECT fi
       DO CASE
          CASE  .NOT. EMPTY(facturacao.containercab.armazem.value)
             REPLACE fi.armazem WITH facturacao.containercab.armazem.value
          CASE  .NOT. EMPTY(myarmazem)
             REPLACE fi.armazem WITH myarmazem
          OTHERWISE
             REPLACE fi.armazem WITH 1
       ENDCASE
       CALCULATE MAX(fi.lordem) TO lcordem 
       lcordem = lcordem+100
       SELECT fi
       REPLACE fi.lordem WITH lcordem
    ENDIF
    SELECT ft
    SELECT fi
    REPLACE fi.ficcusto WITH ft.ccusto
	     
	
 ENDIF
 IF  .NOT. tcbool
 ELSE
    WITH facturacao.pageframe1.page1.griddoc
       GOTO BOTTOM
       .setfocus()
       TRY
          .columns(.activecolumn).readonly = .F.
       CATCH
          uf_perguntalt_chama("ESTE DOCUMENTO N�O TEM WORKFLOW CONFIGURADO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 48)
       ENDTRY
    ENDWITH
    FOR i = 1 TO facturacao.pageframe1.page1.griddoc.columncount
       IF UPPER(facturacao.pageframe1.page1.griddoc.columns(i).controlsource)=="FI.REF"
          facturacao.pageframe1.page1.griddoc.columns(i).setfocus
          EXIT
       ENDIF
    ENDFOR
 ENDIF
 SELECT fi
 INDEX ON fi.lordem TAG lordem
 SELECT fi2
 DELETE TAG all
 IF  .NOT. EMPTY(lcstamp)
    SELECT fi2
    APPEND BLANK
    REPLACE fi2.fistamp WITH lcstamp
 ENDIF
 IF  .NOT. EMPTY(lcstamp)
    SELECT fi
    LOCATE FOR ALLTRIM(fi.fistamp)==ALLTRIM(lcstamp)
 ELSE
    SELECT fi
    LOCATE FOR fi.lordem==lcordem
 ENDIF


  	
 
 
 IF uf_gerais_getparameter("ADM0000000267", "BOOL")
    uf_atendimento_conferir_limpar()
 ELSE
    uf_atendimento_salvar_limpar()
 ENDIF
 

 
 
 
 
 
ENDPROC


FUNCTION uf_atendimento_retornaPlanoCabecalho
 LPARAMETERS lcCabPos
 LOCAL lcposFi, lcTemPlano, lccod 
 
 
 lcTemPlano = .f.
 
 
 SELECT fi
 lcposfi = RECNO("fi")
 
 uf_atendimento_devolvecursorcabecalhofi(lcCabPos)
 
 
 lccod = ""
 IF(USED("ucrsTempFiCabecalho"))
	 SELECT ucrsTempFiCabecalho
	 GO TOP 
	 
	 lccod = STREXTRACT(ucrsTempFiCabecalho.design, '[', ']', 1, 0)
	 IF(!EMPTY(ALLTRIM(lccod)))
	 	lcTemPlano  = .t.
	 ENDIF
	 

	 fecha("ucrsTempFiCabecalho")
 
 ENDIF
 

 SELECT fi
 TRY
    GOTO lcposFi
 CATCH
 ENDTRY
	
	
 RETURN lcTemPlano 

ENDFUNC






**
PROCEDURE uf_atendimento_adicionalinhafimancompart

 LOCAL lcfiref, lcfidesign, lcfistamp, lcnovalin, lctotlin, lclordemfcompart, lcOrdemCab, uv_plano, uv_descSeg, uv_curFistamp

 STORE 0 TO uv_descSeg
 STORE '' TO uv_plano

 SELECT fi
 lcfiref = ALLTRIM(fi.ref)
 lcfidesign = ALLTRIM(fi.design)
 lcfistamp = ALLTRIM(fi.fistamp)
 lctotlin = ROUND(fi.etiliquido, 2)
 lclordemfcompart = fi.lordem
 lcOrdemCab = LEFT(astr(fi.lordem), 2)
 
 
 IF(!USED("fimancompart"))
 	SELECT fi
 	return
 ENDIF
 
 SELECT * FROM fi WITH (BUFFERING = .T.) INTO CURSOR uc_tmpFI

 SELECT uc_tmpFI 
 LOCATE FOR uc_tmpFi.fistamp = fi.FISTAMP

 IF FOUND()

   SELECT uc_tmpFI 

   DO WHILE LEFT(uc_tmpFi.design, 1) <> "."

      SELECT uc_tmpFI
      SKIP -1

   ENDDO

   SELECT uc_tmpFI

   uv_plano = STREXTRACT(uc_tmpFi.design, '[', ']', 1, 0)

 ENDIF

 FECHA("uc_tmpFI")

 SELECT ucrsatendcomp

 LOCATE FOR uf_gerais_compStr(uv_plano, ucrsatendcomp.codigo)

 IF FOUND()
    uv_descSeg = ucrsatendcomp.descHabitual
 ENDIF
	 
 SELECT fimancompart
 LOCATE FOR ALLTRIM(fimancompart.fistamp)=lcfistamp
 IF !FOUND()

   SELECT fimancompart
   GOTO BOTTOM
   APPEND BLANK

   REPLACE fimancompart.ref WITH ALLTRIM(fi.ref),;
            fimancompart.design WITH ALLTRIM(fi.design),;
            fimancompart.fistamp WITH ALLTRIM(fi.fistamp),;
            fimancompart.valortot WITH fi.tiliquido,;
            fimancompart.lordem WITH fi.lordem,;
            fimancompart.perc WITH fi.comp,;
            fimancompart.valor WITH fi.u_ettent1,;
            fimancompart.descSeg WITH uv_descSeg

   **IF (fi.desconto>0) .OR. (fi.desc2>0) .OR. (fi.u_descval>0)
   **   IF (fi.desconto>0)
   **      REPLACE fimancompart.totLin WITH ROUND(fi.epvori*fi.qtt-(fi.epvori*fi.qtt*(fi.desconto/100)), 2)
   **   ENDIF
   **   IF (fi.desc2>0)
   **      IF (fi.desconto=0)
   **         REPLACE fimancompart.totLin WITH ROUND((fi.epvori*fi.qtt)-((fi.epvori*fi.qtt)*(fi.desc2/100)), 2)
   **      ELSE
   **         REPLACE fimancompart.totLin WITH (fi.epvori*fi.qtt)-((fi.epvori*fi.qtt)*(fi.desc2/100))
   **      ENDIF
   **   ENDIF
   **   IF (fi.u_descval>0)
   **      IF fi.desconto==0 .AND. fi.desc2==0
   **         REPLACE fimancompart.totLin WITH ROUND(fi.epvori*fi.qtt-(fi.u_descval), 2)
   **      ELSE
   **         REPLACE fimancompart.totLin WITH (fi.epvori*fi.qtt)-(fi.u_descval)
   **      ENDIF
   **   ENDIF
**
   **ELSE
**
   **   REPLACE fimancompart.totLin WITH ROUND(fi.epvori*fi.qtt,2)
**
   **ENDIF

   REPLACE fimancompart.totLin WITH ROUND(fi.epvori*fi.qtt,2)
   REPLACE fimancompart.totLinDescSeg WITH ROUND(fimancompart.totLin - ((fimancompart.totLin*uv_descSeg)/100), 2)

   SELECT fi
   uv_curFistamp = fi.fistamp

   SELECT fi2
   GO TOP

   LOCATE FOR fi2.fistamp = uv_curFistamp

   IF FOUND()

      SELECT fi2
      REPLACE fi2.descontoHabitual WITH uv_descSeg

   ENDIF 

 ENDIF
 
 SELECT fi
ENDPROC
**
FUNCTION uf_atendimento_apagalinha
 LPARAMETERS lcbool, llreserva, lnapagacabecalho1
 


 IF uf_atendimento_validacampanhas()==.F.
    RETURN .F.
 ENDIF
 atendimento.tmroperador.reset()
 LOCAL lccount, lcvalida, lcfipos, lcvalidadelvale, lcfiposreset, lcbistamp
 STORE 0 TO lccount, lcfipos
 atendimento.lockscreen = .T.
 SELECT fi
 IF (LEFT(fi.design, 1)==".") .AND. (fi.lordem==100000) .AND. lnapagacabecalho1==.F.
    uf_perguntalt_chama("N�O � POSS�VEL APAGAR O CABE�ALHO DA 1� VENDA.", "OK", "", 64)
    atendimento.lockscreen = .F.
    RETURN .F.
 ENDIF
 IF (LEFT(fi.design, 1)<>".") .AND. ( .NOT. EMPTY(fi.tkhposlstamp)) .AND. ( .NOT. lcbool)
    uf_perguntalt_chama("N�O PODE APAGAR PRODUTOS A REGULARIZAR COM REGISTO NOS LOTES.", "OK", "", 64)
    atendimento.lockscreen = .F.
    RETURN .F.
 ENDIF
 LOCAL lcvaladres
 STORE .F. TO lcvaladres
 SELECT fi
 lcfiposreset = RECNO("FI")
 lcbistamp = ALLTRIM(fi.bistamp)

 SELECT FI
  IF LEFT(fi.design, 1)=="."
    IF  .NOT. lcbool

       atendimento.lockscreen = .F.
       uv_opcao = uf_perguntalt_chama("PRETENDE ELIMINAR APENAS O CABE�ALHO DA VENDA OU O CABE�ALHO E AS LINHAS?", "Cab./Linhas", "Cancelar","","","S� Cab.","save_w.png")
       uv_soCab = myretornocancelar

       atendimento.lockscreen = .T.

       IF uv_opcao OR uv_soCab

            uv_curStamp = fi.fistamp
            uv_curLordem = fi.lordem

            SELECT * FROM fi WITH (BUFFERING = .T.) INTO CURSOR uc_tmpFI

            SELECT uc_tmpFI
            LOCATE FOR ALLTRIM(uc_tmpFI.fistamp) == ALLTRIM(uv_curStamp)

            SELECT uc_tmpFI
            CALCULATE MIN(uc_tmpFI.lordem) FOR uc_tmpFI.lordem > uv_curLordem TO uv_lordem

            IF uv_lordem = 0
    
                SELECT uc_tmpFI
                CALCULATE MAX(uc_tmpFI.lordem) FOR uc_tmpFI.lordem < uv_curLordem TO uv_lordem 

            ENDIF


          IF !uv_soCab
             CREATE CURSOR uCrsDelLinhas (lordem NUMERIC(10))
             SELECT fi
             DO WHILE  .NOT. EOF()
                IF (LEFT(fi.design, 1)=".") .AND. (lccount>0)
                   EXIT
                ENDIF
                IF ALLTRIM(fi.ref)=="R000001" .AND. EMPTY(ALLTRIM(fi.ofistamp))
                   IF  .NOT. uf_perguntalt_chama("Uma das refer�ncias � um Adiantamento de Reserva."+CHR(13)+"Ao apagar esta refer�ncia poder� incorrer em erros de caixa."+CHR(13)+"Pretende continuar?", "Sim", "N�o")
                      lcvaladres = .T.
                   ELSE
                      SELECT ucrsatendcl
                      TEXT TO lcsql TEXTMERGE NOSHOW
									INSERT into B_ocorrencias
										(stamp, linkstamp, tipo, grau, descr,
										ovalor, dvalor,
										usr, date, site)
									values
										(LEFT(newid(),25), '', 'Vendas', 2, 'Apagada refer�ncia de Adiantamento de Reserva.',
										'PVP: <<fi.etiliquido>>', 'Cliente: [<<uCrsAtendCl.no>>][<<uCrsAtendCl.estab>>]',
										<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()), '<<mySite>>')
                      ENDTEXT
                      IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
                         uf_perguntalt_chama("OCORREU UM PROBLEMA A GUARDAR REGISTO DE ALTERA��O DO PVP.", "OK", "", 16)
                         atendimento.lockscreen = .F.
                         RETURN .F.
                      ENDIF
                   ENDIF
                ENDIF
                IF  .NOT. lcvaladres
                   SELECT ucrsdellinhas
                   APPEND BLANK
                   REPLACE ucrsdellinhas.lordem WITH fi.lordem
                   lccount = lccount+1
                ELSE
                   lcvaladres = .F.
                ENDIF
                SELECT fi
                TRY
                   SKIP
                CATCH
                ENDTRY
             ENDDO
             
             SELECT fi
             GOTO TOP
             SCAN
                SELECT ucrsdellinhas
                GOTO TOP
                SCAN
                   IF ucrsdellinhas.lordem==fi.lordem
                      IF USED("uCrsVale")
                         lccount = 0
                         SELECT ucrsvale
                         LOCATE FOR ALLTRIM(ucrsvale.fistamp)==ALLTRIM(fi.ofistamp)
                         IF ucrsvale.qtt==fi.qtt
                            SELECT ucrsvale
                            DELETE FROM uCrsVale WHERE ALLTRIM(ucrsvale.fistamp)==ALLTRIM(fi.ofistamp)
                            CALCULATE SUM(ucrsvale.qtt) TO lccount 
                            IF lccount=0
                               fecha("uCrsVale")
                            ELSE
                               GOTO TOP
                            ENDIF
                         ELSE
                            SELECT ucrsvale
                            REPLACE ucrsvale.qtt WITH ucrsvale.qtt-fi.qtt
                            REPLACE ucrsvale.etiliquido WITH ucrsvale.qtt*ucrsvale.etiliquido2/ucrsvale.qtt2
                         ENDIF
                         SELECT fi
                         uf_atendimento_marcademtotal(fi.dem, fi.design, fi.receita, fi.id_validacaodem, .F., .T.)
                         SELECT fi
                         IF ALLTRIM(fi.ref)=='5681903' .OR. ALLTRIM(fi.ref)=='5771050'
                            uf_novonordisk_remove(ALLTRIM(fi.fistamp))
                         ENDIF
                         SELECT fi
                         uf_fi_trans_info_remove(ALLTRIM(fi.ref))
                         SELECT fi
                         uf_atendimento_fi2_linha_remove(ALLTRIM(fi.fistamp))
                         SELECT fi
                         uf_atendimento_fimancompart_linha_remove(ALLTRIM(fi.fistamp))
                         SELECT fi
                         uf_atendimento_removeCompart(.T.)
                         SELECT fi
                         DELETE
                         lcvalida = .T.
                      ELSE

                         SELECT fi
                         uf_atendimento_marcademtotal(fi.dem, fi.design, fi.receita, fi.id_validacaodem, .F., .T.)
                         SELECT fi
                         IF ALLTRIM(fi.ref)=='5681903' .OR. ALLTRIM(fi.ref)=='5771050'
                            uf_novonordisk_remove(ALLTRIM(fi.fistamp))
                         ENDIF
                         SELECT fi
                         uf_fi_trans_info_remove(ALLTRIM(fi.ref))
                         SELECT fi
                         uf_atendimento_fi2_linha_remove(ALLTRIM(fi.fistamp))
                         SELECT fi
                         uf_atendimento_fimancompart_linha_remove(ALLTRIM(fi.fistamp))
                         SELECT fi
                         uf_atendimento_removeCompart(.T.)
                         SELECT fi
                         DELETE
                         lcvalida = .T.
                      ENDIF
                   ENDIF
                   SELECT ucrsdellinhas
                ENDSCAN
                SELECT fi
             ENDSCAN
             fecha("uCrsDelLinhas")
             IF  .NOT. EMPTY(atendimento.txttotalvendas.value)
                atendimento.txttotalvendas.value = atendimento.txttotalvendas.value-1
             ENDIF
             atendimento.grdatend.setfocus
             SELECT fi
             GOTO TOP
             uf_atendimento_transformavaledc(.F., .T., .T.)
             uf_eventoactvalores()
             SELECT fi
             uf_atendimento_verificalordem()


            SELECT fi
			   GO TOP
            atendimento.grdatend.setfocus
            LOCATE FOR fi.lordem == uv_lordem

             atendimento.lockscreen = .F.
             RETURN
          ELSE
             SELECT fi
             uf_atendimento_marcademtotal(fi.dem, fi.design, fi.receita, fi.id_validacaodem, .F.)
             SELECT fi
             IF ALLTRIM(fi.ref)=='5681903' .OR. ALLTRIM(fi.ref)=='5771050'
                uf_novonordisk_remove(ALLTRIM(fi.fistamp))
             ENDIF
             SELECT fi
             uf_fi_trans_info_remove(ALLTRIM(fi.ref))
             SELECT fi
             uf_atendimento_fi2_linha_remove(ALLTRIM(fi.fistamp))
             SELECT fi
             uf_atendimento_fimancompart_linha_remove(ALLTRIM(fi.fistamp))
             SELECT fi
             uf_atendimento_removeCompart(.T.)
             SELECT fi
             DELETE
             lcvalida = .T.
             atendimento.grdatend.refresh
             atendimento.txttotalvendas.value = atendimento.txttotalvendas.value-1
             uf_eventoactvalores()
             SELECT fi
             TRY
                SKIP -1
             CATCH
             ENDTRY
             uf_atendimento_verificalordem()
            SELECT fi
			   GO TOP
            atendimento.grdatend.setfocus
			   LOCATE FOR fi.lordem == uv_lordem

             atendimento.lockscreen = .F.
             RETURN
          ENDIF
          SELECT fi
          GOTO TOP
       ENDIF
    ELSE

       SELECT fi
       uf_atendimento_marcademtotal(fi.dem, fi.design, fi.receita, fi.id_validacaodem, .F.)
       SELECT fi
       IF ALLTRIM(fi.ref)=='5681903' .OR. ALLTRIM(fi.ref)=='5771050'
          uf_novonordisk_remove(ALLTRIM(fi.fistamp))
       ENDIF
       SELECT fi
       uf_fi_trans_info_remove(ALLTRIM(fi.ref))
       SELECT fi
       uf_atendimento_fi2_linha_remove(ALLTRIM(fi.fistamp))
       SELECT fi
       uf_atendimento_fimancompart_linha_remove(ALLTRIM(fi.fistamp))
       SELECT fi
       uf_atendimento_removeCompart(.T.)
       SELECT fi
       DELETE
       lcvalida = .T.
       atendimento.grdatend.setfocus
       atendimento.txttotalvendas.value = atendimento.txttotalvendas.value-1
    ENDIF

	SELECT fi
	atendimento.grdatend.setfocus

    atendimento.lockscreen = .F.

    RETURN
 ENDIF
 IF ALLTRIM(fi.ref)=="R000001" .AND. TYPE("RESERVAS")=="U"
    IF  .NOT. uf_perguntalt_chama("A refer�ncia � um Adiantamento de Reserva."+CHR(13)+"Ao apagar esta refer�ncia poder� incorrer em erros de caixa."+CHR(13)+"Pretende continuar?", "Sim", "N�o")
       atendimento.lockscreen = .F.
       RETURN .F.
    ELSE
       SELECT ucrsatendcl
       TEXT TO lcsql TEXTMERGE NOSHOW
				INSERT into B_ocorrencias
					(stamp, linkstamp, tipo, grau, descr,
					ovalor, dvalor,
					usr, date, site)
				values
					(LEFT(newid(),25), '', 'Vendas', 2, 'Apagada refer�ncia de Adiantamento de Reserva.',
					'PVP: <<fi.etiliquido>>', 'Cliente: [<<uCrsAtendCl.no>>][<<uCrsAtendCl.estab>>]',
					<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()), '<<mySite>>')
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
          uf_perguntalt_chama("OCORREU UM PROBLEMA A GUARDAR REGISTO DE ALTERA��O DO PVP.", "OK", "", 16)
          atendimento.lockscreen = .F.
          RETURN .F.
       ENDIF
    ENDIF
 ENDIF
 

 SELECT fi
 IF  .NOT. EMPTY(fi.id_validacaodem) .AND.  .NOT. EMPTY(fi.receita) .AND. fi.dem=.T.
    LOCAL lcndnrreceita, lcnddescr, lcid_val, lcndqtt, lcndvallinha
    STORE '' TO lcndnrreceita, lcnddescr
    lcid_val = ALLTRIM(fi.id_validacaodem)
    lcndqtt = fi.qtt
    IF USED("uCrsVerificaRespostaDEMTotal")
       SELECT ucrsverificarespostademtotal
       GOTO TOP
       SCAN
          IF ALLTRIM(ucrsverificarespostademtotal.id)==ALLTRIM(lcid_val)
             lcndnrreceita = ALLTRIM(ucrsverificarespostademtotal.receita_nr)
             lcnddescr = ALLTRIM(ucrsverificarespostademtotal.medicamento_descr)
             lcndvallinha = ucrsverificarespostademtotal.validade_linha
             lcndposologia = ucrsverificarespostademtotal.posologia
          ENDIF
       ENDSCAN
       SELECT ucrsdemndisp
       GOTO BOTTOM
       APPEND BLANK
       REPLACE ucrsdemndisp.receita WITH ALLTRIM(lcndnrreceita)
       REPLACE ucrsdemndisp.codacesso WITH ''
       REPLACE ucrsdemndisp.coddiropcao WITH ''
       REPLACE ucrsdemndisp.nrsns WITH ''
       REPLACE ucrsdemndisp.medicamento_descr WITH ALLTRIM(lcnddescr)
       REPLACE ucrsdemndisp.tot WITH lcndqtt
       REPLACE ucrsdemndisp.validade_linha WITH lcndvallinha
       REPLACE ucrsdemndisp.posologia WITH lcndposologia
       
    ENDIF
    SELECT fi
 ENDIF
 

 SELECT fi
 IF  .NOT. EMPTY(fi.design)
    IF ALLTRIM(fi.design)=='Desconto de valor em cart�o'
       REPLACE ucrsatendcl.valcartao WITH ucrsatendcl.valcartao+fi.u_epvp
    ENDIF
    IF  .NOT. EMPTY(fi.ofistamp)
       DELETE FROM uCrsAtendST WHERE ucrsatendst.ststamp=fi.fistamp
       IF USED("uCrsVale")
          lccount = 0
          SELECT ucrsvale
          LOCATE FOR ALLTRIM(ucrsvale.fistamp)==ALLTRIM(fi.ofistamp)
          IF ucrsvale.qtt==fi.qtt
             SELECT ucrsvale
             DELETE FROM uCrsVale WHERE ALLTRIM(ucrsvale.fistamp)==ALLTRIM(fi.ofistamp)
             CALCULATE SUM(ucrsvale.qtt) TO lccount 
             IF lccount=0
                fecha("uCrsVale")
             ELSE
                GOTO TOP
             ENDIF
          ELSE
             SELECT ucrsvale
             REPLACE ucrsvale.qtt WITH ucrsvale.qtt-fi.qtt
             REPLACE ucrsvale.etiliquido WITH ucrsvale.qtt*ucrsvale.etiliquido2/ucrsvale.qtt2
          ENDIF
          SELECT fi
          uf_atendimento_marcademtotal(fi.dem, fi.design, fi.receita, fi.id_validacaodem, .F.)
          SELECT fi
          IF ALLTRIM(fi.ref)=='5681903' .OR. ALLTRIM(fi.ref)=='5771050'
             uf_novonordisk_remove(ALLTRIM(fi.fistamp))
          ENDIF
          SELECT fi
          uf_fi_trans_info_remove(ALLTRIM(fi.ref))
          SELECT fi
          uf_atendimento_fi2_linha_remove(ALLTRIM(fi.fistamp))
          SELECT fi
          uf_atendimento_fimancompart_linha_remove(ALLTRIM(fi.fistamp))
          SELECT fi
          DELETE
          lcvalida = .T.
          atendimento.grdatend.setfocus
       ELSE
          SELECT fi
          uf_atendimento_marcademtotal(fi.dem, fi.design, fi.receita, fi.id_validacaodem, .F.)
          SELECT fi
          IF ALLTRIM(fi.ref)=='5681903' .OR. ALLTRIM(fi.ref)=='5771050'
             uf_novonordisk_remove(ALLTRIM(fi.fistamp))
          ENDIF
          SELECT fi
          uf_fi_trans_info_remove(ALLTRIM(fi.ref))
          SELECT fi
          uf_atendimento_fi2_linha_remove(ALLTRIM(fi.fistamp))
          SELECT fi
          uf_atendimento_fimancompart_linha_remove(ALLTRIM(fi.fistamp))
          SELECT fi
          DELETE
          lcvalida = .T.
          atendimento.grdatend.setfocus
       ENDIF
    ELSE

       SELECT fi
       IF fi.epromo .AND.  .NOT. lcbool
          LOCAL lcvpos
          lcvpos = RECNO("FI")
          SELECT fi
          
          uf_atendimento_marcademtotal(fi.dem, fi.design, fi.receita, fi.id_validacaodem, .F.)
          SELECT fi
          IF ALLTRIM(fi.ref)=='5681903' .OR. ALLTRIM(fi.ref)=='5771050'
             uf_novonordisk_remove(ALLTRIM(fi.fistamp))
          ENDIF
          SELECT fi
          uf_fi_trans_info_remove(ALLTRIM(fi.ref))
          SELECT fi
          uf_atendimento_fi2_linha_remove(ALLTRIM(fi.fistamp))
          SELECT fi
          uf_atendimento_fimancompart_linha_remove(ALLTRIM(fi.fistamp))
          SELECT fi
          DELETE
          SELECT fi
          GOTO TOP
          SCAN FOR  .NOT. (LEFT(fi.design, 1)==".")
             REPLACE fi.u_descval WITH 0
          ENDSCAN
          lcvalidadelvale = .T.
       ELSE
          uf_atendimento_marcademtotal(fi.dem, fi.design, fi.receita, fi.id_validacaodem, .F.)
          SELECT fi
          IF ALLTRIM(fi.ref)=='5681903' .OR. ALLTRIM(fi.ref)=='5771050'
             uf_novonordisk_remove(ALLTRIM(fi.fistamp))
          ENDIF
          SELECT fi
          uf_fi_trans_info_remove(ALLTRIM(fi.ref))
          SELECT fi
          uf_atendimento_fi2_linha_remove(ALLTRIM(fi.fistamp))
          SELECT fi
          uf_atendimento_fimancompart_linha_remove(ALLTRIM(fi.fistamp))
          SELECT fi
          DELETE
       ENDIF
       lcvalida = .T.
       atendimento.grdatend.setfocus
    ENDIF
    IF  .NOT. lcbool
       uf_eventoactvalores()
    ENDIF
 ELSE
    SELECT fi
    uf_atendimento_marcademtotal(fi.dem, fi.design, fi.receita, fi.id_validacaodem, .F.)
    SELECT fi
    IF ALLTRIM(fi.ref)=='5681903' .OR. ALLTRIM(fi.ref)=='5771050'
       uf_novonordisk_remove(ALLTRIM(fi.fistamp))
    ENDIF
    SELECT fi
    uf_fi_trans_info_remove(ALLTRIM(fi.ref))
    SELECT fi
    uf_atendimento_fi2_linha_remove(ALLTRIM(fi.fistamp))
    SELECT fi
    uf_atendimento_fimancompart_linha_remove(ALLTRIM(fi.fistamp))
    SELECT fi
    DELETE
    lcvalida = .T.
    atendimento.grdatend.setfocus
 ENDIF

 IF lcvalida
    SELECT fi
    GOTO BOTTOM
    IF lcvalidadelvale
       uf_atendimento_transformavaledc(.F., .T.)
    ELSE
       uf_atendimento_transformavaledc(.F., .T., .T.)
    ENDIF
    SELECT dadospsico
    IF RECCOUNT("dadosPsico")>0
       SELECT dadospsico
       DELETE ALL
    ENDIF
    atendimento.grdatend.setfocus
 ENDIF
 uf_atendimento_infototais()
 SELECT fi
 LOCATE FOR ALLTRIM(fi.bistamp)==ALLTRIM(lcbistamp) .AND. UPPER(ALLTRIM(fi.ref))=='R000001' .AND.  .NOT. EMPTY(lcbistamp)
 IF FOUND() .AND.  .NOT. llreserva
 
 	LOCAL lcOfiStampTemp
    SELECT fi
    lcOfiStampTemp = ALLTRIM(fi.ofistamp)
    DELETE
    &&aqui
    
    DELETE FROM uCrsVale WHERE ALLTRIM(ucrsvale.fistamp)==lcOfiStampTemp
    uf_atendimento_infototais()
    atendimento.grdatend.setfocus
 ELSE
    SELECT fi
    TRY
       GOTO lcfiposreset+1
    CATCH
       GOTO lcfiposreset-1
    FINALLY
    ENDTRY
 ENDIF

 SELECT FI
 SELECT * FROM fi WITH (BUFFERING = .T.) WHERE fi.u_psico INTO CURSOR uc_fiPsico


 IF RECCOUNT("uc_fiPsico") == 0
	linpsico = .F.
	ATENDIMENTO.pgfDados.refresh()
 ENDIF

 FECHA("uc_fiPsico")

 SELECT FI
 GO TOP
 atendimento.grdatend.setfocus
  
 atendimento.lockscreen = .F.
ENDFUNC
**
PROCEDURE uf_atendimento_verificalordem
 LOCAL lccab, lcnprod, lcncab
 STORE 0 TO lccab, lcnprod
 STORE 1 TO lcncab
 SELECT fi
 lcpos = RECNO("fi")
 SELECT fi
 GOTO TOP
 SCAN
    IF LEFT(fi.design, 1)=="."
       SELECT fi
       REPLACE fi.lordem WITH lcncab*100000
       lccab = fi.lordem
       lcnprod = 0
       lcncab = lcncab+1
    ELSE
       lcnprod = lcnprod+1
       SELECT fi
       REPLACE fi.lordem WITH lccab+(100*lcnprod)
    ENDIF
 ENDSCAN
 SELECT fi
 TRY
    GOTO lcpos
 CATCH
    GOTO TOP
 ENDTRY
ENDPROC
**
PROCEDURE uf_atendimento_verificalordem2
 LOCAL lccab, lcnprod, lcncab
 STORE 0 TO lccab, lcnprod
 STORE 1 TO lcncab
 SELECT fi
 lcfistamp = fi.fistamp
 SELECT * FROM fi ORDER BY lordem INTO CURSOR ucrsFiTempAux READWRITE
 SELECT ucrsfitempaux
 GOTO TOP
 SCAN
    IF LEFT(ucrsfitempaux.design, 1)=="."
       SELECT ucrsfitempaux
       REPLACE ucrsfitempaux.lordem WITH lcncab*100000
       lccab = ucrsfitempaux.lordem
       lcnprod = 0
       lcncab = lcncab+1
    ELSE
       lcnprod = lcnprod+1
       SELECT ucrsfitempaux
       REPLACE ucrsfitempaux.lordem WITH lccab+(100*lcnprod)
    ENDIF
 ENDSCAN
 UPDATE fi FROM fi INNER JOIN ucrsFiTempAux ON fi.fistamp=ucrsfitempaux.fistamp SET fi.lordem = ucrsfitempaux.lordem
 SELECT fi
 LOCATE FOR fi.fistamp==lcfistamp
 IF  .NOT. FOUND()
    SELECT fi
    GOTO TOP
 ENDIF
ENDPROC
**
FUNCTION uf_atendimento_aplicapromo
 LPARAMETERS tcbool, tcnum, tcfo
 IF mypromocoes=.F.
    RETURN .F.
 ENDIF
 LOCAL lcdesconto
 SELECT fi
 lcdesconto = fi.desconto
 IF uf_gerais_getparameter('ADM0000000129', 'bool') 

 	
    IF TYPE("ATENDIMENTO")<>"U"
 		
       SELECT ucrsatendcl
       	
       	IF lcdesconto < ucrsatendcl.desconto
	       lcdesconto = ucrsatendcl.desconto
    	ENDIF 
   
       SELECT fi
       REPLACE fi.desconto WITH lcdesconto
    ELSE
       IF lcdesconto <  facturacao.descontocl
			lcdesconto = facturacao.descontocl
       ENDIF
 	
       SELECT fi
       REPLACE fi.desconto WITH lcdesconto
    ENDIF
 ENDIF
 

 
 
 LOCAL lcposicfi
 lcposicfi = RECNO("FI")
 uf_atendimento_fiiliq()
 SELECT fi
 TRY
    GOTO lcposicfi
 CATCH
 ENDTRY
 IF tcbool .AND.  .NOT. tcfo
    lcposicfi = RECNO("FI")
    uf_atendimento_eventofi()
    SELECT fi
    TRY
       GOTO lcposicfi
    CATCH
    ENDTRY
 ENDIF
 lcposicfi = RECNO("FI")
 uf_atendimento_infototais()
 SELECT fi
 TRY
    GOTO lcposicfi
 CATCH
 ENDTRY
ENDFUNC
**
FUNCTION uf_atendimento_alteraqtt
 LOCAL lcfilordem, lcposicfi
 IF  .NOT. EMPTY(fi.tpres)
    uf_perguntalt_chama("N�o � poss�vel alterar a quantidade nas linhas a reservar.", "OK", "", 48)
    RETURN .F.
 ENDIF
 lcposicfi = RECNO("FI")
 lcfilordem = LEFT(astr(fi.lordem), 2)
 uv_fistamp = fi.fistamp
 uv_planoCab = ""

 SELECT * from fi WITH (BUFFERING = .T.) ORDER BY lordem desc INTO CURSOR uc_tmpFI

 SELECT uc_tmpFI
 LOCATE FOR uc_tmpFi.fistamp = uv_fistamp
 IF FOUND()
   uv_lordem = uc_tmpFI.lordem
   SCAN FOR uc_tmpFI.lordem < uv_lordem

      IF LEFT(ALLTRIM(uc_tmpFI.design),1) = "."
         uv_cabFistamp = uc_tmpFI.fistamp

         IF USED("UCRSATENDCOMP")
            SELECT UCRSATENDCOMP

            LOCATE FOR ALLTRIM(UCRSATENDCOMP.fistamp) = ALLTRIM(uv_cabFistamp)
            IF FOUND()
               uv_planoCab = ALLTRIM(UCRSATENDCOMP.codigo)
            ENDIF
            
         ENDIF

         EXIT

      ENDIF

      SELECT uc_tmpFI
   ENDSCAN
 ENDIF

 FECHA("uc_tmpFI")

 SELECT fi
 GOTO TOP
 SCAN FOR ((LEFT(astr(fi.lordem), 2)=lcfilordem) .AND. (RIGHT(ALLTRIM(fi.design), 10)=="*SUSPENSA*") .OR. (RIGHT(ALLTRIM(fi.design), 18)=="*SUSPENSA RESERVA*"))
    IF FOUND()
       uf_perguntalt_chama("N�o � poss�vel alterar a quantidade nas linhas de venda suspensa.", "OK", "", 48)
       RETURN .F.
    ENDIF
 ENDSCAN
 SELECT fi
 GOTO lcposicfi
 IF fi.dem==.T. AND !uf_gerais_compStr(uf_gerais_retornaTipoDispensaEletronica(ALLTRIM(fi.receita)), "2")
    &&uf_perguntalt_chama("Aten��o ir� alterar a quantidade nas linhas de dispensa eletr�nica.", "OK", "", 64)  
    uf_perguntalt_chama("N�o � poss�vel alterar a quantidade nas linhas de dispensa eletr�nica.", "OK", "", 48)
    RETURN .F.
 ENDIF
 

 ** N�o pode alterar qtt depois de aplicar comparticipa��o manual - Mecofarma
 IF uf_gerais_getparameter_site('ADM0000000060', 'BOOL', mysite)
 	IF fi.u_txcomp<>100
 		uf_perguntalt_chama("N�o � poss�vel alterar a quantidade nas linhas com comparticipa��o j� aplicada.", "OK", "", 48)
	    RETURN .F.
 	ENDIF 
 ENDIF 
 
 IF uf_atendimento_validacampanhas()==.F.
    RETURN .F.
 ENDIF
 IF LEFT(fi.design, 1)=="."
    RETURN .F.
 ENDIF
 IF  .NOT. EMPTY(ALLTRIM(fi.bistamp))
    RETURN .F.
 ENDIF
 LOCAL ti, lcqtt1, lcqtt2, lcqttpos, lcvalida
 STORE 0 TO lcqtt1, lcqtt2, ti, lcqttpos
 STORE .F. TO lcvalida
 SELECT fi
 IF ALLTRIM(fi.ref)=="R000001"
    uf_perguntalt_chama("N�o pode alterar a quantidade do produto ap�s criar a reserva.", "OK", "", 48)
    RETURN .F.
 ENDIF
 SELECT fi
 lcqtt1 = fi.qtt
 uf_tecladonumerico_chama("fi.qtt", "Introduza a Quantidade:", 2, .F., .F., 6, 0)
 SELECT fi
 lcqtt2 = fi.qtt
 IF (lcqtt1<>lcqtt2)
    IF uf_gerais_getparameter_site('ADM0000000060', 'BOOL', mysite)
       LOCAL lcintbit, lcdecbit, lcconversao
       lcconversao = lcqtt2/ucrsatendst.qttminvd
       intbit = INT(lcconversao)
       decbit = (lcconversao-intbit)*10
       IF decbit<>0 .AND. ALLTRIM(ch_grupo)<>'Administrador' .AND. ALLTRIM(ch_grupo)<>'Supervisores'
          IF  .NOT. uf_gerais_valida_password_admin('ATENDIMENTO', 'ALTERA��O QTT')
             uf_perguntalt_chama("A quantidade inserida n�o � v�lida. Por favor verifique.", "OK", "", 64)
             SELECT fi
             REPLACE fi.qtt WITH lcqtt1
             Replace fi.u_descval WITH fi.u_descval* fi.qtt
             RETURN .F.
          ENDIF
       ENDIF
    ENDIF
    IF !EMPTY(uv_planoCab)
      IF !uf_faturacao_checkQttMaxPla(uv_planoCab, fi.qtt)
         SELECT fi
         REPLACE fi.qtt WITH lcqtt1
         Replace fi.u_descval WITH fi.u_descval* fi.qtt
         RETURN .F.
      ENDIF
    ENDIF
    IF fi.partes=0
       IF lcqtt2<=0
          uf_perguntalt_chama("A QUANTIDADE TEM DE SER SUPERIOR A 0.", "OK", "", 48)
          SELECT fi
          REPLACE fi.qtt WITH lcqtt1
          Replace fi.u_descval WITH fi.u_descval* fi.qtt
          RETURN .F.
       ENDIF
       IF (fi.u_stock-lcqtt2)<0
          IF  .NOT. uf_perguntalt_chama("Est� a colocar uma quantidade superior � quantidade que tem em stock."+CHR(13)+"Tem a certeza que pretende continuar?", "Sim", "N�o")
             SELECT fi
             REPLACE fi.qtt WITH lcqtt1
             Replace fi.u_descval WITH fi.u_descval* fi.qtt
             RETURN .F.
          ENDIF
       ENDIF
       IF ucrsatendst.qttcli>0
          IF (ucrsatendst.stock-ucrsatendst.qttcli-lcqtt2)<0
             IF  .NOT. uf_perguntalt_chama("Existe uma reserva aberta com quantidade "+ALLTRIM(STR(ucrsatendst.qttcli))+" para o produto que seleccionou."+CHR(13)+"Tem a certeza que pretende alterar quantidade?", "Sim", "N�o")
                SELECT fi
                REPLACE fi.qtt WITH lcqtt1
             Replace fi.u_descval WITH fi.u_descval* fi.qtt
                RETURN .F.
             ELSE
                IF uf_gerais_getparameter('ADM0000000066', 'BOOL')
                   PUBLIC cval
                   STORE '' TO cval
                   uf_tecladoalpha_chama("cval", "INTRODUZA PASSWORD PARA PERMITIR ALTERAR QTD DE PRODUTO RESERVADO:", .T., .F., 0)
                   IF EMPTY(cval)
                      uf_perguntalt_chama("DEVE INTRODUZIR A PASSWORD.", "OK", "", 64)
                      RELEASE cval
                      SELECT fi
                      REPLACE fi.qtt WITH lcqtt1
		              Replace fi.u_descval WITH fi.u_descval* fi.qtt
                      RETURN .F.
                   ELSE
                      IF  .NOT. (UPPER(ALLTRIM(cval))==UPPER(ALLTRIM(uf_gerais_getparameter('ADM0000000066', 'TEXT'))))
                         uf_perguntalt_chama("A PASSWORD N�O EST� CORRECTA.", "OK", "", 64)
                         RELEASE cval
                         SELECT fi
                         REPLACE fi.qtt WITH lcqtt1
             Replace fi.u_descval WITH fi.u_descval* fi.qtt
                         RETURN .F.
                      ELSE
                         RELEASE cval
                      ENDIF
                   ENDIF
                ENDIF
             ENDIF
          ENDIF
       ENDIF
       IF USED("uCrsVale")
          IF lcqtt2<=0
             uf_perguntalt_chama("N�O PODE ALTERAR A QUANTIDADE PARA '0' OU MENOR QUE '0' NUM PRODUTO A REGULARIZAR.", "OK", "", 48)
             SELECT fi
             REPLACE fi.qtt WITH lcqtt1
             Replace fi.u_descval WITH fi.u_descval* fi.qtt
             RETURN .F.
          ENDIF
          SELECT ucrsvale
          SCAN FOR ALLTRIM(ucrsvale.fistamp)==ALLTRIM(fi.ofistamp)
             IF fi.marcada .AND. fi.fmarcada==.F.
                IF lcqtt2>ucrsvale.qtt2-ROUND(ucrsvale.eaquisicao, 0)
                   uf_perguntalt_chama("A QUANTIDADE DE UM PRODUTO A REGULARIZAR N�O PODE SER SUPERIOR � QUANTIDADE A REGULARIZAR.", "OK", "", 48)
                   SELECT fi
                   REPLACE fi.qtt WITH lcqtt1
             Replace fi.u_descval WITH fi.u_descval* fi.qtt
                   RETURN .F.
                ENDIF
             ELSE
                IF lcqtt2>ucrsvale.qtt2
                   uf_perguntalt_chama("A QUANTIDADE DE UM PRODUTO A REGULARIZAR N�O PODE SER SUPERIOR � QUANTIDADE A REGULARIZAR.", "OK", "", 48)
                   SELECT fi
                   REPLACE fi.qtt WITH lcqtt1
             Replace fi.u_descval WITH fi.u_descval* fi.qtt
                   RETURN .F.
                ENDIF
             ENDIF
          ENDSCAN
          GOTO TOP
          SELECT fi
       ENDIF
       IF USED("uCrsVale")
          SELECT ucrsvale
          SCAN FOR ALLTRIM(ucrsvale.fistamp)==ALLTRIM(fi.ofistamp)
             REPLACE ucrsvale.qtt WITH fi.qtt
             REPLACE ucrsvale.etiliquido WITH fi.qtt*ucrsvale.etiliquido2/ucrsvale.qtt2
             IF ucrsvale.qtt<=ucrsvale.oeaquisicao
                REPLACE ucrsvale.eaquisicao WITH fi.qtt
             ELSE
                REPLACE ucrsvale.eaquisicao WITH ucrsvale.oeaquisicao
             ENDIF
          ENDSCAN
          GOTO TOP
          lcvalida = .T.
       ENDIF
    ELSE
       uf_perguntalt_chama("N�O PODE ALTERAR A QUANTIDADE DE UM PRODUTO MARCADO PARA DEVOLU��O.", "OK", "", 48)
       SELECT fi
       REPLACE fi.qtt WITH lcqtt1
             Replace fi.u_descval WITH fi.u_descval* fi.qtt
       RETURN .F.
    ENDIF
 ENDIF
 LOCAL lcposicfi
 SELECT fi
 lcposicfi = RECNO("FI")
 IF fi.marcada .AND. lcvalida

    uf_atendimento_eventofi(.T.)
 ELSE

    uf_atendimento_eventofi()
    
 ENDIF
 TRY
    GOTO lcposicfi
 CATCH
 ENDTRY
 uf_atendimento_infototais()
ENDFUNC
**
FUNCTION uf_atendimento_validavalegenerico
 LPARAMETERS lcref, lcservico
 IF (LEFT(lcref, 1))=="V" .AND. lcservico==.T.
    RETURN .T.
 ELSE
    RETURN .F.
 ENDIF
ENDFUNC
**
FUNCTION uf_atendimento_alterapvp
 LPARAMETERS lcfistamp

 IF TYPE("ATENDIMENTO")<>"U"
    IF uf_atendimento_validacampanhas()==.F.
       RETURN .F.
    ENDIF
    atendimento.tmroperador.reset()
 ENDIF

 IF TYPE("ATENDIMENTO")<>"U"
    IF  .NOT. EMPTY(fi.tpres)
       uf_perguntalt_chama("N�o � poss�vel alterar pre�os nas linhas a reservar.", "OK", "", 48)
       RETURN .F.
    ENDIF
 ENDIF

 SELECT fi
 IF (fi.epromo==.T. .AND. uf_atendimento_validavalegenerico(UPPER(ALLTRIM(fi.ref)), fi.stns))
    uf_perguntalt_chama("N�O PODE ALTERAR O VALOR DE UM VALE DE DESCONTO GEN�RICO.", "OK", "", 48)
    RETURN .F.
 ENDIF

 SELECT fi
 IF ALLTRIM(fi.ref)=="R000001"
    uf_perguntalt_chama("N�o pode alterar o PVP do produto ap�s criar a reserva.", "OK", "", 48)
    RETURN .F.
 ENDIF

 IF TYPE("ATENDIMENTO")<>"U"
    IF  .NOT. uf_gerais_getparameter('ADM0000000072', 'BOOL')==.T.
       uf_perguntalt_chama("O sistema est� configurado para n�o permitir alterar o PVP.", "OK", "", 64)
       RETURN .F.
    ENDIF
 ENDIF

 IF  .NOT. EMPTY(fi.ofistamp)
    IF TYPE("FACTURACAO")<>"U"
       SELECT ft
       IF ft.tipodoc==2 .OR. ft.tipodoc==3
          uf_perguntalt_chama("N�O PODE ALTERAR PVP'S DE PRODUTOS A REGULARIZAR.", "OK", "", 64)
          RETURN .F.
       ENDIF
    ELSE
       uf_perguntalt_chama("N�O PODE ALTERAR PVP'S DE PRODUTOS A REGULARIZAR.", "OK", "", 64)
       RETURN .F.
    ENDIF
 ENDIF

 LOCAL ti, lcpvppos, lcepvpos, lcpvp, lcpvp2
 STORE 0 TO ti, lcpvppos, lcepvpos, lcpvp, lcpvp2
 SELECT fi
 lcpvp = fi.u_epvp
 SELECT fi
 IF  .NOT. (ALLTRIM(fi.familia)=='1') .AND.  .NOT. (ALLTRIM(fi.familia)=='2') .AND.  .NOT. (ALLTRIM(fi.familia)=='10') .AND.  .NOT. (ALLTRIM(fi.familia)=='23') .AND.  .NOT. (ALLTRIM(fi.familia)=='58')
    IF  .NOT. EMPTY(ALLTRIM(uf_gerais_getparameter('ADM0000000072', 'TEXT')))
       PUBLIC cval
       STORE '' TO cval
       uf_tecladoalpha_chama("cval", "INTRODUZA PASSWORD PARA ALTERA��O DE PVP'S:", .T., .F., 0)
       IF EMPTY(cval)
          uf_perguntalt_chama("DEVE INTRODUZIR A PASSWORD DE SUPERVISOR PARA ALTERAR PVP.", "OK", "", 64)
          RETURN .F.
       ELSE
          IF  .NOT. (UPPER(ALLTRIM(cval))==UPPER(ALLTRIM(uf_gerais_getparameter('ADM0000000072', 'TEXT'))))
             uf_perguntalt_chama("A PASSWORD DE SUPERVISOR N�O EST� CORRECTA.", "OK", "", 64)
             RETURN .F.
          ENDIF
       ENDIF
    ENDIF
    uf_tecladonumerico_chama("fi.u_epvp", "Introduza o PVP:", 2, .T., .F., 9, 2)
    IF (fi.epromo==.T.)
       IF TYPE("ATENDIMENTO")<>"U"
          uf_atendimento_transformavaledc(.F., .F., .F.)
       ELSE
          uf_atendimento_transformavaledc(.T.)
       ENDIF
       RETURN .F.
    ENDIF
 ELSE
    IF TYPE("FACTURACAO")<>"U" .AND. ft.cdata<DATE()
       lcsql = ''
       TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_stocks_hpr '<<ALLTRIM(fi.ref)>>', 3, '<<uf_gerais_getdate(ft.cdata,"SQL")>>'
       ENDTEXT
       IF  .NOT. (uf_gerais_actgrelha("", "uCrsPicValidoData", lcsql))
          uf_perguntalt_chama("Ocorreu uma anomalia a verificar pre�os dispon�veis para o produto. Por favor Contacte o Suporte.", "OK", "", 16)
          RETURN .F.
       ELSE
          uf_valorescombo_chama("fi.u_epvp", 2, "uCrsPicValidoData", 2, "PIC", "PIC, data_inicio, data_fim, DATA_FIM_ESCOAMENTO", .F., .F., .T., .T., .F., .T.)
       ENDIF
    ELSE
    	IF USED("ucrsMergeSearch") AND RECCOUNT("ucrsMergeSearch")>0
    		uf_atendimento_precosSpms_grelhaPrecosValidos(ALLTRIM(fi.ref))
    	ELSE
	       lcsql = ''
	       TEXT TO lcsql TEXTMERGE NOSHOW
					exec up_stocks_hpr '<<ALLTRIM(fi.ref)>>', 1
	       ENDTEXT
	       IF  .NOT. (uf_gerais_actgrelha("", "uCrsPicValido", lcsql))
	          uf_perguntalt_chama("Ocorreu uma anomalia a verificar pre�os dispon�veis para o produto. Por favor Contacte o Suporte.", "OK", "", 16)
	          RETURN .F.
	       ELSE
	          uf_valorescombo_chama("fi.u_epvp", 2, "uCrsPicValido", 2, "PIC", "PIC, PRECO_VALIDO, DATA_FIM_ESCOAMENTO", .F., .F., .T., .T., .F., .T.)
	       ENDIF
	     ENDIF
    ENDIF
 ENDIF
 SELECT fi
 lcpvp2 = fi.u_epvp
 IF (lcpvp<>lcpvp2)
    IF lcpvp2<0
       uf_perguntalt_chama("N�O PODE ALTERAR O PVP PARA NEGATIVO.", "OK", "", 48)
       SELECT fi
       REPLACE fi.u_epvp WITH lcpvp
       RETURN .F.
    ENDIF
    IF (lcpvp2<fi.u_descval)
       uf_perguntalt_chama("N�O PODE ALTERAR O PVP PARA UM VALOR INFERIOR AO DESCONTO EM VALOR.", "OK", "", 48)
       SELECT fi
       REPLACE fi.u_epvp WITH lcpvp
       RETURN .F.
    ENDIF
    IF (lcpvp2>200) .AND. UPPER(ALLTRIM(ucrse1.pais))=='PORTUGAL'
       IF  .NOT. uf_perguntalt_chama("IR� A ALTERAR O PVP PARA UM VALOR SUPERIOR A 200�"+CHR(10)+CHR(10)+"TEM A CERTEZA QUE PRETENDE CONTINUAR?", "Sim", "N�o")
          SELECT fi
          REPLACE fi.u_epvp WITH lcpvp
          RETURN .F.
       ENDIF
    ENDIF
    IF  .NOT. (fi.opcao=='I')
       IF (lcpvp2>fi.pvpmaxre)
          SELECT fi
          REPLACE fi.opcao WITH 'S'
       ELSE
          SELECT fi
          REPLACE fi.opcao WITH 'N'
       ENDIF
    ENDIF
    SELECT fi
    REPLACE fi.amostra WITH .T. IN fi
    SELECT fi
    REPLACE fi.epv WITH fi.u_epvp
    IF UPPER(ALLTRIM(ucrse1.pais))<>'PORTUGAL'
       REPLACE fi.epvori WITH fi.u_epvp
    ENDIF
    uf_atendimento_fiiliq()
    LOCAL lcposicfi
    lcposicfi = RECNO("FI")
    uf_atendimento_eventofi()
    TRY
       GOTO lcposicfi
    CATCH
    ENDTRY
    IF UPPER(ALLTRIM(ucrse1.pais))=='PORTUGAL'
       IF uf_perguntalt_chama("Pretende atualizar a ficha do Produto com o PVP introduzido ?", "Sim", "N�o")
          
       	  lcsql = ''
       	  
       	  fecha("ucrsTempSt")
       	  
          TEXT TO lcsql TEXTMERGE NOSHOW
				SELECT 
					ISNULL(ststamp,'') as ststamp, 
					ISNULL(epv1,0) as epv1,0
                FROM  
                	ST (nolock)		
                WHERE 
                  ref = '<<ALLTRIM(fi.ref)>>' AND site_nr = <<mysite_nr>>				
          ENDTEXT
          IF  .NOT. (uf_gerais_actgrelha("", "ucrsTempSt", lcsql))
             uf_perguntalt_chama("Ocorreu uma anomalia ao atualizar a ficha do Produto. Por favor contacte o Suporte.", "OK", "", 16)
          ENDIF
          
       
          SELECT fi
          lcsql = ''
          TEXT TO lcsql TEXTMERGE NOSHOW
					UPDATE 
                  ST 
               SET 
                  epv1 = '<<lcPvp2>>', 
                  usrdata = CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), 
                  usrhora = CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108),
                  usrinis = '<<m_chinis>>'
               WHERE 
                  ref = '<<ALLTRIM(fi.ref)>>' AND site_nr = <<mysite_nr>>				
          ENDTEXT
          IF  .NOT. (uf_gerais_actgrelha("", "", lcsql))
             uf_perguntalt_chama("Ocorreu uma anomalia ao atualizar a ficha do Produto. Por favor contacte o Suporte.", "OK", "", 16)
          ENDIF
          uf_gerais_registaocorrencia('Vendas', 'Altera��o de PVP Ficha na Venda', 2, ALLTRIM(fi.ref)+' PVP: '+astr(fi.epvori, 8, 2), ALLTRIM(fi.ref)+' PVP: '+astr(fi.u_epvp, 8, 2), ALLTRIM(fi.fistamp), '', '', '')
          

          
         ** REGISTO CONSULTA NOS LOGS DE UTILIZADOR
         if(USED("ucrsTempSt"))
         	 LOCAL lcdados
         	 LOCAL formatMoney
         	 formatMoney = "99999999999.99"
         	 
         	 lcdados= 'Campo: epv1 - Valor original: '  + alltrim(transform(ucrsTempSt.epv1,formatMoney)) + ' - Valor alterado: ' + alltrim(transform(lcPvp2,formatMoney)) + CHR(13)
	         SELECT ucrsTempSt
	         GO TOP
			 uf_user_log_ins('ST', ALLTRIM(ucrsTempSt.ststamp), 'ALTERA��O', lcdados)			 
			 fecha("ucrsTempSt")
		 ENDIF
		 
          
       ENDIF
    ENDIF
 ENDIF
 IF TYPE("FACTURACAO")=="U"
	LOCAL uv_corPvp
    SELECT fi
    uv_corPvp = uf_atendimento_alteracorcelulapvp()
	SELECT fi
    REPLACE fi.cor WITH uv_corPvp
 ENDIF
 IF TYPE("ATENDIMENTO")<>"U"
    uf_atendimento_infototais()
    atendimento.grdatend.setfocus()
 ELSE
    uf_atendimento_actvaloresfi()
    uf_atendimento_acttotaisft()
 ENDIF
ENDFUNC





**
FUNCTION uf_atendimento_moverlinhas
 LPARAMETERS tcbool
 IF  .NOT. (TYPE("ATENDIMENTO")=="U")
    atendimento.tmroperador.reset()
 ENDIF
 LOCAL lcfistamprecalcula, lclordem1, lcfistamp1, lclordem2, lcfistamp2
 STORE "" TO lcfistamprecalcula, lclordem1, lcfistamp1, lclordem2, lcfistamp2
 SELECT fi
 IF (LEFT(fi.design, 1)==".") .AND. (fi.lordem==100000)
    uf_perguntalt_chama("ATEN��O: N�O � POSS�VEL MOVER O CABE�ALHO DA 1� VENDA.", "OK", "", 64)
    RETURN .F.
 ENDIF
 IF (LEFT(fi.design, 1)==".") .AND. ( .NOT. EMPTY(fi.tkhposlstamp))
    uf_perguntalt_chama("ATEN��O:  N�O PODE MOVER UMA LINHA DEM", "OK", "", 64)
    RETURN .F.
 ENDIF
 IF ( .NOT. EMPTY(fi.dem))
    uf_perguntalt_chama("ATEN��O: N�O � POSS�VEL MOVER UMA LINHA DEM", "OK", "", 64)
    RETURN .F.
 ENDIF
 IF tcbool==.F.
    lclordem1 = fi.lordem
    lcfistamp1 = fi.fistamp
    IF USED("ucrsFiTempAux")
       fecha("ucrsFiTempAux")
    ENDIF
    SELECT TOP 1 lordem, fistamp, design, tkhposlstamp FROM fi WHERE lordem<lclordem1 ORDER BY lordem DESC INTO CURSOR ucrsFiTempAux READWRITE
    SELECT ucrsfitempaux
    IF RECCOUNT("ucrsFiTempAux")>0
       lclordem2 = ucrsfitempaux.lordem
       lcfistamp2 = ucrsfitempaux.fistamp
       IF (LEFT(ucrsfitempaux.design, 1)==".") .AND. (ucrsfitempaux.lordem==100000)
          uf_perguntalt_chama("ATEN��O: N�O � POSS�VEL MOVER O CABE�ALHO DA 1� VENDA.", "OK", "", 64)
          RETURN .F.
       ENDIF
       
       uf_atendimento_adicionalinhafimancompart()
       
       UPDATE fi SET lordem = lclordem1 WHERE ALLTRIM(fi.fistamp)=ALLTRIM(lcfistamp2)
       UPDATE fi SET lordem = lclordem2 WHERE ALLTRIM(fi.fistamp)=ALLTRIM(lcfistamp1)
      
       
    ENDIF
 ELSE
    lclordem1 = fi.lordem
    lcfistamp1 = fi.fistamp
    IF USED("ucrsFiTempAux")
       fecha("ucrsFiTempAux")
    ENDIF
    SELECT TOP 1 lordem, fistamp, design, tkhposlstamp FROM fi WHERE lordem>lclordem1 ORDER BY lordem INTO CURSOR ucrsFiTempAux READWRITE
    SELECT ucrsfitempaux
    IF RECCOUNT("ucrsFiTempAux")>0
       lclordem2 = ucrsfitempaux.lordem
       lcfistamp2 = ucrsfitempaux.fistamp
       IF (LEFT(ucrsfitempaux.design, 1)==".") .AND. (ucrsfitempaux.lordem==100000)
          uf_perguntalt_chama("ATEN��O: N�O � POSS�VEL MOVER O CABE�ALHO DA 1� VENDA.", "OK", "", 64)
          RETURN .F.
       ENDIF
       
       uf_atendimento_adicionalinhafimancompart()
       
       
       UPDATE fi SET lordem = lclordem1 WHERE ALLTRIM(fi.fistamp)=ALLTRIM(lcfistamp2)
       UPDATE fi SET lordem = lclordem2 WHERE ALLTRIM(fi.fistamp)=ALLTRIM(lcfistamp1)

    ENDIF
 ENDIF
 IF  .NOT. EMPTY(ALLTRIM(lcfistamp2))
    SELECT fi
    LOCATE FOR ALLTRIM(fi.fistamp)==ALLTRIM(lcfistamp2)
    IF FOUND()
       IF ALLTRIM(LEFT(fi.design, 1))<>"." .AND. (ALLTRIM(fi.ref)<>'5681903' .AND. ALLTRIM(fi.ref)<>'5771050')
          lccod = uf_atendimento_verificacompart()
          IF  .NOT. EMPTY(lccod)
             SELECT fi
             REPLACE fi.u_comp WITH .T.
          ELSE
             SELECT fi
             REPLACE fi.u_comp WITH .F.
          ENDIF
          uf_eventoactvalores()
       ENDIF
    ELSE
       SELECT fi
       GOTO TOP
    ENDIF
 ENDIF
  
 
 IF  .NOT. EMPTY(ALLTRIM(lcfistamp1))
    SELECT fi
    LOCATE FOR ALLTRIM(fi.fistamp)==ALLTRIM(lcfistamp1)
    IF FOUND()
       IF ALLTRIM(LEFT(fi.design, 1))<>"."
          lccod = uf_atendimento_verificacompart()
          IF  .NOT. EMPTY(lccod)
             SELECT fi
             REPLACE fi.u_comp WITH .T.
          ELSE
             SELECT fi
             REPLACE fi.u_comp WITH .F.
          ENDIF
          uf_eventoactvalores()
       ENDIF
    ELSE
       SELECT fi
       GOTO TOP
    ENDIF
 ENDIF
 uf_atendimento_verificalordem2()
 
 
  
 if(USED("fimancompart")) 
     UPDATE fimancompart SET fimancompart.lordem = fi.lordem from  fimancompart  inner join fi on fi.fistamp = fimancompart.fistamp 
 endif  
 
 IF  .NOT. (TYPE("ATENDIMENTO")=="U")
    atendimento.grdatend.refresh
    atendimento.grdatend.setfocus
 ENDIF
  
ENDFUNC


**
FUNCTION uf_atendimento_validasedemconsultada
 LPARAMETERS lcdem, lcreceita
 IF TYPE("myDemDispensada")<>"U"
    IF (mydemdispensada==.T.)
       RETURN .T.
    ENDIF
 ENDIF
 RETURN .F.
ENDFUNC
**
FUNCTION uf_atendimento_retorna_codunicoembalagem
 LPARAMETERS lcautorizationcode
 IF (USED("ucrsTempAuthCodeEmb"))
    fecha("ucrsTempAuthCodeEmb")
 ENDIF
 CREATE CURSOR ucrsTempAuthCodeEmb (tipo NUMERIC(5, 0), authcode VARCHAR(254))
 IF (LEN(lcautorizationcode)<20)
    INSERT INTO ucrsTempAuthCodeEmb VALUES (1, lcautorizationcode)
    RETURN .T.
 ENDIF
 IF EMPTY(uf_gerais_retorna_campos_datamatrix_sql(ALLTRIM(lcautorizationcode)))
    uf_perguntalt_chama("Codigo de autoriza��o / �nico da embalagem inv�lido, por favor, coloque manualmente.", "OK", "", 16)
    INSERT INTO ucrsTempAuthCodeEmb VALUES (1, "")
    RETURN .T.
 ELSE
    LOCAL lcpacksn
    SELECT ucrdatamatrixtemp
    GOTO TOP
    lcpacksn = ALLTRIM(ucrdatamatrixtemp.sn)
    fecha("ucrDataMatrixTemp")
    INSERT INTO ucrsTempAuthCodeEmb VALUES (2, lcpacksn)
    RETURN .T.
 ENDIF
 RETURN .T.
ENDFUNC
**
FUNCTION uf_atendimento_scanner
 LPARAMETERS lcobriganrreceita, lcrefParam
 
 ** os datamatrixs n�o devem ser editados > 25 de caracters
 if(LEN(ALLTRIM(atendimento.txtscanner.value))) <25
 	atendimento.txtscanner.value = STRTRAN(atendimento.txtscanner.value, CHR(39), '')
 ENDIF
 

 IF EMPTY(ALLTRIM(atendimento.txtscanner.value))
    RETURN .F.
 ENDIF

 LOCAL lcModoConferencia
 lcModoConferencia= .f.
 
  IF   uf_atendimento_validacampanhas()==.F. && ignorar se for conferencia de productos
    RETURN .F.
 ENDIF
 ALINES(refnpesq, uf_gerais_getparameter("ADM0000000219", "TEXT"), ";")
 FOR i = 1 TO ALEN(refnpesq)
    IF UPPER(ALLTRIM(refnpesq(i)))==UPPER(ALLTRIM(atendimento.txtscanner.value))
       RETURN .F.
    ENDIF
 ENDFOR
 LOCAL lcref, lcutente
 LOCAL lcvalida, lcvalidaval, lcvalidavalref, lcvalidacl, lcvalidainativo, uv_serialNumber
 STORE .F. TO lcvalida, lcvalidaval, lcvalidavalref, lcvalidacl, lcvalidainativo
 STORE 0 TO lcref, lcutente
 lcref = UPPER(ALLTRIM(atendimento.txtscanner.value))
 IF !EMPTY(lcrefParam)
	lcref = lcrefParam
 ENDIF

 uv_serialNumber = ""
 uv_dMatrix = lcRef

 LOCAL lcMax
 lcMax = 100
 
 LOCAL lcEan
 lcEan = 14

 uv_qrCode = uf_gerais_getFtFromQR(ALLTRIM(lcRef))
 DO CASE
	CASE !EMPTY(uv_qrCode)
		uv_numAtend = ALLTRIM(uf_gerais_getUmValor("ft", "u_nratend", "ftstamp = '" + ALLTRIM(uv_qrCode) + "'"))

		TEXT TO lcsql TEXTMERGE NOSHOW
			SELECT TOP 1 fdata FROM ft (nolock) WHERE ft.u_nratend='<<uv_numAtend>>' ORDER BY ft.fdata desc
		ENDTEXT
		uf_gerais_actgrelha("", "uCrsDataReg", lcsql)
		SELECT ucrsdatareg
		GOTO TOP
		uf_regvendas_chama(.F., ALLTRIM(uv_numAtend))
		regvendas.txtnatend.value = uv_numAtend
		regvendas.txtdataini.value = uf_gerais_getdate(ucrsdatareg.fdata)
		regvendas.txtdatafim.value = uf_gerais_getdate(ucrsdatareg.fdata)
		regvendas.menu1.actualizar.click()


    CASE LEN(lcref)>20 AND EMPTY(uv_qrCode)
       LOCAL lceanid, lcdtvalid, lcloteid, lcidid, lcrefid, lcrefpaisid, lcreftempid, lcposdelimiterid, lcconfereid, lcleituracampos
       STORE '' TO lceanid, lcdtvalid, lcloteid, lcidid, lcrefid, lcrefpaisid, lcleituracampos
       STORE 0 TO lcposdelimiterid
       lcreftempid = (ALLTRIM(atendimento.txtscanner.value))
       lcconfereid = (ALLTRIM(atendimento.txtscanner.value))
       
    
       lcreftempid = STRTRAN(lcreftempid , "'", "-")
       lcconfereid = STRTRAN(lcconfereid , "'", "-")
       
       lcreftempid = uf_gerais_remove_last_caracter_by_char(lcreftempid,'#')      
       lcconfereid = uf_gerais_remove_last_caracter_by_char(lcconfereid,'#')
       
        IF uf_gerais_retorna_campos_datamatrix_sql(lcreftempid)
          
         SELECT ucrDataMatrixTemp
         IF ucrDataMatrixTemp.valido

            lcDtValID = uf_gerais_retornaApenasAlphaNumericos(ucrDataMatrixTemp.data)
            lcRefID  = uf_gerais_retornaApenasAlphaNumericos(ucrDataMatrixTemp.ref)
            lcrefpaisid = ucrDataMatrixTemp.countryCode
            lceanid = ucrDataMatrixTemp.pc
            lcloteid = ucrDataMatrixTemp.lote
            lcidid = ucrDataMatrixTemp.sn

            SELECT fi_trans_info
            GO TOP

            LOCATE FOR uf_gerais_compStr(lceanid, fi_trans_info.productcode) AND uf_gerais_compStr(lcidid, fi_trans_info.packserialnumber)

            IF FOUND() .AND. atendimento.menu1.conferir.tag=="false"

               uf_perguntalt_chama("Este pack j� foi conferido.", "OK", "", 64)
               RETURN .F.

            ENDIF


         ENDIF
         
       ENDIF
       
       IF EMPTY(lcrefid)

          uf_gerais_notif("Medicamento conferido." + chr(13) + "Datamatrix n�o desativado.", 3000)
          uf_gerais_registaerrolog("Erro ao ler o datamatrix no uf_atendimento_scanner:  " +  uv_dMatrix, "datamatrix")

          
          lcrefid = uf_gerais_getFromString(uv_dMatrix, '714', 7, .T.)
          IF LEN(lcrefID) <> 7
            uf_gerais_registaerrolog("Artigo n�o encontrado: " +  uv_dMatrix, "datamatrixCnp")
            uf_perguntalt_chama("ERRO NA LEITURA OU C�DIGO DE BARRAS INV�LIDO.", "OK", "", 64)
            RETURN .F.
          ENDIF

       ENDIF
       
       LOCAL lcjalida
       lcjalida = .F.
       SELECT fi_trans_info
       GOTO TOP
       **SCAN
       **   IF !EMPTY(lcidid) AND ALLTRIM(fi_trans_info.productcode)==ALLTRIM(lceanid) .AND. ALLTRIM(fi_trans_info.packserialnumber)==ALLTRIM(lcidid) .AND. atendimento.menu1.conferir.tag=="false"
       **      uf_perguntalt_chama("ESTA EMBALAGEM J� FOI REGISTADA!", "OK", "", 64)
       **      RETURN (.F.)
       **   ENDIF
       **ENDSCAN
       LOCAL lcatribuiu
       lcatribuiu = .F.
       SELECT fi_trans_info
       GOTO TOP
       lcref = lcrefid
       lcsql = ""
       TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_stocks_procuraRef '<<ALLTRIM(lcRef)>>', <<mysite_nr>>
       ENDTEXT
       IF uf_gerais_actgrelha("", "uCrsFindRef", lcsql) .AND. RECCOUNT()>0
          IF ucrsfindref.inactivo==.F.
             SELECT ucrsfindref
             IF ucrsfindref.qttcli>0 .AND. atendimento.menu1.conferir.tag=="false"
                IF (ucrsfindref.stock-ucrsfindref.qttcli)<=0
                   IF  .NOT. uf_perguntalt_chama("Existe uma reserva aberta com quantidade "+ALLTRIM(STR(ucrsfindref.qttcli))+" para o produto que seleccionou."+CHR(13)+"Tem a certeza que pretende adicion�-lo ao atendimento?", "Sim", "N�o")
                      atendimento.txtscanner.value = ""
                      RETURN .F.
                   ELSE
                      IF uf_gerais_getparameter('ADM0000000066', 'BOOL')
                         PUBLIC cval
                         STORE '' TO cval
                         uf_tecladoalpha_chama("cval", "INTRODUZA PASSWORD PARA PERMITIR SELECCIONAR PRODUTO RESERVADO:", .T., .F., 0)
                         IF EMPTY(cval)
                            uf_perguntalt_chama("DEVE INTRODUZIR A PASSWORD.", "OK", "", 64)
                            RELEASE cval
                            atendimento.txtscanner.value = ""
                            RETURN .F.
                         ELSE
                            IF  .NOT. (UPPER(ALLTRIM(cval))==UPPER(ALLTRIM(uf_gerais_getparameter('ADM0000000066', 'TEXT'))))
                               uf_perguntalt_chama("A PASSWORD N�O EST� CORRECTA.", "OK", "", 64)
                               RELEASE cval
                               atendimento.txtscanner.value = ""
                               RETURN .F.
                            ELSE
                               RELEASE cval
                            ENDIF
                         ENDIF
                      ENDIF
                   ENDIF
                ENDIF
             ENDIF
           
             IF atendimento.menu1.conferir.tag=="true"
                uf_embal_conferereferencia(lcconfereid)
                SELECT ucrsfindref
                uf_atendimento_conferereferencia(ucrsfindref.ref)
                atendimento.txtscanner.value = ''
                atendimento.grdatend.setfocus
                IF ALLTRIM(lcleituracampos)=='ref'
                   uf_infoembal_chama()
                ENDIF
                        	
                IF ucrse1.obrigaconf=.T.
                   IF uf_atendimento_todos_dem_conferidos()
                      uf_atendimento_salvar_cancelar()
                   ELSE
                      uf_atendimento_conferindo_limpar()
                   ENDIF
                ELSE
                   IF uf_atendimento_todos_conferidos()
                      uf_atendimento_salvar_cancelar()
                   ENDIF
                ENDIF
                IF VARTYPE(lcrecnoconf)<>'U'
                   SELECT fi
                   TRY
                      GOTO lcrecnoconf
                   CATCH
                   ENDTRY
                   REPLACE fi.design WITH ALLTRIM(fi.design)
                   atendimento.grdatend.refresh
                   atendimento.grdatend.setfocus
                ENDIF
                RETURN .F.
             ENDIF
             SELECT fi
             IF  .NOT. EMPTY(fi.dem) .AND. atendimento.tmrconsultadem.enabled==.F. .AND.  .NOT. EMPTY(fi.stns) .AND.  .NOT. EMPTY(fi.epromo)
                LOCAL lcdemconsultada
                STORE .T. TO lcdemconsultada
                lcdemconsultada = uf_atendimento_validasedemconsultada()
                IF (lcdemconsultada==.F.)
                   uf_atendimento_novavenda(.T.)
                ENDIF
             ENDIF
             uf_atendimento_adicionalinhafi(.F., .F.)
             SELECT fi
             lcposfi = RECNO("fi")
             REPLACE fi.ref WITH ALLTRIM(ucrsfindref.ref)
             uf_atendimento_actref()
             SELECT fi
             TRY
                GOTO lcposfi
             CATCH
                GOTO BOTTOM
             ENDTRY
             SELECT fi
             IF usamancompart
               IF TYPE("ATENDIMENTO") <> "U"
                  uf_atendimento_aplicaTabCompartAddLine()
               ENDIF
                uf_atendimento_adicionalinhafimancompart()
             ENDIF
             uf_atendimento_eventofi()
             SELECT fi
             TRY
                GOTO lcposfi
             CATCH
                GOTO BOTTOM
             ENDTRY
             uf_atendimento_dadosst()
             SELECT fi
             TRY
                GOTO lcposfi
             CATCH
                GOTO BOTTOM
             ENDTRY
             IF uf_gerais_getparameter("ADM0000000192", "BOOL")
                SELECT ucrsatendcl
                IF ucrsatendcl.no<>200
                   IF  .NOT. EMPTY(ALLTRIM(ucrsatendcl.codpla)) .AND. (uf_gerais_getdate(ucrsatendcl.valipla, "SQL")=="19000101" .OR. ucrsatendcl.valipla>=DATE())
                      SELECT fi
                      IF RECCOUNT("fi")>1
                         uf_atendimento_aplicaplanoauto()
                      ENDIF
                   ENDIF
                ENDIF
                SELECT fi
                TRY
                   GOTO lcposfi
                CATCH
                   GOTO BOTTOM
                ENDTRY
             ENDIF
             LOCAL lcinfoplano
             STORE '' TO lcinfoplano
             lcinfoplano = uf_atendimento_compapoioespecial()
             IF  .NOT. EMPTY(lcinfoplano) .AND. (ALLTRIM(fi.ref)<>'5681903' .AND. ALLTRIM(fi.ref)<>'5771050')
                uf_perguntalt_chama("Aten��o, este produto pode ser comparticipado ao abrigo do programa de apoio especial: "+lcinfoplano+".", "OK", "", 64)
             ENDIF
             IF uf_gerais_getparameter("ADM0000000074", "BOOL")
                SELECT ucrsfindref
                IF ucrsfindref.mfornec>0
                   uf_atendimento_alteradesc(.T., ucrsfindref.mfornec, ucrsfindref.dataFimPromo, ucrsfindref.dataIniPromo)
                ELSE
                   IF ucrsfindref.mfornec2>0
                      uf_atendimento_alteradescval(.T., ucrsfindref.mfornec2, ucrsfindref.dataFimPromo, ucrsfindref.dataIniPromo)
                   ENDIF
                ENDIF
             ENDIF
             LOCAL lcfistampid
             SELECT fi
             lcfistampid = fi.fistamp
            SELECT FI2 
            GO TOP
            LOCATE FOR fi2.fistamp = lcfistampid
            IF FOUND()
               REPLACE fi2.productcodeEmbal WITH ALLTRIM(lceanid)
            ENDIF
             SELECT fi_trans_info
             GOTO BOTTOM
             APPEND BLANK
             REPLACE fi_trans_info.token WITH ALLTRIM(nratendimento)
             REPLACE fi_trans_info.recStamp WITH ALLTRIM(lcfistampid)
             REPLACE fi_trans_info.recTable WITH 'FT'
             REPLACE fi_trans_info.productcode WITH  lceanid
             REPLACE fi_trans_info.productcodescheme WITH 'GTIN'
             REPLACE fi_trans_info.batchid WITH lcloteid
            
            && colocado para os pmh	
		     IF !EMPTY(lcloteid) AND NOT ISNULL(lcloteid)
		        SELECT FI
		        REPLACE fi.lote WITH lcloteid
		     ENDIF
				
			IF !EMPTY(lcDtValID) AND NOT ISNULL(lcDtValID)
				IF ALLTRIM(SUBSTR(lcdtvalid, 5, 2))<>'00' and !EMPTY(ALLTRIM(SUBSTR(lcdtvalid, 5, 2)))
					SELECT FI2
					REPLACE fi2.dataValidade WITH DATE(2000+VAL(SUBSTR(lcdtvalid, 1, 2)), VAL(SUBSTR(lcdtvalid, 3, 2)), VAL(SUBSTR(lcdtvalid, 5, 2)))
				ELSE
					SELECT FI2
					REPLACE fi2.dataValidade WITH GOMONTH(DATE(2000+VAL(SUBSTR(lcdtvalid, 1, 2)), VAL(SUBSTR(lcdtvalid, 3, 2)), 1), 1)-1
				ENDIF
		     ENDIF
	            
             
             if(!EMPTY(uf_gerais_eFormatoDataValidoDataMatrix(lcdtvalid)))
             	  SET CENTURY ON	 
	             IF ALLTRIM(SUBSTR(lcdtvalid, 5, 2))<>'00' and !EMPTY(ALLTRIM(SUBSTR(lcdtvalid, 5, 2)))
	                REPLACE fi_trans_info.batchexpirydate WITH DATE(2000+VAL(SUBSTR(lcdtvalid, 1, 2)), VAL(SUBSTR(lcdtvalid, 3, 2)), VAL(SUBSTR(lcdtvalid, 5, 2)))
	             ELSE
	                REPLACE fi_trans_info.batchexpirydate WITH GOMONTH(DATE(2000+VAL(SUBSTR(lcdtvalid, 1, 2)), VAL(SUBSTR(lcdtvalid, 3, 2)), 1), 1)-1
	             ENDIF
             ENDIF

             LOCAL uv_newClientTrxId
             uv_newClientTrxId = uf_gerais_stamp()

             SELECT fi_trans_info
           
             REPLACE fi_trans_info.packserialnumber WITH lcidid
             REPLACE fi_trans_info.clienttrxid WITH ALLTRIM(uv_newClientTrxId)
             REPLACE fi_trans_info.posterminal WITH ALLTRIM(myterm)
             REPLACE fi_trans_info.country_productnhrn WITH VAL(uf_gerais_retornaApenasAlphaNumericos(lcrefpaisid))
             REPLACE fi_trans_info.productnhrn WITH uf_gerais_retornaApenasAlphaNumericos(lcrefid)
             REPLACE fi_trans_info.ousrinis WITH ALLTRIM(m_chinis)
             REPLACE fi_trans_info.ousrdata WITH DATETIME()
             REPLACE fi_trans_info.tipo WITH 'S'
             IF ALLTRIM(lcleituracampos)=='ref'
                uf_infoembal_chama()
             ENDIF

          ELSE
             lcvalidainativo = .T.
          ENDIF
       ELSE
       	  uf_gerais_registaerrolog("Artigo n�o encontrado: " +  uv_dMatrix, "datamatrixCnp")
		  if(empty(lcref) OR vartype(lcref)=="X")
               uf_perguntalt_chama("Datamatrix incompleto", "OK", "", 64)
           else               
               uf_perguntalt_chama("Artigo n�o encontrado", "OK", "", 64) 
           endif		          
       ENDIF
       IF USED("uCrsFindRef")
          fecha("uCrsFindRef")
       ENDIF
   
**    CASE UPPER(LEFT(lcref, 2))=='CL' .AND. (LEN(lcref)<11)
    CASE UPPER(LEFT(lcref, 2))=='CL'  .AND. (LEN(lcref)<13)
    	IF uf_gerais_getParameter('ADM0000000321','BOOL')
    		LOCAL lcnrcartaocd, lcChkdigit 
    		lcnrcartaocd = ALLTRIM(SUBSTR(lcref,3,15))
    		lcChkdigit = uf_novonordisk_chkdigit(lcnrcartaocd,'Valida')
    		IF lcChkdigit != VAL(RIGHT(ALLTRIM(lcnrcartaocd),1))
    			uf_perguntalt_chama("N� de cart�o inv�lido! Por favor verifique","OK","",64)
    			RETURN .f.
    		ENDIF 
    	ENDIF 
       lcsql = ''
       TEXT TO lcsql TEXTMERGE NOSHOW
				select top 1 
					no, estab, nome
				from 
					b_utentes (nolock)
				where
					inactivo = 0 
					and nrcartao='<<ALLTRIM(lcRef)>>'
				order by estab
       ENDTEXT
       IF uf_gerais_actgrelha("", "uCrsNrUtCartao", lcsql) .AND. RECCOUNT()>0
          uf_atendimento_selcliente(ucrsnrutcartao.no, ucrsnrutcartao.estab, "ATENDIMENTO")
          uf_atendimento_verificainfoutente()
          IF ucrsnrutcartao.no>200
          	 IF uf_gerais_getParameter_site('ADM0000000132', 'BOOL', mySite) = .t.
	          	replace uCrsAtendCL.nrcartao WITH ALLTRIM(lcRef)
	          	**IF uf_gerais_getparameter_site('ADM0000000133', 'BOOL', mysite) 
			       	  atendimento.timer2.interval = 1000
			          atendimento.timer2.reset()
			     **ENDIF 
	          	**replace uCrsAtendCl.valcartao WITH 0
	          ENDIF 
          	 IF uf_gerais_getParameter_site('ADM0000000132', 'BOOL', mySite) = .f.
	             uf_regvendas_chama()
	             uf_regvendas_filtrarres(.T.)
	          ENDIF 
          ENDIF
          fecha("uCrsNrUtCartao")
       ELSE
          lcvalidacl = .T.
       ENDIF
       uf_atendimento_controlapontosapresentacartao(.T.)
   
    CASE ((LEFT(lcref, 3)=='RES' .OR. LEFT(lcref, 2)=='RE' .OR. (LEFT(lcref, 1)=='R')) .AND. LEN(lcref)>7) or (LEFT(lcref, 4)=='NRES' AND LEN(lcref)>5)
       LOCAL lcrefauxnocl, lcrefauxdepcl, lcrefnres
       STORE '' TO lcrefauxnocl, lcrefauxdepcl, lcrefnres
       IF (LEFT(lcref, 3)=='RES' .OR. LEFT(lcref, 2)=='RE')
          lcrefauxnocl = STREXTRACT(lcref, 'CL', 'D', 1)
          lcrefauxdepcl = STREXTRACT(lcref, 'D', '', 1)
       ELSE
          IF (LEFT(lcref, 4)=='NRES')
             lcrefnres = STREXTRACT(lcref, 'NRES', 'E', 1)
          ELSE
             lcrefauxnocl = STREXTRACT(lcref, 'C', 'D', 1)
             lcrefauxdepcl = STREXTRACT(lcref, 'D', '', 1)
          ENDIF
       ENDIF
       IF ( .NOT. EMPTY(ALLTRIM(lcrefauxnocl))) .OR. ( .NOT. EMPTY(ALLTRIM(lcrefnres)))
          lcsql = ''
          IF EMPTY(ALLTRIM(lcrefnres))
             TEXT TO lcsql TEXTMERGE NOSHOW
						select top 1 
							no, estab, nome
						from 
							b_utentes (nolock)
						where
							inactivo = 0 
							and no = CASE WHEN ISNUMERIC('<<ALLTRIM(lcRefAuxNoCl)>>') = 1 THEN CAST('<<ALLTRIM(lcRefAuxNoCl)>>' AS INT) ELSE NULL END
							and estab = case when '<<ALLTRIM(lcRefAuxDepCl)>>'='' then 0 else (CASE WHEN ISNUMERIC('<<ALLTRIM(lcRefAuxDepCl)>>') = 1 THEN CAST('<<ALLTRIM(lcRefAuxDepCl)>>' AS INT) ELSE NULL END) end
						order by estab
             ENDTEXT
          ELSE
             TEXT TO lcsql TEXTMERGE NOSHOW
						select top 1 
							no, estab, nome
						from 
							bo (nolock)
						where
							nmdos='Reserva de Cliente'
							and obrano= <<ALLTRIM(lcRefNRes)>>
							and fechada = 0
						order by dataobra desc
             ENDTEXT
          ENDIF
          IF uf_gerais_actgrelha("", "uCrsAuxTalaoRes", lcsql) .AND. RECCOUNT()>0
             uf_atendimento_selcliente(ucrsauxtalaores.no, ucrsauxtalaores.estab, "ATENDIMENTO")
             uf_atendimento_verificainfoutente()
             IF ucrsauxtalaores.no>200
                uf_regvendas_chama()
                uf_regvendas_filtrarres(.T.)
                uf_regvendas_marcarSel()

                
             ENDIF
             uf_atendimento_controlapontosapresentacartao(.T.)
             IF USED("uCrsAuxTalaoRes")
                fecha("uCrsAuxTalaoRes")
             ENDIF
          ENDIF
       ENDIF
    CASE LEFT(lcref, 1)=='V'
       DO CASE
          CASE LEFT(lcref, 2)=='VG'
             IF uf_gerais_actgrelha("", 'uCrsValeServ', [select top 1 * from b_fidelvalegen where ref=']+ALLTRIM(lcref)+[' and CONVERT(varchar, b_fidelvalegen.ousrdata, 112)<CONVERT(varchar, getdate(), 112) ]) .AND. RECCOUNT("uCrsValeServ")>0
                IF  .NOT. ucrsvaleserv.abatido .AND.  .NOT. ucrsvaleserv.anulado
                   IF uf_gerais_actgrelha("", 'uCrsGetValeServRef', [select top 1 ref from st (nolock) where inactivo = 0 and ref = 'V999999']) .AND. RECCOUNT()>0
                      uf_atendimento_adicionalinhafi(.F.)
                      REPLACE fi.ref WITH ALLTRIM(ucrsgetvaleservref.ref)
                      REPLACE fi.u_refvale WITH ALLTRIM(ucrsvaleserv.ref)
                      lcsql = ""
                      TEXT TO lcsql TEXTMERGE NOSHOW
									update st set epv1=<<Round(uCrsValeServ.valor,2)>> where ref='V999999'
                      ENDTEXT
                      uf_gerais_actgrelha("", "", lcsql)
                      uf_atendimento_actref()
                      lcposicfi = RECNO("FI")
                      uf_atendimento_eventofi()
                      TRY
                         GOTO lcposicfi
                      CATCH
                      ENDTRY
                   ELSE
                      lcvalidavalref = .T.
                   ENDIF
                ELSE
                   lcvalidaval = .T.
                ENDIF
             ELSE
                lcvalidaval = .T.
             ENDIF
          OTHERWISE
             lcsql = ''
             IF  .NOT. uf_gerais_getparameter_site("ADM0000000017", "BOOL")
                TEXT TO lcsql TEXTMERGE NOSHOW
						select 
							top 1 *
							,idade_meses = DATEDIFF(MONTH, B_fidelVale.ousrdata, getdate()) 
						from b_fidelvale (nolock) 
						where ref = '<<Alltrim(lcRef)>>' 
				        and CONVERT(varchar, B_fidelvale.ousrdata, 112)<CONVERT(varchar, getdate(), 112)
                ENDTEXT
             ELSE
                TEXT TO lcsql TEXTMERGE NOSHOW
						select 
							top 1 *
							,idade_meses = DATEDIFF(MONTH, B_fidelVale.ousrdata, getdate()) 
						from b_fidelvale (nolock) 
						where ref = '<<Alltrim(lcRef)>>' 
				        and CONVERT(varchar, B_fidelvale.ousrdata, 112)<CONVERT(varchar, getdate(), 112)
				        and CONVERT(varchar, dateadd(month,<<uf_gerais_getParameter("ADM0000000017","NUM")>>,B_fidelvale.ousrdata), 112) >= CONVERT(varchar, getdate(), 112) 
				        
                ENDTEXT
             ENDIF
             IF uf_gerais_actgrelha("", 'uCrsValeServ', lcsql) .AND. RECCOUNT()>0
                IF  .NOT. ucrsvaleserv.abatido .AND.  .NOT. ucrsvaleserv.anulado
                   lcsql = ''
                   TEXT TO lcsql TEXTMERGE NOSHOW
							select top 1 ref from st (nolock) where inactivo = 0 and Left(ref,1)='v' and epv1=(-1)*<<uCrsValeServ.valor>>
                   ENDTEXT
                   IF uf_gerais_actgrelha("", 'uCrsGetValeServRef', lcsql) .AND. RECCOUNT()>0
                      uf_atendimento_adicionalinhafi(.F.)
                      REPLACE fi.ref WITH ALLTRIM(ucrsgetvaleservref.ref)
                      REPLACE fi.u_refvale WITH ALLTRIM(ucrsvaleserv.ref)
                      uf_atendimento_actref()
                      lcposicfi = RECNO("FI")
                      uf_atendimento_eventofi()
                      TRY
                         GOTO lcposicfi
                      CATCH
                      ENDTRY
                   ELSE
                      uf_atendimento_adicionalinhafi(.F.)
                      REPLACE fi.ref WITH 'V999999'
                      REPLACE fi.u_refvale WITH ALLTRIM(ucrsvaleserv.ref)
                      lcsql = ""
                      TEXT TO lcsql TEXTMERGE NOSHOW
								update st set epv1=<<Round(uCrsValeServ.valor,2)>> * -1 where ref='V999999'
                      ENDTEXT
                      uf_gerais_actgrelha("", "", lcsql)
                      uf_atendimento_actref(.F.)
                      lcposicfi = RECNO("FI")
                      uf_atendimento_eventofi()
                      TRY
                         GOTO lcposicfi
                      CATCH
                      ENDTRY
                   ENDIF
                ELSE
                   lcvalidaval = .T.
                ENDIF
             ELSE
                lcvalidaval = .T.
             ENDIF
       ENDCASE
    CASE LEFT(lcref, 1)=='U' .OR. LEFT(lcref, 1)=='M' .OR. LEFT(lcref, 1)=='D'
       SELECT fi
       IF LEFT(fi.design, 1)=="."
          IF UPPER(LEFT(lcref, 1))=='U'
             REPLACE fi.rvpstamp WITH ALLTRIM(lcref)
          ELSE
             REPLACE fi.szzstamp WITH ALLTRIM(lcref)
          ENDIF
       ENDIF
    CASE ((LEN(lcref)>=9) .AND. (LEN(lcref)<=19))
       LOCAL lcfamilia
       STORE '' TO lcfamilia
       lcsql = ''
       TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_stocks_procuraRef '<<Alltrim(lcRef)>>', <<mysite_nr>>
       ENDTEXT
       IF uf_gerais_actgrelha("", "uCrsFindRef", lcsql) .AND. RECCOUNT()>0
          IF ucrsfindref.inactivo==.F.
             IF atendimento.menu1.conferir.tag=="true"
                SELECT ucrsfindref
                atendimento.txtscanner.value = ''
                uf_atendimento_conferereferencia(ucrsfindref.ref)
                IF ucrse1.obrigaconf=.T.
                   IF uf_atendimento_todos_dem_conferidos()
                      uf_atendimento_salvar_cancelar()
                   ELSE
                      uf_atendimento_conferindo_limpar()
                   ENDIF
                ELSE
                   IF uf_atendimento_todos_conferidos()
                      uf_atendimento_salvar_cancelar()
                   ENDIF
                ENDIF
                IF VARTYPE(lcrecnoconf)<>'U'
                   SELECT fi
                   TRY
                      GOTO lcrecnoconf
                   CATCH
                   ENDTRY
                   REPLACE fi.design WITH ALLTRIM(fi.design)
                   atendimento.grdatend.refresh
                   atendimento.grdatend.setfocus
                ENDIF
                atendimento.grdatend.setfocus
                RETURN .F.
             ENDIF
             lcfamilia = ucrsfindref.familia
             lcarvato = ucrsfindref.dispositivo_seguranca
             uf_atendimento_adicionalinhafi(.F., .F.)
             SELECT fi
             lcposfi = RECNO("fi")
             REPLACE fi.ref WITH ALLTRIM(ucrsfindref.ref)
             uf_atendimento_actref()
             SELECT fi
             TRY
                GOTO lcposfi
             CATCH
                GOTO BOTTOM
             ENDTRY
             uf_atendimento_eventofi()
             SELECT fi
             TRY
                GOTO lcposfi
             CATCH
                GOTO BOTTOM
             ENDTRY
             SELECT fi
             IF usamancompart
               IF TYPE("ATENDIMENTO") <> "U"
                  uf_atendimento_aplicaTabCompartAddLine()
               ENDIF
               uf_atendimento_adicionalinhafimancompart()
             ENDIF
             uf_atendimento_dadosst()
             SELECT fi
             TRY
                GOTO lcposfi
             CATCH
                GOTO BOTTOM
             ENDTRY
             IF uf_gerais_getparameter("ADM0000000192", "BOOL")
                SELECT ucrsatendcl
                IF ucrsatendcl.no<>200
                   IF  .NOT. EMPTY(ALLTRIM(ucrsatendcl.codpla)) .AND. (uf_gerais_getdate(ucrsatendcl.valipla, "SQL")=="19000101" .OR. ucrsatendcl.valipla>=DATE())
                      SELECT fi
                      IF RECCOUNT("fi")>1
                         uf_atendimento_aplicaplanoauto()
                      ENDIF
                   ENDIF
                ENDIF
                SELECT fi
                TRY
                   GOTO lcposfi
                CATCH
                   GOTO BOTTOM
                ENDTRY
             ENDIF
             IF uf_gerais_getparameter("ADM0000000074", "BOOL")
                SELECT ucrsfindref
                IF ucrsfindref.mfornec>0
                   uf_atendimento_alteradesc(.T., ucrsfindref.mfornec, ucrsfindref.dataFimPromo, ucrsfindref.dataIniPromo)
                ELSE
                   IF ucrsfindref.mfornec2>0
                      uf_atendimento_alteradescval(.T., ucrsfindref.mfornec2, ucrsfindref.dataFimPromo, ucrsfindref.dataIniPromo)
                   ENDIF
                ENDIF
             ENDIF
             IF TYPE("ATENDIMENTO")<>"U"
                IF ALLTRIM(lcref)=='5681903' OR ALLTRIM(lcref)=='5771050'
                   uf_novonordisk_leitura(ALLTRIM(lcref))
                ENDIF
                IF insnnordisk=.T.
                	uf_atendimento_compart_nnordisk()
                	
                   lcpos = RECNO("fi")
                   lclordem = fi.lordem
                   lclordem = VAL(LEFT(ALLTRIM(STR(lclordem)), 2)+'0000')
                   SELECT fi
                   GOTO TOP
                   SCAN
                   	  &&fix NOVONORDISK
                   	  &&up_receituario_dadosDoPlano
                      IF fi.lordem=lclordem

						IF uf_atendimento_aplicaPlanoND(fi.fistamp)

							SELECT ucrsatendcomp

							REPLACE fi.design WITH '.[' + ALLTRIM(ucrsatendcomp.codigo) + '][' + ALLTRIM(ucrsatendcomp.cptorgabrev) + '] Receita: -'
							REPLACE fi.tipor WITH 'RM'

						ELSE
							RETURN .F.
						ENDIF
						
                      ENDIF
                   ENDSCAN
                   SELECT fi
                   TRY
                      GOTO lcpos
                   CATCH
                   ENDTRY
                ENDIF
                insnnordisk = .F.
             ENDIF
             IF lcarvato
                uf_insert_fi_trans_info_seminfo()
             ENDIF
             atendimento.grdatend.setfocus
          ELSE
             lcvalidainativo = .T.
          ENDIF
       ELSE
          lcsql = ''
          TEXT TO lcsql TEXTMERGE NOSHOW
					select top 1 
						no, estab, nome
					from  
						b_utentes (nolock)
					where 
						inactivo = 0 
						and (nbenef='<<Left(Alltrim(lcRef),9)>>' or nbenef2='<<Left(Alltrim(lcRef),9)>>')
					order by estab
          ENDTEXT
          IF uf_gerais_actgrelha("", "uCrsNrUtente", lcsql) .AND. RECCOUNT()>0
             uf_atendimento_selcliente(ucrsnrutente.no, ucrsnrutente.estab, "ATENDIMENTO")
             uf_atendimento_verificainfoutente()
             IF ucrsnrutente.no>200
                uf_reservas_cria('LISTA', .F.)
             ENDIF
          ELSE
             IF LEN(lcref)==14 .OR. LEN(lcref)==12
                TEXT TO lcsql TEXTMERGE NOSHOW
							SELECT TOP 1 fdata FROM ft (nolock) WHERE ft.u_nratend='<<lcRef>>' ORDER BY ft.fdata desc
                ENDTEXT
                uf_gerais_actgrelha("", "uCrsDataReg", lcsql)
                SELECT ucrsdatareg
                GOTO TOP
                uf_regvendas_chama(.F., ALLTRIM(lcref))
                regvendas.txtnatend.value = lcref
                regvendas.txtdataini.value = uf_gerais_getdate(ucrsdatareg.fdata)
                regvendas.txtdatafim.value = uf_gerais_getdate(ucrsdatareg.fdata)
                regvendas.menu1.actualizar.click()
             ELSE
                uf_perguntalt_chama("N�O EXISTE NENHUM CLIENTE COM ESSE NR. DE UTENTE ASSOCIADO.", "OK", "", 64)
             ENDIF
          ENDIF
          IF USED("uCrsNrUtente")
             fecha("uCrsNrUtente")
          ENDIF
       ENDIF
    CASE (LEN(lcref)==8) .AND. (LEFT(lcref, 1)=='5')
       uf_pesqstocks_chama("ATENDIMENTO", lcref)
    OTHERWISE
       LOCAL lcfamilia
       STORE '' TO lcfamilia
       lcsql = ""
       TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_stocks_procuraRef '<<ALLTRIM(lcRef)>>', <<mysite_nr>>
       ENDTEXT
       IF uf_gerais_actgrelha("", "uCrsFindRef", lcsql) .AND. RECCOUNT()>0
          IF ucrsfindref.inactivo==.F.
             lcfamilia = ucrsfindref.familia
             SELECT ucrsfindref
             IF ucrsfindref.qttcli>0 .AND. atendimento.menu1.conferir.tag=="false"
                IF (ucrsfindref.stock-ucrsfindref.qttcli)<=0
                   IF  .NOT. uf_perguntalt_chama("Existe uma reserva aberta com quantidade "+ALLTRIM(STR(ucrsfindref.qttcli))+" para o produto que seleccionou."+CHR(13)+"Tem a certeza que pretende adicion�-lo ao atendimento?", "Sim", "N�o")
                      atendimento.txtscanner.value = ""
                      RETURN .F.
                   ELSE
                      IF uf_gerais_getparameter('ADM0000000066', 'BOOL')
                         PUBLIC cval
                         STORE '' TO cval
                         uf_tecladoalpha_chama("cval", "INTRODUZA PASSWORD PARA PERMITIR SELECCIONAR PRODUTO RESERVADO:", .T., .F., 0)
                         IF EMPTY(cval)
                            uf_perguntalt_chama("DEVE INTRODUZIR A PASSWORD.", "OK", "", 64)
                            RELEASE cval
                            atendimento.txtscanner.value = ""
                            RETURN .F.
                         ELSE
                            IF  .NOT. (UPPER(ALLTRIM(cval))==UPPER(ALLTRIM(uf_gerais_getparameter('ADM0000000066', 'TEXT'))))
                               uf_perguntalt_chama("A PASSWORD N�O EST� CORRECTA.", "OK", "", 64)
                               RELEASE cval
                               atendimento.txtscanner.value = ""
                               RETURN .F.
                            ELSE
                               RELEASE cval
                            ENDIF
                         ENDIF
                      ENDIF
                   ENDIF
                ENDIF
             ENDIF
             IF atendimento.menu1.conferir.tag=="true"
                SELECT ucrsfindref
                atendimento.txtscanner.value = ''
                uf_atendimento_conferereferencia(ucrsfindref.ref)
                IF ucrse1.obrigaconf=.T.
                   IF uf_atendimento_todos_dem_conferidos()
                      uf_atendimento_salvar_cancelar()
                   ELSE
                      uf_atendimento_conferindo_limpar()
                   ENDIF
                ELSE
                   IF uf_atendimento_todos_conferidos()
                      uf_atendimento_salvar_cancelar()
                   ENDIF
                ENDIF
                IF VARTYPE(lcrecnoconf)<>'U'
                   SELECT fi
                   TRY
                      GOTO lcrecnoconf
                   CATCH
                   ENDTRY
                   REPLACE fi.design WITH ALLTRIM(fi.design)
                   atendimento.grdatend.refresh
                   atendimento.grdatend.setfocus
                ENDIF
                atendimento.grdatend.setfocus
                RETURN .F.
             ENDIF
             SELECT fi
             IF  .NOT. EMPTY(fi.dem) .AND. atendimento.tmrconsultadem.enabled==.F. .AND.  .NOT. EMPTY(fi.stns) .AND.  .NOT. EMPTY(fi.epromo)
                LOCAL lcdemconsultada
                STORE .T. TO lcdemconsultada
                lcdemconsultada = uf_atendimento_validasedemconsultada()
                IF (lcdemconsultada==.F.)
                   uf_atendimento_novavenda(.T.)
                ENDIF
             ENDIF
             uf_atendimento_adicionalinhafi(.F., .F.)
             SELECT fi
             lcposfi = RECNO("fi")
             SELECT fi
             REPLACE fi.ref WITH ALLTRIM(ucrsfindref.ref)
             uf_atendimento_actref()
             SELECT fi
             TRY
                GOTO lcposfi
             CATCH
                GOTO BOTTOM
             ENDTRY
             IF ALLTRIM(fi.ref)<>'5681903' .AND. ALLTRIM(fi.ref)<>'5771050'
                uf_atendimento_eventofi()
             ENDIF
             SELECT fi
             TRY
                GOTO lcposfi
             CATCH
                GOTO BOTTOM
             ENDTRY
             SELECT fi
             IF usamancompart
               IF TYPE("ATENDIMENTO") <> "U"
                  uf_atendimento_aplicaTabCompartAddLine()
               ENDIF
               uf_atendimento_adicionalinhafimancompart()
             ENDIF
             uf_atendimento_dadosst()
             SELECT fi
             TRY
                GOTO lcposfi
             CATCH
                GOTO BOTTOM
             ENDTRY
             IF uf_gerais_getparameter("ADM0000000192", "BOOL")
                SELECT ucrsatendcl
                IF ucrsatendcl.no<>200
                   IF  .NOT. EMPTY(ALLTRIM(ucrsatendcl.codpla)) .AND. (uf_gerais_getdate(ucrsatendcl.valipla, "SQL")=="19000101" .OR. ucrsatendcl.valipla>=DATE())
                      SELECT fi
                      IF RECCOUNT("fi")>1
                         uf_atendimento_aplicaplanoauto()
                      ENDIF
                   ENDIF
                ENDIF
                SELECT fi
                TRY
                   GOTO lcposfi
                CATCH
                   GOTO BOTTOM
                ENDTRY
             ENDIF
             LOCAL lcinfoplano
             STORE '' TO lcinfoplano
             lcinfoplano = uf_atendimento_compapoioespecial()
             IF  .NOT. EMPTY(lcinfoplano) .AND. (ALLTRIM(fi.ref)<>'5681903' .AND. ALLTRIM(fi.ref)<>'5771050')
                uf_perguntalt_chama("Aten��o, este produto pode ser comparticipado ao abrigo do programa de apoio especial: "+lcinfoplano+".", "OK", "", 64)
             ENDIF
             IF uf_gerais_getparameter("ADM0000000074", "BOOL")
                SELECT ucrsfindref
                IF ucrsfindref.mfornec>0
                   uf_atendimento_alteradesc(.T., ucrsfindref.mfornec, ucrsfindref.dataFimPromo, ucrsfindref.dataIniPromo)
                ELSE
                   IF ucrsfindref.mfornec2>0
                      uf_atendimento_alteradescval(.T., ucrsfindref.mfornec2, ucrsfindref.dataFimPromo, ucrsfindref.dataIniPromo)
                   ENDIF
                ENDIF
             ENDIF
             IF ALLTRIM(lcfamilia)=='1'
                uf_insert_fi_trans_info_seminfo()
             ENDIF
          ELSE
             lcvalidainativo = .T.
          ENDIF
       ELSE
          IF LEN(lcref)>=13 .AND. (ALLTRIM(fi.tipor)<>'RSP' .AND. ALLTRIM(fi.tipor)<>'RN')
             SELECT fi
             IF LEFT(fi.design, 1)=="."
                SELECT ucrsatendcomp
                LOCATE FOR ALLTRIM(ucrsatendcomp.fistamp)==ALLTRIM(fi.fistamp)
                IF FOUND()
                   SELECT fi
                   REPLACE fi.design WITH STREXTRACT(fi.design, '.', ':', 1, 4)+ALLTRIM(lcref)+" -"+STREXTRACT(fi.design, ' -', '', 1, 2)
                   IF EMPTY(fi.dem)
                      REPLACE fi.tipor WITH 'RM'
                   ELSE
                   ENDIF
                   SELECT ft2
                   REPLACE ft2.u_receita WITH ALLTRIM(lcref)
                   uf_atendimento_validanrreceita(lcobriganrreceita, fi.lordem)
                ELSE
                   lcvalida = .T.
                ENDIF
             ELSE
                lcvalida = .T.
             ENDIF
          ELSE
             lcvalida = .T.
          ENDIF
       ENDIF
       IF USED("uCrsFindRef")
          fecha("uCrsFindRef")
       ENDIF
 ENDCASE

 atendimento.txtscanner.value = ""
 IF lcvalida
    uf_perguntalt_chama("ESSA REFER�NCIA N�O EXISTE EM STOCK. POR FAVOR VERIFIQUE.", "OK", "", 64)
 ENDIF
 IF lcvalidainativo
    uf_perguntalt_chama("ESSA REFER�NCIA EST� CONFIGURADA COMO INATIVA. POR FAVOR VERIFIQUE A FICHA DO PRODUTO. OBRIGADO.", "OK", "", 64)
    RETURN .F.
 ENDIF
 IF lcvalidaval
    uf_perguntalt_chama("ESTE VALE � INV�LIDO. PROVAVELMENTE J� FOI UTILIZADO, NUNCA FOI EMITIDO, CADUCOU OU S� PODER� SER UTILIZADO APARTIR DE AMANH�.", "OK", "", 64)
 ENDIF
 IF lcvalidavalref
    uf_perguntalt_chama("ESTE VALE N�O TEM REFER�NCIA ASSOCIADA. DEVE PRIMEIRO CRIAR UMA REFER�NCIA NO ECR� DE STOCKS COM VALOR RESPECTIVO DO VALE A NEGATIVO NO PVP. NOTE QUE ESTA REFER�NCIA DEVE COME�AR PELA LETRA V.", "OK", "", 64)
 ENDIF
 IF lcvalidacl
    uf_perguntalt_chama("N�O EXISTE NENHUM CLIENTE ACTIVO COM O N� DE CART�O QUE SELECCIONOU.", "OK", "", 64)
 ENDIF
 
 atendimento.grdatend.setfocus()
 uf_atendimento_actvalorescab()
 uf_atendimento_infototaiscab()
 uf_atendimento_infototais()
 IF TYPE("lcPosFi")<>'U'
    SELECT fi
    TRY
       GOTO lcposfi
    CATCH
       GOTO BOTTOM
    ENDTRY
 ENDIF
 RETURN .T.
ENDFUNC
**
FUNCTION uf_atendimento_criadadospsico
 LOCAL ordem, validaordem, validavenda, validapsico, lcdempsicoaux
 STORE 0 TO ordem
 STORE .F. TO validaordem, validavenda, validapsico, lcdempsicoaux
 SELECT ucrscabvendas
 GOTO TOP
 SCAN
    SELECT fi
    SCAN FOR fi.design=ucrscabvendas.design .AND. fi.lordem=ucrscabvendas.lordem
       ordem = RECNO("fi")
    ENDSCAN
    SELECT fi
    SCAN
       IF  .NOT. validaordem
          GOTO ordem
          validaordem = .T.
       ENDIF
       IF LEFT(fi.design, 1)="." .AND. EMPTY(ALLTRIM(fi.ref))
          IF  .NOT. validavenda
             validavenda = .T.
          ELSE
             EXIT
          ENDIF
       ELSE
          IF fi.u_psico .AND. EMPTY(fi.bistamp)
             validapsico = .T.
             EXIT
          ENDIF
       ENDIF
    ENDSCAN
    LOCAL lcvalida
    STORE .F. TO lcvalida
    IF validapsico
       SELECT dadospsico
       SCAN FOR dadospsico.cabvendasordem==ucrscabvendas.lordem
          lcvalida = .T.
       ENDSCAN
       IF  .NOT. lcvalida
          SELECT dadospsico
          APPEND BLANK
          SELECT ucrsatendcl
          REPLACE dadospsico.u_nmutavi WITH ucrsatendcl.u_nmutavi
          REPLACE dadospsico.u_moutavi WITH ucrsatendcl.u_moutavi
          REPLACE dadospsico.u_cputavi WITH ucrsatendcl.u_cputavi
          REPLACE dadospsico.u_ndutavi WITH ucrsatendcl.u_ndutavi
          REPLACE dadospsico.u_ddutavi WITH ucrsatendcl.u_ddutavi
          REPLACE dadospsico.u_idutavi WITH ucrsatendcl.u_idutavi
          REPLACE dadospsico.u_nmutdisp WITH ucrsatendcl.nome
          REPLACE dadospsico.u_moutdisp WITH ucrsatendcl.morada
          REPLACE dadospsico.u_cputdisp WITH ucrsatendcl.codpost
          REPLACE dadospsico.nascimento WITH ucrsatendcl.nascimento 
          SELECT ucrscabvendas
          REPLACE dadospsico.u_recdata WITH ucrscabvendas.datareceita
          REPLACE dadospsico.u_medico WITH ALLTRIM(ucrscabvendas.nopresc)
          REPLACE dadosPsico.contactoCli WITH ALLTRIM(ucrsatendcl.contactoUT)
          IF  .NOT. EMPTY(ucrscabvendas.token)
             lcdempsicoaux = .T.
          ENDIF
          SELECT ucrscabvendas
          SELECT dadospsico
          REPLACE dadospsico.cabvendasordem WITH ucrscabvendas.lordem

       ENDIF
    ENDIF
    validapsico = .F.
    validaordem = .F.
    validavenda = .F.
 ENDSCAN
 
 SELECT dadospsico
 IF RECCOUNT("dadosPsico")>0
    LOCAL lcvalpsifalta, lctextopsi
    STORE .T. TO lcvalpsifalta
    STORE '' TO lctextopsi
    IF  .NOT. EMPTY(lcdempsicoaux)
       lcsql = ''
       TEXT TO lcsql TEXTMERGE NOSHOW
				SELECT * FROM dispensa_eletronica_cc (nolock) WHERE LEFT(id,len(id)-1) = '<<ALLTRIM(uCrsatendCl.u_ndutavi)>>' OR doc_id = '<<ALLTRIM(uCrsatendCl.u_ndutavi)>>'
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "uCrsAuxUtente", lcsql)
          uf_perguntalt_chama("Ocorreu uma anomalia ao verificar a informa��o do utentes. Por favor contacte o Suporte.", "OK", "", 16)
          RETURN .F.
       ELSE
          IF RECCOUNT("uCrsAuxUtente")==0
             lcsql = ''
             TEXT TO lcsql TEXTMERGE NOSHOW
						insert into dispensa_eletronica_cc (id, id_emp, nome, morada, nif, nrSNS, sexo, nascimento, validade_fim, publicKey, site, machine, doc_id, tipoDoc) 
                   		values (LEFT(NEWID(),20), '<<ALLTRIM(uCrsE1.id_lt)>>', '<<ALLTRIM(uCrsatendCl.u_nmutavi)>>', '<<ALLTRIM(uCrsatendCl.u_moutavi)>>', '', '', '', '<<uf_gerais_getdate(uCrsatendCl.u_dcutavi,"SQL")>>', '<<uf_gerais_getdate(uCrsatendCl.u_ddutavi,"SQL")>>', '','<<ALLTRIM(mySite)>>', '','<<ALLTRIM(uCrsatendCl.u_ndutavi)>>', '<<ALLTRIM(uCrsAtendCl.codDocID)>>')
             ENDTEXT
             IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
                uf_perguntalt_chama("Ocorreu uma anomalia ao inserir a informa��o detalhada do utente. Por favor contacte o Suporte.", "OK", "", 16)
                RETURN .F.
             ENDIF
          ELSE
             lcsql = ''
             TEXT TO lcsql TEXTMERGE NOSHOW
            UPDATE dispensa_eletronica_cc SET tipoDoc = '<<ALLTRIM(uCrsAtendCl.codDocID)>>', morada = '<<ALLTRIM(uCrsatendCl.u_moutavi)>>', doc_id = '<<ALLTRIM(uCrsatendCl.u_ndutavi)>>'
,nome = '<<ALLTRIM(uCrsatendCl.u_nmutavi)>>', nascimento ='<<uf_gerais_getdate(uCrsatendCl.u_dcutavi,"SQL")>>'
,validade_fim='<<uf_gerais_getdate(uCrsatendCl.u_ddutavi,"SQL")>>'

             ENDTEXT
             IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
                uf_perguntalt_chama("Ocorreu uma anomalia ao atualizar a informa��o detalhada do utente. Por favor contacte o Suporte.", "OK", "", 16)
                RETURN .F.
             ENDIF
          ENDIF
       ENDIF
    ENDIF
 ENDIF
 RETURN .T.
ENDFUNC
**
FUNCTION uf_atendimento_aplicadiplomatouch
 LOCAL lccod
 lccod = ""
 SELECT fi
 IF LEFT(fi.design, 1)=="."
    RETURN .F.
 ENDIF
 IF  .NOT. EMPTY(fi.u_diploma)
    REPLACE fi.u_diploma WITH ""
    atendimento.grdatend.refresh()
    lcposicfi = RECNO("FI")
    uf_atendimento_eventofi()
    TRY
       GOTO lcposicfi
    CATCH
    ENDTRY
    uf_atendimento_dadosst()
    RETURN .T.
 ENDIF
 lccod = uf_atendimento_verificacompart()
 IF  .NOT. EMPTY(lccod)
    IF uf_gerais_actgrelha("", 'uCrsPlaDip', [select diploma from cptpla (nolock) where codigo=']+ALLTRIM(lccod)+['])
       IF RECCOUNT("uCrsPlaDip")=0
          RETURN .F.
       ENDIF
       IF ucrspladip.diploma<>"1"
          RETURN .F.
       ENDIF
       fecha("uCrsPlaDip")
       mydiplomaauto = .F.
       uf_diplomas_chama()
    ENDIF
 ENDIF
ENDFUNC
**
FUNCTION uf_atendimento_comparapvpcelulaemescoamento
 LOCAL lcposfi, lcposatendst, lcemescoamento
 STORE .F. TO lcemescoamento
 SELECT fi
 lcposfi = RECNO("fi")
 SELECT ucrsatendst
 lcposatendst = RECNO("uCrsAtendST")
 SELECT ucrsatendst
 LOCATE FOR ucrsatendst.pvpdic>0 .AND. ucrsatendst.pvpdic<>fi.u_epvp .AND. ALLTRIM(ucrsatendst.ststamp)==ALLTRIM(fi.fistamp)
 IF FOUND()
    lcemescoamento = .T.
 ELSE
    lcemescoamento = .F.
 ENDIF
 SELECT ucrsatendst
 TRY
    GOTO lcposatendst
 CATCH
    GOTO BOTTOM
 ENDTRY
 SELECT fi
 TRY
    GOTO lcposfi
 CATCH
    GOTO BOTTOM
 ENDTRY
 RETURN lcemescoamento
ENDFUNC
**
FUNCTION uf_atendimento_alteracorcelulapvp
 LOCAL lcrefexiste, lcposfi
 lcrefexiste = .F.

 IF ( .NOT. USED("uCrsAtendST") .OR.  .NOT. USED("fi"))
    RETURN "0"
 ENDIF
 **IF ALLTRIM(fi.familia)<>'1' .AND. ALLTRIM(fi.familia)<>'2' .AND. ALLTRIM(fi.familia)<>'10' .AND. ALLTRIM(fi.familia)<>'23' .AND. ALLTRIM(fi.familia)<>'58'
 IF !INLIST(ALLTRIM(fi.familia), '1', '2', '10', '23', '58')
    RETURN "0"
 ENDIF
 SELECT fi
 SELECT ucrsatendst
 IF (USED("uCrsPrecosValidos"))
    FECHA("uCrsPrecosValidos")
 ENDIF
 
 SELECT fi
 TEXT TO lcsql TEXTMERGE NOSHOW
 		exec up_touch_hist_precos '<<ALLTRIM(fi.ref)>>'
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "ucrsprecosvalidos", lcsql)
 	uf_perguntalt_chama("Ocorreu uma anomalia ao atualizar o hist�rico de pre�os. Por favor contacte o Suporte.", "OK", "", 16)
	RETURN "0"
 ENDIF
 
 SELECT ucrsprecosvalidos
 GOTO TOP
 LOCATE FOR uf_gerais_compStr(ucrsprecosvalidos.ref, fi.ref)
 IF  .NOT. FOUND()
    RETURN "0"
 ENDIF

 SELECT fi

 SELECT ucrsprecosvalidos
 GOTO TOP
 LOCATE FOR fi.u_epvp == ucrsprecosvalidos.preco
 IF FOUND()
    IF uf_atendimento_comparapvpcelulaemescoamento()
       RETURN "2"
    ELSE
       RETURN "0"
    ENDIF
 ELSE
    RETURN "1"
 ENDIF
 RETURN "0"
ENDFUNC
**
FUNCTION uf_atendimento_dadoscab
 LOCAL lcvalida, lccod
 STORE .F. TO lcvalida
 SELECT fi
 IF LEFT(fi.design, 1)<>"."
    RETURN .F.
 ELSE
    lccod = STREXTRACT(fi.design, '[', ']', 1, 0)
    SELECT ucrsatendcomp
    SCAN FOR UPPER(ALLTRIM(ucrsatendcomp.fistamp))==UPPER(ALLTRIM(fi.fistamp))
       lcvalida = .T.
       EXIT
    ENDSCAN
    IF lcvalida
       SELECT ucrsatendcomp
       WITH atendimento.pgfdados.page3
          .txtdesign.value = ucrsatendcomp.design
          .txtcod.value = ucrsatendcomp.codigo
          .txtabrev.value = ucrsatendcomp.cptorgabrev
          .txtcod2.value = ucrsatendcomp.codigo2
          .txtabrev2.value = ucrsatendcomp.cptorgabrev2
          .btnreceita.config(STREXTRACT(fi.design, ':', ' -', 1, 0))
          IF  .NOT. EMPTY(astr(STREXTRACT(fi.design, '-', '-', 1, 2)))
             .btnvia.label1.caption = astr(STREXTRACT(fi.design, '-', '-', 1, 2))
          ELSE
             .btnvia.label1.caption = "Normal"
          ENDIF
       ENDWITH
    ELSE
       WITH atendimento.pgfdados.page3
          .txtdesign.value = ''
          .txtcod.value = ''
          .txtabrev.value = ''
          .txtcod2.value = ''
          .txtabrev2.value = ''
          .btnreceita.config('')
       ENDWITH
    ENDIF
 ENDIF
 SELECT fi
 IF LEFT(ALLTRIM(fi.design), 2)==".S"
    atendimento.pgfdados.page3.btnaplicacompart.label1.caption = "APLICA COMPART."
    atendimento.pgfdados.page3.btnaplicacompart.Shape1.backcolor = RGB(0,  167, 231)
 ELSE
    atendimento.pgfdados.page3.btnaplicacompart.label1.caption = "REMOVE COMPART."
    atendimento.pgfdados.page3.btnaplicacompart.Shape1.backcolor = RGB(255, 94, 91)
 ENDIF
 SELECT fi
 IF (RIGHT(ALLTRIM(fi.design), 10)=="*SUSPENSA*") .OR. (RIGHT(ALLTRIM(fi.design), 18)=="*SUSPENSA RESERVA*")
    atendimento.pgfdados.page3.btnsuspender.Shape1.backcolor = RGB(255, 94, 91)
 ELSE
    atendimento.pgfdados.page3.btnsuspender.Shape1.backcolor = RGB(0,  167, 231)
 ENDIF
 IF usamancompart
   atendimento.pgfDados.page3.txtValElegibilidade.value = uf_atendimento_calcValElegibilidade()
 ENDIF
 uf_atendimento_infototaiscab()
 atendimento.selcab = .F.
 atendimento.pgfdados.activepage = 3
 atendimento.refresh
ENDFUNC
**
PROCEDURE uf_atendimento_dadosst
 LOCAL lcvlida, lcrefs, lcpppos
 STORE .F. TO lcvalida
 STORE "" TO lcrefs
 SELECT ucrsatendst
 SCAN FOR ucrsatendst.ststamp==fi.fistamp
    lcvalida = .T.
    EXIT
 ENDSCAN
 IF  .NOT. (lcvalida)
    SELECT ucrsatendst
    GOTO TOP
 ENDIF
 WITH atendimento
    DO CASE
       CASE fi.partes<>0
          IF fi.partes=1
             .lbldev.caption = "Dev. Dinheiro"
          ELSE
             .lbldev.caption = "Dev. Cr�d."
          ENDIF
       OTHERWISE
          .lbldev.caption = "Dev. Prod."
    ENDCASE
 ENDWITH
 WITH atendimento.pgfdados.page2
    IF UPPER(mypaisconfsoftw)=='PORTUGAL'
       IF  .NOT. (ALLTRIM(fi.u_diploma)=='')
          .btndiploma.label1.caption = 'DIPLOMA: '+ALLTRIM(fi.u_diploma)
          .btndiploma.label1.left = 0
          .btndiploma.label1.height = 30
          .btndiploma.Shape1.backcolor = RGB(255, 94, 91)
       ELSE
          .btndiploma.label1.caption = 'APLICAR DIPLOMA'
          .btndiploma.label1.left = 0
          .btndiploma.label1.height = 30
          .btndiploma.label1.top = 9
          .btndiploma.Shape1.backcolor = RGB(0,  167, 231)
       ENDIF
    ENDIF
    uf_excecaomed_preenchecamposlivres()
    IF ALLTRIM(UPPER(fi.u_robot))<>'P' .AND. fi.num1>0
       .txtnum1.disabledbackcolor = RGB(30, 144, 255)
    ELSE
       .txtnum1.disabledbackcolor = RGB(255, 255, 255)
    ENDIF
    IF ALLTRIM(UPPER(fi.tipor))=='RES'
       .lbltpres.disabledbackcolor = RGB(30, 144, 255)
    ELSE
       .lbltpres.disabledbackcolor = RGB(255, 255, 255)
    ENDIF
    IF (fi.u_stock<=0)
       .txtu_stock.disabledbackcolor = RGB(255, 0, 0)
       .txtu_stock.forecolor = RGB(255, 255, 255)
    ENDIF
    SELECT fi_trans_info
    LOCATE FOR ALLTRIM(fi_trans_info.recStamp)==ALLTRIM(fi.fistamp) .AND. (EMPTY(fi_trans_info.productcode) .OR. EMPTY(fi_trans_info.batchid) .OR. EMPTY(fi_trans_info.packserialnumber) .OR. YEAR(fi_trans_info.batchexpirydate)=1900)
    IF FOUND()
       .txtref.disabledbackcolor = RGB(255, 0, 0)
       .txtref.forecolor = RGB(255, 255, 255)
    ELSE
       .txtref.disabledbackcolor = RGB(255, 255, 255)
       .txtref.forecolor = RGB(0, 0, 0)
    ENDIF
    SELECT fi
    LOCAL lccodcnp
    STORE '' TO lccodcnp
    SELECT fi
    TEXT TO lcsql TEXTMERGE NOSHOW
			select codCNP from st(nolock) WHERE ref='<<ALLTRIM(fi.ref)>>' AND site_nr=<<mySite_nr>>
    ENDTEXT
    uf_gerais_actgrelha("", "ucrscodcnp", lcsql)
    lccodcnp = ALLTRIM(ucrscodcnp.codcnp)
    fecha("ucrscodcnp")
    SELECT ucrsatenddci
    IF EMPTY(lccodcnp)
       LOCATE FOR UPPER(ALLTRIM(ucrsatenddci.ref))==UPPER(ALLTRIM(fi.ref))
       IF FOUND()
          .txtdci.value = ucrsatenddci.descricao
       ELSE
          .txtdci.value = ""
       ENDIF
    ELSE
       LOCATE FOR UPPER(ALLTRIM(ucrsatenddci.ref))==UPPER(ALLTRIM(lccodcnp))
       IF FOUND()
          .txtdci.value = ucrsatenddci.descricao
       ELSE
          .txtdci.value = ""
       ENDIF
    ENDIF
    DO CASE
       CASE ALLTRIM(fi.tpres)==''
          .lbltpres.value = ''
       CASE ALLTRIM(fi.tpres)=='RN'
          .lbltpres.value = 'SEM AD.'
       CASE ALLTRIM(fi.tpres)=='RADP'
          .lbltpres.value = ALLTRIM(STR(respagparc))+'% AD'
       OTHERWISE
          .lbltpres.value = '100% AD.'
    ENDCASE
 ENDWITH

 atendimento.pgfdados.activepage = 2
 atendimento.selprod = .F.
 atendimento.refresh()
 uf_gerais_trataValidadeLoteLinhas()
ENDPROC
**
FUNCTION uf_atendimento_pedenrreceitafo
 LPARAMETERS tcabrev, tcabrev2, obriganrreceita, lcfistamp, uv_nrReceita
 IF EMPTY(tcabrev) .AND. EMPTY(tcabrev2)
    RETURN .F.
 ENDIF
 IF  .NOT. EMPTY(obriganrreceita)
 	if(obriganrreceita<>3)
    	uf_atendimento_dadosreceita("Atendimento.pgfDados.page3.btnReceita.label1", obriganrreceita, lcfistamp, .F., uv_nrReceita)
    ENDIF
    	
 ENDIF
ENDFUNC

FUNCTION uf_atendimento_marcaDemMCDT
 LPARAMETERS lcBool, lcLordem
 
 
 LOCAL lcBoolRes
 lcBoolRes = .f.
 
 if(!EMPTY(lcBool))
 	lcBoolRes  = lcBool
 ENDIF

 
 SELECT fi 
 GO TOP 
 UPDATE fi set fi.dem =  lcBoolRes  , fi.token = '' WHERE fi.lordem>=lcLordem

ENDFUNC


FUNCTION uf_atendimento_marcaIdValidacaoDem
 LPARAMETERS ldId, lcLordem
 

 
 SELECT fi 
 GO TOP
 UPDATE fi set fi.id_validacaoDem = ldId WHERE lordem>=lcLordem

ENDFUNC




FUNCTION  uf_atendimento_perguntaSeReceitaRSP
 LPARAM	lcLordem
 
 LOCAL lcposfiaux, lcIsDem 
  
 lcposfiaux= RECNO("fi")

 IF !uf_perguntalt_chama("Tem  Requisi��o ou Guia de Presta��o?", "Req. (RCP)","Guia (RSP)")
 
 	 uf_atendimento_marcaDemMCDT(.t.,lcLordem)
 	 
 	  	 
	 PUBLIC lcDemPosAux
	 STORE '' TO lcDemPosAux
	 
	  DO WHILE !uf_gerais_validaPattern("[0-9]",lcDemPosAux) OR LEN(lcDemPosAux)>3
		  uf_tecladoalpha_chama("lcDemPosAux", "N�mero de Linha da Requisi��o(001 - 099):", .F., .F., 0)	 				 			 
	 ENDDO
	 uf_atendimento_marcaIdValidacaoDem(lcDemPosAux,lcLordem)
	 
 	 RELEASE lcDemPosAux
 	 
 ENDIF

 
 
 SELECT fi
 TRY
    GOTO lcposfiaux
 CATCH
    GOTO BOTTOM
 ENDTRY



ENDFUNC
 		

** fun��o para devolver informa��o do cabe�alho da venda apartir de qualquer linha do atendimento

FUNCTION uf_atendimento_getLordemCabFi
	LPARAMETERS lcOrdemEntrada
	LOCAL lnCabFiMultiplicador, lcOrdemCab, lcOrdemFinalVenda, lcposfiaux, lcFistampCab
	lnCabFiMultiplicador = 100000
	lcFistampCab = ''
	
	lcposfiaux= RECNO("fi")

	
	fecha("ucrsCabInfoFi")
	
	CREATE CURSOR ucrsCabInfoFi (lordemCabPos int, lordemCabFinalPos int, fistampCab varchar(50))
	
	**valor por defeito
	lcOrdemCab = lnCabFiMultiplicador
	
	*valor calculalado
	lcOrdemCab = INT(lcOrdemEntrada/lnCabFiMultiplicador )*lnCabFiMultiplicador
	lordemCabFinal  =  ((INT(lcOrdemEntrada/lnCabFiMultiplicador ) + 1) *lnCabFiMultiplicador)-1
	
	SELECT fi
	GO TOP
	LOCATE FOR  fi.lordem = lcOrdemCab  
	lcFistampCab  = ALLTRIM(fi.fistamp )
		

	INSERT INTO ucrsCabInfoFi(lordemCabPos , lordemCabFinalPos, fistampCab ) values (lcOrdemCab , lordemCabFinal  , lcFistampCab  )
		 
	SELECT fi
	TRY
	    GOTO lcposfiaux
	CATCH
	    GOTO BOTTOM
	ENDTRY
	 
	if(!USED("ucrsCabInfoFi") or RECCOUNT("ucrsCabInfoFi") <= 0 or lcOrdemCab<=0 or  lordemCabFinal<=0 or EMPTY(ALLTRIM(lcFistampCab)) )
	    fecha("ucrsCabInfoFi")
	 	RETURN .f.
	ELSE
	 	RETURN .t.
	ENDIF


ENDFUNC




**
FUNCTION uf_atendimento_aplicacompart
 LPARAMETERS lccodigo, tcbool, tcreservas, lcfistamp, lcsemperguntas
 
 

 IF TYPE("uv_nrCodVac") <> "U"
   RELEASE upv_nrCodVac
 ENDIF

 PUBLIC upv_nrCodVac

 LOCAL lcreceitaanterior
 lcreceitaanterior = ""
 LOCAL lcdesign, lcabrev, lccodigo2, lcdesign2, lcabrev2, lcpos, lcvaldiploma, lccodacesso, lccoddiropcao, lctoken, lcpercadic, lcnrdatareceita, lcnrreceitacomp, lcLordem, lcEfrDescr
 STORE '' TO lcdesign, lcabrev, lccodigo2, lcdesign2, lcabrev2, lccodacesso, lccoddiropcao, lctoken, lcnrreceitacomp, upv_nrCodVac, lcEfrDescr
 STORE .F. TO lcvaldiploma
 STORE CTOT("") TO lcnrdatareceita
 SELECT fi


 lcpos = RECNO()
 lcnrreceitacomp = ALLTRIM(fi.receita) 
 lcLordem = fi.lordem


 IF EMPTY(lcfistamp)
    lcfistamp = fi.fistamp
 ENDIF
 IF EMPTY(ALLTRIM(lccodigo)) .OR.  .NOT. (TYPE("lcCodigo")=="C")
    RETURN .F.
 ENDIF
 SELECT fi
 IF  .NOT. (LEFT(fi.design, 1)==".")
    uf_perguntalt_chama("ESTA OPERA��O N�O � PERMITIDA.", "OK", "", 64)
    RETURN .F.
 ENDIF
 IF (RIGHT(ALLTRIM(fi.design), 18)=="*SUSPENSA RESERVA*")
    IF  .NOT. uf_perguntalt_chama("Esta venda est� marcada como *SUSPENSA RESERVA*."+CHR(13)+"Ao alterar o plano esta venda ser� processada normalmente."+CHR(13)+CHR(13)+"Pretende continuar?", "Sim", "N�o")
       RETURN .F.
    ENDIF
 ENDIF
 IF EMPTY(lcsemperguntas)
    SELECT fi
    IF LEFT(fi.design, 2)<>".S" .AND.  .NOT. ((RIGHT(ALLTRIM(fi.design), 18)=="*SUSPENSA RESERVA*")) 
       IF  .NOT. uf_perguntalt_chama("ESTA VENDA J� TEM UM PLANO DE COMPARTICIPA��O. DESEJA SUBSTITUIR O PLANO?"+CHR(13)+CHR(13)+"SIM: SUBSTITUI O PLANO ACTUAL ASSOCIADO � VENDA.", "Sim", "N�o")
          RETURN .F.
       ENDIF
    ENDIF
 ENDIF

 SELECT * FROM FI WITH (BUFFERING = .T.) ORDER BY lordem INTO CURSOR uc_tmpFI
 

 SELECT uc_tmpFI
 
 IF RECCOUNT("uc_tmpFI") > 1
   LOCATE FOR uc_tmpFI.fistamp = lcfistamp
   IF FOUND()
      SKIP
      uv_lordem = uc_tmpFI.lordem
      SCAN FOR uc_tmpFI.lordem >= uv_lordem
         IF LEFT(ALLTRIM(uc_tmpFI.design),1) = "."
             EXIT
         ENDIF

         IF !empty(uc_tmpFi.ref)
             IF !uf_faturacao_checkQttMaxPla(lccodigo, uc_tmpFi.qtt)
                 RETURN .F.
             ENDIF
         ENDIF
      ENDSCAN
   ENDIF
 ENDIF

 FECHA("uc_tmpFI")
 



 SELECT ucrsatendcomp
 GOTO TOP
 LOCATE FOR ALLTRIM(ucrsatendcomp.fistamp)==ALLTRIM(lcfistamp)
 IF FOUND()

    lccodacesso = ucrsatendcomp.codacesso
    lccoddiropcao = ucrsatendcomp.coddiropcao
    lctoken = ucrsatendcomp.token
    lcnomeutente = ucrsatendcomp.nomeutente
    lccontactoutente = ucrsatendcomp.contactoutente
    lcnomemedico = ucrsatendcomp.nomemedico
    lccontactomedico = ucrsatendcomp.contactomedico
    lcespecialidademedico = ucrsatendcomp.especialidademedico
    lclocalprescricao = ucrsatendcomp.localprescricao
    lcnrcartaosns = ucrsatendcomp.nrcartaosns
    lcnrcartao = ucrsatendcomp.nrcartao
    lcnrdatareceita = uCrsAtendComp.datareceita
    lcEfrDescr = ALLTRIM(uCrsAtendComp.efr_descr)

 ENDIF
 

 LOCAL lcplanotemp
 lcplanotemp = uf_atendimento_retornaplanocombinado(lccodigo)
 IF ( .NOT. EMPTY(lcplanotemp))
    lccodigo = lcplanotemp
 ENDIF
 IF  .NOT. uf_gerais_actgrelha("", 'uCrsTempComp', [exec up_receituario_dadosDoPlano ']+lccodigo+['])
    uf_perguntalt_chama("OCORREU UMA ANOMALIA A APLICAR O PLANO.", "OK", "", 16)
    RETURN .F.
 ELSE

	IF (uCrsTempComp.planoAtivo == 0)
		 uf_perguntalt_chama("O plano encontra-se inativo, por favor verifique o plano selecionado.", "OK", "", 16)
		RETURN .F.
	ENDIF
	
    IF RECCOUNT("uCrsTempComp")>0

      IF uCrsTempComp.obrigaRegistoVacinacao
         IF WEXIST("VACINACAO")
            VACINACAO.hide()
            VACINACAO.release()
         ENDIF
         uf_vacinacao_chama('', .T., .T.)
      ENDIF


    	IF(!EMPTY(uCrsTempComp.pergDem))
    		 uf_atendimento_perguntaSeReceitaRSP(lcLordem)
        ENDIF
	
       SELECT fi
       DELETE FROM uCrsAtendComp WHERE UPPER(ALLTRIM(fistamp))==UPPER(ALLTRIM(fi.fistamp))
       SELECT * FROM uCrsAtendComp UNION ALL SELECT * FROM uCrsTempComp INTO CURSOR uCrsAtendComp READWRITE
       

       
       SELECT ucrsatendcomp
       GOTO BOTTOM
       REPLACE ucrsatendcomp.fistamp WITH lcfistamp
       SELECT ucrsatendcomp
       LOCATE FOR UPPER(ALLTRIM(fistamp))==UPPER(ALLTRIM(lcfistamp))
       IF FOUND()
          IF  .NOT. EMPTY(tcbool)
             REPLACE ucrsatendcomp.codacesso WITH lccodacesso
             REPLACE ucrsatendcomp.coddiropcao WITH lccoddiropcao
             REPLACE ucrsatendcomp.token WITH lctoken           

             IF NOT EMPTY(lcnrdatareceita)
                REPLACE uCrsAtendComp.datareceita WITH lcnrdatareceita
             ENDIF

             IF VARTYPE(lcnomeutente)<>"U"

                REPLACE ucrsatendcomp.nomeutente WITH lcnomeutente
                REPLACE ucrsatendcomp.contactoutente WITH lccontactoutente
                REPLACE ucrsatendcomp.nomemedico WITH lcnomemedico
                REPLACE ucrsatendcomp.contactomedico WITH lccontactomedico
                REPLACE ucrsatendcomp.especialidademedico WITH lcespecialidademedico
                REPLACE ucrsatendcomp.localprescricao WITH lclocalprescricao
                REPLACE ucrsatendcomp.nrcartaosns WITH lcnrcartaosns
                REPLACE ucrsatendcomp.nrcartao WITH lcnrcartao
                REPLACE ucrsatendcomp.ncartao WITH lcnrcartao
                REPLACE ucrsatendcomp.efr_descr WITH lcEfrDescr
			
                SELECT fi
                REPLACE fi.u_nbenef WITH lcnrcartaosns
                SELECT ucrsatendcomp
             ENDIF
          ENDIF
       ENDIF
       
       uf_atendimento_validaNrSnsSNSExterno()
       IF TYPE("myChamaSinave")=="C"      		
	       IF ALLTRIM(myChamaSinave) = 'Sem Comparticipa��o' OR ALLTRIM(myChamaSinave) = 'Com Comparticipa��o'
	       		uf_comunicasinave_chama()
	       		myChamaSinave = 'N�o chama'
	       		
	       		if(USED("fi"))
	       			 SELECT fi
					 TRY
					    GOTO lcpos
					 CATCH
					 ENDTRY
	       		endif
	       		
	       ENDIF 
	   ELSE
	   		RELEASE myChamaSinave
	   		PUBLIC myChamaSinave
	   		STORE 'N�o chama' TO myChamaSinave
		ENDIF 
    ELSE
       uf_perguntalt_chama("N�O EXISTE NENHUM PLANO COM ESSE C�DIGO.", "OK", "", 48)
       RETURN .F.
    ENDIF
 ENDIF

 SELECT ft2
 REPLACE ft2.u_receita WITH ''
 
  

 
local filordempesqr, nrrecfipesq
nrrecfipesq = ''
filordempesqr=fi.lordem
select fi.receita from fi WHERE LEFT(astr(fi.lordem), 2)=LEFT(astr(filordempesqr), 2) and !empty(alltrim(fi.receita)) into cursor finrrec readwrite
SELECT finrrec 
IF len(alltrim(finrrec.receita))!=0
	nrrecfipesq=ALLTRIM(finrrec.receita)
ENDIF 

 LOCAL lcreservachk
 STORE .F. TO lcreservachk

 IF USED("uCrsVale")
    SELECT ucrsvale
    IF AT("Reserva", ALLTRIM(ucrsvale.design))<>0
       lcreservachk = .T.
    ENDIF
 ENDIF



 IF EMPTY(ALLTRIM(ucrstempcomp.codigo2))

    SELECT fi
    lcreceitaanterior = ALLTRIM(STREXTRACT(fi.design, ':', ' -', 1, 0))
    

    IF len(alltrim(lcreceitaanterior))=0 AND  len(alltrim(nrrecfipesq))!=0
    	lcreceitaanterior = ALLTRIM(nrrecfipesq)
    ENDIF 
    IF len(alltrim(lcreceitaanterior))!=0 AND EMPTY(fi.receita)
	    replace fi.receita WITH alltrim(lcreceitaanterior)
	ENDIF 
	
	
	
	IF TYPE("myNrRecSinave")!="U" AND LEN(myNrRecSinave)>0
		REPLACE fi.receita WITH alltrim(myNrRecSinave)
		REPLACE fi.design WITH ".["+ALLTRIM(lccodigo)+"]["+ALLTRIM(ucrstempcomp.cptorgabrev)+"] Receita:"+ALLTRIM(myNrRecSinave)+" -"
		myNrRecSinave = ''
	ELSE
	    REPLACE fi.design WITH ".["+ALLTRIM(lccodigo)+"]["+ALLTRIM(ucrstempcomp.cptorgabrev)+"] Receita:"+ALLTRIM(fi.receita)+" -"
	ENDIF 
    REPLACE fi.tipor WITH IIF(EMPTY(fi.dem), 'RM', fi.tipor)
    

 ELSE

    SELECT fi
    lcreceitaanterior = ALLTRIM(STREXTRACT(fi.design, ':', ' -', 1, 0))
 
    IF len(alltrim(lcreceitaanterior))=0 AND ALLTRIM(fi.receita)<>''
    	lcreceitaanterior = ALLTRIM(fi.receita)
    ENDIF 
    IF len(alltrim(lcreceitaanterior))=0 AND len(alltrim(nrrecfipesq))!=0
    	lcreceitaanterior = ALLTRIM(nrrecfipesq)
    ENDIF 
    

    IF TYPE("myNrRecSinave")!="U" AND LEN(myNrRecSinave)>0
    	REPLACE fi.receita WITH alltrim(myNrRecSinave)
    	REPLACE fi.design WITH ".["+ALLTRIM(lccodigo)+"]["+ALLTRIM(ucrstempcomp.cptorgabrev)+"]["+ALLTRIM(ucrstempcomp.codigo2)+"]["+ALLTRIM(ucrstempcomp.cptorgabrev2)+"] Receita:"+ALLTRIM(myNrRecSinave)+" -"
    	myNrRecSinave = ''
    ELSE 
	    REPLACE fi.design WITH ".["+ALLTRIM(lccodigo)+"]["+ALLTRIM(ucrstempcomp.cptorgabrev)+"]["+ALLTRIM(ucrstempcomp.codigo2)+"]["+ALLTRIM(ucrstempcomp.cptorgabrev2)+"] Receita:"+ALLTRIM(lcreceitaanterior)+" -"
	ENDIF 
    REPLACE fi.tipor WITH IIF(EMPTY(fi.dem), 'RM', fi.tipor)
    REPLACE fi.comp_tipo_2 WITH 1
 ENDIF

 SELECT ucrsatendcl
 SELECT fi
 REPLACE fi.u_nbenef2 WITH ucrsatendcl.nbenef2

 IF USED("uCrsAtendComp")
    SELECT ucrsatendcomp
    IF (ALLTRIM(ucrsatendcomp.dplmsstamp)=="01") .OR. (ALLTRIM(ucrsatendcomp.dplmsstamp)=="02")
       uf_atendimento_appdipesp(ucrsatendcomp.dplmsstamp)
       lcvaldiploma = .T.
    ENDIF
 ENDIF

 IF (RIGHT(ALLTRIM(fi.design), 18)=="*SUSPENSA RESERVA*")
    lcreservachk = .T.
 ENDIF

  IF lcreservachk=.T.
  	
    LOCAL lposlinr, lclordemr, lcbistampr, lcfistampr, lcnrrec
    lcbistampr = ''
    lcnrrec = ''
    SELECT fi
    lposlinr = RECNO("fi")
    lclordemr = fi.lordem
    lcfistampr = fi.fistamp
  
    
         
   	  LOCAL lnCabOrdem, lnVendaOrdemMax, lcCabFistamp
   	  STORE 0 TO lnCabOrdem, lnVendaOrdemMax
   	  STORE '' TO lcCabFistamp
      IF uf_atendimento_getLordemCabFi(lclordemr)

      	if(USED("ucrsCabInfoFi"))
      		SELECT ucrsCabInfoFi
      		GO TOP
      		lnCabOrdem       = ucrsCabInfoFi.lordemCabPos
      		lnVendaOrdemMax  = ucrsCabInfoFi.lordemCabFinalPos
      		lcCabFistamp     = ALLTRIM(ucrsCabInfoFi.fistampCab)   
      		fecha("ucrsCabInfoFi")
      	ENDIF
      
      ENDIF
     

    
    SELECT * FROM fi  WHERE fi.lordem >= lnCabOrdem AND fi.lordem <=lnVendaOrdemMax INTO CURSOR UcrsLinR READWRITE
    SELECT ucrslinr
    GOTO TOP
    SCAN
       IF  .NOT. EMPTY(ucrslinr.bistamp) .AND. EMPTY(lcbistampr)
          lcbistampr = ALLTRIM(ucrslinr.bistamp)
       ENDIF
    ENDSCAN
    
    
    fecha("UcrsLinR")
    IF  !EMPTY(lcbistampr)
       TEXT TO lcsql TEXTMERGE NOSHOW
		
				SELECT bi2.nrreceita, bi2.fistamp_insercao, bo.nmdos, ref, bo.no, bo.estab, bo.ousrhora, convert(varchar, bi.ousrdata, 112) as biData, RTRIM(LTRIM(ISNULL(bo2.u_hclstamp,''))) as u_hclstamp
				from bi (nolock)				
				inner join bo (nolock) on bo.bostamp=bi.bostamp 
				left join bi2 (nolock) on bi.bistamp=bi2.bi2stamp  
				left join bo2 (nolock) on bo2.bo2stamp=bo.bostamp						
				where bi.bistamp='<<ALLTRIM(lcBistampR)>>'
             
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "UcrsResRec", lcsql)
          uf_perguntalt_chama("N�o foi poss�vel os dados da reserva para a receita. Contacte o suporte.", "", "OK", 32)
          RETURN .F.
       ENDIF
       IF ALLTRIM(ucrsresrec.nmdos)='Reserva de Cliente'  AND !EMPTY(ALLTRIM(ucrsresrec.fistamp_insercao))
          LOCAL lcref1, lcno1, lcestab1, lcousrhora1, lcBidata, lcSqlReceita, lbIdDem, lcStampInsercaoReceita, lcOrdemCab
          
          lcBidata=''
          lcSqlReceita = ''
          lbIdDem =.f.
          
          lcref1 = ALLTRIM(ucrsresrec.ref)
          lcno1 = ucrsresrec.no
          lcestab1 = ucrsresrec.estab
          lcousrhora1 = ucrsresrec.ousrhora
          lcBidata = ucrsresrec.biData
          lcStampInsercaoReceita = ALLTRIM(ucrsresrec.fistamp_insercao)
        
     
         
          if(!EMPTY(lcStampInsercaoReceita))
          
	           TEXT TO lcSqlReceita  TEXTMERGE NOSHOW
					select TOP 1 u_receita, ft2.u_nbenef, codAcesso, codDirOpcao, u_design, u_codigo, token ,id_Dispensa_Eletronica_D  as id_validacaoDem, c2codpost
					from fi (nolock)
					inner join ft2(nolock) on  fi.ftstamp = ft2.ft2stamp
					where fi.lrecno = '<<lcStampInsercaoReceita>>'
								
              ENDTEXT
                   
	          uf_gerais_actgrelha("", "uCrsdadosreceita", lcSqlReceita  )
	          
	           **se � DEM
               if(!EMPTY(ALLTRIM(ucrsdadosreceita.token)))
               		lbIdDem  = .t.
               ENDIF
        
	          lcnrrec = ALLTRIM(ucrsdadosreceita.u_receita)
	          SELECT ucrsatendcomp
	          GOTO BOTTOM
	          REPLACE ucrsatendcomp.fistamp WITH lcfistampr
	          REPLACE ucrsatendcomp.codacesso WITH ucrsdadosreceita.codacesso
	          REPLACE ucrsatendcomp.coddiropcao WITH ucrsdadosreceita.coddiropcao
	          REPLACE ucrsatendcomp.token WITH ucrsdadosreceita.token
	          REPLACE ucrsatendcomp.id_validacaodem WITH ucrsdadosreceita.id_validacaodem
	          
	          
              IF(!EMPTY(ALLTRIM(ucrsdadosreceita.c2codpost)))
               		REPLACE ucrsatendcomp.ncartao WITH ALLTRIM(ucrsdadosreceita.c2codpost)
              ELSE
               		REPLACE ucrsatendcomp.ncartao WITH ALLTRIM(ucrsdadosreceita.u_nbenef)
              ENDIF
                
          
                   		

	          SELECT fi 
	          GO TOP
	          
	          UPDATE fi SET dem = lbIdDem , design = SUBSTR(ALLTRIM(design), 1, AT(":", ALLTRIM(design)))+ALLTRIM(ucrsdadosreceita.u_receita)+" -", token = ucrsdadosreceita.token WHERE  uf_gerais_compStr(fistamp,lcCabFistamp) 
	          UPDATE fi SET dem = lbIdDem  , receita = ALLTRIM(ucrsdadosreceita.u_receita), token = ucrsdadosreceita.token, id_validacaodem = ucrsdadosreceita.id_validacaodem WHERE fi.lordem >= lnCabOrdem AND fi.lordem <=lnVendaOrdemMax  
	          
	          
	          
	          fecha("uCrsdadosreceita")
             
             
          ENDIF
          
       ENDIF
       fecha("UcrsResRec")
    ENDIF
    

    SELECT fi
    TRY
       GOTO lposlinr
    CATCH
    ENDTRY
 ENDIF
  
 IF tcbool
 	
    IF TYPE("reservas")=='U' .AND. lcreservachk=.F.
      
       IF mypedelocalpresc==.T.
          uf_atendimento_dadosreceita("Atendimento.pgfDados.page3.txtRvpstamp", .F.)
       ENDIF
       IF mypedecodpresc==.T.
          uf_atendimento_dadosreceita("Atendimento.pgfDados.page3.txtSzzstamp", .F.)
       ENDIF

       IF  .NOT. EMPTY(ALLTRIM(lcreceitaanterior))
          SELECT fi
          REPLACE fi.design WITH STREXTRACT(fi.design, '.', ':', 1, 4)+LEFT(ALLTRIM(lcreceitaanterior), 19)+" -"+STREXTRACT(fi.design, ' -', '', 1, 2)
          REPLACE fi.tipor WITH IIF(EMPTY(fi.dem), 'RM', fi.tipor)
          SELECT ft2
          REPLACE ft2.u_receita WITH LEFT(ALLTRIM(lcreceitaanterior), 19)
       ELSE
          uf_atendimento_pedenrreceitafo(UPPER(ALLTRIM(ucrstempcomp.cptorgabrev)), UPPER(ALLTRIM(ucrstempcomp.cptorgabrev2)), ucrstempcomp.obriga_nrreceita, lcfistamp, upv_nrCodVac)
       ENDIF
      
     
       
       IF USED("uCrsRegVendasTemp") 	
          IF ALLTRIM(ucrsregvendastemp.nmdoc)<>'Reserva de Cliente'
             uf_atendimento_pedenrcartaocompart(ucrstempcomp.cartao, ucrstempcomp.maxembcartao)
          ENDIF
       ELSE
          uf_atendimento_pedenrcartaocompart(ucrstempcomp.cartao, ucrstempcomp.maxembcartao)
       ENDIF

       uf_atendimento_coduniemb()
    ENDIF
    uf_atendimento_dadoscab()
    uf_atendimento_comparticipaprod()
    IF EMPTY(ucrsatendcomp.codacesso)
       IF  .NOT. lcvaldiploma .AND.  .NOT. tcreservas
          uf_atendimento_limpadiploma()
       ENDIF
    ENDIF
    SELECT fi
    TRY
       GOTO lcpos
    CATCH
    ENDTRY
    


    
    SELECT fi
	 IF !EMPTY(fi.receita)
	 	lcnrreceitacomp = ALLTRIM(fi.receita)
	 	SELECT ucrsatendcomp
	 	replace ucrsatendcomp.nrreceita WITH lcnrreceitacomp 
	 ENDIF 
 
    uf_atendimento_actvalorescab()
    uf_atendimento_infototaiscab()
    uf_atendimento_infototais()

 ENDIF
 


 IF USED("uCrsTempComp")
 
 	fecha("UcrsProdComp")
 	SELECT uCrsTempComp
 	lcsql = ''
	TEXT TO lcsql TEXTMERGE NOSHOW
				SELECT *
				from cptorg_dep_prod (nolock)										
				where cptorg_dep_prod.cptorgabrev='<<ALLTRIM(uCrsTempComp.cptorgabrev)>>'
     ENDTEXT
     IF  .NOT. uf_gerais_actgrelha("", "UcrsProdComp", lcsql)
        uf_perguntalt_chama("N�o foi poss�vel os dados da reserva para a receita. Contacte o suporte.", "", "OK", 32)
        RETURN .F.
     ENDIF
    
    if(USED("UcrsProdComp") and RECCOUNT("UcrsProdComp")>0)	
  
    	
		LOCAL lcLordemCab
	 	SELECT UcrsProdComp
	 	GOTO TOP 
	 	
	 	SELECT fi
	 	lcLordemCab = LEFT(ALLTRIM(STR(fi.lordem)),2)
	 	SELECT ref FROM fi WHERE fi.ref =  UcrsProdComp.ref AND LEFT(ALLTRIM(STR(fi.lordem)),2) = lcLordemCab INTO CURSOR UcrsProdCompAux
	 	
	 	SELECT UcrsProdCompAux
	 	GOTO TOP 
	 	IF RECCOUNT("UcrsProdCompAux")  =  0

			Atendimento.txtscanner.value = ''
			Atendimento.txtscanner.setfocus()
			Atendimento.txtscanner.value = UcrsProdComp.ref
			Atendimento.txtscanner.lostfocus()
			Atendimento.txtscanner.value = ''
	 	ENDIF
	 	
	 	fecha("UcrsProdComp")
	 	fecha("UcrsProdCompAux")
	    fecha("uCrsTempComp")
    ENDIF
    
 ENDIF
 uf_atendimento_dadoscab()
 SELECT fi
 TRY
    GOTO lcpos
 CATCH
 ENDTRY
 
 SELECT * FROM fi WITH (BUFFERING = .T.) INTO CURSOR uc_tempFiComp

 SELECT uc_tempFiComp
 GO TOP
 COUNT TO uv_ttNeg FOR !DELETED() AND etiliquido < 0
	

 FECHA("uc_tempFiComp")



 IF uv_ttNeg > 0
	uf_perguntalt_chama("N�O PODE EFETUAR DESCONTOS SUPERIORES AO TOTAL DA LINHA! POR FAVOR RETIFIQUE.", "OK", "", 48)
	uf_atendimento_removecompart(.T.)
	RETURN .F.
 ENDIF
 
 

 SELECT ucrsatendcomp
 LOCATE FOR UPPER(ALLTRIM(fistamp))==UPPER(ALLTRIM(lcfistamp))
 
 atendimento.grdatend.setfocus()
   

 IF USED("uCrsAtendComp")
    LOCAL lcdesignc, lcncartaoc
    STORE '' TO lcdesignc, lcncartaoc
    SELECT ucrsatendcomp
    GOTO TOP
    SCAN
       IF EMPTY(lcdesignc)
          lcdesignc = ALLTRIM(ucrsatendcomp.design)
          lcncartaoc = ALLTRIM(ucrsatendcomp.ncartao)
       ELSE
          IF ALLTRIM(lcdesignc)=ALLTRIM(ucrsatendcomp.design) .AND.  .NOT. EMPTY(lcncartaoc) .AND. EMPTY(ucrsatendcomp.ncartao)
             REPLACE ucrsatendcomp.ncartao WITH ALLTRIM(lcncartaoc)
             lcncartaoc = ''
             lcdesignc = ALLTRIM(ucrsatendcomp.design)
          ENDIF
       ENDIF
    ENDSCAN
 ENDIF
 
  

 
 IF uf_gerais_getparameter("ADM0000000267", "BOOL")
    uf_atendimento_conferir_limpar()
 ELSE
    uf_atendimento_salvar_limpar()
 ENDIF
 IF usamancompart
 ENDIF
 atendimento.refresh()
 
  


 select FI
 LOCATE FOR ALLTRIM(FI.fistamp) == ALLTRIM(lcfistamp)
 
ENDFUNC
**
PROCEDURE uf_atendimento_pedenrcartaocompart
 LPARAMETERS lccartao, lcmaxembcartao
 IF !lccartao
	RETURN .F.
 ENDIF

 uv_valIdade = uf_atendimento_validaIdade()
 	
 IF uv_valIdade <> 2
 	
 	uf_atendimento_dadosreceita("CARTAOCOMPART", .F.)
	SELECT ucrsatendcomp
    IF  .NOT. EMPTY(ALLTRIM(ucrsatendcomp.ncartao))
	    IF ucrsatendcomp.valida_card==.T. .AND. (ucrsatendcomp.e_compart==.F. OR (ucrsatendcomp.e_compart=.t. AND uv_valIdade <> 0))
    		IF  .NOT. EMPTY(uf_atendimento_validacardid(ALLTRIM(ucrsatendcomp.ncartao), ucrsatendcomp.e_compart))
				atendimento.pgfdados.page3.txtncartaocompart.disabledbackcolor = RGB(255, 255, 255)
				atendimento.refresh
			ELSE
				atendimento.pgfdados.page3.txtncartaocompart.disabledbackcolor = RGB(241, 196, 15)
				atendimento.refresh
			ENDIF
		ENDIF
    ENDIF

 ELSE

	uf_atendimento_removecompart(.T.)

 ENDIF
 
ENDPROC
**
PROCEDURE uf_atendimento_verificalocalpresc
 SELECT fi
 IF  .NOT. EMPTY(ALLTRIM(fi.rvpstamp))
    IF UPPER(ALLTRIM(ucrsatendcomp.cptorgabrev))=="ADSE" .OR. UPPER(ALLTRIM(ucrsatendcomp.cptorgabrev2))=="ADSE"
       TEXT TO lcsql TEXTMERGE NOSHOW
				SELECT publico
				from b_cli_locais (nolock)
				where cod_local = <<VAL(RIGHT(ALLTRIM(fi.rvpstamp),LEN(ALLTRIM(fi.rvpstamp))-1))>>
       ENDTEXT
       IF uf_gerais_actgrelha("", "ucrsTempPublico", lcsql)
          IF RECCOUNT("ucrsTempPublico")>0 .AND. ucrstemppublico.publico
             IF uf_perguntalt_chama("O Local de Prescri��o seleccionado pertence a um organismo p�blico."+CHR(13)+CHR(13)+"Pretende substituir o plano actual pelo SNS?", "Sim", "N�o")
                mypedelocalpresc = .F.
                mypedecodpresc = .F.
                SELECT ucrsatendcomp
                IF UPPER(ALLTRIM(ucrsatendcomp.codigo))=="DJ" .OR. UPPER(ALLTRIM(ucrsatendcomp.codigo2))=="DJ"
                   uf_atendimento_aplicacompart("DS", .T.)
                ELSE
                   uf_atendimento_aplicacompart("01", .T.)
                ENDIF
                mypedelocalpresc = uf_gerais_getparameter('ADM0000000001', 'bool')
                mypedecodpresc = uf_gerais_getparameter('ADM0000000002', 'bool')
             ENDIF
          ENDIF
       ENDIF
    ENDIF
 ENDIF
ENDPROC
**
PROCEDURE uf_atendimento_appdipesp
 LPARAMETERS lcdip
 LOCAL lcposact
 lcposact = 1
 SELECT fi
 lcposact = RECNO()
 TRY
    SKIP
 CATCH
 ENDTRY
 DO WHILE  .NOT. EOF()
    IF LEFT(fi.design, 1)=="." .OR.  .NOT. EMPTY(fi.dem)
       EXIT
    ENDIF
    IF ALLTRIM(lcdip)=="01"
       REPLACE fi.u_diploma WITH "4521/01-Paramiloidose"
    ELSE
       REPLACE fi.u_diploma WITH "11387-A/03-Lupus"
    ENDIF
    TRY
       SKIP
    CATCH
    ENDTRY
 ENDDO
 TRY
    GOTO lcposact
 CATCH
 ENDTRY
ENDPROC
**
PROCEDURE uf_atendimento_comparticipaprod
 LOCAL lcpos
 SELECT fi
 lcpos = RECNO("fi")
 IF TYPE("ATENDIMENTO")<>"U"
    SELECT fi
    TRY
       SKIP
    CATCH
    ENDTRY
 ELSE
    SELECT fi
    GOTO TOP
 ENDIF
 SELECT fi
 DO WHILE  .NOT. EOF()
    IF LEFT(fi.design, 1)="."
       EXIT
    ENDIF
    REPLACE fi.u_comp WITH .T. IN fi
    TRY
       SKIP
    CATCH
    ENDTRY
 ENDDO
 SELECT fi
 TRY
    GOTO lcpos
 CATCH
 ENDTRY
ENDPROC
**
PROCEDURE uf_atendimento_limpadiploma
 LOCAL lcpos
 SELECT fi
 lcpos = RECNO("fi")
 IF TYPE("ATENDIMENTO")<>"U"
    SELECT fi
    TRY
       SKIP
    CATCH
    ENDTRY
 ELSE
    SELECT fi
    GOTO TOP
 ENDIF
 SELECT fi
 DO WHILE  .NOT. EOF()
    IF LEFT(fi.design, 1)="."
       EXIT
    ENDIF
    REPLACE fi.u_diploma WITH '' IN fi
    TRY
       SKIP
    CATCH
    ENDTRY
 ENDDO
 SELECT fi
 TRY
    GOTO lcpos
 CATCH
 ENDTRY
ENDPROC
**
PROCEDURE uf_atendimento_compartproduto
 LOCAL lccod
 lccod = ""
 SELECT fi
 IF fi.u_comp=.T.
    REPLACE fi.u_comp WITH .F.
    REPLACE fi.u_diploma WITH ''
    REPLACE fi.nccod WITH ''
    REPLACE fi.ndoc WITH 0
    REPLACE fi.marcada WITH .F.
    REPLACE fi.comp_tipo WITH 0
    REPLACE fi.comp WITH 0
    REPLACE fi.comp_diploma WITH 0
    myremovecompart = .T.
    uf_eventoactvalores()
    IF UPPER(mypaisconfsoftw)=='PORTUGAL'
       WITH atendimento.pgfdados.page2
          .btndiploma.label1.caption = 'APLICAR DIPLOMA'
          .btndiploma.Shape1.backcolor = RGB(0,  167, 231)
       ENDWITH
    ENDIF
 ELSE
    lccod = uf_atendimento_verificacompart()
    IF  .NOT. EMPTY(lccod)
       REPLACE fi.u_comp WITH .T.
       uf_eventoactvalores()
    ENDIF
 ENDIF
ENDPROC
**
PROCEDURE uf_atendimento_removecompart
 LPARAMETERS lcnemitepergunta

 LOCAL lctab, lctab2, lnalias
 LOCAL lccod, lcfipos
 lccod = ""
 
  
   ** valida se tem nrelegibilidade, se tiver n�o permite a remo��o de comparticipa��o
 IF UPPER(ALLTRIM(ucrse1.pais))<>'PORTUGAL'
	    if(USED("ft2"))
		    SELECT ft2
		    GO TOP
		    if(!EMPTY(tokencompart) and  !EMPTY(ALLTRIM(ft2.nrelegibilidade)))
		   		uf_perguntalt_chama("Por favor anule a eligiblidade antes de remover a comparticipa��o.", "OK", "", 16)
		   		RETURN .f.
		   	ENDIF	
	   	ENDIF
  ENDIF
	   	

 
 
 atendimento.tmroperador.reset()
 SELECT fi
 lcfipos = RECNO()
 SELECT fi
 IF LEFT(fi.design, 2)<>".S"
    IF lcnemitepergunta .OR. uf_perguntalt_chama("VAI ELIMINAR O PLANO DE COMPARTICIPA��O DESTA VENDA, PRETENDE CONTINUAR?", "Sim", "N�o")
     
		SELECT ft2
		REPLACE ft2.u_receita WITH ''
		SELECT fi
		REPLACE fi.design WITH ".SEM RECEITA"
		REPLACE fi.tipor WITH 'SR'
		lcordem = fi.lordem
		UPDATE FI SET dem = .F., token = '', u_diploma = "", u_comp = .F., fi.nccod = '', fi.ndoc = 0, fi.marcada = .F., fi.comp=0 WHERE fi.lordem>=lcordem .AND. fi.lordem<lcordem+100000 .AND. fi.marcada=.F.
		UPDATE FI SET dem = .F., token = '', u_diploma = "", u_comp = .F., fi.nccod = '', fi.comp=0  WHERE fi.lordem>=lcordem .AND. fi.lordem<lcordem+100000 .AND. fi.marcada=.T.
		TRY
			SKIP
		CATCH
		ENDTRY
		SELECT fi
		TRY
			GOTO lcfipos
		CATCH
		ENDTRY
		DELETE FROM uCrsAtendComp WHERE UPPER(ALLTRIM(fistamp))==UPPER(ALLTRIM(fi.fistamp))
		IF USED("ucrsverificarespostademtotal")
			DELETE FROM ucrsverificarespostademtotal WHERE ALLTRIM(ucrsverificarespostademtotal.receita_nr)==ALLTRIM(fi.receita)
		ENDIF 
		uf_atendimento_actvaloresfi()
		uf_atendimento_actvalorescab()
		IF(USED("fimancompart"))
			select fimancompart
			GO TOP
			delete from  fimancompart
		ENDIF 

    ENDIF

   	
    
 ENDIF
 

 
 
 uf_atendimento_infototaiscab()
 uf_atendimento_infototais()
 SELECT fi
 TRY
    GOTO lcfipos
 CATCH
 ENDTRY
 
 
 
 atendimento.refresh()
 atendimento.grdatend.setfocus
ENDPROC
**
PROCEDURE uf_atendimento_devolvevenda
 LOCAL lcpos, lcval
 STORE .F. TO lcval
 STORE 0 TO lcpos
 SELECT fi
 lcpos = RECNO("fi")
 SELECT fi
 SCAN
    IF  .NOT. lcval
       TRY
          GOTO lcpos
       CATCH
       ENDTRY
       TRY
          SKIP
       CATCH
       ENDTRY
       lcval = .T.
    ENDIF
    IF LEFT(fi.design, 1)<>"."
       uf_atendimento_devolver()
    ELSE
       EXIT
    ENDIF
 ENDSCAN
 SELECT fi
 TRY
    GOTO lcpos
 CATCH
    GOTO TOP
 ENDTRY
 atendimento.grdatend.setfocus
ENDPROC
**
FUNCTION uf_atendimento_devolver
 LOCAL lcvalida, lcrec
 STORE .F. TO lcvalida
 atendimento.tmroperador.reset()
 SELECT fi
 IF LEFT(fi.design, 1)<>"."
    IF  .NOT. EMPTY(fi.ofistamp)
       SELECT ucrsvale
       GOTO TOP
       SCAN FOR (ucrsvale.fistamp==fi.ofistamp)
          IF ucrsvale.qtt<>fi.qtt
             uf_perguntalt_chama("N�O PODE DEVOLVER UM PRODUTO COM QUANTIDADES DIFERENTES DAQUELAS A REGULARIZAR.", "OK", "", 64)
             RETURN .F.
          ENDIF
       ENDSCAN
       GOTO TOP
       IF (fi.partes==0)
          IF mydevolucoesdinheiro
             SELECT fi
             REPLACE fi.partes WITH 1
             SELECT ucrsvale
             GOTO TOP
             SCAN FOR ALLTRIM(ucrsvale.fistamp)==ALLTRIM(fi.ofistamp)
                REPLACE ucrsvale.dev WITH 1
                SELECT fi
                REPLACE fi.epv WITH ucrsvale.epv, fi.u_txcomp WITH ucrsvale.u_txcomp, fi.u_epvp WITH ucrsvale.u_epvp, fi.u_ettent1 WITH ucrsvale.u_ettent1, fi.u_ettent2 WITH ucrsvale.u_ettent2, fi.desconto WITH ucrsvale.desconto, fi.u_descval WITH ucrsvale.u_descval
				REPLACE fi.etiliquido WITH ucrsvale.etiliquido
             
                SELECT ucrsvale
             ENDSCAN
             SELECT ucrsvale
             GOTO TOP
             uf_fi_trans_info_muda_tipo(ALLTRIM(fi.fistamp), 'E')
             atendimento.lbldev.caption = "Dev. Dinheiro"
          ELSE
             SELECT fi
             REPLACE fi.partes WITH 2
             SELECT ucrsvale
             REPLACE ucrsvale.dev WITH 2 FOR ALLTRIM(ucrsvale.fistamp)==ALLTRIM(fi.ofistamp)
             GOTO TOP
             atendimento.lbldev.caption = "Dev. Cr�d."
          ENDIF
       ELSE
          IF fi.partes==1
             IF mydevolucoescredito
                SELECT fi
                REPLACE fi.partes WITH 2
                SELECT ucrsvale
                REPLACE ucrsvale.dev WITH 2 FOR ALLTRIM(ucrsvale.fistamp)==ALLTRIM(fi.ofistamp)
                GOTO TOP
                atendimento.lbldev.caption = "Dev. Cr�d."
             ELSE
                SELECT fi
                REPLACE fi.partes WITH 0
                SELECT ucrsvale
                REPLACE ucrsvale.dev WITH 0 FOR ALLTRIM(ucrsvale.fistamp)==ALLTRIM(fi.ofistamp)
                GOTO TOP
				uf_fi_trans_info_muda_tipo(ALLTRIM(fi.fistamp), 'S')
                atendimento.lbldev.caption = "DEVOLU��O"
             ENDIF
          ELSE
             SELECT fi
             REPLACE fi.partes WITH 0
             SELECT ucrsvale
             REPLACE ucrsvale.dev WITH 0 FOR ALLTRIM(ucrsvale.fistamp)==ALLTRIM(fi.ofistamp)
             GOTO TOP
			 uf_fi_trans_info_muda_tipo(ALLTRIM(fi.fistamp), 'S')
             atendimento.lbldev.caption = "DEVOLU��O"
          ENDIF
       ENDIF
       lcposicfi = RECNO("FI")
       uf_atendimento_eventofi()
       TRY
          GOTO lcposicfi
       CATCH
       ENDTRY
    ENDIF
 ENDIF
 SELECT fi
ENDFUNC
**
FUNCTION uf_atendimento_dadosreceita
 LPARAMETERS lcobj, lcobriganrreceita, lcfistamp, uv_obrigaPreencher, uv_prevReceita
 atendimento.tmroperador.reset()
 TRY
    SELECT ucrsatendcomp
    GOTO RECNO("uCrsAtendComp")
    SELECT fi
    GOTO RECNO("FI")
    SELECT ft2
    GOTO RECNO("ft2")
 CATCH
 ENDTRY
 IF EMPTY("lcObj") .OR.  .NOT. (TYPE("lcObj")=="C")
    uf_perguntalt_chama("O PAR�METRO: OBJECTO, N�O � V�LIDO.", "OK", "", 48)
    RETURN .F.
 ENDIF
 DO CASE
    CASE LIKE('*TXTRVPSTAMP*', UPPER(lcobj))
       uf_tecladoalpha_chama(lcobj, "Introduza o Local de Prescri��o:", .F., .F., 1)
       RETURN .T.
    CASE LIKE('*TXTSZZSTAMP*', UPPER(lcobj))
       uf_tecladoalpha_chama(lcobj, "Introduza o M�dico:", .F., .F., 1)
       RETURN .T.
    CASE LIKE('*CARTAOCOMPART*', UPPER(lcobj))
       uf_tecladoalpha_chama("uCrsAtendComp.nCartao", "Introduza o N� do Cart�o de Comparticipa��o:", .F., .F., 2)
	   IF !EMPTY(uCrsAtendComp.regexCartao)
			IF !uf_gerais_checkRegex(ALLTRIM(uCrsAtendComp.regexCartao), ALLTRIM(ucrsatendcomp.ncartao))
				uf_perguntalt_chama("Para esta comparticipa��o o N� do Cart�o ter� de ter o seguinte formato: " + chr(13) + ALLTRIM(uCrsAtendComp.regexCartaoSample), "OK", "", 48)
				SELECT ucrsatendcomp
				REPLACE ucrsatendcomp.ncartao WITH ''
            SELECT fi
            REPLACE fi.u_nbenef2 WITH ''
				RETURN .F.
			ENDIF
	   ENDIF
       SELECT fi
       REPLACE fi.u_nbenef2 WITH ucrsatendcomp.ncartao
       RETURN .T.
    CASE LIKE('*TXTCODPOSTAL', UPPER(lcobj))
       uf_tecladoalpha_chama("ft.codpost", "Introduza o c�digo postal do Utente:", .F., .F., 2)
       REPLACE ucrsatendcl.codpost WITH ALLTRIM(ft.codpost)
       RETURN .T.
    CASE LIKE('*TXTMORADA', UPPER(lcobj))
       uf_tecladoalpha_chama("ft.morada", "Introduza a morada do Utente:", .F., .F., 2)
       REPLACE ucrsatendcl.morada WITH ALLTRIM(ft.morada)
       RETURN .T.
    CASE LIKE('*NOMEADQUIRENTE*', UPPER(lcobj))
       uf_tecladoalpha_chama("uCrsAtendCl.u_nmutavi", "Introduza o nome do Adquirente:", .F., .F., 2)
       RETURN .T.
    CASE LIKE('*MORADAADQUIRENTE*', UPPER(lcobj))
       uf_tecladoalpha_chama("uCrsAtendCl.u_moutavi", "Introduza a morada do Adquirente:", .F., .F., 2)
       RETURN .T.
    CASE LIKE('*CODPOSTALADQUIRENTE*', UPPER(lcobj))
       uf_tecladoalpha_chama("uCrsAtendCl.u_cputavi", "Introduza o c�digo postal do Adquirente:", .F., .F., 2)
       RETURN .T.
    CASE LIKE('*DOCIDADQUIRENTE*', UPPER(lcobj))
       uf_tecladoalpha_chama("uCrsAtendCl.u_ndutavi", "Introduza o n� ID Civil", .F., .F., 2)
       SELECT uCrsAtendCl

*!*	       IF !uf_gerais_validaPattern("^[0-9]*$", alltrim(uCrsAtendCl.u_ndutavi)) AND UPPER(ALLTRIM(ucrse1.pais))=='PORTUGAL'
*!*	       
*!*	         REPLACE uCrsAtendCl.u_ndutavi WITH ''
*!*	         uf_perguntalt_chama("O n� ID Civil n�o pode conter letras nem caracteres especiais.", "OK", "", 48)
*!*	         RETURN .F.

*!*	       ENDIF

       **uf_atendimento_validatamanhobi()
       RETURN .T.
   CASE LIKE('*TXTTELEFADQUIRENTE', UPPER(lcobj))
      uf_tecladoalpha_chama(lcobj, "Introduza o Contacto do Adquirente:", .F., .F., 1)
      RETURN .T.
 ENDCASE
 IF (LEFT(fi.design, 1)==".") .AND. (LEFT(fi.design, 2)<>".S")
    DO CASE
       CASE LIKE('*TXTU_NBENEF', UPPER(lcobj))
          uf_tecladoalpha_chama(lcobj, "Introduza o Nr. Utente SNS:", .F., .F., 1)
          uf_atendimento_validatamanhonbenef()
       CASE LIKE('*TXTU_NBENEF2*', UPPER(lcobj))
          uf_tecladoalpha_chama(lcobj, "Introduza o Nr. Benefici�rio:", .F., .F., 1)
		  IF USED("UCRSATENDCOMP")
		  	SELECT UCRSATENDCOMP
			LOCATE FOR ALLTRIM(UCRSATENDCOMP.fistamp) = ALLTRIM(fi.fistamp)
			IF FOUND()
				SELECT UCRSATENDCOMP
				IF !EMPTY(UCRSATENDCOMP.regexCartao)
					IF !uf_gerais_checkRegex(ALLTRIM(uCrsAtendComp.regexCartao), ALLTRIM(&lcobj..VALUE))
						uf_perguntalt_chama("Para esta comparticipa��o o N� do Cart�o ter� de ter o seguinte formato: " + chr(13) + ALLTRIM(uCrsAtendComp.regexCartaoSample), "OK", "", 48)
						SELECT fi
    					REPLACE fi.u_nbenef2 WITH ""
					ENDIF
				ENDIF
			ENDIF
		  ENDIF
          uf_atendimento_validatamanhonbenef()
       CASE (LIKE('*BTNRECEITA*', UPPER(lcobj)) .AND. (ALLTRIM(fi.tipor)<>'RSP' .AND. ALLTRIM(fi.tipor)<>'RN')) .AND. TYPE("REGVENDAS")=="U"
          uf_tecladoalpha_chama(lcobj, "Introduza o Nr. da Receita:", .F., .F., 3, .T., uv_prevReceita)
		  IF !uf_gerais_checkNumReceita(ALLTRIM(&lcobj..CAPTION))
		  	uf_gerais_msgErroReceita(.T.)
		  	RETURN .F.
		  ENDIF
          **if(LEN(ALLTRIM(&lcobj..CAPTION))>19)
          **	 &lcobj..CAPTION = ""
          **	 uf_perguntalt_chama("O n� da receita tem que ter 19 digitos. Se n�o pretender colocar n� seleccione 0.", "OK", "", 48)
    	**	 RETURN .F.
          **ENDIF
          
          IF  .NOT. EMPTY(lcfistamp)
             SELECT fi
             LOCATE FOR ALLTRIM(fi.fistamp)==ALLTRIM(lcfistamp)
          ENDIF
          SELECT fi
          REPLACE fi.DESIGN WITH STREXTRACT(fi.DESIGN, '.', ':', 1, 4) + ALLTRIM(&lcobj..CAPTION) + " -" + STREXTRACT(fi.DESIGN, ' -', '', 1, 2)
          REPLACE fi.tipor WITH IIF(EMPTY(fi.dem), 'RM', fi.tipor)
          replace fi.receita WITH ALLTRIM(&lcobj..CAPTION)
          LOCAL nrrectmp 
          nrrectmp = ALLTRIM(&lcobj..CAPTION)
          lcposicfi = RECNO("FI")
          LOCAL lordecrec
          lordecrec = fi.lordem
          UPDATE fi SET receita=ALLTRIM(nrrectmp) WHERE LEFT(astr(fi.lordem), 2)=LEFT(astr(lordecrec), 2)
          SELECT fi 
          TRY
			    GOTO lcposicfi
			 CATCH
			 ENDTRY
          SELECT ft2
          REPLACE ft2.u_receita WITH ALLTRIM(&lcobj..CAPTION)
          uv_valida = uf_atendimento_validanrreceita(lcobriganrreceita, lordecrec)

         IF uv_obrigaPreencher AND !uv_valida
            RETURN .F.
         ENDIF

       CASE LIKE('*BTNVIA*', UPPER(lcobj))
          DO CASE
             CASE UPPER(&lcobj..label1.CAPTION) == "NORMAL"
                &lcobj..label1.CAPTION = "1� VIA"
                SELECT fi
                REPLACE fi.design WITH STREXTRACT(fi.design, '.', '-', 1, 4)+" 1� Via "+STREXTRACT(fi.design, '*', '*', 1, 4) IN fi
             CASE UPPER(&lcobj..label1.CAPTION) == "1� VIA"
                &lcobj..label1.CAPTION = "2� VIA"
                SELECT fi
                REPLACE fi.design WITH STREXTRACT(fi.design, '.', '-', 1, 4)+" 2� Via "+STREXTRACT(fi.design, '*', '*', 1, 4) IN fi
             CASE UPPER(&lcobj..label1.CAPTION) == "2� VIA"
                &lcobj..label1.CAPTION = "3� VIA"
                SELECT fi
                REPLACE fi.design WITH STREXTRACT(fi.design, '.', '-', 1, 4)+" 3� Via "+STREXTRACT(fi.design, '*', '*', 1, 4) IN fi
             OTHERWISE
                &lcobj..label1.CAPTION = "NORMAL"
                SELECT fi
                REPLACE fi.design WITH STREXTRACT(fi.design, '.', '-', 1, 4)+" Normal "+STREXTRACT(fi.design, '*', '*', 1, 4) IN fi
          ENDCASE
          atendimento.grdatend.refresh
          atendimento.grdatend.setfocus
       CASE LIKE('*TXTCODIGOACESSO', UPPER(lcobj)) .AND. (ALLTRIM(fi.tipor)<>'RSP' .AND. ALLTRIM(fi.tipor)<>'RN')
          uf_tecladoalpha_chama(lcobj, "Introduza o C�digo de Acesso:", .F., .F., 1)
       CASE LIKE('*TXTCODIGODIREITOOPCAO', UPPER(lcobj))
          uf_tecladoalpha_chama(lcobj, "Introduza o C�digo de Direito de Op��o:", .F., .F., 1)
       OTHERWISE
    ENDCASE
 ELSE
    uf_perguntalt_chama("N�O PODE ALTERAR ESTES DADOS EM VENDAS SEM COMPARTICIPA��O.", "OK", "", 48)
    RETURN .F.
 ENDIF
ENDFUNC
**
PROCEDURE uf_atendimento_validatamanhobi
	**IF ALLTRIM(ucrsatendcl.codigop)=="PT" .AND. (LEN(ALLTRIM(ucrsatendcl.u_ndutavi))>8 **.OR. (LEN(ALLTRIM(ucrsatendcl.u_ndutavi))<7))
	IF ALLTRIM(ucrsatendcl.codigop)=="PT" .AND. (LEN(ALLTRIM(ucrsatendcl.u_ndutavi))>8 )
    SELECT ucrsatendcl
    REPLACE ucrsatendcl.u_ndutavi WITH ''
    uf_perguntalt_chama("O N� ID Civil n�o � valido, por favor verifique!", "OK", "", 48)
 ENDIF
ENDPROC
**
PROCEDURE uf_atendimento_validatamanhonbenef
 SELECT fi
 IF LEN(ALLTRIM(fi.u_nbenef))>18
    uf_perguntalt_chama("Tamanho m�ximo do n� de Benefici�rio excedido.", "OK", "", 48)
    SELECT fi
    REPLACE fi.u_nbenef WITH ""
 ELSE
    SELECT fi
    REPLACE fi.u_nbenef WITH ALLTRIM(fi.u_nbenef)
 ENDIF
 IF LEN(ALLTRIM(fi.u_nbenef2))>18
    uf_perguntalt_chama("Tamanho m�ximo do n� de Benefici�rio 2 excedido.", "OK", "", 48)
    SELECT fi
    REPLACE fi.u_nbenef2 WITH ""
 ELSE
    SELECT fi
    REPLACE fi.u_nbenef2 WITH ALLTRIM(fi.u_nbenef2)
 ENDIF
 IF USED("ucrsAtendCL")
    SELECT ucrsAtendCL
    IF LEN(ALLTRIM(ucrsAtendCL.nbenef2))>18
        uf_perguntalt_chama("Tamanho m�ximo do n� de Benefici�rio 2 excedido.", "OK", "", 48)
        SELECT ucrsAtendCL
        REPLACE ucrsAtendCL.nbenef2 WITH ""
    ELSE
        SELECT ucrsAtendCL
        REPLACE ucrsAtendCL.nbenef2 WITH ALLTRIM(ucrsAtendCL.nbenef2)
    ENDIF
 ENDIF
ENDPROC
**
FUNCTION uf_atendimento_validaqttlinha
 LPARAMETERS lcfilordem, lcqttmax
 LOCAL lcposfi, lcvalidaqtt
 STORE .T. TO lcvalidaqtt
 lcposfi = RECNO("fi")
 SELECT fi
 GOTO TOP
 SCAN FOR ((LEFT(astr(fi.lordem), 2)=lcfilordem) .AND. LEFT(fi.design, 1)<>".")
    IF (fi.qtt>lcqttmax)
       lcvalidaqtt = .F.
       lcposfi = RECNO("fi")
    ENDIF
 ENDSCAN
 SELECT fi
 GOTO lcposfi
 RETURN lcvalidaqtt
ENDFUNC
**
FUNCTION uf_atendimento_validanrreceita
 LPARAMETERS lcobriganrreceita, lclordempesq
 IF EMPTY(lcobriganrreceita) .AND. ALLTRIM(ft2.u_receita)=="0"
    RETURN .F.
 ENDIF
 SELECT ft2
 IF !uf_gerais_checkNumReceita(ALLTRIM(ft2.u_receita)) &&LEN(ALLTRIM(ft2.u_receita))<>19
 	uf_gerais_msgErroReceita(.T.)
    **IF EMPTY(lcobriganrreceita)
    **   uf_perguntalt_chama("O n� da receita tem que ter 19 digitos. Se n�o pretender colocar n� seleccione 0.", "OK", "", 48)
    **ELSE
    **   uf_perguntalt_chama("O n� da receita tem que ter 19 digitos.", "OK", "", 48)
    **ENDIF
    SELECT fi
    REPLACE fi.design WITH STRTRAN(fi.design, ':'+ALLTRIM(ft2.u_receita), ":")
    SELECT ft2
    REPLACE ft2.u_receita WITH ""
    atendimento.grdatend.refresh
    atendimento.grdatend.setfocus
    RETURN .F.
 ENDIF
 SELECT ft2
 IF  .NOT. EMPTY(ALLTRIM(ft2.u_receita))
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			SELECT
				COUNT(ctltrctstamp) cont
			from
				ctltrct (nolock)
			where 
				receita = '<<ft2.u_receita>>'
    ENDTEXT
    uf_gerais_actgrelha("", "uCrsRecDup", lcsql)
    SELECT ucrsrecdup
    IF ucrsrecdup.cont>0
       IF  .NOT. uf_perguntalt_chama("J� existe uma receita com o mesmo n� guardada."+CHR(13)+"Prentende continuar?", "Sim", "N�o")
          SELECT fi
          REPLACE fi.design WITH STRTRAN(fi.design, ALLTRIM(ft2.u_receita), "")
          REPLACE fi.receita WITH ''
          SELECT ft2
          REPLACE ft2.u_receita WITH ""
          atendimento.grdatend.refresh
          atendimento.grdatend.setfocus
          RETURN .F.
       ENDIF
    ENDIF
 ENDIF
 SELECT * FROM fi INTO CURSOR uCrsTempFiValidaNrReceita WHERE LEFT(astr(fi.lordem), 2)!=LEFT(astr(lclordempesq), 2) READWRITE
 LOCAL lcposfi, lcval
 STORE .F. TO lcval
 SELECT ft2
 lcposfi = RECNO("ucrsTempFiValidaNrReceita")
 SELECT ucrstempfivalidanrreceita
 LOCATE FOR ALLTRIM(STREXTRACT(ucrstempfivalidanrreceita.design, ':', ' -', 1, 0))==ALLTRIM(ft2.u_receita)
 IF FOUND()
    uf_perguntalt_chama("J� existe uma receita com o mesmo n� nesta venda. S� poder� ocorrer se existirem regulariza��es."+CHR(13)+"Por favor verifique.", "OK", "", 48)
    lcval = .T.
 ENDIF
 IF USED("ucrsTempFiValidaNrReceita")
    fecha("ucrsTempFiValidaNrReceita")
 ENDIF
 atendimento.grdatend.refresh
 atendimento.grdatend.setfocus
ENDFUNC
**
FUNCTION uf_atendimento_suspendevenda
 LPARAMETERS lcreserva
 SELECT fi
 IF ALLTRIM(fi.tipor)='RSP' .OR. ALLTRIM(fi.tipor)='RN'
    uf_perguntalt_chama("N�O PODE FAZER UMA VENDA SUSPENSA COM MEDICAMENTOS DE UMA DEM!"+CHR(13)+"POR FAVOR VERIFIQUE.", "OK", "", 48)
    RETURN .F.
 ENDIF

 IF uf_gerais_getParameter_site('ADM0000000150', 'BOOL', mySite)
	SELECT fi
	IF ALLTRIM(fi.tipoR) == "RM" AND !lcreserva
      IF AT("*SUSPENSA RESERVA*", ALLTRIM(fi.design)) <> 0
   		uf_perguntalt_chama("N�o pode desmarcar uma venda como suspensa quando a mesma � '*SUSPENSA RESERVA*'","OK","",64)
	   	RETURN .F.
      ELSE
   		uf_perguntalt_chama("N�o s�o permitidas vendas suspensas com plano de comparticipa��o","OK","",64)
	   	RETURN .F.
      ENDIF
	ENDIF
 ENDIF
 
 SELECT fi
 LOCAL lcfilordemcovid
 lcfilordemcovid=LEFT(ALLTRIM(str(fi.lordem)),2)
 ** N�o permitir reservas em testes de covid
 SELECT * FROM fi WHERE ALLTRIM(fi.tipor)=='RM' AND AT('[RS]', ALLTRIM(fi.design))>0 AND LEFT(ALLTRIM(str(fi.lordem)),2)=ALLTRIM(lcfilordemcovid) INTO CURSOR ucrsficovidres
 IF RECCOUNT("ucrsficovidres")>0
 	uf_perguntalt_chama("N�O PODE FAZER VENDAS SUSPENSAS COM O PLANO DE COMPARTICIPA��O RS.", "OK", "", 64)
 	fecha("ucrsficovidres")
    RETURN .F.
 ENDIF 
 fecha("ucrsficovidres")
 
 STORE .F. TO lcvalidaqtt
 IF uf_atendimento_validacampanhas()==.F.
    RETURN .F.
 ENDIF
 SELECT fi
 atendimento.tmroperador.reset()
 IF (LEFT(fi.design, 1)==".") && s� faz para cabe�alho
    IF (RIGHT(ALLTRIM(fi.design), 18)=="*SUSPENSA RESERVA*")
       IF  .NOT. uf_perguntalt_chama("Esta venda est� marcada como *SUSPENSA RESERVA*."+CHR(13)+"Ao remover a suspens�o esta venda ser� processada normalmente."+CHR(13)+CHR(13)+"Pretende continuar?", "Sim", "N�o")
          RETURN .F.
       ENDIF
    ENDIF
    IF (RIGHT(ALLTRIM(fi.design), 10)=="*SUSPENSA*") .OR. (RIGHT(ALLTRIM(fi.design), 18)=="*SUSPENSA RESERVA*")
       lcdesignlen = LEN(ALLTRIM(fi.design))
       IF (RIGHT(ALLTRIM(fi.design), 10)=="*SUSPENSA*")
          REPLACE fi.design WITH SUBSTR(ALLTRIM(fi.design), 1, lcdesignlen-10) IN "FI"
          atendimento.pgfdados.page3.btnsuspender.Shape1.backcolor = RGB(0,  167, 231)
       ELSE
          REPLACE fi.design WITH SUBSTR(ALLTRIM(fi.design), 1, lcdesignlen-18) IN "FI"
          atendimento.pgfdados.page3.btnsuspender.Shape1.backcolor = RGB(0,  167, 231)
       ENDIF
    ELSE
       IF uf_atendimento_validaqttlinha(LEFT(astr(fi.lordem), 2), 1)==.F.
          uf_perguntalt_chama("Venda (S): deve selecionar 1 quantidade por linha."+CHR(13)+"Por favor verifique.", "OK", "", 48)
          RETURN .F.
       ENDIF
       IF lcreserva==.F.
          REPLACE fi.design WITH ALLTRIM(fi.design)+" *SUSPENSA*" IN "FI"
          atendimento.pgfdados.page3.btnsuspender.Shape1.backcolor = RGB(255, 94, 91)
       ELSE      
          REPLACE fi.design WITH ALLTRIM(fi.design)+" *SUSPENSA RESERVA*" IN "FI"

          atendimento.pgfdados.page3.btnsuspender.Shape1.backcolor = RGB(255, 94, 91)
          LOCAL lposlinr, lclordemr, lcbistampr, lcfistampr, lcnrrec
          lcbistampr = ''
          lcnrrec = ''
          SELECT fi
          lposlinr = RECNO("fi")
          lclordemr = fi.lordem
          lcfistampr = fi.fistamp
          
       	  LOCAL lnCabOrdem, lnVendaOrdemMax, lcCabFistamp
       	  STORE 0 TO lnCabOrdem, lnVendaOrdemMax
       	  STORE '' TO lcCabFistamp
	      IF uf_atendimento_getLordemCabFi(lclordemr)
	
	      	IF (USED("ucrsCabInfoFi"))
	      		SELECT ucrsCabInfoFi
	      		GO TOP
	      		lnCabOrdem       = ucrsCabInfoFi.lordemCabPos
	      		lnVendaOrdemMax  = ucrsCabInfoFi.lordemCabFinalPos
	      		lcCabFistamp     = ALLTRIM(ucrsCabInfoFi.fistampCab)   
	      		fecha("ucrsCabInfoFi")
		      ENDIF
	      
	      ENDIF
          
        	
          fecha("ucrslinr")	
          

          
          SELECT * FROM fi  WHERE fi.lordem >= lnCabOrdem AND fi.lordem <=lnVendaOrdemMax INTO CURSOR UcrsLinR READWRITE

          
          SELECT ucrslinr
          GOTO TOP
          SCAN
             IF  !EMPTY(ucrslinr.bistamp) AND  EMPTY(lcbistampr)
                lcbistampr = ALLTRIM(ucrslinr.bistamp)
             ENDIF
          ENDSCAN
          fecha("UcrsLinR")
          
          IF  .NOT. EMPTY(lcbistampr)
             TEXT TO lcsql TEXTMERGE NOSHOW
						SELECT bi2.nrreceita, bi2.fistamp_insercao, bo.nmdos, ref, bo.no, bo.estab, bo.ousrhora, convert(varchar, bi.ousrdata, 112) as biData, RTRIM(LTRIM(ISNULL(bo2.u_hclstamp,''))) as u_hclstamp
						from bi (nolock)				
						inner join bo (nolock) on bo.bostamp=bi.bostamp 
						left join bi2 (nolock) on bi.bistamp=bi2.bi2stamp  
						left join bo2 (nolock) on bo2.bo2stamp=bo.bostamp						
						where bi.bistamp='<<ALLTRIM(lcBistampR)>>'
             ENDTEXT
             IF  .NOT. uf_gerais_actgrelha("", "UcrsResRec", lcsql)
                uf_perguntalt_chama("N�o foi poss�vel os dados da reserva para a receita. Contacte o suporte.", "", "OK", 32)
                RETURN .F.
             ENDIF
           
        
             IF ALLTRIM(ucrsresrec.nmdos)='Reserva de Cliente' AND !EMPTY(ALLTRIM(ucrsresrec.fistamp_insercao))
                LOCAL lcref1, lcno1, lcestab1, lcousrhora1, lcBidata, lcHclstamp, lcStampInsercaoReceita, lcSqlReceita, lbIdDem
                lcBidata =''
                lcref1 = ALLTRIM(ucrsresrec.ref)
                lcno1 = ucrsresrec.no
                lcestab1 = ucrsresrec.estab
                lcousrhora1 = left(ucrsresrec.ousrhora,5)
                lcBidata = ucrsresrec.biData
                lcHclstamp = ALLTRIM(ucrsresrec.u_hclstamp)
                lcStampInsercaoReceita = ALLTRIM(ucrsresrec.fistamp_insercao)
         		lcSqlReceita = ''
         		lbIdDem = .f.
         		
         		
        
               
                IF !EMPTY(lcStampInsercaoReceita) 

                   TEXT TO lcSqlReceita  TEXTMERGE NOSHOW
							select TOP 1 u_receita, ft2.u_nbenef, codAcesso, codDirOpcao, u_design, u_codigo, token ,id_Dispensa_Eletronica_D  as id_validacaoDem, c2codpost
							from fi (nolock)
							inner join ft2(nolock) on  fi.ftstamp = ft2.ft2stamp
							where fi.lrecno = '<<lcStampInsercaoReceita>>'
								
                   ENDTEXT
                   

                   uf_gerais_actgrelha("", "uCrsdadosreceita", lcSqlReceita )
                   lcnrrec = ALLTRIM(ucrsdadosreceita.u_receita)
                   **se � DEM
                   if(!EMPTY(ALLTRIM(ucrsdadosreceita.token)))
                   		lbIdDem  = .t.
                   ENDIF
                   
                   
                  
                  
                   
                   SELECT ucrsatendcomp
                   GOTO BOTTOM
                   REPLACE ucrsatendcomp.fistamp WITH lcfistampr
                   REPLACE ucrsatendcomp.codacesso WITH ucrsdadosreceita.codacesso
                   REPLACE ucrsatendcomp.coddiropcao WITH ucrsdadosreceita.coddiropcao
                   REPLACE ucrsatendcomp.token WITH ucrsdadosreceita.token
                   REPLACE ucrsatendcomp.id_validacaodem WITH ucrsdadosreceita.id_validacaodem
                   
                   if(!EMPTY(ALLTRIM(ucrsdadosreceita.c2codpost)))
                   		REPLACE ucrsatendcomp.ncartao WITH ALLTRIM(ucrsdadosreceita.c2codpost)
                   	ELSE
                   		REPLACE ucrsatendcomp.ncartao WITH ALLTRIM(ucrsdadosreceita.u_nbenef)
                   	ENDIF
                   		
                   
      		
                   
                   
                   SELECT fi 
                   GO TOP
                   
                  
                   &&colocar na desigan��o do cabe�alho
                   UPDATE fi SET design = SUBSTR(ALLTRIM(design), 1, AT(":", ALLTRIM(design)))+ALLTRIM(ucrsdadosreceita.u_receita)+" -"+" *SUSPENSA RESERVA*",  token = ucrsdadosreceita.token WHERE uf_gerais_compStr(fistamp,lcCabFistamp) 
                  
                   && colocar os dados da receita em todas as linhas da venda
                   UPDATE fi SET dem = lbIdDem , receita = ALLTRIM(ucrsdadosreceita.u_receita), token = ucrsdadosreceita.token, id_validacaodem = ucrsdadosreceita.id_validacaodem WHERE fi.lordem >= lnCabOrdem AND fi.lordem <=lnVendaOrdemMax
                  
                  
                   && colocar se � comparticipado nas linhas n�o canelaho da venda
                   UPDATE fi SET u_comp = .T. WHERE LEFT(ALLTRIM(STR(fi.lordem)), 1)=LEFT(ALLTRIM(STR(lposlinr)), 1) .AND.  .NOT. EMPTY(ALLTRIM(fi.ref))
                   fecha("uCrsdadosreceita")
                   
                   SELECT fi
				   TRY
				      GOTO lcposicfi
				   CATCH
				   ENDTRY
                   
                   
                ELSE && sem receita na reserva retira a suspensa
                	uf_atendimento_removeSuspensaNome()
                ENDIF
                fecha("uCrsTemReceita")
             ENDIF
             fecha("UcrsResRec")
          ENDIF
          SELECT fi
          TRY
             GOTO lposlinr
          CATCH
          ENDTRY
       ENDIF
    ENDIF
 ENDIF
 IF uf_gerais_getparameter("ADM0000000267", "BOOL")
    uf_atendimento_conferir_limpar()
 ELSE
    uf_atendimento_salvar_limpar()
 ENDIF
 atendimento.refresh()
ENDFUNC
**

FUNCTION uf_atendimento_removeSuspensaNome

	if(!USED("fi"))
		RETURN .f.
	ENDIF
	 SELECT fi
	 IF (RIGHT(ALLTRIM(fi.design), 10)=="*SUSPENSA*") .OR. (RIGHT(ALLTRIM(fi.design), 18)=="*SUSPENSA RESERVA*")
       lcdesignlen = LEN(ALLTRIM(fi.design))
       IF (RIGHT(ALLTRIM(fi.design), 10)=="*SUSPENSA*")
          REPLACE fi.design WITH SUBSTR(ALLTRIM(fi.design), 1, lcdesignlen-10) IN "FI"
          atendimento.pgfdados.page3.btnsuspender.Shape1.backcolor = RGB(0,  167, 231)
       ELSE
          REPLACE fi.design WITH SUBSTR(ALLTRIM(fi.design), 1, lcdesignlen-18) IN "FI"
          atendimento.pgfdados.page3.btnsuspender.Shape1.backcolor = RGB(0,  167, 231)
       ENDIF
    ENDIF
    	
ENDFUNC




PROCEDURE uf_atendimento_limpacab
 LPARAMETERS lnapagacabecalho1
 LOCAL lcvalida, lcstamp, lcnrvendas, lcpos
 STORE 0 TO lcnrvendas, lcpos
 STORE .F. TO lcvalida
 STORE "x" TO lcstamp
 IF  .NOT. EMPTY(atendimento.txttotalvendas.value)
    lcnrvendas = atendimento.txttotalvendas.value
 ENDIF
 IF lcnrvendas>1
    CREATE CURSOR uCrsLimpaCab (stamp C(25), apaga L)
    SELECT fi
    GOTO TOP
    SCAN FOR fi.lordem<>100000 .OR. lnapagacabecalho1==.T.
       IF lcvalida
          IF LEFT(fi.design, 1)<>"."
             lcvalida = .F.
             UPDATE uCrsLimpaCab SET apaga = .F. WHERE stamp=lcstamp
          ENDIF
       ENDIF
       IF LEFT(fi.design, 1)=="."
          lcvalida = .T.
          lcstamp = ALLTRIM(fi.fistamp)
          INSERT INTO uCrsLimpaCab (stamp, apaga) VALUES (lcstamp, .T.)
       ENDIF
    ENDSCAN
    SELECT ucrslimpacab
    GOTO TOP
    SCAN FOR ucrslimpacab.apaga=.T.
       SELECT fi
       GOTO TOP
       SCAN FOR ALLTRIM(fi.fistamp)==ALLTRIM(ucrslimpacab.stamp)
          uf_atendimento_apagalinha(.T., .F., lnapagacabecalho1)
       ENDSCAN
       SELECT ucrslimpacab
    ENDSCAN
    fecha("uCrsLimpaCab")
    SELECT fi
    GOTO TOP
 ENDIF
ENDPROC
**
PROCEDURE uf_atendimento_preenche_codunicoembalagemfi
 LOCAL lcembcode, lcembtipocode
 SELECT fi
 uf_atendimento_retorna_codunicoembalagem(ALLTRIM(fi.u_codemb))
 SELECT ucrstempauthcodeemb
 GOTO TOP
 lcembtipocode = ucrstempauthcodeemb.tipo
 lcembcode = ALLTRIM(ucrstempauthcodeemb.authcode)
 IF (USED("ucrsTempAuthCodeEmb"))
    fecha("ucrsTempAuthCodeEmb")
 ENDIF
 IF (lcembtipocode==1)
    SELECT fi
    REPLACE u_codemb WITH ALLTRIM(lcembcode)
 ELSE
    SELECT fi
    REPLACE u_codemb WITH ALLTRIM(lcembcode)
    SELECT fi2
    REPLACE u_comb_emb_matrix WITH ALLTRIM(lcembcode)
 ENDIF
ENDPROC
**
FUNCTION uf_atendimento_eventofi
 LPARAMETERS tcbool

 
 LOCAL lcsql, txcomp, lccod, lcactlinpos, lcsusp, lcposfi
 STORE "" TO lcsql, lccod
 STORE 0 TO txcomp
 LOCAL lcpercadicseg
 lcpercadicseg = 0
 IF (USED("uCrsAtendComp"))
    SELECT ucrsatendcomp
    GOTO TOP
    IF ucrsatendcomp.percadic>0
       lcpercadicseg = ucrsatendcomp.percadic
    ENDIF
 ENDIF
 TRY
    SELECT fi
    GOTO RECNO("fi")
 CATCH
 ENDTRY
 SELECT fi
 IF RECNO("fi")<0
    GOTO 1
    RETURN .F.
 ENDIF
 SELECT fi
 IF LEFT(ALLTRIM(fi.design), 1)=="."
    RETURN .F.
 ENDIF
 SELECT fi
 lcactlinpos = RECNO("fi")
 IF  .NOT. (fi.epromo)

    SELECT fi
    lcposfi = RECNO("FI")
    IF TYPE("ATENDIMENTO")<>"U"
       lccod = uf_atendimento_verificacompart(.F.)
       lcsusp = uf_atendimento_verificasuspensa(.F.)
    ELSE
       lccod = uf_atendimento_verificacompart(.T.)
       lcsusp = uf_atendimento_verificasuspensa(.T.)
    ENDIF
    SELECT fi
    TRY
       GOTO lcposfi
    CATCH
    ENDTRY
    IF  .NOT. EMPTY(lccod) .AND.  .NOT. (ALLTRIM(fi.ref)=='R000001')

       IF (fi.marcada<>.T. .OR. tcbool==.T. .OR. lcsusp==.T.) .AND. fi.partes==0

          SELECT fi
          IF  .NOT. (LEFT(fi.design, 2)=="  ")
             REPLACE fi.design WITH "  "+ALLTRIM(fi.design)
          ENDIF
          SELECT fi
          TRY
             GOTO lcposfi
             REPLACE fi.u_comp WITH .T.
             lcref = IIF( .NOT. EMPTY(fi.ref), fi.ref, fi.oref)
             lcdiploma = fi.u_diploma
          	 
	         
	        **compart bonus 
	        uf_atendimento_devolveDadosBonusCompart()
		  	LOCAL lcIdExt, lcTipoBonusId 
		    STORE "" TO lcTipoBonusId, lcIdExt
		    SELECT ucrsFiBonusTemp
		    lcTipoBonusId = ALLTRIM(ucrsFiBonusTemp.bonusTipo)
		    lcIdExt = ALLTRIM(ucrsFiBonusTemp.idBonus)             
		    fecha("ucrsFiBonusTemp")
   
             
             SELECT fi
             lcposfi1 = RECNO("FI")
             IF lccod<>'DO'
                uf_receituario_makeprecos(lccod, lcref, lcref, "fi", lcdiploma, .T.,.F., .F.,lcTipoBonusId ,lcIdExt)
             ELSE
                IF  .NOT. EMPTY(lcdiploma)
                   uf_receituario_makeprecos(lccod, lcref, lcref, "fi", lcdiploma, .T.,.F., .F.,lcTipoBonusId ,lcIdExt)
                ENDIF
             ENDIF
             SELECT fi
             TRY
                GOTO lcposfi1
             CATCH
             ENDTRY
             uf_atendimento_fiiliq(.T.)
             SELECT fi
             IF (fi.epv<>0)
                IF (fi.u_epvp=0)
                   REPLACE fi.u_epvp WITH ROUND(fi.epv, 2)
                ENDIF
                IF (fi.qtt>0)
                   txcomp = ((fi.etiliquido+fi.u_descval+(fi.epv*fi.qtt*(fi.desconto/100)))*100)/(fi.u_epvp*fi.qtt)
                   REPLACE fi.u_txcomp WITH ROUND(txcomp, 2)
                ENDIF
             ELSE
                REPLACE fi.u_txcomp WITH 0
             ENDIF
          CATCH
          ENDTRY
       ENDIF
    ELSE

       SELECT fi
       IF  .NOT. (LEFT(fi.design, 2)=="  ")
          REPLACE fi.design WITH "  "+ALLTRIM(fi.design)
       ENDIF
      
       IF (fi.partes=0) .AND.  .NOT. (ALLTRIM(fi.ref)=='R000001')
          SELECT ucrsatendst
          LOCATE FOR ucrsatendst.ststamp==fi.fistamp
          IF  .NOT. fi.marcada=.T. OR (type("forcaalterapreco")<>'U' AND forcaalterapreco=.t.)
             IF  .NOT. fi.amostra OR (type("forcaalterapreco")<>'U' AND forcaalterapreco=.t.)
                IF lcpercadicseg=0
                   REPLACE fi.epv WITH ROUND(ucrsatendst.epv1, 2)
                ELSE
                   REPLACE fi.epv WITH ROUND(ucrsatendst.epv1*(1+(lcpercadicseg)), 2), fi.u_epvp WITH ROUND(ucrsatendst.epv1*(1+(lcpercadicseg)), 2) IN fi
                ENDIF

             ELSE
                REPLACE fi.epv WITH fi.u_epvp, fi.pvmoeda WITH fi.u_epvp, fi.pv WITH fi.u_epvp
             ENDIF
             REPLACE fi.u_ettent1 WITH 0, fi.u_ettent2 WITH 0, fi.u_txcomp WITH 100 IN fi
          ELSE
             REPLACE fi.u_ettent1 WITH 0, fi.u_ettent2 WITH 0, fi.u_txcomp WITH 100 IN fi
             
             &&altera precos de marcada, e deve usar pre�o actual
*!*	             IF  .NOT. fi.amostra
*!*	             	 
*!*		             IF lcpercadicseg=0
*!*		                 REPLACE fi.epv WITH ROUND(ucrsatendst.epv1, 2)
*!*		             ELSE
*!*		                 REPLACE fi.epv WITH ROUND(ucrsatendst.epv1*(1+(lcpercadicseg)), 2), fi.u_epvp WITH ROUND(ucrsatendst.epv1*(1+(lcpercadicseg)), 2) IN fi
*!*		             ENDIF
*!*	             ENDIF
             &&&&
		
          ENDIF
          
          IF type("forcaalterapreco")<>'U'
				forcaalterapreco1 = .f.
			ENDIF 
			
			uf_atendimento_fiiliq(.T.) 
       ENDIF
    ENDIF

    SELECT fi
    IF fi.amostra
       SELECT ucrsatendst
       LOCATE FOR ucrsatendst.ststamp==fi.fistamp
       IF FOUND()
          DO CASE
             CASE ucrsatendst.grphmgcode=='' .OR. ucrsatendst.grphmgcode=='GH0000'
                REPLACE fi.opcao WITH 'I'
             CASE fi.u_epvp>ucrsatendst.pvpmaxre
                REPLACE fi.opcao WITH 'S'
             OTHERWISE
                REPLACE fi.opcao WITH 'N'
          ENDCASE
       ENDIF
    ENDIF
    LOCAL lcobriga
    STORE .F. TO lcobriga
    IF USED("uCrsAtendComp")
       SELECT ucrsatendcomp
       LOCATE FOR UPPER(ALLTRIM(ucrsatendcomp.codigo))=UPPER(ALLTRIM(lccod))
       IF FOUND()
          IF ucrsatendcomp.obriga_codemb == .T.
             lcobriga = .T.
          ELSE
             lcobriga = .F.
          ENDIF
       ENDIF
    ELSE
       IF lccod=="AS" .OR. lccod=="WG" .OR. lccod=="HP" .OR. lccod=="WJ"
          lcobriga = .T.
       ENDIF
    ENDIF
    IF lcobriga==.T.
       IF USED("uCrsAtendComp")
          IF UPPER(ALLTRIM(lccod))=="WJ"
             PUBLIC cval1
             STORE '' TO cval1
             uf_tecladoalpha_chama("cval1", "Introduza o C�digo da Embalagem:", .F., .F., 0)
             SELECT fi
             TRY
                GOTO lcposfi
             CATCH
             ENDTRY
             REPLACE fi.u_codemb WITH ALLTRIM(cval1)
             uf_atendimento_preenche_codunicoembalagemfi()
             IF !uf_faturacao_checkLenEmb(ALLTRIM(lccod), ALLTRIM(fi.u_codemb))
                SELECT FI
                REPLACE fi.u_codemb WITH ''
             ENDIF
          ELSE
             SELECT fi
             SELECT ucrsatendst
             LOCATE FOR ucrsatendst.ststamp=fi.fistamp
             IF FOUND()
                IF ucrsatendst.coduniemb==.T. .AND. EMPTY(fi.u_codemb)
                   PUBLIC cval1
                   STORE '' TO cval1
                   uf_tecladoalpha_chama("cval1", "Introduza o C�digo da Embalagem:", .F., .F., 0)
                   SELECT fi
                   TRY
                       GOTO lcposfi
                   CATCH
                   ENDTRY
                   REPLACE fi.u_codemb WITH ALLTRIM(cval1)
                   uf_atendimento_preenche_codunicoembalagemfi()
                   IF !uf_faturacao_checkLenEmb(ALLTRIM(lccod), ALLTRIM(fi.u_codemb))
                        SELECT FI
                        REPLACE fi.u_codemb WITH ''
                   ENDIF
                ENDIF
             ENDIF
          ENDIF
       ELSE
          IF UPPER(ALLTRIM(lccod))=="WJ"
             uf_perguntalt_chama("Este documento tem produtos que obrigam � coloca��o do c�digo de embalagem. Por favor verifique.", "OK", "", 48)
          ELSE
             SELECT fi
             SELECT ucrsatendst
             LOCATE FOR ucrsatendst.ststamp=fi.fistamp
             IF FOUND()
                IF ucrsatendst.coduniemb==.T. .AND. EMPTY(fi.u_codemb)
                   uf_perguntalt_chama("Este documento tem produtos que obrigam � coloca��o do c�digo de embalagem. Por favor verifique.", "OK", "", 48)
                ENDIF
             ENDIF
          ENDIF
       ENDIF
    ENDIF
 ELSE
    REPLACE fi.design WITH "  "+ALLTRIM(fi.design)
 ENDIF
 uf_atendimento_infototais()
 LOCAL lnsaldo, llmostra, lcvalepos, lcfipos, lcetotal
 STORE 0 TO lnsaldo, lcvalepos, lcfipos, lcetotal
 STORE .F. TO llmostra
 IF USED("uCrsVale")
    uf_actualizavaloresvalere()
 ENDIF
 IF USED("uCrsFp")
    fecha('uCrsFp')
 ENDIF
 SELECT fi
 IF (lcactlinpos>0)
    TRY
       GOTO lcactlinpos
    CATCH
       GOTO BOTTOM
    ENDTRY
 ENDIF

 IF  .NOT. tcbool .AND. TYPE("ATENDIMENTO")<>"U"
    uf_atendimento_dadosst()
 ENDIF
ENDFUNC
**
PROCEDURE uf_eventoactvalores
 LOCAL lcsql, lccod, lcref, lccnp, lndiploma, txcomp, lcactpos, lcesusp, lcposfimp
 STORE "" TO lcsql, lcref, lccod, lccnp, lndiploma, lcesusp
 STORE 0 TO txcomp
 LOCAL lcpercadicseg
 lcpercadicseg = 0
 SELECT ucrsatendcomp
 GOTO TOP
 IF ucrsatendcomp.percadic>0
    lcpercadicseg = ucrsatendcomp.percadic
 ENDIF
 SELECT fi
 lcactpos = RECNO("fi")
 SELECT fi
 GOTO TOP
 SCAN
    IF LEFT(fi.design, 1)=="."
       lccod = STREXTRACT(fi.design, '[', ']', 1, 0)
       lcesusp = RIGHT(ALLTRIM(fi.design), 10)
    ELSE
       IF  .NOT. (LEFT(fi.design, 2)=="  ")
          SELECT fi
          REPLACE fi.design WITH "  "+ALLTRIM(fi.design)
       ENDIF
       IF  .NOT. (fi.epromo) .AND. (ALLTRIM(fi.ref)<>'5681903' .AND. ALLTRIM(fi.ref)<>'5771050')
          IF ( .NOT. EMPTY(lccod)) .AND. (fi.u_comp) .AND. fi.partes==0
          
             lcref = IIF( .NOT. EMPTY(fi.ref), fi.ref, fi.oref)
             lcdiploma = fi.u_diploma

	          **compart bonus
	        uf_atendimento_devolveDadosBonusCompart()
		  	LOCAL lcIdExt, lcTipoBonusId 
		    STORE "" TO lcTipoBonusId, lcIdExt
		    SELECT ucrsFiBonusTemp
		    lcTipoBonusId = ALLTRIM(ucrsFiBonusTemp.bonusTipo)
		    lcIdExt = ALLTRIM(ucrsFiBonusTemp.idBonus)             
		    fecha("ucrsFiBonusTemp")
          
             
             SELECT fi
             lcposfimp = RECNO("FI")
             IF UPPER(ALLTRIM(ucrse1.pais))=='PORTUGAL'
                IF lccod<>'DO'
                   uf_receituario_makeprecos(lccod, lcref, lcref, "fi", lcdiploma, .T.,.F., .F.,lcTipoBonusId ,lcIdExt)
                ELSE
                   IF  .NOT. EMPTY(lcdiploma)
                      uf_receituario_makeprecos(lccod, lcref, lcref, "fi", lcdiploma, .T.,.F., .F.,lcTipoBonusId ,lcIdExt)
                   ENDIF
                ENDIF
             ENDIF
             TRY
                GOTO lcposfimp
             CATCH
             ENDTRY
             uf_atendimento_fiiliq(.T.)
             SELECT fi
             IF (fi.epv<>0)
                IF (fi.u_epvp==0)
                   REPLACE fi.u_epvp WITH ROUND(fi.epv, 2)
                ENDIF
                IF (fi.qtt>0)
                   txcomp = ((fi.etiliquido+fi.u_descval+(fi.epv*fi.qtt*(fi.desconto/100)))*100)/(fi.u_epvp*fi.qtt)
                   REPLACE fi.u_txcomp WITH ROUND(txcomp, 2)
                ENDIF
             ELSE
                REPLACE fi.u_txcomp WITH 0
             ENDIF
          ELSE
      	
             IF fi.partes==0
                SELECT ucrsatendst
  
  
                LOCATE FOR ucrsatendst.ststamp==fi.fistamp
                SELECT fi
  
                IF  .NOT. fi.marcada==.T.  OR (type("forcaalterapreco")<>'U' AND forcaalterapreco=.t.)
                   IF  .NOT. fi.amostra  OR (type("forcaalterapreco")<>'U' AND forcaalterapreco=.t.)
                      IF lcpercadicseg=0
                         REPLACE fi.epv WITH ROUND(ucrsatendst.epv1, 2), fi.u_epvp WITH ROUND(ucrsatendst.epv1, 2) IN fi
                      ELSE
                         REPLACE fi.epv WITH ROUND(ucrsatendst.epv1*(1+(lcpercadicseg)), 2), fi.u_epvp WITH ROUND(ucrsatendst.epv1*(1+(lcpercadicseg)), 2) IN fi
                      ENDIF
                   ELSE
                      REPLACE fi.epv WITH fi.u_epvp, fi.pvmoeda WITH fi.u_epvp, fi.pv WITH fi.u_epvp IN fi
                   ENDIF
                   REPLACE fi.u_ettent1 WITH 0, fi.u_ettent2 WITH 0, fi.u_txcomp WITH 100, fi.u_comp WITH .F. IN fi
                ELSE
                   REPLACE fi.u_ettent1 WITH 0, fi.u_ettent2 WITH 0, fi.u_txcomp WITH 100, fi.epv WITH fi.u_epvp, fi.pvmoeda WITH fi.u_epvp, fi.pv WITH fi.u_epvp, fi.u_comp WITH .F. IN fi
                   
                   &&altera precos de marcada, e deve usar pre�o actual
*!*	                   IF  .NOT. fi.amostra &&se marcada deve alterar o pvp
*!*	                   	  IF lcpercadicseg=0
*!*	                         REPLACE fi.epv WITH ROUND(ucrsatendst.epv1, 2), fi.u_epvp WITH ROUND(ucrsatendst.epv1, 2) IN fi
*!*	                      ELSE
*!*	                         REPLACE fi.epv WITH ROUND(ucrsatendst.epv1*(1+(lcpercadicseg)), 2), fi.u_epvp WITH ROUND(ucrsatendst.epv1*(1+(lcpercadicseg)), 2) IN fi
*!*	                      ENDIF
*!*	                   ENDIF
                   &&&&
                   
                ENDIF
             ENDIF
*!*				IF type("forcaalterapreco")<>'U'
*!*					forcaalterapreco1 = .f.
*!*				ENDIF 
             
             uf_atendimento_fiiliq(.T.)
          ENDIF
       ENDIF
    ENDIF
    SELECT fi
 ENDSCAN
 uf_atendimento_infototais()
 IF USED("uCrsVale")
    uf_actualizavaloresvalere()
 ENDIF
 IF USED("uCrsFp")
    fecha("uCrsFp")
 ENDIF
 
 IF TYPE("ATENDIMENTO") <> "U"
 	atendimento.tmroperador.reset()
 ENDIF
 	
 SELECT fi
 IF lcactpos>0
    TRY
       GOTO lcactpos
    CATCH
       GOTO BOTTOM
    ENDTRY
 ENDIF
ENDPROC
**
FUNCTION uf_atendimento_actvalorescab
 LOCAL lcsql, lccod, lcref, lccnp, lndiploma, txcomp, lcactpos, lcesusp, lccount, lcvalida, lcIsDem
 STORE "" TO lcsql, lcref, lccod, lccnp, lndiploma, lcesusp
 STORE 0 TO txcomp, lccount
 STORE .F. TO lcvalida
 LOCAL lcpercadicseg
 lcpercadicseg = 0
 SELECT ucrsatendcomp
 GOTO TOP
 IF ucrsatendcomp.percadic>0
    lcpercadicseg = ucrsatendcomp.percadic
 ENDIF
 SELECT fi
 lcactpos = RECNO()
 IF LEFT(fi.design, 1)="."
    lccod = STREXTRACT(fi.design, '[', ']', 1, 0)
    lcesusp = RIGHT(ALLTRIM(fi.design), 10)
 ELSE
    RETURN .F.
 ENDIF
 SELECT fi
 SCAN
    IF lcvalida
       IF LEFT(fi.design, 1)=="."
          EXIT
       ENDIF
       IF  .NOT. (LEFT(fi.design, 2)=="  ")
          REPLACE fi.design WITH "  "+ALLTRIM(fi.design)
       ENDIF
       IF  .NOT. (fi.epromo)
          IF ( .NOT. EMPTY(lccod)) .AND. (fi.u_comp) .AND. fi.partes=0
             lcref = IIF( .NOT. EMPTY(fi.ref), fi.ref, fi.oref)
             lcdiploma = fi.u_diploma
         
	        **compart bonus
	        uf_atendimento_devolveDadosBonusCompart()
		  	LOCAL lcIdExt, lcTipoBonusId 
		    STORE "" TO lcTipoBonusId, lcIdExt
		    SELECT ucrsFiBonusTemp
		    lcTipoBonusId = ALLTRIM(ucrsFiBonusTemp.bonusTipo)
		    lcIdExt = ALLTRIM(ucrsFiBonusTemp.idBonus)             
		    fecha("ucrsFiBonusTemp")        

             
             
             IF lccod<>'DO'
                uf_receituario_makeprecos(lccod, lcref, lcref, "fi", lcdiploma, .T., .F., .F.,lcTipoBonusId,lcIdExt)
             ELSE
                IF  .NOT. EMPTY(lcdiploma)
                   uf_receituario_makeprecos(lccod, lcref, lcref, "fi", lcdiploma, .T., .F., .F.,lcTipoBonusId,lcIdExt)
                ENDIF
             ENDIF
             uf_atendimento_fiiliq(.T.)
             SELECT fi
             IF fi.epv<>0
                IF fi.u_epvp=0
                   REPLACE fi.u_epvp WITH ROUND(fi.epv, 2)
                ENDIF
                IF fi.qtt>0
                   txcomp = ((fi.etiliquido+fi.u_descval+(fi.epv*fi.qtt*(fi.desconto/100)))*100)/(fi.u_epvp*fi.qtt)
                   REPLACE fi.u_txcomp WITH ROUND(txcomp, 2)
                ENDIF
             ELSE
                REPLACE fi.u_txcomp WITH 0
             ENDIF
          ELSE
             IF (fi.partes=0)
                SELECT ucrsatendst
                LOCATE FOR ucrsatendst.ststamp=fi.fistamp
                IF  .NOT. fi.amostra
                   IF lcpercadicseg=0
                      REPLACE fi.epv WITH ROUND(ucrsatendst.epv1, 2), fi.u_epvp WITH ROUND(ucrsatendst.epv1, 2) IN fi
                   ELSE
                      REPLACE fi.epv WITH ROUND(ucrsatendst.epv1*(1+(lcpercadicseg)), 2), fi.u_epvp WITH ROUND(ucrsatendst.epv1*(1+(lcpercadicseg)), 2) IN fi
                   ENDIF
                ELSE
                   REPLACE fi.epv WITH fi.u_epvp, fi.pvmoeda WITH fi.u_epvp, fi.pv WITH fi.u_epvp IN fi
                ENDIF
                REPLACE fi.u_ettent1 WITH 0, fi.u_ettent2 WITH 0, fi.u_txcomp WITH 100 IN fi
                uf_atendimento_fiiliq(.T.)
             ENDIF
          ENDIF
       ENDIF
    ENDIF
    IF LEFT(fi.design, 1)=="." .AND. RECNO()==lcactpos
       lcvalida = .T.
    ENDIF
    SELECT fi
 ENDSCAN
 uf_atendimento_infototais()
 IF USED("uCrsVale")
    uf_actualizavaloresvalere()
 ENDIF
 IF USED("uCrsFp")
    fecha("uCrsFp")
 ENDIF

 SELECT fi
 IF lcactpos>0
    TRY
       GOTO lcactpos
    CATCH
    ENDTRY
 ENDIF
ENDFUNC
**
PROCEDURE uf_actualizavaloresvalere
 IF USED("uCrsVale")
    LOCAL lnsaldo, lcvalepos, lcfipos, lcetotal
    STORE 0 TO lnsaldo, lcvalepos, lcfipos, lcetotal
    SELECT fi
    CALCULATE SUM(fi.etiliquido) TO lcetotal FOR (fi.partes<>1 .AND. fi.marcada<>.T.) .OR. (fi.partes=0 .AND. fi.marcada=.T.)
    SELECT ucrsvale
    lcvalepos = RECNO()
    GOTO TOP
    SCAN
       IF (ucrsvale.lancacc<>.T.)
          lnsaldo = lnsaldo+ucrsvale.etiliquido
       ELSE
          IF (ucrsvale.lancacc==.T.) .AND. (ucrsvale.fmarcada)
             IF ucrsvale.etiliquido=ucrsvale.eaquisicao .OR. (ucrsvale.qtt<=ucrsvale.eaquisicao)
                lnsaldo = lnsaldo+ucrsvale.etiliquido
             ELSE
                lnsaldo = lnsaldo+(ucrsvale.eaquisicao*ucrsvale.etiliquido/ucrsvale.qtt)
             ENDIF
          ENDIF
       ENDIF
    ENDSCAN
    
    TRY
       GOTO lcvalepos
    CATCH
    ENDTRY
    IF  .NOT. (TYPE("ATENDIMENTO")=="U")
*!*	    	IF lcetotal-lnsaldo < 0 
*!*				SELECT ucrsvale
*!*				GOTO TOP 	
*!*				CALCULATE SUM(ucrsvale.etiliquido) TO lcetotal 
*!*	    	ENDIF
       	atendimento.txttotalutente.value = ROUND(lcetotal-lnsaldo, 2)
    ENDIF
 ENDIF
ENDPROC
**
FUNCTION uf_atendimento_painelatendimento
 IF (VARTYPE(atendimento)<>'U')
    RETURN .T.
 ENDIF
 RETURN .F.
ENDFUNC
**
FUNCTION uf_atendimento_verificacompart
 LPARAMETERS lcbool
 LOCAL lcfipos, lccod
 lccod = ""
 IF  .NOT. lcbool
    SELECT fi
    lcfipos = RECNO()
    DO WHILE  .NOT. BOF()
       IF LEFT(fi.design, 1)="."
          IF LEFT(fi.design, 2)<>".S"
             lccod = STREXTRACT(fi.design, '[', ']', 1, 0)
             EXIT
          ENDIF
          EXIT
       ENDIF
       TRY
          SKIP -1
       CATCH
       ENDTRY
    ENDDO
    SELECT fi
    IF (lcfipos>0)
       TRY
          GOTO lcfipos
       CATCH
          GOTO BOTTOM
       ENDTRY
    ENDIF
 ELSE
    SELECT ft2
    lccod = ft2.u_codigo
 ENDIF
 RETURN lccod
ENDFUNC
**
FUNCTION uf_atendimento_verificareservasuspensa
 LPARAMETERS lcbool
 LOCAL lcfipos, lcsusp
 STORE .F. TO lcsusp
 IF  .NOT. lcbool
    SELECT fi
    lcfipos = RECNO()
    DO WHILE  .NOT. BOF()
       IF LEFT(fi.design, 1)="."
          IF LEFT(fi.design, 2)<>".S"
             IF RIGHT(ALLTRIM(fi.design), 18)=="*SUSPENSA RESERVA*"
                lcsusp = .T.
             ENDIF
             EXIT
          ENDIF
          EXIT
       ENDIF
       TRY
          SKIP -1
       CATCH
       ENDTRY
    ENDDO
    SELECT fi
    IF lcfipos>0
       TRY
          GOTO lcfipos
       CATCH
          GOTO BOTTOM
       ENDTRY
    ENDIF
 ELSE
    SELECT ft
    lcsusp = ft.cobrado
 ENDIF
 RETURN lcsusp
ENDFUNC
**
FUNCTION uf_atendimento_verificasuspensa
 LPARAMETERS lcbool
 LOCAL lcfipos, lcsusp
 STORE .F. TO lcsusp
 IF  .NOT. lcbool
    SELECT fi
    lcfipos = RECNO()
    DO WHILE  .NOT. BOF()
       IF LEFT(fi.design, 1)="."
          IF LEFT(fi.design, 2)<>".S"
             IF RIGHT(ALLTRIM(fi.design), 10)=="*SUSPENSA*" .OR. RIGHT(ALLTRIM(fi.design), 18)=="*SUSPENSA RESERVA*"
                lcsusp = .T.
             ENDIF
             EXIT
          ENDIF
          EXIT
       ENDIF
       TRY
          SKIP -1
       CATCH
       ENDTRY
    ENDDO
    SELECT fi
    IF lcfipos>0
       TRY
          GOTO lcfipos
       CATCH
          GOTO BOTTOM
       ENDTRY
    ENDIF
 ELSE
    SELECT ft
    lcsusp = ft.cobrado
 ENDIF
 RETURN lcsusp
ENDFUNC
**
PROCEDURE uf_atendimento_validacaosaveerrosfi
 LPARAMETERS lcidline, lcerrorcode
 LOCAL lcposfiaux, lcfistamp
 lcfistamp = ''
 SELECT fi
 lcposfiaux = RECNO("fi")
 IF (USED("fi"))
    SELECT fi
    LOCATE FOR  .NOT. EMPTY(ALLTRIM(fi.id_validacaodem)) .AND. ALLTRIM(fi.id_validacaodem)==ALLTRIM(lcidline)
    IF (FOUND())
       lcfistamp = ALLTRIM(fi.fistamp)
    ENDIF
 ENDIF
 IF ( .NOT. EMPTY(ALLTRIM(lcfistamp)) .AND.  .NOT. EMPTY(ALLTRIM(lcerrorcode)))
    SELECT fi
    IF TYPE("fi.codigoDemResposta")<>'U'
       UPDATE fi SET codigodemresposta = lcerrorcode, tipoerro = 1 WHERE fistamp=lcfistamp
    ENDIF
 ENDIF
 SELECT fi
 TRY
    GOTO lcposfiaux
 CATCH
    GOTO TOP
 ENDTRY
ENDPROC
**
FUNCTION uf_atendimento_devolvejusterrosvalid
 LPARAMETERS lccode
 IF (ALLTRIM(lccode)=='C022' .OR. ALLTRIM(lccode)=='C021'  .OR. ALLTRIM(lccode)=='C023'  .OR. ALLTRIM(lccode)=='D146' )
    RETURN .T.
 ELSE
    RETURN .F.
 ENDIF
ENDFUNC
**
FUNCTION uf_atendimento_devolvejustlinha
 LOCAL lcposfiaux, lcfistamp, lccodedem, lcresult
 SELECT fi
 lcposfiaux = RECNO("fi")
 lccodedem = ''
 lcresult = .F.
 SELECT fi
 IF TYPE("fi.codigoDemResposta")<>'U'
    lccodedem = ALLTRIM(fi.codigodemresposta)
 ENDIF
 SELECT fi
 TRY
    GOTO lcposfiaux
 CATCH
    GOTO TOP
 ENDTRY
 lcresult = uf_atendimento_devolvejusterrosvalid(lccodedem)
 RETURN lcresult
ENDFUNC
**
FUNCTION uf_atendimento_devolvecorgrid
 LPARAMETERS lcgridtipo
 DO CASE
    CASE lcgridtipo==ALLTRIM("tipoR")
       IF (uf_atendimento_devolvejustlinha())
          RETURN RGB(255, 0, 0)
       ELSE
          RETURN .F.
       ENDIF
    OTHERWISE
       RETURN .F.
 ENDCASE
ENDFUNC
**
PROCEDURE uf_atendimento_forceupdategrid
 IF (USED("fi") .AND. VARTYPE(atendimento.grdatend)<>'U')
    SELECT fi
    GOTO TOP
    SCAN
       uf_atendimento_configgrid()
    ENDSCAN
    SELECT fi
    GOTO TOP
    IF (USED("fi2"))
       SELECT fi2
       GOTO TOP
    ENDIF
 ENDIF
ENDPROC
**
FUNCTION uf_atendimento_getcolor
 RETURN IIF(LEFT(ALLTRIM(fi.design), 1)=='.', RGB(0,  167, 231), IIF(fi.tipoerro>0, RGB(255, 0, 0), IIF(fi.qtt<=fi.qtdem .OR. ALLTRIM(fi.ref)=='R000001' .OR. (.NOT. EMPTY(fi.ofistamp) .and. fi.partes=0), RGB(255, 255, 255), RGB(243, 181, 24))))
ENDFUNC
**
PROCEDURE uf_atendimento_configgrid
 FOR i = 1 TO atendimento.grdatend.columncount
    atendimento.grdatend.columns(i).dynamicbackcolor = "iif(Left(fi.design,1)=='.',RGB(0,  167, 231),Rgb(255,255,255))"
    IF UPPER(ALLTRIM(atendimento.grdatend.columns(i).controlsource))=="FI.DESIGN"
       atendimento.grdatend.columns(i).dynamicforecolor = "iif(Left(fi.design,1)=='.',Rgb(255,255,255),RGB(0,0,0))"
    ELSE
       atendimento.grdatend.columns(i).dynamicforecolor = "iif(Left(fi.design,1)=='.',RGB(0,  167, 231),Rgb(0,0,0))"
    ENDIF
    IF UPPER(ALLTRIM(atendimento.grdatend.columns(i).controlsource))=="FI.TIPOR"
       atendimento.grdatend.columns(i).dynamicforecolor = "iif(Left(fi.design,1)=='.',Rgb(255,255,255),RGB(0,0,0))"
       atendimento.grdatend.columns(i).dynamicbackcolor = "uf_atendimento_getColor()"
    ENDIF
    atendimento.grdatend.columns(i).dynamiccurrentcontrol = "iif(Left(fi.design,1)=='.','Text2','Text1')"
    DO CASE
       CASE UPPER(ALLTRIM(atendimento.grdatend.columns(i).controlsource))=="FI.DESIGN"
          atendimento.grdatend.columns(i).dynamicforecolor = "iif(Left(fi.design,1)=='.',Rgb[255,255,255],IIF(fi.partes!=0,Rgb[255,165,0],Iif(!empty(fi.ofistamp),Rgb[139,0,139],IIF(!EMPTY(fi.bistamp),RGB(92,80,67),Rgb[0,0,0]))))"
       CASE UPPER(ALLTRIM(atendimento.grdatend.columns(i).controlsource))=="FI.QTT"
          IF  .NOT. myusarobot .AND.  .NOT. myusafarmax .AND.  .NOT. myusarobotapostore .AND.  .NOT. myusaservicosrobot
             atendimento.grdatend.columns(i).dynamicforecolor = "IIF(UPPER(ALLTRIM(fi.u_robot))=='R',RGB(30,144,255))"
          ELSE
             atendimento.grdatend.columns(i).dynamicbackcolor = "iif(Left(fi.design,1)=='.',RGB(0,  167, 231),IIF(UPPER(ALLTRIM(fi.u_robot))=='R',rgb[30,144,255],IIF(fi.num1>=fi.qtt and fi.num1>0,rgb[50,205,50],IIF(fi.qtt>fi.num1 and fi.num1>0,rgb[255,165,0],Rgb[255,255,255]))))"
          ENDIF
       CASE UPPER(ALLTRIM(atendimento.grdatend.columns(i).controlsource))=="FI.U_EPVP"
          atendimento.grdatend.columns(i).dynamicforecolor = "iif(Left[fi.design,1]=='.',RGB(0,  167, 231),Rgb[0,0,0])"
          atendimento.grdatend.columns(i).dynamicbackcolor = "iif(Left[fi.design,1]=='.',RGB(0,  167, 231),Iif(ALLTRIM(fi.cor)=='2',RGB[241,196,15],Iif(ALLTRIM(fi.cor)=='1',Rgb[255,0,0],RGB[255,255,255])))"
       CASE UPPER(ALLTRIM(atendimento.grdatend.columns(i).controlsource))=="FI.U_TXCOMP"
          atendimento.grdatend.columns(i).dynamicforecolor = "IIF(Left[fi.design,1]=='.',RGB(0,  167, 231),IIF(fi.u_comp = .T. and fi.U_TXCOMP < 100,Rgb[50,205,50],Rgb[0,0,0]))"
       CASE UPPER(ALLTRIM(atendimento.grdatend.columns(i).controlsource))=="FI.DESCONTO"
          atendimento.grdatend.columns(i).dynamicforecolor = "iif(Left[fi.design,1]=='.',RGB(0,  167, 231),Rgb[0,0,0])"
       CASE UPPER(ALLTRIM(atendimento.grdatend.columns(i).controlsource))=="FI.U_DESCVAL"
          atendimento.grdatend.columns(i).dynamicforecolor = "iif(Left[fi.design,1]=='.',RGB(0,  167, 231),Rgb[0,0,0])"
       CASE UPPER(ALLTRIM(atendimento.grdatend.columns(i).controlsource))=="FI.ETILIQUIDO"
          atendimento.grdatend.columns(i).dynamicforecolor = "iif(Left[fi.design,1]=='.',RGB(0,  167, 231),Iif(!(ALLTRIM(fi.u_diploma)==''),Rgb[50,205,50],Rgb[0,0,0]))"
    ENDCASE
 ENDFOR
 atendimento.grdatend.refresh()
ENDPROC
**
FUNCTION uf_atendimento_pvpbackcolor
 LOCAL lcauxcolor, lcauxcolorlordem
 STORE '' TO lcauxcolor, lcauxcolorlordem
 SELECT fi
 lcauxcolorlordem = (LEFT(astr(fi.lordem), 2)+REPLICATE('0', LEN(astr(fi.lordem))-2))
 SELECT token, design FROM fi WHERE astr(fi.lordem)=lcauxcolorlordem INTO CURSOR uCrsAuxColor READWRITE
 lcauxcolor = ALLTRIM(ucrsauxcolor.token)
 IF USED("uCrsAuxColor")
    fecha("uCrsAuxColor")
 ENDIF
 DO CASE
    CASE LEFT(ALLTRIM(fi.design), 1)=='.'
       SELECT fi
       RETURN RGB(0,  167, 231)
    CASE EMPTY(fi.id_validacaodem) .AND.  .NOT. EMPTY(lcauxcolor)
       SELECT fi
       RETURN RGB(255, 0, 0)
    CASE fi.qtt<=fi.qtdem .OR. ALLTRIM(fi.ref)=='R000001' .OR.  .NOT. EMPTY(fi.ofistamp)
       SELECT fi
       RETURN RGB(255, 255, 255)
    OTHERWISE
       SELECT fi
       RETURN RGB(243, 181, 24)
 ENDCASE
ENDFUNC
**
FUNCTION uf_atendimento_alteradescpercalllines
 IF uf_atendimento_validacampanhas()==.F.
    RETURN .F.
 ENDIF
 atendimento.tmroperador.reset()
 IF  .NOT. (uf_gerais_validapermuser(ch_userno, ch_grupo, 'Atendimento - Permite aplicar descontos'))
    uf_perguntalt_chama("O seu perfil n�o permite aplicar descontos", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF uf_gerais_getparameter('ADM0000000069', 'BOOL')
    PUBLIC cval
    STORE '' TO cval
    uf_tecladoalpha_chama("cval", "INTRODUZA PASSWORD PARA PERMITIR APLICAR DESCONTO:", .T., .F., 0)
    IF EMPTY(cval)
       uf_perguntalt_chama("DEVE INTRODUZIR A PASSWORD.", "OK", "", 64)
       RELEASE cval
       RETURN .F.
    ELSE
       IF  .NOT. (UPPER(ALLTRIM(cval))==UPPER(ALLTRIM(uf_gerais_getparameter('ADM0000000069', 'TEXT'))))
          uf_perguntalt_chama("A PASSWORD N�O EST� CORRECTA.", "OK", "", 64)
          RELEASE cval
          RETURN .F.
       ELSE
          RELEASE cval
       ENDIF
    ENDIF
 ENDIF
 PUBLIC lcdescontoatendlinhas
 STORE 0 TO lcdescontoatendlinhas
 IF ucrse1.descpredef==.T.
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			select design as TipoCliente , valor as Desconto FROM empresa_descclientes where empresa_no = <<mysite_nr>>
    ENDTEXT
    IF  .NOT. (uf_gerais_actgrelha("", "uCrsDescPreDef", lcsql))
       uf_perguntalt_chama("Ocorreu uma anomalia a verificar os descontos dispon�veis.", "OK", "", 16)
       RETURN .F.
    ELSE
       uf_valorescombo_chama("lcDescontoAtendLinhas", 0, "uCrsDescPreDef", 2, "Desconto", "TipoCliente, Desconto", .F., .F., .T.)
    ENDIF
 ELSE
    uf_tecladonumerico_chama("lcDescontoAtendLinhas", "Introduza o Desconto (%):", 0, .T., .F., 6, 2)
 ENDIF
 IF EMPTY(lcdescontoatendlinhas)
    lcdescontoatendlinhas = 0
 ENDIF
 IF uf_gerais_getparameter_site('ADM0000000060', 'BOOL', mysite) .AND. TYPE("ATCAMPANHAS")=="U"
    IF ALLTRIM(ch_grupo)<>'Administrador' .AND. ALLTRIM(ch_grupo)<>'Supervisores'
       IF  .NOT. uf_gerais_valida_password_admin('ATENDIMENTO', 'ALTERA��O DESCONTO')
          RETURN .F.
       ENDIF
    ENDIF
 ENDIF
 autorizadesc = .T.
 LOCAL lcposfidescp
 SELECT fi
 lcposfidescp = RECNO("fi")
 SELECT fi
 GOTO TOP
 SCAN
    IF  .NOT. (LEFT(fi.design, 1)==".")
       uf_atendimento_alteradesc(.T., lcdescontoatendlinhas)
    ENDIF
 ENDSCAN
 SELECT fi
 TRY
    GOTO lcposfidescp
 CATCH
    GOTO TOP
 ENDTRY
 autorizadesc = .F.
ENDFUNC
**
FUNCTION uf_atendimento_alteradescvaloralllines
 IF uf_atendimento_validacampanhas()==.F.
    RETURN .F.
 ENDIF
 atendimento.tmroperador.reset()
 IF  .NOT. (uf_gerais_validapermuser(ch_userno, ch_grupo, 'Atendimento - Permite aplicar descontos'))
    uf_perguntalt_chama("O seu perfil n�o permite aplicar descontos", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF uf_gerais_getparameter('ADM0000000324', 'BOOL')
    PUBLIC cval
    STORE '' TO cval
    uf_tecladoalpha_chama("cval", "INTRODUZA PASSWORD PARA PERMITIR APLICAR DESCONTO:", .T., .F., 0)
    IF EMPTY(cval)
       uf_perguntalt_chama("DEVE INTRODUZIR A PASSWORD.", "OK", "", 64)
       RELEASE cval
       RETURN .F.
    ELSE
       IF  .NOT. (UPPER(ALLTRIM(cval))==UPPER(ALLTRIM(uf_gerais_getparameter('ADM0000000324', 'TEXT'))))
          uf_perguntalt_chama("A PASSWORD N�O EST� CORRECTA.", "OK", "", 64)
          RELEASE cval
          RETURN .F.
       ELSE
          RELEASE cval
       ENDIF
    ENDIF
 ENDIF
 PUBLIC lcdescontoatendlinhas
 STORE 0 TO lcdescontoatendlinhas
 uf_tecladonumerico_chama("lcDescontoAtendLinhas", "Introduza o Desconto (�):", 0, .T., .F., 6, 2)
 LOCAL lcposfidescv
 SELECT fi
 lcposfidescv = RECNO("fi")
 SELECT fi
 GOTO TOP
 SCAN
    IF  .NOT. (LEFT(fi.design, 1)==".") &&AND fi.etiliquido > fi.etiliquido - lcdescontoatendlinhas
       uf_atendimento_alteradescval(.T., lcdescontoatendlinhas)
    ENDIF
 ENDSCAN
 SELECT fi
 TRY
    GOTO lcposfidescv
 CATCH
    GOTO TOP
 ENDTRY
ENDFUNC
**
FUNCTION uf_atendimento_alteradesc
 LPARAMETERS lcvalmsg, lcdescperc, lcDataFimPromo, lcDataIniPromo
	LOCAL uv_DtFimPromo, uv_DtIniPromo

	IF INLIST(TYPE("lcDataFimPromo"), "C", "N","U")
		uv_DtFimPromo  	= CTOD(lcDataFimPromo)
		uv_DtIniPromo  	= CTOD(lcDataIniPromo)
	ELSE
		uv_DtFimPromo  	= lcDataFimPromo
		uv_DtIniPromo  	= lcDataIniPromo
	ENDIF
	
 IF  !EMPTY(uv_DtFimPromo) AND YEAR(uv_DtFimPromo) <> 1900
	 IF  !BETWEEN(DATE(), uv_DtIniPromo  , uv_DtFimPromo  ) 
	 	RETURN .F.
	 ENDIF
 ENDIF
 
 
 IF uf_atendimento_validacampanhas()==.F.
    RETURN .F.
 ENDIF
 PUBLIC mydescpredef
 atendimento.tmroperador.reset()
 IF  .NOT. EMPTY(fi.tpres)
    uf_perguntalt_chama("N�o � poss�vel alterar descontos nas linhas a reservar.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF  .NOT. (uf_gerais_validapermuser(ch_userno, ch_grupo, 'Atendimento - Permite aplicar descontos'))
    IF  .NOT. lcvalmsg
       uf_perguntalt_chama("O seu perfil n�o permite aplicar descontos", "OK", "", 48)
    ENDIF
    RETURN .F.
 ENDIF
 SELECT fi
 IF ALLTRIM(fi.ref)=="R000001"
    uf_perguntalt_chama("N�o pode alterar o Desconto do produto ap�s criar a reserva.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF  .NOT. lcvalmsg
    IF uf_gerais_getparameter('ADM0000000069', 'BOOL')
       PUBLIC cval
       STORE '' TO cval
       uf_tecladoalpha_chama("cval", "INTRODUZA PASSWORD PARA PERMITIR APLICAR DESCONTO:", .T., .F., 0)
       IF EMPTY(cval)
          uf_perguntalt_chama("DEVE INTRODUZIR A PASSWORD.", "OK", "", 64)
          RELEASE cval
          RETURN .F.
       ELSE
          IF  .NOT. (UPPER(ALLTRIM(cval))==UPPER(ALLTRIM(uf_gerais_getparameter('ADM0000000069', 'TEXT'))))
             uf_perguntalt_chama("A PASSWORD N�O EST� CORRECTA.", "OK", "", 64)
             RELEASE cval
             RETURN .F.
          ELSE
             RELEASE cval
          ENDIF
       ENDIF
    ENDIF
 ENDIF
 IF uf_gerais_getparameter_site('ADM0000000060', 'BOOL', mysite) .AND. autorizadesc=.F. .AND. TYPE("ATCAMPANHAS")=="U"
    IF ALLTRIM(ch_grupo)<>'Administrador' .AND. ALLTRIM(ch_grupo)<>'Supervisores'
       IF  .NOT. uf_gerais_valida_password_admin('ATENDIMENTO', 'ALTERA��O DESCONTO')
          RETURN .F.
       ENDIF
    ENDIF
 ENDIF
 LOCAL lcvalidadesc, lcdescpos
 LOCAL lcposicfi
 PUBLIC lcdesc1, lcdesc2
 STORE 0 TO ti, lcdesc1, lcdesc2, lcvalidadesc, lcdescpos, lcposicfi
 SELECT fi
 IF LEFT(fi.design, 1)=="."
    RETURN .F.
 ENDIF
 SELECT fi
 lcdesc1 = fi.desconto
 IF  .NOT. lcvalmsg
    SELECT fi
    IF ucrse1.descpredef==.T.
       lcsql = ''
       TEXT TO lcsql TEXTMERGE NOSHOW
				select design as TipoCliente , valor as Desconto FROM empresa_descclientes where empresa_no = <<mysite_nr>>
       ENDTEXT
       IF  .NOT. (uf_gerais_actgrelha("", "uCrsDescPreDef", lcsql))
          uf_perguntalt_chama("Ocorreu uma anomalia a verificar os descontos dispon�veis.", "OK", "", 16)
          RETURN .F.
       ELSE
          uf_valorescombo_chama("myDescPredef", 0, "uCrsDescPreDef", 2, "Desconto", "TipoCliente, Desconto", .F., .F., .T.)
          IF EMPTY(mydescpredef)
             SELECT fi
             REPLACE fi.desconto WITH 0
          ELSE
             SELECT fi
             REPLACE fi.desconto WITH mydescpredef
          ENDIF
       ENDIF
    ELSE
       uf_tecladonumerico_chama("fi.desconto", "Introduza o Desconto:", 2, .T., .F., 6, 2)
    ENDIF
 ELSE
    SELECT fi
    REPLACE fi.desconto WITH lcdescperc
 ENDIF
 
 IF uf_gerais_getparameter_site('ADM0000000077', 'NUM', mysite)>0 .AND. TYPE("ATCAMPANHAS")=="U"
   IF fi.desconto>uf_gerais_getparameter_site('ADM0000000077', 'NUM', mysite)
      uf_perguntalt_chama("EST� A FAZER UM DESCONTO MAIOR QUE O PERMITIDO. A OPERA��O VAI SER CANCELADA.", "OK", "", 48)
      SELECT fi
      REPLACE fi.desconto WITH 0
   ENDIF
 ENDIF

 SELECT fi
 lcdesc2 = fi.desconto
 IF lcdesc1<>lcdesc2
    IF fi.partes==0
       IF lcdesc2<0
          IF  .NOT. lcvalmsg
             uf_perguntalt_chama("N�O PODE ALTERAR O DESCONTO PARA NEGATIVO.", "OK", "", 48)
          ENDIF
          SELECT fi
          REPLACE fi.desconto WITH lcdesc1
          RETURN .F.
       ENDIF
       IF lcdesc2>100
          IF  .NOT. lcvalmsg
             uf_perguntalt_chama("O DESCONTO N�O PODE SER SUPERIOR A 100%.", "OK", "", 48)
          ENDIF
          SELECT fi
          REPLACE fi.desconto WITH lcdesc1
       ENDIF
       IF USED("uCrsVale")
          SELECT fi
          lcposicfi = RECNO("FI")
          SELECT ucrsvale
          SCAN FOR ALLTRIM(ucrsvale.fistamp)==ALLTRIM(fi.ofistamp)
             IF lcdesc2<ucrsvale.desconto
                IF  .NOT. lcvalmsg
                   uf_perguntalt_chama("O DESCONTO N�O PODE SER INFERIOR AO DESCONTO ORIGINAL DO PRODUTO A REGULARIZAR.", "OK", "", 48)
                ENDIF
                SELECT fi
                REPLACE fi.desconto WITH lcdesc1
                RETURN .F.
             ENDIF
          ENDSCAN
          SELECT ucrsvale
          GOTO TOP
          SELECT fi
          TRY
             GOTO lcposicfi
          CATCH
          ENDTRY
       ENDIF
    ELSE
       IF  .NOT. lcvalmsg
          uf_perguntalt_chama("N�O PODE ALTERAR A PERCENTAGEM NUM PRODUTO MARCADO PARA DEVOLU��O.", "OK", "", 48)
       ENDIF
       SELECT fi
       REPLACE fi.desconto WITH lcdesc1
       RETURN .F.
    ENDIF
    lcposicfi = RECNO("FI")
    uf_atendimento_fiiliq(.F.)
    SELECT fi
    TRY
       GOTO lcposicfi
    CATCH
    ENDTRY
    SELECT fi
    IF fi.etiliquido<0
       IF  .NOT. lcvalmsg
          uf_perguntalt_chama("O TOTAL DA LINHA N�O PODE SER NEGATIVO.", "OK", "", 48)
       ENDIF
       SELECT fi
       REPLACE fi.desconto WITH lcdesc1
       lcposicfi = RECNO("FI")
       uf_atendimento_fiiliq(.F.)
       SELECT fi
       TRY
          GOTO lcposicfi
       CATCH
       ENDTRY
       RETURN .F.
    ENDIF
    lcposicfi = RECNO("FI")
    uf_atendimento_eventofi(.F.)
    SELECT fi
    TRY
       GOTO lcposicfi
    CATCH
    ENDTRY
 ENDIF
 lcposicfi = RECNO("FI")
 uf_atendimento_infototais()
 SELECT fi
 TRY
    GOTO lcposicfi
 CATCH
 ENDTRY
 RELEASE mydescpredef
ENDFUNC

FUNCTION uf_atendimento_selcliente_validaSeTemValorEmCartaoLinhas

	 SELECT fi
     GOTO TOP
       SCAN FOR ALLTRIM(fi.ref)==ALLTRIM("V000001")
       	  RETURN .t. 
     ENDSCAN  
       
     RETURN .f.  
       
ENDFUNC



&&apaga dados do cliente na pesquisa
&&linhas de valor em cartao
FUNCTION uf_atendimento_selcliente_cleanDadosEspecificosCliente

	
	&&valor em cartao
	if(USED("fi"))
		LOCAL lcposicfiTemp 
		lcposicfiTemp = RECNO("FI")
	   &&valida se deve apagar
	   	if(!EMPTY(uf_atendimento_selcliente_validaSeTemValorEmCartaoLinhas()))
	   		
	   	  fecha("ucrsFiTempAux")
	   	
	   	  SELECT fi
		  GOTO TOP
		   
		  SELECT * FROM fi INTO CURSOR  ucrsFiTempAux READWRITE
		  
		  SELECT ucrsFiTempAux 
		  GO TOP
			  SCAN FOR ALLTRIM(ucrsFiTempAux.ref)==ALLTRIM("V000001")
			  SELECT FI 
			  LOCATE FOR ALLTRIM(FI.fistamp)==ALLTRIM(ucrsFiTempAux.fistamp) AND ALLTRIM(fi.ref)==ALLTRIM("V000001")
       		  IF  FOUND()
       		  	uf_atendimento_apagalinha(.T.)
          	  ENDIF
          ENDSCAN		
 
		  fecha("ucrsFiTempAux")
	
		   
	   	ENDIF
	   
	   	SELECT fi
	   	TRY
		  GOTO lcposicfiTemp 
	  	CATCH
	  	  GOTO TOP 
	  	ENDTRY
	      			  
	ENDIF

ENDFUNC




**
FUNCTION uf_atendimento_alteradescval
LPARAMETERS lcvalmsg, lcdescval, lcDataFimPromo, lcDataIniPromo
	LOCAL uv_DtFimPromo, uv_DtIniPromo	
	IF INLIST(TYPE("lcDataFimPromo"), "C", "N","U")
		uv_DtFimPromo  	= CTOD(lcDataFimPromo)
		uv_DtIniPromo  	= CTOD(lcDataIniPromo)
	ELSE
		uv_DtFimPromo 	= lcDataFimPromo
		uv_DtIniPromo  	= lcDataIniPromo
	ENDIF

	IF  !EMPTY(uv_DtFimPromo) AND YEAR(uv_DtFimPromo) <> 1900
		IF  !BETWEEN(DATE(), uv_DtIniPromo, uv_DtFimPromo) 
			RETURN .F.
		ENDIF	
	ENDIF

	IF uf_atendimento_validacampanhas()==.F.
		RETURN .F.
	ENDIF
	atendimento.tmroperador.reset()
	IF  .NOT. EMPTY(fi.tpres)
		uf_perguntalt_chama("N�o � poss�vel alterar o desconto nas linhas a reservar. ", "OK", "", 48)
		RETURN .F.
	ENDIF
	IF  .NOT. (uf_gerais_validapermuser(ch_userno, ch_grupo, 'Atendimento - Permite aplicar descontos'))
		IF  .NOT. lcvalmsg
			uf_perguntalt_chama("O seu perfil n�o permite aplicar descontos", "OK", "", 48)
		ENDIF
		RETURN .F.
	ENDIF
	IF  .NOT. lcvalmsg
		IF uf_gerais_getparameter('ADM0000000324', 'BOOL')
			PUBLIC cval
			STORE '' TO cval
			uf_tecladoalpha_chama("cval", "INTRODUZA PASSWORD PARA PERMITIR APLICAR DESCONTO:", .T., .F., 0)
			IF EMPTY(cval)
				uf_perguntalt_chama("DEVE INTRODUZIR A PASSWORD.", "OK", "", 64)
				RELEASE cval
				RETURN .F.
			ELSE
				IF  .NOT. (UPPER(ALLTRIM(cval))==UPPER(ALLTRIM(uf_gerais_getparameter('ADM0000000324', 'TEXT'))))
					uf_perguntalt_chama("A PASSWORD N�O EST� CORRECTA.", "OK", "", 64)
					RELEASE cval
					RETURN .F.
				ELSE
					RELEASE cval
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	SELECT fi
	IF LEFT(fi.design, 1)=="."
		RETURN .F.
	ENDIF
	IF myvaledescval
		IF  .NOT. lcvalmsg
			uf_perguntalt_chama("N�O PODE ALTERAR O DESCONTO EM VALOR QUANDO J� EXISTEM VALES DE DESCONTO APLICADOS.", "OK", "", 48)
		ENDIF
		RETURN .F.
	ENDIF
	IF  .NOT. uf_gerais_getparameter('ADM0000000106', 'BOOL')
		RETURN .F.
	ENDIF
	LOCAL ti, lcdesc1, lcdesc2, lcvalidadesc, lcdescpos, lcposicfi
	STORE 0 TO ti, lcdesc1, lcdesc2, lcvalidadesc, lcdescpos, lcposicfi
	SELECT fi	
	lcdesc1 = fi.u_descval 
	
	IF  .NOT. lcvalmsg
		uf_tecladonumerico_chama("fi.u_descval", "Introduza o Desconto:", 2, .T., .F., 9, 2, .F. , .F. , .T.)
	ELSE
		SELECT fi
		REPLACE fi.u_descval WITH lcdescval
	ENDIF
	SELECT fi
	lcdesc2 = fi.u_descval 
	IF lcdesc1<>lcdesc2

		IF fi.partes=0
			IF lcdesc2<0
				IF  .NOT. lcvalmsg
					uf_perguntalt_chama("N�O PODE ALTERAR O DESCONTO PARA NEGATIVO.", "OK", "", 48)
				ENDIF
				SELECT fi
				REPLACE fi.u_descval WITH lcdesc1
			
				RETURN .F.
			ENDIF
	
			LOCAL lcValorAntesDescVal, lcValorDanteBit
			STORE 0 TO lcValorAntesDescVal
			STORE .F. TO lcValorDanteBit
			** este if existe quando existe um desconto aplicado e queremos mudar para um valor superior^
			** o software nao estava a validar essa situacao
			IF !EMPTY(fi.u_descval)
				IF lcdesc2>fi.epv * fi.qtt
					IF  .NOT. lcvalmsg
						uf_perguntalt_chama("O DESCONTO N�O PODE SER SUPERIOR AO VALOR DO PRODUTO.", "OK", "", 48)
					ENDIF
					SELECT fi
					REPLACE fi.u_descval WITH lcdesc1

					RETURN .F.
				ENDIF
				lcValorDanteBit = .T.
			ENDIF
		
			IF !lcValorDanteBit
				IF lcdesc2>fi.epv * fi.qtt
					IF  .NOT. lcvalmsg
						uf_perguntalt_chama("O DESCONTO N�O PODE SER SUPERIOR AO VALOR DO PRODUTO.", "OK", "", 48)
					ENDIF
					SELECT fi
					REPLACE fi.u_descval WITH lcdesc1
				RETURN .F.
				ENDIF
			ENDIF
		
			IF USED("uCrsVale")
				SELECT fi
				lcposicfi = RECNO("FI")
				SELECT ucrsvale
				SCAN FOR ALLTRIM(ucrsvale.fistamp)==ALLTRIM(fi.ofistamp)
					IF lcdesc2<ucrsvale.u_descval
						IF  .NOT. lcvalmsg
							uf_perguntalt_chama("O DESCONTO N�O PODE SER INFERIOR AO DESCONTO ORIGINAL DO PRODUTO A REGULARIZAR.", "OK", "", 48)
						ENDIF
						SELECT fi
						REPLACE fi.u_descval WITH lcdesc1
	
						RETURN .F.
					ENDIF
				ENDSCAN
				SELECT ucrsvale
				GOTO TOP
				SELECT fi
				TRY
					GOTO lcposicfi
				CATCH
				ENDTRY
			ENDIF
		ELSE
			IF  .NOT. lcvalmsg
				uf_perguntalt_chama("N�O PODE ALTERAR O DESCONTO EM VALOR NUM PRODUTO MARCADO PARA DEVOLU��O.", "OK", "", 48)
			ENDIF
			SELECT fi
			REPLACE fi.u_descval WITH lcdesc1
	
			RETURN .F.
		ENDIF
		lcposicfi = RECNO("FI")
		uf_atendimento_fiiliq(.F.)
		SELECT fi
		TRY
			GOTO lcposicfi
		CATCH
		ENDTRY
		lcposicfi = RECNO("FI")
		uf_atendimento_eventofi(.F.)
		SELECT fi
		TRY
			GOTO lcposicfi
		CATCH
		ENDTRY
		lcposicfi = RECNO("FI")
		uf_atendimento_infototais()
		SELECT fi
		TRY
			GOTO lcposicfi
		CATCH
		ENDTRY
	ENDIF
	
ENDFUNC
**
FUNCTION uf_atendimento_configoperador

IF(TYPE("myCxUser") != "C")
   RETURN .F.
ENDIF

 IF ucrse1.gestao_cx_operador=.F.
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			select TOP 1 
				ssstamp 
			from 
				ss (nolock)
			where 
				site = '<<alltrim(mySite)>>' 
				and pnome = '<<alltrim(myTerm)>>' 
				and auserno = <<ch_vendedor>>
				and cxstamp = '<<alltrim(myCxStamp)>>' 
				and fechada = 0
			order by 
				cxid desc 
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "uCrsSs", lcsql)
       uf_perguntalt_chama("OCORREU UMA ANOMALIA A VALIDAR O REGISTO DE SESS�O DE CAIXA. O SOFTWARE VAI SER ENCERRADO COMO MEDIDA DE PREVEN��O.", "OK", "", 16)
       QUIT
    ELSE
       IF  .NOT. RECCOUNT("uCrsSs")>0
          myssstamp = uf_gerais_stamp()
          lcsql = ''
          TEXT TO lcsql TEXTMERGE NOSHOW
					IF not exists (select ssstamp from ss where ssstamp = '<<alltrim(mySsStamp)>>')
					BEGIN 
						insert into ss
							(ssstamp
							,site
							,pnome
							,pno
							,cxstamp
							,cxusername
							,ausername
							,auserno
							,dabrir
							,habrir
							,fechada
							,ousrinis
							,ousrdata
							,ousrhora
							,usrinis
							,usrdata
							,usrhora )
						Values (
							'<<alltrim(mySsStamp)>>' 
							,'<<alltrim(mySite)>>'
							,'<<alltrim(myTerm)>>'
							,<<myTermNo>>
							,'<<alltrim(myCxStamp)>>'
							,'<<alltrim(myCxUser)>>'
							,'<<alltrim(ch_vendnm)>>'
							,<<ch_vendedor>>
							,CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
							,CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
							,0
							,'<<m_chinis>>'
							,CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
							,CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
							,'<<m_chinis>>'
							,CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
							,CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
						)
					END
          ENDTEXT
          IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
             uf_perguntalt_chama("OCORREU UM ERRO A CRIAR REGISTO DE SESS�O DE CAIXA. O SOFTWARE VAI SER ENCERRADO COMO MEDIDA DE PREVEN��O.", "OK", "", 16)
             QUIT
          ENDIF
       ELSE
          myssstamp = ucrsss.ssstamp
       ENDIF
    ENDIF
 ELSE

    IF uf_gerais_getParameter("ADM0000000338", "BOOL") AND ALLTRIM(UPPER(_screen.activeForm.name)) = "ATENDIMENTO"

        TEXT TO lcsql TEXTMERGE NOSHOW
        
                select * from ss(nolock)
                where
                    ss.site = '<<ALLTRIM(mysite)>>'
                    and ausername = '<<ALLTRIM(ch_vendnm)>>'
                    and CONVERT(VARCHAR,dabrir, 112) < CONVERT(VARCHAR,GETDATE() , 112)
                    and fechada = 0

        ENDTEXT

        IF !uf_gerais_actGrelha("", "uc_cxDiaAnterior", lcSQL)
            uf_perguntalt_chama("OCORREU UMA ANOMALIA AO CONFIGURAR A CAIXA DO UTILIZADOR", "OK", "", 16)
            RETURN .F.
        ENDIF

        IF RECCOUNT("uc_cxDiaAnterior") <> 0 
            uf_perguntalt_chama("Tem uma caixa aberta de dia anterior."+CHR(13)+"Para fechar e abrir uma nova caixa para o dia atual sem cancelar este atendimento: op��es > gest�o caixas > fechar cx > abrir cx" , "OK", "", 16)
            RETURN 2
        ENDIF

        FECHA("uc_cxDiaAnterior")
    ENDIF

    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
    		exec up_caixa_verificaCaixaAberta '<<ALLTRIM(mysite)>>', '<<ALLTRIM(ch_vendnm)>>', 1
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "uCrsValidaSsStamp", lcsql)
       uf_perguntalt_chama("OCORREU UMA ANOMALIA AO CONFIGURAR A CAIXA DO UTILIZADOR", "OK", "", 16)
       fecha("uCrsValidaSsStamp")
       RETURN .F.
    ELSE
       IF RECCOUNT("uCrsValidaSsStamp")>0
          myssstamp = ALLTRIM(ucrsvalidassstamp.ssstamp)
       ELSE

            IF ALLTRIM(UPPER(_screen.activeForm.name)) = "ATENDIMENTO"

               uf_perguntalt_chama("N�o existem caixas abertas para o utilizador seleccionado."+CHR(13)+"Para abrir caixa sem cancelar este atendimento: Op��es > Gest�o Caixas > Abrir Cx."+CHR(13)+" Obrigado", "OK", "", 16)
               fecha("uCrsValidaSsStamp")
               RETURN 2

            ENDIF
       ENDIF
       fecha("uCrsValidaSsStamp")
    ENDIF
 ENDIF
 IF USED("FT")
    SELECT ft
    REPLACE ft.vendnm WITH ch_vendnm
    REPLACE ft.vendedor WITH ch_vendedor
    REPLACE ft.ssusername WITH ch_vendnm
    REPLACE ft.ssstamp WITH myssstamp
 ENDIF
 IF  .NOT. (TYPE("ATENDIMENTO")=="U")
    atendimento.tmroperador.reset()
    atendimento.pedeautenticacao = .F.
 ENDIF
 IF USED("uCrsSs")
    fecha("uCrsSs")
 ENDIF
ENDFUNC
**
FUNCTION uf_atendimento_selcliente
 LPARAMETERS lcno, lcestab, lcecra, lcNaoValObito
 IF EMPTY(lcno) .OR.  .NOT. TYPE("lcNO")=="N"
    lcno = 200
 ENDIF
 IF EMPTY(lcestab) .OR.  .NOT. TYPE("lcEstab")=="N"
    lcestab = 0
 ENDIF
 IF EMPTY(lcecra) .OR.  .NOT. TYPE("lcEcra")=="C"
    uf_perguntalt_chama("O PAR�METRO: ECR�, N�O � V�LIDO", "OK", "", 16)
    RETURN .F.
 ELSE
    lcecra = UPPER(ALLTRIM(lcecra))
 ENDIF
 
 **se consultou beneficios via RNU
 LOCAL lcRecmId
 lcRecmId = ""
 if(USED("uc_rnuRecms"))
 	SELECT uc_rnuRecms
 	GO TOP
 	lcRecmId = ALLTRIM(uc_rnuRecms.codeAsList)
 ENDIF
 
 fecha("uc_rnuRecms")

 
 IF  .NOT. uf_gerais_actgrelha("", 'uCrsAtendCL', 'Exec up_touch_dadosCliente '+astr(lcno)+','+astr(lcestab)+'')
    uf_perguntalt_chama("Ocorreu uma anomalia a obter os dados do Cliente.", "OK", "", 16)
    REPLACE nome WITH '' IN ft
    RETURN .F.
 ELSE
    IF RECCOUNT("uCrsAtendCL")>0    	
       uf_atendimento_selcliente_cleanDadosEspecificosCliente()
       
       && se apagou linhas de desconto, volta a repor a valor inicial em cartao
       SELECT uCrsAtendCL
       REPLACE uCrsAtendCL.valcartao with uCrsAtendCL.valcartaoOld
       REPLACE uCrsAtendCL.recmId WITH lcRecmId
       
       SELECT ft
       REPLACE ft.nome WITH ALLTRIM(ucrsatendcl.nome)
       REPLACE ft.nome2 WITH ALLTRIM(ucrsatendcl.nome2)
       REPLACE ft.no WITH ucrsatendcl.no
       REPLACE ft.morada WITH IIF(USED("uc_rnu"), ALLTRIM(uc_rnu.cl_morada), ALLTRIM(ucrsatendcl.morada))
       REPLACE ft.local WITH ALLTRIM(ucrsatendcl.local)
       REPLACE ft.codpost WITH IIF(USED("uc_rnu"), ALLTRIM(uc_rnu.cl_codPost),ALLTRIM(ucrsatendcl.codpost))
       REPLACE ft.ncont WITH ALLTRIM(ucrsatendcl.ncont)
       REPLACE ft.telefone WITH ALLTRIM(ucrsatendcl.telefone)
       REPLACE ft.zona WITH ALLTRIM(ucrsatendcl.zona)
       REPLACE ft.estab WITH ucrsatendcl.estab
       REPLACE ft.tipo WITH ALLTRIM(ucrsatendcl.tipo)
       REPLACE ft.bino WITH ucrsatendcl.bino
       REPLACE ft.moeda WITH ucrsatendcl.moeda
       REPLACE ft.ccusto WITH ucrsatendcl.ccusto
       REPLACE ft.pais WITH ucrsatendcl.pais 
	   IF TYPE("ft.nascimento") <> "U"
	   		REPLACE ft.nascimento WITH ucrsatendcl.nascimento
	   ENDIF
       SELECT ft2
       REPLACE ft2.morada WITH IIF(USED("uc_rnu"), ALLTRIM(uc_rnu.cl_morada), ALLTRIM(ucrsatendcl.morada))
       REPLACE ft2.local WITH ALLTRIM(ucrsatendcl.local)
       REPLACE ft2.codpost WITH IIF(USED("uc_rnu"), ALLTRIM(uc_rnu.cl_codigoP),ALLTRIM(ucrsatendcl.codpost))
       REPLACE ft2.telefone WITH ALLTRIM(ucrsatendcl.telefone)
       REPLACE ft2.email WITH ALLTRIM(ucrsatendcl.email)
       REPLACE ft2.u_nbenef WITH ALLTRIM(ucrsatendcl.nbenef)
       REPLACE ft2.cativa WITH ucrsatendcl.cativa
       REPLACE ft2.perccativa WITH ucrsatendcl.perccativa
       REPLACE ft2.xpdLocalidade WITH alltrim(ucrsatendcl.local)
       REPLACE ft2.xpdCodpost WITH alltrim(ucrsatendcl.codpost)
       SELECT fi
       REPLACE ficcusto WITH ucrsatendcl.ccusto ALL IN fi
       GOTO TOP
       IF lcecra=="ATENDIMENTO"
          LOCAL lcidade
          lcidade = 0
          IF USED("uc_rnu") OR uf_gerais_getdate(ucrsatendcl.nascimento, "SQL")<>'19000101'
           	 IF USED("uc_rnu")
           		lcidade  =  STR(uf_gerais_getIdade(uc_rnu.cl_nascimento))
           		atendimento.pgfdados.page1.txtU_nbenef.value = uc_rnu.cl_nbenef
           	 ELSE
           		lcidade = STR(uf_gerais_getIdade(ucrsatendcl.nascimento))
           	 ENDIF
          ELSE
             lcidade = STR(lcidade)
          ENDIF
          IF VAL(lcidade)<=3 .AND. uf_gerais_getdate(ucrsatendcl.nascimento, "SQL")<>'19000101'
             SELECT nascimento, DATE(), IIF(YEAR(nascimento)=YEAR(DATE()), MONTH(DATE())-MONTH(nascimento)+1, 12-MONTH(nascimento)+1+(12*(YEAR(DATE())-YEAR(nascimento)-1))+MONTH(DATE())) AS meses FROM ucrsatendcl INTO CURSOR ucrstempidademeses READWRITE
             SELECT ucrstempidademeses
             atendimento.pgfdados.page1.txtidade.value = astr(ucrstempidademeses.meses)+" meses"
             fecha("ucrstempidademeses")
          ELSE
             atendimento.pgfdados.page1.txtidade.value = ALLTRIM(lcidade)+" anos"
          ENDIF
          LOCAL lcimc
          lcimc = 0
          IF ucrsatendcl.altura<>0
             lcimc = ucrsatendcl.peso/(ucrsatendcl.altura*ucrsatendcl.altura)
             atendimento.pgfdados.page1.txtimc.value = ROUND(lcimc, 2)
          ENDIF
          PUBLIC pobsatendcl, up_moradaUt, up_codPostUt, up_nbenef, up_nascimentoUT, up_sexoUT, up_nbenef2

          pobsatendcl = ucrsatendcl.obs
          up_moradaUt = ucrsatendcl.morada
          up_codPostUt = ucrsatendcl.codPost
          up_nbenef = ucrsatendcl.nbenef
          up_nbenef2 = ucrsatendcl.nbenef2
		  up_nascimentoUT = ucrsatendcl.nascimento
		  up_sexoUT = ucrsatendcl.sexo

          IF ucrsatendcl.no==200
             SELECT ucrsatendcl
             REPLACE ucrsatendcl.esaldo WITH 0
          ELSE
             TEXT TO lcsql TEXTMERGE NOSHOW
						select 
							total = ISNULL(sum(edeb - edebf) - sum(ecred - ecredf),0)
						from cc (nolock) 
						where 
							no = <<uCrsatendCl.no>> 
							and estab = <<uCrsatendCl.estab>>
             ENDTEXT
             IF uf_gerais_actgrelha("", "uCrsSaldoCCDepAt", lcsql)
                SELECT ucrsatendcl
                SELECT ucrssaldoccdepat
                REPLACE ucrsatendcl.esaldo WITH ucrssaldoccdepat.total
             ENDIF
             fecha("uCrsSaldoCCDepAt")
          ENDIF
          WITH atendimento.pgfdados.page1
             IF (ucrsatendcl.esaldo=0)
                .txtsaldo.disabledbackcolor = RGB(255, 255, 255)
                .txtsaldo.forecolor = RGB(0, 0, 0)
             ELSE
                IF ucrsatendcl.esaldo>ucrsatendcl.eplafond
                   .txtsaldo.disabledbackcolor = RGB(255, 0, 0)
                   .txtsaldo.forecolor = RGB(255, 255, 255)
                ELSE
                   .txtsaldo.disabledbackcolor = RGB(241, 196, 15)
                   .txtsaldo.forecolor = RGB(0, 0, 0)
                ENDIF
             ENDIF
             IF (DATE()-CTOD(uf_gerais_getdate(ucrsatendcl.nascimento)))<=3 .AND. (DATE()-CTOD(uf_gerais_getdate(ucrsatendcl.nascimento)))>=-3
                .txtnascimento.disabledbackcolor = RGB(255, 0, 0)
                .txtnascimento.forecolor = RGB(255, 255, 255)
             ELSE
                .txtnascimento.disabledbackcolor = RGB(255, 255, 255)
                .txtnascimento.forecolor = RGB(0, 0, 0)
             ENDIF
             SELECT ucrsatendcl
             IF ucrsatendcl.pontos>0
                .txtpontos.disabledbackcolor = RGB(39, 174, 97)
                .txtpontos.forecolor = RGB(255, 255, 255)
             ELSE
                .txtpontos.disabledbackcolor = RGB(255, 255, 255)
                .txtpontos.forecolor = RGB(0, 0, 0)
             ENDIF
             IF ucrsatendcl.valcartao>0
                .txtvalcartao.disabledbackcolor = RGB(39, 174, 97)
                .txtvalcartao.forecolor = RGB(255, 255, 255)
             ELSE
                .txtvalcartao.disabledbackcolor = RGB(255, 255, 255)
                .txtvalcartao.forecolor = RGB(0, 0, 0)
             ENDIF
             TEXT TO lcsql TEXTMERGE NOSHOW
						exec up_cartao_aplicarValesPendentes <<uCrsatendCl.no>>,<<uCrsatendCl.estab>>
             ENDTEXT
             IF  .NOT. uf_gerais_actgrelha("", "uCrsTemValPen", lcsql)
                uf_perguntalt_chama("OCORREU UM ERRO A PROCURAR OS VALES DO CLIENTE.", "OK", "", 16)
                RETURN .F.
             ENDIF
             LOCAL lcidadeval, lcvalcaducados
             STORE 0 TO lcvalcaducados
             lcidadeval = uf_gerais_getparameter("ADM0000000017", "NUM")
             IF uf_gerais_getparameter_site("ADM0000000017", "BOOL")
                SELECT ucrstemvalpen
                GOTO TOP
                SCAN
                   IF ucrstemvalpen.idade_meses>lcidadeval
                      lcvalcaducados = lcvalcaducados+ucrstemvalpen.valor
                   ENDIF
                   SELECT ucrstemvalpen
                ENDSCAN
             ENDIF
             fecha("uCrsTemValPen")
             IF lcvalcaducados>0
                SELECT ucrsatendcl
                REPLACE ucrsatendcl.valorvales WITH ucrsatendcl.valorvales-lcvalcaducados
             ENDIF
             IF ucrsatendcl.valorvales>0
                .txtvales.disabledbackcolor = RGB(39, 174, 97)
                .txtvales.forecolor = RGB(255, 255, 255)
             ELSE
                .txtvales.disabledbackcolor = RGB(255, 255, 255)
                .txtvales.forecolor = RGB(0, 0, 0)
             ENDIF
             IF  .NOT. EMPTY(uf_atendimento_existemcampanhasut())
                .btncampanhas.Shape1.backcolor = RGB(255, 94, 91)
                atendimento.pgfdados.page2.btncampanhas.Shape1.backcolor = RGB(255, 94, 91)
             ELSE
                .btncampanhas.Shape1.backcolor = RGB(0,  167, 231)
                atendimento.pgfdados.page2.btncampanhas.Shape1.backcolor = RGB(0,  167, 231)
             ENDIF
          ENDWITH
          uf_atendimento_controlapontosapresentacartao()
			
			** verifica se obriga � leitura o cart�o para obten��o de pontos
          IF uf_gerais_getParameter_site('ADM0000000132', 'BOOL', mySite) = .t.
	          	replace uCrsAtendCL.nrcartao WITH ''
	          	replace uCrsAtendCl.valcartao WITH 0
	          	 atendimento.pgfDados.Page1.txtValcartao.value=''
  				atendimento.pgfDados.Page1.txtclicartao.value=''	
          ENDIF 
          
          atendimento.pgfdados.page1.refresh()

       ENDIF
       IF  .NOT. (uf_gerais_actgrelha("", 'uCrsAtendCLPato', [exec up_clientes_patologias ']+ALLTRIM(ucrsatendcl.utstamp)+[']))
          uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR AS PATOLOGIAS DO CLIENTE!", "OK", "", 16)
          RETURN .F.
       ENDIF
       SELECT ucrsatendcl
       IF ucrsatendcl.no<>200 .AND. ucrsatendcl.desconto<>0
          DO CASE
             CASE lcecra=="ATENDIMENTO"
                uf_atendimento_aplicadescontocllinhas(.F.)
             CASE lcecra=="FACTURACAO"
                SELECT ucrsatendcl
                facturacao.descontocl = ucrsatendcl.desconto
                uf_atendimento_aplicadescontocllinhas(.T.)
          ENDCASE
	   ELSE
          DO CASE
             CASE lcecra=="ATENDIMENTO"
			    SELECT ucrsAtendCL
				REPLACE ucrsAtendCL.desconto WITH 0
				uf_atendimento_aplicadescontocllinhas(.F.)
             CASE lcecra=="FACTURACAO"
                SELECT ucrsatendcl
                facturacao.descontocl = 0
                uf_atendimento_aplicadescontocllinhas(.T.)
          ENDCASE
       ENDIF
       IF uf_gerais_getparameter("ADM0000000192", "BOOL")
          SELECT ucrsatendcl
          IF ucrsatendcl.no<>200
             IF  .NOT. EMPTY(ALLTRIM(ucrsatendcl.codpla)) .AND. (uf_gerais_getdate(ucrsatendcl.valipla, "SQL")=="19000101" .OR. ucrsatendcl.valipla>=DATE())
                SELECT fi
                IF RECCOUNT("fi")>1
                   uf_atendimento_aplicaplanoauto()
                ENDIF
             ENDIF
          ENDIF
       ENDIF
       SELECT fi
       GOTO TOP
       IF lcecra=="ATENDIMENTO"
          atendimento.grdatend.setfocus()
       ELSE
          uf_faturacao_calcidade()
       ENDIF
       LOCAL lcposfi
       lcposfi = RECNO("FI")
       SELECT fi
       GOTO TOP
       SCAN FOR LEFT(ALLTRIM(fi.design), 1)=="."
          SELECT fi
          REPLACE fi.u_nbenef WITH ucrsatendcl.nbenef
          SELECT fi
          REPLACE fi.u_nbenef2 WITH ucrsatendcl.nbenef2
       ENDSCAN
       SELECT fi
       TRY
          GOTO lcposfi
       CATCH
          GOTO TOP
       ENDTRY
    ENDIF
 ENDIF
 IF USED("uCrsFpato")
    fecha("uCrsFpato")
 ENDIF
 DO CASE
    CASE lcecra=="ATENDIMENTO"
       atendimento.refresh()
       IF atendimento.pgfdados.activepage<>1
          atendimento.pgfdados.activepage = 1
       ENDIF
       IF uf_gerais_getparameter_site('ADM0000000079', 'BOOL', mysite) .AND.  .NOT. EMPTY(ucrsatendcl.no_ext) AND ALLTRIM(ucrsatendcl.no_ext)<>'0'
          atendimento.timer1.interval = 1000
          atendimento.timer1.reset()
       ENDIF
       IF uf_gerais_getparameter_site('ADM0000000133', 'BOOL', mysite) .AND.  .NOT. EMPTY(uCrsAtendCL.nrcartao)
       	  atendimento.timer2.interval = 1000
          atendimento.timer2.reset()
       ENDIF 
    CASE lcecra=="FACTURACAO"
       facturacao.refresh()
 ENDCASE
 TEXT TO lcsql TEXTMERGE NOSHOW
		SELECT utstamp, autorizado FROM b_utentes(nolock) WHERE no=<<ft.no>> and estab=<<ft.estab>>
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "utstamp", lcsql)
    MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A VERIFICAR DADOS DOS SERVI�OS. POR FAVOR CONTACTE O SUPORTE.", 16, "LOGITOOLS SOFTWARE")
    RETURN .F.
 ENDIF
 IF LEFT(ALLTRIM(ft.ncont), 1)<>'5' .AND. LEFT(ALLTRIM(ft.ncont), 1)<>'9' .AND. LEFT(ALLTRIM(ft.ncont), 1)<>'6'
    IF utstamp.autorizado=.F.
       IF uf_gerais_getparameter_site("ADM0000000001", "BOOL", mysite)=.T.
          uf_enviotoken_chama(ALLTRIM(utstamp.utstamp), 'b_utentes')
       ENDIF
    ENDIF
 ENDIF
 
 
 uf_atendimento_validaNrSnsSNSExterno()
 IF TYPE("myChamaSinave")=="C"
  IF ALLTRIM(myChamaSinave) = 'Sem Comparticipa��o' OR ALLTRIM(myChamaSinave) = 'Com Comparticipa��o'
   		uf_comunicasinave_chama()    
   		myChamaSinave = 'N�o chama'
	ENDIF
 ELSE
	RELEASE myChamaSinave
	PUBLIC myChamaSinave
	STORE 'N�o chama' TO myChamaSinave
 ENDIF 
 
   IF !lcNaoValObito
      uf_atendimento_valida_obito()
   ENDIF
   
   
   uf_atendimento_forca_recalculo_compart()
 
ENDFUNC
**
FUNCTION uf_atendimento_navegafichaproduto
 IF atendimento.selprod==.F.
    atendimento.selprod = .T.
 ELSE
    IF uf_gerais_getparameter_site('ADM0000000068', 'BOOL', mysite)=.T. .AND. ALLTRIM(ch_grupo)<>'Administrador' .AND. ALLTRIM(ch_grupo)<>'Supervisores'
       IF  .NOT. uf_gerais_valida_password_admin('ATENDIMENTO', 'FICHA ARTIGO ACEDER')
          RETURN .F.
       ENDIF
    ENDIF
    uf_atendimento_fichaprod()
 ENDIF
 
ENDFUNC
**
PROCEDURE uf_atendimento_navegacabecalho
 IF atendimento.selcab==.F.
    atendimento.selcab = .T.
 ELSE
    IF UPPER(ALLTRIM(ucrse1.pais))=='PORTUGAL'
       uf_atendimento_dadosdemchama()
    ENDIF
 ENDIF
ENDPROC
**
PROCEDURE uf_atendimento_verificainfoutente
 LOCAL lccampos, lcactual, lcpergunta, lctexto
 STORE "" TO lccampos, lcactual
 STORE .F. TO lcpergunta
 lctexto = "Existem campos importantes na ficha do Utente que n�o est�o preenchidos:"
 lccampos = ALLTRIM(uf_gerais_getparameter("ADM0000000186", "TEXT"))
 ALINES(lalines, lccampos, ",")
 FOR i = 1 TO ALEN(lalines)
    lcactual = ALLTRIM(lalines(i))
    SELECT ucrsatendcl
    DO CASE
       CASE TYPE("uCrsAtendCl.&lcActual") == "T"
          IF uf_gerais_getdate(ucrsatendcl.&lcactual,"SQL") == "19000101"
             lcpergunta = .T.
             lctexto = lctexto+CHR(13)+"- "+ALLTRIM(lcactual)
          ENDIF
       CASE TYPE("uCrsAtendCl.&lcActual") == "C"
          IF ALLTRIM(ucrsatendcl.&lcactual) == ""
             lcpergunta = .T.
             lctexto = lctexto+CHR(13)+"- "+ALLTRIM(lcactual)
          ENDIF
    ENDCASE
 ENDFOR
 lctexto = lctexto+CHR(13)+"Pretende editar a ficha do Utente?"
 IF lcpergunta==.T. .AND. uf_perguntalt_chama(lctexto, "Sim", "N�o")
    uf_criarclatend_chama()
    uf_criarclatend_editarcl(ucrsatendcl.no, ucrsatendcl.estab)
 ENDIF
ENDPROC
**
PROCEDURE uf_atendimento_aplicaplanoauto
 LPARAMETERS lnproduto
 LOCAL lcposfi
 STORE 0 TO lcposfi
 
 
 SELECT fi
 GOTO TOP
 SCAN
    IF LEFT(fi.design, 1)=="."
       SELECT ucrsatendcomp
       LOCATE FOR ALLTRIM(ucrsatendcomp.fistamp)==ALLTRIM(fi.fistamp)
       IF  .NOT. FOUND()
          lcposfi = RECNO("fi")
          IF uf_atendimento_verificaprodcomp()
             mypedelocalpresc = .F.
             mypedecodpresc = .F.
             SELECT ucrsatendcl
             uf_atendimento_aplicacompart(ALLTRIM(ucrsatendcl.codpla), .T., .F., fi.fistamp)
             mypedelocalpresc = uf_gerais_getparameter('ADM0000000001', 'bool')
             mypedecodpresc = uf_gerais_getparameter('ADM0000000002', 'bool')
          ENDIF
          TRY
             GOTO lcposfi
          CATCH
          ENDTRY
       ENDIF
    ENDIF
 ENDSCAN
 SELECT fi
 GOTO TOP
ENDPROC
**
FUNCTION uf_atendimento_verificaprodcomp
 LOCAL lcpos, lcvalprodcomp, lcvalprimeiro
 STORE 0 TO lcpos
 STORE .F. TO lcvalprodcomp, lcvalprimeiro
 SELECT fi
 TRY
    SKIP 1
 CATCH
 ENDTRY
 lcpos = RECNO("fi")
 SCAN
    IF  .NOT. lcvalprimeiro
       TRY
          GOTO lcpos
       CATCH
       ENDTRY
       lcvalprimeiro = .T.
    ENDIF
    IF LEFT(fi.design, 1)=="."
       EXIT
    ENDIF
    SELECT ucrsatendst
    LOCATE FOR ALLTRIM(ucrsatendst.ststamp)==ALLTRIM(fi.fistamp)
    IF ucrsatendst.comp==1
       lcvalprodcomp = .T.
    ENDIF
 ENDSCAN
 SELECT fi
 TRY
    GOTO lcpos
 CATCH
 ENDTRY
 TRY
    SKIP -1
 CATCH
 ENDTRY
 RETURN lcvalprodcomp
ENDFUNC
**
FUNCTION uf_atendimento_aplicadescontocllinhas
 LPARAMETERS lcbool
 IF mypromocoes=.F.
    RETURN .F.
 ENDIF
 IF uf_gerais_getparameter('ADM0000000129', 'bool')
    LOCAL lcpos, lcvalupdate, lcdescontoorigem, lcposicfi
    STORE .T. TO lcvalupdate
    STORE 0 TO lcpos, lcdescontoorigem
    SELECT fi
    lcpos = RECNO("fi")
    SELECT fi
    GOTO TOP
    SCAN
       lcvalupdate = .T.
       IF LEFT(fi.design, 1)<>"." .AND. ALLTRIM(fi.ref)<>'R000001' AND EMPTY(fi.bistamp) AND EMPTY(fi.ofistamp)
          IF EMPTY(ALLTRIM(fi.ofistamp)) .AND.  .NOT. (fi.epromo)
             IF USED("uCrsVale")
                SELECT ucrsvale
                SCAN FOR (ucrsvale.fistamp==fi.ofistamp)
                   IF ucrsatendcl.desconto<ucrsvale.desconto
                      lcvalupdate = .F.
                   ENDIF
                ENDSCAN
                GOTO TOP
                SELECT fi
             ENDIF
             IF fi.partes==1
                lcvalupdate = .F.
             ENDIF
             IF lcvalupdate
                lcdescontoorigem = fi.desconto
                REPLACE fi.desconto WITH ucrsatendcl.desconto
                lcposicfi = RECNO("FI")
                uf_atendimento_fiiliq(.F.)
                SELECT fi
                TRY
                   GOTO lcposicfi
                CATCH
                ENDTRY
                SELECT fi
                IF fi.etiliquido<0
                   SELECT fi
                   REPLACE fi.desconto WITH lcdescontoorigem
                   lcposicfi = RECNO("FI")
                   uf_atendimento_fiiliq(.F.)
                   SELECT fi
                   TRY
                      GOTO lcposicfi
                   CATCH
                   ENDTRY
                ENDIF
                lcposicfi = RECNO("FI")
                uf_atendimento_eventofi(.F.)
                TRY
                   GOTO lcposicfi
                CATCH
                ENDTRY
             ENDIF
          ENDIF
       ENDIF
    ENDSCAN
    uf_atendimento_infototais()
    SELECT fi
    TRY
       GOTO lcpos
    CATCH
       GOTO TOP
    ENDTRY
 ENDIF
 IF  .NOT. lcbool
    atendimento.grdatend.setfocus
 ELSE
    facturacao.pageframe1.page1.griddoc.setfocus
 ENDIF
ENDFUNC
**
FUNCTION uf_atendimento_aplicadescontoperclinhas
 atendimento.tmroperador.reset()
 IF  .NOT. (uf_gerais_validapermuser(ch_userno, ch_grupo, 'Atendimento - Permite aplicar descontos'))
    uf_perguntalt_chama("O seu perfil n�o permite aplicar descontos", "OK", "", 48)
    RETURN .F.
 ENDIF
 SELECT fi
 GOTO TOP
 SCAN
    IF ALLTRIM(fi.ref)=="R000001"
       uf_perguntalt_chama("N�o pode alterar o Desconto do produto ap�s criar a reserva.", "OK", "", 48)
       RETURN .F.
    ENDIF
 ENDSCAN
 IF uf_gerais_getparameter('ADM0000000069', 'BOOL')
    PUBLIC cval
    STORE '' TO cval
    uf_tecladoalpha_chama("cval", "INTRODUZA PASSWORD PARA PERMITIR APLICAR DESCONTO:", .T., .F., 0)
    IF EMPTY(cval)
       uf_perguntalt_chama("DEVE INTRODUZIR A PASSWORD.", "OK", "", 64)
       RELEASE cval
       RETURN .F.
    ELSE
       IF  .NOT. (UPPER(ALLTRIM(cval))==UPPER(ALLTRIM(uf_gerais_getparameter('ADM0000000069', 'TEXT'))))
          uf_perguntalt_chama("A PASSWORD N�O EST� CORRECTA.", "OK", "", 64)
          RELEASE cval
          RETURN .F.
       ELSE
          RELEASE cval
       ENDIF
    ENDIF
 ENDIF
 IF uf_gerais_getparameter_site('ADM0000000060', 'BOOL', mysite) .AND. TYPE("ATCAMPANHAS")=="U"
    IF ALLTRIM(ch_grupo)<>'Administrador' .AND. ALLTRIM(ch_grupo)<>'Supervisores'
       IF  .NOT. uf_gerais_valida_password_admin('ATENDIMENTO', 'ALTERAR DESCONTO')
          RETURN .F.
       ENDIF
    ENDIF
 ENDIF
 PUBLIC lcdescontoatendlinhas
 STORE 0 TO lcdescontoatendlinhas
 IF ucrse1.descpredef==.T.
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			select design as TipoCliente , valor as Desconto FROM empresa_descclientes where empresa_no = <<mysite_nr>>
    ENDTEXT
    IF  .NOT. (uf_gerais_actgrelha("", "uCrsDescPreDef", lcsql))
       uf_perguntalt_chama("Ocorreu uma anomalia a verificar os descontos dispon�veis.", "OK", "", 16)
       RETURN .F.
    ELSE
       uf_valorescombo_chama("lcDescontoAtendLinhas", 0, "uCrsDescPreDef", 2, "Desconto", "TipoCliente, Desconto", .F., .F., .T.)
    ENDIF
 ELSE
    uf_tecladonumerico_chama("lcDescontoAtendLinhas", "Introduza o Desconto (%):", 0, .T., .F., 6, 2)
 ENDIF
 IF EMPTY(lcdescontoatendlinhas)
    lcdescontoatendlinhas = 0
 ENDIF
 IF lcdescontoatendlinhas<0 .OR. lcdescontoatendlinhas>100
    RELEASE lcdescontoatendlinhas
    RETURN .F.
 ENDIF

 IF uf_gerais_getparameter_site('ADM0000000077', 'NUM', mysite)>0 .AND. TYPE("ATCAMPANHAS")=="U"
   IF lcdescontoatendlinhas>uf_gerais_getparameter_site('ADM0000000077', 'NUM', mysite)
      uf_perguntalt_chama("EST� A FAZER UM DESCONTO MAIOR QUE O PERMITIDO. A OPERA��O VAI SER CANCELADA.", "OK", "", 48)
      lcdescontoatendlinhas = 0
   ENDIF
 ENDIF

 LOCAL lcpos, lcval, lcvalupdate, lcdescontoorigem
 STORE .F. TO lcval
 STORE .T. TO lcvalupdate
 STORE 0 TO lcpos, lcdescontoorigem
 SELECT fi
 lcpos = RECNO("fi")
 IF lcpos==RECCOUNT("fi")
    RETURN .F.
 ENDIF
 SELECT fi
 SCAN
    lcvalupdate = .T.
    IF  .NOT. lcval
       TRY
          GOTO lcpos
       CATCH
       ENDTRY
       TRY
          SKIP
       CATCH
       ENDTRY
       lcval = .T.
    ENDIF
    IF LEFT(fi.design, 1)<>"."
       IF  .NOT. (fi.epromo)
          IF USED("uCrsVale")
             SELECT ucrsvale
             SCAN FOR (ucrsvale.fistamp==fi.ofistamp)
                IF lcdescontoatendlinhas<ucrsvale.desconto
                   lcvalupdate = .F.
                ENDIF
             ENDSCAN
             GOTO TOP
             SELECT fi
          ENDIF
          IF fi.partes==1
             lcvalupdate = .F.
          ENDIF
          IF lcvalupdate
             lcdescontoorigem = fi.desconto
             REPLACE fi.desconto WITH lcdescontoatendlinhas
             lcposicfi = RECNO("FI")
             uf_atendimento_fiiliq(.F.)
             SELECT fi
             TRY
                GOTO lcposicfi
             CATCH
             ENDTRY
             SELECT fi
             IF fi.etiliquido<0
                SELECT fi
                REPLACE fi.desconto WITH lcdescontoorigem
                lcposicfi = RECNO("FI")
                uf_atendimento_fiiliq(.F.)
                SELECT fi
                TRY
                   GOTO lcposicfi
                CATCH
                ENDTRY
             ENDIF
             lcposicfi = RECNO("FI")
             uf_atendimento_eventofi(.F.)
             SELECT fi
             TRY
                GOTO lcposicfi
             CATCH
             ENDTRY
          ENDIF
       ENDIF
    ELSE
       EXIT
    ENDIF
 ENDSCAN
 SELECT fi
 GOTO TOP
 uf_atendimento_infototais()
 SELECT fi
 TRY
    GOTO lcpos
 CATCH
    GOTO TOP
 ENDTRY
 atendimento.grdatend.setfocus
 RELEASE lcdescontoatendlinhas
ENDFUNC
**
FUNCTION uf_atendimento_aplicadescontovalorlinhas
 atendimento.tmroperador.reset()
 IF  .NOT. (uf_gerais_validapermuser(ch_userno, ch_grupo, 'Atendimento - Permite aplicar descontos'))
    uf_perguntalt_chama("O seu perfil n�o permite aplicar descontos", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF uf_gerais_getparameter('ADM0000000324', 'BOOL')
    PUBLIC cval
    STORE '' TO cval
    uf_tecladoalpha_chama("cval", "INTRODUZA PASSWORD PARA PERMITIR APLICAR DESCONTO:", .T., .F., 0)
    IF EMPTY(cval)
       uf_perguntalt_chama("DEVE INTRODUZIR A PASSWORD.", "OK", "", 64)
       RELEASE cval
       RETURN .F.
    ELSE
       IF  .NOT. (UPPER(ALLTRIM(cval))==UPPER(ALLTRIM(uf_gerais_getparameter('ADM0000000324', 'TEXT'))))
          uf_perguntalt_chama("A PASSWORD N�O EST� CORRECTA.", "OK", "", 64)
          RELEASE cval
          RETURN .F.
       ELSE
          RELEASE cval
       ENDIF
    ENDIF
 ENDIF
 IF myvaledescval
    RETURN .F.
 ENDIF
 IF  .NOT. uf_gerais_getparameter('ADM0000000106', 'BOOL')
    RETURN .F.
 ENDIF
 LOCAL lcvalidadesc
 STORE .T. TO lcvalidadesc
 PUBLIC lcdescontoatendlinhas
 STORE 0 TO lcdescontoatendlinhas
 uf_tecladonumerico_chama("lcDescontoAtendLinhas", "Introduza o Desconto (�):", 0, .T., .F., 6, 2)
 LOCAL lcpos, lcval, lcposicfi
 STORE .F. TO lcval
 STORE 0 TO lcpos
 SELECT fi
 lcpos = RECNO("fi")
 SELECT fi
 SCAN
    IF  .NOT. lcval
       TRY
          GOTO lcpos
       CATCH
       ENDTRY
       TRY
          SKIP
       CATCH
       ENDTRY
       lcval = .T.
    ENDIF
    IF LEFT(fi.design, 1)<>"."
       IF fi.u_descval<>lcdescontoatendlinhas
          IF fi.partes==0
             IF lcdescontoatendlinhas<0
                lcvalidadesc = .F.
             ENDIF
             IF lcdescontoatendlinhas>fi.epv * fi.qtt
                lcvalidadesc = .F.
             ENDIF
             IF USED("uCrsVale")
                SELECT ucrsvale
                SCAN FOR ucrsvale.fistamp==fi.ofistamp
                   IF lcdescontoatendlinhas<ucrsvale.u_descval
                      lcvalidadesc = .F.
                   ENDIF
                ENDSCAN
                SELECT ucrsvale
                GOTO TOP
                SELECT fi
             ENDIF
          ELSE
             lcvalidadesc = .F.
          ENDIF
       ELSE
          lcvalidadesc = .F.
       ENDIF
       IF lcvalidadesc
          SELECT fi
          REPLACE fi.u_descval WITH lcdescontoatendlinhas
     
          lcposicfi = RECNO("FI")
          uf_atendimento_fiiliq(.F.)
          SELECT fi
          TRY
             GOTO lcposicfi
          CATCH
          ENDTRY
          lcposicfi = RECNO("FI")
          uf_atendimento_eventofi(.F.)
          SELECT fi
          TRY
             GOTO lcposicfi
          CATCH
          ENDTRY
       ENDIF
       lcvalidadesc = .T.
    ELSE
       EXIT
    ENDIF
 ENDSCAN
 SELECT fi
 GOTO TOP
 uf_atendimento_infototais()
 SELECT fi
 TRY
    GOTO lcpos
 CATCH
    GOTO TOP
 ENDTRY
 atendimento.grdatend.setfocus
 RELEASE lcdescontoatendlinhas
ENDFUNC
**
PROCEDURE uf_atendimento_fichaprod

 SELECT fi
 IF  .NOT. EMPTY(fi.ref)
    uf_stocks_chama(ALLTRIM(fi.ref))
 ENDIF
 
ENDPROC
**
FUNCTION uf_atendimento_validavenda
 LPARAMETERS lnnv, lncod, lnnr, lnnb, lnnb2, lnsusp
 LOCAL lnvalida, lnvalidado, lncntlin, lncntlin2, lnmaxemb, lnmaxuni, lnlin, lnref, lndev, checkref, lncntpsi, lncntben, lntot, lnpsi, llretval, lnmsg
 STORE .F. TO lnvalida, lnvalidado, lndev, llretval
 STORE 0 TO lncntlin, lncntlin2, lnmaxemb, lnmaxunim, lnlin, lncntpsi, lncntben, lntot, lnpsi
 STORE "" TO lnref, checkref, lnmsg
 LOCAL lctot
 STORE 0 TO lctot
 lncntlin2 = 0
 LOCAL lctiporeceita, lcreservasuspensa
 lcreservasuspensa = .F.
 lctiporeceita = "RM"
 SELECT fi
 GOTO TOP
 SCAN
    IF lnvalida
       IF LEFT(fi.design, 1)=="."
          EXIT
       ELSE
          lnref = fi.ref
          IF EMPTY(fi.partes)
             lndev = .T.
          ENDIF
          IF EMPTY(fi.partes) .AND. (fi.u_comp=.T.) .AND. (EMPTY(fi.epromo) .AND.  .NOT. (ALLTRIM(fi.ref)=='R000001'))
             SELECT ucrsatendst
             LOCATE FOR UPPER(ALLTRIM(ucrsatendst.ref))==UPPER(ALLTRIM(fi.ref))
             IF FOUND() .AND. ALLTRIM(lctiporeceita)<>'RSP' .AND. ALLTRIM(lctiporeceita)<>'DEM' .AND. EMPTY(lcreservasuspensa)
                IF ucrsatendst.protocolo=.T.
                   lncntlin = lncntlin+1
                ELSE
                   lncntlin2 = lncntlin2+1
                ENDIF
             ENDIF
          ENDIF
          IF  .NOT. (UPPER(ALLTRIM(lnref))=='V999999')
             lctot = lctot+fi.etiliquido
          ENDIF
       ENDIF
    ENDIF
    IF ALLTRIM(fi.design)==ALLTRIM(ucrscabvendas.design) .AND. (fi.lordem==ucrscabvendas.lordem)
       lnvalida = .T.
       IF RIGHT(ALLTRIM(fi.design), 18)=="*SUSPENSA RESERVA*"
          lcreservasuspensa = .T.
       ENDIF
       lctiporeceita = ALLTRIM(fi.tipor)
       SELECT ucrsatendcomp
       LOCATE FOR UPPER(ALLTRIM(ucrsatendcomp.fistamp))==UPPER(ALLTRIM(fi.fistamp))
    ENDIF
 ENDSCAN
 lnvalida = .F.
 IF (lctot<0)
    uf_perguntalt_chama("NA VENDA N� "+astr(lnnv)+" N�O PODE TER O TOTAL NEGATIVO.", "OK", "", 64)
    RETURN .F.
 ENDIF
 IF (lncntlin>0) .AND. (lncntlin2>0)
    uf_perguntalt_chama("NA VENDA N� "+astr(lnnv)+" N�O PODE TER PRODUTOS PROTOCOLO E N�O PROTOCOLO.", "OK", "", 64)
    RETURN .F.
 ENDIF
 IF  .NOT. EMPTY(lncod) .AND. (lndev=.T.)
    IF (lncntlin2>0 .AND. ucrsatendcomp.protocolo) .OR. (lncntlin>0 .AND.  .NOT. ucrsatendcomp.protocolo) .OR. (lncntlin>0 .AND. EMPTY(lncod))
       uf_perguntalt_chama("NA VENDA N� "+astr(lnnv)+" SE O PLANO FOR DE PROTOCOLO, OS PRODUTOS TAMB�M T�M DE SER, E VICE-VERSA.", "OK", "", 64)
       RETURN .F.
    ENDIF
 ENDIF
 STORE 0 TO lncntlin, lncntlin2
 IF  .NOT. EMPTY(lncod) .AND. (lndev=.T.)
    SELECT fi
    GOTO TOP
    SCAN
       IF lnvalida
          IF LEFT(fi.design, 1)=="."
             EXIT
          ELSE
             IF fi.u_comp
                lnvalidado = .T.
                IF (ucrsatendcomp.diploma="0") .AND.  .NOT. EMPTY(ALLTRIM(fi.u_diploma)) .AND. UPPER(ALLTRIM(fi.u_diploma))<>'CFT 2.10' 
                   uf_perguntalt_chama("NA VENDA N� "+astr(lnnv)+" O PLANO ["+UPPER(ALLTRIM(lncod))+"] N�O PERMITE DIPLOMAS ASSOCIADOS.", "OK", "", 64)
                   RETURN .F.
                ENDIF
             ENDIF
          ENDIF
       ENDIF
       IF (ALLTRIM(fi.design)==ALLTRIM(ucrscabvendas.design)) .AND. (fi.lordem==ucrscabvendas.lordem)
          lnvalida = .T.
       ENDIF
    ENDSCAN
    lnvalida = .F.
    IF (lnvalidado=.F.)
       IF  .NOT. uf_perguntalt_chama("NA VENDA N� "+astr(lnnv)+" TEM UM PLANO ASSOCIADO � VENDA, MAS N�O TEM PRODUTOS MARCADOS PARA COMPARTICIPA��O. PRETENDE CONTINUAR?", "Sim", "N�o")
          RETURN .F.
       ENDIF
    ENDIF
 ELSE
    SELECT fi
    GOTO TOP
    SCAN
       IF lnvalida
          IF LEFT(fi.design, 1)=="."
             EXIT
          ELSE
             IF fi.u_comp=.T.
                lnvalidado = .T.
             ENDIF
          ENDIF
       ENDIF
       IF ALLTRIM(fi.design)==ALLTRIM(ucrscabvendas.design) .AND. fi.lordem==ucrscabvendas.lordem
          lnvalida = .T.
       ENDIF
    ENDSCAN
    IF (lnvalidado=.T.) .AND. (lndev=.T.)
       IF  .NOT. uf_perguntalt_chama("A VENDA N� "+astr(lnnv)+" N�O TEM PLANO, MAS TEM PRODUTOS MARCADOS PARA COMPARTICIPA��O. PRETENDE CONTINAR?", "Sim", "N�o")
          RETURN .F.
       ENDIF
    ENDIF
    lcvalidade = .F.
    lnvalida = .F.
 ENDIF
 IF (lnsusp==.F.) .AND. UPPER(ALLTRIM(ucrscabvendas.receita_tipo))<>"RSP" .AND. EMPTY(ucrscabvendas.token)
    IF ( .NOT. EMPTY(lncod)) .AND. (lndev=.T.)
       SELECT ucrsatendcomp
       LOCATE FOR UPPER(ALLTRIM(ucrsatendcomp.codigo))==UPPER(ALLTRIM(lncod))
       lnmaxemb = ucrsatendcomp.maxembmed
       lnmaxuni = ucrsatendcomp.maxembuni
       CREATE CURSOR uCrsFi (ref C(18), qtt N(12, 3), u_ettent1 N(15, 3), u_ettent2 N(15, 3), dem L)
       SELECT fi
       GOTO TOP
       SCAN
          IF lnvalida
             IF LEFT(fi.design, 1)=="."
                EXIT
             ELSE
                lnref = IIF( .NOT. EMPTY(fi.ref), fi.ref, fi.oref)
                IF  .NOT. EMPTY(lnref) .AND. fi.partes==0 .AND. EMPTY(fi.id_validacaodem)
                   INSERT INTO uCrsFi (ref, qtt, u_ettent1, u_ettent2, dem) VALUES (lnref, fi.qtt, fi.u_ettent1, fi.u_ettent2, fi.dem)
                ENDIF
             ENDIF
          ENDIF
          IF ALLTRIM(fi.design)==ALLTRIM(ucrscabvendas.design) .AND. fi.lordem==ucrscabvendas.lordem
             lnvalida = .T.
          ENDIF
       ENDSCAN
       lnvalida = .F.
       IF RECCOUNT('uCrsFi')>0
          SELECT ref, SUM(qtt) AS ct, dem  FROM uCrsFi WHERE (u_ettent1+u_ettent2)>0 GROUP BY ref, dem   INTO CURSOR uCrsGrpFi READWRITE
          SELECT ucrsgrpfi
          CALCULATE SUM(ct) TO lnlin 
       ELSE
          lnlin = 0
       ENDIF

       fecha('uCrsFi')
       SELECT ucrsatendcomp
       IF lnlin>ucrsatendcomp.maxembrct
          IF ucrsatendcomp.maxembrct==0
             uf_perguntalt_chama("EXISTEM VENDAS COM PLANOS INV�LIDOS. POR FAVOR VERIFIQUE.", "OK", "", 64)
             RETURN .F.
          ENDIF
          IF  .NOT. uf_perguntalt_chama("Na venda n� "+astr(lnnv)+" o n� m�ximo de embalagens do plano foi excedido. "+CHR(13)+CHR(10)+CHR(13)+CHR(10)+"N.� de Embalagens: "+astr(lnlin)+"; M�ximo do Plano: "+astr(ucrsatendcomp.maxembrct)+"."+CHR(13)+CHR(10)+CHR(13)+CHR(10)+"Se continuar o verso de receita ser� impresso em tal�o.", "Continuar", "Corrigir", 64)
             fecha('uCrsDist')
             RETURN .F.
          ELSE
             fecha('uCrsDist')
          ENDIF
       ENDIF
       fecha('uCrsDist')
       IF (USED("uCrsGrpFi"))
          SELECT ucrsgrpfi
          SCAN
             SELECT ucrsatendst
             LOCATE FOR ALLTRIM(UPPER(ucrsatendst.ref))==UPPER(ALLTRIM(ucrsgrpfi.ref))
             IF (ucrsatendst.tipembdescr='Embalagem N�o Unit�ria') .AND. (ucrsatendst.psico=.F.) .AND. (ucrsgrpfi.ct>lnmaxemb) .AND. (lnmaxemb>0) .AND. Empty(ucrsgrpfi.dem)
                uf_perguntalt_chama("NA VENDA N� "+astr(lnnv)+" O PLANO DE COMPARTICIPA��O '"+UPPER(lncod)+"' N�O PERMITE PARA O ARTIGO '"+ALLTRIM(ucrsgrpfi.ref)+"' UM N�MERO DE EMBALAGENS SUPERIOR A "+astr(lnmaxemb)+". NO ENTANTO NESTA VENDA EXISTEM "+astr(ucrsgrpfi.ct)+" REFERENTES A ESTE PRODUTO", "OK", "", 64)
                fecha('uCrsGrpFi')
                RETURN .F.
             ENDIF
             IF (ucrsatendst.tipembdescr='Embalagem Unit�ria' .OR. ucrsatendst.psico=.T.) .AND. (ucrsgrpfi.ct>lnmaxuni) .AND. (lnmaxemb>0) .AND. Empty(ucrsgrpfi.dem)
                uf_perguntalt_chama("NA VENDA N� "+astr(lnnv)+" O PLANO DE COMPARTICIPA��O '"+UPPER(lncod)+"' N�O PERMITE PARA O ARTIGO '"+ALLTRIM(ucrsgrpfi.ref)+"' UM N�MERO DE EMBALAGENS SUPERIOR A "+astr(lnmaxuni)+". NO ENTANTO NESTA VENDA EXISTEM "+astr(ucrsgrpfi.ct)+" REFERENTES A ESTE PRODUTO", "OK", "", 64)
                fecha('uCrsGrpFi')
                RETURN .F.
             ENDIF
          ENDSCAN
          fecha('uCrsGrpFi')
       ENDIF
    ENDIF
 ENDIF
 IF (lnsusp==.F.)
    IF  .NOT. EMPTY(lncod) .AND. (lndev==.T.)
       IF ucrsatendcomp.beneficiario .AND. EMPTY(lnnb2)
          IF (ALLTRIM(ucrsatendcomp.codigo)<>'FN' .AND. ALLTRIM(ucrsatendcomp.codigo)<>'LS' .AND. ALLTRIM(ucrsatendcomp.codigo)<>'77' .AND. UPPER(ALLTRIM(ucrsatendcomp.codigo))<>'FL')
             uf_tecladoalpha_chama("uCrsCabVendas.u_nbenef2", "Introduza o Nr. Benefici�rio:", .F., .F., 2)
			 IF !EMPTY(ucrsatendcomp.regexCartao)
			 	IF !uf_gerais_checkRegex(ALLTRIM(ucrsatendcomp.regexCartao), ALLTRIM(uCrsCabVendas.u_nbenef2))
					uf_perguntalt_chama("Para esta comparticipa��o o N� do Cart�o ter� de ter o seguinte formato: " + chr(13) + ALLTRIM(uCrsAtendComp.regexCartaoSample), "OK", "", 48)
					REPLACE ucrscabvendas.u_nbenef2 WITH ""
				ENDIF
			 ENDIF
             IF EMPTY(ucrscabvendas.u_nbenef2)
                uf_perguntalt_chama("NA VENDA N� "+astr(lnnv)+" TEM QUE INTRODUZIR UM N� DE BENEFICI�RIO.", "OK", "", 64)
                RETURN .F.
             ELSE
                SELECT fi
                LOCATE FOR fi.lordem=ucrscabvendas.lordem
                REPLACE fi.u_nbenef2 WITH ALLTRIM(ucrscabvendas.u_nbenef2)
             ENDIF
          ELSE
             IF ALLTRIM(ucrsatendcomp.codigo)=='FN'
                IF EMPTY(ft.ncont) .OR. UPPER(ALLTRIM(ft.ncont))=uf_gerais_getparameter_site("ADM0000000075", "text", mysite)
                   uf_tecladoalpha_chama("uCrsCabVendas.u_nbenef2", "Introduza o Nr. De Contribuinte.:", .F., .F., 2)
                   IF EMPTY(ucrscabvendas.u_nbenef2)
                      uf_perguntalt_chama("NA VENDA N� "+astr(lnnv)+" TEM QUE INTRODUZIR O N� DE CONTRIBUINTE.", "OK", "", 64)
                      RETURN .F.
                   ELSE
                      SELECT fi
                      LOCATE FOR fi.lordem=ucrscabvendas.lordem
                      REPLACE fi.u_nbenef2 WITH ALLTRIM(ucrscabvendas.u_nbenef2)
                   ENDIF
                ELSE
                   LOCAL lcnconttemp
                   STORE '' TO lcnconttemp
                   SELECT ucrsatendcl
                   lcnconttemp = ALLTRIM(ucrsatendcl.ncont)
                   SELECT fi
                   LOCATE FOR fi.lordem=ucrscabvendas.lordem
                   REPLACE fi.u_nbenef2 WITH lcnconttemp
                   REPLACE ucrscabvendas.u_nbenef2 WITH lcnconttemp
                ENDIF
             ELSE
                uf_tecladoalpha_chama("uCrsCabVendas.u_nbenef2", "Introduza o Nr. De Sinistro.:", .F., .F., 2)
                IF EMPTY(ucrscabvendas.u_nbenef2)
                   uf_perguntalt_chama("NA VENDA N� "+astr(lnnv)+" TEM QUE INTRODUZIR O N� DE SINISTRO.", "OK", "", 64)
                   RETURN .F.
                ELSE
                   SELECT fi
                   LOCATE FOR fi.lordem=ucrscabvendas.lordem
                   REPLACE fi.u_nbenef2 WITH LEFT(ALLTRIM(ucrscabvendas.u_nbenef2), 20)
                ENDIF
             ENDIF
          ENDIF
       ENDIF
    ENDIF
 ENDIF
 SELECT fi
 GOTO TOP
 SCAN
    IF lnvalida
       IF LEFT(fi.design, 1)=="."
          EXIT
       ENDIF
    ENDIF
    IF ALLTRIM(fi.design)==ALLTRIM(ucrscabvendas.design) .AND. fi.lordem==ucrscabvendas.lordem
       lnvalida = .T.
    ENDIF
 ENDSCAN
 lnvalida = .F.
 SELECT fi
 GOTO TOP
 SCAN
    IF lnvalida
       IF LEFT(fi.design, 1)=="."
          EXIT
       ELSE
          IF (fi.partes=0) .AND. (fi.u_psico)
             IF (fi.qtt>uf_gerais_getParameter("ADM0000000357", "NUM"))
                uf_perguntalt_chama("NA VENDA N�"+astr(lnnv)+" O ARTIGO "+ALLTRIM(fi.design)+" � UM PSICOTR�PPICO, E COMO TAL, N�O PODE TER UMA QUANTIDADE NA LINHA SUPERIOR A 4.", "OK", "", 64)
                RETURN .F.
             ENDIF
             lncntlin = lncntlin+1
             lnpsi = lnpsi+1
          ENDIF
          IF  .NOT. fi.epromo .AND. ALLTRIM(fi.ref)<>'R000001'
             lntot = lntot+1
          ENDIF
       ENDIF
    ENDIF
    IF (ALLTRIM(fi.design)==ALLTRIM(ucrscabvendas.design)) .AND. (fi.lordem==ucrscabvendas.lordem)
       lnvalida = .T.
    ENDIF
 ENDSCAN
 lnvalida = .F.
 lncntlin = 0
 IF UPPER(ALLTRIM(ucrscabvendas.receita_tipo))<>'RSP'
    llretval =  .NOT. (lnpsi>0 .AND. lnpsi<>lntot)
    IF  .NOT. llretval
       uf_perguntalt_chama("N�O PODEM EXISTIR PSICOTR�PICOS E N�O PSICOTR�PICOS NA MESMA VENDA. OS PSICOTR�PICOS DEVEM SER GRAVADOS NUMA VENDA ISOLADA.", "OK", "", 64)
       RETURN llretval
    ENDIF
 ENDIF
 RETURN .T.
ENDFUNC
**
FUNCTION uf_validarecalculofacrec
 LOCAL lcvalidareg, lccompfact, lcvalida, lcpos, lcposicfi
 STORE .F. TO lcvalidareg, lccompfact, lcvalida
 STORE 0 TO lcpos, lcposicfi
 IF  .NOT. USED("uCrsRecFac")
    CREATE CURSOR uCrsRecFac (design C(60), pos N(10), marcada L, plano1 C(5), plano2 C(5), stamp C(28), stamp2 C(29), diploma L)
 ENDIF
 SELECT fi
 GOTO TOP
 SCAN
    IF lcvalida
       IF LEFT(fi.design, 1)=="."
          lcvalida = .F.
       ELSE
          IF fi.marcada<>.T. .AND. fi.partes=0 .OR. (fi.marcada=.T. .AND. fi.partes<>0)
             SELECT ucrsrecfac
             REPLACE ucrsrecfac.marcada WITH .T.
             SELECT fi
             IF fi.u_ettent1+fi.u_ettent2>0
                lccompfact = .T.
             ENDIF
          ELSE
             IF USED("uCrsVale")
                SELECT ucrsvale
                GOTO TOP
                SCAN FOR ALLTRIM(ucrsvale.fistamp)==ALLTRIM(fi.ofistamp)
                   IF  .NOT. (ucrsvale.u_diploma==fi.u_diploma)
                      SELECT ucrsrecfac
                      REPLACE ucrsrecfac.diploma WITH .T.
                   ENDIF
                   SELECT ucrsvale
                ENDSCAN
             ENDIF
             SELECT ucrsvale
             GOTO TOP
             IF  .NOT. (ALLTRIM(fi.tkhposlstamp)==ALLTRIM(ucrsrecfac.stamp)) .AND. fi.u_ettent1+fi.u_ettent2>0 .AND. fi.partes=0
                SELECT ucrsrecfac
                REPLACE ucrsrecfac.stamp2 WITH "xxxzzz"
             ENDIF
          ENDIF
       ENDIF
    ENDIF
    IF LEFT(ALLTRIM(fi.design), 1)=="." .AND.  .NOT. EMPTY(fi.tkhposlstamp) .AND. fi.marcada=.T.
       lcvalida = .T.
       SELECT fi
       lcpos = RECNO()
       SELECT ucrsrecfac
       APPEND BLANK
       REPLACE ucrsrecfac.design WITH ALLTRIM(fi.design)
       REPLACE ucrsrecfac.pos WITH lcpos
       REPLACE ucrsrecfac.marcada WITH .F.
       REPLACE ucrsrecfac.plano1 WITH fi.nccod
       REPLACE ucrsrecfac.plano2 WITH STREXTRACT(fi.design, '[', ']', 1, 0)
       REPLACE ucrsrecfac.stamp WITH ALLTRIM(fi.tkhposlstamp)
       REPLACE ucrsrecfac.stamp2 WITH ALLTRIM(fi.tkhposlstamp)
       REPLACE ucrsrecfac.diploma WITH .F.
    ENDIF
    SELECT fi
 ENDSCAN
 lcvalida = .F.
 SELECT ucrsrecfac
 GOTO TOP
 SCAN
    IF ucrsrecfac.marcada=.T. .OR. ucrsrecfac.plano1<>ucrsrecfac.plano2 .OR. ALLTRIM(ucrsrecfac.stamp)<>ALLTRIM(ucrsrecfac.stamp2) .OR. ucrsrecfac.diploma=.T.
       IF UPPER(ALLTRIM(ucrse1.pais))=='PORTUGAL'
          IF  .NOT. uf_perguntalt_chama("ATEN��O: A VENDA ["+ALLTRIM(ucrsrecfac.design)+"] CONTEM PRODUTOS A CR�DITO QUE T�M DE SER RECALCULADOS. OS PRODUTOS DESTA VENDA V�O DAR ORIGEM A UMA NOVA VENDA."+CHR(10)+CHR(10)+"PRETENDE CONTINUAR?.", "Sim", "N�o")
             fecha("uCrsRecFac")
             RETURN .F.
          ENDIF
       ENDIF
    ENDIF
 ENDSCAN
 SELECT ucrsrecfac
 GOTO TOP
 SCAN FOR marcada=.T. .OR. plano1<>plano2 .OR. ALLTRIM(ucrsrecfac.stamp)<>ALLTRIM(ucrsrecfac.stamp2) .OR. diploma=.T.
    SELECT fi
    GOTO TOP
    SCAN
       IF lcvalida
          IF LEFT(ALLTRIM(fi.design), 1)=="."
             lcvalida = .F.
          ELSE
             lcposicfi = RECNO("FI")
             uf_atendimento_eventofi(.T.)
             TRY
                GOTO lcposicfi
             CATCH
             ENDTRY
          ENDIF
       ENDIF
       IF LEFT(ALLTRIM(fi.design), 1)=="." .AND. RECNO()==ucrsrecfac.pos
          lcvalida = .T.
       ENDIF
    ENDSCAN
    SELECT ucrsrecfac
 ENDSCAN
 SELECT ucrsrecfac
 GOTO TOP
 SCAN FOR marcada==.F. .AND. plano1==plano2 .AND. ALLTRIM(ucrsrecfac.stamp)==ALLTRIM(ucrsrecfac.stamp2) .AND. diploma==.F.
    SELECT fi
    GOTO TOP
    SCAN
       IF lcvalida
          IF LEFT(ALLTRIM(fi.design), 1)=="."
             lcvalida = .F.
          ELSE
             IF  .NOT. EMPTY(fi.ofistamp)
                SELECT ucrsvale
                GOTO TOP
                SCAN FOR ALLTRIM(ucrsvale.fistamp)==ALLTRIM(fi.ofistamp)
                   SELECT fi
                   REPLACE fi.epv WITH ucrsvale.epv
                   REPLACE fi.u_txcomp WITH ucrsvale.u_txcomp
                   REPLACE fi.u_epvp WITH ucrsvale.u_epvp
                   REPLACE fi.u_ettent1 WITH ucrsvale.u_ettent1
                   REPLACE fi.u_ettent2 WITH ucrsvale.u_ettent2
                   lcposicfi = RECNO("FI")
                   uf_atendimento_eventofi()
                   TRY
                      GOTO lcposicfi
                   CATCH
                   ENDTRY
                   SELECT ucrsvale
                ENDSCAN
                SELECT ucrsvale
                GOTO TOP
             ENDIF
          ENDIF
       ENDIF
       IF LEFT(ALLTRIM(fi.design), 1)=="." .AND. RECNO()==ucrsrecfac.pos
          lcvalida = .T.
       ENDIF
       SELECT fi
    ENDSCAN
    SELECT ucrsrecfac
 ENDSCAN
 SELECT ucrsrecfac
 DELETE ALL
 SELECT fi
 GOTO TOP
 SCAN
    IF lcvalida
       IF LEFT(fi.design, 1)=="."
          lcvalida = .F.
       ELSE
          IF USED("uCrsVale")
             SELECT ucrsvale
             GOTO TOP
             SCAN FOR ALLTRIM(ucrsvale.fistamp)==ALLTRIM(fi.ofistamp)
                IF (fi.marcada<>.T. .OR.  .NOT. EMPTY(fi.tkhposlstamp)) .AND. ALLTRIM(fi.u_diploma)==ALLTRIM(ucrsvale.u_diploma)
                   SELECT ucrsrecfac
                   REPLACE ucrsrecfac.marcada WITH .F.
                ENDIF
                SELECT ucrsvale
             ENDSCAN
             SELECT ucrsvale
             GOTO TOP
          ENDIF
       ENDIF
    ENDIF
    IF LEFT(ALLTRIM(fi.design), 1)=="." .AND. EMPTY(fi.tkhposlstamp)
       lcvalida = .T.
       SELECT fi
       lcpos = RECNO()
       SELECT ucrsrecfac
       APPEND BLANK
       REPLACE ucrsrecfac.design WITH ALLTRIM(fi.design)
       REPLACE ucrsrecfac.pos WITH lcpos
       REPLACE ucrsrecfac.marcada WITH .T.
       REPLACE ucrsrecfac.plano1 WITH fi.nccod
       REPLACE ucrsrecfac.plano2 WITH STREXTRACT(fi.design, '[', ']', 1, 0)
    ENDIF
    SELECT fi
 ENDSCAN
 SELECT ucrsrecfac
 GOTO TOP
 SCAN FOR marcada==.T. .AND. (ALLTRIM(ucrsrecfac.plano1))==ALLTRIM(ucrsrecfac.plano2)
    SELECT fi
    GOTO TOP
    SCAN
       IF lcvalida
          IF LEFT(ALLTRIM(fi.design), 1)=="."
             lcvalida = .F.
          ELSE
             IF  .NOT. EMPTY(fi.ofistamp)
                SELECT ucrsvale
                GOTO TOP
                SCAN FOR ALLTRIM(ucrsvale.fistamp)==ALLTRIM(fi.ofistamp)
                   SELECT fi
                   REPLACE fi.epv WITH ucrsvale.epv, fi.u_txcomp WITH ucrsvale.u_txcomp, fi.u_epvp WITH ucrsvale.u_epvp, fi.u_ettent1 WITH ucrsvale.u_ettent1, fi.u_ettent2 WITH ucrsvale.u_ettent2
                   lcposicfi = RECNO("FI")
                   uf_atendimento_eventofi()
                   TRY
                      GOTO lcposicfi
                   CATCH
                   ENDTRY
                   SELECT ucrsvale
                ENDSCAN
                SELECT ucrsvale
                GOTO TOP
             ENDIF
          ENDIF
       ENDIF
       IF LEFT(ALLTRIM(fi.design), 1)=="." .AND. RECNO()==ucrsrecfac.pos
          lcvalida = .T.
       ENDIF
       SELECT fi
    ENDSCAN
    SELECT ucrsrecfac
 ENDSCAN
 SELECT fi
 GOTO TOP
 IF USED("uCrsRecFac")
    fecha("uCrsRecFac")
 ENDIF
 RETURN .T.
ENDFUNC
**
FUNCTION uf_atendimento_validapwdev
 IF mypermitedevolucoes .AND.  .NOT. EMPTY(mypwdevolucoes)
    SELECT fi
    GOTO TOP
    SCAN FOR fi.partes<>0 .AND.  .NOT. (ALLTRIM(fi.ref)=="R000001")
       PUBLIC cval
       STORE '' TO cval
       uf_tecladoalpha_chama("cval", "INTRODUZA PASSWORD DE DEVOLU��ES:", .T., .F., 0)
       IF EMPTY(cval)
          uf_perguntalt_chama("DEVE INTRODUZIR A PASSWORD DE SUPERVISOR PARA PERMITIR DEVOLU��ES.", "OK", "", 64)
          RETURN .F.
       ELSE
          IF  .NOT. (UPPER(ALLTRIM(cval))==UPPER(ALLTRIM(mypwdevolucoes)))
             uf_perguntalt_chama("A PASSWORD DE SUPERVISOR N�O EST� CORRECTA.", "OK", "", 64)
             RETURN .F.
          ENDIF
       ENDIF
       EXIT
    ENDSCAN
    RETURN .T.
 ELSE
    RETURN .T.
 ENDIF
ENDFUNC
**
FUNCTION uf_atendimento_adicionaprod
 LPARAMETERS lcref
 IF EMPTY(lcref)
    uf_perguntalt_chama("A REFER�NCIA N�O PODE SER VAZIA.", "OK", "", 48)
    RETURN .F.
 ENDIF
 uf_atendimento_adicionalinhafi()
 SELECT fi
 REPLACE fi.ref WITH lcref
 uf_atendimento_actref()
 SELECT fi
 IF usamancompart
   IF TYPE("ATENDIMENTO") <> "U"
   uf_atendimento_aplicaTabCompartAddLine()
   ENDIF
   uf_atendimento_adicionalinhafimancompart()
 ENDIF
 lcposicfi = RECNO("FI")
 uf_atendimento_eventofi()
 TRY
    GOTO lcposicfi
 CATCH
 ENDTRY
 atendimento.grdatend.refresh
ENDFUNC
**
FUNCTION uf_atendimento_validaexisteregistofi
 LPARAMETERS lcfistamp
 SELECT fi
 LOCATE FOR UPPER(ALLTRIM(fi.fistamp))==UPPER(ALLTRIM(lcfistamp))
 IF FOUND()
    RETURN .T.
 ELSE
    RETURN .F.
 ENDIF
 RETURN .F.
ENDFUNC
**
PROCEDURE uf_atendimento_acttotaisft
 LPARAMETERS tcbool
 LOCAL lctotalcomiva, lctotalsemiva, lcvaloriva1, lcvaloriva2, lcvaloriva3, lcvaloriva4, lcvaloriva5, lcvaloriva6, lcvaloriva7, lcvaloriva8, lcvaloriva9, lctotaliva, lctotalnservico, lctotqtt, lcqtt1, lcedescc, lcvaloriva10, lcvaloriva11, lcvaloriva12, lcvaloriva13, lceivain10, lceivain11, lceivain12, lceivain13
 STORE 0 TO lctotalcomiva, lctotalsemiva, lcvaloriva1, lcvaloriva2, lcvaloriva3, lcvaloriva4, lcvaloriva5, lcvaloriva6, lcvaloriva7, lcvaloriva8, lcvaloriva9, lctotaliva, lctotalnservico
 STORE 0 TO lceivain1, lceivain2, lceivain3, lceivain4, lceivain5, lceivain6, lceivain7, lceivain8, lceivain9, lctotqtt, lcqtt1, lcedescc, lcvaloriva10, lcvaloriva11, lcvaloriva12, lcvaloriva13, lceivain10, lceivain11, lceivain12, lceivain13
 LOCAL lcfipos, lcvalorsemiva, lcdesc, lctotdesc
 STORE 0 TO lcfipos, lcvalorsemiva, lcdesc, lctotdesc
 SELECT fi
 lcfipos = RECNO()
 GOTO TOP
 SCAN
    lctotqtt = lctotqtt+fi.qtt
    lcqtt1 = lcqtt1+fi.qtt
    IF fi.ivaincl==.T.
       lctotalcomiva = lctotalcomiva+fi.etiliquido
       lctotalsemiva = lctotalsemiva+(fi.etiliquido/(fi.iva/100+1))
    ELSE
       lctotalcomiva = lctotalcomiva+(fi.etiliquido*(fi.iva/100+1))
       lctotalsemiva = lctotalsemiva+(fi.etiliquido)
    ENDIF
    DO CASE
       CASE fi.tabiva=1
          IF fi.ivaincl
             lceivain1 = lceivain1+(fi.etiliquido/(fi.iva/100+1))
             lcvaloriva1 = lcvaloriva1+fi.etiliquido-(fi.etiliquido/(fi.iva/100+1))
          ELSE
             lceivain1 = lceivain1+fi.etiliquido
             lcvaloriva1 = lcvaloriva1+(fi.etiliquido*(fi.iva/100))
          ENDIF
       CASE fi.tabiva=2
          IF fi.ivaincl
             lceivain2 = lceivain2+(fi.etiliquido/(fi.iva/100+1))
             lcvaloriva2 = lcvaloriva2+fi.etiliquido-(fi.etiliquido/(fi.iva/100+1))
          ELSE
             lceivain2 = lceivain2+fi.etiliquido
             lcvaloriva2 = lcvaloriva2+(fi.etiliquido*(fi.iva/100))
          ENDIF
       CASE fi.tabiva=3
          IF fi.ivaincl
             lceivain3 = lceivain3+(fi.etiliquido/(fi.iva/100+1))
             lcvaloriva3 = lcvaloriva3+fi.etiliquido-(fi.etiliquido/(fi.iva/100+1))
          ELSE
             lceivain3 = lceivain3+fi.etiliquido
             lcvaloriva3 = lcvaloriva3+(fi.etiliquido*(fi.iva/100))
          ENDIF
       CASE fi.tabiva=4
          IF fi.ivaincl
             lceivain4 = lceivain4+(fi.etiliquido/(fi.iva/100+1))
             lcvaloriva4 = lcvaloriva4+fi.etiliquido-(fi.etiliquido/(fi.iva/100+1))
          ELSE
             lceivain4 = lceivain4+fi.etiliquido
             lcvaloriva4 = lcvaloriva4+(fi.etiliquido*(fi.iva/100))
          ENDIF
       CASE fi.tabiva=5
          IF fi.ivaincl
             lceivain5 = lceivain5+(fi.etiliquido/(fi.iva/100+1))
             lcvaloriva5 = lcvaloriva5+fi.etiliquido-(fi.etiliquido/(fi.iva/100+1))
          ELSE
             lceivain5 = lceivain5+fi.etiliquido
             lcvaloriva5 = lcvaloriva5+(fi.etiliquido*(fi.iva/100))
          ENDIF
       CASE fi.tabiva=6
          IF fi.ivaincl
             lceivain6 = lceivain6+(fi.etiliquido/(fi.iva/100+1))
             lcvaloriva6 = lcvaloriva6+fi.etiliquido-(fi.etiliquido/(fi.iva/100+1))
          ELSE
             lceivain6 = lceivain6+fi.etiliquido
             lcvaloriva6 = lcvaloriva6+(fi.etiliquido*(fi.iva/100))
          ENDIF
       CASE fi.tabiva=7
          IF fi.ivaincl
             lceivain7 = lceivain7+(fi.etiliquido/(fi.iva/100+1))
             lcvaloriva7 = lcvaloriva7+fi.etiliquido-(fi.etiliquido/(fi.iva/100+1))
          ELSE
             lceivain7 = lceivain7+fi.etiliquido
             lcvaloriva7 = lcvaloriva7+(fi.etiliquido*(fi.iva/100))
          ENDIF
       CASE fi.tabiva=8
          IF fi.ivaincl
             lceivain8 = lceivain8+(fi.etiliquido/(fi.iva/100+1))
             lcvaloriva8 = lcvaloriva8+fi.etiliquido-(fi.etiliquido/(fi.iva/100+1))
          ELSE
             lceivain8 = lceivain8+fi.etiliquido
             lcvaloriva8 = lcvaloriva8+(fi.etiliquido*(fi.iva/100))
          ENDIF
       CASE fi.tabiva=9
          IF fi.ivaincl
             lceivain9 = lceivain9+(fi.etiliquido/(fi.iva/100+1))
             lcvaloriva9 = lcvaloriva9+fi.etiliquido-(fi.etiliquido/(fi.iva/100+1))
          ELSE
             lceivain9 = lceivain9+fi.etiliquido
             lcvaloriva9 = lcvaloriva9+(fi.etiliquido*(fi.iva/100))
          ENDIF
       CASE fi.tabiva=10
          IF fi.ivaincl
             lceivain10 = lceivain10+(fi.etiliquido/(fi.iva/100+1))
             lcvaloriva10 = lcvaloriva10+fi.etiliquido-(fi.etiliquido/(fi.iva/100+1))
          ELSE
             lceivain10 = lceivain10+fi.etiliquido
             lcvaloriva10 = lcvaloriva10+(fi.etiliquido*(fi.iva/100))
          ENDIF
       CASE fi.tabiva=11
          IF fi.ivaincl
             lceivain11 = lceivain11+(fi.etiliquido/(fi.iva/100+1))
             lcvaloriva11 = lcvaloriva11+fi.etiliquido-(fi.etiliquido/(fi.iva/100+1))
          ELSE
             lceivain11 = lceivain11+fi.etiliquido
             lcvaloriva11 = lcvaloriva11+(fi.etiliquido*(fi.iva/100))
          ENDIF
       CASE fi.tabiva=12
          IF fi.ivaincl
             lceivain12 = lceivain12+(fi.etiliquido/(fi.iva/100+1))
             lcvaloriva12 = lcvaloriva12+fi.etiliquido-(fi.etiliquido/(fi.iva/100+1))
          ELSE
             lceivain12 = lceivain12+fi.etiliquido
             lcvaloriva12 = lcvaloriva12+(fi.etiliquido*(fi.iva/100))
          ENDIF
       CASE fi.tabiva=13
          IF fi.ivaincl
             lceivain13 = lceivain13+(fi.etiliquido/(fi.iva/100+1))
             lcvaloriva13 = lcvaloriva13+fi.etiliquido-(fi.etiliquido/(fi.iva/100+1))
          ELSE
             lceivain13 = lceivain13+fi.etiliquido
             lcvaloriva13 = lcvaloriva13+(fi.etiliquido*(fi.iva/100))
          ENDIF
    ENDCASE
    IF (fi.desconto>0) .OR. (fi.desc2>0) .OR. (fi.u_descval>0)
       IF fi.ivaincl
          lcvalorsemiva = (fi.epv*fi.qtt)/(fi.iva/100+1)
       ELSE
          lcvalorsemiva = fi.epv*fi.qtt
       ENDIF
       IF fi.desconto>0
          lcdesc = lcvalorsemiva-(lcvalorsemiva*((100-fi.desconto)/100))
          lctotdesc = lctotdesc+lcdesc
          lcvalorsemiva = lcvalorsemiva-lctotdesc
       ENDIF
       IF fi.desc2>0
          lcdesc = lcvalorsemiva-(lcvalorsemiva*((100-fi.desc2)/100))
          lctotdesc = lctotdesc+lcdesc
          lcvalorsemiva = lcvalorsemiva-lctotdesc
       ENDIF
       IF fi.u_descval>0
          IF fi.ivaincl
             lcdesc = fi.u_descval/(fi.iva/100+1)
          ELSE
             lcdesc = fi.u_descval
          ENDIF
          lctotdesc = lctotdesc+lcdesc
       ENDIF
    ENDIF
    IF fi.stns=.F.
       IF fi.ivaincl
          lctotalnservico = lctotalnservico+(fi.etiliquido/(fi.iva/100+1))
       ELSE
          lctotalnservico = lctotalnservico+fi.etiliquido
       ENDIF
    ENDIF
    SELECT fi
 ENDSCAN
 IF tcbool==.T.
    SELECT ft
    IF ft.fin==0
       REPLACE ft.efinv WITH 0
    ENDIF
    IF ft.fin>0
       REPLACE ft.efinv WITH ROUND(lctotalcomiva*ft.fin/100, 2)
    ENDIF
 ELSE
    SELECT ft
    IF ft.efinv==0
       REPLACE ft.fin WITH 0
    ENDIF
    IF ft.efinv>0 .AND. ft.efinv<lctotalcomiva
       REPLACE ft.fin WITH ROUND(ft.efinv*100/lctotalcomiva, 2)
    ELSE
       REPLACE ft.efinv WITH 0
       REPLACE ft.fin WITH 0
    ENDIF
 ENDIF
 lctotaliva = lcvaloriva1+lcvaloriva2+lcvaloriva3+lcvaloriva4+lcvaloriva5+lcvaloriva6+lcvaloriva7+lcvaloriva8+lcvaloriva9+lcvaloriva10+lcvaloriva11+lcvaloriva12+lcvaloriva13
 SELECT ft
 REPLACE ft.ettiliq WITH ROUND(lctotalsemiva, 2)
 REPLACE ft.ettiva WITH ROUND(lctotaliva, 2)
 REPLACE ft.etotal WITH ft.ettiliq+ft.ettiva
 IF ft.efinv<>0
    REPLACE ft.etotal WITH ft.etotal-ft.efinv
 ENDIF
 REPLACE ft.edescc WITH ROUND(lctotdesc, 2)
 REPLACE ft.descc WITH ROUND(lctotdesc*200.482 , 2)
 REPLACE ft.eivav1 WITH ROUND(lcvaloriva1, 2)
 REPLACE ft.ivav1 WITH ROUND((lcvaloriva1*200.482 ), 2)
 REPLACE ft.eivain1 WITH ROUND(lceivain1, 2)
 REPLACE ft.ivain1 WITH ROUND((lceivain1*200.482 ), 2)
 REPLACE ft.eivav2 WITH ROUND(lcvaloriva2, 2)
 REPLACE ft.ivav2 WITH ROUND((lcvaloriva2*200.482 ), 2)
 REPLACE ft.eivain2 WITH ROUND(lceivain2, 2)
 REPLACE ft.ivain2 WITH ROUND((lceivain2*200.482 ), 2)
 REPLACE ft.eivav3 WITH ROUND(lcvaloriva3, 2)
 REPLACE ft.ivav3 WITH ROUND((lcvaloriva3*200.482 ), 2)
 REPLACE ft.eivain3 WITH ROUND(lceivain3, 2)
 REPLACE ft.ivain3 WITH ROUND((lceivain3*200.482 ), 2)
 REPLACE ft.eivav4 WITH ROUND(lcvaloriva4, 2)
 REPLACE ft.ivav4 WITH ROUND((lcvaloriva4*200.482 ), 2)
 REPLACE ft.eivain4 WITH ROUND(lceivain4, 2)
 REPLACE ft.ivain4 WITH ROUND((lceivain4*200.482 ), 2)
 REPLACE ft.eivav5 WITH ROUND(lcvaloriva5, 2)
 REPLACE ft.ivav5 WITH ROUND((lcvaloriva5*200.482 ), 2)
 REPLACE ft.eivain5 WITH ROUND(lceivain5, 2)
 REPLACE ft.ivain5 WITH ROUND((lceivain5*200.482 ), 2)
 REPLACE ft.eivav6 WITH ROUND(lcvaloriva6, 2)
 REPLACE ft.ivav6 WITH ROUND((lcvaloriva6*200.482 ), 2)
 REPLACE ft.eivain6 WITH ROUND(lceivain6, 2)
 REPLACE ft.ivain6 WITH ROUND((lceivain6*200.482 ), 2)
 REPLACE ft.eivav7 WITH ROUND(lcvaloriva7, 2)
 REPLACE ft.ivav7 WITH ROUND((lcvaloriva7*200.482 ), 2)
 REPLACE ft.eivain7 WITH ROUND(lceivain7, 2)
 REPLACE ft.ivain7 WITH ROUND((lceivain7*200.482 ), 2)
 REPLACE ft.eivav8 WITH ROUND(lcvaloriva8, 2)
 REPLACE ft.ivav8 WITH ROUND((lcvaloriva8*200.482 ), 2)
 REPLACE ft.eivain8 WITH ROUND(lceivain8, 2)
 REPLACE ft.ivain8 WITH ROUND((lceivain8*200.482 ), 2)
 REPLACE ft.eivav9 WITH ROUND(lcvaloriva9, 2)
 REPLACE ft.ivav9 WITH ROUND((lcvaloriva9*200.482 ), 2)
 REPLACE ft.eivain9 WITH ROUND(lceivain9, 2)
 REPLACE ft.ivain9 WITH ROUND((lceivain9*200.482 ), 2)
 REPLACE ft.eivav10 WITH ROUND(lcvaloriva10, 2)
 REPLACE ft.eivain10 WITH ROUND(lceivain10, 2)
 REPLACE ft.eivav11 WITH ROUND(lcvaloriva11, 2)
 REPLACE ft.eivain11 WITH ROUND(lceivain11, 2)
 REPLACE ft.eivav12 WITH ROUND(lcvaloriva12, 2)
 REPLACE ft.eivain12 WITH ROUND(lceivain12, 2)
 REPLACE ft.eivav13 WITH ROUND(lcvaloriva13, 2)
 REPLACE ft.eivain13 WITH ROUND(lceivain13, 2)
 REPLACE ft.totqtt WITH lctotqtt
 REPLACE ft.qtt1 WITH lcqtt1
 SELECT fi
 IF lcfipos>0
    TRY
       GOTO lcfipos
    CATCH
    ENDTRY
 ENDIF
 IF TYPE("FACTURACAO")<>"U"
    facturacao.refresh()
 ENDIF
ENDPROC
**
FUNCTION uf_atendimento_actvaloresfi
 LPARAMETERS uv_fromImport 
 
 
 IF TYPE("FACTURACAO")<>"U"
    SELECT fi
    IF EMPTY(ALLTRIM(fi.ref))
       RETURN .T.
    ENDIF
 ENDIF
 SELECT ft
 IF UPPER(ALLTRIM(ft.nmdoc))=="FACTURA RESUMO"
    RETURN .T.
 ENDIF

 SELECT fi
 IF fi.epromo
    REPLACE fi.desconto WITH 0
    REPLACE fi.u_descval WITH 0
    RETURN .T.
 ENDIF
 SELECT fi
 DO CASE
    CASE (fi.desconto<0) .OR. (fi.desconto>100)
       REPLACE fi.desconto WITH 0
    CASE (fi.desc2<0) .OR. (fi.desc2>100)
       REPLACE fi.desc2 WITH 0
    CASE (fi.desc3<0) .OR. (fi.desc3>100)
       REPLACE fi.desc3 WITH 0
    CASE (fi.desc4<0) .OR. (fi.desc4>100)
       REPLACE fi.desc4 WITH 0
    CASE (fi.u_descval<0) .OR. fi.u_descval>(ROUND(fi.epv*fi.qtt-(fi.epv*fi.qtt*(fi.desconto/100)), 2))
       REPLACE fi.u_descval WITH 0
    OTHERWISE
 ENDCASE
 LOCAL lcpercadicseg
 lcpercadicseg = 0
 IF USED("uCrsAtendComp")
    SELECT ucrsatendcomp
    GOTO TOP
    IF ucrsatendcomp.percadic>0
       lcpercadicseg = ucrsatendcomp.percadic
    ENDIF
 ENDIF
 LOCAL lccod, lccod2, lcref, lcdiploma, txcomp
 STORE "" TO lcref, lcdiploma
 lccod = ft2.u_codigo
 lccod2 = ft2.u_codigo2
 txcomp = 0
 IF  .NOT. EMPTY(ALLTRIM(ft2.u_codigo))
    SELECT fi
    IF ( .NOT. EMPTY(ALLTRIM(fi.ref)) .OR.  .NOT. EMPTY(ALLTRIM(fi.oref)))
       IF fi.u_comp

          lcref = IIF( .NOT. EMPTY(ALLTRIM(fi.ref)), fi.ref, fi.oref)
          lcdiploma = ALLTRIM(fi.u_diploma)


          **compart bonus
          uf_atendimento_devolveDadosBonusCompart()
		  LOCAL lcIdExt, lcTipoBonusId 
		  STORE "" TO lcTipoBonusId, lcIdExt
		  SELECT ucrsFiBonusTemp
		  lcTipoBonusId = ALLTRIM(ucrsFiBonusTemp.bonusTipo)
		  lcIdExt = ALLTRIM(ucrsFiBonusTemp.idBonus)             
		  fecha("ucrsFiBonusTemp")
          
           
          IF lccod<>'DO'
             uf_receituario_makeprecos(lccod, lcref, lcref, "FI", lcdiploma, .F., .F., .F.,lcTipoBonusId ,lcIdExt)
          ELSE
             IF  .NOT. EMPTY(lcdiploma)
                uf_receituario_makeprecos(lccod, lcref, lcref, "FI", lcdiploma,.F., .F., .F.,lcTipoBonusId ,lcIdExt)
             ENDIF
          ENDIF
          uf_atendimento_fiiliq(.T.)
          SELECT fi
          SELECT ucrsatendst
          LOCATE FOR ALLTRIM(ucrsatendst.ststamp)==ALLTRIM(fi.fistamp)
          IF FOUND()
             DO CASE
                CASE ucrsatendst.grphmgcode=='' .OR. ucrsatendst.grphmgcode=='GH0000'
                   REPLACE fi.opcao WITH 'I'
                CASE fi.u_epvp>fi.pvpmaxre
                   REPLACE fi.opcao WITH 'S'
                OTHERWISE
                   REPLACE fi.opcao WITH 'N'
             ENDCASE
          ENDIF
          SELECT fi
          IF fi.epv<>0
             IF fi.u_epvp=0
                REPLACE fi.u_epvp WITH fi.epv
             ENDIF
             IF fi.qtt>0 
                txcomp = ((fi.etiliquido+fi.u_descval+(fi.epv*fi.qtt*(fi.desconto/100)))*100)/(fi.u_epvp*fi.qtt)
                REPLACE fi.u_txcomp WITH ROUND(txcomp, 2)
             ENDIF
          ELSE
             REPLACE fi.u_txcomp WITH 0
          ENDIF
       ELSE
          SELECT ucrsatendst
          LOCATE FOR ucrsatendst.ststamp=fi.fistamp
          SELECT fi
          IF  .NOT. fi.amostra
             SELECT ucrsatendst
             IF lcpercadicseg=0
                REPLACE fi.epv WITH ucrsatendst.epv1, fi.pvmoeda WITH ucrsatendst.epv1, fi.u_epvp WITH ucrsatendst.epv1
             ELSE
                REPLACE fi.epv WITH ROUND(ucrsatendst.epv1*(1+(lcpercadicseg)), 2), fi.pvmoeda WITH ROUND(ucrsatendst.epv1*(1+(lcpercadicseg)), 2), fi.u_epvp WITH ROUND(ucrsatendst.epv1*(1+(lcpercadicseg)), 2)
             ENDIF
          ELSE
             REPLACE fi.epv WITH fi.u_epvp, fi.pvmoeda WITH fi.u_epvp, fi.pv WITH fi.u_epvp
          ENDIF
          
          REPLACE fi.u_ettent1 WITH 0, fi.u_ettent2 WITH 0, fi.u_txcomp WITH 100
          uf_atendimento_fiiliq(.T.)
       ENDIF
    ENDIF
 ELSE
    IF !WEXIST("IMPORTARFACT") &&AND !WEXIST("FACTURACAO")
      SELECT fi
      SELECT ucrsatendst
      LOCATE FOR ucrsatendst.ststamp=fi.fistamp
      SELECT fi
      IF  !fi.amostra .AND. TYPE("FACTURACAO")=="U"
         IF lcpercadicseg=0
               REPLACE fi.epv WITH ucrsatendst.epv1, fi.pvmoeda WITH ucrsatendst.epv1, fi.u_epvp WITH ucrsatendst.epv1
         ELSE
               REPLACE fi.epv WITH ROUND(ucrsatendst.epv1*(1+(lcpercadicseg)), 2), fi.pvmoeda WITH ROUND(ucrsatendst.epv1*(1+(lcpercadicseg)), 2), fi.u_epvp WITH ROUND(ucrsatendst.epv1*(1+(lcpercadicseg)), 2)
         ENDIF
      ELSE

         IF !(TYPE("fi.forceCalcCompart") <> "U" AND fi.forceCalcCompart)

            REPLACE fi.epv WITH fi.u_epvp, fi.pvmoeda WITH fi.u_epvp, fi.pv WITH fi.u_epvp

         ENDIF

      ENDIF

      IF !(TYPE("fi.forceCalcCompart") <> "U" AND fi.forceCalcCompart)

         REPLACE fi.u_ettent1 WITH 0, fi.u_ettent2 WITH 0 
         REPLACE fi.u_txcomp WITH 100

      ENDIF

    ENDIF
		
   uf_atendimento_fiiliq(.T.)
 ENDIF

 SELECT fi
 uf_facturacao_alertapic(fi.fistamp)
ENDFUNC 
 
FUNCTION uf_atendimento_validaBasComComplementar

	fecha("fiAuxTemp")
	LOCAL lcResult
	lcResult = .f.

	if(USED("fi") and USED("fi2"))
		SELECT fi
		GO TOP
 		SELECT fi.u_ettent2, fi.u_ettent1, fi.id_Dispensa_Eletronica_D, fi2.bonusId, fi.ref, fi.id_validacaoDem, fi.token from fi left JOIN fi2 ON fi.fistamp= fi2.fistamp INTO CURSOR fiAuxTemp
 	

 		SELECT fiAuxTemp
 		GO TOP
 		SCAN	
 			** 
 			LOCAL lnValorSnsComComplementar, lcIdDem, lcBonusId, lcToken, lcValidaDem
 			STORE 0 TO lnValorSnsComComplementar
 			STORE '' TO lcIdDem, lcBonusId, lcToken, lcValidaDem
 			
 			lcValorSnsComComplementar = fiAuxTemp.u_ettent2
 			lcIdDem                   = fiAuxTemp.id_Dispensa_Eletronica_D
 			lcBonusId                 = fiAuxTemp.bonusId 
 			lcToken                   = fiAuxTemp.token
 			lcValidaDem               = fiAuxTemp.id_validacaoDem
 			   
 			
 			if(EMPTY(lcValorSnsComComplementar ))
 				lcValorSnsComComplementar  = 0
 			ENDIF
 			
 			if(EMPTY(lcIdDem))
 				lcIdDem = ""
 			ENDIF
 			
 				
 			if(EMPTY(lcToken))
 				lcToken= ""
 			ENDIF
 			
 				
 			if(EMPTY(lcValidaDem))
 				lcValidaDem = ""
 			ENDIF
 			
 			
 			if(EMPTY(lcBonusId))
 				lcBonusId = ""
 			ENDIF
 						
 			** Se o SNS est� na entidade 2 quer dizer que passou a complementar
 			** se � Dem, tem linha de dem ou token de dem
 			** se tem bonus 99
 			IF lcValorSnsComComplementar > 0 and (!EMPTY(lcIdDem) OR !EMPTY(lcToken) OR !EMPTY(lcValidaDem))  and uf_gerais_compStr(lcBonusId,"99")
 				lcResult = .t.
 			ENDIF
 		ENDSCAN
 		
 	ENDIF
 	
 	
 	fecha("fiAuxTemp")
 	

	**RETURN lcResult 
	RETURN  .f.
 	
ENDFUNC
 
 
 

**
FUNCTION uf_atendimento_finalizarvenda
 LOCAL lcdemanulou, lctotprod, lctotserv, lctpempr
 lctotprod = 0
 lctotserv = 0
 lctpempr = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		select top 1 tipoempresa from empresa (nolock)
 ENDTEXT

 uf_gerais_actgrelha("", "uCrstpempr", lcsql)
 lctpempr = ALLTRIM(ucrstpempr.tipoempresa)
 lcmoedadefault = uf_gerais_getparameter("ADM0000000260", "text")
 IF EMPTY(lcmoedadefault)
    IF ALLTRIM(ft.ncont)=uf_gerais_getparameter_site("ADM0000000075", "text", mysite) .OR. EMPTY(ft.ncont)
       SELECT fi
       GOTO TOP
       SCAN
          IF  .NOT. EMPTY(fi.ref) .AND. fi.stns=.T.
             lctotserv = lctotserv+fi.etiliquido
          ENDIF
          IF  .NOT. EMPTY(fi.ref) .AND. fi.stns=.F.
             lctotprod = lctotprod+fi.etiliquido
          ENDIF
       ENDSCAN
       IF lctpempr='FARMACIA' .OR. lctpempr='PARAFARMACIA'
          IF lctotserv>100
             uf_perguntalt_chama("N�o pode efetuar documentos com valor de servi�os superior a 100� sem preencher o NIF do cliente. Por favor corrija.", "OK", "", 16)
             RETURN .F.
          ENDIF
          IF lctotprod>1000
             uf_perguntalt_chama("N�o pode efetuar documentos com valor de artigos superior a 1000� sem preencher o NIF do cliente. Por favor corrija.", "OK", "", 16)
             RETURN .F.
          ENDIF
       ELSE
          IF lctotserv+lctotprod>100
             uf_perguntalt_chama("N�o pode efetuar documentos com valor de superior a 100� sem preencher o NIF do cliente. Por favor corrija.", "OK", "", 16)
             RETURN .F.
          ENDIF
       ENDIF
    ENDIF
 ENDIF

 SELECT * FROM FI WITH(BUFFERING = .T.) WHERE LEFT(fi.design, 1) = "." ORDER BY lordem ASC INTO CURSOR uc_cabFItmp
 SELECT * FROM FI WITH(BUFFERING = .T.) ORDER BY lordem ASC INTO CURSOR uc_fiTmp

 SELECT uc_cabFItmp
 GO TOP
 SCAN

	SELECT UCRSATENDCOMP
	LOCATE FOR ALLTRIM(UCRSATENDCOMP.fistamp) = ALLTRIM(uc_cabFItmp.fistamp)

	IF FOUND()

		uv_plano = ALLTRIM(UCRSATENDCOMP.codigo)

		SELECT uc_fiTmp
		GO TOP
		SCAN FOR uc_fiTmp.lordem > uc_cabFItmp.lordem

			IF LEFT(uc_fiTmp.design, 1) = "."
				EXIT
			ENDIF

			IF !EMPTY(uv_plano) AND !uf_faturacao_checkLenEmb(ALLTRIM(uv_plano), ALLTRIM(uc_fiTmp.U_CODEMB))
				RETURN .F.
			ENDIF

			SELECT uc_fiTmp
		ENDSCAN

	ENDIF

	SELECT uc_cabFItmp
 ENDSCAN

 FECHA("uc_fiTmp")
 FECHA("uc_cabFItmp")

 IF uf_gerais_getParameter_site("ADM0000000203", "BOOL", mySite)

   SELECT * FROM fi WITH (BUFFERING = .T.) WHERE LEFT(fi.design,1) == '.' INTO CURSOR uc_tmpCabs


   SELECT uc_tmpCabs
   GO TOP

   SCAN

      IF EMPTY(STREXTRACT(uc_tmpCabs.design, '[', ']', 1, 0))
         FECHA("uc_tmpCabs")
         uf_perguntalt_chama("O Nr� de Receita � de preenchimento obrigat�rio.", "OK", "", 48)
         RETURN .F.
      ENDIF

   ENDSCAN

   FECHA("uc_tmpCabs")

 ENDIF
 
 IF uf_gerais_getParameter_site("ADM0000000203", "BOOL", mySite)

   SELECT * FROM ucrsatendcomp WITH (BUFFERING = .T.) INTO CURSOR uc_tmpComp

   SELECT uc_tmpComp
   GO TOP
   SCAN

      IF !uf_atendimento_obrigaNrReceita(uc_tmpComp.fistamp)
         RETURN .F.
      ENDIF

      SELECT uc_tmpComp
   ENDSCAN

   FECHA("uc_tmpComp")

 ENDIF

 IF uf_gerais_getParameter_site("ADM0000000203", "BOOL", mySite)

   SELECT * FROM ucrsatendcomp WITH (BUFFERING = .T.) INTO CURSOR uc_tmpComp

   SELECT uc_tmpComp
   GO TOP

   SCAN
      IF !uf_atendimento_obrigaNrUtente(uc_tmpComp.fistamp)
         RETURN .F.
      ENDIF

      SELECT uc_tmpComp
   ENDSCAN

 ENDIF
 
 IF uf_atendimento_validareservaMcdt()
    uf_perguntalt_chama("N�o pode efetuar reservas com mcdts. Por favor valide.", "OK", "", 16)
    RETURN .F.
 ENDIF

 LOCAL lcValidaLoteVet
 STORE '' TO lcValidaLoteVet

 lcValidaLoteVet = uf_atendimento_validaLotesVet()
 
 IF !EMPTY(lcValidaLoteVet)
    uf_perguntalt_chama("O produto " + ALLTRIM(lcValidaLoteVet) + " necessita ter os campos Lote e Validade preenchidos. Por favor valide.", "OK", "", 16)
    RETURN .F.
 ENDIF
 
 
 
 IF !uf_atendimento_validareservaPlano()
    uf_perguntalt_chama("N�o pode efetuar reservas com o plano aplicado. Por favor valide.", "OK", "", 16)
    RETURN .F.
 ENDIF


 if uf_atendimento_validaReservaComVale()
   uf_perguntalt_chama("N�O PODE FAZER RESERVAS QUANDO J� EXISTEM VALES NO ATENDIMENTO.", "OK", "", 64)
    RETURN .F.
 ENDIF
 
 if uf_atendimento_validareserva_planors()
     uf_perguntalt_chama("N�o pode efetuar reservas com o plano RS. Por favor valide.", "OK", "", 16)
    RETURN .F.
 ENDIF
 
  if uf_atendimento_validaTipoDispensasReceitasDiferentes()
     uf_perguntalt_chama("As prescri��es hospitalares (PEM-H) n�o podem ser dispensadas juntamente com outro tipo de receitas. Por favor valide.", "OK", "", 16)
    RETURN .F.
 ENDIF

 uf_atendimento_validaSeEpisodioPEHMFacturado("7532622")
  
 
 
*!*	 if uf_atendimento_validaReserva_planoBonus()
*!*	    uf_perguntalt_chama("N�o pode efetuar reservas com o esta comparticipa��o adicional . Por favor valide.", "OK", "", 16)
*!*		    RETURN .F.
*!*	 ENDIF

 if(uf_atendimento_validaSuspensaEfSNSExt())
 	uf_perguntalt_chama("N�o pode efetuar vendas suspensas com comparticipa��es externas. Por favor valide.", "OK", "", 16)
    RETURN .F.
 ENDIF
 
 IF !uf_atendimento_validaSuspensaPlano()
    uf_perguntalt_chama("N�o pode efetuar vendas suspensas com o plano aplicado. Por favor valide.", "OK", "", 16)
    RETURN .F.
 ENDIF

 
if(uf_atendimento_validaSuspensaMcdt())
 	uf_perguntalt_chama("N�o pode efetuar vendas suspensas com mcdts. Por favor valide.", "OK", "", 16)
    RETURN .F.
 ENDIF
 
 if uf_atendimento_validaSuspensa_planoBonus()
     uf_perguntalt_chama("N�o pode efetuar vendas suspensas com  esta comparticipa��o adicional . Por favor valide.", "OK", "", 16)
    RETURN .F.
 ENDIF
 
 
 if uf_atendimento_validaBasComComplementar()
    uf_perguntalt_chama("N�o pode efetuar vendas com a Portaria n.� 66/2023, de 6 de Mar�o. (BAS) e complementariadade", "OK", "", 16)
    RETURN .F.
 ENDIF

  
 IF uf_atendimento_validaMultiVendaMcdt()
    uf_perguntalt_chama("N�o pode efetuar atendimentos multivenda com mcdts. Por favor valide.", "OK", "", 16)
    RETURN .F.
 ENDIF
 
 
 **IF uf_gerais_validaSeVendaMultiplaMCDTeAlerta()
  **  RETURN .F.
 **ENDIF
 
 
 IF EMPTY(mymultivenda) AND  uf_atendimento_devolveNumeroVendasAtendimento()>1 
    uf_perguntalt_chama("N�o pode efetuar atendimentos multivenda. Por favor valide.", "OK", "", 16)
    RETURN .F.
 ENDIF
 
 

  
 IF EMPTY(mymultivenda) AND uf_atendimento_devolveStampsDocumentosDiferentes()
    uf_perguntalt_chama("N�o pode efetuar atendimentos multivenda. Por favor valide.", "OK", "", 16)
    RETURN .F.
 ENDIF

 IF EMPTY(mymultivenda) AND !uf_atendimento_checkDevMulti()
    uf_perguntalt_chama("N�o pode efetuar uma venda e uma devolu��o em simult�neo. Por favor valide.", "OK", "", 16)
    RETURN .F.
 ENDIF
 
 if !uf_gerais_getparameter("ADM0000000355", "bool")
	uf_atendimento_normalizaLinhasMcdt()
 endif	
 
 uf_atendimento_valida_obito()
 &&covid comentario fim	

 fecha("ucrsFI")
 fecha("ucrsFT")
 fecha("ucrsFT2")
 IF  .NOT. uf_atendimento_valida_artigo_sem_stock()
    RETURN .F.
 ENDIF
 SELECT fi_trans_info
 GOTO TOP

 IF UPPER(ALLTRIM(ucrse1.pais))=='PORTUGAL'
    DELETE FROM fi_trans_info WHERE EMPTY(fi_trans_info.batchid)
 ELSE
    DELETE FROM fi_trans_info WHERE 1=1
 ENDIF

 IF  .NOT. uf_valida_info_caixas_verificacao()
    RETURN .F.
 ELSE
 	** Valida informa��o obrigat�ria medicamentos contrafracionados
	IF !uf_atendimento_valida_estado_datamatrix()   
      RETURN .F.
   ENDIF
 ENDIF
 IF  .NOT. uf_gerais_actgrelha("", "", "SELECT dbo.up_GerarHash('')")
    uf_perguntalt_chama("FUN��O DE GEST�O DE ASSINATURAS DIGITAIS N�O DISPON�VEL, POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
    RETURN .F.
 ENDIF
 
 ** Valida informa��o obrigat�ria medicamentos controlados
 IF uf_gerais_getParameter_site('ADM0000000152', 'BOOL', mySite)
 	IF !uf_atendimento_valida_artigos_controlados()
 		RETURN .f.
 	ENDIF 
 ENDIF 
  
 lcsql = ''
 SELECT ucrse1
 GOTO TOP
 IF ucrse1.gestao_cx_operador=.T.
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_caixa_verificaCaixaAberta '<<ALLTRIM(mysite)>>', '<<ALLTRIM(ch_vendnm)>>', 1
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "uCrsCxAberta", lcsql)
       uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es de Caixa", "OK", "", 16)
       RETURN .F.
    ELSE
       IF  .NOT. RECCOUNT("uCrsCxAberta")>0
          uf_perguntalt_chama("A SUA SESS�O DE CAIXA FOI FECHADA. N�O PODE CONTINUAR SEM PRIMEIRO ABRIR NOVA SESS�O.", "OK", "", 48)
          fecha("uCrsCxAberta")
          RETURN .F.
       ENDIF
       fecha("uCrsCxAberta")
    ENDIF
 ELSE
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_caixa_verificaCaixaAberta '<<ALLTRIM(mysite)>>', '<<ALLTRIM(myTerm)>>', 0
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "uCrsCxAberta", lcsql)
       uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es de Caixa", "OK", "", 16)
       RETURN .F.
    ELSE
       IF  .NOT. RECCOUNT("uCrsCxAberta")>0
          uf_perguntalt_chama("A SUA SESS�O DE CAIXA FOI FECHADA. N�O PODE CONTINUAR SEM PRIMEIRO ABRIR NOVA SESS�O.", "OK", "", 48)
          fecha("uCrsCxAberta")
          RETURN .F.
       ENDIF
       fecha("uCrsCxAberta")
    ENDIF
 ENDIF
 


 SELECT ucrse1
 IF LEN(ALLTRIM(ucrse1.motivo_isencao_iva))=0
 	uf_perguntalt_chama("N�O EXISTE MOTIVO DE ISEN��O DE IVA POR DEFEITO PREENCHIDO NA FICHA DA EMPRESA. POR FAVOR ATUALIZE PARA PODER FINALIZAR O ATENDIMENTO", "OK", "", 48)
    RETURN .F.
 ENDIF 
 
*!*	 IF ( .NOT. EMPTY(ALLTRIM(ft.bino)))
*!*	   IF !uf_gerais_validaPattern("^[0-9]*$", alltrim(ft.bino)) AND UPPER(ALLTRIM(ucrse1.pais))=='PORTUGAL'

*!*	      uf_perguntalt_chama("N�mero de doc ID. N�o pode conter letras nem caracteres especiais."+CHR(13)+"Por favor atualize a ficha do utente e volte a colocar o mesmo.", "OK", "", 48)
*!*	      RETURN .F.

*!*	   ENDIF
*!*	 ENDIF
 IF UPPER(mypaisconfsoftw)=='ANGOLA'
    IF ft.no=200 .AND.  .NOT. EMPTY(ucrsatendcomp.codigo); 
         AND IIF(uf_gerais_getParameter_site("ADM0000000167","BOOL", mySite) AND ALLTRIM(UPPER(uf_gerais_getParameter_site("ADM0000000167","TEXT", mySite))) == ALLTRIM(UPPER(ucrsatendcomp.codigo)), .F., .T.)
       uf_perguntalt_chama("Tem um cliente sem ficha com um plano de comparticipa��o na venda"+CHR(13)+"Por favor remova o mesmo!", "OK", "", 48)
       RETURN .F.
    ENDIF
    SELECT ucrsatendcomp
    LOCAL lcpos, lcval, lcdevolucao
    STORE .F. TO lcval, lcdevolucao
    STORE 0 TO lcpos
    SELECT fi
    lcpos = RECNO("fi")
    SELECT fi
    GOTO TOP
    SCAN
       IF  .NOT. EMPTY(fi.ref)
          IF fi.partes=1
             lcdevolucao = .T.
          ENDIF
       ENDIF
    ENDSCAN
    SELECT fi
    TRY
       GOTO lcpos
    CATCH
       GOTO TOP
    ENDTRY
   IF lcdevolucao=.F. .AND. ucrsatendcomp.e_compart=.F.
      SELECT ucrsatendcomp 
      GO TOP
      SCAN FOR !EMPTY(ucrsatendcomp.codigo) AND !uf_gerais_compStr(ucrsatendcomp.codigo, uf_gerais_getParameter_site("ADM0000000167", "TEXT", mySite))
	      LOCAL lcvalidtemcompart
	      lcvalidtemcompart = .F.
	    
	      SELECT fimancompart
	      GOTO TOP
	      SCAN
	         	  
	         IF fimancompart.valor>0 .OR. fimancompart.perc>0
	            lcvalidtemcompart = .T.
	         ENDIF
	      ENDSCAN
	      IF lcvalidtemcompart=.F. AND !ucrsatendcomp.permiteVendaSemCompart
	         uf_perguntalt_chama("Tem um plano de comparticipa��o selecionado mas nenhuma comparticipa��o preenchida."+CHR(13)+"Por favor corrija!", "OK", "", 48)
	         RETURN .F.
	      ENDIF

         SELECT ucrsatendcomp
	    ENDSCAN  
   ENDIF
 ENDIF
 SELECT ft
 IF (ft.no==200) .AND. ((myvendeclgenerico==.T.)) .OR. EMPTY(ALLTRIM(ft.nome))
    uf_perguntalt_chama("O SISTEMA APENAS PERMITE VENDER A CLIENTES COM FICHA. DEVE ESCOLHER UM CLIENTE ANTES DE PODER CONTINUAR.", "OK", "", 64)
    RETURN .F.
 ENDIF
 SELECT ft
 IF  .NOT. (ft.no==200) .AND. (ucrsatendcl.nocredit==.T.)
    uf_perguntalt_chama("O CLIENTE SELECCIONADO TEM A FACTURA��O CANCELADA. POR FAVOR VERIFIQUE.", "OK", "", 64)
    RETURN .F.
 ENDIF
 SELECT fi
 GOTO TOP
 IF  .NOT. (LEFT(fi.design, 1)==".")
    uf_perguntalt_chama("PARECE HAVER UM PROBLEMA COM O CABE�ALHO DA PRIMEIRA VENDA. POR FAVOR REINICIE O SOFTWARE E TENTE NOVAMENTE.", "OK", "", 48)
    RETURN .F.
 ENDIF
 SELECT fi
 SELECT * FROM fi WHERE  NOT DELETED() AND LEFT(fi.design, 1)<>"." INTO CURSOR countLinhas
 IF _TALLY==0
    uf_perguntalt_chama("N�O PODE GRAVAR UMA VENDA SEM PRODUTOS.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF USED("countLinhas")
    fecha("countLinhas")
 ENDIF
 IF myfechomeianoite OR myfechomeianoiteUser
    IF  .NOT. uf_caixas_validafechomeianoite()
       uf_perguntalt_chama("EXPIROU O TEMPO PERMITIDO PARA FECHAR A CAIXA DEPOIS DA MEIA NOITE. DEVE PROCEDER AO FECHO DE CAIXA ANTES DE PODER CONTINUAR.", "OK", "", 64)
       RETURN .F.
    ENDIF
 ENDIF
 lcmoedadefault = uf_gerais_getparameter("ADM0000000260", "text")
 IF EMPTY(lcmoedadefault)
    IF atendimento.txttotalutente.value>=1000
       SELECT ucrsatendcl
       IF EMPTY(ALLTRIM(ucrsatendcl.ncont)) .OR. ALLTRIM(ucrsatendcl.ncont)=uf_gerais_getparameter_site("ADM0000000075", "text", mysite) .OR. EMPTY(ALLTRIM(ucrsatendcl.morada)) .OR. EMPTY(ALLTRIM(ucrsatendcl.nome))
          uf_perguntalt_chama("Em atendimentos superiores a 1000� � obrigat�rio indicar o NIF, Nome e Morada do Utente.", "OK", "", 48)
          RETURN .F.
       ENDIF
    ENDIF
 ENDIF
 IF EMPTY(uf_atendimento_vendaslinhasdem())
    RETURN .F.
 ENDIF
 IF uf_gerais_getparameter_site("ADM0000000002", "BOOL", mysite)=.T.
    LOCAL lcverifica, lcartigos
    STORE .F. TO lcverifica
    STORE '' TO lcartigos
    IF !uf_gerais_getParameter_site('ADM0000000125', 'BOOL', mySite)
	    SELECT fi
	    GOTO TOP
	    SCAN
	       IF fi.qtt>0 .AND. fi.qtt>fi.qtdem .AND. fi.stns=.F. .AND.  .NOT. EMPTY(fi.ref) .AND. EMPTY(fi.ofistamp) .AND. EMPTY(fi.bistamp)
	          lcverifica = .T.
	          lcartigos = lcartigos+ALLTRIM(fi.design)+CHR(13)
	       ENDIF
	    ENDSCAN
	ELSE
		SELECT fi
	    GOTO TOP
	    SCAN
	       IF fi.qtt>0 .AND. fi.qtt>fi.qtdem .AND. fi.stns=.F. .AND.  .NOT. EMPTY(fi.ref) .AND. (EMPTY(fi.ofistamp) OR (!EMPTY(fi.ofistamp) AND fi.partes=1)) .AND. EMPTY(fi.bistamp)
	          lcverifica = .T.
	          lcartigos = lcartigos+ALLTRIM(fi.design)+CHR(13)
	       ENDIF
	    ENDSCAN
	ENDIF 
    IF lcverifica=.T.
       uf_perguntalt_chama("AINDA H� ARTIGOS POR CONFERIR. POR FAVOR CONFIRA TODOS OS ARTIGOS ANTES DE TERMINAR O ATENDIMENTO!"+CHR(13)+"OBRIGADO.", "OK", "", 48)
       RETURN .F.
    ENDIF
 ENDIF
 SELECT fi
 GOTO TOP
 IF  .NOT. EMPTY(ucrse1.obrigaconf)
    SELECT fi
    GOTO TOP
    SCAN
       IF UPPER(ALLTRIM(fi.tipor))=='RSP'
          lordemauxobriga = LEFT(astr(fi.lordem), 2)
          lcposauxobriga = RECNO("fi")
          SCAN FOR EMPTY(fi.ofistamp) .AND. fi.qtt>fi.qtdem .AND. LEFT(astr(fi.lordem), 2)=lordemauxobriga .AND.  .NOT. EMPTY(fi.ref) .AND. EMPTY(fi.epromo)
             uf_perguntalt_chama("Existem produtos associados a uma RSP que ainda n�o foram conferidos. Por favor verifique.", "OK", "", 48)
             RETURN .F.
          ENDSCAN
          SELECT fi
          TRY
             GOTO lcposauxobriga
          CATCH
             GOTO TOP
          ENDTRY
       ENDIF
       SELECT fi
    ENDSCAN
 ENDIF
 SELECT ucrsatendcl
 IF ucrsatendcl.no<>200
    SELECT ucrsatendcomp
    IF RECCOUNT("uCrsAtendComp")==1 .AND.  .NOT. EMPTY(ALLTRIM(ucrsatendcomp.codigo))
       SELECT ucrsatendcomp
       GOTO TOP
       SELECT fi
       GOTO TOP
       LOCAL lcvalplanovazio, lcvalperguntaplano
       STORE .F. TO lcvalplanovazio, lcvalperguntaplano
       IF EMPTY(ALLTRIM(ucrsatendcl.entpla))
          lcvalplanovazio = .T.
       ELSE
          IF ALLTRIM(ucrsatendcomp.codigo)<>ALLTRIM(ucrsatendcl.codpla) .AND. uf_perguntalt_chama("O plano escolhido no atendimento � diferente do plano da ficha do Utente."+CHR(13)+"Pretende actualizar a ficha do Utente com a informa��o deste atendimento?", "Sim", "N�o")
             lcvalperguntaplano = .T.
          ENDIF
       ENDIF
       IF lcvalplanovazio .OR. lcvalperguntaplano
          TEXT TO lcsql TEXTMERGE NOSHOW
					UPDATE 
						b_utentes
					SET 
					  	 codpla = '<<ALLTRIM(uCrsAtendComp.codigo)>>'
						,desPLA = '<<ALLTRIM(uCrsAtendComp.design)>>' 
						,entpla = '<<ALLTRIM(uCrsAtendComp.cptorgabrev)>>'
						,nbenef = '<<IIF(!empty(ALLTRIM(fi.u_nbenef)),LEFT(ALLTRIM(fi.u_nbenef),18),LEFT(uCrsAtendCl.nbenef,18))>>'
						,nbenef2 = '<<IIF(!empty(ALLTRIM(fi.u_nbenef2)),LEFT(ALLTRIM(fi.u_nbenef2),18),LEFT(uCrsAtendCl.nbenef2,18))>>'
					where 
						utstamp = '<<ALLTRIM(uCrsAtendCl.utstamp)>>'
          ENDTEXT
          uf_gerais_actgrelha("", "", lcsql)
       ENDIF
    ENDIF
 ENDIF
 SELECT ucrsatendcomp
 IF ucrsatendcomp.temsimulacao=.F. .AND. ucrsatendcomp.e_compart=.T. .AND. atendimento.pgfdados.page3.btn12.label1.caption='VALID. COMPART.' .AND. (atendimento.lbldev.caption='Devolu��o' .OR. atendimento.lbldev.caption='Dev. Prod.')
    uf_perguntalt_chama("Escolheu um plano com comparticipa��o automatica e ainda n�o efetuou a mesma. Por favor verifique!", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF totftcompart>0 .AND. totftcompart<>atendimento.txttotalutente.value
    uf_perguntalt_chama("Os valores foram alterados depois da comparticipa��o ter sido efetuada. Por favor anule a comparticipa��o e volte a afetu�-la.", "OK", "", 48)
    RETURN .F.
 ENDIF
 LOCAL lcvalrefexiste, lcposologia
 STORE .F. TO lcvalrefexiste
 STORE '' TO lcposologia
 SELECT ucrsatendst
 GOTO TOP
 SCAN
    lcvalrefexiste = .F.
    lcposologia = ALLTRIM(ucrsatendst.u_posprog)
    SELECT fi
    GOTO TOP
    SCAN
       IF ALLTRIM(ucrsatendst.ststamp)==ALLTRIM(fi.fistamp)
          REPLACE fi.posologia WITH lcposologia
          lcvalrefexiste = .T.
       ENDIF
    ENDSCAN
    IF  .NOT. lcvalrefexiste
       SELECT ucrsatendst
       DELETE
    ENDIF
 ENDSCAN
 SELECT ucrsatendst
 GOTO TOP
 LOCAL lccountx
 lccountx = 0
 IF ((myusarobot .OR. myusarobotapostore) .AND. myrobotstate) .OR. myusafarmax .OR. (myusarobotconsis .AND. myrobotstate)
    IF atendimento.tmrrobot.enabled
       DO WHILE (atendimento.tmrrobot.enabled) .AND. lccountx<10
          lccountx = lccountx+1
          WAIT WINDOW "DISPENSA DO ROBOT EM CURSO, POR FAVOR AGUARDE..." TIMEOUT 2
       ENDDO
    ENDIF
 ENDIF
 LOCAL lctotalprodneg
 STORE 0 TO lctotalprodneg
 IF mypvpnegavativo
    SELECT fi
    CALCULATE SUM(fi.etiliquido) TO lctotalprodneg FOR fi.etiliquido<0 .AND.  .NOT. fi.epromo
    IF lctotalprodneg<0
       uf_perguntalt_chama("N�O � PERMITIDO A VENDA DE PRODUTOS COM PRE�O NEGATIVO. APENAS OS PRODUTOS MARCADOS COMO VALE NA FICHA DO PRODUTO PODER�O SER VENDIDOS A NEGATIVO.", "OK", "", 48)
       uf_eventoactvalores()
       RETURN .F.
    ENDIF
 ENDIF
 PUBLIC mypublicintvar
 LOCAL lcref, lccod, lcnb, lcnb2, lcnr, lcsusp, lccntlin, lcnrvendas, lcabort, lcnv, lccntpsi, lccount, lcvalidaval, lcenforcadatarec, lcqttsref
 STORE "" TO lcsql, lcref, lccod, lcnb, lcnb2, lcnr
 STORE 0 TO lccntlin, lcnrvendas, lcnv, lccntpsi, lccount
 STORE .F. TO lcabort, lcsusp, lcvalidaval, lcenforcadatarec, lcqttsref
 IF USED("uCrsCabVendas")
    fecha("uCrsCabVendas")
 ENDIF
 SELECT design, .F. AS sel, .F. AS sel2, .F. AS receita, .F. AS modopag, etiliquido, etiliquido AS etreg, etiliquido AS valorpagar, u_nbenef, u_nbenef2, lordem, .F. AS saved, fistamp AS stamp, ofistamp, tkhposlstamp AS u_ltstamp, partes, .F. AS fact, .F. AS factdev, .F. AS factpago, rvpstamp AS entpresc, szzstamp AS nopresc, rdata AS datareceita, ref AS codacesso, ref AS coddiropcao, CAST('' AS C(40) ) AS token, CAST('' AS C(35) ) AS cartao, .F. AS temadiantamento, 0.00  AS eacerto, .F. AS isencaoiva, CAST('' AS C(4) ) AS receita_tipo, .F. AS planocomplem, .F. AS mantemreceita, rdata AS datadispensa, bostamp, .F. AS nifgenerico, SPACE(200) as obsCl, SPACE(200) as obsInt , .F. as oferta FROM fi WHERE 1=0 INTO CURSOR uCrsCabVendas READWRITE
 SELECT ucrsatendcomp
 GOTO TOP
 SCAN
    IF EMPTY(ucrsatendcomp.codacesso)
       REPLACE ucrsatendcomp.codacesso WITH ALLTRIM(STR(RAND()*1000000))
    ENDIF
 ENDSCAN
 SELECT fi
 GOTO TOP
 SCAN
    IF LEFT(fi.design, 1)=="."
       LOCAL lcencpaga, lcmodopagcab
       lcencpaga = .F.
       lcmodopagcab = .F.
       SELECT fi
       IF  .NOT. EMPTY(fi.bostamp)
          TEXT TO lcsql TEXTMERGE NOSHOW
					select nrReceita,momento_pagamento, pagamento from bo2 (nolock) where bo2stamp='<<ALLTRIM(fi.bostamp)>>'
          ENDTEXT
          IF  .NOT. uf_gerais_actgrelha("", "uCrsPagamEncom", lcsql)
             uf_perguntalt_chama("OCORREU UM ERRO A VALIDAR ACESSOS. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
             RETURN .F.
          ENDIF
          SELECT ucrspagamencom
          IF EMPTY(ucrspagamencom.nrreceita) .AND. UPPER(ALLTRIM(ucrspagamencom.momento_pagamento))<>'ANTECIPADO'
             lcencpaga = .T.
          ENDIF
          fecha("uCrsPagamEncom")
       ENDIF
       IF lcencpaga=.T.
          lcmodopagcab = .T.
       ENDIF
       lccount = lccount+1
       IF lccount>1
          IF lccntlin=0
             lcabort = .T.
          ELSE
             lccntlin = 0
          ENDIF
       ENDIF
       SELECT ucrsatendcomp
       LOCATE FOR ALLTRIM(ucrsatendcomp.fistamp)==ALLTRIM(fi.fistamp)
       

       &&cria uCrsCabVendas 
       INSERT INTO uCrsCabVendas (design, sel, sel2, receita, modopag, etiliquido, etreg, valorpagar, u_nbenef, u_nbenef2, lordem, u_ltstamp, partes, fact, factdev, factpago, entpresc, nopresc, datareceita, codacesso, coddiropcao, token, cartao, temadiantamento, receita_tipo, planocomplem, datadispensa, bostamp, obsCl, obsInt, stamp, oferta) VALUES (fi.design, .F., .F., .F., lcmodopagcab, fi.etiliquido, fi.etiliquido, fi.etiliquido, fi.u_nbenef, fi.u_nbenef2, fi.lordem, fi.tkhposlstamp, fi.partes, .F., .F., .F., fi.rvpstamp, fi.szzstamp, ucrsatendcomp.datareceita, ALLTRIM(ucrsatendcomp.codacesso), ucrsatendcomp.coddiropcao, fi.token, ucrsatendcomp.ncartao, .F., fi.tipor, IIF( .NOT. EMPTY(fi.comp_tipo_2), .T., .F.), ucrsatendcomp.datadispensa, fi.bostamp, IIF(ALLTRIM(fi.obsCl) == "Observa��es para Cliente", "", ALLTRIM(fi.obsCl)), IIF(ALLTRIM(fi.obsInt) == "Observa��es Internas", "", ALLTRIM(fi.obsInt)), fi.fistamp, .F.)
      
       IF  .NOT. EMPTY(ucrscabvendas.bostamp)
          SELECT ucrscabvendas
          LOCAL lcposcab
          lcposcab = RECNO("uCrsCabVendas")
          LOCAL lcbostamppesqpag
          lcbostamppesqpag = ALLTRIM(ucrscabvendas.bostamp)
          TEXT TO lcsql TEXTMERGE NOSHOW
					select momento_pagamento, nrreceita from bo2 (nolock) where bo2stamp='<<ALLTRIM(lcBostampPesqPag )>>'
          ENDTEXT
          IF  .NOT. uf_gerais_actgrelha("", "uCrsPagamEncomAT", lcsql)
             uf_perguntalt_chama("OCORREU UM ERRO PESQUISAR O MODO DE PAGAMENTO DO CABE�ALHO DA ENCOMENDA. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
             RETURN .F.
          ENDIF
          SELECT ucrspagamencomat
          IF ALLTRIM(ucrspagamencomat.momento_pagamento)<>'Antecipado' .AND. EMPTY(ucrspagamencomat.nrreceita)
             SELECT ucrscabvendas
             TRY
                GOTO lcposcab
             CATCH
             ENDTRY
             SELECT ucrscabvendas
             REPLACE ucrscabvendas.modopag WITH .T.
          ENDIF
          SELECT ucrscabvendas
          TRY
             GOTO lcposcab
          CATCH
          ENDTRY
          fecha("uCrsPagamEncomAT")
       ENDIF
       SELECT ucrscabvendas
    ELSE
       IF  .NOT. EMPTY(fi.ref)
          IF  .NOT. fi.epromo
             lccntlin = lccntlin+1
          ENDIF
          IF fi.partes=0 .AND. fi.u_psico
             lccntpsi = lccntpsi+1
          ENDIF
          IF lcref==ALLTRIM(fi.u_refvale) .AND. UPPER(LEFT(fi.u_refvale, 1))=="V"
             lcvalidaval = .T.
          ENDIF
          IF UPPER(ALLTRIM(fi.ref))=="R000001"
             REPLACE ucrscabvendas.temadiantamento WITH .T. IN "uCrsCabVendas"
          ENDIF
          IF EMPTY(fi.ref) .AND. fi.qtt<>0
             TEXT TO lcsql TEXTMERGE NOSHOW
						select ref from st where design='<<alltrim(fi.design)>>' and site_nr=<<mysite_nr>>
             ENDTEXT
             IF  .NOT. uf_gerais_actgrelha("", "uCrsPesqRefDesign", lcsql)
                uf_perguntalt_chama("Ocorreu uma anomalia pesquisar a refer�ncia de uma linha sem a mesma. Contacte o suporte.", "OK", "", 16)
                RETURN .F.
             ENDIF
             IF RECCOUNT("uCrsPesqRefDesign")=0
                lcqttsref = .T.
             ELSE
                IF  .NOT. EMPTY(ucrspesqrefdesign.ref)
                   SELECT fi
                   REPLACE fi.ref WITH ALLTRIM(ucrspesqrefdesign.ref)
                ELSE
                   lcqttsref = .T.
                ENDIF
             ENDIF
             fecha("uCrsPesqRefDesign")
          ENDIF
          SELECT fi
       ENDIF
    ENDIF
    IF UPPER(LEFT(fi.u_refvale, 1))=="V"
       lcref = ALLTRIM(fi.u_refvale)
    ENDIF
 ENDSCAN
 IF lcqttsref==.T.
    uf_perguntalt_chama("EXISTE UMA OU MAIS LINHAS SEM REFER�NCIA PREENCHIDA. POR FAVOR VERIFIQUE.", "OK", "", 48)
    lcqttsref = .F.
    SELECT fi
    GOTO TOP
    RETURN .F.
 ENDIF
 IF lcabort==.T. .OR. lccntlin==0
    uf_perguntalt_chama("N�O PODE GRAVAR UMA VENDA SEM PRODUTOS.", "OK", "", 48)
    lcabort = .F.
    SELECT fi
    GOTO TOP
    RETURN .F.
 ENDIF
 IF lcvalidaval==.T.
    uf_perguntalt_chama("N�O PODE DUPLICAR O MESMO VALE DE DESCONTO NO MESMO ATENDIMENTO.", "OK", "", 48)
    lcabort = .F.
    SELECT fi
    GOTO TOP
    RETURN .F.
 ENDIF
 lcref = ""
 lccntlin = 0
 lcabort = .F.
 SELECT ucrscabvendas
 CALCULATE CNT() TO lcnrvendas 
 SELECT ucrscabvendas
 GOTO TOP
 SCAN
 	
 	LOCAL lcReceitaAux, lcPlanoReceita1
 	
    IF  .NOT. EMPTY(ucrscabvendas.token) .AND. !uf_gerais_checkNumReceita(STREXTRACT(ucrscabvendas.design, ':', ' -', 1, 0))&&LEN(STREXTRACT(ucrscabvendas.design, ':', ' -', 1, 0))<>19
		uf_gerais_msgErroReceita(.F.)
       **uf_perguntalt_chama("Para receitas dispensadas eletronicamente apenas s�o aceites receitas com n� de tamanho igual a 19 caracteres. Por favor verifique.", "OK", "", 32)
       RETURN .F.
    ENDIF
    
    lcReceitaAux= STREXTRACT(ucrscabvendas.design, ':', ' -', 1, 0)
    lcPlanoReceita1= STREXTRACT(ucrscabvendas.design, '[', ']', 1, 0)

    
    if(!uf_comunicasinave_validaNumeroReceitaSinave(ALLTRIM(lcReceitaAux),ALLTRIM(lcPlanoReceita1)))
       uf_perguntalt_chama("Estrutura de receita sinave inv�lida", "OK", "", 32)
       RETURN .F.
    ENDIF
    
    
    IF RIGHT(ALLTRIM(ucrscabvendas.design), 10)=="*SUSPENSA*" .OR. RIGHT(ALLTRIM(ucrscabvendas.design), 18)=="*SUSPENSA RESERVA*"
       lcsusp = .T.
    ELSE
       lcsusp = .F.
    ENDIF
    lctipodoc = 1
    IF (LEFT(ucrscabvendas.design, 1)==".") .AND. (LEFT(ucrscabvendas.design, 2)<>".S")
       lccod = STREXTRACT(ucrscabvendas.design, '[', ']', 1, 0)
       lcnr = STREXTRACT(ucrscabvendas.design, ':', ' -', 1, 0)
       lcnb = ucrscabvendas.u_nbenef
       lcnb2 = ucrscabvendas.u_nbenef2
    ENDIF
    lcnv = lcnv+1
    IF (uf_atendimento_validavenda(lcnv, lccod, lcnr, lcnb, lcnb2, lcsusp))=.F.
       SELECT fi
       GOTO TOP
       RETURN .F.
    ENDIF
    lccod = ""
    SELECT ucrscabvendas
 ENDSCAN
 uf_pagamento_preparacabvendas()
 lcdemanulou = .F.
 LOCAL ordem, validaordem, validavenda, validapsico, lcdempsicoaux
 STORE 0 TO ordem
 STORE .F. TO validaordem, validavenda, validapsico, lcdempsicoaux
 SELECT ucrscabvendas
 GOTO TOP
 SCAN
    SELECT fi
    SCAN FOR fi.design=ucrscabvendas.design .AND. fi.lordem=ucrscabvendas.lordem
       ordem = RECNO("fi")
    ENDSCAN
    SELECT fi
    SCAN 
       IF  .NOT. validaordem
          GOTO ordem
          validaordem = .T.
       ENDIF
       IF LEFT(fi.design, 1)="."
          IF  .NOT. validavenda
             validavenda = .T.
          ELSE
             EXIT
          ENDIF
       ELSE
          IF fi.u_psico AND !(ALLTRIM(UPPER(fi.tipoR)) = 'RES' AND EMPTY(fi.receita))
             validapsico = .T.
             EXIT
          ENDIF
       ENDIF
    ENDSCAN
    LOCAL lcvalida, lcordemapagapsico
    STORE .F. TO lcvalida
    IF validapsico
       SELECT dadospsico
       SCAN FOR dadospsico.cabvendasordem==ucrscabvendas.lordem
          lcvalida = .T.
          lcposapagapsico = dadospsico.cabvendasordem
       ENDSCAN
       IF ( .NOT. EMPTY(lcvalida))
          DELETE FROM dadosPsico WHERE dadospsico.cabvendasordem=lcposapagapsico &&.AND. EMPTY(dadospsico.ftstamp)
          lcvalida = .F.
       ENDIF

       IF lcvalida=.F.
          SELECT dadospsico
          APPEND BLANK
          SELECT ucrsatendcl
          REPLACE dadospsico.u_nmutavi WITH ucrsatendcl.u_nmutavi
          REPLACE dadospsico.u_moutavi WITH ucrsatendcl.u_moutavi
          REPLACE dadospsico.u_cputavi WITH ucrsatendcl.u_cputavi
          REPLACE dadospsico.u_ndutavi WITH ucrsatendcl.u_ndutavi
          REPLACE dadospsico.u_ddutavi WITH ucrsatendcl.u_ddutavi
          REPLACE dadospsico.u_idutavi WITH ucrsatendcl.u_idutavi
          REPLACE dadospsico.u_nmutdisp WITH ucrsatendcl.nome
          REPLACE dadospsico.u_moutdisp WITH ucrsatendcl.morada
          REPLACE dadospsico.u_cputdisp WITH ucrsatendcl.codpost
          REPLACE dadospsico.codigoDocSPMS WITH ucrsatendcl.codDocID
          REPLACE dadospsico.nascimento WITH ucrsatendcl.nascimento
          SELECT ucrscabvendas
          REPLACE dadospsico.u_recdata WITH ucrscabvendas.datareceita
          REPLACE dadospsico.u_medico WITH ALLTRIM(ucrscabvendas.nopresc)
          REPLACE dadosPsico.contactoCli WITH ALLTRIM(ucrsatendcl.contactoUT)
          IF  .NOT. EMPTY(ucrscabvendas.token)
             lcdempsicoaux = .T.
          ENDIF
          SELECT ucrscabvendas
          SELECT dadospsico
          REPLACE dadospsico.cabvendasordem WITH ucrscabvendas.lordem
       ENDIF
    ENDIF
    validapsico = .F.
    validaordem = .F.
    validavenda = .F.
 ENDSCAN
  
 SELECT dadospsico
 IF RECCOUNT("dadosPsico")>0
    LOCAL lcvalpsifalta, lctextopsi
    STORE .T. TO lcvalpsifalta
    STORE '' TO lctextopsi
    IF  .NOT. EMPTY(lcdempsicoaux)
       lcsql = ''
       TEXT TO lcsql TEXTMERGE NOSHOW
				SELECT * FROM dispensa_eletronica_cc (nolock) WHERE LEFT(id,len(id)-1) = '<<ALLTRIM(uCrsatendCl.u_ndutavi)>>' OR doc_id = '<<ALLTRIM(uCrsatendCl.u_ndutavi)>>'
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "uCrsAuxUtente", lcsql)
          uf_perguntalt_chama("Ocorreu uma anomalia ao verificar a informa��o do utentes. Por favor contacte o Suporte.", "OK", "", 16)
          RETURN .F.
       ELSE
          IF RECCOUNT("uCrsAuxUtente")==0
             lcsql = ''
             TEXT TO lcsql TEXTMERGE NOSHOW
						insert into dispensa_eletronica_cc (id, id_emp, nome, morada, nif, nrSNS, sexo, nascimento, validade_fim, publicKey, site, machine, doc_id, tipoDoc) 
                   		values (LEFT(NEWID(),20), '<<ALLTRIM(uCrsE1.id_lt)>>', '<<ALLTRIM(uCrsatendCl.u_nmutavi)>>', '<<ALLTRIM(uCrsatendCl.u_moutavi)>>', '', '', '', 
                   		'<<uf_gerais_getdate(uCrsatendCl.u_dcutavi,"SQL")>>', '<<uf_gerais_getdate(uCrsatendCl.u_ddutavi,"SQL")>>', '','<<ALLTRIM(mySite)>>', '','<<ALLTRIM(uCrsatendCl.u_ndutavi)>>', '<<ALLTRIM(uCrsAtendCl.codDocID)>>')
             ENDTEXT
             IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
                uf_perguntalt_chama("Ocorreu uma anomalia ao inserir a informa��o detalhada do utente. Por favor contacte o Suporte.", "OK", "", 16)
                RETURN .F.
             ENDIF
          ELSE
             lcsql = ''
             TEXT TO lcsql TEXTMERGE NOSHOW
						UPDATE dispensa_eletronica_cc SET tipoDoc = '<<ALLTRIM(uCrsAtendCl.codDocID)>>', morada = '<<ALLTRIM(uCrsatendCl.u_moutavi)>>', doc_id = '<<ALLTRIM(uCrsatendCl.u_ndutavi)>>'
						,nome = '<<ALLTRIM(uCrsatendCl.u_nmutavi)>>', nascimento ='<<uf_gerais_getdate(uCrsatendCl.u_dcutavi,"SQL")>>'
						,validade_fim='<<uf_gerais_getdate(uCrsatendCl.u_ddutavi,"SQL")>>'
						WHERE LEFT(id,len(id)-1) = '<<ALLTRIM(uCrsatendCl.u_ndutavi)>>'	OR doc_id = '<<ALLTRIM(uCrsatendCl.u_ndutavi)>>'
             ENDTEXT
             IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
                uf_perguntalt_chama("Ocorreu uma anomalia ao atualizar a informa��o detalhada do utente. Por favor contacte o Suporte.", "OK", "", 16)
                RETURN .F.
             ENDIF
          ENDIF
       ENDIF
    ENDIF
    SELECT dadospsico
    GOTO TOP
    SCAN
       SELECT ucrscabvendas
       GO TOP
       SCAN FOR ucrscabvendas.lordem==dadospsico.cabvendasordem

            IF !uf_atendimento_valPsico()
                SELECT dadospsico
                DELETE
                RETURN .F.
            ENDIF
        ENDSCAN
    ENDSCAN
 ENDIF
 IF USED("uCrsVale") .AND. USED("uCrsValeAuxMR")
    SELECT ucrscabvendas
    GOTO TOP
    SCAN
       IF  .NOT. EMPTY(ucrscabvendas.ofistamp)
          IF (uf_atendimento_mantemreceita(ucrscabvendas.lordem))==.T.
             SELECT ucrscabvendas
             REPLACE ucrscabvendas.mantemreceita WITH .T.
          ENDIF
       ENDIF
       SELECT ucrscabvendas
    ENDSCAN
    LOCAL lcltstamp, lcltstamp2
    STORE '' TO lcltstamp, lcltstamp2
    SELECT ucrscabvendas
    GOTO TOP
    SCAN FOR  .NOT. EMPTY(ucrscabvendas.u_ltstamp) .AND. ucrscabvendas.mantemreceita==.F.
       lcsql = ''
       TEXT TO lcsql TEXTMERGE NOSHOW
				select u_ltstamp, u_ltstamp2, ft2.token_efectivacao_compl, ft.ftstamp , ft2.u_receita from ft (nolock) inner join ft2 (nolock) on ft.ftstamp=ft2.ft2stamp where u_ltstamp = '<<ALLTRIM(uCrsCabVendas.u_ltstamp)>>' OR u_ltstamp2 = '<<ALLTRIM(uCrsCabVendas.u_ltstamp)>>'
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "uCrsLtStamp2", lcsql)
          uf_perguntalt_chama("Ocorreu ao verificar a informa��o da receita a eliminar. Por favor contacte o Suporte.", "OK", "", 16)
          lcsql = ''
          TEXT TO lcsql TEXTMERGE NOSHOW
					INSERT INTO B_elog
						(tipo, status, mensagem, origem)
					values 
						('E', 'A', 'Erro a eliminar receita no atendimento.','<<ALLTRIM(uCrsCabVendas.u_ltstamp)>>')
          ENDTEXT
          IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
          ENDIF
          RETURN .F.
       ELSE
          SELECT ucrsltstamp2
          IF RECCOUNT("uCrsLtStamp2")>0 .AND. UPPER(ALLTRIM(ucrse1.pais))=='PORTUGAL'

             IF  .NOT. uf_perguntalt_chama("Est� a regularizar vendas com receitas associadas, se continuar vai anular as receitas. Pretende continuar?", "Sim", "N�o")
                RETURN .F.
             ELSE
                SELECT ucrsltstamp2
                IF  .NOT. EMPTY(ucrsltstamp2.token_efectivacao_compl)
                   uf_compart_anula(ucrsltstamp2.token_efectivacao_compl, ucrsltstamp2.ftstamp)
                ENDIF
                SELECT ucrscabvendas
                IF  .NOT. EMPTY(ALLTRIM(ucrscabvendas.token))
                   uf_atendimento_receitasanular(STREXTRACT(ucrscabvendas.design, ':', ' -', 1, 0), ucrscabvendas.token)
                   lcdemanulou = .T.
                ENDIF
                IF RECCOUNT("uCrsLtStamp2")>0
                   lcltstamp2 = ALLTRIM(ucrsltstamp2.u_ltstamp2)
                   lcltstamp = ALLTRIM(ucrsltstamp2.u_ltstamp)
                ELSE
                   lcltstamp2 = 'xxxyyywwwzzz'
                   lcltstamp = 'xxxyyywwwzzz'
                ENDIF
                lctran = "T"+SYS(2015)
                lcsql = ''
                TEXT TO lcsql TEXTMERGE NOSHOW
							BEGIN TRAN <<lcTran>>
								DELETE 
									ctltrct
								WHERE     
									(ctltrctstamp = '<<ALLTRIM(lcLtStamp)>>' OR ctltrctstamp = '<<ALLTRIM(lcLtStamp2)>>') 
									and fechado = 0
									
								UPDATE 
									ft 
								SET 
									u_ltstamp = ''
									,u_ltstamp2 = ''
									,u_slote = 0
									,u_slote2 = 0
								WHERE 
									(u_ltstamp = '<<ALLTRIM(uCrsCabVendas.u_ltstamp)>>' OR u_ltstamp2 = '<<ALLTRIM(uCrsCabVendas.u_ltstamp)>>' )
									AND ISNULL((select fechado from ctltrct (nolock) where ctltrctstamp='<<uCrsCabVendas.u_ltstamp>>'),0) = 0
									
							IF @@ERROR=0
								COMMIT TRAN <<lcTran>>
							ELSE
								BEGIN
									RollBack TRAN <<lcTran>>
									RAISERROR('Erro na Actualizacao Contadores',16,1)
								END
                ENDTEXT
                IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
                   uf_perguntalt_chama("OCORREU UMA ANOMALIA NA ANULA��O DO DOCUMENTO DE ORIGEM. POR QUEST�ES DE SEGURAN�A O SOFTWARE VAI ENCERRAR.", "OK", "", 16)
                   QUIT
                ELSE
                   LOCAL lcsqlsafe
                   lcsqlsafe = lcsql
                   uf_gerais_registaerrolog("SQL Receitas logs: "+ALLTRIM(lcsqlsafe), "R4")
                ENDIF
                LOCAL lcstamp
				lcstamp = uf_gerais_stamp()
				lcsql = ''
				
				SELECT ucrsltstamp2
				TEXT TO lcsql TEXTMERGE NOSHOW
					Insert Into b_ocorrencias
						(stamp, linkStamp, tipo, grau
						,descr
						,oValor
						,usr, date)
					values
						('<<ALLTRIM(lcStamp)>>', '', 'Lotes', 2
						,'Receita Eliminada Nr: ' + '<<ALLTRIM(ucrsltstamp2.u_receita)>>' 
						,''
						,<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()))
				 ENDTEXT
				 IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
				 ENDIF
             ENDIF
          ENDIF
          fecha("uCrsLtStamp2")
       ENDIF
    ENDSCAN
    LOCAL lcanulaslote
    STORE .F. TO lcanulaslote
    SELECT ucrsvale
    GOTO TOP
    SCAN
       IF  .NOT. EMPTY(ucrsvale.ftstamp) .AND. ucrsvale.dev<>0
          TEXT TO lcsql TEXTMERGE NOSHOW
					select ft2.u_receita, ft2.token, ft.u_lote, ft.u_lote2 
					from ft (nolock)
					inner join ft2 (nolock) on ft.ftstamp=ft2.ft2stamp
					where ftstamp='<<ALLTRIM(uCrsVale.ftstamp)>>'
          ENDTEXT
          IF  .NOT. uf_gerais_actgrelha("", "uCrsRecSLote", lcsql)
             uf_perguntalt_chama("Ocorreu ao verificar a informa��o da receita a eliminar. Por favor contacte o Suporte.", "OK", "", 16)
          ELSE
             IF  .NOT. EMPTY(ucrsrecslote.u_receita) .AND.  .NOT. EMPTY(ucrsrecslote.token) .AND. ucrsrecslote.u_lote=0 .AND. ucrsrecslote.u_lote2=0
                lcanulaslote = .T.
             ENDIF
             fecha("uCrsRecSLote")
          ENDIF
       ENDIF
       SELECT ucrsvale
    ENDSCAN
    IF lcanulaslote=.T. .AND. UPPER(ALLTRIM(ucrse1.pais))=='PORTUGAL'

       IF  .NOT. uf_perguntalt_chama("Est� a regularizar vendas com receitas associadas, se continuar vai anular as receitas. Deseja continuar?", "Sim", "N�o")
          RETURN .F.
       ELSE
          SELECT ucrsvale
          SELECT * FROM uCrsVale INTO CURSOR uCrsValeAUX READWRITE
          SELECT DISTINCT ftstamp, u_ltstamp, u_receita FROM uCrsVale INTO CURSOR uCrsVale READWRITE
          GOTO TOP
          SCAN
             IF  .NOT. EMPTY(ucrsvale.ftstamp)
                TEXT TO lcsql TEXTMERGE NOSHOW
							select ft2.u_receita, ft2.token, ft.u_lote, ft.u_lote2 
							from ft (nolock)
							inner join ft2 (nolock) on ft.ftstamp=ft2.ft2stamp
							where ftstamp='<<ALLTRIM(uCrsVale.ftstamp)>>'
                ENDTEXT
                IF  .NOT. uf_gerais_actgrelha("", "uCrsRecSLote", lcsql)
                   uf_perguntalt_chama("Ocorreu ao verificar a informa��o da receita a eliminar. Por favor contacte o Suporte.", "OK", "", 16)
                ELSE
                   IF  .NOT. EMPTY(ucrsrecslote.u_receita) .AND.  .NOT. EMPTY(ucrsrecslote.token) .AND. ucrsrecslote.u_lote=0 .AND. ucrsrecslote.u_lote2=0
                      uf_atendimento_receitasanular(ALLTRIM(ucrsrecslote.u_receita), ALLTRIM(ucrsrecslote.token))
                   ENDIF
                   fecha("uCrsRecSLote")
                ENDIF
                IF USED("uCrsVale")
                   SELECT ucrsvale
                   IF EMPTY(ucrsvale.u_ltstamp) .AND.  .NOT. EMPTY(ucrsvale.u_receita)
                      lcsql = ""
                      TEXT TO lcsql TEXTMERGE NOSHOW
									select top 1 ft.u_ltstamp, ft.u_ltstamp2 from ft2 (nolock)
									inner join ft (nolock) on ft.ftstamp=ft2.ft2stamp
									where u_receita='<<ALLTRIM(uCrsVale.u_receita)>>'
									and ft.nmdoc='Inser��o de Receita'
									order by fdata desc
                      ENDTEXT
                      uf_gerais_actgrelha("", "ucrsStampRec", lcsql)
                      IF RECCOUNT("ucrsStampRec")>0
                         IF ( .NOT. EMPTY(ALLTRIM(ucrsstamprec.u_ltstamp)))
                            lctran = "T"+SYS(2015)
                            lcsql = ''
                            
   
                            
                            TEXT TO lcsql TEXTMERGE NOSHOW
											BEGIN TRAN <<lcTran>>
												DELETE 
													ctltrct
												WHERE     
													ctltrctstamp = '<<ALLTRIM(ucrsStampRec.u_ltstamp)>>' 
													and fechado = 0
													
												UPDATE 
													ft 
												SET 
													u_ltstamp = ''
													,u_ltstamp2 = ''
													,u_slote = 0
													,u_slote2 = 0
												WHERE 
													u_ltstamp = '<<ALLTRIM(ucrsStampRec.u_ltstamp)>>'
													
											IF @@ERROR=0
												COMMIT TRAN <<lcTran>>
											ELSE
												BEGIN
													RollBack TRAN <<lcTran>>
													RAISERROR('Erro na Actualizacao Contadores',16,1)
												END
                            ENDTEXT
                            IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
                               uf_perguntalt_chama("OCORREU UMA ANOMALIA NA ANULA��O DO DOCUMENTO DE ORIGEM. POR QUEST�ES DE SEGURAN�A O SOFTWARE VAI ENCERRAR.", "OK", "", 16)
                               QUIT
                            ENDIF
                            LOCAL lcsqlsafe
                            lcsqlsafe = lcsql
                            uf_gerais_registaerrolog("SQL Receitas logs: "+ALLTRIM(lcsqlsafe), "R5")
                         ENDIF
                      ENDIF
                      fecha("ucrsStampRec")
                      lcsql = ""
                      TEXT TO lcsql TEXTMERGE NOSHOW
									select top 1 ft.u_ltstamp,  ft.u_ltstamp2 from ft2 (nolock)
									inner join ft (nolock) on ft.ftstamp=ft2.ft2stamp
									where u_receita='<<ALLTRIM(uCrsVale.u_receita)>>'
									and ft.nmdoc='Venda a Dinheiro'
									and u_ltstamp <> ''
									and ft.fdata between DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0) and  DATEADD(d, 5, EOMONTH(getdate()))
									order by fdata desc
                      ENDTEXT
                      uf_gerais_actgrelha("", "ucrsStampRecVD", lcsql)
                      IF RECCOUNT("ucrsStampRecVD")>0
                      	 
                  	     
                      	
                         SELECT ucrsstamprecvd
                         GOTO TOP
                         SCAN
                            IF ( .NOT. EMPTY(ALLTRIM(ucrsstamprecvd.u_ltstamp)))
                               lctran = "T"+SYS(2015)
                               lcsql = ''
   
                               
                               TEXT TO lcsql TEXTMERGE NOSHOW
												BEGIN TRAN <<lcTran>>
													DELETE 
														ctltrct
													WHERE     
														ctltrctstamp = '<<ALLTRIM(ucrsStampRecVD.u_ltstamp)>>'
														and fechado = 0
														and ousrdata between DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0) and  DATEADD(d, 5, EOMONTH(getdate()))
														
													UPDATE 
														ft 
													SET 
														u_ltstamp = ''
														,u_ltstamp2 = ''
														,u_slote = 0
														,u_slote2 = 0
													WHERE 
														u_ltstamp = '<<ALLTRIM(ucrsStampRecVD.u_ltstamp)>>'
														
												IF @@ERROR=0
													COMMIT TRAN <<lcTran>>
												ELSE
													BEGIN
														RollBack TRAN <<lcTran>>
														RAISERROR('Erro na Actualizacao Contadores',16,1)
													END
                               ENDTEXT
                               IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
                                  uf_perguntalt_chama("OCORREU UMA ANOMALIA NA ANULA��O DO DOCUMENTO DE ORIGEM. POR QUEST�ES DE SEGURAN�A O SOFTWARE VAI ENCERRAR.", "OK", "", 16)
                                  QUIT
                               ELSE
                                  LOCAL lcsqlsafe
                                  lcsqlsafe = lcsql
                                  uf_gerais_registaerrolog("SQL Receitas logs: "+ALLTRIM(lcsqlsafe), "R3")
                               ENDIF
                            ENDIF
                            SELECT ucrsstamprecvd
                         ENDSCAN
                      ENDIF
                      fecha("ucrsStampRecVD")
                   ENDIF
                ENDIF
             ENDIF
             SELECT ucrsvale
          ENDSCAN
          SELECT DISTINCT * FROM uCrsValeAUX INTO CURSOR uCrsVale READWRITE
          fecha("uCrsValeAUX")
       ENDIF
    ENDIF
 ENDIF

 
 SELECT ucrscabvendas
 GOTO TOP
 SCAN FOR  .NOT. EMPTY(ALLTRIM(ucrscabvendas.token)) .AND.  .NOT. EMPTY(ucrscabvendas.u_ltstamp) .AND. ((ucrscabvendas.fact==.F.) .OR. (ucrscabvendas.partes<>0)) .AND. RIGHT(ALLTRIM(ucrscabvendas.design), 18)=="*SUSPENSA RESERVA*" .AND. EMPTY(ucrscabvendas.mantemreceita)
    lctokenoriginal = ALLTRIM(ucrscabvendas.token)
    lcreceitanr = STREXTRACT(ucrscabvendas.design, ':', ' -', 1, 0)
    lcnrsns = ucrscabvendas.u_nbenef
    lccodacesso = ucrscabvendas.codacesso
    lccoddiropcao = ucrscabvendas.coddiropcao
    IF USED("ucrsLinhaValidarReg")
       fecha("ucrsLinhaValidarReg")
    ENDIF
    SELECT * FROM fi WHERE  NOT EMPTY(fi.dem) INTO CURSOR ucrsLinhaValidarReg READWRITE
    IF RECCOUNT("ucrsLinhaValidarReg")>0
       uf_atendimento_dem(.T., lcreceitanr, lcnrsns, lccodacesso, lccoddiropcao)
       LOCAL lcnrtentativas
       lcnrtentativas = 0
       DO WHILE .T.
          WAIT WINDOW "" TIMEOUT 3
          lcnrtentativas = lcnrtentativas+1
          lcsql = ''
          TEXT TO lcsql TEXTMERGE NOSHOW
					exec up_dem_dadosReceita '<<ALLTRIM(atendimento.stamptmrConsultaDEM)>>'
          ENDTEXT
         
          uf_gerais_actgrelha("", "uCrsVerificaRespostaDEM", lcsql)
          IF RECCOUNT("uCrsVerificaRespostaDEM")>0
             IF !INLIST(ALLTRIM(ucrsverificarespostadem.resultado_consulta_cod), "100003010001")
                regua(2)
                atendimento.stamptmrconsultadem = ''
                uf_perguntalt_chama("Resposta do Sistema Central de Prescri��es: "+ALLTRIM(ucrsverificarespostadem.resultado_consulta_descr), "OK", "", 16)
                RETURN .F.
             ENDIF

             SELECT ucrsverificarespostadem
             GOTO TOP
             UPDATE FI SET token = ucrsverificarespostadem.token, dem = .T. WHERE fi.lordem>=ucrscabvendas.lordem .AND. fi.lordem<ucrscabvendas.lordem+100000
             EXIT
          ENDIF
          IF lcnrtentativas>10
             EXIT
          ENDIF
       ENDDO
    ENDIF
 ENDSCAN
 IF USED("ucrsFiAuxTemp")
    fecha("ucrsFiAuxTemp")
 ENDIF
 
 
 
 IF (!uf_atendimento_validacaoSNSExterna(myRefCovid,"Covid","validacao_trag")) 
    RETURN .F.
 ENDIF


 
 IF ( .NOT. uf_atendimento_validacaoreceitasdem())
    RETURN .F.
 ENDIF
 
 
 
 LOCAL lctemsimulacao
 lctemsimulacao = .F.
 SELECT ucrsatendcomp
 GOTO TOP
 SCAN
    IF (ucrsatendcomp.temsimulacao==.T.)
       lctemsimulacao = .T.
    ENDIF
 ENDSCAN
 
 IF(VARTYPE(compsemsimul)=="U")
	Public compsemsimul
	compsemsimul = .f.
 ENDIF
 
 
 IF compsemsimul=.F. .AND.  .NOT. EMPTY(lctemsimulacao)
    LOCAL lcfistamp2
    lcfistamp2 = ALLTRIM(ucrsatendcomp.fistamp)
    SELECT * FROM fi WHERE ALLTRIM(fi.fistamp)=ALLTRIM(lcfistamp2) INTO CURSOR ucrsfitemp1 READWRITE
    SELECT ucrsfitemp1
    IF AT('SUSPENSA RESERVA', ucrsfitemp1.design)=0
       IF ( .NOT. uf_atendimento_compart_valida())
          RETURN .F.
       ENDIF
    ENDIF
    fecha("ucrsfitemp1")
 ENDIF
 lcnrlindemerr = 0
 IF USED("ucrsReceitaValidar")
    IF RECCOUNT("ucrsReceitaValidar")>0
       lcsql = ''
       TEXT TO lcsql TEXTMERGE NOSHOW
				select distinct ref from excecoes_erros_dem (nolock)
       ENDTEXT
       uf_gerais_actgrelha("", "uCrsExcecoesErrosDem", lcsql)
       SELECT ucrsreceitavalidar
       GOTO TOP
       SCAN
       
          IF ALLTRIM(ucrsreceitavalidar.resultado_validacao_cod)<>'100003020001' .OR.  .NOT. EMPTY(ucrsreceitavalidar.retorno_erro_descr)
             IF RECCOUNT("uCrsExcecoesErrosDem")>0
                LOCAL lcrefvalidacao, lcfindrefvalidacao
                STORE .F. TO lcfindrefvalidacao
                lcrefvalidacao = SUBSTR(ucrsreceitavalidar.retorno_erro_descr, AT('[', ucrsreceitavalidar.retorno_erro_descr)+1, AT(']', ucrsreceitavalidar.retorno_erro_descr)-(AT('[', ucrsreceitavalidar.retorno_erro_descr)+1))
                SELECT ucrsexcecoeserrosdem
                GOTO TOP
                SCAN
                   IF ALLTRIM(ucrsexcecoeserrosdem.ref)==ALLTRIM(lcrefvalidacao)
                      lcfindrefvalidacao = .T.
                   ENDIF
                ENDSCAN
                IF lcfindrefvalidacao=.F.
                   lcnrlindemerr = lcnrlindemerr+1
                ENDIF
                SELECT ucrsreceitavalidar
             ELSE
                lcnrlindemerr = lcnrlindemerr+1
             ENDIF
          ENDIF
       ENDSCAN
       fecha("uCrsExcecoesErrosDem")
    ENDIF
 ENDIF
 

 
 IF lcnrlindemerr<>0
    uf_infovenda_valida()
    RETURN (.F.)
 ELSE
    uf_infovenda_valida_avisos()
 ENDIF
 IF (USED("uCrsInfoAlertas") .AND. RECCOUNT("uCrsInfoAlertas")>0) .OR. (USED("uCrsListaValidades") .AND. RECCOUNT("uCrsListaValidades")>0) .OR. (USED("uCrsInteraccoes") .AND. RECCOUNT("uCrsInteraccoes")>0) .OR. (USED("uCrsListaPVPDif") .AND. RECCOUNT("uCrsListaPVPDif")>0) .OR. RECCOUNT("ucrsReceitaValidar")>0 .OR. (USED("ucrsProdutosNaoCoferidos") .AND. RECCOUNT("ucrsProdutosNaoCoferidos")>0)
    avisos = .T.
 ENDIF
 IF  .NOT. (uf_validarecalculofacrec())
    RETURN .F.
 ENDIF
 IF  .NOT. (uf_atendimento_validapwdev())
    RETURN .F.
 ENDIF
 LOCAL lcbocarobotmapeada
 lcbocarobotmapeada = uf_robot_servico_terminal_mapeado()
 IF ((myusarobot .OR. myusarobotapostore) .AND. myrobotstate) .OR. myusafarmax .OR. (myusarobotconsis .AND. myrobotstate) .OR. (myusaservicosrobot .AND. myrobotstate)
    IF lccountx=0
       SELECT fi
       GOTO TOP
       SCAN FOR EMPTY(fi.ofistamp) .AND.  .NOT. EMPTY(fi.ref) .AND. EMPTY(fi.u_robot)
          IF myusaservicosrobot=.F.
             IF myusafarmax
                uf_robot_dispensafrontbulkfm(.T.)
             ELSE
                uf_robot_dispensafrontbulk(.T.)
             ENDIF
          ENDIF
          EXIT
       ENDSCAN
    ENDIF
 ENDIF
 SELECT fi
 GOTO TOP
 LOCAL lcenforcalocalpresc, lcenforcaentpresc
 LOCAL lcvalnumrec, lcobriganumrec, lccodrec1, lccodrec2, lcvaldigitosReceita 
 STORE .F. TO lcvalnumrec, lcobriganumrec, lcvaldigitosReceita, lcvaldigitosReceita 
 STORE '' TO lccodrec1, lccodrec2
 SELECT ucrscabvendas
 GOTO TOP
 SCAN
    IF  .NOT. (RIGHT(ALLTRIM(ucrscabvendas.design), 10)=="*SUSPENSA*") .AND.  .NOT. (LEFT(ALLTRIM(ucrscabvendas.design), 2)==".S") .AND.  .NOT. (RIGHT(ALLTRIM(ucrscabvendas.design), 18)=="*SUSPENSA RESERVA*")
       lcreceita = STREXTRACT(ucrscabvendas.design, ':', ' -', 1, 0)
       lccodrec1 = STREXTRACT(ucrscabvendas.design, '[', ']', 1, 0)
       lccodrec2 = STREXTRACT(ucrscabvendas.design, '[', ']', 3, 0)
       lcvaldigitosReceita = .f.
       IF EMPTY(lcreceita)
          IF uf_gerais_actgrelha("", 'uCrsDadosPlanoReceita', [exec up_receituario_dadosDoPlano ']+ALLTRIM(lccodrec1)+['])
             IF RECCOUNT("uCrsDadosPlanoReceita")>0
             	
                IF (ucrsdadosplanoreceita.obriga_nrreceita==2 OR ucrsdadosplanoreceita.obriga_nrreceita==3)
                   lcobriganumrec = .T.
                ELSE
                   IF (ucrsdadosplanoreceita.obriga_nrreceita==1)
                      lcvalnumrec = .T.
                   ENDIF
                   IF (ucrsdadosplanoreceita.obriga_nrreceita==4)
                      lcvaldigitosReceita = .T.
                   ENDIF
                ENDIF
             ENDIF
             fecha("uCrsDadosPlanoReceita")
          ENDIF
       ENDIF
       IF EMPTY(ALLTRIM(ucrscabvendas.entpresc))
          IF mypedelocalpresc==.T.
             lcenforcalocalpresc = .T.
          ENDIF
       ENDIF
       IF EMPTY(ALLTRIM(ucrscabvendas.nopresc))
          IF mypedecodpresc==.T.
             lcenforcaentpresc = .T.
          ENDIF
       ENDIF
       IF EMPTY(ucrscabvendas.datareceita) .OR. uf_gerais_getdate(ucrscabvendas.datareceita, "SQL")=='19000101'
          lcenforcadatarec = .T.
       ENDIF
       if(lcvaldigitosReceita AND LEN(ALLTRIM(lcreceita))!=19)
       	  uf_perguntalt_chama("Estrutura de receita inv�lida. Por favor retifique.", "OK", "", 48)
          RETURN .F.
       ENDIF
    	
    ENDIF
    SELECT ucrscabvendas
 ENDSCAN

 IF uf_gerais_getParameter("ADM0000000348", "BOOL")
    SELECT * FROM FI WITH (BUFFERING = .T.) INTO CURSOR uc_tmpFI

**SCAN FOR !EMPTY(uc_tmpFI.ref) AND uc_tmpFI.qtt <> 0 AND uc_tmpFI.u_epvp == 0 AND !uc_tmpFI.u_comp and uc_tmpFI.ref != 'R0000001'

    SELECT uc_tmpFI
    GO TOP
    SCAN FOR !EMPTY(uc_tmpFI.ref) AND uc_tmpFI.qtt <> 0 AND uc_tmpFI.u_epvp == 0 and  !UF_GERAIS_COMPSTR(uc_tmpFI.ref ,'R000001')
	
		
		
        IF !uf_gerais_getUmValor("st", "qlook", "ref = '" + uc_tmpFI.ref + "' and inactivo = 0 and site_nr = " + ASTR(mySite_nr))
            uf_perguntalt_chama("Existem artigos/servi�os com o PVP a 0. Assim n�o pode continuar.", "OK", "", 48)
            RETURN .F.
        ENDIF

        SELECT uc_tmpFI
    ENDSCAN

    FECHA("uc_tmpFI")
 ENDIF

 IF lcobriganumrec
    uf_perguntalt_chama("Existem receitas sem n�mero de receita preenchido. Assim n�o pode continuar.", "OK", "", 48)
    RETURN .F.
 ELSE
    IF lcvalnumrec
       IF  .NOT. uf_perguntalt_chama("Existem receitas sem n�mero de receita preenchido. Tem a certeza que pretende continuar?", "Sim", "N�o")
          RETURN .F.
       ENDIF
    ENDIF
 ENDIF
 

 
 IF mypedelocalpresc
    IF lcenforcalocalpresc
       uf_perguntalt_chama("EXISTEM RECEITAS SEM O C�DIGO DO LOCAL DE PRESCRI��O PREENCHIDO.", "OK", "", 48)
       RETURN .F.
    ENDIF
 ENDIF
 IF mypedecodpresc
    IF lcenforcaentpresc
       uf_perguntalt_chama("EXISTEM RECEITAS SEM O C�DIGO DO PRESCRITOR PREENCHIDO.", "OK", "", 48)
       RETURN .F.
    ENDIF
 ENDIF
 IF uf_gerais_getparameter("ADM0000000073", "BOOL")
    IF lcenforcadatarec
       uf_perguntalt_chama("EXISTEM RECEITAS SEM DATA DA RECEITA PREENCHIDO.", "OK", "", 48)
       RETURN .F.
    ENDIF
 ENDIF
 IF uf_gerais_getparameter("ADM0000000003", "BOOL")==.F.
    atendimento.pedeautenticacao = .F.
 ELSE
    IF mytmpinactividade=0
       atendimento.pedeautenticacao = .T.
    ENDIF
 ENDIF
 
 ** valida��o de vendas com linhas a negativo ap�s comparticipa��o com desconto
	SELECT fi
	GO TOP 
	SCAN 
		IF fi.qtt>0 AND fi.etiliquido<0 AND fi.u_descval>0
			uf_perguntalt_chama("N�O PODE EFETUAR DESCONTOS SUPERIORES AO TOTAL DA LINHA! POR FAVOR RETIFIQUE.", "OK", "", 48)
	       	RETURN .F.
		ENDIF 
	ENDSCAN 
	
	
 LOCAL lcvalidapw
 STORE 0 TO lcvalidapw
 DO WHILE atendimento.pedeautenticacao==.T.
    lcvalidapw = uf_painelcentral_login()
    IF lcvalidapw==2
       EXIT
    ENDIF
 ENDDO
 IF lcvalidapw==2
    RETURN .F.
 ENDIF
 SELECT ucrsatendcl
 GOTO TOP
 SELECT * FROM uCrsAtendCL INTO CURSOR uCrsCLEF READWRITE
 SELECT ucrsatendcl
 GOTO TOP

 IF (!uf_gerais_compStr(pobsatendcl,ucrsatendcl.obs);
    OR ALLTRIM(up_moradaUt) <> ALLTRIM(ucrsatendcl.morada);
    OR ALLTRIM(up_codPostUt) <> ALLTRIM(ucrsatendcl.codPost);
    OR ALLTRIM(up_nbenef) <> ALLTRIM(ucrsatendcl.nbenef);
    OR ALLTRIM(up_nbenef2) <> ALLTRIM(ucrsatendcl.nbenef2);
    OR up_nascimentoUT <> ucrsatendcl.nascimento;
    OR up_sexoUT <> ucrsatendcl.sexo;
	);
    AND ucrsatendcl.no <> 200
    
    TEXT TO lcsql TEXTMERGE NOSHOW
			UPDATE 
	            b_utentes
			SET 
				obs = '<<ALLTRIM(uCrsAtendCL.obs)>>',
				morada = '<<ALLTRIM(ucrsatendcl.morada)>>',
				codPost = '<<ALLTRIM(ucrsatendcl.codPost)>>',
				nbenef = '<<ALLTRIM(ucrsAtendCl.nbenef)>>',
                nbenef2 = '<<ALLTRIM(ucrsAtendCl.nbenef2)>>',
				nascimento = '<<uf_gerais_getdate(ucrsatendcl.nascimento,"SQL")>>',
				sexo = '<<ALLTRIM(ucrsatendcl.sexo)>>'
			where 
            utstamp='<<uCrsAtendCL.utstamp>>'
    ENDTEXT

    uf_gerais_actgrelha("", "", lcsql)

 ENDIF

 
 SELECT ucrsatendcl
 
 IF !EMPTY(ucrsatendcl.u_adUtstamp) 

   IF (ALLTRIM(up_moradaAd) <> ALLTRIM(ucrsatendcl.u_moutavi); 
         OR ALLTRIM(up_codPostAd) <> ALLTRIM(ucrsatendcl.u_cputavi); 
         OR ALLTRIM(up_bino) <> ALLTRIM(ucrsatendcl.u_ndutavi);
         OR up_validadeFim <> ucrsatendcl.u_ddutavi;
         OR up_nascimento <> ucrsatendcl.u_dcutavi;
		   OR up_sexoUT <> ucrsatendcl.sexo;
         OR ucrsatendcl.codDocID <> ucrsatendcl.codDocIDUT);
         AND uf_gerais_getUmValor("b_utentes","no","utstamp = '" + ALLTRIM(ucrsatendcl.u_adUtstamp) + "'") <> 200;
         AND IIF(ucrsatendcl.u_adUtstamp == ucrsatendcl.utstamp, IIF(ALLTRIM(UPPER(ucrsatendcl.u_nmutavi)) <> ALLTRIM(UPPER(ucrsatendcl.nome)), .F., .T.), .T.)

      TEXT TO lcsql TEXTMERGE NOSHOW

         UPDATE 
            b_utentes
         SET
            morada = '<<ALLTRIM(ucrsatendcl.u_moutavi)>>',
            codPost = '<<ALLTRIM(ucrsatendcl.u_cputavi)>>',
            bino = '<<ALLTRIM(ucrsatendcl.u_ndutavi)>>',
            nascimento = '<<ucrsatendcl.u_dcutavi>>',
			   sexo = '<<ALLTRIM(ucrsatendcl.sexo)>>',
            codigoDocSPMS = '<<ALLTRIM(ucrsatendcl.codDocID)>>'
         WHERE
            utstamp = '<<ALLTRIM(ucrsatendcl.u_adUtstamp)>>'

         UPDATE
            dispensa_eletronica_cc
         SET
            validade_fim = '<<ucrsatendcl.u_ddutavi>>',
            tipoDoc = '<<ALLTRIM(uCrsAtendCl.codDocID)>>'
         WHERE
            utstamp = '<<ALLTRIM(ucrsatendcl.u_adUtstamp)>>'

      ENDTEXT
      

      uf_gerais_actgrelha("", "", lcsql)

   ENDIF

 ENDIF
	
 SELECT fi
 SCAN FOR fi.amostra==.T.
    uf_gerais_registaocorrencia('Vendas', 'Altera��o de PVP na Venda', 2, ALLTRIM(fi.ref)+' PVP: '+astr(fi.epvori, 8, 2), ALLTRIM(fi.ref)+' PVP: '+astr(fi.u_epvp, 8, 2), ALLTRIM(fi.fistamp), '', '', '')
 ENDSCAN
 LOCAL lclordem
 lclordem = 0
 SELECT fi
 GOTO TOP
 SCAN
    IF LEFT(fi.design, 1)=="."
       lclordem = fi.lordem
    ELSE
       IF fi.iva==0 .AND.  .NOT. (UPPER(ALLTRIM(fi.lobs2))=='REF.VD.ORIG.')
          UPDATE uCrsCabVendas SET isencaoiva = .T. WHERE ucrscabvendas.lordem=lclordem
       ENDIF
    ENDIF
 ENDSCAN
 SELECT ft2
 IF uf_gerais_getparameter_site('ADM0000000071', 'BOOL', mysite) .AND. ft2.cativa
    LOCAL lcperccativa, lcvalivalin, lctotcativa
    lcperccativa = (ft2.perccativa/100)
    lctotcativa = 0.00 
    SELECT fi
    GOTO TOP
    SCAN
       lcvalivalin = 0.00 
       IF fi.iva<>0
          lcvalivalin = IIF(fi.ivaincl=.T., ROUND(fi.etiliquido-(fi.etiliquido/(1+(fi.iva/100))), 2), ROUND(fi.etiliquido*(fi.iva/100), 2))
          IF fi.partes=0
             UPDATE fi2 SET valcativa = ROUND(lcvalivalin*lcperccativa, 2) WHERE fi2.fistamp=fi.fistamp
             lctotcativa = lctotcativa+ROUND(lcvalivalin*lcperccativa, 2)
          ELSE
             UPDATE fi2 SET valcativa = ROUND(lcvalivalin*lcperccativa, 2)*-1 WHERE fi2.fistamp=fi.fistamp
             lctotcativa = lctotcativa+(ROUND(lcvalivalin*lcperccativa, 2)*-1)
          ENDIF
       ENDIF
    ENDSCAN
    SELECT ft2
    REPLACE ft2.valcativa WITH lctotcativa
 ENDIF

 uf_atendimento_mostraSinave("ATENDIMENTO")


 uf_pagamento_chama()
 
 
 
 
 
 
ENDFUNC
**
FUNCTION uf_gravarcert
 LPARAMETERS tcstamp, tcnrvenda, tcndoc, tcdata, tcsysdata, tchora, tctotal, tctabela
 LOCAL lcversaochave
 STORE '' TO lcversaochave
 tctotal = ABS(ROUND(tctotal, 2))
 IF mycertactiva
    LOCAL lcinvoicetype, lcincluisaft, lcsql
    STORE .F. TO lcincluisaft
    STORE '' TO lcsql
    DO CASE
       CASE ALLTRIM(tctabela)=='FT'
          TEXT TO lcsql TEXTMERGE NOSHOW
					select top 1 excluisaft, tiposaft from td (nolock) where ndoc=<<tcNdoc>>
          ENDTEXT
       CASE ALLTRIM(tctabela)=='RE'
          lcincluisaft = .T.
          lcinvoicetype = 'RC'
       CASE ALLTRIM(tctabela)=='FO'
          lcincluisaft = .T.
          lcinvoicetype = 'FO'
       CASE ALLTRIM(tctabela)=='BO'
          lcincluisaft = .T.
          lcinvoicetype = 'GD'
       OTHERWISE
          uf_perguntalt_chama("A TABELA ESPECIFICADA A CERTIFICAR N�O EXISTE.", "OK", "", 16)
          RETURN .F.
    ENDCASE
    IF  .NOT. lcincluisaft
       IF uf_gerais_actgrelha("", 'uCrsTipoSaft', lcsql)
          IF RECCOUNT("uCrsTipoSaft")>0
             IF LEN(ALLTRIM(ucrstiposaft.tiposaft))>1
                lcincluisaft = .T.
                lcinvoicetype = ALLTRIM(ucrstiposaft.tiposaft)
             ENDIF
          ENDIF
          fecha("uCrsTipoSaft")
       ENDIF
    ENDIF
    IF  .NOT. lcincluisaft
       RETURN .T.
    ENDIF
    LOCAL lchash, i
    STORE '' TO lchash
    DO CASE
       CASE ALLTRIM(tctabela)=='FT'
          TEXT TO lcsql TEXTMERGE NOSHOW
					select 
						ISNULL(cert.hash,'') as hash
					from 
						B_cert cert (nolock)
						inner join ft (nolock) on ft.ftstamp=cert.stamp
					where 
						ft.ftano = Year('<<tcData>>') 
						and ft.ndoc = <<tcNdoc>> 
						and ft.fno = <<tcNrVenda>>-1 
						and ft.site = cert.site
          ENDTEXT
       CASE ALLTRIM(tctabela)=='RE'
          TEXT TO lcsql TEXTMERGE NOSHOW
					select 
						ISNULL(cert.hash,'') as hash
					from 
						B_cert cert (nolock)
						inner join re (nolock) on re.restamp=cert.stamp
					where 
						re.reano=Year('<<tcData>>') 
						and re.ndoc=<<tcNdoc>> 
						and re.rno=<<tcNrVenda>>-1
						and re.site=cert.site
          ENDTEXT
       CASE ALLTRIM(tctabela)=='BO'
          TEXT TO lcsql TEXTMERGE NOSHOW
					select 
						ISNULL(cert.hash,'') as hash
					from 
						B_cert cert (nolock)
						inner join BO (nolock) on BO.bostamp = cert.stamp
					where 
						bo.boano = Year('<<tcData>>') 
						and bo.ndos = <<tcNdoc>> 
						and bo.obrano = <<tcNrVenda>>-1
						and bo.site = cert.site
          ENDTEXT
       OTHERWISE
          RETURN .F.
    ENDCASE
    FOR i = 1 TO 3
       IF  .NOT. uf_gerais_actgrelha("", "uCrsHash", lcsql)
          uf_perguntalt_chama("OCORREU UM ERRO A PROCURAR A HASH DO DOCUMENTO ANTERIOR.", "OK", "", 16)
          TEXT TO lcsql TEXTMERGE NOSHOW
					INSERT INTO B_elog
						(tipo, status, mensagem, origem)
					values
						('E', 'A', 'Erro a procurar hash do documento anterior','<<ALLTRIM(tcStamp)>>')
          ENDTEXT
          uf_gerais_actgrelha("", "", lcsql)
          RETURN .F.
       ELSE
          SELECT ucrshash
          lchash = ALLTRIM(ucrshash.hash)
          fecha("uCrsHash")
          IF  .NOT. EMPTY(lchash)
             i = 3
          ELSE
             WAIT TIMEOUT 3
          ENDIF
       ENDIF
    ENDFOR
    LOCAL lchash2, lcdata, lcsysdata, lcsysdatahora, lchora, lcinvoiceno, lctotal
    STORE '' TO lchash2, lcdata, lcsysdata, lcsysdatahora, lchora, lcinvoiceno, lctotal
    lcdata = SUBSTR(tcdata, 1, 4)+"-"+SUBSTR(tcdata, 5, 2)+"-"+SUBSTR(tcdata, 7, 2)
    lcsysdata = SUBSTR(tcsysdata, 1, 4)+"-"+SUBSTR(tcsysdata, 5, 2)+"-"+SUBSTR(tcsysdata, 7, 2)
    lchora = ALLTRIM(tchora)
    lcsysdatahora = lcsysdata+"T"+lchora
    lcinvoiceno = astr(tcndoc)+"/"+astr(tcnrvenda)
    lctotal = astr(tctotal, 8, 2)
    IF OCCURS(".", lctotal)==0
       lctotal = lctotal+".00"
    ENDIF
    lchash2 = lcdata+";"
    lchash2 = lchash2+lcsysdatahora+";"
    lchash2 = lchash2+lcinvoicetype+" "+lcinvoiceno+";"
    lchash2 = lchash2+lctotal+";"
    lchash2 = lchash2+ALLTRIM(lchash)
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			INSERT INTO B_cert
				(stamp, date, systemDate, systemTime,
				invoiceType, invoiceNo, total, hash,
				site,versaochave)
			values
				('<<ALLTRIM(tcStamp)>>', '<<lcData>>', '<<lcSysData>>', '<<lcHora>>',
				'<<lcInvoiceType>>', '<<lcInvoiceNo>>', '<<lcTotal>>', dbo.up_gerarhash('<<ALLTRIM(lcHash2)>>'),
				'<<ALLTRIM(mySite)>>',<<myCertVersaoChave>>)
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
       uf_perguntalt_chama("ATEN��O: OCORREU UM PROBLEMA A COMPLETAR DADOS DA VENDA. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
       TEXT TO lcsql TEXTMERGE NOSHOW
				INSERT INTO B_elog
					(tipo, status, mensagem, origem)
				values
					('E', 'A', 'Erro a guardar dados de Certifica��o','<<ALLTRIM(tcStamp)>>')
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
       ENDIF
       RETURN .F.
    ENDIF
 ENDIF
 RETURN .T.
ENDFUNC
**
PROCEDURE uf_atendimento_configuracao
 PUBLIC mymultivenda, mycopyplano, mypermitealtnome, mypedelocalpresc, mypedecodpresc, myvendeclgenerico, myimptalaoconjunto, mypvpnegavativo, myvendascredito, myimpdeclaracaocl, myfechomeianoite, mytmpinactividade, mypermitedevolucoes, mypwdevolucoes, mytimer, mydevolucoescredito, myvalidatroco, mydevolucoesdinheiro, myfechomeianoiteUser
 STORE 0 TO mytimer
 mymultivenda = uf_gerais_getparameter('ADM0000000004', 'bool')
 mycopyplano = uf_gerais_getparameter('ADM0000000086', 'bool')
 mypermitealtnome = uf_gerais_getparameter('ADM0000000123', 'bool')
 mypedelocalpresc = uf_gerais_getparameter('ADM0000000001', 'bool')
 mypedecodpresc = uf_gerais_getparameter('ADM0000000002', 'bool')
 myvendeclgenerico = uf_gerais_getparameter('ADM0000000122', 'bool')
 myimptalaoconjunto = uf_gerais_getparameter('ADM0000000111', 'bool')
 mypvpnegavativo = uf_gerais_getparameter('ADM0000000125', 'bool')
 myvendascredito = uf_gerais_getparameter('ADM0000000088', 'bool')
 myimpdeclaracaocl = uf_gerais_getparameter('ADM0000000134', 'bool')
 myfechomeianoite = uf_gerais_getparameter('ADM0000000033', 'bool')
 myfechomeianoiteUser = uf_gerais_getparameter('ADM0000000338', 'bool')
 mytmpinactividade = uf_gerais_getparameter('ADM0000000003', 'num')
 mypermitedevolucoes = uf_gerais_getparameter('ADM0000000055', 'bool')
 mypwdevolucoes = uf_gerais_getparameter('ADM0000000055', 'text')
 mydevolucoescredito = uf_gerais_getparameter('ADM0000000011', 'bool')
 mydevolucoesdinheiro = uf_gerais_getparameter('ADM0000000012', 'bool')
 myvalidatroco = uf_gerais_getparameter('ADM0000000037', 'bool')
ENDPROC
**
PROCEDURE uf_atendimento_ativacliente
 IF atendimento.pgfdados.activepage<>1
    atendimento.pgfdados.activepage = 1
    SELECT fi
    lcpos = RECNO("fi")
 
    SELECT fi
    TRY
       GOTO lcpos
    CATCH
       GOTO TOP
    ENDTRY
    
    IF (VARTYPE(atendimento)<>'U')
    	RETURN .f.
    ENDIF
    
    WITH atendimento.pgfdados.page1
       .btninter.Shape1.backcolor = RGB(0,  167, 231)
      
       IF ft.no==200 .AND. UPPER(ALLTRIM(ft.nome))<>"CONSUMIDOR FINAL"
          .txtnome.disabledbackcolor = RGB(241, 196, 15)
       ELSE
          TEXT TO lcsql TEXTMERGE NOSHOW
					SELECT autorizado from b_utentes (nolock) WHERE no=<<ft.no>> and estab=<<ft.estab>>
          ENDTEXT
          uf_gerais_actgrelha("", "uCrsAutorizado", lcsql)
          IF ucrsautorizado.autorizado=.T.
             .txtnome.disabledbackcolor = RGB(255, 255, 255)
          ELSE
             IF LEFT(ALLTRIM(ft.ncont), 1)<>'5' .AND. LEFT(ALLTRIM(ft.ncont), 1)<>'9' .AND. LEFT(ALLTRIM(ft.ncont), 1)<>'6'
                .txtnome.disabledbackcolor = RGB(255, 0, 0)
             ELSE
                .txtnome.disabledbackcolor = RGB(255, 255, 255)
             ENDIF
          ENDIF
       ENDIF
    ENDWITH
 ELSE
    uf_atendimento_procuracliente()
 ENDIF
ENDPROC
**
FUNCTION uf_atendimento_pesqcliente
 LOCAL lcpesqutentes
 lcpesqutentes = .F.
 IF uf_atendimento_validacampanhas()==.F.
    RETURN .F.
 ENDIF
 lcpesqutentes = .T.
 IF ucrsatendcl.no==200
    uf_pesqutentes_chama("ATENDIMENTO", IIF(UPPER(ALLTRIM(ucrsatendcl.nome))=="CONSUMIDOR FINAL", "", ALLTRIM(ucrsatendcl.nome)), IIF(ALLTRIM(ucrsatendcl.ncont)=uf_gerais_getparameter_site("ADM0000000075", "text", mysite), .F., ucrsatendcl.ncont))
 ELSE
    uf_pesqutentes_chama("ATENDIMENTO", ALLTRIM(ucrsatendcl.nome))
 ENDIF
 atendimento.pgfdados.activepage = 1
 IF  .NOT. (TYPE("UTENTES")=="U")
    utentes.show
 ENDIF
 IF  .NOT. EMPTY(lcpesqutentes)
    IF TYPE("PESQUTENTES")<>"U"
       pesqutentes.show
    ENDIF
 ENDIF
ENDFUNC
**
PROCEDURE uf_atendimento_subircursor
 atendimento.grdatend.setfocus
 KEYBOARD '{PGUP}'
ENDPROC
**
PROCEDURE uf_atendimento_descercursor
 atendimento.grdatend.setfocus
 KEYBOARD '{PGDN}'
ENDPROC
**
PROCEDURE uf_atendimento_abrirdicionario
 SELECT fi
 IF EMPTY(fi.ref) .OR. LEFT(fi.design, 1)=="."
    uf_pesqartigos_chama()
 ELSE
    SELECT ucrsatendst
    IF EMPTY(ALLTRIM(ucrsatendst.existefprod))
       uf_pesqartigos_chama()
    ELSE
       uf_dicionario_chama(ALLTRIM(ucrsatendst.existefprod))
    ENDIF
 ENDIF
ENDPROC
**
FUNCTION uf_atendimento_dividirreg
 LOCAL lcstampfidup, lcdescontodiv
 STORE '' TO lcstampfidup
 PUBLIC qttdividirreg
 STORE 0 TO qttdividirreg, lcdescontodiv
 uf_tecladonumerico_chama("qttDividirReg", "Introduza a Quantidade:", 0, .F., .F., 6, 0)
 SELECT fi
 IF qttdividirreg<=0 .OR. qttdividirreg>=fi.qtt
    RETURN .F.
 ELSE
    SELECT fi
    IF fi.u_descval>0
       lcdescontodiv = fi.u_descval/fi.qtt
    ENDIF
    REPLACE fi.qtt WITH fi.qtt-qttdividirreg
    SELECT fi
    REPLACE fi.u_descval WITH lcdescontodiv*fi.qtt
    
 ENDIF
 SELECT fi
 lcstampfidup = ALLTRIM(fi.fistamp)
 SELECT * FROM fi WHERE ALLTRIM(fi.fistamp)==ALLTRIM(lcstampfidup) INTO CURSOR uCrsFIDup READWRITE
 SELECT ucrsfidup
 uf_atendimento_novavenda(.T.)
 SELECT fi
 GOTO BOTTOM
 uf_atendimento_adicionalinhafi(.F., .F.)
 SELECT fi
 REPLACE fi.ref WITH ALLTRIM(ucrsfidup.ref)
 REPLACE fi.ofistamp WITH ALLTRIM(ucrsfidup.ofistamp)
 uf_atendimento_actref()
 SELECT fi
 TRY
    GOTO lcposfi
 CATCH
    GOTO BOTTOM
 ENDTRY
 SELECT fi
 REPLACE fi.qtt WITH qttdividirreg
 REPLACE fi.ndoc WITH ucrsfidup.ndoc
 REPLACE fi.tkhposlstamp WITH ucrsfidup.tkhposlstamp
 REPLACE fi.nccod WITH ucrsfidup.nccod
 REPLACE fi.ncinteg WITH ucrsfidup.ncinteg
 REPLACE fi.fmarcada WITH ucrsfidup.fmarcada
 REPLACE fi.desconto WITH ucrsfidup.desconto
 REPLACE fi.desc2 WITH ucrsfidup.desc2
 REPLACE fi.u_descval WITH ucrsfidup.u_descval
 REPLACE fi.u_descval WITH lcdescontodiv*qttdividirreg
 REPLACE fi.u_comp WITH ucrsfidup.u_comp
 REPLACE fi.u_psicont WITH ucrsfidup.u_psicont
 REPLACE fi.u_bencont WITH ucrsfidup.u_bencont
 REPLACE fi.u_diploma WITH ucrsfidup.u_diploma
 REPLACE fi.epv WITH ucrsfidup.epv
 REPLACE fi.pv WITH ucrsfidup.pv
 REPLACE fi.u_txcomp WITH ucrsfidup.u_txcomp
 REPLACE fi.u_epvp WITH ucrsfidup.u_epvp
 REPLACE fi.u_ettent1 WITH ucrsfidup.u_ettent1
 REPLACE fi.u_ettent2 WITH ucrsfidup.u_ettent2
 REPLACE fi.etiliquido WITH ucrsfidup.etiliquido
  
 REPLACE fi.tiliquido WITH ucrsfidup.tiliquido
 REPLACE fi.amostra WITH ucrsfidup.amostra
 REPLACE fi.u_epref WITH ucrsfidup.u_epref
 REPLACE fi.pic WITH ucrsfidup.pvpdic
 REPLACE fi.pvpmaxre WITH ucrsfidup.pvpmaxre
 REPLACE fi.opcao WITH ucrsfidup.opcao
 REPLACE fi.partes WITH ucrsfidup.partes
 REPLACE fi.marcada WITH ucrsfidup.marcada
 REPLACE fi.compsns WITH ucrsfidup.compsns
 uf_eventoactvalores()
 atendimento.txttotalvendas.setfocus
 RELEASE qttdividirreg
ENDFUNC
**
FUNCTION uf_atendimento_duplicarvenda
 LOCAL lccab, lcfistamp, lcnrcab, lcstampcab
 lcstampcab = ""
 IF uf_atendimento_validacampanhas()==.F.
    RETURN .F.
 ENDIF
 IF  .NOT. EMPTY(fi.dem)
    uf_perguntalt_chama("N�o � poss�vel duplicar receitas eletr�nicas.", "OK", "", 48)
    RETURN .F.
 ENDIF
 SELECT fi
 lcfistamp = fi.fistamp
 IF USED("ucrsFiAuxNrCabs")
    fecha("ucrsFiAuxNrCabs")
 ENDIF
 SELECT * FROM fi WHERE LEFT(ALLTRIM(fi.design), 1)=="." INTO CURSOR ucrsFiAuxNrCabs READWRITE
 SELECT .F. AS vendaactual, * FROM fi INTO CURSOR ucrsFiAux READWRITE
 SELECT ucrsfiaux
 GOTO TOP
 SCAN
    IF  .NOT. EMPTY(lccab) .AND. LEFT(ALLTRIM(ucrsfiaux.design), 1)=="." .AND. ALLTRIM(ucrsfiaux.fistamp)<>ALLTRIM(lcfistamp)
       EXIT
    ENDIF
    IF ALLTRIM(ucrsfiaux.fistamp)==ALLTRIM(lcfistamp) .AND. LEFT(ALLTRIM(ucrsfiaux.design), 1)=="."
       lccab = .T.
       REPLACE ucrsfiaux.vendaactual WITH .T.
    ENDIF
    IF  .NOT. EMPTY(lccab)
       SELECT ucrsfiaux
       REPLACE ucrsfiaux.vendaactual WITH .T.
    ENDIF
 ENDSCAN
 SELECT ucrsfiaux
 GOTO TOP
 DELETE FROM ucrsFiAux WHERE EMPTY(ucrsfiaux.vendaactual)
 SELECT ucrsfiaux
 GOTO TOP
 lcncab = RECCOUNT("ucrsFiAuxNrCabs")+1
 lcordem = lcncab*100000
 SELECT ucrsfiaux
 GOTO TOP
 SCAN
    lcstamp = uf_gerais_stamp()
    IF USED("uCrsAtendCompAux")
       fecha("uCrsAtendCompAux")
    ENDIF
    SELECT * FROM uCrsAtendComp WHERE ALLTRIM(ucrsatendcomp.fistamp)==ALLTRIM(ucrsfiaux.fistamp) INTO CURSOR uCrsAtendCompAux READWRITE
    SELECT ucrsatendcompaux
    REPLACE ucrsatendcompaux.fistamp WITH lcstamp
    SELECT ucrsatendcomp
    APPEND FROM DBF("uCrsAtendCompAux")
    SELECT ucrsfiaux
    REPLACE ucrsfiaux.fistamp WITH lcstamp
    REPLACE ucrsfiaux.lordem WITH lcordem
    IF LEFT(ALLTRIM(ucrsfiaux.design), 1)=="."
       IF EMPTY(ALLTRIM(STREXTRACT(ucrsfiaux.design, '.', ':', 1, 4)))
          lcdesign = ". SEM RECEITA"
       ELSE
          lcdesign = STREXTRACT(ucrsfiaux.design, '.', ':', 1, 4)
       ENDIF
       SELECT ucrsfiaux
       REPLACE ucrsfiaux.design WITH lcdesign
       lcstampcab = lcstamp
    ENDIF
    lcordem = lcordem+100
 ENDSCAN
 SELECT fi
 GOTO BOTTOM
 SELECT fi
 APPEND FROM DBF("ucrsFiAux")
 atendimento.grdatend.refresh
 uf_atendimento_actvalorescab()
 uf_atendimento_infototaiscab()
 uf_atendimento_infototais()
 IF USED("ucrsFiAuxNrCabs")
    fecha("ucrsFiAuxNrCabs")
 ENDIF
 IF USED("ucrsFiAux")
    fecha("ucrsFiAux")
 ENDIF
 atendimento.grdatend.setfocus
 atendimento.grdatend.click
 atendimento.grdatend.refresh
 atendimento.txttotalvendas.value = atendimento.txttotalvendas.value+1
 SELECT fi
 GOTO BOTTOM
ENDFUNC
**
FUNCTION uf_atendimento_validapic
 LOCAL lcprecoproduto
 PUBLIC myprecoprodutonovo
 STORE 0 TO lcprecoproduto, myprecoprodutonovo
 SELECT fi
 lcprecoproduto = fi.u_epvp
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_stocks_hpr '<<ALLTRIM(fi.ref)>>', 1
 ENDTEXT
 IF  .NOT. (uf_gerais_actgrelha("", "uCrsPicValido", lcsql))
    uf_perguntalt_chama("Ocorreu uma anomalia a verificar pre�os dispon�veis para o produto. Por favor Contacte o Suporte.", "OK", "", 16)
    RETURN .F.
 ELSE
    uf_valorescombo_chama("myPrecoProdutoNovo", 0, "uCrsPicValido", 2, "PIC", "PIC, PRECO_VALIDO, DATA_FIM_ESCOAMENTO", .F., .F., .T., .T., .T.)
 ENDIF
 IF myprecoprodutonovo<>0
    REPLACE fi.u_epvp WITH myprecoprodutonovo
 ENDIF
 atendimento.grdatend.setfocus()
 RELEASE lcprecoproduto
ENDFUNC
**
FUNCTION uf_atendimento_carregacampanhasut
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_campanhas_listar '<<ALLTRIM(mysite)>>'
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "ucrsCampanhasList", lcsql)
    uf_perguntalt_chama("Ocorreu um problema ao verificar as campanhas.", "OK", "", 16)
    RETURN .F.
 ENDIF
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_valcartao_listar '<<ALLTRIM(mysite)>>'
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "ucrsValcartaoList", lcsql)
    uf_perguntalt_chama("Ocorreu um problema ao verificar as campanhas de valor em cart�o.", "OK", "", 16)
    RETURN .F.
 ENDIF
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		select no, estab, cast(campanhas as varchar(4000)) as campanhas from campanhas_ut_lista (nolock) where campanhas<>''
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsCampanhasListaUt", lcsql)
    uf_perguntalt_chama("Ocorreu uma anomalia a verificar utentes associados �s campanhas.", "OK", "", 16)
    RETURN .F.
 ENDIF
 SELECT * FROM uCrsCampanhasListaUt INTO CURSOR uCrsCampanhasListaUtValor READWRITE
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		Select 
			id = '(' + RTRIM(LTRIM(STR(id))) + ')'
		from 
			campanhas (nolock)
		where 
			descv = 0 
			AND desconto = 0 
			AND valorValeDireto = 0
			and campanhas.id not in (select distinct campanhas_id from campanhas_pOfertas (nolock))
			and convert(date,getdate()) between campanhas.dataInicio and campanhas.dataFim
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsCampanhasListaUtSoPontos", lcsql)
    uf_perguntalt_chama("Ocorreu uma anomalia a verificar utentes associados �s campanhas.", "OK", "", 16)
    RETURN .F.
 ENDIF
ENDFUNC
**
FUNCTION uf_atendimento_existemcampanhasut
 LOCAL lcno, lcestab, lcTemCampanhas
 SELECT ft
 
 lcTemCampanhas = .f.
 lcno = ft.no
 lcestab = ft.estab
 IF  .NOT. USED("uCrsCampanhasListaUt")
    RETURN .F.
 ENDIF
  IF  .NOT. USED("UCRSCAMPANHASLIST")
    RETURN .F.
 ENDIF
 SELECT ucrscampanhaslistautsopontos
 GOTO TOP
 SCAN
    UPDATE uCrsCampanhasListaUt SET ucrscampanhaslistaut.campanhas = STRTRAN(ucrscampanhaslistaut.campanhas, ALLTRIM(ucrscampanhaslistautsopontos.id), "") WHERE ucrscampanhaslistaut.no==lcno .AND. ucrscampanhaslistaut.estab==lcestab
 ENDSCAN
 IF USED("uCrsCampanhasListaUtAux")
    fecha("uCrsCampanhasListaUtAux")
 ENDIF
 IF USED("uCrsCampanhasListAux")
    fecha("uCrsCampanhasListAux")
 ENDIF
 SELECT ucrscampanhaslist
 SELECT * FROM uCrsCampanhasList WHERE valor=0 AND pontos=0 AND DATE()>=datainicio AND DATE()<=datafim INTO CURSOR uCrsCampanhasListAux READWRITE
 SELECT ucrscampanhaslistaut
 SELECT * FROM uCrsCampanhasListaUt INTO CURSOR uCrsCampanhasListaUtAux READWRITE
 SELECT ucrscampanhaslistaux
 IF RECCOUNT("uCrsCampanhasListAux")=0
    SELECT ucrscampanhaslistautaux
    DELETE ALL
 ELSE
 	select ucrscampanhaslistautaux
	go top
    delete from ucrscampanhaslistautaux where ucrscampanhaslistautaux.no<>lcno .AND. ucrscampanhaslistautaux.estab<>lcestab
    
    SELECT ucrscampanhaslistaux
    GOTO TOP
    SCAN
       LOCAL lccamplin
       lccamplin = astr(ucrscampanhaslistaux.id)
       SELECT ucrscampanhaslistautaux
        locate for AT(lccamplin, ucrscampanhaslistautaux.campanhas)>0
		IF FOUND()
			lcTemCampanhas = .t.
		endif
*!*	       GOTO TOP
*!*	       SCAN
*!*	          IF ucrscampanhaslistautaux.no<>lcno .AND. ucrscampanhaslistautaux.estab<>lcestab
*!*	             SELECT ucrscampanhaslistautaux
*!*	             DELETE
*!*	          ELSE
*!*	             IF AT(lccamplin, ucrscampanhaslistautaux.campanhas)>0
*!*	                lcTemCampanhas = .t.
*!*	             ENDIF
*!*	          ENDIF
*!*	       ENDSCAN
       SELECT ucrscampanhaslistaux
    ENDSCAN
 ENDIF

 
 IF !EMPTY(lcTemCampanhas )
    RETURN .T.
 ELSE
    RETURN .F.
 ENDIF
ENDFUNC
**
FUNCTION uf_atendimento_carregacampanhasst
 LOCAL lcsql
 TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_campanhas_configSt '<<ALLTRIM(mysite)>>'
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "ucrsCampanhasConfigSt", lcsql)
    uf_perguntalt_chama("Ocorreu um problema ao verificar defini��o das campanhas.", "OK", "", 16)
    RETURN .F.
 ENDIF
 TEXT TO lcsql TEXTMERGE NOSHOW
		select ref, campanhas from campanhas_st_lista (nolock)
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsCampanhasListaSt", lcsql)
    uf_perguntalt_chama("Ocorreu uma anomalia a verificar produtos associados �s campanhas.", "OK", "", 16)
    RETURN .F.
 ENDIF
 SELECT * FROM uCrsCampanhasListaSt INTO CURSOR uCrsCampanhasListaStValor READWRITE
ENDFUNC
**
PROCEDURE uf_atendimento_campanha
 LOCAL lcsql, lctop, lcnome, lcno, lcestab, lcmorada, lcncont, lcncartao, lctipo, lcprofissao, lcsexo, lcnascimento, lcnrcampanhas
 LOCAL lctlmvl, lcoperador, lcidade, lcobs, lcmail, lcefr, lcpatologia, lcentre, lce, lcref
 LOCAL lcinactivo, lctlf, lcutenteno, lcnbenef, lcvaltouch, lcvalentidadeclinica, lcid, lcvalidautente
 STORE '' TO lcsql, lcnome, lcmorada, lcncont, lcncartao, lctipo, lcprofissao, lcsexo
 STORE '' TO lcobs, lcefr, lcpatologia, lcref, lctlf, lcutenteno, lcnbenef, lcid
 STORE 0 TO lcno, lctlmvl, lcmail, lcinactivo, lcvaltouch, lcvalentidadeclinica, lcnrcampanhas
 STORE '19000101' TO lcnascimento, lcentre
 STORE .F. TO lcvalidautente
 SELECT ft
 lcftno = ft.no
 lcftestab = ft.estab
 uf_atcampanhas_chama()
ENDPROC
**
FUNCTION uf_atendimento_validacampanhas


 IF (TYPE("atendimento")=="U")
    RETURN .F.
 ENDIF
 
 IF (TYPE("atendimento")!="U")
   	if(ALLTRIM(atendimento.menu1.conferir.tag)=="true")
   		 RETURN .t.
   	ENDIF   	
 ENDIF

 IF atendimento.campanhas==.T. .AND. lcresad100=.F. .AND. lcressad=.F. .AND. lcresadparc=.F.
    uf_perguntalt_chama("N�o pode fazer esta opera��o depois de aplicar campanhas.", "OK", "", 48)
    RETURN .F.
 ENDIF
 RETURN .T.
ENDFUNC
**
FUNCTION uf_atendimento_dem
 LPARAMETERS lnregularizacao, lnreceitanr, lnnrsns, lncodacesso, lncoddiropcao, lcbackoffice, lcconsulta
 LOCAL lcstamptmrsinc, lcnrreceita, lccodacesso, lcnrsns, lccodfarm, lcdbserver, lcnomejar, lctestjar
 PUBLIC myreceitanr
 myreceitanr = lnreceitanr
 lcnrreceita = lnreceitanr
 lcnrsns = lnnrsns
 lccodacesso = lncodacesso
 lccoddiropcao = lncoddiropcao
 SELECT ucrse1
 IF ALLTRIM(ucrse1.tipoempresa)<>"FARMACIA"
    RETURN .T.
 ENDIF
 IF uf_gerais_verifyinternet()==.F.
    uf_perguntalt_chama("ESTA FUNCIONALIDADE REQUER LIGA��O � INTERNET."+CHR(13)+"POR FAVOR VERIFIQUE A SUA LIGA��O.", "OK", "", 64)
    RETURN .F.
 ENDIF
 SELECT ucrse1
 IF EMPTY(ALLTRIM(ucrse1.u_codfarm))
    uf_perguntalt_chama("C�digo de Farm�cia n�o configurado. Contacte o suporte.", "OK", "", 32)
    RETURN .F.
 ENDIF
 SELECT ucrse1
 lccodfarm = ALLTRIM(ucrse1.u_codfarm)
 regua(0, 13, "A obter dados do Sistema Central de Prescri��es...")
 regua(1, 1, "A obter dados do Sistema Central de Prescri��es...")
 IF EMPTY(lcbackoffice)
    atendimento.stamptmrconsultadem = uf_gerais_stamp()
 ELSE
    facturacao.stamptmrconsultadem = uf_gerais_stamp()
 ENDIF
 IF DIRECTORY(ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\DEM')
    lcnomejar = "LtsDispensaEletronica.jar"
    IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.F.
       lctestjar = "0"
    ELSE
       lctestjar = "1"
    ENDIF
    IF  .NOT. FILE(ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\DEM\'+ALLTRIM(lcnomejar))
       uf_perguntalt_chama("Ficheiro configura��o n�o encontrado. Contacte o Suporte", "OK", "", 64)
       RETURN .F.
    ENDIF
    IF  .NOT. USED("uCrsServerName")
       TEXT TO lcsql TEXTMERGE NOSHOW
				Select @@SERVERNAME as dbServer
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "ucrsServerName", lcsql)
          uf_perguntalt_chama("N�o foi possivel proceder ao servi�o de consulta da Dispensa. Contacte o suporte.", "OK", "", 32)
          regua(2)
          RETURN .F.
       ENDIF
    ENDIF
    SELECT ucrsservername
    lcdbserver = ALLTRIM(ucrsservername.dbserver)
    uv_tipo = uf_gerais_devolveDescrDispensaEletronica(uf_gerais_retornaTipoDispensaEletronica(ALLTRIM(lcnrreceita)))
    
    uv_comando = IIF(UPPER(ALLTRIM(uv_tipo))= "MCDT", "CONSULTA_MSP", "consulta")
    
    IF !uf_gerais_compStr(uv_tipo,"MCDT") and !uf_gerais_compStr(uv_tipo,"DEM") 
    	uv_comando = "CONSULTA_" + uv_tipo
    ENDIF
    
    
    lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\DEM\'+ALLTRIM(lcnomejar)+' ' + ALLTRIM(uv_comando) +' --chavePedido='+IIF(EMPTY(lcbackoffice), ALLTRIM(atendimento.stamptmrconsultadem), ALLTRIM(facturacao.stamptmrconsultadem))+' --nrSNS='+ALLTRIM(lcnrsns)+' --nrReceita='+UPPER(ALLTRIM(lcnrreceita))+' --codFarmacia='+ALLTRIM(lccodfarm)+' --pinReceita='+ALLTRIM(lccodacesso)+' --dbServer='+ALLTRIM(lcdbserver)+' --dbName='+UPPER(ALLTRIM(sql_db))+' --site='+UPPER(ALLTRIM(STRTRAN(ALLTRIM(mysite), ' ', '_')))+' --test='+lctestjar
    IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.T.
    	uf_perguntalt_chama("Webservice de teste...", "OK", "", 64)
       _CLIPTEXT = lcwspath
    ENDIF
    lcwspath = "javaw -jar "+myEncodingFileJar+lcwspath
    owsshell = CREATEOBJECT("WScript.Shell")
    owsshell.run(lcwspath, 1, .T.)
    IF EMPTY(lnregularizacao)
       regua(1, 2, "A obter dados do Sistema Central de Prescri��es...")
       IF EMPTY(lcbackoffice)
          atendimento.tmrconsultadem.enabled = .T.
       ELSE
          facturacao.tmrconsultadem.enabled = .T.
       ENDIF
    ENDIF
 ENDIF

ENDFUNC




**
FUNCTION uf_atendimento_tmrconsultadem
 LPARAMETERS lcbackoffice, lcconsulta
 LOCAL lcdemcnpem_anterior, lcdemref_anterior
 LOCAL lcref, lcplano, lcdiplomacod, lctipolinha, lctiporeceita, lcrecm, lcpais_migrante, lcdatareceita, lctoken, lcdatareceita, lccodigomedico, lcdemefr
 STORE "" TO lcref, lcplano, lcdatareceita, lccodigomedico, lcdemefr
 PUBLIC mydemdispensada
 
 
 
 IF  .NOT. EMPTY(lcconsulta)
 
    LOCAL lcauxstamp
    lcauxstamp = ""
    lcauxstamp = IIF(EMPTY(lcbackoffice), atendimento.stamptmrconsultadem, facturacao.stamptmrconsultadem)
	
    IF EMPTY(lcbackoffice)
       atendimento.ntmrconsultadem = atendimento.ntmrconsultadem+1
       IF atendimento.ntmrconsultadem>40
          regua(2)
          uf_perguntalt_chama("N�o foi poss�vel obter resposta do Sistema Central de Prescri��es.", "OK", "", 16)
          atendimento.tmrconsultadem.enabled = .F.
          atendimento.ntmrconsultadem = 0
          atendimento.stamptmrconsultadem = ''
       	  regua(2)	
          RETURN .F.
       ENDIF
    ELSE
       facturacao.ntmrconsultadem = facturacao.ntmrconsultadem+1
       IF facturacao.ntmrconsultadem>40
          regua(2)
          uf_perguntalt_chama("N�o foi poss�vel obter resposta do Sistema Central de Prescri��es.", "OK", "", 16)
          facturacao.tmrconsultadem.enabled = .F.
          facturacao.ntmrconsultadem = 0
          facturacao.stamptmrconsultadem = ''
          regua(2)	
          RETURN .F.
       ENDIF
    ENDIF
    
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_dem_dadosReceita '<<ALLTRIM(lcAuxStamp)>>'
    ENDTEXT
    LOCAL lcok
    lcok = uf_gerais_actgrelha("DadosDem.GrdPesq", "uCrsVerificaRespostaDEM", lcsql)
    IF EMPTY(lcbackoffice)
       atendimento.tmrconsultadem.enabled = .F.
       atendimento.ntmrconsultadem = 0
       atendimento.stamptmrconsultadem = ''
    ELSE
       facturacao.tmrconsultadem.enabled = .F.
       facturacao.ntmrconsultadem = 0
       facturacao.stamptmrconsultadem = ''
    ENDIF
    IF ( .NOT. lcok)
       uf_perguntalt_chama("OCORREU UMA ANOMALIA A VERIFICAR A RESPOSTA DO WEBSERVICE. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
       regua(2)	
       RETURN .F.
    ELSE
        IF !INLIST(ALLTRIM(ucrsverificarespostadem.resultado_consulta_cod), "100003010001", "100007010003","10100100001")
            uf_perguntalt_chama("Resposta do Sistema Central de Prescri��es: "+ALLTRIM(ucrsverificarespostadem.resultado_consulta_descr), "OK", "", 16)
            regua(2)	
            RETURN .F.
        ENDIF

        SELECT ucrsverificarespostadem
        GO TOP
        IF !EMPTY(ucrsverificarespostadem.anulacaoMotivo)
            regua(2)
            atendimento.stamptmrconsultadem = ''
            uf_perguntalt_chama("MCDT anulado a " + ALLTRIM(ucrsverificarespostadem.anulacaoData) + "." + CHR(13) + "Motivo:" + CHR(13) + ALLTRIM(ucrsverificarespostadem.anulacaoMotivo), "OK", "", 16)
        ENDIF

       dadosdem.lblregistos.caption = ALLTRIM(STR(RECCOUNT("uCrsVerificaRespostaDEM")))+" registos."
       IF TYPE("atendimento")<>"U"
          LOCAL lclordemcabrec
          SELECT fi
          LOCAL lcposcabrec
          lcposcab = RECNO("uCrsCabVendas")
          lcposcabrec = fi.lordem
          SELECT * FROM fi WHERE  NOT EMPTY(fi.ref) AND LEFT(ALLTRIM(STR(fi.lordem)), 2)==LEFT(ALLTRIM(STR(lcposcabrec)), 2) INTO CURSOR UcrsFiCompRec READWRITE
          SELECT ucrsficomprec
          IF RECCOUNT("UcrsFiCompRec")>0
             SELECT ucrsverificarespostadem
             GOTO TOP
             SCAN
                IF  .NOT. EMPTY(ucrsverificarespostadem.medicamento_cod)
                   LOCAL lcrefaprocurar
                   lcrefaprocurar = ALLTRIM(ucrsverificarespostadem.medicamento_cod)
                   SELECT ucrsficomprec
                   GOTO TOP
                   SELECT * FROM UcrsFiCompRec WHERE ALLTRIM(ucrsficomprec.ref)==ALLTRIM(lcrefaprocurar) INTO CURSOR ucrsEncRefRec READWRITE
                   SELECT ucrsencrefrec
                   IF RECCOUNT("ucrsEncRefRec")>0
                      SELECT ucrsverificarespostadem
                      REPLACE ucrsverificarespostadem.at WITH .T.
                      LOCAL lcjaapagou
                      lcjaapagou = .F.
                      SELECT ucrsficomprec
                      GOTO TOP
                      SCAN
                         IF ALLTRIM(ucrsficomprec.ref)==ALLTRIM(lcrefaprocurar) .AND. lcjaapagou=.F.
                            SELECT ucrsficomprec
                            DELETE
                            lcjaapagou = .T.
                         ENDIF
                      ENDSCAN
                   ENDIF
                   fecha("ucrsEncRefRec")
                ELSE
                   LOCAL lccnpemaprocurar
                   lccnpemaprocurar = ALLTRIM(ucrsverificarespostadem.medicamento_cnpem)
                   

                   if(!EMPTY(ALLTRIM(lcCNPEMAProcurar)))
	                   TEXT TO lcsql TEXTMERGE NOSHOW
							select cnp from fprod (nolock) where cnpem='<<ALLTRIM(lcCNPEMAProcurar)>>'
	                   ENDTEXT
	                   IF  .NOT. uf_gerais_actgrelha("", "uCrsCNPEMRef", lcsql)
	                      uf_perguntalt_chama("Ocorreu uma anomalia a verificar os CNPEM para a receita/atendimento! Por favor contacte o suporte.", "OK", "", 16)
	                   ENDIF
                   ENDIF
             
                   if(USED("uCrsCNPEMRef"))
	                   SELECT ucrscnpemref
	                   IF RECCOUNT("uCrsCNPEMRef")>0
	                      LOCAL lcjaencontrou
	                      lcjaencontrou = .F.
	                      SELECT ucrscnpemref
	                      GOTO TOP
	                      SCAN
	                         IF lcjaencontrou=.F.
	                            LOCAL lcrefaprocurar
	                            lcrefaprocurar = ALLTRIM(ucrscnpemref.cnp)
	                            SELECT ucrsficomprec
	                            GOTO TOP
	                            SELECT * FROM UcrsFiCompRec WHERE ALLTRIM(ucrsficomprec.ref)==ALLTRIM(lcrefaprocurar) INTO CURSOR ucrsEncRefRec READWRITE
	                            SELECT ucrsencrefrec
	                            IF RECCOUNT("ucrsEncRefRec")>0
	                               SELECT ucrsverificarespostadem
	                               REPLACE ucrsverificarespostadem.at WITH .T.
	                               LOCAL lcjaapagou
	                               lcjaapagou = .F.
	                               SELECT ucrsficomprec
	                               GOTO TOP
	                               SCAN
	                                  IF ALLTRIM(ucrsficomprec.ref)==ALLTRIM(lcrefaprocurar) .AND. lcjaapagou=.F.
	                                     SELECT ucrsficomprec
	                                     DELETE
	                                     lcjaapagou = .T.
	                                     lcjaencontrou = .T.
	                                  ENDIF
	                               ENDSCAN
	                            ENDIF
	                            fecha("ucrsEncRefRec")
	                         ENDIF
	                         SELECT ucrscnpemref
	                      ENDSCAN
	                   ENDIF
                   ENDIF
                   
                   fecha("uCrsCNPEMRef")
                ENDIF
                SELECT ucrsverificarespostadem
             ENDSCAN
          ENDIF
       ENDIF
       fecha("UcrsFiCompRec")
       SELECT fi
       TRY
          GOTO lcposcabrec
       CATCH
       ENDTRY
       SELECT ucrsverificarespostadem
       GOTO TOP
       uf_dadosdem_preenchedadosadicionais()
       dadosdem.menu1.estado("consultar", "HIDE")
       dadosdem.menu1.gravar.img1.picture = mypath+"\imagens\icons\checked_w.png"
       dadosdem.menu1.estado("", "", "Disp. Todos", .T., "Sair", .T.)
       uf_dadosdem_colocaCursorGrelha()
		regua(2)
       RETURN .F.
    ENDIF
 ENDIF
 IF EMPTY(lcbackoffice)
    IF USED("uCrsAtendComp")
       SELECT * FROM uCrsAtendComp INTO CURSOR uCrsAtendCompAuxMantemCodigos READWRITE
    ENDIF
 ENDIF
 IF  .NOT. USED("uCrsVerificaRespostaDem")
    uf_perguntalt_chama("OCORREU UMA ANOMALIA AO UTILIZAR A INFORMA��O DA CONSULTA. POR FAVOR CONTACTE O SUPORTE. OBRIGADO.", "OK", "", 16)
    RETURN .F.
 ELSE
    IF RECCOUNT("uCrsVerificaRespostaDEM")>0
       mydemdispensada = .T.
       IF USED("ucrsPlanosDemAutomaticos")
          fecha("ucrsPlanosDemAutomaticos")
       ENDIF
       CREATE CURSOR ucrsPlanosDemAutomaticos (plano C(3))
       SELECT ucrsverificarespostadem
       GOTO TOP
       lctoken = ucrsverificarespostadem.token
       lcdatareceita = ucrsverificarespostadem.receita_data
       lccodigomedico = ALLTRIM(ucrsverificarespostadem.medico_codigo)
       LOCAL lcauxfilordem, lclincab, lcOriginalPos
       STORE '' TO lcauxfilordem
       lcauxfilordem = LEFT(ALLTRIM(STR(fi.lordem)), 2)
       
       
       STORE '' TO lcOriginalPos
       lcOriginalPos = LEFT(ALLTRIM(STR(fi.lordem)), 2)
       
       LOCAL lcdemIdProcesso, lcdemIdEpisodio 
       STORE '' TO lcdemIdProcesso, lcdemIdEpisodio 
       
       
       lclincab = fi.lordem
       lcdemcnpem_anterior = ""
       lcprescnpem = .F.
       SELECT ucrsverificarespostadem
       GOTO TOP
       SCAN FOR ( .NOT. EMPTY(ALLTRIM(ucrsverificarespostadem.medicamento_cod)) .OR.  .NOT. EMPTY(ALLTRIM(astr(ucrsverificarespostadem.medicamento_cnpem))); 
       .OR.  !EMPTY(ALLTRIM(ucrsverificarespostadem.tipocamara))   .OR.  !EMPTY(ALLTRIM(ucrsverificarespostadem.tipoPsci)))
          lcdemref = ALLTRIM(ucrsverificarespostadem.medicamento_cod)
          lcdemcnpem = ALLTRIM(astr(ucrsverificarespostadem.medicamento_cnpem))
          lcdemdescr = ALLTRIM(ucrsverificarespostadem.medicamento_descr)
          lcdemposologia = ALLTRIM(ucrsverificarespostadem.posologia)
          lcdemtiporeceita = ALLTRIM(ucrsverificarespostadem.receita_tipo)
          lcdemtipolinha = ALLTRIM(ucrsverificarespostadem.tipo_linha)
          lcdemefr = ALLTRIM(ucrsverificarespostadem.efr_cod)
          lcdemBonusId = ALLTRIM(ucrsverificarespostadem.bonusId)
          lcdemBonusTipo = ALLTRIM(ucrsverificarespostadem.bonusTipo)
          lcdemBonusDescr = ALLTRIM(ucrsverificarespostadem.bonusDescr)
          lcdemIdProcesso  = ALLTRIM(ucrsverificarespostadem.id_processo)
          lcdemIdEpisodio = ALLTRIM(ucrsverificarespostadem.id_episodio)  
          IF EMPTY(ALLTRIM(ucrsverificarespostadem.medicamento_cod)) .OR. EMPTY(ALLTRIM(ucrsverificarespostadem.medicamento_cod)) .AND.; 
          (.NOT. EMPTY(ALLTRIM(ucrsverificarespostadem.tipocamara)) OR  .NOT. EMPTY(ALLTRIM(ucrsverificarespostadem.tipoPsci)))

             PUBLIC myescolheuprodutodem, myescolheuprodutodemlin
             myescolheuprodutodem = .F.

             **LOCAL lcauxfiexisteprod, lctemencomenda
             **STORE .F. TO lcauxfiexisteprod, lctemencomenda
             lcauxfiexisteprod=.f.
             lctemencomenda=.f.
             ** Verificar se ven de uma encomenda 
             SELECT fi 
             IF !EMPTY(fi.bostamp)
             	TEXT TO lcSQL NOSHOW TEXTMERGE 
					select nmdos from bo (nolock) inner join bo2 (nolock) on bo.bostamp=bo2.bo2stamp where bostamp='<<ALLTRIM(fi.bostamp)>>' and nrReceita<>''
				ENDTEXT
				IF !uf_gerais_actGrelha("","UcrsOrigEnc",lcSQL)
					uf_perguntalt_chama("N�o foi poss�vel confirmar a origem do documento.","OK","",48)
					RETURN .f.
				ENDIF 
				SELECT UcrsOrigEnc
				IF RECCOUNT("UcrsOrigEnc")>0
					IF ALLTRIM(UcrsOrigEnc.nmdos)=='Encomenda de Cliente'
						lctemencomenda=.t.
					ENDIF 
				ENDIF 
				fecha("UcrsOrigEnc")
             ENDIF 
             

          	
          	  	
             IF  .NOT. EMPTY(mydemsuspensa) OR lctemencomenda
             	

             
                lcposfi = RECNO("fi")
                lcposfiant = RECNO("fi")
                SELECT fi
                GOTO TOP
                lcposfi = RECNO("fi")
                LOCAL lcjaexiste
                STORE 0 TO lcjaexiste
                SELECT fi
                SCAN
                   IF ALLTRIM(LEFT(ALLTRIM(STR(fi.lordem)), 2))==ALLTRIM(lcOriginalPos ) .AND. ALLTRIM(fi.cnpem)==lcdemcnpem .AND. EMPTY(fi.token) .AND. lcjaexiste=0
                      lcjaexiste = 1
                      lcauxfiexisteprod = .T.
                      myescolheuprodutodem = .T.
                      REPLACE fi.dem WITH .T.
                      REPLACE fi.token WITH ucrsverificarespostadem.token
                      REPLACE fi.receita WITH ucrsverificarespostadem.receita_nr
                      REPLACE fi.id_validacaodem WITH ucrsverificarespostadem.id
                      
                  
                      
                      SELECT fi
                      if(USED("fi2"))
                     	 UPDATE fi2 SET fi2.bonusTipo = lcdemBonusTipo,  fi2.bonusId=lcdemBonusId, fi2.bonusDescr = lcdemBonusDescr, fi2.idProcesso =  lcdemIdProcesso, fi2.idEpisodio = lcdemIdEpisodio     WHERE fi2.fistamp = ALLTRIM(fi.fistamp)
                      ENDIF

                      lcposfi = RECNO("fi")
                   ENDIF
                ENDSCAN
                IF lcauxfiexisteprod=.F.
                   SELECT fi
                   GOTO lastlin
                ELSE
                   SELECT fi
                   TRY
                      SELECT fi
                      GOTO lcposfi
                   CATCH
                      GOTO BOTTOM
                   ENDTRY
                ENDIF
             ENDIF
             SELECT fi
             IF  .NOT. EMPTY(lcdemcnpem_anterior) .AND. ALLTRIM(lcdemcnpem_anterior)==ALLTRIM(lcdemcnpem) .AND. EMPTY(lcauxfiexisteprod)
                uf_atendimento_adicionalinhafi(.F., .F.)
                SELECT fi
                lcposfi = RECNO("fi")
                REPLACE fi.ref WITH lcdemref_anterior
                SELECT fi
                lcsql = ""
                TEXT TO lcsql TEXTMERGE NOSHOW
							SELECT dispositivo_seguranca FROM fprod (nolock) WHERE fprod.cnp='<<ALLTRIM(fi.ref)>>'
                ENDTEXT
                IF  .NOT. uf_gerais_actgrelha("", "uCrsDadosCaixa", lcsql)
                   uf_perguntalt_chama("N�o foi possivel encontrar regitos.", "", "OK", 16)
                   RETURN .F.
                ELSE
                   IF ucrsdadoscaixa.dispositivo_seguranca
                      SELECT fi
                      uf_insert_fi_trans_info_seminfo()
                   ENDIF
                   fecha("uCrsDadosCaixa")
                ENDIF
                SELECT fi
                uf_atendimento_actref(.T.)
                SELECT fi
                uf_atendimento_eventofi()
                IF EMPTY(lcbackoffice)
                   uf_atendimento_dadosst()
                ENDIF
                SELECT fi
                TRY
                   GOTO lcposfi
                CATCH
                   GOTO BOTTOM
                ENDTRY
                IF EMPTY(lcbackoffice)
                   atendimento.txttotalvendas.setfocus
                ENDIF
                myescolheuprodutodem = .T.
             ELSE
                IF EMPTY(lcauxfiexisteprod)
         
                   lcdemcnpem_anterior = ucrsverificarespostadem.medicamento_cnpem
                   LOCAL lcdemdci
                   lcdemdci = ''
                   IF ( .NOT. EMPTY(ALLTRIM(ucrsverificarespostadem.tipocamara)))
                      lcdemdci = ALLTRIM('TipoCE'+ALLTRIM(ucrsverificarespostadem.tipocamara))
                   ENDIF
                    IF ( .NOT. EMPTY(ALLTRIM(ucrsverificarespostadem.tipoPsci)))
                      lcdemdci = ALLTRIM('TipoPSCI'+ALLTRIM(ucrsverificarespostadem.tipoPsci))
                   ENDIF
                   IF EMPTY(lcbackoffice)
                      uf_pesqstocks_chama("ATENDIMENTO", .F., .T., ALLTRIM(astr(ucrsverificarespostadem.medicamento_cnpem)), .F., lcdemref, lcdemcnpem, lcdemdescr, lcdemposologia, lcdemtiporeceita, lcdemtipolinha, lcdemdci)
                   ELSE
                      uf_pesqstocks_chama("FACTURACAO", .F., .T., ALLTRIM(astr(ucrsverificarespostadem.medicamento_cnpem)), .F., lcdemref, lcdemcnpem, lcdemdescr, lcdemposologia, lcdemtiporeceita, lcdemtipolinha, lcdemdci)
                   ENDIF
                   SELECT fi
                   lcposfi = RECNO("fi")
                   lcdemref_anterior = fi.ref
                ENDIF
             ENDIF
             IF USED("uCrsAtendComp")
                IF  .NOT. EMPTY(stamplindem) .AND. RECCOUNT("uCrsAtendComp")>=1 .AND. EMPTY(mydemsuspensa)
                   LOCAL lcposfidem
                   SELECT fi
                   GOTO TOP
                   SCAN
                      IF ALLTRIM(fi.fistamp)==ALLTRIM(stamplindem)
                         lcposfidem = RECNO("fi")
                      ENDIF
                   ENDSCAN
                   SELECT fi
                   TRY
                      GOTO lcposfidem
                   CATCH
                      GOTO BOTTOM
                   ENDTRY
                ENDIF
             ENDIF
             IF myescolheuprodutodem==.F.
                lcdemcnpem_anterior = ""
                lcdemref_anterior = ""
             ELSE
             
                SELECT fi
                REPLACE fi.dem WITH .T.
                REPLACE fi.token WITH ucrsverificarespostadem.token
                REPLACE fi.receita WITH ucrsverificarespostadem.receita_nr
                REPLACE fi.id_validacaodem WITH ucrsverificarespostadem.id
                REPLACE fi.posologia WITH ucrsverificarespostadem.posologia
                
              
                SELECT fi
                if(USED("fi2"))
                    UPDATE fi2 SET fi2.bonusTipo = ALLTRIM(ucrsverificarespostadem.bonusTipo) ,  fi2.bonusId=ALLTRIM(ucrsverificarespostadem.bonusId) , fi2.bonusDescr = ALLTRIM(ucrsverificarespostadem.bonusDescr),  fi2.idEpisodio = ALLTRIM(ucrsverificarespostadem.id_episodio), fi2.idProcesso = ALLTRIM(ucrsverificarespostadem.id_processo)  WHERE fi2.fistamp = ALLTRIM(fi.fistamp)
                ENDIF
                
                
                IF TYPE("fi.posologia_c") <> "U"
                  REPLACE fi.posologia_c WITH ucrsverificarespostadem.posologia
                ENDIF
                IF ALLTRIM(fi.nmdos)=='Encomenda de Cliente'
                   REPLACE fi.tipor WITH 'RES'
                   REPLACE fi.tpres WITH 'RN'
                   REPLACE fi.qtdem WITH 1
                   SELECT ucrsfireservas
                   GOTO BOTTOM
                   APPEND BLANK
                   REPLACE ucrsfireservas.tpres WITH fi.tpres
                   REPLACE ucrsfireservas.fistamp WITH fi.fistamp
                   REPLACE ucrsfireservas.ref WITH fi.ref
                   REPLACE ucrsfireservas.design WITH fi.design
                   REPLACE ucrsfireservas.qtt WITH fi.qtt
                   REPLACE ucrsfireservas.etiliquido WITH fi.etiliquido
                   REPLACE ucrsfireservas.ivaincl WITH fi.ivaincl
                   REPLACE ucrsfireservas.tabiva WITH fi.tabiva
                   REPLACE ucrsfireservas.armazem WITH fi.armazem
                   REPLACE ucrsfireservas.lordem WITH fi.lordem
                   REPLACE ucrsfireservas.epv WITH fi.epv
                   REPLACE ucrsfireservas.ecusto WITH fi.ecusto
                   REPLACE ucrsfireservas.epvori WITH fi.epvori
                   REPLACE ucrsfireservas.u_epvp WITH fi.u_epvp
                   REPLACE ucrsfireservas.u_ettent1 WITH fi.u_ettent1
                   REPLACE ucrsfireservas.u_ettent2 WITH fi.u_ettent2
                   REPLACE ucrsfireservas.u_txcomp WITH fi.u_txcomp
                   REPLACE ucrsfireservas.comp_tipo WITH fi.comp_tipo
                   REPLACE ucrsfireservas.comp WITH fi.comp
                   REPLACE ucrsfireservas.comp_diploma WITH fi.comp_diploma
                   REPLACE ucrsfireservas.comp_2 WITH fi.comp_2
                   REPLACE ucrsfireservas.comp_diploma_2 WITH fi.comp_diploma_2
                   REPLACE ucrsfireservas.comp_tipo_2 WITH fi.comp_tipo_2
                ENDIF
                uf_atendimento_marcademtotal(fi.dem, fi.design, fi.receita, fi.id_validacaodem, .T., .F.)
                lcdiplomacod = ucrsverificarespostadem.diploma_cod
                lctipolinha = ucrsverificarespostadem.tipo_linha
                lcrecm = ucrsverificarespostadem.recm
                lcpais_migrante = ucrsverificarespostadem.pais_migrante
                lctiporeceita = ucrsverificarespostadem.receita_tipo
                lcnrreceita = ucrsverificarespostadem.receita_nr
                lcToken = ucrsverificarespostadem.token
                lcBonusId = ALLTRIM(ucrsverificarespostadem.bonusId)
                lcBonusTipo = ALLTRIM(ucrsverificarespostadem.bonusTipo)
                lcplano = uf_atendimento_planoautomatico(lcdiplomacod, lctipolinha, lcrecm, lcpais_migrante, lctiporeceita, lcdemefr, lcnrreceita, lcToken)
             

                IF  .NOT. EMPTY(ALLTRIM(ucrsverificarespostadem.excecao))
                   WITH atendimento.pgfdados.page2
                      DO CASE
                         CASE UPPER(ALLTRIM(ucrsverificarespostadem.excecao))=="A"
                            SELECT fi
                            REPLACE fi.lobs3 WITH "A"
                            IF EMPTY(lcbackoffice)
                               tpexcecao = 1
                            ENDIF
                         CASE UPPER(ALLTRIM(ucrsverificarespostadem.excecao))=="B"
                            SELECT fi
                            REPLACE fi.lobs3 WITH "B"
                            IF EMPTY(lcbackoffice)
                               tpexcecao = 2
                            ENDIF
                         CASE UPPER(ALLTRIM(ucrsverificarespostadem.excecao))=="C"
                            SELECT fi
                            REPLACE fi.lobs3 WITH "C2"
                            IF EMPTY(lcbackoffice)
                               tpexcecao = 3
                            ENDIF
                      ENDCASE
                   ENDWITH
                ENDIF
                IF  .NOT. EMPTY(ALLTRIM(ucrsverificarespostadem.diploma_cod))
                   SELECT fi
                   REPLACE fi.u_diploma WITH ALLTRIM(ucrsverificarespostadem.u_design)
                ENDIF
                IF EMPTY(lcbackoffice)
                   IF  .NOT. EMPTY(ucrsverificarespostadem.posologia)
                      SELECT ucrsatendst
                      LOCATE FOR ucrsatendst.ststamp==fi.fistamp
                      IF FOUND()
                         REPLACE ucrsatendst.u_posprog WITH ucrsverificarespostadem.posologia
                      ENDIF
                   ENDIF
                ELSE
                   IF USED("FI")
                     SELECT FI
                     IF TYPE("fi.posologia_c") <> "U"
                        REPLACE fi.posologia_c WITH ucrsverificarespostadem.posologia
                     ENDIF
                   ENDIF
                ENDIF
                LOCAL lcposfi1
                lcposfi1 = RECNO("fi")
                SELECT fi
                GOTO lcposfi1
                SELECT fi
                IF fi.lordem<>0
                   lcfilordemcab = LEFT(astr(fi.lordem), 2)+REPLICATE('0', LEN(astr(fi.lordem))-2)
                ELSE
                   lcfilordemcab = LEFT(astr(nrlincabv), 2)+REPLICATE('0', LEN(astr(nrlincabv))-2)
                ENDIF
                UPDATE Fi SET dem = .T., tipor = lcdemtiporeceita, token = ucrsverificarespostadem.token, receita = ucrsverificarespostadem.receita_nr WHERE astr(fi.lordem)=lcfilordemcab
                IF  .NOT. EMPTY(lcplano)
                   SELECT ucrsplanosdemautomaticos
                   GOTO TOP
                   LOCATE FOR ALLTRIM(ucrsplanosdemautomaticos.plano)==ALLTRIM(lcplano)
                   IF  .NOT. FOUND()
                      SELECT ucrsplanosdemautomaticos
                      APPEND BLANK
                      REPLACE ucrsplanosdemautomaticos.plano WITH lcplano
                   ENDIF
                ENDIF
             ENDIF
          ELSE
             lcref = ALLTRIM(ucrsverificarespostadem.medicamento_cod)
             
             lcrefaux = fi.lordem
             IF  .NOT. EMPTY(lcref)
                lcinseriulinha = .F.
                lcparafarmacia = .T.
                lcsql = ""
                TEXT TO lcsql TEXTMERGE NOSHOW
							exec up_touch_ActRef '<<ALLTRIM(lcRef)>>', <<myArmazem>>, <<mysite_nr>>
                ENDTEXT
                IF  .NOT. uf_gerais_actgrelha("", 'uCrsActRef', lcsql)
                   regua(2)
                   uf_perguntalt_chama("Ocorreu uma anomalia a verificar exist�ncia da refer�ncia.", "OK", "", 64)
                   mydemdispensada = .F.
                   RETURN .F.
                ELSE
                   IF RECCOUNT("uCrsActRef")==0
                      IF ALLTRIM(lcref)=="99999"
                         lcparafarmacia = .T.
                         IF EMPTY(lcbackoffice)
                            uf_pesqstocks_chama("ATENDIMENTO", .F., .T., .F., .T., lcdemref, lcdemcnpem, lcdemdescr, lcdemposologia, lcdemtiporeceita, lcdemtipolinha)
                         ELSE
                            uf_pesqstocks_chama("FACTURACAO", .F., .T., .F., .T., lcdemref, lcdemcnpem, lcdemdescr, lcdemposologia, lcdemtiporeceita, lcdemtipolinha)
                         ENDIF
                      ELSE
                         IF EMPTY(lcbackoffice)
                            uf_pesqstocks_chama("ATENDIMENTO", .F., .T., ALLTRIM(astr(ucrsverificarespostadem.medicamento_cod)), .F., lcdemref, lcdemcnpem, lcdemdescr, lcdemposologia, lcdemtiporeceita, lcdemtipolinha)
                         ELSE
                            uf_pesqstocks_chama("FACTURACAO", .F., .T., ALLTRIM(astr(ucrsverificarespostadem.medicamento_cod)), .F., lcdemref, lcdemcnpem, lcdemdescr, lcdemposologia, lcdemtiporeceita, lcdemtipolinha)
                         ENDIF
                      ENDIF
                      SELECT fi
                      IF (ALLTRIM(fi.ref)==lcref .AND. lcrefaux<>fi.lordem) .OR. (ALLTRIM(lcref)=="99999" .AND. lcrefaux<>fi.lordem)
                         lcinseriulinha = .T.
                      ELSE
                         lcinseriulinha = .F.
                      ENDIF
                   ENDIF

                   
                   IF EMPTY(lcbackoffice)
                      **LOCAL lcauxfilordem, lcauxfiexisteprod
                      **STORE '' TO lcauxfilordem, lctemencomenda
                      lcauxfilordem=''
                      **lctemencomenda=''
                      **STORE .F. TO 
                      lcauxfiexisteprod=.f.
                      lctemencomenda=.f.
                      ** Verificar se vem de uma encomenda 
		              SELECT fi 
		              IF !EMPTY(fi.bostamp)
		             	 TEXT TO lcSQL NOSHOW TEXTMERGE 
					 		select nmdos from bo (nolock) inner join bo2 (nolock) on bo.bostamp=bo2.bo2stamp where bostamp='<<ALLTRIM(fi.bostamp)>>' and nrReceita<>''
				 		ENDTEXT
			 			IF !uf_gerais_actGrelha("","UcrsOrigEnc",lcSQL)
			 				uf_perguntalt_chama("N�o foi poss�vel confirmar a origem do documento.","OK","",48)
							RETURN .f.
						ENDIF 
						SELECT UcrsOrigEnc
						IF RECCOUNT("UcrsOrigEnc")>0
							IF ALLTRIM(UcrsOrigEnc.nmdos)=='Encomenda de Cliente'
								lctemencomenda=.t.
							ENDIF 
						ENDIF 
						fecha("UcrsOrigEnc")
		             ENDIF 
		          
		             IF  .NOT. EMPTY(mydemsuspensa) OR lctemencomenda
		             
		             		
                         &&lcauxfilordem = LEFT(ALLTRIM(STR(fi.lordem)), 2)
                         LOCAL lcposfitmp
                         SELECT fi
                         lcposfitmp = RECNO("fi")

     
                         SELECT fi
                         &&GO TOP &&convem voltar ao inicio antes do locate for
                         LOCATE FOR ALLTRIM(fi.ref)==lcref .AND. LEFT(ALLTRIM(STR(fi.lordem)), 2)=lcOriginalPos .AND. EMPTY(fi.token)
                         IF FOUND()
                         
                            lcauxfiexisteprod = .T.
                            lcinseriulinha = .T.
                         ELSE
                         	
                            SELECT fi
                            TRY
                               GOTO lcposfitmp
                            CATCH
                               GOTO BOTTOM
                            ENDTRY
                         ENDIF
                      ENDIF
                      
                      
           
                      IF lcauxfiexisteprod==.F.
                      	 

                         LOCAL lcposfitmp
                         SELECT fi
                         lcposfitmp = RECNO("fi")
                         atendimento.mododem = .T.
                         SELECT fi
                         TRY
                            GOTO lcposfitmp
                         CATCH
                            GOTO BOTTOM
                         ENDTRY
                         atendimento.grdatend.setfocus
                         atendimento.grdatend.click
                         IF lcinseriulinha==.F. .AND. RECCOUNT("uCrsActRef")>0
                            atendimento.txtscanner.value = ""
                            atendimento.txtscanner.value = ALLTRIM(lcref)
                            lcinseriulinha = uf_atendimento_scanner()
                         ENDIF
                      ENDIF
                      
  
                      
                      lcdiplomacod = ucrsverificarespostadem.diploma_cod
                      lctipolinha = ucrsverificarespostadem.tipo_linha
                      lcrecm = ucrsverificarespostadem.recm
                      lcpais_migrante = ucrsverificarespostadem.pais_migrante
                      lctiporeceita = ucrsverificarespostadem.receita_tipo
                      lcnrreceita = ucrsverificarespostadem.receita_nr
                      lcToken = ucrsverificarespostadem.token
                      lcBonusId = ALLTRIM(ucrsverificarespostadem.bonusId)
                	  lcBonusTipo = ALLTRIM(ucrsverificarespostadem.bonusTipo)
                	  lcplano = uf_atendimento_planoautomatico(lcdiplomacod, lctipolinha, lcrecm, lcpais_migrante, lctiporeceita, lcdemefr, lcnrreceita, lcToken)
                    
                      IF  .NOT. EMPTY(ALLTRIM(ucrsverificarespostadem.excecao)) .AND. lcinseriulinha==.T.
                         WITH atendimento.pgfdados.page2
                            DO CASE
                               CASE UPPER(ALLTRIM(ucrsverificarespostadem.excecao))=="A"
                                  SELECT fi
                                  REPLACE fi.lobs3 WITH "A"
                                  tpexcecao = 1
                               CASE UPPER(ALLTRIM(ucrsverificarespostadem.excecao))=="B"
                                  SELECT fi
                                  REPLACE fi.lobs3 WITH "B"
                                  tpexcecao = 2
                               CASE UPPER(ALLTRIM(ucrsverificarespostadem.excecao))=="C"
                                  SELECT fi
                                  REPLACE fi.lobs3 WITH "C2"
                                  tpexcecao = 1
                            ENDCASE
                         ENDWITH
                      ENDIF
                      
                     SELECT fi
	                 if(USED("fi2"))
	                    UPDATE fi2 SET fi2.bonusTipo = ALLTRIM(ucrsverificarespostadem.bonusTipo) ,  fi2.bonusId=ALLTRIM(ucrsverificarespostadem.bonusId) , fi2.bonusDescr = ALLTRIM(ucrsverificarespostadem.bonusDescr), fi2.idEpisodio = ALLTRIM(ucrsverificarespostadem.id_episodio), fi2.idProcesso = ALLTRIM(ucrsverificarespostadem.id_processo)  WHERE fi2.fistamp = ALLTRIM(fi.fistamp)
	                 ENDIF
                         
                      

                      
                      IF  .NOT. EMPTY(ALLTRIM(ucrsverificarespostadem.diploma_cod)) .AND. lcinseriulinha==.T.
                         SELECT fi
                         REPLACE fi.u_diploma WITH ALLTRIM(ucrsverificarespostadem.u_design)
                      ENDIF
                      IF  .NOT. EMPTY(ucrsverificarespostadem.posologia) .AND. lcinseriulinha==.T.
                         SELECT ucrsatendst
                         LOCATE FOR ucrsatendst.ststamp==fi.fistamp
                         IF FOUND()
                            REPLACE ucrsatendst.u_posprog WITH ucrsverificarespostadem.posologia
                         ENDIF
                      ENDIF
                      IF lcinseriulinha==.T.
                      
                     	 
                         SELECT fi
                         REPLACE fi.dem WITH .T.
                         REPLACE fi.token WITH ucrsverificarespostadem.token
                         REPLACE fi.receita WITH ucrsverificarespostadem.receita_nr
                         REPLACE fi.id_validacaodem WITH ucrsverificarespostadem.id
                         REPLACE fi.posologia WITH ucrsverificarespostadem.posologia
                         IF TYPE("fi.posologia_c") <> "U"
                           REPLACE fi.posologia_c WITH ucrsverificarespostadem.posologia
                         ENDIF
                         
                         

                         SELECT fi
		                 if(USED("fi2"))
		                    UPDATE fi2 SET fi2.bonusTipo = ALLTRIM(ucrsverificarespostadem.bonusTipo) ,  fi2.bonusId=ALLTRIM(ucrsverificarespostadem.bonusId) , fi2.bonusDescr = ALLTRIM(ucrsverificarespostadem.bonusDescr), fi2.idEpisodio = ALLTRIM(ucrsverificarespostadem.id_episodio), fi2.idProcesso = ALLTRIM(ucrsverificarespostadem.id_processo)  WHERE fi2.fistamp = ALLTRIM(fi.fistamp)
		                 ENDIF
                         
                         uf_atendimento_marcademtotal(fi.dem, fi.design, fi.receita, fi.id_validacaodem, .T.)
                      ENDIF
                      SELECT fi
                      IF ALLTRIM(fi.nmdos)=='Encomenda de Cliente'
                         REPLACE fi.tipor WITH 'RES'
                         REPLACE fi.tpres WITH 'RN'
                         REPLACE fi.qtdem WITH 1
                         SELECT ucrsfireservas
                         GOTO BOTTOM
                         APPEND BLANK
                         REPLACE ucrsfireservas.tpres WITH fi.tpres
                         REPLACE ucrsfireservas.fistamp WITH fi.fistamp
                         REPLACE ucrsfireservas.ref WITH fi.ref
                         REPLACE ucrsfireservas.design WITH fi.design
                         REPLACE ucrsfireservas.qtt WITH fi.qtt
                         REPLACE ucrsfireservas.etiliquido WITH fi.etiliquido
                         REPLACE ucrsfireservas.ivaincl WITH fi.ivaincl
                         REPLACE ucrsfireservas.tabiva WITH fi.tabiva
                         REPLACE ucrsfireservas.armazem WITH fi.armazem
                         REPLACE ucrsfireservas.lordem WITH fi.lordem
                         REPLACE ucrsfireservas.epv WITH fi.epv
                         REPLACE ucrsfireservas.ecusto WITH fi.ecusto
                         REPLACE ucrsfireservas.epvori WITH fi.epvori
                         REPLACE ucrsfireservas.u_epvp WITH fi.u_epvp
                         REPLACE ucrsfireservas.u_ettent1 WITH fi.u_ettent1
                         REPLACE ucrsfireservas.u_ettent2 WITH fi.u_ettent2
                         REPLACE ucrsfireservas.u_txcomp WITH fi.u_txcomp
                         REPLACE ucrsfireservas.comp_tipo WITH fi.comp_tipo
                         REPLACE ucrsfireservas.comp WITH fi.comp
                         REPLACE ucrsfireservas.comp_diploma WITH fi.comp_diploma
                         REPLACE ucrsfireservas.comp_2 WITH fi.comp_2
                         REPLACE ucrsfireservas.comp_diploma_2 WITH fi.comp_diploma_2
                         REPLACE ucrsfireservas.comp_tipo_2 WITH fi.comp_tipo_2
                      ENDIF
                      SELECT fi
                      IF lcinseriulinha==.T.
                         LOCAL lcposfitmp
                         SELECT fi
                         lcposfitmp = RECNO("fi")
                         lcfilordemcab = ALLTRIM(LEFT(astr(fi.lordem), 2)+REPLICATE('0', LEN(astr(fi.lordem))-2))
                         UPDATE Fi SET tipor = lcdemtiporeceita, dem = .T., token = ucrsverificarespostadem.token, receita = ucrsverificarespostadem.receita_nr WHERE astr(fi.lordem)=lcfilordemcab
                         SELECT fi
                         TRY
                            GOTO lcposfitmp
                         CATCH
                            GOTO BOTTOM
                         ENDTRY
                      ELSE
                         SELECT fi
                         IF LEFT(ALLTRIM(fi.design), 1)=="."
                            REPLACE receita WITH ALLTRIM(ucrsverificarespostadem.receita_nr)
                            REPLACE tipor WITH lcdemtiporeceita
                            REPLACE dem WITH .T.
                         ENDIF
                      ENDIF
                      IF ALLTRIM(lcref)=="99999" .AND. lcinseriulinha==.T.
                         REPLACE fi.manipulado WITH lcparafarmacia
                      ENDIF
                      IF  .NOT. EMPTY(lcplano)
                         SELECT ucrsplanosdemautomaticos
                         GOTO TOP
                         LOCATE FOR ALLTRIM(ucrsplanosdemautomaticos.plano)==ALLTRIM(lcplano)
                         IF  .NOT. FOUND()
                            SELECT ucrsplanosdemautomaticos
                            APPEND BLANK
                            REPLACE ucrsplanosdemautomaticos.plano WITH lcplano
                         ENDIF
                      ENDIF
*!*	                      IF insnnordisk=.T.
*!*	                      	uf_atendimento_compart_nnordisk()
*!*	                      	
*!*	                         lcpos = RECNO("fi")
*!*	                         lclordem = fi.lordem
*!*	                         lclordem = VAL(LEFT(ALLTRIM(STR(lclordem)), 2)+'0000')
*!*	                         SELECT fi
*!*	                         GOTO TOP
*!*	                         SCAN
*!*	                         	&&fix NOVONORDISK
*!*	                         	&&up_receituario_dadosDoPlano
*!*	                            IF fi.lordem=lclordem

*!*									IF uf_atendimento_aplicaPlanoND(fi.fistamp)

*!*										SELECT ucrsatendcomp

*!*										REPLACE fi.design WITH '.[' + ALLTRIM(ucrsatendcomp.codigo) + '][' + ALLTRIM(ucrsatendcomp.cptorgabrev) + '] Receita:'+LEFT(ALLTRIM(ucrsverificarespostadem.receita_nr), 19)+" -"
*!*										REPLACE fi.tipor WITH IIF(EMPTY(fi.dem), 'RM', ucrsverificarespostadem.receita_tipo)

*!*										SELECT ft2
*!*										REPLACE ft2.u_receita WITH LEFT(ALLTRIM(ucrsverificarespostadem.receita_nr), 19)
*!*										atendimento.pgfdados.page3.btnreceita.label1.caption = LEFT(ALLTRIM(ucrsverificarespostadem.receita_nr), 19)

*!*									ELSE

*!*										RETURN .F.

*!*									ENDIF

*!*	                            ENDIF
*!*	                         ENDSCAN
*!*	                         SELECT fi
*!*	                         TRY
*!*	                            GOTO lcpos
*!*	                         CATCH
*!*	                         ENDTRY
*!*	                         SELECT fi
*!*	                      ENDIF
*!*	                      atendimento.mododem = .F.
                   ELSE
                      SELECT fi
                      IF ALLTRIM(lcref)<>"99999"
                         uf_atendimento_adicionalinhafi(.T., .T.)
                         REPLACE fi.ref WITH ALLTRIM(lcref)
                      ENDIF
                      REPLACE fi.dem WITH .T.
                      REPLACE fi.token WITH ucrsverificarespostadem.token
                      REPLACE fi.receita WITH ucrsverificarespostadem.receita_nr
                      REPLACE fi.id_validacaodem WITH ucrsverificarespostadem.id
                      REPLACE fi.tipor WITH ALLTRIM(ucrsverificarespostadem.receita_tipo)
                      lcdiplomacod = ucrsverificarespostadem.diploma_cod
                      lctipolinha = ucrsverificarespostadem.tipo_linha
                      lcrecm = ucrsverificarespostadem.recm
                      lcpais_migrante = ucrsverificarespostadem.pais_migrante
                      lctiporeceita = ucrsverificarespostadem.receita_tipo
                      lcnrreceita = ucrsverificarespostadem.receita_nr
                      lcToken = ucrsverificarespostadem.token
                      lcBonusId = ALLTRIM(ucrsverificarespostadem.bonusId)
                	  lcBonusTipo = ALLTRIM(ucrsverificarespostadem.bonusTipo)
                      lcplano = uf_atendimento_planoautomatico(lcdiplomacod, lctipolinha, lcrecm, lcpais_migrante, lctiporeceita, lcdemefr, lcnrreceita, lcToken)
               
                      IF  .NOT. EMPTY(ALLTRIM(ucrsverificarespostadem.excecao))
                         DO CASE
                            CASE UPPER(ALLTRIM(ucrsverificarespostadem.excecao))=="A"
                               SELECT fi
                               REPLACE fi.lobs3 WITH "A"
                            CASE UPPER(ALLTRIM(ucrsverificarespostadem.excecao))=="B"
                               SELECT fi
                               REPLACE fi.lobs3 WITH "B"
                            CASE UPPER(ALLTRIM(ucrsverificarespostadem.excecao))=="C"
                               SELECT fi
                               REPLACE fi.lobs3 WITH "C2"
                         ENDCASE
                      ENDIF
                      
         

                     SELECT fi
	                 if(USED("fi2"))
	                    UPDATE fi2 SET fi2.bonusTipo = ALLTRIM(ucrsverificarespostadem.bonusTipo) ,  fi2.bonusId=ALLTRIM(ucrsverificarespostadem.bonusId) , fi2.bonusDescr = ALLTRIM(ucrsverificarespostadem.bonusDescr), fi2.idEpisodio = ALLTRIM(ucrsverificarespostadem.id_episodio), fi2.idProcesso = ALLTRIM(ucrsverificarespostadem.id_processo) WHERE fi2.fistamp = ALLTRIM(fi.fistamp)
	                 ENDIF
                      
                      
                      IF  .NOT. EMPTY(ALLTRIM(ucrsverificarespostadem.diploma_cod))
                         SELECT fi
                         REPLACE fi.u_diploma WITH ALLTRIM(ucrsverificarespostadem.u_design)
                      ENDIF
                      IF ALLTRIM(lcref)=="99999"
                         REPLACE fi.manipulado WITH lcparafarmacia
                      ENDIF
                      IF  .NOT. EMPTY(lcplano)
                         SELECT ucrsplanosdemautomaticos
                         GOTO TOP
                         LOCATE FOR ALLTRIM(ucrsplanosdemautomaticos.plano)==ALLTRIM(lcplano)
                         IF  .NOT. FOUND()
                            SELECT ucrsplanosdemautomaticos
                            APPEND BLANK
                            REPLACE ucrsplanosdemautomaticos.plano WITH lcplano
                         ENDIF
                      ENDIF
                      uf_facturacao_eventoreffi(.T.)
                      uf_atendimento_fiiliq(.T.)
                      uf_atendimento_acttotaisft()
                   ENDIF
                ENDIF
             ENDIF
          ENDIF
        
	       	&& verifica descontos
			IF uf_gerais_getParameter("ADM0000000074","BOOL")
				IF USED("UCRSPESQST")
					SELECT UCRSPESQST
					IF UCRSPESQST.mfornec > 0 &&percentagem
						uf_Atendimento_alteraDesc(.t., UCRSPESQST.mfornec, UCRSPESQST.dataFimPromo, UCRSPESQST.dataIniPromo)
					ELSE
						IF UCRSPESQST.mfornec2 > 0 &&valor
							uf_atendimento_alteraDescVal(.t., UCRSPESQST.mfornec2, UCRSPESQST.dataFimPromo, UCRSPESQST.dataIniPromo)
						ENDIF 
					ENDIF
				ELSE
					LCSQL = ""
					TEXT TO lcsql TEXTMERGE NOSHOW
							exec up_stocks_procuraRef '<<ALLTRIM(fi.ref)>>', <<mysite_nr>>
			      	ENDTEXT
			       	IF uf_gerais_actgrelha("", "uCrsFindRef", lcsql) .AND. RECCOUNT()>0
						SELECT uCrsFindRef
						IF uCrsFindRef.mfornec > 0 &&percentagem
							uf_Atendimento_alteraDesc(.t., uCrsFindRef.mfornec, uCrsFindRef.dataFimPromo, uCrsFindRef.dataIniPromo)
						ELSE
							IF uCrsFindRef.mfornec2 > 0 &&valor
								uf_atendimento_alteraDescVal(.t., uCrsFindRef.mfornec2, uCrsFindRef.dataFimPromo, uCrsFindRef.dataIniPromo)
							ENDIF 
						ENDIF
					ENDIF 	
				ENDIF
			ENDIf
       
      
          if(USED("ucrsverificarespostadem"))
          	SELECT ucrsverificarespostadem
          endif 
       ENDSCAN
       
       IF USED("uCrsPlanosDemAutomaticos")
          LOCAL lcplanocombinado
          lcplanocombinado = .F.
          lcplanocombinado = uf_atendimento_eplanocombinado()
          SELECT ucrsplanosdemautomaticos
          COUNT TO lcplanosaplicar
          IF EMPTY(lcbackoffice)
             DO CASE
                CASE lcplanosaplicar==1 .OR.  .NOT. EMPTY(lcplanocombinado)
                   IF  .NOT. EMPTY(mydemsuspensa)
                      lcfilordemcab = astr(lclincab)
                   ELSE
                      SELECT fi
                      lcfilordemcab = LEFT(astr(fi.lordem), 2)+REPLICATE('0', LEN(astr(fi.lordem))-2)
                   ENDIF
                   SELECT ucrsplanosdemautomaticos
                   GOTO TOP
                   lcplano = ucrsplanosdemautomaticos.plano
                   SELECT fi
                   LOCATE FOR lordem==VAL(lcfilordemcab)
                   IF FOUND()
                      SELECT ucrsverificarespostadem
                      GOTO TOP
                      SELECT fi
                      REPLACE fi.design WITH STREXTRACT(ALLTRIM(fi.design), '.', ':', 1, 4)+LEFT(ALLTRIM(ucrsverificarespostadem.receita_nr), 19)+" -"+STREXTRACT(ALLTRIM(fi.design), ' -', '', 1, 2)
                      REPLACE fi.tipor WITH IIF(EMPTY(fi.dem), 'RM', lcdemtiporeceita)
                      REPLACE fi.szzstamp WITH lccodigomedico
                      SELECT ft2
                      REPLACE ft2.u_receita WITH LEFT(ALLTRIM(ucrsverificarespostadem.receita_nr), 19)
                      SELECT fi
**						IF (ALLTRIM(fi.ref)<>'') .AND. ALLTRIM(fi.ref)<>"5681903" .AND. ALLTRIM(fi.ref)<>"5771050") .OR. (AT("NOVONORDISK", fi.design)=0 .AND. ALLTRIM(fi.ref)=='')

					  IF ((ALLTRIM(fi.ref)<>'') .AND. ALLTRIM(fi.ref)<>"5681903" .AND. ALLTRIM(fi.ref)<>"5771050") .OR. (AT("NOVONORDISK", fi.design)=0 .AND. ALLTRIM(fi.ref)=='')
                         SELECT fi
                         uf_atendimento_aplicacompart(lcplano, .T., .F., .F., .T.)
                      ENDIF
                   ENDIF
                CASE lcplanosaplicar>1 .AND. EMPTY(lcplanocombinado)
                   SELECT fi
                   lcfilordemcab = LEFT(astr(fi.lordem), 2)+REPLICATE('0', LEN(astr(fi.lordem))-2)
                   SELECT fi
                   LOCATE FOR lordem==VAL(lcfilordemcab)
                   IF FOUND()
                      SELECT ucrsverificarespostadem
                      GOTO TOP
                      SELECT fi
                      REPLACE fi.design WITH STREXTRACT(fi.design, '.', ':', 1, 4)+LEFT(ucrsverificarespostadem.receita_nr, 19)+" -"+STREXTRACT(fi.design, ' -', '', 1, 2)
                      REPLACE fi.tipor WITH IIF(EMPTY(fi.dem), 'RM', lcdemtiporeceita)
                      REPLACE fi.szzstamp WITH lccodigomedico
                      SELECT ft2
                      REPLACE ft2.u_receita WITH LEFT(ALLTRIM(ucrsverificarespostadem.receita_nr), 19)
                      uf_selplanocomp_chama("uf_atendimento_aplicaCompart", "codigo", .T., .F., .T., lcfilordemcab)
                   ENDIF
                OTHERWISE
             ENDCASE
             insnnordisk = .F.
          ELSE
             SELECT ucrsverificarespostadem
             GOTO TOP
             SELECT ft2
             GOTO TOP
             UPDATE Ft2 SET token = ALLTRIM(ucrsverificarespostadem.token), u_receita = ALLTRIM(ucrsverificarespostadem.receita_nr)
             IF UPPER(ALLTRIM(ucrsverificarespostadem.receita_tipo))='RSP'
                SELECT ft
                UPDATE FT SET cdata = ucrsverificarespostadem.receita_data
             ENDIF
             DO CASE
                CASE lcplanosaplicar==1 .OR.  .NOT. EMPTY(lcplanocombinado)
                   LOCAL lcplanoauto
                   SELECT ucrsplanosdemautomaticos
                   GOTO TOP
                   lcplanoauto = ALLTRIM(ucrsplanosdemautomaticos.plano)
                   IF ( .NOT. EMPTY(lcplanocombinado))
                      SELECT ft2
                      lcplanoauto = uf_atendimento_retornaplanocombinado()
                   ENDIF
                   lcsql = ""
                   
                   TEXT TO lcsql TEXTMERGE NOSHOW
								select codigo, design, cptorgabrev FROM cptpla (nolock) WHERE codigo = '<<ALLTRIM(lcPlanoAuto)>>' AND inactivo = 0
                   ENDTEXT
                   IF  .NOT. uf_gerais_actgrelha("", 'uCrsAuxPlano', lcsql)
                      uf_perguntalt_chama("Ocorreu uma anomalia a verificar o plano de comparticipa��o a aplicar", "OK", "", 64)
                      RETURN .F.
                   ENDIF

                   SELECT ucrsauxplano
          
                   SELECT ft2
                   UPDATE Ft2 SET u_codigo = ALLTRIM(ucrsauxplano.codigo), u_design = ALLTRIM(ucrsauxplano.design), u_abrev = ALLTRIM(ucrsauxplano.cptorgabrev)
                   uf_facturacao_aplicaplanocomp(.T., .T.)
                   IF USED("uCrsAuxPlano")
                      fecha("uCrsAuxPlano")
                   ENDIF
                CASE lcplanosaplicar>1 .AND. EMPTY(lcplanocombinado)
                   uf_selplanocomp_chama("ft2", "u_codigo|u_design|u_abrev", "codigo|design|cptorgabrev", .T., .T.)
                   uf_facturacao_aplicaplanocomp(.T., .T.)
                OTHERWISE
             ENDCASE
             
         
             
             SELECT fi
             GOTO TOP
             SCAN
                IF EMPTY(fi.ref) .AND. EMPTY(fi.design) .AND. fi.qtt=0
                   SELECT fi
                   DELETE
                ENDIF
             ENDSCAN
              

             uf_atendimento_actvalorescab()
     
             uf_atendimento_infototaiscab()
          
             uf_atendimento_infototais()
                        
             &&na factura��o deve ir sempre para a primeira linha ap�s a consulta
             IF(TYPE("facturacao")!="U")
             	IF(USED("fi"))
             		SELECT  fi
             		GO TOP
             	ENDIF
             	facturacao.refresh
             ENDIF
				
 
          ENDIF
       ENDIF
       IF EMPTY(lcbackoffice)
          IF USED("uCrsAtendCompAuxMantemCodigos")
             IF UPPER(ALLTRIM(ucrsverificarespostadem.receita_tipo))=='RSP'
                UPDATE uCrsAtendComp FROM uCrsAtendComp INNER JOIN uCrsAtendCompAuxMantemCodigos ON ALLTRIM(ucrsatendcomp.fistamp)==ALLTRIM(ucrsatendcompauxmantemcodigos.fistamp) SET ucrsatendcomp.codacesso = ucrsatendcompauxmantemcodigos.codacesso, ucrsatendcomp.coddiropcao = ucrsatendcompauxmantemcodigos.coddiropcao, ucrsatendcomp.datareceita = lcdatareceita
             ENDIF
          ENDIF
       ENDIF
       IF UPPER(ALLTRIM(ucrsverificarespostadem.receita_tipo))=='RSP' .AND. (YEAR(ucrsatendcomp.datareceita)=1900 .OR. YEAR(ucrsatendcomp.datareceita)=0)       
          SELECT ucrsatendcomp
          REPLACE ucrsatendcomp.datareceita WITH lcdatareceita
       ENDIF
       IF EMPTY(lcbackoffice)
          regua(2)
          atendimento.tmrconsultadem.enabled = .F.
          atendimento.ntmrconsultadem = 0
          atendimento.stamptmrconsultadem = ''
       ELSE
          regua(2)
          facturacao.tmrconsultadem.enabled = .F.
          facturacao.ntmrconsultadem = 0
          facturacao.stamptmrconsultadem = ''
       ENDIF
       regua(2)
    ENDIF
 ENDIF
 SELECT fi
 usoureserv = .T.
 IF TYPE("myDemDispensada")<>"U"
    RELEASE mydemdispensada
 ENDIF
 regua(2)
 
 
ENDFUNC
**
FUNCTION uf_atendimento_planoabrangente
 LPARAMETERS lcplanovenda
 LOCAL lcplano
 STORE '' TO lcplano
 IF ( .NOT. USED("uCrsPlanosDemAutomaticos"))
    RETURN .F.
 ENDIF
 IF (EMPTY(lcplanovenda))
    lcplanovenda = ""
 ENDIF
 DIMENSION planoslistabase(3)
 planoslistabase(1) = '01'
 planoslistabase(2) = '46'
 planoslistabase(3) = '48'
 DIMENSION planoslistaabrangente(3)
 planoslistaabrangente(1) = '45'
 planoslistaabrangente(2) = '46A'
 planoslistaabrangente(3) = '48A'
 IF (RECCOUNT("uCrsPlanosDemAutomaticos")>0)
    FOR i = 1 TO 3
       LOCAL lcplanobase, lcplanoabrangente, lctemplanobase, lctemplanoabrangente
       LOCAL lcplanoabrangente
       lcplanobase = ALLTRIM(planoslistabase(i))
       lcplanoabrangente = ALLTRIM(planoslistaabrangente(i))
       lctemplanobase = .F.
       lctemplanoabrangente = .F.
       SELECT ucrsplanosdemautomaticos
       GOTO TOP
       LOCATE FOR ALLTRIM(ucrsplanosdemautomaticos.plano)==ALLTRIM(lcplanobase)
       IF FOUND()
          lctemplanobase = .T.
       ENDIF
       IF (ALLTRIM(lcplanobase)==ALLTRIM(lcplanovenda))
          lctemplanobase = .T.
       ENDIF
       SELECT ucrsplanosdemautomaticos
       GOTO TOP
       LOCATE FOR ALLTRIM(ucrsplanosdemautomaticos.plano)==ALLTRIM(lcplanoabrangente)
       IF FOUND()
          lctemplanoabrangente = .T.
       ENDIF
       IF (ALLTRIM(lcplanoabrangente)==ALLTRIM(lcplanovenda))
          lctemplanoabrangente = .T.
       ENDIF
       IF ( .NOT. EMPTY(lctemplanobase) .AND.  .NOT. EMPTY(lctemplanoabrangente))
          lcplano = ALLTRIM(lcplanoabrangente)
       ENDIF
    ENDFOR
 ENDIF
 RELEASE planoslistabase
 RELEASE planoslistaabrangente
 RETURN lcplano
ENDFUNC
**
FUNCTION uf_atendimentoretornaplanovenda
 IF ( .NOT. USED("Fi"))
    RETURN ""
 ENDIF
 IF ( .NOT. USED("uCrsAtendComp"))
    RETURN ""
 ENDIF
 SELECT fi
 lcposfi = RECNO("fi")
 SELECT fi
 lcfilordemcab = LEFT(astr(fi.lordem), 2)+REPLICATE('0', LEN(astr(fi.lordem))-2)
 SELECT fi
 LOCATE FOR lordem==VAL(lcfilordemcab)
 IF (USED("uCrsPlanoAux"))
    fecha("uCrsPlanoAux")
 ENDIF
 SELECT fi
 SELECT codigo FROM uCrsAtendComp WHERE UPPER(ALLTRIM(fistamp))==UPPER(ALLTRIM(fi.fistamp)) INTO CURSOR uCrsPlanoAux READWRITE
 SELECT ucrsplanoaux
 GOTO TOP
 lcplanovenda = ALLTRIM(ucrsplanoaux.codigo)
 IF (USED("uCrsPlanoAux"))
    fecha("uCrsPlanoAux")
 ENDIF
 SELECT fi
 TRY
    GOTO lcposfi
 CATCH
    GOTO TOP
 ENDTRY
 IF (EMPTY(lcplanovenda))
    lcplanovenda = ""
 ENDIF
 RETURN lcplanovenda
ENDFUNC
**
FUNCTION uf_atendimento_retornaplanocombinadoporplano
 LPARAMETERS lcplanooriginalacombinar
 LOCAL lcexiteplano, lcplanoauto
 lcplanoauto = ""
 lcexiteplano = .F.
 DIMENSION planoslista(9)
 planoslista(1) = '01'
 planoslista(2) = '45'
 planoslista(3) = '45A'
 planoslista(4) = '45B'
 planoslista(5) = '48'
 planoslista(6) = '48A'
 planoslista(7) = '41'
 planoslista(8) = 'LA'
 planoslista(9) = 'DV'
 SELECT ucrsplanostemp
 GOTO TOP
 LOCATE FOR ALLTRIM(ucrsplanostemp.plano)==ALLTRIM(lcplanooriginalacombinar)
 IF (FOUND())
    lcexiteplano = .T.
 ENDIF
 IF ( .NOT. EMPTY(lcexiteplano))
    FOR i = 1 TO ALEN(planoslista)
       LOCAL lcplanotemp
       lcplanotemp = ALLTRIM(planoslista(i))
       SELECT ucrsplanostemp
       GOTO TOP
       LOCATE FOR ALLTRIM(ucrsplanostemp.plano)==lcplanotemp
       IF (FOUND() .AND. lcexiteplano)
          lcplanoauto = lcplanotemp
       ENDIF
    ENDFOR
 ENDIF
 RELEASE planoslista
 RETURN lcplanoauto
ENDFUNC
**
FUNCTION uf_atendimento_retornaplanocombinado
 LPARAMETERS lcplanooriginal
 LOCAL lcposfi, lcplanolista, lcplanoauto, lclordem, lcsql, lcplanoinexistente
 lcplanoauto = ""
 lcsql = ""
 lcplanoinexistente = .F.
 IF ( .NOT. USED("uCrsVerificaRespostaDEMTotal"))
    RETURN ""
 ENDIF
 SELECT fi
 lcposfi = RECNO("fi")
 SELECT fi
 lclordem = LEFT(ALLTRIM(STR(fi.lordem)), 2)
 IF (USED("ucrsFiAux"))
    fecha("ucrsFiAux")
 ENDIF
 IF (USED("uCrsVerificaRespostaDEMTotalAux"))
    fecha("uCrsVerificaRespostaDEMTotalAux")
 ENDIF
 IF (USED("ucrsPlanoAutomaticoAux"))
    fecha("ucrsPlanoAutomaticoAux")
 ENDIF
 IF (TYPE("FACTURACAO")=="U")
    SELECT * FROM fi WHERE LEFT(ALLTRIM(STR(fi.lordem)), 2)=lclordem INTO CURSOR ucrsFiAux READWRITE
    SELECT * FROM uCrsVerificaRespostaDEMTotal WHERE sel=.T. INTO CURSOR uCrsVerificaRespostaDEMTotalAux READWRITE
 ELSE
    SELECT * FROM fi INTO CURSOR ucrsFiAux READWRITE
    SELECT * FROM uCrsVerificaRespostaDem INTO CURSOR uCrsVerificaRespostaDEMTotalAux READWRITE
 ENDIF
 IF (USED("ucrsPlanosTemp"))
    fecha("ucrsPlanosTemp")
 ENDIF
 CREATE CURSOR ucrsPlanosTemp (plano C(3))
 SELECT ucrsfiaux
 GOTO TOP
 SCAN
    SELECT ucrsverificarespostademtotalaux
    GOTO TOP
    SCAN
       IF (ALLTRIM(ucrsverificarespostademtotalaux.id)==ALLTRIM(ucrsfiaux.id_validacaodem))
          LOCAL lcplano, lcdiplomacod, lctipolinha, lcrecm, lcpais_migrante, lctiporeceita, lcplano
          lcdiplomacod = ALLTRIM(ucrsverificarespostademtotalaux.diploma_cod)
          lctipolinha = ALLTRIM(ucrsverificarespostademtotalaux.tipo_linha)
          lcrecm = ALLTRIM(ucrsverificarespostademtotalaux.recm)
          lcpais_migrante = ALLTRIM(ucrsverificarespostademtotalaux.pais_migrante)
          lctiporeceita = ALLTRIM(ucrsverificarespostademtotalaux.receita_tipo)
          lcdemefr = ALLTRIM(ucrsverificarespostademtotalaux.efr_cod)
          lcnrreceita = ucrsverificarespostadem.receita_nr
          lcToken = ucrsverificarespostadem.token
          lcplano = uf_atendimento_planoautomatico(lcdiplomacod, lctipolinha, lcrecm, lcpais_migrante, lctiporeceita, lcdemefr, lcnrreceita, lcToken)

          INSERT INTO ucrsPlanosTemp (plano) VALUES (lcplano)
       ENDIF
    ENDSCAN
    SELECT ucrsfiaux
 ENDSCAN
 IF ( .NOT. EMPTY(lcplanooriginal))
    INSERT INTO ucrsPlanosTemp (plano) VALUES (lcplanooriginal)
 ENDIF
 SELECT ucrsplanostemp
 GOTO TOP
 SCAN
    SELECT ucrsplanoscombinados
    GOTO TOP
    LOCATE FOR ALLTRIM(ucrsplanostemp.plano)==ALLTRIM(ucrsplanoscombinados.codigo)
    IF ( .NOT. FOUND())
       lcplanoinexistente = .T.
    ENDIF
 ENDSCAN
 lcplanoauto = uf_atendimento_retornaplanocombinadoporplano("DS")
 IF (EMPTY(lcplanoauto))
    lcplanoauto = uf_atendimento_retornaplanocombinadoporplano("CE")
 ENDIF
 IF (EMPTY(lcplanoauto))
    lcplanoauto = uf_atendimento_retornaplanocombinadoporplano("47")
 ENDIF
 IF (EMPTY(lcplanoauto))
    lcplanoauto = uf_atendimento_retornaplanocombinadoporplano("DO")
 ENDIF

 DIMENSION planoslistabase(11)
 planoslistabase(1) = '01'
 planoslistabase(2) = '01'
 planoslistabase(3) = '01'
 planoslistabase(4) = '45'
 planoslistabase(5) = '45'
 planoslistabase(6) = '46'
 planoslistabase(7) = '48'
 planoslistabase(8) = '48'
 planoslistabase(9) = '48'
 planoslistabase(10) = '48A'
 planoslistabase(11) = '48A'
 DIMENSION planoslistaabrangente(11)
 planoslistaabrangente(1) = '45'
 planoslistaabrangente(2) = '45A'
 planoslistaabrangente(3) = '45B'
 planoslistaabrangente(4) = '45A'
 planoslistaabrangente(5) = '45B'
 planoslistaabrangente(6) = '46A'
 planoslistaabrangente(7) = '48A'
 planoslistaabrangente(8) = '45A'
 planoslistaabrangente(9) = '45B'
 planoslistaabrangente(10) = '45B'
 planoslistaabrangente(11) = '45A'
 FOR i = 1 TO ALEN(planoslistaabrangente)
    LOCAL lcplanotemp, lctemplano, lctemplano, lctemplanoabrangente
    lcplanotemp = ALLTRIM(planoslistabase(i))
    lcplanoabrangetemp = ALLTRIM(planoslistaabrangente(i))
    lctemplano = .F.
    lctemplanoabrangente = .F.
    SELECT ucrsplanostemp
    GOTO TOP
    LOCATE FOR ALLTRIM(ucrsplanostemp.plano)==lcplanotemp
    IF (FOUND())
       lctemplano = .T.
    ENDIF
    SELECT ucrsplanostemp
    GOTO TOP
    LOCATE FOR ALLTRIM(ucrsplanostemp.plano)==lcplanoabrangetemp
    IF (FOUND())
       lctemplanoabrangente = .T.
    ENDIF
    IF ( .NOT. EMPTY(lctemplano) .AND.  .NOT. EMPTY(lctemplanoabrangente))
       lcplanoauto = lcplanoabrangetemp
    ENDIF
 ENDFOR
 SELECT fi
 TRY
    GOTO lcposfi
 CATCH
    GOTO TOP
 ENDTRY
 IF (USED("ucrsPlanoAutomaticoAux"))
    fecha("ucrsPlanoAutomaticoAux")
 ENDIF
 IF (USED("ucrsFiAux"))
    fecha("ucrsFiAux")
 ENDIF
 IF (USED("uCrsVerificaRespostaDEMTotalAux"))
    fecha("uCrsVerificaRespostaDEMTotalAux")
 ENDIF
 IF (USED("ucrsPlanosTemp"))
    fecha("ucrsPlanosTemp")
 ENDIF
 RELEASE planoslistabase
 RELEASE planoslistaabrangente
 IF  .NOT. EMPTY(lcplanoinexistente)
    lcplanoauto = ""
 ENDIF
 RETURN lcplanoauto
ENDFUNC
**
FUNCTION uf_atendimento_eplanocombinado
 LOCAL lcplanocombinado
 lcplanocombinado = ""
 IF ( .NOT. USED("uCrsPlanosDemAutomaticos"))
    RETURN .F.
 ENDIF
 IF ( .NOT. USED("uCrsPlanosCombinados"))
    RETURN .F.
 ENDIF
 IF (USED("fi"))
    lcplanocombinado = uf_atendimento_retornaplanocombinado()
 ENDIF
 IF (EMPTY(lcplanocombinado))
    RETURN .F.
 ELSE
    RETURN .T.
 ENDIF
ENDFUNC
**

FUNCTION uf_atendimento_escolhePlanoPEMH
	LPARAM lcToken
	LOCAL lcPlanoFinal 
	lcPlanoFinal = ""
	fecha("ucrsPlanoPEMH")	
    IF !uf_gerais_actGrelha("", "ucrsPlanoPEMH", "exec up_dem_planoPEMH'" + ALLTRIM(lcToken) + "'")
        lcPlanoFinal = ""
    ENDIF 
    if(USED("ucrsPlanoPEMH") AND RECCOUNT("ucrsPlanoPEMH")>0)
    	SELECT ucrsPlanoPEMH
    	GO TOP
    	lcPlanoFinal =  ALLTRIM(ucrsPlanoPEMH.plano)
    ENDIF
    
    fecha("ucrsPlanoPEMH")  
	RETURN  lcPlanoFinal 
	
ENDFUNC



FUNCTION uf_atendimento_planoautomatico
 LPARAMETERS lcdiplomacod, lctipolinha, lcrecm, lcpais_migrante, lctiporeceita, lcdemefr, lcnrreceita, lcToken

	
 LOCAL lcplanol, lcreflin
 lcplano = ""
 lcreflin = ""
 IF (USED("fi"))
    lcreflin = ALLTRIM(fi.ref)
 ENDIF

 uv_tipoRec = uf_gerais_devolveDescrDispensaEletronica(uf_gerais_retornaTipoDispensaEletronica(ALLTRIM(lcnrreceita)))
	

 DO CASE
 	&&SD-68009
 	CASE UPPER(ALLTRIM(uv_tipoRec)) = "PEMH"
 		 lcPlano = uf_atendimento_escolhePlanoPEMH(lcToken)
 	&&SD-68009
    CASE UPPER(ALLTRIM(uv_tipoRec)) = "MCDT"

        IF EMPTY(lcToken)
            lcPlano = ""
        ENDIF

        IF !uf_gerais_actGrelha("", "uc_planoMCDT", "exec up_dem_planoMCDT '" + ALLTRIM(lcToken) + "'")
            **uf_perguntalt_chama("Ocorreu uma anomalia a verificar o plano de comparticipa��o a aplicar", "OK", "", 64 )
            lcPlano = ""
        ENDIF

        lcPlano = ALLTRIM(uc_planoMCDT.plano)
	
    CASE ALLTRIM(lcreflin)=="5681903" OR  ALLTRIM(lcreflin)=="5771050"
       lcplano = uf_atendimento_escolhePlanoND()
    CASE ALLTRIM(lcdemefr)=="930003"
       lcplano = "41"
    CASE ALLTRIM(lcdemefr)=="999998"
       lcplano = "SU" 
    CASE ALLTRIM(lcdemefr)=="935626"
       lcplano = "DF"
    CASE ALLTRIM(lcdiplomacod)=="30"
       lcplano = "45B"
    CASE ALLTRIM(lcdiplomacod)=="22"
       lcplano = "45A"
    CASE ALLTRIM(lcdiplomacod)=="81"
       lcplano = "DV"
    CASE ALLTRIM(lcdiplomacod)=="83"
       lcplano = "45"
    CASE ALLTRIM(lctipolinha)=="LMM" .OR. ALLTRIM(lctipolinha)=="MM" .OR. ALLTRIM(lctipolinha)=="LMDT"
       lcplano = "47"
    &&CASE ALLTRIM(lctipolinha)=="LOUT" .OR. ALLTRIM(lctipolinha)=="OUT"
       &&lcplano = ""
    CASE (ALLTRIM(lctipolinha)=="LMDB" .OR. ALLTRIM(lctipolinha)=="MDB" .OR. ALLTRIM(lctiporeceita)=="MDB") .AND. ALLTRIM(lcdiplomacod)<>"100"
       lcplano = "DS"      
    CASE ALLTRIM(lctipolinha)=="LOST" .AND. (ALLTRIM(lcdiplomacod)="71" .OR. ALLTRIM(lcdiplomacod)="72")
       lcplano = "DO"
    CASE ALLTRIM(lctipolinha)=="LCE"
       lcplano = "CE"
    CASE (ALLTRIM(lcdiplomacod)=="65")
       lcplano = "LA"
    CASE lcpais_migrante<>"PT" .AND. EMPTY(ALLTRIM(lcdiplomacod)) .AND. lcrecm<>"N"
       lcplano = "48"
    CASE lcpais_migrante<>"PT" .AND.  .NOT. EMPTY(ALLTRIM(lcdiplomacod)) .AND. lcrecm<>"N"
       lcplano = "48A"
    CASE lcpais_migrante<>"PT" .AND. EMPTY(ALLTRIM(lcdiplomacod))
       lcplano = "46"
    CASE lcpais_migrante<>"PT" .AND.  .NOT. EMPTY(ALLTRIM(lcdiplomacod))
       lcplano = "46A"
    CASE lcrecm="N" .AND. (EMPTY(ALLTRIM(lcdiplomacod)) .OR. ALLTRIM(lcdiplomacod)=="63")
       lcplano = "01"
    CASE lcrecm="N" .AND.  .NOT. EMPTY(ALLTRIM(lcdiplomacod)) .AND. ALLTRIM(lcdiplomacod)<>"63"
       lcplano = "45"
    CASE lcrecm<>"N" .AND.  .NOT. EMPTY(ALLTRIM(lcdiplomacod)) .AND. ALLTRIM(lcdiplomacod)<>"63"
       lcplano = "48A"
    CASE lcrecm<>"N"
       lcplano = "48"
 ENDCASE

 
 RETURN lcplano
ENDFUNC
**
FUNCTION uf_atendimento_validacaoexepcoescompartreceitasdem
 IF ( .NOT. USED('uCrsAtendST'))
    RETURN .T.
 ENDIF
 IF ( .NOT. USED('ucrsDadosValidacaoDEMLinhas'))
    RETURN .T.
 ENDIF
 SELECT ucrsdadosvalidacaodemlinhas
 GOTO TOP
 SCAN
    LOCAL refl
    refl = ALLTRIM(ucrsdadosvalidacaodemlinhas.ref)
    IF ( .NOT. EMPTY(ALLTRIM(refl)))
       SELECT ucrsatendst
       GOTO TOP
       SCAN FOR ALLTRIM(ucrsatendst.ref)==refl
          IF (ALLTRIM(ucrsatendst.grupo)=='SO' .OR. ALLTRIM(ucrsatendst.grupo)=='DO')
             IF (VAL(ALLTRIM(ucrsdadosvalidacaodemlinhas.comp_sns))<>ucrsdadosvalidacaodemlinhas.retorno_comp_sns)
                uf_perguntalt_chama("Foram ultrapassados os limites estipulados na norma 26/2017 para comparticipa��o de dispositivos m�dicos do tipo DO (ostomia - incontin�ncia/reten��o urin�ria)."+CHR(13)+CHR(10)+"Por favor verifique.", "OK", "", 32)
                RETURN .F.
             ENDIF
          ENDIF
       ENDSCAN
    ENDIF
    SELECT ucrsdadosvalidacaodemlinhas
 ENDSCAN
 RETURN .T.
ENDFUNC
**
FUNCTION uf_atendimento_validacaoreceitasdem
 LPARAMETERS lcbackoffice, lcreceita, lccodacesso, lccoddiropcao, lctoken, lcreserva, lcrecautoreserva
 LOCAL lccodfarm, lccodigodireitoopcao, lcnomejar, lctestjar
 STORE "" TO lccodfarm, lccodigodireitoopcao, lcnomejar, lctestjar
 


 
 SELECT ucrse1
 IF ALLTRIM(ucrse1.tipoempresa)<>"FARMACIA"
    RETURN .T.
 ENDIF
 SELECT ucrse1
 IF EMPTY(ALLTRIM(ucrse1.u_codfarm))
    uf_perguntalt_chama("C�digo de Farm�cia n�o configurado. Contacte o suporte.", "OK", "", 32)
    RETURN .F.
 ENDIF
 SELECT ucrse1
 lccodfarm = ALLTRIM(ucrse1.u_codfarm)
 IF USED("ucrsReceitaValidar")
    fecha("ucrsReceitaValidar")
 ENDIF
 IF USED("ucrsReceitaEfetivarAp")
    fecha("ucrsReceitaEfetivarAp")
 ENDIF
 IF USED("ucrsReceitaEfetivar")
    fecha("ucrsReceitaEfetivar")
 ENDIF
 SELECT fi
 GOTO TOP
 SELECT DISTINCT qtt, token, receita, CAST("" AS C(12) ) AS resultado_validacao_cod, CAST("" AS C(200) ) AS resultado_validacao_descr,;
 CAST("" AS C(12) ) AS resultado_efetivacao_cod, CAST("" AS C(200) ) AS resultado_efetivacao_descr, CAST("" AS C(12) ) AS retorno_erro_cod,;
 CAST("" AS C(200) ) AS retorno_erro_descr, CAST("" AS C(19) ) AS receita_nr, CAST("" AS C(6) ) AS codacesso, CAST("" AS C(6) ) AS coddiropcao, CAST("" AS C(4) ) AS receita_tipo, CAST("" AS C(2) ) AS receita_psico;
 FROM fi WHERE  NOT EMPTY(fi.dem) AND  NOT EMPTY(ALLTRIM(fi.token)) AND fi.partes=0 AND LEFT(ALLTRIM(fi.design), 1)<>"." INTO CURSOR ucrsReceitaValidar READWRITE
 
 
 IF  .NOT. EMPTY(lcbackoffice)
    IF RECCOUNT("dadosPsico")>0
       REPLACE ucrsreceitavalidar.receita_psico WITH "LE"
    ENDIF
 ELSE
    IF RECCOUNT("dadosPsico")>0
       SELECT dadospsico
       GOTO TOP
       SCAN
          SELECT ucrscabvendas
          GOTO TOP
          SCAN FOR ALLTRIM(STR(ucrscabvendas.lordem))=ALLTRIM(STR(dadospsico.cabvendasordem))
             SELECT ucrsreceitavalidar
             GOTO TOP
             LOCATE FOR ALLTRIM(ucrscabvendas.token)==ALLTRIM(ucrsreceitavalidar.token)
             IF FOUND()
                REPLACE ucrsreceitavalidar.receita_psico WITH "LE"
             ENDIF
          ENDSCAN
       ENDSCAN
    ENDIF
 ENDIF
 IF USED("uCrsCabVendas")
    SELECT ucrscabvendas
    GOTO TOP
    SCAN
       IF  .NOT. EMPTY(ucrscabvendas.mantemreceita)
          SELECT ucrsreceitavalidar
          GOTO TOP
          LOCATE FOR ALLTRIM(ucrscabvendas.token)==ALLTRIM(ucrsreceitavalidar.token)
          IF FOUND()
             SELECT ucrsreceitavalidar
             DELETE
          ENDIF
       ENDIF
    ENDSCAN
 ENDIF
 IF EMPTY(lcreserva)
    IF USED("uCrsCabVendas")
       SELECT ucrscabvendas
       GOTO TOP
       SCAN FOR (RIGHT(ALLTRIM(ucrscabvendas.design), 18)=="*SUSPENSA RESERVA*") .AND.  .NOT. EMPTY(ALLTRIM(ucrscabvendas.token))
          SELECT ucrsreceitavalidar
          GOTO TOP
          LOCATE FOR ALLTRIM(ucrscabvendas.token)==ALLTRIM(ucrsreceitavalidar.token)
          IF FOUND()
          	 
          	 uf_gerais_registaerrolog("Apaga valida��o da receita " +ALLTRIM(ucrsreceitavalidar.receita), "M99")
             SELECT ucrsreceitavalidar
             DELETE
          ENDIF
       ENDSCAN
    ENDIF
 ENDIF
 IF USED("ucrsCabVendas")
    SELECT ucrscabvendas
    GOTO TOP
    SCAN FOR ucrscabvendas.fact==.T. .AND.  .NOT. EMPTY(ALLTRIM(ucrscabvendas.u_ltstamp))==.T.
       SELECT ucrsreceitavalidar
       GOTO TOP
       SELECT ucrsreceitavalidar
       SCAN FOR ALLTRIM(ucrsreceitavalidar.receita)==ALLTRIM(STREXTRACT(ucrscabvendas.design, ':', ' -', 1, 0))
       
       	  uf_gerais_registaerrolog("Apaga valida��o da receita " +ALLTRIM(ucrsreceitavalidar.receita), "M100")
          SELECT ucrsreceitavalidar
          DELETE
              
       ENDSCAN
    ENDSCAN
    SELECT ucrsreceitavalidar
    GOTO TOP
 ENDIF
 
 

 
 
 IF  .NOT. EMPTY(lcrecautoreserva)
    SELECT * FROM ucrsReceitaValidar INTO CURSOR ucrsReceitaValidarAux READWRITE
    LOCAL lcauxlcrecautoreserva
    STORE 'xxxyyyzzz' TO lcauxlcrecautoreserva
    SELECT ucrsreservabi
    GOTO TOP
    SCAN
       IF  .NOT. EMPTY(ALLTRIM(ucrsreservabi.token)) .AND.  .NOT. EMPTY(ucrsreservabi.sel) .AND. LEFT(astr(ucrsreservabi.lordem), 2)<>lcauxlcrecautoreserva
          lcauxlcrecautoreserva = LEFT(astr(ucrsreservabi.lordem), 2)
          SELECT ucrscabvendas
          LOCATE FOR LEFT(astr(ucrscabvendas.lordem), 2)==lcauxlcrecautoreserva
          IF FOUND()
             SELECT ucrsreceitavalidaraux
             GOTO TOP
             SCAN FOR ALLTRIM(ucrsreceitavalidaraux.receita)==ALLTRIM(STREXTRACT(ucrscabvendas.design, ':', ' -', 1, 0))
             ENDSCAN
          ENDIF
       ENDIF
       SELECT ucrsreservabi
    ENDSCAN
    SELECT ucrsreceitavalidaraux
    GOTO TOP
    SCAN
       SELECT ucrsreceitavalidar
       GOTO TOP
       SELECT ucrsreceitavalidar
       SCAN FOR ALLTRIM(ucrsreceitavalidar.receita)==ALLTRIM(ucrsreceitavalidaraux.receita)
       ENDSCAN
       SELECT ucrsreceitavalidaraux
    ENDSCAN
    IF USED("ucrsReceitaValidarAux")
       fecha("ucrsReceitaValidarAux")
    ENDIF
 ENDIF
 IF EMPTY(lcbackoffice)
    SELECT ucrsreceitavalidar
    GOTO TOP
    SCAN
       SELECT codacesso, coddiropcao FROM uCrsCabVendas WHERE ALLTRIM(STREXTRACT(ucrscabvendas.design, ':', ' -', 1, 0))==ALLTRIM(ucrsreceitavalidar.receita) INTO CURSOR ucrsDadosDemTemp READWRITE
       SELECT ucrsdadosdemtemp
       SELECT ucrsreceitavalidar
       REPLACE ucrsreceitavalidar.codacesso WITH ucrsdadosdemtemp.codacesso
       REPLACE ucrsreceitavalidar.coddiropcao WITH ucrsdadosdemtemp.coddiropcao
    ENDSCAN
 ENDIF
 IF  .NOT. EMPTY(lcbackoffice)
    UPDATE uCrsReceitaValidar SET token = lctoken, receita_nr = lcreceita, codacesso = lccodacesso, coddiropcao = lccoddiropcao
 ENDIF
 IF RECCOUNT("ucrsReceitaValidar")==0
    RETURN .T.
 ENDIF
 SELECT ucrsreceitavalidar
 GOTO TOP
 SCAN
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			UPDATE Dispensa_Eletronica SET resultado_validacao_cod = null, resultado_validacao_descr = null, resultado_efetivacao_cod = null, resultado_efetivacao_descr = null WHERE token = '<<ALLTRIM(ucrsReceitaValidar.token)>>'
			DELETE from Dispensa_Eletronica_DD WHERE token = '<<ALLTRIM(ucrsReceitaValidar.token)>>'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
       uf_perguntalt_chama("N�o foi possivel proceder ao servi�o de Valida��o da Dispensa. Contacte o suporte.", "OK", "", 32)
       RETURN .F.
    ENDIF
 ENDSCAN
 SELECT fi
 GOTO TOP
 SCAN FOR  .NOT. EMPTY(fi.dem) .AND.  .NOT. EMPTY(ALLTRIM(fi.ref))
    LOCAL lcfistamp, lcjusttecdesc, lcjustteccod, lcValidade
    STORE '' TO lcjusttecdesc, lcjustteccod
    lcfistamp = ALLTRIM(fi.fistamp)
    SELECT fi2
    GOTO TOP
    SCAN FOR  .NOT. EMPTY(fi2.fistamp) .AND. fi2.fistamp=lcfistamp
       lcjusttecdesc = ALLTRIM(fi2.justificacao_tecnica_descr)
       lcjustteccod = ALLTRIM(fi2.justificacao_tecnica_cod)
       lcValidade = fi2.dataValidade
    ENDSCAN
    
    
    if(EMPTY(lcValidade ))
    	lcValidade  = "19000101"
    ENDIF
    
    lcValidade  = uf_gerais_getdate(lcValidade,"SQL")
    
    SELECT fi
    &&FOR i = 1 TO fi.qtt
    	
    	

   
    
       TEXT TO lcsql TEXTMERGE NOSHOW
				insert into Dispensa_Eletronica_DD (
					token
					,id
					,ref
					,pvp
					,pref
					,pvp5
					,comp_sns
					,comp_sns_tx
					,comp_diploma_tx
					,retorno_erro_cod
					,justificacao_tecnica_cod
					,justificacao_tecnica_descr
					,comp_sns_old
					,comp_sns_tx_old
					,comp_diploma_tx_old
					,qtt
					,lote
					,dataValidade
				)values(
					'<<ALLTRIM(fi.token)>>'
					,'<<ALLTRIM(fi.id_validacaoDem)>>'
					,'<<ALLTRIM(IIF(fi.manipulado,"99999",fi.ref))>>'
					,<<fi.u_epvp>>
					,<<fi.u_epref>>
					,<<fi.pvpmaxre>>
					,<<IIF(fi.comp_tipo_2==1,fi.u_ettent2,fi.u_ettent1)>>
					,<<IIF(fi.comp_tipo_2==1,fi.comp_2,fi.comp)>>
					,<<IIF(fi.comp_tipo_2==1,fi.comp_diploma_2,fi.comp_diploma)>>
					,null
					,'<<lcJustTecCod>>'
					,'<<lcJustTecDesc>>'
					,<<IIF(fi.comp_tipo_2==1,fi.u_ettent2,fi.u_ettent1)>>
					,<<IIF(fi.comp_tipo_2==1,fi.comp_2,fi.comp)>>
					,<<IIF(fi.comp_tipo_2==1,fi.comp_diploma_2,fi.comp_diploma)>>
					,<<fi.qtt>>
					,'<<ALLTRIM(fi.lote)>>'
					,'<<lcValidade>>'
				)	
       ENDTEXT
       
      
     
       
       IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
          uf_perguntalt_chama("N�o foi possivel proceder ao servi�o de Valida��o da Dispensa. Contacte o suporte.", "OK", "", 32)
          RETURN .F.
       ENDIF
  &&  ENDFOR
 ENDSCAN
 

 
 regua(0, RECCOUNT("uCrsReceitaValidar")+2, "A validar receitas no Sistema Central de Prescri��es...")
 regua(1, 1, "A validar receitas no Sistema Central de Prescri��es...")
 SELECT ucrsreceitavalidar
 GOTO TOP
 SCAN
    regua(1, RECNO(), "A validar receitas no Sistema Central de Prescri��es...")
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			select chave_pedido_relacionado from Dispensa_Eletronica (nolock) where token = '<<ALLTRIM(ucrsReceitaValidar.token)>>'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "ucrsDadosLigacaoDEM", lcsql)
       uf_perguntalt_chama("N�o foi possivel proceder ao servi�o de Efetiva��o da Dispensa. Contacte o suporte.", "OK", "", 32)
       regua(2)
       RETURN .F.
    ENDIF
    SELECT ucrsdadosligacaodem
    lcchavepedidorelacionado = ALLTRIM(ucrsdadosligacaodem.chave_pedido_relacionado)
    IF  .NOT. USED("ucrsServerName")
       TEXT TO lcsql TEXTMERGE NOSHOW
				Select @@SERVERNAME as dbServer
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "ucrsServerName", lcsql)
          uf_perguntalt_chama("N�o foi possivel proceder ao servi�o de Efetiva��o da Dispensa. Contacte o suporte.", "OK", "", 32)
          regua(2)
          RETURN .F.
       ENDIF
    ENDIF
    SELECT ucrsservername
    lcdbserver = ALLTRIM(ucrsservername.dbserver)
    lcnomejar = "LtsDispensaEletronica.jar"
    IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.F.
       lctestjar = "0"
    ELSE
       lctestjar = "1"
    ENDIF
    
    
    LOCAL lcTipo, lcComando
    
    lcTipo= uf_gerais_devolveDescrDispensaEletronica(uf_gerais_retornaTipoDispensaEletronica(ALLTRIM(ucrsreceitavalidar.receita)))
  
    lcComando= IIF(UPPER(ALLTRIM(lcTipo))= "MCDT", "VALIDACAO_MSP", "VALIDACAO")
    IF !uf_gerais_compStr(lcTipo,"MCDT") and !uf_gerais_compStr(lcTipo,"DEM") 
    	lcComando= "VALIDACAO_" + lcTipo
    ENDIF


    
    
    lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\DEM\'+ALLTRIM(lcnomejar)+' ' + ALLTRIM(lcComando) + ' --iniciais=' + ALLTRIM(m_chinis) +' --token='+ALLTRIM(ucrsreceitavalidar.token)+' --chavePedidoRelacionado='+ALLTRIM(lcchavepedidorelacionado)+' --nrReceita='+IIF(EMPTY(lcbackoffice), ALLTRIM(ucrsreceitavalidar.receita), IIF(EMPTY(lcreceita), "", lcreceita))+' --codFarmacia='+ALLTRIM(lccodfarm)+' --pinDirOpcao='+IIF(EMPTY(lcbackoffice), ALLTRIM(ucrsreceitavalidar.coddiropcao), IIF(EMPTY(lccoddiropcao), "", lccoddiropcao))+' --dbServer='+ALLTRIM(lcdbserver)+' --dbName='+UPPER(ALLTRIM(sql_db))+' --site='+UPPER(ALLTRIM(STRTRAN(ALLTRIM(mysite), ' ', '_')))+' --test='+lctestjar
    IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.T.
       _CLIPTEXT = lcwspath
       uf_perguntalt_chama("Webservice de teste...", "OK", "", 64)
    ENDIF
    lcwspath = "javaw -jar "+myEncodingFileJar+lcwspath
    owsshell = CREATEOBJECT("WScript.Shell")
    owsshell.run(lcwspath, 1, .T.)
    regua(1, RECNO(), "A validar resposta do Sistema Central de Prescri��es...")
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_dem_resultadoValidacao '<<ALLTRIM(ucrsReceitaValidar.token)>>'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "ucrsDadosValidacaoDEM", lcsql)
       uf_perguntalt_chama("N�o foi possivel verificar dados de valida��o da receita. Contacte o suporte.", "OK", "", 32)
       regua(2)
       RETURN .F.
    ENDIF
    SELECT ucrsdadosvalidacaodem
    IF RECCOUNT("ucrsDadosValidacaoDEM")>0
       SELECT ucrsreceitavalidar
       REPLACE ucrsreceitavalidar.resultado_validacao_cod WITH ucrsdadosvalidacaodem.resultado_validacao_cod
       REPLACE ucrsreceitavalidar.resultado_validacao_descr WITH ucrsdadosvalidacaodem.resultado_validacao_descr
       REPLACE ucrsreceitavalidar.receita_nr WITH ALLTRIM(ucrsdadosvalidacaodem.receita_nr)
       REPLACE ucrsreceitavalidar.receita_tipo WITH ALLTRIM(ucrsdadosvalidacaodem.receita_tipo)
    ENDIF
    IF USED("ucrsDadosValidacaoDEM")
       fecha("ucrsDadosValidacaoDEM")
    ENDIF
    SELECT ucrsreceitavalidar
 ENDSCAN
 regua(1, RECCOUNT("ucrsReceitaValidar")+1, "A validar receitas no Sistema Central de Prescri��es...")
 
 &&SELECT *, "" AS resultado_comprovativo_registo FROM uCrsReceitaValidar WHERE ALLTRIM(ucrsreceitavalidar.resultado_validacao_cod)=="100003020001" INTO CURSOR ucrsReceitaEfetivar READWRITE
 SELECT *, "" AS resultado_comprovativo_registo, "100003050099" as  resultado_anulacao_cod FROM uCrsReceitaValidar WHERE ALLTRIM(ucrsreceitavalidar.resultado_validacao_cod)=="100003020001" INTO CURSOR ucrsReceitaEfetivar READWRITE
 regua(1, RECCOUNT("ucrsReceitaValidar")+2, "A validar receitas no Sistema Central de Prescri��es...")
 IF (USED("fi"))
    IF TYPE("fi.codigoDemResposta")<>'U'
       UPDATE fi SET codigodemresposta = '', tipoerro = 0
    ENDIF
    SELECT fi
    GOTO TOP
 ENDIF
 

 
 SELECT ucrsreceitavalidar
 GOTO TOP
 SCAN
    IF ALLTRIM(ucrsreceitavalidar.resultado_validacao_cod)<>"100003020001"
       EXIT
    ENDIF
    SELECT id_validacaodem, .F. AS temresposta FROM fi WHERE fi.receita=ucrsreceitavalidar.receita AND LEFT(ALLTRIM(fi.design), 1)<>"." INTO CURSOR ucrsReceitaValidarLinhas READWRITE
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_dem_resultadoValidacaoLinhas '<<ALLTRIM(ucrsReceitaValidar.token)>>'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "ucrsDadosValidacaoDEMLinhas", lcsql)
       uf_perguntalt_chama("N�o foi possivel verificar dados de valida��o da receita. Contacte o suporte.", "OK", "", 32)
       regua(2)
       RETURN .F.
    ENDIF
    UPDATE ucrsReceitaValidarLinhas FROM ucrsReceitaValidarLinhas INNER JOIN ucrsDadosValidacaoDEMLinhas ON ucrsreceitavalidarlinhas.id_validacaodem==ucrsdadosvalidacaodemlinhas.id SET temresposta = .T. WHERE ISNULL(ucrsdadosvalidacaodemlinhas.retorno_erro_cod)==.F. .OR. ISNULL(ucrsdadosvalidacaodemlinhas.retorno_info_prestacao)==.F.
    SELECT ucrsdadosvalidacaodemlinhas
    GOTO TOP
    SCAN

       IF !ISNULL(ucrsdadosvalidacaodemlinhas.retorno_erro_cod) AND !EMPTY(ALLTRIM(ucrsdadosvalidacaodemlinhas.retorno_erro_cod))
       	  
          LOCAL id_validacaodem
          id_validacaodem = ''
          id_validacaodem = ALLTRIM(ucrsdadosvalidacaodemlinhas.id)
          IF  .NOT. ISNULL(ucrsdadosvalidacaodemlinhas.retorno_erro_cod)
             REPLACE ucrsreceitavalidar.retorno_erro_cod WITH ALLTRIM(ucrsdadosvalidacaodemlinhas.retorno_erro_cod) IN ucrsreceitavalidar
          ENDIF
          REPLACE ucrsreceitavalidar.retorno_erro_descr WITH ALLTRIM(ucrsreceitavalidar.retorno_erro_descr)+CHR(13)+CHR(10)+ALLTRIM(ucrsdadosvalidacaodemlinhas.retorno_erro_descr) IN ucrsreceitavalidar
          uf_atendimento_validacaosaveerrosfi(id_validacaodem, ucrsreceitavalidar.retorno_erro_cod)
       ELSE
    	  ** se valida comparts e se n�o vou buscar o valor ao estado
          IF uf_gerais_getparameter("ADM0000000304", "BOOL")==.T. AND uf_gerais_getparameter_site("ADM0000000230", "BOOL",mysite)==.F.
          
             LOCAL lcvalidadiploma
             lcvalidadiploma = .F.
             IF (ucrsdadosvalidacaodemlinhas.retorno_comp_diploma_tx>0)
                lcvalidadiploma = .T.
             ENDIF
             IF (EMPTY(lcvalidadiploma))

                IF (ROUND(VAL(ALLTRIM(ucrsdadosvalidacaodemlinhas.retorno_comp_sns_tx)), 2)<>ucrsdadosvalidacaodemlinhas.comp_sns_tx)
                   IF ( .NOT. uf_atendimento_validaexcecaocompart(ALLTRIM(ucrsdadosvalidacaodemlinhas.ref), ucrsdadosvalidacaodemlinhas.token, ucrsdadosvalidacaodemlinhas.id))
                      REPLACE ucrsreceitavalidar.retorno_erro_cod WITH ALLTRIM("999999999999") IN ucrsreceitavalidar
                      REPLACE ucrsreceitavalidar.retorno_erro_descr WITH "Retorno/Comparticipa��o Errada."+CHR(13)+CHR(10)+"Cnp: "+ucrsdadosvalidacaodemlinhas.ref IN ucrsreceitavalidar
                   ENDIF
                ENDIF
             ELSE
            	
                IF (ucrsdadosvalidacaodemlinhas.comp_diploma_tx<>ucrsdadosvalidacaodemlinhas.retorno_comp_diploma_tx)
                   IF ( .NOT. uf_atendimento_validaexcecaocompart(ALLTRIM(ucrsdadosvalidacaodemlinhas.ref), ucrsdadosvalidacaodemlinhas.token, ucrsdadosvalidacaodemlinhas.id))
                      REPLACE ucrsreceitavalidar.retorno_erro_cod WITH ALLTRIM("999999999999") IN ucrsreceitavalidar
                      REPLACE ucrsreceitavalidar.retorno_erro_descr WITH "Retorno/Comparticipa��o Errada."+CHR(13)+CHR(10)+"Cnp: "+ucrsdadosvalidacaodemlinhas.ref IN ucrsreceitavalidar
                   ENDIF
                ENDIF
             ENDIF
          ENDIF
       ENDIF
       SELECT ucrsdadosvalidacaodemlinhas
    ENDSCAN
    SELECT ucrsreceitavalidar
 ENDSCAN
 IF USED("ucrsReceitaValidarLinhas")
    fecha("ucrsReceitaValidarLinhas")
 ENDIF
 IF USED("ucrsDadosValidacaoDEMLinhas")
    fecha("ucrsDadosValidacaoDEMLinhas")
 ENDIF
 regua(2)
	

 uf_atendimento_forca_recalculo_compart()
 RETURN .T.
ENDFUNC

FUNCTION uf_atendimento_forca_recalculo_compart
	 **COMPART AUTO DEM

	 IF (TYPE("atendimento")<>"U")
	 	uf_eventoactvalores()
	 ENDIF
	 IF (TYPE("facturacao")<>"U")
	 	uf_facturacao_aplicaplanocomp(.T., .F.)
	 ENDIF
ENDFUNC


**
FUNCTION uf_atendimento_validaexcecaocompart
 LPARAMETERS lcref, lctoken, lcid
 LOCAL temexececao, lcdiploma_cod, lcBonusId, lcQtt, lcRecmId 
 lcRecmId = ''
 lcdiploma_cod = ''
 lcBonusId = ''
 lcQtt = 1
 temexececao = .F.
 IF USED("ucrsProdExcecoes")
    fecha("ucrsProdExcecoes")
 ENDIF
 lcsqlcod = ''
 TEXT TO lcsqlcod TEXTMERGE NOSHOW
		SELECT TOP 1 ISNULL(diploma_cod,'') AS diploma_cod, ISNULL(bonusId,'') as bonusId FROM  Dispensa_Eletronica_D (nolock) WHERE token='<<ALLTRIM(lctoken)>>' AND id= '<<ALLTRIM(lcid)>>'
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "ucrsDiplomaCod", lcsqlcod)
    uf_perguntalt_chama("N�o foi possivel Verificar dados de exece��es (diploma) . Contacte o suporte.", "OK", "", 32)
    regua(2)
    RETURN .F.
 ELSE
    lcdiploma_cod = ucrsdiplomacod.diploma_cod
    lcBonusId   = ucrsdiplomacod.bonusId

 ENDIF
 

 
 if(USED("ucrsAtendCl"))
  	IF type("ucrsAtendCl.recmId")<>"U"
 		SELECT ucrsAtendCl
 		lcRecmId = ALLTRIM(ucrsAtendCl.recmId)
 	ENDIF
 ENDIF
 

 
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_gerais_dadosExcecoes 1,'<<ALLTRIM(lcRef)>>', '','<<ALLTRIM(lcdiploma_cod)>>','<<ALLTRIM(lcBonusId)>>','<<ALLTRIM(lcRecmId)>>'
 ENDTEXT
 

 IF  .NOT. uf_gerais_actgrelha("", "ucrsProdExcecaoes", lcsql)
    uf_perguntalt_chama("N�o foi possivel verificar dados de exece��es. Contacte o suporte.", "OK", "", 32)
    regua(2)
    RETURN .F.
 ENDIF
 IF (ucrsprodexcecaoes.no>0)
    temexececao = .T.
 ELSE
    temexececao = .F.
 ENDIF
 IF USED("ucrsProdExcecoes")
    fecha("ucrsProdExcecoes")
 ENDIF
 RETURN temexececao
ENDFUNC
**
FUNCTION uf_atendimento_marcalinhasefectivadas
 LPARAMETERS lccursor
 LOCAL lcposfi
 IF ( .NOT. USED("fi"))
    RETURN .F.
 ENDIF
 IF ( .NOT. USED("ucrsReceitaEfetivar"))
    RETURN .F.
 ENDIF
 lcposfi = RECNO("fi")
 SELECT ucrsreceitaefetivar
 GOTO TOP
 SCAN
    SELECT fi
    GOTO TOP
    LOCATE FOR ALLTRIM(ucrsreceitaefetivar.token)==ALLTRIM(fi.token) .AND. ALLTRIM(ucrsreceitaefetivar.resultado_efetivacao_cod)=="100003040001"  AND  ALLTRIM(ucrsreceitaefetivar.resultado_efetivacao_cod)=="10200100001" .AND.  !EMPTY(ALLTRIM(ucrsreceitaefetivar.resultado_comprovativo_registo))
    IF (FOUND())
       INSERT INTO ucrsDemEfetivada (token) VALUES (ALLTRIM(ucrsreceitaefetivar.token))
    ENDIF
    SELECT ucrsreceitaefetivar
 ENDSCAN
 SELECT fi
 TRY
    GOTO lcposfi
 CATCH
    GOTO TOP
 ENDTRY
ENDFUNC
**
FUNCTION uf_atendimento_receitasefetivar
 LPARAMETERS lcbackoffice, lcreceita, lccodacesso, lccoddiropcao, lctoken
 LOCAL lcnomejar, lctestjar
 STORE '' TO lcnomejar, lctestjar
 

 
 
 SELECT ucrse1
 IF ALLTRIM(ucrse1.tipoempresa)<>"FARMACIA"
    RETURN .T.
 ENDIF
 SELECT ucrse1
 IF EMPTY(ALLTRIM(ucrse1.u_codfarm))
    uf_perguntalt_chama("C�digo de Farm�cia n�o configurado. Contacte o suporte.", "OK", "", 32)
    RETURN .F.
 ENDIF
 SELECT ucrse1
 lccodfarm = ALLTRIM(ucrse1.u_codfarm)
 IF  .NOT. USED("ucrsReceitaEfetivar")
    RETURN .T.
 ENDIF
 

 IF RECCOUNT("ucrsReceitaEfetivar")==0
    RETURN .T.
 ENDIF
 IF EMPTY(lcbackoffice)
    SELECT ucrscabvendas
    GOTO TOP
    SCAN
    	
       IF (ucrscabvendas.partes==1) .OR. (RIGHT(ALLTRIM(ucrscabvendas.design), 10)=="*SUSPENSA*") .OR. (RIGHT(ALLTRIM(ucrscabvendas.design), 18)=="*SUSPENSA RESERVA*")
          IF lcresad100=.F. .AND. lcressad=.F. .AND. lcresadparc=.F.
             DELETE FROM uCrsReceitaEfetivar WHERE UPPER(ALLTRIM(ucrsreceitaefetivar.token))==UPPER(ALLTRIM(ucrscabvendas.token)) AND !EMPTY(ALLTRIM(ucrsreceitaefetivar.token))
          ENDIF
       ENDIF
    ENDSCAN
 ENDIF

 
 regua(0, RECCOUNT("ucrsReceitaEfetivar")+2, "A registar receitas no Sistema Central de Prescri��es...")
 regua(1, 1, "A registar receitas no Sistema Central de Prescri��es...")
 
 
 IF EMPTY(lcbackoffice)
    UPDATE uCrsAtendComp FROM uCrsAtendComp INNER JOIN fi ON fi.fistamp=ucrsatendcomp.fistamp SET ucrsatendcomp.token = fi.token
    UPDATE ucrsReceitaEfetivar FROM ucrsReceitaEfetivar INNER JOIN uCrsAtendComp ON ucrsreceitaefetivar.token=ucrsatendcomp.token SET ucrsreceitaefetivar.codacesso = ucrsatendcomp.codacesso
 ENDIF
 SELECT ucrsreceitaefetivar
 GOTO TOP
 &&se ainda n�o foi efectivada ou se anulada com sucesso, evitar dupla efectivacao, &&TODO 100003050001
 &&  SCAN FOR EMPTY(ucrsreceitaefetivar.resultado_comprovativo_registo) OR  ALLTRIM(ucrsreceitaefetivar.resultado_efetivacao_cod)=='100003050001'   
 SCAN FOR EMPTY(ucrsreceitaefetivar.resultado_comprovativo_registo) OR  ALLTRIM(ucrsreceitaefetivar.resultado_anulacao_cod)=='100003050001'  
    IF EMPTY(ucrsreceitaefetivar.receita_nr) .OR. EMPTY(ucrsreceitaefetivar.codacesso)
       uf_perguntalt_chama("O c�digo de dispensa da receita n�o est� corretamente preenchido. Por favor refa�a o atendimento. Obrigado.", "OK", "", 32)
       regua(2)
       RETURN .F.
    ENDIF
    SELECT ucrsdadosligacaodem
    lcchavepedidorelacionado = ALLTRIM(ucrsdadosligacaodem.chave_pedido_relacionado)
    IF  .NOT. USED("ucrsServerName")
       TEXT TO lcsql TEXTMERGE NOSHOW
				Select @@SERVERNAME as dbServer
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "ucrsServerName", lcsql)
          uf_perguntalt_chama("N�o foi possivel proceder ao servi�o de Efetiva��o da Dispensa. Contacte o suporte.", "OK", "", 32)
          regua(2)
          RETURN .F.
       ENDIF
    ENDIF
    SELECT ucrsservername
    lcdbserver = ALLTRIM(ucrsservername.dbserver)
    lcnomejar = "LtsDispensaEletronica.jar"
    IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.F.
       lctestjar = "0"
    ELSE
       lctestjar = "1"
    ENDIF
    
    LOCAL lcTipo, lcComando
   
    lcTipo= uf_gerais_devolveDescrDispensaEletronica(uf_gerais_retornaTipoDispensaEletronica(ALLTRIM(ucrsreceitaefetivar.receita)))
    lcComando = IIF(UPPER(ALLTRIM(lcTipo))= "MCDT", "EFETIVACAO_MSP", "EFETIVACAO")

    IF !uf_gerais_compStr(lcTipo,"MCDT") and !uf_gerais_compStr(lcTipo,"DEM") 
    	lcComando= "EFETIVACAO_" + lcTipo
    ENDIF


    
    LOCAL lcnrdocid
    STORE '' TO lcnrdocid
    IF EMPTY(lcbackoffice)
       lcnrdocid = ALLTRIM(ucrsatendcl.u_ndutavi)
    ELSE
       lcnrdocid = ALLTRIM(dadospsico.u_ndutavi)
    ENDIF
    lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\DEM\'+ALLTRIM(lcnomejar)+' ' + ALLTRIM(lcComando) + ' --iniciais=' + ALLTRIM(m_chinis) +' --token='+ALLTRIM(ucrsreceitaefetivar.token) +' --pinDirOpcao='+IIF(EMPTY(lcbackoffice), ALLTRIM(ucrsreceitaefetivar.coddiropcao), IIF(EMPTY(lccoddiropcao), "", lccoddiropcao))+' --nrReceita='+IIF(EMPTY(lcbackoffice), ALLTRIM(ucrsreceitaefetivar.receita), IIF(EMPTY(lcreceita), "", lcreceita))+' --codFarmacia='+ALLTRIM(lccodfarm)+' --pinReceita='+IIF(EMPTY(lcbackoffice), ALLTRIM(ucrsreceitaefetivar.codacesso), IIF(EMPTY(lccodacesso), "0", lccodacesso))+' --dbServer='+ALLTRIM(lcdbserver)+' --dbName='+UPPER(ALLTRIM(sql_db))+' --site='+UPPER(ALLTRIM(STRTRAN(ALLTRIM(mysite), ' ', '_')))+' --docId='+IIF( .NOT. EMPTY(ucrsreceitaefetivar.receita_psico), lcnrdocid, '')+' --test='+lctestjar
    IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.T.
       _CLIPTEXT = lcwspath
       uf_perguntalt_chama("Webservice de teste...", "OK", "", 64)
    ENDIF
    lcwspath = "javaw -jar "+myEncodingFileJar+lcwspath
    owsshell = CREATEOBJECT("WScript.Shell")
    owsshell.run(lcwspath, 1, .T.)
    regua(1, RECNO(), "A registar receitas no Sistema Central de Prescri��es...")
 ENDSCAN
 regua(1, RECNO()+1, "A registar receitas no Sistema Central de Prescri��es...")
 SELECT ucrsreceitaefetivar
 GOTO TOP
 SCAN
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_dem_resultadoEfetivacao '<<ALLTRIM(ucrsReceitaEfetivar.token)>>'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "ucrsDadosValidacaoEfetivarDEM", lcsql)
       uf_perguntalt_chama("N�o foi possivel verificar dados de efetiva��o da receita. Contacte o suporte.", "OK", "", 32)
       regua(2)
       RETURN .F.
    ENDIF
    SELECT ucrsdadosvalidacaoefetivardem
    IF RECCOUNT("ucrsDadosValidacaoEfetivarDEM")>0
       SELECT ucrsreceitaefetivar
       REPLACE ucrsreceitaefetivar.resultado_efetivacao_cod WITH ALLTRIM(ucrsdadosvalidacaoefetivardem.resultado_efetivacao_cod)
       REPLACE ucrsreceitaefetivar.resultado_efetivacao_descr WITH ALLTRIM(ucrsdadosvalidacaoefetivardem.resultado_efetivacao_descr)
       REPLACE ucrsreceitaefetivar.resultado_comprovativo_registo WITH ALLTRIM(ucrsdadosvalidacaoefetivardem.resultado_comprovativo_registo_xs)
       
       &&TODO 100003050001 : comentar linha baixo
       REPLACE ucrsreceitaefetivar.resultado_anulacao_cod WITH "100003050099"
    ENDIF
    SELECT ucrsreceitaefetivar
 ENDSCAN
 regua(2)
 IF USED("ucrsReceitaEfetivarAp")
    fecha("ucrsReceitaEfetivarAp")
 ENDIF
 

 
 
 SELECT ucrsreceitaefetivar
 GOTO TOP
 LOCATE FOR (ucrsreceitaefetivar.resultado_efetivacao_cod<>"100003040001" and ucrsreceitaefetivar.resultado_efetivacao_cod<>"100007010003" AND ucrsreceitaefetivar.resultado_efetivacao_cod<>"10200100001") .OR. EMPTY(ALLTRIM(ucrsreceitaefetivar.resultado_comprovativo_registo))
 IF FOUND()
    SELECT * FROM ucrsReceitaEfetivar WHERE (ucrsreceitaefetivar.resultado_efetivacao_cod<>"100003040001"  AND ucrsreceitaefetivar.resultado_efetivacao_cod<>"100007010003" AND ucrsreceitaefetivar.resultado_efetivacao_cod<>"10200100001") OR EMPTY(ALLTRIM(ucrsreceitaefetivar.resultado_comprovativo_registo)) INTO CURSOR ucrsReceitaEfetivarAp READWRITE
 ENDIF
 

 
 IF EMPTY(lcbackoffice)
    uf_atendimento_marcalinhasefectivadas()
 ENDIF
 IF  .NOT. USED("ucrsReceitaEfetivarAp")
    RETURN .T.
 ENDIF
 IF RECCOUNT("ucrsReceitaEfetivarAp")==0
    RETURN .T.
 ELSE
    LOCAL lcrespostadem
    uf_infovenda_valida()
    RETURN .F.
 ENDIF
ENDFUNC


FUNCTION uf_atendimento_validaTipoSnsExtPorPlano
   LPARAMETERS lcCodigo
   	 
   	 LOCAL lcValidExt, lcCount
   	 lcValidExt = .f.
   	 lcCount = 0
   	 
   	 if(USED("ucrsCtlPlaTemp"))
   	 	fecha("ucrsCtlPlaTemp")
   	 ENDIF
   	 
   
     TEXT TO lcsql TEXTMERGE NOSHOW

		SELECT COUNT(*) as ct FROM cptpla(nolock) 
		WHERE codigo = '<<ALLTRIM(lcCodigo)>>' and cptpla.snsExterno = 1 and inactivo = 0
		
	 ENDTEXT
	 IF  .NOT. uf_gerais_actgrelha("", "ucrsCtlPlaTemp", lcsql)
	    uf_perguntalt_chama("N�o foi possivel obter resposta do plano. Contacte o suporte.", "OK", "", 32)
	    regua(2)
	    RETURN .F.
	 ENDIF
	 
	 if(USED("ucrsCtlPlaTemp"))
	 	SELECT ucrsCtlPlaTemp
	 	GO TOP
	 	lcCount = ucrsCtlPlaTemp.ct
	 ENDIF
	 
	 
	 if(lcCount>0)
	 	lcValidExt  = .t.
	 ENDIF
	 
	
	 
	 if(USED("ucrsCtlPlaTemp"))
   	 	fecha("ucrsCtlPlaTemp")
   	 ENDIF
   
   
   	 RETURN lcValidExt  
	
ENDFUNC


FUNCTION uf_atendimento_validaTipoSnsExt
   LPARAMETERS lcToken
   	 
   	 LOCAL lcValidExt, lcCount
   	 lcValidExt = .f.
   	 lcCount = 0
   	 
   	 if(USED("ucrsCtlPlaTemp"))
   	 	fecha("ucrsCtlPlaTemp")
   	 ENDIF
   	 
   
     TEXT TO lcsql TEXTMERGE NOSHOW

		SELECT COUNT(*) as ct FROM ctltrct(nolock) 
		INNER JOIN cptpla(nolock) on ctltrct.cptplacode = cptpla.codigo
		WHERE token = '<<ALLTRIM(lcToken)>>' and cptpla.snsExterno = 1
		
	 ENDTEXT
	 IF  .NOT. uf_gerais_actgrelha("", "ucrsCtlPlaTemp", lcsql)
	    uf_perguntalt_chama("N�o foi possivel obter resposta do plano. Contacte o suporte.", "OK", "", 32)
	    regua(2)
	    RETURN .F.
	 ENDIF
	 
	 if(USED("ucrsCtlPlaTemp"))
	 	SELECT ucrsCtlPlaTemp
	 	GO TOP
	 	lcCount = ucrsCtlPlaTemp.ct
	 ENDIF
	 
	 
	 if(lcCount>0)
	 	lcValidExt  = .t.
	 ENDIF
	 
	
	 
	 if(USED("ucrsCtlPlaTemp"))
   	 	fecha("ucrsCtlPlaTemp")
   	 ENDIF
   
   
   	 RETURN lcValidExt  
	
ENDFUNC

FUNCTION uf_atendimento_receitasanularTragSa
 LPARAMETERS lcreceitanr, lctoken
 LOCAL lcnomejar, lctestjar
 STORE '' TO lcnomejar, lctestjar
 SELECT ucrse1
 IF EMPTY(ALLTRIM(ucrse1.u_codfarm))
    uf_perguntalt_chama("C�digo de Farm�cia n�o configurado. Contacte o suporte.", "OK", "", 32)
    RETURN .F.
 ENDIF
 SELECT ucrse1
 lccodfarm = ALLTRIM(ucrse1.u_codfarm)
 regua(0, 3, "A anular receita: "+ALLTRIM(lcreceitanr)+" no Sistema Central de Prescri��es...")
 regua(1, 1, "A anular receita: "+ALLTRIM(lcreceitanr)+" no Sistema Central de Prescri��es...")
 lcsql = ''
 
 

 LOCAL lcTipo
 lcTipo= 'ANULACAO_TRAG'
 
 
 
 TEXT TO lcsql TEXTMERGE NOSHOW
		update 
			Dispensa_Eletronica 
		set 
			resultado_anulacao_cod = null
			,resultado_anulacao_descr = null
		where 
			token = '<<ALLTRIM(lcToken)>>'
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
    uf_perguntalt_chama("N�o foi possivel verificar dados de anulacao da receita. Contacte o suporte.", "OK", "", 32)
    regua(2)
    RETURN .F.
 ENDIF
 IF  .NOT. USED("ucrsServerName")
    TEXT TO lcsql TEXTMERGE NOSHOW
			Select @@SERVERNAME as dbServer
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "ucrsServerName", lcsql)
       uf_perguntalt_chama("N�o foi possivel proceder ao servi�o de Efetiva��o da Dispensa. Contacte o suporte.", "OK", "", 32)
       regua(2)
       RETURN .F.
    ENDIF
 ENDIF
 SELECT ucrsservername
 lcdbserver = ALLTRIM(ucrsservername.dbserver)
 lcnomejar = "LtsDispensaEletronica.jar"
 IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.F.
    lctestjar = "0"
 ELSE
    lctestjar = "1"
 ENDIF
 lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\DEM\'+ALLTRIM(lcnomejar) + ' ' + ALLTRIM(lcTipo)  +  '  "--token='+ALLTRIM(lctoken)+'"'+' --nrReceita='+ALLTRIM(lcreceitanr)+' --codFarmacia='+ALLTRIM(lccodfarm)+' --dbServer='+ALLTRIM(lcdbserver)+' --dbName='+UPPER(ALLTRIM(sql_db))+' --site='+UPPER(ALLTRIM(STRTRAN(ALLTRIM(mysite), ' ', '_')))+' --test='+lctestjar
 IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.T.
    _CLIPTEXT = lcwspath
    uf_perguntalt_chama("Webservice de teste...", "OK", "", 64)
 ENDIF
 lcwspath = "javaw -jar "+myEncodingFileJar+lcwspath
 owsshell = CREATEOBJECT("WScript.Shell")
 owsshell.run(lcwspath, 1, .T.)
 regua(1, 2, "A anular receita: "+ALLTRIM(lcreceitanr)+" no Sistema Central de Prescri��es...")
 LOCAL j
 j = 0
 DO WHILE .T.
    WAIT WINDOW "" TIMEOUT 3
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_dem_resultadoAnulacao '<<ALLTRIM(lcReceitaNr)>>','<<ALLTRIM(lcToken)>>'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "ucrsDadosValidacaoAnulacaoDEM", lcsql)
       uf_perguntalt_chama("N�o foi possivel verificar dados de anulacao da receita. Contacte o suporte.", "OK", "", 32)
       regua(2)
       RETURN .F.
    ENDIF
    IF  .NOT. (EMPTY(ALLTRIM(ucrsdadosvalidacaoanulacaodem.resultado_anulacao_cod)))
       EXIT
    ENDIF
    j = j+1
    IF j>10
       EXIT
    ENDIF
 ENDDO
 regua(2)
 SELECT ucrsdadosvalidacaoanulacaodem
 IF  .NOT. (ALLTRIM(ucrsdadosvalidacaoanulacaodem.resultado_anulacao_cod)=="100003050001") AND   .NOT.(ALLTRIM(ucrsdadosvalidacaoanulacaodem.resultado_anulacao_cod)=="100004050001") 
    regua(2)
    uf_perguntalt_chama("Resposta do Sistema Central de Prescri��es: "+ALLTRIM(ucrsdadosvalidacaoanulacaodem.resultado_anulacao_descr), "OK", "", 16)
    RETURN .F.
 ENDIF
 LOCAL lcstamp
 lcstamp = uf_gerais_stamp()
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		Insert Into b_ocorrencias
			(stamp, linkStamp, tipo, grau
			,descr
			,oValor
			,usr, date)
		values
			('<<ALLTRIM(lcStamp)>>', '', 'Anulacao DEM', 2
			,'Anula��o de Receita DEM Nr: ' + '<<ALLTRIM(lcReceitaNr)>>' + ' C�digo A:'+'<<ALLTRIM(ucrsDadosValidacaoAnulacaoDEM.resultado_anulacao_cod)>>'
			,'Token: ' + '<<ALLTRIM(lcToken)>>'
			,<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()))
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
 ENDIF
 RETURN .T.
ENDFUNC
**
FUNCTION uf_atendimento_receitasanular
 LPARAMETERS lcreceitanr, lctoken
 LOCAL lcnomejar, lctestjar
 STORE '' TO lcnomejar, lctestjar
 SELECT ucrse1
 IF EMPTY(ALLTRIM(ucrse1.u_codfarm))
    uf_perguntalt_chama("C�digo de Farm�cia n�o configurado. Contacte o suporte.", "OK", "", 32)
    RETURN .F.
 ENDIF
 SELECT ucrse1
 lccodfarm = ALLTRIM(ucrse1.u_codfarm)
 regua(0, 3, "A anular receita: "+ALLTRIM(lcreceitanr)+" no Sistema Central de Prescri��es...")
 regua(1, 1, "A anular receita: "+ALLTRIM(lcreceitanr)+" no Sistema Central de Prescri��es...")
 lcsql = ''
 
 
 LOCAL lcRes,lcTipo
 lcTipo= 'anulacao'
 

 lcRes= uf_gerais_devolveDescrDispensaEletronica(uf_gerais_retornaTipoDispensaEletronica(ALLTRIM(lcreceitanr)))
 lcTipo= IIF(UPPER(ALLTRIM(lcRes))= "MCDT", "ANULACAO_MSP", "ANULACAO")
 IF !uf_gerais_compStr(lcTipo,"MCDT") and !uf_gerais_compStr(lcTipo,"DEM") 
	lcTipo= "ANULACAO_" + lcRes
 ENDIF
 
 
 if(uf_atendimento_validaTipoSnsExt(lctoken))
 	lcTipo= 'ANULACAO_TRAG'
 ENDIF

 **Motivo de anula��o
 LOCAL lcMotivoAnula
 lcMotivoAnula = ""
 if(uf_gerais_compStr(lcRes,"PEMH"))	
    uf_perguntalt_chama("Por favor escolha o motivo de anula��o. ", "OK", "", 64)
 	lcMotivoAnula  = uf_gerais_grelha_multidata_por_tipo("anula_pemh")
 	IF(EMPTY(lcMotivoAnula))
 		regua(2)
 		uf_perguntalt_chama("Motivo n�o selecionado, por favor valide.", "OK", "", 64)
 		RETURN .f.
 	ENDIF	
 ENDIF
 

 TEXT TO lcsql TEXTMERGE NOSHOW
		update 
			Dispensa_Eletronica 
		set 
			resultado_anulacao_cod = null
			,resultado_anulacao_descr = null
		where 
			token = '<<ALLTRIM(lcToken)>>'
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
    uf_perguntalt_chama("N�o foi possivel verificar dados de anulacao da receita. Contacte o suporte.", "OK", "", 32)
    regua(2)
    RETURN .F.
 ENDIF
 IF  .NOT. USED("ucrsServerName")
    TEXT TO lcsql TEXTMERGE NOSHOW
			Select @@SERVERNAME as dbServer
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "ucrsServerName", lcsql)
       uf_perguntalt_chama("N�o foi possivel proceder ao servi�o de Efetiva��o da Dispensa. Contacte o suporte.", "OK", "", 32)
       regua(2)
       RETURN .F.
    ENDIF
 ENDIF
 SELECT ucrsservername
 lcdbserver = ALLTRIM(ucrsservername.dbserver)
 lcnomejar = "LtsDispensaEletronica.jar"
 IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.F.
    lctestjar = "0"
 ELSE
    lctestjar = "1"
 ENDIF
 lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\DEM\'+ALLTRIM(lcnomejar) + ' ' + ALLTRIM(lcTipo)  +  '  "--token='+ALLTRIM(lctoken)+'"'+' --nrReceita='+ALLTRIM(lcreceitanr)+' --motivo='+ALLTRIM(lcMotivoAnula)+' --codFarmacia='+ALLTRIM(lccodfarm)+' --dbServer='+ALLTRIM(lcdbserver)+' --dbName='+UPPER(ALLTRIM(sql_db))+' --site='+UPPER(ALLTRIM(STRTRAN(ALLTRIM(mysite), ' ', '_')))+' --test='+lctestjar
 IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.T.
    _CLIPTEXT = lcwspath
    uf_perguntalt_chama("Webservice de teste...", "OK", "", 64)
 ENDIF
 lcwspath = "javaw -jar "+myEncodingFileJar+lcwspath
 owsshell = CREATEOBJECT("WScript.Shell")
 owsshell.run(lcwspath, 1, .T.)
 regua(1, 2, "A anular receita: "+ALLTRIM(lcreceitanr)+" no Sistema Central de Prescri��es...")
 LOCAL j
 j = 0
 DO WHILE .T.
    WAIT WINDOW "" TIMEOUT 3
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_dem_resultadoAnulacao '<<ALLTRIM(lcReceitaNr)>>','<<ALLTRIM(lcToken)>>'
    ENDTEXT 
    IF  .NOT. uf_gerais_actgrelha("", "ucrsDadosValidacaoAnulacaoDEM", lcsql)
       uf_perguntalt_chama("N�o foi possivel verificar dados de anulacao da receita. Contacte o suporte.", "OK", "", 32)
       regua(2)
       RETURN .F.
    ENDIF
    IF  .NOT. (EMPTY(ALLTRIM(ucrsdadosvalidacaoanulacaodem.resultado_anulacao_cod)))
       EXIT
    ENDIF
    j = j+1
    IF j>10
       EXIT
    ENDIF
 ENDDO
 regua(2)
 SELECT ucrsdadosvalidacaoanulacaodem
 IF  .NOT. (ALLTRIM(ucrsdadosvalidacaoanulacaodem.resultado_anulacao_cod)=="100003050001") AND   .NOT.(ALLTRIM(ucrsdadosvalidacaoanulacaodem.resultado_anulacao_cod)=="100004050001")  AND   .NOT.(ALLTRIM(ucrsdadosvalidacaoanulacaodem.resultado_anulacao_cod)=="100007010003") AND   .NOT.(ALLTRIM(ucrsdadosvalidacaoanulacaodem.resultado_anulacao_cod)=="10300100001")
    regua(2)
    uf_perguntalt_chama("Resposta do Sistema Central de Prescri��es: "+ALLTRIM(ucrsdadosvalidacaoanulacaodem.resultado_anulacao_descr), "OK", "", 16)
    RETURN .F.
 ENDIF
 LOCAL lcstamp
 lcstamp = uf_gerais_stamp()
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		Insert Into b_ocorrencias
			(stamp, linkStamp, tipo, grau
			,descr
			,oValor
			,usr, date)
		values
			('<<ALLTRIM(lcStamp)>>', '', 'Anulacao DEM', 2
			,'Anula��o de Receita DEM Nr: ' + '<<ALLTRIM(lcReceitaNr)>>' + ' C�digo A:'+'<<ALLTRIM(ucrsDadosValidacaoAnulacaoDEM.resultado_anulacao_cod)>>'
			,'Token: ' + '<<ALLTRIM(lcToken)>>'
			,<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()))
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
 ENDIF
 RETURN .T.
ENDFUNC
**
FUNCTION uf_atendimento_vendaslinhasdem
 LOCAL lcdem, lcres,lcRefBufer, lcManualEmDem
 lcdem = .F.
 lcres = .F.
 lcManualEmDem = .f.
 lcRefBufer = 'Os seguintes productos foram adicionados manualmente a vendas de dispensa eletr�nica: ' + CHR(13)
 
 
 &&N�o validar para PEMH 
 SELECT * FROM Fi WHERE epromo==.F. AND partes=0  AND !uf_gerais_compStr(uf_gerais_retornaTipoDispensaEletronica(fi.receita),"2") ORDER BY lordem INTO CURSOR ucrsTempFiDem READWRITE
 SELECT ucrstempfidem
 GOTO TOP
 SCAN
    IF LEFT(ALLTRIM(ucrstempfidem.design), 1)=="."
       IF AT("RESERVA", ALLTRIM(ucrstempfidem.design))=0
          lcdem = ucrstempfidem.dem
          lcres = .F.
       ELSE
          lcres = .T.
       ENDIF
    ELSE
       IF ucrstempfidem.dem<>lcdem .AND. lcres=.F.
               
          if(!EMPTY(ALLTRIM(ucrstempfidem.ref)))          
          	lcRefBufer  = lcRefBufer   + ALLTRIM(ucrstempfidem.ref) + "; "
          ENDIF
          
          lcManualEmDem  = .t.
       ENDIF
    ENDIF
 ENDSCAN
 
 IF USED("ucrsTempFiDem")
    fecha("ucrsTempFiDem")
 ENDIF
 

 

 
 if(!EMPTY(lcManualEmDem))
 	 lcRefBufer = SUBSTR(ALLTRIM(lcRefBufer), 1, 254)
 	 uf_perguntalt_chama(lcRefBufer, "OK", "", 48)
 	 RETURN .F.
 ENDIF
 
 
 RETURN .T.
ENDFUNC
**
PROCEDURE uf_atendimento_devolvecursorcabecalhofi
 LPARAMETERS lclordem
 LOCAL lcposfi
 lcposfi = RECNO("FI")
 IF (USED("ucrsTempFiCabecalho"))
    fecha("ucrsTempFiCabecalho")
 ENDIF
 SELECT * FROM FI WHERE LEFT(ALLTRIM(fi.design), 1)=="." AND LEFT(astr(fi.lordem), 2)=LEFT(astr(lclordem), 2) INTO CURSOR ucrsTempFiCabecalho READWRITE
 SELECT fi
 TRY
    GOTO lcposfi
 CATCH
    GOTO TOP
 ENDTRY
ENDPROC
**
PROCEDURE uf_atendimento_coduniemb
 LOCAL lccod, lcactpos, lclordem, lcobriga, lcfistamp
 STORE .F. TO lcobriga
 SELECT fi
 lccod = STREXTRACT(fi.design, '[', ']', 1, 0)
 lcfistamp = fi.fistamp
 SELECT ucrsatendcomp
 LOCATE FOR UPPER(ALLTRIM(ucrsatendcomp.codigo))=UPPER(ALLTRIM(lccod))
 IF FOUND()
    IF ucrsatendcomp.obriga_codemb==.T.
       lcobriga = .T.
    ELSE
       lcobriga = .F.
    ENDIF
 ENDIF
 IF lcobriga==.T.
    SELECT fi
    lcactpos = RECNO("fi")
    lclordem = fi.lordem
    SELECT fi
    SCAN FOR fi.lordem>lclordem
       IF UPPER(ALLTRIM(lccod))=="WJ"
          uf_tecladoalpha_chama("FI.U_CODEMB", "Introduza o C�digo da Embalagem:", .F., .F., 2)
          uf_atendimento_preenche_codunicoembalagemfi()
          IF !uf_faturacao_checkLenEmb(ALLTRIM(lccod), ALLTRIM(FI.U_CODEMB))
            REPLACE FI.U_CODEMB WITH ''
          ENDIF
       ELSE
          SELECT ucrsatendst
          LOCATE FOR ucrsatendst.ststamp=fi.fistamp
          IF FOUND()
             IF ucrsatendst.coduniemb==.T. .AND. EMPTY(fi.u_codemb)
                uf_tecladoalpha_chama("FI.U_CODEMB", "Introduza o C�digo da Embalagem:", .F., .F., 2)
                uf_atendimento_preenche_codunicoembalagemfi()
                IF !uf_faturacao_checkLenEmb(ALLTRIM(lccod), ALLTRIM(FI.U_CODEMB))
                  REPLACE FI.U_CODEMB WITH ''                  
                ENDIF
             ENDIF
          ENDIF
       ENDIF
       IF LEFT(ALLTRIM(fi.design), 1)=="."
          EXIT
       ENDIF
    ENDSCAN
    SELECT fi
    TRY
       GOTO lcactpos
    CATCH
    ENDTRY
 ENDIF
 SELECT ucrsatendcomp
 LOCATE FOR ALLTRIM(ucrsatendcomp.fistamp)==ALLTRIM(lcfistamp)
ENDPROC
**
PROCEDURE uf_atendimento_encomendamed
 uf_encomendamed_chama()
ENDPROC
**
PROCEDURE uf_atendimento_encomendafornecedor
 SELECT ucrsatendst
 IF  .NOT. EMPTY(ucrsatendst.viaverde)
    uf_dadosencat_chama(.T.)
 ELSE
    uf_dadosencat_chama()
 ENDIF
ENDPROC
**
FUNCTION uf_atendimento_controlapontosapresentacartao
 LPARAMETERS lcscannercartao
 IF lcscannercartao
    atendimento.pgfdados.page1.chkpontos.tag = "true"
    atendimento.pgfdados.page1.chkpontos.image1.picture = mypath+"\imagens\icons\checked_b_24.bmp"
    RETURN .T.
 ENDIF
 IF uf_gerais_getparameter("ADM0000000262", "BOOL")==.T.
    atendimento.pgfdados.page1.chkpontos.tag = "true"
    atendimento.pgfdados.page1.chkpontos.image1.picture = mypath+"\imagens\icons\checked_b_24.bmp"
 ELSE
    atendimento.pgfdados.page1.chkpontos.tag = "false"
    atendimento.pgfdados.page1.chkpontos.image1.picture = mypath+"\imagens\icons\unchecked_b_24.bmp"
 ENDIF
ENDFUNC
**
PROCEDURE uf_atendimento_conferir
 IF atendimento.menu1.conferir.tag=="true"
    atendimento.menu1.conferir.tag = "false"
    atendimento.menu1.conferir.img.picture = mypath+"\imagens\icons\unchecked_w.png"   
 ELSE
    atendimento.menu1.conferir.tag = "true"
    atendimento.menu1.conferir.img.picture = mypath+"\imagens\icons\checked_w.png"
 ENDIF
ENDPROC


** Exece��es as conferencias de embalagens
FUNCTION uf_atendimento_conferereferenciaExececao
	LPARAMETERS lcQttMaior, lcfistamp
	
	if(!USED("fi"))
		RETURN .f.
	ENDIF
	
	LOCAL lcFiPosTemp
	lcFiPosTemp = RECNO("fi")
	
	SELECT fi
	GO TOP
	
	
	SELECT fi.qtt as qtt FROM fi  WHERE ALLTRIM(fi.fistamp)==ALLTRIM(lcfistamp)  INTO CURSOR ucrsFiQttAuxConf READWRITE

	
	if(USED("ucrsFiQttAuxConf"))
		SELECT ucrsFiQttAuxConf
		GO TOP
		if(ucrsFiQttAuxConf.qtt>=lcQttMaior)
			UPDATE fi SET fi.qtdem = fi.qtt WHERE ALLTRIM(fi.fistamp)==ALLTRIM(lcfistamp)
		ENDIF   		
		fecha("ucrsFiQttAuxConf")
	ENDIF
	
	
	
	SELECT fi
    TRY
       GOTO lcFiPosTemp
    CATCH
    	GOTO TOP
    ENDTRY	

ENDFUNC



**
FUNCTION uf_atendimento_conferereferencia
 LPARAMETERS lcref
 
 LOCAL lcNaoObrigaQttMaior, lcQttMaior
 lcNaoObrigaQttMaior = .f.
 lcQttMaior= 0
  **valida se n�o obriga confer�ncia de embalagens com quantidade superior a X
 lcNaoObrigaQttMaior = 	uf_gerais_getparameter_site('ADM0000000120', 'BOOL', mysite)
 lcQttMaior= 	uf_gerais_getparameter_site('ADM0000000120', 'NUM', mysite)
 uv_confDev= 	uf_gerais_getparameter_site('ADM0000000125', 'BOOL', mysite)
 
 **SELECT * FROM fi WHERE fi.qtdem<fi.qtt AND UPPER(ALLTRIM(fi.ref))==UPPER(ALLTRIM(lcref)) INTO CURSOR ucrsFiTempAux READWRITE
 SELECT * FROM fi WHERE fi.qtdem<fi.qtt AND UPPER(ALLTRIM(fi.ref))==UPPER(ALLTRIM(lcref)) AND 1 = IIF(!uv_confDev, IIF(EMPTY(fi.ofistamp), 1, 0), IIF(EMPTY(fi.ofistamp) , 1, IIF(fi.partes <> 0, 1, 0))) INTO CURSOR ucrsFiTempAux READWRITE
 SELECT ucrsfitempaux
 GOTO TOP
 LOCATE FOR UPPER(ALLTRIM(ucrsfitempaux.ref))==UPPER(ALLTRIM(lcref))
 IF FOUND()
    lcfistamp = ucrsfitempaux.fistamp
   
    IF(!EMPTY(lcNaoObrigaQttMaior) AND lcQttMaior>1 AND ucrsfitempaux.qtt>=lcQttMaior AND ucrsfitempaux.qtdem<ucrsfitempaux.qtt)
     	uf_atendimento_conferereferenciaExececao(lcQttMaior, lcfistamp)   	
    ELSE

      uv_qttMin = uf_gerais_getUmValor("st", "qttminvd", "ref = '" + ALLTRIM(lcRef) + "' and site_nr = " + ASTR(mySite_nr))

      IF EMPTY(uv_qttMin)
         UPDATE fi SET fi.qtdem = fi.qtdem+1 WHERE ALLTRIM(fi.fistamp)==ALLTRIM(lcfistamp)
      ELSE

         IF ucrsfitempaux.qtDem + uv_qttMin > ucrsfitempaux.qtt
            uf_perguntalt_chama("A QUANTIDADE CONFERIDA � SUPERIOR � REGISTADA.", "OK", "", 64)
            RETURN .F.
         ELSE
            UPDATE fi SET fi.qtdem = fi.qtdem + uv_qttMin WHERE ALLTRIM(fi.fistamp)==ALLTRIM(lcfistamp)
         ENDIF

      ENDIF

   	
    ENDIF
 
    PUBLIC lcrecnoconf
    SELECT fi
    GOTO TOP
    SCAN
       IF ALLTRIM(fi.fistamp)==ALLTRIM(lcfistamp)
          lcrecnoconf = RECNO("fi")
       ENDIF
    ENDSCAN
    SELECT fi
    TRY
       GOTO lcrecnoconf
    CATCH
    ENDTRY
    REPLACE fi.design WITH ALLTRIM(fi.design)
    atendimento.grdatend.refresh
    atendimento.grdatend.setfocus
    RETURN .T.
 ELSE
    IF uf_gerais_getparameter_site('ADM0000000099', 'BOOL', mysite)
       SELECT * FROM fi WHERE UPPER(ALLTRIM(fi.ref))==UPPER(ALLTRIM(lcref)) INTO CURSOR ucrsFiTempAuxREF READWRITE
       SELECT ucrsfitempauxref
       LOCATE FOR UPPER(ALLTRIM(ucrsfitempauxref.ref))==UPPER(ALLTRIM(lcref))
       IF  .NOT. FOUND()
          uf_perguntalt_chama("O ARTIGO N�O SE ENCONTRA NO ATENDIMENTO.", "OK", "", 64)
          fecha("ucrsFiTempAuxREF")
          IF USED("ucrsFiTempAux")
             fecha("ucrsFiTempAux")
          ENDIF
          RETURN .F.
       ELSE
          uf_perguntalt_chama("A QUANTIDADE CONFERIDA � SUPERIOR � REGISTADA.", "OK", "", 64)
          fecha("ucrsFiTempAuxREF")
          IF USED("ucrsFiTempAux")
             fecha("ucrsFiTempAux")
          ENDIF
          RETURN .F.
       ENDIF
    ENDIF
 ENDIF
 IF USED("ucrsFiTempAux")
    fecha("ucrsFiTempAux")
 ENDIF
 RETURN .F.
ENDFUNC
**
FUNCTION uf_atendimento_todos_conferidos
	LOCAL lcverifica, lcartigos
	 STORE .F. TO lcverifica
	 STORE '' TO lcartigos
	 IF !uf_gerais_getParameter_site('ADM0000000125', 'BOOL', mySite)
		 SELECT fi
		 GOTO TOP
		 SCAN
		    IF fi.qtt>0 .AND. fi.qtt>fi.qtdem .AND. fi.stns=.F. .AND.  .NOT. EMPTY(fi.ref) .AND. EMPTY(fi.ofistamp) .AND. ALLTRIM(fi.ref)<>'R000001'
		       lcverifica = .T.
		    ENDIF
		 ENDSCAN
	 ELSE
	 	SELECT fi
		 GOTO TOP
		 SCAN
		    IF fi.qtt>0 .AND. fi.qtt>fi.qtdem .AND. fi.stns=.F. .AND.  .NOT. EMPTY(fi.ref) .AND. (EMPTY(fi.ofistamp) OR (!EMPTY(fi.ofistamp) AND fi.partes=1)) .AND. ALLTRIM(fi.ref)<>'R000001'
		       lcverifica = .T.
		    ENDIF
		 ENDSCAN
	 ENDIF 
	 SELECT fi
	 GOTO TOP
	 IF lcverifica=.T.
	    RETURN .F.
	 ELSE
	    RETURN .T.
	 ENDIF
ENDFUNC
**
FUNCTION uf_atendimento_todos_dem_conferidos
 LOCAL lcverifica, lcartigos
 STORE .F. TO lcverifica
 STORE '' TO lcartigos
 IF !uf_gerais_getParameter_site('ADM0000000125', 'BOOL', mySite)
	 SELECT fi
	 GOTO TOP
	 SCAN
	    IF fi.qtt>0 .AND. fi.qtt>fi.qtdem .AND. fi.stns=.F. .AND.  .NOT. EMPTY(fi.ref) .AND. EMPTY(fi.ofistamp) .AND. ALLTRIM(fi.ref)<>'R000001' .AND.  .NOT. EMPTY(fi.receita)
	       lcverifica = .T.
	    ENDIF
	 ENDSCAN
 ELSE
	 SELECT fi
	 GOTO TOP
	 SCAN
	    IF fi.qtt>0 .AND. fi.qtt>fi.qtdem .AND. fi.stns=.F. .AND.  .NOT. EMPTY(fi.ref) .AND. (EMPTY(fi.ofistamp) OR (!EMPTY(fi.ofistamp) AND fi.partes=1)) .AND. ALLTRIM(fi.ref)<>'R000001' .AND.  .NOT. EMPTY(fi.receita)
	       lcverifica = .T.
	    ENDIF
	 ENDSCAN
 ENDIF 
	
 SELECT fi
 GOTO TOP
 IF lcverifica=.T.
    RETURN .F.
 ELSE
    RETURN .T.
 ENDIF
ENDFUNC
**
PROCEDURE uf_atendimento_procuracliente
 LOCAL lcsql
 uf_chk_nova_versao()
 IF USED("uCrsDadosCartaoCliente")
    fecha("uCrsDadosCartaoCliente")
 ENDIF
 IF USED("uCrsDadosCliente")
    fecha("uCrsDadosCliente")
 ENDIF
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_utentes_dadosCC '<<ALLTRIM(myClientName)>>', '<<ALLTRIM(mysite)>>', 0
 ENDTEXT
 uf_gerais_actgrelha("", "uCrsDadosCartaoCliente", lcsql)
 IF RECCOUNT("uCrsDadosCartaoCliente")>0
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			select nome, no, estab, ncont, nbenef, sexo, nascimento  FROM b_utentes (nolock) WHERE ncont = '<<ALLTRIM(uCrsDadosCartaoCliente.nif)>>' OR nbenef = '<<uCrsDadosCartaoCliente.nrsns>>' AND inactivo  = 0 ORDER BY no asc
    ENDTEXT
    uf_gerais_actgrelha("", "uCrsDadosCliente", lcsql)
    IF RECCOUNT("uCrsDadosCliente")>0
       IF RECCOUNT("uCrsDadosCliente")=1
          IF EMPTY(ucrsdadoscliente.nbenef) .OR. EMPTY(ucrsdadoscliente.ncont) .OR. uf_gerais_getdate(ucrsdadoscliente.nascimento, "SQL")=='19000101'
             IF uf_perguntalt_chama("Pretende actualizar a informa��o do utente com os dados lidos no Cart�o de Cidad�o?", "Sim", "N�o")
                SELECT ucrsdadoscartaocliente
                GOTO TOP
                lcsql = ''             
                TEXT TO lcsql TEXTMERGE NOSHOW
							UPDATE b_utentes
							SET nome = '<<ALLTRIM(uCrsDadosCartaoCliente.nome)>>'
								,ncont = '<<ALLTRIM(uCrsDadosCartaoCliente.nif)>>'
								,nbenef = '<<uCrsDadosCartaoCliente.nrSNS>>'
								,nascimento = '<<uf_gerais_getdate(uCrsDadosCartaoCliente.nascimento),"SQL">>'
								,sexo = '<<uCrsDadosCartaoCliente.sexo>>'
								,usrdata = '<<uf_gerais_getdate(uCrsDadosCartaoCliente.data_alt,"SQL")>>'
							WHERE no = '<<uCrsDadosCliente.no>>' and estab = '<<uCrsDadosCliente.estab>>' 
                ENDTEXT
                IF  .NOT. (uf_gerais_actgrelha("", "", lcsql))
                   uf_perguntalt_chama("Ocorreu uma anomalia ao atualizar a ficha do cliente. Por favor contacte o Suporte.", "OK", "", 16)
                ENDIF
             ENDIF
          ENDIF
          uf_atendimento_selcliente(ucrsdadoscliente.no, ucrsdadoscliente.estab, "ATENDIMENTO")
       ELSE
          uf_perguntalt_chama("ATEN��O: Existe mais do que uma ficha de cliente com os dados sincronizados. Por favor verifique.", "OK", "", 16)
          SELECT ucrsdadoscliente
          GOTO TOP
          uf_atendimento_selcliente(ucrsdadoscliente.no, ucrsdadoscliente.estab, "ATENDIMENTO")
       ENDIF
    ELSE
       uf_atendimento_pesqcliente()
    ENDIF
 ELSE
    uf_atendimento_pesqcliente()
 ENDIF
 IF USED("uCrsDadosCliente")
    fecha("uCrsDadosCliente")
 ENDIF
 IF USED("uCrsDadosCartaoCliente")
    fecha("uCrsDadosCartaoCliente")
 ENDIF
 IF uf_gerais_getparameter("ADM0000000267", "BOOL")
    uf_atendimento_conferir_limpar()
 ELSE
    uf_atendimento_salvar_limpar()
 ENDIF
ENDPROC
**
PROCEDURE uf_atendimento_verificarobot
 LOCAL lcvalidarobot, lcrobotstate
 myrobotstate = .F.
 DO CASE
    CASE myusarobot .OR. myusarobotapostore
       lcrobotstate = uf_robot_validatecnilabrobotstate()
       DO CASE
          CASE lcrobotstate='00'
             myrobotstate = .T.
          CASE lcrobotstate='02'
             myrobotstate = .T.
          OTHERWISE
       ENDCASE
    CASE myusarobotconsis
       myrobotstate = .T.
    CASE myusaservicosrobot=.T.
       LOCAL lcresp, lcpedstamp, lcmachinestate
       lcpedstamp = uf_gerais_stamp()
       lcresp = uf_robot_servico_status(ALLTRIM(lcpedstamp))
       IF  .NOT. EMPTY(lcresp) .AND. VAL(lcresp)=200
          DECLARE Sleep IN Win32API LONG
          sleep(3500.0 )
          lcrobotstate = uf_robot_servico_status_response(ALLTRIM(lcpedstamp))
          DO CASE
             CASE lcrobotstate='00'
                myrobotstate = .T.
             CASE lcrobotstate='02'
                myrobotstate = .T.
             OTHERWISE
          ENDCASE
       ENDIF
    OTHERWISE
 ENDCASE
ENDPROC
**
FUNCTION uf_atendimento_mantemreceita
 LPARAMETERS lnlordem
 LOCAL lcmantemreceita
 STORE .T. TO lcmantemreceita
 IF  .NOT. USED("uCrsVale")
    uf_perguntalt_chama("Ocorreu uma anomalia ao verificar a informa��o dos produtos. Por favor reinicie  o software.", "OK", "", 16)
    RETURN .F.
 ENDIF
 IF  .NOT. USED("uCrsValeAuxMR")
    uf_perguntalt_chama("Ocorreu uma anomalia ao verificar a informa��o dos produtos. Por favor reinicie  o software.", "OK", "", 16)
    RETURN .F.
 ENDIF
 

 
 SELECT  fi.lordem, NVL(fi.ofistamp, fi.fistamp) AS fistamp, ft.nome, ft.no, ft.estab, ft.ncont, NVL(ucrscabvendas.design, '') AS plano, NVL(ucrscabvendas.u_nbenef, '') AS u_nbenef, NVL(ucrscabvendas.u_nbenef2, '') AS u_nbenef2, NVL(STREXTRACT(ucrscabvendas.design, '[', ']', 1, 0), '') AS u_codigo;
 , NVL(STREXTRACT(ucrscabvendas.design, '[', ']', 3, 0), '') AS u_codigo2, ucrscabvendas.datareceita, NVL(fi.partes, 0) AS dev, ucrscabvendas.fact, ucrscabvendas.factpago, fi.ref, fi.design, fi.desconto, fi.desc2, fi.u_descval, fi.u_comp, fi.etiliquido, fi.epv, NVL(ft.u_ltstamp, ''), NVL(ft.u_ltstamp2, ''), fi.u_diploma, fi.fmarcada, fi.receita, fi.u_txcomp, fi.u_epvp, fi.u_ettent1, fi.u_ettent2, fi.amostra, fi.u_epref, fi.tabiva, fi.iva, fi.opcao, fi.token, fi.id_dispensa_eletronica_d AS id_validacaodem; 
 FROM fi; 
 LEFT JOIN uCrsCabVendas ON LEFT(astr(fi.lordem), 2)=LEFT(astr(ucrscabvendas.lordem), 2); 
 LEFT JOIN ft ON ALLTRIM(UPPER(ft.ftstamp))=ALLTRIM(UPPER(fi.ftstamp));
 WHERE LEFT(astr(fi.lordem), 2)=LEFT(astr(lnlordem), 2) INTO CURSOR uCrsFiAuxMR READWRITE
  

 SELECT ucrsfiauxmr
 GOTO TOP
 SCAN
    IF  .NOT. EMPTY(ucrsfiauxmr.fistamp)
       SELECT ucrsvaleauxmr
       GOTO TOP
       SCAN FOR ALLTRIM(ucrsvaleauxmr.fistamp)=ALLTRIM(ucrsfiauxmr.fistamp)
       	
          DO CASE
             CASE ALLTRIM(ucrsvaleauxmr.u_receita)<>ALLTRIM(ucrsfiauxmr.receita)
             	uf_gerais_registaerrolog("Mantem Receita a False: " +ALLTRIM(ucrsvaleauxmr.fistamp), "M1")
                lcmantemreceita = .F.
             ** lote   
			&& CASE ALLTRIM(ucrsvaleauxmr.u_nbenef2)<>ALLTRIM(ucrsfiauxmr.u_nbenef2) .AND. !EMPTY(ucrsvaleauxmr.u_lote) 
            &&   uf_gerais_registaerrolog("Mantem Receita a False: " +ALLTRIM(ucrsvaleauxmr.fistamp), "M3")
            &&    lcmantemreceita = .F.
            
 
             CASE ucrsvaleauxmr.u_recdata<>ucrsfiauxmr.datareceita
             	uf_gerais_registaerrolog("Mantem Receita a False: " +ALLTRIM(ucrsvaleauxmr.fistamp), "M4")
                lcmantemreceita = .F.
             CASE ucrsvaleauxmr.u_epvp<>ucrsfiauxmr.u_epvp 
             	uf_gerais_registaerrolog("Mantem Receita a False: " +ALLTRIM(ucrsvaleauxmr.fistamp), "M5")
                lcmantemreceita = .F.
             CASE ucrsvaleauxmr.u_ettent1<>ucrsfiauxmr.u_ettent1
             	uf_gerais_registaerrolog("Mantem Receita a False: " +ALLTRIM(ucrsvaleauxmr.fistamp), "M6")
                lcmantemreceita = .F.
             CASE ucrsvaleauxmr.u_ettent2<>ucrsfiauxmr.u_ettent2
             	uf_gerais_registaerrolog("Mantem Receita a False: " +ALLTRIM(ucrsvaleauxmr.fistamp), "M7")
                lcmantemreceita = .F.
             CASE ALLTRIM(ucrsvaleauxmr.u_diploma)<>ALLTRIM(ucrsfiauxmr.u_diploma)
             	uf_gerais_registaerrolog("Mantem Receita a False: " +ALLTRIM(ucrsvaleauxmr.fistamp), "M8")
                lcmantemreceita = .F.
             CASE ucrsvaleauxmr.opcao<>ucrsfiauxmr.opcao
             	uf_gerais_registaerrolog("Mantem Receita a False: " +ALLTRIM(ucrsvaleauxmr.fistamp), "M9")
                lcmantemreceita = .F.
             CASE ALLTRIM(ucrsvaleauxmr.token)<>ALLTRIM(ucrsfiauxmr.token)
             	uf_gerais_registaerrolog("Mantem Receita a False: " +ALLTRIM(ucrsvaleauxmr.fistamp), "M10")
                lcmantemreceita = .F.
             CASE ALLTRIM(ucrsvaleauxmr.id_validacaodem)<>ALLTRIM(ucrsfiauxmr.id_validacaodem)
             	uf_gerais_registaerrolog("Mantem Receita a False: " +ALLTRIM(ucrsvaleauxmr.fistamp), "M11")
                lcmantemreceita = .F.
             CASE ucrsvaleauxmr.u_codigo<>ucrsfiauxmr.u_codigo
             	uf_gerais_registaerrolog("Mantem Receita a False: " +ALLTRIM(ucrsvaleauxmr.fistamp), "M12")
                lcmantemreceita = .F.
             CASE ucrsvaleauxmr.u_codigo2<>ucrsfiauxmr.u_codigo2
             	uf_gerais_registaerrolog("Mantem Receita a False: " +ALLTRIM(ucrsvaleauxmr.fistamp), "M13")
                lcmantemreceita = .F.
             CASE ucrsvaleauxmr.dev<>ucrsfiauxmr.dev
             	uf_gerais_registaerrolog("Mantem Receita a False: " +ALLTRIM(ucrsvaleauxmr.fistamp), "M14")
                lcmantemreceita = .F.
                EXIT
             CASE  .NOT. EMPTY(ALLTRIM(ucrsvaleauxmr.u_receita)) .AND. EMPTY(ucrsvaleauxmr.u_lote) .AND. EMPTY(ucrsvaleauxmr.u_lote2) .AND. EMPTY(ALLTRIM(ucrsvaleauxmr.id_validacaodem))
             	uf_gerais_registaerrolog("Mantem Receita a False: " +ALLTRIM(ucrsvaleauxmr.fistamp), "M15")
                lcmantemreceita = .F.   
             
             
             ** senao lote vazio, se receita vazia, se original � um documento suspenso, se o actual n�o est� suspenso
             CASE EMPTY(ucrsvaleauxmr.u_lote) AND EMPTY(ucrsvaleauxmr.u_receita) AND uf_gerais_verificarStringNoFinalDoNome(ALLTRIM(ucrsvaleauxmr.nmdoc),"(S)") AND !uf_gerais_linha_suspensa_por_ordem(uCrsFiAuxMR.lordem)
                uf_gerais_registaerrolog("Mantem Receita a False: " +ALLTRIM(ucrsvaleauxmr.fistamp), "M21")
                lcmantemreceita = .F.     
                       
             CASE ucrsfiauxmr.fact=.T.
             	uf_gerais_registaerrolog("Mantem Receita a True: " +ALLTRIM(ucrsvaleauxmr.fistamp), "M16")
                lcmantemreceita = .T.
             OTHERWISE
             	uf_gerais_registaerrolog("Mantem Receita a True: " +ALLTRIM(ucrsvaleauxmr.fistamp), "M17")
                lcmantemreceita = .T.
          ENDCASE
          SELECT ucrsvaleauxmr
       ENDSCAN
    ELSE
       IF  .NOT. EMPTY(ucrsfiauxmr.ref)
       	  uf_gerais_registaerrolog("Mantem Receita a False: " +ALLTRIM(ucrsfiauxmr.ref), "M18")
          lcmantemreceita = .F.
       ENDIF
    ENDIF
    IF lcmantemreceita==.F.
       EXIT
    ENDIF
    SELECT ucrsfiauxmr
 ENDSCAN
 SELECT ft2
 IF lcmantemreceita==.T. .AND.  .NOT. EMPTY(ft2.u_receita)
 	uf_gerais_registaerrolog("Mantem Receita a True: " +ALLTRIM(ft2.u_receita), "M19")
    LOCAL ft2u_receita
    ft2u_receita = ALLTRIM(ft2.u_receita)
    SELECT fi.ofistamp FROM fi WHERE LEFT(astr(fi.lordem), 2)=LEFT(astr(lnlordem), 2) INTO CURSOR uCrsFiAuxSUSP READWRITE
     
    SELECT ucrsfiauxsusp
    GOTO TOP
    SCAN
       IF  .NOT. EMPTY(ucrsfiauxsusp.ofistamp)
          lcsql = ''
          TEXT TO lcsql TEXTMERGE NOSHOW
					SELECT
						ft2.u_receita , fi.ofistamp	, ft.cobrado
					from fi (nolock)
					inner join ft (nolock) on fi.ftstamp=ft.ftstamp
					inner join ft2 (nolock) on ft2.ft2stamp=fi.ftstamp 
					where fistamp='<<ALLTRIM(uCrsFiAuxSUSP.ofistamp)>>'
          ENDTEXT

          IF  .NOT. (uf_gerais_actgrelha("", "uCrsAuxINFOSUSP", lcsql))
             uf_perguntalt_chama("Ocorreu uma anomalia ao verificar a informa��o da venda de origem. Por favor contacte o Suporte.", "OK", "", 16)
          ELSE
             SELECT ucrsauxinfosusp
             IF EMPTY(ucrsauxinfosusp.u_receita) .AND. ucrsauxinfosusp.cobrado=.T.
             	uf_gerais_registaerrolog("Mantem Receita a false: " , "M20")
                lcmantemreceita = .F.
             ENDIF
          ENDIF
          IF USED("uCrsAuxINFOSUSP")
             fecha("uCrsAuxINFOSUSP")
          ENDIF
       ENDIF
       SELECT ucrsfiauxsusp
    ENDSCAN
 ENDIF
 IF USED("uCrsFiAuxSUSP")
    fecha("uCrsFiAuxSUSP")
 ENDIF
 IF USED("uCrsFiAuxMR")
    fecha("uCrsFiAuxMR")
 ENDIF
 RETURN lcmantemreceita
ENDFUNC
**
FUNCTION uf_atendimento_compapoioespecial
 LOCAL lcreturn
 STORE '' TO lcreturn
 IF UPPER(ALLTRIM(ucrse1.u_assfarm))<>'ANF'
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			select 
				f.cnp,
				f.design,
				c.cptpladesign,
				c.grupo, 
				c.cptplacode
			from fprod f (nolock)
			inner join cptval c (nolock) on f.grupo = c.grupo
			inner join cptpla (nolock) on c.cptplacode = cptpla.codigo
			where f.ref = '<<fi.ref>>'
				  and cptpla.cptorgabrev in ('NOVARTISAPOIO','SANOFI', 'LUND', 'ASTELLAS')
			      and c.pct > 0
				  and cptpla.inactivo = 0
    ENDTEXT
 ELSE
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			select 
				f.cnp,
				f.design,
				c.cptpladesign,
				c.grupo, 
				c.cptplacode
			from fprod f (nolock)
			inner join cptval c (nolock) on f.grupo = c.grupo
			inner join cptpla (nolock) on c.cptplacode = cptpla.codigo
			where f.ref = '<<fi.ref>>'
				  and cptpla.cptorgabrev in ('NOVARTISAPOIO','OPP','PSOPortugal','SANOFI', 'LUND', 'ASTELLAS')
			      and c.pct > 0
				  and cptpla.inactivo = 0
    ENDTEXT
 ENDIF
 IF  .NOT. (uf_gerais_actgrelha("", "uCrsAuxPrograma", lcsql))
    uf_perguntalt_chama("Ocorreu uma anomalia ao verificar a informa��o do produto. Por favor contacte o Suporte.", "OK", "", 16)
 ELSE
    IF RECCOUNT("uCrsAuxPrograma")=0
       lcreturn = ''
    ELSE
       lcreturn = ALLTRIM(ucrsauxprograma.cptpladesign)
    ENDIF
 ENDIF
 IF USED("uCrsAuxPrograma")
    fecha("uCrsAuxPrograma")
 ENDIF
 RETURN lcreturn
ENDFUNC
**
FUNCTION uf_atendimento_devprodutosnaovendidos
 IF uf_gerais_getparameter('ADM0000000268', 'BOOL')
    IF UPPER(ALLTRIM(ucrse1.pais))=='PORTUGAL'
       IF uf_perguntalt_chama("ATEN��O: Vai aceitar a devolu��o de um produto que n�o foi dispensado na sua loja. Pretende continuar?", "Sim", "N�o")
          IF  .NOT. uf_gerais_getparameter_site('ADM0000000060', 'BOOL', mysite)
             PUBLIC cval
             STORE '' TO cval
             uf_tecladoalpha_chama("cval", "INTRODUZA PASSWORD PARA PERMITIR EFETUAR A DEVOLU��O DO PRODUTO:", .T., .F., 0)
             IF EMPTY(cval)
                uf_perguntalt_chama("DEVE INTRODUZIR A PASSWORD.", "OK", "", 64)
                RELEASE cval
                RETURN .F.
             ELSE
                IF  .NOT. (UPPER(ALLTRIM(cval))==UPPER(ALLTRIM(uf_gerais_getparameter('ADM0000000268', 'TEXT'))))
                   uf_perguntalt_chama("A PASSWORD N�O EST� CORRECTA.", "OK", "", 64)
                   RELEASE cval
                   RETURN .F.
                ELSE
                   RELEASE cval
                ENDIF
             ENDIF
          ENDIF
       ELSE
          RETURN .F.
       ENDIF
    ELSE
       SELECT ft2
       IF EMPTY(ft2.u_docorig)
          IF ALLTRIM(ch_grupo)<>'Administrador' .AND. ALLTRIM(ch_grupo)<>'Supervisores'
             IF  .NOT. uf_gerais_valida_password_admin('ATENDIMENTO', 'DEVOLVER PRODUTO')
                RETURN .F.
             ENDIF
          ENDIF
          PUBLIC cvalnrdoc
          STORE '' TO cvalnrdoc
          uf_tecladoalpha_chama("cvalnrdoc", "Introduza o n� do documento origem:", .F., .F., 0)
          IF EMPTY(cvalnrdoc)
             uf_perguntalt_chama("TEM DE INTRODUZIR OBRIGAT�RIAMENTE O N� DE DOCUMENTO DE ORIGEM! VOLTE A EFETUAR A DEVOLU��O.", "OK", "", 64)
             RETURN (.F.)
          ENDIF
          SELECT ft2
          REPLACE ft2.u_docorig WITH ALLTRIM(cvalnrdoc)
       ENDIF
    ENDIF
 ELSE
    RETURN .F.
 ENDIF
 IF (fi.partes==0)
    SELECT fi
    REPLACE fi.partes WITH 1
    uf_fi_trans_info_muda_tipo(ALLTRIM(fi.fistamp), 'E')
    atendimento.lbldev.caption = "Dev. Dinheiro"
 ELSE
    SELECT fi
    REPLACE fi.partes WITH 0
    uf_fi_trans_info_muda_tipo(ALLTRIM(fi.fistamp), 'S')
    atendimento.lbldev.caption = "Devolu��o"
 ENDIF
 lcposicfi = RECNO("FI")
 uf_atendimento_eventofi()
 TRY
    GOTO lcposicfi
 CATCH
 ENDTRY
 LOCAL lcetotal, lcvalordevolucao
 STORE 0 TO lcetotal, lcvalordevolucao
 lcetotal = atendimento.txttotalutente.value
 SELECT fi
 lcvalordevolucao = fi.etiliquido
 
 atendimento.txttotalutente.value = ROUND(lcetotal-lcvalordevolucao, 2)
 IF USED("uCrsVale")
    SELECT ucrsvale
    APPEND BLANK
    SELECT fi
    SELECT ft
    SELECT ucrsvale
    REPLACE ucrsvale.ftstamp WITH ft.ftstamp, ucrsvale.nmdoc WITH 'Venda a Dinheiro', ucrsvale.fno WITH 0, ucrsvale.qtt WITH fi.qtt, ucrsvale.qtt2 WITH fi.qtt, ucrsvale.sel WITH .T., ucrsvale.copied WITH .T., ucrsvale.eaquisicao WITH fi.qtt, ucrsvale.amostra WITH .F., ucrsvale.docinvert WITH .T., ucrsvale.fistamp WITH fi.fistamp, ucrsvale.nome WITH 'Consumidor Final', ucrsvale.no WITH 200, ucrsvale.estab WITH 0, ucrsvale.ncont WITH uf_gerais_getparameter_site("ADM0000000075", "text", mysite), ucrsvale.ref WITH fi.ref, ucrsvale.design WITH fi.design, ucrsvale.etiliquido WITH fi.etiliquido, ucrsvale.etiliquido2 WITH fi.etiliquido, ucrsvale.epv WITH fi.epv, ucrsvale.ndoc WITH ft.ndoc, ucrsvale.tabiva WITH fi.tabiva, ucrsvale.iva WITH fi.iva, ucrsvale.token WITH '', ucrsvale.dev WITH 1, ucrsvale.u_txcomp WITH 0, ucrsvale.u_epvp WITH fi.epv
 ELSE
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_touch_regularizarVendas '', -1, '', 200, 0, '<<uf_gerais_getdate(date()-15, "SQL")>>', '<<uf_gerais_getdate(date(), "SQL")>>', '', <<ch_userno>>, '<<Alltrim(ch_grupo)>>', '<<mySite>>', 0, 0
    ENDTEXT
    	
    IF  .NOT. uf_gerais_actgrelha("", "uCrsVale", lcsql)
       uf_perguntalt_chama("Ocorreu uma anomalia ao devolver o produto. Por favor contacte o Suporte. Obrigado.", "OK", "", 16)
       RETURN .F.
    ENDIF
    SELECT ucrsvale
    APPEND BLANK
    SELECT fi
    SELECT ft
    SELECT ucrsvale
    REPLACE ucrsvale.ftstamp WITH ft.ftstamp, ucrsvale.nmdoc WITH 'Venda a Dinheiro', ucrsvale.fno WITH 0, ucrsvale.qtt WITH fi.qtt, ucrsvale.qtt2 WITH fi.qtt, ucrsvale.sel WITH .T., ucrsvale.copied WITH .T., ucrsvale.eaquisicao WITH fi.qtt, ucrsvale.docinvert WITH .T., ucrsvale.fistamp WITH fi.fistamp, ucrsvale.nome WITH 'Consumidor Final', ucrsvale.no WITH 200, ucrsvale.estab WITH 0, ucrsvale.ncont WITH uf_gerais_getparameter_site("ADM0000000075", "text", mysite), ucrsvale.ref WITH fi.ref, ucrsvale.design WITH fi.design, ucrsvale.etiliquido WITH fi.etiliquido, ucrsvale.etiliquido2 WITH fi.etiliquido, ucrsvale.epv WITH fi.epv, ucrsvale.ndoc WITH ft.ndoc, ucrsvale.tabiva WITH fi.tabiva, ucrsvale.iva WITH fi.iva, ucrsvale.token WITH '', ucrsvale.dev WITH 1, ucrsvale.u_txcomp WITH 0, ucrsvale.u_epvp WITH fi.epv
 ENDIF
ENDFUNC
**
PROCEDURE uf_atendimento_calcidade
 LOCAL lcidade, lcidadeadqui
 STORE 0 TO lcidade, lcidadeadqui
 IF uf_gerais_getdate(ucrsatendcl.nascimento, "SQL")<>'19000101'
    lcidade = STR(uf_gerais_getIdade(ucrsatendcl.nascimento))
 ELSE
    lcidade = STR(lcidade)
 ENDIF
 IF uf_gerais_getdate(ucrsatendcl.u_dcutavi, "SQL")<>'19000101'
    lcidade = STR(uf_gerais_getIdade(ucrsatendcl.u_dcutavi))
 ELSE
 ENDIF
 SELECT ucrsatendcl
 GOTO TOP
 IF VAL(lcidade)<=1 .AND. uf_gerais_getdate(ucrsatendcl.nascimento, "SQL")<>'19000101'
    SELECT nascimento, DATE(), IIF(YEAR(nascimento)=YEAR(DATE()), MONTH(DATE())-MONTH(nascimento)+1, 12-MONTH(nascimento)+1+(12*(YEAR(DATE())-YEAR(nascimento)-1))+MONTH(DATE())) AS meses FROM uCrsatendCl INTO CURSOR uCrstempIdadeMeses READWRITE
    atendimento.pgfdados.page1.txtidade.value = astr(ucrstempidademeses.meses)+" Meses"
    fecha("uCrstempidademeses")
 ELSE
    atendimento.pgfdados.page1.txtidade.value = ALLTRIM(lcidade)+" Anos"
 ENDIF
 SELECT ucrsatendcl
 GOTO TOP
 IF VAL(lcidade)<=1 .AND. uf_gerais_getdate(ucrsatendcl.u_dcutavi, "SQL")<>'19000101'
    SELECT u_dcutavi, DATE(), IIF(YEAR(u_dcutavi)=YEAR(DATE()), MONTH(DATE())-MONTH(u_dcutavi)+1, 12-MONTH(u_dcutavi)+1+(12*(YEAR(DATE())-YEAR(u_dcutavi)-1))+MONTH(DATE())) AS meses FROM uCrsatendCl INTO CURSOR uCrstempIdadeMeses READWRITE
    atendimento.pgfdados.page1.txtiddadquirente.value = astr(ucrstempidademeses.meses)+" Meses"
    fecha("uCrstempidademeses")
 ELSE
    atendimento.pgfdados.page1.txtiddadquirente.value = ALLTRIM(lcidade)+" Anos"
    REPLACE ucrsatendcl.u_idutavi WITH VAL(lcidade)
 ENDIF
ENDPROC
**
FUNCTION uf_atendimento_validacardid
 LPARAMETERS lncardid, uv_apagaCompart
 LOCAL lctoken, lcentity, lcclid, lcinfoerro, lcvalida, lcsocio
 STORE '' TO lcinfoerro
 STORE .F. TO lcvalida
 SELECT ucrse1
 lcclid = ALLTRIM(ucrse1.id_lt)
 lcnomejar = "LTSESBAssociateCardsClient.jar"
 lctoken = uf_gerais_stamp()
 lcentity = ALLTRIM(STR(ucrsatendcomp.entidade))

 IF AT("SOCIOS", ALLTRIM(UPPER(ucrsatendcomp.design)))>0
    lcentity = lcentity+"S"
 ENDIF
 regua(1, 2, "A efetuar a valida��o do cart�o por favor aguarde.")
 lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\LTSESBAssociateCardsClient\'+ALLTRIM(lcnomejar)+' "--token='+lctoken+'"'+' "--entity='+lcentity+'"'+' "--cardid='+lncardid+'"'+' "--clid='+lcclid+'"'+' "--site='+UPPER(ALLTRIM(mysite))+'"'+' "--stamp='+ALLTRIM(nratendimento)+'"'
 lcwspath = "javaw -jar " + lcwspath
 owsshell = CREATEOBJECT("WScript.Shell")
 owsshell.run(lcwspath, 1, .T.)
 lcsql = ''

 SELECT ucrsatendcomp

 xManipulado = IIF(ucrsatendcomp.manipulado, 1, 0)
 TEXT TO lcsql TEXTMERGE NOSHOW
		EXEC up_atendimento_getAssociateCardInfo '<<lcToken>>', '<<ALLTRIM(ucrsatendcomp.compl)>>', <<xManipulado>>
 ENDTEXT
 _Cliptext = lcsql
 IF  .NOT. uf_gerais_actgrelha("", "uCrsInfoAux", lcsql)
    uf_perguntalt_chama("N�o foi possivel validar o cart�o. Por favor contacte o suporte.", "OK", "", 32)
    IF uv_apagaCompart
        uf_atendimento_removecompart(.T.)
    ENDIF
    regua(2)
    RETURN .F.
 ELSE
    IF RECCOUNT("uCrsInfoAux")>0
    	

	   uf_gerais_actGrelha("", "uc_textoMedis", "exec up_atendimento_devolveTexto_Medis '" + ALLTRIM(lcToken) + "'")

	   uv_obs = IIF(!EMPTY(uc_textoMedis.texto), CHR(13) + CHR(13) + ALLTRIM(uc_textoMedis.texto), "")

       IF EMPTY(ucrsinfoaux.status)
          regua(2)
          uf_perguntalt_chama("Cart�o inv�lido para efeitos de comparticipa��o. Confirme se o n�mero inserido est� correcto." + uv_obs, "OK", "", 32)
          IF uv_apagaCompart
              uf_atendimento_removecompart(.T.)
          ENDIF
       ELSE

          regua(2)          
          atendimento.pgfdados.page3.txtncartaocompart.backcolor = RGB(0,  167, 231)
          lcvalida = .T.

         IF EMPTY(uCrsInfoAux.recomendedPlan)

            uf_perguntalt_chama("Cart�o validado com sucesso." + uv_obs, "OK", "", 32)
         
         ELSE

            uf_perguntalt_chama("Cart�o validado com sucesso." + uv_obs + CHR(13) + "Sob recomenda��o da entidade ser� aplicado o seguinte plano:" + CHR(13) + ALLTRIM(uCrsInfoAux.recomendedPlan) + ' ' + ALLTRIM(uCrsInfoAux.descPlan) , "OK", "", 32)
            uf_atendimento_aplicacompart(uCrsInfoAux.recomendedPlan, IIF(WEXIST("ATENDIMENTO"), .T., .F.))
            
          ENDIF

       ENDIF
    ELSE
       lcsql = ''
       TEXT TO lcsql TEXTMERGE NOSHOW
				SELECT * FROM ext_esb_msgStatus (nolock) WHERE token = '<<lcToken>>'
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "uCrsInfoAuxErro", lcsql)
          uf_perguntalt_chama("N�o foi possivel estabelecer a comunica��o com o servi�o de valida��o. Por favor contacte o suporte.", "OK", "", 32)
          IF uv_apagaCompart
              uf_atendimento_removecompart(.T.)
          ENDIF
          regua(2)
          RETURN .F.
       ELSE
          SELECT ucrsinfoauxerro
          GOTO TOP
          IF  .NOT. EMPTY(ucrsinfoauxerro.descr)
             lcinfoerro = ALLTRIM(ucrsinfoauxerro.descr)
          ELSE
             lcinfoerro = 'N�o foi possivel estabelecer a comunica��o com o servi�o de valida��o. Por favor contacte o suporte.'
          ENDIF
          uf_perguntalt_chama(LEFT(lcinfoerro, 200), "OK", "", 48)
          IF uv_apagaCompart
              uf_atendimento_removecompart(.T.)
          ENDIF
          regua(2)
       ENDIF
    ENDIF
 ENDIF
 SELECT ucrsatendcomp
 SELECT fi
 REPLACE fi.u_nbenef2 WITH ucrsatendcomp.ncartao
 IF USED("uCrsInfoAux")
    fecha("uCrsInfoAux")
 ENDIF
 IF USED("uCrsInfoAuxErro")
    fecha("uCrsInfoAuxErro")
 ENDIF
 RETURN lcvalida
ENDFUNC
**
FUNCTION uf_atendimento_selclientepsico
 LPARAMETERS lcno, lcestab
 IF EMPTY(lcno) .OR.  .NOT. TYPE("lcNO")=="N"
    lcno = 200
 ENDIF
 IF EMPTY(lcestab) .OR.  .NOT. TYPE("lcEstab")=="N"
    lcestab = 0
 ENDIF
 IF  .NOT. uf_gerais_actgrelha("", 'uCrsAtendCLPsico', 'Exec up_touch_dadosCliente '+astr(lcno)+','+astr(lcestab)+'')
    uf_perguntalt_chama("Ocorreu uma anomalia a obter os dados do Cliente.", "OK", "", 16)
    RETURN .F.
 ELSE
    IF RECCOUNT("uCrsAtendCLPSICO")>0

       SELECT ucrsatendcl
       
       REPLACE ;
			   ucrsatendcl.u_nmutavi WITH ALLTRIM(ucrsatendclpsico.nome),; 
            ucrsatendcl.u_moutavi WITH ALLTRIM(ucrsatendclpsico.morada),; 
            ucrsatendcl.u_cputavi WITH ALLTRIM(ucrsatendclpsico.codpost),;
            ucrsatendcl.u_ndutavi WITH ALLTRIM(ucrsatendclpsico.bino),; 
            ucrsatendcl.u_dcutavi WITH ucrsatendclpsico.nascimento,;
            ucrsatendcl.u_ddutavi WITH ucrsatendclpsico.u_ddutavi,;
            ucrsatendcl.u_adUtstamp WITH ucrsatendclpsico.utstamp,;
            ucrsatendcl.codDocID WITH ucrsatendclpsico.codDocIDUT,;
            ucrsatendcl.contactoUT WITH IIF(!EMPTY(ucrsatendclpsico.tlmvl), ucrsatendclpsico.tlmvl, ucrsatendclpsico.telefone)
       
       PUBLIC up_moradaAd, up_codPostAd, up_bino, up_validadeFim, up_nascimento, up_sexoUT

       up_moradaAd = ucrsatendcl.u_moutavi
       up_codPostAd = ucrsatendcl.u_cputavi
       up_bino = ucrsatendcl.u_ndutavi
       up_validadeFim = ucrsatendcl.u_ddutavi
       up_nascimento = ucrsatendcl.u_dcutavi
       up_sexoUT = ucrsatendcl.sexo

       LOCAL lcidade
       lcidade = 0
       IF uf_gerais_getdate(ucrsatendclpsico.nascimento, "SQL")<>'19000101'
          lcidade = STR(uf_gerais_getIdade(ucrsatendclpsico.nascimento))
       ELSE
          lcidade = STR(lcidade)
       ENDIF
       IF VAL(lcidade)<=3 .AND. uf_gerais_getdate(ucrsatendclpsico.nascimento, "SQL")<>'19000101'
          SELECT nascimento, DATE(), IIF(YEAR(nascimento)=YEAR(DATE()), MONTH(DATE())-MONTH(nascimento)+1, 12-MONTH(nascimento)+1+(12*(YEAR(DATE())-YEAR(nascimento)-1))+MONTH(DATE())) AS meses FROM uCrsAtendCLPSICO INTO CURSOR ucrstempidademeses READWRITE
          SELECT ucrstempidademeses
          atendimento.pgfdados.page1.txtiddadquirente.value = astr(ucrstempidademeses.meses)+" meses"
          REPLACE ucrsatendcl.u_idutavi WITH VAL(lcidade)
          fecha("ucrstempidademeses")
       ELSE
          atendimento.pgfdados.page1.txtiddadquirente.value = ALLTRIM(lcidade)+" anos"
          REPLACE ucrsatendcl.u_idutavi WITH VAL(lcidade)
       ENDIF
    ENDIF
 ENDIF
 atendimento.grdatend.setfocus()
 IF USED("uCrsAtendCLPsico")
    fecha("uCrsAtendCLPsico")
 ENDIF
 atendimento.refresh()
 IF atendimento.pgfdados.activepage<>1
    atendimento.pgfdados.activepage = 1
 ENDIF
ENDFUNC
**
PROCEDURE uf_atendimento_dadosdemchama
 LOCAL lcconsultaanterior, lcnrlinlordem, lcnrlincabv, lcrecnolin, lcindlin
 STORE .F. TO lcconsultaanterior
 uf_chk_nova_versao()
 
 if(!USED("fi"))    
 	return .f. 
 ENDIF
 
 SELECT fi
 IF fi.lordem=0
    SELECT fi
    GOTO TOP
 ENDIF
 lcnrlinlordem = fi.lordem
 lcnrlincabv = VAL(LEFT(astr(fi.lordem), 1))*100000
 lcindlin = VAL(LEFT(astr(fi.lordem), 1))
 nrlincabv = VAL(LEFT(astr(fi.lordem), 1))*100000
 SELECT fi
 GOTO TOP
 SCAN
    IF fi.lordem=lcnrlincabv
       lcrecnolin = RECNO()
    ENDIF
    IF lcindlin=VAL(LEFT(astr(fi.lordem), 1))
       lastlin = RECNO()
    ENDIF
 ENDSCAN
 SELECT fi
 GOTO lcrecnolin
 atendimento.grdatend.refresh
 uf_dadosdem_chama(.F., lcconsultaanterior)
ENDPROC
**
PROCEDURE uf_atendimento_controlaexcecaocond
 LOCAL lclyricacompespecial
 STORE .F. TO lclyricacompespecial
 SELECT fi
 IF UPPER(ALLTRIM(fi.design))='LYRICA'
    lclyricacompespecial = .T.
 ENDIF
 SELECT fi
 IF ALLTRIM(fi.lobs3)<>"C1" .AND. ALLTRIM(fi.opcao)=='S'
    REPLACE fi.opcao WITH 'N'
 ENDIF
 SELECT fi
 IF EMPTY(ALLTRIM(fi.lobs3))
    SELECT fi
    SELECT ucrsatendst
    DO CASE
       CASE ucrsatendst.grphmgcode=='' .OR. ucrsatendst.grphmgcode=='GH0000'
          REPLACE fi.opcao WITH 'I'
       CASE fi.u_epvp>fi.pvpmaxre
          REPLACE fi.opcao WITH 'S'
       OTHERWISE
          REPLACE fi.opcao WITH 'N'
    ENDCASE
 ENDIF
 IF  .NOT. EMPTY(lclyricacompespecial)
    uf_eventoactvalores()
 ENDIF
ENDPROC
**
FUNCTION uf_imprimir_guiaaviamento
 LPARAMETERS lcstamp, lcpainel, lcexporta
 
	LOCAL lcRefConcat
	STORE "" TO lcRefConcat
	

	PUBLIC mypaisnacional
 
	IF USED("uCrsFiAux")
   		fecha('uCrsFiAux')
 	ENDIF
	 
	IF USED("uCrsBenef")
	   fecha('uCrsBenef')
	ENDIF
	IF USED("uCrsFt")
	   fecha("uCrsFt")
	ENDIF
	IF USED("uCrsFt2")
	   fecha("uCrsFt2")
	ENDIF
	IF USED("uCrsFi")
	   fecha("uCrsFi")
	ENDIF
	 
	SELECT * FROM ft WITH (BUFFERING=.T.) INTO CURSOR uCrsFt READWRITE
	SELECT * FROM ft2 WITH (BUFFERING=.T.) INTO CURSOR uCrsFt2 READWRITE

	SELECT * FROM Fi WITH (BUFFERING=.T.) INTO CURSOR uCrsFiAux READWRITE	 
	lcRefConcat =  uf_gerais_concatenarCamposSql("uCrsFiAux", "ref", ",")
	TEXT TO lcsql TEXTMERGE NOSHOW			
	select 	ref
			,local
			,u_local
			,u_local2  
	from st (nolock) 
	where site_nr = <<mysite_nr>>
			and st.ref  in (<<lcRefConcat>>)
	 ENDTEXT
	IF !uf_gerais_actgrelha("", "uCrsStLocal", lcsql)
		uf_perguntalt_chama("ERRO AO CONSULTAR A LOCALIZA��O!", "OK", "", 16)
	  	RETURN .f.
	ENDIF

SELECT ; 
	FI.ref, FI.design, FI.partes, FI.ofistamp, SUM(FI.qtt) AS qtt, uCrsStLocal.local ,uCrsStLocal.u_local , uCrsStLocal.u_local2 ;
FROM fi  WITH (BUFFERING=.T.) ;
INNER JOIN uCrsStLocal ON uCrsStLocal.ref = fi.ref  ;
WHERE FI.stns=.F. ;
GROUP BY fi.ref, fi.design, fi.partes, fi.ofistamp, uCrsStLocal.local, uCrsStLocal.u_local, uCrsStLocal.u_local2 ORDER BY fi.design INTO CURSOR uCrsFi READWRITE

SELECT ucrsft
IF  .NOT. USED("uCrsFt")
   RETURN .F.
ELSE
   SELECT ucrsft
    IF  .NOT. RECCOUNT()>0
       RETURN .F.
   ENDIF
ENDIF


IF USED("uCrsStLocal")
	fecha("uCrsStLocal")
ENDIF 

 **IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
iF uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + ALLTRIM(myTerm) + "'")

	uv_idImp = uf_gerais_getUmValor("B_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Guia de Aviamento'")

	uf_imprimirgerais_sendprinter_talao(uv_idImp , .f., .F.)

 ELSE
	 
	 
	 SELECT ucrsft
	 SELECT ucrsft
	 GOTO TOP
	 SELECT ucrsft2
	 GOTO TOP
	 SELECT ucrsfi
	 GOTO TOP
	 lcvalidaimp = uf_gerais_setimpressorapos(.T.)
	 IF lcvalidaimp
	    ??? CHR(27E0)+CHR(64E0)
	    ??? CHR(27)+CHR(99)+CHR(48)+CHR(1)
	    ??? CHR(27E0)+CHR(116)+CHR(003)
	    ??? CHR(27)+CHR(33)+CHR(1)
	    ?? " "
	    ? LEFT(ALLTRIM(ucrse1.nomecomp), uf_gerais_getparameter("ADM0000000194", "NUM"))
	    ??? CHR(27)+CHR(33)+CHR(1)
	    ? SUBSTR(ucrse1.local, 1, 10)
	    ?? " Tel:"
	    ?? TRANSFORM(ucrse1.telefone, "#########")
	    ?? " NIF:"
	    ?? TRANSFORM(ucrse1.ncont, "999999999")
       ? ALLTRIM(uf_gerais_getMacrosReports(2))
	    ? "Dir. Tec. "
	    ?? LEFT(ALLTRIM(ucrse1.u_dirtec), uf_gerais_getparameter("ADM0000000194", "NUM"))
	    ??
	    uf_gerais_separadorpos()
	    ? PADC("GUIA DE AVIAMENTO", uf_gerais_getparameter("ADM0000000194", "NUM"), " ")
	    uf_gerais_separadorpos()
	    SELECT ucrsft
	    ? DATETIME()
	    SELECT ucrsft
	    ? "Op: "
	    ?? SUBSTR(ucrsft.vendnm, 1, 40)+" ("+ALLTRIM(STR(ucrsft.vendedor))+")"
	    ? " "
	    ? "Produto                                    Qtt"
	    uf_gerais_separadorpos()
	    SELECT ucrsfi
	    SCAN FOR  .NOT. EMPTY(ucrsfi.ref) .AND. ucrsfi.partes==0
	       IF EMPTY(ucrsfi.ofistamp)
	          ? SUBSTR(ucrsfi.ref, 1, 30)+"                         "+TRANSFORM(ucrsfi.qtt, "###")
	          ? LEFT(ALLTRIM(ucrsfi.design), 70)
	       		IF !EMPTY(ucrsfi.local)
	       			? "L1: " + ALLTRIM(ucrsfi.local)
	       		ENDIF
	       		IF !EMPTY(ucrsfi.u_local)
	       			? "L2: " + ALLTRIM(ucrsfi.u_local)
	       		ENDIF
	       		IF !EMPTY(ucrsfi.u_local2)
	       			? "L3: " + ALLTRIM(ucrsfi.u_local2)
	       		ENDIF
	          uf_gerais_separadorpos()
	       ENDIF
	    ENDSCAN
	    uf_gerais_feedcutpos()
	 ENDIF
	 uf_gerais_setimpressorapos(.F.)
	 SET PRINTER TO DEFAULT
 ENDIF
	 
 IF USED("uCrsBenef")
    fecha('uCrsBenef')
 ENDIF
 IF USED("uCrsFt")
    fecha("uCrsFt")
 ENDIF
 IF USED("uCrsFt2")
    fecha("uCrsFt2")
 ENDIF
 IF USED("uCrsFi")
    fecha("uCrsFi")
 ENDIF
ENDFUNC
**
FUNCTION uf_imprimir_nao_dispensados
 LPARAMETERS lcstamp, lcpainel, lcexporta
 PUBLIC mypaisnacional
 SELECT ft
 IF  .NOT. USED("Ft")
    RETURN .F.
 ELSE
    SELECT ft
    IF  .NOT. RECCOUNT()>0
       RETURN .F.
    ENDIF
 ENDIF
 SELECT ft
 GOTO TOP
 SELECT ft2
 GOTO TOP
 SELECT fi
 GOTO TOP
 SELECT DISTINCT receita FROM uCrsDEMndisp WITH (BUFFERING=.T.) INTO CURSOR uCrsreceitas READWRITE

 **IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
 IF uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + ALLTRIM(myTerm) + "'")

   uv_idImp = uf_gerais_getUmValor("B_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Medicamentos'")

   uf_imprimirgerais_sendprinter_talao(uv_idImp , .f., .F.)
   

 ELSE

   lcvalidaimp = uf_gerais_setimpressorapos(.T.)
   IF lcvalidaimp
      ??? CHR(27E0)+CHR(64E0)
      ??? CHR(27)+CHR(99)+CHR(48)+CHR(1)
      ??? CHR(27E0)+CHR(116)+CHR(003)
      ??? CHR(27)+CHR(33)+CHR(1)
      ?? " "
      ? LEFT(ALLTRIM(ucrse1.nomecomp), uf_gerais_getparameter("ADM0000000194", "NUM"))
      ??? CHR(27)+CHR(33)+CHR(1)
      ? SUBSTR(ucrse1.local, 1, 10)
      ?? " Tel:"
      ?? TRANSFORM(ucrse1.telefone, "#########")
      ?? " NIF:"
      ?? TRANSFORM(ucrse1.ncont, "999999999")
      ? ALLTRIM(uf_gerais_getMacrosReports(2))
      ? "Dir. Tec. "
      ?? LEFT(ALLTRIM(ucrse1.u_dirtec), uf_gerais_getparameter("ADM0000000194", "NUM"))
      ??
      uf_gerais_separadorpos()
      ? PADC("MEDICAMENTOS POR DISPENSAR", uf_gerais_getparameter("ADM0000000194", "NUM"), " ")
      uf_gerais_separadorpos()
      SELECT ft
      ? DATETIME()
      SELECT ft
      ? "Op: "
      ?? SUBSTR(ft.vendnm, 1, 40)+" ("+ALLTRIM(STR(ft.vendedor))+")"
      ? " "
      ? "Cliente: "+ALLTRIM(ft.nome)
      ? "Nr.: "+astr(ft.no)
      SELECT ucrsreceitas
      GOTO TOP
      SCAN
         uf_gerais_separadorpos()
         ? " "
         ? "Receita Nr.: "+ALLTRIM(ucrsreceitas.receita)
         ? " "
         ? "Codigo de acesso:  ____________________ "
         ? " "
         ? "Codigo de op��o :  ____________________ "
         ? " "
         ? "PRODUTO(S)"
         uf_gerais_separadorpos()
         SELECT ucrsdemndisp
         SCAN FOR ALLTRIM(ucrsdemndisp.receita)==ALLTRIM(ucrsreceitas.receita)
            ? ALLTRIM(ucrsdemndisp.medicamento_descr)
            ? "Validade Prescricao: "+DTOC(ucrsdemndisp.validade_linha)+"        Quant: "+ALLTRIM(TRANSFORM(ucrsdemndisp.tot, "999"))
            uf_gerais_separadorpos()
         ENDSCAN
      ENDSCAN
      uf_gerais_feedcutpos()
   ENDIF
   uf_gerais_setimpressorapos(.F.)
   SET PRINTER TO DEFAULT
 ENDIF
 IF USED("uCrsreceitas")
    fecha('uCrsreceitas')
 ENDIF
ENDFUNC
**
PROCEDURE uf_atendimento_pagar_varios
 LPARAMETERS lcclno, lcclestab, lcvarios
 uf_regvendas_chama()
 

 
 if(lcclestab==0)
 	lcclestab = -1
 ENDIF
  
 regvendas.txtno.value = lcclno
 regvendas.txtestab.value = lcclestab
ENDPROC
**
PROCEDURE uf_atendimento_pagar_variosat
 uf_atendimento_pagar_varios(ucrsatendcl.no, ucrsatendcl.estab, .F.)
ENDPROC
**
PROCEDURE uf_novonordisk_remove
 LPARAMETERS lcfistamp
 IF RECCOUNT("fi_nnordisk")>0
    SELECT fi_nnordisk
    GOTO TOP
    SCAN
       IF ALLTRIM(fi_nnordisk.fistamp)==ALLTRIM(lcfistamp)
          SELECT fi_nnordisk
          DELETE
       ENDIF
    ENDSCAN
 ENDIF
ENDPROC
**
PROCEDURE uf_atendimento_fi2_linha_remove
 LPARAMETERS lcfistamp
 **IF usamancompart
    IF RECCOUNT("fi2")>0
       SELECT fi2
       GOTO TOP
       SCAN
          IF ALLTRIM(fi2.fistamp)==ALLTRIM(lcfistamp)
             SELECT fi2
             DELETE
          ENDIF
       ENDSCAN
    ENDIF
 **ENDIF
ENDPROC
**
PROCEDURE uf_atendimento_fimancompart_linha_remove
 LPARAMETERS lcfistamp
 

 IF usamancompart
    IF RECCOUNT("fimancompart")>0
       SELECT fimancompart
       GOTO TOP
       SCAN
          IF ALLTRIM(fimancompart.fistamp)==ALLTRIM(lcfistamp)
             SELECT fimancompart
             DELETE
          ENDIF
       ENDSCAN
    ENDIF
 ENDIF
ENDPROC
**
PROCEDURE uf_atendimento_escolhejustificaogeral
 LPARAMETERS lctipo
 IF (USED("fi") .AND. USED("fi2"))
    PUBLIC mycodigorest
    LOCAL lcposfi, lcposfi2, lcfistamp
    mycodigorest = "XXX"
    lcposfi = RECNO("fi")
    lcposfi2 = RECNO("fi2")
    SELECT fi
    lcfistamp = ALLTRIM(fi.fistamp)
    TEXT TO lcsql TEXTMERGE NOSHOW
			select codigo,descr from justificao_geral(nolock) where tipo = '<<ALLTRIM(lcTipo)>>'
    ENDTEXT
    IF uf_gerais_actgrelha("", "uCrsTempJustif", lcsql)
       uf_valorescombo_chama("myCodigoRest", 0, "uCrsTempJustif", 2, "codigo", "codigo, descr ", .F., .F., .T., .F.)
    ENDIF
    IF ( .NOT. EMPTY(mycodigorest))
       SELECT ucrstempjustif
       LOCATE FOR ucrstempjustif.codigo=mycodigorest
       LOCAL lccodigo, lcdescr
       STORE '' TO lccodigo, lcdescr
       IF FOUND() .AND.  .NOT. EMPTY(lcfistamp)
          lccodigo = ALLTRIM(ucrstempjustif.codigo)
          lcdescr = ALLTRIM(ucrstempjustif.descr)
          UPDATE fi2 SET justificacao_tecnica_cod = lccodigo, justificacao_tecnica_descr = lcdescr WHERE fistamp=lcfistamp
       ENDIF
    ELSE
       UPDATE fi2 SET justificacao_tecnica_cod = '', justificacao_tecnica_descr = '' WHERE fistamp=lcfistamp
    ENDIF
    IF (USED("uCrsTempJustif"))
       fecha("uCrsTempJustif")
    ENDIF
    RELEASE mycodigorest
    SELECT fi
    TRY
       GOTO lcposfi
    CATCH
       GOTO BOTTOM
    ENDTRY
    SELECT fi2
    TRY
       GOTO lcposfi2
    CATCH
       GOTO BOTTOM
    ENDTRY
 ENDIF
ENDPROC
**
FUNCTION uf_sms_posologia
 LOCAL lcmsg, lccodenvio, lcdestino, lcwspath, lcutstamp
 STORE LEFT(ALLTRIM(m_chinis)+DTOC(DATE())+SYS(2015), 25) TO lccodenvio
 TEXT TO lcsql TEXTMERGE NOSHOW
		select utstamp, email, autoriza_emails, tlmvl, autoriza_sms from b_utentes (nolock) where no=<<ft.No>> and estab=<<ft.Estab>>
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "ucrsDadosCL", lcsql)
    uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!", "OK", "", 16)
    RETURN .F.
 ENDIF
 IF  .NOT. EMPTY(ucrsdadoscl.tlmvl)
    IF ucrsdadoscl.autoriza_sms=.F.
       uf_perguntalt_chama("O UTENTE N�O AUTORIZOU O ENVIO DE SMS'S!", "OK", "", 16)
       RETURN .F.
    ELSE
       lcdestino = ALLTRIM(ucrsdadoscl.tlmvl)
       lcutstamp = ALLTRIM(ucrsdadoscl.utstamp)
    ENDIF
 ELSE
    uf_perguntalt_chama("O UTENTE N�O TEM O N�MERO DE TELEM�VEL PREENCHIDO NA SUA FICHA!", "OK", "", 16)
    RETURN .F.
 ENDIF
 fecha("ucrsDadosCL")
 lcmsg = 'Envio Posologia utente '+ALLTRIM(ft.nome)+' '
 SELECT ucrsatendst
 GOTO TOP
 SCAN
    IF  .NOT. EMPTY(ALLTRIM(LEFT(u_posprog, 254)))
       lcmsg = lcmsg + CHR(13) + " | "+ALLTRIM(ucrsatendst.design)+" : "+ALLTRIM(u_posprog) + CHR(13) + CHR(10)
    ENDIF
 ENDSCAN
 uf_send_sms(lccodenvio, 'POSOLOGIA', 'Atendimento', ALLTRIM(lcmsg), lcdestino, 'CL', lcutstamp, 'Envio de posologia por SMS para o nr '+ALLTRIM(lcdestino))
ENDFUNC
**
FUNCTION uf_email_posologia
 LOCAL lcto, lctoemail, lcsubject, lcbody, lcatatchment, lctoken, lctexto
 STORE '' TO lctexto
 TEXT TO lcsql TEXTMERGE NOSHOW
		select utstamp, email, autoriza_emails, tlmvl, autoriza_sms from b_utentes (nolock) where no=<<ft.No>> and estab=<<ft.Estab>>
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "ucrsDadosCL", lcsql)
    uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!", "OK", "", 16)
    RETURN .F.
 ENDIF
 IF  .NOT. EMPTY(ucrsdadoscl.email)
    IF ucrsdadoscl.autoriza_emails=.F.
       uf_perguntalt_chama("O UTENTE N�O AUTORIZOU O ENVIO DE EMAIL'S!", "OK", "", 16)
       RETURN .F.
    ELSE
       lctoemail = ALLTRIM(ucrsdadoscl.email)
       lcutstamp = ALLTRIM(ucrsdadoscl.utstamp)
    ENDIF
 ELSE
    uf_perguntalt_chama("O UTENTE N�O TEM O ENDERE�O DE EMAIL PREENCHIDO NA SUA FICHA!", "OK", "", 16)
    RETURN .F.
 ENDIF
 fecha("ucrsDadosCL")
 SELECT ft
 lctexto = 'Exmo(a). Sr(a). '+ALLTRIM(ft.nome)+' '+ CHR(13) + CHR(10)
 lctexto = lctexto+CHR(13)+'Vimos por este meio enviar a posologia dos medicamentos adquiridos na nossa farm�cia: '+ CHR(13) + CHR(10)
 lctexto = lctexto+ CHR(13) + CHR(10)
 SELECT ucrsatendst
 GOTO TOP
 SCAN
    IF  .NOT. EMPTY(ALLTRIM(LEFT(u_posprog, 254)))
       lctexto = lctexto+ CHR(13) + CHR(10)+ALLTRIM(ucrsatendst.design)+" : "+ALLTRIM(u_posprog) + CHR(13) + CHR(10)
    ENDIF
 ENDSCAN
 lctexto = lctexto+ CHR(13) + CHR(10)
 lcto = ""
 lctoemail = ALLTRIM(lctoemail)
 lcsubject = 'Envio de Posologia'
 lcbody = lctexto+CHR(13)+ALLTRIM(ucrse1.mailsign)
 lcatatchment = ""
 regua(0, 6, "A enviar email ...")
 uf_startup_sendmail_emp(lcto, lctoemail, lcsubject, lcbody, lcatatchment, .T.)
 regua(2)
 uf_user_log_ins('CL', ALLTRIM(lcutstamp), 'POSOLOGIA', 'Envio de posologia por Email para '+ALLTRIM(lctoemail))
ENDFUNC
**
FUNCTION uf_sms_nao_dispensados
 LOCAL lcmsg, lccodenvio, lcdestino, lcwspath, lcutstamp
 STORE LEFT(ALLTRIM(m_chinis)+DTOC(DATE())+SYS(2015), 25) TO lccodenvio
 STORE '' TO lcmsg
 TEXT TO lcsql TEXTMERGE NOSHOW
		select utstamp, email, autoriza_emails, tlmvl, autoriza_sms from b_utentes (nolock) where no=<<ft.No>> and estab=<<ft.Estab>>
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "ucrsDadosCL", lcsql)
    uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!", "OK", "", 16)
    RETURN .F.
 ENDIF
 IF  .NOT. EMPTY(ucrsdadoscl.tlmvl)
    IF ucrsdadoscl.autoriza_sms=.F.
       uf_perguntalt_chama("O UTENTE N�O AUTORIZOU O ENVIO DE SMS'S!", "OK", "", 16)
       RETURN .F.
    ELSE
       lcdestino = ALLTRIM(ucrsdadoscl.tlmvl)
       lcutstamp = ALLTRIM(ucrsdadoscl.utstamp)
    ENDIF
 ELSE
    uf_perguntalt_chama("O UTENTE N�O TEM O N�MERO DE TELEM�VEL PREENCHIDO NA SUA FICHA!", "OK", "", 16)
    RETURN .F.
 ENDIF
 fecha("ucrsDadosCL")
 SELECT ucrsdemndisp
 GOTO TOP
 SELECT DISTINCT receita FROM uCrsDEMndisp WITH (BUFFERING=.T.) INTO CURSOR uCrsreceitas READWRITE
 LOCAL lcnrrectemp
 STORE '' TO lcnrrectemp
 SELECT ucrsreceitas
 GOTO TOP
 SCAN
    lcnrrectemp = ALLTRIM(ucrsreceitas.receita)
    lcmsg = lcmsg+"Medicamentos por Dispensar da Receita Nr. "+ALLTRIM(lcnrrectemp)+" " + CHR(13) + CHR(10)
    SELECT ucrsdemndisp
    GOTO TOP
    SCAN FOR ALLTRIM(ucrsdemndisp.receita)==ALLTRIM(lcnrrectemp)
       lcmsg = lcmsg+CHR(13)+" | "+ALLTRIM(ucrsdemndisp.medicamento_descr)+" Valid.: "+DTOC(ucrsdemndisp.validade_linha)+" Qtd: "+ALLTRIM(TRANSFORM(ucrsdemndisp.tot, "999")) + CHR(13) + CHR(10)
    ENDSCAN
    SELECT ucrsreceitas
 ENDSCAN
 IF USED("uCrsreceitas")
    fecha('uCrsreceitas')
 ENDIF
 uf_send_sms(lccodenvio, 'MEDNDISP', 'Atendimento', ALLTRIM(lcmsg), lcdestino, 'CL', lcutstamp, 'Envio de medicamentos n�o dispensados da por SMS para o nr '+ALLTRIM(lcdestino))
ENDFUNC
**
FUNCTION uf_email_nao_dispensados
 LOCAL lcto, lctoemail, lcsubject, lcbody, lcatatchment, lctoken, lctexto
 STORE '' TO lctexto
 TEXT TO lcsql TEXTMERGE NOSHOW
		select utstamp, email, autoriza_emails, tlmvl, autoriza_sms from b_utentes (nolock) where no=<<ft.No>> and estab=<<ft.Estab>>
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "ucrsDadosCL", lcsql)
    uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!", "OK", "", 16)
    RETURN .F.
 ENDIF
 IF  .NOT. EMPTY(ucrsdadoscl.email)
    IF ucrsdadoscl.autoriza_emails=.F.
       uf_perguntalt_chama("O UTENTE N�O AUTORIZOU O ENVIO DE EMAIL'S!", "OK", "", 16)
       RETURN .F.
    ELSE
       lctoemail = ALLTRIM(ucrsdadoscl.email)
       lcutstamp = ALLTRIM(ucrsdadoscl.utstamp)
    ENDIF
 ELSE
    uf_perguntalt_chama("O UTENTE N�O TEM O ENDERE�O DE EMAIL PREENCHIDO NA SUA FICHA!", "OK", "", 16)
    RETURN .F.
 ENDIF
 fecha("ucrsDadosCL")
 SELECT ft
 lctexto = 'Exmo(a). Sr(a). '+ALLTRIM(ft.nome)+ CHR(13) + CHR(10)
 lctexto = lctexto+'. Vimos por este meio enviar a listagem dos medicamentos n�o dispensados durante o atendimento efetuado na nossa farm�cia: '+ CHR(13) + CHR(10)
 SELECT ucrsdemndisp
 GOTO TOP
 SELECT DISTINCT receita FROM uCrsDEMndisp WITH (BUFFERING=.T.) INTO CURSOR uCrsreceitas READWRITE
 LOCAL lcnrrectemp
 STORE '' TO lcnrrectemp
 SELECT ucrsreceitas
 GOTO TOP
 SCAN
    lcnrrectemp = ALLTRIM(ucrsreceitas.receita)
    lctexto = lctexto+ CHR(13) + CHR(10)
    lctexto = lctexto+"Medicamentos por Dispensar da Receita Nr. "+ALLTRIM(lcnrrectemp)+" " + CHR(13) + CHR(10)
    lctexto = lctexto+ CHR(13) + CHR(10)
    SELECT ucrsdemndisp
    GOTO TOP
    SCAN FOR ALLTRIM(ucrsdemndisp.receita)==ALLTRIM(lcnrrectemp)
       lctexto = lctexto+CHR(13)+" | "+ALLTRIM(ucrsdemndisp.medicamento_descr)+"  Validade: "+DTOC(ucrsdemndisp.validade_linha)+"  Quantidade: "+ALLTRIM(TRANSFORM(ucrsdemndisp.tot, "999")) + CHR(13) + CHR(10)
    ENDSCAN
    SELECT ucrsreceitas
 ENDSCAN
 IF USED("uCrsreceitas")
    fecha('uCrsreceitas')
 ENDIF
 lctexto = lctexto+CHR(13)
 lcto = ""
 lctoemail = ALLTRIM(lctoemail)
 lcsubject = 'Informa��o de medica��o n�o dispensada'
 lcbody = lctexto+CHR(13)+ALLTRIM(ucrse1.mailsign)
 lcatatchment = ""
 regua(0, 6, "A enviar email ...")
 uf_startup_sendmail_emp(lcto, lctoemail, lcsubject, lcbody, lcatatchment, .T.)
 regua(2)
 uf_user_log_ins('CL', ALLTRIM(lcutstamp), 'MEDNDISP', 'Envio de listagem de medicamentos n�o dispensados por Email para '+ALLTRIM(lctoemail))
ENDFUNC
**
FUNCTION uf_compart_insert_ft_compart
 LPARAMETERS lntoken, lnftstamp, lnefrid, lncardno, lntype, lncodacesso, lnabrev, lnprogramid, lnnrreceita
 LOCAL lctoken, lcftstamp, lcefrid, lccardno, lntype, lccodacesso, lcabrevinsurance, lccodextinsurance, lcabrev, lcprogramid, lcnrreceita
 lctoken = lntoken
 lcftstamp = lnftstamp
 lcefrid = lnefrid
 lccardno = lncardno
 lctype = lntype
 lccodacesso = lncodacesso
 lcabrev = lnabrev
 lcnrreceita = lnnrreceita
 IF (EMPTY(lnprogramid))
    lcprogramid = ""
 ELSE
    lcprogramid = lnprogramid
 ENDIF
 TEXT TO lcsql TEXTMERGE NOSHOW
		select abrev, codExt, efrID from cptorg (nolock) where abrev='<<lcAbrev>>'
 ENDTEXT
 uf_gerais_actgrelha("", "ucrsCompartInf", lcsql)
 lcabrevinsurance = ALLTRIM(ucrscompartinf.abrev)
 lccodextinsurance = ALLTRIM(ucrscompartinf.codext)
 lcefrid = ALLTRIM(ucrscompartinf.efrid)
 fecha("ucrsCompartInf")
 TEXT TO lcsql TEXTMERGE NOSHOW
		insert into ft_compart (
			token
			,siteno
			,efrid
			,ftstamp
			,cardnumber
			,type
			,contributionCorrelationId
			, ousrdata
			, codacesso
			, currency
			, abrevInsurance
			, codExtInsurance
			, ousrUser
			, programId
			, nrreceita
		)values(
			'<<ALLTRIM(lctoken)>>'
			,<<uCrsE1.Siteno>>
			,'<<ALLTRIM(lcEfrID)>>'
			,'<<ALLTRIM(lcFtstamp)+ ALLTRIM(stampcompart)>>'
			,'<<ALLTRIM(lcCardNo)>>'
			,<<lcType>>
			,'<<ALLTRIM(lcCardNo)>>'
			, dateadd(HOUR, <<difhoraria>>, getdate())	
			,'<<ALLTRIM(lcCodacesso)>>'
			,'<<ALLTRIM(uf_gerais_getParameter("ADM0000000260","text"))>>'
			, '<<ALLTRIM(lcabrevInsurance)>>'
			, '<<ALLTRIM(lccodExtInsurance)>>'
			, '<<ALLTRIM(m_chinis)>>'
			, '<<ALLTRIM(lcProgramId)>>'
			, '<<ALLTRIM(lcnrreceita)>>'
		)	
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
    uf_perguntalt_chama("N�o foi possivel proceder inserir linhas na comparticipa��o. Contacte o suporte.", "OK", "", 32)
    RETURN .F.
 ENDIF
ENDFUNC
**


FUNCTION uf_atendimento_limpaCompartsFi
	
	
	if(USED("fi"))
			SELECT fi
			REPLACE fi.comp WITH 0
			REPLACE fi.comp_diploma WITH 0
		    REPLACE fi.comp_2 WITH 0
		    REPLACE fi.u_ettent1 WITH 0
		    REPLACE fi.u_ettent2 WITH 0
		    REPLACE fi.u_txcomp WITH  100
		    REPLACE fi.etiliquido WITH ((fi.u_epvp*fi.qtt)-(fi.u_ettent1+fi.u_ettent2))              
		    REPLACE fi.epv WITH (fi.u_epvp-(fi.u_ettent1+fi.u_ettent2))	    
            IF AT("Vale", ALLTRIM(fi.design))=0
	             IF fi.desconto=0
	             	IF fi.u_descval=0
		            	REPLACE fi.etiliquido WITH ((fi.u_epvp*fi.qtt)-(fi.u_ettent1+fi.u_ettent2))
                      
		            ELSE
		            	REPLACE fi.etiliquido WITH ROUND((fi.epv*fi.qtt)-fi.u_descval,2)
                      
		            ENDIF 
		         ELSE
		         	REPLACE fi.etiliquido WITH ROUND((fi.epv*fi.qtt)-((fi.epv*fi.qtt)*(fi.desconto/100)),2)                   
		         ENDIF 
	             REPLACE fi.epv WITH (fi.u_epvp-(fi.u_ettent1+fi.u_ettent2))
	         ENDIF 
          
	 ENDIF   
	

ENDFUNC



FUNCTION uf_compart_insert_fi_compart
 LPARAMETERS lntoken, lnfistamp, lnabrev, lnabrev2, lnncartaoaux, lncodacesso, lnReceita, lnDescEntidade
 LOCAL lctoken, lcfistamp, lcvalsns, lclordem, lcabrevaux, lcabrevaux2, lcncartaoaux, lccodacesso, lcReceita
 lctoken = lntoken
 lcfistamp = lnfistamp
 lcvalsns = 0
 lcabrevaux = lnabrev
 lcabrevaux2 = lnabrev2
 lclordem = ''
 lcncartaoaux = lnncartaoaux
 lccodacesso = lncodacesso
 lcReceita = lnReceita
 
 if(EMPTY(lnDescEntidade))
 	lnDescEntidade= 0
 ENDIF
 
 if(lnDescEntidade>100)
 	lnDescEntidade = 100
 ENDIF

 
 SELECT fi
 GOTO TOP
 SCAN
    IF ALLTRIM(fi.fistamp)==ALLTRIM(lcfistamp)
       lclordem = LEFT(ALLTRIM(STR(fi.lordem)), 2)
    ENDIF
 ENDSCAN
 SELECT fi
 GOTO TOP
 SCAN
    IF LEFT(ALLTRIM(STR(fi.lordem)), 2)==ALLTRIM(lclordem) .AND.  .NOT. EMPTY(fi.ref)
       lcvalsns = 0
       
        
		       
       IF ALLTRIM(lcabrevaux)='SNS'
          lcvalsns = fi.u_ettent1
                   
          IF fi.comp_2<>0
             REPLACE fi.comp_2 WITH 0
             REPLACE fi.comp_diploma2 WITH 0
             REPLACE fi.u_ettent2 WITH 0
             REPLACE fi.u_txcomp WITH fi.comp_diploma
             REPLACE fi.etiliquido WITH ((fi.u_epvp*fi.qtt)-(fi.u_ettent1+fi.u_ettent2))
              
             REPLACE fi.epv WITH (fi.u_epvp-(fi.u_ettent1+fi.u_ettent2))
          ENDIF
       ENDIF
       IF ALLTRIM(lcabrevaux2)='SNS'
          lcvalsns = fi.u_ettent2
     
          
          IF fi.comp<>0
             REPLACE fi.comp WITH 0
             REPLACE fi.comp_diploma WITH 0
             REPLACE fi.u_ettent1 WITH 0
             REPLACE fi.u_txcomp WITH fi.comp_diploma_2
             IF AT("Vale", ALLTRIM(fi.design))=0
	             IF fi.desconto=0
	             	IF fi.u_descval=0
		            	REPLACE fi.etiliquido WITH ((fi.u_epvp*fi.qtt)-(fi.u_ettent1+fi.u_ettent2))
                      
		            ELSE
		            	REPLACE fi.etiliquido WITH ROUND((fi.epv*fi.qtt)-fi.u_descval,2)
                      
		            ENDIF 
		         ELSE
		         	REPLACE fi.etiliquido WITH ROUND((fi.epv*fi.qtt)-((fi.epv*fi.qtt)*(fi.desconto/100)),2)
                   
		         ENDIF 
	             REPLACE fi.epv WITH (fi.u_epvp-(fi.u_ettent1+fi.u_ettent2))
	         ENDIF 
          ENDIF
         
        
       ENDIF
       
       
       ** limpa tudo se lcabrevaux e lcabrevaux2 !='SNS'
       IF ALLTRIM(lcabrevaux2)!='SNS' AND ALLTRIM(lcabrevaux)!='SNS'       	 
	       	 SELECT fi
	       	 uf_atendimento_limpaCompartsFi()
	       	 SELECT fi
	   ENDIF

       
       TEXT TO lcsql TEXTMERGE NOSHOW
				insert into fi_compart (
					token
					,fistamp
					,ftstamp
					,hasPrescription
					,lineNumber
					,pvpValue
					, prescriptionExceptions
					, prescriptionDispatch
					, prescriptionPVPValue
					, ref
					, quantity
					, snsValue
					, uncontributedValue
					, ousrdata
					, codacesso
					, codCNP
					, percIva
					, ivaincl
					, nrreceita
					, descontoHabitual
					, pvpOriginal
					, qttminvd
					, qttembal
				)values(
					'<<ALLTRIM(lctoken)>>'
					,'<<ALLTRIM(fi.fistamp)>>'
					,'<<ALLTRIM(lcNcartaoAux)+ALLTRIM(stampcompart)>>'
					,1
					,<<fi.lordem>>
					,<<ROUND(fi.epvori  * (1-(ROUND(lnDescEntidade/100,4))),2)>>
					,'<<ALLTRIM(fi.lobs3)>>'
					,''
					,<<ROUND(fi.epv * (1-(ROUND(lnDescEntidade/100,4))),2)>>
					,'<<ALLTRIM(fi.ref)>>'
					,<<fi.qtt>>
					,<<lcValSNS>>
					,<<ROUND(((fi.u_epvp*fi.qtt)-(fi.u_ettent1+fi.u_ettent2)) * (1-(ROUND(lnDescEntidade/100,4))),2)>>
					,dateadd(HOUR, <<difhoraria>>, getdate())
					,'<<ALLTRIM(lcCodacesso)>>'
					,(select codCNP from st (nolock) where ref='<<ALLTRIM(fi.ref)>>' and site_nr=<<mysite_nr>>)
					,<<fi.iva>>
					,<<IIF(fi.ivaincl,1,0)>>
					,'<<ALLTRIM(lnReceita)>>'
					,<<lnDescEntidade>>
					,<<fi.epv*fi.qtt>>
					,ISNULL((select qttminvd from st (nolock) where ref='<<ALLTRIM(fi.ref)>>' and site_nr=<<mysite_nr>>),0)
					,ISNULL((select qttembal from st (nolock) where ref='<<ALLTRIM(fi.ref)>>' and site_nr=<<mysite_nr>>),0)
				)	
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
          uf_perguntalt_chama("N�o foi possivel proceder inserir linhas na comparticipa��o. Contacte o suporte.", "OK", "", 32)
          RETURN .F.
       ENDIF
    ENDIF
    SELECT fi
 ENDSCAN
ENDFUNC
**
FUNCTION uf_atendimento_compart_valida

 LOCAL lctemcompart, lcstampcompart, lcstamplin, lcnrcartao, lcentidade, lcstampcab, lclinabrev, lclinabrev2, lccodacesso, lcprogramid, lcnrreceita
 STORE '' TO lcnrreceita
 STORE .F. TO lctemcompart, lcCompartCovid
 SELECT ucrsatendcomp
 GOTO TOP
 SCAN
    IF ucrsatendcomp.e_compart=.T. .AND. atendimento.pgfdados.page3.btn12.label1.caption<>'COMPART. MANUAL' AND !uf_gerais_savida_afp(ALLTRIM(ucrsatendcomp.cptorgabrev))
       lctemcompart = .T.
    ENDIF
 ENDSCAN

 IF lctemcompart=.T.
 	stampcompart = uf_gerais_stamp_len(8)

    SELECT ucrsatendcomp
    GOTO TOP
    SCAN FOR  .NOT. EMPTY(e_compart)
       IF EMPTY(ucrsatendcomp.ncartao)
          uf_perguntalt_chama("ATEN��O!"+CHR(13)+"TEM DE INSERIR O N� DO CART�O NA RECEITA COMPARTICIPADA.", "OK", "", 48)
          RETURN .F.
       ENDIF
       IF EMPTY(ucrsatendcomp.codacesso)
          REPLACE ucrsatendcomp.codacesso WITH ALLTRIM(STR(RAND()*1000000))
       ENDIF
    ENDSCAN
    lcstampcompart = uf_gerais_stamp()
    IF  .NOT. USED("uCrsLinPresc")
       CREATE CURSOR uCrsLinPresc (fistamp C(30), ncartao C(20), entidade N(6), ftstamp C(25), abrev C(25), abrev2 C(25), codacesso C(10), programid C(30), nrreceita C(30))
    ELSE
       SELECT ucrslinpresc
       DELETE ALL
    ENDIF
 
 
 	SELECT ucrsatendcomp 
 	SELECT fi
    UPDATE ucrsatendcomp FROM ucrsatendcomp   INNER JOIN FI  ON  ALLTRIM(ucrsatendcomp.FISTAMP)=ALLTRIM(fi.FISTAMP)   SET ucrsatendcomp.nrreceita  = ALLTRIM(fi.receita)   WHERE !EMPTY(ALLTRIM(fi.receita))
 
    
    SELECT ucrsatendcomp
    GOTO TOP
    SCAN
       lcstamplin = ucrsatendcomp.fistamp
       lcnrcartao = ALLTRIM(ucrsatendcomp.ncartao)
       lcentidade = ucrsatendcomp.entidade
       lcabrev = ucrsatendcomp.cptorgabrev
       lcabrev2 = ucrsatendcomp.cptorgabrev2
       lccodacesso = ucrsatendcomp.codacesso
       lcprogramid = ALLTRIM(ucrsatendcomp.programid)
       lcnrreceita = ALLTRIM(ucrsatendcomp.nrreceita)
       TEXT TO lcsql TEXTMERGE NOSHOW
				select TOP 1 e_compart from cptpla(nolock) where cptorgabrev='<<ALLTRIM(lcAbrev)>>' and inactivo = 0
       ENDTEXT
       uf_gerais_actgrelha("", "ucrsCompartVal", lcsql)
       IF RECCOUNT("ucrsCompartVal")>0
          IF ucrscompartval.e_compart=.T.
             SELECT ucrslinpresc
             APPEND BLANK
             REPLACE ucrslinpresc.fistamp WITH lcstamplin
             REPLACE ucrslinpresc.ncartao WITH lcnrcartao
             REPLACE ucrslinpresc.entidade WITH lcentidade
             REPLACE ucrslinpresc.abrev WITH lcabrev
             REPLACE ucrslinpresc.abrev2 WITH lcabrev2
             REPLACE ucrslinpresc.codacesso WITH lccodacesso
             REPLACE ucrslinpresc.programid WITH lcprogramid
             REPLACE ucrslinpresc.nrreceita WITH lcnrreceita 
          ENDIF
       ENDIF
       SELECT ucrsatendcomp
    ENDSCAN
    SELECT DISTINCT ncartao, entidade, abrev, abrev2, codacesso, programid, nrreceita FROM uCrsLinPresc INTO CURSOR uCrsCabPresc READWRITE
    fecha("ucrsCompartVal")

    IF (uf_atendimento_validasevalidacompartexterna())
	
       SELECT ucrscabpresc
       GOTO TOP
       SCAN
          uf_compart_insert_ft_compart(ALLTRIM(lcstampcompart), ALLTRIM(ucrscabpresc.ncartao), ucrscabpresc.entidade, ALLTRIM(ucrscabpresc.ncartao), 1, ALLTRIM(ucrscabpresc.codacesso), ALLTRIM(ucrscabpresc.abrev), ALLTRIM(ucrscabpresc.programid), ALLTRIM(ucrscabpresc.nrreceita))
          SELECT ucrscabpresc
       ENDSCAN
       lcstamplin = ''
       SELECT ucrslinpresc
       GOTO TOP
       SCAN
          lcstamplin = ucrslinpresc.fistamp
          lcstampcab = ''
          lclinabrev = ucrslinpresc.abrev
          lclinabrev2 = ucrslinpresc.abrev2
          lclincodacesso = ucrslinpresc.codacesso
          SELECT fi
          GOTO TOP
          SCAN
             IF ALLTRIM(fi.fistamp)==ALLTRIM(lcstamplin)
                lcstampcab = ALLTRIM(fi.ftstamp)
             ENDIF
          ENDSCAN
          SELECT ucrslinpresc
          REPLACE ucrslinpresc.ftstamp WITH lcstampcab
       ENDSCAN
       SELECT ucrslinpresc
       GOTO TOP
       SCAN
          uf_compart_insert_fi_compart(ALLTRIM(lcstampcompart), ALLTRIM(ucrslinpresc.fistamp), ALLTRIM(ucrslinpresc.abrev), ALLTRIM(ucrslinpresc.abrev2), ALLTRIM(ucrslinpresc.ncartao), ALLTRIM(ucrslinpresc.codacesso), ALLTRIM(ucrslinpresc.nrreceita))
          SELECT ucrslinpresc
       ENDSCAN

       uf_presc_com(ALLTRIM(lcstampcompart))

    ENDIF
 ENDIF
 RETURN .T.
ENDFUNC
**
FUNCTION uf_presc_com
 LPARAMETERS lntokencom
 LOCAL lctokencom, lcstampficompart, lcreturncontributedvalue, lcreturncontributedpercentage
 STORE '&' TO lcadd, lcstampficompart
 STORE 0 TO lcreturncontributedvalue, lcreturncontributedpercentage
 lctokencom = lntokencom
 lcnomejar = "ltsCompartCli.jar"
 lcwsdir = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\ltsCompart'
 regua(1, 2, "A efetuar a valida��o da comparticipa��o por favor aguarde.")
 lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\ltsCompart\'+ALLTRIM(lcnomejar)+' "--TOKEN='+ALLTRIM(lctokencom)+'"'+' "--IDCL='+ALLTRIM(UPPER(sql_db))+'"'
 lcwspath = "cmd /c cd /d "+lcwsdir+" "+lcadd+lcadd+" javaw -jar "+lcwspath
 owsshell = CREATEOBJECT("WScript.Shell")
 owsshell.run(lcwspath, 0, .T.)
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		select top 1 
			token
			, ftstamp
			, code
			,(case when message<>'' then cast(message as varchar(250)) else 'N�o foi poss�vel validar os dados de comparticipa��o' end) as message
			, ai
			, version
			, ousrdata 
		from 
			ft_compart_result (nolock)
		where 
			token = '<<ALLTRIM(lcTokenCom)>>' 
		order by 
			code desc
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsAuxCompart", lcsql)
    uf_perguntalt_chama("N�o foi possivel validar a comparticipa��o. Por favor contacte o suporte.", "OK", "", 32)
    regua(2)
    RETURN .F.
 ELSE
 
    SELECT ucrsauxcompart
    IF ucrsauxcompart.code<>200
       SELECT ucrsreceitavalidar
       GOTO BOTTOM
       APPEND BLANK
       REPLACE ucrsreceitavalidar.token WITH ALLTRIM(ucrsauxcompart.token)
       REPLACE ucrsreceitavalidar.retorno_erro_cod WITH STR(ucrsauxcompart.code)
       REPLACE ucrsreceitavalidar.retorno_erro_descr WITH ALLTRIM(ucrsauxcompart.message)
       REPLACE ucrsreceitavalidar.resultado_validacao_cod WITH ALLTRIM(STR(ucrsauxcompart.code))
       REPLACE ucrsreceitavalidar.resultado_validacao_descr WITH ALLTRIM(ucrsauxcompart.message)
       REPLACE ucrsreceitavalidar.resultado_efetivacao_descr WITH ALLTRIM(STR(ucrsauxcompart.code))+" - "+ALLTRIM(ucrsauxcompart.message)
       IF  .NOT. USED("uCrsReceitaValidarAp")
          SELECT * FROM uCrsReceitaValidar WHERE 0=1 INTO CURSOR ucrsReceitaEfetivarAp READWRITE
       ENDIF
       SELECT ucrsreceitaefetivarap
       GOTO BOTTOM
       APPEND BLANK
       REPLACE ucrsreceitaefetivarap.token WITH ALLTRIM(ucrsauxcompart.token)
       REPLACE ucrsreceitaefetivarap.retorno_erro_cod WITH STR(ucrsauxcompart.code)
       REPLACE ucrsreceitaefetivarap.retorno_erro_descr WITH ALLTRIM(ucrsauxcompart.message)
       REPLACE ucrsreceitaefetivarap.resultado_validacao_cod WITH ALLTRIM(STR(ucrsauxcompart.code))
       REPLACE ucrsreceitaefetivarap.resultado_validacao_descr WITH ALLTRIM(ucrsauxcompart.message)
       REPLACE ucrsreceitaefetivarap.resultado_efetivacao_descr WITH ALLTRIM(STR(ucrsauxcompart.code))+" - "+ALLTRIM(ucrsauxcompart.message)
    ELSE
       lcsql = ''
       TEXT TO lcsql TEXTMERGE NOSHOW
				select * from fi_compart (nolock) where token = '<<ALLTRIM(lcTokenCom)>>' order by ousrdata
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "uCrsAuxCompartFi", lcsql)
          uf_perguntalt_chama("N�o foi possivel validar as linhas comparticipa��o. Por favor contacte o suporte.", "OK", "", 32)
          regua(2)
          RETURN .F.
       ELSE
          SELECT ucrsauxcompartfi
          GOTO TOP
          SCAN
             lcstampficompart = ALLTRIM(ucrsauxcompartfi.fistamp)
             lcreturncontributedvalue = ucrsauxcompartfi.returncontributedvalue
             lcreturncontributedpercentage = ucrsauxcompartfi.returncontributedpercentage
             SELECT fi
             GOTO TOP
             SCAN
                IF lcstampficompart=ALLTRIM(fi.fistamp)
                   DO CASE
                      CASE fi.comp=0 .AND. fi.comp_2=0
                         REPLACE fi.comp WITH lcreturncontributedpercentage
                         REPLACE fi.comp_diploma WITH lcreturncontributedpercentage
                         REPLACE fi.u_ettent1 WITH lcreturncontributedvalue
                         REPLACE fi.u_txcomp WITH 100-lcreturncontributedpercentage
                      CASE fi.comp=0 .AND. fi.comp_2<>0 .AND. lcreturncontributedpercentage=100
                         REPLACE fi.comp WITH 100-fi.comp_2
                         REPLACE fi.comp_diploma WITH 100-fi.comp_2
                         REPLACE fi.u_ettent1 WITH lcreturncontributedvalue
                         REPLACE fi.u_txcomp WITH 100-lcreturncontributedpercentage
                      CASE fi.comp=0 .AND. fi.comp_2<>0 .AND. lcreturncontributedpercentage<>100
                         REPLACE fi.comp WITH (100-fi.comp_2)*(lcreturncontributedpercentage/100)
                         REPLACE fi.comp_diploma WITH (100-fi.comp_2)*(lcreturncontributedpercentage/100)
                         REPLACE fi.u_ettent1 WITH lcreturncontributedvalue
                         REPLACE fi.u_txcomp WITH 100-(fi.comp_2+fi.comp)
                      OTHERWISE
                   ENDCASE
                   IF AT("Vale", ALLTRIM(fi.design))=0
	                   IF fi.desconto=0
		                   REPLACE fi.epv WITH (fi.u_epvp-((fi.u_ettent1+fi.u_ettent2)/fi.qtt))
	                   		IF fi.u_descval=0
	                   			REPLACE fi.etiliquido WITH ((fi.u_epvp*fi.qtt)-(fi.u_ettent1+fi.u_ettent2))
                               
	                   		ELSE
	                   			REPLACE fi.etiliquido WITH ROUND((fi.epv*fi.qtt)-fi.u_descval,2)
                               
	                   		ENDIF 
	                   ELSE
	                      IF lcreturncontributedpercentage=100
	                         REPLACE fi.etiliquido WITH 0
	                         REPLACE fi.epv WITH 0
                             
	                      ELSE
	                         **REPLACE fi.etiliquido WITH (((fi.u_epvp*fi.qtt)-(fi.u_ettent1))-(((fi.u_epvp*fi.qtt)-(fi.u_ettent1))*(fi.desconto/100)))-fi.u_ettent2
	                         REPLACE fi.epv WITH (fi.u_epvp-((fi.u_ettent1+fi.u_ettent2)/fi.qtt))
	                         REPLACE fi.etiliquido WITH ROUND((fi.epv*fi.qtt)-((fi.epv*fi.qtt)*(fi.desconto/100)),2)
                             
	                      ENDIF
	                   ENDIF
	                ENDIF
	                IF ALLTRIM(fi.tipor)='RES' AND ALLTRIM(fi.tpres)='RN' 
	                	REPLACE fi.etiliquido WITH 0
                      
               			REPLACE fi.qtdem WITH fi.qtt
	                ENDIF 
                ENDIF

             ENDSCAN
             SELECT ucrsauxcompartfi
          ENDSCAN
          uf_atendimento_infototais()
       ENDIF
       tokencompart = ALLTRIM(lctokencom)
       atendimento.grdatend.refresh
       atendimento.refresh()
    ENDIF
 ENDIF
 regua(2)
 fecha("uCrsAuxCompartFi")
 fecha("uCrsAuxCompart")
ENDFUNC
**
FUNCTION uf_compart_efetiva
 LPARAMETERS lntokencom, lnnewtokencom
 LOCAL lctokencom, lcnewtokencom, lcadd
 STORE '&' TO lcadd
 lctokencom = lntokencom
 lcnewtokencom = lnnewtokencom
 regua(1, 2, "A efetuar a efetiva��o da comparticipa��o por favor aguarde.")
 TEXT TO lcsql TEXTMERGE NOSHOW
		insert into ft_compart (token, siteno, efrId, ftstamp, cardNumber, type, contributionCorrelationId, anulado, ousrdata, codacesso, programId, nrreceita)
		select '<<ALLTRIM(lcNewTokenCom)>>', siteno, efrId, ftstamp, cardNumber, 2, contributionCorrelationId, anulado, dateadd(HOUR, <<difhoraria>>, getdate()), codacesso, programId, nrreceita from ft_compart (nolock) where token='<<ALLTRIM(lcTokenCom)>>'
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
    uf_perguntalt_chama("N�o foi possivel efetivar a comparticipa��o dos cabe�alhos. Por favor contacte o suporte.", "OK", "", 32)
    regua(2)
    RETURN .F.
 ENDIF
 

 TEXT TO lcsql TEXTMERGE NOSHOW
		insert into fi_compart (token, fistamp, ftstamp, hasPrescription, lineNumber, pvpValue, prescriptionExceptions, prescriptionDispatch, prescriptionPVPValue, ref, quantity, snsValue, uncontributedValue, ousrdata, codacesso, nrreceita )
		select '<<ALLTRIM(lcNewTokenCom)>>', fistamp, ftstamp, hasPrescription, lineNumber, pvpValue, prescriptionExceptions, prescriptionDispatch, prescriptionPVPValue, ref, quantity, snsValue, uncontributedValue, dateadd(HOUR, <<difhoraria>>, getdate()), codacesso, nrreceita  from fi_compart (nolock) where token='<<ALLTRIM(lcTokenCom)>>'
 ENDTEXT
 

 IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
    uf_perguntalt_chama("N�o foi possivel efetivar a comparticipa��o das linhas. Por favor contacte o suporte.", "OK", "", 32)
    regua(2)
    RETURN .F.
 ENDIF

 lcnomejar = "ltsCompartCli.jar"
 lcwsdir = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\ltsCompart'
 lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\ltsCompart\'+ALLTRIM(lcnomejar)+' "--TOKEN='+ALLTRIM(lcnewtokencom)+'"'+' "--IDCL='+ALLTRIM(UPPER(sql_db))+'"'
 lcwspath = "cmd /c cd /d "+lcwsdir+" "+lcadd+lcadd+" javaw -jar "+lcwspath

 owsshell = CREATEOBJECT("WScript.Shell")
 owsshell.run(lcwspath, 0, .T.)
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		select top 1 
			token
			, ftstamp
			, code
			,(case when message<>'' then cast(message as varchar(250)) else 'N�o foi poss�vel validar os dados de comparticipa��o' end) as message
			, ai
			, version
			, ousrdata 
		from 
			ft_compart_result (nolock)
		where 
			token = '<<ALLTRIM(lcNewTokenCom)>>' 
		order by 
			ousrdata desc
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsAuxCompart", lcsql)
    uf_perguntalt_chama("N�o foi possivel validar a comparticipa��o. Por favor contacte o suporte.", "OK", "", 32)
    regua(2)
    RETURN .F.
 ELSE
    SELECT ucrsauxcompart
    IF ucrsauxcompart.code<>200
       uf_perguntalt_chama("N�o foi possivel efetivar a comparticipa��o da entidade externa, por favor tente novamente.", "OK", "", 32)
       regua(2)
       RETURN .F.
    ENDIF
 ENDIF
 regua(2)
 fecha("uCrsAuxCompart")
 RETURN .T.
ENDFUNC
**
FUNCTION uf_compart_anula
 LPARAMETERS lntokencom, lnftstamp
 LOCAL lctokencom, lcftstamp, lcadd, lcnewtokenanul
 lctokencom = lntokencom
 lcftstamp = lnftstamp
 lcnewtokenanul = uf_gerais_stamp()
 
  
 IF(VARTYPE(compsemsimul)=="U")
	Public compsemsimul
	compsemsimul = .f.
 ENDIF
 
 
 
 STORE '&' TO lcadd
 regua(1, 2, "A efetuar a anula��o da comparticipa��o por favor aguarde.")
 IF compsemsimul=.F.
    TEXT TO lcsql TEXTMERGE NOSHOW
			insert into ft_compart (token, siteno, efrId, ftstamp, cardNumber, type, contributionCorrelationId, anulado, ousrdata)
			select '<<ALLTRIM(lcNewTokenAnul)>>', siteno, efrId, ftstamp, cardNumber, 3, returncontributionCorrelationId, anulado, dateadd(HOUR, <<difhoraria>>, getdate()) from ft_compart (nolock) where token='<<ALLTRIM(lcTokenCom)>>' AND ftstamp='<<ALLTRIM(lcFtstamp)>>' AND type = 2
    ENDTEXT
 ELSE
    TEXT TO lcsql TEXTMERGE NOSHOW
			insert into ft_compart (token, siteno, efrId, ftstamp, cardNumber, type, contributionCorrelationId, anulado, ousrdata)
			select TOP 1 '<<ALLTRIM(lcNewTokenAnul)>>', siteno, efrId, ftstamp, cardNumber, 3, returncontributionCorrelationId, anulado, dateadd(HOUR, <<difhoraria>>, getdate()) from ft_compart (nolock) where token='<<ALLTRIM(lcTokenCom)>>' AND type=2 ORDER BY ousrdata
    ENDTEXT
 ENDIF
 IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
    uf_perguntalt_chama("N�o foi possivel anular a comparticipa��o da Entidade Externa. Por favor contacte o suporte.", "OK", "", 32)
    regua(2)
    RETURN .F.
 ENDIF
 lcnomejar = "ltsCompartCli.jar"
 lcwsdir = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\ltsCompart'
 lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\ltsCompart\'+ALLTRIM(lcnomejar)+' "--TOKEN='+ALLTRIM(lcnewtokenanul)+'"'+' "--IDCL='+ALLTRIM(UPPER(sql_db))+'"'
 lcwspath = "cmd /c cd /d "+lcwsdir+" "+lcadd+lcadd+" javaw -jar "+lcwspath

 owsshell = CREATEOBJECT("WScript.Shell")
 owsshell.run(lcwspath, 0, .T.)
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		select top 1 
			token
			, ftstamp
			, code
			,(case when message<>'' then cast(message as varchar(250)) else 'N�o foi poss�vel validar os dados de comparticipa��o' end) as message
			, ai
			, version
			, ousrdata 
		from 
			ft_compart_result (nolock)
		where 
			token = '<<ALLTRIM(lcNewTokenAnul)>>' 
		order by 
			ousrdata desc
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsAuxCompart", lcsql)
    uf_perguntalt_chama("N�o foi possivel anular a comparticipa��o. Por favor contacte o suporte.", "OK", "", 32)
    regua(2)
    RETURN .F.
 ELSE
    SELECT ucrsauxcompart
    IF ucrsauxcompart.code<>200
       uf_perguntalt_chama(ALLTRIM(ucrsauxcompart.message), "OK", "", 32)
       regua(2)
       RETURN .F.
    ELSE
    	
       TEXT TO lcsql TEXTMERGE NOSHOW
				UPDATE ft2 SET token_anulacao_compl = '<<ALLTRIM(lcNewTokenAnul)>>' WHERE ft2stamp='<<ALLTRIM(lcFtstamp)>>'
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
          uf_perguntalt_chama("N�o foi possivel atualizar o token da anula��o. Por favor contacte o suporte.", "OK", "", 32)
          regua(2)
          RETURN .F.
       ENDIF
       
       if(USED("ft2") and  TYPE("ft2.nrelegibilidade")<>'U')     
       		SELECT ft2
       		GO TOP
       		UPDATE ft2 SET nrelegibilidade = ''
       ENDIF
    ENDIF
 ENDIF
 regua(2)
 fecha("uCrsAuxCompart")
ENDFUNC
**



FUNCTION uf_compart_envia
 LPARAMETERS lnftstamp, lndocno, lcEfrId, lcElId, lcFtStampSeguradora, lcAbrev
 LOCAL lctokencom, lcftstamp, lcadd, lcnewtokenanul, lcdocno
 lcftstamp = lnftstamp
 lcnewtokenanul = uf_gerais_stamp()
 lcdocno = ALLTRIM(lndocno)
 STORE '&' TO lcadd
 regua(1, 2, "A enviar a comparticipa��o... por favor aguarde.")
 TEXT TO lcsql TEXTMERGE NOSHOW
		insert into ft_compart (token, siteno, efrId,abrevInsurance, ftstamp, cardNumber, type, contributionCorrelationId, anulado, ousrdata, invoiceNumber, ousrUser, docData)
		values ('<<ALLTRIM(lcNewTokenAnul)>>',<<mysite_nr>>,<<lcEfrId>>,'<<ALLTRIM(lcAbrev)>>','<<ALLTRIM(lnftstamp)>>','',4,'<<ALLTRIM(lcElId)>>',0, dateadd(HOUR, <<difhoraria>>, getdate()),'<<ALLTRIM(lcDocNo)>>', '<<ALLTRIM(m_chinis)>>',(SELECT fdata FROM ft (nolock) WHERE ftstamp='<<ALLTRIM(lnFtstamp)>>'))		
 ENDTEXT
 

 IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
    uf_perguntalt_chama("N�o foi possivel validar os dados a comparticipar. Por favor contacte o suporte.", "OK", "", 32)
    regua(2)
    RETURN .F.
 ENDIF
 lcnomejar = "ltsCompartCli.jar"
 lcwsdir = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\ltsCompart'
 lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\ltsCompart\'+ALLTRIM(lcnomejar)+' "--TOKEN='+ALLTRIM(lcnewtokenanul)+'"'+' "--IDCL='+ALLTRIM(UPPER(sql_db))+'"'
 lcwspath = "cmd /c cd /d "+lcwsdir+" "+lcadd+lcadd+" javaw -jar "+lcwspath

 owsshell = CREATEOBJECT("WScript.Shell")
 owsshell.run(lcwspath, 0, .T.)
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		select top 1 
			token
			, ftstamp
			, code
			,(case when message<>'' then cast(message as varchar(250)) else 'N�o foi poss�vel validar os dados de comparticipa��o' end) as message
			, ai
			, version
			, ousrdata 
		from 
			ft_compart_result (nolock)
		where 
			token = '<<ALLTRIM(lcNewTokenAnul)>>' 
		order by 
			ousrdata desc
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsAuxCompart", lcsql)
    uf_perguntalt_chama("N�o foi possivel verificar o envio da comparticipa��o. Por favor contacte o suporte.", "OK", "", 32)
    regua(2)
    RETURN .F.
 ELSE
    SELECT ucrsauxcompart
   IF ucrsauxcompart.code<>200
       uf_perguntalt_chama("N�o foi possivel enviar o documento. Por favor contacte o suporte.", "OK", "", 32)
       regua(2)
       RETURN .F.
    ELSE
       TEXT TO lcsql TEXTMERGE NOSHOW
				UPDATE ft2 SET enviado= 1, data_envio = dateadd(HOUR, <<difhoraria>>, getdate()) WHERE ft2stamp='<<ALLTRIM(lcFtStampSeguradora)>>'
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
          uf_perguntalt_chama("N�o foi possivel atualizar o token da anula��o. Por favor contacte o suporte.", "OK", "", 32)
          regua(2)
          RETURN .F.
       ENDIF
     RETURN .T.
    ENDIF
 ENDIF
 regua(2)
 fecha("uCrsAuxCompart")
ENDFUNC
**
PROCEDURE uf_psico_campos
 IF linpsico=.T.
    atendimento.pgfdados.page1.txtnomeadquirente.disabledbackcolor = RGB(237, 28, 36)
    atendimento.pgfdados.page1.txtmoradaadquirente.disabledbackcolor = RGB(237, 28, 36)
    atendimento.pgfdados.page1.txtcodpostaladquirente.disabledbackcolor = RGB(237, 28, 36)
    atendimento.pgfdados.page1.txtdocidadquirente.disabledbackcolor = RGB(237, 28, 36)
    atendimento.pgfdados.page1.txtvaldocidadquirente.disabledbackcolor = RGB(237, 28, 36)
    atendimento.pgfdados.page1.txtdnadquirente.disabledbackcolor = RGB(237, 28, 36)
    atendimento.pgfdados.page1.txtmorada.disabledbackcolor = RGB(237, 28, 36)
    atendimento.pgfdados.page1.txtcodpostal.disabledbackcolor = RGB(237, 28, 36)
    atendimento.pgfdados.page3.txtszzstamp.disabledbackcolor = RGB(237, 28, 36)
    **atendimento.pgfdados.page3.txtdatareceita.disabledbackcolor = RGB(237, 28, 36)
    **atendimento.pgfdados.page2.btnalertas.shape1.backcolor = RGB(237, 28, 36)
    atendimento.pgfdados.page3.btnreceita.shape1.backcolor = RGB(237, 28, 36)
 ELSE
    atendimento.pgfdados.page1.txtnomeadquirente.disabledbackcolor = RGB(255, 255, 255)
    atendimento.pgfdados.page1.txtmoradaadquirente.disabledbackcolor = RGB(255, 255, 255)
    atendimento.pgfdados.page1.txtcodpostaladquirente.disabledbackcolor = RGB(255, 255, 255)
    atendimento.pgfdados.page1.txtdocidadquirente.disabledbackcolor = RGB(255, 255, 255)
    atendimento.pgfdados.page1.txtvaldocidadquirente.disabledbackcolor = RGB(255, 255, 255)
    atendimento.pgfdados.page1.txtdnadquirente.disabledbackcolor = RGB(255, 255, 255)
    atendimento.pgfdados.page1.txtmorada.disabledbackcolor = RGB(255, 255, 255)
    atendimento.pgfdados.page1.txtcodpostal.disabledbackcolor = RGB(255, 255, 255)
    atendimento.pgfdados.page3.txtszzstamp.disabledbackcolor = RGB(255, 255, 255)
    atendimento.pgfdados.page3.txtdatareceita.disabledbackcolor = RGB(255, 255, 255)
    **atendimento.pgfdados.page2.btnalertas.Shape1.backcolor = RGB(0,  167, 231)
    atendimento.pgfdados.page3.btnreceita.Shape1.backcolor = RGB(255, 255, 255)
 ENDIF
ENDPROC
**
PROCEDURE uf_insert_fi_trans_info_seminfo
   LPARAMETERS uv_refLin, uv_stampLin, uv_recTable

 LOCAL lcfireflin
 lcfireflin = IIF(!EMPTY(uv_refLin), ALLTRIM(uv_refLin), ALLTRIM(fi.ref))
 lcfistamplin = IIF(!EMPTY(uv_stampLin), ALLTRIM(uv_stampLin), ALLTRIM(fi.fistamp))
 SELECT fi_trans_info
 GOTO BOTTOM
 APPEND BLANK
 IF VARTYPE(nratendimento)=='C'
    REPLACE fi_trans_info.token WITH ALLTRIM(nratendimento)
 ELSE

   LOCAL uv_newStamp
   uv_newStamp = uf_gerais_stamp()

   SELECT fi_trans_info

    REPLACE fi_trans_info.token WITH ALLTRIM(uv_newStamp)
 ENDIF
 REPLACE fi_trans_info.recStamp WITH ALLTRIM(lcfistamplin)
 REPLACE fi_trans_info.recTable WITH IIF(EMPTY(uv_recTable), 'FT', ALLTRIM(uv_recTable))
 REPLACE fi_trans_info.productcode WITH ''
 REPLACE fi_trans_info.productcodescheme WITH 'GTIN'
 REPLACE fi_trans_info.batchid WITH ''
 SET CENTURY ON
 REPLACE fi_trans_info.batchexpirydate WITH DATE(1900, 1E0, 1E0)
 REPLACE fi_trans_info.packserialnumber WITH ''

 LOCAL uv_newClientTrxId
 uv_newClientTrxId = uf_gerais_stamp()

 SELECT fi_trans_info

 REPLACE fi_trans_info.clienttrxid WITH ALLTRIM(uv_newClientTrxId)
 REPLACE fi_trans_info.posterminal WITH ALLTRIM(myterm)
 IF LEN(ALLTRIM(lcfireflin))=7
    REPLACE fi_trans_info.country_productnhrn WITH 714
 ELSE
    REPLACE fi_trans_info.country_productnhrn WITH 0
 ENDIF
 REPLACE fi_trans_info.productnhrn WITH ALLTRIM(lcfireflin)
 REPLACE fi_trans_info.ousrinis WITH ALLTRIM(m_chinis)
 REPLACE fi_trans_info.ousrdata WITH DATETIME()
 REPLACE fi_trans_info.tipo WITH 'S'
ENDPROC
**
PROCEDURE uf_fi_trans_info_remove
 PARAMETER lcfiRef
 
 local lcPsoFiQttDem
 
 IF(USED("fi"))
 	SELECT fi
 	lcPsoFiQttDem = RECNO("fi")
 ENDIF
 

 SELECT fi_trans_info
 GOTO TOP
 
 DELETE FROM fi_trans_info WHERE ALLTRIM(fi_trans_info.productNhrn)==ALLTRIM(lcfiRef)
 
 IF(USED("fi"))
 	select fi
 	GO top
 	
 	update fi set qtdem = 0 WHERE ALLTRIM(fi.ref)==ALLTRIM(lcfiRef)
 
	TRY
	   GOTO lcPsoFiQttDem
	CATCH
	ENDTRY
 ENDIF
 
ENDPROC
**
PROCEDURE uf_fi_trans_info_muda_tipo
 PARAMETER lcfistamp, lctranstipo
 IF lctranstipo='E'
    SELECT fi_trans_info
    UPDATE fi_trans_info SET tipo = 'E' WHERE ALLTRIM(fi_trans_info.recStamp)==ALLTRIM(lcfistamp)
 ELSE
    SELECT fi_trans_info
    UPDATE fi_trans_info SET tipo = 'S' WHERE ALLTRIM(fi_trans_info.recStamp)==ALLTRIM(lcfistamp)
 ENDIF
ENDPROC
**
FUNCTION uf_embal_conferereferencia
 LPARAMETERS lcstring, lcBackOffice
 LOCAL lceanid, lcdtvalid, lcloteid, lcidid, lcrefid, lcrefpaisid, lcreftempid, lcposdelimiterid, lcconfereid, lcleituracampos, uv_newStamp
 STORE '' TO lceanid, lcdtvalid, lcloteid, lcidid, lcrefid, lcrefpaisid, lcleituracampos, uv_newStamp
 STORE 0 TO lcposdelimiterid

 
 LOCAL lcMax
 lcMax = 100
 
 LOCAL lcEan
 lcEan = 14
 
 LOCAL lcInitial
 lcInitial = lcstring

 uv_dMatrix = lcstring
 
 lcstring= STRTRAN(lcstring, "'", "-")
 lcstring= uf_gerais_remove_last_caracter_by_char(lcstring,'#')

 lcreftempid = ALLTRIM(lcstring)
 lcconfereid = ALLTRIM(lcstring)

 lcreftempid = uf_gerais_remove_last_caracter_by_char(lcreftempid,'#')      
 lcconfereid = uf_gerais_remove_last_caracter_by_char(lcconfereid,'#')

 
   IF uf_gerais_retorna_campos_datamatrix_sql(lcreftempid)
      
      SELECT ucrDataMatrixTemp
      IF ucrDataMatrixTemp.valido
   
         lcDtValID = uf_gerais_retornaApenasAlphaNumericos(ucrDataMatrixTemp.data)
         lcRefID  = uf_gerais_retornaApenasAlphaNumericos(ucrDataMatrixTemp.ref)
         lcrefpaisid = ucrDataMatrixTemp.countryCode
         lceanid = ucrDataMatrixTemp.pc
         lcloteid = ucrDataMatrixTemp.lote
         lcidid = ucrDataMatrixTemp.sn

      ENDIF

   ENDIF
 
 IF EMPTY(lcrefid)
 	uf_gerais_notif("Medicamento conferido." + chr(13) + "Datamatrix n�o desativado.", 3000)
 	uf_gerais_registaerrolog("Erro ao ler o datamatrix no uf_atendimento_scanner:  " +  uv_dMatrix, "datamatrix")
 
 	lcrefid = uf_gerais_getFromString(uv_dMatrix, '714', 7, .T.)
 	IF LEN(lcrefID) <> 7
   		uf_gerais_registaerrolog("Artigo n�o encontrado: " +  uv_dMatrix, "datamatrixCnp")
 		uf_perguntalt_chama("ERRO NA LEITURA OU C�DIGO DE BARRAS INV�LIDO.", "OK", "", 64)
 		RETURN .F.
 	ENDIF
 
 ENDIF
 
 
 LOCAL lcjalida
 lcjalida = .F.
 SELECT fi_trans_info
 GOTO TOP
 SCAN

    IF ALLTRIM(fi_trans_info.productcode)==ALLTRIM(lceanid) .AND. ALLTRIM(fi_trans_info.packserialnumber)==ALLTRIM(lcidid)
	
       IF fi_trans_info.conferido=.T.
          uf_perguntalt_chama("ESTA EMBALAGEM J� FOI CONFERIDA!.", "OK", "", 64)
          RETURN (.F.)
       ELSE
          REPLACE fi_trans_info.conferido WITH .T.
          lcjalida = .T.
       ENDIF
    ENDIF
 ENDSCAN
 LOCAL lcatribuiu
 lcatribuiu = .F.
 SELECT fi_trans_info
 GOTO TOP
 SCAN
    IF lcjalida=.F.
       IF ALLTRIM(fi_trans_info.productnhrn)==ALLTRIM(lcrefid) .AND. EMPTY(fi_trans_info.packserialnumber) .AND. lcatribuiu=.F.

		FECHA("uc_tmpFiScan")

	   	SELECT * FROM fi WITH (BUFFERING = .T.) WHERE fistamp = fi_trans_info.recStamp INTO CURSOR uc_tmpFiScan

		SELECT uc_tmpFiScan
		IF !EMPTY(uc_tmpFiScan.ofistamp)
         SELECT fi_trans_info
			IF fi_trans_info.tipo = 'S'
				LOOP
			ENDIF
		ENDIF


         uv_newStamp = uf_gerais_stamp()

         SELECT fi_trans_info

          REPLACE fi_trans_info.token WITH IIF(lcBackOffice, ALLTRIM(uv_newStamp), ALLTRIM(nratendimento))
          REPLACE fi_trans_info.productcode WITH lceanid
          REPLACE fi_trans_info.productcodescheme WITH 'GTIN'
          REPLACE fi_trans_info.batchid WITH lcloteid
              
          if(!EMPTY(uf_gerais_eFormatoDataValidoDataMatrix(lcdtvalid)))          
	          SET CENTURY ON
	          IF ALLTRIM(SUBSTR(lcdtvalid, 5, 2))<>'00' and !EMPTY(ALLTRIM(SUBSTR(lcdtvalid, 5, 2)))
	             REPLACE fi_trans_info.batchexpirydate WITH DATE(2000+VAL(SUBSTR(lcdtvalid, 1, 2)), VAL(SUBSTR(lcdtvalid, 3, 2)), VAL(SUBSTR(lcdtvalid, 5, 2)))
	          ELSE
	             REPLACE fi_trans_info.batchexpirydate WITH GOMONTH(DATE(2000+VAL(SUBSTR(lcdtvalid, 1, 2)), VAL(SUBSTR(lcdtvalid, 3, 2)), 1), 1)-1
	          ENDIF          
          ENDIF
          REPLACE fi_trans_info.packserialnumber WITH lcidid
          REPLACE fi_trans_info.posterminal WITH ALLTRIM(myterm)
          REPLACE fi_trans_info.country_productnhrn WITH VAL(uf_gerais_retornaApenasAlphaNumericos(lcrefpaisid))
          REPLACE fi_trans_info.ousrinis WITH ALLTRIM(m_chinis)
          REPLACE fi_trans_info.ousrdata WITH DATETIME()
          REPLACE fi_trans_info.conferido WITH .T.
          lcatribuiu = .T.

		&& colocado para os pmh	
	     IF !EMPTY(lcloteid) AND NOT ISNULL(lcloteid)
	        SELECT FI
	        REPLACE fi.lote WITH lcloteid
	     ENDIF
			
		IF !EMPTY(lcDtValID) AND NOT ISNULL(lcDtValID)
			IF ALLTRIM(SUBSTR(lcdtvalid, 5, 2))<>'00' and !EMPTY(ALLTRIM(SUBSTR(lcdtvalid, 5, 2)))
				SELECT FI2
				REPLACE fi2.dataValidade WITH DATE(2000+VAL(SUBSTR(lcdtvalid, 1, 2)), VAL(SUBSTR(lcdtvalid, 3, 2)), VAL(SUBSTR(lcdtvalid, 5, 2)))
			ELSE
				SELECT FI2
				REPLACE fi2.dataValidade WITH GOMONTH(DATE(2000+VAL(SUBSTR(lcdtvalid, 1, 2)), VAL(SUBSTR(lcdtvalid, 3, 2)), 1), 1)-1
			ENDIF
	     ENDIF
         
       ENDIF
    ENDIF
 ENDSCAN
 IF lcatribuiu=.F. .AND. lcjalida=.F.

   uv_fistamp = ""

	SELECT FI2 
	GO TOP
	LOCATE FOR ALLTRIM(fi2.productcodeEmbal) == ALLTRIM(lceanid) AND !EMPTY(fi2.productcodeEmbal)
	IF FOUND()

		select fi2
		uv_fistamp = fi2.fistamp

	ENDIF

	IF EMPTY(uv_fistamp)

		SELECT * FROM fi WITH (BUFFERING = .T.) WHERE ALLTRIM(fi.ref) == ALLTRIM(lcRefID) INTO CURSOR uc_tmpFI

		SELECT uc_tmpFI
		GO TOP
		SCAN

			SELECT fi_trans_info
			GO TOP
			LOCATE FOR ALLTRIM(fi_trans_info.recStamp) == ALLTRIM(uc_tmpFI.fistamp) and ALLTRIM(fi_trans_info.productnhrn) == ALLTRIM(uc_tmpFI.ref)
			IF !FOUND()
				uv_fistamp = uc_tmpFI.fistamp
				EXIT
			ENDIF

		ENDSCAN

	ENDIF

   IF !EMPTY(uv_fistamp)

      uv_newStamp = uf_gerais_stamp()

		SELECT fi_trans_info
		APPEND BLANK

		REPLACE fi_trans_info.productnhrn WITH ALLTRIM(lcRefID)
		REPLACE fi_trans_info.token WITH IIF(lcBackOffice, ALLTRIM(uv_newStamp), ALLTRIM(nratendimento))
		REPLACE fi_trans_info.productcode WITH lceanid
		REPLACE fi_trans_info.productcodescheme WITH 'GTIN'
		REPLACE fi_trans_info.batchid WITH lcloteid
      IF (!EMPTY(uf_gerais_eFormatoDataValidoDataMatrix(lcdtvalid))) 
         SET CENTURY ON
         IF ALLTRIM(SUBSTR(lcdtvalid, 5, 2))<>'00' and !EMPTY(ALLTRIM(SUBSTR(lcdtvalid, 5, 2)))
            REPLACE fi_trans_info.batchexpirydate WITH DATE(2000+VAL(SUBSTR(lcdtvalid, 1, 2)), VAL(SUBSTR(lcdtvalid, 3, 2)), VAL(SUBSTR(lcdtvalid, 5, 2)))
         ELSE
            REPLACE fi_trans_info.batchexpirydate WITH GOMONTH(DATE(2000+VAL(SUBSTR(lcdtvalid, 1, 2)), VAL(SUBSTR(lcdtvalid, 3, 2)), 1), 1)-1
         ENDIF
      ENDIF
		REPLACE fi_trans_info.packserialnumber WITH lcidid
		REPLACE fi_trans_info.posterminal WITH ALLTRIM(myterm)
		REPLACE fi_trans_info.country_productnhrn WITH VAL(uf_gerais_retornaApenasAlphaNumericos(lcrefpaisid))
		REPLACE fi_trans_info.ousrinis WITH ALLTRIM(m_chinis)
		REPLACE fi_trans_info.ousrdata WITH DATETIME()
		REPLACE fi_trans_info.tipo WITH 'S'
		REPLACE fi_trans_info.recStamp WITH ALLTRIM(uv_fistamp)
		
	&& colocado para os pmh	
     IF !EMPTY(lcloteid) AND NOT ISNULL(lcloteid)
        SELECT FI
        REPLACE fi.lote WITH lcloteid
     ENDIF
		
	IF !EMPTY(lcDtValID) AND NOT ISNULL(lcDtValID)
		IF ALLTRIM(SUBSTR(lcdtvalid, 5, 2))<>'00' and !EMPTY(ALLTRIM(SUBSTR(lcdtvalid, 5, 2)))
			SELECT FI2
			REPLACE fi2.dataValidade WITH DATE(2000+VAL(SUBSTR(lcdtvalid, 1, 2)), VAL(SUBSTR(lcdtvalid, 3, 2)), VAL(SUBSTR(lcdtvalid, 5, 2)))
		ELSE
			SELECT FI2
			REPLACE fi2.dataValidade WITH GOMONTH(DATE(2000+VAL(SUBSTR(lcdtvalid, 1, 2)), VAL(SUBSTR(lcdtvalid, 3, 2)), 1), 1)-1
		ENDIF
     ENDIF
         
      LOCAL uv_newClientTrxId
      uv_newClientTrxId = uf_gerais_stamp()

      SELECT fi_trans_info
      
		REPLACE fi_trans_info.clienttrxid WITH ALLTRIM(uv_newClientTrxId)
      REPLACE fi_trans_info.recTable WITH 'FT'
      REPLACE fi_trans_info.conferido WITH .T.

		SELECT FI2
		LOCATE FOR ALLTRIM(fi2.fistamp) == ALLTRIM(uv_fistamp)
		IF FOUND()
			IF EMPTY(fi2.productcodeEmbal)
				REPLACE fi2.productcodeEmbal WITH ALLTRIM(lceanid)
			ENDIF
		ENDIF

	ELSE

 		uf_gerais_registaerrolog("Artigo n�o encontrado: " +  uv_dMatrix, "datamatrixCnp")
    	uf_perguntalt_chama("Artigo n�o encontrado", "OK", "", 64)

	ENDIF


 	**uf_gerais_registaerrolog("Artigo n�o encontrado no uf_embal_conferereferencia: " +  lcInitial, "datamatrix")
   **uf_perguntalt_chama("Artigo n�o encontrado", "OK", "", 64)
 ENDIF
 SELECT fi_trans_info
 GOTO TOP
ENDFUNC
**
PROCEDURE uf_desc_val_cartao
 PARAMETER lcvaloradescontar
 uf_atendimento_adicionalinhafi(.F.)
 REPLACE fi.ref WITH 'V000001'
 REPLACE fi.u_refvale WITH 'V000001'
 lcsql = ""
 TEXT TO lcsql TEXTMERGE NOSHOW
		update st set epv1=<<Round(lcValoradescontar,2)>>, design='Desconto de valor em cart�o' where ref='V000001'
 ENDTEXT
 uf_gerais_actgrelha("", "", lcsql)

 uf_atendimento_actref()

 REPLACE fi.valcartao WITH lcvaloradescontar*-1
 lcposicfi = RECNO("FI")
 uf_atendimento_eventofi()

 TRY
    GOTO lcposicfi
 CATCH
 ENDTRY
 REPLACE ucrsatendcl.valcartao WITH ucrsatendcl.valcartao-lcvaloradescontar
ENDPROC
**
PROCEDURE uf_desc_val_cartao_formapagamento
 PARAMETER lcvaloradescontar
 uf_atendimento_adicionalinhafi(.F.)
 REPLACE fi.ref WITH 'V000001'
 REPLACE fi.u_refvale WITH 'V000001'
 lcsql = ""
 TEXT TO lcsql TEXTMERGE NOSHOW
		update st set epv1=0, design='Desconto de valor em cart�o: << alltrim(str(lcvaloradescontar,14,4))>>' where ref='V000001'
 ENDTEXT
 uf_gerais_actgrelha("", "", lcsql)

 uf_atendimento_actref()

 REPLACE fi.valcartao WITH lcvaloradescontar*-1
 lcposicfi = RECNO("FI")
 uf_atendimento_eventofi()

 TRY
    GOTO lcposicfi
 CATCH
 ENDTRY
 REPLACE ucrsatendcl.valcartao WITH ucrsatendcl.valcartao-lcvaloradescontar
ENDPROC
**
PROCEDURE uf_atendimento_savetime
 PARAMETER lcordem, lcnome
 LOCAL lcsql, lcor
 lcor = VAL(lcordem)
 TEXT TO lcsql TEXTMERGE NOSHOW
	INSERT into timeTicket
		(ordem,nome,tempo)  
	values
		(<<lcOrdem>>,'<<ALLTRIM(lcNome)>>',getdate())
 ENDTEXT
 uf_gerais_actgrelha("", "", lcsql)
ENDPROC
**
PROCEDURE uf_atendimento_salvar_cancelar
 IF (TYPE("atendimento")<>"U")
    atendimento.menu1.gravar.img1.picture = mypath+"\imagens\icons\save_w.png"
    atendimento.menu1.sair.image1.picture = mypath+"\imagens\icons\cancelar_w.png"
    atendimento.menu1.estado("", "", "Terminar", .T., "Cancelar", .T.)
 ENDIF
ENDPROC
**
PROCEDURE uf_atendimento_conferir_cancelar
 IF (TYPE("atendimento")<>"U")
    atendimento.menu1.conferir.tag = "false"
    atendimento.menu1.gravar.img1.picture = mypath+"\imagens\icons\unchecked_w.png"
    atendimento.menu1.sair.image1.picture = mypath+"\imagens\icons\cancelar_w.png"
    atendimento.menu1.estado("", "", "Conferir", .T., "Cancelar", .T.)
 ENDIF
ENDPROC
**
PROCEDURE uf_atendimento_conferindo_cancelar
 IF (TYPE("atendimento")<>"U")
    atendimento.menu1.conferir.tag = "true"
    atendimento.menu1.gravar.img1.picture = mypath+"\imagens\icons\checked_w.png"
    atendimento.menu1.sair.image1.picture = mypath+"\imagens\icons\cancelar_w.png"
    atendimento.menu1.estado("", "", "Conferir", .T., "Cancelar", .T.)
 ENDIF
ENDPROC
**
PROCEDURE uf_atendimento_conferindo_limpar
 IF (TYPE("atendimento")<>"U")
    atendimento.menu1.conferir.tag = "true"
    atendimento.menu1.gravar.img1.picture = mypath+"\imagens\icons\checked_w.png"
    atendimento.menu1.sair.image1.picture = mypath+"\imagens\icons\limpar_w.png"
    atendimento.menu1.estado("", "", "Conferir", .T., "Limpar", .T.)
 ENDIF
ENDPROC
**
PROCEDURE uf_atendimento_conferir_limpar
 IF (TYPE("atendimento")<>"U")
    atendimento.menu1.conferir.tag = "false"
    atendimento.menu1.gravar.img1.picture = mypath+"\imagens\icons\unchecked_w.png"
    atendimento.menu1.sair.image1.picture = mypath+"\imagens\icons\limpar_w.png"
    atendimento.menu1.estado("", "", "Conferir", .T., "Limpar", .T.)
 ENDIF
ENDPROC
**
PROCEDURE uf_atendimento_salvar_limpar
 IF (TYPE("atendimento")<>"U")
    atendimento.menu1.conferir.tag = "false"
    atendimento.menu1.gravar.img1.picture = mypath+"\imagens\icons\save_w.png"
    atendimento.menu1.sair.image1.picture = mypath+"\imagens\icons\limpar_w.png"
    atendimento.menu1.estado("", "", "Terminar", .T., "Limpar", .T.)
 ENDIF
ENDPROC
**
PROCEDURE uf_atendimento_criar_ut_dem
 IF uf_gerais_getparameter("ADM0000000255", "BOOL") .AND. ALLTRIM(atendimento.pgfdados.page3.txtnifreceita.value)=''
    uf_perguntalt_chama("TEM DE PREENCHER O NIF PARA PODER CRIAR A FICHA DE CLIENTE.", "OK", "", 48)
 ELSE
    IF uf_perguntalt_chama("DESEJA CRIAR UM UTENTE NOVO COM OS DADOS DO UTENTE DA RECEITA?", "Sim", "N�o")
       uf_atendimento_procuracliente()
       pesqutentes.pageframe1.page2.nome.value = ALLTRIM(ucrsatendcomp.nomeutente)
       pesqutentes.pageframe1.page2.tlf.value = ALLTRIM(ucrsatendcomp.contactoutente)
       pesqutentes.pageframe1.page2.ncont.value = ALLTRIM(atendimento.pgfdados.page3.txtnifreceita.value)
       pesqutentes.pageframe1.page2.txtnascimento.value = '19010101'
       uf_pesqutentes_criarficha()
       LOCAL lcclnodem, lcclestabdem
       SELECT ft
       lcclnodem = ft.no
       lcclestabdem = ft.estab

       TEXT TO lcsql TEXTMERGE NOSHOW
				UPDATE b_utentes 
				SET nbenef = '<<alltrim(atendimento.pgfDados.Page3.txtU_nbenef.value)>>'
				where no = <<lcclnoDEM >> and estab = <<lcclestabDEM >>
       ENDTEXT
       uf_gerais_actgrelha("", "", lcsql)
    ENDIF
 ENDIF
ENDPROC
**
PROCEDURE uf_atendimento_atualizar_ut_dem
 IF ft.no>200
    IF uf_perguntalt_chama("DESEJA ATUALIZAR OS DADOS DO UTENTE COM OS DADOS DO UTENTE DA RECEITA?", "Sim", "N�o")
       SELECT ft
       lcclnodem = ft.no
       lcclestabdem = ft.estab

       TEXT TO lcsql TEXTMERGE NOSHOW
				UPDATE b_utentes 
				SET nbenef = '<<alltrim(atendimento.pgfDados.Page3.txtU_nbenef.value)>>'
				, nome = '<<alltrim(uCrsAtendComp.nomeutente)>>'
				, tlmvl = '<<alltrim(uCrsAtendComp.contactoutente)>>'
				, ncont = '<<alltrim(atendimento.pgfDados.Page3.txtNIFreceita.value)>>'
				where no = <<lcclnoDEM >> and estab = <<lcclestabDEM >>
       ENDTEXT
       IF uf_gerais_actgrelha("", "", lcsql)
          uf_perguntalt_chama("DADOS ATUALIZADOS COM SUCESSO.", "OK", "", 48)
       ENDIF
       uf_atendimento_selcliente(ft.no, ft.estab, "ATENDIMENTO")
    ENDIF
 ELSE
    uf_perguntalt_chama("N�O PODE ALTERAR OS DADOS DA FICHA DO CONSUMIDOR FINAL.", "OK", "", 48)
 ENDIF
ENDPROC
**
PROCEDURE uf_atendimento_reservas_cliente
 PARAMETER lcclno, lcclestab
 LOCAL lcsitenores
 SELECT ucrse1
 lcsitenores = ucrse1.siteno
 TEXT TO lcsql TEXTMERGE NOSHOW
		select 
			bi.obrano, bi.ref, bi.qtt 
		from
			Bo (nolock)
		inner join 
			bi (nolock) on bi.bostamp = bo.bostamp		
		where
			bo.nmdos like '%Reserva de Cliente%'
			AND bo.fechada	= 0
			AND bi.fechada  = 0
			AND bi.armazem = <<lcSitenoRes>>
			AND bo.site = '<<mySite>>'
			and bo.no=<<lcClNo>>
			and bo.estab=<<lcClEstab>>
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "curResCliSel", lcsql)
    uf_perguntalt_chama("ERRO AO CONSULTAR AS RESERVAS. POR FAVOR CONSULTE O SUPORTE.", "OK", "", 48)
 ENDIF
 IF RECCOUNT("curResCliSel")>0
    uf_regvendas_chama()
    uf_regvendas_filtrarres(.T.)
    IF !USED("ucrsRegVendas")
        uf_regvendas_pesquisarregvendas()
    ENDIF
    regvendas.pgfreg.page1.grdvendas.refresh
    
 ENDIF
 fecha("curResCliSel")
ENDPROC
**
PROCEDURE uf_atendimento_site_compart
 SELECT ucrsatendcomp
 GOTO TOP
 IF EMPTY(ucrsatendcomp.codigo)
    uf_perguntalt_chama("TEM QUE SELECIONAR UMA COMPARTICIPA��O PARA UTILIZAR ESTA OP��O", "OK", "", 48)
 ELSE
    TEXT TO lcsql TEXTMERGE NOSHOW
			select 
				nome, site_compart
			from
				B_UTENTES (nolock)
			where
				no = <<VAL(ucrsatendcomp.codigo)>>
				AND estab	= 0
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "cursiteCompart", lcsql)
       uf_perguntalt_chama("ERRO AO CONSULTAR AS RESERVAS. POR FAVOR CONSULTE O SUPORTE.", "OK", "", 48)
    ENDIF
    IF RECCOUNT("cursiteCompart")>0
       SELECT cursitecompart
       IF LEN(ALLTRIM(cursitecompart.site_compart))=0
          uf_perguntalt_chama("A seguradora "+ALLTRIM(cursitecompart.nome)+" N�o tem o site para comparticipa��es preenchido.", "OK", "", 48)
       ELSE
          DECLARE INTEGER ShellExecute IN shell32.dll INTEGER, STRING, STRING, STRING, STRING, INTEGER
          shellexecute(0, "open", ALLTRIM(cursitecompart.site_compart), "", "", 1)
       ENDIF
    ELSE
       uf_perguntalt_chama("SITE N�O CONFIGURADO NA FICHA PRINCIPAL DA SEGURADORA", "OK", "", 48)
    ENDIF
    fecha("cursiteCompart")
 ENDIF
ENDPROC
**
FUNCTION uf_atendimento_valida_artigo_sem_stock
 LOCAL lcstklin, lcpassavalidacao
 STORE 0 TO lcstklin
 STORE .T. TO lcpassavalidacao

 IF  .NOT. uf_gerais_getparameter_site('ADM0000000086', 'BOOL', mysite)

    SELECT fi
    GOTO TOP
    SELECT ref, SUM(qtt) AS qtt FROM fi WHERE  NOT EMPTY(ref) AND EMPTY(ofistamp) AND partes=0 AND IIF( !EMPTY(tipoR), IIF(ALLTRIM(UPPER(tipoR)) != 'RES', .T., .F.), .T.) GROUP BY ref INTO CURSOR ucrstotqtts READWRITE
    SELECT ucrstotqtts
    GOTO TOP
    SCAN
       lcstklin = ucrstotqtts.qtt
       SELECT ucrsatendst
       GO TOP
       LOCATE FOR ucrsatendst.ref==ucrstotqtts.ref
       IF FOUND()
          IF lcstklin>ucrsatendst.stock .AND. ucrsatendst.stns=.F.
             uf_perguntalt_chama("O ARTIGO "+ALLTRIM(ucrsatendst.ref)+" N�O TEM STOCK SUFICIENTE PARA FINALIZAR A VENDA. POR FAVOR RETIFIQUE!", "OK", "", 48)
             lcpassavalidacao = .F.
          ENDIF
       ENDIF
    ENDSCAN
    fecha("ucrstotqtts")
 ENDIF
 IF lcpassavalidacao
    RETURN .T.
 ELSE
    SELECT fi
    GOTO TOP
    RETURN .F.
 ENDIF
ENDFUNC
**


FUNCTION uf_atendimento_devolveDadosBonusCompart

	 fecha("fi2Aux")
     LOCAL lcTipoBonusId, lcIdExt
     STORE "" TO lcTipoBonusId, lcIdExt           
     SELECT fi
     if(USED("fi2"))
     	 SELECT fi2
     	 GO TOP	 	           
         SELECT * FROM fi2 WHERE fi2.fistamp = ALLTRIM(fi.fistamp) INTO CURSOR fi2Aux READWRITE          
         SELECT fi2Aux 
         GO TOP
         lcTipoBonusId = ALLTRIM(fi2Aux.bonusTipo)
         lcIdExt       = ALLTRIM(fi2Aux.bonusId)
     ENDIF 
     fecha("fi2Aux")
      
      
     fecha("ucrsFiBonusTemp")
	 CREATE CURSOR ucrsFiBonusTemp (bonusTipo C(100), idBonus C(100)) 	
	 INSERT INTO ucrsFiBonusTemp (bonusTipo , idBonus ) VALUES (lcTipoBonusId, lcIdExt )
	 
	 SELECT ucrsFiBonusTemp 
	 GO TOP
ENDFUNC


FUNCTION uf_atendimento_painel_excecao
 LPARAMETERS lcdemparam
 IF (USED("fi"))
    LOCAL lcposicfi, lcdem, lcisdem, lcfamilia
    lcisdem = -1
    lcposicfi = RECNO("FI")
    SELECT fi
    lcdem = fi.dem
    lcfamilia = ALLTRIM(fi.familia)

    
    uf_atendimento_devolveDadosBonusCompart()
  	LOCAL lcIdExt
    lcIdExt = ""
    SELECT ucrsFiBonusTemp
    lcIdExt = ALLTRIM(ucrsFiBonusTemp.idBonus)             
    fecha("ucrsFiBonusTemp")
  

    IF (EMPTY(lcdem))
       lcisdem = 0
    ELSE
       lcisdem = 1
    ENDIF
    IF ( .NOT. EMPTY(lcdemparam))
       lcisdem = 1
    ENDIF
    
    LOCAL lcReceitaNr, lcTipoReceita 
    lcReceitaNr = ''
    lcReceitaNr = ALLTRIM(fi.receita)
    lcTipoReceita = uf_gerais_retornaTipoDispensaEletronica(lcReceitaNr)
    
    
    
    
    IF(!EMPTY(lcReceitaNr))
    	&&PEHM
        IF uf_gerais_compStr(lcTipoReceita, "2")
           	lcIsDem = 2
        ENDIF   
    ENDIF
    
    
    
    
    fecha("uCrsExcecaoPanel")
    lcsql = ""
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_painel_excecoes <<lcIsDem>> , '' , 1, '<<lcFamilia>>', '<<lcIdExt>>'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "uCrsExcecaoPanel", lcsql)
       uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [uCrsExcecaoPanel]. Por favor reinicie o Software.", "OK", "", 16)
       RETURN .F.
    ENDIF
    IF ( .NOT. uf_atendimento_painelatendimento())
       DELETE FROM uCrsExcecaoPanel WHERE type IN ('COMPART', 'INDISPONIVEL')
       SELECT ucrsexcecaopanel
       GOTO TOP
       IF (TYPE("EXCECAOMED")<>"U")
          excecaomed.pageframe1.page1.grid1.refresh
       ENDIF
    ENDIF
    uf_atendimento_excecaomed_preparapainelvalidacoes()
    SELECT ucrsexcecaopanel
    GOTO TOP
    uf_excecaomed_chama()
    SELECT fi
    TRY
       GOTO lcposicfi
    CATCH
       GOTO TOP
    ENDTRY
 ENDIF
ENDFUNC
**
FUNCTION uf_atendimento_excecaomed_preparapainelvalidacoes
 LOCAL lccompartfi, lclobexcecao, lcfistamp, lcindisponivel, lcjustcode, lcposfi
 lcindisponivel = .F.
 lcjustcode = ''
 IF ( .NOT. USED("uCrsExcecaoPanel"))
    RETURN .F.
 ENDIF
 IF ( .NOT. USED("fi"))
    RETURN .F.
 ENDIF
 IF ( .NOT. USED("fi2"))
    RETURN .F.
 ENDIF
 IF ( .NOT. USED("ft"))
    RETURN .F.
 ENDIF
 lcposfi = RECNO("fi")
 SELECT fi
 lccompartfi = fi.u_comp
 lclobexcecao = ALLTRIM(fi.lobs3)
 lcfistamp = ALLTRIM(fi.fistamp)
 SELECT fi2
 GOTO TOP
 SCAN FOR ALLTRIM(fi2.fistamp)=lcfistamp
    IF  .NOT. EMPTY(fi2.indisponivel)
       lcindisponivel = .T.
    ELSE
       lcindisponivel = .F.
    ENDIF
    lcjustcode = ALLTRIM(fi2.justificacao_tecnica_cod)
 ENDSCAN
 SELECT ucrsexcecaopanel
 GOTO TOP
 SCAN
    IF ALLTRIM(ucrsexcecaopanel.type)=="COMPART"
       IF (EMPTY(lccompartfi))
          REPLACE ucrsexcecaopanel.sel WITH .F.
       ELSE
          REPLACE ucrsexcecaopanel.sel WITH .T.
       ENDIF
    ENDIF
 ENDSCAN
 SELECT ucrsexcecaopanel
 GOTO TOP
 SCAN
    IF ALLTRIM(ucrsexcecaopanel.type)=="COMPART"
       IF (EMPTY(lccompartfi))
          REPLACE ucrsexcecaopanel.sel WITH .F.
       ELSE
          REPLACE ucrsexcecaopanel.sel WITH .T.
       ENDIF
    ENDIF
    IF ALLTRIM(ucrsexcecaopanel.type)=="EXCECAO"
       DO CASE
          CASE lclobexcecao=="A" .AND. ALLTRIM(ucrsexcecaopanel.code)=="EA"
             REPLACE ucrsexcecaopanel.sel WITH .T.
          CASE lclobexcecao=="B" .AND. ALLTRIM(ucrsexcecaopanel.code)=="EB"
             REPLACE ucrsexcecaopanel.sel WITH .T.
          CASE lclobexcecao=="C" .AND. ALLTRIM(ucrsexcecaopanel.code)=="EC1"
             REPLACE ucrsexcecaopanel.sel WITH .T.
          CASE lclobexcecao=="C1" .AND. ALLTRIM(ucrsexcecaopanel.code)=="EC1"
             REPLACE ucrsexcecaopanel.sel WITH .T.
          CASE lclobexcecao=="C2" .AND. ALLTRIM(ucrsexcecaopanel.code)=="EC2"
             REPLACE ucrsexcecaopanel.sel WITH .T.
          OTHERWISE
             REPLACE ucrsexcecaopanel.sel WITH .F.
       ENDCASE
    ENDIF
    IF ALLTRIM(ucrsexcecaopanel.type)=="INDISPONIVEL"
       IF (EMPTY(lcindisponivel))
          REPLACE ucrsexcecaopanel.sel WITH .F.
       ELSE
          REPLACE ucrsexcecaopanel.sel WITH .T.
       ENDIF
    ENDIF
    IF ALLTRIM(ucrsexcecaopanel.type)=="JUSTIFICAODEM" OR  ALLTRIM(ucrsexcecaopanel.type)=="JUSTIFICAOPEMH"
    
    
       DO CASE
       		
     	  CASE AT("JT01",ALLTRIM(lcjustcode))>0  .AND. ALLTRIM(ucrsexcecaopanel.code)=="JT01"     	  
     	  	 SELECT ucrsexcecaopanel
             REPLACE ucrsexcecaopanel.sel WITH .T.
          CASE AT("JT02",ALLTRIM(lcjustcode))>0 .AND. ALLTRIM(ucrsexcecaopanel.code)=="JT02"
          	 SELECT ucrsexcecaopanel
             REPLACE ucrsexcecaopanel.sel WITH .T.
          CASE AT("JT03",ALLTRIM(lcjustcode))>0 .AND. ALLTRIM(ucrsexcecaopanel.code)=="JT03"
             SELECT ucrsexcecaopanel
             REPLACE ucrsexcecaopanel.sel WITH .T.
          CASE AT("JT04",ALLTRIM(lcjustcode))>0 .AND. ALLTRIM(ucrsexcecaopanel.code)=="JT04"
             SELECT ucrsexcecaopanel
             REPLACE ucrsexcecaopanel.sel WITH .T.
          CASE AT("JT05",ALLTRIM(lcjustcode))>0 .AND. ALLTRIM(ucrsexcecaopanel.code)=="JT05"
             SELECT ucrsexcecaopanel
             REPLACE ucrsexcecaopanel.sel WITH .T.
          CASE AT("JT06",ALLTRIM(lcjustcode))>0 .AND. ALLTRIM(ucrsexcecaopanel.code)=="JT06"
             SELECT ucrsexcecaopanel
             REPLACE ucrsexcecaopanel.sel WITH .T.
          CASE AT("JT07",ALLTRIM(lcjustcode))>0  .AND. ALLTRIM(ucrsexcecaopanel.code)=="JT07"
             SELECT ucrsexcecaopanel
             REPLACE ucrsexcecaopanel.sel WITH .T.
          CASE AT("JT08",ALLTRIM(lcjustcode))>0  .AND. ALLTRIM(ucrsexcecaopanel.code)=="JT08"
             SELECT ucrsexcecaopanel
             REPLACE ucrsexcecaopanel.sel WITH .T.
          CASE AT("JT09",ALLTRIM(lcjustcode))>0  .AND. ALLTRIM(ucrsexcecaopanel.code)=="JT09"
             SELECT ucrsexcecaopanel
             REPLACE ucrsexcecaopanel.sel WITH .T.
          CASE AT( "JT10",ALLTRIM(lcjustcode))>0  .AND. ALLTRIM(ucrsexcecaopanel.code)=="JT10"
             SELECT ucrsexcecaopanel
             REPLACE ucrsexcecaopanel.sel WITH .T.
          CASE AT( "1",ALLTRIM(lcjustcode))>0  .AND. ALLTRIM(ucrsexcecaopanel.code)=="1"
             SELECT ucrsexcecaopanel
             REPLACE ucrsexcecaopanel.sel WITH .T.
          CASE AT( "2",ALLTRIM(lcjustcode))>0  .AND. ALLTRIM(ucrsexcecaopanel.code)=="2"
             SELECT ucrsexcecaopanel
             REPLACE ucrsexcecaopanel.sel WITH .T.
          CASE AT( "3",ALLTRIM(lcjustcode))>0  .AND. ALLTRIM(ucrsexcecaopanel.code)=="3"
             SELECT ucrsexcecaopanel
             REPLACE ucrsexcecaopanel.sel WITH .T.
          OTHERWISE
             REPLACE ucrsexcecaopanel.sel WITH .F.
       ENDCASE
    ENDIF
    SELECT ucrsexcecaopanel
 ENDSCAN
 SELECT fi
 TRY
    GOTO lcposfi
 CATCH
    GOTO TOP
 ENDTRY
ENDFUNC

FUNCTION uf_atendimento_compart_desconto_entidade
	LPARAMETERS lcAbrev
	LOCAL lcsql, lnDesc
	
	lnDesc = 0
	
	fecha("ucrsCompartOrg") 
	TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_receituario_getEntidade '<<lcAbrev>>'
    ENDTEXT
    IF !uf_gerais_actgrelha("", "ucrsCompartOrg", lcsql)
       uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [ucrsCompartOrg]. Por favor reinicie o Software.", "OK", "", 16)
       RETURN .F.
	ENDIF

	
	SELECT ucrsCompartOrg
	GO TOP
	lnDesc  = ucrsCompartOrg.desconto
	 	
	fecha("ucrsCompartOrg") 
	
	RETURN lnDesc 
	
ENDFUNC




**
FUNCTION uf_compart_efetiva_sem_validacao
 LOCAL lcstampcompart, lcstamplin, lcnrcartao, lcentidade, lcstampcab, lclinabrev, lclinabrev2, lccodacesso, lctokencom, lcnewtokencom, lcadd, lcprogramid, lcnrreceita,lcDesconto
 STORE '' TO lcnrreceita
 STORE '&' TO lcadd
 
 LOCAL lcDesc
 lnDescontoSeguradora = 0
 
 SELECT ucrsatendcomp
 GOTO TOP
 SCAN FOR  .NOT. EMPTY(e_compart)
    IF EMPTY(ucrsatendcomp.ncartao)
       uf_perguntalt_chama("ATEN��O!"+CHR(13)+"TEM DE INSERIR O N� DO CART�O NA RECEITA COMPARTICIPADA.", "OK", "", 48)
       RETURN .F.
    ENDIF
    IF EMPTY(ucrsatendcomp.codacesso)
       REPLACE ucrsatendcomp.codacesso WITH ALLTRIM(STR(RAND()*1000000))
    ENDIF
 ENDSCAN
 lcstampcompart = uf_gerais_stamp()
 IF  .NOT. USED("uCrsLinPresc")
    CREATE CURSOR uCrsLinPresc (fistamp C(30), ncartao C(20), entidade N(6), ftstamp C(25), abrev C(25), abrev2 C(25), codacesso C(10), programid C(30), nrreceita C(30))
 ELSE
    SELECT ucrslinpresc
    DELETE ALL
 ENDIF
 SELECT ucrsatendcomp
 GOTO TOP
 SCAN
    lcstamplin = ucrsatendcomp.fistamp
    lcnrcartao = ucrsatendcomp.ncartao
    lcentidade = ucrsatendcomp.entidade
    lcabrev = ucrsatendcomp.cptorgabrev
    lcabrev2 = ucrsatendcomp.cptorgabrev2
    lccodacesso = ucrsatendcomp.codacesso
    lcprogramid = ALLTRIM(ucrsatendcomp.programid)
    lcnrreceita = ALLTRIM(ucrsatendcomp.nrreceita)
    TEXT TO lcsql TEXTMERGE NOSHOW
			select TOP 1 e_compart from cptpla(nolock) where cptorgabrev='<<ALLTRIM(lcAbrev)>>' and inactivo = 0
    ENDTEXT
    uf_gerais_actgrelha("", "ucrsCompartVal", lcsql)
        
    lnDescontoSeguradora= uf_atendimento_compart_desconto_entidade(lcAbrev)
    
   
    SELECT ucrsCompartVal
    IF RECCOUNT("ucrsCompartVal")>0
       IF ucrscompartval.e_compart=.T.
          SELECT ucrslinpresc
          APPEND BLANK
          REPLACE ucrslinpresc.fistamp WITH lcstamplin
          REPLACE ucrslinpresc.ncartao WITH lcnrcartao
          REPLACE ucrslinpresc.entidade WITH lcentidade
          REPLACE ucrslinpresc.abrev WITH lcabrev
          REPLACE ucrslinpresc.abrev2 WITH lcabrev2
          REPLACE ucrslinpresc.codacesso WITH lccodacesso
          REPLACE ucrslinpresc.programid WITH lcprogramid
          REPLACE ucrslinpresc.nrreceita WITH nrreceita
       ENDIF
    ENDIF
    SELECT ucrsatendcomp
 ENDSCAN
 SELECT DISTINCT ncartao, entidade, abrev, abrev2, codacesso, programid, nrreceita FROM uCrsLinPresc INTO CURSOR uCrsCabPresc READWRITE
 fecha("ucrsCompartVal")
 lcstampcompart = uf_gerais_stamp()
 lcnewtokencom = lcstampcompart
 regua(1, 2, "A efetuar a efetiva��o da comparticipa��o por favor aguarde.")
 SELECT ucrscabpresc
 GOTO TOP
 SCAN
    uf_compart_insert_ft_compart(ALLTRIM(lcstampcompart), ALLTRIM(ucrscabpresc.ncartao), ucrscabpresc.entidade, ALLTRIM(ucrscabpresc.ncartao), 2, ALLTRIM(ucrscabpresc.codacesso), ALLTRIM(ucrscabpresc.abrev), ALLTRIM(ucrscabpresc.programid), ALLTRIM(ucrscabpresc.nrreceita))
    SELECT ucrscabpresc
 ENDSCAN
 lcstamplin = ''
 SELECT ucrslinpresc
 GOTO TOP
 SCAN
    lcstamplin = ucrslinpresc.fistamp
    lcstampcab = ''
    lclinabrev = ucrslinpresc.abrev
    lclinabrev2 = ucrslinpresc.abrev2
    lclincodacesso = ucrslinpresc.codacesso
    SELECT fi
    GOTO TOP
    SCAN
       IF ALLTRIM(fi.fistamp)==ALLTRIM(lcstamplin)
          lcstampcab = ALLTRIM(fi.ftstamp)
       ENDIF
    ENDSCAN
    SELECT ucrslinpresc
    REPLACE ucrslinpresc.ftstamp WITH lcstampcab
 ENDSCAN
 SELECT ucrslinpresc
 GOTO TOP
 SCAN
    uf_compart_insert_fi_compart(ALLTRIM(lcstampcompart), ALLTRIM(ucrslinpresc.fistamp), ALLTRIM(ucrslinpresc.abrev), ALLTRIM(ucrslinpresc.abrev2), ALLTRIM(ucrslinpresc.ncartao), ALLTRIM(ucrslinpresc.codacesso), ALLTRIM(ucrslinpresc.nrreceita), lnDescontoSeguradora)
    SELECT ucrslinpresc
 ENDSCAN
 lcnomejar = "ltsCompartCli.jar"
 lcwsdir = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\ltsCompart'
 lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\ltsCompart\'+ALLTRIM(lcnomejar)+' "--TOKEN='+ALLTRIM(lcnewtokencom)+'"'+' "--IDCL='+ALLTRIM(UPPER(sql_db))+'"'
 lcwspath = "cmd /c cd /d "+lcwsdir+" "+lcadd+lcadd+" javaw -jar "+lcwspath
 owsshell = CREATEOBJECT("WScript.Shell")
 owsshell.run(lcwspath, 0, .T.)
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		select top 1 
			token
			, ftstamp
			, code
			,(case when message<>'' then cast(message as varchar(250)) else 'N�o foi poss�vel validar os dados de comparticipa��o' end) as message
			, ai
			, version
			, ousrdata 
		from 
			ft_compart_result (nolock)
		where 
			token = '<<ALLTRIM(lcNewTokenCom)>>' 
		order by 
			ousrdata desc
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsAuxCompart", lcsql)
    uf_perguntalt_chama("N�o foi possivel validar a comparticipa��o. Por favor proceda com a comparticipa��o manual.", "OK", "", 32)
    atendimento.pgfdados.page3.btn12.label1.caption = 'COMPART. MANUAL'
    atendimento.pgfdados.page3.btn12.label1.left = 8
    regua(2)
    RETURN .F.
 ELSE
    SELECT ucrsauxcompart
    IF ucrsauxcompart.code<>200
       uf_perguntalt_chama(ALLTRIM(ucrsauxcompart.message), "OK", "", 32)
       atendimento.pgfdados.page3.btn12.label1.caption = 'COMPART. MANUAL'
       atendimento.pgfdados.page3.btn12.label1.left = 8
       regua(2)
       RETURN .F.
    ELSE
       lcsql = ''
       TEXT TO lcsql TEXTMERGE NOSHOW
				select * from fi_compart (nolock) where token = '<<ALLTRIM(lcNewTokenCom)>>' order by ousrdata
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "uCrsAuxCompartFi", lcsql)
          uf_perguntalt_chama("N�o foi possivel validar as linhas comparticipa��o. Por favor proceda com a comparticipa��o manual.", "OK", "", 32)
          atendimento.pgfdados.page3.btn12.label1.caption = 'COMPART. MANUAL'
          atendimento.pgfdados.page3.btn12.label1.left = 8
          regua(2)
          RETURN .F.
       ELSE
          SELECT ucrsauxcompartfi
          GOTO TOP
          SCAN
        	 
        	  	
          	 LOCAL lcPercDecimal
          	 lcPercDecimal =   ROUND(ucrsauxcompartfi.returncontributedpercentage/100,4)
        	 
        	 &&aquiiii
        	 
        	 LOCAL lnDescontoHabitual, lnReturnContributedValue
        	 lnDescontoHabitual = 0
        	 lnDescontoHabitual = ROUND((ucrsauxcompartfi.descontoHabitual/100),5)
        	 lnReturnContributedValue = ROUND(ucrsauxcompartfi.returnContributedValue,2)
        
             lcstampficompart = ALLTRIM(ucrsauxcompartfi.fistamp)                      
             lcreturncontributedvalue =   (ROUND(ROUND(ucrsauxcompartfi.pvpOriginal,2) * lcPercDecimal ,2))       
             lcreturncontributedpercentage =  ucrsauxcompartfi.returncontributedpercentage
       
             SELECT fi
             GOTO TOP
             SCAN
                IF lcstampficompart=ALLTRIM(fi.fistamp)
                   DO CASE
                      CASE fi.comp=0 .AND. fi.comp_2=0
                         REPLACE fi.comp WITH lcreturncontributedpercentage
                         REPLACE fi.comp_diploma WITH lcreturncontributedpercentage
                         REPLACE fi.u_ettent1 WITH lcreturncontributedvalue
                         REPLACE fi.u_txcomp WITH 100-lcreturncontributedpercentage
                      CASE fi.comp=0 .AND. fi.comp_2<>0 .AND. lcreturncontributedpercentage=100
                         REPLACE fi.comp WITH 100-fi.comp_2
                         REPLACE fi.comp_diploma WITH 100-fi.comp_2
                         REPLACE fi.u_ettent1 WITH lcreturncontributedvalue
                         REPLACE fi.u_txcomp WITH 100-lcreturncontributedpercentage
                      CASE fi.comp=0 .AND. fi.comp_2<>0 .AND. lcreturncontributedpercentage<>100
                         REPLACE fi.comp WITH (100-fi.comp_2)*(lcreturncontributedpercentage/100)
                         REPLACE fi.comp_diploma WITH (100-fi.comp_2)*(lcreturncontributedpercentage/100)
                         REPLACE fi.u_ettent1 WITH lcreturncontributedvalue
                         REPLACE fi.u_txcomp WITH 100-(fi.comp_2+fi.comp)
                      OTHERWISE
                   ENDCASE
                   IF fi.desconto=0
                   		 IF lcreturncontributedpercentage=100
                         	REPLACE fi.etiliquido WITH 0
                            
                         	REPLACE fi.epv WITH 0
                      	ELSE
	                      REPLACE fi.etiliquido WITH ((fi.u_epvp*fi.qtt)-(fi.u_ettent1+fi.u_ettent2))
                          
    	                  REPLACE fi.epv WITH (fi.u_epvp-((fi.u_ettent1+fi.u_ettent2)/fi.qtt))
    	                ENDIF 
                   ELSE
                      IF lcreturncontributedpercentage=100
                         REPLACE fi.etiliquido WITH 0
                          
                         REPLACE fi.epv WITH 0
                      ELSE
                         REPLACE fi.etiliquido WITH (((fi.u_epvp*fi.qtt)-(fi.u_ettent1))-(((fi.u_epvp*fi.qtt)-(fi.u_ettent1))*(fi.desconto/100)))-fi.u_ettent2
                          
                         REPLACE fi.epv WITH (fi.u_epvp-((fi.u_ettent1+fi.u_ettent2)/fi.qtt))
                      ENDIF
                   ENDIF
                ENDIF
             ENDSCAN
             
           	 SELECT fi2
	         GO TOP
	         SCAN
	          	IF lcstampficompart=ALLTRIM(fi2.fistamp)
	          		REPLACE fi2.descontoHabitual with lnDescontoSeguradora
	          		REPLACE fi2.returnContributedValue with lnReturnContributedValue
	          	ENDIF   
	         ENDSCAN
          
             
             SELECT ucrsauxcompartfi
          ENDSCAN
          
          
        
          fecha("uCrsAuxCompartFi")
          uf_atendimento_infototais()
       ENDIF
       lcsql = ''
       TEXT TO lcsql TEXTMERGE NOSHOW
				select returnContributionCorrelationId from ft_compart (nolock) where token = '<<ALLTRIM(lcNewTokenCom)>>' AND type=2 order by ousrdata
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "uCrsAuxCompartFT_R", lcsql)
          uf_perguntalt_chama("N�o foi possivel validar os dados da comparticipa��o. Por favor contacte o suporte.", "OK", "", 32)
          regua(2)
          RETURN .F.
       ELSE
          LOCAL lcnrelegib_compart
          SELECT ucrsauxcompartft_r
          lcnrelegib_compart = ALLTRIM(ucrsauxcompartft_r.returncontributioncorrelationid)
          SELECT ft2
          REPLACE ft2.nrelegibilidade WITH ALLTRIM(lcnrelegib_compart)
       ENDIF
       fecha("uCrsAuxCompartFT_R")
       compsemsimul = .T.
       tokencompart = ALLTRIM(lcnewtokencom)
       atendimento.grdatend.refresh
       atendimento.refresh()
    ENDIF
 ENDIF
 regua(2)
 fecha("uCrsAuxCompart")
ENDFUNC
**
FUNCTION uf_atendimento_calcvalorreserva
 IF ( .NOT. USED("fi"))
    RETURN .F.
 ENDIF
 LOCAL lcposfi
 lcposfi = RECNO("fi")
 LOCAL lctipor, lctpagres
 SELECT fi
 lctipor = ALLTRIM(fi.tipor)
 lctpagres = ALLTRIM(fi.tpres)
 IF (lctipor=='RES')
    DO CASE
       CASE ALLTRIM(lctpagres)=='RN'
          SELECT fi
          REPLACE fi.etiliquido WITH 0
           
          REPLACE fi.tiliquido WITH 0
       CASE ALLTRIM(lctpagres)=='RADP'
          IF (VARTYPE("ResPagParc")<>'U')
             SELECT fi
             REPLACE fi.etiliquido WITH ROUND(fi.etiliquido*(respagparc/100), 2)
              
             REPLACE fi.tiliquido WITH ROUND(fi.tiliquido*(respagparc/100), 2)
          ENDIF
       OTHERWISE
    ENDCASE
 ENDIF
 SELECT fi
 TRY
    GOTO lcposfi
 CATCH
    GOTO BOTTOM
 ENDTRY
 RETURN .T.
ENDFUNC
**
FUNCTION uf_atendimento_reserva_prod
 IF ft.no=200
    uf_perguntalt_chama("O SISTEMA APENAS PERMITE EFETUAR RESERVAS A CLIENTES COM FICHA. DEVE ESCOLHER UM CLIENTE ANTES DE PODER CONTINUAR.", "OK", "", 64)
    RETURN .F.
 ENDIF
 
 LOCAL lcfp
 STORE 0 TO lcfp
 SELECT fi
 lcfp = RECNO()
 SELECT fi
 IF  .NOT. EMPTY(fi.bistamp) .OR.  .NOT. EMPTY(fi.ofistamp)
    uf_perguntalt_chama("O SISTEMA N�O PERMITE EFETUAR RESERVAS A PRODUTOS ORIGIN�RIOS DE UMA REGULARIZA��O.", "OK", "", 64)
    RETURN .F.
 ENDIF
 IF myvaledescval
    uf_perguntalt_chama("N�O PODE FAZER RESERVAS QUANDO J� EXISTEM VALES NO ATENDIMENTO.", "OK", "", 64)
    RETURN .F.
 ENDIF
 

 
 SELECT fi
 LOCAL lcfilordemcovid
 lcfilordemcovid=LEFT(ALLTRIM(str(fi.lordem)),2)
 ** N�o permitir reservas em testes de covid
 SELECT * FROM fi WHERE ALLTRIM(fi.tipor)=='RM' AND AT('[RS]', ALLTRIM(fi.design))>0 AND LEFT(ALLTRIM(str(fi.lordem)),2)=ALLTRIM(lcfilordemcovid) INTO CURSOR ucrsficovidres
 IF RECCOUNT("ucrsficovidres")>0
 	uf_perguntalt_chama("N�O PODE FAZER RESERVAS EM LINHAS COM O PLANO DE COMPARTICIPA��O RS.", "OK", "", 64)
 	fecha("ucrsficovidres")
    RETURN .F.
 ENDIF 
 fecha("ucrsficovidres")
 
 SELECT * FROM fi WHERE ALLTRIM(fi.tipor)=='RSP' OR ALLTRIM(fi.tipor)=='RM' INTO CURSOR ucrsfireceirasres
 IF RECCOUNT("ucrsfireceirasres")=0
    IF respagparc=0
       IF fi.etiliquido=0 .AND. ALLTRIM(fi.tipor)==''
          SELECT fi
          REPLACE fi.tipor WITH 'RES'
          REPLACE fi.tpres WITH 'RN'
          REPLACE fi.etiliquido WITH 0
           
          REPLACE fi.qtdem WITH fi.qtt
       ELSE
          DO CASE
             CASE ALLTRIM(fi.tpres)==''
                SELECT fi
                REPLACE fi.tipor WITH 'RES'
                REPLACE fi.tpres WITH 'RAD100'
                REPLACE fi.qtdem WITH fi.qtt
                uf_atendimento_eventofi()
             CASE ALLTRIM(fi.tpres)=='RAD100'
                SELECT fi
                REPLACE fi.tipor WITH 'RES'
                REPLACE fi.tpres WITH 'RN'
                REPLACE fi.etiliquido WITH 0
                 
                REPLACE fi.qtdem WITH fi.qtt
             OTHERWISE
                SELECT fi
                REPLACE fi.tipor WITH ''
                REPLACE fi.tpres WITH ''
                REPLACE fi.qtdem WITH 0
                uf_atendimento_eventofi()
          ENDCASE
       ENDIF
       uf_atendimento_infototais()
    ELSE
       IF fi.etiliquido=0 .AND. ALLTRIM(fi.tipor)==''
          SELECT fi
          REPLACE fi.tipor WITH 'RES'
          REPLACE fi.tpres WITH 'RN'
          REPLACE fi.etiliquido WITH 0
           
          REPLACE fi.qtdem WITH fi.qtt
       ELSE
          DO CASE
             CASE ALLTRIM(fi.tpres)==''
                SELECT fi
                REPLACE fi.tipor WITH 'RES'
                REPLACE fi.tpres WITH 'RAD100'
                REPLACE fi.qtdem WITH fi.qtt
                uf_atendimento_eventofi()
             CASE ALLTRIM(fi.tpres)=='RN'
                SELECT fi
                REPLACE fi.tipor WITH ''
                REPLACE fi.tpres WITH ''
                REPLACE fi.qtdem WITH 0
                uf_atendimento_eventofi()
             CASE ALLTRIM(fi.tpres)=='RADP'
                SELECT fi
                REPLACE fi.tipor WITH 'RES'
                REPLACE fi.tpres WITH 'RN'
                REPLACE fi.etiliquido WITH 0
                 
                REPLACE fi.qtdem WITH fi.qtt
             OTHERWISE
                SELECT fi
                REPLACE fi.tipor WITH 'RES'
                REPLACE fi.tpres WITH 'RADP'
                REPLACE fi.qtdem WITH fi.qtt
                uf_atendimento_eventofi()
          ENDCASE
       ENDIF
       uf_atendimento_infototais()
    ENDIF
 ELSE
    LOCAL lcfistamp2
    SELECT fi
    lcfistamp2 = ALLTRIM(fi.fistamp)
    SELECT * FROM fi WHERE ALLTRIM(fi.tipor)=='RES' AND ALLTRIM(fi.fistamp)<>ALLTRIM(lcfistamp2) INTO CURSOR ucrsfireceirasres1 READWRITE
    IF RECCOUNT("ucrsfireceirasres1")<1
       IF respagparc=0
          IF fi.etiliquido=0 .AND. ALLTRIM(fi.tipor)==''
             SELECT fi
             REPLACE fi.tipor WITH 'RES'
             REPLACE fi.tpres WITH 'RN'
             REPLACE fi.etiliquido WITH 0
              
             REPLACE fi.qtdem WITH fi.qtt
          ELSE
             DO CASE
                CASE ALLTRIM(fi.tpres)==''
                   SELECT fi
                   REPLACE fi.tipor WITH 'RES'
                   REPLACE fi.tpres WITH 'RAD100'
                   REPLACE fi.qtdem WITH fi.qtt
                   uf_atendimento_eventofi()
                CASE ALLTRIM(fi.tpres)=='RAD100'
                   SELECT fi
                   REPLACE fi.tipor WITH 'RES'
                   REPLACE fi.tpres WITH 'RN'
                   REPLACE fi.etiliquido WITH 0
                    
                   REPLACE fi.qtdem WITH fi.qtt
                OTHERWISE
                   SELECT fi
                   REPLACE fi.tipor WITH ''
                   REPLACE fi.tpres WITH ''
                   REPLACE fi.qtdem WITH 0
                   uf_atendimento_eventofi()
             ENDCASE
          ENDIF
          uf_atendimento_infototais()
       ELSE
          IF fi.etiliquido=0 .AND. ALLTRIM(fi.tipor)==''
             SELECT fi
             REPLACE fi.tipor WITH 'RES'
             REPLACE fi.tpres WITH 'RN'
             REPLACE fi.etiliquido WITH 0
              
             REPLACE fi.qtdem WITH fi.qtt
          ELSE
             DO CASE
                CASE ALLTRIM(fi.tpres)==''
                   SELECT fi
                   REPLACE fi.tipor WITH 'RES'
                   REPLACE fi.tpres WITH 'RAD100'
                   REPLACE fi.qtdem WITH fi.qtt
                   uf_atendimento_eventofi()
                CASE ALLTRIM(fi.tpres)=='RN'
                   SELECT fi
                   REPLACE fi.tipor WITH ''
                   REPLACE fi.tpres WITH ''
                   REPLACE fi.qtdem WITH 0
                   uf_atendimento_eventofi()
                CASE ALLTRIM(fi.tpres)=='RADP'
                   SELECT fi
                   REPLACE fi.tipor WITH 'RES'
                   REPLACE fi.tpres WITH 'RN'
                   REPLACE fi.etiliquido WITH 0
                    
                   REPLACE fi.qtdem WITH fi.qtt
                OTHERWISE
                   SELECT fi
                   REPLACE fi.tipor WITH 'RES'
                   REPLACE fi.tpres WITH 'RADP'
                   REPLACE fi.qtdem WITH fi.qtt
                   uf_atendimento_eventofi()
             ENDCASE
          ENDIF
          uf_atendimento_infototais()
       ENDIF
    ELSE
       LOCAL lcrectipor, lcrectpres
       STORE '' TO lcrectipor, lcrectpres
       SELECT ucrsfireceirasres1
       lcrectipor = ALLTRIM(ucrsfireceirasres1.tipor)
       lcrectpres = ALLTRIM(ucrsfireceirasres1.tpres)
       SELECT fi
       IF  .NOT. EMPTY(fi.tpres)
          SELECT fi
          REPLACE fi.tipor WITH ''
          REPLACE fi.tpres WITH ''
          REPLACE fi.qtdem WITH 0
          uf_atendimento_eventofi()
       ELSE
          REPLACE fi.tipor WITH ALLTRIM(lcrectipor)
          REPLACE fi.tpres WITH ALLTRIM(lcrectpres)
          DO CASE
             CASE ALLTRIM(fi.tpres)=='RN'
                REPLACE fi.etiliquido WITH 0
                 
                REPLACE fi.qtdem WITH fi.qtt
             CASE ALLTRIM(fi.tpres)=='RADP'
                REPLACE fi.qtdem WITH fi.qtt
                uf_atendimento_eventofi()
             OTHERWISE
                REPLACE fi.qtdem WITH 0
                uf_atendimento_eventofi()
          ENDCASE
          uf_atendimento_infototais()
       ENDIF
       fecha("ucrsfireceirasres1")
    ENDIF
 ENDIF
 fecha("ucrsfireceirasres")
 atendimento.grdatend.refresh()
 IF TYPE("pesqstocks")=="U"
    SELECT fi
    TRY
       SKIP -1
    CATCH
    ENDTRY
    atendimento.grdatend.setfocus()
    TRY
       SKIP
    CATCH
       GOTO TOP
    ENDTRY
    atendimento.grdatend.setfocus()
 ENDIF
 SELECT fi
 IF lcfp>0
    TRY
       GOTO lcfp
    CATCH
       GOTO BOTTOM
    ENDTRY
 ENDIF
 DO CASE
    CASE ALLTRIM(fi.tipor)=='RES'
       REPLACE fi.qtdem WITH fi.qtt
    OTHERWISE
       REPLACE fi.qtdem WITH 0
 ENDCASE
 uf_atendimento_verificalordem()
 SELECT fi
 atendimento.grdatend.setfocus()
 LOCAL lcselfistamp
 SELECT fi
 lcselfistamp = ALLTRIM(fi.fistamp)
 SELECT ucrsfireservas
 GOTO TOP
 LOCATE FOR ALLTRIM(ucrsfireservas.fistamp)==ALLTRIM(lcselfistamp)
 IF FOUND()
    SELECT ucrsfireservas
    DELETE
 ENDIF
 SELECT fi
 IF  .NOT. EMPTY(fi.tpres)
    SELECT ucrsfireservas
    GOTO BOTTOM
    APPEND BLANK
    REPLACE ucrsfireservas.tpres WITH fi.tpres
    REPLACE ucrsfireservas.fistamp WITH fi.fistamp
    REPLACE ucrsfireservas.ref WITH fi.ref
    REPLACE ucrsfireservas.design WITH fi.design
    REPLACE ucrsfireservas.qtt WITH fi.qtt
    REPLACE ucrsfireservas.etiliquido WITH fi.etiliquido
    REPLACE ucrsfireservas.ivaincl WITH fi.ivaincl
    REPLACE ucrsfireservas.tabiva WITH fi.tabiva
    REPLACE ucrsfireservas.armazem WITH fi.armazem
    REPLACE ucrsfireservas.lordem WITH fi.lordem
    REPLACE ucrsfireservas.epv WITH fi.epv
    REPLACE ucrsfireservas.ecusto WITH fi.ecusto
    REPLACE ucrsfireservas.epvori WITH fi.epvori
    REPLACE ucrsfireservas.u_epvp WITH fi.u_epvp
    REPLACE ucrsfireservas.u_ettent1 WITH fi.u_ettent1
    REPLACE ucrsfireservas.u_ettent2 WITH fi.u_ettent2
    REPLACE ucrsfireservas.u_txcomp WITH fi.u_txcomp
    REPLACE ucrsfireservas.comp_tipo WITH fi.comp_tipo
    REPLACE ucrsfireservas.comp WITH fi.comp
    REPLACE ucrsfireservas.comp_diploma WITH fi.comp_diploma
    REPLACE ucrsfireservas.comp_2 WITH fi.comp_2
    REPLACE ucrsfireservas.comp_diploma_2 WITH fi.comp_diploma_2
    REPLACE ucrsfireservas.comp_tipo_2 WITH fi.comp_tipo_2

 ENDIF
ENDFUNC
**
PROCEDURE uf_atendimento_reserva_cursores
 lcressad = .F.
 lcresad100 = .F.
 lcresadparc = .F.

*!*	 SELECT ucrsfireservas
*!*	 GOTO TOP
*!*	 SCAN
*!*	 	UPDATE  ucrsfireservas from ucrsfireservas inner join fi on ALLTRIM(ucrsfireservas.fistamp) = ALLTRIM(fi.fistamp);
*!*	 		   SET ref =  ALLTRIM(fi.ref), design = ALLTRIM(fi.design), qtt = fi.qtt, etiliquido = fi.etiliquido, ivaincl = fi.ivaincl;
*!*	           ,tabiva = fi.tabiva, armazem = fi.armazem, lordem = fi.lordem, epv = fi.epv, ecusto = fi.ecusto, epvori = fi.epvori, u_epvp = fi.u_epvp;
*!*	           ,u_ettent1= fi.u_ettent1, u_ettent2 = fi.u_ettent2, u_txcomp = fi.u_txcomp, comp_tipo = fi.comp_tipo, comp = fi.comp, comp_diploma = fi.comp_diploma;
*!*	           ,comp_2 = fi.comp_2, comp_diploma_2 = fi.comp_diploma_2, comp_tipo_2 = fi.comp_tipo_2
*!*	    SELECT ucrsfireservas
*!*	    IF uCrsFiReservas.etiliquido=0 AND ALLTRIM(ucrsfireservas.tpres)!='RN'
*!*	    	replace ucrsfireservas.tpres WITH 'RN'
*!*	    	UPDATE fi SET tipor='RN' WHERE ALLTRIM(fi.fistamp)=ALLTRIM(ucrsfireservas.fistamp)
*!*	    ENDIF 
*!*	 ENDSCAN  
 
 SELECT ucrsfireservas
 GOTO TOP
 SCAN
    IF ALLTRIM(ucrsfireservas.tpres)=='RAD100'
       lcresad100 = .T.
    ENDIF
    IF ALLTRIM(ucrsfireservas.tpres)=='RN'
       lcressad = .T.
    ENDIF
    IF ALLTRIM(ucrsfireservas.tpres)=='RADP'
       lcresadparc = .T.
    ENDIF
 ENDSCAN
 IF USED("uCrsFiReservasAD100")
    fecha("uCrsFiReservasAD100")
 ENDIF
 IF USED("uCrsFiReservasNOR")
    fecha("uCrsFiReservasNOR")
 ENDIF
 IF USED("uCrsFiReservasADP")
    fecha("uCrsFiReservasADP")
 ENDIF

 LOCAL lcvaltotres
 IF lcresad100=.T.
    lcvaltotres = 0
    SELECT ucrsfireservas
    GOTO TOP
    SELECT * FROM uCrsFiReservas WHERE ALLTRIM(ucrsfireservas.tpres)='RAD100' INTO CURSOR uCrsFiReservasAD100 READWRITE
    SELECT ucrsfireservasad100
    GOTO TOP
    SCAN
       lcvaltotres = lcvaltotres+ucrsfireservasad100.etiliquido
    ENDSCAN
    SELECT ucrscabvendas
    GOTO BOTTOM
    APPEND BLANK
    REPLACE ucrscabvendas.design WITH 'Reserva 100% AD.'
    REPLACE ucrscabvendas.etiliquido WITH lcvaltotres
    REPLACE ucrscabvendas.valorpagar WITH lcvaltotres
    REPLACE ucrscabvendas.modopag WITH .F.
    REPLACE ucrscabvendas.lordem WITH 910000
    REPLACE ucrscabvendas.partes WITH 0
    REPLACE ucrscabvendas.fact WITH .F.
    REPLACE ucrscabvendas.factdev WITH .F.
    REPLACE ucrscabvendas.factpago WITH .F.
    REPLACE ucrscabvendas.sel WITH .T.
    replace ucrscabvendas.nifgenerico WITH .f.
    replace ucrscabvendas.oferta WITH .f.
	

 ENDIF
 IF lcressad=.T.
    lcvaltotres = 0
    SELECT ucrsfireservas
    GOTO TOP
    SELECT * FROM uCrsFiReservas WHERE ALLTRIM(ucrsfireservas.tpres)='RN' INTO CURSOR uCrsFiReservasNOR READWRITE
    SELECT ucrsfireservasnor
    GOTO TOP
    SCAN
       lcvaltotres = lcvaltotres+ucrsfireservasnor.etiliquido
    ENDSCAN
    SELECT ucrscabvendas
    GOTO BOTTOM
    APPEND BLANK
    REPLACE ucrscabvendas.design WITH 'Reserva S/ AD.'
    REPLACE ucrscabvendas.etiliquido WITH lcvaltotres
    replace ucrscabvendas.nifgenerico WITH .f.
    replace ucrscabvendas.oferta WITH .f.
    


 ENDIF
 IF lcresadparc=.T.
    lcvaltotres = 0
    SELECT ucrsfireservas
    GOTO TOP
    SELECT * FROM uCrsFiReservas WHERE ALLTRIM(ucrsfireservas.tpres)='RADP' INTO CURSOR uCrsFiReservasADP READWRITE
    SELECT ucrsfireservasadp
    GOTO TOP
    SCAN
       lcvaltotres = lcvaltotres+ucrsfireservasadp.etiliquido
    ENDSCAN
    SELECT ucrscabvendas
    GOTO BOTTOM
    APPEND BLANK
    REPLACE ucrscabvendas.design WITH 'Reserva '+ALLTRIM(STR(respagparc))+'% AD.'
    REPLACE ucrscabvendas.etiliquido WITH lcvaltotres
    REPLACE ucrscabvendas.valorpagar WITH lcvaltotres
    REPLACE ucrscabvendas.modopag WITH .F.
    REPLACE ucrscabvendas.lordem WITH 920000
    REPLACE ucrscabvendas.partes WITH 0
    REPLACE ucrscabvendas.fact WITH .F.
    REPLACE ucrscabvendas.factdev WITH .F.
    REPLACE ucrscabvendas.factpago WITH .F.
    REPLACE ucrscabvendas.sel WITH .T.
    replace ucrscabvendas.nifgenerico WITH .f.
    replace ucrscabvendas.oferta WITH .f.
    
    

 ENDIF
ENDPROC
**
FUNCTION uf_atendimento_verifres
 LPARAMETERS lcatendimento
 LOCAL lcfp, lcresver, lclordem
 STORE 0 TO lcfp, lclordem
 STORE .F. TO lcresver
 SELECT fi
 lclordem = fi.lordem
 lcfp = RECNO()
 IF  .NOT. lcatendimento
    SELECT fi
    GOTO TOP
    SCAN
       IF LEFT(ALLTRIM(STR(fi.lordem)), 1)==LEFT(ALLTRIM(STR(lclordem)), 1)
          IF ALLTRIM(fi.tipor)=='RES'
             lcresver = .T.
          ENDIF
       ENDIF
    ENDSCAN
 ELSE
    SELECT fi
    GOTO TOP
    SCAN
       IF ALLTRIM(fi.tipor)=='RES'
          lcresver = .T.
       ENDIF
    ENDSCAN
 ENDIF
 SELECT fi
 IF lcfp>0
    TRY
       GOTO lcfp
    CATCH
       GOTO BOTTOM
    ENDTRY
 ENDIF
 IF lcresver=.F. .AND. USED("ucrscabvendas")
    SELECT ucrscabvendas
    lclordem = ucrscabvendas.lordem
    lcfp = RECNO()
    SELECT ucrscabvendas
    GOTO TOP
    SCAN
       IF AT('Reserva', ALLTRIM(ucrscabvendas.design))>0
          lcresver = .T.
       ENDIF
    ENDSCAN
    SELECT ucrscabvendas
    IF lcfp>0
       TRY
          GOTO lcfp
       CATCH
          GOTO BOTTOM
       ENDTRY
    ENDIF
 ENDIF
 RETURN lcresver
ENDFUNC
**
FUNCTION uf_atendimento_importaencomenda
 IF ft.no=200
    uf_perguntalt_chama("TEM DE ESCOLHER O CLIENTE PARA ESCOLHER A ENCOMENDA.", "OK", "", 64)
    RETURN .F.
 ENDIF
 PUBLIC stampenc
 stampenc = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		select bostamp, obrano,  CONVERT(varchar, dataobra, 102) as data 
		from bo (nolock) 
		WHERE nmdos='Encomenda de Cliente' and no=<<ft.no>> and estab=<<ft.estab>> and fechada=0
			and bostamp in (select distinct regstamp from anexos (nolock) where tabela='documentos' and keyword='Receita M�dica')
			and site='<<ALLTRIM(mysite)>>'
 ENDTEXT
 uf_gerais_actgrelha("", "curencpend", lcsql)
 SELECT curencpend
 uf_valorescombo_chama("stampenc", 0, "curencpend", 2, "bostamp ", "obrano , data")
 IF  .NOT. EMPTY(stampenc)
    TEXT TO lcsql TEXTMERGE NOSHOW
			SELECT * FROM bi (nolock) WHERE bostamp='<<ALLTRIM(stampenc)>>' AND bi.ref<>''
    ENDTEXT
    uf_gerais_actgrelha("", "lincurencpend", lcsql)
    SELECT lincurencpend
    IF RECCOUNT("lincurencpend")>0
       SELECT lincurencpend
       GOTO TOP
       SCAN
          LOCAL lcrefenc, lcqttenc, lbbistampenc, lcprunit
          lcrefenc = ALLTRIM(lincurencpend.ref)
          lcqttenc = lincurencpend.qtt
          lbbistampenc = lincurencpend.bistamp
          lcprunit = lincurencpend.edebito
          uf_atendimento_adicionalinhafi(.F., .F.)
          SELECT fi
          lcposfi = RECNO("fi")
          REPLACE fi.ref WITH lcrefenc
          REPLACE fi.qtt WITH lcqttenc
          REPLACE fi.bistamp WITH lbbistampenc
          uf_atendimento_actref()
          SELECT fi
          TRY
             GOTO lcposfi
          CATCH
             GOTO BOTTOM
          ENDTRY
          uf_atendimento_eventofi()
          SELECT fi
          TRY
             GOTO lcposfi
          CATCH
             GOTO BOTTOM
          ENDTRY
          uf_atendimento_dadosst()
          SELECT fi
          TRY
             GOTO lcposfi
          CATCH
             GOTO BOTTOM
          ENDTRY
          SELECT fi
          REPLACE fi.u_epvp WITH lcprunit
          REPLACE fi.epv WITH lcprunit
          REPLACE fi.epvori WITH lcprunit
          uf_atendimento_fiiliq()
          LOCAL lcposicfi
          lcposicfi = RECNO("FI")
          TRY
             GOTO lcposicfi
          CATCH
          ENDTRY
       ENDSCAN
    ENDIF
 ENDIF
 uf_atendimento_actvalorescab()
 uf_atendimento_infototaiscab()
 uf_atendimento_infototais()
 fecha("curencpend")
ENDFUNC
**
FUNCTION uf_atendimento_validareservaefr
 

 IF ( .NOT. uf_atendimento_verifres(.T.))
    RETURN .F.
 ENDIF
 IF (USED("uCrsAtendComp"))
    SELECT ucrsatendcomp
    GOTO TOP
    SCAN
	    IF (!EMPTY(ucrsatendcomp.e_compart)) AND !uf_gerais_savida_afp(ALLTRIM(ucrsatendcomp.cptorgabrev))
	       RETURN .T.
	    ENDIF
    ENDSCAN    
 ENDIF
 RETURN .F.
ENDFUNC


FUNCTION uf_atendimento_validaReservaComVale 
	 IF ( uf_atendimento_verifres(.T.) AND myvaledescval)
	    RETURN .T.
	 ENDIF
	 RETURN .F.
ENDFUNC

FUNCTION uf_atendimento_validareservaEfSNSExt

 IF ( .NOT. uf_atendimento_verifres(.T.))
    RETURN .F.
 ENDIF
 

 IF (USED("uCrsAtendComp"))
    SELECT ucrsatendcomp
    GOTO TOP
    SCAN
	    IF(TYPE("ucrsatendcomp.snsExterno")!="U")    
		    IF (!EMPTY(ucrsatendcomp.snsExterno))
	    	   RETURN .T.
		    ENDIF
		ENDIF
	ENDSCAN    
 ENDIF
 RETURN .F.
ENDFUNC


FUNCTION uf_atendimento_validareservaMcdt

 IF ( .NOT. uf_atendimento_verifres(.T.))
    RETURN .F.
 ENDIF
 
 
 IF (USED("uCrsAtendComp"))
    SELECT ucrsatendcomp
    GOTO TOP
    SCAN
	    IF(TYPE("ucrsatendcomp.mcdt")!="U")    
		    IF (!EMPTY(ucrsatendcomp.mcdt))
	    	   RETURN .T.
		    ENDIF
		ENDIF
	ENDSCAN   
 ENDIF
 RETURN .F. 
ENDFUNC


FUNCTION uf_atendimento_devolveMsgEpisodioFaturado
	LPARAMETERS lbMostraAlerta, lcRefFacturacao
	if(USED("ucrsEpisodiosPemh"))


		SELECT ucrsEpisodiosPemh
		GO TOP
		SCAN FOR !EMPTY(ALLTRIM(ucrsEpisodiosPemh.idProcesso)) AND   !EMPTY(ALLTRIM(idEpisodio )) 
			fecha("ucrsEpTemp")
			local lcSqlPEMH
			lcSqlPEMH = ""
			TEXT TO lcSqlPEMH TEXTMERGE NOSHOW
				exec up_receituario_episodio_facturado '<<Alltrim(ucrsEpisodiosPemh.idProcesso)>>',  '<<Alltrim(ucrsEpisodiosPemh.idEpisodio)>>','<<mySite>>', '<<lcRefFacturacao>>'
		 	ENDTEXT
		 	
		 	IF !uf_gerais_actGrelha("","ucrsEpTemp",lcSqlPEMH)
				uf_perguntalt_chama("OCORREU UM ERRO A VALIDAR O EPISODIO PEMH","OK","",16)
		 		RETURN .f.
		 	ENDIF
		 	
		 	
		 	if USED("ucrsEpTemp") AND  RECCOUNT("ucrsEpTemp")>0
		 		SELECT ucrsEpTemp
		 		if(lbMostraAlerta and !EMPTY(ALLTRIM(ucrsEpTemp.msg)))
		 			 uf_perguntalt_chama(ALLTRIM(ucrsEpTemp.msg), "OK", "", 16)
		 		ENDIF	
		 		RETURN ucrsEpTemp.resultado
		 	ENDIF
		 	
		 	
		 	fecha("ucrsEpTemp")
		
		ENDSCAN
	ENDIF
	
	fecha("ucrsEpisodiosPemh")
	
	RETURN 0

ENDFUNC


FUNCTION uf_atendimento_validaSePemhFacturado
	LPARAMETERS lcRefFacturacao
	LOCAL lcPosFiAuxTemp, lcLordem
	
	IF(USED("fi"))		
		lcPosFiAuxTemp = RECNO("fi")
		
		fecha("ucrsTempLordemFacturacao")
		fecha("ucrsEpisodiosPemh")
		CREATE CURSOR ucrsTempLordemFacturacao(lordem C(10))
		CREATE CURSOR ucrsEpisodiosPemh(idProcesso C(254), idEpisodio C(254))


		
		**cria cursor com referencias com servi�o de facturacao
		SELECT fi
		GO TOP
		SCAN
			IF uf_gerais_compStr(fi.ref, lcRefFacturacao) AND fi.partes==0
				INSERT INTO ucrsTempLordemFacturacao (lordem ) VALUES (LEFT(ALLTRIM(str(fi.lordem)),2))				
			ENDIF			
		ENDSCAN
		
		
		
		IF (TYPE("atendimento")<>"U")

				SELECT ucrsTempLordemFacturacao 
				GO TOP
				SCAN
					SELECT fi
					GO TOP
					SCAN FOR uf_gerais_compStr((LEFT(ALLTRIM(str(fi.lordem)),2)), ucrsTempLordemFacturacao.lordem) AND !EMPTY(ALLTRIM(fi.id_validacaodem))
						SELECT fi2
						GO TOP
						LOCATE FOR  uf_gerais_compStr(fi.fistamp, fi2.fistamp) AND !EMPTY(fi2.idProcesso)  AND !EMPTY(fi2.idEpisodio)
						if(FOUND())
							INSERT INTO ucrsEpisodiosPemh (idProcesso , idEpisodio) VALUES (ALLTRIM(fi2.idProcesso), ALLTRIM(fi2.idEpisodio))
						ENDIF
			
						SELECT fi
					ENDSCAN
					SELECT ucrsTempLordemFacturacao 
				ENDSCAN
		ELSE
		
				SELECT ucrsTempLordemFacturacao 
				GO TOP
				SCAN
					SELECT fi
					GO TOP
					SCAN FOR  !EMPTY(ALLTRIM(fi.id_validacaodem))
						SELECT fi2
						GO TOP
						LOCATE FOR  uf_gerais_compStr(fi.fistamp, fi2.fistamp) AND  !EMPTY(fi2.idProcesso) AND !EMPTY(fi2.idEpisodio)
						if(FOUND())
							INSERT INTO ucrsEpisodiosPemh (idProcesso , idEpisodio) VALUES (ALLTRIM(fi2.idProcesso), ALLTRIM(fi2.idEpisodio))
						ENDIF
			
						SELECT fi
					ENDSCAN
					SELECT ucrsTempLordemFacturacao 
				ENDSCAN
			
		ENDIF
		
		
		uf_atendimento_devolveMsgEpisodioFaturado(.T., lcRefFacturacao)
		
		

		fecha("ucrsTempLordemFacturacao")
		fecha("ucrsEpisodiosPemh")
		
		SELECT  fi
		TRY
		    GOTO lcPosFiAuxTemp 
		    CATCH
		    GOTO TOP
		ENDTRY	
	ENDIF
	
	RETURN .T.
	

ENDFUNC
FUNCTION uf_atendimento_validaSeEpisodioPEHMFacturado
    LPARAMETERS lcRef, lcReceita
     
    LOCAL lcReceitaPEMH, lcTipoReceita
    lcReceitaPEMH = .F.

    * Verifica se a tabela est� aberta e se lcReceita est� vazia
    IF USED("ucrsatendcomp") AND EMPTY(lcReceita)  
        
        * Percorre a tabela ucrsatendcomp
        SELECT ucrsatendcomp
        GO TOP
        
        SCAN FOR !EMPTY(ALLTRIM(ucrsatendcomp.nrreceita))           
            lcTipoReceita = uf_gerais_retornaTipoDispensaEletronica(ALLTRIM(ucrsatendcomp.nrreceita))           
            
            * 2 - PEHM
            IF uf_gerais_compStr(lcTipoReceita, "2")
                lcReceitaPEMH = .T.              
            ENDIF       
        ENDSCAN
    
    ELSE
        * Se lcReceita foi fornecida, verifica diretamente
        lcTipoReceita = uf_gerais_retornaTipoDispensaEletronica(ALLTRIM(lcReceita))

        * Verifica se � PEHM
        IF uf_gerais_compStr(lcTipoReceita, "2")
            lcReceitaPEMH = .T.
        ENDIF
    ENDIF
    
    * Se uma receita PEHM foi encontrada, valida se foi faturada
    IF lcReceitaPEMH 
        uf_atendimento_validaSePemhFacturado(lcRef)      
    ENDIF
    
    RETURN .T.

ENDFUNC



FUNCTION uf_atendimento_validaTipoDispensasReceitasDiferentes()

    IF USED("ucrsatendcomp")
        LOCAL lcReceitaMCDT, lcReceitaDEM, lcReceitaPEMH, lnConta  
        lcReceitaMCDT = .F.
        lcReceitaDEM = .F.
        lcReceitaPEMH = .F.
        lnConta  = 0
        
        
        SELECT ucrsatendcomp
        GO TOP
        
        SCAN FOR !EMPTY(ALLTRIM(ucrsatendcomp.nrreceita))
            LOCAL lcTipoReceita
            lcTipoReceita = uf_gerais_retornaTipoDispensaEletronica(ALLTRIM(ucrsatendcomp.nrreceita))
            
            * 0 - MCDT Requisi��o
            IF uf_gerais_compStr(lcTipoReceita, "0")
                lcReceitaMCDT = .T.
            ENDIF
            
            * 1 - DEM
            IF uf_gerais_compStr(lcTipoReceita, "1")
                lcReceitaDEM = .T.
            ENDIF
            
            * 2 - PEHM
            IF uf_gerais_compStr(lcTipoReceita, "2")
                lcReceitaPEMH = .T.
            ENDIF
        ENDSCAN
    ENDIF
    
    IF(lcReceitaMCDT)    
    	lnConta = lnConta  + 1 
    ENDIF
    
    IF(lcReceitaDEM)   
    	lnConta = lnConta  + 1 
    ENDIF
    
    IF(lcReceitaPEMH)
    	lnConta = lnConta  + 1 
    ENDIF
	
	if(lnConta >=2 )
		 RETURN .T.
	ENDIF
    	
    
    RETURN .F.
ENDFUNC



FUNCTION uf_atendimento_validareservaPlano

 IF ( .NOT. uf_atendimento_verifres(.T.))
    RETURN .T.
 ENDIF
 
 
 IF (USED("uCrsAtendComp"))
    SELECT ucrsatendcomp
    GOTO TOP
    SCAN
	    IF(TYPE("ucrsatendcomp.permiteReserva")!="U")    
		    IF (EMPTY(ucrsatendcomp.permiteReserva))
	    	   RETURN .F.
		    ENDIF
		ENDIF
	ENDSCAN   
 ENDIF
 RETURN .T.
ENDFUNC



FUNCTION uf_atendimento_validaMultiVendaMcdt

 IF (uf_atendimento_devolveNumeroVendasAtendimento()<=1)
    RETURN .F.
 ENDIF
 
 
 IF (USED("uCrsAtendComp"))
    SELECT ucrsatendcomp
    GOTO TOP
    SCAN
	    IF(TYPE("ucrsatendcomp.mcdt")!="U")    
		    IF (!EMPTY(ucrsatendcomp.mcdt))
	    	   RETURN .T.
		    ENDIF
		ENDIF
	ENDSCAN  
 ENDIF
 RETURN .F.
ENDFUNC



FUNCTION uf_atendimento_validaSuspensaEfSNSExt
 IF (EMPTY(uf_atendimento_verificasuspensa()))
    RETURN .F.
 ENDIF
 IF (USED("uCrsAtendComp"))
    SELECT ucrsatendcomp
    GOTO TOP
    SCAN
	    IF (TYPE("ucrsatendcomp.snsExterno")!="U")
		    IF (!EMPTY(ucrsatendcomp.snsExterno))
	    	   RETURN .T.
		    ENDIF
		ENDIF
	ENDSCAN 
    
 ENDIF
 RETURN .F.
ENDFUNC

FUNCTION uf_atendimento_validaSuspensaMcdt
 IF (EMPTY(uf_atendimento_verificasuspensa()))
    RETURN .F.
 ENDIF
 IF (USED("uCrsAtendComp"))
    SELECT ucrsatendcomp
	GO TOP
	SCAN
	    IF (TYPE("ucrsatendcomp.mcdt")!="U")
		    IF (!EMPTY(ucrsatendcomp.mcdt))
	    	   RETURN .T.
		    ENDIF
		ENDIF 
    ENDSCAN
 ENDIF
 RETURN .F.
ENDFUNC


FUNCTION uf_atendimento_validaSuspensaPlano
 IF (EMPTY(uf_atendimento_verificasuspensa()))
    RETURN .T.
 ENDIF
 IF (USED("uCrsAtendComp"))
    SELECT ucrsatendcomp
	GO TOP
	SCAN
	    IF (TYPE("ucrsatendcomp.permiteSuspensa")!="U")
		    IF (EMPTY(ucrsatendcomp.permiteSuspensa))
	    	   RETURN .F.
		    ENDIF
		ENDIF 
    ENDSCAN
 ENDIF
 RETURN .T.
ENDFUNC







FUNCTION uf_atendimento_validareserva_planors

 IF ( .NOT. uf_atendimento_verifres(.T.))
    RETURN .F.
 ENDIF
 IF (USED("uCrsAtendComp"))
    SELECT ucrsatendcomp
    GOTO TOP
    IF ALLTRIM(ucrsatendcomp.codigo)=='RS'
       RETURN .T.
    ENDIF
 ENDIF
 RETURN .F.
ENDFUNC


FUNCTION uf_atendimento_normalizaLinhasMcdt

 LOCAL lcLordem, lcIsDem, lcDemPosAux 
 lcLordem =100000
 lcDemPosAux = '1'
 
 lcIsDem = .f.
 

 
 IF (USED("uCrsAtendComp"))
    SELECT ucrsatendcomp
    GO TOP
    IF(TYPE("ucrsatendcomp.mcdt")!="U")    
	    IF (!EMPTY(ucrsatendcomp.mcdt))
    	   LOCAL lcFiStamp  
    	   lcFiStamp  = ALLTRIM(ucrsatendcomp.fistamp)
    	   SELECT fi
    	   GO TOP
    	   SCAN FOR ALLTRIM(fi.fistamp)==ALLTRIM(lcFiStamp)
    	   		lcLordem = 	fi.lordem
    	   		lcIsDem  =  fi.dem
    	   		lcDemPosAux = fi.id_validacaoDem
    	   SELECT fi		
    	   ENDSCAN

		   uf_atendimento_marcaDemMCDT(lcIsDem  ,lcLordem)
		   uf_atendimento_marcaIdValidacaoDem(lcDemPosAux,lcLordem)
	    ENDIF
	ENDIF 
 ENDIF
 


ENDFUNC

FUNCTION uf_atendimento_validaReserva_planoBonus

 IF ( .NOT. uf_atendimento_verifres(.T.))
    RETURN .F.
 ENDIF
 
 LOCAL lcTipoBonusId, lcBonusTipo
 STORE "" TO lcTipoBonusId,lcBonusTipo
 IF USED("FI") AND USED("FI2")
	 SELECT FI
	 GO TOP 
	 SCAN
		 SELECT fi2
		 GO TOP
		 SCAN FOR ALLTRIM(fi2.fistamp) = ALLTRIM(fi.fistamp)
			lcTipoBonusId = ALLTRIM(fi2.bonusId)
			lcBonusTipo= ALLTRIM(fi2.bonusTipo)
			if(!EMPTY(lcBonusTipo) and !EMPTY(lcTipoBonusId ))
			
				fecha("ucrsCompartBonusValida")

				TEXT TO lcSql TEXTMERGE NOSHOW
					exec up_receituario_validaCompartBonus  '<<ALLTRIM(lcBonusTipo)>>', '<<ALLTRIM(lcTipoBonusId )>>'
				ENDTEXT
				uf_gerais_actgrelha("", "ucrsCompartBonusValida", lcSql)
				
				if(RECCOUNT("ucrsCompartBonusValida")>0)
					if(USED("ucrsCompartBonusValida"))
						SELECT ucrsCompartBonusValida
						GO TOP
						if(EMPTY(ucrsCompartBonusValida.permiteReserva))
							fecha("ucrsCompartBonusValida")
							return .t.
						ENDIF						
					ENDIF			
				ENDIF
				
				fecha("ucrsCompartBonusValida")
			
			ENDIF
		  SELECT fi2
		 ENDSCAN
		 SELECT FI
	 ENDSCAN
	 
 ENDIF
 RETURN .F.

ENDFUNC



FUNCTION uf_atendimento_validaSuspensa_planoBonus

 IF ( .NOT. uf_atendimento_verificasuspensa(.T.))
    RETURN .F.
 ENDIF
 
 LOCAL lcTipoBonusId, lcBonusTipo
 STORE "" TO lcTipoBonusId,lcBonusTipo
 IF USED("FI") AND USED("FI2")
	 SELECT FI
	 GO TOP 
	 SCAN
		 SELECT fi2
		 GO TOP
		 SCAN FOR ALLTRIM(fi2.fistamp) = ALLTRIM(fi.fistamp)
			lcTipoBonusId = ALLTRIM(fi2.bonusId)
			lcBonusTipo= ALLTRIM(fi2.bonusTipo)
			if(!EMPTY(lcBonusTipo) and !EMPTY(lcTipoBonusId ))
			
				fecha("ucrsCompartBonusValida")

				TEXT TO lcSql TEXTMERGE NOSHOW
					exec up_receituario_validaCompartBonus  '<<ALLTRIM(lcBonusTipo)>>', '<<ALLTRIM(lcTipoBonusId )>>'
				ENDTEXT
				uf_gerais_actgrelha("", "ucrsCompartBonusValida", lcSql)
				
				if(RECCOUNT("ucrsCompartBonusValida")>0)
					if(USED("ucrsCompartBonusValida"))
						SELECT ucrsCompartBonusValida
						GO TOP
						if(EMPTY(ucrsCompartBonusValida.permiteSuspensa))
							fecha("ucrsCompartBonusValida")
							return .t.
						ENDIF						
					ENDIF			
				ENDIF
				
				fecha("ucrsCompartBonusValida")
			
			ENDIF
		  SELECT fi2
		 ENDSCAN
		 SELECT FI
	 ENDSCAN
	 
 ENDIF
 RETURN .F.

ENDFUNC








FUNCTION uf_atendimento_printPosologia
    LPARAMETERS uv_todos

    atendimento.tmroperador.reset()

    IF !USED("FI")
        uf_perguntalt_chama("OCORREU UMA ANOMALIA AO IMPRIMIR A POSOLOGIA. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
        return .f.
    ENDIF

    IF RECCOUNT("FI") = 0
        uf_perguntalt_chama("N�o existe nenhuma linha para imprimir!", "OK", "", 16)
        return .f.
    ENDIF

    IF uv_todos

        select uCrsAtendCL 

        SELECT ;
            fi.design,;
            LEFT(uCrsAtendST.u_posprog,254) as texto,; 
            uCrsAtendCL.nome ;
        FROM ;
            fi ;
            INNER JOIN uCrsAtendST ON fi.fistamp = uCrsAtendST.ststamp ;
            JOIN uCrsAtendCL ON 1=1 ;
        WHERE ;
            !EMPTY(fi.ref) ;
            AND !EMPTY(uCrsAtendST.u_posprog) ;
            AND !EMPTY(FI.QTT) ;
        INTO CURSOR uc_printPos

    ELSE

        select uCrsAtendCL 

        select fi

        uv_curStamp = fi.fistamp
        
        SELECT ;
            fi.design, ;
            "-" + LEFT(uCrsAtendST.u_posprog,254) as texto, ; 
	        uCrsAtendCL.nome ;
        FROM ;
            fi ;
            INNER JOIN uCrsAtendST ON fi.fistamp = uCrsAtendST.ststamp ;
	        JOIN uCrsAtendCL ON 1=1 ;
        WHERE ;
            fi.fistamp = uv_curStamp ;
            AND !EMPTY(fi.ref) ;
            AND !EMPTY(uCrsAtendST.u_posProg) ;
            AND !EMPTY(FI.QTT) ;
        INTO CURSOR uc_printPos


    ENDIF

    IF USED("uc_printPos")

        IF RECCOUNT("uc_printPos") = 0
            uf_perguntalt_chama("N�o existe nenhuma linha para imprimir!", "OK", "", 16)
            return .f.
        ENDIF

    ELSE

        uf_perguntalt_chama("N�o existe nenhuma linha para imprimir!", "OK", "", 16)
        return .f.

    ENDIF
    

    uf_etiquetas_posologia_predef()

    WAIT WINDOW "" TIMEOUT 1

    uf_etiquetas_posologia_linha()


ENDFUNC



FUNCTION uf_atendimento_criaConsultaSNSExternoEletronica
	LPARAMETERS lcNomeServ, lcTipoOp
	
	LOCAL lcsql 
	
	
	
	if(USED("ucrsSNSExternoAtend"))
	
	  	
	  SELECT ucrsSNSExternoAtend
	  GO TOP
	  SCAN
	  		 LOCAL lcToken, lcChave, lcChaveRel, lcNrSnS, lcNome, lcReceitaNr, lcBenef, lcNasc,lcQtDisp, lcSexo
	  		 LOCAL lcResultado_consulta_cod, lcResultado_consulta_descr
	  		 lcToken = ALLTRIM(ucrsSNSExternoAtend.token)
	  		 lcChave= ALLTRIM(ucrsSNSExternoAtend.fistamp)
	  		 lcChaveRel= "Valida-" + ALLTRIM(ucrsSNSExternoAtend.fistamp)
	  		
	  		 lcNasc = '1900.01.01'
			 lcQtDisp= INT(ucrsSNSExternoAtend.qtDisp)
			 STORE '' TO lcNome ,lcNrSnS ,lcResultado_consulta_cod, lcResultado_consulta_descr, lcSexo 
			 
	
	  		 
	  		 If(USED("ucrsAtendCl"))
	  		 	SELECT ucrsAtendCl
	  		 	lcNome  = ALLTRIM(ucrsAtendCl.nome)
	  		 	lcNasc  = uf_gerais_getdate(ucrsatendcl.nascimento)
	  		 	lcNrSnS = ALLTRIM(ucrsatendcl.nbenef)
	  		 	lcSexo  = ALLTRIM(ucrsatendcl.sexo)
	  		 ENDIF
	  		 
	  		 
	  		 
	  		 lcReceitaNr = STREXTRACT(ucrsSNSExternoAtend.design, ':', ' -', 1, 0)
	  		 lcResultado_consulta_cod = ALLTRIM(ucrsSNSExternoAtend.resultado_consulta_cod)
	  		 lcResultado_consulta_descr = ALLTRIM(ucrsSNSExternoAtend.resultado_consulta_descr)
	  		 
				
	  		 
	  		 if(USED("uCrsAtendComp"))
	  		 	SELECT uCrsAtendComp
	  		 	GO TOP
	  		 	lcDataDisp = uf_gerais_getdate(uCrsAtendComp.datadispensa)
	  		 ENDIF
	  		 
	
	  		 
 
	  		 lcsql = ''
		     TEXT TO lcsql TEXTMERGE NOSHOW
					INSERT INTO [dbo].[Dispensa_Eletronica]
			           ([token]
			           ,[chave_pedido]
			           ,[chave_pedido_relacionado]
			           ,[receita_nr]
			           ,[receita_tipo]
			           ,[beneficiario_nr]
			           ,[utente_descr]
					   ,local_presc_dominio
					   ,utente_nascimento
					   ,local_presc_cod
			           ,[data_cre]
			           ,[resultado_consulta_cod]
			           ,[resultado_consulta_descr]
			           ,[qtdisp]
			           ,[dispensa_data]
			           
			           
			           )
			     VALUES
			           (
			           '<<ALLTRIM(lcToken)>>'
			           ,'<<ALLTRIM(lcChave)>>'
			           ,'<<ALLTRIM(lcChaveRel)>>'
			           ,'<<ALLTRIM(lcReceitaNr)>>'
					   ,'EXT'
					   ,'<<ALLTRIM(lcNrSnS)>>'
					   ,'<<ALLTRIM(lcNome)>>'
					   ,'<<ALLTRIM(lcTipoOp)>>'
					   ,'<<lcNasc>>'
					   ,'<<ALLTRIM(lcSexo)>>' 
			           ,getdate()
			           ,'<<ALLTRIM(lcResultado_consulta_cod )>>'
			           ,'<<ALLTRIM(lcResultado_consulta_descr)>>'
			           ,'<<lcQtDisp>>'
			           ,'<<lcDataDisp>>'
					)	
       			ENDTEXT
		       IF  !uf_gerais_actgrelha("", "", lcsql)
		          uf_perguntalt_chama("N�o foi possivel proceder ao servi�o de Valida��o da  TRAG. Contacte o suporte.", "OK", "", 32)
		          RETURN .F.
		       ENDIF    

		           
		           
	     SELECT ucrsSNSExternoAtend
	  ENDSCAN
	  
	ENDIF
	
	

	

	RETURN .t.
ENDFUNC


FUNCTION  uf_atendimento_validacaoSNSExternaCampos
	LPARAMETERS lcTipo
	
	
	LOCAL lcTexto 
	
	lcTexto = ''
	
	if(EMPTY(lcTipo))
		RETURN lcTexto 
	ENDIF
	
	
	IF(UPPER(ALLTRIM(lcTipo)) == "COVID")
	
		 If(!USED("ucrsAtendCl"))
		 	 lcTexto = lcTexto +CHR(13)+"Dados do Utente;"
	  		 RETURN lcTexto 	
	  	 ENDIF

		IF EMPTY(ALLTRIM(uCrsAtendCl.nbenef))
          	lcTexto = lcTexto + IIF(EMPTY(lcTexto), '', chr(13)) + "Numero SNS;"
    	ENDIF
    	
    	
	  	 
	  	**IF uf_gerais_getdate(uCrsAtendCl.nascimento)='1900.01.01'
        **  	lcTexto = lcTexto +CHR(13)+"Data Nascimento;"
    	**ENDIF
	**
		**IF EMPTY(ALLTRIM(ucrsAtendCl.sexo))
		**	lcTexto = lcTexto + IIF(EMPTY(lcTexto), '', chr(13)) + "Sexo;"
		**ENDIF
		
	ENDIF
	
	RETURN lcTexto 
	
ENDFUNC


FUNCTION uf_atendimento_preencheBeneficiarioSnsExterno
	LPARAMETERS lcTipo
	
	
	LOCAL lcPosFiAuxTemp, lcPosCabVendasAuxTemp, lcNrBenef
	lcNrBenef = ""
	
	
	if(USED("fi"))
		lcPosFiAuxTemp = RECNO("fi")
	ENDIF
	
	
	if(USED("ucrscabvendas"))
		lcPosCabVendasAuxTemp= RECNO("ucrscabvendas")
	ENDIF
	

	
	if(USED("uCrsAtendCl"))
		SELECT uCrsAtendCl
		GO TOP
		lcNrBenef = ALLTRIM(uCrsAtendCl.nbenef)
	ENDIF
	
	
	IF (USED("fi") AND !EMPTY(ALLTRIM(lcNrBenef)) AND  UPPER(ALLTRIM(lcTipo)) == "COVID")
		UPDATE fi SET u_nbenef2 = ALLTRIM(lcNrBenef) WHERE ALLTRIM(STREXTRACT(fi.design, '[', ']', 1, 0)) = "RS"
	ENDIF
	
	IF (USED("ucrscabvendas") AND  !EMPTY(ALLTRIM(lcNrBenef))  AND  UPPER(ALLTRIM(lcTipo)) == "COVID")
		UPDATE ucrscabvendas SET u_nbenef2 = ALLTRIM(lcNrBenef) WHERE ALLTRIM(STREXTRACT(ucrscabvendas.design, '[', ']', 1, 0)) = "RS"
	ENDIF
			
	
	if(USED("fi"))		
		SELECT  fi
		TRY
		    GOTO lcPosFiAuxTemp 
		    CATCH
		    	GOTO TOP
		ENDTRY		
	ENDIF
	
	if(USED("ucrscabvendas"))		
		SELECT  ucrscabvendas
		TRY
		    GOTO lcPosCabVendasAuxTemp
		    CATCH
		    	GOTO TOP
		ENDTRY		
	ENDIF
		
ENDFUNC



FUNCTION  uf_atendimento_validacaoSNSExterna
	LPARAMETERS lcRef, lcTipo, lcTipoOp
	
	if(!USED("ucrsatendcomp"))
		RETURN .T.
	ENDIF
	
	LOCAL lcPosTemp
	lcPosTemp= RECNO("ucrsatendcomp")
	

	
	LOCAL lcValidExternalPlan
	lcValidExternalPlan= .f.

	
	SELECT ucrsatendcomp
	GO TOP
	SCAN
		IF(TYPE("ucrsatendcomp.snsExterno")!="U")
			if(!empty(ucrsatendcomp.snsExterno))
				lcValidExternalPlan = .t.
			ENDIF
		ENDIF 
		
	ENDSCAN
	
	
	
	if(!lcValidExternalPlan)
		SELECT  ucrsatendcomp
		TRY
	    GOTO lcPosTemp
	    CATCH
	    	GOTO TOP
	    ENDTRY
		RETURN .t.
	endif
	
	


	LOCAL lcTextoCab,lcTextoValida 
	lcTextoCab= 'Dados em Falta: ' + CHR(13)
	
	

	
	lcTextoValida = uf_atendimento_validacaoSNSExternaCampos(lcTipo)
	

	IF !EMPTY(lcTextoValida)
		uf_perguntalt_chama(SUBSTR(lcTextoCab+lcTextoValida ,1,254) , "OK", "", 64)
		RETURN .f.
	ELSE
		uf_atendimento_preencheBeneficiarioSnsExterno(lcTipo)
	endif 
	

	if(!uf_atendimento_criaCursorSNSExterno(lcRef))
		uf_perguntalt_chama(SUBSTR(lcTextoCab,1,254) , "OK", "", 64)
		RETURN .f.
	endif 


	if(!USED("ucrsSNSExternoAtend"))
		RETURN .f.
	ENDIF
	
		
	if(USED("ucrsSNSExternoAtendTotalTemp"))
		fecha("ucrsSNSExternoAtendTotalTemp")
	ENDIF
		
	
	if(!uf_atendimento_criaConsultaSNSExternoEletronica(lcTipo,lcTipoOp))
		RETURN .f.
	ENDIF
	
	LOCAL lcValid, lcSucessCode
	lcValid = .t.
	lcSucessCode  = "100004030001"
	


	IF TYPE("lcCodResp")!="U"			   
		IF ALLTRIM(lcCodResp)='100004030001'
		 	myChamaSinave = 'Com Comparticipa��o'
		 ELSE
		 	myChamaSinave = 'Sem Comparticipa��o'
		ENDIF 
	ELSE
		myChamaSinave = 'N�o chama'
	ENDIF 
	
	
	
	
	if(USED("ucrsSNSExternoAtendTotalTemp"))
		delete from ucrsSNSExternoAtendTotalTemp
	endif
	
	SELECT ucrsSNSExternoAtend
	GO TOP
	SCAN
		
	    LOCAL lcReceitaNr, lcToken, lcValida
	    lcReceitaNr  = ALLTRIM(STREXTRACT(ucrsSNSExternoAtend.design, ':', ' -', 1, 0))
	    lcToken = ALLTRIM(ucrsSNSExternoAtend.token)       
	    lcValida = uf_atendimento_receitasElectronicaSNSExterno(lcTipoOp,"A Validar receita: "+ALLTRIM(lcReceitaNr)+" no Sistema Central de Prescri��es...", lcToken, lcReceitaNr)
	 		
	
		if(USED("ucrsDadosExternoDEM") and UPPER(ALLTRIM(lcTipoOp))=="VALIDACAO_TRAG")

			uf_geraCursorReceitaValidar()
			
		
			
			DELETE FROM ucrsreceitavalidar WHERE receita_tipo = "EXT"
			
			if(RECCOUNT("ucrsDadosExternoDEM")==0)
				uf_perguntalt_chama("O servi�o n�o retornou qualquer resultado, por favor, volte a tentar.", "OK", "", 48)
				regua(2)
				if(USED("ucrsDadosExternoDEM"))
					fecha("ucrsDadosExternoDEM")
				ENDIF
				
				if(USED("ucrsSNSExternoAtendTotalTemp"))
					fecha("ucrsSNSExternoAtendTotalTemp")
				ENDIF

				RETURN .f.
			ENDIF
			
			
			if(USED("ucrsSNSExternoAtendTotalTemp"))		
				SELECT ucrsSNSExternoAtendTotalTemp
				APPEND FROM DBF('ucrsDadosExternoDEM')								
			ELSE
				SELECT * from ucrsDadosExternoDEM INTO CURSOR ucrsSNSExternoAtendTotalTemp READWRITE	
			ENDIF
			
			
		
			
    	    SELECT ucrsSNSExternoAtendTotalTemp
		    GO TOP
		    SCAN
			       
			    IF ALLTRIM(ucrsSNSExternoAtendTotalTemp.resultado_validacao_cod)!=ALLTRIM(lcSucessCode)
			    
			   
			       SELECT ucrsreceitavalidar
			       GOTO BOTTOM
			       APPEND BLANK
			       REPLACE ucrsreceitavalidar.token WITH ALLTRIM(ucrsSNSExternoAtendTotalTemp.token)
			       REPLACE ucrsreceitavalidar.retorno_erro_cod WITH ALLTRIM(ucrsSNSExternoAtendTotalTemp.resultado_validacao_cod)
			       REPLACE ucrsreceitavalidar.retorno_erro_descr WITH ALLTRIM(ucrsSNSExternoAtendTotalTemp.resultado_validacao_descr)
			       REPLACE ucrsreceitavalidar.resultado_validacao_cod WITH ALLTRIM(ucrsSNSExternoAtendTotalTemp.resultado_validacao_cod)
			       REPLACE ucrsreceitavalidar.resultado_validacao_descr WITH ALLTRIM(ucrsSNSExternoAtendTotalTemp.resultado_validacao_descr)
			       REPLACE ucrsreceitavalidar.receita_nr WITH ALLTRIM(ucrsSNSExternoAtendTotalTemp.receita)
			       REPLACE ucrsreceitavalidar.receita WITH ALLTRIM(ucrsSNSExternoAtendTotalTemp.receita)
			       REPLACE ucrsreceitavalidar.receita_tipo WITH "EXT"


			       
			    ENDIF
			    
			    
			    
			    if(ALLTRIM(ucrsSNSExternoAtendTotalTemp.token) == ALLTRIM(ucrsSNSExternoAtend.token))
			    	REPLACE ucrsSNSExternoAtend.resultado_validacao_descr WITH ALLTRIM(ucrsSNSExternoAtendTotalTemp.resultado_validacao_descr)
		        	REPLACE ucrsSNSExternoAtend.resultado_validacao_cod WITH ALLTRIM(ucrsSNSExternoAtendTotalTemp.resultado_validacao_cod)
		        	UPDATE fi SET token = ALLTRIM(ucrsSNSExternoAtend.token), dem = .f. WHERE LEFT(ALLTRIM(STR(fi.lordem)), 2) =  ALLTRIM(ucrsSNSExternoAtend.lOrdem)
		        	UPDATE uCrsAtendComp  SET ucrsatendcomp.token = ALLTRIM(fi.token) FROM uCrsAtendComp INNER JOIN fi ON fi.fistamp=ucrsatendcomp.fistamp WHERE  uCrsAtendComp.fistamp  =  ALLTRIM(ucrsSNSExternoAtend.fistamp )
			   		if(USED("ucrscabvendas"))
			   			UPDATE ucrscabvendas SET ucrscabvendas.token = ALLTRIM(fi.token) FROM ucrscabvendas INNER JOIN fi ON fi.lordem=ucrscabvendas.lordem	
			   		ENDIF
			   		
			    ENDIF
			    
			    			       
		        
		  
		    ENDSCAN
		    

		    
	    ENDIF
		
	    			    	
    	SELECT ucrsSNSExternoAtend
	ENDSCAN
	
	
	SELECT ucrsSNSExternoAtend
	GO TOP
	SCAN
		IF(ALLTRIM(ucrsSNSExternoAtend.resultado_validacao_cod)!=ALLTRIM(lcSucessCode))
			lcValid   = .f.
		ENDIF		
	ENDSCAN
	

    if(EMPTY(lcValid ))
		uf_infovenda_valida()	
		
	ENDIF
	
	if(USED("ucrsreceitavalidar "))
		DELETE FROM ucrsreceitavalidar WHERE receita_tipo = "EXT"
	ENDIF
			
	
	
	
	regua(2)
	if(USED("ucrsDadosExternoDEM"))
		fecha("ucrsDadosExternoDEM")
	ENDIF
	
	if(USED("ucrsSNSExternoAtendTotalTemp"))
		fecha("ucrsSNSExternoAtendTotalTemp")
	ENDIF


	
	
	RETURN lcValid  
		

ENDFUNC




FUNCTION uf_atendimento_receitasEfetivarSNSExterno

	LOCAL lcValid,lcSucessCode
	lcValid = .t.
	lcSucessCode = "100004040001"
	
	if(!USED("ucrsSNSExternoAtend"))
		RETURN .t.
	ENDIF
	
	
	LOCAL lcTipoOp
	lcTipoOp = 'EFETIVACAO_TRAG'
	
	if(USED("ucrsSNSExternoAtendTotalTemp"))
		delete from ucrsSNSExternoAtendTotalTemp
	ENDIF
	
	
	

	
	SELECT ucrsSNSExternoAtend
	GO TOP
	SCAN 
		
	    LOCAL lcReceitaNr, lcToken, lcValida, lcValidacao
	    lcReceitaNr  =  ALLTRIM(STREXTRACT(ucrsSNSExternoAtend.design, ':', ' -', 1, 0))
	    lcToken = ALLTRIM(ucrsSNSExternoAtend.token)   
	    lcValidaValidacao = ALLTRIM(ucrsSNSExternoAtend.resultado_validacao_cod)        
	    lcValida= uf_atendimento_receitasElectronicaSNSExterno(lcTipoOp,"A Efectivar receita: "+ALLTRIM(lcReceitaNr)+" no Sistema Central de Prescri��es...", lcToken, lcReceitaNr)

	 		
		
		if(USED("ucrsDadosExternoDEM"))
		
			 uf_geraCursorReceitaValidar()
		
			 DELETE FROM ucrsreceitavalidar WHERE receita_tipo = "EXT"
			 
			 	
			if(RECCOUNT("ucrsDadosExternoDEM")==0)
				uf_perguntalt_chama("O servi�o n�o retornou qualquer resultado, por favor, volte a tentar.", "OK", "", 48)
				regua(2)
				if(USED("ucrsDadosExternoDEM"))
					fecha("ucrsDadosExternoDEM")
				ENDIF
				
				if(USED("ucrsSNSExternoAtendTotalTemp"))
					fecha("ucrsSNSExternoAtendTotalTemp")
				ENDIF

				RETURN .f.
			ENDIF
				
			 if(USED("ucrsSNSExternoAtendTotalTemp"))		
				SELECT ucrsSNSExternoAtendTotalTemp
				APPEND FROM DBF('ucrsDadosExternoDEM')								
			ELSE
				SELECT * from ucrsDadosExternoDEM INTO CURSOR ucrsSNSExternoAtendTotalTemp READWRITE	
			ENDIF

		    SELECT ucrsSNSExternoAtendTotalTemp
			GO TOP
			SCAN
			       
			    IF ALLTRIM(ucrsSNSExternoAtendTotalTemp.resultado_efetivacao_cod)!=ALLTRIM(lcSucessCode) OR EMPTY(ALLTRIM(ucrsSNSExternoAtendTotalTemp.resultado_comprovativo_registo))
			    
			   
			       SELECT ucrsreceitavalidar
			       GOTO BOTTOM
			       APPEND BLANK
			       REPLACE ucrsreceitavalidar.token WITH ALLTRIM(ucrsSNSExternoAtendTotalTemp.token)
			       REPLACE ucrsreceitavalidar.retorno_erro_cod WITH ALLTRIM(ucrsSNSExternoAtendTotalTemp.resultado_efetivacao_cod)
			       REPLACE ucrsreceitavalidar.retorno_erro_descr WITH ALLTRIM(ucrsSNSExternoAtendTotalTemp.resultado_efetivacao_descr)
			       REPLACE ucrsreceitavalidar.resultado_efetivacao_cod WITH ALLTRIM(ucrsSNSExternoAtendTotalTemp.resultado_efetivacao_cod)
			       REPLACE ucrsreceitavalidar.resultado_efetivacao_descr WITH ALLTRIM(ucrsSNSExternoAtendTotalTemp.resultado_efetivacao_descr)
			       REPLACE ucrsreceitavalidar.receita_nr WITH ALLTRIM(ucrsSNSExternoAtendTotalTemp.receita)
			       REPLACE ucrsreceitavalidar.receita WITH ALLTRIM(ucrsSNSExternoAtendTotalTemp.receita)
			       REPLACE ucrsreceitavalidar.receita_tipo WITH "EXT"
			  
			       
			    ENDIF	
			    
			    
			        
			    if(ALLTRIM(ucrsSNSExternoAtendTotalTemp.token) == ALLTRIM(ucrsSNSExternoAtend.token))
			    	REPLACE ucrsSNSExternoAtend.resultado_efetivacao_descr WITH ALLTRIM(ucrsSNSExternoAtendTotalTemp.resultado_efetivacao_descr)
		        	REPLACE ucrsSNSExternoAtend.resultado_efetivacao_cod WITH ALLTRIM(ucrsSNSExternoAtendTotalTemp.resultado_efetivacao_cod)
		        	REPLACE ucrsSNSExternoAtend.resultado_comprovativo_registo WITH ALLTRIM(ucrsSNSExternoAtendTotalTemp.resultado_comprovativo_registo)			        
			    ENDIF
			    
		    ENDSCAN
		    

		    
	    ENDIF
		
	    			    	
    	SELECT ucrsSNSExternoAtend
	ENDSCAN
	
	SELECT ucrsSNSExternoAtend
	GO TOP
	SCAN
		IF(ALLTRIM(ucrsSNSExternoAtend.resultado_efetivacao_cod)!=ALLTRIM(lcSucessCode) or EMPTY(ALLTRIM(ucrsSNSExternoAtend.resultado_comprovativo_registo)))
			lcValid   = .f.
		ENDIF		
	ENDSCAN
	


    if(EMPTY(lcValid))
		uf_infovenda_valida()	
		DELETE FROM ucrsreceitavalidar WHERE receita_tipo = "EXT"	
	ENDIF
	
	RETURN 	lcValid  

ENDFUNC			

FUNCTION uf_atendimento_receitasAnularSNSExterno


	LOCAL lcValid,lcSucessCode, lcTipoOp
	lcValid = .t.
	lcSucessCode = "100004040001"
	
	
	
	if(!USED("ucrsSNSExternoAtend"))
		RETURN .t.
	ENDIF
	
	
	LOCAL lcTipoOp
	lcTipoOp = 'ANULACAO_TRAG'
	
	if(USED("ucrsSNSExternoAtendTotalTemp"))
		delete from ucrsSNSExternoAtendTotalTemp
	ENDIF
	
	

			
	SELECT ucrsSNSExternoAtend
	GO TOP
	SCAN 
		
	    LOCAL lcReceitaNr, lcToken, lcValida, lcValidacao
	    lcReceitaNr  =  ALLTRIM(STREXTRACT(ucrsSNSExternoAtend.design, ':', ' -', 1, 0))
	    lcToken = ALLTRIM(ucrsSNSExternoAtend.token)   
		IF(ALLTRIM(ucrsSNSExternoAtend.resultado_efetivacao_cod)==ALLTRIM(lcSucessCode))	           
	    	uf_atendimento_receitasElectronicaSNSExterno(lcTipoOp,"A Anular receita: "+ALLTRIM(lcReceitaNr)+" no Sistema Central de Prescri��es...", lcToken, lcReceitaNr)	    
	    ENDIF
	    

	ENDSCAN 
	
	
	regua(2)
	if(USED("ucrsDadosExternoDEM"))
		fecha("ucrsDadosExternoDEM")
	ENDIF
	
	
	
	if(USED("ucrsSNSExternoAtendTotalTemp"))
		fecha("ucrsSNSExternoAtendTotalTemp")
	ENDIF
	
	if(USED("ucrsSNSExternoAtend"))
	  fecha("ucrsSNSExternoAtend")
	ENDIF
	
	
	IF(vartype(pagamento)!='U')
		uf_PAGAMENTO_sair()
	ENDIF
	
	


ENDFUNC		


FUNCTION uf_atendimento_receitasElectronicaSNSExterno
	LPARAMETERS lcTipo, lcTexto,lcToken,lcReceitaNr
	
	

	 SELECT ucrse1
	 IF ALLTRIM(ucrse1.tipoempresa)<>"FARMACIA"
	 	 uf_perguntalt_chama("Funcionalidade indispon�vel para farmacias.", "OK", "", 32)
	    RETURN .F.
	 ENDIF
	 SELECT ucrse1
	 IF EMPTY(ALLTRIM(ucrse1.u_codfarm))
	    uf_perguntalt_chama("C�digo de Farm�cia n�o configurado. Contacte o suporte.", "OK", "", 32)
	    RETURN .F.
	 ENDIF
	 SELECT ucrse1
	 lcCodFarm = ALLTRIM(ucrse1.u_codfarm)
		
	 lcTexto = ALLTRIM(lcTexto)
	
	
	 regua(0, 3, lcTexto)
	 regua(1, 1, lcTexto)
	 
	 lcSql = ''

	 IF !USED("ucrsServerName")
	    TEXT TO lcsql TEXTMERGE NOSHOW
				Select @@SERVERNAME as dbServer
	    ENDTEXT
	    IF  .NOT. uf_gerais_actgrelha("", "ucrsServerName", lcsql)
	       uf_perguntalt_chama("N�o foi possivel proceder ao servi�o de Dispensa. Contacte o suporte.", "OK", "", 32)
	       regua(2)
	       RETURN .F.
	    ENDIF
	ENDIF
	SELECT ucrsservername
	lcdbserver = ALLTRIM(ucrsservername.dbserver)
	lcnomejar = "LtsDispensaEletronica.jar"
	IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.F.
	    lctestjar = "0"
	ELSE
	    lctestjar = "1"
	ENDIF
	lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\DEM\'+ALLTRIM(lcnomejar)+ ' ' + ALLTRIM(lcTipo)  +  ' "--token='+ALLTRIM(lcToken)+'"'+' --nrReceita='+ALLTRIM(lcreceitanr)+;
	' --codFarmacia='+ALLTRIM(lcCodFarm)+' --dbServer='+ALLTRIM(lcdbserver)+' --dbName='+UPPER(ALLTRIM(sql_db))+' --site='+UPPER(ALLTRIM(STRTRAN(ALLTRIM(mysite), ' ', '_')))+' --test='+lctestjar
	IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.T.
	    _CLIPTEXT = lcwspath
	    uf_perguntalt_chama("Webservice de teste...", "OK", "", 64)
	ENDIF
	lcwspath = "javaw -jar "+myEncodingFileJar+lcwspath
	owsshell = CREATEOBJECT("WScript.Shell")
	owsshell.run(lcwspath, 0, .T.)
	regua(1, 2, lcTexto)
	 
	fecha("ucrsDadosExternoDEM")
	 
	TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_dem_resultadoSNSExterno '<<ALLTRIM(lcToken)>>'
    ENDTEXT
    IF  !uf_gerais_actgrelha("", "ucrsDadosExternoDEM", lcsql)
       uf_perguntalt_chama("N�o foi possivel verificar dados. Contacte o suporte.", "OK", "", 32)
       regua(2)
       RETURN .F.
    ENDIF
    
    if(!USED("ucrsDadosExternoDEM"))
    	 uf_perguntalt_chama("N�o foi possivel verificar dados de resposta. Contacte o suporte.", "OK", "", 32)
		 regua(2)
		 RETURN .F.
    ENDIF
    

    
    if(USED("ucrsDadosExternoDEM"))
    	if(RECCOUNT("ucrsDadosExternoDEM")==0)
    	      uf_perguntalt_chama("N�o foi possivel verificar dados de resposta. Contacte o suporte.", "OK", "", 32)
		      regua(2)
		      RETURN .F.
    	ENDIF
    	
    ENDIF
	
	 regua(2)
	 RETURN .T.
ENDFUNC

FUNCTION uf_geraCursorReceitaValidar

	if(!USED("ucrsreceitavalidar"))
	
		 CREATE CURSOR ucrsReceitaValidar (token C(36), receita C(19), resultado_validacao_cod C(12), resultado_validacao_descr C(200),;
		 resultado_efetivacao_cod C(12), resultado_efetivacao_descr C(200), retorno_erro_cod C(12),	retorno_erro_descr C(200), receita_nr C(19),;
		 codacesso C(4), coddiropcao C(6), receita_tipo C(4), receita_psico C(2)) 

	ENDIF

ENDFUNC

FUNCTION uf_atendimento_validaNrSnsSNSExterno

	
	LOCAL lcExterno
	
	lcExterno = .f.

	if(USED("ucrsatendcomp") and USED("ucrsAtendCl") )
		
		  SELECT ucrsatendcomp
		    IF(TYPE("ucrsatendcomp.snsExterno")!="U")    
			    IF (!EMPTY(ucrsatendcomp.snsExterno))
		    	   lcExterno  = .t.
			    ENDIF
			ENDIF 
			
		
		 	
		 
		 
		 if(!EMPTY(lcExterno))
			 SELECT ucrsAtendCl
			 GO TOP
			 IF !EMPTY(ALLTRIM(uCrsAtendCl.nbenef))			 
				 LOCAL lcToken, lcChave, lcChaveRel, lcNrSnS, lcSexo, lcNasc, lcDataDisp 
				 
				 lcSexo = ''
				 lcNasc ='1900-01-01'
		  
		  		 lcToken =  uf_gerais_stamp()
		  		 lcChave= ALLTRIM(lcToken)
		  		 lcChaveRel= "Valida-" + ALLTRIM(lcToken)
		  		 lcNrSnS = ALLTRIM(ucrsatendcl.nbenef)
		  		 lcSexo  = ALLTRIM(ucrsatendcl.sexo)
		  		 lcNasc  = uf_gerais_getdate(ucrsatendcl.nascimento)
		  		 
		  		 LOCAL lcTipoOp
				 lcTipoOp = 'VALIDACAO_TRAG'
				 
				  if(USED("uCrsAtendComp"))
		  		 	SELECT uCrsAtendComp
		  		 	GO TOP
		  		 	lcDataDisp = uf_gerais_getdate(uCrsAtendComp.datadispensa)
		  		 ENDIF	 	
	 
		  		 lcsql = ''
			     TEXT TO lcsql TEXTMERGE NOSHOW
						INSERT INTO [dbo].[Dispensa_Eletronica]
				           ([token]
				           ,[chave_pedido]
				           ,[chave_pedido_relacionado]
				           ,[receita_tipo]
				           ,[beneficiario_nr]
				           ,[receita_nr]
				           ,utente_nascimento
				           ,local_presc_cod
				           ,dispensa_data			           
				           )
				     VALUES
				           (
				           '<<ALLTRIM(lcToken)>>'
				           ,'<<ALLTRIM(lcChave)>>'
				           ,'<<ALLTRIM(lcChaveRel)>>'
						   ,'EXT'
						   ,'<<ALLTRIM(lcNrSnS)>>'
						   ,''
						   ,'<<lcNasc>>'
						   ,'<<ALLTRIM(lcSexo)>>'
						   ,'<<lcDataDisp>>' 
						   
						)	
	       			ENDTEXT
			       IF  !uf_gerais_actgrelha("", "", lcsql)
			          uf_perguntalt_chama("N�o foi possivel proceder ao servi�o de Valida��o. Contacte o suporte.", "OK", "", 32)
			          RETURN .F.
			       ENDIF          
	  
				   lcValida = uf_atendimento_receitasElectronicaSNSExterno(lcTipoOp,"A Validar SNS: "+ALLTRIM(lcNrSnS)+" no Sistema Central de Prescri��es...", lcToken,"") 
				   regua(2)
				   
				   if(!USED("ucrsDadosExternoDEM"))
				   		uf_perguntalt_chama("N�o foi possivel proceder ao servi�o de Valida��o do numero do SNS. Contacte o suporte.", "OK", "", 64)
				   		if(USED("ucrsDadosExternoDEM"))
							fecha("ucrsDadosExternoDEM")
				   		ENDIF	
				   		RETURN .F.			   
				   ENDIF
				   
				   
				  if(RECCOUNT("ucrsDadosExternoDEM")==0)
				   		uf_perguntalt_chama("N�o foi possivel proceder ao servi�o de Valida��o do numero do SNS. Contacte o suporte.", "OK", "", 64)
				   		if(USED("ucrsDadosExternoDEM"))
							fecha("ucrsDadosExternoDEM")
				   		ENDIF	
				   		RETURN .F.			   
				   ENDIF
					
					
				   LOCAL lcResp, lcCodResp
				   lcResp = 'N�o foi possivel proceder ao servi�o de Valida��o do numero do SNS. Contacte o suporte.'
				   lcCodResp = ''
				   
				   SELECT ucrsDadosExternoDEM
				   GO TOP
				   if(!EMPTY(ucrsDadosExternoDEM.resultado_validacao_descr))
				   		lcResp = ALLTRIM(ucrsDadosExternoDEM.resultado_validacao_descr)
				   		lcCodResp = ALLTRIM(ucrsDadosExternoDEM.resultado_validacao_cod)
				   ENDIF
				   
				    
				   uf_perguntalt_chama(lcResp , "OK", "", 32) 
				 
				   
					IF TYPE("lcCodResp")!="U"			   
						IF ALLTRIM(lcCodResp)='100004030001'
						 	myChamaSinave = 'Com Comparticipa��o'
						 ELSE
						 	myChamaSinave = 'Sem Comparticipa��o'
						ENDIF 
					ELSE
						myChamaSinave = 'N�o chama'
					ENDIF  
				   
		
				   
				   if(USED("ucrsDadosExternoDEM"))
						fecha("ucrsDadosExternoDEM")
				   ENDIF      
	    	 ENDIF
	    	 
    	 ENDIF
	 
	ENDIF 

ENDFUNC




FUNCTION uf_atendimento_criaCursorSNSExterno
    LPARAMETERS lcRef

	if(!USED("ucrsatendcomp"))
		RETURN .f.
	ENDIF
	
	if(!USED("fi"))
		RETURN .f.
	ENDIF
	
	LOCAL lcPos, lcPosFi, lcStamp, lcRef
	
	
	lcPos= RECNO("ucrsatendcomp")
	lcPosFi= RECNO("fi")
	lcStamp = uf_gerais_stamp()
	

	
	if(USED("ucrsSNSExternoAtend"))
	  fecha("ucrsSNSExternoAtend")
	ENDIF
	
	
	
	
	SELECT distinct ucrsatendcomp.fistamp, 'EXT' as tipoReceita, uf_gerais_stamp() as token, fi.design as design, fi.ftstamp as ftstamp, CAST(0 AS Int)  AS qtdisp, LEFT(ALLTRIM(STR(fi.lordem)), 2) as lOrdem,;
    '100003010001' as resultado_consulta_cod, 'Pedido processado com sucesso.' as resultado_consulta_descr,;
    CAST("" AS C(12) ) as resultado_validacao_cod, CAST("" AS C(200) )  as resultado_validacao_descr, CAST("" AS C(12) ) as resultado_efetivacao_cod, CAST("" AS C(200) ) as resultado_efetivacao_descr,;
  	CAST("" AS C(254)) as resultado_comprovativo_registo, CAST("" AS C(12) ) as resultado_anulacao_cod, CAST("" AS C(200) ) as resultado_anulacao_descr, ALLTRIM(fi.token) as fiToken;
    FROM ucrsatendcomp INNER JOIN fi ON ucrsatendcomp.fistamp=fi.fistamp;
    WHERE ucrsatendcomp.snsExterno==.t. AND fi.partes=0;
    and fi.ftstamp in (select ftstamp from fi where fi.ref=ALLTRIM(lcRef)) INTO CURSOR ucrsSNSExternoAtend READWRITE


	
	if(USED("ucrsSNSExternoAtend"))
		
		if(RECCOUNT("ucrsSNSExternoAtend")>0)
		
			UPDATE  ucrsSNSExternoAtend  SET ucrsSNSExternoAtend.qtdisp=NVL((select sum(qtt) from fi where LEFT(ALLTRIM(str(lordem)),2)=ucrsSNSExternoAtend.lordem and  ref=ALLTRIM(lcRef) AND qtt>0 AND fi.partes=0),0)
			
			
		ENDIF
			
			
	ENDIF
	


	
	&&valida se devolve	ou se regulariza
	DELETE FROM ucrsSNSExternoAtend WHERE ALLTRIM(ucrsSNSExternoAtend.fiToken) in (select ALLTRIM(token) FROM fi WHERE (fi.partes=1) AND !EMPTY(ALLTRIM(token)))
	
	



	SELECT  ucrsatendcomp
	TRY
    GOTO lcPos
    CATCH
    	GOTO TOP
    ENDTRY
    
    
    SELECT  fi
    TRY
    GOTO lcPosFi
    CATCH
    	GOTO TOP
    ENDTRY
    
    if(USED("ucrsSNSExternoAtend"))
	    SELECT ucrsSNSExternoAtend
	    GO TOP 
	    SCAN
	    	IF(ucrsSNSExternoAtend.qtdisp<=0) && nada a efectivar
	    		RETURN .f.
	    	ENDIF
	    	
	    ENDSCAN
	ENDIF
	    
    
	
	RETURN .t.


ENDFUNC

FUNCTION uf_atendimento_aplicaTabCompart
    LPARAMETERS uv_plano, uv_ref
    

	IF EMPTY(uv_plano) OR EMPTY(uv_ref) OR !USED("fi")
		RETURN .F.
	ENDIF

    IF uf_gerais_getUmValor("cptpla","e_compart", "codigo = '" + ALLTRIM(uv_plano) + "'")
        RETURN .F.
    ENDIF

	uv_hasPlanoManual = .F.

	IF !EMPTY(uv_plano)
		IF uf_gerais_getUmValor("INFORMATION_SCHEMA.TABLES", "count(*)", "TABLE_NAME = 'cptvalManual'") <> 0
			IF uf_gerais_getUmValor("cptvalManual", "count(*)", "codigo = '" + ALLTRIM(uv_plano) + "'") <> 0
				uv_hasPlanoManual = .T.
			ENDIF
		ENDIF
	ENDIF

	msel = ''

	IF uv_hasPlanoManual
		
		TEXT TO msel TEXTMERGE NOSHOW

			SELECT ref, compart, compart  as edebito, grppreco   FROM cptvalManual(nolock) WHERE codigo = '<<ALLTRIM(uv_plano)>>' and ref = '<<ALLTRIM(uv_ref)>>'

		ENDTEXT

	ELSE

		uv_entidade = uf_gerais_getUmValor("cptorg", "u_no", "cptorgStamp = (select top 1 cptorgStamp from cptpla(nolock) where codigo = '" + ALLTRIM(uv_plano) + "')")

		uv_bostamp = uf_gerais_getUmValor("bo", "ISNULL(bostamp, '')", "ndos = 47 and no = " + ASTR(uv_entidade), "ousrdata desc, ousrhora desc")


    	IF EMPTY(uv_bostamp)
        	RETURN .F.
    	ENDIF

    	TEXT TO msel TEXTMERGE NOSHOW

        	SELECT ref, edebito, (CASE WHEN ISNULL((select bool from B_parameters(nolock) where stamp = 'ADM0000000344'), 0) = 0 then 'val' ELSE 'pvp'  END) as grppreco FROM bi(nolock) WHERE bi.bostamp = '<<ALLTRIM(uv_bostamp)>>' and bi.ref = '<<ALLTRIM(uv_ref)>>'

	    ENDTEXT
	
	ENDIF

	IF EMPTY(msel)
		RETURN .F.
	ENDIF

    IF !uf_gerais_actGrelha("", "uc_comparts", msel)
        RETURN .F.
    ENDIF

    IF RECCOUNT("uc_comparts") = 0
        RETURN .F.
    ENDIF


	SELECT uc_comparts
	GO TOP

	LOCATE FOR ALLTRIM(UPPER(uc_comparts.ref)) = ALLTRIM(UPPER(uv_ref))

	IF FOUND()

		DO CASE 
			
	
			
			&&compart em valor sobre o pvp
			CASE ALLTRIM(UPPER(uc_comparts.grppreco)) = 'VAL'
			
	

				lcfietiliquido = fi.qtt*(fi.u_epvp - (ROUND(fi.u_epvp*(fi.desconto/100), 2)))
				lcvalcomp = uc_comparts.edebito
				
	

				uv_etiliquido = IIF((lcfietiliquido-lcvalcomp) < 0, 0,  (lcfietiliquido-lcvalcomp))


				
				
				LOCAL oldEtiliquido 
				oldEtiliquido = fi.etiliquido

				REPLACE fi.etiliquido WITH uv_etiliquido
				
				
             
				REPLACE fi.pv WITH IIF(fi.qtt <> 0 ,ROUND(uv_etiliquido/fi.qtt, 2), 0)
				REPLACE fi.pvmoeda WITH IIF(FI.QTT <> 0, ROUND(uv_etiliquido/fi.qtt, 2), 0)
				REPLACE fi.epv WITH IIF(fi.qtt <> 0, ROUND(uv_etiliquido/fi.qtt, 2), 0)
				REPLACE fi.u_ettent1 WITH IIF(fi.u_epvp < lcvalcomp, fi.u_epvp, lcvalcomp)
				REPLACE fi.u_comp WITH .T.
				REPLACE fi.u_txcomp WITH 100-IIF(lcfietiliquido <> 0, ROUND(((lcvalcomp*100)/lcfietiliquido), 2), 0)
				REPLACE fi.comp WITH IIF(lcfietiliquido <> 0, ROUND(((lcvalcomp*100)/lcfietiliquido), 2), 0)
				REPLACE fi.comp_diploma WITH IIF(lcfietiliquido <> 0, ROUND(((lcvalcomp*100)/lcfietiliquido), 2), 0)	
				
				if(USED("fimancompart"))
					SELECT fimancompart 
					UPDATE fimancompart SET valor=fi.u_ettent1 , valortot =oldEtiliquido  WHERE fimancompart.fistamp=fi.fistamp	
				ENDIF
				
			
			&&compart por percentagem do pvp
			CASE ALLTRIM(UPPER(uc_comparts.grppreco)) = 'PVP'

				lcfietiliquido = fi.qtt*(fi.u_epvp - (ROUND(fi.u_epvp*(fi.desconto/100), 2)))
				lcvalcomp = ROUND(((uc_comparts.edebito*lcfietiliquido)/100),2)

				uv_etiliquido = ROUND(IIF((lcfietiliquido-lcvalcomp) < 0, 0,  (lcfietiliquido-lcvalcomp)),2)
				
				LOCAL oldEtiliquido 
				oldEtiliquido = fi.etiliquido

				REPLACE fi.etiliquido WITH uv_etiliquido
             
				REPLACE fi.pv WITH IIF(fi.qtt <> 0 ,ROUND(uv_etiliquido/fi.qtt, 2), 0)
				REPLACE fi.pvmoeda WITH IIF(FI.QTT <> 0, ROUND(uv_etiliquido/fi.qtt, 2), 0)
				REPLACE fi.epv WITH IIF(fi.qtt <> 0, ROUND(uv_etiliquido/fi.qtt, 2), 0)
				REPLACE fi.u_ettent1 WITH IIF(fi.u_epvp < lcvalcomp, fi.u_epvp, lcvalcomp)
				REPLACE fi.u_comp WITH .T.
				REPLACE fi.u_txcomp WITH 100 - uc_comparts.edebito
				REPLACE fi.comp WITH uc_comparts.edebito
				REPLACE fi.comp_diploma WITH uc_comparts.edebito
				
				if(USED("fimancompart"))
					SELECT fimancompart 
					UPDATE fimancompart SET perc=uc_comparts.edebito,valortot =oldEtiliquido  WHERE fimancompart.fistamp=fi.fistamp
				ENDIF
				

		ENDCASE
		
		
		
		
		uf_atendimento_fiiliq()
		uf_atendimento_infototais()
		



   ELSE

        RETURN .F.

	ENDIF


    FECHA("uc_comparts")

    uf_atendimento_fiiliq()
	uf_atendimento_infototais()


	RETURN .T.

ENDFUNC


FUNCTION uf_atendimento_associa_cartao
	
	
	IF ft.no=200
		uf_perguntalt_chama("N�O PODE ASSOCIAR CART�O AO CLIENTE 200.","OK","",16)
		RETURN .f.
	ENDIF 

	LOCAL lcTpClsemcartao
	lcTpClsemcartao = uf_gerais_getParameter('ADM0000000329', 'TEXT')
	IF ALLTRIM(lcTpClsemcartao)=''
		IF AT(ALLTRIM(uCrsAtendCl.tipo), ALLTRIM(lcTpClsemcartao))<>0
			uf_perguntalt_chama("N�O PODE ASSOCIAR CART�O A CLIENTES DO TIPO " + ALLTRIM(uCrsAtendCl.tipo) + ".","OK","",16)
			RETURN .f.
		ENDIF 
	ENDIF 
	
	IF uf_gerais_getParameter_site('ADM0000000088', 'BOOL', mySite) AND !uf_gerais_chkconexao()
		uf_perguntalt_chama("N�O EXISTE LIGA��O � BD CENTRAL. A OPERA��O N�O PODE SER EFETUADA.","OK","",16)
		RETURN .f.
	ELSE 
		IF EMPTY(uCrsAtendCL.nrcartao)
			PUBLIC nrcartaout
			STORE '' TO nrcartaout
			uf_tecladoAlpha_Chama("nrcartaout","INTRODUZA O N� DO CART�O PARA ASSOCIAR AO CLIENTE:",.t.,.f.,0)
			IF UPPER(ALLTRIM(SUBSTR(nrcartaout,1,2)))!='CL'
				uf_perguntalt_chama("A IDENTIFICA��O DO CART�O TEM DE COME�AR OBRIGAT�RIAMENTE POR 'CL'. POR FAVOR VERIFIQUE","OK","",16)
				RETURN .f.
			ENDIF 
			
			LOCAL lcnrcartaocd, lcChkdigit, lcSQL, uv_curDT, uv_createCartao
    		lcnrcartaocd = ALLTRIM(SUBSTR(nrcartaout,3,15))
    		lcChkdigit = uf_novonordisk_chkdigit(lcnrcartaocd,'Valida')
    		IF lcChkdigit != VAL(RIGHT(ALLTRIM(lcnrcartaocd),1))
    			uf_perguntalt_chama("N� de cart�o inv�lido! Por favor verifique","OK","",64)
    			RETURN .f.
    		ENDIF 
			
			** verifica se j� est� associado a algum cliente
			lcSQL = ''
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				select * from b_fidel where nrcartao='<<ALLTRIM(nrcartaout)>>'
			ENDTEXT 

			IF !(uf_gerais_actGrelha("","uCrscartaorepetido",lcSQL))
				uf_perguntalt_chama("Ocorreu uma anomalida a verificar os dados do Cart�o do utente. Por favor contacte o Suporte.","OK","",16)
				RETURN .f.
			ENDIF
			IF RECCOUNT("uCrscartaorepetido")>0 
				
				uf_perguntalt_chama("Esse cart�o j� se encontra atr�buido ao cliente n� " + ALLTRIM(STR(uCrscartaorepetido.clno)),"OK","",16)
				RETURN .f.
				
			ELSE
				
				replace uCrsAtendCl.nrcartao WITH ALLTRIM(nrcartaout)
				atendimento.pgfDados.Page1.txtCliCartao.refresh
				
				uv_createCartao = ''

				uv_fStsamp = uf_gerais_stamp()
            SET CENTURY ON
            uv_curDT = DATETIME()

				TEXT TO uv_createCartao NOSHOW TEXTMERGE 
					update b_utentes set nrcartao='<<ALLTRIM(nrcartaout)>>' where no=<<ft.no>> and estab=<<ft.estab>>
					
					insert into B_fidel (fstamp, clstamp, clno, clestab, nrcartao, validade, ousrdata, usrdata)
					select '<<ALLTRIM(uv_fStsamp)>>', '<<ALLTRIM(uCrsAtendCL.utstamp)>>', <<ft.no>>, <<ft.estab>>, '<<ALLTRIM(nrcartaout)>>', '30001231', '<<uv_curDT>>', '<<uv_curDT>>'
				ENDTEXT 

				IF uf_gerais_getParameter_site('ADM0000000133', 'BOOL', mySite)

               IF !uf_gerais_runSQLCentral(uv_createCartao)
   				   uf_perguntalt_chama("Erro a criar cart�o na Central.","OK","",16)
   				   RETURN .F.
               ENDIF

				ENDIF

				IF !(uf_gerais_actGrelha("","",uv_createCartao))
					uf_perguntalt_chama("Ocorreu uma anomalida a verificar os dados do Utente. Por favor contacte o Suporte.","OK","",16)
					RETURN .f.
				ENDIF
				
				SELECT ucrsatendcl
				replace uCrsAtendCL.nrcartao WITH ALLTRIM(nrcartaout)
				
				atendimento.refresh	
				
			ENDIF 
			uf_campanhas_actualizaCampanhasNovoUtente(ft.no, ft.estab)
			uf_perguntalt_chama("Cart�o associado com sucesso.","OK","",64)
		ENDIF 
	ENDIF 
	
ENDFUNC 

FUNCTION uf_atendimento_validaIdade()

    SELECT FI

    uv_curstamp = fi.fistamp

	IF uf_gerais_getParameter("ADM0000000347", "BOOL")

	    SELECT uCrsAtendComp
    	LOCATE FOR ALLTRIM(uCrsAtendComp.fistamp) = ALLTRIM(uv_curstamp)

	    IF FOUND()
    	    IF INLIST(UPPER(ALLTRIM(uCrsAtendComp.codigo)), 'AR', 'RM') AND INLIST(uCrsAtendComp.entidade, 109, 110)
        	    IF uf_perguntalt_chama("Consulta de declara��o para vacina��o contra a gripe:" + chr(13) + chr(13) + ALLTRIM(uf_gerais_getParameter("ADM0000000347", "TEXT")),"Sim","N�o",64)
            	    RETURN 1
	            ELSE
    	        	RETURN 0
        	    ENDIF
	        ENDIF
    	ENDIF
    	
    ENDIF

    RETURN 0
ENDFUNC 

FUNCTION uf_atendimento_valida_artigos_controlados
	LOCAL lcContemControlados, lcValidou
	STORE .f. TO lcContemControlados
	STORE .t. TO lcValidou
	SELECT ucrsatendst
	GO TOP 
	SCAN 
		IF ALLTRIM(ucrsatendst.usr3)='04'
			lcContemControlados=.t.
		ENDIF 
	ENDSCAN 
	IF lcContemControlados = .t.
		LOCAL lcMsgErros
		STORE '' TO lcMsgErros
		IF len(alltrim(atendimento.pgfDados.Page3.txtnomemedico.text))=0
			lcMsgErros = lcMsgErros + 'Nome do M�dico' + CHR(13)
			lcValidou = .f.
		ENDIF 
		IF len(alltrim(atendimento.pgfDados.Page3.txtlocalprescricao.text))=0
			lcMsgErros = lcMsgErros + 'Local de prescri��o' + CHR(13)
			lcValidou = .f.
		ENDIF 
		IF len(alltrim(atendimento.pgfDados.Page3.txtnomeutente.text))=0
			lcMsgErros = lcMsgErros + 'Nome do Utente' + CHR(13)
			lcValidou = .f.
		ENDIF 
		IF len(alltrim(atendimento.pgfDados.Page3.txtcontactoutente.text))=0
			lcMsgErros = lcMsgErros + 'Contacto do Utente' + CHR(13)
			lcValidou = .f.
		ENDIF 
	ENDIF 
	
	IF lcValidou = .f.
		uf_perguntalt_chama("EXISTEM MEDICAMENTOS CONTROLADOS NO ATENDIMENTO E FALTAM PREENCHER OS SEGUINTES CAMPOS: " + CHR(13) + lcMsgErros , "OK", "", 16)
	ENDIF 
	
	RETURN lcValidou
ENDFUNC 


FUNCTION uf_atendimento_aplicaTabCompart_BI
   LPARAMETERS uv_plano, uv_ref

	IF EMPTY(uv_plano) OR EMPTY(uv_ref) OR !USED("fi")
		RETURN .F.
	ENDIF

   IF uf_gerais_getUmValor("cptpla","e_compart", "codigo = '" + ALLTRIM(uv_plano) + "'")
      RETURN .F.
   ENDIF

   LOCAL uv_entidade, uv_bostamp, uv_descSeg, uv_curFistamp

   uv_entidade = uf_gerais_getUmValor("cptorg", "u_no", "cptorgStamp = (select top 1 cptorgStamp from cptpla(nolock) where codigo = '" + ALLTRIM(uv_plano) + "')")
   uv_bostamp = uf_gerais_getUmValor("bo", "ISNULL(bostamp, '')", "ndos = 47 and no = " + ASTR(uv_entidade), "ousrdata desc, ousrhora desc")

   IF EMPTY(uv_bostamp)
      RETURN .F.
   ENDIF

   TEXT TO msel TEXTMERGE NOSHOW
      SELECT ref, edebito, (CASE WHEN ISNULL((select bool from B_parameters(nolock) where stamp = 'ADM0000000344'), 0) = 0 then 'val' ELSE 'pvp'  END) as grppreco FROM bi(nolock) WHERE bi.bostamp = '<<ALLTRIM(uv_bostamp)>>' and bi.ref = '<<ALLTRIM(uv_ref)>>'
   ENDTEXT
   
   IF !uf_gerais_actGrelha("", "uc_comparts", msel)
      RETURN .F.
   ENDIF

   IF RECCOUNT("uc_comparts") = 0

      TEXT TO msel TEXTMERGE NOSHOW
         SELECT ref, edebito, (CASE WHEN ISNULL((select bool from B_parameters(nolock) where stamp = 'ADM0000000344'), 0) = 0 then 'val' ELSE 'pvp'  END) as grppreco FROM bi(nolock) WHERE bi.bostamp = '<<ALLTRIM(uv_bostamp)>>' and bi.usr2 = ( select familia from st(nolock) where st.ref = '<<ALLTRIM(uv_ref)>>' and site_nr = <<mySite_nr>>)
      ENDTEXT
      
      IF !uf_gerais_actGrelha("", "uc_comparts", msel)
         RETURN .F.
      ENDIF

   ENDIF

   IF RECCOUNT("uc_comparts") = 0
      RETURN .F.
   ENDIF

	DO CASE
	 
		&& compart por percentagem do pvp			
		CASE ALLTRIM(UPPER(uc_comparts.grppreco)) = 'PVP'
			lcfietiliquido = fi.qtt*(fi.u_epvp - (ROUND(fi.u_epvp*(fi.desconto/100), 2)))
			lcvalcomp = ROUND(((uc_comparts.edebito*lcfietiliquido)/100),2)

			uv_etiliquido = ROUND(IIF((lcfietiliquido-lcvalcomp) < 0, 0,  (lcfietiliquido-lcvalcomp)),2)
			
			LOCAL oldEtiliquido 
			oldEtiliquido = fi.etiliquido

         SELECT fi

         uv_curFistamp = fi.fistamp

         REPLACE fi.etiliquido WITH lcfietiliquido-lcvalcomp
          
         REPLACE fi.pv WITH ROUND((lcfietiliquido-lcvalcomp)/fi.qtt, 2)
         REPLACE fi.pvmoeda WITH ROUND((lcfietiliquido-lcvalcomp)/fi.qtt, 2)
         REPLACE fi.epv WITH ROUND((lcfietiliquido-lcvalcomp)/fi.qtt, 2)
         REPLACE fi.u_ettent1 WITH lcvalcomp
         REPLACE fi.u_comp WITH .T.
         REPLACE fi.u_txcomp WITH 100-ROUND(((lcvalcomp*100)/lcfietiliquido), 2)
         REPLACE fi.comp WITH ROUND(((lcvalcomp*100)/lcfietiliquido), 2)
         REPLACE fi.comp_diploma WITH ROUND(((lcvalcomp*100)/lcfietiliquido), 2)

         SELECT fi
         uf_atendimento_copyLinFimancompart(0, fi.fistamp, uv_plano)
			
			UPDATE fimancompart SET perc=uc_comparts.edebito, valortot =oldEtiliquido WHERE fimancompart.fistamp=fi.fistamp
		
			uf_atendimento_fiiliq()
			uf_atendimento_infototais()

      CASE ALLTRIM(UPPER(uc_comparts.grppreco)) = 'VAL'

         lcfietiliquido = fi.qtt*(fi.u_epvp - (ROUND(fi.u_epvp*(fi.desconto/100), 2)))
			lcvalcomp = ROUND(uc_comparts.edebito,2)

			uv_etiliquido = ROUND(IIF((lcfietiliquido-lcvalcomp) < 0, 0,  (lcfietiliquido-lcvalcomp)),2)
			
			LOCAL oldEtiliquido 
			oldEtiliquido = fi.etiliquido

         SELECT fi

         uv_curFistamp = fi.fistamp
	
         REPLACE fi.etiliquido WITH lcfietiliquido-lcvalcomp
          
         REPLACE fi.pv WITH ROUND((lcfietiliquido-lcvalcomp)/fi.qtt, 2)
         REPLACE fi.pvmoeda WITH ROUND((lcfietiliquido-lcvalcomp)/fi.qtt, 2)
         REPLACE fi.epv WITH ROUND((lcfietiliquido-lcvalcomp)/fi.qtt, 2)
         REPLACE fi.u_ettent1 WITH lcvalcomp
         REPLACE fi.u_comp WITH .T.
         REPLACE fi.u_txcomp WITH 100-ROUND(((lcvalcomp*100)/lcfietiliquido), 2)
         REPLACE fi.comp WITH ROUND(((lcvalcomp*100)/lcfietiliquido), 2)
         REPLACE fi.comp_diploma WITH ROUND(((lcvalcomp*100)/lcfietiliquido), 2)

         SELECT fi
         uf_atendimento_copyLinFimancompart(0, fi.fistamp, uv_plano)
		
			UPDATE fimancompart SET perc=uc_comparts.edebito, valortot =oldEtiliquido WHERE fimancompart.fistamp=fi.fistamp
			
			uf_atendimento_fiiliq()
			uf_atendimento_infototais()
	
			
	ENDCASE

   SELECT ucrsatendcomp
   GO TOP

   LOCATE FOR ucrsatendcomp.codigo = uv_plano

   IF FOUND()

      uv_descSeg = ucrsatendcomp.descHabitual

   ENDIF

   IF !EMPTY(uv_descSeg)

      SELECT fi2
      GO TOP

      LOCATE FOR fi2.fistamp = uv_curFistamp

      IF FOUND()

         REPLACE fi2.descontoHabitual WITH uv_descSeg

      ENDIF

   ENDIF

	RETURN .T.

ENDFUNC


FUNCTION uf_atendimento_compart_nnordisk

	LOCAL lcdesc, lctxcomp, lccomp
	IF !uf_gerais_getparameter('ADM0000000349', 'BOOL')
		lcdesc = 0.87
		lctxcomp = 87
		lccomp = 13
	ELSE
		DO CASE 
			CASE nrembalnnordisk = 1
				lcdesc = 0.80
	    		lctxcomp = 80
				lccomp = 20
			CASE nrembalnnordisk = 2
				lcdesc = 0.70
	    		lctxcomp = 70
				lccomp = 30
			CASE nrembalnnordisk = 3
				lcdesc = 0.60
	    		lctxcomp = 60
				lccomp = 40
			CASE nrembalnnordisk >= 4
				lcdesc = 0.554
	    		lctxcomp = 55.4
				lccomp = 44.6
			 OTHERWISE 
			
		ENDCASE 
	ENDIF 
	IF nrembalnnordisk > 0
	   SELECT fi
	   REPLACE fi.tiliquido WITH ROUND(fi.u_epvp*lcdesc , 2)
	   REPLACE fi.etiliquido WITH ROUND(fi.u_epvp*lcdesc , 2)
       
	   REPLACE fi.pv WITH ROUND(fi.u_epvp*lcdesc , 2)
	   REPLACE fi.pvmoeda WITH ROUND(fi.u_epvp*lcdesc , 2)
	   REPLACE fi.epv WITH ROUND(fi.u_epvp*lcdesc , 2)
	   REPLACE fi.u_ettent1 WITH ROUND(fi.u_epvp-(fi.u_epvp*lcdesc ), 2)
	   REPLACE fi.u_txcomp WITH lctxcomp
	   REPLACE fi.comp_tipo WITH 2
	   REPLACE fi.comp WITH lccomp
	   REPLACE fi.u_comp WITH .T.
	ENDIF 
	
ENDFUNC 



FUNCTION uf_atendimento_copyLinFimancompart
    LPARAMETERS uv_lordem, uv_curFistamp, uv_plano


   IF EMPTY(uv_lordem) AND EMPTY(uv_curFistamp)
      RETURN .F.
   ENDIF

   LOCAL uv_fistamp, uv_descSeg
   STORE 0 TO uv_descSeg

   SELECT fi

   IF !EMPTY(uv_plano)

      SELECT ucrsatendcomp
      GO TOP

      LOCATE FOR uf_gerais_compStr(ucrsatendcomp.codigo, uv_plano)

      IF FOUND()
         uv_descSeg = UCRSATENDCOMP.descHabitual
      ENDIF

   ELSE
      uv_fistamp = fi.fistamp
   
      SELECT UCRSATENDCOMP
      LOCATE FOR UCRSATENDCOMP.fistamp = uv_fistamp

      IF FOUND()
         uv_descSeg = UCRSATENDCOMP.descHabitual
      ENDIF

   ENDIF
	
	fecha("uc_tmpFI")

   IF EMPTY(uv_curFistamp)

      SELECT FI
      SELECT * FROM FI WITH (BUFFERING = .T.) WHERE fi.LORDEM > uv_lordem order by  fi.LORDEM  ASC INTO CURSOR uc_tmpFI 
   
   ELSE

      SELECT FI
      SELECT * FROM FI WITH (BUFFERING = .T.) WHERE uf_gerais_compStr(fi.fistamp, uv_curFistamp) INTO CURSOR uc_tmpFI 

   ENDIF

   SELECT uc_tmpFI
   GO TOP

   SCAN

      IF LEFT(ALLTRIM(uc_tmpFI.design),1) = "."
         EXIT
      ENDIF

      SELECT fimancompart
      GO TOP

      LOCATE FOR ALLTRIM(fimancompart.fistamp) == ALLTRIM(uc_tmpFI.fistamp)

      IF !FOUND()

         SELECT fimancompart
         APPEND BLANK

         REPLACE fimancompart.ref WITH ALLTRIM(uc_tmpFI.ref),;
                  fimancompart.design WITH ALLTRIM(uc_tmpFI.design),;
                  fimancompart.fistamp WITH ALLTRIM(uc_tmpFI.fistamp),;
                  fimancompart.valortot WITH uc_tmpFI.tiliquido,;
                  fimancompart.lordem WITH uc_tmpFI.lordem,;
                  fimancompart.perc WITH uc_tmpFI.comp,;
                  fimancompart.valor WITH uc_tmpFI.u_ettent1,;
                  fimancompart.descSeg WITH uv_descSeg

         **IF (uc_tmpFI.desconto>0) .OR. (uc_tmpFI.desc2>0) .OR. (uc_tmpFI.u_descval>0)
         **   IF (uc_tmpFI.desconto>0)
         **      REPLACE fimancompart.totLin WITH ROUND(uc_tmpFI.epvori*uc_tmpFI.qtt-(uc_tmpFI.epvori*uc_tmpFI.qtt*(uc_tmpFI.desconto/100)), 2)
         **   ENDIF
         **   IF (uc_tmpFI.desc2>0)
         **      IF (uc_tmpFI.desconto=0)
         **         REPLACE fimancompart.totLin WITH ROUND((uc_tmpFI.epvori*uc_tmpFI.qtt)-((uc_tmpFI.epvori*uc_tmpFI.qtt)*(uc_tmpFI.desc2/100)), 2)
         **      ELSE
         **         REPLACE fimancompart.totLin WITH (uc_tmpFI.epvori*uc_tmpFI.qtt)-((uc_tmpFI.epvori*uc_tmpFI.qtt)*(uc_tmpFI.desc2/100))
         **      ENDIF
         **   ENDIF
         **   IF (uc_tmpFI.u_descval>0)
         **      IF uc_tmpFI.desconto==0 .AND. uc_tmpFI.desc2==0
         **         REPLACE fimancompart.totLin WITH ROUND(uc_tmpFI.epvori*uc_tmpFI.qtt-(uc_tmpFI.u_descval), 2)
         **      ELSE
         **         REPLACE fimancompart.totLin WITH (uc_tmpFI.epvori*uc_tmpFI.qtt)-(uc_tmpFI.u_descval)
         **      ENDIF
         **   ENDIF
**
         **ELSE
**
         **   REPLACE fimancompart.totLin WITH ROUND(uc_tmpFI.epvori*uc_tmpFI.qtt, 2)
**
         **ENDIF

         REPLACE fimancompart.totLin WITH ROUND(uc_tmpFI.epvori*uc_tmpFI.qtt,2)
         REPLACE fimancompart.totLinDescSeg WITH ROUND(fimancompart.totLin - ((fimancompart.totLin*uv_descSeg)/100), 2)

      ELSE

         SELECT fimancompart

         REPLACE fimancompart.valortot WITH uc_tmpFI.tiliquido,;
                  fimancompart.lordem WITH uc_tmpFI.lordem,;
                  fimancompart.descSeg WITH uv_descSeg

         **IF (uc_tmpFI.desconto>0) .OR. (uc_tmpFI.desc2>0) .OR. (uc_tmpFI.u_descval>0)
         **   IF (uc_tmpFI.desconto>0)
         **      REPLACE fimancompart.totLin WITH ROUND(uc_tmpFI.epvori*uc_tmpFI.qtt-(uc_tmpFI.epvori*uc_tmpFI.qtt*(uc_tmpFI.desconto/100)), 2)
         **   ENDIF
         **   IF (uc_tmpFI.desc2>0)
         **      IF (uc_tmpFI.desconto=0)
         **         REPLACE fimancompart.totLin WITH ROUND((uc_tmpFI.epvori*uc_tmpFI.qtt)-((uc_tmpFI.epvori*uc_tmpFI.qtt)*(uc_tmpFI.desc2/100)), 2)
         **      ELSE
         **         REPLACE fimancompart.totLin WITH (uc_tmpFI.epvori*uc_tmpFI.qtt)-((uc_tmpFI.epvori*uc_tmpFI.qtt)*(uc_tmpFI.desc2/100))
         **      ENDIF
         **   ENDIF
         **   IF (uc_tmpFI.u_descval>0)
         **      IF uc_tmpFI.desconto==0 .AND. uc_tmpFI.desc2==0
         **         REPLACE fimancompart.totLin WITH ROUND(uc_tmpFI.epvori*uc_tmpFI.qtt-(uc_tmpFI.u_descval), 2)
         **      ELSE
         **         REPLACE fimancompart.totLin WITH (uc_tmpFI.epvori*uc_tmpFI.qtt)-(uc_tmpFI.u_descval)
         **      ENDIF
         **   ENDIF
**
         **ELSE
**
         **   REPLACE fimancompart.totLin WITH ROUND(uc_tmpFI.epvori*uc_tmpFI.qtt, 2)
**
         **ENDIF

         REPLACE fimancompart.totLin WITH ROUND(uc_tmpFI.epvori*uc_tmpFI.qtt, 2)
         REPLACE fimancompart.totLinDescSeg WITH ROUND(fimancompart.totLin - ((fimancompart.totLin*uv_descSeg)/100), 2)

      ENDIF

   ENDSCAN

ENDFUNC

FUNCTION uf_atendimento_devolveNumeroVendasAtendimento
	
	
	LOCAL lcPosFi, lcConta	
	lcPosFi= RECNO("fi")
	lcConta = 0

	fecha("uc_tmpFI")
    SELECT * FROM FI WITH (BUFFERING = .T.)   INTO CURSOR uc_tmpFI 

    SELECT uc_tmpFI
    GO TOP
    SCAN

        IF LEFT(ALLTRIM(uc_tmpFI.design),1) = "."
            lcConta=lcConta + 1
        ENDIF

    ENDSCAN
    
    fecha("uc_tmpFI")
    
    SELECT  fi
    TRY
    GOTO lcPosFi
    CATCH
    	GOTO TOP
    ENDTRY
	
	
	RETURN  lcConta


ENDFUNC

FUNCTION uf_atendimento_escolhePlanoND

	LOCAL  lcPlan
	lcPlan= ''
	
	fecha("uc_planoND")
	
	IF !uf_gerais_actGrelha("","uc_planoND","exec up_receituario_dadosDoPlano_ND '" + ALLTRIM(mySite) + "'")
		uf_perguntalt_chama("ERRO A VERIFICAR DADOS DO PLANO.", "OK", "", 64)
		RETURN lcPlan
	ENDIF
	
	
	if(USED("uc_planoND"))
		SELECT uc_planoND
		GO TOP
		lcPlan = ALLTRIM(uc_planoND.codigo)		
	ENDIF
	
	
	fecha("uc_planoND")
	
	
	RETURN lcPlan 

ENDFUNC


FUNCTION uf_atendimento_aplicaPlanoND
	LPARAMETERS uv_fistamp

	IF EMPTY(uv_fistamp)
		RETURN .F.
	ENDIF

	IF !uf_gerais_actGrelha("","uc_planoND","exec up_receituario_dadosDoPlano_ND '" + ALLTRIM(mySite) + "'")
		uf_perguntalt_chama("ERRO A VERIFICAR DADOS DO PLANO.", "OK", "", 64)
		RETURN .F.
	ENDIF

	SELECT ucrsatendcomp
	APPEND BLANK
	REPLACE ucrsatendcomp.fistamp WITH uv_fistamp
	REPLACE ucrsatendcomp.codigo WITH ALLTRIM(uc_planoND.codigo)
	REPLACE ucrsatendcomp.design WITH ALLTRIM(uc_planoND.design)
	REPLACE ucrsatendcomp.cptorgabrev WITH ALLTRIM(uc_planoND.cptorgabrev)
	REPLACE ucrsatendcomp.modrct WITH ALLTRIM(uc_planoND.modrct)
	REPLACE ucrsatendcomp.maxembrct WITH uc_planoND.maxembrct
	REPLACE ucrsatendcomp.maxembmed WITH uc_planoND.maxembmed
	REPLACE ucrsatendcomp.maxembuni WITH uc_planoND.maxembuni
	REPLACE ucrsatendcomp.obriga_nrreceita WITH uc_planoND.obriga_nrreceita
	REPLACE ucrsatendcomp.entidade WITH uc_planoND.entidade

	FECHA("uc_planoND")

	RETURN .T.

ENDFUNC

FUNCTION uf_atendimento_chkReservaDEM
	LPARAMETERS uv_op

	IF EMPTY(uv_op)
		RETURN .T.
	ENDIF

	SELECT * FROM fi WITH (BUFFERING = .T.) ORDER BY fi.lordem ASC INTO CURSOR uc_fiTmp READWRITE

	DO CASE

		CASE ALLTRIM(uv_op) = "RESERVA"

         SELECT uc_fiTmp
         COUNT TO uv_count FOR LEFT(ALLTRIM(uc_fiTmp.design) ,1) = "."

         IF uv_count <= 1
            RETURN .T.
         ENDIF

			SELECT uc_fiTmp 
			GO TOP

			LOCATE FOR ALLTRIM(uc_fiTmp.tipoR) = "RM" AND !EMPTY(uc_fiTmp.receita) AND AT("*SUSPENSA RESERVA*", ALLTRIM(uc_fiTmp.design)) <> 0

			IF FOUND()

				uf_perguntalt_chama("N�O PODE CRIAR UMA RESERVA DE UMA DEM E REGULARIZAR UMA RESERVA NO MESMO ATENDIMENTO!", "OK", "", 64)
				RETURN .F.

			ENDIF

		CASE ALLTRIM(uv_op) = "REGULARIZAR"

			SELECT uc_fiTmp 
			GO TOP

			LOCATE FOR ALLTRIM(uc_fiTmp.tipoR) = "RES" AND !EMPTY(uc_fiTmp.receita) AND LEFT(ALLTRIM(uc_fiTmp.design), 1) <> "."

			IF FOUND()

				uf_perguntalt_chama("N�O PODE REGULARIZAR UMA RESERVA DEM E CRIAR UMA RESERVA DEM NO MESMO ATENDIMENTO!", "OK", "", 64)
				RETURN .F.

			ENDIF

		CASE ALLTRIM(uv_op) = "TERMINAR"

         SELECT uc_fiTmp
         COUNT TO uv_count FOR LEFT(ALLTRIM(uc_fiTmp.design) ,1) = "."

         IF uv_count <= 1
            RETURN .T.
         ENDIF
			uv_hasRes = .F.
			uv_hasReg = .F.

			SELECT uc_fiTmp 
			GO TOP

			LOCATE FOR ALLTRIM(uc_fiTmp.tipoR) = "RM" AND !EMPTY(uc_fiTmp.receita) AND AT("*SUSPENSA RESERVA*", ALLTRIM(uc_fiTmp.design)) <> 0

			IF FOUND()

				uv_hasReg = .T.

			ENDIF

			SELECT uc_fiTmp 
			GO TOP

			LOCATE FOR ALLTRIM(uc_fiTmp.tipoR) = "RES" AND !EMPTY(uc_fiTmp.receita) AND LEFT(ALLTRIM(uc_fiTmp.design), 1) <> "."

			IF FOUND()

				uv_hasRes = .T.

			ENDIF

			IF uv_hasReg AND uv_hasRes

				uf_perguntalt_chama("N�O PODE REGULARIZAR UMA RESERVA DEM E CRIAR UMA RESERVA DEM NO MESMO ATENDIMENTO!", "OK", "", 64)
				RETURN .F.				

			ENDIF

		OTHERWISE
			RETURN .T.
	ENDCASE


	RETURN .T.
ENDFUNC

PROCEDURE uf_atendimento_mostraSinave
    LPARAMETERS uv_origem

    DO CASE
        CASE UPPER(ALLTRIM(uv_origem)) = 'ATENDIMENTO'

            IF USED("ucrsAtendComp")

                SELECT fi
                GO TOP

                SELECT ucrsAtendComp
                LOCATE FOR ucrsAtendComp.fistamp = fi.fistamp

                IF FOUND()

                    IF ucrsAtendComp.mcdt

                        IF EMPTY(uf_gerais_getUmValor("ext_sinave", "token", "nr_atend = '" + ALLTRIM(nratendimento) + "' AND nr_receita = '" + ALLTRIM(ucrsAtendComp.nrReceita) + "'"))
                        	IF !EMPTY(ucrsAtendComp.nrReceita) AND LEN(ALLTRIM(ucrsAtendComp.nrReceita)) >=19
                            	uf_atendimento_chamaSinave()
                            ENDIF
                        ENDIF

                    ENDIF

                ENDIF

            ENDIF

        CASE UPPER(ALLTRIM(uv_origem)) = 'FACTURACAO'

            SELECT ft2

            IF uf_gerais_getUmValor("cptpla", "mcdt", "codigo = '" + ALLTRIM(ft2.u_codigo) + "'")

                IF EMPTY(uf_gerais_getUmValor("ext_sinave", "token", "(ftstamp = '" + ALLTRIM(ft.ftstamp) + "' OR nr_atend = '" + ALLTRIM(ft.u_nrAtend) + "') AND nr_receita = '" + ALLTRIM(ft2.u_receita) + "'"))

                    uf_facturacao_chamaRegSinave()

                ENDIF

            ENDIF

    ENDCASE

ENDPROC

FUNCTION uf_atendimento_chamaSinave

    uv_nrReceita = ''
    uv_plano = ''
    uv_nrAtend = ''
    uv_no = ucrsAtendCl.no

    SELECT * FROM fi WITH (BUFFERING = .T.) INTO CURSOR uc_fiTmp READWRITE

    SELECT uc_fiTmp
    GO TOP

    SELECT ucrsAtendComp
    LOCATE FOR ucrsAtendComp.fistamp = uc_fiTmp.fistamp

    IF FOUND()
        uv_nrReceita = uc_fiTmp.receita
        uv_plano = ucrsAtendComp.codigo
        uv_nrAtend = nratendimento
    ENDIF

    IF !EMPTY(uv_plano) AND (EMPTY(uv_nrReceita) OR LEN(ALLTRIM(uv_nrReceita)) < 19)
        uf_perguntalt_chama("TEM DE PREENHCER O N�MERO DA RECEITA!", "OK", "", 64)
        RETURN .F.
    ENDIF
    

    uf_comunicasinave_chama(uv_no, uv_plano, '', uv_nrAtend, uv_nrReceita)

ENDFUNC

FUNCTION uf_atendimento_valPsico

   lctextopsi = ''
   lcvalpsifalta = .T.

   DO CASE
      CASE UPPER(ALLTRIM(mypaisconfsoftw)) = 'ANGOLA'

         IF EMPTY(ALLTRIM(dadospsico.u_nmutavi))
               lcvalpsifalta = .F.
               lctextopsi = lctextopsi+CHR(13)+"Nome Adquirente;"
         ENDIF
         IF EMPTY(ALLTRIM(dadospsico.contactoCli))
                  lcvalpsifalta = .F.
                  lctextopsi = lctextopsi+CHR(13)+"Contacto do Adquirente;"
         ENDIF
         IF EMPTY(ALLTRIM(dadospsico.u_ndutavi))
               lcvalpsifalta = .F.
               lctextopsi = lctextopsi+CHR(13)+"Doc ID Adquirente;"
         ENDIF
         IF uf_gerais_getdate(dadospsico.u_ddutavi, "SQL")=="19000101"
               lcvalpsifalta = .F.
               lctextopsi = lctextopsi+CHR(13)+"Validade Doc ID Adquirente;"
         ENDIF
         IF EMPTY(dadospsico.codigoDocSPMS)
               lcvalpsifalta = .F.
               lctextopsi = lctextopsi+CHR(13)+"Tipo Documento ID Adquirente;"
         ENDIF
         **IF (dadospsico.u_idutavi>130 .OR. dadospsico.u_idutavi<1)
               **lcvalpsifalta = .F.
               **lctextopsi = lctextopsi+CHR(13)+"Data de Nascimento Adquirente;"
         **ENDIF

      OTHERWISE

      IF ALLTRIM(ucrscabvendas.design) = '.SEM RECEITA' .AND.  .NOT. (RIGHT(ALLTRIM(ucrscabvendas.design), 10)=="*SUSPENSA*") .AND.  .NOT. (RIGHT(ALLTRIM(ucrscabvendas.design), 18)=="*SUSPENSA RESERVA*")
         lcvalpsifalta = .F.
               lctextopsi = lctextopsi+CHR(13)+"N� Receita;"
      ENDIF
         IF EMPTY(ALLTRIM(dadospsico.u_medico))
                  lcvalpsifalta = .F.
                  lctextopsi = lctextopsi+CHR(13)+"M�dico;"
         ENDIF
         IF EMPTY(ALLTRIM(dadospsico.u_nmutavi))
               lcvalpsifalta = .F.
               lctextopsi = lctextopsi+CHR(13)+"Nome Adquirente;"
         ENDIF
         IF EMPTY(ALLTRIM(dadospsico.u_moutavi))
               lcvalpsifalta = .F.
               lctextopsi = lctextopsi+CHR(13)+"Morada Adquirente;"
         ENDIF
         IF EMPTY(ALLTRIM(dadospsico.u_cputavi))
               lcvalpsifalta = .F.
               lctextopsi = lctextopsi+CHR(13)+"C�digo Postal Adquirente;"
         ENDIF
         IF EMPTY(ALLTRIM(dadospsico.u_ndutavi))
               lcvalpsifalta = .F.
               lctextopsi = lctextopsi+CHR(13)+"Doc ID Adquirente;"
         ENDIF
         IF uf_gerais_getdate(dadospsico.u_ddutavi, "SQL")=="19000101"
               lcvalpsifalta = .F.
               lctextopsi = lctextopsi+CHR(13)+"Validade Doc ID Adquirente;"
         ENDIF
         IF (dadospsico.u_idutavi>130 .OR. dadospsico.u_idutavi<1)
               lcvalpsifalta = .F.
               lctextopsi = lctextopsi+CHR(13)+"Data de Nascimento Adquirente;"
         ENDIF
         IF EMPTY(ALLTRIM(dadospsico.u_nmutdisp))
               lcvalpsifalta = .F.
               lctextopsi = lctextopsi+CHR(13)+"Nome Doente;"
         ENDIF
         IF EMPTY(ALLTRIM(dadospsico.u_moutdisp))
               lcvalpsifalta = .F.
               lctextopsi = lctextopsi+CHR(13)+"Morada Doente;"
         ENDIF
         IF EMPTY(ALLTRIM(dadospsico.u_cputdisp))
               lcvalpsifalta = .F.
               lctextopsi = lctextopsi+CHR(13)+"C�digo Postal Doente;"
         ENDIF
         IF USED("uCrsAtendCl")
               IF EMPTY(ALLTRIM(uCrsAtendCl.codDocID))
                  lcvalpsifalta = .F.
                  lctextopsi = lctextopsi+CHR(13)+"Tipo de Documento ID;"
               ENDIF
         ENDIF

   ENDCASE

   IF !lcvalpsifalta
      uf_perguntalt_chama(lctextopsi, "OK", "", 64)
      RETURN .F.
   ENDIF

   RETURN .T.

ENDFUNC

FUNCTION uf_atendimento_checkDevMulti

   IF !USED("uCrsVale")
      RETURN .T.
   ENDIF

   uv_hasDev = .F.

   SELECT uCrsVale
   GO TOP

   LOCATE FOR uCrsVale.dev = 1

   IF FOUND()
      uv_hasDev = .T.
   ENDIF

   IF uv_hasDev

      SELECT COUNT(*) AS COUNT FROM FI WITH (BUFFERING = .T.) WHERE ofistamp in (SELECT FISTAMP FROM uCrsVale) INTO CURSOR uc_countDev

      uv_countDev = uc_countDev.count

      FECHA("uc_countDev")

      SELECT fi
      COUNT TO uv_count FOR !EMPTY(fi.ref)

      IF uv_count > uv_countDev
         RETURN .F.
      ENDIF

   ENDIF

   RETURN .T.

ENDFUNC

FUNCTION uf_atendimento_devolveStampsDocumentosDiferentes

	LOCAL lcOfiStamp, lcResult

	lcOfiStamp = ''
	lcResult = .t.
	
	SELECT fi
	GO TOP
	SCAN FOR !EMPTY(ALLTRIM(fi.ofistamp))
		lcOfiStamp 	=  lcOfiStamp   + ALLTRIM(fi.ofistamp) + ";"
	ENDSCAN
	
	if(LEN(lcOfiStamp)>1)
		lcOfiStamp = LEFT(lcOfiStamp ,LEN(TRIM(lcOfiStamp))-1)
	ENDIF
	
	fecha("ucrsTempResult")

	TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_gerais_validaDocumentosDeOrigemDiferentes '<<ALLTRIM(lcOfiStamp)>>'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "ucrsTempResult", lcsql)
       uf_perguntalt_chama("N�o foi poss�vel verificar as origens dos documentos", "OK", "", 16)
       RETURN .T.
    ENDIF
   
    
    SELECT ucrsTempResult
    GO TOP
    lcResult = ucrsTempResult.resultado
	
	fecha("ucrsTempResult")
	
	RETURN lcResult 
	
	
ENDFUNC

FUNCTION uf_atendimento_sendToXopvision

	uv_terminal = myTermXopPos

	IF EMPTY(myXopPosUrl)
		RETURN .F.
	ENDIF

	IF EMPTY(uv_terminal)
		RETURN .F.
	ENDIF
	
	IF !USED("ft") OR !USED("fi")
		RETURN .F.
	ENDIF

	DIMENSION ua_fieldsLinhas(4)
	DIMENSION ua_fieldsFooter(6)

	ua_fieldsLinhas(1) = "Descricao"
	ua_fieldsLinhas(2) = "Qtd"
	ua_fieldsLinhas(3) = "valor"
	ua_fieldsLinhas(4) = "Messages"

	ua_fieldsFooter(1) = "Labeltroco"
	ua_fieldsFooter(2) = "Cliente"
	ua_fieldsFooter(3) = "Itens"
	ua_fieldsFooter(4) = "Totais"
	ua_fieldsFooter(5) = "Troco"
	ua_fieldsFooter(6) = "coin"

	SELECT design as Descricao, ALLTRIM(STR(qtt)) as Qtd, TRANSFORM(etiliquido,'9999999.99') as valor, "" as Messages FROM fi WITH (BUFFERING = .T.) WHERE !EMPTY(ref) AND stns = .F. INTO CURSOR uc_tmpFIXop

	uv_jsonLinhas = uf_gerais_cursorToJson("uc_tmpFIXop", "Lines_Display", .T., @ua_fieldsLinhas)

	SELECT uc_tmpFIXop
	GO TOP

	COUNT TO uv_itens

	uv_total = ATENDIMENTO.txttotalutente.value

	uv_moeda = mySimboloMoeda

	CREATE CURSOR uc_footerXop (Labeltroco C(100), Cliente C(100), Itens C(100), Totais C(100), Troco C(100), coin C(100))

	SELECT uc_footerXop
	APPEND BLANK

	REPLACE uc_footerXop.Labeltroco WITH "troco"
	**REPLACE uc_footerXop.Cliente WITH ALLTRIM(ucrsatendcl.nome)
	REPLACE uc_footerXop.Cliente WITH ""
	REPLACE uc_footerXop.Itens WITH ASTR(uv_itens)
	REPLACE uc_footerXop.Totais WITH TRANSFORM(uv_total, '9999999.99')
	REPLACE uc_footerXop.Troco WITH "0"
	REPLACE uc_footerXop.coin WITH ALLTRIM(uv_moeda)

	uv_jsonFooter = uf_gerais_cursorToJson("uc_footerXop", "Footer_Display", .F., @ua_fieldsFooter)

	uv_json = "{" + CHR(13) + CHR(9) + ALLTRIM(uv_jsonLinhas) + ',' + CHR(13) + CHR(9) + ALLTRIM(uv_jsonFooter) + CHR(13) + "}"

	IF TYPE("upv_ultPedidoXop") <> "U"
		IF ALLTRIM(upv_ultPedidoXop) == ALLTRIM(uv_json)	
			RETURN .F.
		ENDIF
	ELSE
		PUBLIC upv_ultPedidoXop
	ENDIF

	upv_ultPedidoXop = uv_json

	uf_servicos_unblockChilkat()	
	
	loReq = CreateObject('Chilkat_9_5_0.HttpRequest')
	loHttp = CreateObject('Chilkat_9_5_0.Http')

	uv_timeOut = (ATENDIMENTO.timerXop.interval/1000) * 0.9

	loHttp.ConnectTimeout  = uv_timeOut
	loHttp.ReadTimeout = uv_timeOut

	lnSuccess = loHttp.AddQuickHeader("Content-Type","application/json")

	uv_credenciais = myXopPosCred

	IF EMPTY(uv_credenciais)
		RETURN .F.
	ENDIF

	FOR i = 1 TO ALINES(ua_creds, uv_credenciais, ";")

		uv_cred = ua_creds(i)

		uv_cred = STRTRAN(uv_cred, ':', ',')

		loHttp.SetRequestHeader(&uv_cred.)
	NEXT

	uv_url = myXopPosUrl

	loResp = loHttp.PostJson2( ALLTRIM(uv_url) + "?idbalcao=" + ASTR(uv_terminal), "application/json", uv_json)
	
	if(emdesenvolvimento and !EMPTY(uv_json))
	 _cliptext = uv_json
	ENDIF
	 

	IF loHttp.LastMethodSuccess <> 1

		lcBody = ALLTRIM(loHttp.LastErrorText)

		IF !EMPTY(lcBody)
			_clipText = uv_json + CHR(13) + lcBody
		ENDIF
		
		RELEASE loReq
		RELEASE loHttp

		RETURN .F.

	ELSE
		lcResponseHeaderCode = loResp.StatusCode

		IF lcResponseHeaderCode <> 200

			_clipText = uv_json + CHR(13) + ASTR(lcResponseHeaderCode)

		ENDIF

	ENDIF

	RETURN .T.

ENDFUNC

FUNCTION uf_atendimento_clearXopvisionPOS

	uv_terminal = myTermXopPos

	IF EMPTY(myXopPosUrl)
		RETURN .F.
	ENDIF

	IF EMPTY(uv_terminal)
		RETURN .F.
	ENDIF
	
	IF !USED("ft") OR !USED("fi")
		RETURN .F.
	ENDIF

	TEXT TO uv_json TEXTMERGE NOSHOW
		{
			"Lines_Display":[
		],
			"Footer_Display":{
			
				"Labeltroco":"",
				"Cliente":"",
				"Itens":"",
				"Totais":"",
				"Troco":"0",
				"coin":""
			
		}
		}
	ENDTEXT

	uf_servicos_unblockChilkat()	
	
	loReq = CreateObject('Chilkat_9_5_0.HttpRequest')
	loHttp = CreateObject('Chilkat_9_5_0.Http')

	uv_timeOut = (ATENDIMENTO.timerXop.interval/1000) * 0.9

	loHttp.ConnectTimeout  = uv_timeOut
	loHttp.ReadTimeout = uv_timeOut

	lnSuccess = loHttp.AddQuickHeader("Content-Type","application/json")

	uv_credenciais = myXopPosCred

	IF EMPTY(uv_credenciais)
		RETURN .F.
	ENDIF

	FOR i = 1 TO ALINES(ua_creds, uv_credenciais, ";")

		uv_cred = ua_creds(i)

		uv_cred = STRTRAN(uv_cred, ':', ',')

		loHttp.SetRequestHeader(&uv_cred.)
	NEXT

	uv_url = myXopPosUrl

	loResp = loHttp.PostJson2( ALLTRIM(uv_url) + "?idbalcao=" + ASTR(uv_terminal), "application/json", uv_json)

	IF loHttp.LastMethodSuccess <> 1

		lcBody = ALLTRIM(loHttp.LastErrorText)

		IF !EMPTY(lcBody)
			_clipText = uv_json + CHR(13) + lcBody
		ENDIF
		
		RELEASE loReq
		RELEASE loHttp

		RETURN .F.

	ELSE
		lcResponseHeaderCode = loResp.StatusCode

		IF lcResponseHeaderCode <> 200

			_clipText = uv_json + CHR(13) + ASTR(lcResponseHeaderCode)

		ENDIF

	ENDIF

	RETURN .T.

ENDFUNC

FUNCTION uf_atendimento_aplicaTabCompartAddLine

	IF !USED("fi")
		RETURN .F.
	ENDIF

	SELECT FI
	IF !EMPTY(fi.oFistamp)
		RETURN .F.
	ENDIF

	LOCAL uv_curLordem, uv_plano, uv_recNoTop, uv_done, uv_plano

	SELECT FI

	uv_curLordem = fi.lordem

	SELECT * FROM fi WITH (BUFFERING = .T.) WHERE lordem <= uv_curLordem INTO CURSOR uc_tmpFi

	SELECT uc_tmpFi
	GO TOP

	uv_recNoTop = RECNO("uc_tmpFi")

	SELECT uc_tmpFi
	GO BOTTOM


	uv_done = .F.
	uv_plano = ''

	DO WHILE !uv_done

		SELECT uc_tmpFi
		IF RECNO("uc_tmpFi") = uv_recNoTop

			uv_done = .T.

			IF LEFT(uc_tmpFi.design,1) == '.'

				uv_plano = STREXTRACT(uc_tmpFi.design, '[', ']', 1, 0)
		
			ENDIF

		ELSE

			SELECT uc_tmpFi
			IF LEFT(uc_tmpFi.design,1) == '.'

				uv_done = .T.
				uv_plano = STREXTRACT(uc_tmpFi.design, '[', ']', 1, 0)
				
			ELSE

				SELECT uc_tmpFi
				SKIP -1

			ENDIF

		ENDIF

	ENDDO

	IF !EMPTY(uv_plano)

		SELECT FI

		uf_atendimento_aplicaTabCompart_BI(uv_plano, fi.ref)

	ENDIF

ENDFUNC

FUNCTION uf_atendimento_calcValElegibilidade

   IF !usamancompart
      RETURN 0
   ENDIF

   IF !USED("fi")
      RETURN 0
   ENDIF

   LOCAL uv_curLordem, uv_valElegibilidade, uv_descSeg, uv_totLinha
   STORE 0 to uv_valElegibilidade, uv_totLinha

   SELECT FI

   IF STREXTRACT(fi.design, '[', ']', 1, 0) == ""
      RETURN 0
   ENDIF

   uv_curLordem = fi.lordem
   uv_descSeg = uCrsAtendComp.descHabitual

   IF uf_gerais_compStr(uCrsAtendComp.codigo, uf_gerais_getParameter_site("ADM0000000167", "TEXT", mySite))
      RETURN 0
   ENDIF

   SELECT * FROM FI WITH (BUFFERING = .T.) WHERE lordem > uv_curLordem INTO CURSOR uc_tmpFI

   SELECT uc_tmpFI
   GO TOP
   
   SCAN

      IF LEFT(uc_tmpFI.design, 1) == "."
         EXIT
      ENDIF

      uv_totLinha = 0 

      IF (uc_tmpFI.desconto>0) .OR. (uc_tmpFI.desc2>0) .OR. (uc_tmpFI.u_descval>0)
         IF (uc_tmpFI.desconto>0)
            uv_totLinha = ROUND(uc_tmpFI.epvori*uc_tmpFI.qtt-(uc_tmpFI.epvori*uc_tmpFI.qtt*(fi.desconto/100)), 2)
         ENDIF
         IF (uc_tmpFI.desc2>0)
            IF (uc_tmpFI.desconto=0)
               uv_totLinha = ROUND(uc_tmpFI.epvori*uc_tmpFI.qtt-(uc_tmpFI.epvori*uc_tmpFI.qtt*(fi.desc2/100)), 2)
            ELSE
               uv_totLinha = uc_tmpFI.epvori*uc_tmpFI.qtt-((uc_tmpFI.epvori*uc_tmpFI.qtt)*(uc_tmpFI.desc2/100))
            ENDIF
         ENDIF
         IF (uc_tmpFI.u_descval>0)
            IF uc_tmpFI.desconto==0 .AND. uc_tmpFI.desc2==0
               uv_totLinha = ROUND(uc_tmpFI.epvori*uc_tmpFI.qtt-(uc_tmpFI.u_descval), 2)
            ELSE
               uv_totLinha = (uc_tmpFI.epvori*uc_tmpFI.qtt)-(uc_tmpFI.u_descval)
            ENDIF
         ENDIF
      ELSE

         uv_totLinha = ROUND(uc_tmpFI.epvori*uc_tmpFI.qtt,2)

      ENDIF

      uv_valElegibilidade = uv_valElegibilidade + ROUND(uv_totLinha - ((uv_totLinha * uv_descSeg)/100),2)
	
	  SELECT uc_tmpFI
   ENDSCAN
   
   FECHA("uc_tmpFI")

   RETURN uv_valElegibilidade

ENDFUNC

FUNCTION uf_atendimento_obrigaNrReceita
   LPARAMETERS uv_cabStamp

   IF EMPTY(uv_cabStamp)
      RETURN .F.
   ENDIF

   SELECT FI 
   LOCATE FOR uf_gerais_compStr(uv_cabStamp, fi.fistamp)

   IF FOUND()

      SELECT FI

      IF EMPTY(STREXTRACT(fi.design, ':', ' -', 1, 0))
         IF !uf_atendimento_dadosreceita("Atendimento.pgfDados.page3.btnReceita.label1", .T., uv_cabStamp, .T.)
            uf_perguntalt_chama("O Nr� de Receita � de preenchimento obrigat�rio.", "OK", "", 48)
            RETURN .F.
         ENDIF
      ENDIF

   ENDIF

   RETURN .T.

ENDFUNC

FUNCTION uf_atendimento_obrigaNrUtente
   LPARAMETERS uv_cabStamp

   SELECT u_nbenef FROM fi WITH(BUFFERING = .T.) WHERE fistamp = uv_cabStamp INTO CURSOR uc_nBenef

   IF EMPTY(uc_nBenef.u_nbenef)

      uf_tecladoalpha_chama("Atendimento.pgfDados.page1.txtU_nbenef", "Introduza o Nr. Utente SNS:", .F., .F., 1)
      
      SELECT ucrsAtendCL
      
      if(!uf_gerais_IsNumeric(ALLTRIM(ucrsAtendCl.nbenef)))
         uf_perguntalt_chama("N� de Benefici�rio deve ser num�rico.", "OK", "", 48)
         replace ucrsAtendCL.nbenef WITH ""
      ENDIF
      
      
      IF LEN(ALLTRIM(ucrsAtendCl.nbenef)) > 18
         uf_perguntalt_chama("Tamanho m�ximo do n� de Benefici�rio excedido.", "OK", "", 48)
         replace ucrsAtendCL.nbenef WITH ""
         
      ENDIF

      
      IF EMPTY(ucrsAtendCl.nbenef)
         uf_perguntalt_chama("O Nr� de Utente � de preenchimento obrigat�rio.", "OK", "", 48)
         RETURN .F.
      ELSE

         SELECT FI 
         UPDATE FI set u_nbenef = ucrsAtendCl.nbenef WHERE fi.fistamp = uv_cabStamp

      ENDIF
   ENDIF

   FECHA("uc_nBenef")

   RETURN .T.

ENDFUNC


FUNCTION uf_atendimento_valida_estado_datamatrix
   LPARAMETERS lcBackoffice, lcDocumentos
 
  IF USED("fi_trans_info") AND  UPPER(mypaisconfsoftw)=='PORTUGAL' AND !EMPTY(uf_gerais_getParameter_site("ADM0000000205", "BOOL", mySite))
 	IF  (RECCOUNT("fi_trans_info")> 0)
 
		**
 	   uf_infoembal_pullRefs(lcBackoffice, lcDocumentos)

		**
      IF lcDocumentos
         SELECT fi_trans_info
         GO TOP
         CALCULATE COUNT() TO uv_count
         IF uv_count = 0
            RETURN .T.
         ENDIF

      ENDIF


      PUBLIC upv_cancelaInfoEmbal
      STORE .F. TO upv_cancelaInfoEmbal
		
	 	uf_infoembal_chama(lcBackoffice, lcDocumentos)

      IF upv_cancelaInfoEmbal
         RETURN .F.
      ENDIF
	 ENDIF	 		
  ENDIF

  RETURN .T.

ENDFUNC

FUNCTION uf_atendimento_valida_obito
	
	LOCAL lcSqlObitoEdit 
	STORE '' TO lcSqlObitoEdit 
	
	IF !USED("uCrsAtendCL") 
		uf_perguntalt_chama("Erro ao obter o utente ou informa��es da receita.", "OK", "", 48)
      RETURN .F.
	ENDIF
	
   IF uf_gerais_getUmValor("b_utentes", "obitoRNU", "utstamp = '" + uCrsAtendCL.utstamp + "'")
      uf_perguntalt_chama("Aten��o: Este utente tem o campo �bito ativo na ficha, o que poder� gerar receitu�rio devolvido.", "OK", "", 48) 
   ENDIF	
	
	RETURN .T.
	
ENDFUNC

FUNCTION uf_atendimento_precosSpms
	
	LOCAL lcTokenTratado, lcNrProd
	STORE '' TO lcTokenTratado
	STORE 0 TO lcNrProd
	
	uf_servicos_pricesDem_closeCursors()
	
	IF !USED("FI")
		uf_perguntalt_chama("Erro ao obter informa��es dos produtos no atendimento", "OK", "", 48)
     	RETURN .F.
	ENDIF
	
	SELECT FI 
	COUNT TO lcNrProd FOR !EMPTY(fi.ref) AND EMPTY(fi.ofistamp)
	
	IF(lcNrProd = 0)
		uf_perguntalt_chama("Por favor insera um produto para venda, para utilizar o servi�o.", "OK", "", 48)
     	RETURN .F.
	ENDIF
	
	lcTokenTratado = uf_atendimento_precosSpms_inserts()
	
	IF EMPTY(lcTokenTratado)
		RETURN .F.
	ENDIF
	
	IF !USED("fi")
		uf_perguntalt_chama("Ocorreu um erro ao obter as linhas da venda, por favor contacte o suporte.", "OK", "", 48)
     	RETURN .F.
	ENDIF
	
	uf_servicos_pricesDem(lcTokenTratado)
	
ENDFUNC

FUNCTION uf_atendimento_precosSpms_inserts

	LOCAL lcToken
	STORE '' TO lcToken
		
	IF EMPTY(lcToken)
		lcToken = uf_gerais_gerarIdentifiers(36)
	ENDIF
	
	IF EMPTY(nratendimento)
		uf_perguntalt_chama("Ocorreu um erro ao gerar o n�mero de atendimento, por favor contate o suporte.", "OK", "", 48)
     	RETURN lcToken
	ENDIF
	
    SELECT FI
    GO TOP
    SCAN FOR !EMPTY(fi.ref) AND EMPTY(fi.ofistamp)
    	uf_servicos_pricesDem_createSend(lcToken,ALLTRIM(fi.ref), nratendimento)
    ENDSCAN
		
	RETURN lcToken 
ENDFUNC

FUNCTION uf_atendimento_precosSpms_atualizaLinhas
	
	LOCAL lcPosFi
	
	IF !USED("ucrsMergeSearch")
		RETURN .F.
	ENDIF
	
	IF (uf_gerais_getParameter_site("ADM0000000214","BOOL") == .f. AND Reccount("ucrsMergeSearch") = 0)
		RETURN .F.
	ENDIF
	
	IF !USED("fi")
		uf_perguntalt_chama("Ocorreu um erro ao obter as linhas da venda, por favor contacte o suporte.", "OK", "", 48)
     	RETURN .F.
	ENDIF
	
	SELECT FI
	lcPosFi = RECNO("FI")
	
	SELECT fi
	GO TOP
	
	SELECT ucrsMergeSearch
	GO TOP
	SCAN FOR !EMPTY(ALLTRIM(ucrsMergeSearch.ref))
		SELECT FI
		GO TOP
		
		UPDATE fi SET fi.u_epref = ucrsMergeSearch.pricerefere, fi.PVPmaxre = ucrsMergeSearch.pricemax, fi.PVPTOP4  = ucrsMergeSearch.fourlowestprice WHERE ALLTRIM(ucrsMergeSearch.ref) = ALLTRIM(fi.ref) AND EMPTY(ALLTRIM(fi.ofistamp))
		
		SELECT ucrsMergeSearch
	ENDSCAN
	
	SELECT fi
     TRY
        GOTO lcPosFi
     CATCH
        GOTO BOTTOM
     ENDTRY
	
	RETURN .T.
ENDFUNC




FUNCTION uf_atendimento_precosSpms_grelhaPrecosValidos
	LPARAMETERS lcRefPrecosValidos
	
	IF !USED("ucrsMergeSearch")
		uf_perguntalt_chama("Ocorreu um erro ao obter pre�os do spms, por favor contacte o suporte.", "OK", "", 48)
     	RETURN .F.
	ENDIF
	IF !USED("fi")
		uf_perguntalt_chama("Ocorreu um erro ao obter as linhas da venda, por favor contacte o suporte.", "OK", "", 48)
     	RETURN .F.
	ENDIF

	
	SELECT ucrsMergeSearch
	SELECT ROUND(ucrsMergeSearch.pvp,2) AS PIC, "Sim" AS PRECO_VALIDO FROM ucrsMergeSearch WHERE ucrsMergeSearch.ref = lcRefPrecosValidos INTO CURSOR ucrsMergeSearchFilter readwrite

	uf_valorescombo_chama("fi.u_epvp", 2, "ucrsMergeSearchFilter", 2, "PIC", "PIC, PRECO_VALIDO", .F., .F., .T., .T., .F., .T.)
	
	fecha("ucrsMergeSearchFilter")
	
	RETURN .T.
ENDFUNC

PROCEDURE uf_atendimento_interacao_medicamentos
	 LPARAMETERS lbEscondeMostraAlerta
	 IF(TYPE("atendimento")!="U")
		 atendimento.verinteraccoestodas = .T.
	     uf_infovenda_valida(.f.,lbEscondeMostraAlerta)
	     atendimento.verinteraccoestodas = .F.
     ENDIF
    
ENDPROC



PROCEDURE uf_atendimento_validaPlanoSemEntidadeEfr	
	
	IF UPPER(mypaisconfsoftw)=='PORTUGAL'
		
		**Atendimento
		if(USED("uCrsAtendComp") and TYPE("atendimento")!="U")
			LOCAL lcEfrDescr 
			lcEfrDescr = ""
			SELECT uCrsAtendComp
			lcEfrDescr = ALLTRIM(uCrsAtendComp.efr_descr)
			IF (uf_gerais_compStr(lcEfrDescr , "SEM COMPARTICIPA��O PELO SNS"))
				uf_perguntalt_chama("Aten��o: prescri��o sem comparticipa��o pelo SNS, n�o se aplicam entidades complementares.", "OK", "", 48)
			ENDIF			
		ENDIF
		
		**Backoffice
		IF(TYPE("atendimento")=="U")
			IF(USED("ft2"))
				LOCAL lcCodigoPlano
				SELECT ft2
				GO TOP
				lcCodigoPlano = ALLTRIM(ft2.u_codigo)
				IF(!EMPTY(lcCodigoPlano))
					IF uf_gerais_compStr(lcCodigoPlano, "SU")
						uf_perguntalt_chama("Aten��o: prescri��o sem comparticipa��o pelo SNS, n�o se aplicam entidades complementares.", "OK", "", 48)
					ENDIF					
				ENDIF
			ENDIF			
		ENDIF
		
	
	ENDIF
ENDFUNC
 





FUNCTION uf_atendimento_validaLotesVet
	
	LOCAL lcFiStamp
	SELECT fi 
	lcFiStamp = fi.fistamp
	
	IF 	fi.partes = 0 AND !EMPTY(fi.ofistamp)
		RETURN ''
	ENDIF

	
	
	IF !EMPTY(fi.ndoc)
	
		IF uf_gerais_getUmValor("td", "lancasl", "ndoc = '" + STR(fi.ndoc) + "'")  	
			SELECT fi
			GO TOP 
			SELECT fi2
			GO Top 
			SELECT fi.ref, fi.lote, fi2.dataValidade FROM FI ;
			INNER JOIN fi2 ON fi2.fistamp = fi.fistamp ;
			WHERE fi.familia in ("97", "98") AND EMPTY(fi.tpres); 
			INTO CURSOR uc_tmpFIVet

			SELECT fi 
			LOCATE FOR fi.fistamp 	= lcFiStamp 
			SELECT fi2
			Locate FOR fi2.fistamp 	= lcFiStamp 
			
		    SELECT uc_tmpFIVet
		    GO TOP
		    SCAN

				IF EMPTY(uc_tmpFIVet.lote) OR EMPTY(uc_tmpFIVet.dataValidade) OR year(uc_tmpFIVet.dataValidade)  = 1900
					RETURN uc_tmpFIVet.ref
				ENDIF
		    ENDSCAN

		    FECHA("uc_tmpFIVet")
			
			RETURN ''
			
		ENDIF 
	ELSE

		SELECT fi
		GO TOP 
		SELECT fi2
		GO Top 
		SELECT fi.ref, fi.lote, fi2.dataValidade FROM FI ;
		INNER JOIN fi2 ON fi2.fistamp = fi.fistamp ;
		WHERE fi.familia in ("97", "98") AND EMPTY(fi.tpres); 
		INTO CURSOR uc_tmpFIVet

		SELECT fi 
		LOCATE FOR fi.fistamp 	= lcFiStamp 
		SELECT fi2
		Locate FOR fi2.fistamp 	= lcFiStamp 
		
	    SELECT uc_tmpFIVet
	    GO TOP
	    SCAN

			IF EMPTY(uc_tmpFIVet.lote) OR EMPTY(uc_tmpFIVet.dataValidade) OR year(uc_tmpFIVet.dataValidade)  = 1900
				RETURN uc_tmpFIVet.ref
			ENDIF
	    ENDSCAN

	    FECHA("uc_tmpFIVet")
		
		RETURN ''
		
	ENDIF

	RETURN ''
ENDFUNC 

FUNCTION  uf_atendimento_validaDadosPrincipioActivo
	
 IF(!USED("FI"))
	RETURN .F.
 ENDIF
 select fi	
 local 	lcsql, lcDci 
 Store "" to lcsql,lcDci 
 
  IF (VARTYPE(lccodcnpext)<>'U')
    RELEASE lccodcnpext
 ENDIF
 
 Public lccodcnpext
 lccodcnpext = ''

	
 TEXT TO lcsql TEXTMERGE NOSHOW
	select codCNP,  convert(varchar(254),dosuni) as dosuni, convert(varchar(254),fformasabrev) as fformasabrev, ISNULL(fprod.dci,'') as dci from st(nolock) LEFT JOIN fprod(nolock) ON fprod.ref=st.ref WHERE st.ref='<<ALLTRIM(fi.ref)>>' AND st.site_nr=<<mySite_nr>>
 ENDTEXT
 LOCAL  lcDosuni , lcFormasaFarmAbrev 
 uf_gerais_actgrelha("", "ucrscodcnp", lcsql)
 lccodcnpext = ALLTRIM(ucrscodcnp.codcnp)
 lcDosuni  = ALLTRIM(ucrscodcnp.dosuni)
 lcFormasaFarmAbrev  = ALLTRIM(ucrscodcnp.fformasabrev)
 lcDci  = ALLTRIM(ucrscodcnp.dci)
 
 
 fecha("ucrscodcnp")
 SELECT fi
 IF EMPTY(lccodcnpext)

 	IF(TYPE("atendimento")!="U")  	   
		lcDCI = ALLTRIM(atendimento.pgfdados.page2.txtdci.value)
   	ENDIF
   	
    if(USED("ucrsatenddci"))
      SELECT ucrsatenddci
    ENDIF    	
       	 	
    IF (ALLTRIM(fi.lobs3)=="C2")
    	
       uf_principioactivo_chama(ALLTRIM(fi.ref), ALLTRIM(fi.design), lcDCI  , "CNPEM", .f., lcDosuni, lcFormasaFarmAbrev)
    ELSE

       uf_principioactivo_chama(ALLTRIM(fi.ref), ALLTRIM(fi.design), lcDCI  , "DCI",  .f., lcDosuni, lcFormasaFarmAbrev)
    ENDIF
 ELSE
    IF (ALLTRIM(fi.lobs3)=="C2")

       uf_principioactivo_chama(ALLTRIM(lccodcnpext), ALLTRIM(fi.design), lcDCI  ,  .f., "CNPEM", lcDosuni, lcFormasaFarmAbrev)
    ELSE

       uf_principioactivo_chama(ALLTRIM(lccodcnpext), ALLTRIM(fi.design), lcDCI  ,  .f., "DCI", lcDosuni, lcFormasaFarmAbrev)
    ENDIF
 ENDIF
	
	
ENDFUNC



