**
FUNCTION uf_resumos_chama
 LPARAMETERS lcno, lcestab
 IF  .NOT. uf_gerais_validapermuser(ch_userno, ch_grupo, 'Impress�o de Resumos')
    uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL DE IMPRESS�O DE RESUMOS.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF TYPE("resumos")=="U"
    uf_resumos_criacursores()
    DO FORM resumos
    resumos.menu1.adicionaopcao("opcoes", "Op��es", mypath+"\imagens\icons\opcoes_w.png", "", "F1")
    resumos.menu1.adicionaopcao("pesquisar", "Pesquisar", mypath+"\imagens\icons\lupa_w.png", "uf_resumos_pesquisar", "F2")
    resumos.menu1.adicionaopcao("selTodos", "Sel. Todos", mypath+"\imagens\icons\unchecked_w.png", "uf_resumos_selTodos WITH .T.", "F3")
    resumos.menu1.adicionaopcao("cima", "Cima", mypath+"\imagens\icons\ponta_seta_up.png", "uf_resumos_moverLinha WITH .T.", "F4")
    resumos.menu1.adicionaopcao("baixo", "Baixo", mypath+"\imagens\icons\ponta_seta_down.png", "uf_resumos_moverLinha WITH .F.", "F5")
    resumos.menu_opcoes.adicionaopcao("talao", "Imprimir Tal�o", "", "uf_resumos_imprimir with 1,0")
    resumos.menu_opcoes.adicionaopcao("reimp", "Reimprimir Tal�o(�es)", "", "uf_resumos_ImpTalao")
    resumos.menu_opcoes.adicionaopcao("expvdpdf", "Exportar Doc.(s) p/ PDF", "", "uf_resumos_expvdpdf")
    resumos.menu_opcoes.adicionaopcao("imprimir_por_venda", "Imprimir A4 por Venda", "", "uf_resumos_imprimir with 2,2")
    resumos.menu_opcoes.adicionaopcao("imprimir_por_utente", "Imprimir A4 por Utente", "", "uf_resumos_imprimir with 3,2")
    resumos.menu_opcoes.adicionaopcao("previsao_por_venda", "Previs�o A4 por Venda", "", "uf_resumos_imprimir with 2,1")
    resumos.menu_opcoes.adicionaopcao("previsao_por_utente", "Previs�o A4 por Utente", "", "uf_resumos_imprimir with 3,1")
    resumos.menu_opcoes.adicionaopcao("previsao_simpolificada", "Exportar PDF Simplificado", "", "uf_resumos_imprimir with 2,3")
    resumos.menu_opcoes.adicionaopcao("exportxls", "Exportar para Excel", "", "uf_resumos_exporta")
    resumos.txtdataini.value = uf_gerais_getdate(DATETIME()+(difhoraria*3600))
    resumos.txtdatafim.value = uf_gerais_getdate(DATETIME()+(difhoraria*3600))
    IF TYPE("lcNo")=="N"
       resumos.txtnumero.value = lcno
    ENDIF
    IF TYPE("lcEstab")=="N"
       resumos.txtestab.value = lcestab
    ENDIF
 ELSE
    resumos.show
 ENDIF
 	resumos.refresh
ENDFUNC
**
PROCEDURE uf_resumos_seltodos
 LPARAMETERS lcbool
 resumos.txtplano.setfocus()
 SELECT ucrsresumos
 GOTO TOP
 SCAN
    REPLACE ucrsresumos.sel WITH lcbool
 ENDSCAN
 GOTO TOP
 IF lcbool
    resumos.menu1.seltodos.config("Dess. Todos", mypath+"\imagens\icons\checked_w.png", "uf_resumos_selTodos with .F.", "F3")
 ELSE
    resumos.menu1.seltodos.config("Sel. Todos", mypath+"\imagens\icons\unchecked_w.png", "uf_resumos_selTodos with .T.", "F3")
 ENDIF
 uf_resumos_calctotal()
ENDPROC
**
FUNCTION uf_resumos_criacursores
 IF  .NOT. uf_gerais_actgrelha("", "uCrsResumosOps", 'exec up_gerais_vendedores')
    uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR [UCRSRESUMOSOP].", "OK", "", 16)
    RETURN .F.
 ENDIF

 IF  .NOT. uf_gerais_actgrelha("", "uCrsResumosDocs", 'Select distinct convert(varchar(254),nmdoc) as nmdoc  from ft (nolock) where ndoc!=72 and ndoc!=73 order by nmdoc')
    uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR [UCRSRESUMOSDOCS].", "OK", "", 16)
    RETURN .F.
 ENDIF
 SELECT ucrsresumosdocs
 APPEND BLANK
 REPLACE ucrsresumosdocs.nmdoc WITH 'Todos'
 IF  .NOT. uf_gerais_actgrelha("", "uCrsResumosTerminais", 'select distinct terminal,no from b_terminal (nolock) order by no')
    uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR [UCRSRESUMOSTERMINAIS].", "OK", "", 16)
    RETURN .F.
 ENDIF
 SELECT ucrsresumosterminais
 APPEND BLANK
 REPLACE ucrsresumosterminais.no WITH 0
 REPLACE ucrsresumosterminais.terminal WITH 'Todos'
 IF  .NOT. uf_gerais_actgrelha("", "uCrsresumos", [exec up_touch_reImprimirBulk 0, '', 0, 0, '', '', '', '', '', '', 500, '',0,'',0,0])
    uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR [UCRSRESUMOS].", "OK", "", 16)
    RETURN .F.
 ENDIF
ENDFUNC
**
PROCEDURE uf_resumos_sair
 resumos.release
 RELEASE resumos
 RELEASE myorderresumos
 IF USED("uCrsResumosOps")
    fecha("uCrsResumosOps")
 ENDIF
 IF USED("uCrsResumosDocs")
    fecha("uCrsResumosDocs")
 ENDIF
 IF USED("uCrsResumosTerminais")
    fecha("uCrsResumosTerminais")
 ENDIF
 IF USED("UCRSRESUMOS")
    fecha("UCRSRESUMOS")
 ENDIF
ENDPROC
**
FUNCTION uf_resumos_pesquisar
 LOCAL lcsql, lcdataini, lcdatafim, lcoperador, lcterminal, lccliente, lcnumero, lcestab, lctop, lcvenda, lcdoc, lctipocliente, lcSuspensas, lcCredito
 STORE '' TO lcsql
 lcdataini = CTOD(resumos.txtdataini.value)
 lcdatafim = CTOD(resumos.txtdatafim.value)
 lcoperador = ALLTRIM(ucrsresumosops.cmdesc)
 lcterminal = ALLTRIM(ucrsresumosterminais.terminal)
 lccliente = ALLTRIM(STRTRAN(resumos.txtcliente.value, ' ', ''))
 lcnumero = IIF(EMPTY(resumos.txtnumero.value), 0, resumos.txtnumero.value)
 lcestab = IIF(EMPTY(resumos.txtestab.value), -1, resumos.txtestab.value)
 lctipocliente = ''
 lctipocliente = ALLTRIM(IIF(EMPTY(resumos.txttipo.value), '', resumos.txttipo.value))
 lctop = resumos.cmbtop.value
 lcvenda = IIF(EMPTY(resumos.txtvenda.value), 0, resumos.txtvenda.value)
 lcdoc = IIF(ALLTRIM(ucrsresumosdocs.nmdoc)=='Todos', '', ALLTRIM(ucrsresumosdocs.nmdoc))
 lcPlano = IIF (UPPER(ALLTRIM(resumos.ImageCPlano.tag)) == "TRUE",1,0)
 lcSuspensas = ALLTRIM(IIF(EMPTY(resumos.txtsuspensas.value),'',resumos.txtsuspensas.value))
 lcExcluirZero = IIF (UPPER(ALLTRIM(resumos.ImageExcluirZero.tag)) == "TRUE",1,0)
 lcCredito = IIF  (UPPER(ALLTRIM(resumos.imagesocred.tag)) == "TRUE",1,0)
 
 
 IF lcdatafim>DATE() .OR. lcdataini>lcdatafim
    uf_perguntalt_chama("O INTERVALO DE DATAS N�O � V�LIDO.", "OK", "", 64)
    RETURN .F.
 ENDIF
 IF (lcdatafim-lcdataini)>31
    IF  .NOT. uf_perguntalt_chama("SELECCIONOU UM INTERVALO DE DATAS SUPERIORES A 31 DIAS. TEM A CERTEZA QUE QUER CONTINUAR?"+CHR(13)+CHR(13)+"A PESQUISAR DE INTERVALOS DE DATA SUPERIORES A 3 DIAS PODER� AFECTAR O DESEMPENHO DA APLICA��O!", "Sim", "N�o")
       RETURN .F.
    ENDIF
 ENDIF
 resumos.menu1.seltodos.config("Sel. Todos", mypath+"\imagens\icons\unchecked_w.png", "uf_resumos_selTodos with .T.", "F3")
 mntotal = 2
 regua(0, mntotal, "A PESQUISAR...", .F.)
 regua(0, 1, "A PESQUISAR...", .F.)
 TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_touch_reImprimirBulk <<lcVenda>>, '<<lcCliente>>', <<lcNumero>>,<<lcEstab>>, '<<lcTipoCliente>>', '<<lcDoc>>', '<<lcOperador>>', '<<lcTerminal>>', '<<uf_gerais_getdate(lcDataIni,"SQL")>>', '<<uf_gerais_getdate(lcDataFim,"SQL")>>', <<lcTop>>, '<<mySite>>', <<lcPlano>>, '<<lcSuspensas>>', <<lcExcluirZero >>, <<lcCredito>>
 ENDTEXT

 uf_gerais_actgrelha("resumos.grdPesq", "uCrsResumos", lcsql)
 resumos.lblresultados.caption = ALLTRIM(STR(RECCOUNT("uCrsResumos")))+" Resultados"
 regua(0, 2, "A PESQUISAR...", .F.)
 regua(2)
 resumos.menu1.seltodos.config("Sel. Todos", mypath+"\imagens\icons\unchecked_w.png", "uf_resumos_selTodos with .T.", "F3")
 uf_resumos_aplicafiltros()
ENDFUNC
**
PROCEDURE uf_resumos_moverlinha
 LPARAMETERS lcmodo
 SELECT ucrsresumos
 IF lcmodo
    TRY
       SKIP -1
    CATCH
       GOTO TOP
    ENDTRY
 ELSE
    TRY
       SKIP 1
    CATCH
       GOTO BOTTOM
    ENDTRY
 ENDIF
 resumos.grdpesq.setfocus()
ENDPROC
**
FUNCTION uf_resumos_imprimir
 LPARAMETERS tcnum, tcnum2
 LOCAL lcpos, lccountsel, lcvalidaestab0, lcvalidaestabs
 STORE 0 TO lcpos, lccountsel
 STORE .F. TO lcvalidaestab0, lcvalidaestabs
 SELECT ucrsresumos
 lcpos = RECNO()
 IF EMPTY(myposprinter)
    uf_perguntalt_chama("A IMPRESSORA N�O EST� A SER DETECTADA.", "OK", "", 48)
    RETURN .F.
 ENDIF
 SELECT ucrsresumos
 CALCULATE CNT() TO lccountsel FOR ucrsresumos.sel=.T.
 IF (lccountsel=0)
    uf_perguntalt_chama("N�O EXISTE NENHUM ATENDIMENTO SELECCIONADO PARA IMPRIMIR.", "OK", "", 48)
    SELECT ucrsresumos
    TRY
       GOTO lcpos
    CATCH
    ENDTRY
    RETURN .F.
 ENDIF
  
 SELECT ucrsresumos
 LOCATE FOR ucrsresumos.sel=.T.
 lcno = ucrsresumos.no
 lcestab = ucrsresumos.estab
 
  ** NAO VALIDA QUANDO � 2 E 3 PARA PERMITIR enviar mais que um tipo de cliente
 LOCAL lcMostraMsg
 lcMostraMsg = .t.
  
 IF(tcnum==2 and tcnum2==3)	
	lcMostraMsg = .f.
 ELSE
 	IF(uf_resumos_permiteImprimir() == .F.)
     	RETURN .F.
    ENDIF
 ENDIF
 
 SELECT ucrsresumos
 GOTO TOP
 SELECT DISTINCT estab FROM UCRSRESUMOS WHERE estab<>0 AND sel=.T. INTO CURSOR uCrsDistEstab READWRITE
 SELECT ucrsdistestab
 IF RECCOUNT("uCrsDistEstab")<>0
    IF RECCOUNT("uCrsDistEstab")>1
       TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_touch_dadosCliente <<lcNo>> , 0
       ENDTEXT
    ELSE
	       IF lcMostraMsg AND  .NOT. uf_perguntalt_chama("PRETENDE EMITIR A LISTAGEM EM NOME DO ESTABELECIMENTO?", "Sim", "N�o")
	          TEXT TO lcsql TEXTMERGE NOSHOW
						exec up_touch_dadosCliente <<lcNo>> , 0
	          ENDTEXT
	       ELSE
	          SELECT ucrsdistestab
	          TEXT TO lcsql TEXTMERGE NOSHOW
						exec up_touch_dadosCliente <<lcNo>> , <<uCrsDistEstab.estab>>
	          ENDTEXT
	       ENDIF
    ENDIF
 ELSE
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_touch_dadosCliente <<lcNo>> , 0
    ENDTEXT
 ENDIF
 IF  .NOT. uf_gerais_actgrelha("", "uCrsCl1", lcsql) .OR. RECCOUNT("uCrsCl1")=0
    uf_perguntalt_chama("OCORREU UM ERRO A OBTER OS DADOS DO CLIENTE.", "OK", "", 16)
    RETURN .F.
 ENDIF
 SELECT * FROM UCRSRESUMOS WHERE sel=.T. INTO CURSOR uCrsSelVendaImpBulk READWRITE
 SELECT ucrsselvendaimpbulk
 GOTO TOP
 LOCAL i, lcorder
 STORE "" TO lcorder
 FOR i = 1 TO resumos.grdpesq.columncount
    IF resumos.grdpesq.columns(i).header1.fontbold
       lcorder = resumos.grdpesq.columns(i).name
    ENDIF
 ENDFOR
 IF tcnum==2
    PUBLIC pobs
    pobs = ALLTRIM(resumos.txtobs.value)
    DO CASE
       CASE tcnum2=1       	  
          SELECT ucrsselvendaimpbulk
          GOTO TOP
          SET REPORTBEHAVIOR 90
          REPORT FORM ALLTRIM(mypath)+'\analises\resumoFact.frx' TO PRINTER PROMPT NODIALOG PREVIEW
       CASE tcnum2=2
          SELECT ucrsselvendaimpbulk
          GOTO TOP
          REPORT FORM ALLTRIM(mypath)+'\analises\resumoFact.frx' TO PRINTER PROMPT NODIALOG
          resumos.backcolor = RGB(254, 254, 254)
          resumos.refresh
          resumos.backcolor = RGB(255, 255, 255)
          resumos.refresh
        CASE tcnum2==3
 			IF  .NOT. uf_perguntalt_chama("Pretende separar o resumo por cliente?", "Sim", "N�o")
		          		
		        uf_resumos_frxSimple()
		          	
		 		SELECT ucrsFrxSimpleFinal
		        GOTO TOP
		            
		          uv_fileName = "Resumos_"+ uf_gerais_getdate(date(),"SQL") + SYS(2) +".pdf"
		          uf_guarda_pdf(ALLTRIM(mypath)+'\analises\resumosimple.frx', uv_fileName, .T., '', ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'")))
		 
		          IF USED("ucrsFrxSimpleFinal")
						fecha("ucrsFrxSimpleFinal")
				  ENDIF 
			ELSE
				uf_resumos_frxSimple()
		        LOCAL lcClienteNo
		 		SELECT ucrsFrxSimpleFinal
		        GO TOP 
		        SELECT Distinct no FROM ucrsFrxSimpleFinal INTO CURSOR ucrsFrxSimpleFinalNo READWRITE  
		        SELECT ucrsFrxSimpleFinalNo
		        SCAN
		        	SELECT ucrsFrxSimpleFinal
		        	GO TOP 
		        	SET Filter to  ucrsFrxSimpleFinal.No = ucrsFrxSimpleFinalNo.No
		        	uv_fileName = "Resumos_" +ALLTRIM(STR(ucrsFrxSimpleFinalNo.No)) +"_"+ uf_gerais_getdate(date(),"SQL") + SYS(2) +".pdf"
		          	uf_guarda_pdf(ALLTRIM(mypath)+'\analises\resumosimple.frx', uv_fileName, .T., '', ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'")))
		 			
		        	SELECT ucrsFrxSimpleFinal
		        	GO TOP 
		        	SET Filter to  1 = 1
		        ENDSCAN
		        IF USED("ucrsFrxSimpleFinalNo")
						fecha("ucrsFrxSimpleFinal")
				  ENDIF 
		        IF USED("ucrsFrxSimpleFinal")
						fecha("ucrsFrxSimpleFinal")
				  ENDIF 
			ENDIF   
			  	  		   		  
       OTHERWISE
          RETURN .F.
    ENDCASE
 ELSE
    IF tcnum==3
       PUBLIC pobs
       pobs = ALLTRIM(resumos.txtobs.value)
       DO CASE
          CASE tcnum2=1
             SELECT ucrsselvendaimpbulk
             GOTO TOP
             SET REPORTBEHAVIOR 90
             REPORT FORM ALLTRIM(mypath)+'\analises\resumoFactUtente.frx' TO PRINTER PROMPT NODIALOG PREVIEW
          CASE tcnum2=2
             SELECT ucrsselvendaimpbulk
             GOTO TOP
             REPORT FORM ALLTRIM(mypath)+'\analises\resumoFactUtente.frx' TO PRINTER PROMPT NODIALOG
             resumos.backcolor = RGB(254, 254, 254)
             resumos.refresh
             resumos.backcolor = RGB(255, 255, 255)
             resumos.refresh
          OTHERWISE
             RETURN .F.
       ENDCASE
    ENDIF
 ENDIF
 IF tcnum==1 .AND. uf_gerais_getparameter("ADM0000000126", "BOOL")
    uf_perguntalt_chama("ESTA FUNCIONALIDADE N�O � SUPORTADA PARA O TAL�O A6.", "OK", "", 48)
    RETURN .F.
 ENDIF
 IF tcnum==1
	**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
	IF uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + ALLTRIM(myTerm) + "'")

		uv_eti = 0
		uv_desc = 0
		uv_totIva1 = 0
		uv_totIva2 = 0
		uv_totIva3 = 0
		uv_totIva4 = 0
		uv_totIva5 = 0

		CREATE CURSOR uc_totsResumo ( etiliquido n(14,3), totIva1 n(14,3), totIva2 n(14,3), totIva3 n(14,3), totIva4 n(14,3), totIva5 n(14,3), desc n(14,3), lastStamp c(25))

		SELECT ucrsselvendaimpbulk
		GO TOP
		SCAN

			uv_eti = uv_eti + ucrsselvendaimpbulk.etiliquido
			uv_desc = uv_desc + ucrsselvendaimpbulk.descvalor
			IF ucrsselvendaimpbulk.tabiva=1
				uv_totIva1 = uv_totIva1 + (ucrsselvendaimpbulk.etiliquido/(ucrsselvendaimpbulk.iva/100+1))
			ENDIF
			IF ucrsselvendaimpbulk.tabiva=2
				uv_totIva2 = uv_totIva2 + (ucrsselvendaimpbulk.etiliquido/(ucrsselvendaimpbulk.iva/100+1))
			ENDIF
			IF ucrsselvendaimpbulk.tabiva=3
				uv_totIva3 = uv_totIva3 + (ucrsselvendaimpbulk.etiliquido/(ucrsselvendaimpbulk.iva/100+1))
			ENDIF
			IF ucrsselvendaimpbulk.tabiva=4
				uv_totIva4 = uv_totIva4 + (ucrsselvendaimpbulk.etiliquido/(ucrsselvendaimpbulk.iva/100+1))
			ENDIF
			IF ucrsselvendaimpbulk.tabiva=5
				uv_totIva5 = uv_totIva5 + (ucrsselvendaimpbulk.etiliquido/(ucrsselvendaimpbulk.iva/100+1))
			ENDIF

			SELECT ucrsselvendaimpbulk
		ENDSCAN

		SELECT ucrsselvendaimpbulk
		GO BOTTOM

		SELECT uc_totsResumo
		APPEND BLANK

		REPLACE uc_totsResumo.etiliquido WITH uv_eti 
		REPLACE uc_totsResumo.desc WITH uv_desc 
		REPLACE uc_totsResumo.totIva1 WITH uv_totIva1
		REPLACE uc_totsResumo.totIva2 WITH uv_totIva2
		REPLACE uc_totsResumo.totIva3 WITH uv_totIva3
		REPLACE uc_totsResumo.totIva4 WITH uv_totIva4
		REPLACE uc_totsResumo.totIva5 WITH uv_totIva5
		REPLACE uc_totsResumo.lastStamp WITH ucrsselvendaimpbulk.ftstamp

		SELECT uc_totsResumo
		GO TOP

		uv_id = uf_gerais_getUmValor("B_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Resumos' ")

		uf_imprimirgerais_sendprinter_talao(uv_id , .f., .F.)

	ELSE
		IF  .NOT. (uf_gerais_setimpressorapos(.T.))
		SELECT ucrsresumos
		TRY
			GOTO lcpos
		CATCH
		ENDTRY
		RETURN .F.
		ENDIF
		SET HOURS TO 24
		atime = TTOC(DATETIME()+(difhoraria*3600), 2)
		astr = LEFT(atime, 8)
		uf_gerais_criarcabecalhopos()
		uf_gerais_textovermpos()
		? PADC("** DOCUMENTO SEM VALIDADE FISCAL **", uf_gerais_getparameter("ADM0000000194", "NUM")-15, " ")
		? PADC("** NAO VALIDO PARA FACTURA **", uf_gerais_getparameter("ADM0000000194", "NUM")-15, " ")
		uf_gerais_resetcorpos()
		uf_gerais_separadorpos()
		SELECT ucrscl1
		? "Cliente: "
		?? ALLTRIM(ucrscl1.nome)+IIF(ucrscl1.no=200, '', ' ('+ALLTRIM(STR(ucrscl1.no))+')('+ALLTRIM(STR(ucrscl1.estab))+')')
		? "Morada: "
		?? LEFT(ALLTRIM(ucrscl1.morada), uf_gerais_getparameter("ADM0000000194", "NUM"))
		? "Contrib. Nr.: "
		IF (ucrscl1.ncont<>"000000000")
		?? TRANSFORM(ucrscl1.ncont, "99999999999999999999")
		ENDIF
		?? "  Tef.: "
		?? SUBSTR(ucrscl1.telefone, 1, 9)
		uf_gerais_separadorpos()
		? 'Operador: '+ALLTRIM(m_chnome)
		? "Data: "+DTOC(DATE())+" "+astr
		uf_gerais_separadorpos()
		? "Produto"
		IF uf_gerais_getparameter("ADM0000000194", "NUM")>=50
		? "   PVP.     Pref.     Qtd.    Comp.   Total    IVA"
		ELSE
		? "PVP.  | Pref. | Qt. | Comp. | Total |IVA"
		ENDIF
		uf_gerais_separadorpos()
		LOCAL lcndoc, lcfno, lcndoc2, lcfno2
		STORE "" TO lcndoc, lcndoc2
		STORE 0 TO lcfno, lcfno2
		LOCAL lctotdesc, lctotdoc, lctotqtt, lceivain1, lceivain2, lceivain3, lceivain4, lceivain5
		STORE 0 TO lctotdesc, lctotdoc, lctotqtt, lceivain1, lceivain2, lceivain3, lceivain4, lceivain5
		SELECT ucrsselvendaimpbulk
		GOTO TOP
		CALCULATE SUM(ucrsselvendaimpbulk.descvalor) TO lctotdesc 
		SELECT ucrsselvendaimpbulk
		GOTO TOP
		SCAN
		lcndoc2 = ucrsselvendaimpbulk.nmdoc
		lcfno2 = ucrsselvendaimpbulk.fno
		IF lcndoc<>lcndoc2 .OR. lcfno<>lcfno2
			uf_gerais_textovermpos()
			? ALLTRIM(ucrsselvendaimpbulk.nmdoc)+' Nr. '+astr(ucrsselvendaimpbulk.fno)
			uf_gerais_resetcorpos()
			uf_gerais_separadorpos()
		ENDIF
		lcndoc = ucrsselvendaimpbulk.nmdoc
		lcfno = ucrsselvendaimpbulk.fno
		? SUBSTR(ucrsselvendaimpbulk.design, 1, 50)
		IF uf_gerais_getparameter("ADM0000000194", "NUM")>=50
			? TRANSFORM(ucrsselvendaimpbulk.u_epvp, "999999999.99")+" "
			?? TRANSFORM(ucrsselvendaimpbulk.u_epref, "9999999999.99")+"   "
			?? TRANSFORM(ucrsselvendaimpbulk.qtt, "9999.9")
			?? TRANSFORM(ucrsselvendaimpbulk.u_ettent1+ucrsselvendaimpbulk.u_ettent2, "999999999.99")
			?? TRANSFORM(ucrsselvendaimpbulk.etiliquido, "9999999999.99")+"  "
			?? TRANSFORM(ucrsselvendaimpbulk.iva, "9999")+"%"
		ELSE
			? PADC(IIF(OCCURS(".", astr(ROUND(ucrsselvendaimpbulk.u_epvp, 2)))>0, astr(ROUND(ucrsselvendaimpbulk.u_epvp, 2)), astr(ROUND(ucrsselvendaimpbulk.u_epvp, 2))+".00"), 6, " ")+"|"
			?? PADC(IIF(OCCURS(".", astr(ROUND(ucrsselvendaimpbulk.u_epref, 2)))>0, astr(ROUND(ucrsselvendaimpbulk.u_epref, 2)), astr(ROUND(ucrsselvendaimpbulk.u_epref, 2))+".00"), 7, " ")+"|"
			?? PADC(astr(ROUND(ucrsselvendaimpbulk.qtt, 0)), 5, " ")+"|"
			?? PADC(IIF(OCCURS(".", astr(ROUND(ucrsselvendaimpbulk.u_ettent1+ucrsselvendaimpbulk.u_ettent2, 2)))>0, astr(ROUND(ucrsselvendaimpbulk.u_ettent1+ucrsselvendaimpbulk.u_ettent2, 2)), astr(ROUND(ucrsselvendaimpbulk.u_ettent1+ucrsselvendaimpbulk.u_ettent2, 2))+".00"), 7, " ")+"|"
			?? PADC(IIF(OCCURS(".", astr(ROUND(ucrsselvendaimpbulk.etiliquido, 2)))>0, astr(ROUND(ucrsselvendaimpbulk.etiliquido, 2)), astr(ROUND(ucrsselvendaimpbulk.etiliquido, 2))+".00"), 7, " ")+"|"
			?? PADC(astr(ucrsselvendaimpbulk.iva)+"%", 3, " ")
		ENDIF
		IF ucrsselvendaimpbulk.tabiva=1
			lceivain1 = lceivain1+(ucrsselvendaimpbulk.etiliquido/(ucrsselvendaimpbulk.iva/100+1))
		ELSE
			IF ucrsselvendaimpbulk.tabiva=2
				lceivain2 = lceivain2+(ucrsselvendaimpbulk.etiliquido/(ucrsselvendaimpbulk.iva/100+1))
			ELSE
				IF ucrsselvendaimpbulk.tabiva=3
					lceivain3 = lceivain3+(ucrsselvendaimpbulk.etiliquido/(ucrsselvendaimpbulk.iva/100+1))
				ELSE
					IF ucrsselvendaimpbulk.tabiva=4
					lceivain4 = lceivain4+(ucrsselvendaimpbulk.etiliquido/(ucrsselvendaimpbulk.iva/100+1))
					ELSE
					IF ucrsselvendaimpbulk.tabiva=5
						lceivain5 = lceivain5+(ucrsselvendaimpbulk.etiliquido/(ucrsselvendaimpbulk.iva/100+1))
					ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		lctotqtt = lctotqtt+ucrsselvendaimpbulk.qtt
		lctotdoc = lctotdoc+ucrsselvendaimpbulk.etiliquido
		ENDSCAN
		uf_gerais_separadorpos()
		uf_gerais_textovermpos()
		IF lctotdesc<>0
		? "Total Descontos:"
		?? TRANSFORM(lctotdesc, "99999999999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(213)
		ENDIF
		? "Total Documento:"
		?? TRANSFORM(lctotdoc, "99999999999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(213)
		uf_gerais_resetcorpos()
		?
		? "Total por Taxas de IVA:"
		SELECT ucrsselvendaimpbulk
		GOTO TOP
		? "     "+TRANSFORM(ucrsselvendaimpbulk.ivatx1, "99")+"% |     "
		?? TRANSFORM(ucrsselvendaimpbulk.ivatx2, "99")+"% |     "
		?? TRANSFORM(ucrsselvendaimpbulk.ivatx3, "99")+"% |     "
		?? TRANSFORM(ucrsselvendaimpbulk.ivatx4, "99")+"%"
		? TRANSFORM(lceivain1, "99999999999.99")+" |"
		?? TRANSFORM(lceivain2, "99999999999.99")+" |"
		?? TRANSFORM(lceivain3, "99999999999.99")+" |"
		?? TRANSFORM(lceivain4, "99999999999.99")
		?
		uf_gerais_textocjpos()
		uf_gerais_criarrodapepos()
		?
		? "Processado por Computador - IVA INCLUIDO"
		? "Logitools, Lda"
		uf_gerais_feedcutpos()
		uf_gerais_setimpressorapos(.F.)
	ENDIF
 ENDIF
 SELECT ucrsresumos
 TRY
    GOTO lcpos
 CATCH
 ENDTRY
ENDFUNC
**
PROCEDURE uf_resumos_calctotal
 LOCAL lcpos, lctotal
 STORE 0 TO lcpos, lctotal
 SELECT ucrsresumos
 lcpos = RECNO("uCrsResumos")
 SELECT ucrsresumos
 GOTO TOP
 SCAN
    IF ucrsresumos.sel
       lctotal = lctotal+ucrsresumos.ettcl
    ENDIF
 ENDSCAN
 SELECT ucrsresumos
 TRY
    GOTO lcpos
 CATCH
    GOTO TOP
 ENDTRY
 resumos.txttotal.value = ROUND(lctotal, 2)
ENDPROC
**
PROCEDURE uf_resumos_actualizarinfo
 SELECT ucrsresumos
 resumos.txtplano.value = ucrsresumos.u_design
 resumos.txtl.value = ucrsresumos.u_lote
 resumos.txtnr.value = ucrsresumos.u_nslote
 resumos.txtl2.value = ucrsresumos.u_lote2
 resumos.txtnr2.value = ucrsresumos.u_nslote2
 resumos.txtqtt.value = ucrsresumos.qttreg
 resumos.txtvendedor.value = ucrsresumos.vendedor
 resumos.txtterminal.value = ucrsresumos.pnome
ENDPROC
**
PROCEDURE uf_resumos_aplicafiltros

 LOCAL lcfiltro

 lcfiltro = ""

*!*	 IF UPPER(ALLTRIM(resumos.txtsuspensas.value))==UPPER(ALLTRIM("N�o Suspensas"))
*!*	 
*!*	       lcfiltro = ' upper(right(alltrim(uCrsResumos.nmdoc),3)) != "(S)"'
*!*	 
*!*	 ENDIF

*!*	 IF UPPER(ALLTRIM(resumos.txtsuspensas.value))==UPPER(ALLTRIM("Suspensas"))

*!*	       lcfiltro = lcfiltro + IIF(EMPTY(lcFiltro), '', ' AND') + ' upper(right(alltrim(uCrsResumos.nmdoc),3)) == "(S)"'

*!*	 ENDIF

*!*	 IF UPPER(ALLTRIM(resumos.imagesocred.tag)) == "TRUE"

*!*	       lcfiltro = lcfiltro + IIF(EMPTY(lcFiltro), '', ' AND') +' (qtt > eaquisicao)' &&OR valorNreg != 0)'

*!*	 ENDIF

 IF !EMPTY(lcfiltro)
    lcfiltro = lcfiltro + ' ' + 'OR uCrsResumos.sel=.t.'
 ENDIF
 
 SELECT ucrsresumos
 GOTO TOP

 SET FILTER TO &lcfiltro 

 SELECT ucrsresumos
 GOTO TOP

 resumos.grdpesq.refresh
 
ENDPROC
**
FUNCTION uf_resumos_imptalao
 LOCAL lcpainel, lcreimp, lcprinttype, lcadireserva, lcnrlin, lcstampant
 lcpainel = 'REIMP'
 lcreimp = .T.
 lcprinttype = 0
 lcnrlin = 0
 lcstampant = 'AAA'
 SELECT ucrsresumos
 GOTO TOP
 SCAN
    IF ucrsresumos.sel=.T.
       lcnrlin = lcnrlin+1
    ENDIF
 ENDSCAN
 IF lcnrlin=0
    uf_perguntalt_chama("N�O SELECIONOU NENHUM DOCUMENTO PARA IMPRIMIR.", "OK", "", 48)
    RETURN .F.
 ENDIF
 SELECT ucrsresumos
 GOTO TOP
 SCAN FOR ucrsresumos.sel=.T.
    IF ALLTRIM(ucrsresumos.ftstamp)<>ALLTRIM(lcstampant)

		**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
		IF uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + ALLTRIM(myTerm) + "'")

            uv_idImp = uf_gerais_getUmValor("b_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Atendimento'")
            IF !uf_imprimirgerais_createCursTalao(uv_idImp, ucrsresumos.ftstamp, ucrsresumos.ftstamp, '', 'RESUMOS', 0, ucrsresumos.cobrado)
            	uf_perguntalt_chama("ERRO A GERAR CURSORES DO TAL�O." + chr(13) + "POR FAVOR CONTACTE O SUPORTE.", "OK", "", 48)
            	RETURN .F.
            ENDIF
            uf_imprimirgerais_sendprinter_talao(uv_idImp, .T., .F.)

		ELSE
			LOCAL lcvalidaimp, lcisentoiva, lcsql, lcmoeda, lcmotisencaoiva, lcstamp, lcnratend, lcftno
			STORE .F. TO lcvalidaimp, lcisencaoiva
			STORE 'EURO' TO lcmoeda
			STORE '' TO lcnratend
			STORE 0 TO lcftno
			lcstamp = ALLTRIM(ucrsresumos.ftstamp)
			lcmoeda = uf_gerais_getparameter("ADM0000000260", "text")
			IF EMPTY(lcmoeda)
				lcmoeda = 'EURO'
			ENDIF
			LOCAL lcmascaradinheiro
			lcmascaradinheiro = uf_imprimirpos_devolvemascarapvptalao()
			lcmotisencaoiva = uf_gerais_getparameter("ADM0000000261", "Bool")
			STORE 1 TO mynrvendastalao
			IF USED("uCrsFt")
				fecha("uCrsFt")
			ENDIF
			IF USED("uCrsFt2")
				fecha("uCrsFt2")
			ENDIF
			IF USED("uCrsFi")
				fecha("uCrsFi")
			ENDIF
			IF USED("uCrsInfoAtendPagCentral")
				fecha("uCrsInfoAtendPagCentral")
			ENDIF
			
			CREATE CURSOR uCrsStampsQRCode (ftstamp c(50))
			
			uf_gerais_actgrelha("", 'uCrsFt', [exec up_touch_ft ']+ALLTRIM(lcstamp)+['])
			SELECT ucrsft
			uf_gerais_actgrelha("", 'uCrsFt2', [exec up_touch_ft2 ']+ALLTRIM(lcstamp)+['])
			SELECT ucrsft2
			uf_gerais_actgrelha("", 'uCrsFi', [exec up_touch_fi ']+ALLTRIM(lcstamp)+[', '', 0])
			SELECT ucrsfi
			lcsql = ''
			TEXT TO lcsql TEXTMERGE NOSHOW
						select * from B_pagcentral (nolock) WHERE stamp='<<ALLTRIM(lcStamp)>>'
			ENDTEXT
			IF  .NOT. uf_gerais_actgrelha("", "uCrsInfoAtendPagCentral", lcsql)
				uf_perguntalt_chama("N�o foi poss�vel encontrar a informa��o do atendimento. Por favor contacte o Suporte.", "", "OK", 32)
				RETURN .F.
			ENDIF
			SELECT ucrsinfoatendpagcentral
			lctroco = ucrsinfoatendpagcentral.etroco
			lctotal = ucrsinfoatendpagcentral.total_bruto
			lcmodopag1 = ucrsinfoatendpagcentral.evdinheiro_semtroco
			lcmodopag2 = ucrsinfoatendpagcentral.epaga1
			lcmodopag3 = ucrsinfoatendpagcentral.epaga2
			lcmodopag4 = ucrsinfoatendpagcentral.echtotal
			lcmodopag5 = ucrsinfoatendpagcentral.epaga3
			lcmodopag6 = ucrsinfoatendpagcentral.epaga4
			lcmodopag7 = ucrsinfoatendpagcentral.epaga5
			lcmodopag8 = ucrsinfoatendpagcentral.epaga6
			fecha("uCrsInfoAtendPagCentral")
			LOCAL lcmpag1, lcmpag2, lcmpag3, lcmpag4, lcmpag5, lcmpag6, lcmpag7, lcmpag8
			STORE '' TO lcmpag1, lcmpag2, lcmpag3, lcmpag4, lcmpag5, lcmpag6, lcmpag7, lcmpag8
			IF  .NOT. USED("uCrsConfigModPag")
				TEXT TO lcsql TEXTMERGE NOSHOW
							select * from B_modoPag
				ENDTEXT
				IF  .NOT. uf_gerais_actgrelha("", "UcrsConfigModPag", lcsql)
					uf_perguntalt_chama("N�o foi poss�vel verificar as configura��es de Modo de Pagamento. Contacte o suporte.", "", "OK", 32)
					RETURN .F.
				ENDIF
			ENDIF
			SELECT ucrsconfigmodpag
			GOTO TOP
			SCAN
				DO CASE
					CASE ALLTRIM(ucrsconfigmodpag.ref)=="01"
						lcmpag1 = ALLTRIM(ucrsconfigmodpag.design)
					CASE ALLTRIM(ucrsconfigmodpag.ref)=="02"
						lcmpag2 = ALLTRIM(ucrsconfigmodpag.design)
					CASE ALLTRIM(ucrsconfigmodpag.ref)=="03"
						lcmpag3 = ALLTRIM(ucrsconfigmodpag.design)
					CASE ALLTRIM(ucrsconfigmodpag.ref)=="04"
						lcmpag4 = ALLTRIM(ucrsconfigmodpag.design)
					CASE ALLTRIM(ucrsconfigmodpag.ref)=="05"
						lcmpag5 = ALLTRIM(ucrsconfigmodpag.design)
					CASE ALLTRIM(ucrsconfigmodpag.ref)=="06"
						lcmpag6 = ALLTRIM(ucrsconfigmodpag.design)
					CASE ALLTRIM(ucrsconfigmodpag.ref)=="07"
						lcmpag7 = ALLTRIM(ucrsconfigmodpag.design)
					CASE ALLTRIM(ucrsconfigmodpag.ref)=="08"
						lcmpag8 = ALLTRIM(ucrsconfigmodpag.design)
				ENDCASE
			ENDSCAN
			IF mytipocartao=='Valor'
				LOCAL lctotalhistcartao, lcvaldispcartao
				STORE 0 TO lctotalhistcartao, lcvaldispcartao
				SELECT ucrsft
				lcsql = ''
				TEXT TO lcsql TEXTMERGE NOSHOW
							SELECT valcartaohist, valcartao FROM b_fidel WHERE clno=<<ucrsft.no>> and clestab=<<ucrsft.estab>>
				ENDTEXT
				uf_gerais_actgrelha("", "ucrsverifvalcartao", lcsql)
				IF RECCOUNT("ucrsverifvalcartao")>0
					lctotalhistcartao = ucrsverifvalcartao.valcartaohist
					lcvaldispcartao = ucrsverifvalcartao.valcartao
				ENDIF
			ENDIF
			LOCAL lctiposaft
			STORE 0 TO lctiposaft
			TEXT TO lcsql TEXTMERGE NOSHOW
						select tiposaft from td where ndoc=<<uCrsFt.ndoc>>
			ENDTEXT
			uf_gerais_actgrelha("", "uCrsSaftFile", lcsql)
			lctiposaft = ucrssaftfile.tiposaft
			fecha("uCrsSaftFile")
			lcvalidaimp = uf_gerais_setimpressorapos(.T.)
			IF lcvalidaimp
				STORE .F. TO lcisentoiva
				IF UPPER(ALLTRIM(lcpainel))=="ATENDIMENTO"
					IF USED("uCrsFiPromo")
						fecha("uCrsFiPromo")
					ENDIF
					SELECT ucrsfi
					GOTO TOP
					SELECT ref, u_refvale, u_epvp, design FROM uCrsFi WHERE ucrsfi.epromo=.T. AND ALLTRIM(ucrsfi.ftstamp)=ALLTRIM(lcstamp) INTO CURSOR uCrsFiPromo
					SELECT ucrsfipromo
				ENDIF
				LOCAL psico, descutente, doctotalsemdesc
				STORE 0 TO psico, descutente, doctotalsemdesc
				SELECT ucrsfi
				CALCULATE SUM(ucrsfi.epv*ucrsfi.qtt) TO doctotalsemdesc FOR ALLTRIM(ucrsfi.ftstamp)==ALLTRIM(lcstamp)
				SELECT ucrsft
				LOCATE FOR ALLTRIM(ucrsft.ftstamp)=ALLTRIM(lcstamp)
				descutente = doctotalsemdesc-ucrsft.etotal
				uf_gerais_criarcabecalhopos()
				IF lcreimp
					IF uf_gerais_getparameter("ADM0000000209", "BOOL")
						? PADL("2a VIA", uf_gerais_getparameter("ADM0000000194", "NUM"), " ")
					ENDIF
				ELSE
					? PADL("ORIGINAL", uf_gerais_getparameter("ADM0000000194", "NUM"), " ")
				ENDIF
				uf_gerais_textovermpos()
				SELECT ucrsft
				LOCATE FOR ALLTRIM(ucrsft.ftstamp)==ALLTRIM(lcstamp)
				IF UPPER(ALLTRIM(ucrse1.pais))=='PORTUGAL'
					IF (ucrsft.u_tipodoc==8 .OR. ucrsft.ndoc==84)
						IF YEAR(ucrsft.fdata)>2012
						? "Fatura-Recibo Nr."
						ELSE
						? "Vnd. a Dinheiro Nr."
						ENDIF
						?? TRANSFORM(ucrsft.fno, "999999999")+" "
					ENDIF
					IF (ucrsft.u_tipodoc==9) .OR. (ucrsft.ndoc==11) .OR. (ucrsft.ndoc==12)
						? "Fatura Nr."
						?? TRANSFORM(ucrsft.fno, "999999999")+" "
					ENDIF
					IF (ucrsft.u_tipodoc==2 .OR. ucrsft.u_tipodoc==7)
						? "Nt. Credito Nr."
						?? TRANSFORM(ucrsft.fno, "999999999")+" "
					ENDIF
					IF (ucrsft.u_tipodoc==4)
						? "Fatura Acordo Nr."
						?? TRANSFORM(ucrsft.fno, "999999999")+" "
					ENDIF
					IF (ucrsft.u_tipodoc=8 .OR. ucrsft.ndoc=84) .AND. ucrsft.u_lote=0 .AND. ucrsft.cobrado=.F.
						?? "(VD)"
					ENDIF
					IF (ucrsft.u_tipodoc=8 .OR. ucrsft.ndoc=84) .AND. ucrsft.u_lote=0 .AND. ucrsft.cobrado=.T.
						?? "(VD)(S)"
					ENDIF
					IF (ucrsft.u_tipodoc=8 .OR. ucrsft.ndoc=84) .AND. ucrsft.u_lote>0
						?? "(VD-CR)"
					ENDIF
					IF (ucrsft.u_tipodoc=9) .AND. ucrsft.cobrado=.F. .AND. ucrsft.u_lote=0
						?? "(FT)"
					ENDIF
					IF (ucrsft.u_tipodoc=9) .AND. ucrsft.cobrado=.T. .AND. ucrsft.u_lote=0
						?? "(FT)(S)"
					ENDIF
					IF (ucrsft.u_tipodoc=9) .AND. ucrsft.u_lote>0
						?? "(FT-CR)"
					ENDIF
					IF (ucrsft.u_tipodoc=2 .OR. ucrsft.u_tipodoc=7)
						?? "(RC)"
					ENDIF
					IF (ucrsft.u_tipodoc=4) .AND. ucrsft.cobrado=.F. .AND. ucrsft.u_lote=0
						?? "(FA)"
					ENDIF
					IF (ucrsft.u_tipodoc=4) .AND. ucrsft.cobrado=.T. .AND. ucrsft.u_lote=0
						?? "(FA)(S)"
					ENDIF
					IF (ucrsft.u_tipodoc=4) .AND. ucrsft.u_lote>0
						?? "(FA-CR)"
					ENDIF
					?? "/"+astr(ucrsft.ndoc)
				ELSE
					SELECT ucrsft
					? ALLTRIM(ucrsft.nmdoc)+' Nr. '+ALLTRIM(lctiposaft)+' '+ALLTRIM(STR(ucrsft.ndoc))+'/'+ALLTRIM(STR(ucrsft.fno))
				ENDIF
				? "Data: "
				?? uf_gerais_getdate(ucrsft.fdata, "DATA")
				?? "      Hora: "
				?? SUBSTR(ucrsft.ousrhora, 1, 5)
				uf_gerais_resetcorpos()
				IF ucrsft.cobrado
					IF uf_gerais_getparameter("ADM0000000014", "NUM")>0
						uf_gerais_resetcorpos()
						uf_gerais_separadorpos()
						uf_gerais_textovermpos()
						? "Prazo de Regularizacao: "+ALLTRIM(STR(uf_gerais_getparameter("ADM0000000014", "NUM")))+" Dias"
					ENDIF
				ENDIF
				SELECT ucrsft
				IF ucrsft.u_tipodoc==3 .OR. ucrsft.u_tipodoc==4
					? "*** Talao sem Validade Fiscal ***"
				ENDIF
				uf_gerais_resetcorpos()
				uf_gerais_separadorpos()
				? "Cliente: "
				?? ALLTRIM(ucrsft.nome)+IIF(ucrsft.no==200, '', ' ('+astr(ucrsft.no)+')('+astr(ucrsft.estab)+')')
				? "Morada: "
				?? ALLTRIM(ucrsft.morada)
				? "Cod.Post.: "
				?? LEFT(ALLTRIM(ucrsft.codpost), uf_gerais_getparameter("ADM0000000194", "NUM")-11)
				? "Contrib. Nr.: "
				IF (ALLTRIM(ucrsft.ncont)<>"999999990") .AND.  .NOT. EMPTY(ALLTRIM(ucrsft.ncont)) .AND. (ALLTRIM(ucrsft.ncont)<>"9999999999")
					?? TRANSFORM(ucrsft.ncont, "99999999999999999999")
				ELSE
					?? ALLTRIM(uf_gerais_getparameter_site('ADM0000000075', 'text', mysite))
					IF UPPER(ALLTRIM(ucrse1.pais))=='PORTUGAL'
						? "Nao sujeito passivo de IVA "
					ELSE
						? " "
					ENDIF
				ENDIF
				? "Tlf.: "
				?? SUBSTR(ucrsft.telefone, 1, 9)
				SELECT ucrse1
				GOTO TOP
				IF ALLTRIM(ucrse1.tipoempresa)=='FARMACIA'
					? "N.Utente: "
					?? IIF(EMPTY(ALLTRIM(ucrsft2.u_nbenef)), "         ", ALLTRIM(ucrsft2.u_nbenef))
					?? " N.Benef.: "
					?? ALLTRIM(ucrsft2.u_nbenef2)
				ENDIF
				SELECT ucrsft2
				LOCATE FOR ALLTRIM(ucrsft2.ft2stamp)=ALLTRIM(lcstamp)
				IF ucrsft2.cativa .AND. uf_gerais_validaivadireitodeducao()==0
					? "Sujeito a cativa��o de IVA."
				ENDIF
				IF ucrsft2.u_vddom
					uf_gerais_separadorpos()
					? "Entrega ao Domicilio: "
					? "Transportador: "+LEFT(ALLTRIM(ucrsft2.contacto), uf_gerais_getparameter("ADM0000000194", "NUM")-15)
					? "Morada: "+ALLTRIM(ucrsft2.morada)
					? "Cod.Post: "+TRANSFORM(ucrsft2.codpost, "########")
					?? "  Local.: "+TRANSFORM(ucrsft2.local, "#################")
					? "Tel.: "+TRANSFORM(ucrsft2.telefone, "##############")
					?? "Email: "+SUBSTR(ucrsft2.email, 1, 25)
					IF  .NOT. EMPTY(ucrsft2.u_viatura)
						? "Viatura: "+TRANSFORM(ucrsft2.u_viatura, "#########################")
						?? "Hora: "+SUBSTR(ucrsft2.horaentrega, 1, 5)
					ENDIF
				ENDIF
				LOCAL lctamanhodesign
				IF  .NOT. EMPTY(ucrsft2.u_codigo)
					uf_gerais_separadorpos()
					IF lcmotisencaoiva==.T. .AND. uf_gerais_validaivadireitodeducao()==0
						? "Produto"
						IF uf_gerais_getparameter("ADM0000000194", "NUM")>=50
						? "   PVP.     Pref.     Qtd.    Comp.   Total    IVA"
						ELSE
						? "PVP.  | Pref. | Qt. | Comp. | Total |IVA"
						ENDIF
					ELSE
						? "Produto"
						IF uf_gerais_getparameter("ADM0000000194", "NUM")>=50
						? "   PVP.     Pref.     Qtd.    Comp.   Total"
						ELSE
						? "PVP.  | Pref. | Qt. | Comp. | Total"
						ENDIF
					ENDIF
					SELECT ucrsfi
					SCAN FOR ALLTRIM(ucrsfi.ftstamp)=ALLTRIM(lcstamp) .AND. ucrsfi.epromo=.F.
						lctamanhodesign = uf_gerais_getparameter("ADM0000000194", "NUM")-IIF(UPPER(ALLTRIM(ucrsfi.lobs2))=='REF.VD.ORIG.', 2, 0)-IIF(ucrsfi.iva==0 .AND.  .NOT. (UPPER(ALLTRIM(ucrsfi.lobs2))=='REF.VD.ORIG.'), 5, 0)
						? IIF(UPPER(ALLTRIM(ucrsfi.lobs2))=='REF.VD.ORIG.', '  ', '')+LEFT(ALLTRIM(ucrsfi.design), lctamanhodesign)+IIF(ucrsfi.iva==0 .AND.  .NOT. (UPPER(ALLTRIM(ucrsfi.lobs2))=='REF.VD.ORIG.') .AND. ALLTRIM(ucrsfi.ref)<>'V000001', ' (*1)', '')
						IF uf_gerais_getparameter("ADM0000000194", "NUM")>=50
						IF  .NOT. (UPPER(ALLTRIM(ucrsfi.lobs2))=='REF.VD.ORIG.')
							? TRANSFORM(ucrsfi.u_epvp, lcmascaradinheiro)+" "
							?? TRANSFORM(ucrsfi.u_epref, lcmascaradinheiro)+"   "
							?? TRANSFORM(ucrsfi.qtt, "9999.9")+"     "
							?? TRANSFORM(ucrsfi.u_ettent1+ucrsfi.u_ettent2, lcmascaradinheiro)
							?? TRANSFORM(ucrsfi.etiliquido, lcmascaradinheiro)+" "
							IF lcmotisencaoiva==.T. .AND. uf_gerais_validaivadireitodeducao()==0
								?? TRANSFORM(ucrsfi.iva, "9999")+"%"
							ENDIF
							IF ucrsfi.iva=0 .AND. uf_gerais_validaivadireitodeducao()==0
								IF TYPE("uCrsFi.motisencao")<>'U'
									? ALLTRIM(ucrsfi.motisencao)
								ENDIF
							ENDIF
						ENDIF
						ELSE
						IF  .NOT. (UPPER(ALLTRIM(ucrsfi.lobs2))=='REF.VD.ORIG.')
							? PADC(IIF(OCCURS(".", astr(ROUND(ucrsfi.u_epvp, 2), 8, 2))>0, astr(ROUND(ucrsfi.u_epvp, 2), 8, 2), astr(ROUND(ucrsfi.u_epvp, 2), 8, 2)+".00"), 6, " ")+"|"
							?? PADC(IIF(OCCURS(".", astr(ROUND(ucrsfi.u_epref, 2), 8, 2))>0, astr(ROUND(ucrsfi.u_epref, 2), 8, 2), astr(ROUND(ucrsfi.u_epref, 2), 8, 2)+".00"), 7, " ")+"|"
							?? PADC(astr(ROUND(ucrsfi.qtt, 0), 8, 2), 5, " ")+"|"
							?? PADC(IIF(OCCURS(".", astr(ROUND(ucrsfi.u_ettent1+ucrsfi.u_ettent2, 2), 8, 2))>0, astr(ROUND(ucrsfi.u_ettent1+ucrsfi.u_ettent2, 2), 8, 2), astr(ROUND(ucrsfi.u_ettent1+ucrsfi.u_ettent2, 2), 8, 2)+".00"), 7, " ")+"|"
							?? PADC(IIF(OCCURS(".", astr(ROUND(ucrsfi.etiliquido, 2), 8, 2))>0, astr(ROUND(ucrsfi.etiliquido, 2), 8, 2), astr(ROUND(ucrsfi.etiliquido, 2), 8, 2)+".00"), 7, " ")+"|"
							IF lcmotisencaoiva==.T. .AND. uf_gerais_validaivadireitodeducao()==0
								?? PADC(astr(ucrsfi.iva)+"%", 3, " ")
							ENDIF
							IF ucrsfi.iva=0 .AND. uf_gerais_validaivadireitodeducao()==0
								IF TYPE("uCrsFi.motisencao")<>'U'
									? ALLTRIM(ucrsfi.motisencao)
								ENDIF
							ENDIF
						ENDIF
						ENDIF
						IF ucrsfi.iva==0 .AND.  .NOT. (UPPER(ALLTRIM(ucrsfi.lobs2))=='REF.VD.ORIG.')
						lcisentoiva = .T.
						ENDIF
					ENDSCAN
				ENDIF
				IF EMPTY(ucrsft2.u_codigo)
					uf_gerais_separadorpos()
					IF lcmotisencaoiva==.T. .AND. uf_gerais_validaivadireitodeducao()==0
						? "Produto"
						IF uf_gerais_getparameter("ADM0000000194", "NUM")>=45 .AND. uf_gerais_validaivadireitodeducao()==0
						? "     PVP.      Qtd.          Total        IVA"
						ELSE
						? " PVP.   |   Qtd.   |   Total   |   IVA  "
						ENDIF
					ELSE
						? "Produto"
						IF uf_gerais_getparameter("ADM0000000194", "NUM")>=45
						? "     PVP.      Qtd.          Total"
						ELSE
						? " PVP.   |   Qtd.   |   Total"
						ENDIF
					ENDIF
					SELECT ucrsfi
					SCAN FOR ALLTRIM(ucrsfi.ftstamp)=ALLTRIM(lcstamp) .AND. ucrsfi.epromo=.F.
						lctamanhodesign = uf_gerais_getparameter("ADM0000000194", "NUM")-IIF(UPPER(ALLTRIM(ucrsfi.lobs2))=='REF.VD.ORIG.', 2, 0)-IIF(ucrsfi.iva==0 .AND.  .NOT. (UPPER(ALLTRIM(ucrsfi.lobs2))=='REF.VD.ORIG.'), 5, 0)
						? IIF(UPPER(ALLTRIM(ucrsfi.lobs2))=='REF.VD.ORIG.', '  ', '')+LEFT(ALLTRIM(ucrsfi.design), lctamanhodesign)+IIF(ucrsfi.iva==0 .AND.  .NOT. (UPPER(ALLTRIM(ucrsfi.lobs2))=='REF.VD.ORIG.') .AND. ALLTRIM(ucrsfi.ref)<>'V000001', ' (*1)', '')
						IF uf_gerais_getparameter("ADM0000000194", "NUM")>=45
						IF  .NOT. (UPPER(ALLTRIM(ucrsfi.lobs2))=='REF.VD.ORIG.')
							? TRANSFORM(ucrsfi.epv, lcmascaradinheiro)+"   "
							?? TRANSFORM(ucrsfi.qtt, "99999.9")+"     "
							?? TRANSFORM(ucrsfi.etiliquido, lcmascaradinheiro)+"    "
							IF lcmotisencaoiva==.T. .AND. uf_gerais_validaivadireitodeducao()==0
								?? TRANSFORM(ucrsfi.iva, "9999")+"%"
							ENDIF
							IF ucrsfi.iva=0 .AND. uf_gerais_validaivadireitodeducao()==0
								IF TYPE("uCrsFi.motisencao")<>'U'
									? ALLTRIM(ucrsfi.motisencao)
								ENDIF
							ENDIF
						ENDIF
						ELSE
						IF  .NOT. (UPPER(ALLTRIM(ucrsfi.lobs2))=='REF.VD.ORIG.')
							? PADC(IIF(OCCURS(".", astr(ROUND(ucrsfi.epv, 2), 8, 2))>0, astr(ROUND(ucrsfi.epv, 2), 8, 2), astr(ROUND(ucrsfi.epv, 2), 8, 2)+".00"), 8, " ")+"|"
							?? PADC(astr(ROUND(ucrsfi.qtt, 2), 8, 2), 10, " ")+"|"
							?? PADC(IIF(OCCURS(".", astr(ROUND(ucrsfi.etiliquido, 2), 8, 2))>0, astr(ROUND(ucrsfi.etiliquido, 2), 8, 2), astr(ROUND(ucrsfi.etiliquido, 2), 8, 2)+".00"), 11, " ")+"|"
							IF lcmotisencaoiva==.T. .AND. uf_gerais_validaivadireitodeducao()==0
								?? PADC(astr(ucrsfi.iva)+"%", 8, " ")
							ENDIF
							IF ucrsfi.iva=0 .AND. uf_gerais_validaivadireitodeducao()==0
								IF TYPE("uCrsFi.motisencao")<>'U'
									? ALLTRIM(ucrsfi.motisencao)
								ENDIF
							ENDIF
						ENDIF
						ENDIF
						IF ucrsfi.iva==0 .AND.  .NOT. (UPPER(ALLTRIM(ucrsfi.lobs2))=='REF.VD.ORIG.')
						lcisentoiva = .T.
						ENDIF
					ENDSCAN
				ENDIF
				uf_gerais_separadorpos()
				uf_gerais_textovermpos()
				IF UPPER(ALLTRIM(lcmoeda))=='EURO'
					SELECT ucrsft
					LOCATE FOR ALLTRIM(ucrsft.ftstamp)=ALLTRIM(lcstamp)
					IF ucrsft.edescc>0
						? "TOTAL SEM DESCONTOS:"
						?? TRANSFORM(doctotalsemdesc, "999999999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(213)
						? "Descontos:          "
						?? TRANSFORM(descutente, "999999999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(213)
					ENDIF
				ELSE
					SELECT ucrsft
					LOCATE FOR ALLTRIM(ucrsft.ftstamp)=ALLTRIM(lcstamp)
					IF ucrsft.edescc>0
						IF uf_gerais_getparameter("ADM0000000260", "text")='USD'
						? "TOTAL SEM DESCONTOS:"
						?? TRANSFORM(doctotalsemdesc, "999999999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(36)
						? "Descontos:          "
						?? TRANSFORM(descutente, "999999999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(36)
						ELSE
						? "TOTAL SEM DESCONTOS:"
						?? TRANSFORM(doctotalsemdesc, "999999999999.99 ")+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
						? "Descontos:          "
						?? TRANSFORM(descutente, "999999999999.99 ")+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
						ENDIF
					ENDIF
				ENDIF
				IF UPPER(ALLTRIM(lcmoeda))=='EURO'
					IF lcprinttype<>myregcli .AND. lcprinttype<>85
						? "TOTAL DOCUMENTO:    "
						?? TRANSFORM(ucrsft.etotal, "999999999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(213)
					ELSE
						? "VALOR A PAGAR:"
						?? TRANSFORM(ucrsft.etotal, "999999999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(213)
					ENDIF
				ELSE
					IF uf_gerais_getparameter("ADM0000000260", "text")='USD'
						IF lcprinttype<>myregcli .AND. lcprinttype<>85
						? "TOTAL DOCUMENTO:    "
						?? TRANSFORM(ucrsft.etotal, "999999999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(36)
						ELSE
						? "VALOR A PAGAR:"
						?? TRANSFORM(ucrsft.etotal, "999999999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(36)
						ENDIF
					ELSE
						IF lcprinttype<>myregcli .AND. lcprinttype<>85
						? "TOTAL DOCUMENTO:    "
						?? TRANSFORM(ucrsft.etotal, "999999999999.99 ")+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
						ELSE
						? "VALOR A PAGAR:"
						?? TRANSFORM(ucrsft.etotal, "999999999999.99 ")+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
						ENDIF
					ENDIF
				ENDIF
				IF UPPER(ALLTRIM(lcmoeda))=='EURO'
					IF lcmodopag1>0
						? SUBSTR(lcmpag1, 1, 20)+":		  	     "
						?? TRANSFORM(lcmodopag1, "99999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(213)
					ENDIF
					IF lcmodopag2>0
						? SUBSTR(lcmpag2, 1, 20)+":			     "
						?? TRANSFORM(lcmodopag2, "99999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(213)
					ENDIF
					IF lcmodopag3>0
						? SUBSTR(lcmpag3, 1, 20)+":			  	     "
						?? TRANSFORM(lcmodopag3, "99999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(213)
					ENDIF
					IF lcmodopag4>0
						? SUBSTR(lcmpag4, 1, 20)+":		  	     "
						?? TRANSFORM(lcmodopag4, "99999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(213)
					ENDIF
					IF lcmodopag5>0
						? SUBSTR(lcmpag5, 1, 20)+":			  	     "
						?? TRANSFORM(lcmodopag5, "99999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(213)
					ENDIF
					IF lcmodopag6>0
						? SUBSTR(lcmpag6, 1, 20)+":			     "
						?? TRANSFORM(lcmodopag6, "99999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(213)
					ENDIF
					IF lcmodopag7>0
						? SUBSTR(lcmpag7, 1, 20)+":			     "
						?? TRANSFORM(lcmodopag7, "99999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(213)
					ENDIF
					IF lcmodopag8>0
						? SUBSTR(lcmpag8, 1, 20)+":			     "
						?? TRANSFORM(lcmodopag8, "99999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(213)
					ENDIF
					IF lctroco>0
						? "TROCO:				     "
						?? TRANSFORM(lctroco, "99999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(213)
					ENDIF
				ELSE
					IF uf_gerais_getparameter("ADM0000000260", "text")='USD'
						IF lcmodopag1>0
						? SUBSTR(lcmpag1, 1, 20)+":	"
						?? TRANSFORM(lcmodopag1, "99999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(36)
						ENDIF
						IF lcmodopag2>0
						? SUBSTR(lcmpag2, 1, 20)+":	"
						?? TRANSFORM(lcmodopag2, "99999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(36)
						ENDIF
						IF lcmodopag3>0
						? SUBSTR(lcmpag3, 1, 20)+":	"
						?? TRANSFORM(lcmodopag3, "99999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(36)
						ENDIF
						IF lcmodopag4>0
						? SUBSTR(lcmpag4, 1, 20)+":	"
						?? TRANSFORM(lcmodopag4, "99999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(36)
						ENDIF
						IF lcmodopag5>0
						? SUBSTR(lcmpag5, 1, 20)+":	"
						?? TRANSFORM(lcmodopag5, "99999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(36)
						ENDIF
						IF lcmodopag6>0
						? SUBSTR(lcmpag6, 1, 20)+":	"
						?? TRANSFORM(lcmodopag6, "99999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(36)
						ENDIF
						IF lcmodopag7>0
						? SUBSTR(lcmpag7, 1, 20)+":	"
						?? TRANSFORM(lcmodopag7, "99999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(36)
						ENDIF
						IF lcmodopag8>0
						? SUBSTR(lcmpag8, 1, 20)+":	"
						?? TRANSFORM(lcmodopag8, "99999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(36)
						ENDIF
						IF lctroco>0
						? "TROCO:	"
						?? TRANSFORM(lctroco, "99999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(36)
						ENDIF
					ELSE
						IF lcmodopag1>0
						? SUBSTR(lcmpag1, 1, 20)+":	"
						?? TRANSFORM(lcmodopag1, "99999999.99 ")+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
						ENDIF
						IF lcmodopag2>0
						? SUBSTR(lcmpag2, 1, 20)+":	"
						?? TRANSFORM(lcmodopag2, "99999999.99 ")+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
						ENDIF
						IF lcmodopag3>0
						? SUBSTR(lcmpag3, 1, 20)+":	"
						?? TRANSFORM(lcmodopag3, "99999999.99 ")+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
						ENDIF
						IF lcmodopag4>0
						? SUBSTR(lcmpag4, 1, 20)+":	"
						?? TRANSFORM(lcmodopag4, "99999999.99 ")+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
						ENDIF
						IF lcmodopag5>0
						? SUBSTR(lcmpag5, 1, 20)+":	"
						?? TRANSFORM(lcmodopag5, "99999999.99 ")+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
						ENDIF
						IF lcmodopag6>0
						? SUBSTR(lcmpag6, 1, 20)+":	"
						?? TRANSFORM(lcmodopag6, "99999999.99 ")+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
						ENDIF
						IF lcmodopag7>0
						? SUBSTR(lcmpag7, 1, 20)+":	"
						?? TRANSFORM(lcmodopag7, "99999999.99 ")+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
						ENDIF
						IF lcmodopag8>0
						? SUBSTR(lcmpag8, 1, 20)+":	"
						?? TRANSFORM(lcmodopag8, "99999999.99 ")+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
						ENDIF
						IF lctroco>0
						? "TROCO:	"
						?? TRANSFORM(lctroco, "99999999.99 ")+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
						ENDIF
					ENDIF
				ENDIF
				uf_gerais_resetcorpos()
				IF lcmotisencaoiva==.T. .AND. uf_gerais_validaivadireitodeducao()==0
					SELECT ucrsft
					? ""
					? "Resumo do IVA:"
					? "Taxa	     "+"Base     "+"IVA     "+"Total"
					IF ucrsft.eivav4<>0
						? TRANSFORM(ucrsft.ivatx4, "99")+"%      "
						?? TRANSFORM(ucrsft.eivain4, lcmascaradinheiro)+" "
						?? TRANSFORM(ucrsft.eivav4, lcmascaradinheiro)+" "
						?? TRANSFORM(ucrsft.eivain4+ucrsft.eivav4, lcmascaradinheiro+" ")+" "
					ENDIF
					IF ucrsft.eivav1<>0
						? TRANSFORM(ucrsft.ivatx1, "99")+"%      "
						?? TRANSFORM(ucrsft.eivain1, lcmascaradinheiro)+" "
						?? TRANSFORM(ucrsft.eivav1, lcmascaradinheiro)+" "
						?? TRANSFORM(ucrsft.eivain1+ucrsft.eivav1, lcmascaradinheiro+" ")+" "
					ENDIF
					IF ucrsft.eivav3<>0
						? TRANSFORM(ucrsft.ivatx3, "99")+"%      "
						?? TRANSFORM(ucrsft.eivain3, lcmascaradinheiro)+" "
						?? TRANSFORM(ucrsft.eivav3, lcmascaradinheiro)+" "
						?? TRANSFORM(ucrsft.eivain3+ucrsft.eivav3, lcmascaradinheiro+" ")+" "
					ENDIF
					IF ucrsft.eivav2<>0
						? TRANSFORM(ucrsft.ivatx2, "99")+"%      "
						?? TRANSFORM(ucrsft.eivain2, lcmascaradinheiro)+" "
						?? TRANSFORM(ucrsft.eivav2, lcmascaradinheiro)+" "
						?? TRANSFORM(ucrsft.eivain2+ucrsft.eivav2, lcmascaradinheiro+" ")+" "
					ENDIF
				ENDIF
				uf_gerais_textovermpos()
				uf_gerais_resetcorpos()
				SELECT ucrsft
				GOTO TOP
				IF  .NOT. EMPTY(ucrsft.u_nratend)
					?
					?? CHR(27)+CHR(97)+CHR(1)
					? 'Atendimento:'
					?
					??? CHR(29)+CHR(104)+CHR(50)+CHR(29)+CHR(72)+CHR(2)+CHR(29)+CHR(119)+CHR(2)+CHR(29)+CHR(107)+CHR(4)+UPPER(ALLTRIM(ucrsft.u_nratend))+CHR(0)
					uf_gerais_textoljpos()
				ENDIF
				IF  .NOT. EMPTY(lcadireserva)
					IF USED("uCrsNrReserva")
						SELECT ucrsnrreserva
						GOTO TOP
						IF  .NOT. EMPTY(ucrsnrreserva.nrreserva)
						?
						?? CHR(27)+CHR(97)+CHR(1)
						? 'ID Reserva:'
						?
						??? CHR(29)+CHR(104)+CHR(50)+CHR(29)+CHR(72)+CHR(2)+CHR(29)+CHR(119)+CHR(2)+CHR(29)+CHR(107)+CHR(4)+UPPER(ALLTRIM(ucrsnrreserva.nrreserva))+CHR(0)
						uf_gerais_textoljpos()
						ENDIF
					ENDIF
				ENDIF
				uf_gerais_textocjpos()
				??? CHR(27)+CHR(97)+CHR(1)
				IF UPPER(ALLTRIM(lcpainel))=="ATENDIMENTO"
					IF lcprinttype==myregcli .OR. lcprinttype==85
						?
						? "    ----------------------------------"
						? "       O Cliente"
					ENDIF
				ENDIF
				IF lcmotisencaoiva==.T. .AND. uf_gerais_validaivadireitodeducao()==0
					IF lcisentoiva
						DO CASE
						CASE ucrsft.pais==3
							? "Nota (*1): Isento Artigo 14. do CIVA (ou similar)."
						CASE ucrsft.pais==2
							? "Nota (*1): Isento Artigo 14. do RITI (ou similar)."
						OTHERWISE
							? ALLTRIM(ucrse1.motivo_isencao_iva)
						ENDCASE
					ENDIF
				ENDIF
				? 'Os bens/servi�os foram colocados � disposi��o do adquirente na data do documento'
				IF uf_gerais_actgrelha("", "uCrsIduCert", "exec up_gerais_iduCert '"+ALLTRIM(lcstamp)+"'")
					IF RECCOUNT("uCrsIduCert")>0
						IF ucrsiducert.temcert
						IF lcmotisencaoiva==.T. .AND. uf_gerais_validaivadireitodeducao()==0
							? 'IVA INCLUIDO'
						ENDIF
						? ALLTRIM(ucrsiducert.parte1)
						?? '-'+ALLTRIM(ucrsiducert.parte2)+' '+ALLTRIM(ucrsft2.u_docorig)
						ELSE
						IF uf_gerais_actgrelha("", "ucrsRodapeCert", [exec up_cert_TextoRodape ']+ALLTRIM(ucrsft.tiposaft)+['])
							? ALLTRIM(ucrsrodapecert.rodape)
						ELSE
							IF lcmotisencaoiva==.T. .AND. uf_gerais_validaivadireitodeducao()==0
								? "Processado por Computador - IVA INCLUIDO"
							ENDIF
						ENDIF
						ENDIF
					ELSE
						IF uf_gerais_actgrelha("", "ucrsRodapeCert", [exec up_cert_TextoRodape ']+ALLTRIM(ucrsft.tiposaft)+['])
						? ALLTRIM(ucrsrodapecert.rodape)
						ELSE
						IF lcmotisencaoiva==.T. .AND. uf_gerais_validaivadireitodeducao()==0
							? "Processado por Computador - IVA INCLUIDO"
						ENDIF
						ENDIF
					ENDIF
					fecha("uCrsIduCert")
					
					SELECT uCrsStampsQRCode
					APPEND BLANK 
					replace uCrsStampsQRCode.ftstamp WITH ALLTRIM(uCrsFt.ftstamp)
							
				ELSE
					IF uf_gerais_actgrelha("", "ucrsRodapeCert", [exec up_cert_TextoRodape ']+ALLTRIM(ucrsft.tiposaft)+['])
						? ALLTRIM(ucrsrodapecert.rodape)
					ELSE
						IF lcmotisencaoiva==.T. .AND. uf_gerais_validaivadireitodeducao()==0
						? "Processado por Computador - IVA INCLUIDO"
						ENDIF
					ENDIF
				ENDIF
				? "Logitools, Lda"
				LOCAL lcopcode
				STORE 0 TO lcopcode
				IF uf_gerais_getparameter("ADM0000000047", "BOOL")
					lcopcode = myopaltcod
				ELSE
					lcopcode = ucrsft.vendedor
				ENDIF
				SELECT ucrsft
				GOTO TOP
				? "Op: "
				IF lcopcode==0
					?? SUBSTR(ucrsft.vendnm, 1, 30)+" ("+ALLTRIM(STR(ucrsft.vendedor))+")"
					? ""
				ELSE
					?? SUBSTR(ucrsft.vendnm, 1, 30)+" ("+ALLTRIM(STR(lcopcode))+")"
					? ""
				ENDIF
				uf_gerais_criarrodapepos()
				
				IF reccount("uCrsStampsQRCode")=0	
					uf_gerais_feedcutpos()
				ENDIF 
				
*!*					IF uf_gerais_getparameter("ADM0000000207", "BOOL") .AND.  .NOT. lcreimp
*!*						SELECT ucrsft
*!*						GOTO TOP
*!*						IF  .NOT. EMPTY(ucrsft.u_nratend)
*!*							?
*!*							?
*!*							?? CHR(27)+CHR(97)+CHR(1)
*!*							? 'Atendimento:'
*!*							?
*!*							??? CHR(29)+CHR(104)+CHR(50)+CHR(29)+CHR(72)+CHR(2)+CHR(29)+CHR(119)+CHR(2)+CHR(29)+CHR(107)+CHR(4)+UPPER(ALLTRIM(ucrsft.u_nratend))+CHR(0)
*!*							uf_gerais_textoljpos()
*!*							LOCAL lctotatendsep
*!*							SELECT ucrsft
*!*							CALCULATE SUM(ucrsft.etotal) TO lctotatendsep 
*!*							? "Total Documento   :"
*!*							IF EMPTY(uf_gerais_getparameter("ADM0000000260", "text")) .OR. UPPER(ALLTRIM(uf_gerais_getparameter("ADM0000000260", "text")))=="EURO"
*!*							?? TRANSFORM(lctotatendsep, "99999999999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(213)
*!*							ELSE
*!*							IF uf_gerais_getparameter("ADM0000000260", "text")='USD'
*!*								?? TRANSFORM(lctotatendsep, "99999999999999.99 ")+CHR(27)+CHR(116)+CHR(19)+CHR(36)
*!*							ELSE
*!*								?? TRANSFORM(lctotatendsep, "99999999999999.99 ")+" "+ALLTRIM(uf_gerais_getparameter_site('ADM0000000004', 'text', mysite))
*!*							ENDIF
*!*							ENDIF
*!*							uf_gerais_feedcutpos()
*!*						ENDIF
*!*					ENDIF
				uf_gerais_setimpressorapos(.F.)
				
				IF reccount("uCrsStampsQRCode")>0 AND UPPER(mypaisconfsoftw)=='PORTUGAL'
					** QRCODES
					LOCAL lcTokenQrcode, lcordemQRCode
					STORE 1 TO lcordemQRCode
					lcTokenQrcode = uf_gerais_stamp()
					SELECT uCrsStampsQRCode
					GO TOP 
					SCAN
						uf_gerais_gerar_qrcodejpg(ALLTRIM(uCrsStampsQRCode.ftstamp), ALLTRIM(lcTokenQrcode), lcordemQRCode)
						lcordemQRCode = lcordemQRCode + 1
					ENDSCAN 
					** Indicar o corte na �ltima imagem
					IF uf_gerais_getParameter_site("ADM0000000149", "BOOL", mysite)
						TEXT TO lcsql TEXTMERGE NOSHOW
						 	update 
						 		image_printPos 
						 	set 
						 		feedcut=1 
						 	where 
						 		token='<<ALLTRIM(lcTokenQrcode)>>' 
						 		and [order]=(select max([order]) from image_printPos (nolock) where token='<<ALLTRIM(lcTokenQrcode)>>')
					    ENDTEXT
					    uf_gerais_actgrelha("", "", lcsql)
					ENDIF 
					
					STORE '&' TO lcadd
					lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\ltsPrinter\ltsPrinter.jar'+ ' ' + '"--idCl='+UPPER(ALLTRIM(sql_db))+'" '+' "--token='+ALLTRIM(lcTokenQrcode)+'"'
					lcwsdir = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\ltsPrinter'
					lcwspath = "cmd /c cd /d "+lcwsdir+" "+lcadd+lcadd+" javaw -jar "+lcwspath
					_CLIPTEXT = lcwspath 
					owsshell = CREATEOBJECT("WScript.Shell")
					owsshell.run(lcwspath, 0, .T.) 
					
					&& Funcoes abrir Gaveta e Cortar Papel
					IF !uf_gerais_getParameter_site("ADM0000000149", "BOOL", mysite)
						uf_gerais_setImpressoraPOS(.t.)
						uf_gerais_feedCutPOS()
						uf_gerais_setImpressoraPOS(.f.)
					ENDIF 
				ENDIF 
				
				IF USED("uCrsStampsQRCode")
					fecha("uCrsStampsQRCode")
				ENDIF
				
				IF UPPER(ALLTRIM(lcpainel))=="ATENDIMENTO"
					IF lcprinttype<>myregcli .AND. lcprinttype<>85
						uf_imprimirpos_impprevpsico(lcstamp, .F., lcpainel)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
    SELECT ucrsresumos
    lcstampant = ALLTRIM(ucrsresumos.ftstamp)
 ENDSCAN
ENDFUNC
**
FUNCTION uf_resumos_expvdpdf
 PUBLIC lcexportsyntstamp
 LOCAL lccont2, lccharsep, lcvar, lcnrlin
 lccont2 = 1
 lcnrlin = 0
 STORE "" TO lcexportsyntstamp, lccharsep, lcvar
 SELECT ucrsresumos
 GOTO TOP
 SCAN
    IF ucrsresumos.sel=.T.
       lcnrlin = lcnrlin+1
    ENDIF
 ENDSCAN
 IF lcnrlin=0
    uf_perguntalt_chama("N�O SELECIONOU NENHUM DOCUMENTO PARA EXPORTAR.", "OK", "", 48)
    RETURN .F.
 ENDIF

 **oshell = CREATEOBJECT("Shell.Application")
 **ofolder = oshell.application.browseforfolder(_SCREEN.hwnd, "SELECCIONE A PASTA DE DESTINO:", 1)
 **IF ISNULL(ofolder)
 **   RETURN .F.
 **ELSE
 **   ? ofolder.self.path
 **ENDIF

 **myfilepathseq = ofolder.self.path+"\"

 **uv_path = ALLTRIM(uf_gerais_getUmValor("B_Terminal", "LTRIM(RTRIM(pathExport))", "terminal = '" + alltrim(myTerm) + "' and site = '" + alltrim(mySite) + "'"))
 uv_path = ALLTRIM(uf_gerais_getUmValor("B_Terminal", "LTRIM(RTRIM(pathExport))", "terminal = '" + alltrim(myTerm) + "'"))

 IF EMPTY(uv_path)

    uf_perguntalt_chama('Aten��o!' + chr(13) + 'N�o preencheu o par�metro "Caminho Export.".' + chr(13) + 'Ter� de preencher o mesmo para continuar.', "Ok", "", 48)
    RETURN .f.

 ENDIF

 IF !uf_gerais_createDir(uv_path)
    RETURN .F.
 ENDIF

 myfilepathseq = uv_path + "\"

 SELECT * FROM uCrsResumos WITH (BUFFERING = .T.) WHERE ucrsresumos.sel=.T. INTO CURSOR ucrsPesqFactAux READWRITE
 SELECT ucrspesqfactaux
 uv_recCount = RECCOUNT("ucrsPesqFactAux")
 regua(0, uv_recCount, "A Exportar Documentos...")

 SELECT ucrsPesqFactAux
 GOTO TOP
 SCAN
    
    regua(1, lccont2, "A Exportar Documento: "+astr(lccont2))

	SET PROCEDURE TO LOCFILE("FoxBarcodeQR.prg") ADDITIVE
	**--- Create FoxBarcodeQR private object
	PRIVATE poFbc
	m.poFbc = CREATEOBJECT("FoxBarcodeQR")

    TEXT TO lcSQL TEXTMERGE NOSHOW
        exec up_print_qrcode_a4 '<<ALLTRIM(ucrsPesqFactAux.ftstamp)>>'
    ENDTEXT
    uf_gerais_actgrelha("", "FTQRCODE", lcSQL)

    SELECT ucrsPesqFactAux
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_touch_ft '<<ALLTRIM(ucrsPesqFactAux.ftstamp)>>'
    ENDTEXT
    uf_gerais_actgrelha("", "FT", lcsql)
    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			SELECT respostaDt = isnull((select top 1 DescrResposta from b_dt_resposta WHERE stampDoc = '<<ALLTRIM(ucrsPesqFactAux.ftstamp)>>' order by DataEnvio),''),* FROM FT2 (nolock) WHERE ft2stamp='<<ALLTRIM(ucrsPesqFactAux.ftstamp)>>'
    ENDTEXT
    uf_gerais_actgrelha("", "FT2", lcsql)

    lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_facturacao_linhas 0, <<mysite_nr>>, '<<ALLTRIM(ucrsPesqFactAux.ftstamp)>>'
    ENDTEXT
    uf_gerais_actgrelha("", "FI", lcsql)
    
    SELECT ft
    SELECT ft2
    SELECT fi
    GOTO TOP
    IF USED("TD")
       fecha("TD")
    ENDIF
    SELECT * FROM uCrsGeralTD WHERE ucrsgeraltd.ndoc==ft.ndoc AND ALLTRIM(ucrsgeraltd.site)==ALLTRIM(mysite) INTO CURSOR TD READWRITE
    myfilenameseq = ""
    SELECT ft
    myfilenameseq = ALLTRIM(STRTRAN(ALLTRIM(ft.nome), " ", "")) + '_' + ALLTRIM(ft.nmdoc)+'_'+ALLTRIM(td.tiposaft)+'_'+ALLTRIM(STR(ft.ndoc))+'_'+ALLTRIM(STR(ft.fno))
    myfilenameseq = STRTRAN(myfilenameseq, '.', '')

    SELECT ft
    SELECT ft2
    SELECT fi
    myfiletableseq = "FI"
    uf_imprimirgerais_chama('FACTURACAO', .T., .F., .F.)
    lccont2 = lccont2+1
 ENDSCAN
 uf_perguntalt_chama("PROCESSO CONCLU�DO COM SUCESSO. POR FAVOR VERIFIQUE OS FICHEIROS NA DIRECTORIA DEFINIDA."+CHR(10)+CHR(10)+"NOTA: A GERA��O DOS FICHEIROS PODER� AINDA ESTAR EM CURSO.", "OK", "", 64)
 regua(2)
 fecha("ucrsPesqFactAux")
ENDFUNC
**

FUNCTION uf_resumos_exporta
	
	SELECT * FROM uCrsResumos INTO CURSOR uCrsResumosAux READWRITE 
*!*		
*!*		 LOCAL lcfiltro
*!*		 lcfiltro = ""
*!*		 IF UPPER(ALLTRIM(resumos.txtsuspensas.value))==UPPER(ALLTRIM("N�o Suspensas"))
*!*		 	SELECT uCrsResumosAux 
*!*		 	GO TOP 
*!*		    DELETE FROM uCrsResumosAux where at('(S)',nmdoc)<>0
*!*		 ENDIF
*!*		 IF UPPER(ALLTRIM(resumos.txtsuspensas.value))==UPPER(ALLTRIM("Suspensas"))
*!*		    SELECT uCrsResumosAux 
*!*		 	GO TOP 
*!*		    DELETE FROM uCrsResumosAux where at('(S)',nmdoc)=0
*!*		 ENDIF
	 IF resumos.imagesocred.tag=="true"

	    SELECT uCrsResumosAux 
	 	GO TOP 
	    DELETE FROM uCrsResumosAux where at('Factura',nmdoc)=0
	        SELECT uCrsResumosAux 
	 	GO TOP 
	     **DELETE FROM uCrsResumosAux where  valorNreg = 0
	      DELETE FROM uCrsResumosAux where  qtt <= eaquisicao and  valorNreg = 0
	
	 ENDIF
	 
	 SELECT uCrsResumosAux 

	SELECT data as data, ousrhora as hora, nmdoc as documento, fno as nrdocumento, cliente, ref, design as Designacao, qtt, etiliquido as total, utentenome as nomeutente, nmdoc, no FROM uCrsResumosAux INTO CURSOR uCrsResumosxls READWRITE 
	sele uCrsResumosxls 
	
	Local mdir,lcCampos
	
	**oShell = CREATEOBJECT("Shell.Application")
	**oFolder = oShell.Application.BrowseForFolder(_screen.HWnd,"Selecione a pasta para a exporta��o.", 1)
	**IF isnull(oFolder)
	**	RETURN .f.
	**ENDIF
	**
	**mdir = oFolder.self.path

    **uv_path = ALLTRIM(uf_gerais_getUmValor("B_Terminal", "LTRIM(RTRIM(pathExport))", "terminal = '" + alltrim(myTerm) + "' and site = '" + alltrim(mySite) + "'"))
    uv_path = ALLTRIM(uf_gerais_getUmValor("B_Terminal", "LTRIM(RTRIM(pathExport))", "terminal = '" + alltrim(myTerm) + "'"))

    IF EMPTY(uv_path)

		uf_perguntalt_chama('Aten��o!' + chr(13) + 'N�o preencheu o par�metro "Caminho Export.".' + chr(13) + 'Ter� de preencher o mesmo para continuar.', "Ok", "", 48)
		RETURN .f.

    ENDIF

    IF !uf_gerais_createDir(uv_path)
        RETURN .F.
    ENDIF

    mdir = uv_path + "\"
	
	
	IF  .NOT. uf_perguntalt_chama("Pretende separar o resumo por cliente?", "Sim", "N�o")	
		IF !EMPTY(mdir)
			lcCacheDir = ALLTRIM(uf_gerais_getParameter('ADM0000000015','TEXT')) + "\Cache\XLS"
			lcFile = ALLTRIM(lcCacheDir) + "\Resumos"+alltrim(str(day(date())))+alltrim(str(month(date())))+alltrim(str(year(date())))+strtran(time(),':','')+".xls"
			lcFiledest = ALLTRIM(mdir) + "\resumos.xls"

			IF USED("CrsResumosxls")
				fecha("CrsResumosxls")
			ENDIF 

			SELECT uCrsResumosxls 
			gnFieldcount = AFIELDS(gaMyArray) 
			FOR nCount = 1 TO gnFieldcount 
				IF EMPTY(lcCampos)
					lcCampos = proper(gaMyArray(nCount,1))
				ELSE
					lcCampos = lcCampos + "," + proper(gaMyArray(nCount,1))
				ENDIF 
			ENDFOR 

			SELECT &lcCampos FROM uCrsResumosxls INTO CURSOR CrsResumosxls READWRITE 

			SELECT CrsResumosxls 
			GO TOP 	
			SELECT CrsResumosxls 
			COPY TO &lcFile TYPE XL5
			COPY FILE (lcFile) TO (lcFiledest)
			
			uf_perguntalt_chama("PROCESSO CONCLU�DO COM SUCESSO. POR FAVOR VERIFIQUE OS FICHEIROS NA DIRECTORIA DEFINIDA."+CHR(10)+CHR(10)+"NOTA: A GERA��O DOS FICHEIROS PODER� AINDA ESTAR EM CURSO.", "OK", "", 64)
		ENDIF
	ELSE
			
		IF !EMPTY(mdir)
			
			
			SELECT Distinct no FROM UCRSRESUMOS  INTO CURSOR UCRSRESUMOSNO readwrite   
			
			SELECT UCRSRESUMOSNO
			SCAN 
				lcCacheDir = ALLTRIM(uf_gerais_getParameter('ADM0000000015','TEXT')) + "\Cache\XLS"
				lcFile = ALLTRIM(lcCacheDir) + "\Resumos_"+ALLTRIM(STR(UCRSRESUMOSNO.NO))+"_"+alltrim(str(day(date())))+alltrim(str(month(date())))+alltrim(str(year(date())))+strtran(time(),':','')+".xls"
				lcFiledest = ALLTRIM(mdir) + "\resumos_"+ALLTRIM(STR(UCRSRESUMOSNO.NO))+".xls"

				IF USED("CrsResumosxls")
					fecha("CrsResumosxls")
				ENDIF 

				SELECT uCrsResumosxls 
				
				SELECT * FROM uCrsResumosxls WHERE uCrsResumosxls.No = UCRSRESUMOSNO.no INTO CURSOR uCrsResumosxlsCli readwrite  
				
				gnFieldcount = AFIELDS(gaMyArray)
				lcCampos = '' 
				FOR nCount = 1 TO gnFieldcount 
					IF EMPTY(lcCampos)
						lcCampos = proper(gaMyArray(nCount,1))
					ELSE
						lcCampos = lcCampos + "," + proper(gaMyArray(nCount,1))
					ENDIF 
				ENDFOR 

				SELECT &lcCampos FROM uCrsResumosxls INTO CURSOR CrsResumosxls READWRITE 

				SELECT uCrsResumosxlsCli
				GO TOP 	
				SELECT uCrsResumosxlsCli
				COPY TO &lcFile TYPE XL5
				COPY FILE (lcFile) TO (lcFiledest)
				
			
				
				SELECT uCrsResumosxls 
				GO TOP 
				SET FILTER TO 1 = 1
				fecha("uCrsResumosxlsCli")
			ENDSCAN 
			uf_perguntalt_chama("PROCESSO CONCLU�DO COM SUCESSO. POR FAVOR VERIFIQUE OS FICHEIROS NA DIRECTORIA DEFINIDA."+CHR(10)+CHR(10)+"NOTA: A GERA��O DOS FICHEIROS PODER� AINDA ESTAR EM CURSO.", "OK", "", 64)
		ENDIF
	ENDIF  
ENDFUNC 

PROCEDURE uf_resumos_frxSimple

 IF  .NOT. uf_gerais_actgrelha("", "uCrsResumosOps", 'exec up_gerais_vendedores')
    uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR [UCRSRESUMOSOP].", "OK", "", 16)
    RETURN .F.
 ENDIF
  
 SELECT design,cnpem, * FROM UCRSRESUMOS INTO CURSOR ucrsFrxSimpleTemp READWRITE

 SELECT  design_a,ref, Cliente,SUM(qtt) as qtt, no, estab, cnpem_a, nbenef FROM ucrsFrxSimpleTemp WHERE sel=.t. GROUP BY Cliente,design_a,ref, no, estab, cnpem_a, nbenef  INTO CURSOR ucrsFrxSimpleFinal READWRITE

 IF USED("ucrsFrxSimpleTemp")
	fecha("ucrsFrxSimpleTemp")
 ENDIF
 
ENDPROC

FUNCTION uf_resumos_permiteImprimir
	IF !USED("ucrsresumos")
		RETURN .F.
 	ENDIF

	SELECT ucrsresumos
	GOTO TOP
 	SCAN FOR (ucrsresumos.sel==.T.) .AND. (ucrsresumos.no<>lcno)
    		uf_perguntalt_chama("S� PODE IMPRIMIR RESUMOS PARA O MESMO CLIENTE.", "OK", "", 48)
    		RETURN .F.
 	ENDSCAN
 	RETURN .T.
ENDFUNC

