**
PROCEDURE uf_filaespera_chama
 uf_filaespera_carregadados()
 DO FORM filaEspera
ENDPROC
**
FUNCTION uf_filaespera_carregadados
 LOCAL lcmethod, lctoken, lcclid, lcsiteloja
 IF VARTYPE(lcinfomessage)<>"U"
    lcinfomessage = ALLTRIM(lcinfomessage)
 ELSE
    lcinfomessage = ''
 ENDIF
 SELECT ucrse1
 lcsiteloja = ucrse1.siteloja
 sitenr = ucrse1.siteno
 lctoken = uf_gerais_stamp()
 regua(1, 2, "A carregar dados. Por favor aguarde.")
 uf_chilkat_send_request_getwaitingqueue_ticket(lcsiteloja, lctoken, astr(sitenr))
 regua(2)
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_senhas_listaEspera '<<lcToken>>'
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "ucrsfilaEspera", lcsql)
    uf_perguntalt_chama("N�o foi poss�vel obter os dados. Por favor contacte o suporte.", "OK", "", 32)
    regua(2)
    RETURN .F.
 ELSE
    regua(2)
 ENDIF
ENDFUNC
**
PROCEDURE uf_filaespera_sair
 lcinfomessage = ''
 IF USED("ucrsfilaEspera")
    fecha("ucrsfilaEspera")
 ENDIF
 IF (TYPE("filaEspera")<>"U")
 ENDIF
ENDPROC
**
