**
PROCEDURE uf_gestaosessaocaixa_chama
 IF TYPE("GestaoSessaoCaixa")=="U"
    DO FORM GestaoSessaoCaixa
 ELSE
    gestaosessaocaixa.show
 ENDIF
ENDPROC
**
PROCEDURE uf_gestaosessaocaixa_chamaContagemNumerario()
	 uf_contagemnumerario_initCursores()
	 uf_contagemnumerario_chama()
ENDPROC
**
FUNCTION uf_gestaosessaocaixa_init
 LOCAL lcsql
 IF ucrse1.gestao_cx_operador=.T.
    IF uf_gerais_getparameter('ADM0000000026', 'bool')
       gestaosessaocaixa.txtfundo.value = astr(ROUND(uf_gerais_getparameter('ADM0000000026', 'num'), 2), 8, 2)
    ELSE
       gestaosessaocaixa.txtfundo.value = ''
    ENDIF
 ELSE
    IF EMPTY(mycxstamp)
       IF uf_gerais_getparameter('ADM0000000026', 'bool')
          gestaosessaocaixa.txtfundo.value = astr(ROUND(uf_gerais_getparameter('ADM0000000026', 'num'), 2), 8, 2)
          gestaosessaocaixa.txtfundo.readonly = .T.
       ENDIF
    ELSE
       lcsql = ''
       TEXT TO lcsql TEXTMERGE NOSHOW
				select TOP 1 * FROM cx WHERE cxstamp = '<<ALLTRIM(myCxStamp)>>'
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "uCrsFcx", lcsql)
          uf_perguntalt_chama("N�o foi poss�vel verificar o valor dispon�vel para fundo de caixa.", "OK", "", 32)
          regua(2)
          RETURN .F.
       ELSE
          IF RECCOUNT()>0
             gestaosessaocaixa.txtfundo.value = ALLTRIM(STR(ucrsfcx.fundocaixa, 10, 2))
          ENDIF
          fecha("uCrsFcx")
       ENDIF
    ENDIF
 ENDIF
 gestaosessaocaixa.menu1.estado("", "", "Gravar", .T.)
 gestaosessaocaixa.tecladovirtual1.setestado(3)
 gestaosessaocaixa.tecladovirtual1.setsize(73, 56)
 gestaosessaocaixa.tecladovirtual1.funcaoteclas = "pressKeyNum"
 gestaosessaocaixa.tecladovirtual1.allowdot(.T.)
 gestaosessaocaixa.tecladovirtual1.allownegativevalue(.F.)
 gestaosessaocaixa.tecladovirtual1.nrcasasdecimais = 2
 IF USED("uCrsTmpSS")
    gestaosessaocaixa.txtdinheiro.value = astr(ucrstmpss.evdinheiro, 15, 2)
    gestaosessaocaixa.txtmultibanco.value = astr(ucrstmpss.epaga1, 15, 2)
    gestaosessaocaixa.txtvisa.value = astr(ucrstmpss.epaga2, 15, 2)
    gestaosessaocaixa.txtcheques.value = astr(ucrstmpss.echtotal, 15, 2)
    gestaosessaocaixa.txtmodpag3.value = astr(ucrstmpss.fecho_epaga3, 15, 2)
    gestaosessaocaixa.txtmodpag4.value = astr(ucrstmpss.fecho_epaga4, 15, 2)
    gestaosessaocaixa.txtmodpag5.value = astr(ucrstmpss.fecho_epaga5, 15, 2)
    gestaosessaocaixa.txtmodpag6.value = astr(ucrstmpss.fecho_epaga6, 15, 2)
    gestaosessaocaixa.txtcomissaotpa.value = astr(ucrstmpss.comissaotpa)
    gestaosessaocaixa.txtobs.value = ucrstmpss.causa
    gestaosessaocaixa.txtfundo.value = astr(ucrstmpss.fundocaixa, 15, 2)
 ENDIF
ENDFUNC
**
PROCEDURE uf_gestaosessaocaixa_sair
 IF USED("uCrsTmpSS")
    fecha("uCrsTmpSS")
 ENDIF
 gestaosessaocaixa.hide
 gestaosessaocaixa.release
ENDPROC
**
FUNCTION uf_gestaosessaocaixa_gravar
 IF uf_gerais_getparameter_site('ADM0000000063', 'BOOL', mysite)
    IF  .NOT. uf_gestaosessaocaixa_verifvalores()
       RETURN .F.
    ENDIF
 ENDIF
 IF ucrse1.gestao_cx_operador=.T.
    IF EMPTY(myssstamp)
       uf_caixas_criarsessao(ROUND(VAL(gestaosessaocaixa.txtfundo.value), 2))
    ELSE
       uf_caixas_fecharcaixa(.T.)
    ENDIF
 ELSE
    IF EMPTY(mycxstamp)
       uf_caixas_criarsessao(ROUND(VAL(gestaosessaocaixa.txtfundo.value), 2))
    ELSE
       uf_caixas_fecharcaixa(.T.)
    ENDIF
 ENDIF
 uf_gestaosessaocaixa_sair()
ENDFUNC
**
FUNCTION uf_gestaosessaocaixa_verifvalores
 LOCAL lctotfact
 STORE 0 TO lctotfact
 lcsql = ''
 TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_caixa_dadosParaTaloes '<<ALLTRIM(myCxStamp)>>', '<<ALLTRIM(mySite)>>'
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsValComparacao", lcsql)
    uf_perguntalt_chama("N�o foi poss�vel verificar os valores da fatura��o do dia.", "OK", "", 32)
    fecha("uCrsValComparacao")
    RETURN .F.
 ELSE
    IF RECCOUNT()>0
       lctotfact = ucrsvalcomparacao.totalcaixa
    ENDIF
 ENDIF
 fecha("uCrsValComparacao")
 IF lctotfact=0
    uf_perguntalt_chama("N�o foi registada qualquer fatura��o para o dia de hoje. N�o h� valores para comparar.", "OK", "", 32)
 ELSE
    LOCAL lcval1, lcval2, lcval3, lcval4, lcval5, lcval6, lcval7, lcval8, lcvaltot
    STORE 0 TO lcval1, lcval2, lcval3, lcval4, lcval5, lcval6, lcval7, lcval8, lcvaltot
    IF  .NOT. EMPTY(gestaosessaocaixa.txtdinheiro.value)
       lcval1 = VAL(gestaosessaocaixa.txtdinheiro.value)
    ENDIF
    IF  .NOT. EMPTY(gestaosessaocaixa.txtmultibanco.value)
       lcval2 = VAL(gestaosessaocaixa.txtmultibanco.value)
    ENDIF
    IF  .NOT. EMPTY(gestaosessaocaixa.txtcheques.value)
       lcval3 = VAL(gestaosessaocaixa.txtcheques.value)
    ENDIF
    IF  .NOT. EMPTY(gestaosessaocaixa.txtvisa.value)
       lcval4 = VAL(gestaosessaocaixa.txtvisa.value)
    ENDIF
    IF  .NOT. EMPTY(gestaosessaocaixa.txtmodpag3.value)
       lcval5 = VAL(gestaosessaocaixa.txtmodpag3.value)
       IF ALLTRIM(gestaosessaocaixa.label3.caption)='EUR' .OR. ALLTRIM(gestaosessaocaixa.label3.caption)='USD'
          lcval5 = uf_gestaosessaocaixa_moeda(ALLTRIM(gestaosessaocaixa.label3.caption), lcval5)
       ENDIF
    ENDIF
    IF  .NOT. EMPTY(gestaosessaocaixa.txtmodpag4.value)
       lcval6 = VAL(gestaosessaocaixa.txtmodpag4.value)
       IF ALLTRIM(gestaosessaocaixa.label17.caption)='EUR' .OR. ALLTRIM(gestaosessaocaixa.label17.caption)='USD'
          lcval6 = uf_gestaosessaocaixa_moeda(ALLTRIM(gestaosessaocaixa.label17.caption), lcval6)
       ENDIF
    ENDIF
    IF  .NOT. EMPTY(gestaosessaocaixa.txtmodpag5.value)
       lcval7 = VAL(gestaosessaocaixa.txtmodpag5.value)
    ENDIF
    IF  .NOT. EMPTY(gestaosessaocaixa.txtmodpag6.value)
       lcval7 = VAL(gestaosessaocaixa.txtmodpag6.value)
    ENDIF
    lcvaltot = lcval1+lcval2+lcval3+lcval4+lcval5+lcval6+lcval7+lcval8
    IF lctotfact<>lcvaltot
       IF uf_perguntalt_chama("EXISTEM DIFEREN�AS ENTRE A CONTAGEM E O VALOR REGISTADO NOS ATENDIMENTOS."+CHR(13)+"DESEJA CORRIGIR?", "Sim", "N�o")
          RETURN .F.
       ENDIF
    ENDIF
 ENDIF
 RETURN .T.
ENDFUNC
**
FUNCTION uf_gestaosessaocaixa_moeda
 LPARAMETERS moeda, lcvalpagmoeda
 TEXT TO lcsql TEXTMERGE NOSHOW
		select top 1 cambio from cb where moeda='<<ALLTRIM(moeda)>>' order by data desc
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "curcamb", lcsql)
    uf_perguntalt_chama("OCORREU UM ERRO A OBTER O C�MBIO.", "OK", "", 64)
    RETURN .F.
 ENDIF
 LOCAL lcretvalmoeda
 STORE 0 TO lcretvalmoeda
 SELECT curcamb
 IF RECCOUNT("curcamb")=0
    uf_perguntalt_chama("N�O EXISTE NENHUM C�MBIO REGISTADO PARA A MOEDA "+ALLTRIM(moeda)+". POR FAVOR VERIFIQUE!", "OK", "", 64)
    fecha("curcamb")
 ELSE
    lcretvalmoeda = ROUND(lcvalpagmoeda*(1/curcamb.cambio), 2)
    fecha("curcamb")
 ENDIF
 RETURN lcretvalmoeda
ENDFUNC
**
