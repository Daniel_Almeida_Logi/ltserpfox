FUNCTION uf_envNotasDem_chama
	LPARAMETERS uv_nrReceita, uv_nrLinha, uv_design, uv_operador, uv_utente
	
	IF !uf_gerais_checkCedula()
		RETURN .F.
	ENDIF

	IF WEXIST("ENVNOTASDEM")
		uf_envNotasDem_sair()
	ENDIF
	
	DO FORM ENVNOTASDEM WITH uv_nrReceita, uv_nrLinha, uv_design, uv_operador, uv_utente

ENDFUNC

FUNCTION uf_envNotasDem_sair

	IF WEXIST("ENVNOTASDEM")
	
		ENVNOTASDEM.HIDE()
		ENVNOTASDEM.RELEASE()
	
	ENDIF

ENDFUNC

FUNCTION uf_envnotasdem_motivosNota
	LPARAMETERS uv_dispensa

	TEXT TO uv_sql TEXTMERGE NOSHOW

		SELECT descricao FROM motivosNotasTerapeticas(nolock) WHERE dispensa = <<IIF(uv_dispensa, '1', '0')>> ORDER BY nr ASC

	ENDTEXT

	IF !uf_gerais_actGrelha("", "ucrsmotivosNT", uv_sql)
		RETURN .F.
	ENDIF

	PUBLIC upv_motivoNotaTerapeutica
	upv_motivoNotaTerapeutica = ""

	SELECT ucrsmotivosNT 
	GO TOP

	uf_valorescombo_chama("upv_motivoNotaTerapeutica",0,"ucrsmotivosNT",2,"descricao", "descricao", .F., .F., .T., .T., .F., .F.,.F.,.F.)

	IF EMPTY(upv_motivoNotaTerapeutica)
		FECHA("ucrsmotivosNT")
		RELEASE upv_motivoNotaTerapeutica
		RETURN .F.
	ENDIF

	ENVNOTASDEM.editNota.value = IIF(uv_dispensa, "Motivo de Dispensa: ",  "Motivo de n�o Dispensa: ") + ALLTRIM(upv_motivoNotaTerapeutica)

	FECHA("ucrsmotivosNT")
	RELEASE upv_motivoNotaTerapeutica

ENDFUNC

FUNCTION uf_envNotasDem_validaCamposOrig

	IF EMPTY(ENVNOTASDEM.txtNrReceita.value)
		uf_perguntalt_chama("O campo Nr� de Receita � de preenchimento obrigat�rio.", "OK", "", 48)
		ENVNOTASDEM.txtNrReceita.setFocus()
		RETURN .F.
	ENDIF

	IF EMPTY(ENVNOTASDEM.txtNrLinha.value)
		uf_perguntalt_chama("O campo Nr� da Linha � de preenchimento obrigat�rio.", "OK", "", 48)
		ENVNOTASDEM.txtNrLinha.setFocus()
		RETURN .F.
	ENDIF

	IF EMPTY(ENVNOTASDEM.txtDesign.value)
		uf_perguntalt_chama("O campo Designa��o � de preenchimento obrigat�rio.", "OK", "", 48)
		ENVNOTASDEM.txtDesign.setFocus()
		RETURN .F.
	ENDIF

	IF EMPTY(ENVNOTASDEM.editNota.value)
		uf_perguntalt_chama("O campo Nota Terap�utica � de preenchimento obrigat�rio.", "OK", "", 48)
		ENVNOTASDEM.editNota.setFocus()
		RETURN .F.
	ENDIF
	
	
	IF LEN(ENVNOTASDEM.editNota.value) > 500
		uf_perguntalt_chama("O campo Nota Terap�utica tem um limite m�ximo de 500 caracteres, por favor retifique." + CHR(13) + "Neste momento tem " + ALLTRIM(STR(LEN(ENVNOTASDEM.editNota.value))) + " caracteres." , "OK", "", 48)
		ENVNOTASDEM.editNota.setFocus()
		RETURN .F.
	ENDIF

	RETURN .T.

ENDFUNC

FUNCTION uf_envNotasDem_gravar

	IF !uf_envNotasDem_validaCamposOrig()
		RETURN .F.
	ENDIF

	LOCAL uv_sql, uv_token

	uv_token = uf_gerais_stamp()
	
	TEXT TO uv_sql TEXTMERGE NOSHOW
		INSERT INTO 
			logs_com
			(stamp, tipo, ousrinis, texto, destino, data, site, token, dataNota, id)
		VALUES
			(
				LEFT(NEWID(), 15) + RIGHT(NEWID(), 10), 
				'NOTAS_TERAPEUTICAS',
				'<<m_chinis>>',
				'<<ALLTRIM(ENVNOTASDEM.editNota.value)>>',
				'<<m_chinis>>',
				CONVERT(VARCHAR, GETDATE(), 112),
				'<<ALLTRIM(mySite)>>',
				'<<ALLTRIM(uv_token)>>',
				GETDATE(),
				'<<ALLTRIM(ENVNOTASDEM.txtNrLinha.value)>>'
			)
	ENDTEXT

	LOCAL lcdbserver, lcnomejar, lctestjar, lcwspath, lccodfarm

	IF !uf_gerais_actGrelha("", "", uv_sql)
		uf_perguntalt_chama("Erro a gerar o pedido!" + CHR(13) + "Por favor contacte o suporte.", "OK", "", 48)
		regua(2)
		RETURN .F.
	ENDIF

	IF  !USED("ucrsServerName")
		TEXT TO lcsql TEXTMERGE NOSHOW
			Select @@SERVERNAME as dbServer
		ENDTEXT
		IF  .NOT. uf_gerais_actgrelha("", "ucrsServerName", lcsql)
			uf_perguntalt_chama("N�o foi possivel proceder ao servi�o de Envio de SMS de acesso." + CHR(13) + "Contacte o suporte.", "OK", "", 32)
			regua(2)
			RETURN .F.
		ENDIF
	ENDIF

	SELECT ucrse1

	lccodfarm = ALLTRIM(ucrse1.u_codfarm)

	SELECT ucrsservername
	lcdbserver = ALLTRIM(ucrsservername.dbserver)

	lcnomejar = "LtsDispensaEletronica.jar"

	IF uf_gerais_getparameter("ADM0000000227", "BOOL")
		lctestjar = "1"
	ELSE
		lctestjar = "0"
	ENDIF

	lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\DEM\'+ ALLTRIM(lcnomejar) +' ' ;
				+ ' ENVIA_NOTA' ;
				+ ' --docId=' + ALLTRIM(ENVNOTASDEM.txtNrLinha.value) ;
				+ ' --chavePedido=' + uf_gerais_stamp() ;
				+ ' --token=' + ALLTRIM(uv_token) ;
				+ ' --iniciais=' + ALLTRIM(m_chinis) ;
				+ ' --codFarmacia=' + ALLTRIM(lccodfarm) ;
				+ ' --dbServer=' + ALLTRIM(lcdbserver) ;
				+ ' --dbName=' + UPPER(ALLTRIM(sql_db)) ;
				+ ' --site=' + UPPER(ALLTRIM(STRTRAN(ALLTRIM(mysite), ' ', '_'))) ;
				+ ' --test=' + lctestjar

	IF lctestjar == "1"
		_clipText = lcwspath
		uf_perguntalt_chama("Webservice teste...", "OK", "")
	ENDIF

	regua(1, 2, "A enviar Nota...")

	lcwspath = "javaw -jar "+myEncodingFileJar+lcwspath
	owsshell = CREATEOBJECT("WScript.Shell")
	owsshell.run(lcwspath, 1, .T.)

	TEXT TO uv_sql TEXTMERGE NOSHOW

		SELECT codigo,descricao FROM logs_com(nolock) WHERE token = '<<ALLTRIM(uv_token)>>' and tipo = 'NOTAS_TERAPEUTICAS' 

	ENDTEXT

	IF !uf_gerais_actGrelha("", "uc_respServicoNT", uv_sql)
		uf_perguntalt_chama("Erro a obter resposta do servi�o!" + CHR(13) + "Por favor contacte o suporte.", "OK", "", 48)
		regua(2)
		RETURN .F.
	ENDIF

	regua(2)

	IF EMPTY(uc_respServicoNT.codigo)
		uf_perguntalt_chama("Erro a obter resposta do servi�o!" + CHR(13) + "Por favor contacte o suporte.", "OK", "", 48)
		FECHA("uc_respServicoNT")
		RETURN .F.
	ENDIF

	IF uf_gerais_compStr(uc_respServicoNT.codigo, "100005040001")
		uf_perguntalt_chama(ALLTRIM(uc_respServicoNT.descricao), "OK", "", 64)
		FECHA("uc_respServicoNT")
		uf_envNotasDem_sair()
	ELSE
		uf_perguntalt_chama(ALLTRIM(uc_respServicoNT.descricao), "OK", "", 48)
		FECHA("uc_respServicoNT")
	ENDIF

ENDFUNC