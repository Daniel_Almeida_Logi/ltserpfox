FUNCTION uf_histDem_chama
	LPARAMETERS uv_nrUtente
	
	IF WEXIST('HISTDEM')
		uf_histDem_sair()
	ENDIF

   uf_histDem_createCab()
   uf_histDem_createLin()
	
	DO FORM HISTDEM WITH uv_nrUtente
	
ENDFUNC

FUNCTION uf_histDem_sair

	IF !WEXIST('HISTDEM')
		RETURN .F.
	ENDIF

   FECHA("uc_histDemCab")
   FECHA("uc_histDemLin")

	HISTDEM.hide()
	HISTDEM.release()
	
	
   if(VARTYPE(dadosDem) != "U")
   		dadosDem.alwaysOnTop = .T.
   endif	


ENDFUNC

FUNCTION uf_histDem_createCab
   LPARAMETERS uv_groupToken, uv_soPorDispensar

   LOCAL uv_sql, lnColumns, lcCursor, loGrid

   IF !EMPTY(uv_groupToken)
      
      TEXT TO uv_sql TEXTMERGE NOSHOW

         exec up_dem_histDemCab '<<ALLTRIM(uv_groupToken)>>', <<IIF(uv_soPorDispensar, "1", "0")>>

      ENDTEXT

   ELSE

      TEXT TO uv_sql TEXTMERGE NOSHOW

         set fmtonly on

         exec up_dem_histDemCab ''
         
         set fmtonly off

      ENDTEXT

   ENDIF

   IF WEXIST('HISTDEM')
      loGrid = HISTDEM.grdCab
      lcCursor = "uc_histDemCab"

	   lnColumns = loGrid.ColumnCount
	
	   IF lnColumns > 0

		   DIMENSION laControlSource[lnColumns]
		   FOR lnColumn = 1 TO lnColumns
			   laControlSource[lnColumn] = loGrid.Columns[lnColumn].ControlSource
		   ENDFOR

	   ENDIF

      loGrid.RecordSource = ""

   ENDIF

   IF !uf_gerais_actGrelha("", "uc_histDemCab", uv_sql)
      uf_perguntalt_chama("Erro a carregar os cabe�alhos." + CHR(13) + "Por favor contacte o suporte.", "OK", "", 48)
      RETURN .F.
   ENDIF

   SELECT uc_histDemCab
   GO TOP

   IF WEXIST('HISTDEM')

      loGrid.RecordSource = lcCursor
	
	   FOR lnColumn = 1 TO lnColumns
   		loGrid.Columns[lnColumn].ControlSource = laControlSource[lnColumn]
	   ENDFOR

      HISTDEM.refresh()
      
      HISTDEM.grdCab.receita_nr.width = 192
      HISTDEM.grdCab.receita_data.width = 100
      HISTDEM.grdCab.sel.width = 40

      HISTDEM.lblCounter.caption = ASTR(RECCOUNT("uc_histDemCab")) + IIF(RECCOUNT("uc_histDemCab")= 1,  " Receita", " Receitas")

      IF RECCOUNT("uc_histDemCab") > 0
         SELECT uc_histDemCab
         uf_histDem_createLin(ALLTRIM(uc_histDemCab.token), uv_soPorDispensar)
      ENDIF

   ENDIF

ENDFUNC

FUNCTION uf_histDem_createLin
   LPARAMETERS uv_token, uv_soPorDispensar

   LOCAL uv_sql, lnColumns, lcCursor, loGrid

   IF !EMPTY(uv_token)
      
      TEXT TO uv_sql TEXTMERGE NOSHOW

         exec up_dem_histDemLin '<<ALLTRIM(uv_token)>>', <<IIF(uv_soPorDispensar, "1", "0")>>

      ENDTEXT

   ELSE

      TEXT TO uv_sql TEXTMERGE NOSHOW

         set fmtonly on

         exec up_dem_histDemLin ''
         
         set fmtonly off

      ENDTEXT

   ENDIF

   IF WEXIST('HISTDEM')
      loGrid = HISTDEM.grdLin
      lcCursor = "uc_histDemLin"

	   lnColumns = loGrid.ColumnCount
	
	   IF lnColumns > 0

		   DIMENSION laControlSource[lnColumns]
		   FOR lnColumn = 1 TO lnColumns
			   laControlSource[lnColumn] = loGrid.Columns[lnColumn].ControlSource
		   ENDFOR

	   ENDIF

      loGrid.RecordSource = ""

   ENDIF

   IF !uf_gerais_actGrelha("", "uc_histDemLin", uv_sql)
      uf_perguntalt_chama("Erro a carregar as Linhas." + CHR(13) + "Por favor contacte o suporte.", "OK", "", 48)
      RETURN .F.
   ENDIF

   SELECT uc_histDemLin
   GO TOP

   IF WEXIST('HISTDEM')

      loGrid.RecordSource = lcCursor
	
	   FOR lnColumn = 1 TO lnColumns
   		loGrid.Columns[lnColumn].ControlSource = laControlSource[lnColumn]
	   ENDFOR

      HISTDEM.refresh()

      uf_histDem_fitGridLin()
      
   ENDIF

ENDFUNC

PROCEDURE uf_histDem_fitGridLin

   LOCAL uv_widthDesign

   WITH HISTDEM.grdLin

      .dataDispensa.width = 100
      .data_caducidade.width = 100

      .medicamento_cnpem.width = 70
      .medicamento_cod.width = 70

      .numRegistoDispensado.width = 70
      .dispensado.width = 75

      IF .width > 485

         uv_widthDesign = .width - 485

         .medicamento_descr.width = uv_widthDesign/2
         .designDispensado.width = uv_widthDesign/2

      ELSE

         .medicamento_descr.width = 350
         .designDispensado.width = 350

      ENDIF

   ENDWITH

ENDPROC

FUNCTION uf_histDem_acessoHistDispensas
   LPARAMETERS uv_nrUtente, uv_fromPainel

   IF !USED("ucrsuser")
      uf_perguntalt_chama("N�o foi poss�vel validar os dados do utilizador." + CHR(13) + "Por favor contacte o suporte.", "OK", "", 48)
      RETURN .F.
   ENDIF

   IF EMPTY(ucrsuser.drcedula)   
      uf_perguntalt_chama("Tem de preencher o campo 'N.� da C�dula Profissional' na ficha do utilizador para utilizar esta funcionalidade.", "OK", "", 48)
      RETURN .F.
   ENDIF

   IF EMPTY(ucrsuser.drclprofi)
      uf_perguntalt_chama("Tem de preencher o campo 'Classe Profissional' na ficha do utilizador para utilizar esta funcionalidade.", "OK", "", 48)
      RETURN .F.
   ENDIF

   LOCAL uv_sql, uv_token

   IF EMPTY(uv_nrUtente)
      IF uv_fromPainel
         uf_perguntalt_chama("Tem de preencher o campo 'N� Utente'.", "OK", "", 48)
         HISTDEM.txtNrUtente.setFocus()
      ENDIF
      RETURN .F.
   ENDIF

   regua(0, 2, "A enviar SMS...")

   regua(1, 1, "A enviar SMS...")

   uv_token = uf_gerais_stamp()

   TEXT TO uv_sql TEXTMERGE NOSHOW

      insert into spmsAcessoCliente
      ([stamp] ,[token] ,[code] ,[descr] ,[pinDireitoOpcao] ,[pinAcesso] ,[servico] ,[idServico] ,[ousrinis] ,[ousrdata] ,[usrinis] ,[usrdata])
      VALUES
      (
         '<<ALLTRIM(uv_token)>>'
         ,'<<ALLTRIM(uv_token)>>'
         ,''
         ,''
         ,''
         ,''
         ,'ACESSOHISTDISPENSAS'
         ,'1',
         '<<m_chinis>>'
         ,getdate()
         ,'<<m_chinis>>'
         ,getdate()
      )

   ENDTEXT

   LOCAL lcdbserver, lcnomejar, lctestjar, lcwspath, lccodfarm

   IF !uf_gerais_actGrelha("", "", uv_sql)
      uf_perguntalt_chama("Erro a gerar o pedido!" + CHR(13) + "Por favor contacte o suporte.", "OK", "", 48)
      regua(2)
      RETURN .F.
   ENDIF

   IF  !USED("ucrsServerName")
      TEXT TO lcsql TEXTMERGE NOSHOW
         Select @@SERVERNAME as dbServer
      ENDTEXT
      IF  .NOT. uf_gerais_actgrelha("", "ucrsServerName", lcsql)
         uf_perguntalt_chama("N�o foi possivel proceder ao servi�o de Envio de SMS de acesso." + CHR(13) + "Contacte o suporte.", "OK", "", 32)
         regua(2)
         RETURN .F.
      ENDIF
   ENDIF

   SELECT ucrse1

   lccodfarm = ALLTRIM(ucrse1.u_codfarm)

   SELECT ucrsservername
   lcdbserver = ALLTRIM(ucrsservername.dbserver)

   lcnomejar = "LtsDispensaEletronica.jar"

   IF uf_gerais_getparameter("ADM0000000227", "BOOL")
      lctestjar = "1"
   ELSE
      lctestjar = "0"
   ENDIF

   lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\DEM\'+ ALLTRIM(lcnomejar) +' ' ;
              + ' ACESSOHISTDISPENSAS' ;
              + ' --chavePedidoRelacionado=' + ALLTRIM(uv_token) ;
              + ' --token=' + ALLTRIM(uv_token) ;
              + ' --nrSNS=' + ALLTRIM(uv_nrUtente) ;
              + ' --iniciais=' + ALLTRIM(m_chinis) ;
              + ' --codFarmacia=' + ALLTRIM(lccodfarm) ;
              + ' --dbServer=' + ALLTRIM(lcdbserver) ;
              + ' --dbName=' + UPPER(ALLTRIM(sql_db)) ;
              + ' --site=' + UPPER(ALLTRIM(STRTRAN(ALLTRIM(mysite), ' ', '_'))) ;
              + ' --test=' + lctestjar

   IF lctestjar == "1"
      _clipText = lcwspath
      uf_perguntalt_chama("Webservice teste...", "OK", "")
   ENDIF

   regua(1, 2, "A enviar SMS...")

   lcwspath = "javaw -jar "+myEncodingFileJar+lcwspath
   owsshell = CREATEOBJECT("WScript.Shell")
   owsshell.run(lcwspath, 1, .T.)

   regua(2)

   uv_msgRetorno = ALLTRIM(uf_gerais_getUmValor("spmsAcessoCliente", "CONVERT(VARCHAR(254),descr)", "token = '" + ALLTRIM(uv_token) + "'"))

   uf_perguntalt_chama(IIF(!EMPTY(uv_msgRetorno), ALLTRIM(uv_msgRetorno), "Erro a comunicar com o servi�o." + CHR(13) + "Por favor contacte o suporte."), "OK", "")
   
ENDFUNC

FUNCTION uf_histDem_consultaHist
   LPARAMETERS uv_nrUtente, uv_codAcesso, uv_dataIni, uv_dataFim, uv_fromPainel

   IF EMPTY(uv_nrUtente)
      IF uv_fromPainel
         uf_perguntalt_chama("Tem de preencher o campo 'N� Utente'.", "OK", "", 48)
         HISTDEM.txtNrUtente.setFocus()
      ENDIF
      RETURN .F.
   ENDIF

   IF EMPTY(uv_codAcesso)
      IF uv_fromPainel
         uf_perguntalt_chama("Tem de preencher o campo 'C�digo de Acesso'.", "OK", "", 48)
         HISTDEM.txtCodAcesso.setFocus()
      ENDIF
      RETURN .F.
   ENDIF

   IF EMPTY(uv_dataIni)
      IF uv_fromPainel
         uf_perguntalt_chama("Tem de preencher o campo 'Data In�cio'.", "OK", "", 48)
         HISTDEM.DataIni.setFocus()
      ENDIF
      RETURN .F.
   ENDIF

   IF EMPTY(uv_dataFim)
      IF uv_fromPainel
         uf_perguntalt_chama("Tem de preencher o campo 'Data Fim'.", "OK", "", 48)
         HISTDEM.DataFim.setFocus()
      ENDIF
      RETURN .F.
   ENDIF

   regua(0, 2, "A consultar Hist�rico...")
   regua(1, 2, "A consultar Hist�rico...")

   IF  !USED("ucrsServerName")
      TEXT TO lcsql TEXTMERGE NOSHOW
         Select @@SERVERNAME as dbServer
      ENDTEXT
      IF  .NOT. uf_gerais_actgrelha("", "ucrsServerName", lcsql)
         uf_perguntalt_chama("N�o foi possivel proceder ao servi�o de Envio de SMS de acesso." + CHR(13) + "Contacte o suporte.", "OK", "", 32)
         regua(2)
         RETURN .F.
      ENDIF
   ENDIF

   SELECT ucrse1

   lccodfarm = ALLTRIM(ucrse1.u_codfarm)

   SELECT ucrsservername
   lcdbserver = ALLTRIM(ucrsservername.dbserver)

   lcnomejar = "LtsDispensaEletronica.jar"

   IF uf_gerais_getparameter("ADM0000000227", "BOOL")
      lctestjar = "1"
   ELSE
      lctestjar = "0"
   ENDIF

   uv_groupToken = uf_gerais_stamp()

   lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\DEM\'+ ALLTRIM(lcnomejar) +' ' ;
              + ' CONSULTAHISTDISPENSAS' ;
              + ' --chavePedido=' + ALLTRIM(uv_groupToken) ;
              + ' --dataIni=' + uf_gerais_getdate(uv_dataIni, "SQL") ;
              + ' --dataFim=' + uf_gerais_getdate(uv_dataFim, "SQL") ;
              + ' --token=' + ALLTRIM(uv_groupToken) ;
              + ' --nrSNS=' + ALLTRIM(uv_nrUtente) ;
              + ' --iniciais=' + ALLTRIM(m_chinis) ;
              + ' --codFarmacia=' + ALLTRIM(lccodfarm) ;
              + ' --dbServer=' + ALLTRIM(lcdbserver) ;
              + ' --dbName=' + UPPER(ALLTRIM(sql_db)) ;
              + ' --site=' + UPPER(ALLTRIM(STRTRAN(ALLTRIM(mysite), ' ', '_'))) ;
              + ' --test=' + lctestjar ;
              + ' --pinReceita=' + ALLTRIM(uv_codAcesso)

   IF lctestjar == "1"
      _clipText = lcwspath
      uf_perguntalt_chama("Webservice teste...", "OK", "")
   ENDIF

   regua(1, 2, "A consultar Hist�rico...")

   lcwspath = "javaw -jar "+myEncodingFileJar+lcwspath
   owsshell = CREATEOBJECT("WScript.Shell")
   owsshell.run(lcwspath, 1, .T.)

   IF uf_gerais_getUmValor("Dispensa_Eletronica", "count(*)", "resultado_consulta_cod = 'CPF0001' and groupToken = '" + ALLTRIM(uv_groupToken) + "'") = 0

      uv_returnMsg = ALLTRIM(uf_gerais_getUmValor("Dispensa_Eletronica", "CONVERT(VARCHAR(254), resultado_consulta_descr)", "groupToken = '" + ALLTRIM(uv_groupToken) + "'"))

      uf_perguntalt_chama( IIF(!EMPTY(uv_returnMsg), uv_returnMsg, "Erro a comunicar com o servi�o." + CHR(13) + "Por favor contacte o suporte."), "OK", "", 48)

      uf_histDem_createCab('')
      uf_histDem_createLin('')

      IF uv_fromPainel
         HISTDEM.groupToken = ''
      ENDIF

      regua(2)
      RETURN .F.

   ELSE

      IF uv_fromPainel
         HISTDEM.groupToken = ALLTRIM(uv_groupToken)
      ENDIF

      uf_histDem_createCab(uv_groupToken)

   ENDIF

   regua(2)

ENDFUNC

FUNCTION uf_histDem_consultar

   uf_histDem_consultaHist(HISTDEM.txtNrUtente.value, HISTDEM.txtCodAcesso.value, HISTDEM.dataIni.value, HISTDEM.dataFim.value, .T.)

ENDFUNC

FUNCTION uf_histDem_consultarRNU

   IF EMPTY(HISTDEM.txtNrUtente.value)
      uf_perguntalt_chama("Tem de preencher o campo 'N� Utente'.", "OK", "", 48)
      HISTDEM.txtNrUtente.setFocus()
      RETURN .F.
   ENDIF

   IF !uf_utentes_sincronizarRNU(ALLTRIM(HISTDEM.txtNrUtente.value),"")
      RETURN .F.
   ENDIF

   SELECT uc_rnu

   IF RECCOUNT("uc_rnu") = 0
      uf_perguntalt_chama("Erro a comunicar com o servi�o RNU.", "OK", "", 48)
   ENDIF


   IF uc_rnu.cl_obitornu
      uf_perguntalt_chama("Aten��o: Este utente tem o campo �bito ativo na ficha, o que poder� gerar receitu�rio devolvido.", "OK", "", 48)
   ENDIF

   IF EMPTY(uc_rnu.cl_tlmvl)
      uf_perguntalt_chama("Este utente n�o tem nenhum n�mero de telem�vel associado.", "OK", "", 48)
      RETURN .F.
   ENDIF

   uf_perguntalt_chama("Contacto associado ao Registo Nacional de Utentes: " + ALLTRIM(uc_rnu.cl_tlmvl), "OK", "", 64)

   FECHA('uc_rnu')

ENDFUNC

FUNCTION uf_histDem_gravar

   IF !WEXIST("DADOSDEM")
      RETURN .F.
   ENDIF

   IF !USED("uc_histDemCab")
      uf_perguntalt_chama("N�o existe nenhuma receita por dispensar.", "OK", "", 48)
      RETURN .F.
   ENDIF

   SELECT uc_histDemCab

   COUNT TO uv_count FOR !DELETED() AND uc_histDemCab.sel

   IF uv_count = 0
      uf_perguntalt_chama("N�o selecionou nenhuma receita para dispensar.", "OK", "", 48)
      RETURN .F.
   ELSE
      IF uv_count > 1
         uf_perguntalt_chama("N�o pode dispensar mais que uma receita.", "OK", "", 48)
         RETURN .F.
      ENDIF
   ENDIF

   FECHA("uc_histDemCab_tmp")

   SELECT * FROM uc_histDemCab WITH (BUFFERING = .F.) WHERE uc_histDemCab.sel INTO CURSOR uc_histDemCab_tmp

   SELECT uc_histDemCab_tmp
   GO TOP

   DADOSDEM.txtNrSNS.value = "" &&ALLTRIM(HISTDEM.txtNrUtente.value)
   DADOSDEM.txtReceita.value = ALLTRIM(uc_histDemCab_tmp.receita_nr)
   DADOSDEM.txtCodAcesso.value = ALLTRIM(HISTDEM.txtCodAcesso.value)
   DADOSDEM.txtCodDirOpcao.value = ALLTRIM(HISTDEM.txtDireitoOpcao.value)

   FECHA("uc_histDemCab_tmp")

   uf_histDem_sair()

ENDFUNC

FUNCTION uf_histDem_imprimir


   IF USED("uCrsBenef")
      fecha('uCrsBenef')
   ENDIF
   IF USED("uCrsFt")
      fecha("uCrsFt")
   ENDIF
   IF USED("uCrsFt2")
      fecha("uCrsFt2")
   ENDIF
   IF USED("uCrsFi")
      fecha("uCrsFi")
   ENDIF
   SELECT * FROM ft WITH (BUFFERING=.T.) INTO CURSOR uCrsFt READWRITE
   SELECT * FROM ft2 WITH (BUFFERING=.T.) INTO CURSOR uCrsFt2 READWRITE
   SELECT * FROM fi WITH (BUFFERING=.T.) INTO CURSOR uCrsFi READWRITE
   SELECT ucrsft

   IF  .NOT. USED("uCrsFt")
      RETURN .F.
   ELSE
      SELECT ucrsft
      IF  .NOT. RECCOUNT()>0
         RETURN .F.
      ENDIF
   ENDIF

   SELECT ucrsft
   SELECT ucrsft
   GOTO TOP
   SELECT ucrsft2
   GOTO TOP
   SELECT ucrsfi
   GOTO TOP

   LOCAL lcnrreceita

   SELECT * FROM uc_histDemCab WITH (BUFFERING = .T.) INTO CURSOR uc_histDemCab_tmp

   SELECT uc_histDemCab_tmp
   GO TOP

   COUNT TO uv_count FOR !DELETED() AND uc_histDemCab_tmp.sel

   IF uv_count = 0
      uf_perguntalt_chama("N�o selecionou nenhuma Receita para imprimir.", "OK", "", 48)
      RETURN .F.
   ENDIF

   SCAN FOR uc_histDemCab_tmp.sel

      lcnrreceita = ALLTRIM(uc_histDemCab_tmp.receita_nr)

      TEXT TO uv_sql TEXTMERGE NOSHOW
          exec up_dem_histDemLin '<<ALLTRIM(uc_histDemCab_tmp.token)>>', 0
      ENDTEXT

      IF !uf_gerais_actGrelha("", "uc_histDemLin_tmp", uv_sql)
         uf_perguntalt_chama("Erro a obter os dados da receita." + CHR(13) + "Por favor contacte o suporte.", "OK", "", 48)
         RETURN .F.
      ENDIF

      SELECT token,medicamento_descr, CTOT(uf_gerais_getDate(uc_histDemLin.data_caducidade)) as validade_linha, COUNT(medicamento_descr) AS tot, medicamento_cod, medicamento_cnpem, posologia FROM uc_histDemLin_tmp WITH (BUFFERING=.T.) GROUP BY TOKEN,medicamento_descr, validade_linha, medicamento_cod, medicamento_cnpem, posologia INTO CURSOR uCrsVerificaRespostaDEM_tot READWRITE
   
      IF uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + ALLTRIM(myTerm) + "'")

            IF RECCOUNT("ucrsverificarespostadem_tot") = 0
               RETURN .F.
            ENDIF

            PUBLIC upv_nrReceita, upv_nmCl

            upv_nmCl = ALLTRIM(uc_histDemCab_tmp.utente_descr)

            upv_nrReceita = lcnrreceita

            uv_idImp = uf_gerais_getUmValor("B_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Guia DEM'")

            uf_imprimirgerais_sendprinter_talao(uv_idImp , .f., .F.)    

      ELSE
         lcvalidaimp = uf_gerais_setimpressorapos(.T.)
         IF lcvalidaimp
            ??? CHR(27E0)+CHR(64E0)
            ??? CHR(27)+CHR(99)+CHR(48)+CHR(1)
            ??? CHR(27E0)+CHR(116)+CHR(003)
            ??? CHR(27)+CHR(33)+CHR(1)
            ?? " "
            ? LEFT(ALLTRIM(ucrse1.nomecomp), uf_gerais_getparameter("ADM0000000194", "NUM"))
            ??? CHR(27)+CHR(33)+CHR(1)
            ? SUBSTR(ucrse1.local, 1, 10)
            ?? " Tel:"
            ?? TRANSFORM(ucrse1.telefone, "#########")
            ?? " NIF:"
            ?? TRANSFORM(ucrse1.ncont, "999999999")
            ? ALLTRIM(uf_gerais_getMacrosReports(2))
            ? "Dir. Tec. "
            ?? LEFT(ALLTRIM(ucrse1.u_dirtec), uf_gerais_getparameter("ADM0000000194", "NUM"))
            ??
            uf_gerais_separadorpos()
            ? PADC("MEDICAMENTOS POR DISPENSAR", uf_gerais_getparameter("ADM0000000194", "NUM"), " ")
            uf_gerais_separadorpos()
            SELECT ucrsft
            ? DATETIME()
            SELECT ucrsft
            ? "Op: "
            ?? SUBSTR(ucrsft.vendnm, 1, 40)+" ("+ALLTRIM(STR(ucrsft.vendedor))+")"
            ? " "
            IF dadosdem.txtnomeutente.value=''
                  ? "Cliente: "+ALLTRIM(uCrsAtendComp.nomeutente)
            ELSE
                  ? "Cliente: "+ALLTRIM(dadosdem.txtnomeutente.value)
            ENDIF 
            ? " "
            ? "Receita Nr.: "+ALLTRIM(lcnrreceita)
            ? " "
            ? "Codigo de acesso: _______________ "
            ? " "
            ? "Codigo de op��o : _______________ "
            ? " "
            ? "PRODUTO(S)"
            uf_gerais_separadorpos()
            SELECT ucrsverificarespostadem_tot
            SCAN FOR  .NOT. EMPTY(ucrsverificarespostadem_tot.medicamento_descr)
            ? ALLTRIM(ucrsverificarespostadem_tot.medicamento_descr)
            ? "Validade Prescricao: "+DTOC(ucrsverificarespostadem_tot.validade_linha)+"        Quant: "+ALLTRIM(TRANSFORM(ucrsverificarespostadem_tot.tot, "999"))
            uf_gerais_separadorpos()
            ENDSCAN
            uf_gerais_feedcutpos()
         ENDIF
         uf_gerais_setimpressorapos(.F.)
         SET PRINTER TO DEFAULT
      ENDIF

      SELECT uc_histDemCab_tmp
   ENDSCAN

   fecha('uCrsBenef')
   fecha("uCrsFt")
   fecha("uCrsFt2")
   fecha("uCrsFi")
   fecha("uCrsVerificaRespostaDEM_tot")
   fecha("uc_histDemCab_tmp")


ENDFUNC

FUNCTION uf_histDem_pedirCodAcesso

   IF !uf_histDem_acessoHistDispensas(ALLTRIM(HISTDEM.txtNrUtente.Value), .T.)
      RETURN .F.
   ENDIF

   RETURN .T.

ENDFUNC

FUNCTION uf_histDem_envNotasDem

	IF !USED("uc_histDemCab") OR !USED("uc_histDemLin")
		uf_perguntalt_chama("N�o selecionou nenhuma linha de receita!", "OK", "", 48)
		RETURN .F.
	ENDIF
	
	IF RECCOUNT("uc_histDemCab") = 0 OR RECCOUNT("uc_histDemCab") = 0
 		uf_perguntalt_chama("N�o selecionou nenhuma linha de receita!", "OK", "", 48)
		RETURN .F.
	ENDIF
	
	LOCAL uv_nrReceita, uv_nrLinha, uv_design, uv_operador, uv_utente
	
	SELECT uc_histDemCab
	
	uv_nrReceita = ALLTRIM(uc_histDemCab.receita_nr)	
	uv_utente = ALLTRIM(uc_histDemCab.utente_descr)
	
	SELECT uc_histDemLin
	
	uv_nrLinha = ALLTRIM(uc_histDemLin.id)
	
	IF LEN(ALLTRIM(uv_nrLinha)) < 20
		uf_perguntalt_chama("N�o � permitido adicionar uma nota a esta linha de receita.", "OK", "", 48)
		RETURN .F.
	ENDIF
	
	uv_design = ALLTRIM(uc_histDemLin.medicamento_descr)
	
	SELECT ucrsUser
	
	uv_operador = ALLTRIM(ucrsUser.nome)
	
	uf_envNotasDem_chama(uv_nrReceita, uv_nrLinha, uv_design, uv_operador, uv_utente)

ENDFUNC

FUNCTION uf_histDem_consultNotasDem

   LOCAL uv_nrReceita, uv_nrLinha, uv_operador, uv_operadorInis, uv_utente, uv_design
   STORE '' TO uv_nrReceita, uv_nrLinha, uv_operador, uv_operadorInis, uv_utente, uv_design

	IF USED("uc_histDemCab") OR USED("uc_histDemLin")
		
	   IF RECCOUNT("uc_histDemCab") <> 0 OR RECCOUNT("uc_histDemCab") <> 0
         
         SELECT uc_histDemCab
         
         uv_nrReceita = ALLTRIM(uc_histDemCab.receita_nr)	
         uv_utente = ALLTRIM(uc_histDemCab.utente_descr)
         
         SELECT uc_histDemLin
         
         uv_nrLinha = ALLTRIM(uc_histDemLin.id)
         uv_design = ALLTRIM(uc_histDemLin.medicamento_descr)

      ENDIF

   ENDIF
	
	SELECT ucrsUser
	
	uv_operador = ALLTRIM(ucrsUser.nome)
	uv_operadorInis = ALLTRIM(m_chInis)
	
	uf_histnotasdem_chama(uv_nrReceita, uv_nrLinha, uv_operador, uv_operadorInis, uv_utente, uv_design)

ENDFUNC