FUNCTION uf_histPEMH_chama
	LPARAMETERS uv_nrUtente
	
	IF WEXIST('HISTPEMH')
		uf_histPEMH_sair()
	ENDIF

   uf_histPEMH_createCab()
   uf_histPEMH_createLin()
	
	DO FORM HISTPEMH WITH uv_nrUtente
	
ENDFUNC

FUNCTION uf_histPEMH_sair

	IF !WEXIST('HISTPEMH')
		RETURN .F.
	ENDIF

   FECHA("uc_histPEMHCab")
   FECHA("uc_histPEMHLin")

	HISTPEMH.hide()
	HISTPEMH.release()
	
	
   if(VARTYPE(dadosDEM) != "U")
   		dadosDEM.alwaysOnTop = .T.
   endif	


ENDFUNC

FUNCTION uf_histPEMH_createCab
   LPARAMETERS uv_groupToken,uv_nrUtente
   LOCAL uv_sql, lnColumns, lcCursor, loGrid, lcnrUtente
   
	lcnrUtente = uv_nrUtente
   
   IF !EMPTY(uv_groupToken)
      
      TEXT TO uv_sql TEXTMERGE NOSHOW

         exec up_PEMH_histPEMHCab '<<ALLTRIM(uv_groupToken)>>', '<<lcnrUtente>>'
      ENDTEXT

   ELSE

      TEXT TO uv_sql TEXTMERGE NOSHOW

         set fmtonly on

         exec up_PEMH_histPEMHCab ''
         
         set fmtonly off

      ENDTEXT

   ENDIF
	
   IF WEXIST('HISTPEMH')
      loGrid = HISTPEMH.grdCab
      lcCursor = "uc_histPEMHCab"

	   lnColumns = loGrid.ColumnCount
	
	   IF lnColumns > 0

		   DIMENSION laControlSource[lnColumns]
		   FOR lnColumn = 1 TO lnColumns
			   laControlSource[lnColumn] = loGrid.Columns[lnColumn].ControlSource
		   ENDFOR

	   ENDIF

      loGrid.RecordSource = ""

   ENDIF

   IF !uf_gerais_actGrelha("", "uc_histPEMHCab", uv_sql)
      uf_perguntalt_chama("Erro a carregar os cabe�alhos." + CHR(13) + "Por favor contacte o suporte.", "OK", "", 48)
      RETURN .F.
   ENDIF

   SELECT uc_histPEMHCab
   GO TOP

   IF WEXIST('HISTPEMH')

      loGrid.RecordSource = lcCursor
	   FOR lnColumn = 1 TO lnColumns
   		loGrid.Columns[lnColumn].ControlSource = laControlSource[lnColumn]
	   ENDFOR

      HISTPEMH.refresh()
      
      HISTPEMH.grdCab.receita_nr.width = 192
      HISTPEMH.grdCab.receita_data.width = 100


      HISTPEMH.lblCounter.caption = ASTR(RECCOUNT("uc_histPEMHCab")) + IIF(RECCOUNT("uc_histPEMHCab")= 1,  " Receita", " Receitas")

      IF RECCOUNT("uc_histPEMHCab") > 0
         SELECT uc_histPEMHCab
         uf_histPEMH_createLin(ALLTRIM(uc_histPEMHCab.token))
      ELSE 
      	 TEXT TO uv_sql TEXTMERGE NOSHOW

	         set fmtonly on

	         exec up_PEMH_histPEMHLin ''
	         
	         set fmtonly off

	      ENDTEXT
	      IF !uf_gerais_actGrelha("", "uc_histPEMHLin", uv_sql)
		      uf_perguntalt_chama("Erro a carregar as Linhas." + CHR(13) + "Por favor contacte o suporte.", "OK", "", 48)
		      RETURN .F.
		   ENDIF
      ENDIF

   ENDIF

ENDFUNC

FUNCTION uf_histPEMH_createLin
   LPARAMETERS uv_token

   LOCAL uv_sql, lnColumns, lcCursor, loGrid

   IF !EMPTY(uv_token)
      
      TEXT TO uv_sql TEXTMERGE NOSHOW

         exec up_PEMH_histPEMHLin '<<ALLTRIM(uv_token)>>'

      ENDTEXT

   ELSE

      TEXT TO uv_sql TEXTMERGE NOSHOW

         set fmtonly on

         exec up_PEMH_histPEMHLin ''
         
         set fmtonly off

      ENDTEXT

   ENDIF

   IF WEXIST('HISTPEMH')
      loGrid = HISTPEMH.grdLin
      lcCursor = "uc_histPEMHLin"

	   lnColumns = loGrid.ColumnCount
	
	   IF lnColumns > 0

		   DIMENSION laControlSource[lnColumns]
		   FOR lnColumn = 1 TO lnColumns
			   laControlSource[lnColumn] = loGrid.Columns[lnColumn].ControlSource
		   ENDFOR

	   ENDIF

      loGrid.RecordSource = ""

   ENDIF

   IF !uf_gerais_actGrelha("", "uc_histPEMHLin", uv_sql)
      uf_perguntalt_chama("Erro a carregar as Linhas." + CHR(13) + "Por favor contacte o suporte.", "OK", "", 48)
      RETURN .F.
   ENDIF

   SELECT uc_histPEMHLin
   GO TOP

   IF WEXIST('HISTPEMH')

      loGrid.RecordSource = lcCursor
	
	   FOR lnColumn = 1 TO lnColumns
   		loGrid.Columns[lnColumn].ControlSource = laControlSource[lnColumn]
	   ENDFOR
*!*			IF TYPE(HISTPEMH.grdlIN.qtdMaxDispensavel) <> "U"
*!*				HISTPEMH.grdlIN.qtdMaxDispensavel.width = 80
*!*		      	HISTPEMH.grdLIN.qtt_presc.width = 80
*!*			ENDIF
		
      	HISTPEMH.refresh()
		
      
   ENDIF

ENDFUNC

*!*	PROCEDURE uf_histPEMH_fitGridLin

*!*	   LOCAL uv_widthDesign

*!*	   WITH HISTPEMH.grdLin

*!*	      .dataDispensa.width = 100
*!*	      .data_caducidade.width = 100

*!*	      .medicamento_cnpem.width = 70
*!*	      .medicamento_cod.width = 70

*!*	      .numRegistoDispensado.width = 70
*!*	      .dispensado.width = 75

*!*	      IF .width > 485

*!*	         uv_widthDesign = .width - 485

*!*	         .medicamento_descr.width = uv_widthDesign/2
*!*	         .designDispensado.width = uv_widthDesign/2

*!*	      ELSE

*!*	         .medicamento_descr.width = 350
*!*	         .designDispensado.width = 350

*!*	      ENDIF

*!*	   ENDWITH

*!*	ENDPROC

FUNCTION uf_histPEMH_consultaHist
   LPARAMETERS uv_dataIni, uv_dataFim, uv_fromPainel

	LOCAL uv_nrUtente 
	STORE '' TO uv_nrUtente 

   IF EMPTY(uv_dataIni)
      IF uv_fromPainel
         uf_perguntalt_chama("Tem de preencher o campo 'Data In�cio'.", "OK", "", 48)
         HISTPEMH.DataIni.setFocus()
      ENDIF
      RETURN .F.
   ENDIF

   IF EMPTY(uv_dataFim)
      IF uv_fromPainel
         uf_perguntalt_chama("Tem de preencher o campo 'Data Fim'.", "OK", "", 48)
         HISTPEMH.DataFim.setFocus()
      ENDIF
      RETURN .F.
   ENDIF

   regua(0, 2, "A consultar Hist�rico...")
   regua(1, 2, "A consultar Hist�rico...")

   IF  !USED("ucrsServerName")
      TEXT TO lcsql TEXTMERGE NOSHOW
         Select @@SERVERNAME as dbServer
      ENDTEXT
      IF  .NOT. uf_gerais_actgrelha("", "ucrsServerName", lcsql)
         uf_perguntalt_chama("N�o foi possivel proceder ao servi�o de Envio de SMS de acesso." + CHR(13) + "Contacte o suporte.", "OK", "", 32)
         regua(2)
         RETURN .F.
      ENDIF
   ENDIF

   SELECT ucrse1

   lccodfarm = ALLTRIM(ucrse1.u_codfarm)

   SELECT ucrsservername
   lcdbserver = ALLTRIM(ucrsservername.dbserver)

   lcnomejar = "LtsDispensaEletronica.jar"

   IF uf_gerais_getparameter("ADM0000000227", "BOOL")
      lctestjar = "1"
   ELSE
      lctestjar = "0"
   ENDIF

   uv_groupToken = uf_gerais_stamp()

   lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\DEM\'+ ALLTRIM(lcnomejar) +' ' ;
              + ' CONSULTA_PRESC_PEMH' ;
              + ' --chavePedido=' + ALLTRIM(uv_groupToken) ;
              + ' --dataIni=' + uf_gerais_getdate(uv_dataIni, "SQL") ;
              + ' --dataFim=' + uf_gerais_getdate(uv_dataFim, "SQL") ;
              + ' --token=' + ALLTRIM(uv_groupToken) ;
              + ' --iniciais=' + ALLTRIM(m_chinis) ;
              + ' --codFarmacia=' + ALLTRIM(lccodfarm) ;
              + ' --dbServer=' + ALLTRIM(lcdbserver) ;
              + ' --dbName=' + UPPER(ALLTRIM(sql_db)) ;
              + ' --site=' + UPPER(ALLTRIM(STRTRAN(ALLTRIM(mysite), ' ', '_'))) ;
              + ' --test=' + lctestjar ;

   IF lctestjar == "1"
      _clipText = lcwspath
      uf_perguntalt_chama("Webservice teste...", "OK", "")
   ENDIF

   regua(1, 2, "A consultar Hist�rico...")

   lcwspath = "javaw -jar "+myEncodingFileJar+lcwspath
   owsshell = CREATEOBJECT("WScript.Shell")
   owsshell.run(lcwspath, 1, .T.)

   IF uf_gerais_getUmValor("Dispensa_Eletronica", "count(*)", "resultado_consulta_cod = '10400100001' and groupToken = '" + ALLTRIM(uv_groupToken) + "'") = 0

      uv_returnMsg = ALLTRIM(uf_gerais_getUmValor("Dispensa_Eletronica", "CONVERT(VARCHAR(254), resultado_consulta_descr)", "groupToken = '" + ALLTRIM(uv_groupToken) + "'"))

      uf_perguntalt_chama( IIF(!EMPTY(uv_returnMsg), uv_returnMsg, "Erro a comunicar com o servi�o." + CHR(13) + "Por favor contacte o suporte."), "OK", "", 48)

      uf_histPEMH_createCab('')
      uf_histPEMH_createLin('')

      IF uv_fromPainel
         HISTPEMH.groupToken = ''
      ENDIF

      regua(2)
      RETURN .F.

   ELSE
   
   

     IF uv_fromPainel
         HISTPEMH.groupToken = ALLTRIM(uv_groupToken)
     ENDIF

	IF TYPE("HistPemh") <>"U"
		uv_nrUtente= HistPemh.txtNrUtente.value
	ELSE 
   		uv_nrUtente = ''
   	ENDIF
      uf_histPEMH_createCab(uv_groupToken, uv_nrUtente )

   ENDIF

   regua(2)

ENDFUNC

FUNCTION uf_histPEMH_consultar

   uf_histPEMH_consultaHist(HISTPEMH.dataIni.value, HISTPEMH.dataFim.value, .T.)

ENDFUNC

FUNCTION uf_histPEMH_gravar

   uf_histPEMH_sair()
ENDFUNC




*!*	FUNCTION uf_histPEMH_imprimir


*!*	   IF USED("uCrsBenef")
*!*	      fecha('uCrsBenef')
*!*	   ENDIF
*!*	   IF USED("uCrsFt")
*!*	      fecha("uCrsFt")
*!*	   ENDIF
*!*	   IF USED("uCrsFt2")
*!*	      fecha("uCrsFt2")
*!*	   ENDIF
*!*	   IF USED("uCrsFi")
*!*	      fecha("uCrsFi")
*!*	   ENDIF
*!*	   SELECT * FROM ft WITH (BUFFERING=.T.) INTO CURSOR uCrsFt READWRITE
*!*	   SELECT * FROM ft2 WITH (BUFFERING=.T.) INTO CURSOR uCrsFt2 READWRITE
*!*	   SELECT * FROM fi WITH (BUFFERING=.T.) INTO CURSOR uCrsFi READWRITE
*!*	   SELECT ucrsft

*!*	   IF  .NOT. USED("uCrsFt")
*!*	      RETURN .F.
*!*	   ELSE
*!*	      SELECT ucrsft
*!*	      IF  .NOT. RECCOUNT()>0
*!*	         RETURN .F.
*!*	      ENDIF
*!*	   ENDIF

*!*	   SELECT ucrsft
*!*	   SELECT ucrsft
*!*	   GOTO TOP
*!*	   SELECT ucrsft2
*!*	   GOTO TOP
*!*	   SELECT ucrsfi
*!*	   GOTO TOP

*!*	   LOCAL lcnrreceita

*!*	   SELECT * FROM uc_histPEMHCab WITH (BUFFERING = .T.) INTO CURSOR uc_histPEMHCab_tmp

*!*	   SELECT uc_histPEMHCab_tmp
*!*	   GO TOP

*!*	   COUNT TO uv_count FOR !DELETED() AND uc_histPEMHCab_tmp.sel

*!*	   IF uv_count = 0
*!*	      uf_perguntalt_chama("N�o selecionou nenhuma Receita para imprimir.", "OK", "", 48)
*!*	      RETURN .F.
*!*	   ENDIF

*!*	   SCAN FOR uc_histPEMHCab_tmp.sel

*!*	      lcnrreceita = ALLTRIM(uc_histPEMHCab_tmp.receita_nr)

*!*	      TEXT TO uv_sql TEXTMERGE NOSHOW
*!*	          exec up_PEMH_histPEMHLin '<<ALLTRIM(uc_histPEMHCab_tmp.token)>>', 0
*!*	      ENDTEXT

*!*	      IF !uf_gerais_actGrelha("", "uc_histPEMHLin_tmp", uv_sql)
*!*	         uf_perguntalt_chama("Erro a obter os dados da receita." + CHR(13) + "Por favor contacte o suporte.", "OK", "", 48)
*!*	         RETURN .F.
*!*	      ENDIF

*!*	      SELECT token,medicamento_descr, CTOT(uf_gerais_getDate(uc_histPEMHLin.data_caducidade)) as validade_linha, COUNT(medicamento_descr) AS tot, medicamento_cod, medicamento_cnpem, posologia FROM uc_histPEMHLin_tmp WITH (BUFFERING=.T.) GROUP BY TOKEN,medicamento_descr, validade_linha, medicamento_cod, medicamento_cnpem, posologia INTO CURSOR uCrsVerificaRespostaPEMH_tot READWRITE
*!*	   
*!*	      IF uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + ALLTRIM(myTerm) + "'")

*!*	            IF RECCOUNT("ucrsverificarespostaPEMH_tot") = 0
*!*	               RETURN .F.
*!*	            ENDIF

*!*	            PUBLIC upv_nrReceita, upv_nmCl

*!*	            upv_nmCl = ALLTRIM(uc_histPEMHCab_tmp.utente_descr)

*!*	            upv_nrReceita = lcnrreceita

*!*	            uv_idImp = uf_gerais_getUmValor("B_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Guia PEMH'")

*!*	            uf_imprimirgerais_sendprinter_talao(uv_idImp , .f., .F.)    

*!*	      ELSE
*!*	         lcvalidaimp = uf_gerais_setimpressorapos(.T.)
*!*	         IF lcvalidaimp
*!*	            ??? CHR(27E0)+CHR(64E0)
*!*	            ??? CHR(27)+CHR(99)+CHR(48)+CHR(1)
*!*	            ??? CHR(27E0)+CHR(116)+CHR(003)
*!*	            ??? CHR(27)+CHR(33)+CHR(1)
*!*	            ?? " "
*!*	            ? LEFT(ALLTRIM(ucrse1.nomecomp), uf_gerais_getparameter("ADM0000000194", "NUM"))
*!*	            ??? CHR(27)+CHR(33)+CHR(1)
*!*	            ? SUBSTR(ucrse1.local, 1, 10)
*!*	            ?? " Tel:"
*!*	            ?? TRANSFORM(ucrse1.telefone, "#########")
*!*	            ?? " NIF:"
*!*	            ?? TRANSFORM(ucrse1.ncont, "999999999")
*!*	            ? ALLTRIM(uf_gerais_getMacrosReports(2))
*!*	            ? "Dir. Tec. "
*!*	            ?? LEFT(ALLTRIM(ucrse1.u_dirtec), uf_gerais_getparameter("ADM0000000194", "NUM"))
*!*	            ??
*!*	            uf_gerais_separadorpos()
*!*	            ? PADC("MEDICAMENTOS POR DISPENSAR", uf_gerais_getparameter("ADM0000000194", "NUM"), " ")
*!*	            uf_gerais_separadorpos()
*!*	            SELECT ucrsft
*!*	            ? DATETIME()
*!*	            SELECT ucrsft
*!*	            ? "Op: "
*!*	            ?? SUBSTR(ucrsft.vendnm, 1, 40)+" ("+ALLTRIM(STR(ucrsft.vendedor))+")"
*!*	            ? " "
*!*	            IF dadosPEMH.txtnomeutente.value=''
*!*	                  ? "Cliente: "+ALLTRIM(uCrsAtendComp.nomeutente)
*!*	            ELSE
*!*	                  ? "Cliente: "+ALLTRIM(dadosPEMH.txtnomeutente.value)
*!*	            ENDIF 
*!*	            ? " "
*!*	            ? "Receita Nr.: "+ALLTRIM(lcnrreceita)
*!*	            ? " "
*!*	            ? "Codigo de acesso: _______________ "
*!*	            ? " "
*!*	            ? "Codigo de op��o : _______________ "
*!*	            ? " "
*!*	            ? "PRODUTO(S)"
*!*	            uf_gerais_separadorpos()
*!*	            SELECT ucrsverificarespostaPEMH_tot
*!*	            SCAN FOR  .NOT. EMPTY(ucrsverificarespostaPEMH_tot.medicamento_descr)
*!*	            ? ALLTRIM(ucrsverificarespostaPEMH_tot.medicamento_descr)
*!*	            ? "Validade Prescricao: "+DTOC(ucrsverificarespostaPEMH_tot.validade_linha)+"        Quant: "+ALLTRIM(TRANSFORM(ucrsverificarespostaPEMH_tot.tot, "999"))
*!*	            uf_gerais_separadorpos()
*!*	            ENDSCAN
*!*	            uf_gerais_feedcutpos()
*!*	         ENDIF
*!*	         uf_gerais_setimpressorapos(.F.)
*!*	         SET PRINTER TO DEFAULT
*!*	      ENDIF

*!*	      SELECT uc_histPEMHCab_tmp
*!*	   ENDSCAN

*!*	   fecha('uCrsBenef')
*!*	   fecha("uCrsFt")
*!*	   fecha("uCrsFt2")
*!*	   fecha("uCrsFi")
*!*	   fecha("uCrsVerificaRespostaPEMH_tot")
*!*	   fecha("uc_histPEMHCab_tmp")


*!*	ENDFUNC

