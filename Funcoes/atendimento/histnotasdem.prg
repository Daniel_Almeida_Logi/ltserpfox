FUNCTION uf_histnotasdem_chama
   LPARAMETERS uv_nrReceita, uv_nrLinha, uv_operador, uv_operadorInis, uv_utente, uv_design

	IF !uf_gerais_checkCedula()
		RETURN .F.
	ENDIF
	
	IF WEXIST("HISTNOTASDEM")
		uf_histnotasdem_sair()
	ENDIF

	uf_histnotasdem_criarCurs('')

	DO FORM HISTNOTASDEM WITH uv_nrReceita, uv_nrLinha, uv_operador, uv_operadorInis, uv_utente, uv_design
	
ENDFUNC

FUNCTION uf_histnotasdem_sair

		
	fecha("uc_histNotasDEMAux")
	fecha("uc_histNotasDEM")	

	IF WEXIST("HISTNOTASDEM")
		HISTNOTASDEM.hide()
		HISTNOTASDEM.release()
	ENDIF

ENDFUNC

FUNCTION uf_histnotasdem_criarCurs
	LPARAMETERS uv_token, uv_nrLinha, uv_nrReceita, uv_utente, uv_design

	LOCAL uv_sql, uv_filtroToken, lnColumns, lcCursor, loGrid, uv_filtroNrLinha, uv_filtroNrReceita, uv_utenteFiltro, uv_designFiltro
	STORE '' TO uv_sql, uv_filtroToken, uv_filtroNrLinha, uv_filtroNrReceita, uv_utenteFiltro, uv_designFiltro

	IF !EMPTY(uv_token)
		uv_filtroToken = ALLTRIM(uv_token)
	ENDIF

	IF !EMPTY(uv_nrLinha)
		uv_filtroNrLinha = ALLTRIM(uv_nrLinha)
	ENDIF

	IF !EMPTY(uv_nrReceita)
		uv_filtroNrReceita = ALLTRIM(uv_nrReceita)
	ENDIF

	IF !EMPTY(uv_utente)
		uv_utenteFiltro = ALLTRIM(uv_utente)
	ENDIF

	IF !EMPTY(uv_design)
		uv_designFiltro = ALLTRIM(uv_design)
	ENDIF

	TEXT TO uv_sql TEXTMERGE NOSHOW

		exec up_dem_getHistNotas '<<ALLTRIM(uv_filtroToken)>>', '<<ALLTRIM(uv_filtroNrLinha)>>', '<<ALLTRIM(uv_filtroNrReceita)>>', '<<ALLTRIM(uv_utenteFiltro)>>', '<<ALLTRIM(uv_designFiltro)>>'

	ENDTEXT

	IF WEXIST("HISTNOTASDEM")

		loGrid = HISTNOTASDEM.grdHist
		lcCursor = "uc_histNotasDEM"

		lnColumns = loGrid.ColumnCount

		IF lnColumns > 0

			DIMENSION laControlSource[lnColumns]
			FOR lnColumn = 1 TO lnColumns
				laControlSource[lnColumn] = loGrid.Columns[lnColumn].ControlSource
			ENDFOR

		ENDIF

		loGrid.RecordSource = ""

	ENDIF

	IF !uf_gerais_actGrelha("", "uc_histNotasDEM", uv_sql)
		uf_perguntalt_chama("Erro a carregar o hist�rico." + CHR(13) + "Por favor contacte o suporte.", "OK", "", 48)
		RETURN .F.
	ENDIF

	SELECT uc_histNotasDEM
   	GO TOP

	IF WEXIST('HISTNOTASDEM')

		loGrid.RecordSource = lcCursor
		
		FOR lnColumn = 1 TO lnColumns
			loGrid.Columns[lnColumn].ControlSource = laControlSource[lnColumn]
		ENDFOR

		loGrid.autoFit()

	ENDIF

ENDFUNC

FUNCTION uf_histnotasdem_validaCamposOrig

	IF EMPTY(HISTNOTASDEM.txtOperador.Tag)
		uf_perguntalt_chama("O campo Operador � de preenchimento obrigat�rio.", "OK", "", 48)
		HISTNOTASDEM.txtOperador.setFocus()
		RETURN .F.
	ENDIF

	IF EMPTY(HISTNOTASDEM.dataIni.value)
		uf_perguntalt_chama("O Data In�cio � de preenchimento obrigat�rio.", "OK", "", 48)
		HISTNOTASDEM.dataIni.setFocus()
		RETURN .F.
	ENDIF


	RETURN .T.

ENDFUNC

FUNCTION uf_histnotasdem_consult

	IF !uf_histnotasdem_validaCamposOrig()
		RETURN .F.
	ENDIF

	LOCAL uv_token, lcdbserver, lcnomejar, lctestjar, lcwspath, lccodfarm

	uv_token = uf_gerais_stamp()

	IF  !USED("ucrsServerName")
		TEXT TO lcsql TEXTMERGE NOSHOW
			Select @@SERVERNAME as dbServer
		ENDTEXT
		IF  .NOT. uf_gerais_actgrelha("", "ucrsServerName", lcsql)
			uf_perguntalt_chama("N�o foi possivel proceder ao servi�o de Envio de SMS de acesso." + CHR(13) + "Contacte o suporte.", "OK", "", 32)
			regua(2)
			RETURN .F.
		ENDIF
	ENDIF

	SELECT ucrse1

	lccodfarm = ALLTRIM(ucrse1.u_codfarm)

	SELECT ucrsservername
	lcdbserver = ALLTRIM(ucrsservername.dbserver)

	lcnomejar = "LtsDispensaEletronica.jar"

	IF uf_gerais_getparameter("ADM0000000227", "BOOL")
		lctestjar = "1"
	ELSE
		lctestjar = "0"
	ENDIF

	lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\DEM\'+ ALLTRIM(lcnomejar) +' ' ;
				+ ' CONSULTA_NOTA' ;
				+ ' --chavePedido=' + uf_gerais_stamp() ;
				+ ' --dataIni=' + uf_gerais_getDate(HISTNOTASDEM.dataIni.value, "SQL") ;
				+ ' --token=' + ALLTRIM(uv_token) ;
				+ ' --iniciais=' + ALLTRIM(HISTNOTASDEM.txtOperador.Tag) ;
				+ ' --codFarmacia=' + ALLTRIM(lccodfarm) ;
				+ ' --dbServer=' + ALLTRIM(lcdbserver) ;
				+ ' --dbName=' + UPPER(ALLTRIM(sql_db)) ;
				+ ' --site=' + UPPER(ALLTRIM(STRTRAN(ALLTRIM(mysite), ' ', '_'))) ;
				+ ' --test=' + lctestjar

	IF lctestjar == "1"
		_clipText = lcwspath
		uf_perguntalt_chama("Webservice teste...", "OK", "")
	ENDIF

	regua(1, 2, "A consultar Notas...")

	lcwspath = "javaw -jar "+myEncodingFileJar+lcwspath
	owsshell = CREATEOBJECT("WScript.Shell")
	owsshell.run(lcwspath, 1, .T.)
	
	if(uf_histnotasdem_validaRespostaServico(uv_token))		
		uf_histnotasdem_criarCurs(uv_token, ALLTRIM(HISTNOTASDEM.txtDesign.tag), ALLTRIM(HISTNOTASDEM.txtNrReceita.value), ALLTRIM(HISTNOTASDEM.txtUtente.value), ALLTRIM(HISTNOTASDEM.txtDesign.value))
	ELSE
		uf_histnotasdem_criarCurs('')
	ENDIF
	
	regua(2)

ENDFUNC

FUNCTION uf_histnotasdem_validaRespostaServico
	LPARAMETERS uv_token 
	
	LOCAL lcResp
	lcResp = ''
	fecha("uc_histNotasDEMAux")
	
	LOCAL lcSql
	lcSql = ''
	
	
	TEXT TO lcSql TEXTMERGE NOSHOW
		exec up_dem_getHistNotasDevolveResultado '<<ALLTRIM(uv_token)>>'
	ENDTEXT
	
	IF !uf_gerais_actGrelha("","uc_histNotasDEMAux",lcSql)
		lcResp  = "Erro ao consultar resultado do hist�rico." + CHR(13) + "Por favor contacte o suporte."
		uf_perguntalt_chama(lcResp, "OK", "", 48)
		RETURN .f.	
	ENDIF
	
	if(USED("uc_histNotasDEMAux"))	
		SELECT uc_histNotasDEMAux
	   	GO TOP
	   	if(ALLTRIM(uc_histNotasDEMAux.codigo)!="100006010001") OR RECCOUNT("uc_histNotasDEMAux") == 0		
	   		lcResp = ALLTRIM(uc_histNotasDEMAux.descricao)
	   		if(EMPTY(lcResp))
	   			lcResp  = "Erro ao consultar o hist�rico." + CHR(13) + "Por favor contacte o suporte."
	   		ENDIF
	   		fecha("uc_histNotasDEMAux")	
	   		uf_perguntalt_chama(lcResp, "OK", "", 48)
			RETURN .F.
	   	ENDIF	
	ENDIF
		
	fecha("uc_histNotasDEMAux")	
	RETURN .t.
	 	
ENDFUNC
