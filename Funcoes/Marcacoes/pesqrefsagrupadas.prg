**
FUNCTION uf_pesqrefsagrupadas_chama
	LPARAMETERS tcBool, lcOrigem, lcNaoAlteraLocal

	IF TYPE("pesqrefsagrupadas")=="U"
	
		
		** Calcula conven��o particular do local caso exsits
		
		TEXT TO lcSQL TEXTMERGE NOSHOW
			Select top 1 descr from cpt_conv where site = '<<ALLTRIM(mysite)>>' AND descr like '%particular%'
		ENDTEXT 
		IF !uf_gerais_actgrelha("", "ucrsPesqRefsConvencaoParticularDefault", lcSql)
			MESSAGEBOX("N�o foi possivel verificar existencia de conven��o particular.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
		
		
		lcConvencaoParticularDefault = ""			
		IF RECCOUNT("ucrsPesqRefsConvencaoParticularDefault")>0
			SELECT ucrsPesqRefsConvencaoParticularDefault
			lcConvencaoParticularDefault = ALLTRIM(ucrsPesqRefsConvencaoParticularDefault.descr )
		ENDIF 
		
		IF !USED("ucrsPesqRefsAgrupadas")
			TEXT TO lcSQL TEXTMERGE NOSHOW
				 exec up_marcacoes_pesqRefsAgrupadas '','','','','<<lcConvencaoParticularDefault>>','',<<mysite_nr>>
			ENDTEXT 
			IF !uf_gerais_actgrelha("", "ucrsPesqRefsAgrupadas", lcSql)
				MESSAGEBOX("N�o foi possivel aplicar defini��es pesquisa. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
				RETURN .f.
			ENDIF
		ENDIF
		
		DO FORM pesqrefsagrupadas WITH lcNaoAlteraLocal
		
		**
		uf_pesqrefsagrupadas_carregaMenu()
	ELSE
		pesqrefsagrupadas.show
	ENDIF

	IF !EMPTY(lcOrigem)
		pesqrefsagrupadas.origem = lcOrigem
	ENDIF 
	pesqrefsagrupadas.abrepainelseries = tcBool
	
	IF EMPTY(tcBool)
		pesqrefsagrupadas.especialidade.click
	ENDIF 

ENDFUNC 


**
FUNCTION uf_pesqrefsagrupadas_carregaMenu
	WITH pesqrefsagrupadas
		.menu1.adicionaOpcao("actualizar", "Actualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_pesqrefsagrupadas_actPesquisa","A")
		.menu1.estado("", "SHOW", "Aplicar", .t., "Cancelar", .t.)
	ENDWITH
ENDFUNC


**
FUNCTION uf_pesqrefsagrupadas_actPesquisa

	
	SELECT * FROM ucrsPesqRefsAgrupadas WHERE !EMPTY(Sel) INTO CURSOR ucrsPesqRefsAgrupadasSelecionadas READWRITE
	
	SELECT ucrsPesqRefsAgrupadas 
	GO Top
	SCAN 
		DELETE 
	ENDSCAN 
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_marcacoes_pesqRefsAgrupadas '<<ALLTRIM(pesqrefsagrupadas.ref.value)>>','<<ALLTRIM(pesqrefsagrupadas.design.value)>>','<<ALLTRIM(pesqrefsagrupadas.especialidade.value)>>','<<ALLTRIM(pesqrefsagrupadas.especialista.value)>>','<<ALLTRIM(pesqrefsagrupadas.convencao.value)>>','<<ALLTRIM(pesqrefsagrupadas.site.value)>>',<<mysite_nr>>
	ENDTEXT 

	IF !uf_gerais_actgrelha("", "ucrsPesqRefsAgrupadasAux", lcSql)
		MESSAGEBOX("N�o foi possivel aplicar defini��es pesquisa. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	
	DELETE ucrsPesqRefsAgrupadasAux FROM ucrsPesqRefsAgrupadasAux inner join ucrsPesqRefsAgrupadasSelecionadas on ucrsPesqRefsAgrupadasAux.ref = ucrsPesqRefsAgrupadasSelecionadas.ref
	
	
	SELECT ucrsPesqRefsAgrupadas 
	APPEND FROM DBF("ucrsPesqRefsAgrupadasSelecionadas")
	SELECT ucrsPesqRefsAgrupadas 
	APPEND FROM DBF("ucrsPesqRefsAgrupadasAux")
	
	IF USED("ucrsPesqRefsAgrupadasSelecionadas")
		fecha("ucrsPesqRefsAgrupadasSelecionadas")
	ENDIF
	IF USED("ucrsPesqRefsAgrupadasAux")
		fecha("ucrsPesqRefsAgrupadasAux")
	ENDIF 
	
	SELECT ucrsPesqRefsAgrupadas 
	GO top
	
	pesqrefsagrupadas.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("ucrsPesqRefsAgrupadas"))) + " Resultados"
	pesqrefsagrupadas.refresh
	pesqrefsagrupadas.gridPesq.refresh
ENDFUNC


**
FUNCTION uf_pesqrefsagrupadas_sel
	**
	
	
	
ENDFUNC


**
FUNCTION uf_pesqrefsagrupadas_gravar
	DO CASE 
		CASE EMPTY(pesqrefsagrupadas.origem)
			uf_PLANEAMENTOMR_chama(0)
			
			PLANEAMENTOMR.lockscreen =  .f.
			
			
			SELECT ucrsPesqRefsAgrupadas
			GO TOP 
			SCAN FOR !EMPTY(ucrsPesqRefsAgrupadas.sel)
				lcstamp = uf_gerais_stamp()
				
				SELECT ucrsServicosSerie
				CALCULATE MAX(ucrsServicosSerie.ordem) TO lcMaxOrdem
				IF lcMaxOrdem == 0
					lcMaxOrdem = 1
				ENDIF
				
				SELECT ucrsServicosSerie
				APPEND BLANK
				Replace ucrsServicosSerie.serieservstamp WITH lcstamp
				Replace ucrsServicosSerie.ref WITH ucrsPesqRefsAgrupadas.ref
				Replace ucrsServicosSerie.design WITH ucrsPesqRefsAgrupadas.design
				Replace ucrsServicosSerie.qtt WITH 1
				Replace ucrsServicosSerie.duracao WITH ucrsPesqRefsAgrupadas.u_duracao
				Replace ucrsServicosSerie.mrsimultaneo WITH ucrsPesqRefsAgrupadas.mrsimultaneo
				Replace ucrsServicosSerie.ordem WITH lcMaxOrdem
				PLANEAMENTOMR.ContainerDetalhe.PageFrame1.page3.GridServicosSerie.Refresh
			
				
				** Adiciona Especialidade associada ao servi�o
				TEXT TO lcSQL NOSHOW TEXTMERGE
					select ref,nome,* 
					from 
						b_cli_stRecursos 
					WHERE 
						b_cli_stRecursos.ref = '<<ALLTRIM(ucrsPesqRefsAgrupadas.ref)>>' 
						AND b_cli_stRecursos.marcacao = 1
				ENDTEXT 
		
				IF !uf_gerais_actgrelha("", "uCrsAddEspecialidadeSerie",lcSQL)
					RETURN .f.
				ENDIF 
				
				IF RECCOUNT("uCrsAddEspecialidadeSerie") > 0

					SELECT uCrsAddEspecialidadeSerie
					GO Top
					SCAN &&FOR UPPER(uCrsAddEspecialidadeSerie.tipo) == "UTILIZADOR"
					
						Select uCrsRecursosDaSerie
						LOCATE FOR UPPER(ALLTRIM(uCrsRecursosDaSerie.nome)) == UPPER(ALLTRIM(uCrsAddEspecialidadeSerie.nome))
						IF !FOUND()
							lcStamp = uf_gerais_getId(1)
							Select ucrsSeriesServ
					
							SELECT uCrsRecursosDaSerie
							APPEND BLANK
							
							Replace uCrsRecursosDaSerie.serierecstamp WITH lcStamp
							Replace uCrsRecursosDaSerie.seriestamp WITH ucrsSeriesServ.seriestamp
							Replace uCrsRecursosDaSerie.stamp WITH uCrsAddEspecialidadeSerie.stamp 
							Replace uCrsRecursosDaSerie.ref WITH ucrsPesqRefsAgrupadas.ref
							Replace uCrsRecursosDaSerie.nome WITH uCrsAddEspecialidadeSerie.nome
							Replace uCrsRecursosDaSerie.no WITH uCrsAddEspecialidadeSerie.no
							Replace uCrsRecursosDaSerie.tipo WITH uCrsAddEspecialidadeSerie.tipo 
							
						ENDIF 
					ENDSCAN 				

					Select uCrsRecursosDaSerie
					GO TOP 	 
					PLANEAMENTOMR.ContainerDetalhe.PageFrame1.page3.gridRecursos.refresh
				
				ENDIF
			ENDSCAN 
		
			uf_pesqrefsagrupadas_sair()
			uf_PLANEAMENTOMR_atualizaCalendarioMensal()
			PLANEAMENTOMR.lockscreen =  .f.
			PLANEAMENTOMR.REFRESH

		**
		CASE pesqrefsagrupadas.origem == "PLANEAMENTO"
			
			SELECT ucrsPesqRefsAgrupadas
			GO TOP 
			SCAN FOR !EMPTY(ucrsPesqRefsAgrupadas.sel)
			
				lcstamp = uf_gerais_stamp()
				SELECT ucrsServicosSerie
				CALCULATE MAX(ucrsServicosSerie.ordem) TO lcMaxOrdem
				IF lcMaxOrdem == 0
					lcMaxOrdem = 1
				ENDIF
				
				SELECT ucrsServicosSerie
				APPEND BLANK
				Replace ucrsServicosSerie.serieservstamp WITH lcstamp
				Replace ucrsServicosSerie.ref WITH ucrsPesqRefsAgrupadas.ref
				Replace ucrsServicosSerie.design WITH ucrsPesqRefsAgrupadas.design
				Replace ucrsServicosSerie.qtt WITH 1
				Replace ucrsServicosSerie.duracao WITH ucrsPesqRefsAgrupadas.u_duracao
				Replace ucrsServicosSerie.mrsimultaneo WITH ucrsPesqRefsAgrupadas.mrsimultaneo
				Replace ucrsServicosSerie.ordem WITH lcMaxOrdem
			ENDSCAN 
				
			PLANEAMENTOMR.ContainerDetalhe.PageFrame1.page3.GridServicosSerie.Refresh
			uf_pesqrefsagrupadas_sair()
		&&
		CASE pesqrefsagrupadas.origem == "MARCACOES"
			
			LOCAL lcservmrstamp, lcMaxOrdemServMarc
			
			
			SELECT ucrsPesqRefsAgrupadas
			GO TOP 
			SCAN FOR !EMPTY(ucrsPesqRefsAgrupadas.sel)
				
				lcservmrstamp  = uf_gerais_stamp()
					
				Select UcrsMarcacoesDia
				**
				Select uCrsServMarcacao
				CALCULATE MAX(ordem) TO lcMaxOrdemServMarc
										
				**
				Select uCrsServMarcacao
				APPEND BLANK
				Replace uCrsServMarcacao.ref WITH ucrsPesqRefsAgrupadas.ref
				Replace uCrsServMarcacao.design WITH ucrsPesqRefsAgrupadas.design
				Replace uCrsServMarcacao.ordem WITH IIF(ISNULL(lcMaxOrdemServMarc),1,lcMaxOrdemServMarc+1)
				
				Replace uCrsServMarcacao.servmrstamp WITH lcservmrstamp  
				Replace uCrsServMarcacao.mrstamp WITH UcrsMarcacoesDia.mrstamp
				Replace uCrsServMarcacao.duracao WITH ucrsPesqRefsAgrupadas.u_duracao
				Replace uCrsServMarcacao.qtt WITH 1
				Replace uCrsServMarcacao.dataInicio WITH UcrsMarcacoesDia.data
				Replace uCrsServMarcacao.dataFim WITH UcrsMarcacoesDia.dataFim
				Replace uCrsServMarcacao.hinicio WITH UcrsMarcacoesDia.hinicio
				Replace uCrsServMarcacao.hfim WITH UcrsMarcacoesDia.hfim 
				Replace uCrsServMarcacao.compart WITH 0
				Replace uCrsServMarcacao.pvp WITH ucrsPesqRefsAgrupadas.epv1
				Replace uCrsServMarcacao.total WITH ucrsPesqRefsAgrupadas.epv1
			
							
				** Verificar se tem Conven��o, caso nao tenha aplica o PVP particular do Local
				uf_pesqrefsagrupadas_dadosComparticipacao()


				** Actualiza linhas de Recursos sem referencia associada devido � migra��o
				SELECT ucrsPesqRefsAgrupadas
				lcRef = ucrsPesqRefsAgrupadas.ref
				UPDATE uCrsRecursosServMarcacao SET uCrsRecursosServMarcacao.ref = lcRef WHERE uCrsRecursosServMarcacao.ref = ""

			ENDSCAN
			
			MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page2.GridConsumos.Refresh
			uf_pesqrefsagrupadas_sair()
		&&
		CASE pesqrefsagrupadas.origem == "CONVENCOES"
			
			SELECT ucrsPesqRefsAgrupadas
			GO TOP 
			SCAN FOR !EMPTY(ucrsPesqRefsAgrupadas.sel)
				SELECT ucrsPesqRefsAgrupadas
				SELECT uCrsConvencoesLin
				Replace uCrsConvencoesLin.ref WITH ucrsPesqRefsAgrupadas.ref
				Replace uCrsConvencoesLin.design WITH ucrsPesqRefsAgrupadas.design
				Replace uCrsConvencoesLin.especialidade WITH ucrsPesqRefsAgrupadas.especialidade 
			ENDSCAN
				
			uf_pesqrefsagrupadas_sair()
			
		CASE  pesqrefsagrupadas.origem == "DISPONIBILIDADESRECURSOS"
		
			SELECT ucrsPesqRefsAgrupadas
			GO TOP 
			SCAN FOR !EMPTY(ucrsPesqRefsAgrupadas.sel)
				SELECT ucrsUsDisponibilidades
				lcSeriestamp = ucrsUsDisponibilidades.seriestamp
				lcIdDisp = ucrsUsDisponibilidades.id_disp
				SELECT ucrsUsServicos
				APPEND BLANK
						
				Replace ucrsUsServicos.seriestamp WITH lcSeriestamp
				Replace ucrsUsServicos.id_disp WITH lcIdDisp
				Replace ucrsUsServicos.ref WITH ucrsPesqRefsAgrupadas.ref
				Replace ucrsUsServicos.nome WITH ucrsPesqRefsAgrupadas.design
				Replace ucrsUsServicos.tipo WITH "Servicos"
			ENDSCAN 
			
			uf_utilizadores_actualizaServDisp()
			uf_pesqrefsagrupadas_sair()
		
		CASE pesqrefsagrupadas.origem == "CONFIGHONORARIOS"
			
			uf_CONFIGHONORARIOS_pesqrefsagrupadasSel()
			uf_pesqrefsagrupadas_sair()
	ENDCASE
ENDFUNC 


**
FUNCTION uf_pesqrefsagrupadas_dadosComparticipacao
	LOCAL lcSQL
	
	SELECT UcrsMarcacoesDia
	lcUtstamp = UcrsMarcacoesDia.utstamp
	SELECT uCrsServMarcacao
	lcservmrstamp = uCrsServMarcacao.servmrstamp
	lcRef = uCrsServMarcacao.ref
	
	lcListaUsers = ""
	SELECT uCrsRecursosServMarcacao
	GO Top
	SCAN
		lcListaUsers = lcListaUsers + IIF(!EMPTY(lcListaUsers),",","") + ALLTRIM(uCrsRecursosServMarcacao.nome)
	ENDSCAN 
	
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_clinica_DadosComparticipacao '<<ALLTRIM(lcUtstamp)>>','<<ALLTRIM(lcRef)>>','<<ALLTRIM(mysite)>>','<<ALLTRIM(lcListaUsers)>>'
	ENDTEXT

*!*	_CLIPTEXT = lcSQL
*!*	MESSAGEBOX(lcSQL)

	IF !uf_gerais_actGrelha("", "ucrsDadosComparticipacaoSerieAux",lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VALIDAR DADOS DE COMPARTICIPA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	IF RECCOUNT("ucrsDadosComparticipacaoSerieAux")>0

		** Cursor de Servi�os
		SELECT ucrsDadosComparticipacaoSerieAux	
		lcPVP = ucrsDadosComparticipacaoSerieAux.pvp
		lcValorUtente = ucrsDadosComparticipacaoSerieAux.pvp

		DO CASE
		CASE UPPER(ALLTRIM(ucrsDadosComparticipacaoSerieAux.tipoCompart)) == UPPER(ALLTRIM("Valor")) && Comparticipa��o em valor

			lcValorUtente =  ucrsDadosComparticipacaoSerieAux.compart

		CASE UPPER(ALLTRIM(ucrsDadosComparticipacaoSerieAux.tipoCompart)) == UPPER(ALLTRIM("Percent")) && Comparticipa��o em percentagem

			lcValorPercent = (lcPvp*ucrsDadosComparticipacaoSerieAux.compart)/100
			lcValorUtente =  lcValorPercent

		OTHERWISE
			**
		ENDCASE

		lcValorEntidade = lcPVP - lcValorUtente			


		SELECT uCrsServMarcacao
		Replace uCrsServMarcacao.pvp WITH lcPVP
		Replace uCrsServMarcacao.total WITH lcValorUtente
		Replace uCrsServMarcacao.compart WITH lcValorEntidade


		** Cursor de Comparticipa��es Entidade
		lcservcompmrstamp  = uf_gerais_stamp()
		Select uCrsCompServMarcacao
		CALCULATE MAX(ordem) TO lcMaxOrdemServMarc


		SELECT ucrsDadosComparticipacaoSerieAux
		SELECT uCrsCompServMarcacao
		APPEND BLANK
		REPLACE uCrsCompServMarcacao.entidadeno WITH ucrsDadosComparticipacaoSerieAux.eno
		REPLACE uCrsCompServMarcacao.entidadeestab WITH ucrsDadosComparticipacaoSerieAux.eestab
		REPLACE uCrsCompServMarcacao.entidade WITH ucrsDadosComparticipacaoSerieAux.enome
	
		Replace uCrsCompServMarcacao.servcompmrstamp WITH lcservcompmrstamp  
		Replace uCrsCompServMarcacao.mrstamp WITH UcrsMarcacoesDia.mrstamp
		Replace uCrsCompServMarcacao.servmrstamp WITH lcservmrstamp
		Replace uCrsCompServMarcacao.ref WITH ucrsDadosComparticipacaoSerieAux.ref
		Replace uCrsCompServMarcacao.design  WITH ucrsDadosComparticipacaoSerieAux.design
		
		Replace uCrsCompServMarcacao.pvp WITH ucrsDadosComparticipacaoSerieAux.pvp
		Replace uCrsCompServMarcacao.Total WITH ucrsDadosComparticipacaoSerieAux.pvp
		Replace uCrsCompServMarcacao.valor WITH lcValorEntidade 
		Replace uCrsCompServMarcacao.compart WITH lcValorUtente
		Replace uCrsCompServMarcacao.ordem WITH IIF(ISNULL(lcMaxOrdemServMarc),1,lcMaxOrdemServMarc+1)
		Replace uCrsCompServMarcacao.tipoCompart WITH "Valor" && Aplica sempre em valor mesmo que na defini��o tenha perecentagem
	ENDIF 
	

	MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page2.GridConsumos.refresh
	MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page2.GridComp.refresh

ENDFUNC 

**
FUNCTION uf_pesqrefsagrupadas_sair

	IF USED("ucrsPesqRefsAgrupadas")
		fecha("ucrsPesqRefsAgrupadas")
	ENDIF

	pesqrefsagrupadas.hide
	pesqrefsagrupadas.release
ENDFUNC
