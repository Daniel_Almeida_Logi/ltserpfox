**
FUNCTION uf_mrpagamento_chama
	LPARAMETERS lcOrigem
	PUBLIC myMrPagamentoCancelou, myMrPagamentoPagou
	STORE .f. TO myMrPagamentoCancelou, myMrPagamentoPagou

	
	IF EMPTY(lcOrigem)
		lcOrigem = "MARCACOES"
	ENDIF 

	LOCAL lcValorPagar 
	lcValorPagar = 0
	
	**
	IF !USED("UcrsConfigModPag")
		TEXT TO lcSQL NOSHOW TEXTMERGE
			select * from B_modoPag
		ENDTEXT 
		If !uf_gerais_actGrelha("", "UcrsConfigModPag", lcSql)
			uf_perguntalt_chama("N�o foi poss�vel verificar as configura��es de Modo de Pagamento. Contacte o suporte.", "", "OK", 32)
			RETURN .f.
		ENDIF
	ENDIF
	
	IF !USED("UcrsTipoDescricaoLinhasFtMarc") 
		CREATE CURSOR UcrsTipoDescricaoLinhasFtMarc (tipo c(254))
		
		SELECT UcrsTipoDescricaoLinhasFtMarc 
		APPEND BLANK
		Replace UcrsTipoDescricaoLinhasFtMarc.tipo WITH "<Data><Hora Inicio>/<Hora Fim>-<Designa��o Servi�o>"
		
	ENDIF 
	
	
	IF !USED("ucrsDadosTalaoPagamento") 
		CREATE CURSOR ucrsDadosTalaoPagamento(entrega c(254), obs c(254))
		SELECT ucrsDadosTalaoPagamento
		APPEND Blank
	ENDIF 
	
	SELECT ucrsDadosTalaoPagamento
	Replace ucrsDadosTalaoPagamento.entrega WITH "N/Instala��es"
	Replace ucrsDadosTalaoPagamento.obs WITH ""
	
	
	Select design, qtt, ref, epv as pvp, 0.00 as compart, etiliquido as total from fi INTO CURSOR uCrsServMarcacaoPag READWRITE
	SELECT uCrsServMarcacaoPag 
	GO Top
	SELECT ft
	lcValorPagar = ft.etotal

	** Cria cursor de Cqabe�alho de Pagamento
	IF USED("ucrsCabMrPag")
		fecha("ucrsCabMrPag")
	ENDIF 
	CREATE CURSOR ucrsCabMrPag (nratendimento c(20),valorPagar n(9,2), dinheiro n(9,2), mb n(9,2), visa n(9,2), cheque n(9,2), modPag3 n(9,2), modPag4 n(9,2), modPag5 n(9,2), modPag6 n(9,2), valorRecebido n(9,2), Troco n(9,2), taxaCorreio l, taxaUrgencia l)
	SELECT ucrsCabMrPag 
	APPEND BLANK
	Replace ucrsCabMrPag.valorPagar WITH lcValorPagar
	IF !(TYPE("FACTURACAO") == "U")
		IF !EMPTY(ucrsPagCentral.evdinheiro)
			Replace ucrsCabMrPag.dinheiro WITH ucrsPagCentral.evdinheiro
		ENDIF
		IF !EMPTY(ucrsPagCentral.echtotal)
			Replace ucrsCabMrPag.cheque WITH ucrsPagCentral.echtotal
		ENDIF
		IF !EMPTY(ucrsPagCentral.epaga1)
			Replace ucrsCabMrPag.visa WITH ucrsPagCentral.epaga1
		ENDIF
		IF !EMPTY(ucrsPagCentral.epaga2)
			Replace ucrsCabMrPag.mb WITH ucrsPagCentral.epaga2
		ENDIF
		IF !EMPTY(ucrsPagCentral.epaga3)
			Replace ucrsCabMrPag.modpag3 WITH ucrsPagCentral.epaga3
		ENDIF
		IF !EMPTY(ucrsPagCentral.epaga4)
			Replace ucrsCabMrPag.modpag4 WITH ucrsPagCentral.epaga4
		ENDIF
		IF !EMPTY(ucrsPagCentral.epaga5)
			Replace ucrsCabMrPag.modpag5 WITH ucrsPagCentral.epaga5
		ENDIF
		IF !EMPTY(ucrsPagCentral.epaga6)
			Replace ucrsCabMrPag.modpag6 WITH ucrsPagCentral.epaga6
		ENDIF
		Replace ucrsCabMrPag.valorRecebido WITH ucrsPagCentral.evdinheiro + ucrsPagCentral.echtotal + ucrsPagCentral.epaga1 + ucrsPagCentral.epaga2 + ucrsPagCentral.epaga3 + ucrsPagCentral.epaga4 + ucrsPagCentral.epaga5 + ucrsPagCentral.epaga6
	ENDIF 
	
	**
	IF TYPE("MRPAGAMENTO")=="U"
		DO FORM MRPAGAMENTO WITH lcOrigem
		
	ELSE
		MRPAGAMENTO.show
	ENDIF

ENDFUNC 


**
FUNCTION uf_mrpagamento_init()

	&& configurar menu
	MRPAGAMENTO.menu1.adicionaOpcao("talaoLevantamento", "T.Levant.", myPath + "\imagens\icons\imprimir_w.png", "uf_MRPAGAMENTO_talaoLevantamento","T")


ENDFUNC

*************************************************
* preencher textbox: dinheiro, mb, visa, cheque *
*************************************************
Function uf_mrpagamento_modoPagamento
	Lparameters lcModoP

	SELECT ucrsCabMrPag
	GO Top

	If Round(ucrsCabMrPag.valorRecebido,2) >= Round(ucrsCabMrPag.valorPagar,2) &&OR myPagCentral
		RETURN .f.
	ENDIF
	


	&&
	MRPAGAMENTO.txtValAtend.SetFocus
	
	If Empty(MRPAGAMENTO.txtValRec.value) AND !Empty(MRPAGAMENTO.txtValAtend.value)
		Do Case
			Case lcModoP=="dinheiro"
				MRPAGAMENTO.txtDinheiro.value = MRPAGAMENTO.txtValAtend.value
			Case lcModoP=="mb"
				MRPAGAMENTO.txtMB.value = MRPAGAMENTO.txtValAtend.value
			Case lcModoP=="visa"
				MRPAGAMENTO.txtVisa.value = MRPAGAMENTO.txtValAtend.value
			Case lcModoP=="cheque"
				MRPAGAMENTO.txtCheque.value = MRPAGAMENTO.txtValAtend.value
			Case lcModoP=="modpag3"

				IF uf_gerais_getUmValor("b_modoPag", "pagamentoExterno", "ref = '05'")
					IF !uf_mrpagamento_modoPagamentoExterno("05")
						RETURN .F.
					ENDIF
				ENDIF

				MRPAGAMENTO.txtmodpag3.value = MRPAGAMENTO.txtValAtend.value
			Case lcModoP=="modpag4"

				IF uf_gerais_getUmValor("b_modoPag", "pagamentoExterno", "ref = '06'")
					IF !uf_mrpagamento_modoPagamentoExterno("06")
						RETURN .F.
					ENDIF
				ENDIF

				MRPAGAMENTO.txtmodpag4.value = MRPAGAMENTO.txtValAtend.value
			Case lcModoP=="modpag5"

				IF uf_gerais_getUmValor("b_modoPag", "pagamentoExterno", "ref = '07'")
					IF !uf_mrpagamento_modoPagamentoExterno("07")
						RETURN .F.
					ENDIF
				ENDIF

				MRPAGAMENTO.txtmodpag5.value = MRPAGAMENTO.txtValAtend.value	
			Case lcModoP=="modpag6"
			
				IF uf_gerais_getUmValor("b_modoPag", "pagamentoExterno", "ref = '08'")
					IF !uf_mrpagamento_modoPagamentoExterno("08")
						RETURN .F.
					ENDIF
				ENDIF
			
				MRPAGAMENTO.txtmodpag6.value = MRPAGAMENTO.txtValAtend.value	
			OTHERWISE
				uf_perguntalt_chama("PAR�METRO DE PAGAMENTO INV�LIDO","OK","",64)
				RETURN .f.
		EndCase
	ELSE

		IF (Round(ucrsCabMrPag.valorPagar,2) - Round(ucrsCabMrPag.valorRecebido,2)) > 0
			Do Case
				Case lcModoP=="dinheiro"
		
					SELECT ucrsCabMrPag
					Replace ucrsCabMrPag.dinheiro WITH ucrsCabMrPag.valorPagar - ;
					(ucrsCabMrPag.mb + ucrsCabMrPag.visa + ucrsCabMrPag.cheque + ucrsCabMrPag.modPag3 + ucrsCabMrPag.modPag4 + ucrsCabMrPag.modPag5 + ucrsCabMrPag.modPag6)

					MRPAGAMENTO.txtDinheiro.refresh
					MRPAGAMENTO.txtDinheiro.SetFocus
					
				Case lcModoP=="mb"
					
					SELECT ucrsCabMrPag
					Replace ucrsCabMrPag.mb WITH ucrsCabMrPag.valorPagar - ;
					(ucrsCabMrPag.dinheiro + ucrsCabMrPag.visa + ucrsCabMrPag.cheque + ucrsCabMrPag.modPag3 + ucrsCabMrPag.modPag4 + ucrsCabMrPag.modPag5 + ucrsCabMrPag.modPag6)
					
					MRPAGAMENTO.txtMB.refresh
					MRPAGAMENTO.txtMB.SetFocus
					
				Case lcModoP=="visa"
					
					SELECT ucrsCabMrPag
					Replace ucrsCabMrPag.visa WITH ucrsCabMrPag.valorPagar - ;
					(ucrsCabMrPag.dinheiro + ucrsCabMrPag.mb + ucrsCabMrPag.cheque + ucrsCabMrPag.modPag3 + ucrsCabMrPag.modPag4 + ucrsCabMrPag.modPag5 + ucrsCabMrPag.modPag6)

					MRPAGAMENTO.txtVisa.refresh
					MRPAGAMENTO.txtVisa.SetFocus
					
					
				Case lcModoP=="cheque"
					
					SELECT ucrsCabMrPag
					Replace ucrsCabMrPag.cheque WITH ucrsCabMrPag.valorPagar - ;
					(ucrsCabMrPag.dinheiro + ucrsCabMrPag.mb + ucrsCabMrPag.visa + ucrsCabMrPag.modPag3 + ucrsCabMrPag.modPag4 + ucrsCabMrPag.modPag5 + ucrsCabMrPag.modPag6)

					MRPAGAMENTO.txtCheque.refresh
					MRPAGAMENTO.txtCheque.SetFocus
				
				Case lcModoP=="modpag3"

					IF uf_gerais_getUmValor("b_modoPag", "pagamentoExterno", "ref = '05'")
						IF !uf_mrpagamento_modoPagamentoExterno("05")
							RETURN .F.
						ENDIF
					ENDIF
					
					SELECT ucrsCabMrPag
					Replace ucrsCabMrPag.modPag3 WITH ucrsCabMrPag.valorPagar - ;
					(ucrsCabMrPag.dinheiro + ucrsCabMrPag.mb + ucrsCabMrPag.visa + ucrsCabMrPag.cheque + ucrsCabMrPag.modPag4 + ucrsCabMrPag.modPag5 + ucrsCabMrPag.modPag6)
					
					MRPAGAMENTO.txtModPag3.refresh
					MRPAGAMENTO.txtModPag3.SetFocus
				
				Case lcModoP=="modpag4"

					IF uf_gerais_getUmValor("b_modoPag", "pagamentoExterno", "ref = '06'")
						IF !uf_mrpagamento_modoPagamentoExterno("06")
							RETURN .F.
						ENDIF
					ENDIF
					
					SELECT ucrsCabMrPag
					Replace ucrsCabMrPag.modPag4 WITH ucrsCabMrPag.valorPagar - ;
					(ucrsCabMrPag.dinheiro + ucrsCabMrPag.mb + ucrsCabMrPag.visa + ucrsCabMrPag.cheque + ucrsCabMrPag.modPag3 + ucrsCabMrPag.modPag5 + ucrsCabMrPag.modPag6)

					
					MRPAGAMENTO.txtModPag4.refresh
					MRPAGAMENTO.txtModPag4.SetFocus
				
				Case lcModoP=="modpag5"

					IF uf_gerais_getUmValor("b_modoPag", "pagamentoExterno", "ref = '07'")
						IF !uf_mrpagamento_modoPagamentoExterno("07")
							RETURN .F.
						ENDIF
					ENDIF
					
					SELECT ucrsCabMrPag
					Replace ucrsCabMrPag.modPag5 WITH ucrsCabMrPag.valorPagar - ;
					(ucrsCabMrPag.dinheiro + ucrsCabMrPag.mb + ucrsCabMrPag.visa + ucrsCabMrPag.cheque + ucrsCabMrPag.modPag3 + ucrsCabMrPag.modPag4 + ucrsCabMrPag.modPag6)

					
					MRPAGAMENTO.txtModPag5.refresh
					MRPAGAMENTO.txtModPag5.SetFocus
				
				case lcModoP=="modpag6"

					IF uf_gerais_getUmValor("b_modoPag", "pagamentoExterno", "ref = '08'")
						IF !uf_mrpagamento_modoPagamentoExterno("08")
							RETURN .F.
						ENDIF
					ENDIF
					
					SELECT ucrsCabMrPag
					Replace ucrsCabMrPag.modPag6 WITH ucrsCabMrPag.valorPagar - ;
					(ucrsCabMrPag.dinheiro + ucrsCabMrPag.mb + ucrsCabMrPag.visa + ucrsCabMrPag.cheque + ucrsCabMrPag.modPag3 + ucrsCabMrPag.modPag4 + ucrsCabMrPag.modPag5)

					
					MRPAGAMENTO.txtModPag6.refresh
					MRPAGAMENTO.txtModPag6.SetFocus
					
				OTHERWISE
					uf_perguntalt_chama("PAR�METRO DE PAGAMENTO INV�LIDO","OK","",64)
					RETURN .f.
			ENDCASE
		ENDIF

	ENDIF
	
	uf_mrpagamento_actValoresPg()
ENDFUNC


*******************************************
* Actualizar valores de pagamento e troco *
*******************************************
FUNCTION uf_mrpagamento_actValoresPg
	
	&& Valor Recebido
	SELECT ucrsCabMrPag
	replace ucrsCabMrPag.valorRecebido WITH (ucrsCabMrPag.dinheiro + ucrsCabMrPag.mb + ucrsCabMrPag.visa + ucrsCabMrPag.cheque + ucrsCabMrPag.modPag3 + ucrsCabMrPag.modPag4 + ucrsCabMrPag.modPag5 + ucrsCabMrPag.modPag6)
	
	&& Troco
	SELECT ucrsCabMrPag
	If ucrsCabMrPag.valorPagar > 0
		lcTroco = round(ucrsCabMrPag.valorRecebido,2) - Round(ucrsCabMrPag.valorPagar,2)
		If lcTroco > 0
			REPLACE ucrsCabMrPag.troco WITH Round(lcTroco,2)
		Else
			REPLACE ucrsCabMrPag.troco WITH 0
		EndIf
	ENDIF

	MRPAGAMENTO.refresh
	
ENDFUNC


**
FUNCTION uf_MRPAGAMENTO_talaoLevantamento
	
	IF USED("ucrsMarcacoesDia")
			
		SELECT ucrsMarcacoesDia
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			UPDATE 
				marcacoes 
			SET 
				marcacoes.LocalEntrega = '<<ALLTRIM(ucrsMarcacoesDia.LocalEntrega)>>'
				,talaolevantamento = 1 
			WHERE
				marcacoes.mrstamp = '<<ALLTRIM(ucrsMarcacoesDia.mrstamp)>>'
		ENDTEXT 
		If !uf_gerais_actGrelha("", "", lcSql)
			uf_perguntalt_chama("N�o foi poss�vel gravar defini��es do tal�o de levantamento.", "", "OK", 32)
			RETURN .f.
		ENDIF
		
		
		*** Imprime
		uf_mrpagamento_imprimir_TLAVANTAMENTO()
		
	ENDIF 
ENDFUNC 



**
FUNCTION uf_mrpagamento_taxacorreio
	LOCAL lcSQL 
	
	SELECT ucrsCabMrPag

	IF !EMPTY(ucrsCabMrPag.taxaCorreio)

		TEXT TO lcSQL NOSHOW TEXTMERGE
			SELECT * FROM st WHERE ref = 'T000001'
		ENDTEXT 	
		If !uf_gerais_actGrelha("","uCrsDefinicoesTaxaCorreio",lcSql)
			RETURN .f.
		ENDIF 
		
		If Reccount("uCrsDefinicoesTaxaCorreio") == 0
			uf_perguntalt_chama("N�o foi encontrado o servi�o Taxa de Correio.","OK","",64)
			RETURN .f.
		ENDIF
		
		SELECT uCrsDefinicoesTaxaCorreio
		SELECT uCrsServMarcacaoPag
		APPEND BLANK
		Replace uCrsServMarcacaoPag.ref WITH uCrsDefinicoesTaxaCorreio.ref
		Replace uCrsServMarcacaoPag.design WITH uCrsDefinicoesTaxaCorreio.design
		Replace uCrsServMarcacaoPag.qtt WITH 1	
		Replace uCrsServMarcacaoPag.pvp WITH uCrsDefinicoesTaxaCorreio.epv1
		Replace uCrsServMarcacaoPag.total WITH uCrsDefinicoesTaxaCorreio.epv1
		
		IF USED("uCrsDefinicoesTaxaCorreio")
			fecha("uCrsDefinicoesTaxaCorreio")
		ENDIF 
	ELSE
		DELETE FROM uCrsServMarcacaoPag WHERE UPPER(ALLTRIM(uCrsServMarcacaoPag.ref)) == 'T000001'
	ENDIF 

	**
	uf_mrpagamento_actualizaTotal()
	mrpagamento.GridConsumos.Refresh
ENDFUNC 


**
FUNCTION uf_mrpagamento_taxaUrgencia
	LOCAL lcSQL 
	
	SELECT ucrsCabMrPag

	IF !EMPTY(ucrsCabMrPag.taxaUrgencia)
		
		TEXT TO lcSQL NOSHOW TEXTMERGE
			SELECT * FROM st WHERE ref = 'T000002'
		ENDTEXT 	
		If !uf_gerais_actGrelha("","uCrsDefinicoesTaxaUrg",lcSql)
			RETURN .f.
		ENDIF 
		
		If Reccount("uCrsDefinicoesTaxaUrg") == 0
			uf_perguntalt_chama("N�o foi encontrado o servi�o Taxa de Urg�ncia.","OK","",64)
			RETURN .f.
		ENDIF
		
		SELECT uCrsDefinicoesTaxaUrg
		SELECT uCrsServMarcacaoPag
		APPEND BLANK
		Replace uCrsServMarcacaoPag.ref WITH uCrsDefinicoesTaxaUrg.ref
		Replace uCrsServMarcacaoPag.design WITH uCrsDefinicoesTaxaUrg.design
		Replace uCrsServMarcacaoPag.qtt WITH 1	
		Replace uCrsServMarcacaoPag.pvp WITH uCrsDefinicoesTaxaUrg.epv1
		Replace uCrsServMarcacaoPag.total WITH uCrsDefinicoesTaxaUrg.epv1
		
		IF USED("uCrsDefinicoesTaxaUrg")
			fecha("uCrsDefinicoesTaxaUrg")
		ENDIF 
	ELSE
		DELETE FROM uCrsServMarcacaoPag WHERE UPPER(ALLTRIM(uCrsServMarcacaoPag.ref)) == 'T000002'
	ENDIF 
	
	
	**
	uf_mrpagamento_actualizaTotal()
	mrpagamento.GridConsumos.Refresh
ENDFUNC 


** 
FUNCTION uf_mrpagamento_actualizaTotal
	SELECT uCrsServMarcacaoPag 
	GO Top
	SELECT uCrsServMarcacaoPag 
	CALCULATE SUM(uCrsServMarcacaoPag.total) TO lcValorPagar

	SELECT ucrsCabMrPag
	Replace ucrsCabMrPag.valorPagar WITH lcValorPagar
	
	IF !EMPTY(ucrsCabMrPag.dinheiro)
		Replace ucrsCabMrPag.dinheiro WITH 0
	ENDIF 
	IF !EMPTY(ucrsCabMrPag.mb)
		Replace ucrsCabMrPag.mb WITH 0
	ENDIF 
	IF !EMPTY(ucrsCabMrPag.visa)
		Replace ucrsCabMrPag.visa WITH 0
	ENDIF 
	IF !EMPTY(ucrsCabMrPag.cheque)	
		Replace ucrsCabMrPag.cheque WITH 0
	ENDIF 
	IF !EMPTY(ucrsCabMrPag.modPag3)		
		Replace ucrsCabMrPag.modPag3 WITH 0
	ENDIF 
	IF !EMPTY(ucrsCabMrPag.modPag4)
		Replace ucrsCabMrPag.modPag4 WITH 0
	ENDIF 
	IF !EMPTY(ucrsCabMrPag.modPag5)
		Replace ucrsCabMrPag.modPag5 WITH 0
	ENDIF 
	IF !EMPTY(ucrsCabMrPag.modPag6)
		Replace ucrsCabMrPag.modPag6 WITH 0
	ENDIF 
	IF !EMPTY(ucrsCabMrPag.valorRecebido)
		Replace ucrsCabMrPag.valorRecebido WITH 0
	ENDIF 
	IF !EMPTY(ucrsCabMrPag.Troco)
		Replace ucrsCabMrPag.Troco WITH 0
	ENDIF 
	
	mrpagamento.refresh
ENDFUNC 


**
FUNCTION uf_mrpagamento_RegrasAntesGravar
	
	*** VERIFICA��ES ***
	&& VERIFICAR CAIXA
	IF TYPE("FACTURACAO")=='U'
		LOCAL lcSql
		lcSql = ''
		Text to lcSql noshow textmerge
			exec up_caixa_verificaCaixaAberta '<<alltrim(mySite)>>', '<<alltrim(myTerm)>>', 0
		ENDTEXT
		If uf_gerais_actGrelha("","uCrsCxAberta",lcSql)
			If !Reccount()>0
				uf_perguntalt_chama("A SUA SESS�O DE CAIXA FOI FECHADA. N�O PODE CONTINUAR SEM PRIMEIRO ABRIR NOVA SESS�O.","OK","",64)
				fecha("uCrsCxAberta")
				RETURN .f.
			ENDIF
			fecha("uCrsCxAberta")
		ENDIF
	ENDIF 
		
	IF MRPAGAMENTO.credito == .f.
		&& Valida Troco nos meios de pagamento != dinheiro
		SELECT ucrsCabMrPag
		IF ucrsCabMrPag.troco > 0;
			AND (ucrsCabMrPag.mb != 0 OR ucrsCabMrPag.visa != 0 OR ucrsCabMrPag.cheque != 0 OR ucrsCabMrPag.modPag3 != 0 OR ucrsCabMrPag.modPag3 != 0 OR ucrsCabMrPag.modPag5 != 0 OR ucrsCabMrPag.modPag6 != 0)
					
			uf_perguntalt_chama("APENAS PODER� INCLUIR TROCO NO MEIO DE PAGAMENTO A DINHEIRO. POR FAVOR VERIFIQUE.","OK","",64)
			RETURN .f.
		ENDIF

		&& Valida Total Recebido
		IF ucrsCabMrPag.valorRecebido < ucrsCabMrPag.valorPagar 
			uf_perguntalt_chama("VALOR RECEBIDO INFERIOR AO VALOR DA MARCA��O.","OK","",64)
			RETURN .f.
		ENDIF
	ENDIF 
	
	RETURN .t. 
ENDFUNC 


** 
FUNCTION uf_mrpagamento_gravar

	IF empty(uf_mrpagamento_RegrasAntesGravar())
		return .f.
	ENDIF 

	&& Pagcentral
	IF EMPTY(uf_mrpagamento_gravarPagCentral())
		RETURN .f.
	ENDIF
	
	
	IF MRPAGAMENTO.ORIGEM == "MARCACOES"
		IF EMPTY(uf_mrpagamento_emitirDocumento())
			RETURN .f.
		ENDIF
	ENDIF 	
	
	myMrPagamentoPagou  = .t.
	uf_mrpagamento_sair(.t.)
	
ENDFUNC 

** 
FUNCTION uf_mrpagamento_gravarPagCentral
		
	&& Guardar dados para Pagamento Central
	LOCAL lcTrocoPc, lcDinheiroPc, lcMbPc, lcVisaPc, lcChequePc, lcModPag3, lcModPag4, lcModPag5, lcModPag6
	STORE 0 TO lcTrocoPc, lcDinheiroPc, lcMbPc, lcVisaPc, lcChequePc, lcModPag3, lcModPag4, lcModPag5, lcModPag6
	
	
	IF EMPTY(ucrsCabMrPag.Troco)
		lcTrocoPc = 0
	ELSE
		lcTrocoPc = ucrsCabMrPag.Troco
	ENDIF
	IF EMPTY(ucrsCabMrPag.dinheiro)
		lcDinheiroPc = 0
	ELSE
		lcDinheiroPc = ucrsCabMrPag.dinheiro
	ENDIF
	IF EMPTY(ucrsCabMrPag.mb)
		lcMbPc = 0
	ELSE
		lcMbPc = ucrsCabMrPag.mb
	ENDIF
	IF empty(ucrsCabMrPag.visa)
		lcVisaPc = 0
	ELSE
		lcVisaPc = ucrsCabMrPag.visa
	ENDIF
	IF empty(ucrsCabMrPag.cheque)
		lcChequePc = 0
	ELSE
		lcChequePc = ucrsCabMrPag.cheque
	ENDIF
	IF EMPTY(ucrsCabMrPag.modPag3)		
		lcModPag3 = 0
	ELSE 
		lcModPag3 = ucrsCabMrPag.modPag3
	ENDIF 
	IF EMPTY(ucrsCabMrPag.modPag4)		
		lcModPag4 = 0
	ELSE 
		lcModPag4 = ucrsCabMrPag.modPag4
	ENDIF 
	IF EMPTY(ucrsCabMrPag.modPag5)		
		lcModPag5 = 0
	ELSE 
		lcModPag5 = ucrsCabMrPag.modPag5
	ENDIF 
	IF EMPTY(ucrsCabMrPag.modPag6)		
		lcModPag6 = 0
	ELSE 
		lcModPag6 = ucrsCabMrPag.modPag6
	ENDIF 
	
	***************************************
	
	IF MRPAGAMENTO.credito == .t.
		UPDATE ucrsCabMrPag SET Troco = 0, dinheiro = 0, mb = 0, visa = 0, cheque = 0, modPag3 = 0, modPag4 = 0, modPag5 = 0, modPag6 = 0 
	ENDIF 
	
	
	&& totais
	lcTotal = IIF(MRPAGAMENTO.credito == .f.,ucrsCabMrPag.valorPagar,0)
	lcTotalBruto = IIF(MRPAGAMENTO.credito == .f.,ucrsCabMrPag.valorPagar,0)
	lcCreditoPc = IIF(MRPAGAMENTO.credito == .t.,ucrsCabMrPag.valorPagar,0)
	lcDevRealPc = 0
	lcNtCreditoPc = 0&&IIF(MRPAGAMENTO.credito == .t.,ucrsCabMrPag.valorPagar,0)

	
	DO CASE 
		CASE MRPAGAMENTO.ORIGEM == "MARCACOES"
			SELECT ucrsMarcacoesDia
			lcStamp = ucrsMarcacoesDia.mrstamp
			PUBLIC nrAtendimento
			uf_atendimento_geraNrAtendimento()
			
			UPDATE ucrsCabMrPag SET ucrsCabMrPag.nrAtendimento = nrAtendimento
			
			lcNrVendas = 1
			lcClNo = ucrsMarcacoesDia.no
			lcClEstab = ucrsMarcacoesDia.estab
		CASE MRPAGAMENTO.ORIGEM == "FACTURACAO"
			SELECT ft
			lcStamp = ft.ftstamp
			PUBLIC nrAtendimento
			uf_atendimento_geraNrAtendimento()
			
			UPDATE ucrsCabMrPag SET ucrsCabMrPag.nrAtendimento = nrAtendimento
			UPDATE ft SET ft.u_nratend = nrAtendimento
			
			lcNrVendas = 1
			lcClNo = ft.no
			lcClEstab = ft.estab
		
	ENDCASE 		
	&& Gravar B_pagCentral
	IF EMPTY(mySsStamp)
		mySsStamp = uf_gerais_stamp()
	ENDIF 
	
    LOCAL lcDeveFechar, lcFechaStamp
    lcDeveFechar = .f.
    lcFechaStamp = ''
    
        
    ** fecha vendas sem dinheiro
    IF uf_gerais_validaPagCentralDinheiro()
        if(ROUND((lcDinheiroPc-lcTrocoPc),2))!=0
            lcDeveFechar =.f.
        ELSE
            lcDeveFechar =.t.
            lcFechaStamp = uf_gerais_stamp()
        ENDIF
        
    ENDIF
    
  
	IF !uf_mrpagamento_geraPagExterno(lcClNo, lcClEstab, nrAtendimento)
		RETURN .F.
	ENDIF
	
	
	LOCAL lcSQL
	lcSql = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW
		INSERT INTO B_pagCentral
			(stamp
			,nrAtend
			,nrVendas
			,total
			,ano
			,no
			,estab
			,vendedor
			,nome
			,terminal
			,terminal_nome
			,evdinheiro
			,epaga1
			,epaga2
			,echtotal
			,evdinheiro_semTroco
			,etroco
			,total_bruto
			,devolucoes
			,ntCredito
			,creditos
			,cxstamp
			,ssstamp
			,site
			,fechado
			,epaga3
			,epaga4
			,epaga5
			,epaga6
			,odata
			,udata
			,fechastamp
		)
		values
			('<<lcStamp>>'
			,'<<nrAtendimento>>'
			,<<lcNrVendas>>
			,<<Round(mrpagamento.txtValAtend.value,2)>>
			,<<YEAR(DATE())>>
			,<<lcClNo>>
			,<<lcClEstab>>
			,<<ch_vendedor>>
			,'<<ALLTRIM(ch_vendnm)>>'
			,<<myTermNo>>
			,'<<ALLTRIM(myTerm)>>'
			,<<ROUND((lcDinheiroPc-lcTrocoPc),2)>>
			,<<ROUND(lcVisaPc,2)>>
			,<<ROUND(lcMbPc,2)>>
			,<<ROUND(lcChequePc,2)>>
			,<<ROUND(lcDinheiroPc,2)>>
			,<<ROUND(lcTrocoPc,2)>>
			,<<ROUND(lcTotalBruto,2)>>
			,<<ROUND(lcDevRealPc,2)>>
			,<<ROUND(lcNtCreditoPc,2)>>
			,<<ROUND(lcCreditoPc,2)>>
			,'<<ALLTRIM(myCxStamp)>>'
			,'<<ALLTRIM(mySsStamp)>>'
			,'<<ALLTRIM(mySite)>>'
			,<<IIF(MRPAGAMENTO.credito == .f. AND EMPTY(lcDeveFechar) ,0,1)>>
			,<<ROUND(lcModPag3,2)>>
			,<<ROUND(lcModPag4,2)>>
			,<<ROUND(lcModPag5,2)>>
			,<<ROUND(lcModPag6,2)>>
			, dateadd(HOUR, <<difhoraria>>, getdate())
			, dateadd(HOUR, <<difhoraria>>, getdate())
			, '<<ALLTRIM(lcFechaStamp)>>'
		)
	ENDTEXT
	******************************************
	
*!*	_CLIPTEXT = lcSQL
*!*	MESSAGEBOX(lcSQL)


	If !uf_gerais_actGrelha("", "", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel registar o movimento na tabela de Pagamentos Centrais. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	
	RETURN .t.
ENDFUNC 


**
FUNCTION uf_mrpagamento_emitirDocumento
	LPARAMETERS lcCredito
	
	LOCAL lcSQL, lcValorFacturarUtente, lcEmitiuFt
	STORE 0 TO lcValorFacturarUtente
	STORE .f. TO lcEmitiuFt 
	
	Select UcrsMarcacoesDia
	lcNome = ALLTRIM(UcrsMarcacoesDia.nome)
	
	IF USED("ucrsDadosFacturacaoUtentesDetalhe")
		fecha("ucrsDadosFacturacaoUtentesDetalhe")
	ENDIF 
	IF USED("ucrsDadosFacturacaoUtentes")
		fecha("ucrsDadosFacturacaoUtentes")
	ENDIF 
	
	
	SELECT uCrsServMarcacao
	GO top
	SELECT * FROM uCrsServMarcacao WHERE !EMPTY(sel) INTO CURSOR uCrsServMarcacaoPag READWRITE && AND Total > 0
		
	SELECT uCrsServMarcacaoPag 
	GO Top
	SELECT uCrsServMarcacaoPag 
	CALCULATE SUM(uCrsServMarcacaoPag.total) TO lcValorPagar
	
	
	** Cria cursor de Cabe�alho de Pagamento
	IF USED("ucrsCabMrPag")
		fecha("ucrsCabMrPag")
	ENDIF 
	CREATE CURSOR ucrsCabMrPag (nratendimento c(20),valorPagar n(9,2), dinheiro n(9,2), mb n(9,2), visa n(9,2), cheque n(9,2), modPag3 n(9,2), modPag4 n(9,2), modPag5 n(9,2), modPag6 n(9,2), valorRecebido n(9,2), Troco n(9,2), taxaCorreio l, taxaUrgencia l)
	SELECT ucrsCabMrPag 
	APPEND BLANK
	Replace ucrsCabMrPag.valorPagar WITH lcValorPagar
	
	

	** Prepara Cursor de Detalhe para documento de fatura��o	
	Select .f. as sel;
			,'' as Entidade;
			,0 as EntidadeNo;
			,0 as EntidadeEstab;
			,ucrsMarcacoesDia.mrno as mrno;
			,uCrsServMarcacao.dataInicio as dataInicio;
			,uCrsServMarcacao.dataFim as dataFim;
			,uCrsServMarcacao.hinicio as hinicio;
			,uCrsServMarcacao.hfim as hfim;
			,ucrsMarcacoesDia.nome as nome;
			,ucrsMarcacoesDia.no as no;
			,ucrsMarcacoesDia.estab as estab;
			,uCrsServMarcacao.ref as ref;
			,uCrsServMarcacao.design as design; 
			,uCrsServMarcacao.pvp as pvp;
			,uCrsServMarcacao.qtt as qtt;
			,uCrsServMarcacao.total as total;
			,"" as tipoCompart;
			,0 as compart;
			,0 as maximo;
			,"" as obs;
			,'' as nbenef;
			,uCrsServMarcacao.total as faturarUtente;
			,0 as faturarEntidade;
			,0 as tabiva;
			,0 as iva;
			,.f. as ivaincl;
			,uCrsServMarcacao.servmrstamp as servmrstamp;
	FROM uCrsServMarcacao;
	WHERE !EMPTY(uCrsServMarcacao.sel); 
	INTO CURSOR ucrsDadosFacturacaoUtentesDetalhe READWRITE

	SELECT ucrsDadosFacturacaoUtentesDetalhe 
	CALCULATE SUM(faturarUtente) TO lcValorFacturarUtente

	
	create cursor ucrsDadosFacturacaoUtentes (sel l, UtenteNo n(9), UtenteEstab n(5), Utente c(100), faturarUtente n(9,3), dataIni d, dataFim d)
	SELECT ucrsDadosFacturacaoUtentes
	APPEND BLANK
	Replace ucrsDadosFacturacaoUtentes.sel WITH .t.
	Replace ucrsDadosFacturacaoUtentes.UtenteNo WITH UcrsMarcacoesDia.no
	Replace ucrsDadosFacturacaoUtentes.UtenteEstab WITH UcrsMarcacoesDia.estab
	Replace ucrsDadosFacturacaoUtentes.Utente WITH UcrsMarcacoesDia.nome
	Replace ucrsDadosFacturacaoUtentes.faturarUtente WITH lcValorFacturarUtente
	
**	AKI	
	lcEmitiuFt = uf_FACTENTIDADESCLINICA_emitirFacturasUtentes(.t.,lcCredito, "MRPAGAMENTO")


	
	RETURN .t.
ENDFUNC 



**
FUNCTION uf_mrpagamento_imprimir_TLAVANTAMENTO
&&	LPARAMETERS lcTipo
	lcTipo = "print"

	** Guarda a impressora por defeito do Windows***
	lcDefaultPrinter=Set("printer",2)
	*****************************************
	
	** Obtem Nome do Ficheiro Tabela de Impressoes **
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select Top 1 nomeficheiro, tabela, temduplicados, numduplicados
		From b_impressoes (nolock)
		Where nomeimpressao = 'TALAO_LEVANTAMENTO'
	Endtext
	
	If !uf_gerais_actGrelha("","templcSQL",lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR A CONFIGURA��O DE IMPRESS�ES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return
	Endif
	
	If Reccount("templcSQL")>0
		lcnomeFicheiro = Alltrim(templcSQL.nomeficheiro)
		lcprinter = "Impressora por defeito do Windows"
		lctabela = templcSQL.tabela
		lctemduplicados = templcSQL.temduplicados
		lcnumduplicados = templcSQL.numduplicados
	Else
		RETURN .f.
	Endif

	If !(uf_gerais_actGrelha("","uCrsImpressao",lcSQL))
		&&erro
	ENDIF
	
	****************************************
	** PREPARAR CURSORES PARA IMPRESS�ES **
	****************************************
	
	** Obtem dados Gerais da Emprea / Loja **
	If !USED("uCrsE1")
		uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR OS DADOS GERAIS DA EMPRESA. CONTACTE O SUPORTE.","OK","",16)	
		RETURN .f.
	Endif
	********************************
	
	SELECT UcrsMarcacoesDia
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_dadosTalaoLEvantamento '<<ALLTRIM(UcrsMarcacoesDia.mrstamp)>>'
	ENDTEXT 
	
	IF !uf_gerais_actGrelha("","uCrsRefsMarcacoes",lcSQL)
		uf_perguntalt_chama("Ocorreu um erro a imprimir tal�o de levantamento.", "OK", "", 16)
		RETURN .f.
 	ENDIF

	SELECT uCrsRefsMarcacoes				
	lcreportTalaoLevantamento = Alltrim(myPath)+"\analises\levantamento_exames.frx"

	TRY		
		Report Form Alltrim(lcreportTalaoLevantamento) To Printer
	CATCH
		uf_perguntalt_chama("Foi detectado um problema com a impressora." + CHR(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
	ENDTRY
	
	** preparar impressora **
	If !(alltrim(lcPrinter) == "Impressora por defeito do Windows")
		LOCAL lcValErroPrinter
		TRY 
			Set Printer To Name ''+alltrim(lcPrinter)+''
		CATCH
			uf_perguntalt_chama("Foi detectado um problema com a impressora." + CHR(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
			lcValErroPrinter = .t.
		ENDTRY
		IF lcValErroPrinter == .t.
			RETURN .f.
		ENDIF
	Endif
		
	Set Device To print
	Set Console off
	***********************************
	
	
	** por impressora no default **
	Set Device To screen
	Set printer to Name ''+lcDefaultPrinter+''
	Set Console On
	**************************	
ENDFUNC 


**
FUNCTION uf_mrpagamento_sair
	LPARAMETERS lcBool
	
	IF EMPTY(lcBool)
		myMrPagamentoPagou = .f. 
		myMrPagamentoCancelou = .t.
	ENDIF 
	
	MRPAGAMENTO.hide
	MRPAGAMENTO.release
	
ENDFUNC 

FUNCTION uf_mrpagamento_modoPagamentoExterno
	LPARAMETERS uv_modoPag

	IF EMPTY(uv_modoPag) OR !USED("ft")
		RETURN .F.
	ENDIF

   SELECT ft

	IF MRPAGAMENTO.txtvalatend.value < 0
		uf_perguntalt_chama("N�o pode utilizar este modo de pagamento para devolu��es.","OK","",16)
		RETURN .F.
	ENDIF

	IF !uf_pagExterno_chama(uv_modoPag, ft.no, ft.estab)
		RETURN .F.
	ENDIF

	IF !USED("uc_dadosPagExterno" + ALLTRIM(uv_modoPag))
		RETURN .F.
	ENDIF

	RETURN .T.

ENDFUNC

FUNCTION uf_mrpagamento_geraPagExterno
	LPARAMETERS uv_no, uv_estab, uv_nrAtend

	IF EMPTY(uv_no) OR TYPE("uv_estab") <> "N" OR EMPTY(uv_nrAtend)
		RETURN .F.
	ENDIF

	IF !uf_gerais_actGrelha("","uc_modosPagExterno", "exec up_vendas_checkModoPagExterno")

		uf_perguntalt_chama("Erro a verificar modos de pagamento externo. Por favor contacte o suporte.","OK","",16)
		RETURN .F.

	ENDIF

	IF RECCOUNT("uc_modosPagExterno") > 0

		CREATE CURSOR uc_pagsExterno(modoPag C(10), tlmvl C(50), email C(254), duracao C(50), descricao M, valor N(14,2), tipoPagExt N(3), tipoPagExtDesc C(100), receiverID C(100), receiverName C(100), no N(9), estab N(5), nrAtend C(20))
		LOCAL uv_valor, uv_curCursor

		SELECT uc_modosPagExterno
		GO TOP

		SCAN

			uv_valor = 0

			DO CASE
				CASE ALLTRIM(uc_modosPagExterno.ref) = "05"

					uv_valor = MRPAGAMENTO.txtModPag3.value

					IF uv_valor <> 0 AND !USED("uc_dadosPagExterno05") 
						uf_perguntalt_chama("N�o preencheu os dados para Pagamento Externo:" + CHR(13) + ALLTRIM(MRPAGAMENTO.btnModPag3.Label7.caption),"OK","",16)
						RETURN .F.
					ENDIF
					
				CASE ALLTRIM(uc_modosPagExterno.ref) = "06"

					uv_valor = MRPAGAMENTO.txtModPag4.value

					IF uv_valor <> 0 AND !USED("uc_dadosPagExterno06") 
						uf_perguntalt_chama("N�o preencheu os dados para Pagamento Externo:" + CHR(13) + ALLTRIM(MRPAGAMENTO.btnModPag4.Label7.caption),"OK","",16)
						RETURN .F.
					ENDIF

				CASE ALLTRIM(uc_modosPagExterno.ref) = "07"

					uv_valor = MRPAGAMENTO.txtModPag5.value

					IF uv_valor <> 0 AND !USED("uc_dadosPagExterno07") 
						uf_perguntalt_chama("N�o preencheu os dados para Pagamento Externo:" + CHR(13) + ALLTRIM(MRPAGAMENTO.btnModPag5.Label7.caption),"OK","",16)
						RETURN .F.
					ENDIF

				CASE ALLTRIM(uc_modosPagExterno.ref) = "08"

					uv_valor = MRPAGAMENTO.txtModPag6.value

					IF uv_valor <> 0 AND !USED("uc_dadosPagExterno08") 
						uf_perguntalt_chama("N�o preencheu os dados para Pagamento Externo:" + CHR(13) + ALLTRIM(MRPAGAMENTO.btnModPag6.Label7.caption),"OK","",16)
						RETURN .F.
					ENDIF

				OTHERWISE
					LOOP
			ENDCASE

			uv_curCursor = "uc_dadosPagExterno" + ALLTRIM(uc_modosPagExterno.ref)

			IF uv_valor <> 0 AND USED(uv_curCursor)

					SELECT uc_pagsExterno
					APPEND BLANK

					REPLACE uc_pagsExterno.modoPag WITH ALLTRIM(uc_modosPagExterno.ref)
					REPLACE uc_pagsExterno.tlmvl WITH ALLTRIM(&uv_curCursor..tlmvl)
					REPLACE uc_pagsExterno.email WITH ALLTRIM(&uv_curCursor..email)
					REPLACE uc_pagsExterno.duracao WITH ALLTRIM(&uv_curCursor..duracao)
					REPLACE uc_pagsExterno.descricao WITH ALLTRIM(&uv_curCursor..descricao)
					REPLACE uc_pagsExterno.valor WITH uv_valor
					REPLACE uc_pagsExterno.tipoPagExt WITH uc_modosPagExterno.tipoPagExt
					REPLACE uc_pagsExterno.tipoPagExtDesc WITH uc_modosPagExterno.tipoPagExtDesc
					REPLACE uc_pagsExterno.receiverID WITH uc_modosPagExterno.receiverID
					REPLACE uc_pagsExterno.receiverName WITH uc_modosPagExterno.receiverName
					REPLACE uc_pagsExterno.no WITH uv_no
					REPLACE uc_pagsExterno.estab WITH uv_estab
					REPLACE uc_pagsExterno.nrAtend WITH ALLTRIM(uv_nrAtend)

			ENDIF


			SELECT uc_modosPagExterno
		ENDSCAN

		SELECT uc_pagsExterno
      
		IF RECCOUNT("uc_pagsExterno") > 0
			IF !uf_pagamento_reqPagExterno()
				uf_perguntalt_chama("N�o foi poss�vel criar os pagamentos externos. Por favor contacte o suporte.","OK","",16)
				RETURN .F.
			ENDIF
		ENDIF	

	ENDIF

	RETURN .T.

ENDFUNC