DO SERIESSERV
DO PESQSERIES
DO pesqseriesrecursos
DO MRPAGAMENTO
DO pesqrefsagrupadas
DO PLANEAMENTOMR

**
FUNCTION uf_marcacoes_chama
	SET HOURS TO 24	
	
	
	**
	IF !uf_gerais_validaPermUser(m.ch_userno, m.ch_grupo, 'Gest�o de Marca��es')
		uf_perguntalt_chama("O seu perfil n�o permite aceder � Gest�o de Marca��es.", "OK", "", 64)
		RETURN	.f.
	ENDIF
	**
	
	**Valida Licenciamento	
	IF (uf_ligacoes_addConnection('ME') == .f.)
		RETURN .F.
	ENDIF

	IF TYPE("PESQUTENTES") != "U"
		uf_pesqutentes_sair()
	ENDIF

	PUBLIC myMarcacoesIntroducao, myMarcacoesAlteracao, myMarcacoesFtUtentes, myMarcacoesAlteracaoFaturada

	IF TYPE("MARCACOES") == "U"
		uf_marcacoes_cursoresIniciais()
		
		DO FORM MARCACOES
	ELSE
		MARCACOES.show
	ENDIF

	

	uf_marcacoes_actualiza()
	marcacoes.gridPesq.estado.setfocus
ENDFUNC


**
FUNCTION uf_marcacoes_cursoresIniciais
	LOCAL lcSQL
		
	IF USED("ucrsMarcacoesCab")
		fecha("ucrsMarcacoesCab")
	ENDIF
	CREATE CURSOR ucrsMarcacoesCab(data d, ano n(4), mes n(2), mesdesc c(254), utente c(254), especialista c(254), especialidade c(254), servico c(254), horaInicio c(5), horaFim c(5)) 
	SELECT ucrsMarcacoesCab
	APPEND BLANK
	
	**Cursor de Marca��es
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_marcacoes_lista '19000101'
	ENDTEXT
	If !uf_gerais_actGrelha("", "UcrsMarcacoesDia", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar os periodos de Marca��o. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	
	**Cursor de Marca��es
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_marcacoes_lista '19000101'
	ENDTEXT
	If !uf_gerais_actGrelha("", "UCRSMARCACOESDIAAGENDA", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar os periodos de Marca��o. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
		
	**Cursos de Servi�os Associados � marca��o
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_Marcacoes_ServMarc null
	ENDTEXT
	If !uf_gerais_actGrelha("", "uCrsServMarcacao", lcSql)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR OS SERVI�OS DE MARCA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	
	**
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		SET fmtonly on
		exec up_Marcacoes_CompServMarc null
		SET fmtonly off
	ENDTEXT
	If !uf_gerais_actGrelha("", "uCrsCompServMarcacao", lcSql)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR A COMPARTICIPA��O DOS SERVI�OS DE MARCA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	lcSQL = ""
	**Cursor de Recursos Associados � marca��o
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_Marcacoes_RecursoServMarc ''
	ENDTEXT
	If !uf_gerais_actGrelha("", "uCrsRecursosServMarcacao", lcSql)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR OS RECURSOS ASSOCIADOS � MARCA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
		
			
	** Altertas do painel de marca��es
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_marcacoes_ValidaAlertasUtilizador <<ch_userno>>
	ENDTEXT
	If !uf_gerais_actGrelha("", "ucrsAlertasUt", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar os perfis de alertas do utilizador. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF


	** Actualiza Especialidades/utilizadores
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select 	
			b_us.userno
			,b_us.nome
			,b_usesp.especialidade
		From	
			b_usesp
			inner join B_us on b_usesp.userno = B_us.userno
	ENDTEXT
	If !uf_gerais_actGrelha("", "ucrsEspecialidadesUtilizadores", lcSql)
		uf_perguntalt_chama("N�o foi possivel verificar a listagem de especialidades. Por favor contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
		
	SELECT ucrsAlertasUt
	GO Top
	
	IF !USED("ucrsSessoesSerieMr")
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_marcacoes_sessoes null
		ENDTEXT	
		IF !uf_gerais_actgrelha("", "ucrsSessoesSerieMr", lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A VERIFICAR AS SESS�ES ASSOCIADAS � S�RIE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	ENDIF 
	
ENDFUNC 


**
FUNCTION uf_marcacoes_init

	&& Configura menu principal
	WITH MARCACOES.menu1

		&&
		.adicionaOpcao("opcoes","Op��es",myPath + "\imagens\icons\opcoes_w.png","","O")
		.adicionaOpcao("atualizar","Atualizar", myPath + "\imagens\icons\actualizar_w.png","uf_marcacoes_actualiza","A")
		.adicionaOpcao("novaseriesServicos","Servi�os",myPath + "\imagens\icons\mais_w.png","uf_marcacoes_novaSerieServicos")
		
		&&
		.adicionaOpcao("editar", "Editar", myPath + "\imagens\icons\lapis_w.png", "uf_marcacoes_editar","E")
		.adicionaOpcao("editarSerie", "Edit. Serie", myPath + "\imagens\icons\lapis_w.png", "uf_marcacoes_editarSerie","E")
		.adicionaOpcao("utentes", "Utentes", myPath + "\imagens\icons\ut_w.png", "uf_utentes_chama")
		.adicionaOpcao("especialistas", "Especialistas", myPath + "\imagens\icons\operador_w.png", "uf_utilizadores_chama")
		.adicionaOpcao("caixa", "Gest�o Cx", myPath + "\imagens\icons\fecharDia.png", "uf_Identificacao_chama with .t.")
		
	ENDWITH
	
	WITH MARCACOES.menu_opcoes		
		.adicionaOpcao("apagar","Eliminar","","uf_marcacoes_apagar")
		.adicionaOpcao("facturacaoEntidadesClinica","Emiss�o Fatura��o","","uf_FACTENTIDADESCLINICA_chama with ''")
		.adicionaOpcao("imprimirDeclaracaoPresenca", "Dec. Presen�a", "", "uf_marcacoes_declaracaoPresenca")
		.adicionaOpcao("imprimirTalaoLevantamento", "Tal�o Levantamento", "", "uf_marcacoes_TalaoLevantamento")
		.adicionaOpcao("caixa", "Gest�o Cx", "", "uf_Identificacao_chama with .t.")
		.adicionaOpcao("novaseriesAtividades","Atividades",myPath + "\imagens\icons\mais_w.png","uf_marcacoes_novaSerieAtividades")
		.adicionaOpcao("urgente","Urgente",myPath + "\imagens\icons\mais_w.png","uf_marcacoes_novo")
	ENDWITH

	SELECT ucrsMarcacoesCab
	REPLACE ucrsMarcacoesCab.data WITH datetime()+(difhoraria*3600)
	REPLACE ucrsMarcacoesCab.ano WITH YEAR(datetime()+(difhoraria*3600)) 
	REPLACE ucrsMarcacoesCab.mes WITH MONTH(datetime()+(difhoraria*3600))
	REPLACE ucrsMarcacoesCab.mesdesc WITH uf_gerais_getMonthToMonthEx(MONTH(datetime()+(difhoraria*3600)))
	**REPLACE ucrsMarcacoesCab.data WITH DATE()
	**REPLACE ucrsMarcacoesCab.ano WITH YEAR(DATE()) 
	**REPLACE ucrsMarcacoesCab.mes WITH MONTH(DATE())
	**REPLACE ucrsMarcacoesCab.mesdesc WITH uf_gerais_getMonthToMonthEx(MONTH(DATE()))

	
	uf_marcacoes_PreencheCalendario()
	uf_marcacoes_alternaMenu()

ENDFUNC



**
FUNCTION uf_marcacoes_alternaMenu
	LPARAMETERS lcFaturacaoUtentes
	
	WITH MARCACOES.menu1	
		IF EMPTY(lcFaturacaoUtentes)
			IF myMarcacoesIntroducao == .t. OR myMarcacoesAlteracao == .t.
				.estado("opcoes", "SHOW", "Gravar", .t., "Cancelar", .t.)
				.estado("atualizar, novaseriesServicos, editar, editarSerie, utentes, especialistas, caixa", "HIDE")
				
				WITH MARCACOES.menu_opcoes
					.estado("apagar,imprimirDeclaracaoPresenca, imprimirTalaoLevantamento, urgente, novaseriesAtividades", "HIDE")
				ENDWITH
				
				
				**
				IF EMPTY(myMarcacoesAlteracaoFaturada)
				
					WITH MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page2.GridConsumos
					 	FOR i=1 TO .columnCount
							IF .Columns(i).name == "qtt" OR .Columns(i).name == "pvp" OR .Columns(i).name == "comp"
								.Columns(i).readonly = .f.
								.Columns(i).Text1.readonly = .f.
							ENDIF
						ENDFOR
					ENDWITH 

					MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.txtNome.disabledBackColor= RGB(191,223,223)
					MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.no.disabledBackColor= RGB(191,223,223)
					MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.estab.disabledBackColor= RGB(191,223,223)
					MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.obs.disabledBackColor= RGB(191,223,223)
					MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.obs.BackColor= RGB(191,223,223)
					MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.estado.disabledBackColor= RGB(191,223,223)
					MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.hinicio.readonly = .f.
					MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.hfim.readonly = .f.
					MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.hinicio.BackColor= RGB(191,223,223)
					MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.hfim.BackColor= RGB(191,223,223)
					MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.hinicio.disabledBackColor= RGB(191,223,223)
					MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.hfim.disabledBackColor= RGB(191,223,223)
					MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.motivoCancelamento.disabledBackColor= RGB(191,223,223)
					MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.motivoCancelamento.BackColor= RGB(191,223,223)
					MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.ChkTLevantamento.enable(.t.)
					MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.ChkEntregue.enable(.t.)
				ELSE
				
					WITH MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page2.GridConsumos
					 	FOR i=1 TO .columnCount
							IF .Columns(i).name == "qtt" OR .Columns(i).name == "pvp" OR .Columns(i).name == "comp"
								.Columns(i).readonly = .t.
								.Columns(i).Text1.readonly = .t.
							ENDIF
						ENDFOR
					ENDWITH 
					
					MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.ChkTLevantamento.enable(.t.)
					MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.ChkEntregue.enable(.t.)
				ENDIF
				
			ELSE 
				.estado("atualizar, opcoes, novaseriesServicos, editar, editarSerie", "SHOW", "Gravar", .f., "Sair", .t.)
				.estado("utentes, especialistas, caixa", "HIDE")
				
				WITH MARCACOES.menu_opcoes
					.estado("apagar,imprimirDeclaracaoPresenca, imprimirTalaoLevantamento, novaseriesAtividades, urgente", "SHOW")
				ENDWITH
				
				WITH MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page2.GridConsumos
				 	FOR i=1 TO .columnCount
						IF .Columns(i).name == "qtt" OR .Columns(i).name == "pvp" OR .Columns(i).name == "comp"
							.Columns(i).readonly = .t.
							.Columns(i).Text1.readonly = .t.
						ENDIF
					ENDFOR
				ENDWITH 
				
				MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.txtNome.disabledBackColor= RGB(255,255,255)
				MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.no.disabledBackColor= RGB(255,255,255)
				MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.estab.disabledBackColor= RGB(255,255,255)
				MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.obs.disabledBackColor= RGB(255,255,255)
				MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.obs.BackColor= RGB(255,255,255)
				MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.estado.disabledBackColor= RGB(255,255,255)
				MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.hinicio.readonly = .t.
				MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.hfim.readonly = .t.
				MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.hinicio.BackColor= RGB(255,255,255)
				MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.hfim.BackColor= RGB(255,255,255)
				MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.hinicio.disabledBackColor= RGB(255,255,255)
				MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.hfim.disabledBackColor=RGB(255,255,255)
				MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.motivoCancelamento.disabledBackColor= RGB(255,255,255)
				MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.motivoCancelamento.BackColor= RGB(255,255,255)
				
				
				MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.ChkTLevantamento.enable(.f.)
				MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.ChkEntregue.enable(.f.)

				
			ENDIF 
		ELSE
			.estado("caixa", "SHOW", "Pagar", .t., "Cancelar", .t.)
			.estado("opcoes, atualizar, novaseriesServicos, utentes, editar, editarSerie, especialistas", "HIDE")
				
			WITH MARCACOES.menu_opcoes
				.estado("apagar, novaseriesAtividades, urgente", "HIDE")
			ENDWITH
		ENDIF 
	ENDWITH
	
	marcacoes.refresh
ENDFUNC



** SET FILTERS
FUNCTION uf_marcacoes_aplicaFiltros
	LPARAMETERS lcCursor

	IF EMPTY(lcCursor)
		lcCursor = "ucrsMarcacoesDia"	
	ENDIF 

	LOCAL lcFiltro

	STORE '' TO lcFiltro

	IF !USED(lcCursor)
		RETURN .f.
	ENDIF

	
	Select &lcCursor
	SET FILTER TO 
	

	** Construir express�o do filtro
	&& Especialista
	IF !EMPTY(ALLTRIM(UPPER(MARCACOES.especialista.value))) 
	
		IF !EMPTY(ALLTRIM(lcFiltro))
			lcFiltro = lcFiltro + [AND '] + ALLTRIM(UPPER(MARCACOES.especialista.value)) + [' $ ALLTRIM(UPPER(especialistas))]
		ELSE
			lcFiltro = ['] + ALLTRIM(UPPER(MARCACOES.especialista.value)) + [' $ ALLTRIM(UPPER(especialistas))]
		ENDIF
	ENDIF
	
	
	&& especialidade
	IF !EMPTY(ALLTRIM(UPPER(MARCACOES.especialidade.value))) 

		IF !EMPTY(ALLTRIM(lcFiltro))
			lcFiltro = lcFiltro + [AND '] + ALLTRIM(UPPER(MARCACOES.especialidade.value)) + [' $ ALLTRIM(UPPER(especialidade))]
		ELSE
			lcFiltro = ['] + ALLTRIM(UPPER(MARCACOES.especialidade.value)) + [' $ ALLTRIM(UPPER(especialidade))]
		ENDIF

	ENDIF
	
	&&Utente
	IF !EMPTY(ALLTRIM(UPPER(MARCACOES.utente.value)))
		IF !EMPTY(ALLTRIM(lcFiltro))
			lcFiltro = lcFiltro + ' AND UPPER(nome) = ALLTRIM(UPPER(MARCACOES.utente.value))'
		ELSE
			lcFiltro = ' ALLTRIM(UPPER(nome)) = ALLTRIM(UPPER(MARCACOES.utente.value))'
		ENDIF
	ENDIF

	&&Local
	IF !EMPTY(ALLTRIM(UPPER(MARCACOES.local.value)))
		IF !EMPTY(ALLTRIM(lcFiltro))
			lcFiltro = lcFiltro + ' AND UPPER(site) = ALLTRIM(UPPER(MARCACOES.local.value))'
		ELSE
			lcFiltro = ' ALLTRIM(UPPER(site)) = ALLTRIM(UPPER(MARCACOES.local.value))'
		ENDIF
	ENDIF

	** Estado ******************************
	** Pedido
	IF EMPTY(MARCACOES.apedido)
		IF !EMPTY(ALLTRIM(lcFiltro))
			lcFiltro = lcFiltro + [ AND estado != 'PEDIDO']
		ELSE
			lcFiltro = [ estado != 'PEDIDO']
		ENDIF
	ENDIF 
	** Marcado
	IF EMPTY(MARCACOES.amarcado)
		IF !EMPTY(ALLTRIM(lcFiltro))
			lcFiltro = lcFiltro + [ AND estado != 'MARCADO']
		ELSE
			lcFiltro = [ estado != 'MARCADO']
		ENDIF
	ENDIF 
	** Atrasado
	IF EMPTY(MARCACOES.aatrasado)
		IF !EMPTY(ALLTRIM(lcFiltro))
			lcFiltro = lcFiltro + [ AND estado != 'ATRASADO']
		ELSE
			lcFiltro = [ estado != 'ATRASADO']
		ENDIF
	ENDIF
	** Cancelado
	IF EMPTY(MARCACOES.acancelado)
		IF !EMPTY(ALLTRIM(lcFiltro))
			lcFiltro = lcFiltro + [ AND estado != 'CANCELADO']
		ELSE
			lcFiltro = [ estado != 'CANCELADO']
		ENDIF
	ENDIF
	** Presente
	IF EMPTY(MARCACOES.apresente)
		IF !EMPTY(ALLTRIM(lcFiltro))
			lcFiltro = lcFiltro + [ AND estado != 'PRESENTE']
		ELSE
			lcFiltro = [ estado != 'PRESENTE']
		ENDIF
	ENDIF 
	** Tratamento
	IF EMPTY(MARCACOES.atratamento)
		IF !EMPTY(ALLTRIM(lcFiltro))
			lcFiltro = lcFiltro + [ AND estado != 'TRATAMENTO']
		ELSE
			lcFiltro = [ estado != 'TRATAMENTO']
		ENDIF
	ENDIF
	** Efetuado
	IF EMPTY(MARCACOES.aefetuado)
		IF !EMPTY(ALLTRIM(lcFiltro))
			lcFiltro = lcFiltro + [ AND estado != 'EFETUADO']
		ELSE
			lcFiltro = [ estado != 'EFETUADO']
		ENDIF
	ENDIF
	
	
	**
	** Niveis de Urgencia (So aplicado para os estados: PRESENTE; TRATAMENTO)
	** Emergente
	IF EMPTY(MARCACOES.aemergente)
		IF !EMPTY(ALLTRIM(lcFiltro))
			lcFiltro = lcFiltro + [ AND (nivelurgencia != 'EMERGENTE')]
		ELSE
			lcFiltro = [ (nivelurgencia != 'EMERGENTE')]
		ENDIF
	ENDIF
	** MTO. URGENTE
	IF EMPTY(MARCACOES.amtourgente)
		IF !EMPTY(ALLTRIM(lcFiltro))
			lcFiltro = lcFiltro + [ AND (nivelurgencia != 'MTO. URGENTE')]
		ELSE
			lcFiltro = [ (nivelurgencia != 'MTO. URGENTE')]
		ENDIF
	ENDIF
	** URGENTE
	IF EMPTY(MARCACOES.aurgente)
		IF !EMPTY(ALLTRIM(lcFiltro))
			lcFiltro = lcFiltro + [ AND (nivelurgencia != 'URGENTE')]
		ELSE
			lcFiltro = [ (nivelurgencia != 'URGENTE')]
		ENDIF
	ENDIF
	** POUCO URGENTE
	IF EMPTY(MARCACOES.apoucourgente)
		IF !EMPTY(ALLTRIM(lcFiltro))
			lcFiltro = lcFiltro + [ AND (nivelurgencia != 'POUCO URGENTE')]
		ELSE
			lcFiltro = [ (nivelurgencia != 'POUCO URGENTE')]
		ENDIF
	ENDIF
	** N�O URGENTE
	IF EMPTY(MARCACOES.anaourgente)
		IF !EMPTY(ALLTRIM(lcFiltro))
			lcFiltro = lcFiltro + [ AND (nivelurgencia != 'N�O URGENTE')]
		ELSE
			lcFiltro = [ (nivelurgencia != 'N�O URGENTE')]
		ENDIF
	ENDIF
	

	** Series
	IF !EMPTY(ALLTRIM(UPPER(MARCACOES.serie.value)))
		IF !EMPTY(ALLTRIM(lcFiltro))
			lcFiltro = lcFiltro + ' AND UPPER(serienome) = ALLTRIM(UPPER(MARCACOES.serie.value))'
		ELSE
			lcFiltro = ' ALLTRIM(UPPER(serienome)) = ALLTRIM(UPPER(MARCACOES.serie.value))'
		ENDIF
	ENDIF

	** aplicar filtro
	SELECT &lcCursor
	GO top
	IF !EMPTY(ALLTRIM(lcFiltro))
		SELECT &lcCursor
		SET FILTER TO &lcFiltro
	ELSE
		SELECT &lcCursor
		SET FILTER TO 
	ENDIF

	SELECT &lcCursor
	GO top

	IF lcCursor != "UcrsAlertaCalendarioDia"
		MARCACOES.gridPesq.setFocus
		MARCACOES.gridPesq.refresh	
		marcacoes.pageframedata.page1.refresh
	ENDIF 
ENDFUNC 


**
FUNCTION uf_marcacoes_PreencheCalendario
	LOCAL lcData, lcAnoActual, lcMesActual, lcPosDia, lcPosDia1, lcPosDia2, lcPosDia3, lcUltimoDiaMesAnterior, lcUltimoDia, lnShift, ldShift
	LOCAL lcSQL
	STORE 0 TO lcAnoActual, lcMesActual, lcPosDia, lcPosDia1, lcPosDia2, lcPosDia3, lcUltimoDiaMesAnterior, lcUltimoDia
	
	Select ucrsMarcacoesCab
	lcAno = ucrsMarcacoesCab.ano
	lcMes =  ucrsMarcacoesCab.mes
	
	
	** Calcula Marca��es para o M�s
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_mensal <<lcAno>>, <<lcMes>>
	ENDTEXT 
	If !uf_gerais_actGrelha("", "UcrsAlertaCalendarioDia", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar as marca��es mensais. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	uf_marcacoes_aplicaFiltros("UcrsAlertaCalendarioDia") && Aplica Filtros
	
	IF EMPTY(lcAno)
		lcAno = YEAR(datetime()+(difhoraria*3600))
		**lcAno = YEAR(DATE())
	ENDIF
	IF EMPTY(lcMes)
		lcMes= MONTH(datetime()+(difhoraria*3600))
		**lcMes= MONTH(DATE())
	ENDIF
	lcData = DATE(lcAno,lcMes,1)

	**Marca o Primeiro dia do Mes
	DO CASE 
		*2a
		CASE UPPER(CDOW(GOMONTH(lcData, IIF(DAY(lcData)=1,1,0)) - DAY(GOMONTH(lcData, IIF(DAY(lcData)=1,1,0))-1))) == UPPER("Monday")
			lcObj2 = "MARCACOES.d8"
			lcPosDia = 8
		*3a
		CASE UPPER(CDOW(GOMONTH(lcData, IIF(DAY(lcData)=1,1,0)) - DAY(GOMONTH(lcData, IIF(DAY(lcData)=1,1,0))-1))) == UPPER("Tuesday")
			lcObj2 = "MARCACOES.d2"
			lcPosDia = 2
		*4a
		CASE UPPER(CDOW(GOMONTH(lcData, IIF(DAY(lcData)=1,1,0)) - DAY(GOMONTH(lcData, IIF(DAY(lcData)=1,1,0))-1))) == UPPER("Wednesday")
			lcObj2 = "MARCACOES.d3"
			lcPosDia = 3
		*5a
		CASE UPPER(CDOW(GOMONTH(lcData, IIF(DAY(lcData)=1,1,0)) - DAY(GOMONTH(lcData, IIF(DAY(lcData)=1,1,0))-1))) == UPPER("Thursday")
			lcObj2 = "MARCACOES.d4"
			lcPosDia = 4
		*6a
		CASE UPPER(CDOW(GOMONTH(lcData, IIF(DAY(lcData)=1,1,0)) - DAY(GOMONTH(lcData, IIF(DAY(lcData)=1,1,0))-1))) == UPPER("Friday")
			lcObj2 = "MARCACOES.d5"
			lcPosDia = 5
		*Sabado
		CASE UPPER(CDOW(GOMONTH(lcData, IIF(DAY(lcData)=1,1,0)) - DAY(GOMONTH(lcData, IIF(DAY(lcData)=1,1,0))-1))) == UPPER("Saturday")
			lcObj2 = "MARCACOES.d6"
			lcPosDia = 6
		*Domingo
		CASE UPPER(CDOW(GOMONTH(lcData, IIF(DAY(lcData)=1,1,0)) - DAY(GOMONTH(lcData, IIF(DAY(lcData)=1,1,0))-1))) == UPPER("Sunday")
			lcObj2 = "MARCACOES.d7"
			lcPosDia = 7
		OTHERWISE
			**
	ENDCASE
	
	&lcObj2..label1.caption = "1"
	&lcObj2..label1.foreColor = RGB(3,128,139)
	&lcObj2..borderColor = RGB(163,163,163)
	&lcObj2..borderWidth = 0
	&lcObj2..backColor	= RGB(255,255,255)
	&lcObj2..enabled 	= .t.
	&lcObj2..label1.foreColor = RGB(3,128,139)
	
	Select UcrsAlertaCalendarioDia
	COUNT TO lcExistemMarcacoes FOR UcrsAlertaCalendarioDia.data == lcData
	IF lcExistemMarcacoes > 0
		&lcObj2..label1.backColor =  RGB(200,200,255)
	ENDIF 
		
	**Calcula o ultimo dia do mes
	LOCAL lnShift, ldShift
	lnShift = 35-DAY(lcData) && enough days to move us into the next month	
	ldShift = lcData+lnShift
	lcUltimoDia = DAY(ldShift-DAY(ldShift))
	*********************

	lcPosDia1 = lcPosDia  
	***Preeche caption para o mes actual
	FOR i = 1 TO lcUltimoDia 
		
		lcObj = "MARCACOES.d" + ALLTRIM(STR(lcPosDia1))
		&lcObj..borderColor = RGB(163,163,163)
		&lcObj..borderWidth = 0
		&lcObj..backColor	= RGB(255,255,255)
		&lcObj..enabled 	= .t.
		&lcObj..label1.foreColor = RGB(3,128,139)
		&lcObj..label1.fontsize = 10
		&lcObj..label1.caption = ALLTRIM(STR(i))
		
		lcPosDia1 = lcPosDia1 + 1
		Select UcrsAlertaCalendarioDia
		COUNT TO lcExistemMarcacoes FOR UcrsAlertaCalendarioDia.data == lcData + i - 1 
		IF lcExistemMarcacoes > 0
			&lcObj..backColor = RGB(42,144,155)&&RGB(232,76,61) && Cor Lt
			&lcObj..label1.foreColor = RGB(255,255,255)
		ENDIF 
	ENDFOR
	******************************************************

	**Preenche dias Do M�s Anterior
	**Calcula o ultimo dia do mes anterior
	lnShift = 35-DAY(GOMONTH(lcData, -1)) && enough days to move us into the next month	
	ldShift = GOMONTH(lcData, -1)+lnShift
	lcUltimoDiaMesAnterior = DAY(ldShift-DAY(ldShift))
	lcPosDia2 = lcPosDia - 1
	DO WHILE lcPosDia2 > 0
		lcObj = "MARCACOES.d" + ALLTRIM(STR(lcPosDia2))
		&lcObj..label1.caption 	= ALLTRIM(STR(lcUltimoDiaMesAnterior))
		&lcObj..backColor	= RGB(234,234,234)
		&lcObj..borderWidth = 0
		&lcObj..enabled 	= .f.
		&lcObj..borderColor = RGB(226,226,226)
		&lcObj..label1.foreColor = RGB(190,190,190)
		&lcObj..label1.fontsize = 10
		&lcObj..label1.forecolor = RGB(42,143,154)
		
		lcPosDia2 = lcPosDia2 - 1
		lcUltimoDiaMesAnterior = lcUltimoDiaMesAnterior - 1

	ENDDO
	*****************************************************

	** Preenche dias Do M�s Seguinte *************************
	lcPosDia3 = lcPosDia + lcUltimoDia 
	i = 1
	DO WHILE lcPosDia3 <= 42

		lcObj = "MARCACOES.d" + ALLTRIM(STR(lcPosDia3))
		&lcObj..label1.caption 	= 	ALLTRIM(STR(i))
		&lcObj..backColor	= RGB(234,234,234)
		&lcObj..borderWidth = 0
		&lcObj..enabled 	= .f.
		&lcObj..borderColor = RGB(226,226,226)
		&lcObj..label1.foreColor = RGB(190,190,190)
		&lcObj..label1.fontsize = 10
		&lcObj..label1.forecolor = RGB(42,143,154)
				
		lcPosDia3 = lcPosDia3 + 1
		i= i +1
	ENDDO
	*********************************************************

ENDFUNC


**
FUNCTION uf_marcacoes_actualiza
	**
	uf_marcacoes_PreencheCalendario()
	
	** Actualiza Especialidades/utilizadores
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select 	
			b_us.userno, b_us.nome, especialidade
		From
			b_usesp (nolock)
			inner join B_us (nolock) on b_usesp.userno = B_us.userno
	ENDTEXT
	If !uf_gerais_actGrelha("", "ucrsEspecialidadesUtilizadores", lcSql)
		uf_perguntalt_chama("N�o foi possivel verificar a listagem de especialidades. Por favor contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	
	**
	IF UPPER(ALLTRIM(Marcacoes.visualizacao.value)) == "AGENDA"
		UF_MARCACOES_activateVistaAgenda()
		
	ELSE
		SELECT ucrsMarcacoesCab
		uf_marcacoes_AplicaDia(LEFT(uf_gerais_getdate(ucrsMarcacoesCab.data,"DATA"),2))
	ENDIF 	
	
ENDFUNC



**
FUNCTION uf_marcacoes_AplicaDia
	LPARAMETERS lcDia

	IF myMarcacoesIntroducao == .t. OR myMarcacoesAlteracao == .t. OR myMarcacoesFtUtentes == .t.
		uf_perguntalt_chama("Deve gravar ou cancelar a grava��o antes de alterar o dia.", "", "OK", 64)
		return .f.
	ENDIF
	
	IF empty(lcDia)
		RETURN .f.
	ENDIF
	
	marcacoes.dia = ALLTRIM(STR(VAL(lcDia)))
 	
	i = 1 && Colocar cor de dia activo
	DO WHILE i <= 42

		lcObj = "MARCACOES.d" + ALLTRIM(STR(i))
		
		IF &lcObj..enabled 	== .t.
			IF &lcObj..label1.caption == marcacoes.dia
				&lcObj..borderColor = RGB(232,76,61) &&RGB(42,143,154)
				&lcObj..borderWidth = 2
			ELSE
				&lcObj..borderColor = RGB(255,255,255)
				&lcObj..borderWidth = 0
			ENDIF
		ENDIF 
		i= i +1
	ENDDO
	
	SELECT ucrsMarcacoesCab
	lcAno = ucrsMarcacoesCab.ano
	lcMes = ucrsMarcacoesCab.mes
	lcDia = VAL(marcacoes.dia)
	Replace ucrsMarcacoesCab.data WITH DATE(lcAno,lcMes, lcDia)

	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_marcacoes_lista '<<uf_gerais_getdate(ucrsMarcacoesCab.data,"SQL")>>'
	ENDTEXT
	
	If !uf_gerais_actGrelha("MARCACOES.gridPesq", "UcrsMarcacoesDia", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar os periodos de Marca��o. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	
	
	uf_marcacoes_aplicaFiltros("ucrsMarcacoesDia")
	
	SELECT ucrsMarcacoesDia
	GO top
	MARCACOES.gridPesq.setFocus
	MARCACOES.gridPesq.refresh	
	marcacoes.pageframedata.page1.refresh
	
ENDFUNC



**
FUNCTION uf_marcacoes_expandeDetalhe

	WITH MARCACOES
		If marcacoes.detalhesVisiveis == .f.
			.gridPesq.height = marcacoes.height - 200&&70 
			marcacoes.detalhesVisiveis = .t.
			.Container1.ContainerDetalhes.visible = .f.
			.Container1.ContainerLocal.check1.config(myPath + "\imagens\icons\pageup_B.png", "")
		ELSE
			.gridPesq.height = .Container1.ContainerDetalhes.top - 10 &&20
			marcacoes.detalhesVisiveis = .f.
			.Container1.ContainerDetalhes.visible = .t.
			.Container1.ContainerLocal.check1.config(myPath + "\imagens\icons\pagedown_B.png", "")
		ENDIF
		
		.Container1.ContainerLocal.top = .gridPesq.height + 5
	ENDWITH 
	
ENDFUNC



FUNCTION uf_marcacoes_verificarcor
	Lparameters lcestado, lcrecno
	Local lccor 
	
	DO CASE 
		CASE upper(alltrim(lcestado)) == "PEDIDO"
			lccor = RGB(183,183,183)
		CASE upper(alltrim(lcestado)) == "MARCADO"
			lccor = RGB(123,185,225)
		CASE upper(alltrim(lcestado)) == "ATRASADO"
			lccor =  RGB(241,196,15)
		CASE upper(alltrim(lcestado)) == "CANCELADO"
			lccor = rgb(0,0,0)
		CASE upper(alltrim(lcestado)) == "PRESENTE"
			lccor = RGB(0,128,64)
		CASE upper(alltrim(lcestado)) == "TRATAMENTO"
			lccor = rgb(34,185,178)
		CASE upper(alltrim(lcestado)) == "EFETUADO"
			lccor = rgb(0,0,0)
		OTHERWISE 
			lccor = rgb(255,255,255)
	
	ENDCASE 
		
	return lccor
ENDFUNC 


**
FUNCTION uf_Marcacoes_verificarCorNivelUrgencia
	LPARAMETERS lcNivelUrgencia, lcrecno
	
	DO CASE 
		CASE upper(alltrim(lcNivelUrgencia)) == "EMERGENTE"
			lccor = RGB(232,76,61)
		CASE upper(alltrim(lcNivelUrgencia)) == "MTO. URGENTE"
			lccor = RGB(238,142,56)
		CASE upper(alltrim(lcNivelUrgencia)) == "URGENTE"
			lccor = RGB(243,181,24)
		CASE upper(alltrim(lcNivelUrgencia)) == "POUCO URGENTE"
			lccor = RGB(0,217,0)
		CASE upper(alltrim(lcNivelUrgencia)) == "N�O URGENTE"
			lccor = RGB(0,232,232)
		OTHERWISE 
			lccor = rgb(255,255,255)
	
	ENDCASE 
	
	return lccor
ENDFUNC 


**
FUNCTION uf_marcacoes_verificarBackColor
	Lparameters lcestado, lcrecno
	
	IF lcrecno !=0 
		lccor = rgb(255,255,255)
	ELSE
		lccor = rgb(240,240,240)
	ENDIF
ENDFUNC 


**
FUNCTION uf_marcacoes_gridMarcDblClick
	marcacoes.detalhesVisiveis = .t.
	uf_marcacoes_expandeDetalhe()
	uf_marcacoes_editar()
ENDFUNC


**
FUNCTION uf_marcacoes_gridMarcClick
	marcacoes.detalhesVisiveis = .t.
	uf_marcacoes_expandeDetalhe()
ENDFUNC


**
FUNCTION uf_marcacoes_editar
	IF myMarcacoesIntroducao == .t. OR myMarcacoesAlteracao == .t. 
 		RETURN .f.
 	ENDIF 
 	  	
 	  	
 	SELECT uCrsServMarcacao
	GO Top
 	LOCATE FOR !EMPTY(uCrsServMarcacao.fu) OR !EMPTY(uCrsServMarcacao.fe) OR !EMPTY(uCrsServMarcacao.honorario)
 	IF FOUND()
 		myMarcacoesAlteracaoFaturada = .t.
 	ENDIF  	
*!*	 	  	
 	myMarcacoesAlteracao = .t.
	
 	uf_marcacoes_alternaMenu()
 	
 	marcacoes.detalhesVisiveis = .t.
	uf_marcacoes_expandeDetalhe()
 	
ENDFUNC 


**
FUNCTION uf_marcacoes_editarSerie

	uf_SERIESSERV_chama('', .f., .f., .f., UcrsMarcacoesDia.serieNo)
	uf_SERIESSERV_editar()

ENDFUNC 



**
FUNCTION uf_marcacoes_apagar
 	
 	**
	IF !uf_gerais_validaPermUser(m.ch_userno, m.ch_grupo, 'Marca��es - Eliminar')
		uf_perguntalt_chama("O seu perfil n�o permite eliminar marca��es.", "OK", "", 64)
		RETURN	.f.
	ENDIF
	**

 	
 	IF myMarcacoesIntroducao == .t. OR myMarcacoesAlteracao == .t. OR myMarcacoesFtUtentes == .t.
 		RETURN .f.
 	ENDIF 
 	
 	
 	SELECT uCrsServMarcacao
 	GO Top
 	LOCATE FOR !EMPTY(uCrsServMarcacao.fu) OR !EMPTY(uCrsServMarcacao.fe) OR !EMPTY(uCrsServMarcacao.honorario)
 	IF FOUND()
 		uf_perguntalt_chama("N�o pode eliminar uma marca��o Faturada.", "", "OK", 32)
 		RETURN .f.
 	ENDIF 
 	
 	
 	SELECT ucrsMarcacoesDia
 	LOCAL lcSQL
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		DELETE FROM marcacoes WHERE mrstamp = '<<ALLTRIM(ucrsMarcacoesDia.mrstamp)>>'
		DELETE FROM marcacoesServ WHERE mrstamp = '<<ALLTRIM(ucrsMarcacoesDia.mrstamp)>>'
		DELETE FROM cpt_marcacoes WHERE mrstamp = '<<ALLTRIM(ucrsMarcacoesDia.mrstamp)>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("", "ucrsApagaMarc", lcSQL)
		MESSAGEBOX("N�o foi possivel eliminar a marca��o. Por favor contacte o suporte",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ELSE
		** Aplica dia actual	
		SELECT ucrsMarcacoesCab
		uf_marcacoes_AplicaDia(LEFT(uf_gerais_getdate(ucrsMarcacoesCab.data,"DATA"),2))
		uf_perguntalt_chama("Marca��o eliminada com sucesso!", "", "OK", 64)
	ENDIF
	
ENDFUNC



**
FUNCTION uf_marcacoes_camposObrigatorios
	
	SELECT ucrsMarcacoesDia
	
	&&Utente
	IF EMPTY(ALLTRIM(ucrsMarcacoesDia.nome))
		uf_perguntalt_chama("Exitem campos obrigat�rios por preencher: Identifica��o do utente.", "", "OK", 64)
		RETURN .f.
	ENDIF	
	
	&&ESTADO
	IF EMPTY(ALLTRIM(ucrsMarcacoesDia.estado)) OR ALLTRIM(UPPER(ucrsMarcacoesDia.estado)) == "DISPONIVEL"
		uf_perguntalt_chama("Exitem campos obrigat�rios por preencher: Deve atribuir um estado � marca��o diferente de dispon�vel.", "", "OK", 64)
		RETURN .f.
	ENDIF
	
	
	IF ALLTRIM(UPPER(ucrsMarcacoesDia.estado)) == "CANCELADO" 
		IF EMPTY(ALLTRIM(ucrsMarcacoesDia.motivoCancelamento))
			uf_perguntalt_chama("� obrigat�rio indicar motivo de cancelamento da marca��o.", "", "OK", 64)
			RETURN .f.
		ENDIF 
		
		IF UPPER(ALLTRIM(ucrsMarcacoesDia.motivoCancelamento)) == "OUTROS" AND EMPTY(ALLTRIM(ucrsMarcacoesDia.obs))
			uf_perguntalt_chama("� obrigat�rio indicar observa��es para motivo de cancelamento da marca��o: OUTROS.", "", "OK", 64)
			RETURN .f.
		ENDIF 
		
	ENDIF 	

	
	
*!*		&&ESPECIALIDADE
*!*		IF EMPTY(ALLTRIM(ucrsMarcacoesDia.dvnome)) AND EMPTY(ALLTRIM(ucrsMarcacoesDia.recursonome))
*!*			uf_perguntalt_chama("Exitem campos obrigat�rios por preencher: Especialidade.", "", "OK", 64)
*!*			RETURN .f.
*!*		ENDIF	
*!*		
*!*		&&ESPECIALISTA
*!*		IF EMPTY(ALLTRIM(ucrsMarcacoesDia.drnome)) AND EMPTY(ALLTRIM(ucrsMarcacoesDia.recursonome))
*!*			uf_perguntalt_chama("Exitem campos obrigat�rios por preencher: Especialista.", "", "OK", 64)
*!*			RETURN .f.
*!*		ENDIF	

	RETURN .t.
ENDFUNC 


**
FUNCTION uf_marcacoes_gravar
	PUBLIC myCxStamp
	LOCAL lcMrstamp, lcNrRecursos 
	SELECT ucrsMarcacoesDia
	lcMrstamp = ALLTRIM(ucrsMarcacoesDia.mrstamp)
	lcSite = ALLTRIM(ucrsMarcacoesDia.site)
	lcNrRecursos = 0	
	
	
	** N�o deixa gravar sem recursos	
	SELECT uCrsRecursosServMarcacao
	COUNT TO lcNrRecursos
	IF lcNrRecursos == 0
		uf_perguntalt_chama("A marca��o n�o tem recursos associados. Por favor verifique.", "", "OK", 32)
		RETURN .f.
	ENDIF 
		
		
	**	
	IF myMarcacoesFtUtentes && Gerar Fatura��o Utentes

		** Valida se a marca��o � do local actual, caso n�o seja n�o permite a factura��o
		IF UPPER(ALLTRIM(lcSite)) !=  UPPER(ALLTRIM(mysite))
			uf_perguntalt_chama("N�o � possivel fatuar uma marca��o de outro Local.", "", "OK", 32)
			RETURN .f.
		ENDIF 
		

		IF uf_perguntalt_chama("Tipo de Factura��o.", "Dinheiro", "Cr�dito", 64)
			&& VD
			uf_mrpagamento_emitirDocumento(.f.)
		ELSE
			uf_mrpagamento_emitirDocumento(.t.)		
		ENDIF 

		
		return .f.
	ENDIF 
	
	LOCAL lcInicio, lcFim
	
	** For�a grava��o dos dados no cursor
	TABLEUPDATE(.t.,.f.,"ucrsMarcacoesDia")
		
	LOCAL lcExisteMarc, lcCamposObrigatorios, lcValidaInsertServicosMarcacao, lcMrstampActual 
	STORE .f. TO lcCamposObrigatorios, lcValidaInsertServicosMarcacao 
	STORE .f. TO lcExisteMarc
	STORE '' TO lcInicio, lcFim, lcMrstampActual 
	
	lcCamposObrigatorios = uf_marcacoes_camposObrigatorios()
	
	IF lcCamposObrigatorios==.f.
		RETURN .f.
	ENDIF


	SELECT ucrsMarcacoesCab
	lcAno = ucrsMarcacoesCab.ano
	lcMes = ucrsMarcacoesCab.mes
	lcDia = VAL(marcacoes.dia)

*!*		SELECT UCRSMARCACOESDIA
*!*		REPLACE UCRSMARCACOESDIA.data WITH DATE(lcAno , lcMes, lcDia)

	lcInicio = UCRSMARCACOESDIA.hinicio
	lcFim  = UCRSMARCACOESDIA.hfim


	&& Introdu��o
	IF myMarcacoesIntroducao == .t.
				
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_marcacoes_numero
		ENDTEXT
		IF !uf_gerais_actGrelha("", "uCrsNumeraMarcacao",lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A VERIFICAR NUMERA��O DE MARCA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
		
		Select UCRSMARCACOESDIA
		replace UCRSMARCACOESDIA.mrno WITH uCrsNumeraMarcacao.mrno 
		
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			INSERT INTO	marcacoes (	
				mrstamp
				,data
				,dataFim
				,hinicio
				,hfim
				,utstamp
				,nome
				,no
				,estab
				,obs
				,serieno
				,serienome
				,sessao
				,estado
				,nivelurgencia
				,mrno
				,id_us
				,data_cri
				,id_us_alt
				,data_alt
				,site
				,motivocancelamento
				,talaolevantamento
				,LocalEntrega
				,EntregueTLev
				,designEntrega
				,dataEntrega
				,userEntrega
				,dataLevantamento
			)VALUES (
				'<<ALLTRIM(UCRSMARCACOESDIA.mrstamp)>>'
				,'<<uf_gerais_getdate(UCRSMARCACOESDIA.data,"SQL")>>'
				,'<<uf_gerais_getdate(UCRSMARCACOESDIA.dataFim,"SQL")>>'
				,'<<ALLTRIM(UCRSMARCACOESDIA.hinicio)>>'
				,'<<ALLTRIM(UCRSMARCACOESDIA.hfim)>>'
				,'<<ALLTRIM(UCRSMARCACOESDIA.utstamp)>>'
				,'<<ALLTRIM(UCRSMARCACOESDIA.nome)>>'
				,<<UCRSMARCACOESDIA.no>>
				,<<UCRSMARCACOESDIA.estab>>
				,'<<UCRSMARCACOESDIA.OBS>>'
				,<<UCRSMARCACOESDIA.serieno>>
				,'<<ALLTRIM(UCRSMARCACOESDIA.serienome)>>'
				,<<UCRSMARCACOESDIA.sessao>>
				,'<<ALLTRIM(UCRSMARCACOESDIA.ESTADO)>>'
				,'<<ALLTRIM(UCRSMARCACOESDIA.nivelurgencia)>>'
				,<<UCRSMARCACOESDIA.mrno>>
				,<<ch_userno>>
				,dateadd(HOUR, <<difhoraria>>, getdate())
				,<<ch_userno>>
				,dateadd(HOUR, <<difhoraria>>, getdate())
				,'<<ALLTRIM(UCRSMARCACOESDIA.site)>>'
				,'<<ALLTRIM(UCRSMARCACOESDIA.motivocancelamento)>>'
				,<<IIF(UCRSMARCACOESDIA.talaolevantamento,1,0)>>
				,'<<ALLTRIM(UCRSMARCACOESDIA.LocalEntrega)>>'
				,<<IIF(UCRSMARCACOESDIA.EntregueTLev,1,0)>>
				,'<<ALLTRIM(UCRSMARCACOESDIA.designEntrega)>>'
				,'<<uf_gerais_getdate(UCRSMARCACOESDIA.dataEntrega,"SQL")>>'
				,<<UCRSMARCACOESDIA.userEntrega>>
				,'<<uf_gerais_getdate(UCRSMARCACOESDIA.datalevantamento,"SQL")>>'
			)
		ENDTEXT

*!*	_CLIPTEXT = lcSQL
*!*	MESSAGEBOX(lcSQL)

		IF !uf_gerais_actGrelha("", "",lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A INSERIR O DETALHE DE MARCA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			lcValidaUpdateMarcacao = .f.
		ENDIF
	ENDIF
	
	
	IF myMarcacoesAlteracao == .t.	
	
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			UPDATE	
				marcacoes
			SET		
				data = '<<uf_gerais_getdate(UCRSMARCACOESDIA.data,"SQL")>>'
				,dataFim = '<<uf_gerais_getdate(UCRSMARCACOESDIA.dataFim,"SQL")>>'
				,hinicio = '<<ALLTRIM(UCRSMARCACOESDIA.hinicio)>>'
				,hfim = '<<ALLTRIM(UCRSMARCACOESDIA.hfim)>>'
				,utstamp = '<<ALLTRIM(UCRSMARCACOESDIA.utstamp)>>'
				,nome = '<<ALLTRIM(UCRSMARCACOESDIA.nome)>>'
				,no = <<UCRSMARCACOESDIA.no>>
				,estab = <<UCRSMARCACOESDIA.estab>>
				,obs = '<<UCRSMARCACOESDIA.OBS>>'
				,serieno = <<UCRSMARCACOESDIA.serieno>>
				,serienome = '<<ALLTRIM(UCRSMARCACOESDIA.serienome)>>'
				,sessao  = <<UCRSMARCACOESDIA.sessao>>
				,estado = '<<ALLTRIM(UCRSMARCACOESDIA.estado)>>'
				,nivelurgencia = '<<ALLTRIM(UCRSMARCACOESDIA.nivelurgencia )>>'
				,mrno = <<UCRSMARCACOESDIA.mrno>>
				,id_us_alt = <<ch_userno>>
				,data_alt = dateadd(HOUR, <<difhoraria>>, getdate())
				,site = '<<ALLTRIM(UCRSMARCACOESDIA.site)>>'
				,motivocancelamento = '<<ALLTRIM(UCRSMARCACOESDIA.motivocancelamento)>>'
				,talaolevantamento = <<IIF(UCRSMARCACOESDIA.talaolevantamento,1,0)>>
				,LocalEntrega = '<<ALLTRIM(UCRSMARCACOESDIA.LocalEntrega)>>'
				,EntregueTLev = <<IIF(UCRSMARCACOESDIA.EntregueTLev,1,0)>>
				,designEntrega = '<<ALLTRIM(UCRSMARCACOESDIA.designEntrega)>>'
				,dataEntrega = '<<uf_gerais_getdate(UCRSMARCACOESDIA.dataEntrega,"SQL")>>'
				,userEntrega = <<UCRSMARCACOESDIA.userEntrega>>
				,datalevantamento = '<<uf_gerais_getdate(UCRSMARCACOESDIA.datalevantamento,"SQL")>>'
			WHERE	
				mrstamp	= '<<ALLTRIM(UCRSMARCACOESDIA.mrstamp)>>'
		ENDTEXT

		IF !uf_gerais_actGrelha("", "",lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A ACTUALIZAR A MARCA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			lcValidaUpdateMarcacao = .f.
		ENDIF
	ENDIF
	
	
	**Grava Comparticipa��es
	uf_marcacoes_gravaCompartServicos()
	
	** Grava Servi�os
	uf_marcacoes_gravaServicos()
	
	** Grava Recursos
	uf_marcacoes_gravaRecursos()
	
	myMarcacoesIntroducao = .f.
	myMarcacoesAlteracao = .f.
	uf_marcacoes_alternaMenu()
		

	**	
	** Atualizada dados gravados no cursor
	SELECT ucrsMarcacoesCab
	uf_marcacoes_AplicaDia(LEFT(uf_gerais_getdate(ucrsMarcacoesCab.data,"DATA"),2))
	
	**
	MARCACOES.mrstamp = ALLTRIM(lcMrstamp)
	SELECT ucrsMarcacoesDia
	GO Top
	SELECT ucrsMarcacoesDia
	LOCATE FOR ALLTRIM(ucrsMarcacoesDia.mrstamp) == ALLTRIM(MARCACOES.mrstamp)
	
	** Coloca na posi��o de grava��o
	uf_marcacoes_AfterRowColChange()
	MARCACOES.gridPesq.refresh
	MARCACOES.gridPesq.setFocus
	MARCACOES.gridPesq.nome.setfocus	
	
	
ENDFUNC


**
Function uf_marcacoes_gravaServicos

	** Apaga os servi�os que j� n�o existem
	TEXT TO lcSQL NOSHOW TEXTMERGE
		Select servmrstamp FROM marcacoesServ WHERE mrstamp = '<<uCrsServMarcacao.mrstamp>>'
	ENDTEXT 
	IF !uf_gerais_actGrelha("", "ucrsMarcacoesAnteriores",lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A INSERIR SERVI�O DE MARCA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	Select ucrsMarcacoesAnteriores.servmrstamp ;
	FROM ;
		ucrsMarcacoesAnteriores ;
		LEFT JOIN uCrsServMarcacao ON  ucrsMarcacoesAnteriores.servmrstamp == uCrsServMarcacao.servmrstamp ;
	WHERE ;
		ISNULL(uCrsServMarcacao.servmrstamp) == .t.;
	INTO CURSOR ucrsServMarcaEliminar READWRITE
	
	Select ucrsServMarcaEliminar 
	GO Top
	SCAN
		TEXT TO lcSQL NOSHOW TEXTMERGE
			DELETE FROM marcacoesServ WHERE servmrstamp  ='<<ucrsServMarcaEliminar.servmrstamp>>'
		ENDTEXT 

		IF !uf_gerais_actGrelha("", "",lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A ELIMINAR SERVI�O DE MARCA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	ENDSCAN 

	Select ucrsMarcacoesDia
	lcUtstamp = ucrsMarcacoesDia.utstamp
	lcMrStamp = ucrsMarcacoesDia.mrstamp
	lcDataIni = ucrsMarcacoesDia.data
	lcDataFim = ucrsMarcacoesDia.dataFim

	Select uCrsServMarcacao
	GO Top
	SCAN
	
	
		TEXT TO lcSQL NOSHOW TEXTMERGE
			if not exists (select * from marcacoesServ where servmrstamp = '<<uCrsServMarcacao.servmrstamp>>')
			BEGIN			
				insert into marcacoesServ (
					servmrstamp
					,mrstamp
					,ref
					,design
					,duracao
					,qtt
					,pvp
					,compart
					,total
					,dataInicio
					,dataFim
					,hinicio
					,hfim
					,ordem
				) values(
					'<<uCrsServMarcacao.servmrstamp>>'
					,'<<uCrsServMarcacao.mrstamp>>'
					,'<<uCrsServMarcacao.ref>>'
					,'<<uCrsServMarcacao.design>>'
					,<<uCrsServMarcacao.duracao>>
					,<<uCrsServMarcacao.qtt>>
					,<<uCrsServMarcacao.pvp>>
					,<<uCrsServMarcacao.compart>>
					,<<uCrsServMarcacao.total>>
					,'<<uf_gerais_getdate(uCrsServMarcacao.datainicio,"SQL")>>'
					,'<<uf_gerais_getdate(uCrsServMarcacao.datafim,"SQL")>>'
					,'<<uCrsServMarcacao.hInicio>>'
					,'<<uCrsServMarcacao.hFim>>'
					,<<uCrsServMarcacao.ordem>>
				)
			END
			ELSE
			BEGIN
				update marcacoesServ 
				SET 
					mrstamp = '<<uCrsServMarcacao.mrstamp>>'
					,ref = '<<uCrsServMarcacao.ref>>'
					,design = '<<uCrsServMarcacao.design>>'
					,duracao = <<uCrsServMarcacao.duracao>>
					,qtt = <<uCrsServMarcacao.qtt>>
					,pvp = <<uCrsServMarcacao.pvp>>
					,compart = <<uCrsServMarcacao.compart>>
					,total =<<uCrsServMarcacao.total>>
					,dataInicio = '<<uf_gerais_getdate(uCrsServMarcacao.datainicio,"SQL")>>'
					,dataFim = '<<uf_gerais_getdate(uCrsServMarcacao.datafim,"SQL")>>'
					,hinicio = '<<uCrsServMarcacao.hInicio>>'
					,hfim = '<<uCrsServMarcacao.hFim>>'
					,ordem = <<uCrsServMarcacao.ordem>>
				Where
					servmrstamp = '<<uCrsServMarcacao.servmrstamp>>'
			END
			
		ENDTEXT
*!*	_CLIPTEXT = lcSQL
*!*	MESSAGEBOX(lcSQL)				
		IF !uf_gerais_actGrelha("", "",lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A INSERIR SERVI�O DE MARCA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	
	ENDSCAN 
	
	
	
ENDFUNC 



**
Function uf_marcacoes_gravaCompartServicos

	** Apaga comparticipa��es que j� n�o existem
	TEXT TO lcSQL NOSHOW TEXTMERGE
		Select servcompmrstamp FROM cpt_marcacoes WHERE mrstamp = '<<uCrsServMarcacao.mrstamp>>'
	ENDTEXT 
	IF !uf_gerais_actGrelha("", "ucrsMarcacoesCompAnteriores",lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A INSERIR COMPARTICIPA��O DE MARCA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	Select ucrsMarcacoesCompAnteriores.servcompmrstamp ;
	FROM ;
		ucrsMarcacoesCompAnteriores;
		LEFT JOIN uCrsCompServMarcacao ON  ucrsMarcacoesCompAnteriores.servcompmrstamp == uCrsCompServMarcacao.servcompmrstamp ;
	WHERE ;
		ISNULL(uCrsCompServMarcacao.servcompmrstamp) == .t.;
	INTO CURSOR ucrsServCompMarcaEliminar READWRITE
	
	Select ucrsServCompMarcaEliminar 
	GO Top
	SCAN
		TEXT TO lcSQL NOSHOW TEXTMERGE
			DELETE FROM cpt_marcacoes WHERE servcompmrstamp ='<<ucrsServCompMarcaEliminar .servcompmrstamp >>'
		ENDTEXT 

		IF !uf_gerais_actGrelha("", "",lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A ELIMINAR COMPARTICIPA��O DE MARCA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	ENDSCAN 

	Select uCrsCompServMarcacao
	GO Top
	SCAN
			
	
		TEXT TO lcSQL NOSHOW TEXTMERGE
			if not exists (select * from cpt_marcacoes where servcompmrstamp = '<<uCrsCompServMarcacao.servcompmrstamp>>')
			BEGIN			
				insert into cpt_marcacoes(
					servcompmrstamp
					,servmrstamp
					,mrstamp
					,id_cp
					,ordem
					,valor
					,entidade
					,entidadeno
					,entidadeestab
					,compart
					,maximo
					,tipoCompart								
				) values(
					'<<uCrsCompServMarcacao.servcompmrstamp>>'
					,'<<uCrsCompServMarcacao.servmrstamp>>'
					,'<<uCrsCompServMarcacao.mrstamp>>'
					,'<<uCrsCompServMarcacao.id_cp>>'
					,<<uCrsCompServMarcacao.ordem>>
					,<<uCrsCompServMarcacao.valor>>
					,'<<uCrsCompServMarcacao.entidade>>'
					,<<uCrsCompServMarcacao.entidadeno>>
					,<<uCrsCompServMarcacao.entidadeestab>>
					,<<uCrsCompServMarcacao.compart>>
					,<<uCrsCompServMarcacao.maximo>>
					,'<<uCrsCompServMarcacao.tipoCompart>>'
				)
			
			END
			ELSE
			BEGIN
				update cpt_marcacoes
				SET 
					servmrstamp = '<<uCrsCompServMarcacao.servmrstamp>>'
					,mrstamp = '<<uCrsCompServMarcacao.mrstamp>>'
					,id_cp = '<<uCrsCompServMarcacao.id_cp>>'
					,ordem = <<uCrsCompServMarcacao.ordem>>
					,valor = <<uCrsCompServMarcacao.valor>>
					,entidade = '<<uCrsCompServMarcacao.entidade>>'
					,entidadeno = <<uCrsCompServMarcacao.entidadeno>>
					,entidadeestab = <<uCrsCompServMarcacao.entidadeestab>>
					,compart = <<uCrsCompServMarcacao.compart>>
					,maximo = <<uCrsCompServMarcacao.maximo>>
					,tipoCompart = '<<uCrsCompServMarcacao.tipoCompart>>'
				Where
					servcompmrstamp = '<<uCrsCompServMarcacao.servcompmrstamp>>'
			END
			
		ENDTEXT

*!*	_cliptext = lcSQL
*!*	MESSAGEBOX(lcSQL)
			
		IF !uf_gerais_actGrelha("", "",lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A INSERIR UMa COMPARTICIPA��O  DE MARCA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	
	ENDSCAN 
	
ENDFUNC 



**
Function uf_marcacoes_gravaRecursos

	Select ucrsMarcacoesDia
	lcMrStamp = ucrsMarcacoesDia.mrstamp

	** Apaga os servi�os que j� n�o existem
	TEXT TO lcSQL NOSHOW TEXTMERGE
		DELETE FROM marcacoesRecurso WHERE mrstamp = '<<lcMrStamp>>'
	ENDTEXT 
	IF !uf_gerais_actGrelha("", "",lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A INSERIR SERVI�O DE MARCA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	

	
	Select uCrsRecursosServMarcacao
	GO Top
	SCAN
	
	
		TEXT TO lcSQL NOSHOW TEXTMERGE
						
				insert into marcacoesRecurso (
					id
					,mrstamp
					,nome
					,no
					,tipo
					,ref
				) values(
					(select ISNULL(MAX(id),0)+1 from marcacoesRecurso)
					,'<<ALLTRIM(uCrsRecursosServMarcacao.mrstamp)>>'
					,'<<ALLTRIM(uCrsRecursosServMarcacao.nome)>>'
					,<<uCrsRecursosServMarcacao.no>>
					,'<<ALLTRIM(uCrsRecursosServMarcacao.tipo)>>'	
					,'<<ALLTRIM(uCrsRecursosServMarcacao.ref)>>'					
				)
		
		ENDTEXT
*!*	_CLIPTEXT = lcSQL
*!*	MESSAGEBOX(lcSQL)				
		IF !uf_gerais_actGrelha("", "",lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A INSERIR RECURSOS DA MARCA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	
	ENDSCAN 
	
	
	
ENDFUNC 




**Criar nova Marca��o URGENTE
FUNCTION uf_marcacoes_novo
	
	**
	IF !uf_gerais_validaPermUser(m.ch_userno, m.ch_grupo, 'Marca��es - Introduzir')
		uf_perguntalt_chama("O seu perfil n�o permite introduzir marca��es.", "OK", "", 64)
		RETURN	.f.
	ENDIF
	
	PUBLIC myMarcacoesIntroducao, myMarcacoesAlteracao
	LOCAL lcStamp 
	
	IF myMarcacoesIntroducao==.f. AND myMarcacoesAlteracao ==.f.

		** Coloca-se no dia actual
		uf_marcacoes_AplicaDia(LEFT(uf_gerais_getdate((datetime()+(difhoraria*3600)),"DATA"),2))
		**uf_marcacoes_AplicaDia(LEFT(uf_gerais_getdate(DATE(),"DATA"),2))
		
		marcacoes.detalhesVisiveis = .t.
		uf_marcacoes_expandeDetalhe()
		
		
		myMarcacoesIntroducao = .t. 
		
		SET HOURS TO 24
		Atime=ttoc(datetime()+(difhoraria*3600),2)
		Astr=left(Atime,8)

		lcStamp = uf_gerais_stamp()

		Select uCrsMarcacoesDia
		APPEND BLANK
		Replace uCrsMarcacoesDia.mrstamp 	WITH lcStamp 
		Replace uCrsMarcacoesDia.data 		WITH datetime()+(difhoraria*3600)
		Replace uCrsMarcacoesDia.dataFim 	WITH datetime()+(difhoraria*3600)
		**Replace uCrsMarcacoesDia.data 		WITH DATE()
		**Replace uCrsMarcacoesDia.dataFim 	WITH DATE()
		Replace uCrsMarcacoesDia.hinicio 	WITH left(Astr,5)
		Replace uCrsMarcacoesDia.hfim 		WITH left(Astr,5)
		**Replace uCrsMarcacoesDia.hinicio 	WITH left(Time(),5)
		**Replace uCrsMarcacoesDia.hfim 		WITH left(Time(),5)
	 	Replace ucrsMarcacoesDia.estado 	WITH "PRESENTE"
	 	Replace ucrsMarcacoesDia.nivelUrgencia 	WITH "URGENTE"
 		Replace ucrsMarcacoesDia.site 		WITH mysite
 	
 	
		MARCACOES.gridPesq.refresh
	
		SELECT uCrsServMarcacao
		GO Top
		SCAN
			DELETE 
		ENDSCAN 
		
		SELECT uCrsCompServMarcacao
		GO Top
		SCAN
			DELETE 
		ENDSCAN 
		
		SELECT uCrsRecursosServMarcacao
		GO Top
		SCAN
			DELETE 
		ENDSCAN 
	
	
		MARCACOES.Container1.ContainerDetalhes.Pageframe1.activePage = 1
		MARCACOES.Container1.ContainerDetalhes.Pageframe1.page1.refresh
		MARCACOES.Container1.ContainerDetalhes.Pageframe1.page2.refresh
		MARCACOES.Container1.ContainerDetalhes.Pageframe1.page3.refresh
		
		Select uCrsMarcacoesDia
		uf_marcacoes_alternaMenu()			

	ENDIF
ENDFUNC


**
FUNCTION uf_marcacoes_DiaNome
	LPARAMETERS lcData
	
	**Marca o Primeiro dia do Mes
	DO CASE 
		*2a
		CASE CDOW(lcData) == "Monday"
			RETURN "SEGUNDA-FEIRA"
		*3a
		CASE CDOW(lcData) == "Tuesday"
			RETURN "TER�A-FEIRA"
		*4a
		CASE CDOW(lcData) == "Wednesday"
			RETURN "QUARTA-FEIRA"
		*5a
		CASE CDOW(lcData) == "Thursday"
			RETURN "QUINTA-FEIRA"
		*6a
		CASE CDOW(lcData) == "Friday"
			RETURN "SEXTA-FEIRA"				
		*Sabado
		CASE CDOW(lcData) == "Saturday"
			RETURN "S�BADO"
		*Domingo
		CASE CDOW(lcData) == "Sunday"
			RETURN "DOMINGO"
		OTHERWISE
			RETURN ''
			**
	ENDCASE
ENDFUNC


**
FUNCTION uf_marcacoes_controlaTamanhoLabel
	LOCAL lcObj, lcNAlertas
	
	FOR j = 1 TO 42
		lcObj = "MARCACOES.d" + ALLTRIM(STR(j))

		**Tamanho Label
		IF &lcObj..height > 20
			&lcObj..Label1.Fontsize = 10
		ELSE
			&lcObj..Label1.Fontsize = 8		
		ENDIF
	ENDFOR
ENDFUNC


**
FUNCTION uf_marcacoes_alertasSelecionados
	LOCAL lcNAlertas 
	
	lcNAlertas = 0
	
	IF marcacoes.adisponivel == .t.
		lcNAlertas = lcNAlertas + 1
	ENDIF
	IF marcacoes.amarcado == .t.
		lcNAlertas = lcNAlertas + 1
	ENDIF
	IF marcacoes.aatrasado == .t.
		lcNAlertas = lcNAlertas + 1
	ENDIF
	IF marcacoes.apresente == .t.
		lcNAlertas = lcNAlertas + 1
	ENDIF
	IF marcacoes.aefetuado == .t.
		lcNAlertas = lcNAlertas + 1
	ENDIF
	IF marcacoes.afaturado == .t.
		lcNAlertas = lcNAlertas + 1
	ENDIF
	IF marcacoes.afaltou == .t.
		lcNAlertas = lcNAlertas + 1
	ENDIF
	IF marcacoes.acancelado == .t.
		lcNAlertas = lcNAlertas + 1
	ENDIF
	
	RETURN lcNAlertas 
ENDFUNC


**
FUNCTION uf_marcacoes_actualizaAlertaUtilizador
	LPARAMETERS lcAlerta, lcValor
	LOCAL lcSQL,lcStamp 
	
	lcStamp = uf_gerais_stamp()

	**Se ainda n�o existir um perfil para o utilizador cria
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_marcacoes_criarPerfilAlertas  '<<lcStamp>>', <<ch_userno>>
	ENDTEXT
	If !uf_gerais_actGrelha("", "", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar os perfis de alertas utilizador. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	
	DO CASE 
		CASE lcAlerta == "Pedido"
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				UPDATE marcacoes_alertas SET pedido = <<IIF(lcValor,1,0)>> WHERE userno = <<ch_userno>>  
			ENDTEXT
		CASE lcAlerta == "Marcado"
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				UPDATE marcacoes_alertas SET marcado = <<IIF(lcValor,1,0)>> WHERE userno = <<ch_userno>>  
			ENDTEXT
		CASE lcAlerta == "Atrasado"
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				UPDATE marcacoes_alertas SET atrasado = <<IIF(lcValor,1,0)>> WHERE userno = <<ch_userno>>  
			ENDTEXT
		CASE lcAlerta == "Cancelado"
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				UPDATE marcacoes_alertas SET cancelado = <<IIF(lcValor,1,0)>>  WHERE userno = <<ch_userno>>  
			ENDTEXT
		CASE lcAlerta == "Presente"
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				UPDATE marcacoes_alertas SET presente = <<IIF(lcValor,1,0)>> WHERE userno = <<ch_userno>>  
			ENDTEXT
		CASE lcAlerta == "Tratamento"
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				UPDATE marcacoes_alertas SET tratamento = <<IIF(lcValor,1,0)>> WHERE userno = <<ch_userno>>  
			ENDTEXT
		CASE lcAlerta == "Efetuado"
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				UPDATE marcacoes_alertas SET efetuado = <<IIF(lcValor,1,0)>>  WHERE userno = <<ch_userno>>  
			ENDTEXT
		CASE lcAlerta == "Emergente"
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				UPDATE marcacoes_alertas SET emergente = <<IIF(lcValor,1,0)>>  WHERE userno = <<ch_userno>>  
			ENDTEXT
		CASE lcAlerta == "Mto. Urgente"
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				UPDATE marcacoes_alertas SET mtourgente = <<IIF(lcValor,1,0)>>  WHERE userno = <<ch_userno>>  
			ENDTEXT
		CASE lcAlerta == "Urgente"
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				UPDATE marcacoes_alertas SET urgente = <<IIF(lcValor,1,0)>> WHERE userno = <<ch_userno>>  
			ENDTEXT
		CASE lcAlerta == "Pouco Urgente"
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				UPDATE marcacoes_alertas SET poucourgente = <<IIF(lcValor,1,0)>> WHERE userno = <<ch_userno>>
			ENDTEXT
		CASE lcAlerta == "NaoUrgente"
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				UPDATE marcacoes_alertas SET naourgente = <<IIF(lcValor,1,0)>> WHERE userno = <<ch_userno>>
			ENDTEXT
				
		OTHERWISE
			**
	ENDCASE
	
	If !uf_gerais_actGrelha("", "", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar os perfis de alertas utilizador. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	
	**
	uf_marcacoes_actualiza()
ENDFUNC



**
FUNCTION uf_marcacoes_agruparHoraInicio
	LPARAMETERS lcCursor
	SELECT &lcCursor
	GO TOP
	SCAN FOR estado = "DISPONIVEL"
		REPLACE &lcCursor..hinicio WITH LEFT(&lcCursor..hinicio,3) + "00"
		REPLACE &lcCursor..hfim WITH LEFT(&lcCursor..hinicio,3) + "59"	
	ENDSCAN
	SELECT &lcCursor
	GO TOP
ENDFUNC




FUNCTION uf_marcacoes_Colunas
	LPARAMETERS lcDesign
	
	With MARCACOES.gridPesq
		IF lcDesign == .t.
			FOR i=1 TO .columnCount
				IF .Columns(i).name == "design"
					.Columns(i).visible = .t.
				ENDIF
				IF .Columns(i).name == "recurso"
					.Columns(i).visible = .f.
				ENDIF
			ENDFOR
		ELSE
			FOR i=1 TO .columnCount
				IF .Columns(i).name == "design"
					.Columns(i).visible = .f.
				ENDIF
				IF .Columns(i).name == "recurso"
					.Columns(i).visible = .t.
				ENDIF
			
			ENDFOR
		ENDIF
	ENDWITH
ENDFUNC



**
FUNCTION uf_marcacoes_AfterRowColChange
	
	** Caso esteja em modo de edi��o, n�o refresca esta informa��o
	LOCAL lcMrstamp
	STORE '' TO lcMrstamp

	Select ucrsMarcacoesDia
	
	lcSQL = ""
	**Cursor de Servi�os Associados � marca��o
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_Marcacoes_ServMarc '<<ucrsMarcacoesDia.mrstamp>>'
	ENDTEXT
	If !uf_gerais_actGrelha("MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page2.GridConsumos", "uCrsServMarcacao", lcSql)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR OS SERVI�OS DE MARCA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF		
	
	
	lcSQL = ""
	**Cursor de Comparticipa��es Associados � marca��o
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_Marcacoes_CompServMarc '<<ucrsMarcacoesDia.mrstamp>>'
	ENDTEXT
	If !uf_gerais_actGrelha("MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page2.GridComp", "uCrsCompServMarcacao", lcSql)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR A COMPARTICIPA��O DOS SERVI�OS DE MARCA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	lcSQL = ""
	**Cursor de Recursos Associados � marca��o
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_Marcacoes_RecursoServMarc '<<ucrsMarcacoesDia.mrstamp>>'
	ENDTEXT
	If !uf_gerais_actGrelha("MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page2.GridRecursos", "uCrsRecursosServMarcacao", lcSql)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR OS RECURSOS ASSOCIADOS � MARCA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	

	WITH MARCACOES.container1.ContainerDetalhes.pageFrame1
		
		.page1.refresh
		.page1.Registoedicao1.refresh
		.page2.refresh
		IF UPPER(ALLTRIM(.Page1.estado.value)) == "CANCELADO" 
			.Page1.motivoCancelamento.visible =  .t.
			.Page1.LabelMotivo.visible =  .t.
		ELSE
			.Page1.motivoCancelamento.visible =  .f.
			.Page1.LabelMotivo.visible =  .f.
		ENDIF 

	ENDWITH 
ENDFUNC


**
FUNCTION uf_marcacoes_serviconovo
	
	IF myMarcacoesIntroducao == .t. OR myMarcacoesAlteracao == .t.
	
		SELECT ucrsMarcacoesDia
		IF EMPTY(ALLTRIM(ucrsMarcacoesDia.nome))
			uf_perguntalt_chama("Selecione o utente antes de adicionar servi�os � marca��o.", "", "OK", 64)
			RETURN .f.
		ENDIF 
		
		uf_pesqrefsagrupadas_chama(.t.,"MARCACOES",.t.)
	ENDIF
	

ENDFUNC 


**
FUNCTION uf_marcacoes_servicoapaga

	LOCAL lcRef, lcPos 
	lcRef = ""
	
	IF myMarcacoesIntroducao == .f. AND myMarcacoesAlteracao == .f.
		RETURN .f.
	ENDIF 
	
	
	
	select uCrsServMarcacao
	IF uCrsServMarcacao.fu == .t. OR uCrsServMarcacao.fe == .t. OR uCrsServMarcacao.honorario == .t.
		
		uf_perguntalt_chama("Este servi�o j� foi processado. N�o � possivel eliminar.", "", "OK", 64)
		Select uCrsServMarcacao
		Replace uCrsServMarcacao.sel WITH .f.
		MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page2.GridConsumos.refresh
		
		RETURN .f.	
	ENDIF 
	
	select uCrsServMarcacao
	Delete


	select uCrsServMarcacao
	TRY
		select uCrsServMarcacao
		SKIP -1
	CATCH
		**
	ENDTRY

	MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page2.GridConsumos.Refresh

ENDFUNC 


**
FUNCTION uf_marcacoes_recursonovo
	
	IF myMarcacoesIntroducao == .t. OR myMarcacoesAlteracao == .t.
		uf_PESQSERIESRECURSOS_Chama('MARCACOES')
	ENDIF

ENDFUNC 


**
FUNCTION uf_marcacoes_recursoapaga

	LOCAL lcRef, lcPos 
	lcRef = ""
	
	IF myMarcacoesIntroducao == .f. AND myMarcacoesAlteracao == .f.
		RETURN .f.
	ENDIF 
	
		
	select uCrsRecursosServMarcacao
	Delete


	select uCrsRecursosServMarcacao
	TRY
		select uCrsRecursosServMarcacao
		SKIP -1
	CATCH
		**
	ENDTRY

	MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page2.GridRecursos.refresh

ENDFUNC 


**
FUNCTION uf_marcacoes_servicoCompnovo
	
	IF myMarcacoesIntroducao == .t. OR myMarcacoesAlteracao == .t.
		
		LOCAL lcservcompmrstamp, lcMaxOrdemServMarc
		LOCAL lcSQL
		
		Select UcrsMarcacoesDia
		lcMrstamp = UcrsMarcacoesDia.mrstamp
		
		SELECT uCrsServMarcacao 
		lcServmrstamp = uCrsServMarcacao.Servmrstamp
		lcRef = uCrsServMarcacao.ref
		lcDesign = uCrsServMarcacao.design
		lcPVP = uCrsServMarcacao.PVP
		
		IF EMPTY(lcRef)
			SELECT uCrsServMarcacao 
			GO Top
			lcServmrstamp = uCrsServMarcacao.Servmrstamp
			lcRef = uCrsServMarcacao.ref
			lcDesign = uCrsServMarcacao.design
			lcPVP = uCrsServMarcacao.PVP
		ENDIF 		
		
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			Select 
				B_entidadesUtentes.enome as Entidade 
				,B_entidadesUtentes.eno as Nr
				,B_entidadesUtentes.eestab
			from 
				B_entidadesUtentes 
			Where
				B_entidadesUtentes.utstamp = '<<ALLTRIM(uCrsMarcacoesDia.Utstamp)>>'
		ENDTEXT
		If !uf_gerais_actGrelha("", "ucrsTempEntidadesUtente", lcSql)
			uf_perguntalt_chama("N�o foi possivel encontrar regitos.", "", "OK", 16)
			RETURN .f.
		ENDIF
		PUBLIC myEntidadeCompNome
		myEntidadeCompNome = ""
		
		uf_valorescombo_chama("myEntidadeCompNome", 0, "ucrsTempEntidadesUtente", 2, "Entidade", "Entidade, Nr", .f.,.f.,.t.,.t.)

		SELECT ucrsTempEntidadesUtente
		LOCATE FOR ALLTRIM(ucrsTempEntidadesUtente.Entidade) == ALLTRIM(myEntidadeCompNome)
		IF FOUND()
		
			Select uCrsCompServMarcacao
			CALCULATE MAX(ordem) TO lcMaxOrdemServMarc
		
			SELECT uCrsCompServMarcacao
			APPEND BLANK
			REPLACE uCrsCompServMarcacao.entidadeno WITH ucrsTempEntidadesUtente.Nr
			REPLACE uCrsCompServMarcacao.entidadeestab WITH ucrsTempEntidadesUtente.eestab
			REPLACE uCrsCompServMarcacao.entidade WITH myEntidadeCompNome
				
			
			lcservcompmrstamp  = uf_gerais_stamp()
			
			**
			Select uCrsCompServMarcacao
		
			Replace uCrsCompServMarcacao.servcompmrstamp WITH lcservcompmrstamp  
			Replace uCrsCompServMarcacao.mrstamp WITH UcrsMarcacoesDia.mrstamp
			Replace uCrsCompServMarcacao.servmrstamp WITH lcservmrstamp
			Replace uCrsCompServMarcacao.ref WITH lcref
			Replace uCrsCompServMarcacao.design  WITH lcdesign
			Replace uCrsCompServMarcacao.pvp WITH lcpvp
			Replace uCrsCompServMarcacao.Total WITH lcpvp
			Replace uCrsCompServMarcacao.valor WITH 0
			Replace uCrsCompServMarcacao.ordem WITH IIF(ISNULL(lcMaxOrdemServMarc),1,lcMaxOrdemServMarc+1)

			MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page2.GridComp.Refresh
		ENDIF
	ENDIF 

ENDFUNC 


**
FUNCTION uf_marcacoes_servicoCompApaga

	lcRef = ""
	
	IF myMarcacoesIntroducao == .f. AND myMarcacoesAlteracao == .f.
		RETURN .f.
	ENDIF 
	
	
	select uCrsCompServMarcacao
	lcServmrstampAtual = uCrsCompServMarcacao.Servmrstamp
	
	select uCrsCompServMarcacao
	Delete


	select uCrsCompServMarcacao
	TRY
		SKIP -1
	CATCH
		**
	ENDTRY

	MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page2.GridComp.Refresh

	**
	Select IIF(ISNULL(SUM(uCrsCompServMarcacao.valor)),0.00,SUM(uCrsCompServMarcacao.valor)) as totalCompart;
	FROM uCrsCompServMarcacao WHERE servmrstamp = lcServmrstampAtual INTO CURSOR uCrsCompServMarcacaoAux READWRITE
	
	Select uCrsCompServMarcacaoAux 
	lcValorCompartTotal = uCrsCompServMarcacaoAux.totalCompart 

	**
	UPDATE uCrsServMarcacao SET compart = lcValorCompartTotal, Total = pvp - lcValorCompartTotal WHERE servmrstamp = lcServmrstampAtual 


	MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page2.GridComp.Refresh
	MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page2.GridConsumos.Refresh
	
	MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page2.GridComp.ref.setFocus
ENDFUNC 


**
FUNCTION uf_marcacoes_mesSeguinte
	LOCAL lcMesDesc, lcMes, lcAno
	
	SELECT ucrsMarcacoesCab
	lcMes = ucrsMarcacoesCab.mes
	lcMesExt = ucrsMarcacoesCab.mesDesc
	lcAno = ucrsMarcacoesCab.ano
	

	IF lcMes != 12
		lcMes = lcMes + 1
	ELSE
		lcMes = 1
		lcAno = lcAno + 1
	ENDIF
	
	
	SELECT ucrsMarcacoesCab
	Replace ucrsMarcacoesCab.mes WITH lcMes
	Replace ucrsMarcacoesCab.mesDesc WITH uf_gerais_getMonthToMonthEx(lcMes)
	Replace ucrsMarcacoesCab.ano WITH lcAno
	
	
	marcacoes.pageframedata.page1.mes.refresh	
	marcacoes.pageframedata.page1.ano.refresh
		
	**
	uf_marcacoes_PreencheCalendario()
	uf_marcacoes_actualiza()
ENDFUNC 

**
FUNCTION uf_marcacoes_mesAnterior

	LOCAL lcMesDesc, lcMes, lcAno
	
	SELECT ucrsMarcacoesCab
	lcMes = ucrsMarcacoesCab.mes
	lcMesExt = ucrsMarcacoesCab.mesDesc
	lcAno = ucrsMarcacoesCab.ano
	

	IF lcMes != 1
		lcMes = lcMes - 1
	ELSE
		lcMes = 12
		lcAno = lcAno - 1
	ENDIF
	
	
	SELECT ucrsMarcacoesCab
	Replace ucrsMarcacoesCab.mes WITH lcMes
	Replace ucrsMarcacoesCab.mesDesc WITH uf_gerais_getMonthToMonthEx(lcMes)
	Replace ucrsMarcacoesCab.ano WITH lcAno
	
	
	marcacoes.pageframedata.page1.mes.refresh	
	marcacoes.pageframedata.page1.ano.refresh
		
	**
	uf_marcacoes_PreencheCalendario()
	uf_marcacoes_actualiza()
	
ENDFUNC 

**
FUNCTION uf_marcacoes_selServico

	IF !EMPTY(uCrsServMarcacao.fu)
		uf_perguntalt_chama("Este servi�o j� foi faturado.", "", "OK", 64)
		Select uCrsServMarcacao
		Replace uCrsServMarcacao.sel WITH .f.
		MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page2.GridConsumos.refresh
		RETURN .f.
	ENDIF 
	

	IF myMarcacoesIntroducao == .t. OR myMarcacoesAlteracao == .t.
		uf_perguntalt_chama("A selec��o de Servi�os deve ser feita ap�s a edi��o da marca��o.", "", "OK", 64)
		Select uCrsServMarcacao
		Replace uCrsServMarcacao.sel WITH .f.
		MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page2.GridConsumos.refresh
		RETURN .f.
	ENDIF 

	myMarcacoesFtUtentes = .t.
	uf_marcacoes_alternaMenu(.t.)

	
ENDFUNC 




**
FUNCTION uf_marcacoes_sair

	IF  myMarcacoesFtUtentes == .t.
		Select uCrsServMarcacao
		GO Top
		SCAN 
			Replace uCrsServMarcacao.sel WITH .f.
		ENDSCAN 
		
		myMarcacoesFtUtentes = .f.
		uf_marcacoes_alternaMenu()
		RETURN .f.
	ENDIF 

	**
	IF myMarcacoesIntroducao == .t. OR myMarcacoesAlteracao == .t.
		IF uf_perguntalt_chama("Pretende cancelar as altera��es?", "Sim", "N�o", 32)
			
			TABLEREVERT(.t., "ucrsMarcacoesDia")
			
			myMarcacoesIntroducao = .f.
			myMarcacoesAlteracao = .f.
			myMarcacoesAlteracaoFaturada = .f.
			
			uf_marcacoes_alternaMenu()
		ENDIF
	ELSE
		uf_marcacoes_exit()
	ENDIF
ENDFUNC


FUNCTION uf_marcacoes_novaSerieServicos
	uf_pesqrefsagrupadas_chama()
ENDFUNC


FUNCTION uf_marcacoes_novaSerieAtividades
	uf_PLANEAMENTOMR_chama()
	Select ucrsSeriesServ
	Replace ucrsSeriesServ.tipo with "Atividades"
ENDFUNC



**
FUNCTION uf_marcacoes_ServDestino
	
	SELECT uCrsServMarcacao
	IF EMPTY(uCrsServMarcacao.fu) AND EMPTY(uCrsServMarcacao.fe)
		uf_perguntalt_chama("O servi�o ainda n�o foi faturado.","OK","",16)
	ELSE
		uf_OrigensDestinos_Chama('MARCACOESSERV')
	ENDIF
ENDFUNC




**
FUNCTION uf_marcacoes_calculaValorCompart
	LOCAL lcServmrstamp, lcValor,lcValorJaCompart, lcOrdem 
	STORE 0  TO lcValor,lcValorJaCompart, lcOrdem 
	
	IF myMarcacoesIntroducao == .f. AND myMarcacoesAlteracao == .f.
		RETURN .f.
	ENDIF

	Select uCrsCompServMarcacao
	lcServmrstampAtual = uCrsCompServMarcacao.servmrstamp

		
	Select uCrsCompServMarcacao
	GO Top
	SCAN 
			
		Select uCrsCompServMarcacao
		lcServmrstamp = uCrsCompServMarcacao.servmrstamp
		lcOrdem = uCrsCompServMarcacao.ordem
		
		IF USED("uCrsCompServMarcacaoAux")
			fecha("uCrsCompServMarcacaoAux")
		ENDIF 
		
		Select SUM(uCrsCompServMarcacao.valor) as ValorJaCompart FROM uCrsCompServMarcacao WHERE servmrstamp = lcServmrstamp AND ordem < lcOrdem INTO CURSOR uCrsCompServMarcacaoAux READWRITE

		Select uCrsCompServMarcacaoAux 
		lcValorJaCompart = uCrsCompServMarcacaoAux.ValorJaCompart 
		lcValor = uCrsCompServMarcacao.pvp - IIF(ISNULL(lcValorJaCompart),0,lcValorJaCompart)

		Select uCrsCompServMarcacao
		DO CASE
			CASE UPPER(ALLTRIM(uCrsCompServMarcacao.tipoCompart)) == UPPER(ALLTRIM("Valor")) && Comparticipa��o em valor
				
				Replace uCrsCompServMarcacao.valor WITH lcValor - uCrsCompServMarcacao.compart
		
			CASE UPPER(ALLTRIM(uCrsCompServMarcacao.tipoCompart)) == UPPER(ALLTRIM("Percent")) && Comparticipa��o em percentagem
				
				Replace uCrsCompServMarcacao.valor WITH lcValor - ((lcValor * uCrsCompServMarcacao.compart)/100)
		
			OTHERWISE
				**
		ENDCASE
		
	ENDSCAN 
	
	IF USED("uCrsCompServMarcacaoAux")
		fecha("uCrsCompServMarcacaoAux")
	ENDIF 
	
	

	**
	Select SUM(uCrsCompServMarcacao.valor) as totalCompart FROM uCrsCompServMarcacao WHERE servmrstamp = lcServmrstamp INTO CURSOR uCrsCompServMarcacaoAux READWRITE
	Select uCrsCompServMarcacaoAux 
	lcValorCompartTotal = uCrsCompServMarcacaoAux.totalCompart 

	**
	UPDATE uCrsServMarcacao SET compart = lcValorCompartTotal, Total = pvp - lcValorCompartTotal WHERE servmrstamp = lcServmrstamp	
	
	**
	IF USED("uCrsCompServMarcacaoAux")
		fecha("uCrsCompServMarcacaoAux")
	ENDIF 
	
	&& Posicao
	Select uCrsCompServMarcacao
	LOCATE  FOR  servmrstamp == lcServmrstampAtual 
	
	MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page2.GridComp.Refresh
	MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page2.GridConsumos.refresh	
	
ENDFUNC 


**
FUNCTION uf_marcacoes_ReagendarRecursos

	Select UcrsMarcacoesDia
	
	TEXT TO lcSQL NOSHOW TEXTMERGE
		select seriestamp,tipo from b_series where serieno = <<UcrsMarcacoesDia.serieno>>
	ENDTEXT
	If !uf_gerais_actGrelha("", "UcrsDadosSerieReag", lcSql)
		RETURN .f.
	ENDIF
	
	IF RECCOUNT("UcrsDadosSerieReag")>0
		**	
		Select UcrsDadosSerieReag
		uf_SERIESSERV_chama(UcrsDadosSerieReag.seriestamp,UcrsDadosSerieReag.tipo, .t.,UcrsMarcacoesDia.mrstamp) && 3 parametro Reagendar
	ENDIF 
		
	**
	IF USED("UcrsDadosSerieReag")
		fecha("UcrsDadosSerieReag")
	ENDIF 

	
ENDFUNC 


** 
FUNCTION UF_MARCACOES_activateVistaAgenda
	LOCAL i, LcCampoAdicionado, lcAno, lcMes, lcSQL
	
	SELECT ucrsMarcacoesCab
	lcAno = ucrsMarcacoesCab.ano
	lcMes = ucrsMarcacoesCab.mes
	
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_marcacoes_listaAgenda <<lcAno>>,<<lcMes>>
	ENDTEXT
	
	If !uf_gerais_actGrelha("MARCACOES.gridPesq", "UcrsMarcacoesDia", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar os periodos de Marca��o. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	
	uf_marcacoes_aplicaFiltros("ucrsMarcacoesDia")

	MARCACOES.gridPesq.left = MARCACOES.Container1.left
	MARCACOES.gridPesq.width = MARCACOES.Container1.width - 10

	LcCampoAdicionado = .f.
	FOR i=1 TO MARCACOES.gridPesq.columnCount
		IF MARCACOES.gridPesq.Columns(MARCACOES.gridPesq.ColumnCount).Name == "data"
			LcCampoAdicionado = .t.
		ENDIF
	ENDFOR
	
	IF LcCampoAdicionado == .f.
		WITH MARCACOES.gridPesq
			.AddColumn(1)   && Insert column at left.
			.Columns(MARCACOES.gridPesq.ColumnCount).Name = "data"
			.data.header1.FontName = "Verdana"
			.data.FontName = "Verdana"
			.data.Fontsize = 10
			.data.ReadOnly = .t.
			.data.text1.ReadOnly = .t.
			.data.header1.caption = "Data"
			.data.header1.fontBold = .t.
			.data.header1.Alignment = 2
			.data.text1.BorderStyle =  0
			.data.width = 80
			.data.ControlSource = "uf_gerais_getdate(ucrsMarcacoesDia.data)"
		ENDWITH
	ENDIF
		
	MARCACOES.gridPesq.setFocus
	MARCACOES.gridPesq.refresh	
	MARCACOES.pageframedata.page2.refresh
	

ENDFUNC 


** 
FUNCTION UF_MARCACOES_activateVistaCalendario
	
	LcCampoAdicionado = .f.
	FOR i=1 TO MARCACOES.gridPesq.columnCount
		IF MARCACOES.gridPesq.Columns(MARCACOES.gridPesq.ColumnCount).Name == "data"
			LcCampoAdicionado = .t.
		ENDIF
	ENDFOR
	
	IF LcCampoAdicionado == .t.
		MARCACOES.gridPesq.deleteColumn(1)
	ENDIF
	
	
	MARCACOES.gridPesq.left = 331
	MARCACOES.gridPesq.width = MARCACOES.Container1.width - 331
	
	
	** Aplica dia actual	
	SELECT ucrsMarcacoesCab
	uf_marcacoes_AplicaDia(LEFT(uf_gerais_getdate(ucrsMarcacoesCab.data,"DATA"),2))
ENDFUNC 


**
FUNCTION uf_marcacoes_exit
	**
	RELEASE myMarcacoesIntroducao
	RELEASE myMarcacoesAlteracao
	RELEASE myMarcacoesFtUtentes
	RELEASE myMarcacoesAlteracaoFaturada
	
		
	MARCACOES.hide
	MARCACOES.release
ENDFUNC

**
FUNCTION uf_marcacoes_refresh

	IF EMPTY(myMarcacoesIntroducao) AND  EMPTY(myMarcacoesAlteracao) AND EMPTY(myMarcacoesFtUtentes)

		MARCACOES.LockScreen = .t.
		**
		**uf_marcacoes_actualiza()
		
		SELECT UcrsMarcacoesDia
		lcMrStamp = UcrsMarcacoesDia.mrstamp
		
		SELECT uCrsServMarcacao 
		lcServMrStamp = uCrsServMarcacao.servmrstamp
		
		**
		uf_marcacoes_PreencheCalendario()
		
		SELECT ucrsMarcacoesCab
		
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_marcacoes_listaAgenda <<YEAR(ucrsMarcacoesCab.data)>>,<<MONTH(ucrsMarcacoesCab.data)>>
		ENDTEXT
		
		If !uf_gerais_actGrelha("", "UcrsMarcacoesDiaAux", lcSql)
			uf_perguntalt_chama("N�o foi poss�vel verificar os periodos de Marca��o. Contacte o suporte.", "", "OK", 32)
			RETURN .f.
		ENDIF
		
		
		
		** Atualiza Estados
		UPDATE UcrsMarcacoesDia From UcrsMarcacoesDia inner join UcrsMarcacoesDiaAux on UcrsMarcacoesDia.mrstamp = UcrsMarcacoesDiaAux.mrstamp SET UcrsMarcacoesDia.estado = UcrsMarcacoesDiaAux.estado
		
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_marcacoes_ServicosMes <<YEAR(ucrsMarcacoesCab.data)>>,<<MONTH(ucrsMarcacoesCab.data)>>
		ENDTEXT
		
		If !uf_gerais_actGrelha("", "uCrsServMarcacaoAux", lcSql)
			RETURN .f.
		ENDIF
		
		** Atualiza Estados
		UPDATE uCrsServMarcacao From uCrsServMarcacao inner join uCrsServMarcacaoAux on uCrsServMarcacao.servmrstamp = uCrsServMarcacaoAux.servmrstamp SET uCrsServMarcacao.fu = uCrsServMarcacaoAux.fu, uCrsServMarcacao.fe = uCrsServMarcacaoAux.fe, uCrsServMarcacao.honorario= uCrsServMarcacaoAux.honorario
		

		**
		SELECT uCrsServMarcacao 
		LOCATE FOR uCrsServMarcacao.servmrstamp = lcServMrStamp
		**
		SELECT UcrsMarcacoesDia
		LOCATE FOR UcrsMarcacoesDia.mrstamp = lcMrStamp 
		
		
		i = 1 && Colocar cor de dia activo
		DO WHILE i <= 42

			lcObj = "MARCACOES.d" + ALLTRIM(STR(i))
			
			IF &lcObj..enabled 	== .t.
				IF &lcObj..label1.caption == marcacoes.dia
					&lcObj..borderColor = RGB(232,76,61) &&RGB(42,143,154)
					&lcObj..borderWidth = 2
				ELSE
					&lcObj..borderColor = RGB(255,255,255)
					&lcObj..borderWidth = 0
				ENDIF
			ENDIF 
			i= i +1
		ENDDO
		
		MARCACOES.LockScreen = .f.

	ENDIF 

ENDFUNC 



** 

** PVP foi alterado manualmente
FUNCTION uf_marcacoes_recalculaTotais
	LPARAMETERS lcCampo
	LOCAL lcSQL, lcPvp, lcTotal, lcCompart, lcServmrstamp   
	STORE 0 TO lcPvp, lcTotal, lcCompart
	STORE '' TO lcServmrstamp   
	
	SELECT uCrsCompServMarcacao
	
	SELECT uCrsCompServMarcacao
	COUNT TO lcRegistosCompServMarcacao

	IF lcRegistosCompServMarcacao == 0 && N�o existem entidades de comparticipa��o

		SELECT uCrsServMarcacao
		Replace uCrsServMarcacao.total WITH uCrsServMarcacao.qtt*uCrsServMarcacao.pvp
		Replace uCrsServMarcacao.compart WITH 0
		uf_perguntalt_chama("Para definir comparticipa��o deve associar pelo menos uma entidade � marca��o.", "", "OK", 64)
		return .f.
	ENDIF  
	
	DO CASE 
		CASE lcCampo == "QTT"
			SELECT uCrsServMarcacao
			Replace uCrsServMarcacao.total WITH IIF(uCrsServMarcacao.qtt*uCrsServMarcacao.pvp-uCrsServMarcacao.compart<0,0,uCrsServMarcacao.qtt*uCrsServMarcacao.pvp-uCrsServMarcacao.compart)
		CASE lcCampo == "PVP"
			SELECT uCrsServMarcacao
			Replace uCrsServMarcacao.total WITH IIF(uCrsServMarcacao.qtt*uCrsServMarcacao.pvp-uCrsServMarcacao.compart<0,0,uCrsServMarcacao.qtt*uCrsServMarcacao.pvp-uCrsServMarcacao.compart)
		CASE lcCampo == "COMP"
			SELECT uCrsServMarcacao
			Replace uCrsServMarcacao.total WITH IIF(uCrsServMarcacao.qtt*uCrsServMarcacao.pvp-uCrsServMarcacao.compart<0,0,uCrsServMarcacao.qtt*uCrsServMarcacao.pvp-uCrsServMarcacao.compart)
	ENDCASE 	


	** 
	LOCAL lcOrdem
	lcOrdem = 1
	
	SELECT uCrsCompServMarcacao 
	CALCULATE MIN(ordem) TO lcOrdem
	
	SELECT uCrsServMarcacao
	lcPvp = uCrsServMarcacao.pvp
	lcTotal = uCrsServMarcacao.total
	lcCompart = uCrsServMarcacao.Compart 
	lcServmrstamp = uCrsServMarcacao.servmrstamp
	
	**
	UPDATE uCrsCompServMarcacao;
	SET uCrsCompServMarcacao.tipoCompart = "Valor";
		,uCrsCompServMarcacao.PVP = lcPvp ;
		,uCrsCompServMarcacao.Valor = lcTotal ;
		,uCrsCompServMarcacao.Compart = lcCompart  ;
	WHERE ;
		uCrsCompServMarcacao.servmrstamp = uCrsServMarcacao.servmrstamp ;
		AND uCrsCompServMarcacao.ORDEM = lcOrdem
		
	MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page2.GridComp.Refresh
	MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page2.GridConsumos.refresh
		
ENDFUNC 


**
Function UF_MARCACOES_stocks_chama
	SELECT uCrsServMarcacao
	uf_stocks_chama(uCrsServMarcacao.ref)
ENDFUNC 



FUNCTION uf_marcacoes_updateEstado

	SELECT UcrsMarcacoesDia
	IF EMPTY(ALLTRIM(UcrsMarcacoesDia.estado))
		RETURN .f.
	ENDIF 
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		UPDATE 
			marcacoes 
		SET 
			marcacoes.estado = '<<UPPER(ALLTRIM(UcrsMarcacoesDia.estado))>>' 
			,marcacoes.nivelUrgencia = 
				Case 
					when '<<UPPER(ALLTRIM(UcrsMarcacoesDia.estado))>>' = 'PRESENTE' or '<<UPPER(ALLTRIM(UcrsMarcacoesDia.estado))>>' = 'TRATAMENTO' then 'N�O URGENTE'
					else ''
				end	
			,marcacoes.motivoCancelamento = '<<UPPER(ALLTRIM(UcrsMarcacoesDia.motivoCancelamento))>>' 
		WHERE 
			marcacoes.mrstamp = '<<UPPER(ALLTRIM(UcrsMarcacoesDia.mrstamp))>>'
	ENDTEXT
	
	If !uf_gerais_actGrelha("", "", lcSql)
		uf_perguntalt_chama("N�o foi possivel actualizar o estado da marca��o.", "", "OK", 16)
		RETURN .f.
	ENDIF

	SELECT UcrsMarcacoesDia
	DO CASE 
		
		CASE UPPER(ALLTRIM(UcrsMarcacoesDia.estado)) == 'PRESENTE'
			IF EMPTY(UcrsMarcacoesDia.nivelUrgencia)
	 			 REPLACE UcrsMarcacoesDia.nivelUrgencia WITH 'N�O URGENTE'
			ENDIF 
		CASE UPPER(ALLTRIM(UcrsMarcacoesDia.estado)) == 'TRATAMENTO'
			IF EMPTY(UcrsMarcacoesDia.nivelUrgencia)
	 			 REPLACE UcrsMarcacoesDia.nivelUrgencia WITH 'N�O URGENTE'
			ENDIF 
	ENDCASE

	MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.estado.Refresh
	MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.nivelUrgencia.Refresh
	MARCACOES.GridPesq.Refresh
ENDFUNC 



**
FUNCTION uf_marcacoes_updateNivelUrgencia
	
	SELECT UcrsMarcacoesDia
	IF EMPTY(ALLTRIM(UcrsMarcacoesDia.nivelUrgencia))
		RETURN .f.
	ENDIF 
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		UPDATE 
			marcacoes 
		SET 
			marcacoes.nivelUrgencia = '<<UPPER(ALLTRIM(UcrsMarcacoesDia.nivelUrgencia))>>'
		WHERE 
			marcacoes.mrstamp = '<<UPPER(ALLTRIM(UcrsMarcacoesDia.mrstamp))>>'
	ENDTEXT
	
	If !uf_gerais_actGrelha("", "", lcSql)
		uf_perguntalt_chama("N�o foi possivel actualizar o Nivel de Urg�ncia da marca��o.", "", "OK", 16)
		RETURN .f.
	ENDIF

	MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.estado.Refresh
	MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.nivelUrgencia.Refresh
	MARCACOES.GridPesq.Refresh
	
ENDFUNC 


FUNCTION uf_marcacoes_updateMotivoCancelamento

	SELECT UcrsMarcacoesDia
	IF EMPTY(ALLTRIM(UcrsMarcacoesDia.MotivoCancelamento))
		RETURN .f.
	ENDIF 
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		UPDATE 
			marcacoes 
		SET 
			marcacoes.MotivoCancelamento= '<<UPPER(ALLTRIM(UcrsMarcacoesDia.MotivoCancelamento))>>'
		WHERE 
			marcacoes.mrstamp = '<<UPPER(ALLTRIM(UcrsMarcacoesDia.mrstamp))>>'
	ENDTEXT
	
	If !uf_gerais_actGrelha("", "", lcSql)
		uf_perguntalt_chama("N�o foi possivel actualizar o Nivel de Urg�ncia da marca��o.", "", "OK", 16)
		RETURN .f.
	ENDIF

ENDFUNC 


*!*	**
*!*	Function UF_MARCACOES_estadoSeguinte
*!*		
*!*		SELECT UcrsMarcacoesDia
*!*		IF EMPTY(ALLTRIM(UcrsMarcacoesDia.estado))
*!*			RETURN .f.
*!*		ENDIF 
*!*		
*!*		lcSQL = ""
*!*		TEXT TO lcSQL TEXTMERGE NOSHOW
*!*			exec up_marcacoes_estadoSeguinte '<<UPPER(ALLTRIM(UcrsMarcacoesDia.estado))>>','<<UPPER(ALLTRIM(UcrsMarcacoesDia.mrstamp))>>'
*!*		ENDTEXT
*!*		If !uf_gerais_actGrelha("", "", lcSql)
*!*			uf_perguntalt_chama("N�o foi possivel actualizar o estado da marca��o.", "", "OK", 16)
*!*			RETURN .f.
*!*		ENDIF
*!*		
*!*		SELECT UcrsMarcacoesDia
*!*		DO CASE 
*!*			CASE UPPER(ALLTRIM(UcrsMarcacoesDia.estado)) == 'PEDIDO'
*!*				REPLACE UcrsMarcacoesDia.estado WITH 'MARCADO'
*!*				REPLACE UcrsMarcacoesDia.nivelUrgencia WITH ''
*!*			CASE UPPER(ALLTRIM(UcrsMarcacoesDia.estado)) == 'MARCADO'
*!*				 REPLACE UcrsMarcacoesDia.estado WITH 'ATRASADO'
*!*				 REPLACE UcrsMarcacoesDia.nivelUrgencia WITH ''
*!*			CASE UPPER(ALLTRIM(UcrsMarcacoesDia.estado)) == 'ATRASADO'
*!*				 REPLACE UcrsMarcacoesDia.estado WITH 'CANCELADO'
*!*				 REPLACE UcrsMarcacoesDia.nivelUrgencia WITH ''
*!*			CASE UPPER(ALLTRIM(UcrsMarcacoesDia.estado)) == 'CANCELADO'
*!*				 REPLACE UcrsMarcacoesDia.estado WITH 'PRESENTE'
*!*				 IF EMPTY(UcrsMarcacoesDia.nivelUrgencia)
*!*		 			 REPLACE UcrsMarcacoesDia.nivelUrgencia WITH 'N�O URGENTE'
*!*				ENDIF 
*!*			CASE UPPER(ALLTRIM(UcrsMarcacoesDia.estado)) == 'PRESENTE'
*!*				
*!*				REPLACE UcrsMarcacoesDia.estado WITH 'TRATAMENTO'
*!*				IF EMPTY(UcrsMarcacoesDia.nivelUrgencia)
*!*		 			 REPLACE UcrsMarcacoesDia.nivelUrgencia WITH 'N�O URGENTE'
*!*				ENDIF 
*!*			CASE UPPER(ALLTRIM(UcrsMarcacoesDia.estado)) == 'TRATAMENTO'
*!*				 REPLACE UcrsMarcacoesDia.estado WITH 'EFETUADO'
*!*				 REPLACE UcrsMarcacoesDia.nivelUrgencia WITH ''
*!*	 	 
*!*		ENDCASE
*!*			
*!*		MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.estado.Refresh
*!*		MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.nivelUrgencia.Refresh
*!*		MARCACOES.GridPesq.Refresh

*!*	ENDFUNC 


**
*!*	FUNCTION uf_marcacoes_nivelUrgenciaSeguinte
*!*		
*!*		SELECT UcrsMarcacoesDia
*!*		IF EMPTY(ALLTRIM(UcrsMarcacoesDia.nivelUrgencia))
*!*			RETURN .f.
*!*		ENDIF 
*!*		
*!*		lcSQL = ""
*!*		TEXT TO lcSQL TEXTMERGE NOSHOW
*!*			exec up_marcacoes_nivelUrgenciaSeguinte '<<UPPER(ALLTRIM(UcrsMarcacoesDia.nivelUrgencia))>>','<<UPPER(ALLTRIM(UcrsMarcacoesDia.mrstamp))>>'
*!*		ENDTEXT
*!*		If !uf_gerais_actGrelha("", "", lcSql)
*!*			uf_perguntalt_chama("N�o foi possivel actualizar o estado da marca��o.", "", "OK", 16)
*!*			RETURN .f.
*!*		ENDIF
*!*		
*!*		SELECT UcrsMarcacoesDia
*!*		DO CASE 
*!*			CASE UPPER(ALLTRIM(UcrsMarcacoesDia.nivelUrgencia)) == 'EMERGENTE'
*!*				 REPLACE UcrsMarcacoesDia.nivelUrgencia WITH 'MTO. URGENTE'
*!*			CASE UPPER(ALLTRIM(UcrsMarcacoesDia.nivelUrgencia)) == 'MTO. URGENTE'
*!*				 REPLACE UcrsMarcacoesDia.nivelUrgencia WITH 'URGENTE'
*!*			CASE UPPER(ALLTRIM(UcrsMarcacoesDia.nivelUrgencia)) == 'URGENTE'
*!*				 REPLACE UcrsMarcacoesDia.nivelUrgencia WITH 'POUCO URGENTE'
*!*			CASE UPPER(ALLTRIM(UcrsMarcacoesDia.nivelUrgencia)) == 'POUCO URGENTE'
*!*				 REPLACE UcrsMarcacoesDia.nivelUrgencia WITH 'N�O URGENTE'
*!*			CASE UPPER(ALLTRIM(UcrsMarcacoesDia.nivelUrgencia)) == 'N�O URGENTE'
*!*				 REPLACE UcrsMarcacoesDia.nivelUrgencia WITH 'EMERGENTE'
*!*		ENDCASE
*!*			
*!*		MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.estado.Refresh
*!*		MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.nivelUrgencia.Refresh
*!*		MARCACOES.GridPesq.Refresh

*!*	ENDFUNC 


**
FUNCTION uf_MARCACOES_controlaPass
	
	PUBLIC myVirtualText, myVirtualVar, myPassWordChar 
		
	Local lcPass, cval, lcVal
	Store '' To lcPass, cval
	Store .f. To lcVal
			
	** guardar pass **
	IF !Empty(uf_gerais_getParameter("ADM0000000076","TEXT"))
		lcPass = uf_gerais_getParameter("ADM0000000076","TEXT")
	ENDIF
	
	** pedir password **
	MARCACOES.pass.value = ''
		
	**chama Painel Virtual, funcoes genericas
	uf_tecladoAlpha_Chama('MARCACOES.pass','INTRODUZA PASSWORD DE SUPERVISOR:',.t.,.f.,1)

	cval=alltrim(MARCACOES.pass.value)
	
	DO CASE
		CASE empty(cval) Or Empty(lcPass)
			uf_perguntalt_chama("DEVE INTRODUZIR A PASSWORD DE SUPERVISOR PARA REALIZAR ESTA OPERA��O.","OK","",48)
		CASE !(Upper(Alltrim(lcPass))==Upper(Alltrim(cval)))
			uf_perguntalt_chama("A PASSWORD N�O EST� CORRECTA. POR FAVOR VERIFIQUE SE EST� A INTRODUZIR CORRECTAMENTE.","OK","",16)
		OTHERWISE
			lcVal = .t.
	ENDCASE
		
	RETURN lcVal	
	
ENDFUNC 


**
FUNCTION uf_marcacoes_EntregaTalaoLevantamento
	
	SELECT ucrsMarcacoesDia
	Replace ucrsMarcacoesDia.designEntrega WITH ucrsMarcacoesDia.nome
	Replace ucrsMarcacoesDia.dataEntrega WITH datetime()+(difhoraria*3600)
	**Replace ucrsMarcacoesDia.dataEntrega WITH DATE()
	Replace ucrsMarcacoesDia.userEntrega WITH ch_userno

	MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.designEntrega.refresh
	MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.dataEntrega.refresh
	MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.userEntrega.refresh
ENDFUNC 


**
FUNCTION UF_MARCACOES_scanner

	IF myMarcacoesIntroducao == .t. OR myMarcacoesAlteracao == .t. OR myMarcacoesFtUtentes == .t.
		uf_perguntalt_chama("Deve gravar ou cancelar a grava��o antes de alterar o dia.", "", "OK", 64)
		RETURN .f.
	ENDIF 


	** Pesquisa por Numero de Marcacao
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_Marcacoes_MarcacoesScanner '<<ALLTRIM(MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.txtScanner.value)>>'
	ENDTEXT 
	
	If !uf_gerais_actGrelha("", "ucrsMrScanner", lcSql)
		uf_perguntalt_chama("N�o foi possivel verficar as marca��es.", "", "OK", 16)
		RETURN .f.
	ENDIF
	
	IF RECCOUNT("ucrsMrScanner")> 0
		
		
		LOCAL lcDataPeriodo, lcMrstamp
		SELECT ucrsMrScanner
		lcDataPeriodo = ucrsMrScanner.data
		lcMrstamp = ucrsMrScanner.mrstamp
		uf_marcacoes_chama()

		SELECT ucrsMarcacoesCab
		Replace ucrsMarcacoesCab.ano WITH YEAR(lcDataPeriodo)
		Replace ucrsMarcacoesCab.mes WITH MONTH(lcDataPeriodo)
		Replace ucrsMarcacoesCab.mesDesc WITH uf_gerais_getMonthToMonthEx(MONTH(lcDataPeriodo))
		
		uf_marcacoes_AplicaDia(LEFT(uf_gerais_getdate(lcDataPeriodo,"DATA"),2))
		marcacoes.local.value = ""
		marcacoes.utente.value = ALLTRIM(ucrsMrScanner.nome)
		marcacoes.serie.value = ""
		marcacoes.especialidade.value = ""
		marcacoes.servico.value = ""
		marcacoes.especialista.value = ""

		
		SELECT ucrsMarcacoesDia
		LOCATE FOR ucrsMarcacoesDia.mrstamp == ALLTRIM(lcMrstamp)
		IF FOUND()
			marcacoes.gridPesq.nome.setfocus
		ENDIF 			
		
	ENDIF 
	
	**
	MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.txtScanner.value = ""

ENDFUNC 


**
FUNCTION uf_marcacoes_TalaoLevantamentoDefault
	
	SELECT ucrsMarcacoesDia
	Replace ucrsMarcacoesDia.localEntrega WITH "N/Instala��es"
	Replace ucrsMarcacoesDia.dataLevantamento WITH datetime()+(difhoraria*3600)
	**Replace ucrsMarcacoesDia.dataLevantamento WITH DATE()

	MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.localEntrega.refresh
	MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page1.dataLevantamento.refresh

ENDFUNC 


**
FUNCTION uf_marcacoes_declaracaoPresenca

	&&	LPARAMETERS lcTipo
	lcTipo = "print"

	** Guarda a impressora por defeito do Windows***
	lcDefaultPrinter=Set("printer",2)
	*****************************************
	
	** Obtem Nome do Ficheiro Tabela de Impressoes **
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select Top 1 nomeficheiro, tabela, temduplicados, numduplicados
		From b_impressoes (nolock)
		Where nomeimpressao = 'TALAO_LEVANTAMENTO'
	Endtext
	
	If !uf_gerais_actGrelha("","templcSQL",lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR A CONFIGURA��O DE IMPRESS�ES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return
	Endif
	
	If Reccount("templcSQL")>0
		lcnomeFicheiro = Alltrim(templcSQL.nomeficheiro)
		lcprinter = "Impressora por defeito do Windows"
		lctabela = templcSQL.tabela
		lctemduplicados = templcSQL.temduplicados
		lcnumduplicados = templcSQL.numduplicados
	Else
		RETURN .f.
	Endif

	If !(uf_gerais_actGrelha("","uCrsImpressao",lcSQL))
		&&erro
	ENDIF
	
	****************************************
	** PREPARAR CURSORES PARA IMPRESS�ES **
	****************************************
	
	** Obtem dados Gerais da Emprea / Loja **
	If !USED("uCrsE1")
		uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR OS DADOS GERAIS DA EMPRESA. CONTACTE O SUPORTE.","OK","",16)	
		RETURN .f.
	Endif
	********************************
	
	
	SELECT UcrsMarcacoesDia
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_declaracaoPresenca '<<ALLTRIM(UcrsMarcacoesDia.mrstamp)>>'
	ENDTEXT 
	
	IF !uf_gerais_actGrelha("","uCrsDecPresenca",lcSQL)
		uf_perguntalt_chama("Ocorreu um erro a imprimir a declara��o de Presen�a.", "OK", "", 16)
		RETURN .f.
 	ENDIF

	SELECT uCrsDecPresenca				
	lcreportTalaoLevantamento = Alltrim(myPath)+"\analises\declaracao_presenca.frx"

	TRY		
		Report Form Alltrim(lcreportTalaoLevantamento) To Printer
	CATCH
		uf_perguntalt_chama("Foi detectado um problema com a impressora." + CHR(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
	ENDTRY
	
	** preparar impressora **
	If !(alltrim(lcPrinter) == "Impressora por defeito do Windows")
		LOCAL lcValErroPrinter
		TRY 
			Set Printer To Name ''+alltrim(lcPrinter)+''
		CATCH
			uf_perguntalt_chama("Foi detectado um problema com a impressora." + CHR(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
			lcValErroPrinter = .t.
		ENDTRY
		IF lcValErroPrinter == .t.
			RETURN .f.
		ENDIF
	Endif
		
	Set Device To print
	Set Console off
	***********************************
	
	
	** por impressora no default **
	Set Device To screen
	Set printer to Name ''+lcDefaultPrinter+''
	Set Console On
	**************************	


	
ENDFUNC 


**
FUNCTION uf_marcacoes_TalaoLevantamento

	IF USED("ucrsMarcacoesDia")
			
		SELECT ucrsMarcacoesDia
		IF !EMPTY(ucrsMarcacoesDia.talaolevantamento)
			*** Imprime
			uf_mrpagamento_imprimir_TLAVANTAMENTO()
		ELSE
			uf_perguntalt_chama("A marca��o n�o est� definida para ter Tal�o de Levantamento.","OK","",64)
		ENDIF 
	ENDIF 

ENDFUNC 