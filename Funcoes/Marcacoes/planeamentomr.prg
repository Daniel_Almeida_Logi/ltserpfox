
** Define class para os eventos **
DEFINE CLASS pbordoPlaneamentoMr As Custom
	
	PROCEDURE proc_eventoGotFocus
		PLANEAMENTOMR.gridDoc.columns(PLANEAMENTOMR.gridDoc.ActiveColumn).text1.BackColor = RGB(42,143,154)
	ENDPROC	

ENDDEFINE 

**
FUNCTION uf_PLANEAMENTOMR_chama
	Lparameters lcSerieNo
	
	PUBLIC myPLANEAMENTOMRIntroducao, myPLANEAMENTOMRAlteracao

	IF TYPE("PLANEAMENTOMR") == "U"
		uf_PLANEAMENTOMR_defaults()
		DO FORM PLANEAMENTOMR
	ELSE
		PLANEAMENTOMR.show
	ENDIF

	*** Cria classe para permitir os eventos dinamicos **
	If Type("PLANEAMENTOMR.oCust") # "O"
		PLANEAMENTOMR.AddObject("oCust","pbordoPlaneamentoMr")
	ENDIF

	IF !EMPTY(lcSerieNo)
		uf_PLANEAMENTOMR_CalculaCursores(lcSerieNo)
	ELSE
		DO CASE 
			CASE ucrsSeriesServ.tipo == "Atividades"
				uf_PLANEAMENTOMR_novo(3)
			OTHERWISE
				uf_PLANEAMENTOMR_novo(1)
		ENDCASE 
	ENDIF 
	
	**
	Select ucrsSeriesServ
	GO Top
	
	uf_PLANEAMENTOMR_atualizaCalendarioMensal()
	PLANEAMENTOMR.REFRESH

ENDFUNC

**
FUNCTION uf_PLANEAMENTOMR_novo
	LPARAMETERS lcTipo
		
	IF EMPTY(lcTipo)
	 lcTipo = 0
	ENDIF 
	
	LOCAL lcStamp
	
	** 
	Select ucrsSeriesServ
	GO TOP 
	SCAN 
		Delete
	ENDSCAN
	
	**Numeracao da serie
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_marcacoes_seriesNum
	ENDTEXT
	If !uf_gerais_actGrelha("", "ucrsSeriesServNum", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar a numera��o a atrinuir � S�rie. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	SELECT ucrsSeriesServNum
	LcNo = ucrsSeriesServNum.numero
	
	**Paciente Generico
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		select TOP 1 utstamp, nome, no, estab from b_utentes where pacgen = 1
	ENDTEXT
	If !uf_gerais_actGrelha("", "ucrsDadosPacGenerico", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar informa��o do Paciente Gen�rico. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	
	
	lcStamp = uf_gerais_stamp()

	Select ucrsSeriesServ
	APPEND BLANK
	Replace ucrsSeriesServ.seriestamp WITH lcStamp 
	Replace ucrsSeriesServ.serieno 	WITH LcNo 
	Replace ucrsSeriesServ.dataInicio WITH DATE()
	Replace ucrsSeriesServ.dataFim WITH DATE()
	Replace ucrsSeriesServ.dataIniRep WITH DATE()
	
	Replace ucrsSeriesServ.horaInicio WITH "00:00"
	Replace ucrsSeriesServ.horaFim WITH "23:59"
	Replace ucrsSeriesServ.tempoinatividade WITH 90

	Replace ucrsSeriesServ.repetir WITH .f.
	Replace ucrsSeriesServ.criterio WITH "Todos os dias"
	Replace ucrsSeriesServ.valorRep WITH 1
	Replace ucrsSeriesServ.repnunca WITH .f.
	Replace ucrsSeriesServ.repapos WITH .t.
	Replace ucrsSeriesServ.repdia WITH .f.
	Replace ucrsSeriesServ.sessoes 	WITH 0
	Replace ucrsSeriesServ.dataFimRep WITH DATE()
	
	** paciente Gen�rico
	IF RECCOUNT("ucrsDadosPacGenerico") > 0
	
		Replace ucrsSeriesServ.utstamp WITH ucrsDadosPacGenerico.utstamp
		Replace ucrsSeriesServ.nome WITH ALLTRIM(ucrsDadosPacGenerico.nome)
		Replace ucrsSeriesServ.no WITH ucrsDadosPacGenerico.no
		Replace ucrsSeriesServ.estab WITH ucrsDadosPacGenerico.estab 
	
	ENDIF 
			
	**Apaga servi�os associadoa � Serie anterior
	SELECT ucrsServicosSerie
	GO Top
	SCAN
		delete
	ENDSCAN
	
			
	**Apaga recursos associadas � Serie anterior
	SELECT uCrsRecursosDaSerie
	SET FILTER TO 
	GO Top
	SCAN
		delete
	ENDSCAN
		
	
	**Apaga consumos associadas � Serie anterior
	SELECT uCrsSeriesConsumo
	GO Top
	SCAN
		delete
	ENDSCAN
	
	DO CASE 
		CASE lcTipo = 1 && Servi�o
			Select ucrsSeriesServ
			Replace ucrsSeriesServ.tipo with "Servi�os"
			REPLACE ucrsSeriesServ.serienome with "Planeamento Servi�o Nr." + ASTR(ucrsSeriesServ.serieno) 
		
		CASE lcTipo = 3 && disponibilidade Marca��o Atividade
			Select ucrsSeriesServ
			Replace ucrsSeriesServ.tipo with "Atividades"
			REPLACE ucrsSeriesServ.serienome with "Planeamento Marca��o Nr." + ASTR(ucrsSeriesServ.serieno) 

	ENDCASE 

ENDFUNC 


**
FUNCTION uf_PLANEAMENTOMR_defaults
	
	IF !USED("ucrsSeriesServ")
		**Cursor Series
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_marcacoes_series null
		ENDTEXT

		If !uf_gerais_actGrelha("", "ucrsSeriesServ", lcSql)
			uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es de S�ries de Servi�o. Contacte o suporte.", "", "OK", 32)
			RETURN .f.
		ENDIF
	ENDIF

	IF !USED("ucrsServicosSerie")
		**Cursor Series
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_marcacoes_seriesServicos null
		ENDTEXT

		If !uf_gerais_actGrelha("", "ucrsServicosSerie", lcSql)
			uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es dos Servi�os da S�ries. Contacte o suporte.", "", "OK", 32)
			RETURN .f.
		ENDIF
	ENDIF
	
	**	
	IF !USED("ucrsPeriodosCalendarioMensal")
		CREATE CURSOR ucrsPeriodosCalendarioMensal(posicao n(9,0), periodo n(9,0), dataIni d,dataFim d,horaIni c(5),horaFim c(5),nome c(150), sel l, duracao n(8,0), extra l, marcado l)
	ENDIF 
	

	**
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_DadosRecursos
	ENDTEXT 
	If !uf_gerais_actGrelha("", "ucrsRecursos", lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar dados dos recursos. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF

	**	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_marcacoes_SeriesRecursos null
	ENDTEXT	
	
	IF !uf_gerais_actgrelha("", "uCrsRecursosDaSerie", lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR OS RECURSOS ASSOCIADOS � S�RIE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	**
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_marcacoes_SeriesConsumo null
	ENDTEXT	
	IF !uf_gerais_actgrelha("", "uCrsSeriesConsumo", lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR OS CONSUMOS ASSOCIADOS � S�RIE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF


	IF !USED("ucrsSessoesSerie")
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_marcacoes_sessoes null
		ENDTEXT	
		IF !uf_gerais_actgrelha("", "ucrsSessoesSerie", lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A VERIFICAR AS SESS�ES ASSOCIADAS � S�RIE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	ELSE
		SELECT ucrsSessoesSerie
		GO TOP 
		SCAN
			DELETE 
		ENDSCAN 
	ENDIF 

	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_clinica_DadosComparticipacao '','',''
	ENDTEXT
	IF !uf_gerais_actGrelha("", "ucrsDadosComparticipacaoSerie",lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VALIDAR DADOS DE COMPARTICIPA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF


	IF USED("ucrsRecursosCalendarioMensal")
		fecha("ucrsRecursosCalendarioMensal")
	ENDIF 
	IF !USED("ucrsRecursosCalendarioMensal")
		LOCAL lcVarCreateCursor 
		lcVarCreateCursor = "stamp c(25), descricao c(245), mrsimultaneo n(9,0), duracao n(9,0), ordem n(9), filtro l, local c(20), pvp n(19,3),compart n(19,3), utente n(19,3),  ref c(18), utilizador n(19,3), design c(254), nomeEspecialista c(254)"
		lcData = DATE()
		lcDataFim = GOMONTH(lcData, +1)
		
		
		DO WHILE lcData < lcDataFim 
			lcVarCreateCursor = lcVarCreateCursor + ", d" + uf_gerais_getdate(lcData,"SQL") + " c(20)"
			lcData = lcData + 1
		ENDDO
		CREATE CURSOR ucrsRecursosCalendarioMensal(&lcVarCreateCursor)
	ENDIF

ENDFUNC


**
FUNCTION uf_PLANEAMENTOMR_CalculaCursores
	Lparameters lcSerieNo

	LOCAL lcSerieStamp 
	lcSerieStamp = ""

	IF !EMPTY(lcSerieNo)
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			select seriestamp from b_series where serieno = <<lcSerieNo>>
		ENDTEXT
		
		If !uf_gerais_actGrelha("", "ucrsSeriesStamp", lcSql)
			uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es da S�rie. Contacte o suporte.", "", "OK", 32)
			RETURN .f.
		ENDIF
		
		IF RECCOUNT("ucrsSeriesStamp")>0
			SELECT ucrsSeriesStamp
			lcSerieStamp = ucrsSeriesStamp.seriestamp 
		ENDIF 
	ENDIF 

	**Cursor Series
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_marcacoes_series '<<lcSerieStamp>>'
	ENDTEXT

	If !uf_gerais_actGrelha("", "ucrsSeriesServ", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es de S�ries de Servi�o. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	
	**Cursor Servi�os
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_marcacoes_seriesServicos '<<lcSerieStamp>>'
	ENDTEXT

	If !uf_gerais_actGrelha("PLANEAMENTOMR.ContainerDetalhe.PageFrame1.page3.GridServicosSerie", "ucrsServicosSerie", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es dos Servi�os da S�ries. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_marcacoes_SeriesRecursos '<<lcSerieStamp>>'
	ENDTEXT	
	
	IF !uf_gerais_actgrelha("PLANEAMENTOMR.ContainerDetalhe.PageFrame1.page3.gridRecursos", "uCrsRecursosDaSerie", lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR OS RECURSOS ASSOCIADOS � S�RIE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_marcacoes_SeriesConsumo '<<lcSerieStamp>>'
	ENDTEXT	

	IF !uf_gerais_actgrelha("PLANEAMENTOMR.ContainerDetalhe.PageFrame1.page3.GridConsumos", "uCrsSeriesConsumo", lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR OS CONSUMOS ASSOCIADOS � S�RIE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	** Calcula informa��o de recursos
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_DadosRecursos
	ENDTEXT 
	If !uf_gerais_actGrelha("", "ucrsRecursos", lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar dados dos recursos. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	
	** Sessoes da Serie
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_marcacoes_sessoes <<lcSerieNo>>
	ENDTEXT
	IF !uf_gerais_actgrelha("PLANEAMENTOMR.ContainerDetalhe.Pageframe1.Page2.Container1.GridSessoes", "ucrsSessoesSerie", lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR AS SESS�ES ASSOCIADAS � S�RIE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
		
	
	PLANEAMENTOMR.refresh
ENDFUNC 


**
FUNCTION uf_PLANEAMENTOMR_init
	
	&& Configura menu principal
	WITH PLANEAMENTOMR.menu1
		.adicionaOpcao("fichaUtente","Utente",myPath + "\imagens\icons\detalhe_micro.png","uf_PLANEAMENTOMR_fichaUtente","U")
		.adicionaOpcao("fichaServi�o","Servi�o",myPath + "\imagens\icons\detalhe_micro.png","uf_PLANEAMENTOMR_fichaServico","S")
		.adicionaOpcao("fichaUtilizador","Especialista",myPath + "\imagens\icons\detalhe_micro.png","uf_PLANEAMENTOMR_fichaEspecialista","S")
		.estado("", "SHOW", "Gravar", .t., "Cancelar", .t.)
	ENDWITH

ENDFUNC


**
FUNCTION uf_PLANEAMENTOMR_fichaUtente
	SELECT ucrsSeriesServ
	IF !EMPTY(ucrsSeriesServ.no)
		uf_utentes_chama(ucrsSeriesServ.no)
	ENDIF 
ENDFUNC

**
FUNCTION uf_PLANEAMENTOMR_fichaServico
	SELECT ucrsRecursosCalendarioMensal
	IF !EMPTY(ucrsRecursosCalendarioMensal.ref)
		uf_stocks_chama(ucrsRecursosCalendarioMensal.ref)
	ENDIF 
ENDFUNC

**
FUNCTION uf_PLANEAMENTOMR_fichaEspecialista
	SELECT ucrsRecursosCalendarioMensal
	IF !EMPTY(ucrsRecursosCalendarioMensal.utilizador)
		uf_utilizadores_chama(ucrsRecursosCalendarioMensal.utilizador)
	ENDIF 
ENDFUNC

**
FUNCTION uf_PLANEAMENTOMR_ApagaServico
	LOCAL lcRef 
	lcRef = ""
	
			
	select ucrsServicosSerie
	lcRef = ucrsServicosSerie.ref
	DELETE	
	

	**Apaga recursos associadas � Serie anterior
	SELECT uCrsRecursosDaSerie
	GO TOP 
	SCAN FOR UPPER(ALLTRIM(ref)) == UPPER(ALLTRIM(lcRef))
		delete
	ENDSCAN
		
	**Apaga consumos associadas � Serie anterior
	SELECT uCrsSeriesConsumo
	GO Top
	SCAN FOR UPPER(ALLTRIM(ref)) == UPPER(ALLTRIM(lcRef))
		delete
	ENDSCAN
	
	
	Select ucrsServicosSerie
	lcPos = recno()
	select ucrsServicosSerie
	TRY
		IF lcPos>1
			go lcPos-1
		ENDIF
	CATCH
		**
	ENDTRY

	PLANEAMENTOMR.ContainerDetalhe.PageFrame1.page3.GridServicosSerie.REFRESH
			
	**
	uf_PLANEAMENTOMR_calculaDuracaoSessao()	 
ENDFUNC




**
FUNCTION uf_PLANEAMENTOMR_calculaDuracaoSessao
	LOCAL lcDuracao
	lcDuracao = 0
	
	select ucrsServicosSerie 
	Go Top
	
	Select ordem,MAX(duracao) as duracao from ucrsServicosSerie GROUP BY ordem INTO CURSOR ucrsDuracaoSessao READWRITE

	Select ucrsDuracaoSessao 
	GO TOP
	CALCULATE SUM(duracao) TO lcDuracao
	
	SELECT ucrsSeriesServ
	Replace ucrsSeriesServ.duracao WITH lcDuracao 
	
ENDFUNC




FUNCTION uf_PLANEAMENTOMR_consumonovo
	LOCAL lcRefs 
	STORE 0 TO lcRefs 
	
	uf_pesqstocks_chama('CONSUMO')

ENDFUNC 



FUNCTION uf_PLANEAMENTOMR_consumoapaga
	
	select uCrsSeriesConsumo
	DELETE 
	
	Select uCrsSeriesConsumo
	lcPos = recno()
	select uCrsSeriesConsumo
	TRY
		IF lcPos>1
			go lcPos-1
		ENDIF
	CATCH
		**
	ENDTRY

	PLANEAMENTOMR.ContainerDetalhe.Pageframe1.Page3.Refresh	
	
	select uCrsSeriesConsumo
	GO BOTTOM 
ENDFUNC 




**
FUNCTION uf_PLANEAMENTOMR_RecursoApaga
		
	select uCrsRecursosDaSerie
	DELETE 
	
	Select uCrsRecursosDaSerie
	lcPos = recno()
	select uCrsRecursosDaSerie
	TRY
		IF lcPos>1
			go lcPos-1
		ENDIF
	CATCH
		**
	ENDTRY

	PLANEAMENTOMR.ContainerDetalhe.Pageframe1.Page3.GridRecursos.Refresh
	
	select uCrsRecursosDaSerie
	GO BOTTOM 
		 
ENDFUNC


**
FUNCTION uf_PLANEAMENTOMR_RecursoNovo

	** No caso da serie ser do tipo servi�os � obrigadtorio especificar um servi�o
	select ucrsSeriesServ
	IF ALLTRIM(UPPER(ucrsSeriesServ.tipo)) == "SERVI�OS" AND EMPTY(ALLTRIM(ucrsServicosSerie.ref))
		uf_perguntalt_chama("Selecione o servi�o ao qual vai adicionar recursos.", "", "OK", 64)
		RETURN .f.
	ENDIF 
	
	**
	uf_PESQSERIESRECURSOS_Chama('')


ENDFUNC



**
FUNCTION uf_PLANEAMENTOMR_atualizaCalendarioMensal
	LOCAL i,j, LcNumDiasCalculo, lcDesign 
	i = 0
	LcNumDiasCalculo = 0
	
	**
	PLANEAMENTOMR.GridDoc.recordSource = ""
	WITH PLANEAMENTOMR.GridDoc
		FOR j=1 TO .columnCount
			.DeleteColumn
		ENDFOR
	ENDWITH 

	IF USED("ucrsRecursosCalendarioMensal")
		FECHA("ucrsRecursosCalendarioMensal")
	ENDIF


	lcVarCreateCursor = "stamp c(25), descricao c(245), mrsimultaneo n(9,0), duracao n(9,0), ordem n(9), filtro l, local c(20), pvp n(19,3),compart n(19,3), utente n(19,3),  ref c(18), utilizador n(19,3), design c(254), nomeEspecialista c(254)"
	lcData = DATE(PLANEAMENTOMR.anoPlaneamento,PLANEAMENTOMR.mesPlaneamento,PLANEAMENTOMR.diaPlaneamento)
	lcDataFim = GOMONTH(lcData, +1)
	
	DO WHILE lcData < lcDataFim 
		lcVarCreateCursor = lcVarCreateCursor + ", d" + uf_gerais_getdate(lcData,"SQL") + " c(20)"
		lcData = lcData + 1
	ENDDO
	CREATE CURSOR ucrsRecursosCalendarioMensal(&lcVarCreateCursor)

	lcData = DATE(PLANEAMENTOMR.anoPlaneamento,PLANEAMENTOMR.mesPlaneamento,PLANEAMENTOMR.diaPlaneamento)
	lcDataFim = GOMONTH(lcData, +1)

	** Primeria Coluna (Disponibilidades Recursos)
	WITH PLANEAMENTOMR.GridDoc
		
		**
		.recordSource = "ucrsRecursosCalendarioMensal"
		
		** Local
		i = 1
		.AddColumn 
		.RowHeight = uf_gerais_getRowHeight()
		.columns(i).ControlSource = "ucrsRecursosCalendarioMensal.local"
		.columns(i).text1.ControlSource = "ucrsRecursosCalendarioMensal.local"
		.columns(i).header1.FontName = "Verdana"
		.columns(i).header1.Fontsize = 7
		.columns(i).header1.caption = "LOCAL" 
		.columns(i).header1.Alignment = 2
		.columns(i).header1.FontBold = .f.
		.columns(i).FontName = "Verdana"
		.columns(i).Fontsize = 8
		.columns(i).ReadOnly = .t.
		.columns(i).text1.ReadOnly = .t.
		.Columns(i).columnorder = i
		.Columns(i).dynamicBackColor="uf_PLANEAMENTOMR_calendarioMensalBackColor()"				
		.Columns(i).width = 150
		.Columns(i).InputMask=""
		.Columns(i).text1.InputMask=""
		.columns(i).text1.BorderStyle =  0
		.columns(i).text1.ForeColor = RGB(0,0,0)
		.columns(i).text1.SelectedForeColor = RGB(0,0,0)
		.columns(i).text1.FontBold = .t.
		.columns(i).text1.MousePointer = 15
		.columns(i).text1.SelectedBackColor = RGB(42,143,154)
		.Columns(i).Format="RTK"
		.Columns(i).text1.Format="RTK"
		.Columns(i).text1.hideSelection=.t.	
		***********************************************
		
		i = 2		
		.AddColumn 
		.RowHeight = uf_gerais_getRowHeight()
		.columns(i).ControlSource = "ucrsRecursosCalendarioMensal.nomeEspecialista"
		.columns(i).text1.ControlSource = "ucrsRecursosCalendarioMensal.nomeEspecialista"
		.columns(i).header1.FontName = "Verdana"
		.columns(i).header1.Fontsize = 7
		.columns(i).header1.caption = "ESPECIALISTAS" 
		.columns(i).header1.Alignment = 2
		.columns(i).header1.FontBold = .f.
		.columns(i).FontName = "Verdana"
		.columns(i).Fontsize = 8
		.columns(i).ReadOnly = .t.
		.columns(i).text1.ReadOnly = .t.
		.Columns(i).columnorder = i
		.Columns(i).dynamicBackColor="uf_PLANEAMENTOMR_calendarioMensalBackColor()"
		.Columns(i).width = 150
		.Columns(i).InputMask=""
		.Columns(i).text1.InputMask=""
		.columns(i).text1.BorderStyle =  0
		.columns(i).text1.ForeColor = RGB(0,0,0)
		.columns(i).text1.SelectedForeColor = RGB(0,0,0)
		.columns(i).text1.FontBold = .t.
		.columns(i).text1.MousePointer = 15
		.columns(i).text1.SelectedBackColor = RGB(42,143,154)
		.Columns(i).Format="RTK"
		.Columns(i).text1.Format="RTK"
		.Columns(i).text1.hideSelection=.t.	
		*************************************
		
		DO WHILE lcData < lcDataFim 
			
			i = i +1
			
			lcNomeCampo = "d" + uf_gerais_getdate(lcData,"SQL")
			lcControlSource = "ucrsRecursosCalendarioMensal." + ALLTRIM(lcNomeCampo)

			.AddColumn 
			.RowHeight = uf_gerais_getRowHeight()
			.columns(i).ControlSource = lcControlSource 
			.columns(i).text1.ControlSource = lcControlSource 
			.columns(i).alignment = 2	
			.columns(i).header1.FontName = "Verdana"
			.columns(i).header1.Fontsize = 7
			.columns(i).FontName = "Verdana"
			.columns(i).Fontsize = 8
			.columns(i).ReadOnly = .t.
			.columns(i).text1.ReadOnly = .t.
			.Columns(i).columnorder = i
			.Columns(i).dynamicBackColor="uf_PLANEAMENTOMR_calendarioMensalBackColor()"
									
			.Columns(i).width = 50
			
			.Columns(i).InputMask="##################"
			.Columns(i).text1.InputMask="##################"
			.columns(i).header1.caption = LEFT(uf_marcacoes_DiaNome(lcData),3) +  CHR(13) + ASTR(DAY(lcData)) + [/] + ASTR(MONTH(lcData)) 
			.columns(i).header1.Alignment = 2
			.columns(i).header1.WordWrap = .t.
			.columns(i).header1.FontBold = .f.
			.columns(i).text1.BorderStyle =  0
			.columns(i).text1.ForeColor = RGB(0,0,0)
			.columns(i).text1.SelectedForeColor = RGB(0,0,0)
			.columns(i).text1.FontBold = .f.
			.columns(i).text1.MousePointer = 15
			.columns(i).text1.SelectedBackColor = RGB(42,143,154)
			.columns(i).text1.Alignment = 2
			

			
			.Columns(i).Format="RTK"
			.Columns(i).text1.Format="RTK"
			.Columns(i).text1.hideSelection=.t.	
	
			BindEvent(.columns(i).text1,"GotFocus",PLANEAMENTOMR.oCust,"proc_eventoGotFocus")
			
			lcData = lcData + 1
				
		ENDDO 
		
		.LockColumns = 2
	ENDWITH
	
	
	
	IF USED("ucrsMapeamentoRecursosCombinacao")
		fecha("ucrsMapeamentoRecursosCombinacao")
	ENDIF 
	
	CREATE CURSOR ucrsMapeamentoRecursosCombinacao (stamp c(25), no n(9), nome c(55), tipo c(30), site c(20), ref c(18), especialidade c(55))

	** Acrescenta Combina��es Possiveis ** 
	Select uCrsRecursosDaSerie
	SET FILTER TO 

	lcOrdem = 0
	lcOrdemCalendario = 0
	lcDesign = ""

	
	&&lcModo = "SERVI�OS"
	select ucrsSeriesServ
	lcModo = UPPER(ucrsSeriesServ.tipo)

	** Calcula informa��o de especialidades associadas aos utilizadores
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_EspecialidadesUs 
	ENDTEXT 

	If !uf_gerais_actGrelha("", "ucrsUsEspecialidades", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es de Especialidades associadas a utilizadores. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	********************************************************************	

	** Calcula cursor de indiponibilidades *****************************************
	LOCAL lcUserNos 
	lcUserNos = ""
	
	Select ucrsMapeamentoRecursosCombinacao
	GO Top
	SCAN
		IF ALLTRIM(lcUserNos) == ""
			lcUserNos = ASTR(ucrsMapeamentoRecursosCombinacao.no)
		ELSE
			lcUserNos = lcUserNos + "," + ASTR(ucrsMapeamentoRecursosCombinacao.no)
		ENDIF 
	ENDSCAN 
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_indisponibilidades '<<IIF(EMPTY(lcUserNos),0,lcUserNos)>>'
	ENDTEXT 
	If !uf_gerais_actGrelha("", "ucrsIndisponibilidadesUsMarcacoes", lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar indisponibilidades dos recursos. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	***************************************************************************

	DO CASE
		CASE ALLTRIM(UPPER(lcModo)) == "SERVI�OS"
			
			** 
			LOCAL lcRefs
			lcRefs = ""
			
			Select ucrsServicosSerie
			GO Top
			SCAN
				IF empty(lcRefs)
					lcRefs = ALLTRIM(ucrsServicosSerie.ref)
				ELSE
					lcRefs = "," + ALLTRIM(ucrsServicosSerie.ref)
				ENDIF 
			ENDSCAN 
			
			** Calcula informa��o de duracao dos servi�os da Series relacionados com os utilizadores 
			SELECT ucrsSeriesServ
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				exec up_Marcacoes_DuracaoMrSimServico '<<ALLTRIM(lcRefs)>>'
			ENDTEXT 	
			If !uf_gerais_actGrelha("", "ucrsDuracaoServicos", lcSql)
				uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es de Especialidades associadas a utilizadores. Contacte o suporte.", "", "OK", 32)
				RETURN .f.
			ENDIF
			********************************************************************	
			
	
				
			** adiciona todos os horarios dos utilizadores
			IF USED("ucrsHorariosPorLoja")
				fecha("ucrsHorariosPorLoja")
			ENDIF 
			SELECT distinct local FROM ucrsUsEspecialidades INNER JOIN uCrsRecursosDaSerie ON uCrsRecursosDaSerie.no == ucrsUsEspecialidades.userno ;
			ORDER BY local ;
			WHERE !EMPTY(ucrsUsEspecialidades.local) INTO CURSOR ucrsHorariosPorLoja READWRITE

			** Filtros
			** Local
			lcLocal = ALLTRIM(PLANEAMENTOMR.local.value)
			lcEspecialista = ALLTRIM(PLANEAMENTOMR.especialista.value)
			
			SELECT ucrsHorariosPorLoja
			GO TOP 
			SCAN FOR (UPPER(ALLTRIM(ucrsHorariosPorLoja.local)) == UPPER(ALLTRIM(PLANEAMENTOMR.local.value)) OR EMPTY(ALLTRIM(PLANEAMENTOMR.local.value))) 
			** 

				** 1 Precorre servi�os
				Select ucrsServicosSerie
				GO Top
				SCAN
					
					Select uCrsRecursosDaSerie
					LOCATE FOR ALLTRIM(ucrsServicosSerie.ref) == ALLTRIM(uCrsRecursosDaSerie.ref);
						AND ALLTRIM(UPPER(uCrsRecursosDaSerie.tipo)) != 'ESPECIALIDADE'
					IF FOUND()
						
						lcOrdemCalendario = lcOrdemCalendario +1
						lcDesign = ""
						lcStamp = uf_gerais_stamp()
						lcRef = ALLTRIM(ucrsServicosSerie.ref)
						
					
						Select ucrsRecursosCalendarioMensal
						APPEND BLANK
						Replace ucrsRecursosCalendarioMensal.stamp with lcStamp 
						Replace ucrsRecursosCalendarioMensal.ordem WITH lcOrdemCalendario
						Replace ucrsRecursosCalendarioMensal.ref WITH ucrsServicosSerie.ref
						Replace ucrsRecursosCalendarioMensal.utilizador WITH uCrsRecursosDaSerie.no
						Replace ucrsRecursosCalendarioMensal.design WITH ALLTRIM(ucrsServicosSerie.design) 
						
						lcDesign = lcDesign + IIF(!EMPTY(ALLTRIM(lcDesign))," + ","") + ALLTRIM(ucrsServicosSerie.design) 
						lcLocal = ALLTRIM(ucrsHorariosPorLoja.local)
						lcNomesEspecialistas = ""
					
						&& Especialistas 
						Select uCrsRecursosDaSerie
						GO Top
						SCAN FOR ALLTRIM(lcRef) == ALLTRIM(uCrsRecursosDaSerie.ref) AND ALLTRIM(UPPER(uCrsRecursosDaSerie.tipo)) != 'ESPECIALIDADE'

							lcDesign = lcDesign + IIF(!EMPTY(ALLTRIM(lcDesign))," + ","") + ALLTRIM(uCrsRecursosDaSerie.nome)
							lcLocal = ALLTRIM(ucrsHorariosPorLoja.local)
							lcNomesEspecialistas = lcNomesEspecialistas + IIF(!EMPTY(ALLTRIM(lcNomesEspecialistas ))," + ","") + ALLTRIM(uCrsRecursosDaSerie.nome)
							
							Select uCrsRecursosDaSerie
							** Cria Cursor que mapeia as linhas e os recursos associados
							Select ucrsMapeamentoRecursosCombinacao && este cursor tem todos os utilizadores de uma determinada combina��o, � usado para gravar na Marca��o
							APPEND BLANK
							Replace ucrsMapeamentoRecursosCombinacao.stamp WITH ucrsRecursosCalendarioMensal.stamp
							Replace ucrsMapeamentoRecursosCombinacao.no WITH uCrsRecursosDaSerie.no
							Replace ucrsMapeamentoRecursosCombinacao.nome WITH uCrsRecursosDaSerie.nome
							Replace ucrsMapeamentoRecursosCombinacao.tipo WITH uCrsRecursosDaSerie.tipo
							
							Replace ucrsMapeamentoRecursosCombinacao.site WITH ALLTRIM(ucrsHorariosPorLoja.local)
							Replace ucrsMapeamentoRecursosCombinacao.ref WITH ucrsServicosSerie.ref
							
							
							SELECT ucrsDuracaoServicos
							LOCATE FOR UPPER(ALLTRIM(ucrsDuracaoServicos.ref)) == UPPER(ALLTRIM(ucrsServicosSerie.ref));
									AND uCrsRecursosDaSerie.no = ucrsDuracaoServicos.no;
									AND ALLTRIM(UPPER(ucrsHorariosPorLoja.local)) = ALLTRIM(UPPER(ucrsDuracaoServicos.site));
									AND !EMPTY(ucrsDuracaoServicos.duracao)
							IF FOUND()
								Select ucrsRecursosCalendarioMensal
								Replace ucrsRecursosCalendarioMensal.duracao WITH ucrsDuracaoServicos.duracao
								Replace ucrsRecursosCalendarioMensal.mrsimultaneo WITH ucrsDuracaoServicos.mrsimultaneo
							ELSE
								Select ucrsRecursosCalendarioMensal
								Replace ucrsRecursosCalendarioMensal.duracao WITH ucrsServicosSerie.duracao
								Replace ucrsRecursosCalendarioMensal.mrsimultaneo WITH ucrsServicosSerie.mrsimultaneo
							ENDIF 							
							
						ENDSCAN 
						
						
						Select ucrsRecursosCalendarioMensal
						Replace ucrsRecursosCalendarioMensal.descricao WITH lcDesign 
						Replace ucrsRecursosCalendarioMensal.local WITH ALLTRIM(ucrsHorariosPorLoja.local)
						Replace ucrsRecursosCalendarioMensal.nomeespecialista WITH ALLTRIM(lcNomesEspecialistas) 
					
					ENDIF 
				ENDSCAN 
			ENDSCAN 
				
			** 
			Select ucrsServicosSerie
			GO Top
			SCAN
				
				lcRef = ALLTRIM(ucrsServicosSerie.ref)
				
				&& ESPECIALIDADE - Se for especialidade considera todos os utilizadores da especialidade
				Select uCrsRecursosDaSerie
				GO Top
				SCAN FOR ALLTRIM(lcRef) = ALLTRIM(uCrsRecursosDaSerie.ref) AND ALLTRIM(UPPER(uCrsRecursosDaSerie.tipo)) == 'ESPECIALIDADE'
					** adiciona todos os utilizadores da especialidade
					SELECT ucrsUsEspecialidades
					SCAN FOR ALLTRIM(UPPER(uCrsRecursosDaSerie.nome)) == ALLTRIM(UPPER(ucrsUsEspecialidades.especialidade));
						AND (UPPER(ALLTRIM(ucrsUsEspecialidades.local)) == UPPER(ALLTRIM(PLANEAMENTOMR.local.value)) OR EMPTY(ALLTRIM(PLANEAMENTOMR.local.value)));
						AND (UPPER(ALLTRIM(ucrsUsEspecialidades.nome)) == UPPER(ALLTRIM(PLANEAMENTOMR.especialista.value)) OR EMPTY(ALLTRIM(PLANEAMENTOMR.especialista.value)))
						
						lcOrdemCalendario = lcOrdemCalendario + 1
						lcDesign = ALLTRIM(ucrsServicosSerie.design) + " + " +ALLTRIM(ucrsUsEspecialidades.nome) 
						lcLocal = ALLTRIM(ucrsUsEspecialidades.local)
						lcStamp = uf_gerais_stamp()
						
						
						Select ucrsRecursosCalendarioMensal
						APPEND BLANK
						Replace ucrsRecursosCalendarioMensal.stamp with lcStamp 
						Replace ucrsRecursosCalendarioMensal.ordem WITH lcOrdemCalendario
						Replace ucrsRecursosCalendarioMensal.ref WITH lcRef 
						Replace ucrsRecursosCalendarioMensal.utilizador WITH ucrsUsEspecialidades.userno
						Replace ucrsRecursosCalendarioMensal.design WITH ALLTRIM(ucrsServicosSerie.design) 
						Replace ucrsRecursosCalendarioMensal.nomeespecialista WITH ALLTRIM(ucrsUsEspecialidades.nome) 
						
						
						** Cria Cursor que mapeia as linhas e os recursos associados
						Select ucrsMapeamentoRecursosCombinacao  && este cursor tem todos os utilizadores de uma determinada combina��o, � usado para gravar na Marca��o
						APPEND BLANK
						Replace ucrsMapeamentoRecursosCombinacao.stamp WITH ucrsRecursosCalendarioMensal.stamp
						Replace ucrsMapeamentoRecursosCombinacao.no WITH ucrsUsEspecialidades.userno
						Replace ucrsMapeamentoRecursosCombinacao.nome WITH ucrsUsEspecialidades.nome
						Replace ucrsMapeamentoRecursosCombinacao.tipo WITH 'Utilizadores'
						Replace ucrsMapeamentoRecursosCombinacao.site WITH ALLTRIM(ucrsUsEspecialidades.local)
						Replace ucrsMapeamentoRecursosCombinacao.ref WITH ALLTRIM(lcRef)
						
						Select ucrsRecursosCalendarioMensal
						Replace ucrsRecursosCalendarioMensal.descricao WITH lcDesign 
						Replace ucrsRecursosCalendarioMensal.local WITH lcLocal 
						Replace ucrsRecursosCalendarioMensal.mrsimultaneo WITH ucrsServicosSerie.mrsimultaneo
						
						
						SELECT ucrsDuracaoServicos
						LOCATE FOR UPPER(ALLTRIM(ucrsDuracaoServicos.ref)) == UPPER(ALLTRIM(lcRef));
								AND ucrsUsEspecialidades.userno = ucrsDuracaoServicos.no;
								AND ALLTRIM(UPPER(lcLocal)) = ALLTRIM(UPPER(ucrsDuracaoServicos.site));
								AND !EMPTY(ucrsDuracaoServicos.duracao)
						IF FOUND()
							Select ucrsRecursosCalendarioMensal
							Replace ucrsRecursosCalendarioMensal.duracao WITH ucrsDuracaoServicos.duracao
							Replace ucrsRecursosCalendarioMensal.mrsimultaneo WITH ucrsDuracaoServicos.mrsimultaneo
						ELSE
							Select ucrsRecursosCalendarioMensal
							Replace ucrsRecursosCalendarioMensal.duracao WITH ucrsServicosSerie.duracao
							Replace ucrsRecursosCalendarioMensal.mrsimultaneo WITH ucrsServicosSerie.mrsimultaneo
						ENDIF 
					
					ENDSCAN 
						
				ENDSCAN 

				lcOrdem = ucrsServicosSerie.ordem
				
			ENDSCAN 


			uf_PLANEAMENTOMR_atualizaPVPs()
			SELECT ucrsRecursosCalendarioMensal
			GO Top

		
		CASE ALLTRIM(UPPER(lcModo)) == "ATIVIDADES"
			
				
			** adiciona todos os horarios dos utilizadores
			IF USED("ucrsHorariosPorLoja")
				fecha("ucrsHorariosPorLoja")
			ENDIF 
			SELECT distinct local FROM ucrsUsEspecialidades INNER JOIN uCrsRecursosDaSerie ON uCrsRecursosDaSerie.no == ucrsUsEspecialidades.userno ;
			WHERE !EMPTY(ucrsUsEspecialidades.local) INTO CURSOR ucrsHorariosPorLoja READWRITE

			** Filtros
			** Local
			lcLocal = ALLTRIM(PLANEAMENTOMR.local.value)
			lcEspecialista = ALLTRIM(PLANEAMENTOMR.especialista.value)
			
			SELECT ucrsHorariosPorLoja
			GO TOP 
			SCAN FOR (UPPER(ALLTRIM(ucrsHorariosPorLoja.local)) == UPPER(ALLTRIM(PLANEAMENTOMR.local.value)) OR EMPTY(ALLTRIM(PLANEAMENTOMR.local.value))) 
			** 
				lcDesign = ""
				lcStamp = uf_gerais_stamp()
				
				Select ucrsRecursosCalendarioMensal
				APPEND BLANK
				Replace ucrsRecursosCalendarioMensal.ordem WITH 1
				Replace ucrsRecursosCalendarioMensal.stamp with lcStamp 

				Select uCrsRecursosDaSerie
				GO Top
				SCAN
					lcDesign = lcDesign + IIF(!EMPTY(lcDesign)," + ","") + ALLTRIM(uCrsRecursosDaSerie.nome)
					
					** Cria Cursor que mapeia as linhas e os recursos associados
					Select ucrsMapeamentoRecursosCombinacao 
					APPEND BLANK
					Replace ucrsMapeamentoRecursosCombinacao.stamp WITH ucrsRecursosCalendarioMensal.stamp
					Replace ucrsMapeamentoRecursosCombinacao.no WITH uCrsRecursosDaSerie.no
					Replace ucrsMapeamentoRecursosCombinacao.nome WITH uCrsRecursosDaSerie.nome
					Replace ucrsMapeamentoRecursosCombinacao.tipo WITH uCrsRecursosDaSerie.tipo
					
				ENDSCAN 
				Select ucrsRecursosCalendarioMensal
				Replace ucrsRecursosCalendarioMensal.descricao WITH lcDesign
				Replace ucrsRecursosCalendarioMensal.local WITH ucrsHorariosPorLoja.local
		
		ENDSCAN 
			
	ENDCASE
			
	
	**
	Select ucrsSeriesServ
	** 
	IF !empty(ucrsSeriesServ.serieno)
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_marcacoes_sessoes <<ucrsSeriesServ.serieno>>
		ENDTEXT	

*!*	_CLIPTEXT = lcSQL
*!*	MESSAGEBOX(lcSQL)	

		SELECT ucrsSessoesSerie
		GO top
		SCAN 
			DELETE 
		ENDSCAN 

		&& Cursor auxiliar para n�o perder as configuracaoes da grelha
		IF !uf_gerais_actgrelha("", "ucrsSessoesSerieAux", lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A VERIFICAR AS SESS�ES ASSOCIADAS � S�RIE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF	
		
		SELECT ucrsSessoesSerie
		APPEND FROM DBF('ucrsSessoesSerieAux')
		SELECT ucrsSessoesSerie
		GO Top
	ENDIF 	





			
	uf_PLANEAMENTOMR_indicadoresCalendarioMensal()	
	Select ucrsRecursosCalendarioMensal
	SET FILTER TO
	Select ucrsRecursosCalendarioMensal
	GO top	
	PLANEAMENTOMR.GridDoc.refresh

ENDFUNC 


**
FUNCTION uf_PLANEAMENTOMR_indicadoresCalendarioMensal
	LOCAL lcPeriodosDisponiveis, lcPeriodosMarcados, lcData, lcDataFim, LcNumDiasCalculo
	
	STORE 0 TO LcNumDiasCalculo,lcPeriodosDisponiveis, lcPeriodosMarcados

	lcData = DATE(PLANEAMENTOMR.anoPlaneamento,PLANEAMENTOMR.mesPlaneamento,PLANEAMENTOMR.diaPlaneamento)
	lcDataFim = GOMONTH(lcData, +1)
	
	** Dura��o de servi�os default **
	lcDuracao = 30

		
	** Disponibilidades de todos os utilizadores 
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_DadosRecursos
	ENDTEXT 
	If !uf_gerais_actGrelha("", "ucrsRecursos", lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar dados dos recursos. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	****************************************************************
	** Limita��es dos Servi�os associados �s disponibilidades, caso n�o exista nenhum servi�o
	** associado � disponibilidade, permite todos os servi�os caso contrario apenas permite os servi�os especificados
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_ServicosDisponibilidades
	ENDTEXT 
	If !uf_gerais_actGrelha("", "ucrsServicosDisponibilidades", lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar os servi�os associados �s disponibilidades. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	****************************************************************
	** Calcula informa��o das marca��es existentes no intervalo 
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_MarcacoesExistentesPlaneamento
	ENDTEXT 
	If !uf_gerais_actGrelha("", "MarcacoesExistentesPlaneamento", lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar marca��es existentes. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	****************************************************************
	

	
	Select distinct *;
	FROM;
		ucrsMapeamentoRecursosCombinacao;
		LEFT JOIN ucrsRecursos ON ucrsMapeamentoRecursosCombinacao.no = ucrsRecursos.no;
			AND UPPER(ALLTRIM(ucrsMapeamentoRecursosCombinacao.tipo)) = UPPER(ALLTRIM(ucrsRecursos.tipo));
			AND UPPER(ALLTRIM(ucrsMapeamentoRecursosCombinacao.site)) = UPPER(ALLTRIM(ucrsRecursos.site));
	INTO CURSOR ucrsRecursosDependencia READWRITE
		
		
	
	** Calcula cursor de indiponibilidades *****************************************
	LOCAL lcUserNos 
	lcUserNos = ""
	
	Select ucrsMapeamentoRecursosCombinacao
	GO Top
	SCAN
		IF ALLTRIM(lcUserNos) == ""
			lcUserNos = ASTR(ucrsMapeamentoRecursosCombinacao.no)
		ELSE
			lcUserNos = lcUserNos + "," + ASTR(ucrsMapeamentoRecursosCombinacao.no)
		ENDIF 
	ENDSCAN 
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_indisponibilidades '<<IIF(EMPTY(lcUserNos),0,lcUserNos)>>'
	ENDTEXT 
	If !uf_gerais_actGrelha("", "ucrsIndisponibilidadesUsMarcacoes", lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar indisponibilidades dos recursos. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	***************************************************************************

		
	** Calcula marca��es efecutadas para os dias de analise, para os recursos
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_existentes '<<uf_gerais_getdate(lcData,"SQL")>>','<<uf_gerais_getdate(lcDataFim,"SQL")>>'
	ENDTEXT 
	If !uf_gerais_actGrelha("", "ucrsMarcacoesExistentes", lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar dados dos recursos. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF

	k = 0
	Select ucrsRecursosCalendarioMensal
	GO Top
	SCAN
		
		
		**
		lcCombinacao = ALLTRIM(ucrsRecursosCalendarioMensal.descricao)

		lcData = DATE(PLANEAMENTOMR.anoPlaneamento,PLANEAMENTOMR.mesPlaneamento,PLANEAMENTOMR.diaPlaneamento)
		lcDataFim = GOMONTH(lcData, +1)

		DO WHILE lcData < lcDataFim 
		
			lcPeriodosDisponiveis = 0
			
			lcNomeCampo = "d" + uf_gerais_getdate(lcData,"SQL")
			lcControlSource = "ucrsRecursosCalendarioMensal." + ALLTRIM(lcNomeCampo)
		
			lcDateTime = DATETIME(1900,1,1,0,0,1)
			lcDatetimeFim = DATETIME(1900,1,1,23,59,59)

			Select ucrsPeriodosCalendarioMensal
			GO Top
			SCAN
				DELETE 
			ENDSCAN 
			
			lcMrsimultaneo = IIF(EMPTY(ucrsRecursosCalendarioMensal.mrsimultaneo),1,ucrsRecursosCalendarioMensal.mrsimultaneo)
			
						
			IF lcData >= DATE() && S� calcula periodos disponiveis para datas superiores � actual
	
	
				** Cursor que contem numero de marca��es Efetuadas por Periodo	
				SELECT ucrsMarcacoesExistentes.data, ucrsMarcacoesExistentes.combinacao, ucrsMarcacoesExistentes.hInicio, ucrsMarcacoesExistentes.hFim, ucrsMarcacoesExistentes.site, COUNT(*) as ctn;
				FROM ucrsMarcacoesExistentes;
				GROUP BY ucrsMarcacoesExistentes.data,ucrsMarcacoesExistentes.combinacao, ucrsMarcacoesExistentes.hInicio, ucrsMarcacoesExistentes.hFim, ucrsMarcacoesExistentes.site;
				INTO CURSOR ucrsMarcacoesExistentesContador READWRITE
				****

				Select ucrsRecursosDependencia
				GO Top
				SCAN FOR ucrsRecursosDependencia.stamp == ucrsRecursosCalendarioMensal.stamp; 
					AND ucrsRecursosDependencia.dataInicio <= lcData;
					AND ucrsRecursosDependencia.dataFim >= lcData;
					AND (;
						(uf_marcacoes_DiaNome(lcData) == "SEGUNDA-FEIRA" AND ucrsRecursosDependencia.segunda == .t.);
						OR (uf_marcacoes_DiaNome(lcData) == "TER�A-FEIRA" AND ucrsRecursosDependencia.terca == .t.);
						OR (uf_marcacoes_DiaNome(lcData) == "QUARTA-FEIRA" AND ucrsRecursosDependencia.quarta == .t.);
						OR (uf_marcacoes_DiaNome(lcData) == "QUINTA-FEIRA" AND ucrsRecursosDependencia.quinta == .t.);
						OR (uf_marcacoes_DiaNome(lcData) == "SEXTA-FEIRA" AND ucrsRecursosDependencia.sexta == .t.);
						OR (uf_marcacoes_DiaNome(lcData) == "S�BADO" AND ucrsRecursosDependencia.sabado == .t.);
						OR (uf_marcacoes_DiaNome(lcData) == "DOMINGO" AND ucrsRecursosDependencia.domingo == .t.);
					)
					
					
					
					
					** Verifica se existe limita��o de Referencias na disponibilidade/Series actual
					SELECT ucrsRecursosDependencia
					lcSerieStampDependencia = ucrsRecursosDependencia.seriestamp
					SELECT ucrsRecursosCalendarioMensal
					lcRef = ucrsRecursosCalendarioMensal.ref
					lcRefNaoPermitida = .f.				
					
					** 
					lcTemReferenciasAssociadasDisponibilidade = 0
					SELECT ucrsServicosDisponibilidades
					COUNT FOR ALLTRIM(ucrsServicosDisponibilidades.seriestamp) == ALLTRIM(lcSerieStampDependencia) TO lcTemReferenciasAssociadasDisponibilidade
					IF lcTemReferenciasAssociadasDisponibilidade == 0
						lcRefNaoPermitida = .f.	
					ELSE 
						SELECT ucrsServicosDisponibilidades
						GO Top
						LOCATE FOR ALLTRIM(ucrsServicosDisponibilidades.seriestamp) == ALLTRIM(lcSerieStampDependencia) AND ALLTRIM(ucrsServicosDisponibilidades.ref) == ALLTRIM(lcRef)
						IF !FOUND()	
							lcRefNaoPermitida = .t.	
						ENDIF 
					ENDIF 
					
					IF lcRefNaoPermitida == .f.

					
						*********************
					
						lcMaxHoraInicio = ucrsRecursosDependencia.HoraInicio
						lcMaxHoraFim = ucrsRecursosDependencia.HoraFim
						
						
						
						lcTempoInicio = DATETIME(YEAR(lcData),MONTH(lcData),DAY(lcData),val(LEFT(lcMaxHoraInicio,2)),val(RIGHT(lcMaxHoraInicio,2)),0)
						lcTempoFim = DATETIME(YEAR(lcData),MONTH(lcData),DAY(lcData),val(LEFT(lcMaxHoraFim,2)),val(RIGHT(lcMaxHoraFim,2)),0)
						

						**
						SELECT ucrsRecursosCalendarioMensal
						lcDuracao = ucrsRecursosCalendarioMensal.duracao
						IF EMPTY(lcDuracao) && Se n�a estiver definido atribui dura��o default de 30 min 
							lcDuracao  = 30
							SELECT ucrsRecursosCalendarioMensal
							Replace ucrsRecursosCalendarioMensal.duracao WITH lcDuracao
						ENDIF 
						
						SELECT ucrsRecursosDependencia
						lcUserNo = ucrsRecursosDependencia.no_a	
						lcUserSite = ucrsRecursosDependencia.site_a
						
						** Se tiver definido a dura��o na Diponibilidade, esta prevalece
						IF !EMPTY(ucrsRecursosDependencia.duracao)
							lcDuracao  = ucrsRecursosDependencia.duracao
						ENDIF 
						
						
										
						&&lcPeriodosDisponiveis = (lcTempoFim - lcTempoInicio)/60/lcDuracao
						DO WHILE lcTempoInicio < lcTempoFim
							lcTempoInicio1 = lcTempoInicio  
							lcTempoInicio = lcTempoInicio + (lcDuracao * 60)
							lcTempoFim1 = lcTempoInicio	
							
							
											
							** valida se o periodo esta dentro das indisponibilidades					
							SELECT ucrsIndisponibilidadesUsMarcacoes
							GO Top
							SELECT ucrsIndisponibilidadesUsMarcacoes
							LOCATE FOR;
								ucrsIndisponibilidadesUsMarcacoes.userno == lcUserNo;
								AND ALLTRIM(UPPER(ucrsIndisponibilidadesUsMarcacoes.site)) == ALLTRIM(UPPER(lcUserSite)) ;
								AND ucrsIndisponibilidadesUsMarcacoes.dataInicio <= lcData;
								AND ucrsIndisponibilidadesUsMarcacoes.dataFim >= lcData;
								AND (;
									(uf_marcacoes_DiaNome(lcData) == "SEGUNDA-FEIRA" AND ucrsIndisponibilidadesUsMarcacoes.segunda == .t.);
									OR (uf_marcacoes_DiaNome(lcData) == "TER�A-FEIRA" AND ucrsIndisponibilidadesUsMarcacoes.terca == .t.);
									OR (uf_marcacoes_DiaNome(lcData) == "QUARTA-FEIRA" AND ucrsIndisponibilidadesUsMarcacoes.quarta == .t.);
									OR (uf_marcacoes_DiaNome(lcData) == "QUINTA-FEIRA" AND ucrsIndisponibilidadesUsMarcacoes.quinta == .t.);
									OR (uf_marcacoes_DiaNome(lcData) == "SEXTA-FEIRA" AND ucrsIndisponibilidadesUsMarcacoes.sexta == .t.);
									OR (uf_marcacoes_DiaNome(lcData) == "S�BADO" AND ucrsIndisponibilidadesUsMarcacoes.sabado == .t.);
									OR (uf_marcacoes_DiaNome(lcData) == "DOMINGO" AND ucrsIndisponibilidadesUsMarcacoes.domingo == .t.);
								);
								AND ucrsIndisponibilidadesUsMarcacoes.horainicio <= LEFT(TtoC(lcTempoInicio1,2),5);
								AND ucrsIndisponibilidadesUsMarcacoes.horafim >= LEFT(TtoC(lcTempoFim1,2),5)	
							IF FOUND()

								&& Encontrou indisponibilidade
							ELSE

								** Subtrai numero de Marca��es j� efectuadas para o periodo
								SELECT ucrsMarcacoesExistentesContador
								LOCATE FOR;
									ucrsMarcacoesExistentesContador.data == lcData;
									AND ALLTRIM(ucrsMarcacoesExistentesContador.combinacao) == ALLTRIM(lcCombinacao);
									AND ucrsMarcacoesExistentesContador.hinicio <= LEFT(TtoC(lcTempoInicio1,2),5);
									AND ucrsMarcacoesExistentesContador.hfim >= LEFT(TtoC(lcTempoFim1,2),5);
									AND ALLTRIM(UPPER(ucrsMarcacoesExistentesContador.site)) ==  ALLTRIM(UPPER(lcUserSite))
								IF FOUND()
									lcMrsimultaneo2 = lcMrsimultaneo - ucrsMarcacoesExistentesContador.ctn
								ELSE
									lcMrsimultaneo2 = lcMrsimultaneo	
								ENDIF
								
								FOR lcK = 1 TO lcMrsimultaneo2 
									lcPeriodosDisponiveis  = lcPeriodosDisponiveis +1						
								ENDFOR
							ENDIF
							
						ENDDO 
					
					ENDIF 
					
				ENDSCAN 
				
			ENDIF 
					
			Select ucrsPeriodosCalendarioMensal
			GO Top
			
			
			** Indica��o de Periodos
			IF !EMPTY(lccontrolSource)
			
				IF lcPeriodosDisponiveis> 0 OR lcPeriodosMarcados > 0
				
					lcSessoesMarcadasDia = UF_SERIESSERV_SessoesAtribuidas(lcData,ucrsRecursosCalendarioMensal.descricao, ucrsRecursosCalendarioMensal.local)
				
					Select ucrsRecursosCalendarioMensal
					Replace &lccontrolSource WITH ASTR(lcPeriodosDisponiveis) + lcSessoesMarcadasDia &&"|" + ASTR(lcPeriodosMarcados)
				ELSE
					Select ucrsRecursosCalendarioMensal
					Replace &lccontrolSource WITH ""
				ENDIF
			ENDIF
	
			lcData = lcData + 1
		ENDDO 
		
		Select ucrsRecursosCalendarioMensal
	ENDSCAN 
	
	
	
	Select ucrsRecursosCalendarioMensal
	GO Top
	PLANEAMENTOMR.GridDoc.refresh
	PLANEAMENTOMR.GridDoc.Columns(1).setFocus

ENDFUNC 


** Calcula apenas para o dia
FUNCTION uf_PLANEAMENTOMR_indicadoresCalendarioMensalDia
	LPARAMETERS lcData

	LOCAL lcPeriodosDisponiveis, LCPERIODOSMARCADOS 
	lcPeriodosDisponiveis = 0
	LCPERIODOSMARCADOS = 0
			
	lcNomeCampo = "d" + uf_gerais_getdate(lcData,"SQL")
	lcControlSource = "ucrsRecursosCalendarioMensal." + ALLTRIM(lcNomeCampo)

	lcDateTime = DATETIME(1900,1,1,0,0,1)
	lcDatetimeFim = DATETIME(1900,1,1,23,59,59)

	Select ucrsPeriodosCalendarioMensal
	GO Top
	SCAN
		DELETE 
	ENDSCAN 
	
	SELECT ucrsRecursosCalendarioMensal
	lcMrsimultaneo = IIF(EMPTY(ucrsRecursosCalendarioMensal.mrsimultaneo),1,ucrsRecursosCalendarioMensal.mrsimultaneo)
	lcCombinacao = ALLTRIM(ucrsRecursosCalendarioMensal.descricao)
				
	IF lcData >= DATE() 


		** Cursor que contem numero de marca��es Efetuadas por Periodo	
		SELECT ucrsMarcacoesExistentes.data, ucrsMarcacoesExistentes.combinacao, ucrsMarcacoesExistentes.hInicio, ucrsMarcacoesExistentes.hFim, ucrsMarcacoesExistentes.site, COUNT(*) as ctn;
		FROM ucrsMarcacoesExistentes;
		GROUP BY ucrsMarcacoesExistentes.data,ucrsMarcacoesExistentes.combinacao, ucrsMarcacoesExistentes.hInicio, ucrsMarcacoesExistentes.hFim, ucrsMarcacoesExistentes.site;
		INTO CURSOR ucrsMarcacoesExistentesContador READWRITE
		****

		Select ucrsRecursosDependencia
		GO Top
		SCAN FOR ucrsRecursosDependencia.stamp == ucrsRecursosCalendarioMensal.stamp; 
			AND ucrsRecursosDependencia.dataInicio <= lcData;
			AND ucrsRecursosDependencia.dataFim >= lcData;
			AND (;
				(uf_marcacoes_DiaNome(lcData) == "SEGUNDA-FEIRA" AND ucrsRecursosDependencia.segunda == .t.);
				OR (uf_marcacoes_DiaNome(lcData) == "TER�A-FEIRA" AND ucrsRecursosDependencia.terca == .t.);
				OR (uf_marcacoes_DiaNome(lcData) == "QUARTA-FEIRA" AND ucrsRecursosDependencia.quarta == .t.);
				OR (uf_marcacoes_DiaNome(lcData) == "QUINTA-FEIRA" AND ucrsRecursosDependencia.quinta == .t.);
				OR (uf_marcacoes_DiaNome(lcData) == "SEXTA-FEIRA" AND ucrsRecursosDependencia.sexta == .t.);
				OR (uf_marcacoes_DiaNome(lcData) == "S�BADO" AND ucrsRecursosDependencia.sabado == .t.);
				OR (uf_marcacoes_DiaNome(lcData) == "DOMINGO" AND ucrsRecursosDependencia.domingo == .t.);
			)
			
			** Verifica se existe limita��o de Referencias na disponibilidade/Series actual
			SELECT ucrsRecursosDependencia
			lcSerieStampDependencia = ucrsRecursosDependencia.seriestamp
			SELECT ucrsRecursosCalendarioMensal
			lcRef = ucrsRecursosCalendarioMensal.ref
			lcRefNaoPermitida = .f.				
			
			** 
			lcTemReferenciasAssociadasDisponibilidade = 0
			SELECT ucrsServicosDisponibilidades
			COUNT FOR ALLTRIM(ucrsServicosDisponibilidades.seriestamp) == ALLTRIM(lcSerieStampDependencia) TO lcTemReferenciasAssociadasDisponibilidade
			IF lcTemReferenciasAssociadasDisponibilidade == 0
				lcRefNaoPermitida = .f.	
			ELSE 
				SELECT ucrsServicosDisponibilidades
				GO Top
				LOCATE FOR ALLTRIM(ucrsServicosDisponibilidades.seriestamp) == ALLTRIM(lcSerieStampDependencia) AND ALLTRIM(ucrsServicosDisponibilidades.ref) == ALLTRIM(lcRef)
				IF !FOUND()	
					lcRefNaoPermitida = .t.	
				ENDIF 
			ENDIF 
			
			IF lcRefNaoPermitida == .f.
			
				lcMaxHoraInicio = ucrsRecursosDependencia.HoraInicio
				lcMaxHoraFim = ucrsRecursosDependencia.HoraFim
				
				
				
				lcTempoInicio = DATETIME(YEAR(lcData),MONTH(lcData),DAY(lcData),val(LEFT(lcMaxHoraInicio,2)),val(RIGHT(lcMaxHoraInicio,2)),0)
				lcTempoFim = DATETIME(YEAR(lcData),MONTH(lcData),DAY(lcData),val(LEFT(lcMaxHoraFim,2)),val(RIGHT(lcMaxHoraFim,2)),0)
				

				**
				SELECT ucrsRecursosCalendarioMensal
				lcDuracao = ucrsRecursosCalendarioMensal.duracao
				IF EMPTY(lcDuracao) && Se n�a estiver definido atribui dura��o default de 30 min 
					lcDuracao  = 30
					SELECT ucrsRecursosCalendarioMensal
					Replace ucrsRecursosCalendarioMensal.duracao WITH lcDuracao
				ENDIF 
				
				SELECT ucrsRecursosDependencia
				lcUserNo = ucrsRecursosDependencia.no_a	
				lcUserSite = ucrsRecursosDependencia.site_a
				
				** Se tiver definido a dura��o na Diponibilidade, esta prevalece
				IF !EMPTY(ucrsRecursosDependencia.duracao)
					lcDuracao  = ucrsRecursosDependencia.duracao
				ENDIF 
				
				
								
				&&lcPeriodosDisponiveis = (lcTempoFim - lcTempoInicio)/60/lcDuracao
				DO WHILE lcTempoInicio < lcTempoFim
					lcTempoInicio1 = lcTempoInicio  
					lcTempoInicio = lcTempoInicio + (lcDuracao * 60)
					lcTempoFim1 = lcTempoInicio	
					
					
									
					** valida se o periodo esta dentro das indisponibilidades					
					SELECT ucrsIndisponibilidadesUsMarcacoes
					GO Top
					SELECT ucrsIndisponibilidadesUsMarcacoes
					LOCATE FOR;
						ucrsIndisponibilidadesUsMarcacoes.userno == lcUserNo;
						AND ALLTRIM(UPPER(ucrsIndisponibilidadesUsMarcacoes.site)) == ALLTRIM(UPPER(lcUserSite)) ;
						AND ucrsIndisponibilidadesUsMarcacoes.dataInicio <= lcData;
						AND ucrsIndisponibilidadesUsMarcacoes.dataFim >= lcData;
						AND (;
							(uf_marcacoes_DiaNome(lcData) == "SEGUNDA-FEIRA" AND ucrsIndisponibilidadesUsMarcacoes.segunda == .t.);
							OR (uf_marcacoes_DiaNome(lcData) == "TER�A-FEIRA" AND ucrsIndisponibilidadesUsMarcacoes.terca == .t.);
							OR (uf_marcacoes_DiaNome(lcData) == "QUARTA-FEIRA" AND ucrsIndisponibilidadesUsMarcacoes.quarta == .t.);
							OR (uf_marcacoes_DiaNome(lcData) == "QUINTA-FEIRA" AND ucrsIndisponibilidadesUsMarcacoes.quinta == .t.);
							OR (uf_marcacoes_DiaNome(lcData) == "SEXTA-FEIRA" AND ucrsIndisponibilidadesUsMarcacoes.sexta == .t.);
							OR (uf_marcacoes_DiaNome(lcData) == "S�BADO" AND ucrsIndisponibilidadesUsMarcacoes.sabado == .t.);
							OR (uf_marcacoes_DiaNome(lcData) == "DOMINGO" AND ucrsIndisponibilidadesUsMarcacoes.domingo == .t.);
						);
						AND ucrsIndisponibilidadesUsMarcacoes.horainicio <= LEFT(TtoC(lcTempoInicio1,2),5);
						AND ucrsIndisponibilidadesUsMarcacoes.horafim >= LEFT(TtoC(lcTempoFim1,2),5)	
					IF FOUND()

						&& Encontrou indisponibilidade
					ELSE

						** Subtrai numero de Marca��es j� efectuadas para o periodo
						SELECT ucrsMarcacoesExistentesContador
						LOCATE FOR;
							ucrsMarcacoesExistentesContador.data == lcData;
							AND ALLTRIM(ucrsMarcacoesExistentesContador.combinacao) == ALLTRIM(lcCombinacao);
							AND ucrsMarcacoesExistentesContador.hinicio <= LEFT(TtoC(lcTempoInicio1,2),5);
							AND ucrsMarcacoesExistentesContador.hfim >= LEFT(TtoC(lcTempoFim1,2),5);
							AND ALLTRIM(UPPER(ucrsMarcacoesExistentesContador.site)) ==  ALLTRIM(UPPER(lcUserSite))
						IF FOUND()
							lcMrsimultaneo2 = lcMrsimultaneo - ucrsMarcacoesExistentesContador.ctn
						ELSE
							lcMrsimultaneo2 = lcMrsimultaneo	
						ENDIF
						
						FOR lcK = 1 TO lcMrsimultaneo2 
							lcPeriodosDisponiveis  = lcPeriodosDisponiveis +1						
						ENDFOR
					ENDIF
					
				ENDDO 

			ENDIF 

		ENDSCAN 
		
	ENDIF 
			
	Select ucrsPeriodosCalendarioMensal
	GO Top
	
	
	** Indica��o de Periodos
	IF !EMPTY(lccontrolSource)
	
		IF lcPeriodosDisponiveis> 0 OR lcPeriodosMarcados > 0
		
			lcSessoesMarcadasDia = UF_PLANEAMENTOMR_SessoesAtribuidas(lcData,ucrsRecursosCalendarioMensal.descricao, ucrsRecursosCalendarioMensal.local)
		
	MESSAGEBOX(lcSessoesMarcadasDia)
		
			Select ucrsRecursosCalendarioMensal
			Replace &lccontrolSource WITH ASTR(lcPeriodosDisponiveis) + lcSessoesMarcadasDia &&"|" + ASTR(lcPeriodosMarcados)
		ELSE
			Select ucrsRecursosCalendarioMensal
			Replace &lccontrolSource WITH ""
		ENDIF
	ENDIF
	
ENDFUNC 


**
FUNCTION UF_PLANEAMENTOMR_SessoesAtribuidas
	LPARAMETERS lcData, lcCombinacao, lcLocal
	** 
	LOCAL lctxtSessoes
	lctxtSessoes = ""
	
	********************************
	Select ucrsSeriesServ
	** 
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_marcacoes_sessoes <<ucrsSeriesServ.serieno>>,'<<lcLocal>>'
	ENDTEXT	
	
	IF !uf_gerais_actgrelha("", "ucrsSessoesSerieLocal", lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR AS SESS�ES ASSOCIADAS � S�RIE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	*************************************
	
	** Dias com sessoes atribuidas
	Select ucrsSessoesSerieLocal
	GO Top
	SCAN FOR ucrsSessoesSerieLocal.ano == YEAR(lcData) AND ucrsSessoesSerieLocal.mes == MONTH(lcData) AND ucrsSessoesSerieLocal.dia == DAY(lcData);
		AND ALLTRIM(ucrsSessoesSerieLocal.combinacao) == ALLTRIM(lcCombinacao)
				
		IF !EMPTY(lctxtSessoes)
			lctxtSessoes = lctxtSessoes + ","
		ENDIF 		
		lctxtSessoes = lctxtSessoes +  ASTR(ucrsSessoesSerieLocal.Sessao)
	ENDSCAN 
	
	IF !EMPTY(lctxtSessoes)
		lctxtSessoes = "(" + lctxtSessoes + ")"
	ENDIF
	
	RETURN lctxtSessoes

ENDFUNC


**
FUNCTION uf_PLANEAMENTOMR_calendarioMensalAfterRowColumnChange
	LOCAL lccontrolSource, lcPeriodosDisponiveis, lcPeriodosMarcados 
	PUBLIC myDataPlaneamento
	
*!*		IF EMPTY(SERIESSERV.ano) OR EMPTY(SERIESSERV.mes) &&OR EMPTY(SERIESSERV.mes)
*!*			RETURN .f.
*!*		ENDIF 

	Select ucrsRecursosCalendarioMensal	
	PLANEAMENTOMR.stampPlaneamento = ucrsRecursosCalendarioMensal.stamp
	
	Select ucrsRecursosCalendarioMensal	
	PLANEAMENTOMR.ContainerDetalhe.PageFrame1.page1.combinacao.controlsource = "ucrsRecursosCalendarioMensal.design"
	PLANEAMENTOMR.ContainerDetalhe.PageFrame1.page1.nomeEspecialista.controlsource = "ucrsRecursosCalendarioMensal.nomeEspecialista"
	PLANEAMENTOMR.ContainerDetalhe.PageFrame1.page1.local.controlsource = "ucrsRecursosCalendarioMensal.local"
	PLANEAMENTOMR.ContainerDetalhe.PageFrame1.page1.pvp.controlsource = "ucrsRecursosCalendarioMensal.pvp"
	PLANEAMENTOMR.ContainerDetalhe.PageFrame1.page1.comp.controlsource = "ucrsRecursosCalendarioMensal.compart"
	PLANEAMENTOMR.ContainerDetalhe.PageFrame1.page1.utente.controlsource = "ucrsRecursosCalendarioMensal.utente"
	
	
	** 
	** Seleciona a celula escolhida
	lccontrolSource = ""	
	WITH PLANEAMENTOMR.GridDoc
		FOR i=1 TO .ColumnCount

			IF .columns(i).controlSource != "ucrsRecursosCalendarioMensal.nomeEspecialista" AND .columns(i).controlSource != "ucrsRecursosCalendarioMensal.local"
				lcAno = VAL(LEFT(RIGHT(.columns(i).controlSource,8),4))
				lcMes = VAL(RIGHT(LEFT(RIGHT(.columns(i).controlSource,8),6),2))
				lcDia = VAL(RIGHT(RIGHT(.columns(i).controlSource,8),2))
				lcData = DATE(lcAno,lcMes,lcDia)
			ENDIF 
								
			If i == .ActiveColumn 
				
				IF .columns(i).controlSource == "ucrsRecursosCalendarioMensal.nomeEspecialista" OR .columns(i).controlSource == "ucrsRecursosCalendarioMensal.local"
					.columns(i).dynamicBackColor="IIF(recno()%2!=0, Rgb[255,255,255], rgb[245,245,245])"
				ELSE
					&& Sabado, Domingo
					IF CDOW(lcData) == "Sunday" OR CDOW(lcData) == "Saturday"
						.columns(i).dynamicBackColor="IIF(recno()== "+ ASTR(.ActiveRow) +",rgb[42,143,154],IIF(recno()%2!=0, Rgb[250,247,231], Rgb[243,235,197]))"
					ELSE
						.columns(i).dynamicBackColor="IIF(recno()== "+ ASTR(.ActiveRow) +",rgb[42,143,154],IIF(recno()%2!=0, Rgb[255,255,255], rgb[245,245,245]))"
					ENDIF 
				ENDIF
				
				lccontrolSource = .columns(i).controlSource 
			ELSE
				IF .columns(i).controlSource == "ucrsRecursosCalendarioMensal.nomeEspecialista" OR .columns(i).controlSource == "ucrsRecursosCalendarioMensal.local"
					.columns(i).dynamicBackColor="IIF(recno()%2!=0, Rgb[255,255,255], rgb[245,245,245])"
				ELSE
				
					&& Sabado, Domingo
					IF CDOW(lcData) == "Sunday" OR CDOW(lcData) == "Saturday"
						.columns(i).dynamicBackColor="IIF(recno()%2!=0, Rgb[250,247,231], Rgb[243,235,197])"
					ELSE
						.columns(i).dynamicBackColor="IIF(recno()%2!=0, Rgb[255,255,255], rgb[245,245,245])"
					ENDIF 
				
				ENDIF
			ENDIF 
		ENDFOR 
	ENDWITH
	 
	** Atribui data Selecionada
	IF ALLTRIM(lccontrolSource) == "ucrsRecursosCalendarioMensal.nomeEspecialista" OR ALLTRIM(lccontrolSource) == "ucrsRecursosCalendarioMensal.local"
		PLANEAMENTOMR.ContainerDetalhe.PageFrame1.page2.Container1.dataSelecionada.caption = ""
		PLANEAMENTOMR.ContainerDetalhe.PageFrame1.page2.Container1.dataSelecionada.visible = .f.
		PLANEAMENTOMR.ano = "1900"
		PLANEAMENTOMR.mes = "1"
		PLANEAMENTOMR.dia = "1"
	ELSE
		lccontrolSource2 = RIGHT(lccontrolSource,8)
		PLANEAMENTOMR.ano = LEFT(RIGHT(lccontrolSource2,8),4)
		PLANEAMENTOMR.mes = RIGHT(LEFT(RIGHT(lccontrolSource2,8),6),2)
		PLANEAMENTOMR.dia = RIGHT(RIGHT(lccontrolSource2,8),2)
		PLANEAMENTOMR.ContainerDetalhe.PageFrame1.page2.Container1.dataSelecionada.caption = "Data: " + PLANEAMENTOMR.ano + "." + PLANEAMENTOMR.mes + "." + PLANEAMENTOMR.dia
		PLANEAMENTOMR.ContainerDetalhe.PageFrame1.page2.Container1.dataSelecionada.visible = .t.
	ENDIF 
	
	SET HOURS TO 24
	
	IF !USED("ucrsPeriodosCalendarioMensal")
		CREATE CURSOR ucrsPeriodosCalendarioMensal(posicao n(9,0), periodo n(9,0), dataIni d,dataFim d,horaIni c(5),horaFim c(5),nome c(150), sel l, duracao n(8,0), extra l, marcado l)
	ELSE
		SELECT ucrsPeriodosCalendarioMensal
		Go Top
		SCAN 
			DELETE 
		ENDSCAN 
	ENDIF

	IF !EMPTY(PLANEAMENTOMR.ano) AND !EMPTY(PLANEAMENTOMR.mes) AND !EMPTY(PLANEAMENTOMR.dia)
		lcData = DATE(VAL(PLANEAMENTOMR.ano),VAL(PLANEAMENTOMR.mes),VAL(PLANEAMENTOMR.dia))
		myDataPlaneamento = lcData 
	ELSE
		lcData = myDataPlaneamento 
	ENDIF
	
	
	** Calcula marca��es efecutadas para os dias de analise, para os recursos
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_existentes '<<uf_gerais_getdate(lcData,"SQL")>>','<<uf_gerais_getdate(lcData,"SQL")>>'
	ENDTEXT 
	If !uf_gerais_actGrelha("", "ucrsMarcacoesExistentes", lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar dados dos recursos. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF

	** Adiciona Marca��es Efectuadas para combina��o selecionada
	lcCombinacao = ALLTRIM(ucrsRecursosCalendarioMensal.descricao)
	lcMrsimultaneo = IIF(EMPTY(ucrsRecursosCalendarioMensal.mrsimultaneo),1,ucrsRecursosCalendarioMensal.mrsimultaneo)

	SELECT ucrsRecursosCalendarioMensal
	IF EMPTY(ucrsRecursosCalendarioMensal.duracao)
		lcDuracao = 30
	ELSE
		lcDuracao = ucrsRecursosCalendarioMensal.duracao
	ENDIF 	

	
	Select ucrsMarcacoesExistentes
	SCAN FOR ucrsMarcacoesExistentes.data == lcData;
		AND ALLTRIM(ucrsMarcacoesExistentes.combinacao) == ALLTRIM(lcCombinacao);
		AND UPPER(ALLTRIM(ucrsMarcacoesExistentes.site)) == UPPER(ALLTRIM(ucrsRecursosCalendarioMensal.local))
	
		
		Select ucrsPeriodosCalendarioMensal
		COUNT to lcPosicao

		Select ucrsPeriodosCalendarioMensal
		APPEND blank
		replace ucrsPeriodosCalendarioMensal.posicao WITH lcPosicao+1
		replace ucrsPeriodosCalendarioMensal.dataIni WITH lcData
		replace ucrsPeriodosCalendarioMensal.dataFim WITH lcData
		replace ucrsPeriodosCalendarioMensal.horaIni WITH ucrsMarcacoesExistentes.hinicio
		replace ucrsPeriodosCalendarioMensal.horaFim WITH ucrsMarcacoesExistentes.hfim
		replace ucrsPeriodosCalendarioMensal.nome WITH ucrsMarcacoesExistentes.nome
		replace ucrsPeriodosCalendarioMensal.duracao WITH ucrsMarcacoesExistentes.duracao
		replace ucrsPeriodosCalendarioMensal.marcado WITH .t.



	ENDSCAN 
											
	*************

	IF lcData >= DATE()	&& S� calcula periodos disponiveis para datas superiores � actual
		lcDateTime = DATETIME(YEAR(lcData),MONTH(lcData),DAY(lcData),0,0,1)
		lcDatetimeFim = DATETIME(YEAR(lcData),MONTH(lcData),DAY(lcData),23,59,59)

		** Cursor que contem numero de marca��es Efetuadas por Periodo	
		SELECT ucrsMarcacoesExistentes.data, ucrsMarcacoesExistentes.combinacao, ucrsMarcacoesExistentes.hInicio, ucrsMarcacoesExistentes.hFim, ucrsMarcacoesExistentes.site, COUNT(*) as ctn;
		FROM ucrsMarcacoesExistentes;
		GROUP BY ucrsMarcacoesExistentes.data,ucrsMarcacoesExistentes.combinacao, ucrsMarcacoesExistentes.hInicio, ucrsMarcacoesExistentes.hFim, ucrsMarcacoesExistentes.site;
		INTO CURSOR ucrsMarcacoesExistentesContador READWRITE
		****

		Select ucrsRecursosDependencia
		GO Top
		SCAN FOR ucrsRecursosDependencia.stamp == ucrsRecursosCalendarioMensal.stamp; 
			AND ucrsRecursosDependencia.dataInicio <= lcData;
			AND ucrsRecursosDependencia.dataFim >= lcData;
			AND (;
				(uf_marcacoes_DiaNome(lcData) == "SEGUNDA-FEIRA" AND ucrsRecursosDependencia.segunda == .t.);
				OR (uf_marcacoes_DiaNome(lcData) == "TER�A-FEIRA" AND ucrsRecursosDependencia.terca == .t.);
				OR (uf_marcacoes_DiaNome(lcData) == "QUARTA-FEIRA" AND ucrsRecursosDependencia.quarta == .t.);
				OR (uf_marcacoes_DiaNome(lcData) == "QUINTA-FEIRA" AND ucrsRecursosDependencia.quinta == .t.);
				OR (uf_marcacoes_DiaNome(lcData) == "SEXTA-FEIRA" AND ucrsRecursosDependencia.sexta == .t.);
				OR (uf_marcacoes_DiaNome(lcData) == "S�BADO" AND ucrsRecursosDependencia.sabado == .t.);
				OR (uf_marcacoes_DiaNome(lcData) == "DOMINGO" AND ucrsRecursosDependencia.domingo == .t.);
			)
	
			
			** Verifica se existe limita��o de Referencias na disponibilidade/Series actual
			SELECT ucrsRecursosDependencia
			lcSerieStampDependencia = ucrsRecursosDependencia.seriestamp
			SELECT ucrsRecursosCalendarioMensal
			lcRef = ucrsRecursosCalendarioMensal.ref
			lcRefNaoPermitida = .f.				
			
			** 
			lcTemReferenciasAssociadasDisponibilidade = 0
			SELECT ucrsServicosDisponibilidades
			COUNT FOR ALLTRIM(ucrsServicosDisponibilidades.seriestamp) == ALLTRIM(lcSerieStampDependencia) TO lcTemReferenciasAssociadasDisponibilidade
			IF lcTemReferenciasAssociadasDisponibilidade == 0
				lcRefNaoPermitida = .f.	
			ELSE 
				SELECT ucrsServicosDisponibilidades
				GO Top
				LOCATE FOR ALLTRIM(ucrsServicosDisponibilidades.seriestamp) == ALLTRIM(lcSerieStampDependencia) AND ALLTRIM(ucrsServicosDisponibilidades.ref) == ALLTRIM(lcRef)
				IF !FOUND()	
					lcRefNaoPermitida = .t.	
				ENDIF 
			ENDIF 
			
			IF lcRefNaoPermitida == .f.
			
				lcMaxHoraInicio = ucrsRecursosDependencia.HoraInicio
				lcMaxHoraFim = ucrsRecursosDependencia.HoraFim
					
				lcDataPermitida = .f.
				
				lcTempoInicio = DATETIME(YEAR(lcData),MONTH(lcData),DAY(lcData),val(LEFT(lcMaxHoraInicio,2)),val(RIGHT(lcMaxHoraInicio,2)),0)
				lcTempoFim = DATETIME(YEAR(lcData),MONTH(lcData),DAY(lcData),val(LEFT(lcMaxHoraFim,2)),val(RIGHT(lcMaxHoraFim,2)),0)
				
				
				SELECT ucrsRecursosDependencia
				lcUserNo = ucrsRecursosDependencia.no_a	
				lcUserSite = ucrsRecursosDependencia.site_a	
					
				** Se tiver definido a dura��o na Diponibilidade, esta prevalece
				IF !EMPTY(ucrsRecursosDependencia.duracao)
					lcDuracao  = ucrsRecursosDependencia.duracao
				ENDIF 
					
				&&lcPeriodosDisponiveis = (lcTempoFim - lcTempoInicio)/60/lcDuracao
				DO WHILE lcTempoInicio < lcTempoFim
					lcTempoInicio1 = lcTempoInicio  
					lcTempoInicio = lcTempoInicio + (lcDuracao * 60)
					lcTempoFim1 = lcTempoInicio	
					
					
					** valida se o periodo esta dentro das indisponibilidades					
					SELECT ucrsIndisponibilidadesUsMarcacoes
					GO Top
					SELECT ucrsIndisponibilidadesUsMarcacoes
					LOCATE FOR;
						ucrsIndisponibilidadesUsMarcacoes.userno == lcUserNo;
						AND ALLTRIM(UPPER(ucrsIndisponibilidadesUsMarcacoes.site)) == ALLTRIM(UPPER(lcUserSite)) ;
						AND ucrsIndisponibilidadesUsMarcacoes.dataInicio <= lcData;
						AND ucrsIndisponibilidadesUsMarcacoes.dataFim >= lcData;
						AND (;
							(uf_marcacoes_DiaNome(lcData) == "SEGUNDA-FEIRA" AND ucrsIndisponibilidadesUsMarcacoes.segunda == .t.);
							OR (uf_marcacoes_DiaNome(lcData) == "TER�A-FEIRA" AND ucrsIndisponibilidadesUsMarcacoes.terca == .t.);
							OR (uf_marcacoes_DiaNome(lcData) == "QUARTA-FEIRA" AND ucrsIndisponibilidadesUsMarcacoes.quarta == .t.);
							OR (uf_marcacoes_DiaNome(lcData) == "QUINTA-FEIRA" AND ucrsIndisponibilidadesUsMarcacoes.quinta == .t.);
							OR (uf_marcacoes_DiaNome(lcData) == "SEXTA-FEIRA" AND ucrsIndisponibilidadesUsMarcacoes.sexta == .t.);
							OR (uf_marcacoes_DiaNome(lcData) == "S�BADO" AND ucrsIndisponibilidadesUsMarcacoes.sabado == .t.);
							OR (uf_marcacoes_DiaNome(lcData) == "DOMINGO" AND ucrsIndisponibilidadesUsMarcacoes.domingo == .t.);
						);
						AND ucrsIndisponibilidadesUsMarcacoes.horainicio <= LEFT(TtoC(lcTempoInicio1,2),5);
						AND ucrsIndisponibilidadesUsMarcacoes.horafim >= LEFT(TtoC(lcTempoFim1,2),5)	
					IF FOUND()

						&& Encontrou indisponibilidade
					ELSE
					
						** Subtrai numero de Marca��es j� efectuadas para o periodo
						SELECT ucrsMarcacoesExistentesContador
						LOCATE FOR;
							ucrsMarcacoesExistentesContador.data == lcData;
							AND ALLTRIM(ucrsMarcacoesExistentesContador.combinacao) == ALLTRIM(lcCombinacao);
							AND ucrsMarcacoesExistentesContador.hinicio <= LEFT(TtoC(lcTempoInicio1,2),5);
							AND ucrsMarcacoesExistentesContador.hfim >= LEFT(TtoC(lcTempoFim1,2),5);
							AND ALLTRIM(UPPER(ucrsMarcacoesExistentesContador.site)) == ALLTRIM(UPPER(lcUserSite))
						IF FOUND()
							lcMrsimultaneo2 = lcMrsimultaneo - ucrsMarcacoesExistentesContador.ctn
						ELSE
							lcMrsimultaneo2 = lcMrsimultaneo	
						ENDIF
						
						
						Select ucrsPeriodosCalendarioMensal
						CALCULATE MAX(ucrsPeriodosCalendarioMensal.periodo) TO lcPeriodo
						lcPeriodo = lcPeriodo + 1 
						
						
							
							&& Repete o numero de marcacoes Simultaneas 
							FOR lcK = 1 TO lcMrsimultaneo2 
							
								Select ucrsPeriodosCalendarioMensal
								COUNT to lcPosicao
								
								Select ucrsPeriodosCalendarioMensal
								APPEND blank
								Replace ucrsPeriodosCalendarioMensal.periodo WITH lcPeriodo
								replace ucrsPeriodosCalendarioMensal.posicao WITH lcPosicao+1
								replace ucrsPeriodosCalendarioMensal.dataIni WITH lcData
								replace ucrsPeriodosCalendarioMensal.dataFim WITH lcData
								replace ucrsPeriodosCalendarioMensal.horaIni WITH LEFT(TtoC(lcTempoInicio1,2),5)
								replace ucrsPeriodosCalendarioMensal.horaFim WITH LEFT(TtoC(lcTempoFim1,2),5)
								replace ucrsPeriodosCalendarioMensal.duracao WITH lcDuracao
							ENDFOR 
						ENDIF

				ENDDO 
			
			ENDIF 
			
		ENDSCAN 

		
	ENDIF
	
	Select ucrsPeriodosCalendarioMensal
	GO Top

	**
	lcContador = 0
	select ucrsPeriodosCalendarioMensal
	Go Top
	SCAN 
		lcContador = lcContador +1
		replace ucrsPeriodosCalendarioMensal.periodo with lcContador
		replace ucrsPeriodosCalendarioMensal.posicao with lcContador
	ENDSCAN 
	
	Select ucrsPeriodosCalendarioMensal
	GO Top
	
	** Ordernar Periodos
	Select ucrsPeriodosCalendarioMensal
	Index on horaIni + horaFim Tag "OP"
	SET COLLATE TO "GENERAL"
	
	
	PLANEAMENTOMR.ContainerDetalhe.Pageframe1.Page2.Container1.GridPesq.refresh
	
	
	uf_PLANEAMENTOMR_dadosComparticipacao()


ENDFUNC 


**
FUNCTION UF_PLANEAMENTOMR_afterRowColumnChangeServicos
	
	Select ucrsServicosSerie
	Select uCrsRecursosDaSerie
	GO Top
	SET FILTER TO UPPER(ALLTRIM(uCrsRecursosDaSerie.ref)) == ALLTRIM(UPPER(ucrsServicosSerie.ref))

	Select uCrsSeriesConsumo
	GO Top
	SET FILTER TO UPPER(ALLTRIM(uCrsSeriesConsumo.reforiginal)) == ALLTRIM(UPPER(ucrsServicosSerie.ref))

	PLANEAMENTOMR.ContainerDetalhe.Pageframe1.Page3.lblRecursos.caption = "Recursos(" + ALLTRIM(ucrsServicosSerie.design)+ ")"
	PLANEAMENTOMR.ContainerDetalhe.Pageframe1.Page3.labelConsumos.caption = "Consumos(" + ALLTRIM(ucrsServicosSerie.design)+ ")"
	PLANEAMENTOMR.ContainerDetalhe.Pageframe1.Page3.refresh
ENDFUNC 


**
FUNCTION uf_PLANEAMENTOMR_calendarioMensalBackColor

	DO CASE
		CASE recno()%2!=0
			RETURN Rgb[255,255,255]
		OTHERWISE 
			RETURN rgb[240,240,240]
	ENDCASE

ENDFUNC 


** 
FUNCTION uf_PLANEAMENTOMR_novopedidoimprevisto

	LOCAL lcPosicao, lcData 
	
	lcData = DATE(VAL(PLANEAMENTOMR.ano),VAL(PLANEAMENTOMR.mes),VAL(PLANEAMENTOMR.dia))

	Select ucrsPeriodosCalendarioMensal
	Calculate Max(posicao) to lcPosicao
	
	Select ucrsPeriodosCalendarioMensal
	Calculate Max(Horafim) to lcHinicio

	** Dura��o de servi�os ***************************************
	lcDuracao = 30
	
	lcTempoInicio = DATETIME(YEAR(lcData),MONTH(lcData),DAY(lcData),val(LEFT(lcHinicio,2)),val(RIGHT(lcHinicio,2)),0)
	lcHfim = lcTempoInicio + (lcDuracao * 60)
					
	Select ucrsPeriodosCalendarioMensal
	APPEND blank
	replace ucrsPeriodosCalendarioMensal.posicao WITH lcPosicao+1
	replace ucrsPeriodosCalendarioMensal.periodo WITH lcPosicao+1
	replace ucrsPeriodosCalendarioMensal.dataIni WITH lcData
	replace ucrsPeriodosCalendarioMensal.dataFim WITH lcData
	replace ucrsPeriodosCalendarioMensal.horaIni WITH lcHinicio
	replace ucrsPeriodosCalendarioMensal.horaFim WITH LEFT(TtoC(lcHfim,2),5)
	replace ucrsPeriodosCalendarioMensal.extra WITH .t.

	SELECT ucrsPeriodosCalendarioMensal
	GO BOTTOM 
	PLANEAMENTOMR.ContainerDetalhe.Pageframe1.Page2.Container1.GridPesq.refresh

ENDFUNC 
 

**
FUNCTION uf_PLANEAMENTOMR_EliminaSessao

	**
	IF !uf_gerais_validaPermUser(m.ch_userno, m.ch_grupo, 'Marca��es - Eliminar')
		uf_perguntalt_chama("O seu perfil n�o permite eliminar marca��es.", "OK", "", 64)
		RETURN	.f.
	ENDIF
	**


	**
	Select ucrsSessoesSerie
	IF !EMPTY(ucrsSessoesSerie.mrstamp)

		IF !uf_perguntalt_chama("Vai eliminar a Sess�o. Pretende continuar?", "Sim", "N�o", 32)
			RETURN .f.
		ENDIF
		
		LOCAL lcSQL
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			DELETE FROM marcacoes WHERE mrstamp = '<<ALLTRIM(ucrsSessoesSerie.mrstamp)>>'
			DELETE FROM marcacoesServ WHERE mrstamp = '<<ALLTRIM(ucrsSessoesSerie.mrstamp)>>'
			DELETE FROM cpt_marcacoes WHERE mrstamp = '<<ALLTRIM(ucrsSessoesSerie.mrstamp)>>'
		ENDTEXT
		IF !uf_gerais_actGrelha("", "", lcSQL)
			MESSAGEBOX("N�o foi possivel eliminar a Sessao. Por favor contacte o suporte",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ELSE
			uf_perguntalt_chama("Sess�o eliminada com sucesso!", "", "OK", 64)
		ENDIF


		**
		Select ucrsSeriesServ
		** 
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_marcacoes_sessoes <<ucrsSeriesServ.serieno>>
		ENDTEXT	
		IF !uf_gerais_actgrelha("PLANEAMENTOMR.ContainerDetalhe.Pageframe1.Page2.Container1.GridSessoes", "ucrsSessoesSerie", lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A VERIFICAR AS SESS�ES ASSOCIADAS � S�RIE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
		SELECT ucrsSessoesSerie
		GO Top
		
		IF EMPTY(myDataPlaneamento)
			lcData = DATE()
		ELSE
			lcData = myDataPlaneamento
		ENDIF 		
		uf_PLANEAMENTOMR_indicadoresCalendarioMensalDia(lcData)
		uf_PLANEAMENTOMR_calendarioMensalAfterRowColumnChange() && Actualiza Periodos
		
	ENDIF 

ENDFUNC  


**
FUNCTION uf_PLANEAMENTOMR_gravar
	
	
	**
	IF !uf_gerais_validaPermUser(m.ch_userno, m.ch_grupo, 'Marca��es - Introduzir')
		uf_perguntalt_chama("O seu perfil n�o permite introduzir marca��es.", "OK", "", 64)
		RETURN	.f.
	ENDIF
	**
	
	IF EMPTY(PLANEAMENTOMR.stampPlaneamento)
		RETURN .f.
	ENDIF 
	
	SELECT ucrsRecursosCalendarioMensal
	LOCATE FOR ucrsRecursosCalendarioMensal.stamp == PLANEAMENTOMR.stampPlaneamento
	IF !FOUND()
		RETURN .f.
	ENDIF 
	SELECT ucrsRecursosCalendarioMensal
	STORE .f. TO lcCamposObrigatorios, lcValidaInsert, lcValidaUpdate, lcActualizaRecursos, lcValidaSequenciaPeriodos 
	
	** Regras grava��o S�rie
	lcCamposObrigatorios = uf_PLANEAMENTOMR_camposObrigatorios()
	IF lcCamposObrigatorios==.f.
		RETURN .f.
	ENDIF

	** Trata Transac��es para garantir a Integridade de Dados
	IF uf_gerais_actGrelha("", "", [BEGIN TRANSACTION])

		SELECT ucrsSeriesServ
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
	
			IF (select count(seriestamp) from b_series where b_series.seriestamp = '<<ALLTRIM(ucrsSeriesServ.seriestamp)>>')=0 
			BEGIN
				INSERT INTO	b_series (	
						seriestamp
						,serieno
						,serienome
						,nome
						,no
						,estab
						,dirClinico
						,dirServico
						,dataInicio
						,sessoes
						,ousrinis
						,ousrdata
						,ousrhora
						,usrinis
						,usrdata
						,usrhora
						,horaInicio
						,horafim
						,segunda
						,terca
						,quarta
						,quinta
						,sexta
						,sabado
						,sempre
						,tododia
						,domingo
						,dataFim
						,dataIniRep
						,duracao
						,tipo
						,tempoinatividade
						,repetir
						,criterio
						,valorrep
						,repnunca
						,repapos
						,repdia
						,dataFimRep
						,template
						,site
				)VALUES (
						'<<ALLTRIM(ucrsSeriesServ.seriestamp)>>', 
						<<ucrsSeriesServ.serieno>>, 
						'<<ALLTRIM(ucrsSeriesServ.serienome)>>',
						'<<ALLTRIM(ucrsSeriesServ.nome)>>',
						<<ucrsSeriesServ.no>>,
						<<ucrsSeriesServ.estab>>,
						'<<ALLTRIM(ucrsSeriesServ.dirClinico)>>',
						'<<ALLTRIM(ucrsSeriesServ.dirServico)>>',
						'<<uf_gerais_getdate(ucrsSeriesServ.dataInicio,"SQL")>>',
						<<ucrsSeriesServ.sessoes>>
						,'<<m.m_chinis>>'
						,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
						,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
						,'<<m.m_chinis>>'
						,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
						,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
						,'<<ALLTRIM(ucrsSeriesServ.horaInicio)>>'
						,'<<ALLTRIM(ucrsSeriesServ.horafim)>>'
						,<<IIF(ucrsSeriesServ.segunda,1,0)>>
						,<<IIF(ucrsSeriesServ.terca,1,0)>>
						,<<IIF(ucrsSeriesServ.quarta,1,0)>>
						,<<IIF(ucrsSeriesServ.quinta,1,0)>>
						,<<IIF(ucrsSeriesServ.sexta,1,0)>>
						,<<IIF(ucrsSeriesServ.sabado,1,0)>>
						,<<IIF(ucrsSeriesServ.sempre,1,0)>>
						,<<IIF(ucrsSeriesServ.tododia,1,0)>>
						,<<IIF(ucrsSeriesServ.domingo,1,0)>>
						,'<<uf_gerais_getdate(ucrsSeriesServ.dataFim,"SQL")>>'
						,'<<uf_gerais_getdate(ucrsSeriesServ.dataIniRep,"SQL")>>'
						,<<ucrsSeriesServ.duracao>>
						,'<<ALLTRIM(ucrsSeriesServ.tipo)>>'
						,<<ucrsSeriesServ.tempoinatividade>>
						,<<IIF(ucrsSeriesServ.repetir,1,0)>>
						,'<<ALLTRIM(ucrsSeriesServ.criterio)>>'
						,<<ucrsSeriesServ.valorrep>>
						,<<IIF(ucrsSeriesServ.repnunca,1,0)>>
						,<<IIF(ucrsSeriesServ.repapos,1,0)>>
						,<<IIF(ucrsSeriesServ.repdia,1,0)>>
						,'<<uf_gerais_getdate(ucrsSeriesServ.dataFimRep,"SQL")>>'
						,<<IIF(ucrsSeriesServ.template,1,0)>>
						,'<<ALLTRIM(ucrsSeriesServ.site)>>'
				)
			END
			ELSE
			BEGIN
				UPDATE 
					b_series 
				SET 
					serienome= '<<ALLTRIM(ucrsSeriesServ.serienome)>>'
					,nome = '<<ALLTRIM(ucrsSeriesServ.nome)>>'
					,no = <<ucrsSeriesServ.no>>
					,estab = <<ucrsSeriesServ.estab>>
					,dirClinico = '<<ALLTRIM(ucrsSeriesServ.dirClinico)>>'
					,dirServico = '<<ALLTRIM(ucrsSeriesServ.dirServico)>>'
					,dataInicio = '<<uf_gerais_getdate(ucrsSeriesServ.dataInicio,"SQL")>>'
					,sessoes = <<ucrsSeriesServ.sessoes>>
					,usrinis = '<<m.m_chinis>>'
					,usrdata = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,usrhora = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
					,horaInicio = '<<ALLTRIM(ucrsSeriesServ.horaInicio)>>'
					,horafim = '<<ALLTRIM(ucrsSeriesServ.horafim)>>'
					,segunda = <<IIF(ucrsSeriesServ.segunda,1,0)>>
					,terca = <<IIF(ucrsSeriesServ.terca,1,0)>>
					,quarta = <<IIF(ucrsSeriesServ.quarta,1,0)>>
					,quinta = <<IIF(ucrsSeriesServ.quinta,1,0)>>
					,sexta = <<IIF(ucrsSeriesServ.sexta,1,0)>>
					,sabado = <<IIF(ucrsSeriesServ.sabado,1,0)>>
					,sempre = <<IIF(ucrsSeriesServ.sempre,1,0)>>
					,tododia = <<IIF(ucrsSeriesServ.tododia,1,0)>>
					,domingo = <<IIF(ucrsSeriesServ.domingo,1,0)>>
					,dataFim = '<<uf_gerais_getdate(ucrsSeriesServ.dataFim,"SQL")>>'
					,dataIniRep = '<<uf_gerais_getdate(ucrsSeriesServ.dataIniRep,"SQL")>>'
					,duracao = <<ucrsSeriesServ.duracao>>
					,tipo = '<<ALLTRIM(ucrsSeriesServ.tipo)>>'
					,tempoinatividade = <<ucrsSeriesServ.tempoinatividade>>
					,repetir = <<IIF(ucrsSeriesServ.repetir,1,0)>>
					,criterio = '<<ALLTRIM(ucrsSeriesServ.criterio)>>'
					,valorrep = <<ucrsSeriesServ.valorrep>>
					,repnunca = <<IIF(ucrsSeriesServ.repnunca,1,0)>>
					,repapos = <<IIF(ucrsSeriesServ.repapos ,1,0)>>
					,repdia = <<IIF(ucrsSeriesServ.repdia ,1,0)>>
					,dataFimRep = '<<uf_gerais_getdate(ucrsSeriesServ.dataFimRep,"SQL")>>'
					,template = <<IIF(ucrsSeriesServ.template,1,0)>>
					,site = '<<ALLTRIM(ucrsSeriesServ.site)>>'
				WHERE
					b_series.seriestamp = '<<ALLTRIM(ucrsSeriesServ.seriestamp )>>'
			END
			
			
		ENDTEXT

		IF !uf_gerais_actGrelha("", "",lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A GRAVAR A S�RIE DE SERVI�OS! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			lcValidaInsert = .t.
		ENDIF			
					
		lcActualizaServicos = uf_PLANEAMENTOMR_insereServicosBD()	
		lcActualizaRecursos = UF_PLANEAMENTOMR_GRAVARECURSOS()	
		lcActualizaConsumos = UF_PLANEAMENTOMR_GRAVACONSUMOS()			
		
		
		IF lcValidaInsert == .f. AND lcActualizaServicos == .t. AND lcActualizaRecursos == .t. AND lcActualizaConsumos == .t.
			uf_gerais_actGrelha("", "",[COMMIT TRANSACTION])
		ELSE
			uf_gerais_actGrelha("", "",[ROLLBACK])
		Endif	
	ENDIF 
	
	
	Select ucrsSeriesServ
	GO TOP


	IF UPPER(ALLTRIM(ucrsSeriesServ.tipo)) == "SERVI�OS";
		 OR UPPER(ALLTRIM(ucrsSeriesServ.tipo)) == "ATIVIDADES" 
						
			** Efetua Marca��o 
			uf_PLANEAMENTOMR_marcar()
			** 

	ENDIF 

ENDFUNC 


**
FUNCTION uf_PLANEAMENTOMR_camposObrigatorios
	
	SELECT ucrsSeriesServ
	
	** Descricao/Tipo
	IF empty(ucrsSeriesServ.serienome) OR empty(ucrsSeriesServ.tipo) 
		uf_perguntalt_chama("Existem campos obrigat�rios por preencher. Por favor verifique.", "", "OK", 64)
		RETURN .f.
	ENDIF	
		
		
	IF UPPER(ALLTRIM(ucrsSeriesServ.tipo)) == "SERVI�OS" 
		
		IF EMPTY(ALLTRIM(ucrsSeriesServ.nome))
			uf_perguntalt_chama("A identifica��o do Utente � obrigat�ria.", "", "OK", 64)
			RETURN .f.
		ENDIF
		
		Select ucrsServicosSerie
		COUNT TO lcNumServicos
		IF lcNumServicos == 0
			uf_perguntalt_chama("N�o � possivel gravar a s�rie sem servi�os.", "", "OK", 64)
			RETURN .f.
		ENDIF 
	ENDIF
		
	RETURN .t.

ENDFUNC 

** 
FUNCTION uf_PLANEAMENTOMR_marcar

	
	LOCAL lcValorUtente, lcValorEntidade
	STORE 0 to lcValorUtente, lcValorEntidade
	
	** Se o utente n�o tiver sido escolhido coloca informa��o do pacgen = 1 (b_utentes), caso n�o exista paciente gen�rico n�o deixa gravar
	IF EMPTY(ALLTRIM(ucrsSeriesServ.nome))
		TEXT TO lcSQL NOSHOW TEXTMERGE
			select TOP 1 utstamp, nome, no, estab from B_utentes where pacgen = 1
		ENDTEXT 
		IF !uf_gerais_actGrelha("", "ucrsPacGen",lcSQL)
			MESSAGEBOX("OCORREU UM ERRO NA VERIFICA��O DE EXISTENCIA DO PACIENTE GEN�RICO! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
		IF RECCOUNT("ucrsPacGen") == 0
			uf_perguntalt_chama("Paciente Gen�rico n�o configurado. N�o pode proceder � marca��o sem selecionar um utente.","OK","",16)
			RETURN .f.
		ENDIF 
		
		Select ucrsSeriesServ
		Select ucrsPacGen
		Replace ucrsSeriesServ.utstamp WITH ucrsPacGen.utstamp
		Replace ucrsSeriesServ.nome WITH ALLTRIM(ucrsPacGen.nome)
		Replace ucrsSeriesServ.no WITH ucrsPacGen.no
		Replace ucrsSeriesServ.estab WITH ucrsPacGen.estab 
		
	ENDIF 


	** data Do Periodo Selecionado
	SELECT ucrsPeriodosCalendarioMensal
	lcData  = ucrsPeriodosCalendarioMensal.dataIni



	SELECT * from ucrsPeriodosCalendarioMensal WHERE !EMPTY(sel) INTO CURSOR ucrsPeriodosCalendarioVerificaSelect READWRITE
	IF RECCOUNT("ucrsPeriodosCalendarioVerificaSelect") == 0
		** N�o selecionou Periodo
		RETURN .f.
	ENDIF  

	** Periodos Sequenciais
	select MIN(horaIni) as horaIni, MAX(HoraFim) as horaFim from ucrsPeriodosCalendarioMensal WHERE !EMPTY(sel) INTO CURSOR ucrsPeriodosCalendarioMensalAgrupados READWRITE 
	select ucrsPeriodosCalendarioMensalAgrupados 
	GO Top
	
	IF RECCOUNT("ucrsPeriodosCalendarioMensalAgrupados") == 0
		** N�o selecionou Periodo
		RETURN .f.
	ENDIF  
	
	

	** Stamp 
	Select ucrsRecursosCalendarioMensal
	lcCombinacao  = ALLTRIM(ucrsRecursosCalendarioMensal.descricao)
	lcPVP = ucrsRecursosCalendarioMensal.PVP
	lcCompart = ucrsRecursosCalendarioMensal.Compart
	lcUtente = ucrsRecursosCalendarioMensal.utente
	lcRef = ucrsRecursosCalendarioMensal.ref
	lcSite = ucrsRecursosCalendarioMensal.local
	
	
	** Cria Stamp para inser��o dos dados da marca��o
	lcMrStamp = uf_gerais_stamp()
	Select ucrsSeriesServ
	
	TEXT TO lcSQL NOSHOW TEXTMERGE
					
		insert into marcacoes (
			mrstamp
			,data
			,dataFim
			,hinicio
			,hfim
			,utstamp
			,nome
			,no
			,estab
			,serieno
			,serienome
			,sessao
			,estado
			,mrno
			,combinacao
			,site
			,id_us
			,data_cri
			,id_us_alt
			,data_alt
		) values(
			'<<lcMrStamp>>'
			,'<<uf_gerais_getdate(lcData,"SQL")>>'
			,'<<uf_gerais_getdate(lcData,"SQL")>>'
			,'<<ucrsPeriodosCalendarioMensalAgrupados.horaIni>>'
			,'<<ucrsPeriodosCalendarioMensalAgrupados.horaFim>>'
			,<<IIF(EMPTY(ALLTRIM(ucrsSeriesServ.utstamp)) or ISNULL(ucrsSeriesServ.utstamp),'null',"'" + ALLTRIM(ucrsSeriesServ.utstamp) +"'")>>
			,'<<ucrsSeriesServ.nome>>'
			,<<ucrsSeriesServ.no>>
			,<<ucrsSeriesServ.estab>>
			,<<ucrsSeriesServ.serieno>>
			,'<<ucrsSeriesServ.serienome>>'
			,0
			,'MARCADO'
			,(select isnull(MAX(mrno),0)+1 from marcacoes)
			,'<<ALLTRIM(lcCombinacao)>>'
			,'<<ALLTRIM(lcSite)>>'
			,<<ch_userno>>
			,dateadd(HOUR, <<difhoraria>>, getdate())
			,<<ch_userno>>
			,dateadd(HOUR, <<difhoraria>>, getdate())
		)
	ENDTEXT
	
	IF !uf_gerais_actGrelha("", "",lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A INSERIR MARCA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	
	
	&& For�a posi��o cursor
	Select ucrsServicosSerie
	GO TOP 
	SCAN 
*!*			LOCATE FOR ALLTRIM(ucrsServicosSerie.ref) == ALLTRIM(lcRef)
*!*			IF FOUND()
*!*				lcDesign = ALLTRIM(ucrsServicosSerie.design)
*!*			ELSE
*!*			 	lcDesign = ""
*!*			ENDIF 	


		lcServStamp = uf_gerais_stamp()
		lcOrdemComp = 0
		lcValorUtente = lcPVP
		
	
		Select ucrsDadosComparticipacaoSerie
		GO top
		SCAN FOR ALLTRIM(ucrsDadosComparticipacaoSerie.ref) == ALLTRIM(ucrsServicosSerie.ref)

			lcmarcacoesCompStamp = uf_gerais_stamp()
			lcOrdemComp  = lcOrdemComp  +1 
			TEXT TO lcSQL NOSHOW TEXTMERGE
							
				insert into cpt_marcacoes(
					servcompmrstamp
					,servmrstamp
					,mrstamp
					,id_cp
					,ordem
					,valor
					,entidade
					,entidadeno
					,entidadeestab
					,compart
					,maximo
					,tipoCompart								
				) values(
					'<<lcmarcacoesCompStamp>>'
					,<<IIF(EMPTY(ALLTRIM(lcServStamp)) or ISNULL(lcServStamp),'null',"'" + ALLTRIM(lcServStamp) +"'")>>
					,<<IIF(EMPTY(ALLTRIM(lcMrStamp)) or ISNULL(lcMrStamp),'null',"'" + ALLTRIM(lcMrStamp) +"'")>>
					,'<<ucrsDadosComparticipacaoSerie.id>>'
					,<<lcOrdemComp>>
					,<<ucrsDadosComparticipacaoSerie.valorEntidade>>
					,'<<ALLTRIM(ucrsDadosComparticipacaoSerie.enome)>>'
					,<<ucrsDadosComparticipacaoSerie.eno>>
					,<<ucrsDadosComparticipacaoSerie.eestab>>
					,<<ucrsDadosComparticipacaoSerie.compart>>
					,<<ucrsDadosComparticipacaoSerie.maximo>>
					,'<<ALLTRIM(ucrsDadosComparticipacaoSerie.tipoCompart)>>'
				)
			ENDTEXT
*!*	_CLIPTEXT = lcSQL
*!*	MESSAGEBOX(lcSQL)				

			IF !uf_gerais_actGrelha("", "",lcSQL)
				MESSAGEBOX("OCORREU UM ERRO A INSERIR SERVI�O DE MARCA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
				RETURN .f.
			ENDIF
		ENDSCAN 


		lcDesign = ALLTRIM(ucrsServicosSerie.design)
		TEXT TO lcSQL NOSHOW TEXTMERGE
						
			insert into marcacoesServ (
				servmrstamp
				,mrstamp
				,ref
				,design
				,duracao
				,qtt
				,pvp
				,compart
				,total
				,dataInicio
				,dataFim
				,hinicio
				,hfim
				,ordem
			) values(
				'<<lcServStamp>>'
				,'<<lcMrStamp>>'
				,'<<ALLTRIM(ucrsServicosSerie.ref)>>'
				,'<<ALLTRIM(lcDesign)>>'
				,<<ucrsServicosSerie.duracao>>
				,<<ucrsServicosSerie.qtt>>
				,<<lcPvp>>
				,<<lcCompart>>
				,<<lcUtente>>
				,'<<uf_gerais_getdate(lcData,"SQL")>>'
				,'<<uf_gerais_getdate(lcData,"SQL")>>'
				,'<<ucrsPeriodosCalendarioMensalAgrupados.horaIni>>'
				,'<<ucrsPeriodosCalendarioMensalAgrupados.horaFim>>'
				,1
			)
		ENDTEXT
		
	*!*	_CLIPTEXT = lcSQL
	*!*	MESSAGEBOX(lcSQL)

		IF !uf_gerais_actGrelha("", "",lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A INSERIR SERVI�O DE MARCA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	ENDSCAN 
	
	**Grava Recursos associados � marca��o
	SELECT ucrsRecursosCalendarioMensal
	lcStampCombinacao = ucrsRecursosCalendarioMensal.stamp
	SELECT ucrsMapeamentoRecursosCombinacao
	GO Top
	SCAN FOR ALLTRIM(ucrsMapeamentoRecursosCombinacao.stamp) == ALLTRIM(lcStampCombinacao)
		
		TEXT TO lcSQL NOSHOW TEXTMERGE
			
			insert into marcacoesRecurso(
				id
				,mrstamp
				,nome
				,no
				,tipo
				,ref
			) 
			SELECT 
				id = (select ISNULL(MAX(id),0) + 1 from marcacoesRecurso)
				,mrstamp = '<<lcMrStamp>>'
				,nome = '<<ALLTRIM(ucrsMapeamentoRecursosCombinacao.nome)>>'
				,no = <<ucrsMapeamentoRecursosCombinacao.no>>
				,tipo = '<<ALLTRIM(ucrsMapeamentoRecursosCombinacao.tipo)>>'
				,ref = '<<ALLTRIM(ucrsMapeamentoRecursosCombinacao.ref)>>'
		ENDTEXT
*!*	_CLIPTEXT = lcSQL
*!*	MESSAGEBOX(lcSQL)				
		IF !uf_gerais_actGrelha("", "",lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A INSERIR RECURSOS NA MARCA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	ENDSCAN 
	**********************************************


	**
	uf_PLANEAMENTOMR_indicadoresCalendarioMensalDia(lcData)
	uf_PLANEAMENTOMR_calendarioMensalAfterRowColumnChange() && Actualiza Periodos
	**************************

	**
	Select ucrsSeriesServ
	** 
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_marcacoes_sessoes <<ucrsSeriesServ.serieno>>
	ENDTEXT
	
	IF !uf_gerais_actgrelha("PLANEAMENTOMR.ContainerDetalhe.Pageframe1.Page2.Container1.GridSessoes", "ucrsSessoesSerie", lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR AS SESS�ES ASSOCIADAS � S�RIE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF


	uf_perguntalt_chama("Marca��o efetuada com sucesso.", "", "OK", 64)	
	
	**
	PLANEAMENTOMR.gridDoc.setfocus
ENDFUNC 



**
FUNCTION uf_PLANEAMENTOMR_insereServicosBD

	**Trata Inser��o na tabela de US
	TEXT TO lcSQL NOSHOW textmerge
		delete
		FROM	b_seriesServicos
		WHERE	seriestamp = '<<ALLTRIM(ucrsSeriesServ.seriestamp)>>'
	ENDTEXT
	IF !uf_gerais_actgrelha("", "", lcSQL)
		MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ACTUALIZAR OS SERVI�OS DA S�RIE. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		return .f.
	ENDIF 
	
	SELECT ucrsServicosSerie
	IF RECCOUNT("ucrsServicosSerie")>0
		SELECT ucrsServicosSerie
		GO TOP
		SCAN
			lcStamp = uf_gerais_stamp()
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
								
				INSERT INTO b_seriesServicos
				(	serieservstamp
					,seriestamp
					,ref
					,design
					,qtt
					,pvp
					,total
					,coddiv
					,div
					,drno
					,drnome
					,duracao
					,ousrinis
					,ousrdata
					,ousrhora
					,usrinis
					,usrdata
					,usrhora
					,ordem
					,mrsimultaneo
					
				)
				VALUES	(		
					'<<ALLTRIM(lcStamp)>>'
					,'<<ALLTRIM(ucrsSeriesServ.seriestamp)>>'
					,'<<ALLTRIM(ucrsServicosSerie.ref)>>'
					,'<<ALLTRIM(ucrsServicosSerie.design)>>'
					,<<ucrsServicosSerie.qtt>>
					,<<ucrsServicosSerie.pvp>>
					,<<ucrsServicosSerie.total>>
					,'<<ALLTRIM(ucrsServicosSerie.coddiv)>>'
					,'<<ALLTRIM(ucrsServicosSerie.div)>>'
					,<<ucrsServicosSerie.drno>>
					,'<<ALLTRIM(ucrsServicosSerie.drnome)>>'
					,<<ucrsServicosSerie.duracao>>
					,'<<m.m_chinis>>'
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
					,'<<m.m_chinis>>'
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
					,<<ucrsServicosSerie.ordem>>
					,<<ucrsServicosSerie.mrsimultaneo>>
				)
			ENDTEXT

*!*	_CLIPTEXT = lcSQL
*!*	messagebox(lcSQL)
			
			IF !uf_gerais_actgrelha("", "", lcSQL)
				MESSAGEBOX("OCORREU UM ERRO A ACTUALIZAR OS SERVI�OS DA SERIE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
				return .f.
			ENDIF
		ENDSCAN
	ENDIF

	return .t.
ENDFUNC 



**
FUNCTION UF_PLANEAMENTOMR_GRAVARECURSOS
	Select ucrsSeriesServ
	
	**Trata Inser��o na tabela de REcursos
	TEXT TO lcSQL NOSHOW textmerge
		DELETE FROM	b_seriesRecurso WHERE seriestamp= '<<ALLTRIM(ucrsSeriesServ.seriestamp)>>'
	ENDTEXT
	IF !uf_gerais_actgrelha("", "", lcSQL)
		MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ACTUALIZAR OS RECURSOS AFECTOS � ACTIVIDADE. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		return .f.
	ENDIF 
	
	Select uCrsRecursosDaSerie
	SET FILTER TO 
	
	SELECT uCrsRecursosDaSerie
	IF RECCOUNT("uCrsRecursosDaSerie")>0
		SELECT uCrsRecursosDaSerie
		GO TOP
		SCAN
			
			lcStamp = uf_gerais_stamp()
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
								
				INSERT INTO b_seriesRecurso
				(	serierecstamp
					,seriestamp
					,ref
					,nome
					,no
					,stamp
					,tipo
					,ousrinis
					,ousrdata
					,ousrhora
				)
				VALUES	(		
					'<<ALLTRIM(lcStamp)>>'
					,<<IIF(EMPTY(ALLTRIM(ucrsSeriesServ.seriestamp)) or ISNULL(ucrsSeriesServ.seriestamp),'null',"'" + ALLTRIM(ucrsSeriesServ.seriestamp) +"'")>>
					,'<<ALLTRIM(uCrsRecursosDaSerie.ref)>>'
					,'<<ALLTRIM(uCrsRecursosDaSerie.nome)>>'
					,<<uCrsRecursosDaSerie.no>>
					,<<IIF(EMPTY(ALLTRIM(uCrsRecursosDaSerie.stamp)) or ISNULL(uCrsRecursosDaSerie.stamp),'null',"'" + ALLTRIM(uCrsRecursosDaSerie.stamp) +"'")>>
					,'<<ALLTRIM(uCrsRecursosDaSerie.tipo)>>'
					,'<<m_chinis>>'
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
					)
			ENDTEXT
*!*	_CLIPTEXT = lcSQL
*!*	MESSAGEBOX(lcSQL)
			IF !uf_gerais_actgrelha("", "", lcSQL)
				MESSAGEBOX("OCORREU UM ERRO A ACTUALIZAR OS RECURSOS AFECTOS � ACTIVIDADE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
				return .f.
			ENDIF
		ENDSCAN
	ENDIF
	Select uCrsRecursosDaSerie
	GO top
ENDFUNC 


**
FUNCTION UF_PLANEAMENTOMR_GRAVACONSUMOS

	**Trata Inser��o na tabela de b_seriesConsumo
	TEXT TO lcSQL NOSHOW textmerge
		DELETE FROM	b_seriesConsumo WHERE seriestamp= '<<ALLTRIM(ucrsSeriesServ.seriestamp)>>'
	ENDTEXT
	IF !uf_gerais_actgrelha("", "", lcSQL)
		MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ACTUALIZAR OS CONSUMOS AFECTOS � ACTIVIDADE. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		return .f.
	ENDIF 

	SELECT uCrsSeriesConsumo
	SET FILTER TO 
	IF RECCOUNT("uCrsSeriesConsumo")>0
		SELECT uCrsSeriesConsumo
		GO TOP
		SCAN
			lcStamp = uf_gerais_stamp()
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
								
				INSERT INTO b_seriesConsumo
				(	consumoseriestamp
					,seriestamp
					,ref
					,reforiginal
					,duracao
					,qtt
				)
				VALUES	(		
					'<<ALLTRIM(lcStamp)>>'
					,<<IIF(EMPTY(ALLTRIM(ucrsSeriesServ.seriestamp)) or ISNULL(ucrsSeriesServ.seriestamp),'null',"'" + ALLTRIM(ucrsSeriesServ.seriestamp) +"'")>>
					,'<<ALLTRIM(uCrsSeriesConsumo.ref)>>'
					,'<<ALLTRIM(uCrsSeriesConsumo.reforiginal)>>'
					,<<uCrsSeriesConsumo.duracao>>
					,<<uCrsSeriesConsumo.qtt>>
					)
			ENDTEXT

			IF !uf_gerais_actgrelha("", "", lcSQL)
				MESSAGEBOX("OCORREU UM ERRO A ACTUALIZAR OS CONSUMOS AFECTOS � ACTIVIDADE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
				return .f.
			ENDIF
		ENDSCAN
	ENDIF
	Select uCrsSeriesConsumo
	GO Top
ENDFUNC 


**
FUNCTION uf_PLANEAMENTOMR_atualizaPVPs

	LOCAL lcRefs
	lcRefs = ""
	
	Select ucrsServicosSerie
	GO Top
	SCAN
		IF empty(lcRefs)
			lcRefs = ALLTRIM(ucrsServicosSerie.ref)
		ELSE
			lcRefs = "," + ALLTRIM(ucrsServicosSerie.ref)
		ENDIF 
	ENDSCAN 
	
	
	** Calcula PVPs para caso n�o existam conven��es para o servi�o e utente
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_PvpsSemConvencao '<<ALLTRIM(lcRefs)>>'
	ENDTEXT 	
	If !uf_gerais_actGrelha("", "ucrsPvpsSemConvencao", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar as pre�os dos servi�os. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	********************************************************************	


	** Calcula PVPs para caso n�o existam conven��es para o servi�o e utente, mas que em que existem o servi�o na teabela de pre�os do m�dico
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_PvpsEspecialistaSemConvencao'<<ALLTRIM(lcRefs)>>'
	ENDTEXT 	
	If !uf_gerais_actGrelha("", "ucrsPvpsEspecialistaSemConvencao", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar as pre�os dos servi�os. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	********************************************************************	

	** Calcula informa��o de Conven��es e PVP associados ao utente para todos os servi�os
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_convencoesUtentes <<ucrsSeriesServ.no>>,<<ucrsSeriesServ.estab>>
	ENDTEXT 	
	If !uf_gerais_actGrelha("", "ucrsConvencoesUtente", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar as conven��es associadas �s entidades do utente. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	********************************************************************	
				
	
	** Conven��o e PVP
	SELECT ucrsRecursosCalendarioMensal
	GO Top
	SCAN 
		lcRef = ucrsRecursosCalendarioMensal.ref
		lcSite = ucrsRecursosCalendarioMensal.local
		lcUserNo = ucrsRecursosCalendarioMensal.utilizador
		
	
		** 1. CONVEN��O
		SELECT ucrsConvencoesUtente
		LOCATE FOR ALLTRIM(ucrsConvencoesUtente.site) == ALLTRIM(lcSite) AND ALLTRIM(ucrsConvencoesUtente.ref) == ALLTRIM(lcRef) 
		IF FOUND()
			SELECT ucrsRecursosCalendarioMensal	
			Replace ucrsRecursosCalendarioMensal.pvp WITH ucrsConvencoesUtente.pvp 
		ELSE

			** 2. PVP ESPECIALISTA LOCAL
			&& Caso exista pre�o para o servi�o,especialista e local
			SELECT ucrsPvpsEspecialistaSemConvencao
			GO Top
			SELECT ucrsPvpsEspecialistaSemConvencao
			LOCATE FOR ALLTRIM(ucrsPvpsEspecialistaSemConvencao.site) == ALLTRIM(lcSite) AND ALLTRIM(ucrsPvpsEspecialistaSemConvencao.ref) == ALLTRIM(lcRef);
				AND ucrsPvpsEspecialistaSemConvencao.no = lcUserNo; 
				AND !EMPTY(ucrsPvpsEspecialistaSemConvencao.PVP)
			IF FOUND()

				SELECT ucrsRecursosCalendarioMensal	
				Replace ucrsRecursosCalendarioMensal.pvp WITH ucrsPvpsEspecialistaSemConvencao.pvp
			ELSE
					
				** 3. PVP FICHA LOCAL
				&& caso n�o tenha conven��o coloca pre�o da ficha do local em causa
				SELECT ucrsPvpsSemConvencao
				GO Top
				SELECT ucrsPvpsSemConvencao
				LOCATE FOR ALLTRIM(ucrsPvpsSemConvencao.site) == ALLTRIM(lcSite) AND ALLTRIM(ucrsPvpsSemConvencao.ref) == ALLTRIM(lcRef) 
				IF FOUND()

					SELECT ucrsRecursosCalendarioMensal	
					Replace ucrsRecursosCalendarioMensal.pvp WITH ucrsPvpsSemConvencao.pvp
				ELSE
					SELECT ucrsRecursosCalendarioMensal	
					Replace ucrsRecursosCalendarioMensal.pvp WITH 0
				ENDIF 
				
			ENDIF 
		ENDIF
	ENDSCAN 
	SELECT ucrsRecursosCalendarioMensal
	GO Top
	
	
ENDFUNC 


**
FUNCTION uf_PLANEAMENTOMR_dadosComparticipacao
	LOCAL lcSQL
	
	SELECT ucrsRecursosCalendarioMensal
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_clinica_DadosComparticipacao '<<ALLTRIM(ucrsSeriesServ.utstamp)>>','<<ALLTRIM(ucrsRecursosCalendarioMensal.ref)>>','<<ALLTRIM(ucrsRecursosCalendarioMensal.local)>>','<<ALLTRIM(ucrsRecursosCalendarioMensal.nomeespecialista)>>'
	ENDTEXT

*!*	_CLIPTEXT = lcSQL
*!*	MESSAGEBOX(lcSQL)
	IF !uf_gerais_actGrelha("PLANEAMENTOMR.ContainerDetalhe.Pageframe1.Page3.GridCompart", "ucrsDadosComparticipacaoSerie",lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VALIDAR DADOS DE COMPARTICIPA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF


	lcServStamp = uf_gerais_stamp()
	lcOrdemComp = 0
	lcPVP = ucrsRecursosCalendarioMensal.pvp
	lcValorUtente = ucrsRecursosCalendarioMensal.pvp

	SELECT ucrsDadosComparticipacaoSerie
	Replace ucrsDadosComparticipacaoSerie.pvp WITH lcValorUtente
	lcPvp = lcValorUtente

	DO CASE
	CASE UPPER(ALLTRIM(ucrsDadosComparticipacaoSerie.tipoCompart)) == UPPER(ALLTRIM("Valor")) && Comparticipa��o em valor

		lcValorUtente =  ucrsDadosComparticipacaoSerie.compart

	CASE UPPER(ALLTRIM(ucrsDadosComparticipacaoSerie.tipoCompart)) == UPPER(ALLTRIM("Percent")) && Comparticipa��o em percentagem

		lcValorPercent = (lcPvp*ucrsDadosComparticipacaoSerie.compart)/100
		lcValorUtente =  lcValorPercent

	OTHERWISE
		**
	ENDCASE

	lcValorEntidade = lcPVP - lcValorUtente			

	SELECT ucrsDadosComparticipacaoSerie
	Replace ucrsDadosComparticipacaoSerie.valorUtente WITH lcValorUtente
	Replace ucrsDadosComparticipacaoSerie.valorEntidade WITH lcValorEntidade

	LOCAL lcValorTotalEntidade
	STORE 0 TO lcValorTotalEntidade	
	
	SELECT ucrsDadosComparticipacaoSerie
	CALCULATE SUM(valorEntidade) TO lcValorTotalEntidade
	
	SELECT ucrsRecursosCalendarioMensal
	Replace ucrsRecursosCalendarioMensal.compart WITH lcValorTotalEntidade
	Replace ucrsRecursosCalendarioMensal.utente WITH ucrsRecursosCalendarioMensal.pvp - lcValorTotalEntidade

	WITH PLANEAMENTOMR.ContainerDetalhe.Pageframe1.Page1
		.pvp.refresh
		.comp.refresh
		.utente.refresh
	ENDWITH 
		
ENDFUNC 


**
FUNCTION uf_PLANEAMENTOMR_navegamarcacao
	LOCAL lcDataPeriodo, lcMrstamp
	SELECT ucrsSessoesSerie
	lcDataPeriodo = DATE(ucrsSessoesSerie.ano,ucrsSessoesSerie.mes,ucrsSessoesSerie.dia)
	lcMrstamp = ucrsSessoesSerie.mrstamp
	uf_marcacoes_chama()

	SELECT ucrsMarcacoesCab
	Replace ucrsMarcacoesCab.ano WITH YEAR(lcDataPeriodo)
	Replace ucrsMarcacoesCab.mes WITH MONTH(lcDataPeriodo)
	Replace ucrsMarcacoesCab.mesDesc WITH uf_gerais_getMonthToMonthEx(MONTH(lcDataPeriodo))
	
	uf_marcacoes_AplicaDia(LEFT(uf_gerais_getdate(lcDataPeriodo,"DATA"),2))
	marcacoes.local.value = ""
	marcacoes.utente.value = ALLTRIM(ucrsSeriesServ.nome)
	marcacoes.serie.value = ALLTRIM(ucrsSeriesServ.serienome)
	marcacoes.especialidade.value = ""
	marcacoes.servico.value = ""
	marcacoes.especialista.value = ""

	
	SELECT ucrsMarcacoesDia
	LOCATE FOR ucrsMarcacoesDia.mrstamp == ALLTRIM(lcMrstamp)
	IF FOUND()
		uf_PLANEAMENTOMR_sair()
		marcacoes.gridPesq.nome.setfocus
	ENDIF 			
	
		
ENDFUNC


**
FUNCTION uf_planeamentomr_expandeDetalhe
	
	IF EMPTY(PLANEAMENTOMR.expandeDetalhe)
		PLANEAMENTOMR.gridDoc.height = PLANEAMENTOMR.gridDoc.height - 200
		PLANEAMENTOMR.ContainerDetalhe.height = PLANEAMENTOMR.ContainerDetalhe.height + 200
		PLANEAMENTOMR.expandeDetalhe = .t.
		PLANEAMENTOMR.ContainerDetalhe.check1.config(myPath + "\imagens\icons\pageup_B.png", "")
	ELSE
		PLANEAMENTOMR.gridDoc.height = PLANEAMENTOMR.gridDoc.height + 200
		PLANEAMENTOMR.ContainerDetalhe.height = PLANEAMENTOMR.ContainerDetalhe.height - 200
		PLANEAMENTOMR.expandeDetalhe = .f.
		PLANEAMENTOMR.ContainerDetalhe.check1.config(myPath + "\imagens\icons\pagedown_B.png", "")
	ENDIF 
	
	PLANEAMENTOMR.ContainerDetalhe.top = PLANEAMENTOMR.gridDoc.height + 68

ENDFUNC 


**
FUNCTION uf_PLANEAMENTOMR_sair
	**
	RELEASE myPLANEAMENTOMRIntroducao
	RELEASE myPLANEAMENTOMRAlteracao
	
	
	IF USED("ucrsPeriodosDisponiveisCombinados")
		fecha("ucrsPeriodosDisponiveisCombinados")
	ENDIF
	
	IF USED("ucrsPeriodosDisponiveisCombinadosAux")
		fecha("ucrsPeriodosDisponiveisCombinadosAux")
	ENDIF
	
	IF USED("ucrsPeriodosDisponiveis2")
		fecha("ucrsPeriodosDisponiveis2")
	ENDIF
	
	IF USED("ucrsPeriodosDisponiveisRefs")
		fecha("ucrsPeriodosDisponiveisRefs")
	ENDIF
	
	IF USED("ucrsPeriodosDisponiveis")
		fecha("ucrsPeriodosDisponiveis")
	ENDIF
	
	IF USED("ucrsSessoesSerie")
		fecha("ucrsSessoesSerie")
	ENDIF 
	
	IF USED("ucrsServicosSerie")
		fecha("ucrsServicosSerie")
	ENDIF
	
	
	
	PLANEAMENTOMR.hide
	PLANEAMENTOMR.release
ENDFUNC




