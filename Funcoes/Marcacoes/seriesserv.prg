
**
FUNCTION uf_SERIESSERV_chama
	Lparameters lcSerieStamp, lcTipo, lcReagendar, lcMrStamp, lcSerieNo
	
	PUBLIC mySERIESSERVIntroducao, mySERIESSERVAlteracao

	IF TYPE("SERIESSERV") == "U"
		uf_SERIESSERV_defaults()
		DO FORM SERIESSERV WITH lcReagendar, lcMrStamp
	ELSE
		SERIESSERV.reagendar = lcReagendar
		SERIESSERV.mrstamp = lcMrStamp
		
		SERIESSERV.show
	ENDIF

	IF !EMPTY(lcSerieStamp) OR !EMPTY(lcSerieNo)
		uf_SERIESSERV_CalculaCursores(lcSerieStamp,lcSerieNo)
	ENDIF
		
		
	WITH SERIESSERV.pageframe1.page1.pageframe1.page1.CtnRepeticao	
		IF !EMPTY(ucrsSeriesServ.repetir)
			.Visible =  .t.
		ELSE
			.Visible =  .f.
		ENDIF 
	ENDWITH 
	
	IF lcReagendar == .t.
		SERIESSERV.caption = "Reagendamento de Marca��o"
		uf_SERIESSERV_dispRecursos()	
	ELSE
		**
		uf_SERIESSERV_alternaMenu()
		uf_SERIESSERV_alteraVisualizacao()
	ENDIF 
	

ENDFUNC


**
FUNCTION uf_SERIESSERV_defaults
	
	IF !USED("ucrsSeriesServ")
		**Cursor Series
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_marcacoes_series null
		ENDTEXT

		If !uf_gerais_actGrelha("", "ucrsSeriesServ", lcSql)
			uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es de S�ries de Servi�o. Contacte o suporte.", "", "OK", 32)
			RETURN .f.
		ENDIF
	ENDIF

	IF !USED("ucrsServicosSerie")
		**Cursor Series
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_marcacoes_seriesServicos null
		ENDTEXT

		If !uf_gerais_actGrelha("", "ucrsServicosSerie", lcSql)
			uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es dos Servi�os da S�ries. Contacte o suporte.", "", "OK", 32)
			RETURN .f.
		ENDIF
	ENDIF
	
	**	
	IF !USED("ucrsPeriodosCalendarioMensal")
		CREATE CURSOR ucrsPeriodosCalendarioMensal(posicao n(9,0), periodo n(9,0), dataIni d,dataFim d,horaIni c(5),horaFim c(5),nome c(150), sel l, duracao n(8,0), extra l)
	ENDIF 
	

	**
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_DadosRecursos
	ENDTEXT 
	If !uf_gerais_actGrelha("", "ucrsRecursos", lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar dados dos recursos. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF

	**	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_marcacoes_SeriesRecursos null
	ENDTEXT	
	
	IF !uf_gerais_actgrelha("", "uCrsRecursosDaSerie", lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR OS RECURSOS ASSOCIADOS � S�RIE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	**
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_marcacoes_SeriesConsumo null
	ENDTEXT	
	IF !uf_gerais_actgrelha("", "uCrsSeriesConsumo", lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR OS CONSUMOS ASSOCIADOS � S�RIE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF


	IF !USED("ucrsSessoesSerie")
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_marcacoes_sessoes null
		ENDTEXT	
		IF !uf_gerais_actgrelha("", "ucrsSessoesSerie", lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A VERIFICAR AS SESS�ES ASSOCIADAS � S�RIE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	ELSE
		SELECT ucrsSessoesSerie
		GO TOP 
		SCAN
			DELETE 
		ENDSCAN 
	ENDIF 

	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_clinica_DadosComparticipacao '','',''
	ENDTEXT
	IF !uf_gerais_actGrelha("", "ucrsDadosComparticipacaoSerie",lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VALIDAR DADOS DE COMPARTICIPA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF


ENDFUNC


**
FUNCTION uf_SERIESSERV_CalculaCursores
	Lparameters lcSerieStamp, lcSerieNo
	
	IF !EMPTY(lcSerieNo)
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			select seriestamp from b_series where serieno = <<lcSerieNo>>
		ENDTEXT
		
		If !uf_gerais_actGrelha("", "ucrsSeriesStamp", lcSql)
			uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es da S�rie. Contacte o suporte.", "", "OK", 32)
			RETURN .f.
		ENDIF
		
		IF RECCOUNT("ucrsSeriesStamp")>0
			SELECT ucrsSeriesStamp
			lcSerieStamp = ucrsSeriesStamp.seriestamp 
		ENDIF 
	ENDIF 


	**Cursor Series
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_marcacoes_series '<<lcSerieStamp>>'
	ENDTEXT

	If !uf_gerais_actGrelha("", "ucrsSeriesServ", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es de S�ries de Servi�o. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	
	**Cursor Servi�os
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_marcacoes_seriesServicos '<<lcSerieStamp>>'
	ENDTEXT

	If !uf_gerais_actGrelha("SERIESSERV.PageFrame1.page1.PageFrame1.page1.GridServicosSerie", "ucrsServicosSerie", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es dos Servi�os da S�ries. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_marcacoes_SeriesRecursos '<<lcSerieStamp>>'
	ENDTEXT	
	
	IF !uf_gerais_actgrelha("SERIESSERV.PageFrame1.page1.PageFrame1.page1.gridRecursos", "uCrsRecursosDaSerie", lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR OS RECURSOS ASSOCIADOS � S�RIE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_marcacoes_SeriesConsumo '<<lcSerieStamp>>'
	ENDTEXT	

	IF !uf_gerais_actgrelha("SERIESSERV.PageFrame1.page1.PageFrame1.page1.GridConsumos", "uCrsSeriesConsumo", lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR OS CONSUMOS ASSOCIADOS � S�RIE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	** Calcula informa��o de recursos
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_DadosRecursos
	ENDTEXT 
	If !uf_gerais_actGrelha("", "ucrsRecursos", lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar dados dos recursos. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	
	uf_SERIESSERV_alteraVisualizacao()		

	SERIESSERV.refresh

ENDFUNC



**
FUNCTION uf_SERIESSERV_init
	
	&& Configura menu principal
	WITH SERIESSERV.menu1
		.adicionaOpcao("opcoes","Op��es",myPath + "\imagens\icons\opcoes_w.png","","O")
		.adicionaOpcao("pesquisar","Pesquisar",myPath + "\imagens\icons\lupa_w.png","uf_pesqSeries_Chama", "P")
		&&.adicionaOpcao("nova","Novo",myPath + "\imagens\icons\mais_w.png","uf_SERIESSERV_novo","N")
		
		.adicionaOpcao("novoServ","Servi�os",myPath + "\imagens\icons\mais_w.png","uf_SERIESSERV_novo with 1","N")
		.adicionaOpcao("novaAtividade","Atividades",myPath + "\imagens\icons\mais_w.png","uf_SERIESSERV_novo with 3","N")
		&&.adicionaOpcao("novoUtilizador","Utilizadores",myPath + "\imagens\icons\mais_w.png","uf_SERIESSERV_novo with 2","N")
		&&.adicionaOpcao("novoRecurso","Recursos",myPath + "\imagens\icons\mais_w.png","uf_SERIESSERV_novo with 4","N")
		
		
		.adicionaOpcao("editar","Edit. Serie",myPath + "\imagens\icons\lapis_w.png","uf_SERIESSERV_editar","E")
		.adicionaOpcao("dispRecursos","Planeamento",myPath + "\imagens\icons\marcacoes.png","uf_SERIESSERV_dispRecursos","R")
		
		
		
		.adicionaOpcao("fichaUtente","Utente",myPath + "\imagens\icons\detalhe.png","uf_SERIESSERV_fichaUtente","M")
		.adicionaOpcao("fichaServi�o","Servi�o",myPath + "\imagens\icons\detalhe.png","uf_SERIESSERV_fichaServico","M")
		&&.adicionaOpcao("remarcar","Reagendar",myPath + "\imagens\icons\marcacoes.png","uf_SERIESSERV_remarcar","R")
		.adicionaOpcao("remarcarRepeticoes","Marcar Rep.",myPath + "\imagens\icons\marcacoes.png","uf_SERIESSERV_marcarRep","R")
		
	ENDWITH

	**
	WITH SERIESSERV.menu_opcoes
		.adicionaOpcao("apagar","Eliminar","","uf_SERIESSERV_apagar")
		.adicionaOpcao("imprimir", "Imprimir", "", "uf_SERIESSERV_reportSessoes")
		.adicionaOpcao("ultimo", "�ltimo", "", "uf_SERIESSERV_ultimo")
	ENDWITH

ENDFUNC


**
FUNCTION uf_SERIESSERV_alternaMenu
	
	WITH SERIESSERV.menu1	
		IF mySERIESSERVIntroducao == .t. OR mySERIESSERVAlteracao == .t.
			.estado("opcoes, dispRecursos", "SHOW", "Gravar", .t., "Cancelar", .t.)
			.estado("pesquisar,novoServ,novaAtividade, editar", "HIDE") &&novoUtilizador,&&novoRecurso
			
			WITH SERIESSERV.menu_opcoes
				.estado("apagar,ultimo", "HIDE")
			ENDWITH
		ELSE 
			.estado("opcoes, pesquisar, novoServ,novaAtividade, editar, dispRecursos", "SHOW", "Gravar", .f., "Sair", .t.)
			
			WITH SERIESSERV.menu_opcoes
				.estado("apagar,ultimo", "SHOW")
			ENDWITH
		ENDIF 
		
		IF SERIESSERV.pageFrame1.activepage == 5
			.estado("pesquisar, novoServ,novaAtividade, editar, dispRecursos, remarcarRepeticoes", "HIDE")
			.estado("", "SHOW", "", .f., "Voltar", .t.)	
		ELSE
			.estado("opcoes, dispRecursos", "SHOW")
		ENDIF 

	ENDWITH


	WITH SERIESSERV
		IF mySERIESSERVIntroducao == .t. OR mySERIESSERVAlteracao == .t.
			.tipo.readonly = .f.
			.serienome.readonly = .f.
			.serieno.readonly = .f.
			.nome.readonly = .f.
			.no.readonly = .f.
			.estab.readonly = .f.
			.Pageframe1.Page1.Pageframe1.Page1.inatividade.readonly = .f.
			.pageframe1.page1.pageframe1.page1.dirtecnico.readonly = .f.
			.pageframe1.page1.pageframe1.page1.dirservico.readonly = .f.

			WITH .pageframe1.page1.pageframe1.page1.CtnRepeticao

				.visible =  ucrsSeriesServ.repetir
				.container1.visible = IIF(ALLTRIM(UPPER(.criterio.value)) == "SEMANAL",.t.,.f.)
				.criterio.readonly = .f.
				.valorRep.readonly = .f.
				.ocorrencias.readonly = .f.
			ENDWITH
						
			WITH .pageframe1.page1.Pageframe1.Page1.GridServicosSerie
				FOR i=1 TO .columnCount
					DO CASE 
						CASE .Columns(i).name == "ordem"
							.Columns(i).readonly = .f.
						CASE .Columns(i).name == "qt"
							.Columns(i).readonly = .f.		
						OTHERWISE
							.Columns(i).readonly = .t.
					ENDCASE
				ENDFOR
			ENDWITH
			
 		ELSE
			.tipo.readonly = .t.
	 		.serienome.readonly = .t.
			.serieno.readonly = .t.
			.nome.readonly = .t.
			.no.readonly = .t.
			.estab.readonly = .t.
			.Pageframe1.Page1.Pageframe1.Page1.inatividade.readonly = .t.
			.pageframe1.page1.pageframe1.page1.dirtecnico.readonly = .t.
			.pageframe1.page1.pageframe1.page1.dirservico.readonly = .t.
			
			WITH .pageframe1.page1.pageframe1.page1.CtnRepeticao
				.visible =  ucrsSeriesServ.repetir
				.container1.visible = IIF(ALLTRIM(UPPER(.criterio.value)) == "SEMANAL",.t.,.f.)
				.criterio.readonly = .t.
				.valorRep.readonly = .t.
				.ocorrencias.readonly = .t.
			ENDWITH
			
			WITH .pageframe1.page1.Pageframe1.Page1.GridServicosSerie
				FOR i=1 TO .columnCount
					.Columns(i).readonly = .t.
				ENDFOR
			ENDWITH
 		ENDIF
 		
 	ENDWITH 

	SERIESSERV.refresh
ENDFUNC


**
FUNCTION uf_SERIESSERV_novo
	LPARAMETERS lcTipo
	
	IF EMPTY(lcTipo)
	 lcTipo = 0
	ENDIF 
	
	LOCAL lcStamp
	
	IF mySERIESSERVAlteracao==.T. OR mySERIESSERVIntroducao==.t.
		RETURN .f.
	ENDIF	
	
	mySERIESSERVIntroducao = .t.
	
	** 
	Select ucrsSeriesServ
	GO TOP 
	SCAN 
		Delete
	ENDSCAN
	
	**Numeracao da serie
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_marcacoes_seriesNum
	ENDTEXT
	If !uf_gerais_actGrelha("", "ucrsSeriesServNum", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar a numera��o a atrinuir � S�rie. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	SELECT ucrsSeriesServNum
	LcNo = ucrsSeriesServNum.numero
	
	**Paciente Generico
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		select TOP 1 utstamp, nome, no, estab from b_utentes where pacgen = 1
	ENDTEXT
	If !uf_gerais_actGrelha("", "ucrsDadosPacGenerico", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar informa��o do Paciente Gen�rico. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	
	
	lcStamp = uf_gerais_stamp()

	Select ucrsSeriesServ
	APPEND BLANK
	Replace ucrsSeriesServ.seriestamp WITH lcStamp 
	Replace ucrsSeriesServ.serieno 	WITH LcNo 
	Replace ucrsSeriesServ.dataInicio WITH DATE()
	Replace ucrsSeriesServ.dataFim WITH DATE()
	Replace ucrsSeriesServ.dataIniRep WITH DATE()
	
	Replace ucrsSeriesServ.horaInicio WITH "00:00"
	Replace ucrsSeriesServ.horaFim WITH "23:59"
	Replace ucrsSeriesServ.tempoinatividade WITH 90

	Replace ucrsSeriesServ.repetir WITH .f.
	Replace ucrsSeriesServ.criterio WITH "Todos os dias"
	Replace ucrsSeriesServ.valorRep WITH 1
	Replace ucrsSeriesServ.repnunca WITH .f.
	Replace ucrsSeriesServ.repapos WITH .t.
	Replace ucrsSeriesServ.repdia WITH .f.
	Replace ucrsSeriesServ.sessoes 	WITH 0
	Replace ucrsSeriesServ.dataFimRep WITH DATE()
	
	** paciente Gen�rico
	IF RECCOUNT("ucrsDadosPacGenerico") > 0
	
		Replace ucrsSeriesServ.utstamp WITH ucrsDadosPacGenerico.utstamp
		Replace ucrsSeriesServ.nome WITH ALLTRIM(ucrsDadosPacGenerico.nome)
		Replace ucrsSeriesServ.no WITH ucrsDadosPacGenerico.no
		Replace ucrsSeriesServ.estab WITH ucrsDadosPacGenerico.estab 
	
	ENDIF 
			
	**Apaga servi�os associadoa � Serie anterior
	SELECT ucrsServicosSerie
	GO Top
	SCAN
		delete
	ENDSCAN
	
			
	**Apaga recursos associadas � Serie anterior
	SELECT uCrsRecursosDaSerie
	SET FILTER TO 
	GO Top
	SCAN
		delete
	ENDSCAN
		
	
	**Apaga consumos associadas � Serie anterior
	SELECT uCrsSeriesConsumo
	GO Top
	SCAN
		delete
	ENDSCAN
	
	DO CASE 
		CASE lcTipo = 1 && Servi�o
			Select ucrsSeriesServ
			Replace ucrsSeriesServ.tipo with "Servi�os"
			REPLACE ucrsSeriesServ.serienome with "Planeamento Servi�o Nr." + ASTR(ucrsSeriesServ.serieno) 
			
		CASE lcTipo = 2 && disponibilidade Utilizador
			Select ucrsSeriesServ
			Replace ucrsSeriesServ.tipo with "Utilizadores"
			REPLACE ucrsSeriesServ.serienome with "Disp. Utilizador Nr." + ASTR(ucrsSeriesServ.serieno) 
		CASE lcTipo = 3 && disponibilidade Marca��o Atividade
			Select ucrsSeriesServ
			Replace ucrsSeriesServ.tipo with "Atividades"
			REPLACE ucrsSeriesServ.serienome with "Planeamento Marca��o Nr." + ASTR(ucrsSeriesServ.serieno) 
		CASE lcTipo = 4 && disponibilidade Recurso
			Select ucrsSeriesServ
			Replace ucrsSeriesServ.tipo with "Recursos"
			REPLACE ucrsSeriesServ.serienome with "Disp. Recurso Nr." + ASTR(ucrsSeriesServ.serieno) 
	ENDCASE 
	
	uf_SERIESSERV_alternaMenu()			
	uf_SERIESSERV_alteraVisualizacao()
	
	SERIESSERV.refresh
	SERIESSERV.pageframe1.page1.PageFrame1.page1.refresh

	

ENDFUNC


** 
FUNCTION uf_SERIESSERV_editar

	IF mySERIESSERVAlteracao==.f. AND mySERIESSERVIntroducao==.f.
		mySERIESSERVAlteracao=.t.
		uf_SERIESSERV_alternaMenu()			
	ENDIF
	SERIESSERV.refresh
	SERIESSERV.pageframe1.page1.PageFrame1.page1.refresh
	
	uf_SERIESSERV_alteraVisualizacao()

ENDFUNC


** 
FUNCTION uf_SERIESSERV_apagar

	IF !uf_perguntalt_chama("Pretende eliminar a S�rie de Servi�os?", "Sim", "N�o", 64)
		RETURN .f.
	ENDIF

*!*		** Desmarca marca��es associadas
*!*		IF uf_SERIESSERV_VerificaMarcacoesDaSerie() == .f.
*!*			uf_SERIESSERV_desmarcar()
*!*		ENDIF

	SELECT ucrsSeriesServ
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		DELETE b_series WHERE b_series.seriestamp = '<<ALLTRIM(ucrsSeriesServ.seriestamp)>>'
		DELETE b_seriesServicos WHERE b_seriesServicos.seriestamp = '<<ALLTRIM(ucrsSeriesServ.seriestamp)>>'
		
	ENDTEXT

	IF !uf_gerais_actGrelha("", "",lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A ELIMINAR A S�RIE DE SERVI�OS! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		lcValidaUpdate = .t.
	ENDIF
	
	**
	**Ultimo
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		 exec up_marcacoes_seriesUlt
	ENDTEXT

	IF !uf_gerais_actGrelha("", "ucrsUltimaSerie",lcSQL)
		MESSAGEBOX("Ocorreu um erro a validar a ultima s�rie de servi�os! por favor contacte o suporte.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
		
	SELECT ucrsUltimaSerie
	uf_SERIESSERV_chama(ucrsUltimaSerie.seriestamp)

	SERIESSERV.refresh
ENDFUNC



** Apaga marca��es asssociadas � serie
FUNCTION uf_SERIESSERV_desmarcar

*!*		SELECT ucrsSeriesServ
*!*		lcSQL = ""
*!*		TEXT TO lcSQL NOSHOW TEXTMERGE
*!*			DELETE b_cli_mr WHERE serieno = <<ucrsSeriesServ.serieno>>
*!*			DELETE B_cli_mrRecurso from b_cli_mr inner join B_cli_mrRecurso on b_cli_mr.mrstamp = B_cli_mrRecurso.mrstamp WHERE serieno = <<ucrsSeriesServ.serieno>>
*!*			DELETE b_cli_mrConsumo from b_cli_mr inner join b_cli_mrConsumo on b_cli_mr.mrstamp = b_cli_mrConsumo.mrstamp WHERE serieno = <<ucrsSeriesServ.serieno>>
*!*		ENDTEXT

*!*		IF !uf_gerais_actGrelha("", "",lcSQL)
*!*			MESSAGEBOX("OCORREU UM ERRO A ELIMINAR AS MARCA��ES ASSOCIADAS � S�RIE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
*!*		ELSE
*!*			**uf_perguntalt_chama("S�rie desmarcada com sucesso.", "", "OK", 64)
*!*		ENDIF

ENDFUNC


FUNCTION uf_SERIESSERV_camposObrigatorios

	SELECT ucrsSeriesServ
	
	** Descricao/Tipo
	IF empty(ucrsSeriesServ.serienome) OR empty(ucrsSeriesServ.tipo) 
		uf_perguntalt_chama("Existem campos obrigat�rios por preencher. Por favor verifique.", "", "OK", 64)
		RETURN .f.
	ENDIF	
		
	** Nome
	IF empty(ucrsSeriesServ.nome)
				
		DO CASE 
			CASE UPPER(ALLTRIM(ucrsSeriesServ.tipo)) == "SERVI�OS"
*!*					uf_perguntalt_chama("Existem campos obrigat�rios por preencher. A identifica��o do Utente � obrigat�ria.", "", "OK", 64)
*!*					RETURN .f.
			CASE UPPER(ALLTRIM(ucrsSeriesServ.tipo)) == "RECURSOS"
				uf_perguntalt_chama("Existem campos obrigat�rios por preencher. A identifica��o do Recurso � obrigat�ria.", "", "OK", 64)
				RETURN .f.
			CASE UPPER(ALLTRIM(ucrsSeriesServ.tipo)) == "UTILIZADORES"
				uf_perguntalt_chama("Existem campos obrigat�rios por preencher. A identifica��o do Utilizador/Especialista � obrigat�ria.", "", "OK", 64)
				RETURN .f.
		ENDCASE 
	ENDIF	
	
	IF UPPER(ALLTRIM(ucrsSeriesServ.tipo)) == "SERVI�OS" 
		
		IF EMPTY(ALLTRIM(ucrsSeriesServ.nome))
			uf_perguntalt_chama("A identifica��o do Utente � obrigat�ria.", "", "OK", 64)
			RETURN .f.
		ENDIF
		
		Select ucrsServicosSerie
		COUNT TO lcNumServicos
		IF lcNumServicos == 0
			uf_perguntalt_chama("N�o � possivel gravar a s�rie sem servi�os.", "", "OK", 64)
			RETURN .f.
		ENDIF 
	ENDIF
		
	RETURN .t.
	
	
ENDFUNC

** 
FUNCTION uf_SERIESSERV_gravar
	LOCAL lcCamposObrigatorios, lcValidaInsert, lcValidaUpdate, lcStamp, lcActualizaRecursos, lcValidaSequenciaPeriodos 

	STORE .f. TO lcCamposObrigatorios, lcValidaInsert, lcValidaUpdate, lcActualizaRecursos, lcValidaSequenciaPeriodos 
	
	** Regras grava��o S�rie
	lcCamposObrigatorios = uf_SERIESSERV_camposObrigatorios()
	IF lcCamposObrigatorios==.f.
		RETURN .f.
	ENDIF
	
*!*		select ucrsSeriesServ
*!*		IF ALLTRIM(UPPER(ucrsSeriesServ.tipo)) == "SERVI�OS" OR ALLTRIM(UPPER(ucrsSeriesServ.tipo)) == "RECURSOS"
*!*			** Regras grava��o Marca��o
*!*			lcValidaSequenciaPeriodos = uf_SERIESSERV_RegrasMarcacaoMensal()
*!*			IF EMPTY(lcValidaSequenciaPeriodos)
*!*				RETURN .f.
*!*			ENDIF 
*!*		ENDIF 
	
	** Marca��o ainda n�o existe vai fazer insert
	IF mySERIESSERVIntroducao == .t.
	
		** Trata Transac��es para garantir a Integridade de Dados
		IF uf_gerais_actGrelha("", "", [BEGIN TRANSACTION])
		
			**Numeracao da serie
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_marcacoes_seriesNum
			ENDTEXT
			If !uf_gerais_actGrelha("", "ucrsSeriesServNum", lcSql)
				uf_perguntalt_chama("N�o foi poss�vel verificar a numera��o a atrinuir � S�rie. Contacte o suporte.", "", "OK", 32)
				RETURN .f.
			ENDIF

			SELECT ucrsSeriesServ
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				INSERT INTO	b_series (	
						seriestamp
						,serieno
						,serienome
						,nome
						,no
						,estab
						,dirClinico
						,dirServico
						,dataInicio
						,sessoes
						,ousrinis
						,ousrdata
						,ousrhora
						,usrinis
						,usrdata
						,usrhora
						,horaInicio
						,horafim
						,segunda
						,terca
						,quarta
						,quinta
						,sexta
						,sabado
						,sempre
						,tododia
						,domingo
						,dataFim
						,dataIniRep
						,duracao
						,tipo
						,tempoinatividade
						,repetir
						,criterio
						,valorrep
						,repnunca
						,repapos
						,repdia
						,dataFimRep
						,template
						,site
				)VALUES (
						'<<ALLTRIM(ucrsSeriesServ.seriestamp)>>', 
						<<ucrsSeriesServNum.numero>>, 
						'<<ALLTRIM(ucrsSeriesServ.serienome)>>',
						'<<ALLTRIM(ucrsSeriesServ.nome)>>',
						<<ucrsSeriesServ.no>>,
						<<ucrsSeriesServ.estab>>,
						'<<ALLTRIM(ucrsSeriesServ.dirClinico)>>',
						'<<ALLTRIM(ucrsSeriesServ.dirServico)>>',
						'<<uf_gerais_getdate(ucrsSeriesServ.dataInicio,"SQL")>>',
						<<ucrsSeriesServ.sessoes>>
						,'<<m.m_chinis>>'
						,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
						,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
						,'<<m.m_chinis>>'
						,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
						,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
						,'<<ALLTRIM(ucrsSeriesServ.horaInicio)>>'
						,'<<ALLTRIM(ucrsSeriesServ.horafim)>>'
						,<<IIF(ucrsSeriesServ.segunda,1,0)>>
						,<<IIF(ucrsSeriesServ.terca,1,0)>>
						,<<IIF(ucrsSeriesServ.quarta,1,0)>>
						,<<IIF(ucrsSeriesServ.quinta,1,0)>>
						,<<IIF(ucrsSeriesServ.sexta,1,0)>>
						,<<IIF(ucrsSeriesServ.sabado,1,0)>>
						,<<IIF(ucrsSeriesServ.sempre,1,0)>>
						,<<IIF(ucrsSeriesServ.tododia,1,0)>>
						,<<IIF(ucrsSeriesServ.domingo,1,0)>>
						,'<<uf_gerais_getdate(ucrsSeriesServ.dataFim,"SQL")>>'
						,'<<uf_gerais_getdate(ucrsSeriesServ.dataIniRep,"SQL")>>'
						,<<ucrsSeriesServ.duracao>>
						,'<<ALLTRIM(ucrsSeriesServ.tipo)>>'
						,<<ucrsSeriesServ.tempoinatividade>>
						,<<IIF(ucrsSeriesServ.repetir,1,0)>>
						,'<<ALLTRIM(ucrsSeriesServ.criterio)>>'
						,<<ucrsSeriesServ.valorrep>>
						,<<IIF(ucrsSeriesServ.repnunca,1,0)>>
						,<<IIF(ucrsSeriesServ.repapos,1,0)>>
						,<<IIF(ucrsSeriesServ.repdia,1,0)>>
						,'<<uf_gerais_getdate(ucrsSeriesServ.dataFimRep,"SQL")>>'
						,<<IIF(ucrsSeriesServ.template,1,0)>>
						,'<<ALLTRIM(ucrsSeriesServ.site)>>'
				)
			ENDTEXT

			IF !uf_gerais_actGrelha("", "",lcSQL)
				MESSAGEBOX("OCORREU UM ERRO A GRAVAR A S�RIE DE SERVI�OS! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
				lcValidaInsert = .t.
			ENDIF			
						
			lcActualizaServicos = uf_SERIESSERV_insereServicosBD()	
			lcActualizaRecursos = UF_SERIESSERV_GRAVARECURSOS()	
			lcActualizaConsumos = UF_SERIESSERV_GRAVACONSUMOS()			
			
			
			IF lcValidaInsert == .f. AND lcActualizaServicos == .t. AND lcActualizaRecursos == .t. AND lcActualizaConsumos == .t.
				uf_gerais_actGrelha("", "",[COMMIT TRANSACTION])
			ELSE
				uf_gerais_actGrelha("", "",[ROLLBACK])
			Endif	
			
		ENDIF
	
	ELSE && Marca��o j� existe vai fazer update
		
		IF uf_gerais_actGrelha("", "",[BEGIN TRANSACTION])
		
			SELECT ucrsSeriesServ
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				UPDATE 
					b_series 
				SET 
					serienome= '<<ALLTRIM(ucrsSeriesServ.serienome)>>'
					,nome = '<<ALLTRIM(ucrsSeriesServ.nome)>>'
					,no = <<ucrsSeriesServ.no>>
					,estab = <<ucrsSeriesServ.estab>>
					,dirClinico = '<<ALLTRIM(ucrsSeriesServ.dirClinico)>>'
					,dirServico = '<<ALLTRIM(ucrsSeriesServ.dirServico)>>'
					,dataInicio = '<<uf_gerais_getdate(ucrsSeriesServ.dataInicio,"SQL")>>'
					,sessoes = <<ucrsSeriesServ.sessoes>>
					,usrinis = '<<m.m_chinis>>'
					,usrdata = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,usrhora = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
					,horaInicio = '<<ALLTRIM(ucrsSeriesServ.horaInicio)>>'
					,horafim = '<<ALLTRIM(ucrsSeriesServ.horafim)>>'
					,segunda = <<IIF(ucrsSeriesServ.segunda,1,0)>>
					,terca = <<IIF(ucrsSeriesServ.terca,1,0)>>
					,quarta = <<IIF(ucrsSeriesServ.quarta,1,0)>>
					,quinta = <<IIF(ucrsSeriesServ.quinta,1,0)>>
					,sexta = <<IIF(ucrsSeriesServ.sexta,1,0)>>
					,sabado = <<IIF(ucrsSeriesServ.sabado,1,0)>>
					,sempre = <<IIF(ucrsSeriesServ.sempre,1,0)>>
					,tododia = <<IIF(ucrsSeriesServ.tododia,1,0)>>
					,domingo = <<IIF(ucrsSeriesServ.domingo,1,0)>>
					,dataFim = '<<uf_gerais_getdate(ucrsSeriesServ.dataFim,"SQL")>>'
					,dataIniRep = '<<uf_gerais_getdate(ucrsSeriesServ.dataIniRep,"SQL")>>'
					,duracao = <<ucrsSeriesServ.duracao>>
					,tipo = '<<ALLTRIM(ucrsSeriesServ.tipo)>>'
					,tempoinatividade = <<ucrsSeriesServ.tempoinatividade>>
					,repetir = <<IIF(ucrsSeriesServ.repetir,1,0)>>
					,criterio = '<<ALLTRIM(ucrsSeriesServ.criterio)>>'
					,valorrep = <<ucrsSeriesServ.valorrep>>
					,repnunca = <<IIF(ucrsSeriesServ.repnunca,1,0)>>
					,repapos = <<IIF(ucrsSeriesServ.repapos ,1,0)>>
					,repdia = <<IIF(ucrsSeriesServ.repdia ,1,0)>>
					,dataFimRep = '<<uf_gerais_getdate(ucrsSeriesServ.dataFimRep,"SQL")>>'
					,template = <<IIF(ucrsSeriesServ.template,1,0)>>
					,site = '<<ALLTRIM(ucrsSeriesServ.site)>>'
				WHERE
					b_series.seriestamp = '<<ALLTRIM(ucrsSeriesServ.seriestamp )>>'
			ENDTEXT
						
			IF !uf_gerais_actGrelha("", "",lcSQL)
				MESSAGEBOX("OCORREU UM ERRO A GRAVAR A S�RIE DE SERVI�OS! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
				lcValidaUpdate = .t.
			ENDIF
						
			lcActualizaServicos = uf_SERIESSERV_insereServicosBD()	
			lcActualizaRecursos = UF_SERIESSERV_GRAVARECURSOS()	
			lcActualizaConsumos = UF_SERIESSERV_GRAVACONSUMOS()	
				
			IF lcValidaUpdate == .f. AND lcActualizaServicos  == .t. AND lcActualizaRecursos == .t. AND lcActualizaConsumos == .t. 
				uf_gerais_actGrelha("", "",[COMMIT TRANSACTION])
			ELSE
				uf_gerais_actGrelha("", "",[ROLLBACK])
			Endif	
	
		ENDIF

	ENDIF

	mySERIESSERVIntroducao= .f.
	mySERIESSERVAlteracao= .f.
	

	Select ucrsSeriesServ
	GO TOP

	SERIESSERV.serienome.setfocus

	**
	uf_SERIESSERV_alternaMenu()
	
	IF UPPER(ALLTRIM(ucrsSeriesServ.tipo)) == "SERVI�OS";
		 OR UPPER(ALLTRIM(ucrsSeriesServ.tipo)) == "ATIVIDADES" 
		
		SELECT ucrsPeriodosCalendarioMensal 
		LOCATE FOR !EMPTY(ucrsPeriodosCalendarioMensal.sel)
		IF FOUND()
	
			SELECT ucrsPeriodosCalendarioMensal
		
			lcDataPeriodo = ucrsPeriodosCalendarioMensal.dataIni
		
			** Efetua Marca��o - aki
			uf_SERIESSERV_marcar(.f.,.t.)
			** 
			

		ENDIF 		
	ENDIF 
		
	
ENDFUNC


**
FUNCTION uf_SERIESSERV_navegaMarcacao
		
		LOCAL lcDataPeriodo, lcMrstamp
		SELECT ucrsSessoesSerie
		lcDataPeriodo = DATE(ucrsSessoesSerie.ano,ucrsSessoesSerie.mes,ucrsSessoesSerie.dia)
		lcMrstamp = ucrsSessoesSerie.mrstamp
		uf_marcacoes_chama()

		SELECT ucrsMarcacoesCab
		Replace ucrsMarcacoesCab.ano WITH YEAR(lcDataPeriodo)
		Replace ucrsMarcacoesCab.mes WITH MONTH(lcDataPeriodo)
		Replace ucrsMarcacoesCab.mesDesc WITH uf_gerais_getMonthToMonthEx(MONTH(lcDataPeriodo))
		
		uf_marcacoes_AplicaDia(LEFT(uf_gerais_getdate(lcDataPeriodo,"DATA"),2))
		marcacoes.utente.value = ALLTRIM(SERIESSERV.nome.value)
		
		
		SELECT ucrsMarcacoesDia
		LOCATE FOR ucrsMarcacoesDia.mrstamp == ALLTRIM(lcMrstamp)
		IF FOUND()
			marcacoes.gridPesq.nome.setfocus
		ENDIF 			
		uf_SERIESSERV_exit()
		**

ENDFUNC 



FUNCTION uf_SERIESSERV_insereServicosBD
	**Trata Inser��o na tabela de US
	TEXT TO lcSQL NOSHOW textmerge
		delete
		FROM	b_seriesServicos
		WHERE	seriestamp = '<<ALLTRIM(ucrsSeriesServ.seriestamp)>>'
	ENDTEXT
	IF !uf_gerais_actgrelha("", "", lcSQL)
		MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ACTUALIZAR OS SERVI�OS DA S�RIE. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		return .f.
	ENDIF 
	
	SELECT ucrsServicosSerie
	IF RECCOUNT("ucrsServicosSerie")>0
		SELECT ucrsServicosSerie
		GO TOP
		SCAN
			lcStamp = uf_gerais_stamp()
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
								
				INSERT INTO b_seriesServicos
				(	serieservstamp
					,seriestamp
					,ref
					,design
					,qtt
					,pvp
					,total
					,coddiv
					,div
					,drno
					,drnome
					,duracao
					,ousrinis
					,ousrdata
					,ousrhora
					,usrinis
					,usrdata
					,usrhora
					,ordem
					,mrsimultaneo
					
				)
				VALUES	(		
					'<<ALLTRIM(lcStamp)>>'
					,'<<ALLTRIM(ucrsSeriesServ.seriestamp)>>'
					,'<<ALLTRIM(ucrsServicosSerie.ref)>>'
					,'<<ALLTRIM(ucrsServicosSerie.design)>>'
					,<<ucrsServicosSerie.qtt>>
					,<<ucrsServicosSerie.pvp>>
					,<<ucrsServicosSerie.total>>
					,'<<ALLTRIM(ucrsServicosSerie.coddiv)>>'
					,'<<ALLTRIM(ucrsServicosSerie.div)>>'
					,<<ucrsServicosSerie.drno>>
					,'<<ALLTRIM(ucrsServicosSerie.drnome)>>'
					,<<ucrsServicosSerie.duracao>>
					,'<<m.m_chinis>>'
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
					,'<<m.m_chinis>>'
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
					,<<ucrsServicosSerie.ordem>>
					,<<ucrsServicosSerie.mrsimultaneo>>
				)
			ENDTEXT

*!*	_CLIPTEXT = lcSQL
*!*	messagebox(lcSQL)
			
			IF !uf_gerais_actgrelha("", "", lcSQL)
				MESSAGEBOX("OCORREU UM ERRO A ACTUALIZAR OS SERVI�OS DA SERIE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
				return .f.
			ENDIF
		ENDSCAN
	ENDIF

	return .t.
ENDFUNC


**
FUNCTION uf_SERIESSERV_NovoServico
	
	IF mySERIESSERVIntroducao== .f. AND mySERIESSERVAlteracao== .f.
		**
		SELECT ucrsServicosSerie
		IF !EMPTY(ucrsServicosSerie.ref)
			uf_stocks_chama(ucrsServicosSerie.ref)
		ENDIF
		RETURN .f.
	ENDIF 
	
	** valida se j� existe a areferencia associada � Serie
	uf_pesqstocks_chama("SERIESSERV")

	**
	uf_seriesserv_calculaDuracaoSessao()
			
	SERIESSERV.PageFrame1.page1.PageFrame1.page1.GridServicosSerie.Refresh
ENDFUNC



**
FUNCTION uf_SERIESSERV_ApagaServico
	LOCAL lcRef 
	lcRef = ""
	
	IF mySERIESSERVIntroducao== .f. AND mySERIESSERVAlteracao== .f.
		RETURN .f.
	ENDIF 
		
	select ucrsServicosSerie
	lcRef = ucrsServicosSerie.ref
	DELETE	
	

	**Apaga recursos associadas � Serie anterior
	SELECT uCrsRecursosDaSerie
	GO TOP 
	SCAN FOR UPPER(ALLTRIM(ref)) == UPPER(ALLTRIM(lcRef))
		delete
	ENDSCAN
		
	**Apaga consumos associadas � Serie anterior
	SELECT uCrsSeriesConsumo
	GO Top
	SCAN FOR UPPER(ALLTRIM(ref)) == UPPER(ALLTRIM(lcRef))
		delete
	ENDSCAN
	
	
	Select ucrsServicosSerie
	lcPos = recno()
	select ucrsServicosSerie
	TRY
		IF lcPos>1
			go lcPos-1
		ENDIF
	CATCH
		**
	ENDTRY

	SERIESSERV.PageFrame1.page1.PageFrame1.page1.GridServicosSerie.REFRESH
			
	**
	uf_seriesserv_calculaDuracaoSessao()	 
ENDFUNC



*!*	**
FUNCTION uf_seriesserv_dynamicCurrentControl
	
	DO CASE
		CASE !EMPTY(ucrsPeriodosDisponiveisCombinados.cabecalho)
			RETURN 'Text2'
		OTHERWISE
			RETURN 'Text1'
	ENDCASE

ENDFUNC 


**
FUNCTION uf_SERIESSERV_marcar
	LPARAMETERS lcData, lcMostraMensagens && Caso venha preenchido, marca��o de repeti��es
	
	LOCAL lcValorUtente, lcValorEntidade
	STORE 0 to lcValorUtente, lcValorEntidade
	
	** Se o utente n�o tiver sido escolhido coloca informa��o do pacgen = 1 (b_utentes), caso n�o exista paciente gen�rico n�o deixa gravar
	IF EMPTY(ALLTRIM(ucrsSeriesServ.nome))
		TEXT TO lcSQL NOSHOW TEXTMERGE
			select TOP 1 utstamp, nome, no, estab from B_utentes where pacgen = 1
		ENDTEXT 
		IF !uf_gerais_actGrelha("", "ucrsPacGen",lcSQL)
			MESSAGEBOX("OCORREU UM ERRO NA VERIFICA��O DE EXISTENCIA DO PACIENTE GEN�RICO! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
		IF RECCOUNT("ucrsPacGen") == 0
			uf_perguntalt_chama("Paciente Gen�rico n�o configurado. N�o pode proceder � marca��o sem selecionar um utente.","OK","",16)
			RETURN .f.
		ENDIF 
		
		Select ucrsSeriesServ
		Select ucrsPacGen
		Replace ucrsSeriesServ.utstamp WITH ucrsPacGen.utstamp
		Replace ucrsSeriesServ.nome WITH ALLTRIM(ucrsPacGen.nome)
		Replace ucrsSeriesServ.no WITH ucrsPacGen.no
		Replace ucrsSeriesServ.estab WITH ucrsPacGen.estab 
		
	ENDIF 
	
	IF EMPTY(lcData)
		lcData = DATE(VAL(SERIESSERV.ano),VAL(SERIESSERV.mes),VAL(SERIESSERV.dia))
	ENDIF

	select MIN(horaIni) as horaIni, MAX(HoraFim) as horaFim from ucrsPeriodosCalendarioMensal WHERE !EMPTY(sel) INTO CURSOR ucrsPeriodosCalendarioMensalAgrupados READWRITE 
	select ucrsPeriodosCalendarioMensalAgrupados 
	GO Top


	** Stamp 
	Select ucrsRecursosCalendarioMensal
	lcCombinacao  = ALLTRIM(ucrsRecursosCalendarioMensal.descricao)
	lcPVP = ucrsRecursosCalendarioMensal.PVP
	lcCompart = ucrsRecursosCalendarioMensal.Compart
	lcUtente = ucrsRecursosCalendarioMensal.utente
	lcRef = ucrsRecursosCalendarioMensal.ref
	
	** Cria Stamp para inser��o dos dados da marca��o
	lcMrStamp = uf_gerais_stamp()
	Select ucrsSeriesServ
	
			
	TEXT TO lcSQL NOSHOW TEXTMERGE
					
		insert into marcacoes (
			mrstamp
			,data
			,dataFim
			,hinicio
			,hfim
			,utstamp
			,nome
			,no
			,estab
			,serieno
			,serienome
			,sessao
			,estado
			,mrno
			,combinacao
			,site
			,id_us
			,data_cri
			,id_us_alt
			,data_alt
		) values(
			'<<lcMrStamp>>'
			,'<<uf_gerais_getdate(lcData,"SQL")>>'
			,'<<uf_gerais_getdate(lcData,"SQL")>>'
			,'<<ucrsPeriodosCalendarioMensalAgrupados.horaIni>>'
			,'<<ucrsPeriodosCalendarioMensalAgrupados.horaFim>>'
			,<<IIF(EMPTY(ALLTRIM(ucrsSeriesServ.utstamp)) or ISNULL(ucrsSeriesServ.utstamp),'null',"'" + ALLTRIM(ucrsSeriesServ.utstamp) +"'")>>
			,'<<ucrsSeriesServ.nome>>'
			,<<ucrsSeriesServ.no>>
			,<<ucrsSeriesServ.estab>>
			,<<ucrsSeriesServ.serieno>>
			,'<<ucrsSeriesServ.serienome>>'
			,0
			,'MARCADO'
			,(select isnull(MAX(mrno),0)+1 from marcacoes)
			,'<<ALLTRIM(lcCombinacao)>>'
			,'<<ALLTRIM(ucrsRecursosCalendarioMensal.local)>>'
			,<<ch_userno>>
			,dateadd(HOUR, <<difhoraria>>, getdate())
			,<<ch_userno>>
			,dateadd(HOUR, <<difhoraria>>, getdate())
		)
	ENDTEXT
	
*!*	_CLIPTEXT = lcSQL
*!*	MESSAGEBOX(lcSQL)

	IF !uf_gerais_actGrelha("", "",lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A INSERIR MARCA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	lcServStamp = uf_gerais_stamp()
	lcOrdemComp = 0
	lcValorUtente = lcPVP
	
	
	Select ucrsDadosComparticipacaoSerie
	IF RECCOUNT("ucrsDadosComparticipacaoSerie")>0
		Select ucrsDadosComparticipacaoSerie
		GO top
		SCAN

			lcmarcacoesCompStamp = uf_gerais_stamp()
			lcOrdemComp  = lcOrdemComp  +1 
			TEXT TO lcSQL NOSHOW TEXTMERGE
							
				insert into cpt_marcacoes(
					servcompmrstamp
					,servmrstamp
					,mrstamp
					,id_cp
					,ordem
					,valor
					,entidade
					,entidadeno
					,entidadeestab
					,compart
					,maximo
					,tipoCompart								
				) values(
					'<<lcmarcacoesCompStamp>>'
					,<<IIF(EMPTY(ALLTRIM(lcServStamp)) or ISNULL(lcServStamp),'null',"'" + ALLTRIM(lcServStamp) +"'")>>
					,<<IIF(EMPTY(ALLTRIM(lcMrStamp)) or ISNULL(lcMrStamp),'null',"'" + ALLTRIM(lcMrStamp) +"'")>>
					,'<<ucrsDadosComparticipacaoSerie.id>>'
					,<<lcOrdemComp>>
					,<<ucrsDadosComparticipacaoSerie.valorEntidade>>
					,'<<ALLTRIM(ucrsDadosComparticipacaoSerie.enome)>>'
					,<<ucrsDadosComparticipacaoSerie.eno>>
					,<<ucrsDadosComparticipacaoSerie.eestab>>
					,<<ucrsDadosComparticipacaoSerie.compart>>
					,<<ucrsDadosComparticipacaoSerie.maximo>>
					,'<<ALLTRIM(ucrsDadosComparticipacaoSerie.tipoCompart)>>'
				)
			ENDTEXT
*!*	_CLIPTEXT = lcSQL
*!*	MESSAGEBOX(lcSQL)				

			IF !uf_gerais_actGrelha("", "",lcSQL)
				MESSAGEBOX("OCORREU UM ERRO A INSERIR SERVI�O DE MARCA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
				RETURN .f.
			ENDIF
		ENDSCAN 
	ENDIF 
	
	&& For�a posi��o cursor
	Select ucrsServicosSerie
	LOCATE FOR ALLTRIM(ucrsServicosSerie.ref) == ALLTRIM(lcRef)
	IF FOUND()
		lcDesign = ALLTRIM(ucrsServicosSerie.design)
	ELSE
	 	lcDesign = ""
	ENDIF 	
	TEXT TO lcSQL NOSHOW TEXTMERGE
					
		insert into marcacoesServ (
			servmrstamp
			,mrstamp
			,ref
			,design
			,duracao
			,qtt
			,pvp
			,compart
			,total
			,dataInicio
			,dataFim
			,hinicio
			,hfim
			,ordem
		) values(
			'<<lcServStamp>>'
			,'<<lcMrStamp>>'
			,'<<lcRef>>'
			,'<<ALLTRIM(lcDesign)>>'
			,<<ucrsServicosSerie.duracao>>
			,<<ucrsServicosSerie.qtt>>
			,<<lcPvp>>
			,<<lcCompart>>
			,<<lcUtente>>
			,'<<uf_gerais_getdate(lcData,"SQL")>>'
			,'<<uf_gerais_getdate(lcData,"SQL")>>'
			,'<<ucrsPeriodosCalendarioMensalAgrupados.horaIni>>'
			,'<<ucrsPeriodosCalendarioMensalAgrupados.horaFim>>'
			,1
		)
	ENDTEXT
	
*!*	_CLIPTEXT = lcSQL
*!*	MESSAGEBOX(lcSQL)

	IF !uf_gerais_actGrelha("", "",lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A INSERIR SERVI�O DE MARCA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF

	**Grava Recursos associados � marca��o
	SELECT ucrsRecursosCalendarioMensal
	lcStampCombinacao = ucrsRecursosCalendarioMensal.stamp
	SELECT ucrsMapeamentoRecursosCombinacao
	GO Top
	SCAN FOR ALLTRIM(ucrsMapeamentoRecursosCombinacao.stamp) == ALLTRIM(lcStampCombinacao)
		
		TEXT TO lcSQL NOSHOW TEXTMERGE
			
			insert into marcacoesRecurso(
				id
				,mrstamp
				,nome
				,no
				,tipo
				,ref
			) 
			SELECT 
				id = (select ISNULL(MAX(id),0) + 1 from marcacoesRecurso)
				,mrstamp = '<<lcMrStamp>>'
				,nome = '<<ALLTRIM(ucrsMapeamentoRecursosCombinacao.nome)>>'
				,no = <<ucrsMapeamentoRecursosCombinacao.no>>
				,tipo = '<<ALLTRIM(ucrsMapeamentoRecursosCombinacao.tipo)>>'
				,ref = '<<ALLTRIM(ucrsMapeamentoRecursosCombinacao.ref)>>'
		ENDTEXT
*!*	_CLIPTEXT = lcSQL
*!*	MESSAGEBOX(lcSQL)				
		IF !uf_gerais_actGrelha("", "",lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A INSERIR RECURSOS NA MARCA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	ENDSCAN 
	**********************************************


	uf_seriesserv_indicadoresCalendarioMensal()
	UF_SERIESSERV_afterRowColumnChange()

	IF EMPTY(lcMostraMensagens)
		uf_perguntalt_chama("Marca��o efetuada com sucesso.", "", "OK", 64)
	ENDIF 

	**
	Select ucrsSeriesServ

	** 
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_marcacoes_sessoes <<ucrsSeriesServ.serieno>>
	ENDTEXT	
	IF !uf_gerais_actgrelha("seriesserv.Pageframe1.Page5.Container1.GridSessoes", "ucrsSessoesSerie", lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR AS SESS�ES ASSOCIADAS � S�RIE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	
ENDFUNC


**
FUNCTION uf_SERIESSERV_reportSessoes
	LOCAL lcSql 
	lcSql = ""
	Select ucrsSeriesServ

	** Construir string com parametros
	lcSql =	"&stamp="	+ ALLTRIM(ucrsSeriesServ.seriestamp) + "&site="	+ ALLTRIM(mysite)

	uf_gerais_chamaReport("relatorio_marcacoes_Sessoes",lcSql)

ENDFUNC



**
FUNCTION uf_SERIESSERV_RecursoApaga

	IF mySERIESSERVIntroducao == .f. AND mySERIESSERVAlteracao == .f.
		RETURN .f.
	ENDIF 
		
		
	select uCrsRecursosDaSerie
	DELETE 
	
	Select uCrsRecursosDaSerie
	lcPos = recno()
	select uCrsRecursosDaSerie
	TRY
		IF lcPos>1
			go lcPos-1
		ENDIF
	CATCH
		**
	ENDTRY

	SERIESSERV.pageframe1.page1.PageFrame1.page1.REFRESH
	
	select uCrsRecursosDaSerie
	GO BOTTOM 
		 
ENDFUNC


**
FUNCTION uf_SERIESSERV_RecursoNovo

	IF mySERIESSERVIntroducao == .f. AND mySERIESSERVAlteracao == .f.
		RETURN .f.
	ENDIF 

	** No caso da serie ser do tipo servi�os � obrigadtorio especificar um servi�o
	select ucrsSeriesServ
	IF ALLTRIM(UPPER(ucrsSeriesServ.tipo)) == "SERVI�OS" AND EMPTY(ALLTRIM(ucrsServicosSerie.ref))
		uf_perguntalt_chama("Selecione o servi�o ao qual vai adicionar recursos.", "", "OK", 64)
		RETURN .f.
	ENDIF 
	
	**
	uf_PESQSERIESRECURSOS_Chama('')


ENDFUNC


** Excepcoes
FUNCTION uf_SERIESSERV_gravaRecursos
	Select ucrsSeriesServ
	
	**Trata Inser��o na tabela de REcursos
	TEXT TO lcSQL NOSHOW textmerge
		DELETE FROM	b_seriesRecurso WHERE seriestamp= '<<ALLTRIM(ucrsSeriesServ.seriestamp)>>'
	ENDTEXT
	IF !uf_gerais_actgrelha("", "", lcSQL)
		MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ACTUALIZAR OS RECURSOS AFECTOS � ACTIVIDADE. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		return .f.
	ENDIF 
	
	Select uCrsRecursosDaSerie
	SET FILTER TO 
	
	SELECT uCrsRecursosDaSerie
	IF RECCOUNT("uCrsRecursosDaSerie")>0
		SELECT uCrsRecursosDaSerie
		GO TOP
		SCAN
			
			lcStamp = uf_gerais_stamp()
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
								
				INSERT INTO b_seriesRecurso
				(	serierecstamp
					,seriestamp
					,ref
					,nome
					,no
					,stamp
					,tipo
					,ousrinis
					,ousrdata
					,ousrhora
				)
				VALUES	(		
					'<<ALLTRIM(lcStamp)>>'
					,<<IIF(EMPTY(ALLTRIM(ucrsSeriesServ.seriestamp)) or ISNULL(ucrsSeriesServ.seriestamp),'null',"'" + ALLTRIM(ucrsSeriesServ.seriestamp) +"'")>>
					,'<<ALLTRIM(uCrsRecursosDaSerie.ref)>>'
					,'<<ALLTRIM(uCrsRecursosDaSerie.nome)>>'
					,<<uCrsRecursosDaSerie.no>>
					,<<IIF(EMPTY(ALLTRIM(uCrsRecursosDaSerie.stamp)) or ISNULL(uCrsRecursosDaSerie.stamp),'null',"'" + ALLTRIM(uCrsRecursosDaSerie.stamp) +"'")>>
					,'<<ALLTRIM(uCrsRecursosDaSerie.tipo)>>'
					,'<<m_chinis>>'
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
					)
			ENDTEXT
*!*	_CLIPTEXT = lcSQL
*!*	MESSAGEBOX(lcSQL)
			IF !uf_gerais_actgrelha("", "", lcSQL)
				MESSAGEBOX("OCORREU UM ERRO A ACTUALIZAR OS RECURSOS AFECTOS � ACTIVIDADE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
				return .f.
			ENDIF
		ENDSCAN
	ENDIF
	Select uCrsRecursosDaSerie
	GO top
	
ENDFUNC


**
FUNCTION UF_SERIESSERV_GRAVACONSUMOS
	**Trata Inser��o na tabela de b_seriesConsumo
	TEXT TO lcSQL NOSHOW textmerge
		DELETE FROM	b_seriesConsumo WHERE seriestamp= '<<ALLTRIM(ucrsSeriesServ.seriestamp)>>'
	ENDTEXT
	IF !uf_gerais_actgrelha("", "", lcSQL)
		MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ACTUALIZAR OS CONSUMOS AFECTOS � ACTIVIDADE. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		return .f.
	ENDIF 

	SELECT uCrsSeriesConsumo
	SET FILTER TO 
	IF RECCOUNT("uCrsSeriesConsumo")>0
		SELECT uCrsSeriesConsumo
		GO TOP
		SCAN
			lcStamp = uf_gerais_stamp()
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
								
				INSERT INTO b_seriesConsumo
				(	consumoseriestamp
					,seriestamp
					,ref
					,reforiginal
					,duracao
					,qtt
				)
				VALUES	(		
					'<<ALLTRIM(lcStamp)>>'
					,<<IIF(EMPTY(ALLTRIM(ucrsSeriesServ.seriestamp)) or ISNULL(ucrsSeriesServ.seriestamp),'null',"'" + ALLTRIM(ucrsSeriesServ.seriestamp) +"'")>>
					,'<<ALLTRIM(uCrsSeriesConsumo.ref)>>'
					,'<<ALLTRIM(uCrsSeriesConsumo.reforiginal)>>'
					,<<uCrsSeriesConsumo.duracao>>
					,<<uCrsSeriesConsumo.qtt>>
					)
			ENDTEXT

			IF !uf_gerais_actgrelha("", "", lcSQL)
				MESSAGEBOX("OCORREU UM ERRO A ACTUALIZAR OS CONSUMOS AFECTOS � ACTIVIDADE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
				return .f.
			ENDIF
		ENDSCAN
	ENDIF
	Select uCrsSeriesConsumo
	GO Top

ENDFUNC 


**
Function UF_SERIESSERV_afterRowColumnChange

	Select ucrsServicosSerie
	Select uCrsRecursosDaSerie
	GO Top
	SET FILTER TO UPPER(ALLTRIM(uCrsRecursosDaSerie.ref)) == ALLTRIM(UPPER(ucrsServicosSerie.ref))

	Select uCrsSeriesConsumo
	GO Top
	SET FILTER TO UPPER(ALLTRIM(uCrsSeriesConsumo.reforiginal)) == ALLTRIM(UPPER(ucrsServicosSerie.ref))

	SERIESSERV.Pageframe1.Page1.Pageframe1.Page1.lblRecursos.caption = "Recursos(" + ALLTRIM(ucrsServicosSerie.design)+ ")"
	SERIESSERV.Pageframe1.Page1.Pageframe1.Page1.labelConsumos.caption = "Consumos(" + ALLTRIM(ucrsServicosSerie.design)+ ")"
	SERIESSERV.Pageframe1.Page1.PageFrame1.page1.refresh
	
ENDFUNC



FUNCTION uf_seriesserv_consumonovo
	LOCAL lcRefs 
	STORE 0 TO lcRefs 

	IF mySERIESSERVIntroducao == .f. AND mySERIESSERVAlteracao == .f.
		RETURN .f.
	ENDIF 
	
	uf_pesqstocks_chama('CONSUMO')

ENDFUNC 



FUNCTION uf_seriesserv_consumoapaga
	IF mySERIESSERVIntroducao == .f. AND mySERIESSERVAlteracao == .f.
		RETURN .f.
	ENDIF 		
		
	select uCrsSeriesConsumo
	DELETE 
	
	Select uCrsSeriesConsumo
	lcPos = recno()
	select uCrsSeriesConsumo
	TRY
		IF lcPos>1
			go lcPos-1
		ENDIF
	CATCH
		**
	ENDTRY

	SERIESSERV.Pageframe1.Page1.PageFrame1.page1.Refresh	
	
	select uCrsSeriesConsumo
	GO BOTTOM 
ENDFUNC 


**
FUNCTION uf_seriesserv_ultimo
	LOCAL lcSerieStamp 
	
	**Ultimo
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		 exec up_marcacoes_seriesUlt
	ENDTEXT
	IF !uf_gerais_actGrelha("", "ucrsUltimaSerie",lcSQL)
		MESSAGEBOX("Ocorreu um erro a validar a ultima s�rie de servi�os! por favor contacte o suporte.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	SELECT ucrsUltimaSerie
	lcSerieStamp = ucrsUltimaSerie.seriestamp
	uf_SERIESSERV_chama(lcSerieStamp)
	
	
*!*		uf_SERIESSERV_CalculaCursores(lcSerieStamp)

*!*		WITH SERIESSERV.pageframe1.page1.pageframe1.page1.CtnRepeticao	
*!*			IF !EMPTY(ucrsSeriesServ.repetir)
*!*				.Visible =  .t.
*!*			ELSE
*!*				.Visible =  .f.
*!*			ENDIF 
*!*		ENDWITH 
ENDFUNC




**
FUNCTION uf_seriesserv_calculaDuracaoSessao
	LOCAL lcDuracao
	lcDuracao = 0
	IF mySERIESSERVIntroducao == .f. AND mySERIESSERVAlteracao == .f.
		RETURN .f.
	ENDIF

	select ucrsServicosSerie 
	Go Top
	
	Select ordem,MAX(duracao) as duracao from ucrsServicosSerie GROUP BY ordem INTO CURSOR ucrsDuracaoSessao READWRITE

	Select ucrsDuracaoSessao 
	GO TOP
	CALCULATE SUM(duracao) TO lcDuracao
	
	SELECT ucrsSeriesServ
	Replace ucrsSeriesServ.duracao WITH lcDuracao 
	
	seriesserv.pageframe1.page1.PageFrame1.page1.duracao.refresh
	
	
ENDFUNC


*********************************************************************************************************************
**
** CALCULO MENSAL
**
*********************************************************************************************************************


**
FUNCTION uf_SERIESSERV_alteraVisualizacao
	
	select ucrsSeriesServ
	lcModo = UPPER(ucrsSeriesServ.tipo)
	
	WITH SERIESSERV

		DO CASE
			CASE ALLTRIM(UPPER(lcModo)) == "SERVI�OS"
			
				SERIESSERV.menu1.estado("", "SHOW")
				&&SERIESSERV.menu1.estado("marcar", "HIDE") 		
				IF !EMPTY(ucrsSeriesServ.repetir)
					SERIESSERV.menu1.estado("remarcarRepeticoes", "SHOW")
				ELSE 
					SERIESSERV.menu1.estado("remarcarRepeticoes", "HIDE")
				ENDIF 
				
				.pageFrame1.activepage = 1
				.pageFrame1.Page1.pageFrame1.activepage = 1
				
				.lbl11.caption = "Utente"
				.lbl11.visible = .t.
				.nome.visible = .t.
				.no.visible = .t.
				.estab.visible = .t.
							
				
				WITH .pageFrame1.Page1.pageFrame1.page1
					.LabelServicos.visible =  .t.
					.GridServicosSerie.visible =  .t.
					.BtnimgAddServicos.visible =  .t.
					.BtnimgDelServicos.visible =  .t.
					
					.LabelConsumos.visible = .t.
					.GridConsumos.visible = .t.
					.BtnimgAddConsumo.visible = .t.
					.BtnimgDelConsumo.visible = .t.
					
					SERIESSERV.pageFrame1.Page1.pageFrame1.page1.lblRecursos.left = SERIESSERV.pageFrame1.Page1.pageFrame1.page1.LabelConsumos.left
					SERIESSERV.pageFrame1.Page1.pageFrame1.page1.gridRecursos.left = SERIESSERV.pageFrame1.Page1.pageFrame1.page1.GridConsumos.left
					SERIESSERV.pageFrame1.Page1.pageFrame1.page1.gridRecursos.width = SERIESSERV.pageFrame1.Page1.pageFrame1.page1.GridConsumos.width
					SERIESSERV.pageFrame1.Page1.pageFrame1.page1.BtnimgAddRecurso.left = SERIESSERV.pageFrame1.Page1.pageFrame1.page1.BtnimgAddConsumo.left
					SERIESSERV.pageFrame1.Page1.pageFrame1.page1.BtnimgDelRecurso.left = SERIESSERV.pageFrame1.Page1.pageFrame1.page1.BtnimgDelConsumo.left
					
				ENDWITH 							
							
				seriesserv.refresh
				
			CASE ALLTRIM(UPPER(lcModo)) == "ATIVIDADES"
			
				SERIESSERV.menu1.estado("", "SHOW")
	 			&&SERIESSERV.menu1.estado("marcar", "HIDE") 		
				
				IF !EMPTY(ucrsSeriesServ.repetir)
					SERIESSERV.menu1.estado("remarcarRepeticoes", "SHOW")
				ELSE 
					SERIESSERV.menu1.estado("remarcarRepeticoes", "HIDE")
				ENDIF 
				
				.pageFrame1.activepage = 1
				.pageFrame1.Page1.pageFrame1.activepage = 1
			
				.lbl11.caption = ""
				.lbl11.visible = .f.
				.nome.visible = .f.
				.no.visible = .f.
				.estab.visible = .f.
				
				WITH SERIESSERV.pageFrame1.Page1.pageFrame1.page1
					.LabelServicos.visible =  .f.
					.GridServicosSerie.visible =  .f.
					.BtnimgAddServicos.visible =  .f.
					.BtnimgDelServicos.visible =  .f.
				
					.LabelConsumos.visible = .f.
					.GridConsumos.visible = .f.
					.BtnimgAddConsumo.visible = .f.
					.BtnimgDelConsumo.visible = .f.
													
					SERIESSERV.pageFrame1.Page1.pageFrame1.page1.lblRecursos.left = 10
					SERIESSERV.pageFrame1.Page1.pageFrame1.page1.gridRecursos.left = 10
&&					SERIESSERV.pageFrame1.Page1.pageFrame1.page1.gridRecursos.width = SERIESSERV.pageFrame1.Page1.pageFrame1.page1.shape.width - 20
					SERIESSERV.pageFrame1.Page1.pageFrame1.page1.BtnimgAddRecurso.left = SERIESSERV.pageFrame1.Page1.pageFrame1.page1.BtnimgAddServicos.left
					SERIESSERV.pageFrame1.Page1.pageFrame1.page1.BtnimgDelRecurso.left = SERIESSERV.pageFrame1.Page1.pageFrame1.page1.BtnimgDelServicos.left

					
				ENDWITH 				
							
				seriesserv.refresh
				
			CASE ALLTRIM(UPPER(lcModo)) == "RECURSOS"

	 			SERIESSERV.menu1.estado("dispRecursos, remarcarRepeticoes", "HIDE") 		
			
				.pageFrame1.activepage = 1
				.pageFrame1.Page1.pageFrame1.activepage = 2
				
				.lbl11.caption = "Recurso"
				.lbl11.visible = .t.
				.nome.visible = .t.
				.no.visible = .t.
				.estab.visible = .f.

				seriesserv.refresh
			
					
			CASE ALLTRIM(UPPER(lcModo)) == "UTILIZADORES"

	 			SERIESSERV.menu1.estado("dispRecursos, remarcarRepeticoes", "HIDE") 		
			
				.pageFrame1.activepage = 1
				.pageFrame1.Page1.pageFrame1.activepage = 2
				
				.lbl11.caption = "Utilizador"
				.lbl11.visible = .t.
				.nome.visible = .t.
				.no.visible = .t.
				.estab.visible = .f.
			
				seriesserv.refresh
								
			OTHERWISE

	 			SERIESSERV.menu1.estado("dispRecursos, remarcarRepeticoes", "HIDE") 		
			
				.pageFrame1.activePage = 6
							
				.lbl11.caption = ""
				.lbl11.visible = .f.
				.nome.visible = .f.
				.no.visible = .f.
				.estab.visible = .f.
			
				seriesserv.refresh
		ENDCASE
		
	ENDWITH 

ENDFUNC


**
FUNCTION uf_seriesserv_atualizaCalendarioMensal
	LOCAL i,j, LcNumDiasCalculo, lcDesign 
	i = 0
	LcNumDiasCalculo = 0
	
	**
	seriesserv.Pageframe1.Page5.GridDoc.recordSource = ""
	WITH seriesserv.Pageframe1.Page5.GridDoc
		FOR j=1 TO .columnCount
			.DeleteColumn
		ENDFOR
	ENDWITH 

	IF USED("ucrsRecursosCalendarioMensal")
		FECHA("ucrsRecursosCalendarioMensal")
	ENDIF

	LcNumDiasCalculo = uf_gerais_getParameter("ADM0000000237","NUM")
	IF LcNumDiasCalculo == 0
		LcNumDiasCalculo = 65
	ENDIF


	lcVarCreateCursor = "stamp c(25), descricao c(245), mrsimultaneo n(9,0), duracao n(9,0), ordem n(9), filtro l, local c(20), pvp n(19,3),compart n(19,3), utente n(19,3),  ref c(18), utilizador n(19,3), design c(254), nomeEspecialista c(254)"
*!*		lcData = DATE()-5
*!*		lcDataFim = DATE() + LcNumDiasCalculo
	lcData = DATE(seriesserv.anoPlaneamento,seriesserv.mesPlaneamento,1)
	lcDataFim = GOMONTH(lcData, +1)
	
	
	DO WHILE lcData < lcDataFim 
		lcVarCreateCursor = lcVarCreateCursor + ", d" + uf_gerais_getdate(lcData,"SQL") + " c(20)"
		lcData = lcData + 1
	ENDDO
	CREATE CURSOR ucrsRecursosCalendarioMensal(&lcVarCreateCursor)

	lcData = DATE(seriesserv.anoPlaneamento,seriesserv.mesPlaneamento,1)
	lcDataFim = GOMONTH(lcData, +1)

	** Primeria Coluna (Disponibilidades Recursos)
	WITH seriesserv.Pageframe1.Page5.GridDoc
		
		**
		.recordSource = "ucrsRecursosCalendarioMensal"
		
		** Local
		i = 1
		.AddColumn 
		.RowHeight = uf_gerais_getRowHeight()
		.columns(i).ControlSource = "ucrsRecursosCalendarioMensal.local"
		.columns(i).text1.ControlSource = "ucrsRecursosCalendarioMensal.local"
		.columns(i).header1.FontName = "Verdana"
		.columns(i).header1.Fontsize = 7
		.columns(i).header1.caption = "LOCAL" 
		.columns(i).header1.Alignment = 2
		.columns(i).header1.FontBold = .f.
		.columns(i).FontName = "Verdana"
		.columns(i).Fontsize = 8
		.columns(i).ReadOnly = .t.
		.columns(i).text1.ReadOnly = .t.
		.Columns(i).columnorder = i
		.Columns(i).dynamicBackColor="uf_seriesserv_calendarioMensalBackColor()"				
		.Columns(i).width = 150
		.Columns(i).InputMask=""
		.Columns(i).text1.InputMask=""
		.columns(i).text1.BorderStyle =  0
		.columns(i).text1.ForeColor = RGB(0,0,0)
		.columns(i).text1.SelectedForeColor = RGB(0,0,0)
		.columns(i).text1.FontBold = .t.
		.columns(i).text1.MousePointer = 15
		.columns(i).text1.SelectedBackColor = RGB(42,143,154)
		.Columns(i).Format="RTK"
		.Columns(i).text1.Format="RTK"
		.Columns(i).text1.hideSelection=.t.	
		***********************************************
		
		i = 2		
		.AddColumn 
		.RowHeight = uf_gerais_getRowHeight()
		.columns(i).ControlSource = "ucrsRecursosCalendarioMensal.descricao"
		.columns(i).text1.ControlSource = "ucrsRecursosCalendarioMensal.descricao"
		.columns(i).header1.FontName = "Verdana"
		.columns(i).header1.Fontsize = 7
		.columns(i).header1.caption = "COMBINA��O" 
		.columns(i).header1.Alignment = 2
		.columns(i).header1.FontBold = .f.
		.columns(i).FontName = "Verdana"
		.columns(i).Fontsize = 8
		.columns(i).ReadOnly = .t.
		.columns(i).text1.ReadOnly = .t.
		.Columns(i).columnorder = i
		.Columns(i).dynamicBackColor="uf_seriesserv_calendarioMensalBackColor()"
		.Columns(i).width = 150
		.Columns(i).InputMask=""
		.Columns(i).text1.InputMask=""
		.columns(i).text1.BorderStyle =  0
		.columns(i).text1.ForeColor = RGB(0,0,0)
		.columns(i).text1.SelectedForeColor = RGB(0,0,0)
		.columns(i).text1.FontBold = .t.
		.columns(i).text1.MousePointer = 15
		.columns(i).text1.SelectedBackColor = RGB(42,143,154)
		.Columns(i).Format="RTK"
		.Columns(i).text1.Format="RTK"
		.Columns(i).text1.hideSelection=.t.	
		*************************************
		
		DO WHILE lcData < lcDataFim 
			
			i = i +1
			
			lcNomeCampo = "d" + uf_gerais_getdate(lcData,"SQL")
			lcControlSource = "ucrsRecursosCalendarioMensal." + ALLTRIM(lcNomeCampo)

			.AddColumn 
			.RowHeight = uf_gerais_getRowHeight()
			.columns(i).ControlSource = lcControlSource 
			.columns(i).text1.ControlSource = lcControlSource 
			.columns(i).alignment = 2	
			.columns(i).header1.FontName = "Verdana"
			.columns(i).header1.Fontsize = 7
			.columns(i).FontName = "Verdana"
			.columns(i).Fontsize = 8
			.columns(i).ReadOnly = .t.
			.columns(i).text1.ReadOnly = .t.
			.Columns(i).columnorder = i
			.Columns(i).dynamicBackColor="uf_seriesserv_calendarioMensalBackColor()"
									
			.Columns(i).width = 50
			
			.Columns(i).InputMask=""
			.Columns(i).text1.InputMask=""
			.columns(i).header1.caption = LEFT(uf_marcacoes_DiaNome(lcData),3) +  CHR(13) + ASTR(DAY(lcData)) + [/] + ASTR(MONTH(lcData)) 
			.columns(i).header1.Alignment = 2
			.columns(i).header1.WordWrap = .t.
			.columns(i).header1.FontBold = .f.
			.columns(i).text1.BorderStyle =  0
			.columns(i).text1.ForeColor = RGB(0,0,0)
			.columns(i).text1.SelectedForeColor = RGB(0,0,0)
			.columns(i).text1.FontBold = .f.
			.columns(i).text1.MousePointer = 15
			.columns(i).text1.SelectedBackColor = RGB(42,143,154)

			
			.Columns(i).Format="RTK"
			.Columns(i).text1.Format="RTK"
			.Columns(i).text1.hideSelection=.t.	
			
	
			lcData = lcData + 1
				
		ENDDO 
		
		.LockColumns = 2
	ENDWITH
	
	
	
	IF USED("ucrsMapeamentoRecursosCombinacao")
		fecha("ucrsMapeamentoRecursosCombinacao")
	ENDIF 
	
	CREATE CURSOR ucrsMapeamentoRecursosCombinacao (stamp c(25), no n(9), nome c(55), tipo c(30), site c(20), ref c(18), especialidade c(55))

	** Acrescenta Combina��es Possiveis ** 
	Select uCrsRecursosDaSerie
	SET FILTER TO 

	lcOrdem = 0
	lcOrdemCalendario = 0
	lcDesign = ""

	
	lcModo = UPPER(ucrsSeriesServ.tipo)

	DO CASE
		CASE ALLTRIM(UPPER(lcModo)) == "SERVI�OS"
		
						
			** Calcula informa��o de especialidades associadas aos utilizadores
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				exec up_marcacoes_EspecialidadesUs 
			ENDTEXT 	
			If !uf_gerais_actGrelha("", "ucrsUsEspecialidades", lcSql)
				uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es de Especialidades associadas a utilizadores. Contacte o suporte.", "", "OK", 32)
				RETURN .f.
			ENDIF
			********************************************************************	
			
			** 
			LOCAL lcRefs
			lcRefs = ""
			
			Select ucrsServicosSerie
			GO Top
			SCAN
				IF empty(lcRefs)
					lcRefs = ALLTRIM(ucrsServicosSerie.ref)
				ELSE
					lcRefs = "," + ALLTRIM(ucrsServicosSerie.ref)
				ENDIF 
			ENDSCAN 
			
			** Calcula informa��o de duracao dos servi�os da Series relacionados com os utilizadores 
			SELECT ucrsSeriesServ
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				exec up_Marcacoes_DuracaoMrSimServico '<<ALLTRIM(lcRefs)>>'
			ENDTEXT 	
			If !uf_gerais_actGrelha("", "ucrsDuracaoServicos", lcSql)
				uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es de Especialidades associadas a utilizadores. Contacte o suporte.", "", "OK", 32)
				RETURN .f.
			ENDIF
			********************************************************************	
			
			** Calcula cursor de indiponibilidades *****************************************
			LOCAL lcUserNos 
			lcUserNos = ""
			
			Select ucrsMapeamentoRecursosCombinacao
			GO Top
			SCAN
				IF ALLTRIM(lcUserNos) == ""
					lcUserNos = ASTR(ucrsMapeamentoRecursosCombinacao.no)
				ELSE
					lcUserNos = lcUserNos + "," + ASTR(ucrsMapeamentoRecursosCombinacao.no)
				ENDIF 
			ENDSCAN 
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				exec up_marcacoes_indisponibilidades '<<IIF(EMPTY(lcUserNos),0,lcUserNos)>>'
			ENDTEXT 
			If !uf_gerais_actGrelha("", "ucrsIndisponibilidadesUsMarcacoes", lcSql)
				uf_perguntalt_chama("Ocorreu um erro a verificar indisponibilidades dos recursos. Contacte o suporte.", "", "OK", 32)
				RETURN .f.
			ENDIF
			***************************************************************************
					
			
				
			** adiciona todos os horarios dos utilizadores
			IF USED("ucrsHorariosPorLoja")
				fecha("ucrsHorariosPorLoja")
			ENDIF 
			SELECT distinct local FROM ucrsUsEspecialidades INNER JOIN uCrsRecursosDaSerie ON uCrsRecursosDaSerie.no == ucrsUsEspecialidades.userno ;
			WHERE !EMPTY(ucrsUsEspecialidades.local) INTO CURSOR ucrsHorariosPorLoja READWRITE

			** Filtros
			** Local
			lcLocal = ALLTRIM(SERIESSERV.Pageframe1.Page5.local.value)
			lcEspecialista = ALLTRIM(SERIESSERV.Pageframe1.Page5.local.value)
			
			SELECT ucrsHorariosPorLoja
			GO TOP 
			SCAN FOR (UPPER(ALLTRIM(ucrsHorariosPorLoja.local)) == UPPER(ALLTRIM(SERIESSERV.Pageframe1.Page5.local.value)) OR EMPTY(ALLTRIM(SERIESSERV.Pageframe1.Page5.local.value)))
			** 

				** 1 Precorre servi�os
				Select ucrsServicosSerie
				GO Top
				SCAN
					
					Select uCrsRecursosDaSerie
					LOCATE FOR ALLTRIM(ucrsServicosSerie.ref) == ALLTRIM(uCrsRecursosDaSerie.ref);
						AND ALLTRIM(UPPER(uCrsRecursosDaSerie.tipo)) != 'ESPECIALIDADE'
					IF FOUND()
						
						lcOrdemCalendario = lcOrdemCalendario +1
						lcDesign = ""
						lcStamp = uf_gerais_stamp()
						lcRef = ALLTRIM(ucrsServicosSerie.ref)
						
					
						Select ucrsRecursosCalendarioMensal
						APPEND BLANK
						Replace ucrsRecursosCalendarioMensal.stamp with lcStamp 
						Replace ucrsRecursosCalendarioMensal.ordem WITH lcOrdemCalendario
						Replace ucrsRecursosCalendarioMensal.ref WITH ucrsServicosSerie.ref
						Replace ucrsRecursosCalendarioMensal.utilizador WITH uCrsRecursosDaSerie.no
						Replace ucrsRecursosCalendarioMensal.design WITH ALLTRIM(ucrsServicosSerie.design) 
						
						lcDesign = lcDesign + IIF(!EMPTY(ALLTRIM(lcDesign))," + ","") + ALLTRIM(ucrsServicosSerie.design) 
						lcLocal = ALLTRIM(ucrsHorariosPorLoja.local)
						lcNomesEspecialistas = ""
					
						&& Especialistas 
						Select uCrsRecursosDaSerie
						GO Top
						SCAN FOR ALLTRIM(lcRef) == ALLTRIM(uCrsRecursosDaSerie.ref) AND ALLTRIM(UPPER(uCrsRecursosDaSerie.tipo)) != 'ESPECIALIDADE'

							lcDesign = lcDesign + IIF(!EMPTY(ALLTRIM(lcDesign))," + ","") + ALLTRIM(uCrsRecursosDaSerie.nome)
							lcLocal = ALLTRIM(ucrsHorariosPorLoja.local)
							lcNomesEspecialistas = lcNomesEspecialistas + IIF(!EMPTY(ALLTRIM(lcNomesEspecialistas ))," + ","") + ALLTRIM(uCrsRecursosDaSerie.nome)
							
							Select uCrsRecursosDaSerie
							** Cria Cursor que mapeia as linhas e os recursos associados
							Select ucrsMapeamentoRecursosCombinacao && este cursor tem todos os utilizadores de uma determinada combina��o, � usado para gravar na Marca��o
							APPEND BLANK
							Replace ucrsMapeamentoRecursosCombinacao.stamp WITH ucrsRecursosCalendarioMensal.stamp
							Replace ucrsMapeamentoRecursosCombinacao.no WITH uCrsRecursosDaSerie.no
							Replace ucrsMapeamentoRecursosCombinacao.nome WITH uCrsRecursosDaSerie.nome
							Replace ucrsMapeamentoRecursosCombinacao.tipo WITH uCrsRecursosDaSerie.tipo
							
							Replace ucrsMapeamentoRecursosCombinacao.site WITH ALLTRIM(ucrsHorariosPorLoja.local)
							Replace ucrsMapeamentoRecursosCombinacao.ref WITH ucrsServicosSerie.ref
							
							
							SELECT ucrsDuracaoServicos
							LOCATE FOR UPPER(ALLTRIM(ucrsDuracaoServicos.ref)) == UPPER(ALLTRIM(ucrsServicosSerie.ref));
									AND uCrsRecursosDaSerie.no = ucrsDuracaoServicos.no;
									AND ALLTRIM(UPPER(ucrsHorariosPorLoja.local)) = ALLTRIM(UPPER(ucrsDuracaoServicos.site));
									AND !EMPTY(ucrsDuracaoServicos.duracao)
							IF FOUND()
								Select ucrsRecursosCalendarioMensal
								Replace ucrsRecursosCalendarioMensal.duracao WITH ucrsDuracaoServicos.duracao
								Replace ucrsRecursosCalendarioMensal.mrsimultaneo WITH ucrsDuracaoServicos.mrsimultaneo
							ELSE
								Select ucrsRecursosCalendarioMensal
								Replace ucrsRecursosCalendarioMensal.duracao WITH ucrsServicosSerie.duracao
								Replace ucrsRecursosCalendarioMensal.mrsimultaneo WITH ucrsServicosSerie.mrsimultaneo
							ENDIF 							
							
						ENDSCAN 
						
						
						Select ucrsRecursosCalendarioMensal
						Replace ucrsRecursosCalendarioMensal.descricao WITH lcDesign 
						Replace ucrsRecursosCalendarioMensal.local WITH ALLTRIM(ucrsHorariosPorLoja.local)
						Replace ucrsRecursosCalendarioMensal.nomeespecialista WITH ALLTRIM(lcNomesEspecialistas) 
					
					ENDIF 
				ENDSCAN 
			ENDSCAN 
				
			** 
			Select ucrsServicosSerie
			GO Top
			SCAN
				
			&&	MESSAGEBOX(ucrsServicosSerie.ref)
				
				lcRef = ALLTRIM(ucrsServicosSerie.ref)
				
				&& ESPECIALIDADE - Se for especialidade considera todos os utilizadores da especialidade
				Select uCrsRecursosDaSerie
				GO Top
				SCAN FOR ALLTRIM(lcRef) = ALLTRIM(uCrsRecursosDaSerie.ref) AND ALLTRIM(UPPER(uCrsRecursosDaSerie.tipo)) == 'ESPECIALIDADE'
					** adiciona todos os utilizadores da especialidade
					SELECT ucrsUsEspecialidades
					SCAN FOR ALLTRIM(UPPER(uCrsRecursosDaSerie.nome)) == ALLTRIM(UPPER(ucrsUsEspecialidades.especialidade));
						AND (UPPER(ALLTRIM(ucrsUsEspecialidades.local)) == UPPER(ALLTRIM(SERIESSERV.Pageframe1.Page5.local.value)) OR EMPTY(ALLTRIM(SERIESSERV.Pageframe1.Page5.local.value)));
						AND (UPPER(ALLTRIM(ucrsUsEspecialidades.nome)) == UPPER(ALLTRIM(SERIESSERV.Pageframe1.Page5.especialista.value)) OR EMPTY(ALLTRIM(SERIESSERV.Pageframe1.Page5.especialista.value)))
						
						lcOrdemCalendario = lcOrdemCalendario + 1
						lcDesign = ALLTRIM(ucrsServicosSerie.design) + " + " +ALLTRIM(ucrsUsEspecialidades.nome) 
						lcLocal = ALLTRIM(ucrsUsEspecialidades.local)
						lcStamp = uf_gerais_stamp()
						
						
						Select ucrsRecursosCalendarioMensal
						APPEND BLANK
						Replace ucrsRecursosCalendarioMensal.stamp with lcStamp 
						Replace ucrsRecursosCalendarioMensal.ordem WITH lcOrdemCalendario
						Replace ucrsRecursosCalendarioMensal.ref WITH lcRef 
						Replace ucrsRecursosCalendarioMensal.utilizador WITH ucrsUsEspecialidades.userno
						Replace ucrsRecursosCalendarioMensal.design WITH ALLTRIM(ucrsServicosSerie.design) 
						Replace ucrsRecursosCalendarioMensal.nomeespecialista WITH ALLTRIM(ucrsUsEspecialidades.nome) 
						
						
						** Cria Cursor que mapeia as linhas e os recursos associados
						Select ucrsMapeamentoRecursosCombinacao  && este cursor tem todos os utilizadores de uma determinada combina��o, � usado para gravar na Marca��o
						APPEND BLANK
						Replace ucrsMapeamentoRecursosCombinacao.stamp WITH ucrsRecursosCalendarioMensal.stamp
						Replace ucrsMapeamentoRecursosCombinacao.no WITH ucrsUsEspecialidades.userno
						Replace ucrsMapeamentoRecursosCombinacao.nome WITH ucrsUsEspecialidades.nome
						Replace ucrsMapeamentoRecursosCombinacao.tipo WITH 'Utilizadores'
						Replace ucrsMapeamentoRecursosCombinacao.site WITH ALLTRIM(ucrsUsEspecialidades.local)
						Replace ucrsMapeamentoRecursosCombinacao.ref WITH ALLTRIM(lcRef)
						
						Select ucrsRecursosCalendarioMensal
						Replace ucrsRecursosCalendarioMensal.descricao WITH lcDesign 
						Replace ucrsRecursosCalendarioMensal.local WITH lcLocal 
						Replace ucrsRecursosCalendarioMensal.mrsimultaneo WITH ucrsServicosSerie.mrsimultaneo
						
						
						SELECT ucrsDuracaoServicos
						LOCATE FOR UPPER(ALLTRIM(ucrsDuracaoServicos.ref)) == UPPER(ALLTRIM(lcRef));
								AND ucrsUsEspecialidades.userno = ucrsDuracaoServicos.no;
								AND ALLTRIM(UPPER(lcLocal)) = ALLTRIM(UPPER(ucrsDuracaoServicos.site));
								AND !EMPTY(ucrsDuracaoServicos.duracao)
						IF FOUND()
							Select ucrsRecursosCalendarioMensal
							Replace ucrsRecursosCalendarioMensal.duracao WITH ucrsDuracaoServicos.duracao
							Replace ucrsRecursosCalendarioMensal.mrsimultaneo WITH ucrsDuracaoServicos.mrsimultaneo
						ELSE
							Select ucrsRecursosCalendarioMensal
							Replace ucrsRecursosCalendarioMensal.duracao WITH ucrsServicosSerie.duracao
							Replace ucrsRecursosCalendarioMensal.mrsimultaneo WITH ucrsServicosSerie.mrsimultaneo
						ENDIF 
					
					ENDSCAN 
						
				ENDSCAN 

				lcOrdem = ucrsServicosSerie.ordem
				
			ENDSCAN 


			uf_SERIESSERV_atualizaPVPs()
			SELECT ucrsRecursosCalendarioMensal
			GO Top

		
		CASE ALLTRIM(UPPER(lcModo)) == "ATIVIDADES"
			
			lcDesign = ""
			lcStamp = uf_gerais_stamp()
			
			Select ucrsRecursosCalendarioMensal
			APPEND BLANK
			Replace ucrsRecursosCalendarioMensal.ordem WITH 1
			Replace ucrsRecursosCalendarioMensal.stamp with lcStamp 

			Select uCrsRecursosDaSerie
			GO Top
			SCAN
				lcDesign = lcDesign + IIF(!EMPTY(lcDesign)," + ","") + ALLTRIM(uCrsRecursosDaSerie.nome)
				
				** Cria Cursor que mapeia as linhas e os recursos associados
				Select ucrsMapeamentoRecursosCombinacao 
				APPEND BLANK
				Replace ucrsMapeamentoRecursosCombinacao.stamp WITH ucrsRecursosCalendarioMensal.stamp
				Replace ucrsMapeamentoRecursosCombinacao.no WITH uCrsRecursosDaSerie.no
				Replace ucrsMapeamentoRecursosCombinacao.nome WITH uCrsRecursosDaSerie.nome
				Replace ucrsMapeamentoRecursosCombinacao.tipo WITH uCrsRecursosDaSerie.tipo
				
			ENDSCAN 
			Select ucrsRecursosCalendarioMensal
			Replace ucrsRecursosCalendarioMensal.descricao WITH lcDesign
			
	ENDCASE
			
	**
	Select ucrsSeriesServ
	** 
	IF !empty(ucrsSeriesServ.serieno)
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_marcacoes_sessoes <<ucrsSeriesServ.serieno>>
		ENDTEXT	

*!*	_CLIPTEXT = lcSQL
*!*	MESSAGEBOX(lcSQL)	

		SELECT ucrsSessoesSerie
		GO top
		SCAN 
			DELETE 
		ENDSCAN 

		&& Cursor auxiliar para n�o perder as configuracaoes da grelha
		IF !uf_gerais_actgrelha("", "ucrsSessoesSerieAux", lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A VERIFICAR AS SESS�ES ASSOCIADAS � S�RIE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF	
		
		SELECT ucrsSessoesSerie
		APPEND FROM DBF('ucrsSessoesSerieAux')
	ENDIF 	


			
	uf_seriesserv_indicadoresCalendarioMensal()	
	Select ucrsRecursosCalendarioMensal
	SET FILTER TO
	Select ucrsRecursosCalendarioMensal
	GO top	
	seriesserv.Pageframe1.Page5.GridDoc.refresh



ENDFUNC 


**
FUNCTION UF_SERIESSERV_SessoesAtribuidas
	LPARAMETERS lcData, lcCombinacao, lcLocal
	** 
	LOCAL lctxtSessoes
	lctxtSessoes = ""
	
	********************************
	Select ucrsSeriesServ
	** 
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_marcacoes_sessoes <<ucrsSeriesServ.serieno>>,'<<lcLocal>>'
	ENDTEXT	
	IF !uf_gerais_actgrelha("", "ucrsSessoesSerieLocal", lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR AS SESS�ES ASSOCIADAS � S�RIE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	*************************************
	
	** Dias com sessoes atribuidas
	Select ucrsSessoesSerieLocal
	GO Top
	SCAN FOR ucrsSessoesSerieLocal.ano == YEAR(lcData) AND ucrsSessoesSerieLocal.mes == MONTH(lcData) AND ucrsSessoesSerieLocal.dia == DAY(lcData);
		AND ALLTRIM(ucrsSessoesSerieLocal.combinacao) == ALLTRIM(lcCombinacao)
				
		IF !EMPTY(lctxtSessoes)
			lctxtSessoes = lctxtSessoes + ","
		ENDIF 		
		lctxtSessoes = lctxtSessoes +  ASTR(ucrsSessoesSerieLocal.Sessao)
	ENDSCAN 
	
	IF !EMPTY(lctxtSessoes)
		lctxtSessoes = "(" + lctxtSessoes + ")"
	ENDIF
	
	RETURN lctxtSessoes

ENDFUNC



**
FUNCTION uf_SERIESSERV_RegrasMarcacaoMensal
	LOCAL lcJaSelecionou, lcPrimeiraPosicao 
	lcJaSelecionou = .f.
	lcPrimeiraPosicao = 0

	** Verifica se existe algum periodo selecionado
	select ucrsPeriodosCalendarioMensal
	COUNT FOR !EMPTY(ucrsPeriodosCalendarioMensal.sel) TO lcNrRegistas
	IF lcNrRegistas == 0
		RETURN .f.
	ENDIF 
	
	select ucrsPeriodosCalendarioMensal
	GO Top
	SCAN FOR !EMPTY(ucrsPeriodosCalendarioMensal.sel) 
		lcPrimeiraPosicao = ucrsPeriodosCalendarioMensal.posicao
		EXIT
	ENDSCAN
	
	SELECT ucrsPeriodosCalendarioMensal
	GO Top
	SCAN FOR !EMPTY(ucrsPeriodosCalendarioMensal.sel)
		
		IF ucrsPeriodosCalendarioMensal.posicao > lcPrimeiraPosicao +1
			uf_perguntalt_chama("Os periodos selecionados devem ser sequenciais.", "", "OK", 32)
			RETURN .f.
		ENDIF 
		lcPrimeiraPosicao =  ucrsPeriodosCalendarioMensal.posicao
	ENDSCAN

	RETURN .t.

ENDFUNC 



** Fala periodos Marcados
FUNCTION uf_seriesserv_indicadoresCalendarioMensal
	LOCAL lcPeriodosDisponiveis, lcPeriodosMarcados, lcData, lcDataFim, LcNumDiasCalculo
	
	STORE 0 TO LcNumDiasCalculo 
	
	LcNumDiasCalculo = uf_gerais_getParameter("ADM0000000237","NUM")
	IF LcNumDiasCalculo == 0
		LcNumDiasCalculo = 65
	ENDIF
	 
	lcPeriodosDisponiveis = 0
	lcPeriodosMarcados = 0
*!*		lcData = DATE() - 5
*!*		lcDataFim = DATE() + LcNumDiasCalculo
*!*		
	lcData = DATE(seriesserv.anoPlaneamento,seriesserv.mesPlaneamento,1)
	lcDataFim = GOMONTH(lcData, +1)
	
	
	
	** Dura��o de servi�os default ***************************************
	lcDuracao = 30

		
	** Disponibilidades de todos os utilizadores 
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_DadosRecursos
	ENDTEXT 
	If !uf_gerais_actGrelha("", "ucrsRecursos", lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar dados dos recursos. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	****************************************************************
	** Limita��es dos Servi�os associados �s disponibilidades, caso n�o exista nenhum servi�o
	** associado � disponibilidade, permite todos os servi�os caso contrario apenas permite os servi�os especificados
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_ServicosDisponibilidades
	ENDTEXT 
	If !uf_gerais_actGrelha("", "ucrsServicosDisponibilidades", lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar os servi�os associados �s disponibilidades. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	****************************************************************
	** Calcula informa��o das marca��es existentes no intervalo 
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_MarcacoesExistentesPlaneamento
	ENDTEXT 
	If !uf_gerais_actGrelha("", "MarcacoesExistentesPlaneamento", lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar marca��es existentes. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	****************************************************************
	

	
	Select distinct *;
	FROM;
		ucrsMapeamentoRecursosCombinacao;
		LEFT JOIN ucrsRecursos ON ucrsMapeamentoRecursosCombinacao.no = ucrsRecursos.no;
			AND UPPER(ALLTRIM(ucrsMapeamentoRecursosCombinacao.tipo)) = UPPER(ALLTRIM(ucrsRecursos.tipo));
			AND UPPER(ALLTRIM(ucrsMapeamentoRecursosCombinacao.site)) = UPPER(ALLTRIM(ucrsRecursos.site));
	INTO CURSOR ucrsRecursosDependencia READWRITE
		
		
	
	** Calcula cursor de indiponibilidades *****************************************
	LOCAL lcUserNos 
	lcUserNos = ""
	
	Select ucrsMapeamentoRecursosCombinacao
	GO Top
	SCAN
		IF ALLTRIM(lcUserNos) == ""
			lcUserNos = ASTR(ucrsMapeamentoRecursosCombinacao.no)
		ELSE
			lcUserNos = lcUserNos + "," + ASTR(ucrsMapeamentoRecursosCombinacao.no)
		ENDIF 
	ENDSCAN 
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_indisponibilidades '<<IIF(EMPTY(lcUserNos),0,lcUserNos)>>'
	ENDTEXT 
	If !uf_gerais_actGrelha("", "ucrsIndisponibilidadesUsMarcacoes", lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar indisponibilidades dos recursos. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	***************************************************************************

		
	** Calcula marca��es efecutadas para os dias de analise, para os recursos
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_existentes '<<uf_gerais_getdate(lcData,"SQL")>>','<<uf_gerais_getdate(lcDataFim,"SQL")>>'
	ENDTEXT 
	If !uf_gerais_actGrelha("", "ucrsMarcacoesExistentes", lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar dados dos recursos. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF

	k = 0
	Select ucrsRecursosCalendarioMensal
	GO Top
	SCAN
		
		
		**
		lcCombinacao = ALLTRIM(ucrsRecursosCalendarioMensal.descricao)

	&&	lcData = DATE()-5
		lcData = DATE(seriesserv.anoPlaneamento,seriesserv.mesPlaneamento,1)
		lcDataFim = GOMONTH(lcData, +1)

		DO WHILE lcData < lcDataFim 
		
			lcPeriodosDisponiveis = 0
			
			lcNomeCampo = "d" + uf_gerais_getdate(lcData,"SQL")
			lcControlSource = "ucrsRecursosCalendarioMensal." + ALLTRIM(lcNomeCampo)
		
			lcDateTime = DATETIME(1900,1,1,0,0,1)
			lcDatetimeFim = DATETIME(1900,1,1,23,59,59)

			Select ucrsPeriodosCalendarioMensal
			GO Top
			SCAN
				DELETE 
			ENDSCAN 
			
			
			lcMrsimultaneo = IIF(EMPTY(ucrsRecursosCalendarioMensal.mrsimultaneo),1,ucrsRecursosCalendarioMensal.mrsimultaneo)
			
						
			IF lcData >= DATE() &&	AND lcDataPermitida == .t. AND lcDiaPermitido == .t.  && S� calcula periodos disponiveis para datas superiores � actual
	
	
				** Cursor que contem numero de marca��es Efetuadas por Periodo	
				SELECT ucrsMarcacoesExistentes.data, ucrsMarcacoesExistentes.combinacao, ucrsMarcacoesExistentes.hInicio, ucrsMarcacoesExistentes.hFim, COUNT(*) as ctn;
				FROM ucrsMarcacoesExistentes;
				GROUP BY ucrsMarcacoesExistentes.data,ucrsMarcacoesExistentes.combinacao, ucrsMarcacoesExistentes.hInicio, ucrsMarcacoesExistentes.hFim;
				INTO CURSOR ucrsMarcacoesExistentesContador READWRITE
				****

				Select ucrsRecursosDependencia
				GO Top
				SCAN FOR ucrsRecursosDependencia.stamp == ucrsRecursosCalendarioMensal.stamp; 
					AND ucrsRecursosDependencia.dataInicio <= lcData;
					AND ucrsRecursosDependencia.dataFim >= lcData;
					AND (;
						(uf_marcacoes_DiaNome(lcData) == "SEGUNDA-FEIRA" AND ucrsRecursosDependencia.segunda == .t.);
						OR (uf_marcacoes_DiaNome(lcData) == "TER�A-FEIRA" AND ucrsRecursosDependencia.terca == .t.);
						OR (uf_marcacoes_DiaNome(lcData) == "QUARTA-FEIRA" AND ucrsRecursosDependencia.quarta == .t.);
						OR (uf_marcacoes_DiaNome(lcData) == "QUINTA-FEIRA" AND ucrsRecursosDependencia.quinta == .t.);
						OR (uf_marcacoes_DiaNome(lcData) == "SEXTA-FEIRA" AND ucrsRecursosDependencia.sexta == .t.);
						OR (uf_marcacoes_DiaNome(lcData) == "S�BADO" AND ucrsRecursosDependencia.sabado == .t.);
						OR (uf_marcacoes_DiaNome(lcData) == "DOMINGO" AND ucrsRecursosDependencia.domingo == .t.);
					)
					
					** Verifica se existe limita��o de Referencias na disponibilidade/Series actual
					SELECT ucrsRecursosCalendarioMensal
					lcRef = ucrsRecursosCalendarioMensal.ref
					lcRefNaoPermitida = .f.					
					SELECT ucrsServicosDisponibilidades
					GO Top
					SCAN FOR ALLTRIM(ucrsServicosDisponibilidades.seriestamp) == ALLTRIM(ucrsRecursosDependencia.seriestamp)
						
						lcRefNaoPermitida = .t.
						
						IF ALLTRIM(ucrsServicosDisponibilidades.ref) == ALLTRIM(lcRef)
							lcRefNaoPermitida = .f.	
							EXIT 
						ENDIF 					
					ENDSCAN
					
					IF lcRefNaoPermitida == .t.
						EXIT 
					ENDIF 
					*********************
					
					lcMaxHoraInicio = ucrsRecursosDependencia.HoraInicio
					lcMaxHoraFim = ucrsRecursosDependencia.HoraFim
					
					
					
					lcTempoInicio = DATETIME(YEAR(lcData),MONTH(lcData),DAY(lcData),val(LEFT(lcMaxHoraInicio,2)),val(RIGHT(lcMaxHoraInicio,2)),0)
					lcTempoFim = DATETIME(YEAR(lcData),MONTH(lcData),DAY(lcData),val(LEFT(lcMaxHoraFim,2)),val(RIGHT(lcMaxHoraFim,2)),0)
					

					**
					SELECT ucrsRecursosCalendarioMensal
					lcDuracao = ucrsRecursosCalendarioMensal.duracao
					IF EMPTY(lcDuracao) && Se n�a estiver definido atribui dura��o default de 30 min 
						lcDuracao  = 30
						SELECT ucrsRecursosCalendarioMensal
						Replace ucrsRecursosCalendarioMensal.duracao WITH lcDuracao
					ENDIF 
					
					SELECT ucrsRecursosDependencia
					lcUserNo = ucrsRecursosDependencia.no_a	
					lcUserSite = ucrsRecursosDependencia.site_a
					
					** Se tiver definido a dura��o na Diponibilidade, esta prevalece
					IF !EMPTY(ucrsRecursosDependencia.duracao)
						lcDuracao  = ucrsRecursosDependencia.duracao
					ENDIF 
					
					
									
					&&lcPeriodosDisponiveis = (lcTempoFim - lcTempoInicio)/60/lcDuracao
					DO WHILE lcTempoInicio < lcTempoFim
						lcTempoInicio1 = lcTempoInicio  
						lcTempoInicio = lcTempoInicio + (lcDuracao * 60)
						lcTempoFim1 = lcTempoInicio	
						
						
										
						** valida se o periodo esta dentro das indisponibilidades					
						SELECT ucrsIndisponibilidadesUsMarcacoes
						GO Top
						SELECT ucrsIndisponibilidadesUsMarcacoes
						LOCATE FOR;
							ucrsIndisponibilidadesUsMarcacoes.userno == lcUserNo;
							AND ALLTRIM(UPPER(ucrsIndisponibilidadesUsMarcacoes.site)) == ALLTRIM(UPPER(lcUserSite)) ;
							AND ucrsIndisponibilidadesUsMarcacoes.dataInicio <= lcData;
							AND ucrsIndisponibilidadesUsMarcacoes.dataFim >= lcData;
							AND (;
								(uf_marcacoes_DiaNome(lcData) == "SEGUNDA-FEIRA" AND ucrsIndisponibilidadesUsMarcacoes.segunda == .t.);
								OR (uf_marcacoes_DiaNome(lcData) == "TER�A-FEIRA" AND ucrsIndisponibilidadesUsMarcacoes.terca == .t.);
								OR (uf_marcacoes_DiaNome(lcData) == "QUARTA-FEIRA" AND ucrsIndisponibilidadesUsMarcacoes.quarta == .t.);
								OR (uf_marcacoes_DiaNome(lcData) == "QUINTA-FEIRA" AND ucrsIndisponibilidadesUsMarcacoes.quinta == .t.);
								OR (uf_marcacoes_DiaNome(lcData) == "SEXTA-FEIRA" AND ucrsIndisponibilidadesUsMarcacoes.sexta == .t.);
								OR (uf_marcacoes_DiaNome(lcData) == "S�BADO" AND ucrsIndisponibilidadesUsMarcacoes.sabado == .t.);
								OR (uf_marcacoes_DiaNome(lcData) == "DOMINGO" AND ucrsIndisponibilidadesUsMarcacoes.domingo == .t.);
							);
							AND ucrsIndisponibilidadesUsMarcacoes.horainicio <= LEFT(TtoC(lcTempoInicio1,2),5);
							AND ucrsIndisponibilidadesUsMarcacoes.horafim >= LEFT(TtoC(lcTempoFim1,2),5)	
						IF FOUND()

							&& Encontrou indisponibilidade
						ELSE

							** Subtrai numero de Marca��es j� efectuadas para o periodo
							SELECT ucrsMarcacoesExistentesContador
							LOCATE FOR;
								ucrsMarcacoesExistentesContador.data == lcData;
								AND ALLTRIM(ucrsMarcacoesExistentesContador.combinacao) == ALLTRIM(lcCombinacao);
								AND ucrsMarcacoesExistentesContador.hinicio == LEFT(TtoC(lcTempoInicio1,2),5);
								AND ucrsMarcacoesExistentesContador.hinicio == LEFT(TtoC(lcTempoInicio1,2),5);
								AND ucrsMarcacoesExistentesContador.hfim == LEFT(TtoC(lcTempoFim1,2),5)
							IF FOUND()
								lcMrsimultaneo2 = lcMrsimultaneo - ucrsMarcacoesExistentesContador.ctn
							ELSE
								lcMrsimultaneo2 = lcMrsimultaneo	
							ENDIF
							
							FOR lcK = 1 TO lcMrsimultaneo2 
								lcPeriodosDisponiveis  = lcPeriodosDisponiveis +1						
							ENDFOR
						ENDIF
						
					ENDDO 
				
				ENDSCAN 
				
			ENDIF 
					
			Select ucrsPeriodosCalendarioMensal
			GO Top
			
			
			** Indica��o de Periodos
			IF !EMPTY(lccontrolSource)
			
				IF lcPeriodosDisponiveis> 0 OR lcPeriodosMarcados > 0
				
					lcSessoesMarcadasDia = UF_SERIESSERV_SessoesAtribuidas(lcData,ucrsRecursosCalendarioMensal.descricao, ucrsRecursosCalendarioMensal.local)
				
					Select ucrsRecursosCalendarioMensal
					Replace &lccontrolSource WITH ASTR(lcPeriodosDisponiveis) + lcSessoesMarcadasDia &&"|" + ASTR(lcPeriodosMarcados)
				ELSE
					Select ucrsRecursosCalendarioMensal
					Replace &lccontrolSource WITH ""
				ENDIF
			ENDIF
	
			lcData = lcData + 1
		ENDDO 
		
		Select ucrsRecursosCalendarioMensal
	ENDSCAN 
	
	
	
	Select ucrsRecursosCalendarioMensal
	GO Top
	seriesserv.Pageframe1.Page5.GridDoc.refresh
	seriesserv.Pageframe1.Page5.GridDoc.Columns(1).setFocus

ENDFUNC 


**
FUNCTION uf_seriesserv_calendarioMensalAfterRowColumnChange
	LOCAL lccontrolSource, lcPeriodosDisponiveis, lcPeriodosMarcados 
	
	
	IF EMPTY(SERIESSERV.ano) OR EMPTY(SERIESSERV.mes) &&OR EMPTY(SERIESSERV.mes)
		RETURN .f.
	ENDIF 

	Select ucrsRecursosCalendarioMensal	
	seriesserv.Pageframe1.Page5.Container1.combinacao.controlsource = "ucrsRecursosCalendarioMensal.design"
	seriesserv.Pageframe1.Page5.Container1.nomeEspecialista.controlsource = "ucrsRecursosCalendarioMensal.nomeEspecialista"
	seriesserv.Pageframe1.Page5.Container1.local.controlsource = "ucrsRecursosCalendarioMensal.local"
	seriesserv.Pageframe1.Page5.Container1.pvp.controlsource = "ucrsRecursosCalendarioMensal.pvp"
	seriesserv.Pageframe1.Page5.Container1.comp.controlsource = "ucrsRecursosCalendarioMensal.compart"
	seriesserv.Pageframe1.Page5.Container1.utente.controlsource = "ucrsRecursosCalendarioMensal.utente"
	
	** 
	** Seleciona a celula escolhida
	lccontrolSource = ""	
	WITH seriesserv.Pageframe1.Page5.GridDoc
		FOR i=1 TO .ColumnCount

			IF .columns(i).controlSource != "ucrsRecursosCalendarioMensal.descricao" AND .columns(i).controlSource != "ucrsRecursosCalendarioMensal.local"
				lcAno = VAL(LEFT(RIGHT(.columns(i).controlSource,8),4))
				lcMes = VAL(RIGHT(LEFT(RIGHT(.columns(i).controlSource,8),6),2))
				lcDia = VAL(RIGHT(RIGHT(.columns(i).controlSource,8),2))
				lcData = DATE(lcAno,lcMes,lcDia)
			ENDIF 
								
			If i == .ActiveColumn 
				
				IF .columns(i).controlSource == "ucrsRecursosCalendarioMensal.descricao" OR .columns(i).controlSource == "ucrsRecursosCalendarioMensal.local"
					.columns(i).dynamicBackColor="IIF(recno()%2!=0, Rgb[255,255,255], rgb[245,245,245])"
				ELSE
					&& Sabado, Domingo
					IF CDOW(lcData) == "Sunday" OR CDOW(lcData) == "Saturday"
						.columns(i).dynamicBackColor="IIF(recno()== "+ ASTR(.ActiveRow) +",rgb[42,143,154],IIF(recno()%2!=0, Rgb[250,247,231], Rgb[243,235,197]))"
					ELSE
						.columns(i).dynamicBackColor="IIF(recno()== "+ ASTR(.ActiveRow) +",rgb[42,143,154],IIF(recno()%2!=0, Rgb[255,255,255], rgb[245,245,245]))"
					ENDIF 
				ENDIF
				
				lccontrolSource = .columns(i).controlSource 
			ELSE
				IF .columns(i).controlSource == "ucrsRecursosCalendarioMensal.descricao" OR .columns(i).controlSource == "ucrsRecursosCalendarioMensal.local"
					.columns(i).dynamicBackColor="IIF(recno()%2!=0, Rgb[255,255,255], rgb[245,245,245])"
				ELSE
				
					&& Sabado, Domingo
					IF CDOW(lcData) == "Sunday" OR CDOW(lcData) == "Saturday"
						.columns(i).dynamicBackColor="IIF(recno()%2!=0, Rgb[250,247,231], Rgb[243,235,197])"
					ELSE
						.columns(i).dynamicBackColor="IIF(recno()%2!=0, Rgb[255,255,255], rgb[245,245,245])"
					ENDIF 
				
				ENDIF
			ENDIF 
		ENDFOR 
	ENDWITH
	 
	** Atribui data Selecionada
	IF ALLTRIM(lccontrolSource) == "ucrsRecursosCalendarioMensal.descricao" OR ALLTRIM(lccontrolSource) == "ucrsRecursosCalendarioMensal.local"
		seriesserv.Pageframe1.Page5.Container1.dataSelecionada.caption = ""
		seriesserv.Pageframe1.Page5.Container1.dataSelecionada.visible = .f.
		SERIESSERV.ano = "1900"
		SERIESSERV.mes = "1"
		SERIESSERV.dia = "1"
	ELSE
		lccontrolSource2 = RIGHT(lccontrolSource,8)
		SERIESSERV.ano = LEFT(RIGHT(lccontrolSource2,8),4)
		SERIESSERV.mes = RIGHT(LEFT(RIGHT(lccontrolSource2,8),6),2)
		SERIESSERV.dia = RIGHT(RIGHT(lccontrolSource2,8),2)
		seriesserv.Pageframe1.Page5.Container1.dataSelecionada.caption = "Data: " + SERIESSERV.ano + "." + SERIESSERV.mes + "." + SERIESSERV.dia
		seriesserv.Pageframe1.Page5.Container1.dataSelecionada.visible = .t.
	ENDIF 
	
	SET HOURS TO 24
	
	IF !USED("ucrsPeriodosCalendarioMensal")
		CREATE CURSOR ucrsPeriodosCalendarioMensal(posicao n(9,0),dataIni d,dataFim d,horaIni c(5),horaFim c(5),nome c(150), sel l, duracao n(8))
	ELSE
		SELECT ucrsPeriodosCalendarioMensal
		Go Top
		SCAN 
			DELETE 
		ENDSCAN 
	ENDIF

	
	lcData = DATE(VAL(SERIESSERV.ano),VAL(SERIESSERV.mes),VAL(SERIESSERV.dia))

	** Adiciona Marca��es Efectuadas para combina��o selecionada
	lcCombinacao = ALLTRIM(ucrsRecursosCalendarioMensal.descricao)
	lcMrsimultaneo = IIF(EMPTY(ucrsRecursosCalendarioMensal.mrsimultaneo),1,ucrsRecursosCalendarioMensal.mrsimultaneo)

	SELECT ucrsRecursosCalendarioMensal
	IF EMPTY(ucrsRecursosCalendarioMensal.duracao)
		lcDuracao = 30
	ELSE
		lcDuracao = ucrsRecursosCalendarioMensal.duracao
	ENDIF 	

	
	Select ucrsMarcacoesExistentes
	SCAN FOR ucrsMarcacoesExistentes.data == lcData AND ALLTRIM(ucrsMarcacoesExistentes.combinacao) == ALLTRIM(lcCombinacao)
		
		Select ucrsPeriodosCalendarioMensal
		COUNT to lcPosicao
					
		Select ucrsPeriodosCalendarioMensal
		APPEND blank
		replace ucrsPeriodosCalendarioMensal.posicao WITH lcPosicao+1
		replace ucrsPeriodosCalendarioMensal.dataIni WITH lcData
		replace ucrsPeriodosCalendarioMensal.dataFim WITH lcData
		replace ucrsPeriodosCalendarioMensal.horaIni WITH ucrsMarcacoesExistentes.hinicio
		replace ucrsPeriodosCalendarioMensal.horaFim WITH ucrsMarcacoesExistentes.hfim
		replace ucrsPeriodosCalendarioMensal.nome WITH ucrsMarcacoesExistentes.nome
		replace ucrsPeriodosCalendarioMensal.duracao WITH lcDuracao

	ENDSCAN 
											
	*************

	IF lcData >= DATE()	&& S� calcula periodos disponiveis para datas superiores � actual
		lcDateTime = DATETIME(YEAR(lcData),MONTH(lcData),DAY(lcData),0,0,1)
		lcDatetimeFim = DATETIME(YEAR(lcData),MONTH(lcData),DAY(lcData),23,59,59)

		** Cursor que contem numero de marca��es Efetuadas por Periodo	
		SELECT ucrsMarcacoesExistentes.data, ucrsMarcacoesExistentes.combinacao, ucrsMarcacoesExistentes.hInicio, ucrsMarcacoesExistentes.hFim, COUNT(*) as ctn;
		FROM ucrsMarcacoesExistentes;
		GROUP BY ucrsMarcacoesExistentes.data,ucrsMarcacoesExistentes.combinacao, ucrsMarcacoesExistentes.hInicio, ucrsMarcacoesExistentes.hFim;
		INTO CURSOR ucrsMarcacoesExistentesContador READWRITE
		****

		Select ucrsRecursosDependencia
		GO Top
		SCAN FOR ucrsRecursosDependencia.stamp == ucrsRecursosCalendarioMensal.stamp; 
			AND ucrsRecursosDependencia.dataInicio <= lcData;
			AND ucrsRecursosDependencia.dataFim >= lcData;
			AND (;
				(uf_marcacoes_DiaNome(lcData) == "SEGUNDA-FEIRA" AND ucrsRecursosDependencia.segunda == .t.);
				OR (uf_marcacoes_DiaNome(lcData) == "TER�A-FEIRA" AND ucrsRecursosDependencia.terca == .t.);
				OR (uf_marcacoes_DiaNome(lcData) == "QUARTA-FEIRA" AND ucrsRecursosDependencia.quarta == .t.);
				OR (uf_marcacoes_DiaNome(lcData) == "QUINTA-FEIRA" AND ucrsRecursosDependencia.quinta == .t.);
				OR (uf_marcacoes_DiaNome(lcData) == "SEXTA-FEIRA" AND ucrsRecursosDependencia.sexta == .t.);
				OR (uf_marcacoes_DiaNome(lcData) == "S�BADO" AND ucrsRecursosDependencia.sabado == .t.);
				OR (uf_marcacoes_DiaNome(lcData) == "DOMINGO" AND ucrsRecursosDependencia.domingo == .t.);
			)
	
			
			** Verifica se existe limita��o de Referencias na disponibilidade/Series actual
			SELECT ucrsRecursosCalendarioMensal
			lcRef = ucrsRecursosCalendarioMensal.ref
			lcRefNaoPermitida = .f.					
			SELECT ucrsServicosDisponibilidades
			GO Top
			SCAN FOR ALLTRIM(ucrsServicosDisponibilidades.seriestamp) == ALLTRIM(ucrsRecursosDependencia.seriestamp)
				
				lcRefNaoPermitida = .t.
				
				IF ALLTRIM(ucrsServicosDisponibilidades.ref) == ALLTRIM(lcRef)
					lcRefNaoPermitida = .f.	
					EXIT 
				ENDIF 					
			ENDSCAN
			
			IF lcRefNaoPermitida == .t.
				EXIT 
			ENDIF 
			*********************
			
			lcMaxHoraInicio = ucrsRecursosDependencia.HoraInicio
			lcMaxHoraFim = ucrsRecursosDependencia.HoraFim
				
			lcDataPermitida = .f.
			
			lcTempoInicio = DATETIME(YEAR(lcData),MONTH(lcData),DAY(lcData),val(LEFT(lcMaxHoraInicio,2)),val(RIGHT(lcMaxHoraInicio,2)),0)
			lcTempoFim = DATETIME(YEAR(lcData),MONTH(lcData),DAY(lcData),val(LEFT(lcMaxHoraFim,2)),val(RIGHT(lcMaxHoraFim,2)),0)
			
			
			SELECT ucrsRecursosDependencia
			lcUserNo = ucrsRecursosDependencia.no_a	
			lcUserSite = ucrsRecursosDependencia.site_a	
				
			** Se tiver definido a dura��o na Diponibilidade, esta prevalece
			IF !EMPTY(ucrsRecursosDependencia.duracao)
				lcDuracao  = ucrsRecursosDependencia.duracao
			ENDIF 
				
			&&lcPeriodosDisponiveis = (lcTempoFim - lcTempoInicio)/60/lcDuracao
			DO WHILE lcTempoInicio < lcTempoFim
				lcTempoInicio1 = lcTempoInicio  
				lcTempoInicio = lcTempoInicio + (lcDuracao * 60)
				lcTempoFim1 = lcTempoInicio	
				
				
				** valida se o periodo esta dentro das indisponibilidades					
				SELECT ucrsIndisponibilidadesUsMarcacoes
				GO Top
				SELECT ucrsIndisponibilidadesUsMarcacoes
				LOCATE FOR;
					ucrsIndisponibilidadesUsMarcacoes.userno == lcUserNo;
					AND ALLTRIM(UPPER(ucrsIndisponibilidadesUsMarcacoes.site)) == ALLTRIM(UPPER(lcUserSite)) ;
					AND ucrsIndisponibilidadesUsMarcacoes.dataInicio <= lcData;
					AND ucrsIndisponibilidadesUsMarcacoes.dataFim >= lcData;
					AND (;
						(uf_marcacoes_DiaNome(lcData) == "SEGUNDA-FEIRA" AND ucrsIndisponibilidadesUsMarcacoes.segunda == .t.);
						OR (uf_marcacoes_DiaNome(lcData) == "TER�A-FEIRA" AND ucrsIndisponibilidadesUsMarcacoes.terca == .t.);
						OR (uf_marcacoes_DiaNome(lcData) == "QUARTA-FEIRA" AND ucrsIndisponibilidadesUsMarcacoes.quarta == .t.);
						OR (uf_marcacoes_DiaNome(lcData) == "QUINTA-FEIRA" AND ucrsIndisponibilidadesUsMarcacoes.quinta == .t.);
						OR (uf_marcacoes_DiaNome(lcData) == "SEXTA-FEIRA" AND ucrsIndisponibilidadesUsMarcacoes.sexta == .t.);
						OR (uf_marcacoes_DiaNome(lcData) == "S�BADO" AND ucrsIndisponibilidadesUsMarcacoes.sabado == .t.);
						OR (uf_marcacoes_DiaNome(lcData) == "DOMINGO" AND ucrsIndisponibilidadesUsMarcacoes.domingo == .t.);
					);
					AND ucrsIndisponibilidadesUsMarcacoes.horainicio <= LEFT(TtoC(lcTempoInicio1,2),5);
					AND ucrsIndisponibilidadesUsMarcacoes.horafim >= LEFT(TtoC(lcTempoFim1,2),5)	
				IF FOUND()

					&& Encontrou indisponibilidade
				ELSE
				
					** Subtrai numero de Marca��es j� efectuadas para o periodo
					SELECT ucrsMarcacoesExistentesContador
					LOCATE FOR;
						ucrsMarcacoesExistentesContador.data == lcData;
						AND ALLTRIM(ucrsMarcacoesExistentesContador.combinacao) == ALLTRIM(lcCombinacao);
						AND ucrsMarcacoesExistentesContador.hinicio == LEFT(TtoC(lcTempoInicio1,2),5);
						AND ucrsMarcacoesExistentesContador.hinicio == LEFT(TtoC(lcTempoInicio1,2),5);
						AND ucrsMarcacoesExistentesContador.hfim == LEFT(TtoC(lcTempoFim1,2),5)
					IF FOUND()
						lcMrsimultaneo2 = lcMrsimultaneo - ucrsMarcacoesExistentesContador.ctn
					ELSE
						lcMrsimultaneo2 = lcMrsimultaneo	
					ENDIF
					
					
					Select ucrsPeriodosCalendarioMensal
					CALCULATE MAX(ucrsPeriodosCalendarioMensal.periodo) TO lcPeriodo
					lcPeriodo = lcPeriodo + 1 
					
					
						
						&& Repete o numero de marcacoes Simultaneas 
						FOR lcK = 1 TO lcMrsimultaneo2 
						
							Select ucrsPeriodosCalendarioMensal
							COUNT to lcPosicao
							
							Select ucrsPeriodosCalendarioMensal
							APPEND blank
							Replace ucrsPeriodosCalendarioMensal.periodo WITH lcPeriodo
							replace ucrsPeriodosCalendarioMensal.posicao WITH lcPosicao+1
							replace ucrsPeriodosCalendarioMensal.dataIni WITH lcData
							replace ucrsPeriodosCalendarioMensal.dataFim WITH lcData
							replace ucrsPeriodosCalendarioMensal.horaIni WITH LEFT(TtoC(lcTempoInicio1,2),5)
							replace ucrsPeriodosCalendarioMensal.horaFim WITH LEFT(TtoC(lcTempoFim1,2),5)
							replace ucrsPeriodosCalendarioMensal.duracao WITH lcDuracao
						ENDFOR 
					ENDIF

			ENDDO 

		ENDSCAN 

		
	ENDIF
	
	Select ucrsPeriodosCalendarioMensal
	GO Top

	**
	lcContador = 0
	select ucrsPeriodosCalendarioMensal
	Go Top
	SCAN 
		lcContador = lcContador +1
		replace ucrsPeriodosCalendarioMensal.periodo with lcContador
		replace ucrsPeriodosCalendarioMensal.posicao with lcContador
	ENDSCAN 
	
	Select ucrsPeriodosCalendarioMensal
	GO Top
	
	** Ordernar Periodos
	Select ucrsPeriodosCalendarioMensal
	Index on horaIni + horaFim Tag "OP"
	SET COLLATE TO "GENERAL"
	
	
	seriesserv.Pageframe1.Page5.Container1.GridPesq.refresh
	
	
	uf_SERIESSERV_dadosComparticipacao()
	seriesserv.Pageframe1.Page5.Container1.refresh
	
ENDFUNC 


**
FUNCTION uf_SERIESSERV_DuracaoMrSimServico
	**
	SELECT ucrsRecursosCalendarioMensal
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		 exec up_Marcacoes_DuracaoMrSimServico '<<ALLTRIM(ucrsRecursosCalendarioMensal.ref)>>','<<ALLTRIM(ucrsRecursosCalendarioMensal.local)>>'
	ENDTEXT

*!*	_CLIPTEXT = lcSQL
*!*	MESSAGEBOX(lcSQL)
	IF !uf_gerais_actGrelha("", "ucrsDadosDuracaoMrSimServico",lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VALIDAR DADOS DE DURA��O DO SERVI�O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	SELECT ucrsDadosDuracaoMrSimServico
	lcDuracaoServico = ucrsDadosDuracaoMrSimServico.duracao
	lcMrSimultaneo = ucrsDadosDuracaoMrSimServico.mrsimultaneo
	
	
ENDFUNC 


**
FUNCTION uf_SERIESSERV_dadosComparticipacao
	LOCAL lcSQL
	
	SELECT ucrsRecursosCalendarioMensal
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_clinica_DadosComparticipacao '<<ALLTRIM(ucrsSeriesServ.utstamp)>>','<<ALLTRIM(ucrsRecursosCalendarioMensal.ref)>>','<<ALLTRIM(ucrsRecursosCalendarioMensal.local)>>','<<ALLTRIM(ucrsRecursosCalendarioMensal.nomeespecialista)>>'
	ENDTEXT

*!*	_CLIPTEXT = lcSQL
*!*	MESSAGEBOX(lcSQL)
	IF !uf_gerais_actGrelha("seriesserv.Pageframe1.Page5.Container1.GridCompart", "ucrsDadosComparticipacaoSerie",lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VALIDAR DADOS DE COMPARTICIPA��O! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF


	lcServStamp = uf_gerais_stamp()
	lcOrdemComp = 0
	lcPVP = ucrsRecursosCalendarioMensal.pvp
	lcValorUtente = ucrsRecursosCalendarioMensal.pvp

	SELECT ucrsDadosComparticipacaoSerie
	Replace ucrsDadosComparticipacaoSerie.pvp WITH lcValorUtente
	lcPvp = lcValorUtente

	DO CASE
	CASE UPPER(ALLTRIM(ucrsDadosComparticipacaoSerie.tipoCompart)) == UPPER(ALLTRIM("Valor")) && Comparticipa��o em valor

		lcValorUtente =  ucrsDadosComparticipacaoSerie.compart

	CASE UPPER(ALLTRIM(ucrsDadosComparticipacaoSerie.tipoCompart)) == UPPER(ALLTRIM("Percent")) && Comparticipa��o em percentagem

		lcValorPercent = (lcPvp*ucrsDadosComparticipacaoSerie.compart)/100
		lcValorUtente =  lcValorPercent

	OTHERWISE
		**
	ENDCASE

	lcValorEntidade = lcPVP - lcValorUtente			

	SELECT ucrsDadosComparticipacaoSerie
	Replace ucrsDadosComparticipacaoSerie.valorUtente WITH lcValorUtente
	Replace ucrsDadosComparticipacaoSerie.valorEntidade WITH lcValorEntidade

	LOCAL lcValorTotalEntidade
	STORE 0 TO lcValorTotalEntidade	
	
	SELECT ucrsDadosComparticipacaoSerie
	CALCULATE SUM(valorEntidade) TO lcValorTotalEntidade
	
	SELECT ucrsRecursosCalendarioMensal
	Replace ucrsRecursosCalendarioMensal.compart WITH lcValorTotalEntidade
	Replace ucrsRecursosCalendarioMensal.utente WITH ucrsRecursosCalendarioMensal.pvp - lcValorTotalEntidade

	WITH seriesserv.Pageframe1.Page5.Container1
		.pvp.refresh
		.comp.refresh
		.utente.refresh
	ENDWITH 
		
ENDFUNC 




**
FUNCTION uf_seriesserv_calendarioMensalBackColor
	DO CASE
		CASE recno()%2!=0
			RETURN Rgb[255,255,255]
		OTHERWISE 
			RETURN rgb[240,240,240]
	ENDCASE
ENDFUNC 


**
FUNCTION uf_SERIESSERV_dispRecursos
	LOCAL lcDuracao
	lcDuracao = 0
	
	SERIESSERV.pageFrame1.activepage = 5
	
	** Coloca Intervalo igual ao somatorio da dura��o dos servi�os
	SELECT ucrsServicosSerie 
	CALCULATE SUM(ucrsServicosSerie.duracao) TO lcDuracao
	
*!*		IF !EMPTY(lcDuracao)
*!*			seriesserv.Pageframe1.Page5.intervalo.value = ROUND(lcDuracao,0)
*!*		ENDIF 
*!*		
	uf_seriesserv_atualizaCalendarioMensal()
	uf_SERIESSERV_alternaMenu()
ENDFUNC


**
FUNCTION uf_SERIESSERV_dispRecursosActivate
	uf_SERIESSERV_alternaMenu()
ENDFUNC 


**
FUNCTION uf_SERIESSERV_dispRecursosDeActivate
	uf_SERIESSERV_alternaMenu()
ENDFUNC


**
FUNCTION uf_SERIESSERV_Voltar
	SERIESSERV.pageFrame1.activepage = 1
ENDFUNC


**
FUNCTION uf_SERIESSERV_marcacoesDaSerieActivate

	WITH SERIESSERV.menu1	
		.estado("opcoes, pesquisar,novoServ,novaAtividade, editar, disponibilidades", "HIDE", "", .f., "Cancelar", .t.)
		&&.estado("remarcar", "SHOW", "", .f., "Voltar", .t.)
	ENDWITH
	
ENDFUNC 


**
FUNCTION uf_SERIESSERV_marcacoesDaSerieDeActivate
	WITH SERIESSERV.menu1	
		.estado("opcoes, pesquisar,novoServ,novaAtividade, editar", "SHOW")
	ENDWITH
	uf_SERIESSERV_alteraVisualizacao()
ENDFUNC


**
FUNCTION uf_SERIESSERV_ecolheTipoSerie
	LPARAMETERS tcMarcacoes

	IF USED("ucrsTipoMarc")
		fecha("ucrsTipoMarc")
	ENDIF
	
	CREATE CURSOR ucrsTipoMarc(tipo c(30), descricao c(254), seriestamp c(25))
	
	IF EMPTY(tcMarcacoes)
		SELECT ucrsTipoMarc
		APPEND BLANK
		Replace ucrsTipoMarc.tipo WITH "Servi�os"
		APPEND BLANK
		Replace ucrsTipoMarc.tipo WITH "Atividades"
		APPEND BLANK
		Replace ucrsTipoMarc.tipo WITH "Recursos"
		APPEND BLANK
		Replace ucrsTipoMarc.tipo WITH "Utilizadores"
	ENDIF 
	
	TEXT TO lcSQL NOSHOW TEXTMERGE
		select tipo, descricao = 'Template ' + serienome, seriestamp from b_series where template = 1 order by serienome
	ENDTEXT 

	IF uf_gerais_actGrelha("", "ucrsTemplateSeries",lcSQL)
		Select ucrsTemplateSeries
		GO Top
		SCAN 
			Select ucrsTipoMarc
			APPEND BLANK
			Replace ucrsTipoMarc.tipo WITH ucrsTemplateSeries.tipo
			Replace ucrsTipoMarc.descricao WITH ucrsTemplateSeries.descricao 
			Replace ucrsTipoMarc.seriestamp WITH ucrsTemplateSeries.seriestamp 
	
		ENDSCAN 		
	ENDIF 
	
	PUBLIC myVarEscolhaSerie
	myVarEscolhaSerie = .f.
	
	uf_valorescombo_chama("myVarEscolhaSerie", 0, "ucrsTipoMarc", 2, "tipo", "tipo , descricao", .F.,.F.,.T.)

	IF EMPTY(myVarEscolhaSerie)
		RETURN .f.
	ENDIF 
	
	** verifica se � template, caso seja coloca as defini��es do template
	IF !EMPTY(UCRSPESQMULTI.seriestamp)
		uf_SERIESSERV_chama(UCRSPESQMULTI.seriestamp,UCRSPESQMULTI.tipo)
		
		** Coloca novo stamp e N�mero
		
		**Numeracao da serie
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_marcacoes_seriesNum
		ENDTEXT

		If !uf_gerais_actGrelha("", "ucrsSeriesServNum", lcSql)
			uf_perguntalt_chama("N�o foi poss�vel verificar a numera��o a atrinuir � S�rie. Contacte o suporte.", "", "OK", 32)
			RETURN .f.
		ENDIF
		SELECT ucrsSeriesServNum
		LcNo = ucrsSeriesServNum.numero
		
		lcStamp = uf_gerais_stamp()

		Select ucrsSeriesServ
		Replace ucrsSeriesServ.seriestamp WITH lcStamp 
		Replace ucrsSeriesServ.serieno 	WITH LcNo 
		Replace ucrsSeriesServ.dataInicio WITH DATE()
		Replace ucrsSeriesServ.dataFim WITH DATE()
		Replace ucrsSeriesServ.dataIniRep WITH DATE()
		**
		Replace ucrsSeriesServ.serienome WITH ""
		Replace ucrsSeriesServ.template WITH .f.
		
		
		update ucrsServicosSerie SET seriestamp = lcStamp
		update uCrsRecursosDaSerie SET seriestamp = lcStamp
		update uCrsSeriesConsumo SET seriestamp = lcStamp
		update ucrsRecursos SET seriestamp = lcStamp
		
		uf_SERIESSERV_editar()
		
		mySERIESSERVAlteracao = .f.
		mySERIESSERVIntroducao = .t.
						
		SERIESSERV.refresh
				
	ELSE
		uf_SERIESSERV_chama("")
		uf_SERIESSERV_novo()
		Select ucrsSeriesServ
		Replace ucrsSeriesServ.tipo with ALLTRIM(myVarEscolhaSerie)
	ENDIF	
	
	DO CASE 
		CASE ALLTRIM(ucrsSeriesServ.tipo) == "Servi�os"
			Select ucrsSeriesServ
			REPLACE ucrsSeriesServ.serienome with "Planeamento Servi�o Nr." + ASTR(ucrsSeriesServ.serieno) 
			
		CASE ALLTRIM(ucrsSeriesServ.tipo) == "Utilizadores"
			Select ucrsSeriesServ
			REPLACE ucrsSeriesServ.serienome with "Disp. Utilizador Nr." + ASTR(ucrsSeriesServ.serieno) 
			
		CASE ALLTRIM(ucrsSeriesServ.tipo) == "Atividades"
			Select ucrsSeriesServ
			REPLACE ucrsSeriesServ.serienome with "Planeamento Marca��o Nr." + ASTR(ucrsSeriesServ.serieno) 
			
		CASE ALLTRIM(ucrsSeriesServ.tipo) == "Recursos"

			Select ucrsSeriesServ
			REPLACE ucrsSeriesServ.serienome with "Disp. Recurso Nr." + ASTR(ucrsSeriesServ.serieno) 
	ENDCASE 
	
	uf_SERIESSERV_alternaMenu()	
	uf_SERIESSERV_alteraVisualizacao()
ENDFUNC 

**
FUNCTION uf_SERIESSERV_sair

	** Voltar
	IF SERIESSERV.pageframe1.activePage == 4;
		OR SERIESSERV.pageframe1.activePage == 5;
		OR SERIESSERV.pageframe1.activePage == 2
		
		SERIESSERV.pageframe1.activePage = 1
		SERIESSERV.menu1.estado("", "SHOW", "", .f., "Sair", .t.)
		uf_SERIESSERV_alternaMenu()	
		
		RETURN .f.
	ENDIF 

	**
	IF mySERIESSERVIntroducao== .t. OR mySERIESSERVAlteracao== .t.
		IF uf_perguntalt_chama("Pretende cancelar as altera��es?", "Sim", "N�o", 32)
			
			mySERIESSERVIntroducao= .f.
			mySERIESSERVAlteracao= .f.
			uf_SERIESSERV_alternaMenu()
			uf_seriesserv_ultimo()
		ENDIF
	ELSE
		uf_SERIESSERV_exit()
	ENDIF
ENDFUNC


**
FUNCTION uf_SERIESSERV_EliminaSessao

	**
	Select ucrsSessoesSerie
	IF !EMPTY(ucrsSessoesSerie.mrstamp)

		IF !uf_perguntalt_chama("Vai eliminar a Sess�o. Pretende continuar?", "Sim", "N�o", 32)
			RETURN .f.
		ENDIF
		
		LOCAL lcSQL
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			DELETE FROM marcacoes WHERE mrstamp = '<<ALLTRIM(ucrsSessoesSerie.mrstamp)>>'
			DELETE FROM marcacoesServ WHERE mrstamp = '<<ALLTRIM(ucrsSessoesSerie.mrstamp)>>'
			DELETE FROM cpt_marcacoes WHERE mrstamp = '<<ALLTRIM(ucrsSessoesSerie.mrstamp)>>'
		ENDTEXT
		IF !uf_gerais_actGrelha("", "", lcSQL)
			MESSAGEBOX("N�o foi possivel eliminar a Sessao. Por favor contacte o suporte",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ELSE
			uf_perguntalt_chama("Sess�o eliminada com sucesso!", "", "OK", 64)
		ENDIF


		**
		Select ucrsSeriesServ
		** 
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_marcacoes_sessoes <<ucrsSeriesServ.serieno>>
		ENDTEXT	
		IF !uf_gerais_actgrelha("seriesserv.Pageframe1.Page5.Container1.GridSessoes", "ucrsSessoesSerie", lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A VERIFICAR AS SESS�ES ASSOCIADAS � S�RIE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
		
		**
		uf_seriesserv_indicadoresCalendarioMensal()
		
	ENDIF 

ENDFUNC 


** 
FUNCTION uf_SERIESSERV_marcarRep
	LOCAL lcOcorrencia, lcNrOcorrencias, LcNumDiasCalculo,lcDataDiasCalculo, lcDataAnteriorAtribuida 

	**
	IF EMPTY(ucrsSeriesServ.repetir)
		RETURN .f.
	ENDIF 
	
	LcNumDiasCalculo = uf_gerais_getParameter("ADM0000000237","NUM")
	IF LcNumDiasCalculo == 0
		LcNumDiasCalculo = 65
	ENDIF
	lcDataDiasCalculo = DATE() + LcNumDiasCalculo -1
	
	uf_seriesserv_atualizaCalendarioMensal()

	regua(0,lcNrOcorrencias,"A processar Marca��es...")
	regua(1,1,"A processar Marca��es...")

		
	
	DO CASE 
		CASE UPPER(ALLTRIM(ucrsSeriesServ.criterio)) == "TODOS OS DIAS"
						
			lcDiasRepetir = ucrsSeriesServ.valorRep
			lcOcorrencia = 0
			
			lcNrOcorrencias = ucrsSeriesServ.sessoes
			IF lcNrOcorrencias == 0 && N�o numero de ocorrencias definidos
				Return .f.
			ENDIF 
			
			lcData = TTOD(ucrsSeriesServ.datainirep)
			&& Caso n�o tenha difinido a data de fim de repeti��o considera 90 dias
			lcDataFim = IIF(empty(ucrsSeriesServ.repdia),lcData + 90, TTOD(ucrsSeriesServ.datafimrep))

			&& Procura durante 90 dias e o numero de ocorrencias ainda n�o tiver sido satisfeito			
			DO WHILE lcData <= lcDataFim AND (lcOcorrencia < lcNrOcorrencias OR !empty(ucrsSeriesServ.repdia))
															
				lcVarCalendarioMensalCursor =  "ucrsRecursosCalendarioMensal.d" + uf_gerais_getdate(lcData,"SQL")	
				
				** Valida se j� ultrapassou o numero de dias de calculo
				IF lcData  > lcDataDiasCalculo
					uf_perguntalt_chama("Foram efectuadas "+ ASTR(lcOcorrencia) + " marca��es. N�o foi poss�vel encontrar mais disponibilidades nos " + ASTR(LcNumDiasCalculo) +;
					" dias definidos como m�ximo para c�lculo. Disponibilidades verificadas at� dia: " + ASTR(lcDataDiasCalculo) + ".", "", "OK", 32)

					regua(2)
					RETURN .f.
				ENDIF 
				**

				&& Verifica se todas as combina��es t�m pelo menos um periodo disponivel
				lcTodasCombinacoesTemPeriodoDisponivel = .t.
				SELECT ucrsRecursosCalendarioMensal
				GO TOP
				SCAN FOR EMPTY(&lcVarCalendarioMensalCursor)
					lcTodasCombinacoesTemPeriodoDisponivel = .f.
				ENDSCAN 						

				IF lcTodasCombinacoesTemPeriodoDisponivel == .t.
				
					lcOcorrencia = lcOcorrencia + 1
				
					regua(1,lcOcorrencia,"A processar Marca��es... Marca��o: " + ASTR(lcOcorrencia) + " Data: " + ASTR(lcData))
					
					SELECT ucrsRecursosCalendarioMensal && Marca para todas as combina��es
					GO TOP
					SCAN 
						lcStamp = ucrsRecursosCalendarioMensal.stamp
						uf_seriesserv_CalculaPrimeiroPeriodoDisponivelData(lcData, lcStamp)
					ENDSCAN 
					
					lcDataAnteriorAtribuida = lcData
				ENDIF 
				
				&& Numero de dias a repetir (caso ultrapasse considera o dia seguinte)
				IF EMPTY(lcDiasRepetir)
					lcDiasRepetir = 1
				ENDIF

				lcData = lcData + lcDiasRepetir
				**
			ENDDO 

			
		
		CASE UPPER(ALLTRIM(ucrsSeriesServ.criterio)) == "SEMANAL"
		
		CASE UPPER(ALLTRIM(ucrsSeriesServ.criterio)) == "MENSAL"
								
		CASE UPPER(ALLTRIM(ucrsSeriesServ.criterio)) == "ANUAL"

		OTHERWISE
			**
			RETURN .f.
	ENDCASE 
	
	regua(2)								
	uf_perguntalt_chama("Foram efectuadas "+ ASTR(lcOcorrencia) + " marca��es.", "", "OK", 64)
	


ENDFUNC 



**
FUNCTION uf_seriesserv_CalculaPrimeiroPeriodoDisponivelData
	LPARAMETERS lcData, lcStamp
		
	LOCAL lccontrolSource, lcPeriodosDisponiveis, lcPeriodosMarcados 
	
	SET HOURS TO 24
	
	IF !USED("ucrsPeriodosCalendarioMensal")
		CREATE CURSOR ucrsPeriodosCalendarioMensal(posicao n(9,0),dataIni d,dataFim d,horaIni c(5),horaFim c(5),nome c(150), sel l)
	ELSE
		SELECT ucrsPeriodosCalendarioMensal
		Go Top
		SCAN 
			DELETE 
		ENDSCAN 
	ENDIF


	** duracoa default
	lcDuracao = 30
		

	** Adiciona Marca��es Efectuadas para combina��o selecionada
	lcCombinacao = ALLTRIM(ucrsRecursosCalendarioMensal.descricao)
	lcMrsimultaneo = IIF(EMPTY(ucrsRecursosCalendarioMensal.mrsimultaneo),1,ucrsRecursosCalendarioMensal.mrsimultaneo)
		
	&&seriesserv.Pageframe1.Page5.Container1.marcSimultaneas.caption = "Marca��es Simult�neas: " + ASTR(lcMrsimultaneo)
	&&seriesserv.Pageframe1.Page5.Container1.marcSimultaneas.visible = .t.
	
	
	Select ucrsMarcacoesExistentes
	SCAN FOR ucrsMarcacoesExistentes.data == lcData AND ALLTRIM(ucrsMarcacoesExistentes.combinacao) == ALLTRIM(lcCombinacao)
		
		Select ucrsPeriodosCalendarioMensal
		COUNT to lcPosicao
					
		Select ucrsPeriodosCalendarioMensal
		APPEND blank
		replace ucrsPeriodosCalendarioMensal.posicao WITH lcPosicao+1
		replace ucrsPeriodosCalendarioMensal.dataIni WITH lcData
		replace ucrsPeriodosCalendarioMensal.dataFim WITH lcData
		replace ucrsPeriodosCalendarioMensal.horaIni WITH ucrsMarcacoesExistentes.hinicio
		replace ucrsPeriodosCalendarioMensal.horaFim WITH ucrsMarcacoesExistentes.hfim
		replace ucrsPeriodosCalendarioMensal.nome WITH ucrsMarcacoesExistentes.nome

	ENDSCAN 
											
	*************

	IF lcData >= DATE()	&& S� calcula periodos disponiveis para datas superiores � actual

		lcDateTime = DATETIME(YEAR(lcData),MONTH(lcData),DAY(lcData),0,0,1)
		lcDatetimeFim = DATETIME(YEAR(lcData),MONTH(lcData),DAY(lcData),23,59,59)

		lcDiaPermitido = .t.
		lcDataPermitida = .t.
		

		IF lcDiaPermitido == .t. AND lcDataPermitida == .t. && S� calcula periodos disponiveis para datas superiores � actual


			SELECT ucrsRecursosCalendarioMensal
		
			** Cursor que contem numero de marca��es Efetuadas por Periodo	
			SELECT ucrsMarcacoesExistentes.data, ucrsMarcacoesExistentes.combinacao, ucrsMarcacoesExistentes.hInicio, ucrsMarcacoesExistentes.hFim, COUNT(*) as ctn;
			FROM ucrsMarcacoesExistentes;
			GROUP BY ucrsMarcacoesExistentes.data,ucrsMarcacoesExistentes.combinacao, ucrsMarcacoesExistentes.hInicio, ucrsMarcacoesExistentes.hFim;
			INTO CURSOR ucrsMarcacoesExistentesContador READWRITE
			****

			** Valida Horas sobrepostas
			Select ucrsRecursosDependencia
			CALCULATE MAX(ucrsRecursosDependencia.horaInicio);
			FOR ucrsRecursosDependencia.stamp == lcStamp;
				AND (lcData >= ucrsRecursosDependencia.dataInicio OR lcData <= ucrsRecursosDependencia.dataFim);
			TO lcMaxHoraInicio

			**
			Select ucrsRecursosDependencia
			CALCULATE MIN(ucrsRecursosDependencia.horaFim);
			FOR ucrsRecursosDependencia.stamp == lcStamp;
				AND (lcData >= ucrsRecursosDependencia.dataInicio OR lcData <= ucrsRecursosDependencia.dataFim);
			TO lcMaxHoraFim
			

			lcDataPermitida = .f.

			lcTempoInicio = DATETIME(YEAR(lcData),MONTH(lcData),DAY(lcData),val(LEFT(lcMaxHoraInicio,2)),val(RIGHT(lcMaxHoraInicio,2)),0)
			lcTempoFim = DATETIME(YEAR(lcData),MONTH(lcData),DAY(lcData),val(LEFT(lcMaxHoraFim,2)),val(RIGHT(lcMaxHoraFim,2)),0)
		
			&&lcPeriodosDisponiveis = (lcTempoFim - lcTempoInicio)/60/lcDuracao
			DO WHILE lcTempoInicio < lcTempoFim
				lcTempoInicio1 = lcTempoInicio  
				lcTempoInicio = lcTempoInicio + (lcDuracao * 60)
				lcTempoFim1 = lcTempoInicio	
				
				
				** Subtrai numero de Marca��es j� efectuadas para o periodo
				SELECT ucrsMarcacoesExistentesContador
				LOCATE FOR;
					ucrsMarcacoesExistentesContador.data == lcData;
					AND ALLTRIM(ucrsMarcacoesExistentesContador.combinacao) == ALLTRIM(lcCombinacao);
					AND ucrsMarcacoesExistentesContador.hinicio == LEFT(TtoC(lcTempoInicio1,2),5);
					AND ucrsMarcacoesExistentesContador.hinicio == LEFT(TtoC(lcTempoInicio1,2),5);
					AND ucrsMarcacoesExistentesContador.hfim == LEFT(TtoC(lcTempoFim1,2),5)
				IF FOUND()
					lcMrsimultaneo2 = lcMrsimultaneo - ucrsMarcacoesExistentesContador.ctn
				ELSE
					lcMrsimultaneo2 = lcMrsimultaneo	
				ENDIF
				
				
				Select ucrsPeriodosCalendarioMensal
				CALCULATE MAX(ucrsPeriodosCalendarioMensal.periodo) TO lcPeriodo
				lcPeriodo = lcPeriodo + 1 
				
				&& Repete o numero de marcacoes Simultaneas 
				FOR lcK = 1 TO lcMrsimultaneo2 
				
					Select ucrsPeriodosCalendarioMensal
					COUNT to lcPosicao
					
					Select ucrsPeriodosCalendarioMensal
					APPEND blank
					Replace ucrsPeriodosCalendarioMensal.periodo WITH lcPeriodo
					replace ucrsPeriodosCalendarioMensal.posicao WITH lcPosicao+1
					replace ucrsPeriodosCalendarioMensal.dataIni WITH lcData
					replace ucrsPeriodosCalendarioMensal.dataFim WITH lcData
					replace ucrsPeriodosCalendarioMensal.horaIni WITH LEFT(TtoC(lcTempoInicio1,2),5)
					replace ucrsPeriodosCalendarioMensal.horaFim WITH LEFT(TtoC(lcTempoFim1,2),5)
					
				ENDFOR 

			ENDDO 
	
		ENDIF
	ENDIF	
	
	DELETE FROM ucrsPeriodosCalendarioMensal where EMPTY(ucrsPeriodosCalendarioMensal.periodo)
	Select ucrsPeriodosCalendarioMensal
	GO Top
	Replace ucrsPeriodosCalendarioMensal.sel WITH .t.

	
	uf_SERIESSERV_marcar(lcData, .t.)
ENDFUNC 


**
FUNCTION uf_SERIESSERV_FilterPlaneamento
	LPARAMETERS lcLocal
	
	LOCAL lcFiltro 

	IF EMPTY(lcLocal)	
		select ucrsRecursosCalendarioMensal
		set filter to
	ELSE
		lcFiltro = "UPPER(ALLTRIM(ucrsRecursosCalendarioMensal.local)) = UPPER(ALLTRIM('" + UPPER(ALLTRIM(lcLocal)) + "'))"
		select ucrsRecursosCalendarioMensal
		set filter to &lcFiltro 
	ENDIF 
	
	seriesserv.Pageframe1.Page5.GridDoc.refresh
	SELECT ucrsRecursosCalendarioMensal
	GO top
ENDFUNC 


**
FUNCTION uf_SERIESSERV_atualizaPVPs

*!*		** Calcula informa��o de Conven��es e PVP associados ao utente para todos os servi�os
*!*		lcSQL = ""
*!*		TEXT TO lcSQL NOSHOW TEXTMERGE 
*!*			exec up_marcacoes_convencoesUtentes <<ucrsSeriesServ.no>>,<<ucrsSeriesServ.estab>>
*!*		ENDTEXT 	
*!*		If !uf_gerais_actGrelha("", "ucrsConvencoesUtente", lcSql)
*!*			uf_perguntalt_chama("N�o foi poss�vel verificar as conven��es associadas �s entidades do utente. Contacte o suporte.", "", "OK", 32)
*!*			RETURN .f.
*!*		ENDIF
*!*		********************************************************************	
*!*		
*!*		** Calcula PVPs para caso n�o existam conven��es para o servi�o e utente
*!*		lcSQL = ""
*!*		TEXT TO lcSQL NOSHOW TEXTMERGE 
*!*			exec up_marcacoes_PvpsSemConvencao
*!*		ENDTEXT 	
*!*		If !uf_gerais_actGrelha("", "ucrsPvpsSemConvencao", lcSql)
*!*			uf_perguntalt_chama("N�o foi poss�vel verificar as pre�os dos servi�os. Contacte o suporte.", "", "OK", 32)
*!*			RETURN .f.
*!*		ENDIF
*!*		********************************************************************	


	** 
	LOCAL lcRefs
	lcRefs = ""
	
	Select ucrsServicosSerie
	GO Top
	SCAN
		IF empty(lcRefs)
			lcRefs = ALLTRIM(ucrsServicosSerie.ref)
		ELSE
			lcRefs = "," + ALLTRIM(ucrsServicosSerie.ref)
		ENDIF 
	ENDSCAN 
	
	
	** Calcula PVPs para caso n�o existam conven��es para o servi�o e utente
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_PvpsSemConvencao '<<ALLTRIM(lcRefs)>>'
	ENDTEXT 	
	If !uf_gerais_actGrelha("", "ucrsPvpsSemConvencao", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar as pre�os dos servi�os. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	********************************************************************	


	** Calcula PVPs para caso n�o existam conven��es para o servi�o e utente, mas que em que existem o servi�o na teabela de pre�os do m�dico
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_PvpsEspecialistaSemConvencao'<<ALLTRIM(lcRefs)>>'
	ENDTEXT 	
	If !uf_gerais_actGrelha("", "ucrsPvpsEspecialistaSemConvencao", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar as pre�os dos servi�os. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	********************************************************************	

	** Calcula informa��o de Conven��es e PVP associados ao utente para todos os servi�os
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_marcacoes_convencoesUtentes <<ucrsSeriesServ.no>>,<<ucrsSeriesServ.estab>>
	ENDTEXT 	
	If !uf_gerais_actGrelha("", "ucrsConvencoesUtente", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar as conven��es associadas �s entidades do utente. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	********************************************************************	
				
	
	** Conven��o e PVP
	SELECT ucrsRecursosCalendarioMensal
	GO Top
	SCAN 
		lcRef = ucrsRecursosCalendarioMensal.ref
		lcSite = ucrsRecursosCalendarioMensal.local
		lcUserNo = ucrsRecursosCalendarioMensal.utilizador
		
	
		** 1. CONVEN��O
		SELECT ucrsConvencoesUtente
		LOCATE FOR ALLTRIM(ucrsConvencoesUtente.site) == ALLTRIM(lcSite) AND ALLTRIM(ucrsConvencoesUtente.ref) == ALLTRIM(lcRef) 
		IF FOUND()
			SELECT ucrsRecursosCalendarioMensal	
			Replace ucrsRecursosCalendarioMensal.pvp WITH ucrsConvencoesUtente.pvp 
		ELSE

			** 2. PVP ESPECIALISTA LOCAL
			&& Caso exista pre�o para o servi�o,especialista e local
			SELECT ucrsPvpsEspecialistaSemConvencao
			GO Top
			SELECT ucrsPvpsEspecialistaSemConvencao
			LOCATE FOR ALLTRIM(ucrsPvpsEspecialistaSemConvencao.site) == ALLTRIM(lcSite) AND ALLTRIM(ucrsPvpsEspecialistaSemConvencao.ref) == ALLTRIM(lcRef);
				AND ucrsPvpsEspecialistaSemConvencao.no = lcUserNo 
			IF FOUND()

				SELECT ucrsRecursosCalendarioMensal	
				Replace ucrsRecursosCalendarioMensal.pvp WITH ucrsPvpsEspecialistaSemConvencao.pvp
			ELSE
					
				** 3. PVP FICHA LOCAL
				&& caso n�o tenha conven��o coloca pre�o da ficha do local em causa
				SELECT ucrsPvpsSemConvencao
				GO Top
				SELECT ucrsPvpsSemConvencao
				LOCATE FOR ALLTRIM(ucrsPvpsSemConvencao.site) == ALLTRIM(lcSite) AND ALLTRIM(ucrsPvpsSemConvencao.ref) == ALLTRIM(lcRef) 
				IF FOUND()

					SELECT ucrsRecursosCalendarioMensal	
					Replace ucrsRecursosCalendarioMensal.pvp WITH ucrsPvpsSemConvencao.pvp
				ELSE
					SELECT ucrsRecursosCalendarioMensal	
					Replace ucrsRecursosCalendarioMensal.pvp WITH 0
				ENDIF 
				
			ENDIF 
		ENDIF
	ENDSCAN 
	SELECT ucrsRecursosCalendarioMensal
	GO Top
	
	
ENDFUNC



**
FUNCTION uf_SERIESSERV_controlaPass
	
	PUBLIC myVirtualText, myVirtualVar, myPassWordChar 
		
	Local lcPass, cval, lcVal
	Store '' To lcPass, cval
	Store .f. To lcVal
			
	** guardar pass **
	IF !Empty(uf_gerais_getParameter("ADM0000000076","TEXT"))
		lcPass = uf_gerais_getParameter("ADM0000000076","TEXT")
	ENDIF
	
	** pedir password **
	SERIESSERV.pass.value = ''
		
	**chama Painel Virtual, funcoes genericas
	uf_tecladoAlpha_Chama('SERIESSERV.pass','INTRODUZA PASSWORD DE SUPERVISOR:',.t.,.f.,1)

	cval=alltrim(SERIESSERV.pass.value)
	
	DO CASE
		CASE empty(cval) Or Empty(lcPass)
			uf_perguntalt_chama("DEVE INTRODUZIR A PASSWORD DE SUPERVISOR PARA REALIZAR ESTA OPERA��O.","OK","",48)
		CASE !(Upper(Alltrim(lcPass))==Upper(Alltrim(cval)))
			uf_perguntalt_chama("A PASSWORD N�O EST� CORRECTA. POR FAVOR VERIFIQUE SE EST� A INTRODUZIR CORRECTAMENTE.","OK","",16)
		OTHERWISE
			lcVal = .t.
	ENDCASE
		
	RETURN lcVal	
	
ENDFUNC 


**
FUNCTION uf_SERIESSERV_NovoPedidoImprevisto
	LOCAL lcPosicao, lcData 
	
	lcData = DATE(VAL(SERIESSERV.ano),VAL(SERIESSERV.mes),VAL(SERIESSERV.dia))

	Select ucrsPeriodosCalendarioMensal
	Calculate Max(posicao) to lcPosicao
	
	Select ucrsPeriodosCalendarioMensal
	Calculate Max(Horafim) to lcHinicio

	** Dura��o de servi�os ***************************************
	lcDuracao = 30
	
	lcTempoInicio = DATETIME(YEAR(lcData),MONTH(lcData),DAY(lcData),val(LEFT(lcHinicio,2)),val(RIGHT(lcHinicio,2)),0)
	lcHfim = lcTempoInicio + (lcDuracao * 60)
					
	Select ucrsPeriodosCalendarioMensal
	APPEND blank
	replace ucrsPeriodosCalendarioMensal.posicao WITH lcPosicao+1
	replace ucrsPeriodosCalendarioMensal.periodo WITH lcPosicao+1
	replace ucrsPeriodosCalendarioMensal.dataIni WITH lcData
	replace ucrsPeriodosCalendarioMensal.dataFim WITH lcData
	replace ucrsPeriodosCalendarioMensal.horaIni WITH lcHinicio
	replace ucrsPeriodosCalendarioMensal.horaFim WITH LEFT(TtoC(lcHfim,2),5)
	replace ucrsPeriodosCalendarioMensal.extra WITH .t.

	SELECT ucrsPeriodosCalendarioMensal
	GO BOTTOM 
	seriesserv.Pageframe1.Page5.Container1.GridPesq.refresh
ENDFUNC 


**
FUNCTION uf_SERIESSERV_fichaUtente
	SELECT ucrsSeriesServ
	IF !EMPTY(ucrsSeriesServ.no)
		uf_utentes_chama(ucrsSeriesServ.no)
	ENDIF 
ENDFUNC



**
FUNCTION uf_SERIESSERV_fichaServico

	SELECT ucrsRecursosCalendarioMensal
	IF !EMPTY(ucrsRecursosCalendarioMensal.ref)
		uf_stocks_chama(ucrsRecursosCalendarioMensal.ref)
	ENDIF 
	
ENDFUNC


**
FUNCTION uf_SERIESSERV_exit
	**
	RELEASE mySERIESSERVIntroducao
	RELEASE mySERIESSERVAlteracao
	
	
	IF USED("ucrsPeriodosDisponiveisCombinados")
		fecha("ucrsPeriodosDisponiveisCombinados")
	ENDIF
	
	IF USED("ucrsPeriodosDisponiveisCombinadosAux")
		fecha("ucrsPeriodosDisponiveisCombinadosAux")
	ENDIF
	
	IF USED("ucrsPeriodosDisponiveis2")
		fecha("ucrsPeriodosDisponiveis2")
	ENDIF
	
	IF USED("ucrsPeriodosDisponiveisRefs")
		fecha("ucrsPeriodosDisponiveisRefs")
	ENDIF
	
	IF USED("ucrsPeriodosDisponiveis")
		fecha("ucrsPeriodosDisponiveis")
	ENDIF
	
	IF USED("ucrsSessoesSerie")
		fecha("ucrsSessoesSerie")
	ENDIF 
	
	
	SERIESSERV.hide
	SERIESSERV.release
ENDFUNC




