**
FUNCTION uf_pesqSeries_Chama
	LPARAMETERS lcOrigem, lcNo, lcTipo

	IF EMPTY(lcNo)
		lcNo = 0
	ENDIF 
	IF EMPTY(lcTipo)
		lcTipo = ""
	ENDIF 
	
	IF TYPE("pesqseries")=="U"
	
		IF !USED("ucrsPesqActividades")
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_marcacoes_seriesPesq '',0,'<<ALLTRIM(lcTipo)>>',0,<<lcNo>>
			ENDTEXT 
			IF !uf_gerais_actgrelha("", "ucrsPesqSeries", lcSql)
				MESSAGEBOX("N�o foi possivel aplicar defini��es pesquisa. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
				RETURN .f.
			ENDIF
		ENDIF
		
		DO FORM pesqseries
	ELSE
		pesqseries.show
	ENDIF
	
	IF !EMPTY(lcOrigem)
		pesqseries.origem = "MARCACOES_EDIT"
	ELSE
		pesqseries.origem = ""
	ENDIF
	
	uf_pesqSeries_carregaMenu(lcTipo)
	
	IF ALLTRIM(UPPER(lcTipo)) == "UTILIZADORES"
		pesqSeries.tipo.value = "Utilizadores"
		pesqSeries.tipo.refresh
	ENDIF 
ENDFUNC 


**
FUNCTION uf_pesqSeries_carregaMenu
	LPARAMETERS lnTipo
	LOCAL lcTipo
	
	IF !EMPTY(lnTipo)
		lcTipo = lnTipo
	ELSE
		lcTipo = ""
	ENDIF 

	pesqSeries.menu1.adicionaOpcao("actualizar", "Actualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_pesqSeries_actPesquisa","A")
	
	IF ALLTRIM(UPPER(lcTipo)) != "UTILIZADORES"
		pesqSeries.menu1.adicionaOpcao("novoServ","Servi�os",myPath + "\imagens\icons\mais_w.png","uf_pesqSeries_novaSerie with 1","N")
		pesqSeries.menu1.adicionaOpcao("novaAtividade","Atividades",myPath + "\imagens\icons\mais_w.png","uf_pesqSeries_novaSerie with 3","N")
	ENDIF 
	
	**
	pesqSeries.menu1.adicionaOpcao("novoUtilizador","Utilizadores",myPath + "\imagens\icons\mais_w.png","uf_pesqSeries_novaSerie with 2","N")
	
	IF ALLTRIM(UPPER(lcTipo)) != "UTILIZADORES"
		pesqSeries.menu1.adicionaOpcao("novoRecurso","Recursos",myPath + "\imagens\icons\mais_w.png","uf_pesqSeries_novaSerie with 4","N")
	ENDIF
			
	pesqSeries.menu1.adicionaOpcao("limpar", "Limpar", myPath + "\imagens\icons\limpar_w.png", "uf_pesqSeries_limparDados")
&&	pesqSeries.menu1.adicionaOpcao("subir", "Subir", myPath + "\imagens\icons\ponta_seta_up.png", "uf_gerais_MovePage with .t.,'pesqSeries.gridpesq','ucrsPesqSeries'")
&&	pesqSeries.menu1.adicionaOpcao("descer", "Descer", myPath + "\imagens\icons\ponta_seta_down.png", "uf_gerais_MovePage with .f.,'pesqSeries.gridpesq','ucrsPesqSeries'")
ENDFUNC


**
FUNCTION uf_pesqSeries_actPesquisa

	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_marcacoes_seriesPesq '<<ALLTRIM(pesqSeries.nome.value)>>',<<IIF(EMPTY(ALLTRIM(pesqSeries.no.value)),0,ALLTRIM(pesqSeries.no.value))>>,'<<ALLTRIM(pesqSeries.tipo.value)>>'
	ENDTEXT 

	IF !uf_gerais_actgrelha("PESQSERIES.gridPesq", "ucrsPesqSeries", lcSql)
		MESSAGEBOX("N�o foi possivel aplicar defini��es pesquisa. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	pesqSeries.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("ucrsPesqSeries"))) + " Resultados"
	
	pesqSeries.refresh
ENDFUNC


**
FUNCTION uf_pesqSeries_limparDados

	pesqSeries.nome.value = ''
	pesqSeries.no.value = ''
	pesqSeries.tipo.value = ''
	uf_pesqSeries_actPesquisa()
ENDFUNC 


**
FUNCTION uf_pesqseries_sel

	IF pesqseries.origem = "MARCACOES_EDIT"
		SELECT uCrsPesqSeries
		SELECT ucrsMarcacoesDia
		Replace ucrsMarcacoesDia.serieno WITH uCrsPesqSeries.serieno
		Replace ucrsMarcacoesDia.serienome WITH uCrsPesqSeries.serienome
		
		marcacoes.refresh
	ELSE
		SELECT uCrsPesqSeries
		uf_SERIESSERV_chama(uCrsPesqSeries.seriestamp)
	ENDIF
	uf_pesqseries_sair()

ENDFUNC


**
FUNCTION uf_pesqSeries_novaSerie
	LPARAMETERS lcTipo
	
	uf_SERIESSERV_chama()
	uf_SERIESSERV_novo()
	DO CASE 
		CASE lcTipo = 1 && Servi�o
			Select ucrsSeriesServ
			Replace ucrsSeriesServ.tipo with "Servi�os"
			
		CASE lcTipo = 2 && disponibilidade Utilizador
			Select ucrsSeriesServ
			Replace ucrsSeriesServ.tipo with "Utilizadores"
			
			IF USED("ucrsDadosUsDisp") && Coloca Utilizador
				
				
				SELECT ucrsSeriesServ
				SELECT ucrsDadosUsDisp 
				REPLACE ucrsSeriesServ.nome WITH ALLTRIM(ucrsDadosUsDisp.nome)
				REPLACE ucrsSeriesServ.no WITH ucrsDadosUsDisp.userno
				REPLACE ucrsSeriesServ.estab WITH 0
						
			ENDIF 
			
		CASE lcTipo = 3 && disponibilidade Marca��o Atividade
			Select ucrsSeriesServ
			Replace ucrsSeriesServ.tipo with "Atividades"
		CASE lcTipo = 4 && disponibilidade Recurso
			Select ucrsSeriesServ
			Replace ucrsSeriesServ.tipo with "Recursos"
	ENDCASE 	
		
	uf_SERIESSERV_alternaMenu()	
	uf_SERIESSERV_alteraVisualizacao()	
	uf_pesqseries_sair()
ENDFUNC 


**
FUNCTION uf_pesqSeries_sair

	IF USED("ucrsPesqSeries")
		fecha("ucrsPesqSeries")
	ENDIF

	pesqSeries.hide
	pesqSeries.release
ENDFUNC
