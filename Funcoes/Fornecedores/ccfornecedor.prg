FUNCTION uf_CCFornecedor_chama
	LPARAMETERS lcNo, lcEstab, lcNome
	
	&&valida parameters
	IF EMPTY(lcNO) OR !(Type("lcNO")=="N")
		lcNO= 0
	ENDIF
	
	IF EMPTY(lcEstab) OR !(Type("lcEstab")=="N")
		lcEstab = 0
	ENDIF
	
	IF EMPTY(lcNome)
		lcNome = ""
	ENDIF
	*******************************
	
	** cria cursor vazio
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_fornecedores_cc <<lcNo>>, <<lcEstab>>, '20000101', '20000101', ''
	ENDTEXT 
	
	IF !uf_gerais_actgrelha("", "uCrsPesqCCF", lcSql)
		uf_perguntalt_chama("ERRO AO LER A CONTA CORRENTE DO FORNECEDOR!","OK","",16)
		RETURN .F.
	ENDIF 
	
	&& Controla Abertura do Painel			
	if type("CCFornecedor")=="U"
		DO FORM CCFornecedor
	ELSE
		CCFornecedor.show
	ENDIF
	
	CCFornecedor.txtNome.value = ALLTRIM(lcNome)
	CCFornecedor.txtNo.value = lcNo
	CCFornecedor.txtEstab.value = lcEstab
	
	** carrega dados
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_fornecedores_cc <<lcNo>>, <<lcEstab>>, '<<uf_gerais_getDate(CCFornecedor.txtDe.value, "SQL")>>', '<<uf_gerais_getDate(CCFornecedor.txtA.value, "SQL")>>',<<IIF(CCFornecedor.menu1.loja.tag=='true', "'"+ALLTRIM(mySite)+"'", "''")>>
	ENDTEXT 	
	IF !uf_gerais_actgrelha("CCFornecedor.GridPesq", "uCrsPesqCCF", lcSql)
		uf_perguntalt_chama("ERRO AO LER A CONTA CORRENTE DO FORNECEDOR!","OK","", 16)
		RETURN .F.
	ENDIF
	
	uf_CCFornecedor_CalcularTotais()
	
	CCFornecedor.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqCCF"))) + " registos"
ENDFUNC



**
FUNCTION uf_CCFornecedor_carregamenu
	CCFornecedor.menu1.adicionaopcao("atualizar", "Atualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_CCFornecedor_Pesquisa", "A")
	CCFornecedor.menu1.adicionaopcao("estab", "Inc. Estab.", myPath + "\imagens\icons\unchecked_w.png", "uf_CCFornecedor_incluirEstab","E")
	CCFornecedor.menu1.adicionaopcao("imprimir", "Imprimir", myPath + "\imagens\icons\imprimir_w.png", "uf_CCFornecedor_imprimir","I")
	CCFornecedor.menu1.adicionaopcao("navegar", "Ver Doc.", myPath + "\imagens\icons\doc_seta_w.png", "uf_CCFornecedor_Navegar","N")
	CCFornecedor.menu1.adicionaopcao("loja", "Loja Act.", myPath + "\imagens\icons\unchecked_w.png", "uf_CCFornecedor_lojaAct","L")
	
	CCFornecedor.menu1.estado("estab", "HIDE")
	
ENDFUNC

**
FUNCTION uf_CCFornecedor_incluirEstab
	IF CCFornecedor.menu1.estab.tag == 'false'
		&& aplica sele��o
		CCFornecedor.menu1.estab.tag = 'true'
		
		&& altera o botao
		CCFornecedor.menu1.estab.config("Inc. Estab.", myPath + "\imagens\icons\checked_w.png", "uf_CCFornecedor_incluirEstab","E")
	ELSE
		&& aplica sele��o
		CCFornecedor.menu1.estab.tag = 'false'
		
		&& altera o botao
		CCFornecedor.menu1.estab.config("Inc. Estab.", myPath + "\imagens\icons\unchecked_w.png", "uf_CCFornecedor_incluirEstab","E")
	ENDIF
	
	uf_CCFornecedor_Pesquisa()
ENDFUNC

**
FUNCTION uf_CCFornecedor_lojaAct
	IF CCFornecedor.menu1.loja.tag == 'false'
		&& aplica sele��o
		CCFornecedor.menu1.loja.tag = 'true'
		
		&& altera o botao
		CCFornecedor.menu1.loja.config("Loja Act.", myPath + "\imagens\icons\checked_w.png", "uf_CCFornecedor_lojaAct","L")
	ELSE
		&& aplica sele��o
		CCFornecedor.menu1.loja.tag = 'false'
		
		&& altera o botao
		CCFornecedor.menu1.loja.config("Loja Act.", myPath + "\imagens\icons\unchecked_w.png", "uf_CCFornecedor_lojaAct","L")
	ENDIF
	
	uf_CCFornecedor_Pesquisa()
ENDFUNC


*!*	**
*!*	FUNCTION uf_CCFornecedor_CalcularTotais
*!*		LOCAL lcTotEdeb, lcTotEcred, lcSaldo, lcTotEdeb2, lcTotEcred2, lcSaldoAnterior 
*!*		lcTotEdeb = 0
*!*		lcTotEcred = 0
*!*		lcSaldo = 0		
*!*		lcTotEdeb2 = 0
*!*		lcTotEcred2 = 0
*!*		lcSaldoAnterior = 0
*!*		
*!*		
*!*		lcSQL = ""
*!*		TEXT TO lcSQL TEXTMERGE NOSHOW
*!*			exec up_fornecedores_cc <<CCFornecedor.txtNo.value>>, <<CCFornecedor.txtEstab.value>>, '19000101', '<<uf_gerais_getDate(CTOD(CCFornecedor.txtDe.value)-1, "SQL")>>', ''
*!*		ENDTEXT 
*!*		
*!*		IF uf_gerais_actgrelha("", "uCrsCCFAnt", lcSql)
*!*			IF RECCOUNT("uCrsCCFAnt")>0
*!*				Select uCrsCCFAnt
*!*				Go TOP
*!*				SCAN
*!*					*******************************
*!*					* Calcula o Total das colunas *
*!*					*******************************
*!*					lcTotEdeb2=lcTotEdeb2+uCrsCCFAnt.edeb
*!*					lcTotEcred2=lcTotEcred2+uCrsCCFAnt.ecred
*!*				ENDSCAN
*!*				
*!*				Select uCrsPesqCCF
*!*				GO TOP 
*!*				replace	uCrsPesqCCF.DATALC WITH "..."
*!*				replace	uCrsPesqCCF.CMDESC WITH "Saldo Anterior"
*!*				replace uCrsPesqCCF.EDEB WITH lcTotEdeb2
*!*				replace uCrsPesqCCF.ECRED WITH lcTotEcred2
*!*				replace uCrsPesqCCF.SALDO WITH lcTotEcred2 - lcTotEdeb2
*!*				lcSaldoAnterior = lcTotEcred2 - lcTotEdeb2
*!*			
*!*			ELSE
*!*				IF Used("uCrsCCFAnt")
*!*					Fecha("uCrsCCFAnt")
*!*				ENDIF
*!*				
*!*				CCFornecedor.txtDeb.value = 0
*!*				CCFornecedor.txtCred.value = 0
*!*				CCFornecedor.txtSaldo.value = 0
*!*			ENDIF
*!*		ENDIF
*!*		
*!*		
*!*		**
*!*		IF RECCOUNT("uCrsPesqCCF")>0
*!*		
*!*		
*!*			
*!*			Select uCrsPesqCCF
*!*			Go top
*!*			Scan
*!*				**********************************
*!*				* Calcular o Saldo de cada linha *
*!*				**********************************
*!*				lcSaldo = lcSaldoAnterior + lcSaldo + (uCrsPesqCCF.ecred - uCrsPesqCCF.edeb) 
*!*				replace uCrsPesqCCF.saldo	With 	lcSaldo

*!*				*******************************
*!*				* Calcula o Total das colunas *
*!*				*******************************
*!*				lcTotEdeb=lcTotEdeb + uCrsPesqCCF.edeb
*!*				lcTotEcred=lcTotEcred + uCrsPesqCCF.ecred
*!*			ENDSCAN
*!*			
*!*			SELECT uCrsPesqCCF
*!*			Append Blank
*!*			Append Blank
*!*			replace uCrsPesqCCF.CMDESC With	"Total do Per�odo"
*!*			replace uCrsPesqCCF.EDEB WITH lcTotEdeb
*!*			replace uCrsPesqCCF.ECRED WITH lcTotEcred
*!*			replace uCrsPesqCCF.saldo WITH Round(lcTotEcred - lcTotEdeb,2) 
*!*			
*!*			Select uCrsPesqCCF
*!*			GO TOP
*!*		ENDIF 
*!*		
*!*		CCFornecedor.txtDeb.value = Round(lcTotEdeb+lcTotEdeb2,2)
*!*		CCFornecedor.txtCred.value = Round(lcTotEcred+lcTotEcred2,2)
*!*		CCFornecedor.txtSaldo.value = Round((lcTotEcred+lcTotEcred2)-(lcTotEdeb+lcTotEdeb2),2)
*!*		
*!*		

*!*		
*!*		CCFornecedor.gridPesq.refresh
*!*	ENDFUNC 



**
FUNCTION uf_CCFornecedor_CalcularTotais
	LOCAL lcTotEdeb, lcTotEcred, lcSaldo, lcTotEdeb2, lcTotEcred2
	lcTotEdeb = 0
	lcTotEcred = 0
	lcSaldo = 0		
	lcTotEdeb2 = 0
	lcTotEcred2 = 0
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW

		exec up_fornecedores_cc 
			<<CCFornecedor.txtNo.value>>,
			<<CCFornecedor.txtEstab.value>>, 
			'19000101',
			'<<uf_gerais_getDate(CTOD(CCFornecedor.txtDe.value)-1, "SQL")>>',
			'',
			0

		
	ENDTEXT 

	IF uf_gerais_actgrelha("", "uCrsCCFAnt", lcSql)
		IF RECCOUNT("uCrsCCFAnt")>0
			Select uCrsCCFAnt
			Go TOP
			SCAN
				*******************************
				* Calcula o Total das colunas *
				*******************************
				lcTotEdeb2 = lcTotEdeb2+uCrsCCFAnt.edeb
				lcTotEcred2 = lcTotEcred2+uCrsCCFAnt.ecred
			ENDSCAN
			
			Select uCrsPesqCCF
			GO TOP 
			replace	uCrsPesqCCF.DATALC WITH "..."
			replace	uCrsPesqCCF.CMDESC WITH "Saldo Anterior"
			replace uCrsPesqCCF.EDEB WITH lcTotEdeb2
			replace uCrsPesqCCF.ECRED WITH lcTotEcred2
			replace uCrsPesqCCF.SALDO WITH lcTotEcred2 - lcTotEdeb2
		ELSE
			IF Used("uCrsCCFAnt")
				Fecha("uCrsCCFAnt")
			ENDIF
			
			CCFornecedor.txtDeb.value = 0
			CCFornecedor.txtCred.value = 0
			CCFornecedor.txtSaldo.value = 0
			CCFornecedor.txtDebT.value = 0
			CCFornecedor.txtCredT.value = 0
			CCFornecedor.txtSaldoT.value = 0
		ENDIF
	ENDIF
	
	IF RECCOUNT("uCrsPesqCCF") > 0
		lcTotEdeb  = 0
		lcTotEcred = 0
		
		Select uCrsPesqCCF
		GO TOP
		SCAN
			**********************************
			* Calcular o Saldo de cada linha *
			**********************************
			lcSaldo = lcSaldo  + (uCrsPesqCCF.ecred - uCrsPesqCCF.edeb)
			SELECT uCrsPesqCCF
			replace uCrsPesqCCF.saldo WITH lcSaldo

			*******************************
			* Calcula o Total das colunas *
			*******************************
			IF RECNO("uCrsPesqCCF") > 1
				lcTotEdeb = lcTotEdeb+uCrsPesqCCF.edeb
				lcTotEcred = lcTotEcred+uCrsPesqCCF.ecred
			ENDIF 
		ENDSCAN

		CCFornecedor.txtDeb.value = Round(lcTotEdeb,2)
		CCFornecedor.txtCred.value = Round(lcTotEcred,2)
		CCFornecedor.txtSaldo.value = Round(lcTotEcred - lcTotEdeb,2)
		
		CCFornecedor.txtDebT.value = Round(lcTotEdeb + lcTotEdeb2,2)
		CCFornecedor.txtCredT.value = Round(lcTotEcred + lcTotEcred2,2)
		CCFornecedor.txtSaldoT.value = Round(lcTotEcred - lcTotEdeb + lcTotEcred2 - lcTotEdeb2,2)
		
		Select uCrsPesqCCF
		GO TOP
		TRY
			SKIP 1
		CATCH
		ENDTRY
	ENDIF 
	
	CCFornecedor.gridPesq.refresh
ENDFUNC 




**
FUNCTION uf_CCFornecedor_Pesquisa
	LOCAL lcSQL
	
	regua(0,0,"A pesquisar...")
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_fornecedores_cc <<CCFornecedor.txtNo.value>>, <<CCFornecedor.txtEstab.value>>, '<<uf_gerais_getdate(CCFornecedor.txtDe.value, "SQL")>>', '<<uf_gerais_getDate(CCFornecedor.txtA.value, "SQL")>>',<<IIF(CCFornecedor.menu1.loja.tag=='true', "'"+ALLTRIM(mySite)+"'", "''")>>
	ENDTEXT

	uf_gerais_actGrelha("CCFornecedor.GridPesq", "ucrsPesqCCF", lcSQL)
		
	uf_CCFornecedor_CalcularTotais()
	
	regua(2)
	
	CCFornecedor.Refresh
	
	CCFornecedor.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqCCF"))) + " registos"
ENDFUNC



**
FUNCTION uf_CCFornecedor_Imprimir
	IF RECCOUNT("ucrsPesqCCF") > 0
	
		**Trata parametros
		LOCAL lcNo, lcEstab
		lcNo = 0		
		lcEstab = 0
		
		IF !Empty(CCFornecedor.txtNo.value)
			lcNo = CCFornecedor.txtNo.value
		ENDIF
		IF !Empty(CCFornecedor.txtEstab.value)
			lcEstab = CCFornecedor.txtEstab.value
		ENDIF	

		** Construir string com parametros
		lcSql = "&No=" 			+ ALLTRIM(STR(lcNo)) +;
				"&estab=" 		+ ALLTRIM(STR(lcEstab)) +;
				"&dataIni="		+ uf_gerais_getDate(CCFornecedor.txtDe.value, 'DATA') +;
				"&dataFim="		+ uf_gerais_getDate(CCFornecedor.txtA.value, 'DATA') +;
				"&site=" 		+ IIF(CCFornecedor.menu1.loja.tag=='true', ALLTRIM(mySite), "")

		** chamar report pelo nome dele
		uf_gerais_chamaReport("relatorio_CC_Fornecedor", lcSql)
	ENDIF
ENDFUNC



**
FUNCTION uf_CCFornecedor_Navegar
	If myDocIntroducao == .t. OR myDocAlteracao == .t.
		uf_perguntalt_chama("O ECR� DE DOCUMENTOS ENCONTRA-SE EM MODO DE INTRODU��O OU EDI��O.","OK","", 48)
	ELSE
		SELECT uCrsPesqCCF
		DO CASE 
			CASE uCrsPesqCCF.origem == "FO"
				If !Empty(uCrsPesqCCF.fcstamp)
					uf_documentos_chama(Alltrim(uCrsPesqCCF.fcstamp))
				ENDIF
			CASE uCrsPesqCCF.origem == "PO"
				If !Empty(uCrsPesqCCF.fcstamp)
					uf_pagforn_chama(Alltrim(uCrsPesqCCF.fcstamp))
				ENDIF
		ENDCASE
	Endif
ENDFUNC



**
FUNCTION uf_CCFornecedor_limpa
	CCFornecedor.txtDe.Value = uf_gerais_getDate(Date()-60)
	CCFornecedor.txtA.Value = uf_gerais_getDate()
	
	IF CCFornecedor.menu1.estab.tag == 'true'
		uf_CCFornecedor_incluirEstab()
	ENDIF
ENDFUNC



**
FUNCTION uf_CCFornecedor_sair
	**Fecha Cursores
	IF USED("uCrsPesqCCF")
		fecha("uCrsPesqCCF")
	ENDIF 
	
	CCFornecedor.hide
	CCFornecedor.release
ENDFUNC