**
FUNCTION uf_pesqpagforn_chama
	LOCAL lcSQL
	lcSql = ''
	
	** Controle de Permiss�es	
	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Pagamentos a fornecedores')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL GEST�O DE PAGAMENTOS.","OK","",48)
		RETURN .f.
	ENDIF
	
	uf_pesqpagforn_CarregaDados(.t.)
	
	IF TYPE('PESQPAGFORN')=="U"
		DO FORM PESQPAGFORN
	ELSE
		PESQPAGFORN.show
	ENDIF
ENDFUNC


**
** tcBool	: caso venha a .t. cria apenas o cursor
**
FUNCTION uf_PESQPAGFORN_CarregaDados
	LPARAMETERS tcBool
	
	LOCAL lcSQL
	lcSql = ''
	
	IF tcBool == .t.
	
		TEXT TO lcSQL NOSHOW TEXTMERGE
			SET fmtonly on
			exec up_fornecedores_pesqpagforn 0,'',0,-1,'19000101','20140101'
			SET fmtonly off
		ENDTEXT

		uf_gerais_actGrelha(IIF(TYPE("PESQPAGFORN")=="U","","PESQPAGFORN.grdPagForn"), "uCrsPesqPagForn", lcSQL)
		
	ELSE && Refresca valores da Grid
	
		regua(0, 2, "A processar a Pesquisa...")
		regua(1, 1, "A processar a Pesquisa...")
		
		LOCAL lcNo, lcForn, lcFornNo, lcFornEstab, lcDataIni, lcDataFim
		lcNo 		= IIF(EMPTY(PESQPAGFORN.txtNo.value),0,PESQPAGFORN.txtNo.value)
		lcForn		= ALLTRIM(PESQPAGFORN.txtForn.value)
		lcFornNo	= IIF(EMPTY(PESQPAGFORN.txtNoForn.value),0,PESQPAGFORN.txtNoforn.value)
		lcFornEstab	= IIF(EMPTY(PESQPAGFORN.txtDepForn.value),-1,PESQPAGFORN.txtDepForn.value)
		lcDataIni	= uf_gerais_getDate(PESQPAGFORN.txtdataIni.value)
		lcDataFim	= uf_gerais_getDate(PESQPAGFORN.txtdataFim.value)
		
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_fornecedores_pesqpagforn <<lcNo>>,'<<lcForn>>',<<lcFornNo>>,<<lcFornEstab>>,'<<lcDataIni>>','<<lcDataFim>>'
		ENDTEXT

		uf_gerais_actGrelha("PESQPAGFORN.grdPagForn", "uCrsPesqPagForn", lcSQL)
		
		regua(1, 2, "A processar a Pesquisa...")
		
		regua(2)

		PESQPAGFORN.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqPagForn"))) + " Resultados"

		PESQPAGFORN.Refresh
	ENDIF
ENDFUNC

FUNCTION uf_PESQPAGFORN_imprimir
	LOCAL lcNo, lcForn, lcFornNo, lcFornEstab, lcDataIni, lcDataFim
	lcNo 		= IIF(EMPTY(PESQPAGFORN.txtNo.value),0,PESQPAGFORN.txtNo.value)
	lcForn		= ALLTRIM(PESQPAGFORN.txtForn.value)
	lcFornNo	= IIF(EMPTY(PESQPAGFORN.txtNoForn.value),0,PESQPAGFORN.txtNoforn.value)
	lcFornEstab	= IIF(EMPTY(PESQPAGFORN.txtDepForn.value),-1,PESQPAGFORN.txtDepForn.value)
	lcDataIni	= uf_gerais_getDate(PESQPAGFORN.txtdataIni.value)
	lcDataFim	= uf_gerais_getDate(PESQPAGFORN.txtdataFim.value)
	
	parametros  = ''
	TEXT TO parametros NOSHOW TEXTMERGE
		&no=<<lcNo>>&fornecedor=<<lcForn>>&fornno=<<lcFornNo>>&fornestab=<<lcFornEstab>>&datainicio=<<lcDataIni>>&dataFim=<<lcDataFim>>&site=<<mySite>>
	ENDTEXT
	
	uf_gerais_chamaReport("listagem_pesquisa_pagforn", parametros)
ENDFUNC

**
FUNCTION uf_PESQPAGFORN_Sel
	Select uCrsPesqPagForn
	uf_pagforn_chama(uCrsPesqPagForn.postamp)
		
	uf_PESQPAGFORN_sair()
ENDFUNC


**
FUNCTION uf_PESQPAGFORN_sair
	** Fecha Cursores
	fecha("uCrsPesqPagForn")

	PESQPAGFORN.hide
	PESQPAGFORN.release
ENDFUNC


**
FUNCTION uf_PESQPAGFORN_CarregaMenu
	PESQPAGFORN.menu1.adicionaOpcao("actualizar", "Actualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_PESQPAGFORN_CarregaDados with .f.","A")
	PESQPAGFORN.menu1.adicionaOpcao("imprimir", "Imprimir", myPath + "\imagens\icons\imprimir_w.png", "uf_PESQPAGFORN_imprimir","I")
	**PESQPAGFORN.menu1.adicionaOpcao("tecladoVirtual", "Teclado", myPath + "\imagens\icons\teclado_w.png", "uf_PESQPAGFORN_tecladoVirtual","T")

	PESQPAGFORN.refresh
ENDFUNC


**
FUNCTION uf_PESQPAGFORN_tecladoVirtual
	PESQPAGFORN.tecladoVirtual1.show("PESQPAGFORN.grdPagForn", 161, "PESQPAGFORN.shape7", 161)
ENDFUNC
