*********************************************
*				FUN��O PRINCIPAL			*
*********************************************
FUNCTION uf_pagforn_chama
	LPARAMETERS lcStamp
		
	&& CONTROLA PERFIS DE ACESSO
	IF uf_gerais_validaPermUser(ch_userno,ch_grupo,'Pagamentos a fornecedores') == .f.
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL PAGAMENTOS A FORNECEDOR.","OK","",48)
		RETURN .f.
	ENDIF



	PUBLIC myPagFornIntroducao, myPagFornAlteracao
	
	IF EMPTY(lcStamp) OR !(Type("lcStamp")=="C")
		lcStamp = ''
	ENDIF
			
	regua(1,2,"A carregar painel...")
	&& Cursor da FL
	LOCAL lcSql
	
	lcSql = ''
	TEXT TO lcSql NOSHOW textmerge
		SELECT TOP 1 * 
		FROM 
			PO (NOLOCK)
		WHERE
			postamp = '<<lcStamp>>'
	ENDTEXT
	uf_gerais_actgrelha("", "PO", lcSql)
	
	regua(1,3,"A carregar painel...")
	
	SELECT PO
	
	&& Cursor de linhas
	lcSQl=''
	TEXT TO lcSQL TEXTMERGE noshow		
		SELECT *, CAST('' as varchar(20)) as numVFactR from pl (NOLOCK)
		where
			postamp = '<<ALLTRIM(PO.postamp)>>'
		order by
			lordem
	ENDTEXT
	uf_gerais_actgrelha(IIF(TYPE("PAGFORN")=="U","","pagforn.pageframe1.page1.grdpl"), "PL", lcSql)
	
	regua(1,4,"A carregar painel...")
	
	&& Controla Abertura do Painel
	IF type("PAGFORN")=="U"
	
		**Valida Licenciamento	
		IF (uf_gerais_addConnection('PAGFOR') == .f.)
			regua(2)
			RETURN .F.
		ENDIF
	
		DO FORM PAGFORN
	ELSE
		PAGFORN.show
	ENDIF
	
	regua(1,5,"A carregar painel...")
	
	IF TYPE("pagforn.menu1.opcoes") == "U"
		uf_PAGFORN_carregamenu()
	ENDIF
	
	&& Configura Ecra
	uf_PAGFORN_confEcra()
	
	
	SELECT PO
	PAGFORN.pageframe1.page1.txtETOTOL1.value = PO.etotal - PO.etotol2
	
	IF PO.process == .t.
		PAGFORN.lblProcessado.visible = .t.
	ELSE
		PAGFORN.lblProcessado.visible = .f.
	ENDIF 
	
	&& Actualiza Ecra
	PAGFORN.refresh
	
	regua(2)
ENDFUNC 



**
FUNCTION uf_PAGFORN_carregamenu
	&& Configura menu principal
	PAGFORN.menu1.adicionaOpcao("opcoes", "Op��es", myPath + "\imagens\icons\opcoes_w.png", "","O")
	PAGFORN.menu1.adicionaOpcao("pesquisar", "Pesquisar", myPath + "\imagens\icons\lupa_w.png", [uf_PAGFORN_pesquisar],"P")
	PAGFORN.menu1.adicionaOpcao("atualizar", "Atualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_PAGFORN_refrescar","A")
	PAGFORN.menu1.adicionaOpcao("novo", "Novo", myPath + "\imagens\icons\mais_w.png", "uf_PAGFORN_novo","N")
	PAGFORN.menu1.adicionaOpcao("editar", "Editar", myPath + "\imagens\icons\lapis_w.png", "uf_PAGFORN_alterar","E")
	**PAGFORN.menu1.adicionaOpcao("eliminar", "Eliminar", myPath + "\imagens\icons\cruz_w.png", "uf_PAGFORN_eliminar","X")
	PAGFORN.menu1.adicionaOpcao("resumo","Resumo",myPath + "\imagens\icons\doc_seta_w.png","uf_pagforn_importREsumo","T")
	PAGFORN.menu1.adicionaOpcao("anterior", "Anterior", myPath + "\imagens\icons\ponta_seta_left_w.png", "uf_PAGFORN_anterior","19")
	PAGFORN.menu1.adicionaOpcao("seguinte", "Seguinte", myPath + "\imagens\icons\ponta_seta_rigth_w.png", "uf_PAGFORN_seguinte","04")
	PAGFORN.menu1.adicionaOpcao("impDoc","Importar CC",myPath + "\imagens\icons\doc_seta_w.png","uf_pagforn_importCC","I")
	

	&& configura menu de op��es
	PAGFORN.menu_opcoes.adicionaOpcao("imprimir", "Imprimir", "", "uf_pagforn_imprimir")
	PAGFORN.menu_opcoes.adicionaOpcao("ultimo", "�ltimo", "", "uf_PAGFORN_navegaUltRegisto")
	PAGFORN.menu_opcoes.adicionaOpcao("eliminar", "Eliminar", "", "uf_PAGFORN_eliminar")
ENDFUNC


FUNCTION uf_pagforn_importCC

	IF !EMPTY(po.numVFactR)

		uf_perguntalt_chama("N�o pode importar documentos depois de importar uma V\Factura Resumo!","OK","",64)

	ELSE

		IF !myPagFornAlteracao AND !myPagFornIntroducao
			**
		ELSE
			SELECT PO
			IF EMPTY(PO.no)
				uf_perguntalt_chama("Por favor selecione um fornecedor.","OK","",64)
			ELSE
				uf_PAGPENDENTES_chama()
			ENDIF
		ENDIF

	ENDIF

ENDFUNC


**
FUNCTION uf_pagforn_imprimir
	SELECT PO
	IF !EMPTY(ALLTRIM(PO.postamp))
		uf_imprimirgerais_Chama("PAGAMENTO")
	ELSE
		RETURN .f.
	ENDIF
ENDFUNC 


**
FUNCTION uf_PAGFORN_alternaMenu
	IF myPagFornIntroducao OR myPagFornAlteracao
		PAGFORN.menu1.estado("opcoes, pesquisar, atualizar, novo, editar, anterior, seguinte", "HIDE", "Gravar", .t., "Sair", .t.)
		PAGFORN.menu1.estado("impDoc, resumo", "SHOW", "Gravar", .t., "Sair", .t.)
	ELSE
		PAGFORN.menu1.estado("opcoes, pesquisar, novo, editar, anterior, seguinte", "SHOW", "Gravar", .f., "Sair", .t.)
        PAGFORN.menu1.estado("atualizar", "HIDE")
		PAGFORN.menu1.estado("impDoc, resumo", "HIDE", "Gravar", .f., "Sair", .t.)
	ENDIF
ENDFUNC


** Configura Ecra Fornecedor
FUNCTION uf_PAGFORN_confEcra
	PAGFORN.pageframe1.tabs	=	.f.

	IF !myPagFornAlteracao AND !myPagFornIntroducao && ecra em modo de consulta
		**
		PAGFORN.caption = "Pagamentos a Fornecedor"

		&&Dados PageFrame
		**PAGFORN.pageframe1.enabled = .f.
		
		WITH PAGFORN.pageframe1.Page1
			.txtRdata.readonly = .t.
			.txtCmdesc.readonly = .t.
			.txtAdoc.readonly = .t.
			.txtMoeda.readonly = .t.
			.grdPL.readonly = .t.
			.txtOlcodigo.readonly = .t.
			.txtOllocal.readonly = .t.
			.txtOllocal2.readonly = .t.			
			.txtEtotol1.readonly = .t.
			.txtEtotol2.readonly = .t.
			.txtFin.readonly = .t.
			.txtEFinv.readonly = .t.
			.txtEtotal.readonly = .t.
			.chkRegIva.enable(.f.)
		ENDWITH															
		
		WITH PAGFORN.pageframe1.Page2
			.txtZona.readonly = .t.
			.txtnib.readonly = .t.
			.txtcobranca.readonly = .t.
			.txtdescba.readonly = .t.
			.txtTxIRS.readonly = .t.
			.txtDesc1.readonly = .t.
			.txtFref.readonly = .t.
			.txtccusto.readonly = .t.			
			.txtintid.readonly = .t.
			.Text2.readonly = .t.
			.txtprocdata.readonly = .t.
			.txtdvalor.readonly = .t.
		ENDWITH						
		
		
		&&P�gina 2
		PAGFORN.pageframe1.page2.chkfaztrf.enable(.f.)
		PAGFORN.pageframe1.page2.chktbok.enable(.f.)
		PAGFORN.pageframe1.page2.chkprocess.enable(.f.)
		PAGFORN.pageframe1.page2.chkImpresso.enable(.f.)
		PAGFORN.pageframe1.page2.chkPlano.enable(.f.)
		
		&& Configura campos obrigat�rios
		PAGFORN.txtNome.disabledbackcolor = RGB(255,255,255)
		PAGFORN.txtNo.disabledbackcolor = RGB(255,255,255)
		PAGFORN.txtEstab.disabledbackcolor = RGB(255,255,255)
		PAGFORN.pageframe1.page1.txtOlcodigo.disabledbackcolor = RGB(255,255,255)
		PAGFORN.pageframe1.page1.txtOllocal.disabledbackcolor = RGB(255,255,255)
	ELSE
		IF myPagFornAlteracao
			PAGFORN.caption = "Pagamentos a Fornecedor - Modo de Altera��o"
		ELSE
			PAGFORN.caption = "Pagamentos a Fornecedor - Modo de Introdu��o"
		ENDIF

		&&Dados PageFrame
		PAGFORN.pageframe1.enabled = .t.
		
		WITH PAGFORN.pageframe1.Page1
			.txtRdata.readonly = .f.
			.txtCmdesc.readonly = .f.
			.txtAdoc.readonly = .f.
			.txtMoeda.readonly = .f.
			.grdPL.readonly = .f.
			.txtOlcodigo.readonly = .f.
			.txtOllocal.readonly = .f.
			.txtOllocal2.readonly = .f.			
			.txtEtotol2.readonly = .f.
			.txtFin.readonly = .f.
			.txtEFinv.readonly = .f.
			.chkRegIva.enable(.t.)
		ENDWITH															
		
		WITH PAGFORN.pageframe1.Page2
			.txtZona.readonly = .f.
			.txtnib.readonly = .f.
			.txtcobranca.readonly = .f.
			.txtdescba.readonly = .f.
			.txtTxIRS.readonly = .f.
			.txtDesc1.readonly = .f.
			.txtFref.readonly = .f.
			.txtccusto.readonly = .f.			
			.txtintid.readonly = .f.
			.Text2.readonly = .f.
			.txtprocdata.readonly = .f.
			.txtdvalor.readonly = .f.
		ENDWITH	
		
		&&p�gina 2
		PAGFORN.pageframe1.page2.chkfaztrf.enable(.t.)
		PAGFORN.pageframe1.page2.chktbok.enable(.t.)
		PAGFORN.pageframe1.page2.chkprocess.enable(.t.)
		PAGFORN.pageframe1.page2.chkImpresso.enable(.t.)
		PAGFORN.pageframe1.page2.chkPlano.enable(.t.)
		
		&& Configura campos obrigat�rios
		PAGFORN.txtNome.disabledbackcolor = RGB(230,242,255)
		PAGFORN.txtNo.disabledbackcolor = RGB(230,242,255)
		PAGFORN.txtEstab.disabledbackcolor = RGB(230,242,255)
		PAGFORN.pageframe1.page1.txtOlcodigo.disabledbackcolor = RGB(230,242,255)
		PAGFORN.pageframe1.page1.txtOllocal.disabledbackcolor = RGB(230,242,255)
	ENDIF
	
	uf_PAGFORN_alternaMenu()
ENDFUNC



** PESQUISAR FORNECEDOR
FUNCTION uf_PAGFORN_pesquisar
	uf_pesqpagforn_chama()
ENDFUNC



** INSERIR FORNECEDOR
FUNCTION uf_PAGFORN_novo
	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Pagamentos a fornecedores - Introduzir')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE CRIAR PAGAMENTOS.","OK","",48)
		RETURN .f.
	ENDIF
	
	uf_PAGFORN_chama('')
	
	SELECT PO
	APPEND BLANK
	
	myPagFornIntroducao = .t.
	uf_PAGFORN_confEcra()
	
	PAGFORN.pageframe1.activepage = 1
					
	&& Valores por defeito
	uf_PAGFORN_ValoresDefeito()
	
	&& Actualiza ecra
	SELECT PO
	PAGFORN.refresh
		
	PAGFORN.txtnome.setfocus
ENDFUNC


** Alterar Fornecedor
FUNCTION uf_PAGFORN_alterar
	
	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Pagamentos a fornecedores - Alterar')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ALTERAR PAGAMENTOS.","OK","",48)
		RETURN .f.
	ENDIF


	&&verifica se o doc j� foi exportado
	select PO
	IF !EMPTY(PO.exportado)
		IF uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Altera��o registos exportados') == .t.
			uf_perguntalt_chama("O seu perfil n�o permite editar registos exportados para a contabilidade.","OK","",32)
			Return .f.
		ENDIF
	
		IF !uf_perguntalt_chama("Documento j� exportado para a Contabilidade. Pretende anular a exporta��o?","N�o","Sim",48)
			
			TEXT TO lcSQL NOSHOW TEXTMERGE
				UPDATE PO SET exportado = 0 WHERE postamp = '<<ALLTRIM(PO.postamp)>>'
			ENDTEXT 
			
			
			If !uf_gerais_actGrelha("", "", lcSQL)
				uf_perguntalt_chama("N�o foi possivel anular a exporta��o do documento","OK","",16)
				RETURN .f.
			ELSE

				SELECT PO
				Replace PO.exportado WITH .f.
			
				uf_perguntalt_chama("Exporta��o anulada com sucesso.","OK","",64)
			Endif
			
		ELSE 
			RETURN .f.			
		ENDIF 
	ENDIF 


	&& Valida Cursor
	SELECT PO
	IF EMPTY(PO.postamp)
		RETURN .f.
	ENDIF
	
	&&ver se est� processado
	SELECT PO
	IF PO.process == .t.
		IF !uf_perguntalt_chama("N�o � poss�vel editar um pagamento j� processado." + CHR(13) + CHR(13) + "Pretende desprocessar o pagamento?","Sim","N�o")
			RETURN .f.
		ELSE
			lcSQL = ''
			TEXT TO lcSQL TEXTMERGE NOSHOW
				UPDATE
					PL
				SET
					process = 0
				where
					postamp = '<<PO.postamp>>'
					
				UPDATE 
					PO 
				SET 
					process = 0
				where 
					postamp = '<<PO.postamp>>'
			ENDTEXT
			IF uf_gerais_actgrelha("", "", lcSQL)
				uf_perguntalt_chama("PAGAMENTO DESPROCESSADO COM SUCESSO!","OK","",64)
				SELECT PO
				replace po.process WITH .f.
				
				SELECT PL
				replace ALL pl.process WITH .f.
			ELSE
				uf_perguntalt_chama("OCORREU UM ERRO AO DESPROCESSAR O PAGAMENTO!","OK","",16)
				RETURN .f.
			ENDIF
		ENDIF
	ENDIF 
	
	&& Configura ecra	
	myPagFornAlteracao = .t.
	uf_PAGFORN_confEcra()
	pagforn.refresh()
ENDFUNC



** Gravar Dados
FUNCTION uf_PAGFORN_gravar
	LOCAL lcSQL,lcValida
	STORE '' TO lcSQL
	STORE 0 TO lcValida

	** Valida��es
	IF empty(ALLTRIM(PAGFORN.txtNome.value))
		uf_perguntalt_chama("O CAMPO: FORNECEDOR, � DE PREENCHIMENTO OBRIGAT�RIO.","OK","", 48)
		RETURN .f.
	ENDIF
	IF empty(ALLTRIM(PAGFORN.pageframe1.page1.txtOlcodigo.value))
		uf_perguntalt_chama("O CAMPO: CLASSIFICA��O, � DE PREENCHIMENTO OBRIGAT�RIO.","OK","", 48)
		RETURN .f.
	ENDIF
	IF empty(ALLTRIM(PAGFORN.pageframe1.page1.txtOllocal.value))
		uf_perguntalt_chama("O CAMPO: LOCAL, � DE PREENCHIMENTO OBRIGAT�RIO.","OK","", 48)
		RETURN .f.
	ENDIF
	
	&&ver se est� processado
	SELECT PO
	IF PO.process == .f.
		IF uf_perguntalt_chama("Pretende processar o pagamento?" + CHR(13) + CHR(13) + "Ser� processado com a data: " + uf_gerais_getDate(PO.procdata,"DATA"),"Sim","N�o")
			SELECT PO
			replace po.process WITH .t.
			
			SELECT PL
			replace ALL pl.process WITH .t.
		ELSE		
			SELECT PL
			replace ALL pl.process WITH .f.
		ENDIF
	ELSE
		SELECT PL
		replace ALL pl.process WITH .t.
	ENDIF 
	
	&& coluna desc1 na PO
	LOCAL lcDesc1
	STORE '' TO lcDesc1
	SELECT PL
	GO TOP
	SCAN 
		lcDesc1 = lcDesc1 + ALLTRIM(pl.cdesc) + " - " + ALLTRIM(pl.adoc) + "; "
	ENDSCAN
	SELECT PO
	replace po.desc1 WITH lcDesc1
	
	lcSQL=""
	IF myPagFornIntroducao == .t.

		** VALIDA��ES
		SELECT PO
		&& Verifica N� do pagamento
		IF uf_gerais_actgrelha("", "uCrsTempNo", "Select Isnull(Max(RNO),0) + 1 as no From PO (nolock) where poano = YEAR('"+uf_gerais_getDate(po.rdata,"SQL")+"')")
			SELECT uCrsTempNo
			IF uCrsTempNo.no > PAGFORN.txtrno.value
				SELECT PO
				replace PO.RNO WITH uCrsTempNo.no
				
				SELECT PL
				replace ALL PL.rno WITH uCrsTempNo.no
				GO TOP
				
				PAGFORN.txtrno.refresh
			Endif
			fecha("uCrsTempNo")
		ELSE
			RETURN .f.
		ENDIF
		***************************************************
		
		LOCAL lcValidaInsereCab, lcValidaInsereLinhas
		STORE .f. TO lcValidaInsereCab, lcValidaInsereLinhas
		
		IF uf_gerais_actGrelha("","","BEGIN TRANSACTION")
			lcValidaInsereCab = uf_PAGFORN_insereCab()
			lcValidaInsereLinhas = uf_PAGFORN_insereLinhas('')
			IF lcValidaInsereCab AND lcValidaInsereLinhas
				uf_gerais_actgrelha("", "", [COMMIT TRANSACTION])
				** actualiza ultimo registo **
				SELECT PO
				uf_gerais_gravaUltRegisto('po', po.postamp)
				
				uf_fechoautomaticodoc_chama("PAGFORN")
				
				uf_perguntalt_chama("PAGAMENTO EFECTUADO COM SUCESSO.","OK","",64)

				** chama fornecedor inserido **
				SELECT PO
				uf_PAGFORN_chama(po.postamp)
			ELSE
				uf_gerais_actGrelha("","","ROLLBACK")
				RETURN .f.
			ENDIF 
		ELSE
			uf_perguntalt_chama("OCORREU UM ERRO A INSERIR O PAGAMENTO.","OK","", 16)
			RETURN .f.
		ENDIF
	ELSE
		LOCAL lcValidaUpdateCab, lcValidaUpdateLinhas
		STORE .f. TO lcValidaUpdateCab, lcValidaUpdateLinhas
		
		IF uf_gerais_actGrelha("","","BEGIN TRANSACTION")
			lcValidaUpdateCab = uf_PAGFORN_updateCab()
			lcValidaUpdateLinhas = uf_PAGFORN_updateLinhas()
			IF lcValidaUpdateCab AND lcValidaUpdateLinhas
				uf_gerais_actgrelha("", "", [COMMIT TRANSACTION])
				** actualiza ultimo registo **
				SELECT PO
				uf_gerais_gravaUltRegisto('po', po.postamp)
				
				uf_fechoautomaticodoc_chama("PAGFORN")
				
				uf_perguntalt_chama("PAGAMENTO ALTERADO COM SUCESSO.","OK","", 64)
				
				** chama fornecedor **
				SELECT PO
				uf_PAGFORN_chama(po.postamp)
			ELSE
				uf_gerais_actGrelha("","","ROLLBACK")
				RETURN .f.
			ENDIF 
		ELSE
			uf_perguntalt_chama("OCORREU UM ERRO AO GRAVAR O PAGAMENTO.","OK","", 16)
			RETURN .f.
		ENDIF
	ENDIF
	
	&& Actualiza Ecra
	myPagFornIntroducao = .f.
	myPagFornAlteracao = .f.
	uf_PAGFORN_confEcra()
	PAGFORN.refresh
ENDFUNC


**
FUNCTION uf_PAGFORN_insereCab
	lcSQL=''
	TEXT TO lcSQl TEXTMERGE NOSHOW
		INSERT INTO po
		(
			adoc, arred, ccusto
			,cm, cmdesc, cobranca, codpost, contado 
			,contado2, desc1, descba 
			,difcambio
			,dvalor, earred, edifcambio
			,efinv, eivav1, eivav2, eivav3, eivav4, eivav5
			,eivav6, eivav7, eivav8, eivav9, estab, etotal
			,etotol2, evirs, faztrf
			,fin, finv, finvmoeda, fref, impresso, intid
			,local, luserfin
			,memissao, moeda
			,moeda2, moeda3, morada, ncont, ncusto, nib, no, nome, olcodigo
			,ollocal, ollocal2, ousrdata, ousrhora, ousrinis, pais, plano, pno
			,pnome, poano, postamp, procdata, process, rdata, regiva, rno
			,site, tbok, telocal
			,telocal2, tipo, total, totalmoeda, totol2, txirs
			,usrdata, usrhora, usrinis
			,virs, zona, obs, numVFactR, VFactRStamp
		)
		Values
		(
			'<<PO.adoc>>', <<PO.arred>>, '<<PO.ccusto>>'
			,<<PO.cm>>, '<<PO.cmdesc>>', '<<PO.cobranca>>', '<<PO.codpost>>', <<PO.contado>>
			,<<PO.contado2>>, '<<PO.desc1>>', '<<PO.descba>>'
			,<<PO.difcambio>>
			,'<<uf_gerais_getDate(PO.dvalor,"SQL")>>', <<PO.earred>>, <<PO.edifcambio>>
			,<<PO.efinv>>, <<PO.eivav1>>, <<PO.eivav2>>, <<PO.eivav3>>, <<PO.eivav4>>, <<PO.eivav5>>
			,<<PO.eivav6>>, <<PO.eivav7>>, <<PO.eivav8>>, <<PO.eivav9>>, <<PO.estab>>, <<PO.etotal>>
			,<<PO.etotol2>>, <<PO.evirs>>, <<IIF(PO.faztrf,1,0)>>
			,<<PO.fin>>, <<PO.efinv * 200.482>>, <<PO.finvmoeda>>, '<<PO.fref>>', <<IIF(PO.impresso,1,0)>>, '<<PO.intid>>'
			,'<<PO.local>>', <<IIF(PO.luserfin,1,0)>>
			,'<<PO.memissao>>', '<<PO.moeda>>'
			,'<<PO.moeda2>>', '<<PO.moeda3>>', '<<PO.morada>>', '<<PO.ncont>>', '<<PO.ncusto>>', '<<PO.nib>>', <<PO.no>>, '<<PO.nome>>', '<<PO.olcodigo>>'
			,'<<PO.ollocal>>', '<<PO.ollocal2>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108), '<<m_chinis>>', <<PO.pais>>, <<IIF(PO.plano,1,0)>>, <<myTermNo>>
			,'<<myTerm>>', <<year(po.rdata)>>, '<<PO.postamp>>', '<<uf_gerais_getDate(PO.procdata,"SQL")>>', <<IIF(PO.process,1,0)>>, '<<uf_gerais_getDate(PO.rdata,"SQL")>>', <<IIF(PO.regiva,1,0)>>, <<PO.rno>>
			,'<<mySite>>', <<IIF(PO.tbok,1,0)>>, '<<PO.telocal>>'
			,'<<PO.telocal2>>', '<<PO.tipo>>', <<PO.etotal * 200.482>>, <<PO.totalmoeda>>, <<PO.etotol2 * 200.482>>, <<PO.txirs>>
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108), '<<m_chinis>>'
			,<<PO.virs>>, '<<PO.zona>>', '<<PO.OBS>>', '<<ALLTRIM(po.numVFactR)>>', '<<ALLTRIM(po.VFactRStamp)>>'
		)
	ENDTEXT

	IF !EMPTY(po.VFactRStamp)

		TEXT TO msel TEXTMERGE NOSHOW

			UPDATE 
				FC
			SET
				fc.credfm = fc.cred
				,fc.debfm = fc.deb
				,fc.ecredf = fc.ecred
				,fc.edebf = fc.edeb
			WHERE
				fc.fostamp = '<<ALLTRIM(po.VFactRStamp)>>'

		ENDTEXT

		lcSQL = ALLTRIM(lcSQL) + CHR(13) + ALLTRIM(msel)

	ENDIF


	IF !uf_gerais_actgrelha("", "", lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A INSERIR O PAGAMENTO.","OK","", 16)
		RETURN .f.
	ELSE
		RETURN .t.
	ENDIF
ENDFUNC


**
FUNCTION uf_PAGFORN_updateCab
	lcSQl=""
	TEXT TO lcSQL TEXTMERGE noshow
		UPDATE
			PO
		SET
			adoc = '<<PO.adoc>>'
			,arred = <<PO.arred>>
			,ccusto = '<<PO.ccusto>>'
			,cm = <<PO.cm>>
			,cmdesc = '<<PO.cmdesc>>'
			,cobranca = '<<PO.cobranca>>'
			,codpost = '<<PO.codpost>>'
			,contado  = <<PO.contado>>
			,contado2 = <<PO.contado2>>
			,desc1  = '<<PO.desc1>>'
			,descba  = '<<PO.descba>>'
			,difcambio = <<PO.difcambio>>
			,dvalor = '<<uf_gerais_getDate(PO.dvalor,"SQL")>>'
			,earred = <<PO.earred>>
			,edifcambio = <<PO.edifcambio>>
			,efinv = <<PO.efinv>>
			,eivav1 = <<PO.eivav1>>
			,eivav2 = <<PO.eivav2>>
			,eivav3 = <<PO.eivav3>>
			,eivav4 = <<PO.eivav4>>
			,eivav5 = <<PO.eivav5>>
			,eivav6 = <<PO.eivav6>>
			,eivav7 = <<PO.eivav7>>
			,eivav8 = <<PO.eivav8>>
			,eivav9 = <<PO.eivav9>>
			,estab = <<PO.estab>>
			,etotal = <<PO.etotal>>
			,etotol2 = <<PO.etotol2>>
			,evirs = <<PO.evirs>>
			,faztrf = <<IIF(PO.faztrf,1,0)>>
			,fin = <<PO.fin>>
			,finv = <<PO.efinv * 200.482>>
			,finvmoeda = <<PO.finvmoeda>>
			,fref = '<<PO.fref>>'
			,impresso = <<IIF(PO.impresso,1,0)>>
			,intid = '<<PO.intid>>'			
			,local = '<<PO.local>>'
			,luserfin = <<IIF(PO.luserfin,1,0)>>			
			,memissao = '<<PO.memissao>>'
			,moeda = '<<PO.moeda>>'
			,moeda2 = '<<PO.moeda2>>'
			,moeda3 = '<<PO.moeda3>>'
			,morada = '<<PO.morada>>'
			,ncont = '<<PO.ncont>>'
			,ncusto = '<<PO.ncusto>>'
			,nib = '<<PO.nib>>'
			,no = <<PO.no>>
			,nome = '<<PO.nome>>'
			,olcodigo = '<<PO.olcodigo>>'
			,ollocal = '<<PO.ollocal>>'
			,ollocal2 = '<<PO.ollocal2>>'
			,pais = <<PO.pais>>
			,plano = <<IIF(PO.plano,1,0)>>
			,pno = <<myTermNo>>
			,pnome = '<<myTerm>>'
			,poano = <<year(po.rdata)>>
			,postamp = '<<PO.postamp>>'
			,procdata = '<<uf_gerais_getDate(PO.procdata,"SQL")>>'
			,process = <<IIF(PO.process,1,0)>>
			,rdata = '<<uf_gerais_getDate(PO.rdata,"SQL")>>'
			,regiva = <<IIF(PO.regiva,1,0)>>
			,rno = <<PO.rno>>
			,site = '<<mySite>>'
			,tbok = <<IIF(PO.tbok,1,0)>>
			,telocal = '<<PO.telocal>>'
			,telocal2 = '<<PO.telocal2>>'
			,tipo = '<<PO.tipo>>'
			,total = <<PO.etotal * 200.482>>
			,totalmoeda = <<PO.totalmoeda>>
			,totol2 = <<PO.etotol2 * 200.482>>
			,txirs = <<PO.txirs>>
			,usrdata = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,usrhora = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
			,usrinis = '<<m_chinis>>'
			,virs = <<PO.virs>>
			,zona = '<<PO.zona>>'
			,obs = '<<PO.OBS>>'
		where
			po.postamp = '<<po.postamp>>'
	ENDTEXT

	IF !uf_gerais_actgrelha("", "", lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO AO GRAVAR O PAGAMENTO.","OK","", 16)
		RETURN .f.
	ELSE
		RETURN .t.
	ENDIF
ENDFUNC


**
FUNCTION uf_PAGFORN_insereLinhas
	LPARAMETERS lcPLstamp
	
	IF EMPTY(ALLTRIM(lcPLstamp)) 
		SELECT PL
		GO TOP
		SCAN
			IF !uf_gerais_actgrelha("", "", uf_PAGFORN_insereLinhasSQL())
				uf_perguntalt_chama("OCORREU UM ERRO A INSERIR O PAGAMENTO.","OK","", 16)
				RETURN .f.
			ENDIF
		ENDSCAN
	ELSE
		SELECT PL
		LOCATE FOR ALLTRIM(pl.plstamp) == ALLTRIM(lcPLstamp)
		IF FOUND()
			IF !uf_gerais_actgrelha("", "", uf_PAGFORN_insereLinhasSQL())
				uf_perguntalt_chama("OCORREU UM ERRO A EDITAR O PAGAMENTO.","OK","", 16)
				RETURN .f.
			ENDIF
		ELSE
			RETURN .f.
		ENDIF 	
	ENDIF
	
	RETURN .t.
ENDFUNC 


&&<<IIF(pl.erec < 0,PL.erec*200.482*-1,PL.erec*200.482)>> -  para ser possivel integrar na contabilidade pagamentos negativos incluidos manualmente
FUNCTION uf_PAGFORN_insereLinhasSQL
	lcSQL=''
	TEXT TO lcSQl TEXTMERGE NOSHOW
		INSERT INTO PL
		(
			adoc, arred, cambio, cdesc, cm, crend, datalc, dataven, desconto
			,earred, ecambio, eivav1, eivav2, eivav3, eivav4, eivav5, eivav6, eivav7, eivav8
			,eivav9, enaval, erec, escrec, eval, evirs, evori, fcorigem
			,fcstamp, ivatx1, ivatx2, ivatx3, ivatx4, ivatx5, ivatx6, ivatx7, ivatx8, ivatx9
			,lordem, moeda, moedoc
			,ousrdata, ousrhora, ousrinis, plstamp, postamp, process, rdata, rec, rno,usrdata
			,usrhora, usrinis, virs
		)
		Values
		(
			'<<PL.adoc>>', <<PL.arred>>, <<PL.cambio>>, '<<PL.cdesc>>', <<PL.cm>>, '<<PL.crend>>', '<<uf_gerais_getDate(PL.datalc,"SQL")>>', '<<uf_gerais_getDate(PL.dataven,"SQL")>>', <<PL.desconto>>
			,<<PL.earred>>, <<PL.ecambio>>, <<PL.eivav1>>, <<PL.eivav2>>, <<PL.eivav3>>, <<PL.eivav4>>, <<PL.eivav5>>, <<PL.eivav6>>, <<PL.eivav7>>, <<PL.eivav8>>
			,<<PL.eivav9>>, <<PL.enaval>>, <<PL.erec>>, ROUND(<<IIF(pl.erec < 0,PL.erec*200.482*-1,PL.erec*200.482)>>,2), <<PL.eval>>, <<PL.evirs>>, <<PL.evori>>, '<<PL.fcorigem>>'
			,'<<PL.fcstamp>>', <<PL.ivatx1>>, <<PL.ivatx2>>, <<PL.ivatx3>>, <<PL.ivatx4>>, <<PL.ivatx5>>, <<PL.ivatx6>>, <<PL.ivatx7>>, <<PL.ivatx8>>, <<PL.ivatx9>>
			,<<PL.lordem>>, '<<PL.moeda>>', '<<PL.moedoc>>'
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108), '<<m_chinis>>', '<<PL.plstamp>>', '<<PL.postamp>>', <<IIF(PL.process,1,0)>>, '<<uf_gerais_getDate(PL.rdata,"SQL")>>', <<PL.rec>>, <<PL.rno>>, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108), '<<m_chinis>>', <<PL.virs>>
		)
	ENDTEXT
	
	RETURN lcSQL
ENDFUNC


**
FUNCTION uf_PAGFORN_updateLinhas
	SELECT PO
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW
		SELECT 
			plstamp
		from 
			pl (nolock)
		where
			pl.postamp = '<<ALLTRIM(PO.postamp)>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("","uCrsPLstamp",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A EDITAR O PAGAMENTO.","OK","", 16)
		RETURN .f.
	ENDIF 

	&&apagar as que j� n�o existem
	LOCAL lcStampApagar
	STORE '' TO lcStampApagar
	SELECT uCrsPLstamp
	GO TOP
	SCAN
		SELECT PL
		LOCATE FOR ALLTRIM(PL.plstamp) == ALLTRIM(uCrsPLstamp.plstamp)
		IF !FOUND()
			IF empty(ALLTRIM(lcStampApagar))
				lcStampApagar = "'" + ALLTRIM(uCrsPLstamp.plstamp) + "'"
			ELSE
				lcStampApagar = lcStampApagar + ",'" + ALLTRIM(uCrsPLstamp.plstamp) + "'"
			ENDIF
			
		ENDIF 
		
		SELECT uCrsPLstamp
	ENDSCAN
	
	IF !EMPTY(ALLTRIM(lcStampApagar))
		SELECT PO
		lcSQL = ''
		TEXT TO lcSQL TEXTMERGE NOSHOW
			DELETE FROM PL
			where
				plstamp in (<<lcStampApagar>>)
				and postamp = '<<ALLTRIM(po.postamp)>>'
		ENDTEXT
		IF !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A EDITAR O PAGAMENTO.","OK","", 16)
			RETURN .f.
		ENDIF
	ENDIF
	
	&&editar as que j� existem
	SELECT uCrsPLstamp
	GO TOP
	SCAN
		SELECT PL
		LOCATE FOR ALLTRIM(PL.plstamp) == ALLTRIM(uCrsPLstamp.plstamp)
		IF FOUND()
			lcSQL = ''
			TEXT TO lcSQL TEXTMERGE NOSHOW
				UPDATE 
					PL
				SET
					adoc = '<<PL.adoc>>'
					,arred = <<PL.arred>>
					,cambio = <<PL.cambio>>
					,cdesc = '<<PL.cdesc>>'
					,cm = <<PL.cm>>
					,crend = '<<PL.crend>>'
					,datalc = '<<uf_gerais_getDate(PL.datalc,"SQL")>>'
					,dataven = '<<uf_gerais_getDate(PL.dataven,"SQL")>>'
					,desconto = <<PL.desconto>>
					,earred = <<PL.earred>>
					,ecambio = <<PL.ecambio>>
					,eivav1 = <<PL.eivav1>>
					,eivav2 = <<PL.eivav2>>
					,eivav3 = <<PL.eivav3>>
					,eivav4 = <<PL.eivav4>>
					,eivav5 = <<PL.eivav5>>
					,eivav6 = <<PL.eivav6>>
					,eivav7 = <<PL.eivav7>>
					,eivav8 = <<PL.eivav8>>
					,eivav9 = <<PL.eivav9>>
					,enaval = <<PL.enaval>>
					,erec = <<PL.erec>>
					,escrec = <<PL.escrec>>
					,eval = <<PL.eval>>
					,evirs = <<PL.evirs>>
					,evori = <<PL.evori>>
					,fcorigem = '<<PL.fcorigem>>'
					,fcstamp = '<<PL.fcstamp>>'
					,ivatx1 = <<PL.ivatx1>>
					,ivatx2 = <<PL.ivatx2>>
					,ivatx3 = <<PL.ivatx3>>
					,ivatx4 = <<PL.ivatx4>>
					,ivatx5 = <<PL.ivatx5>>
					,ivatx6 = <<PL.ivatx6>>
					,ivatx7 = <<PL.ivatx7>>
					,ivatx8 = <<PL.ivatx8>>
					,ivatx9 = <<PL.ivatx9>>
					,lordem = <<PL.lordem>>
					,moeda = '<<PL.moeda>>'
					,moedoc = '<<PL.moedoc>>'
					,plstamp = '<<PL.plstamp>>'
					,postamp = '<<PL.postamp>>'
					,process = <<IIF(PL.process,1,0)>>
					,rdata = '<<uf_gerais_getDate(PL.rdata,"SQL")>>'
					,rec = <<PL.rec>>
					,rno = <<PL.rno>>
					,usrdata = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,usrhora = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
					,usrinis = '<<m_chinis>>'
					,virs = <<PL.virs>>
				WHERE
					pl.plstamp = '<<pl.plstamp>>'
			ENDTEXT 
			IF !uf_gerais_actGrelha("","",lcSQL)
				uf_perguntalt_chama("OCORREU UM ERRO A EDITAR O PAGAMENTO.","OK","", 16)
				RETURN .f.
			ENDIF 
		ENDIF 
		
		SELECT uCrsPLstamp
	ENDSCAN
	
	&&adicionar as novas
	SELECT PL
	GO TOP
	SCAN
		SELECT uCrsPLstamp
		LOCATE FOR ALLTRIM(uCrsPLstamp.plstamp) == ALLTRIM(PL.plstamp)
		IF !FOUND()
			SELECT PL
			IF !uf_PAGFORN_insereLinhas(ALLTRIM(pl.plstamp))
				RETURN .f.
			ENDIF
		ENDIF
		SELECT PL
	ENDSCAN
	
	fecha("uCrsPLstamp")
	
	RETURN .t.
ENDFUNC 


** ELIMINAR FORNECEDOR
FUNCTION uf_PAGFORN_eliminar

	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Pagamentos a fornecedores - Eliminar')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ELIMINAR PAGAMENTOS.","OK","",48)
		RETURN .f.
	ENDIF	
	
	&&Valida��es
	IF USED("PO")
		SELECT PO
		IF !EMPTY(po.postamp)
			LOCAL lcProcess
			STORE .f. TO lcProcess
			
			SELECT PO
			IF PO.process
				IF uf_perguntalt_chama("O Pagamento j� se encontra processado."+CHR(13)+CHR(13)+"Pretende desprocessar o pagamento?","Sim","N�o")				
					lcSQL = ''
					TEXT TO lcSQL TEXTMERGE NOSHOW
						UPDATE
							PL
						SET
							process = 0
						where
							postamp = '<<PO.postamp>>'
							
							
						UPDATE 
							PO 
						SET 
							process = 0
						where 
							postamp = '<<PO.postamp>>'
					ENDTEXT
					IF uf_gerais_actgrelha("", "", lcSQL)
						uf_perguntalt_chama("PAGAMENTO DESPROCESSADO COM SUCESSO!","OK","",64)
						lcProcess = .t.
					ELSE
						uf_perguntalt_chama("OCORREU UM ERRO AO DESPROCESSAR O PAGAMENTO!","OK","",16)
						RETURN .f.
					ENDIF
				ELSE
					RETURN .f.
				ENDIF
			ENDIF
			&& Passou valida��es - vai apagar
			IF uf_perguntalt_chama("Tem a certeza que deseja apagar o Pagamento?"+CHR(13)+CHR(13)+"Vai perder todos os dados.","Sim","N�o")
				SELECT PO
				lcSQL = ''
				TEXT TO lcSQL TEXTMERGE NOSHOW 
					delete from po 
					where 
						postamp='<<po.postamp>>'
						
					delete from pl
					where 
						postamp='<<po.postamp>>'
				ENDTEXT 

				IF !EMPTY(po.VFactRStamp)

					TEXT TO msel TEXTMERGE NOSHOW

						UPDATE
							FC
						SET
							fc.credfm = 0
							,fc.debfm = 0
							,fc.ecredf = 0
							,fc.edebf = 0
						WHERE
							fc.fostamp = '<<ALLTRIM(po.VFactRStamp)>>'

					ENDTEXT

					lcsql = ALLTRIM(lcSQL) + CHR(13) + ALLTRIM(msel)

				ENDIF

				IF uf_gerais_actgrelha("", "", lcSQL)

					uf_perguntalt_chama("PAGAMENTO APAGADO COM SUCESSO!","OK","",64)

					&& navega para ultimo registo
					uf_PAGFORN_anterior()
				ELSE
					uf_perguntalt_chama("OCORREU UM ERRO AO APAGAR O PAGAMENTO!","OK","",16)
				ENDIF
			ELSE
				IF lcProcess
					SELECT PO
					uf_pagforn_chama(po.postamp)
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
ENDFUNC



**
FUNCTION uf_PAGFORN_navegaUltRegisto
	LOCAL lcPOStamp
	lcPOStamp = uf_gerais_devolveUltRegisto('PO')
							
	IF uf_gerais_actgrelha("", "ucrsUltRegFlTemp", [select top 1 postamp from po (nolock) where postamp='] + ALLTRIM(lcPOStamp) + ['])
		IF RECCOUNT("ucrsUltRegFlTemp") > 0
			uf_PAGFORN_chama(ucrsUltRegFlTemp.postamp)
		ELSE
			uf_PAGFORN_chama('')
		ENDIF
		fecha("ucrsUltRegFlTemp")
	ENDIF
ENDFUNC



** Sair do ecra de fornecedores *
FUNCTION uf_PAGFORN_sair
		
	IF myPagFornIntroducao OR myPagFornAlteracao
		IF uf_perguntalt_chama("Deseja cancelar as altera��es?"+CHR(13)+CHR(13)+"Ir� perder as �ltimas altera��es feitas.","Sim","N�o")

			IF myPagFornIntroducao && Introduc��o
				uf_PAGFORN_navegaUltRegisto()
			ELSE && Altera��o
				SELECT PO
				uf_PAGFORN_chama(po.postamp)
			ENDIF

			&&Actualizar Painel	
			myPagFornIntroducao = .f.
			myPagFornAlteracao = .f.
			uf_PAGFORN_confEcra()

			PAGFORN.Refresh
		ENDIF	
	ELSE
		&& Fecha cursores
		Fecha("PO")
		fecha("PL")
			
		PAGFORN.hide
		PAGFORN.Release
	ENDIF
ENDFUNC



** NAVEGA REGISTO ANTERIOR
FUNCTION uf_PAGFORN_anterior
	
	SELECT PO
	lcSQL = ""
		
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		SELECT TOP 1
			postamp
		FROM
			po (nolock)
		WHERE
			rno < <<IIF(EMPTY(po.rno),0,po.rno)>>
			and poano = <<po.poano>>
		order by
			rno desc
	ENDTEXT  
	If !uf_gerais_actgrelha("", "uCrstempFL", lcSql)
		RETURN .f.
	ENDIF
	
	uf_PAGFORN_chama(uCrstempFL.postamp)
	
	fecha("uCrstempFL")
ENDFUNC 	



** NAVEGA PROXIMO REGISTO
FUNCTION uf_PAGFORN_seguinte
	
	SELECT po
	lcSQL=""
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		SELECT TOP 1
			postamp
		FROM
			po (nolock)
		WHERE
			rno > <<IIF(EMPTY(po.rno),0,po.rno)>>
			and poano = <<po.poano>>
		order by
			rno asc
	ENDTEXT  
	If !uf_gerais_actgrelha("", "uCrstempFL", lcSql)
		RETURN .f.
	ENDIF
	
	uf_PAGFORN_chama(uCrstempFL.postamp)
	
	Fecha("uCrstempFL")
ENDFUNC


** VALORES POR DEFEITO DOS FORNECEDORES
FUNCTION uf_PAGFORN_ValoresDefeito
	
	
	LOCAL lcStamp
	lcStamp = uf_gerais_stamp()
	
	&& Stamp	
	SELECT PO
	replace po.postamp WITH lcStamp
	replace po.rdata WITH datetime()+(difhoraria*3600)
	**replace po.rdata WITH DATE()

	&& No de fornecedor
	uf_gerais_actgrelha("", "uCrsTmp", "Select Isnull(Max(RNO),0) + 1 as no From PO (nolock) where poano = YEAR('"+uf_gerais_getDate(po.rdata,"SQL")+"')")
	SELECT uCrsTmp
	SELECT PO
	replace po.rno WITH uCrsTmp.no
	fecha("uCrsTmp")
		
	&& MOEDA
	LOCAL lcMoedaDefault
	STORE 'EURO' TO lcMoedaDefault
	
	lcMoedaDefault = uf_gerais_getParameter("ADM0000000260","text")
	
	&& Se parametro n�o estiver preenchido assume � por default
	IF EMPTY(lcMoedaDefault)
		SELECT PO
		replace po.moeda WITH 'EURO'
		replace po.moeda2 WITH 'EURO'
		replace po.moeda3 WITH 'EURO'
		replace po.memissao WITH 'EURO'
	ELSE
		SELECT PO
		replace po.moeda WITH lcMoedaDefault 
		replace po.moeda2 WITH lcMoedaDefault 
		replace po.moeda3 WITH lcMoedaDefault 
		replace po.memissao WITH lcMoedaDefault 
	ENDIF 
	
	&& Dados de Cria��o
	SELECT po
	replace po.ousrdata WITH datetime()+(difhoraria*3600)
	**replace po.ousrdata WITH date()
	replace po.ousrinis WITH m_chinis
	
	SELECT PO
	replace po.cmdesc WITH 'N/Cheque'
	replace po.cm WITH 6
	replace po.olcodigo WITH 'P10001'
	replace po.ollocal WITH 'Conta Geral Pagamento'
	replace po.contado WITH 98
	replace po.telocal WITH 'C'
	replace po.site WITH mySite
	replace po.luserfin WITH .t.
	replace po.procdata WITH datetime()+(difhoraria*3600)
	replace po.dvalor WITH datetime()+(difhoraria*3600)
**	replace po.procdata WITH DATE()
**	replace po.dvalor WITH DATE()
	replace po.regiva WITH .t.
			
	PAGFORN.refresh
ENDFUNC


**
FUNCTION uf_PAGFORN_refrescar
	SELECT PO
	uf_PAGFORN_chama(po.postamp)
ENDFUNC



FUNCTION uf_pagforn_adicionaLinha
	LOCAL lcStamp, lcOrdem
	lcStamp = uf_gerais_stamp()
	
	SELECT PL
	IF RECCOUNT("PL") > 0
		CALCULATE MAX(PL.lordem) TO lcOrdem
		
		lcOrdem = lcOrdem + 100
	ELSE
		lcOrdem = 100
	ENDIF 
	
	SELECT PO
	
	SELECT PL
	GO BOTTOM
	APPEND BLANK
	SELECT PL
	replace ;
		PL.plstamp 	WITH lcStamp, ;
		PL.rno		WITH PO.rno, ;
		PL.process	WITH .f., ;
		PL.moeda	WITH PO.moeda, ;
		PL.rdata	WITH PO.rdata, ;
		PL.postamp	WITH PO.postamp, ;
		PL.lordem	WITH lcOrdem
		
	*PAGFORN.pageframe1.page1.grdPL.setfocus()
ENDFUNC


FUNCTION uf_pagforn_apagaLinha

	SELECT PO

	IF EMPTY(po.numVFactR)

		SELECT PL
		DELETE
	
		SELECT PL
		TRY
			SKIP -1
		CATCH
			TRY
				SKIP 1
			CATCH
				GO TOP
			ENDTRY
		ENDTRY
	
		&&calcular totais
		uf_pagforn_calTotais()
	
		PAGFORN.pageframe1.page1.grdPL.setfocus()

	ELSE

		IF uf_perguntalt_chama("Este documento est� associado a uma V\Factura Resumo." + chr(13) + "Ao eliminar esta linha ter� de eliminar todas as linhas da mesma." + chr(13) + "Tem a certeza que pretende continuar?","Sim","N�o",48)

			SELECT PL 

			DELETE FOR 1=1

			SELECT PO
			REPLACE po.numVFactR WITH '', po.VFactRStamp WITH ''

			PAGFORN.refresh()

			uf_pagforn_calTotais()

			PAGFORN.pageframe1.page1.grdPL.setfocus()

		ENDIF
	ENDIF

ENDFUNC


**
FUNCTION uf_pagforn_calTotais
	LOCAL lcTotal, lcPos, lcTotalDescontoEuro, lcTotalSemDesconto, lcValorLinha
	STORE 0 TO lcTotal, lcTotalDescontoEuro, lcTotalSemDesconto, lcValorLinha
	SELECT PL
	lcPos = RECNO("PL")
	

	SELECT PL
	GO TOP
	SCAN
		SELECT PO
		IF PO.regiva == .t.
			lcValorLinha = PL.erec - PL.eivav1 - PL.eivav2 - PL.eivav3 - PL.eivav4 - PL.eivav5 - PL.eivav6 - PL.eivav7 - PL.eivav8 - PL.eivav9
		ELSE
			lcValorLinha = PL.erec
		ENDIF 

		lcTotal = lcTotal + (PL.erec - (lcValorLinha * (PL.desconto / 100)))
		lcTotalDescontoEuro = lcTotalDescontoEuro + lcValorLinha * (PL.desconto / 100)

		lcTotalSemDesconto = lcTotalSemDesconto + PL.erec
		
		SELECT PL
		IF PL.desconto > 0
			SELECT PO
			replace PO.luserfin WITH .f.
		ENDIF 
	ENDSCAN
	
	
	SELECT po
	replace po.etotal WITH lcTotal
	replace po.efinv WITH lcTotalDescontoEuro
	
	IF po.efinv == 0
		replace po.fin WITH 0
	ELSE 
		replace po.fin WITH po.efinv / lcTotalSemDesconto * 100
	ENDIF
	
	PAGFORN.pageframe1.page1.txtEtotal.refresh
	PAGFORN.pageframe1.page1.txtEfinv.refresh
	PAGFORN.pageframe1.page1.txtfin.refresh

	SELECT PO
	IF !EMPTY(ALLTRIM(PO.ollocal2))
		PAGFORN.pageframe1.page1.txtEtotol1.value = lcTotal - PO.etotol2
	ELSE
		PAGFORN.pageframe1.page1.txtEtotol1.value = lcTotal
	ENDIF
	
	&&valores de IVA na PO
	LOCAL lcEIVAV1, lcEIVAV2, lcEIVAV3, lcEIVAV4, lcEIVAV5, lcEIVAV6, lcEIVAV7, lcEIVAV8, lcEIVAV9
	STORE 0 TO lcEIVAV1, lcEIVAV2, lcEIVAV3, lcEIVAV4, lcEIVAV5, lcEIVAV6, lcEIVAV7, lcEIVAV8, lcEIVAV9
	
	SELECT PL
	CALCULATE SUM(eivav1) TO lcEIVAV1
	CALCULATE SUM(eivav2) TO lcEIVAV2
	CALCULATE SUM(eivav3) TO lcEIVAV3
	CALCULATE SUM(eivav4) TO lcEIVAV4
	CALCULATE SUM(eivav5) TO lcEIVAV5
	CALCULATE SUM(eivav6) TO lcEIVAV6
	CALCULATE SUM(eivav7) TO lcEIVAV7
	CALCULATE SUM(eivav8) TO lcEIVAV8
	CALCULATE SUM(eivav9) TO lcEIVAV9
	
	SELECT PO
	GO TOP
	replace ;
		po.eivav1 WITH lcEIVAV1, ;
		po.eivav2 WITH lcEIVAV2, ;
		po.eivav3 WITH lcEIVAV3, ;
		po.eivav4 WITH lcEIVAV4, ;
		po.eivav5 WITH lcEIVAV5, ;
		po.eivav6 WITH lcEIVAV6, ;
		po.eivav7 WITH lcEIVAV7, ;
		po.eivav8 WITH lcEIVAV8, ;
		po.eivav9 WITH lcEIVAV9
	
	SELECT PL
	TRY
		GO lcPos
	CATCH
	ENDTRY
ENDFUNC


**
FUNCTION uf_pagforn_calDescFinPerc
	LOCAL lcTotal, lcTotalDescontoEuro, lcTotalSemDesconto,lcValorLinha
	STORE 0 TO lcTotal, lcTotalDescontoEuro, lcTotalSemDesconto, lcValorLinha


	SELECT PL
	GO TOP
	SCAN
		replace pl.desconto WITH 0
		
		SELECT PO
		IF PO.regiva == .t.
			lcValorLinha = PL.erec - PL.eivav1 - PL.eivav2 - PL.eivav3 - PL.eivav4 - PL.eivav5 - PL.eivav6 - PL.eivav7 - PL.eivav8 - PL.eivav9
		ELSE
			lcValorLinha = PL.erec
		ENDIF 
		IF pagforn.Desctotal == 0
			lcTotal = lcTotal + (PL.erec - (lcValorLinha * (PL.desconto / 100)))
			lcTotalDescontoEuro = lcTotalDescontoEuro + lcValorLinha * (PL.desconto / 100)
		ELSE
			lcTotal = lcTotal + lcValorLinha
		ENDIF
		lcTotalSemDesconto = lcTotalSemDesconto + PL.erec 
		
		SELECT PL
		IF PL.desconto > 0
			SELECT PO
			replace PO.luserfin WITH .f.
		ENDIF 
	ENDSCAN

	Select PO
	IF po.fin == 0
		replace po.efinv WITH 0
	ELSE 
		replace po.efinv WITH lcTotal * (po.fin / 100)
	ENDIF 

	PAGFORN.pageframe1.page1.txtEfinv.refresh
	PAGFORN.pageframe1.page1.txtfin.refresh
	
	SELECT PO
	lcTotal = lcTotal - po.efinv
	
	SELECT po
	replace po.etotal WITH lcTotal

	SELECT PO
	IF !EMPTY(ALLTRIM(PO.ollocal2))
		PAGFORN.pageframe1.page1.txtEtotol1.value = lcTotal - PO.etotol2
	ELSE
		PAGFORN.pageframe1.page1.txtEtotol1.value = lcTotal
	ENDIF

	
	PAGFORN.pageframe1.page1.txtEtotol1.refresh
	PAGFORN.pageframe1.page1.txtEtotal.refresh

ENDFUNC


**
FUNCTION uf_pagforn_calDescFinValor
	LOCAL lcTotal, lcTotalDescontoEuro, lcTotalSemDesconto,lcValorLinha, lcCaixa1, lcCaixa2
	STORE 0 TO lcTotal, lcTotalDescontoEuro, lcTotalSemDesconto, lcValorLinha, lcCaixa1, lcCaixa2

	SELECT PL
	GO TOP
	SCAN
	
		replace pl.desconto WITH 0
		
		SELECT PO
		IF PO.regiva == .t.
			lcValorLinha = PL.erec - PL.eivav1 - PL.eivav2 - PL.eivav3 - PL.eivav4 - PL.eivav5 - PL.eivav6 - PL.eivav7 - PL.eivav8 - PL.eivav9
		ELSE
			lcValorLinha = PL.erec
		ENDIF 
		IF pagforn.Desctotal == 0
			lcTotal = lcTotal + (PL.erec - (lcValorLinha * (PL.desconto / 100)))
			lcTotalDescontoEuro = lcTotalDescontoEuro + lcValorLinha * (PL.desconto / 100)
		ELSE
			lcTotal = lcTotal + lcValorLinha
		ENDIF
		lcTotalSemDesconto = lcTotalSemDesconto + PL.erec
		
		SELECT PL
		IF PL.desconto > 0
			SELECT PO
			replace PO.luserfin WITH .f.
		ENDIF 
	ENDSCAN

	Select PO
	IF po.efinv == 0
		replace po.fin WITH 0
	ELSE 
		replace po.fin WITH po.efinv / lcTotal * 100
	ENDIF
	
	PAGFORN.pageframe1.page1.txtEfinv.refresh
	PAGFORN.pageframe1.page1.txtfin.refresh
	
	SELECT PO
	lcTotal = lcTotal - po.efinv
	
	SELECT po
	replace po.etotal WITH lcTotal
	
	
	SELECT PO
	IF !EMPTY(ALLTRIM(PO.ollocal2))
		PAGFORN.pageframe1.page1.txtEtotol1.value = lcTotal - PO.etotol2
	ELSE
		PAGFORN.pageframe1.page1.txtEtotol1.value = lcTotal
	ENDIF

	
	PAGFORN.pageframe1.page1.txtEtotol1.refresh	
	PAGFORN.pageframe1.page1.txtEtotal.refresh
	
ENDFUNC


**
FUNCTION uf_pagforn_calOlLocal2

	SELECT PO
	IF !EMPTY(ALLTRIM(PO.ollocal2))
		PAGFORN.pageframe1.page1.txtEtotol1.value = po.etotal - PO.etotol2
	ELSE
		PAGFORN.pageframe1.page1.txtEtotol1.value = po.etotal
	ENDIF
	
	PAGFORN.pageframe1.page1.txtEtotol1.refresh
ENDFUNC



*
FUNCTION uf_pagforn_importResumo
	SELECT PO

	IF !EMPTY(po.numVFactR)

		uf_perguntalt_chama("N�o pode importar mais do que uma V\Factura Resumo por pagamento!","OK","",64)

	ELSE

		IF EMPTY(PO.no)
			uf_perguntalt_chama("Por favor selecione um fornecedor.","OK","",64)
		ELSE	
			uf_pesqDocumentos_Chama("VFATURARESUMO", ALLTRIM(PO.nome))
		ENDIF

	ENDIF

ENDFUNC 


**
FUNCTION uf_pagForn_importaMovimentosResumo


	SELECT ucrsPesqDocs
	GO Top
	SCAN FOR !EMPTY(ucrsPesqDocs.sel)

		SELECT PL
		DELETE FROM PL WHERE EMPTY(pl.cdesc) AND EMPTY(pl.adoc) AND EMPTY(pl.eval) AND EMPTY(pl.erec) AND EMPTY(pl.desconto)

		uv_numVFactR = uf_gerais_getUmValor("fo", "adoc", "fostamp = '" + ALLTRIM(ucrsPesqDocs.cabStamp) + "'")
		

		SELECT PO 
		REPLACE po.numVFactR WITH uv_numVFactR, po.VFactRStamp WITH ucrsPesqDocs.cabStamp
	
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			Select distinct fostamp from fn (nolock) where fnstamp in (Select ofnstamp from fn (nolock) where fostamp = '<<ucrsPesqDocs.cabStamp>>')
		ENDTEXT 
		uf_gerais_actGrelha("", "uCrsPagDocumentosDoResumo", lcSQL)


		SELECT uCrsPagDocumentosDoResumo
		GO TOP
		SCAN 
		
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_fornecedores_pagpendentesfornResumo '<<uCrsPagDocumentosDoResumo.fostamp>>'
			ENDTEXT

			
			uf_gerais_actGrelha("", "uCrsPagPendentesFornResumo", lcSQL)

			IF RECCOUNT("uCrsPagPendentesFornResumo")>0
				uf_pagforn_adicionaLinha()
				SELECT PL
				GO BOTTOM
				replace pl.cdesc 	WITH uCrsPagPendentesFornResumo.cmdesc
				replace pl.adoc 	WITH uCrsPagPendentesFornResumo.adoc
				replace pl.datalc	WITH uCrsPagPendentesFornResumo.datalc
				replace pl.dataven	WITH uCrsPagPendentesFornResumo.dataven
				replace pl.fcstamp	WITH uCrsPagPendentesFornResumo.fcstamp
				replace pl.cm		WITH uCrsPagPendentesFornResumo.cm
				replace pl.eval		WITH uCrsPagPendentesFornResumo.valorNreg
				replace pl.erec 	WITH uCrsPagPendentesFornResumo.regularizar
				replace pl.escrec	WITH uCrsPagPendentesFornResumo.regularizar * 200.482
				
				SELECT PL
				&&valores do IVA sobre o valor regularizado
				replace pl.eivav1	WITH uCrsPagPendentesFornResumo.eivav1 
				replace pl.eivav2	WITH uCrsPagPendentesFornResumo.eivav2 
				replace pl.eivav3	WITH uCrsPagPendentesFornResumo.eivav3 
				replace pl.eivav4	WITH uCrsPagPendentesFornResumo.eivav4 
				replace pl.eivav5	WITH uCrsPagPendentesFornResumo.eivav5 
				replace pl.eivav6	WITH uCrsPagPendentesFornResumo.eivav6 
				replace pl.eivav7	WITH uCrsPagPendentesFornResumo.eivav7 
				replace pl.eivav8	WITH uCrsPagPendentesFornResumo.eivav8 
				replace pl.eivav9	WITH uCrsPagPendentesFornResumo.eivav9 
				replace pl.enaval	WITH uCrsPagPendentesFornResumo.valorNreg
				replace pl.evori	WITH uCrsPagPendentesFornResumo.valorNreg
				replace pl.moedoc	WITH uCrsPagPendentesFornResumo.moeda
				
				&&taxas de IVA
				replace pl.ivatx1	WITH uCrsPagPendentesFornResumo.ivatx1
				replace pl.ivatx2	WITH uCrsPagPendentesFornResumo.ivatx2
				replace pl.ivatx3	WITH uCrsPagPendentesFornResumo.ivatx3
				replace pl.ivatx4	WITH uCrsPagPendentesFornResumo.ivatx4
				replace pl.ivatx5	WITH uCrsPagPendentesFornResumo.ivatx5
				replace pl.ivatx6	WITH uCrsPagPendentesFornResumo.ivatx6
				replace pl.ivatx7	WITH uCrsPagPendentesFornResumo.ivatx7
				replace pl.ivatx8	WITH uCrsPagPendentesFornResumo.ivatx8
				replace pl.ivatx9	WITH uCrsPagPendentesFornResumo.ivatx9
				replace pl.fcorigem	WITH uCrsPagPendentesFornResumo.origem
				replace pl.numVFactR WITH uv_numVFactR
			ENDIF 
		ENDSCAN

        uf_pagforn_calTotais()

        PAGFORN.PAGEFRAME1.PAGE1.txtFin.value = uf_gerais_getUmValor("FO", "FIN", "FOSTAMP = '" + ucrsPesqDocs.cabStamp + "'")
        uf_pagforn_calDescFinPerc()

	ENDSCAN 

	SELECT PL
	GO TOP

	uf_pesqDocumentos_sair()
ENDFUNC 