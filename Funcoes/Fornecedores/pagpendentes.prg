**
FUNCTION uf_PAGPENDENTES_chama
	LOCAL lcSQL
	lcSql = ''
	
	uf_PAGPENDENTES_CarregaDados()
	
	IF TYPE('PAGPENDENTES')=="U"
		DO FORM PAGPENDENTES
	ELSE
		PAGPENDENTES.show
	ENDIF
ENDFUNC


**
** tcBool	: caso venha a .t. cria apenas o cursor
**
FUNCTION uf_PAGPENDENTES_CarregaDados
	LOCAL lcSQL
	lcSql = ''
	
	regua(0, 2, "A processar a Pesquisa...")
	regua(1, 1, "A processar a Pesquisa...")
	
	SELECT PO
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_fornecedores_pagpendentesforn <<PO.no>>, <<PO.estab>>
	ENDTEXT

	uf_gerais_actGrelha("", "uCrsPagPendentesForn", lcSQL)
	
	SELECT PL
	GO TOP
	SCAN
		SELECT uCrsPagPendentesForn
		LOCATE FOR ALLTRIM(uCrsPagPendentesForn.fcstamp) == ALLTRIM(PL.fcstamp)
		IF FOUND()
			SELECT uCrsPagPendentesForn
			replace uCrsPagPendentesForn.sel WITH .t.
			replace uCrsPagPendentesForn.jaincluido WITH .t.
			replace uCrsPagPendentesForn.regularizar WITH pl.erec
		ENDIF
		
		SELECT PL
	ENDSCAN
	SELECT PL
	GO TOP
	
	SELECT uCrsPagPendentesForn
	GO TOP
	
	regua(1, 2, "A processar a Pesquisa...")
	
	regua(2)
ENDFUNC


**
FUNCTION uf_PAGPENDENTES_Sel
	SELECT uCrsPagPendentesForn
	IF uCrsPagPendentesForn.jaincluido
		uf_perguntalt_chama("Este documento j� foi seleccionado para pagamento." + CHR(13) + CHR(13) + "Por favor verifique.","OK","",64)
		RETURN .f.
	ELSE
		IF uCrsPagPendentesForn.inclPag > 0 AND uCrsPagPendentesForn.sel
			IF !uf_perguntalt_chama("Este documento j� foi inclu�do noutro pagamento. Valor: " + astr(uCrsPagPendentesForn.inclPag,8,2) + CHR(13) + CHR(13) + "Pretende voltar a incluir?","Sim","N�o",64)
				RETURN .f.
			ENDIF
		ENDIF 
		SELECT uCrsPagPendentesForn
		IF ABS(uCrsPagPendentesForn.regularizar) > ABS(uCrsPagPendentesForn.valorNreg)
			SELECT uCrsPagPendentesForn
			replace uCrsPagPendentesForn.regularizar WITH uCrsPagPendentesForn.valorNreg
			uf_perguntalt_chama("N�o pode regularizar um valor superior ao valor n�o regularizado.","OK","",64)
		ENDIF
		uf_PAGPENDENTES_calTotalSel()
	ENDIF
ENDFUNC

FUNCTION uf_PAGPENDENTES_calTotalSel
	LOCAL lcPos, lcTotal
	STORE 0 TO lcTotal
	
	SELECT uCrsPagPendentesForn
	lcPos = RECNO("uCrsPagPendentesForn")
	
	SELECT uCrsPagPendentesForn
	SCAN FOR uCrsPagPendentesForn.sel == .t.
		lcTotal = lcTotal + uCrsPagPendentesForn.regularizar
	ENDSCAN
	
	PAGPENDENTES.txtTotalSel.value = lcTotal
	
	SELECT uCrsPagPendentesForn
	TRY
		GO lcPos
	CATCH
	ENDTRY
ENDFUNC

**
FUNCTION uf_PAGPENDENTES_sair
	** Fecha Cursores
	fecha("uCrsPagPendentesForn")

	PAGPENDENTES.hide
	PAGPENDENTES.release
ENDFUNC

**a janela flutuante com opcoes de pesquisa desaparece e ficam pesquisa entre datas na parte de cima do form
FUNCTION uf_PAGPENDENTES_CarregaMenu
	
	**PAGPENDENTES.menu1.adicionaOpcao("pesq", "Pesquisar", myPath + "\imagens\icons\lupa_w.png", "uf_PAGPENDENTES_pesquisar","P")		
	**a pesquisa passa a ser feita a partir das caixas no form do titulo em vez de abrir janela por cima
	PAGPENDENTES.menu1.adicionaOpcao("pesq", "Pesquisar", myPath + "\imagens\icons\lupa_w.png", "uf_PAGPENDENTES_ctnPesquisaGravar","P")	
	PAGPENDENTES.menu1.adicionaOpcao("selTodos", "Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_PAGPENDENTES_selTodos with .t.","T")
	
	
	PAGPENDENTES.menu1.estado("","","Introduzir",.t.)	
	
	** Menu do container pesquisar
*!*		PAGPENDENTES.ctnPesquisa.menu1.estado("", "", "OK", .t., "Sair", .t.)
*!*		PAGPENDENTES.ctnPesquisa.menu1.funcaoSair = "uf_PAGPENDENTES_ctnPesquisaFechar"
*!*		PAGPENDENTES.ctnPesquisa.menu1.funcaoGravar = "uf_PAGPENDENTES_ctnPesquisaGravar"	
	PAGPENDENTES.refresh
ENDFUNC



*!*	FUNCTION uf_PAGPENDENTES_pesquisar
*!*		&&sombra e container
*!*		pagpendentes.ctnPesquisa.left = (pagpendentes.width/2) - (pagpendentes.ctnPesquisa.width/2)
*!*		pagpendentes.ctnPesquisa.top = (pagpendentes.height/2) - (pagpendentes.ctnPesquisa.height/2)
*!*		
*!*		PAGPENDENTES.ctnPesquisa.visible = .t.
*!*		
*!*		PAGPENDENTES.ctnPesquisa.optTipo.option1.click()
*!*		
*!*		uf_PAGPENDENTES_sombra(.t.)
*!*	ENDFUNC

*!*	FUNCTION uf_PAGPENDENTES_ctnPesquisaFechar
*!*		** Esconder o painel
*!*		PAGPENDENTES.ctnPesquisa.visible = .f.

*!*		uf_PAGPENDENTES_sombra(.f.)
*!*	ENDFUNC




FUNCTION uf_PAGPENDENTES_ctnPesquisaGravar
	select uCrsPagPendentesForn
	
	DO CASE
*!*			CASE PAGPENDENTES.optTipo.option1.value == 1
*!*				LOCATE FOR uf_gerais_getDate(uCrsPagPendentesForn.Datadoc,"SQL") == uf_gerais_getDate(PAGPENDENTES.txtPesq.value,"SQL")
*!*				
*!*			CASE PAGPENDENTES.optTipo.option2.value == 1
*!*				LOCATE FOR uCrsPagPendentesForn.adoc = ALLTRIM(PAGPENDENTES.txtPesq.value)
*!*				
*!*			CASE PAGPENDENTES.optTipo.option3.value == 1
*!*				LOCATE FOR uf_gerais_getDate(uCrsPagPendentesForn.datavencimento,"SQL") == uf_gerais_getDate(PAGPENDENTES.txtPesq.value,"SQL")
*!*				
*!*			CASE PAGPENDENTES.optTipo.option4.value == 1
*!*				LOCATE FOR uCrsPagPendentesForn.valornReg = Val(astr(PAGPENDENTES.txtPesq.value,8,2))
*!*			
*!*			CASE PAGPENDENTES.optTipo.option5.value == 1
*!*				LOCATE FOR uCrsPagPendentesForn.regularizar = Val(astr(PAGPENDENTES.txtPesq.value,8,2))
			
			
		**20201006, JG	
		**entre datas documento
		CASE PAGPENDENTES.optTipo.option6.value == 1
			SET FILTER TO  uf_gerais_getDate(uCrsPagPendentesForn.Datadoc,"SQL") >= uf_gerais_getDate(PAGPENDENTES.txtPesq.value,"SQL"); 
			AND  uf_gerais_getDate(uCrsPagPendentesForn.Datadoc,"SQL") <= uf_gerais_getDate(PAGPENDENTES.txtPesq2.value,"SQL");
			OR uCrsPagPendentesForn.sel == .t. 
	
		** entre datas vencimento
		CASE PAGPENDENTES.optTipo.option7.value == 1
			SET FILTER TO  uf_gerais_getDate(uCrsPagPendentesForn.datavencimento,"SQL") >= uf_gerais_getDate(PAGPENDENTES.txtPesq.value,"SQL"); 
			AND  uf_gerais_getDate(uCrsPagPendentesForn.datavencimento,"SQL") <= uf_gerais_getDate(PAGPENDENTES.txtPesq2.value,"SQL");
			OR uCrsPagPendentesForn.sel == .t. 
	
		** tipo de documento
		CASE PAGPENDENTES.optTipo.option8.value == 1
			SET FILTER TO  uCrsPagPendentesForn.Cmdesc = ALLTRIM(PAGPENDENTES.txtPesq3.value);
			OR uCrsPagPendentesForn.sel == .t.  
		
		** todos - remove os filtros
*!*			CASE PAGPENDENTES.optTipo.option9.value == 1
*!*				SET FILTER TO  		
				
			
		OTHERWISE
			*****
	ENDCASE
	
	GO TOP	
	**uf_PAGPENDENTES_ctnPesquisaFechar()
	
	PAGPENDENTES.grdPagPendentes.setfocus()
ENDFUNC

*!*	FUNCTION uf_PAGPENDENTES_ctnPesquisaGravar
*!*		select uCrsPagPendentesForn
*!*		
*!*		DO CASE
*!*			CASE PAGPENDENTES.ctnPesquisa.optTipo.option1.value == 1
*!*				LOCATE FOR uf_gerais_getDate(uCrsPagPendentesForn.Datadoc,"SQL") == uf_gerais_getDate(PAGPENDENTES.ctnPesquisa.txtPesq.value,"SQL")
*!*				
*!*			CASE PAGPENDENTES.ctnPesquisa.optTipo.option2.value == 1
*!*				LOCATE FOR uCrsPagPendentesForn.adoc = ALLTRIM(PAGPENDENTES.ctnPesquisa.txtPesq.value)
*!*				
*!*			CASE PAGPENDENTES.ctnPesquisa.optTipo.option3.value == 1
*!*				LOCATE FOR uf_gerais_getDate(uCrsPagPendentesForn.datavencimento,"SQL") == uf_gerais_getDate(PAGPENDENTES.ctnPesquisa.txtPesq.value,"SQL")
*!*				
*!*			CASE PAGPENDENTES.ctnPesquisa.optTipo.option4.value == 1
*!*				LOCATE FOR uCrsPagPendentesForn.valornReg = Val(astr(PAGPENDENTES.ctnPesquisa.txtPesq.value,8,2))
*!*			
*!*			CASE PAGPENDENTES.ctnPesquisa.optTipo.option5.value == 1
*!*				LOCATE FOR uCrsPagPendentesForn.regularizar = Val(astr(PAGPENDENTES.ctnPesquisa.txtPesq.value,8,2))
*!*				
*!*				
*!*			**20201006, JG	
*!*			**entre datas documento
*!*			CASE PAGPENDENTES.ctnPesquisa.optTipo.option6.value == 1
*!*				SET FILTER TO  uf_gerais_getDate(uCrsPagPendentesForn.Datadoc,"SQL") >= uf_gerais_getDate(PAGPENDENTES.ctnPesquisa.txtPesq.value,"SQL"); 
*!*				AND  uf_gerais_getDate(uCrsPagPendentesForn.Datadoc,"SQL") <= uf_gerais_getDate(PAGPENDENTES.ctnPesquisa.txtPesq2.value,"SQL");
*!*				OR uCrsPagPendentesForn.sel == .t. 
*!*		
*!*			** entre datas vencimento
*!*			CASE PAGPENDENTES.ctnPesquisa.optTipo.option7.value == 1
*!*				SET FILTER TO  uf_gerais_getDate(uCrsPagPendentesForn.datavencimento,"SQL") >= uf_gerais_getDate(PAGPENDENTES.ctnPesquisa.txtPesq.value,"SQL"); 
*!*				AND  uf_gerais_getDate(uCrsPagPendentesForn.datavencimento,"SQL") <= uf_gerais_getDate(PAGPENDENTES.ctnPesquisa.txtPesq2.value,"SQL");
*!*				OR uCrsPagPendentesForn.sel == .t. 
*!*		
*!*			** tipo de documento
*!*			CASE PAGPENDENTES.ctnPesquisa.optTipo.option8.value == 1
*!*				SET FILTER TO  uCrsPagPendentesForn.Cmdesc = ALLTRIM(PAGPENDENTES.ctnPesquisa.txtPesq3.value);
*!*				OR uCrsPagPendentesForn.sel == .t.  
*!*			
*!*			** todos - remove os filtros
*!*			CASE PAGPENDENTES.ctnPesquisa.optTipo.option9.value == 1
*!*				SET FILTER TO  		
*!*					
*!*				
*!*			OTHERWISE
*!*				*****
*!*		ENDCASE
*!*		
*!*		uf_PAGPENDENTES_ctnPesquisaFechar()
*!*		
*!*		PAGPENDENTES.grdPagPendentes.setfocus()
*!*	ENDFUNC

*!*	FUNCTION uf_PAGPENDENTES_sombra
*!*		LPARAMETERS tcBool
*!*		
*!*		IF tcBool
*!*			** remover foco da grelha
*!*			PAGPENDENTES.txtNo.setfocus
*!*			
*!*			** Colocar sombra
*!*			PAGPENDENTES.sombra.width	= PAGPENDENTES.width
*!*			PAGPENDENTES.sombra.height	= PAGPENDENTES.height
*!*			PAGPENDENTES.sombra.left 	= 0
*!*			PAGPENDENTES.sombra.top 	= 0
*!*			
*!*			PAGPENDENTES.sombra.visible 	= .t.
*!*		ELSE
*!*			** remover sombra
*!*			PAGPENDENTES.sombra.visible = .f.
*!*		ENDIF
*!*	ENDFUNC

FUNCTION uf_PAGPENDENTES_gravar
	LOCAL lcValRecPerc
	STORE 0 TO lcValRecPerc
	SELECT uCrsPagPendentesForn
	GO TOP
	SCAN FOR uCrsPagPendentesForn.sel == .t. AND uCrsPagPendentesForn.jaincluido == .f.
		uf_pagforn_adicionaLinha()
		SELECT PL
		GO BOTTOM
		replace pl.cdesc 	WITH uCrsPagPendentesForn.cmdesc
		replace pl.adoc 	WITH uCrsPagPendentesForn.adoc
		replace pl.datalc	WITH uCrsPagPendentesForn.datalc
		replace pl.dataven	WITH uCrsPagPendentesForn.dataven
		replace pl.fcstamp	WITH uCrsPagPendentesForn.fcstamp
		replace pl.cm		WITH uCrsPagPendentesForn.cm
		replace pl.eval		WITH uCrsPagPendentesForn.valorNreg
		replace pl.erec 	WITH uCrsPagPendentesForn.regularizar
		*replace pl.escval	WITH uCrsPagPendentesForn.valorNreg * 200.482
		replace pl.escrec	WITH uCrsPagPendentesForn.regularizar * 200.482
		
		&&calcular percentagem de valor recibado
		lcValRecPerc = uCrsPagPendentesForn.regularizar / uCrsPagPendentesForn.valorNreg
		
		SELECT PL
		&&valores do IVA sobre o valor regularizado
		replace pl.eivav1	WITH uCrsPagPendentesForn.eivav1 * lcValRecPerc
		*replace pl.ivav1	WITH uCrsPagPendentesForn.eivav1 * lcValRecPerc * 200.482
		replace pl.eivav2	WITH uCrsPagPendentesForn.eivav2 * lcValRecPerc
		*replace pl.ivav2	WITH uCrsPagPendentesForn.eivav2 * lcValRecPerc * 200.482
		replace pl.eivav3	WITH uCrsPagPendentesForn.eivav3 * lcValRecPerc
		*replace pl.ivav3	WITH uCrsPagPendentesForn.eivav3 * lcValRecPerc * 200.482
		replace pl.eivav4	WITH uCrsPagPendentesForn.eivav4 * lcValRecPerc
		*replace pl.ivav4	WITH uCrsPagPendentesForn.eivav4 * lcValRecPerc * 200.482
		replace pl.eivav5	WITH uCrsPagPendentesForn.eivav5 * lcValRecPerc
		*replace pl.ivav5	WITH uCrsPagPendentesForn.eivav5 * lcValRecPerc * 200.482
		replace pl.eivav6	WITH uCrsPagPendentesForn.eivav6 * lcValRecPerc
		*replace pl.ivav6	WITH uCrsPagPendentesForn.eivav6 * lcValRecPerc * 200.482
		replace pl.eivav7	WITH uCrsPagPendentesForn.eivav7 * lcValRecPerc
		*replace pl.ivav7	WITH uCrsPagPendentesForn.eivav7 * lcValRecPerc * 200.482
		replace pl.eivav8	WITH uCrsPagPendentesForn.eivav8 * lcValRecPerc
		*replace pl.ivav8	WITH uCrsPagPendentesForn.eivav8 * lcValRecPerc * 200.482
		replace pl.eivav9	WITH uCrsPagPendentesForn.eivav9 * lcValRecPerc
		*replace pl.ivav9	WITH uCrsPagPendentesForn.eivav9 * lcValRecPerc * 200.482
		
		replace pl.enaval	WITH uCrsPagPendentesForn.valorNreg
		*replace pl.naval	WITH uCrsPagPendentesForn.valorNreg * 200.482
		replace pl.evori	WITH uCrsPagPendentesForn.valorNreg
		*replace pl.vori		WITH uCrsPagPendentesForn.valorNreg * 200.482
		
		&&valores do IVA originais
*!*			replace pl.eivavori1	WITH uCrsPagPendentesForn.eivav1
*!*			replace pl.ivavori1		WITH uCrsPagPendentesForn.ivav1
*!*			replace pl.eivavori2	WITH uCrsPagPendentesForn.eivav2
*!*			replace pl.ivavori2		WITH uCrsPagPendentesForn.ivav2
*!*			replace pl.eivavori3	WITH uCrsPagPendentesForn.eivav3
*!*			replace pl.ivavori3		WITH uCrsPagPendentesForn.ivav3
*!*			replace pl.eivavori4	WITH uCrsPagPendentesForn.eivav4
*!*			replace pl.ivavori4		WITH uCrsPagPendentesForn.ivav4
*!*			replace pl.eivavori5	WITH uCrsPagPendentesForn.eivav5
*!*			replace pl.ivavori5		WITH uCrsPagPendentesForn.ivav5
*!*			replace pl.eivavori6	WITH uCrsPagPendentesForn.eivav6
*!*			replace pl.ivavori6		WITH uCrsPagPendentesForn.ivav6
*!*			replace pl.eivavori7	WITH uCrsPagPendentesForn.eivav7
*!*			replace pl.ivavori7		WITH uCrsPagPendentesForn.ivav7
*!*			replace pl.eivavori8	WITH uCrsPagPendentesForn.eivav8
*!*			replace pl.ivavori8		WITH uCrsPagPendentesForn.ivav8
*!*			replace pl.eivavori9	WITH uCrsPagPendentesForn.eivav9
*!*			replace pl.ivavori9		WITH uCrsPagPendentesForn.ivav9
		
		replace pl.moedoc	WITH uCrsPagPendentesForn.moeda
		
		&&taxas de IVA
		replace pl.ivatx1	WITH uCrsPagPendentesForn.ivatx1
		replace pl.ivatx2	WITH uCrsPagPendentesForn.ivatx2
		replace pl.ivatx3	WITH uCrsPagPendentesForn.ivatx3
		replace pl.ivatx4	WITH uCrsPagPendentesForn.ivatx4
		replace pl.ivatx5	WITH uCrsPagPendentesForn.ivatx5
		replace pl.ivatx6	WITH uCrsPagPendentesForn.ivatx6
		replace pl.ivatx7	WITH uCrsPagPendentesForn.ivatx7
		replace pl.ivatx8	WITH uCrsPagPendentesForn.ivatx8
		replace pl.ivatx9	WITH uCrsPagPendentesForn.ivatx9
		
		replace pl.fcorigem	WITH uCrsPagPendentesForn.origem
		
	ENDSCAN
	uf_PAGPENDENTES_sair()
	uf_pagforn_calTotais()
	PAGFORN.refresh()
ENDFUNC


FUNCTION uf_PAGPENDENTES_selTodos
	LPARAMETERS lcBool
	
	PAGPENDENTES.txtNo.setfocus()
	
	SELECT uCrsPagPendentesForn
	GO TOP 
	SCAN
		replace uCrsPagPendentesForn.sel WITH lcBool
	ENDSCAN 
	GO TOP 
	
	IF lcBool
		PAGPENDENTES.menu1.selTodos.config("Dess. Todos", myPath + "\imagens\icons\checked_w.png", "uf_PAGPENDENTES_selTodos with .F.","T")
	ELSE
		PAGPENDENTES.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_PAGPENDENTES_selTodos with .T.","T")
	ENDIF 
	
	uf_PAGPENDENTES_calTotalSel()
ENDFUNC 