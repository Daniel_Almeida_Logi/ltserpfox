
** lcOrigem	: painel que chamou este painel
FUNCTION uf_pesqfornecedores_chama
	LPARAMETERS lcOrigem

	** Controle de Permiss�es	
	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Fornecedores')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL GEST�O DE FORNECEDORES.","OK","",48)
		RETURN .f.
	ENDIF
	
	PUBLIC myOrigemPPFl
	myOrigemPPFl = lcOrigem
	
	LOCAL lcSQL
	lcSql = ''

	IF USED("uCrsPesqEnt")
		fecha("uCrsPesqEnt")
	ENDIF
		
	uf_pesqFornecedores_CarregaDados(.t.)
	
	IF TYPE('PESQFORNECEDORES')=="U"
		DO FORM PESQFORNECEDORES
	ELSE
		PESQFORNECEDORES.show
	ENDIF
	
	
	&&Filtra Fornecedores
	IF(lcOrigem== "ENCAUTOMATICAS_FILTRAR")
		uf_pesqFornecedores_retornaFornecedoresNaLista()
	ENDIF
	
ENDFUNC


** tcBool	: caso venha a .t. cria apenas o cursor
FUNCTION uf_pesqFornecedores_CarregaDados
	LPARAMETERS tcBool, tcReport
	
	DO CASE
		CASE !TYPE("PESQFORNECEDORES") == "U" 
			IF !uf_gerais_validaPermUser(ch_userno, ch_grupo,'Relat�rios - Painel')
				uf_perguntalt_chama("O SEU PERFIL N�O TEM PERMISS�ES PARA ACEDER A ESTA FUNCIONALIDADE.","OK","",48)
				RETURN .f.
			ENDIF
	ENDCASE
	
	
	**
	LOCAL lcIDSedeGrupo
	lcIDSedeGrupo = ""
	IF USED("uCrsConfiguracaoSedeGrupo")
		IF myencGrupo == .t.
			SELECT uCrsConfiguracaoSedeGrupo
			lcIDSedeGrupo = ALLTRIM(uCrsConfiguracaoSedeGrupo.bdname)
		ENDIF
	ENDIF 
	

	IF tcBool 
		IF !USED("uCrsPesqEnt")	
			LOCAL lcSQL
			lcSql = ''

			IF myOrigemPPFl != "ENCAUTOMATICAS_REF"
				
				IF( myOrigemPPFl == "ENCAUTOMATICAS_FILTRAR")
	
					TEXT TO lcSql NOSHOW TEXTMERGE
						exec up_fornecedores_pesquisar '', 0, 999, '', '', '', 0, '', <<mysite_nr>>
					ENDTEXT
				
				ELSE
					
					TEXT TO lcSql NOSHOW TEXTMERGE
						exec up_fornecedores_pesquisar '', 0, 0, '', '', '', 0, '', <<mysite_nr>>
					ENDTEXT
						
				ENDIF
				
	
			ELSE
				LOCAL lcRef 
				lcRef = ''
				 
				if(USED("crsenc"))
					lcRef = ALLTRIM(crsenc.ref)
				ENDIF
				
				SELECT crsenc
				TEXT TO lcSql NOSHOW TEXTMERGE
					exec up_fornecedores_pesquisar '', 0, 999, '', '', '', 0, '<<ALLTRIM(lcRef )>>',<<mysite_nr>>,'<<ALLTRIM(lcIDSedeGrupo)>>'
				ENDTEXT
			ENDIF

			IF type("PESQFORNECEDORES")=="U"
				IF !uf_gerais_actGrelha("",[ucrsPesqEnt],lcSql)
					uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR A LISTAGEM. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
					RETURN .f.
				ENDIF
			ELSE					
				
				IF uf_gerais_actGrelha("PESQENTIDADES.GridPesq","ucrsPesqEnt",lcSQL)
					PESQFORNECEDORES.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqEnt"))) + " Resultados"
				ENDIF
			ENDIF
			

		ENDIF
			
	ELSE && Refresca valores da Grid
	
		LOCAL lcNome, lcNo, lcEstab, lcTipo, lcNcont, lcTelef, lcInactivo
		STORE '' TO lcNome, lcTipo, lcNcont
		STORE 0 TO lcNo, lcEstab, lcTelef, lcInactivo
		
		lcNome 		= STRTRAN(ALLTRIM(PESQFORNECEDORES.txtNome.value), ' ', '%')
		lcNo		= IIF(EMPTY(ALLTRIM(PESQFORNECEDORES.txtNo.value)), 0, ALLTRIM(PESQFORNECEDORES.txtNo.value))
		lcEstab		= IIF(EMPTY(ALLTRIM(PESQFORNECEDORES.txtEstab.value)), 999, ALLTRIM(PESQFORNECEDORES.txtEstab.value))
		lcTipo		= ALLTRIM(PESQFORNECEDORES.txtTipo.value)
		lcNcont		= ALLTRIM(PESQFORNECEDORES.txtNcont.value)
		lcTelef		= ALLTRIM(PESQFORNECEDORES.txtTelef.value)
		lcInactivo	= IIF(PESQFORNECEDORES.chkinativo.tag = "true", 1, 0)
		lcRef		= ALLTRIM(PESQFORNECEDORES.ref.value)
		
		IF EMPTY(tcReport)
		
			IF PESQFORNECEDORES.chkContido.tag = "true"
				TEXT TO lcSQL NOSHOW TEXTMERGE
					exec up_fornecedores_pesquisar_contido '<<lcNome>>', <<lcNo>>, <<lcEstab>>, '<<lcTipo>>',
					     '<<lcNcont>>', '<<lcTelef>>', <<lcInactivo>>, '<<lcRef>>', <<mysite_nr>>,'<<lcIDSedeGrupo>>'
				ENDTEXT
			ELSE
				TEXT TO lcSQL NOSHOW TEXTMERGE
					exec up_fornecedores_pesquisar '<<lcNome>>', <<lcNo>>, <<lcEstab>>, '<<lcTipo>>',
					     '<<lcNcont>>', '<<lcTelef>>', <<lcInactivo>>, '<<lcRef>>', <<mysite_nr>>,'<<lcIDSedeGrupo>>'
				ENDTEXT			
			ENDIF 
	
			uf_gerais_actGrelha("PESQFORNECEDORES.grdEntidades", "uCrsPesqEnt", lcSQL)
			
			regua(2)

			PESQFORNECEDORES.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqEnt"))) + " Resultados"

			PESQFORNECEDORES.grdEntidades.Refresh
		
		ELSE
			TEXT TO lcReport TEXTMERGE NOSHOW	
				&nome=<<lcNome>>&no=<<lcNo>>&estab=<<lcEstab>>&tipo=<<lcTipo>>&ncont=<<lcNcont>>&ntelef=<<lcTelef>>&site_nr=<<mysite_nr>>&site=<<ALLTRIM(ucrsE1.siteLoja)>>&ref=<<lcRef>>&inactivo=<<IIF(lcInactivo==1,"true","false")>>
			ENDTEXT
			
			SELECT ucrsPesqEnt
			GO TOP
				
				IF RECCOUNT("ucrsPesqEnt") > 0
					uf_gerais_chamaReport("listagem_fornecedores_avancada", ALLTRIM(lcReport))
				ELSE
					uf_perguntalt_chama("N�o existem dados para Impress�o. Por favor verifique. Obrigado.","OK","",16)
				ENDIF
		ENDIF	
		regua(2)
	ENDIF
	
	** Coloca Cursor na Coluna de selec��o de Fornecedor
	SELECT uCrsPesqEnt
	IF RECCOUNT("uCrsPesqEnt") > 0
		IF TYPE('PESQFORNECEDORES')!="U"
			PESQFORNECEDORES.grdentidades.sel.setfocus
		ENDIF
	ELSE
		IF TYPE('PESQFORNECEDORES')!="U"
			PESQFORNECEDORES.txtnome.setfocus
		ENDIF
	ENDIF
	
ENDFUNC

FUNCTION uf_pesqFornecedores_retornaFornecedoresNaLista
	LOCAL lcTotalForn
	STORE 0 TO lcTotalForn
	&& se existe o cursod e encomendas
	IF(USED("crsenc"))
		
		&&cria copia do cursor e apaga todas as linas da copia
				
		SELECT * from ucrsPesqEnt INTO CURSOR ucrsPesqEntAux READWRITE
		DELETE  FROM  ucrsPesqEntAux 
		
		SELECT crsenc
		GO TOP
		SCAN
			&& Insere as linhas no cursor auxiliar
			INSERT INTO ucrsPesqEntAux SELECT *  from  ucrsPesqEnt  WHERE crsenc.fornec=ucrsPesqEnt.no AND crsenc.fornestab=ucrsPesqEnt.estab
		ENDSCAN	
		
		
		&&limpa o cursor principal
		DELETE  FROM  ucrsPesqEnt
		&&preenche com os dados do cursor auxiliar	
		INSERT INTO ucrsPesqEnt SELECT distinct  *  FROM  ucrsPesqEntAux 
								
		Select ucrsPesqEnt 
 		Calculate Count(no) To lcTotalForn
		
		&&ir para o topo
		SELECT  ucrsPesqEnt 
		GO TOP

		
		&&actualiza informa��o
		PESQFORNECEDORES.lblRegistos.caption = ALLTRIM(STR(lcTotalForn))+ " Resultados"
		PESQFORNECEDORES.grdEntidades.refresh

		
		
		fecha("ucrsPesqEntAux")
	
	ENDIF
	

ENDFUNC



***
FUNCTION uf_pesqfornecedores_AplicaVazio

	DO CASE
		CASE myOrigemPPFl == "DOCUMENTOS"
			Select ucrsPesqEnt
			
			Select cabdoc
			replace cabdoc.nome		WITH ""
			replace cabdoc.no		WITH 0
			replace cabdoc.estab	WITH 0
			Replace cabdoc.DescFin WITH 0
						
			DOCUMENTOS.ContainerCab.refresh
		
		CASE myOrigemPPFl == "STOCKS"
			Select ucrsPesqEnt
	
			Select st
			replace st.fornecedor	WITH ""
			replace st.fornec		WITH 0
			replace st.fornestab	WITH 0
			
			stocks.pageframe1.page3.refresh
		
		CASE myOrigemPPFl == "STOCKS_ALTERACOES"
			Select ucrsPesqEnt
	
			pesqstocks.container2.txtfornecedor.value	= ""
			pesqstocks.container2.txtfornec.value		= 0
			pesqstocks.container2.txtfornestab.value	= 0
					
		CASE myOrigemPPFl == "FORNECEDORES"
			Select ucrsPesqEnt
			uf_forn_chama(0, 0)
		
		CASE myOrigemPPFl == "ENCAUTOMATICAS"
			Select ucrsPesqEnt

			uf_encautomaticas_alteraFornecedorSel("",0,0)
			
		CASE myOrigemPPFl == "ENCAUTOMATICAS_REF"
			IF USED("crsenc")
				Select ucrsPesqEnt
				SELECT crsenc
				Replace  crsenc.fornecedor 	WITH ""
				Replace  crsenc.fornec 		WITH 0
				Replace  crsenc.fornestab	WITH 0
				Replace  crsenc.enc			WITH .f.
				
				** recalcula totais
				uf_encautomaticas_ActualizaQtValorEnc()
				
			ENDIF
			
		CASE myOrigemPPFl == "ENCAUTOMATICAS_FILTRAR"
			Select ucrsPesqEnt
			
			encautomaticas.nome.value	= ""
			encautomaticas.no.value		= 0
			encautomaticas.estab.value	= 0

			uf_encautomaticas_aplicaFiltros()
			
		CASE myOrigemPPFl == "PREPARARENCOMENDA"
			Select ucrsPesqEnt
			
			prepararencomenda.nome.value	= ""
			prepararencomenda.no.value		= 0
			prepararencomenda.estab.value	= 0
			
		CASE myOrigemPPFl == "PAGAMENTOS"
			If Used("PO")
				Select PO
				replace po.nome		with		""
				replace po.no		with		0
				replace po.estab	with		0
				replace po.pais		with		""
				replace po.morada	with		""
				replace po.local	with		""
				replace po.codpost	with		""
				replace po.ncont	with		""
				replace po.ccusto	with		""
				replace po.zona		with		""
				pagforn.refresh
			ENDIF
			
		CASE myOrigemPPFl == "PROMOCOES"
			Select uCrsPromoC
			replace uCrsPromoC.Forn_nome	WITH "" ;
				uCrsPromoC.forn_no		WITH 0 ;
				uCrsPromoC.forn_estab	WITH 0
						
			promocoes.pageframe1.page2.refresh
		
		CASE myOrigemPPFl == "DADOSENCAT"
			
			DADOSENCAT.txtnome.value	= ""
			DADOSENCAT.txtno.value		= 0
			DADOSENCAT.txtestab.value	= 0
		
		CASE myOrigemPPFl == "PESQSTOCKS"
            PESQSTOCKS.PageFrame1.page1.fornecedor.tag = ""
			PESQSTOCKS.PageFrame1.page1.fornecedor.value = ""
			
		OTHERWISE
			RETURN .f.
	ENDCASE
	
	uf_pesqFornecedores_sair()

ENDFUNC


**
FUNCTION uf_pesqFornecedores_Sel

	DO CASE
		CASE myOrigemPPFl == "DOCUMENTOS"
			Select ucrsPesqEnt
			
			Select cabdoc
			replace cabdoc.nome		WITH ucrsPesqEnt.nome
			replace cabdoc.no		WITH ucrsPesqEnt.no
			replace cabdoc.estab	WITH uCrsPesqEnt.estab
			replace cabdoc.condPag	WITH uCrsPesqEnt.tpdesc
			replace cabdoc.datavenc	WITH cabDoc.datadoc

         REPLACE cabdoc.xpdmorada WITH "[" + ASTR(uCrsPesqEnt.NO) + "]" +  ALLTRIM(uCrsPesqEnt.morada),;  
                  cabdoc.xpdcodpost WITH ALLTRIM(uCrsPesqEnt.codPost),; 
                  cabdoc.xpdLocalidade WITH  ALLTRIM(uCrsPesqEnt.local)

			
			&& Encomendas a Fornecedor actualiza linhas com pre�os do Fornecedor
			IF cabDoc.numinternodoc == 2
				uf_documentos_TabelaPrecosDocumentos()
			ENDIF
			
			&& Tabala fixa de Iva para fornecedores estrangeiros
			uf_documentos_recalculodocforn(uCrsPesqEnt.tabIva)

			uf_documentos_CalculaDataVencimento()

			IF !empty(uCrsPesqEnt.retencao)
				SELECT uCrsPesqEnt
				REPLACE CabDOC.DESCFIN WITH uCrsPesqEnt.retencao
			ENDIF 
			
			** Alterado JC 01-10-2018 - se o fornecedor anterior tivesse desconto e o atual n�o n�o substituia
			IF !empty(uCrsPesqEnt.desconto)
				DOCUMENTOS.descontoFL = uCrsPesqEnt.desconto
				uf_documentos_aplicaDescontoComFl()
			ELSE 
				DOCUMENTOS.descontoFL = 0
				uf_documentos_aplicaDescontoComFl()
			ENDIF 

			IF USED("ucrsts")
				SELECT ucrsts
				IF ucrsTs.usaDescForn

					SELECT BI
					GO TOP

					SCAN FOR !EMPTY(bi.ref)

						SELECT cabdoc
						SELECT bi

						TEXT TO lcSQL TEXTMERGE NOSHOW
							up_documentos_getDescForn '<<ALLTRIM(bi.ref)>>', <<cabdoc.no>>, <<cabdoc.estab>>
						ENDTEXT

						IF !uf_gerais_actGrelha("", "uc_descFornEncAuto", lcSQL)
							uf_perguntalt_chama("Erro a obter desconto de Fornecedor." + CHR(13) + "Por favor contacte o suporte.", "OK", "", 48)
						ELSE
							SELECT uc_descFornEncAuto
							SELECT bi

							REPLACE bi.desconto WITH uc_descFornEncAuto.desconto

							uf_documentos_recalculaTotaisBI()

							FECHA("uc_descFornEncAuto")

						ENDIF

						SELECT bi
					ENDSCAN



					SELECT BI 
					GO TOP

					uf_documentos_actualizaTotaisCAB()

				ENDIF
			ENDIF
					
			uf_documentos_actualizatotaisCAB(.f.)
			uf_documentos_CalculaTotais()
			
			DOCUMENTOS.ContainerCab.refresh
			DOCUMENTOS.PageFrame1.Page1.detalhest1.refresh
		
		CASE myOrigemPPFl == "STOCKS"
			Select ucrsPesqEnt
	
			Select st
			replace st.fornecedor	WITH ucrsPesqEnt.nome
			replace st.fornec		WITH ucrsPesqEnt.no
			replace st.fornestab	WITH uCrsPesqEnt.estab		
			
			stocks.pageframe1.page3.refresh
		
		CASE myOrigemPPFl == "STOCKS_ALTERACOES"
			Select ucrsPesqEnt
	
			pesqstocks.container2.PageFrame1.Page1.txtfornecedor.value	= ucrsPesqEnt.nome
			pesqstocks.container2.PageFrame1.Page1.txtfornec.value		= ucrsPesqEnt.no
			pesqstocks.container2.PageFrame1.Page1.txtfornestab.value	= ucrsPesqEnt.estab	
		
		CASE myOrigemPPFl == "FORNECEDORES"
			Select ucrsPesqEnt
			uf_forn_chama(ucrsPesqEnt.no, ucrsPesqEnt.estab)
		
		CASE myOrigemPPFl == "ENCAUTOMATICAS"
			Select ucrsPesqEnt

			uf_encautomaticas_alteraFornecedorSel(ucrsPesqEnt.nome, ucrsPesqEnt.no, ucrsPesqEnt.estab, ucrsPesqEnt.nome2)

		CASE myOrigemPPFl == "ENCAUTOMATICAS_REF"
            IF USED("crsenc")
                Select ucrsPesqEnt
                SELECT crsenc
                Replace  crsenc.fornecedor     	WITH ALLTRIM(ucrsPesqEnt.nome)
                Replace  crsenc.fornec         	WITH ucrsPesqEnt.no
                Replace  crsenc.fornestab    	WITH ucrsPesqEnt.estab
                Replace  crsenc.bonusfornec    	WITH IIF(!EMPTY(ucrsPesqEnt.bonus),'S','')
                Replace  crsenc.pclfornec    	WITH ucrsPesqEnt.pcl
                Replace  crsenc.esgotado    	WITH IIF(UPPER(ALLTRIM(ucrsPesqEnt.esgotado))='SIM','S','')
				Replace  crsenc.alertaStockGH 	WITH ucrsPesqEnt.alertaStockGH
               	Replace  crsenc.alertaPCL 		WITH ucrsPesqEnt.alertaPCL
               	Replace  crsenc.alertaBonus 	WITH ucrsPesqEnt.alertaBonus
               	replace crsenc.fornecabrev 		WITH IIF(EMPTY(ucrsPesqEnt.nome2), ALLTRIM(ucrsPesqEnt.nome),ALLTRIM(ucrsPesqEnt.nome2))

				IF TYPE("crsenc.descontoForn") <> "U"

					LOCAL uv_sqlDescForn

					TEXT TO uv_sqlDescForn TEXTMERGE NOSHOW
						up_documentos_getDescForn '<<ALLTRIM(crsenc.ref)>>', <<ucrsPesqEnt.no>>, <<ucrsPesqEnt.estab>>
					ENDTEXT

					IF !uf_gerais_actGrelha("", "uc_descFornEncAuto", uv_sqlDescForn)
						uf_perguntalt_chama("Erro a obter desconto de Fornecedor." + CHR(13) + "Por favor contacte o suporte.", "OK", "", 48)
					ELSE
						SELECT uc_descFornEncAuto
						SELECT crsenc

						REPLACE crsenc.descontoForn WITH uc_descFornEncAuto.desconto
						REPLACE crsenc.descontoFornOri WITH uc_descFornEncAuto.desconto

						FECHA("uc_descFornEncAuto")
					ENDIF

				ENDIF
               
               	SELECT crsenc
                Replace  crsenc.enc    WITH .t.
                
                
                ENCAUTOMATICAS.detalhest1.refresh
                
                ** recalcula totais
				uf_encautomaticas_ActualizaQtValorEnc()
                
            ENDIF		
			
		CASE myOrigemPPFl == "ENCAUTOMATICAS_FILTRAR"
			Select ucrsPesqEnt
			
			encautomaticas.nome.value	= ALLTRIM(ucrsPesqEnt.nome)
			encautomaticas.no.value		= ucrsPesqEnt.no
			encautomaticas.estab.value	= ucrsPesqEnt.estab

			uf_encautomaticas_aplicaFiltros()
			
		CASE myOrigemPPFl == "PREPARARENCOMENDA"
			Select ucrsPesqEnt
			
			prepararencomenda.nome.value	= ALLTRIM(ucrsPesqEnt.nome)
			prepararencomenda.no.value		= ucrsPesqEnt.no
			prepararencomenda.estab.value	= ucrsPesqEnt.estab
			
		CASE myOrigemPPFl == "PAGAMENTOS"
			If Used("PO")
				Select PO
				replace po.nome		with		Alltrim(ucrsPesqEnt.nome)
				replace po.no		with		ucrsPesqEnt.no
				replace po.estab	with		ucrsPesqEnt.estab
				replace po.pais		with		ucrsPesqEnt.pais
				replace po.morada	with		Alltrim(ucrsPesqEnt.morada)
				replace po.local	with		Alltrim(ucrsPesqEnt.local)
				replace po.codpost	with		ucrsPesqEnt.codpost
				replace po.ncont	with		ucrsPesqEnt.ncont
				replace po.ccusto	with		ucrsPesqEnt.ccusto
				replace po.zona		with		Alltrim(ucrsPesqEnt.zona)
				pagforn.refresh
			Endif
		
		CASE myOrigemPPFl == "ADIANTAMENTOS"
			If Used("PD")	
				Select pd
				replace pd.nome		WITH Alltrim(ucrsPesqEnt.nome)
				replace pd.no		WITH ucrsPesqEnt.no
				replace pd.estab	WITH ucrsPesqEnt.estab
				replace pd.ccusto	WITH ucrsPesqEnt.ccusto
				spd.refresh
			Endif
		
		CASE myOrigemPPFl == "PROMOCOES"
			Select ucrsPesqEnt
			
			Select uCrsPromoC
			replace uCrsPromoC.Forn_nome	WITH ALLTRIM(ucrsPesqEnt.nome) ;
				uCrsPromoC.forn_no		WITH ucrsPesqEnt.no ;
				uCrsPromoC.forn_estab	WITH ucrsPesqEnt.estab
						
			promocoes.pageframe1.page2.refresh
		
		CASE myOrigemPPFl == "DADOSENCAT"
			
			DADOSENCAT.txtnome.value	= ALLTRIM(ucrsPesqEnt.nome)
			DADOSENCAT.txtno.value		= ucrsPesqEnt.no
			DADOSENCAT.txtestab.value	=  ucrsPesqEnt.estab
			
		CASE myOrigemPPFl == "PESQSTOCKS"
            PESQSTOCKS.PageFrame1.page1.fornecedor.tag = ALLTRIM(ucrsPesqEnt.flstamp)
			PESQSTOCKS.PageFrame1.page1.fornecedor.value= ALLTRIM(ucrsPesqEnt.nome)		
			
		CASE ALLTRIM(UPPER(myOrigemPPFl)) == "PCENTRAL"
			uf_forn_chama(ucrsPesqEnt.no, ucrsPesqEnt.estab)
		CASE ALLTRIM(UPPER(myOrigemPpFl)) == "MORADA"	
			SELECT cabdoc 
			**REPLACE cabdoc.xpdmorada WITH "[" + ASTR(uCrsPesqEnt.NO) + "]" +  ALLTRIM(uCrsPesqEnt.morada) + " " + ALLTRIM(uCrsPesqEnt.codPost) + " " + ALLTRIM(uCrsPesqEnt.local)
            REPLACE cabdoc.xpdmorada WITH "[" + ASTR(uCrsPesqEnt.NO) + "]" +  ALLTRIM(uCrsPesqEnt.morada),;  
                    cabdoc.xpdcodpost WITH ALLTRIM(uCrsPesqEnt.codPost),; 
                    cabdoc.xpdLocalidade WITH  ALLTRIM(uCrsPesqEnt.local)

            IF WEXIST("DOCUMENTOS")
               DOCUMENTOS.ContainerCab.refresh
			      DOCUMENTOS.PageFrame1.Page1.detalhest1.refresh
            ENDIF
			
		OTHERWISE
			RETURN .f.
	ENDCASE
	
	uf_pesqFornecedores_sair()
ENDFUNC


**
FUNCTION uf_pesqFornecedores_gravar
ENDFUNC


**
FUNCTION uf_pesqFornecedores_sair
	** Fecha Cursores
	**IF USED("ucrsPesqEnt")
	**	fecha("ucrsPesqEnt")
	**ENDIF
	
	IF myOrigemPPFl == "ENCAUTOMATICAS_REF"
		ENCAUTOMATICAS.enabled = .t.
	ENDIF
	
	RELEASE myOrigemPPFl
	
	PESQFORNECEDORES.hide
	PESQFORNECEDORES.release
ENDFUNC


**
FUNCTION uf_pesqFornecedores_CarregaMenu
	
	** Configura Menu Barra Lateral
	WITH PESQFORNECEDORES.menu1
		**.adicionaOpcao("aplicacoes", "Aplica��es", myPath + "\imagens\icons\aplicacoes_w.png", "uf_gerais_menuAplicacoes With 'pesqfornecedores'","O")
		.adicionaOpcao("opcoes", "Op��es", myPath + "\imagens\icons\opcoes_w.png", "uf_gerais_menuOpcoes With 'pesqfornecedores'","O")
		.adicionaOpcao("actualizar", "Pesquisar", myPath + "\imagens\icons\lupa_w.png", "uf_pesqFornecedores_CarregaDados with .f.","A")
		.adicionaOpcao("novo", "Novo", myPath + "\imagens\icons\mais_w.png", "uf_pesqFornecedores_novo","N")
		**.adicionaOpcao("selTodos","Sel. Todos",myPath + "\imagens\icons\unchecked_w.png","uf_pesqFornecedores_seltodos","T")
		.adicionaOpcao("subir", "Subir", myPath + "\imagens\icons\pg_up_b.png", "uf_gerais_MovePage with .t.,'pesqFornecedores.menu1','ucrsPesqEnt'")
		.adicionaOpcao("descer", "Descer", myPath + "\imagens\icons\pg_down_b.png", "uf_gerais_MovePage with .f.,'pesqFornecedores.menu1','ucrsPesqEnt'")	
		**IF myOrigemPPFl == "STOCKS_ALTERACOES" OR myOrigemPPFl == "DADOSENCAT" OR myOrigemPPFl == "PESQSTOCKS"
		IF myOrigemPPFl == "DADOSENCAT" OR myOrigemPPFl == "PESQSTOCKS"
			.adicionaOpcao("vazio","Sem Forn.",myPath + "\imagens\icons\unchecked_w.png","uf_pesqFornecedores_vazio","V")
		ENDIF
	
	ENDWITH
	
	** Configura Menu Aplica��es
**	WITH PESQFORNECEDORES.menu_aplicacoes
**		.adicionaOpcao("documentos", "Documentos", "", "uf_PESQFORNECEDORES_chamadocumentos")
**		.adicionaOpcao("pagamentos", "Pagamentos", "", "uf_PESQFORNECEDORES_chamapagamentos")
**	ENDWITH

	** Configura Menu Op��es
	WITH PESQFORNECEDORES.menu_opcoes
		.adicionaOpcao("Imprimir","Imprimir Dados","","uf_pesqFornecedores_CarregaDados with .f.,.t.")
		.adicionaOpcao("documentos", "Documentos", "", "uf_PESQFORNECEDORES_chamadocumentos")
		.adicionaOpcao("pagamentos", "Pagamentos", "", "uf_PESQFORNECEDORES_chamapagamentos")
	ENDWITH 
	
	DO CASE 
		CASE myOrigemPPFl == "DADOSENCAT" OR myOrigemPPFl == "PESQSTOCKS" OR myOrigemPPFl == "STOCKS_ALTERACOES" 
			PESQFORNECEDORES.menu1.estado("opcoes, novo", "HIDE")
		**
	ENDCASE

	PESQFORNECEDORES.refresh
ENDFUNC


**
FUNCTION uf_pesqfornecedores_chamaPesqFornecedores
	IF !myFlIntroducao AND !myFlAlteracao
		SELECT ucrsPesqEnt
		uf_forn_chama(ucrsPesqEnt.no, ucrsPesqEnt.estab)
		uf_pesqfornecedores_sair()
		forn.show
	ELSE
		uf_perguntalt_chama("O ECR� DE FORNECEDORES ENCONTRA-SE EM MODO DE INTRODU��O OU EDI��O","OK","", 48)
	ENDIF
ENDFUNC


**
FUNCTION uf_pesqFornecedores_incluirInactivos
	IF EMPTY(PESQFORNECEDORES.menu1.inactivos.tag) OR PESQFORNECEDORES.menu1.inactivos.tag == "true"
		PESQFORNECEDORES.menu1.inactivos.tag = "false"
		PESQFORNECEDORES.menu1.inactivos.config("Inactivos", myPath + "\imagens\icons\unchecked_w.png", "uf_pesqFornecedores_incluirInactivos","I")
	ELSE
		PESQFORNECEDORES.menu1.inactivos.tag = "true"
		PESQFORNECEDORES.menu1.inactivos.config("Inactivos", myPath + "\imagens\icons\checked_w.png", "uf_pesqFornecedores_incluirInactivos","I")
	ENDIF
ENDFUNC


**
FUNCTION uf_pesqFornecedores_tecladoVirtual
	PESQFORNECEDORES.tecladoVirtual1.show("PESQFORNECEDORES.grdEntidades", 161, "PESQFORNECEDORES.shape7", 161)
ENDFUNC


**
FUNCTION uf_pesqFornecedores_novo
	
	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Fornecedores - Introduzir')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE CRIAR FORNECEDORES.","OK","",48)
		return
	ENDIF

	uf_forn_novo()
	uf_pesqFornecedores_sair()

ENDFUNC


**fun��o para seleccionar todos os fornecedores
FUNCTION uf_PESQFORNECEDORES_seltodos
	LOCAL lcPos
		
		SELECT ucrsPesqEnt
		lcPos = RECNO("ucrsPesqEnt")
		
	IF PESQFORNECEDORES.menu1.selTodos.lbl.caption == "Sel. Todos"
		SELECT ucrsPesqEnt
		replace ALL ucrsPesqEnt.sel WITH .t.

		PESQFORNECEDORES.menu1.selTodos.config("De-Sel. Todos", myPath + "\imagens\icons\checked_w.png", "uf_PESQFORNECEDORES_seltodos")
	ELSE	
		SELECT ucrsPesqEnt
		replace ALL ucrsPesqEnt.sel WITH .f.

		PESQFORNECEDORES.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_PESQFORNECEDORES_seltodos")
	ENDIF
	
	SELECT ucrsPesqEnt
	TRY
		GO lcPos
	CATCH
	ENDTRY
	
ENDFUNC


**fun��o para abrir painel de documentos
FUNCTION uf_PESQFORNECEDORES_chamadocumentos
	** Fecha Cursores
	IF USED("ucrsPesqEnt")
		fecha("ucrsPesqEnt")
	ENDIF
	
	uf_documentos_chama()
	
	PESQFORNECEDORES.hide
	PESQFORNECEDORES.release
ENDFUNC


**funca��o para abrir painel de pagamentos a fornecedor
FUNCTION uf_PESQFORNECEDORES_chamapagamentos
	** Fecha Cursores
	IF USED("ucrsPesqEnt")
		fecha("ucrsPesqEnt")
	ENDIF
	
	uf_forn_pagamentos()
	
	PESQFORNECEDORES.hide
	PESQFORNECEDORES.release
ENDFUNC


** filtro para pesquisa de produtos sem fornecedor e para colocar produtos sem fornecedor na altera��o de produtos em grupo
FUNCTION uf_pesqFornecedores_vazio
	
	DO CASE 
		CASE myOrigemPPFl == "DADOSENCAT"
			DADOSENCAT.txtnome.value	= ""
			DADOSENCAT.txtno.value		= 0
			DADOSENCAT.txtestab.value	= 0
			
		CASE myOrigemPPFl == "STOCKS_ALTERACOES"
			Select ucrsPesqEnt
			pesqstocks.container2.PageFrame1.page1.txtfornecedor.value	= 'Sem Fornecedor Preferencial'
			pesqstocks.container2.PageFrame1.page1.txtfornec.value		= 0
			pesqstocks.container2.PageFrame1.page1.txtfornestab.value	= 0
			
			&& Temp
			PESQSTOCKS.PageFrame1.page1.fornecedor.value = "S/ Fornecedor"			
		
		CASE myOrigemPPFl == "PESQSTOCKS"
            PESQSTOCKS.PageFrame1.page1.fornecedor.tag = ""
			PESQSTOCKS.PageFrame1.page1.fornecedor.value = "S/ Fornecedor"			

	ENDCASE
	
	uf_pesqFornecedores_sair()
	
ENDFUNC
