FUNCTION uf_CCNRFornecedor_chama
	LPARAMETERS lcNo, lcEstab, lcNome
	
	&&valida parameters
	IF EMPTY(lcNO) OR !(Type("lcNO")=="N")
		lcNO= 0
	ENDIF
	
	IF EMPTY(lcEstab) OR !(Type("lcEstab")=="N")
		lcEstab = 0
	ENDIF
	
	IF EMPTY(lcNome)
		lcNome = ""
	ENDIF
	*******************************
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		SET fmtonly on
		exec up_fornecedores_cc_nreg <<lcNo>>, <<lcEstab>>, ''
		SET fmtonly off
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "uCrsPesqCCNRF", lcSql)
		uf_perguntalt_chama("ERRO AO LER A CONTA CORRENTE N�O REGULARIZADA DO FORNECEDOR!","OK","", 16)
		RETURN .F.
	ENDIF 
	
	&& Controla Abertura do Painel			
	if type("CCNRFornecedor")=="U"
		DO FORM CCNRFornecedor
	ELSE
		CCNRFornecedor.show
	ENDIF

	CCNRFornecedor.txtNome.value = ALLTRIM(lcNome)
	CCNRFornecedor.txtNo.value = lcNo
	CCNRFornecedor.txtEstab.value = lcEstab
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_fornecedores_cc_nreg <<lcNo>>, <<lcEstab>>, ''
	ENDTEXT 
	IF !uf_gerais_actgrelha("CCNRFornecedor.gridpesq", "uCrsPesqCCNRF", lcSql)
		uf_perguntalt_chama("ERRO AO LER A CONTA CORRENTE N�O REGULARIZADA DO FORNECEDOR!","OK","",16)
		RETURN .F.
	ENDIF
	
	uf_CCNRFornecedor_CalcularTotais()
	
	CCNRFornecedor.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqCCNRF"))) + " registos"
ENDFUNC



**
FUNCTION uf_CCNRFornecedor_carregamenu
	CCNRFornecedor.menu1.adicionaopcao("estab", "Inc. Estab.", myPath + "\imagens\icons\unchecked_w.png", "uf_CCNRFornecedor_incluirEstab","E")
	CCNRFornecedor.menu1.adicionaopcao("imprimir", "Imprimir", myPath + "\imagens\icons\imprimir_w.png", "uf_CCNRFornecedor_imprimir","I")
	CCNRFornecedor.menu1.adicionaopcao("navegar", "Ver Doc.", myPath + "\imagens\icons\doc_seta_w.png", "uf_CCNRFornecedor_Navegar","N")
	CCNRFornecedor.menu1.adicionaopcao("loja", "Loja Act.", myPath + "\imagens\icons\unchecked_w.png", "uf_CCNRFornecedor_lojaAct","L")
	
	CCNRFornecedor.menu1.estado("estab", "HIDE")
ENDFUNC



**
FUNCTION uf_CCNRFornecedor_incluirEstab
	IF CCNRFornecedor.menu1.estab.tag == 'false'
		&& aplica sele��o
		CCNRFornecedor.menu1.estab.tag = 'true'
		
		&& altera o botao
		CCNRFornecedor.menu1.estab.config("Inc. Estab.", myPath + "\imagens\icons\checked_w.png", "uf_CCNRFornecedor_incluirEstab","E")
	ELSE
		&& aplica sele��o
		CCNRFornecedor.menu1.estab.tag = 'false'
		
		&& altera o botao
		CCNRFornecedor.menu1.estab.config("Inc. Estab.", myPath + "\imagens\icons\unchecked_w.png", "uf_CCNRFornecedor_incluirEstab","E")
	ENDIF
	
	uf_CCNRFornecedor_Pesquisa()
ENDFUNC

**
FUNCTION uf_CCNRFornecedor_lojaAct
	IF CCNRFornecedor.menu1.loja.tag == 'false'
		&& aplica sele��o
		CCNRFornecedor.menu1.loja.tag = 'true'
		
		&& altera o botao
		CCNRFornecedor.menu1.loja.config("Loja Act.", myPath + "\imagens\icons\checked_w.png", "uf_CCNRFornecedor_lojaAct","L")
	ELSE
		&& aplica sele��o
		CCNRFornecedor.menu1.loja.tag = 'false'
		
		&& altera o botao
		CCNRFornecedor.menu1.loja.config("Loja Act.", myPath + "\imagens\icons\unchecked_w.png", "uf_CCNRFornecedor_lojaAct","L")
	ENDIF
	
	uf_CCNRFornecedor_Pesquisa()
ENDFUNC



**
FUNCTION uf_CCNRFornecedor_CalcularTotais
	LOCAL lcTotEdeb, lcTotEcred, lcTotEdebf, lcTotEcredf, lcSaldo, lcValor
	lcTotEdeb	=	0
	lcTotEcred	=	0
	lcTotEdebf	=	0
	lcTotEcredf	=	0
	lcSaldo 	=	0
	lcValor		=	0
	
	IF RECCOUNT("ucrsPesqCCNRF")>0
		Select ucrsPesqCCNRF
		GO TOP
		SCAN
			*******************************
			* Calcula o Total das colunas *
			*******************************
			**lcTotEdeb	=	lcTotEdeb + ucrsPesqCCNRF.edeb
			**lcTotEcred	=	lcTotEcred + ucrsPesqCCNRF.ecred
			
			lcTotEdeb	=	lcTotEdeb + (ucrsPesqCCNRF.edeb - ucrsPesqCCNRF.edebf)
			lcTotEcred	=	lcTotEcred + (ucrsPesqCCNRF.ecred - abs(ucrsPesqCCNRF.ecredf))
			
			
			lcValor		=	lcValor + ucrsPesqCCNRF.valor
			**lcSaldo 	= 	lcSaldo + ((ucrsPesqCCNRF.Ecred) - (ucrsPesqCCNRF.Edeb))
			
			**lcSaldo = lcSaldo  + ((ucrsPesqCCNRF.ecred-abs(ucrsPesqCCNRF.ecredf))- (ucrsPesqCCNRF.edeb-ucrsPesqCCNRF.edebf))
			**replace ucrsPesqCCNRF.Saldo WITH Round(lcSaldo,2)
		ENDSCAN
		
		CCNRFornecedor.txtValor.value = Round(lcValor,2)
		CCNRFornecedor.txtSaldo.value = Round(lcTotEcred - lcTotEdeb,2)
	ELSE
		CCNRFornecedor.txtValor.value = 0
		CCNRFornecedor.txtSaldo.value = 0
	ENDIF 
	
	Select ucrsPesqCCNRF
	GO TOP
	
	CCNRFornecedor.gridPesq.refresh
ENDFUNC 



**
FUNCTION uf_CCNRFornecedor_Pesquisa
	LOCAL lcSQL
	regua(0,0,"A pesquisar...")
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_fornecedores_cc_nreg <<CCNRFornecedor.txtNo.value>>, <<CCNRFornecedor.txtEstab.value>>,<<IIF(CCNRFornecedor.menu1.loja.tag=='true', "'"+ALLTRIM(mySite)+"'", "''")>>
	ENDTEXT
	uf_gerais_actGrelha("CCNRFornecedor.GridPesq", "ucrsPesqCCNRF", lcSQL)
		
	uf_CCNRFornecedor_CalcularTotais()
	
	regua(2)
	
	CCNRFornecedor.Refresh
	
	CCNRFornecedor.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("ucrsPesqCCNRF"))) + " registos"
ENDFUNC



**
FUNCTION uf_CCNRFornecedor_Imprimir
	IF RECCOUNT("ucrsPesqCCNRF") > 0
	
		**Trata parametros
		LOCAL lcNo, lcEstab
		lcNo 	= Iif(Empty(CCNRFornecedor.txtNo.value), 0, CCNRFornecedor.txtNo.value)
		lcEstab	= Iif(Empty(CCNRFornecedor.txtEstab.value), 0, CCNRFornecedor.txtEstab.value)

		** Construir string com parametros
		lcSql = "&No=" 			+ ALLTRIM(STR(lcNo)) +;
				"&estab=" 		+ ALLTRIM(STR(lcEstab)) +;
				"&site=" 		+ mySite 
*!*					+;
*!*					"&loja=" 		+ IIF(CCNRFornecedor.menu1.loja.tag=='true', ALLTRIM(mySite), "")
			
		** chamar report pelo nome dele
		uf_gerais_chamaReport("relatorio_nReg_Fornecedor", lcSql)
	ENDIF
ENDFUNC




**
FUNCTION uf_CCNRFornecedor_Navegar

	If myDocIntroducao == .t. OR myDocAlteracao == .t.
		uf_perguntalt_chama("O ECR� DE DOCUMENTOS ENCONTRA-SE EM MODO DE INTRODU��O OU EDI��O.","OK","",48)
	Else
		SELECT ucrsPesqCCNRF
		If !Empty(ucrsPesqCCNRF.Fcstamp)
			uf_documentos_chama(Alltrim(ucrsPesqCCNRF.Fcstamp))
		Endif
	Endif
ENDFUNC 




**
FUNCTION uf_CCNRFornecedor_sair
	**Fecha Cursores
	IF USED("ucrsPesqCCNRF")
		fecha("ucrsPesqCCNRF")
	ENDIF 
	
	CCNRFornecedor.hide
	CCNRFornecedor.release
ENDFUNC