
**
FUNCTION uf_ligacoes_controlaIntegridadeAll
	LOCAL lcSQL
	lcSQL = ""
	
	lcSQl = ""
	TEXT TO lcSQl NOSHOW TEXTMERGE
		exec up_ligacoes_verifyIntgLicControlAll
	ENDTEXT
	IF !uf_gerais_actgrelha("", "ucrsProgramasLt", lcSql)
		RETURN .f.
	ENDIF

	IF RECCOUNT("ucrsProgramasLt")> 0
		uf_perguntalt_chama("LICENCIAMENTO INV�LIDO OU CORROMPIDO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
	
	IF USED("ucrsProgramasLt")
		fecha("ucrsProgramasLt")
	ENDIF
	
	RETURN .t.
	
ENDFUNC 



FUNCTION uf_ligacoes_addConnection
	LPARAMETERS lcName
	Local lcSQl

	IF uf_ligacoes_controlaIntegridadeAll() == .f.
		RETURN .f.
	ENDIF

	lcSQl = ""
	TEXT TO lcSQl NOSHOW TEXTMERGE
		exec up_ligacoes_verifyControl '<<lcName>>'
	ENDTEXT
	IF !uf_gerais_actgrelha("", "ucrsVerifyControl", lcSql)
		RETURN .f.
	ENDIF
	
	SELECT ucrsVerifyControl
	IF ucrsVerifyControl.connections < ucrsVerifyControl.allowed AND ucrsVerifyControl.allowed != 0
		lcSQl = ""
		TEXT TO lcSQl NOSHOW TEXTMERGE
			exec up_ligacoes_addToControl '<<lcName>>'
		ENDTEXT
		IF !uf_gerais_actgrelha("", "", lcSql)
			RETURN .f.
		ENDIF
	ELSE
		uf_perguntalt_chama("N�O EXISTEM LICEN�AS DISPON�VEIS PARA ESTE M�DULO.","OK","",48)	
		RETURN .f.
	ENDIF
		
	RETURN .t.
ENDFUNC




FUNCTION uf_ligacoes_verificaConfiguracaoLicenca
	LPARAMETERS lcName
	Local lcSQl

	lcSQl = ""
	TEXT TO lcSQl NOSHOW TEXTMERGE
		exec up_ligacoes_verifyControl '<<lcName>>'
	ENDTEXT
	IF !uf_gerais_actgrelha("", "ucrsVerifyControl", lcSql)
		RETURN .f.
	ENDIF
	
	SELECT ucrsVerifyControl
	IF ucrsVerifyControl.allowed != 0
		RETURN .t. &&existem licen�as para este m�dulo
	ENDIF 
	
	RETURN .f. 
ENDFUNC

