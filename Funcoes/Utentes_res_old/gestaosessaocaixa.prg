FUNCTION uf_chamaGestaoSessaoCaixa
	&& Controla abertura do Painel
	if type("GestaoSessaoCaixa")=="U"
		DO FORM GestaoSessaoCaixa
	ELSE
		GestaoSessaoCaixa.show
	ENDIF
	
	&& Define o valor do Fundo de caixa
	IF EMPTY(myCxStamp)
		If u_sqlexec([exec up_parameters_Procura 'ADM0000000026'],[uCrsValidaFundoCaixa])
			If Reccount()>0
				GestaoSessaoCaixa.txtFundo.value = astr(Round(uCrsValidaFundoCaixa.numValue,2))
				If uCrsValidaFundoCaixa2.bool
					GestaoSessaoCaixa.txtFundo.readonly = .t.
				ENDIF
			ENDIF
			Fecha("uCrsValidaFundoCaixa")				
		ENDIF
	ELSE
		If u_sqlexec([select top 1 efundocx from fcx (nolock) where pno=]+alltrim(Str(myTermNo))+[ and operacao='A' order by ousrdata desc, ousrhora desc],[uCrsFcx])
			If Reccount()>0
				GestaoSessaoCaixa.txtFundo.value = alltrim(Str(uCrsFcx.efundocx,10,2))
			ENDIF
			fecha("uCrsFcx")
		ENDIF
	ENDIF	
ENDFUNC


**
FUNCTION uf_GestaoSessaoCaixa_sair
	GestaoSessaoCaixa.release
	RELEASE GestaoSessaoCaixa
ENDFUNC

***************************************************
** Cria Sess�es de caixa - (uf_gravarFechoCx) **
***************************************************
Function uf_GestaoSessaoCaixa_gravar
	Set Point To "."

	If empty(myCxStamp) && Se a Caixa estiver fechada, abrir caixa, sen�o fecha.
		uf_caixas_criarSessao(Round(Val(GestaoSessaoCaixa.txtFundo.value),2))
	ELSE
		uf_caixas_fecharCaixa(.t.)
	ENDIF
	
	uf_exitEcraCaixa()
ENDFUNC

