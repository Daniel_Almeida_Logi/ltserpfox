**
FUNCTION uf_ProdSelCLI_chama

	IF !USED("uCrsProdutosCLI")
		uf_gerais_actGrelha("","uCrsProdutosCLI","select top 0 ref, design, faminome, u_lab, usr1 from st (nolock)")
	ENDIF
	SELECT uCrsProdutosCLI
	GO TOP

	&& Controla Abertura do Painel
	if type("PRODSELCLI")=="U"
		DO FORM PRODSELCLI
	ELSE
		PRODSELCLI.show
	ENDIF
ENDFUNC

********************************
* Sair do ecra de Produtos CLI *
********************************
FUNCTION uf_prodselcli_sair

	** 
	PUBLIC MyMarcaAux
	STORE '' TO MyMarcaAux
	MyMarcaAux = ALLTRIM(ProdSelCLI.marca.value)
	
	PRODSELCLI.hide
	PRODSELCLI.release
	
ENDFUNC

***************************
* Pesquisa Especialidades *
***************************
FUNCTION uf_prodselcli_PesqProdutos
	SELECT uCrsProdutosCLI
	GO TOP
	
*!*		IF !EMPTY(PRODSELCLI.nome.value)
*!*			SET FILTER TO LIKE('*'+UPPER(ALLTRIM(PRODSELCLI.nome.value))+'*',UPPER(uCrsProdutosCLI.ref)) OR LIKE('*'+UPPER(ALLTRIM(PRODSELCLI.nome.value))+'*',UPPER(uCrsProdutosCLI.design))
*!*		ELSE
*!*			SET FILTER TO
*!*		ENDIF
	
	SELECT uCrsProdutosCLI
	GO TOP
	
	prodselcli.refresh
ENDFUNC 


** Elimina 
FUNCTION uf_prodselcli_Elimina
	LOCAL lcRef
	lcRef=UPPER(ALLTRIM(uCrsProdutosCLI.ref))

	IF uf_perguntalt_chama("TEM A CERTEZA QUE PRETENDE ELIMINAR A REFER�NCIA: " + lcRef + "?","Sim","N�o")
		&& actualizar grelha
		
		IF RECCOUNT("uCrsProdutosCLI") > 0
			DELETE FROM uCrsProdutosCLI WHERE uCrsProdutosCLI.ref=lcRef
		ENDIF
		

		**PRODSELCLI.nome.value = ""
	
		PRODSELCLI.menu1.actualizar.click()
	ENDIF 
ENDFUNC