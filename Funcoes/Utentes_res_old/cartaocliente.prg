*************************
*** CART�O DE CLIENTE ***
*************************
FUNCTION uf_cartaocliente_chama
	LPARAMETERS lcNo, lcEstab, lcPagina
		
	**Valida Licenciamento
	IF (uf_ligacoes_addConnection('CRM') == .f.) 
		RETURN .f.
	ENDIF
	
	
	**
	IF uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Cart�o de Cliente') == .t.
		uf_perguntalt_chama("O seu perfil n�o permite o acesso ao painel de CRM","OK","",48)
		RETURN .f.
	Endif 
	
	** Valida perfis
	IF uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Cart�o de Cliente - Cart�es') == .t.;
		AND uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Cart�o de Cliente - Vales') == .t.;
		AND uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Cart�o de Cliente - Regras') == .t.
		
		uf_perguntalt_chama("O seu perfil n�o permite o acesso ao painel de CRM","OK","",48)
		RETURN .f.
	ENDIF 	
	
	IF TYPE("PESQUTENTES") != "U"
		uf_pesqutentes_sair()
	ENDIF
		
	** Controla Abertura do Painel
	IF type("cartaocliente")=="U"

		&& Cursor de Utentes
		TEXT TO lcSql NOSHOW TEXTMERGE
			exec up_cartao_listarFidel 0, '', -1, 999, '',1
		ENDTEXT
		IF !uf_gerais_actgrelha("", "uCrsFidel", lcSql)
			uf_perguntalt_chama("OCORREU UM PROBLEMA A LISTAR CART�ES DE UTENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF

		lcSQL = ""

		TEXT TO lcSql NOSHOW TEXTMERGE
			SELECT TOP 0 sel = convert(bit,0),* FROM Campanhas WHERE Campanhas.eliminada = 0  
		ENDTEXT
		IF !uf_gerais_actGrelha("",[uCrsPesqCampanhas],lcSql)
			uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR A LISTAGEM DE REGRAS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
		
		&& Cursor de Vales Pendentes
		TEXT TO lcSql NOSHOW TEXTMERGE
			exec up_cartao_listarValesPendentes 0, 'xx', 0, 999, '',0,0
		ENDTEXT
		IF !uf_gerais_actgrelha("", "uCrsFidelValesPendentes", lcSql)
			uf_perguntalt_chama("OCORREU UM PROBLEMA A LISTAR OS VALES PENDENTES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
		
		
		DO FORM cartaocliente
		
		&&menu
		IF TYPE("cartaocliente.menu1.novo") == "U"
			
			cartaocliente.menu1.adicionaOpcao("opcoes", "Op��es", myPath + "\imagens\icons\opcoes_w.png", "","F1")
			cartaocliente.menu1.adicionaOpcao("actualizar","Actualizar",myPath + "\imagens\icons\actualizar_w.png","uf_cartaocliente_pesquisa", "F2")
			
			&&utentes com cart�o
			cartaocliente.menu1.adicionaOpcao("novo", "Novo", myPath + "\imagens\icons\mais_w.png", "uf_cartaocliente_novoCartao", "F3")			
			IF emdesenvolvimento == .t.
				cartaocliente.menu1.adicionaOpcao("corrigir", "Corrigir Todos", myPath + "\imagens\icons\mais_w.png", "uf_cartaocliente_corrigirCartoes", "F4")
			ENDIF
			cartaocliente.menu1.adicionaOpcao("editar", "Editar", myPath + "\imagens\icons\lapis_w.png", "uf_cartaocliente_editarCartao", "F4")
			cartaocliente.menu1.adicionaOpcao("eliminar", "Eliminar", myPath + "\imagens\icons\eliminar_w.png", "uf_cartaocliente_apagaFidel", "F5")
			cartaocliente.menu1.adicionaOpcao("etiquetacc", "Etiqueta CC", myPath + "\imagens\icons\imprimir_w.png", "uf_UTENTES_imprimirCartao with .t.","F6")
			cartaocliente.menu1.adicionaOpcao("imprimircc", "Listagem", myPath + "\imagens\icons\imprimir_w.png", "uf_cartaocliente_imprimeFidel with .t.", "F7")
			cartaocliente.menu1.adicionaOpcao("navegarcc", "Ficha", myPath + "\imagens\icons\detalhe.png", "uf_cartaocliente_verFichaClGcc with .t.", "F8")
			cartaocliente.menu1.adicionaOpcao("novaCampanha", "Novo", myPath + "\imagens\icons\mais_w.png", "uf_cartaocliente_novaCampanha", "F3")

			&&Vales Pendentes
			cartaocliente.menu1.adicionaOpcao("anular", "Anular", myPath + "\imagens\icons\cruz_w.png", "uf_cartaocliente_anulaVale", "F2")
			cartaocliente.menu1.adicionaOpcao("imprimirVale", "Imprimir Vale", myPath + "\imagens\icons\imprimir_w.png", "uf_cartaocliente_imprimirVale", "F2")
			
			&&utentes sem cart�o
			cartaocliente.menu1.adicionaOpcao("imprimirsc", "Imprimir", myPath + "\imagens\icons\imprimir_w.png", "uf_cartaocliente_imprimeFidel with .f.", "F2")
			cartaocliente.menu1.adicionaOpcao("navegarsc", "Ficha", myPath + "\imagens\icons\detalhe_micro.png", "uf_cartaocliente_verFichaClGcc with .f.", "F3")

			** Processar Campanhas
			cartaocliente.menu1.adicionaOpcao("processar","Processar",myPath + "\imagens\icons\ferramentas_w_lateral.png","uf_campanhas_actualizaCampanhasAtuais","")
		ENDIF
		uf_cartaocliente_alternamenu()
		
		cartaocliente.Menu1.gravar.img1.Picture = myPath + "\imagens\icons\guardar_w.png"
		
		** Menu do container editar
		cartaocliente.ctnDadosCartao.menu1.estado("", "", "Gravar", .t., "Sair", .t.)
		cartaocliente.ctnDadosCartao.menu1.funcaoSair = "uf_cartaocliente_ctnDadosCartaoFechar"
		cartaocliente.ctnDadosCartao.menu1.funcaoGravar = "uf_cartaocliente_ctnDadosCartaoGravar"
	ELSE
		cartaocliente.show
	ENDIF
	
	
	** Coloca-se na pagina onde tiver permiss�es
	DO CASE
		CASE !uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Cart�o de Cliente - Cart�es')
			cartaocliente.lbl1.click
		CASE !uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Cart�o de Cliente - Vales')
			cartaocliente.lbl2.click
		CASE !uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Cart�o de Cliente - Regras')
			cartaocliente.lbl3.click
	ENDCASE 

	&& Chamado o atendimento
	IF !EMPTY(lcPagina)
		
		DO CASE 
			CASE lcPagina == 1
				WITH cartaocliente.pageframe1.page1
					.no.value = lcNo
					.estab.value = lcEstab
				ENDWITH 
				cartaocliente.lbl1.click
			CASE lcPagina == 2
				WITH cartaocliente.pageframe1.page1
					.no.value = lcNo
					.estab.value = lcEstab
				ENDWITH 
				cartaocliente.lbl2.click
			CASE lcPagina == 3
				WITH cartaocliente.pageframe1.page1
					.no.value = lcNo
					.estab.value = lcEstab
				ENDWITH 
				cartaocliente.lbl3.click
		ENDCASE 	
		
	ENDIF 
	
ENDFUNC


**
FUNCTION uf_cartaocliente_ctnDadosCartaoFechar
	** Esconder o painel
	cartaocliente.ctnDadosCartao.visible = .f.
ENDFUNC


**
FUNCTION uf_cartaocliente_alternamenu
	DO CASE
		CASE cartaocliente.pageframe1.activepage == 1
			IF uf_gerais_validaPermUser(ch_userno,ch_grupo,'Alterar dados Cart�o')
				cartaocliente.menu1.estado("imprimirsc,navegarsc,anular, novaCampanha, processar, imprimirVale","HIDE")	
				cartaocliente.menu1.estado("actualizar,novo,editar,eliminar,imprimircc,etiquetacc,navegarcc","SHOW")
			ELSE
				cartaocliente.menu1.estado("imprimirsc,navegarsc,anular, editar, novaCampanha, processar, imprimirVale","HIDE")	
				cartaocliente.menu1.estado("actualizar,novo,eliminar,imprimircc,etiquetacc,navegarcc","SHOW")
			ENDIF 
			cartaocliente.menu1.estado("","","Gravar",.f.)

		CASE cartaocliente.pageframe1.activepage == 2

			cartaocliente.menu1.estado("novo,editar,eliminar,imprimircc,etiquetacc,navegarcc,imprimirsc,navegarsc, novaCampanha, processar","HIDE")
			cartaocliente.menu1.estado("actualizar,anular,imprimirVale ","SHOW")
			cartaocliente.menu1.estado("","","Gravar",.f.)
			
		CASE cartaocliente.pageframe1.activepage == 3

			cartaocliente.menu1.estado("novo,editar,eliminar,imprimircc,etiquetacc,navegarcc,anular, imprimirVale","HIDE")
			cartaocliente.menu1.estado("actualizar,novaCampanha, processar","SHOW")
			cartaocliente.menu1.estado("","","Gravar",.f.)
			
		OTHERWISE
			***
	ENDCASE
ENDFUNC



FUNCTION uf_cartaocliente_pesquisa
	LOCAL lcTop, lcCliente, lcNo, lcEstab, lcTipo
	STORE 0 TO lcTop, lcNo
	STORE "" TO lcCliente, lcTipo
	STORE 999 TO lcEstab
	
	lcObj = "cartaocliente.pageFrame1.Page" + ASTR(cartaocliente.pageFrame1.activePage)
	WITH &lcObj
	
		IF cartaocliente.pageFrame1.activePage != 3
	
			IF !EMPTY(ALLTRIM(.cliente.value))
				lcCliente = ALLTRIM(.cliente.value)
			ENDIF 
			&&No
			IF !EMPTY(astr(.no.value))
				lcNo = .no.value
			ENDIF 
			&&Estab
			IF !EMPTY(astr(.estab.value))
				lcEstab = .estab.value
			ENDIF 
			&&tipo
			IF !EMPTY(ALLTRIM(.tipo.value))
				lcTipo = ALLTRIM(.tipo.value)
			ENDIF 
			
			lcCartao = IIF(cartaocliente.pageFrame1.Page1.cartao.tag== "false",0,1)
		ENDIF 
	ENDWITH
	
	DO CASE
		CASE cartaocliente.pageframe1.activepage == 1
		
			
			TEXT TO lcSql NOSHOW TEXTMERGE
				exec up_cartao_listarFidel <<lcTop>>, '<<lcCliente>>', <<lcNo>>, <<lcEstab>>, '<<lcTipo>>',<<lcCartao>>
			ENDTEXT
			
			
			IF !uf_gerais_actgrelha("cartaocliente.pageframe1.page1.grdCartoes", "uCrsFidel", lcSql)
				uf_perguntalt_chama("OCORREU UM PROBLEMA A LISTAR CART�ES DE UTENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF
			cartaocliente.pageframe1.page1.grdCartoes.refresh
			
			cartaocliente.pageframe1.page1.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("uCrsFidel"))) + " Resultados"

		CASE cartaocliente.pageframe1.activepage == 2
		
			lcAbatidos = IIF(cartaocliente.pageFrame1.Page2.abatidos.tag== "false",0,1)
			lcAnulados = IIF(cartaocliente.pageFrame1.Page2.anulados.tag== "false",0,1)
			WITH cartaocliente.pageframe1.page2
				lcDataIni = uf_gerais_getdate(.dataIni.value,"SQL")
				lcDataFim = uf_gerais_getdate(.dataFim.value,"SQL")
			ENDWITH 
			Text To lcSql Noshow textmerge
				exec up_cartao_listarValesPendentes <<lcTop>>, '<<lcCliente>>', <<lcNo>>, <<lcEstab>>, '<<lcTipo>>',<<lcAbatidos>>,<<lcAnulados>>,'<<lcDataIni>>','<<lcDataFim>>'
			ENDTEXT
*!*	_cliptext = lcSQL
*!*	MESSAGEBOX(lcSQL)			
			IF !uf_gerais_actgrelha("cartaocliente.pageframe1.page2.grdVales", "uCrsFidelValesPendentes", lcSql)
				uf_perguntalt_chama("OCORREU UM PROBLEMA A LISTAR CART�ES DE UTENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF
			cartaocliente.pageframe1.page2.grdVales.refresh
			
			cartaocliente.pageframe1.page2.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("uCrsFidelValesPendentes"))) + " Resultados"
	
		CASE cartaocliente.pageframe1.activepage == 3
			
			WITH cartaocliente.pageframe1.page3
				lcNo = IIF(EMPTY(.no.value),0,.no.value)
				lcNome = .nome.value
				lcDataIni = uf_gerais_getdate(.dataIni.value,"SQL")
				lcDataFim = uf_gerais_getdate(.dataFim.value,"SQL")
			ENDWITH 
			
			** RETIRAR INATIVOS - Alterado em 04-12-2017 por JC para come�ar a filtar por local da promo��o
			
			lcInactivos	= IIF(&lcObj..chkinactivos.tag == "true", 1, 0)
			
			IF lcInactivos=0 then
						
				lcSQL = ""
				TEXT TO lcSql NOSHOW TEXTMERGE 
					SELECT 
						sel = convert(bit,0),* 
					FROM 
						Campanhas
					Where
						(Campanhas.id = <<lcNo>> or <<lcNo>> = 0)
						and Campanhas.descricao like '<<ALLTRIM(lcNome)>>%'
						and Campanhas.DataInicio >= '<<lcDataIni>>'
						and Campanhas.DataFim <= '<<lcDataFim>>'
						and Campanhas.eliminada = 0
						and Campanhas.inativo=0
						and campanhas.site='<<ALLTRIM(mysite)>>'
				ENDTEXT
			ELSE
				lcSQL = ""
				TEXT TO lcSql NOSHOW TEXTMERGE 
					SELECT 
						sel = convert(bit,0),* 
					FROM 
						Campanhas
					Where
						(Campanhas.id = <<lcNo>> or <<lcNo>> = 0)
						and Campanhas.descricao like '<<ALLTRIM(lcNome)>>%'
						and Campanhas.DataInicio >= '<<lcDataIni>>'
						and Campanhas.DataFim <= '<<lcDataFim>>'
						and Campanhas.eliminada = 0
						and campanhas.site='<<ALLTRIM(mysite)>>'
				ENDTEXT			
			ENDIF
			
			uf_gerais_actGrelha("cartaocliente.pageframe1.page3.GridPesq", "uCrsPesqCampanhas",lcSQL)
			
			cartaocliente.pageframe1.page3.GridPesq.Refresh

			cartaocliente.pageframe1.page3.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqCampanhas"))) + " Resultados"
			
		OTHERWISE
			***	
	ENDCASE 
ENDFUNC 


**
FUNCTION uf_cartaocliente_geraNumero
	LPARAMETERS lcManual
	
	LOCAL lcNCartaoCliente
	STORE '' TO lcNCartaoCliente

	
	If uf_gerais_getParameter("ADM0000000021","BOOL") OR lcManual == .t.
		If uf_gerais_actGrelha("","uCrsNrCartaoCl",[select isnull(MAX(convert(int,REPLACE(nrcartao,'CL',''))+1),'0000000') as nrcartao from	B_fidel (nolock)])
			If Reccount("uCrsNrCartaoCl")>0
				IF EMPTY(uCrsNrCartaoCl.nrcartao)
					lcNCartaoCliente = 'CL0000001'
				ELSE
					
					LOCAL lcNrCartaoUlt, lcNrCartao, lcNCartao2
					STORE '' TO lcNrCartao, lcNCartao2,lcNrCartaoUlt
					lcNCartaoCliente = 'CL' + astr(uCrsNrCartaoCl.nrcartao)
					
					IF LEN(lcNrCartao) < 10
						lcNCartao2 = RIGHT(lcNCartaoCliente , LEN(lcNCartaoCliente )-2)
						FOR i=1 TO 7-LEN(lcNCartao2)
							lcNCartao2 = '0' + lcNCartao2
						ENDFOR 
						lcNCartaoCliente = 'CL' + lcNCartao2 
					ENDIF 	
				ENDIF		 
			ENDIF
			Fecha("uCrsNrCartaoCl")
		ENDIF
	ELSE
		lcNCartaoCliente = ''	
	ENDIF

	
	RETURN lcNCartaoCliente
ENDFUNC 


**
FUNCTION uf_cartaocliente_editarCartao
	
	
	SELECT uCrsFidel
	lcClStamp = uCrsFidel.clstamp
	
	IF !(uf_gerais_validaPermUser(ch_userno,ch_grupo,'Cart�o de Cliente - Editar Dados'))
		uf_perguntalt_chama("N�O TEM PERMISS�ES PARA EDITAR OS DADOS DO CART�O DE CLIENTE.","OK","", 48)
		RETURN .f.
	ENDIF

	SELECT uCrsFidel
	IF EMPTY(ALLTRIM(uCrsFidel.nrcartao))
		uf_perguntalt_chama("O Cliente selecionado n�o tem cart�o associado. N�o � possivel editar.","OK","", 48)
		RETURN .f.
	ENDIF

	SELECT uCrsFidel
	IF uCrsFidel.inactivo==.f.
		&&sombra e container
		cartaocliente.ctnDadosCartao.visible = .t.
		cartaocliente.ctnDadosCartao.refresh()
		cartaocliente.ctnDadosCartao.lbl1.click()

	ELSE
		IF uf_perguntalt_chama("N�O PODE EDITAR UM CART�O INACTIVO."+CHR(10)+"PRETENDE ACTIVAR O CART�O?","Sim","N�o")
			SELECT uCrsfidel
			
			** actualizar tabela fidel **
			IF !uf_gerais_actgrelha("", "", [update B_fidel set inactivo=0 where clstamp=']+ALLTRIM(uCrsFidel.clstamp)+['])
				uf_perguntalt_chama("OCORREU UM PROBLEMA A ACTIVAR CART�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ELSE
				LOCAL lcStamp
				lcStamp = uf_gerais_stamp()
				
				** actualizar cursor **
				SELECT ucrsfidel
				replace uCrsFidel.inactivo WITH .f.
				***********************
				
				** registar ocorrencia **
				lcSQL = ''
				TEXT TO lcSql NOSHOW textmerge
					INSERT INTO B_ocorrencias(
						stamp, tipo, grau, descr,
						oValor, dValor,
						usr, date)
					values
						('<<lcStamp>>', 'Cart�o Cliente', 3, 'Cart�o <<ALLTRIM(uCrsFidel.nrcartao)>> Activado',
						'Cliente N� <<astr(uCrsFidel.clno)>>', '',
						<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()))
				ENDTEXT
				IF !uf_gerais_actgrelha("", "", lcSql)
					uf_perguntalt_chama("OCORREU UM PROBLEMA A REGISTAR OCORR�NCIA. POR FAVOR CONTACTE O SUPORTE.","OK","",48)
				ENDIF
				*************************
			ENDIF
		ENDIF
	ENDIF

ENDFUNC


** funcao utilizada para corrigir cartoes em bulk
** funcionalidade criada apenas para o administrador

FUNCTION uf_cartaocliente_corrigirCartoes
	LOCAL lcCount
	
	STORE 1 TO lcCount
	
		
	SELECT uCrsFidel
	GO TOP
	SCAN
		regua(1,lcCount,"A corrigir cartoes...")	
		uf_cartaocliente_apagaFidelCorrigir(.f.)
		uf_cartaocliente_novoCartaoCorrigir(.f.)
		lcCount= lcCount+ 1
	ENDSCAN
	

	REGUA(2)
	
	uf_cartaocliente_sair()

ENDFUNC



FUNCTION uf_cartaocliente_importarPontosVendasAnterioresCorrigir
	LPARAMETERS lcNo, lcEstab

	** 
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		Select 
			fi.*
		from 
			ft (nolock)
			inner join fi (nolock) on fi.ftstamp = ft.ftstamp 
		where 
			ft.no = <<lcNo>>
			and ft.estab = <<lcEstab>>
			and fi.partes = 0
	ENDTEXT
	

	IF !uf_gerais_actGrelha("","ucrsFiPontosVDAnt",lcSql)
		uf_perguntalt_chama("N�o foi possivel calcular hist�rico do utente.","OK","",16)
		RETURN .f.
	ENDIF
	
	IF RECCOUNT("ucrsFiPontosVDAnt") == 0
		RETURN .f.
	ENDIF
	

	uf_CARTAOCLIENTE_actualizaPontosClPag(.t.,.f.,.t.,lcNo,lcEstab)
	

	IF USED("ucrsFiPontosVDAnt")
		fecha("ucrsFiPontosVDAnt")
	ENDIF 
ENDFUNC 



** funcao utilizada para apagar cartoes em bulk
** funcionalidade criada apenas para o administrador
FUNCTION uf_cartaocliente_apagaFidelCorrigir

	LPARAMETERS tcBool
	
	Local lcClStamp
	
	SELECT uCrsFidel
	lcClStamp = uCrsFidel.clstamp
	
	SELECT uCrsFidel
	IF EMPTY(ALLTRIM(uCrsFidel.nrcartao))
		uf_perguntalt_chama("O Cliente selecionado n�o tem cart�o associado. N�o � possivel eliminar.","OK","", 48)
		RETURN .f.
	ENDIF
	
	
	SELECT uCrsFidel
	
	** apagar fidel **
	IF !uf_gerais_actgrelha("", "", [delete B_fidel where clstamp=']+ALLTRIM(uCrsFidel.clstamp)+['])
		uf_perguntalt_chama("OCORREU UM PROBLEMA A APAGAR CART�O DE CLIENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
	
	** update cl **
	IF !uf_gerais_actgrelha("", "", [update b_utentes set nrcartao='' where b_utentes.utstamp=']+ALLTRIM(uCrsFidel.clstamp)+['])
		uf_perguntalt_chama("OCORREU UM PROBLEMA A APAGAR CART�O DE CLIENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
	ENDIF
	
	

ENDFUNC


** funcao utilizada para criar cartoes em bulk
** funcionalidade criada apenas para o administrador
FUNCTION uf_cartaocliente_novoCartaoCorrigir
	LPARAMETERS lcAtendimento
	
	LOCAL lcNrCartao, lcClStamp


			
	If uf_gerais_actgrelha("", "uCrsNrCartaoCl", [select isnull(MAX(convert(int,REPLACE(nrcartao,'CL',''))+1),'0000000') as nrcartao from	B_fidel (nolock)])
		If Reccount("uCrsNrCartaoCl")>0
			IF EMPTY(uCrsNrCartaoCl.nrcartao)
				lcNrCartao = 'CL0000001'
			ELSE			
				LOCAL lcNrCartaoUlt, lcNrCartao, lcNCartao2
				STORE '' TO lcNrCartao, lcNCartao2,lcNrCartaoUlt
				lcNrCartao= 'CL' + astr(uCrsNrCartaoCl.nrcartao)
				
				IF LEN(lcNrCartao) < 10
					lcNCartao2 = RIGHT(lcNrCartao, LEN(lcNrCartao)-2)
					FOR i=1 TO 7-LEN(lcNCartao2)
						lcNCartao2 = '0' + lcNCartao2
					ENDFOR 
					lcNrCartao = 'CL' + lcNCartao2 
				ENDIF 
			ENDIF
		ENDIF
		Fecha("uCrsNrCartaoCl")
	ELSE
		uf_perguntalt_chama("OCORREU UM ERRO AO GERAR O N�MERO DO CART�O DE CLIENTE! POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
		RETURN .f.
	ENDIF
	
	IF EMPTY(lcNrCartao)
		lcNrCartao = 'CL0000000'
	ENDIF
	
	
	LOCAL lcStamp
	lcStamp = uf_gerais_stamp()
	
	IF EMPTY(lcAtendimento)
		** gravar altera��es **
		Select uCrsFidel
		
		TEXT TO lcSql Noshow textmerge
			Insert into 
				B_fidel (
					fstamp
					,clno
					,clestab
					,nrcartao
					,clstamp
					,pontosatrib
					,pontosusa
					,inactivo
					,atendimento
					,validade
					,ousrdata
					,usrdata
					,ousr
					,usr
			)Values (
				'<<lcStamp>>',
				<<uCrsFidel.clno>>
				,<<uCrsFidel.clestab>>
				,'<<Alltrim(lcNrCartao)>>'
				,'<<Alltrim(uCrsFidel.clstamp)>>'
				,0
				,0
				,0
				,0
				,convert(varchar,DATEADD(MONTH,<<myValidadeVale>>,GETDATE()),112)
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112)
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112)
				,<<ch_userno>>
				,<<ch_userno>>
			)
			
			UPDATE b_utentes SET nrcartao = '<<Alltrim(lcNrCartao)>>' WHERE utstamp = '<<Alltrim(uCrsFidel.clstamp)>>'
		ENDTEXT
	ELSE
		SELECT uCrsAtendCL
		
		Text To lcSql Noshow textmerge
			Insert into 
				B_fidel (
					fstamp
					,clno
					,clestab
					,nrcartao
					,clstamp
					,pontosatrib
					,pontosusa
					,inactivo
					,atendimento
					,validade
					,ousrdata
					,usrdata
					,ousr
					,usr
			)Values (
				'<<lcStamp>>',
				<<uCrsAtendCL.no>>
				,<<uCrsAtendCL.estab>>
				,'<<Alltrim(lcNrCartao)>>'
				,'<<Alltrim(uCrsAtendCL.utstamp)>>'
				,0
				,0
				,0
				,0
				,convert(varchar,DATEADD(MONTH,<<myValidadeVale>>,GETDATE()),112)
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112)
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112)
				,<<ch_userno>>
				,<<ch_userno>>
			)
			
			UPDATE b_utentes SET nrcartao = '<<Alltrim(lcNrCartao)>>' WHERE utstamp = '<<Alltrim(uCrsAtendCL.utstamp)>>'
		ENDTEXT
	ENDIF
	If !uf_gerais_actGrelha("","",lcSql)
		uf_perguntalt_chama("OCORREU UM PROBLEMA A CRIAR O CART�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
	ENDIF
	
	uf_cartaocliente_importarPontosVendasAnterioresCorrigir(uCrsFidel.clno,uCrsFidel.clestab)

ENDFUNC



** Cria Cart�o Cliente
FUNCTION uf_cartaocliente_novoCartao
	LPARAMETERS lcAtendimento
	
	LOCAL lcNrCartao, lcClStamp
	
	IF EMPTY(lcAtendimento)
		SELECT uCrsFidel
		lcClStamp = uCrsFidel.clstamp
	
		
		SELECT uCrsFidel
		IF !EMPTY(ALLTRIM(uCrsFidel.nrcartao))
			uf_perguntalt_chama("O utente selecionado j� tem cart�o associado.","OK","", 32)
			RETURN .f.
		ENDIF 
	ENDIF


			
	If uf_gerais_actgrelha("", "uCrsNrCartaoCl", [select isnull(MAX(convert(int,REPLACE(nrcartao,'CL',''))+1),'0000000') as nrcartao from	B_fidel (nolock)])
		If Reccount("uCrsNrCartaoCl")>0
			IF EMPTY(uCrsNrCartaoCl.nrcartao)
				lcNrCartao = 'CL0000001'
			ELSE			
				LOCAL lcNrCartaoUlt, lcNrCartao, lcNCartao2
				STORE '' TO lcNrCartao, lcNCartao2,lcNrCartaoUlt
				lcNrCartao= 'CL' + astr(uCrsNrCartaoCl.nrcartao)
				
				IF LEN(lcNrCartao) < 10
					lcNCartao2 = RIGHT(lcNrCartao, LEN(lcNrCartao)-2)
					FOR i=1 TO 7-LEN(lcNCartao2)
						lcNCartao2 = '0' + lcNCartao2
					ENDFOR 
					lcNrCartao = 'CL' + lcNCartao2 
				ENDIF 
			ENDIF
		ENDIF
		Fecha("uCrsNrCartaoCl")
	ELSE
		uf_perguntalt_chama("OCORREU UM ERRO AO GERAR O N�MERO DO CART�O DE CLIENTE! POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
		RETURN .f.
	ENDIF
	
	IF EMPTY(lcNrCartao)
		lcNrCartao = 'CL0000000'
	ENDIF
	
	LOCAL lcStamp
	lcStamp = uf_gerais_stamp()
	
	IF EMPTY(lcAtendimento)
		** gravar altera��es **
		Select uCrsFidel
		
		TEXT TO lcSql Noshow textmerge
			Insert into 
				B_fidel (
					fstamp
					,clno
					,clestab
					,nrcartao
					,clstamp
					,pontosatrib
					,pontosusa
					,inactivo
					,atendimento
					,validade
					,ousrdata
					,usrdata
					,ousr
					,usr
			)Values (
				'<<lcStamp>>',
				<<uCrsFidel.clno>>
				,<<uCrsFidel.clestab>>
				,'<<Alltrim(lcNrCartao)>>'
				,'<<Alltrim(uCrsFidel.clstamp)>>'
				,<<uCrsFidel.pontosAtrib>>
				,<<uCrsFidel.pontosAtrib-uCrsFidel.pontosusa>>
				,0
				,0
				,convert(varchar,DATEADD(MONTH,<<myValidadeVale>>,GETDATE()),112)
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112)
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112)
				,<<ch_userno>>
				,<<ch_userno>>
			)
			
			UPDATE b_utentes SET nrcartao = '<<Alltrim(lcNrCartao)>>' WHERE utstamp = '<<Alltrim(uCrsFidel.clstamp)>>'
		ENDTEXT
	ELSE
		SELECT uCrsAtendCL
		
		Text To lcSql Noshow textmerge
			Insert into 
				B_fidel (
					fstamp
					,clno
					,clestab
					,nrcartao
					,clstamp
					,pontosatrib
					,pontosusa
					,inactivo
					,atendimento
					,validade
					,ousrdata
					,usrdata
					,ousr
					,usr
			)Values (
				'<<lcStamp>>',
				<<uCrsAtendCL.no>>
				,<<uCrsAtendCL.estab>>
				,'<<Alltrim(lcNrCartao)>>'
				,'<<Alltrim(uCrsAtendCL.utstamp)>>'
				,0
				,0
				,0
				,0
				,convert(varchar,DATEADD(MONTH,<<myValidadeVale>>,GETDATE()),112)
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112)
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112)
				,<<ch_userno>>
				,<<ch_userno>>
			)
			
			UPDATE b_utentes SET nrcartao = '<<Alltrim(lcNrCartao)>>' WHERE utstamp = '<<Alltrim(uCrsAtendCL.utstamp)>>'
		ENDTEXT
	ENDIF
	If !uf_gerais_actGrelha("","",lcSql)
		uf_perguntalt_chama("OCORREU UM PROBLEMA A CRIAR O CART�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
	ENDIF

	IF EMPTY(lcAtendimento)
		uf_cartaocliente_pesquisa()
		
		SELECT uCrsFidel
		LOCATE FOR uCrsFidel.clstamp == lcClStamp
		
		cartaocliente.pageframe1.page1.grdCartoes.refresh
	ELSE
		**
		&&ATENDIMENTO.txtPontos.value = Alltrim(lcNrCartao)
		Atendimento.pgfDados.Page1.txtCliCartao.value = Alltrim(lcNrCartao)
	ENDIF
	
	**
	IF EMPTY(lcAtendimento)
		uf_cartaocliente_importarPontosVendasAnteriores(uCrsFidel.clno,uCrsFidel.clestab)
	ELSE
		uf_cartaocliente_importarPontosVendasAnteriores(uCrsAtendCL.no,uCrsAtendCL.estab)
	ENDIF 
	
ENDFUNC





**
FUNCTION uf_cartaocliente_apagaFidel
	LPARAMETERS tcBool
	
	Local lcClStamp
	
	SELECT uCrsFidel
	lcClStamp = uCrsFidel.clstamp
	
	SELECT uCrsFidel
	IF EMPTY(ALLTRIM(uCrsFidel.nrcartao))
		uf_perguntalt_chama("O Cliente selecionado n�o tem cart�o associado. N�o � possivel eliminar.","OK","", 48)
		RETURN .f.
	ENDIF
	
	IF uf_perguntalt_chama("TEM A CERTEZA QUE PRETENDE ELIMINAR ESTE CART�O DE UTENTE","Sim","N�o")
		SELECT uCrsFidel
		
		** apagar fidel **
		IF !uf_gerais_actgrelha("", "", [delete B_fidel where clstamp=']+ALLTRIM(uCrsFidel.clstamp)+['])
			uf_perguntalt_chama("OCORREU UM PROBLEMA A APAGAR CART�O DE CLIENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
		
		** update cl **
		IF !uf_gerais_actgrelha("", "", [update b_utentes set nrcartao='' where b_utentes.utstamp=']+ALLTRIM(uCrsFidel.clstamp)+['])
			uf_perguntalt_chama("OCORREU UM PROBLEMA A APAGAR CART�O DE CLIENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		ENDIF
		
		LOCAL lcStamp
		lcStamp = uf_gerais_stamp()
		
		** registo ocurrencia **
		TEXT TO lcSql NOSHOW textmerge
			INSERT INTO B_ocorrencias
				(stamp, tipo, grau, descr, oValor,
				usr, date)
			values
				('<<ALLTRIM(lcStamp)>>', 'Cart�o Cliente', 3, 'Cart�o '+'<<+ALLTRIM(uCrsFidel.nrcartao)>>'+' Eliminado',
				'Cliente N� <<astr(uCrsFidel.clno)>>',
				<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()))
		ENDTEXT
		IF !uf_gerais_actgrelha("", "", lcSql)
			***
		ENDIF
		
	ENDIF
	
	**
	uf_cartaocliente_pesquisa()
	
	SELECT uCrsFidel
	LOCATE FOR uCrsFidel.clstamp == lcClStamp
	cartaocliente.pageframe1.page1.grdCartoes.refresh
ENDFUNC


**
**
FUNCTION uf_cartaocliente_ctnDadosCartaoGravar
	IF cartaocliente.ctnDadosCartao.pageframe1.activePage == 1 && altera��o dos dados do cart�o
		** Verificar inconsist�ncias **
		**IF LEN(astr(cartaocliente.ctnDadosCartao.pageframe1.page1.txtNrCartao.value))<5
		**	uf_perguntalt_chama("O CART�O DE CLIENTE TEM DE SER COMPOSTO POR 5 N�MEROS.","OK","",48)
		**	RETURN .f.
		**ENDIF
		
*!*			IF cartaocliente.ctnDadosCartao.pageframe1.page1.txtPTotal.value < 0
*!*				IF ALLTRIM(myTipoCartao) == 'Pontos'
*!*					uf_perguntalt_chama("N�O PODE ATRIBUIR PONTOS NEGATIVOS AO CART�O DO CLIENTE.","OK","", 48)
*!*				ELSE
*!*					uf_perguntalt_chama("N�O PODE ATRIBUIR VALORES NEGATIVOS AO CART�O DO CLIENTE.","OK","", 48)
*!*				ENDIF 
*!*				RETURN .f.
*!*			ENDIF
		
		SELECT uCrsFidel
		**IF ASTR(cartaocliente.ctnDadosCartao.pageframe1.page1.txtNrCartao.value) == RIGHT(ALLTRIM(uCrsFidel.nrcartao),5) AND cartaocliente.ctnDadosCartao.pageframe1.page1.txtPTotal.value==uCrsFidel.pontosatrib AND cartaocliente.ctnDadosCartao.pageframe1.page1.chkInactivo.tag == "false"
		IF (cartaocliente.ctnDadosCartao.pageframe1.page1.txtNrCartao.value) == (ALLTRIM(uCrsFidel.nrcartao)) AND (cartaocliente.ctnDadosCartao.pageframe1.page1.txtPImput.value)=0 AND cartaocliente.ctnDadosCartao.pageframe1.page1.chkInactivo.tag == "false"
			uf_perguntalt_chama("N�O TEM NADA A ALTERAR.","OK","",64)
			RETURN .f.
		ENDIF
		
		IF uf_gerais_actGrelha("","uCrsValNrCartao",[SELECT CLNO, nrcartao from B_fidel (nolock) where nrcartao=']+(cartaocliente.ctnDadosCartao.pageframe1.page1.txtNrCartao.value)+[' and nrcartao!=']+ALLTRIM(uCrsFidel.nrcartao)+['])
			IF RECCOUNT("uCrsValNrCartao")>0
				uf_perguntalt_chama("J� EXISTE O CLIENTE N� "+ASTR(uCrsValNrCartao.clno)+" COM O MESMO N� DE CART�O ATRIBUIDO.","OK","",48)
				fecha("uCrsValNrCartao")
				RETURN .f.
			ENDIF
			fecha("uCrsValNrCartao")
		ENDIF
		*******************************
		
		** PEDIR VALIDA��O AO UTILIZADOR **
		IF !uf_perguntalt_chama("VAI PROCEDER �S ALTERA��ES DO CART�O PARA ESTE CLIENTE. PRETENDE CONTINUAR?","Sim","N�o")
			RETURN .f.
		ELSE
			IF cartaocliente.ctnDadosCartao.pageframe1.page1.chkInactivo.tag=="true"
				IF !uf_perguntalt_chama("ATEN��O: VAI INACTIVAR O CART�O DE CLIENTE. TEM A CERTEZA QUE PRETENDE CONTINUAR?","Sim","N�o")
					RETURN .f.
				ENDIF
			ENDIF
		ENDIF
		************************************
		
		** fazer as altera��es **
		** guardar pontos livres para recalculo
*!*			LOCAL lcLivres
*!*			lcLivres = 0
*!*			
*!*			SELECT ucrsfidel
*!*			IF uf_gerais_actGrelha("","uCrsPontosUsa",[select top 1 pontosusa from B_fidel (nolock) where nrcartao=']+ALLTRIM(uCrsFidel.nrcartao)+[' and clno=]+astr(uCrsFidel.clno)+[ and clestab=]+astr(uCrsFidel.clestab))
*!*				IF RECCOUNT("uCrsPontosUsa")>0
*!*					lcLivres = cartaocliente.ctnDadosCartao.pageframe1.page1.txtPTotal.value - uCrsPontosUsa.pontosusa
*!*				ENDIF
*!*				fecha("uCrsPontosUsa")
*!*			ENDIF
			*************************************
		SELECT uCrsFidel
		IF ALLTRIM(myTipoCartao) == 'Pontos'
			TEXT TO lcSql NOSHOW textmerge
				UPDATE B_fidel
				SET pontosatrib = pontosatrib + <<(cartaocliente.ctnDadosCartao.pageframe1.page1.txtPImput.value)>>,
					nrcartao = '<<cartaocliente.ctnDadosCartao.pageframe1.page1.txtNrCartao.value>>',
					inactivo = <<IIF(cartaocliente.ctnDadosCartao.pageframe1.page1.chkInactivo.tag=="true", 1, 0)>>
				WHERE B_fidel.nrcartao='<<uCrsFidel.nrcartao>>' and B_fidel.clno=<<uCrsFidel.clno>> and B_fidel.clestab=<<uCrsFidel.clestab>>
				
				UPDATE b_utentes
				SET b_utentes.nrcartao = '<<cartaocliente.ctnDadosCartao.pageframe1.page1.txtNrCartao.value>>'
				where b_utentes.no=<<uCrsFidel.clno>> and b_utentes.estab=<<uCrsFidel.clestab>>
			ENDTEXT
		ELSE
			TEXT TO lcSql NOSHOW textmerge
				UPDATE B_fidel
				SET valcartao = valcartao + <<(cartaocliente.ctnDadosCartao.pageframe1.page1.txtPImput.value)>>,
					valcartaohist = valcartaohist + <<(cartaocliente.ctnDadosCartao.pageframe1.page1.txtPImput.value)>>,
					nrcartao = '<<cartaocliente.ctnDadosCartao.pageframe1.page1.txtNrCartao.value>>',
					inactivo = <<IIF(cartaocliente.ctnDadosCartao.pageframe1.page1.chkInactivo.tag=="true", 1, 0)>>
				WHERE B_fidel.nrcartao='<<uCrsFidel.nrcartao>>' and B_fidel.clno=<<uCrsFidel.clno>> and B_fidel.clestab=<<uCrsFidel.clestab>>
				
				UPDATE b_utentes
				SET b_utentes.nrcartao = '<<cartaocliente.ctnDadosCartao.pageframe1.page1.txtNrCartao.value>>'
				where b_utentes.no=<<uCrsFidel.clno>> and b_utentes.estab=<<uCrsFidel.clestab>>
			ENDTEXT
		ENDIF 
		IF !uf_gerais_actGrelha("","",lcSql)
			uf_perguntalt_chama("OCORREU UM PROBLEMA A ACTUALIZAR O CART�O DO CLIENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ELSE
			LOCAL lcStamp
			lcStamp = uf_gerais_stamp()
			
			** registo ocorrencia **
			IF ALLTRIM(myTipoCartao) == 'Pontos'
				TEXT TO lcSql NOSHOW textmerge
					INSERT INTO B_ocorrencias(
						stamp
						,tipo
						,grau
						,descr
						,oValor
						,dValor
						,usr
						,date
						,linkStamp
						,nvalor
					)Values
						('<<ALLTRIM(lcStamp)>>'
						,'Cart�o Cliente'
						,2
						,'Cart�o '+'<<+ALLTRIM(uCrsFidel.nrcartao)>>'+' <<IIF(cartaocliente.ctnDadosCartao.pageframe1.page1.chkInactivo.tag=="true",'Desactivado','Alterado')>>'
						,'<<ALLTRIM(uCrsFidel.nrcartao)>> pts: <<astr(uCrsFidel.pontosatrib)>> livres: <<astr(uCrsFidel.pontosusa)>>'
						,'<<cartaocliente.ctnDadosCartao.pageframe1.page1.txtNrCartao.value>>' 
						,<<ch_userno>>
						,dateadd(HOUR, <<difhoraria>>, getdate())
						,'<<uCrsFidel.clstamp>>'
						,<<cartaocliente.ctnDadosCartao.pageframe1.page1.txtPImput.value>>
					)
				ENDTEXT
			ELSE
				TEXT TO lcSql NOSHOW textmerge
					INSERT INTO B_ocorrencias(
						stamp
						,tipo
						,grau
						,descr
						,oValor
						,dValor
						,usr
						,date
						,linkStamp
						,nvalor
					)Values
						('<<ALLTRIM(lcStamp)>>'
						,'Cart�o Cliente'
						,2
						,'Cart�o '+'<<+ALLTRIM(uCrsFidel.nrcartao)>>'+' <<IIF(cartaocliente.ctnDadosCartao.pageframe1.page1.chkInactivo.tag=="true",'Desactivado','Alterado')>>'
						,'<<ALLTRIM(uCrsFidel.nrcartao)>> valor: <<astr(uCrsFidel.pontosatrib)>> dispon�vel: <<astr(uCrsFidel.pontosusa)>>'
						,'<<cartaocliente.ctnDadosCartao.pageframe1.page1.txtNrCartao.value>>'
						,<<ch_userno>>
						,dateadd(HOUR, <<difhoraria>>, getdate())
						,'<<uCrsFidel.clstamp>>'
						,<<cartaocliente.ctnDadosCartao.pageframe1.page1.txtPImput.value>>
					)
				ENDTEXT

			ENDIF 
			IF !uf_gerais_actGrelha("","",lcSql)
				***
			ENDIF
		ENDIF
		*************************
		
		SELECT uCrsFidel
		replace uCrsFidel.pontosatrib	WITH uCrsFidel.pontosatrib + cartaocliente.ctnDadosCartao.pageframe1.page1.txtPImput.value
		replace uCrsFidel.nrcartao		WITH cartaocliente.ctnDadosCartao.pageframe1.page1.txtNrCartao.value
		replace uCrsFidel.pontosusa		WITH uCrsFidel.pontosusa + cartaocliente.ctnDadosCartao.pageframe1.page1.txtPImput.value
		replace uCrsFidel.inactivo		WITH IIF(cartaocliente.ctnDadosCartao.pageframe1.page1.chkInactivo.tag=="true", .t., .f.)
	ENDIF
	
	IF cartaocliente.ctnDadosCartao.pageframe1.activePage == 2 && transfer�ncia de pontos
		LOCAL lcSucesso, lcPos, lcClStampUt2
		local lcTotalOo, lcTotalOd, lcTotalDo, lcTotalDd, lcTotalValTr
		STORE .t. TO lcSucesso
		STORE 0 TO lcPos, lcTotalOo, lcTotalOd, lcTotalDo, lcTotalDd, lcTotalValTr
		STORE '' TO lcClStampUt2

		** varificar inconsist�ncias **
		IF EMPTY(cartaocliente.ctnDadosCartao.pageframe1.page2.txtNo.value)
			IF ALLTRIM(myTipoCartao) == 'Pontos'
				uf_perguntalt_chama("N�O ESCOLHEU O CLIENTE PARA TRANSFERIR OS PONTOS.","OK","",48)
			ELSE
				uf_perguntalt_chama("N�O ESCOLHEU O CLIENTE PARA TRANSFERIR OS VALORES EM CART�O.","OK","",48)
			ENDIF 
			RETURN .f.
		ELSE
			IF(cartaocliente.ctnDadosCartao.pageframe1.page2.txtNo.value == ucrsFidel.clno) AND (cartaocliente.ctnDadosCartao.pageframe1.page2.txtEstab.value == ucrsFidel.clestab)
				uf_perguntalt_chama("O CART�O DE ORIGEM N�O PODE SER O MESMO QUE O CART�O DE DESTINO.","OK","",48)
				RETURN .f.
			ENDIF
		ENDIF
		IF EMPTY(cartaocliente.ctnDadosCartao.pageframe1.page2.txtCartao.value)
			IF ALLTRIM(myTipoCartao) == 'Pontos'
				uf_perguntalt_chama("N�O PODE TRANSFERIR PONTOS PARA UM CLIENTE SEM CART�O.","OK","",48)
			ELSE
				uf_perguntalt_chama("N�O PODE TRANSFERIR VALORES PARA UM CLIENTE SEM CART�O.","OK","",48)
			ENDIF 
			RETURN .f.
		ENDIF
		IF cartaocliente.ctnDadosCartao.pageframe1.page2.chkInactivo.tag == "true"
			IF ALLTRIM(myTipoCartao) == 'Pontos'
				uf_perguntalt_chama("N�O PODE TRANSFERIR PONTOS PARA UM CLIENTE COM O CART�O INACTIVO.","OK","",48)
			ELSE
				uf_perguntalt_chama("N�O PODE TRANSFERIR VALORES PARA UM CLIENTE COM O CART�O INACTIVO.","OK","",48)
			ENDIF 
			RETURN .f.
		ENDIF
		IF uCrsFidel.pontosUsa < (cartaocliente.ctnDadosCartao.pageframe1.page2.transferPontos.value)
			IF ALLTRIM(myTipoCartao) == 'Pontos'
				uf_perguntalt_chama("O TOTAL DE PONTOS A TRANSFERIR N�O PODE SER SUPERIOR AO TOTAL DE PONTOS LIVRES DO CLIENTE ORIGINAL.","OK","",48)
			ELSE
				uf_perguntalt_chama("O TOTAL DO VALOR A TRANSFERIR N�O PODE SER SUPERIOR AO TOTAL DE VALOR LIVRE DO CLIENTE ORIGINAL.","OK","",48)
			ENDIF 
			RETURN .f.
		ENDIF
		IF !(ALLTRIM(cartaocliente.ctnDadosCartao.pageframe1.page2.txtTipo.value)==ALLTRIM(uCrsFidel.Tipo))
			IF ALLTRIM(myTipoCartao) == 'Pontos'
				IF !uf_perguntalt_chama("ATEN��O: VAI TRANSFERIR PONTOS DE UM CLIENTE ["+ALLTRIM(uCrsFidel.Tipo)+"] PARA UM CLIENTE ["+ALLTRIM(cartaocliente.ctnDadosCartao.pageframe1.page2.txtTipo.value)+"]. TEM A CERTEZA QUE PRETENDE CONTINUAR?","Sim","N�o")
					RETURN .f.
				ENDIF
			ELSE
				IF !uf_perguntalt_chama("ATEN��O: VAI TRANSFERIR VALORES DE UM CLIENTE ["+ALLTRIM(uCrsFidel.Tipo)+"] PARA UM CLIENTE ["+ALLTRIM(cartaocliente.ctnDadosCartao.pageframe1.page2.txtTipo.value)+"]. TEM A CERTEZA QUE PRETENDE CONTINUAR?","Sim","N�o")
					RETURN .f.
				ENDIF
			ENDIF 
		ENDIF
		*******************************
	
		** Alterar Dados do Painel **
			** guardar valores para registo de ocorrencia **
		lcTotalOo = uCrsFidel.pontosatrib
		lcTotalOd = uCrsFidel.pontosatrib - (cartaocliente.ctnDadosCartao.pageframe1.page2.transferPontos.value)
		IF ALLTRIM(myTipoCartao) == 'Pontos'
			lcTotalDo = INT(cartaocliente.ctnDadosCartao.pageframe1.page2.txtPTotal.value)
			lcTotalDd = INT(cartaocliente.ctnDadosCartao.pageframe1.page2.txtPtotal.value + (cartaocliente.ctnDadosCartao.pageframe1.page2.transferPontos.value))
		ELSE
			lcTotalDo = (cartaocliente.ctnDadosCartao.pageframe1.page2.txtPTotal.value)
			lcTotalDd = (cartaocliente.ctnDadosCartao.pageframe1.page2.txtPtotal.value + (cartaocliente.ctnDadosCartao.pageframe1.page2.transferPontos.value))		
		ENDIF 
		lcTotalValTr = (cartaocliente.ctnDadosCartao.pageframe1.page2.transferPontos.value)
			*************************************************
		cartaocliente.ctnDadosCartao.pageframe1.page2.txtPTotal.value = lcTotalDd
		IF ALLTRIM(myTipoCartao) == 'Pontos'
			cartaocliente.ctnDadosCartao.pageframe1.page2.txtPLivres.value	= INT(cartaocliente.ctnDadosCartao.pageframe1.page2.txtPLivres.value + (cartaocliente.ctnDadosCartao.pageframe1.page2.transferPontos.value))
		ELSE
			cartaocliente.ctnDadosCartao.pageframe1.page2.txtPLivres.value	= (cartaocliente.ctnDadosCartao.pageframe1.page2.txtPLivres.value + (cartaocliente.ctnDadosCartao.pageframe1.page2.transferPontos.value))
		ENDIF 
		cartaocliente.ctnDadosCartao.txtPTotal.value = lcTotalOd
		IF ALLTRIM(myTipoCartao) == 'Pontos'
			cartaocliente.ctnDadosCartao.txtPLivres.value	= INT(cartaocliente.ctnDadosCartao.txtPLivres.value - (cartaocliente.ctnDadosCartao.pageframe1.page2.transferPontos.value))
		ELSE
			cartaocliente.ctnDadosCartao.txtPLivres.value	= (cartaocliente.ctnDadosCartao.txtPLivres.value - (cartaocliente.ctnDadosCartao.pageframe1.page2.transferPontos.value))
		ENDIF 
		*****************************
	
		** Calcula Stamp do cliente para onde se v�o transferir pontos
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			SELECT utstamp FROM b_utentes (nolock) WHERE no = <<cartaocliente.ctnDadosCartao.pageframe1.page2.txtno.value>> and estab = <<cartaocliente.ctnDadosCartao.pageframe1.page2.txtestab.value>>
		ENDTEXT 
		IF !uf_gerais_actGrelha("","uCrsFidelStampUt2",lcSql)
			IF ALLTRIM(myTipoCartao) == 'Pontos'
				uf_perguntalt_chama("OCORREU UM PROBLEMA A TRANSFERIR OS PONTOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			ELSE
				uf_perguntalt_chama("OCORREU UM PROBLEMA A TRANSFERIR OS VALORES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			ENDIF 
		ENDIF
		SELECT uCrsFidelStampUt2 
		lcClStampUt2 = uCrsFidelStampUt2 .utstamp 
		IF USED("uCrsFidelStampUt2")
			fecha("uCrsFidelStampUt2")
		ENDIF 
		
		** TRANSFERIR PONTOS **
		SELECT uCrsFidel
		IF ALLTRIM(myTipoCartao) == 'Pontos'
			TEXT TO lcSql NOSHOW textmerge
				UPDATE B_fidel
				SET pontosatrib = <<cartaocliente.ctnDadosCartao.pageframe1.page2.txtptotal.value>>
				where clno = <<cartaocliente.ctnDadosCartao.pageframe1.page2.txtno.value>> and clestab = <<cartaocliente.ctnDadosCartao.pageframe1.page2.txtestab.value>>
				
				UPDATE B_fidel
				SET pontosatrib = <<uCrsFidel.pontosatrib>>
				where clno = <<uCrsFidel.clno>> and clestab = <<uCrsFidel.clestab>>
			ENDTEXT
		ELSE
			TEXT TO lcSql NOSHOW textmerge
				UPDATE B_fidel
				SET valcartao = valcartao - <<lcTotalValTr>>
				, valcartaohist = valcartaohist - <<lcTotalValTr>>
				where clno = <<uCrsFidel.clno>> and clestab = <<uCrsFidel.clestab>>
				
				UPDATE B_fidel
				SET valcartao = valcartao + <<lcTotalValTr>>
				, valcartaohist = valcartaohist + <<lcTotalValTr>>
				where clno = <<cartaocliente.ctnDadosCartao.pageframe1.page2.txtno.value>> and clestab = <<cartaocliente.ctnDadosCartao.pageframe1.page2.txtestab.value>>
			ENDTEXT
		ENDIF 
		IF !uf_gerais_actGrelha("","",lcSql)
			IF ALLTRIM(myTipoCartao) == 'Pontos'
				uf_perguntalt_chama("OCORREU UM PROBLEMA A TRANSFERIR OS PONTOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			ELSE
				uf_perguntalt_chama("OCORREU UM PROBLEMA A TRANSFERIR OS VALORES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			ENDIF 
			lcSucesso = .f.
		ENDIF
		***********************
		
		** actualizar cursor **
		SELECT uCrsFidel
		IF ALLTRIM(myTipoCartao) != 'Pontos'
			replace uCrsFidel.pontosatrib	WITH uCrsFidel.pontosatrib - lcTotalValTr
			replace uCrsFidel.pontosusa		WITH uCrsFidel.pontosusa - lcTotalValTr
		ENDIF 
		lcPos = RECNO("uCrsFidel")
		GO TOP 
		SCAN FOR uCrsFidel.clno==cartaocliente.ctnDadosCartao.pageframe1.page2.txtno.value and uCrsFidel.clestab==cartaocliente.ctnDadosCartao.pageframe1.page2.txtestab.value
			IF ALLTRIM(myTipoCartao) == 'Pontos'
				replace uCrsFidel.pontosatrib	WITH cartaocliente.ctnDadosCartao.pageframe1.page2.txtptotal.value
				replace uCrsFidel.pontosusa		WITH cartaocliente.ctnDadosCartao.pageframe1.page2.txtplivres.value
			ELSE
				replace uCrsFidel.pontosatrib	WITH uCrsFidel.pontosatrib + lcTotalValTr
				replace uCrsFidel.pontosusa		WITH uCrsFidel.pontosusa + lcTotalValTr
			ENDIF 
			SELECT uCrsFidel
		ENDSCAN
	
		SELECT uCrsFidel
		IF lcPos>0
			go lcPos
		ENDIF
		**********************
	
		IF lcSucesso
			LOCAl lcstamp, lcstamp2
			lcstamp		= uf_gerais_stamp()
			lcstamp2	= uf_gerais_stamp()
	
			IF ALLTRIM(myTipoCartao) == 'Pontos'		
				TEXT TO lcSql NOSHOW textmerge
					INSERT INTO B_ocorrencias(
						stamp
						,tipo
						,grau
						,descr
						,ovalor
						,dvalor
						,usr
						,date
						,linkStamp
						,nvalor
					)values(
						'<<ALLTRIM(lcstamp)>>'
						,'Cart�o Cliente'
						,3
						,'Transfer�ncia de Pontos'
						,'<<ALLTRIM(uCrsFidel.nrcartao)>>'+' Pts.:'+'<<astr(lcTotalOo)>>'
						,'<<ALLTRIM(uCrsFidel.nrcartao)>>'+' Pts.:'+'<<astr(lcTotalOd)>>'
						,<<ch_userno>>
						,dateadd(HOUR, <<difhoraria>>, getdate())
						,'<<uCrsFidel.clstamp>>'
						,<<lcTotalOd-lcTotalOo>>
					)
						
					INSERT INTO B_ocorrencias(
						stamp
						,tipo
						,grau
						,descr
						,ovalor
						,dvalor
						,usr
						,date
						,linkStamp
						,nvalor
					)values(
						'<<ALLTRIM(lcstamp2)>>'
						,'Cart�o Cliente'
						,3
						,'Transfer�ncia de Pontos'
						,'<<ALLTRIM(cartaocliente.ctnDadosCartao.pageframe1.page2.txtcartao.value)>>'+' Pts.:'+'<<astr(lcTotalDo)>>',
						'<<ALLTRIM(cartaocliente.ctnDadosCartao.pageframe1.page2.txtcartao.value)>>'+' Pts.:'+'<<astr(lcTotalDd)>>'
						,<<ch_userno>>
						,dateadd(HOUR, <<difhoraria>>, getdate())
						,'<<lcClStampUt2>>'
						,<<lcTotalDd-lcTotalDo>>
					)
				ENDTEXT
			ELSE
				TEXT TO lcSql NOSHOW textmerge
					INSERT INTO B_ocorrencias(
						stamp
						,tipo
						,grau
						,descr
						,ovalor
						,dvalor
						,usr
						,date
						,linkStamp
						,nvalor
					)values(
						'<<ALLTRIM(lcstamp)>>'
						,'Cart�o Cliente'
						,3
						,'Transfer�ncia de Valor em cart�o'
						,'<<ALLTRIM(uCrsFidel.nrcartao)>>'+' �:'+'<<astr(lcTotalOo)>>'
						,'<<ALLTRIM(uCrsFidel.nrcartao)>>'+' �:'+'<<astr(lcTotalOd)>>'
						,<<ch_userno>>
						,dateadd(HOUR, <<difhoraria>>, getdate())
						,'<<uCrsFidel.clstamp>>'
						,<<lcTotalOd-lcTotalOo>>
					)
						
					INSERT INTO B_ocorrencias(
						stamp
						,tipo
						,grau
						,descr
						,ovalor
						,dvalor
						,usr
						,date
						,linkStamp
						,nvalor
					)values(
						'<<ALLTRIM(lcstamp2)>>'
						,'Cart�o Cliente'
						,3
						,'Transfer�ncia de Valor em cart�o'
						,'<<ALLTRIM(cartaocliente.ctnDadosCartao.pageframe1.page2.txtcartao.value)>>'+' �:'+'<<astr(lcTotalDo)>>',
						'<<ALLTRIM(cartaocliente.ctnDadosCartao.pageframe1.page2.txtcartao.value)>>'+' �:'+'<<astr(lcTotalDd)>>'
						,<<ch_userno>>
						,dateadd(HOUR, <<difhoraria>>, getdate())
						,'<<lcClStampUt2>>'
						,<<lcTotalValTr>>
					)
				ENDTEXT
			
			ENDIF 
			IF !uf_gerais_actGrelha("","",lcSql)
				uf_perguntalt_chama("OCORREU UM PROBLEMA GUARDAR REGISTO DAS ALTERA��ES.","OK","",48)
			ENDIF
			
			IF ALLTRIM(myTipoCartao) == 'Pontos'
				uf_perguntalt_chama("PONTOS TRANSFERIDOS COM SUCESSO.","OK","",64)
			ELSE
				uf_perguntalt_chama("VALORES TRANSFERIDOS COM SUCESSO.","OK","",64)
			ENDIF 
		ENDIF
	ENDIF

	** SAIR DO PAINEL DE MANIPULA��O DE DADOS DO CART�O DE CLIENTE **
	uf_cartaocliente_ctnDadosCartaoFechar()
ENDFUNC


**
Function uf_cartaocliente_selClienteMDCC	
	uf_pesqUtentes_Chama("CARTAO_CLIENTE_MANIPULACAO")
Endfunc


**
FUNCTION uf_cartaocliente_verFichaClGcc
	LPARAMETERS tcBool
		
	IF tcBool
		Select uCrsFidel
		If !Empty(uCrsFidel.clstamp)
			uf_utentes_chama(uCrsFidel.clno,uCrsFidel.clEstab)
		ENDIF
	ELSE
		Select uCrsFidelClSemCartao
		If !Empty(uCrsFidelClSemCartao.utstamp)
			uf_utentes_chama(uCrsFidelClSemCartao.no,uCrsFidelClSemCartao.estab)
		ENDIF
	ENDIF
ENDFUNC


FUNCTION uf_cartaocliente_imprimeFidel
	LPARAMETERS tcBool
	
	SELECT uCrsE1
	
	IF tcBool
		Select uCrsFidel
		Report Form myPath + '\analises\ListagemCartoesCliente.frx' To Printer Prompt
	ELSE
		Select uCrsFidelClSemCartao
		Report Form myPath + '\analises\listagemclientesSemCartao.frx' To Printer Prompt
	ENDIF
	
	cartaocliente.backcolor=rgb(255,255,255)
ENDFUNC


***************************************
**
****************************************
FUNCTION uf_cartaocliente_verifyValesPenAtend
	IF myCartaoClienteLt
		SELECT ft
				
		IF !uf_gerais_actGrelha("","uCrsTemValPen",[exec up_cartao_aplicarValesPendentes ]+STR(ft.no)+[, ]+STR(ft.estab)+[])
			uf_perguntalt_chama("OCORREU UM ERRO A PROCURAR OS VALES DO CLIENTE.","OK","",16)
			RETURN .F.
		ELSE
			IF RECCOUNT("uCrsTemValPen")>0
				** eliminar vales que j� tenham sido aplicados � venda **
				SELECT uCrsTemValPen
				GO TOP
				SCAN
					SELECT fi
					SCAN FOR !EMPTY(fi.u_refvale)
						IF ALLTRIM(fi.u_refvale) == ALLTRIM(uCrsTemValPen.ref)
							SELECT uCrsTemValPen
							DELETE
						ENDIF
					
						SELECT fi
					ENDSCAN

					SELECT uCrsTemValPen
				ENDSCAN
				
				SELECT uCrsTemValPen
				GO TOP
				IF !EMPTY(uCrsTemValPen.ref)
					uf_valespendentes_chama()
				ENDIF
			ELSE
				fecha("uCrsTemValPen")
			ENDIF
		ENDIF
	ENDIF
ENDFUNC


** Actualiza Pontos do cliente
FUNCTION uf_CARTAOCLIENTE_actualizaPontosClPag
	LPARAMETERS tcNotAtend, lcNrAtendimento, lcImportarHistoricoPontos, lnNo, lnEstab 

	**
	uf_atendimento_CarregaCampanhasUt()
	
	LOCAL lcValorVenda, lcValidaUtente, lcValidaSt

	LOCAL lcSQL, lcTop, lcNome, lcNo, lcEstab, lcMorada, lcNcont, lcNCartao, lcTipo, lcProfissao, lcSexo, lcNascimento
	LOCAL lcTlmvl, lcOperador, lcIdade, lcObs, lcMail, lcEFR, lcPatologia, lcEntre, lcE, lcRef
	LOCAL lcInactivo, lctlf, lcutenteno, lcnbenef, lcValTouch, lcValEntidadeClinica,lcID
	STORE '' TO lcSQL, lcNome, lcMorada, lcNcont, lcNCartao, lcTipo, lcProfissao, lcSexo
	STORE '' TO lcObs, lcEFR, lcPatologia, lcRef, lctlf, lcutenteno, lcnbenef,lcID
	STORE 0 TO lcNo, lcTlmvl, lcMail, lcInactivo, lcValTouch, lcValEntidadeClinica
	STORE '19000101' TO lcNascimento, lcEntre
	
	IF EMPTY(lcImportarHistoricoPontos)
		SELECT ft
		lcFtNo = ft.no
		lcFtEstab = ft.estab
	ELSE
		lcFtNo = lnNo
		lcFtEstab = lnEstab 
	ENDIF
	
	&& Caso o establecimento n�o tenha cart�o emite os pontos no nome da sede
	IF lcFtEstab != 0
		IF USED("uCrsAtendCl")
			IF EMPTY(uCrsAtendCl.nrcartao)
				
				lcSQL = ''		
				TEXT TO lcSQL NOSHOW TEXTMERGE 
					SELECT nrcartao FROM b_utentes (nolock) WHERE b_utentes.no = <<lcFtno>> and estab = 0 
				ENDTEXT 
				If !uf_gerais_actGrelha("","ucrsCartaoClienteSede",lcSql)
					uf_perguntalt_chama("Ocorreu uma anomalia ao verificar defini��o das campanhas.","OK","",16)
					RETURN .f.
				ENDIF
				
				SELECT ucrsCartaoClienteSede
				IF !EMPTY(ucrsCartaoClienteSede.nrcartao)
					SELECT uCrsAtendCl
					Replace uCrsAtendCl.nrcartao WITH ucrsCartaoClienteSede.nrcartao
					
					lcFTEstab = 0
				ENDIF 
				
				IF USED("ucrsCartaoClienteSede")
					fecha("ucrsCartaoClienteSede")
				ENDIF 
			ENDIF 
		ENDIF 
	ENDIF 

	&& Valida se o cart�o de utente est� inactivo
	IF USED("uCrsAtendCl")
		lcSQL = ''		
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			SELECT inactivo FROM b_fidel (nolock) WHERE nrcartao = '<<ALLTRIM(uCrsAtendCl.nrcartao)>>'
		ENDTEXT 
	ELSE
		lcSQL = ''		
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			SELECT inactivo FROM b_fidel (nolock) WHERE nrcartao = '<<ALLTRIM(uCrsFidel.nrcartao)>>'	
		ENDTEXT 
	ENDIF
	IF !uf_gerais_actGrelha("","uCrsValidaCartao",lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia ao verificar a informa��o do cart�o do cliente","OK","",16)
		RETURN .f.
	ELSE 
		IF uCrsValidaCartao.inactivo == .t.
			fecha("uCrsValidaCartao")		
			RETURN .f.
		ELSE
			fecha("uCrsValidaCartao")
		ENDIF
	ENDIF
	
	** Defini��es ST
	IF !USED("ucrsCampanhasConfigSt")
		TEXT TO lcSql NOSHOW TEXTMERGE 
			exec up_campanhas_configST '<<ALLTRIM(mysite)>>'
		ENDTEXT 	
		If !uf_gerais_actGrelha("","ucrsCampanhasConfigSt",lcSql)
			uf_perguntalt_chama("Ocorreu uma anomalia ao verificar defini��o das campanhas. Por favor contacte o suporte.","OK","",16)
			RETURN .f.
		ENDIF
	ENDIF 
	
	LOCAL lcValLimiteCampanha
	STORE 0 TO lcValLimiteCampanha
	SELECT uCrsCampanhasList
	GO TOP 
	SCAN 
		lcValLimiteCampanha = uCrsCampanhasList.vallimite
		IF USED("uCrsCampanhasListaUt")
			Select no FROM uCrsCampanhasListaUt	where uCrsCampanhasListaUt.no = lcFtNo AND uCrsCampanhasListaUt.estab = lcFtEstab AND at("("+ALLTRIM(STR(ucrsCampanhasList.id))+")",uCrsCampanhasListaUt.campanhas)>0 INTO CURSOR ucrsValidaUtCampanha READWRITE 
		ELSE
			lcSQL = ''
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				Select 
					no 
				from
					campanhas_ut_lista (nolock)
				where 
					campanhas_ut_lista.no = <<lcFtNo>>
					and	campanhas_ut_lista.estab = <<lcFtEstab>>
					and campanhas like '%(<<ucrsCampanhasList.id>>)%'
			ENDTEXT 
			IF !uf_gerais_actGrelha("","ucrsValidaUtCampanha",lcSql)
				uf_perguntalt_chama("Ocorreu uma anomalia na valida��o das campanhas do utente. Por favor contacte o suporte.","OK","",48)
				RETURN .f.
			ENDIF
		ENDIF 
		
		IF RECCOUNT("ucrsValidaUtCampanha")==0
			lcValidaUtente = .f.
		ELSE
			lcValidaUtente = .t.
		ENDIF
		IF USED("ucrsValidaUtCampanha")
			fecha("ucrsValidaUtCampanha")
		ENDIF 		

		IF lcValidaUtente == .t.

			IF EMPTY(lcImportarHistoricoPontos)
				&& Se a Campanha n�o tiver linhas nas defini��es dos produtos � sempre valida
				IF !USED("uCrsFiPontos")
					IF lcValLimiteCampanha = 0 
						SELECT * FROM fi WHERE !EMPTY(fi.ref) AND EMPTY(ALLTRIM(fi.ofistamp)) INTO CURSOR ucrsFiPontos READWRITE
					ELSE
						SELECT * FROM fi WHERE !EMPTY(fi.ref) AND EMPTY(ALLTRIM(fi.ofistamp)) AND fi.u_epvp < lcValLimiteCampanha INTO CURSOR ucrsFiPontos READWRITE
					ENDIF 
				ENDIF 
			ENDIF
		
			SELECT ucrsCampanhasConfigSt
			LOCATE FOR ucrsCampanhasConfigSt.id == ucrsCampanhasList.id
			IF FOUND()
				lcQttAplicavel = ucrsCampanhasConfigSt.qt
			ELSE
				lcQttAplicavel = 1
			ENDIF 
			
			** 
			IF EMPTY(lcImportarHistoricoPontos)
				uf_ATCAMPANHAS_verificaLinhasCampanha(ucrsCampanhasList.id, uCrsCampanhasList.vallimite)
			ELSE
				
				SELECT * FROM ucrsFiPontosVDAnt INTO CURSOR ucrsFiPontos READWRITE
				uf_ATCAMPANHAS_verificaLinhasCampanha(ucrsCampanhasList.id, uCrsCampanhasList.vallimite, .t.)

			ENDIF 

			** Elimina Linhas de Devolu��o
			IF EMPTY(lcImportarHistoricoPontos)
				DELETE FROM ucrsFiPontos WHERE ucrsFiPontos.partes != 0
			ENDIF

			SELECT ucrsFiPontos
			CALCULATE SUM(ucrsFiPontos.qtt) TO lcQtProdVendas

			IF lcQtProdVendas >= lcQttAplicavel

				SELECT ucrsFiPontos && Valida criterio de aplica��o 
				COUNT TO lcRegitosFIPontos

				IF EMPTY(lcImportarHistoricoPontos)
					IF USED("uCrsAtendCl")
						IF !EMPTY(uCrsAtendCl.nrcartao)
							IF ucrsCampanhasList.pontos > 0 AND ucrsCampanhasList.valor > 0 AND lcRegitosFIPontos > 0
								uf_CARTAOCLIENTE_emitirPontos(lcFtNo, lcFtEstab, ucrsCampanhasList.id, ucrsCampanhasList.descricao)
							ENDIF
						
							IF ucrsCampanhasList.nat > 0 AND ucrsCampanhasList.pontosat > 0 AND lcRegitosFIPontos > 0
								**
								uf_CARTAOCLIENTE_emitirPontosAt(lcFtNo,lcFtEstab,ucrsCampanhasList.nat, ucrsCampanhasList.pontosat,ucrsCampanhasList.pordiaat, lcNrAtendimento,ucrsCampanhasList.id, ucrsCampanhasList.descricao)
							ENDIF 
							
							IF ucrsCampanhasList.nvd > 0 AND ucrsCampanhasList.pontosvd > 0 AND lcRegitosFIPontos > 0
								uf_CARTAOCLIENTE_emitirPontosVd(lcFtNo,lcFtEstab,ucrsCampanhasList.nvd, ucrsCampanhasList.pontosvd, lcNrAtendimento,ucrsCampanhasList.id, ucrsCampanhasList.descricao)
							ENDIF 
						ENDIF
					ENDIF 
				ELSE
					IF ucrsCampanhasList.pontos > 0 AND ucrsCampanhasList.valor > 0 AND lcRegitosFIPontos > 0
						uf_CARTAOCLIENTE_emitirPontos(lcFtNo, lcFtEstab, ucrsCampanhasList.id, ucrsCampanhasList.descricao)
					ENDIF
				ENDIF

				IF lcRegitosFIPontos > 0 AND EMPTY(lcImportarHistoricoPontos)
					uf_CARTAOCLIENTE_emitirVales(lcFtNo,lcFtEstab)
				ENDIF 
			
				IF ucrsCampanhasList.valorValeDireto > 0 AND lcRegitosFIPontos > 0 AND EMPTY(lcImportarHistoricoPontos)
					IF lcFtNo != 200
						uf_CARTAOCLIENTE_emitirValesDirectos(lcFtNo,lcFtEstab, ucrsCampanhasList.descricao, ucrsCampanhasList.valorValeDireto, ucrsCampanhasList.mensagemVale)
					ENDIF 
				ENDIF 						
			ENDIF 
		ENDIF 
	
		&& Campanha de vales emitidos por %% de valor atendimento	
		IF ucrsCampanhasList.descValorAtPerc > 0 AND EMPTY(lcImportarHistoricoPontos)
			uf_CARTAOCLIENTE_emitirValesdescValorAtPerc(lcFtNo, lcFtEstab, ucrsCampanhasList.descricao, ucrsCampanhasList.descValorAtPerc, ucrsCampanhasList.ValorValesDistribuir, ucrsCampanhasList.remanescente, ucrsCampanhasList.mensagemVale, ucrsCampanhasList.mensagemValeRem, ucrsCampanhasList.id)
		ENDIF 

	ENDSCAN 
ENDFUNC 


**
FUNCTION uf_CARTAOCLIENTE_Mensagem
	LPARAMETERS lcMensagem
	
	uf_perguntalt_chama(ALLTRIM(lcMensagem),"OK","",48)

ENDFUNC 



**
FUNCTION uf_CARTAOCLIENTE_emitirPontosAt
	LPARAMETERS lcFtNo, lcFtEstab, lcNat, lcPontosAt, lcPordiaat, lnNrAtendimento, lcCampanhaID, lcCampanhaDescr

	local lcNrAtendimento, lcCampanhas
	 
	lcNrAtendimento = IIF(EMPTY(lnNrAtendimento),"",lnNrAtendimento)
	lccampanhas = ""

		
	IF !EMPTY(lcPordiaat) && verifica se ja registou pontos no dia 
		TEXT TO lcSql noshow textmerge
			SELECT 
				pontos = isnull(sum(pontosAt),0)
			from 
				b_pagcentral (nolock)
			WHERE 
				convert(date,b_pagcentral.odata) = convert(date,dateadd(HOUR, <<difhoraria>>, getdate())) 
				AND b_pagcentral.no = <<lcFtNo>> 
				and b_pagcentral.estab = <<lcFtEstab>>
		ENDTEXT

		IF !uf_gerais_actGrelha("","ucrsControlaPontoPorDiaAt",lcSql)
			IF ALLTRIM(myTipoCartao) == 'Pontos'
				uf_perguntalt_chama("N�O FOI POSS�VEL GUARDAR OS PONTOS NO CART�O DESSE CLIENTE.","OK","",48)
			ELSE
				uf_perguntalt_chama("N�O FOI POSS�VEL GUARDAR OS VALORES NO CART�O DESSE CLIENTE.","OK","",48)
			ENDIF 
			RETURN .f.
		ENDIF
		
		IF ucrsControlaPontoPorDiaAt.pontos != 0
			
			IF USED("ucrsControlaPontoPorDiaAt")
				fecha("ucrsControlaPontoPorDiaAt")
			ENDIF 
			
			RETURN .f.
		ENDIF 
		
	ENDIF 

	lcCampanhas = ASTR(lcCampanhaId) + "-(" + STRTRAN(ALLTRIM(lcCampanhaDescr),',','')+ ")"

	TEXT TO lcSql noshow textmerge
		UPDATE 
			B_fidel
		SET 
			pontosatrib = pontosatrib + case when nat+1 >= <<lcNat>> then <<lcPontosAt>> else 0 end
			,nat = case when nat+1 >= <<lcNat>> then 0 else nat+1 end
		where 
			B_fidel.clno = <<lcFtNo>> 
			and B_fidel.clestab = <<lcFtEstab>>
			
		UPDATE 
			b_pagcentral 
		SET 
			pontosAt = pontosAt + <<lcPontosAt>>
			,campanhas = Case when campanhas = '' then '<<ALLTRIM(lcCampanhas)>>' else campanhas + ',' +  '<<ALLTRIM(lcCampanhas)>>' end 
		where 
			b_pagcentral.nrAtend = '<<ALLTRIM(lcNrAtendimento)>>'
	ENDTEXT

	IF !uf_gerais_actGrelha("","",lcSql)
		IF ALLTRIM(myTipoCartao) == 'Pontos'
			uf_perguntalt_chama("N�O FOI POSS�VEL GUARDAR OS PONTOS NO CART�O DESSE CLIENTE.","OK","",48)
		ELSE
			uf_perguntalt_chama("N�O FOI POSS�VEL GUARDAR OS VALORES NO CART�O DESSE CLIENTE.","OK","",48)
		ENDIF
	ENDIF
	
ENDFUNC 


**
FUNCTION uf_CARTAOCLIENTE_emitirPontosVd
	LPARAMETERS lcFtNo, lcFtEstab, lcNvd, lcPontosVd, lnNrAtendimento, lcCampanhaID, lcCampanhaDescr
	
	local lcNrAtendimento, lcCampanhas
	
	lcNrAtendimento = IIF(EMPTY(lnNrAtendimento),"",lnNrAtendimento)
	lccampanhas = "" 
		
	LOCAL lcNrVendas 
	lcNrVendas = 0
	
	SELECT uCrsCabVendas
	COUNT TO lcNrVendas 
		
	lcSQL = ''	
	TEXT TO lcSql noshow textmerge
		UPDATE 
			B_fidel
		SET 
			pontosatrib = pontosatrib + case when nvd+<<lcNrVendas>> >= <<lcNvd>> then <<lcPontosVd*lcNrVendas>> else 0 end
			,nvd = case when nvd+<<lcNrVendas>> >= <<lcNvd>> then nvd+<<lcNrVendas>>-<<lcNvd>> else nvd+<<lcNrVendas>> end
		where 
			B_fidel.clno = <<lcFtNo>> 
			and B_fidel.clestab = <<lcFtEstab>>
	ENDTEXT

	IF !uf_gerais_actGrelha("","",lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL GUARDAR OS PONTOS NO CART�O DESSE CLIENTE.","OK","",48)
	ENDIF
	
	** Determina Vendas em fun��o do numero de atendimento
	TEXT TO lcSql noshow textmerge
		SELECT distinct ftstamp FROM ft (nolock) where ft.u_nratend = '<<ALLTRIM(lcNrAtendimento)>>'
	ENDTEXT

	IF !uf_gerais_actGrelha("","ucrsVendasPTVD",lcSql)
		IF ALLTRIM(myTipoCartao) == 'Pontos'
			uf_perguntalt_chama("N�O FOI POSS�VEL GUARDAR OS PONTOS NO CART�O DESSE CLIENTE.","OK","",48)
		ELSE
			uf_perguntalt_chama("N�O FOI POSS�VEL GUARDAR OS VALORES NO CART�O DESSE CLIENTE.","OK","",48)
		ENDIF 
	ENDIF
	
	lcCampanhas = ASTR(lcCampanhaId) + "-(" + STRTRAN(ALLTRIM(lcCampanhaDescr),',','')+ ")"

	SELECT ucrsVendasPTVD
	GO TOP
	SCAN
		TEXT TO lcSql NOSHOW TEXTMERGE 
			UPDATE 
				ft 
			SET 
				ft.pontosVD = ft.pontosVD + <<lcPontosVd>> 
				,campanhas = Case when campanhas = '' then '<<ALLTRIM(lcCampanhas)>>' else campanhas + ',' +  '<<ALLTRIM(lcCampanhas)>>' end 
			where 
				ft.ftstamp = '<<ucrsVendasPTVD.ftstamp>>'
		ENDTEXT

*!*	_cliptext = lcSQL
*!*	MESSAGEBOX(lcSQL)

		IF !uf_gerais_actGrelha("","ucrsVendasPTVD",lcSql)
			IF ALLTRIM(myTipoCartao) == 'Pontos'
				uf_perguntalt_chama("N�O FOI POSS�VEL GUARDAR OS PONTOS NO CART�O DESSE CLIENTE.","OK","",48)
			ELSE
				uf_perguntalt_chama("N�O FOI POSS�VEL GUARDAR OS VALORES NO CART�O DESSE CLIENTE.","OK","",48)
			ENDIF 
		ENDIF
	ENDSCAN
	
	IF USED("ucrsVendasPTVD")
		fecha("ucrsVendasPTVD")
	ENDIF 	
ENDFUNC 


** Verifica se os pontos atingidos pelo utente permite a emiss�o de vales de desconto
FUNCTION uf_CARTAOCLIENTE_emitirPontos
	LPARAMETERS lcFtNo, lcFtEstab, lcCampanhaID, lcCampanhaDescr
	
	PUBLIC myTotalPagCc && Variavel usada para imprimir pontos gerados no talao de venda
	LOCAL lcValorVenda, lcTotalPontosDistribuidos, lcPontosAtribuir, lcPontosEmFalta, lcExecuteSQL, lcCampanhas
	STORE 0 TO myTotalPagCc, lcValorVenda, lcTotalPontosDistribuidos, lcPontosAtribuir, lcPontosEmFalta
	STORE "" TO lcExecuteSQL, lcCampanhas
		
	SELECT ucrsFiPontos
	GO Top
	SELECT ucrsFiPontos
	CALCULATE SUM(ucrsFiPontos.etiliquido) TO lcValorVenda

	** Arredondamento
	IF UPPER(ALLTRIM(myMetodoConversao))=="ARREDONDAMENTO"
		lcValorVenda  = round(lcValorVenda ,0)
	ELSE
		lcValorVenda = INT(lcValorVenda)
	ENDIF
	*******************
		
	SELECT ucrsCampanhasList
	lcCampanhas = ASTR(lcCampanhaId) + "-(" + STRTRAN(ALLTRIM(lcCampanhaDescr),',','')+ ")"
	lcPontos = ucrsCampanhasList.pontos
	lcValor = ucrsCampanhasList.valor

	IF !empty(lcPontos) and !empty(lcValor)

		IF lcValor > 0
			lcPontosAtribuir = INT((lcValorVenda * lcPontos)/lcValor)
		ELSE
			lcPontosAtribuir = 0
			RETURN .f.
		ENDIF 

		myTotalPagCc = lcPontosAtribuir
		wait window 'Pontos Atribuidos: ' + ASTR(lcPontosAtribuir)  timeout 1
		
		IF lcPontosAtribuir = 0
			RETURN .f.
		ENDIF 

		TEXT TO lcSql noshow textmerge
			UPDATE 
				B_fidel
			SET 
				pontosatrib = pontosatrib + <<lcPontosAtribuir>>
				,atendimento = (case when <<lcValorVenda>> > 0 then atendimento + 1 else atendimento end)
			where 
				B_fidel.clno = <<lcFtNo>> 
				and B_fidel.clestab = <<lcFtEstab>>
		ENDTEXT

		IF !uf_gerais_actGrelha("","UcrsPontosApos",lcSql)
			IF ALLTRIM(myTipoCartao) == 'Pontos'
				uf_perguntalt_chama("N�O FOI POSS�VEL GUARDAR OS PONTOS NO CART�O DESSE CLIENTE.","OK","",48)
			ELSE
				uf_perguntalt_chama("N�O FOI POSS�VEL GUARDAR OS VALORES NO CART�O DESSE CLIENTE.","OK","",48)
			ENDIF 
		ENDIF
	
		** Distribui Pontos pela Venda
		SELECT ucrsFiPontos
		GO Top
		SCAN 
			Replace ucrsFiPontos.pontos WITH INT((ucrsFiPontos.etiliquido*lcPontosAtribuir)/lcValorVenda)
			lcTotalPontosDistribuidos = lcTotalPontosDistribuidos  + INT((ucrsFiPontos.etiliquido*lcPontosAtribuir)/lcValorVenda)
		ENDSCAN 
	
		** Verifica se distribuiu todos os pontos
		IF lcTotalPontosDistribuidos != lcPontosAtribuir
			lcPontosEmFalta = lcPontosAtribuir - lcTotalPontosDistribuidos 
			SELECT ucrsFiPontos
			GO Top
			LOCATE FOR !EMPTY(ucrsFiPontos.pontos)
			IF FOUND()
				Replace ucrsFiPontos.pontos WITH ucrsFiPontos.pontos+lcPontosEmFalta 
			ENDIF
		ENDIF
		
		SELECT ucrsFiPontos
		GO Top
		SCAN FOR !EMPTY(ucrsFiPontos.pontos) 
			
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				UPDATE 
					fi 
				SET 
					fi.pontos = fi.pontos + <<ucrsFiPontos.pontos>> 
					,campanhas = Case when campanhas = '' then '<<ALLTRIM(lcCampanhas)>>' else  rtrim(ltrim(campanhas)) + ',' +  '<<ALLTRIM(lcCampanhas)>>' end 
				where 
					fi.fistamp = '<<ALLTRIM(ucrsFiPontos.fistamp)>>'
					
			ENDTEXT 
			lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
		ENDSCAN 
		lcExecuteSQL  = uf_gerais_trataPlicasSQL(lcExecuteSQL)

		
		IF !EMPTY(lcExecuteSQL) 

			lcSql = ''
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_gerais_execSql 'Update pontos fi', 1,'<<lcExecuteSQL >>', '', '', '' , '', ''
			ENDTEXT 
								
			If !uf_gerais_actGrelha("","",lcSQL)
				uf_perguntalt_chama("OCORREU UM ERRO A REGISTAR PONTOS NAS LINHAS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .f.
			ENDIF
		ENDIF
	ENDIF 
ENDFUNC 


**
FUNCTION uf_CARTAOCLIENTE_eliminarPontosNC
	LPARAMETERS lcFtstamp, lcFtNo, lcFtEstab
	
	&& Caso o establecimento n�o tenha cart�o emite os pontos no nome da sede
	IF lcFtEstab != 0
		IF USED("uCrsAtendCl")
			IF EMPTY(uCrsAtendCl.nrcartao)		
				lcFTEstab = 0
			ENDIF 
		ENDIF 
	ENDIF 
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		declare @pontosRetirar as numeric(9,0)
		set @pontosRetirar = isnull((Select SUM(pontos) from fi (nolock) where ftstamp = '<<lcFtstamp>>'),0)

		IF @pontosRetirar != 0
		BEGIN	
			update b_fidel set pontosatrib = CASE WHEN pontosatrib + @pontosRetirar < 0 then 0 else pontosatrib + @pontosRetirar end where B_fidel.clno = <<lcFtNo>> and B_fidel.clestab = <<lcFtEstab>>
		END
	ENDTEXT 
	
*!*	_cliptext = lcSQL
*!*	messagebox(lcSQL)

	If !uf_gerais_actGrelha("","",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR PONTOS NO CART�O DO CLIENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .f.
	ENDIF
ENDFUNC 


** Verifica se os pontos atingidos pelo utente permite a emiss�o de vales de desconto
FUNCTION uf_CARTAOCLIENTE_emitirVales
	LPARAMETERS lcFtNo, lcFtEstab
	
	LOCAL lcValidaUtente 
	lcValidaUtente = .f.
		
	TEXT TO lcSql NOSHOW TEXTMERGE 
		SELECT
			b_utentes.utstamp
			,b_utentes.nome
			,pontosatrib - pontosUsa as pontos 
		from 
			b_fidel 
			inner join b_utentes on b_fidel.clno = b_utentes.no and b_fidel.clestab = b_utentes.estab
		where 
			B_fidel.clno = <<lcFtNo>> 
			and B_fidel.clestab = <<lcFtEstab>>
	ENDTEXT 	
	If !uf_gerais_actGrelha("","ucrsPontosActuais",lcSql)
		uf_perguntalt_chama("Ocorreu um problema ao verificar regras para aplicar pontos ao utente.","OK","",16)
		RETURN .f.
	ENDIF
	**
			
	SELECT ucrsPontosActuais
	lcPontosActuais = ucrsPontosActuais.pontos
	lcNome = ucrsPontosActuais.nome
	lcUtstamp = ucrsPontosActuais.utstamp
	
	IF lcPontosActuais > 0
	
		** Niveis de pontos para emitir vale de desconto (Rebatimento)
		TEXT TO lcSql NOSHOW TEXTMERGE 
			exec up_campanhas_rebatimentos_PontosVales '<<ALLTRIM(mysite)>>'
		ENDTEXT 	
		If !uf_gerais_actGrelha("","ucrsRebatimentosPontosVales",lcSql)
			uf_perguntalt_chama("Ocorreu um problema ao verificar regras para aplicar pontos ao utente.","OK","",16)
			RETURN .f.
		ENDIF


		SELECT ucrsRebatimentosPontosVales
		GO Top
		SCAN FOR ucrsCampanhasList.id == ucrsRebatimentosPontosVales.campanhas_id

			**
			IF ucrsRebatimentosPontosVales.pontos <= lcPontosActuais
				LOCAL lcsimbmoeda
				IF EMPTY(uf_gerais_getParameter("ADM0000000260","text")) or UPPER(ALLTRIM(uf_gerais_getParameter("ADM0000000260","text"))) == "EURO"
					lcsimbmoeda=  " EUROS"
				ELSE
					lcsimbmoeda = ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
				ENDIF 
				

				IF uf_perguntalt_chama("ATEN��O: ESTE CLIENTE ATINGIU OS "+ASTR(lcPontosActuais)+" PONTOS. DESEJA IMPRIMIR UM VALE NO VALOR DE: "+ASTR(ucrsRebatimentosPontosVales.valor,8,2)+" " + ALLTRIM(lcsimbmoeda) + " ?","Sim","N�o")
					usacampanha=1
					select uCrsE1
					
					*** REGISTAR VALE ***
					LOCAL nrVale, refVale
					STORE 0 TO nrVale
					STORE "" TO refVale
					
					IF uf_gerais_actGrelha("","uCrsNrVale",[select ISNULL(MAX(nr),0) as nr from B_fidelvale (nolock)])
						IF RECCOUNT()>0
							nrVale = uCrsNrVale.nr + 1
							refVale = astr(nrVale)
							DO WHILE len(refVale)<6
								refVale = '0' + refVale
							ENDDO
							refVale = 'v' + refVale
						ENDIF
					ELSE
						uf_perguntalt_chama("N�O FOI POSS�VEL REGISTAR O VALE. OS PONTOS CONTINUAR�O NO CART�O DO CLIENTE.","OK","",16)
						RETURN .f.
					ENDIF
					
					** preparar impress�o 
					lcValidaImp = uf_gerais_setImpressoraPOS(.t.)
					If lcValidaImp=.f.
						uf_perguntalt_chama("N�O FOI POSS�VEL IMPRIMIR O VALE PORQUE A IMPRESSORA N�O EST� OPERACIONAL.","OK","",48)
						RETURN .f.
					ENDIF
					
					uf_gerais_criarCabecalhoPOS()
					uf_gerais_textoVermPOS()
					** IMPRIMIR O TIPO DE DOCUMENTO **
					?"	*** VALE DE DESCONTO ***"
					?"DATA IMPRESSAO: " + uf_gerais_getDate((datetime()+(difhoraria*3600)),"DATA")
					
					IF myValidadeVale > 0
						?"VALIDADE: "+ ASTR(ROUND(myValidadeVale,0)) +" MESES"
					ENDIF
					
					IF myValeIntr 
						?"VALE PESSOAL E INTRANSMISSIVEL"
					ENDIF
						
					?"VALIDO A PARTIR DO DIA SEGUINTE"
					uf_gerais_resetCorPOS()
					uf_gerais_separadorPOS()
					SELECT ft
					?"Operador: " + Alltrim(ft.vendnm) + " [" + astr(ft.vendedor) + "]"
					?"Cliente: " + ALLTRIM(lcnome) + " [" + astr(lcFtNo) + "][" + astr(lcFtEstab) + "]"
					uf_gerais_separadorPOS()
					?
					
					uf_gerais_textoCJPOS()
					
					???	chr(27)+chr(33)+chr(2)
					IF EMPTY(uf_gerais_getParameter("ADM0000000260","text")) or UPPER(ALLTRIM(uf_gerais_getParameter("ADM0000000260","text"))) == "EURO"
						?"VALOR DO VALE: " + ASTR(ucrsRebatimentosPontosVales.valor,8,2) + " EUROS*"
					ELSE
						?"VALOR DO VALE: " + ASTR(ucrsRebatimentosPontosVales.valor,8,2) + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))+ "*"
					ENDIF 
					**?"VALOR DO VALE: " + ASTR(ucrsRebatimentosPontosVales.valor,8,2)+ " EUROS*"
					???	chr(27)+chr(33)+chr(1)
					?
					???	chr(29)+chr(102)+chr(1) + chr(29)+chr(72)+chr(0) + chr(29)+chr(104)+chr(50) + chr(29)+chr(107)+chr(4) + UPPER(ALLTRIM(refVale)) + chr(0)
					
					****** IMPRESS�O DO RODAP� ******
					uf_gerais_separadorPOS()
					?"Processado por Computador"
					?"* O codigo de barras ilegivel invalida o vale. "
					?"Gratos pela sua compreens�o. *"

					uf_gerais_textoLJPOS()

					uf_gerais_feedCutPOS()

					**************************************************************

					** LIBERTAR IMPRESSORA, COLOCAR NO DEFAULT **
					uf_gerais_setImpressoraPOS(.f.)
					
					
					**	
					LOCAL lcStamp
					lcStamp = uf_gerais_stamp()
					
					TEXT TO lcSql NOSHOW textmerge
						INSERT INTO B_fidelvale(
							fvstamp, nr, ref, valor, pontos
							,clstamp
							,ousrdata, usrdata, ousr, usr)
						values ('<<ALLTRIM(lcStamp)>>', <<nrVale)>>, '<<ALLTRIM(refVale)>>'
								,<<ucrsRebatimentosPontosVales.valor>>, <<ucrsRebatimentosPontosVales.pontos>>
								,'<<ALLTRIM(lcUtstamp)>>'
								,dateadd(HOUR, <<difhoraria>>, getdate()), dateadd(HOUR, <<difhoraria>>, getdate()), <<ch_vendedor>>, <<ch_vendedor>>
							)
					ENDTEXT
					IF !uf_gerais_actGrelha("","",lcSql)
						uf_perguntalt_chama("N�O FOI POSS�VEL REGISTAR O VALE. OS PONTOS CONTINUAR�O NO CART�O DO CLIENTE.","OK","",48)
						RETURN .f.
					ENDIF
					
					** DESCONTAR OS PONTOS DO CART�O DE CLIENTE **
					TEXT TO lcSql NOSHOW TEXTMERGE
						update B_fidel
						set pontosusa = pontosusa + <<ucrsRebatimentosPontosVales.pontos>>
						where 
							B_fidel.clno = <<lcFtNo>> 
							and B_fidel.clestab = <<lcFtEstab>>
					ENDTEXT

					IF !uf_gerais_actGrelha("","",lcSql)
						uf_perguntalt_chama("OCORREU UM ERRO A ABATER PONTOS DO CLIENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					ENDIF
					
					
					lcPontosActuais = lcPontosActuais - ucrsRebatimentosPontosVales.pontos
					
					**********************************************
				ENDIF
				
				*******************************************************************
			ENDIF
			
		ENDSCAN
	ENDIF 
ENDFUNC 



** Verifica se os pontos atingidos pelo utente permite a emiss�o de vales de desconto
FUNCTION uf_CARTAOCLIENTE_emitirValesDirectos
	LPARAMETERS lcFtNo, lcFtEstab, lcCampanha, lcValor, lcSemPerguntas, lcMensagemVale
		
	** Dados do Cliente
	TEXT TO lcSQl NOSHOW TEXTMERGE 
		SELECT nome, no, estab, utstamp FROM b_utentes WHERE b_utentes.no = <<lcFtNo>> and b_utentes.estab = <<lcFtEstab>>
	ENDTEXT 
	IF !uf_gerais_actGrelha("","ucrsUtValesDirectos",lcSql)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR DADOS DO CLIENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
	
	SELECT ucrsUtValesDirectos
	&&lcPontosActuais = ucrsPontosActuais.pontos
	lcNome 		= ucrsUtValesDirectos.nome
	lcUtstamp 	= ucrsUtValesDirectos.utstamp
	lcNo 		= ucrsUtValesDirectos.no
	lcEstab 	= ucrsUtValesDirectos.estab
	
	IF EMPTY(lcSemPerguntas)
		IF !uf_perguntalt_chama("ATEN��O: EXISTE UMA CAMPANHA DE DESCONTOS DIRETOS ATIVA:  " + ALLTRIM(lcCampanha) + ". DESEJA IMPRIMIR UM VALE NO VALOR DE: "+ASTR(lcValor,8,2)+"�?","Sim","N�o")
			RETURN .f.
		ENDIF
		usacampanha=1
	ENDIF 
		
	select uCrsE1
	
	*** REGISTAR VALE ***
	LOCAL nrVale, refVale
	STORE 0 TO nrVale
	STORE "" TO refVale
	
	IF uf_gerais_actGrelha("","uCrsNrVale",[select ISNULL(MAX(nr),0) as nr from B_fidelvale (nolock)])
		IF RECCOUNT()>0
			nrVale = uCrsNrVale.nr + 1
			refVale = astr(nrVale)
			DO WHILE len(refVale)<6
				refVale = '0' + refVale
			ENDDO
			refVale = 'v' + refVale
		ENDIF
	ELSE
		uf_perguntalt_chama("N�O FOI POSS�VEL REGISTAR O VALE.","OK","",16)
		RETURN .f.
	ENDIF
	
	LOCAL lcStamp
	lcStamp = uf_gerais_stamp()
	
	TEXT TO lcSql NOSHOW textmerge
		INSERT INTO B_fidelvale(
			fvstamp, nr, ref, valor, pontos
			,clstamp
			,ousrdata, usrdata, ousr, usr)
		values ('<<ALLTRIM(lcStamp)>>', <<nrVale)>>, '<<ALLTRIM(refVale)>>'
				,<<lcValor>>, 0
				,'<<ALLTRIM(lcUtstamp)>>'
				,dateadd(HOUR, <<difhoraria>>, getdate()), dateadd(HOUR, <<difhoraria>>, getdate()), <<ch_vendedor>>, <<ch_vendedor>>
			)
	ENDTEXT
	IF !uf_gerais_actGrelha("","",lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL REGISTAR O VALE.","OK","",48)
		RETURN .f.
	ENDIF

	
	*** preparar impress�o ***
	lcValidaImp = uf_gerais_setImpressoraPOS(.t.)
	If lcValidaImp=.f.
		uf_perguntalt_chama("N�O FOI POSS�VEL IMPRIMIR O VALE PORQUE A IMPRESSORA N�O EST� OPERACIONAL.","OK","",48)
		RETURN .f.
	ENDIF
	
	uf_gerais_criarCabecalhoPOS()
	uf_gerais_textoVermPOS()
	** IMPRIMIR O TIPO DE DOCUMENTO **
	?"	*** VALE DE DESCONTO ***"
	?"DATA IMPRESSAO: " + uf_gerais_getDate((datetime()+(difhoraria*3600)),"DATA")
	
	IF myValidadeVale > 0
		?"VALIDADE: "+ ASTR(ROUND(myValidadeVale,0)) +" MESES"
	ENDIF
	
	IF myValeIntr 
		?"VALE PESSOAL E INTRANSMISSIVEL"
	ENDIF
		
	?"VALIDO A PARTIR DO DIA SEGUINTE"
	uf_gerais_resetCorPOS()
	uf_gerais_separadorPOS()
	SELECT ft
	?"Operador: " + Alltrim(ft.vendnm) + " [" + astr(ft.vendedor) + "]"
	?"Cliente: " + ALLTRIM(lcnome) + " [" + astr(lcFtNo) + "][" + astr(lcFtEstab) + "]"
	uf_gerais_separadorPOS()
	?
	
	uf_gerais_textoCJPOS()
	
	???	chr(27)+chr(33)+chr(2)
	IF EMPTY(uf_gerais_getParameter("ADM0000000260","text")) or UPPER(ALLTRIM(uf_gerais_getParameter("ADM0000000260","text"))) == "EURO"
		?"VALOR DO VALE: " + ASTR(lcValor,8,2) + " EUROS*"
	ELSE
		?"VALOR DO VALE: " + ASTR(lcValor,8,2) + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))+ "*"
	ENDIF 
	**?"VALOR DO VALE: " + ASTR(lcValor,8,2)+ " EUROS*"
	???	chr(27)+chr(33)+chr(1)
	?
	???	chr(29)+chr(102)+chr(1) + chr(29)+chr(72)+chr(0) + chr(29)+chr(104)+chr(50) + chr(29)+chr(107)+chr(4) + UPPER(ALLTRIM(refVale)) + chr(0)
	
	****** IMPRESS�O DO RODAP� ******
	uf_gerais_separadorPOS()
	?"Processado por Computador"
	?"* O codigo de barras ilegivel invalida o vale. "
	?"Gratos pela sua compreens�o. *"
	?ALLTRIM(IIF(EMPTY(lcMensagemVale),"",lcMensagemVale))

	uf_gerais_textoLJPOS()

	uf_gerais_feedCutPOS()
	**

	** LIBERTAR IMPRESSORA, COLOCAR NO DEFAULT **
	uf_gerais_setImpressoraPOS(.f.)

ENDFUNC 


** 	Campanha de vales emitidos por %% de valor atendimento	
FUNCTION uf_CARTAOCLIENTE_emitirValesdescValorAtPerc
	LPARAMETERS lcFtNo, lcFtEstab, lcCampanha, lcDescValorAtPerc, lcValorValesDistribuir, lcRemanescente, lcMensagemVale, lcMensagemValeRem, lcId
	
	** Verificar a validade da campanha
	TEXT TO lcSQl NOSHOW TEXTMERGE 
		select dataInicio, dataFim from campanhas where id =<<(lcId)>>
	ENDTEXT 
	IF !uf_gerais_actGrelha("","uCrsvalcampanha",lcSql)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR DADOS DOS ARTIGOS NAS CAMPANHAS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF

	IF uCrsvalcampanha.dataInicio<=DATE() AND uCrsvalcampanha.dataFim>=DATE()

		SELECT * FROM fi WITH (buffering=.t.) INTO CURSOR fincopiados READWRITE WHERE !EMPTY(fi.ref) AND EMPTY(fi.ofistamp)
	
		IF lcFtNo != 200 AND RECCOUNT("fincopiados")>0
		
			** Dados do Cliente
			TEXT TO lcSQl NOSHOW TEXTMERGE 
				SELECT nome, no, estab, utstamp FROM b_utentes WHERE b_utentes.no = <<lcFtNo>> and b_utentes.estab = <<lcFtEstab>>
			ENDTEXT 
			IF !uf_gerais_actGrelha("","uCrsUtValesDirectos",lcSql)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR DADOS DO CLIENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF
			
			**
			SELECT ucrsUtValesDirectos
			lcNome 		= ucrsUtValesDirectos.nome
			lcUtstamp 	= ucrsUtValesDirectos.utstamp
			lcNo 		= ucrsUtValesDirectos.no
			lcEstab 	= ucrsUtValesDirectos.estab						
	
			** Verificar os artigos da campanha com os artigos da FI
			TEXT TO lcSQl NOSHOW TEXTMERGE 
				SELECT distinct ref from campanhas_st_lista where campanhas like '%<<(lcId)>>%'
			ENDTEXT 
			IF !uf_gerais_actGrelha("","uCrsStCampanhas",lcSql)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR DADOS DOS ARTIGOS NAS CAMPANHAS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF
			
			SELECT * FROM fi WITH (buffering=.t.) INTO CURSOR ficampanhas READWRITE WHERE fi.ref in (SELECT ref FROM uCrsStCampanhas)
			
			lcTotalAtendimento = 0
			IF RECCOUNT("ficampanhas")>0 then
				SELECT ficampanhas
				GO top
				SCAN
					IF ficampanhas.partes==0 then
						IF EMPTY(ficampanhas.ofistamp)
							lcTotalAtendimento = lcTotalAtendimento + ficampanhas.etiliquido
						endif
					ELSE
						lcTotalAtendimento = lcTotalAtendimento - ficampanhas.etiliquido
					ENDIF 
				ENDSCAN 
			ENDIF 
		
			**lcTotalAtendimento = pagamento.txtValAtend.value
			**lcValorDesconto    =  ROUND(lcTotalAtendimento/lcDescValorAtPerc,2)
			lcValorDesconto    =  ROUND(lcTotalAtendimento*(lcDescValorAtPerc/100),2)
		
			** O Valor n�o atinge o valor minimo do Vale a distribuir
			IF lcValorDesconto < lcValorValesDistribuir AND lcRemanescente == .f.
				RETURN .f.
			ENDIF 
		
			**
			IF lcValorDesconto > 0
				IF uf_perguntalt_chama("ATEN��O: EXISTE UMA CAMPANHA DE DESCONTOS DIRETOS ATIVA:  (" + ALLTRIM(lcCampanha) + "). DESEJA IMPRIMIR OS VALES CORRESPONDENTES? VALOR TOTAL: "+ASTR(lcValorDesconto,8,2)+"�?","Sim","N�o")
					usacampanha=1
					lcValorDescontado = lcValorDesconto
					lcValorEmitido = 0
					lcEfetivamenteEmitido = 0
	
					Wait Window "Total =" + ASTR(lcValorDesconto,9,2) timeout 1
				
					DO WHILE lcValorEmitido < lcValorDesconto 
	
						lcValorEmitido = lcValorEmitido + lcValorValesDistribuir
					
						IF lcValorEmitido <= lcValorDesconto 		
							uf_CARTAOCLIENTE_emitirValesDirectos(lcFtNo, lcFtEstab, lcCampanha, lcValorValesDistribuir, .t., lcMensagemVale)
	
							Wait Window "Vale =" + ASTR(lcValorValesDistribuir,9,2) timeout 1					
							lcEfetivamenteEmitido = lcEfetivamenteEmitido + lcValorValesDistribuir
	
						ENDIF
				
					ENDDO
	
					IF !EMPTY(lcRemanescente)
						IF lcValorDesconto-lcEfetivamenteEmitido > 0
							Wait Window "Vale Remanescente =" + ASTR(lcValorDesconto-lcEfetivamenteEmitido,9,2) timeout 1
							uf_CARTAOCLIENTE_emitirValesDirectos(lcFtNo, lcFtEstab, lcCampanha, lcValorDesconto-lcEfetivamenteEmitido, .t., lcMensagemValeRem)
						ENDIF 
					ENDIF 
				ENDIF
			ENDIF 
		**
		ENDIF 
	ENDIF 
	
	If Used("uCrsvalcampanha")
		Fecha("uCrsvalcampanha")
	ENDIF
	If Used("ficampanhas")
		Fecha("ficampanhas")
	ENDIF
	If Used("fincopiados")
		Fecha("fincopiados")
	Endif	
ENDFUNC 


**
FUNCTION uf_cartaocliente_anulaVale
	If Used("uCrsFidelValesPendentes")
		Select uCrsFidelValesPendentes
		
		If !uf_perguntalt_chama("ATEN��O, VAI ABATER O VALE ["+Alltrim(uCrsFidelValesPendentes.ref)+"] NO VALOR DE ["+astr(uCrsFidelValesPendentes.valor,8,2)+" �]." + CHR(13) + "TEM A CERTEZA QUE PRETENDE CONTINUAR?","Sim","N�o")
			RETURN .f.
		Endif
		
		Local lcPontosLivres
		Store 0 To lcPontosLivres
		
		If uf_perguntalt_chama("PRETENDE RE-INSERIR OS PONTOS NO CART�O DE CLIENTE?","Sim","N�o")
			** re-inserir pontos **
			Text To lcSql Noshow textmerge
				Update B_fidel
				Set pontosusa	= pontosusa - <<uCrsFidelValesPendentes.pontos>>
					,usr			= <<ch_userno>>
					,usrdata	= dateadd(HOUR, <<difhoraria>>, getdate())
				where clstamp 	= '<<Alltrim(uCrsFidelValesPendentes.clstamp)>>'
			Endtext
			If !uf_gerais_actGrelha("","",lcSql)
				uf_perguntalt_chama("OCORREU UM PROBLEMA A RE-INSERIR OS PONTOS NO CART�O DO CLIENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			Endif
			*****************
			
			** guardar pontos livres finais **
			Select uCrsFIdelValesPendentes
			lcPontosLivres = uCrsFidelValesPendentes.pontoslivres + uCrsFidelValesPendentes.pontos
			**************************
		Else
			** guardar pontos livres finais **
			Select uCrsFIdelValesPendentes
			lcPontosLivres = uCrsFidelValesPendentes.pontoslivres
			**************************
		Endif
		
		LOCAL lcStamp
		lcStamp = uf_gerais_stamp()
		
		** registar ocorrencia **
		Text To lcSql Noshow textmerge
			Insert Into B_ocorrencias(
				stamp
				,linkstamp
				,tipo
				,grau
				,descr
				,oValor
				,dValor
				,usr
				,date
				,nValor
			)Values
				('<<Alltrim(lcStamp)>>', '<<Alltrim(uCrsFidelValesPendentes.fvstamp)>>'
				,'Cart�o Cliente', 3, 'Vale '+'<<Alltrim(uCrsFidelValesPendentes.ref)>>'+' Abatido' + ' (Cliente ' + '<<astr(uCrsFidelValesPendentes.no)>>' + '['+'<<astr(uCrsFidelValesPendentes.estab)>>'+'])'
				,'<<Alltrim(uCrsFidelValesPendentes.u_nrcartao)>>' + ' pts.livres: ' + '<<uCrsFidelValesPendentes.pontoslivres>>'
				,'<<Alltrim(uCrsFidelValesPendentes.u_nrcartao)>>' + ' pts.livres: ' + '<<lcPontosLivres>>'
				,<<ch_userno>>
				,dateadd(HOUR, <<difhoraria>>, getdate())
				,<<lcPontosLivres-uCrsFidelValesPendentes.pontoslivres>>
			)
		Endtext
		If !uf_gerais_actGrelha("","",lcSql)
			uf_perguntalt_chama("OCORREU UM PROBLEMA A REGISTAR A OCORR�NCIA. POR FAVOR CONTACTE O SUPORTE","OK","",16)
			RETURN .f.
		ENDIF
		*********************
		
		** abater vale **
		TEXT TO lcSql NOSHOW TEXTMERGE
			Update B_fidelvale
			Set anulado=1, usr=<<ch_userno>>, usrdata=dateadd(HOUR, <<difhoraria>>, getdate())
			where fvstamp='<<Alltrim(uCrsFidelValesPendentes.fvstamp)>>'
		ENDTEXT
		IF !uf_gerais_actGrelha("","",lcSql)
			uf_perguntalt_chama("OCORREU UM PROBLEMA A ABATER O VALE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
		***************
		
		** actualiza ecra **
*!*			SELECT uCrsFIdelValesPendentes
*!*			DELETE
		cartaocliente.pageframe1.page2.grdVales.refresh
	ENDIF
ENDFUNC


**
FUNCTION uf_cartaocliente_novaCampanha
	uf_campanhas_chama()
	uf_CAMPANHAS_novo()
	uf_cartaocliente_sair()
ENDFUNC 


**
FUNCTION uf_cartaocliente_sair
		
	If Used("uCrsFidel")
		Fecha("uCrsFidel")
	Endif
	If Used("uCrsFidel2")
		Fecha("uCrsFidel2")
	Endif

	cartaocliente.hide
	cartaocliente.release
ENDFUNC



**
FUNCTION uf_cartaocliente_importarPontosVendasAnteriores
	LPARAMETERS lcNo, lcEstab
		
	** 
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		Select 
			fi.*
		from 
			ft (nolock)
			inner join fi (nolock) on fi.ftstamp = ft.ftstamp 
		where 
			ft.no = <<lcNo>>
			and ft.estab = <<lcEstab>>
			and fi.partes = 0
	ENDTEXT
	
	IF !uf_gerais_actGrelha("","ucrsFiPontosVDAnt",lcSql)
		uf_perguntalt_chama("N�o foi possivel calcular hist�rico do utente.","OK","",16)
		RETURN .f.
	ENDIF
	
	IF RECCOUNT("ucrsFiPontosVDAnt") == 0
		RETURN .f.
	ENDIF 
	

	** parametro valida se pretende importar pontos ou n�o - luis leal 20161108
	IF uf_gerais_getParameter("ADM0000000271","BOOL") AND myTipoCartao == 'Pontos'	
		IF uf_perguntalt_chama("Deseja importar os pontos correspondentes ao hist�rico de vendas do utente? (Ser�o aplicadas as regras atualmente em vigor)","Sim","N�o",64) 
			uf_CARTAOCLIENTE_actualizaPontosClPag(.t.,.f.,.t.,lcNo,lcEstab)
		ENDIF
	ENDIF


	IF USED("ucrsFiPontosVDAnt")
		fecha("ucrsFiPontosVDAnt")
	ENDIF 
ENDFUNC 


** funcao temp p criar cart�es de cliente n�o � usada no software, serve apenas para casos espor�dicos.
FUNCTION uf_cartaocliente_novoCartao_Temp
	
	LOCAL lcNrCartao, lcClStamp

	&& lista utentes
	lcSQL = ''
	TEXT TO lcSql NOSHOW TEXTMERGE
		select * from b_utentes (nolock) where no > 200	and nrcartao = '' and inactivo = 0
	ENDTEXT
	IF !uf_gerais_actgrelha("", "uCrsFidel", lcSql)
		uf_perguntalt_chama("OCORREU UM PROBLEMA A LISTAR CART�ES DE UTENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
			
	SELECT uCrsFidel
	GO TOP 
	SCAN 		
			
		If uf_gerais_actgrelha("", "uCrsNrCartaoCl", [select isnull(MAX(convert(int,REPLACE(nrcartao,'CL',''))+1),'0000000') as nrcartao from	B_fidel (nolock)])
			If Reccount("uCrsNrCartaoCl")>0
				IF EMPTY(uCrsNrCartaoCl.nrcartao)
					lcNrCartao = 'CL0000001'
				ELSE			
					LOCAL lcNrCartaoUlt, lcNrCartao, lcNCartao2
					STORE '' TO lcNrCartao, lcNCartao2,lcNrCartaoUlt
					lcNrCartao= 'CL' + astr(uCrsNrCartaoCl.nrcartao)
					
					IF LEN(lcNrCartao) < 10
						lcNCartao2 = RIGHT(lcNrCartao, LEN(lcNrCartao)-2)
						FOR i=1 TO 7-LEN(lcNCartao2)
							lcNCartao2 = '0' + lcNCartao2
						ENDFOR 
						lcNrCartao = 'CL' + lcNCartao2 
					ENDIF 
				ENDIF
			ENDIF
			Fecha("uCrsNrCartaoCl")
		ELSE
			uf_perguntalt_chama("OCORREU UM ERRO AO GERAR O N�MERO DO CART�O DE CLIENTE! POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
			RETURN .f.
		ENDIF
	
		IF EMPTY(lcNrCartao)
			lcNrCartao = 'CL0000000'
		ENDIF
	
		LOCAL lcStamp
		lcStamp = uf_gerais_stamp()
	

		&& Insere Cart�o
		SELECT  uCrsFidel
		
		TEXT TO lcSql Noshow textmerge
			Insert into 
				B_fidel (
					fstamp
					,clno
					,clestab
					,nrcartao
					,clstamp
					,pontosatrib
					,pontosusa
					,inactivo
					,atendimento
					,validade
					,ousrdata
					,usrdata
					,ousr
					,usr
			)Values (
				'<<lcStamp>>',
				<<uCrsFidel.no>>
				,<<uCrsFidel.estab>>
				,'<<Alltrim(lcNrCartao)>>'
				,'<<Alltrim(uCrsFidel.utstamp)>>'
				,0
				,0
				,0
				,0
				,convert(varchar,DATEADD(MONTH,<<myValidadeVale>>,GETDATE()),112)
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112)
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112)
				,<<ch_userno>>
				,<<ch_userno>>
			)
			
			UPDATE b_utentes SET nrcartao = '<<Alltrim(lcNrCartao)>>' WHERE utstamp = '<<Alltrim(uCrsFidel.utstamp)>>'
		ENDTEXT
	
		IF !uf_gerais_actGrelha("","",lcSql)
			uf_perguntalt_chama("OCORREU UM PROBLEMA A CRIAR O CART�O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		ENDIF
	ENDSCAN 
	
ENDFUNC


** funcao que permite reimprimir vales de desconto Lu�s Leal 20161108 
FUNCTION uf_cartaocliente_imprimirVale

	&& validar impressora
	LOCAL lcValidaImp
	STORE .f. TO lcValidaImp
	
	lcValidaImp = uf_gerais_setImpressoraPOS(.t.)
	
	IF lcValidaImp=.f.
		uf_perguntalt_chama("N�O FOI POSS�VEL IMPRIMIR O VALE PORQUE A IMPRESSORA N�O EST� OPERACIONAL. POR FAVOR VERIFIQUE.","OK","",48)
		RETURN .f.
	ENDIF
	**
			
	**
	uf_gerais_criarCabecalhoPOS()
	
	uf_gerais_textoVermPOS()
	
	** IMPRIMIR O TIPO DE DOCUMENTO **
	?"	*** VALE DE DESCONTO ***"
	?"DATA EMISSAO: " + astr(uf_gerais_getDate(uCrsFidelValesPendentes.dataEmissao))

	IF uf_gerais_getParameter("ADM0000000017","NUM")>0
		?"VALIDADE: "+ ALLTRIM(STR(ROUND(uf_gerais_getParameter("ADM0000000017","NUM"),0))) +" MESES"
	ENDIF

	IF uf_gerais_getParameter("ADM0000000020","BOOL")
		?"VALE PESSOAL E INTRANSMISSIVEL"
	ENDIF
		
	?"VALIDO A PARTIR DO DIA SEGUINTE"

	uf_gerais_resetCorPOS()
	
	uf_gerais_separadorPOS()
	
	?"Operador: " + Alltrim(ch_vendnm) + " [" + astr(ch_vendedor) + "]"
	?"Cliente: " + ALLTRIM(uCrsFidelValesPendentes.NOME) + " [" + astr(uCrsFidelValesPendentes.NO) + "][" + astr(uCrsFidelValesPendentes.ESTAB) + "]"
	uf_gerais_separadorPOS()
	?
							
	uf_gerais_textoCJPOS()
								
	???	chr(27)+chr(33)+chr(2)
	IF EMPTY(uf_gerais_getParameter("ADM0000000260","text")) or UPPER(ALLTRIM(uf_gerais_getParameter("ADM0000000260","text"))) == "EURO"
		?"VALOR DO VALE: " + ALLTRIM(STR(uCrsFidelValesPendentes.VALOR,8,2)) + " EUROS*"
	ELSE
		?"VALOR DO VALE: " + ALLTRIM(STR(uCrsFidelValesPendentes.VALOR,8,2)) + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))+ "*"
	ENDIF 
	**?"VALOR DO VALE: " + ALLTRIM(STR(uCrsFidelValesPendentes.VALOR,8,2)) + " EUROS*"
	???	chr(27)+chr(33)+chr(1)
	?
	???	chr(29)+chr(104)+chr(50) + chr(29)+chr(72)+chr(2) + chr(29)+chr(119)+chr(2) + chr(29)+chr(107)+chr(4) + UPPER(ALLTRIM(LEFT(uCrsFidelValesPendentes.REF,10))) + chr(0)
	
	****** IMPRESS�O DO RODAP� ******
	uf_gerais_separadorPOS()
	
	?"Processado por Computador"
	?"* O codigo de barras ilegivel invalida o vale. "
	?"Gratos pela sua compreensao. *"

	uf_gerais_textoLJPOS()

	uf_gerais_feedCutPOS()

	** LIBERTAR IMPRESSORA, COLOCAR NO DEFAULT **
	uf_gerais_setImpressoraPOS(.f.)


ENDFUNC 