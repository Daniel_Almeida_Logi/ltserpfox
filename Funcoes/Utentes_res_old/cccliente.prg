FUNCTION uf_CCCliente_chama
	LPARAMETERS lcNo, lcEstab, lcNome
	
	&&valida parameters
	IF EMPTY(lcNO) OR !(Type("lcNO")=="N")
		lcNO= 0
	ENDIF
	
	IF EMPTY(lcEstab) OR !(Type("lcEstab")=="N")
		lcEstab = 0
	ENDIF
	
	IF EMPTY(lcNome)
		lcNome = ""
	ENDIF
	*******************************
	
	** cria cursor vazio
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_clientes_cc <<lcNo>>,<<lcEstab>>,'20000101','20000101',0,0,''
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "uCrsPesqCCC", lcSql)
		uf_perguntalt_chama("ERRO AO LER A CONTA CORRENTE DO CLIENTE!","OK","",16)
		RETURN .F.
	ENDIF 
	
	&& Controla Abertura do Painel			
	if type("CCCLIENTE")=="U"
		DO FORM CCCLIENTE
	ELSE
		CCCLIENTE.show
	ENDIF
	
	CCCLIENTE.txtNome.value = ALLTRIM(lcNome)
	CCCLIENTE.txtNo.value = lcNo
	CCCLIENTE.txtEstab.value = lcEstab
	
	** carrega dados
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_clientes_cc <<lcNo>>,<<lcEstab>>,'<<uf_gerais_getdate(CCCLIENTE.txtDe.value, "SQL")>>','<<uf_gerais_getdate(CCCLIENTE.txtA.value, "SQL")>>', 0 ,0,''
	ENDTEXT 	
	IF !uf_gerais_actgrelha("cccliente.GridPesq", "uCrsPesqCCC", lcSql)
		uf_perguntalt_chama("ERRO AO LER A CONTA CORRENTE DO CLIENTE!","OK","",16)
		RETURN .F.
	ENDIF 
	
	uf_CCCLIENTE_CalcularTotais()
	
	CCCLIENTE.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqCCC"))) + " Resultados"
ENDFUNC


**
FUNCTION uf_CCCLIENTE_carregamenu
	cccliente.menu1.adicionaOpcao("opcoes", "Op��es", myPath + "\imagens\icons\opcoes_w.png", "","F1")
	cccliente.menu1.adicionaopcao("atualizar", "Atualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_CCCLIENTE_Pesquisa", "F2")
	cccliente.menu1.adicionaopcao("limpa", "Limpar", myPath + "\imagens\icons\limpar_w.png", "uf_cccliente_limpa", "F3")
	cccliente.menu1.adicionaopcao("estab", "Inc. Estab.", myPath + "\imagens\icons\unchecked_w.png", "uf_ccliente_incluirEstab", "F4")
	cccliente.menu1.adicionaopcao("imprimir", "Imprimir", myPath + "\imagens\icons\imprimir_w.png", "uf_CCCLIENTE_Imprimir", "F5")
	cccliente.menu1.adicionaopcao("navegar", "Ver Doc.", myPath + "\imagens\icons\doc_seta_w.png", "uf_CCCLIENTE_Navegar", "F6")
	cccliente.menu1.adicionaopcao("loja", "Loja Act.", myPath + "\imagens\icons\unchecked_w.png", "uf_ccliente_lojaAct", "F7")
ENDFUNC


**
FUNCTION uf_ccliente_incluirEstab
	IF cccliente.menu1.estab.tag == 'false'
		&& aplica sele��o
		cccliente.menu1.estab.tag = 'true'
		
		&& altera o botao
		cccliente.menu1.estab.config("Inc. Estab.", myPath + "\imagens\icons\checked_w.png", "uf_ccliente_incluirEstab", "F4")
	ELSE
		&& aplica sele��o
		cccliente.menu1.estab.tag = 'false'
		
		&& altera o botao
		cccliente.menu1.estab.config("Inc. Estab.", myPath + "\imagens\icons\unchecked_w.png", "uf_ccliente_incluirEstab", "F4")
	ENDIF
	
	uf_CCCLIENTE_Pesquisa()
ENDFUNC

**
FUNCTION uf_ccliente_lojaAct
	IF cccliente.menu1.loja.tag == 'false'
		&& aplica sele��o
		cccliente.menu1.loja.tag = 'true'
		
		&& altera o botao
		cccliente.menu1.loja.config("Loja Act.", myPath + "\imagens\icons\checked_w.png", "uf_ccliente_lojaAct","F7")
	ELSE
		&& aplica sele��o
		cccliente.menu1.loja.tag = 'false'
		
		&& altera o botao
		cccliente.menu1.loja.config("Loja Act.", myPath + "\imagens\icons\unchecked_w.png", "uf_ccliente_lojaAct","F7")
	ENDIF
	
	uf_CCCLIENTE_Pesquisa()
ENDFUNC


**
FUNCTION uf_CCCLIENTE_CalcularTotais
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_clientes_cc_totais 
			<<CCCLIENTE.txtNo.value>>
			,<<CCCLIENTE.txtEstab.value>>
			,'<<uf_gerais_getDate(CTOD(CCCLIENTE.txtDe.value), "SQL")>>'
			,'<<uf_gerais_getDate(CTOD(CCCLIENTE.txtA.value), "SQL")>>'
			,0
			,<<IIF(CCCLIENTE.menu1.estab.tag=='true', 1, 0)>>
			,''
	ENDTEXT 

	IF uf_gerais_actgrelha("", "uCrsCCCAnt", lcSql)
		IF RECCOUNT("uCrsCCCAnt")>0
			Select uCrsCCCAnt
			Go TOP

			** Adiciona linha com saldo anterior			
			Select uCrsPesqCCC
			GO TOP 
			replace	uCrsPesqCCC.DATALC WITH "..."
			replace	uCrsPesqCCC.CMDESC WITH "Saldo Anterior"
			replace uCrsPesqCCC.EDEB WITH uCrsCCCAnt.debito_anterior
			replace uCrsPesqCCC.ECRED WITH uCrsCCCAnt.credito_anterior
			replace uCrsPesqCCC.SALDO WITH uCrsCCCAnt.saldo_anterior

			** Atualiza valores periodo
			CCCLIENTE.txtDeb.value = Round(uCrsCCCAnt.debito_periodo,2)
			CCCLIENTE.txtCred.value = Round(uCrsCCCAnt.credito_periodo,2)
			CCCLIENTE.txtSaldo.value = Round(uCrsCCCAnt.saldo_periodo,2)
		
			** Atualiza valores total
			CCCLIENTE.txtDebT.value = Round(uCrsCCCAnt.debito_total,2)
			CCCLIENTE.txtCredT.value = Round(uCrsCCCAnt.credito_total,2)
			CCCLIENTE.txtSaldoT.value = Round(uCrsCCCAnt.saldo_total,2)

		ELSE
			IF Used("uCrsCCCAnt")
				Fecha("uCrsCCCAnt")
			ENDIF
			
			CCCLIENTE.txtDeb.value = 0
			CCCLIENTE.txtCred.value = 0
			CCCLIENTE.txtSaldo.value = 0
			CCCLIENTE.txtDebT.value = 0
			CCCLIENTE.txtCredT.value = 0
			CCCLIENTE.txtSaldoT.value = 0
		ENDIF
	ENDIF
	
	CCCLIENTE.gridPesq.refresh
ENDFUNC 



**
FUNCTION uf_CCCLIENTE_Pesquisa
	LOCAL lcSQL
	
	regua(0,0,"A pesquisar...")
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_clientes_cc <<CCCLIENTE.txtNo.value>>, <<CCCLIENTE.txtEstab.value>>, '<<uf_gerais_getDate(CCCLIENTE.txtDe.value, "SQL")>>','<<uf_gerais_getDate(CCCLIENTE.txtA.value, "SQL")>>', 0, <<IIF(CCCLIENTE.menu1.estab.tag=='true', 1, 0)>>,<<IIF(CCCLIENTE.menu1.loja.tag=='true', "'"+ALLTRIM(mySite)+"'", "''")>>
	ENDTEXT

	uf_gerais_actGrelha("CCCLIENTE.GridPesq", "ucrsPesqCCC", lcSQL)

	uf_CCCLIENTE_CalcularTotais()
	
	regua(2)
	
	CCCLIENTE.Refresh
	
	CCCLIENTE.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqCCC"))) + " Resultados"
ENDFUNC



**
FUNCTION uf_CCCLIENTE_Imprimir
	IF RECCOUNT("ucrsPesqCCC") > 0
	
		**Trata parametros
		LOCAL lcNo, lcEstab
		lcNo 	= Iif(Empty(CCCLIENTE.txtNo.value), 0, CCCLIENTE.txtNo.value)
		lcEstab	= Iif(Empty(CCCLIENTE.txtEstab.value), 0, CCCLIENTE.txtEstab.value)

		** Construir string com parametros
		lcSql = "&No=" 			+ ALLTRIM(STR(lcNo)) +;
				"&estab=" 		+ ALLTRIM(STR(lcEstab)) +;
				"&site=" 		+ mySite +;
				"&dataIni="		+ uf_gerais_getDate(CCCLIENTE.txtDe.value, 'DATA') +;
				"&dataFim="		+ uf_gerais_getDate(CCCLIENTE.txtA.value, 'DATA') +;
				"&modo="		+ "True" +;
				"&verTodos="	+ CCCLIENTE.menu1.estab.tag +;
				"&loja=" 		+ IIF(CCCLIENTE.menu1.loja.tag=='true', ALLTRIM(mySite), "")
				
		
		** chamar report pelo nome dele
		uf_gerais_chamaReport("relatorio_CC_Cliente", lcSql)
	ENDIF
ENDFUNC


**
FUNCTION uf_CCCLIENTE_Navegar

	If TYPE('ATENDIMENTO') == 'U'
	
		If myFtIntroducao == .t. OR myFtAlteracao == .t.
			uf_perguntalt_chama("O ECR� DE FACTURA��O ENCONTRA-SE EM MODO DE INTRODU��O OU EDI��O.","OK","", 48)
		ELSE
			SELECT uCrsPesqCCC
			If !Empty(uCrsPesqCCC.ccstamp)
				IF ALLTRIM(uCrsPesqCCC.cmdesc) == "N/Recibo"				
					uf_regvendas_consultarRecibos(Alltrim(uCrsPesqCCC.ccstamp))
				ELSE
					uf_facturacao_chama(Alltrim(uCrsPesqCCC.ccstamp))
				ENDIF 
			ELSE
				uf_perguntalt_chama("A linha seleccionada n�o tem nenhum documento associado.","OK","",64)
			ENDIF
		Endif
	Else
		uf_perguntalt_chama("N�O PODE ACEDER AO ECRA DE FACTURA��O COM O ECRA DE ATENDIMENTO ABERTO.","OK","",48)
	Endif
ENDFUNC



**
FUNCTION uf_cccliente_limpa
	CCCLIENTE.txtDe.Value = uf_gerais_getdate(Date()-60)
	CCCLIENTE.txtA.Value = uf_gerais_getdate()
	
	IF CCCLIENTE.menu1.estab.tag == 'true'
		uf_ccliente_incluirEstab()
	ENDIF
ENDFUNC



**
FUNCTION uf_CCCLIENTE_sair
	**Fecha Cursores
	IF USED("uCrsPesqCCC")
		fecha("uCrsPesqCCC")
	ENDIF 
	
	CCCLIENTE.hide
	CCCLIENTE.release
ENDFUNC