****************************
** inclui prg secund�rios ***
****************************
DO cartaocliente
DO pesqutentes
DO prodselcli
DO cccliente
DO ccnrcliente
DO sincclientes
DO fatassociados
DO logcliente
DO enviotoken
DO direitos

*************************
**	Func��o Principal  **
*************************


FUNCTION uf_utentes_chama
	LPARAMETERS lcNO, lcestab, lcidno, lcIdstamp		

	IF !uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Gest�o de Clientes')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL GEST�O DE CLIENTES.","OK","",48)
		RETURN .f.
	ENDIF
	
	PUBLIC contadorSaldoUT
	contadorSaldoUT = 10
	
	regua(0,10,"A carregar painel...")
	regua(1,1,"A carregar painel...")
			
	LOCAL lcSql
	lcSql = ''
	
	IF EMPTY(lcNO) OR !(Type("lcNO")=="N")
		lcNO = 0
	ENDIF
	IF EMPTY(lcEstab) OR !(Type("lcEstab")=="N")
		lcEstab = 0
	ENDIF
		
	**chamado pelo PHC
	IF !EMPTY(lcIdstamp)
	
		TEXT TO lcSql NOSHOW textmerge
			SELECT TOP 1 no,estab FROM b_utentes (nolock) Where	b_utentes.idstamp = '<<ALLTRIM(lcIdstamp)>>'
		ENDTEXT
		IF !uf_gerais_actgrelha("", "ucrsIdno", lcSql)
			lcNo = 0
			lcEstab = 0
		ELSE
			SELECT ucrsIdno
			lcNO = ucrsIdno.no
			lcEstab = ucrsIdno.estab
		ENDIF
		IF USED("ucrsIdno")
			fecha("ucrsIdno")
		ENDIF 
	ENDIF
	**	

	** Controla Abertura do Painel
	IF type("utentes")=="U"
	
	
		regua(1,2,"A carregar painel...")
		uf_utentes_criaCursoresDefalt(lcNo, lcEstab)
		regua(1,6,"A carregar painel...")
		
		DO FORM utentes
	ELSE
		regua(1,6,"A carregar painel...")
		utentes.show
	ENDIF
	
	regua(1,7,"A carregar painel...")
	uf_utentes_carregaDados(lcNo, lcEstab)
	
	regua(1,8,"A carregar painel...")
	uf_utentes_confEcra()
		
	regua(1,9,"A carregar painel...")
	&& Preenche patologias
	uf_utentes_preenchepatologias()
	
	&& Prrenche Diplomas
	&&uf_utentes_PreencheDiplomas()
	
	regua(1,10,"A carregar painel...")
	&& Hist�rico Tratamento
	uf_utentes_actDadosHistTrat()
	
	&& Hist�rico Tratamento
	uf_utentes_actOutrosBenef()
	
	&& EFRs	
	uf_utentes_actEFRs()
		
	&& Registo Clinico	
	uf_utentes_actRegistoClinico()
	
	&&BioRegistos
	uf_utentes_actBioRegistos()
	
	&&Entidades de Fatura��o
	uf_utentes_actEntidadesUtentes() 

	&&Servi�os para Fatura��o
	uf_utentes_actServicosFact() 

	IF type("marcacoes")!="U"
		uf_utentes_AlternaTabs(2)
		uf_gerais_navegaPgframe('UTENTES','lbl10','ctnTabs2')
	ENDIF 

	IF lcNO<>0
		
		** REGISTO CONSULTA NOS LOGS DE UTILIZADOR
		uf_user_log_ins('CL', ALLTRIM(cl.utstamp), 'CONSULTA', '')
		
	ENDIF 
	
	&& Actualiza Ecr�
	utentes.refresh
	
	regua(2)
ENDFUNC

**
FUNCTION uf_utentes_actualizar
	IF myClIntroducao == .T. OR myClAlteracao == .T.
		RETURN .f.
	ENDIF 
	
	IF USED("CL")
		select cl
		IF !EMPTY(cl.no)
			uf_utentes_chama(cl.no,cl.estab)
		ENDIF 
	ENDIF 

ENDFUNC 

**
FUNCTION uf_utentes_criaCursoresDefalt
	LPARAMETERS lcNo, lcEstab
		
	&& Cursor da CL
	TEXT TO lcSql NOSHOW textmerge
		SELECT Top 0 *, pontos = 0, binovalidade = '19000101',compfixa=0 FROM b_utentes (nolock) WHERE b_utentes.no= 0 and b_utentes.estab = 0
	ENDTEXT
	IF !uf_gerais_actgrelha("", "cl", lcSql)
		regua(2)
		RETURN .f.
	ENDIF
	
	regua(1,3,"A carregar painel...")
	&& Cursor de Dependentes
	TEXT TO lcSql NOSHOW textmerge
		select TOP 0 estab, nome, morada, telefone, fax, email, esaldo, entFact from  b_utentes (NOLOCK) where no=0 and estab != 0
	ENDTEXT
	IF !uf_gerais_actgrelha("", "uCrsEstabCL", lcSql)
		regua(2)
		RETURN .f.
	ENDIF
	
	regua(1,4,"A carregar painel...")
	&& Cursor de patologias do Cliente
	TEXT TO lcSql NOSHOW textmerge
		SET fmtonly on
		exec up_clientes_patologias ''
		SET fmtonly off
	ENDTEXT
	IF !uf_gerais_actgrelha("", "uCrsPatologiasCL", lcSql)
		regua(2)
		RETURN .f.
	ENDIF
	
	&& Cursor de patologias gerais
	TEXT TO lcSql NOSHOW textmerge
		SET fmtonly on
		exec up_clientes_patologiasLista
		SET fmtonly off
	ENDTEXT
	IF !uf_gerais_actgrelha("", "uCrsPatologias", lcSql)
		regua(2)
		RETURN .f.
	ENDIF

	&& Cursor de Diplomas Cliente
	IF !USED("uCrsDiplomasID")
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			exec up_utentes_DiplomaUtente ''
		ENDTEXT 
	
		IF !(uf_gerais_actGrelha("","uCrsDiplomasID",lcSQL))
			uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR OS DIPLOMAS.","OK","",16)
			regua(2)
			RETURN .f.
		ENDIF
	ENDIF
	
	regua(1,5,"A carregar painel...")
	&&Cursor Hist�rico Tratamento
	TEXT TO lcSql NOSHOW textmerge
		set fmtonly on 
		exec up_clientes_historicoTratamento 0, 0, '','', '', ''
		set fmtonly off
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "uCrsHisTrat", lcSql)
		regua(2)
		RETURN .f.
	ENDIF

	**CREATE CURSOR uCrsHisTrat(dataDoc d, ref c(18), design c(60), qtt n(9), vendnm c(20), obsdoc c(254), tipo c(10), usrhora c(8),pesquisa l, stamp c(25), dci c(254), posologia c(254), dtini d, dtp d, palmoco l, almoco l, lanche l, jantar l, noite l, fistamp c(25), alterado l)
	
	&& Cursor de EFRs
	IF !USED("uCrsEFRs")
		lcSQL = ''	
		TEXT TO lcSql NOSHOW textmerge
			exec up_prescricao_efrutente '<<ALLTRIM(cl.utstamp)>>'
		ENDTEXT
		IF !uf_gerais_actGrelha("","uCrsEFRs",lcSql)
			uf_perguntalt_chama("Ocorreu um erro a verificar EFRs associadas ao utentes.","OK","",16)
			regua(2)
			RETURN .f.
		ENDIF
	ENDIF
	
	IF !USED("uCrsOutrosBenef")
		&& Cursor de outros beneficios
		lcSQL = ''	
		TEXT TO lcSql NOSHOW textmerge
			exec up_prescricao_outrosBenef '<<ALLTRIM(cl.utstamp)>>'
		ENDTEXT
		IF !uf_gerais_actGrelha("","uCrsOutrosBenef",lcSql)
			uf_perguntalt_chama("OCORREU UM ERRO A VERIFICAR OS OUTROS BENEF�CIOS.","OK","",16)
			regua(2)
			RETURN .f.
		ENDIF
	ENDIF
	
	IF !USED("uCrsRegistoClinico")
		lcSQL = ''	
		TEXT TO lcSql NOSHOW textmerge
			exec up_utentes_registoClinico '<<ALLTRIM(cl.utstamp)>>'
		ENDTEXT
		IF !uf_gerais_actGrelha("","uCrsRegistoClinico",lcSql)
			uf_perguntalt_chama("Ocorreu um erro a verificar Registos Clinicos associadas ao utentes.","OK","",16)
			RETURN .f.
		ENDIF
	ENDIF
	
	** Entidades de Factura��o
	IF !USED("ucrsEntidadesUtentes")
		lcSQL = ''	
		TEXT TO lcSql NOSHOW textmerge
			exec up_utentes_Entidades ''
		ENDTEXT
		IF !uf_gerais_actGrelha("","ucrsEntidadesUtentes",lcSql)
			uf_perguntalt_chama("Ocorreu um erro a verificar Entidades de Factura��o associadas ao utentes.","OK","",16)
			RETURN .f.
		ENDIF
	ENDIF
	
	LOCAL lcTotalPagar
	STORE 0 TO lcTotalPagar
	
	** Servi�os Factura��o
	IF !USED("ucrsServicosFaturacao")
		lcSQL = ''	
		TEXT TO lcSql NOSHOW textmerge
			exec up_utentes_ServicosFaturacao '', <<mysite_nr>>
		ENDTEXT
		IF !uf_gerais_actGrelha("","ucrsServicosFaturacao",lcSql)
			uf_perguntalt_chama("Ocorreu um erro a verificar Servi�os de Factura��o associadas ao utentes.","OK","",16)
			RETURN .f.
		ENDIF
	
	ENDIF
	
	** BIOPARAMETROS
	lcSQL = ''	
	TEXT TO lcSql NOSHOW textmerge
		exec up_utentes_BioParametros
	ENDTEXT
	IF !uf_gerais_actGrelha("","ucrsBioparametros",lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar lista de Bioparametros.","OK","",16)
		RETURN .f.
	ENDIF
	
	** BIOREGISTOS
	lcSQL = ''	
	TEXT TO lcSql NOSHOW textmerge
		exec up_utentes_BioRegistos '<<ALLTRIM(cl.utstamp)>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("","ucrsBioRegistos",lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar lista de Bioregistos associados ao utente.","OK","",16)
		RETURN .f.
	ENDIF
	
	** chamar painel
	IF uf_gerais_addConnection("CLI") == .F.
		regua(2)
		RETURN .f.
	ENDIF 
		
ENDFUNC 

**
FUNCTION uf_utentes_carregamenu
	&& Configura menu principal
	utentes.menu1.adicionaOpcao("aplicacoes", "Aplica��es", myPath + "\imagens\icons\aplicacoes_w.png", "uf_gerais_menuAplicacoes With 'Utentes'","O")
	utentes.menu1.adicionaOpcao("opcoes", "Op��es", myPath + "\imagens\icons\opcoes_w.png", "","F1")
	utentes.menu1.adicionaOpcao("pesquisar", "Pesquisar", myPath + "\imagens\icons\lupa_w.png", [uf_PESQUTENTES_chama with "UTENTES"],"F2")
	utentes.menu1.adicionaOpcao("novo", "Novo", myPath + "\imagens\icons\mais_w.png", "uf_utentes_novo","F3")
	utentes.menu1.adicionaOpcao("editar", "Editar", myPath + "\imagens\icons\lapis_w.png", "uf_utentes_editar","F4")
	utentes.menu1.adicionaOpcao("anterior", "Anterior", myPath + "\imagens\icons\ponta_seta_left_w.png", "uf_utentes_anterior","F5")
	utentes.menu1.adicionaOpcao("seguinte", "Seguinte", myPath + "\imagens\icons\ponta_seta_rigth_w.png", "uf_utentes_seguinte","F6")
	utentes.menu1.adicionaOpcao("sinc_RNU", "Sinc RNU", myPath + "\imagens\icons\actualizar_w.png", "uf_utentes_sincUtente", "F7")
	**utentes.menu1.adicionaOpcao("anexos", "Anexos", myPath + "\imagens\icons\anexar_w.png", "uf_anexardoc_chama with 'b_utentes', ALLTRIM(cl.utstamp), 'Utente '+ ALLTRIM(STR(cl.no)), ALLTRIM(STR(cl.no))", "F8")

	
	** Configura menu de Aplica��es
	utentes.menu_aplicacoes.adicionaOpcao("faturacao", "Documentos Fatura��o", "", "uf_facturacao_chama with ''")
	utentes.menu_aplicacoes.adicionaOpcao("recebimentos", "Recebimentos", "", "uf_utentes_chamaRecebimentos")
	utentes.menu_aplicacoes.adicionaOpcao("cartaoCliente", "CRM", "", "uf_utentes_chamaCartaoCliente")
	utentes.menu_aplicacoes.adicionaOpcao("sms", "Comunica��es", "", "uf_SMS_chama with ''")
	utentes.menu_aplicacoes.adicionaOpcao("arquivodigital", "Arquivo Digital", "", "uf_arquivodigital_chama")
	&&utentes.menu_aplicacoes.adicionaOpcao("promocoes", "Promo��es", "", "uf_perguntalt_chama with 'Brevemente dispon�vel.','OK','',64")
	utentes.menu_aplicacoes.adicionaOpcao("prescricao", "Prescri��o", "", "uf_utentes_prescMed")
	utentes.menu_aplicacoes.adicionaOpcao("marcacoes", "Marca��es", "", "uf_marcacoes_chama")
	utentes.menu_aplicacoes.adicionaOpcao("calculadora", "Calculadora", "", "uf_calculadora_chama")
	utentes.menu_aplicacoes.adicionaOpcao("ftassociados", "Ft. Autom�tica", "", "uf_FATASSOCIADOS_chama")
	utentes.menu_aplicacoes.adicionaOpcao("vacinacao", "Registo Vacina��o", "", "uf_utentes_Vacinacao_chama")

	IF uf_gerais_getParameter('ADM0000000082', 'TEXT') == "CLINICA"
		utentes.menu_aplicacoes.adicionaOpcao("convencoes", "Conven��es", "", "uf_pesqconvencoes_chama")
	ENDIF
	
	IF uf_gerais_getParameter("ADM0000000224","BOOL") == .t. && Clinica
		utentes.menu_aplicacoes.adicionaOpcao("facturacaoEntidadesClinica","Emiss�o Fatura��o","","uf_FACTENTIDADESCLINICA_chama")
	ENDIF 
		
	&& configura menu de Op��es
	utentes.menu_opcoes.adicionaOpcao("resumos", "Imprimir Resumos", "", "uf_utentes_chamaResumos")
	&&utentes.menu_opcoes.adicionaOpcao("cartao", "Imprimir Cart�o H", "", "uf_utentes_imprimirCartaoH")
	utentes.menu_opcoes.adicionaOpcao("etiquetacc", "Imprimir Etiqueta CC", "", "uf_UTENTES_imprimirCartao")
	utentes.menu_opcoes.adicionaOpcao("cc", "Conta Corrente", "", "uf_utentes_chamaCC")
	utentes.menu_opcoes.adicionaOpcao("ccnr", "Conta Corrente n/Reg.", "", "uf_utentes_chamaCCNR")
	utentes.menu_opcoes.adicionaOpcao("documentos", "Consulta Documentos", "", "uf_utentes_chamaDocumentos")
	utentes.menu_opcoes.adicionaOpcao("proforma", "Emitir FT. Proforma", "", "uf_utentes_criaProforma")
	utentes.menu_opcoes.adicionaOpcao("imprimir", "Imprimir", "", "uf_ImprimirCLFL_chama with 'B_UTENTES'")
	**utentes.menu_opcoes.adicionaOpcao("eliminar", "Eliminar", "", "uf_utentes_eliminar") ** Retirado TD-48086
	utentes.menu_opcoes.adicionaOpcao("duplicar", "Duplicar\Inativar", "", "uf_butentes_duplicar")
	utentes.menu_opcoes.adicionaOpcao("ultimo", "�ltimo", "", "uf_utentes_navegaUltRegisto")
	utentes.menu_opcoes.adicionaOpcao("Envios","Envio de Informa��es","","uf_ENVIOS_chama with 'UTENTES'")
	utentes.menu_opcoes.adicionaOpcao("anexos", "Anexos", "", "uf_anexardoc_chama with 'b_utentes', ALLTRIM(cl.utstamp), 'Utente '+ ALLTRIM(STR(cl.no)), ALLTRIM(STR(cl.no))")
	utentes.menu_opcoes.adicionaOpcao("log", "Log de acessos", "", "uf_LOGCliente_chama with ALLTRIM(cl.utstamp)")
	utentes.menu_opcoes.adicionaOpcao("esquece", "RGPD-Esquecimento", "", "uf_DIREITOS_chama with ALLTRIM(cl.utstamp), 'b_utentes', 'esquecimento'")
	utentes.menu_opcoes.adicionaOpcao("token", "RGPD-C�d. Aprova��o", "", "uf_enviotoken_chama with ALLTRIM(cl.utstamp), 'b_utentes'")
	utentes.menu_opcoes.adicionaOpcao("oposi��o", "RGPD-Limita��o", "", "uf_DIREITOS_chama with ALLTRIM(cl.utstamp), 'b_utentes', 'oposicao'")
	utentes.menu_opcoes.adicionaOpcao("enviodados", "RGPD-Portabilidade", "", "uf_DIREITOS_chama with ALLTRIM(cl.utstamp), 'b_utentes', 'portabilidade'")

	IF uf_gerais_getParameter_site('ADM0000000067', 'BOOL', mySite) = .t. AND ALLTRIM(ch_grupo) <> 'Administrador' AND ALLTRIM(ch_grupo) <> 'Supervisores'
		utentes.menu1.estado("opcoes, aplicacoes", "HIDE")
	ENDIF 
	
	IF emdesenvolvimento == .t.
		utentes.menu_opcoes.adicionaOpcao("actCC", "Corrigir CC", "", "uf_utentes_CorrigeCC")
	ENDIF
	utentes.Menu1.gravar.img1.Picture = myPath + "\imagens\icons\guardar_w.png"
	
ENDFUNC

**
FUNCTION uf_utentes_prescMed
	LPARAMETERS lnPesqUtentes
		initpesq=1
		IF lnPesqUtentes == .f.
			uf_prescricao_chama("",.t.,ALLTRIM(cl.utstamp),.f.)
		ELSE
			uf_prescricao_chama("",.f.,'',.f.)
		ENDIF
ENDFUNC

**
FUNCTION uf_utentes_actOutrosBenef
	&& Cursor de outros beneficios
	lcSQL = ''	
	TEXT TO lcSql NOSHOW textmerge
		exec up_prescricao_outrosBenef '<<ALLTRIM(cl.utstamp)>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("utentes.pageframe1.page7.grid1","uCrsOutrosBenef",lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A OS OUTROS BENEF�CIOS.","OK","",16)
		RETURN .f.
	ENDIF
ENDFUNC

**
FUNCTION uf_utentes_actEFRs
	&& Cursor de EFRs
	lcSQL = ''	
	TEXT TO lcSql NOSHOW textmerge
		exec up_prescricao_efrutente '<<ALLTRIM(cl.utstamp)>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("utentes.pageframe1.page7.grid2","uCrsEFRs",lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar EFRs associadas ao utentes.","OK","",16)
		RETURN .f.
	ENDIF
ENDFUNC

**
FUNCTION uf_utentes_sincUtente

	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()
	
	IF !myClIntroducao AND !myClAlteracao
		uf_perguntalt_chama("S� pode sincronizar utentes quando o ecr� se encontra em modo de introdu��o ou altera��o.","OK","",64)
		RETURN .f.
	ENDIF

	&& valida��es
	*nutente
*!*		SELECT cl
*!*		IF EMPTY(ALLTRIM(cl.nbenef)) OR LEN(ALLTRIM(cl.nbenef)) != 9
*!*			uf_perguntalt_chama("O n� de Utente � inv�lido. Por favor valide.","OK","",64)
*!*			RETURN .f.		
*!*		ENDIF
	*internet
	IF !uf_gerais_verifyInternet()
		uf_perguntalt_chama("ESTA FUNCIONALIDADE REQUER LIGA��O � INTERNET."+CHR(13)+"POR FAVOR VERIFIQUE A SUA LIGA��O.","OK","",64)
		RETURN .f.
	ENDIF
	**
	
	regua(0,13,"A obter dados do Registo Nacional de Utentes...")
	regua(1,1,"A obter dados do Registo Nacional de Utentes...")
	
	utentes.stampTmrSinc = uf_gerais_stamp()
	
	LOCAL lcTeste, lctest

	SELECT uCrsE1
	lcClid = ALLTRIM(uCrsE1.id_lt)

	IF uf_gerais_getParameter("ADM0000000227","BOOL")	
		lcTeste = 1
	ELSE
		lcTeste = 0
	ENDIF
	
	**	
	SELECT cl
	IF DIRECTORY(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\PE\RNU')
		
		IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\PE\RNU\RNU.jar')
			uf_perguntalt_chama("Ficheiro configura��o n�o encontrado. Contacte o Suporte","OK","",64)
			RETURN .f.
		ENDIF 
		lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\PE\RNU\RNU.jar';
		 	+ ' "--connStamp=' + ALLTRIM(utentes.stampTmrSinc) + ["];
		 	+ ' "--usernr=' + ALLTRIM(cl.nbenef) + ["];
		 	+ ' "--cardnr=' + cl.cesdcart + ["];
		 	+ ' "--cardtype=' + IIF(!EMPTY(cl.cesd),"CESD","") + ["];
		 	+ ' "--idCL=' + lcClId + ["];
		 	+ ' "--test=' + ALLTRIM(STR(lcTeste)) + ["]
		 	
		IF 	lcTeste = 1 	
			_cliptext = lcWsPath
			messagebox("WEBSERVICE DE TESTE...")
			messagebox(lcWsPath)
		ENDIF
	
		lcWsPath = "javaw -jar " + lcWsPath 
		
		oWSShell = CREATEOBJECT("WScript.Shell")
		oWSShell.Run(lcWsPath,1,.f.)
		
		regua(1,2,"A obter dados do Registo Nacional de Utentes...")
		utentes.tmrSinc.enabled = .t.
	ENDIF
ENDFUNC

**
FUNCTION uf_utentes_tmrSinc
	utentes.ntmrSinc = utentes.ntmrSinc + 1
	IF utentes.ntmrSinc > 5 && 5 tentativas de 3 segundos
		regua(2)
		uf_perguntalt_chama("N�o foi poss�vel obter resposta do RNU. Por favor volte a tentar sincronizar.","OK","",16)
		utentes.tmrSinc.enabled = .f.
		utentes.ntmrSinc = 0
		utentes.stampTmrSinc = ''
		RETURN .f.
	ENDIF
	regua(1,utentes.ntmrSinc + 2,"A obter dados do Registo Nacional de Utentes...")
	TEXT TO lcSql NOSHOW textmerge
		exec up_prescricao_dados '<<ALLTRIM(utentes.stampTmrSinc)>>'
	ENDTEXT

	IF !uf_gerais_actGrelha("","uCrsVerificaRespostaUtente",lcSql)
		regua(2)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA A VERIFICAR A RESPOSTA DO WEB SERVICE UTENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		utentes.tmrSinc.enabled = .f.
		utentes.ntmrSinc = 0
		utentes.stampTmrSinc = ''
	ELSE
	
		SELECT uCrsVerificaRespostaUtente
		IF RECCOUNT("uCrsVerificaRespostaUtente") > 0
			SELECT uCrsVerificaRespostaUtente
			GO TOP
			
			*Verifica se n�o existe codigo de anomalia
			IF ALLTRIM(uCrsVerificaRespostaUtente.mensagemCodigo) != "2200101001"  AND ALLTRIM(uCrsVerificaRespostaUtente.mensagemCodigo) != "2200201001" 
				regua(2)
				uf_perguntalt_chama("N�o foi possivel actualizar dados do utente." + CHR(13) + "Motivo: "+ UPPER(ALLTRIM(uCrsVerificaRespostaUtente.mensagemDescricao)),"OK","",64)

				utentes.tmrSinc.enabled = .f.
				utentes.ntmrSinc = 0
				utentes.stampTmrSinc = ''
				RETURN .f.
			ENDIF
			
			*Identifica��o
			SELECT cl
			replace ;
				cl.nome 		 WITH uCrsVerificaRespostaUtente.NomeCompleto ;
				cl.nascimento	 WITH uCrsVerificaRespostaUtente.DataNascimento ;
				cl.sexo			 WITH uCrsVerificaRespostaUtente.Sexo ;
				cl.codigoP		 WITH uCrsVerificaRespostaUtente.PaisNacionalidade ;
				cl.codigoPNat	 WITH uCrsVerificaRespostaUtente.PaisNacionalidade ;
				cl.duplicado	 WITH uCrsVerificaRespostaUtente.duplicado ;
				cl.nomesproprios WITH uCrsVerificaRespostaUtente.nomesproprios ;
				cl.obito	  	 WITH uCrsVerificaRespostaUtente.obito ;
				cl.apelidos		 WITH uCrsVerificaRespostaUtente.apelidos
			
			utentes.pageframe1.page1.cmbcodPNat.programmaticchange
			utentes.pageframe1.page1.cmbcodigoP.programmaticchange
			
			IF 	uCrsVerificaRespostaUtente.Duplicado == "S"
				SELECT cl
				replace cl.nbenef WITH uCrsVerificaRespostaUtente.NumeroSNS
			ENDIF
			
			*RECM
			SELECT cl
			replace ;
				cl.recmmotivo		WITH uCrsVerificaRespostaUtente.BenefRECMMotivo ;
				cl.recmdescricao	WITH uCrsVerificaRespostaUtente.BenefRECMDescricao ;
				cl.recmdatainicio	WITH uCrsVerificaRespostaUtente.BenefRECMDataInicio ;
				cl.recmdatafim		WITH uCrsVerificaRespostaUtente.BenefRECMDataFim
		
			&& Cursor de EFRs
			lcSQL=''
			TEXT TO lcSQL TEXTMERGE NOSHOW 
				
				DELETE FROM b_cli_efrutente WHERE stamp = '<<cl.utstamp>>'
				
				UPDATE 
					b_cli_efrutente
				SET 
					stamp = '<<cl.utstamp>>'
				where 
					stamp = '<<utentes.stampTmrSinc>>'
			ENDTEXT 
			uf_gerais_actGrelha("","",lcSQL)
	
			lcSQL = ''	
			TEXT TO lcSql NOSHOW textmerge
				exec up_prescricao_efrutente '<<ALLTRIM(cl.utstamp)>>'
			ENDTEXT
			IF !uf_gerais_actGrelha("utentes.pageframe1.page7.grid2","uCrsEFRs",lcSql)
				uf_perguntalt_chama("Ocorreu um erro a verificar EFRs associadas ao utentes.","OK","",16)
				RETURN .f.
			ENDIF
			
			*EFR
			SELECT uCrsEFRs
			GO Top
			
			IF RECCOUNT("uCrsEFRs")> 0 
				SELECT uCrsEFRs
				GO Top
				SELECT cl
				replace ;
					cl.entPla	WITH uCrsEFRs.abrev ;
					cl.codPla	WITH uCrsEFRs.Codigo ;
					cl.desPla	WITH uCrsEFRs.Descricao ;
					cl.nbenef2	WITH uCrsEFRs.numeroBenefEntidade;
					cl.valipla2	WITH uCrsEFRs.DataValidade
			ELSE
			
				IF !EMPTY(ALLTRIM(cl.nbenef))
					SELECT cl
					replace ;
						cl.codPla	WITH "935601" ;
						cl.desPla	WITH "Servi�o Nacional de Sa�de" ;
						cl.nbenef2	WITH "";
						cl.valipla2	WITH ctod("1900.01.01")
						
					SELECT 	uCrsEFRs
					APPEND BLANK
					Replace uCrsEFRs.stamp WITH cl.utstamp
					Replace uCrsEFRs.Codigo WITH "935601"
					Replace uCrsEFRs.Descricao WITH "Servi�o Nacional de Sa�de"
				ELSE
					SELECT cl
					replace ;
						cl.codPla	WITH "999998" ;
						cl.desPla	WITH "Sem Comparticipa��o pelo SNS" ;
						cl.nbenef2	WITH "";
						cl.valipla2	WITH ctod("1900.01.01")
						
					SELECT 	uCrsEFRs
					APPEND BLANK
					Replace uCrsEFRs.stamp WITH cl.utstamp
					Replace uCrsEFRs.Codigo WITH "999998"
					Replace uCrsEFRs.Descricao WITH "Sem Comparticipa��o pelo SNS"
				ENDIF
			ENDIF

			*IT
			SELECT cl
			replace ;
				cl.itmotivo		WITH uCrsVerificaRespostaUtente.BenefITMMotivo ;
				cl.itdescricao	WITH uCrsVerificaRespostaUtente.BenefITDescricao ;
				cl.itdatainicio	WITH uCrsVerificaRespostaUtente.BenefITDataInicio ;
				cl.itdatafim	WITH uCrsVerificaRespostaUtente.BenefITDataFim
			
			*Outros Beneficios
			lcSQL=''
			TEXT TO lcSQL TEXTMERGE NOSHOW 
				DELETE FROM b_cli_outrosBenef WHERE stamp = '<<cl.utstamp>>'
			
				UPDATE 
					b_cli_outrosBenef
				SET 
					stamp = '<<cl.utstamp>>'
				where 
					stamp = '<<utentes.stampTmrSinc>>'
			ENDTEXT 
			uf_gerais_actGrelha("","",lcSQL)
			
			lcSQL = ''	
			TEXT TO lcSql NOSHOW TEXTMERGE 
				exec up_prescricao_outrosBenef '<<ALLTRIM(cl.utstamp)>>'
			ENDTEXT

			IF !uf_gerais_actGrelha("utentes.pageframe1.page7.grid1","uCrsOutrosBenef",lcSql)
				uf_perguntalt_chama("OCORREU UM ERRO A VERIFICAR OS OUTROS BENEF�CIOS.","OK","",16)
				RETURN .f.
			ENDIF
				
			**Calculo do Regime de Comparticipa��o RECMTipo - est� dependente dos beneficios e outros beneficios
			** 
			lcCodigosBenf = ""
			SELECT uCrsOutrosBenef
			GO Top
			SCAN FOR !EMPTY(ALLTRIM(uCrsOutrosBenef.motivo))
				IF !EMPTY(ALLTRIM(lcCodigosBenf))
					lcCodigosBenf = ALLTRIM(lcCodigosBenf) + "," + ALLTRIM(uCrsOutrosBenef.motivo)
				ELSE
					lcCodigosBenf = ALLTRIM(uCrsOutrosBenef.motivo)
				ENDIF
			ENDSCAN 
			
			SELECT uCrsVerificaRespostaUtente
			IF !EMPTY(ALLTRIM(lcCodigosBenf))
				lcCodigosBenf = ALLTRIM(lcCodigosBenf) + "," + ALLTRIM(uCrsVerificaRespostaUtente.BenefRECMMotivo)
			ELSE
				lcCodigosBenf = ALLTRIM(uCrsVerificaRespostaUtente.BenefRECMMotivo)
			ENDIF
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				exec up_prescricao_RecmTipo '<<lcCodigosBenf>>',0
			ENDTEXT
		
			IF !uf_gerais_actGrelha("","uCrsTipoRECM",lcSQL)
				uf_perguntalt_chama("N�o foi poss�vel verificar Regime de comparticipa��o.","OK","",16)
				return .f.
			ENDIF
			**
						
			SELECT uCrsTipoRECM
			SELECT cl
			replace cl.recmtipo	WITH uCrsTipoRECM.tipo
					
			** No caso de encontrar EFR
			
			regua(2)
			IF utentes.semperguntas == .f.
				uf_perguntalt_chama("DADOS DO UTENTE ACTUALIZADOS COM SUCESSO.","OK","",64)
			ENDIF
			utentes.tmrSinc.enabled = .f.
			utentes.ntmrSinc = 0
			utentes.stampTmrSinc = ''
		ENDIF
	ENDIF 
	
	utentes.refresh()
ENDFUNC 


**
FUNCTION uf_utentes_chamaRecebimentos
	LPARAMETERS lnPesqUtentes
	**MESSAGEBOX('1')
	**Valida Licenciamento	
	IF (uf_ligacoes_addConnection('CLI') == .f.)
		RETURN .F.
	ENDIF
	
	IF TYPE("ATENDIMENTO") != "U" AND UtilPlafond = .f.
		uf_perguntalt_chama("Esta op��o n�o pode ser utilizada com o ecr� de Atendimento aberto." + CHR(13) + CHR(13) + "Por favor verifique.","OK","",64)
		return .f.
	ENDIF 
	IF TYPE("UtilPlafond") == "U"
		&& CONTROLA PERFIS DE ACESSO
		IF (uf_gerais_validaPermUser(ch_userno,ch_grupo,'Recebimentos de Clientes'))
			IF lnPesqUtentes == .f.
				IF USED("CL")
					SELECT CL
					IF CL.no == 200
						uf_perguntalt_chama("N�o pode emitir recibos para o cliente Consumidor Final.","OK","",64)
						RETURN .f.
					ENDIF 
				ENDIF
			ENDIF 	
			uf_regvendas_chama(.t.)
			uf_regvendas_VerRegCreditos()
			regVendas.menu1.estado("voltar","HIDE")
			IF TYPE("regVendas.menu1.recibos") == "U"
				regVendas.menu1.adicionaOpcao("recibos","Recibos",myPath + "\imagens\icons\doc_re_w.png","uf_regvendas_gerirRecibos","G")
			ENDIF
			regVendas.menu1.refresh()
			regVendas.windowstate = 2
		ELSE
			uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL DE RECEBIMENTOS DE CLIENTES.","OK","",48)
		ENDIF
	ELSE
		IF UtilPlafond = .t.
			uf_regvendas_chama(.t.)
			uf_regvendas_VerRegCreditos()
			uf_regvendas_porValor()
			regVendas.menu1.estado("voltar, opcoes, selTodos, nTaloes","HIDE")
			
			regvendas.pgfReg.Page2.chkPorProduto.visible=.f.
			regvendas.pgfReg.Page2.chkPorValor.visible=.f.
			regvendas.pgfReg.Page2.ChkSoSusp.visible=.f.
			regvendas.pgfReg.Page2.ChkSoNaoSusp.visible=.f.
			regvendas.pgfReg.Page2.chkPorUtente.visible=.f.
			regvendas.txtNVenda.visible=.f.
			regvendas.txtCliente.visible=.f.
			regvendas.txtNo.visible=.f.
			regvendas.txtEstab.visible=.f.
			regvendas.txtProduto.visible=.f.
			regvendas.txtDataIni.visible=.f.
			regvendas.txtDataFim.visible=.f.
			regvendas.lblNVenda.visible=.f.
			regvendas.lblCliente.visible=.f.
			regvendas.lblProduto.visible=.f.
			regvendas.lblData.visible=.f.
			regvendas.Label1.visible=.f.
			regvendas.txtNAtend.value = ALLTRIM(nrAtendimentoRec)
			regvendas.lblA.visible=.f.
			regvendas.pgfReg.Page2.chkMovCaixa.visible=.f.
			regvendas.pgfReg.Page2.lblData.visible=.f.
			regvendas.pgfReg.Page2.txtDataRec.visible=.f.
			regvendas.pgfReg.Page2.chkRecEnt.visible=.f.
			regvendas.pgfReg.Page2.Label4.visible=.f.
			regvendas.pgfReg.Page2.txtOlcodigo.visible=.f.
			regvendas.pgfReg.Page2.Label6.visible=.f.
			regvendas.pgfReg.Page2.txtOllocal.visible=.f.
			regvendas.pgfReg.Page2.Label19.visible=.f.
			regvendas.pgfReg.Page2.txtDescPerc.visible=.f.
			regvendas.pgfReg.Page2.Label1.visible=.f.
			regvendas.pgfReg.Page2.txtDescEuro.visible=.f.
			regvendas.pgfReg.Page2.Label2.visible=.f.
			**regvendas.pgfReg.Page2.txtOllocal.visible=.f.
			
			regVendas.pgfReg.page2.chkPorProduto.tag = "false"
			regVendas.menu1.refresh()
			regVendas.windowstate = 2
			
			uf_regvendas_pesquisarRegCreditos()
			Text to lcSql noshow textmerge
				exec up_touch_regularizarCreditosPorValor -1, '', -1, -1, '<<uf_gerais_getdate(REGVENDAS.txtDataIni.value, "SQL")>>', '<<uf_gerais_getdate(REGVENDAS.txtDataFim.value, "SQL")>>', '<<ALLTRIM(nrAtendimentoRec)>>', <<ch_userno>>, '<<Alltrim(ch_grupo)>>', '<<mySite>>'
			ENDTEXT
			uf_gerais_actGrelha("REGVENDAS.pgfReg.page2.grdRegCreditoValor","uCrsRegCreditosValor",lcSQL)

			LOCAL lcSomaRestante, lcValadescontar
			STORE 0 TO lcSomaRestante, lcValadescontar
			
			SELECT uCrsValsPagam
			GO TOP 
			REGVENDAS.ctnPagamento.txtDinheiro.value = ALLTRIM(STR(uCrsValsPagam.pagam1,15,2))
			lcSomaRestante = lcSomaRestante + uCrsValsPagam.pagam1
			REGVENDAS.ctnPagamento.txtMB.value = ALLTRIM(STR(uCrsValsPagam.pagam2,15,2))
			lcSomaRestante = lcSomaRestante + uCrsValsPagam.pagam2
			REGVENDAS.ctnPagamento.txtVisa.value = ALLTRIM(STR(uCrsValsPagam.pagam3,15,2))
			lcSomaRestante = lcSomaRestante + uCrsValsPagam.pagam3
			REGVENDAS.ctnPagamento.txtCheque.value = ALLTRIM(STR(uCrsValsPagam.pagam4,15,2))
			lcSomaRestante = lcSomaRestante + uCrsValsPagam.pagam4
			REGVENDAS.ctnPagamento.txtModPag3.value = ALLTRIM(STR(uCrsValsPagam.pagam5,15,2))
			lcSomaRestante = lcSomaRestante + uCrsValsPagam.pagam5
			REGVENDAS.ctnPagamento.txtModPag4.value = ALLTRIM(STR(uCrsValsPagam.pagam6,15,2))
			lcSomaRestante = lcSomaRestante + uCrsValsPagam.pagam6
			REGVENDAS.ctnPagamento.txtModPag5.value = ALLTRIM(STR(uCrsValsPagam.pagam7,15,2))
			lcSomaRestante = lcSomaRestante + uCrsValsPagam.pagam7
			**REGVENDAS.ctnPagamento.txtModPag6.value = ALLTRIM(STR(uCrsValsPagam.pagam8))
			lcValadescontar = uCrsValsPagam.pagam8
			
			**LOCAL lcValadescontar
			**lcValadescontar = PLafondDispUt
			SELECT uCrsRegCreditosValor
			GO TOP 
			DELETE FROM uCrsRegCreditosValor WHERE ALLTRIM(uCrsRegCreditosValor.nmdoc) <> 'Factura'
			SELECT uCrsRegCreditosValor
			GO TOP 
			SCAN 
				replace uCrsRegCreditosValor.regularizar WITH lcSomaRestante 
				regvendas.ctnPagamento.txtValAtend.value = lcSomaRestante 
*!*					replace uCrsRegCreditosValor.regularizar WITH uCrsRegCreditosValor.valornreg - lcValadescontar 
*!*					regvendas.ctnPagamento.txtValAtend.value = uCrsRegCreditosValor.valornreg - lcValadescontar 
				replace uCrsRegCreditosValor.sel WITH .t.	
			ENDSCAN 
			
			uf_regVendas_configGrdRegCreditos()
			
			regvendas.menu1.estado("", "", "Gravar", .t., "Sair", .f.)
			
			uf_regvendas_gravar()
			
		ENDIF 
	ENDIF 
ENDFUNC


**
FUNCTION uf_utentes_alternaMenu

	
	IF myClIntroducao OR myClAlteracao
		utentes.menu1.estado("opcoes, aplicacoes, pesquisar, novo, editar, anterior, seguinte", "HIDE", "Gravar", .t., "Sair", .t.) && atualizar
		
		IF UPPER(ALLTRIM(uCrsE1.tipoempresa)) != "CLINICA"
			utentes.menu_aplicacoes.estado("prescricao", "HIDE")
			utentes.menu1.estado("sinc_RNU", "HIDE", "Gravar", .t., "Sair", .t.)
		ELSE
			**utentes.menu_opcoes.estado("eliminar", "HIDE") ** Retirado TD-48086
			utentes.menu1.estado("sinc_RNU", "SHOW", "Gravar", .t., "Sair", .t.)
		ENDIF 
		
	ELSE
	
		IF UPPER(ALLTRIM(uCrsE1.tipoempresa)) != "CLINICA"
			utentes.menu_aplicacoes.estado("prescricao", "HIDE")
		ENDIF 

		utentes.menu1.estado("opcoes, aplicacoes,pesquisar, novo, editar, anterior, seguinte", "SHOW", "Gravar", .f., "Sair", .t.) && atualizar
**		IF uf_gerais_getParameter_site('ADM0000000013', 'BOOL') == .t.
**			utentes.menu1.estado("anexos", "SHOW")
**		ELSE
**			utentes.menu1.estado("anexos", "HIDE")
**		ENDIF 
		utentes.menu1.estado("sinc_RNU", "HIDE", "Gravar", .f., "Sair", .t.)
		**utentes.menu_opcoes.estado("eliminar", "SHOW") ** Retirado TD-48086
	ENDIF
	
	IF uf_gerais_getParameter_site('ADM0000000067', 'BOOL', mySite) = .t. AND ALLTRIM(ch_grupo) <> 'Administrador' AND ALLTRIM(ch_grupo) <> 'Supervisores'
		utentes.menu1.estado("opcoes, aplicacoes", "HIDE")
	ENDIF 
ENDFUNC


**
FUNCTION uf_utentes_carregaDados
	LPARAMETERS nNo, nEstab
	
	LOCAL lcSql
	lcSql = ''
	
	&& Cursor da CL
	TEXT TO lcSql NOSHOW textmerge
		SELECT Top 1 
			b_utentes.* 
			,pontos = isnull(b_fidel.pontosAtrib,0) - isnull(b_fidel.pontosUsa,0)
			,binoValidade = convert(datetime,isnull(cc.validade_fim, '19000101'))
			,compfixa = isnull(compfixa,0)
		FROM 
			b_utentes (nolock) 
			left join b_fidel (nolock)  on b_utentes.no = b_fidel.clno and b_utentes.estab = b_fidel.clestab
			left join dispensa_eletronica_cc cc (nolock) on b_utentes.utstamp = cc.utstamp
			left join cptorg (nolock) on cptorg.u_no=b_utentes.no
		where 
			b_utentes.no = <<nNo>> 
			and b_utentes.estab = <<nEstab>>
	ENDTEXT
	IF !uf_gerais_actgrelha("", "cl", lcSql)
		RETURN .f.
	ENDIF
	
	SELECT cl
	
	&& Cursor de Dependentes
	TEXT TO lcSql NOSHOW textmerge
		select 
			estab
			,nome
			,morada
			,telefone
			,fax
			,email
			,esaldo 
			,entFact
		from 
			b_utentes (NOLOCK) 
		where 
			b_utentes.no=<<nNo>> 
			and b_utentes.estab != <<nEstab>>
	ENDTEXT
	IF !uf_gerais_actgrelha("utentes.pageframe1.page3.grdEstab", "uCrsEstabCL", lcSql)
		RETURN .f.
	ENDIF

	&& Cursor de patologias do Cliente
	utentes.pageframe1.page2.list2.rowsource =	''
	
	TEXT TO lcSql NOSHOW textmerge
		exec up_clientes_patologias '<<ALLTRIM(cl.utstamp)>>'
	ENDTEXT
	IF !uf_gerais_actgrelha("", "uCrsPatologiasCL", lcSql)
		RETURN .f.
	ENDIF
	
	utentes.pageframe1.page2.list2.rowsource = 'uCrsPatologiasCL'
	
	&& Cursor de Diplomas Cliente
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_utentes_DiplomaUtente '<<ALLTRIM(cl.utstamp)>>'
	ENDTEXT 

	IF !(uf_gerais_actGrelha("UTENTES.Pageframe1.page7.GridDiplomas","uCrsDiplomasID",lcSQL))
		uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR OS DIPLOMAS.","OK","",16)
		regua(2)
		RETURN .f.
	ENDIF
	
	&& Saldo conta corrente
	IF nNo == 200
		utentes.pageframe1.page4.txtEsaldo.value = 0
	ELSE
		TEXT TO lcSQL TEXTMERGE NOSHOW
			select 
				total = ISNULL(sum(edeb - edebf) - sum(ecred - ecredf),0)
			from cc (nolock) 
			where 
				no = <<nNo>> 
				and estab = <<nEstab>>
		ENDTEXT
		IF !uf_gerais_ActGRelha("","uCrsSaldoCCDep",lcSQL)
			RETURN .f.
		ELSE
			SELECT uCrsSaldoCCDep
			IF RECCOUNT("uCrsSaldoCCDep") > 0
				utentes.pageframe1.page4.txtEsaldo.value = uCrsSaldoCCDep.total
			ELSE
				utentes.pageframe1.page4.txtEsaldo.value = 0
			ENDIF 
		ENDIF 
		fecha("uCrsSaldoCCDep")
	ENDIF
		
	**
	uf_utentes_actRegistoClinico()

ENDFUNC


*********************
*	Configura Ecra	*
*********************
FUNCTION uf_utentes_confEcra

	** VERIFICAR SE O UTENTE EXERCEU A OP��O DE ESQUECIMENTO
	IF cl.removido=.f.
		utentes.pageframe1.enabled = .t.
		utentes.pageframe1.tabs = .f.
		utentes.pageframe1.enabled = .t.
		utentes.pageframe1.visible = .t.
		IF utentes.ctnTabs1.visible = .f.
			utentes.ctnTabs1.visible = .f.
			utentes.ctnTabs2.visible = .t.
		ELSE
			utentes.ctnTabs1.visible = .t.
			utentes.ctnTabs2.visible = .f.
		ENDIF 
		utentes.menu1.estado("opcoes, editar", "SHOW", "Gravar", .f., "Sair", .t.)
		
		IF uf_gerais_getParameter_site('ADM0000000061', 'BOOL', mySite) AND cl.no>200
			WITH UTENTES.pageframe1.page1
				.Label19.visible = .f.
				.txtsite_compart.visible = .f.
			ENDWITH
		ENDIF 
		
		** Controlo dos objectos por tipo de Cliente - Lu�s Leal 2015-07-21
		IF ALLTRIM(uCrsE1.tipoempresa) == "ASSOCIADOS"
			WITH UTENTES.pageframe1.page1
				.txtbino.visible = .f.
				.label14.visible = .f.
				.txtnascimento.visible = .f.
				.label30.visible = .f.
				.txtPai.visible = .f.
				.label17.visible = .f.
				.txtMae.visible = .f.
				.label21.visible = .f.
				.txtPeso.visible = .f.
				.label23.visible = .f.
				.txtAltura.visible = .f.
				.label31.visible = .f.
				.cmbsexo.visible = .f.
				.label13.visible = .f.
				.label4.visible = .f.
			ENDWITH
			UTENTES.Pageframe1.Page1.Shape6.Top=420
			UTENTES.Pageframe1.Page1.Label27.Top=423
			UTENTES.Pageframe1.Page1.CHECK1.Top=481
			UTENTES.Pageframe1.Page1.Edit1.Top=441
			
			UTENTES.pageframe1.page1.pageframe1.activepage = 2
		ENDIF
		
		IF ALLTRIM(cl.tipo)='SEG'
			UTENTES.Pageframe1.Page7.lblcompfixa.visible=.t.
			UTENTES.Pageframe1.Page7.txtcompfixa.visible=.t.
		ELSE 
			UTENTES.Pageframe1.Page7.lblcompfixa.visible=.f.
			UTENTES.Pageframe1.Page7.txtcompfixa.visible=.f.
		ENDIF 
		
		**
		IF !myClAlteracao AND !myClIntroducao
			WITH UTENTES
				.Caption	= "Gest�o de Utentes"	
		
				** Dados Principais
				.txtNome.readonly = .t.
				.txtNcont.readonly = .t.
				.txtNome2.readonly = .t.
				.txtu_nbenef.readonly = .t.
				.txtID.readonly = .t.
				.chkinactivo.enable(.f.)
				.txtTratamento.readonly = .t.
				
				** Pagina 1 - Dados Principais
				WITH UTENTES.pageframe1.page1
					.txtMorada.readonly = .t.
					.txtCodPost.readonly = .t.
					.txtLocal.readonly = .t.
					.txtTlmvl.readonly = .t.
					.txtTelefone.readonly = .t.
					.txtFax.readonly = .t.
					.txtEmail.readonly = .t.
					.txtURL.readonly = .t.
					.txtsite_compart.readonly = .t.
					.txtu_profi.readonly = .t.
					.txtbino.readonly = .t.
					.txtnascimento.readonly = .t.
					.txtPai.readonly = .t.
					.txtMae.readonly = .t.
					.txtPeso.readonly = .t.
					.txtAltura.readonly = .t.
					.Edit1.readonly = .t.
					.chkEntFact.enable(.f.)
					.chkEntCompart.enable(.f.)
					.chkAutorizaEmails.enable(.f.)
					.chkAutorizaSMS.enable(.f.)

				ENDWITH
				
				** Pagina 1 - Outros Dados
				WITH UTENTES.pageframe1.page1.pageframe1.page1
					.chknaoencomenda.enable(.f.)
					.chknocredit.enable(.f.)
					.chkcobnao.enable(.f.)
					.chkpacgen.enable(.f.)
					.chknotif.enable(.f.)
					.chkcativa.enable(.f.)
					.txtperccativa.readonly = (.t.)
				ENDWITH 
				
				WITH UTENTES.pageframe1.page1.pageframe1.page2
					.txtDt.readonly = .t.
					.txtProprietario.readonly = .t.
					.txtMoradaAlt.readonly = .t.
					.txtNoAssociado.readonly = .t.
					.txtAlvara.readonly = .t.
				ENDWITH	
				
				.pageframe1.page8.chkcesd.enable(.f.)
				.pageframe1.page8.chkatestado.enable(.f.)
				
				** Info. clinica
				.pageframe1.page2.btnAdiciona.enable(.f.)
				.pageframe1.page2.btnRemove.enable(.f.)
				
				** Financeiros
				.pageframe1.page4.chkParticular.enable(.f.)
				.pageframe1.page4.chkAutofact.enable(.f.)
				.pageframe1.page4.chkClivd.enable(.f.)
				.pageframe1.page4.noConvencao.readonly = .t.
				.pageframe1.page4.designConvencao.readonly = .t.
				.pageframe1.page4.gridservfat.desvio_mes_fat.readonly = .t.
				.pageframe1.page4.gridservfat.pvp_produto.readonly = .t.
				.pageframe1.page4.gridservfat.desconto_produto.readonly = .t.
				.pageframe1.page4.gridservfat.desconto_valor.readonly = .t.
				.pageframe1.page4.gridservfat.qtt_produto.readonly = .t.
				.pageframe1.page4.gridservfat.data_fat.readonly = .t.
				
				** Integra��o
				.pageframe1.page5.btnImg1.enable(.f.)			
				.pageframe1.page5.btnImg2.enable(.f.)
				.pageframe1.page5.btnImg3.enable(.f.)
				.pageframe1.page5.btnImg4.enable(.f.)
				.pageframe1.page5.btnImg5.enable(.f.)
				.pageframe1.page5.btnImg6.enable(.f.)
				.pageframe1.page5.btnImg7.enable(.f.)
				.pageframe1.page5.btnImg8.enable(.f.)
					
				&& PageFrame da Sede
				IF .txtEstab.Value > 0
					.pageframe1.page3.btnSede.Enable(.t.)
					.pageframe1.page3.btnNovo.Enable(.t.)
				ELSE
					.pageframe1.page3.btnSede.Enable(.t.)
					.pageframe1.page3.btnNovo.Enable(.t.)
				ENDIF

				&& Configura campos obrigat�rios - preciso verificar isto para verificar se parametros que obrigam est�o ativos ou n�o
				
				.txtNome.backcolor								= RGB(191,223,223)
				
				IF uf_gerais_getParameter('ADM0000000022', 'BOOL') &&telefone
					.pageframe1.page1.txtTelefone.backcolor 	= rgb[191,223,223]
				ENDIF
				
				IF uf_gerais_getParameter('ADM0000000023', 'BOOL') &&data nascimento
					.pageframe1.page1.txtnascimento.backcolor 	= rgb[191,223,223]
				ENDIF
				
				IF uf_gerais_getParameter('ADM0000000046', 'BOOL') &&NIF
					.txtNCONT.backcolor 	= rgb[191,223,223]
				ENDIF
				
				IF uf_gerais_getParameter('ADM0000000112', 'BOOL') &&morada
					.pageframe1.page1.txtMorada.backcolor 	= rgb[191,223,223]
				ENDIF
				
	*!*			 	.pageframe1.txtNcont.backcolor 					= RGB(191,223,223)
	*!*				.pageframe1.page1.txtTelefone.backcolor 		= RGB(191,223,223)
	*!*				.pageframe1.page1.txtnascimento.backcolor		= RGB(191,223,223)
	*!*				.pageframe1.page1.txtMorada.backcolor 			= RGB(191,223,223)
			
				.pageframe1.page7.container1.btnPlano.enable(.f.)
				
			ENDWITH
		ELSE
			IF myClIntroducao
				utentes.Caption	= "Gest�o de Utentes - Modo de Introdu��o"	
			ELSE
				utentes.Caption	= "Gest�o de Utentes - Modo de Altera��o"	
			ENDIF
			
			** Pageframe
			**utentes.pageframe1.enabled			=	.T.
			
			** Dados Principais
			utentes.txtNome.readonly			=	.f.
			utentes.txtNcont.readonly			=	.f.
			utentes.txtNome2.readonly			=	.f.
			utentes.txtu_nbenef.readonly		=	.f.
			utentes.txtID.readonly				=	.f.
			utentes.txtTratamento.readonly 		= .f.
			utentes.chkinactivo.enable(.t.)
			
			SELECT cl
			IF cl.Estab == 0
				utentes.pageframe1.page1.chkEntCompart.enable(.t.)
			ELSE
				utentes.pageframe1.page1.chkEntCompart.enable(.f.)
			ENDIF 
			
			** Pagina 1  - Dados Principais
			WITH UTENTES.pageframe1.page1
				.txtMorada.readonly = .f.
				.txtCodPost.readonly = .f.
				.txtLocal.readonly = .f.
				.txtTlmvl.readonly = .f.
				.txtTelefone.readonly = .f.
				.txtFax.readonly = .f.
				.txtEmail.readonly = .f.
				.txtURL.readonly = .f.
				.txtsite_compart.readonly = .f.
				.txtu_profi.readonly = .f.
				.txtbino.readonly = .f.
				.txtnascimento.readonly = .f.
				.txtPai.readonly = .f.
				.txtMae.readonly = .f.
				.txtPeso.readonly = .f.
				.txtAltura.readonly = .f.
				.Edit1.readonly = .f.
				.chkEntFact.enable(.t.)
				.chkAutorizaEmails.enable(.f.)
				.chkAutorizaSMS.enable(.f.)			
			ENDWITH
			
			WITH UTENTES.pageframe1.page1.pageframe1.page1
				.chknaoencomenda.enable(.t.)
				.chknocredit.enable(.t.)
				.chkcobnao.enable(.t.)
				.chkpacgen.enable(.t.)
				.chknotif.enable(.t.)
				.chkcativa.enable(.t.)
				.txtperccativa.readonly = (.t.)
			ENDWITH		
			
			WITH UTENTES.pageframe1.page1.pageframe1.page2
				.txtDt.readonly = .f.
				.txtProprietario.readonly = .f.
				.txtMoradaAlt.readonly = .f.
				.txtNoAssociado.readonly = .f.
				.txtAlvara.readonly = .f.
			ENDWITH	
			
			utentes.pageframe1.page8.chkcesd.enable(.t.)
			utentes.pageframe1.page8.chkatestado.enable(.t.)
			**utentes.pageframe1.enabled			=	.T.
			
			** Info. clinica
			utentes.pageframe1.page2.btnAdiciona.enable(.t.)
			utentes.pageframe1.page2.btnRemove.enable(.t.)
			
			** Financeiros
			utentes.pageframe1.page4.chkParticular.enable(.t.)
			utentes.pageframe1.page4.chkAutofact.enable(.t.)
			utentes.pageframe1.page4.chkClivd.enable(.t.)
			utentes.pageframe1.page4.noConvencao.readonly = .f.
			utentes.pageframe1.page4.designConvencao.readonly = .f.
			utentes.pageframe1.page4.gridservfat.desvio_mes_fat.readonly = .f.
			utentes.pageframe1.page4.gridservfat.pvp_produto.readonly = .f.
			utentes.pageframe1.page4.gridservfat.desconto_produto.readonly = .f.
			utentes.pageframe1.page4.gridservfat.desconto_valor.readonly = .f.
			utentes.pageframe1.page4.gridservfat.qtt_produto.readonly = .f.
				
			** Integra��o
			utentes.pageframe1.page5.btnImg1.enable(.t.)			
			utentes.pageframe1.page5.btnImg2.enable(.t.)
			utentes.pageframe1.page5.btnImg3.enable(.t.)
			utentes.pageframe1.page5.btnImg4.enable(.t.)
			utentes.pageframe1.page5.btnImg5.enable(.t.)
			utentes.pageframe1.page5.btnImg6.enable(.t.)
			utentes.pageframe1.page5.btnImg7.enable(.t.)
			utentes.pageframe1.page5.btnImg8.enable(.t.)
			
			utentes.pageframe1.page2.list2.setfocus
			
			&& PageFrame da Sede
			utentes.pageframe1.page3.btnSede.enable(.f.)
			utentes.pageframe1.page3.btnNovo.enable(.f.)
			
			&& Configura patologias
			uf_utentes_PreenchePatologias()
			
			&& configura Diplomas
			&&uf_utentes_PreencheDiplomas()
			
			&& Configura campos obrigat�rios
			utentes.txtNome.backcolor 	= rgb[191,223,223]
			
			&& Par�metros
			IF uf_gerais_getParameter('ADM0000000022', 'BOOL') &&telefone
				utentes.pageframe1.page1.txtTelefone.backcolor 	= rgb[191,223,223]
			ENDIF
			
			IF uf_gerais_getParameter('ADM0000000023', 'BOOL') &&data nascimento
				utentes.pageframe1.page1.txtnascimento.backcolor 	= rgb[191,223,223]
			ENDIF
			
			IF uf_gerais_getParameter('ADM0000000046', 'BOOL') &&NIF
				utentes.txtNCONT.backcolor 	= rgb[191,223,223]
			ENDIF
			
			IF uf_gerais_getParameter('ADM0000000112', 'BOOL') &&morada
				utentes.pageframe1.page1.txtMorada.backcolor 	= rgb[191,223,223]
			ENDIF
			
			utentes.pageframe1.page7.container1.btnPlano.enable(.t.)

		ENDIF
	ELSE 
		utentes.pageframe1.enabled = .f.
		utentes.pageframe1.visible = .f.
		utentes.ctnTabs1.visible = .f.
		utentes.menu1.estado("opcoes ,editar", "HIDE", "Gravar", .f., "Sair", .t.)
		utentes.txtNome.value = '*********'
		utentes.txtNCONT.value = '*********'
		utentes.txtu_nbenef.value = '*********'
		utentes.txtNome2.value = '*********'
		utentes.txtu_nrcartao.value = '*********'
		utentes.txtID.value = '*********'
		utentes.txtTratamento.value = '*********'
		
	ENDIF 
	
	IF uf_gerais_getParameter_site('ADM0000000067', 'BOOL', mySite) = .t. AND ALLTRIM(ch_grupo) <> 'Administrador' AND ALLTRIM(ch_grupo) <> 'Supervisores'
		utentes.menu1.estado("opcoes, aplicacoes", "HIDE")
	ENDIF 
	
ENDFUNC


** PREENCHE PATOLOGIAS
FUNCTION uf_utentes_PreenchePatologias
	
	IF USED("uCrsPatologiasCL") AND USED("uCrsPatologias") && S� entra se os cursores existirem
		utentes.pageframe1.page2.list1.rowsource=''

		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			exec up_clientes_patologiasLista 
		ENDTEXT 
		IF !uf_gerais_actgrelha("", "uCrsPatologias", lcSQL)
			RETURN .f.
		ENDIF

		SELECT uCrsPatologiasCL
		GO TOP
		SCAN
			&& Apaga Valores do cursor
			SELECT uCrsPatologias
			DELETE FROM uCrsPatologias WHERE uCrsPatologiasCL.patol_ID == uCrsPatologias.patol_ID
			
			SELECT uCrsPatologiasCL
		ENDSCAN

		utentes.pageframe1.page2.list1.rowsource='uCrsPatologias'
			
		&& Actualiza cursor com base no filtro
		utentes.pageframe1.page2.txtFiltro.interactivechange
	ELSE
		uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR AS PATOLOGIAS! POR FAVOR CONTACTE O SUPORTE.","OK","",16)	
		RETURN .f.
	ENDIF
ENDFUNC


** Novo Cliente
FUNCTION uf_utentes_novo

	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()
	
	&& Verificar Se o Sistema Permite Criar utentes
	IF uf_gerais_getParameter('ADM0000000120', 'BOOL')
		uf_perguntalt_chama("A CRIA��O/ALTERA��O/ELIMINA��O DE UTENTES EST� BLOQUEADA NESTE SISTEMA.","OK","",64)
		RETURN .f.
	ENDIF
	
	&& Valida permiss�o do utilizador
	IF !(uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Gest�o de Clientes - Introduzir'))
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE CRIAR UTENTES.","OK","", 48)
		RETURN .f.
	ENDIF

	uf_utentes_chama(0)
	SELECT CL
	APPEND BLANK

	&& Valores por defeito
	uf_utentes_ValoresDefeito("CL", "utentes")
	
	myClIntroducao = .t.
	uf_utentes_confEcra()
	uf_utentes_alternaMenu()

	utentes.ctnTabs1.lbl1.click
	utentes.ctnTabs2.visible = .f.
	utentes.ctnTabs3.visible = .f.
	utentes.ctnTabs1.visible = .t.
	
	dadospessalt=.t.
	
	IF uf_gerais_getParameter_site('ADM0000000088', 'BOOL', mySite)
	
		regua(0,2,"A criar cliente no servidor...")
		LOCAL lcNrUtRemoto, lcStamp 
		lcStamp = uf_gerais_stamp()
		lcNrUtRemoto = uf_utentes_criaremoto(ALLTRIM(lcStamp))
		IF lcNrUtRemoto = 0
			uf_perguntalt_chama("N�O � POSS�VEL CONTATAR COM O DATA CENTER PARA A CRIA��O DO CLIENTE.","OK","", 48)
			regua(2)
			RETURN .f.
		ELSE
			regua(1,1,"A criar cliente no servidor...")
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				
				insert into b_utentes (utstamp, no, estab, moeda, descp, codigop,descpnat, codigopnat)
				select '<<ALLTRIM(lcStamp)>>', <<lcNrUtRemoto>>,0, 'AOA', 'Angola', 'AOA', 'Angola','AOA'
				GO 
				
			ENDTEXT 
		
			IF !uf_gerais_actgrelha("", "", lcSQL)
				uf_perguntalt_chama("N�O FOI POSS�VEL CRIAR O CLIENTE NA BD LOCAL. POR FAVOR CONTACTE O SUPORTE.","OK","", 64)
				regua(2)
				RETURN 0
			ENDIF
			
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				
				delete from Rep_Control where Identifier='<<ALLTRIM(lcStamp)>>'
				
			ENDTEXT 
		
			IF !uf_gerais_actgrelha("", "", lcSQL)
				uf_perguntalt_chama("N�O FOI POSS�VEL CRIAR O CLIENTE NA BD LOCAL. POR FAVOR CONTACTE O SUPORTE.","OK","", 64)
				regua(2)
				RETURN 0
			ENDIF
			
			SELECT cl 
			replace cl.no WITH lcNrUtRemoto 
			replace cl.utstamp WITH ALLTRIM(lcStamp)
			regua(2)
			myClIntroducao = .f.
			myClAlteracao = .t.
		ENDIF 
	ENDIF 
					
	&& Actualiza ecra
	SELECT CL
	utentes.refresh
ENDFUNC

FUNCTION uf_utentes_criaremoto
	LPARAMETERS StampUt 
	
	#DEFINE  FLAG_ICC_FORCE_CONNECTION 1
 	DECLARE Long InternetCheckConnection IN Wininet.dll String Url, Long dwFlags, Long Reserved
	 
	* Fast and reliable web site
	LOCAL lcValidaLigInternet, lcUrl, lcRemoteSQL, lcNoRemoto, lcRemoteDB, lcRemoteSRV
	STORE .t. TO lcvalidaLigInternet
	STORE '' TO lcUrl , lcRemoteSQL, lcRemoteDB, lcRemoteSRV
	STORE 0 TO lcNoRemoto

	
	lcUrl = uf_gerais_getParameter_site('ADM0000000089', 'TEXT', mySite)
	lcRemoteSQL = uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite)
	lcRemoteDB = substr(uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite),at('].[',uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite))+2 ,99)
	lcRemoteSRV = substr(uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite), 1,at('].[',uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite)))
	IF InternetCheckConnection(lcUrl, FLAG_ICC_FORCE_CONNECTION, 0) <> 0
		*? "Connection is available"
		lcValidaLigInternet = .t.
	ELSE
		lcvalidaLigInternet = .f.
	ENDIF
	
	IF lcValidaLigInternet == .f.
		** n�o tem liga��o com o servidor
		RETURN 0
	ELSE
		** tem liga��o com o servidor
		lcSQL =''
		lcSQL1 =''
		lcSQL2 =''
		
		TEXT TO lcSQL1 NOSHOW TEXTMERGE 
		
			EXEC ('ALTER TABLE <<ALLTRIM(lcRemoteDB)>>.[dbo].[b_utentes] DISABLE TRIGGER Rep_B_Utentes_Control') AT <<ALLTRIM(lcRemoteSRV)>>
			

		ENDTEXT 
		TEXT TO lcSQL2 NOSHOW TEXTMERGE 
		
			EXEC ('ALTER TABLE <<ALLTRIM(lcRemoteDB)>>.[dbo].[b_utentes] ENABLE TRIGGER Rep_B_Utentes_Control') AT <<ALLTRIM(lcRemoteSRV)>>
			
			exec <<ALLTRIM(lcRemoteSQL)>>.[dbo].sp_insert_b_us_loja '<<mysite>>', '<<ALLTRIM(StampUt)>>'
			
		ENDTEXT
		
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			
			exec <<ALLTRIM(lcRemoteSQL)>>.[dbo].sp_insert_utente_loja_dc  '<<ALLTRIM(StampUt)>>'

		ENDTEXT 
		
		IF uf_gerais_getParameter_site('ADM0000000091', 'BOOL', mySite)
			lcSQL = lcSQL1 + CHR(13) + lcSQL + CHR(13) + lcSQL2 
		ENDIF 

		IF !uf_gerais_actgrelha("", "", lcSQL)
			uf_perguntalt_chama("N�O FOI POSS�VEL CRIAR O CLIENTE NA BD REMOTA. POR FAVOR CONTACTE O SUPORTE.","OK","", 64)
			RETURN 0
		ENDIF
		
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			SELECT no FROM <<ALLTRIM(lcRemoteSQL)>>.dbo.b_utentes WHERE utstamp='<<ALLTRIM(StampUt)>>'
		ENDTEXT 
		IF !uf_gerais_actgrelha("", "ucrsnoremoto", lcSQL)
			uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR O NR. DO NOVO CLIENTE NA BD REMOTA. POR FAVOR CONTACTE O SUPORTE.","OK","", 64)
			RETURN 0
		ENDIF
		lcNoRemoto = ucrsnoremoto.no
		fecha("ucrsnoremoto")
		RETURN lcNoRemoto 
	ENDIF
ENDFUNC 

FUNCTION uf_utentes_insremoto_at
	regua(0,2,"A criar cliente no servidor...")
	LOCAL StampUt 
	StampUt = uf_gerais_stamp()
	
	#DEFINE  FLAG_ICC_FORCE_CONNECTION 1
 	DECLARE Long InternetCheckConnection IN Wininet.dll String Url, Long dwFlags, Long Reserved
	 
	LOCAL lcValidaLigInternet, lcUrl, lcRemoteSQL, lcNoRemoto, lcRemoteDB, lcRemoteSRV
	STORE .t. TO lcvalidaLigInternet
	STORE '' TO lcUrl , lcRemoteSQL, lcRemoteDB, lcRemoteSRV
	STORE 0 TO lcNoRemoto

	
	lcUrl = uf_gerais_getParameter_site('ADM0000000089', 'TEXT', mySite)
	lcRemoteSQL = uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite)
	lcRemoteDB = substr(uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite),at('].[',uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite))+2 ,99)
	lcRemoteSRV = substr(uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite), 1,at('].[',uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite)))
	IF InternetCheckConnection(lcUrl, FLAG_ICC_FORCE_CONNECTION, 0) <> 0
		*? "Connection is available"
		lcValidaLigInternet = .t.
	ELSE
		lcvalidaLigInternet = .f.
	ENDIF
	
	IF lcValidaLigInternet == .f.
		** n�o tem liga��o com o servidor
		regua(2)
		RETURN 0
	ELSE
		** tem liga��o com o servidor
		lcSQL =''
		lcSQL1 =''
		lcSQL2 =''
		
		TEXT TO lcSQL1 NOSHOW TEXTMERGE 
		
			EXEC ('ALTER TABLE <<ALLTRIM(lcRemoteDB)>>.[dbo].[b_utentes] DISABLE TRIGGER Rep_B_Utentes_Control') AT <<ALLTRIM(lcRemoteSRV)>>
			

		ENDTEXT 
		TEXT TO lcSQL2 NOSHOW TEXTMERGE 
		
			EXEC ('ALTER TABLE <<ALLTRIM(lcRemoteDB)>>.[dbo].[b_utentes] ENABLE TRIGGER Rep_B_Utentes_Control') AT <<ALLTRIM(lcRemoteSRV)>>

		ENDTEXT
		
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			
			exec <<ALLTRIM(lcRemoteSQL)>>.[dbo].sp_insert_utente_loja_dc_atendimento 
				'<<ALLTRIM(StampUt)>>'
				,'<<ALLTRIM(PESQUTENTES.Pageframe1.page2.nome.value)>>'
				,'<<ALLTRIM(PESQUTENTES.Pageframe1.page2.ncont.value)>>'
				,'<<ALLTRIM(PESQUTENTES.Pageframe1.page2.morada.value)>>'
				,'<<ALLTRIM(PESQUTENTES.Pageframe1.page2.tlf.value)>>'
				,'<<ALLTRIM(PESQUTENTES.Pageframe1.page2.utenteno.value)>>'
				,'<<ALLTRIM(PESQUTENTES.Pageframe1.page2.email.value)>>'
				,'<<ALLTRIM(PESQUTENTES.Pageframe1.page2.nid.value)>>'
				,'<<ALLTRIM(PESQUTENTES.Pageframe1.page2.sexo.value)>>'
				,'<<ALLTRIM(strtran(PESQUTENTES.Pageframe1.page2.txtnascimento.value,".",""))>>'
			 
		ENDTEXT 
		
		IF uf_gerais_getParameter_site('ADM0000000091', 'BOOL', mySite)
			lcSQL = lcSQL1 + CHR(13) + lcSQL + CHR(13) + lcSQL2 
		ENDIF 

		IF !uf_gerais_actgrelha("", "ucrsnoremoto", lcSQL)
			uf_perguntalt_chama("N�O FOI POSS�VEL INSERIR CLIENTE NA BD REMOTA. POR FAVOR CONTACTE O SUPORTE.","OK","", 64)
			regua(2)
			RETURN .f.
		ENDIF
		regua(1,2,"A criar utente no servidor...")
*!*			TEXT TO lcSQL NOSHOW TEXTMERGE 
*!*				SELECT no FROM <<ALLTRIM(lcRemoteSQL)>>.dbo.b_utentes WHERE utstamp='<<ALLTRIM(StampUt)>>'
*!*			ENDTEXT 
*!*			IF !uf_gerais_actgrelha("", "ucrsnoremoto", lcSQL)
*!*				uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR O NR. DO NOVO CLIENTE NA BD REMOTA. POR FAVOR CONTACTE O SUPORTE.","OK","", 64)
*!*				regua(2)
*!*				RETURN 0
*!*			ENDIF
		lcNoRemoto = ucrsnoremoto.no
		fecha("ucrsnoremoto")
		
		IF lcNoRemoto = 0
			uf_perguntalt_chama("N�O FOI POSS�VEL INSERIR CLIENTE NA BD REMOTA. POR FAVOR TENTE CRIAR NOVAMENTE.","OK","", 64)
			lcNoRemoto = 200
		ELSE
			** inserir localmente
			lcSQL =''
			
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				
				INSERT INTO b_utentes (utstamp, no, estab, nome ,ncont ,morada ,tlmvl ,nbenef ,email , bino, moeda, descp, codigop,descpnat, codigopnat, sexo ,nascimento)
				SELECT '<<ALLTRIM(StampUt)>>'
					, <<lcNoRemoto>>
					,0
					,'<<ALLTRIM(PESQUTENTES.Pageframe1.page2.nome.value)>>'
					,'<<ALLTRIM(PESQUTENTES.Pageframe1.page2.ncont.value)>>'
					,'<<ALLTRIM(PESQUTENTES.Pageframe1.page2.morada.value)>>'
					,'<<ALLTRIM(PESQUTENTES.Pageframe1.page2.tlf.value)>>'
					,'<<ALLTRIM(PESQUTENTES.Pageframe1.page2.utenteno.value)>>'
					,'<<ALLTRIM(PESQUTENTES.Pageframe1.page2.email.value)>>'
					,'<<ALLTRIM(PESQUTENTES.Pageframe1.page2.nid.value)>>'
					, 'AOA', 'Angola', 'AOA', 'Angola','AOA'
					,'<<ALLTRIM(PESQUTENTES.Pageframe1.page2.sexo.value)>>'
					,'<<ALLTRIM(strtran(PESQUTENTES.Pageframe1.page2.txtnascimento.value,".",""))>>'

			ENDTEXT 

			IF !uf_gerais_actgrelha("", "", lcSQL)
				uf_perguntalt_chama("N�O FOI POSS�VEL INSERIR CLIENTE NA BD LOCAL. POR FAVOR CONTACTE O SUPORTE.","OK","", 64)
				regua(2)
				RETURN .f.
			ENDIF
			
			lcSQL =''
			
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				
				delete from Rep_Control where Identifier= '<<ALLTRIM(StampUt)>>'

			ENDTEXT 

			IF !uf_gerais_actgrelha("", "", lcSQL)
				uf_perguntalt_chama("N�O FOI POSS�VEL INSERIR CLIENTE NA BD LOCAL. POR FAVOR CONTACTE O SUPORTE.","OK","", 64)
				regua(2)
				RETURN .f.
			ENDIF
		ENDIF 
		regua(2)
		RETURN lcNoRemoto
	ENDIF
ENDFUNC 


** INSERIR ESTABLECIMENTO
FUNCTION uf_utentes_novoEstab
	&& Verificar Se o Sistema Permite Criar Clientes
	IF uf_gerais_getParameter('ADM0000000120', 'BOOL')
		uf_perguntalt_chama("A CRIA��O/ALTERA��O/ELIMINA��O DE UTENTES EST� BLOQUEADA NESTE SISTEMA.","OK","", 64)
		RETURN .f.
	ENDIF
		
	&& Valida Clientes abaixo do 200
	IF UPPER(ALLTRIM(LEFT(uf_gerais_getParameter("ADM0000000157","TEXT"),1))) == "F"
		SELECT cl
		IF cl.no < 200
			uf_perguntalt_chama("N�O PODE CRIAR DEPENDENTES PARA UTENTES ABAIXO DO N� 200!", "OK","",48)	
			RETURN .f.
		ENDIF
		IF cl.no == 200
			uf_perguntalt_chama("N�O PODE CRIAR DEPENDENTES PARA O UTENTE 200!", "OK","",48)	
			RETURN .f.
		ENDIF 	
	ENDIF
		
	&& Valida permiss�o do utilizador
	IF !(uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Gest�o de Clientes - Introduzir'))
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE CRIAR NOVOS UTENTES.", "OK","",48)
		RETURN .f.
	ENDIF	
	
	IF EMPTY(astr(utentes.txtNo.value))
		RETURN .f.
	ENDIF

	myClIntroducao = .t.
	dadospessalt=.t.
				
	&& No de estab
	text to lcSQL textmerge noshow
		Select 
			case when Max(estab) is null then 1 else max(estab) + 1 end as estab
		From b_utentes (nolock)
		where 
			no=<<utentes.txtNo.value>>
	ENDTEXT
	uf_gerais_actgrelha("", "uCrsTempEstab", lcSql)
	
	LOCAL lcStamp
	lcStamp = uf_gerais_stamp() 
	
	SELECT CL
	replace estab		WITH uCrsTempEstab.estab
	replace utstamp		WITH lcStamp
	replace nrcartao	WITH ''
	replace esaldo		WITH 0
	replace ncont		WITH ''
	replace nbenef		WITH ''
	replace nbenef2		WITH ''
	replace id			WITH ''
	replace nome		WITH ''
	replace	nascimento 	WITH CTOD('1900.01.01')
	
	uf_utentes_confEcra()
	uf_utentes_alternaMenu()
	
	utentes.refresh
	
	fecha("uCrsTempEstab")
	
	utentes.ctnTabs1.lbl1.click
	utentes.ctnTabs2.visible=.f.
	utentes.ctnTabs3.visible=.f.
	utentes.ctnTabs1.visible=.t.
ENDFUNC


** Alterar Cliente
FUNCTION uf_utentes_editar

	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()
	
	&& Verificar Se o Sistema Permite Criar Clientes
	IF uf_gerais_getparameter('ADM0000000120', 'BOOL')
		uf_perguntalt_chama("A CRIA��O/ALTERA��O/ELIMINA��O DE UTENTES EST� BLOQUEADA NESTE SISTEMA.","OK","", 64)
		RETURN .f.
	ENDIF
	
	&& Valida o n� de cliente
	SELECT cl
	IF cl.no = 200
		uf_perguntalt_chama("DESCULPE, MAS N�O PODE EDITAR O UTENTE 200!","OK","", 48)
		RETURN .f.
	ENDIF

	&& valida se s� altera dados do cliente sendo administrador ou com password do mesmo
	IF uf_gerais_getParameter_site('ADM0000000067', 'BOOL', mySite) = .t. AND ALLTRIM(ch_grupo) <> 'Administrador' AND ALLTRIM(ch_grupo) <> 'Supervisores'
		IF !uf_gerais_valida_password_admin('UTENTES', 'ALTERA��O DE FICHA')
			RETURN .f.
		ENDIF 
	ENDIF 
	
	&& Valida permiss�o do utilizador
	IF (uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Clientes - Alterar'))
		&& Valida Cursor
		SELECT CL
		IF EMPTY(CL.utstamp)
			RETURN .f.
		ENDIF
		
		&& Configura ecra
		myClAlteracao = .t.
		uf_utentes_confEcra()
		uf_utentes_alternaMenu()
	ELSE
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ALTERAR CLIENTES.","OK","",48)
	ENDIF	
	
	utentes.refresh
	utentes.txtNome.setfocus()
ENDFUNC



** INSERIR CLIENTE
FUNCTION uf_utentes_insere
	LPARAMETERS lcCursor

	LOCAL lcSQL
	STORE '' TO lcSQL

	SELECT &lcCursor
	GO TOP 
	SELECT * FROM &lcCursor INTO CURSOR uCrsTempInCl READWRITE
	SELECT uCrsTempInCl
	GO TOP
	
	** Valida��es
	IF uCrsTempInCl.Estab==0
		&& Verifica N� do Cliente
		
			uf_gerais_actgrelha("", "uCrsTempNo", [Select Isnull(Max(NO),0)+1 as no From b_utentes (nolock)])
			SELECT uCrsTempNo
			IF uCrsTempNo.no > uCrsTempInCl.no
				replace uCrsTempInCl.NO	WITH uCrsTempNo.no
				
				SELECT &lcCursor
				replace NO WITH uCrsTempNo.no
			ENDIF
			fecha("uCrsTempNo")
		
	ELSE
		&& Verifica N� dependente				
		text to lcSQL textmerge noshow
			Select 
				case when Max(estab) is null then 0 else max(estab) + 1 end as estab 
			From b_utentes (nolock)
			where 
				no=<<uCrsTempInCl.no>>
		ENDTEXT
		uf_gerais_actgrelha("", "uCrsTempEstab", lcSql)

		SELECT uCrsTempInCl
		replace uCrsTempInCl.Estab with	uCrsTempEstab.estab
		
		SELECT &lcCursor
		replace Estab WITH uCrsTempEstab.estab
		fecha("uCrsTempEstab")
	ENDIF
	
	&& Cart�o de Cliente
	LOCAL lcNrCartaoCl
	IF !uf_gerais_getParameter("ADM0000000021","BOOL") AND !EMPTY(ALLTRIM(uCrsTempInCl.nrcartao))
		llManual = .t. &&bot�o act. cart�o na cria��o de fichas no atendimento
	ELSE
		llManual = .f.
	ENDIF
	lcNrCartaoCl = uf_cartaocliente_geraNumero(llManual)

	
	If (uf_gerais_getParameter("ADM0000000021","BOOL") OR llManual) AND (&lcCursor..estab == 0)
		SELECT uCrsTempInCl
		replace uCrsTempInCl.nrcartao WITH lcNrCartaoCl
		
		SELECT &lcCursor
		replace nrcartao WITH lcNrCartaoCl
	ENDIF

	**Contas SNC
	uf_utentes_contaSNC(.f., "UCRSTEMPINCL")

	lcSQL = ''	
	TEXT TO lcSql NOSHOW textmerge
		exec up_prescricao_efrutente '<<ALLTRIM(uCrsTempInCl.utstamp)>>'
	ENDTEXT

	IF !uf_gerais_actGrelha("","uCrsEFRsAct",lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar EFRs associadas ao utentes.","OK","",16)
		RETURN .f.
	ENDIF
	
	*EFR
	IF UPPER(ALLTRIM(uCrsE1.tipoempresa)) != "FARMACIA"
		SELECT uCrsEFRsAct
		IF RECCOUNT("uCrsEFRsAct") > 0 
			
			SELECT uCrsEFRsAct
			GO TOP 
			SELECT uCrsTempInCl
			replace ;
				uCrsTempInCl.entPla		WITH uCrsEFRsAct.abrev ;
				uCrsTempInCl.codPla		WITH uCrsEFRsAct.Codigo ;
				uCrsTempInCl.desPla		WITH uCrsEFRsAct.Descricao ;
				uCrsTempInCl.nbenef2	WITH uCrsEFRsAct.numeroBenefEntidade;
				uCrsTempInCl.valipla2	WITH uCrsEFRsAct.DataValidade
		ELSE
			IF EMPTY(ALLTRIM(uCrsTempInCl.nbenef))
				SELECT uCrsTempInCl
				replace ;
					uCrsTempInCl.codPla		WITH "999998" ;
					uCrsTempInCl.desPla		WITH "Sem Comparticipa��o pelo SNS" ;
					uCrsTempInCl.nbenef2	WITH "";
					uCrsTempInCl.valipla2	WITH ctod("1900.01.01")
			ELSE
				SELECT uCrsTempInCl
				replace ;
					uCrsTempInCl.codPla		WITH "935601" ;
					uCrsTempInCl.desPla		WITH "Servi�o Nacional de Sa�de" ;
					uCrsTempInCl.nbenef2	WITH "";
					uCrsTempInCl.valipla2	WITH ctod("1900.01.01")
			ENDIF
		ENDIF
	ENDIF
	
	
	TEXT TO lcSQl TEXTMERGE NOSHOW
		INSERT INTO b_utentes 
		(
			utstamp, nome, no, estab, ncont,
			nome2, nrcartao,inactivo,naoencomenda,nocredit,cobnao,
			morada, codpost,local,zona, fax, telefone, tlmvl, tipo, email, url, profi,hablit,bino,nascimento, sexo, codigoP, descP,
			nbenef, nbenef2, codpla, desPLA, entPla, VALIPLA, VALIPLA2,
			pai, mae, notif, pacgen, tratamento, obs, ninscs, csaude, drfamilia,
			Vencimento,DESCONTO, tipodesc, alimite, modofact,odatraso,tabiva, particular,autofact,clivd,clinica,
			eplafond,
			descpp,radicaltipoemp,TpDesc,Classe,Ccusto,fref,Moeda,
			conta,contaacer,contafac,contaainc,contatit,contaletdes,contalet,contaletsac,
			ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora
			,entfact, peso, altura
			,recmmotivo, recmdescricao, recmdatainicio, recmdatafim, recmtipo, itmotivo, itdescricao, itdatainicio, itdatafim
			,cesd, cesdcart, cesdcp, cesdidi, cesdidp, cesdpd, cesdval
			,atestado, atnumero, attipo, atcp, atpd, atval
			,codigoPNat, descPNat, pais, entCompart
			,noConvencao
			,designConvencao
			,id, nprescsns
			,direcaoTec, proprietario, nib, moradaAlt, noAssociado, alvara, software, ars, no_ext, codSwift, nomcom, autorizado, autoriza_sms, autoriza_emails
			,site_compart
		)
		Values
		(
			'<<ALLTRIM(uCrsTempInCl.utstamp)>>',
			'<<ALLTRIM(uCrsTempInCl.Nome)>>',
			<<uCrsTempInCl.No>>,
			<<uCrsTempInCl.estab>>,
			'<<ALLTRIM(uCrsTempInCl.Ncont)>>',
			
			'<<uCrsTempInCl.Nome2>>',
			'<<ALLTRIM(uCrsTempInCl.nrcartao)>>',
			<<IIF(uCrsTempInCl.inactivo,1,0)>>,
			<<IIF(uCrsTempInCl.naoencomenda,1,0)>>,
			<<IIF(uCrsTempInCl.nocredit,1,0)>>,
			<<IIF(uCrsTempInCl.cobnao,1,0)>>,
			
			'<<ALLTRIM(uCrsTempInCl.Morada)>>',
			'<<ALLTRIM(uCrsTempInCl.CodPost)>>',
			'<<ALLTRIM(uCrsTempInCl.Local)>>',
			'<<uCrsTempInCl.Zona>>',
			'<<ALLTRIM(uCrsTempInCl.Fax)>>',
			'<<ALLTRIM(uCrsTempInCl.Telefone)>>',
			'<<ALLTRIM(uCrsTempInCl.TLMVL)>>',
			'<<uCrsTempInCl.Tipo>>',
			'<<ALLTRIM(uCrsTempInCl.Email)>>',
			'<<ALLTRIM(uCrsTempInCl.URL)>>',
			'<<ALLTRIM(uCrsTempInCl.profi)>>',
			'<<ALLTRIM(uCrsTempInCl.hablit)>>',
			'<<ALLTRIM(uCrsTempInCl.bino)>>',
			'<<DTOS(uCrsTempInCl.nascimento)>>',
			'<<uCrsTempInCl.sexo>>',
			'<<uCrsTempInCl.codigoP>>',
			'<<uCrsTempInCl.descP>>',
			
			'<<ALLTRIM(uCrsTempInCl.nbenef)>>',
			'<<ALLTRIM(uCrsTempInCl.nbenef2)>>',
			'<<ALLTRIM(uCrsTempInCl.codpla)>>',
			'<<ALLTRIM(uCrsTempInCl.desPLA)>>',
			'<<ALLTRIM(uCrsTempInCl.entPLA)>>',
			'<<DTOS(uCrsTempInCl.VALIPLA)>>',	
			'<<DTOS(uCrsTempInCl.VALIPLA2)>>',
			
			'<<ALLTRIM(uCrsTempInCl.pai)>>',
			'<<ALLTRIM(uCrsTempInCl.mae)>>',
			<<IIF(uCrsTempInCl.notif,1,0)>>,
			<<IIF(uCrsTempInCl.pacgen,1,0)>>,
			'<<ALLTRIM(uCrsTempInCl.tratamento)>>',
			'<<ALLTRIM(uCrsTempInCl.OBS)>>',
			'<<ALLTRIM(uCrsTempInCl.ninscs)>>',
			'<<ALLTRIM(uCrsTempInCl.csaude)>>',
			'<<ALLTRIM(uCrsTempInCl.drfamilia)>>',
					
			<<uCrsTempInCl.Vencimento>>,
			<<uCrsTempInCl.Desconto>>,
			'<<uCrsTempInCl.tipodesc>>',
			<<uCrsTempInCl.alimite>>,
			'<<uCrsTempInCl.modofact>>',
			<<uCrsTempInCl.odatraso>>,
			<<uCrsTempInCl.tabiva>>,				
			<<IIF(uCrsTempInCl.particular,1,0)>>,
			<<IIF(uCrsTempInCl.autofact,1,0)>>,
			<<IIF(uCrsTempInCl.clivd,1,0)>>,
			<<IIF(uCrsTempInCl.clinica,1,0)>>,
			
			<<uCrsTempInCl.eplafond>>,
			
			<<uCrsTempInCl.descpp>>,
			<<uCrsTempInCl.radicaltipoemp)>>,
			'<<uCrsTempInCl.TpDesc>>',
			'<<uCrsTempInCl.Classe>>',
			'<<uCrsTempInCl.Ccusto>>',
			'<<uCrsTempInCl.fref>>',
			'<<uCrsTempInCl.Moeda>>',
							
			'<<ALLTRIM(uCrsTempInCl.Conta)>>',
			'<<ALLTRIM(uCrsTempInCl.contaacer)>>',
			'<<ALLTRIM(uCrsTempInCl.contafac)>>',
			'<<ALLTRIM(uCrsTempInCl.contaainc)>>',
			'<<ALLTRIM(uCrsTempInCl.contatit)>>',
			'<<ALLTRIM(uCrsTempInCl.contaletdes)>>',
			'<<ALLTRIM(uCrsTempInCl.contalet)>>',
			'<<ALLTRIM(uCrsTempInCl.contaletsac)>>',
						
			'<<m_chinis>>',
			convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
			convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108),
			'<<m_chinis>>',
			convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
			convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
			
			,<<IIF(uCrsTempInCl.entfact,1,0)>>
			,<<uCrsTempInCl.peso>>
			,<<uCrsTempInCl.altura>>
			
			,'<<ALLTRIM(uCrsTempInCl.recmmotivo)>>'
			,'<<ALLTRIM(uCrsTempInCl.recmdescricao)>>'
			,'<<DTOS(uCrsTempInCl.recmdatainicio)>>'
			,'<<DTOS(uCrsTempInCl.recmdatafim)>>'
			,'<<ALLTRIM(uCrsTempInCl.recmtipo)>>'
			,'<<ALLTRIM(uCrsTempInCl.itmotivo)>>'
			,'<<ALLTRIM(uCrsTempInCl.itdescricao)>>'
			,'<<DTOS(uCrsTempInCl.itdatainicio)>>'	
			,'<<DTOS(uCrsTempInCl.itdatafim)>>'
			
			,<<IIF(uCrsTempInCl.cesd,1,0)>>
			,'<<ALLTRIM(uCrsTempInCl.cesdcart)>>'
			,'<<ALLTRIM(uCrsTempInCl.cesdcp)>>'
			,'<<ALLTRIM(uCrsTempInCl.cesdidi)>>'
			,'<<ALLTRIM(uCrsTempInCl.cesdidp)>>'
			,'<<ALLTRIM(uCrsTempInCl.cesdpd)>>'
			,'<<DTOS(uCrsTempInCl.cesdval)>>'
			
			,<<IIF(uCrsTempInCl.atestado,1,0)>>
			,'<<ALLTRIM(uCrsTempInCl.atnumero)>>'
			,'<<ALLTRIM(uCrsTempInCl.attipo)>>'
			,'<<ALLTRIM(uCrsTempInCl.atcp)>>'
			,'<<ALLTRIM(uCrsTempInCl.atpd)>>'
			,'<<DTOS(uCrsTempInCl.atval)>>'
			
			,'<<uCrsTempInCl.codigoPNat>>'
			,'<<uCrsTempInCl.descPNat>>'
			,<<uCrsTempInCl.pais>>
			,<<IIF(uCrsTempInCl.entCompart,1,0)>>
			,'<<ALLTRIM(uCrsTempInCl.noConvencao)>>'
			,'<<ALLTRIM(uCrsTempInCl.designConvencao)>>'
			,'<<ALLTRIM(uCrsTempInCl.id)>>'
			,<<IIF(uCrsTempInCl.nprescsns,1,0)>>
			,'<<ALLTRIM(uCrsTempInCl.direcaoTec)>>'
			,'<<ALLTRIM(uCrsTempInCl.proprietario)>>'
			,'<<astr(uCrsTempInCl.nib)>>'
			,'<<ALLTRIM(uCrsTempInCl.moradaAlt)>>'
			,<<uCrsTempInCl.noAssociado>>
			,<<uCrsTempInCl.alvara>>
			,'<<ALLTRIM(uCrsTempInCl.software)>>'
			,'<<ALLTRIM(uCrsTempInCl.ars)>>'
			,'<<ALLTRIM(uCrsTempInCl.no_ext)>>'
			,'<<ALLTRIM(uCrsTempInCl.codSwift)>>'
			,'<<ALLTRIM(uCrsTempInCl.nomcom)>>'
			,<<IIF(uCrsTempInCl.autorizado,1,0)>>
			,<<IIF(uCrsTempInCl.autoriza_sms,1,0)>>
			,<<IIF(uCrsTempInCl.autoriza_emails,1,0)>>
			,'<<ALLTRIM(uCrsTempInCl.site_compart)>>'
		)
		
	ENDTEXT
	
	IF UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'
		STORE '' TO lcSQl1
		TEXT TO lcSQL1 TEXTMERGE noshow		
			IF NOT exists (select * from dispensa_eletronica_cc where utstamp = '<<ALLTRIM(uCrsTempInCl.utstamp)>>')
			BEGIN
				insert into dispensa_eletronica_cc (id, id_emp, nome, morada, nif, nrSNS, sexo, nascimento, validade_fim, publicKey, site, machine, doc_id, tipoDoc, utstamp) 
				values (LEFT(NEWID(),20), '<<ALLTRIM(uCrsE1.id_lt)>>', '<<ALLTRIM(uCrsTempInCl.Nome)>>', '<<ALLTRIM(uCrsTempInCl.Morada)>>'
						, '<<ALLTRIM(uCrsTempInCl.Ncont)>>', '<<ALLTRIM(uCrsTempInCl.nbenef)>>', '<<uCrsTempInCl.sexo>>', '<<uf_gerais_getdate(uCrsTempInCl.nascimento,"SQL")>>', '<<uf_gerais_getDate(uCrsTempInCl.binoValidade,"SQL")>>'
						, '','<<ALLTRIM(mySite)>>', '','<<ALLTRIM(uCrsTempInCl.bino)>>', 'CC', '<<ALLTRIM(uCrsTempInCl.utstamp)>>')
			END
		ENDTEXT
		lcSQl = lcSQl + CHR(13) + lcSQl1
	ENDIF 

	**_cliptext=lcSQL

	IF USED("uCrsTempInCl")
		fecha("uCrsTempInCl")
	ENDIF
			
	IF !uf_gerais_actgrelha("", "", lcSQL)
		RETURN	.f.
	ELSE
		IF uf_gerais_getParameter_site('ADM0000000088', 'BOOL', mySite)
			uf_utentes_updateremoto(ALLTRIM(uCrsTempInCl.utstamp))
		ENDIF 
		RETURN	.t.
	ENDIF
ENDFUNC


** INSER��O DE PATOLOGIAS
FUNCTION uf_utentes_inserePatologias
	LPARAMETERS lcCursor, lcStamp
	
	SELECT &lcCursor
	GO TOP
	SELECT * FROM &lcCursor INTO CURSOR uCrsTmpPatoIns READWRITE
	
	SELECT uCrsTmpPatoIns 
	GO TOP
	SCAN
		lcSQL=''
		TEXT TO lcSQl TEXTMERGE NOSHOW
			INSERT INTO B_patologias
				(
					patostamp,ostamp,tabela,patol_ID,ousrinis,ousrdata,ousrhora
				)
			Values
				(
					'<<ALLTRIM(uCrsTmpPatoIns.patostamp)>>',
					'<<lcStamp>>',
					'b_utentes',
					'<<uCrsTmpPatoIns.patol_ID>>',
					'<<m_chinis>>',
					convert(nvarchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
					convert(nvarchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
				)
		ENDTEXT

		IF !uf_gerais_actgrelha("", "", lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A INSERIR AS PATOLOGIAS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
	ENDSCAN
	
	IF USED("uCrsTmpPatoIns")
		fecha("uCrsTmpPatoIns")
	ENDIF

	RETURN .t.
ENDFUNC


**
FUNCTION uf_utentes_insereDiplomas
	LPARAMETERS lcIdstamp
	LOCAL lcSQL
			
	SELECT CL	
	IF !EMPTY(CL.utstamp)
		**Apaga registo de diplomas
		lcSQL = ""
		TEXT TO lcSQL NOSHOW textmerge
			DELETE 
			FROM	b_cli_diplomasID 
			WHERE	idstamp = '<<ALLTRIM(lcIdstamp)>>'
		ENDTEXT
		IF !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR OS DIPLOMAS ASSOCIADOS AO UTENTE. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: ACT-DIPLOMAS","OK","",16)
			RETURN .f.
		Endif
		
		**Insere registos
		SELECT ucrsdiplomasID
		GO TOP 
		SCAN
			lcSQL = ""
			TEXT TO lcSQL NOSHOW textmerge
				INSERT INTO b_cli_diplomasID (idstamp,dplmsstamp,diploma,u_design, patologia)
				values('<<ALLTRIM(lcIdstamp)>>','<<ALLTRIM(ucrsdiplomasID.dplmsstamp)>>','<<ALLTRIM(ucrsdiplomasID.diploma)>>','<<ALLTRIM(ucrsdiplomasID.u_design)>>','<<ALLTRIM(ucrsdiplomasID.patologia)>>')
			ENDTEXT

			IF !uf_gerais_actGrelha("","",lcSQL)
				uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR OS DIPLOMAS ASSOCIADOS AO UTENTE. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: ACT-DIPLOMAS","OK","",16)
				RETURN .f.
			Endif
		ENDSCAN
	
	ENDIF		

	RETURN .t.
ENDFUNC


** INSER��O DE HISTORICO
FUNCTION uf_utentes_insereHistorico
	IF !USED("CL")
		RETURN .t.
	ENDIF

	&&actualizar FT para "des-mapear" as linhas removidas
	IF USED("uCrsHistRemoveLinhas")
		SELECT uCrsHistRemoveLinhas
		SCAN
			lcSQL = ''
			TEXT TO lcSQL TEXTMERGE NOSHOW
				UPDATE ft
				SET u_hclstamp='apagado'
				where ftstamp='<<ALLTRIM(uCrsHistRemoveLinhas.stamp)>>'
			ENDTEXT
			uf_gerais_actGrelha("","",lcSQL)
		ENDSCAN
	ENDIF

	
	SELECT cl
	lcSQL = ""
	TEXT TO lcSQL textmerge NOSHOW
		DELETE 
			B_histConsumo
		WHERE
			clstamp='<<cl.utstamp>>'
			and data between '<<uf_gerais_getdate(utentes.pageframe1.page6.txtDe.value,"SQL")>>' and '<<uf_gerais_getdate(utentes.pageframe1.page6.txtA.value,"SQL")>>'
	ENDTEXT

	IF !uf_gerais_actgrelha("", "", lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR O HIST�RICO. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: ACT-HISTORICO","OK","",16)
		RETURN .f.
	ENDIF
	
	SELECT cl
	SELECT uCrsHisTrat
	GO TOP
	SCAN FOR uCrsHisTrat.alterado == .t.
		lcSQL = ''
		TEXT TO lcSQl TEXTMERGE NOSHOW
			INSERT INTO B_histConsumo
			(
				ref
				,design
				,data
				,qtt
				,obs
				,clstamp
				,site
				,ousrinis
				,ousrdata
				,ousrhora
				,usrinis
				,usrdata
				,usrhora
				,dci
				,posologia
				,dtini
				,dtp
				,palmoco
				,almoco
				,lanche
				,jantar
				,noite
				,fistamp
			)
			Values
			(	
				'<<ALLTRIM(uCrsHisTrat.ref)>>'
				,'<<ALLTRIM(uCrsHisTrat.design)>>'
				,'<<uf_gerais_getdate(uCrsHisTrat.datadoc,"SQL")>>'
				,<<uCrsHisTrat.qtt>>
				,'<<ALLTRIM(uCrsHisTrat.obsdoc)>>'
				,'<<cl.utstamp>>'
				,'<<mySite>>'
				,'<<m_chinis>>'
				,convert(nvarchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
				,convert(nvarchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
				,'<<m_chinis>>'
				,convert(nvarchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
				,convert(nvarchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
				,'<<ALLTRIM(uCrsHisTrat.dci)>>'
				,'<<ALLTRIM(uCrsHisTrat.posologia)>>'
				,'<<uf_gerais_getdate(uCrsHisTrat.dtini,"SQL")>>'
				,'<<uf_gerais_getdate(uCrsHisTrat.dtp,"SQL")>>'
				,<<IIF(uCrsHisTrat.palmoco,1,0)>>
				,<<IIF(uCrsHisTrat.almoco,1,0)>>
				,<<IIF(uCrsHisTrat.lanche,1,0)>>
				,<<IIF(uCrsHisTrat.jantar,1,0)>>
				,<<IIF(uCrsHisTrat.noite,1,0)>>
				,'<<ALLTRIM(uCrsHisTrat.fistamp)>>'
			)
		ENDTEXT

		IF !uf_gerais_actgrelha("", "", lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A INSERIR O HIST�RICO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
	ENDSCAN
	
	RETURN .t.
ENDFUNC


** INSERE CARTAO CLIENTE
FUNCTION uf_utentes_insereCartaoCliente
	LPARAMETERS lcCursor

	SELECT &lcCursor
	GO TOP
	SELECT * FROM &lcCursor INTO CURSOR uCrsTmpCartaoIns READWRITE
	
	SELECT uCrsTmpCartaoIns
	GO TOP

	** Se o cliente for novo e usar cart�o, validar registo na fidel **
	IF !EMPTY(uCrsTmpCartaoIns.nrcartao) AND LEN(alltrim(uCrsTmpCartaoIns.nrcartao)) > 6

		IF uf_gerais_actgrelha("", "uCrsInsCartao", [select nrcartao from B_fidel where nrcartao=']+ALLTRIM(uCrsTmpCartaoIns.nrcartao)+['])
			LOCAL lcStamp
			lcStamp = uf_gerais_stamp()
			IF RECCOUNT("uCrsInsCartao") == 0
				TEXT TO lcSql NOSHOW TEXTMERGE
					INSERT INTO B_fidel
						(
							fstamp, clstamp, clno, clestab,
							nrcartao, validade, ousr, usr
						)
					values
						('<<ALLTRIM(lcStamp)>>', '<<ALLTRIM(uCrsTmpCartaoIns.utstamp)>>', <<uCrsTmpCartaoIns.no>>, <<uCrsTmpCartaoIns.estab>>,
						'<<alltrim(uCrsTmpCartaoIns.nrcartao)>>', '30001231', <<ch_userno>>, <<ch_userno>>)
				ENDTEXT
				IF !uf_gerais_actgrelha("", "", lcSql)
					uf_perguntalt_chama("ATEN��O: OCORREU UM ERRO A REGISTAR CART�O DE CLIENTE. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
					RETURN .f.	
				ENDIF
			ELSE
				uf_perguntalt_chama("J� EXISTE UM CART�O DE CLIENTE COM O MESMO N�MERO. POR FAVOR VERIFIQUE.","OK","",48)
				RETURN .F.
			ENDIF
			fecha("uCrsInsCartao")
		ENDIF
	ENDIF

	IF USED("uCrsTmpCartaoIns")
		fecha("uCrsTmpCartaoIns")
	ENDIF 
	
	RETURN .t.
ENDFUNC 


** UPDATE CLIENTE
FUNCTION uf_utentes_update
	LPARAMETERS lcCursor
	
	&& Verificar se o cliente tem movimentos de conta corrente. Se tiver n�o deixa alterar o NIF
	text to lcSQLcl textmerge noshow
		Select ncont,nome  From b_utentes (nolock) where no=<<cl.no>> and estab=<<cl.estab>>
	ENDTEXT
	uf_gerais_actgrelha("", "uCrsncontcl", lcSqlcl)
	IF ALLTRIM(cl.ncont)<>ALLTRIM(uCrsncontcl.ncont) AND !EMPTY(uCrsncontcl.ncont)&& AND UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'
		text to lcSQLclcc textmerge noshow
			select ftstamp from ft where no=<<cl.no>> and estab=<<cl.estab>>
		ENDTEXT
		uf_gerais_actgrelha("", "uCrsclcc", lcSqlclcc)
		IF RECCOUNT("uCrsclcc")>0
			uf_perguntalt_chama("DESCULPE, MAS N�O PODE ALTERAR O NIF DE UM CLIENTE COM MOVIMENTOS DE CONTA CORRENTE!","OK","", 48)
			fecha("uCrsclcc")
			RETURN .f.
		ENDIF 
		fecha("uCrsclcc")
	ENDIF 
	
	IF ALLTRIM(cl.nome)<>ALLTRIM(uCrsncontcl.nome) AND (EMPTY(cl.ncont) OR ALLTRIM(cl.ncont)=='9999999999') AND UPPER(ALLTRIM(ucrse1.pais)) <> 'PORTUGAL'
		text to lcSQLclcc textmerge noshow
			select ftstamp from ft where no=<<cl.no>> and estab=<<cl.estab>>
		ENDTEXT
		uf_gerais_actgrelha("", "uCrsclcc", lcSqlclcc)
		IF RECCOUNT("uCrsclcc")>0
			uf_perguntalt_chama("DESCULPE, MAS N�O PODE ALTERAR O NOME DE UM CLIENTE SEM NIF E COM MOVIMENTOS DE CONTA CORRENTE!","OK","", 48)
			fecha("uCrsclcc")
			RETURN .f.
		ENDIF 
		fecha("uCrsclcc")
	ENDIF 
	fecha("uCrsclcc")
	
	&& Verificar se o cliente tem movimentos de conta corrente. Se tiver n�o deixa alterar o NOME
	IF (ALLTRIM(cl.ncont)=='' OR ALLTRIM(cl.ncont)=='999999999' OR ALLTRIM(cl.ncont)=='999999990' OR ALLTRIM(cl.ncont)=='9999999999') AND UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'
		text to lcSQLcl textmerge noshow
			Select nome From b_utentes (nolock) where no=<<cl.no>> and estab=<<cl.estab>>
		ENDTEXT
		uf_gerais_actgrelha("", "uCrsnomecl", lcSqlcl)
		IF (ALLTRIM(cl.nome)<>ALLTRIM(uCrsnomecl.nome)  or  len(alltrim(cl.nome))<>len(alltrim(uCrsnomecl.nome))) AND UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'
			text to lcSQLclcc textmerge noshow
				select ftstamp from ft where no=<<cl.no>> and estab=<<cl.estab>>
			ENDTEXT
			uf_gerais_actgrelha("", "uCrsclcc", lcSqlclcc)
			IF RECCOUNT("uCrsclcc")>0
				uf_perguntalt_chama("DESCULPE, MAS N�O PODE ALTERAR O NOME DE UM CLIENTE COM MOVIMENTOS DE CONTA CORRENTE!","OK","", 48)
				fecha("uCrsclcc")
				fecha("uCrsnomecl")
				RETURN .f.
			ENDIF 
			fecha("uCrsclcc")
		ENDIF
		fecha("uCrsnomecl") 
	ENDIF 
	
	IF !uf_gerais_getParameter_site('ADM0000000088', 'BOOL', mySite)
		** REGISTO DE ALTERA��ES NA TABELA DE LOGS DE UTILIZADOR
		local lcdados
		lcdados=''
		select cl
		select * FROM cl INTO CURSOR ucl READWRITE 
		select ucl
		
		TEXT TO lcSql NOSHOW textmerge
			SELECT *, pontos = 0, binovalidade = '19000101',compfixa = isnull(compfixa,0)
			FROM b_utentes (nolock) 
			left join cptorg (nolock) on cptorg.u_no=b_utentes.no
			WHERE utstamp= '<<alltrim(ucl.utstamp)>>'
		ENDTEXT
		IF !uf_gerais_actgrelha("", "cl_aux", lcSql)
			regua(2)
			RETURN .f.
		ENDIF
		
		SELECT ucl
		scan all
		SCATTER MEMO TO lacl
		SELECT cl_aux
		SCATTER MEMO TO lacl_aux
			FOR i = 1 TO ALEN(lacl)
			if vartype(lacl[i]) == vartype(lacl_aux[i])
				IF lacl[i] <> lacl_aux[i]
					IF alltrim(vartype(lacl[i]))=='C'
						lcdados=lcdados + 'Campo: '+ alltrim(field(i)) + ' - Valor original: '  + alltrim(lacl_aux[i]) + ' - Valor alterado: ' + alltrim(lacl[i]) + CHR(13)
					ENDIF 
					IF alltrim(vartype(lacl[i]))=='N'
						lcdados=lcdados + 'Campo: '+ alltrim(field(i)) + ' - Valor original: '  + alltrim(str(lacl_aux[i])) + ' - Valor alterado: ' + alltrim(str(lacl[i])) + CHR(13)
					ENDIF 
					IF alltrim(vartype(lacl[i]))=='L'
						lcdados=lcdados + 'Campo: '+ alltrim(field(i)) + ' - Valor original: '  + alltrim(iif(lacl_aux[i]=.t.,'Verdadeiro','Falso')) + ' - Valor alterado: ' + alltrim(iif(lacl[i]=.t.,'Verdadeiro','Falso')) + CHR(13)
					ENDIF
					IF UPPER(alltrim(field(i)))=='NO' OR UPPER(alltrim(field(i)))=='NCONT' OR UPPER(alltrim(field(i)))=='NOME2' OR UPPER(alltrim(field(i)))=='MORADA' OR UPPER(alltrim(field(i)))=='CODPOST' OR;
						UPPER(alltrim(field(i)))=='LOCAL' OR UPPER(alltrim(field(i)))=='ZONA' OR UPPER(alltrim(field(i)))=='FAX' OR UPPER(alltrim(field(i)))=='TELEFONE' OR UPPER(alltrim(field(i)))=='TLMVL' OR;
						UPPER(alltrim(field(i)))=='EMAIL' OR UPPER(alltrim(field(i)))=='URL' OR UPPER(alltrim(field(i)))=='PROFI' OR UPPER(alltrim(field(i)))=='HABILIT' OR UPPER(alltrim(field(i)))=='BINO' OR;
						UPPER(alltrim(field(i)))=='NASCIMENTO' OR UPPER(alltrim(field(i)))=='SEXO' OR UPPER(alltrim(field(i)))=='NBENEF' OR UPPER(alltrim(field(i)))=='NBENEF2' OR UPPER(alltrim(field(i)))=='PAI' OR;
						UPPER(alltrim(field(i)))=='MAE' 
						dadospessalt = .t.
					ENDIF 
				endif 
			endif 
		NEXT
		SKIP IN cl_aux
		SELECT ucl
		ENDSCAN
		**messagebox(lcdados)
		
		** REGISTO CONSULTA NOS LOGS DE UTILIZADOR
		uf_user_log_ins('CL', ALLTRIM(cl.utstamp), 'ALTERA��O', lcdados)
	ELSE 
	
	ENDIF 
	
	** Valida��o Dados antes update - A valida��o est� na fun��o de Gravar independentemente de inserir registo novo ou atualizar um registo j� existente.
	**uf_utente_validacoes()		
			
	SELECT &lcCursor
	GO TOP 
	SELECT * FROM &lcCursor INTO CURSOR uCrsTempUpCl READWRITE
	SELECT uCrsTempUpCl
	GO TOP
	
**	LOCAL lcSQL
	STORE '' TO lcSQl
	TEXT TO lcSQL TEXTMERGE noshow
		UPDATE b_utentes
		SET
			Nome			=	'<<ALLTRIM(uCrsTempUpCl.Nome)>>',
			No				=	<<uCrsTempUpCl.No>>,
			estab			=	<<uCrsTempUpCl.estab>>,
			Ncont			=	'<<ALLTRIM(uCrsTempUpCl.Ncont)>>',
			
			Nome2			=	'<<uCrsTempUpCl.Nome2>>',
			nrcartao		=	'<<ALLTRIM(uCrsTempUpCl.nrcartao)>>',
			inactivo		=	<<IIF(uCrsTempUpCl.inactivo,1,0)>>,
			naoencomenda	=	<<IIF(uCrsTempUpCl.naoencomenda,1,0)>>,
			nocredit		=	<<IIF(uCrsTempUpCl.nocredit,1,0)>>,
			cobnao			=	<<IIF(uCrsTempUpCl.cobnao,1,0)>>,
			
			Morada			=	'<<ALLTRIM(uCrsTempUpCl.Morada)>>',
			CodPost			=	'<<ALLTRIM(uCrsTempUpCl.CodPost)>>',
			Local			=	'<<ALLTRIM(uCrsTempUpCl.Local)>>',
			Zona			=	'<<uCrsTempUpCl.Zona>>',
			Fax				=	'<<ALLTRIM(uCrsTempUpCl.Fax)>>',
			Telefone		=	'<<ALLTRIM(uCrsTempUpCl.Telefone)>>',
			TLMVL			=	'<<ALLTRIM(uCrsTempUpCl.TLMVL)>>',
			Tipo			=	'<<uCrsTempUpCl.Tipo>>',
			Email			=	'<<ALLTRIM(uCrsTempUpCl.Email)>>',
			URL				=	'<<ALLTRIM(uCrsTempUpCl.URL)>>',
			profi			=	'<<ALLTRIM(uCrsTempUpCl.profi)>>',
			hablit			=	'<<ALLTRIM(uCrsTempUpCl.hablit)>>',
			bino			=	'<<ALLTRIM(uCrsTempUpCl.bino)>>',
			nascimento		=	'<<DTOS(uCrsTempUpCl.nascimento)>>',
			sexo			=	'<<uCrsTempUpCl.sexo>>',
			codigoP			=	'<<uCrsTempUpCl.codigoP>>',
			descP			=	'<<uCrsTempUpCl.descP>>',
			
			nbenef			=	'<<ALLTRIM(uCrsTempUpCl.nbenef)>>',
			nbenef2			=	'<<ALLTRIM(uCrsTempUpCl.nbenef2)>>',
			codpla			=	'<<ALLTRIM(uCrsTempUpCl.codpla)>>',
			desPLA			=	'<<ALLTRIM(uCrsTempUpCl.desPLA)>>',
			entPla			=	'<<ALLTRIM(uCrsTempUpCl.entPla)>>',
			VALIPLA			=	'<<DTOS(uCrsTempUpCl.VALIPLA)>>',
			VALIPLA2		=	'<<DTOS(uCrsTempUpCl.VALIPLA2)>>',
			
			Pai				=	'<<ALLTRIM(uCrsTempUpCl.pai)>>',
			mae				=	'<<ALLTRIM(uCrsTempUpCl.mae)>>',
			notif			=	<<IIF(uCrsTempUpCl.notif,1,0)>>,
			pacgen			=	<<IIF(uCrsTempUpCl.pacgen,1,0)>>,
			tratamento		=	'<<ALLTRIM(uCrsTempUpCl.tratamento)>>',
			OBS				=	'<<ALLTRIM(uCrsTempUpCl.OBS)>>',
			ninscs			=	'<<ALLTRIM(uCrsTempUpCl.ninscs)>>',
			csaude			=	'<<ALLTRIM(uCrsTempUpCl.csaude)>>',
			drfamilia		=	'<<ALLTRIM(uCrsTempUpCl.drfamilia)>>',
					
			Vencimento		=	<<uCrsTempUpCl.Vencimento>>,
			Desconto		=	<<uCrsTempUpCl.Desconto>>,
			tipodesc		=	'<<uCrsTempUpCl.tipodesc>>',
			alimite			=	<<uCrsTempUpCl.alimite>>,
			modofact		=	'<<uCrsTempUpCl.modofact>>',
			odatraso		=	<<uCrsTempUpCl.odatraso>>,
			tabiva			=	<<uCrsTempUpCl.tabiva>>,
			nib				=	'<<astr(uCrsTempUpCl.nib)>>',				
			particular		=	<<IIF(uCrsTempUpCl.particular,1,0)>>,
			autofact		=	<<IIF(uCrsTempUpCl.autofact,1,0)>>,
			clivd			=	<<IIF(uCrsTempUpCl.clivd,1,0)>>,
			clinica			=	<<IIF(uCrsTempUpCl.clinica,1,0)>>,
			
			eplafond		=	<<uCrsTempUpCl.eplafond>>,
			
			DescPP			=	<<uCrsTempUpCl.DescPP>>,
			radicaltipoemp	=	<<uCrsTempUpCl.radicaltipoemp)>>,
			TpDesc			=	'<<uCrsTempUpCl.TpDesc>>',
			Classe			=	'<<uCrsTempUpCl.Classe>>',
			Ccusto			=	'<<uCrsTempUpCl.Ccusto>>',
			fref			=	'<<uCrsTempUpCl.fref>>',
			Moeda			=	'<<uCrsTempUpCl.Moeda>>',
							
			Conta			=	'<<ALLTRIM(uCrsTempUpCl.Conta)>>',
			contaacer		=	'<<ALLTRIM(uCrsTempUpCl.contaacer)>>',
			contafac		=	'<<ALLTRIM(uCrsTempUpCl.contafac)>>',
			contaainc		=	'<<ALLTRIM(uCrsTempUpCl.contaainc)>>',
			contatit		=	'<<ALLTRIM(uCrsTempUpCl.contatit)>>',
			contaletdes		=	'<<ALLTRIM(uCrsTempUpCl.contaletdes)>>',
			contalet		=	'<<ALLTRIM(uCrsTempUpCl.contalet)>>',
			contaletsac		=	'<<ALLTRIM(uCrsTempUpCl.contaletsac)>>',
			
			usrinis			=	'<<m_chinis>>',
			usrdata			=	convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
			usrhora			=	convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108),
			
			entfact			=	<<IIF(uCrsTempUpCl.entfact,1,0)>>,
			peso			=	<<uCrsTempUpCl.peso>>,
			altura			=	<<uCrsTempUpCl.altura>>,
			
			recmmotivo		=	'<<ALLTRIM(uCrsTempUpCl.recmmotivo)>>',
			recmdescricao	=	'<<ALLTRIM(uCrsTempUpCl.recmdescricao)>>',
			recmdatainicio	=	'<<DTOS(uCrsTempUpCl.recmdatainicio)>>',
			recmdatafim		=	'<<DTOS(uCrsTempUpCl.recmdatafim)>>',
			recmtipo		=	'<<ALLTRIM(uCrsTempUpCl.recmtipo)>>',
			itmotivo		=	'<<ALLTRIM(uCrsTempUpCl.itmotivo)>>',
			itdescricao		=	'<<ALLTRIM(uCrsTempUpCl.itdescricao)>>',
			itdatainicio	=	'<<DTOS(uCrsTempUpCl.itdatainicio)>>',
			itdatafim		=	'<<DTOS(uCrsTempUpCl.itdatafim)>>',
			
			cesd			=	<<IIF(uCrsTempUpCl.cesd,1,0)>>,
			cesdcart		=	'<<ALLTRIM(uCrsTempUpCl.cesdcart)>>',
			cesdcp			=	'<<ALLTRIM(uCrsTempUpCl.cesdcp)>>',
			cesdidi			=	'<<ALLTRIM(uCrsTempUpCl.cesdidi)>>',
			cesdidp			=	'<<ALLTRIM(uCrsTempUpCl.cesdidp)>>',
			cesdpd			=	'<<ALLTRIM(uCrsTempUpCl.cesdpd)>>',
			cesdval			=	'<<DTOS(uCrsTempUpCl.cesdval)>>',
			
			atestado		=	<<IIF(uCrsTempUpCl.atestado,1,0)>>,
			atnumero		=	'<<ALLTRIM(uCrsTempUpCl.atnumero)>>',
			attipo			=	'<<ALLTRIM(uCrsTempUpCl.attipo)>>',
			atcp			=	'<<ALLTRIM(uCrsTempUpCl.atcp)>>',
			atpd			=	'<<ALLTRIM(uCrsTempUpCl.atpd)>>',
			atval			=	'<<DTOS(uCrsTempUpCl.atval)>>',
			
			codigoPNat		=	'<<uCrsTempUpCl.codigoPNat>>',
			descPNat		=	'<<uCrsTempUpCl.descPNat>>',
			pais 			= 	<<uCrsTempUpCl.pais>>,
			entCompart 		= 	<<IIF(uCrsTempUpCl.entCompart,1,0)>>,
			noConvencao		= 	'<<ALLTRIM(uCrsTempUpCl.noConvencao)>>',
			designConvencao	= 	'<<ALLTRIM(uCrsTempUpCl.designConvencao)>>',
			id				= 	'<<ALLTRIM(uCrsTempUpCl.id)>>',
			nprescsns		= 	<<IIF(uCrsTempUpCl.nprescsns,1,0)>>,
			exportado		= 	0,
			direcaoTec		= 	'<<ALLTRIM(uCrsTempUpCl.direcaoTec)>>',
			proprietario	= 	'<<ALLTRIM(uCrsTempUpCl.proprietario)>>',
			moradaAlt		= 	'<<ALLTRIM(uCrsTempUpCl.moradaAlt)>>',
			noAssociado		= 	<<uCrsTempUpCl.noAssociado>>,
			alvara			= 	<<uCrsTempUpCl.alvara>>,
			software		= 	'<<ALLTRIM(uCrsTempUpCl.software)>>',
			ars				= 	'<<ALLTRIM(uCrsTempUpCl.ars)>>',
			no_ext 			= 	'<<ALLTRIM(uCrsTempUpCl.no_ext)>>',
			codSwift		= 	'<<ALLTRIM(uCrsTempUpCl.codSwift)>>',
			nomcom			= 	'<<ALLTRIM(uCrsTempUpCl.nomcom)>>',
			autorizado		=	<<IIF(uCrsTempUpCl.autorizado,1,0)>>,
			autoriza_sms	=	<<IIF(uCrsTempUpCl.autoriza_sms,1,0)>>,
			autoriza_emails	=	<<IIF(uCrsTempUpCl.autoriza_emails,1,0)>>,
			site_compart	= 	'<<ALLTRIM(uCrsTempUpCl.site_compart)>>'
		where
			utstamp = '<<uCrsTempUpCl.utstamp>>'
	ENDTEXT
	
	IF UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'
		STORE '' TO lcSQl1
		TEXT TO lcSQL1 TEXTMERGE noshow		
			IF NOT exists (select * from dispensa_eletronica_cc where utstamp = '<<ALLTRIM(uCrsTempUpCl.utstamp)>>')
				BEGIN
					insert into dispensa_eletronica_cc (id, id_emp, nome, morada, nif, nrSNS, sexo, nascimento, validade_fim, publicKey, site, machine, doc_id, tipoDoc, utstamp) 
					values (LEFT(NEWID(),20), '<<ALLTRIM(uCrsE1.id_lt)>>', '<<ALLTRIM(uCrsTempUpCl.Nome)>>', '<<ALLTRIM(uCrsTempUpCl.Morada)>>'
						, '<<ALLTRIM(uCrsTempUpCl.Ncont)>>', '<<ALLTRIM(uCrsTempUpCl.nbenef)>>', '<<uCrsTempUpCl.sexo>>', '<<uf_gerais_getdate(uCrsTempUpCl.nascimento,"SQL")>>', '<<uf_gerais_getDate(uCrsTempUpCl.binoValidade,"SQL")>>'
						, '','<<ALLTRIM(mySite)>>', '','<<ALLTRIM(uCrsTempUpCl.bino)>>', 'CC', '<<ALLTRIM(uCrsTempUpCl.utstamp)>>')
				END
			ELSE 
				BEGIN 
					UPDATE dispensa_eletronica_cc SET validade_fim = '<<uf_gerais_getDate(uCrsTempUpCl.binoValidade,"SQL")>>' WHERE utstamp = '<<ALLTRIM(uCrsTempUpCl.utstamp)>>'
				END 
		ENDTEXT
		lcSQl = lcSQl + CHR(13) + lcSQl1
	ENDIF 
	
	IF !uf_gerais_actgrelha("", "", lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO ATUALIZAR OS DADOS DO UTENTE. POR FAVOR CONTACTE O SUPORTE. OBRIGADO.","OK","",16)
		RETURN .f.
	ELSE
		RETURN .t.
	ENDIF
ENDFUNC

** UPDATE DAS PATOLOGIAS
FUNCTION uf_utentes_updatePatologias
	&& Obtem as patologias anteriores para compara��o
	lcSQL = ""
	TEXT TO lcSQL NOSHOW textmerge
		SELECT	*
		FROM
			B_patologias (nolock)
		WHERE
			ostamp = '<<ALLTRIM(cl.utstamp)>>'
	ENDTEXT

	IF !uf_gerais_actgrelha("", "uCrsPatologiasAnteriores", lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR AS PATOLOGIAS. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: ACT-PATOLOGIAS","OK","", 16)
		RETURN .f.
	Endif
	
	&& APAGA LINHAS QUE J� N�O EXISTEM, existem na BD Mas n�o existem no cursor proposto para grava��o
	Select * from uCrsPatologiasAnteriores where patostamp not in (SELECT patostamp from UCRSPATOLOGIASCL) into cursor lcCursorDeletePatologias
	
	SELECT lcCursorDeletePatologias
	GO TOP
	SCAN
		lcSQL = ""
		TEXT TO lcSQL textmerge NOSHOW
			DELETE 
				B_patologias
			WHERE
				patostamp = '<<ALLTRIM(lcCursorDeletePatologias.patostamp)>>'
		ENDTEXT
		IF !uf_gerais_actgrelha("", "", lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR AS PATOLOGIAS. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: ACT-PATOLOGIAS","OK","",16)
			RETURN .f.
		ENDIF
		
		SELECT lcCursorDeletePatologias
	ENDSCAN
	
	IF USED("lcCursorDeletePatologias")
		Fecha("lcCursorDeletePatologias")
	ENDIF

	&& INSERE LINHAS NOVAS, EXISTEM NO CURSOR MAS N�O NA BASE DE DADOS
	SELECT * from UCRSPATOLOGIASCL where patostamp not in (select patostamp from uCrsPatologiasAnteriores) into cursor lcCursorInsertPatologias

	SELECT lcCursorInsertPatologias
	GO TOP
	SCAN
		lcSQL=""
		TEXT TO lcSQl TEXTMERGE NOSHOW
			INSERT INTO B_patologias (patostamp, ostamp, tabela, patol_id, ousrinis, ousrdata, ousrhora)
			Values
				(
					'<<ALLTRIM(lcCursorInsertPatologias.patostamp)>>',
					'<<CL.utstamp>>',
					'b_utentes',
					'<<lcCursorInsertPatologias.patol_id>>',
					'<<m_chinis>>',						
					convert(nvarchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
					convert(nvarchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
				)
		ENDTEXT	
		IF !uf_gerais_actgrelha("", "", lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR AS PATOLOGIAS. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: ACT-PATOLOGIAS","OK","",16)
			RETURN .f.	
		ENDIF
		
		SELECT lcCursorInsertPatologias
	ENDSCAN
	
	IF USED("lcCursorInsertPatologias")
		Fecha("lcCursorInsertPatologias")
	ENDIF
	
	IF USED("uCrsPatologiasAnteriores")
		Fecha("uCrsPatologiasAnteriores")
	ENDIF
	
	RETURN .t.
ENDFUNC


** UPDATE HISTORICO
FUNCTION uf_utentes_updateHistorico
			
	&& INSERE LINHAS
	IF uf_utentes_insereHistorico()	
		RETURN .t.
	ELSE
		RETURN .f.
	ENDIF
ENDFUNC


** Gravar Dados
FUNCTION uf_utentes_gravar
	LOCAL lcValida
	lcValida = .f.
	
	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()
	
	IF utentes.tmrSinc.enabled == .t.
		RETURN .f.
	ENDIF
	
	IF UPPER(ALLTRIM(uCrsE1.tipoempresa)) != "FARMACIA"
		*EFR
		SELECT uCrsEFRs
		GO Top
		IF RECCOUNT("uCrsEFRs")> 0 
			SELECT uCrsEFRs
			GO Top
			SELECT cl
			replace cl.entPla	WITH uCrsEFRs.abrev 
			replace cl.codPla	WITH uCrsEFRs.Codigo
			replace cl.desPla	WITH uCrsEFRs.Descricao 
			replace cl.nbenef2	WITH uCrsEFRs.numeroBenefEntidade
			replace cl.valipla2	WITH uCrsEFRs.DataValidade	
		ELSE
			IF EMPTY(ALLTRIM(cl.nbenef))
				SELECT cl
				replace cl.codPla	WITH "999998" 
				replace cl.desPla	WITH "Sem Comparticipa��o pelo SNS" 
				replace cl.nbenef2	WITH ""
				replace cl.valipla2	WITH ctod("1900.01.01")
					
				SELECT 	uCrsEFRs
				APPEND BLANK
				Replace uCrsEFRs.stamp WITH cl.utstamp
				Replace uCrsEFRs.Codigo WITH "999998"
				Replace uCrsEFRs.Descricao WITH "Sem Comparticipa��o pelo SNS"
			ELSE
				SELECT cl
				replace cl.codPla	WITH "935601" 
				replace cl.desPla	WITH "Servi�o Nacional de Sa�de" 
				replace cl.nbenef2	WITH ""
				replace cl.valipla2	WITH ctod("1900.01.01")
					
				SELECT 	uCrsEFRs
				APPEND BLANK
				Replace uCrsEFRs.stamp WITH cl.utstamp
				Replace uCrsEFRs.Codigo WITH "935601"
				Replace uCrsEFRs.Descricao WITH "Servi�o Nacional de Sa�de"
			ENDIF
		ENDIF
	ENDIF
	
	
	** Funcao com validacoes antes da gravacao
	IF UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'
		lcValida = uf_utente_validacoes()
	ELSE
		IF cl.no > 200
			lcValida = uf_utente_validacoes()
		ELSE
			lcValida = .t.
		ENDIF 
	ENDIF 
	
	IF lcValida == .t.
		uf_utentes_gravarBD('cl', 'UTENTES', 'uCrsPatologiasCL')
		IF LEFT(ALLTRIM(cl.ncont),1) <> '5' AND LEFT(ALLTRIM(cl.ncont),1) <> '9' AND LEFT(ALLTRIM(cl.ncont),1) <> '6'
			IF dadospessalt=.t.
				TEXT TO lcSQL TEXTMERGE NOSHOW
					update b_utentes SET autorizado=0, autoriza_emails=0, autoriza_sms=0 where no=<<cl.no>> and estab=<<cl.estab>>
				ENDTEXT 
				IF !uf_gerais_actgrelha("", "", lcSql)
					uf_perguntalt_chama("ERRO AO ATUALIZAR A TABELA DE UTENTES!","OK","",16)
					RETURN .F.
				ENDIF 
				uf_enviotoken_chama(ALLTRIM(ALLTRIM(Cl.utstamp)), 'b_utentes')
				dadospessalt=.f.
			ENDIF 
		ENDIF
	ENDIF 
ENDFUNC


**
FUNCTION uf_utente_validacoes
	LPARAMETERS lcCursor
	
	IF EMPTY(lcCursor)
		lcCursor = "cl"
	ENDIF
	
	

	** Verificar EFR
	IF UPPER(ALLTRIM(uCrsE1.tipoempresa)) == "CLINICA"
		SELECT &lcCursor 
		TEXT TO lcSql NOSHOW textmerge
			select 
				efr.dominio
			from 
				b_cli_efr efr 
			Where
				efr.cod = '<<ALLTRIM(cl.codpla)>>'
		ENDTEXT

		IF !uf_gerais_actGrelha("","uCrsEfrInfo",lcSql)
			uf_perguntalt_chama("N�o foi possivel verificar dados da Entidade Financeira. Contacte o suporte.","OK","",16)
			RETURN .f.
		ENDIF
		
		SELECT &lcCursor 
		SELECT uCrsEfrInfo
		
		** For�a Calculo do Regime de Comparticipa��o RECMTipo - est� dependente dos beneficios e outros beneficios, no caso de existirem altera��es
		lcCodigosBenf = ""
		SELECT uCrsOutrosBenef
		GO Top
		SCAN FOR !EMPTY(ALLTRIM(uCrsOutrosBenef.motivo))
			IF !EMPTY(ALLTRIM(lcCodigosBenf))
				lcCodigosBenf = ALLTRIM(lcCodigosBenf) + "," + ALLTRIM(uCrsOutrosBenef.motivo)
			ELSE
				lcCodigosBenf = ALLTRIM(uCrsOutrosBenef.motivo)
			ENDIF
		ENDSCAN 
		
		IF !EMPTY(ALLTRIM(lcCodigosBenf))
			lcCodigosBenf = ALLTRIM(lcCodigosBenf) + "," + ALLTRIM(cl.recmmotivo)
		ELSE
			lcCodigosBenf = ALLTRIM(cl.recmmotivo)
		ENDIF
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			exec up_prescricao_RecmTipo '<<lcCodigosBenf>>',0
		ENDTEXT

		IF !uf_gerais_actGrelha("","uCrsTipoRECM",lcSQL)
			uf_perguntalt_chama("N�o foi poss�vel verificar Regime de comparticipa��o.","OK","",16)
			return .f.
		ENDIF
		**	
		SELECT uCrsTipoRECM
		SELECT &lcCursor 
		replace &lcCursor..recmtipo	WITH uCrsTipoRECM.tipo
	ENDIF 
	**
	
	** Valida��es 
	&& Nome do cliente
	IF EMPTY(&lcCursor..nome)
		uf_perguntalt_chama("O CAMPO: NOME, � DE PREENCHIMENTO OBRIGAT�RIO.","OK","",48)
		RETURN .f.
	ENDIF
	
	&& VALIDA��O DA IDADE DO CLIENTE PRINCIPAL
	IF uf_gerais_getParameter_site('ADM0000000016', 'BOOL') == .t. AND year(&lcCursor..nascimento)=1900 AND &lcCursor..estab=0
		uf_perguntalt_chama("ATEN��O: O CLIENTE TEM DE TER A DATA DE NASCIMENTO PREENCHIDA.","OK","",48)
		RETURN .f.
	ENDIF 
	IF TYPE("PESQUTENTES") != "U" AND USED("uCrsInCLAtend")
		IF uf_gerais_getParameter_site('ADM0000000016', 'BOOL') == .t. AND year(uCrsInCLAtend.nascimento)=0 AND &lcCursor..estab=0
			uf_perguntalt_chama("ATEN��O: O CLIENTE TEM DE TER A DATA DE NASCIMENTO PREENCHIDA.","OK","",48)
			RETURN .f.
		ENDIF 
	ENDIF 
	IF uf_gerais_getParameter_site('ADM0000000016', 'BOOL') == .t. AND year(&lcCursor..nascimento)<>1900 AND &lcCursor..estab=0
		set century on
		IF int((date()-ctod(dtoc(&lcCursor..nascimento)))/365) < 18
			uf_perguntalt_chama("ATEN��O: O CLIENTE N�O PODE SER MENOR DE IDADE.","OK","",48)
			RETURN .f.
		ENDIF 
	ENDIF 
	
	&& Moeda
	SELECT &lcCursor
	GO TOP
	
	REPLACE &lcCursor..MOEDA WITH 'EURO'
	REPLACE &lcCursor..MOEDA WITH uf_gerais_getParameter("ADM0000000260","text")	
	
	&& Dados Duplicados De Cliente - Controlado via SP com os campos indicados abaixo - Lu�s Leal 2015-03-01
	LOCAL lcUtstamp, lcNO, lcEstab, lcNcont, lcNbenef, lcNbenef2, lcEmail, lcTlmvl, lcTelefone, lcID, lcMorada, lcNascimento, lcNo_ext, lcPais, lcBI
	STORE '' TO lcUtstamp, lcNbenef, lcNbenef2, lcEmail, lcID, lcMorada, lcMorada, lcPais
	STORE 0 TO lcNO, lcEstab, lcNcont, lcTlmvl, lcTelefone
	
	lcUtstamp 		= &lcCursor..utstamp
	lcNo 			= &lcCursor..no
	lcEstab 		= &lcCursor..estab
	lcNcont			= &lcCursor..ncont
	lcNbenef 		= ALLTRIM(&lcCursor..nbenef)
	lcNbenef2		= ALLTRIM(&lcCursor..nbenef2)
	lcEmail			= ALLTRIM(&lcCursor..email)
	lcTlmvl			= ALLTRIM(&lcCursor..tlmvl)
	lcTelefone		= ALLTRIM(&lcCursor..telefone)
	lcID			= ALLTRIM(&lcCursor..id)
	lcMorada		= ALLTRIM(&lcCursor..morada)
	lcNascimento	= DTOS(&lcCursor..nascimento)
	lcNo_ext		= ALLTRIM(&lcCursor..No_ext)
	lcPais 			= ALLTRIM(&lcCursor..codigop)
	lcBI			= ALLTRIM(&lcCursor..bino)
	
	
	** Valida��es 
	&& Numero contribuinte - na altera��o pode inativar

	IF USED("CL")	
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			Select inactivo From b_utentes (nolock) where no=<<cl.no>> and estab=<<cl.estab>>
		ENDTEXT 			

		IF !uf_gerais_actGrelha("", "uCrsInativo",lcSQL)
			MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A VERIFICAR DADOS DO UTENTE. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
		
		IF (!EMPTY(ALLTRIM(lcNcont)) AND !(LEN(ALLTRIM(lcNcont)) == 9) AND ALLTRIM(lcPais)=="PT" AND cl.inactivo=uCrsInativo.inactivo AND cl.inactivo=.f.)
			uf_perguntalt_chama("N�mero de contribuinte inv�lido. Deve conter 9 n�meros.","OK","",48)
			RETURN .f.
		ENDIF
	ENDIF 
	IF USED("FT")	
		IF (!EMPTY(ALLTRIM(lcNcont)) AND !(LEN(ALLTRIM(lcNcont)) == 9) AND ALLTRIM(lcPais)=="PT")
			uf_perguntalt_chama("N�mero de contribuinte inv�lido. Deve conter 9 n�meros.","OK","",48)
			RETURN .f.
		ENDIF
	ENDIF 

	
	
	&& Numero BI, apenas se preenchido - N�o � campo obrigat�rio
	IF (!EMPTY(ALLTRIM(lcBI))) 	
		IF (!(LEN(ALLTRIM(lcBI)) == 8) AND ALLTRIM(lcPais)=="PT")
			uf_perguntalt_chama("N�mero de doc ID. Deve conter 8 n�meros.","OK","",48)			
			RETURN .f.
		ENDIF
	ENDIF
	
	
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_utentes_camposduplicados '<<lcUtstamp>>', <<lcNo>>, <<lcEstab>>, '<<ALLTRIM(lcNcont)>>', '<<ALLTRIM(lcNbenef)>>', '<<ALLTRIM(lcNbenef2)>>'
										 ,'<<ALLTRIM(lcEmail)>>', '<<ALLTRIM(lcTlmvl)>>', '<<ALLTRIM(lcTelefone)>>', '<<ALLTRIM(lcID)>>', '<<ALLTRIM(lcMorada)>>', '<<ALLTRIM(lcNascimento)>>', '<<ALLTRIM(lcNo_ext)>>'
										 , '<<ALLTRIM(lcBi)>>'
	ENDTEXT 

	IF !(uf_gerais_actGrelha("","uCrsCamposRepetidos",lcSQL))
		uf_perguntalt_chama("Ocorreu uma anomalida a verificar os dados do Utente. Por favor contacte o Suporte.","OK","",16)
		RETURN .f.
	ENDIF

	IF !EMPTY(ALLTRIM(uCrsCamposRepetidos.texto)) AND uCrsCamposRepetidos.aviso == .f.
		uf_perguntalt_chama(uCrsCamposRepetidos.texto,"OK","",16)
		RETURN .f.
	ELSE
		IF !EMPTY(ALLTRIM(uCrsCamposRepetidos.texto)) AND uCrsCamposRepetidos.aviso == .t.
			uf_perguntalt_chama(uCrsCamposRepetidos.texto,"OK","",64)
		ENDIF
	ENDIF	

	&& Contas SNC
	SELECT &lcCursor
	GO TOP 
	IF EMPTY(&lcCursor..conta) 
		replace conta 		with ''
	ENDIF
	IF EMPTY(&lcCursor..contaacer)
		replace contaacer 	with ''
	ENDIF
	IF EMPTY(&lcCursor..contaainc)
		replace contaainc 	with ''
	ENDIF	
	IF EMPTY(&lcCursor..contalet)
		replace contalet 	with ''
	ENDIF
	IF EMPTY(&lcCursor..contaletdes)
		replace contaletdes with ''
	ENDIF
	If EMPTY(&lcCursor..contaletsac)
		replace contaletsac with ''
	endif	
	IF EMPTY(&lcCursor..contafac)
		replace contafac 	with ''
	ENDIF
	IF EMPTY(&lcCursor..contatit)
		replace contatit 	with ''	
	ENDIF

	&& Valida��es Para Prescri��o Electr�nica
	IF UPPER(ALLTRIM(uCrsE1.tipoempresa)) == "CLINICA"
		SELECT uCrsLojas
		LOCATE FOR ALLTRIM(uCrsLojas.site) == ALLTRIM(mySite)
		IF FOUND() AND uCrsLojas.prescricao == 1
			&& N� UTENTE
			&& Regras:
				* Preenchimento obrigatorio do N de Utente nos casos:
					* A) A EFR � o SNS
					* B) CNPRP
			&& Inicializa a variavel
			lcValida = .F.

			&& CASO A
			SELECT &lcCursor
			GO TOP
			IF UPPER(ALLTRIM(&lcCursor..despla)) = 'SNS'
				lcValida = .T.
			ENDIF
			
			&& CASO B
			IF (ALLTRIM(&lcCursor..codpla) == '41') AND (UPPER(ALLTRIM(&lcCursor..despla))=='SNS - DOEN�AS PROFISSIONAIS')
				lcValida = .T.
			ENDIF
			
			IF EMPTY(ALLTRIM(&lcCursor..nbenef))
				IF lcValida
					uf_perguntalt_chama("O UTENTE TEM UMA ENTIDADE FINANCEIRA RESPONS�VEL QUE REQUER O PREENCHIMENTO DO N� DE UTENTE!","OK","",48)
					RETURN .f.
				ENDIF
			ELSE
				IF !(LEN(ALLTRIM(&lcCursor..nbenef)) == 9)
					uf_perguntalt_chama("N�MERO DE UTENTE INV�LIDO. DEVE CONTER 9 NUMEROS. POR FAVOR VERIFIQUE."+CHR(13)+"Sincronize o Utente com RNU.","OK","",48)
					RETURN .f.
				ENDIF
			ENDIF
			**
			
			&& N� BENEFICI�RIO
			&& Regras:
				* Preenchimento obrigatorio do N benefeciario excepto nos casos:
					* A)O utente e beneficario apenas do SNS
					* B)O utente � cidadao estrangeiro e nao possui numero de utente nem cartao EFR nacional nem documento de direito. Neste caso a entidade deve ser preenchida
					*	como independente, nao devendo ser preenchidos o n� utente nem n� de beneficiario
				* Caso C) Se o utente tiver uma entidade portuguesa deve sempre pedir o n� beneficiario, salvo excep��es
			
			&& Inicializa as variaveis
			lcValida	= .T.
			SELECT &lcCursor
			lcNBenefEFR = &lcCursor..nbenef2
			
			&& CASO A
			IF UPPER(Alltrim(&lcCursor..entpla)) = 'SNS'
				lcValida = .f.
			ENDIF
				
			&& CASO B
			IF UPPER(ALLTRIM(&lcCursor..codigoP))!='PT' AND EMPTY(ALLTRIM(&lcCursor..nbenef)) AND EMPTY(ALLTRIM(&lcCursor..desPla))
				SELECT &lcCursor
				IF LEN(ALLTRIM(lcNBenefEFR))>0
					uf_perguntalt_chama("NO CASO DE CIDAD�OS ESTRANGEIROS SEM N�MERO DE UTENTE E CART�O EFR NACIONAL, O N� DE BENEFICI�RIO DA EFR N�O PODE ESTAR PREENCHIDO. POR FAVOR VERIFIQUE!","OK","",48)
					RETURN .f.
				ENDIF
				
				SELECT &lcCursor
				replace codpla WITH '999998'
				replace despla WITH 'Sem Comparticipa��o pelo SNS'
				replace entpla WITH 'Sem Comparticipa��o p/ SNS'
				
				lcValida = .f.
			ELSE
				** se for estrangeiro  e j� tiver entidade preenchida n�o obrigar n� de beneficiario
				lcValida = .f.
			ENDIF
			
			SELECT &lcCursor
			IF lcValida == .t. AND EMPTY(ALLTRIM(lcNBenefEFR))
				uf_perguntalt_chama("O CAMPO: N� BENEFICI�RIO DA EFR, � DE PREENCHIMENTO OBRIGAT�RIO!","OK","",48)
				RETURN .f.
			ENDIF
			**
			
			&& Entidade Financeira Responsavel
			SELECT &lcCursor
			IF EMPTY(ALLTRIM(&lcCursor..despla)) AND !EMPTY(&lcCursor..nbenef)
				uf_perguntalt_chama("O CAMPO: EFR, � DE PREENCHIMENTO OBRIGAT�RIO QUANDO O CIDAD�O � PORTUGU�S (VERIFIQUE A NACIONALIDADE)!","OK","",48)
				RETURN .f.
			ENDIF
			**
		ENDIF
	ENDIF
		
	&& Cartao Europeu de Seguro de Doen�as
	SELECT &lcCursor
	IF (&lcCursor..cesd)
		IF EMPTY(ALLTRIM(&lcCursor..cesdcp)) OR EMPTY(ALLTRIM(&lcCursor..cesdpd))
			uf_perguntalt_chama("O CAMPO: PA�S DA ENTIDADE - CESD, � DE PREENCHIMENTO OBRIGAT�RIO!","OK","",48)
			RETURN .f.
		ENDIF 
		
		IF EMPTY(ALLTRIM(&lcCursor..cesdidi))
			uf_perguntalt_chama("O CAMPO: N� DE IDENTIFICA��O DA INSTITUI��O - CESD, � DE PREENCHIMENTO OBRIGAT�RIO!","OK","",48)
			RETURN .f.
		ENDIF
		
		IF EMPTY(ALLTRIM(&lcCursor..cesdcart))
			uf_perguntalt_chama("O CAMPO: N� DE IDENTIFICA��O DO CART�O -CESD, � DE PREENCHIMENTO OBRIGAT�RIO!","OK","",48)
			RETURN .f.
		ENDIF
		
		IF EMPTY(&lcCursor..cesdval)
			uf_perguntalt_chama("O CAMPO: VALIDADE DO CART�O - CESD, � DE PREENCHIMENTO OBRIGAT�RIO!","OK","",48)
			RETURN .f.
		ENDIF
		
		IF EMPTY(ALLTRIM(&lcCursor..cesdidp))
			uf_perguntalt_chama("O CAMPO: N� DE IDENTIFICA��O PESSOAL - CESD, � DE PREENCHIMENTO OBRIGAT�RIO!","OK","",48)
			RETURN .f.
		ENDIF
	ENDIF
	
	&& Atestado de Direito
	SELECT &lcCursor
	IF (&lcCursor..atestado)
		IF EMPTY(ALLTRIM(&lcCursor..atcp)) OR EMPTY(ALLTRIM(&lcCursor..atpd))
			uf_perguntalt_chama("O CAMPO: PA�S DA ENTIDADE - ATESTADO DE DIREITO, � DE PREENCHIMENTO OBRIGAT�RIO!","OK","",48)
			RETURN .f.
		ENDIF 
		
		IF EMPTY(ALLTRIM(&lcCursor..atnumero))
			uf_perguntalt_chama("O CAMPO: N� DE IDENTIFICA��O DO ATESTADO DE DIREITO, � DE PREENCHIMENTO OBRIGAT�RIO!","OK","",48)
			RETURN .f.
		ENDIF
		
		IF EMPTY(ALLTRIM(&lcCursor..attipo))
			uf_perguntalt_chama("O CAMPO: TIPO DE ATESTADO DE DIREITO, � DE PREENCHIMENTO OBRIGAT�RIO!","OK","",48)
			RETURN .f.
		ENDIF
		
		IF EMPTY(&lcCursor..atval)
			uf_perguntalt_chama("O CAMPO: VALIDADE DO ATESTADO DE DIREITO, � DE PREENCHIMENTO OBRIGAT�RIO!","OK","",48)
			RETURN .f.
		ENDIF
	ENDIF
		
ENDFUNC 


**
FUNCTION uf_utentes_gravarBD
	LPARAMETERS lcCursor, lcPainel, lcCursorPato

	LOCAL lcSQL,lcValida,validaInsereCliente,validaInserePatologias,validaUpdateCliente,validaUpdatePatologias,;
		validaInsereFidel, validaInsereHistorico, validaUpdateHistorico, validaInsereDiplomas, validaUpdateDiplomas,;
	 	validaGravaOutrosBenef, validaGravaEfrs, validaGravaComparticipacoes,validaGravaBioRegistos, lcutstamp
	
	STORE '' TO lcSQL
	STORE 0 TO lcValida
	STORE .f. TO validaInsereCliente, validaInserePatologias, validaUpdateCliente, validaUpdatePatologias,;
				 validaInsereFidel, validaInsereDiplomas, validaUpdateDiplomas, validaGravaOutrosBenef,;
				 validaGravaEfrs, validaGravaOutrosBenef, validaGravaComparticipacoes, validaGravaBioRegistos   
	
	SELECT &lcCursor
	GO TOP

	** For�ar a actualiza��o de dados antes de gravar
	IF UPPER(ALLTRIM(lcPainel)) == "UTENTES"
		utentes.txtNome.setFocus
	ENDIF
	
	IF uf_gerais_actgrelha("", "", [Begin Transaction])

		IF myClIntroducao OR lcPainel == "CRIARCLATEND"
			&& dados gerais
			validaInsereCliente 	= uf_utentes_insere(lcCursor)

			&& patologias
			IF TYPE("UTENTES")!= "U" AND utentes.flagPatologias
				validaInserePatologias = uf_utentes_inserePatologias(lcCursorPato, &lcCursor..utstamp)
				utentes.flagPatologias = .f.
			ELSE
				validaInserePatologias = .t.
			ENDIF 
			
			&& diplomas
			IF TYPE("UTENTES")!= "U" &&AND utentes.flagDiplomas
				validaInsereDiplomas = uf_utentes_insereDiplomas(&lcCursor..utstamp)
			ELSE
				validaInsereDiplomas = .t.
			ENDIF
			
			&& cart�o de cliente
			validaInsereFidel = uf_utentes_insereCartaoCliente(lcCursor)
			
			&& hist�rico de tratamento
			validaInsereHistorico = uf_utentes_insereHistorico()
						
			&& outros benef�cios
			validaGravaOutrosBenef = uf_utentes_GravaOutrosBenef(&lcCursor..utstamp)
					
			&&Efrs
			validaGravaEfrs = uf_utentes_GravaEFRs(&lcCursor..utstamp)
			
			&&RegistoClinico
			validaGravaRegistoClinico = uf_utentes_GravaRegistoClinico(&lcCursor..utstamp)	
			
			&& BioRegistos
			validaGravaBioRegistos = uf_utentes_GravaBioRegistos()
			
			&&Entidade
			validaGravaEntidadesFaturacao = uf_utentes_entidadesFaturacao()			
			
			&&Servi�os Fact
			validaGravaServFaturacao = uf_utentes_ServFaturacao()			
			
			Select &lcCursor
			lcutstamp = &lcCursor..utstamp
			
			IF validaInsereCliente AND validaInserePatologias AND validaInsereFidel AND validaInsereHistorico;
				AND validaInsereDiplomas AND validaGravaOutrosBenef AND validaGravaRegistoClinico AND validaGravaBioRegistos 
				
				
				IF uf_gerais_actgrelha("", "", [commit Transaction])
				
					** Atribui campanhas ao novo utente - Problemas de Performance
					uf_campanhas_actualizaCampanhasNovoUtente(&lcCursor..no, &lcCursor..estab)
						
					** actualiza ultimo registo **
					uf_gerais_gravaUltRegisto('b_utentes', &lcCursor..utstamp)
					
					IF UPPER(ALLTRIM(lcPainel)) == "UTENTES"
						uf_perguntalt_chama("UTENTE INSERIDO COM SUCESSO.","OK","",64)	
						
						&& Configura ecra
						myClIntroducao = .f.
						myClAlteracao = .f.
						uf_utentes_confEcra()
						uf_utentes_alternaMenu()
						
						** chama cliente
						uf_utentes_chama(&lcCursor..no, &lcCursor..estab)		
						utentes.refresh
					ENDIF
				ELSE
					uf_gerais_actgrelha("", "", [rollback transaction])
				ENDIF
			ELSE
				uf_gerais_actgrelha("", "", [rollback transaction])
			ENDIF
		ELSE
			&& dados gerais
			validaUpdateCliente = uf_utentes_update("CL")
			
			&& patologias
			IF utentes.flagPatologias
				validaUpdatepatologias = uf_utentes_updatePatologias()
				utentes.flagPatologias = .f.
			ELSE
				validaUpdatepatologias = .t.
			ENDIF 
			
			&& diplomas
			IF TYPE("UTENTES")!= "U" &&AND utentes.flagDiplomas
				validaInsereDiplomas = uf_utentes_insereDiplomas(&lcCursor..utstamp)
			ELSE
				validaInsereDiplomas = .t.
			ENDIF 
			
			&& hist�rico de tratamento
			validaUpdateHistorico = uf_utentes_updateHistorico()
			
			&& outros benef�cios
			validaGravaOutrosBenef = uf_utentes_GravaOutrosBenef(&lcCursor..utstamp)
			
			&& Efrs
			validaGravaEfrs = uf_utentes_GravaEFRs(&lcCursor..utstamp)
			
			&& RegistoClinico
			validaGravaRegistoClinico = uf_utentes_GravaRegistoClinico(&lcCursor..utstamp)
			
			&& BioRegistos
			validaGravaBioRegistos = uf_utentes_GravaBioRegistos()
			
			&& Entidades
			validaGravaEntidadesFaturacao = uf_utentes_entidadesFaturacao()
			
			&& Servi�os Fact
			validaGravaServFaturacao = uf_utentes_ServFaturacao()
				
			Select &lcCursor
			lcutstamp = &lcCursor..utstamp
			
			IF validaUpdateCliente AND validaUpdatepatologias AND validaUpdateHistorico AND validaInsereDiplomas;
				AND validaGravaOutrosBenef AND validaGravaEfrs AND validaGravaRegistoClinico AND validaGravaBioRegistos
				
				IF uf_gerais_actgrelha("", "", [commit Transaction])
				
					IF utentes.semperguntas == .f.
						uf_perguntalt_chama("DADOS ALTERADOS COM SUCESSO.","OK","",64)
					ENDIF

					&& Configura ecra
					myClIntroducao = .f.
					myClAlteracao = .f.
					uf_utentes_confEcra()
					uf_utentes_alternamenu()
					
					** Atribui campanhas ao novo utente
					uf_campanhas_actualizaCampanhasNovoUtente(&lcCursor..no,&lcCursor..estab)
							
					** actualiza ultimo registo **
					uf_gerais_gravaUltRegisto('b_utentes', lcutstamp)
						
					uf_utentes_chama(&lcCursor..no, &lcCursor..estab)		
					utentes.refresh
				ELSE
					uf_gerais_actgrelha("", "", [rollback transaction])
				endif	
			ELSE
				uf_gerais_actgrelha("", "", [rollback transaction])
			endif
		ENDIF
	ELSE
		uf_gerais_actgrelha("", "", [rollback transaction])
	ENDIF	
	
	RETURN .t.
ENDFUNC


**
FUNCTION uf_utentes_GravaOutrosBenef
	Lparameters lcIdstamp 

	IF EMPTY(lcIdstamp)
		RETURN .f.
	ENDIF
	
	IF USED("uCrsOutrosBenef")
		lcSQL = ""
		TEXT TO lcSQL NOSHOW textmerge
			DELETE FROM b_cli_OutrosBenef Where stamp = '<<ALLTRIM(lcIdstamp)>>'
		ENDTEXT
		IF !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR OS OUTROS BENEFICIOS.","OK","",16)
			RETURN .f.
		ENDIF

		Select uCrsOutrosBenef
		GO TOP
		SCAN
			IF !EMPTY(ALLTRIM(uCrsOutrosBenef.motivo))
				lcSQL = ""
				TEXT TO lcSQL NOSHOW textmerge
					INSERT INTO b_cli_OutrosBenef (stamp,motivo,descricao,datainicio,datafim)
					values
					(
						'<<ALLTRIM(lcIdstamp)>>',
						'<<ALLTRIM(uCrsOutrosBenef.motivo)>>',
						'<<ALLTRIM(uCrsOutrosBenef.descricao)>>',
						'<<uf_gerais_getDate(uCrsOutrosBenef.datainicio,"SQL")>>',
						'<<uf_gerais_getDate(uCrsOutrosBenef.datafim,"SQL")>>'		
					)
				ENDTEXT

				IF !uf_gerais_actGrelha("","",lcSQL)
					uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR OUTROS BENEFICIOS.","OK","",16)
					RETURN .f.
				ENDIF
			ENDIF		
		ENDSCAN
		Select uCrsOutrosBenef
		GO TOP
		
	ENDIF
	
	RETURN .t.
ENDFUNC


**
FUNCTION uf_utentes_GravaEFRs
	Lparameters lcIdstamp 

	LOCAL lcnbenef2, lccodpla, desPLA, entPla, VALIPLA2 
	

	IF EMPTY(lcIdstamp)
		RETURN .f.
	ENDIF
		
	&& For�a actualiza��o dos dados
	
	IF USED("uCrsEFRs")
		lcSQL = ""
		TEXT TO lcSQL NOSHOW textmerge
			DELETE FROM b_cli_efrutente Where stamp = '<<ALLTRIM(lcIdstamp)>>'
		ENDTEXT
		IF !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR AS EFRs.","OK","",16)
			RETURN .f.
		ENDIF
		
		lcEntidadeDefeito = 0
		Select uCrsEFRs
		GO TOP
		SCAN
			IF !EMPTY(ALLTRIM(uCrsEFRs.codigo))
			
				lcSQL = ""
				TEXT TO lcSQL NOSHOW textmerge
					INSERT INTO b_cli_efrutente(stamp,codigo,descricao,numerobenefentidade,datavalidade,pais,numcartao,tipocartao)
					values
					(
						'<<ALLTRIM(lcIdstamp)>>',
						'<<ALLTRIM(uCrsEFRs.codigo)>>',
						'<<ALLTRIM(uCrsEFRs.descricao)>>',
						'<<ALLTRIM(uCrsEFRs.numerobenefentidade)>>',
						'<<uf_gerais_getDate(uCrsEFRs.datavalidade,"SQL")>>',
						'<<ALLTRIM(uCrsEFRs.pais)>>',
						'<<ALLTRIM(uCrsEFRs.numcartao)>>',
						'<<ALLTRIM(uCrsEFRs.tipocartao)>>'
					)
				ENDTEXT

				IF lcEntidadeDefeito == 0
					lcnbenef2  = uCrsEFRs.numeroBenefEntidade 
					lccodpla = ALLTRIM(uCrsEFRs.codigo)
					lcdesPLA = ALLTRIM(uCrsEFRs.descricao)
					lcVALIPLA2 = uCrsEFRs.datavalidade
					
					lcEntidadeDefeito = 1
				ENDIF

				IF !uf_gerais_actGrelha("","",lcSQL)
					uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR AS EFRs.","OK","",16)
					RETURN .f.
				ENDIF
			ENDIF		
		ENDSCAN
		
		
		IF !EMPTY(lccodpla) AND UPPER(ALLTRIM(uCrsE1.tipoempresa)) == "CLINICA"
			lcSQL = ""
			TEXT TO lcSQL NOSHOW textmerge
				UPDATE 
					b_utentes
				SET 	
					nbenef2 = '<<lcnbenef2>>'
					,codpla = '<<ALLTRIM(lccodpla)>>'
					,desPLA = '<<ALLTRIM(lcdesPLA)>>'
					,entPla = ISNULL((select top 1 abrev from b_cli_efr where cod = '<<ALLTRIM(lccodpla)>>'),'')
					,VALIPLA2 = '<<uf_gerais_getDate(lcVALIPLA2,"SQL")>>'
				Where 
					utstamp = '<<ALLTRIM(lcIdstamp)>>'
			ENDTEXT	
			IF !uf_gerais_actGrelha("","",lcSQL)
				uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR AS EFRs.","OK","",16)
				RETURN .f.
			ENDIF
		ENDIF
	ENDIF
	
	RETURN .t.
ENDFUNC



** ELIMINAR Cliente	
FUNCTION uf_utentes_eliminar
	IF !(uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Gest�o de Clientes - Eliminar'))
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ELIMINAR CLIENTES.","OK","",48)
		RETURN .f.
	ENDIF
	
	LOCAL lcSQL
	STORE '' TO lcSQL	
	
	&&Valida��es
	IF USED("CL")
		SELECT CL
		IF !EMPTY(CL.utstamp)
			&& vali��es apenas para clientes do tipo Farm�cia devido � numera��o especifica de clientes nas farm�cias - LL 20160518
			IF UPPER(ALLTRIM(uCrsE1.tipoEmpresa)) == "FARMACIA"
				&& Valida Clientes abaixo do 200
				SELECT cl
				IF cl.no < 200
					uf_perguntalt_chama("N�O PODE APAGAR CLIENTES A BAIXO DO N�200!","OK","", 48)
					RETURN .f.
				ENDIF
				IF cl.no == 200
					uf_perguntalt_chama("N�O PODE APAGAR O CLIENTE 200!","OK","", 48)
					RETURN .f.
				ENDIF
			ENDIF

			&& Verificar Se o Sistema Permite Criar Clientes
			IF uf_gerais_getParameter("ADM0000000120", "BOOL")
				uf_perguntalt_chama("A CRIA��O/ALTERA��O/ELIMINA��O DE CLIENTES EST� BLOQUEADA NESTE SISTEMA.","OK","", 64)
				RETURN .f.
			ENDIF	
				
			&& Conta corrente
			IF uf_gerais_actgrelha("", "uCrstempCC", [Select top 1 * from ft (nolock) where no=]+STR(CL.no)+[ and estab =]+STR(CL.estab))
				IF RECCOUNT("uCrstempCC")>0
					uf_perguntalt_chama("N�O PODE APAGAR CLIENTES COM MOVIMENTOS.","OK","", 48)
					FECHA("uCrstempCC")
					RETURN .f.
				ENDIF
				FECHA("uCrstempCC")
			ENDIF
			
			&& Documentos de Factura��o
			IF uf_gerais_actgrelha("", "uCrstempFT", [Select top 1 ftstamp from ft (nolock) where no=]+STR(CL.no)+[ and estab=]+STR(CL.estab))
				IF RECCOUNT("uCrstempFT")>0
					uf_perguntalt_chama("N�O PODE APAGAR CLIENTES COM DOCUMENTOS DE FACTURA��O.","OK","", 48)	
					FECHA("uCrstempFT")
					RETURN .f.
				ENDIF 	
				FECHA("uCrstempFT")
			ENDIF
			
			&& Dossiers Internos
			IF uf_gerais_actgrelha("", "uCrstempBO", [Select top 1 bostamp from BO (nolock) inner join ts (nolock) on bo.nmdos=ts.nmdos where bdempresas='CL' and no=]+STR(CL.no)+[ and estab=]+STR(CL.estab))
				IF RECCOUNT("uCrstempBO") > 0
					uf_perguntalt_chama("N�O PODE APAGAR CLIENTES COM DOSSIERS INTERNOS.","OK","", 48)	
					FECHA("uCrstempBO")
					RETURN .f.
				ENDIF 	
				FECHA("uCrstempBO")
			ENDIF
	
			&& Estabelecimentos
			SELECT CL
			IF (CL.estab == 0)
				IF uf_gerais_actgrelha("", "uCrstempCLestab", [Select top 1 * from b_utentes (nolock) where no=]+STR(CL.no)+[ and estab>0])
					IF RECCOUNT("uCrstempCLestab")>0
						uf_perguntalt_chama("N�O PODE APAGAR CLIENTES COM DEPENDENTES.","OK","",48)
						fecha("uCrstempCLestab")
						RETURN .f.
					ENDIF
					FECHA("uCrstempCLestab")
				ENDIF
			ENDIF

			&& Se tem prescri��es
			uf_gerais_actGrelha("","uCrstempPacPresc","Select top 1 * from b_cli_presc (nolock) where utstamp = '" + ALLTRIM(astr(cl.utstamp)) + "'")	
			IF RECCOUNT()>0
				uf_perguntalt_chama("N�O PODE APAGAR UTENTES COM PRESCRI��ES ASSOCIADAS.","OK","",48)	
				RETURN .f.
			ENDIF
			IF USED("uCrstempPacPresc")
				FECHA("uCrstempPacPresc")
			ENDIF

			&& Passou valida��es - vai apagar				
			IF uf_perguntalt_chama("Tem a certeza que deseja apagar o Utente?" + CHR(13) + "Vai perder todos os dados para sempre.","Sim","N�o")
				IF uf_gerais_actgrelha("", "", [BEGIN TRANSACTION])	
					SELECT CL
					IF uf_gerais_actgrelha("", "", [delete from B_PATOLOGIAS where ostamp=']+ALLTRIM(cl.utstamp)+['])		
						&& Verifica se usa Cart�o de Cliente e Inactiva Cart�o **
						IF uf_gerais_getParameter('ADM0000000052', 'BOOL')
							SELECT CL	
							IF !uf_gerais_actgrelha("", "", [update B_fidel set inactivo = 1 where clstamp=']+alltrim(cl.utstamp)+['])
								uf_gerais_actgrelha("", "", [ROLLBACK])
								uf_perguntalt_chama("OCORREU UMA ANOMALIA A INACTIVAR CART�O DE CLIENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
								RETURN .f.
							ENDIF
						ENDIF
						
						&&hist consumo
						SELECT CL
						IF !uf_gerais_actgrelha("", "", [delete from B_histConsumo where clstamp=']+ALLTRIM(cl.utstamp)+['])
							uf_gerais_actgrelha("", "", [ROLLBACK])
							uf_perguntalt_chama("OCORREU UMA ANOMALIA AO APAGAR O CLIENTE", "OK","",16)
							RETURN .f.
						ENDIF 
						
						&&outros benef
						SELECT CL
						IF !uf_gerais_actgrelha("", "", [delete from b_cli_outrosbenef where stamp=']+ALLTRIM(cl.utstamp)+['])
							uf_gerais_actgrelha("", "", [ROLLBACK])
							uf_perguntalt_chama("OCORREU UMA ANOMALIA AO APAGAR O CLIENTE", "OK","",16)
							RETURN .f.
						ENDIF 
						
						&& APAGA FICHA DO CLIENTE 
						SELECT CL
						IF uf_gerais_actgrelha("", "", [delete from b_utentes where utstamp=']+ALLTRIM(cl.utstamp)+['])

							uf_gerais_actgrelha("", "", [COMMIT TRANSACTION])

							uf_perguntalt_chama("REGISTO APAGADO COM SUCESSO.","OK","",64)
							
							** limpar cursor de pesquisa
							IF USED("uCrsPesquisarCL")
								SELECT uCrsPesquisarCL
								LOCATE FOR ALLTRIM(uCrsPesquisarCL.utstamp) == ALLTRIM(cl.utstamp)
								DELETE
								GO TOP
							ENDIF 
							**
							
							&& navega para ultimo registo
							uf_utentes_navegaUltRegisto()
						ELSE
							uf_gerais_actgrelha("", "", [ROLLBACK])
							uf_perguntalt_chama("OCORREU UMA ANOMALIA AO APAGAR O CLIENTE!", "OK","",16)
						ENDIF
					ELSE
						uf_gerais_actgrelha("", "", [ROLLBACK])
						uf_perguntalt_chama("OCORREU UMA ANOMALIA AO APAGAR O CLIENTE!","OK","", 16)
					ENDIF
				ELSE
					RETURN .f.
				ENDIF	
			ENDIF 
		ENDIF 
	ENDIF
ENDFUNC



** Sair do ecra de Clientes
FUNCTION uf_utentes_sair
	dadospessalt = .f.
	IF utentes.tmrSinc.enabled == .t. AND utentes.semperguntas == .f.
		RETURN .f.
	ENDIF
	initpesq=0
	IF (myClAlteracao == .t. OR myClIntroducao == .t.) AND utentes.semperguntas != .t.
		If uf_perguntalt_chama("Deseja cancelar as altera��es?" + CHR(13) + "Ir� perder as �ltimas altera��es feitas.","Sim","N�o")
			IF myClIntroducao
				&& navega para ultimo registo
				myClIntroducao = .f.
				myClAlteracao = .f.
				uf_utentes_navegaUltRegisto()
			ELSE && Altera��o
				myClIntroducao = .f.
				myClAlteracao = .f.
				uf_utentes_chama(CL.no,CL.estab)
			ENDIF

			&&Actualizar Painel
		*	uf_utentes_confEcra()
			
			uf_utentes_alternaMenu()
			utentes.ctnTabs1.visible = .t.
			utentes.ctnTabs2.visible = .f.
			utentes.ctnTabs3.visible = .f.
			utentes.ctnTabs1.lbl1.click
			
			utentes.Refresh
		ENDIF	
	ELSE
		&& Evitar erros de rowsource/recordsource ao sair		
		utentes.pageframe1.page2.list1.rowsource		=	''
		utentes.pageframe1.page2.list2.rowsource		=	''

		utentes.pageframe1.page3.grdEstab.recordsource	=	''
	
		&& fecha cursores
		Fecha("CL")
		Fecha("uCrsCLZona")
		Fecha("uCrsCLTipo")
		Fecha("uCrsCLNacins")
		FECHA("uCrsPatologias")
		FECHA("uCrsPatologiasCL")
		Fecha("uCrsEstabCL")
		fecha("uCrsCLTipoDesc")
		FECHA("uCrsCLCct")
		FECHA("uCrsCLMoeda")
		FECHA("uCrsCLFref")
		fecha("uCrsPesquisarCLTipo")
		fecha("uCrsHisTrat")
		initpesq=0
		** painel
		utentes.hide
		utentes.Release
		
		IF !TYPE("ImprimirCLFL")=="U"
			IF myOrigemTabelaImpClFl == "CL"
				uf_imprimirClFl_sair()
			ENDIF
		ENDIF
	ENDIF
ENDFUNC


** CONTROLA ACESSO AO PAINEL PAGAMENTOS A CLIENTES
*!*	FUNCTION uf_utentes_chamaPagamentos
*!*		IF (uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Pagamentos a Clientes'))
*!*			navega("PO")
*!*		ELSE
*!*			uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL DE PAGAMENTOS A CLIENTES.", "OK","",48)
*!*		ENDIF
*!*	ENDFUNC 


** NAVEGA REGISTO ANTERIOR
FUNCTION uf_utentes_anterior
	SELECT CL
	lcSQL=""
	
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		SELECT TOP 1
			no
		FROM
			b_utentes (nolock)
		WHERE
			no < <<IIF(EMPTY(CL.no),0,CL.no)>>
		order by
			no desc
	ENDTEXT  
	If !uf_gerais_actgrelha("", "uCrstempCL", lcSQL)
		RETURN .f.
	ENDIF
	
	uf_utentes_chama(uCrstempCL.no)
	
	fecha("uCrstempCL")
endfunc	



*********************************
*	 NAVEGA PROXIMO REGISTO 	*
*********************************
FUNCTION uf_utentes_seguinte
	SELECT CL
	lcSQL=""
	
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		SELECT TOP 1
			no, estab
		FROM
			b_utentes (nolock)
		WHERE
			no > <<IIF(EMPTY(CL.no),0,CL.no)>>
		order by
			no asc
	ENDTEXT  
	
	If !uf_gerais_actgrelha("", "uCrstempCL", lcSQL)
		RETURN .f.
	ENDIF
	
	uf_utentes_chama(uCrstempCL.no)
	
	fecha("uCrstempCL")
ENDFUNC


** Valida Acesso - Conta Corrente
FUNCTION uf_utentes_chamaCC
	IF (uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Clientes - Conta Corrente'))
		SELECT cl
		uf_ccCliente_chama(cl.no, cl.estab, cl.nome)
	ELSE
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE CONSULTAR OS MOVIMENTOS DE CONTA CORRENTE.","OK","",48)
	ENDIF
ENDFUNC


** Valida Acesso - Conta Corrente N.Reg
FUNCTION uf_utentes_chamaCCNR
	IF (uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Clientes - Conta Corrente'))
		SELECT cl
		uf_ccNrCliente_chama(cl.no,cl.estab,cl.nome)
	ELSE
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE CONSULTAR OS MOVIMENTOS DE CONTA CORRENTE.","OK","",48)
	ENDIF
ENDFUNC


** CALCULAR CONTA SNC PARA Cliente
FUNCTION uf_utentes_contaSNC
	LPARAMETERS tcBool, lcPainel
	
	LOCAL lcSQL, lctemp, lcConta, lcContalet, lcContaainc, lcContaletdes, lcContafac, lcContatit, lcContaletsac, lcContaacer,lcDescrConta
	STORE '' TO lcSql, lcConta, lcContalet, lcContaainc, lcContaletdes, lcContafac, lcContatit, lcContaletsac, lcContaacer,lcDescrConta
	STORE 0 TO lctemp
	
	FOR lctemp = 1 TO 8
		lcSQL=''
		DO CASE 
			CASE lcTemp = 1	
				lcDescrConta = UPPER('cl_snc_cc')
				lcValConta = uf_gerais_getParameter('ADM0000000242', 'TEXT') 
			CASE lcTemp = 2
				lcDescrConta = UPPER('CL_snc_letras')
				lcValConta = uf_gerais_getParameter('ADM0000000247', 'TEXT') 
			CASE lctemp  = 3
				lcDescrConta = UPPER('CL_snc_adiantprecoincerto')
				lcValConta = uf_gerais_getParameter('ADM0000000243', 'TEXT') 
			CASE lctemp  = 4
				lcDescrConta = UPPER('cl_snc_letrasdescontadas')
				lcValConta = uf_gerais_getParameter('ADM0000000248', 'TEXT') 
			CASE lctemp  = 5
				lcDescrConta = UPPER('cl_snc_factoring')
				lcValConta = uf_gerais_getParameter('ADM0000000246', 'TEXT') 
			CASE lctemp  = 6
				lcDescrConta = UPPER('cl_snc_dividastituladas')
				lcValConta = uf_gerais_getParameter('ADM0000000245', 'TEXT') 
			CASE lctemp  = 7
				lcDescrConta = UPPER('cl_snc_descontoletras')
				lcValConta = uf_gerais_getParameter('ADM0000000248', 'TEXT') 				
			CASE lctemp  = 8
				lcDescrConta = UPPER('cl_snc_adiantprecocerto')
				lcValConta = uf_gerais_getParameter('ADM0000000244', 'TEXT') 				
			OTHERWISE
				***	
		ENDCASE 

	* NOTAS :
	*********************************************************************************************************
	* Se substituirem o valor na B_PARAMETERS para um numero fixo (sem N's) esse numero ser� aplicado directamente *
	* Se o valor na para1 tiver N's, ir� ser gerado um numero din�mico com base no n� de N's                *
	*********************************************************************************************************		
		
		Local a, b, snc, NCHAR,nCLNO
		STORE 0 TO nchar,nCLno
		
		&& CALCULA O NUMERO DE N'S
		nchar = len(lcValConta)-len(Strtran(lcValConta,'N','')) 

		&& CALCULA O NUMERO DE DIGITOS DO N� DE Cliente
		IF !UPPER(ALLTRIM(lcPainel)) == "UCRSTEMPINCL"
			IF !tcBool
				IF USED("CL")
					Select CL
					nCLNO = Len(ALLTRIM(astr(CL.no)))
				ELSE
					RETURN
				ENDIF
			ELSE
				IF USED("uCrsCL")
					Select uCrsCL
					nCLNO = Len(ALLTRIM(astr(uCrsCL.no)))
				ELSE
					RETURN
				ENDIF
			ENDIF
		ELSE
			Select UCRSTEMPINCL
			nCLNO = Len(ALLTRIM(astr(UCRSTEMPINCL.no)))
		ENDIF		
						
		&& PREPARA DADOS PARA SUBSTITUIR CARACTERES PELO NUMERO DO Cliente
		a=''
		For i = 1 To nchar
			a = a + astr('N')
		ENDFOR
		
		b=''
		For i = 1 To (nchar - nCLNo)
			b = b + astr('0')
		ENDFOR 
		
		IF !UPPER(ALLTRIM(lcPainel)) == "UCRSTEMPINCL"
			IF !tcBool
				b = b + ALLTRIM(astr(CL.no))
			ELSE
				b = b + ALLTRIM(astr(uCrsCL.no))
			ENDIF 
		ELSE
			b = b + ALLTRIM(astr(UCRSTEMPINCL.no))
		ENDIF
		
		&& SUBSTITUI PELO NUMERO FINAL
		snc = ''

		&& VALIDA NUMERO DE CARACTERES DISPONIVEIS
		IF ((nchar - nCLNO )<0) AND (nchar > 0)
			uf_perguntalt_chama("ATEN��O: O N�MERO DE CARACTERES DISPON�VEIS NA CONTA SNC " + lcDescrConta + " � INSUFICIENTE! POR FAVOR CONTACTE O SUPORTE.","OK","",48)
		ELSE	
			DO case
				CASE lctemp	= 1
					lcConta			=	Strtran(lcValConta,'TP','11')
					lcConta 		=	Strtran(lcConta,a,b)
				
				CASE lctemp  = 2
					lcContalet		=	Strtran(lcValConta,'TP','1')
					lcContalet		=	Strtran(lcContalet,a,b)
				
				CASE lctemp  = 3
					lcContaainc		=	Strtran(lcValConta,'TP','11')
					lcContaainc		=	Strtran(lcContaainc,a,b)
				
				CASE lctemp  = 4
					lcContaletdes	=	Strtran(lcValConta,'TP','11')
					lcContaletdes	=	Strtran(lcContaletdes,a,b)
				
				CASE lctemp  = 5
					lcContafac		=	Strtran(lcValConta,'TP','11')
					lcContafac		=	Strtran(lcContafac,a,b)
				
				CASE lctemp  = 6
					lcContatit		=	Strtran(lcValConta,'TP','11')
					lcContatit		=	Strtran(lcContatit,a,b)
				
				CASE lctemp  = 7
					lcContaletsac	=	Strtran(lcValConta,'TP','11')
					lcContaletsac	=	Strtran(lcContaletsac,a,b)					
				
				CASE lctemp  = 8
					lcContaacer		=	Strtran(lcValConta,'TP','11')
					lcContaacer		=	Strtran(lcContaacer,a,b)					
				
				OTHERWISE
					***	
			ENDCASE 	
		ENDIF 	
	ENDFOR 	
	
	IF !tcBool
		DO CASE
			CASE UPPER(ALLTRIM(lcPainel)) = "UTENTES"
				utentes.pageframe1.page5.txtconta.value			=	lcConta 
				utentes.pageframe1.page5.txtcontalet.value		=	lcContalet 
				utentes.pageframe1.page5.txtcontaainc.value 	=	lcContaainc 
				utentes.pageframe1.page5.txtContaletdes.value	=	lcContaletdes 
				utentes.pageframe1.page5.txtContafac.value		=	lcContafac 
				utentes.pageframe1.page5.txtcontatit.value		=	lcContatit 
				utentes.pageframe1.page5.txtContaLetSac.value	=	lcContaletsac 
				utentes.pageframe1.page5.txtCONTAACER.value		=	lcContaacer
				
			CASE UPPER(ALLTRIM(lcPainel)) = "CRIARCLATEND"
				SELECT uCrsInCLAtend
				replace;
					uCrsInCLAtend.Conta 		WITH lcConta;
					uCrsInCLAtend.Contalet		WITH lcContalet;
					uCrsInCLAtend.Contaainc 	WITH lcContaainc; 
					uCrsInCLAtend.Contaletdes	WITH lcContaletdes;
					uCrsInCLAtend.Contafac 		WITH lcContafac;
					uCrsInCLAtend.Contatit 		WITH lcContatit;
					uCrsInCLAtend.Contaletsac 	WITH lcContaletsac;
					uCrsInCLAtend.Contaacer 	WITH lcContaacer 

			CASE UPPER(ALLTRIM(lcPainel)) = "PESQUTENTES"
				SELECT uCrsInCLAtend
				replace;
					uCrsInCLAtend.Conta 		WITH lcConta;
					uCrsInCLAtend.Contalet		WITH lcContalet;
					uCrsInCLAtend.Contaainc 	WITH lcContaainc; 
					uCrsInCLAtend.Contaletdes	WITH lcContaletdes;
					uCrsInCLAtend.Contafac 		WITH lcContafac;
					uCrsInCLAtend.Contatit 		WITH lcContatit;
					uCrsInCLAtend.Contaletsac 	WITH lcContaletsac;
					uCrsInCLAtend.Contaacer 	WITH lcContaacer 
					
			CASE UPPER(ALLTRIM(lcPainel)) = "UCRSTEMPINCL"
				SELECT uCrsTempInCl
				replace;
					uCrsTempInCl.Conta 			WITH lcConta;
					uCrsTempInCl.Contalet		WITH lcContalet;
					uCrsTempInCl.Contaainc 		WITH lcContaainc; 
					uCrsTempInCl.Contaletdes	WITH lcContaletdes;
					uCrsTempInCl.Contafac 		WITH lcContafac;
					uCrsTempInCl.Contatit 		WITH lcContatit;
					uCrsTempInCl.Contaletsac 	WITH lcContaletsac;
					uCrsTempInCl.Contaacer 		WITH lcContaacer 		

		ENDCASE
	ELSE
		lcSQL=''
		TEXT TO lcSQL TEXTMERGE NOSHOW
			UPDATE
				b_utentes
			set
				conta 			= '<<ALLTRIM(aSTR(lcConta))>>'
				,contafac 		= '<<ALLTRIM(aSTR(lcContafac))>>'
				,contatit 		= '<<ALLTRIM(aSTR(lcContatit))>>'
				,contalet 		= '<<ALLTRIM(aSTR(lcContalet))>>'
				,contaacer 		= '<<ALLTRIM(aSTR(lcContaacer))>>'
				,contaainc 		= '<<ALLTRIM(aSTR(lcContaainc))>>'
				,contaletdes 	= '<<ALLTRIM(aSTR(lcContaletdes))>>'
				,contaletsac 	= '<<ALLTRIM(aSTR(lcContaletsac))>>'
			where
				utstamp		= '<<uCrsCL.utstamp>>'
		ENDTEXT
		
		IF !uf_gerais_actgrelha("", "", lcSQL)
			uf_perguntalt_chama("OCURREU UM ERRO AO ACTUALIZAR AS CONTAS DO CLIENTE.","OK","",48)
		ENDIF 
	ENDIF 
ENDFUNC 


** VALORES POR DEFEITO DOS CLIENTES
FUNCTION uf_utentes_ValoresDefeito
	LPARAMETERS lcCursor, lcPainel
	
	LOCAL lcStamp
	lcStamp = uf_gerais_stamp()
	
	SELECT &lcCursor
	GO TOP
	
	&& Stamp	
	SELECT &lcCursor
	replace utstamp	WITH lcStamp
		
	&& No de Cliente
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_utentes_numero
	ENDTEXT 
	uf_gerais_actgrelha("", "uCrsTmp", lcSQL)
	SELECT uCrsTmp
	SELECT &lcCursor
	replace &lcCursor..No	WITH uCrsTmp.no
	fecha("uCrsTmp")
	
	&& Estab de Cliente
	SELECT &lcCursor
	replace estab	WITH	0
	
	&& SEXO
	SELECT &lcCursor
	replace	sexo WITH 'M'
	
	&& Data de Nascimento
	SELECT &lcCursor
	replace	nascimento WITH CTOD('1900.01.01')
		
	&&Entidade de Factura��o
	select &lcCursor
	replace entfact with .T.
	
	&& Tipo de Cliente
	SELECT &lcCursor
	replace tipo			WITH	'Normal'
	
	&&Pais contabilidade
	select &lcCursor
	replace pais with 1
		
	&& Cart�o de Cliente
	LOCAL lcNrCartaoCl
	lcNrCartaoCl = uf_cartaocliente_geraNumero()		
	If uf_gerais_getParameter("ADM0000000021","BOOL") AND (&lcCursor..estab=0)
		SELECT &lcCursor
		replace nrcartao WITH lcNrCartaoCl 
	ENDIF
				
	&& Calcular N� Conta SNC
	uf_utentes_contaSNC(.f., "UTENTES")
		
	&& Moeda - 
	LOCAL lcMoeda
	STORE 'EURO' TO lcMoeda
	
	lcMoeda = uf_gerais_getParameter("ADM0000000260","text")	
	IF EMPTY(lcMoeda)
		lcMoeda = 'EURO'
	ENDIF
	
	SELECT &lcCursor
	replace moeda	with UPPER(ALLTRIM(lcMoeda))
		
	&& Radical
	SELECT &lcCursor
	replace RadicalTipoEmp	with	1
		
	&& Vencimento
	SELECT &lcCursor
	replace vencimento		with	uf_gerais_getParameter('ADM0000000298', 'NUM')	
	
	IF UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'	
		&& Nacionalidade
		SELECT &lcCursor
		replace descp			with	'PORTUGAL'
		replace codigop			WITH	'PT'
		
		&& Naturalidade
		SELECT &lcCursor
		replace descpNat		with	'PORTUGAL'
		replace codigopNat		WITH	'PT'
	ELSE
		SELECT &lcCursor
		replace descp			with	UPPER(ALLTRIM(ucrse1.pais))
		replace descpNat		with	UPPER(ALLTRIM(ucrse1.pais))
		TEXT TO lcSql noshow textmerge
			select code from B_cli_tabela_pais (nolock) where nomePais='<<UPPER(ALLTRIM(ucrse1.pais))>>'
		ENDTEXT
		uf_gerais_actgrelha("", "uCrsCodP", lcSql)
		LOCAL lcCodP
		lcCodP = ALLTRIM(uCrsCodP.code)
		SELECT &lcCursor
		replace codigop			WITH	lcCodP 
		replace codigopNat		WITH	lcCodP 
		fecha("uCrsCodP")
	ENDIF 
		
	&& CheckBoxes do painel
	SELECT &lcCursor
	replace inactivo		WITH .f.
	replace	naoencomenda	WITH .f.	
	replace	autorizado		WITH .f.	
	replace	nocredit		WITH .f.
	replace	cobnao			WITH .f.
	replace	particular		WITH .f.
	replace	autofact		WITH .f.
	replace	clivd			WITH .f.
	replace	clinica			WITH .f.
	replace pacgen 			WITH .f.
	replace notif 			WITH .t.
		
	&& Datas
	SELECT &lcCursor
	replace VALIPLA		WITH date(1900,1,1)
	replace VALIPLA2	WITH date(1900,1,1)
		
	&& Dados de Cria��o
	SELECT &lcCursor
	replace ousrdata	WITH date()
	replace ousrinis	WITH m_chinis
	
	&&Entidade Financeira Responsavel
	IF !(UPPER(ALLTRIM(LEFT(uf_gerais_getParameter("ADM0000000157","TEXT"),1))) == "F")
		SELECT &lcCursor
		replace codpla WITH "999998"
		replace despla WITH "Sem Comparticipa��o pelo SNS"
	ENDIF
	
	
	
	** Nr externo de cliente
	If uf_gerais_getParameter("ADM0000000273","BOOL")
		LOCAL lcClNrExt
		lcClNrExt = "0"
		
		IF !EMPTY(ALLTRIM(uf_gerais_getParameter("ADM0000000273","TEXT")))
			lcClNrExt = ALLTRIM(uf_gerais_getParameter("ADM0000000273","TEXT"))	
		ELSE
		
			TEXT TO lcSql noshow textmerge
				SELECT no_ext = isnull(convert(int,MAX(no_ext)),0) + 1	FROM b_utentes (nolock)
			ENDTEXT
			IF uf_gerais_actgrelha("", "uCrsClNrExtAux", lcSql)
				IF RECCOUNT("uCrsClNrExtAux")>0
					lcClNrExt = ALLTRIM(STR(uCrsClNrExtAux.no_ext))
				ELSE
					lcClNrExt = "1"
				ENDIF
				fecha("uCrsClNrExtAux")
			ENDIF
		ENDIF
		
		SELECT &lcCursor
		replace no_ext WITH lcClNrExt
	ENDIF
	
	** atualiza ecras
	DO CASE
		CASE UPPER(ALLTRIM(lcPainel)) = "UTENTES"
			utentes.refresh	
			
		CASE UPPER(ALLTRIM(lcPainel)) = "CRIARCLATEND"
			criarclatend.refresh
	ENDCASE
ENDFUNC


** IMPRIME DECLARA��O DE AUTORIZA��O - TALAO A6
FUNCTION uf_utentes_impDeclaracaoAutorizacao	
	LOCAL lcNo, lcEstab, lcSql
	STORE 0 TO lcNo, lcEstab
	lcSql = ""
	
	IF USED("uCrsAtendCl")
		SELECT uCrsAtendCl
		GO TOP
		lcNo	= uCrsAtendCl.no
		lcEstab	= uCrsAtendCl.estab
	ELSE
		IF used("cl")
			SELECT cl
			GO TOP
			
			TEXT TO lcSql noshow textmerge
				Exec up_touch_dadosCliente <<cl.no>>, <<cl.estab>>
			ENDTEXT
			uf_gerais_actgrelha("", "uCrsAtendCl", lcSql)

			SELECT uCrsAtendCl
			lcNo	= uCrsAtendCl.no
			lcEstab	= uCrsAtendCl.estab
		ENDIF
	ENDIF

	IF !(lcNo <= 200) && Adaptar ao cursor da FT para incluir no atendimento
		LOCAL lcSql, lcDefaultPrinter
		STORE "" TO lcSql, lcDefaultPrinter
		
		lcSQL=''
		TEXT To lcSQL Textmerge NOSHOW
			Select 
				COUNT(ftstamp) as contador
			From
				ft (nolock)
			where
				no=<<lcNo>> and estab=<<lcEstab>>
		ENDTEXT
		IF uf_gerais_actgrelha("", "uCrsClTopVd", lcSql)
			select uCrsClTopVd
			IF uCrsClTopVd.contador == 1
				** preparar impressora **
				lcDefaultPrinter = Set("printer",2) && save default printer
				
				Set Printer To Name ''+alltrim(myPosPrinter)+''
				Set Device To print
				Set Console off
				***********************************
	
				select * FROM uCrsAtendCl INTO CURSOR uCrsCl READWRITE
				select uCrsCl
				GO TOP
				REPORT FORM Alltrim(myPath) + '\analises\declaracao.frx' To Printer
				
				** por impressora no default **
				Set Device To screen
				Set printer to Name ''+lcDefaultPrinter+''
				Set Console On
				**************************
				
				uf_perguntalt_chama("ATEN��O: IMPRIMIU A DECLARA��O DE AUTORIZA��O DE CLIENTE. POR FAVOR VERIFIQUE A IMPRESSORA.","OK","",64)
			ENDIF
			
			fecha("uCrsClTopVd")
		ELSE
			uf_perguntalt_chama("OCORREU UM ERRO A CALCULAR O N�MERO DE VENDAS DO CLIENTE! POR FAVOR, CONTACTE O SUPORTE.","OK","",16)
		ENDIF
	ENDIF
ENDFUNC


** ABERTURA DO PAINEL DE RE-IMP BULK
FUNCTION uf_utentes_chamaResumos
	uf_resumos_chama()
ENDFUNC


**
FUNCTION uf_utentes_navegaUltRegisto
	LOCAL lcClStamp
	lcClStamp = uf_gerais_devolveUltRegisto('b_utentes')
							
	IF uf_gerais_actgrelha("", "ucrsUltRegClTemp", [select top 1 no, estab from b_utentes (nolock) where utstamp=']+ALLTRIM(lcClStamp)+['])
		IF RECCOUNT("ucrsUltRegClTemp") > 0
			uf_utentes_chama(ucrsUltRegClTemp.no, ucrsUltRegClTemp.estab)
			fecha("ucrsUltRegClTemp")
		ELSE
			IF uf_gerais_actgrelha("", "ucrsUltRegClTemp", "select top 1 no, estab from b_utentes (nolock) order by ousrdata desc, ousrhora desc")
				IF RECCOUNT("ucrsUltRegClTemp") > 0
					uf_utentes_chama(ucrsUltRegClTemp.no, ucrsUltRegClTemp.estab)
					fecha("ucrsUltRegClTemp")
				ELSE
					uf_utentes_chama()
				ENDIF 
			ELSE
				uf_utentes_chama()
			ENDIF
		ENDIF
	ELSE
		uf_utentes_chama()
	ENDIF
ENDFUNC



**
FUNCTION uf_utentes_chamaDocumentos
	SELECT cl
	IF !EMPTY(cl.no)
		uf_LISTADOCUMENTOS_chama(cl.no, cl.estab, cl.nome, 'CL', '')
	ENDIF
ENDFUNC



**
FUNCTION uf_utentes_chamaCartaoCliente
	SELECT cl
	IF !EMPTY(cl.no)
		uf_cartaocliente_chama(cl.no,cl.estab)
	ELSE
		uf_cartaocliente_chama('','')
	ENDIF
ENDFUNC 


**
FUNCTION uf_utentes_AlternaTabs
	LPARAMETERS lcOpcao
	
	DO CASE 
		CASE lcOpcao == 1
			utentes.ctnTabs1.visible = .t.
			utentes.ctnTabs2.visible = .f.
			utentes.ctnTabs3.visible = .f.
			utentes.ctnTabs1.lbl1.click()
		CASE lcOpcao == 2
			utentes.ctnTabs1.visible = .f.
			utentes.ctnTabs2.visible = .t.
			utentes.ctnTabs3.visible = .f.
			utentes.ctnTabs2.lbl6.click()
		CASE lcOpcao == 3
			utentes.ctnTabs1.visible = .f.
			utentes.ctnTabs3.visible = .t.
			utentes.ctnTabs2.visible = .f.
			utentes.ctnTabs3.lbl7.click()	
	ENDCASE 
ENDFUNC 


**
FUNCTION uf_utentes_actDadosHistTrat
	IF utentes.pageframe1.activepage != 6 
		IF USED("uCrsHisTrat")
			SELECT uCrsHisTrat
			DELETE ALL
		ENDIF
		
		RETURN .f.
	ELSE
		utentes.pageframe1.page6.activate
	ENDIF 
	
	SELECT CL
	IF EMPTY(ALLTRIM(cl.utstamp))
		RETURN .f.
	ENDIF 
	
	IF myClIntroducao OR myClAlteracao
		IF RECCOUNT("uCrsHisTrat")!= 0 AND !uf_perguntalt_chama("Se efectuou altera��es ao hist�rico ir� perd�-las. Dever� gravar as altera��es antes de actualizar os dados." + CHR(13) + "Pretende continuar?","Sim","N�o")
			RETURN .f.
		ENDIF 
	ENDIF 
	
	LOCAL lcDe, lcA
	
	lcDe = ctod(uf_gerais_getDate(utentes.pageframe1.page6.txtDe.value,"DATA"))
	lcA = ctod(uf_gerais_getDate(utentes.pageframe1.page6.txtA.value,"DATA"))
	IF lcA-lcDe > 30
		IF !uf_perguntalt_chama("O per�odo de datas seleccionado � superior a 30 dias." + CHR(13) + "A pesquisa poder� ser demorada." + CHR(13) + "Tem a certeza que pretende continuar?", "Sim", "N�o")
			RETURN .f.
		ENDIF 
	ENDIF 
	
	TEXT TO lcSql NOSHOW textmerge
		exec up_clientes_historicoTratamento <<CL.no>>, <<CL.estab>>, '<<CL.utstamp>>','<<uf_gerais_getDate(utentes.pageframe1.page6.txtDe.value,"SQL")>>', '<<uf_gerais_getDate(utentes.pageframe1.page6.txtA.value,"SQL")>>', '<<mySite>>'
	ENDTEXT 
	IF !uf_gerais_actgrelha("utentes.pageframe1.page6.grdHist", "uCrsHisTrat", lcSql)
		RETURN .f.
	ENDIF
ENDFUNC 


**
FUNCTION uf_utentes_imprimirHistTrat
	IF RECCOUNT("uCrsHisTrat") > 0
	
		**Trata parametros
		SELECT CL

		** Construir string com parametros
		lcSql = "&no=" 			+ str(CL.NO) + ;
				"&estab=" 		+ STR(CL.Estab) + ;
				"&stamp=" 		+ ALLTRIM(CL.utstamp) + ;
				"&de="			+ uf_gerais_getDate(utentes.pageframe1.page6.txtDe.value, 'DATA') + ;
				"&a="			+ uf_gerais_getDate(utentes.pageframe1.page6.txtA.value, 'DATA') + ;
				"&site="		+ mySite + ;
				"&ordem="		+ "0"
				
			
		** chamar report pelo nome dele
		uf_gerais_chamaReport("relatorio_clientes_HistoricoTratamento", lcSql)
	ENDIF
ENDFUNC 


**
FUNCTION uf_utentes_actualizaObsHist
	LPARAMETERS lcObs
	
	LOCAL lcSQL
	lcSQL = ''
	
	SELECT uCrsHisTrat
	IF uCrsHisTrat.tipo == "I"
		TEXT TO lcSQL TEXTMERGE NOSHOW
			UPDATE B_histConsumo
			SET 
				obs = '<<lcObs>>'
			where 
				clstamp = '<<uCrsHisTrat.stamp>>' 
				and data = '<<uf_gerais_getdate(uCrsHisTrat.datadoc,"SQL")>>' 
				and usrhora = '<<uCrsHisTrat.usrhora>>'
				and design = '<<uCrsHisTrat.design>>'
		ENDTEXT
	ELSE
		TEXT TO lcSQL TEXTMERGE NOSHOW
			UPDATE ft2
			SET 
				obsdoc = '<<lcObs>>'
			where 
				ft2stamp = '<<uCrsHisTrat.stamp>>'
		ENDTEXT
	ENDIF 
	
	IF !uf_gerais_actGrelha("","",lcSQL)
		uf_perguntalt_chama("Ocorreu um erro a actualizar as observa��es.","OK","",16)
	ENDIF
ENDFUNC 


**
Function uf_utentesGeraNumInternoCL
	Local lcSQL,lcNum
	Store '' To lcSQL
	
	Select cl
	
	***************
	*	Valida��es	*
	***************
		&& Distrito
			If Empty(Alltrim(cl.u_distrito))
				Messagebox("TEM DE PREENCHER O DISTRITO PRIMEIRO.",48,"LOGITOOLS SOFTWARE")
				Return .f.	
			Endif
			
		&& Tipo	
			If Empty(Alltrim(cl.tipo))
				Messagebox("TEM DE PREENCHER O TIPO PRIMEIRO.",48,"LOGITOOLS SOFTWARE")
				Return .f.	
			Endif
			
			If (!Upper(ALLTRIM(cl.tipo))=="FARM�CIA") AND (!Upper(ALLTRIM(cl.tipo))=="PARAFARM�CIA") AND (!Upper(ALLTRIM(cl.tipo))=="CL�NICA") And !(Upper(Alltrim(cl.tipo))=="NORMAL") And !(Upper(Alltrim(cl.tipo))=="CONTABILIDADE")
				Messagebox("N�O PODE GERAR NUMERO INTERNO PARA ESTE TIPO DE CLIENTE.",48,"LOGITOOLS SOFTWARE")
				Return .F.
			Endif
		
	*****************************
	*	GERA NUMERO INTERNO	*
	*****************************		
		Text To lcSQL Textmerge noshow
			select top 1 ref 
			from b_distritos (nolock)
			where nome='<<Alltrim(cl.u_distrito)>>'
		Endtext 
	
		If !u_sqlexec(lcSQL,"uCrsTmpDistrito")
			Messagebox("OCORREU UM ERRO A GERAR O N�MERO INTERNO.",16,"LOGITOOLS SOFTWARE")
			Return .f.
		Else			
			Select cl 
			lcNum = astr(CL.NO)
			
			Do While Len(lcNum)<3
				lcNum = '0' + Alltrim(lcNum)
			enddo
			
			Select CL
			
			If  !(Upper(Alltrim(cl.tipo))=="CONTABILIDADE")
				
				
				**REPLACE	U_NO	WITH	Left(cl.tipo,1) + Alltrim(uCrsTmpDistrito.REF) +  lcNum
				_cliptext = Left(cl.tipo,1) + Alltrim(uCrsTmpDistrito.REF) +  lcNum
				Messagebox(Left(cl.tipo,1) + Alltrim(uCrsTmpDistrito.REF) +  lcNum)
			Else
				**REPLACE	U_NO	WITH	'O' + Alltrim(uCrsTmpDistrito.REF) +  lcNum
				
				_cliptext = 'O' + Alltrim(uCrsTmpDistrito.REF) +  lcNum
				Messagebox('O' + Alltrim(uCrsTmpDistrito.REF) +  lcNum)
			Endif
			
		Endif
		
		If Used("uCrsTmpDistrito")
			Fecha("uCrsTmpDistrito")
		Endif
Endfunc 


**
FUNCTION uf_utentes_CorrigeCC
	LOCAL lcSQL
	
	IF !uf_perguntalt_chama("Pretende for�ar a atualiza��o de Contas Correntes de Clientes? Deve executar backup da Base de Dados antes.","Sim","N�o",32)
		RETURN .f.
	ENDIF
	&& Corrigir CC
	TEXT TO lcSQL NOSHOW TEXTMERGE	
		UPDATE CC
		set 
			edebf = 
				isnull((
					select SUM((case when rl.erec>0 then rl.erec-rl.earred else 0 end))
					From 
						rl	
					where  
						cc.ccstamp = rl.ccstamp 
					),0)
			,ecredf = isnull((
				select SUM((case when rl.erec<0 then rl.erec-rl.earred else 0 end ))
				From 
					rl	
				where  
					cc.ccstamp = rl.ccstamp 
				),0)*-1
		From
			cc
			
		update
			cc	
		set
			edebf = edeb
			,ecredf = ecred
		from 
			cc
		where
			cmdesc in ('Desc.Financ.','N/Recibo')
			
	ENDTEXT 
	IF !(uf_gerais_actGrelha("","",lcSQL))
		uf_perguntalt_chama("OCORREU UM ERRO A ATUALIZAR CONTAS CORRENTES!","OK","",16)
		RETURN .f.
	ELSE
		uf_perguntalt_chama("Contas Correntes atualizadas com sucesso!","OK","",64)
	ENDIF
	
	&& Actualizar B_utentes
	TEXT TO lcSQL NOSHOW TEXTMERGE	
		DECLARE CLCURSOR Cursor
		for
		select distinct 
			no
			,estab 
		from b_utentes (nolock)

		open CLCURSOR

		declare @no numeric(10), @estab numeric(3)

		fetch next from CLCURSOR INTO @no, @estab
		while (@@FETCH_STATUS <> -1)
		begin
			update b_utentes
			set esaldo = (
				select 
					total = ISNULL(sum(edeb - edebf) - sum(ecred - ecredf),0)
				from cc (nolock)
				left join ft (nolock) on cc.ftstamp=ft.ftstamp
				where 
					ft.no = @no
					and ft.estab = @estab
					and ft.anulado is not null
				)
			where 
				b_utentes.no = @no
				and b_utentes.estab = @estab
		FETCH NEXT FROM CLCURSOR INTO @no, @estab
		END
		CLOSE CLCURSOR
		DEALLOCATE CLCURSOR	
	ENDTEXT 
	IF !(uf_gerais_actGrelha("","",lcSQL))
		uf_perguntalt_chama("OCORREU UM ERRO A ATUALIZAR CONTAS CORRENTES!","OK","",16)
		RETURN .f.
	ELSE
		uf_perguntalt_chama("Contas Correntes atualizadas com sucesso!","OK","",64)
	ENDIF
		 
ENDFUNC


**
FUNCTION uf_utentes_expandeRegistoClinico
	WITH UTENTES.Pageframe1.page10
		IF .Grid2.Height + 100 >  .Shape18.Height
			.Grid2.Height = .Grid2.Height - 200
			.ContainerOpcoes.CHECK1.config(myPath + "\imagens\icons\down_b.png", "")
			UTENTES.registoClinico = .t.
			.ContainerSoap.visible =  .t.
		ELSE
			.Grid2.Height = .Grid2.Height + 200
			.ContainerOpcoes.CHECK1.config(myPath + "\imagens\icons\up_b.png", "")
			UTENTES.registoClinico = .f.
			.ContainerSoap.visible =  .f.
		ENDIF
		.ContainerOpcoes.top = .Grid2.top + .Grid2.height + 5

	ENDWITH
ENDFUNC 


**
FUNCTION uf_utentes_actRegistoClinico
	&& Cursor de EFRs
	lcSQL = ''	
	TEXT TO lcSql NOSHOW textmerge
		exec up_utentes_registoClinico '<<ALLTRIM(cl.utstamp)>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("utentes.pageframe1.page10.grid2","uCrsRegistoClinico",lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar Registos Clinicos associadas ao utentes.","OK","",16)
		RETURN .f.
	ENDIF

ENDFUNC


**
FUNCTION uf_utentes_actBioRegistos
	** BIOREGISTOS
	lcSQL = ''	
	TEXT TO lcSql NOSHOW textmerge
		exec up_utentes_BioRegistos '<<ALLTRIM(cl.utstamp)>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("Utentes.Pageframe1.page9.Grid1","ucrsBioRegistos",lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar lista de Bioregistos associados ao utente.","OK","",16)
		RETURN .f.
	ENDIF
ENDFUNC


** 
FUNCTION uf_utentes_actEntidadesUtentes 

	lcSQL = ''	
	TEXT TO lcSql NOSHOW textmerge
		exec up_utentes_Entidades '<<ALLTRIM(cl.utstamp)>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("UTENTES.Pageframe1.Page4.grdEntidades","ucrsEntidadesUtentes",lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar Entidades de Factura��o associadas ao utente.","OK","",16)
		RETURN .f.
	ENDIF

ENDFUNC 

** 
FUNCTION uf_utentes_actServicosFact

	lcSQL = ''	
	TEXT TO lcSql NOSHOW textmerge
		exec up_utentes_ServicosFaturacao '<<ALLTRIM(cl.utstamp)>>', <<mysite_nr>>
	ENDTEXT
	IF !uf_gerais_actGrelha("UTENTES.Pageframe1.Page4.GridServFat","ucrsServicosFaturacao",lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar Servi�os de Factura��o associadas ao utente.","OK","",16)
		RETURN .f.
	ENDIF
	
	SELECT ucrsServicosFaturacao
	GO TOP 
	CALCULATE SUM(ucrsServicosFaturacao.preco * ucrsServicosFaturacao.qtt - (ucrsServicosFaturacao.desconto/100 * (ucrsServicosFaturacao.preco*qtt)) - ucrsServicosFaturacao.desconto_valor) TO lcTotalPagar
	
	utentes.pageframe1.page4.txtValorFaturar.value = lcTotalPagar
	utentes.pageframe1.page4.txtValorFaturar.refresh

ENDFUNC


**
FUNCTION uf_utentes_GravaRegistoClinico
	Lparameters lcUtstamp 
	LOCAL lcStamp, lcSQL
	
	IF EMPTY(lcUtstamp)
		RETURN .t.
	ENDIF
	
	** Limpa possiveis ordena��es
	IF USED("uCrsRegistoClinico")
	
		select ucrsRegistoClinico 
		DELETE TAG ALL 	
			
		lcSQL = ""
		TEXT TO lcSQL NOSHOW textmerge
			DELETE FROM registoClinico 	Where utstamp = '<<ALLTRIM(lcUtstamp)>>'
		ENDTEXT
		IF !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR O REGISTO CLINICO DO UTENTE.","OK","",16)
			RETURN .f.
		ENDIF
		
	
		Select uCrsRegistoClinico
		GO TOP
		SCAN
			IF LEFT(ALLTRIM(uCrsRegistoClinico.s),10)!="Prescri��o"
				lcStamp = uf_gerais_stamp()
				lcSQL = ""
				
				LOCAL opUsrInis,  opOusrInis
				
				** valida se deve alterar ou manter as iniciais 				
				IF(EMPTY(ALLTRIM(uCrsRegistoClinico.ousrinis)))
					opOusrInis=ALLTRIM(m_chinis)
				ELSE
					opOusrInis = ALLTRIM(uCrsRegistoClinico.ousrinis)
				ENDIF
				
				IF(EMPTY(ALLTRIM(uCrsRegistoClinico.usrinis)))
					opUsrInis=ALLTRIM(m_chinis)
				ELSE
					opUsrInis= ALLTRIM(uCrsRegistoClinico.ousrinis)
				ENDIF
		
				
				TEXT TO lcSQL NOSHOW textmerge
					INSERT INTO registoClinico (
						regstamp
						,utstamp
						,data
						,hora
						,ousrinis
						,ousrdata
						,ousrhora
						,usrinis
						,usrdata
						,usrhora
						,s
						,o
						,a
						,p
						,tipo
					)values (
						'<<ALLTRIM(lcStamp)>>'
						,'<<ALLTRIM(lcUtstamp)>>'
						,'<<uf_gerais_getdate(uCrsRegistoClinico.data,"SQL")>>'
						,'<<uCrsRegistoClinico.hora>>'
						,'<<opOusrInis>>'
						,convert(nvarchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
						,convert(nvarchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
						,'<<opUsrInis>>'
						,convert(nvarchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
						,convert(nvarchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
						,'<<uCrsRegistoClinico.s>>'
						,'<<uCrsRegistoClinico.o>>'
						,'<<uCrsRegistoClinico.a>>'
						,'<<uCrsRegistoClinico.p>>'
						,'<<uCrsRegistoClinico.tipo>>'
					)
				ENDTEXT

				IF !uf_gerais_actGrelha("","",lcSQL)
					uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR O REGISTO CLINICO DO UTENTE.","OK","",16)
					RETURN .f.
				ENDIF
			ENDIF 
		ENDSCAN
	ENDIF
	
	RETURN .t.
ENDFUNC


**
FUNCTION uf_utentes_RegistoClinicoNovo
	LPARAMETERS lcS
	
	
	select uCrsRegistoClinico
	lcSanterior = uCrsRegistoClinico.s
	
	
	SET HOURS TO 24
	Atime=ttoc(datetime()+(difhoraria*3600),2)
	Astr=left(Atime,8)
	select uCrsRegistoClinico
	GO bottom
	APPEND BLANK
	REplace uCrsRegistoClinico.data WITH DATE()
	REplace uCrsRegistoClinico.hora WITH Astr
	**REplace uCrsRegistoClinico.hora WITH TIME()
	replace uCrsRegistoClinico.ousrinis with ALLTRIM(m_chinis)
	
	IF !EMPTY(lcS)
		REplace uCrsRegistoClinico.s WITH lcSanterior
	ENDIF
	
	
	UTENTES.Pageframe1.page10.grid2.setfocus
	UTENTES.Pageframe1.page10.grid2.click
	UTENTES.Pageframe1.page10.grid2.refresh
	
	utentes.Pageframe1.Page10.containerSoap.Refresh
	
	IF UTENTES.registoClinico == .f.
		uf_utentes_expandeRegistoClinico()
	ENDIF
ENDFUNC


**
FUNCTION uf_utentes_RegistoClinicoApaga
	LOCAL lcRegPos 
	
	lcRegPos = RECNO("uCrsRegistoClinico")

	SELECT uCrsRegistoClinico
	DELETE

	SELECT uCrsRegistoClinico
	TRY
		SKIP -1
	CATCH 
		SKIP 1
	ENDTRY
	
	UTENTES.Pageframe1.page10.grid2.setfocus
	utentes.Pageframe1.Page10.containerSoap.Refresh
ENDFUNC


**
FUNCTION uf_utentes_RegistoClinicoImprime

ENDFUNC


**
FUNCTION uf_utentes_adicionaEntidade
	uf_PESQUTENTES_Chama("ENTIDADE")
ENDFUNC


**
FUNCTION uf_utentes_RemoveEntidade
	LOCAL lcRegPos 
	
	lcRegPos = RECNO("ucrsEntidadesUtentes")


	SELECT ucrsEntidadesUtentes
	TRY
		SELECT ucrsEntidadesUtentes
		DELETE
		
		SKIP -1
	CATCH 
		SKIP 1
	ENDTRY
	
	UTENTES.Pageframe1.Page4.grdEntidades.refresh
	
ENDFUNC


**
FUNCTION uf_utentes_adicionaServicoFact

	uf_pesqStocks_Chama("SERVFACTUT")
	
ENDFUNC


**
FUNCTION uf_utentes_removeServicoFact
	LOCAL lcRegPos 
	
	TRY
		SELECT ucrsServicosFaturacao
		DELETE
		
		SKIP -1
	CATCH 
		SKIP 1
	ENDTRY
	
	Utentes.Pageframe1.page4.GridServFat.Refresh
ENDFUNC


**
FUNCTION uf_utentes_AdicionaLinhaComparticipacao
	
	IF myClAlteracao == .t. OR myClIntroducao == .t.
		uf_pesqStocks_Chama("ENTIDADECOMPARTICIPADORA")
	ENDIF
	
ENDFUNC 


**
FUNCTION uf_utentes_BioRegistoNovo
	LOCAL lcBioparametroID,lcId
	
	Select cl
	select ucrsBioparametros
	
	IF EMPTY(ucrsBioparametros.id)
		uf_perguntalt_chama("Selecione o indicador a registar.","OK","",64)
		RETURN .f.
	ENDIF
	
	lcBioparametroID = ucrsBioparametros.id
	lcId = uf_gerais_getId(1)
	
	SET HOURS TO 24
	Atime=ttoc(datetime()+(difhoraria*3600),2)
	Astr=left(Atime,8)
	select ucrsBioRegistos
	GO bottom
	APPEND BLANK
	Replace ucrsBioRegistos.data WITH DATE()
	Replace ucrsBioRegistos.hora WITH Astr
	**Replace ucrsBioRegistos.hora WITH TIME()
	Replace ucrsBioRegistos.id WITH lcId 
	Replace ucrsBioRegistos.utstamp WITH cl.utstamp
	Replace ucrsBioRegistos.id_bioparametros WITH lcBioparametroID
	Replace ucrsBioRegistos.valor WITH 0
	
	UTENTES.Pageframe1.page9.grid1.setfocus
	UTENTES.Pageframe1.page9.grid1.click
	UTENTES.Pageframe1.page9.grid1.refresh
	
ENDFUNC


**
FUNCTION uf_utentes_BioRegistoApaga
	LOCAL lcRegPos 
	
	lcRegPos = RECNO("ucrsBioRegistos")

	SELECT ucrsBioRegistos
	DELETE

	SELECT ucrsBioRegistos
	TRY
		SKIP -1
	CATCH 
		SKIP 1
	ENDTRY
	
	UTENTES.Pageframe1.page9.grid1.setfocus
	
ENDFUNC


**
FUNCTION uf_utentes_GravaBioRegistos
	LOCAL lcStamp, lcSQL
	
	IF !USED("CL") && kd chamado do painel de atendimento
		RETURN .t.
	ENDIF 
	
	IF EMPTY(ALLTRIM(cl.utstamp))
		RETURN .t.
	ENDIF
	
	** Limpa possiveis ordena��es
	IF USED("ucrsBioRegistos")
	
		lcSQL = ""
		TEXT TO lcSQL NOSHOW textmerge
			DELETE FROM bioregistos	Where utstamp = '<<ALLTRIM(cl.utstamp)>>'
		ENDTEXT
		IF !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR OS REGISTOS DE MONITORIZA��O DO UTENTE.","OK","",16)
			RETURN .f.
		ENDIF
		
		Select ucrsBioRegistos
		SET FILTER TO 
		Select ucrsBioRegistos
		GO TOP
		SCAN
			
				lcStamp = uf_gerais_stamp()
				lcSQL = ""
				TEXT TO lcSQL NOSHOW textmerge
					INSERT INTO bioregistos(
						id
						,utstamp
						,id_bioparametros
						,data
						,hora
						,valor
						,obs
						
					)values (
						'<<ALLTRIM(ucrsBioRegistos.id)>>'
						,'<<ALLTRIM(ucrsBioRegistos.utstamp)>>'
						,'<<ALLTRIM(ucrsBioRegistos.id_bioparametros)>>'
						,'<<uf_gerais_getdate(ucrsBioRegistos.data,"SQL")>>'
						,'<<ALLTRIM(ucrsBioRegistos.hora)>>'
						,<<ucrsBioRegistos.valor>>
						,'<<ALLTRIM(ucrsBioRegistos.obs)>>'
					)
				ENDTEXT
				IF !uf_gerais_actGrelha("","",lcSQL)
					uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR OS REGISTOS DE MONITORIZA��O DO UTENTE.","OK","",16)
					RETURN .f.
				ENDIF
			
		ENDSCAN
	ENDIF
	
	RETURN .t.
ENDFUNC


**
FUNCTION uf_utentes_entidadesFaturacao
	LOCAL lcStamp, lcSQL, Lordem 
	
	Lordem = 1
	
	IF !USED("CL") && kd chamado do painel de atendimento
		RETURN .t.
	ENDIF 
	
	IF EMPTY(ALLTRIM(cl.utstamp))
		RETURN .t.
	ENDIF
	
	** Limpa possiveis ordena��es
	IF USED("ucrsEntidadesUtentes")
	
		lcSQL = ""
		TEXT TO lcSQL NOSHOW textmerge
			DELETE FROM B_entidadesUtentes Where B_entidadesUtentes.utstamp = '<<ALLTRIM(cl.utstamp)>>'
		ENDTEXT
		IF !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR OS REGISTOS DE MONITORIZA��O DO UTENTE.","OK","",16)
			RETURN .f.
		ENDIF
		Select cl
		Select ucrsEntidadesUtentes
		SET FILTER TO 
		Select ucrsEntidadesUtentes
		GO TOP
		SCAN
			
				Lordem = Lordem + 1000
				lcStamp = uf_gerais_stamp()
				lcSQL = ""
				TEXT TO lcSQL NOSHOW textmerge
					INSERT INTO B_entidadesUtentes(
						eutstamp
						,eno
						,eestab
						,enome
						,utstamp
						,nome
						,no
						,estab
						,lordem
						,ousrinis
						,ousrdata
						,ousrhora
						,usrinis
						,usrdata
						,usrhora
						,cartao
						,validade
					)values (
						'<<ALLTRIM(ucrsEntidadesUtentes.eutstamp)>>'
						,<<ucrsEntidadesUtentes.eno>>
						,<<ucrsEntidadesUtentes.eestab>>
						,'<<ALLTRIM(ucrsEntidadesUtentes.enome)>>'
						,'<<ALLTRIM(cl.utstamp)>>'
						,'<<ALLTRIM(cl.nome)>>'
						,<<cl.no>>
						,<<cl.estab>>
						,<<Lordem>>
						,'<<m_chinis>>'
						,convert(nvarchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
						,convert(nvarchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
						,'<<m_chinis>>'					
						,convert(nvarchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
						,convert(nvarchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
						,'<<ALLTRIM(ucrsEntidadesUtentes.cartao)>>'
						,'<<uf_gerais_getdate(ucrsEntidadesUtentes.validade,"SQL")>>'
						
					)
				ENDTEXT
				IF !uf_gerais_actGrelha("","",lcSQL)
					uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR ENTIDADES DE FATURACAO DO UTENTE.","OK","",16)
					RETURN .f.
				ENDIF
			
		ENDSCAN
	ENDIF
	
	RETURN .t.
ENDFUNC


** Grava Servi�os para Factura��o 
FUNCTION uf_utentes_ServFaturacao
	LOCAL lcSQL
		
	IF !USED("CL") && qd chamado do painel de atendimento
		RETURN .t.
	ENDIF 
	
	IF EMPTY(ALLTRIM(cl.utstamp))
		RETURN .t.
	ENDIF
	
	lcSql = ""
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		DELETE FROM cl_servicos	WHERE cl_servicos.nr_cl = <<cl.no>>	and cl_servicos.dep_cl = <<cl.estab>> 
	ENDTEXT 

	IF !uf_gerais_actGrelha("", "",lcSQL)
			MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A VERIFICAR DADOS DOS SERVI�OS. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	
	&& Insere Servi�os
	SELECT ucrsServicosFaturacao
	GO TOP 
	SCAN	
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE noshow
			INSERT INTO
				 cl_servicos (nr_cl, dep_cl, ref, fatura, data, desvio_mes_fat, preco, desconto, qtt, desconto_valor)
			VALUES
				 (
				  <<cl.no>>
				 ,<<cl.estab>>
				 ,'<<ALLTRIM(ucrsServicosFaturacao.ref)>>'
				 ,<<IIF(ucrsServicosFaturacao.fatura,1,0)>>
				 ,'<<uf_gerais_getdate(ucrsServicosFaturacao.data,"SQL")>>'
				 ,<<ucrsServicosFaturacao.desvio_mes_fat>>
				 ,'<<ucrsServicosFaturacao.preco>>'
				 ,'<<ucrsServicosFaturacao.desconto>>'
				 ,'<<ucrsServicosFaturacao.qtt>>'
 				 ,'<<ucrsServicosFaturacao.desconto_valor>>'
				 ) 
		ENDTEXT		
	
		IF !uf_gerais_actGrelha("", "",lcSQL)
			MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A VERIFICAR DADOS DOS SERVI�OS. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
		
		SELECT ucrsServicosFaturacao
	ENDSCAN
	
	RETURN .t.
ENDFUNC 


**
FUNCTION uf_utentes_bioparametrosAfterRowColChange
	Select ucrsBioparametros
	Select ucrsBioRegistos
	SET FILTER TO ucrsBioRegistos.id_bioparametros == ucrsBioparametros.id
	Select ucrsBioRegistos
	GO TOP

	TEXT TO lcSQL NOSHOW TEXTMERGE 
		Select * from bioregistos WHERE id_bioparametros = '<<ALLTRIM(ucrsBioparametros.id)>>' AND utstamp = '<<ALLTRIM(cl.utstamp)>>' ORDER BY data, hora
	ENDTEXT 

	If !uf_gerais_actGrelha("", "ucrsBioRegistosGrafico", lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A VERIFICAR OS REGISTOS DE MONITORIZA��O DO UTENTE.","OK","",16)
		RETURN .f.
	ENDIF 

	WITH UTENTES.Pageframe1.page9.ContainerGrafico.chart1
	
		IF RECCOUNT("ucrsBioRegistosGrafico") > 0
				
			.cAlias = "ucrsBioRegistosGrafico"
			.cdata = "valor"
			.cRowLabels = "data"    
			.cLabels  = "Valor"
			.showLegend = .f.
			.cColours = "green"
			.createchart
		ELSE

			Select ucrsBioRegistosGrafico
			APPEND BLANK
			Replace ucrsBioRegistosGrafico.data WITH DATE(1900,1,1)
			Replace ucrsBioRegistosGrafico.hora WITH "00:00:00"
			Replace ucrsBioRegistosGrafico.valor WITH 0
			
			.cAlias = "ucrsBioRegistosGrafico"
			.cdata = "valor"
			.cRowLabels = "data"    
			.cLabels  = "Valor"
			.showLegend = .f.
			.cColours = "green"
			.createchart
		ENDIF
	ENDWITH

	UTENTES.Pageframe1.page9.ContainerGrafico.chart1.refresh
	
	Utentes.Pageframe1.Page9.ContainerGrafico.LabelIndicadores.Caption = "Evolu��o dos Indicadores: " + ALLTRIM(ucrsBioparametros.nome) + " - " + ALLTRIM(ucrsBioparametros.variavel)
	UTENTES.Pageframe1.page9.grid1.refresh
	
ENDFUNC 


*!*	**
*!*	FUNCTION uf_utentes_adicionaDiploma
*!*		
*!*		IF !(uf_gerais_actGrelha("","uCrsTempListaDiplomas",[exec up_utentes_ListaDiplomas]))
*!*			uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR OS DIPLOMAS!","OK","",16)
*!*			RETURN .f.
*!*		ELSE
*!*			uf_valorescombo_chama("uCrsDiplomasID.diploma", 2, "uCrsTempListaDiplomas", 2, "diploma", "patologia,despacho,diploma",.t.,.t.)
*!*		Endif


*!*		UTENTES.Pageframe1.page7.GridDiplomas.refresh
*!*	ENDFUNC 


**
FUNCTION uf_utentes_adicionaDiploma
	IF !(uf_gerais_actGrelha("","uCrsTempListaDiplomas",[exec up_utentes_ListaDiplomas]))
		uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR OS DIPLOMAS!","OK","",16)
		RETURN .f.
	ELSE
		uf_valorescombo_chama("uCrsDiplomasID.patologia", 2, "uCrsTempListaDiplomas", 2, "patologia", "patologia,despacho,diploma",.t.,.f.)
	Endif


*!*		

*!*		update ;
*!*			uCrsDiplomasID;
*!*		set ;
*!*			uCrsDiplomasID.idstamp = cl.utstamp;
*!*			,uCrsDiplomasID.diploma = uCrsTempListaDiplomas.despacho;
*!*			,uCrsDiplomasID.u_design = uCrsTempListaDiplomas.diploma;
*!*			,uCrsDiplomasID.patologia = uCrsTempListaDiplomas.patologia;
*!*		from ;
*!*			uCrsDiplomasID ;
*!*			inner join uCrsTempListaDiplomas on uCrsDiplomasID.patologia = uCrsTempListaDiplomas.patologia 

*!*		UTENTES.Pageframe1.page7.GridDiplomas.refresh
ENDFUNC 


**
FUNCTION uf_utentes_removeDiploma

	TRY
		SELECT uCrsDiplomasID 
		DELETE
		
		SKIP -1
	CATCH 
		SKIP 1
	ENDTRY
	
	UTENTES.Pageframe1.page7.GridDiplomas.refresh
ENDFUNC 



**
FUNCTION uf_utentes_tratamentoimprime
	LOCAL lcNregistosHistTrat
	lcNregistosHistTrat = 0
	
	**
	IF !USED("uCrsHisTrat")
		RETURN .f.
	ENDIF
	select uCrsHisTrat
	COUNT TO lcNregistosHistTrat
	IF lcNregistosHistTrat==0
		uf_perguntalt_chama("N�o existem dados para impress�o.","OK","",64)
		RETURN .f.
	ELSE
		uf_imprimirgerais_Chama("TRATAMENTO")
	ENDIF
ENDFUNC


** 
FUNCTION uf_utentes_tratamentoMarcaAlteracao
	IF USED("uCrsHisTrat")
		select uCrsHisTrat
		Replace uCrsHisTrat.alterado WITH .t.
	ENDIF
ENDFUNC


**
FUNCTION uf_utentes_criaProforma
	
	SELECT cl
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_utentes_dadosClServicos <<cl.no>>,<<cl.estab>>,<<mysite_nr>>
	ENDTEXT 
	IF !uf_gerais_actgrelha("","UcrsDadosClServicos",lcSQL)
		regua(2)
		uf_perguntalt_chama("Ocorreu um erro na introdu��o do documento Factura Proforma. N�o � possivel continuar.","OK","",16)
		RETURN .f.
    ENDIF
    
	LOCAL lnStamp,lnNo,lcEstab,lnNdoc,lnData,lnHora,lnVendedor,lnSuspensa,lcTotal;
			,lnTotalQtt,lnTotalCusto,lnTotalDesconto,lnTotalBase1,lnTotalBase2,lnTotalBase3;
			,lnTotalBase4,lnTotalBase5,lnTotalBase6,lnTotalBase7,lnTotalBase8,lnTotalBase9;
			,lnTotalIva1,lnTotalIva2,lnTotalIva3,lnTotalIva4,lnTotalIva5,lnTotalIva6,lnTotalIva7;
			,lnTotalIva8,lnTotalIva9,lnSite,lcTerminal,lnCxstamp,lnSsstamp,lnObs,lnSaleRef

	LOCAL lnftstamp,lnfistamp,lnref,lnqt,lntotal,lniva,lnivaIncluido;
			,lnarmazem,lnordem,lnpvp,lndescontoP,lndescontoV,lnDesign 

	lnftstamp = uf_gerais_getId(2)
	lnNo = cl.no
	lnEstab = cl.estab
	
	SELECT ucrsGeralTd
	LOCATE FOR UPPER(ALLTRIM(ucrsGeralTD.nmdoc)) == UPPER(ALLTRIM("Factura Proforma"));
		AND UPPER(ALLTRIM(ucrsGeralTD.site)) == UPPER(ALLTRIM(mySite))
	IF !FOUND()
		**
		uf_perguntalt_chama("A Fatura Proforma n�o est� definida para o Local atual.","OK","",16)
		RETURN .f.
	ELSE 
		SELECT ucrsGeralTd
		lnNdoc= ucrsGeralTd.ndoc		
	ENDIF 	

	** Data, Hora e Vendedor
	SET HOURS TO 24
	Atime=ttoc(datetime()+(difhoraria*3600),2)
	Astr=left(Atime,8)
	lnData = DATE()
	lnHora = Astr
	**lnHora = TIME()
	lnVendedor = ch_userno 
	lnSuspensa = .f.

	** Calcular Totais por Documento
	SELECT UcrsDadosClServicos
	CALCULATE SUM(Total) TO lcTotal
	lnTotalQtt = 1 && Calculo de QTT
	lnTotalCusto = 0
	lnTotalDesconto = 0
	
	CALCULATE for UcrsDadosClServicos.tabiva = 1 SUM(UcrsDadosClServicos.totalbase) to lnTotalBase1 
	CALCULATE for UcrsDadosClServicos.tabiva = 2 SUM(UcrsDadosClServicos.totalbase) to lnTotalBase2
	CALCULATE for UcrsDadosClServicos.tabiva = 3 SUM(UcrsDadosClServicos.totalbase) to lnTotalBase3
	CALCULATE for UcrsDadosClServicos.tabiva = 4 SUM(UcrsDadosClServicos.totalbase) to lnTotalBase4
	CALCULATE for UcrsDadosClServicos.tabiva = 5 SUM(UcrsDadosClServicos.totalbase) to lnTotalBase5
	CALCULATE for UcrsDadosClServicos.tabiva = 6 SUM(UcrsDadosClServicos.totalbase) to lnTotalBase6
	CALCULATE for UcrsDadosClServicos.tabiva = 7 SUM(UcrsDadosClServicos.totalbase) to lnTotalBase7
	CALCULATE for UcrsDadosClServicos.tabiva = 8 SUM(UcrsDadosClServicos.totalbase) to lnTotalBase8
	CALCULATE for UcrsDadosClServicos.tabiva = 9 SUM(UcrsDadosClServicos.totalbase) to lnTotalBase9
	
	CALCULATE for UcrsDadosClServicos.tabiva = 1 SUM(UcrsDadosClServicos.totaliva) to lnTotalIva1 
	CALCULATE for UcrsDadosClServicos.tabiva = 2 SUM(UcrsDadosClServicos.totaliva) to lnTotalIva2 
	CALCULATE for UcrsDadosClServicos.tabiva = 3 SUM(UcrsDadosClServicos.totaliva) to lnTotalIva3 
	CALCULATE for UcrsDadosClServicos.tabiva = 4 SUM(UcrsDadosClServicos.totaliva) to lnTotalIva4 
	CALCULATE for UcrsDadosClServicos.tabiva = 5 SUM(UcrsDadosClServicos.totaliva) to lnTotalIva5 
	CALCULATE for UcrsDadosClServicos.tabiva = 6 SUM(UcrsDadosClServicos.totaliva) to lnTotalIva6 
	CALCULATE for UcrsDadosClServicos.tabiva = 7 SUM(UcrsDadosClServicos.totaliva) to lnTotalIva7 
	CALCULATE for UcrsDadosClServicos.tabiva = 8 SUM(UcrsDadosClServicos.totaliva) to lnTotalIva8 
	CALCULATE for UcrsDadosClServicos.tabiva = 9 SUM(UcrsDadosClServicos.totaliva) to lnTotalIva9 

	
	lnSite = mysite
	lcTerminal = mytermno
	lnCxstamp = ''
	lnSsstamp = ''
	lnObs = 'Documento Gerado via Factura��o Autom�tica'
	lnSaleRef = ''
			

	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_sales_createHeader 
			'<<ALLTRIM(lnftstamp)>>'
			,0
			,<<lnNo>>
			,<<lnEstab>>
			,<<lnNdoc>>
			,'<<uf_gerais_getdate(lnData,"SQL")>>'
			,'<<lnHora>>'
			,<<lnVendedor>>
			,0
			,<<lcTotal>>
			,<<lnTotalQtt>>
			,<<lnTotalCusto>>
			,<<lnTotalDesconto>>
			,<<ROUND(lnTotalBase1,2)>> ,<<ROUND(lnTotalBase2,2)>> ,<<ROUND(lnTotalBase3,2)>> ,<<ROUND(lnTotalBase4,2)>> ,<<ROUND(lnTotalBase5,2)>> ,<<ROUND(lnTotalBase6,2)>> ,<<ROUND(lnTotalBase7,2)>> ,<<ROUND(lnTotalBase8,2)>> ,<<ROUND(lnTotalBase9,2)>>
			,<<ROUND(lnTotalIva1,2)>> ,<<ROUND(lnTotalIva2,2)>> ,<<ROUND(lnTotalIva3,2)>> ,<<ROUND(lnTotalIva4,2)>>	,<<ROUND(lnTotalIva5,2)>> ,<<ROUND(lnTotalIva6,2)>> ,<<ROUND(lnTotalIva7,2)>> ,<<ROUND(lnTotalIva8,2)>>	,<<ROUND(lnTotalIva9,2)>>
			,'<<ALLTRIM(lnSite)>>'
			,<<lcTerminal>>
			,'<<ALLTRIM(lnCxstamp)>>' ,'<<ALLTRIM(lnSsstamp)>>'
			,'<<ALLTRIM(lnObs)>>' ,'<<ALLTRIM(lnSaleRef)>>'
	ENDTEXT 

	IF !uf_gerais_actgrelha("","",lcSQL)
		regua(2)
		uf_perguntalt_chama("Ocorreu um erro na introdu��o do documento Factura Proforma. N�o � possivel continuar.","OK","",16)
		RETURN .f.
    ENDIF

	lnordem = 100
    
	Select UcrsDadosClServicos
	GO Top
	SCAN 

		*************************************************
		** LINHAS
		*************************************************
		lnfistamp =uf_gerais_getId(2)
		lnref = UcrsDadosClServicos.ref 
		lnqt = UcrsDadosClServicos.qtt 
		lntotal = UcrsDadosClServicos.total
		lniva = UcrsDadosClServicos.taxa
		lnivaIncluido = UcrsDadosClServicos.ivaincl
		lnarmazem = myarmazem
		lnordem = lnordem +1
		lnpvp = UcrsDadosClServicos.preco
		lndescontoP = UcrsDadosClServicos.desconto
		lndescontoV = 0
		lnDesign = UcrsDadosClServicos.design

		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_sales_createDetail
				'<<ALLTRIM(lnftstamp)>>'
				,'<<ALLTRIM(lnfistamp)>>'
				,'<<ALLTRIM(lnref)>>'
				,'<<LEFT(ALLTRIM(lnDesign),60)>>'
				,<<lnqt>>
				,<<lntotal>>
				,<<lniva>>
				,<<IIF(lnivaIncluido,1,0)>>
				,<<lnarmazem>>
				,<<lnordem>>
				,<<lnpvp>>
				,<<lndescontoP>>
				,<<lndescontoV>>
		ENDTEXT 

		IF !uf_gerais_actgrelha("","",lcSQL)
			regua(2)
			uf_perguntalt_chama("Ocorreu um erro na introdu��o do documento Factura a Proforma. N�o � possivel continuar.","OK","",16)
			RETURN .f.
	 	ENDIF 
	 	
	ENDSCAN 

	uf_perguntalt_chama("Factura Proforma gerada com sucesso.","OK","",64)
	
ENDFUNC 


**
FUNCTION uf_utentes_imprimirCartaoH
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_utentes_cartaoH <<cl.no>>
	ENDTEXT 

	IF !uf_gerais_actgrelha("","ucrsDadosCartaoH",lcSQL)
		uf_perguntalt_chama("Ocorreu um erro na verifica��o de dados do cart�o H. N�o � possivel continuar.","OK","",16)
		RETURN .f.
 	ENDIF 
 	
 	
 	IF RECCOUNT("ucrsDadosCartaoH")== 0
 		uf_perguntalt_chama("O Utente n�o tem nenhuma Entidade de Cart�o H associada.","OK","",16)
		RETURN .f.
 	ENDIF 
 	
 	uf_imprimirgerais_Chama('CARTAO_H')
 	
ENDFUNC  


** Funcao para imprimir Etiqueta com c�digo de barras para colar em cart�o - Lu�s Leal 20150803
FUNCTION uf_UTENTES_imprimirCartao
	LPARAMETERS lcCRM
	LOCAL lcNrCartao
	lcNrCartao = ""
	
	IF EMPTY(lcCRM)
		SELECT cl
		lcNrCartao = cl.nrcartao
	ELSE
		SELECT uCrsFidel
		lcNrCartao = uCrsFidel.nrcartao
	ENDIF 
	
	IF !EMPTY(lcNrCartao)
		IF !uf_perguntalt_chama("Pretende imprimir uma etiqueta com dados do cart�o?","Sim","N�o")
			RETURN .f.
		ENDIF

		Local lcCount, lcPrinter,lcOldPrinter
		Store 0 To lcCount
		lcOldPrinter= set("printer",2)


		TEXT TO lcSQL TEXTMERGE NOSHOW
			select documento,nomeficheiro from B_impressoes (nolock) where tabela = 'CARTAO_ETIQUETAS'
		ENDTEXT
		If !uf_gerais_actGrelha("", "ucrsEtiquetasFicheiro", lcSql)
			uf_perguntalt_chama("N�o foi possivel verificar a configura��o de etiquetas. Por favor contacte o suporte.", "", "OK", 32)
			RETURN .f.
		ENDIF

		lcReportCartao = ALLTRIM(ucrsEtiquetasFicheiro.nomeficheiro)
			
		&& Verifica se os reports de etiquetas existem
		If !File(Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportCartao))
			uf_perguntalt_chama("Modelo de impress�o n�o encontrado. Se a dificuldade persistir contacte o Suporte.","OK","",48)
			RETURN .f.
		ENDIF

		**
		lcPrinter = uf_etiquetas_calculaLabelPrinter("PRODUTO")			

		&& Verificar se a impressora existe
		IF EMPTY(lcPrinter)
			uf_perguntalt_chama("A impressora n�o est� a ser detectada. Se a dificuldade persistir contacte o Suporte.","OK","",48)
			RETURN .f.
		ELSE
			DIMENSION lcPrinters[1,1]
			APRINTERS(lcPrinters)
			LOCAL lcExistePrinter
			FOR lnIndex = 1 TO ALEN(lcPrinters,1)
				IF upper(alltrim(lcPrinters(lnIndex,1))) == upper(alltrim(lcPrinter))
					lcExistePrinter = .t.
				ENDIF
			ENDFOR
			IF !lcExistePrinter && se existir
				uf_perguntalt_chama("A impressora n�o est� a ser detectada. Se a dificuldade persistir contacte o Suporte.","OK","",48)
				RETURN .f.
			ENDIF
		ENDIF 
			
		&& Define a Impressora	
		Set Printer To Name ''+lcPrinter+''
		Set Device To print
		Set Console off

		&& Cria cursor Aux que permite usar o mesmo IDU em diferentes paineis
		**select * FROM Cl INTO CURSOR auxImpCl READWRITE
		IF EMPTY(lcCRM)
			SELECT no, nome, '*'+alltrim(nrcartao)+'*'  as nrcartao FROM cl INTO CURSOR auxImpCl READWRITE 
		ELSE
			
			lcNo = uCrsFidel.clno
			SELECT clno as no, nome, '*'+alltrim(nrcartao)+'*'  as nrcartao FROM uCrsFidel WHERE clno = lcNo INTO CURSOR auxImpCl READWRITE 
		ENDIF
		
		** Trata dados para impress�o
		SELECT auxImpCl 
		GO TOP 
			Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportCartao) To Printer

		&& Define a impressora no default
		Set Device To screen
		Set printer to Name ''+lcOldPrinter+''
		Set Console On

		IF USED("auxImpCl")
			fecha("auxImpCl")
		ENDIF
	ELSE
		uf_perguntalt_chama("O Cliente seleccionado n�o tem Cart�o de Cliente associado. Por favor verifique.","OK","",48)
		RETURN .f.
	ENDIF
ENDFUNC 

*!*	FUNCTION uf_Esquecimento_Cliente 
*!*		LPARAMETERS lcUtstamp

*!*		IF uf_perguntalt_chama("Deseja imprimir a minuta do pedido para o utente assinar?","Sim","N�o")
*!*			SELECT cl
*!*			GO TOP 	

*!*			&& PREPARAR DEFINI��ES GERAIS DA IMPRESSORA
*!*			lcValidaImp = uf_gerais_setImpressoraPOS(.t.)
*!*			
*!*			IF lcValidaImp
*!*			*** INICIALIZAR A IMPRESSORA ***
*!*			???	chr(027)+chr(064)
*!*			*** ESCOLHER A IMPRESSORA ***
*!*			???	chr(27)+chr(99)+chr(48)+chr(1)
*!*			*** DEFINIR C�DIGO DE CARACTERES PORTUGU�S ***
*!*			???	chr(027)+chr(116)+chr(003)
*!*			*** ESCOLHER MODOS DE IMPRESS�O ***
*!*			???	chr(27)+chr(33)+chr(1)
*!*							
*!*							
*!*			****** IMPRESS�O DO CABE�ALHO ******
*!*			*** NOME DA EMPRESA ***
*!*			??	" "
*!*			?	LEFT(ALLTRIM(uCrsE1.NOMECOMP),uf_gerais_getParameter("ADM0000000194","NUM"))

*!*			*** OUTROS DADOS DA EMPRESA ***
*!*			??? chr(27)+chr(33)+chr(1)	&& print mode B
*!*			?	ALLTRIM(uCrsE1.morada)
*!*			?	ALLTRIM(uCrsE1.local)
*!*			?	ALLTRIM(uCrsE1.codpost)
*!*			?	"Tel:"
*!*			??	TRANSFORM(uCrsE1.TELEFONE,"#########")
*!*			??	" NIF:"
*!*			??	TRANSFORM(uCrsE1.NCONT,"999999999")
*!*			?	"Dir. Tec. "
*!*			??	LEFT(ALLTRIM(uCrsE1.U_DIRTEC),uf_gerais_getParameter("ADM0000000194","NUM"))
*!*			??	
*!*			?	" "
*!*			uf_gerais_separadorPOS()
*!*			?	PADC("CONFIRMACAO DE PEDIDO DE ESQUECIMENTO",uf_gerais_getParameter("ADM0000000194","NUM")," ")
*!*			uf_gerais_separadorPOS()
*!*			?	" "

*!*			?	DATETIME()

*!*			SELECT CL
*!*			?	"Operador: " + Alltrim(m_chinis)

*!*			?	" "
*!*			?	"Nr.: "+ Astr(cl.no)
*!*			?	"Cliente: "+ ALLTRIM(CL.nome)
*!*			?	"NIF: "+ ALLTRIM(CL.ncont)
*!*			?	"Morada: "+ ALLTRIM(cl.morada)
*!*			?	"Localidade: "+ ALLTRIM(cl.local)
*!*			?	"Cod. Postal: "+ ALLTRIM(cl.codpost)
*!*			?	" "
*!*			
*!*			uf_gerais_separadorPOS()
*!*			?	" "
*!*			?	PADC("Confirmo que efetuei o pedido de esquecimento",uf_gerais_getParameter("ADM0000000194","NUM")," ")
*!*			?	PADC("dos meus dados",uf_gerais_getParameter("ADM0000000194","NUM")," ")
*!*			?	" "
*!*			?	" " 
*!*			?	" "
*!*			uf_gerais_separadorPOS()
*!*			?   ALLTRIM(ucrse1.local) 
*!*			?   DATE()

*!*			****** IMPRESS�O DO RODAP� ******
*!*														
*!*			uf_gerais_feedCutPOS()
*!*			ENDIF
*!*			uf_gerais_setImpressoraPOS(.f.)

*!*			SET PRINTER TO DEFAULT
*!*		ELSE
*!*			uf_perguntalt_chama("N�o se esque�a que deve ter a formaliza��o do pedido feito pelo cliente.","OK","",48)	
*!*		ENDIF 

*!*		IF uf_perguntalt_chama("Confirma o pedido de esquecimento do utente?","Sim","N�o")

*!*			TEXT TO lcSQL TEXTMERGE NOSHOW
*!*				UPDATE b_utentes SET removido=1, data_removido=getdate(), inactivo=1 WHERE utstamp='<<ALLTRIM(lcUtstamp)>>'
*!*			ENDTEXT
*!*			If !uf_gerais_actGrelha("", "", lcSql)
*!*				uf_perguntalt_chama("N�o foi possivel exercer a op��o de esquecimento. Por favor contacte o suporte.", "", "OK", 32)
*!*				RETURN .f.
*!*			ENDIF
*!*			uf_user_log_ins('CL', ALLTRIM(lcUtstamp), 'ESQUECIMENTO', 'Exercida a op��o de esquecimento')	
*!*			IF !EMPTY(cl.email) AND ucrse1.usacoms=.t. 
*!*				LOCAL lcTo, lcToEmail, lcSubject, lcBody
*!*				lcTo = ""
*!*				lcToEmail = ALLTRIM(cl.email)
*!*				lcSubject = 'Exercimento da Op��o de Esquecimento'
*!*				lcBody = "Exmo(a). Sr(a). " + CHR(13) + CHR(13) + "Vimos por este meio confirmar o seu pedido de esquecimento dos dados feito na nossa farm�cia.";
*!*						 + CHR(13) + 'A sua informa��o pessoal foi removida do nosso sistema com sucesso.' + CHR(13);
*!*						 + CHR(13) + ALLTRIM(ucrse1.mailsign)
*!*				lcAtatchment = ""
*!*				regua(0,6,"A enviar email para o utente com a confirma��o...")
*!*				uf_startup_sendmail(lcTo, lcToEmail, lcSubject, lcBody , lcAtatchment, .t.)
*!*				regua(2)
*!*			ENDIF 
*!*			
*!*			uf_perguntalt_chama("Dados do utente removidos do sistema.","OK","",48)	
*!*			uf_PESQUTENTES_chama("UTENTES")
*!*		ELSE 
*!*			uf_perguntalt_chama("Opera��o cancelada.","OK","",48)	
*!*			RETURN .f.
*!*		ENDIF
*!*		

*!*	ENDFUNC 

*!*	FUNCTION uf_envio_dados
*!*		LPARAMETERS lcUtstamp
*!*		LOCAL mDir, lcTo, lcToEmail, lcSubject, lcBody, lcAtatchment, lcNomeFicheiroXLS
*!*		
*!*		IF !EMPTY(cl.email)
*!*			IF uf_perguntalt_chama("CONFIRMA O ENVIO DO FICHEIRO PARA O UTENTE?" + CHR(13) + "CONFIRME QUE TEM A FORMALIZA��O DO PEDIDO NA SUA POSSE.","Sim","N�o")
*!*				mdir = ALLTRIM(uf_gerais_getParameter('ADM0000000015','TEXT')) + "\Cache\XLS"
*!*				lcNomeFicheiroXLS= 'Utente'+ALLTRIM(STR(cl.no)) + ".xls"
*!*				lcTo = ""
*!*				lcToEmail = ALLTRIM(cl.email)
*!*				lcSubject = 'Envio dos dados pessoais'
*!*				lcBody = "Exmo(a). Sr(a). " + CHR(13) + CHR(13) + "No seguimento do seu pedido, vimos por este meio enviar-lhe o documento em excel com os seus dados pessoais." + CHR(13) + ALLTRIM(ucrse1.mailsign)
*!*				lcAtatchment = mdir + "\"+ALLTRIM(lcNomeFicheiroXLS)

*!*				TEXT TO lcSQL TEXTMERGE NOSHOW
*!*					up_utentes_exporta '<<ALLTRIM(lcUtstamp)>>'
*!*				ENDTEXT 
*!*				IF !uf_gerais_actgrelha("", "ucrsDadosUt", lcSql)
*!*					uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!","OK","",16)
*!*					RETURN .F.
*!*				ENDIF 

*!*				SELECT ucrsDadosUt
*!*				export to ALLTRIM(lcAtatchment) type xl5
*!*				SELECT cl
*!*				
*!*				regua(0,6,"A enviar email ...")
*!*				uf_startup_sendmail(lcTo, lcToEmail, lcSubject, lcBody , lcAtatchment, .t.)
*!*				regua(2)

*!*				uf_user_log_ins('CL', ALLTRIM(lcUtstamp), 'EMAIL', 'Envio de dados pessoais por email - ' + ALLTRIM(lcNomeFicheiroXLS))
*!*			ELSE
*!*				uf_perguntalt_chama("ENVIO CANCELADO!","OK","",16)
*!*			ENDIF 

*!*		ELSE
*!*			uf_perguntalt_chama("N�o existe um email para envio preenchido." + CHR(13) + "Insira um ou atualize a ficha respetiva.","OK","",64)
*!*			RETURN .f.
*!*		ENDIF 
*!*	ENDFUNC 

FUNCTION uf_butentes_duplicar
	LOCAL lcButentesNewStamp, lcButentesNewNo
	STORE 0 TO lcButentesNewNo
	lcButentesNewStamp = uf_gerais_stamp()

	IF EMPTY(cl.ncont)
		uf_perguntalt_chama("N�O PODE DUPLICAR / INATIVAR UM UTENTE SEM O NIF PREENCHIDO!","OK","",16)
		RETURN .F.
	ENDIF 
	
	IF cl.estab=0
		TEXT TO lcSQL TEXTMERGE NOSHOW
			select * from b_utentes where no=<<cl.no>> and estab>0  AND inactivo=0
		ENDTEXT 
		IF !uf_gerais_actgrelha("", "ucrsDepUt", lcSql)
			uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!","OK","",16)
			RETURN .F.
		ENDIF 
		IF RECCOUNT("ucrsDepUt")>0 then
			uf_perguntalt_chama("N�O PODE DUPLICAR / INATIVAR UM UTENTE COM DEPENDENTES!","OK","",16)
			RETURN .F.
		ENDIF
		fecha("ucrsDepUt") 
	ENDIF 

	TEXT TO lcSQL TEXTMERGE NOSHOW
		select * from cc where no=<<cl.no>> and estab=<<cl.estab>> and (edeb<>edebf or ecred<>ecredf)
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "ucrsCCNRegUt", lcSql)
		uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!","OK","",16)
		RETURN .F.
	ENDIF 
	IF RECCOUNT("ucrsCCNRegUt")>0 then
		uf_perguntalt_chama("N�O PODE DUPLICAR / INATIVAR UM UTENTE COM D�VIDAS!","OK","",16)
		RETURN .F.
	ENDIF
	fecha("ucrsCCNRegUt") 

	TEXT TO lcSQL TEXTMERGE NOSHOW
		select * from ft where cobrado=1 and no=<<cl.no>> and estab=<<cl.estab>> and facturada=0
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "ucrsVSuspUt", lcSql)
		uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!","OK","",16)
		RETURN .F.
	ENDIF 
	IF RECCOUNT("ucrsVSuspUt")>0 then
		uf_perguntalt_chama("N�O PODE DUPLICAR / INATIVAR UM UTENTE COM VENDAS SUSPENSAS!","OK","",16)
		RETURN .F.
	ENDIF
	fecha("ucrsVSuspUt") 

	TEXT TO lcSQL TEXTMERGE NOSHOW
		select max(bu.no)+1 as no from b_utentes bu
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "ucrsNewUt", lcSql)
		uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!","OK","",16)
		RETURN .F.
	ENDIF 
	lcButentesNewNo = ucrsNewUt.no
	fecha("ucrsNewUt") 


	TEXT TO lcSql NOSHOW TEXTMERGE
		insert into b_utentes (
			utstamp
			,nome
			,no
			,estab
			,ncont
			,nbenef
			,local
			,nascimento
			,morada
			,telefone
			,obs
			,codpost
			,bino
			,fax
			,tlmvl
			,zona
			,tipo
			,sexo
			,email
			,codpla
			,despla
			,valipla
			,valipla2
			,nrcartao
			,hablit
			,profi
			,nbenef2
			,entfact
			,peso
			,altura
			,inactivo
			,nocredit
			,ousrinis
			,ousrdata
			,ousrhora
			,usrinis
			,usrdata
			,usrhora
			,nome2
			,url
			,cobnao
			,naoencomenda
			,vencimento
			,eplafond
			,nib
			,odatraso
			,tabiva
			,tipodesc
			,esaldlet
			,clinica
			,modofact
			,desconto
			,alimite
			,particular
			,clivd
			,autofact
			,esaldo
			,conta
			,descpp
			,ccusto
			,contalet
			,contaletdes
			,contaletsac
			,contaainc
			,contaacer
			,tpdesc
			,classe
			,radicaltipoemp
			,fref
			,moeda
			,contatit
			,contafac
			,ultvenda
			,entpla
			,descp
			,codigop
			,mae
			,pai
			,notif
			,pacgen
			,tratamento
			,ninscs
			,csaude
			,drfamilia
			,recmtipo
			,recmmotivo
			,recmdescricao
			,recmdatainicio
			,recmdatafim
			,itmotivo
			,itdescricao
			,itdatainicio
			,itdatafim
			,cesd
			,cesdcart
			,cesdcp
			,cesdidi
			,cesdidp
			,cesdpd
			,cesdval
			,atestado
			,atnumero
			,attipo
			,atcp
			,atpd
			,atval
			,descpNat
			,codigopNat
			,duplicado
			,nomesproprios
			,obito
			,apelidos
			,idstamp
			,pais
			,exportado
			,entCompart
			,no_ext
			,noConvencao
			,designConvencao
			,id
			,nprescsns
			,direcaoTec
			,proprietario
			,moradaAlt
			,noAssociado
			,Alvara
			,software
			,ars
			,codSwift
			,designcom
			,nomcom
			,autorizado
			,removido
			,data_removido
			,tokenaprovacao
			,autoriza_sms
			,autoriza_emails
			,site_compart
			)
		select 
			'<<ALLTRIM(lcButentesNewStamp)>>'
			,nome
			,<<lcButentesNewNo >>
			,0
			,ncont
			,nbenef
			,local
			,nascimento
			,morada
			,telefone
			,obs
			,codpost
			,bino
			,fax
			,tlmvl
			,zona
			,tipo
			,sexo
			,email
			,codpla
			,despla
			,valipla
			,valipla2
			,nrcartao
			,hablit
			,profi
			,nbenef2
			,entfact
			,peso
			,altura
			,inactivo
			,nocredit
			,ousrinis
			,ousrdata
			,ousrhora
			,usrinis
			,usrdata
			,usrhora
			,nome2
			,url
			,cobnao
			,naoencomenda
			,vencimento
			,eplafond
			,nib
			,odatraso
			,tabiva
			,tipodesc
			,esaldlet
			,clinica
			,modofact
			,desconto
			,alimite
			,particular
			,clivd
			,autofact
			,esaldo
			,conta
			,descpp
			,ccusto
			,contalet
			,contaletdes
			,contaletsac
			,contaainc
			,contaacer
			,tpdesc
			,classe
			,radicaltipoemp
			,fref
			,moeda
			,contatit
			,contafac
			,ultvenda
			,entpla
			,descp
			,codigop
			,mae
			,pai
			,notif
			,pacgen
			,tratamento
			,ninscs
			,csaude
			,drfamilia
			,recmtipo
			,recmmotivo
			,recmdescricao
			,recmdatainicio
			,recmdatafim
			,itmotivo
			,itdescricao
			,itdatainicio
			,itdatafim
			,cesd
			,cesdcart
			,cesdcp
			,cesdidi
			,cesdidp
			,cesdpd
			,cesdval
			,atestado
			,atnumero
			,attipo
			,atcp
			,atpd
			,atval
			,descpNat
			,codigopNat
			,duplicado
			,nomesproprios
			,obito
			,apelidos
			,idstamp
			,pais
			,exportado
			,entCompart
			,no_ext
			,noConvencao
			,designConvencao
			,id
			,nprescsns
			,direcaoTec
			,proprietario
			,moradaAlt
			,noAssociado
			,Alvara
			,software
			,ars
			,codSwift
			,designcom
			,nomcom
			,0
			,0
			,'19000101'
			,0
			,0
			,0
			,''
		from 
			b_utentes
		where 
			no=<<cl.no>>
			and estab=<<cl.estab>>

		update b_utentes set inactivo=1 where no=<<cl.no>> and estab=<<cl.estab>>
		
		UPDATE b_fidel SET clstamp='<<ALLTRIM(lcButentesNewStamp)>>', clno=<<lcButentesNewNo>>, clestab=0 WHERE clstamp='<<cl.utstamp>>'
		
		UPDATE b_fidelvale SET clstamp='<<ALLTRIM(lcButentesNewStamp)>>' WHERE clstamp='<<cl.utstamp>>'
	ENDTEXT
	
	IF !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("OCORREU UM PROBLEMA A DUPLICAR O UTENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ELSE
		uf_perguntalt_chama("UTENTE DUPLICADO COM SUCESSO.","OK","",16)
		uf_utentes_chama(lcButentesNewNo, 0)
	ENDIF


ENDFUNC 

FUNCTION uf_utentes_Vacinacao_chama()
	uf_Vacinacao_chama()
	uf_Vacinacao_inserir()
	replace uCrsVacinacao.nome WITH cl.nome
	replace uCrsVacinacao.DataNasc WITH cl.nascimento
	IF !EMPTY(cl.tlmvl) THEN 
		replace uCrsVacinacao.Contacto WITH cl.tlmvl
	ELSE
		replace uCrsVacinacao.Contacto WITH cl.telefone
	ENDIF 
	replace uCrsVacinacao.nrSns WITH cl.nbenef
	
	vacinacao.Refresh
ENDFUNC 