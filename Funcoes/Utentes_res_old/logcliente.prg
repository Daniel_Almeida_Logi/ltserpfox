FUNCTION uf_LOGCliente_chama
	LPARAMETERS lcStamp
		
	** cria cursor vazio
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select * from user_logs WHERE 0=1 ORDER BY DATA
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "uCrsLogCl", lcSql)
		uf_perguntalt_chama("ERRO AO LER A TABELA DE LOGS!","OK","",16)
		RETURN .F.
	ENDIF 
	
	&& Controla Abertura do Painel			
	if type("LOGCLIENTE")=="U"
		DO FORM LOGCLIENTE
	ELSE
		LOGCLIENTE.show
	ENDIF
	
	** carrega dados
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select * from user_logs WHERE REGSTAMP='<<lcStamp>>' AND [table]='CL' ORDER BY DATA
	ENDTEXT 	
	IF !uf_gerais_actgrelha("logcliente.GridPesq", "uCrsLogCl", lcSql)
		uf_perguntalt_chama("ERRO AO LER A TABELA DE LOGS!","OK","",16)
		RETURN .F.
	ENDIF 
	
ENDFUNC


**
FUNCTION uf_LOGCLIENTE_carregamenu
	logcliente.menu1.adicionaopcao("atualizar", "Atualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_LOGCLIENTE_Pesquisa", "F2")
ENDFUNC


**
FUNCTION uf_LOGCLIENTE_Pesquisa
	LOCAL lcSQL
	
	regua(0,0,"A atualizar...")
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select * from user_logs WHERE REGSTAMP='<<lcStamp>>' AND [table]='CL' ORDER BY DATA
	ENDTEXT

	uf_gerais_actGrelha("LOGCLIENTE.GridPesq", "ucrsLogCl", lcSQL)

	regua(2)
	
	LOGCLIENTE.Refresh
	
ENDFUNC

**
FUNCTION uf_LOGCLIENTE_sair
	**Fecha Cursores
	IF USED("uCrsLogCl")
		fecha("uCrsLogCl")
	ENDIF 
	
	LOGCLIENTE.hide
	LOGCLIENTE.release
ENDFUNC