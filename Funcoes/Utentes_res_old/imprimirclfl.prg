FUNCTION uf_chamaImprimirCLFL		
	LPARAMETERS tcTable
	
	&& recupera fun��es
		uf_recoverImprimirCLFL()
	
	IF EMPTY(tcTable)
		MESSAGEBOX("O PAR�METRO: TABELA, N�O PODE SER NULO!",16,"LOGITOOLS SOFTWARE")
		RETURN
	ENDIF
	
	&& Controla abertura do Painel
		if type("ImprimirCLFL")=="U"
			DO FORM ImprimirCLFL
		ELSE
			ImprimirCLFL.show
		Endif
	
	&& Configura o ecr�
		DO case
			&& Painel de Clientes - CL
				CASE UPPER(tcTable) == UPPER("cl")
					ImprimirCLFL.text1.value = UPPER(tcTable)
					
					IF USED("cl")
						SELECT cl
						ImprimirCLFL.txtDO.value 	= cl.no
						ImprimirCLFL.txtAO.value 	= cl.no
						ImprimirCLFL.txtEstab.value = cl.estab
					ELSE
						ImprimirCLFL.txtDO.value 	= 0
						ImprimirCLFL.txtAO.value 	= 0
						ImprimirCLFL.txtEstab.value = 0
					endif	
					
					&& Verifica se o report existe
						If !File(Alltrim(mypath)+'\analises\clientes.frx')
							MESSAGEBOX("DESCULPE, O REPORT DE CLIENTES N�O EST� A SER DETECTADO! POR FAVOR CONTACTE O SUPORTE.",48,"LOGITOOLS SOFTWARE")
							return
						endif
					
					&& Muda a caption do Formulario
						imprimirCLFL.caption = "Impress�o de Clientes"
						
						
			&& Painel de Fornecedores - FL
				CASE UPPER(tcTable) == UPPER("fl")	
					ImprimirCLFL.text1.value = UPPER(tcTable)
					
					IF USED("Fl")
						SELECT Fl
						ImprimirCLFL.txtDO.value 	= FL.no
						ImprimirCLFL.txtAO.value 	= FL.no
						ImprimirCLFL.txtEstab.value = fl.estab
					ELSE
						ImprimirCLFL.txtDO.value 	= 0
						ImprimirCLFL.txtAO.value 	= 0
						ImprimirCLFL.txtEstab.value = 0
					ENDIF			
					
					&& Verifica se o report existe
						If !File(Alltrim(mypath)+'\analises\fornecedores.frx')
							MESSAGEBOX("DESCULPE, O REPORT DE FORNECEDORES N�O EST� A SER DETECTADO! POR FAVOR CONTACTE O SUPORTE.",48,"LOGITOOLS SOFTWARE")
							return
						ENDIF
					
					&& Muda a caption do Formulario
						imprimirCLFL.caption = "Impress�o de Fornecedores"	
				
			&& Painel de Clinica - ID	
				
			OTHERWISE
				MESSAGEBOX("O PARAMETRO: TABELA, N�O FOI ACEITE!",16,"LOGITOOLS SOFTWARE")
				return
		ENDCASE 		
				
ENDFUNC 



FUNCTION uf_PrevImprimirCLFL		
	
	&& recupera fun��es
		uf_recoverImprimirCLFL()
	
	&& A previs�o s� funciona para um cliente de cada vez
		IF ImprimirCLFL.txtDO.value <> ImprimirCLFL.txtAO.value
			MESSAGEBOX("ESTA FUNCIONALIDADE S� EST� DISPON�VEL PARA PR�-VISUALIZAR UM CLIENTE APENAS.",48,"LOGITOOLS SOFTWARE")
			return
		endif
	
	&& Valida Dados
		LOCAL lcValida
		STORE .f. TO lcValida	
		
		lcValida = uf_DadosImprimirCLFL()
		
		IF !lcValida	
			return
		endif
	
	&& Prepara impressora
		lcReport = IIF(UPPER(ImprimirCLFL.text1.value)==UPPER('cl'),ALLTRIM(mypath)+'\analises\clientes.frx',ALLTRIM(mypath)+'\analises\fornecedores.frx')
		lcPrinter = Getprinter()
		lcDefaultPrinter=set("printer",2)
	
	If !Empty(lcPrinter)	
		&& Establecimentos
			IF (ImprimirCLFL.checkEstab.value == 0)
				u_sqlexec([select * from ]+ImprimirCLFL.text1.value+[ (nolock) where no=-45 order by no],[ucrsEstabCLFL])
			else
				IF !u_sqlexec([select * from ]+ImprimirCLFL.text1.value+[ (nolock) where no between ]+STR(ImprimirCLFL.txtDO.value)+[ and ]+STR(ImprimirCLFL.txtDO.value)+[ and estab!=]+STR(ImprimirCLFL.txtEstab.value)+[ order by no],[ucrsEstabCLFL])
					MESSAGEBOX("OCORREU UM ERRO AO CARREGAR OS DADOS PARA IMPRIMIR! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
					RETURN .F.
				ENDIF
			ENDIF 	
			
			SELECT ucrsImprimirCLFL 
			SELECT ucrsEstabCLFL
			SELECT uCRsE1
			
			Report Form Alltrim(lcReport) To Printer Prompt Nodialog preview
	ENDIF 
	
	&& por impressora no default
		Set Device To screen
		Set printer to Name ''+lcDefaultPrinter+''
		Set Console On	
ENDFUNC 




FUNCTION uf_PrintImprimirCLFL		
	&& recupera fun��es
		uf_recoverImprimirCLFL()
	
	LOCAL lcValida
	STORE .f. TO lcValida	
	
	&& Valida Dados
		lcValida = uf_DadosImprimirCLFL()
		
		IF !lcValida	
			return
		ENDIF
	
	&& Prepara impressora
		lcReport = IIF(UPPER(ImprimirCLFL.text1.value)==UPPER('cl'),ALLTRIM(mypath)+'\analises\clientes.frx',ALLTRIM(mypath)+'\analises\fornecedores.frx')
		lcPrinter = Getprinter()
		lcDefaultPrinter=set("printer",2)
	
	If !Empty(lcPrinter)			
		** Preparar Regua **
		Select uCrsImprimirCLFL
		mntotal=reccount()
		regua(0,mntotal,"IMPRIMINDO...",.f.)
		****************

		Set Printer To Name ''+lcPrinter+''
		Set Device To print
		Set Console off
			
		SELECT uCrsImprimirCLFL
		GO top
		SCAN
			&& Estabelecimentos
			IF (ImprimirCLFL.checkEstab.value==0)
				u_sqlexec([select * from ]+ImprimirCLFL.text1.value+[ (nolock) where no=-45 order by estab],[ucrsEstabCLFL])
			else
				IF !u_sqlexec([select * from ]+ImprimirCLFL.text1.value+[ (nolock) where no =]+STR(uCrsImprimirCLFL.no)+[ and estab!=]+STR(ImprimirCLFL.txtEstab.value)+[ order by estab],[ucrsEstabCLFL])
					MESSAGEBOX("OCORREU UM ERRO AO CARREGAR OS DADOS PARA IMPRIMIR! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
					RETURN .F.
				ENDIF
			ENDIF 	
			
			SELECT ucrsImprimirCLFL 
			GO TOP 
			
			SELECT uCRsE1
			GO TOP
			
			SELECT ucrsEstabCLFL
			GO TOP 
			
			Report Form Alltrim(lcReport) To Printer
		ENDSCAN
	
		&& fecha a regua
		regua(2)
	Else
		Messagebox("DESCULPE, MAS N�O � POSS�VEL IMPRIMIR SEM ESCOLHER UMA IMPRESSORA.",48,"LOGITOOLS SOFTWARE")
	Endif	
	
	&& por impressora no default
		Set Device To screen
		Set printer to Name ''+lcDefaultPrinter+''
		Set Console On		
ENDFUNC 
	
	
FUNCTION uf_DadosImprimirCLFL
	
	&& Valida��es
		&& Campo inicial preenchido
			IF EMPTY(ImprimirCLFL.txtDO.value) OR (ImprimirCLFL.txtDO.value = 0)
				MESSAGEBOX("POR FAVOR PREENCHA UM N�MERO INICIAL V�LIDO!",48,"LOGITOOLS SOFTWARE")
				ImprimirCLFL.txtDO.SETFOCUS
				RETURN .F.
			endif
		
		&& Campo final preenchido
			IF EMPTY(ImprimirCLFL.txtAO.value) OR (ImprimirCLFL.txtAO.value = 0)
				MESSAGEBOX("POR FAVOR PREENCHA UM N�MERO FINAL V�LIDO!",48,"LOGITOOLS SOFTWARE")
				ImprimirCLFL.txtAO.SETFOCUS
				RETURN .F.
			endif	
		
		&& && Campo final > que o inicial
			IF ImprimirCLFL.txtAO.value > ImprimirCLFL.txtDO.value 	
				MESSAGEBOX("O N�MERO FINAL N�O PODE SER SUPERIOR AO INICIAL!",48,"LOGITOOLS SOFTWARE")
				ImprimirCLFL.txtAO.SETFOCUS
				RETURN .F.
			ENDIF
		
		&& Valida se o N� inicial existe
			DO case
				CASE UPPER(ImprimirCLFL.text1.value) == UPPER('CL')
					IF u_sqlexec([select top 1 clstamp as stamp from cl (nolock) where no=]+STR(ImprimirCLFL.txtDO.value)+[ and estab=]+STR(ImprimirCLFL.txtEstab.value),[uCrsCheckImprimirCLFL])
						IF RECCOUNT("uCrsCheckImprimirCLFL")=0
							MESSAGEBOX("DESCULPE MAS O N�MERO INICIAL INTRODUZIDO N�O TEM FICHA ASSOCIADA! POR FAVOR PREENCHA UM N�MERO V�LIDO.",48,"LOGITOOLS SOFTWARE")
							FECHA("uCrsCheckImprimirCLFL")
							RETURN .F.
						ENDIF
					ELSE
						MESSAGEBOX("DESCULPE, OCORREU UM ERRO AO VALIDAR O N�MERO INICIAL!. POR FAVOR TENTE NOVAMENTE.",16,"LOGITOOLS SOFTWARE")
						RETURN .F.
					ENDIF	
				
				CASE UPPER(ImprimirCLFL.text1.value) == UPPER('FL')
					IF u_sqlexec([select top 1 flstamp as stamp from fl (nolock) where no=]+STR(ImprimirCLFL.txtDO.value)+[ and estab=]+STR(ImprimirCLFL.txtEstab.value),[uCrsCheckImprimirCLFL])
						IF RECCOUNT("uCrsCheckImprimirCLFL")=0
							MESSAGEBOX("DESCULPE MAS O N�MERO INICIAL INTRODUZIDO N�O TEM FICHA ASSOCIADA! POR FAVOR PREENCHA UM N�MERO V�LIDO.",48,"LOGITOOLS SOFTWARE")
							FECHA("uCrsCheckImprimirCLFL")
							RETURN .F.
						ENDIF
					ELSE
						MESSAGEBOX("DESCULPE, OCORREU UM ERRO AO VALIDAR O N�MERO INICIAL!. POR FAVOR TENTE NOVAMENTE.",16,"LOGITOOLS SOFTWARE")
						RETURN .F.
					ENDIF
			ENDCASE 				
					
		&& Valida se o N� final existe
			DO case
				CASE UPPER(ImprimirCLFL.text1.value) == UPPER('CL')
					IF u_sqlexec([select top 1 clstamp as stamp from cl (nolock) where no=]+STR(ImprimirCLFL.txtAO.value)+[ and estab=]+STR(ImprimirCLFL.txtEstab.value),[uCrsCheckImprimirCLFL])
						IF RECCOUNT("uCrsCheckImprimirCLFL")=0
							MESSAGEBOX("DESCULPE MAS O N�MERO FINAL INTRODUZIDO N�O TEM FICHA ASSOCIADA! POR FAVOR PREENCHA UM N�MERO V�LIDO.",48,"LOGITOOLS SOFTWARE")
							FECHA("uCrsCheckImprimirCLFL")
							RETURN .F.
						ENDIF
					ELSE
						MESSAGEBOX("DESCULPE, OCORREU UM ERRO AO VALIDAR O N�MERO FINAL!. POR FAVOR TENTE NOVAMENTE.",16,"LOGITOOLS SOFTWARE")
						RETURN .F.
					ENDIF	
				
				CASE UPPER(ImprimirCLFL.text1.value) == UPPER('FL')
					IF u_sqlexec([select top 1 flstamp as stamp from fl (nolock) where no=]+STR(ImprimirCLFL.txtAO.value)+[ and estab=]+STR(ImprimirCLFL.txtEstab.value),[uCrsCheckImprimirCLFL])
						IF RECCOUNT("uCrsCheckImprimirCLFL")=0
							MESSAGEBOX("DESCULPE MAS O N�MERO FINAL INTRODUZIDO N�O TEM FICHA ASSOCIADA! POR FAVOR PREENCHA UM N�MERO V�LIDO.",48,"LOGITOOLS SOFTWARE")
							FECHA("uCrsCheckImprimirCLFL")
							RETURN .F.
						ENDIF
					ELSE
						MESSAGEBOX("DESCULPE, OCORREU UM ERRO AO VALIDAR O N�MERO FINAL!. POR FAVOR TENTE NOVAMENTE.",16,"LOGITOOLS SOFTWARE")
						RETURN .F.
					ENDIF
			ENDCASE 	
			
		&& Carrega dados Cliente/fornecedor
			LOCAL lcSQL
			STORE '' TO lcSQL
				
			TEXT TO lcSQL TEXTMERGE noshow
				select * 
				from <<alltrim(ImprimirCLFL.text1.value)>> (nolock) 
				where no between <<ImprimirCLFL.txtDO.value>> and <<ImprimirCLFL.txtAO.value>>
				and estab = <<IIF(ImprimirCLFL.checkEstab.value = 0,0,ImprimirCLFL.txtEstab.value))>>
				order by no			
			endtext
				
			IF !u_sqlexec(lcSQL,[ucrsImprimirCLFL])
				MESSAGEBOX("OCORREU UM ERRO AO CARREGAR OS DADOS PARA IMPRIMIR! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
				RETURN .F.
			ENDIF	
			
		&& Carrega Dados da empresa
			IF !u_sqlexec([exec up_gerais_dadosEmpresa ']+Alltrim(mysite)+['],[uCRsE1])
				MESSAGEBOX("OCORREU UM ERRO A CARREGAR OS DADOS DA EMPRESA! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
				RETURN .f.
			ENDIF	
		
		&& Verifica se usa o Cartao de Cliente	
			IF !(u_sqlexec([exec up_parameters_ProcuraBool 'ADM0000000052'],[uCrsChkCrtImprimir]))
				MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A VALIDAR O M�DULO DE CART�O DE CLIENTE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			endif		
		
		RETURN .t.
ENDFUNC 		



FUNCTION uf_exitImprimirCLFL
	&& recupera fun��es
		uf_recoverImprimirCLFL()
	
	IF USED("uCrsImprimirCLFL")
		Fecha("uCrsImprimirCLFL")
	ENDIF
	
	IF USED("uCrsEstabCLFL")
		Fecha("uCrsEstabCLFL")
	ENDIF
	
	IF USED("uCrsCheckImprimirCLFL")
		Fecha("uCrsCheckImprimirCLFL")
	ENDIF
	
	IF USED("uCrsChkCrtImprimir")
		Fecha("uCrsChkCrtImprimir")
	ENDIF

	ImprimirCLFL.hide
	ImprimirCLFL.Release
		
	RELEASE ImprimirCLFL
ENDFUNC 


		
		
*********************************************************************
*						Recupera Fun��es							*
*********************************************************************
FUNCTION uf_recoverImprimirCLFL
	try
		uf_recoverAppCompras()
		uf_recoverAppAtend()
	Catch
		do uf_startTerminal in alltrim(myPath)+'\startup\startup.app'
	ENDTRY
ENDFUNC 	
*********************************************************************		