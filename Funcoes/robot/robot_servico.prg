FUNCTION uf_servicos_unblockChilkat
	LOCAL loGlob
	LOCAL lnSuccess
	
	loGlob = CreateObject('Chilkat_9_5_0.Global')
	lnSuccess = loGlob.UnlockBundle("LGTLSP.CB1052019_eQLZAyAR9RkB")
	IF (lnSuccess <> 1) THEN
	    ? loGlob.LastErrorText
	    RELEASE loGlob
	    CANCEL
	ENDIF
	
	RELEASE loGlob
ENDFUNC	


FUNCTION uf_chilkat_send_request
	PARAMETERS lcPostHTTP, lcText, lcTipo
	LOCAL loReq, loHttp, lnSuccess,	lnSuccess1,	lcJsonText,	loResp

	uf_servicos_unblockChilkat()
	
	loReq = CreateObject('Chilkat_9_5_0.HttpRequest')
	loHttp = CreateObject('Chilkat_9_5_0.Http')

	loJson = CreateObject('Chilkat_9_5_0.JsonObject')

	loHttp.AcceptCharset = ""
	loHttp.UserAgent = ""
	loHttp.AcceptLanguage = ""
	loHttp.AllowGzip = 0

	lnSuccess = loHttp.AddQuickHeader("Content-Type","application/json")

	loResp = loHttp.PostJson(lcPostHTTP,lcText)
	IF (loHttp.LastMethodSuccess <> 1) THEN
	    MESSAGEBOX(loHttp.LastErrorText)
	ELSE
	ENDIF

	RELEASE loReq
	RELEASE loHttp

	loJson.Load(loResp.BodyStr)
	loJson.EmitCompact = 0
	lcRev = loJson.ObjectOf("status")
	return(lcRev.StringOf("t"))
	
	RELEASE loResp
	
ENDFUNC 

FUNCTION uf_robot_servico_gettimeout
	PARAMETERS lcOperationType
	
	LOCAL lcReturnVal
	STORE 0 TO lcReturnVal
	
	DO CASE 
		CASE ALLTRIM(lcOperationType) = 'status'
			lcReturnVal = myServRobotTOsta
		CASE ALLTRIM(lcOperationType) = 'getProductInfo'
			lcReturnVal = myServRobotTOGPI
		CASE ALLTRIM(lcOperationType) = 'retrieve'
			lcReturnVal = myServRobotTOret
		CASE ALLTRIM(lcOperationType) = 'stock'
			lcReturnVal = myServRobotTOinv
	ENDCASE 
	RETURN lcReturnVal 
		
ENDFUNC 


FUNCTION uf_robot_servico_header
	PARAMETERS lcOperationType, lcShouldWait, lcPedStamp2 
	LOCAL lcSendMsg, loDateTime, lnBLocal, lcUnixtime, lcTimeOut
	
	lcWaitsForLastResponse = 1

	loDateTime = CreateObject('Chilkat_9_5_0.CkDateTime')
	uf_servicos_unblockChilkat()
	loDateTime.SetFromCurrentSystemTime()

	lnBLocal = 1
	lcUnixtime = ALLTRIM(loDateTime.GetAsUnixTimeStr(lnBLocal))
	RELEASE loDateTime

	lcTimeOut = uf_robot_servico_gettimeout(ALLTRIM(lcOperationType))

	lcSendMsg = ''
	lcSendMsg = lcSendMsg + '{'
	lcSendMsg = lcSendMsg + '"operation": {'
	lcSendMsg = lcSendMsg + '"dispenserId": "'+ALLTRIM(myTipoRobot)+'",'
	lcSendMsg = lcSendMsg + '"operationType": "'+ALLTRIM(lcOperationType)+'",'
	lcSendMsg = lcSendMsg + '"sender": "'+LOWER(ALLTRIM(ucrse1.id_lt))+'",'
	lcSendMsg = lcSendMsg + '"terminal": "'+LOWER(ALLTRIM(myTerm))+'",'
	lcSendMsg = lcSendMsg + '"timestamp": '+ALLTRIM(lcUnixtime)+','
	lcSendMsg = lcSendMsg + '"token": "'+ALLTRIM(lcPedStamp2)+'",'
	lcSendMsg = lcSendMsg + '"shouldWait": '+IIF(lcShouldWait=.t., 'true', 'false')+','
	lcSendMsg = lcSendMsg + '"timeout": '+ALLTRIM(STR(lcTimeOut))+','

	RETURN lcSendMsg 
	
ENDFUNC 


FUNCTION uf_robot_servico_send
	PARAMETERS lcText, lcTimeOut, lcTypeServ
				
	LOCAL loReq, loHttp, lnSuccess,	lnSuccess1,	lcJsonText,	loResp
	
	uf_servicos_unblockChilkat()
	
	loReq = CreateObject('Chilkat_9_5_0.HttpRequest')
	loHttp = CreateObject('Chilkat_9_5_0.Http')

	loJson = CreateObject('Chilkat_9_5_0.JsonObject')

	loHttp.AcceptCharset = ""
	loHttp.UserAgent = ""
	loHttp.AcceptLanguage = ""

	loHttp.AllowGzip = 0
	
	loHttp.ConnectTimeout  = lcTimeOut
  	loHttp.ReadTimeout = lcTimeOut

	LcTokenReq = 'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3Z'
  	lnSuccess = loHttp.AddQuickHeader("o-auth-token",LcTokenReq)
  	
  	lcPostHTTP='https://app.lts.pt:50001/api/robot/relay?id='+LOWER(ALLTRIM(ucrse1.id_lt))
	
	loResp = loHttp.PostJson2(lcPostHTTP,"application/json",lcText)

	IF (loHttp.LastMethodSuccess <> 1) THEN
	&&	uf_perguntalt_chama("OCORREU UM ERRO NA COMUNICA��O COM O ROBOT! POR FAVOR CONTACTE O SUPORTE!","OK","", 16)
		return(.f.)
	ELSE
		LOCAL lcReturnI, lcReturnS, lcNmvsTrxId, lcState, lcReasons, lcCode, lcDescription, lcReturnCode, lcReturnStk, lcReturnStkCap, lcReturnProductCode
		STORE '' to lcReturnI, lcReturnS, lcNmvsTrxId, lcState, lcReasons, lcCode, lcDescription, lcReturnCode, lcReturnProductCode
		STORE 0 TO lcReturnStk, lcReturnStkCap 
	
		DO CASE 
			CASE ALLTRIM(lcTypeServ)= 'status'
			    RELEASE loReq
				RELEASE loHttp
				loJson.Load(loResp.BodyStr)
				loJson.EmitCompact = 0
				IF !(VARTYPE(loJson.ObjectOf("status"))=='U')
					lcRev = loJson.ObjectOf("status")
					lcReturnI = (lcRev.StringOf("i"))
				ENDIF 
			
			CASE ALLTRIM(lcTypeServ)= 'getProductInfoStatus'
			    RELEASE loReq
				RELEASE loHttp
				loJson.Load(loResp.BodyStr)
				loJson.EmitCompact = 0
				IF !(ISNULL(loJson.ObjectOf("operation")))
					lcRev1 = loJson.ObjectOf("operation")
					IF !(ISNULL(lcRev1.StringOf("status")))
						lcReturnI = (lcRev1.StringOf("status"))
					ENDIF 
				ENDIF 
				
			CASE ALLTRIM(lcTypeServ)= 'getProductInfo'
			    RELEASE loReq
				RELEASE loHttp
				loJson.Load(loResp.BodyStr)
				loJson.EmitCompact = 0
				lcReturnStk  = "-99999"	
				IF !(ISNULL(loJson.ObjectOf("operation")))
					lcRev1 = loJson.ObjectOf("operation")
					IF !(ISNULL("lcRev1"))
						IF !(ISNULL(lcRev1.StringOf("status")))
							LOCAL lcReturnStkI 
							lcReturnStkI = (lcRev1.StringOf("status"))
							if(lcReturnStkI == "200")
								IF !(ISNULL(lcRev1.ObjectOf("payload")))
									lcRev2 = lcRev1.ObjectOf("payload")
									IF !(ISNULL(lcRev2.StringOf("totalStock")))
										lcReturnStk = (lcRev2.StringOf("totalStock"))
										** actualiza a capacidade de stock do robot na ficha do producto
										IF !(ISNULL(lcRev2.StringOf("totalCapacity")) AND ISNULL(lcRev2.StringOf("productCode")) AND !EMPTY(lcRev2.StringOf("totalCapacity")) AND !EMPTY(lcRev2.StringOf("productCode")))
											lcReturnStkCap  = (lcRev2.StringOf("totalCapacity"))
											lcReturnProductCode  = (lcRev2.StringOf("productCode"))
											uf_robot_servico_actCapacity(lcReturnProductCode,lcReturnStkCap)
										ENDIF
								
									ENDIF 
									
								ENDIF 		
							ENDIF						
						ENDIF 
				  	ENDIF 
				ENDIF 
			CASE ALLTRIM(lcTypeServ)= 'stock'
			    RELEASE loReq
				RELEASE loHttp
				loJson.Load(loResp.BodyStr)
				loJson.EmitCompact = 0
				IF !(ISNULL(loJson.ObjectOf("status")))
					lcRev = loJson.ObjectOf("status")
					lcReturnI = (lcRev.StringOf("i"))
				ENDIF 
				
			CASE ALLTRIM(lcTypeServ)= 'retrieve'
			    RELEASE loReq
				RELEASE loHttp
				loJson.Load(loResp.BodyStr)
				loJson.EmitCompact = 0
				IF !(ISNULL(loJson.ObjectOf("operation")))
					lcRev1 = loJson.ObjectOf("operation")
					IF !(ISNULL(lcRev1.StringOf("status")))
						lcReturnI = (lcRev1.StringOf("status"))
					ENDIF 
					IF !(ISNULL("lcRev1"))
						IF !(ISNULL(lcRev1.ObjectOf("payload")))
							lcRev2 = lcRev1.ObjectOf("payload")
							IF !(ISNULL(lcRev2.StringOf("products")))
								loproducts = lcRev2.ArrayOf("products")
								IF (lcRev2.LastMethodSuccess = 0) THEN
								    uf_perguntalt_chama("N�O RETORNARAM QUAISQUER ARTIGOS DO ROBOT!","OK","", 16)
								    RELEASE loJson
								    **CANCEL
								ENDIF

								* Iterate over each employee, getting the JSON object at each index.
								lnNumproducts = loproducts.Size
								i = 0
								DO WHILE i < lnNumproducts

								    loproductsObj = loproducts.ObjectAt(i)
									
									SELECT ucrsRespRobotLin 
									GO BOTTOM 
									APPEND BLANK 
									replace ucrsRespRobotLin.ref WITH loproductsObj .StringOf("productCode")
									replace ucrsRespRobotLin.qtt WITH VAL(loproductsObj .StringOf("quantity"))
								    RELEASE loproductsObj

								    i = i + 1
								ENDDO

								RELEASE loproducts
							ENDIF 
						ENDIF 
					ENDIF 
				ENDIF 

			OTHERWISE

		ENDCASE 
	
	ENDIF 
	RELEASE loResp
		
	DO CASE 
		CASE ALLTRIM(lcTypeServ)= 'status'
			RETURN lcReturnI 
		CASE ALLTRIM(lcTypeServ)= 'getProductInfoStatus'
			RETURN lcReturnI 
		CASE ALLTRIM(lcTypeServ)= 'getProductInfo'
			RETURN lcReturnStk 
		CASE ALLTRIM(lcTypeServ)= 'stock'
			RETURN lcReturnI 
		CASE ALLTRIM(lcTypeServ)= 'retrieve'
			RETURN lcReturnI 
		OTHERWISE
			RETURN ''
	ENDCASE 
	
ENDFUNC 


FUNCTION uf_robot_servico_actCapacity
	LPARAMETERS lcCode, lcStockMax
	
	LOCAL lcSQL
	
	
	if(EMPTY(ALLTRIM(lcCode)) OR  EMPTY(ALLTRIM(lcStockMax)))
		RETURN .f.
	ENDIF
	
	lcStockMax = VAL(lcStockMax)
	

	TEXT TO lcSql NOSHOW TEXTMERGE PRETEXT 8
		UPDATE st SET robotStockMax=<<lcStockMax>> where st.ref='<<alltrim(lcCode)>>'  and st.site_nr = <<mysite_nr>>
	ENDTEXT
	IF !uf_gerais_actGrelha("","",lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia na atualiza��o do stock m�ximo do robot. Contacte o suporte.","OK","",16)
		RETURN .f.
	ENDIF

	
ENDFUNC




FUNCTION uf_robot_servico_status
	PARAMETERS lcPedStamp1 
	
	LOCAL lcSendMsg, lcResp, lcResposta, lcStatusRobot
	
	STORE .f. TO lcResposta, lcStatusRobot
	
	lcSendMsg = uf_robot_servico_header("status", .f., lcPedStamp1)
	
	lcSendMsg = lcSendMsg + '"payload": {'
	lcSendMsg = lcSendMsg + '"numberOfRequestingLocation": "'+RIGHT('00'+ALLTRIM(STR(myTermNo)),3)+'"'
	lcSendMsg = lcSendMsg + '} } }'
	
	lcResp = uf_robot_servico_send(lcSendMsg , myServRobotTOsta, "status")
	
	RETURN lcResp 
ENDFUNC

FUNCTION uf_robot_servico_status_response
	PARAMETERS lcPedStampR
	
	LOCAL lcResponse 
	STORE 'empty' TO lcResponse 
		
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		select rrp.machineState from robot_response_payload rrp (nolock)
		inner join robot_response rr (nolock) on rr.stamp=rrp.responseStamp
		where rr.token='<<lcPedStampR>>'
	ENDTEXT

	IF !uf_gerais_actGrelha("","ucrsresposta",lcSQL)
		uf_perguntalt_chama("ERRO AO PESQUISAR RESULTADO DO PEDIDO DE STATUS","OK","",48)
		RETURN .f.
	ENDIF
	
	
	IF RECCOUNT("ucrsresposta")>0
		lcResponse = ALLTRIM(ucrsresposta.machineState)
	ENDIF 


	RETURN lcResponse 
		
ENDFUNC 


FUNCTION uf_robot_servico_getprondinfo
	PARAMETERS lcPedStamp1, lcProductCode
	
	LOCAL lcSendMsg, lcResp, lcResposta, lcStatusRobot, lcPosLetterV, lcPosLetterR
	
	STORE .f. TO lcResposta, lcStatusRobot
	
	STORE 0 TO lcPosLetterV, lcPosLetterR
	
	lcPosLetterV = AT("V", ALLTRIM(lcProductCode))
	lcPosLetterR = AT("R", ALLTRIM(lcProductCode))
	
	if(lcPosLetterV>0 OR lcPosLetterR >0 OR EMPTY(ALLTRIM(lcProductCode)))
		RETURN "-999"
	ENDIF
	
	
	lcSendMsg = uf_robot_servico_header("getProductInfo", .t., lcPedStamp1)
	
	lcSendMsg = lcSendMsg + '"payload": {'
	lcSendMsg = lcSendMsg + '"numberOfRequestingLocation": "'+RIGHT('00'+ALLTRIM(STR(myTermNo)),3)+'",'
	lcSendMsg = lcSendMsg + '"productCode": "'+ALLTRIM(lcProductCode)+'"'
	lcSendMsg = lcSendMsg + '} } }'
	
	
	lcResp = uf_robot_servico_send(lcSendMsg , myServRobotTOGPI, "getProductInfo")
	

	RETURN lcResp 

ENDFUNC

FUNCTION uf_robot_servico_getprondinfo_status
	PARAMETERS lcPedStamp1, lcProductCode
	
	LOCAL lcSendMsg, lcResp, lcResposta, lcStatusRobot
	
	STORE .f. TO lcResposta, lcStatusRobot
	
	lcSendMsg = uf_robot_servico_header("getProductInfo", .t., lcPedStamp1)
	
	lcSendMsg = lcSendMsg + '"payload": {'
	lcSendMsg = lcSendMsg + '"numberOfRequestingLocation": "'+RIGHT('00'+ALLTRIM(STR(myTermNo)),3)+'",'
	lcSendMsg = lcSendMsg + '"productCode": "'+ALLTRIM(lcProductCode)+'"'
	lcSendMsg = lcSendMsg + '} } }'
	
	lcResp = uf_robot_servico_send(lcSendMsg , myServRobotTOGPI, "getProductInfoStatus")

	RETURN lcResp 

ENDFUNC


FUNCTION uf_robot_servico_inventory

	PARAMETERS lcPedStamp3 
	LOCAL lcSendMsg, lcResp, lcResposta, lcStatusRobot
	STORE .f. TO lcResposta, lcStatusRobot
	
	lcSendMsg = uf_robot_servico_header("stock", .f., lcPedStamp3)
	
	lcSendMsg = lcSendMsg + '"payload": {'
	lcSendMsg = lcSendMsg + '"numberOfRequestingLocation": "'+RIGHT('00'+ALLTRIM(STR(myTermNo)),3)+'",'
	lcSendMsg = lcSendMsg + '"productCode" : "0000000"'
	lcSendMsg = lcSendMsg + '} } }'
	
	lcResp = uf_robot_servico_send(lcSendMsg , myServRobotTOinv, "stock")
	
	RETURN lcResp 

ENDFUNC 


FUNCTION uf_robot_servico_retrieve_single
	LOCAL lcRetriveStamp 
	lcRetriveStamp = uf_gerais_stamp()
		
	if(uf_robot_servico_terminal_mapeado())				
		uf_robot_servico_retrieve(lcRetriveStamp,.t.)			
	endif
ENDFUNC


PROCEDURE uf_robot_servico_retrieve_line
	
	LOCAL lcorderNumber, lcBocaRobot 
		
		if(uf_robot_servico_terminal_mapeado() and USED("uCrsDispRobot"))
			
			LOCAL lcSQL, lcBocaRobot, lcSendMsgHeader, lcSendMsgHeader
			STORE '' TO lcSQL, lcSendMsgHeader	
			
			fecha("ucrsbocarobot")
			
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				SELECT drop_location FROM b_terminal(nolock) WHERE no=<<myTermNo>>
			ENDTEXT
			IF !uf_gerais_actGrelha("","ucrsbocarobot",lcSQL)
				uf_perguntalt_chama("ERRO AO PESQUISAR A BOCA DO ROBOT PARA DISPENSAR","OK","",48)
				regua(2)
				RETURN .f.
			ENDIF
	
			SELECT ucrsbocarobot
			lcBocaRobot = ucrsbocarobot.drop_location

			fecha("ucrsbocarobot")
			
			
			SELECT uCrsDispRobot
			GO TOP
			SCAN
				LOCAL lcRef, lcQtt
				lcRef = ALLTRIM(uCrsDispRobot.ref)
				lcQtt= uCrsDispRobot.qtt
				
				regua(1,2,"A DISPENSAR O ARTIGO: " + lcRef )	
				
				LOCAL lcRetriveStamp, lcorderNumber
				lcRetriveStamp = uf_gerais_stamp()
				lcSendMsg  = ''
				
				loDateTime = CreateObject('Chilkat_9_5_0.CkDateTime')
				uf_servicos_unblockChilkat()
				loDateTime.SetFromCurrentSystemTime()	
				
				
				
				lnBLocal = 1
				lcorderNumber  = uf_robot_servico_getOrderNumber()
							
				lcSendMsg = uf_robot_servico_header("retrieve", .t., lcRetriveStamp)
				
				lcSendMsg = lcSendMsg + '"payload": {'
				lcSendMsg = lcSendMsg + '"orderNumber": "'+ALLTRIM((lcorderNumber))+'",'
				lcSendMsg = lcSendMsg + '"numberOfRequestingLocation": "'+RIGHT('00'+ALLTRIM(STR(myTermNo)),3)+'",'
				lcSendMsg = lcSendMsg + '"deliveryLocation": "'+ALLTRIM((lcBocaRobot))+'",'
				lcSendMsg = lcSendMsg + '"priority": 1,'			
				lcSendMsg = lcSendMsg + '"products": ['
				lcSendMsg = lcSendMsg + '{'
				lcSendMsg = lcSendMsg + '"productCode" : "'+ALLTRIM(lcRef)+'",'
				lcSendMsg = lcSendMsg + '"quantity" : '+ALLTRIM(STR(lcQtt))+','
				lcSendMsg = lcSendMsg + '"deleteStoragePlace" : '+IIF(myServRobotDeleteStorPl = .t., 'true', 'false')+''
				lcSendMsg = lcSendMsg + '}'
				lcSendMsg = lcSendMsg + ']'
				lcSendMsg = lcSendMsg + '}}}'
				
				lcResp = uf_robot_servico_send(lcSendMsg , myServRobotTOret, "retrieve")
				
				wait timeout 1 
				
				
				**se for vazio, erro interno do servidor(500)
				IF(EMPTY(lcResp))
					 lcResp = ''
				ENDIF
				
				regua(2)
				RELEASE loDateTime		
				
			SELECT uCrsDispRobot	
			ENDSCAN
			
		ENDIF
ENDPROC



FUNCTION uf_robot_servico_terminal_mapeado
	LOCAL lcTermMap
	
	lcTermMap = .f.

	if(USED("ucrsbocarobotTemp"))
		fecha("ucrsbocarobotTemp")
	ENDIF
		

	TEXT TO lcSQL NOSHOW TEXTMERGE 
		SELECT COUNT(drop_location) as map FROM b_terminal(nolock) WHERE no=<<myTermNo>> AND drop_location>0
	ENDTEXT
	IF !uf_gerais_actGrelha("","ucrsbocarobotTemp",lcSQL)
		lcTermMap =.f.
	ELSE
		IF(ucrsbocarobotTemp.map>0)
			lcTermMap =.t.	
		ELSE
			lcTermMap =.f.	
		ENDIF
				
	ENDIF
	

	
	if(USED("ucrsbocarobotTemp"))
		fecha("ucrsbocarobotTemp")
	ENDIF
		
	RETURN lcTermMap 

ENDFUNC

FUNCTION uf_robot_servico_marcaComoEnviadoRobotFi


	
	SELECT ucrsFiRobot
	GO TOP
	SCAN
		SELECT fi
		GO TOP 
		LOCATE FOR ALLTRIM(fi.ref) == ALLTRIM(ucrsFiRobot.ref)
		if(FOUND())
			UPDATE fi SET fi.u_robot='R', fi.qttrobot = fi.qtt WHERE ALLTRIM(fi.ref) == ALLTRIM(ucrsFiRobot.ref)  AND EMPTY(fi.ofistamp)  AND fi.num1>0
			TABLEUPDATE(.t.)
		ENDIF
		
	SELECT ucrsFiRobot
	ENDSCAN

ENDFUNC




FUNCTION uf_robot_servicos_processInventario
	LPARAMETERS lcPedStampStRobot
	
	LOCAL lcRobotstatus

	
	regua(0,2,"A consultar o invent�rio do robot. Por favor aguarde")

	lcRobotstatus = uf_robot_servico_inventory(lcPedStampStRobot)


	IF VAL(lcRobotstatus) = 200
		LOCAL lcFimInv, LclCounter
		STORE .f. TO lcFimInv
		LOCAL lcCont, lcCont2
		lcCont = myServRobotTOinv*2
		lcCont2 = 1
		LclCounter = myServRobotTOinv
		DO WHILE lcFimInv = .f.
			regua(1,lcCont2,"A IMPORTAR O INVENT�RIO DO ROBOT...")
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				select stamp from robot_response_payload_stock(nolock)
				where token='<<ALLTRIM(lcPedStampStRobot)>>' and lastrequest=1
			ENDTEXT

			IF !uf_gerais_actGrelha("","ucrsresposta",lcSQL)
				regua(2)
				uf_perguntalt_chama("ERRO AO PESQUISAR RESULTADO DO FIM DE INVENT�RIO ROBOTICO","OK","",48)
				RETURN .f.
			ENDIF
			
			IF RECCOUNT("ucrsresposta")>0
				lcFimInv = .t.
			ENDIF 
			LclCounter = LclCounter - 0.5
			
			IF LclCounter = 0 
				lcFimInv = .t.
			ENDIF 

			WAIT "" TIMEOUT 1
			lcCont2 = lcCont2 + 1
			fecha("ucrsresposta")
		ENDDO
		
			
		IF LclCounter = 0 
			uf_perguntalt_chama("O PEDIDO DE INVENT�RIO N�O FOI CONCLUIDO NO TEMPO PARAMETRIZADO. POR FAVOR VERIFIQUE!","OK","",48)
			regua(2)
			RETURN .f.
		ENDIF
			
	
	ELSE
		uf_perguntalt_chama("O Logitools n�o obteve resposta do equipamento, por favor volte a tentar. Obrigado","OK","",16)	
		regua(2)	
		RETURN .f.
	ENDIF
		
	regua(2)
	RETURN .t.

ENDFUNC


FUNCTION uf_robot_servico_dispensaArmazem()
	LPARAMETERS lcTipo
		
		** apenas se o terminal estiver mapeado com o robot
		IF(uf_robot_servico_terminal_mapeado())		
			uf_robot_dispensaArmazem(ALLTRIM(lcTipo))
		ELSE
			RETURN .f.	
		ENDIF
		
	

ENDFUNC

FUNCTION uf_robot_servico_getOrderNumber	
		LOCAL lcorderNumber 
		lcorderNumber  = ""
	
		if(ALLTRIM(uf_gerais_getParameter_site('ADM0000000033', 'TEXT', mySite)) == "kardex") and USED("uCrsUser")
					
			LOCAL lcUserName
			SELECT uCrsUser
			GO TOP
			
			lcUserName = ALLTRIM(uCrsUser.nome)
			lcorderNumber = RIGHT(ALLTRIM(loDateTime.GetAsUnixTimeStr(lnBLocal)),8) 	+ "-" + ALLTRIM(lcUserName) 
		ELSE
			lcorderNumber = RIGHT(ALLTRIM(loDateTime.GetAsUnixTimeStr(lnBLocal)),8)
		ENDIF
		
		RETURN  lcorderNumber 

ENDFUNC




FUNCTION uf_robot_servico_retrieve
	PARAMETERS lcPedStamp3, lcSingleDispense
		
	
	IF EMPTY(lcPedStamp3)
		lcPedStamp3 = uf_gerais_stamp()
	ENDIF 
	
	IF(EMPTY(lcSingleDispense))
		&& dispensa todas as linhas 	
		SELECT fi
		GO TOP
		SELECT ref, SUM(qtt) as qtt, 0 as qttdisp FROM fi WHERE !empty(ALLTRIM(fi.ref)) AND EMPTY(fi.ofistamp) AND EMPTY(fi.u_robot) AND fi.num1>0 GROUP BY fi.ref INTO CURSOR ucrsFiRobot READWRITE 
	ELSE
		&& dispensa apenas uma linha (seleccionada)
		LOCAL lcFiStamp
		SELECT fi
		lcFiStamp = ALLTRIM(fi.fistamp)	
		SELECT ref,  qtt, 0 as qttdisp FROM fi WHERE fi.fistamp = lcFiStamp  and !empty(ALLTRIM(fi.ref))  AND EMPTY(fi.ofistamp) AND fi.num1>0 INTO CURSOR ucrsFiRobot READWRITE 	
	ENDIF


	IF RECCOUNT("ucrsFiRobot") > 0
				
	
		DO CASE 
			CASE RECCOUNT("ucrsFiRobot") > 0 AND RECCOUNT("ucrsFiRobot") <= 10
		
				regua(0,2,"A DISPENSAR OS MEDICAMENTOS NO ROBOT...")
				
				CREATE CURSOR ucrsRespRobotLin (ref c(20), qtt n(10,0), qttfi n(10,0))
			
				LOCAL lcSendMsg, lcResp, lcResposta, lcStatusRobot, lcBocaRobot, lcNrLinRobot, lcNrLin
				STORE .f. TO lcResposta, lcStatusRobot
				STORE 0 TO lcNrLinRobot, lcNrLin
				
				TEXT TO lcSQL NOSHOW TEXTMERGE 
					SELECT drop_location FROM b_terminal(nolock) WHERE no=<<myTermNo>>
				ENDTEXT
				IF !uf_gerais_actGrelha("","ucrsbocarobot",lcSQL)
					uf_perguntalt_chama("ERRO AO PESQUISAR A BOCA DO ROBOT PARA DISPENSAR","OK","",48)
					regua(2)
					RETURN .f.
				ENDIF
				lcBocaRobot = ucrsbocarobot.drop_location
				fecha("ucrsbocarobot")
				
				loDateTime = CreateObject('Chilkat_9_5_0.CkDateTime')
				uf_servicos_unblockChilkat()
				loDateTime.SetFromCurrentSystemTime()

				lnBLocal = 1
				
				LOCAL lcorderNumber 			
				lcorderNumber  = uf_robot_servico_getOrderNumber()

				
				RELEASE loDateTime
				
			
				
				lcSendMsg = uf_robot_servico_header("retrieve", .t., lcPedStamp3)
				
				lcSendMsg = lcSendMsg + '"payload": {'
				lcSendMsg = lcSendMsg + '"orderNumber": "'+ALLTRIM((lcorderNumber))+'",'
				lcSendMsg = lcSendMsg + '"numberOfRequestingLocation": "'+RIGHT('00'+ALLTRIM(STR(myTermNo)),3)+'",'
				lcSendMsg = lcSendMsg + '"deliveryLocation": "'+ALLTRIM((lcBocaRobot))+'",'
				lcSendMsg = lcSendMsg + '"priority": 1,'
				lcSendMsg = lcSendMsg + '"products": ['
				
				lcNrLin=1
				lcNrLinRobot = RECCOUNT("ucrsFiRobot")
				SELECT ucrsFiRobot
				GO TOP 
				SCAN 
					lcSendMsg = lcSendMsg + '{'
					lcSendMsg = lcSendMsg + '"productCode" : "'+ALLTRIM(ucrsFiRobot.ref)+'",'
					lcSendMsg = lcSendMsg + '"quantity" : '+ALLTRIM(STR(ucrsFiRobot.qtt))+','
					lcSendMsg = lcSendMsg + '"deleteStoragePlace" : '+IIF(myServRobotDeleteStorPl = .t., 'true', 'false')+''
					lcSendMsg = lcSendMsg + '}'+IIF(lcNrLinRobot = lcNrLin, '',',')
					lcNrLin = lcNrLin + 1
				ENDSCAN 
				lcSendMsg = lcSendMsg + '] } } }'
				
				lcResp = uf_robot_servico_send(lcSendMsg , myServRobotTOret, "retrieve")
				regua(1,2,"A DISPENSAR OS ARTIGOS DISPON�VEIS...")
				wait timeout 1 
				
				
				**se for vazio, erro interno do servidor(500)
				IF(EMPTY(lcResp))
					 lcResp = ''
				ENDIF
				
				
				IF ALLTRIM(lcResp)='200'

					** valida se espera e processa a resposta de dispensa do robot
					if(uf_gerais_getParameter_site('ADM0000000070', 'BOOL', mySite))
						** processa resposta a dispense order 04
						LOCAL lcrefr, lcQttr, lcQttfir
						STORE '' TO lcrefr
						STORE 0 TO lcQttr, lcQttfir
						
						SELECT ucrsRespRobotLin
						GO TOP 
						SCAN 
							lcrefr = ALLTRIM(ucrsRespRobotLin.ref)
							lcQttr = ucrsRespRobotLin.qtt
							lcQttfir = ucrsRespRobotLin.qttfi
							
							SELECT fi
							GO TOP 
							SCAN
								IF ALLTRIM(fi.ref) == ALLTRIM(lcrefr)
									IF lcQttr - lcQttfir > 0
										replace fi.u_robot WITH 'R'
										IF fi.qtt <= (lcQttr - lcQttfir)
											replace fi.qttrobot WITH fi.qtt
										ELSE
											replace fi.qttrobot WITH lcQttr - lcQttfir
										ENDIF 
									ENDIF
									lcQttfir = lcQttfir + fi.qtt 
								ENDIF 
							ENDSCAN 
							
							SELECT ucrsRespRobotLin
						ENDSCAN 
						
					ELSE
						** ** processa resposta a dispense order 01
						uf_robot_servico_marcaComoEnviadoRobotFi()					
					ENDIF
					
				ELSE 					
					regua(2)
					RETURN .f.
				ENDIF 
			
			CASE RECCOUNT("ucrsFiRobot") > 10 
				
		
				regua(0,2,"A DISPENSAR OS MEDICAMENTOS NO ROBOT...")
				
				CREATE CURSOR ucrsRespRobotLin (ref c(20), qtt n(10,0), qttfi n(10,0))
			
				LOCAL lcSendMsg, lcResp, lcResposta, lcStatusRobot, lcBocaRobot, lcNrLinRobot, lcNrLin, lcNrLinAux
				STORE .f. TO lcResposta, lcStatusRobot
				STORE 0 TO lcNrLinRobot, lcNrLin
				
				TEXT TO lcSQL NOSHOW TEXTMERGE 
					SELECT drop_location FROM b_terminal(NOLOCK) WHERE no=<<myTermNo>>
				ENDTEXT
				IF !uf_gerais_actGrelha("","ucrsbocarobot",lcSQL)
					uf_perguntalt_chama("ERRO AO PESQUISAR A BOCA DO ROBOT PARA DISPENSAR","OK","",48)
					regua(2)
					RETURN .f.
				ENDIF
				lcBocaRobot = ucrsbocarobot.drop_location
				fecha("ucrsbocarobot")
				
				SELECT ucrsFiRobot 
				SELECT * FROM ucrsFiRobot INTO CURSOR ucrsFiRobotAuxTot READWRITE 
				SELECT * FROM ucrsFiRobot INTO CURSOR ucrsFiRobotAux READWRITE 
				SELECT ucrsFiRobotAux 
				COUNT FOR !DELETED() TO lcNrLinAux
				
				DO WHILE lcNrLinAux <> 0 
					fecha("ucrsFiRobot")
					SELECT TOP 10 * FROM ucrsFiRobotAux ORDER BY ucrsFiRobotAux.ref INTO CURSOR ucrsFiRobot READWRITE 

					
					loDateTime = CreateObject('Chilkat_9_5_0.CkDateTime')
					uf_servicos_unblockChilkat()
					loDateTime.SetFromCurrentSystemTime()

					lnBLocal = 1
					
					
					LOCAL lcorderNumber 			
					lcorderNumber  = uf_robot_servico_getOrderNumber()
					
					
					RELEASE loDateTime
					
					lcSendMsg = uf_robot_servico_header("retrieve", .t., lcPedStamp3)
					
					lcSendMsg = lcSendMsg + '"payload": {'
					lcSendMsg = lcSendMsg + '"orderNumber": "'+ALLTRIM((lcorderNumber))+'",'
					lcSendMsg = lcSendMsg + '"numberOfRequestingLocation": "'+RIGHT('00'+ALLTRIM(STR(myTermNo)),3)+'",'
					lcSendMsg = lcSendMsg + '"deliveryLocation": "'+ALLTRIM((lcBocaRobot))+'",'
					lcSendMsg = lcSendMsg + '"priority": 1,'
					lcSendMsg = lcSendMsg + '"products": ['
					
					lcNrLin=1
					lcNrLinRobot = RECCOUNT("ucrsFiRobot")
					SELECT ucrsFiRobot
					GO TOP 
					SCAN 
						lcSendMsg = lcSendMsg + '{'
						lcSendMsg = lcSendMsg + '"productCode" : "'+ALLTRIM(ucrsFiRobot.ref)+'",'
						lcSendMsg = lcSendMsg + '"quantity" : '+ALLTRIM(STR(ucrsFiRobot.qtt))+','
						lcSendMsg = lcSendMsg + '"deleteStoragePlace" : '+IIF(myServRobotDeleteStorPl = .t., 'true', 'false')+''
						lcSendMsg = lcSendMsg + '}'+IIF(lcNrLinRobot = lcNrLin, '',',')
						lcNrLin = lcNrLin + 1
					ENDSCAN 
					lcSendMsg = lcSendMsg + '] } } }'
					
					lcResp = uf_robot_servico_send(lcSendMsg , myServRobotTOinv, "retrieve")
			**		MESSAGEBOX(lcResp)
					regua(1,2,"A DISPENSAR OS ARTIGOS DISPON�VEIS...")
					wait timeout 1 
					
					**se for vazio, erro interno do servidor(500)
					IF(EMPTY(lcResp))
						 lcResp = ''
					ENDIF
					
					
					IF ALLTRIM(lcResp)='200'
					

						if(uf_gerais_getParameter_site('ADM0000000070', 'BOOL', mySite))
							** processa resposta a dispense order 04
							LOCAL lcrefr, lcQttr, lcQttfir
							STORE '' TO lcrefr
							STORE 0 TO lcQttr, lcQttfir
							
							SELECT ucrsRespRobotLin
							GO TOP 
							SCAN 
								lcrefr = ALLTRIM(ucrsRespRobotLin.ref)
								lcQttr = ucrsRespRobotLin.qtt
								lcQttfir = ucrsRespRobotLin.qttfi
								
								SELECT fi
								GO TOP 
								SCAN
									IF ALLTRIM(fi.ref) == ALLTRIM(lcrefr)
										IF lcQttr - lcQttfir > 0
											replace fi.u_robot WITH 'R'
											IF fi.qtt <= (lcQttr - lcQttfir)
												replace fi.qttrobot WITH fi.qtt
											ELSE
												replace fi.qttrobot WITH lcQttr - lcQttfir
											ENDIF 
										ENDIF
										lcQttfir = lcQttfir + fi.qtt 
									ENDIF 
								ENDSCAN 
								
								SELECT ucrsRespRobotLin
							ENDSCAN 
							
						ELSE
							** ** processa resposta a dispense order 01
							uf_robot_servico_marcaComoEnviadoRobotFi()
						ENDIF
							
					ELSE 
						uf_perguntalt_chama("LIMITE DE TEMPO DE ESPERA DE RESPOSTA DO ROBOT ULTRAPASSADO!","OK","",48)
						regua(2)
						RETURN .f.
					ENDIF 
					SELECT ucrsFiRobotAux 
					GO TOP 
					DELETE NEXT 10 
					COUNT FOR !DELETED() TO lcNrLinAux	
				ENDDO 
				
			OTHERWISE 	
			
		ENDCASE 
		regua(1,2,"DISPENSA CONCLU�DA...")
		wait timeout 1 
		fecha("ucrsFiRobot")
		fecha("ucrsRespRobotLin")
		regua(2)
		RETURN lcResp 
	
	ELSE
	
		** nao existem medicamentos para dispensar
		RETURN "40004"
		
		
	ENDIF 

ENDFUNC 

FUNCTION uf_imprimir_nao_dispensados_robot
	
	&& PREPARAR DEFINI��ES GERAIS DA IMPRESSORA
	lcValidaImp = uf_gerais_setImpressoraPOS(.t.)
	
	IF lcValidaImp
	*** INICIALIZAR A IMPRESSORA ***
	???	chr(027)+chr(064)
	*** ESCOLHER A IMPRESSORA ***
	???	chr(27)+chr(99)+chr(48)+chr(1)
	*** DEFINIR C�DIGO DE CARACTERES PORTUGU�S ***
	???	chr(027)+chr(116)+chr(003)
	*** ESCOLHER MODOS DE IMPRESS�O ***
	???	chr(27)+chr(33)+chr(1)
					
					
	****** IMPRESS�O DO CABE�ALHO ******
	*** NOME DA EMPRESA ***
	??	" "
	?	LEFT(ALLTRIM(uCrsE1.NOMECOMP),uf_gerais_getParameter("ADM0000000194","NUM"))

	uf_gerais_separadorPOS()
	?	PADC("MEDICAMENTOS NAO DISPENSADOS PELO ROBOT",uf_gerais_getParameter("ADM0000000194","NUM")," ")
	uf_gerais_separadorPOS()

	?	datetime()

	SELECT Ft
	?"Op: "
	??Substr(Ft.vendnm,1,40) + " (" + Alltrim(Str(Ft.vendedor)) + ")" 

	?	" "
	?	"Cliente: "+ ALLTRIM(ft.nome)
	?	"Nr.: "+ Astr(ft.no)
	
	uf_gerais_separadorPOS()

	****** IMPRESS�O DAS LINHAS ******
	SELECT ucrsNdispRobot
	GO TOP
	SCAN 
		?	"Prod: "+ ALLTRIM(ucrsNdispRobot.ref) + " - " + ALLTRIM(ucrsNdispRobot.design)
		?	"Qtt: " + ALLTRIM(Transform(ucrsNdispRobot.qtt,"999")) + " | Disp. Robot: " + ALLTRIM(Transform(ucrsNdispRobot.qttrobot,"999")) + " | Em falta: "+ALLTRIM(Transform(ucrsNdispRobot.emfalta,"999"))
		uf_gerais_separadorPOS()
	ENDSCAN
		

	****** IMPRESS�O DO RODAP� ******
												
	uf_gerais_feedCutPOS()
	ENDIF
	uf_gerais_setImpressoraPOS(.f.)

	SET PRINTER TO DEFAULT
	
	** limpar cursores e vari�veis publicas **

ENDFUNC



