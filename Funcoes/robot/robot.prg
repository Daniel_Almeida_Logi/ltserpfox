** Carrega APPs
DO MAPAROBOT
DO ROBOT_SERVICO

*********************************************************
** Fun��o geral para dispensar produtos do atendimento **
*********************************************************
FUNCTION uf_robot_dispensaFrontBulk
	LPARAMETERS tcBool, tcRobo
	
	IF EMPTY(tcRobo)
		tcRobo=1
	ENDIF
	
	DO CASE
		CASE myUsaRobot OR myUsaRobotApostore
		
			uf_robot_dispensaFrontBulkTl(tcBool)
			
		CASE myUsaFarmax
			
			uf_robot_dispensaFrontBulkFm()
			
		CASE myUsaRobotConsis && uf_gerais_getParameter('ADM0000000221','bool')
			
			uf_robot_dispensaFrontBulkConsis()
			
		OTHERWISE
			**
	ENDCASE

ENDFUNC


*** ROBOT Tecnilab ***
FUNCTION uf_robot_TimerTecnilab
	myTimer = myTimer + 3000

	IF myTimer>=120000
		uf_robot_setTimerRobot(.f.)
		RETURN .f.
	ENDIF
	
	TEXT TO lcSql NOSHOW textmerge
		exec up_robot_respostaA '<<astr(myTermNo)>>', <<myNrProd>>, <<IIF(myUsaRobotApostore,1,0)>>
	ENDTEXT

	IF !uf_gerais_actGrelha("","uCrsVerificaDispensa",lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia a verificar o stock no Robot. Por favor contacte o Suporte.","OK","",16)
		uf_robot_setTimerRobot(.f.)
		RETURN
	ELSE
		IF RECCOUNT("uCrsVerificaDispensa")>0
			SELECT uCrsVerificaDispensa
			GO TOP
			SCAN
				IF uCrsVerificaDispensa.stock > 0
					LOCAL lcPos
					
					SELECT fi
					lcPos = RECNO()
					
					SELECT fi
					GO TOP
					SCAN FOR ALLTRIM(fi.ref)==ALLTRIM(uCrsVerificaDispensa.ref) AND EMPTY(fi.ofistamp) AND !(ALLTRIM(fi.u_robot)=="R")
						replace fi.u_robot WITH 'R' IN fi						
						** transferir de stock **
				*		IF uCrsVerificaDispensa.stock>0
				*			uf_trfEntreArmazensRobot(tcRef, uCrsVerificaDispensa.stock, m.ch_vendedor, m.ch_vendnm, 1) && desactivo ate integra��o do robot back-office
				*		ENDIF
						SELECT fi
					ENDSCAN
					
					SELECT fi
					IF lcPos>0
						TRY
							GO lcPos
						CATCH
						ENDTRY
					ENDIF
				ENDIF
				SELECT uCrsVerificaDispensa
			ENDSCAN
				
			fecha("uCrsVerificaDispensa")
			
			** apagar registos **
			IF !emdesenvolvimento
				uf_gerais_actGrelha("","",[exec up_robot_delTable ']+astr(myTermNo)+['])
			ENDIF
			
			uf_robot_setTimerRobot(.f.)
		ENDIF
	ENDIF
ENDFUNC


** && n�o esta a ser utilizada
FUNCTION uf_robot_StarttecnilabRobot 
	LPARAMETERS tcPath
	
	DECLARE INTEGER ShellExecute IN shell32.dll ;
	INTEGER hndWin, ;
	STRING cAction, ;
	STRING cFileName, ;
	STRING cParams, ;
	STRING cDir, ;
	INTEGER nShowWin

	cFileName = ALLTRIM(tcPath)+"\externos\tecnilab\Robot_Server_Thread.jar"
	cAction = "open"
	ShellExecute(0,cAction,cFileName,"","",1)
ENDFUNC


** Valida estado do robot da tecnilab (parte 1)
FUNCTION uf_robot_validatecnilabRobot
	STORE '000' TO myDropLocal

	IF myUsaRobot OR myUsaRobotApostore
		private lcRobotState
		lcRobotState	= '99'

		lcRobotState	= uf_robot_validaTecnilabRobotState()

		Do case
			Case lcRobotState = '00'
				myRobotState=.t.
			CASE lcRobotState = '02'
				myRobotState=.t.
			Otherwise
				myRobotState=.f.
		Endcase
	ENDIF
ENDFUNC


** Valida estado do robot da tecnilab (parte 1)
FUNCTION uf_robot_validatecnilabRobotState
		
	IF !myUsaRobot AND !myUsaRobotApostore
		uf_perguntalt_chama("N�O TEM O ROB� PARAMETRIZADO.","OK","",48)
		RETURN ''
	ENDIF

	LOCAL lcState
	STORE '' TO lcState
	
	IF !empty(myPath)
		uf_robot_tecnilabMsgS(myPath) && pedir estado do robot
	ELSE
		uf_perguntalt_chama("N�O EST� DEFINIDO O CAMINHO PARA O EXECUT�VEL DO ROB�.","OK","",16)
		RETURN '01'
	ENDIF

	** Ler resposta do robot **
	TEXT TO lcSql NOSHOW textmerge
		exec up_robot_respostaS '<<astr(myTermNo)>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("","uCrsMachineState",lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A VALIDAR ESTADO DO ROB�.","OK","",16)
		RETURN '01'
	ELSE
		lcState = uCrsMachineState.machine_state
		
		IF !emdesenvolvimento		
			uf_gerais_actGrelha("","",[exec up_robot_delTable ']+astr(myTermNo)+['])
		ENDIF
		
		fecha("uCrsMachineState")
	ENDIF
	
	RETURN lcState
ENDFUNC


**
FUNCTION uf_robot_dispensaFront && n�o esta a ser utilizada
	IF !myUsaRobot AND !myUsaRobotApostore
		uf_perguntalt_chama("N�O TEM O ROB� PARAMETRIZADO.","OK","",48)
		RETURN
	ELSE
		** Verificar se o produto j� foi pedido **
		SELECT fi
		IF fi.u_robot=='R'
			uf_perguntalt_chama("ESTE PRODUTO J� FOI PEDIDO AO ROB�, N�O PODE PEDIR NOVAMENTE.","OK","",64)
			RETURN
		ENDIF
		
		IF myRobotState = .f.
			LOCAL lcRobotState
			lcRobotState = uf_robot_validatecnilabRobotState()
			
			IF lcRobotState='06'
				uf_perguntalt_chama("O ROB� DE MOMENTO EST� OCUPADO. TENTE NOVAMENTE DENTRO DE ALGUNS SEGUNDOS.","OK","",64)
				RETURN
			ENDIF
			IF lcRobotState='01' OR (lcRobotState!='00' AND lcRobotState!='02')
				uf_perguntalt_chama("O ROB� EST� COM O ESTADO: N�O OK.","OK","",48)
				RETURN
			ENDIF
		ENDIF
	ENDIF
	
	** verificar se existe stock **
	SELECT fi
	uf_robot_tecnilabMsgB(fi.ref)
	
	TEXT TO lcSql NOSHOW textmerge
		exec up_robot_respostaB '<<astr(myTermNo)>>', <<IIF(myUsaRobotApostore,1,0)>>
	ENDTEXT
	IF !uf_gerais_actGrelha("","uCrsVerificaDispStock",lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A VALIDAR STOCK NO ROB�.","OK","",16)
		RETURN '0'
	ELSE
		IF RECCOUNT()>0
			LOCAL lcStock
			lcStock = uCrsVerificaDispStock.stock
		
			IF !emdesenvolvimento
				uf_gerais_actGrelha("","",[exec up_robot_delTable ']+astr(myTermNo)+['])
			ENDIF
			
			fecha("uCrsVerificaDispStock")
		
			IF lcStock=0
				uf_perguntalt_chama("ESTE PRODUTO N�O TEM STOCK NO ROB�.","OK","",64)
			ELSE
				SELECT fi
				IF lcStock < fi.qtt
					IF uf_perguntalt_chama("O ROB� N�O TEM STOCK SUFICIENTE."+CHR(10)+"STOCK PEDIDO = "+ASTR(FI.QTT)+" STOCK DISPON�VEL = "+ASTR(lcStock)+CHR(10)+"DESEJA DUPLICAR A LINHA?","Sim","N�o")
						** Verificar novamente o stock no robot **
						LOCAL lcStock2
						STORE 0 TO lcStock2
						
						SELECT fi
						uf_robot_tecnilabMsgB(fi.ref)
	
						TEXT TO lcSql NOSHOW textmerge
							exec up_robot_respostaB '<<astr(myTermNo)>>', <<IIF(myUsaRobotApostore,1,0)>>
						ENDTEXT
						IF !uf_gerais_actGrelha("","uCrsVerificaDispStock",lcSql)
							uf_perguntalt_chama("OCORREU UM ERRO A VALIDAR STOCK NO ROB�.","OK","",16)
							RETURN '0'
						ELSE
							IF RECCOUNT()>0
								LOCAL lcStock2
								lcStock2 = uCrsVerificaDispStock.stock
								
								IF !emdesenvolvimento
									uf_gerais_actGrelha("","",[exec up_robot_delTable ']+astr(myTermNo)+['])
								ENDIF
							ENDIF
							fecha("uCrsVerificaDispStock")
						ENDIF
						******************************************
						
						IF lcStock==lcStock2
							LOCAL splitRef, lcQtAct
							SELECT fi
							slipRef	= fi.ref
							lcQtAct	= fi.qtt
							
							** Tratar Linha actual **
							SELECT fi
							replace fi.qtt 		WITH lcStock
							uf_atendimento_eventoFi()
							
							** Pedir produto ao robot **
							SELECT fi
							uf_robot_tecnilabMsgA(fi.ref, fi.qtt)
							
							** Validar que o robot devolveu quantidade > 0 
							TEXT TO lcSql NOSHOW textmerge
								exec up_robot_respostaA '<<astr(myTermNo)>>', 1, <<IIF(myUsaRobotApostore,1,0)>>
							ENDTEXT
							IF !uf_gerais_actGrelha("","uCrsVerificaDispensa",lcSql)
								uf_perguntalt_chama("OCORREU UM ERRO A VALIDAR O STOCK NO ROB�.","OK","",16)
							ELSE
								IF RECCOUNT()>0
									SELECT uCrsVerificaDispensa
									IF uCrsVerificaDispensa.stock > 0
										replace fi.u_robot	WITH 'R'
									ENDIF
									
									FECHA("uCrsVerificaDispensa")
									
									IF !emdesenvolvimento
										uf_gerais_actGrelha("","",[exec up_robot_delTable ']+astr(myTermNo)+['])
									ENDIF
							
									** Tratar Linha duplicada **
									uf_atendimento_AdicionaLinhaFi()
									SELECT fi
									replace fi.ref 		WITH ALLTRIM(slipRef)
									
									uf_atendimento_actRef()
									SELECT fi
									replace fi.qtt		WITH lcQtAct-lcStock
									replace u_robot		WITH 'P'
									
									uf_atendimento_eventoFi()
									SELECT fi
									****************************
								ELSE
									uf_perguntalt_chama("O ROB� N�O DEVOLVEU NENHUM PRODUTO."+CHR(10)+"AGUARDE UNS SEGUNDOS E TENTE NOVAMENTE.","OK","",64)
								ENDIF
							ENDIF
							*************************************************
						ELSE
							uf_perguntalt_chama("O STOCK DO PRODUTO NO ROB� FOI ALTERADO."+CHR(10)+"PROVAVELMENTE FOI PEDIDO POR OUTRO TERMINAL DURANTE ESTE PERIODO."+CHR(10)+"POR FAVOR TENTE NOVAMENTE.","OK","",48)
						ENDIF
					ENDIF
				ELSE
					** pedir produto ao robot **
					SELECT fi
					uf_robot_tecnilabMsgA(fi.ref, fi.qtt)

					** Validar que o robot devolveu quantidade > 0
					TEXT TO lcSql NOSHOW textmerge
						exec up_robot_respostaA '<<astr(myTermNo)>>', 1, <<IIF(myUsaRobotApostore,1,0)>>
					ENDTEXT
					IF !uf_gerais_actGrelha("","uCrsVerificaDispensa",lcSql)
						uf_perguntalt_chama("OCORREU UM ERRO A VALIDAR O STOCK NO ROB�.","OK","",16)
					ELSE
						IF RECCOUNT()>0
							SELECT uCrsVerificaDispensa
							IF uCrsVerificaDispensa.stock > 0
								replace fi.u_robot	WITH 'R'
							ENDIF
							
							FECHA("uCrsVerificaDispensa")
							
							IF !emdesenvolvimento
								uf_gerais_actGrelha("","",[exec up_robot_delTable ']+astr(myTermNo)+['])
							ENDIF
						ELSE
							uf_perguntalt_chama("O ROB� N�O DEVOLVEU NENHUM PRODUTO."+CHR(10)+"AGUARDE UNS SEGUNDOS E TENTE NOVAMENTE.","OK","",48)
						ENDIF
					ENDIF
					****************************
				ENDIF
								
				** transferir de stock **
				*uf_trfEntreArmazensRobot(fi.ref, uCrsVerificaDispensa.stock, m.ch_vendedor, m.ch_vendnm, 1) && desactivo ate se integrar back-office Robot
			ENDIF
		ELSE
			uf_perguntalt_chama("O ROB� PARECE ESTAR OCUPADO. TENTE NOVAMENTE DAQUI A UNS SEGUNDOS.","OK","",48)
			RETURN
		ENDIF
	ENDIF
	
	IF !emdesenvolvimento
		uf_gerais_actGrelha("","",[exec up_robot_delTable ']+astr(myTermNo)+['])
	ENDIF
ENDFUNC



**
FUNCTION uf_robot_dispensaFrontBulkTl
	LPARAMETERS tcBool
	
	IF !myUsaRobot AND !myUsaRobotApostore
		RETURN .f.
	ELSE
		IF myRobotState=.f.
			LOCAL lcRobotState
			
			lcRobotState = uf_robot_validatecnilabRobotState()
			
			IF !(lcRobotState=='00') AND !(lcRobotState=='02')
				IF !tcBool
					RETURN
				ELSE
					uf_perguntalt_chama("O ROB� EST� COM O ESTADO: N�O OK.","OK","",48)
					RETURN
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	** Validar se existe algum pedido em curso **
	IF Type("Atendimento.tmrRobot") == "O"
		IF Atendimento.tmrRobot.enabled
			RETURN .f.
		ENDIF
	ENDIF
	*********************************************

	myNrProd = 0
	
	LOCAL lcQt, lcMsg, lcEspacos, lcEspacosQt, lcEspacosQtt, pcProdCode, lcProdCode2, lcSumQt, lcFiPos, lcRef2
	Store '' to lcMsg, lcEspacos, lcEspacosQt, lcEspacosQtt, pcProdCode, lcProdCode2, lcRef2
	Store 0 to lcSumQt, lcFiPos

	lcEspacos	= "========================="
	lcEspacosQt	= "00000"
	lcEspacosQtt = "00"

	SELECT fi
	lcFiPos = RECNO()
	SELECT fi
	GO TOP
	SCAN FOR !empty(fi.ref) AND EMPTY(fi.u_robot) AND EMPTY(fi.ofistamp)
		DO CASE
			CASE myUsaRobot
				lcProdCode	= "03904"+alltrim(fi.ref)
				lcQt 		= fi.qtt
				lcProdCode2 = lcProdCode2 + lcProdCode + LEFT(lcEspacos,LEN(lcEspacos)-LEN(lcProdCode)) + LEFT(lcEspacosQt,LEN(lcEspacosQt)-LEN(astr(lcQt)))+astr(lcQt) + "1"
				
			CASE myUsaRobotApostore
				lcRef2		= uf_robot_getCodIntRef(fi.ref, .f.)
				SELECT fi
				
				lcProdCode	= alltrim(lcRef2)
				lcQt 		= fi.qtt
				lcProdCode2 = lcProdCode2 + lcProdCode + LEFT(lcEspacosQt,LEN(lcEspacosQt)-LEN(astr(lcQt)))+astr(lcQt) + "1"
		ENDCASE
		
		myNrProd = myNrProd + 1
		lcSumQt = lcSumQt + fi.qtt
				
		** marcar o produto para caso o robo n�o envie o produto este n�o ser pedido novamente
		replace fi.u_robot WITH 'P'
	ENDSCAN

	IF myNrProd>99
		uf_perguntalt_chama("N�O � POSS�VEL PEDIR AO ROB� MAIS DO QUE 99 LINHAS DE PRODUTOS.","OK","",64)
		RETURN .f.
	ELSE
		IF myNrProd<1
			uf_perguntalt_chama("N�O EXISTEM PRODUTOS A PEDIR AO ROB�.","OK","",64)
			RETURN .f.
		ENDIF
	ENDIF
	
	** Escolher Prioridade ***
	LOCAL lcPriority
	lcPriority = 1
	IF uf_gerais_getParameter("ADM0000000067","NUM") > lcSumQt
		lcPriority = 1
	ELSE
		lcPriority = 3 && 3 ou 2? confirmar com tecnilab
	ENDIF
	**************************
	
	** definir terminal **
	LOCAL lcTerm
	lcTerm	= substr("000",1,len("000")-len(astr(myTermNo))) + astr(myTermNo)
	**********************
	
	SELECT fi
	IF lcFiPos>0
		TRY
			GO lcFiPos
		CATCH
			GO BOTTOM
		ENDTRY
	ENDIF
	
	DO CASE
		case myUsaRobot
			lcProdEnc	= left(lcEspacosQtt,len(lcEspacosQtt)-len(astr(myNrProd))) + astr(myNrProd)
			lcMsg 		= "A"+"10000"+ALLTRIM(lcTerm)+ALLTRIM(myDropLocal)+ALLTRIM(myDropLocal)+ astr(lcPriority) + lcProdEnc
			lcMsg		= lcMsg + lcProdCode2

		CASE myUsaRobotApostore
			** Ex: A | 10000001 | 001 | 101 | 1 | 01 | 8168617 | 00004 | 1
			lcProdEnc	= left(lcEspacosQtt,len(lcEspacosQtt)-len(astr(myNrProd))) + astr(myNrProd)
			lcMsg 		= "A"+"10000"+ALLTRIM(lcTerm)+ALLTRIM(lcTerm)+ALLTRIM(myDropLocal)+ astr(lcPriority) + lcProdEnc
			lcMsg		= lcMsg + lcProdCode2
	ENDCASE
	
	Local lcRpath

	** pedir produto ao robot **
	IF !tcBool
		DO CASE
			CASE myUsaRobot
				lcRpath	 = ALLTRIM(myPath)+'\externos\tecnilab\viriato.jar "' +lcMsg+ '"' + ' "' +astr(myTermNo)+ '"' + ' "0"'
		
			CASE myUsaRobotApostore
				lcRpath	 = ALLTRIM(myPath)+'\externos\robo\apostore\ApostoreClient.jar "' +lcMsg+ '"' + ' "' +astr(myTermNo)+ '"' + ' "0"'

		ENDCASE
		
		IF emdesenvolvimento &&debug mode
			uf_robot_Log(lcRpath, 'PHC: Robo', 'uf_robot_dispensaFrontBulkTl')
		ENDIF
		
		oWSShell = CREATEOBJECT("WScript.Shell")
		oWSShell.Run(lcRpath,1,.f.)
									
		uf_robot_setTimerRobot(.t.)
	ELSE
		DO CASE
			CASE myUsaRobot
				lcRpath	 = ALLTRIM(myPath)+'\externos\tecnilab\viriato.jar "' +lcMsg+ '"' + ' "' +astr(myTermNo)+ '"' + ' "1"'
			CASE myUsaRobotApostore
				lcRpath	 = ALLTRIM(myPath)+'\externos\robo\apostore\ApostoreClient.jar "' +lcMsg+ '"' + ' "' +astr(myTermNo)+ '"' + ' "1"'
		ENDCASE
		
		IF emdesenvolvimento &&debug mode
			uf_robot_Log(lcRpath, 'PHC: Robo', 'uf_robot_dispensaFrontBulkTl')
		ENDIF
		
		oWSShell = CREATEOBJECT("WScript.Shell")
		oWSShell.Run(lcRpath,1,.t.)
		
		TEXT TO lcSql NOSHOW TEXTMERGE
			exec up_robot_respostaA '<<astr(myTermNo)>>', <<myNrProd>>, <<IIF(myUsaRobotApostore,1,0)>>
		ENDTEXT
		IF !uf_gerais_actGrelha("","uCrsVerificaDispensa",lcSql)
			uf_perguntalt_chama("OCORREU UM ERRO A VALIDAR O STOCK NO ROB�.","OK","",16)
			RETURN '0'
		ELSE
			IF RECCOUNT()>0
				SELECT uCrsVerificaDispensa
				GO TOP
				SCAN
					IF uCrsVerificaDispensa.stock > 0 AND uCrsVerificaDispensa.stock < 999
						SELECT fi
						GO top
						SCAN FOR ALLTRIM(fi.ref)==ALLTRIM(uCrsVerificaDispensa.ref) AND EMPTY(fi.ofistamp) AND !(ALLTRIM(fi.u_robot)=='R')
							replace fi.u_robot WITH 'R' IN fi
							
							** transferir de stock **
					*		IF uCrsVerificaDispensa.stock>0
					*			uf_trfEntreArmazensRobot(tcRef, uCrsVerificaDispensa.stock, m.ch_vendedor, m.ch_vendnm, 1) && desactivo ate integra��o do robot back-office
					*		ENDIF
							SELECT fi
						ENDSCAN
					ENDIF
					SELECT uCrsVerificaDispensa
				ENDSCAN
					
				fecha("uCrsVerificaDispensa")
			ENDIF
		ENDIF
		
		** apagar registos **
		IF !emdesenvolvimento
			uf_gerais_actGrelha("","",[exec up_robot_delTable ']+astr(myTermNo)+['])
		ENDIF
		
		SELECT fi
		COUNT TO lcTotalRows
		IF lcFiPos>0 AND lcFiPos<=lcTotalRows
			TRY
				GO lcFiPos
			CATCH
			ENDTRY
		ELSE
			TRY
				GO lcTotalRows
			CATCH
			ENDTRY
		ENDIF
	ENDIF
ENDFUNC



**
FUNCTION uf_robot_dispensaArmazem
	LPARAMETERS lcTipo
	

	IF !myUsaRobot AND !myUsaRobotApostore AND !myUsaFarmax AND !myUsaRobotConsis AND !myUsaServicosRobot &&uf_gerais_getParameter("ADM0000000221","BOOL") 
		uf_perguntalt_chama("N�O TEM O ROB� PARAMETRIZADO.","OK","",64)
		RETURN .f.
	ENDIF
	


	** se enviar o comando para marcar como producto de robot
	IF(UPPER(ALLTRIM(lcTipo))=='ROBOT')
		**sen�o estiver marcado para dispensa de armazem e existir no robot
		SELECT fi

		IF UPPER(ALLTRIM(fi.u_robot))!='P' 	
			if(fi.num1>0 AND myUsaServicosRobot )	
				uf_robot_servico_retrieve_single()	
			endif		
		ELSE
			atendimento.pgfDados.page2.txtnum1.disabledbackcolor = RGB(42,143,154)
			replace fi.u_robot WITH ''	
		ENDIF
	
	
	ELSE
		SELECT fi
		replace fi.u_robot WITH 'P'	
		
		atendimento.pgfDados.page2.txtnum1.disabledbackcolor = RGB(255,255,255)
		
	ENDIF
	

ENDFUNC



**
FUNCTION uf_robot_tecnilabMsgS
	LPARAMETERS tcPath

	Local lcRpath, lcTerm, lcDebug

	lcTerm	= substr("000",1,len("000")-len(astr(myTermNo))) + astr(myTermNo)
	
	DO CASE
		CASE myUsaRobot
			IF FILE(ALLTRIM(tcPath)+'\externos\tecnilab\viriato.jar')
				lcRpath = ALLTRIM(tcPath)+'\externos\tecnilab\viriato.jar "S' +ALLTRIM(lcTerm)+ '"' + ' "' +astr(myTermNo)+ '" ' + '"0"'
			ELSE
				uf_perguntalt_chama("O SOFTWARE DO ROB� N�O EST� A SER ENCONTRADO","OK","", 16)
				RETURN .F.
			ENDIF

		CASE myUsaRobotApostore
			IF FILE(ALLTRIM(tcPath)+'\externos\robo\apostore\ApostoreClient.jar')
				lcRpath = ALLTRIM(tcPath)+'\externos\robo\apostore\ApostoreClient.jar "S' +ALLTRIM(lcTerm)+ '"' + ' "' +astr(myTermNo)+ '" ' + '"0"'
			ELSE
				uf_perguntalt_chama("O SOFTWARE DO ROB� N�O EST� A SER ENCONTRADO","OK","", 16)
				RETURN .F.
			ENDIF
			
		OTHERWISE
			RETURN
	ENDCASE

	IF emdesenvolvimento &&debug mode
		uf_robot_Log(lcRpath, 'PHC: Robo', 'uf_robot_tecnilabMsgS')
	ENDIF
	
	oWSShell = CREATEOBJECT("WScript.Shell")
	oWSShell.Run(lcRpath,1,.t.)
ENDFUNC



**
FUNCTION uf_robot_tecnilabMsgA
	LPARAMETERS tcRef, tcQt
	
	LOCAL lcMsg, lcEspacos, lcEspacosQt, pcProdCode, lcTerm, lcRpath, lcIDCliente 
	lcIDCliente = uf_gerais_getParameter('ADM0000000157','TEXT')
	
	IF EMPTY(myPath)
		uf_perguntalt_chama("N�O EST� DEFINIDO O CAMINHO PARA O EXECUT�VEL DO ROB�.","OK","",48)
		RETURN '01'
	ENDIF

	lcTerm	= substr("000",1,len("000")-len(astr(myTermNo))) + astr(myTermNo)
	
	DO CASE
		CASE myUsaRobot
			lcEspacos	= "========================="
			lcEspacosQt	= "00000"

			lcMsg 		= "A"+"10000"+ALLTRIM(lcTerm)+ALLTRIM(myDropLocal)+ALLTRIM(myDropLocal)+"1"+"01"
			lcProdCode	= "03904"+alltrim(tcRef)
			lcMsg 		= lcMsg + lcProdCode + LEFT(lcEspacos,LEN(lcEspacos)-LEN(lcProdCode))
			lcMsg		= lcMsg + LEFT(lcEspacosQt,LEN(lcEspacosQt)-LEN(astr(tcQt)))+astr(tcQt) + "1"
	
			lcRpath = ALLTRIM(myPath)+'\externos\tecnilab\viriato.jar "' +lcMsg+ '"' + ' "' +astr(myTermNo)+ '"' + ' "0"'
			
		CASE myUsaRobotApostore
			** Ex: A | 10000001 | 001 | 101 | 1 | 01 | 8168617 | 00004 | 1
			lcEspacosQt	= "00000"
			
			tcRef		= uf_robot_getCodIntRef(tcRef, .f.)
			
			lcMsg 		= "A"+"10000"+ALLTRIM(lcTerm)+ALLTRIM(lcTerm)+ALLTRIM(myDropLocal)+"1"+"01"
			lcMsg 		= lcMsg + alltrim(tcRef)
			lcMsg		= lcMsg + LEFT(lcEspacosQt,LEN(lcEspacosQt)-LEN(astr(tcQt)))+astr(tcQt) + "1"
	
			lcRpath = ALLTRIM(myPath)+'\externos\robo\apostore\ApostoreClient.jar "' +lcMsg+ '"' + ' "' +astr(myTermNo)+ '"' + ' "0"'

		CASE myUsaRobotConsis &&uf_gerais_getParameter("ADM0000000221","BOOL") == .t.
			
			IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + "\Robo\consis\ConsisClient.jar")
				uf_perguntalt_chama("Ficheiro de configura��o do Robo Consis n�o encontrado. Contacte o suporte.","OK","",16)
				RETURN .f.
			ENDIF 
			
			** Ex: A | 00000002 | 999 | 005 | 3 | 01 | 0173010 | 00001 | 1
			lcEspacosQt	= "00000"
			
			tcRef		= tcRef
			
			lcMsg 		= "A"+"10000"+ALLTRIM(lcTerm)+ALLTRIM(lcTerm)+ALLTRIM(myDropLocal)+"3"+"01"
			lcMsg 		= lcMsg + alltrim(tcRef)
			lcMsg		= lcMsg + LEFT(lcEspacosQt,LEN(lcEspacosQt)-LEN(astr(tcQt)))+astr(tcQt) + "1"
		
			lcRpath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + "\Robo\consis\ConsisClient.jar " + lcMsg + " " + ALLTRIM(lcIDCliente) + " " + astr(myTermNo)+ " 0"

*!*		_CLIPTEXT = lcRpath	
*!*		MESSAGEBOX(lcRpath)
*!*					
		OTHERWISE
			RETURN	
	ENDCASE

	IF emdesenvolvimento &&debug mode
		uf_robot_Log(lcRpath, 'PHC: Robo', 'uf_robot_tecnilabMsgA')
	ENDIF
	
	oWSShell = CREATEOBJECT("WScript.Shell")
	oWSShell.Run(lcRpath,1,.f.)
ENDFUNC



**
FUNCTION uf_robot_tecnilabMsgB
	LPARAMETERS tcRef
	
	LOCAL lcMsg, lcEspacos, lcTerm, lcRpath
	
	IF EMPTY(myPath)
		uf_perguntalt_chama("N�O EST� DEFINIDO O CAMINHO PARA O EXECUT�VEL DO ROB�.","OK","",48)
		RETURN '01'
	ENDIF
	
	lcTerm	= substr("000",1,len("000")-len(astr(myTermNo))) + astr(myTermNo)
	
	DO CASE
		CASE myUsaRobot
			lcEspacos	= "============================="
			lcMsg 		= "B"+ALLTRIM(lcTerm)+ALLTRIM(myRobotCodInter)+ALLTRIM(tcRef)
			lcMsg 		= lcMsg + LEFT(lcEspacos,LEN(lcEspacos)-LEN(lcMsg))
			
			lcRpath 	= ALLTRIM(myPath)+'\externos\tecnilab\viriato.jar "' +lcMsg+ '"'+ ' "' +astr(myTermNo)+ '"' + ' "0"'
			
		CASE myUsaRobotApostore
			tcRef	= uf_robot_getCodIntRef(tcRef, .f.)
			
			lcMsg 	= "B"+ALLTRIM(lcTerm)+ALLTRIM(tcRef)
			
			lcRpath = ALLTRIM(myPath)+'\externos\robo\apostore\apostoreClient.jar "' +lcMsg+ '"'+ ' "' +astr(myTermNo)+ '"' + ' "0"'
		
		OTHERWISE
			RETURN
	ENDCASE
	
	IF emdesenvolvimento &&debug mode
		uf_robot_Log(lcRpath, 'PHC: Robo', 'uf_robot_tecnilabMsgB')
	ENDIF
	
	oWSShell = CREATEOBJECT("WScript.Shell")
	oWSShell.Run(lcRpath,1,.t.)
ENDFUNC


**************************
** executa a mensagem K **
**************************
FUNCTION uf_robot_tecnilabMsgK
	LPARAMETERS tcRef
	
	LOCAL lcMsg, lcEspacos, lcProdCode, lcNrProd, lcRpath, lcTerm
	
	IF EMPTY(myPath)
		uf_perguntalt_chama("N�O EST� DEFINIDO O CAMINHO PARA O EXECUT�VEL DO ROB�.","OK","",48)
		RETURN '01'
	ENDIF

	lcNrProd 	= "10"
	
	DO CASE
		CASE myUsaRobot
			lcEspacos	= "========================="
			lcMsg 		= "K"+"000"
			lcProdCode	= "03904"+alltrim(tcRef)
			lcMsg 		= lcMsg + lcProdCode + LEFT(lcEspacos,LEN(lcEspacos)-LEN(lcProdCode))
			lcMsg		= lcMsg + ALLTRIM(lcNrProd)

			lcRpath 	= ALLTRIM(myPath)+'\externos\tecnilab\Viriato.jar "' + lcMsg + '"' + ' "' + astr(myTermNo) + '"' + ' "0"'
			
		CASE myUsaRobotApostore
			**Ex: K | 001 | 0000000 | 10
			lcTerm	= substr("000",1,len("000")-len(astr(myTermNo))) + astr(myTermNo)
			lcMsg 		= "K" + ALLTRIM(lcTerm)
			lcProdCode	= ALLTRIM(tcRef)
			lcMsg 		= lcMsg + ALLTRIM(lcProdCode)
			lcMsg		= lcMsg + ALLTRIM(lcNrProd)
			
			lcRpath 	= ALLTRIM(myPath)+'\externos\robo\apostore\ApostoreClient.jar "' + lcMsg + '"' + ' "' + astr(myTermNo) + '"' + ' "0"'
	ENDCASE

	IF emdesenvolvimento &&debug mode
		uf_robot_Log(lcRpath, 'PHC: Robo', 'uf_robot_tecnilabMsgK')
	ENDIF

	oWSShell = CREATEOBJECT("WScript.Shell")
	oWSShell.Run(lcRpath,1,.f.)
ENDFUNC


********************************************************
** Transfere qtt do stock do robot para outro armazem **
********************************************************
FUNCTION uf_robot_trfEntreArmazensRobot
	LPARAMETERS tcRef, tcQt, tcOp, tcOpNome, tcArmazem
	
	TEXT TO lcSql NOSHOW textmerge
		exec up_robot_trfentrearmazens '<<ALLTRIM(tcRef)>>', <<tcQt>>, '<<ALLTRIM(mySite)>>', <<tcOp>>, '<<ALLTRIM(tcOpNome)>>', <<tcArmazem>>
	ENDTEXT
	IF !uf_gerais_actGrelha("","",lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A TRANSFERIR PRODUTO DO ARMAZ�M ROB�.","OK","",16)
	ENDIF
ENDFUNC



**
Function uf_robot_setTimerRobot
	LPARAMETERS tcBool
	
	IF tcBool
		IF !(Atendimento.tmrRobot.enabled)
			myTimer = 0
			Atendimento.tmrRobot.enabled = .t.
		ELSE
			uf_perguntalt_chama("J� EXISTE UM PEDIDO EM CURSO.","OK","",64)
		ENDIF
	ELSE
		Atendimento.tmrRobot.enabled = .f.
		myTimer = 0
	ENDIF
ENDFUNC


****************************
*** FIM - ROBOT Tecnilab ***
****************************


********************
*** ROBOT FarMax ***
********************
FUNCTION uf_robot_TimerFarmax
	myTimer = myTimer + 2500

	IF (myTimer >= 60000)
		uf_robot_setTimerFarMax(.f.)
		RETURN
	ENDIF
	
	TEXT TO lcSql NOSHOW textmerge
		exec up_farmax_resposta '<<astr(myTermNo)>>', <<myNrProd>>
	ENDTEXT
	IF !uf_gerais_actGrelha("","uCrsVerificaDispensa",lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A VALIDAR RESPOSTA DO ROB�.","OK","",16)
		uf_robot_setTimerFarmax(.f.)
		RETURN
	ELSE
		IF RECCOUNT()>0
			SELECT uCrsVerificaDispensa
			GO TOP
			SCAN
				IF uCrsVerificaDispensa.stock > 0 AND uCrsVerificaDispensa.stock < 999
					LOCAL lcPos
					
					SELECT fi
					lcPos = RECNO()
					
					SELECT fi
					GO TOP
					SCAN FOR ALLTRIM(fi.ref)==ALLTRIM(uCrsVerificaDispensa.ref) AND EMPTY(fi.ofistamp) AND !(ALLTRIM(fi.u_robot)=='R')
						replace fi.u_robot WITH 'R' IN fi
						** transferir de stock **
				*		IF uCrsVerificaDispensa.stock>0
				*			uf_trfEntreArmazensRobot(tcRef, uCrsVerificaDispensa.stock, m.ch_vendedor, m.ch_vendnm, 1) && desactivo ate integra��o do robot back-office
				*		ENDIF
						SELECT fi
					ENDSCAN

					SELECT fi
					IF lcPos>0
						TRY
							GO lcPos
						CATCH
							GO BOTTOM
						ENDTRY
					ENDIF
				ENDIF
				SELECT uCrsVerificaDispensa
			ENDSCAN
				
			fecha("uCrsVerificaDispensa")
			
			** apagar registos **
			uf_gerais_actGrelha("","",[exec up_robot_delTable ']+astr(myTermNo)+['])
			
			uf_robot_setTimerFarmax(.f.)
		ENDIF
	ENDIF
ENDFUNC


********************************************************
** Dispensa todos os produtos do atendimento (Farmax) **
********************************************************
FUNCTION uf_robot_dispensaFrontBulkFm
	LPARAMETERS tcBool
	
	IF !myUsaFarmax
		RETURN
	ENDIF
	
	** validar se existe algum pedido em curso **
	IF Type("Atendimento.tmrRobot") == "O"
		IF Atendimento.tmrRobot.enabled
			RETURN
		ENDIF
	ENDIF
	*********************************************
	
	myNrProd = 0
	
	LOCAL lcQt, lcMsg, pcProdCode, lcProdCode2, lcSumQt, lcFiPos
	Store '' to lcMsg, pcProdCode, lcProdCode2
	Store 0 to lcSumQt, lcFiPos

	SELECT fi
	lcFiPos = RECNO()
	SELECT fi
	GO TOP
	SCAN FOR !empty(fi.ref) AND EMPTY(fi.u_robot) AND EMPTY(fi.ofistamp)
		lcProdCode = SUBSTR("0000000",1,LEN("0000000")-LEN(ALLTRIM(fi.ref))) + LEFT(alltrim(fi.ref),7)
		lcQt = fi.qtt
		IF EMPTY(lcProdCode2)
			lcProdCode2 = lcProdCode2 + lcProdCode +  left(SUBSTR("000",1,LEN("000")-LEN(astr(lcQt))) + astr(lcQt),3)
		ELSE
			lcProdCode2 = lcProdCode2 + "-" + lcProdCode + left(SUBSTR("000",1,LEN("000")-LEN(astr(lcQt))) + astr(lcQt),3)
		ENDIF
		myNrProd = myNrProd + 1
		lcSumQt	= lcSumQt + fi.qtt
		** marcar o produto para caso o produto n�o chegue do robo o POS n�o o pedir novamente
		replace fi.u_robot WITH 'P'
	ENDSCAN

	IF (myNrProd > 99)
		uf_perguntalt_chama("N�O � POSS�VEL PEDIR AO ROB� MAIS DO QUE 99 PRODUTOS.","OK","",64)
		RETURN
	ELSE
		IF (myNrProd < 1)
			uf_perguntalt_chama("N�O EXISTEM PRODUTOS A PEDIR AO ROB�.","OK","",64)
			RETURN
		ENDIF
	ENDIF
	
	SELECT fi
	IF (lcFiPos > 0)
		TRY
			GO lcFiPos
		CATCH
			GO BOTTOM
		ENDTRY
	ENDIF

	Local lcRpath
	
	** pedir produto ao robot **
	IF !tcBool
		lcRpath	 = ALLTRIM(myPath)+'\externos\farmax\helena.jar "' +lcProdCode2+ '"' + ' "' +astr(myTermNo)+ '"' + ' "0"'

		oWSShell = CREATEOBJECT("WScript.Shell")
		oWSShell.Run(lcRpath,1,.f.)
		
		uf_robot_setTimerFarmax(.t.)
	ELSE
		lcRpath	 = ALLTRIM(myPath)+'\externos\farmax\helena.jar "' +lcProdCode2+ '"' + ' "' +astr(myTermNo)+ '"' + ' "1"'
		oWSShell = CREATEOBJECT("WScript.Shell")
		oWSShell.Run(lcRpath,1,.t.)
		
		IF !uf_gerais_actGrelha("",[uCrsVerificaDispensa],[exec up_farmax_resposta ']+astr(myTermNo)+[', ]+astr(myNrProd)+[])
			uf_perguntalt_chama("OCORREU UM ERRO A VALIDAR STOCK NO ROB�.","OK","",16)
			RETURN '0'
		ELSE
			IF RECCOUNT()>0
				SELECT uCrsVerificaDispensa
				GO TOP
				SCAN
					IF uCrsVerificaDispensa.stock > 0 AND uCrsVerificaDispensa.stock < 999
						SELECT fi
						GO top
						SCAN FOR ALLTRIM(fi.ref)==ALLTRIM(uCrsVerificaDispensa.ref) AND EMPTY(fi.ofistamp) AND !(ALLTRIM(fi.u_robot)=='R')
							replace fi.u_robot WITH 'R' IN fi
							SELECT fi
						ENDSCAN
					ENDIF
					SELECT uCrsVerificaDispensa
				ENDSCAN
					
				fecha("uCrsVerificaDispensa")
			ENDIF
		ENDIF
		
		** apagar registos **
		uf_gerais_actGrelha("","",[exec up_robot_delTable ']+astr(myTermNo)+['])
	ENDIF
ENDFUNC


****************************************
** Processa pedidos ao robot (Farmax) **
****************************************
FUNCTION uf_robot_sendFarMaxRequest
	LPARAMETERS lcMsg, lcState
	
	Local lcRpath
	
	lcRpath = ALLTRIM(myPath)+'\externos\farmax\helena.jar "' +lcMsg+ '"' + ' "' +astr(myTermNo)+ '"' + ' "' +astr(lcState)+ '"'
	oWSShell = CREATEOBJECT("WScript.Shell")
	IF lcState = 0
		oWSShell.Run(lcRpath,1,.f.)
	ELSE
		oWSShell.Run(lcRpath,1,.t.)
	ENDIF
ENDFUNC


**
FUNCTION uf_robot_setTimerFarMax
	LPARAMETERS tcBool
		
	If tcBool
		IF !(Atendimento.tmrRobot.enabled)
			myTimer = 0
			Atendimento.tmrRobot.enabled = .t.
		ELSE
			uf_perguntalt_chama("J� EXISTE UM PEDIDO EM CURSO.","OK","",64)
		ENDIF
	ELSE
		Atendimento.tmrRobot.enabled = .f.
		myTimer = 0
		
		** apagar registos **
		uf_gerais_actGrelha("","",[exec up_robot_delTable ']+astr(myTermNo)+['])
	ENDIF
ENDFUNC
**************************
*** FIM - ROBOT FarMax ***
**************************


********************
*** ROBOT Consis ***
********************
********************************************************
** Dispensa todos os produtos do atendimento (Consis) **
********************************************************
FUNCTION uf_robot_dispensaFrontBulkConsis
	LPARAMETERS tcBool
	
	LOCAL lcControlaPedidos 
	lcControlaPedidos = .f.
	
	IF !myUsaRobotConsis &&uf_gerais_getParameter('ADM0000000221','bool')
		RETURN .f.
	ENDIF
	
	** Validar se existe algum pedido em curso **
	**IF Type("Atendimento.tmrRobot") == "O"
		IF Atendimento.tmrRobot.enabled ==  .T.
			RETURN .f.
		ENDIF
	**ENDIF
	*********************************************

	myNrProd = 0

	SELECT fi
	lcFiPos = RECNO()
	SELECT fi
	GO TOP
	SCAN FOR !empty(fi.ref) AND EMPTY(fi.u_robot) AND EMPTY(fi.ofistamp)
		myNrProd = myNrProd + 1

		uf_robot_tecnilabMsgA(alltrim(fi.ref), fi.qtt)
		lcControlaPedidos = .t.
		** marcar o produto para caso o robo n�o envie o produto este n�o ser pedido novamente
		replace fi.u_robot WITH 'P'
		
		SELECT fi
	ENDSCAN

	SELECT fi
	IF lcFiPos>0
		TRY
			GO lcFiPos
		CATCH
			GO BOTTOM
		ENDTRY
	ENDIF
	IF lcControlaPedidos ==.t.
		uf_robot_setTimerRobot(.t.)
	ENDIF 
ENDFUNC


**************************
*** FIM - ROBOT Consis ***
**************************


***********
** Geral **
***********
FUNCTION uf_robot_getCodIntRef
	LPARAMETERS tcRef, tcBool
	
	LOCAL lcRef, lcSql
	STORE '' TO lcSql
	STORE '9999999' TO lcRef
	
	IF !tcBool
		TEXT TO lcSql NOSHOW textmerge
			SELECT u_codint as ref FROM st (nolock) WHERE ref='<<ALLTRIM(tcRef)>>' AND site_nr = <<mysite_nr>>
		ENDTEXT
	ELSE
		TEXT TO lcSql NOSHOW textmerge
			SELECT ref FROM st (nolock) WHERE u_codint='<<ALLTRIM(tcRef)>>' AND site_nr = <<mysite_nr>>
		ENDTEXT
	ENDIF
	IF !uf_gerais_actgrelha("", [uCrsRobotCodInt], lcSql)
		uf_perguntalt_chama("OCORREU UM PROBLEMA A PROCURAR REFER�NCIA INTERNA DO ROB�.","OK","",16)
		RETURN lcRef
	ENDIF

	SELECT uCrsRobotCodInt
	IF RECCOUNT("uCrsRobotCodInt")>0
		GO TOP
		lcRef = ALLTRIM(uCrsRobotCodInt.ref)
	ENDIF
	fecha("uCrsRobotCodInt")	

	IF EMPTY(lcRef)
		lcRef = '9999999'
	ENDIF 
	
	RETURN lcRef
ENDFUNC


**********************************
** Regista informa��es nos Logs **
**********************************
FUNCTION uf_robot_Log
	LPARAMETERS tcMsg, tcApp, tcFunc
	
	IF EMPTY(tcMsg) OR EMPTY(tcFunc)
		RETURN
	ENDIF
	LOCAL lcSql
	lcSql = ''
	
	TEXT TO lcSql NOSHOW textmerge
		INSERT INTO b_movtec_robot_log
			(data, source_class, source_method, nivel, message)
		values
			(dateadd(HOUR, <<difhoraria>>, getdate()), '<<ALLTRIM(tcApp)>>', '<<ALLTRIM(tcFunc)>>', 'Info', '<<ALLTRIM(tcMsg)>>');
    endtext
    IF !uf_gerais_actGrelha("","",lcSql)
    	uf_perguntalt_chama("OCORREU UM ERRO A REGISTAR O LOG DO ROBO.","OK","",16)
    	RETURN
    ENDIF
ENDFUNC