**
FUNCTION uf_importhonorarios_chama
	LPARAMETERS lcUserNo, lcNome
	
	IF EMPTY(lcUserno)
		lcUserno = 0
	ENDIF 
	
	**Valida Licenciamento	
*!*		IF (uf_gerais_addConnection('HON') == .f.)
*!*			RETURN .F.
*!*		ENDIF
	
	IF TYPE("importhonorarios") == "U"
		uf_importhonorarios_cursoresIniciais(lcUserNo)
		DO FORM importhonorarios WITH lcUserNo, lcNome
	ELSE
		importhonorarios.show
	ENDIF
ENDFUNC 


**
FUNCTION uf_importhonorarios_cursoresIniciais
	LPARAMETERS lcUserNo
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		declare @dataIni datetime, @datafim datetime
		set @dataIni = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate())-30,112)
		set @datafim = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112)

		exec up_clinica_importaHonorarios
				@dataIni 
				,@datafim 
				,<<lcUserNo>>
				,''
				,''
				,''
	ENDTEXT 

	If !uf_gerais_actGrelha("", "ucrsImportHonorarios", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar dados para c�lculo de honor�rios. Por favor contacte o suporte.","OK","",16)
		RETURN .f.
	ENDIF 
	
	uf_importhonorarios_calculaHonorarios()
	
ENDFUNC 


**
FUNCTION uf_importhonorarios_carregaMenu
	importhonorarios.menu1.adicionaOpcao("actualizar", "Atualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_importhonorarios_atualiza","A")
	importhonorarios.menu1.adicionaOpcao("selTodos", "Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_importhonorarios_selTodos","T")
	importhonorarios.menu1.estado("", "SHOW", "Importar", .t., "Sair", .t.)
ENDFUNC 


**
FUNCTION uf_importhonorarios_atualiza
	LOCAL lcTipo, lcBi, lcValor, lcValorAplicar
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
			exec up_clinica_importaHonorarios 
				'<<uf_gerais_getdate(importhonorarios.dataIni.Value,"SQL")>>'
				,'<<uf_gerais_getdate(importhonorarios.dataFim.Value,"SQL")>>'
				,<<importhonorarios.userno>>
				,''
				,''
				,''
	ENDTEXT 
			
	If !uf_gerais_actGrelha("importhonorarios.GridPesq", "ucrsImportHonorarios", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar dados para c�lculo de honor�rios. Por favor contacte o suporte.","OK","",16)
		RETURN .f.
	ENDIF 
	
	uf_importhonorarios_calculaHonorarios()
	
ENDFUNC 


**
FUNCTION uf_importhonorarios_selTodos

	IF EMPTY(importhonorarios.menu1.seltodos.tag) OR importhonorarios.menu1.seltodos.tag == "false"

		&& aplica sele��o
		UPDATE ucrsImportHonorarios SET ucrsImportHonorarios.sel = .t.
		SELECT ucrsImportHonorarios
		GO Top
		
		&& altera o botao
		importhonorarios.menu1.selTodos.tag = "true"
		importhonorarios.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\checked_w.png", "uf_importhonorarios_selTodos","T")

	ELSE
		UPDATE ucrsImportHonorarios SET ucrsImportHonorarios.sel = .f. 
		SELECT ucrsImportHonorarios
		GO Top
		
		&& altera o botao
		importhonorarios.menu1.selTodos.tag = "false"
		importhonorarios.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_importhonorarios_selTodos","T")
		
	ENDIF
	
	uf_honorarios_calculaTotalHonorarios()

ENDFUNC 


**
FUNCTION uf_importhonorarios_gravar
	LOCAL lcEspecialistaNo 
	
	SELECT ucrsImportHonorarios
	GO Top
	SCAN FOR !EMPTY(ucrsImportHonorarios.sel)
		
		SELECT FN
		CALCULATE MAX(FN.lordem) TO lcOrdem
		lcOrdem = lcOrdem + 1
		
		Select FN
		Append Blank
		
		
		*VALORES POR DEFEITO
		lcStamp = uf_gerais_stamp()
		SELECT FN
		REPLACE FN.FNSTAMP WITH	lcStamp
		REPLACE FN.docNome WITH	Alltrim(cabDoc.doc)
		REPLACE FN.adoc WITH cabDoc.numDoc
		REPLACE FN.lordem WITH lcOrdem
		
		REPLACE FN.data_h WITH uf_gerais_getdate(ucrsImportHonorarios.data)
		REPLACE FN.servmrstamp WITH ucrsImportHonorarios.servmrstamp 
		REPLACE FN.EntidadeNome WITH ucrsImportHonorarios.entidade
		REPLACE FN.entidadeNo WITH ucrsImportHonorarios.entidadeNo
		REPLACE FN.utente WITH ucrsImportHonorarios.nome
		REPLACE FN.utenteNo WITH ucrsImportHonorarios.no 
		REPLACE FN.pvp WITH ucrsImportHonorarios.pvp 
		REPLACE FN.valorUtente WITH ucrsImportHonorarios.total
		REPLACE FN.valorEntidade WITH ucrsImportHonorarios.compart
		REPLACE FN.despesa WITH ucrsImportHonorarios.despesa 
		REPLACE FN.honorario WITH ucrsImportHonorarios.honorario 
		
		REPLACE FN.honorarioDeducao WITH ucrsImportHonorarios.honorarioDeducao 
		REPLACE FN.honorariovalor WITH ucrsImportHonorarios.honorariovalor 
		REPLACE FN.honorariobi WITH ucrsImportHonorarios.honorariobi 
		REPLACE FN.honorariotipo WITH ucrsImportHonorarios.honorariotipo 
		REPLACE FN.userno WITH ucrsImportHonorarios.userno 
		REPLACE FN.id_cpt_val_cli WITH ucrsImportHonorarios.id_cpt_val_cli 
		REPLACE FN.convencao WITH ucrsImportHonorarios.convencao 

		REPLACE FN.ref WITH	ucrsImportHonorarios.ref
		REPLACE	FN.design WITH ucrsImportHonorarios.design
		REPLACE	FN.qtt WITH 1
		REPLACE	FN.etiliquido WITH ucrsImportHonorarios.honorario
		REPLACE	FN.epv WITH  ucrsImportHonorarios.honorario
		REPLACE	FN.ivaincl WITH .f.
		REPLACE	FN.tabiva WITH 4
		REPLACE	FN.armazem WITH 1
		REPLACE	FN.stns	WITH .t.
		

		*CONTROLA LIGACAO AO DOCUMENTO DE ORIGEM
		uf_documentos_recalculaTotaisFn()
		
	ENDSCAN 

	**
	uf_documentos_actualizatotaisCAB()
	**

	**
	uf_importhonorarios_sair()
	
ENDFUNC

*!*	**
*!*	FUNCTION uf_honorarios_reportEmitidos

*!*		uf_relatorio_chama()
*!*		WITH relatorio.pagina2.wdgselreport
*!*			.lcOrdem = 98
*!*			.lcReport = "relatorio_HonorariosEmitidos"
*!*			.txtAnalise.value = "Relatorio Honor�rios Emitidos"
*!*		ENDWITH

*!*	ENDFUNC 


**
FUNCTION uf_importhonorarios_calculaHonorarios
	
	LOCAL lcTipo, lcBi, lcValor, lcDesconto, lcValorAplicar, lcDescontoAplicar, lcEspecialisataNo, lcTotalHonorariosSelecionados 

	SELECT ucrsImportHonorarios
	GO TOP 
	SCAN 
		Select ucrsImportHonorarios
		lcTipo = UPPER(ALLTRIM(ucrsImportHonorarios.HonorarioTipo))
		lcBi = UPPER(ALLTRIM(ucrsImportHonorarios.HonorarioBi))
		lcValor = ucrsImportHonorarios.HonorarioValor
		lcValorAplicar = 0.00
	
		DO CASE 
			CASE lcTipo == "VALOR"
				
				lcValorAplicar = lcValor
			
				Select ucrsImportHonorarios		
				Replace ucrsImportHonorarios.honorario WITH (lcValorAplicar) + ucrsImportHonorarios.despesa
						
			CASE lcBi  == "VALOR TOTAL"
				lcValorAplicar = (ucrsImportHonorarios.pvp * lcValor)/100
				
				Select ucrsImportHonorarios
				Replace ucrsImportHonorarios.honorario WITH (lcValorAplicar) + ucrsImportHonorarios.despesa
				
			CASE lcBi == "VALOR PAGO PELA ENTIDADE"
				lcValorAplicar = (ucrsImportHonorarios.compart * lcValor)/100
				
				Select ucrsImportHonorarios
				Replace ucrsImportHonorarios.honorario WITH (lcValorAplicar) + ucrsImportHonorarios.despesa
			
			CASE lcBi == "VALOR PAGO PELO UTENTE"
				lcValorAplicar = (ucrsImportHonorarios.total * lcValor)/100

				Select ucrsImportHonorarios
				Replace ucrsImportHonorarios.honorario 	WITH (lcValorAplicar) + ucrsImportHonorarios.despesa

			OTHERWISE
				**
		ENDCASE	 
		
	ENDSCAN
	
	SELECT ucrsImportHonorarios
	GO TOP 
ENDFUNC 



**
FUNCTION uf_honorarios_calculaTotalHonorarios
	
	LOCAL lcSubTotal, lcRetencao, lcTotal
	STORE 0.00 TO lcSubTotal, lcRetencao, lcTotal
	
	
	Select ucrsImportHonorarios
	lcStamp = ucrsImportHonorarios.servmrstamp
	
	
	SELECT ucrsImportHonorarios
	GO TOP 
	SCAN FOR !EMPTY(ucrsImportHonorarios.sel)
		Select ucrsImportHonorarios
		lcTipo = UPPER(ALLTRIM(ucrsImportHonorarios.HonorarioTipo))
		lcBi = UPPER(ALLTRIM(ucrsImportHonorarios.HonorarioBi))
		lcValor = ucrsImportHonorarios.HonorarioValor
		lcValorAplicar = 0.00
		lcTotalHonorariosSelecionados = 0.00
	
		DO CASE 
			CASE lcTipo == "VALOR"
				
				lcValorAplicar = ROUND(lcValor,2) 
				lcTotal = lcTotal + (ROUND(lcValorAplicar,2) ) + ucrsImportHonorarios.despesa
						
			CASE lcBi  == "VALOR TOTAL"

				lcValorAplicar = (ucrsImportHonorarios.pvp * ROUND(lcValor,2))/100
				lcTotal = lcTotal + (ROUND(lcValorAplicar,2)) + ucrsImportHonorarios.despesa
				
			CASE lcBi == "VALOR PAGO PELA ENTIDADE"

				lcValorAplicar = (ucrsImportHonorarios.compart * ROUND(lcValor,2))/100
				lcTotal = lcTotal + (ROUND(lcValorAplicar,2) ) + ucrsImportHonorarios.despesa
			
			CASE lcBi == "VALOR PAGO PELO UTENTE"

				lcValorAplicar = (ucrsImportHonorarios.total * ROUND(lcValor,2))/100
				lcTotal = lcTotal + (ROUND(lcValorAplicar,2)) + ucrsImportHonorarios.despesa

			OTHERWISE
				**
		ENDCASE	 
		
	ENDSCAN
	
	
	Select ucrsImportHonorarios
	LOCATE FOR ucrsImportHonorarios.servmrstamp == lcStamp 
	
	IMPORTHONORARIOS.total.value = lcTotal
	
	
ENDFUNC 



**
FUNCTION uf_importhonorarios_sair
	
	importhonorarios.hide
	importhonorarios.release
ENDFUNC



