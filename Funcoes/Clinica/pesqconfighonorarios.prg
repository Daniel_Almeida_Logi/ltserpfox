**
FUNCTION uf_CONFIGHONORARIOS_chama

	**
	IF TYPE("CONFIGHONORARIOS") == "U"
	
		uf_CONFIGHONORARIOS_cursoresIniciais()
		DO FORM CONFIGHONORARIOS
		uf_CONFIGHONORARIOS_carregaMenu()
	ELSE
		
		**
		uf_CONFIGHONORARIOS_actualizaDados()
		CONFIGHONORARIOS.show
	ENDIF

ENDFUNC 


**
FUNCTION uf_CONFIGHONORARIOS_carregaMenu
	&& Configura menu principal
	WITH CONFIGHONORARIOS.menu1
		.adicionaOpcao("atualizar","Atualizar", myPath + "\imagens\icons\actualizar_w.png","uf_CONFIGHONORARIOS_actualizaDados","A")
		.adicionaOpcao("selTodos", "Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_CONFIGHONORARIOS_SelTodos","T")
		.adicionaOpcao("aplicaCondicoes", "Aplicar", myPath + "\imagens\icons\ferramentas_w.png", "uf_CONFIGHONORARIOS_AplicaCondicoes","F")
		.estado("", "SHOW", "Gravar", .t., "Sair", .t.)
	ENDWITH

ENDFUNC 


**
FUNCTION uf_CONFIGHONORARIOS_cursoresIniciais
	**
	CREATE CURSOR ucrsConfgHonorariosUs (sel l,especialidade c(254),convencao c(254), ref c(100), design c(100), refEntidade c(100), designEntidade c(254), HonorarioBi c(30), HonorarioTipo c(15), HonorarioValor n(9,2), HonorarioDeducao n(5,2), id i, id_cpt_val_cli c(25))
ENDFUNC 


**
FUNCTION uf_CONFIGHONORARIOS_actualizaDados
	LOCAL lcSql 
	lcSql = ""
	
	IF EMPTY(ALLTRIM(CONFIGHONORARIOS.especialista.value))
		uf_perguntalt_chama("Para actualizar os dados deve selecionar o Especialista.","OK","",64)
		RETURN .f.
	ENDIF 
	
	&& Cursor de Dependentes
	TEXT TO lcSql NOSHOW textmerge
		exec up_clinica_configHonorariosUs '<<ALLTRIM(CONFIGHONORARIOS.especialista.value)>>','<<ALLTRIM(CONFIGHONORARIOS.convencao.value)>>','<<ALLTRIM(CONFIGHONORARIOS.especialidade.value)>>'
	ENDTEXT
	IF !uf_gerais_actgrelha("CONFIGHONORARIOS.gridpesq", "ucrsConfgHonorariosUs", lcSql)
		RETURN .f.
	ENDIF

ENDFUNC 


**
FUNCTION uf_CONFIGHONORARIOS_SelTodos
	IF EMPTY(CONFIGHONORARIOS.menu1.seltodos.tag) OR CONFIGHONORARIOS.menu1.seltodos.tag == "false"
		&& aplica seleção
		REPLACE ALL sel WITH .t. IN ucrsConfgHonorariosUs
		
		&& altera o botao
		CONFIGHONORARIOS.menu1.selTodos.tag = "true"
		CONFIGHONORARIOS.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\checked_w.png", "uf_CONFIGHONORARIOS_SelTodos","T")
	ELSE
		&& aplica seleção
		REPLACE ALL sel WITH .f. IN ucrsConfgHonorariosUs
		
		&& altera o botao
		CONFIGHONORARIOS.menu1.selTodos.tag = "false"
		CONFIGHONORARIOS.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_CONFIGHONORARIOS_SelTodos","T")
	ENDIF


	SELECT ucrsConfgHonorariosUs
	GO top
ENDFUNC


**
FUNCTION uf_CONFIGHONORARIOS_AplicaCondicoes

	lcBi = CONFIGHONORARIOS.BI.value
	lcTipo = CONFIGHONORARIOS.Tipo.value
	lcValor = CONFIGHONORARIOS.valor.value
	lcDesconto = CONFIGHONORARIOS.desconto.value
	
	UPDATE ucrsConfgHonorariosUs;
	SET ucrsConfgHonorariosUs.honorarioBI = lcBi,;
		ucrsConfgHonorariosUs.honorarioTipo = lcTipo,;
		ucrsConfgHonorariosUs.honorarioValor = lcValor,;
		ucrsConfgHonorariosUs.honorarioDeducao = lcDesconto;
	WHERE !EMPTY(ucrsConfgHonorariosUs.sel)

	
	SELECT ucrsConfgHonorariosUs
	GO TOP

	uf_perguntalt_chama("Configuração aplicada com sucesso.","OK","",64)
ENDFUNC 



**
FUNCTION uf_CONFIGHONORARIOS_gravar
	SELECT ucrsConfgHonorariosUs
	GO Top
	SCAN FOR ucrsConfgHonorariosUs.sel == .t.
		
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			
			IF NOT exists (select id from cpt_val_cli_us where cpt_val_cli_us.id = <<ucrsConfgHonorariosUs.id>>)
			BEGIN			
				INSERT INTO cpt_val_cli_us (
					id
					,id_cpt_val_cli
					,userno
					,HonorarioDeducao
					,HonorarioValor
					,HonorarioBi
					,HonorarioTipo
					,id_us
					,id_us_alt
					,data_cri
					,data_alt
				) VALUES (
					(select ISNULL(MAX(id),0) +1 from cpt_val_cli_us)
					,'<<ALLTRIM(ucrsConfgHonorariosUs.id_cpt_val_cli)>>'
					,<<ucrsConfgHonorariosUs.userno>>
					,<<ucrsConfgHonorariosUs.HonorarioDeducao>>
					,<<ucrsConfgHonorariosUs.HonorarioValor>>
					,'<<ALLTRIM(ucrsConfgHonorariosUs.HonorarioBi)>>'
					,'<<ALLTRIM(ucrsConfgHonorariosUs.HonorarioTipo)>>'
					,<<ch_userno>>
					,<<ch_userno>>
					,getdate()
					,getdate()
				)
			END
			ELSE			
			BEGIN
				UPDATE cpt_val_cli_us 
				SET	id_cpt_val_cli = '<<ALLTRIM(ucrsConfgHonorariosUs.id_cpt_val_cli)>>'
					,userno = <<ucrsConfgHonorariosUs.userno>>
					,HonorarioDeducao = <<ucrsConfgHonorariosUs.HonorarioDeducao>>
					,HonorarioValor = <<ucrsConfgHonorariosUs.HonorarioValor>>
					,HonorarioBi = '<<ALLTRIM(ucrsConfgHonorariosUs.HonorarioBi)>>'
					,HonorarioTipo = '<<ALLTRIM(ucrsConfgHonorariosUs.HonorarioTipo)>>'
					,id_us_alt = <<ch_userno>>
					,data_alt = getdate()
				WHERE
					cpt_val_cli_us.id = <<ucrsConfgHonorariosUs.id>>
			END
		ENDTEXT 
		
		
		IF !uf_gerais_actgrelha("", "", lcSql)
			uf_perguntalt_chama("Ocorreu um erro na actualização na configuração do Honorários. Contacte o suporte.","OK","",16)
			RETURN .f.
		ENDIF
		
	
	ENDSCAN
	

ENDFUNC 



**
FUNCTION uf_CONFIGHONORARIOS_sair

	**
	IF USED("ucrsConfgHonorariosUs")
		fecha("ucrsConfgHonorariosUs")
	ENDIF 
	
	
	CONFIGHONORARIOS.hide
	CONFIGHONORARIOS.release
ENDFUNC

