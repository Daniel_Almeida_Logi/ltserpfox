**
FUNCTION uf_CONFIGHONORARIOS_chama
	PUBLIC myConfigHonorariosAlteracao
	LOCAL lcUserno
	
	&& CONTROLA PERFIS DE ACESSO 
	IF !uf_gerais_validaPermUser(m.ch_userno,m.ch_grupo,'Configura��o de Honorarios')
		uf_perguntalt_chama("O seu perfil n�o permite aceder � configura��o de Honor�rios.","OK","",48)
		RETURN .f.
	ENDIF
	
	IF !USED("ucrsUs")
		RETURN .f.
	ENDIF 
	
	SELECT ucrsUs
	lcUserno = ucrsUs.userno
	IF EMPTY(lcUserno)
		RETURN .f.
	ENDIF 
	
	**
	IF TYPE("CONFIGHONORARIOS") == "U"
	
		uf_CONFIGHONORARIOS_cursoresIniciais()
		DO FORM CONFIGHONORARIOS
		uf_CONFIGHONORARIOS_carregaMenu()
	ELSE
		
		**
		uf_CONFIGHONORARIOS_actualizaDados()
		CONFIGHONORARIOS.show
	ENDIF

	CONFIGHONORARIOS.userno = lcUserno 
	uf_CONFIGHONORARIOS_AlternaMenu()
ENDFUNC 


**
FUNCTION uf_CONFIGHONORARIOS_carregaMenu
	&& Configura menu principal
	WITH CONFIGHONORARIOS.menu1
		.adicionaOpcao("atualizar","Atualizar", myPath + "\imagens\icons\actualizar_w.png","uf_CONFIGHONORARIOS_actualizaDados","A")
		.adicionaOpcao("editar", "Editar", myPath + "\imagens\icons\lapis_w.png", "uf_CONFIGHONORARIOS_editar","E")
		
		.adicionaOpcao("convencao", "Conven��o", myPath + "\imagens\icons\doc_seta_w.png", "uf_CONFIGHONORARIOS_importaConvencao","N")
		.adicionaOpcao("novaLinha", "Nova Linha", myPath + "\imagens\icons\mais_w.png", "uf_CONFIGHONORARIOS_novaLinha","N")
		.adicionaOpcao("eliminaLinha", "Eliminar Linha", myPath + "\imagens\icons\cruz_w.png", "uf_CONFIGHONORARIOS_apagaLinha","E")
		
		
		.adicionaOpcao("selTodos", "Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_CONFIGHONORARIOS_SelTodos","T")
		.adicionaOpcao("aplicaCondicoes", "Aplicar", myPath + "\imagens\icons\ferramentas_w.png", "uf_CONFIGHONORARIOS_AplicaCondicoes","F")
	ENDWITH

ENDFUNC 


**
FUNCTION uf_CONFIGHONORARIOS_AlternaMenu

	WITH CONFIGHONORARIOS.menu1
		IF myConfigHonorariosAlteracao == .f.
			.estado("selTodos, aplicaCondicoes, novaLinha, eliminaLinha, convencao", "HIDE")
			.estado("atualizar, editar", "SHOW", "Gravar", .f., "Sair", .t.)
			
			CONFIGHONORARIOS.LabelBi.visible =  .f. 
			CONFIGHONORARIOS.BI.visible =  .f.
			CONFIGHONORARIOS.LabelTipoComissao.visible =  .f.
			CONFIGHONORARIOS.tipo.visible =  .f.
			CONFIGHONORARIOS.LabelValor.visible =  .f.
			CONFIGHONORARIOS.valor.visible =  .f.
		

			
			WITH CONFIGHONORARIOS.GridPesq
				FOR i=1 TO .columnCount
					IF .Columns(i).name == "sel"
						.Columns(i).readonly = .t.
						.Columns(i).Check1.readonly = .t.
						.Columns(i).visible =  .f.
					ENDIF 
					IF .Columns(i).name == "bi" OR .Columns(i).name == "tipocomissao";
						OR .Columns(i).name == "honorarioValor" OR .Columns(i).name == "HonorarioDesc"
						
						.Columns(i).readonly = .t.
						.Columns(i).Text1.readonly = .t.
					ENDIF 
					
				ENDFOR
			ENDWITH 

		ELSE
			.estado("atualizar, editar", "HIDE")
			.estado("novaLinha, eliminaLinha, selTodos, aplicaCondicoes, convencao", "SHOW", "Gravar", .t., "Cancelar", .t.)
			
			
			CONFIGHONORARIOS.LabelBi.visible =  .t. 
			CONFIGHONORARIOS.BI.visible =  .t.
			CONFIGHONORARIOS.LabelTipoComissao.visible =  .t.
			CONFIGHONORARIOS.tipo.visible =  .t.
			CONFIGHONORARIOS.LabelValor.visible =  .t.
			CONFIGHONORARIOS.valor.visible =  .t.

			
			WITH CONFIGHONORARIOS.GridPesq
				FOR i=1 TO .columnCount
					IF .Columns(i).name == "sel"
						.Columns(i).readonly = .f.
						.Columns(i).Check1.readonly = .f.
						.Columns(i).visible =  .t.
					ENDIF 
					IF .Columns(i).name == "bi" OR .Columns(i).name == "tipocomissao";
						OR .Columns(i).name == "honorarioValor" OR .Columns(i).name == "HonorarioDesc"
						
						.Columns(i).readonly = .f.
						.Columns(i).Text1.readonly = .f.
					ENDIF 
					
				ENDFOR
			ENDWITH 

		ENDIF 
	ENDWITH 

ENDFUNC 


**
FUNCTION uf_CONFIGHONORARIOS_editar
	myConfigHonorariosAlteracao  = .t.
	uf_CONFIGHONORARIOS_AlternaMenu()
ENDFUNC


**
FUNCTION uf_CONFIGHONORARIOS_cursoresIniciais
	LOCAL lcSql,lcNome 
	lcSql = ""
	lcNome = ''
	
	SELECT ucrsUs
	lcNome = ucrsUs.nome
	&& Cursor de Dependentes
	TEXT TO lcSql NOSHOW textmerge
		exec up_clinica_configHonorariosUs '<<ALLTRIM(lcNome)>>','',''
	ENDTEXT
	IF !uf_gerais_actgrelha("", "ucrsConfgHonorariosUs", lcSql)
		RETURN .f.
	ENDIF

ENDFUNC 


**
FUNCTION uf_CONFIGHONORARIOS_actualizaDados
	LOCAL lcSql 
	lcSql = ""
	
	IF EMPTY(ALLTRIM(CONFIGHONORARIOS.especialista.value))
		uf_perguntalt_chama("Para actualizar os dados deve selecionar o Especialista.","OK","",64)
		RETURN .f.
	ENDIF 
	
	&& Cursor de Dependentes
	TEXT TO lcSql NOSHOW textmerge
		exec up_clinica_configHonorariosUs '<<ALLTRIM(CONFIGHONORARIOS.especialista.value)>>','',''
	ENDTEXT
	IF !uf_gerais_actgrelha("CONFIGHONORARIOS.gridpesq", "ucrsConfgHonorariosUs", lcSql)
		RETURN .f.
	ENDIF
	
	uf_CONFIGHONORARIOS_filtra()

ENDFUNC 


**
FUNCTION uf_CONFIGHONORARIOS_SelTodos
	IF EMPTY(CONFIGHONORARIOS.menu1.seltodos.tag) OR CONFIGHONORARIOS.menu1.seltodos.tag == "false"
		&& aplica sele��o
		REPLACE ALL sel WITH .t. IN ucrsConfgHonorariosUs
		
		&& altera o botao
		CONFIGHONORARIOS.menu1.selTodos.tag = "true"
		CONFIGHONORARIOS.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\checked_w.png", "uf_CONFIGHONORARIOS_SelTodos","T")
	ELSE
		&& aplica sele��o
		REPLACE ALL sel WITH .f. IN ucrsConfgHonorariosUs
		
		&& altera o botao
		CONFIGHONORARIOS.menu1.selTodos.tag = "false"
		CONFIGHONORARIOS.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_CONFIGHONORARIOS_SelTodos","T")
	ENDIF


	SELECT ucrsConfgHonorariosUs
	GO top
ENDFUNC


**
FUNCTION uf_CONFIGHONORARIOS_AplicaCondicoes

	lcBi = CONFIGHONORARIOS.BI.value
	lcTipo = CONFIGHONORARIOS.Tipo.value
	lcValor = CONFIGHONORARIOS.valor.value
	
	UPDATE ucrsConfgHonorariosUs;
	SET ucrsConfgHonorariosUs.honorarioBI = lcBi,;
		ucrsConfgHonorariosUs.honorarioTipo = lcTipo,;
		ucrsConfgHonorariosUs.honorarioValor = lcValor;
	WHERE !EMPTY(ucrsConfgHonorariosUs.sel)

	
	SELECT ucrsConfgHonorariosUs
	GO TOP

	uf_perguntalt_chama("Configura��o aplicada com sucesso.","OK","",64)
ENDFUNC 



**
FUNCTION uf_CONFIGHONORARIOS_gravar

	Select ucrsConfgHonorariosUs
	SET FILTER TO 
	
	SELECT ucrsUs
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		DELETE from cpt_val_cli_us WHERE  userno = <<ucrsUs.userno>>
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("Ocorreu um erro na actualiza��o na configura��o do Honor�rios. Contacte o suporte.","OK","",16)
		RETURN .f.
	ENDIF

	
	SELECT ucrsConfgHonorariosUs
	GO Top
	SCAN
		
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			
				INSERT INTO cpt_val_cli_us (
					id
					,ref
					,id_cpt_val_cli
					,userno
					,HonorarioDeducao
					,HonorarioValor
					,HonorarioBi
					,HonorarioTipo
					,id_us
					,id_us_alt
					,data_cri
					,data_alt
					,id_cpt_conv
				) VALUES (
					(select ISNULL(MAX(id),0) +1 from cpt_val_cli_us)
					,'<<ALLTRIM(ucrsConfgHonorariosUs.ref)>>'
					,'<<ALLTRIM(ucrsConfgHonorariosUs.id_cpt_val_cli)>>'
					,<<ucrsConfgHonorariosUs.userno>>
					,<<ucrsConfgHonorariosUs.HonorarioDeducao>>
					,<<ucrsConfgHonorariosUs.HonorarioValor>>
					,'<<ALLTRIM(ucrsConfgHonorariosUs.HonorarioBi)>>'
					,'<<ALLTRIM(ucrsConfgHonorariosUs.HonorarioTipo)>>'
					,<<ch_userno>>
					,<<ch_userno>>
					,dateadd(HOUR, <<difhoraria>>, getdate())
					,dateadd(HOUR, <<difhoraria>>, getdate())
					,<<ucrsConfgHonorariosUs.id_cpt_conv>>
				)
		ENDTEXT 
	
		IF !uf_gerais_actgrelha("", "", lcSql)
			uf_perguntalt_chama("Ocorreu um erro na actualiza��o na configura��o do Honor�rios. Contacte o suporte.","OK","",16)
			RETURN .f.
		ENDIF
		
	
	ENDSCAN
	
	
	myConfigHonorariosAlteracao= .f.
	uf_CONFIGHONORARIOS_AlternaMenu()
	uf_CONFIGHONORARIOS_actualizaDados()
ENDFUNC 

**
FUNCTION uf_CONFIGHONORARIOS_novaLinha

	uf_pesqrefsagrupadas_chama(.t.,"CONFIGHONORARIOS",.t.)

ENDFUNC


**
FUNCTION uf_CONFIGHONORARIOS_apagaLinha

	SELECT ucrsConfgHonorariosUs
	DELETE
	
	TRY 
		SELECT ucrsConfgHonorariosUs
		SKIP -1
	ENDTRY

	CONFIGHONORARIOS.gridpesq.refresh
ENDFUNC 


**
FUNCTION uf_CONFIGHONORARIOS_importaConvencao

	uf_pesqconvencoes_chama("CONFIGHONORARIOS")
	
ENDFUNC 


**
FUNCTION uf_CONFIGHONORARIOS_importaConvencaoSel
	LOCAL lcId

	SELECT uCrsPesqConvencoes
	lcId = uCrsPesqConvencoes.id
	

	SELECT uCrsConvencoesLin
	GO Top
	SCAN FOR !EMPTY(uCrsConvencoesLin.sel)
		
		SELECT ucrsConfgHonorariosUs
		CALCULATE MAX(ucrsConfgHonorariosUs.id) TO lcMaxId 
		lcMaxId = lcMaxId +1
		SELECT ucrsConfgHonorariosUs
		APPEND BLANK
				
		Replace ucrsConfgHonorariosUs.id WITH lcMaxId
		Replace ucrsConfgHonorariosUs.ref WITH uCrsConvencoesLin.ref
		Replace ucrsConfgHonorariosUs.design WITH uCrsConvencoesLin.design
		Replace ucrsConfgHonorariosUs.convencao WITH uCrsConvencoesLin.Convencao
		Replace ucrsConfgHonorariosUs.id_cpt_val_cli WITH uCrsConvencoesLin.id
		Replace ucrsConfgHonorariosUs.id_cpt_conv WITH uCrsConvencoesLin.id_cpt_conv 
		Replace ucrsConfgHonorariosUs.refentidade WITH uCrsConvencoesLin.refentidade 
		Replace ucrsConfgHonorariosUs.designentidade WITH uCrsConvencoesLin.designentidade 
		Replace ucrsConfgHonorariosUs.especialidade WITH uCrsConvencoesLin.especialidade 
		Replace ucrsConfgHonorariosUs.userno WITH CONFIGHONORARIOS.userno
				
	ENDSCAN 
	
	IF TYPE("CONVENCOES") != "U"
		uf_CONVENCOES_exit()
	ENDIF 
	
	IF TYPE("PESQCONVENCOES") != "U"
		uf_pesqconvencoes_sair()
	ENDIF 
	
	
ENDFUNC 


FUNCTION uf_CONFIGHONORARIOS_pesqrefsagrupadasSel
	SELECT ucrsPesqRefsAgrupadas
	GO TOP 
	SCAN FOR !EMPTY(ucrsPesqRefsAgrupadas.sel)
		
		SELECT ucrsConfgHonorariosUs
		CALCULATE MAX(ucrsConfgHonorariosUs.id) TO lcMaxId 
		lcMaxId = lcMaxId +1
		SELECT ucrsConfgHonorariosUs
		APPEND BLANK
				
		Replace ucrsConfgHonorariosUs.ref WITH ucrsPesqRefsAgrupadas.ref
		Replace ucrsConfgHonorariosUs.design WITH ucrsPesqRefsAgrupadas.design
		Replace ucrsConfgHonorariosUs.id WITH lcMaxId
		Replace ucrsConfgHonorariosUs.convencao WITH "SEM CONVENCAO"
		Replace ucrsConfgHonorariosUs.userno WITH CONFIGHONORARIOS.userno
	ENDSCAN 
	
ENDFUNC 


**
FUNCTION uf_CONFIGHONORARIOS_filtra
	LOCAL lcFiltro
	lcFiltro = ""
	
*!*		IF myConfigHonorariosAlteracao == .f.
*!*			uf_CONFIGHONORARIOS_actualizaDados()
*!*			RETURN .f.
*!*		ENDIF 
	
	Select ucrsConfgHonorariosUs
	SET FILTER TO 
	
	** Construir express�o do filtro
	
	&& especialidade
	IF !EMPTY(ALLTRIM(UPPER(CONFIGHONORARIOS.especialidade.value))) 

		IF !EMPTY(ALLTRIM(lcFiltro))
			lcFiltro = lcFiltro + [AND '] + ALLTRIM(UPPER(CONFIGHONORARIOS.especialidade.value)) + [' $ ALLTRIM(UPPER(especialidade))]
		ELSE
			lcFiltro = ['] + ALLTRIM(UPPER(CONFIGHONORARIOS.especialidade.value)) + [' $ ALLTRIM(UPPER(especialidade))]
		ENDIF
	ENDIF

	** Convencao
	IF !EMPTY(ALLTRIM(UPPER(CONFIGHONORARIOS.convencao.value)))
		IF !EMPTY(ALLTRIM(lcFiltro))
			lcFiltro = lcFiltro + ' AND UPPER(convencao) = ALLTRIM(UPPER(CONFIGHONORARIOS.convencao.value))'
		ELSE
			lcFiltro = ' ALLTRIM(UPPER(convencao)) = ALLTRIM(UPPER(CONFIGHONORARIOS.convencao.value))'
		ENDIF
	ENDIF

	** Designa��o
	IF !EMPTY(ALLTRIM(UPPER(CONFIGHONORARIOS.design.value)))
		IF !EMPTY(ALLTRIM(lcFiltro))
			lcFiltro = lcFiltro + ' AND UPPER(design) = ALLTRIM(UPPER(CONFIGHONORARIOS.design.value))'
		ELSE
			lcFiltro = ' ALLTRIM(UPPER(design)) = ALLTRIM(UPPER(CONFIGHONORARIOS.design.value))'
		ENDIF
	ENDIF
	

	** aplicar filtro
	SELECT ucrsConfgHonorariosUs
	GO top
	IF !EMPTY(ALLTRIM(lcFiltro))
		SELECT ucrsConfgHonorariosUs
		SET FILTER TO &lcFiltro
	ELSE
		SELECT ucrsConfgHonorariosUs
		SET FILTER TO 
	ENDIF

	SELECT ucrsConfgHonorariosUs
	GO top

	CONFIGHONORARIOS.gridPesq.refresh
ENDFUNC 

**
FUNCTION uf_CONFIGHONORARIOS_sair

	IF myConfigHonorariosAlteracao == .t.
		myConfigHonorariosAlteracao= .f.
		uf_CONFIGHONORARIOS_AlternaMenu()		
		uf_CONFIGHONORARIOS_actualizaDados()
		
		RETURN .f.
		
	ENDIF 

	**
	IF USED("ucrsConfgHonorariosUs")
		fecha("ucrsConfgHonorariosUs")
	ENDIF 
	
	
	CONFIGHONORARIOS.hide
	CONFIGHONORARIOS.release
ENDFUNC

