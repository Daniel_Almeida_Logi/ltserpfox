**
FUNCTION uf_FACTENTIDADESCLINICA_chama
	LPARAMETERS lcPainelOrigem 
	
	**Valida Licenciamento	
	IF (uf_gerais_addConnection('FEC') == .f.)
		RETURN .F.
	ENDIF

	IF lcPainelOrigem == "PESQUTENTES"
		IF TYPE("PESQUTENTES") != "U"
			uf_pesqutentes_sair()
		ENDIF
	ENDIF 
	
	uf_FACTENTIDADESCLINICA_cursoresIniciais()

	IF TYPE("FACTENTIDADESCLINICA") == "U"
		DO FORM FACTENTIDADESCLINICA
	ELSE
		FACTENTIDADESCLINICA.show
	ENDIF
	
	
	FACTENTIDADESCLINICA.Pageframe1.page1.gridPesq.setfocus
	uf_FACTENTIDADESCLINICA_AfterRowColChangeEntidade()

ENDFUNC 


**
FUNCTION uf_FACTENTIDADESCLINICA_carregaMenu
	FACTENTIDADESCLINICA.menu1.adicionaOpcao("actualizar", "Actualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_FACTENTIDADESCLINICA_atualiza","A")
	FACTENTIDADESCLINICA.menu1.adicionaOpcao("selTodos", "Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_FACTENTIDADESCLINICA_selTodos","T")
ENDFUNC 


**
FUNCTION uf_FACTENTIDADESCLINICA_cursoresIniciais
	LOCAL lcSQL
	lcSQL = ""	

	IF !USED("ucrsDadosFacturacaoEntidades")
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			declare @dataIni as datetime, @dataFim as datetime
			set @dataIni = dateadd(dd,-30,convert(date,dateadd(HOUR, <<difhoraria>>, getdate())))
			set @dataFim = convert(date,dateadd(HOUR, <<difhoraria>>, getdate()))
			exec up_clinica_DadosParaFacturacaoEntidades '', @dataIni, @dataFim
		ENDTEXT 
		
		If !uf_gerais_actGrelha("", "ucrsDadosFacturacaoEntidades", lcSql)
			uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR DADOS DE FATURACAO A ENTIDADES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF 
	ENDIF 
	IF !USED("ucrsDadosFacturacaoEntidadesUtentes")
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			declare @dataIni as datetime, @dataFim as datetime
			set @dataIni = dateadd(dd,-30,convert(date,dateadd(HOUR, <<difhoraria>>, getdate())))
			set @dataFim = convert(date,dateadd(HOUR, <<difhoraria>>, getdate()))
			exec up_clinica_DadosParaFacturacaoUtentes '', @dataIni, @dataFim
		ENDTEXT 

		If !uf_gerais_actGrelha("", "ucrsDadosFacturacaoUtentes", lcSql)
			uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR DADOS DE FATURACAO A UTENTES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF 
	ENDIF
	IF !USED("ucrsDadosFacturacaoEntidadesDetalhe")
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			declare @dataIni as datetime, @dataFim as datetime
			set @dataIni = dateadd(dd,-30,convert(date,dateadd(HOUR, <<difhoraria>>, getdate())))
			set @dataFim = convert(date,dateadd(HOUR, <<difhoraria>>, getdate()))
			exec up_clinica_DadosParaFacturacaoEntidadesDetalhe '', @dataIni, @dataFim,'',''
		ENDTEXT 
		
		If !uf_gerais_actGrelha("", "ucrsDadosFacturacaoEntidadesDetalhe", lcSql)
			uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR DADOS DE FATURACAO A ENTIDADES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF 
	ENDIF
	IF !USED("ucrsDadosFacturacaoUtentesDetalhe")
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			declare @dataIni as datetime, @dataFim as datetime
			set @dataIni = dateadd(dd,-30,convert(date,dateadd(HOUR, <<difhoraria>>, getdate())))
			set @dataFim = convert(date,dateadd(HOUR, <<difhoraria>>, getdate()))
			exec up_clinica_DadosParaFacturacaoUtentesDetalhe '',@dataIni, @dataFim,'',''
		ENDTEXT 

*!*	**
*!*	_cliptext = lcSQL
*!*	MESSAGEBOX(lcSQL)

		If !uf_gerais_actGrelha("", "ucrsDadosFacturacaoUtentesDetalhe", lcSql)
			uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR DADOS DE FATURACAO A ENTIDADES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF 
	ENDIF

ENDFUNC 


**
FUNCTION uf_FACTENTIDADESCLINICA_selTodos

	DO CASE 
		CASE FACTENTIDADESCLINICA.PageFrame1.activePage == 2
			**		
			
		CASE FACTENTIDADESCLINICA.PageFrame1.activePage == 1	
	
			IF EMPTY(FACTENTIDADESCLINICA.menu1.seltodos.tag) OR FACTENTIDADESCLINICA.menu1.seltodos.tag == "false"
				&& aplica sele��o
				REPLACE ALL sel WITH .t. IN ucrsDadosFacturacaoEntidadesDetalhe
				
				&& altera o botao
				FACTENTIDADESCLINICA.menu1.selTodos.tag = "true"
				FACTENTIDADESCLINICA.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\checked_w.png", "uf_FACTENTIDADESCLINICA_selTodos","T")

			ELSE
				&& aplica sele��o
				REPLACE ALL sel WITH .f. IN ucrsDadosFacturacaoEntidadesDetalhe
				
				&& altera o botao
				FACTENTIDADESCLINICA.menu1.selTodos.tag = "false"
				FACTENTIDADESCLINICA.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_FACTENTIDADESCLINICA_selTodos","T")
				
			ENDIF
			
			LOCAL lcTotal
			lcTotal = 0
			SELECT ucrsDadosFacturacaoEntidadesDetalhe
			GO top
			UF_FACTENTIDADESCLINICA_calculaTotalEntidades()
			
		CASE FACTENTIDADESCLINICA.PageFrame1.activePage == 3
		
			IF EMPTY(FACTENTIDADESCLINICA.menu1.seltodos.tag) OR FACTENTIDADESCLINICA.menu1.seltodos.tag == "false"
				&& aplica sele��o
				REPLACE ALL sel WITH .t. IN ucrsDadosFacturacaoUtentes
				
				&& altera o botao
				FACTENTIDADESCLINICA.menu1.selTodos.tag = "true"
				FACTENTIDADESCLINICA.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\checked_w.png", "uf_FACTENTIDADESCLINICA_selTodos","T")

			ELSE
				&& aplica sele��o
				REPLACE ALL sel WITH .f. IN ucrsDadosFacturacaoUtentes
				
				&& altera o botao
				FACTENTIDADESCLINICA.menu1.selTodos.tag = "false"
				FACTENTIDADESCLINICA.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_FACTENTIDADESCLINICA_selTodos","T")
				
			ENDIF
			
			SELECT ucrsDadosFacturacaoUtentes
			GO top
		
		OTHERWISE
			**
	ENDCASE

ENDFUNC 


**
FUNCTION UF_FACTENTIDADESCLINICA_calculaTotalEntidades
	SELECT ucrsDadosFacturacaoEntidadesDetalhe
	CALCULATE SUM(ucrsDadosFacturacaoEntidadesDetalhe.faturarEntidade) FOR !EMPTY(ucrsDadosFacturacaoEntidadesDetalhe.sel) TO lcTotal
	FACTENTIDADESCLINICA.PageFrame1.page1.total.value = lcTotal
	FACTENTIDADESCLINICA.PageFrame1.page1.total.refresh
ENDFUNC 


**
FUNCTION uf_FACTENTIDADESCLINICA_atualiza
	LOCAL lcSQL
	lcSQL = ""
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_clinica_DadosParaFacturacaoEntidades 
			'<<ALLTRIM(FACTENTIDADESCLINICA.entidade.value)>>'
			,'<<uf_gerais_getdate(FACTENTIDADESCLINICA.dataIni.Value,"SQL")>>'
			,'<<uf_gerais_getdate(FACTENTIDADESCLINICA.dataFim.Value,"SQL")>>'
	ENDTEXT 
		
	If !uf_gerais_actGrelha("FACTENTIDADESCLINICA.Pageframe1.page1.gridPesq", "ucrsDadosFacturacaoEntidades", lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR DADOS DE FATURACAO A ENTIDADES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF 
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_clinica_DadosParaFacturacaoEntidadesDetalhe 
			'<<ALLTRIM(FACTENTIDADESCLINICA.utente.value)>>'
			,'<<uf_gerais_getdate(FACTENTIDADESCLINICA.dataIni.Value,"SQL")>>'
			,'<<uf_gerais_getdate(FACTENTIDADESCLINICA.dataFim.Value,"SQL")>>'
			,'<<ALLTRIM(FACTENTIDADESCLINICA.entidade.value)>>'
			,'<<ALLTRIM(FACTENTIDADESCLINICA.servico.value)>>'
	ENDTEXT 
		
	If !uf_gerais_actGrelha("FACTENTIDADESCLINICA.Pageframe1.page1.gridDetalhe", "ucrsDadosFacturacaoEntidadesDetalhe", lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR DADOS DE FATURACAO A ENTIDADES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF 
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_clinica_DadosParaFacturacaoUtentesDetalhe
			'<<ALLTRIM(FACTENTIDADESCLINICA.utente.value)>>'
			,'<<uf_gerais_getdate(FACTENTIDADESCLINICA.dataIni.Value,"SQL")>>'
			,'<<uf_gerais_getdate(FACTENTIDADESCLINICA.dataFim.Value,"SQL")>>'
			,'<<ALLTRIM(FACTENTIDADESCLINICA.entidade.value)>>'
			,'<<ALLTRIM(FACTENTIDADESCLINICA.servico.value)>>'
	ENDTEXT 
*!*	**
*!*	_cliptext = lcSQL
*!*	MESSAGEBOX(lcSQL)
*!*	*!*			
	If !uf_gerais_actGrelha("FACTENTIDADESCLINICA.Pageframe1.page3.gridDetalhe", "ucrsDadosFacturacaoUtentesDetalhe", lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR DADOS DE FATURACAO A ENTIDADES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF 
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_clinica_DadosParaFacturacaoUtentes 
			'<<ALLTRIM(FACTENTIDADESCLINICA.utente.value)>>'
			,'<<uf_gerais_getdate(FACTENTIDADESCLINICA.dataIni.Value,"SQL")>>'
			,'<<uf_gerais_getdate(FACTENTIDADESCLINICA.dataFim.Value,"SQL")>>'
	ENDTEXT 
		
	If !uf_gerais_actGrelha("FACTENTIDADESCLINICA.Pageframe1.page3.gridPesq", "ucrsDadosFacturacaoUtentes", lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR DADOS DE FATURACAO A ENTIDADES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF 
	
	FACTENTIDADESCLINICA.Pageframe1.page1.gridPesq.AfterRowColChange
	FACTENTIDADESCLINICA.Pageframe1.page3.gridPesq.AfterRowColChange
	FACTENTIDADESCLINICA.Pageframe1.page1.gridPesq.setfocus
	
ENDFUNC 



**
FUNCTION uf_FACTENTIDADESCLINICA_emitirFacturasEntidades
	LPARAMETERS lcReferente
	
	** Valida se existem linhas de varias entidades Selecionadas
	IF USED("ucrsControlaVariasEntidades")
		fecha("ucrsControlaVariasEntidades")
	ENDIF 
	
	select ucrsDadosFacturacaoEntidadesDetalhe 
	Go Top
	Select entidade from ucrsDadosFacturacaoEntidadesDetalhe WHERE !EMPTY(ucrsDadosFacturacaoEntidadesDetalhe.sel) group BY Entidade INTO CURSOR ucrsControlaVariasEntidades READWRITE
	
	IF RECCOUNT("ucrsControlaVariasEntidades")>1
		uf_perguntalt_chama("N�o � possivel faturar v�rias entidades ao mesmo tempo. Por favor verifique.","OK","",64)
		RETURN .f.
	ENDIF 
	IF USED("ucrsControlaVariasEntidades")
		fecha("ucrsControlaVariasEntidades")
	ENDIF 
	*******************************
	
	
	** Chamar Painel de Factura��o
	uf_facturacao_chama('')
	
	** Novo Documento
	uf_Facturacao_novo(.t.)
	
	
	** Tipo Documentos
	IF USED("TD")
		fecha("TD")
	ENDIF
	
	SELECT * FROM uCrsGeralTD WHERE uCrsGeralTD.ndoc == mySEnt AND ALLTRIM(uCrsGeralTD.site) == ALLTRIM(mySite) INTO CURSOR TD READWRITE
	SELECT TD
	FACTURACAO.ContainerCab.nmdoc.value = ALLTRIM(TD.nmdoc)


	Select ucrsDadosFacturacaoEntidadesDetalhe 
	GO TOP 
	LOCATE FOR !EMPTY(ucrsDadosFacturacaoEntidadesDetalhe.sel)
	** Configurar Cliente
	lnNo = ucrsDadosFacturacaoEntidadesDetalhe.Entidadeno
	lnEstab = ucrsDadosFacturacaoEntidadesDetalhe.Entidadeestab
	
	TEXT TO lcSQL TEXTMERGE noshow
		exec up_clientes_pesquisa 1, '', <<lnNo>>, <<lnEstab>>, '', '','', '', '','', 0, '', 0, '', 0, '','', '', '', '', '', 0,'', '', '', 0,0, ''
	ENDTEXT

	uf_gerais_actGrelha("", "uCrsPesquisarCLAux", lcSQL)		
	
	**
	uf_atendimento_selCliente(lnNo , lnEstab ,"FACTURACAO")
			
	** desconto do cliente
	facturacao.descontoCL = uCrsPesquisarCLAux.desconto
	facturacao.noCreditCL = uCrsPesquisarCLAux.nocredit
	facturacao.modoFactCL = uCrsPesquisarCLAux.modofact
		
	uf_facturacao_dataVencimentoCliente()
		
	&& Tabala fixa de Iva para clientes estrangeiros
	uf_facturacao_TabelaFixaIvaClientes(uCrsPesquisarCLAux.tabIva)
	
	IF !EMPTY(lcReferente)
		UPDATE ft2 SET obsdoc = lcReferente
	ENDIF 
	
	** Configurar Cabe�alho
	lclordem = 0
	SELECT Fi
	GO Top
	SCAN
		DELETE 
	ENDSCAN 
	

	** Configurar Linhas
	Select ucrsFtEnt 
	Go top
	SCAN 
		
		lnDesign = ucrsFtEnt.design
		
		uf_atendimento_AdicionaLinhaFi(.t.,.t.)
		REPLACE fi.ref WITH ucrsFtEnt.ref
		REPLACE fi.refentidade WITH ucrsFtEnt.refentidade
		uf_facturacao_eventoRefFi(.t.)
		
		Replace fi.design WITH lnDesign
		Replace fi.qtt WITH ucrsFtEnt.qtt
		Replace fi.lordem WITH lclordem + 1
		lclordem = lclordem +1
		
		&& campo usado para liga��o entre as tabelas - trigger
		&&Replace Fi.bistamp 	With ucrsDadosFacturacaoEntidadesDetalhe.servmrstamp

		REPLACE	FI.U_EPVP	WITH  ucrsFtEnt.FaturarEntidade / ucrsFtEnt.qtt
		REPLACE	FI.EPV	WITH  ucrsFtEnt.FaturarEntidade / ucrsFtEnt.qtt
		REPLACE	FI.ETILIQUIDO	WITH  ucrsFtEnt.FaturarEntidade 
		REPLACE	FI.TILIQUIDO	WITH  ucrsFtEnt.FaturarEntidade * 200.482
	
		uf_atendimento_fiiliq(.t.)
		
	ENDSCAN
	

	**	
	uf_atendimento_actTotaisFt()
	FACTURACAO.refresh
	
	uf_FACTENTIDADESCLINICA_sair()
ENDFUNC 


**
FUNCTION uf_FACTENTIDADESCLINICA_emitirFacturasUtentes
	LPARAMETERS lcOrigemMarcacoes, lcCredito, lcEcra
	LOCAL lnDesign, lnNo, lnEstab
	LOCAL lcNrATendimento, lcDinheiro, lcMb, lcVisa, lcCheque, lcModoPag3, lcModoPag4, lcModoPag5, lcModoPag6    
	STORE '' TO lcNrATendimento 
	STORE 0 TO lcDinheiro, lcMb, lcVisa, lcCheque, lcModoPag3, lcModoPag4, lcModoPag5, lcModoPag6    	
	
	
	IF ALLTRIM(UPPER(lcEcra)) == "MRPAGAMENTO"
		uf_marcacoes_sair()
	ENDIF 

	** Chamar Painel de Factura��o
	uf_facturacao_chama('')
	
	** Novo Documento
	uf_Facturacao_novo(.t.)
	
	
	** Tipo Documentos
	IF USED("TD")
		fecha("TD")
	ENDIF
	
	SELECT * FROM uCrsGeralTD WHERE uCrsGeralTD.ndoc == IIF(lcCredito,myFact,myVd) AND ALLTRIM(uCrsGeralTD.site) == ALLTRIM(mySite) INTO CURSOR TD READWRITE
	SELECT TD
	FACTURACAO.ContainerCab.nmdoc.value = ALLTRIM(TD.nmdoc)


	** Configurar Cliente
	SELECT ucrsDadosFacturacaoUtentes
	GO TOP 
	lnNo = ucrsDadosFacturacaoUtentes.Utenteno
	lnEstab = ucrsDadosFacturacaoUtentes.Utenteestab
	
	TEXT TO lcSQL TEXTMERGE noshow
		exec up_clientes_pesquisa 1, '', <<lnNo>>, <<lnEstab>>, '', '','', '', '','', 0, '>', -999999, '', 0, '','', '', '', '', '', 0,'', '', '', 0,0, ''
	ENDTEXT
	uf_gerais_actGrelha("", "uCrsPesquisarCLAux", lcSQL)		
	
	** desconto do cliente
	facturacao.descontoCL = uCrsPesquisarCLAux.desconto
	facturacao.noCreditCL = uCrsPesquisarCLAux.nocredit
	facturacao.modoFactCL = uCrsPesquisarCLAux.modofact
	
	**
	uf_atendimento_selCliente(lnNo , lnEstab ,"FACTURACAO")
		
	uf_facturacao_dataVencimentoCliente()
		
	&& Tabala fixa de Iva para clientes estrangeiros
	uf_facturacao_TabelaFixaIvaClientes(uCrsPesquisarCLAux.tabIva)
	
	
	** Configurar Cabe�alho
	lclordem = 0
	SELECT Fi
	GO Top
	SCAN
		DELETE 
	ENDSCAN 
	

	** Configurar Linhas
	Select ucrsDadosFacturacaoUtentesDetalhe
	Go top
	SCAN FOR !EMPTY(ucrsDadosFacturacaoUtentesDetalhe.sel) OR ALLTRIM(UPPER(lcEcra)) == "MRPAGAMENTO"
	
	
		lnDesign = uf_gerais_getdate(ucrsDadosFacturacaoUtentesDetalhe.dataInicio);
					+ " - " + ucrsDadosFacturacaoUtentesDetalhe.design
					
		uf_atendimento_AdicionaLinhaFi(.t.,.t.)
		REPLACE fi.ref WITH ucrsDadosFacturacaoUtentesDetalhe.ref
		uf_facturacao_eventoRefFi(.t.)
		
		
		
		Replace fi.design WITH lnDesign
		Replace fi.qtt WITH ucrsDadosFacturacaoUtentesDetalhe.qtt
		Replace fi.lordem WITH lclordem + 1
		lclordem = lclordem +1
		
		&& campo usado para liga��o entre as tabelas - trigger
		Replace Fi.bistamp 	With ucrsDadosFacturacaoUtentesDetalhe.servmrstamp

		REPLACE	FI.U_EPVP	WITH  ucrsDadosFacturacaoUtentesDetalhe.FaturarUtente
		REPLACE	FI.EPV	WITH  ucrsDadosFacturacaoUtentesDetalhe.FaturarUtente
		REPLACE	FI.ETILIQUIDO	WITH  ucrsDadosFacturacaoUtentesDetalhe.FaturarUtente * ucrsDadosFacturacaoUtentesDetalhe.qtt
		REPLACE	FI.TILIQUIDO	WITH  ucrsDadosFacturacaoUtentesDetalhe.FaturarUtente * ucrsDadosFacturacaoUtentesDetalhe.qtt * 200.482
	
		uf_atendimento_fiiliq(.t.)
		
	ENDSCAN
	

	**	
	uf_atendimento_actTotaisFt()
	
	
	IF USED("ucrsCabMrPag")
		** Actualiza Meios de Pagamento
		SELECT ucrsCabMrPag
		lcNrATendimento = ucrsCabMrPag.nrAtendimento
		lcDinheiro = ucrsCabMrPag.dinheiro
		lcMb = ucrsCabMrPag.mb
		lcVisa = ucrsCabMrPag.visa
		lcCheque = ucrsCabMrPag.cheque
		lcModoPag3 = ucrsCabMrPag.modPag3
		lcModoPag4 = ucrsCabMrPag.modPag4
		lcModoPag5 = ucrsCabMrPag.modPag5
		lcModoPag6 = ucrsCabMrPag.modPag6
		
			
		SELECT Ft
		UPDATE ft SET ft.u_nratend = lcNrATendimento	
		
		SELECT ucrsPagCentral
		update ucrsPagCentral set ucrsPagCentral.evdinheiro = lcDinheiro, ucrsPagCentral.epaga1 = lcVisa;
			,ucrsPagCentral.epaga2 = lcMb,ucrsPagCentral.echtotal = lcCheque, ucrsPagCentral.epaga3 = lcCheque;
			,ucrsPagCentral.epaga4 = lcModoPag3, ucrsPagCentral.epaga5 = lcModoPag4, ucrsPagCentral.epaga6 = lcModoPag6
		
		FACTURACAO.refresh
	ENDIF 
	
ENDFUNC 


**
FUNCTION UF_FACTENTIDADESCLINICA_Certifica
	LPARAMETERS lcFtStamp
	
	LOCAL lcSQL
    lcSQL=''
    TEXT TO lcSQL TEXTMERGE NOSHOW
        SELECT
             ftstamp, fno, ndoc, convert(varchar,ousrdata,112) ousrdata, ousrhora, etotal, efinv
        from
            ft (nolock)
        where
            ftstamp = '<<lcFtStamp>>'
    ENDTEXT

    IF !uf_gerais_actgrelha("","uCrsFTCert",lcSQL)
        MESSAGEBOX("ERRO A LER A TABELA FT",16)
    ELSE
        SELECT uCrsFTCert
        GO TOP
        SCAN
            uf_gravarCert(uCrsFTCert.ftstamp, uCrsFTCert.fno, uCrsFTCert.ndoc, uCrsFTCert.ousrdata, uCrsFTCert.ousrdata, uCrsFTCert.ousrhora, ROUND(uCrsFTCert.Etotal+uCrsFTCert.efinv,2), 'FT')
        ENDSCAN
    ENDIF
ENDFUNC 


** 
FUNCTION uf_FACTENTIDADESCLINICA_AfterRowColChangeEntidade

	Select ucrsDadosFacturacaoEntidadesDetalhe
	SET FILTER TO ucrsDadosFacturacaoEntidadesDetalhe.Entidadeno == ucrsDadosFacturacaoEntidades.Entidadeno;
			AND ucrsDadosFacturacaoEntidadesDetalhe.Entidadeestab == ucrsDadosFacturacaoEntidades.Entidadeestab
			
	Select ucrsDadosFacturacaoEntidadesDetalhe
	GO Top
	
	UPDATE ucrsDadosFacturacaoEntidadesDetalhe SET ucrsDadosFacturacaoEntidadesDetalhe.sel = .f.
	
	**
	UF_FACTENTIDADESCLINICA_calculaTotalEntidades()	

	Select ucrsDadosFacturacaoEntidadesDetalhe
	GO Top
	FACTENTIDADESCLINICA.Pageframe1.page1.gridDetalhe.refresh	
	
ENDFUNC



** 
FUNCTION uf_FACTENTIDADESCLINICA_AfterRowColChangeUtente

	Select ucrsDadosFacturacaoUtentesDetalhe
	SET FILTER TO ucrsDadosFacturacaoUtentesDetalhe.no == ucrsDadosFacturacaoUtentes.Utenteno;
			AND ucrsDadosFacturacaoUtentesDetalhe.estab == ucrsDadosFacturacaoUtentes.Utenteestab

	FACTENTIDADESCLINICA.Pageframe1.page3.gridDetalhe.refresh
			
ENDFUNC


**
FUNCTION uf_FACTENTIDADESCLINICA_AlternaMenu
	DO CASE 
		CASE FACTENTIDADESCLINICA.Pageframe1.activePage == 1
			FACTENTIDADESCLINICA.menu1.estado("", "SHOW", "Ft. Entidades", .t., "Sair", .t.)
		CASE FACTENTIDADESCLINICA.Pageframe1.activePage == 3
			FACTENTIDADESCLINICA.menu1.estado("", "SHOW", "Ft. Utentes", .t., "Sair", .t.)
		OTHERWISE
	ENDCASE 
ENDFUNC 



** 
FUNCTION uf_FACTENTIDADESCLINICA_marcaDetalhesEntidade

	SELECT ucrsDadosFacturacaoEntidades
	lcno = ucrsDadosFacturacaoEntidades.EntidadeNo
	lcestab = ucrsDadosFacturacaoEntidades.EntidadeEstab
	lcSel = ucrsDadosFacturacaoEntidades.sel
	
	update ucrsDadosFacturacaoEntidadesDetalhe set sel = lcSel WHERE EntidadeNo= lcno  AND EntidadeEstab= lcestab 
	
	IF !EMPTY(lcSel)
		update ucrsDadosFacturacaoEntidadesDetalhe set sel = .f. WHERE EntidadeNo != lcno 
		update ucrsDadosFacturacaoEntidades set sel = .f. WHERE EntidadeNo != lcno 
	ENDIF 
	
	FACTENTIDADESCLINICA.refresh
ENDFUNC 



FUNCTION uf_FACTENTIDADESCLINICA_marcaDetalhesUtente

	SELECT ucrsDadosFacturacaoUtentes
	lcno = ucrsDadosFacturacaoUtentes.UtenteNo
	lcestab = ucrsDadosFacturacaoUtentes.UtenteEstab
	lcSel = ucrsDadosFacturacaoUtentes.sel
	
	update ucrsDadosFacturacaoUtentesDetalhe set sel = lcSel WHERE No = lcno  AND Estab = lcestab
	
	IF !EMPTY(lcSel)
		update ucrsDadosFacturacaoUtentesDetalhe set sel = .f. WHERE No != lcno 
		update ucrsDadosFacturacaoUtentes set sel = .f. WHERE UtenteNo != lcno 
	ENDIF 
	
	FACTENTIDADESCLINICA.refresh
ENDFUNC 


**
FUNCTION uf_FACTENTIDADESCLINICA_gravar
	LOCAL lcReferente
	lcReferente = ""

	DO CASE 
		CASE FACTENTIDADESCLINICA.Pageframe1.activePage == 1

			DO CASE 
				CASE LEFT(ALLTRIM(FACTENTIDADESCLINICA.configImp.value),1) == "1"
					
					lcReferente = "Referente a: CONSULTAS EFECTUADAS ENTRE: " + uf_gerais_getdate(FACTENTIDADESCLINICA.dataIni.Value) + " e " + uf_gerais_getdate(FACTENTIDADESCLINICA.dataFim.Value)
					SELECT ref, refentidade , ALLTRIM(design) as design, SUM(qtt) as qtt, SUM(faturarentidade) as faturarentidade WHERE !EMPTY(sel) FROM ucrsDadosFacturacaoEntidadesDetalhe group by ref, refentidade, design INTO CURSOR ucrsFtEnt READWRITE 

				CASE LEFT(ALLTRIM(FACTENTIDADESCLINICA.configImp.value),1) == "2"

					IF EMPTY(ALLTRIM(FACTENTIDADESCLINICA.utente.value))
						uf_perguntalt_chama("Para usar esta configura��o de impress�o � necess�rio selecionar o utente.","OK","",64)
						RETURN .f.
					ENDIF 
					
					
					** Calcula Nome do Utente e Apolice
					SELECT ucrsDadosFacturacaoEntidades
					TEXT TO lcSQL NOSHOW TEXTMERGE 
						
						Select 
							cartao,b_utentes.nome 
						from 
							B_entidadesUtentes 
							inner join b_utentes on B_entidadesUtentes.no = b_utentes.no
						where 
							eno = <<ucrsDadosFacturacaoEntidades.entidadeno>>
							and b_utentes.nome = '<<ALLTRIM(FACTENTIDADESCLINICA.utente.value)>>'
					ENDTEXT
					If !uf_gerais_actGrelha("", "ucrsDadosUtFtEnt", lcSql)
						uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR DADOS DO UTENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
						RETURN .f.
					ENDIF
					
					lcReferente = "Referente a: " + ALLTRIM(ucrsDadosUtFtEnt.nome) + "," + " Ap�lice n� " + ALLTRIM(ucrsDadosUtFtEnt.cartao) + ", PROC. " 
					SELECT ref, refentidade , uf_gerais_getdate(ucrsDadosFacturacaoEntidadesDetalhe.dataInicio) + " " + ALLTRIM(design) as design, qtt, faturarentidade WHERE !EMPTY(sel) FROM ucrsDadosFacturacaoEntidadesDetalhe INTO CURSOR ucrsFtEnt READWRITE 
	
				CASE LEFT(ALLTRIM(FACTENTIDADESCLINICA.configImp.value),1) == "3"
					
					lcReferente = "Referente a: CONSULTAS EFECTUADAS ENTRE: " + uf_gerais_getdate(FACTENTIDADESCLINICA.dataIni.Value) + " e " + uf_gerais_getdate(FACTENTIDADESCLINICA.dataFim.Value)
					SELECT ref, refentidade , uf_gerais_getdate(ucrsDadosFacturacaoEntidadesDetalhe.dataInicio) + " " + ALLTRIM(design) + " - " + ALLTRIM(nome) as design , qtt, faturarentidade WHERE !EMPTY(sel) FROM ucrsDadosFacturacaoEntidadesDetalhe INTO CURSOR ucrsFtEnt READWRITE 
			ENDCASE
		
		
			IF USED("ucrsFtEnt")
				uf_FACTENTIDADESCLINICA_emitirFacturasEntidades(lcReferente)
			ENDIF 
			
		CASE FACTENTIDADESCLINICA.Pageframe1.activePage == 3
			uf_FACTENTIDADESCLINICA_emitirFacturasUtentes(.f., .f., "FACTENTIDADESCLINICA")
		OTHERWISE
	ENDCASE 
ENDFUNC 



**
FUNCTION uf_FACTENTIDADESCLINICA_sair
	
	
	
	FACTENTIDADESCLINICA.hide
	FACTENTIDADESCLINICA.release
ENDFUNC




** Emitir ficheiro SAF(T) 
Function uf_FACTENTIDADESCLINICA_EXPORT_FT
	LPARAMETERS lcTipo
	
	
	LOCAL lcValidaFile 	
	
	lcDirectoria = Alltrim(GETDIR())
	lcDiretoriaLocal = GETENV('TEMP')
	
	** abrir regua de progresso
	regua(0,3,"A obter dados para ficheiro "+ UPPER(ALLTRIM(lcTipo)) +"...")	
	
	LOCAL lcStamp
	
	** Gerar e correr comando para gerar o SAFT 
	DO CASE 
		CASE UPPER(ALLTRIM(lcTipo)) == "ADM"	
			Text To lcSql Noshow Textmerge
				exec up_clinica_exportFt_ADM '<<ft.ftstamp>>'
			ENDTEXT
			
		CASE UPPER(ALLTRIM(lcTipo)) == "ADSE"
			Text To lcSql Noshow Textmerge
				exec up_clinica_exportFt_ADSE '<<ft.ftstamp>>'
			ENDTEXT
		OTHERWISE
			uf_perguntalt_chama("Par�metro inv�lido. Por favor contacte o suporte.","OK","",48)
			RETURN .f.
	ENDCASE

*!*	_CLIPTEXT = lcSQL
*!*	MESSAGEBOX(lcSQL)
	
	If !uf_gerais_actGrelha("","uCrsExportFtClinica",lcSql)
		REGUA(2)
		uf_perguntalt_chama("N�o foi poss�vel gerar o ficheiro para a Entidade. Por favor contacte o suporte.","OK","",16)
	Else

		REGUA(1,3,"A gerar ficheiro "+ ALLTRIM(lcTipo) + "...")	
		
		IF  Reccount("uCrsExportFtClinica")>0
		
		
		
			SELECT Ft
			lcFile = lcDirectoria + "\" + Alltrim(ft.ftstamp) + right(uf_gerais_getDate(Ft.fdata,"DATA"),4) + '.txt'
			
			lck=0
			SELECT uCrsExportFtClinica
			GO Top
			SCAN 
					
				Replace  uCrsExportFtClinica.texto WITH uCrsExportFtClinica.texto + CHR(13) + CHR(10)
			**TRY 
				Set Safety off
				
				IF lcK = 0
					COPY MEMO uCrsExportFtClinica.texto   TO (lcFile)
					lcK = 1
				ELSE
					COPY MEMO uCrsExportFtClinica.texto TO (lcFile) ADDITIVE
				ENDIF 
			
				
				Set Safety on		
				lcValidaFile = .t.
*!*				CATCH
*!*				
*!*					lcValidaFile = .f.			
*!*					lcStamp = uf_gerais_stamp()
*!*					
*!*				ENDTRY 
			ENDSCAN 
			REGUA(2)		
			IF lcValidaFile == .f.
				uf_perguntalt_chama("N�o foi poss�vel gerar o ficheiro para a Entidade. Por favor contacte o suporte.","OK","",16)
				Fecha("uCrsExportFtClinica")
			ELSE
				uf_perguntalt_chama("FICHEIRO GERADO COM SUCESSO.","OK","",64)
				
			ENDIF
			
		ELSE 
			REGUA(2)
			uf_perguntalt_chama("N�O FORAM ENCONTRADOS MOVIMENTOS PARA GERAR O FICHEIRO.","OK","",48)
		ENDIF 
		
		Fecha("uCrsExportFtClinica")
	Endif
	
ENDFUNC


**
FUNCTION uf_FACTENTIDADESCLINICA_actualizaMarcacoes

	IF USED("ucrsDadosFacturacaoEntidadesDetalhe") 
		SELECT ucrsDadosFacturacaoEntidadesDetalhe
		GO Top
		SCAN FOR !EMPTY(ucrsDadosFacturacaoEntidadesDetalhe.sel)
			TEXT TO lcSQL NOSHOW TEXTMERGE 

				UPDATE 
					marcacoesServ 
				SET 
					fe = 1
				FROM
					marcacoesServ
				WHERE 
					marcacoesServ.servmrstamp = '<<ALLTRIM(ucrsDadosFacturacaoEntidadesDetalhe.servmrstamp)>>'
					
					
			ENDTEXT 
		
			If !uf_gerais_actGrelha("", "", lcSql)
				uf_perguntalt_chama("N�O FOI POSS�VEL ACTUALIZAR AS MARCA��ES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF	

		ENDSCAN
		
		fecha("ucrsDadosFacturacaoEntidadesDetalhe")
	ENDIF 	


ENDFUNC